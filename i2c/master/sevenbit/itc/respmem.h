/*
   Copyright (C) 2006 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_i2c_master_sevenbit_itc_respmemh_
#define _oscl_i2c_master_sevenbit_itc_respmemh_
#include "oscl/memory/block.h"
#include "respapi.h"

/** */
namespace Oscl {
/** */
namespace I2C {
/** */
namespace Master {
/** */
namespace SevenBit {
/** */
namespace Resp {

struct ReadRespMem {
	/** */
	Oscl::Memory::
	AlignedBlock<sizeof(SevenBit::Resp::Api::ReadResp)>		resp;
	/** */
	Oscl::Memory::
	AlignedBlock<sizeof(SevenBit::Req::Api::ReadPayload)>	payload;
	};

struct CancelReadRespMem {
	/** */
	Oscl::Memory::
	AlignedBlock<sizeof(SevenBit::Resp::Api::CancelReadResp)>		resp;
	/** */
	Oscl::Memory::
	AlignedBlock<sizeof(SevenBit::Req::Api::CancelReadPayload)>	payload;
	};

struct WriteRespMem {
	/** */
	Oscl::Memory::
	AlignedBlock<sizeof(SevenBit::Resp::Api::WriteResp)>		resp;
	/** */
	Oscl::Memory::
	AlignedBlock<sizeof(SevenBit::Req::Api::WritePayload)>	payload;
	};

struct CancelWriteRespMem {
	/** */
	Oscl::Memory::
	AlignedBlock<sizeof(SevenBit::Resp::Api::CancelWriteResp)>		resp;
	/** */
	Oscl::Memory::
	AlignedBlock<sizeof(SevenBit::Req::Api::CancelWritePayload)>	payload;
	};

union Mem {
	/** */
	void*			__qitemlink;
	/** */
	ReadRespMem		read;
	/** */
	WriteRespMem	write;
	};

union CancelMem {
	/** */
	void*			__qitemlink;
	/** */
	CancelReadRespMem	cancelRead;
	/** */
	CancelWriteRespMem	cancelWrite;
	};

}
}
}
}
}

#endif
