/*
   Copyright (C) 2006 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_i2c_master_sevenbit_itc_reqapih_
#define _oscl_i2c_master_sevenbit_itc_reqapih_
#include "oscl/mt/itc/mbox/srvreq.h"
#include "oscl/mt/itc/mbox/sap.h"
#include "oscl/buffer/var.h"

/** */
namespace Oscl {
/** */
namespace I2C {
/** */
namespace Master {
/** */
namespace SevenBit {
/** */
namespace Req {

/** */
class Api {
	public:
		/** */
		class ReadPayload {
			public:
				/** */
				void*	const		_dataBuffer;
				/** */
				const unsigned		_nDataBytesToRead;
				/** */
				const unsigned char	_address;
				/** */
				unsigned char		_failed;
			public:
				/** */
				ReadPayload(	unsigned char	address,
								void*			dataBuffer,
								unsigned		nDataBytesToRead
								) noexcept;
			};
		/** */
		typedef Oscl::Mt::Itc::SrvRequest<Api,ReadPayload>		ReadReq;
		/** */
		class CancelReadPayload {
			public:
				/** */
				ReadReq&	_readToCancel;
			public:
				/** */
				CancelReadPayload(ReadReq& readToCancel) noexcept;
			};
		/** */
		typedef Oscl::Mt::Itc::SrvRequest<Api,CancelReadPayload>	CancelReadReq;
		/** */
		class WritePayload {
			public:
				/** */
				const void*	const	_data;
				/** */
				const unsigned		_nDataBytes;
				/** */
				const unsigned char	_address;
				/** */
				unsigned char		_failed;
			public:
				/** */
				WritePayload(	unsigned char	address,
								const void*		data,
								unsigned		nDataBytes
								) noexcept;
			};
		/** */
		typedef Oscl::Mt::Itc::SrvRequest<Api,WritePayload>		WriteReq;
		/** */
		class CancelWritePayload {
			public:
				/** */
				WriteReq&	_writeToCancel;
			public:
				/** */
				CancelWritePayload(WriteReq& writeToCancel) noexcept;
			};
		/** */
		typedef Oscl::Mt::Itc::SrvRequest<Api,CancelWritePayload>	CancelWriteReq;
		/** */
		typedef Oscl::Mt::Itc::SAP<Api>	SAP;
		/** */
		typedef Oscl::Mt::Itc::ConcreteSAP<Api>	ConcreteSAP;
	public:
		/** */
		virtual ~Api() {}
		/** */
		virtual void	request(ReadReq& msg) noexcept=0;
		/** */
		virtual void	request(CancelReadReq& msg) noexcept=0;
		/** */
		virtual void	request(WriteReq& msg) noexcept=0;
		/** */
		virtual void	request(CancelWriteReq& msg) noexcept=0;
	};

}
}
}
}
}

#endif
