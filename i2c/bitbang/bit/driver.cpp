#include "driver.h"

using namespace Oscl::I2C::BitBang::Bit;

Driver::Driver(	Oscl::I2C::BitBang::Driver::Api&	driverApi,
				Oscl::Delay::Nano::Api&				nanoDelayApi
				) noexcept:
		_driverApi(driverApi),
		_nanoDelayApi(nanoDelayApi)
		{
	}

bool	Driver::resetBus() noexcept{
	_driverApi.sdaHigh();
	_nanoDelayApi.wait(1000);
	_driverApi.sclHigh();
	if(waitForSclHigh()){
		return true;
		}
	for(unsigned i=0;i<9;++i){
		_nanoDelayApi.wait(4000);
		bool	sdaValue	= _driverApi.sda();
		if(sdaValue){
			// This is what we want!
			return false;
			}
		_driverApi.sclLow();
		_nanoDelayApi.wait(4700+300);
		_driverApi.sclHigh();
		if(waitForSclHigh()){
			return true;
			}
		}
	// SDA is stuck LOW.
	return true;
	}

bool	Driver::sendStartCondition() noexcept{
	_driverApi.sdaLow();
	_nanoDelayApi.wait(4000+300);
	return false;
	}

bool	Driver::sendWriteStopCondition() noexcept{
	_driverApi.sdaHigh();
	_nanoDelayApi.wait(4700+1000);
	return false;
	}

bool	Driver::sendReadStopCondition() noexcept{
	_driverApi.sclLow();
	_driverApi.sdaLow();
	_nanoDelayApi.wait(4700+300);
	_driverApi.sclHigh();
	if(waitForSclHigh()){
		return true;
		}
	_nanoDelayApi.wait(4000);
	_driverApi.sdaHigh();
	_nanoDelayApi.wait(4700+1000);
	return false;
	}

bool	Driver::sendBit(bool value) noexcept{
	_driverApi.sclLow();
	_nanoDelayApi.wait(300);
	if(value){
		_driverApi.sdaHigh();
		}
	else {
		_driverApi.sdaLow();
		}
	_nanoDelayApi.wait(4700+300);
	_driverApi.sclHigh();
	if(waitForSclHigh()){
		return true;
		}
	_nanoDelayApi.wait(4000);
	return false;
	}

bool	Driver::readBit(bool& value) noexcept{
	_driverApi.sclLow();
	_nanoDelayApi.wait(300);
	_driverApi.sdaHigh();	// float the bus if it's not already
	_nanoDelayApi.wait(4700);
	_driverApi.sclHigh();
	if(waitForSclHigh()){
		return true;
		}
	_nanoDelayApi.wait(4000);
	value	= _driverApi.sda();
	return false;
	}

bool	Driver::writeAck() noexcept{
	return sendBit(false);
	}

bool	Driver::readAck(bool& ack) noexcept{
	_driverApi.sclLow();
	_nanoDelayApi.wait(300);
	_driverApi.sdaHigh();
	_nanoDelayApi.wait(4700+300);
	_driverApi.sclHigh();
	if(waitForSclHigh()){
		return true;
		}
	_nanoDelayApi.wait(4000);
	ack	= _driverApi.sda();
	return false;
	}

bool	Driver::waitForSclHigh() noexcept{
	bool	sclValue;
	// FIXME: hardcoded number
	// This will wait at *least* 1 second for SCL to go HIGH.
	// It will wait at *least* 100 times the minimum wait period
	// which could mean 100 seconds if the minimum wait period
	// is one second.
	for(unsigned i=0;i<100;++i){
		sclValue=_driverApi.scl();
		if(sclValue){
			return false;
			}
		// This delay is chosen to balance maximum wait time
		// reasonably fast response time.
		_nanoDelayApi.wait(10000);
		}
	return true;
	}

