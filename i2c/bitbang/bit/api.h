#ifndef _oscl_i2c_bitbang_bit_apih_
#define _oscl_i2c_bitbang_bit_apih_

namespace Oscl {
namespace I2C {
namespace BitBang {
namespace Bit {

/** This class describes the interface to the bit level
	protocol of the bit banged I2C bus. All operations
	except for resetBus() expect both SCL and SDA to
	be in the HIGH (floating) condition.
	All operations except sendStartCondition() leave the
	SDA in the HIGH (floating) condition.
	All operations leave SCL in the HIGH condition.
	Note however, that a slave device may still hold
	the bus in a LOW condition.
 */
class Api {
	public:
		/** Reset the bus. Executes a sequence that
			ensures that sane slave devices are no longer
			driving SDA and/or SCL LOW. This operation should
			*always* be executed upon power-on.
			Returns true on failure (SCL or SDA stuck LOW condition).
		 */
		virtual bool	resetBus() noexcept=0;

		/** Asserts SDA LOW while SCL is HIGH, indicating the start
			of a new I2C transaction. On exit, SDA is LOW and SCL
			is HIGH.
			Returns true on failure.
		 */
		virtual bool	sendStartCondition() noexcept=0;

		/** This operation is to be invoked after an octet is
			written to the I2C bus by the master to terminate
			the I2C transaction. Note that the address/control
			octet at the beginning of a transaction is considered
			as a write, even if the read/write bit of the control
			is a read. Normally, however, this operation is only
			called at the end of a write transaction.
			Returns true on failure.
		 */
		virtual bool	sendWriteStopCondition() noexcept=0;

		/** This operation is to be invoked after an octet is
			read from the I2C bus by the master to terminate
			the I2C transaction.
			This operation is only called at the end of an I2C
			read transaction.
			Returns true on failure.
		 */
		virtual bool	sendReadStopCondition() noexcept=0;

		/** Send the bit value.
			Return true if SCL is stuck LOW.
		 */
		virtual bool	sendBit(bool value) noexcept=0;
		/** Read the next bit value.
			Return true if SCL is stuck LOW.
		 */
		virtual bool	readBit(bool& value) noexcept=0;
		/** Perform the ACK sequence for a read transaction.
			Returns true if SCL is stuck LOW.
		 */
		virtual bool	writeAck() noexcept=0;
		/** Perform the ACK sequence for a write transaction
			and return the value of the ACK bit to the
			variable referenced by the ack argument.
			Returns true if SCL is stuck LOW.
		 */
		virtual bool	readAck(bool& ack) noexcept=0;
	};


}
}
}
}

#endif

