#ifndef _oscl_i2c_bitbang_bit_driverh_
#define _oscl_i2c_bitbang_bit_driverh_

#include "oscl/delay/nano.h"
#include "oscl/i2c/bitbang/driver/api.h"
#include "api.h"

namespace Oscl {
namespace I2C {
namespace BitBang {
namespace Bit {

class Driver : public Oscl::I2C::BitBang::Bit::Api {
	private:
		Oscl::I2C::BitBang::Driver::Api&	_driverApi;
		Oscl::Delay::Nano::Api&				_nanoDelayApi;
	public:
		Driver(	Oscl::I2C::BitBang::Driver::Api&	driverApi,
				Oscl::Delay::Nano::Api&				nanoDelayApi
				) noexcept;
		// Resets the bus. Returns true on failure.
		bool	resetBus() noexcept;
		bool	sendStartCondition() noexcept;
		bool	sendWriteStopCondition() noexcept;
		bool	sendReadStopCondition() noexcept;
		bool	sendBit(bool value) noexcept;
		bool	readBit(bool& value) noexcept;
		bool	writeAck() noexcept;
		bool	readAck(bool& ack) noexcept;
	private:
		bool	waitForSclHigh() noexcept;
	};

}
}
}
}

#endif

