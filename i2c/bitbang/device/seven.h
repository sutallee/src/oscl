#ifndef _oscl_i2c_bitbang_device_sevenh_
#define _oscl_i2c_bitbang_device_sevenh_

#include "oscl/i2c/device/api.h"
#include "oscl/i2c/bitbang/driver/api.h"
#include "oscl/i2c/bitbang/bit/api.h"
#include "oscl/i2c/bitbang/byte/driver.h"

namespace Oscl {
namespace I2C {
namespace BitBang {
namespace Device {

class SevenBitAddress : public Oscl::I2C::Device::Api {
	private:
		Oscl::I2C::BitBang::Byte::Driver	_byteDriver;
		const uint8_t						_address;

	public:
		SevenBitAddress(	Oscl::I2C::BitBang::Bit::Api&	bitApi,
							uint8_t							address
							) noexcept;

	private:
		bool	resetBus() noexcept;

		bool	write(	const void*	data,
						unsigned	nDataBytes
						) noexcept;

		bool	read(	void*		dataBuffer,
						unsigned	nDataBytesToRead
						) noexcept;
	private:
		bool	writeByte(uint8_t value) noexcept;
		void	readByte(uint8_t value) noexcept;
	};


}
}
}
}

#endif

