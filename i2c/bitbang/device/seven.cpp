#include "seven.h"

using namespace Oscl::I2C::BitBang::Device;

SevenBitAddress::SevenBitAddress(	Oscl::I2C::BitBang::Bit::Api&	bitApi,
									uint8_t							address
									) noexcept:
		_byteDriver(bitApi),
		_address(address << 1)
		{
	}

bool	SevenBitAddress::resetBus() noexcept{
	return _byteDriver.getBitApi().resetBus();
	}

bool	SevenBitAddress::write(	const void*	data,
								unsigned	nDataBytes
								) noexcept{
	uint8_t	control	= _address | 0x00;	// Set R/W to write.
	_byteDriver.getBitApi().sendStartCondition();
	if(_byteDriver.write(control)){
		// no ack!
		_byteDriver.getBitApi().sendWriteStopCondition();
		return true;
		}

	const uint8_t*	p	= (const uint8_t*)data;
	for(unsigned i=0;i<nDataBytes;++i){
		if(_byteDriver.write(p[i])){
			// no ack!
			_byteDriver.getBitApi().sendWriteStopCondition();
			return true;
			}
		}
	_byteDriver.getBitApi().sendWriteStopCondition();
	return false;
	}

bool	SevenBitAddress::read(	void*		dataBuffer,
								unsigned	nDataBytesToRead
								) noexcept{
	uint8_t	control	= _address | 0x01;	// Set R/W to read.
	_byteDriver.getBitApi().sendStartCondition();
	if(_byteDriver.write(control)){
		// no ack!
		_byteDriver.getBitApi().sendWriteStopCondition();
		return true;
		}

	uint8_t*	p	= (uint8_t*)dataBuffer;
	for(unsigned i=0;i<nDataBytesToRead;++i){
		if( _byteDriver.read(p[i])){
			_byteDriver.getBitApi().sendReadStopCondition();
			return true;
			}
		}
	_byteDriver.getBitApi().sendReadStopCondition();
	return false;
	}

