#ifndef _oscl_i2c_bitbang_driver_apih_
#define _oscl_i2c_bitbang_driver_apih_

/** */
namespace Oscl {
/** */
namespace I2C {
/** */
namespace BitBang {
/** */
namespace Driver {

/** This class describes the interface for a low level
	device driver that is to be used by the Bit-Banged
	I2C device driver infrastructure. The implementation
	of this interface maps I2C driver to the specific
	target hardware implementation.
 */
class Api {
	public:
		/** Set the open-drain/collector I2C SCL line
			to the HIGH (floating) state.
		 */
		virtual void	sclHigh() noexcept=0;

		/** Set the open-drain/collector I2C SCL line
			to the LOW state.
		 */
		virtual void	sclLow() noexcept=0;

		/** Set the open-drain/collector I2C SDA line
			to the HIGH (floating) state.
		 */
		virtual void	sdaHigh() noexcept=0;

		/** Set the open-drain/collector I2C SDA line
			to the LOW state.
		 */
		virtual void	sdaLow() noexcept=0;

		/** Returns the current state of the I2C SDA
			line. A true value indicates that the I2C
			SDA line is HIGH, and a false return value
			indicates that the SDA line is LOW. The returned
			value MUST indicate the actual state of the
			bus, and NOT the last value written by
			the device driver (e.g. shadow).
		 */
		virtual bool	sda() noexcept=0;

		/** Returns the current state of the I2C SCL
			line. A true value indicates that the I2C
			SCL line is HIGH, and a false return value
			indicates that the SCL line is LOW. The returned
			value MUST indicate the actual state of the
			bus, and NOT the last value written by
			the device driver (e.g. shadow).
			Note that some (incorrect) implementations of
			the I2C bus do not allow for the SCL clock
			line to be read. For such hardware implementations,
			this function must always return true.
		 */
		virtual bool	scl() noexcept=0;
	};

}
}
}
}

#endif

