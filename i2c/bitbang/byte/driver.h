#ifndef _oscl_i2c_bitbang_byte_driverh_
#define _oscl_i2c_bitbang_byte_driverh_

#include <stdint.h>
#include "oscl/delay/nano.h"
#include "oscl/i2c/bitbang/driver/api.h"
#include "oscl/i2c/bitbang/bit/api.h"
#include "oscl/i2c/bitbang/byte/driver.h"

namespace Oscl {
namespace I2C {
namespace BitBang {
namespace Byte {

class Driver {
	private:
		Oscl::I2C::BitBang::Bit::Api&	_bitApi;

	public:
		Driver(Oscl::I2C::BitBang::Bit::Api& bitApi) noexcept;

		inline Oscl::I2C::BitBang::Bit::Api& getBitApi() noexcept {
			return _bitApi;
			}

		bool	write(uint8_t value) noexcept;
		bool	read(uint8_t& value) noexcept;
	};

}
}
}
}

#endif

