#include "driver.h"

using namespace Oscl::I2C::BitBang::Byte;

Driver::Driver(Oscl::I2C::BitBang::Bit::Api& bitApi) noexcept:
		_bitApi(bitApi)
		{
	}

bool	Driver::write(uint8_t value) noexcept{
	for(unsigned i=0;i<8;++i){
		_bitApi.sendBit(value & (0x80>>i));
		}
	bool	ack;
	if(_bitApi.readAck(ack)){
		return true;
		}
	return ack;
	}

bool	Driver::read(uint8_t& value) noexcept{
	uint8_t	v	= 0;
	for(unsigned i=0;i<8;++i){
		v	<<= 1;
		bool	bitValue;
		if(_bitApi.readBit(bitValue)){
			// SCL stuck LOW
			return true;
			}
		if(bitValue){
			v	|= 1;
			}
		}
	value	= v;
	return _bitApi.writeAck();
	}

