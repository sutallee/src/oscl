/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_i2c_device_sevenbit_adapterh_
#define _oscl_i2c_device_sevenbit_adapterh_

#include <stdint.h>
#include "oscl/i2c/device/api.h"
#include "oscl/i2c/master/sevenbit/api.h"

/** */
namespace Oscl {
/** */
namespace I2C {
/** */
namespace Device {
/** */
namespace SevenBit {

/** This class implements the I2C device API on
	an I2C master.
 */
class Adapter: public Oscl::I2C::Device::Api {
	private:
		/** */
		Oscl::I2C::Master::SevenBit::Api&	_api;

		/** */
		const uint8_t						_deviceAddress;

	public:
		/** */
		Adapter(
			Oscl::I2C::Master::SevenBit::Api&	api,
			uint8_t								deviceAddress
			) noexcept;

	public:
		/** FIXME */
		bool	resetBus() noexcept;

		/** */
		bool	write(
					const void*	data,
					unsigned	nDataBytes
					) noexcept;

		/** */
		bool	read(
					void*		dataBuffer,
					unsigned	nDataBytesToRead
					) noexcept;
	};

}
}
}
}

#endif
