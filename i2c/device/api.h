/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_i2c_device_apih_
#define _oscl_i2c_device_apih_

/** */
namespace Oscl {
/** */
namespace I2C {
/** */
namespace Device {

/** This class describes the generic interface for a particular
	device that resides on an I2C bus.
 */
class Api {
	public:
		/** The client may reset the I2C bus using this operation.
			Returns true if the I2C bus is stuck and false if
			successful.
		 */
		virtual bool	resetBus() noexcept=0;

		/** This operation writes the octets referenced by the
			data argument. The number of contiguous octets to be
			written are specified by the nDataBytes argument.
			Returns false when successful, or true if any
			error is encountered.
		 */
		virtual bool	write(	const void*	data,
								unsigned	nDataBytes
								) noexcept=0;

		/** This operation reads the number of octets specified
			by the nDataBytesToRead argument into the buffer
			specified by the dataBuffer argument.
			Returns false when successful, or true if any
			error is encountered.
		 */
		virtual bool	read(	void*		dataBuffer,
								unsigned	nDataBytesToRead
								) noexcept=0;
	};

}
}
}

#endif
