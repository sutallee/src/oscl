/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

module tundra_qspan {
include "oscl/compiler/types.h";
namespace Oscl {
namespace Tundra {
namespace Qspan {
	register ConfigSpaceId uint32_t {
		field DID 16 16 {
			value Max 0xFFFF
			}
		field VID 0 16 {
			value Max 0xFFFF
			}
		}
	register ConfigSpaceControlStatus uint32_t {
		field D_PE 31 1 {
			value Clear	1
			value Error 1
			value NoError 0
			}
		field S_SERR 30 1 {
			value Clear	1
			value Error 1
			value NoError 0
			}
		field R_MA 29 1 {
			value Clear	1
			value Abort 1
			value NoAbort 0
			}
		field R_TA 28 1 {
			value Clear	1
			value Abort 1
			value NoAbort 0
			}
		field S_TA 27 1 {
			value Clear	1
			value Abort 1
			value NoAbort 0
			}
		field DEVSEL 25 2 {
			value Max 3
			value Min 0
			}
		field DP_D 24 1 {
			value Clear	1
			value Error 1
			value NoError 0
			}
		field TFBBC 23 1 {
			value Capable 1
			value Incapable 0
			}
		field UDFS 22 1 {
			value Supported 1
			value Unsupported 0
			}
		field DEV66 21 1 {
			value Capable 1
			value Incapable 0
			}
		field MFFBC 9 1 {
			value Capable 1
			value Incapable 0
			}
		field SERR_EN 8 1 {
			value Enable 1
			value Disable 0
			}
		field WAIT 7 1 {
			value Supported 1
			value Unsupported 0
			}
		field PERESP 6 1 {
			value Enable 1
			value Disable 0
			}
		field VGAPS 5 1 {
			value Enable 1
			value Disable 0
			}
		field MWI_EN 4 1 {
			value Enable 1
			value Disable 0
			}
		field SC 3 1 {
			value Enable 1
			value Disable 0
			}
		field BM 2 1 {
			value Enable 1
			value Disable 0
			}
		field MS 1 1 {
			value Enable 1
			value Disable 0
			}
		field IOS 0 1 {
			value Enable 1
			value Disable 0
			}
		}
	register ConfigClass uint32_t {
		}
	register ConfigMisc0 uint32_t {
		}
	register ConfigBaseAddrForMemory uint32_t {
		}
	register ConfigBaseAddrForTarget0 uint32_t {
		}
	register ConfigBaseAddrForTarget1 uint32_t {
		}
	register ConfigSubsystemId uint32_t {
		}
	register ConfigExpansionRomBaseAddr uint32_t {
		}
	register ConfigMisc1 uint32_t {
		}
	register BusTargetImage0Control uint32_t {
		field EN 31 1 {
			// Image Enable
			value Enable	1
			value Disable	0
			}
		field BS 24 4 {
			// Block Size
			value Size64K	0x00
			value Size128K	0x01
			value Size256K	0x02
			value Size512K	0x03
			value Size1M	0x04
			value Size2M	0x05
			value Size4M	0x06
			value Size8M	0x07
			value Size16M	0x08
			value Size32M	0x09
			value Size64M	0x0A
			value Size128M	0x0B
			value Size256M	0x0C
			value Size512M	0x0D
			value Size1G	0x0E
			value Size2G	0x0F
			}
		field PREN 23 1 {
			// Prefetch Read Enable
			value Enable	1
			value Disable	0
			}
		field BRSTWREN 22 1 {
			// Burst Write Enable
			value Enable	1
			value Disable	0
			}
		field INVEND 19 1 {
			// Invert Endian-ness from QB_BOC Setting in MISC_CTL
			value UseQB_BOC		0
			value InvertQB_BOC	1
			}
		field TC 12 4 {
			// QBus Transaction Code
			value Default	0
			}
		field DSIZE 10 2 {
			// QBus Destination Port Size
			value Size32Bit	0
			value Size16Bit	2
			value Size8Bit	1
			}
		field PWEN 7 1 {
			// Posted Write Enable
			value Enable	1
			value Disable	0
			}
		field PAS 6 1 {
			// PCI Bus Address Space
			value MemorySpace	0
			value IoSpace		1
			}
		}
	register BusTargetImage0Address uint32_t {
		field BA 16 16 {
			}
		field TA 0 16 {
			}
		}
	register BusTargetImage1Control uint32_t {
		}
	register BusTargetImage1Address uint32_t {
		}
	register BusExpansionRomControl uint32_t {
		}
	register BusErrorControlStatus uint32_t {
		}
	register BusAddressErrorLog uint32_t {
		}
	register BusDataErrorLog uint32_t {
		}
	register IdmaControlStatus uint32_t {
		}
	register IdmaAddress uint32_t {
		}
	register IdmaTransferCount uint32_t {
		}
	register ConfigAddress uint32_t {
		field BUS_NUM 16 8 {
			value LastBus 255
			}
		field DEV_NUM 11 4 {
			value LastDev 15
			}
		field FUNC_NUM 8 3 {
			value LastFunc 7
			}
		field REG_NUM 2 6 {
			value Last 63
			}
		field TYPE 0 1 {
			value Type0 0
			value Type1 1
			}
		}
	register ConfigData uint32_t {
		}
	register IackCycleGenerator uint32_t {
		}
	register InterruptStatus uint32_t {
		field PEL 31 1 {
			value NotPending	0
			value Pending		1
			value Clear			1
			}
		field QEL 30 1 {
			value NotPending	0
			value Pending		1
			value Clear			1
			}
		field DPD 29 1 {
			value NotPending	0
			value Pending		1
			value Clear			1
			}
		field IQE 27 1 {
			value NotPending	0
			value Pending		1
			value Clear			1
			}
		field IPE 26 1 {
			value NotPending	0
			value Pending		1
			value Clear			1
			}
		field IRST 25 1 {
			value NotPending	0
			value Pending		1
			value Clear			1
			}
		field DONE 24 1 {
			value NotPending	0
			value Pending		1
			value Clear			1
			}
		field INT 23 1 {
			value NotPending	0
			value Pending		1
			value Clear			1
			}
		field PERR 22 1 {
			value NotPending	0
			value Pending		1
			value Clear			1
			}
		field SERR 21 0 {
			value NotPending	0
			value Pending		1
			value Clear			1
			}
		field QINT 20 1 {
			value NotPending	0
			value Pending		1
			value Clear			1
			}
		field SI3 3 1 {
			value NotPending	0
			value Pending		1
			value Clear			1
			}
		field SI2 2 1 {
			value NotPending	0
			value Pending		1
			value Clear			1
			}
		field SI1 1 1 {
			value NotPending	0
			value Pending		1
			value Clear			1
			}
		field SI0 0 1 {
			value NotPending	0
			value Pending		1
			value Clear			1
			}
		}
	register InterruptControl uint32_t {
		field PEL 31 1 {
			value Enable	1
			value Disable	0
			}
		field QEL 30 1 {
			value Enable	1
			value Disable	0
			}
		field DPD 29 1 {
			value Enable	1
			value Disable	0
			}
		field IQE 27 1 {
			value Enable	1
			value Disable	0
			}
		field IPE 26 1 {
			value Enable	1
			value Disable	0
			}
		field IRST 25 1 {
			value Enable	1
			value Disable	0
			}
		field DONE 24 1 {
			value Enable	1
			value Disable	0
			}
		field INT 23 1 {
			value Enable	1
			value Disable	0
			}
		field PERR 22 1 {
			value Enable	1
			value Disable	0
			}
		field SERR 21 1 {
			value Enable	1
			value Disable	0
			}
		field QINT 20 1 {
			value Enable	1
			value Disable	0
			}
		field SI1 1 1 {
			value Enable	1
			value Disable	0
			}
		field SI0 0 1 {
			value Enable	1
			value Disable	0
			}
		}
	register InterruptDirectionControl uint32_t {
		field PEL 31 1 {
			value ToQBus	0
			value ToPciBus	1
			}
		field QEL 30 1 {
			value ToQBus	0
			value ToPciBus	1
			}
		field DPD 29 1 {
			value ToQBus	0
			value ToPciBus	1
			}
		field IQE 27 1 {
			value ToQBus	0
			value ToPciBus	1
			}
		field IPE 26 1 {
			value ToQBus	0
			value ToPciBus	1
			}
		field IRST 25 1 {
			value ToQBus	0
			value ToPciBus	1
			}
		field DONE 24 1 {
			value ToQBus	0
			value ToPciBus	1
			}
		field SI3 3 1 {
			value ToQBus	0
			value ToPciBus	1
			}
		field SI2 2 1 {
			value ToQBus	0
			value ToPciBus	1
			}
		field SI1 1 1 {
			value ToQBus	0
			value ToPciBus	1
			}
		field SI0 0 1 {
			value ToQBus	0
			value ToPciBus	1
			}
		}
	register InterruptControl2 uint32_t {
		}
	register MiscControlStatus uint32_t {
		field SW_RST 31 1 {
			// QBus Software Reset Control
			value Negated	0
			value Asserted	1
			value Negate	0
			value Assert	1
			}
		field S_BG 19 1 {
			// Synchronous Burst Grant
			value Asynchronous	0
			value Synchronous	1
			}
		field S_BB 18 1 {
			// Synhcronous Bus Grant Acknowledge
			value Asynchronous	0
			value Synchronous	1
			}
		field QB_BOC 16 1 {
			// QBus int8_t Ordering Control
			value BigEndian		0
			value LittleEndian	1
			}
		field MA_BE_D 12 1 {
			// Master Abort - Bus Error mapping disable
			value BusErrorOnAbort	0
			value Pci2_1			1
			}
		field PRCNT 2 6 {
			// Prefetch Read int8_t Count (Really DWORD count)
			// Actual number is 4 times this number
			value Maximum	0x03F
			}
		field MSTSLV 0 2 {
			// Master/Slave Mode
			value QuiccMasterQuiccM68040Slave			0
			value QuiccMasterQuiccPowerQuiccSlave		1
			value M68040MasterQuiccM68040Slave			2
			value PowerQuiccMasterQuiccPowerQuiccSlave	3
			}
		}
	register EepromControlStatus uint32_t {
		}
	register QBusSlaveImage0Control uint32_t {
		field PWEN 31 1 {
			value Disable	0
			value Enable	0
			}
		field PAS 24 1 {
			value Memory	0
			value IO		1
			}
		}
	register QBusSlaveImage0AddressTranslation uint32_t {
		field TA 16 16 {
			}
		field BS 4 4 {
			value Size64K	0x00
			value Size128K	0x01
			value Size256K	0x02
			value Size512K	0x03
			value Size1M	0x04
			value Size2M	0x05
			value Size4M	0x06
			value Size8M	0x07
			value Size16M	0x08
			value Size32M	0x09
			value Size64M	0x0A
			value Size128M	0x0B
			value Size256M	0x0C
			value Size512M	0x0D
			value Size1G	0x0E
			value Size2G	0x0F
			}
		field EN 0 1 {
			value Enable	1
			value Disable	0
			}
		}
	register QBusSlaveImage1Control uint32_t {
		}
	register QBusSlaveImage1AddressTranslation uint32_t {
		}
	register QBusErrorLogControlStatus uint32_t {
		}
	register QBusAddressErrorLog uint32_t {
		}
	register QBusDataErrorLog uint32_t {
		}
	}
	}
	}
	}

