/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _tundra_qspan_
#define _tundra_qspan_
#include <stdint.h>
namespace Oscl { // Namespace description
	namespace Tundra { // Namespace description
		namespace Qspan { // Namespace description
			namespace ConfigSpaceId { // Register description
				typedef uint32_t	Reg;
				namespace DID { // Field Description
					enum {Lsb = 16};
					enum {FieldMask = 0xFFFF0000};
					enum {Value_Max = 0xFFFF};
					enum {ValueMask_Max = 0xFFFF0000};
					};
				namespace VID { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x0000FFFF};
					enum {Value_Max = 0xFFFF};
					enum {ValueMask_Max = 0x0000FFFF};
					};
				};
			namespace ConfigSpaceControlStatus { // Register description
				typedef uint32_t	Reg;
				namespace D_PE { // Field Description
					enum {Lsb = 31};
					enum {FieldMask = 0x80000000};
					enum {Value_Clear = 0x1};
					enum {ValueMask_Clear = 0x80000000};
					enum {Value_Error = 0x1};
					enum {ValueMask_Error = 0x80000000};
					enum {Value_NoError = 0x0};
					enum {ValueMask_NoError = 0x00000000};
					};
				namespace S_SERR { // Field Description
					enum {Lsb = 30};
					enum {FieldMask = 0x40000000};
					enum {Value_Clear = 0x1};
					enum {ValueMask_Clear = 0x40000000};
					enum {Value_Error = 0x1};
					enum {ValueMask_Error = 0x40000000};
					enum {Value_NoError = 0x0};
					enum {ValueMask_NoError = 0x00000000};
					};
				namespace R_MA { // Field Description
					enum {Lsb = 29};
					enum {FieldMask = 0x20000000};
					enum {Value_Clear = 0x1};
					enum {ValueMask_Clear = 0x20000000};
					enum {Value_Abort = 0x1};
					enum {ValueMask_Abort = 0x20000000};
					enum {Value_NoAbort = 0x0};
					enum {ValueMask_NoAbort = 0x00000000};
					};
				namespace R_TA { // Field Description
					enum {Lsb = 28};
					enum {FieldMask = 0x10000000};
					enum {Value_Clear = 0x1};
					enum {ValueMask_Clear = 0x10000000};
					enum {Value_Abort = 0x1};
					enum {ValueMask_Abort = 0x10000000};
					enum {Value_NoAbort = 0x0};
					enum {ValueMask_NoAbort = 0x00000000};
					};
				namespace S_TA { // Field Description
					enum {Lsb = 27};
					enum {FieldMask = 0x08000000};
					enum {Value_Clear = 0x1};
					enum {ValueMask_Clear = 0x08000000};
					enum {Value_Abort = 0x1};
					enum {ValueMask_Abort = 0x08000000};
					enum {Value_NoAbort = 0x0};
					enum {ValueMask_NoAbort = 0x00000000};
					};
				namespace DEVSEL { // Field Description
					enum {Lsb = 25};
					enum {FieldMask = 0x06000000};
					enum {Value_Max = 0x3};
					enum {ValueMask_Max = 0x06000000};
					enum {Value_Min = 0x0};
					enum {ValueMask_Min = 0x00000000};
					};
				namespace DP_D { // Field Description
					enum {Lsb = 24};
					enum {FieldMask = 0x01000000};
					enum {Value_Clear = 0x1};
					enum {ValueMask_Clear = 0x01000000};
					enum {Value_Error = 0x1};
					enum {ValueMask_Error = 0x01000000};
					enum {Value_NoError = 0x0};
					enum {ValueMask_NoError = 0x00000000};
					};
				namespace TFBBC { // Field Description
					enum {Lsb = 23};
					enum {FieldMask = 0x00800000};
					enum {Value_Capable = 0x1};
					enum {ValueMask_Capable = 0x00800000};
					enum {Value_Incapable = 0x0};
					enum {ValueMask_Incapable = 0x00000000};
					};
				namespace UDFS { // Field Description
					enum {Lsb = 22};
					enum {FieldMask = 0x00400000};
					enum {Value_Supported = 0x1};
					enum {ValueMask_Supported = 0x00400000};
					enum {Value_Unsupported = 0x0};
					enum {ValueMask_Unsupported = 0x00000000};
					};
				namespace DEV66 { // Field Description
					enum {Lsb = 21};
					enum {FieldMask = 0x00200000};
					enum {Value_Capable = 0x1};
					enum {ValueMask_Capable = 0x00200000};
					enum {Value_Incapable = 0x0};
					enum {ValueMask_Incapable = 0x00000000};
					};
				namespace MFFBC { // Field Description
					enum {Lsb = 9};
					enum {FieldMask = 0x00000200};
					enum {Value_Capable = 0x1};
					enum {ValueMask_Capable = 0x00000200};
					enum {Value_Incapable = 0x0};
					enum {ValueMask_Incapable = 0x00000000};
					};
				namespace SERR_EN { // Field Description
					enum {Lsb = 8};
					enum {FieldMask = 0x00000100};
					enum {Value_Enable = 0x1};
					enum {ValueMask_Enable = 0x00000100};
					enum {Value_Disable = 0x0};
					enum {ValueMask_Disable = 0x00000000};
					};
				namespace WAIT { // Field Description
					enum {Lsb = 7};
					enum {FieldMask = 0x00000080};
					enum {Value_Supported = 0x1};
					enum {ValueMask_Supported = 0x00000080};
					enum {Value_Unsupported = 0x0};
					enum {ValueMask_Unsupported = 0x00000000};
					};
				namespace PERESP { // Field Description
					enum {Lsb = 6};
					enum {FieldMask = 0x00000040};
					enum {Value_Enable = 0x1};
					enum {ValueMask_Enable = 0x00000040};
					enum {Value_Disable = 0x0};
					enum {ValueMask_Disable = 0x00000000};
					};
				namespace VGAPS { // Field Description
					enum {Lsb = 5};
					enum {FieldMask = 0x00000020};
					enum {Value_Enable = 0x1};
					enum {ValueMask_Enable = 0x00000020};
					enum {Value_Disable = 0x0};
					enum {ValueMask_Disable = 0x00000000};
					};
				namespace MWI_EN { // Field Description
					enum {Lsb = 4};
					enum {FieldMask = 0x00000010};
					enum {Value_Enable = 0x1};
					enum {ValueMask_Enable = 0x00000010};
					enum {Value_Disable = 0x0};
					enum {ValueMask_Disable = 0x00000000};
					};
				namespace SC { // Field Description
					enum {Lsb = 3};
					enum {FieldMask = 0x00000008};
					enum {Value_Enable = 0x1};
					enum {ValueMask_Enable = 0x00000008};
					enum {Value_Disable = 0x0};
					enum {ValueMask_Disable = 0x00000000};
					};
				namespace BM { // Field Description
					enum {Lsb = 2};
					enum {FieldMask = 0x00000004};
					enum {Value_Enable = 0x1};
					enum {ValueMask_Enable = 0x00000004};
					enum {Value_Disable = 0x0};
					enum {ValueMask_Disable = 0x00000000};
					};
				namespace MS { // Field Description
					enum {Lsb = 1};
					enum {FieldMask = 0x00000002};
					enum {Value_Enable = 0x1};
					enum {ValueMask_Enable = 0x00000002};
					enum {Value_Disable = 0x0};
					enum {ValueMask_Disable = 0x00000000};
					};
				namespace IOS { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x00000001};
					enum {Value_Enable = 0x1};
					enum {ValueMask_Enable = 0x00000001};
					enum {Value_Disable = 0x0};
					enum {ValueMask_Disable = 0x00000000};
					};
				};
			namespace ConfigClass { // Register description
				typedef uint32_t	Reg;
				};
			namespace ConfigMisc0 { // Register description
				typedef uint32_t	Reg;
				};
			namespace ConfigBaseAddrForMemory { // Register description
				typedef uint32_t	Reg;
				};
			namespace ConfigBaseAddrForTarget0 { // Register description
				typedef uint32_t	Reg;
				};
			namespace ConfigBaseAddrForTarget1 { // Register description
				typedef uint32_t	Reg;
				};
			namespace ConfigSubsystemId { // Register description
				typedef uint32_t	Reg;
				};
			namespace ConfigExpansionRomBaseAddr { // Register description
				typedef uint32_t	Reg;
				};
			namespace ConfigMisc1 { // Register description
				typedef uint32_t	Reg;
				};
			namespace BusTargetImage0Control { // Register description
				typedef uint32_t	Reg;
				namespace EN { // Field Description
					enum {Lsb = 31};
					enum {FieldMask = 0x80000000};
					enum {Value_Enable = 0x1};
					enum {ValueMask_Enable = 0x80000000};
					enum {Value_Disable = 0x0};
					enum {ValueMask_Disable = 0x00000000};
					};
				namespace BS { // Field Description
					enum {Lsb = 24};
					enum {FieldMask = 0x0F000000};
					enum {Value_Size64K = 0x0};
					enum {ValueMask_Size64K = 0x00000000};
					enum {Value_Size128K = 0x1};
					enum {ValueMask_Size128K = 0x01000000};
					enum {Value_Size256K = 0x2};
					enum {ValueMask_Size256K = 0x02000000};
					enum {Value_Size512K = 0x3};
					enum {ValueMask_Size512K = 0x03000000};
					enum {Value_Size1M = 0x4};
					enum {ValueMask_Size1M = 0x04000000};
					enum {Value_Size2M = 0x5};
					enum {ValueMask_Size2M = 0x05000000};
					enum {Value_Size4M = 0x6};
					enum {ValueMask_Size4M = 0x06000000};
					enum {Value_Size8M = 0x7};
					enum {ValueMask_Size8M = 0x07000000};
					enum {Value_Size16M = 0x8};
					enum {ValueMask_Size16M = 0x08000000};
					enum {Value_Size32M = 0x9};
					enum {ValueMask_Size32M = 0x09000000};
					enum {Value_Size64M = 0xA};
					enum {ValueMask_Size64M = 0x0A000000};
					enum {Value_Size128M = 0xB};
					enum {ValueMask_Size128M = 0x0B000000};
					enum {Value_Size256M = 0xC};
					enum {ValueMask_Size256M = 0x0C000000};
					enum {Value_Size512M = 0xD};
					enum {ValueMask_Size512M = 0x0D000000};
					enum {Value_Size1G = 0xE};
					enum {ValueMask_Size1G = 0x0E000000};
					enum {Value_Size2G = 0xF};
					enum {ValueMask_Size2G = 0x0F000000};
					};
				namespace PREN { // Field Description
					enum {Lsb = 23};
					enum {FieldMask = 0x00800000};
					enum {Value_Enable = 0x1};
					enum {ValueMask_Enable = 0x00800000};
					enum {Value_Disable = 0x0};
					enum {ValueMask_Disable = 0x00000000};
					};
				namespace BRSTWREN { // Field Description
					enum {Lsb = 22};
					enum {FieldMask = 0x00400000};
					enum {Value_Enable = 0x1};
					enum {ValueMask_Enable = 0x00400000};
					enum {Value_Disable = 0x0};
					enum {ValueMask_Disable = 0x00000000};
					};
				namespace INVEND { // Field Description
					enum {Lsb = 19};
					enum {FieldMask = 0x00080000};
					enum {Value_UseQB_BOC = 0x0};
					enum {ValueMask_UseQB_BOC = 0x00000000};
					enum {Value_InvertQB_BOC = 0x1};
					enum {ValueMask_InvertQB_BOC = 0x00080000};
					};
				namespace TC { // Field Description
					enum {Lsb = 12};
					enum {FieldMask = 0x0000F000};
					enum {Value_Default = 0x0};
					enum {ValueMask_Default = 0x00000000};
					};
				namespace DSIZE { // Field Description
					enum {Lsb = 10};
					enum {FieldMask = 0x00000C00};
					enum {Value_Size32Bit = 0x0};
					enum {ValueMask_Size32Bit = 0x00000000};
					enum {Value_Size16Bit = 0x2};
					enum {ValueMask_Size16Bit = 0x00000800};
					enum {Value_Size8Bit = 0x1};
					enum {ValueMask_Size8Bit = 0x00000400};
					};
				namespace PWEN { // Field Description
					enum {Lsb = 7};
					enum {FieldMask = 0x00000080};
					enum {Value_Enable = 0x1};
					enum {ValueMask_Enable = 0x00000080};
					enum {Value_Disable = 0x0};
					enum {ValueMask_Disable = 0x00000000};
					};
				namespace PAS { // Field Description
					enum {Lsb = 6};
					enum {FieldMask = 0x00000040};
					enum {Value_MemorySpace = 0x0};
					enum {ValueMask_MemorySpace = 0x00000000};
					enum {Value_IoSpace = 0x1};
					enum {ValueMask_IoSpace = 0x00000040};
					};
				};
			namespace BusTargetImage0Address { // Register description
				typedef uint32_t	Reg;
				namespace BA { // Field Description
					enum {Lsb = 16};
					enum {FieldMask = 0xFFFF0000};
					};
				namespace TA { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x0000FFFF};
					};
				};
			namespace BusTargetImage1Control { // Register description
				typedef uint32_t	Reg;
				};
			namespace BusTargetImage1Address { // Register description
				typedef uint32_t	Reg;
				};
			namespace BusExpansionRomControl { // Register description
				typedef uint32_t	Reg;
				};
			namespace BusErrorControlStatus { // Register description
				typedef uint32_t	Reg;
				};
			namespace BusAddressErrorLog { // Register description
				typedef uint32_t	Reg;
				};
			namespace BusDataErrorLog { // Register description
				typedef uint32_t	Reg;
				};
			namespace IdmaControlStatus { // Register description
				typedef uint32_t	Reg;
				};
			namespace IdmaAddress { // Register description
				typedef uint32_t	Reg;
				};
			namespace IdmaTransferCount { // Register description
				typedef uint32_t	Reg;
				};
			namespace ConfigAddress { // Register description
				typedef uint32_t	Reg;
				namespace BUS_NUM { // Field Description
					enum {Lsb = 16};
					enum {FieldMask = 0x00FF0000};
					enum {Value_LastBus = 0xFF};
					enum {ValueMask_LastBus = 0x00FF0000};
					};
				namespace DEV_NUM { // Field Description
					enum {Lsb = 11};
					enum {FieldMask = 0x00007800};
					enum {Value_LastDev = 0xF};
					enum {ValueMask_LastDev = 0x00007800};
					};
				namespace FUNC_NUM { // Field Description
					enum {Lsb = 8};
					enum {FieldMask = 0x00000700};
					enum {Value_LastFunc = 0x7};
					enum {ValueMask_LastFunc = 0x00000700};
					};
				namespace REG_NUM { // Field Description
					enum {Lsb = 2};
					enum {FieldMask = 0x000000FC};
					enum {Value_Last = 0x3F};
					enum {ValueMask_Last = 0x000000FC};
					};
				namespace TYPE { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x00000001};
					enum {Value_Type0 = 0x0};
					enum {ValueMask_Type0 = 0x00000000};
					enum {Value_Type1 = 0x1};
					enum {ValueMask_Type1 = 0x00000001};
					};
				};
			namespace ConfigData { // Register description
				typedef uint32_t	Reg;
				};
			namespace IackCycleGenerator { // Register description
				typedef uint32_t	Reg;
				};
			namespace InterruptStatus { // Register description
				typedef uint32_t	Reg;
				namespace PEL { // Field Description
					enum {Lsb = 31};
					enum {FieldMask = 0x80000000};
					enum {Value_NotPending = 0x0};
					enum {ValueMask_NotPending = 0x00000000};
					enum {Value_Pending = 0x1};
					enum {ValueMask_Pending = 0x80000000};
					enum {Value_Clear = 0x1};
					enum {ValueMask_Clear = 0x80000000};
					};
				namespace QEL { // Field Description
					enum {Lsb = 30};
					enum {FieldMask = 0x40000000};
					enum {Value_NotPending = 0x0};
					enum {ValueMask_NotPending = 0x00000000};
					enum {Value_Pending = 0x1};
					enum {ValueMask_Pending = 0x40000000};
					enum {Value_Clear = 0x1};
					enum {ValueMask_Clear = 0x40000000};
					};
				namespace DPD { // Field Description
					enum {Lsb = 29};
					enum {FieldMask = 0x20000000};
					enum {Value_NotPending = 0x0};
					enum {ValueMask_NotPending = 0x00000000};
					enum {Value_Pending = 0x1};
					enum {ValueMask_Pending = 0x20000000};
					enum {Value_Clear = 0x1};
					enum {ValueMask_Clear = 0x20000000};
					};
				namespace IQE { // Field Description
					enum {Lsb = 27};
					enum {FieldMask = 0x08000000};
					enum {Value_NotPending = 0x0};
					enum {ValueMask_NotPending = 0x00000000};
					enum {Value_Pending = 0x1};
					enum {ValueMask_Pending = 0x08000000};
					enum {Value_Clear = 0x1};
					enum {ValueMask_Clear = 0x08000000};
					};
				namespace IPE { // Field Description
					enum {Lsb = 26};
					enum {FieldMask = 0x04000000};
					enum {Value_NotPending = 0x0};
					enum {ValueMask_NotPending = 0x00000000};
					enum {Value_Pending = 0x1};
					enum {ValueMask_Pending = 0x04000000};
					enum {Value_Clear = 0x1};
					enum {ValueMask_Clear = 0x04000000};
					};
				namespace IRST { // Field Description
					enum {Lsb = 25};
					enum {FieldMask = 0x02000000};
					enum {Value_NotPending = 0x0};
					enum {ValueMask_NotPending = 0x00000000};
					enum {Value_Pending = 0x1};
					enum {ValueMask_Pending = 0x02000000};
					enum {Value_Clear = 0x1};
					enum {ValueMask_Clear = 0x02000000};
					};
				namespace DONE { // Field Description
					enum {Lsb = 24};
					enum {FieldMask = 0x01000000};
					enum {Value_NotPending = 0x0};
					enum {ValueMask_NotPending = 0x00000000};
					enum {Value_Pending = 0x1};
					enum {ValueMask_Pending = 0x01000000};
					enum {Value_Clear = 0x1};
					enum {ValueMask_Clear = 0x01000000};
					};
				namespace INT { // Field Description
					enum {Lsb = 23};
					enum {FieldMask = 0x00800000};
					enum {Value_NotPending = 0x0};
					enum {ValueMask_NotPending = 0x00000000};
					enum {Value_Pending = 0x1};
					enum {ValueMask_Pending = 0x00800000};
					enum {Value_Clear = 0x1};
					enum {ValueMask_Clear = 0x00800000};
					};
				namespace PERR { // Field Description
					enum {Lsb = 22};
					enum {FieldMask = 0x00400000};
					enum {Value_NotPending = 0x0};
					enum {ValueMask_NotPending = 0x00000000};
					enum {Value_Pending = 0x1};
					enum {ValueMask_Pending = 0x00400000};
					enum {Value_Clear = 0x1};
					enum {ValueMask_Clear = 0x00400000};
					};
				namespace SERR { // Field Description
					enum {Lsb = 21};
					enum {FieldMask = 0x00000000};
					enum {Value_NotPending = 0x0};
					enum {ValueMask_NotPending = 0x00000000};
					enum {Value_Pending = 0x1};
					enum {ValueMask_Pending = 0x00200000};
					enum {Value_Clear = 0x1};
					enum {ValueMask_Clear = 0x00200000};
					};
				namespace QINT { // Field Description
					enum {Lsb = 20};
					enum {FieldMask = 0x00100000};
					enum {Value_NotPending = 0x0};
					enum {ValueMask_NotPending = 0x00000000};
					enum {Value_Pending = 0x1};
					enum {ValueMask_Pending = 0x00100000};
					enum {Value_Clear = 0x1};
					enum {ValueMask_Clear = 0x00100000};
					};
				namespace SI3 { // Field Description
					enum {Lsb = 3};
					enum {FieldMask = 0x00000008};
					enum {Value_NotPending = 0x0};
					enum {ValueMask_NotPending = 0x00000000};
					enum {Value_Pending = 0x1};
					enum {ValueMask_Pending = 0x00000008};
					enum {Value_Clear = 0x1};
					enum {ValueMask_Clear = 0x00000008};
					};
				namespace SI2 { // Field Description
					enum {Lsb = 2};
					enum {FieldMask = 0x00000004};
					enum {Value_NotPending = 0x0};
					enum {ValueMask_NotPending = 0x00000000};
					enum {Value_Pending = 0x1};
					enum {ValueMask_Pending = 0x00000004};
					enum {Value_Clear = 0x1};
					enum {ValueMask_Clear = 0x00000004};
					};
				namespace SI1 { // Field Description
					enum {Lsb = 1};
					enum {FieldMask = 0x00000002};
					enum {Value_NotPending = 0x0};
					enum {ValueMask_NotPending = 0x00000000};
					enum {Value_Pending = 0x1};
					enum {ValueMask_Pending = 0x00000002};
					enum {Value_Clear = 0x1};
					enum {ValueMask_Clear = 0x00000002};
					};
				namespace SI0 { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x00000001};
					enum {Value_NotPending = 0x0};
					enum {ValueMask_NotPending = 0x00000000};
					enum {Value_Pending = 0x1};
					enum {ValueMask_Pending = 0x00000001};
					enum {Value_Clear = 0x1};
					enum {ValueMask_Clear = 0x00000001};
					};
				};
			namespace InterruptControl { // Register description
				typedef uint32_t	Reg;
				namespace PEL { // Field Description
					enum {Lsb = 31};
					enum {FieldMask = 0x80000000};
					enum {Value_Enable = 0x1};
					enum {ValueMask_Enable = 0x80000000};
					enum {Value_Disable = 0x0};
					enum {ValueMask_Disable = 0x00000000};
					};
				namespace QEL { // Field Description
					enum {Lsb = 30};
					enum {FieldMask = 0x40000000};
					enum {Value_Enable = 0x1};
					enum {ValueMask_Enable = 0x40000000};
					enum {Value_Disable = 0x0};
					enum {ValueMask_Disable = 0x00000000};
					};
				namespace DPD { // Field Description
					enum {Lsb = 29};
					enum {FieldMask = 0x20000000};
					enum {Value_Enable = 0x1};
					enum {ValueMask_Enable = 0x20000000};
					enum {Value_Disable = 0x0};
					enum {ValueMask_Disable = 0x00000000};
					};
				namespace IQE { // Field Description
					enum {Lsb = 27};
					enum {FieldMask = 0x08000000};
					enum {Value_Enable = 0x1};
					enum {ValueMask_Enable = 0x08000000};
					enum {Value_Disable = 0x0};
					enum {ValueMask_Disable = 0x00000000};
					};
				namespace IPE { // Field Description
					enum {Lsb = 26};
					enum {FieldMask = 0x04000000};
					enum {Value_Enable = 0x1};
					enum {ValueMask_Enable = 0x04000000};
					enum {Value_Disable = 0x0};
					enum {ValueMask_Disable = 0x00000000};
					};
				namespace IRST { // Field Description
					enum {Lsb = 25};
					enum {FieldMask = 0x02000000};
					enum {Value_Enable = 0x1};
					enum {ValueMask_Enable = 0x02000000};
					enum {Value_Disable = 0x0};
					enum {ValueMask_Disable = 0x00000000};
					};
				namespace DONE { // Field Description
					enum {Lsb = 24};
					enum {FieldMask = 0x01000000};
					enum {Value_Enable = 0x1};
					enum {ValueMask_Enable = 0x01000000};
					enum {Value_Disable = 0x0};
					enum {ValueMask_Disable = 0x00000000};
					};
				namespace INT { // Field Description
					enum {Lsb = 23};
					enum {FieldMask = 0x00800000};
					enum {Value_Enable = 0x1};
					enum {ValueMask_Enable = 0x00800000};
					enum {Value_Disable = 0x0};
					enum {ValueMask_Disable = 0x00000000};
					};
				namespace PERR { // Field Description
					enum {Lsb = 22};
					enum {FieldMask = 0x00400000};
					enum {Value_Enable = 0x1};
					enum {ValueMask_Enable = 0x00400000};
					enum {Value_Disable = 0x0};
					enum {ValueMask_Disable = 0x00000000};
					};
				namespace SERR { // Field Description
					enum {Lsb = 21};
					enum {FieldMask = 0x00200000};
					enum {Value_Enable = 0x1};
					enum {ValueMask_Enable = 0x00200000};
					enum {Value_Disable = 0x0};
					enum {ValueMask_Disable = 0x00000000};
					};
				namespace QINT { // Field Description
					enum {Lsb = 20};
					enum {FieldMask = 0x00100000};
					enum {Value_Enable = 0x1};
					enum {ValueMask_Enable = 0x00100000};
					enum {Value_Disable = 0x0};
					enum {ValueMask_Disable = 0x00000000};
					};
				namespace SI1 { // Field Description
					enum {Lsb = 1};
					enum {FieldMask = 0x00000002};
					enum {Value_Enable = 0x1};
					enum {ValueMask_Enable = 0x00000002};
					enum {Value_Disable = 0x0};
					enum {ValueMask_Disable = 0x00000000};
					};
				namespace SI0 { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x00000001};
					enum {Value_Enable = 0x1};
					enum {ValueMask_Enable = 0x00000001};
					enum {Value_Disable = 0x0};
					enum {ValueMask_Disable = 0x00000000};
					};
				};
			namespace InterruptDirectionControl { // Register description
				typedef uint32_t	Reg;
				namespace PEL { // Field Description
					enum {Lsb = 31};
					enum {FieldMask = 0x80000000};
					enum {Value_ToQBus = 0x0};
					enum {ValueMask_ToQBus = 0x00000000};
					enum {Value_ToPciBus = 0x1};
					enum {ValueMask_ToPciBus = 0x80000000};
					};
				namespace QEL { // Field Description
					enum {Lsb = 30};
					enum {FieldMask = 0x40000000};
					enum {Value_ToQBus = 0x0};
					enum {ValueMask_ToQBus = 0x00000000};
					enum {Value_ToPciBus = 0x1};
					enum {ValueMask_ToPciBus = 0x40000000};
					};
				namespace DPD { // Field Description
					enum {Lsb = 29};
					enum {FieldMask = 0x20000000};
					enum {Value_ToQBus = 0x0};
					enum {ValueMask_ToQBus = 0x00000000};
					enum {Value_ToPciBus = 0x1};
					enum {ValueMask_ToPciBus = 0x20000000};
					};
				namespace IQE { // Field Description
					enum {Lsb = 27};
					enum {FieldMask = 0x08000000};
					enum {Value_ToQBus = 0x0};
					enum {ValueMask_ToQBus = 0x00000000};
					enum {Value_ToPciBus = 0x1};
					enum {ValueMask_ToPciBus = 0x08000000};
					};
				namespace IPE { // Field Description
					enum {Lsb = 26};
					enum {FieldMask = 0x04000000};
					enum {Value_ToQBus = 0x0};
					enum {ValueMask_ToQBus = 0x00000000};
					enum {Value_ToPciBus = 0x1};
					enum {ValueMask_ToPciBus = 0x04000000};
					};
				namespace IRST { // Field Description
					enum {Lsb = 25};
					enum {FieldMask = 0x02000000};
					enum {Value_ToQBus = 0x0};
					enum {ValueMask_ToQBus = 0x00000000};
					enum {Value_ToPciBus = 0x1};
					enum {ValueMask_ToPciBus = 0x02000000};
					};
				namespace DONE { // Field Description
					enum {Lsb = 24};
					enum {FieldMask = 0x01000000};
					enum {Value_ToQBus = 0x0};
					enum {ValueMask_ToQBus = 0x00000000};
					enum {Value_ToPciBus = 0x1};
					enum {ValueMask_ToPciBus = 0x01000000};
					};
				namespace SI3 { // Field Description
					enum {Lsb = 3};
					enum {FieldMask = 0x00000008};
					enum {Value_ToQBus = 0x0};
					enum {ValueMask_ToQBus = 0x00000000};
					enum {Value_ToPciBus = 0x1};
					enum {ValueMask_ToPciBus = 0x00000008};
					};
				namespace SI2 { // Field Description
					enum {Lsb = 2};
					enum {FieldMask = 0x00000004};
					enum {Value_ToQBus = 0x0};
					enum {ValueMask_ToQBus = 0x00000000};
					enum {Value_ToPciBus = 0x1};
					enum {ValueMask_ToPciBus = 0x00000004};
					};
				namespace SI1 { // Field Description
					enum {Lsb = 1};
					enum {FieldMask = 0x00000002};
					enum {Value_ToQBus = 0x0};
					enum {ValueMask_ToQBus = 0x00000000};
					enum {Value_ToPciBus = 0x1};
					enum {ValueMask_ToPciBus = 0x00000002};
					};
				namespace SI0 { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x00000001};
					enum {Value_ToQBus = 0x0};
					enum {ValueMask_ToQBus = 0x00000000};
					enum {Value_ToPciBus = 0x1};
					enum {ValueMask_ToPciBus = 0x00000001};
					};
				};
			namespace InterruptControl2 { // Register description
				typedef uint32_t	Reg;
				};
			namespace MiscControlStatus { // Register description
				typedef uint32_t	Reg;
				namespace SW_RST { // Field Description
					enum {Lsb = 31};
					enum {FieldMask = 0x80000000};
					enum {Value_Negated = 0x0};
					enum {ValueMask_Negated = 0x00000000};
					enum {Value_Asserted = 0x1};
					enum {ValueMask_Asserted = 0x80000000};
					enum {Value_Negate = 0x0};
					enum {ValueMask_Negate = 0x00000000};
					enum {Value_Assert = 0x1};
					enum {ValueMask_Assert = 0x80000000};
					};
				namespace S_BG { // Field Description
					enum {Lsb = 19};
					enum {FieldMask = 0x00080000};
					enum {Value_Asynchronous = 0x0};
					enum {ValueMask_Asynchronous = 0x00000000};
					enum {Value_Synchronous = 0x1};
					enum {ValueMask_Synchronous = 0x00080000};
					};
				namespace S_BB { // Field Description
					enum {Lsb = 18};
					enum {FieldMask = 0x00040000};
					enum {Value_Asynchronous = 0x0};
					enum {ValueMask_Asynchronous = 0x00000000};
					enum {Value_Synchronous = 0x1};
					enum {ValueMask_Synchronous = 0x00040000};
					};
				namespace QB_BOC { // Field Description
					enum {Lsb = 16};
					enum {FieldMask = 0x00010000};
					enum {Value_BigEndian = 0x0};
					enum {ValueMask_BigEndian = 0x00000000};
					enum {Value_LittleEndian = 0x1};
					enum {ValueMask_LittleEndian = 0x00010000};
					};
				namespace MA_BE_D { // Field Description
					enum {Lsb = 12};
					enum {FieldMask = 0x00001000};
					enum {Value_BusErrorOnAbort = 0x0};
					enum {ValueMask_BusErrorOnAbort = 0x00000000};
					enum {Value_Pci2_1 = 0x1};
					enum {ValueMask_Pci2_1 = 0x00001000};
					};
				namespace PRCNT { // Field Description
					enum {Lsb = 2};
					enum {FieldMask = 0x000000FC};
					enum {Value_Maximum = 0x3F};
					enum {ValueMask_Maximum = 0x000000FC};
					};
				namespace MSTSLV { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x00000003};
					enum {Value_QuiccMasterQuiccM68040Slave = 0x0};
					enum {ValueMask_QuiccMasterQuiccM68040Slave = 0x00000000};
					enum {Value_QuiccMasterQuiccPowerQuiccSlave = 0x1};
					enum {ValueMask_QuiccMasterQuiccPowerQuiccSlave = 0x00000001};
					enum {Value_M68040MasterQuiccM68040Slave = 0x2};
					enum {ValueMask_M68040MasterQuiccM68040Slave = 0x00000002};
					enum {Value_PowerQuiccMasterQuiccPowerQuiccSlave = 0x3};
					enum {ValueMask_PowerQuiccMasterQuiccPowerQuiccSlave = 0x00000003};
					};
				};
			namespace EepromControlStatus { // Register description
				typedef uint32_t	Reg;
				};
			namespace QBusSlaveImage0Control { // Register description
				typedef uint32_t	Reg;
				namespace PWEN { // Field Description
					enum {Lsb = 31};
					enum {FieldMask = 0x80000000};
					enum {Value_Disable = 0x0};
					enum {ValueMask_Disable = 0x00000000};
					enum {Value_Enable = 0x0};
					enum {ValueMask_Enable = 0x00000000};
					};
				namespace PAS { // Field Description
					enum {Lsb = 24};
					enum {FieldMask = 0x01000000};
					enum {Value_Memory = 0x0};
					enum {ValueMask_Memory = 0x00000000};
					enum {Value_IO = 0x1};
					enum {ValueMask_IO = 0x01000000};
					};
				};
			namespace QBusSlaveImage0AddressTranslation { // Register description
				typedef uint32_t	Reg;
				namespace TA { // Field Description
					enum {Lsb = 16};
					enum {FieldMask = 0xFFFF0000};
					};
				namespace BS { // Field Description
					enum {Lsb = 4};
					enum {FieldMask = 0x000000F0};
					enum {Value_Size64K = 0x0};
					enum {ValueMask_Size64K = 0x00000000};
					enum {Value_Size128K = 0x1};
					enum {ValueMask_Size128K = 0x00000010};
					enum {Value_Size256K = 0x2};
					enum {ValueMask_Size256K = 0x00000020};
					enum {Value_Size512K = 0x3};
					enum {ValueMask_Size512K = 0x00000030};
					enum {Value_Size1M = 0x4};
					enum {ValueMask_Size1M = 0x00000040};
					enum {Value_Size2M = 0x5};
					enum {ValueMask_Size2M = 0x00000050};
					enum {Value_Size4M = 0x6};
					enum {ValueMask_Size4M = 0x00000060};
					enum {Value_Size8M = 0x7};
					enum {ValueMask_Size8M = 0x00000070};
					enum {Value_Size16M = 0x8};
					enum {ValueMask_Size16M = 0x00000080};
					enum {Value_Size32M = 0x9};
					enum {ValueMask_Size32M = 0x00000090};
					enum {Value_Size64M = 0xA};
					enum {ValueMask_Size64M = 0x000000A0};
					enum {Value_Size128M = 0xB};
					enum {ValueMask_Size128M = 0x000000B0};
					enum {Value_Size256M = 0xC};
					enum {ValueMask_Size256M = 0x000000C0};
					enum {Value_Size512M = 0xD};
					enum {ValueMask_Size512M = 0x000000D0};
					enum {Value_Size1G = 0xE};
					enum {ValueMask_Size1G = 0x000000E0};
					enum {Value_Size2G = 0xF};
					enum {ValueMask_Size2G = 0x000000F0};
					};
				namespace EN { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x00000001};
					enum {Value_Enable = 0x1};
					enum {ValueMask_Enable = 0x00000001};
					enum {Value_Disable = 0x0};
					enum {ValueMask_Disable = 0x00000000};
					};
				};
			namespace QBusSlaveImage1Control { // Register description
				typedef uint32_t	Reg;
				};
			namespace QBusSlaveImage1AddressTranslation { // Register description
				typedef uint32_t	Reg;
				};
			namespace QBusErrorLogControlStatus { // Register description
				typedef uint32_t	Reg;
				};
			namespace QBusAddressErrorLog { // Register description
				typedef uint32_t	Reg;
				};
			namespace QBusDataErrorLog { // Register description
				typedef uint32_t	Reg;
				};
			};
		};
	};
#endif
