/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_hw_tundra_qspan_qspanh_
#define _oscl_hw_tundra_qspan_qspanh_
#include "fields.h"

namespace Oscl {
namespace Tundra {
namespace Qspan {

typedef struct RegMap {
	volatile ConfigSpaceId::Reg						PCI_ID;
	volatile ConfigSpaceControlStatus::Reg			PCI_CS;
	volatile ConfigClass::Reg						PCI_CLASS;
	volatile ConfigMisc0::Reg						PCI_MISC0;
	volatile ConfigBaseAddrForMemory::Reg			PCI_BSM;
	uint32_t										unimplemented0x014;
	volatile ConfigBaseAddrForTarget0::Reg			PCI_BST0;
	volatile ConfigBaseAddrForTarget1::Reg			PCI_BST1;
	uint32_t										unimplemented0x020;
	uint32_t										unimplemented0x024;
	uint32_t										unimplemented0x028;
	volatile ConfigSubsystemId::Reg					PCI_SID;
	volatile ConfigExpansionRomBaseAddr::Reg		PCI_BSROM;
	uint32_t										reserved0x034;
	uint32_t										reserved0x038;
	volatile ConfigMisc1::Reg						PCI_MISC1;
	uint32_t										reserved0x040[48];
	volatile BusTargetImage0Control::Reg			PBTI0_CTL;
	volatile BusTargetImage0Address::Reg			PBTI0_ADD;
	uint32_t										reserved0x108;
	uint32_t										reserved0x10C;
	volatile BusTargetImage1Control::Reg			PBTI1_CTL;
	volatile BusTargetImage1Address::Reg			PBTI1_ADD;
	uint32_t										reserved0x118[9];
	volatile BusExpansionRomControl::Reg			PBROM_CTL;
	volatile BusErrorControlStatus::Reg				PB_ERRCS;
	volatile BusAddressErrorLog::Reg				PB_AERR;
	volatile BusDataErrorLog::Reg					PB_DERR;
	uint32_t										reserved0x14C[173];
	volatile IdmaControlStatus::Reg					IDMA_CS;
	volatile IdmaAddress::Reg						IDMA_ADD;
	volatile IdmaTransferCount::Reg					IDMA_CNT;
	uint32_t										reserved0x40C[61];
	volatile ConfigAddress::Reg						CON_ADD;
	volatile ConfigData::Reg						CON_DATA;
	volatile IackCycleGenerator::Reg				IACK_GEN;
	uint32_t										reserved0x50C[61];
	volatile InterruptStatus::Reg					INT_STAT;
	volatile InterruptControl::Reg					INT_CTL;
	volatile InterruptDirectionControl::Reg			INT_DIR;
	volatile InterruptControl2::Reg					INT_CTL2;
	uint32_t										reserved0x610[124];
	volatile MiscControlStatus::Reg					MISC_CTL;
	volatile EepromControlStatus::Reg				EEPROM_CS;
	uint32_t										reserved0x808[446];
	volatile QBusSlaveImage0Control::Reg			QBSI0_CTL;
	volatile QBusSlaveImage0AddressTranslation::Reg	QBSI0_AT;
	uint32_t										reserved0xF08[2];
	volatile QBusSlaveImage1Control::Reg			QBSI1_CTL;
	volatile QBusSlaveImage1AddressTranslation::Reg	QBSI1_AT;
	uint32_t										reserved0xF18[26];
	volatile QBusErrorLogControlStatus::Reg			QB_ERRCS;
	volatile QBusAddressErrorLog::Reg				QB_AERR;
	volatile QBusDataErrorLog::Reg					QB_DERR;
	uint32_t										reserved0xF8C[29];
	} RegMap;

}
}
}

#endif
