/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _hw_pci_config_standard_
#define _hw_pci_config_standard_
#include <stdint.h>
namespace Oscl { // Namespace description
	namespace Pci { // Namespace description
		namespace Config { // Namespace description
			namespace HeaderTypeZero { // Namespace description
				namespace DwRegOffset { // Namespace description
					enum {BaseAddr0 = 4};
					enum {BaseAddr1 = 5};
					enum {BaseAddr2 = 6};
					enum {BaseAddr3 = 7};
					enum {BaseAddr4 = 8};
					enum {BaseAddr5 = 9};
					enum {CardBusCisPtr = 10};
					enum {SubsystemID = 11};
					enum {ExpRomBaseAddr = 12};
					enum {MaxLat = 15};
					enum {MinGnt = 15};
					enum {IntPin = 15};
					enum {IntLine = 15};
					};
				namespace CmdStatus { // Register description
					typedef uint32_t	Reg;
					namespace CmdReg { // Field Description
						enum {Lsb = 0};
						enum {FieldMask = 0x0000FFFF};
						namespace MemorySpace { // Field Description
							enum {Lsb = 1};
							enum {FieldMask = 0x00000002};
							enum {Value_Enable = 0x1};
							enum {ValueMask_Enable = 0x00000002};
							enum {Value_Disable = 0x0};
							enum {ValueMask_Disable = 0x00000000};
							};
						namespace IoSpace { // Field Description
							enum {Lsb = 0};
							enum {FieldMask = 0x00000001};
							enum {Value_Enable = 0x1};
							enum {ValueMask_Enable = 0x00000001};
							enum {Value_Disable = 0x0};
							enum {ValueMask_Disable = 0x00000000};
							};
						};
					};
				enum {MaxBaseAddressRegisters = 6};
				namespace Subsystem { // Register description
					typedef uint32_t	Reg;
					namespace ID { // Field Description
						enum {Lsb = 16};
						enum {FieldMask = 0xFFFF0000};
						};
					namespace Vendor { // Field Description
						enum {Lsb = 0};
						enum {FieldMask = 0x0000FFFF};
						};
					};
				namespace MaxLatMinGntIntPinIntLine { // Register description
					typedef uint32_t	Reg;
					namespace MaxLatency { // Field Description
						enum {Lsb = 24};
						enum {FieldMask = 0xFF000000};
						};
					namespace MaxGrant { // Field Description
						enum {Lsb = 16};
						enum {FieldMask = 0x00FF0000};
						};
					namespace InterruptPin { // Field Description
						enum {Lsb = 8};
						enum {FieldMask = 0x0000FF00};
						};
					namespace InterruptLine { // Field Description
						enum {Lsb = 0};
						enum {FieldMask = 0x000000FF};
						};
					};
				};
			};
		};
	};
#endif
