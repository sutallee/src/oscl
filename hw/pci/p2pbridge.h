/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _hw_pci_config_p2pbridge_
#define _hw_pci_config_p2pbridge_
#include <stdint.h>
namespace Oscl { // Namespace description
	namespace Pci { // Namespace description
		namespace Config { // Namespace description
			namespace P2PBridge { // Namespace description
				namespace DwRegOffset { // Namespace description
					enum {BaseAddr0 = 4};
					enum {BaseAddr1 = 5};
					enum {SecLatencyTimer = 6};
					enum {SubordinateBusNum = 6};
					enum {SecondaryBusNum = 6};
					enum {PrimaryBusNum = 6};
					enum {SecondaryStatus = 7};
					enum {IoLimit = 7};
					enum {IoBase = 7};
					enum {MemoryLimit = 8};
					enum {MemoryBase = 8};
					enum {PrefetchMemLimit = 9};
					enum {PrefetchMemBase = 9};
					enum {PrefetchableBase = 10};
					enum {PrefetchableLimit = 11};
					enum {IoLimit = 12};
					enum {IoBase = 12};
					enum {CapabilityPointer = 13};
					enum {ExpRomBaseAddr = 14};
					enum {BridgeControl = 15};
					enum {IntPin = 15};
					enum {IntLine = 15};
					};
				namespace SecLatSubBusSecBusPriBus { // Register description
					typedef uint32_t	Reg;
					namespace SecondaryLatencyTimer { // Field Description
						enum {Lsb = 24};
						enum {FieldMask = 0xFF000000};
						enum {Value_Max = 0xFF};
						enum {ValueMask_Max = 0xFF000000};
						};
					namespace SubordinateBusNum { // Field Description
						enum {Lsb = 16};
						enum {FieldMask = 0x00FF0000};
						enum {Value_Max = 0xFF};
						enum {ValueMask_Max = 0x00FF0000};
						};
					namespace SecondaryBusNum { // Field Description
						enum {Lsb = 8};
						enum {FieldMask = 0x0000FF00};
						enum {Value_Max = 0xFF};
						enum {ValueMask_Max = 0x0000FF00};
						};
					namespace PrimaryBusNum { // Field Description
						enum {Lsb = 0};
						enum {FieldMask = 0x000000FF};
						enum {Value_Max = 0xFF};
						enum {ValueMask_Max = 0x000000FF};
						};
					};
				namespace SecStatusIoLimBase { // Register description
					typedef uint32_t	Reg;
					namespace SecondaryStatus { // Field Description
						enum {Lsb = 16};
						enum {FieldMask = 0xFFFF0000};
						namespace DetectedParityError { // Field Description
							enum {Lsb = 31};
							enum {FieldMask = 0x80000000};
							enum {Value_Clear = 0x1};
							enum {ValueMask_Clear = 0x80000000};
							enum {Value_Error = 0x1};
							enum {ValueMask_Error = 0x80000000};
							enum {Value_NoError = 0x0};
							enum {ValueMask_NoError = 0x00000000};
							};
						namespace ReceivedSystemError { // Field Description
							enum {Lsb = 30};
							enum {FieldMask = 0x40000000};
							enum {Value_Clear = 0x1};
							enum {ValueMask_Clear = 0x40000000};
							enum {Value_Error = 0x1};
							enum {ValueMask_Error = 0x40000000};
							enum {Value_NoError = 0x0};
							enum {ValueMask_NoError = 0x00000000};
							};
						namespace ReceivedMasterAbort { // Field Description
							enum {Lsb = 29};
							enum {FieldMask = 0x20000000};
							enum {Value_Abort = 0x1};
							enum {ValueMask_Abort = 0x20000000};
							enum {Value_NoAbort = 0x0};
							enum {ValueMask_NoAbort = 0x00000000};
							enum {Value_Clear = 0x1};
							enum {ValueMask_Clear = 0x20000000};
							};
						namespace ReceivedTargetAbort { // Field Description
							enum {Lsb = 28};
							enum {FieldMask = 0x10000000};
							enum {Value_Abort = 0x1};
							enum {ValueMask_Abort = 0x10000000};
							enum {Value_NoAbort = 0x0};
							enum {ValueMask_NoAbort = 0x00000000};
							enum {Value_Clear = 0x1};
							enum {ValueMask_Clear = 0x10000000};
							};
						namespace SignaledTargetAbort { // Field Description
							enum {Lsb = 27};
							enum {FieldMask = 0x08000000};
							enum {Value_Abort = 0x1};
							enum {ValueMask_Abort = 0x08000000};
							enum {Value_NoAbort = 0x0};
							enum {ValueMask_NoAbort = 0x00000000};
							enum {Value_Clear = 0x1};
							enum {ValueMask_Clear = 0x08000000};
							};
						namespace DeviceSelectTiming { // Field Description
							enum {Lsb = 25};
							enum {FieldMask = 0x06000000};
							enum {Value_Fast = 0x0};
							enum {ValueMask_Fast = 0x00000000};
							enum {Value_Medium = 0x1};
							enum {ValueMask_Medium = 0x02000000};
							enum {Value_Slow = 0x2};
							enum {ValueMask_Slow = 0x04000000};
							};
						namespace MasterDataParityError { // Field Description
							enum {Lsb = 24};
							enum {FieldMask = 0x01000000};
							enum {Value_Error = 0x1};
							enum {ValueMask_Error = 0x01000000};
							enum {Value_NoError = 0x0};
							enum {ValueMask_NoError = 0x00000000};
							enum {Value_Clear = 0x1};
							enum {ValueMask_Clear = 0x01000000};
							};
						namespace FastBackToBackCapable { // Field Description
							enum {Lsb = 23};
							enum {FieldMask = 0x00800000};
							enum {Value_Capable = 0x1};
							enum {ValueMask_Capable = 0x00800000};
							enum {Value_Incapable = 0x0};
							enum {ValueMask_Incapable = 0x00000000};
							};
						namespace UdfSupport { // Field Description
							enum {Lsb = 22};
							enum {FieldMask = 0x00400000};
							enum {Value_Supported = 0x1};
							enum {ValueMask_Supported = 0x00400000};
							enum {Value_Unsupported = 0x0};
							enum {ValueMask_Unsupported = 0x00000000};
							};
						namespace CapableOf66MHz { // Field Description
							enum {Lsb = 21};
							enum {FieldMask = 0x00200000};
							enum {Value_Capable = 0x1};
							enum {ValueMask_Capable = 0x00200000};
							enum {Value_Incapable = 0x0};
							enum {ValueMask_Incapable = 0x00000000};
							};
						namespace CapabilitiesList { // Field Description
							enum {Lsb = 20};
							enum {FieldMask = 0x00100000};
							enum {Value_Implemented = 0x1};
							enum {ValueMask_Implemented = 0x00100000};
							enum {Value_Unimplemented = 0x0};
							enum {ValueMask_Unimplemented = 0x00000000};
							};
						};
					namespace IoLimit { // Field Description
						enum {Lsb = 8};
						enum {FieldMask = 0x0000FF00};
						};
					namespace IoBase { // Field Description
						enum {Lsb = 0};
						enum {FieldMask = 0x000000FF};
						};
					};
				namespace MemLimitBase { // Register description
					typedef uint32_t	Reg;
					namespace Limit { // Field Description
						enum {Lsb = 16};
						enum {FieldMask = 0xFFFF0000};
						};
					namespace Base { // Field Description
						enum {Lsb = 0};
						enum {FieldMask = 0x0000FFFF};
						};
					};
				namespace PreFetchMemLimitBase { // Register description
					typedef uint32_t	Reg;
					namespace Limit { // Field Description
						enum {Lsb = 16};
						enum {FieldMask = 0xFFFF0000};
						};
					namespace Base { // Field Description
						enum {Lsb = 0};
						enum {FieldMask = 0x0000FFFF};
						};
					};
				namespace PreFetchBase { // Register description
					typedef uint32_t	Reg;
					};
				namespace PreFetchLimit { // Register description
					typedef uint32_t	Reg;
					};
				namespace IoLimitBase { // Register description
					typedef uint32_t	Reg;
					namespace Limit { // Field Description
						enum {Lsb = 16};
						enum {FieldMask = 0xFFFF0000};
						};
					namespace Base { // Field Description
						enum {Lsb = 0};
						enum {FieldMask = 0x0000FFFF};
						};
					};
				namespace CapabilityPointer { // Register description
					typedef uint32_t	Reg;
					namespace CapPtr { // Field Description
						enum {Lsb = 0};
						enum {FieldMask = 0x000000FF};
						};
					};
				namespace BridgeCtrlIntPinLine { // Register description
					typedef uint32_t	Reg;
					namespace BridgeControl { // Field Description
						enum {Lsb = 16};
						enum {FieldMask = 0xFFFF0000};
						};
					namespace InterruptPin { // Field Description
						enum {Lsb = 8};
						enum {FieldMask = 0x0000FF00};
						};
					namespace InterruptLine { // Field Description
						enum {Lsb = 0};
						enum {FieldMask = 0x000000FF};
						};
					};
				};
			};
		};
	};
#endif
