/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

module hw_pci_config_p2pbridge {
include "oscl/compiler/types.h";
namespace Oscl {
namespace Pci {
namespace Config {
namespace P2PBridge {
	namespace DwRegOffset {
		value BaseAddr0			4
		value BaseAddr1			5
		value SecLatencyTimer	6
		value SubordinateBusNum	6
		value SecondaryBusNum	6
		value PrimaryBusNum		6
		value SecondaryStatus	7
		value IoLimit			7
		value IoBase			7
		value MemoryLimit		8
		value MemoryBase		8
		value PrefetchMemLimit	9
		value PrefetchMemBase	9
		value PrefetchableBase	10
		value PrefetchableLimit	11
		value IoLimit			12
		value IoBase			12
		value CapabilityPointer	13
		value ExpRomBaseAddr	14
		value BridgeControl		15
		value IntPin			15
		value IntLine			15
		}
	register SecLatSubBusSecBusPriBus uint32_t {
		field SecondaryLatencyTimer 24 8 {
			// Defines the timeslice when the bridge
			// is acting as the initiator on the
			// secondary interface.
			value Max 0xFF
			}
		field SubordinateBusNum 16 8 {
			// Highest numbered bus that exists
			// downstream. If there are no
			// P2P bridges on the secondary bus
			// the value is set to the same as the
			// secondary bus number register.
			value Max 0xFF
			}
		field SecondaryBusNum 8 8 {
			value Max 0xFF
			}
		field PrimaryBusNum 0 8 {
			value Max 0xFF
			}
		}
	register SecStatusIoLimBase uint32_t {
		field SecondaryStatus 16 16 {
			field DetectedParityError 15 1 {
				value Clear 1
				value Error 1
				value NoError 0
				}
			field ReceivedSystemError 14 1 {
				value Clear 1
				value Error 1
				value NoError 0
				}
			field ReceivedMasterAbort 13 1 {
				value Abort 1
				value NoAbort 0
				value Clear 1
				}
			field ReceivedTargetAbort 12 1 {
				value Abort 1
				value NoAbort 0
				value Clear 1
				}
			field SignaledTargetAbort 11 1 {
				value Abort 1
				value NoAbort 0
				value Clear 1
				}
			field DeviceSelectTiming 9 2 {
				value Fast		0
				value Medium	1
				value Slow		2
				}
			field MasterDataParityError 8 1 {
				value Error 1
				value NoError 0
				value Clear 1
				}
			field FastBackToBackCapable 7 1 {
				value Capable 1
				value Incapable 0
				}
			field UdfSupport 6 1 {
				value Supported 1
				value Unsupported 0
				}
			field CapableOf66MHz 5 1 {
				value Capable 1
				value Incapable 0
				}
			field CapabilitiesList 4 1 {
				value Implemented 1
				value Unimplemented 0
				}
			}
		field IoLimit 8 8 {
			}
		field IoBase 0 8 {
			}
		}
	register MemLimitBase uint32_t {
		field Limit 16 16 {
			}
		field Base 0 16 {
			}
		}
	register PreFetchMemLimitBase uint32_t {
		field Limit 16 16 {
			}
		field Base 0 16 {
			}
		}
	register PreFetchBase uint32_t {
		}
	register PreFetchLimit uint32_t {
		}
	register IoLimitBase uint32_t {
		field Limit 16 16 {
			}
		field Base 0 16 {
			}
		}
	register CapabilityPointer uint32_t {
		field CapPtr 0 8 {
			}
		}
	register BridgeCtrlIntPinLine uint32_t {
		field BridgeControl 16 16 {
			}
		field InterruptPin 8 8 {
			}
		field InterruptLine 0 8 {
			}
		}
	}
}
}
}
}

