/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _hw_pci_config_
#define _hw_pci_config_
#include <stdint.h>
namespace Oscl { // Namespace description
	namespace Pci { // Namespace description
		namespace Config { // Namespace description
			enum {MaxBusesPerSystem = 256};
			enum {MaxDevicesPerBus = 32};
			enum {MaxFunctionsPerDevice = 8};
			namespace DwRegOffset { // Namespace description
				enum {ID = 0};
				enum {CmdStatus = 1};
				enum {ClassRev = 2};
				enum {Type = 3};
				enum {Bist = 3};
				enum {Latency = 3};
				enum {CacheLine = 3};
				};
			namespace ID { // Register description
				typedef uint32_t	Reg;
				namespace Device { // Field Description
					enum {Lsb = 16};
					enum {FieldMask = 0xFFFF0000};
					enum {Value_Max = 0xFFFF};
					enum {ValueMask_Max = 0xFFFF0000};
					};
				namespace Vendor { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x0000FFFF};
					enum {Value_NoFunctionPresent = 0xFFFF};
					enum {ValueMask_NoFunctionPresent = 0x0000FFFF};
					enum {Value_Max = 0xFFFF};
					enum {ValueMask_Max = 0x0000FFFF};
					};
				};
			namespace CmdStatus { // Register description
				typedef uint32_t	Reg;
				namespace CmdReg { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x0000FFFF};
					namespace FastBackToBack { // Field Description
						enum {Lsb = 9};
						enum {FieldMask = 0x00000200};
						enum {Value_Enable = 0x1};
						enum {ValueMask_Enable = 0x00000200};
						enum {Value_Disable = 0x0};
						enum {ValueMask_Disable = 0x00000000};
						};
					namespace SystemError { // Field Description
						enum {Lsb = 8};
						enum {FieldMask = 0x00000100};
						enum {Value_Enable = 0x1};
						enum {ValueMask_Enable = 0x00000100};
						enum {Value_Disable = 0x0};
						enum {ValueMask_Disable = 0x00000000};
						};
					namespace SteppingControl { // Field Description
						enum {Lsb = 7};
						enum {FieldMask = 0x00000080};
						enum {Value_Enable = 0x1};
						enum {ValueMask_Enable = 0x00000080};
						enum {Value_Disable = 0x0};
						enum {ValueMask_Disable = 0x00000000};
						};
					namespace ParityErrorResponse { // Field Description
						enum {Lsb = 6};
						enum {FieldMask = 0x00000040};
						enum {Value_Enable = 0x1};
						enum {ValueMask_Enable = 0x00000040};
						enum {Value_Disable = 0x0};
						enum {ValueMask_Disable = 0x00000000};
						};
					namespace VgaPaletteSnoop { // Field Description
						enum {Lsb = 5};
						enum {FieldMask = 0x00000020};
						enum {Value_Enable = 0x1};
						enum {ValueMask_Enable = 0x00000020};
						enum {Value_Disable = 0x0};
						enum {ValueMask_Disable = 0x00000000};
						};
					namespace MemoryWriteAndInvalidate { // Field Description
						enum {Lsb = 4};
						enum {FieldMask = 0x00000010};
						enum {Value_Enable = 0x1};
						enum {ValueMask_Enable = 0x00000010};
						enum {Value_Disable = 0x0};
						enum {ValueMask_Disable = 0x00000000};
						};
					namespace SpecialCycles { // Field Description
						enum {Lsb = 3};
						enum {FieldMask = 0x00000008};
						enum {Value_Enable = 0x1};
						enum {ValueMask_Enable = 0x00000008};
						enum {Value_Disable = 0x0};
						enum {ValueMask_Disable = 0x00000000};
						};
					namespace BusMaster { // Field Description
						enum {Lsb = 2};
						enum {FieldMask = 0x00000004};
						enum {Value_Enable = 0x1};
						enum {ValueMask_Enable = 0x00000004};
						enum {Value_Disable = 0x0};
						enum {ValueMask_Disable = 0x00000000};
						};
					};
				namespace StatusReg { // Field Description
					enum {Lsb = 16};
					enum {FieldMask = 0xFFFF0000};
					namespace DetectedParityError { // Field Description
						enum {Lsb = 31};
						enum {FieldMask = 0x80000000};
						enum {Value_Clear = 0x1};
						enum {ValueMask_Clear = 0x80000000};
						enum {Value_Error = 0x1};
						enum {ValueMask_Error = 0x80000000};
						enum {Value_NoError = 0x0};
						enum {ValueMask_NoError = 0x00000000};
						};
					namespace SignaledSystemError { // Field Description
						enum {Lsb = 30};
						enum {FieldMask = 0x40000000};
						enum {Value_Clear = 0x1};
						enum {ValueMask_Clear = 0x40000000};
						enum {Value_Error = 0x1};
						enum {ValueMask_Error = 0x40000000};
						enum {Value_NoError = 0x0};
						enum {ValueMask_NoError = 0x00000000};
						};
					namespace ReceivedMasterAbort { // Field Description
						enum {Lsb = 29};
						enum {FieldMask = 0x20000000};
						enum {Value_Abort = 0x1};
						enum {ValueMask_Abort = 0x20000000};
						enum {Value_NoAbort = 0x0};
						enum {ValueMask_NoAbort = 0x00000000};
						enum {Value_Clear = 0x1};
						enum {ValueMask_Clear = 0x20000000};
						};
					namespace ReceivedTargetAbort { // Field Description
						enum {Lsb = 28};
						enum {FieldMask = 0x10000000};
						enum {Value_Abort = 0x1};
						enum {ValueMask_Abort = 0x10000000};
						enum {Value_NoAbort = 0x0};
						enum {ValueMask_NoAbort = 0x00000000};
						enum {Value_Clear = 0x1};
						enum {ValueMask_Clear = 0x10000000};
						};
					namespace SignaledTargetAbort { // Field Description
						enum {Lsb = 27};
						enum {FieldMask = 0x08000000};
						enum {Value_Abort = 0x1};
						enum {ValueMask_Abort = 0x08000000};
						enum {Value_NoAbort = 0x0};
						enum {ValueMask_NoAbort = 0x00000000};
						enum {Value_Clear = 0x1};
						enum {ValueMask_Clear = 0x08000000};
						};
					namespace DeviceSelectTiming { // Field Description
						enum {Lsb = 25};
						enum {FieldMask = 0x06000000};
						enum {Value_Fast = 0x0};
						enum {ValueMask_Fast = 0x00000000};
						enum {Value_Medium = 0x1};
						enum {ValueMask_Medium = 0x02000000};
						enum {Value_Slow = 0x2};
						enum {ValueMask_Slow = 0x04000000};
						};
					namespace MasterDataParityError { // Field Description
						enum {Lsb = 24};
						enum {FieldMask = 0x01000000};
						enum {Value_Error = 0x1};
						enum {ValueMask_Error = 0x01000000};
						enum {Value_NoError = 0x0};
						enum {ValueMask_NoError = 0x00000000};
						enum {Value_Clear = 0x1};
						enum {ValueMask_Clear = 0x01000000};
						};
					namespace FastBackToBackCapable { // Field Description
						enum {Lsb = 23};
						enum {FieldMask = 0x00800000};
						enum {Value_Capable = 0x1};
						enum {ValueMask_Capable = 0x00800000};
						enum {Value_Incapable = 0x0};
						enum {ValueMask_Incapable = 0x00000000};
						};
					namespace UdfSupport { // Field Description
						enum {Lsb = 22};
						enum {FieldMask = 0x00400000};
						enum {Value_Supported = 0x1};
						enum {ValueMask_Supported = 0x00400000};
						enum {Value_Unsupported = 0x0};
						enum {ValueMask_Unsupported = 0x00000000};
						};
					namespace CapableOf66MHz { // Field Description
						enum {Lsb = 21};
						enum {FieldMask = 0x00200000};
						enum {Value_Capable = 0x1};
						enum {ValueMask_Capable = 0x00200000};
						enum {Value_Incapable = 0x0};
						enum {ValueMask_Incapable = 0x00000000};
						};
					namespace CapabilitiesList { // Field Description
						enum {Lsb = 20};
						enum {FieldMask = 0x00100000};
						enum {Value_Implemented = 0x1};
						enum {ValueMask_Implemented = 0x00100000};
						enum {Value_Unimplemented = 0x0};
						enum {ValueMask_Unimplemented = 0x00000000};
						};
					};
				};
			namespace ClassRev { // Register description
				typedef uint32_t	Reg;
				namespace ClassCode { // Field Description
					enum {Lsb = 24};
					enum {FieldMask = 0xFF000000};
					enum {Value_Unknown = 0x0};
					enum {ValueMask_Unknown = 0x00000000};
					enum {Value_MassStorageController = 0x1};
					enum {ValueMask_MassStorageController = 0x01000000};
					enum {Value_NetworkController = 0x2};
					enum {ValueMask_NetworkController = 0x02000000};
					enum {Value_DisplayController = 0x3};
					enum {ValueMask_DisplayController = 0x03000000};
					enum {Value_MultimediaDevice = 0x4};
					enum {ValueMask_MultimediaDevice = 0x04000000};
					enum {Value_MemoryController = 0x5};
					enum {ValueMask_MemoryController = 0x05000000};
					enum {Value_BridgeDevice = 0x6};
					enum {ValueMask_BridgeDevice = 0x06000000};
					enum {Value_SimpleCommController = 0x7};
					enum {ValueMask_SimpleCommController = 0x07000000};
					enum {Value_BaseSystemPeripheral = 0x8};
					enum {ValueMask_BaseSystemPeripheral = 0x08000000};
					enum {Value_InputDevice = 0x9};
					enum {ValueMask_InputDevice = 0x09000000};
					enum {Value_DockingStation = 0xA};
					enum {ValueMask_DockingStation = 0x0A000000};
					enum {Value_Processor = 0xB};
					enum {ValueMask_Processor = 0x0B000000};
					enum {Value_SerialBusController = 0xC};
					enum {ValueMask_SerialBusController = 0x0C000000};
					enum {Value_WirelessController = 0xD};
					enum {ValueMask_WirelessController = 0x0D000000};
					enum {Value_IntelligentIoController = 0xE};
					enum {ValueMask_IntelligentIoController = 0x0E000000};
					enum {Value_SatelliteCommControler = 0xF};
					enum {ValueMask_SatelliteCommControler = 0x0F000000};
					enum {Value_EncryptionController = 0x10};
					enum {ValueMask_EncryptionController = 0x10000000};
					enum {Value_DataAcquisitionDspController = 0x11};
					enum {ValueMask_DataAcquisitionDspController = 0x11000000};
					enum {Value_DoesNotFitDefinedClassCode = 0xFF};
					enum {ValueMask_DoesNotFitDefinedClassCode = 0xFF000000};
					};
				namespace SubClassCode { // Field Description
					enum {Lsb = 16};
					enum {FieldMask = 0x00FF0000};
					enum {Value_Other = 0x80};
					enum {ValueMask_Other = 0x00800000};
					namespace Unknown { // Field Description
						enum {Lsb = 16};
						enum {FieldMask = 0x00FF0000};
						enum {Value_AllDevicesOtherThanVga = 0x0};
						enum {ValueMask_AllDevicesOtherThanVga = 0x00000000};
						enum {Value_VgaCompatibleDevice = 0x1};
						enum {ValueMask_VgaCompatibleDevice = 0x00010000};
						};
					namespace MassStorageController { // Field Description
						enum {Lsb = 16};
						enum {FieldMask = 0x00FF0000};
						enum {Value_SCSI = 0x0};
						enum {ValueMask_SCSI = 0x00000000};
						enum {Value_IDE = 0x1};
						enum {ValueMask_IDE = 0x00010000};
						enum {Value_Floppy = 0x2};
						enum {ValueMask_Floppy = 0x00020000};
						enum {Value_IPI = 0x3};
						enum {ValueMask_IPI = 0x00030000};
						enum {Value_RAID = 0x4};
						enum {ValueMask_RAID = 0x00040000};
						};
					namespace NetworkController { // Field Description
						enum {Lsb = 16};
						enum {FieldMask = 0x00FF0000};
						enum {Value_Ethernet = 0x0};
						enum {ValueMask_Ethernet = 0x00000000};
						enum {Value_TokenRing = 0x1};
						enum {ValueMask_TokenRing = 0x00010000};
						enum {Value_FDDI = 0x2};
						enum {ValueMask_FDDI = 0x00020000};
						enum {Value_ATM = 0x3};
						enum {ValueMask_ATM = 0x00030000};
						enum {Value_ISDN = 0x4};
						enum {ValueMask_ISDN = 0x00040000};
						};
					namespace DisplayController { // Field Description
						enum {Lsb = 16};
						enum {FieldMask = 0x00FF0000};
						enum {Value_VGA = 0x0};
						enum {ValueMask_VGA = 0x00000000};
						enum {Value_XGA = 0x1};
						enum {ValueMask_XGA = 0x00010000};
						enum {Value_ThreeD = 0x2};
						enum {ValueMask_ThreeD = 0x00020000};
						};
					namespace MultimediaDevice { // Field Description
						enum {Lsb = 16};
						enum {FieldMask = 0x00FF0000};
						enum {Value_Video = 0x0};
						enum {ValueMask_Video = 0x00000000};
						enum {Value_Audio = 0x1};
						enum {ValueMask_Audio = 0x00010000};
						enum {Value_ComputerTelephony = 0x2};
						enum {ValueMask_ComputerTelephony = 0x00020000};
						};
					namespace MemoryController { // Field Description
						enum {Lsb = 16};
						enum {FieldMask = 0x00FF0000};
						enum {Value_RAM = 0x0};
						enum {ValueMask_RAM = 0x00000000};
						enum {Value_Flash = 0x1};
						enum {ValueMask_Flash = 0x00010000};
						};
					namespace BridgeDevice { // Field Description
						enum {Lsb = 16};
						enum {FieldMask = 0x00FF0000};
						enum {Value_HostPci = 0x0};
						enum {ValueMask_HostPci = 0x00000000};
						enum {Value_PciIsa = 0x1};
						enum {ValueMask_PciIsa = 0x00010000};
						enum {Value_PciEisa = 0x2};
						enum {ValueMask_PciEisa = 0x00020000};
						enum {Value_PciMicroChannel = 0x3};
						enum {ValueMask_PciMicroChannel = 0x00030000};
						enum {Value_PciPci = 0x4};
						enum {ValueMask_PciPci = 0x00040000};
						enum {Value_PciPcmcia = 0x5};
						enum {ValueMask_PciPcmcia = 0x00050000};
						enum {Value_PciNuBus = 0x6};
						enum {ValueMask_PciNuBus = 0x00060000};
						enum {Value_PciCardBus = 0x7};
						enum {ValueMask_PciCardBus = 0x00070000};
						enum {Value_PciRaceway = 0x8};
						enum {ValueMask_PciRaceway = 0x00080000};
						};
					namespace SimpleCommController { // Field Description
						enum {Lsb = 16};
						enum {FieldMask = 0x00FF0000};
						enum {Value_Serial = 0x0};
						enum {ValueMask_Serial = 0x00000000};
						enum {Value_Parallel = 0x1};
						enum {ValueMask_Parallel = 0x00010000};
						enum {Value_MultiportSerial = 0x2};
						enum {ValueMask_MultiportSerial = 0x00020000};
						enum {Value_Modem = 0x3};
						enum {ValueMask_Modem = 0x00030000};
						};
					namespace BaseSystemPeripheral { // Field Description
						enum {Lsb = 16};
						enum {FieldMask = 0x00FF0000};
						enum {Value_PIC = 0x0};
						enum {ValueMask_PIC = 0x00000000};
						enum {Value_DMA = 0x1};
						enum {ValueMask_DMA = 0x00010000};
						enum {Value_Timer = 0x2};
						enum {ValueMask_Timer = 0x00020000};
						enum {Value_RTC = 0x3};
						enum {ValueMask_RTC = 0x00030000};
						enum {Value_PciHotPlug = 0x4};
						enum {ValueMask_PciHotPlug = 0x00040000};
						};
					namespace InputDevice { // Field Description
						enum {Lsb = 16};
						enum {FieldMask = 0x00FF0000};
						enum {Value_Keyboard = 0x0};
						enum {ValueMask_Keyboard = 0x00000000};
						enum {Value_Digitizer = 0x1};
						enum {ValueMask_Digitizer = 0x00010000};
						enum {Value_Mouse = 0x2};
						enum {ValueMask_Mouse = 0x00020000};
						enum {Value_Scanner = 0x3};
						enum {ValueMask_Scanner = 0x00030000};
						enum {Value_Gameport = 0x4};
						enum {ValueMask_Gameport = 0x00040000};
						};
					namespace DockingStation { // Field Description
						enum {Lsb = 16};
						enum {FieldMask = 0x00FF0000};
						enum {Value_Generic = 0x0};
						enum {ValueMask_Generic = 0x00000000};
						};
					namespace Processor { // Field Description
						enum {Lsb = 16};
						enum {FieldMask = 0x00FF0000};
						enum {Value_Intel386 = 0x0};
						enum {ValueMask_Intel386 = 0x00000000};
						enum {Value_Intel486 = 0x1};
						enum {ValueMask_Intel486 = 0x00010000};
						enum {Value_Pentium = 0x2};
						enum {ValueMask_Pentium = 0x00020000};
						enum {Value_Alpha = 0x10};
						enum {ValueMask_Alpha = 0x00100000};
						enum {Value_PowerPC = 0x20};
						enum {ValueMask_PowerPC = 0x00200000};
						enum {Value_MIPS = 0x30};
						enum {ValueMask_MIPS = 0x00300000};
						enum {Value_CoProcessor = 0x40};
						enum {ValueMask_CoProcessor = 0x00400000};
						};
					namespace SerialBusController { // Field Description
						enum {Lsb = 16};
						enum {FieldMask = 0x00FF0000};
						enum {Value_Firewire = 0x0};
						enum {ValueMask_Firewire = 0x00000000};
						enum {Value_AccessBus = 0x1};
						enum {ValueMask_AccessBus = 0x00010000};
						enum {Value_SSA = 0x2};
						enum {ValueMask_SSA = 0x00020000};
						enum {Value_USB = 0x3};
						enum {ValueMask_USB = 0x00030000};
						enum {Value_FibreChannel = 0x4};
						enum {ValueMask_FibreChannel = 0x00040000};
						enum {Value_SMBus = 0x5};
						enum {ValueMask_SMBus = 0x00050000};
						};
					namespace WirelessController { // Field Description
						enum {Lsb = 16};
						enum {FieldMask = 0x00FF0000};
						enum {Value_iRDA = 0x0};
						enum {ValueMask_iRDA = 0x00000000};
						enum {Value_ConsumerIR = 0x1};
						enum {ValueMask_ConsumerIR = 0x00010000};
						enum {Value_RF = 0x1};
						enum {ValueMask_RF = 0x00010000};
						};
					namespace IntelligentIoController { // Field Description
						enum {Lsb = 16};
						enum {FieldMask = 0x00FF0000};
						};
					namespace SatelliteCommControler { // Field Description
						enum {Lsb = 16};
						enum {FieldMask = 0x00FF0000};
						enum {Value_TV = 0x1};
						enum {ValueMask_TV = 0x00010000};
						enum {Value_Audio = 0x2};
						enum {ValueMask_Audio = 0x00020000};
						enum {Value_Voice = 0x3};
						enum {ValueMask_Voice = 0x00030000};
						enum {Value_Data = 0x4};
						enum {ValueMask_Data = 0x00040000};
						};
					namespace EncryptionController { // Field Description
						enum {Lsb = 16};
						enum {FieldMask = 0x00FF0000};
						enum {Value_NetworkComputing = 0x0};
						enum {ValueMask_NetworkComputing = 0x00000000};
						enum {Value_Entertainment = 0x1};
						enum {ValueMask_Entertainment = 0x00010000};
						};
					namespace DataAcquisitionDspController { // Field Description
						enum {Lsb = 16};
						enum {FieldMask = 0x00FF0000};
						enum {Value_DPIO = 0x0};
						enum {ValueMask_DPIO = 0x00000000};
						};
					namespace DoesNotFitDefinedClassCode { // Field Description
						enum {Lsb = 16};
						enum {FieldMask = 0x00FF0000};
						};
					};
				namespace ProgIf { // Field Description
					enum {Lsb = 8};
					enum {FieldMask = 0x0000FF00};
					enum {Value_Original = 0x0};
					enum {ValueMask_Original = 0x00000000};
					namespace Unknown { // Field Description
						enum {Lsb = 8};
						enum {FieldMask = 0x0000FF00};
						namespace VgaCompatibleDevice { // Field Description
							enum {Lsb = 8};
							enum {FieldMask = 0x0000FF00};
							enum {Value_Generic = 0x0};
							enum {ValueMask_Generic = 0x00000000};
							};
						};
					namespace MassStorageController { // Field Description
						enum {Lsb = 8};
						enum {FieldMask = 0x0000FF00};
						namespace SCSI { // Field Description
							enum {Lsb = 8};
							enum {FieldMask = 0x0000FF00};
							enum {Value_Generic = 0x0};
							enum {ValueMask_Generic = 0x00000000};
							};
						namespace IDE { // Field Description
							enum {Lsb = 8};
							enum {FieldMask = 0x0000FF00};
							namespace PrimaryOperatingMode { // Field Description
								enum {Lsb = 16};
								enum {FieldMask = 0x00010000};
								};
							namespace PrimaryProgrammableIndicator { // Field Description
								enum {Lsb = 17};
								enum {FieldMask = 0x00020000};
								};
							namespace SecondaryOperatingMode { // Field Description
								enum {Lsb = 18};
								enum {FieldMask = 0x00040000};
								};
							namespace SecondaryProgrammableIndicator { // Field Description
								enum {Lsb = 19};
								enum {FieldMask = 0x00080000};
								};
							namespace MasterDevice { // Field Description
								enum {Lsb = 23};
								enum {FieldMask = 0x00800000};
								};
							};
						namespace Floppy { // Field Description
							enum {Lsb = 8};
							enum {FieldMask = 0x0000FF00};
							enum {Value_Generic = 0x0};
							enum {ValueMask_Generic = 0x00000000};
							};
						namespace IPI { // Field Description
							enum {Lsb = 8};
							enum {FieldMask = 0x0000FF00};
							enum {Value_Generic = 0x0};
							enum {ValueMask_Generic = 0x00000000};
							};
						namespace RAID { // Field Description
							enum {Lsb = 8};
							enum {FieldMask = 0x0000FF00};
							enum {Value_Generic = 0x0};
							enum {ValueMask_Generic = 0x00000000};
							};
						};
					namespace NetworkController { // Field Description
						enum {Lsb = 8};
						enum {FieldMask = 0x0000FF00};
						namespace Ethernet { // Field Description
							enum {Lsb = 8};
							enum {FieldMask = 0x0000FF00};
							enum {Value_Generic = 0x0};
							enum {ValueMask_Generic = 0x00000000};
							};
						namespace TokenRing { // Field Description
							enum {Lsb = 8};
							enum {FieldMask = 0x0000FF00};
							enum {Value_Generic = 0x0};
							enum {ValueMask_Generic = 0x00000000};
							};
						namespace FDDI { // Field Description
							enum {Lsb = 8};
							enum {FieldMask = 0x0000FF00};
							enum {Value_Generic = 0x0};
							enum {ValueMask_Generic = 0x00000000};
							};
						namespace ATM { // Field Description
							enum {Lsb = 8};
							enum {FieldMask = 0x0000FF00};
							enum {Value_Generic = 0x0};
							enum {ValueMask_Generic = 0x00000000};
							};
						namespace ISDN { // Field Description
							enum {Lsb = 8};
							enum {FieldMask = 0x0000FF00};
							enum {Value_Generic = 0x0};
							enum {ValueMask_Generic = 0x00000000};
							};
						};
					namespace DisplayController { // Field Description
						enum {Lsb = 8};
						enum {FieldMask = 0x0000FF00};
						namespace VGA { // Field Description
							enum {Lsb = 8};
							enum {FieldMask = 0x0000FF00};
							enum {Value_Generic = 0x0};
							enum {ValueMask_Generic = 0x00000000};
							enum {Value_Compatible8215 = 0x1};
							enum {ValueMask_Compatible8215 = 0x00000100};
							};
						namespace XGA { // Field Description
							enum {Lsb = 8};
							enum {FieldMask = 0x0000FF00};
							enum {Value_Generic = 0x0};
							enum {ValueMask_Generic = 0x00000000};
							};
						namespace ThreeD { // Field Description
							enum {Lsb = 8};
							enum {FieldMask = 0x0000FF00};
							enum {Value_Generic = 0x0};
							enum {ValueMask_Generic = 0x00000000};
							};
						};
					namespace MultimediaDevice { // Field Description
						enum {Lsb = 8};
						enum {FieldMask = 0x0000FF00};
						namespace Video { // Field Description
							enum {Lsb = 8};
							enum {FieldMask = 0x0000FF00};
							enum {Value_Generic = 0x0};
							enum {ValueMask_Generic = 0x00000000};
							};
						namespace Audio { // Field Description
							enum {Lsb = 8};
							enum {FieldMask = 0x0000FF00};
							enum {Value_Generic = 0x0};
							enum {ValueMask_Generic = 0x00000000};
							};
						namespace ComputerTelephony { // Field Description
							enum {Lsb = 8};
							enum {FieldMask = 0x0000FF00};
							enum {Value_Generic = 0x0};
							enum {ValueMask_Generic = 0x00000000};
							};
						};
					namespace MemoryController { // Field Description
						enum {Lsb = 8};
						enum {FieldMask = 0x0000FF00};
						namespace RAM { // Field Description
							enum {Lsb = 8};
							enum {FieldMask = 0x0000FF00};
							enum {Value_Generic = 0x0};
							enum {ValueMask_Generic = 0x00000000};
							};
						namespace Flash { // Field Description
							enum {Lsb = 8};
							enum {FieldMask = 0x0000FF00};
							enum {Value_Generic = 0x0};
							enum {ValueMask_Generic = 0x00000000};
							};
						};
					namespace BridgeDevice { // Field Description
						enum {Lsb = 8};
						enum {FieldMask = 0x0000FF00};
						namespace HostPci { // Field Description
							enum {Lsb = 8};
							enum {FieldMask = 0x0000FF00};
							enum {Value_Generic = 0x0};
							enum {ValueMask_Generic = 0x00000000};
							};
						namespace PciIsa { // Field Description
							enum {Lsb = 8};
							enum {FieldMask = 0x0000FF00};
							enum {Value_Generic = 0x0};
							enum {ValueMask_Generic = 0x00000000};
							};
						namespace PciEisa { // Field Description
							enum {Lsb = 8};
							enum {FieldMask = 0x0000FF00};
							enum {Value_Generic = 0x0};
							enum {ValueMask_Generic = 0x00000000};
							};
						namespace PciMicroChannel { // Field Description
							enum {Lsb = 8};
							enum {FieldMask = 0x0000FF00};
							enum {Value_Generic = 0x0};
							enum {ValueMask_Generic = 0x00000000};
							};
						namespace PciPci { // Field Description
							enum {Lsb = 8};
							enum {FieldMask = 0x0000FF00};
							enum {Value_Generic = 0x0};
							enum {ValueMask_Generic = 0x00000000};
							enum {Value_PciSubtractive = 0x1};
							enum {ValueMask_PciSubtractive = 0x00000100};
							};
						namespace PciPcmcia { // Field Description
							enum {Lsb = 8};
							enum {FieldMask = 0x0000FF00};
							enum {Value_Generic = 0x0};
							enum {ValueMask_Generic = 0x00000000};
							};
						namespace PciNuBus { // Field Description
							enum {Lsb = 8};
							enum {FieldMask = 0x0000FF00};
							enum {Value_Generic = 0x0};
							enum {ValueMask_Generic = 0x00000000};
							};
						namespace PciCardBus { // Field Description
							enum {Lsb = 8};
							enum {FieldMask = 0x0000FF00};
							enum {Value_Generic = 0x0};
							enum {ValueMask_Generic = 0x00000000};
							};
						namespace PciRaceway { // Field Description
							enum {Lsb = 8};
							enum {FieldMask = 0x0000FF00};
							enum {Value_Generic = 0x0};
							enum {ValueMask_Generic = 0x00000000};
							};
						};
					namespace SimpleCommController { // Field Description
						enum {Lsb = 8};
						enum {FieldMask = 0x0000FF00};
						namespace Serial { // Field Description
							enum {Lsb = 8};
							enum {FieldMask = 0x0000FF00};
							enum {Value_GenericXt = 0x0};
							enum {ValueMask_GenericXt = 0x00000000};
							enum {Value_Compatible16450 = 0x1};
							enum {ValueMask_Compatible16450 = 0x00000100};
							enum {Value_Compatible16550 = 0x2};
							enum {ValueMask_Compatible16550 = 0x00000200};
							enum {Value_Compatible16650 = 0x3};
							enum {ValueMask_Compatible16650 = 0x00000300};
							enum {Value_Compatible16750 = 0x4};
							enum {ValueMask_Compatible16750 = 0x00000400};
							enum {Value_Compatible16850 = 0x5};
							enum {ValueMask_Compatible16850 = 0x00000500};
							enum {Value_Compatible16950 = 0x6};
							enum {ValueMask_Compatible16950 = 0x00000600};
							};
						namespace Parallel { // Field Description
							enum {Lsb = 8};
							enum {FieldMask = 0x0000FF00};
							enum {Value_Generic = 0x0};
							enum {ValueMask_Generic = 0x00000000};
							enum {Value_BidirectionalParallel = 0x1};
							enum {ValueMask_BidirectionalParallel = 0x00000100};
							enum {Value_Ecp1_XCompliant = 0x2};
							enum {ValueMask_Ecp1_XCompliant = 0x00000200};
							enum {Value_Ieee1284Controller = 0x3};
							enum {ValueMask_Ieee1284Controller = 0x00000300};
							enum {Value_Ieee1284Target = 0xFE};
							enum {ValueMask_Ieee1284Target = 0x0000FE00};
							};
						namespace MultiportSerial { // Field Description
							enum {Lsb = 8};
							enum {FieldMask = 0x0000FF00};
							enum {Value_Generic = 0x0};
							enum {ValueMask_Generic = 0x00000000};
							};
						namespace Modem { // Field Description
							enum {Lsb = 8};
							enum {FieldMask = 0x0000FF00};
							enum {Value_Generic = 0x0};
							enum {ValueMask_Generic = 0x00000000};
							enum {Value_Hayes16450 = 0x1};
							enum {ValueMask_Hayes16450 = 0x00000100};
							enum {Value_Hayes16550 = 0x2};
							enum {ValueMask_Hayes16550 = 0x00000200};
							enum {Value_Hayes16650 = 0x3};
							enum {ValueMask_Hayes16650 = 0x00000300};
							enum {Value_Hayes16750 = 0x4};
							enum {ValueMask_Hayes16750 = 0x00000400};
							};
						};
					namespace BaseSystemPeripheral { // Field Description
						enum {Lsb = 8};
						enum {FieldMask = 0x0000FF00};
						namespace PIC { // Field Description
							enum {Lsb = 8};
							enum {FieldMask = 0x0000FF00};
							enum {Value_Generic8259 = 0x0};
							enum {ValueMask_Generic8259 = 0x00000000};
							enum {Value_Isa = 0x1};
							enum {ValueMask_Isa = 0x00000100};
							enum {Value_Eisa = 0x2};
							enum {ValueMask_Eisa = 0x00000200};
							enum {Value_IoA = 0x10};
							enum {ValueMask_IoA = 0x00001000};
							enum {Value_IoxA = 0x20};
							enum {ValueMask_IoxA = 0x00002000};
							};
						namespace DMA { // Field Description
							enum {Lsb = 8};
							enum {FieldMask = 0x0000FF00};
							enum {Value_Generic8237 = 0x0};
							enum {ValueMask_Generic8237 = 0x00000000};
							enum {Value_Isa = 0x1};
							enum {ValueMask_Isa = 0x00000100};
							enum {Value_Eisa = 0x2};
							enum {ValueMask_Eisa = 0x00000200};
							};
						namespace Timer { // Field Description
							enum {Lsb = 8};
							enum {FieldMask = 0x0000FF00};
							enum {Value_Generic8254 = 0x0};
							enum {ValueMask_Generic8254 = 0x00000000};
							enum {Value_Isa = 0x1};
							enum {ValueMask_Isa = 0x00000100};
							enum {Value_Eisa = 0x2};
							enum {ValueMask_Eisa = 0x00000200};
							};
						namespace RTC { // Field Description
							enum {Lsb = 8};
							enum {FieldMask = 0x0000FF00};
							enum {Value_Generic = 0x0};
							enum {ValueMask_Generic = 0x00000000};
							enum {Value_Isa = 0x1};
							enum {ValueMask_Isa = 0x00000100};
							};
						namespace PciHotPlug { // Field Description
							enum {Lsb = 8};
							enum {FieldMask = 0x0000FF00};
							enum {Value_Generic = 0x0};
							enum {ValueMask_Generic = 0x00000000};
							};
						};
					namespace InputDevice { // Field Description
						enum {Lsb = 8};
						enum {FieldMask = 0x0000FF00};
						namespace Keyboard { // Field Description
							enum {Lsb = 8};
							enum {FieldMask = 0x0000FF00};
							enum {Value_Generic = 0x0};
							enum {ValueMask_Generic = 0x00000000};
							};
						namespace Digitizer { // Field Description
							enum {Lsb = 8};
							enum {FieldMask = 0x0000FF00};
							enum {Value_Generic = 0x0};
							enum {ValueMask_Generic = 0x00000000};
							};
						namespace Mouse { // Field Description
							enum {Lsb = 8};
							enum {FieldMask = 0x0000FF00};
							enum {Value_Generic = 0x0};
							enum {ValueMask_Generic = 0x00000000};
							};
						namespace Scanner { // Field Description
							enum {Lsb = 8};
							enum {FieldMask = 0x0000FF00};
							enum {Value_Generic = 0x0};
							enum {ValueMask_Generic = 0x00000000};
							};
						namespace Gameport { // Field Description
							enum {Lsb = 8};
							enum {FieldMask = 0x0000FF00};
							enum {Value_Generic = 0x0};
							enum {ValueMask_Generic = 0x00000000};
							enum {Value_Legacy = 0x0};
							enum {ValueMask_Legacy = 0x00000000};
							};
						};
					namespace DockingStation { // Field Description
						enum {Lsb = 8};
						enum {FieldMask = 0x0000FF00};
						namespace Generic { // Field Description
							enum {Lsb = 8};
							enum {FieldMask = 0x0000FF00};
							enum {Value_Generic = 0x0};
							enum {ValueMask_Generic = 0x00000000};
							};
						};
					namespace Processor { // Field Description
						enum {Lsb = 8};
						enum {FieldMask = 0x0000FF00};
						namespace Intel386 { // Field Description
							enum {Lsb = 8};
							enum {FieldMask = 0x0000FF00};
							enum {Value_Generic = 0x0};
							enum {ValueMask_Generic = 0x00000000};
							};
						namespace Intel486 { // Field Description
							enum {Lsb = 8};
							enum {FieldMask = 0x0000FF00};
							enum {Value_Generic = 0x0};
							enum {ValueMask_Generic = 0x00000000};
							};
						namespace Pentium { // Field Description
							enum {Lsb = 8};
							enum {FieldMask = 0x0000FF00};
							enum {Value_Generic = 0x0};
							enum {ValueMask_Generic = 0x00000000};
							};
						namespace Alpha { // Field Description
							enum {Lsb = 8};
							enum {FieldMask = 0x0000FF00};
							enum {Value_Generic = 0x0};
							enum {ValueMask_Generic = 0x00000000};
							};
						namespace PowerPC { // Field Description
							enum {Lsb = 8};
							enum {FieldMask = 0x0000FF00};
							enum {Value_Generic = 0x0};
							enum {ValueMask_Generic = 0x00000000};
							};
						namespace MIPS { // Field Description
							enum {Lsb = 8};
							enum {FieldMask = 0x0000FF00};
							enum {Value_Generic = 0x0};
							enum {ValueMask_Generic = 0x00000000};
							};
						namespace CoProcessor { // Field Description
							enum {Lsb = 8};
							enum {FieldMask = 0x0000FF00};
							enum {Value_Generic = 0x0};
							enum {ValueMask_Generic = 0x00000000};
							};
						};
					namespace SerialBusController { // Field Description
						enum {Lsb = 8};
						enum {FieldMask = 0x0000FF00};
						namespace Firewire { // Field Description
							enum {Lsb = 8};
							enum {FieldMask = 0x0000FF00};
							enum {Value_Generic = 0x0};
							enum {ValueMask_Generic = 0x00000000};
							enum {Value_OHCI = 0x1};
							enum {ValueMask_OHCI = 0x00000100};
							};
						namespace AccessBus { // Field Description
							enum {Lsb = 8};
							enum {FieldMask = 0x0000FF00};
							enum {Value_Generic = 0x0};
							enum {ValueMask_Generic = 0x00000000};
							};
						namespace SSA { // Field Description
							enum {Lsb = 8};
							enum {FieldMask = 0x0000FF00};
							enum {Value_Generic = 0x0};
							enum {ValueMask_Generic = 0x00000000};
							};
						namespace USB { // Field Description
							enum {Lsb = 8};
							enum {FieldMask = 0x0000FF00};
							enum {Value_Uhci = 0x0};
							enum {ValueMask_Uhci = 0x00000000};
							enum {Value_Ohci = 0x10};
							enum {ValueMask_Ohci = 0x00001000};
							enum {Value_Unspecified = 0x80};
							enum {ValueMask_Unspecified = 0x00008000};
							enum {Value_Device = 0xFE};
							enum {ValueMask_Device = 0x0000FE00};
							};
						namespace FibreChannel { // Field Description
							enum {Lsb = 8};
							enum {FieldMask = 0x0000FF00};
							enum {Value_Generic = 0x0};
							enum {ValueMask_Generic = 0x00000000};
							};
						namespace SMBus { // Field Description
							enum {Lsb = 8};
							enum {FieldMask = 0x0000FF00};
							enum {Value_Generic = 0x0};
							enum {ValueMask_Generic = 0x00000000};
							};
						};
					namespace WirelessController { // Field Description
						enum {Lsb = 8};
						enum {FieldMask = 0x0000FF00};
						namespace iRDA { // Field Description
							enum {Lsb = 8};
							enum {FieldMask = 0x0000FF00};
							enum {Value_Generic = 0x0};
							enum {ValueMask_Generic = 0x00000000};
							};
						namespace ConsumerIR { // Field Description
							enum {Lsb = 8};
							enum {FieldMask = 0x0000FF00};
							enum {Value_Generic = 0x0};
							enum {ValueMask_Generic = 0x00000000};
							};
						namespace RF { // Field Description
							enum {Lsb = 8};
							enum {FieldMask = 0x0000FF00};
							enum {Value_Generic = 0x0};
							enum {ValueMask_Generic = 0x00000000};
							};
						};
					namespace IntelligentIoController { // Field Description
						enum {Lsb = 8};
						enum {FieldMask = 0x0000FF00};
						namespace MessageFifo { // Field Description
							enum {Lsb = 8};
							enum {FieldMask = 0x0000FF00};
							enum {Value_Generic = 0x0};
							enum {ValueMask_Generic = 0x00000000};
							};
						};
					namespace SatelliteCommControler { // Field Description
						enum {Lsb = 8};
						enum {FieldMask = 0x0000FF00};
						namespace TV { // Field Description
							enum {Lsb = 8};
							enum {FieldMask = 0x0000FF00};
							enum {Value_Generic = 0x0};
							enum {ValueMask_Generic = 0x00000000};
							};
						namespace Audio { // Field Description
							enum {Lsb = 8};
							enum {FieldMask = 0x0000FF00};
							enum {Value_Generic = 0x0};
							enum {ValueMask_Generic = 0x00000000};
							};
						namespace Voice { // Field Description
							enum {Lsb = 8};
							enum {FieldMask = 0x0000FF00};
							enum {Value_Generic = 0x0};
							enum {ValueMask_Generic = 0x00000000};
							};
						namespace Data { // Field Description
							enum {Lsb = 8};
							enum {FieldMask = 0x0000FF00};
							enum {Value_Generic = 0x0};
							enum {ValueMask_Generic = 0x00000000};
							};
						};
					namespace EncryptionController { // Field Description
						enum {Lsb = 8};
						enum {FieldMask = 0x0000FF00};
						namespace NetworkComputing { // Field Description
							enum {Lsb = 8};
							enum {FieldMask = 0x0000FF00};
							enum {Value_Generic = 0x0};
							enum {ValueMask_Generic = 0x00000000};
							};
						namespace Entertainment { // Field Description
							enum {Lsb = 8};
							enum {FieldMask = 0x0000FF00};
							enum {Value_Generic = 0x0};
							enum {ValueMask_Generic = 0x00000000};
							};
						};
					namespace DataAcquisitionDspController { // Field Description
						enum {Lsb = 8};
						enum {FieldMask = 0x0000FF00};
						namespace DPIO { // Field Description
							enum {Lsb = 8};
							enum {FieldMask = 0x0000FF00};
							enum {Value_Generic = 0x0};
							enum {ValueMask_Generic = 0x00000000};
							};
						};
					};
				namespace Revision { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x000000FF};
					};
				};
			namespace BistTypeLatCache { // Register description
				typedef uint32_t	Reg;
				namespace BistCapable { // Field Description
					enum {Lsb = 31};
					enum {FieldMask = 0x80000000};
					enum {Value_Incapable = 0x0};
					enum {ValueMask_Incapable = 0x00000000};
					enum {Value_Capable = 0x1};
					enum {ValueMask_Capable = 0x80000000};
					};
				namespace BistStart { // Field Description
					enum {Lsb = 30};
					enum {FieldMask = 0x40000000};
					enum {Value_Start = 0x1};
					enum {ValueMask_Start = 0x40000000};
					};
				namespace CompletionCode { // Field Description
					enum {Lsb = 24};
					enum {FieldMask = 0x0F000000};
					enum {Value_Pass = 0x0};
					enum {ValueMask_Pass = 0x00000000};
					};
				namespace HeaderFormat { // Field Description
					enum {Lsb = 16};
					enum {FieldMask = 0x007F0000};
					enum {Value_HeaderTypeZero = 0x0};
					enum {ValueMask_HeaderTypeZero = 0x00000000};
					enum {Value_PciToPciBridge = 0x1};
					enum {ValueMask_PciToPciBridge = 0x00010000};
					enum {Value_CardBusBridge = 0x2};
					enum {ValueMask_CardBusBridge = 0x00020000};
					};
				namespace MultiFunction { // Field Description
					enum {Lsb = 23};
					enum {FieldMask = 0x00800000};
					enum {Value_SingleFunctionDevice = 0x0};
					enum {ValueMask_SingleFunctionDevice = 0x00000000};
					enum {Value_MultiFunctionDevice = 0x1};
					enum {ValueMask_MultiFunctionDevice = 0x00800000};
					};
				namespace LatencyTimer { // Field Description
					enum {Lsb = 8};
					enum {FieldMask = 0x0000FF00};
					};
				namespace CacheLineSize { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x000000FF};
					};
				};
			namespace BaseAddress { // Register description
				typedef uint32_t	Reg;
				namespace SpaceType { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x00000001};
					enum {Value_Memory = 0x0};
					enum {ValueMask_Memory = 0x00000000};
					enum {Value_IO = 0x1};
					enum {ValueMask_IO = 0x00000001};
					};
				namespace DecoderType { // Field Description
					enum {Lsb = 1};
					enum {FieldMask = 0x00000006};
					enum {Value_Size32Bit = 0x0};
					enum {ValueMask_Size32Bit = 0x00000000};
					enum {Value_Size20Bit = 0x1};
					enum {ValueMask_Size20Bit = 0x00000002};
					enum {Value_Size64Bit = 0x2};
					enum {ValueMask_Size64Bit = 0x00000004};
					};
				namespace Prefetchable { // Field Description
					enum {Lsb = 3};
					enum {FieldMask = 0x00000008};
					enum {Value_NotPrefetchable = 0x0};
					enum {ValueMask_NotPrefetchable = 0x00000000};
					enum {Value_Prefetchable = 0x1};
					enum {ValueMask_Prefetchable = 0x00000008};
					};
				namespace Address { // Field Description
					enum {Lsb = 4};
					enum {FieldMask = 0xFFFFFFF0};
					};
				};
			namespace ExpansionRomBaseAddr { // Register description
				typedef uint32_t	Reg;
				namespace Address { // Field Description
					enum {Lsb = 11};
					enum {FieldMask = 0xFFFFF800};
					};
				namespace Decoder { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x00000001};
					enum {Value_Disable = 0x0};
					enum {ValueMask_Disable = 0x00000000};
					enum {Value_Enable = 0x1};
					enum {ValueMask_Enable = 0x00000001};
					};
				};
			};
		};
	};
#endif
