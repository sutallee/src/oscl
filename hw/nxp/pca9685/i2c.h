/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

/* The ST LMS9DS1 module actually contains two diffent
	I2C modules.
	- Accelerometer/Gyroscope/Temperature
	- Magnetometer

	Each module has its own I2C addres.
 */

#ifndef _oscl_hw_nxp_pca9685_i2ch_
#define _oscl_hw_nxp_pca9685_i2ch_

#include <stdint.h>

/** */
namespace Oscl {
/** */
namespace HW {
/** */
namespace NXP {
/** */
namespace PCA9685 {
/** */
namespace I2C {

/**	This template variable evaluates to the I2C address of
	the PCA9685 based on the state of the A5:A0 address pins.
 */
template<
	bool a0,
	bool a1,
	bool a2,
	bool a3,
	bool a4,
	bool a5
	>
constexpr uint8_t	address	= (
		(a0 << 0)
	|	(a1 << 1)
	|	(a2 << 2)
	|	(a3 << 3)
	|	(a4 << 4)
	|	(a5 << 5)
	|	(1  << 6)
	);

/** This function returns the I2C address of the 
	PCA9685 based on the state of the A5:A0 pins.
 */
uint8_t	getAddress(
			bool a0,
			bool a1,
			bool a2,
			bool a3,
			bool a4,
			bool a5
			) noexcept;

}
}
}
}
}

#endif
