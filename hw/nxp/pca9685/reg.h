#ifndef __oscl_hw_nxp_pca9685_regh__
#define __oscl_hw_nxp_pca9685_regh__
#include <stdint.h>
namespace Oscl { // Namespace description
	namespace HW { // Namespace description
		namespace NXP { // Namespace description
			namespace PCA9685 { // Namespace description
				enum {MODE1 = 0};
				enum {MODE2 = 1};
				enum {SUBADR1 = 2};
				enum {SUBADR2 = 3};
				enum {SUBADR3 = 4};
				enum {ALLCALLADDR = 5};
				enum {LED_ON_L = 0};
				enum {LED_ON_H = 1};
				enum {LED_OFF_L = 2};
				enum {LED_OFF_H = 3};
				enum {LED0 = 6};
				enum {LED0_ON_L = 6};
				enum {LED0_ON_H = 7};
				enum {LED0_OFF_L = 8};
				enum {LED0_OFF_H = 9};
				enum {LED1 = 10};
				enum {LED1_ON_L = 10};
				enum {LED1_ON_H = 11};
				enum {LED1_OFF_L = 12};
				enum {LED1_OFF_H = 13};
				enum {LED2 = 14};
				enum {LED2_ON_L = 14};
				enum {LED2_ON_H = 15};
				enum {LED2_OFF_L = 16};
				enum {LED2_OFF_H = 17};
				enum {LED3 = 18};
				enum {LED3_ON_L = 18};
				enum {LED3_ON_H = 19};
				enum {LED3_OFF_L = 20};
				enum {LED3_OFF_H = 21};
				enum {LED4 = 22};
				enum {LED4_ON_L = 22};
				enum {LED4_ON_H = 23};
				enum {LED4_OFF_L = 24};
				enum {LED4_OFF_H = 25};
				enum {LED5 = 26};
				enum {LED5_ON_L = 26};
				enum {LED5_ON_H = 27};
				enum {LED5_OFF_L = 28};
				enum {LED5_OFF_H = 29};
				enum {LED6 = 30};
				enum {LED6_ON_L = 30};
				enum {LED6_ON_H = 31};
				enum {LED6_OFF_L = 32};
				enum {LED6_OFF_H = 33};
				enum {LED7 = 34};
				enum {LED7_ON_L = 34};
				enum {LED7_ON_H = 35};
				enum {LED7_OFF_L = 36};
				enum {LED7_OFF_H = 37};
				enum {LED8 = 38};
				enum {LED8_ON_L = 38};
				enum {LED8_ON_H = 39};
				enum {LED8_OFF_L = 40};
				enum {LED8_OFF_H = 41};
				enum {LED9 = 42};
				enum {LED9_ON_L = 42};
				enum {LED9_ON_H = 43};
				enum {LED9_OFF_L = 44};
				enum {LED9_OFF_H = 45};
				enum {LED10 = 46};
				enum {LED10_ON_L = 46};
				enum {LED10_ON_H = 47};
				enum {LED10_OFF_L = 48};
				enum {LED10_OFF_H = 49};
				enum {LED11 = 50};
				enum {LED11_ON_L = 50};
				enum {LED11_ON_H = 51};
				enum {LED11_OFF_L = 52};
				enum {LED11_OFF_H = 53};
				enum {LED12 = 54};
				enum {LED12_ON_L = 54};
				enum {LED12_ON_H = 55};
				enum {LED12_OFF_L = 56};
				enum {LED12_OFF_H = 57};
				enum {LED13 = 58};
				enum {LED13_ON_L = 58};
				enum {LED13_ON_H = 59};
				enum {LED13_OFF_L = 60};
				enum {LED13_OFF_H = 61};
				enum {LED14 = 62};
				enum {LED14_ON_L = 62};
				enum {LED14_ON_H = 63};
				enum {LED14_OFF_L = 64};
				enum {LED14_OFF_H = 65};
				enum {LED15 = 66};
				enum {LED15_ON_L = 66};
				enum {LED15_ON_H = 67};
				enum {LED15_OFF_L = 68};
				enum {LED15_OFF_H = 69};
				enum {ALL_LED = 250};
				enum {ALL_LED_ON_L = 250};
				enum {ALL_LED_ON_H = 251};
				enum {ALL_LED_OFF_L = 252};
				enum {ALL_LED_OFF_H = 253};
				enum {PRE_SCALE = 254};
				enum {TestMode = 255};
				enum {InternalOscillatorFreqInHz = 25000000};
				namespace Mode1 { // Register description
					typedef uint8_t	Reg;
					namespace RESTART { // Field Description
						enum {Lsb = 7};
						enum {FieldMask = 0x00000080};
						enum {Value_Default = 0x0};
						enum {ValueMask_Default = 0x00000000};
						enum {Value_Restart = 0x1};
						enum {ValueMask_Restart = 0x00000080};
						enum {Value_Disabled = 0x0};
						enum {ValueMask_Disabled = 0x00000000};
						enum {Value_Enabled = 0x1};
						enum {ValueMask_Enabled = 0x00000080};
						};
					namespace EXTCLK { // Field Description
						enum {Lsb = 6};
						enum {FieldMask = 0x00000040};
						enum {Value_Default = 0x0};
						enum {ValueMask_Default = 0x00000000};
						enum {Value_UseInternalClock = 0x0};
						enum {ValueMask_UseInternalClock = 0x00000000};
						enum {Value_UseExtclkPin = 0x1};
						enum {ValueMask_UseExtclkPin = 0x00000040};
						};
					namespace AI { // Field Description
						enum {Lsb = 5};
						enum {FieldMask = 0x00000020};
						enum {Value_Default = 0x0};
						enum {ValueMask_Default = 0x00000000};
						enum {Value_AutoIncrementDisable = 0x0};
						enum {ValueMask_AutoIncrementDisable = 0x00000000};
						enum {Value_AutoIncrementEnable = 0x1};
						enum {ValueMask_AutoIncrementEnable = 0x00000020};
						};
					namespace SLEEP { // Field Description
						enum {Lsb = 4};
						enum {FieldMask = 0x00000010};
						enum {Value_Default = 0x1};
						enum {ValueMask_Default = 0x00000010};
						enum {Value_Normal = 0x0};
						enum {ValueMask_Normal = 0x00000000};
						enum {Value_LowPowerMode = 0x1};
						enum {ValueMask_LowPowerMode = 0x00000010};
						};
					namespace SUB1 { // Field Description
						enum {Lsb = 3};
						enum {FieldMask = 0x00000008};
						enum {Value_Default = 0x0};
						enum {ValueMask_Default = 0x00000000};
						enum {Value_IgnoreSubAddress = 0x0};
						enum {ValueMask_IgnoreSubAddress = 0x00000000};
						enum {Value_RespondToSubAddress = 0x1};
						enum {ValueMask_RespondToSubAddress = 0x00000008};
						};
					namespace SUB2 { // Field Description
						enum {Lsb = 2};
						enum {FieldMask = 0x00000004};
						enum {Value_Default = 0x0};
						enum {ValueMask_Default = 0x00000000};
						enum {Value_IgnoreSubAddress = 0x0};
						enum {ValueMask_IgnoreSubAddress = 0x00000000};
						enum {Value_RespondToSubAddress = 0x1};
						enum {ValueMask_RespondToSubAddress = 0x00000004};
						};
					namespace SUB3 { // Field Description
						enum {Lsb = 1};
						enum {FieldMask = 0x00000002};
						enum {Value_Default = 0x0};
						enum {ValueMask_Default = 0x00000000};
						enum {Value_IgnoreSubAddress = 0x0};
						enum {ValueMask_IgnoreSubAddress = 0x00000000};
						enum {Value_RespondToSubAddress = 0x1};
						enum {ValueMask_RespondToSubAddress = 0x00000002};
						};
					namespace ALLCALL { // Field Description
						enum {Lsb = 3};
						enum {FieldMask = 0x00000008};
						enum {Value_Default = 0x1};
						enum {ValueMask_Default = 0x00000008};
						enum {Value_IgnoreAllCallAddress = 0x0};
						enum {ValueMask_IgnoreAllCallAddress = 0x00000000};
						enum {Value_RespondToAllCallAddress = 0x1};
						enum {ValueMask_RespondToAllCallAddress = 0x00000008};
						};
					};
				namespace Mode2 { // Register description
					typedef uint8_t	Reg;
					namespace INVRT { // Field Description
						enum {Lsb = 4};
						enum {FieldMask = 0x00000010};
						enum {Value_Default = 0x0};
						enum {ValueMask_Default = 0x00000000};
						enum {Value_NormalOutput = 0x0};
						enum {ValueMask_NormalOutput = 0x00000000};
						enum {Value_InvertOutput = 0x1};
						enum {ValueMask_InvertOutput = 0x00000010};
						};
					namespace OCH { // Field Description
						enum {Lsb = 3};
						enum {FieldMask = 0x00000008};
						enum {Value_Default = 0x0};
						enum {ValueMask_Default = 0x00000000};
						enum {Value_OuputsChangeOnSTOP = 0x0};
						enum {ValueMask_OuputsChangeOnSTOP = 0x00000000};
						enum {Value_OuputsChangeOnACK = 0x1};
						enum {ValueMask_OuputsChangeOnACK = 0x00000008};
						};
					namespace OUTDRV { // Field Description
						enum {Lsb = 2};
						enum {FieldMask = 0x00000004};
						enum {Value_Default = 0x1};
						enum {ValueMask_Default = 0x00000004};
						enum {Value_OpenDrain = 0x0};
						enum {ValueMask_OpenDrain = 0x00000000};
						enum {Value_TotemPole = 0x1};
						enum {ValueMask_TotemPole = 0x00000004};
						};
					namespace OUTNE { // Field Description
						enum {Lsb = 0};
						enum {FieldMask = 0x00000003};
						enum {Value_Default = 0x0};
						enum {ValueMask_Default = 0x00000000};
						enum {Value_LedLow = 0x0};
						enum {ValueMask_LedLow = 0x00000000};
						enum {Value_LedHigh = 0x1};
						enum {ValueMask_LedHigh = 0x00000001};
						enum {Value_LedHighZ = 0x2};
						enum {ValueMask_LedHighZ = 0x00000002};
						};
					};
				namespace LedOnL { // Register description
					typedef uint8_t	Reg;
					namespace LED_ON { // Field Description
						enum {Lsb = 0};
						enum {FieldMask = 0x000000FF};
						enum {Value_Default = 0x0};
						enum {ValueMask_Default = 0x00000000};
						};
					};
				namespace LedOnH { // Register description
					typedef uint8_t	Reg;
					namespace FULL_ON { // Field Description
						enum {Lsb = 4};
						enum {FieldMask = 0x00000010};
						enum {Value_Default = 0x0};
						enum {ValueMask_Default = 0x00000000};
						enum {Value_FullOn = 0x1};
						enum {ValueMask_FullOn = 0x00000010};
						};
					namespace LED_ON { // Field Description
						enum {Lsb = 0};
						enum {FieldMask = 0x0000000F};
						enum {Value_Default = 0x0};
						enum {ValueMask_Default = 0x00000000};
						};
					};
				namespace LedOffL { // Register description
					typedef uint8_t	Reg;
					namespace LED_ON { // Field Description
						enum {Lsb = 0};
						enum {FieldMask = 0x000000FF};
						enum {Value_Default = 0x0};
						enum {ValueMask_Default = 0x00000000};
						};
					};
				namespace LedOffH { // Register description
					typedef uint8_t	Reg;
					namespace FULL_OFF { // Field Description
						enum {Lsb = 4};
						enum {FieldMask = 0x00000010};
						enum {Value_Default = 0x0};
						enum {ValueMask_Default = 0x00000000};
						enum {Value_FullOff = 0x1};
						enum {ValueMask_FullOff = 0x00000010};
						};
					namespace LED_ON { // Field Description
						enum {Lsb = 0};
						enum {FieldMask = 0x0000000F};
						enum {Value_Default = 0x0};
						enum {ValueMask_Default = 0x00000000};
						};
					};
				namespace PreScale { // Register description
					typedef uint8_t	Reg;
					namespace REFL { // Field Description
						enum {Lsb = 0};
						enum {FieldMask = 0x000000FF};
						enum {Value_Default = 0x1E};
						enum {ValueMask_Default = 0x0000001E};
						enum {Value_Minimum = 0x3};
						enum {ValueMask_Minimum = 0x00000003};
						enum {Value__200Hz = 0x1E};
						enum {ValueMask__200Hz = 0x0000001E};
						};
					};
				}
			}
		}
	}
#endif
