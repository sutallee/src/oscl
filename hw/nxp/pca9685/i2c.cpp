/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

/* The ST LMS9DS1 module actually contains two diffent
	I2C modules.
	- Accelerometer/Gyroscope/Temperature
	- Magnetometer

	Each module has its own I2C addres.
 */

#include "i2c.h"

using namespace Oscl::HW::NXP::PCA9685::I2C;

uint8_t	Oscl::HW::NXP::PCA9685::I2C::getAddress(
			bool a0,
			bool a1,
			bool a2,
			bool a3,
			bool a4,
			bool a5
			) noexcept{
	static const uint8_t	A1	= a0?1:0;
	static const uint8_t	A2	= a1?1:0;
	static const uint8_t	A3	= a2?1:0;
	static const uint8_t	A4	= a3?1:0;
	static const uint8_t	A5	= a4?1:0;
	static const uint8_t	A6	= a5?1:0;
	return (
			(0  << 0)
		|	(A1 << 1)
		|	(A2 << 2)
		|	(A3 << 3)
		|	(A4 << 4)
		|	(A5 << 5)
		|	(A6 << 6)
		|	(1  << 7)
		);
	}
