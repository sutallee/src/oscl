#ifndef _hw_intel_pciregh_
#define _hw_intel_pciregh_
#include <stdint.h>
namespace Oscl { // Namespace description
	namespace IXP4XX { // Namespace description
		namespace PCI { // Namespace description
			namespace CSR { // Namespace description
				namespace PCI_NP_AD { // Register description
					typedef uint32_t	Reg;
					namespace RST { // Field Description
						enum {Lsb = 15};
						enum {FieldMask = 0x00008000};
						enum {Value_Normal = 0x0};
						enum {ValueMask_Normal = 0x00000000};
						enum {Value_Reset = 0x1};
						enum {ValueMask_Reset = 0x00008000};
						};
					};
				};
			};
		};
	};
#endif
