/*
   Copyright (C) 2006 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_hw_intel_ixp4xx_gpioh_
#define _oscl_hw_intel_ixp4xx_gpioh_

#include "gpioreg.h"
/** */
namespace Oscl {
/** */
namespace IXP4XX {
/** */
namespace GPIO {

/**	This is the structure of the IXP4XX GPIO registers.
	Starts at 0xC8004000
 */
struct GpioRegisters {
	/** 0xC8004000 */
	GPOUTR::Reg			gpoutr;
	/** 0xC8004004 */
	GPOER::Reg			gpoer;
	/** 0xC8004008 */
	volatile GPINR::Reg	gpinr;
	/** 0xC800400C */
	volatile GPISR::Reg	gpisr;
	/** 0xC8004010 */
	GPIT1R::Reg			gpit1r;
	/** 0xC8004014 */
	GPIT2R::Reg			gpit2r;
	/** 0xC8004018 */
	GPCLKR::Reg			gpclkr;
	/** 0xC800401C */
	uint32_t	reserved;
	};

}
}
}
#endif
