/*
   Copyright (C) 2005 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/


module hw_intel_expregh {
	include "oscl/compiler/types.h";
	namespace Oscl {
		namespace IXP4XX {
			namespace EXP {
				register TIMING_CSx uint32_t {
					field BYTE_EN 0 1 {
						value ExpansionBus16BitsWide	0
						value ExpansionBus8BitsWide		1
						}
					field WR_EN 1 1 {
						value WritesDisable	0
						value WritesEnable	1
						}
					field SPLT_EN 3 1 {
						value AhbSplitTransfersDisable	0
						value AhbSplitTransfersEnable	1
						}
					field MUX_EN 4 1 {
						value SeparateAddressAndDataBuses	0
						value MultiplexedAddressDataBus		1
						}
					field HRDY_POL 5 1 {
						value PolarityLowTrue	0
						value PolarityHighTrue	1
						}
					field BYTE_RD16 6 1 {
						value ByteAccessEnable	1
						value ByteAccessDisable	0
						}
					field CNFG 10 4 {
						value AddressSpace512Bytes	0x00
						value AddressSpace1KBytes	0x01
						value AddressSpace2KBytes	0x02
						value AddressSpace4KBytes	0x03
						value AddressSpace8KBytes	0x04
						value AddressSpace16KBytes	0x05
						value AddressSpace32KBytes	0x06
						value AddressSpace64KBytes	0x07
						value AddressSpace128KBytes	0x08
						value AddressSpace256KBytes	0x09
						value AddressSpace512KBytes	0x0A
						value AddressSpace1MBytes	0x0B
						value AddressSpace2MBytes	0x0C
						value AddressSpace4MBytes	0x0D
						value AddressSpace8MBytes	0x0E
						value AddressSpace16MBytes	0x0F
						}
					field CYCLE_TYPE 14 2 {
						value IntelCycles		0x00
						value MotorolaCycles	0x01
						value HpiCycles			0x02
						}
					field T5 16 4 {
						value Normal						0x00
						value ExtendRecoveryPhaseBy1Clock	0x01
						value ExtendRecoveryPhaseBy2Clocks	0x02
						value ExtendRecoveryPhaseBy3Clocks	0x03
						value ExtendRecoveryPhaseBy4Clocks	0x04
						value ExtendRecoveryPhaseBy5Clocks	0x05
						value ExtendRecoveryPhaseBy6Clocks	0x06
						value ExtendRecoveryPhaseBy7Clocks	0x07
						value ExtendRecoveryPhaseBy8Clocks	0x08
						value ExtendRecoveryPhaseBy9Clocks	0x09
						value ExtendRecoveryPhaseBy10Clocks	0x0A
						value ExtendRecoveryPhaseBy11Clocks	0x0B
						value ExtendRecoveryPhaseBy12Clocks	0x0C
						value ExtendRecoveryPhaseBy13Clocks	0x0D
						value ExtendRecoveryPhaseBy14Clocks	0x0E
						value ExtendRecoveryPhaseBy15Clocks	0x0F
						}
					field T4 20 2 {
						value Normal						0x00
						value ExtendHoldPhaseBy1Clock		0x01
						value ExtendHoldPhaseBy2Clocks		0x02
						value ExtendHoldPhaseBy3Clocks		0x03
						}
					field T3 22 4 {
						value Normal						0x00
						value ExtendStrobePhaseBy1Clock		0x01
						value ExtendStrobePhaseBy2Clocks	0x02
						value ExtendStrobePhaseBy3Clocks	0x03
						value ExtendStrobePhaseBy4Clocks	0x04
						value ExtendStrobePhaseBy5Clocks	0x05
						value ExtendStrobePhaseBy6Clocks	0x06
						value ExtendStrobePhaseBy7Clocks	0x07
						value ExtendStrobePhaseBy8Clocks	0x08
						value ExtendStrobePhaseBy9Clocks	0x09
						value ExtendStrobePhaseBy10Clocks	0x0A
						value ExtendStrobePhaseBy11Clocks	0x0B
						value ExtendStrobePhaseBy12Clocks	0x0C
						value ExtendStrobePhaseBy13Clocks	0x0D
						value ExtendStrobePhaseBy14Clocks	0x0E
						value ExtendStrobePhaseBy15Clocks	0x0F
						}
					field T2 26 2 {
						value Normal						0x00
						value ExtendSetupPhaseBy1Clock		0x01
						value ExtendSetupPhaseBy2Clocks		0x02
						value ExtendSetupPhaseBy3Clocks		0x03
						}
					field T1 28 2 {
						value Normal						0x00
						value ExtendAddressPhase1Clock		0x01
						value ExtendAddressPhayse2Clocks	0x02
						value ExtendAddressPhayse3Clocks	0x03
						}
					field CSx_EN 31 1 {
						value Enable	1
						value Disable	0
						}
					}
				register CNFG0 uint32_t {
					field FlashBusWidth 0 1 {
						value Width16Bits	0
						value Width8Bits	1
						}
					field PCI_HOST 1 1 {
						value PccControllerNonHost	0
						value PccControllerAsHost	1
						}
					field PCI_ARB 2 1 {
						value Disable	0
						value Enable	1
						}
					field PCI_CLK 4 1 {
						value PciClock33MHz	0
						value PciClock66MHz	1
						}
					field ClockSet 21 3 {
						// My be used to slow core clock speed
						}
					field MEM_MAP 31 1 {
						value LocateAt0x50000000	0
						value LocateAt0x00000000	1
						}
					}
				register CNFG1 uint32_t {
					field SW_INT0 0 1 {
						value GenerateInterrupt	1
						value DisableInterrupt	0
						}
					field SW_INT1 1 1 {
						value GenerateInterrupt	1
						value DisableInterrupt	0
						}
					field BYTE_SWAP_EN 8 1 {
						// "Sets bytes swapping at the Core Gasket"
						value Disable	0
						value Enable	1
						}
					}
				}
			}
		}
	}

