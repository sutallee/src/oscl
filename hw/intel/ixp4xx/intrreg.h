#ifndef _hw_intel_ixp4xx_intrregh_
#define _hw_intel_ixp4xx_intrregh_
#include <stdint.h>
namespace Oscl { // Namespace description
	namespace IXP4XX { // Namespace description
		namespace INTR { // Namespace description
			namespace INTR_ST { // Register description
				typedef uint32_t	Reg;
				enum {Pending = 1};
				enum {Mask = 1};
				namespace WAN_HSS_NPE { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x00000001};
					enum {Value_Pending = 0x1};
					enum {ValueMask_Pending = 0x00000001};
					};
				namespace NPEA { // Field Description
					enum {Lsb = 1};
					enum {FieldMask = 0x00000002};
					enum {Value_Pending = 0x1};
					enum {ValueMask_Pending = 0x00000002};
					};
				namespace NPEB { // Field Description
					enum {Lsb = 2};
					enum {FieldMask = 0x00000004};
					enum {Value_Pending = 0x1};
					enum {ValueMask_Pending = 0x00000004};
					};
				namespace QueueManager1_32 { // Field Description
					enum {Lsb = 3};
					enum {FieldMask = 0x00000008};
					enum {Value_Pending = 0x1};
					enum {ValueMask_Pending = 0x00000008};
					};
				namespace QueueManager33_64 { // Field Description
					enum {Lsb = 4};
					enum {FieldMask = 0x00000010};
					enum {Value_Pending = 0x1};
					enum {ValueMask_Pending = 0x00000010};
					};
				namespace Timers0 { // Field Description
					enum {Lsb = 5};
					enum {FieldMask = 0x00000020};
					enum {Value_Pending = 0x1};
					enum {ValueMask_Pending = 0x00000020};
					};
				namespace GPIO0 { // Field Description
					enum {Lsb = 6};
					enum {FieldMask = 0x00000040};
					enum {Value_Pending = 0x1};
					enum {ValueMask_Pending = 0x00000040};
					};
				namespace GPIO1 { // Field Description
					enum {Lsb = 7};
					enum {FieldMask = 0x00000080};
					enum {Value_Pending = 0x1};
					enum {ValueMask_Pending = 0x00000080};
					};
				namespace PCI { // Field Description
					enum {Lsb = 8};
					enum {FieldMask = 0x00000100};
					enum {Value_Pending = 0x1};
					enum {ValueMask_Pending = 0x00000100};
					};
				namespace PciDmaChannel1 { // Field Description
					enum {Lsb = 9};
					enum {FieldMask = 0x00000200};
					enum {Value_Pending = 0x1};
					enum {ValueMask_Pending = 0x00000200};
					};
				namespace PciDmaChannel2 { // Field Description
					enum {Lsb = 10};
					enum {FieldMask = 0x00000400};
					enum {Value_Pending = 0x1};
					enum {ValueMask_Pending = 0x00000400};
					};
				namespace Timers1 { // Field Description
					enum {Lsb = 11};
					enum {FieldMask = 0x00000800};
					enum {Value_Pending = 0x1};
					enum {ValueMask_Pending = 0x00000800};
					};
				namespace USB { // Field Description
					enum {Lsb = 12};
					enum {FieldMask = 0x00001000};
					enum {Value_Pending = 0x1};
					enum {ValueMask_Pending = 0x00001000};
					};
				namespace ConsoleUART { // Field Description
					enum {Lsb = 13};
					enum {FieldMask = 0x00002000};
					enum {Value_Pending = 0x1};
					enum {ValueMask_Pending = 0x00002000};
					};
				namespace TimestampTimer { // Field Description
					enum {Lsb = 14};
					enum {FieldMask = 0x00004000};
					enum {Value_Pending = 0x1};
					enum {ValueMask_Pending = 0x00004000};
					};
				namespace HighSpeedUART { // Field Description
					enum {Lsb = 15};
					enum {FieldMask = 0x00008000};
					enum {Value_Pending = 0x1};
					enum {ValueMask_Pending = 0x00008000};
					};
				namespace WatchdogTimer { // Field Description
					enum {Lsb = 16};
					enum {FieldMask = 0x00010000};
					enum {Value_Pending = 0x1};
					enum {ValueMask_Pending = 0x00010000};
					};
				namespace PmuCounter { // Field Description
					enum {Lsb = 17};
					enum {FieldMask = 0x00020000};
					enum {Value_Pending = 0x1};
					enum {ValueMask_Pending = 0x00020000};
					};
				namespace XscalePmuCounter { // Field Description
					enum {Lsb = 18};
					enum {FieldMask = 0x00040000};
					enum {Value_Pending = 0x1};
					enum {ValueMask_Pending = 0x00040000};
					};
				namespace GPIO2 { // Field Description
					enum {Lsb = 19};
					enum {FieldMask = 0x00080000};
					enum {Value_Pending = 0x1};
					enum {ValueMask_Pending = 0x00080000};
					};
				namespace GPIO3 { // Field Description
					enum {Lsb = 20};
					enum {FieldMask = 0x00100000};
					enum {Value_Pending = 0x1};
					enum {ValueMask_Pending = 0x00100000};
					};
				namespace GPIO4 { // Field Description
					enum {Lsb = 21};
					enum {FieldMask = 0x00200000};
					enum {Value_Pending = 0x1};
					enum {ValueMask_Pending = 0x00200000};
					};
				namespace GPIO5 { // Field Description
					enum {Lsb = 22};
					enum {FieldMask = 0x00400000};
					enum {Value_Pending = 0x1};
					enum {ValueMask_Pending = 0x00400000};
					};
				namespace GPIO6 { // Field Description
					enum {Lsb = 23};
					enum {FieldMask = 0x00800000};
					enum {Value_Pending = 0x1};
					enum {ValueMask_Pending = 0x00800000};
					};
				namespace GPIO7 { // Field Description
					enum {Lsb = 24};
					enum {FieldMask = 0x01000000};
					enum {Value_Pending = 0x1};
					enum {ValueMask_Pending = 0x01000000};
					};
				namespace GPIO8 { // Field Description
					enum {Lsb = 25};
					enum {FieldMask = 0x02000000};
					enum {Value_Pending = 0x1};
					enum {ValueMask_Pending = 0x02000000};
					};
				namespace GPIO9 { // Field Description
					enum {Lsb = 26};
					enum {FieldMask = 0x04000000};
					enum {Value_Pending = 0x1};
					enum {ValueMask_Pending = 0x04000000};
					};
				namespace GPIO10 { // Field Description
					enum {Lsb = 27};
					enum {FieldMask = 0x08000000};
					enum {Value_Pending = 0x1};
					enum {ValueMask_Pending = 0x08000000};
					};
				namespace GPIO11 { // Field Description
					enum {Lsb = 28};
					enum {FieldMask = 0x10000000};
					enum {Value_Pending = 0x1};
					enum {ValueMask_Pending = 0x10000000};
					};
				namespace GPIO12 { // Field Description
					enum {Lsb = 29};
					enum {FieldMask = 0x20000000};
					enum {Value_Pending = 0x1};
					enum {ValueMask_Pending = 0x20000000};
					};
				namespace SwInterrupt0 { // Field Description
					enum {Lsb = 30};
					enum {FieldMask = 0x40000000};
					enum {Value_Pending = 0x1};
					enum {ValueMask_Pending = 0x40000000};
					};
				namespace SwInterrupt1 { // Field Description
					enum {Lsb = 31};
					enum {FieldMask = 0x80000000};
					enum {Value_Pending = 0x1};
					enum {ValueMask_Pending = 0x80000000};
					};
				};
			namespace INTR_EN { // Register description
				typedef uint32_t	Reg;
				enum {Enable = 1};
				enum {Disable = 0};
				enum {Mask = 1};
				namespace WAN_HSS_NPE { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x00000001};
					enum {Value_Enable = 0x1};
					enum {ValueMask_Enable = 0x00000001};
					enum {Value_Disable = 0x0};
					enum {ValueMask_Disable = 0x00000000};
					};
				namespace NPEA { // Field Description
					enum {Lsb = 1};
					enum {FieldMask = 0x00000002};
					enum {Value_Enable = 0x1};
					enum {ValueMask_Enable = 0x00000002};
					enum {Value_Disable = 0x0};
					enum {ValueMask_Disable = 0x00000000};
					};
				namespace NPEB { // Field Description
					enum {Lsb = 2};
					enum {FieldMask = 0x00000004};
					enum {Value_Enable = 0x1};
					enum {ValueMask_Enable = 0x00000004};
					enum {Value_Disable = 0x0};
					enum {ValueMask_Disable = 0x00000000};
					};
				namespace QueueManager1_32 { // Field Description
					enum {Lsb = 3};
					enum {FieldMask = 0x00000008};
					enum {Value_Enable = 0x1};
					enum {ValueMask_Enable = 0x00000008};
					enum {Value_Disable = 0x0};
					enum {ValueMask_Disable = 0x00000000};
					};
				namespace QueueManager33_64 { // Field Description
					enum {Lsb = 4};
					enum {FieldMask = 0x00000010};
					enum {Value_Enable = 0x1};
					enum {ValueMask_Enable = 0x00000010};
					enum {Value_Disable = 0x0};
					enum {ValueMask_Disable = 0x00000000};
					};
				namespace Timers0 { // Field Description
					enum {Lsb = 5};
					enum {FieldMask = 0x00000020};
					enum {Value_Enable = 0x1};
					enum {ValueMask_Enable = 0x00000020};
					enum {Value_Disable = 0x0};
					enum {ValueMask_Disable = 0x00000000};
					};
				namespace GPIO0 { // Field Description
					enum {Lsb = 6};
					enum {FieldMask = 0x00000040};
					enum {Value_Enable = 0x1};
					enum {ValueMask_Enable = 0x00000040};
					enum {Value_Disable = 0x0};
					enum {ValueMask_Disable = 0x00000000};
					};
				namespace GPIO1 { // Field Description
					enum {Lsb = 7};
					enum {FieldMask = 0x00000080};
					enum {Value_Enable = 0x1};
					enum {ValueMask_Enable = 0x00000080};
					enum {Value_Disable = 0x0};
					enum {ValueMask_Disable = 0x00000000};
					};
				namespace PCI { // Field Description
					enum {Lsb = 8};
					enum {FieldMask = 0x00000100};
					enum {Value_Enable = 0x1};
					enum {ValueMask_Enable = 0x00000100};
					enum {Value_Disable = 0x0};
					enum {ValueMask_Disable = 0x00000000};
					};
				namespace PciDmaChannel1 { // Field Description
					enum {Lsb = 9};
					enum {FieldMask = 0x00000200};
					enum {Value_Enable = 0x1};
					enum {ValueMask_Enable = 0x00000200};
					enum {Value_Disable = 0x0};
					enum {ValueMask_Disable = 0x00000000};
					};
				namespace PciDmaChannel2 { // Field Description
					enum {Lsb = 10};
					enum {FieldMask = 0x00000400};
					enum {Value_Enable = 0x1};
					enum {ValueMask_Enable = 0x00000400};
					enum {Value_Disable = 0x0};
					enum {ValueMask_Disable = 0x00000000};
					};
				namespace Timers1 { // Field Description
					enum {Lsb = 11};
					enum {FieldMask = 0x00000800};
					enum {Value_Enable = 0x1};
					enum {ValueMask_Enable = 0x00000800};
					enum {Value_Disable = 0x0};
					enum {ValueMask_Disable = 0x00000000};
					};
				namespace USB { // Field Description
					enum {Lsb = 12};
					enum {FieldMask = 0x00001000};
					enum {Value_Enable = 0x1};
					enum {ValueMask_Enable = 0x00001000};
					enum {Value_Disable = 0x0};
					enum {ValueMask_Disable = 0x00000000};
					};
				namespace ConsoleUART { // Field Description
					enum {Lsb = 13};
					enum {FieldMask = 0x00002000};
					enum {Value_Enable = 0x1};
					enum {ValueMask_Enable = 0x00002000};
					enum {Value_Disable = 0x0};
					enum {ValueMask_Disable = 0x00000000};
					};
				namespace TimestampTimer { // Field Description
					enum {Lsb = 14};
					enum {FieldMask = 0x00004000};
					enum {Value_Enable = 0x1};
					enum {ValueMask_Enable = 0x00004000};
					enum {Value_Disable = 0x0};
					enum {ValueMask_Disable = 0x00000000};
					};
				namespace HighSpeedUART { // Field Description
					enum {Lsb = 15};
					enum {FieldMask = 0x00008000};
					enum {Value_Enable = 0x1};
					enum {ValueMask_Enable = 0x00008000};
					enum {Value_Disable = 0x0};
					enum {ValueMask_Disable = 0x00000000};
					};
				namespace WatchdogTimer { // Field Description
					enum {Lsb = 16};
					enum {FieldMask = 0x00010000};
					enum {Value_Enable = 0x1};
					enum {ValueMask_Enable = 0x00010000};
					enum {Value_Disable = 0x0};
					enum {ValueMask_Disable = 0x00000000};
					};
				namespace PmuCounter { // Field Description
					enum {Lsb = 17};
					enum {FieldMask = 0x00020000};
					enum {Value_Enable = 0x1};
					enum {ValueMask_Enable = 0x00020000};
					enum {Value_Disable = 0x0};
					enum {ValueMask_Disable = 0x00000000};
					};
				namespace XscalePmuCounter { // Field Description
					enum {Lsb = 18};
					enum {FieldMask = 0x00040000};
					enum {Value_Enable = 0x1};
					enum {ValueMask_Enable = 0x00040000};
					enum {Value_Disable = 0x0};
					enum {ValueMask_Disable = 0x00000000};
					};
				namespace GPIO2 { // Field Description
					enum {Lsb = 19};
					enum {FieldMask = 0x00080000};
					enum {Value_Enable = 0x1};
					enum {ValueMask_Enable = 0x00080000};
					enum {Value_Disable = 0x0};
					enum {ValueMask_Disable = 0x00000000};
					};
				namespace GPIO3 { // Field Description
					enum {Lsb = 20};
					enum {FieldMask = 0x00100000};
					enum {Value_Enable = 0x1};
					enum {ValueMask_Enable = 0x00100000};
					enum {Value_Disable = 0x0};
					enum {ValueMask_Disable = 0x00000000};
					};
				namespace GPIO4 { // Field Description
					enum {Lsb = 21};
					enum {FieldMask = 0x00200000};
					enum {Value_Enable = 0x1};
					enum {ValueMask_Enable = 0x00200000};
					enum {Value_Disable = 0x0};
					enum {ValueMask_Disable = 0x00000000};
					};
				namespace GPIO5 { // Field Description
					enum {Lsb = 22};
					enum {FieldMask = 0x00400000};
					enum {Value_Enable = 0x1};
					enum {ValueMask_Enable = 0x00400000};
					enum {Value_Disable = 0x0};
					enum {ValueMask_Disable = 0x00000000};
					};
				namespace GPIO6 { // Field Description
					enum {Lsb = 23};
					enum {FieldMask = 0x00800000};
					enum {Value_Enable = 0x1};
					enum {ValueMask_Enable = 0x00800000};
					enum {Value_Disable = 0x0};
					enum {ValueMask_Disable = 0x00000000};
					};
				namespace GPIO7 { // Field Description
					enum {Lsb = 24};
					enum {FieldMask = 0x01000000};
					enum {Value_Enable = 0x1};
					enum {ValueMask_Enable = 0x01000000};
					enum {Value_Disable = 0x0};
					enum {ValueMask_Disable = 0x00000000};
					};
				namespace GPIO8 { // Field Description
					enum {Lsb = 25};
					enum {FieldMask = 0x02000000};
					enum {Value_Enable = 0x1};
					enum {ValueMask_Enable = 0x02000000};
					enum {Value_Disable = 0x0};
					enum {ValueMask_Disable = 0x00000000};
					};
				namespace GPIO9 { // Field Description
					enum {Lsb = 26};
					enum {FieldMask = 0x04000000};
					enum {Value_Enable = 0x1};
					enum {ValueMask_Enable = 0x04000000};
					enum {Value_Disable = 0x0};
					enum {ValueMask_Disable = 0x00000000};
					};
				namespace GPIO10 { // Field Description
					enum {Lsb = 27};
					enum {FieldMask = 0x08000000};
					enum {Value_Enable = 0x1};
					enum {ValueMask_Enable = 0x08000000};
					enum {Value_Disable = 0x0};
					enum {ValueMask_Disable = 0x00000000};
					};
				namespace GPIO11 { // Field Description
					enum {Lsb = 28};
					enum {FieldMask = 0x10000000};
					enum {Value_Enable = 0x1};
					enum {ValueMask_Enable = 0x10000000};
					enum {Value_Disable = 0x0};
					enum {ValueMask_Disable = 0x00000000};
					};
				namespace GPIO12 { // Field Description
					enum {Lsb = 29};
					enum {FieldMask = 0x20000000};
					enum {Value_Enable = 0x1};
					enum {ValueMask_Enable = 0x20000000};
					enum {Value_Disable = 0x0};
					enum {ValueMask_Disable = 0x00000000};
					};
				namespace SwInterrupt0 { // Field Description
					enum {Lsb = 30};
					enum {FieldMask = 0x40000000};
					enum {Value_Enable = 0x1};
					enum {ValueMask_Enable = 0x40000000};
					enum {Value_Disable = 0x0};
					enum {ValueMask_Disable = 0x00000000};
					};
				namespace SwInterrupt1 { // Field Description
					enum {Lsb = 31};
					enum {FieldMask = 0x80000000};
					enum {Value_Enable = 0x1};
					enum {ValueMask_Enable = 0x80000000};
					enum {Value_Disable = 0x0};
					enum {ValueMask_Disable = 0x00000000};
					};
				};
			namespace INTR_SEL { // Register description
				typedef uint32_t	Reg;
				enum {SelectIRQ = 0};
				enum {SelectFIQ = 1};
				enum {Mask = 1};
				namespace WAN_HSS_NPE { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x00000001};
					enum {Value_SelectIRQ = 0x0};
					enum {ValueMask_SelectIRQ = 0x00000000};
					enum {Value_SelectFIQ = 0x1};
					enum {ValueMask_SelectFIQ = 0x00000001};
					};
				namespace NPEA { // Field Description
					enum {Lsb = 1};
					enum {FieldMask = 0x00000002};
					enum {Value_SelectIRQ = 0x0};
					enum {ValueMask_SelectIRQ = 0x00000000};
					enum {Value_SelectFIQ = 0x1};
					enum {ValueMask_SelectFIQ = 0x00000002};
					};
				namespace NPEB { // Field Description
					enum {Lsb = 2};
					enum {FieldMask = 0x00000004};
					enum {Value_SelectIRQ = 0x0};
					enum {ValueMask_SelectIRQ = 0x00000000};
					enum {Value_SelectFIQ = 0x1};
					enum {ValueMask_SelectFIQ = 0x00000004};
					};
				namespace QueueManager1_32 { // Field Description
					enum {Lsb = 3};
					enum {FieldMask = 0x00000008};
					enum {Value_SelectIRQ = 0x0};
					enum {ValueMask_SelectIRQ = 0x00000000};
					enum {Value_SelectFIQ = 0x1};
					enum {ValueMask_SelectFIQ = 0x00000008};
					};
				namespace QueueManager33_64 { // Field Description
					enum {Lsb = 4};
					enum {FieldMask = 0x00000010};
					enum {Value_SelectIRQ = 0x0};
					enum {ValueMask_SelectIRQ = 0x00000000};
					enum {Value_SelectFIQ = 0x1};
					enum {ValueMask_SelectFIQ = 0x00000010};
					};
				namespace Timers0 { // Field Description
					enum {Lsb = 5};
					enum {FieldMask = 0x00000020};
					enum {Value_SelectIRQ = 0x0};
					enum {ValueMask_SelectIRQ = 0x00000000};
					enum {Value_SelectFIQ = 0x1};
					enum {ValueMask_SelectFIQ = 0x00000020};
					};
				namespace GPIO0 { // Field Description
					enum {Lsb = 6};
					enum {FieldMask = 0x00000040};
					enum {Value_SelectIRQ = 0x0};
					enum {ValueMask_SelectIRQ = 0x00000000};
					enum {Value_SelectFIQ = 0x1};
					enum {ValueMask_SelectFIQ = 0x00000040};
					};
				namespace GPIO1 { // Field Description
					enum {Lsb = 7};
					enum {FieldMask = 0x00000080};
					enum {Value_SelectIRQ = 0x0};
					enum {ValueMask_SelectIRQ = 0x00000000};
					enum {Value_SelectFIQ = 0x1};
					enum {ValueMask_SelectFIQ = 0x00000080};
					};
				namespace PCI { // Field Description
					enum {Lsb = 8};
					enum {FieldMask = 0x00000100};
					enum {Value_SelectIRQ = 0x0};
					enum {ValueMask_SelectIRQ = 0x00000000};
					enum {Value_SelectFIQ = 0x1};
					enum {ValueMask_SelectFIQ = 0x00000100};
					};
				namespace PciDmaChannel1 { // Field Description
					enum {Lsb = 9};
					enum {FieldMask = 0x00000200};
					enum {Value_SelectIRQ = 0x0};
					enum {ValueMask_SelectIRQ = 0x00000000};
					enum {Value_SelectFIQ = 0x1};
					enum {ValueMask_SelectFIQ = 0x00000200};
					};
				namespace PciDmaChannel2 { // Field Description
					enum {Lsb = 10};
					enum {FieldMask = 0x00000400};
					enum {Value_SelectIRQ = 0x0};
					enum {ValueMask_SelectIRQ = 0x00000000};
					enum {Value_SelectFIQ = 0x1};
					enum {ValueMask_SelectFIQ = 0x00000400};
					};
				namespace Timers1 { // Field Description
					enum {Lsb = 11};
					enum {FieldMask = 0x00000800};
					enum {Value_SelectIRQ = 0x0};
					enum {ValueMask_SelectIRQ = 0x00000000};
					enum {Value_SelectFIQ = 0x1};
					enum {ValueMask_SelectFIQ = 0x00000800};
					};
				namespace USB { // Field Description
					enum {Lsb = 12};
					enum {FieldMask = 0x00001000};
					enum {Value_SelectIRQ = 0x0};
					enum {ValueMask_SelectIRQ = 0x00000000};
					enum {Value_SelectFIQ = 0x1};
					enum {ValueMask_SelectFIQ = 0x00001000};
					};
				namespace ConsoleUART { // Field Description
					enum {Lsb = 13};
					enum {FieldMask = 0x00002000};
					enum {Value_SelectIRQ = 0x0};
					enum {ValueMask_SelectIRQ = 0x00000000};
					enum {Value_SelectFIQ = 0x1};
					enum {ValueMask_SelectFIQ = 0x00002000};
					};
				namespace TimestampTimer { // Field Description
					enum {Lsb = 14};
					enum {FieldMask = 0x00004000};
					enum {Value_SelectIRQ = 0x0};
					enum {ValueMask_SelectIRQ = 0x00000000};
					enum {Value_SelectFIQ = 0x1};
					enum {ValueMask_SelectFIQ = 0x00004000};
					};
				namespace HighSpeedUART { // Field Description
					enum {Lsb = 15};
					enum {FieldMask = 0x00008000};
					enum {Value_SelectIRQ = 0x0};
					enum {ValueMask_SelectIRQ = 0x00000000};
					enum {Value_SelectFIQ = 0x1};
					enum {ValueMask_SelectFIQ = 0x00008000};
					};
				namespace WatchdogTimer { // Field Description
					enum {Lsb = 16};
					enum {FieldMask = 0x00010000};
					enum {Value_SelectIRQ = 0x0};
					enum {ValueMask_SelectIRQ = 0x00000000};
					enum {Value_SelectFIQ = 0x1};
					enum {ValueMask_SelectFIQ = 0x00010000};
					};
				namespace PmuCounter { // Field Description
					enum {Lsb = 17};
					enum {FieldMask = 0x00020000};
					enum {Value_SelectIRQ = 0x0};
					enum {ValueMask_SelectIRQ = 0x00000000};
					enum {Value_SelectFIQ = 0x1};
					enum {ValueMask_SelectFIQ = 0x00020000};
					};
				namespace XscalePmuCounter { // Field Description
					enum {Lsb = 18};
					enum {FieldMask = 0x00040000};
					enum {Value_SelectIRQ = 0x0};
					enum {ValueMask_SelectIRQ = 0x00000000};
					enum {Value_SelectFIQ = 0x1};
					enum {ValueMask_SelectFIQ = 0x00040000};
					};
				namespace GPIO2 { // Field Description
					enum {Lsb = 19};
					enum {FieldMask = 0x00080000};
					enum {Value_SelectIRQ = 0x0};
					enum {ValueMask_SelectIRQ = 0x00000000};
					enum {Value_SelectFIQ = 0x1};
					enum {ValueMask_SelectFIQ = 0x00080000};
					};
				namespace GPIO3 { // Field Description
					enum {Lsb = 20};
					enum {FieldMask = 0x00100000};
					enum {Value_SelectIRQ = 0x0};
					enum {ValueMask_SelectIRQ = 0x00000000};
					enum {Value_SelectFIQ = 0x1};
					enum {ValueMask_SelectFIQ = 0x00100000};
					};
				namespace GPIO4 { // Field Description
					enum {Lsb = 21};
					enum {FieldMask = 0x00200000};
					enum {Value_SelectIRQ = 0x0};
					enum {ValueMask_SelectIRQ = 0x00000000};
					enum {Value_SelectFIQ = 0x1};
					enum {ValueMask_SelectFIQ = 0x00200000};
					};
				namespace GPIO5 { // Field Description
					enum {Lsb = 22};
					enum {FieldMask = 0x00400000};
					enum {Value_SelectIRQ = 0x0};
					enum {ValueMask_SelectIRQ = 0x00000000};
					enum {Value_SelectFIQ = 0x1};
					enum {ValueMask_SelectFIQ = 0x00400000};
					};
				namespace GPIO6 { // Field Description
					enum {Lsb = 23};
					enum {FieldMask = 0x00800000};
					enum {Value_SelectIRQ = 0x0};
					enum {ValueMask_SelectIRQ = 0x00000000};
					enum {Value_SelectFIQ = 0x1};
					enum {ValueMask_SelectFIQ = 0x00800000};
					};
				namespace GPIO7 { // Field Description
					enum {Lsb = 24};
					enum {FieldMask = 0x01000000};
					enum {Value_SelectIRQ = 0x0};
					enum {ValueMask_SelectIRQ = 0x00000000};
					enum {Value_SelectFIQ = 0x1};
					enum {ValueMask_SelectFIQ = 0x01000000};
					};
				namespace GPIO8 { // Field Description
					enum {Lsb = 25};
					enum {FieldMask = 0x02000000};
					enum {Value_SelectIRQ = 0x0};
					enum {ValueMask_SelectIRQ = 0x00000000};
					enum {Value_SelectFIQ = 0x1};
					enum {ValueMask_SelectFIQ = 0x02000000};
					};
				namespace GPIO9 { // Field Description
					enum {Lsb = 26};
					enum {FieldMask = 0x04000000};
					enum {Value_SelectIRQ = 0x0};
					enum {ValueMask_SelectIRQ = 0x00000000};
					enum {Value_SelectFIQ = 0x1};
					enum {ValueMask_SelectFIQ = 0x04000000};
					};
				namespace GPIO10 { // Field Description
					enum {Lsb = 27};
					enum {FieldMask = 0x08000000};
					enum {Value_SelectIRQ = 0x0};
					enum {ValueMask_SelectIRQ = 0x00000000};
					enum {Value_SelectFIQ = 0x1};
					enum {ValueMask_SelectFIQ = 0x08000000};
					};
				namespace GPIO11 { // Field Description
					enum {Lsb = 28};
					enum {FieldMask = 0x10000000};
					enum {Value_SelectIRQ = 0x0};
					enum {ValueMask_SelectIRQ = 0x00000000};
					enum {Value_SelectFIQ = 0x1};
					enum {ValueMask_SelectFIQ = 0x10000000};
					};
				namespace GPIO12 { // Field Description
					enum {Lsb = 29};
					enum {FieldMask = 0x20000000};
					enum {Value_SelectIRQ = 0x0};
					enum {ValueMask_SelectIRQ = 0x00000000};
					enum {Value_SelectFIQ = 0x1};
					enum {ValueMask_SelectFIQ = 0x20000000};
					};
				namespace SwInterrupt0 { // Field Description
					enum {Lsb = 30};
					enum {FieldMask = 0x40000000};
					enum {Value_SelectIRQ = 0x0};
					enum {ValueMask_SelectIRQ = 0x00000000};
					enum {Value_SelectFIQ = 0x1};
					enum {ValueMask_SelectFIQ = 0x40000000};
					};
				namespace SwInterrupt1 { // Field Description
					enum {Lsb = 31};
					enum {FieldMask = 0x80000000};
					enum {Value_SelectIRQ = 0x0};
					enum {ValueMask_SelectIRQ = 0x00000000};
					enum {Value_SelectFIQ = 0x1};
					enum {ValueMask_SelectFIQ = 0x80000000};
					};
				};
			namespace INTR_IRQ_ST { // Register description
				typedef uint32_t	Reg;
				enum {Pending = 1};
				enum {Mask = 1};
				namespace WAN_HSS_NPE { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x00000001};
					enum {Value_Pending = 0x1};
					enum {ValueMask_Pending = 0x00000001};
					};
				namespace NPEA { // Field Description
					enum {Lsb = 1};
					enum {FieldMask = 0x00000002};
					enum {Value_Pending = 0x1};
					enum {ValueMask_Pending = 0x00000002};
					};
				namespace NPEB { // Field Description
					enum {Lsb = 2};
					enum {FieldMask = 0x00000004};
					enum {Value_Pending = 0x1};
					enum {ValueMask_Pending = 0x00000004};
					};
				namespace QueueManager1_32 { // Field Description
					enum {Lsb = 3};
					enum {FieldMask = 0x00000008};
					enum {Value_Pending = 0x1};
					enum {ValueMask_Pending = 0x00000008};
					};
				namespace QueueManager33_64 { // Field Description
					enum {Lsb = 4};
					enum {FieldMask = 0x00000010};
					enum {Value_Pending = 0x1};
					enum {ValueMask_Pending = 0x00000010};
					};
				namespace Timers0 { // Field Description
					enum {Lsb = 5};
					enum {FieldMask = 0x00000020};
					enum {Value_Pending = 0x1};
					enum {ValueMask_Pending = 0x00000020};
					};
				namespace GPIO0 { // Field Description
					enum {Lsb = 6};
					enum {FieldMask = 0x00000040};
					enum {Value_Pending = 0x1};
					enum {ValueMask_Pending = 0x00000040};
					};
				namespace GPIO1 { // Field Description
					enum {Lsb = 7};
					enum {FieldMask = 0x00000080};
					enum {Value_Pending = 0x1};
					enum {ValueMask_Pending = 0x00000080};
					};
				namespace PCI { // Field Description
					enum {Lsb = 8};
					enum {FieldMask = 0x00000100};
					enum {Value_Pending = 0x1};
					enum {ValueMask_Pending = 0x00000100};
					};
				namespace PciDmaChannel1 { // Field Description
					enum {Lsb = 9};
					enum {FieldMask = 0x00000200};
					enum {Value_Pending = 0x1};
					enum {ValueMask_Pending = 0x00000200};
					};
				namespace PciDmaChannel2 { // Field Description
					enum {Lsb = 10};
					enum {FieldMask = 0x00000400};
					enum {Value_Pending = 0x1};
					enum {ValueMask_Pending = 0x00000400};
					};
				namespace Timers1 { // Field Description
					enum {Lsb = 11};
					enum {FieldMask = 0x00000800};
					enum {Value_Pending = 0x1};
					enum {ValueMask_Pending = 0x00000800};
					};
				namespace USB { // Field Description
					enum {Lsb = 12};
					enum {FieldMask = 0x00001000};
					enum {Value_Pending = 0x1};
					enum {ValueMask_Pending = 0x00001000};
					};
				namespace ConsoleUART { // Field Description
					enum {Lsb = 13};
					enum {FieldMask = 0x00002000};
					enum {Value_Pending = 0x1};
					enum {ValueMask_Pending = 0x00002000};
					};
				namespace TimestampTimer { // Field Description
					enum {Lsb = 14};
					enum {FieldMask = 0x00004000};
					enum {Value_Pending = 0x1};
					enum {ValueMask_Pending = 0x00004000};
					};
				namespace HighSpeedUART { // Field Description
					enum {Lsb = 15};
					enum {FieldMask = 0x00008000};
					enum {Value_Pending = 0x1};
					enum {ValueMask_Pending = 0x00008000};
					};
				namespace WatchdogTimer { // Field Description
					enum {Lsb = 16};
					enum {FieldMask = 0x00010000};
					enum {Value_Pending = 0x1};
					enum {ValueMask_Pending = 0x00010000};
					};
				namespace PmuCounter { // Field Description
					enum {Lsb = 17};
					enum {FieldMask = 0x00020000};
					enum {Value_Pending = 0x1};
					enum {ValueMask_Pending = 0x00020000};
					};
				namespace XscalePmuCounter { // Field Description
					enum {Lsb = 18};
					enum {FieldMask = 0x00040000};
					enum {Value_Pending = 0x1};
					enum {ValueMask_Pending = 0x00040000};
					};
				namespace GPIO2 { // Field Description
					enum {Lsb = 19};
					enum {FieldMask = 0x00080000};
					enum {Value_Pending = 0x1};
					enum {ValueMask_Pending = 0x00080000};
					};
				namespace GPIO3 { // Field Description
					enum {Lsb = 20};
					enum {FieldMask = 0x00100000};
					enum {Value_Pending = 0x1};
					enum {ValueMask_Pending = 0x00100000};
					};
				namespace GPIO4 { // Field Description
					enum {Lsb = 21};
					enum {FieldMask = 0x00200000};
					enum {Value_Pending = 0x1};
					enum {ValueMask_Pending = 0x00200000};
					};
				namespace GPIO5 { // Field Description
					enum {Lsb = 22};
					enum {FieldMask = 0x00400000};
					enum {Value_Pending = 0x1};
					enum {ValueMask_Pending = 0x00400000};
					};
				namespace GPIO6 { // Field Description
					enum {Lsb = 23};
					enum {FieldMask = 0x00800000};
					enum {Value_Pending = 0x1};
					enum {ValueMask_Pending = 0x00800000};
					};
				namespace GPIO7 { // Field Description
					enum {Lsb = 24};
					enum {FieldMask = 0x01000000};
					enum {Value_Pending = 0x1};
					enum {ValueMask_Pending = 0x01000000};
					};
				namespace GPIO8 { // Field Description
					enum {Lsb = 25};
					enum {FieldMask = 0x02000000};
					enum {Value_Pending = 0x1};
					enum {ValueMask_Pending = 0x02000000};
					};
				namespace GPIO9 { // Field Description
					enum {Lsb = 26};
					enum {FieldMask = 0x04000000};
					enum {Value_Pending = 0x1};
					enum {ValueMask_Pending = 0x04000000};
					};
				namespace GPIO10 { // Field Description
					enum {Lsb = 27};
					enum {FieldMask = 0x08000000};
					enum {Value_Pending = 0x1};
					enum {ValueMask_Pending = 0x08000000};
					};
				namespace GPIO11 { // Field Description
					enum {Lsb = 28};
					enum {FieldMask = 0x10000000};
					enum {Value_Pending = 0x1};
					enum {ValueMask_Pending = 0x10000000};
					};
				namespace GPIO12 { // Field Description
					enum {Lsb = 29};
					enum {FieldMask = 0x20000000};
					enum {Value_Pending = 0x1};
					enum {ValueMask_Pending = 0x20000000};
					};
				namespace SwInterrupt0 { // Field Description
					enum {Lsb = 30};
					enum {FieldMask = 0x40000000};
					enum {Value_Pending = 0x1};
					enum {ValueMask_Pending = 0x40000000};
					};
				namespace SwInterrupt1 { // Field Description
					enum {Lsb = 31};
					enum {FieldMask = 0x80000000};
					enum {Value_Pending = 0x1};
					enum {ValueMask_Pending = 0x80000000};
					};
				};
			namespace INTR_FIQ_ST { // Register description
				typedef uint32_t	Reg;
				enum {Pending = 1};
				enum {Mask = 1};
				namespace WAN_HSS_NPE { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x00000001};
					enum {Value_Pending = 0x1};
					enum {ValueMask_Pending = 0x00000001};
					};
				namespace NPEA { // Field Description
					enum {Lsb = 1};
					enum {FieldMask = 0x00000002};
					enum {Value_Pending = 0x1};
					enum {ValueMask_Pending = 0x00000002};
					};
				namespace NPEB { // Field Description
					enum {Lsb = 2};
					enum {FieldMask = 0x00000004};
					enum {Value_Pending = 0x1};
					enum {ValueMask_Pending = 0x00000004};
					};
				namespace QueueManager1_32 { // Field Description
					enum {Lsb = 3};
					enum {FieldMask = 0x00000008};
					enum {Value_Pending = 0x1};
					enum {ValueMask_Pending = 0x00000008};
					};
				namespace QueueManager33_64 { // Field Description
					enum {Lsb = 4};
					enum {FieldMask = 0x00000010};
					enum {Value_Pending = 0x1};
					enum {ValueMask_Pending = 0x00000010};
					};
				namespace Timers0 { // Field Description
					enum {Lsb = 5};
					enum {FieldMask = 0x00000020};
					enum {Value_Pending = 0x1};
					enum {ValueMask_Pending = 0x00000020};
					};
				namespace GPIO0 { // Field Description
					enum {Lsb = 6};
					enum {FieldMask = 0x00000040};
					enum {Value_Pending = 0x1};
					enum {ValueMask_Pending = 0x00000040};
					};
				namespace GPIO1 { // Field Description
					enum {Lsb = 7};
					enum {FieldMask = 0x00000080};
					enum {Value_Pending = 0x1};
					enum {ValueMask_Pending = 0x00000080};
					};
				namespace PCI { // Field Description
					enum {Lsb = 8};
					enum {FieldMask = 0x00000100};
					enum {Value_Pending = 0x1};
					enum {ValueMask_Pending = 0x00000100};
					};
				namespace PciDmaChannel1 { // Field Description
					enum {Lsb = 9};
					enum {FieldMask = 0x00000200};
					enum {Value_Pending = 0x1};
					enum {ValueMask_Pending = 0x00000200};
					};
				namespace PciDmaChannel2 { // Field Description
					enum {Lsb = 10};
					enum {FieldMask = 0x00000400};
					enum {Value_Pending = 0x1};
					enum {ValueMask_Pending = 0x00000400};
					};
				namespace Timers1 { // Field Description
					enum {Lsb = 11};
					enum {FieldMask = 0x00000800};
					enum {Value_Pending = 0x1};
					enum {ValueMask_Pending = 0x00000800};
					};
				namespace USB { // Field Description
					enum {Lsb = 12};
					enum {FieldMask = 0x00001000};
					enum {Value_Pending = 0x1};
					enum {ValueMask_Pending = 0x00001000};
					};
				namespace ConsoleUART { // Field Description
					enum {Lsb = 13};
					enum {FieldMask = 0x00002000};
					enum {Value_Pending = 0x1};
					enum {ValueMask_Pending = 0x00002000};
					};
				namespace TimestampTimer { // Field Description
					enum {Lsb = 14};
					enum {FieldMask = 0x00004000};
					enum {Value_Pending = 0x1};
					enum {ValueMask_Pending = 0x00004000};
					};
				namespace HighSpeedUART { // Field Description
					enum {Lsb = 15};
					enum {FieldMask = 0x00008000};
					enum {Value_Pending = 0x1};
					enum {ValueMask_Pending = 0x00008000};
					};
				namespace WatchdogTimer { // Field Description
					enum {Lsb = 16};
					enum {FieldMask = 0x00010000};
					enum {Value_Pending = 0x1};
					enum {ValueMask_Pending = 0x00010000};
					};
				namespace PmuCounter { // Field Description
					enum {Lsb = 17};
					enum {FieldMask = 0x00020000};
					enum {Value_Pending = 0x1};
					enum {ValueMask_Pending = 0x00020000};
					};
				namespace XscalePmuCounter { // Field Description
					enum {Lsb = 18};
					enum {FieldMask = 0x00040000};
					enum {Value_Pending = 0x1};
					enum {ValueMask_Pending = 0x00040000};
					};
				namespace GPIO2 { // Field Description
					enum {Lsb = 19};
					enum {FieldMask = 0x00080000};
					enum {Value_Pending = 0x1};
					enum {ValueMask_Pending = 0x00080000};
					};
				namespace GPIO3 { // Field Description
					enum {Lsb = 20};
					enum {FieldMask = 0x00100000};
					enum {Value_Pending = 0x1};
					enum {ValueMask_Pending = 0x00100000};
					};
				namespace GPIO4 { // Field Description
					enum {Lsb = 21};
					enum {FieldMask = 0x00200000};
					enum {Value_Pending = 0x1};
					enum {ValueMask_Pending = 0x00200000};
					};
				namespace GPIO5 { // Field Description
					enum {Lsb = 22};
					enum {FieldMask = 0x00400000};
					enum {Value_Pending = 0x1};
					enum {ValueMask_Pending = 0x00400000};
					};
				namespace GPIO6 { // Field Description
					enum {Lsb = 23};
					enum {FieldMask = 0x00800000};
					enum {Value_Pending = 0x1};
					enum {ValueMask_Pending = 0x00800000};
					};
				namespace GPIO7 { // Field Description
					enum {Lsb = 24};
					enum {FieldMask = 0x01000000};
					enum {Value_Pending = 0x1};
					enum {ValueMask_Pending = 0x01000000};
					};
				namespace GPIO8 { // Field Description
					enum {Lsb = 25};
					enum {FieldMask = 0x02000000};
					enum {Value_Pending = 0x1};
					enum {ValueMask_Pending = 0x02000000};
					};
				namespace GPIO9 { // Field Description
					enum {Lsb = 26};
					enum {FieldMask = 0x04000000};
					enum {Value_Pending = 0x1};
					enum {ValueMask_Pending = 0x04000000};
					};
				namespace GPIO10 { // Field Description
					enum {Lsb = 27};
					enum {FieldMask = 0x08000000};
					enum {Value_Pending = 0x1};
					enum {ValueMask_Pending = 0x08000000};
					};
				namespace GPIO11 { // Field Description
					enum {Lsb = 28};
					enum {FieldMask = 0x10000000};
					enum {Value_Pending = 0x1};
					enum {ValueMask_Pending = 0x10000000};
					};
				namespace GPIO12 { // Field Description
					enum {Lsb = 29};
					enum {FieldMask = 0x20000000};
					enum {Value_Pending = 0x1};
					enum {ValueMask_Pending = 0x20000000};
					};
				namespace SwInterrupt0 { // Field Description
					enum {Lsb = 30};
					enum {FieldMask = 0x40000000};
					enum {Value_Pending = 0x1};
					enum {ValueMask_Pending = 0x40000000};
					};
				namespace SwInterrupt1 { // Field Description
					enum {Lsb = 31};
					enum {FieldMask = 0x80000000};
					enum {Value_Pending = 0x1};
					enum {ValueMask_Pending = 0x80000000};
					};
				};
			namespace INTR_PRTY { // Register description
				typedef uint32_t	Reg;
				namespace Prior_Intbus7 { // Field Description
					enum {Lsb = 21};
					enum {FieldMask = 0x00E00000};
					enum {Value_Priority0 = 0x0};
					enum {ValueMask_Priority0 = 0x00000000};
					enum {Value_Priority1 = 0x1};
					enum {ValueMask_Priority1 = 0x00200000};
					enum {Value_Priority2 = 0x2};
					enum {ValueMask_Priority2 = 0x00400000};
					enum {Value_Priority3 = 0x3};
					enum {ValueMask_Priority3 = 0x00600000};
					enum {Value_Priority4 = 0x4};
					enum {ValueMask_Priority4 = 0x00800000};
					enum {Value_Priority5 = 0x5};
					enum {ValueMask_Priority5 = 0x00A00000};
					enum {Value_Priority6 = 0x6};
					enum {ValueMask_Priority6 = 0x00C00000};
					enum {Value_Priority7 = 0x7};
					enum {ValueMask_Priority7 = 0x00E00000};
					};
				namespace Prior_Intbus6 { // Field Description
					enum {Lsb = 18};
					enum {FieldMask = 0x001C0000};
					enum {Value_Priority0 = 0x0};
					enum {ValueMask_Priority0 = 0x00000000};
					enum {Value_Priority1 = 0x1};
					enum {ValueMask_Priority1 = 0x00040000};
					enum {Value_Priority2 = 0x2};
					enum {ValueMask_Priority2 = 0x00080000};
					enum {Value_Priority3 = 0x3};
					enum {ValueMask_Priority3 = 0x000C0000};
					enum {Value_Priority4 = 0x4};
					enum {ValueMask_Priority4 = 0x00100000};
					enum {Value_Priority5 = 0x5};
					enum {ValueMask_Priority5 = 0x00140000};
					enum {Value_Priority6 = 0x6};
					enum {ValueMask_Priority6 = 0x00180000};
					enum {Value_Priority7 = 0x7};
					enum {ValueMask_Priority7 = 0x001C0000};
					};
				namespace Prior_Intbus5 { // Field Description
					enum {Lsb = 15};
					enum {FieldMask = 0x00038000};
					enum {Value_Priority0 = 0x0};
					enum {ValueMask_Priority0 = 0x00000000};
					enum {Value_Priority1 = 0x1};
					enum {ValueMask_Priority1 = 0x00008000};
					enum {Value_Priority2 = 0x2};
					enum {ValueMask_Priority2 = 0x00010000};
					enum {Value_Priority3 = 0x3};
					enum {ValueMask_Priority3 = 0x00018000};
					enum {Value_Priority4 = 0x4};
					enum {ValueMask_Priority4 = 0x00020000};
					enum {Value_Priority5 = 0x5};
					enum {ValueMask_Priority5 = 0x00028000};
					enum {Value_Priority6 = 0x6};
					enum {ValueMask_Priority6 = 0x00030000};
					enum {Value_Priority7 = 0x7};
					enum {ValueMask_Priority7 = 0x00038000};
					};
				namespace Prior_Intbus4 { // Field Description
					enum {Lsb = 12};
					enum {FieldMask = 0x00007000};
					enum {Value_Priority0 = 0x0};
					enum {ValueMask_Priority0 = 0x00000000};
					enum {Value_Priority1 = 0x1};
					enum {ValueMask_Priority1 = 0x00001000};
					enum {Value_Priority2 = 0x2};
					enum {ValueMask_Priority2 = 0x00002000};
					enum {Value_Priority3 = 0x3};
					enum {ValueMask_Priority3 = 0x00003000};
					enum {Value_Priority4 = 0x4};
					enum {ValueMask_Priority4 = 0x00004000};
					enum {Value_Priority5 = 0x5};
					enum {ValueMask_Priority5 = 0x00005000};
					enum {Value_Priority6 = 0x6};
					enum {ValueMask_Priority6 = 0x00006000};
					enum {Value_Priority7 = 0x7};
					enum {ValueMask_Priority7 = 0x00007000};
					};
				namespace Prior_Intbus3 { // Field Description
					enum {Lsb = 9};
					enum {FieldMask = 0x00000E00};
					enum {Value_Priority0 = 0x0};
					enum {ValueMask_Priority0 = 0x00000000};
					enum {Value_Priority1 = 0x1};
					enum {ValueMask_Priority1 = 0x00000200};
					enum {Value_Priority2 = 0x2};
					enum {ValueMask_Priority2 = 0x00000400};
					enum {Value_Priority3 = 0x3};
					enum {ValueMask_Priority3 = 0x00000600};
					enum {Value_Priority4 = 0x4};
					enum {ValueMask_Priority4 = 0x00000800};
					enum {Value_Priority5 = 0x5};
					enum {ValueMask_Priority5 = 0x00000A00};
					enum {Value_Priority6 = 0x6};
					enum {ValueMask_Priority6 = 0x00000C00};
					enum {Value_Priority7 = 0x7};
					enum {ValueMask_Priority7 = 0x00000E00};
					};
				namespace Prior_Intbus2 { // Field Description
					enum {Lsb = 6};
					enum {FieldMask = 0x000001C0};
					enum {Value_Priority0 = 0x0};
					enum {ValueMask_Priority0 = 0x00000000};
					enum {Value_Priority1 = 0x1};
					enum {ValueMask_Priority1 = 0x00000040};
					enum {Value_Priority2 = 0x2};
					enum {ValueMask_Priority2 = 0x00000080};
					enum {Value_Priority3 = 0x3};
					enum {ValueMask_Priority3 = 0x000000C0};
					enum {Value_Priority4 = 0x4};
					enum {ValueMask_Priority4 = 0x00000100};
					enum {Value_Priority5 = 0x5};
					enum {ValueMask_Priority5 = 0x00000140};
					enum {Value_Priority6 = 0x6};
					enum {ValueMask_Priority6 = 0x00000180};
					enum {Value_Priority7 = 0x7};
					enum {ValueMask_Priority7 = 0x000001C0};
					};
				namespace Prior_Intbus1 { // Field Description
					enum {Lsb = 3};
					enum {FieldMask = 0x00000038};
					enum {Value_Priority0 = 0x0};
					enum {ValueMask_Priority0 = 0x00000000};
					enum {Value_Priority1 = 0x1};
					enum {ValueMask_Priority1 = 0x00000008};
					enum {Value_Priority2 = 0x2};
					enum {ValueMask_Priority2 = 0x00000010};
					enum {Value_Priority3 = 0x3};
					enum {ValueMask_Priority3 = 0x00000018};
					enum {Value_Priority4 = 0x4};
					enum {ValueMask_Priority4 = 0x00000020};
					enum {Value_Priority5 = 0x5};
					enum {ValueMask_Priority5 = 0x00000028};
					enum {Value_Priority6 = 0x6};
					enum {ValueMask_Priority6 = 0x00000030};
					enum {Value_Priority7 = 0x7};
					enum {ValueMask_Priority7 = 0x00000038};
					};
				namespace Prior_Intbus0 { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x00000007};
					enum {Value_Priority0 = 0x0};
					enum {ValueMask_Priority0 = 0x00000000};
					enum {Value_Priority1 = 0x1};
					enum {ValueMask_Priority1 = 0x00000001};
					enum {Value_Priority2 = 0x2};
					enum {ValueMask_Priority2 = 0x00000002};
					enum {Value_Priority3 = 0x3};
					enum {ValueMask_Priority3 = 0x00000003};
					enum {Value_Priority4 = 0x4};
					enum {ValueMask_Priority4 = 0x00000004};
					enum {Value_Priority5 = 0x5};
					enum {ValueMask_Priority5 = 0x00000005};
					enum {Value_Priority6 = 0x6};
					enum {ValueMask_Priority6 = 0x00000006};
					enum {Value_Priority7 = 0x7};
					enum {ValueMask_Priority7 = 0x00000007};
					};
				};
			namespace INTR_ENC_ST { // Register description
				typedef uint32_t	Reg;
				namespace ENC_ST { // Field Description
					enum {Lsb = 2};
					enum {FieldMask = 0x000000FC};
					};
				namespace RES { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x00000003};
					};
				};
			};
		};
	};
#endif
