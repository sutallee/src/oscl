#ifndef _hw_intel_ixp4xx_gpioregh_
#define _hw_intel_ixp4xx_gpioregh_
#include <stdint.h>
namespace Oscl { // Namespace description
	namespace IXP4XX { // Namespace description
		namespace GPIO { // Namespace description
			namespace GPOUTR { // Register description
				typedef uint32_t	Reg;
				enum {High = 1};
				enum {Low = 0};
				enum {Mask = 1};
				};
			namespace GPOER { // Register description
				typedef uint32_t	Reg;
				enum {TriState = 1};
				enum {Driven = 0};
				enum {Mask = 1};
				};
			namespace GPINR { // Register description
				typedef uint32_t	Reg;
				enum {High = 1};
				enum {Low = 0};
				enum {Mask = 1};
				};
			namespace GPISR { // Register description
				typedef uint32_t	Reg;
				enum {Clear = 1};
				enum {Pending = 1};
				enum {NotPending = 0};
				enum {Mask = 1};
				};
			namespace GPIT1R { // Register description
				typedef uint32_t	Reg;
				enum {ActiveHigh = 0};
				enum {ActiveLow = 1};
				enum {RisingEdge = 2};
				enum {FallingEdge = 3};
				enum {Transitional = 4};
				enum {Mask = 7};
				namespace GPIO0 { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x00000007};
					enum {Value_ActiveHigh = 0x0};
					enum {ValueMask_ActiveHigh = 0x00000000};
					enum {Value_ActiveLow = 0x1};
					enum {ValueMask_ActiveLow = 0x00000001};
					enum {Value_RisingEdge = 0x2};
					enum {ValueMask_RisingEdge = 0x00000002};
					enum {Value_FallingEdge = 0x3};
					enum {ValueMask_FallingEdge = 0x00000003};
					enum {Value_Transitional = 0x4};
					enum {ValueMask_Transitional = 0x00000004};
					};
				namespace GPIO1 { // Field Description
					enum {Lsb = 3};
					enum {FieldMask = 0x00000038};
					enum {Value_ActiveHigh = 0x0};
					enum {ValueMask_ActiveHigh = 0x00000000};
					enum {Value_ActiveLow = 0x1};
					enum {ValueMask_ActiveLow = 0x00000008};
					enum {Value_RisingEdge = 0x2};
					enum {ValueMask_RisingEdge = 0x00000010};
					enum {Value_FallingEdge = 0x3};
					enum {ValueMask_FallingEdge = 0x00000018};
					enum {Value_Transitional = 0x4};
					enum {ValueMask_Transitional = 0x00000020};
					};
				namespace GPIO2 { // Field Description
					enum {Lsb = 6};
					enum {FieldMask = 0x000001C0};
					enum {Value_ActiveHigh = 0x0};
					enum {ValueMask_ActiveHigh = 0x00000000};
					enum {Value_ActiveLow = 0x1};
					enum {ValueMask_ActiveLow = 0x00000040};
					enum {Value_RisingEdge = 0x2};
					enum {ValueMask_RisingEdge = 0x00000080};
					enum {Value_FallingEdge = 0x3};
					enum {ValueMask_FallingEdge = 0x000000C0};
					enum {Value_Transitional = 0x4};
					enum {ValueMask_Transitional = 0x00000100};
					};
				namespace GPIO3 { // Field Description
					enum {Lsb = 9};
					enum {FieldMask = 0x00000E00};
					enum {Value_ActiveHigh = 0x0};
					enum {ValueMask_ActiveHigh = 0x00000000};
					enum {Value_ActiveLow = 0x1};
					enum {ValueMask_ActiveLow = 0x00000200};
					enum {Value_RisingEdge = 0x2};
					enum {ValueMask_RisingEdge = 0x00000400};
					enum {Value_FallingEdge = 0x3};
					enum {ValueMask_FallingEdge = 0x00000600};
					enum {Value_Transitional = 0x4};
					enum {ValueMask_Transitional = 0x00000800};
					};
				namespace GPIO4 { // Field Description
					enum {Lsb = 12};
					enum {FieldMask = 0x00007000};
					enum {Value_ActiveHigh = 0x0};
					enum {ValueMask_ActiveHigh = 0x00000000};
					enum {Value_ActiveLow = 0x1};
					enum {ValueMask_ActiveLow = 0x00001000};
					enum {Value_RisingEdge = 0x2};
					enum {ValueMask_RisingEdge = 0x00002000};
					enum {Value_FallingEdge = 0x3};
					enum {ValueMask_FallingEdge = 0x00003000};
					enum {Value_Transitional = 0x4};
					enum {ValueMask_Transitional = 0x00004000};
					};
				namespace GPIO5 { // Field Description
					enum {Lsb = 15};
					enum {FieldMask = 0x00038000};
					enum {Value_ActiveHigh = 0x0};
					enum {ValueMask_ActiveHigh = 0x00000000};
					enum {Value_ActiveLow = 0x1};
					enum {ValueMask_ActiveLow = 0x00008000};
					enum {Value_RisingEdge = 0x2};
					enum {ValueMask_RisingEdge = 0x00010000};
					enum {Value_FallingEdge = 0x3};
					enum {ValueMask_FallingEdge = 0x00018000};
					enum {Value_Transitional = 0x4};
					enum {ValueMask_Transitional = 0x00020000};
					};
				namespace GPIO6 { // Field Description
					enum {Lsb = 18};
					enum {FieldMask = 0x001C0000};
					enum {Value_ActiveHigh = 0x0};
					enum {ValueMask_ActiveHigh = 0x00000000};
					enum {Value_ActiveLow = 0x1};
					enum {ValueMask_ActiveLow = 0x00040000};
					enum {Value_RisingEdge = 0x2};
					enum {ValueMask_RisingEdge = 0x00080000};
					enum {Value_FallingEdge = 0x3};
					enum {ValueMask_FallingEdge = 0x000C0000};
					enum {Value_Transitional = 0x4};
					enum {ValueMask_Transitional = 0x00100000};
					};
				namespace GPIO7 { // Field Description
					enum {Lsb = 21};
					enum {FieldMask = 0x00E00000};
					enum {Value_ActiveHigh = 0x0};
					enum {ValueMask_ActiveHigh = 0x00000000};
					enum {Value_ActiveLow = 0x1};
					enum {ValueMask_ActiveLow = 0x00200000};
					enum {Value_RisingEdge = 0x2};
					enum {ValueMask_RisingEdge = 0x00400000};
					enum {Value_FallingEdge = 0x3};
					enum {ValueMask_FallingEdge = 0x00600000};
					enum {Value_Transitional = 0x4};
					enum {ValueMask_Transitional = 0x00800000};
					};
				};
			namespace GPIT2R { // Register description
				typedef uint32_t	Reg;
				enum {ActiveHigh = 0};
				enum {ActiveLow = 1};
				enum {RisingEdge = 2};
				enum {FallingEdge = 3};
				enum {Transitional = 4};
				enum {Mask = 7};
				namespace GPIO8 { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x00000007};
					enum {Value_ActiveHigh = 0x0};
					enum {ValueMask_ActiveHigh = 0x00000000};
					enum {Value_ActiveLow = 0x1};
					enum {ValueMask_ActiveLow = 0x00000001};
					enum {Value_RisingEdge = 0x2};
					enum {ValueMask_RisingEdge = 0x00000002};
					enum {Value_FallingEdge = 0x3};
					enum {ValueMask_FallingEdge = 0x00000003};
					enum {Value_Transitional = 0x4};
					enum {ValueMask_Transitional = 0x00000004};
					};
				namespace GPIO9 { // Field Description
					enum {Lsb = 3};
					enum {FieldMask = 0x00000038};
					enum {Value_ActiveHigh = 0x0};
					enum {ValueMask_ActiveHigh = 0x00000000};
					enum {Value_ActiveLow = 0x1};
					enum {ValueMask_ActiveLow = 0x00000008};
					enum {Value_RisingEdge = 0x2};
					enum {ValueMask_RisingEdge = 0x00000010};
					enum {Value_FallingEdge = 0x3};
					enum {ValueMask_FallingEdge = 0x00000018};
					enum {Value_Transitional = 0x4};
					enum {ValueMask_Transitional = 0x00000020};
					};
				namespace GPIO10 { // Field Description
					enum {Lsb = 6};
					enum {FieldMask = 0x000001C0};
					enum {Value_ActiveHigh = 0x0};
					enum {ValueMask_ActiveHigh = 0x00000000};
					enum {Value_ActiveLow = 0x1};
					enum {ValueMask_ActiveLow = 0x00000040};
					enum {Value_RisingEdge = 0x2};
					enum {ValueMask_RisingEdge = 0x00000080};
					enum {Value_FallingEdge = 0x3};
					enum {ValueMask_FallingEdge = 0x000000C0};
					enum {Value_Transitional = 0x4};
					enum {ValueMask_Transitional = 0x00000100};
					};
				namespace GPIO11 { // Field Description
					enum {Lsb = 9};
					enum {FieldMask = 0x00000E00};
					enum {Value_ActiveHigh = 0x0};
					enum {ValueMask_ActiveHigh = 0x00000000};
					enum {Value_ActiveLow = 0x1};
					enum {ValueMask_ActiveLow = 0x00000200};
					enum {Value_RisingEdge = 0x2};
					enum {ValueMask_RisingEdge = 0x00000400};
					enum {Value_FallingEdge = 0x3};
					enum {ValueMask_FallingEdge = 0x00000600};
					enum {Value_Transitional = 0x4};
					enum {ValueMask_Transitional = 0x00000800};
					};
				namespace GPIO12 { // Field Description
					enum {Lsb = 12};
					enum {FieldMask = 0x00007000};
					enum {Value_ActiveHigh = 0x0};
					enum {ValueMask_ActiveHigh = 0x00000000};
					enum {Value_ActiveLow = 0x1};
					enum {ValueMask_ActiveLow = 0x00001000};
					enum {Value_RisingEdge = 0x2};
					enum {ValueMask_RisingEdge = 0x00002000};
					enum {Value_FallingEdge = 0x3};
					enum {ValueMask_FallingEdge = 0x00003000};
					enum {Value_Transitional = 0x4};
					enum {ValueMask_Transitional = 0x00004000};
					};
				namespace GPIO13 { // Field Description
					enum {Lsb = 15};
					enum {FieldMask = 0x00038000};
					enum {Value_ActiveHigh = 0x0};
					enum {ValueMask_ActiveHigh = 0x00000000};
					enum {Value_ActiveLow = 0x1};
					enum {ValueMask_ActiveLow = 0x00008000};
					enum {Value_RisingEdge = 0x2};
					enum {ValueMask_RisingEdge = 0x00010000};
					enum {Value_FallingEdge = 0x3};
					enum {ValueMask_FallingEdge = 0x00018000};
					enum {Value_Transitional = 0x4};
					enum {ValueMask_Transitional = 0x00020000};
					};
				namespace GPIO14 { // Field Description
					enum {Lsb = 18};
					enum {FieldMask = 0x001C0000};
					enum {Value_ActiveHigh = 0x0};
					enum {ValueMask_ActiveHigh = 0x00000000};
					enum {Value_ActiveLow = 0x1};
					enum {ValueMask_ActiveLow = 0x00040000};
					enum {Value_RisingEdge = 0x2};
					enum {ValueMask_RisingEdge = 0x00080000};
					enum {Value_FallingEdge = 0x3};
					enum {ValueMask_FallingEdge = 0x000C0000};
					enum {Value_Transitional = 0x4};
					enum {ValueMask_Transitional = 0x00100000};
					};
				namespace GPIO15 { // Field Description
					enum {Lsb = 21};
					enum {FieldMask = 0x00E00000};
					enum {Value_ActiveHigh = 0x0};
					enum {ValueMask_ActiveHigh = 0x00000000};
					enum {Value_ActiveLow = 0x1};
					enum {ValueMask_ActiveLow = 0x00200000};
					enum {Value_RisingEdge = 0x2};
					enum {ValueMask_RisingEdge = 0x00400000};
					enum {Value_FallingEdge = 0x3};
					enum {ValueMask_FallingEdge = 0x00600000};
					enum {Value_Transitional = 0x4};
					enum {ValueMask_Transitional = 0x00800000};
					};
				};
			namespace GPCLKR { // Register description
				typedef uint32_t	Reg;
				namespace CLK0DC { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x0000000F};
					enum {Value_PCLK2 = 0xF};
					enum {ValueMask_PCLK2 = 0x0000000F};
					};
				namespace CLK0TC { // Field Description
					enum {Lsb = 4};
					enum {FieldMask = 0x000000F0};
					enum {Value_PCLK2 = 0xF};
					enum {ValueMask_PCLK2 = 0x000000F0};
					};
				namespace MUX14 { // Field Description
					enum {Lsb = 8};
					enum {FieldMask = 0x00000100};
					enum {Value_GPOUTR = 0x1};
					enum {ValueMask_GPOUTR = 0x00000100};
					enum {Value_ClockOutput = 0x0};
					enum {ValueMask_ClockOutput = 0x00000000};
					};
				namespace CLK1DC { // Field Description
					enum {Lsb = 9};
					enum {FieldMask = 0x000FFE00};
					enum {Value_PCLK2 = 0xF};
					enum {ValueMask_PCLK2 = 0x00001E00};
					};
				namespace CLK1TC { // Field Description
					enum {Lsb = 20};
					enum {FieldMask = 0x00F00000};
					enum {Value_PCLK2 = 0xF};
					enum {ValueMask_PCLK2 = 0x00F00000};
					};
				namespace MUX15 { // Field Description
					enum {Lsb = 24};
					enum {FieldMask = 0x01000000};
					enum {Value_GPOUTR = 0x1};
					enum {ValueMask_GPOUTR = 0x01000000};
					enum {Value_ClockOutput = 0x0};
					enum {ValueMask_ClockOutput = 0x00000000};
					};
				};
			namespace OST_STATUS { // Register description
				typedef uint32_t	Reg;
				namespace WarmReset { // Field Description
					enum {Lsb = 4};
					enum {FieldMask = 0x00000010};
					enum {Value_Pending = 0x1};
					enum {ValueMask_Pending = 0x00000010};
					enum {Value_Clear = 0x1};
					enum {ValueMask_Clear = 0x00000010};
					};
				namespace WatchdogInterrupt { // Field Description
					enum {Lsb = 3};
					enum {FieldMask = 0x00000008};
					enum {Value_Pending = 0x1};
					enum {ValueMask_Pending = 0x00000008};
					enum {Value_Clear = 0x1};
					enum {ValueMask_Clear = 0x00000008};
					};
				namespace TimeStampInterrupt { // Field Description
					enum {Lsb = 2};
					enum {FieldMask = 0x00000004};
					enum {Value_Pending = 0x1};
					enum {ValueMask_Pending = 0x00000004};
					enum {Value_Clear = 0x1};
					enum {ValueMask_Clear = 0x00000004};
					};
				namespace Timer1Interrupt { // Field Description
					enum {Lsb = 1};
					enum {FieldMask = 0x00000002};
					enum {Value_Pending = 0x1};
					enum {ValueMask_Pending = 0x00000002};
					enum {Value_Clear = 0x1};
					enum {ValueMask_Clear = 0x00000002};
					};
				namespace Timer0Interrupt { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x00000001};
					enum {Value_Pending = 0x1};
					enum {ValueMask_Pending = 0x00000001};
					enum {Value_Clear = 0x1};
					enum {ValueMask_Clear = 0x00000001};
					};
				};
			};
		};
	};
#endif
