#ifndef _hw_intel_ixp4xx_ostregh_
#define _hw_intel_ixp4xx_ostregh_
#include <stdint.h>
namespace Oscl { // Namespace description
	namespace IXP4XX { // Namespace description
		namespace OST { // Namespace description
			namespace OST_TS { // Register description
				typedef uint32_t	Reg;
				};
			namespace OST_TIM { // Register description
				typedef uint32_t	Reg;
				};
			namespace OST_TIM_RL { // Register description
				typedef uint32_t	Reg;
				namespace ReloadValue { // Field Description
					enum {Lsb = 2};
					enum {FieldMask = 0xFFFFFFFC};
					};
				namespace Mode { // Field Description
					enum {Lsb = 1};
					enum {FieldMask = 0x00000002};
					enum {Value_ReloadAndContinue = 0x0};
					enum {ValueMask_ReloadAndContinue = 0x00000000};
					enum {Value_OneShot = 0x1};
					enum {ValueMask_OneShot = 0x00000002};
					};
				namespace Enable { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x00000001};
					enum {Value_Disable = 0x0};
					enum {ValueMask_Disable = 0x00000000};
					enum {Value_Enable = 0x1};
					enum {ValueMask_Enable = 0x00000001};
					};
				};
			namespace OST_WDOG { // Register description
				typedef uint32_t	Reg;
				};
			namespace OST_WDOG_ENAB { // Register description
				typedef uint32_t	Reg;
				namespace CountEnable { // Field Description
					enum {Lsb = 2};
					enum {FieldMask = 0x00000004};
					enum {Value_Disable = 0x0};
					enum {ValueMask_Disable = 0x00000000};
					enum {Value_Enable = 0x1};
					enum {ValueMask_Enable = 0x00000004};
					};
				namespace InterruptEnable { // Field Description
					enum {Lsb = 1};
					enum {FieldMask = 0x00000002};
					enum {Value_Disable = 0x0};
					enum {ValueMask_Disable = 0x00000000};
					enum {Value_Enable = 0x1};
					enum {ValueMask_Enable = 0x00000002};
					};
				namespace ResetEnable { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x00000001};
					enum {Value_Disable = 0x0};
					enum {ValueMask_Disable = 0x00000000};
					enum {Value_Enable = 0x1};
					enum {ValueMask_Enable = 0x00000001};
					};
				};
			namespace OST_WDOG_KEY { // Register description
				typedef uint32_t	Reg;
				enum {Key = 18478};
				};
			namespace OST_STATUS { // Register description
				typedef uint32_t	Reg;
				namespace WarmReset { // Field Description
					enum {Lsb = 4};
					enum {FieldMask = 0x00000010};
					enum {Value_Pending = 0x1};
					enum {ValueMask_Pending = 0x00000010};
					enum {Value_Clear = 0x1};
					enum {ValueMask_Clear = 0x00000010};
					};
				namespace WatchdogInterrupt { // Field Description
					enum {Lsb = 3};
					enum {FieldMask = 0x00000008};
					enum {Value_Pending = 0x1};
					enum {ValueMask_Pending = 0x00000008};
					enum {Value_Clear = 0x1};
					enum {ValueMask_Clear = 0x00000008};
					};
				namespace TimeStampInterrupt { // Field Description
					enum {Lsb = 2};
					enum {FieldMask = 0x00000004};
					enum {Value_Pending = 0x1};
					enum {ValueMask_Pending = 0x00000004};
					enum {Value_Clear = 0x1};
					enum {ValueMask_Clear = 0x00000004};
					};
				namespace Timer1Interrupt { // Field Description
					enum {Lsb = 1};
					enum {FieldMask = 0x00000002};
					enum {Value_Pending = 0x1};
					enum {ValueMask_Pending = 0x00000002};
					enum {Value_Clear = 0x1};
					enum {ValueMask_Clear = 0x00000002};
					};
				namespace Timer0Interrupt { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x00000001};
					enum {Value_Pending = 0x1};
					enum {ValueMask_Pending = 0x00000001};
					enum {Value_Clear = 0x1};
					enum {ValueMask_Clear = 0x00000001};
					};
				};
			};
		};
	};
#endif
