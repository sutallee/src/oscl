/*
   Copyright (C) 2005 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_hw_intel_ixp4xx_exph_
#define _oscl_hw_intel_ixp4xx_exph_
#include "expreg.h"

namespace Oscl {
namespace IXP4XX {
namespace EXP {

/**	This is the structure of an IXP4XX Expansion Bus Configuration Registers
 */
struct ExpansionBus {
	// 0xC4000000
	TIMING_CSx::Reg			exp_timing_cs0;
	// 0xC4000004
	TIMING_CSx::Reg			exp_timing_cs1;
	// 0xC4000008
	TIMING_CSx::Reg			exp_timing_cs2;
	// 0xC400000C
	TIMING_CSx::Reg			exp_timing_cs3;
	// 0xC4000010
	TIMING_CSx::Reg			exp_timing_cs4;
	// 0xC4000014
	TIMING_CSx::Reg			exp_timing_cs5;
	// 0xC4000018
	TIMING_CSx::Reg			exp_timing_cs6;
	// 0xC400001C
	TIMING_CSx::Reg			exp_timing_cs7;
	// 0xC4000020
	CNFG0::Reg				exp_cnfg0;
	// 0xC4000024
	CNFG1::Reg				exp_cnfg1;
	};

}
}
}

#endif

