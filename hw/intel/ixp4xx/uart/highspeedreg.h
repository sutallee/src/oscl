#ifndef _hw_intel_ixp4xx_uart_highspeedh_
#define _hw_intel_ixp4xx_uart_highspeedh_
#include <stdint.h>
namespace Oscl { // Namespace description
	namespace IXP4XX { // Namespace description
		namespace UART { // Namespace description
			namespace HighSpeed { // Namespace description
				namespace RBR_THR_DLL { // Register description
					typedef uint32_t	Reg;
					namespace RBR_THR_DLL { // Field Description
						enum {Lsb = 0};
						enum {FieldMask = 0x000000FF};
						};
					};
				namespace IER_DLH { // Register description
					typedef uint32_t	Reg;
					namespace IER_DLH { // Field Description
						enum {Lsb = 0};
						enum {FieldMask = 0x000000FF};
						};
					};
				namespace IIR_FCR { // Register description
					typedef uint32_t	Reg;
					namespace IIR_FCR { // Field Description
						enum {Lsb = 0};
						enum {FieldMask = 0x000000FF};
						};
					};
				namespace LCR { // Register description
					typedef uint32_t	Reg;
					namespace LCR { // Field Description
						enum {Lsb = 0};
						enum {FieldMask = 0x000000FF};
						};
					};
				namespace MCR { // Register description
					typedef uint32_t	Reg;
					namespace MCR { // Field Description
						enum {Lsb = 0};
						enum {FieldMask = 0x000000FF};
						};
					};
				namespace LSR { // Register description
					typedef uint32_t	Reg;
					namespace LSR { // Field Description
						enum {Lsb = 0};
						enum {FieldMask = 0x000000FF};
						};
					};
				namespace MSR { // Register description
					typedef uint32_t	Reg;
					namespace MSR { // Field Description
						enum {Lsb = 0};
						enum {FieldMask = 0x000000FF};
						};
					};
				namespace SPR { // Register description
					typedef uint32_t	Reg;
					namespace SPR { // Field Description
						enum {Lsb = 0};
						enum {FieldMask = 0x000000FF};
						};
					};
				namespace ISR { // Register description
					typedef uint32_t	Reg;
					namespace RXPL { // Field Description
						enum {Lsb = 4};
						enum {FieldMask = 0x00000010};
						enum {Value_PositivePulseAsZero = 0x0};
						enum {ValueMask_PositivePulseAsZero = 0x00000000};
						enum {Value_NegativePulseAsZero = 0x1};
						enum {ValueMask_NegativePulseAsZero = 0x00000010};
						};
					namespace TXPL { // Field Description
						enum {Lsb = 3};
						enum {FieldMask = 0x00000008};
						enum {Value_PositivePulseAsZero = 0x0};
						enum {ValueMask_PositivePulseAsZero = 0x00000000};
						enum {Value_NegativePulseAsZero = 0x1};
						enum {ValueMask_NegativePulseAsZero = 0x00000008};
						};
					namespace XMODE { // Field Description
						enum {Lsb = 2};
						enum {FieldMask = 0x00000004};
						enum {Value_ThreeSixteenthsWidth = 0x0};
						enum {ValueMask_ThreeSixteenthsWidth = 0x00000000};
						enum {Value_OnePointSixMicrosecond = 0x1};
						enum {ValueMask_OnePointSixMicrosecond = 0x00000004};
						};
					namespace RCVEIR { // Field Description
						enum {Lsb = 1};
						enum {FieldMask = 0x00000002};
						enum {Value_UartMode = 0x0};
						enum {ValueMask_UartMode = 0x00000000};
						enum {Value_InfraredMode = 0x1};
						enum {ValueMask_InfraredMode = 0x00000002};
						};
					namespace XMITIR { // Field Description
						enum {Lsb = 0};
						enum {FieldMask = 0x00000001};
						enum {Value_UartMode = 0x0};
						enum {ValueMask_UartMode = 0x00000000};
						enum {Value_InfraredMode = 0x1};
						enum {ValueMask_InfraredMode = 0x00000001};
						};
					};
				};
			};
		};
	};
#endif
