/*
   Copyright (C) 2006 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/
#ifndef _oscl_hw_intel_ixp4xx_uart_consoleh_
#define _oscl_hw_intel_ixp4xx_uart_consoleh_

#include "consolereg.h"
/** */
namespace Oscl {
/** */
namespace IXP4XX {
/** */
namespace UART {
/** */
namespace Console {

/**	This is the structure of the IXP4XX Console UART registers.
	Starts at 0xC8001000
 */
struct Registers {
	// 0xC8001000
	RBR_THR_DLL::Reg	rbr_thr_dll;
	// 0xC8001004
	IER_DLH::Reg		ier_dlh;
	// 0xC8001008
	IIR_FCR::Reg		iir_fcr;
	// 0xC800100C
	LCR::Reg			lcr;
	// 0xC8001010
	MCR::Reg			mcr;
	// 0xC8001014
	LSR::Reg			lsr;
	// 0xC8001018
	MSR::Reg			msr;
	// 0xC800101C
	SPR::Reg			spr;
	// 0xC8001020
	ISR::Reg			isr;
	};

}
}
}
}

#endif
