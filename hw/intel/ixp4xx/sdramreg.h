#ifndef _hw_intel_sdramregh_
#define _hw_intel_sdramregh_
#include <stdint.h>
namespace Oscl { // Namespace description
	namespace IXP4XX { // Namespace description
		namespace SDRAM { // Namespace description
			namespace CONFIG { // Register description
				typedef uint32_t	Reg;
				namespace MemConfig { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x00000007};
					enum {Value_Banks2Depth8MWidth16 = 0x0};
					enum {ValueMask_Banks2Depth8MWidth16 = 0x00000000};
					enum {Value_Banks4Depth8MWidth16 = 0x1};
					enum {ValueMask_Banks4Depth8MWidth16 = 0x00000001};
					enum {Value_Banks2Depth16MWidth16 = 0x2};
					enum {ValueMask_Banks2Depth16MWidth16 = 0x00000002};
					enum {Value_Banks4Depth16MWidth16 = 0x3};
					enum {ValueMask_Banks4Depth16MWidth16 = 0x00000003};
					enum {Value_Banks2Depth32MWidth16 = 0x4};
					enum {ValueMask_Banks2Depth32MWidth16 = 0x00000004};
					enum {Value_Banks4Depth32MWidth16 = 0x5};
					enum {ValueMask_Banks4Depth32MWidth16 = 0x00000005};
					};
				namespace CASLat { // Field Description
					enum {Lsb = 3};
					enum {FieldMask = 0x00000008};
					enum {Value_TwoCycleLatency = 0x0};
					enum {ValueMask_TwoCycleLatency = 0x00000000};
					enum {Value_ThreeCycleLatency = 0x1};
					enum {ValueMask_ThreeCycleLatency = 0x00000008};
					};
				namespace RASLat { // Field Description
					enum {Lsb = 4};
					enum {FieldMask = 0x00000010};
					enum {Value_ThreeCycleLatency = 0x1};
					enum {ValueMask_ThreeCycleLatency = 0x00000010};
					};
				namespace ChipSize { // Field Description
					enum {Lsb = 5};
					enum {FieldMask = 0x00000020};
					enum {Value_Size64MBit = 0x1};
					enum {ValueMask_Size64MBit = 0x00000020};
					enum {Value_Size128MBit = 0x0};
					enum {ValueMask_Size128MBit = 0x00000000};
					enum {Value_Size256MBit = 0x0};
					enum {ValueMask_Size256MBit = 0x00000000};
					enum {Value_Size512MBit = 0x0};
					enum {ValueMask_Size512MBit = 0x00000000};
					};
				};
			};
		};
	};
#endif
