#ifndef _hw_intel_pcicsrregh_
#define _hw_intel_pcicsrregh_
#include <stdint.h>
namespace Oscl { // Namespace description
	namespace IXP4XX { // Namespace description
		namespace PCI { // Namespace description
			namespace CSR { // Namespace description
				namespace PCI_NP_AD { // Register description
					typedef uint32_t	Reg;
					};
				namespace PCI_NP_CBE { // Register description
					typedef uint32_t	Reg;
					namespace NP_CMD { // Field Description
						enum {Lsb = 0};
						enum {FieldMask = 0x0000000F};
						};
					namespace NP_BE { // Field Description
						enum {Lsb = 4};
						enum {FieldMask = 0x000000F0};
						};
					};
				namespace PCI_NP_WDATA { // Register description
					typedef uint32_t	Reg;
					};
				namespace PCI_NP_RDATA { // Register description
					typedef uint32_t	Reg;
					};
				namespace PCI_CRP_AD_CBE { // Register description
					typedef uint32_t	Reg;
					namespace CRP_AD { // Field Description
						enum {Lsb = 0};
						enum {FieldMask = 0x000007FF};
						};
					namespace CRP_CMD { // Field Description
						enum {Lsb = 16};
						enum {FieldMask = 0x000F0000};
						namespace CMD { // Field Description
							enum {Lsb = 17};
							enum {FieldMask = 0x000E0000};
							};
						namespace DIR { // Field Description
							enum {Lsb = 16};
							enum {FieldMask = 0x00010000};
							enum {Value_Read = 0x0};
							enum {ValueMask_Read = 0x00000000};
							enum {Value_Write = 0x0};
							enum {ValueMask_Write = 0x00000000};
							};
						};
					namespace CRP_BE { // Field Description
						enum {Lsb = 20};
						enum {FieldMask = 0x00F00000};
						namespace Bits7Thru0 { // Field Description
							enum {Lsb = 20};
							enum {FieldMask = 0x00100000};
							enum {Value_Enable = 0x1};
							enum {ValueMask_Enable = 0x00100000};
							enum {Value_Disable = 0x0};
							enum {ValueMask_Disable = 0x00000000};
							};
						namespace Bits15Thru8 { // Field Description
							enum {Lsb = 21};
							enum {FieldMask = 0x00200000};
							enum {Value_Enable = 0x1};
							enum {ValueMask_Enable = 0x00200000};
							enum {Value_Disable = 0x0};
							enum {ValueMask_Disable = 0x00000000};
							};
						namespace Bits23Thru16 { // Field Description
							enum {Lsb = 22};
							enum {FieldMask = 0x00400000};
							enum {Value_Enable = 0x1};
							enum {ValueMask_Enable = 0x00400000};
							enum {Value_Disable = 0x0};
							enum {ValueMask_Disable = 0x00000000};
							};
						namespace Bits31Thru24 { // Field Description
							enum {Lsb = 23};
							enum {FieldMask = 0x00800000};
							enum {Value_Enable = 0x1};
							enum {ValueMask_Enable = 0x00800000};
							enum {Value_Disable = 0x0};
							enum {ValueMask_Disable = 0x00000000};
							};
						};
					};
				namespace PCI_CRP_WDATA { // Register description
					typedef uint32_t	Reg;
					};
				namespace PCI_CRP_RDATA { // Register description
					typedef uint32_t	Reg;
					};
				namespace PCI_CSR { // Register description
					typedef uint32_t	Reg;
					namespace HOST { // Field Description
						enum {Lsb = 0};
						enum {FieldMask = 0x00000001};
						};
					namespace ARBEN { // Field Description
						enum {Lsb = 1};
						enum {FieldMask = 0x00000002};
						};
					namespace ABS { // Field Description
						enum {Lsb = 2};
						enum {FieldMask = 0x00000004};
						enum {Value_SwapAhbByteLanes = 0x1};
						enum {ValueMask_SwapAhbByteLanes = 0x00000004};
						enum {Value_NoSwapAhbByteLanes = 0x0};
						enum {ValueMask_NoSwapAhbByteLanes = 0x00000000};
						};
					namespace PBS { // Field Description
						enum {Lsb = 3};
						enum {FieldMask = 0x00000008};
						enum {Value_SwapPciByteLanes = 0x1};
						enum {ValueMask_SwapPciByteLanes = 0x00000008};
						enum {Value_NoSwapPciByteLanes = 0x0};
						enum {ValueMask_NoSwapPciByteLanes = 0x00000000};
						};
					namespace ABE { // Field Description
						enum {Lsb = 4};
						enum {FieldMask = 0x00000010};
						enum {Value_AhbBigEndian = 0x1};
						enum {ValueMask_AhbBigEndian = 0x00000010};
						enum {Value_AhbLittleEndian = 0x0};
						enum {ValueMask_AhbLittleEndian = 0x00000000};
						};
					namespace ASE { // Field Description
						enum {Lsb = 8};
						enum {FieldMask = 0x00000100};
						enum {Value_EnableSystemError = 0x1};
						enum {ValueMask_EnableSystemError = 0x00000100};
						enum {Value_DisableSystemError = 0x0};
						enum {ValueMask_DisableSystemError = 0x00000000};
						};
					namespace IC { // Field Description
						enum {Lsb = 15};
						enum {FieldMask = 0x00008000};
						enum {Value_ForceRetries = 0x0};
						enum {ValueMask_ForceRetries = 0x00000000};
						enum {Value_AcceptConfigCycles = 0x1};
						enum {ValueMask_AcceptConfigCycles = 0x00008000};
						};
					namespace PRST { // Field Description
						enum {Lsb = 16};
						enum {FieldMask = 0x00010000};
						enum {Value_NoAction = 0x0};
						enum {ValueMask_NoAction = 0x00000000};
						enum {Value_InitializeFlipFlops = 0x1};
						enum {ValueMask_InitializeFlipFlops = 0x00010000};
						};
					};
				namespace PCI_ISR { // Register description
					typedef uint32_t	Reg;
					namespace PSE { // Field Description
						enum {Lsb = 0};
						enum {FieldMask = 0x00000001};
						enum {Value_Clear = 0x1};
						enum {ValueMask_Clear = 0x00000001};
						enum {Value_Error = 0x1};
						enum {ValueMask_Error = 0x00000001};
						enum {Value_NoError = 0x0};
						enum {ValueMask_NoError = 0x00000000};
						};
					namespace PFE { // Field Description
						enum {Lsb = 1};
						enum {FieldMask = 0x00000002};
						enum {Value_Clear = 0x1};
						enum {ValueMask_Clear = 0x00000002};
						enum {Value_Error = 0x1};
						enum {ValueMask_Error = 0x00000002};
						enum {Value_NoError = 0x0};
						enum {ValueMask_NoError = 0x00000000};
						};
					namespace PPE { // Field Description
						enum {Lsb = 2};
						enum {FieldMask = 0x00000004};
						enum {Value_Clear = 0x1};
						enum {ValueMask_Clear = 0x00000004};
						enum {Value_Error = 0x1};
						enum {ValueMask_Error = 0x00000004};
						enum {Value_NoError = 0x0};
						enum {ValueMask_NoError = 0x00000000};
						};
					namespace AHBE { // Field Description
						enum {Lsb = 3};
						enum {FieldMask = 0x00000008};
						enum {Value_Clear = 0x1};
						enum {ValueMask_Clear = 0x00000008};
						enum {Value_Error = 0x1};
						enum {ValueMask_Error = 0x00000008};
						enum {Value_NoError = 0x0};
						enum {ValueMask_NoError = 0x00000000};
						};
					namespace APDC { // Field Description
						enum {Lsb = 4};
						enum {FieldMask = 0x00000010};
						enum {Value_DmaComplete = 0x1};
						enum {ValueMask_DmaComplete = 0x00000010};
						enum {Value_DmaBusy = 0x0};
						enum {ValueMask_DmaBusy = 0x00000000};
						};
					namespace ADB { // Field Description
						enum {Lsb = 6};
						enum {FieldMask = 0x00000040};
						enum {Value_IrqPending = 0x1};
						enum {ValueMask_IrqPending = 0x00000040};
						enum {Value_IrqNotPending = 0x0};
						enum {ValueMask_IrqNotPending = 0x00000000};
						};
					namespace PDB { // Field Description
						enum {Lsb = 7};
						enum {FieldMask = 0x00000080};
						enum {Value_IrqPending = 0x1};
						enum {ValueMask_IrqPending = 0x00000080};
						enum {Value_IrqNotPending = 0x0};
						enum {ValueMask_IrqNotPending = 0x00000000};
						};
					};
				namespace PCI_INTEN { // Register description
					typedef uint32_t	Reg;
					namespace PSE { // Field Description
						enum {Lsb = 0};
						enum {FieldMask = 0x00000001};
						enum {Value_Enable = 0x1};
						enum {ValueMask_Enable = 0x00000001};
						enum {Value_Disable = 0x0};
						enum {ValueMask_Disable = 0x00000000};
						};
					namespace PFE { // Field Description
						enum {Lsb = 1};
						enum {FieldMask = 0x00000002};
						enum {Value_Enable = 0x1};
						enum {ValueMask_Enable = 0x00000002};
						enum {Value_Disable = 0x0};
						enum {ValueMask_Disable = 0x00000000};
						};
					namespace PPE { // Field Description
						enum {Lsb = 2};
						enum {FieldMask = 0x00000004};
						enum {Value_Enable = 0x1};
						enum {ValueMask_Enable = 0x00000004};
						enum {Value_Disable = 0x0};
						enum {ValueMask_Disable = 0x00000000};
						};
					namespace AHBE { // Field Description
						enum {Lsb = 3};
						enum {FieldMask = 0x00000008};
						enum {Value_Enable = 0x1};
						enum {ValueMask_Enable = 0x00000008};
						enum {Value_Disable = 0x0};
						enum {ValueMask_Disable = 0x00000000};
						};
					namespace APDC { // Field Description
						enum {Lsb = 4};
						enum {FieldMask = 0x00000010};
						enum {Value_Enable = 0x1};
						enum {ValueMask_Enable = 0x00000010};
						enum {Value_Disable = 0x0};
						enum {ValueMask_Disable = 0x00000000};
						};
					namespace PADC { // Field Description
						enum {Lsb = 5};
						enum {FieldMask = 0x00000020};
						enum {Value_Enable = 0x1};
						enum {ValueMask_Enable = 0x00000020};
						enum {Value_Disable = 0x0};
						enum {ValueMask_Disable = 0x00000000};
						};
					namespace ADB { // Field Description
						enum {Lsb = 6};
						enum {FieldMask = 0x00000040};
						enum {Value_Enable = 0x1};
						enum {ValueMask_Enable = 0x00000040};
						enum {Value_Disable = 0x0};
						enum {ValueMask_Disable = 0x00000000};
						};
					namespace PDB { // Field Description
						enum {Lsb = 7};
						enum {FieldMask = 0x00000080};
						enum {Value_Enable = 0x1};
						enum {ValueMask_Enable = 0x00000080};
						enum {Value_Disable = 0x0};
						enum {ValueMask_Disable = 0x00000000};
						};
					};
				namespace PCI_DMACTRL { // Register description
					typedef uint32_t	Reg;
					namespace APDCEN { // Field Description
						enum {Lsb = 0};
						enum {FieldMask = 0x00000001};
						enum {Value_Enable = 0x1};
						enum {ValueMask_Enable = 0x00000001};
						enum {Value_Disable = 0x0};
						enum {ValueMask_Disable = 0x00000000};
						};
					namespace APDC0 { // Field Description
						enum {Lsb = 4};
						enum {FieldMask = 0x00000010};
						enum {Value_Clear = 0x1};
						enum {ValueMask_Clear = 0x00000010};
						enum {Value_Complete = 0x1};
						enum {ValueMask_Complete = 0x00000010};
						enum {Value_Busy = 0x0};
						enum {ValueMask_Busy = 0x00000000};
						};
					namespace APDE0 { // Field Description
						enum {Lsb = 5};
						enum {FieldMask = 0x00000020};
						enum {Value_Error = 0x1};
						enum {ValueMask_Error = 0x00000020};
						enum {Value_NoError = 0x0};
						enum {ValueMask_NoError = 0x00000000};
						};
					namespace APDC1 { // Field Description
						enum {Lsb = 6};
						enum {FieldMask = 0x00000040};
						enum {Value_Clear = 0x1};
						enum {ValueMask_Clear = 0x00000040};
						enum {Value_Complete = 0x1};
						enum {ValueMask_Complete = 0x00000040};
						enum {Value_Busy = 0x0};
						enum {ValueMask_Busy = 0x00000000};
						};
					namespace APDE1 { // Field Description
						enum {Lsb = 7};
						enum {FieldMask = 0x00000080};
						enum {Value_Error = 0x1};
						enum {ValueMask_Error = 0x00000080};
						enum {Value_NoError = 0x0};
						enum {ValueMask_NoError = 0x00000000};
						};
					namespace PADC0 { // Field Description
						enum {Lsb = 12};
						enum {FieldMask = 0x00001000};
						enum {Value_Clear = 0x1};
						enum {ValueMask_Clear = 0x00001000};
						enum {Value_Complete = 0x1};
						enum {ValueMask_Complete = 0x00001000};
						enum {Value_Busy = 0x0};
						enum {ValueMask_Busy = 0x00000000};
						};
					namespace PADE0 { // Field Description
						enum {Lsb = 13};
						enum {FieldMask = 0x00002000};
						enum {Value_Error = 0x1};
						enum {ValueMask_Error = 0x00002000};
						enum {Value_NoError = 0x0};
						enum {ValueMask_NoError = 0x00000000};
						};
					namespace PADC1 { // Field Description
						enum {Lsb = 14};
						enum {FieldMask = 0x00004000};
						enum {Value_Clear = 0x1};
						enum {ValueMask_Clear = 0x00004000};
						enum {Value_Complete = 0x1};
						enum {ValueMask_Complete = 0x00004000};
						enum {Value_Busy = 0x0};
						enum {ValueMask_Busy = 0x00000000};
						};
					namespace PADE1 { // Field Description
						enum {Lsb = 15};
						enum {FieldMask = 0x00008000};
						enum {Value_Error = 0x1};
						enum {ValueMask_Error = 0x00008000};
						enum {Value_NoError = 0x0};
						enum {ValueMask_NoError = 0x00000000};
						};
					};
				namespace PCI_AHBMEMBASE { // Register description
					typedef uint32_t	Reg;
					namespace AHBbase3 { // Field Description
						enum {Lsb = 0};
						enum {FieldMask = 0x000000FF};
						};
					namespace AHBbase2 { // Field Description
						enum {Lsb = 8};
						enum {FieldMask = 0x0000FF00};
						};
					namespace AHBbase1 { // Field Description
						enum {Lsb = 16};
						enum {FieldMask = 0x00FF0000};
						};
					namespace AHBbase0 { // Field Description
						enum {Lsb = 24};
						enum {FieldMask = 0xFF000000};
						};
					};
				namespace PCI_AHBIOBASE { // Register description
					typedef uint32_t	Reg;
					namespace IObase { // Field Description
						enum {Lsb = 0};
						enum {FieldMask = 0x00FFFFFF};
						};
					};
				namespace PCI_PCIMEMBASE { // Register description
					typedef uint32_t	Reg;
					namespace Membase3 { // Field Description
						enum {Lsb = 0};
						enum {FieldMask = 0x000000FF};
						};
					namespace Membase2 { // Field Description
						enum {Lsb = 8};
						enum {FieldMask = 0x0000FF00};
						};
					namespace Membase1 { // Field Description
						enum {Lsb = 16};
						enum {FieldMask = 0x00FF0000};
						};
					namespace Membase0 { // Field Description
						enum {Lsb = 24};
						enum {FieldMask = 0xFF000000};
						};
					};
				namespace PCI_AHBDOORBELL { // Register description
					typedef uint32_t	Reg;
					namespace Bit0 { // Field Description
						enum {Lsb = 0};
						enum {FieldMask = 0x00000001};
						enum {Value_PciSet = 0x1};
						enum {ValueMask_PciSet = 0x00000001};
						enum {Value_AhbClear = 0x1};
						enum {ValueMask_AhbClear = 0x00000001};
						enum {Value_Pending = 0x1};
						enum {ValueMask_Pending = 0x00000001};
						enum {Value_NotPending = 0x0};
						enum {ValueMask_NotPending = 0x00000000};
						};
					namespace Bit1 { // Field Description
						enum {Lsb = 1};
						enum {FieldMask = 0x00000002};
						enum {Value_PciSet = 0x1};
						enum {ValueMask_PciSet = 0x00000002};
						enum {Value_AhbClear = 0x1};
						enum {ValueMask_AhbClear = 0x00000002};
						enum {Value_Pending = 0x1};
						enum {ValueMask_Pending = 0x00000002};
						enum {Value_NotPending = 0x0};
						enum {ValueMask_NotPending = 0x00000000};
						};
					namespace Bit2 { // Field Description
						enum {Lsb = 2};
						enum {FieldMask = 0x00000004};
						enum {Value_PciSet = 0x1};
						enum {ValueMask_PciSet = 0x00000004};
						enum {Value_AhbClear = 0x1};
						enum {ValueMask_AhbClear = 0x00000004};
						enum {Value_Pending = 0x1};
						enum {ValueMask_Pending = 0x00000004};
						enum {Value_NotPending = 0x0};
						enum {ValueMask_NotPending = 0x00000000};
						};
					namespace Bit3 { // Field Description
						enum {Lsb = 3};
						enum {FieldMask = 0x00000008};
						enum {Value_PciSet = 0x1};
						enum {ValueMask_PciSet = 0x00000008};
						enum {Value_AhbClear = 0x1};
						enum {ValueMask_AhbClear = 0x00000008};
						enum {Value_Pending = 0x1};
						enum {ValueMask_Pending = 0x00000008};
						enum {Value_NotPending = 0x0};
						enum {ValueMask_NotPending = 0x00000000};
						};
					namespace Bit4 { // Field Description
						enum {Lsb = 4};
						enum {FieldMask = 0x00000010};
						enum {Value_PciSet = 0x1};
						enum {ValueMask_PciSet = 0x00000010};
						enum {Value_AhbClear = 0x1};
						enum {ValueMask_AhbClear = 0x00000010};
						enum {Value_Pending = 0x1};
						enum {ValueMask_Pending = 0x00000010};
						enum {Value_NotPending = 0x0};
						enum {ValueMask_NotPending = 0x00000000};
						};
					namespace Bit5 { // Field Description
						enum {Lsb = 5};
						enum {FieldMask = 0x00000020};
						enum {Value_PciSet = 0x1};
						enum {ValueMask_PciSet = 0x00000020};
						enum {Value_AhbClear = 0x1};
						enum {ValueMask_AhbClear = 0x00000020};
						enum {Value_Pending = 0x1};
						enum {ValueMask_Pending = 0x00000020};
						enum {Value_NotPending = 0x0};
						enum {ValueMask_NotPending = 0x00000000};
						};
					namespace Bit6 { // Field Description
						enum {Lsb = 6};
						enum {FieldMask = 0x00000040};
						enum {Value_PciSet = 0x1};
						enum {ValueMask_PciSet = 0x00000040};
						enum {Value_AhbClear = 0x1};
						enum {ValueMask_AhbClear = 0x00000040};
						enum {Value_Pending = 0x1};
						enum {ValueMask_Pending = 0x00000040};
						enum {Value_NotPending = 0x0};
						enum {ValueMask_NotPending = 0x00000000};
						};
					namespace Bit7 { // Field Description
						enum {Lsb = 7};
						enum {FieldMask = 0x00000080};
						enum {Value_PciSet = 0x1};
						enum {ValueMask_PciSet = 0x00000080};
						enum {Value_AhbClear = 0x1};
						enum {ValueMask_AhbClear = 0x00000080};
						enum {Value_Pending = 0x1};
						enum {ValueMask_Pending = 0x00000080};
						enum {Value_NotPending = 0x0};
						enum {ValueMask_NotPending = 0x00000000};
						};
					namespace Bit8 { // Field Description
						enum {Lsb = 8};
						enum {FieldMask = 0x00000100};
						enum {Value_PciSet = 0x1};
						enum {ValueMask_PciSet = 0x00000100};
						enum {Value_AhbClear = 0x1};
						enum {ValueMask_AhbClear = 0x00000100};
						enum {Value_Pending = 0x1};
						enum {ValueMask_Pending = 0x00000100};
						enum {Value_NotPending = 0x0};
						enum {ValueMask_NotPending = 0x00000000};
						};
					namespace Bit9 { // Field Description
						enum {Lsb = 9};
						enum {FieldMask = 0x00000200};
						enum {Value_PciSet = 0x1};
						enum {ValueMask_PciSet = 0x00000200};
						enum {Value_AhbClear = 0x1};
						enum {ValueMask_AhbClear = 0x00000200};
						enum {Value_Pending = 0x1};
						enum {ValueMask_Pending = 0x00000200};
						enum {Value_NotPending = 0x0};
						enum {ValueMask_NotPending = 0x00000000};
						};
					namespace Bit10 { // Field Description
						enum {Lsb = 10};
						enum {FieldMask = 0x00000400};
						enum {Value_PciSet = 0x1};
						enum {ValueMask_PciSet = 0x00000400};
						enum {Value_AhbClear = 0x1};
						enum {ValueMask_AhbClear = 0x00000400};
						enum {Value_Pending = 0x1};
						enum {ValueMask_Pending = 0x00000400};
						enum {Value_NotPending = 0x0};
						enum {ValueMask_NotPending = 0x00000000};
						};
					namespace Bit11 { // Field Description
						enum {Lsb = 11};
						enum {FieldMask = 0x00000800};
						enum {Value_PciSet = 0x1};
						enum {ValueMask_PciSet = 0x00000800};
						enum {Value_AhbClear = 0x1};
						enum {ValueMask_AhbClear = 0x00000800};
						enum {Value_Pending = 0x1};
						enum {ValueMask_Pending = 0x00000800};
						enum {Value_NotPending = 0x0};
						enum {ValueMask_NotPending = 0x00000000};
						};
					namespace Bit12 { // Field Description
						enum {Lsb = 12};
						enum {FieldMask = 0x00001000};
						enum {Value_PciSet = 0x1};
						enum {ValueMask_PciSet = 0x00001000};
						enum {Value_AhbClear = 0x1};
						enum {ValueMask_AhbClear = 0x00001000};
						enum {Value_Pending = 0x1};
						enum {ValueMask_Pending = 0x00001000};
						enum {Value_NotPending = 0x0};
						enum {ValueMask_NotPending = 0x00000000};
						};
					namespace Bit13 { // Field Description
						enum {Lsb = 13};
						enum {FieldMask = 0x00002000};
						enum {Value_PciSet = 0x1};
						enum {ValueMask_PciSet = 0x00002000};
						enum {Value_AhbClear = 0x1};
						enum {ValueMask_AhbClear = 0x00002000};
						enum {Value_Pending = 0x1};
						enum {ValueMask_Pending = 0x00002000};
						enum {Value_NotPending = 0x0};
						enum {ValueMask_NotPending = 0x00000000};
						};
					namespace Bit14 { // Field Description
						enum {Lsb = 14};
						enum {FieldMask = 0x00004000};
						enum {Value_PciSet = 0x1};
						enum {ValueMask_PciSet = 0x00004000};
						enum {Value_AhbClear = 0x1};
						enum {ValueMask_AhbClear = 0x00004000};
						enum {Value_Pending = 0x1};
						enum {ValueMask_Pending = 0x00004000};
						enum {Value_NotPending = 0x0};
						enum {ValueMask_NotPending = 0x00000000};
						};
					namespace Bit15 { // Field Description
						enum {Lsb = 15};
						enum {FieldMask = 0x00008000};
						enum {Value_PciSet = 0x1};
						enum {ValueMask_PciSet = 0x00008000};
						enum {Value_AhbClear = 0x1};
						enum {ValueMask_AhbClear = 0x00008000};
						enum {Value_Pending = 0x1};
						enum {ValueMask_Pending = 0x00008000};
						enum {Value_NotPending = 0x0};
						enum {ValueMask_NotPending = 0x00000000};
						};
					namespace Bit16 { // Field Description
						enum {Lsb = 16};
						enum {FieldMask = 0x00010000};
						enum {Value_PciSet = 0x1};
						enum {ValueMask_PciSet = 0x00010000};
						enum {Value_AhbClear = 0x1};
						enum {ValueMask_AhbClear = 0x00010000};
						enum {Value_Pending = 0x1};
						enum {ValueMask_Pending = 0x00010000};
						enum {Value_NotPending = 0x0};
						enum {ValueMask_NotPending = 0x00000000};
						};
					namespace Bit17 { // Field Description
						enum {Lsb = 17};
						enum {FieldMask = 0x00020000};
						enum {Value_PciSet = 0x1};
						enum {ValueMask_PciSet = 0x00020000};
						enum {Value_AhbClear = 0x1};
						enum {ValueMask_AhbClear = 0x00020000};
						enum {Value_Pending = 0x1};
						enum {ValueMask_Pending = 0x00020000};
						enum {Value_NotPending = 0x0};
						enum {ValueMask_NotPending = 0x00000000};
						};
					namespace Bit18 { // Field Description
						enum {Lsb = 18};
						enum {FieldMask = 0x00040000};
						enum {Value_PciSet = 0x1};
						enum {ValueMask_PciSet = 0x00040000};
						enum {Value_AhbClear = 0x1};
						enum {ValueMask_AhbClear = 0x00040000};
						enum {Value_Pending = 0x1};
						enum {ValueMask_Pending = 0x00040000};
						enum {Value_NotPending = 0x0};
						enum {ValueMask_NotPending = 0x00000000};
						};
					namespace Bit19 { // Field Description
						enum {Lsb = 19};
						enum {FieldMask = 0x00080000};
						enum {Value_PciSet = 0x1};
						enum {ValueMask_PciSet = 0x00080000};
						enum {Value_AhbClear = 0x1};
						enum {ValueMask_AhbClear = 0x00080000};
						enum {Value_Pending = 0x1};
						enum {ValueMask_Pending = 0x00080000};
						enum {Value_NotPending = 0x0};
						enum {ValueMask_NotPending = 0x00000000};
						};
					namespace Bit20 { // Field Description
						enum {Lsb = 20};
						enum {FieldMask = 0x00100000};
						enum {Value_PciSet = 0x1};
						enum {ValueMask_PciSet = 0x00100000};
						enum {Value_AhbClear = 0x1};
						enum {ValueMask_AhbClear = 0x00100000};
						enum {Value_Pending = 0x1};
						enum {ValueMask_Pending = 0x00100000};
						enum {Value_NotPending = 0x0};
						enum {ValueMask_NotPending = 0x00000000};
						};
					namespace Bit21 { // Field Description
						enum {Lsb = 21};
						enum {FieldMask = 0x00200000};
						enum {Value_PciSet = 0x1};
						enum {ValueMask_PciSet = 0x00200000};
						enum {Value_AhbClear = 0x1};
						enum {ValueMask_AhbClear = 0x00200000};
						enum {Value_Pending = 0x1};
						enum {ValueMask_Pending = 0x00200000};
						enum {Value_NotPending = 0x0};
						enum {ValueMask_NotPending = 0x00000000};
						};
					namespace Bit22 { // Field Description
						enum {Lsb = 22};
						enum {FieldMask = 0x00400000};
						enum {Value_PciSet = 0x1};
						enum {ValueMask_PciSet = 0x00400000};
						enum {Value_AhbClear = 0x1};
						enum {ValueMask_AhbClear = 0x00400000};
						enum {Value_Pending = 0x1};
						enum {ValueMask_Pending = 0x00400000};
						enum {Value_NotPending = 0x0};
						enum {ValueMask_NotPending = 0x00000000};
						};
					namespace Bit23 { // Field Description
						enum {Lsb = 23};
						enum {FieldMask = 0x00800000};
						enum {Value_PciSet = 0x1};
						enum {ValueMask_PciSet = 0x00800000};
						enum {Value_AhbClear = 0x1};
						enum {ValueMask_AhbClear = 0x00800000};
						enum {Value_Pending = 0x1};
						enum {ValueMask_Pending = 0x00800000};
						enum {Value_NotPending = 0x0};
						enum {ValueMask_NotPending = 0x00000000};
						};
					namespace Bit24 { // Field Description
						enum {Lsb = 24};
						enum {FieldMask = 0x01000000};
						enum {Value_PciSet = 0x1};
						enum {ValueMask_PciSet = 0x01000000};
						enum {Value_AhbClear = 0x1};
						enum {ValueMask_AhbClear = 0x01000000};
						enum {Value_Pending = 0x1};
						enum {ValueMask_Pending = 0x01000000};
						enum {Value_NotPending = 0x0};
						enum {ValueMask_NotPending = 0x00000000};
						};
					namespace Bit25 { // Field Description
						enum {Lsb = 25};
						enum {FieldMask = 0x02000000};
						enum {Value_PciSet = 0x1};
						enum {ValueMask_PciSet = 0x02000000};
						enum {Value_AhbClear = 0x1};
						enum {ValueMask_AhbClear = 0x02000000};
						enum {Value_Pending = 0x1};
						enum {ValueMask_Pending = 0x02000000};
						enum {Value_NotPending = 0x0};
						enum {ValueMask_NotPending = 0x00000000};
						};
					namespace Bit26 { // Field Description
						enum {Lsb = 26};
						enum {FieldMask = 0x04000000};
						enum {Value_PciSet = 0x1};
						enum {ValueMask_PciSet = 0x04000000};
						enum {Value_AhbClear = 0x1};
						enum {ValueMask_AhbClear = 0x04000000};
						enum {Value_Pending = 0x1};
						enum {ValueMask_Pending = 0x04000000};
						enum {Value_NotPending = 0x0};
						enum {ValueMask_NotPending = 0x00000000};
						};
					namespace Bit27 { // Field Description
						enum {Lsb = 27};
						enum {FieldMask = 0x08000000};
						enum {Value_PciSet = 0x1};
						enum {ValueMask_PciSet = 0x08000000};
						enum {Value_AhbClear = 0x1};
						enum {ValueMask_AhbClear = 0x08000000};
						enum {Value_Pending = 0x1};
						enum {ValueMask_Pending = 0x08000000};
						enum {Value_NotPending = 0x0};
						enum {ValueMask_NotPending = 0x00000000};
						};
					namespace Bit28 { // Field Description
						enum {Lsb = 28};
						enum {FieldMask = 0x10000000};
						enum {Value_PciSet = 0x1};
						enum {ValueMask_PciSet = 0x10000000};
						enum {Value_AhbClear = 0x1};
						enum {ValueMask_AhbClear = 0x10000000};
						enum {Value_Pending = 0x1};
						enum {ValueMask_Pending = 0x10000000};
						enum {Value_NotPending = 0x0};
						enum {ValueMask_NotPending = 0x00000000};
						};
					namespace Bit29 { // Field Description
						enum {Lsb = 29};
						enum {FieldMask = 0x20000000};
						enum {Value_PciSet = 0x1};
						enum {ValueMask_PciSet = 0x20000000};
						enum {Value_AhbClear = 0x1};
						enum {ValueMask_AhbClear = 0x20000000};
						enum {Value_Pending = 0x1};
						enum {ValueMask_Pending = 0x20000000};
						enum {Value_NotPending = 0x0};
						enum {ValueMask_NotPending = 0x00000000};
						};
					namespace Bit30 { // Field Description
						enum {Lsb = 30};
						enum {FieldMask = 0x40000000};
						enum {Value_PciSet = 0x1};
						enum {ValueMask_PciSet = 0x40000000};
						enum {Value_AhbClear = 0x1};
						enum {ValueMask_AhbClear = 0x40000000};
						enum {Value_Pending = 0x1};
						enum {ValueMask_Pending = 0x40000000};
						enum {Value_NotPending = 0x0};
						enum {ValueMask_NotPending = 0x00000000};
						};
					namespace Bit31 { // Field Description
						enum {Lsb = 31};
						enum {FieldMask = 0x80000000};
						enum {Value_PciSet = 0x1};
						enum {ValueMask_PciSet = 0x80000000};
						enum {Value_AhbClear = 0x1};
						enum {ValueMask_AhbClear = 0x80000000};
						enum {Value_Pending = 0x1};
						enum {ValueMask_Pending = 0x80000000};
						enum {Value_NotPending = 0x0};
						enum {ValueMask_NotPending = 0x00000000};
						};
					};
				namespace PCI_PCIDOORBELL { // Register description
					typedef uint32_t	Reg;
					namespace Bit0 { // Field Description
						enum {Lsb = 0};
						enum {FieldMask = 0x00000001};
						enum {Value_AhbSet = 0x1};
						enum {ValueMask_AhbSet = 0x00000001};
						enum {Value_PciClear = 0x1};
						enum {ValueMask_PciClear = 0x00000001};
						enum {Value_Pending = 0x1};
						enum {ValueMask_Pending = 0x00000001};
						enum {Value_NotPending = 0x0};
						enum {ValueMask_NotPending = 0x00000000};
						};
					namespace Bit1 { // Field Description
						enum {Lsb = 1};
						enum {FieldMask = 0x00000002};
						enum {Value_AhbSet = 0x1};
						enum {ValueMask_AhbSet = 0x00000002};
						enum {Value_PciClear = 0x1};
						enum {ValueMask_PciClear = 0x00000002};
						enum {Value_Pending = 0x1};
						enum {ValueMask_Pending = 0x00000002};
						enum {Value_NotPending = 0x0};
						enum {ValueMask_NotPending = 0x00000000};
						};
					namespace Bit2 { // Field Description
						enum {Lsb = 2};
						enum {FieldMask = 0x00000004};
						enum {Value_AhbSet = 0x1};
						enum {ValueMask_AhbSet = 0x00000004};
						enum {Value_PciClear = 0x1};
						enum {ValueMask_PciClear = 0x00000004};
						enum {Value_Pending = 0x1};
						enum {ValueMask_Pending = 0x00000004};
						enum {Value_NotPending = 0x0};
						enum {ValueMask_NotPending = 0x00000000};
						};
					namespace Bit3 { // Field Description
						enum {Lsb = 3};
						enum {FieldMask = 0x00000008};
						enum {Value_AhbSet = 0x1};
						enum {ValueMask_AhbSet = 0x00000008};
						enum {Value_PciClear = 0x1};
						enum {ValueMask_PciClear = 0x00000008};
						enum {Value_Pending = 0x1};
						enum {ValueMask_Pending = 0x00000008};
						enum {Value_NotPending = 0x0};
						enum {ValueMask_NotPending = 0x00000000};
						};
					namespace Bit4 { // Field Description
						enum {Lsb = 4};
						enum {FieldMask = 0x00000010};
						enum {Value_AhbSet = 0x1};
						enum {ValueMask_AhbSet = 0x00000010};
						enum {Value_PciClear = 0x1};
						enum {ValueMask_PciClear = 0x00000010};
						enum {Value_Pending = 0x1};
						enum {ValueMask_Pending = 0x00000010};
						enum {Value_NotPending = 0x0};
						enum {ValueMask_NotPending = 0x00000000};
						};
					namespace Bit5 { // Field Description
						enum {Lsb = 5};
						enum {FieldMask = 0x00000020};
						enum {Value_AhbSet = 0x1};
						enum {ValueMask_AhbSet = 0x00000020};
						enum {Value_PciClear = 0x1};
						enum {ValueMask_PciClear = 0x00000020};
						enum {Value_Pending = 0x1};
						enum {ValueMask_Pending = 0x00000020};
						enum {Value_NotPending = 0x0};
						enum {ValueMask_NotPending = 0x00000000};
						};
					namespace Bit6 { // Field Description
						enum {Lsb = 6};
						enum {FieldMask = 0x00000040};
						enum {Value_AhbSet = 0x1};
						enum {ValueMask_AhbSet = 0x00000040};
						enum {Value_PciClear = 0x1};
						enum {ValueMask_PciClear = 0x00000040};
						enum {Value_Pending = 0x1};
						enum {ValueMask_Pending = 0x00000040};
						enum {Value_NotPending = 0x0};
						enum {ValueMask_NotPending = 0x00000000};
						};
					namespace Bit7 { // Field Description
						enum {Lsb = 7};
						enum {FieldMask = 0x00000080};
						enum {Value_AhbSet = 0x1};
						enum {ValueMask_AhbSet = 0x00000080};
						enum {Value_PciClear = 0x1};
						enum {ValueMask_PciClear = 0x00000080};
						enum {Value_Pending = 0x1};
						enum {ValueMask_Pending = 0x00000080};
						enum {Value_NotPending = 0x0};
						enum {ValueMask_NotPending = 0x00000000};
						};
					namespace Bit8 { // Field Description
						enum {Lsb = 8};
						enum {FieldMask = 0x00000100};
						enum {Value_AhbSet = 0x1};
						enum {ValueMask_AhbSet = 0x00000100};
						enum {Value_PciClear = 0x1};
						enum {ValueMask_PciClear = 0x00000100};
						enum {Value_Pending = 0x1};
						enum {ValueMask_Pending = 0x00000100};
						enum {Value_NotPending = 0x0};
						enum {ValueMask_NotPending = 0x00000000};
						};
					namespace Bit9 { // Field Description
						enum {Lsb = 9};
						enum {FieldMask = 0x00000200};
						enum {Value_AhbSet = 0x1};
						enum {ValueMask_AhbSet = 0x00000200};
						enum {Value_PciClear = 0x1};
						enum {ValueMask_PciClear = 0x00000200};
						enum {Value_Pending = 0x1};
						enum {ValueMask_Pending = 0x00000200};
						enum {Value_NotPending = 0x0};
						enum {ValueMask_NotPending = 0x00000000};
						};
					namespace Bit10 { // Field Description
						enum {Lsb = 10};
						enum {FieldMask = 0x00000400};
						enum {Value_AhbSet = 0x1};
						enum {ValueMask_AhbSet = 0x00000400};
						enum {Value_PciClear = 0x1};
						enum {ValueMask_PciClear = 0x00000400};
						enum {Value_Pending = 0x1};
						enum {ValueMask_Pending = 0x00000400};
						enum {Value_NotPending = 0x0};
						enum {ValueMask_NotPending = 0x00000000};
						};
					namespace Bit11 { // Field Description
						enum {Lsb = 11};
						enum {FieldMask = 0x00000800};
						enum {Value_AhbSet = 0x1};
						enum {ValueMask_AhbSet = 0x00000800};
						enum {Value_PciClear = 0x1};
						enum {ValueMask_PciClear = 0x00000800};
						enum {Value_Pending = 0x1};
						enum {ValueMask_Pending = 0x00000800};
						enum {Value_NotPending = 0x0};
						enum {ValueMask_NotPending = 0x00000000};
						};
					namespace Bit12 { // Field Description
						enum {Lsb = 12};
						enum {FieldMask = 0x00001000};
						enum {Value_AhbSet = 0x1};
						enum {ValueMask_AhbSet = 0x00001000};
						enum {Value_PciClear = 0x1};
						enum {ValueMask_PciClear = 0x00001000};
						enum {Value_Pending = 0x1};
						enum {ValueMask_Pending = 0x00001000};
						enum {Value_NotPending = 0x0};
						enum {ValueMask_NotPending = 0x00000000};
						};
					namespace Bit13 { // Field Description
						enum {Lsb = 13};
						enum {FieldMask = 0x00002000};
						enum {Value_AhbSet = 0x1};
						enum {ValueMask_AhbSet = 0x00002000};
						enum {Value_PciClear = 0x1};
						enum {ValueMask_PciClear = 0x00002000};
						enum {Value_Pending = 0x1};
						enum {ValueMask_Pending = 0x00002000};
						enum {Value_NotPending = 0x0};
						enum {ValueMask_NotPending = 0x00000000};
						};
					namespace Bit14 { // Field Description
						enum {Lsb = 14};
						enum {FieldMask = 0x00004000};
						enum {Value_AhbSet = 0x1};
						enum {ValueMask_AhbSet = 0x00004000};
						enum {Value_PciClear = 0x1};
						enum {ValueMask_PciClear = 0x00004000};
						enum {Value_Pending = 0x1};
						enum {ValueMask_Pending = 0x00004000};
						enum {Value_NotPending = 0x0};
						enum {ValueMask_NotPending = 0x00000000};
						};
					namespace Bit15 { // Field Description
						enum {Lsb = 15};
						enum {FieldMask = 0x00008000};
						enum {Value_AhbSet = 0x1};
						enum {ValueMask_AhbSet = 0x00008000};
						enum {Value_PciClear = 0x1};
						enum {ValueMask_PciClear = 0x00008000};
						enum {Value_Pending = 0x1};
						enum {ValueMask_Pending = 0x00008000};
						enum {Value_NotPending = 0x0};
						enum {ValueMask_NotPending = 0x00000000};
						};
					namespace Bit16 { // Field Description
						enum {Lsb = 16};
						enum {FieldMask = 0x00010000};
						enum {Value_AhbSet = 0x1};
						enum {ValueMask_AhbSet = 0x00010000};
						enum {Value_PciClear = 0x1};
						enum {ValueMask_PciClear = 0x00010000};
						enum {Value_Pending = 0x1};
						enum {ValueMask_Pending = 0x00010000};
						enum {Value_NotPending = 0x0};
						enum {ValueMask_NotPending = 0x00000000};
						};
					namespace Bit17 { // Field Description
						enum {Lsb = 17};
						enum {FieldMask = 0x00020000};
						enum {Value_AhbSet = 0x1};
						enum {ValueMask_AhbSet = 0x00020000};
						enum {Value_PciClear = 0x1};
						enum {ValueMask_PciClear = 0x00020000};
						enum {Value_Pending = 0x1};
						enum {ValueMask_Pending = 0x00020000};
						enum {Value_NotPending = 0x0};
						enum {ValueMask_NotPending = 0x00000000};
						};
					namespace Bit18 { // Field Description
						enum {Lsb = 18};
						enum {FieldMask = 0x00040000};
						enum {Value_AhbSet = 0x1};
						enum {ValueMask_AhbSet = 0x00040000};
						enum {Value_PciClear = 0x1};
						enum {ValueMask_PciClear = 0x00040000};
						enum {Value_Pending = 0x1};
						enum {ValueMask_Pending = 0x00040000};
						enum {Value_NotPending = 0x0};
						enum {ValueMask_NotPending = 0x00000000};
						};
					namespace Bit19 { // Field Description
						enum {Lsb = 19};
						enum {FieldMask = 0x00080000};
						enum {Value_AhbSet = 0x1};
						enum {ValueMask_AhbSet = 0x00080000};
						enum {Value_PciClear = 0x1};
						enum {ValueMask_PciClear = 0x00080000};
						enum {Value_Pending = 0x1};
						enum {ValueMask_Pending = 0x00080000};
						enum {Value_NotPending = 0x0};
						enum {ValueMask_NotPending = 0x00000000};
						};
					namespace Bit20 { // Field Description
						enum {Lsb = 20};
						enum {FieldMask = 0x00100000};
						enum {Value_AhbSet = 0x1};
						enum {ValueMask_AhbSet = 0x00100000};
						enum {Value_PciClear = 0x1};
						enum {ValueMask_PciClear = 0x00100000};
						enum {Value_Pending = 0x1};
						enum {ValueMask_Pending = 0x00100000};
						enum {Value_NotPending = 0x0};
						enum {ValueMask_NotPending = 0x00000000};
						};
					namespace Bit21 { // Field Description
						enum {Lsb = 21};
						enum {FieldMask = 0x00200000};
						enum {Value_AhbSet = 0x1};
						enum {ValueMask_AhbSet = 0x00200000};
						enum {Value_PciClear = 0x1};
						enum {ValueMask_PciClear = 0x00200000};
						enum {Value_Pending = 0x1};
						enum {ValueMask_Pending = 0x00200000};
						enum {Value_NotPending = 0x0};
						enum {ValueMask_NotPending = 0x00000000};
						};
					namespace Bit22 { // Field Description
						enum {Lsb = 22};
						enum {FieldMask = 0x00400000};
						enum {Value_AhbSet = 0x1};
						enum {ValueMask_AhbSet = 0x00400000};
						enum {Value_PciClear = 0x1};
						enum {ValueMask_PciClear = 0x00400000};
						enum {Value_Pending = 0x1};
						enum {ValueMask_Pending = 0x00400000};
						enum {Value_NotPending = 0x0};
						enum {ValueMask_NotPending = 0x00000000};
						};
					namespace Bit23 { // Field Description
						enum {Lsb = 23};
						enum {FieldMask = 0x00800000};
						enum {Value_AhbSet = 0x1};
						enum {ValueMask_AhbSet = 0x00800000};
						enum {Value_PciClear = 0x1};
						enum {ValueMask_PciClear = 0x00800000};
						enum {Value_Pending = 0x1};
						enum {ValueMask_Pending = 0x00800000};
						enum {Value_NotPending = 0x0};
						enum {ValueMask_NotPending = 0x00000000};
						};
					namespace Bit24 { // Field Description
						enum {Lsb = 24};
						enum {FieldMask = 0x01000000};
						enum {Value_AhbSet = 0x1};
						enum {ValueMask_AhbSet = 0x01000000};
						enum {Value_PciClear = 0x1};
						enum {ValueMask_PciClear = 0x01000000};
						enum {Value_Pending = 0x1};
						enum {ValueMask_Pending = 0x01000000};
						enum {Value_NotPending = 0x0};
						enum {ValueMask_NotPending = 0x00000000};
						};
					namespace Bit25 { // Field Description
						enum {Lsb = 25};
						enum {FieldMask = 0x02000000};
						enum {Value_AhbSet = 0x1};
						enum {ValueMask_AhbSet = 0x02000000};
						enum {Value_PciClear = 0x1};
						enum {ValueMask_PciClear = 0x02000000};
						enum {Value_Pending = 0x1};
						enum {ValueMask_Pending = 0x02000000};
						enum {Value_NotPending = 0x0};
						enum {ValueMask_NotPending = 0x00000000};
						};
					namespace Bit26 { // Field Description
						enum {Lsb = 26};
						enum {FieldMask = 0x04000000};
						enum {Value_AhbSet = 0x1};
						enum {ValueMask_AhbSet = 0x04000000};
						enum {Value_PciClear = 0x1};
						enum {ValueMask_PciClear = 0x04000000};
						enum {Value_Pending = 0x1};
						enum {ValueMask_Pending = 0x04000000};
						enum {Value_NotPending = 0x0};
						enum {ValueMask_NotPending = 0x00000000};
						};
					namespace Bit27 { // Field Description
						enum {Lsb = 27};
						enum {FieldMask = 0x08000000};
						enum {Value_AhbSet = 0x1};
						enum {ValueMask_AhbSet = 0x08000000};
						enum {Value_PciClear = 0x1};
						enum {ValueMask_PciClear = 0x08000000};
						enum {Value_Pending = 0x1};
						enum {ValueMask_Pending = 0x08000000};
						enum {Value_NotPending = 0x0};
						enum {ValueMask_NotPending = 0x00000000};
						};
					namespace Bit28 { // Field Description
						enum {Lsb = 28};
						enum {FieldMask = 0x10000000};
						enum {Value_AhbSet = 0x1};
						enum {ValueMask_AhbSet = 0x10000000};
						enum {Value_PciClear = 0x1};
						enum {ValueMask_PciClear = 0x10000000};
						enum {Value_Pending = 0x1};
						enum {ValueMask_Pending = 0x10000000};
						enum {Value_NotPending = 0x0};
						enum {ValueMask_NotPending = 0x00000000};
						};
					namespace Bit29 { // Field Description
						enum {Lsb = 29};
						enum {FieldMask = 0x20000000};
						enum {Value_AhbSet = 0x1};
						enum {ValueMask_AhbSet = 0x20000000};
						enum {Value_PciClear = 0x1};
						enum {ValueMask_PciClear = 0x20000000};
						enum {Value_Pending = 0x1};
						enum {ValueMask_Pending = 0x20000000};
						enum {Value_NotPending = 0x0};
						enum {ValueMask_NotPending = 0x00000000};
						};
					namespace Bit30 { // Field Description
						enum {Lsb = 30};
						enum {FieldMask = 0x40000000};
						enum {Value_AhbSet = 0x1};
						enum {ValueMask_AhbSet = 0x40000000};
						enum {Value_PciClear = 0x1};
						enum {ValueMask_PciClear = 0x40000000};
						enum {Value_Pending = 0x1};
						enum {ValueMask_Pending = 0x40000000};
						enum {Value_NotPending = 0x0};
						enum {ValueMask_NotPending = 0x00000000};
						};
					namespace Bit31 { // Field Description
						enum {Lsb = 31};
						enum {FieldMask = 0x80000000};
						enum {Value_AhbSet = 0x1};
						enum {ValueMask_AhbSet = 0x80000000};
						enum {Value_PciClear = 0x1};
						enum {ValueMask_PciClear = 0x80000000};
						enum {Value_Pending = 0x1};
						enum {ValueMask_Pending = 0x80000000};
						enum {Value_NotPending = 0x0};
						enum {ValueMask_NotPending = 0x00000000};
						};
					};
				namespace PCI_DMA_SRCADDR { // Register description
					typedef uint32_t	Reg;
					};
				namespace PCI_DMA_DESTADDR { // Register description
					typedef uint32_t	Reg;
					};
				namespace PCI_DMA_LENGTH { // Register description
					typedef uint32_t	Reg;
					namespace Wordcount { // Field Description
						enum {Lsb = 0};
						enum {FieldMask = 0x0000FFFF};
						};
					namespace DS { // Field Description
						enum {Lsb = 28};
						enum {FieldMask = 0x10000000};
						enum {Value_ByteSwap = 0x1};
						enum {ValueMask_ByteSwap = 0x10000000};
						enum {Value_NoByteSwap = 0x0};
						enum {ValueMask_NoByteSwap = 0x00000000};
						};
					namespace EN { // Field Description
						enum {Lsb = 31};
						enum {FieldMask = 0x80000000};
						enum {Value_Enable = 0x1};
						enum {ValueMask_Enable = 0x80000000};
						enum {Value_Disable = 0x0};
						enum {ValueMask_Disable = 0x00000000};
						};
					};
				namespace PCI_ATPDMA1_PCIADDR { // Register description
					typedef uint32_t	Reg;
					namespace Bit0 { // Field Description
						enum {Lsb = 0};
						enum {FieldMask = 0x00000001};
						};
					};
				};
			};
		};
	};
#endif
