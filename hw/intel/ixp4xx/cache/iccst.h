/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _hw_mot8xx_cache_iccsth_
#define _hw_mot8xx_cache_iccsth_
#include <stdint.h>
namespace Oscl { // Namespace description
	namespace Mot8xx { // Namespace description
		namespace Cache { // Namespace description
			namespace IcCst { // Register description
				typedef uint32_t	Reg;
				namespace IEN { // Field Description
					enum {Lsb = 31};
					enum {FieldMask = 0x80000000};
					enum {Value_CacheDisabled = 0x0};
					enum {ValueMask_CacheDisabled = 0x00000000};
					enum {Value_CacheEnabled = 0x1};
					enum {ValueMask_CacheEnabled = 0x80000000};
					};
				namespace CMD { // Field Description
					enum {Lsb = 25};
					enum {FieldMask = 0x0E000000};
					enum {Value_CacheEnable = 0x1};
					enum {ValueMask_CacheEnable = 0x02000000};
					enum {Value_CacheDisable = 0x2};
					enum {ValueMask_CacheDisable = 0x04000000};
					enum {Value_LoadAndLockCacheBlock = 0x3};
					enum {ValueMask_LoadAndLockCacheBlock = 0x06000000};
					enum {Value_UnlockCacheBlock = 0x4};
					enum {ValueMask_UnlockCacheBlock = 0x08000000};
					enum {Value_UnlockAll = 0x5};
					enum {ValueMask_UnlockAll = 0x0A000000};
					enum {Value_InvalidateAll = 0x6};
					enum {ValueMask_InvalidateAll = 0x0C000000};
					};
				namespace CCER1 { // Field Description
					enum {Lsb = 21};
					enum {FieldMask = 0x00200000};
					enum {Value_NoError = 0x0};
					enum {ValueMask_NoError = 0x00000000};
					enum {Value_BusError = 0x1};
					enum {ValueMask_BusError = 0x00200000};
					};
				namespace CCER2 { // Field Description
					enum {Lsb = 20};
					enum {FieldMask = 0x00100000};
					enum {Value_NoError = 0x0};
					enum {ValueMask_NoError = 0x00000000};
					enum {Value_NoWayAvailable = 0x1};
					enum {ValueMask_NoWayAvailable = 0x00100000};
					};
				namespace CCER3 { // Field Description
					enum {Lsb = 19};
					enum {FieldMask = 0x00080000};
					};
				};
			};
		};
	};
#endif
