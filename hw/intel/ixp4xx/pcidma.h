/*
   Copyright (C) 2005 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/
#ifndef _oscl_hw_intel_ixp4xx_pcidmah_
#define _oscl_hw_intel_ixp4xx_pcidmah_
#include "pcicsrreg.h"

namespace Oscl {
namespace IXP4XX {
namespace PCI {
namespace CSR {

/**	This is the structure of an IXP4XX PCI DMA Controller.
 */
struct PciDmaController {
	PCI_DMA_SRCADDR::Reg	sourceAddress;
	PCI_DMA_DESTADDR::Reg	destinationAddress;
	PCI_DMA_LENGTH::Reg		lengthAndControl;
	};

}
}
}
}

#endif

