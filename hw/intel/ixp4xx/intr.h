/*
   Copyright (C) 2006 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_hw_intel_ixp4xx_intrh_
#define _oscl_hw_intel_ixp4xx_intrh_

#include "intrreg.h"
/** */
namespace Oscl {
/** */
namespace IXP4XX {
/** */
namespace INTR {

/**	This is the structure of the IXP4XX Console UART registers.
	Starts at 0xC8003000
 */
struct InterruptController {
	/** 0xC8003000 */
	volatile INTR_ST::Reg		intr_st;
	/** 0xC8003004 */
	INTR_EN::Reg				intr_en;
	/** 0xC8003008 */
	INTR_SEL::Reg				intr_sel;
	/** 0xC800300C */
	volatile INTR_IRQ_ST::Reg	intr_irq_st;
	/** 0xC8003010 */
	volatile INTR_FIQ_ST::Reg	intr_fiq_st;
	/** 0xC8003014 */
	volatile INTR_PRTY::Reg		intr_prty;
	/** 0xC8003018 */
	volatile INTR_ENC_ST::Reg	intr_irq_enc_st;
	/** 0xC800301C */
	volatile INTR_ENC_ST::Reg	intr_fiq_enc_st;
	};

}
}
}
#endif
