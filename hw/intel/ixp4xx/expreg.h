#ifndef _hw_intel_expregh_
#define _hw_intel_expregh_
#include <stdint.h>
namespace Oscl { // Namespace description
	namespace IXP4XX { // Namespace description
		namespace EXP { // Namespace description
			namespace TIMING_CSx { // Register description
				typedef uint32_t	Reg;
				namespace BYTE_EN { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x00000001};
					enum {Value_ExpansionBus16BitsWide = 0x0};
					enum {ValueMask_ExpansionBus16BitsWide = 0x00000000};
					enum {Value_ExpansionBus8BitsWide = 0x1};
					enum {ValueMask_ExpansionBus8BitsWide = 0x00000001};
					};
				namespace WR_EN { // Field Description
					enum {Lsb = 1};
					enum {FieldMask = 0x00000002};
					enum {Value_WritesDisable = 0x0};
					enum {ValueMask_WritesDisable = 0x00000000};
					enum {Value_WritesEnable = 0x1};
					enum {ValueMask_WritesEnable = 0x00000002};
					};
				namespace SPLT_EN { // Field Description
					enum {Lsb = 3};
					enum {FieldMask = 0x00000008};
					enum {Value_AhbSplitTransfersDisable = 0x0};
					enum {ValueMask_AhbSplitTransfersDisable = 0x00000000};
					enum {Value_AhbSplitTransfersEnable = 0x1};
					enum {ValueMask_AhbSplitTransfersEnable = 0x00000008};
					};
				namespace MUX_EN { // Field Description
					enum {Lsb = 4};
					enum {FieldMask = 0x00000010};
					enum {Value_SeparateAddressAndDataBuses = 0x0};
					enum {ValueMask_SeparateAddressAndDataBuses = 0x00000000};
					enum {Value_MultiplexedAddressDataBus = 0x1};
					enum {ValueMask_MultiplexedAddressDataBus = 0x00000010};
					};
				namespace HRDY_POL { // Field Description
					enum {Lsb = 5};
					enum {FieldMask = 0x00000020};
					enum {Value_PolarityLowTrue = 0x0};
					enum {ValueMask_PolarityLowTrue = 0x00000000};
					enum {Value_PolarityHighTrue = 0x1};
					enum {ValueMask_PolarityHighTrue = 0x00000020};
					};
				namespace BYTE_RD16 { // Field Description
					enum {Lsb = 6};
					enum {FieldMask = 0x00000040};
					enum {Value_ByteAccessEnable = 0x1};
					enum {ValueMask_ByteAccessEnable = 0x00000040};
					enum {Value_ByteAccessDisable = 0x0};
					enum {ValueMask_ByteAccessDisable = 0x00000000};
					};
				namespace CNFG { // Field Description
					enum {Lsb = 10};
					enum {FieldMask = 0x00003C00};
					enum {Value_AddressSpace512Bytes = 0x0};
					enum {ValueMask_AddressSpace512Bytes = 0x00000000};
					enum {Value_AddressSpace1KBytes = 0x1};
					enum {ValueMask_AddressSpace1KBytes = 0x00000400};
					enum {Value_AddressSpace2KBytes = 0x2};
					enum {ValueMask_AddressSpace2KBytes = 0x00000800};
					enum {Value_AddressSpace4KBytes = 0x3};
					enum {ValueMask_AddressSpace4KBytes = 0x00000C00};
					enum {Value_AddressSpace8KBytes = 0x4};
					enum {ValueMask_AddressSpace8KBytes = 0x00001000};
					enum {Value_AddressSpace16KBytes = 0x5};
					enum {ValueMask_AddressSpace16KBytes = 0x00001400};
					enum {Value_AddressSpace32KBytes = 0x6};
					enum {ValueMask_AddressSpace32KBytes = 0x00001800};
					enum {Value_AddressSpace64KBytes = 0x7};
					enum {ValueMask_AddressSpace64KBytes = 0x00001C00};
					enum {Value_AddressSpace128KBytes = 0x8};
					enum {ValueMask_AddressSpace128KBytes = 0x00002000};
					enum {Value_AddressSpace256KBytes = 0x9};
					enum {ValueMask_AddressSpace256KBytes = 0x00002400};
					enum {Value_AddressSpace512KBytes = 0xA};
					enum {ValueMask_AddressSpace512KBytes = 0x00002800};
					enum {Value_AddressSpace1MBytes = 0xB};
					enum {ValueMask_AddressSpace1MBytes = 0x00002C00};
					enum {Value_AddressSpace2MBytes = 0xC};
					enum {ValueMask_AddressSpace2MBytes = 0x00003000};
					enum {Value_AddressSpace4MBytes = 0xD};
					enum {ValueMask_AddressSpace4MBytes = 0x00003400};
					enum {Value_AddressSpace8MBytes = 0xE};
					enum {ValueMask_AddressSpace8MBytes = 0x00003800};
					enum {Value_AddressSpace16MBytes = 0xF};
					enum {ValueMask_AddressSpace16MBytes = 0x00003C00};
					};
				namespace CYCLE_TYPE { // Field Description
					enum {Lsb = 14};
					enum {FieldMask = 0x0000C000};
					enum {Value_IntelCycles = 0x0};
					enum {ValueMask_IntelCycles = 0x00000000};
					enum {Value_MotorolaCycles = 0x1};
					enum {ValueMask_MotorolaCycles = 0x00004000};
					enum {Value_HpiCycles = 0x2};
					enum {ValueMask_HpiCycles = 0x00008000};
					};
				namespace T5 { // Field Description
					enum {Lsb = 16};
					enum {FieldMask = 0x000F0000};
					enum {Value_Normal = 0x0};
					enum {ValueMask_Normal = 0x00000000};
					enum {Value_ExtendRecoveryPhaseBy1Clock = 0x1};
					enum {ValueMask_ExtendRecoveryPhaseBy1Clock = 0x00010000};
					enum {Value_ExtendRecoveryPhaseBy2Clocks = 0x2};
					enum {ValueMask_ExtendRecoveryPhaseBy2Clocks = 0x00020000};
					enum {Value_ExtendRecoveryPhaseBy3Clocks = 0x3};
					enum {ValueMask_ExtendRecoveryPhaseBy3Clocks = 0x00030000};
					enum {Value_ExtendRecoveryPhaseBy4Clocks = 0x4};
					enum {ValueMask_ExtendRecoveryPhaseBy4Clocks = 0x00040000};
					enum {Value_ExtendRecoveryPhaseBy5Clocks = 0x5};
					enum {ValueMask_ExtendRecoveryPhaseBy5Clocks = 0x00050000};
					enum {Value_ExtendRecoveryPhaseBy6Clocks = 0x6};
					enum {ValueMask_ExtendRecoveryPhaseBy6Clocks = 0x00060000};
					enum {Value_ExtendRecoveryPhaseBy7Clocks = 0x7};
					enum {ValueMask_ExtendRecoveryPhaseBy7Clocks = 0x00070000};
					enum {Value_ExtendRecoveryPhaseBy8Clocks = 0x8};
					enum {ValueMask_ExtendRecoveryPhaseBy8Clocks = 0x00080000};
					enum {Value_ExtendRecoveryPhaseBy9Clocks = 0x9};
					enum {ValueMask_ExtendRecoveryPhaseBy9Clocks = 0x00090000};
					enum {Value_ExtendRecoveryPhaseBy10Clocks = 0xA};
					enum {ValueMask_ExtendRecoveryPhaseBy10Clocks = 0x000A0000};
					enum {Value_ExtendRecoveryPhaseBy11Clocks = 0xB};
					enum {ValueMask_ExtendRecoveryPhaseBy11Clocks = 0x000B0000};
					enum {Value_ExtendRecoveryPhaseBy12Clocks = 0xC};
					enum {ValueMask_ExtendRecoveryPhaseBy12Clocks = 0x000C0000};
					enum {Value_ExtendRecoveryPhaseBy13Clocks = 0xD};
					enum {ValueMask_ExtendRecoveryPhaseBy13Clocks = 0x000D0000};
					enum {Value_ExtendRecoveryPhaseBy14Clocks = 0xE};
					enum {ValueMask_ExtendRecoveryPhaseBy14Clocks = 0x000E0000};
					enum {Value_ExtendRecoveryPhaseBy15Clocks = 0xF};
					enum {ValueMask_ExtendRecoveryPhaseBy15Clocks = 0x000F0000};
					};
				namespace T4 { // Field Description
					enum {Lsb = 20};
					enum {FieldMask = 0x00300000};
					enum {Value_Normal = 0x0};
					enum {ValueMask_Normal = 0x00000000};
					enum {Value_ExtendHoldPhaseBy1Clock = 0x1};
					enum {ValueMask_ExtendHoldPhaseBy1Clock = 0x00100000};
					enum {Value_ExtendHoldPhaseBy2Clocks = 0x2};
					enum {ValueMask_ExtendHoldPhaseBy2Clocks = 0x00200000};
					enum {Value_ExtendHoldPhaseBy3Clocks = 0x3};
					enum {ValueMask_ExtendHoldPhaseBy3Clocks = 0x00300000};
					};
				namespace T3 { // Field Description
					enum {Lsb = 22};
					enum {FieldMask = 0x03C00000};
					enum {Value_Normal = 0x0};
					enum {ValueMask_Normal = 0x00000000};
					enum {Value_ExtendStrobePhaseBy1Clock = 0x1};
					enum {ValueMask_ExtendStrobePhaseBy1Clock = 0x00400000};
					enum {Value_ExtendStrobePhaseBy2Clocks = 0x2};
					enum {ValueMask_ExtendStrobePhaseBy2Clocks = 0x00800000};
					enum {Value_ExtendStrobePhaseBy3Clocks = 0x3};
					enum {ValueMask_ExtendStrobePhaseBy3Clocks = 0x00C00000};
					enum {Value_ExtendStrobePhaseBy4Clocks = 0x4};
					enum {ValueMask_ExtendStrobePhaseBy4Clocks = 0x01000000};
					enum {Value_ExtendStrobePhaseBy5Clocks = 0x5};
					enum {ValueMask_ExtendStrobePhaseBy5Clocks = 0x01400000};
					enum {Value_ExtendStrobePhaseBy6Clocks = 0x6};
					enum {ValueMask_ExtendStrobePhaseBy6Clocks = 0x01800000};
					enum {Value_ExtendStrobePhaseBy7Clocks = 0x7};
					enum {ValueMask_ExtendStrobePhaseBy7Clocks = 0x01C00000};
					enum {Value_ExtendStrobePhaseBy8Clocks = 0x8};
					enum {ValueMask_ExtendStrobePhaseBy8Clocks = 0x02000000};
					enum {Value_ExtendStrobePhaseBy9Clocks = 0x9};
					enum {ValueMask_ExtendStrobePhaseBy9Clocks = 0x02400000};
					enum {Value_ExtendStrobePhaseBy10Clocks = 0xA};
					enum {ValueMask_ExtendStrobePhaseBy10Clocks = 0x02800000};
					enum {Value_ExtendStrobePhaseBy11Clocks = 0xB};
					enum {ValueMask_ExtendStrobePhaseBy11Clocks = 0x02C00000};
					enum {Value_ExtendStrobePhaseBy12Clocks = 0xC};
					enum {ValueMask_ExtendStrobePhaseBy12Clocks = 0x03000000};
					enum {Value_ExtendStrobePhaseBy13Clocks = 0xD};
					enum {ValueMask_ExtendStrobePhaseBy13Clocks = 0x03400000};
					enum {Value_ExtendStrobePhaseBy14Clocks = 0xE};
					enum {ValueMask_ExtendStrobePhaseBy14Clocks = 0x03800000};
					enum {Value_ExtendStrobePhaseBy15Clocks = 0xF};
					enum {ValueMask_ExtendStrobePhaseBy15Clocks = 0x03C00000};
					};
				namespace T2 { // Field Description
					enum {Lsb = 26};
					enum {FieldMask = 0x0C000000};
					enum {Value_Normal = 0x0};
					enum {ValueMask_Normal = 0x00000000};
					enum {Value_ExtendSetupPhaseBy1Clock = 0x1};
					enum {ValueMask_ExtendSetupPhaseBy1Clock = 0x04000000};
					enum {Value_ExtendSetupPhaseBy2Clocks = 0x2};
					enum {ValueMask_ExtendSetupPhaseBy2Clocks = 0x08000000};
					enum {Value_ExtendSetupPhaseBy3Clocks = 0x3};
					enum {ValueMask_ExtendSetupPhaseBy3Clocks = 0x0C000000};
					};
				namespace T1 { // Field Description
					enum {Lsb = 28};
					enum {FieldMask = 0x30000000};
					enum {Value_Normal = 0x0};
					enum {ValueMask_Normal = 0x00000000};
					enum {Value_ExtendAddressPhase1Clock = 0x1};
					enum {ValueMask_ExtendAddressPhase1Clock = 0x10000000};
					enum {Value_ExtendAddressPhayse2Clocks = 0x2};
					enum {ValueMask_ExtendAddressPhayse2Clocks = 0x20000000};
					enum {Value_ExtendAddressPhayse3Clocks = 0x3};
					enum {ValueMask_ExtendAddressPhayse3Clocks = 0x30000000};
					};
				namespace CSx_EN { // Field Description
					enum {Lsb = 31};
					enum {FieldMask = 0x80000000};
					enum {Value_Enable = 0x1};
					enum {ValueMask_Enable = 0x80000000};
					enum {Value_Disable = 0x0};
					enum {ValueMask_Disable = 0x00000000};
					};
				};
			namespace CNFG0 { // Register description
				typedef uint32_t	Reg;
				namespace FlashBusWidth { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x00000001};
					enum {Value_Width16Bits = 0x0};
					enum {ValueMask_Width16Bits = 0x00000000};
					enum {Value_Width8Bits = 0x1};
					enum {ValueMask_Width8Bits = 0x00000001};
					};
				namespace PCI_HOST { // Field Description
					enum {Lsb = 1};
					enum {FieldMask = 0x00000002};
					enum {Value_PccControllerNonHost = 0x0};
					enum {ValueMask_PccControllerNonHost = 0x00000000};
					enum {Value_PccControllerAsHost = 0x1};
					enum {ValueMask_PccControllerAsHost = 0x00000002};
					};
				namespace PCI_ARB { // Field Description
					enum {Lsb = 2};
					enum {FieldMask = 0x00000004};
					enum {Value_Disable = 0x0};
					enum {ValueMask_Disable = 0x00000000};
					enum {Value_Enable = 0x1};
					enum {ValueMask_Enable = 0x00000004};
					};
				namespace PCI_CLK { // Field Description
					enum {Lsb = 4};
					enum {FieldMask = 0x00000010};
					enum {Value_PciClock33MHz = 0x0};
					enum {ValueMask_PciClock33MHz = 0x00000000};
					enum {Value_PciClock66MHz = 0x1};
					enum {ValueMask_PciClock66MHz = 0x00000010};
					};
				namespace ClockSet { // Field Description
					enum {Lsb = 21};
					enum {FieldMask = 0x00E00000};
					};
				namespace MEM_MAP { // Field Description
					enum {Lsb = 31};
					enum {FieldMask = 0x80000000};
					enum {Value_LocateAt0x50000000 = 0x0};
					enum {ValueMask_LocateAt0x50000000 = 0x00000000};
					enum {Value_LocateAt0x00000000 = 0x1};
					enum {ValueMask_LocateAt0x00000000 = 0x80000000};
					};
				};
			namespace CNFG1 { // Register description
				typedef uint32_t	Reg;
				namespace SW_INT0 { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x00000001};
					enum {Value_GenerateInterrupt = 0x1};
					enum {ValueMask_GenerateInterrupt = 0x00000001};
					enum {Value_DisableInterrupt = 0x0};
					enum {ValueMask_DisableInterrupt = 0x00000000};
					};
				namespace SW_INT1 { // Field Description
					enum {Lsb = 1};
					enum {FieldMask = 0x00000002};
					enum {Value_GenerateInterrupt = 0x1};
					enum {ValueMask_GenerateInterrupt = 0x00000002};
					enum {Value_DisableInterrupt = 0x0};
					enum {ValueMask_DisableInterrupt = 0x00000000};
					};
				namespace BYTE_SWAP_EN { // Field Description
					enum {Lsb = 8};
					enum {FieldMask = 0x00000100};
					enum {Value_Disable = 0x0};
					enum {ValueMask_Disable = 0x00000000};
					enum {Value_Enable = 0x1};
					enum {ValueMask_Enable = 0x00000100};
					};
				};
			};
		};
	};
#endif
