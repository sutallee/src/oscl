/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_hw_intel_ixp4xx_maph_
#define _oscl_hw_intel_ixp4xx_maph_
#include <stdint.h>
#include "oscl/hw/intel/ixp4xx/pcicsr.h"
#include "oscl/hw/intel/ixp4xx/exp.h"
#include "oscl/hw/intel/ixp4xx/uart/console.h"
#include "oscl/hw/intel/ixp4xx/uart/highspeed.h"
#include "oscl/hw/intel/ixp4xx/imm.h"

/** This file declares each hardware device as a
	data structure. The actual declaration is in
	the link.map file which assigns absolute addresses
	at link time.
 */

enum {IntelIxp4xxSdramSize = (64*1024*1024)};
enum {IntelIxp4xxPciBusSize = (128*1024*1024)};
enum {IntelIxp4xxExpansionBusSize = (256*1024*1024)};
enum {IntelIxp4xxQueueManagerSize = (64*1024*1024)};
enum {IntelIxp4xxPciConfigRegSize = (64*1024*1024)};
enum {IntelIxp4xxExpansionConfigRegSize = (64*1024*1024)};
enum {IntelIxp4xxHighSpeedUartSize = (1*1024)};
enum {IntelIxp4xxConsoleUartSize = (1*1024)};
enum {IntelIxp4xxInternalBusPerfMonSize = (1*1024)};
enum {IntelIxp4xxInterruptControllerSize = (1*1024)};
enum {IntelIxp4xxGpioControllerSize = (1*1024)};
enum {IntelIxp4xxTimersSize = (1*1024)};
enum {IntelIxp4xxWanHssNpeSize = (1*1024)};
enum {IntelIxp4xxEnetNpeASize = (1*1024)};
enum {IntelIxp4xxEnetNpeBSize = (1*1024)};
enum {IntelIxp4xxEnetMacASize = (1*1024)};
enum {IntelIxp4xxEnetMacBSize = (1*1024)};
enum {IntelIxp4xxUsbControllerSize = (1*1024)};
enum {IntelIxp4xxSdramConfigSize = (1*1024)};

extern Oscl::Intel::IXP4xx::Map	IntelIxp4xxMap;
extern uint8_t   IntelIxp4xxSDRAM[IntelIxp4xxSdramSize];
extern uint8_t   IntelIxp4xxPciBus[IntelIxp4xxPciBusSize];
extern uint8_t   IntelIxp4xxExpansionBus[IntelIxp4xxExpansionBusSize];
extern uint8_t   IntelIxp4xxQueueManager[IntelIxp4xxQueueManagerSize];
// FIXME: Need register defs
extern Oscl::IXP4XX::PCI::CSR::PciCsr   IntelIxp4xxPciConfigReg;
//extern uint8_t   IntelIxp4xxExpansionConfigReg[IntelIxp4xxExpansionConfigRegSize];
extern Oscl::IXP4XX::EXP::ExpansionBus   IntelIxp4xxExpansionConfigReg;
extern Oscl::IXP4XX::UART::HighSpeed::Registers	IntelIxp4xxHighSpeedUART;
extern Oscl::IXP4XX::UART::Console::Registers	IntelIxp4xxConsoleUART;
extern uint8_t   IntelIxp4xxInternalBusPerfMon[IntelIxp4xxInternalBusPerfMonSize];
extern uint8_t   IntelIxp4xxInterruptController[IntelIxp4xxInterruptControllerSize];
extern uint8_t   IntelIxp4xxGpioController[IntelIxp4xxGpioControllerSize];
extern uint8_t   IntelIxp4xxTimers[IntelIxp4xxTimersSize];
extern uint8_t   IntelIxp4xxWanHssNpe[IntelIxp4xxWanHssNpeSize];
extern uint8_t   IntelIxp4xxEnetNpeA[IntelIxp4xxEnetNpeASize];
extern uint8_t   IntelIxp4xxEnetNpeB[IntelIxp4xxEnetNpeBSize];
extern uint8_t   IntelIxp4xxEnetMacA[IntelIxp4xxEnetMacASize];
extern uint8_t   IntelIxp4xxEnetMacB[IntelIxp4xxEnetMacBSize];
extern uint8_t   IntelIxp4xxUsbController[IntelIxp4xxUsbControllerSize];
extern uint8_t   IntelIxp4xxSdramConfig[IntelIxp4xxSdramConfigSize];

#endif
