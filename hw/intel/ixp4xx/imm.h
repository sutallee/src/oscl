/*
   Copyright (C) 2006 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_hw_intel_ixp4xx_immh_
#define _oscl_hw_intel_ixp4xx_immh_
#include "oscl/hw/intel/ixp4xx/uart/console.h"
#include "oscl/hw/intel/ixp4xx/uart/highspeed.h"
#include "oscl/hw/intel/ixp4xx/intr.h"
#include "oscl/hw/intel/ixp4xx/ost.h"
#include "oscl/hw/intel/ixp4xx/gpio.h"

/** */
namespace Oscl {
/** */
namespace Intel {
/** */
namespace IXP4xx {

/** PCI Controller Configuration and Status Registers (64MB) */
struct PciController {
	/** */
	uint32_t	stuff[64*1024*1024/sizeof(uint32_t)];
	};

/** Expansion Bus Configuration Registers (64MB) */
struct ExpansionBusConfig {
	/** */
	uint32_t	stuff[64*1024*1024/sizeof(uint32_t)];
	};

/** High Speed UART (1KB) */
struct HighSpeedUART {
	/** */
	Oscl::IXP4XX::UART::HighSpeed::Registers	registers;
	uint8_t									reserved[4096-sizeof(Oscl::IXP4XX::UART::HighSpeed::Registers)];
	};

/** Console UART (1KB) */
struct ConsoleUART {
	/** */
	Oscl::IXP4XX::UART::Console::Registers	registers;
	uint8_t										reserved[4096-sizeof(Oscl::IXP4XX::UART::Console::Registers)];
	};

/** Internal Bus Performance Monitoring Unit (1KB) */
struct InternalBusPMU {
	/** */
	uint32_t								stuff[4096/sizeof(uint32_t)];
	};

/** Interrupt Controller (1KB) */
struct InterruptController {
	/** */
	Oscl::IXP4XX::INTR::InterruptController	intr;
	/** */
	uint8_t								stuff[4096-sizeof(Oscl::IXP4XX::INTR::InterruptController)];
	};

/** GPIO Controller (1KB) */
struct GpioController {
	Oscl::IXP4XX::GPIO::GpioRegisters		gpio;
	/** */
	uint8_t								stuff[4096-sizeof(Oscl::IXP4XX::GPIO::GpioRegisters)];
	};

/** Timers (1KB) */
struct Timers {
	/** */
	Oscl::IXP4XX::OST::TimerRegisters		ost;
	uint8_t								stuff[4096-sizeof(Oscl::IXP4XX::OST::TimerRegisters)];
	};

/** WAN_HSS_NPE (1KB) */
struct WanHssNpe {
	/** */
	uint32_t								stuff[4096/sizeof(uint32_t)];
	};

/** Ethernet NPE (1KB) */
struct EthernetNPE {
	/** */
	uint32_t								stuff[4096/sizeof(uint32_t)];
	};

/** Ethernet MAC (1KB) */
struct EthernetMac {
	/** */
	uint32_t								stuff[4096/sizeof(uint32_t)];
	};

/** USB Controller (1KB) */
struct UsbController {
	/** */
	uint32_t								stuff[4096/sizeof(uint32_t)];
	};

/** SDRAM Configuration Registers (256 bytes) */
struct SdramConfig {
	/** */
	uint32_t								stuff[256/sizeof(uint32_t)];
	};

/** */
struct Map {
	/** PCI Controller 0xC0000000 */
	PciController					pciController;

	/** Expansion Bus Configuration 0xC4000000 */
	ExpansionBusConfig				expansionBusConfig;

	/** High Speed UART 0xC8000000 */
	HighSpeedUART					highSpeedUART;

	/** High Speed UART 0xC8001000 */
	ConsoleUART						consoleUART;

	/** Internal Bus Performance Monitoring Unit 0xC8002000*/
	InternalBusPMU					internalBusPMU;

	/** Interrupt Controller 0xC8003000 */
	InterruptController				interruptController;

	/** GPIO Controller 0xC8004000 */
	GpioController					gpioController;

	/** Timers 0xC8005000 */
	Timers							timers;

	/** WAN/HSS NPE 0xC8006000 */
	WanHssNpe						wan_hss_npe;

	/** Ethernet NPE A 0xC8007000 */
	EthernetNPE						npea;

	/** Ethernet NPEBA 0xC8008000 */
	EthernetNPE						npeb;

	/** Ethernet MAC A 0xC8009000 */
	EthernetMac						maca;

	/** Ethernet MAC B 0xC800A000 */
	EthernetMac						macb;

	/** USB Controller 0xC800B000 */
	UsbController					usbController;

	/** Reserved 0xC800C000 */
	uint32_t						reserved1[(0xC8010000-0xC800C000)/sizeof(uint32_t)];

	/** Reserved 0xC8010000 */
	uint32_t						reserved2[(0xCC000000-0xC8010000)/sizeof(uint32_t)];

	/** SDRAM Configuration Registers 0xCC000000 */
	SdramConfig						sdramConfig;

	/** Reserved 0xC8000100 */
	uint32_t						reserved3[(0xD0000000-0xCC000100)/sizeof(uint32_t)];

	/** Reserved 0xD0000000 */
	uint32_t						reserved4[((0xFFFFfFFF-0xD0000000)+1)/sizeof(uint32_t)];
	};

}
}
}

#endif
