/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _hw_mot8xx_mictrh_
#define _hw_mot8xx_mictrh_
#include <stdint.h>
namespace Oscl { // Namespace description
	namespace Mot8xx { // Namespace description
		namespace Mmu { // Namespace description
			namespace MiCtr { // Register description
				typedef uint32_t	Reg;
				namespace GPM { // Field Description
					enum {Lsb = 31};
					enum {FieldMask = 0x80000000};
					enum {Value_PowerPcMode = 0x0};
					enum {ValueMask_PowerPcMode = 0x00000000};
					enum {Value_DomainMgrMode = 0x0};
					enum {ValueMask_DomainMgrMode = 0x00000000};
					};
				namespace PPM { // Field Description
					enum {Lsb = 30};
					enum {FieldMask = 0x40000000};
					enum {Value_ProtectPage = 0x0};
					enum {ValueMask_ProtectPage = 0x00000000};
					enum {Value_Protect1KFor4Kpage = 0x1};
					enum {ValueMask_Protect1KFor4Kpage = 0x40000000};
					};
				namespace CIDEF { // Field Description
					enum {Lsb = 29};
					enum {FieldMask = 0x20000000};
					enum {Value_Allow = 0x0};
					enum {ValueMask_Allow = 0x00000000};
					enum {Value_Inhibit = 0x1};
					enum {ValueMask_Inhibit = 0x20000000};
					};
				namespace RSV4I { // Field Description
					enum {Lsb = 27};
					enum {FieldMask = 0x08000000};
					enum {Value_Modulo32 = 0x0};
					enum {ValueMask_Modulo32 = 0x00000000};
					enum {Value_Modulo28 = 0x1};
					enum {ValueMask_Modulo28 = 0x08000000};
					};
				namespace PPCS { // Field Description
					enum {Lsb = 25};
					enum {FieldMask = 0x02000000};
					enum {Value_Ignore = 0x0};
					enum {ValueMask_Ignore = 0x00000000};
					enum {Value_Compare = 0x1};
					enum {ValueMask_Compare = 0x02000000};
					};
				namespace ITLB_INDX { // Field Description
					enum {Lsb = 8};
					enum {FieldMask = 0x00001F00};
					};
				};
			};
		};
	};
#endif
