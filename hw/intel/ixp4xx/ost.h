/*
   Copyright (C) 2006 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_hw_intel_ixp4xx_osth_
#define _oscl_hw_intel_ixp4xx_osth_

#include "ostreg.h"
/** */
namespace Oscl {
/** */
namespace IXP4XX {
/** */
namespace OST {

/**	This is the structure of the IXP4XX OST timer registers.
	Starts at 0xC8005000
 */
struct TimerRegisters {
	/** 0xC8005000 */
	OST_TS::Reg			ost_ts;
	/** 0xC8005004 */
	OST_TIM::Reg		ost_tim0;
	/** 0xC8005008 */
	OST_TIM_RL::Reg		ost_tim0_rl;
	/** 0xC800500C */
	OST_TIM::Reg		ost_tim1;
	/** 0xC8005010 */
	OST_TIM_RL::Reg		ost_tim1_rl;
	/** 0xC8005014 */
	OST_WDOG::Reg		ost_wdog;
	/** 0xC8005018 */
	OST_WDOG_ENAB::Reg	ost_wdog_enab;
	/** 0xC800501C */
	OST_WDOG_KEY::Reg	ost_wdog_key;
	/** 0xC8005020 */
	OST_STATUS::Reg		ost_status;
	};

}
}
}
#endif
