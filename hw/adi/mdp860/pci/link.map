/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

/*
	MPC8xx Chip selects have a granularity of 32KB.
	IMMR - Internal Peripherals (16K max, 64K boundary)
		Mapped @ FF000000 - FF003FFF
	CS0 - Jumper to select 1MB On-board Flash Sockets.
		JP19 - Jumper ON	CS0 to 16-bit wide 2 x 29F040 Flash Sockets
		JP18 - Jumper OFF	CS0 to 32-bit wide FlashSIMM
		This flash will be used for Boot ROM.
		Mapped @ FFE00000 - FFEFFFFF
	CS1 - Unmapped Upper bank of Flash SIMM
	CS2 - Lower bank of DRAM SIMM (16MB Max)
		Mapped @ 00000000 - 00FFFFFF
	CS3 - Unmapped Upper bank of DRAM SIMM (16MB Max)
	CS4 - Static RAM (2MB Max) (32-bit port)
		Mapped @ 30000000 - 3FFFFFFF (256MB)
		Actual @ 30000000 - 301FFFFF (2MB)
		Note: To keep this accessible to PCI bus masters and
			to keep the PCI slave aperature at 1GB size, this
			SRAM should be mapped into the bottom !GB of memory
			with other RAM.
	CS5 - Discrete Control/Status Registers and EEPROM (see notes)
		Mapped @ FF200000 - FF203FFF	(16K)
		Mapped @ FF200000 - FF201FFF	(Control registers)
		Mapped @ FF202000 - FF203FFF	(EEPROM)
	CS6 - PCI bus aperature selected
		JP26 Jumper on pins 1-2 to select PCI
		Mapped @ 80000000 - 8FFFFFFF	(1GB)
	CS7 - QSPAN
		JP27 Jumper on pins 1-2 to select QSPAN
		Mapped @ FF100000 - FF100FFF	(4KB actual)
	====================================================
	Physical Memory Map: first------last-----size-----bar------mask---mask size
	CS0 -	Flash		FFF00000 - FFFFFFFF	(1MB)	FFF00000 FFF00000 (1MB)
	CS5 -	Discrete	FF200000 - FF203FFF	(16K)	FF200000 FFFF8000 (32K)
	CS7 -	QSPAN		FF100000 - FF100FFF	(4KB)	FF100000 FFFF8000 (32K)
	IMMR -	QUICC		FF000000 - FF003FFF	(16K)	FF000000 FFFF0000 (64K?)
	CS6 -	PCI			80000000 - 8FFFFFFF	(1GB)	80000000 C0000000 (1GB)
	CS4 -	SRAM		30000000 - 301FFFFF	(2MB)	30000000 FFE00000 (2MB)
	CS2 -	DRAM		00000000 - 00FFFFFF (16MB)	00000000 FFC00000 (4MB)
	CS1 -	FlashSIMM
	CS3 -	DRAM2
	====================================================
 */

PROVIDE(QspanPciBridge = 0xFF100000);
PROVIDE(Mdp8xxPCI = 0x80000000);

