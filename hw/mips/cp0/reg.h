#ifndef _hw_mips_cp0_regh_
#define _hw_mips_cp0_regh_
#include <stdint.h>
namespace Oscl { // Namespace description
	namespace HW { // Namespace description
		namespace MIPS { // Namespace description
			namespace CP0 { // Namespace description
				namespace Index { // Register description
					typedef uint32_t	Reg;
					namespace P { // Field Description
						enum {Lsb = 31};
						enum {FieldMask = 0x80000000};
						enum {Value_Match = 0x1};
						enum {ValueMask_Match = 0x80000000};
						enum {Value_NoMatch = 0x0};
						enum {ValueMask_NoMatch = 0x00000000};
						};
					namespace Index { // Field Description
						enum {Lsb = 0};
						enum {FieldMask = 0x7FFFFFFF};
						};
					};
				namespace Random { // Register description
					typedef uint32_t	Reg;
					};
				namespace EntryLo { // Register description
					typedef uint32_t	Reg;
					namespace PFN { // Field Description
						enum {Lsb = 6};
						enum {FieldMask = 0x3FFFFFC0};
						};
					namespace C { // Field Description
						enum {Lsb = 3};
						enum {FieldMask = 0x00000038};
						enum {Value_Uncached = 0x2};
						enum {ValueMask_Uncached = 0x00000010};
						enum {Value_Cacheable = 0x3};
						enum {ValueMask_Cacheable = 0x00000018};
						};
					namespace D { // Field Description
						enum {Lsb = 2};
						enum {FieldMask = 0x00000004};
						enum {Value_Dirty = 0x1};
						enum {ValueMask_Dirty = 0x00000004};
						enum {Value_Unwritten = 0x0};
						enum {ValueMask_Unwritten = 0x00000000};
						};
					namespace V { // Field Description
						enum {Lsb = 1};
						enum {FieldMask = 0x00000002};
						enum {Value_Valid = 0x1};
						enum {ValueMask_Valid = 0x00000002};
						enum {Value_Invalid = 0x0};
						enum {ValueMask_Invalid = 0x00000000};
						};
					namespace G { // Field Description
						enum {Lsb = 0};
						enum {FieldMask = 0x00000001};
						enum {Value_IgnoreASID = 0x1};
						enum {ValueMask_IgnoreASID = 0x00000001};
						enum {Value_CompareASID = 0x0};
						enum {ValueMask_CompareASID = 0x00000000};
						};
					};
				namespace Context { // Register description
					typedef uint32_t	Reg;
					namespace PTEBase { // Field Description
						enum {Lsb = 23};
						enum {FieldMask = 0xFF800000};
						};
					namespace BadVPN2 { // Field Description
						enum {Lsb = 4};
						enum {FieldMask = 0x007FFFF0};
						};
					};
				namespace PageMask { // Register description
					typedef uint32_t	Reg;
					namespace Mask { // Field Description
						enum {Lsb = 13};
						enum {FieldMask = 0x1FFFE000};
						enum {Value_Size4KB = 0x0};
						enum {ValueMask_Size4KB = 0x00000000};
						enum {Value_Size16KB = 0x3};
						enum {ValueMask_Size16KB = 0x00006000};
						enum {Value_Size64KB = 0xF};
						enum {ValueMask_Size64KB = 0x0001E000};
						enum {Value_Size256KB = 0x3F};
						enum {ValueMask_Size256KB = 0x0007E000};
						enum {Value_Size1MB = 0xFF};
						enum {ValueMask_Size1MB = 0x001FE000};
						enum {Value_Size4MB = 0x3FF};
						enum {ValueMask_Size4MB = 0x007FE000};
						enum {Value_Size16MB = 0xFFF};
						enum {ValueMask_Size16MB = 0x01FFE000};
						enum {Value_Size64MB = 0x3FFF};
						enum {ValueMask_Size64MB = 0x07FFE000};
						enum {Value_Size256MB = 0xFFFF};
						enum {ValueMask_Size256MB = 0x1FFFE000};
						};
					};
				namespace Wired { // Register description
					typedef uint32_t	Reg;
					};
				namespace BadVAddr { // Register description
					typedef uint32_t	Reg;
					};
				namespace Count { // Register description
					typedef uint32_t	Reg;
					};
				namespace EntryHi { // Register description
					typedef uint32_t	Reg;
					namespace VPN2 { // Field Description
						enum {Lsb = 13};
						enum {FieldMask = 0xFFFFE000};
						};
					namespace ASID { // Field Description
						enum {Lsb = 0};
						enum {FieldMask = 0x000000FF};
						};
					};
				namespace Compare { // Register description
					typedef uint32_t	Reg;
					};
				namespace Status { // Register description
					typedef uint32_t	Reg;
					namespace CU { // Field Description
						enum {Lsb = 28};
						enum {FieldMask = 0xF0000000};
						enum {Value_AccessAllowed = 0x1};
						enum {ValueMask_AccessAllowed = 0x10000000};
						enum {Value_NoAccess = 0x0};
						enum {ValueMask_NoAccess = 0x00000000};
						namespace CU3 { // Field Description
							enum {Lsb = 31};
							enum {FieldMask = 0x80000000};
							enum {Value_AccessAllowed = 0x1};
							enum {ValueMask_AccessAllowed = 0x80000000};
							enum {Value_NoAccess = 0x0};
							enum {ValueMask_NoAccess = 0x00000000};
							};
						namespace CU2 { // Field Description
							enum {Lsb = 30};
							enum {FieldMask = 0x40000000};
							enum {Value_AccessAllowed = 0x1};
							enum {ValueMask_AccessAllowed = 0x40000000};
							enum {Value_NoAccess = 0x0};
							enum {ValueMask_NoAccess = 0x00000000};
							};
						namespace CU1 { // Field Description
							enum {Lsb = 29};
							enum {FieldMask = 0x20000000};
							enum {Value_AccessAllowed = 0x1};
							enum {ValueMask_AccessAllowed = 0x20000000};
							enum {Value_NoAccess = 0x0};
							enum {ValueMask_NoAccess = 0x00000000};
							};
						namespace CU0 { // Field Description
							enum {Lsb = 28};
							enum {FieldMask = 0x10000000};
							enum {Value_AccessAllowed = 0x1};
							enum {ValueMask_AccessAllowed = 0x10000000};
							enum {Value_NoAccess = 0x0};
							enum {ValueMask_NoAccess = 0x00000000};
							};
						};
					namespace RP { // Field Description
						enum {Lsb = 27};
						enum {FieldMask = 0x08000000};
						enum {Value_ReducedPowerMode = 0x1};
						enum {ValueMask_ReducedPowerMode = 0x08000000};
						enum {Value_NormalPowerMode = 0x0};
						enum {ValueMask_NormalPowerMode = 0x00000000};
						};
					namespace FR { // Field Description
						enum {Lsb = 26};
						enum {FieldMask = 0x04000000};
						};
					namespace RE { // Field Description
						enum {Lsb = 25};
						enum {FieldMask = 0x02000000};
						enum {Value_UserModeConfigEndianess = 0x0};
						enum {ValueMask_UserModeConfigEndianess = 0x00000000};
						enum {Value_UserModeReverseEndianess = 0x1};
						enum {ValueMask_UserModeReverseEndianess = 0x02000000};
						};
					namespace MX { // Field Description
						enum {Lsb = 24};
						enum {FieldMask = 0x01000000};
						};
					namespace PX { // Field Description
						enum {Lsb = 23};
						enum {FieldMask = 0x00800000};
						};
					namespace BEV { // Field Description
						enum {Lsb = 22};
						enum {FieldMask = 0x00400000};
						enum {Value_Normal = 0x0};
						enum {ValueMask_Normal = 0x00000000};
						enum {Value_Bootstrap = 0x1};
						enum {ValueMask_Bootstrap = 0x00400000};
						};
					namespace TS { // Field Description
						enum {Lsb = 21};
						enum {FieldMask = 0x00200000};
						enum {Value_Clear = 0x0};
						enum {ValueMask_Clear = 0x00000000};
						enum {Value_MatchedMultipleTlbEntries = 0x1};
						enum {ValueMask_MatchedMultipleTlbEntries = 0x00200000};
						enum {Value_Normal = 0x0};
						enum {ValueMask_Normal = 0x00000000};
						};
					namespace SR { // Field Description
						enum {Lsb = 20};
						enum {FieldMask = 0x00100000};
						enum {Value_NotSoftReset = 0x0};
						enum {ValueMask_NotSoftReset = 0x00000000};
						enum {Value_SoftReset = 0x1};
						enum {ValueMask_SoftReset = 0x00100000};
						};
					namespace NMI { // Field Description
						enum {Lsb = 19};
						enum {FieldMask = 0x00080000};
						enum {Value_NotNMI = 0x0};
						enum {ValueMask_NotNMI = 0x00000000};
						enum {Value_NMI = 0x1};
						enum {ValueMask_NMI = 0x00080000};
						};
					namespace IM { // Field Description
						enum {Lsb = 8};
						enum {FieldMask = 0x0000FF00};
						enum {Value_Disable = 0x0};
						enum {ValueMask_Disable = 0x00000000};
						enum {Value_Enable = 0x1};
						enum {ValueMask_Enable = 0x00000100};
						enum {Value_Disabled = 0x0};
						enum {ValueMask_Disabled = 0x00000000};
						enum {Value_Enabled = 0x1};
						enum {ValueMask_Enabled = 0x00000100};
						namespace IM7 { // Field Description
							enum {Lsb = 15};
							enum {FieldMask = 0x00008000};
							enum {Value_Disable = 0x0};
							enum {ValueMask_Disable = 0x00000000};
							enum {Value_Enable = 0x1};
							enum {ValueMask_Enable = 0x00008000};
							enum {Value_Disabled = 0x0};
							enum {ValueMask_Disabled = 0x00000000};
							enum {Value_Enabled = 0x1};
							enum {ValueMask_Enabled = 0x00008000};
							};
						namespace IM6 { // Field Description
							enum {Lsb = 14};
							enum {FieldMask = 0x00004000};
							enum {Value_Disable = 0x0};
							enum {ValueMask_Disable = 0x00000000};
							enum {Value_Enable = 0x1};
							enum {ValueMask_Enable = 0x00004000};
							enum {Value_Disabled = 0x0};
							enum {ValueMask_Disabled = 0x00000000};
							enum {Value_Enabled = 0x1};
							enum {ValueMask_Enabled = 0x00004000};
							};
						namespace IM5 { // Field Description
							enum {Lsb = 13};
							enum {FieldMask = 0x00002000};
							enum {Value_Disable = 0x0};
							enum {ValueMask_Disable = 0x00000000};
							enum {Value_Enable = 0x1};
							enum {ValueMask_Enable = 0x00002000};
							enum {Value_Disabled = 0x0};
							enum {ValueMask_Disabled = 0x00000000};
							enum {Value_Enabled = 0x1};
							enum {ValueMask_Enabled = 0x00002000};
							};
						namespace IM4 { // Field Description
							enum {Lsb = 12};
							enum {FieldMask = 0x00001000};
							enum {Value_Disable = 0x0};
							enum {ValueMask_Disable = 0x00000000};
							enum {Value_Enable = 0x1};
							enum {ValueMask_Enable = 0x00001000};
							enum {Value_Disabled = 0x0};
							enum {ValueMask_Disabled = 0x00000000};
							enum {Value_Enabled = 0x1};
							enum {ValueMask_Enabled = 0x00001000};
							};
						namespace IM3 { // Field Description
							enum {Lsb = 11};
							enum {FieldMask = 0x00000800};
							enum {Value_Disable = 0x0};
							enum {ValueMask_Disable = 0x00000000};
							enum {Value_Enable = 0x1};
							enum {ValueMask_Enable = 0x00000800};
							enum {Value_Disabled = 0x0};
							enum {ValueMask_Disabled = 0x00000000};
							enum {Value_Enabled = 0x1};
							enum {ValueMask_Enabled = 0x00000800};
							};
						namespace IM2 { // Field Description
							enum {Lsb = 10};
							enum {FieldMask = 0x00000400};
							enum {Value_Disable = 0x0};
							enum {ValueMask_Disable = 0x00000000};
							enum {Value_Enable = 0x1};
							enum {ValueMask_Enable = 0x00000400};
							enum {Value_Disabled = 0x0};
							enum {ValueMask_Disabled = 0x00000000};
							enum {Value_Enabled = 0x1};
							enum {ValueMask_Enabled = 0x00000400};
							};
						namespace IM1 { // Field Description
							enum {Lsb = 9};
							enum {FieldMask = 0x00000200};
							enum {Value_Disable = 0x0};
							enum {ValueMask_Disable = 0x00000000};
							enum {Value_Enable = 0x1};
							enum {ValueMask_Enable = 0x00000200};
							enum {Value_Disabled = 0x0};
							enum {ValueMask_Disabled = 0x00000000};
							enum {Value_Enabled = 0x1};
							enum {ValueMask_Enabled = 0x00000200};
							};
						namespace IM0 { // Field Description
							enum {Lsb = 8};
							enum {FieldMask = 0x00000100};
							enum {Value_Disable = 0x0};
							enum {ValueMask_Disable = 0x00000000};
							enum {Value_Enable = 0x1};
							enum {ValueMask_Enable = 0x00000100};
							enum {Value_Disabled = 0x0};
							enum {ValueMask_Disabled = 0x00000000};
							enum {Value_Enabled = 0x1};
							enum {ValueMask_Enabled = 0x00000100};
							};
						};
					namespace KX { // Field Description
						enum {Lsb = 7};
						enum {FieldMask = 0x00000080};
						};
					namespace SX { // Field Description
						enum {Lsb = 6};
						enum {FieldMask = 0x00000040};
						};
					namespace UX { // Field Description
						enum {Lsb = 5};
						enum {FieldMask = 0x00000020};
						};
					namespace KSU { // Field Description
						enum {Lsb = 3};
						enum {FieldMask = 0x00000018};
						enum {Value_KernelMode = 0x0};
						enum {ValueMask_KernelMode = 0x00000000};
						enum {Value_SupervisorMode = 0x1};
						enum {ValueMask_SupervisorMode = 0x00000008};
						enum {Value_UserMode = 0x2};
						enum {ValueMask_UserMode = 0x00000010};
						namespace UM { // Field Description
							enum {Lsb = 4};
							enum {FieldMask = 0x00000010};
							enum {Value_User = 0x1};
							enum {ValueMask_User = 0x00000010};
							enum {Value_Kernel = 0x1};
							enum {ValueMask_Kernel = 0x00000010};
							};
						namespace R0 { // Field Description
							enum {Lsb = 3};
							enum {FieldMask = 0x00000008};
							enum {Value_Supervisor = 0x1};
							enum {ValueMask_Supervisor = 0x00000008};
							enum {Value_Normal = 0x0};
							enum {ValueMask_Normal = 0x00000000};
							};
						};
					namespace ERL { // Field Description
						enum {Lsb = 2};
						enum {FieldMask = 0x00000004};
						enum {Value_Normal = 0x0};
						enum {ValueMask_Normal = 0x00000000};
						enum {Value_Error = 0x1};
						enum {ValueMask_Error = 0x00000004};
						};
					namespace EXL { // Field Description
						enum {Lsb = 1};
						enum {FieldMask = 0x00000002};
						enum {Value_Normal = 0x0};
						enum {ValueMask_Normal = 0x00000000};
						enum {Value_Exception = 0x1};
						enum {ValueMask_Exception = 0x00000002};
						};
					namespace IE { // Field Description
						enum {Lsb = 0};
						enum {FieldMask = 0x00000001};
						enum {Value_InterruptsDisabled = 0x0};
						enum {ValueMask_InterruptsDisabled = 0x00000000};
						enum {Value_InterruptsEnabled = 0x1};
						enum {ValueMask_InterruptsEnabled = 0x00000001};
						enum {Value_InterruptsDisable = 0x0};
						enum {ValueMask_InterruptsDisable = 0x00000000};
						enum {Value_InterruptsEnable = 0x1};
						enum {ValueMask_InterruptsEnable = 0x00000001};
						};
					};
				namespace SRSCtl { // Register description
					typedef uint32_t	Reg;
					namespace HSS { // Field Description
						enum {Lsb = 26};
						enum {FieldMask = 0x3C000000};
						};
					namespace EICSS { // Field Description
						enum {Lsb = 18};
						enum {FieldMask = 0x003C0000};
						};
					namespace ESS { // Field Description
						enum {Lsb = 12};
						enum {FieldMask = 0x0000F000};
						};
					namespace PSS { // Field Description
						enum {Lsb = 6};
						enum {FieldMask = 0x000003C0};
						};
					namespace CSS { // Field Description
						enum {Lsb = 0};
						enum {FieldMask = 0x0000000F};
						};
					};
				namespace Cause { // Register description
					typedef uint32_t	Reg;
					namespace BD { // Field Description
						enum {Lsb = 31};
						enum {FieldMask = 0x80000000};
						enum {Value_NotInDelaySlot = 0x0};
						enum {ValueMask_NotInDelaySlot = 0x00000000};
						enum {Value_InDelaySlot = 0x1};
						enum {ValueMask_InDelaySlot = 0x80000000};
						};
					namespace CE { // Field Description
						enum {Lsb = 28};
						enum {FieldMask = 0x30000000};
						};
					namespace IV { // Field Description
						enum {Lsb = 23};
						enum {FieldMask = 0x00800000};
						enum {Value_UseGeneralVector0x180 = 0x0};
						enum {ValueMask_UseGeneralVector0x180 = 0x00000000};
						enum {Value_UseSpecialVector0x200 = 0x1};
						enum {ValueMask_UseSpecialVector0x200 = 0x00800000};
						};
					namespace WP { // Field Description
						enum {Lsb = 22};
						enum {FieldMask = 0x00400000};
						enum {Value_WatchExceptionDeferred = 0x1};
						enum {ValueMask_WatchExceptionDeferred = 0x00400000};
						enum {Value_Clear = 0x0};
						enum {ValueMask_Clear = 0x00000000};
						};
					namespace IP { // Field Description
						enum {Lsb = 10};
						enum {FieldMask = 0x0000FC00};
						enum {Value_NonePending = 0x0};
						enum {ValueMask_NonePending = 0x00000000};
						namespace IP2 { // Field Description
							enum {Lsb = 10};
							enum {FieldMask = 0x00000400};
							enum {Value_Pending = 0x1};
							enum {ValueMask_Pending = 0x00000400};
							};
						namespace IP3 { // Field Description
							enum {Lsb = 11};
							enum {FieldMask = 0x00000800};
							enum {Value_Pending = 0x1};
							enum {ValueMask_Pending = 0x00000800};
							};
						namespace IP4 { // Field Description
							enum {Lsb = 12};
							enum {FieldMask = 0x00001000};
							enum {Value_Pending = 0x1};
							enum {ValueMask_Pending = 0x00001000};
							};
						namespace IP5 { // Field Description
							enum {Lsb = 13};
							enum {FieldMask = 0x00002000};
							enum {Value_Pending = 0x1};
							enum {ValueMask_Pending = 0x00002000};
							};
						namespace IP6 { // Field Description
							enum {Lsb = 14};
							enum {FieldMask = 0x00004000};
							enum {Value_Pending = 0x1};
							enum {ValueMask_Pending = 0x00004000};
							};
						namespace IP7 { // Field Description
							enum {Lsb = 15};
							enum {FieldMask = 0x00008000};
							enum {Value_Pending = 0x1};
							enum {ValueMask_Pending = 0x00008000};
							};
						};
					namespace ExcCode { // Field Description
						enum {Lsb = 2};
						enum {FieldMask = 0x0000007C};
						enum {Value_Int = 0x0};
						enum {ValueMask_Int = 0x00000000};
						enum {Value_Mod = 0x1};
						enum {ValueMask_Mod = 0x00000004};
						enum {Value_TLBL = 0x2};
						enum {ValueMask_TLBL = 0x00000008};
						enum {Value_TLBS = 0x3};
						enum {ValueMask_TLBS = 0x0000000C};
						enum {Value_AdEL = 0x4};
						enum {ValueMask_AdEL = 0x00000010};
						enum {Value_AdES = 0x5};
						enum {ValueMask_AdES = 0x00000014};
						enum {Value_IBE = 0x6};
						enum {ValueMask_IBE = 0x00000018};
						enum {Value_DBE = 0x7};
						enum {ValueMask_DBE = 0x0000001C};
						enum {Value_Sys = 0x8};
						enum {ValueMask_Sys = 0x00000020};
						enum {Value_Bp = 0x9};
						enum {ValueMask_Bp = 0x00000024};
						enum {Value_RI = 0xA};
						enum {ValueMask_RI = 0x00000028};
						enum {Value_CpU = 0xB};
						enum {ValueMask_CpU = 0x0000002C};
						enum {Value_Ov = 0xC};
						enum {ValueMask_Ov = 0x00000030};
						enum {Value_Tr = 0xD};
						enum {ValueMask_Tr = 0x00000034};
						enum {Value_FPE = 0xF};
						enum {ValueMask_FPE = 0x0000003C};
						enum {Value_C2E = 0x12};
						enum {ValueMask_C2E = 0x00000048};
						enum {Value_MDMX = 0x16};
						enum {ValueMask_MDMX = 0x00000058};
						enum {Value_WATCH = 0x17};
						enum {ValueMask_WATCH = 0x0000005C};
						enum {Value_MCheck = 0x18};
						enum {ValueMask_MCheck = 0x00000060};
						enum {Value_CacheErr = 0x1E};
						enum {ValueMask_CacheErr = 0x00000078};
						};
					};
				namespace EPC { // Register description
					typedef uint32_t	Reg;
					};
				namespace PRId { // Register description
					typedef uint32_t	Reg;
					namespace CompanyOptions { // Field Description
						enum {Lsb = 24};
						enum {FieldMask = 0xFF000000};
						};
					namespace CompanyID { // Field Description
						enum {Lsb = 16};
						enum {FieldMask = 0x00FF0000};
						enum {Value_NotMIPS32orMIPS64 = 0x0};
						enum {ValueMask_NotMIPS32orMIPS64 = 0x00000000};
						enum {Value_MIPSTechnologiesInc = 0x1};
						enum {ValueMask_MIPSTechnologiesInc = 0x00010000};
						};
					namespace ProcessorID { // Field Description
						enum {Lsb = 8};
						enum {FieldMask = 0x0000FF00};
						};
					namespace Revision { // Field Description
						enum {Lsb = 0};
						enum {FieldMask = 0x000000FF};
						};
					};
				namespace Config { // Register description
					typedef uint32_t	Reg;
					namespace M { // Field Description
						enum {Lsb = 31};
						enum {FieldMask = 0x80000000};
						enum {Value_Config1Implemented = 0x1};
						enum {ValueMask_Config1Implemented = 0x80000000};
						enum {Value_Config1NotImplemented = 0x0};
						enum {ValueMask_Config1NotImplemented = 0x00000000};
						};
					namespace Impl { // Field Description
						enum {Lsb = 16};
						enum {FieldMask = 0x7FFF0000};
						};
					namespace BE { // Field Description
						enum {Lsb = 15};
						enum {FieldMask = 0x00008000};
						enum {Value_LittleEndian = 0x0};
						enum {ValueMask_LittleEndian = 0x00000000};
						enum {Value_BigEndian = 0x1};
						enum {ValueMask_BigEndian = 0x00008000};
						};
					namespace AT { // Field Description
						enum {Lsb = 13};
						enum {FieldMask = 0x00006000};
						enum {Value_MIPS32 = 0x0};
						enum {ValueMask_MIPS32 = 0x00000000};
						enum {Value_MIPS64_32Seg = 0x1};
						enum {ValueMask_MIPS64_32Seg = 0x00002000};
						enum {Value_MIPS64_AllSeg = 0x2};
						enum {ValueMask_MIPS64_AllSeg = 0x00004000};
						};
					namespace AR { // Field Description
						enum {Lsb = 10};
						enum {FieldMask = 0x00001C00};
						enum {Value_Revision1 = 0x0};
						enum {ValueMask_Revision1 = 0x00000000};
						};
					namespace MT { // Field Description
						enum {Lsb = 7};
						enum {FieldMask = 0x00000380};
						enum {Value_None = 0x0};
						enum {ValueMask_None = 0x00000000};
						enum {Value_StandardTLB = 0x1};
						enum {ValueMask_StandardTLB = 0x00000080};
						enum {Value_StandardBAT = 0x2};
						enum {ValueMask_StandardBAT = 0x00000100};
						enum {Value_StandardFixedMap = 0x3};
						enum {ValueMask_StandardFixedMap = 0x00000180};
						};
					namespace K0 { // Field Description
						enum {Lsb = 0};
						enum {FieldMask = 0x00000007};
						enum {Value_Uncached = 0x2};
						enum {ValueMask_Uncached = 0x00000002};
						enum {Value_Cacheable = 0x3};
						enum {ValueMask_Cacheable = 0x00000003};
						};
					};
				namespace Config1 { // Register description
					typedef uint32_t	Reg;
					namespace M { // Field Description
						enum {Lsb = 31};
						enum {FieldMask = 0x80000000};
						enum {Value_Config2Implemented = 0x1};
						enum {ValueMask_Config2Implemented = 0x80000000};
						enum {Value_Config2NotImplemented = 0x0};
						enum {ValueMask_Config2NotImplemented = 0x00000000};
						};
					namespace MMUSizeMinus1 { // Field Description
						enum {Lsb = 25};
						enum {FieldMask = 0x7E000000};
						};
					namespace IS { // Field Description
						enum {Lsb = 22};
						enum {FieldMask = 0x01C00000};
						enum {Value_Sets64PerWay = 0x0};
						enum {ValueMask_Sets64PerWay = 0x00000000};
						enum {Value_Sets128PerWay = 0x1};
						enum {ValueMask_Sets128PerWay = 0x00400000};
						enum {Value_Sets256PerWay = 0x2};
						enum {ValueMask_Sets256PerWay = 0x00800000};
						enum {Value_Sets512PerWay = 0x3};
						enum {ValueMask_Sets512PerWay = 0x00C00000};
						enum {Value_Sets1024PerWay = 0x4};
						enum {ValueMask_Sets1024PerWay = 0x01000000};
						enum {Value_Sets2048PerWay = 0x5};
						enum {ValueMask_Sets2048PerWay = 0x01400000};
						enum {Value_Sets4096PerWay = 0x6};
						enum {ValueMask_Sets4096PerWay = 0x01800000};
						};
					namespace IL { // Field Description
						enum {Lsb = 19};
						enum {FieldMask = 0x00380000};
						enum {Value_NoIcachePresent = 0x0};
						enum {ValueMask_NoIcachePresent = 0x00000000};
						enum {Value_LineSize4Bytes = 0x1};
						enum {ValueMask_LineSize4Bytes = 0x00080000};
						enum {Value_LineSize8Bytes = 0x2};
						enum {ValueMask_LineSize8Bytes = 0x00100000};
						enum {Value_LineSize16Bytes = 0x3};
						enum {ValueMask_LineSize16Bytes = 0x00180000};
						enum {Value_LineSize32Bytes = 0x4};
						enum {ValueMask_LineSize32Bytes = 0x00200000};
						enum {Value_LineSize64Bytes = 0x5};
						enum {ValueMask_LineSize64Bytes = 0x00280000};
						enum {Value_LineSize128Bytes = 0x6};
						enum {ValueMask_LineSize128Bytes = 0x00300000};
						};
					namespace IA { // Field Description
						enum {Lsb = 16};
						enum {FieldMask = 0x00070000};
						enum {Value_DirectMapped = 0x0};
						enum {ValueMask_DirectMapped = 0x00000000};
						enum {Value_Assoc2Way = 0x1};
						enum {ValueMask_Assoc2Way = 0x00010000};
						enum {Value_Assoc3Way = 0x2};
						enum {ValueMask_Assoc3Way = 0x00020000};
						enum {Value_Assoc4Way = 0x3};
						enum {ValueMask_Assoc4Way = 0x00030000};
						enum {Value_Assoc5Way = 0x4};
						enum {ValueMask_Assoc5Way = 0x00040000};
						enum {Value_Assoc6Way = 0x5};
						enum {ValueMask_Assoc6Way = 0x00050000};
						enum {Value_Assoc7Way = 0x6};
						enum {ValueMask_Assoc7Way = 0x00060000};
						enum {Value_Assoc8Way = 0x7};
						enum {ValueMask_Assoc8Way = 0x00070000};
						};
					namespace DS { // Field Description
						enum {Lsb = 13};
						enum {FieldMask = 0x0000E000};
						enum {Value_Sets64PerWay = 0x0};
						enum {ValueMask_Sets64PerWay = 0x00000000};
						enum {Value_Sets128PerWay = 0x1};
						enum {ValueMask_Sets128PerWay = 0x00002000};
						enum {Value_Sets256PerWay = 0x2};
						enum {ValueMask_Sets256PerWay = 0x00004000};
						enum {Value_Sets512PerWay = 0x3};
						enum {ValueMask_Sets512PerWay = 0x00006000};
						enum {Value_Sets1024PerWay = 0x4};
						enum {ValueMask_Sets1024PerWay = 0x00008000};
						enum {Value_Sets2048PerWay = 0x5};
						enum {ValueMask_Sets2048PerWay = 0x0000A000};
						enum {Value_Sets4096PerWay = 0x6};
						enum {ValueMask_Sets4096PerWay = 0x0000C000};
						};
					namespace DL { // Field Description
						enum {Lsb = 10};
						enum {FieldMask = 0x00001C00};
						enum {Value_NoDcachePresent = 0x0};
						enum {ValueMask_NoDcachePresent = 0x00000000};
						enum {Value_LineSize4Bytes = 0x1};
						enum {ValueMask_LineSize4Bytes = 0x00000400};
						enum {Value_LineSize8Bytes = 0x2};
						enum {ValueMask_LineSize8Bytes = 0x00000800};
						enum {Value_LineSize16Bytes = 0x3};
						enum {ValueMask_LineSize16Bytes = 0x00000C00};
						enum {Value_LineSize32Bytes = 0x4};
						enum {ValueMask_LineSize32Bytes = 0x00001000};
						enum {Value_LineSize64Bytes = 0x5};
						enum {ValueMask_LineSize64Bytes = 0x00001400};
						enum {Value_LineSize128Bytes = 0x6};
						enum {ValueMask_LineSize128Bytes = 0x00001800};
						};
					namespace DA { // Field Description
						enum {Lsb = 7};
						enum {FieldMask = 0x00000380};
						enum {Value_DirectMapped = 0x0};
						enum {ValueMask_DirectMapped = 0x00000000};
						enum {Value_Assoc2Way = 0x1};
						enum {ValueMask_Assoc2Way = 0x00000080};
						enum {Value_Assoc3Way = 0x2};
						enum {ValueMask_Assoc3Way = 0x00000100};
						enum {Value_Assoc4Way = 0x3};
						enum {ValueMask_Assoc4Way = 0x00000180};
						enum {Value_Assoc5Way = 0x4};
						enum {ValueMask_Assoc5Way = 0x00000200};
						enum {Value_Assoc6Way = 0x5};
						enum {ValueMask_Assoc6Way = 0x00000280};
						enum {Value_Assoc7Way = 0x6};
						enum {ValueMask_Assoc7Way = 0x00000300};
						enum {Value_Assoc8Way = 0x7};
						enum {ValueMask_Assoc8Way = 0x00000380};
						};
					namespace C2 { // Field Description
						enum {Lsb = 6};
						enum {FieldMask = 0x00000040};
						enum {Value_NoCoprocessor2Implemented = 0x0};
						enum {ValueMask_NoCoprocessor2Implemented = 0x00000000};
						enum {Value_Coprocessor2Implemented = 0x1};
						enum {ValueMask_Coprocessor2Implemented = 0x00000040};
						};
					namespace MD { // Field Description
						enum {Lsb = 5};
						enum {FieldMask = 0x00000020};
						};
					namespace PC { // Field Description
						enum {Lsb = 4};
						enum {FieldMask = 0x00000010};
						enum {Value_NoPerformanceCounters = 0x0};
						enum {ValueMask_NoPerformanceCounters = 0x00000000};
						enum {Value_PerformanceCountersImplemented = 0x1};
						enum {ValueMask_PerformanceCountersImplemented = 0x00000010};
						};
					namespace WR { // Field Description
						enum {Lsb = 3};
						enum {FieldMask = 0x00000008};
						enum {Value_NoWatchRegisters = 0x0};
						enum {ValueMask_NoWatchRegisters = 0x00000000};
						enum {Value_WatchRegistersImplemented = 0x1};
						enum {ValueMask_WatchRegistersImplemented = 0x00000008};
						};
					namespace CA { // Field Description
						enum {Lsb = 2};
						enum {FieldMask = 0x00000004};
						enum {Value_MIPS16NotImplemented = 0x0};
						enum {ValueMask_MIPS16NotImplemented = 0x00000000};
						enum {Value_MIPS16Implemented = 0x1};
						enum {ValueMask_MIPS16Implemented = 0x00000004};
						};
					namespace EP { // Field Description
						enum {Lsb = 1};
						enum {FieldMask = 0x00000002};
						enum {Value_EJTAGNotImplemented = 0x0};
						enum {ValueMask_EJTAGNotImplemented = 0x00000000};
						enum {Value_EJTAGImplemented = 0x1};
						enum {ValueMask_EJTAGImplemented = 0x00000002};
						};
					namespace FP { // Field Description
						enum {Lsb = 0};
						enum {FieldMask = 0x00000001};
						enum {Value_FPUNotImplemented = 0x0};
						enum {ValueMask_FPUNotImplemented = 0x00000000};
						enum {Value_FPUImplemented = 0x1};
						enum {ValueMask_FPUImplemented = 0x00000001};
						};
					};
				namespace Config2 { // Register description
					typedef uint32_t	Reg;
					namespace M { // Field Description
						enum {Lsb = 31};
						enum {FieldMask = 0x80000000};
						enum {Value_Config3Implemented = 0x1};
						enum {ValueMask_Config3Implemented = 0x80000000};
						enum {Value_Config3NotImplemented = 0x0};
						enum {ValueMask_Config3NotImplemented = 0x00000000};
						};
					namespace TBS { // Field Description
						enum {Lsb = 0};
						enum {FieldMask = 0x3FFFFFFF};
						};
					};
				namespace Config3 { // Register description
					typedef uint32_t	Reg;
					namespace M { // Field Description
						enum {Lsb = 31};
						enum {FieldMask = 0x80000000};
						enum {Value_Config4Implemented = 0x1};
						enum {ValueMask_Config4Implemented = 0x80000000};
						enum {Value_Config4NotImplemented = 0x0};
						enum {ValueMask_Config4NotImplemented = 0x00000000};
						};
					namespace SM { // Field Description
						enum {Lsb = 1};
						enum {FieldMask = 0x00000002};
						enum {Value_SmartMipsAseNotImplemented = 0x0};
						enum {ValueMask_SmartMipsAseNotImplemented = 0x00000000};
						enum {Value_SmartMipsAseImplemented = 0x1};
						enum {ValueMask_SmartMipsAseImplemented = 0x00000002};
						};
					namespace TL { // Field Description
						enum {Lsb = 0};
						enum {FieldMask = 0x00000001};
						enum {Value_TraceLogicNotImplemented = 0x0};
						enum {ValueMask_TraceLogicNotImplemented = 0x00000000};
						enum {Value_TraceLogicImplemented = 0x1};
						enum {ValueMask_TraceLogicImplemented = 0x00000001};
						};
					};
				namespace LLAddr { // Register description
					typedef uint32_t	Reg;
					};
				namespace WatchLo { // Register description
					typedef uint32_t	Reg;
					namespace VAddr { // Field Description
						enum {Lsb = 3};
						enum {FieldMask = 0x7FFFFFF8};
						};
					namespace I { // Field Description
						enum {Lsb = 2};
						enum {FieldMask = 0x00000004};
						enum {Value_InstructionWatchEnabled = 0x1};
						enum {ValueMask_InstructionWatchEnabled = 0x00000004};
						enum {Value_InstructionWatchDisabled = 0x0};
						enum {ValueMask_InstructionWatchDisabled = 0x00000000};
						};
					namespace R { // Field Description
						enum {Lsb = 1};
						enum {FieldMask = 0x00000002};
						enum {Value_ReadWatchEnabled = 0x1};
						enum {ValueMask_ReadWatchEnabled = 0x00000002};
						enum {Value_ReadWatchDisabled = 0x0};
						enum {ValueMask_ReadWatchDisabled = 0x00000000};
						};
					namespace W { // Field Description
						enum {Lsb = 0};
						enum {FieldMask = 0x00000001};
						enum {Value_WriteWatchEnabled = 0x1};
						enum {ValueMask_WriteWatchEnabled = 0x00000001};
						enum {Value_WriteWatchDisabled = 0x0};
						enum {ValueMask_WriteWatchDisabled = 0x00000000};
						};
					};
				namespace WatchHi { // Register description
					typedef uint32_t	Reg;
					namespace M { // Field Description
						enum {Lsb = 31};
						enum {FieldMask = 0x80000000};
						enum {Value_AnotherWatchPairImplemented = 0x1};
						enum {ValueMask_AnotherWatchPairImplemented = 0x80000000};
						enum {Value_AnotherWatchPairNotImplemented = 0x0};
						enum {ValueMask_AnotherWatchPairNotImplemented = 0x00000000};
						};
					namespace G { // Field Description
						enum {Lsb = 30};
						enum {FieldMask = 0x40000000};
						enum {Value_AsidMatchNotRequired = 0x1};
						enum {ValueMask_AsidMatchNotRequired = 0x40000000};
						enum {Value_AsidMatchRequired = 0x0};
						enum {ValueMask_AsidMatchRequired = 0x00000000};
						};
					namespace ASID { // Field Description
						enum {Lsb = 16};
						enum {FieldMask = 0x00FF0000};
						};
					namespace Mask { // Field Description
						enum {Lsb = 3};
						enum {FieldMask = 0x00000FF8};
						};
					};
				namespace Debug { // Register description
					typedef uint32_t	Reg;
					};
				namespace DEPC { // Register description
					typedef uint32_t	Reg;
					};
				namespace PerfCntCtl { // Register description
					typedef uint32_t	Reg;
					namespace M { // Field Description
						enum {Lsb = 31};
						enum {FieldMask = 0x80000000};
						enum {Value_AnotherPairImplemented = 0x1};
						enum {ValueMask_AnotherPairImplemented = 0x80000000};
						enum {Value_AnotherPairNotImplemented = 0x0};
						enum {ValueMask_AnotherPairNotImplemented = 0x00000000};
						};
					namespace Event { // Field Description
						enum {Lsb = 5};
						enum {FieldMask = 0x000007E0};
						};
					namespace IE { // Field Description
						enum {Lsb = 4};
						enum {FieldMask = 0x00000010};
						enum {Value_InterruptDisabled = 0x0};
						enum {ValueMask_InterruptDisabled = 0x00000000};
						enum {Value_InterruptEnabled = 0x1};
						enum {ValueMask_InterruptEnabled = 0x00000010};
						};
					namespace U { // Field Description
						enum {Lsb = 3};
						enum {FieldMask = 0x00000008};
						enum {Value_DisableCountingInUserMode = 0x0};
						enum {ValueMask_DisableCountingInUserMode = 0x00000000};
						enum {Value_EnableCountingInUserMode = 0x1};
						enum {ValueMask_EnableCountingInUserMode = 0x00000008};
						};
					namespace S { // Field Description
						enum {Lsb = 2};
						enum {FieldMask = 0x00000004};
						enum {Value_DisableCountingInSupervisorMode = 0x0};
						enum {ValueMask_DisableCountingInSupervisorMode = 0x00000000};
						enum {Value_EnableCountingInSupervisorMode = 0x1};
						enum {ValueMask_EnableCountingInSupervisorMode = 0x00000004};
						};
					namespace K { // Field Description
						enum {Lsb = 1};
						enum {FieldMask = 0x00000002};
						enum {Value_DisableCountingInKernelMode = 0x0};
						enum {ValueMask_DisableCountingInKernelMode = 0x00000000};
						enum {Value_EnableCountingInKernelMode = 0x1};
						enum {ValueMask_EnableCountingInKernelMode = 0x00000002};
						};
					namespace EXL { // Field Description
						enum {Lsb = 0};
						enum {FieldMask = 0x00000001};
						enum {Value_DisableCountingWhileEXL = 0x0};
						enum {ValueMask_DisableCountingWhileEXL = 0x00000000};
						enum {Value_EnableCountingWhileEXL = 0x1};
						enum {ValueMask_EnableCountingWhileEXL = 0x00000001};
						};
					};
				namespace PerfCntCnt { // Register description
					typedef uint32_t	Reg;
					};
				namespace ErrCtl { // Register description
					typedef uint32_t	Reg;
					};
				namespace CacheErr { // Register description
					typedef uint32_t	Reg;
					};
				namespace TagLo { // Register description
					typedef uint32_t	Reg;
					};
				namespace DataLo { // Register description
					typedef uint32_t	Reg;
					};
				namespace TagHi { // Register description
					typedef uint32_t	Reg;
					};
				namespace DataHi { // Register description
					typedef uint32_t	Reg;
					};
				namespace ErrorEPC { // Register description
					typedef uint32_t	Reg;
					};
				namespace DESAVE { // Register description
					typedef uint32_t	Reg;
					};
				};
			};
		};
	};
#endif
