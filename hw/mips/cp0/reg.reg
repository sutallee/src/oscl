/*
   Copyright (C) 2006 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/


module hw_mips_cp0_regh {
	include "oscl/compiler/types.h";
	namespace Oscl {
		namespace HW {
			namespace MIPS {
				namespace CP0 {
					register Index uint32_t {
						field P 31 1 {
							value Match		1
							value NoMatch	0
							}
						field Index 0 31 { }
						}
					register Random uint32_t { }
					register  EntryLo uint32_t {
						field PFN 6 24 { }
						field C 3 3 {
							value Uncached	2
							value Cacheable	3
							}
						field D 2 1 {
							value Dirty		1
							value Unwritten	0
							}
						field V 1 1 {
							value Valid		1
							value Invalid	0
							}
						field G 0 1 {
							value IgnoreASID	1
							value CompareASID	0
							}
						}
					register  Context uint32_t {
						field PTEBase 23 9 {}
						field BadVPN2 4 19 {}
						}
					register  PageMask uint32_t {
						field Mask 13 16 {
							value Size4KB	0x0000
							value Size16KB	0x0003
							value Size64KB	0x000F
							value Size256KB	0x003F
							value Size1MB	0x00FF
							value Size4MB	0x03FF
							value Size16MB	0x0FFF
							value Size64MB	0x3FFF
							value Size256MB	0xFFFF
							}
						}
					register Wired uint32_t { }
					register BadVAddr uint32_t { }
					register Count uint32_t { }
					register EntryHi uint32_t {
						field VPN2 13 19 { }
						field ASID 0 8 { }
						}
					register Compare uint32_t { }
					register Status uint32_t {
						field CU 28 4 {
							value AccessAllowed	1
							value NoAccess		0
							field CU3 3 1 {
								value AccessAllowed	1
								value NoAccess		0
								}
							field CU2 2 1 {
								value AccessAllowed	1
								value NoAccess		0
								}
							field CU1 1 1 {
								value AccessAllowed	1
								value NoAccess		0
								}
							field CU0 0 1 {
								value AccessAllowed	1
								value NoAccess		0
								}
							}
						field RP 27 1 {
							value ReducedPowerMode	1
							value NormalPowerMode	0
							}
						field FR 26 1 { }
						field RE 25 1 {
							value UserModeConfigEndianess	0
							value UserModeReverseEndianess	1
							}
						field MX 24 1 { }
						field PX 23 1 { }
						field BEV 22 1 {
							value Normal	0
							value Bootstrap	1
							}
						field TS 21 1 {
							value Clear						0
							value MatchedMultipleTlbEntries	1
							value Normal					0
							}
						field SR 20 1 {
							value NotSoftReset	0
							value SoftReset		1
							}
						field NMI 19 1 {
							value NotNMI	0
							value NMI		1
							}
						field IM 8 8 {
							value Disable	0
							value Enable	1
							value Disabled	0
							value Enabled	1
							field IM7 7 1 {
								value Disable	0
								value Enable	1
								value Disabled	0
								value Enabled	1
								}
							field IM6 6 1 {
								value Disable	0
								value Enable	1
								value Disabled	0
								value Enabled	1
								}
							field IM5 5 1 {
								value Disable	0
								value Enable	1
								value Disabled	0
								value Enabled	1
								}
							field IM4 4 1 {
								value Disable	0
								value Enable	1
								value Disabled	0
								value Enabled	1
								}
							field IM3 3 1 {
								value Disable	0
								value Enable	1
								value Disabled	0
								value Enabled	1
								}
							field IM2 2 1 {
								value Disable	0
								value Enable	1
								value Disabled	0
								value Enabled	1
								}
							field IM1 1 1 {
								value Disable	0
								value Enable	1
								value Disabled	0
								value Enabled	1
								}
							field IM0 0 1 {
								value Disable	0
								value Enable	1
								value Disabled	0
								value Enabled	1
								}
							}
						field KX 7 1 {}
						field SX 6 1 {}
						field UX 5 1 {}
						field KSU 3 2 {
							value KernelMode		0x00
							value SupervisorMode	0x01
							value UserMode			0x02
							field UM 1 1 {
								value User		1
								value Kernel	1
								}
							field R0 0 1 {
								value Supervisor	1
								value Normal		0
								}
							}
						field ERL 2 1 {
							value Normal	0
							value Error		1
							}
						field EXL 1 1 {
							value Normal	0
							value Exception	1
							}
						field IE 0 1 {
							value InterruptsDisabled	0
							value InterruptsEnabled		1
							value InterruptsDisable		0
							value InterruptsEnable		1
							}
						}
					register SRSCtl uint32_t {
						field HSS 26 4 { }
						field EICSS 18 4 { }
						field ESS 12 4 { }
						field PSS 6 4 { }
						field CSS 0 4 { }
						}
					register Cause uint32_t {
						field BD 31 1 {
							value NotInDelaySlot	0
							value InDelaySlot		1
							}
						field CE 28 2 { }
						field IV 23 1 {
							value UseGeneralVector0x180	0
							value UseSpecialVector0x200	1
							}
						field WP 22 1 {
							value WatchExceptionDeferred	1
							value Clear						0
							}
						field IP 10 6 {
							value NonePending	0
							field IP2 0 1 {
								value Pending	1
								}
							field IP3 1 1 {
								value Pending	1
								}
							field IP4 2 1 {
								value Pending	1
								}
							field IP5 3 1 {
								value Pending	1
								}
							field IP6 4 1 {
								value Pending	1
								}
							field IP7 5 1 {
								value Pending	1
								}
							}
						field ExcCode 2 5 {
							value Int		0
							value Mod		1
							value TLBL		2
							value TLBS		3
							value AdEL		4
							value AdES		5
							value IBE		6
							value DBE		7
							value Sys		8
							value Bp		9
							value RI		10
							value CpU		11
							value Ov		12
							value Tr		13
							value FPE		15
							value C2E		18
							value MDMX		22
							value WATCH		23
							value MCheck	24
							value CacheErr	30
							}
						}
					register EPC uint32_t { }
					register PRId uint32_t {
						field CompanyOptions 24 8 {}
						field CompanyID 16 8 {
							value NotMIPS32orMIPS64		0
							value MIPSTechnologiesInc	1
							}
						field ProcessorID 8 8 {}
						field Revision 0 8 {}
						}
					register Config uint32_t {
						field M 31 1 {
							value Config1Implemented	1
							value Config1NotImplemented	0
							}
						field Impl 16 15 { }
						field BE 15 1 {
							value LittleEndian	0
							value BigEndian		1
							}
						field AT 13 2 {
							value MIPS32		0
							value MIPS64_32Seg	1
							value MIPS64_AllSeg	2
							}
						field AR 10 3 {
							value Revision1	0
							}
						field MT 7 3 {
							value None				0
							value StandardTLB		1
							value StandardBAT		2
							value StandardFixedMap	3
							}
						field K0 0 3 {
							value Uncached	2
							value Cacheable	3
							}
						}
					register Config1 uint32_t {
						field M 31 1 {
							value Config2Implemented	1
							value Config2NotImplemented	0
							}
						field MMUSizeMinus1 25 6 { }
						field IS 22 3 {
							value Sets64PerWay		0
							value Sets128PerWay		1
							value Sets256PerWay		2
							value Sets512PerWay		3
							value Sets1024PerWay	4
							value Sets2048PerWay	5
							value Sets4096PerWay	6
							}
						field IL 19 3 {
							value NoIcachePresent	0
							value LineSize4Bytes	1
							value LineSize8Bytes	2
							value LineSize16Bytes	3
							value LineSize32Bytes	4
							value LineSize64Bytes	5
							value LineSize128Bytes	6
							}
						field IA 16 3 {
							value DirectMapped	0
							value Assoc2Way		1
							value Assoc3Way		2
							value Assoc4Way		3
							value Assoc5Way		4
							value Assoc6Way		5
							value Assoc7Way		6
							value Assoc8Way		7
							}
						field DS 13 3 {
							value Sets64PerWay		0
							value Sets128PerWay		1
							value Sets256PerWay		2
							value Sets512PerWay		3
							value Sets1024PerWay	4
							value Sets2048PerWay	5
							value Sets4096PerWay	6
							}
						field DL 10 3 {
							value NoDcachePresent	0
							value LineSize4Bytes	1
							value LineSize8Bytes	2
							value LineSize16Bytes	3
							value LineSize32Bytes	4
							value LineSize64Bytes	5
							value LineSize128Bytes	6
							}
						field DA 7 3 {
							value DirectMapped	0
							value Assoc2Way		1
							value Assoc3Way		2
							value Assoc4Way		3
							value Assoc5Way		4
							value Assoc6Way		5
							value Assoc7Way		6
							value Assoc8Way		7
							}
						field C2 6 1 {
							value NoCoprocessor2Implemented		0
							value Coprocessor2Implemented		1
							}
						field MD 5 1 { }
						field PC 4 1 {
							value NoPerformanceCounters				0
							value PerformanceCountersImplemented	1
							}
						field WR 3 1 {
							value NoWatchRegisters			0
							value WatchRegistersImplemented	1
							}
						field CA 2 1 {
							value MIPS16NotImplemented	0
							value MIPS16Implemented		1
							}
						field EP 1 1 {
							value EJTAGNotImplemented	0
							value EJTAGImplemented		1
							}
						field FP 0 1 {
							value FPUNotImplemented	0
							value FPUImplemented	1
							}
						}
					register Config2 uint32_t {
						field M 31 1 {
							value Config3Implemented	1
							value Config3NotImplemented	0
							}
						field TBS 0 30 { }
						}
					register Config3 uint32_t {
						field M 31 1 {
							value Config4Implemented	1
							value Config4NotImplemented	0
							}
						field SM 1 1 {
							value SmartMipsAseNotImplemented	0
							value SmartMipsAseImplemented		1
							}
						field TL 0 1 {
							value TraceLogicNotImplemented	0
							value TraceLogicImplemented		1
							}
						}
					register LLAddr uint32_t { }
					register WatchLo uint32_t {
						field VAddr 3 28 { }
						field I 2 1 {
							value InstructionWatchEnabled	1
							value InstructionWatchDisabled	0
							}
						field R 1 1 {
							value ReadWatchEnabled	1
							value ReadWatchDisabled	0
							}
						field W 0 1 {
							value WriteWatchEnabled		1
							value WriteWatchDisabled	0
							}
						}
					register WatchHi uint32_t {
						field M 31 1 {
							value AnotherWatchPairImplemented		1
							value AnotherWatchPairNotImplemented	0
							}
						field G 30 1 {
							value AsidMatchNotRequired	1
							value AsidMatchRequired		0
							}
						field ASID 16 8 { }
						field Mask 3 9 { }
						}
					register Debug uint32_t {}
					register DEPC uint32_t {}
					register PerfCntCtl uint32_t {
						field M 31 1 {
							value AnotherPairImplemented	1
							value AnotherPairNotImplemented	0
							}
						field Event 5 6 { }
						field IE 4 1 {
							value InterruptDisabled	0
							value InterruptEnabled	1
							}
						field U 3 1 {
							value DisableCountingInUserMode	0
							value EnableCountingInUserMode	1
							}
						field S 2 1 {
							value DisableCountingInSupervisorMode	0
							value EnableCountingInSupervisorMode	1
							}
						field K 1 1 {
							value DisableCountingInKernelMode	0
							value EnableCountingInKernelMode	1
							}
						field EXL 0 1 {
							value DisableCountingWhileEXL	0
							value EnableCountingWhileEXL	1
							}
						}
					register PerfCntCnt uint32_t {}
					register ErrCtl uint32_t {}
					register CacheErr uint32_t {}
					register TagLo uint32_t {}
					register DataLo uint32_t {}
					register TagHi uint32_t {}
					register DataHi uint32_t {}
					register ErrorEPC uint32_t {}
					register DESAVE uint32_t {}
					}
				}
			}
		}
	}
