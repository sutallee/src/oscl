/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_hw_siemens_hscxh_
#define _oscl_hw_siemens_hscxh_
#include "oscl/compiler/types.h"
#include <stdint.h>

/** This file represents the register structure of the
	SIEMENS SAB82525 and SAB82526 HDLC controllers.
 */

/*******************************************************************************
 * HSCX Register Interface
 */

/*******************************************************************************
 * Macros
 */

/*******************************************************************************
 * Type Definitions
 */

typedef READONLY	Oscl::Flags	ReadFlags;
typedef READONLY	uint8_t	ReadReg;
typedef WRITEONLY	Oscl::Flags	WriteFlags;
typedef WRITEONLY	uint8_t	WriteReg;
typedef Oscl::Reg8	Reg;

typedef struct HscxWriteChannel{
	WriteReg		XFIFO[32];
	WriteFlags		MASK;
	WriteFlags		CMDR;
	Oscl::PAD pad0[2];
	WriteReg		XAD1;
	WriteReg		XAD2;
	WriteReg		RAH1;
	WriteReg		RAH2;
	Oscl::PAD pad1;
	WriteReg		RAL2;
	WriteReg		XBCL;
	WriteReg		BGR;
	Oscl::PAD pad2;
	WriteReg		XBCH;
	WriteFlags		RLCR;
	Oscl::PAD pad3;
	WriteReg		TSAX;
	WriteReg		TSAR;
	WriteReg		XCCR;
	WriteReg		RCCR;
	Oscl::PAD pad4[12];					/* HSCX Register space is 64 bytes */
	}HscxWriteChannel;
/* typedef struct HscxWriteChannel HscxWriteChannel; */

typedef struct HscxReadChannel{
	ReadReg			RFIFO[32];
	ReadFlags		ISTA;
	ReadFlags		STAR;
	Oscl::PAD pad0[2];
	ReadFlags		EXIR;
	ReadReg			RBCL;
	Oscl::PAD pad1;
	ReadFlags		RSTA;
	Oscl::PAD pad2;
	ReadReg			RHCR;
	Oscl::PAD pad3[3];
	ReadReg			RBCH;
	ReadReg			VSTR;
	}HscxReadChannel;
/* typedef struct HscxReadChannel HscxReadChannel; */

typedef struct HscxReadWriteChannel{
	Oscl::PAD pad0[34];
	Reg				MODE;
	Reg				TIMR;
	Oscl::PAD pad1[4];
	Reg				RAL1;
	Oscl::PAD pad2[3];
	Reg				CCR2;
	Oscl::PAD pad3[2];
	Reg				CCR1;
	}HscxReadWriteChannel;
/* typedef struct HscxReadWriteChannel HscxReadWriteChannel; */

typedef union HscxChannel{
	HscxWriteChannel		write;
	HscxReadChannel			read;
	HscxReadWriteChannel	rw;
	}HscxChannel;
/* typedef union HscxChannel HscxChannel; */

typedef HscxChannel SAB82526;
typedef HscxChannel SAB82525[2];

/*******************************************************************************
 * ISTA	: Interrupt Status Register (Read)
 */

enum{
	ISTA_RME = 0x07,
	ISTA_RPF = 0x06,
	ISTA_RSC = 0x05,
	ISTA_XPR = 0x04,
	ISTA_TIN = 0x03,
	ISTA_ICA = 0x02,
	ISTA_EXA = 0x01,
	ISTA_EXB = 0x00
	};

/*******************************************************************************
 * MASK	: Interrupt Mask Register (Write)
 * = * All interrupts enabled (0x00) after RESET
 * = * Set bit to individually mask interrupt sources.
 */

enum{
	MASK_RME = 0x07,
	MASK_RPF = 0x06,
	MASK_RSC = 0x05,
	MASK_XPR = 0x04,
	MASK_TIN = 0x03,
	MASK_ICA = 0x02,
	MASK_EXA = 0x01,
	MASK_EXB = 0x00
	};

/*******************************************************************************
 * EXIR	: Extended Interrupt Register (Read)
 *	Note: Bits 0 & 1 read as zero.
 */

enum{
	EXIR_XMR = 0x07,
	EXIR_XDU = 0x06,
	EXIR_EXE = 0x06,
	EXIR_PCE = 0x05,
	EXIR_RFO = 0x04,
	EXIR_CSC = 0x03,
	EXIR_RFS = 0x02
	};

/*******************************************************************************
 * STAR	: Status Register (Read)
 *	Note: Bit 0 reads as zero.
 */

enum{
	STAR_XDOV = 0x07,
	STAR_XFW = 0x06,
	STAR_XRNR = 0x05,
	STAR_RRNR = 0x04,
	STAR_RLI = 0x03,
	STAR_CEC = 0x02,
	STAR_CTS = 0x01
	};


/*******************************************************************************
 * CMDR	: Command Register (Write)
 */

enum{
	CMDR_RMC = 0x07,
	CMDR_RHR = 0x06,
	CMDR_RNR = 0x05,
	CMDR_XREP = 0x05,
	CMDR_STI = 0x04,
	CMDR_XTF = 0x03,
	CMDR_XIF = 0x02,
	CMDR_XME = 0x01,
	CMDR_XRES = 0x00
	};

/*******************************************************************************
 * MODE	: Mode Register (Read/Write)
 */

enum{
	MODE_MDS_MASK = 0xC0,
	MODE_MDS_AUTO = 0x00,
	MODE_MDS_NON_AUTO = 0x40,
	MODE_MDS_TRANS_0 = 0x80,
	MODE_MDS_TRANS_1 = 0xA0,
	MODE_MDS_XTRANS = 0xC0,

	MODE_MDS1 = 0x07,
	MODE_MDS0 = 0x06,
	MODE_ADM = 0x05,
	MODE_TMD = 0x04,
	MODE_RAC = 0x03,
	MODE_RTS = 0x02,
	MODE_TRS = 0x01,
	MODE_TLP = 0x00
	};

/*******************************************************************************
 * TIMR	: Timer Register (Read/Write)
 */

enum{
	TIMR_CNT_MASK = 0xE0,
	TIMR_VALUE_MASK = 0x1F,

	TIMR_CNT0 = 0x05,
	TIMR_VALUE0 = 0x00
	};

/*******************************************************************************
 * RAH1	: Receive Address int8_t High Register 1 (Write)
 */

enum{
	RAH1_ADDR_MASK = 0xFC,

	RAH1_ADDR = 0x02,
	RAH1_CRI = 0x01
	};

/*******************************************************************************
 * RAH2	: Receive Address int8_t High Register 2 (Write)
 */

enum{
	RAH2_ADDR_MASK = 0xFC,

	RAH2_ADDR = 0x02,
	RAH2_MCS = 0x01
	};

/*******************************************************************************
 * RSTA	: Receive Status Register (Read)
 */

enum{
	RSTA_VFR = 0x07,
	RSTA_RDO = 0x06,
	RSTA_CRC = 0x05,
	RSTA_RAB = 0x04,
	RSTA_HA1 = 0x03,
	RSTA_HA0 = 0x02,
	RSTA_CR = 0x01,
	RSTA_LA = 0x00
	};

/*******************************************************************************
 * CCR2	: Channel Configuration Register 2 (Read/Write)
 */

enum{
	CCR2_BCS_MASK = 0xC0,	/* Modes 0,1,5 */
	CCR2_BCS1 = 0x07,	/* Modes 0,1,5 */
	CCR2_BCS0 = 0x06,	/* Modes 0,1,5 */
	CCR2_BDF = 0x05,	/* Modes 2,6 */
	CCR2_XCS0 = 0x05,	/* Mode 5 */
	CCR2_TSS = 0x04,	/* Modes 2,6 */
	CCR2_RCS0 = 0x04,	/* Mode 5 */
	CCR2_TIO = 0x03,	/* Modes 2,3,4,5,6 */
	CCR2_CIE = 0x02,	/* All Modes */
	CCR2_RIE = 0x01,	/* All Modes */
	CCR2_DIV = 0x00		/* All Modes */
	};

/*******************************************************************************
 * XBCH	: Transmit int8_t Count High (Write)
 */

enum{
	XBCH_XBC_MASK = 0x0F,

	XBCH_DMA = 0x07,
	XBCH_NRM = 0x06,
	XBCH_CAS = 0x05,
	XBCH_XC = 0x04,
	XBCH_XBC11 = 0x03,
	XBCH_XBC8 = 0x00
	};

/*******************************************************************************
 * RBCH	: Receive int8_t Count High (Read)
 */

enum{
	RBCH_RBC_MASK = 0x0F,

	RBCH_DMA = 0x07,
	RBCH_NRM = 0x06,
	RBCH_CAS = 0x05,
	RBCH_OV = 0x04,
	RBCH_RBC11 = 0x03,
	RBCH_RBC8 = 0x00
	};

/*******************************************************************************
 * VSTR	: Version Status Register (Read)
 */

enum{
	VSTR_VN_MASK = 0x0F,

	VSTR_CD = 0x07,
	VSTR_VN3 = 0x03,
	VSTR_VN2 = 0x02,
	VSTR_VN1 = 0x01,
	VSTR_VN0 = 0x00
	};

/*******************************************************************************
 * RLCR	: Receive Length Check Register (Write)
 */

enum{
	RLCR_RLC_MASK = 0x7F,

	RLCR_RC = 0x07,
	RLCR_RLC6 = 0x06,
	RLCR_RLC0 = 0x00
	};

/*******************************************************************************
 * CCR1	: Channel Configuration Register 1 (Read/Write)
 */

enum{
	CCR1_CM_MASK = 0x07,
	CCR1_CM_7 = 0x07,
	CCR1_CM_6 = 0x06,
	CCR1_CM_5 = 0x05,
	CCR1_CM_4 = 0x04,
	CCR1_CM_3 = 0x03,
	CCR1_CM_2 = 0x02,
	CCR1_CM_1 = 0x01,
	CCR1_CM_0 = 0x00
	};


enum{
	CCR1_PU			= 0x07,
	CCR1_SC1		= 0x06,
	CCR1_SC2		= 0x05,
	CCR1_NRZ		= 0x00<<5,
	CCR1_NRZI		= 0x02<<5,
	CCR1_BUS_TM1	= 0x01<<5,
	CCR1_BUS_TM2	= 0x03<<5,
	CCR1_ODS		= 0x04,
	CCR1_ITF		= 0x03,
	CCR1_OIN		= 0x03,
	CCR1_CM2		= 0x02,
	CCR1_CM1		= 0x01,
	CCR1_CM0		= 0x00
	};

/*******************************************************************************
 * TSAX	: Time Slot Assignment Register Transmit (Write)
 */

enum{
	TSAX_TSNX_MASK = 0xFC,

	TSAX_TSNX = 0x02,
	TSAX_XCS2 = 0x01,
	TSAX_XCS1 = 0x00
	};

/*******************************************************************************
 * TSAR	: Time Slot Assignment Register Receive (Write)
 */

enum{
	TSAR_TSNR_MASK = 0xFC,

	TSAR_TSNR = 0x02,
	TSAR_RCS2 = 0x01,
	TSAR_RCS1 = 0x00
	};

#endif
