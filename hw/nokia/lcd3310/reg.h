#ifndef _hw_nokia_lcd3310_regh_
#define _hw_nokia_lcd3310_regh_
#include <stdint.h>
namespace Oscl { // Namespace description
	namespace HW { // Namespace description
		namespace Nokia { // Namespace description
			namespace LCD3310 { // Namespace description
				namespace NOP { // Register description
					typedef uint8_t	Reg;
					enum {OpCode = 0};
					};
				namespace FunctionSet { // Register description
					typedef uint8_t	Reg;
					namespace OpCode { // Field Description
						enum {Lsb = 3};
						enum {FieldMask = 0x000000F8};
						enum {Value_Always = 0x4};
						enum {ValueMask_Always = 0x00000020};
						};
					namespace H { // Field Description
						enum {Lsb = 0};
						enum {FieldMask = 0x00000001};
						enum {Value_Basic = 0x0};
						enum {ValueMask_Basic = 0x00000000};
						enum {Value_Extended = 0x1};
						enum {ValueMask_Extended = 0x00000001};
						};
					namespace V { // Field Description
						enum {Lsb = 1};
						enum {FieldMask = 0x00000002};
						enum {Value_HorizontalAddr = 0x0};
						enum {ValueMask_HorizontalAddr = 0x00000000};
						enum {Value_VerticalAddr = 0x1};
						enum {ValueMask_VerticalAddr = 0x00000002};
						};
					namespace PD { // Field Description
						enum {Lsb = 2};
						enum {FieldMask = 0x00000004};
						enum {Value_Active = 0x0};
						enum {ValueMask_Active = 0x00000000};
						enum {Value_PowerDown = 0x1};
						enum {ValueMask_PowerDown = 0x00000004};
						};
					};
				namespace Display { // Register description
					typedef uint8_t	Reg;
					enum {Blank = 8};
					enum {NormalMode = 12};
					enum {AllSegmentsOn = 9};
					enum {InverseVideo = 13};
					};
				namespace SetY { // Register description
					typedef uint8_t	Reg;
					namespace OpCode { // Field Description
						enum {Lsb = 3};
						enum {FieldMask = 0x000000F8};
						enum {Value_Always = 0x8};
						enum {ValueMask_Always = 0x00000040};
						};
					namespace Address { // Field Description
						enum {Lsb = 0};
						enum {FieldMask = 0x00000007};
						enum {Value_Min = 0x0};
						enum {ValueMask_Min = 0x00000000};
						enum {Value_Max = 0x5};
						enum {ValueMask_Max = 0x00000005};
						};
					};
				namespace SetX { // Register description
					typedef uint8_t	Reg;
					namespace OpCode { // Field Description
						enum {Lsb = 7};
						enum {FieldMask = 0x00000080};
						enum {Value_Always = 0x1};
						enum {ValueMask_Always = 0x00000080};
						};
					namespace Address { // Field Description
						enum {Lsb = 0};
						enum {FieldMask = 0x0000007F};
						enum {Value_Min = 0x0};
						enum {ValueMask_Min = 0x00000000};
						enum {Value_Max = 0x53};
						enum {ValueMask_Max = 0x00000053};
						};
					};
				namespace Temperature { // Register description
					typedef uint8_t	Reg;
					namespace OpCode { // Field Description
						enum {Lsb = 2};
						enum {FieldMask = 0x000000FC};
						enum {Value_Always = 0x1};
						enum {ValueMask_Always = 0x00000004};
						};
					namespace TC { // Field Description
						enum {Lsb = 0};
						enum {FieldMask = 0x00000003};
						};
					};
				namespace BiasSystem { // Register description
					typedef uint8_t	Reg;
					namespace OpCode { // Field Description
						enum {Lsb = 3};
						enum {FieldMask = 0x000000F8};
						enum {Value_Always = 0x2};
						enum {ValueMask_Always = 0x00000010};
						};
					namespace BS { // Field Description
						enum {Lsb = 0};
						enum {FieldMask = 0x00000007};
						};
					};
				namespace SetVop { // Register description
					typedef uint8_t	Reg;
					namespace OpCode { // Field Description
						enum {Lsb = 7};
						enum {FieldMask = 0x00000080};
						enum {Value_Always = 0x1};
						enum {ValueMask_Always = 0x00000080};
						};
					namespace Vop { // Field Description
						enum {Lsb = 0};
						enum {FieldMask = 0x0000007F};
						};
					};
				};
			};
		};
	};
#endif
