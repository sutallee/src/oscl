/*
   Copyright (C) 2007 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

/* AKA Philips Semiconductor PCD8544 */
module hw_nokia_lcd3310_regh {
	include "oscl/compiler/types.h";
	namespace Oscl {
		namespace HW {
			namespace Nokia {
				namespace LCD3310 {
					register NOP uint8_t {
						value OpCode 0x00
						}
					register FunctionSet uint8_t {
						field OpCode 3 5 {
							value Always	0x4
							}
						field H 0 1 {
							value Basic		0
							value Extended	1
							}
						field V 1 1 {
							value HorizontalAddr	0
							value VerticalAddr		1
							}
						field PD 2 1 {
							value Active		0
							value PowerDown		1
							}
						}
					register Display uint8_t {
						value Blank				0x8
						value NormalMode		0xC
						value AllSegmentsOn		0x9
						value InverseVideo		0xD
						}
					register SetY uint8_t {
						field OpCode 3 5 {
							value Always	0x8
							}
						field Address 0 3 {
							value Min 0
							value Max 5
							}
						}
					register SetX uint8_t {
						field OpCode 7 1 {
							value Always	0x1
							}
						field Address 0 7 {
							value Min 0
							value Max 83
							}
						}
					register Temperature uint8_t {
						field OpCode 2 6 {
							value Always	0x1
							}
						field TC 0 2 { }
						}
					register BiasSystem uint8_t {
						field OpCode 3 5 {
							value Always	0x2
							}
						field BS 0 3 { }
						}
					register SetVop uint8_t {
						field OpCode 7 1 {
							value Always	0x1
							}
						field Vop 0 7 { }
						}
					}
				}
			}
		}
	}

