#ifndef _oscl_hw_ti_msp430_x16x_moduleh_
#define _oscl_hw_ti_msp430_x16x_moduleh_
#include <stdint.h>
namespace Oscl { // Namespace description
	namespace HW { // Namespace description
		namespace TI { // Namespace description
			namespace Msp430 { // Namespace description
				namespace x16x { // Namespace description
					namespace Module { // Namespace description
						namespace ME1 { // Register description
							typedef uint8_t	Reg;
							namespace USPIE0 { // Field Description
								enum {Lsb = 6};
								enum {FieldMask = 0x00000040};
								enum {Value_ValueAtReset = 0x0};
								enum {ValueMask_ValueAtReset = 0x00000000};
								enum {Value_ModuleDisabled = 0x0};
								enum {ValueMask_ModuleDisabled = 0x00000000};
								enum {Value_ModuleDisable = 0x0};
								enum {ValueMask_ModuleDisable = 0x00000000};
								enum {Value_ModuleEnabled = 0x1};
								enum {ValueMask_ModuleEnabled = 0x00000040};
								enum {Value_ModuleEnable = 0x1};
								enum {ValueMask_ModuleEnable = 0x00000040};
								};
							};
						namespace ME2 { // Register description
							typedef uint8_t	Reg;
							namespace USPIE1 { // Field Description
								enum {Lsb = 4};
								enum {FieldMask = 0x00000010};
								enum {Value_ValueAtReset = 0x0};
								enum {ValueMask_ValueAtReset = 0x00000000};
								enum {Value_ModuleDisabled = 0x0};
								enum {ValueMask_ModuleDisabled = 0x00000000};
								enum {Value_ModuleDisable = 0x0};
								enum {ValueMask_ModuleDisable = 0x00000000};
								enum {Value_ModuleEnabled = 0x1};
								enum {ValueMask_ModuleEnabled = 0x00000010};
								enum {Value_ModuleEnable = 0x1};
								enum {ValueMask_ModuleEnable = 0x00000010};
								};
							};
						};
					};
				};
			};
		};
	};
#endif
