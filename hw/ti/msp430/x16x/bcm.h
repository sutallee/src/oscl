#ifndef _oscl_hw_ti_msp430_x16x_bcmh_
#define _oscl_hw_ti_msp430_x16x_bcmh_
#include <stdint.h>
namespace Oscl { // Namespace description
	namespace HW { // Namespace description
		namespace TI { // Namespace description
			namespace Msp430 { // Namespace description
				namespace x16x { // Namespace description
					namespace BCM { // Namespace description
						namespace DCOCTL { // Register description
							typedef uint8_t	Reg;
							namespace DCOx { // Field Description
								enum {Lsb = 5};
								enum {FieldMask = 0x000000E0};
								enum {Value_HighestFreq = 0x7};
								enum {ValueMask_HighestFreq = 0x000000E0};
								enum {Value_LowestFreq = 0x0};
								enum {ValueMask_LowestFreq = 0x00000000};
								enum {Value_Max = 0x7};
								enum {ValueMask_Max = 0x000000E0};
								enum {Value_Min = 0x0};
								enum {ValueMask_Min = 0x00000000};
								enum {Value_ModxNotUsableAtThisValue = 0x7};
								enum {ValueMask_ModxNotUsableAtThisValue = 0x000000E0};
								enum {Value_ValueAtReset = 0x3};
								enum {ValueMask_ValueAtReset = 0x00000060};
								};
							namespace MODx { // Field Description
								enum {Lsb = 0};
								enum {FieldMask = 0x0000001F};
								enum {Value_Max = 0x1F};
								enum {ValueMask_Max = 0x0000001F};
								enum {Value_Min = 0x0};
								enum {ValueMask_Min = 0x00000000};
								enum {Value_ValueAtReset = 0x0};
								enum {ValueMask_ValueAtReset = 0x00000000};
								};
							};
						namespace BCSCTL1 { // Register description
							typedef uint8_t	Reg;
							namespace XT2OFF { // Field Description
								enum {Lsb = 7};
								enum {FieldMask = 0x00000080};
								enum {Value_ON = 0x0};
								enum {ValueMask_ON = 0x00000000};
								enum {Value_OFF = 0x1};
								enum {ValueMask_OFF = 0x00000080};
								enum {Value_ValueAtReset = 0x1};
								enum {ValueMask_ValueAtReset = 0x00000080};
								};
							namespace XTS { // Field Description
								enum {Lsb = 6};
								enum {FieldMask = 0x00000040};
								enum {Value_LowFreqMode = 0x0};
								enum {ValueMask_LowFreqMode = 0x00000000};
								enum {Value_HighFreqMode = 0x1};
								enum {ValueMask_HighFreqMode = 0x00000040};
								enum {Value_ValueAtReset = 0x0};
								enum {ValueMask_ValueAtReset = 0x00000000};
								};
							namespace DIVAx { // Field Description
								enum {Lsb = 4};
								enum {FieldMask = 0x00000030};
								enum {Value_DivideBy1 = 0x0};
								enum {ValueMask_DivideBy1 = 0x00000000};
								enum {Value_DivideBy2 = 0x1};
								enum {ValueMask_DivideBy2 = 0x00000010};
								enum {Value_DivideBy4 = 0x2};
								enum {ValueMask_DivideBy4 = 0x00000020};
								enum {Value_DivideBy8 = 0x3};
								enum {ValueMask_DivideBy8 = 0x00000030};
								enum {Value_ValueAtReset = 0x0};
								enum {ValueMask_ValueAtReset = 0x00000000};
								};
							namespace XT5V { // Field Description
								enum {Lsb = 3};
								enum {FieldMask = 0x00000008};
								enum {Value_ReqiredDefault = 0x0};
								enum {ValueMask_ReqiredDefault = 0x00000000};
								enum {Value_ValueAtReset = 0x0};
								enum {ValueMask_ValueAtReset = 0x00000000};
								};
							namespace RSELx { // Field Description
								enum {Lsb = 0};
								enum {FieldMask = 0x00000007};
								enum {Value_LowestFreq = 0x0};
								enum {ValueMask_LowestFreq = 0x00000000};
								enum {Value_HighestFreq = 0x7};
								enum {ValueMask_HighestFreq = 0x00000007};
								enum {Value_ValueAtReset = 0x4};
								enum {ValueMask_ValueAtReset = 0x00000004};
								};
							};
						namespace BCSCTL2 { // Register description
							typedef uint8_t	Reg;
							namespace SELMx { // Field Description
								enum {Lsb = 6};
								enum {FieldMask = 0x000000C0};
								enum {Value_ValueAtReset = 0x0};
								enum {ValueMask_ValueAtReset = 0x00000000};
								enum {Value_DCOCLK = 0x0};
								enum {ValueMask_DCOCLK = 0x00000000};
								enum {Value_DCOCLKx = 0x1};
								enum {ValueMask_DCOCLKx = 0x00000040};
								enum {Value_XT2CLKorLFXT1CLK = 0x2};
								enum {ValueMask_XT2CLKorLFXT1CLK = 0x00000080};
								enum {Value_LFXT1CLK = 0x3};
								enum {ValueMask_LFXT1CLK = 0x000000C0};
								};
							namespace DIVMx { // Field Description
								enum {Lsb = 4};
								enum {FieldMask = 0x00000030};
								enum {Value_ValueAtReset = 0x0};
								enum {ValueMask_ValueAtReset = 0x00000000};
								enum {Value_DivideBy1 = 0x0};
								enum {ValueMask_DivideBy1 = 0x00000000};
								enum {Value_DivideBy2 = 0x1};
								enum {ValueMask_DivideBy2 = 0x00000010};
								enum {Value_DivideBy4 = 0x2};
								enum {ValueMask_DivideBy4 = 0x00000020};
								enum {Value_DivideBy8 = 0x3};
								enum {ValueMask_DivideBy8 = 0x00000030};
								};
							namespace SELS { // Field Description
								enum {Lsb = 3};
								enum {FieldMask = 0x00000008};
								enum {Value_ValueAtReset = 0x0};
								enum {ValueMask_ValueAtReset = 0x00000000};
								enum {Value_DCOCLK = 0x0};
								enum {ValueMask_DCOCLK = 0x00000000};
								enum {Value_XT2CLKorLFXT1CLK = 0x1};
								enum {ValueMask_XT2CLKorLFXT1CLK = 0x00000008};
								};
							namespace DIVSx { // Field Description
								enum {Lsb = 1};
								enum {FieldMask = 0x00000006};
								enum {Value_ValueAtReset = 0x0};
								enum {ValueMask_ValueAtReset = 0x00000000};
								enum {Value_DivideBy1 = 0x0};
								enum {ValueMask_DivideBy1 = 0x00000000};
								enum {Value_DivideBy2 = 0x1};
								enum {ValueMask_DivideBy2 = 0x00000002};
								enum {Value_DivideBy4 = 0x2};
								enum {ValueMask_DivideBy4 = 0x00000004};
								enum {Value_DivideBy8 = 0x3};
								enum {ValueMask_DivideBy8 = 0x00000006};
								};
							namespace DCOR { // Field Description
								enum {Lsb = 0};
								enum {FieldMask = 0x00000001};
								enum {Value_ValueAtReset = 0x0};
								enum {ValueMask_ValueAtReset = 0x00000000};
								enum {Value_InternalResistor = 0x0};
								enum {ValueMask_InternalResistor = 0x00000000};
								enum {Value_ExternalResistor = 0x1};
								enum {ValueMask_ExternalResistor = 0x00000001};
								};
							};
						};
					};
				};
			};
		};
	};
#endif
