#ifndef _oscl_hw_ti_msp430_x16x_usarth_
#define _oscl_hw_ti_msp430_x16x_usarth_
#include <stdint.h>
namespace Oscl { // Namespace description
	namespace HW { // Namespace description
		namespace TI { // Namespace description
			namespace Msp430 { // Namespace description
				namespace x16x { // Namespace description
					namespace USART { // Namespace description
						namespace UxCTL { // Register description
							typedef uint8_t	Reg;
							namespace I2C { // Field Description
								enum {Lsb = 5};
								enum {FieldMask = 0x00000020};
								enum {Value_ValueAtReset = 0x0};
								enum {ValueMask_ValueAtReset = 0x00000000};
								enum {Value_SPI = 0x0};
								enum {ValueMask_SPI = 0x00000000};
								enum {Value_I2C = 0x1};
								enum {ValueMask_I2C = 0x00000020};
								};
							namespace CHAR { // Field Description
								enum {Lsb = 4};
								enum {FieldMask = 0x00000010};
								enum {Value_ValueAtReset = 0x0};
								enum {ValueMask_ValueAtReset = 0x00000000};
								enum {Value_SevenBit = 0x0};
								enum {ValueMask_SevenBit = 0x00000000};
								enum {Value_EightBit = 0x1};
								enum {ValueMask_EightBit = 0x00000010};
								};
							namespace LISTEN { // Field Description
								enum {Lsb = 3};
								enum {FieldMask = 0x00000008};
								enum {Value_ValueAtReset = 0x0};
								enum {ValueMask_ValueAtReset = 0x00000000};
								enum {Value_Disabled = 0x0};
								enum {ValueMask_Disabled = 0x00000000};
								enum {Value_Disable = 0x0};
								enum {ValueMask_Disable = 0x00000000};
								enum {Value_Enabled = 0x1};
								enum {ValueMask_Enabled = 0x00000008};
								enum {Value_Enable = 0x1};
								enum {ValueMask_Enable = 0x00000008};
								};
							namespace SYNC { // Field Description
								enum {Lsb = 2};
								enum {FieldMask = 0x00000004};
								enum {Value_ValueAtReset = 0x0};
								enum {ValueMask_ValueAtReset = 0x00000000};
								enum {Value_UART = 0x0};
								enum {ValueMask_UART = 0x00000000};
								enum {Value_SPI = 0x1};
								enum {ValueMask_SPI = 0x00000004};
								};
							namespace MM { // Field Description
								enum {Lsb = 1};
								enum {FieldMask = 0x00000002};
								enum {Value_ValueAtReset = 0x0};
								enum {ValueMask_ValueAtReset = 0x00000000};
								enum {Value_Slave = 0x0};
								enum {ValueMask_Slave = 0x00000000};
								enum {Value_Master = 0x1};
								enum {ValueMask_Master = 0x00000002};
								};
							namespace SWRST { // Field Description
								enum {Lsb = 0};
								enum {FieldMask = 0x00000001};
								enum {Value_ValueAtReset = 0x1};
								enum {ValueMask_ValueAtReset = 0x00000001};
								enum {Value_Disabled = 0x0};
								enum {ValueMask_Disabled = 0x00000000};
								enum {Value_Disable = 0x0};
								enum {ValueMask_Disable = 0x00000000};
								enum {Value_Enabled = 0x1};
								enum {ValueMask_Enabled = 0x00000001};
								enum {Value_Enable = 0x1};
								enum {ValueMask_Enable = 0x00000001};
								enum {Value_NormalOperation = 0x0};
								enum {ValueMask_NormalOperation = 0x00000000};
								enum {Value_Reset = 0x1};
								enum {ValueMask_Reset = 0x00000001};
								};
							};
						namespace UxTCTL { // Register description
							typedef uint8_t	Reg;
							namespace CKPH { // Field Description
								enum {Lsb = 7};
								enum {FieldMask = 0x00000080};
								enum {Value_ValueAtReset = 0x0};
								enum {ValueMask_ValueAtReset = 0x00000000};
								enum {Value_NormalUCLK = 0x0};
								enum {ValueMask_NormalUCLK = 0x00000000};
								enum {Value_DelayUCLK = 0x1};
								enum {ValueMask_DelayUCLK = 0x00000080};
								};
							namespace CKPL { // Field Description
								enum {Lsb = 6};
								enum {FieldMask = 0x00000040};
								enum {Value_ValueAtReset = 0x0};
								enum {ValueMask_ValueAtReset = 0x00000000};
								enum {Value_OutRisingInFalling = 0x0};
								enum {ValueMask_OutRisingInFalling = 0x00000000};
								enum {Value_OutFallingInRising = 0x1};
								enum {ValueMask_OutFallingInRising = 0x00000040};
								};
							namespace SSELx { // Field Description
								enum {Lsb = 4};
								enum {FieldMask = 0x00000030};
								enum {Value_ValueAtReset = 0x0};
								enum {ValueMask_ValueAtReset = 0x00000000};
								enum {Value_ExternalUCLK = 0x0};
								enum {ValueMask_ExternalUCLK = 0x00000000};
								enum {Value_ACLK = 0x1};
								enum {ValueMask_ACLK = 0x00000010};
								enum {Value_SMCLK = 0x2};
								enum {ValueMask_SMCLK = 0x00000020};
								enum {Value_SMCLKalso = 0x3};
								enum {ValueMask_SMCLKalso = 0x00000030};
								};
							namespace STC { // Field Description
								enum {Lsb = 1};
								enum {FieldMask = 0x00000002};
								enum {Value_ValueAtReset = 0x0};
								enum {ValueMask_ValueAtReset = 0x00000000};
								enum {Value_SteEnabled = 0x0};
								enum {ValueMask_SteEnabled = 0x00000000};
								enum {Value_SteEnable = 0x0};
								enum {ValueMask_SteEnable = 0x00000000};
								enum {Value_SteDisabled = 0x1};
								enum {ValueMask_SteDisabled = 0x00000002};
								enum {Value_SteDisable = 0x1};
								enum {ValueMask_SteDisable = 0x00000002};
								};
							namespace TXEPT { // Field Description
								enum {Lsb = 0};
								enum {FieldMask = 0x00000001};
								enum {Value_ValueAtReset = 0x1};
								enum {ValueMask_ValueAtReset = 0x00000001};
								enum {Value_TxNotEmpty = 0x0};
								enum {ValueMask_TxNotEmpty = 0x00000000};
								enum {Value_UxTxbuffAndTxSrEmpty = 0x1};
								enum {ValueMask_UxTxbuffAndTxSrEmpty = 0x00000001};
								};
							};
						namespace UxRCTL { // Register description
							typedef uint8_t	Reg;
							namespace FE { // Field Description
								enum {Lsb = 7};
								enum {FieldMask = 0x00000080};
								enum {Value_ValueAtReset = 0x0};
								enum {ValueMask_ValueAtReset = 0x00000000};
								enum {Value_NoConflict = 0x0};
								enum {ValueMask_NoConflict = 0x00000000};
								enum {Value_BusConflict = 0x1};
								enum {ValueMask_BusConflict = 0x00000080};
								};
							namespace OE { // Field Description
								enum {Lsb = 5};
								enum {FieldMask = 0x00000020};
								enum {Value_ValueAtReset = 0x0};
								enum {ValueMask_ValueAtReset = 0x00000000};
								enum {Value_NoError = 0x0};
								enum {ValueMask_NoError = 0x00000000};
								enum {Value_OverrunError = 0x0};
								enum {ValueMask_OverrunError = 0x00000000};
								};
							};
						namespace UxMCTL { // Register description
							typedef uint8_t	Reg;
							enum {SpiModeValue = 0};
							};
						namespace UxBR0 { // Register description
							typedef uint8_t	Reg;
							enum {Min = 2};
							enum {Max = 255};
							};
						namespace UxBR1 { // Register description
							typedef uint8_t	Reg;
							enum {Min = 0};
							enum {Max = 255};
							};
						namespace UxRXBUF { // Register description
							typedef uint8_t	Reg;
							};
						namespace UxTXBUF { // Register description
							typedef uint8_t	Reg;
							};
						};
					};
				};
			};
		};
	};
#endif
