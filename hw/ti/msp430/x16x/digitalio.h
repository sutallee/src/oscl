#ifndef _oscl_hw_ti_msp430_x16x_digitialioh__
#define _oscl_hw_ti_msp430_x16x_digitialioh__
#include <stdint.h>
namespace Oscl { // Namespace description
	namespace HW { // Namespace description
		namespace TI { // Namespace description
			namespace Msp430 { // Namespace description
				namespace x16x { // Namespace description
					namespace DigitalIO { // Namespace description
						namespace IN { // Register description
							typedef uint8_t	Reg;
							namespace Bit7 { // Field Description
								enum {Lsb = 7};
								enum {FieldMask = 0x00000080};
								enum {Value_ValueAtReset = 0x0};
								enum {ValueMask_ValueAtReset = 0x00000000};
								enum {Value_Low = 0x0};
								enum {ValueMask_Low = 0x00000000};
								enum {Value_High = 0x1};
								enum {ValueMask_High = 0x00000080};
								enum {Value_Reset = 0x0};
								enum {ValueMask_Reset = 0x00000000};
								enum {Value_Set = 0x1};
								enum {ValueMask_Set = 0x00000080};
								};
							namespace Bit6 { // Field Description
								enum {Lsb = 6};
								enum {FieldMask = 0x00000040};
								enum {Value_ValueAtReset = 0x0};
								enum {ValueMask_ValueAtReset = 0x00000000};
								enum {Value_Low = 0x0};
								enum {ValueMask_Low = 0x00000000};
								enum {Value_High = 0x1};
								enum {ValueMask_High = 0x00000040};
								enum {Value_Reset = 0x0};
								enum {ValueMask_Reset = 0x00000000};
								enum {Value_Set = 0x1};
								enum {ValueMask_Set = 0x00000040};
								};
							namespace Bit5 { // Field Description
								enum {Lsb = 5};
								enum {FieldMask = 0x00000020};
								enum {Value_ValueAtReset = 0x0};
								enum {ValueMask_ValueAtReset = 0x00000000};
								enum {Value_Low = 0x0};
								enum {ValueMask_Low = 0x00000000};
								enum {Value_High = 0x1};
								enum {ValueMask_High = 0x00000020};
								enum {Value_Reset = 0x0};
								enum {ValueMask_Reset = 0x00000000};
								enum {Value_Set = 0x1};
								enum {ValueMask_Set = 0x00000020};
								};
							namespace Bit4 { // Field Description
								enum {Lsb = 4};
								enum {FieldMask = 0x00000010};
								enum {Value_ValueAtReset = 0x0};
								enum {ValueMask_ValueAtReset = 0x00000000};
								enum {Value_Low = 0x0};
								enum {ValueMask_Low = 0x00000000};
								enum {Value_High = 0x1};
								enum {ValueMask_High = 0x00000010};
								enum {Value_Reset = 0x0};
								enum {ValueMask_Reset = 0x00000000};
								enum {Value_Set = 0x1};
								enum {ValueMask_Set = 0x00000010};
								};
							namespace Bit3 { // Field Description
								enum {Lsb = 3};
								enum {FieldMask = 0x00000008};
								enum {Value_ValueAtReset = 0x0};
								enum {ValueMask_ValueAtReset = 0x00000000};
								enum {Value_Low = 0x0};
								enum {ValueMask_Low = 0x00000000};
								enum {Value_High = 0x1};
								enum {ValueMask_High = 0x00000008};
								enum {Value_Reset = 0x0};
								enum {ValueMask_Reset = 0x00000000};
								enum {Value_Set = 0x1};
								enum {ValueMask_Set = 0x00000008};
								};
							namespace Bit2 { // Field Description
								enum {Lsb = 2};
								enum {FieldMask = 0x00000004};
								enum {Value_ValueAtReset = 0x0};
								enum {ValueMask_ValueAtReset = 0x00000000};
								enum {Value_Low = 0x0};
								enum {ValueMask_Low = 0x00000000};
								enum {Value_High = 0x1};
								enum {ValueMask_High = 0x00000004};
								enum {Value_Reset = 0x0};
								enum {ValueMask_Reset = 0x00000000};
								enum {Value_Set = 0x1};
								enum {ValueMask_Set = 0x00000004};
								};
							namespace Bit1 { // Field Description
								enum {Lsb = 1};
								enum {FieldMask = 0x00000002};
								enum {Value_ValueAtReset = 0x0};
								enum {ValueMask_ValueAtReset = 0x00000000};
								enum {Value_Low = 0x0};
								enum {ValueMask_Low = 0x00000000};
								enum {Value_High = 0x1};
								enum {ValueMask_High = 0x00000002};
								enum {Value_Reset = 0x0};
								enum {ValueMask_Reset = 0x00000000};
								enum {Value_Set = 0x1};
								enum {ValueMask_Set = 0x00000002};
								};
							namespace Bit0 { // Field Description
								enum {Lsb = 0};
								enum {FieldMask = 0x00000001};
								enum {Value_ValueAtReset = 0x0};
								enum {ValueMask_ValueAtReset = 0x00000000};
								enum {Value_Low = 0x0};
								enum {ValueMask_Low = 0x00000000};
								enum {Value_High = 0x1};
								enum {ValueMask_High = 0x00000001};
								enum {Value_Reset = 0x0};
								enum {ValueMask_Reset = 0x00000000};
								enum {Value_Set = 0x1};
								enum {ValueMask_Set = 0x00000001};
								};
							};
						namespace OUT { // Register description
							typedef uint8_t	Reg;
							namespace Bit7 { // Field Description
								enum {Lsb = 7};
								enum {FieldMask = 0x00000080};
								enum {Value_ValueAtReset = 0x0};
								enum {ValueMask_ValueAtReset = 0x00000000};
								enum {Value_Low = 0x0};
								enum {ValueMask_Low = 0x00000000};
								enum {Value_High = 0x1};
								enum {ValueMask_High = 0x00000080};
								enum {Value_Reset = 0x0};
								enum {ValueMask_Reset = 0x00000000};
								enum {Value_Set = 0x1};
								enum {ValueMask_Set = 0x00000080};
								};
							namespace Bit6 { // Field Description
								enum {Lsb = 6};
								enum {FieldMask = 0x00000040};
								enum {Value_ValueAtReset = 0x0};
								enum {ValueMask_ValueAtReset = 0x00000000};
								enum {Value_Low = 0x0};
								enum {ValueMask_Low = 0x00000000};
								enum {Value_High = 0x1};
								enum {ValueMask_High = 0x00000040};
								enum {Value_Reset = 0x0};
								enum {ValueMask_Reset = 0x00000000};
								enum {Value_Set = 0x1};
								enum {ValueMask_Set = 0x00000040};
								};
							namespace Bit5 { // Field Description
								enum {Lsb = 5};
								enum {FieldMask = 0x00000020};
								enum {Value_ValueAtReset = 0x0};
								enum {ValueMask_ValueAtReset = 0x00000000};
								enum {Value_Low = 0x0};
								enum {ValueMask_Low = 0x00000000};
								enum {Value_High = 0x1};
								enum {ValueMask_High = 0x00000020};
								enum {Value_Reset = 0x0};
								enum {ValueMask_Reset = 0x00000000};
								enum {Value_Set = 0x1};
								enum {ValueMask_Set = 0x00000020};
								};
							namespace Bit4 { // Field Description
								enum {Lsb = 4};
								enum {FieldMask = 0x00000010};
								enum {Value_ValueAtReset = 0x0};
								enum {ValueMask_ValueAtReset = 0x00000000};
								enum {Value_Low = 0x0};
								enum {ValueMask_Low = 0x00000000};
								enum {Value_High = 0x1};
								enum {ValueMask_High = 0x00000010};
								enum {Value_Reset = 0x0};
								enum {ValueMask_Reset = 0x00000000};
								enum {Value_Set = 0x1};
								enum {ValueMask_Set = 0x00000010};
								};
							namespace Bit3 { // Field Description
								enum {Lsb = 3};
								enum {FieldMask = 0x00000008};
								enum {Value_ValueAtReset = 0x0};
								enum {ValueMask_ValueAtReset = 0x00000000};
								enum {Value_Low = 0x0};
								enum {ValueMask_Low = 0x00000000};
								enum {Value_High = 0x1};
								enum {ValueMask_High = 0x00000008};
								enum {Value_Reset = 0x0};
								enum {ValueMask_Reset = 0x00000000};
								enum {Value_Set = 0x1};
								enum {ValueMask_Set = 0x00000008};
								};
							namespace Bit2 { // Field Description
								enum {Lsb = 2};
								enum {FieldMask = 0x00000004};
								enum {Value_ValueAtReset = 0x0};
								enum {ValueMask_ValueAtReset = 0x00000000};
								enum {Value_Low = 0x0};
								enum {ValueMask_Low = 0x00000000};
								enum {Value_High = 0x1};
								enum {ValueMask_High = 0x00000004};
								enum {Value_Reset = 0x0};
								enum {ValueMask_Reset = 0x00000000};
								enum {Value_Set = 0x1};
								enum {ValueMask_Set = 0x00000004};
								};
							namespace Bit1 { // Field Description
								enum {Lsb = 1};
								enum {FieldMask = 0x00000002};
								enum {Value_ValueAtReset = 0x0};
								enum {ValueMask_ValueAtReset = 0x00000000};
								enum {Value_Low = 0x0};
								enum {ValueMask_Low = 0x00000000};
								enum {Value_High = 0x1};
								enum {ValueMask_High = 0x00000002};
								enum {Value_Reset = 0x0};
								enum {ValueMask_Reset = 0x00000000};
								enum {Value_Set = 0x1};
								enum {ValueMask_Set = 0x00000002};
								};
							namespace Bit0 { // Field Description
								enum {Lsb = 0};
								enum {FieldMask = 0x00000001};
								enum {Value_ValueAtReset = 0x0};
								enum {ValueMask_ValueAtReset = 0x00000000};
								enum {Value_Low = 0x0};
								enum {ValueMask_Low = 0x00000000};
								enum {Value_High = 0x1};
								enum {ValueMask_High = 0x00000001};
								enum {Value_Reset = 0x0};
								enum {ValueMask_Reset = 0x00000000};
								enum {Value_Set = 0x1};
								enum {ValueMask_Set = 0x00000001};
								};
							};
						namespace DIR { // Register description
							typedef uint8_t	Reg;
							namespace Bit7 { // Field Description
								enum {Lsb = 7};
								enum {FieldMask = 0x00000080};
								enum {Value_ValueAtReset = 0x0};
								enum {ValueMask_ValueAtReset = 0x00000000};
								enum {Value_Input = 0x0};
								enum {ValueMask_Input = 0x00000000};
								enum {Value_Output = 0x1};
								enum {ValueMask_Output = 0x00000080};
								};
							namespace Bit6 { // Field Description
								enum {Lsb = 6};
								enum {FieldMask = 0x00000040};
								enum {Value_ValueAtReset = 0x0};
								enum {ValueMask_ValueAtReset = 0x00000000};
								enum {Value_Input = 0x0};
								enum {ValueMask_Input = 0x00000000};
								enum {Value_Output = 0x1};
								enum {ValueMask_Output = 0x00000040};
								};
							namespace Bit5 { // Field Description
								enum {Lsb = 5};
								enum {FieldMask = 0x00000020};
								enum {Value_ValueAtReset = 0x0};
								enum {ValueMask_ValueAtReset = 0x00000000};
								enum {Value_Input = 0x0};
								enum {ValueMask_Input = 0x00000000};
								enum {Value_Output = 0x1};
								enum {ValueMask_Output = 0x00000020};
								};
							namespace Bit4 { // Field Description
								enum {Lsb = 4};
								enum {FieldMask = 0x00000010};
								enum {Value_ValueAtReset = 0x0};
								enum {ValueMask_ValueAtReset = 0x00000000};
								enum {Value_Input = 0x0};
								enum {ValueMask_Input = 0x00000000};
								enum {Value_Output = 0x1};
								enum {ValueMask_Output = 0x00000010};
								};
							namespace Bit3 { // Field Description
								enum {Lsb = 3};
								enum {FieldMask = 0x00000008};
								enum {Value_ValueAtReset = 0x0};
								enum {ValueMask_ValueAtReset = 0x00000000};
								enum {Value_Input = 0x0};
								enum {ValueMask_Input = 0x00000000};
								enum {Value_Output = 0x1};
								enum {ValueMask_Output = 0x00000008};
								};
							namespace Bit2 { // Field Description
								enum {Lsb = 2};
								enum {FieldMask = 0x00000004};
								enum {Value_ValueAtReset = 0x0};
								enum {ValueMask_ValueAtReset = 0x00000000};
								enum {Value_Input = 0x0};
								enum {ValueMask_Input = 0x00000000};
								enum {Value_Output = 0x1};
								enum {ValueMask_Output = 0x00000004};
								};
							namespace Bit1 { // Field Description
								enum {Lsb = 1};
								enum {FieldMask = 0x00000002};
								enum {Value_ValueAtReset = 0x0};
								enum {ValueMask_ValueAtReset = 0x00000000};
								enum {Value_Input = 0x0};
								enum {ValueMask_Input = 0x00000000};
								enum {Value_Output = 0x1};
								enum {ValueMask_Output = 0x00000002};
								};
							namespace Bit0 { // Field Description
								enum {Lsb = 0};
								enum {FieldMask = 0x00000001};
								enum {Value_ValueAtReset = 0x0};
								enum {ValueMask_ValueAtReset = 0x00000000};
								enum {Value_Input = 0x0};
								enum {ValueMask_Input = 0x00000000};
								enum {Value_Output = 0x1};
								enum {ValueMask_Output = 0x00000001};
								};
							};
						namespace IFG { // Register description
							typedef uint8_t	Reg;
							namespace Bit7 { // Field Description
								enum {Lsb = 7};
								enum {FieldMask = 0x00000080};
								enum {Value_ValueAtReset = 0x0};
								enum {ValueMask_ValueAtReset = 0x00000000};
								enum {Value_Reset = 0x0};
								enum {ValueMask_Reset = 0x00000000};
								enum {Value_Set = 0x1};
								enum {ValueMask_Set = 0x00000080};
								};
							namespace Bit6 { // Field Description
								enum {Lsb = 6};
								enum {FieldMask = 0x00000040};
								enum {Value_ValueAtReset = 0x0};
								enum {ValueMask_ValueAtReset = 0x00000000};
								enum {Value_Reset = 0x0};
								enum {ValueMask_Reset = 0x00000000};
								enum {Value_Set = 0x1};
								enum {ValueMask_Set = 0x00000040};
								};
							namespace Bit5 { // Field Description
								enum {Lsb = 5};
								enum {FieldMask = 0x00000020};
								enum {Value_ValueAtReset = 0x0};
								enum {ValueMask_ValueAtReset = 0x00000000};
								enum {Value_Reset = 0x0};
								enum {ValueMask_Reset = 0x00000000};
								enum {Value_Set = 0x1};
								enum {ValueMask_Set = 0x00000020};
								};
							namespace Bit4 { // Field Description
								enum {Lsb = 4};
								enum {FieldMask = 0x00000010};
								enum {Value_ValueAtReset = 0x0};
								enum {ValueMask_ValueAtReset = 0x00000000};
								enum {Value_Reset = 0x0};
								enum {ValueMask_Reset = 0x00000000};
								enum {Value_Set = 0x1};
								enum {ValueMask_Set = 0x00000010};
								};
							namespace Bit3 { // Field Description
								enum {Lsb = 3};
								enum {FieldMask = 0x00000008};
								enum {Value_ValueAtReset = 0x0};
								enum {ValueMask_ValueAtReset = 0x00000000};
								enum {Value_Reset = 0x0};
								enum {ValueMask_Reset = 0x00000000};
								enum {Value_Set = 0x1};
								enum {ValueMask_Set = 0x00000008};
								};
							namespace Bit2 { // Field Description
								enum {Lsb = 2};
								enum {FieldMask = 0x00000004};
								enum {Value_ValueAtReset = 0x0};
								enum {ValueMask_ValueAtReset = 0x00000000};
								enum {Value_Reset = 0x0};
								enum {ValueMask_Reset = 0x00000000};
								enum {Value_Set = 0x1};
								enum {ValueMask_Set = 0x00000004};
								};
							namespace Bit1 { // Field Description
								enum {Lsb = 1};
								enum {FieldMask = 0x00000002};
								enum {Value_ValueAtReset = 0x0};
								enum {ValueMask_ValueAtReset = 0x00000000};
								enum {Value_Reset = 0x0};
								enum {ValueMask_Reset = 0x00000000};
								enum {Value_Set = 0x1};
								enum {ValueMask_Set = 0x00000002};
								};
							namespace Bit0 { // Field Description
								enum {Lsb = 0};
								enum {FieldMask = 0x00000001};
								enum {Value_ValueAtReset = 0x0};
								enum {ValueMask_ValueAtReset = 0x00000000};
								enum {Value_Reset = 0x0};
								enum {ValueMask_Reset = 0x00000000};
								enum {Value_Set = 0x1};
								enum {ValueMask_Set = 0x00000001};
								};
							};
						namespace IES { // Register description
							typedef uint8_t	Reg;
							namespace Bit7 { // Field Description
								enum {Lsb = 7};
								enum {FieldMask = 0x00000080};
								enum {Value_ValueAtReset = 0x0};
								enum {ValueMask_ValueAtReset = 0x00000000};
								enum {Value_Reset = 0x0};
								enum {ValueMask_Reset = 0x00000000};
								enum {Value_Set = 0x1};
								enum {ValueMask_Set = 0x00000080};
								};
							namespace Bit6 { // Field Description
								enum {Lsb = 6};
								enum {FieldMask = 0x00000040};
								enum {Value_ValueAtReset = 0x0};
								enum {ValueMask_ValueAtReset = 0x00000000};
								enum {Value_Reset = 0x0};
								enum {ValueMask_Reset = 0x00000000};
								enum {Value_Set = 0x1};
								enum {ValueMask_Set = 0x00000040};
								};
							namespace Bit5 { // Field Description
								enum {Lsb = 5};
								enum {FieldMask = 0x00000020};
								enum {Value_ValueAtReset = 0x0};
								enum {ValueMask_ValueAtReset = 0x00000000};
								enum {Value_Reset = 0x0};
								enum {ValueMask_Reset = 0x00000000};
								enum {Value_Set = 0x1};
								enum {ValueMask_Set = 0x00000020};
								};
							namespace Bit4 { // Field Description
								enum {Lsb = 4};
								enum {FieldMask = 0x00000010};
								enum {Value_ValueAtReset = 0x0};
								enum {ValueMask_ValueAtReset = 0x00000000};
								enum {Value_Reset = 0x0};
								enum {ValueMask_Reset = 0x00000000};
								enum {Value_Set = 0x1};
								enum {ValueMask_Set = 0x00000010};
								};
							namespace Bit3 { // Field Description
								enum {Lsb = 3};
								enum {FieldMask = 0x00000008};
								enum {Value_ValueAtReset = 0x0};
								enum {ValueMask_ValueAtReset = 0x00000000};
								enum {Value_Reset = 0x0};
								enum {ValueMask_Reset = 0x00000000};
								enum {Value_Set = 0x1};
								enum {ValueMask_Set = 0x00000008};
								};
							namespace Bit2 { // Field Description
								enum {Lsb = 2};
								enum {FieldMask = 0x00000004};
								enum {Value_ValueAtReset = 0x0};
								enum {ValueMask_ValueAtReset = 0x00000000};
								enum {Value_Reset = 0x0};
								enum {ValueMask_Reset = 0x00000000};
								enum {Value_Set = 0x1};
								enum {ValueMask_Set = 0x00000004};
								};
							namespace Bit1 { // Field Description
								enum {Lsb = 1};
								enum {FieldMask = 0x00000002};
								enum {Value_ValueAtReset = 0x0};
								enum {ValueMask_ValueAtReset = 0x00000000};
								enum {Value_Reset = 0x0};
								enum {ValueMask_Reset = 0x00000000};
								enum {Value_Set = 0x1};
								enum {ValueMask_Set = 0x00000002};
								};
							namespace Bit0 { // Field Description
								enum {Lsb = 0};
								enum {FieldMask = 0x00000001};
								enum {Value_ValueAtReset = 0x0};
								enum {ValueMask_ValueAtReset = 0x00000000};
								enum {Value_Reset = 0x0};
								enum {ValueMask_Reset = 0x00000000};
								enum {Value_Set = 0x1};
								enum {ValueMask_Set = 0x00000001};
								};
							};
						namespace IE { // Register description
							typedef uint8_t	Reg;
							namespace Bit7 { // Field Description
								enum {Lsb = 7};
								enum {FieldMask = 0x00000080};
								enum {Value_ValueAtReset = 0x0};
								enum {ValueMask_ValueAtReset = 0x00000000};
								enum {Value_Reset = 0x0};
								enum {ValueMask_Reset = 0x00000000};
								enum {Value_Set = 0x1};
								enum {ValueMask_Set = 0x00000080};
								};
							namespace Bit6 { // Field Description
								enum {Lsb = 6};
								enum {FieldMask = 0x00000040};
								enum {Value_ValueAtReset = 0x0};
								enum {ValueMask_ValueAtReset = 0x00000000};
								enum {Value_Reset = 0x0};
								enum {ValueMask_Reset = 0x00000000};
								enum {Value_Set = 0x1};
								enum {ValueMask_Set = 0x00000040};
								};
							namespace Bit5 { // Field Description
								enum {Lsb = 5};
								enum {FieldMask = 0x00000020};
								enum {Value_ValueAtReset = 0x0};
								enum {ValueMask_ValueAtReset = 0x00000000};
								enum {Value_Reset = 0x0};
								enum {ValueMask_Reset = 0x00000000};
								enum {Value_Set = 0x1};
								enum {ValueMask_Set = 0x00000020};
								};
							namespace Bit4 { // Field Description
								enum {Lsb = 4};
								enum {FieldMask = 0x00000010};
								enum {Value_ValueAtReset = 0x0};
								enum {ValueMask_ValueAtReset = 0x00000000};
								enum {Value_Reset = 0x0};
								enum {ValueMask_Reset = 0x00000000};
								enum {Value_Set = 0x1};
								enum {ValueMask_Set = 0x00000010};
								};
							namespace Bit3 { // Field Description
								enum {Lsb = 3};
								enum {FieldMask = 0x00000008};
								enum {Value_ValueAtReset = 0x0};
								enum {ValueMask_ValueAtReset = 0x00000000};
								enum {Value_Reset = 0x0};
								enum {ValueMask_Reset = 0x00000000};
								enum {Value_Set = 0x1};
								enum {ValueMask_Set = 0x00000008};
								};
							namespace Bit2 { // Field Description
								enum {Lsb = 2};
								enum {FieldMask = 0x00000004};
								enum {Value_ValueAtReset = 0x0};
								enum {ValueMask_ValueAtReset = 0x00000000};
								enum {Value_Reset = 0x0};
								enum {ValueMask_Reset = 0x00000000};
								enum {Value_Set = 0x1};
								enum {ValueMask_Set = 0x00000004};
								};
							namespace Bit1 { // Field Description
								enum {Lsb = 1};
								enum {FieldMask = 0x00000002};
								enum {Value_ValueAtReset = 0x0};
								enum {ValueMask_ValueAtReset = 0x00000000};
								enum {Value_Reset = 0x0};
								enum {ValueMask_Reset = 0x00000000};
								enum {Value_Set = 0x1};
								enum {ValueMask_Set = 0x00000002};
								};
							namespace Bit0 { // Field Description
								enum {Lsb = 0};
								enum {FieldMask = 0x00000001};
								enum {Value_ValueAtReset = 0x0};
								enum {ValueMask_ValueAtReset = 0x00000000};
								enum {Value_Reset = 0x0};
								enum {ValueMask_Reset = 0x00000000};
								enum {Value_Set = 0x1};
								enum {ValueMask_Set = 0x00000001};
								};
							};
						namespace SEL { // Register description
							typedef uint8_t	Reg;
							namespace Bit7 { // Field Description
								enum {Lsb = 7};
								enum {FieldMask = 0x00000080};
								enum {Value_ValueAtReset = 0x0};
								enum {ValueMask_ValueAtReset = 0x00000000};
								enum {Value_IO = 0x0};
								enum {ValueMask_IO = 0x00000000};
								enum {Value_Peripheral = 0x1};
								enum {ValueMask_Peripheral = 0x00000080};
								};
							namespace Bit6 { // Field Description
								enum {Lsb = 6};
								enum {FieldMask = 0x00000040};
								enum {Value_ValueAtReset = 0x0};
								enum {ValueMask_ValueAtReset = 0x00000000};
								enum {Value_IO = 0x0};
								enum {ValueMask_IO = 0x00000000};
								enum {Value_Peripheral = 0x1};
								enum {ValueMask_Peripheral = 0x00000040};
								};
							namespace Bit5 { // Field Description
								enum {Lsb = 5};
								enum {FieldMask = 0x00000020};
								enum {Value_ValueAtReset = 0x0};
								enum {ValueMask_ValueAtReset = 0x00000000};
								enum {Value_IO = 0x0};
								enum {ValueMask_IO = 0x00000000};
								enum {Value_Peripheral = 0x1};
								enum {ValueMask_Peripheral = 0x00000020};
								};
							namespace Bit4 { // Field Description
								enum {Lsb = 4};
								enum {FieldMask = 0x00000010};
								enum {Value_ValueAtReset = 0x0};
								enum {ValueMask_ValueAtReset = 0x00000000};
								enum {Value_IO = 0x0};
								enum {ValueMask_IO = 0x00000000};
								enum {Value_Peripheral = 0x1};
								enum {ValueMask_Peripheral = 0x00000010};
								};
							namespace Bit3 { // Field Description
								enum {Lsb = 3};
								enum {FieldMask = 0x00000008};
								enum {Value_ValueAtReset = 0x0};
								enum {ValueMask_ValueAtReset = 0x00000000};
								enum {Value_IO = 0x0};
								enum {ValueMask_IO = 0x00000000};
								enum {Value_Peripheral = 0x1};
								enum {ValueMask_Peripheral = 0x00000008};
								};
							namespace Bit2 { // Field Description
								enum {Lsb = 2};
								enum {FieldMask = 0x00000004};
								enum {Value_ValueAtReset = 0x0};
								enum {ValueMask_ValueAtReset = 0x00000000};
								enum {Value_IO = 0x0};
								enum {ValueMask_IO = 0x00000000};
								enum {Value_Peripheral = 0x1};
								enum {ValueMask_Peripheral = 0x00000004};
								};
							namespace Bit1 { // Field Description
								enum {Lsb = 1};
								enum {FieldMask = 0x00000002};
								enum {Value_ValueAtReset = 0x0};
								enum {ValueMask_ValueAtReset = 0x00000000};
								enum {Value_IO = 0x0};
								enum {ValueMask_IO = 0x00000000};
								enum {Value_Peripheral = 0x1};
								enum {ValueMask_Peripheral = 0x00000002};
								};
							namespace Bit0 { // Field Description
								enum {Lsb = 0};
								enum {FieldMask = 0x00000001};
								enum {Value_ValueAtReset = 0x0};
								enum {ValueMask_ValueAtReset = 0x00000000};
								enum {Value_IO = 0x0};
								enum {ValueMask_IO = 0x00000000};
								enum {Value_Peripheral = 0x1};
								enum {ValueMask_Peripheral = 0x00000001};
								};
							};
						};
					};
				};
			};
		};
	};
#endif
