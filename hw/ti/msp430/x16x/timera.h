#ifndef _oscl_hw_ti_msp430_x16x_timerah_
#define _oscl_hw_ti_msp430_x16x_timerah_
#include <stdint.h>
namespace Oscl { // Namespace description
	namespace HW { // Namespace description
		namespace TI { // Namespace description
			namespace Msp430 { // Namespace description
				namespace x16x { // Namespace description
					namespace TimerA { // Namespace description
						namespace TACTL { // Register description
							typedef uint16_t	Reg;
							namespace TASSELx { // Field Description
								enum {Lsb = 8};
								enum {FieldMask = 0x00000300};
								enum {Value_ValueAtReset = 0x0};
								enum {ValueMask_ValueAtReset = 0x00000000};
								enum {Value_TACLK = 0x0};
								enum {ValueMask_TACLK = 0x00000000};
								enum {Value_ACLK = 0x1};
								enum {ValueMask_ACLK = 0x00000100};
								enum {Value_SMCLK = 0x2};
								enum {ValueMask_SMCLK = 0x00000200};
								enum {Value_INCLK = 0x3};
								enum {ValueMask_INCLK = 0x00000300};
								};
							namespace IDx { // Field Description
								enum {Lsb = 6};
								enum {FieldMask = 0x000000C0};
								enum {Value_ValueAtReset = 0x0};
								enum {ValueMask_ValueAtReset = 0x00000000};
								enum {Value_DivideBy1 = 0x0};
								enum {ValueMask_DivideBy1 = 0x00000000};
								enum {Value_DivideBy2 = 0x1};
								enum {ValueMask_DivideBy2 = 0x00000040};
								enum {Value_DivideBy4 = 0x2};
								enum {ValueMask_DivideBy4 = 0x00000080};
								enum {Value_DivideBy8 = 0x3};
								enum {ValueMask_DivideBy8 = 0x000000C0};
								};
							namespace MCx { // Field Description
								enum {Lsb = 4};
								enum {FieldMask = 0x00000030};
								enum {Value_ValueAtReset = 0x0};
								enum {ValueMask_ValueAtReset = 0x00000000};
								enum {Value_Stop = 0x0};
								enum {ValueMask_Stop = 0x00000000};
								enum {Value_Up = 0x1};
								enum {ValueMask_Up = 0x00000010};
								enum {Value_Continuous = 0x2};
								enum {ValueMask_Continuous = 0x00000020};
								enum {Value_UpDown = 0x3};
								enum {ValueMask_UpDown = 0x00000030};
								};
							namespace TACLR { // Field Description
								enum {Lsb = 2};
								enum {FieldMask = 0x00000004};
								enum {Value_ValueAtReset = 0x0};
								enum {ValueMask_ValueAtReset = 0x00000000};
								enum {Value_Nop = 0x0};
								enum {ValueMask_Nop = 0x00000000};
								enum {Value_Clear = 0x1};
								enum {ValueMask_Clear = 0x00000004};
								};
							namespace TAIE { // Field Description
								enum {Lsb = 1};
								enum {FieldMask = 0x00000002};
								enum {Value_ValueAtReset = 0x0};
								enum {ValueMask_ValueAtReset = 0x00000000};
								enum {Value_InterruptDisabled = 0x0};
								enum {ValueMask_InterruptDisabled = 0x00000000};
								enum {Value_InterruptDisable = 0x0};
								enum {ValueMask_InterruptDisable = 0x00000000};
								enum {Value_InterruptEnabled = 0x1};
								enum {ValueMask_InterruptEnabled = 0x00000002};
								enum {Value_InterruptEnable = 0x1};
								enum {ValueMask_InterruptEnable = 0x00000002};
								};
							namespace TAIFG { // Field Description
								enum {Lsb = 0};
								enum {FieldMask = 0x00000001};
								enum {Value_ValueAtReset = 0x0};
								enum {ValueMask_ValueAtReset = 0x00000000};
								enum {Value_InterruptNotPending = 0x0};
								enum {ValueMask_InterruptNotPending = 0x00000000};
								enum {Value_InterruptPending = 0x1};
								enum {ValueMask_InterruptPending = 0x00000001};
								};
							};
						namespace TAR { // Register description
							typedef uint16_t	Reg;
							namespace TARx { // Field Description
								enum {Lsb = 0};
								enum {FieldMask = 0x0000FFFF};
								enum {Value_ValueAtReset = 0x0};
								enum {ValueMask_ValueAtReset = 0x00000000};
								enum {Value_Max = 0xFFFF};
								enum {ValueMask_Max = 0x0000FFFF};
								enum {Value_Min = 0x0};
								enum {ValueMask_Min = 0x00000000};
								};
							};
						namespace TACCTLx { // Register description
							typedef uint16_t	Reg;
							namespace CMx { // Field Description
								enum {Lsb = 14};
								enum {FieldMask = 0x0000C000};
								enum {Value_ValueAtReset = 0x0};
								enum {ValueMask_ValueAtReset = 0x00000000};
								enum {Value_NoCapture = 0x0};
								enum {ValueMask_NoCapture = 0x00000000};
								enum {Value_CaptureOnRisingEdge = 0x0};
								enum {ValueMask_CaptureOnRisingEdge = 0x00000000};
								enum {Value_CaptureOnFallingEdge = 0x0};
								enum {ValueMask_CaptureOnFallingEdge = 0x00000000};
								enum {Value_CaptureOnBothEdges = 0x0};
								enum {ValueMask_CaptureOnBothEdges = 0x00000000};
								};
							namespace CCISx { // Field Description
								enum {Lsb = 12};
								enum {FieldMask = 0x00003000};
								enum {Value_ValueAtReset = 0x0};
								enum {ValueMask_ValueAtReset = 0x00000000};
								enum {Value_CCIxA = 0x0};
								enum {ValueMask_CCIxA = 0x00000000};
								enum {Value_CCIxB = 0x1};
								enum {ValueMask_CCIxB = 0x00001000};
								enum {Value_GND = 0x2};
								enum {ValueMask_GND = 0x00002000};
								enum {Value_Vcc = 0x3};
								enum {ValueMask_Vcc = 0x00003000};
								};
							namespace SCS { // Field Description
								enum {Lsb = 11};
								enum {FieldMask = 0x00000800};
								enum {Value_ValueAtReset = 0x0};
								enum {ValueMask_ValueAtReset = 0x00000000};
								enum {Value_Async = 0x0};
								enum {ValueMask_Async = 0x00000000};
								enum {Value_Sync = 0x1};
								enum {ValueMask_Sync = 0x00000800};
								};
							namespace SCCI { // Field Description
								enum {Lsb = 10};
								enum {FieldMask = 0x00000400};
								};
							namespace CAP { // Field Description
								enum {Lsb = 8};
								enum {FieldMask = 0x00000100};
								enum {Value_ValueAtReset = 0x0};
								enum {ValueMask_ValueAtReset = 0x00000000};
								enum {Value_CompareMode = 0x0};
								enum {ValueMask_CompareMode = 0x00000000};
								enum {Value_CaptureMode = 0x1};
								enum {ValueMask_CaptureMode = 0x00000100};
								};
							namespace OUTMODx { // Field Description
								enum {Lsb = 5};
								enum {FieldMask = 0x000000E0};
								enum {Value_ValueAtReset = 0x0};
								enum {ValueMask_ValueAtReset = 0x00000000};
								enum {Value_OutBitValue = 0x0};
								enum {ValueMask_OutBitValue = 0x00000000};
								enum {Value_Set = 0x1};
								enum {ValueMask_Set = 0x00000020};
								enum {Value_ToggleReset = 0x2};
								enum {ValueMask_ToggleReset = 0x00000040};
								enum {Value_SetReset = 0x3};
								enum {ValueMask_SetReset = 0x00000060};
								enum {Value_Toggle = 0x4};
								enum {ValueMask_Toggle = 0x00000080};
								enum {Value_Reset = 0x5};
								enum {ValueMask_Reset = 0x000000A0};
								enum {Value_ToggleSet = 0x6};
								enum {ValueMask_ToggleSet = 0x000000C0};
								enum {Value_ResetSet = 0x7};
								enum {ValueMask_ResetSet = 0x000000E0};
								};
							namespace CCIE { // Field Description
								enum {Lsb = 4};
								enum {FieldMask = 0x00000010};
								enum {Value_ValueAtReset = 0x0};
								enum {ValueMask_ValueAtReset = 0x00000000};
								enum {Value_InterruptDisabled = 0x0};
								enum {ValueMask_InterruptDisabled = 0x00000000};
								enum {Value_InterruptDisable = 0x0};
								enum {ValueMask_InterruptDisable = 0x00000000};
								enum {Value_InterruptEnabled = 0x1};
								enum {ValueMask_InterruptEnabled = 0x00000010};
								enum {Value_InterruptEnable = 0x1};
								enum {ValueMask_InterruptEnable = 0x00000010};
								};
							namespace CCI { // Field Description
								enum {Lsb = 3};
								enum {FieldMask = 0x00000008};
								};
							namespace OUT { // Field Description
								enum {Lsb = 2};
								enum {FieldMask = 0x00000004};
								enum {Value_ValueAtReset = 0x0};
								enum {ValueMask_ValueAtReset = 0x00000000};
								enum {Value_Low = 0x0};
								enum {ValueMask_Low = 0x00000000};
								enum {Value_High = 0x1};
								enum {ValueMask_High = 0x00000004};
								};
							namespace COV { // Field Description
								enum {Lsb = 1};
								enum {FieldMask = 0x00000002};
								enum {Value_ValueAtReset = 0x0};
								enum {ValueMask_ValueAtReset = 0x00000000};
								enum {Value_NoCaptureOverflow = 0x0};
								enum {ValueMask_NoCaptureOverflow = 0x00000000};
								enum {Value_CaptureOverflowed = 0x1};
								enum {ValueMask_CaptureOverflowed = 0x00000002};
								};
							namespace CCIFG { // Field Description
								enum {Lsb = 0};
								enum {FieldMask = 0x00000001};
								enum {Value_ValueAtReset = 0x0};
								enum {ValueMask_ValueAtReset = 0x00000000};
								enum {Value_InterruptNotPending = 0x0};
								enum {ValueMask_InterruptNotPending = 0x00000000};
								enum {Value_InterruptPending = 0x0};
								enum {ValueMask_InterruptPending = 0x00000000};
								};
							};
						namespace TACCRx { // Register description
							typedef uint16_t	Reg;
							enum {Max = 65535};
							enum {Min = 0};
							};
						namespace TAIV { // Register description
							typedef uint16_t	Reg;
							enum {ValueAtReset = 0};
							enum {InterruptNotPending = 0};
							enum {CaptureCompare1 = 2};
							enum {CaptureCompare2 = 4};
							enum {TimerOverflow = 10};
							namespace TAIVx { // Field Description
								enum {Lsb = 1};
								enum {FieldMask = 0x0000000E};
								enum {Value_ValueAtReset = 0x0};
								enum {ValueMask_ValueAtReset = 0x00000000};
								enum {Value_InterruptNotPending = 0x0};
								enum {ValueMask_InterruptNotPending = 0x00000000};
								enum {Value_CaptureCompare1 = 0x1};
								enum {ValueMask_CaptureCompare1 = 0x00000002};
								enum {Value_CaptureCompare2 = 0x2};
								enum {ValueMask_CaptureCompare2 = 0x00000004};
								enum {Value_TimerOverflow = 0x5};
								enum {ValueMask_TimerOverflow = 0x0000000A};
								};
							};
						};
					};
				};
			};
		};
	};
#endif
