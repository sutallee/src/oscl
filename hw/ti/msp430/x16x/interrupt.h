#ifndef _oscl_hw_ti_msp430_x16x_interrupth_
#define _oscl_hw_ti_msp430_x16x_interrupth_
#include <stdint.h>
namespace Oscl { // Namespace description
	namespace HW { // Namespace description
		namespace TI { // Namespace description
			namespace Msp430 { // Namespace description
				namespace x16x { // Namespace description
					namespace Interrupt { // Namespace description
						namespace IE1 { // Register description
							typedef uint8_t	Reg;
							namespace UTXIE0 { // Field Description
								enum {Lsb = 7};
								enum {FieldMask = 0x00000080};
								enum {Value_ValueAtReset = 0x0};
								enum {ValueMask_ValueAtReset = 0x00000000};
								enum {Value_InterruptDisabled = 0x0};
								enum {ValueMask_InterruptDisabled = 0x00000000};
								enum {Value_InterruptDisable = 0x0};
								enum {ValueMask_InterruptDisable = 0x00000000};
								enum {Value_InterruptEnabled = 0x1};
								enum {ValueMask_InterruptEnabled = 0x00000080};
								enum {Value_InterruptEnable = 0x1};
								enum {ValueMask_InterruptEnable = 0x00000080};
								};
							namespace URXIE0 { // Field Description
								enum {Lsb = 6};
								enum {FieldMask = 0x00000040};
								enum {Value_ValueAtReset = 0x0};
								enum {ValueMask_ValueAtReset = 0x00000000};
								enum {Value_InterruptDisabled = 0x0};
								enum {ValueMask_InterruptDisabled = 0x00000000};
								enum {Value_InterruptDisable = 0x0};
								enum {ValueMask_InterruptDisable = 0x00000000};
								enum {Value_InterruptEnabled = 0x1};
								enum {ValueMask_InterruptEnabled = 0x00000040};
								enum {Value_InterruptEnable = 0x1};
								enum {ValueMask_InterruptEnable = 0x00000040};
								};
							namespace OFIE { // Field Description
								enum {Lsb = 1};
								enum {FieldMask = 0x00000002};
								enum {Value_ValueAtReset = 0x0};
								enum {ValueMask_ValueAtReset = 0x00000000};
								enum {Value_InterruptDisabled = 0x0};
								enum {ValueMask_InterruptDisabled = 0x00000000};
								enum {Value_InterruptDisable = 0x0};
								enum {ValueMask_InterruptDisable = 0x00000000};
								enum {Value_InterruptEnabled = 0x1};
								enum {ValueMask_InterruptEnabled = 0x00000002};
								enum {Value_InterruptEnable = 0x1};
								enum {ValueMask_InterruptEnable = 0x00000002};
								};
							};
						namespace IE2 { // Register description
							typedef uint8_t	Reg;
							namespace UTXIE1 { // Field Description
								enum {Lsb = 5};
								enum {FieldMask = 0x00000020};
								enum {Value_ValueAtReset = 0x0};
								enum {ValueMask_ValueAtReset = 0x00000000};
								enum {Value_InterruptDisabled = 0x0};
								enum {ValueMask_InterruptDisabled = 0x00000000};
								enum {Value_InterruptDisable = 0x0};
								enum {ValueMask_InterruptDisable = 0x00000000};
								enum {Value_InterruptEnabled = 0x1};
								enum {ValueMask_InterruptEnabled = 0x00000020};
								enum {Value_InterruptEnable = 0x1};
								enum {ValueMask_InterruptEnable = 0x00000020};
								};
							namespace URXIE1 { // Field Description
								enum {Lsb = 4};
								enum {FieldMask = 0x00000010};
								enum {Value_ValueAtReset = 0x0};
								enum {ValueMask_ValueAtReset = 0x00000000};
								enum {Value_InterruptDisabled = 0x0};
								enum {ValueMask_InterruptDisabled = 0x00000000};
								enum {Value_InterruptDisable = 0x0};
								enum {ValueMask_InterruptDisable = 0x00000000};
								enum {Value_InterruptEnabled = 0x1};
								enum {ValueMask_InterruptEnabled = 0x00000010};
								enum {Value_InterruptEnable = 0x1};
								enum {ValueMask_InterruptEnable = 0x00000010};
								};
							};
						namespace IFG1 { // Register description
							typedef uint8_t	Reg;
							namespace OFIFG { // Field Description
								enum {Lsb = 1};
								enum {FieldMask = 0x00000002};
								enum {Value_ValueAtReset = 0x1};
								enum {ValueMask_ValueAtReset = 0x00000002};
								enum {Value_InterruptNotPending = 0x0};
								enum {ValueMask_InterruptNotPending = 0x00000000};
								enum {Value_InterruptPending = 0x1};
								enum {ValueMask_InterruptPending = 0x00000002};
								};
							namespace WDTIFG { // Field Description
								enum {Lsb = 0};
								enum {FieldMask = 0x00000001};
								enum {Value_ValueAtReset = 0x0};
								enum {ValueMask_ValueAtReset = 0x00000000};
								enum {Value_InterruptNotPending = 0x0};
								enum {ValueMask_InterruptNotPending = 0x00000000};
								enum {Value_InterruptPending = 0x1};
								enum {ValueMask_InterruptPending = 0x00000001};
								};
							namespace NMIFG { // Field Description
								enum {Lsb = 4};
								enum {FieldMask = 0x00000010};
								enum {Value_ValueAtReset = 0x0};
								enum {ValueMask_ValueAtReset = 0x00000000};
								enum {Value_InterruptNotPending = 0x0};
								enum {ValueMask_InterruptNotPending = 0x00000000};
								enum {Value_InterruptPending = 0x1};
								enum {ValueMask_InterruptPending = 0x00000010};
								};
							namespace UTXIFG0 { // Field Description
								enum {Lsb = 7};
								enum {FieldMask = 0x00000080};
								enum {Value_ValueAtReset = 0x1};
								enum {ValueMask_ValueAtReset = 0x00000080};
								enum {Value_InterruptNotPending = 0x0};
								enum {ValueMask_InterruptNotPending = 0x00000000};
								enum {Value_InterruptPending = 0x1};
								enum {ValueMask_InterruptPending = 0x00000080};
								};
							namespace URXIFG0 { // Field Description
								enum {Lsb = 6};
								enum {FieldMask = 0x00000040};
								enum {Value_ValueAtReset = 0x0};
								enum {ValueMask_ValueAtReset = 0x00000000};
								enum {Value_InterruptNotPending = 0x0};
								enum {ValueMask_InterruptNotPending = 0x00000000};
								enum {Value_InterruptPending = 0x1};
								enum {ValueMask_InterruptPending = 0x00000040};
								};
							};
						namespace IFG2 { // Register description
							typedef uint8_t	Reg;
							namespace UTXIFG { // Field Description
								enum {Lsb = 5};
								enum {FieldMask = 0x00000020};
								enum {Value_ValueAtReset = 0x1};
								enum {ValueMask_ValueAtReset = 0x00000020};
								enum {Value_InterruptNotPending = 0x0};
								enum {ValueMask_InterruptNotPending = 0x00000000};
								enum {Value_InterruptPending = 0x1};
								enum {ValueMask_InterruptPending = 0x00000020};
								};
							namespace URXIFG { // Field Description
								enum {Lsb = 4};
								enum {FieldMask = 0x00000010};
								enum {Value_ValueAtReset = 0x1};
								enum {ValueMask_ValueAtReset = 0x00000010};
								enum {Value_InterruptNotPending = 0x0};
								enum {ValueMask_InterruptNotPending = 0x00000000};
								enum {Value_InterruptPending = 0x1};
								enum {ValueMask_InterruptPending = 0x00000010};
								};
							};
						};
					};
				};
			};
		};
	};
#endif
