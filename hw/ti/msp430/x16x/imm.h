/*
   Copyright (C) 2007 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_hw_ti_msp430_x16x_immh_
#define _oscl_hw_ti_msp430_x16x_immh_

#include "oscl/hw/ti/msp430/x16x/interrupt.h"
#include "oscl/hw/ti/msp430/x16x/bcm.h"
#include "oscl/hw/ti/msp430/x16x/usart.h"
#include "oscl/hw/ti/msp430/x16x/timera.h"
#include "oscl/hw/ti/msp430/x16x/timerb.h"
#include "oscl/hw/ti/msp430/x16x/module.h"
#include "oscl/hw/ti/msp430/x16x/digitalio.h"

/** */
namespace Oscl {
/** */
namespace HW {
/** */
namespace TI {
/** */
namespace Msp430 {
/** */
namespace x16x {

/** 0x000 */
struct SpecialFunctionRegisters {
	/** */
	volatile Oscl::HW::TI::Msp430::x16x::Interrupt::IE1::Reg		IE1;
	/** */
	volatile Oscl::HW::TI::Msp430::x16x::Interrupt::IE2::Reg		IE2;
	/** */
	volatile Oscl::HW::TI::Msp430::x16x::Interrupt::IFG1::Reg	IFG1;
	/** */
	volatile Oscl::HW::TI::Msp430::x16x::Interrupt::IFG2::Reg	IFG2;
	/** */
	volatile Oscl::HW::TI::Msp430::x16x::Module::ME1::Reg		ME1;
	/** */
	volatile Oscl::HW::TI::Msp430::x16x::Module::ME2::Reg		ME2;
	/** */
	uint8_t	reserved[0x010-0x006];
	};

/** 0x056 - 0x58 */
struct BasicClockModule {
	/** 0x56 */
	volatile Oscl::HW::TI::Msp430::x16x::BCM::DCOCTL::Reg		DCOCTL;
	/** 0x57 */
	volatile Oscl::HW::TI::Msp430::x16x::BCM::BCSCTL1::Reg		BCSCTL1;
	/** 0x58 */
	volatile Oscl::HW::TI::Msp430::x16x::BCM::BCSCTL2::Reg		BCSCTL2;
	};

/** 0x160 - 17F*/
struct Timer_A {
	/** 0x160 */
	volatile Oscl::HW::TI::Msp430::x16x::TimerA::TACTL::Reg		TACTL;
	/** 0x162 */
	volatile Oscl::HW::TI::Msp430::x16x::TimerA::TACCTLx::Reg	TACCTL0;
	/** 0x164 */
	volatile Oscl::HW::TI::Msp430::x16x::TimerA::TACCTLx::Reg	TACCTL1;
	/** 0x166 */
	volatile Oscl::HW::TI::Msp430::x16x::TimerA::TACCTLx::Reg	TACCTL2;
	/** */
	uint8_t											reserved3[0x170-0x168];
	/** 0x170 */
	volatile Oscl::HW::TI::Msp430::x16x::TimerA::TAR::Reg		TAR;
	/** 0x172 */
	volatile Oscl::HW::TI::Msp430::x16x::TimerA::TACCRx::Reg		TACCR0;
	/** 0x174 */
	volatile Oscl::HW::TI::Msp430::x16x::TimerA::TACCRx::Reg		TACCR1;
	/** 0x0176 */
	volatile Oscl::HW::TI::Msp430::x16x::TimerA::TACCRx::Reg		TACCR2;
	/** */
	uint8_t											reserved4[0x180-0x178];
	};

/** 0x180 - 0x19F */
struct Timer_B {
	/** 0x0180 */
	volatile Oscl::HW::TI::Msp430::x16x::TimerB::TBCTL::Reg		TBCTL;
	/** 0x182 */
	volatile Oscl::HW::TI::Msp430::x16x::TimerB::TBCCTLx::Reg	TBCCTL0;
	/** 0x184 */
	volatile Oscl::HW::TI::Msp430::x16x::TimerB::TBCCTLx::Reg	TBCCTL1;
	/** 0x186 */
	volatile Oscl::HW::TI::Msp430::x16x::TimerB::TBCCTLx::Reg	TBCCTL2;
	/** 0x188 */
	volatile Oscl::HW::TI::Msp430::x16x::TimerB::TBCCTLx::Reg	TBCCTL3;
	/** 0x18A */
	volatile Oscl::HW::TI::Msp430::x16x::TimerB::TBCCTLx::Reg	TBCCTL4;
	/** 0x18C */
	volatile Oscl::HW::TI::Msp430::x16x::TimerB::TBCCTLx::Reg	TBCCTL5;
	/** 0x18E */
	volatile Oscl::HW::TI::Msp430::x16x::TimerB::TBCCTLx::Reg	TBCCTL6;
	/** 0x190 */
	volatile Oscl::HW::TI::Msp430::x16x::TimerB::TBR::Reg		TBR;
	/** 0x192 */
	volatile Oscl::HW::TI::Msp430::x16x::TimerB::TBCCRx::Reg	TBCCR0;
	/** 0x194 */
	volatile Oscl::HW::TI::Msp430::x16x::TimerB::TBCCRx::Reg	TBCCR1;
	/** 0x196 */
	volatile Oscl::HW::TI::Msp430::x16x::TimerB::TBCCRx::Reg	TBCCR2;
	/** 0x198 */
	volatile Oscl::HW::TI::Msp430::x16x::TimerB::TBCCRx::Reg	TBCCR3;
	/** 0x19A */
	volatile Oscl::HW::TI::Msp430::x16x::TimerB::TBCCRx::Reg	TBCCR4;
	/** 0x19C */
	volatile Oscl::HW::TI::Msp430::x16x::TimerB::TBCCRx::Reg	TBCCR5;
	/** 0x19E */
	volatile Oscl::HW::TI::Msp430::x16x::TimerB::TBCCRx::Reg	TBCCR6;
	};

/** */
struct USARTx {
	/** */
	volatile Oscl::HW::TI::Msp430::x16x::USART::UxCTL::Reg		UxCTL;
	/** */
	volatile Oscl::HW::TI::Msp430::x16x::USART::UxTCTL::Reg		UxTCTL;
	/** */
	volatile Oscl::HW::TI::Msp430::x16x::USART::UxTCTL::Reg		UxRCTL;
	/** */
	volatile Oscl::HW::TI::Msp430::x16x::USART::UxMCTL::Reg		UxMCTL;
	/** */
	volatile Oscl::HW::TI::Msp430::x16x::USART::UxBR0::Reg		UxBR0;
	/** */
	volatile Oscl::HW::TI::Msp430::x16x::USART::UxBR1::Reg		UxBR1;
	/** */
	volatile Oscl::HW::TI::Msp430::x16x::USART::UxRXBUF::Reg		UxRXBUF;
	/** */
	volatile Oscl::HW::TI::Msp430::x16x::USART::UxTXBUF::Reg		UxTXBUF;
	};

/** 0x010-0x0FF */
struct EightBitPeripherals {
	/** 0x010 */
	unsigned char	reserved1[0x018-0x010];

	/** 0x018 */
	Oscl::HW::TI::Msp430::x16x::DigitalIO::IN::Reg	P3IN;
	/** 0x019 */
	Oscl::HW::TI::Msp430::x16x::DigitalIO::OUT::Reg	P3OUT;
	/** 0x01A */
	Oscl::HW::TI::Msp430::x16x::DigitalIO::DIR::Reg	P3DIR;
	/** 0x01B */
	Oscl::HW::TI::Msp430::x16x::DigitalIO::SEL::Reg	P3SEL;

	/** 0x01C */
	Oscl::HW::TI::Msp430::x16x::DigitalIO::IN::Reg	P4IN;
	/** 0x01D */
	Oscl::HW::TI::Msp430::x16x::DigitalIO::OUT::Reg	P4OUT;
	/** 0x01E */
	Oscl::HW::TI::Msp430::x16x::DigitalIO::DIR::Reg	P4DIR;
	/** 0x01F */
	Oscl::HW::TI::Msp430::x16x::DigitalIO::SEL::Reg	P4SEL;

	/** 0x020 */
	Oscl::HW::TI::Msp430::x16x::DigitalIO::IN::Reg	P1IN;
	/** 0x021 */
	Oscl::HW::TI::Msp430::x16x::DigitalIO::OUT::Reg	P1OUT;
	/** 0x022 */
	Oscl::HW::TI::Msp430::x16x::DigitalIO::DIR::Reg	P1DIR;
	/** 0x023 */
	Oscl::HW::TI::Msp430::x16x::DigitalIO::IFG::Reg	P1IFG;
	/** 0x024 */
	Oscl::HW::TI::Msp430::x16x::DigitalIO::IES::Reg	P1IES;
	/** 0x025 */
	Oscl::HW::TI::Msp430::x16x::DigitalIO::IE::Reg	P1IE;
	/** 0x026 */
	Oscl::HW::TI::Msp430::x16x::DigitalIO::SEL::Reg	P1SEL;
	/** 0x027 */
	unsigned char	reserved2[0x028-0x027];
	/** 0x028 */
	Oscl::HW::TI::Msp430::x16x::DigitalIO::IN::Reg	P2IN;
	/** 0x029 */
	Oscl::HW::TI::Msp430::x16x::DigitalIO::OUT::Reg	P2OUT;
	/** 0x02A */
	Oscl::HW::TI::Msp430::x16x::DigitalIO::DIR::Reg	P2DIR;
	/** 0x02B */
	Oscl::HW::TI::Msp430::x16x::DigitalIO::IFG::Reg	P2IFG;
	/** 0x02C */
	Oscl::HW::TI::Msp430::x16x::DigitalIO::IES::Reg	P2IES;
	/** 0x02D */
	Oscl::HW::TI::Msp430::x16x::DigitalIO::IE::Reg	P2IE;
	/** 0x02E */
	Oscl::HW::TI::Msp430::x16x::DigitalIO::SEL::Reg	P2SEL;

	/** 0x2F */
	unsigned char	reserved3[0x030-0x02F];

	/** 0x030 */
	Oscl::HW::TI::Msp430::x16x::DigitalIO::IN::Reg	P5IN;
	/** 0x031 */
	Oscl::HW::TI::Msp430::x16x::DigitalIO::OUT::Reg	P5OUT;
	/** 0x032 */
	Oscl::HW::TI::Msp430::x16x::DigitalIO::DIR::Reg	P5DIR;
	/** 0x033 */
	Oscl::HW::TI::Msp430::x16x::DigitalIO::SEL::Reg	P5SEL;

	/** 0x034 */
	Oscl::HW::TI::Msp430::x16x::DigitalIO::IN::Reg	P6IN;
	/** 0x035 */
	Oscl::HW::TI::Msp430::x16x::DigitalIO::OUT::Reg	P6OUT;
	/** 0x036 */
	Oscl::HW::TI::Msp430::x16x::DigitalIO::DIR::Reg	P6DIR;
	/** 0x037 */
	Oscl::HW::TI::Msp430::x16x::DigitalIO::SEL::Reg	P6SEL;

	/** 0x38-0x55 */
	unsigned char	reserved4[0x056-0x038];

	/** 0x56 - 0x58 */
	BasicClockModule								bcm;
	/** */
	uint8_t		reserved5[0x070-0x059];
	/** */
	USARTx											usart0;
	/** */
	USARTx											usart1;
	/** */
	uint8_t		reserved6[0x100-0x080];
	};

/** 0x100 - 0x1FF */
struct SixteenBitPeripherals {
	/** */
	uint8_t												reserved1[0x11E - 0x100];
	/** 0x11E */
	volatile Oscl::HW::TI::Msp430::x16x::TimerB::TBIV::Reg	TBIV;
	/** */
	uint8_t												reserved2[0x12E - 0x120];
	/** 0x012E */
	volatile Oscl::HW::TI::Msp430::x16x::TimerA::TAIV::Reg	TAIV;
	/** */
	uint8_t												reserved3[0x160 - 0x130];
	/** 0x160 */
	Timer_A													timera;
	/** 0x180 - 0x19F */
	Timer_B													timerb;
	/** */
	uint8_t												reserved4[0x200 - 0x1A0];
	};

}
}
}
}
}

#endif
