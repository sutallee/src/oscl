/*
   Copyright (C) 2007 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_hw_ti_msp430_x16x_maph_
#define _oscl_hw_ti_msp430_x16x_maph_

/** This file declares each hardware device as a
	data structure. The actual declaration is in
	the link.map file which assigns absolute addresses
	at link time.
 */

#include "imm.h"

extern Oscl::HW::TI::Msp430::x16x::SpecialFunctionRegisters	Msp430x16xSpecialFunctions;
extern Oscl::HW::TI::Msp430::x16x::EightBitPeripherals		Msp430x16xEightBitPeripherals;
extern Oscl::HW::TI::Msp430::x16x::SixteenBitPeripherals	Msp430x16xSixteenBitPeripherals;
extern uint8_t										Msp430x16xRamBase;
extern uint16_t										Msp430x16xInterruptVectorTable[16];

#endif
