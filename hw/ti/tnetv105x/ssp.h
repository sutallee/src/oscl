#ifndef _oscl_hw_ti_tnetv105x_ssph_
#define _oscl_hw_ti_tnetv105x_ssph_
#include "sspreg.h"

/** */
namespace Oscl {
/** */
namespace HW {
/** */
namespace TI {
/** */
namespace TNETV105X {
/** */
namespace SSP {

/** */
struct Port {
	/** */
	volatile CONFIG_2::Reg	config_2;	// offset 0x00
	/** */
	volatile ADDR::Reg		addr;		// offset 0x04
	/** */
	volatile DATA::Reg		data;		// offset 0x08
	/** */
	volatile CONFIG_1::Reg	config_1;	// offset 0x0C
	/** */
	volatile STATE::Reg		state;		// offset 0x10
	};

/** The SSP offset map. */
struct Map {
	/** */
	BLK_REV::Reg				blk_rev;			// 0x0000
	/** */
	volatile IO_SEL_CTRL_1::Reg	io_sel_ctrl_1;	// 0x0004
	/** */
	volatile IO_SEL_CTRL_2::Reg	io_sel_ctrl_2;	// 0x0008
	/** */
	volatile CLK_DIV::Reg		clk_div;	// 0x000C
	/** */
	volatile INT_STAT::Reg		int_stat;			// 0x0010
	/** */
	volatile INT_ENBL::Reg		int_enbl;			// 0x0014
	/** */
	volatile TST_CTRL::Reg		tst_ctrl;			// 0x0018
	/** */
	uint8_t				reserved1[0x40-0x1C];
	/** */
	Port						port_0;	// 0x0040
	/** */
	uint8_t				reserved2[0x80-0x54];
	/** */
	Port						port_1;	// 0x0080
	/** */
	uint8_t				reserved3[0x100-0x94];
	/** */
	volatile SEQ::Reg			seq_port_0[SEQ::nWords];	// 0x0100
	/** */
	volatile SEQ::Reg			seq_port_1[SEQ::nWords];	// 0x0180
	};

}
}
}
}
}

#endif
