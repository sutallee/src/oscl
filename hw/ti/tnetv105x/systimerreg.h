#ifndef _hw_ti_tnetv105x_systimerregh_
#define _hw_ti_tnetv105x_systimerregh_
#include <stdint.h>
namespace Oscl { // Namespace description
	namespace HW { // Namespace description
		namespace TI { // Namespace description
			namespace TNETV105X { // Namespace description
				namespace SYS_TIMER { // Namespace description
					namespace CTRL { // Register description
						typedef uint32_t	Reg;
						namespace ENABLE_PRESCALER { // Field Description
							enum {Lsb = 15};
							enum {FieldMask = 0x00008000};
							enum {Value_Enable = 0x1};
							enum {ValueMask_Enable = 0x00008000};
							enum {Value_Enabled = 0x1};
							enum {ValueMask_Enabled = 0x00008000};
							enum {Value_Disable = 0x0};
							enum {ValueMask_Disable = 0x00000000};
							enum {Value_Disabled = 0x0};
							enum {ValueMask_Disabled = 0x00000000};
							};
						namespace CLOCK_PRESCALER { // Field Description
							enum {Lsb = 2};
							enum {FieldMask = 0x0000003C};
							};
						namespace AUTO_LOAD { // Field Description
							enum {Lsb = 1};
							enum {FieldMask = 0x00000002};
							enum {Value_AutoLoad = 0x1};
							enum {ValueMask_AutoLoad = 0x00000002};
							enum {Value_SingleShot = 0x0};
							enum {ValueMask_SingleShot = 0x00000000};
							};
						namespace START_STOP { // Field Description
							enum {Lsb = 0};
							enum {FieldMask = 0x00000001};
							enum {Value_Start = 0x1};
							enum {ValueMask_Start = 0x00000001};
							enum {Value_Stop = 0x0};
							enum {ValueMask_Stop = 0x00000000};
							};
						};
					namespace LOAD { // Register description
						typedef uint32_t	Reg;
						namespace START_LOAD { // Field Description
							enum {Lsb = 0};
							enum {FieldMask = 0x0000FFFF};
							};
						};
					namespace VALUE { // Register description
						typedef uint32_t	Reg;
						namespace COUNT_READ { // Field Description
							enum {Lsb = 0};
							enum {FieldMask = 0x0000FFFF};
							};
						};
					namespace INT { // Register description
						typedef uint32_t	Reg;
						namespace COUNT_INT { // Field Description
							enum {Lsb = 0};
							enum {FieldMask = 0x00000001};
							enum {Value_Clear = 0x1};
							enum {ValueMask_Clear = 0x00000001};
							enum {Value_Active = 0x1};
							enum {ValueMask_Active = 0x00000001};
							enum {Value_NotActive = 0x0};
							enum {ValueMask_NotActive = 0x00000000};
							};
						};
					};
				};
			};
		};
	};
#endif
