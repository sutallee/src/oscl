/*
   Copyright (C) 2006 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_hw_ti_tnetv105x_sysgpioh_
#define _oscl_hw_ti_tnetv105x_sysgpioh_

#include "sysgpioreg.h"
/** */
namespace Oscl {
/** */
namespace HW {
/** */
namespace TI {
/** */
namespace TNETV105X {
/** */
namespace SYS_GPIO {

/**	This is the structure of the TNETV105X GPIO registers.
 */
struct Map {
	/** 0x08610900 */
	DATA_IN::Reg			data_in_0;
	/** 0x08610904 */
	DATA_IN::Reg			data_in_1;
	/** 0x08610908 */
	DATA_OUT::Reg			data_out_0;
	/** 0x0861090C */
	DATA_OUT::Reg			data_out_1;
	/** 0x08610910 */
	DIR::Reg				dir_0;
	/** 0x08610914 */
	DIR::Reg				dir_1;
	/** 0x08610918 */
	ENBL::Reg				enbl_0;
	/** 0x0861091C */
	ENBL::Reg				enbl_1;
	/** */
	unsigned char			reserved[0x924-0x920];
	/** 0x08610924 */
	CHIP_REV::Reg			chip_rev;
	/** 0x08610928 */
	CHIP_REV::Reg			die_id_lo;
	/** 0x0861092C */
	CHIP_REV::Reg			die_id_hi;
	};

}
}
}
}
}

#endif
