/*
   Copyright (C) 2006 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/


module hw_ti_tnetv105x_sspregh {
	include "oscl/compiler/types.h";
	namespace Oscl {
		namespace HW {
			namespace TI {
				namespace TNETV105X {
					namespace SSP {
						register BLK_REV uint32_t {
							field MAJOR_REVISION 8 8 { }
							field MINOR_REVISION 8 0 { }
							}
						register IO_SEL_CTRL_1 uint32_t {
							value AlwaysInput		0
							value PORT_0_DATA_OUT	1
							value PORT_0_SERIAL_CLK	2
							value PORT_0_CHIP_SEL	3
							value GPIO_OUT			4
							value PORT_1_DATA_OUT	5
							value PORT_1_SERIAL_CLK	6
							value PORT_1_CHIP_SEL	7
							field PORT_0_ALL 15 1 {
								value UseAllSequenceRamForPort0				1
								value SplitSequenceRamBetweenPort0AndPort1	0
								}
							field SSP4_SEL 12 3 {
								value AlwaysInput		0
								value PORT_0_DATA_OUT	1
								value PORT_0_SERIAL_CLK	2
								value PORT_0_CHIP_SEL	3
								value GPIO_OUT			4
								value PORT_1_DATA_OUT	5
								value PORT_1_SERIAL_CLK	6
								value PORT_1_CHIP_SEL	7
								}
							field SSP3_SEL 9 3 {
								value AlwaysInput		0
								value PORT_0_DATA_OUT	1
								value PORT_0_SERIAL_CLK	2
								value PORT_0_CHIP_SEL	3
								value GPIO_OUT			4
								value PORT_1_DATA_OUT	5
								value PORT_1_SERIAL_CLK	6
								value PORT_1_CHIP_SEL	7
								}
							field SSP2_SEL 6 3 {
								value AlwaysInput		0
								value PORT_0_DATA_OUT	1
								value PORT_0_SERIAL_CLK	2
								value PORT_0_CHIP_SEL	3
								value GPIO_OUT			4
								value PORT_1_DATA_OUT	5
								value PORT_1_SERIAL_CLK	6
								value PORT_1_CHIP_SEL	7
								}
							field SSP1_SEL 3 3 {
								value AlwaysInput		0
								value PORT_0_DATA_OUT	1
								value PORT_0_SERIAL_CLK	2
								value PORT_0_CHIP_SEL	3
								value GPIO_OUT			4
								value PORT_1_DATA_OUT	5
								value PORT_1_SERIAL_CLK	6
								value PORT_1_CHIP_SEL	7
								}
							field SSP0_SEL 0 3 {
								value AlwaysInput		0
								value PORT_0_DATA_OUT	1
								value PORT_0_SERIAL_CLK	2
								value PORT_0_CHIP_SEL	3
								value GPIO_OUT			4
								value PORT_1_DATA_OUT	5
								value PORT_1_SERIAL_CLK	6
								value PORT_1_CHIP_SEL	7
								}
							}
						register IO_SEL_CTRL_2 uint32_t {
							value HIGH	1
							value LOW	0
							field SSP4_IN 15 1 {
								value HIGH	1
								value LOW	0
								}
							field SSP3_IN 14 1 {
								value HIGH	1
								value LOW	0
								}
							field SSP2_IN 13 1 {
								value HIGH	1
								value LOW	0
								}
							field SSP1_IN 12 1 {
								value HIGH	1
								value LOW	0
								}
							field SSP0_IN 11 1 {
								value HIGH	1
								value LOW	0
								}
							field SSP4_OUT 10 1 {
								value HIGH	1
								value LOW	0
								}
							field SSP3_OUT 9 1 {
								value HIGH	1
								value LOW	0
								}
							field SSP2_OUT 8 1 {
								value HIGH	1
								value LOW	0
								}
							field SSP1_OUT 7 1 {
								value HIGH	1
								value LOW	0
								}
							field SSP0_OUT 6 1 {
								value HIGH	1
								value LOW	0
								}
							value SSP0	0
							value SSP1	1
							value SSP2	2
							value SSP3	3
							value SSP4	4
							field PORT_1_SEL 3 3 {
								value SSP0	0
								value SSP1	1
								value SSP2	2
								value SSP3	3
								value SSP4	4
								}
							field PORT_0_SEL 0 3 {
								value SSP0	0
								value SSP1	1
								value SSP2	2
								value SSP3	3
								value SSP4	4
								}
							}
						register CLK_DIV uint32_t {
							field CLK_SEL 0 8 {}
							}
						register INT_STAT uint32_t {
							value Pending		1
							value NotPending	0
							value Clear			1
							field INT_PORT_1 1 1 {
								value Pending		1
								value NotPending	0
								value Clear			1
								}
							field INT_PORT_0 0 1 {
								value Pending		1
								value NotPending	0
								value Clear			1
								}
							}
						register INT_ENBL uint32_t {
							value Enabled	1
							value Disabled	0
							value Enable	1
							value Disable	0
							field INT_ENBL_1 1 1 {
								value Enabled	1
								value Disabled	0
								value Enable	1
								value Disable	0
								}
							field INT_ENBL_0 0 1 {
								value Enabled	1
								value Disabled	0
								value Enable	1
								value Disable	0
								}
							}
						register TST_CTRL uint32_t {
							field TEST_ENBL 15 1 {
								value TestEnabled	1
								value TestDisabled	0
								value TestEnable	1
								value TestDisable	0
								}
							field TEST_SIGNALS 8 7 {}
							field OUT 3 1 {
								value Normal	0
								value Replace	1
								}
							field IN 2 1 {
								value Normal	0
								value Observe	1
								}
							field CNT_CLK_OBSRV 1 1 {
								value Normal	0
								value Observe	1
								}
							field WAIT_TRANS 0 1 {
								value WaitForTransfer	0
								value IgnoreAndZero		0
								}
							}
						register CONFIG_2 uint32_t {
							field SOFT_RESET 8 1 {
								value Assert	1
								value Negate	0
								}
							field CONFIG_TRANS 7 1 {
								value TransferOnDataWrite	1
								value TransferOnCommand		0
								}
							field CONFIG_SIZE 6 1 {
								value SixteenBits			1
								value FifteenBitsWithStatus	0
								}
							field READ_RAM 5 1 {
								value ReadSeqencerRam	1
								value Normal			0
								}
							field WRITE_RAM 4 1 {
								value WriteSeqencerRam	1
								value Normal			0
								}
							field CLK_RATE 0 4 { }
							}
						register ADDR uint32_t {
							field SERIAL_ADDR 0 16 {}
							}
						register DATA uint32_t {
							field BUSY 15 1 {
								value TransferInProgress	1
								value NotBusy				0
								}
							field SERIAL_DATA 0 16 {}
							}
						register CONFIG_1 uint32_t {
							field START_TRANS 15 1 {
								value StartTransfer	1
								value NoOperation	0
								}
							field SHIFT_DATA_LSB 13 1 {
								value LSB	1
								value MSB	0
								}
							field SHIFT_ADDR_LSB 12 1 {
								value LSB	1
								value MSB	0
								}
							field RESET_DATA_REG 11 1 {
								value ResetDataRegister	1
								value NoOperation		0
								}
							field BUSY 10 1 {
								value TransferInProgress	1
								value NotBusy				0
								}
							field DELAY_OUT 9 1 {
								value DelayByOneVBUS_CLK	1
								value NoDelay				0
								}
							field EARLY_IN 8 1 {
								value MidCycle		1
								value EndOfCycle	0
								}
							field ALT_START 7 1 {
								value StartAtSTART_ADDR		1
								value StartAtAddressZero	0
								}
							field START_ADDR 0 6 { }
							}
						register STATE uint32_t {
							field BRANCH_ADDR 8 8 {}
							field STOP_ADDR 0 6 {}
							}
						register SEQ uint32_t {
							value nWords	32
							field BRA_CNT 8 6 {}
							field OPCODE 5 3 {
								value DIRECT	0
								value TOGGLE	1
								value SHIFT		2
								value BRANCH0	4
								value BRANCH1	5
								value BRANCH	6
								value STOP		7
								}
							field SHIFT_REG 4 1 {
								value DataRegister		1
								value AddressRegister	0
								}
							field IO_MODE 3 1 {
								value OutputMode	1
								value InputMode		0
								}
							field CHIP_SEL 2 1 {
								value HIGH	1
								value LOW	0
								}
							field DATA_OUT 1 1 {
								value HIGH	1
								value LOW	0
								}
							field SERIAL_CLK 0 1 {
								value HIGH	1
								value LOW	0
								}
							}
						}
					}
				}
			}
		}
	}

