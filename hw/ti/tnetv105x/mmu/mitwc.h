/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _hw_mot8xx_mitwch_
#define _hw_mot8xx_mitwch_
#include <stdint.h>
namespace Oscl { // Namespace description
	namespace Mot8xx { // Namespace description
		namespace Mmu { // Namespace description
			namespace MiTwc { // Register description
				typedef uint32_t	Reg;
				namespace APG { // Field Description
					enum {Lsb = 5};
					enum {FieldMask = 0x000001E0};
					enum {Value_Minimum = 0x0};
					enum {ValueMask_Minimum = 0x00000000};
					enum {Value_Maximum = 0xF};
					enum {ValueMask_Maximum = 0x000001E0};
					};
				namespace G { // Field Description
					enum {Lsb = 4};
					enum {FieldMask = 0x00000010};
					enum {Value_Nonguarded = 0x0};
					enum {ValueMask_Nonguarded = 0x00000000};
					enum {Value_Guarded = 0x1};
					enum {ValueMask_Guarded = 0x00000010};
					};
				namespace PS { // Field Description
					enum {Lsb = 2};
					enum {FieldMask = 0x0000000C};
					enum {Value_SizeSmall = 0x0};
					enum {ValueMask_SizeSmall = 0x00000000};
					enum {Value_Size512K = 0x1};
					enum {ValueMask_Size512K = 0x00000004};
					enum {Value_Size8M = 0x3};
					enum {ValueMask_Size8M = 0x0000000C};
					};
				namespace V { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x00000001};
					enum {Value_NotValid = 0x0};
					enum {ValueMask_NotValid = 0x00000000};
					enum {Value_Valid = 0x1};
					enum {ValueMask_Valid = 0x00000001};
					};
				};
			};
		};
	};
#endif
