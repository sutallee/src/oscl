/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _hw_mot8xx_mirpnh_
#define _hw_mot8xx_mirpnh_
#include <stdint.h>
namespace Oscl { // Namespace description
	namespace Mot8xx { // Namespace description
		namespace Mmu { // Namespace description
			namespace Write { // Namespace description
				namespace MiRpn { // Register description
					typedef uint32_t	Reg;
					namespace RPN { // Field Description
						enum {Lsb = 12};
						enum {FieldMask = 0xFFFFF000};
						};
					namespace Ppm0 { // Field Description
						enum {Lsb = 3};
						enum {FieldMask = 0x00000FF8};
						namespace Encoding { // Field Description
							enum {Lsb = 9};
							enum {FieldMask = 0x00000200};
							enum {Value_Ppc = 0x0};
							enum {ValueMask_Ppc = 0x00000000};
							enum {Value_Extended = 0x1};
							enum {ValueMask_Extended = 0x00000200};
							};
						namespace ExtEnc { // Field Description
							enum {Lsb = 9};
							enum {FieldMask = 0x00000E00};
							namespace PpInst { // Field Description
								enum {Lsb = 9};
								enum {FieldMask = 0x00000E00};
								enum {Value_NoAccess = 0x1};
								enum {ValueMask_NoAccess = 0x00000200};
								enum {Value_SuperExecOnly011 = 0x3};
								enum {ValueMask_SuperExecOnly011 = 0x00000600};
								enum {Value_SuperExecOnly = 0x0};
								enum {ValueMask_SuperExecOnly = 0x00000000};
								enum {Value_AllExec = 0x2};
								enum {ValueMask_AllExec = 0x00000400};
								enum {Value_AllExec100 = 0x4};
								enum {ValueMask_AllExec100 = 0x00000800};
								enum {Value_AllExec110 = 0x6};
								enum {ValueMask_AllExec110 = 0x00000C00};
								};
							};
						namespace PpcEnc { // Field Description
							enum {Lsb = 9};
							enum {FieldMask = 0x00000E00};
							namespace PpInst { // Field Description
								enum {Lsb = 10};
								enum {FieldMask = 0x00000C00};
								enum {Value_SupervisorExecutable = 0x0};
								enum {ValueMask_SupervisorExecutable = 0x00000000};
								enum {Value_AllExecutable = 0x1};
								enum {ValueMask_AllExecutable = 0x00000400};
								enum {Value_AllExecutable10 = 0x2};
								enum {ValueMask_AllExecutable10 = 0x00000800};
								enum {Value_AllExecutable11 = 0x3};
								enum {ValueMask_AllExecutable11 = 0x00000C00};
								};
							};
						namespace Change { // Field Description
							enum {Lsb = 8};
							enum {FieldMask = 0x00000100};
							enum {Value_Unchanged = 0x0};
							enum {ValueMask_Unchanged = 0x00000000};
							enum {Value_Changed = 0x1};
							enum {ValueMask_Changed = 0x00000100};
							};
						namespace Ppcs0 { // Field Description
							enum {Lsb = 4};
							enum {FieldMask = 0x000000F0};
							namespace Mode3 { // Field Description
								enum {Lsb = 4};
								enum {FieldMask = 0x000000F0};
								enum {Value_SubPage1 = 0x1};
								enum {ValueMask_SubPage1 = 0x00000010};
								enum {Value_SubPage2 = 0x2};
								enum {ValueMask_SubPage2 = 0x00000020};
								enum {Value_SubPage3 = 0x3};
								enum {ValueMask_SubPage3 = 0x00000030};
								enum {Value_SubPage4 = 0x4};
								enum {ValueMask_SubPage4 = 0x00000040};
								};
							namespace Other { // Field Description
								enum {Lsb = 4};
								enum {FieldMask = 0x000000F0};
								enum {Value_Fixed = 0xF};
								enum {ValueMask_Fixed = 0x000000F0};
								};
							};
						namespace Ppcs1 { // Field Description
							enum {Lsb = 4};
							enum {FieldMask = 0x000000F0};
							namespace Compare { // Field Description
								enum {Lsb = 4};
								enum {FieldMask = 0x000000F0};
								enum {Value_HitSuperOnly = 0x8};
								enum {ValueMask_HitSuperOnly = 0x00000080};
								enum {Value_HitUserOnly = 0x4};
								enum {ValueMask_HitUserOnly = 0x00000040};
								enum {Value_HitBoth = 0x6};
								enum {ValueMask_HitBoth = 0x00000060};
								};
							};
						namespace SPS { // Field Description
							enum {Lsb = 3};
							enum {FieldMask = 0x00000008};
							enum {Value_Size4Kbyte = 0x0};
							enum {ValueMask_Size4Kbyte = 0x00000000};
							enum {Value_Size16Kbyte = 0x1};
							enum {ValueMask_Size16Kbyte = 0x00000008};
							};
						};
					namespace Ppm1 { // Field Description
						enum {Lsb = 3};
						enum {FieldMask = 0x00000FF8};
						namespace Pp1Inst { // Field Description
							enum {Lsb = 10};
							enum {FieldMask = 0x00000C00};
							enum {Value_NoAccess = 0x0};
							enum {ValueMask_NoAccess = 0x00000000};
							enum {Value_SupervisorExecutable = 0x1};
							enum {ValueMask_SupervisorExecutable = 0x00000400};
							enum {Value_AllExecutable = 0x2};
							enum {ValueMask_AllExecutable = 0x00000800};
							};
						namespace Pp2Inst { // Field Description
							enum {Lsb = 8};
							enum {FieldMask = 0x00000300};
							enum {Value_NoAccess = 0x0};
							enum {ValueMask_NoAccess = 0x00000000};
							enum {Value_SupervisorExecutable = 0x1};
							enum {ValueMask_SupervisorExecutable = 0x00000100};
							enum {Value_AllExecutable = 0x2};
							enum {ValueMask_AllExecutable = 0x00000200};
							};
						namespace Pp3Inst { // Field Description
							enum {Lsb = 6};
							enum {FieldMask = 0x000000C0};
							enum {Value_NoAccess = 0x0};
							enum {ValueMask_NoAccess = 0x00000000};
							enum {Value_SupervisorExecutable = 0x1};
							enum {ValueMask_SupervisorExecutable = 0x00000040};
							enum {Value_AllExecutable = 0x2};
							enum {ValueMask_AllExecutable = 0x00000080};
							};
						namespace Pp4Inst { // Field Description
							enum {Lsb = 4};
							enum {FieldMask = 0x00000030};
							enum {Value_NoAccess = 0x0};
							enum {ValueMask_NoAccess = 0x00000000};
							enum {Value_SupervisorExecutable = 0x1};
							enum {ValueMask_SupervisorExecutable = 0x00000010};
							enum {Value_AllExecutable = 0x2};
							enum {ValueMask_AllExecutable = 0x00000020};
							};
						namespace SPS { // Field Description
							enum {Lsb = 3};
							enum {FieldMask = 0x00000008};
							enum {Value_Fixed = 0x0};
							enum {ValueMask_Fixed = 0x00000000};
							};
						};
					namespace SH { // Field Description
						enum {Lsb = 2};
						enum {FieldMask = 0x00000004};
						enum {Value_MatchASID = 0x0};
						enum {ValueMask_MatchASID = 0x00000000};
						enum {Value_IgnoreASID = 0x1};
						enum {ValueMask_IgnoreASID = 0x00000004};
						};
					namespace CI { // Field Description
						enum {Lsb = 1};
						enum {FieldMask = 0x00000002};
						enum {Value_CacheEnabled = 0x0};
						enum {ValueMask_CacheEnabled = 0x00000000};
						enum {Value_CacheInhibited = 0x1};
						enum {ValueMask_CacheInhibited = 0x00000002};
						};
					namespace V { // Field Description
						enum {Lsb = 0};
						enum {FieldMask = 0x00000001};
						enum {Value_Invalid = 0x0};
						enum {ValueMask_Invalid = 0x00000000};
						enum {Value_Valid = 0x1};
						enum {ValueMask_Valid = 0x00000001};
						};
					};
				};
			namespace Read { // Namespace description
				namespace MiRpn { // Register description
					typedef uint32_t	Reg;
					};
				};
			};
		};
	};
#endif
