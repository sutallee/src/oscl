/*
   Copyright (C) 2006 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_hw_ti_tnetv105x_maph_
#define _oscl_hw_ti_tnetv105x_maph_
#include "oscl/hw/ti/tnetv105x/sysint.h"
#include "oscl/hw/ti/tnetv105x/systimer.h"
#include "oscl/hw/ti/tnetv105x/uart.h"
#include "oscl/hw/ti/tnetv105x/ssp.h"
#include "oscl/hw/ti/tnetv105x/sysgpio.h"

/** */
namespace Oscl {
/** */
namespace HW {
/** */
namespace TI {
/** */
namespace TNETV105X {

/** 0xB8610000 */
struct Map {
	/** */
	uint32_t						reserved0[((0x0900-0x0000))/sizeof(uint32_t)];
	/** */
	Oscl::HW::TI::TNETV105X::SYS_GPIO::Map	sys_gpio;	// offset: 0x0900 - 0x92F
	/** */
	uint32_t						reserved1[((0x0C00-0x0930))/sizeof(uint32_t)];
	/** */
	Oscl::HW::TI::TNETV105X::SYS_TIMER::Map	sys_timer_0;	// offset: 0x0C00
	/** */
	uint32_t						reserved2[(0x0D00-0x0C10)/sizeof(uint32_t)];
	/** */
	Oscl::HW::TI::TNETV105X::SYS_TIMER::Map	sys_timer_1;	// offset: 0x0D00
	/** */
	uint32_t						reserved3[((0x0E00-0x0D10))/sizeof(uint32_t)];
	/** */
	Oscl::HW::TI::TNETV105X::UART::Map		uart;			// offset: 0x0E00
	/** */
	uint32_t						reserved4[((0x1000-0x0E20))/sizeof(uint32_t)];
	/** */
	Oscl::HW::TI::TNETV105X::SSP::Map		ssp;			// offset: 0x1000
	/** */
	uint32_t						reserved5[((0x2400-0x1200))/sizeof(uint32_t)];
	/** SYS_INT 0x2400 */
	Oscl::HW::TI::TNETV105X::SYS_INT::Map	sys_int;		// offset: 0x2400
	};

}
}
}
}

#endif
