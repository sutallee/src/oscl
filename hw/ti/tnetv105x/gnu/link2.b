#
# The project overrides.b should define:
#
#FINAL_LINK_FLAGS=   \
#	-T $(srcroot)/src/oscl/hw/est/mdp8xx/mem8Mram8MflashDebug.map \
#	$(BSP_LINK_FLAGS)
#
# The first "-T" argument defines the size of RAM and Flash, and
# may vary with the implementation.
#

BSP_LINK_FLAGS=	\
	-T $(srcroot)/src/oscl/hw/ti/tnetv105x/gnu/link.map \
	-T $(srcroot)/tools/benvc/Linux/mips-elf/mips-elf/lib/ldscripts/elf32ebmip.x

