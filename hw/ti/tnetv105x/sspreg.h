#ifndef _hw_ti_tnetv105x_sspregh_
#define _hw_ti_tnetv105x_sspregh_
#include <stdint.h>
namespace Oscl { // Namespace description
	namespace HW { // Namespace description
		namespace TI { // Namespace description
			namespace TNETV105X { // Namespace description
				namespace SSP { // Namespace description
					namespace BLK_REV { // Register description
						typedef uint32_t	Reg;
						namespace MAJOR_REVISION { // Field Description
							enum {Lsb = 8};
							enum {FieldMask = 0x0000FF00};
							};
						namespace MINOR_REVISION { // Field Description
							enum {Lsb = 8};
							enum {FieldMask = 0x00000000};
							};
						};
					namespace IO_SEL_CTRL_1 { // Register description
						typedef uint32_t	Reg;
						enum {AlwaysInput = 0};
						enum {PORT_0_DATA_OUT = 1};
						enum {PORT_0_SERIAL_CLK = 2};
						enum {PORT_0_CHIP_SEL = 3};
						enum {GPIO_OUT = 4};
						enum {PORT_1_DATA_OUT = 5};
						enum {PORT_1_SERIAL_CLK = 6};
						enum {PORT_1_CHIP_SEL = 7};
						namespace PORT_0_ALL { // Field Description
							enum {Lsb = 15};
							enum {FieldMask = 0x00008000};
							enum {Value_UseAllSequenceRamForPort0 = 0x1};
							enum {ValueMask_UseAllSequenceRamForPort0 = 0x00008000};
							enum {Value_SplitSequenceRamBetweenPort0AndPort1 = 0x0};
							enum {ValueMask_SplitSequenceRamBetweenPort0AndPort1 = 0x00000000};
							};
						namespace SSP4_SEL { // Field Description
							enum {Lsb = 12};
							enum {FieldMask = 0x00007000};
							enum {Value_AlwaysInput = 0x0};
							enum {ValueMask_AlwaysInput = 0x00000000};
							enum {Value_PORT_0_DATA_OUT = 0x1};
							enum {ValueMask_PORT_0_DATA_OUT = 0x00001000};
							enum {Value_PORT_0_SERIAL_CLK = 0x2};
							enum {ValueMask_PORT_0_SERIAL_CLK = 0x00002000};
							enum {Value_PORT_0_CHIP_SEL = 0x3};
							enum {ValueMask_PORT_0_CHIP_SEL = 0x00003000};
							enum {Value_GPIO_OUT = 0x4};
							enum {ValueMask_GPIO_OUT = 0x00004000};
							enum {Value_PORT_1_DATA_OUT = 0x5};
							enum {ValueMask_PORT_1_DATA_OUT = 0x00005000};
							enum {Value_PORT_1_SERIAL_CLK = 0x6};
							enum {ValueMask_PORT_1_SERIAL_CLK = 0x00006000};
							enum {Value_PORT_1_CHIP_SEL = 0x7};
							enum {ValueMask_PORT_1_CHIP_SEL = 0x00007000};
							};
						namespace SSP3_SEL { // Field Description
							enum {Lsb = 9};
							enum {FieldMask = 0x00000E00};
							enum {Value_AlwaysInput = 0x0};
							enum {ValueMask_AlwaysInput = 0x00000000};
							enum {Value_PORT_0_DATA_OUT = 0x1};
							enum {ValueMask_PORT_0_DATA_OUT = 0x00000200};
							enum {Value_PORT_0_SERIAL_CLK = 0x2};
							enum {ValueMask_PORT_0_SERIAL_CLK = 0x00000400};
							enum {Value_PORT_0_CHIP_SEL = 0x3};
							enum {ValueMask_PORT_0_CHIP_SEL = 0x00000600};
							enum {Value_GPIO_OUT = 0x4};
							enum {ValueMask_GPIO_OUT = 0x00000800};
							enum {Value_PORT_1_DATA_OUT = 0x5};
							enum {ValueMask_PORT_1_DATA_OUT = 0x00000A00};
							enum {Value_PORT_1_SERIAL_CLK = 0x6};
							enum {ValueMask_PORT_1_SERIAL_CLK = 0x00000C00};
							enum {Value_PORT_1_CHIP_SEL = 0x7};
							enum {ValueMask_PORT_1_CHIP_SEL = 0x00000E00};
							};
						namespace SSP2_SEL { // Field Description
							enum {Lsb = 6};
							enum {FieldMask = 0x000001C0};
							enum {Value_AlwaysInput = 0x0};
							enum {ValueMask_AlwaysInput = 0x00000000};
							enum {Value_PORT_0_DATA_OUT = 0x1};
							enum {ValueMask_PORT_0_DATA_OUT = 0x00000040};
							enum {Value_PORT_0_SERIAL_CLK = 0x2};
							enum {ValueMask_PORT_0_SERIAL_CLK = 0x00000080};
							enum {Value_PORT_0_CHIP_SEL = 0x3};
							enum {ValueMask_PORT_0_CHIP_SEL = 0x000000C0};
							enum {Value_GPIO_OUT = 0x4};
							enum {ValueMask_GPIO_OUT = 0x00000100};
							enum {Value_PORT_1_DATA_OUT = 0x5};
							enum {ValueMask_PORT_1_DATA_OUT = 0x00000140};
							enum {Value_PORT_1_SERIAL_CLK = 0x6};
							enum {ValueMask_PORT_1_SERIAL_CLK = 0x00000180};
							enum {Value_PORT_1_CHIP_SEL = 0x7};
							enum {ValueMask_PORT_1_CHIP_SEL = 0x000001C0};
							};
						namespace SSP1_SEL { // Field Description
							enum {Lsb = 3};
							enum {FieldMask = 0x00000038};
							enum {Value_AlwaysInput = 0x0};
							enum {ValueMask_AlwaysInput = 0x00000000};
							enum {Value_PORT_0_DATA_OUT = 0x1};
							enum {ValueMask_PORT_0_DATA_OUT = 0x00000008};
							enum {Value_PORT_0_SERIAL_CLK = 0x2};
							enum {ValueMask_PORT_0_SERIAL_CLK = 0x00000010};
							enum {Value_PORT_0_CHIP_SEL = 0x3};
							enum {ValueMask_PORT_0_CHIP_SEL = 0x00000018};
							enum {Value_GPIO_OUT = 0x4};
							enum {ValueMask_GPIO_OUT = 0x00000020};
							enum {Value_PORT_1_DATA_OUT = 0x5};
							enum {ValueMask_PORT_1_DATA_OUT = 0x00000028};
							enum {Value_PORT_1_SERIAL_CLK = 0x6};
							enum {ValueMask_PORT_1_SERIAL_CLK = 0x00000030};
							enum {Value_PORT_1_CHIP_SEL = 0x7};
							enum {ValueMask_PORT_1_CHIP_SEL = 0x00000038};
							};
						namespace SSP0_SEL { // Field Description
							enum {Lsb = 0};
							enum {FieldMask = 0x00000007};
							enum {Value_AlwaysInput = 0x0};
							enum {ValueMask_AlwaysInput = 0x00000000};
							enum {Value_PORT_0_DATA_OUT = 0x1};
							enum {ValueMask_PORT_0_DATA_OUT = 0x00000001};
							enum {Value_PORT_0_SERIAL_CLK = 0x2};
							enum {ValueMask_PORT_0_SERIAL_CLK = 0x00000002};
							enum {Value_PORT_0_CHIP_SEL = 0x3};
							enum {ValueMask_PORT_0_CHIP_SEL = 0x00000003};
							enum {Value_GPIO_OUT = 0x4};
							enum {ValueMask_GPIO_OUT = 0x00000004};
							enum {Value_PORT_1_DATA_OUT = 0x5};
							enum {ValueMask_PORT_1_DATA_OUT = 0x00000005};
							enum {Value_PORT_1_SERIAL_CLK = 0x6};
							enum {ValueMask_PORT_1_SERIAL_CLK = 0x00000006};
							enum {Value_PORT_1_CHIP_SEL = 0x7};
							enum {ValueMask_PORT_1_CHIP_SEL = 0x00000007};
							};
						};
					namespace IO_SEL_CTRL_2 { // Register description
						typedef uint32_t	Reg;
						enum {HIGH = 1};
						enum {LOW = 0};
						namespace SSP4_IN { // Field Description
							enum {Lsb = 15};
							enum {FieldMask = 0x00008000};
							enum {Value_HIGH = 0x1};
							enum {ValueMask_HIGH = 0x00008000};
							enum {Value_LOW = 0x0};
							enum {ValueMask_LOW = 0x00000000};
							};
						namespace SSP3_IN { // Field Description
							enum {Lsb = 14};
							enum {FieldMask = 0x00004000};
							enum {Value_HIGH = 0x1};
							enum {ValueMask_HIGH = 0x00004000};
							enum {Value_LOW = 0x0};
							enum {ValueMask_LOW = 0x00000000};
							};
						namespace SSP2_IN { // Field Description
							enum {Lsb = 13};
							enum {FieldMask = 0x00002000};
							enum {Value_HIGH = 0x1};
							enum {ValueMask_HIGH = 0x00002000};
							enum {Value_LOW = 0x0};
							enum {ValueMask_LOW = 0x00000000};
							};
						namespace SSP1_IN { // Field Description
							enum {Lsb = 12};
							enum {FieldMask = 0x00001000};
							enum {Value_HIGH = 0x1};
							enum {ValueMask_HIGH = 0x00001000};
							enum {Value_LOW = 0x0};
							enum {ValueMask_LOW = 0x00000000};
							};
						namespace SSP0_IN { // Field Description
							enum {Lsb = 11};
							enum {FieldMask = 0x00000800};
							enum {Value_HIGH = 0x1};
							enum {ValueMask_HIGH = 0x00000800};
							enum {Value_LOW = 0x0};
							enum {ValueMask_LOW = 0x00000000};
							};
						namespace SSP4_OUT { // Field Description
							enum {Lsb = 10};
							enum {FieldMask = 0x00000400};
							enum {Value_HIGH = 0x1};
							enum {ValueMask_HIGH = 0x00000400};
							enum {Value_LOW = 0x0};
							enum {ValueMask_LOW = 0x00000000};
							};
						namespace SSP3_OUT { // Field Description
							enum {Lsb = 9};
							enum {FieldMask = 0x00000200};
							enum {Value_HIGH = 0x1};
							enum {ValueMask_HIGH = 0x00000200};
							enum {Value_LOW = 0x0};
							enum {ValueMask_LOW = 0x00000000};
							};
						namespace SSP2_OUT { // Field Description
							enum {Lsb = 8};
							enum {FieldMask = 0x00000100};
							enum {Value_HIGH = 0x1};
							enum {ValueMask_HIGH = 0x00000100};
							enum {Value_LOW = 0x0};
							enum {ValueMask_LOW = 0x00000000};
							};
						namespace SSP1_OUT { // Field Description
							enum {Lsb = 7};
							enum {FieldMask = 0x00000080};
							enum {Value_HIGH = 0x1};
							enum {ValueMask_HIGH = 0x00000080};
							enum {Value_LOW = 0x0};
							enum {ValueMask_LOW = 0x00000000};
							};
						namespace SSP0_OUT { // Field Description
							enum {Lsb = 6};
							enum {FieldMask = 0x00000040};
							enum {Value_HIGH = 0x1};
							enum {ValueMask_HIGH = 0x00000040};
							enum {Value_LOW = 0x0};
							enum {ValueMask_LOW = 0x00000000};
							};
						enum {SSP0 = 0};
						enum {SSP1 = 1};
						enum {SSP2 = 2};
						enum {SSP3 = 3};
						enum {SSP4 = 4};
						namespace PORT_1_SEL { // Field Description
							enum {Lsb = 3};
							enum {FieldMask = 0x00000038};
							enum {Value_SSP0 = 0x0};
							enum {ValueMask_SSP0 = 0x00000000};
							enum {Value_SSP1 = 0x1};
							enum {ValueMask_SSP1 = 0x00000008};
							enum {Value_SSP2 = 0x2};
							enum {ValueMask_SSP2 = 0x00000010};
							enum {Value_SSP3 = 0x3};
							enum {ValueMask_SSP3 = 0x00000018};
							enum {Value_SSP4 = 0x4};
							enum {ValueMask_SSP4 = 0x00000020};
							};
						namespace PORT_0_SEL { // Field Description
							enum {Lsb = 0};
							enum {FieldMask = 0x00000007};
							enum {Value_SSP0 = 0x0};
							enum {ValueMask_SSP0 = 0x00000000};
							enum {Value_SSP1 = 0x1};
							enum {ValueMask_SSP1 = 0x00000001};
							enum {Value_SSP2 = 0x2};
							enum {ValueMask_SSP2 = 0x00000002};
							enum {Value_SSP3 = 0x3};
							enum {ValueMask_SSP3 = 0x00000003};
							enum {Value_SSP4 = 0x4};
							enum {ValueMask_SSP4 = 0x00000004};
							};
						};
					namespace CLK_DIV { // Register description
						typedef uint32_t	Reg;
						namespace CLK_SEL { // Field Description
							enum {Lsb = 0};
							enum {FieldMask = 0x000000FF};
							};
						};
					namespace INT_STAT { // Register description
						typedef uint32_t	Reg;
						enum {Pending = 1};
						enum {NotPending = 0};
						enum {Clear = 1};
						namespace INT_PORT_1 { // Field Description
							enum {Lsb = 1};
							enum {FieldMask = 0x00000002};
							enum {Value_Pending = 0x1};
							enum {ValueMask_Pending = 0x00000002};
							enum {Value_NotPending = 0x0};
							enum {ValueMask_NotPending = 0x00000000};
							enum {Value_Clear = 0x1};
							enum {ValueMask_Clear = 0x00000002};
							};
						namespace INT_PORT_0 { // Field Description
							enum {Lsb = 0};
							enum {FieldMask = 0x00000001};
							enum {Value_Pending = 0x1};
							enum {ValueMask_Pending = 0x00000001};
							enum {Value_NotPending = 0x0};
							enum {ValueMask_NotPending = 0x00000000};
							enum {Value_Clear = 0x1};
							enum {ValueMask_Clear = 0x00000001};
							};
						};
					namespace INT_ENBL { // Register description
						typedef uint32_t	Reg;
						enum {Enabled = 1};
						enum {Disabled = 0};
						enum {Enable = 1};
						enum {Disable = 0};
						namespace INT_ENBL_1 { // Field Description
							enum {Lsb = 1};
							enum {FieldMask = 0x00000002};
							enum {Value_Enabled = 0x1};
							enum {ValueMask_Enabled = 0x00000002};
							enum {Value_Disabled = 0x0};
							enum {ValueMask_Disabled = 0x00000000};
							enum {Value_Enable = 0x1};
							enum {ValueMask_Enable = 0x00000002};
							enum {Value_Disable = 0x0};
							enum {ValueMask_Disable = 0x00000000};
							};
						namespace INT_ENBL_0 { // Field Description
							enum {Lsb = 0};
							enum {FieldMask = 0x00000001};
							enum {Value_Enabled = 0x1};
							enum {ValueMask_Enabled = 0x00000001};
							enum {Value_Disabled = 0x0};
							enum {ValueMask_Disabled = 0x00000000};
							enum {Value_Enable = 0x1};
							enum {ValueMask_Enable = 0x00000001};
							enum {Value_Disable = 0x0};
							enum {ValueMask_Disable = 0x00000000};
							};
						};
					namespace TST_CTRL { // Register description
						typedef uint32_t	Reg;
						namespace TEST_ENBL { // Field Description
							enum {Lsb = 15};
							enum {FieldMask = 0x00008000};
							enum {Value_TestEnabled = 0x1};
							enum {ValueMask_TestEnabled = 0x00008000};
							enum {Value_TestDisabled = 0x0};
							enum {ValueMask_TestDisabled = 0x00000000};
							enum {Value_TestEnable = 0x1};
							enum {ValueMask_TestEnable = 0x00008000};
							enum {Value_TestDisable = 0x0};
							enum {ValueMask_TestDisable = 0x00000000};
							};
						namespace TEST_SIGNALS { // Field Description
							enum {Lsb = 8};
							enum {FieldMask = 0x00007F00};
							};
						namespace OUT { // Field Description
							enum {Lsb = 3};
							enum {FieldMask = 0x00000008};
							enum {Value_Normal = 0x0};
							enum {ValueMask_Normal = 0x00000000};
							enum {Value_Replace = 0x1};
							enum {ValueMask_Replace = 0x00000008};
							};
						namespace IN { // Field Description
							enum {Lsb = 2};
							enum {FieldMask = 0x00000004};
							enum {Value_Normal = 0x0};
							enum {ValueMask_Normal = 0x00000000};
							enum {Value_Observe = 0x1};
							enum {ValueMask_Observe = 0x00000004};
							};
						namespace CNT_CLK_OBSRV { // Field Description
							enum {Lsb = 1};
							enum {FieldMask = 0x00000002};
							enum {Value_Normal = 0x0};
							enum {ValueMask_Normal = 0x00000000};
							enum {Value_Observe = 0x1};
							enum {ValueMask_Observe = 0x00000002};
							};
						namespace WAIT_TRANS { // Field Description
							enum {Lsb = 0};
							enum {FieldMask = 0x00000001};
							enum {Value_WaitForTransfer = 0x0};
							enum {ValueMask_WaitForTransfer = 0x00000000};
							enum {Value_IgnoreAndZero = 0x0};
							enum {ValueMask_IgnoreAndZero = 0x00000000};
							};
						};
					namespace CONFIG_2 { // Register description
						typedef uint32_t	Reg;
						namespace SOFT_RESET { // Field Description
							enum {Lsb = 8};
							enum {FieldMask = 0x00000100};
							enum {Value_Assert = 0x1};
							enum {ValueMask_Assert = 0x00000100};
							enum {Value_Negate = 0x0};
							enum {ValueMask_Negate = 0x00000000};
							};
						namespace CONFIG_TRANS { // Field Description
							enum {Lsb = 7};
							enum {FieldMask = 0x00000080};
							enum {Value_TransferOnDataWrite = 0x1};
							enum {ValueMask_TransferOnDataWrite = 0x00000080};
							enum {Value_TransferOnCommand = 0x0};
							enum {ValueMask_TransferOnCommand = 0x00000000};
							};
						namespace CONFIG_SIZE { // Field Description
							enum {Lsb = 6};
							enum {FieldMask = 0x00000040};
							enum {Value_SixteenBits = 0x1};
							enum {ValueMask_SixteenBits = 0x00000040};
							enum {Value_FifteenBitsWithStatus = 0x0};
							enum {ValueMask_FifteenBitsWithStatus = 0x00000000};
							};
						namespace READ_RAM { // Field Description
							enum {Lsb = 5};
							enum {FieldMask = 0x00000020};
							enum {Value_ReadSeqencerRam = 0x1};
							enum {ValueMask_ReadSeqencerRam = 0x00000020};
							enum {Value_Normal = 0x0};
							enum {ValueMask_Normal = 0x00000000};
							};
						namespace WRITE_RAM { // Field Description
							enum {Lsb = 4};
							enum {FieldMask = 0x00000010};
							enum {Value_WriteSeqencerRam = 0x1};
							enum {ValueMask_WriteSeqencerRam = 0x00000010};
							enum {Value_Normal = 0x0};
							enum {ValueMask_Normal = 0x00000000};
							};
						namespace CLK_RATE { // Field Description
							enum {Lsb = 0};
							enum {FieldMask = 0x0000000F};
							};
						};
					namespace ADDR { // Register description
						typedef uint32_t	Reg;
						namespace SERIAL_ADDR { // Field Description
							enum {Lsb = 0};
							enum {FieldMask = 0x0000FFFF};
							};
						};
					namespace DATA { // Register description
						typedef uint32_t	Reg;
						namespace BUSY { // Field Description
							enum {Lsb = 15};
							enum {FieldMask = 0x00008000};
							enum {Value_TransferInProgress = 0x1};
							enum {ValueMask_TransferInProgress = 0x00008000};
							enum {Value_NotBusy = 0x0};
							enum {ValueMask_NotBusy = 0x00000000};
							};
						namespace SERIAL_DATA { // Field Description
							enum {Lsb = 0};
							enum {FieldMask = 0x0000FFFF};
							};
						};
					namespace CONFIG_1 { // Register description
						typedef uint32_t	Reg;
						namespace START_TRANS { // Field Description
							enum {Lsb = 15};
							enum {FieldMask = 0x00008000};
							enum {Value_StartTransfer = 0x1};
							enum {ValueMask_StartTransfer = 0x00008000};
							enum {Value_NoOperation = 0x0};
							enum {ValueMask_NoOperation = 0x00000000};
							};
						namespace SHIFT_DATA_LSB { // Field Description
							enum {Lsb = 13};
							enum {FieldMask = 0x00002000};
							enum {Value_LSB = 0x1};
							enum {ValueMask_LSB = 0x00002000};
							enum {Value_MSB = 0x0};
							enum {ValueMask_MSB = 0x00000000};
							};
						namespace SHIFT_ADDR_LSB { // Field Description
							enum {Lsb = 12};
							enum {FieldMask = 0x00001000};
							enum {Value_LSB = 0x1};
							enum {ValueMask_LSB = 0x00001000};
							enum {Value_MSB = 0x0};
							enum {ValueMask_MSB = 0x00000000};
							};
						namespace RESET_DATA_REG { // Field Description
							enum {Lsb = 11};
							enum {FieldMask = 0x00000800};
							enum {Value_ResetDataRegister = 0x1};
							enum {ValueMask_ResetDataRegister = 0x00000800};
							enum {Value_NoOperation = 0x0};
							enum {ValueMask_NoOperation = 0x00000000};
							};
						namespace BUSY { // Field Description
							enum {Lsb = 10};
							enum {FieldMask = 0x00000400};
							enum {Value_TransferInProgress = 0x1};
							enum {ValueMask_TransferInProgress = 0x00000400};
							enum {Value_NotBusy = 0x0};
							enum {ValueMask_NotBusy = 0x00000000};
							};
						namespace DELAY_OUT { // Field Description
							enum {Lsb = 9};
							enum {FieldMask = 0x00000200};
							enum {Value_DelayByOneVBUS_CLK = 0x1};
							enum {ValueMask_DelayByOneVBUS_CLK = 0x00000200};
							enum {Value_NoDelay = 0x0};
							enum {ValueMask_NoDelay = 0x00000000};
							};
						namespace EARLY_IN { // Field Description
							enum {Lsb = 8};
							enum {FieldMask = 0x00000100};
							enum {Value_MidCycle = 0x1};
							enum {ValueMask_MidCycle = 0x00000100};
							enum {Value_EndOfCycle = 0x0};
							enum {ValueMask_EndOfCycle = 0x00000000};
							};
						namespace ALT_START { // Field Description
							enum {Lsb = 7};
							enum {FieldMask = 0x00000080};
							enum {Value_StartAtSTART_ADDR = 0x1};
							enum {ValueMask_StartAtSTART_ADDR = 0x00000080};
							enum {Value_StartAtAddressZero = 0x0};
							enum {ValueMask_StartAtAddressZero = 0x00000000};
							};
						namespace START_ADDR { // Field Description
							enum {Lsb = 0};
							enum {FieldMask = 0x0000003F};
							};
						};
					namespace STATE { // Register description
						typedef uint32_t	Reg;
						namespace BRANCH_ADDR { // Field Description
							enum {Lsb = 8};
							enum {FieldMask = 0x0000FF00};
							};
						namespace STOP_ADDR { // Field Description
							enum {Lsb = 0};
							enum {FieldMask = 0x0000003F};
							};
						};
					namespace SEQ { // Register description
						typedef uint32_t	Reg;
						enum {nWords = 32};
						namespace BRA_CNT { // Field Description
							enum {Lsb = 8};
							enum {FieldMask = 0x00003F00};
							};
						namespace OPCODE { // Field Description
							enum {Lsb = 5};
							enum {FieldMask = 0x000000E0};
							enum {Value_DIRECT = 0x0};
							enum {ValueMask_DIRECT = 0x00000000};
							enum {Value_TOGGLE = 0x1};
							enum {ValueMask_TOGGLE = 0x00000020};
							enum {Value_SHIFT = 0x2};
							enum {ValueMask_SHIFT = 0x00000040};
							enum {Value_BRANCH0 = 0x4};
							enum {ValueMask_BRANCH0 = 0x00000080};
							enum {Value_BRANCH1 = 0x5};
							enum {ValueMask_BRANCH1 = 0x000000A0};
							enum {Value_BRANCH = 0x6};
							enum {ValueMask_BRANCH = 0x000000C0};
							enum {Value_STOP = 0x7};
							enum {ValueMask_STOP = 0x000000E0};
							};
						namespace SHIFT_REG { // Field Description
							enum {Lsb = 4};
							enum {FieldMask = 0x00000010};
							enum {Value_DataRegister = 0x1};
							enum {ValueMask_DataRegister = 0x00000010};
							enum {Value_AddressRegister = 0x0};
							enum {ValueMask_AddressRegister = 0x00000000};
							};
						namespace IO_MODE { // Field Description
							enum {Lsb = 3};
							enum {FieldMask = 0x00000008};
							enum {Value_OutputMode = 0x1};
							enum {ValueMask_OutputMode = 0x00000008};
							enum {Value_InputMode = 0x0};
							enum {ValueMask_InputMode = 0x00000000};
							};
						namespace CHIP_SEL { // Field Description
							enum {Lsb = 2};
							enum {FieldMask = 0x00000004};
							enum {Value_HIGH = 0x1};
							enum {ValueMask_HIGH = 0x00000004};
							enum {Value_LOW = 0x0};
							enum {ValueMask_LOW = 0x00000000};
							};
						namespace DATA_OUT { // Field Description
							enum {Lsb = 1};
							enum {FieldMask = 0x00000002};
							enum {Value_HIGH = 0x1};
							enum {ValueMask_HIGH = 0x00000002};
							enum {Value_LOW = 0x0};
							enum {ValueMask_LOW = 0x00000000};
							};
						namespace SERIAL_CLK { // Field Description
							enum {Lsb = 0};
							enum {FieldMask = 0x00000001};
							enum {Value_HIGH = 0x1};
							enum {ValueMask_HIGH = 0x00000001};
							enum {Value_LOW = 0x0};
							enum {ValueMask_LOW = 0x00000000};
							};
						};
					};
				};
			};
		};
	};
#endif
