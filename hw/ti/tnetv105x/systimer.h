#ifndef _oscl_hw_ti_tnetv105x_systimerh_
#define _oscl_hw_ti_tnetv105x_systimerh_
#include "systimerreg.h"

namespace Oscl {
namespace HW {
namespace TI {
namespace TNETV105X {
namespace SYS_TIMER {

/** The SYS_TIMER offset map. */
struct Map {
	volatile CTRL::Reg		ctrl;			// 0x00
	LOAD::Reg				load;			// 0x04
	volatile VALUE::Reg		value;			// 0x08
	volatile INT::Reg		intr;			// 0x0C
	};

}
}
}
}
}

#endif
