/*
   Copyright (C) 2021 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_hw_nordic_spim_regh_
#define _oscl_hw_nordic_spim_regh_
#include <stdint.h>
namespace Oscl { // Namespace description
	namespace Nordic { // Namespace description
		namespace SPIM { // Namespace description
			namespace TASKS_START { // Register description
				typedef uint32_t	Reg;
				namespace START { // Field Description
					constexpr Reg Lsb = 0;
					constexpr Reg FieldMask = 0x00000001;
					constexpr Reg Value_Trigger = 0x1;
					constexpr Reg ValueMask_Trigger = 0x00000001;
					};
				};
			namespace TASKS_STOP { // Register description
				typedef uint32_t	Reg;
				namespace STOP { // Field Description
					constexpr Reg Lsb = 0;
					constexpr Reg FieldMask = 0x00000001;
					constexpr Reg Value_Trigger = 0x1;
					constexpr Reg ValueMask_Trigger = 0x00000001;
					};
				};
			namespace TASKS_SUSPEND { // Register description
				typedef uint32_t	Reg;
				namespace SUSPEND { // Field Description
					constexpr Reg Lsb = 0;
					constexpr Reg FieldMask = 0x00000001;
					constexpr Reg Value_Trigger = 0x1;
					constexpr Reg ValueMask_Trigger = 0x00000001;
					};
				};
			namespace TASKS_RESUME { // Register description
				typedef uint32_t	Reg;
				namespace RESUME { // Field Description
					constexpr Reg Lsb = 0;
					constexpr Reg FieldMask = 0x00000001;
					constexpr Reg Value_Trigger = 0x1;
					constexpr Reg ValueMask_Trigger = 0x00000001;
					};
				};
			namespace EVENTS_STOPPED { // Register description
				typedef uint32_t	Reg;
				namespace STOPPED { // Field Description
					constexpr Reg Lsb = 0;
					constexpr Reg FieldMask = 0x00000001;
					constexpr Reg Value_NotGenerated = 0x0;
					constexpr Reg ValueMask_NotGenerated = 0x00000000;
					constexpr Reg Value_Generated = 0x1;
					constexpr Reg ValueMask_Generated = 0x00000001;
					};
				};
			namespace EVENTS_ENDRX { // Register description
				typedef uint32_t	Reg;
				namespace ENDRX { // Field Description
					constexpr Reg Lsb = 0;
					constexpr Reg FieldMask = 0x00000001;
					constexpr Reg Value_NotGenerated = 0x0;
					constexpr Reg ValueMask_NotGenerated = 0x00000000;
					constexpr Reg Value_Generated = 0x1;
					constexpr Reg ValueMask_Generated = 0x00000001;
					};
				};
			namespace EVENTS_END { // Register description
				typedef uint32_t	Reg;
				namespace END { // Field Description
					constexpr Reg Lsb = 0;
					constexpr Reg FieldMask = 0x00000001;
					constexpr Reg Value_NotGenerated = 0x0;
					constexpr Reg ValueMask_NotGenerated = 0x00000000;
					constexpr Reg Value_Generated = 0x1;
					constexpr Reg ValueMask_Generated = 0x00000001;
					};
				};
			namespace EVENTS_ENDTX { // Register description
				typedef uint32_t	Reg;
				namespace ENDTX { // Field Description
					constexpr Reg Lsb = 0;
					constexpr Reg FieldMask = 0x00000001;
					constexpr Reg Value_NotGenerated = 0x0;
					constexpr Reg ValueMask_NotGenerated = 0x00000000;
					constexpr Reg Value_Generated = 0x1;
					constexpr Reg ValueMask_Generated = 0x00000001;
					};
				};
			namespace EVENTS_STARTED { // Register description
				typedef uint32_t	Reg;
				namespace STARTED { // Field Description
					constexpr Reg Lsb = 0;
					constexpr Reg FieldMask = 0x00000001;
					constexpr Reg Value_NotGenerated = 0x0;
					constexpr Reg ValueMask_NotGenerated = 0x00000000;
					constexpr Reg Value_Generated = 0x1;
					constexpr Reg ValueMask_Generated = 0x00000001;
					};
				};
			namespace SHORTS { // Register description
				typedef uint32_t	Reg;
				namespace END_START { // Field Description
					constexpr Reg Lsb = 0;
					constexpr Reg FieldMask = 0x00000001;
					constexpr Reg Value_Disabled = 0x0;
					constexpr Reg ValueMask_Disabled = 0x00000000;
					constexpr Reg Value_Enabled = 0x1;
					constexpr Reg ValueMask_Enabled = 0x00000001;
					};
				};
			namespace INTENSET { // Register description
				typedef uint32_t	Reg;
				constexpr Reg	Set = 1;
				constexpr Reg	Disabled = 0;
				constexpr Reg	Enabled = 1;
				namespace STOPPED { // Field Description
					constexpr Reg Lsb = 1;
					constexpr Reg FieldMask = 0x00000002;
					constexpr Reg Value_Set = 0x1;
					constexpr Reg ValueMask_Set = 0x00000002;
					constexpr Reg Value_Disabled = 0x0;
					constexpr Reg ValueMask_Disabled = 0x00000000;
					constexpr Reg Value_Enabled = 0x1;
					constexpr Reg ValueMask_Enabled = 0x00000002;
					};
				namespace ENDRX { // Field Description
					constexpr Reg Lsb = 4;
					constexpr Reg FieldMask = 0x00000010;
					constexpr Reg Value_Set = 0x1;
					constexpr Reg ValueMask_Set = 0x00000010;
					constexpr Reg Value_Disabled = 0x0;
					constexpr Reg ValueMask_Disabled = 0x00000000;
					constexpr Reg Value_Enabled = 0x1;
					constexpr Reg ValueMask_Enabled = 0x00000010;
					};
				namespace END { // Field Description
					constexpr Reg Lsb = 6;
					constexpr Reg FieldMask = 0x00000040;
					constexpr Reg Value_Set = 0x1;
					constexpr Reg ValueMask_Set = 0x00000040;
					constexpr Reg Value_Disabled = 0x0;
					constexpr Reg ValueMask_Disabled = 0x00000000;
					constexpr Reg Value_Enabled = 0x1;
					constexpr Reg ValueMask_Enabled = 0x00000040;
					};
				namespace ENDTX { // Field Description
					constexpr Reg Lsb = 8;
					constexpr Reg FieldMask = 0x00000100;
					constexpr Reg Value_Set = 0x1;
					constexpr Reg ValueMask_Set = 0x00000100;
					constexpr Reg Value_Disabled = 0x0;
					constexpr Reg ValueMask_Disabled = 0x00000000;
					constexpr Reg Value_Enabled = 0x1;
					constexpr Reg ValueMask_Enabled = 0x00000100;
					};
				namespace STARTED { // Field Description
					constexpr Reg Lsb = 19;
					constexpr Reg FieldMask = 0x00080000;
					constexpr Reg Value_Set = 0x1;
					constexpr Reg ValueMask_Set = 0x00080000;
					constexpr Reg Value_Disabled = 0x0;
					constexpr Reg ValueMask_Disabled = 0x00000000;
					constexpr Reg Value_Enabled = 0x1;
					constexpr Reg ValueMask_Enabled = 0x00080000;
					};
				};
			namespace INTENCLR { // Register description
				typedef uint32_t	Reg;
				constexpr Reg	Clear = 1;
				constexpr Reg	Disabled = 0;
				constexpr Reg	Enabled = 1;
				namespace STOPPED { // Field Description
					constexpr Reg Lsb = 1;
					constexpr Reg FieldMask = 0x00000002;
					constexpr Reg Value_Clear = 0x1;
					constexpr Reg ValueMask_Clear = 0x00000002;
					constexpr Reg Value_Disabled = 0x0;
					constexpr Reg ValueMask_Disabled = 0x00000000;
					constexpr Reg Value_Enabled = 0x1;
					constexpr Reg ValueMask_Enabled = 0x00000002;
					};
				namespace ENDRX { // Field Description
					constexpr Reg Lsb = 4;
					constexpr Reg FieldMask = 0x00000010;
					constexpr Reg Value_Clear = 0x1;
					constexpr Reg ValueMask_Clear = 0x00000010;
					constexpr Reg Value_Disabled = 0x0;
					constexpr Reg ValueMask_Disabled = 0x00000000;
					constexpr Reg Value_Enabled = 0x1;
					constexpr Reg ValueMask_Enabled = 0x00000010;
					};
				namespace END { // Field Description
					constexpr Reg Lsb = 6;
					constexpr Reg FieldMask = 0x00000040;
					constexpr Reg Value_Clear = 0x1;
					constexpr Reg ValueMask_Clear = 0x00000040;
					constexpr Reg Value_Disabled = 0x0;
					constexpr Reg ValueMask_Disabled = 0x00000000;
					constexpr Reg Value_Enabled = 0x1;
					constexpr Reg ValueMask_Enabled = 0x00000040;
					};
				namespace ENDTX { // Field Description
					constexpr Reg Lsb = 8;
					constexpr Reg FieldMask = 0x00000100;
					constexpr Reg Value_Clear = 0x1;
					constexpr Reg ValueMask_Clear = 0x00000100;
					constexpr Reg Value_Disabled = 0x0;
					constexpr Reg ValueMask_Disabled = 0x00000000;
					constexpr Reg Value_Enabled = 0x1;
					constexpr Reg ValueMask_Enabled = 0x00000100;
					};
				namespace STARTED { // Field Description
					constexpr Reg Lsb = 19;
					constexpr Reg FieldMask = 0x00080000;
					constexpr Reg Value_Clear = 0x1;
					constexpr Reg ValueMask_Clear = 0x00080000;
					constexpr Reg Value_Disabled = 0x0;
					constexpr Reg ValueMask_Disabled = 0x00000000;
					constexpr Reg Value_Enabled = 0x1;
					constexpr Reg ValueMask_Enabled = 0x00080000;
					};
				};
			namespace STALLSTAT { // Register description
				typedef uint32_t	Reg;
				constexpr Reg	NoStall = 0;
				constexpr Reg	Stall = 1;
				namespace TX { // Field Description
					constexpr Reg Lsb = 0;
					constexpr Reg FieldMask = 0x00000001;
					constexpr Reg Value_NoStall = 0x0;
					constexpr Reg ValueMask_NoStall = 0x00000000;
					constexpr Reg Value_Stall = 0x1;
					constexpr Reg ValueMask_Stall = 0x00000001;
					};
				namespace RX { // Field Description
					constexpr Reg Lsb = 1;
					constexpr Reg FieldMask = 0x00000002;
					constexpr Reg Value_NoStall = 0x0;
					constexpr Reg ValueMask_NoStall = 0x00000000;
					constexpr Reg Value_Stall = 0x1;
					constexpr Reg ValueMask_Stall = 0x00000002;
					};
				};
			namespace ENABLE { // Register description
				typedef uint32_t	Reg;
				namespace ENABLE { // Field Description
					constexpr Reg Lsb = 0;
					constexpr Reg FieldMask = 0x0000000F;
					constexpr Reg Value_Disabled = 0x0;
					constexpr Reg ValueMask_Disabled = 0x00000000;
					constexpr Reg Value_Enabled = 0x7;
					constexpr Reg ValueMask_Enabled = 0x00000007;
					};
				};
			namespace PSEL_SCK { // Register description
				typedef uint32_t	Reg;
				namespace PIN { // Field Description
					constexpr Reg Lsb = 0;
					constexpr Reg FieldMask = 0x0000001F;
					};
				namespace PORT { // Field Description
					constexpr Reg Lsb = 5;
					constexpr Reg FieldMask = 0x00000020;
					};
				namespace CONNECT { // Field Description
					constexpr Reg Lsb = 31;
					constexpr Reg FieldMask = 0x80000000;
					constexpr Reg Value_Disconnect = 0x1;
					constexpr Reg ValueMask_Disconnect = 0x80000000;
					constexpr Reg Value_Connect = 0x0;
					constexpr Reg ValueMask_Connect = 0x00000000;
					};
				};
			namespace PSEL_MOSI { // Register description
				typedef uint32_t	Reg;
				namespace PIN { // Field Description
					constexpr Reg Lsb = 0;
					constexpr Reg FieldMask = 0x0000001F;
					};
				namespace PORT { // Field Description
					constexpr Reg Lsb = 5;
					constexpr Reg FieldMask = 0x00000020;
					};
				namespace CONNECT { // Field Description
					constexpr Reg Lsb = 31;
					constexpr Reg FieldMask = 0x80000000;
					constexpr Reg Value_Disconnect = 0x1;
					constexpr Reg ValueMask_Disconnect = 0x80000000;
					constexpr Reg Value_Connect = 0x0;
					constexpr Reg ValueMask_Connect = 0x00000000;
					};
				};
			namespace PSEL_MISO { // Register description
				typedef uint32_t	Reg;
				namespace PIN { // Field Description
					constexpr Reg Lsb = 0;
					constexpr Reg FieldMask = 0x0000001F;
					};
				namespace PORT { // Field Description
					constexpr Reg Lsb = 5;
					constexpr Reg FieldMask = 0x00000020;
					};
				namespace CONNECT { // Field Description
					constexpr Reg Lsb = 31;
					constexpr Reg FieldMask = 0x80000000;
					constexpr Reg Value_Disconnect = 0x1;
					constexpr Reg ValueMask_Disconnect = 0x80000000;
					constexpr Reg Value_Connect = 0x0;
					constexpr Reg ValueMask_Connect = 0x00000000;
					};
				};
			namespace PSEL_CSN { // Register description
				typedef uint32_t	Reg;
				namespace PIN { // Field Description
					constexpr Reg Lsb = 0;
					constexpr Reg FieldMask = 0x0000001F;
					};
				namespace PORT { // Field Description
					constexpr Reg Lsb = 5;
					constexpr Reg FieldMask = 0x00000020;
					};
				namespace CONNECT { // Field Description
					constexpr Reg Lsb = 31;
					constexpr Reg FieldMask = 0x80000000;
					constexpr Reg Value_Disconnect = 0x1;
					constexpr Reg ValueMask_Disconnect = 0x80000000;
					constexpr Reg Value_Connect = 0x0;
					constexpr Reg ValueMask_Connect = 0x00000000;
					};
				};
			namespace FREQUENCY { // Register description
				typedef uint32_t	Reg;
				constexpr Reg	K125 = 33554432;
				constexpr Reg	K250 = 67108864;
				constexpr Reg	K500 = 150994944;
				constexpr Reg	M1 = 268435456;
				constexpr Reg	M2 = 536870912;
				constexpr Reg	M4 = 1073741824;
				constexpr Reg	M8 = -2147483648;
				constexpr Reg	M16 = 167772160;
				constexpr Reg	M32 = 335544320;
				};
			namespace RXD_PTR { // Register description
				typedef uint32_t	Reg;
				};
			namespace RXDMAXCNT { // Register description
				typedef uint32_t	Reg;
				namespace MAXCNT { // Field Description
					constexpr Reg Lsb = 0;
					constexpr Reg FieldMask = 0x00007FFF;
					constexpr Reg Value_Max = 0x7FFF;
					constexpr Reg ValueMask_Max = 0x00007FFF;
					};
				};
			namespace RXDAMOUNT { // Register description
				typedef uint32_t	Reg;
				namespace AMOUNT { // Field Description
					constexpr Reg Lsb = 0;
					constexpr Reg FieldMask = 0x00007FFF;
					constexpr Reg Value_Max = 0x7FFF;
					constexpr Reg ValueMask_Max = 0x00007FFF;
					};
				};
			namespace RXDLIST { // Register description
				typedef uint32_t	Reg;
				namespace LIST { // Field Description
					constexpr Reg Lsb = 0;
					constexpr Reg FieldMask = 0x00000003;
					constexpr Reg Value_Disabled = 0x0;
					constexpr Reg ValueMask_Disabled = 0x00000000;
					constexpr Reg Value_ArrayList = 0x1;
					constexpr Reg ValueMask_ArrayList = 0x00000001;
					};
				};
			namespace TXD_PTR { // Register description
				typedef uint32_t	Reg;
				};
			namespace TXDMAXCNT { // Register description
				typedef uint32_t	Reg;
				namespace MAXCNT { // Field Description
					constexpr Reg Lsb = 0;
					constexpr Reg FieldMask = 0x00007FFF;
					constexpr Reg Value_Max = 0x7FFF;
					constexpr Reg ValueMask_Max = 0x00007FFF;
					};
				};
			namespace TXDAMOUNT { // Register description
				typedef uint32_t	Reg;
				namespace AMOUNT { // Field Description
					constexpr Reg Lsb = 0;
					constexpr Reg FieldMask = 0x00007FFF;
					constexpr Reg Value_Max = 0x7FFF;
					constexpr Reg ValueMask_Max = 0x00007FFF;
					};
				};
			namespace TXDLIST { // Register description
				typedef uint32_t	Reg;
				namespace LIST { // Field Description
					constexpr Reg Lsb = 0;
					constexpr Reg FieldMask = 0x00000003;
					constexpr Reg Value_Disabled = 0x0;
					constexpr Reg ValueMask_Disabled = 0x00000000;
					constexpr Reg Value_ArrayList = 0x1;
					constexpr Reg ValueMask_ArrayList = 0x00000001;
					};
				};
			namespace CONFIG { // Register description
				typedef uint32_t	Reg;
				namespace ORDER { // Field Description
					constexpr Reg Lsb = 0;
					constexpr Reg FieldMask = 0x00000001;
					constexpr Reg Value_MsbFirst = 0x0;
					constexpr Reg ValueMask_MsbFirst = 0x00000000;
					constexpr Reg Value_LsbFirst = 0x1;
					constexpr Reg ValueMask_LsbFirst = 0x00000001;
					};
				namespace CPHA { // Field Description
					constexpr Reg Lsb = 1;
					constexpr Reg FieldMask = 0x00000002;
					constexpr Reg Value_Leading = 0x0;
					constexpr Reg ValueMask_Leading = 0x00000000;
					constexpr Reg Value_Trailing = 0x1;
					constexpr Reg ValueMask_Trailing = 0x00000002;
					};
				namespace CPOL { // Field Description
					constexpr Reg Lsb = 2;
					constexpr Reg FieldMask = 0x00000004;
					constexpr Reg Value_ActiveHigh = 0x0;
					constexpr Reg ValueMask_ActiveHigh = 0x00000000;
					constexpr Reg Value_ActiveLow = 0x1;
					constexpr Reg ValueMask_ActiveLow = 0x00000004;
					};
				};
			namespace IFTIMINGRXDELAY { // Register description
				typedef uint32_t	Reg;
				namespace RXDELAY { // Field Description
					constexpr Reg Lsb = 0;
					constexpr Reg FieldMask = 0x00000007;
					constexpr Reg Value_Max = 0xF;
					constexpr Reg ValueMask_Max = 0x0000000F;
					};
				};
			namespace IFTIMINGCSNDUR { // Register description
				typedef uint32_t	Reg;
				namespace CSNDUR { // Field Description
					constexpr Reg Lsb = 0;
					constexpr Reg FieldMask = 0x000000FF;
					constexpr Reg Value_Max = 0xFF;
					constexpr Reg ValueMask_Max = 0x000000FF;
					};
				};
			namespace CSNPOL { // Register description
				typedef uint32_t	Reg;
				namespace CSNPOL { // Field Description
					constexpr Reg Lsb = 0;
					constexpr Reg FieldMask = 0x00000001;
					constexpr Reg Value_LOW = 0x0;
					constexpr Reg ValueMask_LOW = 0x00000000;
					constexpr Reg Value_HIGH = 0x1;
					constexpr Reg ValueMask_HIGH = 0x00000001;
					};
				};
			namespace PSEL_DCX { // Register description
				typedef uint32_t	Reg;
				namespace PIN { // Field Description
					constexpr Reg Lsb = 0;
					constexpr Reg FieldMask = 0x0000001F;
					};
				namespace PORT { // Field Description
					constexpr Reg Lsb = 5;
					constexpr Reg FieldMask = 0x00000020;
					};
				namespace CONNECT { // Field Description
					constexpr Reg Lsb = 31;
					constexpr Reg FieldMask = 0x80000000;
					constexpr Reg Value_Disconnect = 0x1;
					constexpr Reg ValueMask_Disconnect = 0x80000000;
					constexpr Reg Value_Connect = 0x0;
					constexpr Reg ValueMask_Connect = 0x00000000;
					};
				};
			namespace DCXCNT { // Register description
				typedef uint32_t	Reg;
				namespace DCXCNT { // Field Description
					constexpr Reg Lsb = 0;
					constexpr Reg FieldMask = 0x0000000F;
					constexpr Reg Value_Max = 0xF;
					constexpr Reg ValueMask_Max = 0x0000000F;
					};
				};
			namespace ORC { // Register description
				typedef uint32_t	Reg;
				namespace ORC { // Field Description
					constexpr Reg Lsb = 0;
					constexpr Reg FieldMask = 0x000000FF;
					constexpr Reg Value_Max = 0xFF;
					constexpr Reg ValueMask_Max = 0x000000FF;
					};
				};
			}
		}
	}
#endif
