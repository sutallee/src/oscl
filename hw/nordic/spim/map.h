/*
   Copyright (C) 2021 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_hw_nordic_spim_maph_
#define _oscl_hw_nordic_spim_maph_

#include "reg.h"

/** */
namespace Oscl {

/** */
namespace Nordic {

/** */
namespace SPIM {

/** */
struct Map {
	uint32_t		unused0x000[(0x010-0x000)/4];

	/** Offset 0x010 */
	volatile Oscl::Nordic::SPIM::TASKS_START::Reg		start;

	/** Offset 0x014 */
	volatile Oscl::Nordic::SPIM::TASKS_STOP::Reg		stop;

	uint32_t		unused0x018[(0x01C-0x018)/4];

	/** Offset 0x01C */
	volatile Oscl::Nordic::SPIM::TASKS_SUSPEND::Reg		suspend;

	/** Offset 0x020 */
	volatile Oscl::Nordic::SPIM::TASKS_RESUME::Reg		resume;

	uint32_t		unused0x024[(0x104-0x024)/4];

	/** Offset 0x104 */
	volatile Oscl::Nordic::SPIM::EVENTS_STOPPED::Reg	stopped;

	uint32_t		unused0x108[(0x110-0x108)/4];

	/** Offset 0x110 */
	volatile Oscl::Nordic::SPIM::EVENTS_ENDRX::Reg		endrx;

	uint32_t		unused0x114[(0x118-0x114)/4];

	/** Offset 0x118 */
	volatile Oscl::Nordic::SPIM::EVENTS_END::Reg		end;

	uint32_t		unused0x11C[(0x120-0x11C)/4];

	/** Offset 0x120 */
	volatile Oscl::Nordic::SPIM::EVENTS_ENDTX::Reg		endtx;

	uint32_t		unused0x124[(0x14C-0x124)/4];

	/** Offset 0x14C */
	volatile Oscl::Nordic::SPIM::EVENTS_STARTED::Reg	started;

	uint32_t		unused0x150[(0x200-0x150)/4];

	/** Offset 0x200 */
	volatile Oscl::Nordic::SPIM::SHORTS::Reg			shorts;

	uint32_t		unused0x204[(0x304-0x204)/4];

	/** Offset 0x304 */
	volatile Oscl::Nordic::SPIM::INTENSET::Reg			intenset;

	/** Offset 0x308 */
	volatile Oscl::Nordic::SPIM::INTENCLR::Reg			intenclr;

	uint32_t		unused0x30C[(0x400-0x30C)/4];

	/** Offset 0x400 */
	volatile Oscl::Nordic::SPIM::STALLSTAT::Reg			stallstat;

	uint32_t		unused0x404[(0x500-0x404)/4];

	/** Offset 0x500 */
	volatile Oscl::Nordic::SPIM::ENABLE::Reg			enable;

	uint32_t		unused0x504[(0x508-0x504)/4];

	/** Offset 0x508 */
	volatile Oscl::Nordic::SPIM::PSEL_SCK::Reg			pselsck;

	/** Offset 0x50C */
	volatile Oscl::Nordic::SPIM::PSEL_MOSI::Reg			pselmosi;

	/** Offset 0x510 */
	volatile Oscl::Nordic::SPIM::PSEL_MISO::Reg			pselmiso;

	/** Offset 0x514 */
	volatile Oscl::Nordic::SPIM::PSEL_CSN::Reg			pselcsn;

	uint32_t		unused0x518[(0x524-0x518)/4];

	/** Offset 0x524 */
	volatile Oscl::Nordic::SPIM::FREQUENCY::Reg			frequency;

	uint32_t		unused0x528[(0x534-0x528)/4];

	/** Offset 0x534 */
	volatile Oscl::Nordic::SPIM::RXD_PTR::Reg			rxdptr;

	/** Offset 0x538 */
	volatile Oscl::Nordic::SPIM::RXDMAXCNT::Reg			rxdmaxcnt;

	/** Offset 0x53C */
	volatile Oscl::Nordic::SPIM::RXDAMOUNT::Reg			rxdamount;

	/** Offset 0x540 */
	volatile Oscl::Nordic::SPIM::RXDLIST::Reg			rxdlist;

	/** Offset 0x544 */
	volatile Oscl::Nordic::SPIM::TXD_PTR::Reg			txdptr;

	/** Offset 0x548 */
	volatile Oscl::Nordic::SPIM::TXDMAXCNT::Reg			txdmaxcnt;

	/** Offset 0x54C */
	volatile Oscl::Nordic::SPIM::TXDAMOUNT::Reg			txdamount;

	/** Offset 0x550 */
	volatile Oscl::Nordic::SPIM::TXDLIST::Reg			txdlist;

	/** Offset 0x554 */
	volatile Oscl::Nordic::SPIM::CONFIG::Reg			config;

	uint32_t		unused0x558[(0x560-0x558)/4];

	/** Offset 0x560 */
	volatile Oscl::Nordic::SPIM::IFTIMINGRXDELAY::Reg	iftimingrxdelay;

	/** Offset 0x564 */
	volatile Oscl::Nordic::SPIM::IFTIMINGCSNDUR::Reg	iftimingcsndur;

	/** Offset 0x568 */
	volatile Oscl::Nordic::SPIM::CSNPOL::Reg			csnpol;

	/** Offset 0x56C */
	volatile Oscl::Nordic::SPIM::PSEL_DCX::Reg			pseldcx;

	/** Offset 0x570 */
	volatile Oscl::Nordic::SPIM::DCXCNT::Reg			dcxcnt;

	uint32_t		unused0x574[(0x5C0-0x574)/4];

	/** Offset 0x5C0 */
	volatile Oscl::Nordic::SPIM::ORC::Reg				orc;
	};

}
}
}

#endif
