/*
   Copyright (C) 2020 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_hw_nordic_saadc_maph_
#define _oscl_hw_nordic_saadc_maph_

#include "reg.h"

/** */
namespace Oscl {

/** */
namespace Nordic {

/** */
namespace SAADC {

/** */
struct Map {
	/** Offset 0x000 */
	volatile Oscl::Nordic::SAADC::TASKS_START::Reg		start;

	/** Offset 0x004 */
	volatile Oscl::Nordic::SAADC::TASKS_SAMPLE::Reg		sample;

	/** Offset 0x008 */
	volatile Oscl::Nordic::SAADC::TASKS_STOP::Reg		stop;

	/** Offset 0x00C */
	volatile Oscl::Nordic::SAADC::TASKS_CALIBRATEOFFSET::Reg	calibrateOffset;

	uint32_t		unused0x010[(0x100-0x010)/4];

	/** Offset 0x100 */
	volatile Oscl::Nordic::SAADC::EVENTS_STARTED::Reg	started;

	/** Offset 0x104 */
	volatile Oscl::Nordic::SAADC::EVENTS_END::Reg		end;

	/** Offset 0x108 */
	volatile Oscl::Nordic::SAADC::EVENTS_DONE::Reg		done;

	/** Offset 0x10C */
	volatile Oscl::Nordic::SAADC::EVENTS_RESULTDONE::Reg	resultDone;

	/** Offset 0x110 */
	volatile Oscl::Nordic::SAADC::EVENTS_CALIBRATEDONE::Reg	calibrateDone;

	/** Offset 0x114 */
	volatile Oscl::Nordic::SAADC::EVENTS_STOPPED::Reg		stopped;

	/** Offset 0x118 */
	struct {
		/** */
		volatile Oscl::Nordic::SAADC::EVENTS_LIMITH::Reg	limith;
		/** */
		volatile Oscl::Nordic::SAADC::EVENTS_LIMITH::Reg	limitl;
		} limit_ch[8];

	uint32_t		unused0x158[(0x300-0x158)/4];

	/** Offset 0x300 */
	volatile Oscl::Nordic::SAADC::INTEN::Reg		inten;

	/** Offset 0x304 */
	volatile Oscl::Nordic::SAADC::INTENSET::Reg		intenset;

	/** Offset 0x308 */
	volatile Oscl::Nordic::SAADC::INTENCLR::Reg		intenclr;

	uint32_t		unused0x30C[(0x400-0x30C)/4];

	/** Offset 0x400 */
	volatile Oscl::Nordic::SAADC::STATUS::Reg		status;

	uint32_t		unused0x404[(0x500-0x404)/4];

	/** Offset 0x500 */
	volatile Oscl::Nordic::SAADC::ENABLE::Reg		enable;

	uint32_t		unused0x504[(0x510-0x504)/4];

	/** Offset 0x510 */
	struct {
		/** */
		volatile Oscl::Nordic::SAADC::PSELP::Reg	pselp;
		/** */
		volatile Oscl::Nordic::SAADC::PSELN::Reg	pseln;
		/** */
		volatile Oscl::Nordic::SAADC::CONFIG::Reg	config;
		/** */
		volatile Oscl::Nordic::SAADC::LIMIT::Reg	limit;
		} ch[8];

	uint32_t		unused0x590[(0x5F0-0x590)/4];

	/** Offset 0x5F0 */
	volatile Oscl::Nordic::SAADC::RESOLUTION::Reg		resolution;

	/** Offset 0x5F4 */
	volatile Oscl::Nordic::SAADC::OVERSAMPLE::Reg		oversample;

	/** Offset 0x5F8 */
	volatile Oscl::Nordic::SAADC::SAMPLERATE::Reg		samplerate;

	uint32_t		unused0x5FC[(0x62C-0x5FC)/4];

	/** Offset 0x62C */
	volatile Oscl::Nordic::SAADC::RESULTPTR::Reg		resultptr;

	/** Offset 0x630 */
	volatile Oscl::Nordic::SAADC::RESULTMAXCNT::Reg		resultmaxcnt;

	/** Offset 0x634 */
	volatile Oscl::Nordic::SAADC::RESULTAMOUNT::Reg		resultamount;
	};

}
}
}

#endif
