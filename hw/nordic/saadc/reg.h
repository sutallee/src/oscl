#ifndef _oscl_hw_nordic_saadc_regh_
#define _oscl_hw_nordic_saadc_regh_
#include <stdint.h>
namespace Oscl { // Namespace description
	namespace Nordic { // Namespace description
		namespace SAADC { // Namespace description
			namespace TASKS_START { // Register description
				typedef uint32_t	Reg;
				namespace START { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x00000001};
					enum {Value_Trigger = 0x1};
					enum {ValueMask_Trigger = 0x00000001};
					};
				};
			namespace TASKS_SAMPLE { // Register description
				typedef uint32_t	Reg;
				namespace SAMPLE { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x00000001};
					enum {Value_Trigger = 0x1};
					enum {ValueMask_Trigger = 0x00000001};
					};
				};
			namespace TASKS_STOP { // Register description
				typedef uint32_t	Reg;
				namespace STOP { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x00000001};
					enum {Value_Trigger = 0x1};
					enum {ValueMask_Trigger = 0x00000001};
					};
				};
			namespace TASKS_CALIBRATEOFFSET { // Register description
				typedef uint32_t	Reg;
				namespace CALIBRATE { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x00000001};
					enum {Value_Trigger = 0x1};
					enum {ValueMask_Trigger = 0x00000001};
					};
				};
			namespace EVENTS_STARTED { // Register description
				typedef uint32_t	Reg;
				namespace STARTED { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x00000001};
					enum {Value_NotGenerated = 0x0};
					enum {ValueMask_NotGenerated = 0x00000000};
					enum {Value_Generated = 0x1};
					enum {ValueMask_Generated = 0x00000001};
					};
				};
			namespace EVENTS_END { // Register description
				typedef uint32_t	Reg;
				namespace END { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x00000001};
					enum {Value_NotGenerated = 0x0};
					enum {ValueMask_NotGenerated = 0x00000000};
					enum {Value_Generated = 0x1};
					enum {ValueMask_Generated = 0x00000001};
					};
				};
			namespace EVENTS_DONE { // Register description
				typedef uint32_t	Reg;
				namespace DONE { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x00000001};
					enum {Value_NotGenerated = 0x0};
					enum {ValueMask_NotGenerated = 0x00000000};
					enum {Value_Generated = 0x1};
					enum {ValueMask_Generated = 0x00000001};
					};
				};
			namespace EVENTS_RESULTDONE { // Register description
				typedef uint32_t	Reg;
				namespace RESULTDONE { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x00000001};
					enum {Value_NotGenerated = 0x0};
					enum {ValueMask_NotGenerated = 0x00000000};
					enum {Value_Generated = 0x1};
					enum {ValueMask_Generated = 0x00000001};
					};
				};
			namespace EVENTS_CALIBRATEDONE { // Register description
				typedef uint32_t	Reg;
				namespace CALIBRATEDONE { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x00000001};
					enum {Value_NotGenerated = 0x0};
					enum {ValueMask_NotGenerated = 0x00000000};
					enum {Value_Generated = 0x1};
					enum {ValueMask_Generated = 0x00000001};
					};
				};
			namespace EVENTS_STOPPED { // Register description
				typedef uint32_t	Reg;
				namespace STOPPED { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x00000001};
					enum {Value_NotGenerated = 0x0};
					enum {ValueMask_NotGenerated = 0x00000000};
					enum {Value_Generated = 0x1};
					enum {ValueMask_Generated = 0x00000001};
					};
				};
			namespace EVENTS_LIMITH { // Register description
				typedef uint32_t	Reg;
				namespace LIMITH { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x00000001};
					enum {Value_NotGenerated = 0x0};
					enum {ValueMask_NotGenerated = 0x00000000};
					enum {Value_Generated = 0x1};
					enum {ValueMask_Generated = 0x00000001};
					};
				};
			namespace EVENTS_LIMITL { // Register description
				typedef uint32_t	Reg;
				namespace LIMITL { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x00000001};
					enum {Value_NotGenerated = 0x0};
					enum {ValueMask_NotGenerated = 0x00000000};
					enum {Value_Generated = 0x1};
					enum {ValueMask_Generated = 0x00000001};
					};
				};
			namespace INTEN { // Register description
				typedef uint32_t	Reg;
				enum {Disabled = 0};
				enum {Enabled = 1};
				namespace STARTED { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x00000001};
					enum {Value_Disabled = 0x0};
					enum {ValueMask_Disabled = 0x00000000};
					enum {Value_Enabled = 0x1};
					enum {ValueMask_Enabled = 0x00000001};
					};
				namespace END { // Field Description
					enum {Lsb = 1};
					enum {FieldMask = 0x00000002};
					enum {Value_Disabled = 0x0};
					enum {ValueMask_Disabled = 0x00000000};
					enum {Value_Enabled = 0x1};
					enum {ValueMask_Enabled = 0x00000002};
					};
				namespace DONE { // Field Description
					enum {Lsb = 2};
					enum {FieldMask = 0x00000004};
					enum {Value_Disabled = 0x0};
					enum {ValueMask_Disabled = 0x00000000};
					enum {Value_Enabled = 0x1};
					enum {ValueMask_Enabled = 0x00000004};
					};
				namespace RESULTDONE { // Field Description
					enum {Lsb = 3};
					enum {FieldMask = 0x00000008};
					enum {Value_Disabled = 0x0};
					enum {ValueMask_Disabled = 0x00000000};
					enum {Value_Enabled = 0x1};
					enum {ValueMask_Enabled = 0x00000008};
					};
				namespace CALIBRATEDONE { // Field Description
					enum {Lsb = 4};
					enum {FieldMask = 0x00000010};
					enum {Value_Disabled = 0x0};
					enum {ValueMask_Disabled = 0x00000000};
					enum {Value_Enabled = 0x1};
					enum {ValueMask_Enabled = 0x00000010};
					};
				namespace STOPPED { // Field Description
					enum {Lsb = 5};
					enum {FieldMask = 0x00000020};
					enum {Value_Disabled = 0x0};
					enum {ValueMask_Disabled = 0x00000000};
					enum {Value_Enabled = 0x1};
					enum {ValueMask_Enabled = 0x00000020};
					};
				namespace CH0LIMITH { // Field Description
					enum {Lsb = 6};
					enum {FieldMask = 0x00000040};
					enum {Value_Disabled = 0x0};
					enum {ValueMask_Disabled = 0x00000000};
					enum {Value_Enabled = 0x1};
					enum {ValueMask_Enabled = 0x00000040};
					};
				namespace CH0LIMITL { // Field Description
					enum {Lsb = 7};
					enum {FieldMask = 0x00000080};
					enum {Value_Disabled = 0x0};
					enum {ValueMask_Disabled = 0x00000000};
					enum {Value_Enabled = 0x1};
					enum {ValueMask_Enabled = 0x00000080};
					};
				namespace CH1LIMITH { // Field Description
					enum {Lsb = 8};
					enum {FieldMask = 0x00000100};
					enum {Value_Disabled = 0x0};
					enum {ValueMask_Disabled = 0x00000000};
					enum {Value_Enabled = 0x1};
					enum {ValueMask_Enabled = 0x00000100};
					};
				namespace CH1LIMITL { // Field Description
					enum {Lsb = 9};
					enum {FieldMask = 0x00000200};
					enum {Value_Disabled = 0x0};
					enum {ValueMask_Disabled = 0x00000000};
					enum {Value_Enabled = 0x1};
					enum {ValueMask_Enabled = 0x00000200};
					};
				namespace CH2LIMITH { // Field Description
					enum {Lsb = 10};
					enum {FieldMask = 0x00000400};
					enum {Value_Disabled = 0x0};
					enum {ValueMask_Disabled = 0x00000000};
					enum {Value_Enabled = 0x1};
					enum {ValueMask_Enabled = 0x00000400};
					};
				namespace CH2LIMITL { // Field Description
					enum {Lsb = 11};
					enum {FieldMask = 0x00000800};
					enum {Value_Disabled = 0x0};
					enum {ValueMask_Disabled = 0x00000000};
					enum {Value_Enabled = 0x1};
					enum {ValueMask_Enabled = 0x00000800};
					};
				namespace CH3LIMITH { // Field Description
					enum {Lsb = 12};
					enum {FieldMask = 0x00001000};
					enum {Value_Disabled = 0x0};
					enum {ValueMask_Disabled = 0x00000000};
					enum {Value_Enabled = 0x1};
					enum {ValueMask_Enabled = 0x00001000};
					};
				namespace CH3LIMITL { // Field Description
					enum {Lsb = 13};
					enum {FieldMask = 0x00002000};
					enum {Value_Disabled = 0x0};
					enum {ValueMask_Disabled = 0x00000000};
					enum {Value_Enabled = 0x1};
					enum {ValueMask_Enabled = 0x00002000};
					};
				namespace CH4LIMITH { // Field Description
					enum {Lsb = 14};
					enum {FieldMask = 0x00004000};
					enum {Value_Disabled = 0x0};
					enum {ValueMask_Disabled = 0x00000000};
					enum {Value_Enabled = 0x1};
					enum {ValueMask_Enabled = 0x00004000};
					};
				namespace CH4LIMITL { // Field Description
					enum {Lsb = 15};
					enum {FieldMask = 0x00008000};
					enum {Value_Disabled = 0x0};
					enum {ValueMask_Disabled = 0x00000000};
					enum {Value_Enabled = 0x1};
					enum {ValueMask_Enabled = 0x00008000};
					};
				namespace CH5LIMITH { // Field Description
					enum {Lsb = 16};
					enum {FieldMask = 0x00010000};
					enum {Value_Disabled = 0x0};
					enum {ValueMask_Disabled = 0x00000000};
					enum {Value_Enabled = 0x1};
					enum {ValueMask_Enabled = 0x00010000};
					};
				namespace CH5LIMITL { // Field Description
					enum {Lsb = 17};
					enum {FieldMask = 0x00020000};
					enum {Value_Disabled = 0x0};
					enum {ValueMask_Disabled = 0x00000000};
					enum {Value_Enabled = 0x1};
					enum {ValueMask_Enabled = 0x00020000};
					};
				namespace CH6LIMITH { // Field Description
					enum {Lsb = 18};
					enum {FieldMask = 0x00040000};
					enum {Value_Disabled = 0x0};
					enum {ValueMask_Disabled = 0x00000000};
					enum {Value_Enabled = 0x1};
					enum {ValueMask_Enabled = 0x00040000};
					};
				namespace CH6LIMITL { // Field Description
					enum {Lsb = 19};
					enum {FieldMask = 0x00080000};
					enum {Value_Disabled = 0x0};
					enum {ValueMask_Disabled = 0x00000000};
					enum {Value_Enabled = 0x1};
					enum {ValueMask_Enabled = 0x00080000};
					};
				namespace CH7LIMITH { // Field Description
					enum {Lsb = 20};
					enum {FieldMask = 0x00100000};
					enum {Value_Disabled = 0x0};
					enum {ValueMask_Disabled = 0x00000000};
					enum {Value_Enabled = 0x1};
					enum {ValueMask_Enabled = 0x00100000};
					};
				namespace CH7LIMITL { // Field Description
					enum {Lsb = 21};
					enum {FieldMask = 0x00200000};
					enum {Value_Disabled = 0x0};
					enum {ValueMask_Disabled = 0x00000000};
					enum {Value_Enabled = 0x1};
					enum {ValueMask_Enabled = 0x00200000};
					};
				};
			namespace INTENSET { // Register description
				typedef uint32_t	Reg;
				enum {Set = 1};
				enum {Disabled = 0};
				enum {Enabled = 1};
				namespace STARTED { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x00000001};
					enum {Value_Set = 0x1};
					enum {ValueMask_Set = 0x00000001};
					enum {Value_Disabled = 0x0};
					enum {ValueMask_Disabled = 0x00000000};
					enum {Value_Enabled = 0x1};
					enum {ValueMask_Enabled = 0x00000001};
					};
				namespace END { // Field Description
					enum {Lsb = 1};
					enum {FieldMask = 0x00000002};
					enum {Value_Set = 0x1};
					enum {ValueMask_Set = 0x00000002};
					enum {Value_Disabled = 0x0};
					enum {ValueMask_Disabled = 0x00000000};
					enum {Value_Enabled = 0x1};
					enum {ValueMask_Enabled = 0x00000002};
					};
				namespace DONE { // Field Description
					enum {Lsb = 2};
					enum {FieldMask = 0x00000004};
					enum {Value_Set = 0x1};
					enum {ValueMask_Set = 0x00000004};
					enum {Value_Disabled = 0x0};
					enum {ValueMask_Disabled = 0x00000000};
					enum {Value_Enabled = 0x1};
					enum {ValueMask_Enabled = 0x00000004};
					};
				namespace RESULTDONE { // Field Description
					enum {Lsb = 3};
					enum {FieldMask = 0x00000008};
					enum {Value_Set = 0x1};
					enum {ValueMask_Set = 0x00000008};
					enum {Value_Disabled = 0x0};
					enum {ValueMask_Disabled = 0x00000000};
					enum {Value_Enabled = 0x1};
					enum {ValueMask_Enabled = 0x00000008};
					};
				namespace CALIBRATEDONE { // Field Description
					enum {Lsb = 4};
					enum {FieldMask = 0x00000010};
					enum {Value_Set = 0x1};
					enum {ValueMask_Set = 0x00000010};
					enum {Value_Disabled = 0x0};
					enum {ValueMask_Disabled = 0x00000000};
					enum {Value_Enabled = 0x1};
					enum {ValueMask_Enabled = 0x00000010};
					};
				namespace STOPPED { // Field Description
					enum {Lsb = 5};
					enum {FieldMask = 0x00000020};
					enum {Value_Set = 0x1};
					enum {ValueMask_Set = 0x00000020};
					enum {Value_Disabled = 0x0};
					enum {ValueMask_Disabled = 0x00000000};
					enum {Value_Enabled = 0x1};
					enum {ValueMask_Enabled = 0x00000020};
					};
				namespace CH0LIMITH { // Field Description
					enum {Lsb = 6};
					enum {FieldMask = 0x00000040};
					enum {Value_Set = 0x1};
					enum {ValueMask_Set = 0x00000040};
					enum {Value_Disabled = 0x0};
					enum {ValueMask_Disabled = 0x00000000};
					enum {Value_Enabled = 0x1};
					enum {ValueMask_Enabled = 0x00000040};
					};
				namespace CH0LIMITL { // Field Description
					enum {Lsb = 7};
					enum {FieldMask = 0x00000080};
					enum {Value_Set = 0x1};
					enum {ValueMask_Set = 0x00000080};
					enum {Value_Disabled = 0x0};
					enum {ValueMask_Disabled = 0x00000000};
					enum {Value_Enabled = 0x1};
					enum {ValueMask_Enabled = 0x00000080};
					};
				namespace CH1LIMITH { // Field Description
					enum {Lsb = 8};
					enum {FieldMask = 0x00000100};
					enum {Value_Set = 0x1};
					enum {ValueMask_Set = 0x00000100};
					enum {Value_Disabled = 0x0};
					enum {ValueMask_Disabled = 0x00000000};
					enum {Value_Enabled = 0x1};
					enum {ValueMask_Enabled = 0x00000100};
					};
				namespace CH1LIMITL { // Field Description
					enum {Lsb = 9};
					enum {FieldMask = 0x00000200};
					enum {Value_Set = 0x1};
					enum {ValueMask_Set = 0x00000200};
					enum {Value_Disabled = 0x0};
					enum {ValueMask_Disabled = 0x00000000};
					enum {Value_Enabled = 0x1};
					enum {ValueMask_Enabled = 0x00000200};
					};
				namespace CH2LIMITH { // Field Description
					enum {Lsb = 10};
					enum {FieldMask = 0x00000400};
					enum {Value_Set = 0x1};
					enum {ValueMask_Set = 0x00000400};
					enum {Value_Disabled = 0x0};
					enum {ValueMask_Disabled = 0x00000000};
					enum {Value_Enabled = 0x1};
					enum {ValueMask_Enabled = 0x00000400};
					};
				namespace CH2LIMITL { // Field Description
					enum {Lsb = 11};
					enum {FieldMask = 0x00000800};
					enum {Value_Set = 0x1};
					enum {ValueMask_Set = 0x00000800};
					enum {Value_Disabled = 0x0};
					enum {ValueMask_Disabled = 0x00000000};
					enum {Value_Enabled = 0x1};
					enum {ValueMask_Enabled = 0x00000800};
					};
				namespace CH3LIMITH { // Field Description
					enum {Lsb = 12};
					enum {FieldMask = 0x00001000};
					enum {Value_Set = 0x1};
					enum {ValueMask_Set = 0x00001000};
					enum {Value_Disabled = 0x0};
					enum {ValueMask_Disabled = 0x00000000};
					enum {Value_Enabled = 0x1};
					enum {ValueMask_Enabled = 0x00001000};
					};
				namespace CH3LIMITL { // Field Description
					enum {Lsb = 13};
					enum {FieldMask = 0x00002000};
					enum {Value_Set = 0x1};
					enum {ValueMask_Set = 0x00002000};
					enum {Value_Disabled = 0x0};
					enum {ValueMask_Disabled = 0x00000000};
					enum {Value_Enabled = 0x1};
					enum {ValueMask_Enabled = 0x00002000};
					};
				namespace CH4LIMITH { // Field Description
					enum {Lsb = 14};
					enum {FieldMask = 0x00004000};
					enum {Value_Set = 0x1};
					enum {ValueMask_Set = 0x00004000};
					enum {Value_Disabled = 0x0};
					enum {ValueMask_Disabled = 0x00000000};
					enum {Value_Enabled = 0x1};
					enum {ValueMask_Enabled = 0x00004000};
					};
				namespace CH4LIMITL { // Field Description
					enum {Lsb = 15};
					enum {FieldMask = 0x00008000};
					enum {Value_Set = 0x1};
					enum {ValueMask_Set = 0x00008000};
					enum {Value_Disabled = 0x0};
					enum {ValueMask_Disabled = 0x00000000};
					enum {Value_Enabled = 0x1};
					enum {ValueMask_Enabled = 0x00008000};
					};
				namespace CH5LIMITH { // Field Description
					enum {Lsb = 16};
					enum {FieldMask = 0x00010000};
					enum {Value_Set = 0x1};
					enum {ValueMask_Set = 0x00010000};
					enum {Value_Disabled = 0x0};
					enum {ValueMask_Disabled = 0x00000000};
					enum {Value_Enabled = 0x1};
					enum {ValueMask_Enabled = 0x00010000};
					};
				namespace CH5LIMITL { // Field Description
					enum {Lsb = 17};
					enum {FieldMask = 0x00020000};
					enum {Value_Set = 0x1};
					enum {ValueMask_Set = 0x00020000};
					enum {Value_Disabled = 0x0};
					enum {ValueMask_Disabled = 0x00000000};
					enum {Value_Enabled = 0x1};
					enum {ValueMask_Enabled = 0x00020000};
					};
				namespace CH6LIMITH { // Field Description
					enum {Lsb = 18};
					enum {FieldMask = 0x00040000};
					enum {Value_Set = 0x1};
					enum {ValueMask_Set = 0x00040000};
					enum {Value_Disabled = 0x0};
					enum {ValueMask_Disabled = 0x00000000};
					enum {Value_Enabled = 0x1};
					enum {ValueMask_Enabled = 0x00040000};
					};
				namespace CH6LIMITL { // Field Description
					enum {Lsb = 19};
					enum {FieldMask = 0x00080000};
					enum {Value_Set = 0x1};
					enum {ValueMask_Set = 0x00080000};
					enum {Value_Disabled = 0x0};
					enum {ValueMask_Disabled = 0x00000000};
					enum {Value_Enabled = 0x1};
					enum {ValueMask_Enabled = 0x00080000};
					};
				namespace CH7LIMITH { // Field Description
					enum {Lsb = 20};
					enum {FieldMask = 0x00100000};
					enum {Value_Set = 0x1};
					enum {ValueMask_Set = 0x00100000};
					enum {Value_Disabled = 0x0};
					enum {ValueMask_Disabled = 0x00000000};
					enum {Value_Enabled = 0x1};
					enum {ValueMask_Enabled = 0x00100000};
					};
				namespace CH7LIMITL { // Field Description
					enum {Lsb = 21};
					enum {FieldMask = 0x00200000};
					enum {Value_Set = 0x1};
					enum {ValueMask_Set = 0x00200000};
					enum {Value_Disabled = 0x0};
					enum {ValueMask_Disabled = 0x00000000};
					enum {Value_Enabled = 0x1};
					enum {ValueMask_Enabled = 0x00200000};
					};
				};
			namespace INTENCLR { // Register description
				typedef uint32_t	Reg;
				enum {Clear = 1};
				enum {Disabled = 0};
				enum {Enabled = 1};
				namespace STARTED { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x00000001};
					enum {Value_Clear = 0x1};
					enum {ValueMask_Clear = 0x00000001};
					enum {Value_Disabled = 0x0};
					enum {ValueMask_Disabled = 0x00000000};
					enum {Value_Enabled = 0x1};
					enum {ValueMask_Enabled = 0x00000001};
					};
				namespace END { // Field Description
					enum {Lsb = 1};
					enum {FieldMask = 0x00000002};
					enum {Value_Clear = 0x1};
					enum {ValueMask_Clear = 0x00000002};
					enum {Value_Disabled = 0x0};
					enum {ValueMask_Disabled = 0x00000000};
					enum {Value_Enabled = 0x1};
					enum {ValueMask_Enabled = 0x00000002};
					};
				namespace DONE { // Field Description
					enum {Lsb = 2};
					enum {FieldMask = 0x00000004};
					enum {Value_Clear = 0x1};
					enum {ValueMask_Clear = 0x00000004};
					enum {Value_Disabled = 0x0};
					enum {ValueMask_Disabled = 0x00000000};
					enum {Value_Enabled = 0x1};
					enum {ValueMask_Enabled = 0x00000004};
					};
				namespace RESULTDONE { // Field Description
					enum {Lsb = 3};
					enum {FieldMask = 0x00000008};
					enum {Value_Clear = 0x1};
					enum {ValueMask_Clear = 0x00000008};
					enum {Value_Disabled = 0x0};
					enum {ValueMask_Disabled = 0x00000000};
					enum {Value_Enabled = 0x1};
					enum {ValueMask_Enabled = 0x00000008};
					};
				namespace CALIBRATEDONE { // Field Description
					enum {Lsb = 4};
					enum {FieldMask = 0x00000010};
					enum {Value_Clear = 0x1};
					enum {ValueMask_Clear = 0x00000010};
					enum {Value_Disabled = 0x0};
					enum {ValueMask_Disabled = 0x00000000};
					enum {Value_Enabled = 0x1};
					enum {ValueMask_Enabled = 0x00000010};
					};
				namespace STOPPED { // Field Description
					enum {Lsb = 5};
					enum {FieldMask = 0x00000020};
					enum {Value_Clear = 0x1};
					enum {ValueMask_Clear = 0x00000020};
					enum {Value_Disabled = 0x0};
					enum {ValueMask_Disabled = 0x00000000};
					enum {Value_Enabled = 0x1};
					enum {ValueMask_Enabled = 0x00000020};
					};
				namespace CH0LIMITH { // Field Description
					enum {Lsb = 6};
					enum {FieldMask = 0x00000040};
					enum {Value_Clear = 0x1};
					enum {ValueMask_Clear = 0x00000040};
					enum {Value_Disabled = 0x0};
					enum {ValueMask_Disabled = 0x00000000};
					enum {Value_Enabled = 0x1};
					enum {ValueMask_Enabled = 0x00000040};
					};
				namespace CH0LIMITL { // Field Description
					enum {Lsb = 7};
					enum {FieldMask = 0x00000080};
					enum {Value_Clear = 0x1};
					enum {ValueMask_Clear = 0x00000080};
					enum {Value_Disabled = 0x0};
					enum {ValueMask_Disabled = 0x00000000};
					enum {Value_Enabled = 0x1};
					enum {ValueMask_Enabled = 0x00000080};
					};
				namespace CH1LIMITH { // Field Description
					enum {Lsb = 8};
					enum {FieldMask = 0x00000100};
					enum {Value_Clear = 0x1};
					enum {ValueMask_Clear = 0x00000100};
					enum {Value_Disabled = 0x0};
					enum {ValueMask_Disabled = 0x00000000};
					enum {Value_Enabled = 0x1};
					enum {ValueMask_Enabled = 0x00000100};
					};
				namespace CH1LIMITL { // Field Description
					enum {Lsb = 9};
					enum {FieldMask = 0x00000200};
					enum {Value_Clear = 0x1};
					enum {ValueMask_Clear = 0x00000200};
					enum {Value_Disabled = 0x0};
					enum {ValueMask_Disabled = 0x00000000};
					enum {Value_Enabled = 0x1};
					enum {ValueMask_Enabled = 0x00000200};
					};
				namespace CH2LIMITH { // Field Description
					enum {Lsb = 10};
					enum {FieldMask = 0x00000400};
					enum {Value_Clear = 0x1};
					enum {ValueMask_Clear = 0x00000400};
					enum {Value_Disabled = 0x0};
					enum {ValueMask_Disabled = 0x00000000};
					enum {Value_Enabled = 0x1};
					enum {ValueMask_Enabled = 0x00000400};
					};
				namespace CH2LIMITL { // Field Description
					enum {Lsb = 11};
					enum {FieldMask = 0x00000800};
					enum {Value_Clear = 0x1};
					enum {ValueMask_Clear = 0x00000800};
					enum {Value_Disabled = 0x0};
					enum {ValueMask_Disabled = 0x00000000};
					enum {Value_Enabled = 0x1};
					enum {ValueMask_Enabled = 0x00000800};
					};
				namespace CH3LIMITH { // Field Description
					enum {Lsb = 12};
					enum {FieldMask = 0x00001000};
					enum {Value_Clear = 0x1};
					enum {ValueMask_Clear = 0x00001000};
					enum {Value_Disabled = 0x0};
					enum {ValueMask_Disabled = 0x00000000};
					enum {Value_Enabled = 0x1};
					enum {ValueMask_Enabled = 0x00001000};
					};
				namespace CH3LIMITL { // Field Description
					enum {Lsb = 13};
					enum {FieldMask = 0x00002000};
					enum {Value_Clear = 0x1};
					enum {ValueMask_Clear = 0x00002000};
					enum {Value_Disabled = 0x0};
					enum {ValueMask_Disabled = 0x00000000};
					enum {Value_Enabled = 0x1};
					enum {ValueMask_Enabled = 0x00002000};
					};
				namespace CH4LIMITH { // Field Description
					enum {Lsb = 14};
					enum {FieldMask = 0x00004000};
					enum {Value_Clear = 0x1};
					enum {ValueMask_Clear = 0x00004000};
					enum {Value_Disabled = 0x0};
					enum {ValueMask_Disabled = 0x00000000};
					enum {Value_Enabled = 0x1};
					enum {ValueMask_Enabled = 0x00004000};
					};
				namespace CH4LIMITL { // Field Description
					enum {Lsb = 15};
					enum {FieldMask = 0x00008000};
					enum {Value_Clear = 0x1};
					enum {ValueMask_Clear = 0x00008000};
					enum {Value_Disabled = 0x0};
					enum {ValueMask_Disabled = 0x00000000};
					enum {Value_Enabled = 0x1};
					enum {ValueMask_Enabled = 0x00008000};
					};
				namespace CH5LIMITH { // Field Description
					enum {Lsb = 16};
					enum {FieldMask = 0x00010000};
					enum {Value_Clear = 0x1};
					enum {ValueMask_Clear = 0x00010000};
					enum {Value_Disabled = 0x0};
					enum {ValueMask_Disabled = 0x00000000};
					enum {Value_Enabled = 0x1};
					enum {ValueMask_Enabled = 0x00010000};
					};
				namespace CH5LIMITL { // Field Description
					enum {Lsb = 17};
					enum {FieldMask = 0x00020000};
					enum {Value_Clear = 0x1};
					enum {ValueMask_Clear = 0x00020000};
					enum {Value_Disabled = 0x0};
					enum {ValueMask_Disabled = 0x00000000};
					enum {Value_Enabled = 0x1};
					enum {ValueMask_Enabled = 0x00020000};
					};
				namespace CH6LIMITH { // Field Description
					enum {Lsb = 18};
					enum {FieldMask = 0x00040000};
					enum {Value_Clear = 0x1};
					enum {ValueMask_Clear = 0x00040000};
					enum {Value_Disabled = 0x0};
					enum {ValueMask_Disabled = 0x00000000};
					enum {Value_Enabled = 0x1};
					enum {ValueMask_Enabled = 0x00040000};
					};
				namespace CH6LIMITL { // Field Description
					enum {Lsb = 19};
					enum {FieldMask = 0x00080000};
					enum {Value_Clear = 0x1};
					enum {ValueMask_Clear = 0x00080000};
					enum {Value_Disabled = 0x0};
					enum {ValueMask_Disabled = 0x00000000};
					enum {Value_Enabled = 0x1};
					enum {ValueMask_Enabled = 0x00080000};
					};
				namespace CH7LIMITH { // Field Description
					enum {Lsb = 20};
					enum {FieldMask = 0x00100000};
					enum {Value_Clear = 0x1};
					enum {ValueMask_Clear = 0x00100000};
					enum {Value_Disabled = 0x0};
					enum {ValueMask_Disabled = 0x00000000};
					enum {Value_Enabled = 0x1};
					enum {ValueMask_Enabled = 0x00100000};
					};
				namespace CH7LIMITL { // Field Description
					enum {Lsb = 21};
					enum {FieldMask = 0x00200000};
					enum {Value_Clear = 0x1};
					enum {ValueMask_Clear = 0x00200000};
					enum {Value_Disabled = 0x0};
					enum {ValueMask_Disabled = 0x00000000};
					enum {Value_Enabled = 0x1};
					enum {ValueMask_Enabled = 0x00200000};
					};
				};
			namespace STATUS { // Register description
				typedef uint32_t	Reg;
				namespace STATUS { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x00000001};
					enum {Value_Ready = 0x0};
					enum {ValueMask_Ready = 0x00000000};
					enum {Value_Busy = 0x1};
					enum {ValueMask_Busy = 0x00000001};
					};
				};
			namespace ENABLE { // Register description
				typedef uint32_t	Reg;
				namespace ENABLE { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x00000001};
					enum {Value_Disabled = 0x0};
					enum {ValueMask_Disabled = 0x00000000};
					enum {Value_Enabled = 0x1};
					enum {ValueMask_Enabled = 0x00000001};
					};
				};
			namespace PSELP { // Register description
				typedef uint32_t	Reg;
				namespace PSELP { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x0000001F};
					enum {Value_NotConnected = 0x0};
					enum {ValueMask_NotConnected = 0x00000000};
					enum {Value_AIN0 = 0x1};
					enum {ValueMask_AIN0 = 0x00000001};
					enum {Value_AIN1 = 0x2};
					enum {ValueMask_AIN1 = 0x00000002};
					enum {Value_AIN2 = 0x3};
					enum {ValueMask_AIN2 = 0x00000003};
					enum {Value_AIN3 = 0x4};
					enum {ValueMask_AIN3 = 0x00000004};
					enum {Value_AIN4 = 0x5};
					enum {ValueMask_AIN4 = 0x00000005};
					enum {Value_AIN5 = 0x6};
					enum {ValueMask_AIN5 = 0x00000006};
					enum {Value_AIN6 = 0x7};
					enum {ValueMask_AIN6 = 0x00000007};
					enum {Value_AIN7 = 0x8};
					enum {ValueMask_AIN7 = 0x00000008};
					enum {Value_VDD = 0x9};
					enum {ValueMask_VDD = 0x00000009};
					enum {Value_VDDHDIV5 = 0xD};
					enum {ValueMask_VDDHDIV5 = 0x0000000D};
					};
				};
			namespace PSELN { // Register description
				typedef uint32_t	Reg;
				namespace PSELN { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x0000001F};
					enum {Value_NotConnected = 0x0};
					enum {ValueMask_NotConnected = 0x00000000};
					enum {Value_AIN0 = 0x1};
					enum {ValueMask_AIN0 = 0x00000001};
					enum {Value_AIN1 = 0x2};
					enum {ValueMask_AIN1 = 0x00000002};
					enum {Value_AIN2 = 0x3};
					enum {ValueMask_AIN2 = 0x00000003};
					enum {Value_AIN3 = 0x4};
					enum {ValueMask_AIN3 = 0x00000004};
					enum {Value_AIN4 = 0x5};
					enum {ValueMask_AIN4 = 0x00000005};
					enum {Value_AIN5 = 0x6};
					enum {ValueMask_AIN5 = 0x00000006};
					enum {Value_AIN6 = 0x7};
					enum {ValueMask_AIN6 = 0x00000007};
					enum {Value_AIN7 = 0x8};
					enum {ValueMask_AIN7 = 0x00000008};
					enum {Value_VDD = 0x9};
					enum {ValueMask_VDD = 0x00000009};
					enum {Value_VDDHDIV5 = 0xD};
					enum {ValueMask_VDDHDIV5 = 0x0000000D};
					};
				};
			namespace CONFIG { // Register description
				typedef uint32_t	Reg;
				namespace RESP { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x00000003};
					enum {Value_Bypass = 0x0};
					enum {ValueMask_Bypass = 0x00000000};
					enum {Value_Pulldown = 0x1};
					enum {ValueMask_Pulldown = 0x00000001};
					enum {Value_Pullup = 0x2};
					enum {ValueMask_Pullup = 0x00000002};
					enum {Value_VDD1_2 = 0x3};
					enum {ValueMask_VDD1_2 = 0x00000003};
					};
				namespace RESN { // Field Description
					enum {Lsb = 4};
					enum {FieldMask = 0x00000030};
					enum {Value_Bypass = 0x0};
					enum {ValueMask_Bypass = 0x00000000};
					enum {Value_Pulldown = 0x1};
					enum {ValueMask_Pulldown = 0x00000010};
					enum {Value_Pullup = 0x2};
					enum {ValueMask_Pullup = 0x00000020};
					enum {Value_VDD1_2 = 0x3};
					enum {ValueMask_VDD1_2 = 0x00000030};
					};
				namespace GAIN { // Field Description
					enum {Lsb = 8};
					enum {FieldMask = 0x00000700};
					enum {Value_Gain1_6 = 0x0};
					enum {ValueMask_Gain1_6 = 0x00000000};
					enum {Value_Gain1_5 = 0x1};
					enum {ValueMask_Gain1_5 = 0x00000100};
					enum {Value_Gain1_4 = 0x2};
					enum {ValueMask_Gain1_4 = 0x00000200};
					enum {Value_Gain1_3 = 0x3};
					enum {ValueMask_Gain1_3 = 0x00000300};
					enum {Value_Gain1_2 = 0x4};
					enum {ValueMask_Gain1_2 = 0x00000400};
					enum {Value_Gain1 = 0x5};
					enum {ValueMask_Gain1 = 0x00000500};
					enum {Value_Gain2 = 0x6};
					enum {ValueMask_Gain2 = 0x00000600};
					enum {Value_Gain4 = 0x7};
					enum {ValueMask_Gain4 = 0x00000700};
					};
				namespace REFSEL { // Field Description
					enum {Lsb = 12};
					enum {FieldMask = 0x00001000};
					enum {Value_Internal = 0x0};
					enum {ValueMask_Internal = 0x00000000};
					enum {Value_VDD1_4 = 0x1};
					enum {ValueMask_VDD1_4 = 0x00001000};
					};
				namespace TACQ { // Field Description
					enum {Lsb = 16};
					enum {FieldMask = 0x00070000};
					enum {Value_ACQ3us = 0x0};
					enum {ValueMask_ACQ3us = 0x00000000};
					enum {Value_ACQ5us = 0x1};
					enum {ValueMask_ACQ5us = 0x00010000};
					enum {Value_ACQ10us = 0x2};
					enum {ValueMask_ACQ10us = 0x00020000};
					enum {Value_ACQ15us = 0x3};
					enum {ValueMask_ACQ15us = 0x00030000};
					enum {Value_ACQ20us = 0x4};
					enum {ValueMask_ACQ20us = 0x00040000};
					enum {Value_ACQ40us = 0x5};
					enum {ValueMask_ACQ40us = 0x00050000};
					};
				namespace MODE { // Field Description
					enum {Lsb = 20};
					enum {FieldMask = 0x00100000};
					enum {Value_SE = 0x0};
					enum {ValueMask_SE = 0x00000000};
					enum {Value_Diff = 0x1};
					enum {ValueMask_Diff = 0x00100000};
					};
				namespace BURST { // Field Description
					enum {Lsb = 24};
					enum {FieldMask = 0x01000000};
					enum {Value_Disabled = 0x0};
					enum {ValueMask_Disabled = 0x00000000};
					enum {Value_Enabled = 0x1};
					enum {ValueMask_Enabled = 0x01000000};
					};
				};
			namespace LIMIT { // Register description
				typedef uint32_t	Reg;
				namespace LOW { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x0000FFFF};
					enum {Value_Min = 0x8000};
					enum {ValueMask_Min = 0x00008000};
					enum {Value_Default = 0x0};
					enum {ValueMask_Default = 0x00000000};
					enum {Value_Max = 0x7FFF};
					enum {ValueMask_Max = 0x00007FFF};
					};
				namespace HIGH { // Field Description
					enum {Lsb = 16};
					enum {FieldMask = 0xFFFF0000};
					enum {Value_Min = 0x8000};
					enum {ValueMask_Min = 0x80000000};
					enum {Value_Default = 0x7FFF};
					enum {ValueMask_Default = 0x7FFF0000};
					enum {Value_Max = 0x7FFF};
					enum {ValueMask_Max = 0x7FFF0000};
					};
				};
			namespace RESOLUTION { // Register description
				typedef uint32_t	Reg;
				namespace VAL { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x00000007};
					enum {Value_Val8bit = 0x0};
					enum {ValueMask_Val8bit = 0x00000000};
					enum {Value_Val10bit = 0x1};
					enum {ValueMask_Val10bit = 0x00000001};
					enum {Value_Val12bit = 0x2};
					enum {ValueMask_Val12bit = 0x00000002};
					enum {Value_Val14bit = 0x3};
					enum {ValueMask_Val14bit = 0x00000003};
					};
				};
			namespace OVERSAMPLE { // Register description
				typedef uint32_t	Reg;
				namespace OVERSAMPLE { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x0000000F};
					enum {Value_Bypass = 0x0};
					enum {ValueMask_Bypass = 0x00000000};
					enum {Value_Over2x = 0x1};
					enum {ValueMask_Over2x = 0x00000001};
					enum {Value_Over4x = 0x2};
					enum {ValueMask_Over4x = 0x00000002};
					enum {Value_Over8x = 0x3};
					enum {ValueMask_Over8x = 0x00000003};
					enum {Value_Over16x = 0x4};
					enum {ValueMask_Over16x = 0x00000004};
					enum {Value_Over32x = 0x5};
					enum {ValueMask_Over32x = 0x00000005};
					enum {Value_Over64x = 0x6};
					enum {ValueMask_Over64x = 0x00000006};
					enum {Value_Over128x = 0x7};
					enum {ValueMask_Over128x = 0x00000007};
					enum {Value_Over256x = 0x8};
					enum {ValueMask_Over256x = 0x00000008};
					};
				};
			namespace SAMPLERATE { // Register description
				typedef uint32_t	Reg;
				namespace CC { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x000007FF};
					enum {Value_Default = 0x0};
					enum {ValueMask_Default = 0x00000000};
					enum {Value_Min = 0x50};
					enum {ValueMask_Min = 0x00000050};
					enum {Value_Max = 0x7FF};
					enum {ValueMask_Max = 0x000007FF};
					};
				namespace MODE { // Field Description
					enum {Lsb = 12};
					enum {FieldMask = 0x00001000};
					enum {Value_Task = 0x0};
					enum {ValueMask_Task = 0x00000000};
					enum {Value_Timers = 0x1};
					enum {ValueMask_Timers = 0x00001000};
					};
				};
			namespace RESULTPTR { // Register description
				typedef uint32_t	Reg;
				};
			namespace RESULTMAXCNT { // Register description
				typedef uint32_t	Reg;
				namespace MAXCNT { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x00007FFF};
					enum {Value_Max = 0x7FFF};
					enum {ValueMask_Max = 0x00007FFF};
					};
				};
			namespace RESULTAMOUNT { // Register description
				typedef uint32_t	Reg;
				namespace AMOUNT { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x00007FFF};
					enum {Value_Max = 0x7FFF};
					enum {ValueMask_Max = 0x00007FFF};
					};
				};
			}
		}
	}
#endif
