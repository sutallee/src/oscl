/*
   Copyright (C) 2020 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_hw_nordic_clock_maph_
#define _oscl_hw_nordic_clock_maph_

#include "reg.h"

/** */
namespace Oscl {

/** */
namespace Nordic {

/** */
namespace CLOCK {

/** */
struct Map {
	/** Offset 0x000 */
	volatile Oscl::Nordic::CLOCK::TASKS_HFCLKSTART::Reg	tasks_hfclkstart;

	/** Offset 0x004 */
	volatile Oscl::Nordic::CLOCK::TASKS_HFCLKSTOP::Reg	tasks_hfclkstop;

	/** Offset 0x008 */
	volatile Oscl::Nordic::CLOCK::TASKS_LFCLKSTART::Reg	tasks_lfclkstart;

	/** Offset 0x00C */
	volatile Oscl::Nordic::CLOCK::TASKS_LFCLKSTOP::Reg	tasks_lfclkstop;

	/** Offset 0x010 */
	volatile Oscl::Nordic::CLOCK::TASKS_CAL::Reg	tasks_cal;

	/** Offset 0x014 */
	volatile Oscl::Nordic::CLOCK::TASKS_CTSTART::Reg	tasks_ctstart;

	/** Offset 0x018 */
	volatile Oscl::Nordic::CLOCK::TASKS_CTSTOP::Reg	tasks_ctstop;

	uint32_t		unused0x01C[(0x100-0x01C)/4];

	/** Offset 0x100 */
	volatile Oscl::Nordic::CLOCK::EVENTS_HFCLKSTARTED::Reg	events_hfclkstarted;

	/** Offset 0x104 */
	volatile Oscl::Nordic::CLOCK::EVENTS_LFCLKSTARTED::Reg	events_lfclkstarted;

	uint32_t		unused0x108[(0x10C-0x108)/4];

	/** Offset 0x10C */
	volatile Oscl::Nordic::CLOCK::EVENTS_DONE::Reg	events_done;

	/** Offset 0x110 */
	volatile Oscl::Nordic::CLOCK::EVENTS_CTO::Reg	events_cto;

	uint32_t		unused0x114[(0x128-0x114)/4];

	/** Offset 0x128 */
	volatile Oscl::Nordic::CLOCK::EVENTS_CTSTARTED::Reg	events_ctstarted;

	/** Offset 0x12C */
	volatile Oscl::Nordic::CLOCK::EVENTS_CTSTOPPED::Reg	events_ctstopped;

	uint32_t		unused0x130[(0x304-0x130)/4];

	/** Offset 0x304 */
	volatile Oscl::Nordic::CLOCK::INTENSET::Reg	intenset;

	/** Offset 0x308 */
	volatile Oscl::Nordic::CLOCK::INTENCLR::Reg	intenclr;

	uint32_t		unused0x30C[(0x408-0x30C)/4];

	/** Offset 0x408 */
	volatile Oscl::Nordic::CLOCK::HFCLKRUN::Reg	hfclkrun;

	/** Offset 0x40C */
	volatile Oscl::Nordic::CLOCK::HFCLKSTAT::Reg	hfclkstat;

	uint32_t		unused0x410[(0x414-0x410)/4];

	/** Offset 0x414 */
	volatile Oscl::Nordic::CLOCK::LFCLKRUN::Reg	lfclkrun;

	/** Offset 0x418 */
	volatile Oscl::Nordic::CLOCK::LFCLKSTAT::Reg	lfclkstat;

	/** Offset 0x41C */
	volatile Oscl::Nordic::CLOCK::LFCLKSRCCOPY::Reg	lfclksrccopy;

	uint32_t		unused0x420[(0x518-0x420)/4];

	/** Offset 0x518 */
	volatile Oscl::Nordic::CLOCK::LFCLKSRC::Reg	lfclksrc;

	uint32_t		unused0x51C[(0x528-0x51C)/4];

	/** Offset 0x528 */
	volatile Oscl::Nordic::CLOCK::HFXODEBOUNCE::Reg	hfxodebounce;

	uint32_t		unused0x52C[(0x538-0x52C)/4];

	/** Offset 0x538 */
	volatile Oscl::Nordic::CLOCK::CTIV::Reg	ctiv;

	uint32_t		unused0x53C[(0x55C-0x53C)/4];

	/** Offset 0x55C */
	volatile Oscl::Nordic::CLOCK::TRACECONFIG::Reg	traceconfig;

	uint32_t		unused0x560[(0x5B4-0x560)/4];

	/** Offset 0x5B4 */
	volatile Oscl::Nordic::CLOCK::LFRCMODE::Reg	lfrcmode;
	};

}
}
}

#endif
