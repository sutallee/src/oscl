#ifndef _oscl_hw_nordic_clock_regh_
#define _oscl_hw_nordic_clock_regh_
#include <stdint.h>
namespace Oscl { // Namespace description
	namespace Nordic { // Namespace description
		namespace CLOCK { // Namespace description
			namespace TASKS_HFCLKSTART { // Register description
				typedef uint32_t	Reg;
				namespace START { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x00000001};
					enum {Value_Trigger = 0x1};
					enum {ValueMask_Trigger = 0x00000001};
					};
				};
			namespace TASKS_HFCLKSTOP { // Register description
				typedef uint32_t	Reg;
				namespace STOP { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x00000001};
					enum {Value_Trigger = 0x1};
					enum {ValueMask_Trigger = 0x00000001};
					};
				};
			namespace TASKS_LFCLKSTART { // Register description
				typedef uint32_t	Reg;
				namespace START { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x00000001};
					enum {Value_Trigger = 0x1};
					enum {ValueMask_Trigger = 0x00000001};
					};
				};
			namespace TASKS_LFCLKSTOP { // Register description
				typedef uint32_t	Reg;
				namespace STOP { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x00000001};
					enum {Value_Trigger = 0x1};
					enum {ValueMask_Trigger = 0x00000001};
					};
				};
			namespace TASKS_CAL { // Register description
				typedef uint32_t	Reg;
				namespace START { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x00000001};
					enum {Value_Trigger = 0x1};
					enum {ValueMask_Trigger = 0x00000001};
					};
				};
			namespace TASKS_CTSTART { // Register description
				typedef uint32_t	Reg;
				namespace START { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x00000001};
					enum {Value_Trigger = 0x1};
					enum {ValueMask_Trigger = 0x00000001};
					};
				};
			namespace TASKS_CTSTOP { // Register description
				typedef uint32_t	Reg;
				namespace STOP { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x00000001};
					enum {Value_Trigger = 0x1};
					enum {ValueMask_Trigger = 0x00000001};
					};
				};
			namespace EVENTS_HFCLKSTARTED { // Register description
				typedef uint32_t	Reg;
				namespace EVENT { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x00000001};
					enum {Value_NoteGenerated = 0x0};
					enum {ValueMask_NoteGenerated = 0x00000000};
					enum {Value_Generated = 0x1};
					enum {ValueMask_Generated = 0x00000001};
					};
				};
			namespace EVENTS_LFCLKSTARTED { // Register description
				typedef uint32_t	Reg;
				namespace EVENT { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x00000001};
					enum {Value_NoteGenerated = 0x0};
					enum {ValueMask_NoteGenerated = 0x00000000};
					enum {Value_Generated = 0x1};
					enum {ValueMask_Generated = 0x00000001};
					};
				};
			namespace EVENTS_DONE { // Register description
				typedef uint32_t	Reg;
				namespace EVENT { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x00000001};
					enum {Value_NoteGenerated = 0x0};
					enum {ValueMask_NoteGenerated = 0x00000000};
					enum {Value_Generated = 0x1};
					enum {ValueMask_Generated = 0x00000001};
					};
				};
			namespace EVENTS_CTO { // Register description
				typedef uint32_t	Reg;
				namespace EVENT { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x00000001};
					enum {Value_NoteGenerated = 0x0};
					enum {ValueMask_NoteGenerated = 0x00000000};
					enum {Value_Generated = 0x1};
					enum {ValueMask_Generated = 0x00000001};
					};
				};
			namespace EVENTS_CTSTARTED { // Register description
				typedef uint32_t	Reg;
				namespace EVENT { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x00000001};
					enum {Value_NoteGenerated = 0x0};
					enum {ValueMask_NoteGenerated = 0x00000000};
					enum {Value_Generated = 0x1};
					enum {ValueMask_Generated = 0x00000001};
					};
				};
			namespace EVENTS_CTSTOPPED { // Register description
				typedef uint32_t	Reg;
				namespace EVENT { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x00000001};
					enum {Value_NoteGenerated = 0x0};
					enum {ValueMask_NoteGenerated = 0x00000000};
					enum {Value_Generated = 0x1};
					enum {ValueMask_Generated = 0x00000001};
					};
				};
			namespace INTENSET { // Register description
				typedef uint32_t	Reg;
				namespace HFCLKSTARTED { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x00000001};
					enum {Value_Disabled = 0x0};
					enum {ValueMask_Disabled = 0x00000000};
					enum {Value_Enabled = 0x1};
					enum {ValueMask_Enabled = 0x00000001};
					enum {Value_Set = 0x1};
					enum {ValueMask_Set = 0x00000001};
					};
				namespace LFCLKSTARTED { // Field Description
					enum {Lsb = 1};
					enum {FieldMask = 0x00000002};
					enum {Value_Disabled = 0x0};
					enum {ValueMask_Disabled = 0x00000000};
					enum {Value_Enabled = 0x1};
					enum {ValueMask_Enabled = 0x00000002};
					enum {Value_Set = 0x1};
					enum {ValueMask_Set = 0x00000002};
					};
				namespace DONE { // Field Description
					enum {Lsb = 3};
					enum {FieldMask = 0x00000008};
					enum {Value_Disabled = 0x0};
					enum {ValueMask_Disabled = 0x00000000};
					enum {Value_Enabled = 0x1};
					enum {ValueMask_Enabled = 0x00000008};
					enum {Value_Set = 0x1};
					enum {ValueMask_Set = 0x00000008};
					};
				namespace CTO { // Field Description
					enum {Lsb = 4};
					enum {FieldMask = 0x00000010};
					enum {Value_Disabled = 0x0};
					enum {ValueMask_Disabled = 0x00000000};
					enum {Value_Enabled = 0x1};
					enum {ValueMask_Enabled = 0x00000010};
					enum {Value_Set = 0x1};
					enum {ValueMask_Set = 0x00000010};
					};
				namespace CTSTARTED { // Field Description
					enum {Lsb = 10};
					enum {FieldMask = 0x00000400};
					enum {Value_Disabled = 0x0};
					enum {ValueMask_Disabled = 0x00000000};
					enum {Value_Enabled = 0x1};
					enum {ValueMask_Enabled = 0x00000400};
					enum {Value_Set = 0x1};
					enum {ValueMask_Set = 0x00000400};
					};
				namespace CTSTOPPED { // Field Description
					enum {Lsb = 11};
					enum {FieldMask = 0x00000800};
					enum {Value_Disabled = 0x0};
					enum {ValueMask_Disabled = 0x00000000};
					enum {Value_Enabled = 0x1};
					enum {ValueMask_Enabled = 0x00000800};
					enum {Value_Set = 0x1};
					enum {ValueMask_Set = 0x00000800};
					};
				};
			namespace INTENCLR { // Register description
				typedef uint32_t	Reg;
				namespace HFCLKSTARTED { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x00000001};
					enum {Value_Disabled = 0x0};
					enum {ValueMask_Disabled = 0x00000000};
					enum {Value_Enabled = 0x1};
					enum {ValueMask_Enabled = 0x00000001};
					enum {Value_Clear = 0x1};
					enum {ValueMask_Clear = 0x00000001};
					};
				namespace LFCLKSTARTED { // Field Description
					enum {Lsb = 1};
					enum {FieldMask = 0x00000002};
					enum {Value_Disabled = 0x0};
					enum {ValueMask_Disabled = 0x00000000};
					enum {Value_Enabled = 0x1};
					enum {ValueMask_Enabled = 0x00000002};
					enum {Value_Clear = 0x1};
					enum {ValueMask_Clear = 0x00000002};
					};
				namespace DONE { // Field Description
					enum {Lsb = 3};
					enum {FieldMask = 0x00000008};
					enum {Value_Disabled = 0x0};
					enum {ValueMask_Disabled = 0x00000000};
					enum {Value_Enabled = 0x1};
					enum {ValueMask_Enabled = 0x00000008};
					enum {Value_Clear = 0x1};
					enum {ValueMask_Clear = 0x00000008};
					};
				namespace CTO { // Field Description
					enum {Lsb = 4};
					enum {FieldMask = 0x00000010};
					enum {Value_Disabled = 0x0};
					enum {ValueMask_Disabled = 0x00000000};
					enum {Value_Enabled = 0x1};
					enum {ValueMask_Enabled = 0x00000010};
					enum {Value_Clear = 0x1};
					enum {ValueMask_Clear = 0x00000010};
					};
				namespace CTSTARTED { // Field Description
					enum {Lsb = 10};
					enum {FieldMask = 0x00000400};
					enum {Value_Disabled = 0x0};
					enum {ValueMask_Disabled = 0x00000000};
					enum {Value_Enabled = 0x1};
					enum {ValueMask_Enabled = 0x00000400};
					enum {Value_Clear = 0x1};
					enum {ValueMask_Clear = 0x00000400};
					};
				namespace CTSTOPPED { // Field Description
					enum {Lsb = 11};
					enum {FieldMask = 0x00000800};
					enum {Value_Disabled = 0x0};
					enum {ValueMask_Disabled = 0x00000000};
					enum {Value_Enabled = 0x1};
					enum {ValueMask_Enabled = 0x00000800};
					enum {Value_Clear = 0x1};
					enum {ValueMask_Clear = 0x00000800};
					};
				};
			namespace HFCLKRUN { // Register description
				typedef uint32_t	Reg;
				namespace STATUS { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x00000001};
					enum {Value_NotTriggered = 0x0};
					enum {ValueMask_NotTriggered = 0x00000000};
					enum {Value_Triggered = 0x1};
					enum {ValueMask_Triggered = 0x00000001};
					};
				};
			namespace HFCLKSTAT { // Register description
				typedef uint32_t	Reg;
				namespace SRC { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x00000001};
					enum {Value_RC = 0x0};
					enum {ValueMask_RC = 0x00000000};
					enum {Value_Xtal = 0x1};
					enum {ValueMask_Xtal = 0x00000001};
					};
				namespace STATE { // Field Description
					enum {Lsb = 16};
					enum {FieldMask = 0x00010000};
					enum {Value_NotRunning = 0x0};
					enum {ValueMask_NotRunning = 0x00000000};
					enum {Value_Running = 0x1};
					enum {ValueMask_Running = 0x00010000};
					};
				};
			namespace LFCLKRUN { // Register description
				typedef uint32_t	Reg;
				namespace STATUS { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x00000001};
					enum {Value_NotTriggered = 0x0};
					enum {ValueMask_NotTriggered = 0x00000000};
					enum {Value_Triggered = 0x1};
					enum {ValueMask_Triggered = 0x00000001};
					};
				};
			namespace LFCLKSTAT { // Register description
				typedef uint32_t	Reg;
				namespace SRC { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x00000003};
					enum {Value_RC = 0x0};
					enum {ValueMask_RC = 0x00000000};
					enum {Value_Xtal = 0x1};
					enum {ValueMask_Xtal = 0x00000001};
					enum {Value_Synth = 0x2};
					enum {ValueMask_Synth = 0x00000002};
					};
				namespace STATE { // Field Description
					enum {Lsb = 16};
					enum {FieldMask = 0x00010000};
					enum {Value_NotRunning = 0x0};
					enum {ValueMask_NotRunning = 0x00000000};
					enum {Value_Running = 0x1};
					enum {ValueMask_Running = 0x00010000};
					};
				};
			namespace LFCLKSRCCOPY { // Register description
				typedef uint32_t	Reg;
				namespace SRC { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x00000003};
					enum {Value_RC = 0x0};
					enum {ValueMask_RC = 0x00000000};
					enum {Value_Xtal = 0x1};
					enum {ValueMask_Xtal = 0x00000001};
					enum {Value_Synth = 0x2};
					enum {ValueMask_Synth = 0x00000002};
					};
				};
			namespace LFCLKSRC { // Register description
				typedef uint32_t	Reg;
				namespace SRC { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x00000003};
					enum {Value_RC = 0x0};
					enum {ValueMask_RC = 0x00000000};
					enum {Value_Xtal = 0x1};
					enum {ValueMask_Xtal = 0x00000001};
					enum {Value_Synth = 0x2};
					enum {ValueMask_Synth = 0x00000002};
					};
				namespace BYPASS { // Field Description
					enum {Lsb = 16};
					enum {FieldMask = 0x00010000};
					enum {Value_Disabled = 0x0};
					enum {ValueMask_Disabled = 0x00000000};
					enum {Value_Enabled = 0x1};
					enum {ValueMask_Enabled = 0x00010000};
					};
				namespace EXTERNAL { // Field Description
					enum {Lsb = 17};
					enum {FieldMask = 0x00020000};
					enum {Value_Disabled = 0x0};
					enum {ValueMask_Disabled = 0x00000000};
					enum {Value_Enabled = 0x1};
					enum {ValueMask_Enabled = 0x00020000};
					};
				};
			namespace HFXODEBOUNCE { // Register description
				typedef uint32_t	Reg;
				namespace TIME { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x000000FF};
					enum {Value_Dbx16usFirst = 0x1};
					enum {ValueMask_Dbx16usFirst = 0x00000001};
					enum {Value_Dbx16usLast = 0xFF};
					enum {ValueMask_Dbx16usLast = 0x000000FF};
					enum {Value_DbDefault = 0x10};
					enum {ValueMask_DbDefault = 0x00000010};
					enum {Value_Db16us = 0x1};
					enum {ValueMask_Db16us = 0x00000001};
					enum {Value_Db256us = 0x10};
					enum {ValueMask_Db256us = 0x00000010};
					enum {Value_Db1024us = 0x40};
					enum {ValueMask_Db1024us = 0x00000040};
					};
				};
			namespace CTIV { // Register description
				typedef uint32_t	Reg;
				namespace INTERVAL { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x0000007F};
					enum {Value_Invalid = 0x0};
					enum {ValueMask_Invalid = 0x00000000};
					enum {Value_First250ms = 0x1};
					enum {ValueMask_First250ms = 0x00000001};
					enum {Value_Last31p75s = 0x7F};
					enum {ValueMask_Last31p75s = 0x0000007F};
					};
				};
			namespace TRACECONFIG { // Register description
				typedef uint32_t	Reg;
				namespace TRACEPORTSPEED { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x00000003};
					enum {Value_f32MHz = 0x0};
					enum {ValueMask_f32MHz = 0x00000000};
					enum {Value_f16MHz = 0x1};
					enum {ValueMask_f16MHz = 0x00000001};
					enum {Value_f8MHz = 0x2};
					enum {ValueMask_f8MHz = 0x00000002};
					enum {Value_f4MHz = 0x3};
					enum {ValueMask_f4MHz = 0x00000003};
					};
				namespace TRACEMUX { // Field Description
					enum {Lsb = 16};
					enum {FieldMask = 0x00030000};
					enum {Value_GPIO = 0x0};
					enum {ValueMask_GPIO = 0x00000000};
					enum {Value_Serial = 0x1};
					enum {ValueMask_Serial = 0x00010000};
					enum {Value_Parallel = 0x2};
					enum {ValueMask_Parallel = 0x00020000};
					};
				};
			namespace LFRCMODE { // Register description
				typedef uint32_t	Reg;
				namespace MODE { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x00000001};
					enum {Value_Normal = 0x0};
					enum {ValueMask_Normal = 0x00000000};
					enum {Value_ULP = 0x1};
					enum {ValueMask_ULP = 0x00000001};
					};
				namespace STATUS { // Field Description
					enum {Lsb = 16};
					enum {FieldMask = 0x00010000};
					enum {Value_Normal = 0x0};
					enum {ValueMask_Normal = 0x00000000};
					enum {Value_ULP = 0x1};
					enum {ValueMask_ULP = 0x00010000};
					};
				};
			}
		}
	}
#endif
