#ifndef _oscl_hw_nordic_uarte_regh_
#define _oscl_hw_nordic_uarte_regh_
#include <stdint.h>
namespace Oscl { // Namespace description
	namespace Nordic { // Namespace description
		namespace UARTE { // Namespace description
			namespace TASKS_STARTRX { // Register description
				typedef uint32_t	Reg;
				namespace STARTRX { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x00000001};
					enum {Value_Trigger = 0x1};
					enum {ValueMask_Trigger = 0x00000001};
					};
				};
			namespace TASKS_STOPRX { // Register description
				typedef uint32_t	Reg;
				namespace STOPRX { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x00000001};
					enum {Value_Trigger = 0x1};
					enum {ValueMask_Trigger = 0x00000001};
					};
				};
			namespace TASKS_STARTTX { // Register description
				typedef uint32_t	Reg;
				namespace STARTTX { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x00000001};
					enum {Value_Trigger = 0x1};
					enum {ValueMask_Trigger = 0x00000001};
					};
				};
			namespace TASKS_STOPTX { // Register description
				typedef uint32_t	Reg;
				namespace STOPTX { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x00000001};
					enum {Value_Trigger = 0x1};
					enum {ValueMask_Trigger = 0x00000001};
					};
				};
			namespace TASKS_FLUSHRX { // Register description
				typedef uint32_t	Reg;
				namespace FLUSHRX { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x00000001};
					enum {Value_Trigger = 0x1};
					enum {ValueMask_Trigger = 0x00000001};
					};
				};
			namespace EVENTS_CTS { // Register description
				typedef uint32_t	Reg;
				namespace CTS { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x00000001};
					enum {Value_NotGenerated = 0x0};
					enum {ValueMask_NotGenerated = 0x00000000};
					enum {Value_Generated = 0x1};
					enum {ValueMask_Generated = 0x00000001};
					};
				};
			namespace EVENTS_NCTS { // Register description
				typedef uint32_t	Reg;
				namespace NCTS { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x00000001};
					enum {Value_NotGenerated = 0x0};
					enum {ValueMask_NotGenerated = 0x00000000};
					enum {Value_Generated = 0x1};
					enum {ValueMask_Generated = 0x00000001};
					};
				};
			namespace EVENTS_RXDRDY { // Register description
				typedef uint32_t	Reg;
				namespace RXRDY { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x00000001};
					enum {Value_NotGenerated = 0x0};
					enum {ValueMask_NotGenerated = 0x00000000};
					enum {Value_Generated = 0x1};
					enum {ValueMask_Generated = 0x00000001};
					};
				};
			namespace EVENTS_ENDRX { // Register description
				typedef uint32_t	Reg;
				namespace ENDRX { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x00000001};
					enum {Value_NotGenerated = 0x0};
					enum {ValueMask_NotGenerated = 0x00000000};
					enum {Value_Generated = 0x1};
					enum {ValueMask_Generated = 0x00000001};
					};
				};
			namespace EVENTS_TXDRDY { // Register description
				typedef uint32_t	Reg;
				namespace TXRDY { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x00000001};
					enum {Value_NotGenerated = 0x0};
					enum {ValueMask_NotGenerated = 0x00000000};
					enum {Value_Generated = 0x1};
					enum {ValueMask_Generated = 0x00000001};
					};
				};
			namespace EVENTS_ENDTX { // Register description
				typedef uint32_t	Reg;
				namespace ENDTX { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x00000001};
					enum {Value_NotGenerated = 0x0};
					enum {ValueMask_NotGenerated = 0x00000000};
					enum {Value_Generated = 0x1};
					enum {ValueMask_Generated = 0x00000001};
					};
				};
			namespace EVENTS_ERROR { // Register description
				typedef uint32_t	Reg;
				namespace ERROR { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x00000001};
					enum {Value_NotGenerated = 0x0};
					enum {ValueMask_NotGenerated = 0x00000000};
					enum {Value_Generated = 0x1};
					enum {ValueMask_Generated = 0x00000001};
					};
				};
			namespace EVENTS_RXTO { // Register description
				typedef uint32_t	Reg;
				namespace RXTO { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x00000001};
					enum {Value_NotGenerated = 0x0};
					enum {ValueMask_NotGenerated = 0x00000000};
					enum {Value_Generated = 0x1};
					enum {ValueMask_Generated = 0x00000001};
					};
				};
			namespace EVENTS_RXSTARTED { // Register description
				typedef uint32_t	Reg;
				namespace RXSTARTED { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x00000001};
					enum {Value_NotGenerated = 0x0};
					enum {ValueMask_NotGenerated = 0x00000000};
					enum {Value_Generated = 0x1};
					enum {ValueMask_Generated = 0x00000001};
					};
				};
			namespace EVENTS_TXSTARTED { // Register description
				typedef uint32_t	Reg;
				namespace TXSTARTED { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x00000001};
					enum {Value_NotGenerated = 0x0};
					enum {ValueMask_NotGenerated = 0x00000000};
					enum {Value_Generated = 0x1};
					enum {ValueMask_Generated = 0x00000001};
					};
				};
			namespace EVENTS_TXSTOPPED { // Register description
				typedef uint32_t	Reg;
				namespace TXSTOPPED { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x00000001};
					enum {Value_NotGenerated = 0x0};
					enum {ValueMask_NotGenerated = 0x00000000};
					enum {Value_Generated = 0x1};
					enum {ValueMask_Generated = 0x00000001};
					};
				};
			namespace SHORTS { // Register description
				typedef uint32_t	Reg;
				namespace STARTRX { // Field Description
					enum {Lsb = 5};
					enum {FieldMask = 0x00000020};
					enum {Value_Disabled = 0x0};
					enum {ValueMask_Disabled = 0x00000000};
					enum {Value_Enable = 0x1};
					enum {ValueMask_Enable = 0x00000020};
					};
				namespace STOPRX { // Field Description
					enum {Lsb = 6};
					enum {FieldMask = 0x00000040};
					enum {Value_Disabled = 0x0};
					enum {ValueMask_Disabled = 0x00000000};
					enum {Value_Enable = 0x1};
					enum {ValueMask_Enable = 0x00000040};
					};
				};
			namespace INTEN { // Register description
				typedef uint32_t	Reg;
				namespace CTS { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x00000001};
					enum {Value_Disabled = 0x0};
					enum {ValueMask_Disabled = 0x00000000};
					enum {Value_Enable = 0x1};
					enum {ValueMask_Enable = 0x00000001};
					};
				namespace NCTS { // Field Description
					enum {Lsb = 1};
					enum {FieldMask = 0x00000002};
					enum {Value_Disabled = 0x0};
					enum {ValueMask_Disabled = 0x00000000};
					enum {Value_Enable = 0x1};
					enum {ValueMask_Enable = 0x00000002};
					};
				namespace RXRDY { // Field Description
					enum {Lsb = 2};
					enum {FieldMask = 0x00000004};
					enum {Value_Disabled = 0x0};
					enum {ValueMask_Disabled = 0x00000000};
					enum {Value_Enable = 0x1};
					enum {ValueMask_Enable = 0x00000004};
					};
				namespace ENDRX { // Field Description
					enum {Lsb = 4};
					enum {FieldMask = 0x00000010};
					enum {Value_Disabled = 0x0};
					enum {ValueMask_Disabled = 0x00000000};
					enum {Value_Enable = 0x1};
					enum {ValueMask_Enable = 0x00000010};
					};
				namespace TXRDY { // Field Description
					enum {Lsb = 7};
					enum {FieldMask = 0x00000080};
					enum {Value_Disabled = 0x0};
					enum {ValueMask_Disabled = 0x00000000};
					enum {Value_Enable = 0x1};
					enum {ValueMask_Enable = 0x00000080};
					};
				namespace ENDTX { // Field Description
					enum {Lsb = 8};
					enum {FieldMask = 0x00000100};
					enum {Value_Disabled = 0x0};
					enum {ValueMask_Disabled = 0x00000000};
					enum {Value_Enable = 0x1};
					enum {ValueMask_Enable = 0x00000100};
					};
				namespace ERROR { // Field Description
					enum {Lsb = 9};
					enum {FieldMask = 0x00000200};
					enum {Value_Disabled = 0x0};
					enum {ValueMask_Disabled = 0x00000000};
					enum {Value_Enable = 0x1};
					enum {ValueMask_Enable = 0x00000200};
					};
				namespace RXTO { // Field Description
					enum {Lsb = 17};
					enum {FieldMask = 0x00020000};
					enum {Value_Disabled = 0x0};
					enum {ValueMask_Disabled = 0x00000000};
					enum {Value_Enable = 0x1};
					enum {ValueMask_Enable = 0x00020000};
					};
				namespace RXSTARTED { // Field Description
					enum {Lsb = 19};
					enum {FieldMask = 0x00080000};
					enum {Value_Disabled = 0x0};
					enum {ValueMask_Disabled = 0x00000000};
					enum {Value_Enable = 0x1};
					enum {ValueMask_Enable = 0x00080000};
					};
				namespace TXSTARTED { // Field Description
					enum {Lsb = 20};
					enum {FieldMask = 0x00100000};
					enum {Value_Disabled = 0x0};
					enum {ValueMask_Disabled = 0x00000000};
					enum {Value_Enable = 0x1};
					enum {ValueMask_Enable = 0x00100000};
					};
				namespace TXSTOPPED { // Field Description
					enum {Lsb = 22};
					enum {FieldMask = 0x00400000};
					enum {Value_Disabled = 0x0};
					enum {ValueMask_Disabled = 0x00000000};
					enum {Value_Enable = 0x1};
					enum {ValueMask_Enable = 0x00400000};
					};
				};
			namespace INTENSET { // Register description
				typedef uint32_t	Reg;
				namespace CTS { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x00000001};
					enum {Value_Set = 0x1};
					enum {ValueMask_Set = 0x00000001};
					enum {Value_Disabled = 0x0};
					enum {ValueMask_Disabled = 0x00000000};
					enum {Value_Enable = 0x1};
					enum {ValueMask_Enable = 0x00000001};
					};
				namespace NCTS { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x00000001};
					enum {Value_Set = 0x1};
					enum {ValueMask_Set = 0x00000001};
					enum {Value_Disabled = 0x0};
					enum {ValueMask_Disabled = 0x00000000};
					enum {Value_Enable = 0x1};
					enum {ValueMask_Enable = 0x00000001};
					};
				namespace RXRDY { // Field Description
					enum {Lsb = 2};
					enum {FieldMask = 0x00000004};
					enum {Value_Set = 0x1};
					enum {ValueMask_Set = 0x00000004};
					enum {Value_Disabled = 0x0};
					enum {ValueMask_Disabled = 0x00000000};
					enum {Value_Enable = 0x1};
					enum {ValueMask_Enable = 0x00000004};
					};
				namespace ENDRX { // Field Description
					enum {Lsb = 4};
					enum {FieldMask = 0x00000010};
					enum {Value_Set = 0x1};
					enum {ValueMask_Set = 0x00000010};
					enum {Value_Disabled = 0x0};
					enum {ValueMask_Disabled = 0x00000000};
					enum {Value_Enable = 0x1};
					enum {ValueMask_Enable = 0x00000010};
					};
				namespace TXRDY { // Field Description
					enum {Lsb = 7};
					enum {FieldMask = 0x00000080};
					enum {Value_Set = 0x1};
					enum {ValueMask_Set = 0x00000080};
					enum {Value_Disabled = 0x0};
					enum {ValueMask_Disabled = 0x00000000};
					enum {Value_Enable = 0x1};
					enum {ValueMask_Enable = 0x00000080};
					};
				namespace ENDTX { // Field Description
					enum {Lsb = 8};
					enum {FieldMask = 0x00000100};
					enum {Value_Set = 0x1};
					enum {ValueMask_Set = 0x00000100};
					enum {Value_Disabled = 0x0};
					enum {ValueMask_Disabled = 0x00000000};
					enum {Value_Enable = 0x1};
					enum {ValueMask_Enable = 0x00000100};
					};
				namespace ERROR { // Field Description
					enum {Lsb = 9};
					enum {FieldMask = 0x00000200};
					enum {Value_Set = 0x1};
					enum {ValueMask_Set = 0x00000200};
					enum {Value_Disabled = 0x0};
					enum {ValueMask_Disabled = 0x00000000};
					enum {Value_Enable = 0x1};
					enum {ValueMask_Enable = 0x00000200};
					};
				namespace RXTO { // Field Description
					enum {Lsb = 17};
					enum {FieldMask = 0x00020000};
					enum {Value_Set = 0x1};
					enum {ValueMask_Set = 0x00020000};
					enum {Value_Disabled = 0x0};
					enum {ValueMask_Disabled = 0x00000000};
					enum {Value_Enable = 0x1};
					enum {ValueMask_Enable = 0x00020000};
					};
				namespace RXSTARTED { // Field Description
					enum {Lsb = 19};
					enum {FieldMask = 0x00080000};
					enum {Value_Set = 0x1};
					enum {ValueMask_Set = 0x00080000};
					enum {Value_Disabled = 0x0};
					enum {ValueMask_Disabled = 0x00000000};
					enum {Value_Enable = 0x1};
					enum {ValueMask_Enable = 0x00080000};
					};
				namespace TXSTARTED { // Field Description
					enum {Lsb = 20};
					enum {FieldMask = 0x00100000};
					enum {Value_Set = 0x1};
					enum {ValueMask_Set = 0x00100000};
					enum {Value_Disabled = 0x0};
					enum {ValueMask_Disabled = 0x00000000};
					enum {Value_Enable = 0x1};
					enum {ValueMask_Enable = 0x00100000};
					};
				namespace TXSTOPPED { // Field Description
					enum {Lsb = 22};
					enum {FieldMask = 0x00400000};
					enum {Value_Set = 0x1};
					enum {ValueMask_Set = 0x00400000};
					enum {Value_Disabled = 0x0};
					enum {ValueMask_Disabled = 0x00000000};
					enum {Value_Enable = 0x1};
					enum {ValueMask_Enable = 0x00400000};
					};
				};
			namespace INTENCLR { // Register description
				typedef uint32_t	Reg;
				namespace CTS { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x00000001};
					enum {Value_Clear = 0x1};
					enum {ValueMask_Clear = 0x00000001};
					enum {Value_Disabled = 0x0};
					enum {ValueMask_Disabled = 0x00000000};
					enum {Value_Enable = 0x1};
					enum {ValueMask_Enable = 0x00000001};
					};
				namespace NCTS { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x00000001};
					enum {Value_Clear = 0x1};
					enum {ValueMask_Clear = 0x00000001};
					enum {Value_Disabled = 0x0};
					enum {ValueMask_Disabled = 0x00000000};
					enum {Value_Enable = 0x1};
					enum {ValueMask_Enable = 0x00000001};
					};
				namespace RXDRDY { // Field Description
					enum {Lsb = 2};
					enum {FieldMask = 0x00000004};
					enum {Value_Clear = 0x1};
					enum {ValueMask_Clear = 0x00000004};
					enum {Value_Disabled = 0x0};
					enum {ValueMask_Disabled = 0x00000000};
					enum {Value_Enable = 0x1};
					enum {ValueMask_Enable = 0x00000004};
					};
				namespace ENDRX { // Field Description
					enum {Lsb = 4};
					enum {FieldMask = 0x00000010};
					enum {Value_Clear = 0x1};
					enum {ValueMask_Clear = 0x00000010};
					enum {Value_Disabled = 0x0};
					enum {ValueMask_Disabled = 0x00000000};
					enum {Value_Enable = 0x1};
					enum {ValueMask_Enable = 0x00000010};
					};
				namespace TXDRDY { // Field Description
					enum {Lsb = 7};
					enum {FieldMask = 0x00000080};
					enum {Value_Clear = 0x1};
					enum {ValueMask_Clear = 0x00000080};
					enum {Value_Disabled = 0x0};
					enum {ValueMask_Disabled = 0x00000000};
					enum {Value_Enable = 0x1};
					enum {ValueMask_Enable = 0x00000080};
					};
				namespace ENDTX { // Field Description
					enum {Lsb = 8};
					enum {FieldMask = 0x00000100};
					enum {Value_Clear = 0x1};
					enum {ValueMask_Clear = 0x00000100};
					enum {Value_Disabled = 0x0};
					enum {ValueMask_Disabled = 0x00000000};
					enum {Value_Enable = 0x1};
					enum {ValueMask_Enable = 0x00000100};
					};
				namespace ERROR { // Field Description
					enum {Lsb = 9};
					enum {FieldMask = 0x00000200};
					enum {Value_Clear = 0x1};
					enum {ValueMask_Clear = 0x00000200};
					enum {Value_Disabled = 0x0};
					enum {ValueMask_Disabled = 0x00000000};
					enum {Value_Enable = 0x1};
					enum {ValueMask_Enable = 0x00000200};
					};
				namespace RXTO { // Field Description
					enum {Lsb = 17};
					enum {FieldMask = 0x00020000};
					enum {Value_Clear = 0x1};
					enum {ValueMask_Clear = 0x00020000};
					enum {Value_Disabled = 0x0};
					enum {ValueMask_Disabled = 0x00000000};
					enum {Value_Enable = 0x1};
					enum {ValueMask_Enable = 0x00020000};
					};
				namespace RXSTARTED { // Field Description
					enum {Lsb = 19};
					enum {FieldMask = 0x00080000};
					enum {Value_Clear = 0x1};
					enum {ValueMask_Clear = 0x00080000};
					enum {Value_Disabled = 0x0};
					enum {ValueMask_Disabled = 0x00000000};
					enum {Value_Enable = 0x1};
					enum {ValueMask_Enable = 0x00080000};
					};
				namespace TXSTARTED { // Field Description
					enum {Lsb = 20};
					enum {FieldMask = 0x00100000};
					enum {Value_Clear = 0x1};
					enum {ValueMask_Clear = 0x00100000};
					enum {Value_Disabled = 0x0};
					enum {ValueMask_Disabled = 0x00000000};
					enum {Value_Enable = 0x1};
					enum {ValueMask_Enable = 0x00100000};
					};
				namespace TXSTOPPED { // Field Description
					enum {Lsb = 22};
					enum {FieldMask = 0x00400000};
					enum {Value_Clear = 0x1};
					enum {ValueMask_Clear = 0x00400000};
					enum {Value_Disabled = 0x0};
					enum {ValueMask_Disabled = 0x00000000};
					enum {Value_Enable = 0x1};
					enum {ValueMask_Enable = 0x00400000};
					};
				};
			namespace ERRORSRC { // Register description
				typedef uint32_t	Reg;
				namespace OVERRUN { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x00000001};
					enum {Value_NotPresent = 0x0};
					enum {ValueMask_NotPresent = 0x00000000};
					enum {Value_Present = 0x1};
					enum {ValueMask_Present = 0x00000001};
					};
				namespace PARITY { // Field Description
					enum {Lsb = 1};
					enum {FieldMask = 0x00000002};
					enum {Value_NotPresent = 0x0};
					enum {ValueMask_NotPresent = 0x00000000};
					enum {Value_Present = 0x1};
					enum {ValueMask_Present = 0x00000002};
					};
				namespace FRAMING { // Field Description
					enum {Lsb = 2};
					enum {FieldMask = 0x00000004};
					enum {Value_NotPresent = 0x0};
					enum {ValueMask_NotPresent = 0x00000000};
					enum {Value_Present = 0x1};
					enum {ValueMask_Present = 0x00000004};
					};
				namespace BREAK { // Field Description
					enum {Lsb = 3};
					enum {FieldMask = 0x00000008};
					enum {Value_NotPresent = 0x0};
					enum {ValueMask_NotPresent = 0x00000000};
					enum {Value_Present = 0x1};
					enum {ValueMask_Present = 0x00000008};
					};
				};
			namespace ENABLE { // Register description
				typedef uint32_t	Reg;
				namespace ENABLE { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x0000000F};
					enum {Value_Disabled = 0x0};
					enum {ValueMask_Disabled = 0x00000000};
					enum {Value_Enabled = 0x8};
					enum {ValueMask_Enabled = 0x00000008};
					};
				};
			namespace PSELRTS { // Register description
				typedef uint32_t	Reg;
				namespace PIN { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x0000001F};
					};
				namespace PORT { // Field Description
					enum {Lsb = 5};
					enum {FieldMask = 0x00000020};
					};
				namespace CONNECT { // Field Description
					enum {Lsb = 31};
					enum {FieldMask = 0x80000000};
					enum {Value_Disconnected = 0x1};
					enum {ValueMask_Disconnected = 0x80000000};
					enum {Value_Connected = 0x0};
					enum {ValueMask_Connected = 0x00000000};
					};
				};
			namespace PSELTXD { // Register description
				typedef uint32_t	Reg;
				namespace PIN { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x0000001F};
					};
				namespace PORT { // Field Description
					enum {Lsb = 5};
					enum {FieldMask = 0x00000020};
					};
				namespace CONNECT { // Field Description
					enum {Lsb = 31};
					enum {FieldMask = 0x80000000};
					enum {Value_Disconnected = 0x1};
					enum {ValueMask_Disconnected = 0x80000000};
					enum {Value_Connected = 0x0};
					enum {ValueMask_Connected = 0x00000000};
					};
				};
			namespace PSELCTS { // Register description
				typedef uint32_t	Reg;
				namespace PIN { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x0000001F};
					};
				namespace PORT { // Field Description
					enum {Lsb = 5};
					enum {FieldMask = 0x00000020};
					};
				namespace CONNECT { // Field Description
					enum {Lsb = 31};
					enum {FieldMask = 0x80000000};
					enum {Value_Disconnected = 0x1};
					enum {ValueMask_Disconnected = 0x80000000};
					enum {Value_Connected = 0x0};
					enum {ValueMask_Connected = 0x00000000};
					};
				};
			namespace PSELRXD { // Register description
				typedef uint32_t	Reg;
				namespace PIN { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x0000001F};
					};
				namespace PORT { // Field Description
					enum {Lsb = 5};
					enum {FieldMask = 0x00000020};
					};
				namespace CONNECT { // Field Description
					enum {Lsb = 31};
					enum {FieldMask = 0x80000000};
					enum {Value_Disconnected = 0x1};
					enum {ValueMask_Disconnected = 0x80000000};
					enum {Value_Connected = 0x0};
					enum {ValueMask_Connected = 0x00000000};
					};
				};
			namespace BAUDRATE { // Register description
				typedef uint32_t	Reg;
				namespace BAUDRATE { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0xFFFFFFFF};
					enum {Value_Baud1200 = 0x4F000};
					enum {ValueMask_Baud1200 = 0x0004F000};
					enum {Value_Baud2400 = 0x9D000};
					enum {ValueMask_Baud2400 = 0x0009D000};
					enum {Value_Baud4800 = 0x13B000};
					enum {ValueMask_Baud4800 = 0x0013B000};
					enum {Value_Baud9600 = 0x275000};
					enum {ValueMask_Baud9600 = 0x00275000};
					enum {Value_Baud14400 = 0x3AF000};
					enum {ValueMask_Baud14400 = 0x003AF000};
					enum {Value_Baud19200 = 0x4EA000};
					enum {ValueMask_Baud19200 = 0x004EA000};
					enum {Value_Baud28800 = 0x75C000};
					enum {ValueMask_Baud28800 = 0x0075C000};
					enum {Value_Baud31250 = 0x800000};
					enum {ValueMask_Baud31250 = 0x00800000};
					enum {Value_Baud38400 = 0x9D0000};
					enum {ValueMask_Baud38400 = 0x009D0000};
					enum {Value_Baud56000 = 0xE50000};
					enum {ValueMask_Baud56000 = 0x00E50000};
					enum {Value_Baud57600 = 0xEB0000};
					enum {ValueMask_Baud57600 = 0x00EB0000};
					enum {Value_Baud76800 = 0x13A9000};
					enum {ValueMask_Baud76800 = 0x013A9000};
					enum {Value_Baud115200 = 0x1D60000};
					enum {ValueMask_Baud115200 = 0x01D60000};
					enum {Value_Baud115200b = 0x1D7E000};
					enum {ValueMask_Baud115200b = 0x01D7E000};
					enum {Value_Baud230400 = 0x3B00000};
					enum {ValueMask_Baud230400 = 0x03B00000};
					enum {Value_Baud250000 = 0x4000000};
					enum {ValueMask_Baud250000 = 0x04000000};
					enum {Value_Baud460800 = 0x7400000};
					enum {ValueMask_Baud460800 = 0x07400000};
					enum {Value_Baud921600 = 0xF000000};
					enum {ValueMask_Baud921600 = 0x0F000000};
					enum {Value_Baud1M = 0x10000000};
					enum {ValueMask_Baud1M = 0x10000000};
					};
				};
			namespace RXDPTR { // Register description
				typedef uint32_t	Reg;
				};
			namespace RXDMAXCNT { // Register description
				typedef uint32_t	Reg;
				namespace MAXCNT { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x0000FFFF};
					};
				};
			namespace RXDAMOUNT { // Register description
				typedef uint32_t	Reg;
				namespace AMOUNT { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x0000FFFF};
					};
				};
			namespace TXDPTR { // Register description
				typedef uint32_t	Reg;
				};
			namespace TXDMAXCNT { // Register description
				typedef uint32_t	Reg;
				namespace MAXCNT { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x0000FFFF};
					};
				};
			namespace TXDAMOUNT { // Register description
				typedef uint32_t	Reg;
				namespace AMOUNT { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x0000FFFF};
					};
				};
			namespace CONFIG { // Register description
				typedef uint32_t	Reg;
				namespace HWFC { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x00000001};
					enum {Value_Disabled = 0x0};
					enum {ValueMask_Disabled = 0x00000000};
					enum {Value_Enabled = 0x1};
					enum {ValueMask_Enabled = 0x00000001};
					};
				namespace PARITY { // Field Description
					enum {Lsb = 1};
					enum {FieldMask = 0x0000000E};
					enum {Value_Excluded = 0x0};
					enum {ValueMask_Excluded = 0x00000000};
					enum {Value_NoParity = 0x0};
					enum {ValueMask_NoParity = 0x00000000};
					enum {Value_IncludeEvenParity = 0x7};
					enum {ValueMask_IncludeEvenParity = 0x0000000E};
					enum {Value_EvenParity = 0x7};
					enum {ValueMask_EvenParity = 0x0000000E};
					};
				namespace STOP { // Field Description
					enum {Lsb = 4};
					enum {FieldMask = 0x00000010};
					enum {Value_One = 0x0};
					enum {ValueMask_One = 0x00000000};
					enum {Value_Two = 0x1};
					enum {ValueMask_Two = 0x00000010};
					};
				};
			}
		}
	}
#endif
