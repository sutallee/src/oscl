/*
   Copyright (C) 2020 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_hw_nordic_uarte_maph_
#define _oscl_hw_nordic_uarte_maph_

#include "reg.h"

/** */
namespace Oscl {

/** */
namespace Nordic {

/** */
namespace UARTE {

/** */
struct Map {
	/** Offset 0x000 */
	volatile Oscl::Nordic::UARTE::TASKS_STARTRX::Reg	startrx;

	/** Offset 0x004 */
	volatile Oscl::Nordic::UARTE::TASKS_STOPRX::Reg		stoprx;

	/** Offset 0x008 */
	volatile Oscl::Nordic::UARTE::TASKS_STARTTX::Reg	starttx;

	/** Offset 0x00C */
	volatile Oscl::Nordic::UARTE::TASKS_STOPTX::Reg		stoptx;

	uint32_t		unused0x010[(0x02C-0x010)/4];

	/** Offset 0x02C */
	volatile Oscl::Nordic::UARTE::TASKS_FLUSHRX::Reg	flushrx;

	uint32_t		unused0x030[(0x100-0x030)/4];

	/** Offset 0x100 */
	volatile Oscl::Nordic::UARTE::EVENTS_CTS::Reg		cts;

	/** Offset 0x104 */
	volatile Oscl::Nordic::UARTE::EVENTS_NCTS::Reg		ncts;

	/** Offset 0x108 */
	volatile Oscl::Nordic::UARTE::EVENTS_RXDRDY::Reg	rxdrdy;

	uint32_t		unused0x10C;

	/** Offset 0x110 */
	volatile Oscl::Nordic::UARTE::EVENTS_ENDRX::Reg		endrx;

	uint32_t		unused0x114[(0x11C-0x114)/4];

	/** Offset 0x11C */
	volatile Oscl::Nordic::UARTE::EVENTS_TXDRDY::Reg	txdrdy;

	/** Offset 0x120 */
	volatile Oscl::Nordic::UARTE::EVENTS_ENDTX::Reg		endtx;

	/** Offset 0x124 */
	volatile Oscl::Nordic::UARTE::EVENTS_ERROR::Reg		error;

	uint32_t		unused0x128[(0x144-0x128)/4];

	/** Offset 0x144 */
	volatile Oscl::Nordic::UARTE::EVENTS_RXTO::Reg		rxto;

	uint32_t		unused0x148;

	/** Offset 0x14C */
	volatile Oscl::Nordic::UARTE::EVENTS_RXSTARTED::Reg	rxstarted;

	/** Offset 0x150 */
	volatile Oscl::Nordic::UARTE::EVENTS_TXSTARTED::Reg	txstarted;

	uint32_t		unused0x154;

	/** Offset 0x158 */
	volatile Oscl::Nordic::UARTE::EVENTS_TXSTOPPED::Reg	txstopped;

	uint32_t		unused0x15C[(0x200-0x15C)/4];

	/** Offset 0x200 */
	volatile Oscl::Nordic::UARTE::SHORTS::Reg			shorts;

	uint32_t		unused0x204[(0x300-0x204)/4];

	/** Offset 0x300 */
	volatile Oscl::Nordic::UARTE::INTEN::Reg			inten;

	/** Offset 0x304 */
	volatile Oscl::Nordic::UARTE::INTENSET::Reg			intenset;

	/** Offset 0x308 */
	volatile Oscl::Nordic::UARTE::INTENCLR::Reg			intenclr;

	uint32_t		unused0x30C[(0x480-0x30C)/4];

	/** Offset 0x480 */
	volatile Oscl::Nordic::UARTE::ERRORSRC::Reg			errorsrc;

	uint32_t		unused0x484[(0x500-0x484)/4];

	/** Offset 0x500 */
	volatile Oscl::Nordic::UARTE::ENABLE::Reg			enable;

	uint32_t		unused0x504;

	/** Offset 0x508 */
	volatile Oscl::Nordic::UARTE::PSELRTS::Reg			pselrts;

	/** Offset 0x50C */
	volatile Oscl::Nordic::UARTE::PSELTXD::Reg			pseltxd;

	/** Offset 0x510 */
	volatile Oscl::Nordic::UARTE::PSELCTS::Reg			pselcts;

	/** Offset 0x514 */
	volatile Oscl::Nordic::UARTE::PSELRXD::Reg			pselrxd;

	uint32_t		unused0x518[(0x524-0x518)/4];

	/** Offset 0x524 */
	volatile Oscl::Nordic::UARTE::BAUDRATE::Reg			baudrate;

	uint32_t		unused0x528[(0x534-0x528)/4];

	/** Offset 0x534 */
	volatile Oscl::Nordic::UARTE::RXDPTR::Reg			rxdptr;

	/** Offset 0x538 */
	volatile Oscl::Nordic::UARTE::RXDMAXCNT::Reg		rxdmaxcnt;

	/** Offset 0x53C */
	volatile Oscl::Nordic::UARTE::RXDAMOUNT::Reg		rxdamount;

	uint32_t		unused0x540[(0x544-0x540)/4];

	/** Offset 0x544 */
	volatile Oscl::Nordic::UARTE::TXDPTR::Reg			txdptr;

	/** Offset 0x548 */
	volatile Oscl::Nordic::UARTE::TXDMAXCNT::Reg		txdmaxcnt;

	/** Offset 0x54C */
	volatile Oscl::Nordic::UARTE::TXDAMOUNT::Reg		txdamount;

	uint32_t		unused0x550[(0x56C-0x550)/4];

	/** Offset 0x56C */
	volatile Oscl::Nordic::UARTE::CONFIG::Reg			config;
	};

}
}
}

#endif
