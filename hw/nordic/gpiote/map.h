/*
   Copyright (C) 2020 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_hw_nordic_gpiote_maph_
#define _oscl_hw_nordic_gpiote_maph_

#include "reg.h"

/** */
namespace Oscl {

/** */
namespace Nordic {

/** */
namespace GPIOTE {

/** */
struct Map {
	/** Offset 0x000 */
	volatile Oscl::Nordic::GPIOTE::TASKS_OUT::Reg	tasks_out[8];

	uint32_t		unused0x020[(0x030-0x020)/4];

	/** Offset 0x030 */
	volatile Oscl::Nordic::GPIOTE::TASKS_SET::Reg	tasks_set[8];

	uint32_t		unused0x050[(0x060-0x050)/4];

	/** Offset 0x060 */
	volatile Oscl::Nordic::GPIOTE::TASKS_CLR::Reg	tasks_clr[8];

	uint32_t		unused0x080[(0x100-0x080)/4];

	/** Offset 0x100 */
	volatile Oscl::Nordic::GPIOTE::EVENTS_IN::Reg	events_in[8];

	uint32_t		unused0x120[(0x304-0x120)/4];

	/** Offset 0x304 */
	volatile Oscl::Nordic::GPIOTE::INTENSET::Reg	intenset;

	/** Offset 0x308 */
	volatile Oscl::Nordic::GPIOTE::INTENCLR::Reg	intenclr;

	uint32_t		unused0x30C[(0x510-0x30C)/4];

	/** Offset 0x510 */
	volatile Oscl::Nordic::GPIOTE::CONFIG::Reg	config[8];
	};

}
}
}

#endif
