#ifndef _oscl_hw_nordic_gpiote_regh_
#define _oscl_hw_nordic_gpiote_regh_
#include <stdint.h>
namespace Oscl { // Namespace description
	namespace Nordic { // Namespace description
		namespace GPIOTE { // Namespace description
			namespace TASKS_OUT { // Register description
				typedef uint32_t	Reg;
				namespace OUT { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x00000001};
					enum {Value_Trigger = 0x1};
					enum {ValueMask_Trigger = 0x00000001};
					};
				};
			namespace TASKS_SET { // Register description
				typedef uint32_t	Reg;
				namespace SET { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x00000001};
					enum {Value_Trigger = 0x1};
					enum {ValueMask_Trigger = 0x00000001};
					};
				};
			namespace TASKS_CLR { // Register description
				typedef uint32_t	Reg;
				namespace CLR { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x00000001};
					enum {Value_Trigger = 0x1};
					enum {ValueMask_Trigger = 0x00000001};
					};
				};
			namespace EVENTS_IN { // Register description
				typedef uint32_t	Reg;
				namespace IN { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x00000001};
					enum {Value_NotGenerated = 0x0};
					enum {ValueMask_NotGenerated = 0x00000000};
					enum {Value_Generated = 0x1};
					enum {ValueMask_Generated = 0x00000001};
					};
				};
			namespace EVENTS_PORT { // Register description
				typedef uint32_t	Reg;
				namespace PORT { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x00000001};
					enum {Value_NotGenerated = 0x0};
					enum {ValueMask_NotGenerated = 0x00000000};
					enum {Value_Generated = 0x1};
					enum {ValueMask_Generated = 0x00000001};
					};
				};
			namespace INTENSET { // Register description
				typedef uint32_t	Reg;
				enum {Set = 1};
				enum {Disabled = 0};
				enum {Enabled = 1};
				};
			namespace INTENCLR { // Register description
				typedef uint32_t	Reg;
				enum {Clear = 1};
				enum {Disabled = 0};
				enum {Enabled = 1};
				};
			namespace CONFIG { // Register description
				typedef uint32_t	Reg;
				namespace MODE { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x00000003};
					enum {Value_Disabled = 0x0};
					enum {ValueMask_Disabled = 0x00000000};
					enum {Value_Event = 0x1};
					enum {ValueMask_Event = 0x00000001};
					enum {Value_Task = 0x3};
					enum {ValueMask_Task = 0x00000003};
					};
				namespace PSEL { // Field Description
					enum {Lsb = 8};
					enum {FieldMask = 0x00001F00};
					};
				namespace PORT { // Field Description
					enum {Lsb = 13};
					enum {FieldMask = 0x00002000};
					};
				namespace POLARITY { // Field Description
					enum {Lsb = 16};
					enum {FieldMask = 0x00030000};
					enum {Value_None = 0x0};
					enum {ValueMask_None = 0x00000000};
					enum {Value_LoToHi = 0x1};
					enum {ValueMask_LoToHi = 0x00010000};
					enum {Value_HiToLo = 0x2};
					enum {ValueMask_HiToLo = 0x00020000};
					enum {Value_Toggle = 0x3};
					enum {ValueMask_Toggle = 0x00030000};
					};
				namespace OUTPUT { // Field Description
					enum {Lsb = 20};
					enum {FieldMask = 0x00100000};
					enum {Value_Low = 0x0};
					enum {ValueMask_Low = 0x00000000};
					enum {Value_High = 0x1};
					enum {ValueMask_High = 0x00100000};
					};
				};
			}
		}
	}
#endif
