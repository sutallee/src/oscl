/*
   Copyright (C) 2020 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_hw_nordic_temp_maph_
#define _oscl_hw_nordic_temp_maph_

#include "reg.h"

/** */
namespace Oscl {

/** */
namespace Nordic {

/** */
namespace TEMP {

/** */
struct Map {
	/** Offset 0x000 */
	volatile Oscl::Nordic::TEMP::TASKS_START::Reg		start;

	/** Offset 0x004 */
	volatile Oscl::Nordic::TEMP::TASKS_STOP::Reg		stop;

	uint32_t		unused0x008[(0x100-0x008)/4];

	/** Offset 0x100 */
	volatile Oscl::Nordic::TEMP::EVENTS_DATARDY::Reg	datardy;

	uint32_t		unused0x104[(0x304-0x104)/4];

	/** Offset 0x304 */
	volatile Oscl::Nordic::TEMP::INTENSET::Reg			intenset;

	/** Offset 0x308 */
	volatile Oscl::Nordic::TEMP::INTENCLR::Reg			intenclr;

	uint32_t		unused0x30C[(0x508-0x30C)/4];

	/** Offset 0x508 */
	volatile Oscl::Nordic::TEMP::TEMP::Reg				temp;

	uint32_t		unused0x50C[(0x520-0x50C)/4];

	/** Offset 0x520 */
	volatile Oscl::Nordic::TEMP::A::Reg					a[6];

	uint32_t		unused0x538[(0x540-0x538)/4];

	/** Offset 0x540 */
	volatile Oscl::Nordic::TEMP::B::Reg					b[6];

	uint32_t		unused0x558[(0x560-0x558)/4];

	/** Offset 0x560 */
	volatile Oscl::Nordic::TEMP::T::Reg					t[5];

	};

}
}
}

#endif
