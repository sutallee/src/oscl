#ifndef _oscl_hw_nordic_temp_regh_
#define _oscl_hw_nordic_temp_regh_
#include <stdint.h>
namespace Oscl { // Namespace description
	namespace Nordic { // Namespace description
		namespace TEMP { // Namespace description
			namespace TASKS_START { // Register description
				typedef uint32_t	Reg;
				namespace START { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x00000001};
					enum {Value_Trigger = 0x1};
					enum {ValueMask_Trigger = 0x00000001};
					};
				};
			namespace TASKS_STOP { // Register description
				typedef uint32_t	Reg;
				namespace STOP { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x00000001};
					enum {Value_Trigger = 0x1};
					enum {ValueMask_Trigger = 0x00000001};
					};
				};
			namespace EVENTS_DATARDY { // Register description
				typedef uint32_t	Reg;
				namespace DATARDY { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x00000001};
					enum {Value_NotGenerated = 0x0};
					enum {ValueMask_NotGenerated = 0x00000000};
					enum {Value_Generated = 0x1};
					enum {ValueMask_Generated = 0x00000001};
					};
				};
			namespace INTENSET { // Register description
				typedef uint32_t	Reg;
				enum {Set = 1};
				enum {Disabled = 0};
				enum {Enabled = 1};
				namespace DATARDY { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x00000001};
					enum {Value_Set = 0x1};
					enum {ValueMask_Set = 0x00000001};
					enum {Value_Disabled = 0x0};
					enum {ValueMask_Disabled = 0x00000000};
					enum {Value_Enabled = 0x1};
					enum {ValueMask_Enabled = 0x00000001};
					};
				};
			namespace INTENCLR { // Register description
				typedef uint32_t	Reg;
				enum {Clear = 1};
				enum {Disabled = 0};
				enum {Enabled = 1};
				namespace DATARDY { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x00000001};
					enum {Value_Clear = 0x1};
					enum {ValueMask_Clear = 0x00000001};
					enum {Value_Disabled = 0x0};
					enum {ValueMask_Disabled = 0x00000000};
					enum {Value_Enabled = 0x1};
					enum {ValueMask_Enabled = 0x00000001};
					};
				};
			namespace TEMP { // Register description
				typedef int32_t	Reg;
				};
			namespace A { // Register description
				typedef uint32_t	Reg;
				namespace SLOPE { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x00000FFF};
					};
				};
			namespace B { // Register description
				typedef uint32_t	Reg;
				namespace YINTERCEPT { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x00003FFF};
					};
				};
			namespace T { // Register description
				typedef uint32_t	Reg;
				namespace ENDPOINT { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x000000FF};
					};
				};
			}
		}
	}
#endif
