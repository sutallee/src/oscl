/*
   Copyright (C) 2020 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

module oscl_hw_nordic_temp_regh {
include "oscl/compiler/types.h";
	namespace Oscl {
		namespace Nordic {
		namespace TEMP {
			register TASKS_START uint32_t {
				field START 0 1 {
					value Trigger	1
					}
				}
			register TASKS_STOP uint32_t {
				field STOP 0 1 {
					value Trigger	1
					}
				}
			register EVENTS_DATARDY uint32_t {
				field DATARDY 0 1 {
					value NotGenerated	0
					value Generated		1
					}
				}
			register INTENSET uint32_t {
				value Set		1
				value Disabled	0
				value Enabled	1
				field DATARDY 0 1 {
					value Set		1
					value Disabled	0
					value Enabled	1
					}
				}
			register INTENCLR uint32_t {
				value Clear		1
				value Disabled	0
				value Enabled	1

				field DATARDY 0 1 {
					value Clear		1
					value Disabled	0
					value Enabled	1
					}
				}
			register TEMP int32_t {
				/* Temperature in 1/4 degree Celsius */
				}
			register A uint32_t {
				/* Slope of piece wise linear function. */
				field SLOPE 0 12 {
					}
				}
			register B uint32_t {
				/* Y-intercept of piece wise linear function. */
				field YINTERCEPT 0 14 {
					}
				}
			register T uint32_t {
				/* End-point of piece wise linear function. */
				field ENDPOINT 0 8 {
					}
				}
			}
		}
	}
}

