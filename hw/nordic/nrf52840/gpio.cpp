#include "map.h"


/** RETURN: Pointer GPIO instance corresponding
	the GPIO port number or zero for failure.
 */
Oscl::Nordic::GPIO::Map*	Oscl::Nordic::GPIO::getInstance(unsigned port) noexcept{
	switch(port){
		case 0:
			return &Nrf52840GPIOP0;
		case 1:
			return &Nrf52840GPIOP1;
		default:
			return 0;
		}
	}

