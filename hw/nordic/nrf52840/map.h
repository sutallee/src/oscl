/*
   Copyright (C) 2020 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_hw_nordic_nrf52840_maph_
#define _oscl_hw_nordic_nrf52840_maph_

#include "oscl/hw/nordic/uarte/map.h"
#include "oscl/hw/nordic/gpio/map.h"
#include "oscl/hw/nordic/gpiote/map.h"
#include "oscl/hw/nordic/clock/map.h"
#include "oscl/hw/nordic/saadc/map.h"
#include "oscl/hw/nordic/spim/map.h"
#include "oscl/hw/nordic/temp/map.h"

extern Oscl::Nordic::UARTE::Map	Nrf52840UARTE0;

extern Oscl::Nordic::UARTE::Map	Nrf52840UARTE1;

extern Oscl::Nordic::GPIO::Map	Nrf52840GPIOP0;

extern Oscl::Nordic::GPIO::Map	Nrf52840GPIOP1;

extern Oscl::Nordic::GPIOTE::Map	Nrf52840GPIOTE;

extern Oscl::Nordic::CLOCK::Map	Nrf52840CLOCK;

extern Oscl::Nordic::SAADC::Map	Nrf52840SAADC;

extern Oscl::Nordic::SPIM::Map	Nrf52840SPIM0;
extern Oscl::Nordic::SPIM::Map	Nrf52840SPIM1;
extern Oscl::Nordic::SPIM::Map	Nrf52840SPIM2;
extern Oscl::Nordic::SPIM::Map	Nrf52840SPIM3;

extern Oscl::Nordic::TEMP::Map	Nrf52840TEMP;

#endif
