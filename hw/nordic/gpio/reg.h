/*
   Copyright (C) 2021 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_hw_nordic_gpio_regh_
#define _oscl_hw_nordic_gpio_regh_
#include <stdint.h>
namespace Oscl { // Namespace description
	namespace Nordic { // Namespace description
		namespace GPIO { // Namespace description
			namespace OUT { // Register description
				typedef uint32_t	Reg;
				constexpr Reg	Low = 0;
				constexpr Reg	High = 1;
				};
			namespace OUTSET { // Register description
				typedef uint32_t	Reg;
				constexpr Reg	Low = 0;
				constexpr Reg	High = 1;
				constexpr Reg	Set = 1;
				};
			namespace OUTCLR { // Register description
				typedef uint32_t	Reg;
				constexpr Reg	Low = 0;
				constexpr Reg	High = 1;
				constexpr Reg	Clear = 1;
				};
			namespace IN { // Register description
				typedef uint32_t	Reg;
				constexpr Reg	Low = 0;
				constexpr Reg	High = 1;
				};
			namespace DIR { // Register description
				typedef uint32_t	Reg;
				constexpr Reg	Input = 0;
				constexpr Reg	Output = 1;
				};
			namespace DIRSET { // Register description
				typedef uint32_t	Reg;
				constexpr Reg	Input = 0;
				constexpr Reg	Output = 1;
				constexpr Reg	Set = 1;
				};
			namespace DIRCLR { // Register description
				typedef uint32_t	Reg;
				constexpr Reg	Input = 0;
				constexpr Reg	Output = 1;
				constexpr Reg	Clear = 1;
				};
			namespace LATCH { // Register description
				typedef uint32_t	Reg;
				constexpr Reg	NotLatched = 0;
				constexpr Reg	Latched = 1;
				};
			namespace DETECTMODE { // Register description
				typedef uint32_t	Reg;
				constexpr Reg	Default = 0;
				constexpr Reg	LDETECT = 1;
				};
			namespace PIN_CNF { // Register description
				typedef uint32_t	Reg;
				namespace DIR { // Field Description
					constexpr Reg Lsb = 0;
					constexpr Reg FieldMask = 0x00000001;
					constexpr Reg Value_Input = 0x0;
					constexpr Reg ValueMask_Input = 0x00000000;
					constexpr Reg Value_Output = 0x1;
					constexpr Reg ValueMask_Output = 0x00000001;
					};
				namespace INPUT { // Field Description
					constexpr Reg Lsb = 1;
					constexpr Reg FieldMask = 0x00000002;
					constexpr Reg Value_Connect = 0x0;
					constexpr Reg ValueMask_Connect = 0x00000000;
					constexpr Reg Value_Disconnect = 0x1;
					constexpr Reg ValueMask_Disconnect = 0x00000002;
					};
				namespace PULL { // Field Description
					constexpr Reg Lsb = 2;
					constexpr Reg FieldMask = 0x0000000C;
					constexpr Reg Value_NoPull = 0x0;
					constexpr Reg ValueMask_NoPull = 0x00000000;
					constexpr Reg Value_Pulldown = 0x1;
					constexpr Reg ValueMask_Pulldown = 0x00000004;
					constexpr Reg Value_Pullup = 0x3;
					constexpr Reg ValueMask_Pullup = 0x0000000C;
					};
				namespace DRIVE { // Field Description
					constexpr Reg Lsb = 8;
					constexpr Reg FieldMask = 0x00000700;
					constexpr Reg Value_S0S1 = 0x0;
					constexpr Reg ValueMask_S0S1 = 0x00000000;
					constexpr Reg Value_H0S1 = 0x1;
					constexpr Reg ValueMask_H0S1 = 0x00000100;
					constexpr Reg Value_S0H1 = 0x2;
					constexpr Reg ValueMask_S0H1 = 0x00000200;
					constexpr Reg Value_H0H1 = 0x3;
					constexpr Reg ValueMask_H0H1 = 0x00000300;
					constexpr Reg Value_D0S1 = 0x4;
					constexpr Reg ValueMask_D0S1 = 0x00000400;
					constexpr Reg Value_D0H1 = 0x5;
					constexpr Reg ValueMask_D0H1 = 0x00000500;
					constexpr Reg Value_S0D1 = 0x6;
					constexpr Reg ValueMask_S0D1 = 0x00000600;
					constexpr Reg Value_H0D1 = 0x7;
					constexpr Reg ValueMask_H0D1 = 0x00000700;
					};
				namespace SENSE { // Field Description
					constexpr Reg Lsb = 16;
					constexpr Reg FieldMask = 0x00030000;
					constexpr Reg Value_Disabled = 0x0;
					constexpr Reg ValueMask_Disabled = 0x00000000;
					constexpr Reg Value_High = 0x2;
					constexpr Reg ValueMask_High = 0x00020000;
					constexpr Reg Value_Low = 0x3;
					constexpr Reg ValueMask_Low = 0x00030000;
					};
				};
			}
		}
	}
#endif
