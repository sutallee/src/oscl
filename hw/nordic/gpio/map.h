/*
   Copyright (C) 2020 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_hw_nordic_gpio_maph_
#define _oscl_hw_nordic_gpio_maph_

#include "reg.h"

/** */
namespace Oscl {

/** */
namespace Nordic {

/** */
namespace GPIO {

/** */
struct Map {
	uint32_t		unused0x000[(0x504-0x000)/4];

	/** Offset 0x504 */
	volatile Oscl::Nordic::GPIO::OUT::Reg	out;

	/** Offset 0x508 */
	volatile Oscl::Nordic::GPIO::OUTSET::Reg	outset;

	/** Offset 0x50C */
	volatile Oscl::Nordic::GPIO::OUTCLR::Reg	outclr;

	/** Offset 0x510 */
	volatile Oscl::Nordic::GPIO::IN::Reg	in;

	/** Offset 0x514 */
	volatile Oscl::Nordic::GPIO::DIR::Reg	dir;

	/** Offset 0x518 */
	volatile Oscl::Nordic::GPIO::DIRSET::Reg	dirset;

	/** Offset 0x51C */
	volatile Oscl::Nordic::GPIO::DIRCLR::Reg	dirclr;

	/** Offset 0x520 */
	volatile Oscl::Nordic::GPIO::LATCH::Reg	latch;

	/** Offset 0x524 */
	volatile Oscl::Nordic::GPIO::DETECTMODE::Reg	detectmode;

	uint32_t		unused0x528[(0x700-0x528)/4];

	/** Offset 0x700 */
	volatile Oscl::Nordic::GPIO::PIN_CNF::Reg	pin_cnf[32];
	};

/** RETURN: Pointer GPIO instance corresponding
	the GPIO port number or zero for failure.
 */
Oscl::Nordic::GPIO::Map*	getInstance(unsigned port) noexcept;

}
}
}

#endif
