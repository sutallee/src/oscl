/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _powerpc_uisah_
#define _powerpc_uisah_
#include <stdint.h>
namespace Oscl { // Namespace description
	namespace Ppc { // Namespace description
		namespace Uisa { // Namespace description
			namespace Gpr { // Register description
				typedef uint32_t	Reg;
				};
			namespace GP { // Namespace description
				enum {R0 = 0};
				enum {R1 = 1};
				enum {R2 = 2};
				enum {R3 = 3};
				enum {R4 = 4};
				enum {R5 = 5};
				enum {R6 = 6};
				enum {R7 = 7};
				enum {R8 = 8};
				enum {R9 = 9};
				enum {R10 = 10};
				enum {R11 = 11};
				enum {R12 = 12};
				enum {R13 = 13};
				enum {R14 = 14};
				enum {R15 = 15};
				enum {R16 = 16};
				enum {R17 = 17};
				enum {R18 = 18};
				enum {R19 = 19};
				enum {R20 = 20};
				enum {R21 = 21};
				enum {R22 = 22};
				enum {R23 = 23};
				enum {R24 = 24};
				enum {R25 = 25};
				enum {R26 = 26};
				enum {R27 = 27};
				enum {R28 = 28};
				enum {R29 = 29};
				enum {R30 = 30};
				enum {R31 = 31};
				};
			enum {CR0 = 0};
			enum {CR1 = 1};
			enum {CR2 = 2};
			enum {CR3 = 3};
			enum {CR4 = 4};
			enum {CR5 = 5};
			enum {CR6 = 6};
			enum {CR7 = 7};
			namespace Cr { // Register description
				typedef uint32_t	Reg;
				namespace Lt0 { // Field Description
					enum {Lsb = 31};
					enum {FieldMask = 0x80000000};
					enum {Value_Negative = 0x1};
					enum {ValueMask_Negative = 0x80000000};
					enum {Value_Positive = 0x0};
					enum {ValueMask_Positive = 0x00000000};
					};
				namespace Lt1 { // Field Description
					enum {Lsb = 27};
					enum {FieldMask = 0x08000000};
					enum {Value_Negative = 0x1};
					enum {ValueMask_Negative = 0x08000000};
					enum {Value_Positive = 0x0};
					enum {ValueMask_Positive = 0x00000000};
					};
				namespace Lt2 { // Field Description
					enum {Lsb = 23};
					enum {FieldMask = 0x00800000};
					enum {Value_Negative = 0x1};
					enum {ValueMask_Negative = 0x00800000};
					enum {Value_Positive = 0x0};
					enum {ValueMask_Positive = 0x00000000};
					};
				namespace Lt3 { // Field Description
					enum {Lsb = 19};
					enum {FieldMask = 0x00080000};
					enum {Value_Negative = 0x1};
					enum {ValueMask_Negative = 0x00080000};
					enum {Value_Positive = 0x0};
					enum {ValueMask_Positive = 0x00000000};
					};
				namespace Lt4 { // Field Description
					enum {Lsb = 15};
					enum {FieldMask = 0x00008000};
					enum {Value_Negative = 0x1};
					enum {ValueMask_Negative = 0x00008000};
					enum {Value_Positive = 0x0};
					enum {ValueMask_Positive = 0x00000000};
					};
				namespace Lt5 { // Field Description
					enum {Lsb = 11};
					enum {FieldMask = 0x00000800};
					enum {Value_Negative = 0x1};
					enum {ValueMask_Negative = 0x00000800};
					enum {Value_Positive = 0x0};
					enum {ValueMask_Positive = 0x00000000};
					};
				namespace Lt6 { // Field Description
					enum {Lsb = 7};
					enum {FieldMask = 0x00000080};
					enum {Value_Negative = 0x1};
					enum {ValueMask_Negative = 0x00000080};
					enum {Value_Positive = 0x0};
					enum {ValueMask_Positive = 0x00000000};
					};
				namespace Lt7 { // Field Description
					enum {Lsb = 3};
					enum {FieldMask = 0x00000008};
					enum {Value_Negative = 0x1};
					enum {ValueMask_Negative = 0x00000008};
					enum {Value_Positive = 0x0};
					enum {ValueMask_Positive = 0x00000000};
					};
				namespace Gt0 { // Field Description
					enum {Lsb = 30};
					enum {FieldMask = 0x40000000};
					enum {Value_PositiveAndNonZero = 0x1};
					enum {ValueMask_PositiveAndNonZero = 0x40000000};
					enum {Value_NegativeOrZero = 0x0};
					enum {ValueMask_NegativeOrZero = 0x00000000};
					};
				namespace Eq0 { // Field Description
					enum {Lsb = 29};
					enum {FieldMask = 0x20000000};
					enum {Value_Zero = 0x1};
					enum {ValueMask_Zero = 0x20000000};
					enum {Value_NonZero = 0x0};
					enum {ValueMask_NonZero = 0x00000000};
					};
				namespace So0 { // Field Description
					enum {Lsb = 28};
					enum {FieldMask = 0x10000000};
					enum {Value_Overflow = 0x1};
					enum {ValueMask_Overflow = 0x10000000};
					enum {Value_Normal = 0x0};
					enum {ValueMask_Normal = 0x00000000};
					};
				};
			};
		};
	};
#endif
