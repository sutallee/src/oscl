/*
   Copyright (C) 2007 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

module hw_powerpc_mmu_desch {
	include "oscl/compiler/types.h";
	namespace Oscl {
		namespace HW {
			namespace PPC {
				namespace MMU {
					namespace SegDesc {
						register DirectStore uint32_t {	/* Segment Registers SR0-SR15 */
							field T 31 1 {
								value DirectStoreDescType	1
								}
							field Ks 30 1 {
								}
							field Kp 29 1 {
								}
							field BUID 20 9 { }
							field CNTLR_SPEC 0 20 { }
							} 
						register PageAddrTrans uint32_t {	/* Segment Registers SR0-SR15 */
							field T 31 1 {
								value PageAddrTransType	0
								}
							field 
							field Ks 30 1 {
								}
							field Kp 29 1 {
								}
							field N 28 1 {
								}
							field VSID 0 24 { }
							} 
						}
					register IBATU uint32_t {
						field BEPI 17 15 { }
						field BL 12 11 { }
						field Vs 1 1 {
							value ValidForSupervisorMode	1
							value NotValid					0
							}
						field Vp 0 1 {
							value ValidForUserMode			1
							value NotValid					0
							}
						}
					register IBATL uint32_t {
						field BRPN 17 15 { }
						field WIMG 3 4 {
							field I 2 1 {
								value CacheInhibit	1
								value CacheEnabled	0
								}
							field M 1 1 {
								value CoherencyEnforced		1
								value CoherencyNotEnforced	0
								}
							}
						field PP 0 2 {
							value NoAccess	0
							value ReadOnly	1
							value ReadWrite	2
							}
						}
					register DBATU uint32_t {
						field BEPI 17 15 { }
						field BL 12 11 { }
						field Vs 1 1 {
							value ValidForSupervisorMode	1
							value NotValid					0
							}
						field Vp 0 1 {
							value ValidForUserMode			1
							value NotValid					0
							}
						}
					register DBATL uint32_t {
						field BRPN 17 15 { }
						field WIMG 3 4 {
							field W 3 1 {
								value WriteThrough	1
								value WriteBack		0
								}
							field I 2 1 {
								value CacheInhibit	1
								value CacheEnabled	0
								}
							field M 1 1 {
								value CoherencyEnforced		1
								value CoherencyNotEnforced	0
								}
							field G 0 1 {
								value Guarded		1
								value NotGuarded	0
								}
							}
						field PP 0 2 {
							value NoAccess	0
							value ReadOnly	1
							value ReadWrite	2
							}
						}
					register SDR1 uint32_t {
						}

#if 0


					namespace Twam1 {
						register EA uint32_t {
							field	Level1Index		22 10 {}
							field	Level2Index		12 10 {}
							field	PageOffset4K	0 12 {}
							field	PageOffset16K	0 14 {}
							field	PageOffset512K	0 19 {}
							field	PageOffset8M	0 23 {}
							}
						} 
					value nEntriesInLevel1Twam0Table	4096
					value nEntriesInLevel1Twam1Table	1024
					value nEntriesInLevel2Table			1024
					register Level2Desc uint32_t {
						field RPN 12 20 { }
						field PP 3 9 {}
						field Ppm0 3 9 {
							field Encoding 6 1 {
								value Ppc 0
								value Extended 1
								}
							field ExtEnc 6 3 {
								field PP 0 3 {
									// Extended
									value NoAccess 		1
									value SuperReadOnly 3
									// PPC
									value SuperReadWriteOnly 		0
									value SuperReadWriteUserReadOnly 2
									value AllReadWrite 4
									value AllReadOnly 6
									}
								}
							field PpcEnc 6 3 {
								field PP 1 2 {
									value SuperReadWriteUserNoAccess 0
									value SuperReadWriteUserReadOnly 1
									value AllReadWrite 2
									value AllReadOnly 3
									}
								}
							field Change 5 1 {
								value Unchanged	0
								value Changed	1
								}
							field Ppcs0 1 4 {
								field Mode3 0 4 {
									value SubPage1	0x01
									value SubPage2	0x02
									value SubPage3	0x03
									value SubPage4	0x04
									}
								field Other 0 4 {
									value Fixed	0x0F
									}
								}
							field Ppcs1 1 4 {
								field Compare 0 4 {
									value HitSuperOnly 8
									value HitUserOnly 4
									value HitBoth 6
									}
								}
							field SPS 0 1 {
								value Size4Kbyte 0
								value Size16Kbyte 1
								}
							}
		
		
						field Ppm1 3 9 {
							field PP1 7 2 {
								value NoAccess 0
								value SuperOnlyReadWrite 1
								value SuperReadWriteUserReadOnly 2
								value AllReadWrite 3
								}
							field PP2 5 2 {
								value NoAccess 0
								value SuperOnlyReadWrite 1
								value SuperReadWriteUserReadOnly 2
								value AllReadWrite 3
								}
							field PP3 3 2 {
								value NoAccess 0
								value SuperOnlyReadWrite 1
								value SuperReadWriteUserReadOnly 2
								value AllReadWrite 3
								}
							field PP4 1 2 {
								value NoAccess 0
								value SuperOnlyReadWrite 1
								value SuperReadWriteUserReadOnly 2
								value AllReadWrite 3
								}
							field SPS 0 1 {
								value Fixed 0
								}
							}
		
						field SH 2 1 {
							value MatchASID		0
							value IgnoreASID	1
							}
						field CI 1 1 {
							value CacheEnabled		0
							value CacheInhibited	1
							}
						field V 0 1 {
							value Invalid	0
							value Valid		1
							}
						}
		
					register Level1Desc uint32_t {
						field L2BA 12 20 { }
						field APG 5 4 { }
						field G 4 1 {
							value NonGuarded 0
							value Guarded 1
							}
						field PS 2 2 {
							value Small 0
							value 512K 1
							value 8M 3
							}
						field WT 1 1 {
							value Copyback 0
							value Writethrough 0
							}
						field V 0 1 {
							value Invalid 0
							value Valid 1
							}
						}
#endif
					}
				}
			}
		}
	}
