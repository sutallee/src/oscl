#ifndef _hw_powerpc_mmu_desch_
#define _hw_powerpc_mmu_desch_
#include <stdint.h>
namespace Oscl { // Namespace description
	namespace HW { // Namespace description
		namespace PPC { // Namespace description
			namespace MMU { // Namespace description
				namespace SR { // Register description
					typedef uint32_t	Reg;
					};
				namespace IBATU { // Register description
					typedef uint32_t	Reg;
					namespace BEPI { // Field Description
						enum {Lsb = 17};
						enum {FieldMask = 0xFFFE0000};
						};
					namespace BL { // Field Description
						enum {Lsb = 12};
						enum {FieldMask = 0x007FF000};
						};
					namespace Vs { // Field Description
						enum {Lsb = 1};
						enum {FieldMask = 0x00000002};
						enum {Value_ValidForSupervisorMode = 0x1};
						enum {ValueMask_ValidForSupervisorMode = 0x00000002};
						enum {Value_NotValid = 0x0};
						enum {ValueMask_NotValid = 0x00000000};
						};
					namespace Vp { // Field Description
						enum {Lsb = 0};
						enum {FieldMask = 0x00000001};
						enum {Value_ValidForUserMode = 0x1};
						enum {ValueMask_ValidForUserMode = 0x00000001};
						enum {Value_NotValid = 0x0};
						enum {ValueMask_NotValid = 0x00000000};
						};
					};
				namespace IBATL { // Register description
					typedef uint32_t	Reg;
					namespace BRPN { // Field Description
						enum {Lsb = 17};
						enum {FieldMask = 0xFFFE0000};
						};
					namespace WIMG { // Field Description
						enum {Lsb = 3};
						enum {FieldMask = 0x00000078};
						namespace I { // Field Description
							enum {Lsb = 5};
							enum {FieldMask = 0x00000020};
							enum {Value_CacheInhibit = 0x1};
							enum {ValueMask_CacheInhibit = 0x00000020};
							enum {Value_CacheEnabled = 0x0};
							enum {ValueMask_CacheEnabled = 0x00000000};
							};
						namespace M { // Field Description
							enum {Lsb = 4};
							enum {FieldMask = 0x00000010};
							enum {Value_CoherencyEnforced = 0x1};
							enum {ValueMask_CoherencyEnforced = 0x00000010};
							enum {Value_CoherencyNotEnforced = 0x0};
							enum {ValueMask_CoherencyNotEnforced = 0x00000000};
							};
						};
					namespace PP { // Field Description
						enum {Lsb = 0};
						enum {FieldMask = 0x00000003};
						enum {Value_NoAccess = 0x0};
						enum {ValueMask_NoAccess = 0x00000000};
						enum {Value_ReadOnly = 0x1};
						enum {ValueMask_ReadOnly = 0x00000001};
						enum {Value_ReadWrite = 0x2};
						enum {ValueMask_ReadWrite = 0x00000002};
						};
					};
				namespace DBATU { // Register description
					typedef uint32_t	Reg;
					namespace BEPI { // Field Description
						enum {Lsb = 17};
						enum {FieldMask = 0xFFFE0000};
						};
					namespace BL { // Field Description
						enum {Lsb = 12};
						enum {FieldMask = 0x007FF000};
						};
					namespace Vs { // Field Description
						enum {Lsb = 1};
						enum {FieldMask = 0x00000002};
						enum {Value_ValidForSupervisorMode = 0x1};
						enum {ValueMask_ValidForSupervisorMode = 0x00000002};
						enum {Value_NotValid = 0x0};
						enum {ValueMask_NotValid = 0x00000000};
						};
					namespace Vp { // Field Description
						enum {Lsb = 0};
						enum {FieldMask = 0x00000001};
						enum {Value_ValidForUserMode = 0x1};
						enum {ValueMask_ValidForUserMode = 0x00000001};
						enum {Value_NotValid = 0x0};
						enum {ValueMask_NotValid = 0x00000000};
						};
					};
				namespace DBATL { // Register description
					typedef uint32_t	Reg;
					namespace BRPN { // Field Description
						enum {Lsb = 17};
						enum {FieldMask = 0xFFFE0000};
						};
					namespace WIMG { // Field Description
						enum {Lsb = 3};
						enum {FieldMask = 0x00000078};
						namespace W { // Field Description
							enum {Lsb = 6};
							enum {FieldMask = 0x00000040};
							enum {Value_WriteThrough = 0x1};
							enum {ValueMask_WriteThrough = 0x00000040};
							enum {Value_WriteBack = 0x0};
							enum {ValueMask_WriteBack = 0x00000000};
							};
						namespace I { // Field Description
							enum {Lsb = 5};
							enum {FieldMask = 0x00000020};
							enum {Value_CacheInhibit = 0x1};
							enum {ValueMask_CacheInhibit = 0x00000020};
							enum {Value_CacheEnabled = 0x0};
							enum {ValueMask_CacheEnabled = 0x00000000};
							};
						namespace M { // Field Description
							enum {Lsb = 4};
							enum {FieldMask = 0x00000010};
							enum {Value_CoherencyEnforced = 0x1};
							enum {ValueMask_CoherencyEnforced = 0x00000010};
							enum {Value_CoherencyNotEnforced = 0x0};
							enum {ValueMask_CoherencyNotEnforced = 0x00000000};
							};
						namespace G { // Field Description
							enum {Lsb = 3};
							enum {FieldMask = 0x00000008};
							enum {Value_Guarded = 0x1};
							enum {ValueMask_Guarded = 0x00000008};
							enum {Value_NotGuarded = 0x0};
							enum {ValueMask_NotGuarded = 0x00000000};
							};
						};
					namespace PP { // Field Description
						enum {Lsb = 0};
						enum {FieldMask = 0x00000003};
						enum {Value_NoAccess = 0x0};
						enum {ValueMask_NoAccess = 0x00000000};
						enum {Value_ReadOnly = 0x1};
						enum {ValueMask_ReadOnly = 0x00000001};
						enum {Value_ReadWrite = 0x2};
						enum {ValueMask_ReadWrite = 0x00000002};
						};
					};
				namespace SDR1 { // Register description
					typedef uint32_t	Reg;
					};
				namespace Twam1 { // Namespace description
					namespace EA { // Register description
						typedef uint32_t	Reg;
						namespace Level1Index { // Field Description
							enum {Lsb = 22};
							enum {FieldMask = 0xFFC00000};
							};
						namespace Level2Index { // Field Description
							enum {Lsb = 12};
							enum {FieldMask = 0x003FF000};
							};
						namespace PageOffset4K { // Field Description
							enum {Lsb = 0};
							enum {FieldMask = 0x00000FFF};
							};
						namespace PageOffset16K { // Field Description
							enum {Lsb = 0};
							enum {FieldMask = 0x00003FFF};
							};
						namespace PageOffset512K { // Field Description
							enum {Lsb = 0};
							enum {FieldMask = 0x0007FFFF};
							};
						namespace PageOffset8M { // Field Description
							enum {Lsb = 0};
							enum {FieldMask = 0x007FFFFF};
							};
						};
					};
				enum {nEntriesInLevel1Twam0Table = 4096};
				enum {nEntriesInLevel1Twam1Table = 1024};
				enum {nEntriesInLevel2Table = 1024};
				namespace Level2Desc { // Register description
					typedef uint32_t	Reg;
					namespace RPN { // Field Description
						enum {Lsb = 12};
						enum {FieldMask = 0xFFFFF000};
						};
					namespace PP { // Field Description
						enum {Lsb = 3};
						enum {FieldMask = 0x00000FF8};
						};
					namespace Ppm0 { // Field Description
						enum {Lsb = 3};
						enum {FieldMask = 0x00000FF8};
						namespace Encoding { // Field Description
							enum {Lsb = 9};
							enum {FieldMask = 0x00000200};
							enum {Value_Ppc = 0x0};
							enum {ValueMask_Ppc = 0x00000000};
							enum {Value_Extended = 0x1};
							enum {ValueMask_Extended = 0x00000200};
							};
						namespace ExtEnc { // Field Description
							enum {Lsb = 9};
							enum {FieldMask = 0x00000E00};
							namespace PP { // Field Description
								enum {Lsb = 9};
								enum {FieldMask = 0x00000E00};
								enum {Value_NoAccess = 0x1};
								enum {ValueMask_NoAccess = 0x00000200};
								enum {Value_SuperReadOnly = 0x3};
								enum {ValueMask_SuperReadOnly = 0x00000600};
								enum {Value_SuperReadWriteOnly = 0x0};
								enum {ValueMask_SuperReadWriteOnly = 0x00000000};
								enum {Value_SuperReadWriteUserReadOnly = 0x2};
								enum {ValueMask_SuperReadWriteUserReadOnly = 0x00000400};
								enum {Value_AllReadWrite = 0x4};
								enum {ValueMask_AllReadWrite = 0x00000800};
								enum {Value_AllReadOnly = 0x6};
								enum {ValueMask_AllReadOnly = 0x00000C00};
								};
							};
						namespace PpcEnc { // Field Description
							enum {Lsb = 9};
							enum {FieldMask = 0x00000E00};
							namespace PP { // Field Description
								enum {Lsb = 10};
								enum {FieldMask = 0x00000C00};
								enum {Value_SuperReadWriteUserNoAccess = 0x0};
								enum {ValueMask_SuperReadWriteUserNoAccess = 0x00000000};
								enum {Value_SuperReadWriteUserReadOnly = 0x1};
								enum {ValueMask_SuperReadWriteUserReadOnly = 0x00000400};
								enum {Value_AllReadWrite = 0x2};
								enum {ValueMask_AllReadWrite = 0x00000800};
								enum {Value_AllReadOnly = 0x3};
								enum {ValueMask_AllReadOnly = 0x00000C00};
								};
							};
						namespace Change { // Field Description
							enum {Lsb = 8};
							enum {FieldMask = 0x00000100};
							enum {Value_Unchanged = 0x0};
							enum {ValueMask_Unchanged = 0x00000000};
							enum {Value_Changed = 0x1};
							enum {ValueMask_Changed = 0x00000100};
							};
						namespace Ppcs0 { // Field Description
							enum {Lsb = 4};
							enum {FieldMask = 0x000000F0};
							namespace Mode3 { // Field Description
								enum {Lsb = 4};
								enum {FieldMask = 0x000000F0};
								enum {Value_SubPage1 = 0x1};
								enum {ValueMask_SubPage1 = 0x00000010};
								enum {Value_SubPage2 = 0x2};
								enum {ValueMask_SubPage2 = 0x00000020};
								enum {Value_SubPage3 = 0x3};
								enum {ValueMask_SubPage3 = 0x00000030};
								enum {Value_SubPage4 = 0x4};
								enum {ValueMask_SubPage4 = 0x00000040};
								};
							namespace Other { // Field Description
								enum {Lsb = 4};
								enum {FieldMask = 0x000000F0};
								enum {Value_Fixed = 0xF};
								enum {ValueMask_Fixed = 0x000000F0};
								};
							};
						namespace Ppcs1 { // Field Description
							enum {Lsb = 4};
							enum {FieldMask = 0x000000F0};
							namespace Compare { // Field Description
								enum {Lsb = 4};
								enum {FieldMask = 0x000000F0};
								enum {Value_HitSuperOnly = 0x8};
								enum {ValueMask_HitSuperOnly = 0x00000080};
								enum {Value_HitUserOnly = 0x4};
								enum {ValueMask_HitUserOnly = 0x00000040};
								enum {Value_HitBoth = 0x6};
								enum {ValueMask_HitBoth = 0x00000060};
								};
							};
						namespace SPS { // Field Description
							enum {Lsb = 3};
							enum {FieldMask = 0x00000008};
							enum {Value_Size4Kbyte = 0x0};
							enum {ValueMask_Size4Kbyte = 0x00000000};
							enum {Value_Size16Kbyte = 0x1};
							enum {ValueMask_Size16Kbyte = 0x00000008};
							};
						};
					namespace Ppm1 { // Field Description
						enum {Lsb = 3};
						enum {FieldMask = 0x00000FF8};
						namespace PP1 { // Field Description
							enum {Lsb = 10};
							enum {FieldMask = 0x00000C00};
							enum {Value_NoAccess = 0x0};
							enum {ValueMask_NoAccess = 0x00000000};
							enum {Value_SuperOnlyReadWrite = 0x1};
							enum {ValueMask_SuperOnlyReadWrite = 0x00000400};
							enum {Value_SuperReadWriteUserReadOnly = 0x2};
							enum {ValueMask_SuperReadWriteUserReadOnly = 0x00000800};
							enum {Value_AllReadWrite = 0x3};
							enum {ValueMask_AllReadWrite = 0x00000C00};
							};
						namespace PP2 { // Field Description
							enum {Lsb = 8};
							enum {FieldMask = 0x00000300};
							enum {Value_NoAccess = 0x0};
							enum {ValueMask_NoAccess = 0x00000000};
							enum {Value_SuperOnlyReadWrite = 0x1};
							enum {ValueMask_SuperOnlyReadWrite = 0x00000100};
							enum {Value_SuperReadWriteUserReadOnly = 0x2};
							enum {ValueMask_SuperReadWriteUserReadOnly = 0x00000200};
							enum {Value_AllReadWrite = 0x3};
							enum {ValueMask_AllReadWrite = 0x00000300};
							};
						namespace PP3 { // Field Description
							enum {Lsb = 6};
							enum {FieldMask = 0x000000C0};
							enum {Value_NoAccess = 0x0};
							enum {ValueMask_NoAccess = 0x00000000};
							enum {Value_SuperOnlyReadWrite = 0x1};
							enum {ValueMask_SuperOnlyReadWrite = 0x00000040};
							enum {Value_SuperReadWriteUserReadOnly = 0x2};
							enum {ValueMask_SuperReadWriteUserReadOnly = 0x00000080};
							enum {Value_AllReadWrite = 0x3};
							enum {ValueMask_AllReadWrite = 0x000000C0};
							};
						namespace PP4 { // Field Description
							enum {Lsb = 4};
							enum {FieldMask = 0x00000030};
							enum {Value_NoAccess = 0x0};
							enum {ValueMask_NoAccess = 0x00000000};
							enum {Value_SuperOnlyReadWrite = 0x1};
							enum {ValueMask_SuperOnlyReadWrite = 0x00000010};
							enum {Value_SuperReadWriteUserReadOnly = 0x2};
							enum {ValueMask_SuperReadWriteUserReadOnly = 0x00000020};
							enum {Value_AllReadWrite = 0x3};
							enum {ValueMask_AllReadWrite = 0x00000030};
							};
						namespace SPS { // Field Description
							enum {Lsb = 3};
							enum {FieldMask = 0x00000008};
							enum {Value_Fixed = 0x0};
							enum {ValueMask_Fixed = 0x00000000};
							};
						};
					namespace SH { // Field Description
						enum {Lsb = 2};
						enum {FieldMask = 0x00000004};
						enum {Value_MatchASID = 0x0};
						enum {ValueMask_MatchASID = 0x00000000};
						enum {Value_IgnoreASID = 0x1};
						enum {ValueMask_IgnoreASID = 0x00000004};
						};
					namespace CI { // Field Description
						enum {Lsb = 1};
						enum {FieldMask = 0x00000002};
						enum {Value_CacheEnabled = 0x0};
						enum {ValueMask_CacheEnabled = 0x00000000};
						enum {Value_CacheInhibited = 0x1};
						enum {ValueMask_CacheInhibited = 0x00000002};
						};
					namespace V { // Field Description
						enum {Lsb = 0};
						enum {FieldMask = 0x00000001};
						enum {Value_Invalid = 0x0};
						enum {ValueMask_Invalid = 0x00000000};
						enum {Value_Valid = 0x1};
						enum {ValueMask_Valid = 0x00000001};
						};
					};
				namespace Level1Desc { // Register description
					typedef uint32_t	Reg;
					namespace L2BA { // Field Description
						enum {Lsb = 12};
						enum {FieldMask = 0xFFFFF000};
						};
					namespace APG { // Field Description
						enum {Lsb = 5};
						enum {FieldMask = 0x000001E0};
						};
					namespace G { // Field Description
						enum {Lsb = 4};
						enum {FieldMask = 0x00000010};
						enum {Value_NonGuarded = 0x0};
						enum {ValueMask_NonGuarded = 0x00000000};
						enum {Value_Guarded = 0x1};
						enum {ValueMask_Guarded = 0x00000010};
						};
					namespace PS { // Field Description
						enum {Lsb = 2};
						enum {FieldMask = 0x0000000C};
						enum {Value_Small = 0x0};
						enum {ValueMask_Small = 0x00000000};
						enum {Value_512K = 0x1};
						enum {ValueMask_512K = 0x00000004};
						enum {Value_8M = 0x3};
						enum {ValueMask_8M = 0x0000000C};
						};
					namespace WT { // Field Description
						enum {Lsb = 1};
						enum {FieldMask = 0x00000002};
						enum {Value_Copyback = 0x0};
						enum {ValueMask_Copyback = 0x00000000};
						enum {Value_Writethrough = 0x0};
						enum {ValueMask_Writethrough = 0x00000000};
						};
					namespace V { // Field Description
						enum {Lsb = 0};
						enum {FieldMask = 0x00000001};
						enum {Value_Invalid = 0x0};
						enum {ValueMask_Invalid = 0x00000000};
						enum {Value_Valid = 0x1};
						enum {ValueMask_Valid = 0x00000001};
						};
					};
				};
			};
		};
	};
#endif
