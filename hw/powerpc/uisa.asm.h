/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _powerpc_uisah_
#define _powerpc_uisah_
#define OsclPpcUisaGPR0	0
#define OsclPpcUisaGPR1	1
#define OsclPpcUisaGPR2	2
#define OsclPpcUisaGPR3	3
#define OsclPpcUisaGPR4	4
#define OsclPpcUisaGPR5	5
#define OsclPpcUisaGPR6	6
#define OsclPpcUisaGPR7	7
#define OsclPpcUisaGPR8	8
#define OsclPpcUisaGPR9	9
#define OsclPpcUisaGPR10	10
#define OsclPpcUisaGPR11	11
#define OsclPpcUisaGPR12	12
#define OsclPpcUisaGPR13	13
#define OsclPpcUisaGPR14	14
#define OsclPpcUisaGPR15	15
#define OsclPpcUisaGPR16	16
#define OsclPpcUisaGPR17	17
#define OsclPpcUisaGPR18	18
#define OsclPpcUisaGPR19	19
#define OsclPpcUisaGPR20	20
#define OsclPpcUisaGPR21	21
#define OsclPpcUisaGPR22	22
#define OsclPpcUisaGPR23	23
#define OsclPpcUisaGPR24	24
#define OsclPpcUisaGPR25	25
#define OsclPpcUisaGPR26	26
#define OsclPpcUisaGPR27	27
#define OsclPpcUisaGPR28	28
#define OsclPpcUisaGPR29	29
#define OsclPpcUisaGPR30	30
#define OsclPpcUisaGPR31	31
#define OsclPpcUisaCR0	0
#define OsclPpcUisaCR1	1
#define OsclPpcUisaCR2	2
#define OsclPpcUisaCR3	3
#define OsclPpcUisaCR4	4
#define OsclPpcUisaCR5	5
#define OsclPpcUisaCR6	6
#define OsclPpcUisaCR7	7
#define OsclPpcUisaCrLt0Lsb	31
#define OsclPpcUisaCrLt0FieldMask	0x80000000
#define OsclPpcUisaCrLt0Value_Negative	0x1
#define OsclPpcUisaCrLt0ValueMask_Negative	0x80000000
#define OsclPpcUisaCrLt0Value_Positive	0x0
#define OsclPpcUisaCrLt0ValueMask_Positive	0x00000000
#define OsclPpcUisaCrLt1Lsb	27
#define OsclPpcUisaCrLt1FieldMask	0x08000000
#define OsclPpcUisaCrLt1Value_Negative	0x1
#define OsclPpcUisaCrLt1ValueMask_Negative	0x08000000
#define OsclPpcUisaCrLt1Value_Positive	0x0
#define OsclPpcUisaCrLt1ValueMask_Positive	0x00000000
#define OsclPpcUisaCrLt2Lsb	23
#define OsclPpcUisaCrLt2FieldMask	0x00800000
#define OsclPpcUisaCrLt2Value_Negative	0x1
#define OsclPpcUisaCrLt2ValueMask_Negative	0x00800000
#define OsclPpcUisaCrLt2Value_Positive	0x0
#define OsclPpcUisaCrLt2ValueMask_Positive	0x00000000
#define OsclPpcUisaCrLt3Lsb	19
#define OsclPpcUisaCrLt3FieldMask	0x00080000
#define OsclPpcUisaCrLt3Value_Negative	0x1
#define OsclPpcUisaCrLt3ValueMask_Negative	0x00080000
#define OsclPpcUisaCrLt3Value_Positive	0x0
#define OsclPpcUisaCrLt3ValueMask_Positive	0x00000000
#define OsclPpcUisaCrLt4Lsb	15
#define OsclPpcUisaCrLt4FieldMask	0x00008000
#define OsclPpcUisaCrLt4Value_Negative	0x1
#define OsclPpcUisaCrLt4ValueMask_Negative	0x00008000
#define OsclPpcUisaCrLt4Value_Positive	0x0
#define OsclPpcUisaCrLt4ValueMask_Positive	0x00000000
#define OsclPpcUisaCrLt5Lsb	11
#define OsclPpcUisaCrLt5FieldMask	0x00000800
#define OsclPpcUisaCrLt5Value_Negative	0x1
#define OsclPpcUisaCrLt5ValueMask_Negative	0x00000800
#define OsclPpcUisaCrLt5Value_Positive	0x0
#define OsclPpcUisaCrLt5ValueMask_Positive	0x00000000
#define OsclPpcUisaCrLt6Lsb	7
#define OsclPpcUisaCrLt6FieldMask	0x00000080
#define OsclPpcUisaCrLt6Value_Negative	0x1
#define OsclPpcUisaCrLt6ValueMask_Negative	0x00000080
#define OsclPpcUisaCrLt6Value_Positive	0x0
#define OsclPpcUisaCrLt6ValueMask_Positive	0x00000000
#define OsclPpcUisaCrLt7Lsb	3
#define OsclPpcUisaCrLt7FieldMask	0x00000008
#define OsclPpcUisaCrLt7Value_Negative	0x1
#define OsclPpcUisaCrLt7ValueMask_Negative	0x00000008
#define OsclPpcUisaCrLt7Value_Positive	0x0
#define OsclPpcUisaCrLt7ValueMask_Positive	0x00000000
#define OsclPpcUisaCrGt0Lsb	30
#define OsclPpcUisaCrGt0FieldMask	0x40000000
#define OsclPpcUisaCrGt0Value_PositiveAndNonZero	0x1
#define OsclPpcUisaCrGt0ValueMask_PositiveAndNonZero	0x40000000
#define OsclPpcUisaCrGt0Value_NegativeOrZero	0x0
#define OsclPpcUisaCrGt0ValueMask_NegativeOrZero	0x00000000
#define OsclPpcUisaCrEq0Lsb	29
#define OsclPpcUisaCrEq0FieldMask	0x20000000
#define OsclPpcUisaCrEq0Value_Zero	0x1
#define OsclPpcUisaCrEq0ValueMask_Zero	0x20000000
#define OsclPpcUisaCrEq0Value_NonZero	0x0
#define OsclPpcUisaCrEq0ValueMask_NonZero	0x00000000
#define OsclPpcUisaCrSo0Lsb	28
#define OsclPpcUisaCrSo0FieldMask	0x10000000
#define OsclPpcUisaCrSo0Value_Overflow	0x1
#define OsclPpcUisaCrSo0ValueMask_Overflow	0x10000000
#define OsclPpcUisaCrSo0Value_Normal	0x0
#define OsclPpcUisaCrSo0ValueMask_Normal	0x00000000
#endif
