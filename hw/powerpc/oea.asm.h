/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _powerpc_oeah_
#define _powerpc_oeah_
#define OsclPpcOeaSprPVR	287
#define OsclPpcOeaSprIBAT0U	528
#define OsclPpcOeaSprIBAT0L	529
#define OsclPpcOeaSprIBAT1U	530
#define OsclPpcOeaSprIBAT1L	531
#define OsclPpcOeaSprIBAT2U	532
#define OsclPpcOeaSprIBAT2L	533
#define OsclPpcOeaSprIBAT3U	534
#define OsclPpcOeaSprIBAT3L	535
#define OsclPpcOeaSprDBAT0U	536
#define OsclPpcOeaSprDBAT0L	537
#define OsclPpcOeaSprDBAT1U	538
#define OsclPpcOeaSprDBAT1L	539
#define OsclPpcOeaSprDBAT2U	540
#define OsclPpcOeaSprDBAT2L	541
#define OsclPpcOeaSprDBAT3U	542
#define OsclPpcOeaSprDBAT3L	543
#define OsclPpcOeaSprSDR1	25
#define OsclPpcOeaSprASR	280
#define OsclPpcOeaSprDAR	19
#define OsclPpcOeaSprDSISR	18
#define OsclPpcOeaSprSPRG0	272
#define OsclPpcOeaSprSPRG1	273
#define OsclPpcOeaSprSPRG2	274
#define OsclPpcOeaSprSPRG3	275
#define OsclPpcOeaSprSRR0	26
#define OsclPpcOeaSprSRR1	27
#define OsclPpcOeaSprFPECR	1022
#define OsclPpcOeaSprTBL	284
#define OsclPpcOeaSprTBU	285
#define OsclPpcOeaSprDEC	22
#define OsclPpcOeaSprPIR	1023
#define OsclPpcOeaSprDABR	1013
#define OsclPpcOeaSprEAR	282
#define OsclPpcOeaMsrPowLsb	18
#define OsclPpcOeaMsrPowFieldMask	0x00040000
#define OsclPpcOeaMsrPowValue_Disabled	0x0
#define OsclPpcOeaMsrPowValueMask_Disabled	0x00000000
#define OsclPpcOeaMsrPowValue_Enabled	0x1
#define OsclPpcOeaMsrPowValueMask_Enabled	0x00040000
#define OsclPpcOeaMsrIleLsb	16
#define OsclPpcOeaMsrIleFieldMask	0x00010000
#define OsclPpcOeaMsrIleValue_BigEndian	0x0
#define OsclPpcOeaMsrIleValueMask_BigEndian	0x00000000
#define OsclPpcOeaMsrIleValue_LittleEndian	0x1
#define OsclPpcOeaMsrIleValueMask_LittleEndian	0x00010000
#define OsclPpcOeaMsrEeLsb	15
#define OsclPpcOeaMsrEeFieldMask	0x00008000
#define OsclPpcOeaMsrEeValue_Disabled	0x0
#define OsclPpcOeaMsrEeValueMask_Disabled	0x00000000
#define OsclPpcOeaMsrEeValue_Enabled	0x1
#define OsclPpcOeaMsrEeValueMask_Enabled	0x00008000
#define OsclPpcOeaMsrPrLsb	14
#define OsclPpcOeaMsrPrFieldMask	0x00004000
#define OsclPpcOeaMsrPrValue_Supervisor	0x0
#define OsclPpcOeaMsrPrValueMask_Supervisor	0x00000000
#define OsclPpcOeaMsrPrValue_User	0x1
#define OsclPpcOeaMsrPrValueMask_User	0x00004000
#define OsclPpcOeaMsrFpLsb	13
#define OsclPpcOeaMsrFpFieldMask	0x00002000
#define OsclPpcOeaMsrFpValue_Disabled	0x0
#define OsclPpcOeaMsrFpValueMask_Disabled	0x00000000
#define OsclPpcOeaMsrFpValue_Enabled	0x1
#define OsclPpcOeaMsrFpValueMask_Enabled	0x00002000
#define OsclPpcOeaMsrMeLsb	12
#define OsclPpcOeaMsrMeFieldMask	0x00001000
#define OsclPpcOeaMsrMeValue_Disabled	0x0
#define OsclPpcOeaMsrMeValueMask_Disabled	0x00000000
#define OsclPpcOeaMsrMeValue_Enabled	0x1
#define OsclPpcOeaMsrMeValueMask_Enabled	0x00001000
#define OsclPpcOeaMsrFe0Lsb	11
#define OsclPpcOeaMsrFe0FieldMask	0x00000800
#define OsclPpcOeaMsrFe0Value_Zero	0x0
#define OsclPpcOeaMsrFe0ValueMask_Zero	0x00000000
#define OsclPpcOeaMsrFe0Value_One	0x1
#define OsclPpcOeaMsrFe0ValueMask_One	0x00000800
#define OsclPpcOeaMsrSeLsb	10
#define OsclPpcOeaMsrSeFieldMask	0x00000400
#define OsclPpcOeaMsrSeValue_Normal	0x0
#define OsclPpcOeaMsrSeValueMask_Normal	0x00000000
#define OsclPpcOeaMsrSeValue_Trace	0x1
#define OsclPpcOeaMsrSeValueMask_Trace	0x00000400
#define OsclPpcOeaMsrBeLsb	9
#define OsclPpcOeaMsrBeFieldMask	0x00000200
#define OsclPpcOeaMsrBeValue_Normal	0x0
#define OsclPpcOeaMsrBeValueMask_Normal	0x00000000
#define OsclPpcOeaMsrBeValue_Trace	0x1
#define OsclPpcOeaMsrBeValueMask_Trace	0x00000200
#define OsclPpcOeaMsrFe1Lsb	8
#define OsclPpcOeaMsrFe1FieldMask	0x00000100
#define OsclPpcOeaMsrFe1Value_Zero	0x0
#define OsclPpcOeaMsrFe1ValueMask_Zero	0x00000000
#define OsclPpcOeaMsrFe1Value_One	0x1
#define OsclPpcOeaMsrFe1ValueMask_One	0x00000100
#define OsclPpcOeaMsrIpLsb	6
#define OsclPpcOeaMsrIpFieldMask	0x00000040
#define OsclPpcOeaMsrIpValue_X000	0x0
#define OsclPpcOeaMsrIpValueMask_X000	0x00000000
#define OsclPpcOeaMsrIpValue_XFFF	0x1
#define OsclPpcOeaMsrIpValueMask_XFFF	0x00000040
#define OsclPpcOeaMsrIrLsb	5
#define OsclPpcOeaMsrIrFieldMask	0x00000020
#define OsclPpcOeaMsrIrValue_Disabled	0x0
#define OsclPpcOeaMsrIrValueMask_Disabled	0x00000000
#define OsclPpcOeaMsrIrValue_Enabled	0x1
#define OsclPpcOeaMsrIrValueMask_Enabled	0x00000020
#define OsclPpcOeaMsrDrLsb	4
#define OsclPpcOeaMsrDrFieldMask	0x00000010
#define OsclPpcOeaMsrDrValue_Disabled	0x0
#define OsclPpcOeaMsrDrValueMask_Disabled	0x00000000
#define OsclPpcOeaMsrDrValue_Enabled	0x1
#define OsclPpcOeaMsrDrValueMask_Enabled	0x00000010
#define OsclPpcOeaMsrRiLsb	1
#define OsclPpcOeaMsrRiFieldMask	0x00000002
#define OsclPpcOeaMsrRiValue_NotRecoverable	0x0
#define OsclPpcOeaMsrRiValueMask_NotRecoverable	0x00000000
#define OsclPpcOeaMsrRiValue_Recoverable	0x1
#define OsclPpcOeaMsrRiValueMask_Recoverable	0x00000002
#define OsclPpcOeaMsrLeLsb	0
#define OsclPpcOeaMsrLeFieldMask	0x00000001
#define OsclPpcOeaMsrLeValue_BigEndian	0x0
#define OsclPpcOeaMsrLeValueMask_BigEndian	0x00000000
#define OsclPpcOeaMsrLeValue_LittleEndian	0x1
#define OsclPpcOeaMsrLeValueMask_LittleEndian	0x00000001
#endif
