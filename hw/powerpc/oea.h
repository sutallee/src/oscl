/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _powerpc_oeah_
#define _powerpc_oeah_
#include <stdint.h>
namespace Oscl { // Namespace description
	namespace Ppc { // Namespace description
		namespace Oea { // Namespace description
			namespace Spr { // Namespace description
				enum {PVR = 287};
				enum {IBAT0U = 528};
				enum {IBAT0L = 529};
				enum {IBAT1U = 530};
				enum {IBAT1L = 531};
				enum {IBAT2U = 532};
				enum {IBAT2L = 533};
				enum {IBAT3U = 534};
				enum {IBAT3L = 535};
				enum {DBAT0U = 536};
				enum {DBAT0L = 537};
				enum {DBAT1U = 538};
				enum {DBAT1L = 539};
				enum {DBAT2U = 540};
				enum {DBAT2L = 541};
				enum {DBAT3U = 542};
				enum {DBAT3L = 543};
				enum {SDR1 = 25};
				enum {ASR = 280};
				enum {DAR = 19};
				enum {DSISR = 18};
				enum {SPRG0 = 272};
				enum {SPRG1 = 273};
				enum {SPRG2 = 274};
				enum {SPRG3 = 275};
				enum {SRR0 = 26};
				enum {SRR1 = 27};
				enum {FPECR = 1022};
				enum {TBL = 284};
				enum {TBU = 285};
				enum {DEC = 22};
				enum {PIR = 1023};
				enum {DABR = 1013};
				enum {EAR = 282};
				};
			namespace Srr0 { // Register description
				typedef uint32_t	Reg;
				};
			namespace Srr1 { // Register description
				typedef uint32_t	Reg;
				};
			namespace Msr { // Register description
				typedef uint32_t	Reg;
				namespace Pow { // Field Description
					enum {Lsb = 18};
					enum {FieldMask = 0x00040000};
					enum {Value_Disabled = 0x0};
					enum {ValueMask_Disabled = 0x00000000};
					enum {Value_Enabled = 0x1};
					enum {ValueMask_Enabled = 0x00040000};
					};
				namespace Ile { // Field Description
					enum {Lsb = 16};
					enum {FieldMask = 0x00010000};
					enum {Value_BigEndian = 0x0};
					enum {ValueMask_BigEndian = 0x00000000};
					enum {Value_LittleEndian = 0x1};
					enum {ValueMask_LittleEndian = 0x00010000};
					};
				namespace Ee { // Field Description
					enum {Lsb = 15};
					enum {FieldMask = 0x00008000};
					enum {Value_Disabled = 0x0};
					enum {ValueMask_Disabled = 0x00000000};
					enum {Value_Enabled = 0x1};
					enum {ValueMask_Enabled = 0x00008000};
					};
				namespace Pr { // Field Description
					enum {Lsb = 14};
					enum {FieldMask = 0x00004000};
					enum {Value_Supervisor = 0x0};
					enum {ValueMask_Supervisor = 0x00000000};
					enum {Value_User = 0x1};
					enum {ValueMask_User = 0x00004000};
					};
				namespace Fp { // Field Description
					enum {Lsb = 13};
					enum {FieldMask = 0x00002000};
					enum {Value_Disabled = 0x0};
					enum {ValueMask_Disabled = 0x00000000};
					enum {Value_Enabled = 0x1};
					enum {ValueMask_Enabled = 0x00002000};
					};
				namespace Me { // Field Description
					enum {Lsb = 12};
					enum {FieldMask = 0x00001000};
					enum {Value_Disabled = 0x0};
					enum {ValueMask_Disabled = 0x00000000};
					enum {Value_Enabled = 0x1};
					enum {ValueMask_Enabled = 0x00001000};
					};
				namespace Fe0 { // Field Description
					enum {Lsb = 11};
					enum {FieldMask = 0x00000800};
					enum {Value_Zero = 0x0};
					enum {ValueMask_Zero = 0x00000000};
					enum {Value_One = 0x1};
					enum {ValueMask_One = 0x00000800};
					};
				namespace Se { // Field Description
					enum {Lsb = 10};
					enum {FieldMask = 0x00000400};
					enum {Value_Normal = 0x0};
					enum {ValueMask_Normal = 0x00000000};
					enum {Value_Trace = 0x1};
					enum {ValueMask_Trace = 0x00000400};
					};
				namespace Be { // Field Description
					enum {Lsb = 9};
					enum {FieldMask = 0x00000200};
					enum {Value_Normal = 0x0};
					enum {ValueMask_Normal = 0x00000000};
					enum {Value_Trace = 0x1};
					enum {ValueMask_Trace = 0x00000200};
					};
				namespace Fe1 { // Field Description
					enum {Lsb = 8};
					enum {FieldMask = 0x00000100};
					enum {Value_Zero = 0x0};
					enum {ValueMask_Zero = 0x00000000};
					enum {Value_One = 0x1};
					enum {ValueMask_One = 0x00000100};
					};
				namespace Ip { // Field Description
					enum {Lsb = 6};
					enum {FieldMask = 0x00000040};
					enum {Value_X000 = 0x0};
					enum {ValueMask_X000 = 0x00000000};
					enum {Value_XFFF = 0x1};
					enum {ValueMask_XFFF = 0x00000040};
					};
				namespace Ir { // Field Description
					enum {Lsb = 5};
					enum {FieldMask = 0x00000020};
					enum {Value_Disabled = 0x0};
					enum {ValueMask_Disabled = 0x00000000};
					enum {Value_Enabled = 0x1};
					enum {ValueMask_Enabled = 0x00000020};
					};
				namespace Dr { // Field Description
					enum {Lsb = 4};
					enum {FieldMask = 0x00000010};
					enum {Value_Disabled = 0x0};
					enum {ValueMask_Disabled = 0x00000000};
					enum {Value_Enabled = 0x1};
					enum {ValueMask_Enabled = 0x00000010};
					};
				namespace Ri { // Field Description
					enum {Lsb = 1};
					enum {FieldMask = 0x00000002};
					enum {Value_NotRecoverable = 0x0};
					enum {ValueMask_NotRecoverable = 0x00000000};
					enum {Value_Recoverable = 0x1};
					enum {ValueMask_Recoverable = 0x00000002};
					};
				namespace Le { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x00000001};
					enum {Value_BigEndian = 0x0};
					enum {ValueMask_BigEndian = 0x00000000};
					enum {Value_LittleEndian = 0x1};
					enum {ValueMask_LittleEndian = 0x00000001};
					};
				};
			namespace Dsisr { // Register description
				typedef uint32_t	Reg;
				};
			namespace Dar { // Register description
				typedef uint32_t	Reg;
				};
			};
		};
	};
#endif
