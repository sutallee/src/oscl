#ifndef _hw_national_uart_pc16550regh_
#define _hw_national_uart_pc16550regh_
#include <stdint.h>
namespace Oscl { // Namespace description
	namespace HW { // Namespace description
		namespace National { // Namespace description
			namespace UART { // Namespace description
				namespace PC16550 { // Namespace description
					namespace RBR { // Register description
						typedef uint8_t	Reg;
						};
					namespace THR { // Register description
						typedef uint8_t	Reg;
						};
					namespace DLL { // Register description
						typedef uint8_t	Reg;
						};
					namespace DLH { // Register description
						typedef uint8_t	Reg;
						};
					namespace IER { // Register description
						typedef uint8_t	Reg;
						namespace DMAE { // Field Description
							enum {Lsb = 7};
							enum {FieldMask = 0x00000080};
							enum {Value_Enable = 0x1};
							enum {ValueMask_Enable = 0x00000080};
							enum {Value_Disable = 0x0};
							enum {ValueMask_Disable = 0x00000000};
							};
						namespace UUE { // Field Description
							enum {Lsb = 6};
							enum {FieldMask = 0x00000040};
							enum {Value_Disable = 0x0};
							enum {ValueMask_Disable = 0x00000000};
							enum {Value_Enable = 0x1};
							enum {ValueMask_Enable = 0x00000040};
							};
						namespace NRZE { // Field Description
							enum {Lsb = 5};
							enum {FieldMask = 0x00000020};
							enum {Value_Enable = 0x1};
							enum {ValueMask_Enable = 0x00000020};
							enum {Value_Disable = 0x0};
							enum {ValueMask_Disable = 0x00000000};
							};
						namespace RTOIE { // Field Description
							enum {Lsb = 4};
							enum {FieldMask = 0x00000010};
							enum {Value_Enable = 0x1};
							enum {ValueMask_Enable = 0x00000010};
							enum {Value_Disable = 0x0};
							enum {ValueMask_Disable = 0x00000000};
							};
						namespace RIE { // Field Description
							enum {Lsb = 3};
							enum {FieldMask = 0x00000008};
							enum {Value_Enable = 0x1};
							enum {ValueMask_Enable = 0x00000008};
							enum {Value_Disable = 0x0};
							enum {ValueMask_Disable = 0x00000000};
							};
						namespace RLSE { // Field Description
							enum {Lsb = 2};
							enum {FieldMask = 0x00000004};
							enum {Value_Enable = 0x1};
							enum {ValueMask_Enable = 0x00000004};
							enum {Value_Disable = 0x0};
							enum {ValueMask_Disable = 0x00000000};
							};
						namespace TIE { // Field Description
							enum {Lsb = 1};
							enum {FieldMask = 0x00000002};
							enum {Value_Enable = 0x1};
							enum {ValueMask_Enable = 0x00000002};
							enum {Value_Disable = 0x0};
							enum {ValueMask_Disable = 0x00000000};
							};
						namespace RAVIE { // Field Description
							enum {Lsb = 0};
							enum {FieldMask = 0x00000001};
							enum {Value_Enable = 0x1};
							enum {ValueMask_Enable = 0x00000001};
							enum {Value_Disable = 0x0};
							enum {ValueMask_Disable = 0x00000000};
							};
						};
					namespace IIR { // Register description
						typedef uint8_t	Reg;
						namespace FIFOES { // Field Description
							enum {Lsb = 6};
							enum {FieldMask = 0x000000C0};
							enum {Value_NonFifoModeIsSelected = 0x0};
							enum {ValueMask_NonFifoModeIsSelected = 0x00000000};
							enum {Value_FifoModeIsSelected = 0x3};
							enum {ValueMask_FifoModeIsSelected = 0x000000C0};
							};
						namespace ID { // Field Description
							enum {Lsb = 0};
							enum {FieldMask = 0x0000000F};
							enum {Value_NonePending = 0x1};
							enum {ValueMask_NonePending = 0x00000001};
							enum {Value_ReceiverLineStatus = 0x6};
							enum {ValueMask_ReceiverLineStatus = 0x00000006};
							enum {Value_ReceivedDataAvailable = 0x4};
							enum {ValueMask_ReceivedDataAvailable = 0x00000004};
							enum {Value_CharacterTimeoutIndication = 0xC};
							enum {ValueMask_CharacterTimeoutIndication = 0x0000000C};
							enum {Value_TransmitterHoldingRegisterEmpty = 0x2};
							enum {ValueMask_TransmitterHoldingRegisterEmpty = 0x00000002};
							enum {Value_ModemStatus = 0x0};
							enum {ValueMask_ModemStatus = 0x00000000};
							};
						};
					namespace FCR { // Register description
						typedef uint8_t	Reg;
						namespace ITL { // Field Description
							enum {Lsb = 6};
							enum {FieldMask = 0x000000C0};
							enum {Value_OneByteOrMore = 0x0};
							enum {ValueMask_OneByteOrMore = 0x00000000};
							enum {Value_FourBytesOrMore = 0x1};
							enum {ValueMask_FourBytesOrMore = 0x00000040};
							enum {Value_EightBytesOrMore = 0x2};
							enum {ValueMask_EightBytesOrMore = 0x00000080};
							enum {Value_FourteenBytesOrMore = 0x3};
							enum {ValueMask_FourteenBytesOrMore = 0x000000C0};
							};
						namespace RESETTF { // Field Description
							enum {Lsb = 2};
							enum {FieldMask = 0x00000004};
							enum {Value_NoEffect = 0x0};
							enum {ValueMask_NoEffect = 0x00000000};
							enum {Value_ClearTxFifo = 0x1};
							enum {ValueMask_ClearTxFifo = 0x00000004};
							};
						namespace RESETRF { // Field Description
							enum {Lsb = 1};
							enum {FieldMask = 0x00000002};
							enum {Value_NoEffect = 0x0};
							enum {ValueMask_NoEffect = 0x00000000};
							enum {Value_ClearRxFifo = 0x1};
							enum {ValueMask_ClearRxFifo = 0x00000002};
							};
						namespace TRFIFOE { // Field Description
							enum {Lsb = 0};
							enum {FieldMask = 0x00000001};
							enum {Value_DisableFifos = 0x0};
							enum {ValueMask_DisableFifos = 0x00000000};
							enum {Value_EnableFifos = 0x1};
							enum {ValueMask_EnableFifos = 0x00000001};
							};
						};
					namespace LCR { // Register description
						typedef uint8_t	Reg;
						namespace DLAB { // Field Description
							enum {Lsb = 7};
							enum {FieldMask = 0x00000080};
							enum {Value_Normal = 0x0};
							enum {ValueMask_Normal = 0x00000000};
							enum {Value_DivisorLatch = 0x1};
							enum {ValueMask_DivisorLatch = 0x00000080};
							};
						namespace SB { // Field Description
							enum {Lsb = 6};
							enum {FieldMask = 0x00000040};
							enum {Value_NoEffect = 0x0};
							enum {ValueMask_NoEffect = 0x00000000};
							enum {Value_ForceTxdToZero = 0x1};
							enum {ValueMask_ForceTxdToZero = 0x00000040};
							};
						namespace STKYP { // Field Description
							enum {Lsb = 5};
							enum {FieldMask = 0x00000020};
							enum {Value_NoEffect = 0x0};
							enum {ValueMask_NoEffect = 0x00000000};
							enum {Value_ForceParityBitToBeOppositeOfEpsBit = 0x1};
							enum {ValueMask_ForceParityBitToBeOppositeOfEpsBit = 0x00000020};
							};
						namespace EPS { // Field Description
							enum {Lsb = 4};
							enum {FieldMask = 0x00000010};
							enum {Value_OddParity = 0x0};
							enum {ValueMask_OddParity = 0x00000000};
							enum {Value_EvenParity = 0x1};
							enum {ValueMask_EvenParity = 0x00000010};
							};
						namespace PEN { // Field Description
							enum {Lsb = 3};
							enum {FieldMask = 0x00000008};
							enum {Value_NoParity = 0x0};
							enum {ValueMask_NoParity = 0x00000000};
							enum {Value_EnableParity = 0x1};
							enum {ValueMask_EnableParity = 0x00000008};
							};
						namespace STB { // Field Description
							enum {Lsb = 2};
							enum {FieldMask = 0x00000004};
							enum {Value_OneStopBit = 0x0};
							enum {ValueMask_OneStopBit = 0x00000000};
							enum {Value_TwoStopBits = 0x1};
							enum {ValueMask_TwoStopBits = 0x00000004};
							};
						namespace WLS { // Field Description
							enum {Lsb = 2};
							enum {FieldMask = 0x00000004};
							enum {Value_FiveBitCharacter = 0x0};
							enum {ValueMask_FiveBitCharacter = 0x00000000};
							enum {Value_SixBitCharacter = 0x1};
							enum {ValueMask_SixBitCharacter = 0x00000004};
							enum {Value_SevenBitCharacter = 0x2};
							enum {ValueMask_SevenBitCharacter = 0x00000008};
							enum {Value_EightBitCharacter = 0x3};
							enum {ValueMask_EightBitCharacter = 0x0000000C};
							};
						};
					namespace MCR { // Register description
						typedef uint8_t	Reg;
						namespace AFE { // Field Description
							enum {Lsb = 5};
							enum {FieldMask = 0x00000020};
							enum {Value_AutoFlowDisable = 0x0};
							enum {ValueMask_AutoFlowDisable = 0x00000000};
							enum {Value_AutoFlowEnable = 0x1};
							enum {ValueMask_AutoFlowEnable = 0x00000020};
							};
						namespace LOOP { // Field Description
							enum {Lsb = 4};
							enum {FieldMask = 0x00000010};
							enum {Value_Normal = 0x0};
							enum {ValueMask_Normal = 0x00000000};
							enum {Value_TestMode = 0x1};
							enum {ValueMask_TestMode = 0x00000010};
							};
						namespace OUT2 { // Field Description
							enum {Lsb = 3};
							enum {FieldMask = 0x00000008};
							enum {Value_Masked = 0x0};
							enum {ValueMask_Masked = 0x00000000};
							enum {Value_NotMasked = 0x1};
							enum {ValueMask_NotMasked = 0x00000008};
							enum {Value_ForceDcdToOne = 0x1};
							enum {ValueMask_ForceDcdToOne = 0x00000008};
							enum {Value_ForceDcdToZero = 0x0};
							enum {ValueMask_ForceDcdToZero = 0x00000000};
							};
						namespace OUT1 { // Field Description
							enum {Lsb = 2};
							enum {FieldMask = 0x00000004};
							enum {Value_ForceRiToOne = 0x1};
							enum {ValueMask_ForceRiToOne = 0x00000004};
							enum {Value_ForceRiToZero = 0x0};
							enum {ValueMask_ForceRiToZero = 0x00000000};
							};
						namespace RTS { // Field Description
							enum {Lsb = 1};
							enum {FieldMask = 0x00000002};
							enum {Value_Negate = 0x0};
							enum {ValueMask_Negate = 0x00000000};
							enum {Value_Assert = 0x1};
							enum {ValueMask_Assert = 0x00000002};
							};
						namespace DTR { // Field Description
							enum {Lsb = 0};
							enum {FieldMask = 0x00000001};
							enum {Value_Negate = 0x0};
							enum {ValueMask_Negate = 0x00000000};
							enum {Value_Assert = 0x1};
							enum {ValueMask_Assert = 0x00000001};
							};
						};
					namespace LSR { // Register description
						typedef uint8_t	Reg;
						namespace FIFOE { // Field Description
							enum {Lsb = 7};
							enum {FieldMask = 0x00000080};
							enum {Value_NoRxFifoErrors = 0x0};
							enum {ValueMask_NoRxFifoErrors = 0x00000000};
							enum {Value_AtLeastOneCharacterInRxFifoHasErrors = 0x1};
							enum {ValueMask_AtLeastOneCharacterInRxFifoHasErrors = 0x00000080};
							};
						namespace TEMT { // Field Description
							enum {Lsb = 6};
							enum {FieldMask = 0x00000040};
							enum {Value_HoldAndShiftRegEmpty = 0x1};
							enum {ValueMask_HoldAndShiftRegEmpty = 0x00000040};
							enum {Value_NotEmpty = 0x0};
							enum {ValueMask_NotEmpty = 0x00000000};
							};
						namespace TDRQ { // Field Description
							enum {Lsb = 5};
							enum {FieldMask = 0x00000020};
							enum {Value_TxReady = 0x1};
							enum {ValueMask_TxReady = 0x00000020};
							enum {Value_NotReady = 0x0};
							enum {ValueMask_NotReady = 0x00000000};
							};
						namespace BI { // Field Description
							enum {Lsb = 4};
							enum {FieldMask = 0x00000010};
							enum {Value_BreakReceived = 0x1};
							enum {ValueMask_BreakReceived = 0x00000010};
							enum {Value_NoBreakReceived = 0x0};
							enum {ValueMask_NoBreakReceived = 0x00000000};
							};
						namespace FE { // Field Description
							enum {Lsb = 3};
							enum {FieldMask = 0x00000008};
							enum {Value_FramingError = 0x1};
							enum {ValueMask_FramingError = 0x00000008};
							enum {Value_NoFramingError = 0x0};
							enum {ValueMask_NoFramingError = 0x00000000};
							};
						namespace PE { // Field Description
							enum {Lsb = 2};
							enum {FieldMask = 0x00000004};
							enum {Value_ParityError = 0x1};
							enum {ValueMask_ParityError = 0x00000004};
							enum {Value_NoParityError = 0x0};
							enum {ValueMask_NoParityError = 0x00000000};
							};
						namespace OE { // Field Description
							enum {Lsb = 1};
							enum {FieldMask = 0x00000002};
							enum {Value_OverrunError = 0x1};
							enum {ValueMask_OverrunError = 0x00000002};
							enum {Value_NoOverrunError = 0x0};
							enum {ValueMask_NoOverrunError = 0x00000000};
							};
						namespace DR { // Field Description
							enum {Lsb = 0};
							enum {FieldMask = 0x00000001};
							enum {Value_DataAvailableInRbrOrFifo = 0x1};
							enum {ValueMask_DataAvailableInRbrOrFifo = 0x00000001};
							enum {Value_NoDataAvailableInRbrOrFifo = 0x0};
							enum {ValueMask_NoDataAvailableInRbrOrFifo = 0x00000000};
							};
						};
					namespace MSR { // Register description
						typedef uint8_t	Reg;
						namespace DCD { // Field Description
							enum {Lsb = 7};
							enum {FieldMask = 0x00000080};
							enum {Value_Asserted = 0x1};
							enum {ValueMask_Asserted = 0x00000080};
							enum {Value_Negated = 0x0};
							enum {ValueMask_Negated = 0x00000000};
							};
						namespace RI { // Field Description
							enum {Lsb = 6};
							enum {FieldMask = 0x00000040};
							enum {Value_Asserted = 0x1};
							enum {ValueMask_Asserted = 0x00000040};
							enum {Value_Negated = 0x0};
							enum {ValueMask_Negated = 0x00000000};
							};
						namespace DSR { // Field Description
							enum {Lsb = 5};
							enum {FieldMask = 0x00000020};
							enum {Value_Asserted = 0x1};
							enum {ValueMask_Asserted = 0x00000020};
							enum {Value_Negated = 0x0};
							enum {ValueMask_Negated = 0x00000000};
							};
						namespace CTS { // Field Description
							enum {Lsb = 4};
							enum {FieldMask = 0x00000010};
							enum {Value_Asserted = 0x1};
							enum {ValueMask_Asserted = 0x00000010};
							enum {Value_Negated = 0x0};
							enum {ValueMask_Negated = 0x00000000};
							};
						namespace DDCD { // Field Description
							enum {Lsb = 3};
							enum {FieldMask = 0x00000008};
							enum {Value_ChangedState = 0x1};
							enum {ValueMask_ChangedState = 0x00000008};
							enum {Value_NoChange = 0x0};
							enum {ValueMask_NoChange = 0x00000000};
							};
						namespace TERI { // Field Description
							enum {Lsb = 2};
							enum {FieldMask = 0x00000004};
							enum {Value_ChangedFromAssertToNegate = 0x1};
							enum {ValueMask_ChangedFromAssertToNegate = 0x00000004};
							enum {Value_NoChange = 0x0};
							enum {ValueMask_NoChange = 0x00000000};
							};
						namespace DDSR { // Field Description
							enum {Lsb = 1};
							enum {FieldMask = 0x00000002};
							enum {Value_ChangedState = 0x1};
							enum {ValueMask_ChangedState = 0x00000002};
							enum {Value_NoChange = 0x0};
							enum {ValueMask_NoChange = 0x00000000};
							};
						namespace DCTS { // Field Description
							enum {Lsb = 0};
							enum {FieldMask = 0x00000001};
							enum {Value_ChangedState = 0x1};
							enum {ValueMask_ChangedState = 0x00000001};
							enum {Value_NoChange = 0x0};
							enum {ValueMask_NoChange = 0x00000000};
							};
						};
					namespace SPR { // Register description
						typedef uint8_t	Reg;
						};
					};
				};
			};
		};
	};
#endif
