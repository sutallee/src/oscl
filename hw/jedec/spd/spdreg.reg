/*
   Copyright (C) 2006 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/


module oscl_hw_jedec_spd_spdh {
	include "oscl/compiler/types.h";
	namespace Oscl {
		namespace Hw {
			namespace JEDEC {
				namespace SPD {
					register FundamentalMemoryType uint8_t {
						value FpmDRAM				1
						value EDO					2
						value PipelinedNibble		3
						value SdrSDRAM				4
						value ROM					5
						value DdrSGRAM				6
						value DdrSDRAM				7
						}
					register NumOfRowAddresses uint8_t {
						field SubfieldA 0 4 {
							}
						field SubfieldB 4 4 {
							value NoAsymetricPhysicalBank	0
							value OneFor16					1

							}
						}
					register NumOfColumnAddresses uint8_t {
						field SubfieldA 0 4 {
							}
						field SubfieldB 4 4 {
							value NoAsymetricPhysicalBank	0
							value OneFor16					1

							}
						}
					register VoltageInterfaceLevel uint8_t {
						value TTL5voltTolerant		0
						value LVTTLNot5voltTolerant	1
						value HSTL1_5volt			2
						value SSTL3_3volt			3
						value SSTL2_5volt			4
						}
					register tCKmin uint8_t {
						field Nanoseconds 4 4 { }
						field TenthsOfNanoseconds 0 4 { }
						}
					register tAC uint8_t {
						field TenthsNanoseconds 4 4 { }
						field HundrethsOfNanoseconds 0 4 { }
						}
					register DimmConfigurationType uint8_t {
						value None					0
						value Parity				1
						value ECC					2
						}
					register RefreshRateType uint8_t {
						field SelfRefreshFlag 7 1 {
							value SelfRefreshUnsupported	0
							value SelfRefreshSupported		1
							}
						field Rate 0 7 {
							value Normal15_625us	0
							value Reduced_25x		1
							value Reduced_5x		2
							value Extended2x		3
							value Extended4x		4
							value Extended8x		5
							}
						}
					register PrimarySdramWidth uint8_t {
						field DiffSizeBankFlag 7 1 {
							value OneBankOrSameSizeBank		0
							value DifferentSizeBankPresent	1
							}
						field Width 0 7 { }
						}
					register ErrorCheckingSdramWidth uint8_t {
						field DiffSizeBankFlag 7 1 {
							value OneBankOrSameSizeBank		0
							value DifferentSizeBankPresent	1
							}
						field Width 0 7 { }
						}
					register SupportedBurstLengths uint8_t {
						field BurstLength1 0 1 {
							value NotSupported	0
							value Supported		1
							}
						field BurstLength2 1 1 {
							value NotSupported	0
							value Supported		1
							}
						field BurstLength4 2 1 {
							value NotSupported	0
							value Supported		1
							}
						field BurstLength8 3 1 {
							value NotSupported	0
							value Supported		1
							}
						field BurstLengthPage 7 1 {
							value NotSupported	0
							value Supported		1
							}
						}
					register CasLatency uint8_t {
						field CasLatency1_0 0 1 {
							value NotSupported	0
							value Supported		1
							}
						field CasLatency1_5 1 1 {
							value NotSupported	0
							value Supported		1
							}
						field CasLatency2_0 2 1 {
							value NotSupported	0
							value Supported		1
							}
						field CasLatency2_5 3 1 {
							value NotSupported	0
							value Supported		1
							}
						field CasLatency3_0 4 1 {
							value NotSupported	0
							value Supported		1
							}
						field CasLatency3_5 5 1 {
							value NotSupported	0
							value Supported		1
							}
						}
					register ChipSelectLatency uint8_t {
						field Latency0 0 1 {
							value NotSupported	0
							value Supported		1
							}
						field Latency1 1 1 {
							value NotSupported	0
							value Supported		1
							}
						field Latency2 2 1 {
							value NotSupported	0
							value Supported		1
							}
						field Latency3 3 1 {
							value NotSupported	0
							value Supported		1
							}
						field Latency4 4 1 {
							value NotSupported	0
							value Supported		1
							}
						field Latency5 5 1 {
							value NotSupported	0
							value Supported		1
							}
						field Latency6 6 1 {
							value NotSupported	0
							value Supported		1
							}
						}
					register WriteLatency uint8_t {
						field Latency0 0 1 {
							value NotSupported	0
							value Supported		1
							}
						field Latency1 1 1 {
							value NotSupported	0
							value Supported		1
							}
						field Latency2 2 1 {
							value NotSupported	0
							value Supported		1
							}
						field Latency3 3 1 {
							value NotSupported	0
							value Supported		1
							}
						}
					register ModuleAttributes uint8_t {
						field BufferedAddressAndControlInputs 0 1 {
							value NotIncluded	0
							value Included		1
							}
						field RegisteredAddressAndControlInputs 1 1 {
							value NotIncluded	0
							value Included		1
							}
						field OnCardPLL 2 1 {
							value NotPresent	0
							value Present		1
							}
						field FetSwitchOnCardEnable 3 1 {
							value NotIncluded	0
							value Included		1
							}
						field FetSwitchExternalEnable 4 1 {
							value NotIncluded	0
							value Included		1
							}
						field DifferentialClockInput 5 1 {
							value NotIncluded	0
							value Included		1
							}
						}
					register SdramDeviceAttributes uint8_t {
						field WeakDriver 0 1 {
							value NotIncluded	0
							value Included		1
							}
						field LowerVddTolerance 4 1 {
							value Volts0_2	0
							value TBD		1
							}
						field UpperVddTolerance 5 1 {
							value Volts0_2	0
							value TBD		1
							}
						field ConcurrentAutoPrecharge 6 1 {
							value NotSupported	0
							value Supported		1
							}
						field FastAP 7 1 {
							value tRapIstRas	0
							value tRapIstRcd	1
							}
						}
					register MinimumClockCycleAtClx05 uint8_t {
						field Nanoseconds 4 4 { }
						field TenthsOfNanoseconds 0 4 { }
						}
					register TAcAtCLX05 uint8_t {
						field TenthsOfNanoseconds 4 4 { }
						field HundredthsOfNanoseconds 0 4 { }
						}
					register MinimumClockCycleAtCLX1 uint8_t {
						field Nanoseconds 4 4 { }
						field TenthsOfNanoseconds 0 4 { }
						}
					register TAcAtCLX1 uint8_t {
						field TenthsOfNanoseconds 4 4 { }
						field HundredthsOfNanoseconds 0 4 { }
						}
					register TRP uint8_t {
						field Nanoseconds 2 6 { }
						field FourthsOfNanoseconds 0 2 { }
						}
					register TRRD uint8_t {
						field Nanoseconds 2 6 { }
						field FourthsOfNanoseconds 0 2 { }
						}
					register TRCD uint8_t {
						field Nanoseconds 2 6 { }
						field FourthsOfNanoseconds 0 2 { }
						}
					register TRAS uint8_t {}
					register ModuleBankDensity uint8_t {
						field Size1GB 0 1 {
							value NotSupported	0
							value Supported		1
							}
						field Size2GB 1 1 {
							value NotSupported	0
							value Supported		1
							}
						field Size16MB 2 1 {
							value NotSupported	0
							value Supported		1
							}
						field Size32MB 3 1 {
							value NotSupported	0
							value Supported		1
							}
						field Size64MB 4 1 {
							value NotSupported	0
							value Supported		1
							}
						field Size128MB 5 1 {
							value NotSupported	0
							value Supported		1
							}
						field Size256MB 6 1 {
							value NotSupported	0
							value Supported		1
							}
						field Size512MB 7 1 {
							value NotSupported	0
							value Supported		1
							}
						}
					register AddressAndCommandInputSetupTimeBeforeClock uint8_t {
						field TenthsOfNanoseconds 4 4 { }
						field HundredthsOfNanoseconds 0 4 { }
						}
					register AddressAndCommandInputHoldTimeAfterClock uint8_t {
						field TenthsOfNanoseconds 4 4 { }
						field HundredthsOfNanoseconds 0 4 { }
						}
					register SdramDeviceDataMaskInputSetupTimeBeforeDataStrobe uint8_t {
						field TenthsOfNanoseconds 4 4 { }
						field HundredthsOfNanoseconds 0 4 { }
						}
					register SdramDeviceDataMaskInputHoldTimeAfterDataStrobe uint8_t {
						field TenthsOfNanoseconds 4 4 { }
						field HundredthsOfNanoseconds 0 4 { }
						}
					register TRC uint8_t {}
					}
				}
			}
		}
	}

