#ifndef _oscl_hw_jedec_spd_spdh_
#define _oscl_hw_jedec_spd_spdh_
#include <stdint.h>
namespace Oscl { // Namespace description
	namespace Hw { // Namespace description
		namespace JEDEC { // Namespace description
			namespace SPD { // Namespace description
				namespace FundamentalMemoryType { // Register description
					typedef uint8_t	Reg;
					enum {FpmDRAM = 1};
					enum {EDO = 2};
					enum {PipelinedNibble = 3};
					enum {SdrSDRAM = 4};
					enum {ROM = 5};
					enum {DdrSGRAM = 6};
					enum {DdrSDRAM = 7};
					};
				namespace NumOfRowAddresses { // Register description
					typedef uint8_t	Reg;
					namespace SubfieldA { // Field Description
						enum {Lsb = 0};
						enum {FieldMask = 0x0000000F};
						};
					namespace SubfieldB { // Field Description
						enum {Lsb = 4};
						enum {FieldMask = 0x000000F0};
						enum {Value_NoAsymetricPhysicalBank = 0x0};
						enum {ValueMask_NoAsymetricPhysicalBank = 0x00000000};
						enum {Value_OneFor16 = 0x1};
						enum {ValueMask_OneFor16 = 0x00000010};
						};
					};
				namespace NumOfColumnAddresses { // Register description
					typedef uint8_t	Reg;
					namespace SubfieldA { // Field Description
						enum {Lsb = 0};
						enum {FieldMask = 0x0000000F};
						};
					namespace SubfieldB { // Field Description
						enum {Lsb = 4};
						enum {FieldMask = 0x000000F0};
						enum {Value_NoAsymetricPhysicalBank = 0x0};
						enum {ValueMask_NoAsymetricPhysicalBank = 0x00000000};
						enum {Value_OneFor16 = 0x1};
						enum {ValueMask_OneFor16 = 0x00000010};
						};
					};
				namespace VoltageInterfaceLevel { // Register description
					typedef uint8_t	Reg;
					enum {TTL5voltTolerant = 0};
					enum {LVTTLNot5voltTolerant = 1};
					enum {HSTL1_5volt = 2};
					enum {SSTL3_3volt = 3};
					enum {SSTL2_5volt = 4};
					};
				namespace tCKmin { // Register description
					typedef uint8_t	Reg;
					namespace Nanoseconds { // Field Description
						enum {Lsb = 4};
						enum {FieldMask = 0x000000F0};
						};
					namespace TenthsOfNanoseconds { // Field Description
						enum {Lsb = 0};
						enum {FieldMask = 0x0000000F};
						};
					};
				namespace tAC { // Register description
					typedef uint8_t	Reg;
					namespace TenthsNanoseconds { // Field Description
						enum {Lsb = 4};
						enum {FieldMask = 0x000000F0};
						};
					namespace HundrethsOfNanoseconds { // Field Description
						enum {Lsb = 0};
						enum {FieldMask = 0x0000000F};
						};
					};
				namespace DimmConfigurationType { // Register description
					typedef uint8_t	Reg;
					enum {None = 0};
					enum {Parity = 1};
					enum {ECC = 2};
					};
				namespace RefreshRateType { // Register description
					typedef uint8_t	Reg;
					namespace SelfRefreshFlag { // Field Description
						enum {Lsb = 7};
						enum {FieldMask = 0x00000080};
						enum {Value_SelfRefreshUnsupported = 0x0};
						enum {ValueMask_SelfRefreshUnsupported = 0x00000000};
						enum {Value_SelfRefreshSupported = 0x1};
						enum {ValueMask_SelfRefreshSupported = 0x00000080};
						};
					namespace Rate { // Field Description
						enum {Lsb = 0};
						enum {FieldMask = 0x0000007F};
						enum {Value_Normal15_625us = 0x0};
						enum {ValueMask_Normal15_625us = 0x00000000};
						enum {Value_Reduced_25x = 0x1};
						enum {ValueMask_Reduced_25x = 0x00000001};
						enum {Value_Reduced_5x = 0x2};
						enum {ValueMask_Reduced_5x = 0x00000002};
						enum {Value_Extended2x = 0x3};
						enum {ValueMask_Extended2x = 0x00000003};
						enum {Value_Extended4x = 0x4};
						enum {ValueMask_Extended4x = 0x00000004};
						enum {Value_Extended8x = 0x5};
						enum {ValueMask_Extended8x = 0x00000005};
						};
					};
				namespace PrimarySdramWidth { // Register description
					typedef uint8_t	Reg;
					namespace DiffSizeBankFlag { // Field Description
						enum {Lsb = 7};
						enum {FieldMask = 0x00000080};
						enum {Value_OneBankOrSameSizeBank = 0x0};
						enum {ValueMask_OneBankOrSameSizeBank = 0x00000000};
						enum {Value_DifferentSizeBankPresent = 0x1};
						enum {ValueMask_DifferentSizeBankPresent = 0x00000080};
						};
					namespace Width { // Field Description
						enum {Lsb = 0};
						enum {FieldMask = 0x0000007F};
						};
					};
				namespace ErrorCheckingSdramWidth { // Register description
					typedef uint8_t	Reg;
					namespace DiffSizeBankFlag { // Field Description
						enum {Lsb = 7};
						enum {FieldMask = 0x00000080};
						enum {Value_OneBankOrSameSizeBank = 0x0};
						enum {ValueMask_OneBankOrSameSizeBank = 0x00000000};
						enum {Value_DifferentSizeBankPresent = 0x1};
						enum {ValueMask_DifferentSizeBankPresent = 0x00000080};
						};
					namespace Width { // Field Description
						enum {Lsb = 0};
						enum {FieldMask = 0x0000007F};
						};
					};
				namespace SupportedBurstLengths { // Register description
					typedef uint8_t	Reg;
					namespace BurstLength1 { // Field Description
						enum {Lsb = 0};
						enum {FieldMask = 0x00000001};
						enum {Value_NotSupported = 0x0};
						enum {ValueMask_NotSupported = 0x00000000};
						enum {Value_Supported = 0x1};
						enum {ValueMask_Supported = 0x00000001};
						};
					namespace BurstLength2 { // Field Description
						enum {Lsb = 1};
						enum {FieldMask = 0x00000002};
						enum {Value_NotSupported = 0x0};
						enum {ValueMask_NotSupported = 0x00000000};
						enum {Value_Supported = 0x1};
						enum {ValueMask_Supported = 0x00000002};
						};
					namespace BurstLength4 { // Field Description
						enum {Lsb = 2};
						enum {FieldMask = 0x00000004};
						enum {Value_NotSupported = 0x0};
						enum {ValueMask_NotSupported = 0x00000000};
						enum {Value_Supported = 0x1};
						enum {ValueMask_Supported = 0x00000004};
						};
					namespace BurstLength8 { // Field Description
						enum {Lsb = 3};
						enum {FieldMask = 0x00000008};
						enum {Value_NotSupported = 0x0};
						enum {ValueMask_NotSupported = 0x00000000};
						enum {Value_Supported = 0x1};
						enum {ValueMask_Supported = 0x00000008};
						};
					namespace BurstLengthPage { // Field Description
						enum {Lsb = 7};
						enum {FieldMask = 0x00000080};
						enum {Value_NotSupported = 0x0};
						enum {ValueMask_NotSupported = 0x00000000};
						enum {Value_Supported = 0x1};
						enum {ValueMask_Supported = 0x00000080};
						};
					};
				namespace CasLatency { // Register description
					typedef uint8_t	Reg;
					namespace CasLatency1_0 { // Field Description
						enum {Lsb = 0};
						enum {FieldMask = 0x00000001};
						enum {Value_NotSupported = 0x0};
						enum {ValueMask_NotSupported = 0x00000000};
						enum {Value_Supported = 0x1};
						enum {ValueMask_Supported = 0x00000001};
						};
					namespace CasLatency1_5 { // Field Description
						enum {Lsb = 1};
						enum {FieldMask = 0x00000002};
						enum {Value_NotSupported = 0x0};
						enum {ValueMask_NotSupported = 0x00000000};
						enum {Value_Supported = 0x1};
						enum {ValueMask_Supported = 0x00000002};
						};
					namespace CasLatency2_0 { // Field Description
						enum {Lsb = 2};
						enum {FieldMask = 0x00000004};
						enum {Value_NotSupported = 0x0};
						enum {ValueMask_NotSupported = 0x00000000};
						enum {Value_Supported = 0x1};
						enum {ValueMask_Supported = 0x00000004};
						};
					namespace CasLatency2_5 { // Field Description
						enum {Lsb = 3};
						enum {FieldMask = 0x00000008};
						enum {Value_NotSupported = 0x0};
						enum {ValueMask_NotSupported = 0x00000000};
						enum {Value_Supported = 0x1};
						enum {ValueMask_Supported = 0x00000008};
						};
					namespace CasLatency3_0 { // Field Description
						enum {Lsb = 4};
						enum {FieldMask = 0x00000010};
						enum {Value_NotSupported = 0x0};
						enum {ValueMask_NotSupported = 0x00000000};
						enum {Value_Supported = 0x1};
						enum {ValueMask_Supported = 0x00000010};
						};
					namespace CasLatency3_5 { // Field Description
						enum {Lsb = 5};
						enum {FieldMask = 0x00000020};
						enum {Value_NotSupported = 0x0};
						enum {ValueMask_NotSupported = 0x00000000};
						enum {Value_Supported = 0x1};
						enum {ValueMask_Supported = 0x00000020};
						};
					};
				namespace ChipSelectLatency { // Register description
					typedef uint8_t	Reg;
					namespace Latency0 { // Field Description
						enum {Lsb = 0};
						enum {FieldMask = 0x00000001};
						enum {Value_NotSupported = 0x0};
						enum {ValueMask_NotSupported = 0x00000000};
						enum {Value_Supported = 0x1};
						enum {ValueMask_Supported = 0x00000001};
						};
					namespace Latency1 { // Field Description
						enum {Lsb = 1};
						enum {FieldMask = 0x00000002};
						enum {Value_NotSupported = 0x0};
						enum {ValueMask_NotSupported = 0x00000000};
						enum {Value_Supported = 0x1};
						enum {ValueMask_Supported = 0x00000002};
						};
					namespace Latency2 { // Field Description
						enum {Lsb = 2};
						enum {FieldMask = 0x00000004};
						enum {Value_NotSupported = 0x0};
						enum {ValueMask_NotSupported = 0x00000000};
						enum {Value_Supported = 0x1};
						enum {ValueMask_Supported = 0x00000004};
						};
					namespace Latency3 { // Field Description
						enum {Lsb = 3};
						enum {FieldMask = 0x00000008};
						enum {Value_NotSupported = 0x0};
						enum {ValueMask_NotSupported = 0x00000000};
						enum {Value_Supported = 0x1};
						enum {ValueMask_Supported = 0x00000008};
						};
					namespace Latency4 { // Field Description
						enum {Lsb = 4};
						enum {FieldMask = 0x00000010};
						enum {Value_NotSupported = 0x0};
						enum {ValueMask_NotSupported = 0x00000000};
						enum {Value_Supported = 0x1};
						enum {ValueMask_Supported = 0x00000010};
						};
					namespace Latency5 { // Field Description
						enum {Lsb = 5};
						enum {FieldMask = 0x00000020};
						enum {Value_NotSupported = 0x0};
						enum {ValueMask_NotSupported = 0x00000000};
						enum {Value_Supported = 0x1};
						enum {ValueMask_Supported = 0x00000020};
						};
					namespace Latency6 { // Field Description
						enum {Lsb = 6};
						enum {FieldMask = 0x00000040};
						enum {Value_NotSupported = 0x0};
						enum {ValueMask_NotSupported = 0x00000000};
						enum {Value_Supported = 0x1};
						enum {ValueMask_Supported = 0x00000040};
						};
					};
				namespace WriteLatency { // Register description
					typedef uint8_t	Reg;
					namespace Latency0 { // Field Description
						enum {Lsb = 0};
						enum {FieldMask = 0x00000001};
						enum {Value_NotSupported = 0x0};
						enum {ValueMask_NotSupported = 0x00000000};
						enum {Value_Supported = 0x1};
						enum {ValueMask_Supported = 0x00000001};
						};
					namespace Latency1 { // Field Description
						enum {Lsb = 1};
						enum {FieldMask = 0x00000002};
						enum {Value_NotSupported = 0x0};
						enum {ValueMask_NotSupported = 0x00000000};
						enum {Value_Supported = 0x1};
						enum {ValueMask_Supported = 0x00000002};
						};
					namespace Latency2 { // Field Description
						enum {Lsb = 2};
						enum {FieldMask = 0x00000004};
						enum {Value_NotSupported = 0x0};
						enum {ValueMask_NotSupported = 0x00000000};
						enum {Value_Supported = 0x1};
						enum {ValueMask_Supported = 0x00000004};
						};
					namespace Latency3 { // Field Description
						enum {Lsb = 3};
						enum {FieldMask = 0x00000008};
						enum {Value_NotSupported = 0x0};
						enum {ValueMask_NotSupported = 0x00000000};
						enum {Value_Supported = 0x1};
						enum {ValueMask_Supported = 0x00000008};
						};
					};
				namespace ModuleAttributes { // Register description
					typedef uint8_t	Reg;
					namespace BufferedAddressAndControlInputs { // Field Description
						enum {Lsb = 0};
						enum {FieldMask = 0x00000001};
						enum {Value_NotIncluded = 0x0};
						enum {ValueMask_NotIncluded = 0x00000000};
						enum {Value_Included = 0x1};
						enum {ValueMask_Included = 0x00000001};
						};
					namespace RegisteredAddressAndControlInputs { // Field Description
						enum {Lsb = 1};
						enum {FieldMask = 0x00000002};
						enum {Value_NotIncluded = 0x0};
						enum {ValueMask_NotIncluded = 0x00000000};
						enum {Value_Included = 0x1};
						enum {ValueMask_Included = 0x00000002};
						};
					namespace OnCardPLL { // Field Description
						enum {Lsb = 2};
						enum {FieldMask = 0x00000004};
						enum {Value_NotPresent = 0x0};
						enum {ValueMask_NotPresent = 0x00000000};
						enum {Value_Present = 0x1};
						enum {ValueMask_Present = 0x00000004};
						};
					namespace FetSwitchOnCardEnable { // Field Description
						enum {Lsb = 3};
						enum {FieldMask = 0x00000008};
						enum {Value_NotIncluded = 0x0};
						enum {ValueMask_NotIncluded = 0x00000000};
						enum {Value_Included = 0x1};
						enum {ValueMask_Included = 0x00000008};
						};
					namespace FetSwitchExternalEnable { // Field Description
						enum {Lsb = 4};
						enum {FieldMask = 0x00000010};
						enum {Value_NotIncluded = 0x0};
						enum {ValueMask_NotIncluded = 0x00000000};
						enum {Value_Included = 0x1};
						enum {ValueMask_Included = 0x00000010};
						};
					namespace DifferentialClockInput { // Field Description
						enum {Lsb = 5};
						enum {FieldMask = 0x00000020};
						enum {Value_NotIncluded = 0x0};
						enum {ValueMask_NotIncluded = 0x00000000};
						enum {Value_Included = 0x1};
						enum {ValueMask_Included = 0x00000020};
						};
					};
				namespace SdramDeviceAttributes { // Register description
					typedef uint8_t	Reg;
					namespace WeakDriver { // Field Description
						enum {Lsb = 0};
						enum {FieldMask = 0x00000001};
						enum {Value_NotIncluded = 0x0};
						enum {ValueMask_NotIncluded = 0x00000000};
						enum {Value_Included = 0x1};
						enum {ValueMask_Included = 0x00000001};
						};
					namespace LowerVddTolerance { // Field Description
						enum {Lsb = 4};
						enum {FieldMask = 0x00000010};
						enum {Value_Volts0_2 = 0x0};
						enum {ValueMask_Volts0_2 = 0x00000000};
						enum {Value_TBD = 0x1};
						enum {ValueMask_TBD = 0x00000010};
						};
					namespace UpperVddTolerance { // Field Description
						enum {Lsb = 5};
						enum {FieldMask = 0x00000020};
						enum {Value_Volts0_2 = 0x0};
						enum {ValueMask_Volts0_2 = 0x00000000};
						enum {Value_TBD = 0x1};
						enum {ValueMask_TBD = 0x00000020};
						};
					namespace ConcurrentAutoPrecharge { // Field Description
						enum {Lsb = 6};
						enum {FieldMask = 0x00000040};
						enum {Value_NotSupported = 0x0};
						enum {ValueMask_NotSupported = 0x00000000};
						enum {Value_Supported = 0x1};
						enum {ValueMask_Supported = 0x00000040};
						};
					namespace FastAP { // Field Description
						enum {Lsb = 7};
						enum {FieldMask = 0x00000080};
						enum {Value_tRapIstRas = 0x0};
						enum {ValueMask_tRapIstRas = 0x00000000};
						enum {Value_tRapIstRcd = 0x1};
						enum {ValueMask_tRapIstRcd = 0x00000080};
						};
					};
				namespace MinimumClockCycleAtClx05 { // Register description
					typedef uint8_t	Reg;
					namespace Nanoseconds { // Field Description
						enum {Lsb = 4};
						enum {FieldMask = 0x000000F0};
						};
					namespace TenthsOfNanoseconds { // Field Description
						enum {Lsb = 0};
						enum {FieldMask = 0x0000000F};
						};
					};
				namespace TAcAtCLX05 { // Register description
					typedef uint8_t	Reg;
					namespace TenthsOfNanoseconds { // Field Description
						enum {Lsb = 4};
						enum {FieldMask = 0x000000F0};
						};
					namespace HundredthsOfNanoseconds { // Field Description
						enum {Lsb = 0};
						enum {FieldMask = 0x0000000F};
						};
					};
				namespace MinimumClockCycleAtCLX1 { // Register description
					typedef uint8_t	Reg;
					namespace Nanoseconds { // Field Description
						enum {Lsb = 4};
						enum {FieldMask = 0x000000F0};
						};
					namespace TenthsOfNanoseconds { // Field Description
						enum {Lsb = 0};
						enum {FieldMask = 0x0000000F};
						};
					};
				};
			};
		};
	};
#endif
