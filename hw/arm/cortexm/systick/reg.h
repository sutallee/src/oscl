/*
   Copyright (C) 2024 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _hw_arm_cortexm_systick_regh_
#define _hw_arm_cortexm_systick_regh_
#include <stdint.h>
namespace Oscl { // Namespace description
	namespace Arm { // Namespace description
		namespace CortexM { // Namespace description
			namespace pSYST { // Namespace description
				namespace rCSR { // Register description
					typedef uint32_t	Reg;
					constexpr Reg	valueAtReset = 0x00000004;
					namespace fENABLE { // Field Description
						constexpr Reg Lsb = 0;
						constexpr Reg FieldMask = 0x00000001;
						constexpr Reg Value_valueAtReset = 0x0;
						constexpr Reg ValueMask_valueAtReset = 0x00000000;
						constexpr Reg Value_CounterDisabled = 0x0;
						constexpr Reg ValueMask_CounterDisabled = 0x00000000;
						constexpr Reg Value_CounterDisable = 0x0;
						constexpr Reg ValueMask_CounterDisable = 0x00000000;
						constexpr Reg Value_CounterEnabled = 0x1;
						constexpr Reg ValueMask_CounterEnabled = 0x00000001;
						constexpr Reg Value_CounterEnable = 0x1;
						constexpr Reg ValueMask_CounterEnable = 0x00000001;
						};
					namespace fTICKINT { // Field Description
						constexpr Reg Lsb = 1;
						constexpr Reg FieldMask = 0x00000002;
						constexpr Reg Value_valueAtReset = 0x0;
						constexpr Reg ValueMask_valueAtReset = 0x00000000;
						constexpr Reg Value_CountToZeroExceptionDisabled = 0x0;
						constexpr Reg ValueMask_CountToZeroExceptionDisabled = 0x00000000;
						constexpr Reg Value_CountToZeroExceptionDisable = 0x0;
						constexpr Reg ValueMask_CountToZeroExceptionDisable = 0x00000000;
						constexpr Reg Value_CountToZeroExceptionEnabled = 0x1;
						constexpr Reg ValueMask_CountToZeroExceptionEnabled = 0x00000002;
						constexpr Reg Value_CountToZeroExceptionEnable = 0x1;
						constexpr Reg ValueMask_CountToZeroExceptionEnable = 0x00000002;
						};
					namespace fCLKSOURCE { // Field Description
						constexpr Reg Lsb = 2;
						constexpr Reg FieldMask = 0x00000004;
						constexpr Reg Value_valueAtReset = 0x1;
						constexpr Reg ValueMask_valueAtReset = 0x00000004;
						constexpr Reg Value_ExternalClock = 0x0;
						constexpr Reg ValueMask_ExternalClock = 0x00000000;
						constexpr Reg Value_ProcessorClock = 0x1;
						constexpr Reg ValueMask_ProcessorClock = 0x00000004;
						};
					namespace fCOUNTFLAG { // Field Description
						constexpr Reg Lsb = 16;
						constexpr Reg FieldMask = 0x00010000;
						constexpr Reg Value_valueAtReset = 0x0;
						constexpr Reg ValueMask_valueAtReset = 0x00000000;
						constexpr Reg Value_TimerHasNotCountedToZero = 0x0;
						constexpr Reg ValueMask_TimerHasNotCountedToZero = 0x00000000;
						constexpr Reg Value_TimerHasCountedToZero = 0x1;
						constexpr Reg ValueMask_TimerHasCountedToZero = 0x00010000;
						};
					};
				namespace rRVR { // Register description
					typedef uint32_t	Reg;
					namespace fRELOAD { // Field Description
						constexpr Reg Lsb = 0;
						constexpr Reg FieldMask = 0x00FFFFFF;
						constexpr Reg Value_minValue = 0x0;
						constexpr Reg ValueMask_minValue = 0x00000000;
						constexpr Reg Value_maxValue = 0xFFFFFF;
						constexpr Reg ValueMask_maxValue = 0x00FFFFFF;
						};
					};
				namespace rCVR { // Register description
					typedef uint32_t	Reg;
					namespace fCURRENT { // Field Description
						constexpr Reg Lsb = 0;
						constexpr Reg FieldMask = 0x00FFFFFF;
						constexpr Reg Value_minValue = 0x0;
						constexpr Reg ValueMask_minValue = 0x00000000;
						constexpr Reg Value_maxValue = 0xFFFFFF;
						constexpr Reg ValueMask_maxValue = 0x00FFFFFF;
						};
					};
				namespace rCALIB { // Register description
					typedef uint32_t	Reg;
					constexpr Reg	valueAtReset = 0xC0000000;
					namespace fTENMS { // Field Description
						constexpr Reg Lsb = 0;
						constexpr Reg FieldMask = 0x00FFFFFF;
						constexpr Reg Value_valueAtReset = 0x0;
						constexpr Reg ValueMask_valueAtReset = 0x00000000;
						constexpr Reg Value_unknown = 0x0;
						constexpr Reg ValueMask_unknown = 0x00000000;
						};
					namespace fSKEW { // Field Description
						constexpr Reg Lsb = 30;
						constexpr Reg FieldMask = 0x40000000;
						constexpr Reg Value_valueAtReset = 0x1;
						constexpr Reg ValueMask_valueAtReset = 0x40000000;
						constexpr Reg Value_Exact = 0x0;
						constexpr Reg ValueMask_Exact = 0x00000000;
						constexpr Reg Value_InExact = 0x1;
						constexpr Reg ValueMask_InExact = 0x40000000;
						};
					namespace fNOREF { // Field Description
						constexpr Reg Lsb = 31;
						constexpr Reg FieldMask = 0x80000000;
						constexpr Reg Value_valueAtReset = 0x1;
						constexpr Reg ValueMask_valueAtReset = 0x80000000;
						constexpr Reg Value_ReferenceClockIsImplemented = 0x0;
						constexpr Reg ValueMask_ReferenceClockIsImplemented = 0x00000000;
						constexpr Reg Value_ReferenceClockIsNotImplemented = 0x1;
						constexpr Reg ValueMask_ReferenceClockIsNotImplemented = 0x80000000;
						};
					};
				}
			}
		}
	}
#endif
