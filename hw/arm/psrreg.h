#ifndef _hw_arm_psrregh_
#define _hw_arm_psrregh_
#include <stdint.h>
namespace Oscl { // Namespace description
	namespace ARM { // Namespace description
		namespace PSR { // Register description
			typedef uint32_t	Reg;
			namespace N { // Field Description
				enum {Lsb = 31};
				enum {FieldMask = 0x80000000};
				enum {Value_Positive = 0x0};
				enum {ValueMask_Positive = 0x00000000};
				enum {Value_Negative = 0x1};
				enum {ValueMask_Negative = 0x80000000};
				};
			namespace Z { // Field Description
				enum {Lsb = 30};
				enum {FieldMask = 0x40000000};
				enum {Value_NotZero = 0x0};
				enum {ValueMask_NotZero = 0x00000000};
				enum {Value_Zero = 0x1};
				enum {ValueMask_Zero = 0x40000000};
				};
			namespace C { // Field Description
				enum {Lsb = 29};
				enum {FieldMask = 0x20000000};
				enum {Value_NotCarry = 0x0};
				enum {ValueMask_NotCarry = 0x00000000};
				enum {Value_Carry = 0x1};
				enum {ValueMask_Carry = 0x20000000};
				};
			namespace V { // Field Description
				enum {Lsb = 28};
				enum {FieldMask = 0x10000000};
				enum {Value_NotOverflow = 0x0};
				enum {ValueMask_NotOverflow = 0x00000000};
				enum {Value_Overflow = 0x1};
				enum {ValueMask_Overflow = 0x10000000};
				};
			namespace Q { // Field Description
				enum {Lsb = 27};
				enum {FieldMask = 0x08000000};
				enum {Value_NotOverflow = 0x0};
				enum {ValueMask_NotOverflow = 0x00000000};
				enum {Value_Overflow = 0x1};
				enum {ValueMask_Overflow = 0x08000000};
				};
			namespace I { // Field Description
				enum {Lsb = 7};
				enum {FieldMask = 0x00000080};
				enum {Value_InterruptsEnabled = 0x0};
				enum {ValueMask_InterruptsEnabled = 0x00000000};
				enum {Value_InterruptsDisabled = 0x1};
				enum {ValueMask_InterruptsDisabled = 0x00000080};
				};
			namespace F { // Field Description
				enum {Lsb = 6};
				enum {FieldMask = 0x00000040};
				enum {Value_InterruptsEnabled = 0x0};
				enum {ValueMask_InterruptsEnabled = 0x00000000};
				enum {Value_InterruptsDisabled = 0x1};
				enum {ValueMask_InterruptsDisabled = 0x00000040};
				};
			namespace T { // Field Description
				enum {Lsb = 5};
				enum {FieldMask = 0x00000020};
				enum {Value_ArmExecution = 0x0};
				enum {ValueMask_ArmExecution = 0x00000000};
				enum {Value_ThumbExecution = 0x1};
				enum {ValueMask_ThumbExecution = 0x00000020};
				};
			namespace M { // Field Description
				enum {Lsb = 0};
				enum {FieldMask = 0x0000001F};
				enum {Value_UserMode = 0x10};
				enum {ValueMask_UserMode = 0x00000010};
				enum {Value_FiqMode = 0x11};
				enum {ValueMask_FiqMode = 0x00000011};
				enum {Value_IrqMode = 0x12};
				enum {ValueMask_IrqMode = 0x00000012};
				enum {Value_SupervisorMode = 0x13};
				enum {ValueMask_SupervisorMode = 0x00000013};
				enum {Value_AbortMode = 0x17};
				enum {ValueMask_AbortMode = 0x00000017};
				enum {Value_Undefined = 0x1B};
				enum {ValueMask_Undefined = 0x0000001B};
				enum {Value_System = 0x1F};
				enum {ValueMask_System = 0x0000001F};
				};
			};
		};
	};
#endif
