#ifndef _hw_arm_mmu_level1h_
#define _hw_arm_mmu_level1h_
#include <stdint.h>
namespace Oscl { // Namespace description
	namespace HW { // Namespace description
		namespace ARM { // Namespace description
			namespace MMU { // Namespace description
				namespace VirtualAddress { // Register description
					typedef uint32_t	Reg;
					namespace FirstLevelTableIndex { // Field Description
						enum {Lsb = 20};
						enum {FieldMask = 0xFFF00000};
						};
					namespace SectionIndex { // Field Description
						enum {Lsb = 0};
						enum {FieldMask = 0x000FFFFF};
						};
					namespace CoarseTableIndex { // Field Description
						enum {Lsb = 12};
						enum {FieldMask = 0x000FF000};
						};
					namespace FineTableIndex { // Field Description
						enum {Lsb = 10};
						enum {FieldMask = 0x000FFC00};
						};
					namespace LargePageIndex { // Field Description
						enum {Lsb = 0};
						enum {FieldMask = 0x0000FFFF};
						};
					namespace SmallPageIndex { // Field Description
						enum {Lsb = 0};
						enum {FieldMask = 0x00000FFF};
						};
					namespace TinyPageIndex { // Field Description
						enum {Lsb = 0};
						enum {FieldMask = 0x000003FF};
						};
					};
				namespace Level1Desc { // Namespace description
					namespace All { // Register description
						typedef uint32_t	Reg;
						namespace Format { // Field Description
							enum {Lsb = 0};
							enum {FieldMask = 0x00000003};
							enum {Value_Fault = 0x0};
							enum {ValueMask_Fault = 0x00000000};
							enum {Value_Coarse = 0x1};
							enum {ValueMask_Coarse = 0x00000001};
							enum {Value_Section = 0x2};
							enum {ValueMask_Section = 0x00000002};
							enum {Value_Fine = 0x3};
							enum {ValueMask_Fine = 0x00000003};
							};
						};
					namespace Coarse { // Register description
						typedef uint32_t	Reg;
						namespace PageTableBaseAddress { // Field Description
							enum {Lsb = 10};
							enum {FieldMask = 0xFFFFFC00};
							};
						namespace SBZ { // Field Description
							enum {Lsb = 9};
							enum {FieldMask = 0x00000200};
							};
						namespace Domain { // Field Description
							enum {Lsb = 5};
							enum {FieldMask = 0x000001E0};
							enum {Value_D0 = 0x0};
							enum {ValueMask_D0 = 0x00000000};
							enum {Value_D1 = 0x1};
							enum {ValueMask_D1 = 0x00000020};
							enum {Value_D2 = 0x2};
							enum {ValueMask_D2 = 0x00000040};
							enum {Value_D3 = 0x3};
							enum {ValueMask_D3 = 0x00000060};
							enum {Value_D4 = 0x4};
							enum {ValueMask_D4 = 0x00000080};
							enum {Value_D5 = 0x5};
							enum {ValueMask_D5 = 0x000000A0};
							enum {Value_D6 = 0x6};
							enum {ValueMask_D6 = 0x000000C0};
							enum {Value_D7 = 0x7};
							enum {ValueMask_D7 = 0x000000E0};
							enum {Value_D8 = 0x8};
							enum {ValueMask_D8 = 0x00000100};
							enum {Value_D9 = 0x9};
							enum {ValueMask_D9 = 0x00000120};
							enum {Value_D10 = 0xA};
							enum {ValueMask_D10 = 0x00000140};
							enum {Value_D11 = 0xB};
							enum {ValueMask_D11 = 0x00000160};
							enum {Value_D12 = 0xC};
							enum {ValueMask_D12 = 0x00000180};
							enum {Value_D13 = 0xD};
							enum {ValueMask_D13 = 0x000001A0};
							enum {Value_D14 = 0xE};
							enum {ValueMask_D14 = 0x000001C0};
							enum {Value_D15 = 0xF};
							enum {ValueMask_D15 = 0x000001E0};
							};
						namespace IMP { // Field Description
							enum {Lsb = 2};
							enum {FieldMask = 0x0000001C};
							};
						};
					namespace Section { // Register description
						typedef uint32_t	Reg;
						namespace BaseAddress { // Field Description
							enum {Lsb = 20};
							enum {FieldMask = 0xFFF00000};
							};
						namespace SBZ1 { // Field Description
							enum {Lsb = 12};
							enum {FieldMask = 0x000FF000};
							};
						namespace AP { // Field Description
							enum {Lsb = 10};
							enum {FieldMask = 0x00000C00};
							enum {Value_PrivNoAccessUserNoAccessS0R0 = 0x0};
							enum {ValueMask_PrivNoAccessUserNoAccessS0R0 = 0x00000000};
							enum {Value_PrivReadOnlyUserNoAccessS1R0 = 0x0};
							enum {ValueMask_PrivReadOnlyUserNoAccessS1R0 = 0x00000000};
							enum {Value_PrivReadOnlyUserReadOnlyS0R1 = 0x0};
							enum {ValueMask_PrivReadOnlyUserReadOnlyS0R1 = 0x00000000};
							enum {Value_PrivReadWriteUserNoAccess = 0x1};
							enum {ValueMask_PrivReadWriteUserNoAccess = 0x00000400};
							enum {Value_PrivReadWriteUserReadOnly = 0x2};
							enum {ValueMask_PrivReadWriteUserReadOnly = 0x00000800};
							enum {Value_PrivReadWriteUserReadWrite = 0x3};
							enum {ValueMask_PrivReadWriteUserReadWrite = 0x00000C00};
							};
						namespace SBZ { // Field Description
							enum {Lsb = 9};
							enum {FieldMask = 0x00000200};
							};
						namespace Domain { // Field Description
							enum {Lsb = 5};
							enum {FieldMask = 0x000001E0};
							enum {Value_D0 = 0x0};
							enum {ValueMask_D0 = 0x00000000};
							enum {Value_D1 = 0x1};
							enum {ValueMask_D1 = 0x00000020};
							enum {Value_D2 = 0x2};
							enum {ValueMask_D2 = 0x00000040};
							enum {Value_D3 = 0x3};
							enum {ValueMask_D3 = 0x00000060};
							enum {Value_D4 = 0x4};
							enum {ValueMask_D4 = 0x00000080};
							enum {Value_D5 = 0x5};
							enum {ValueMask_D5 = 0x000000A0};
							enum {Value_D6 = 0x6};
							enum {ValueMask_D6 = 0x000000C0};
							enum {Value_D7 = 0x7};
							enum {ValueMask_D7 = 0x000000E0};
							enum {Value_D8 = 0x8};
							enum {ValueMask_D8 = 0x00000100};
							enum {Value_D9 = 0x9};
							enum {ValueMask_D9 = 0x00000120};
							enum {Value_D10 = 0xA};
							enum {ValueMask_D10 = 0x00000140};
							enum {Value_D11 = 0xB};
							enum {ValueMask_D11 = 0x00000160};
							enum {Value_D12 = 0xC};
							enum {ValueMask_D12 = 0x00000180};
							enum {Value_D13 = 0xD};
							enum {ValueMask_D13 = 0x000001A0};
							enum {Value_D14 = 0xE};
							enum {ValueMask_D14 = 0x000001C0};
							enum {Value_D15 = 0xF};
							enum {ValueMask_D15 = 0x000001E0};
							};
						namespace IMP { // Field Description
							enum {Lsb = 4};
							enum {FieldMask = 0x00000010};
							};
						namespace C { // Field Description
							enum {Lsb = 3};
							enum {FieldMask = 0x00000008};
							enum {Value_Uncached = 0x0};
							enum {ValueMask_Uncached = 0x00000000};
							enum {Value_Cached = 0x1};
							enum {ValueMask_Cached = 0x00000008};
							};
						namespace B { // Field Description
							enum {Lsb = 2};
							enum {FieldMask = 0x00000004};
							enum {Value_Unbuffered = 0x0};
							enum {ValueMask_Unbuffered = 0x00000000};
							enum {Value_Buffered = 0x1};
							enum {ValueMask_Buffered = 0x00000004};
							};
						};
					namespace Fine { // Register description
						typedef uint32_t	Reg;
						namespace PageTableBaseAddress { // Field Description
							enum {Lsb = 12};
							enum {FieldMask = 0xFFFFF000};
							};
						namespace SBZ { // Field Description
							enum {Lsb = 9};
							enum {FieldMask = 0x00000E00};
							};
						namespace Domain { // Field Description
							enum {Lsb = 5};
							enum {FieldMask = 0x000001E0};
							enum {Value_D0 = 0x0};
							enum {ValueMask_D0 = 0x00000000};
							enum {Value_D1 = 0x1};
							enum {ValueMask_D1 = 0x00000020};
							enum {Value_D2 = 0x2};
							enum {ValueMask_D2 = 0x00000040};
							enum {Value_D3 = 0x3};
							enum {ValueMask_D3 = 0x00000060};
							enum {Value_D4 = 0x4};
							enum {ValueMask_D4 = 0x00000080};
							enum {Value_D5 = 0x5};
							enum {ValueMask_D5 = 0x000000A0};
							enum {Value_D6 = 0x6};
							enum {ValueMask_D6 = 0x000000C0};
							enum {Value_D7 = 0x7};
							enum {ValueMask_D7 = 0x000000E0};
							enum {Value_D8 = 0x8};
							enum {ValueMask_D8 = 0x00000100};
							enum {Value_D9 = 0x9};
							enum {ValueMask_D9 = 0x00000120};
							enum {Value_D10 = 0xA};
							enum {ValueMask_D10 = 0x00000140};
							enum {Value_D11 = 0xB};
							enum {ValueMask_D11 = 0x00000160};
							enum {Value_D12 = 0xC};
							enum {ValueMask_D12 = 0x00000180};
							enum {Value_D13 = 0xD};
							enum {ValueMask_D13 = 0x000001A0};
							enum {Value_D14 = 0xE};
							enum {ValueMask_D14 = 0x000001C0};
							enum {Value_D15 = 0xF};
							enum {ValueMask_D15 = 0x000001E0};
							};
						namespace IMP { // Field Description
							enum {Lsb = 2};
							enum {FieldMask = 0x0000001C};
							};
						};
					};
				};
			};
		};
	};
#endif
