#ifndef _hw_arm_mmu_level2h_
#define _hw_arm_mmu_level2h_
#include <stdint.h>
namespace Oscl { // Namespace description
	namespace HW { // Namespace description
		namespace ARM { // Namespace description
			namespace MMU { // Namespace description
				namespace Level2Desc { // Namespace description
					namespace All { // Register description
						typedef uint32_t	Reg;
						namespace Format { // Field Description
							enum {Lsb = 0};
							enum {FieldMask = 0x00000003};
							enum {Value_Fault = 0x0};
							enum {ValueMask_Fault = 0x00000000};
							enum {Value_LargePage = 0x1};
							enum {ValueMask_LargePage = 0x00000001};
							enum {Value_SmallPage = 0x2};
							enum {ValueMask_SmallPage = 0x00000002};
							enum {Value_TinyPage = 0x3};
							enum {ValueMask_TinyPage = 0x00000003};
							};
						};
					namespace LargePage { // Register description
						typedef uint32_t	Reg;
						namespace PageBaseAddress { // Field Description
							enum {Lsb = 16};
							enum {FieldMask = 0xFFFF0000};
							};
						namespace SBZ { // Field Description
							enum {Lsb = 12};
							enum {FieldMask = 0x0000F000};
							};
						namespace AP3 { // Field Description
							enum {Lsb = 10};
							enum {FieldMask = 0x00000C00};
							enum {Value_PrivNoAccessUserNoAccessS0R0 = 0x0};
							enum {ValueMask_PrivNoAccessUserNoAccessS0R0 = 0x00000000};
							enum {Value_PrivReadOnlyUserNoAccessS1R0 = 0x0};
							enum {ValueMask_PrivReadOnlyUserNoAccessS1R0 = 0x00000000};
							enum {Value_PrivReadOnlyUserReadOnlyS0R1 = 0x0};
							enum {ValueMask_PrivReadOnlyUserReadOnlyS0R1 = 0x00000000};
							enum {Value_PrivReadWriteUserNoAccess = 0x1};
							enum {ValueMask_PrivReadWriteUserNoAccess = 0x00000400};
							enum {Value_PrivReadWriteUserReadOnly = 0x2};
							enum {ValueMask_PrivReadWriteUserReadOnly = 0x00000800};
							enum {Value_PrivReadWriteUserReadWrite = 0x3};
							enum {ValueMask_PrivReadWriteUserReadWrite = 0x00000C00};
							};
						namespace AP2 { // Field Description
							enum {Lsb = 8};
							enum {FieldMask = 0x00000300};
							enum {Value_PrivNoAccessUserNoAccessS0R0 = 0x0};
							enum {ValueMask_PrivNoAccessUserNoAccessS0R0 = 0x00000000};
							enum {Value_PrivReadOnlyUserNoAccessS1R0 = 0x0};
							enum {ValueMask_PrivReadOnlyUserNoAccessS1R0 = 0x00000000};
							enum {Value_PrivReadOnlyUserReadOnlyS0R1 = 0x0};
							enum {ValueMask_PrivReadOnlyUserReadOnlyS0R1 = 0x00000000};
							enum {Value_PrivReadWriteUserNoAccess = 0x1};
							enum {ValueMask_PrivReadWriteUserNoAccess = 0x00000100};
							enum {Value_PrivReadWriteUserReadOnly = 0x2};
							enum {ValueMask_PrivReadWriteUserReadOnly = 0x00000200};
							enum {Value_PrivReadWriteUserReadWrite = 0x3};
							enum {ValueMask_PrivReadWriteUserReadWrite = 0x00000300};
							};
						namespace AP1 { // Field Description
							enum {Lsb = 8};
							enum {FieldMask = 0x00000300};
							enum {Value_PrivNoAccessUserNoAccessS0R0 = 0x0};
							enum {ValueMask_PrivNoAccessUserNoAccessS0R0 = 0x00000000};
							enum {Value_PrivReadOnlyUserNoAccessS1R0 = 0x0};
							enum {ValueMask_PrivReadOnlyUserNoAccessS1R0 = 0x00000000};
							enum {Value_PrivReadOnlyUserReadOnlyS0R1 = 0x0};
							enum {ValueMask_PrivReadOnlyUserReadOnlyS0R1 = 0x00000000};
							enum {Value_PrivReadWriteUserNoAccess = 0x1};
							enum {ValueMask_PrivReadWriteUserNoAccess = 0x00000100};
							enum {Value_PrivReadWriteUserReadOnly = 0x2};
							enum {ValueMask_PrivReadWriteUserReadOnly = 0x00000200};
							enum {Value_PrivReadWriteUserReadWrite = 0x3};
							enum {ValueMask_PrivReadWriteUserReadWrite = 0x00000300};
							};
						namespace AP0 { // Field Description
							enum {Lsb = 6};
							enum {FieldMask = 0x000000C0};
							enum {Value_PrivNoAccessUserNoAccessS0R0 = 0x0};
							enum {ValueMask_PrivNoAccessUserNoAccessS0R0 = 0x00000000};
							enum {Value_PrivReadOnlyUserNoAccessS1R0 = 0x0};
							enum {ValueMask_PrivReadOnlyUserNoAccessS1R0 = 0x00000000};
							enum {Value_PrivReadOnlyUserReadOnlyS0R1 = 0x0};
							enum {ValueMask_PrivReadOnlyUserReadOnlyS0R1 = 0x00000000};
							enum {Value_PrivReadWriteUserNoAccess = 0x1};
							enum {ValueMask_PrivReadWriteUserNoAccess = 0x00000040};
							enum {Value_PrivReadWriteUserReadOnly = 0x2};
							enum {ValueMask_PrivReadWriteUserReadOnly = 0x00000080};
							enum {Value_PrivReadWriteUserReadWrite = 0x3};
							enum {ValueMask_PrivReadWriteUserReadWrite = 0x000000C0};
							};
						namespace C { // Field Description
							enum {Lsb = 3};
							enum {FieldMask = 0x00000008};
							enum {Value_Uncached = 0x0};
							enum {ValueMask_Uncached = 0x00000000};
							enum {Value_Cached = 0x1};
							enum {ValueMask_Cached = 0x00000008};
							};
						namespace B { // Field Description
							enum {Lsb = 2};
							enum {FieldMask = 0x00000004};
							enum {Value_Unbuffered = 0x0};
							enum {ValueMask_Unbuffered = 0x00000000};
							enum {Value_Buffered = 0x1};
							enum {ValueMask_Buffered = 0x00000004};
							};
						};
					namespace SmallPage { // Register description
						typedef uint32_t	Reg;
						namespace PageBaseAddress { // Field Description
							enum {Lsb = 12};
							enum {FieldMask = 0xFFFFF000};
							};
						namespace AP3 { // Field Description
							enum {Lsb = 10};
							enum {FieldMask = 0x00000C00};
							enum {Value_PrivNoAccessUserNoAccessS0R0 = 0x0};
							enum {ValueMask_PrivNoAccessUserNoAccessS0R0 = 0x00000000};
							enum {Value_PrivReadOnlyUserNoAccessS1R0 = 0x0};
							enum {ValueMask_PrivReadOnlyUserNoAccessS1R0 = 0x00000000};
							enum {Value_PrivReadOnlyUserReadOnlyS0R1 = 0x0};
							enum {ValueMask_PrivReadOnlyUserReadOnlyS0R1 = 0x00000000};
							enum {Value_PrivReadWriteUserNoAccess = 0x1};
							enum {ValueMask_PrivReadWriteUserNoAccess = 0x00000400};
							enum {Value_PrivReadWriteUserReadOnly = 0x2};
							enum {ValueMask_PrivReadWriteUserReadOnly = 0x00000800};
							enum {Value_PrivReadWriteUserReadWrite = 0x3};
							enum {ValueMask_PrivReadWriteUserReadWrite = 0x00000C00};
							};
						namespace AP2 { // Field Description
							enum {Lsb = 8};
							enum {FieldMask = 0x00000300};
							enum {Value_PrivNoAccessUserNoAccessS0R0 = 0x0};
							enum {ValueMask_PrivNoAccessUserNoAccessS0R0 = 0x00000000};
							enum {Value_PrivReadOnlyUserNoAccessS1R0 = 0x0};
							enum {ValueMask_PrivReadOnlyUserNoAccessS1R0 = 0x00000000};
							enum {Value_PrivReadOnlyUserReadOnlyS0R1 = 0x0};
							enum {ValueMask_PrivReadOnlyUserReadOnlyS0R1 = 0x00000000};
							enum {Value_PrivReadWriteUserNoAccess = 0x1};
							enum {ValueMask_PrivReadWriteUserNoAccess = 0x00000100};
							enum {Value_PrivReadWriteUserReadOnly = 0x2};
							enum {ValueMask_PrivReadWriteUserReadOnly = 0x00000200};
							enum {Value_PrivReadWriteUserReadWrite = 0x3};
							enum {ValueMask_PrivReadWriteUserReadWrite = 0x00000300};
							};
						namespace AP1 { // Field Description
							enum {Lsb = 8};
							enum {FieldMask = 0x00000300};
							enum {Value_PrivNoAccessUserNoAccessS0R0 = 0x0};
							enum {ValueMask_PrivNoAccessUserNoAccessS0R0 = 0x00000000};
							enum {Value_PrivReadOnlyUserNoAccessS1R0 = 0x0};
							enum {ValueMask_PrivReadOnlyUserNoAccessS1R0 = 0x00000000};
							enum {Value_PrivReadOnlyUserReadOnlyS0R1 = 0x0};
							enum {ValueMask_PrivReadOnlyUserReadOnlyS0R1 = 0x00000000};
							enum {Value_PrivReadWriteUserNoAccess = 0x1};
							enum {ValueMask_PrivReadWriteUserNoAccess = 0x00000100};
							enum {Value_PrivReadWriteUserReadOnly = 0x2};
							enum {ValueMask_PrivReadWriteUserReadOnly = 0x00000200};
							enum {Value_PrivReadWriteUserReadWrite = 0x3};
							enum {ValueMask_PrivReadWriteUserReadWrite = 0x00000300};
							};
						namespace AP0 { // Field Description
							enum {Lsb = 6};
							enum {FieldMask = 0x000000C0};
							enum {Value_PrivNoAccessUserNoAccessS0R0 = 0x0};
							enum {ValueMask_PrivNoAccessUserNoAccessS0R0 = 0x00000000};
							enum {Value_PrivReadOnlyUserNoAccessS1R0 = 0x0};
							enum {ValueMask_PrivReadOnlyUserNoAccessS1R0 = 0x00000000};
							enum {Value_PrivReadOnlyUserReadOnlyS0R1 = 0x0};
							enum {ValueMask_PrivReadOnlyUserReadOnlyS0R1 = 0x00000000};
							enum {Value_PrivReadWriteUserNoAccess = 0x1};
							enum {ValueMask_PrivReadWriteUserNoAccess = 0x00000040};
							enum {Value_PrivReadWriteUserReadOnly = 0x2};
							enum {ValueMask_PrivReadWriteUserReadOnly = 0x00000080};
							enum {Value_PrivReadWriteUserReadWrite = 0x3};
							enum {ValueMask_PrivReadWriteUserReadWrite = 0x000000C0};
							};
						namespace C { // Field Description
							enum {Lsb = 3};
							enum {FieldMask = 0x00000008};
							enum {Value_Uncached = 0x0};
							enum {ValueMask_Uncached = 0x00000000};
							enum {Value_Cached = 0x1};
							enum {ValueMask_Cached = 0x00000008};
							};
						namespace B { // Field Description
							enum {Lsb = 2};
							enum {FieldMask = 0x00000004};
							enum {Value_Unbuffered = 0x0};
							enum {ValueMask_Unbuffered = 0x00000000};
							enum {Value_Buffered = 0x1};
							enum {ValueMask_Buffered = 0x00000004};
							};
						};
					namespace TinyPage { // Register description
						typedef uint32_t	Reg;
						namespace PageBaseAddress { // Field Description
							enum {Lsb = 10};
							enum {FieldMask = 0xFFFFFC00};
							};
						namespace SBZ { // Field Description
							enum {Lsb = 6};
							enum {FieldMask = 0x000003C0};
							};
						namespace AP { // Field Description
							enum {Lsb = 4};
							enum {FieldMask = 0x00000030};
							enum {Value_PrivNoAccessUserNoAccessS0R0 = 0x0};
							enum {ValueMask_PrivNoAccessUserNoAccessS0R0 = 0x00000000};
							enum {Value_PrivReadOnlyUserNoAccessS1R0 = 0x0};
							enum {ValueMask_PrivReadOnlyUserNoAccessS1R0 = 0x00000000};
							enum {Value_PrivReadOnlyUserReadOnlyS0R1 = 0x0};
							enum {ValueMask_PrivReadOnlyUserReadOnlyS0R1 = 0x00000000};
							enum {Value_PrivReadWriteUserNoAccess = 0x1};
							enum {ValueMask_PrivReadWriteUserNoAccess = 0x00000010};
							enum {Value_PrivReadWriteUserReadOnly = 0x2};
							enum {ValueMask_PrivReadWriteUserReadOnly = 0x00000020};
							enum {Value_PrivReadWriteUserReadWrite = 0x3};
							enum {ValueMask_PrivReadWriteUserReadWrite = 0x00000030};
							};
						namespace C { // Field Description
							enum {Lsb = 3};
							enum {FieldMask = 0x00000008};
							enum {Value_Uncached = 0x0};
							enum {ValueMask_Uncached = 0x00000000};
							enum {Value_Cached = 0x1};
							enum {ValueMask_Cached = 0x00000008};
							};
						namespace B { // Field Description
							enum {Lsb = 2};
							enum {FieldMask = 0x00000004};
							enum {Value_Unbuffered = 0x0};
							enum {ValueMask_Unbuffered = 0x00000000};
							enum {Value_Buffered = 0x1};
							enum {ValueMask_Buffered = 0x00000004};
							};
						};
					};
				};
			};
		};
	};
#endif
