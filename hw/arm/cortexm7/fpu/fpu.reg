/*
   Copyright (C) 2024 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

module oscl_hw_arm_cortexm7_fpu_regh {
	sysinclude "stdint.h";

namespace Oscl {
namespace Arm {
namespace CortexM7 {
namespace Fpu {
register rCPACR uint32_t {
	field fCP10 20 2 {
		value AccessDenied			0x00
		value PrivilegedAccessOnly	0x01
		value FullAccess			0x03
		}
	field fCP11 22 2 {
		value AccessDenied			0x00
		value PrivilegedAccessOnly	0x01
		value FullAccess			0x03
		}
	}
register rFPCCR uint32_t {
	field fLSPACT 0 1 {
		value NotActive	0
		value Active	1
		}
	field fUSER 1 1 {
		value NotUserPriveledge	0
		value UserPriveledge	1
		}
	field fTHREAD 3 1 {
		value NotThreadMode	0
		value ThreadMode	1
		}
	field fHFRDY 4 1 {
		value NotPermitted	0
		value Permitted		1
		}
	field fMMRDY 5 1 {
		value DisabledOrNotPermitted	0
		value EnabledAndPermitted		1
		}
	field fBFRDY 6 1 {
		value DisabledOrNotPermitted	0
		value EnabledAndPermitted		1
		}
	field fMONRDY 8 1 {
		value DisabledOrNotPermitted	0
		value EnabledAndPermitted		1
		}
	field fLSPEN 30 1 {
		value Disable	0
		value Enable	1
		value Disabled	0
		value Enabled	1
		}
	field fASPEN 31 1 {
		value Disable	0
		value Enable	1
		value Disabled	0
		value Enabled	1
		}
	}
register rFPACR uint32_t {
	field fADDRESS 3 29 {
		}
	}
register rFPSCR uint32_t {
	field fIOC 0 1 {
		value InvalidOperationException	1
		}
	field fDZC 1 1 {
		value DivisionByZeroException	1
		}
	field fOFC 2 1 {
		value OverflowException	1
		}
	field fUFC 3 1 {
		value UnderflowException	1
		}
	field fIXC 4 1 {
		value InexactException	1
		}
	field fIDC 7 1 {
		value Exception	1
		}
	field fRMode 22 2 {
		value RoundToNearest			0x00
		value RoundTowardsPlusInfinity	0x01
		value RoundTowardsMinusInfinity	0x02
		value RoundTowardsZero			0x03
		}
	field fFZ 24 1 {
		value FlushToZeroDisable	0
		value FlushToZeroEnable		1
		value FlushToZeroDisabled	0
		value FlushToZeroEnabled	1
		}
	field fDN 25 1 {
		value NaNOperandsPropagate	0
		value NaNReturnsDefault		1
		}
	field fAHP 26 1 {
		value IEEE			0
		value Alternative	1
		}
	field fV 28 1 {
		value NotOverflow	0
		value Overflow		1
		}
	field fC 29 1 {
		value NotCarry	0
		value Carry		1
		}
	field fZ 30 1 {
		value NotZero	0
		value Zero		1
		}
	field fN 31 1 {
		value IsPositive	0
		value IsNegative	1
		}
	}
register rFPDSCR uint32_t {
	field fRMode 22 2 {
		value Default	0
		value RoundToNearest			0x00
		value RoundTowardsPlusInfinity	0x01
		value RoundTowardsMinusInfinity	0x02
		value RoundTowardsZero			0x03
		}
	field fFZ 24 1 {
		value Default	0
		value FlushToZeroDisabled	0
		value FlushToZeroEnabled	1
		}
	field fDN 25 1 {
		value Default	0
		value NaNOperandsPropagate	0
		value NaNReturnsDefault		1
		}
	field fAHP 26 1 {
		value Default	0
		value IEEE			0
		value Alternative	1
		}
	}
}
}
}
}
}
