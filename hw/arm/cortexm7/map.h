/*
   Copyright (C) 2024 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_hw_arm_cortexm7_maph_
#define _oscl_hw_arm_cortexm7_maph_

#include "oscl/hw/arm/cortexm7/fpu/map.h"
#include "oscl/hw/arm/cortexm7/scb/map.h"
#include "oscl/hw/arm/cortexm7/cache/map.h"
#include "oscl/hw/arm/cortexm7/feature/map.h"
#include "oscl/hw/arm/cortexm7/mpu/map.h"
#include "oscl/hw/arm/cortexm7/dcr/map.h"
#include "oscl/hw/arm/cortexm7/tpiu/map.h"
#include "oscl/hw/arm/cortexm7/itm/map.h"
#include "oscl/hw/arm/cortexm7/dwt/map.h"


extern Oscl::Arm::CortexM7::Fpu::Map	CortexM7FPU;
extern Oscl::Arm::CortexM7::Scb::Map	CortexM7SCB;
extern Oscl::Arm::CortexM7::Cache::Map	CortexM7CACHE;
extern Oscl::Arm::CortexM7::Feature::Map	CortexM7FEATURE;
extern Oscl::Arm::CortexM7::Mpu::Map	CortexM7MPU;
extern Oscl::Arm::CortexM7::pDCR::Map	CortexM7DCR;
extern Oscl::Arm::CortexM7::pTPIU::Map	CortexM7TPIU;
extern Oscl::Arm::CortexM7::pITM::Map	CortexM7ITM;
extern Oscl::Arm::CortexM7::pDWT::Map	CortexM7DWT;

#endif
