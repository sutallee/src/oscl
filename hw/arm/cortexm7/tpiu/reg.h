/*
   Copyright (C) 2024 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _hw_arm_cortexm7_tpiu_regh_
#define _hw_arm_cortexm7_tpiu_regh_
#include <stdint.h>
namespace Oscl { // Namespace description
	namespace Arm { // Namespace description
		namespace CortexM7 { // Namespace description
			namespace pTPIU { // Namespace description
				namespace rSSPSR { // Register description
					typedef uint32_t	Reg;
					namespace fSWIDTH { // Field Description
						constexpr Reg Lsb = 0;
						constexpr Reg FieldMask = 0xFFFFFFFF;
						};
					};
				namespace rCSPSR { // Register description
					typedef uint32_t	Reg;
					namespace fCWIDTH { // Field Description
						constexpr Reg Lsb = 0;
						constexpr Reg FieldMask = 0xFFFFFFFF;
						};
					};
				namespace rACPR { // Register description
					typedef uint32_t	Reg;
					namespace fSWOSCALER { // Field Description
						constexpr Reg Lsb = 0;
						constexpr Reg FieldMask = 0x0000FFFF;
						};
					};
				namespace rSPPR { // Register description
					typedef uint32_t	Reg;
					namespace fTXMODE { // Field Description
						constexpr Reg Lsb = 0;
						constexpr Reg FieldMask = 0x00000003;
						constexpr Reg Value_parallelTracePort = 0x0;
						constexpr Reg ValueMask_parallelTracePort = 0x00000000;
						constexpr Reg Value_swoManchester = 0x1;
						constexpr Reg ValueMask_swoManchester = 0x00000001;
						constexpr Reg Value_swoNRZ = 0x2;
						constexpr Reg ValueMask_swoNRZ = 0x00000002;
						};
					};
				namespace rFFCR { // Register description
					typedef uint32_t	Reg;
					namespace fENFTC { // Field Description
						constexpr Reg Lsb = 0;
						constexpr Reg FieldMask = 0x00000001;
						constexpr Reg Value_disable = 0x0;
						constexpr Reg ValueMask_disable = 0x00000000;
						constexpr Reg Value_disabled = 0x0;
						constexpr Reg ValueMask_disabled = 0x00000000;
						constexpr Reg Value_enable = 0x1;
						constexpr Reg ValueMask_enable = 0x00000001;
						constexpr Reg Value_enabled = 0x1;
						constexpr Reg ValueMask_enabled = 0x00000001;
						};
					namespace fENFCONT { // Field Description
						constexpr Reg Lsb = 1;
						constexpr Reg FieldMask = 0x00000002;
						constexpr Reg Value_disable = 0x0;
						constexpr Reg ValueMask_disable = 0x00000000;
						constexpr Reg Value_disabled = 0x0;
						constexpr Reg ValueMask_disabled = 0x00000000;
						constexpr Reg Value_enable = 0x1;
						constexpr Reg ValueMask_enable = 0x00000002;
						constexpr Reg Value_enabled = 0x1;
						constexpr Reg ValueMask_enabled = 0x00000002;
						};
					namespace fFONFLIN { // Field Description
						constexpr Reg Lsb = 4;
						constexpr Reg FieldMask = 0x00000010;
						constexpr Reg Value_noEffect = 0x0;
						constexpr Reg ValueMask_noEffect = 0x00000000;
						constexpr Reg Value_flush = 0x1;
						constexpr Reg ValueMask_flush = 0x00000010;
						};
					namespace fFONTRIG { // Field Description
						constexpr Reg Lsb = 5;
						constexpr Reg FieldMask = 0x00000020;
						constexpr Reg Value_noEffect = 0x0;
						constexpr Reg ValueMask_noEffect = 0x00000000;
						constexpr Reg Value_flush = 0x1;
						constexpr Reg ValueMask_flush = 0x00000020;
						};
					namespace fFONMAN { // Field Description
						constexpr Reg Lsb = 6;
						constexpr Reg FieldMask = 0x00000040;
						constexpr Reg Value_noEffect = 0x0;
						constexpr Reg ValueMask_noEffect = 0x00000000;
						constexpr Reg Value_flush = 0x1;
						constexpr Reg ValueMask_flush = 0x00000040;
						};
					namespace fTRIGIN { // Field Description
						constexpr Reg Lsb = 8;
						constexpr Reg FieldMask = 0x00000100;
						constexpr Reg Value_noEffect = 0x0;
						constexpr Reg ValueMask_noEffect = 0x00000000;
						constexpr Reg Value_trigger = 0x1;
						constexpr Reg ValueMask_trigger = 0x00000100;
						constexpr Reg Value_triggered = 0x1;
						constexpr Reg ValueMask_triggered = 0x00000100;
						};
					namespace fTRIGEVT { // Field Description
						constexpr Reg Lsb = 9;
						constexpr Reg FieldMask = 0x00000200;
						constexpr Reg Value_noEffect = 0x0;
						constexpr Reg ValueMask_noEffect = 0x00000000;
						constexpr Reg Value_trigger = 0x1;
						constexpr Reg ValueMask_trigger = 0x00000200;
						constexpr Reg Value_triggered = 0x1;
						constexpr Reg ValueMask_triggered = 0x00000200;
						};
					namespace fTRIGFL { // Field Description
						constexpr Reg Lsb = 10;
						constexpr Reg FieldMask = 0x00000400;
						constexpr Reg Value_noEffect = 0x0;
						constexpr Reg ValueMask_noEffect = 0x00000000;
						constexpr Reg Value_trigger = 0x1;
						constexpr Reg ValueMask_trigger = 0x00000400;
						constexpr Reg Value_triggered = 0x1;
						constexpr Reg ValueMask_triggered = 0x00000400;
						};
					namespace fSTOPFL { // Field Description
						constexpr Reg Lsb = 12;
						constexpr Reg FieldMask = 0x00001000;
						constexpr Reg Value_noEffect = 0x0;
						constexpr Reg ValueMask_noEffect = 0x00000000;
						constexpr Reg Value_stopWhenFlushed = 0x1;
						constexpr Reg ValueMask_stopWhenFlushed = 0x00001000;
						};
					namespace fSTOPTRIG { // Field Description
						constexpr Reg Lsb = 13;
						constexpr Reg FieldMask = 0x00002000;
						constexpr Reg Value_noEffect = 0x0;
						constexpr Reg ValueMask_noEffect = 0x00000000;
						constexpr Reg Value_stopWhenTriggered = 0x1;
						constexpr Reg ValueMask_stopWhenTriggered = 0x00002000;
						};
					};
				namespace rTYPE { // Register description
					typedef uint32_t	Reg;
					namespace fFIFOSZ { // Field Description
						constexpr Reg Lsb = 6;
						constexpr Reg FieldMask = 0x000001C0;
						};
					namespace fPTINVALID { // Field Description
						constexpr Reg Lsb = 9;
						constexpr Reg FieldMask = 0x00000200;
						constexpr Reg Value_notSupported = 0x0;
						constexpr Reg ValueMask_notSupported = 0x00000000;
						constexpr Reg Value_supported = 0x1;
						constexpr Reg ValueMask_supported = 0x00000200;
						};
					namespace fMANCVALID { // Field Description
						constexpr Reg Lsb = 10;
						constexpr Reg FieldMask = 0x00000400;
						constexpr Reg Value_notSupported = 0x0;
						constexpr Reg ValueMask_notSupported = 0x00000000;
						constexpr Reg Value_supported = 0x1;
						constexpr Reg ValueMask_supported = 0x00000400;
						};
					namespace fNRZVALID { // Field Description
						constexpr Reg Lsb = 11;
						constexpr Reg FieldMask = 0x00000800;
						constexpr Reg Value_notSupported = 0x0;
						constexpr Reg ValueMask_notSupported = 0x00000000;
						constexpr Reg Value_supported = 0x1;
						constexpr Reg ValueMask_supported = 0x00000800;
						};
					};
				}
			}
		}
	}
#endif
