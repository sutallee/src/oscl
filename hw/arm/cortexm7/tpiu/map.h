/*
   Copyright (C) 2024 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/
#ifndef _oscl_hw_arm_cortexm7_tpiu_maph_
#define _oscl_hw_arm_cortexm7_tpiu_maph_
#include "reg.h"

/** */
namespace Oscl {
/** */
namespace Arm {
/** */
namespace CortexM7 {
/** */
namespace pTPIU {

/** 0xE0040000 */
struct Map {
	/** 0xE0040000, offset 0x00000000 */
	volatile rSSPSR::Reg	sspsr;

	/** 0xE0040004, offset 0x00000004 */
	volatile rCSPSR::Reg	cspsr;

	/** 0xE0040008, offset 0x00000008 */
	uint8_t	reservedE0040008[0xE0040010-0xE0040008];

	/** 0xE0040010, offset 0x00000010 */
	volatile rACPR::Reg	acpr;

	/** 0xE0040014, offset 0x00000014 */
	uint8_t	reservedE0040014[0xE00400F0-0xE0040014];

	/** 0xE00400F0, offset 0x000000F0 */
	volatile rSPPR::Reg	sppr;

	/** 0xE00400F4, offset 0x00000014 */
	uint8_t	reservedE00400F4[0xE0040304-0xE00400F4];

	/** 0xE0040304, offset 0x00000304 */
	volatile rFFCR::Reg	ffcr; /* This is NOT a part of Cortex-M7 generic */

	/** 0xE0040308, offset 0x00000308 */
	uint8_t	reservedE0040308[0xE0040FC8-0xE0040308];

	/** 0xE0040FC8, offset 0x00000FC8 */
	volatile rTYPE::Reg	type;

	/** 0xE0040FCC, offset 0x00000FCC */
	uint8_t	reservedE0040FCC[0xE0041000-0xE0040FCC];
	};

}
}
}
}

#endif
