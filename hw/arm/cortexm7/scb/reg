/*
   Copyright (C) 2024 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

module hw_arm_cortexm7_scbh {
sysinclude "stdint.h";
namespace Oscl {
namespace Arm {
namespace CortexM7 {
namespace pSCB {
	/* Auxiliary Control Register */
	register rACTLR uint32_t {
		value	reset	0x00000000
		field fDISFOLD 2 1 {
			value Normal	0
			}
		field fFPEXCODIS 10 1 {
			value Normal	0
			value FpuExceptionOutputDisabled	1
			value FpuExceptionOutputDisable	1
			}
		field fDISRAMODE 11 1 {
			value Normal	0
			value DynamicDisabled	1
			value DynamicDisable	1
			}
		/* This bit is always 1 and therefore RAO/WI. */
		field fDISITMATBFLUSH 12 1 {
			value Normal	1
			value Disabled	1
			value Disable	1
			}
		field fDISBTACREAD 13 1 {
			value Normal	0
			value BranchTargetCacheEnabled	0
			value BranchTargetCacheEnable	0
			value BranchTargetCacheDisabled		1
			value BranchTargetCacheDisable		1
			}
		field fDISBTACALLOC 14 1 {
			value Normal	0
			value BranchTargetCacheAllocationEnable		0
			value BranchTargetCacheAllocationEnabled	0
			value BranchTargetCacheAllocationDisable	1
			value BranchTargetCacheAllocationDisabled	1
			}
		field fDISCRITAXIRUR 15 1 {
			value Normal	0
			value Enable	0
			value Enabled	0
			value Disable	1
			value Disabled	1
			}
		field fDISDI 16 5 {
			value Normal	0
			value NoDualIssueInChannel0	1
			field fVFP 4 1 {
				}
			field fIntegerMacAndMul 3 1 {
				}
			field fLoadsToPC 2 1 {
				}
			field fIndirectBranchesNotToPC 1 1 {
				}
			field fDirectBranches 0 1 {
				}
			}
		field fDISISSCH1 21 5 {
			value Normal	0
			value NoInstructionTypeChannel1	1
			field fVFP 4 1 {
				}
			field fIntegerMacAndMul 3 1 {
				}
			field fLoadsToPC 2 1 {
				}
			field fIndirectBranchesNotToPC 1 1 {
				}
			field fDirectBranches 0 1 {
				}
			}
		field fDISDYNADD 26 1 {
			value	Normal	0
			value	SomeAddAndSubInstructionsAreResolvedInEx1	0
			value	AllAddAndSubInstructionsAreResolvedInEx2	1
			}
		field fDISCRITAXIRUW 27 1 {
			value	Normal	0
			value	BackwardsCompatibleWithR0	0
			value	AxiReadsToDevSOMemory		1
			}
		field fDISFPUISSOPT 28 1 {
			value	Normal	0
			}
		}
	/* CPUID Base Register */
	register rCPUID uint32_t {
		value	reset	0x411FC272
		field fREVISION 0 4 {
			value	Patch0	0
			value	Patch1	1
			value	Patch2	2
			}
		field fPARTNO 4 12 {
			value CortexM7	0xC27
			}
		field fCONSTANT 16 4 {
			value Architecture	0x0F
			}
		field fVARIANT 20 4 {
			value Revision0	0x00
			value Revision1	0x01
			}
		field fIMPLEMENTER 24 8 {
			value Arm	0x41
			}
		}
	/* Interrupt Control and State Register */
	register rICSR uint32_t {
		value	reset	0x00000000
		field fVECTACTIVE 0 6 {
			value ThreadMode		0
			value CmsisIrqOffset	16
			}
		field fRETTOBASE 11 1 {
			value PreemtedActiveExceptionsToExecute		0
			value NoPreemtedActiveExceptionsToExecute	1
			}
		field fVECTPENDING 12 9 {
			value NoPendingException	0
			}
		field fISRPENDING 22 1 {
			value NoInterruptPending	0
			value InterruptPending		1
			}
		field fPENDSTCLR 25 1 {
			value	NoEffect			0
			value	ClearSysTickPending	1
			}
		field fPENDSTSET 26 1 {
			value NoEffect						0
			value SysTickSetPending				1
			value SysTickExceptionNotPending	0
			value SysTickExceptionPending		1
			}
		field fPENDSVCLR 27 1 {
			value NoEffect						0
			value ClearPendSv					1
			}
		field fPENDSVSET 28 1 {
			value	NoEffect					0
			value	PendSvSetPending			1
			value	PendSvExceptionNotPending	0
			value	PendSvExceptionPending		1
			}
		field fNMIPENDSET 31 1 {
			value	NoEffect					0
			value	SetNmiPending				1
			value	NmiPendingNotPending		0
			value	NmiPendingPending			1
			}
		}
	/* Vector Table Offset Register */
	register rVTOR uint32_t {
		field fTBLOFF 7 25 {
			}
		}
	/* Application Interrupt and Reset Control Register */
	register rAIRCR uint32_t {
		value	reset	0xFA050000
		field fVECTRESET 0 1 {
			value NoEffect			0
			}
		field fVECTCLRACTIVE 1 1 {
			value NoEffect			0
			}
		field fSYSRESETREQ 1 1 {
			value NoEffect			0
			value SystemLevelReset	1
			}
		field fPRIGROUP 8 3 {
			value Default	0
			value group0	7
			}
		field fENDIANESS 15 1 {
			value DataLittleEndian	0
			value DataBigEndian	0
			}
		field fVECTKEY 16 16 {
			value key	0x05FA
			}
		}
	/* System Control Register */
	register rSCR uint32_t {
		value	reset	0x00000000
		field fSLEEPONEXIT 1 1 {
			value ReturnToThreadModeDoNotSleep		0
			value ReturnToThreadModeSleep			1
			value sleep		0
			value deepSleep	1
			}
		field fSLEEPDEEP 1 1 {
			value Sleep		0
			value DeepSleep	1
			}
		field fSEVONPEND 4 1 {
			value OnlyEventsAndEnabledInterruptsCauseWakeup	0
			value AllEventsAndInterruptsCauseWakeup			1
			}
		}
	/* Configuration and Control Register */
	register rCCR uint32_t {
		value	reset	0x00000200
		field fNONBASETHRDENA 0 1 {
			value Default	0
			value EnterThreadModeOtherFromThanBaseFaults	0
			value EnterThreadModeFromAny					1
			}
		field fUSERSETMPEND 1 1 {
			value Default	0
			value UnprivilegedSoftwareCannotAccessTheSTIR	0
			value UnprivilegedSoftwareCanAccessTheSTIR		1
			}
		field fUNALIGN_TRP 3 1 {
			value Default	0
			value TrappingDisabled						0
			value TrappingEnabled						1
			value AllUnalignedAccessGenerateAHardFault	1
			}
		field fDIV_0_TRP 4 1 {
			value Default	0
			value TrappingDisabled						0
			value TrappingEnabled						1
			}
		field fBFHFNMIGN 8 1 {
			value Default	0x02
			value PreciseDataAccessFaultCausesLockup	0
			value HandlerIgnoresTheFault				1
			}
		field fSTKALIGN 9 1 {
			value Default	0
			value StackAlignmentIs4Byte				0
			value StackAlignmentIs8ByteAdjusted		1
			value StackAlignment8ByteOnException	1
			}
		field fDC 16 1 {
			value Default	0
			value disable	0
			value disabled	0
			value enable	1
			value enabld	1
			}
		field fIC 17 1 {
			value Default	0
			value disable	0
			value disabled	0
			value enable	1
			value enabld	1
			}
		field fBP 18 1 {
			value Default	0
			value disable	0
			value disabled	0
			value enable	1
			value enabld	1
			}
		}
	/* System Handler Priority Register 1 */
	register rSHPR1 uint32_t {
		value	reset	0x00000000
		field fPRI_4 0 8 {
			/* Priority of MemManage */
			}
		field fPRI_5 8 8 {
			/* Priority of BusFault */
			}
		field fPRI_6 16 8 {
			/* Priority of UsageFault */
			}
		}
	/* System Handler Priority Register 2 */
	register rSHPR2 uint32_t {
		value	reset	0x00000000
		field fPRI_11 24 8 {
			/* Priority of SVCall */
			}
		}
	/* System Handler Priority Register 3 */
	register rSHPR3 uint32_t {
		value	reset	0x00000000
		field fPRI_14 16 8 {
			/* Priority of PendSV */
			}
		field fPRI_15 25 8 {
			/* Priority of SysTick */
			}
		}
	/* System Handler Control and State Register */
	register rSHCSR uint32_t {
		value	reset	0x00000000
		field fMEMFAULTACT 0 1 {
			value NotActive	0
			value Active	1
			}
		field fBUSFAULTACT 1 1 {
			value NotActive	0
			value Active	1
			}
		field fUSGFAULTACT 3 1 {
			value NotActive	0
			value Active	1
			}
		field fSVCALLACT 7 1 {
			value NotActive	0
			value Active	1
			}
		field fMONITORACT 8 1 {
			value NotActive	0
			value Active	1
			}
		field fPENDSVACT 10 1 {
			value NotActive	0
			value Active	1
			}
		field fSYSTICKACT 11 1 {
			value NotActive	0
			value Active	1
			}
		field fUSGFAULTPENDED 12 1 {
			value NotPending	0
			value Pending		1
			}
		field fMEMFAULTPENDED 13 1 {
			value NotPending	0
			value Pending		1
			}
		field fBUSFAULTPENDED 14 1 {
			value NotPending	0
			value Pending		1
			}
		field fSVCALLPENDED 15 1 {
			value NotPending	0
			value Pending		1
			}
		field fMEMFAULTENA 16 1 {
			value Disable	0
			value Enable	1
			}
		field fBUSFAULTENA 17 1 {
			value Disable	0
			value Enable	1
			}
		field fUSGFAULTENA 18 1 {
			value Disable	0
			value Enable	1
			}
		}
	/* Configurable Fault Status Registers */
	register rCFSR uint32_t {
		value	reset	0x00000000
		field fMMFSR 0 8 {
			field fIACCVIOL 0 1 {
				value NoMpuOrExecuteNeverViolation	0
				value MpuOrExecuteNeverViolation	1
				}
			field fDACCVIOL 1 1 {
				value NoDataAccessViolation			0
				value DataAccessViolation			1
				}
			field fMUNSTKERR 3 1 {
				value NoExceptionReturnDerivedMemManageFault	0
				value ExceptionReturnDerivedMemManageFault		1
				}
			field fMSTKERR 4 1 {
				value NoExceptionEntryDerivedMemManageFault	0
				value ExceptionEntryDerivedMemManageFault	1
				}
			field fMLSPERR 5 1 {
				value NoFpLazyStatePreservationMemManageFault	0
				value pLazyStatePreservationMemManageFault		1
				}
			field fMMARVALID 7 1 {
				value MmarNotValid	0
				value MmarValid		1
				}
			}
		field fBFSR 8 8 {
			field fIBUSERR 8 1 {
				value NoBusFault	0
				value BusFault		1
				}
			field fPRECISERR 9 1 {
				value NoDataAccessError	0
				value DataAccessError	1
				}
			field fIMPRECISERR 10 1 {
				value NoDataAccessError	0
				value DataAccessError	1
				}
			field fUNSTKERR 11 1 {
				value NoExceptionReturnDerivedBusFault	0
				value ExceptionReturnDerivedBusFault	1
				}
			field fSTKERR 12 1 {
				value NoExceptionEntryDerivedBusFault	0
				value ExceptionEntryDerivedBusFault		1
				}
			field fLSPERR 13 1 {
				value NoFpLazyBusFault	0
				value FpLazyBusFault	1
				}
			field fBFARVALID 15 1 {
				value BfarNotValid	0
				value BfarValid		1
				}
			}
		field fUFSR 16 16 {
			field fUNDEFINSTR 16 1 {
				value NoUndefinedInstructionUsageFault	0
				value UndefinedInstructionUsageFault	1
				}
			field fINVSTATE 17 1 {
				value ValidEpsrIT	0
				value IvalidEpsrtIT	1
				}
			field fINVPC 18 1 {
				value NoError	0
				value Error		1
				}
			field fNOCP 19 1 {
				value NoError	0
				value Error		1
				}
			field fUNALIGNED 24 1 {
				value NoUnaligedAccess	0
				value UnaligedAccess	1
				}
			field fDIVBYZERO 25 1 {
				value NoDivideByZerorError	0
				value DivideByZerorError	1
				}
			}
		}
	/* HardFault Status Register */
	register rHFSR uint32_t {
		value	reset	0x00000000
		field fVECTTBL 1 1 {
			value NoReadFault	0
			value ReadFault		1
			}
		field fFORCED 30 1 {
			value NoPriorityEscalation	0
			value PriorityEscalation	1
			}
		field fDEBUGEVT 30 1 {
			value NoHaltingDebugDisabledEvent	0
			value HaltingDebugDisabledEvent		1
			}
		}
	/* MemManage Fault Address Register */
	register rMMFAR uint32_t {
		field fADDRESS 0 32 {
			}
		}
	/* BusFault Address Register */
	register rBFAR uint32_t {
		field fADDRESS 0 32 {
			}
		}
	/* Auxiliary Fault Status Register */
	register rAFSR uint32_t {
		field fIMPDEF 0 32 {
			}
		}
}
}
}
}
}

