/*
   Copyright (C) 2024 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _hw_arm_cortexm7_scbh_
#define _hw_arm_cortexm7_scbh_
#include <stdint.h>
namespace Oscl { // Namespace description
	namespace Arm { // Namespace description
		namespace CortexM7 { // Namespace description
			namespace pSCB { // Namespace description
				namespace rACTLR { // Register description
					typedef uint32_t	Reg;
					constexpr Reg	reset = 0;
					namespace fDISFOLD { // Field Description
						constexpr Reg Lsb = 2;
						constexpr Reg FieldMask = 0x00000004;
						constexpr Reg Value_Normal = 0x0;
						constexpr Reg ValueMask_Normal = 0x00000000;
						};
					namespace fFPEXCODIS { // Field Description
						constexpr Reg Lsb = 10;
						constexpr Reg FieldMask = 0x00000400;
						constexpr Reg Value_Normal = 0x0;
						constexpr Reg ValueMask_Normal = 0x00000000;
						constexpr Reg Value_FpuExceptionOutputDisabled = 0x1;
						constexpr Reg ValueMask_FpuExceptionOutputDisabled = 0x00000400;
						constexpr Reg Value_FpuExceptionOutputDisable = 0x1;
						constexpr Reg ValueMask_FpuExceptionOutputDisable = 0x00000400;
						};
					namespace fDISRAMODE { // Field Description
						constexpr Reg Lsb = 11;
						constexpr Reg FieldMask = 0x00000800;
						constexpr Reg Value_Normal = 0x0;
						constexpr Reg ValueMask_Normal = 0x00000000;
						constexpr Reg Value_DynamicDisabled = 0x1;
						constexpr Reg ValueMask_DynamicDisabled = 0x00000800;
						constexpr Reg Value_DynamicDisable = 0x1;
						constexpr Reg ValueMask_DynamicDisable = 0x00000800;
						};
					namespace fDISITMATBFLUSH { // Field Description
						constexpr Reg Lsb = 12;
						constexpr Reg FieldMask = 0x00001000;
						constexpr Reg Value_Normal = 0x1;
						constexpr Reg ValueMask_Normal = 0x00001000;
						constexpr Reg Value_Disabled = 0x1;
						constexpr Reg ValueMask_Disabled = 0x00001000;
						constexpr Reg Value_Disable = 0x1;
						constexpr Reg ValueMask_Disable = 0x00001000;
						};
					namespace fDISBTACREAD { // Field Description
						constexpr Reg Lsb = 13;
						constexpr Reg FieldMask = 0x00002000;
						constexpr Reg Value_Normal = 0x0;
						constexpr Reg ValueMask_Normal = 0x00000000;
						constexpr Reg Value_BranchTargetCacheEnabled = 0x0;
						constexpr Reg ValueMask_BranchTargetCacheEnabled = 0x00000000;
						constexpr Reg Value_BranchTargetCacheEnable = 0x0;
						constexpr Reg ValueMask_BranchTargetCacheEnable = 0x00000000;
						constexpr Reg Value_BranchTargetCacheDisabled = 0x1;
						constexpr Reg ValueMask_BranchTargetCacheDisabled = 0x00002000;
						constexpr Reg Value_BranchTargetCacheDisable = 0x1;
						constexpr Reg ValueMask_BranchTargetCacheDisable = 0x00002000;
						};
					namespace fDISBTACALLOC { // Field Description
						constexpr Reg Lsb = 14;
						constexpr Reg FieldMask = 0x00004000;
						constexpr Reg Value_Normal = 0x0;
						constexpr Reg ValueMask_Normal = 0x00000000;
						constexpr Reg Value_BranchTargetCacheAllocationEnable = 0x0;
						constexpr Reg ValueMask_BranchTargetCacheAllocationEnable = 0x00000000;
						constexpr Reg Value_BranchTargetCacheAllocationEnabled = 0x0;
						constexpr Reg ValueMask_BranchTargetCacheAllocationEnabled = 0x00000000;
						constexpr Reg Value_BranchTargetCacheAllocationDisable = 0x1;
						constexpr Reg ValueMask_BranchTargetCacheAllocationDisable = 0x00004000;
						constexpr Reg Value_BranchTargetCacheAllocationDisabled = 0x1;
						constexpr Reg ValueMask_BranchTargetCacheAllocationDisabled = 0x00004000;
						};
					namespace fDISCRITAXIRUR { // Field Description
						constexpr Reg Lsb = 15;
						constexpr Reg FieldMask = 0x00008000;
						constexpr Reg Value_Normal = 0x0;
						constexpr Reg ValueMask_Normal = 0x00000000;
						constexpr Reg Value_Enable = 0x0;
						constexpr Reg ValueMask_Enable = 0x00000000;
						constexpr Reg Value_Enabled = 0x0;
						constexpr Reg ValueMask_Enabled = 0x00000000;
						constexpr Reg Value_Disable = 0x1;
						constexpr Reg ValueMask_Disable = 0x00008000;
						constexpr Reg Value_Disabled = 0x1;
						constexpr Reg ValueMask_Disabled = 0x00008000;
						};
					namespace fDISDI { // Field Description
						constexpr Reg Lsb = 16;
						constexpr Reg FieldMask = 0x001F0000;
						constexpr Reg Value_Normal = 0x0;
						constexpr Reg ValueMask_Normal = 0x00000000;
						constexpr Reg Value_NoDualIssueInChannel0 = 0x1;
						constexpr Reg ValueMask_NoDualIssueInChannel0 = 0x00010000;
						namespace fVFP { // Field Description
							constexpr Reg Lsb = 20;
							constexpr Reg FieldMask = 0x00100000;
							};
						namespace fIntegerMacAndMul { // Field Description
							constexpr Reg Lsb = 19;
							constexpr Reg FieldMask = 0x00080000;
							};
						namespace fLoadsToPC { // Field Description
							constexpr Reg Lsb = 18;
							constexpr Reg FieldMask = 0x00040000;
							};
						namespace fIndirectBranchesNotToPC { // Field Description
							constexpr Reg Lsb = 17;
							constexpr Reg FieldMask = 0x00020000;
							};
						namespace fDirectBranches { // Field Description
							constexpr Reg Lsb = 16;
							constexpr Reg FieldMask = 0x00010000;
							};
						};
					namespace fDISISSCH1 { // Field Description
						constexpr Reg Lsb = 21;
						constexpr Reg FieldMask = 0x03E00000;
						constexpr Reg Value_Normal = 0x0;
						constexpr Reg ValueMask_Normal = 0x00000000;
						constexpr Reg Value_NoInstructionTypeChannel1 = 0x1;
						constexpr Reg ValueMask_NoInstructionTypeChannel1 = 0x00200000;
						namespace fVFP { // Field Description
							constexpr Reg Lsb = 25;
							constexpr Reg FieldMask = 0x02000000;
							};
						namespace fIntegerMacAndMul { // Field Description
							constexpr Reg Lsb = 24;
							constexpr Reg FieldMask = 0x01000000;
							};
						namespace fLoadsToPC { // Field Description
							constexpr Reg Lsb = 23;
							constexpr Reg FieldMask = 0x00800000;
							};
						namespace fIndirectBranchesNotToPC { // Field Description
							constexpr Reg Lsb = 22;
							constexpr Reg FieldMask = 0x00400000;
							};
						namespace fDirectBranches { // Field Description
							constexpr Reg Lsb = 21;
							constexpr Reg FieldMask = 0x00200000;
							};
						};
					namespace fDISDYNADD { // Field Description
						constexpr Reg Lsb = 26;
						constexpr Reg FieldMask = 0x04000000;
						constexpr Reg Value_Normal = 0x0;
						constexpr Reg ValueMask_Normal = 0x00000000;
						constexpr Reg Value_SomeAddAndSubInstructionsAreResolvedInEx1 = 0x0;
						constexpr Reg ValueMask_SomeAddAndSubInstructionsAreResolvedInEx1 = 0x00000000;
						constexpr Reg Value_AllAddAndSubInstructionsAreResolvedInEx2 = 0x1;
						constexpr Reg ValueMask_AllAddAndSubInstructionsAreResolvedInEx2 = 0x04000000;
						};
					namespace fDISCRITAXIRUW { // Field Description
						constexpr Reg Lsb = 27;
						constexpr Reg FieldMask = 0x08000000;
						constexpr Reg Value_Normal = 0x0;
						constexpr Reg ValueMask_Normal = 0x00000000;
						constexpr Reg Value_BackwardsCompatibleWithR0 = 0x0;
						constexpr Reg ValueMask_BackwardsCompatibleWithR0 = 0x00000000;
						constexpr Reg Value_AxiReadsToDevSOMemory = 0x1;
						constexpr Reg ValueMask_AxiReadsToDevSOMemory = 0x08000000;
						};
					namespace fDISFPUISSOPT { // Field Description
						constexpr Reg Lsb = 28;
						constexpr Reg FieldMask = 0x10000000;
						constexpr Reg Value_Normal = 0x0;
						constexpr Reg ValueMask_Normal = 0x00000000;
						};
					};
				namespace rCPUID { // Register description
					typedef uint32_t	Reg;
					constexpr Reg	reset = 1092600434;
					namespace fREVISION { // Field Description
						constexpr Reg Lsb = 0;
						constexpr Reg FieldMask = 0x0000000F;
						constexpr Reg Value_Patch0 = 0x0;
						constexpr Reg ValueMask_Patch0 = 0x00000000;
						constexpr Reg Value_Patch1 = 0x1;
						constexpr Reg ValueMask_Patch1 = 0x00000001;
						constexpr Reg Value_Patch2 = 0x2;
						constexpr Reg ValueMask_Patch2 = 0x00000002;
						};
					namespace fPARTNO { // Field Description
						constexpr Reg Lsb = 4;
						constexpr Reg FieldMask = 0x0000FFF0;
						constexpr Reg Value_CortexM7 = 0xC27;
						constexpr Reg ValueMask_CortexM7 = 0x0000C270;
						};
					namespace fCONSTANT { // Field Description
						constexpr Reg Lsb = 16;
						constexpr Reg FieldMask = 0x000F0000;
						constexpr Reg Value_Architecture = 0xF;
						constexpr Reg ValueMask_Architecture = 0x000F0000;
						};
					namespace fVARIANT { // Field Description
						constexpr Reg Lsb = 20;
						constexpr Reg FieldMask = 0x00F00000;
						constexpr Reg Value_Revision0 = 0x0;
						constexpr Reg ValueMask_Revision0 = 0x00000000;
						constexpr Reg Value_Revision1 = 0x1;
						constexpr Reg ValueMask_Revision1 = 0x00100000;
						};
					namespace fIMPLEMENTER { // Field Description
						constexpr Reg Lsb = 24;
						constexpr Reg FieldMask = 0xFF000000;
						constexpr Reg Value_Arm = 0x41;
						constexpr Reg ValueMask_Arm = 0x41000000;
						};
					};
				namespace rICSR { // Register description
					typedef uint32_t	Reg;
					constexpr Reg	reset = 0;
					namespace fVECTACTIVE { // Field Description
						constexpr Reg Lsb = 0;
						constexpr Reg FieldMask = 0x0000003F;
						constexpr Reg Value_ThreadMode = 0x0;
						constexpr Reg ValueMask_ThreadMode = 0x00000000;
						constexpr Reg Value_CmsisIrqOffset = 0x10;
						constexpr Reg ValueMask_CmsisIrqOffset = 0x00000010;
						};
					namespace fRETTOBASE { // Field Description
						constexpr Reg Lsb = 11;
						constexpr Reg FieldMask = 0x00000800;
						constexpr Reg Value_PreemtedActiveExceptionsToExecute = 0x0;
						constexpr Reg ValueMask_PreemtedActiveExceptionsToExecute = 0x00000000;
						constexpr Reg Value_NoPreemtedActiveExceptionsToExecute = 0x1;
						constexpr Reg ValueMask_NoPreemtedActiveExceptionsToExecute = 0x00000800;
						};
					namespace fVECTPENDING { // Field Description
						constexpr Reg Lsb = 12;
						constexpr Reg FieldMask = 0x001FF000;
						constexpr Reg Value_NoPendingException = 0x0;
						constexpr Reg ValueMask_NoPendingException = 0x00000000;
						};
					namespace fISRPENDING { // Field Description
						constexpr Reg Lsb = 22;
						constexpr Reg FieldMask = 0x00400000;
						constexpr Reg Value_NoInterruptPending = 0x0;
						constexpr Reg ValueMask_NoInterruptPending = 0x00000000;
						constexpr Reg Value_InterruptPending = 0x1;
						constexpr Reg ValueMask_InterruptPending = 0x00400000;
						};
					namespace fPENDSTCLR { // Field Description
						constexpr Reg Lsb = 25;
						constexpr Reg FieldMask = 0x02000000;
						constexpr Reg Value_NoEffect = 0x0;
						constexpr Reg ValueMask_NoEffect = 0x00000000;
						constexpr Reg Value_ClearSysTickPending = 0x1;
						constexpr Reg ValueMask_ClearSysTickPending = 0x02000000;
						};
					namespace fPENDSTSET { // Field Description
						constexpr Reg Lsb = 26;
						constexpr Reg FieldMask = 0x04000000;
						constexpr Reg Value_NoEffect = 0x0;
						constexpr Reg ValueMask_NoEffect = 0x00000000;
						constexpr Reg Value_SysTickSetPending = 0x1;
						constexpr Reg ValueMask_SysTickSetPending = 0x04000000;
						constexpr Reg Value_SysTickExceptionNotPending = 0x0;
						constexpr Reg ValueMask_SysTickExceptionNotPending = 0x00000000;
						constexpr Reg Value_SysTickExceptionPending = 0x1;
						constexpr Reg ValueMask_SysTickExceptionPending = 0x04000000;
						};
					namespace fPENDSVCLR { // Field Description
						constexpr Reg Lsb = 27;
						constexpr Reg FieldMask = 0x08000000;
						constexpr Reg Value_NoEffect = 0x0;
						constexpr Reg ValueMask_NoEffect = 0x00000000;
						constexpr Reg Value_ClearPendSv = 0x1;
						constexpr Reg ValueMask_ClearPendSv = 0x08000000;
						};
					namespace fPENDSVSET { // Field Description
						constexpr Reg Lsb = 28;
						constexpr Reg FieldMask = 0x10000000;
						constexpr Reg Value_NoEffect = 0x0;
						constexpr Reg ValueMask_NoEffect = 0x00000000;
						constexpr Reg Value_PendSvSetPending = 0x1;
						constexpr Reg ValueMask_PendSvSetPending = 0x10000000;
						constexpr Reg Value_PendSvExceptionNotPending = 0x0;
						constexpr Reg ValueMask_PendSvExceptionNotPending = 0x00000000;
						constexpr Reg Value_PendSvExceptionPending = 0x1;
						constexpr Reg ValueMask_PendSvExceptionPending = 0x10000000;
						};
					namespace fNMIPENDSET { // Field Description
						constexpr Reg Lsb = 31;
						constexpr Reg FieldMask = 0x80000000;
						constexpr Reg Value_NoEffect = 0x0;
						constexpr Reg ValueMask_NoEffect = 0x00000000;
						constexpr Reg Value_SetNmiPending = 0x1;
						constexpr Reg ValueMask_SetNmiPending = 0x80000000;
						constexpr Reg Value_NmiPendingNotPending = 0x0;
						constexpr Reg ValueMask_NmiPendingNotPending = 0x00000000;
						constexpr Reg Value_NmiPendingPending = 0x1;
						constexpr Reg ValueMask_NmiPendingPending = 0x80000000;
						};
					};
				namespace rVTOR { // Register description
					typedef uint32_t	Reg;
					namespace fTBLOFF { // Field Description
						constexpr Reg Lsb = 7;
						constexpr Reg FieldMask = 0xFFFFFF80;
						};
					};
				namespace rAIRCR { // Register description
					typedef uint32_t	Reg;
					constexpr Reg	reset = -100335616;
					namespace fVECTRESET { // Field Description
						constexpr Reg Lsb = 0;
						constexpr Reg FieldMask = 0x00000001;
						constexpr Reg Value_NoEffect = 0x0;
						constexpr Reg ValueMask_NoEffect = 0x00000000;
						};
					namespace fVECTCLRACTIVE { // Field Description
						constexpr Reg Lsb = 1;
						constexpr Reg FieldMask = 0x00000002;
						constexpr Reg Value_NoEffect = 0x0;
						constexpr Reg ValueMask_NoEffect = 0x00000000;
						};
					namespace fSYSRESETREQ { // Field Description
						constexpr Reg Lsb = 1;
						constexpr Reg FieldMask = 0x00000002;
						constexpr Reg Value_NoEffect = 0x0;
						constexpr Reg ValueMask_NoEffect = 0x00000000;
						constexpr Reg Value_SystemLevelReset = 0x1;
						constexpr Reg ValueMask_SystemLevelReset = 0x00000002;
						};
					namespace fPRIGROUP { // Field Description
						constexpr Reg Lsb = 8;
						constexpr Reg FieldMask = 0x00000700;
						constexpr Reg Value_Default = 0x0;
						constexpr Reg ValueMask_Default = 0x00000000;
						};
					namespace fENDIANESS { // Field Description
						constexpr Reg Lsb = 15;
						constexpr Reg FieldMask = 0x00008000;
						constexpr Reg Value_DataLittleEndian = 0x0;
						constexpr Reg ValueMask_DataLittleEndian = 0x00000000;
						constexpr Reg Value_DataBigEndian = 0x0;
						constexpr Reg ValueMask_DataBigEndian = 0x00000000;
						};
					namespace fVECTKEY { // Field Description
						constexpr Reg Lsb = 16;
						constexpr Reg FieldMask = 0xFFFF0000;
						constexpr Reg Value_key = 0x5FA;
						constexpr Reg ValueMask_key = 0x05FA0000;
						};
					};
				namespace rSCR { // Register description
					typedef uint32_t	Reg;
					constexpr Reg	reset = 0;
					namespace fSLEEPONEXIT { // Field Description
						constexpr Reg Lsb = 1;
						constexpr Reg FieldMask = 0x00000002;
						constexpr Reg Value_ReturnToThreadModeDoNotSleep = 0x0;
						constexpr Reg ValueMask_ReturnToThreadModeDoNotSleep = 0x00000000;
						constexpr Reg Value_ReturnToThreadModeSleep = 0x1;
						constexpr Reg ValueMask_ReturnToThreadModeSleep = 0x00000002;
						constexpr Reg Value_sleep = 0x0;
						constexpr Reg ValueMask_sleep = 0x00000000;
						constexpr Reg Value_deepSleep = 0x1;
						constexpr Reg ValueMask_deepSleep = 0x00000002;
						};
					namespace fSLEEPDEEP { // Field Description
						constexpr Reg Lsb = 1;
						constexpr Reg FieldMask = 0x00000002;
						constexpr Reg Value_Sleep = 0x0;
						constexpr Reg ValueMask_Sleep = 0x00000000;
						constexpr Reg Value_DeepSleep = 0x1;
						constexpr Reg ValueMask_DeepSleep = 0x00000002;
						};
					namespace fSEVONPEND { // Field Description
						constexpr Reg Lsb = 4;
						constexpr Reg FieldMask = 0x00000010;
						constexpr Reg Value_OnlyEventsAndEnabledInterruptsCauseWakeup = 0x0;
						constexpr Reg ValueMask_OnlyEventsAndEnabledInterruptsCauseWakeup = 0x00000000;
						constexpr Reg Value_AllEventsAndInterruptsCauseWakeup = 0x1;
						constexpr Reg ValueMask_AllEventsAndInterruptsCauseWakeup = 0x00000010;
						};
					};
				namespace rCCR { // Register description
					typedef uint32_t	Reg;
					constexpr Reg	reset = 512;
					namespace fNONBASETHRDENA { // Field Description
						constexpr Reg Lsb = 0;
						constexpr Reg FieldMask = 0x00000001;
						constexpr Reg Value_Default = 0x0;
						constexpr Reg ValueMask_Default = 0x00000000;
						constexpr Reg Value_EnterThreadModeOtherFromThanBaseFaults = 0x0;
						constexpr Reg ValueMask_EnterThreadModeOtherFromThanBaseFaults = 0x00000000;
						constexpr Reg Value_EnterThreadModeFromAny = 0x1;
						constexpr Reg ValueMask_EnterThreadModeFromAny = 0x00000001;
						};
					namespace fUSERSETMPEND { // Field Description
						constexpr Reg Lsb = 1;
						constexpr Reg FieldMask = 0x00000002;
						constexpr Reg Value_Default = 0x0;
						constexpr Reg ValueMask_Default = 0x00000000;
						constexpr Reg Value_UnprivilegedSoftwareCannotAccessTheSTIR = 0x0;
						constexpr Reg ValueMask_UnprivilegedSoftwareCannotAccessTheSTIR = 0x00000000;
						constexpr Reg Value_UnprivilegedSoftwareCanAccessTheSTIR = 0x1;
						constexpr Reg ValueMask_UnprivilegedSoftwareCanAccessTheSTIR = 0x00000002;
						};
					namespace fUNALIGN_TRP { // Field Description
						constexpr Reg Lsb = 3;
						constexpr Reg FieldMask = 0x00000008;
						constexpr Reg Value_Default = 0x0;
						constexpr Reg ValueMask_Default = 0x00000000;
						constexpr Reg Value_TrappingDisabled = 0x0;
						constexpr Reg ValueMask_TrappingDisabled = 0x00000000;
						constexpr Reg Value_TrappingEnabled = 0x1;
						constexpr Reg ValueMask_TrappingEnabled = 0x00000008;
						constexpr Reg Value_AllUnalignedAccessGenerateAHardFault = 0x1;
						constexpr Reg ValueMask_AllUnalignedAccessGenerateAHardFault = 0x00000008;
						};
					namespace fDIV_0_TRP { // Field Description
						constexpr Reg Lsb = 4;
						constexpr Reg FieldMask = 0x00000010;
						constexpr Reg Value_Default = 0x0;
						constexpr Reg ValueMask_Default = 0x00000000;
						constexpr Reg Value_TrappingDisabled = 0x0;
						constexpr Reg ValueMask_TrappingDisabled = 0x00000000;
						constexpr Reg Value_TrappingEnabled = 0x1;
						constexpr Reg ValueMask_TrappingEnabled = 0x00000010;
						};
					namespace fBFHFNMIGN { // Field Description
						constexpr Reg Lsb = 8;
						constexpr Reg FieldMask = 0x00000100;
						constexpr Reg Value_Default = 0x2;
						constexpr Reg ValueMask_Default = 0x00000200;
						constexpr Reg Value_PreciseDataAccessFaultCausesLockup = 0x0;
						constexpr Reg ValueMask_PreciseDataAccessFaultCausesLockup = 0x00000000;
						constexpr Reg Value_HandlerIgnoresTheFault = 0x1;
						constexpr Reg ValueMask_HandlerIgnoresTheFault = 0x00000100;
						};
					namespace fSTKALIGN { // Field Description
						constexpr Reg Lsb = 9;
						constexpr Reg FieldMask = 0x00000200;
						constexpr Reg Value_Default = 0x0;
						constexpr Reg ValueMask_Default = 0x00000000;
						constexpr Reg Value_StackAlignmentIs4Byte = 0x0;
						constexpr Reg ValueMask_StackAlignmentIs4Byte = 0x00000000;
						constexpr Reg Value_StackAlignmentIs8ByteAdjusted = 0x1;
						constexpr Reg ValueMask_StackAlignmentIs8ByteAdjusted = 0x00000200;
						constexpr Reg Value_StackAlignment8ByteOnException = 0x1;
						constexpr Reg ValueMask_StackAlignment8ByteOnException = 0x00000200;
						};
					namespace fDC { // Field Description
						constexpr Reg Lsb = 16;
						constexpr Reg FieldMask = 0x00010000;
						constexpr Reg Value_Default = 0x0;
						constexpr Reg ValueMask_Default = 0x00000000;
						constexpr Reg Value_disable = 0x0;
						constexpr Reg ValueMask_disable = 0x00000000;
						constexpr Reg Value_disabled = 0x0;
						constexpr Reg ValueMask_disabled = 0x00000000;
						constexpr Reg Value_enable = 0x1;
						constexpr Reg ValueMask_enable = 0x00010000;
						constexpr Reg Value_enabld = 0x1;
						constexpr Reg ValueMask_enabld = 0x00010000;
						};
					namespace fIC { // Field Description
						constexpr Reg Lsb = 17;
						constexpr Reg FieldMask = 0x00020000;
						constexpr Reg Value_Default = 0x0;
						constexpr Reg ValueMask_Default = 0x00000000;
						constexpr Reg Value_disable = 0x0;
						constexpr Reg ValueMask_disable = 0x00000000;
						constexpr Reg Value_disabled = 0x0;
						constexpr Reg ValueMask_disabled = 0x00000000;
						constexpr Reg Value_enable = 0x1;
						constexpr Reg ValueMask_enable = 0x00020000;
						constexpr Reg Value_enabld = 0x1;
						constexpr Reg ValueMask_enabld = 0x00020000;
						};
					namespace fBP { // Field Description
						constexpr Reg Lsb = 18;
						constexpr Reg FieldMask = 0x00040000;
						constexpr Reg Value_Default = 0x0;
						constexpr Reg ValueMask_Default = 0x00000000;
						constexpr Reg Value_disable = 0x0;
						constexpr Reg ValueMask_disable = 0x00000000;
						constexpr Reg Value_disabled = 0x0;
						constexpr Reg ValueMask_disabled = 0x00000000;
						constexpr Reg Value_enable = 0x1;
						constexpr Reg ValueMask_enable = 0x00040000;
						constexpr Reg Value_enabld = 0x1;
						constexpr Reg ValueMask_enabld = 0x00040000;
						};
					};
				namespace rSHPR1 { // Register description
					typedef uint32_t	Reg;
					constexpr Reg	reset = 0;
					namespace fPRI_4 { // Field Description
						constexpr Reg Lsb = 0;
						constexpr Reg FieldMask = 0x000000FF;
						};
					namespace fPRI_5 { // Field Description
						constexpr Reg Lsb = 8;
						constexpr Reg FieldMask = 0x0000FF00;
						};
					namespace fPRI_6 { // Field Description
						constexpr Reg Lsb = 16;
						constexpr Reg FieldMask = 0x00FF0000;
						};
					};
				namespace rSHPR2 { // Register description
					typedef uint32_t	Reg;
					constexpr Reg	reset = 0;
					namespace fPRI_11 { // Field Description
						constexpr Reg Lsb = 24;
						constexpr Reg FieldMask = 0xFF000000;
						};
					};
				namespace rSHPR3 { // Register description
					typedef uint32_t	Reg;
					constexpr Reg	reset = 0;
					namespace fPRI_14 { // Field Description
						constexpr Reg Lsb = 16;
						constexpr Reg FieldMask = 0x00FF0000;
						};
					namespace fPRI_15 { // Field Description
						constexpr Reg Lsb = 25;
						constexpr Reg FieldMask = 0xFE000000;
						};
					};
				namespace rSHCSR { // Register description
					typedef uint32_t	Reg;
					constexpr Reg	reset = 0;
					namespace fMEMFAULTACT { // Field Description
						constexpr Reg Lsb = 0;
						constexpr Reg FieldMask = 0x00000001;
						constexpr Reg Value_NotActive = 0x0;
						constexpr Reg ValueMask_NotActive = 0x00000000;
						constexpr Reg Value_Active = 0x1;
						constexpr Reg ValueMask_Active = 0x00000001;
						};
					namespace fBUSFAULTACT { // Field Description
						constexpr Reg Lsb = 1;
						constexpr Reg FieldMask = 0x00000002;
						constexpr Reg Value_NotActive = 0x0;
						constexpr Reg ValueMask_NotActive = 0x00000000;
						constexpr Reg Value_Active = 0x1;
						constexpr Reg ValueMask_Active = 0x00000002;
						};
					namespace fUSGFAULTACT { // Field Description
						constexpr Reg Lsb = 3;
						constexpr Reg FieldMask = 0x00000008;
						constexpr Reg Value_NotActive = 0x0;
						constexpr Reg ValueMask_NotActive = 0x00000000;
						constexpr Reg Value_Active = 0x1;
						constexpr Reg ValueMask_Active = 0x00000008;
						};
					namespace fSVCALLACT { // Field Description
						constexpr Reg Lsb = 7;
						constexpr Reg FieldMask = 0x00000080;
						constexpr Reg Value_NotActive = 0x0;
						constexpr Reg ValueMask_NotActive = 0x00000000;
						constexpr Reg Value_Active = 0x1;
						constexpr Reg ValueMask_Active = 0x00000080;
						};
					namespace fMONITORACT { // Field Description
						constexpr Reg Lsb = 8;
						constexpr Reg FieldMask = 0x00000100;
						constexpr Reg Value_NotActive = 0x0;
						constexpr Reg ValueMask_NotActive = 0x00000000;
						constexpr Reg Value_Active = 0x1;
						constexpr Reg ValueMask_Active = 0x00000100;
						};
					namespace fPENDSVACT { // Field Description
						constexpr Reg Lsb = 10;
						constexpr Reg FieldMask = 0x00000400;
						constexpr Reg Value_NotActive = 0x0;
						constexpr Reg ValueMask_NotActive = 0x00000000;
						constexpr Reg Value_Active = 0x1;
						constexpr Reg ValueMask_Active = 0x00000400;
						};
					namespace fSYSTICKACT { // Field Description
						constexpr Reg Lsb = 11;
						constexpr Reg FieldMask = 0x00000800;
						constexpr Reg Value_NotActive = 0x0;
						constexpr Reg ValueMask_NotActive = 0x00000000;
						constexpr Reg Value_Active = 0x1;
						constexpr Reg ValueMask_Active = 0x00000800;
						};
					namespace fUSGFAULTPENDED { // Field Description
						constexpr Reg Lsb = 12;
						constexpr Reg FieldMask = 0x00001000;
						constexpr Reg Value_NotPending = 0x0;
						constexpr Reg ValueMask_NotPending = 0x00000000;
						constexpr Reg Value_Pending = 0x1;
						constexpr Reg ValueMask_Pending = 0x00001000;
						};
					namespace fMEMFAULTPENDED { // Field Description
						constexpr Reg Lsb = 13;
						constexpr Reg FieldMask = 0x00002000;
						constexpr Reg Value_NotPending = 0x0;
						constexpr Reg ValueMask_NotPending = 0x00000000;
						constexpr Reg Value_Pending = 0x1;
						constexpr Reg ValueMask_Pending = 0x00002000;
						};
					namespace fBUSFAULTPENDED { // Field Description
						constexpr Reg Lsb = 14;
						constexpr Reg FieldMask = 0x00004000;
						constexpr Reg Value_NotPending = 0x0;
						constexpr Reg ValueMask_NotPending = 0x00000000;
						constexpr Reg Value_Pending = 0x1;
						constexpr Reg ValueMask_Pending = 0x00004000;
						};
					namespace fSVCALLPENDED { // Field Description
						constexpr Reg Lsb = 15;
						constexpr Reg FieldMask = 0x00008000;
						constexpr Reg Value_NotPending = 0x0;
						constexpr Reg ValueMask_NotPending = 0x00000000;
						constexpr Reg Value_Pending = 0x1;
						constexpr Reg ValueMask_Pending = 0x00008000;
						};
					namespace fMEMFAULTENA { // Field Description
						constexpr Reg Lsb = 16;
						constexpr Reg FieldMask = 0x00010000;
						constexpr Reg Value_Disable = 0x0;
						constexpr Reg ValueMask_Disable = 0x00000000;
						constexpr Reg Value_Enable = 0x1;
						constexpr Reg ValueMask_Enable = 0x00010000;
						};
					namespace fBUSFAULTENA { // Field Description
						constexpr Reg Lsb = 17;
						constexpr Reg FieldMask = 0x00020000;
						constexpr Reg Value_Disable = 0x0;
						constexpr Reg ValueMask_Disable = 0x00000000;
						constexpr Reg Value_Enable = 0x1;
						constexpr Reg ValueMask_Enable = 0x00020000;
						};
					namespace fUSGFAULTENA { // Field Description
						constexpr Reg Lsb = 18;
						constexpr Reg FieldMask = 0x00040000;
						constexpr Reg Value_Disable = 0x0;
						constexpr Reg ValueMask_Disable = 0x00000000;
						constexpr Reg Value_Enable = 0x1;
						constexpr Reg ValueMask_Enable = 0x00040000;
						};
					};
				namespace rCFSR { // Register description
					typedef uint32_t	Reg;
					constexpr Reg	reset = 0;
					namespace fMMFSR { // Field Description
						constexpr Reg Lsb = 0;
						constexpr Reg FieldMask = 0x000000FF;
						namespace fIACCVIOL { // Field Description
							constexpr Reg Lsb = 0;
							constexpr Reg FieldMask = 0x00000001;
							constexpr Reg Value_NoMpuOrExecuteNeverViolation = 0x0;
							constexpr Reg ValueMask_NoMpuOrExecuteNeverViolation = 0x00000000;
							constexpr Reg Value_MpuOrExecuteNeverViolation = 0x1;
							constexpr Reg ValueMask_MpuOrExecuteNeverViolation = 0x00000001;
							};
						namespace fDACCVIOL { // Field Description
							constexpr Reg Lsb = 1;
							constexpr Reg FieldMask = 0x00000002;
							constexpr Reg Value_NoDataAccessViolation = 0x0;
							constexpr Reg ValueMask_NoDataAccessViolation = 0x00000000;
							constexpr Reg Value_DataAccessViolation = 0x1;
							constexpr Reg ValueMask_DataAccessViolation = 0x00000002;
							};
						namespace fMUNSTKERR { // Field Description
							constexpr Reg Lsb = 3;
							constexpr Reg FieldMask = 0x00000008;
							constexpr Reg Value_NoExceptionReturnDerivedMemManageFault = 0x0;
							constexpr Reg ValueMask_NoExceptionReturnDerivedMemManageFault = 0x00000000;
							constexpr Reg Value_ExceptionReturnDerivedMemManageFault = 0x1;
							constexpr Reg ValueMask_ExceptionReturnDerivedMemManageFault = 0x00000008;
							};
						namespace fMSTKERR { // Field Description
							constexpr Reg Lsb = 4;
							constexpr Reg FieldMask = 0x00000010;
							constexpr Reg Value_NoExceptionEntryDerivedMemManageFault = 0x0;
							constexpr Reg ValueMask_NoExceptionEntryDerivedMemManageFault = 0x00000000;
							constexpr Reg Value_ExceptionEntryDerivedMemManageFault = 0x1;
							constexpr Reg ValueMask_ExceptionEntryDerivedMemManageFault = 0x00000010;
							};
						namespace fMLSPERR { // Field Description
							constexpr Reg Lsb = 5;
							constexpr Reg FieldMask = 0x00000020;
							constexpr Reg Value_NoFpLazyStatePreservationMemManageFault = 0x0;
							constexpr Reg ValueMask_NoFpLazyStatePreservationMemManageFault = 0x00000000;
							constexpr Reg Value_pLazyStatePreservationMemManageFault = 0x1;
							constexpr Reg ValueMask_pLazyStatePreservationMemManageFault = 0x00000020;
							};
						namespace fMMARVALID { // Field Description
							constexpr Reg Lsb = 7;
							constexpr Reg FieldMask = 0x00000080;
							constexpr Reg Value_MmarNotValid = 0x0;
							constexpr Reg ValueMask_MmarNotValid = 0x00000000;
							constexpr Reg Value_MmarValid = 0x1;
							constexpr Reg ValueMask_MmarValid = 0x00000080;
							};
						};
					namespace fBFSR { // Field Description
						constexpr Reg Lsb = 8;
						constexpr Reg FieldMask = 0x0000FF00;
						namespace fIBUSERR { // Field Description
							constexpr Reg Lsb = 16;
							constexpr Reg FieldMask = 0x00010000;
							constexpr Reg Value_NoBusFault = 0x0;
							constexpr Reg ValueMask_NoBusFault = 0x00000000;
							constexpr Reg Value_BusFault = 0x1;
							constexpr Reg ValueMask_BusFault = 0x00010000;
							};
						namespace fPRECISERR { // Field Description
							constexpr Reg Lsb = 17;
							constexpr Reg FieldMask = 0x00020000;
							constexpr Reg Value_NoDataAccessError = 0x0;
							constexpr Reg ValueMask_NoDataAccessError = 0x00000000;
							constexpr Reg Value_DataAccessError = 0x1;
							constexpr Reg ValueMask_DataAccessError = 0x00020000;
							};
						namespace fIMPRECISERR { // Field Description
							constexpr Reg Lsb = 18;
							constexpr Reg FieldMask = 0x00040000;
							constexpr Reg Value_NoDataAccessError = 0x0;
							constexpr Reg ValueMask_NoDataAccessError = 0x00000000;
							constexpr Reg Value_DataAccessError = 0x1;
							constexpr Reg ValueMask_DataAccessError = 0x00040000;
							};
						namespace fUNSTKERR { // Field Description
							constexpr Reg Lsb = 19;
							constexpr Reg FieldMask = 0x00080000;
							constexpr Reg Value_NoExceptionReturnDerivedBusFault = 0x0;
							constexpr Reg ValueMask_NoExceptionReturnDerivedBusFault = 0x00000000;
							constexpr Reg Value_ExceptionReturnDerivedBusFault = 0x1;
							constexpr Reg ValueMask_ExceptionReturnDerivedBusFault = 0x00080000;
							};
						namespace fSTKERR { // Field Description
							constexpr Reg Lsb = 20;
							constexpr Reg FieldMask = 0x00100000;
							constexpr Reg Value_NoExceptionEntryDerivedBusFault = 0x0;
							constexpr Reg ValueMask_NoExceptionEntryDerivedBusFault = 0x00000000;
							constexpr Reg Value_ExceptionEntryDerivedBusFault = 0x1;
							constexpr Reg ValueMask_ExceptionEntryDerivedBusFault = 0x00100000;
							};
						namespace fLSPERR { // Field Description
							constexpr Reg Lsb = 21;
							constexpr Reg FieldMask = 0x00200000;
							constexpr Reg Value_NoFpLazyBusFault = 0x0;
							constexpr Reg ValueMask_NoFpLazyBusFault = 0x00000000;
							constexpr Reg Value_FpLazyBusFault = 0x1;
							constexpr Reg ValueMask_FpLazyBusFault = 0x00200000;
							};
						namespace fBFARVALID { // Field Description
							constexpr Reg Lsb = 23;
							constexpr Reg FieldMask = 0x00800000;
							constexpr Reg Value_BfarNotValid = 0x0;
							constexpr Reg ValueMask_BfarNotValid = 0x00000000;
							constexpr Reg Value_BfarValid = 0x1;
							constexpr Reg ValueMask_BfarValid = 0x00800000;
							};
						};
					namespace fUFSR { // Field Description
						constexpr Reg Lsb = 16;
						constexpr Reg FieldMask = 0xFFFF0000;
						namespace fUNDEFINSTR { // Field Description
							constexpr Reg Lsb = 32;
							constexpr Reg FieldMask = 0x00000000;
							constexpr Reg Value_NoUndefinedInstructionUsageFault = 0x0;
							constexpr Reg ValueMask_NoUndefinedInstructionUsageFault = 0x00000000;
							constexpr Reg Value_UndefinedInstructionUsageFault = 0x1;
							constexpr Reg ValueMask_UndefinedInstructionUsageFault = 0x00000001;
							};
						namespace fINVSTATE { // Field Description
							constexpr Reg Lsb = 33;
							constexpr Reg FieldMask = 0x00000000;
							constexpr Reg Value_ValidEpsrIT = 0x0;
							constexpr Reg ValueMask_ValidEpsrIT = 0x00000000;
							constexpr Reg Value_IvalidEpsrtIT = 0x1;
							constexpr Reg ValueMask_IvalidEpsrtIT = 0x00000002;
							};
						namespace fINVPC { // Field Description
							constexpr Reg Lsb = 34;
							constexpr Reg FieldMask = 0x00000000;
							constexpr Reg Value_NoError = 0x0;
							constexpr Reg ValueMask_NoError = 0x00000000;
							constexpr Reg Value_Error = 0x1;
							constexpr Reg ValueMask_Error = 0x00000004;
							};
						namespace fNOCP { // Field Description
							constexpr Reg Lsb = 35;
							constexpr Reg FieldMask = 0x00000000;
							constexpr Reg Value_NoError = 0x0;
							constexpr Reg ValueMask_NoError = 0x00000000;
							constexpr Reg Value_Error = 0x1;
							constexpr Reg ValueMask_Error = 0x00000008;
							};
						namespace fUNALIGNED { // Field Description
							constexpr Reg Lsb = 40;
							constexpr Reg FieldMask = 0x00000000;
							constexpr Reg Value_NoUnaligedAccess = 0x0;
							constexpr Reg ValueMask_NoUnaligedAccess = 0x00000000;
							constexpr Reg Value_UnaligedAccess = 0x1;
							constexpr Reg ValueMask_UnaligedAccess = 0x00000100;
							};
						namespace fDIVBYZERO { // Field Description
							constexpr Reg Lsb = 41;
							constexpr Reg FieldMask = 0x00000000;
							constexpr Reg Value_NoDivideByZerorError = 0x0;
							constexpr Reg ValueMask_NoDivideByZerorError = 0x00000000;
							constexpr Reg Value_DivideByZerorError = 0x1;
							constexpr Reg ValueMask_DivideByZerorError = 0x00000200;
							};
						};
					};
				namespace rHFSR { // Register description
					typedef uint32_t	Reg;
					constexpr Reg	reset = 0;
					namespace fVECTTBL { // Field Description
						constexpr Reg Lsb = 1;
						constexpr Reg FieldMask = 0x00000002;
						constexpr Reg Value_NoReadFault = 0x0;
						constexpr Reg ValueMask_NoReadFault = 0x00000000;
						constexpr Reg Value_ReadFault = 0x1;
						constexpr Reg ValueMask_ReadFault = 0x00000002;
						};
					namespace fFORCED { // Field Description
						constexpr Reg Lsb = 30;
						constexpr Reg FieldMask = 0x40000000;
						constexpr Reg Value_NoPriorityEscalation = 0x0;
						constexpr Reg ValueMask_NoPriorityEscalation = 0x00000000;
						constexpr Reg Value_PriorityEscalation = 0x1;
						constexpr Reg ValueMask_PriorityEscalation = 0x40000000;
						};
					namespace fDEBUGEVT { // Field Description
						constexpr Reg Lsb = 30;
						constexpr Reg FieldMask = 0x40000000;
						constexpr Reg Value_NoHaltingDebugDisabledEvent = 0x0;
						constexpr Reg ValueMask_NoHaltingDebugDisabledEvent = 0x00000000;
						constexpr Reg Value_HaltingDebugDisabledEvent = 0x1;
						constexpr Reg ValueMask_HaltingDebugDisabledEvent = 0x40000000;
						};
					};
				namespace rMMFAR { // Register description
					typedef uint32_t	Reg;
					namespace fADDRESS { // Field Description
						constexpr Reg Lsb = 0;
						constexpr Reg FieldMask = 0xFFFFFFFF;
						};
					};
				namespace rBFAR { // Register description
					typedef uint32_t	Reg;
					namespace fADDRESS { // Field Description
						constexpr Reg Lsb = 0;
						constexpr Reg FieldMask = 0xFFFFFFFF;
						};
					};
				namespace rAFSR { // Register description
					typedef uint32_t	Reg;
					namespace fIMPDEF { // Field Description
						constexpr Reg Lsb = 0;
						constexpr Reg FieldMask = 0xFFFFFFFF;
						};
					};
				}
			}
		}
	}
#endif
