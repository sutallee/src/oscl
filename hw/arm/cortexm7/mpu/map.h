/*
   Copyright (C) 2024 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/
#ifndef _oscl_hw_arm_cortexm7_mpu_maph_
#define _oscl_hw_arm_cortexm7_mpu_maph_
#include "reg.h"

/** */
namespace Oscl {
/** */
namespace Arm {
/** */
namespace CortexM7 {
/** */
namespace Mpu {

/** 0xE000ED90 */
struct Map {
	/** 0xE000ED90, offset 0x00000000 */
	volatile Oscl::Arm::CortexM7::pMPU::rTYPE::Reg	type;

	/** 0xE000ED94, offset 0x0000004 */
	volatile Oscl::Arm::CortexM7::pMPU::rCTRL::Reg	ctrl;

	/** 0xE000ED98, offset 0x00000008 */
	volatile Oscl::Arm::CortexM7::pMPU::rRNR::Reg	rnr;

	/** 0xE000ED9C, offset 0x0000000C */
	volatile Oscl::Arm::CortexM7::pMPU::rRBAR::Reg	rbar;

	/** 0xE000EDA0, offset 0x00000010 */
	volatile Oscl::Arm::CortexM7::pMPU::rRASR::Reg	rasr;

	/** 0xE000EDA4, offset 0x00000014 */
	volatile Oscl::Arm::CortexM7::pMPU::rRBAR_A1::Reg	rbar_a1;

	/** 0xE000EDA8, offset 0x00000018 */
	volatile Oscl::Arm::CortexM7::pMPU::rRASR_A1 ::Reg	rasr_a1;

	/** 0xE000EDAC, offset 0x0000001C */
	volatile Oscl::Arm::CortexM7::pMPU::rRBAR_A2::Reg	rbar_a2;

	/** 0xE000EDB0, offset 0x00000020 */
	volatile Oscl::Arm::CortexM7::pMPU::rRASR_A2 ::Reg	rasr_a2;

	/** 0xE000EDB4, offset 0x00000024 */
	volatile Oscl::Arm::CortexM7::pMPU::rRBAR_A3::Reg	rbar_a3;

	/** 0xE000EDB8, offset 0x00000028 */
	volatile Oscl::Arm::CortexM7::pMPU::rRASR_A3 ::Reg	rasr_a3;

	};

}
}
}
}

#endif
