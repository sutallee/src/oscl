/*
   Copyright (C) 2024 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _hw_arm_cortexm7_cache_regh_
#define _hw_arm_cortexm7_cache_regh_
#include <stdint.h>
namespace Oscl { // Namespace description
	namespace Arm { // Namespace description
		namespace CortexM7 { // Namespace description
			namespace pMPU { // Namespace description
				namespace rTYPE { // Register description
					typedef uint32_t	Reg;
					constexpr Reg	valueAtReset = 0x00000800;
					namespace fDREGION { // Field Description
						constexpr Reg Lsb = 8;
						constexpr Reg FieldMask = 0x0000FF00;
						constexpr Reg Value_valueAtReset = 0x8;
						constexpr Reg ValueMask_valueAtReset = 0x00000800;
						constexpr Reg Value_minValue = 0x0;
						constexpr Reg ValueMask_minValue = 0x00000000;
						constexpr Reg Value_maxValue = 0xFF;
						constexpr Reg ValueMask_maxValue = 0x0000FF00;
						};
					namespace fIREGION { // Field Description
						constexpr Reg Lsb = 16;
						constexpr Reg FieldMask = 0x00FF0000;
						constexpr Reg Value_valueAtReset = 0x0;
						constexpr Reg ValueMask_valueAtReset = 0x00000000;
						constexpr Reg Value_minValue = 0x0;
						constexpr Reg ValueMask_minValue = 0x00000000;
						constexpr Reg Value_maxValue = 0x0;
						constexpr Reg ValueMask_maxValue = 0x00000000;
						};
					};
				namespace rCTRL { // Register description
					typedef uint32_t	Reg;
					constexpr Reg	valueAtReset = 0x00000000;
					namespace fENABLE { // Field Description
						constexpr Reg Lsb = 0;
						constexpr Reg FieldMask = 0x00000001;
						constexpr Reg Value_valueAtReset = 0x0;
						constexpr Reg ValueMask_valueAtReset = 0x00000000;
						constexpr Reg Value_disable = 0x0;
						constexpr Reg ValueMask_disable = 0x00000000;
						constexpr Reg Value_disabled = 0x0;
						constexpr Reg ValueMask_disabled = 0x00000000;
						constexpr Reg Value_enable = 0x1;
						constexpr Reg ValueMask_enable = 0x00000001;
						constexpr Reg Value_enabled = 0x1;
						constexpr Reg ValueMask_enabled = 0x00000001;
						};
					namespace fHFNMIENA { // Field Description
						constexpr Reg Lsb = 1;
						constexpr Reg FieldMask = 0x00000002;
						constexpr Reg Value_valueAtReset = 0x0;
						constexpr Reg ValueMask_valueAtReset = 0x00000000;
						constexpr Reg Value_mpuDisabledForHardFault = 0x0;
						constexpr Reg ValueMask_mpuDisabledForHardFault = 0x00000000;
						constexpr Reg Value_mpuEnabledForHardFault = 0x1;
						constexpr Reg ValueMask_mpuEnabledForHardFault = 0x00000002;
						};
					namespace fPRIVDEFENA { // Field Description
						constexpr Reg Lsb = 2;
						constexpr Reg FieldMask = 0x00000004;
						constexpr Reg Value_valueAtReset = 0x0;
						constexpr Reg ValueMask_valueAtReset = 0x00000000;
						constexpr Reg Value_disableDefaultMap = 0x0;
						constexpr Reg ValueMask_disableDefaultMap = 0x00000000;
						constexpr Reg Value_enableDefaultMap = 0x1;
						constexpr Reg ValueMask_enableDefaultMap = 0x00000004;
						};
					};
				namespace rRNR { // Register description
					typedef uint32_t	Reg;
					namespace fREGION { // Field Description
						constexpr Reg Lsb = 0;
						constexpr Reg FieldMask = 0x000000FF;
						constexpr Reg Value_minValue = 0x0;
						constexpr Reg ValueMask_minValue = 0x00000000;
						constexpr Reg Value_someMaxValue = 0x7;
						constexpr Reg ValueMask_someMaxValue = 0x00000007;
						constexpr Reg Value_maxValue = 0xF;
						constexpr Reg ValueMask_maxValue = 0x0000000F;
						};
					};
				namespace rRBAR { // Register description
					typedef uint32_t	Reg;
					namespace fREGION { // Field Description
						constexpr Reg Lsb = 0;
						constexpr Reg FieldMask = 0x0000000F;
						constexpr Reg Value_minValue = 0x0;
						constexpr Reg ValueMask_minValue = 0x00000000;
						constexpr Reg Value_maxValue = 0xF;
						constexpr Reg ValueMask_maxValue = 0x0000000F;
						};
					namespace fVALID { // Field Description
						constexpr Reg Lsb = 4;
						constexpr Reg FieldMask = 0x00000010;
						constexpr Reg Value_ignoreRegionField = 0x0;
						constexpr Reg ValueMask_ignoreRegionField = 0x00000000;
						constexpr Reg Value_updateRegionField = 0x1;
						constexpr Reg ValueMask_updateRegionField = 0x00000010;
						};
					namespace fADDR { // Field Description
						constexpr Reg Lsb = 5;
						constexpr Reg FieldMask = 0xFFFFFFE0;
						};
					};
				namespace rRASR { // Register description
					typedef uint32_t	Reg;
					namespace fENABLE { // Field Description
						constexpr Reg Lsb = 0;
						constexpr Reg FieldMask = 0x00000001;
						constexpr Reg Value_disable = 0x0;
						constexpr Reg ValueMask_disable = 0x00000000;
						constexpr Reg Value_disabled = 0x0;
						constexpr Reg ValueMask_disabled = 0x00000000;
						constexpr Reg Value_enable = 0x1;
						constexpr Reg ValueMask_enable = 0x00000001;
						constexpr Reg Value_enabled = 0x1;
						constexpr Reg ValueMask_enabled = 0x00000001;
						};
					namespace fSIZE { // Field Description
						constexpr Reg Lsb = 1;
						constexpr Reg FieldMask = 0x0000003E;
						constexpr Reg Value_minValue = 0x4;
						constexpr Reg ValueMask_minValue = 0x00000008;
						constexpr Reg Value_maxValue = 0x1F;
						constexpr Reg ValueMask_maxValue = 0x0000003E;
						constexpr Reg Value_v32B = 0x4;
						constexpr Reg ValueMask_v32B = 0x00000008;
						constexpr Reg Value_v64B = 0x5;
						constexpr Reg ValueMask_v64B = 0x0000000A;
						constexpr Reg Value_v128B = 0x6;
						constexpr Reg ValueMask_v128B = 0x0000000C;
						constexpr Reg Value_v256B = 0x7;
						constexpr Reg ValueMask_v256B = 0x0000000E;
						constexpr Reg Value_v512B = 0x8;
						constexpr Reg ValueMask_v512B = 0x00000010;
						constexpr Reg Value_v1KB = 0x9;
						constexpr Reg ValueMask_v1KB = 0x00000012;
						constexpr Reg Value_v2KB = 0xA;
						constexpr Reg ValueMask_v2KB = 0x00000014;
						constexpr Reg Value_v4KB = 0xB;
						constexpr Reg ValueMask_v4KB = 0x00000016;
						constexpr Reg Value_v8KB = 0xC;
						constexpr Reg ValueMask_v8KB = 0x00000018;
						constexpr Reg Value_v16KB = 0xD;
						constexpr Reg ValueMask_v16KB = 0x0000001A;
						constexpr Reg Value_v32KB = 0xE;
						constexpr Reg ValueMask_v32KB = 0x0000001C;
						constexpr Reg Value_v64KB = 0xF;
						constexpr Reg ValueMask_v64KB = 0x0000001E;
						constexpr Reg Value_v128KB = 0x10;
						constexpr Reg ValueMask_v128KB = 0x00000020;
						constexpr Reg Value_v256KB = 0x11;
						constexpr Reg ValueMask_v256KB = 0x00000022;
						constexpr Reg Value_v512KB = 0x12;
						constexpr Reg ValueMask_v512KB = 0x00000024;
						constexpr Reg Value_v1MB = 0x13;
						constexpr Reg ValueMask_v1MB = 0x00000026;
						constexpr Reg Value_v2MB = 0x14;
						constexpr Reg ValueMask_v2MB = 0x00000028;
						constexpr Reg Value_v4MB = 0x15;
						constexpr Reg ValueMask_v4MB = 0x0000002A;
						constexpr Reg Value_v8MB = 0x16;
						constexpr Reg ValueMask_v8MB = 0x0000002C;
						constexpr Reg Value_v16MB = 0x17;
						constexpr Reg ValueMask_v16MB = 0x0000002E;
						constexpr Reg Value_v32MB = 0x18;
						constexpr Reg ValueMask_v32MB = 0x00000030;
						constexpr Reg Value_v64MB = 0x19;
						constexpr Reg ValueMask_v64MB = 0x00000032;
						constexpr Reg Value_v128MB = 0x1A;
						constexpr Reg ValueMask_v128MB = 0x00000034;
						constexpr Reg Value_v256MB = 0x1B;
						constexpr Reg ValueMask_v256MB = 0x00000036;
						constexpr Reg Value_v512MB = 0x1C;
						constexpr Reg ValueMask_v512MB = 0x00000038;
						constexpr Reg Value_v1GB = 0x1D;
						constexpr Reg ValueMask_v1GB = 0x0000003A;
						constexpr Reg Value_v2GB = 0x1E;
						constexpr Reg ValueMask_v2GB = 0x0000003C;
						constexpr Reg Value_v4GB = 0x1F;
						constexpr Reg ValueMask_v4GB = 0x0000003E;
						};
					namespace fSRD { // Field Description
						constexpr Reg Lsb = 8;
						constexpr Reg FieldMask = 0x0000FF00;
						};
					namespace fB { // Field Description
						constexpr Reg Lsb = 16;
						constexpr Reg FieldMask = 0x00010000;
						constexpr Reg Value_notBufferable = 0x0;
						constexpr Reg ValueMask_notBufferable = 0x00000000;
						constexpr Reg Value_bufferable = 0x1;
						constexpr Reg ValueMask_bufferable = 0x00010000;
						};
					namespace fC { // Field Description
						constexpr Reg Lsb = 17;
						constexpr Reg FieldMask = 0x00020000;
						constexpr Reg Value_notCacheable = 0x0;
						constexpr Reg ValueMask_notCacheable = 0x00000000;
						constexpr Reg Value_cacheable = 0x1;
						constexpr Reg ValueMask_cacheable = 0x00020000;
						};
					namespace fS { // Field Description
						constexpr Reg Lsb = 18;
						constexpr Reg FieldMask = 0x00040000;
						constexpr Reg Value_notShareable = 0x0;
						constexpr Reg ValueMask_notShareable = 0x00000000;
						constexpr Reg Value_shareable = 0x1;
						constexpr Reg ValueMask_shareable = 0x00040000;
						};
					namespace fTEX { // Field Description
						constexpr Reg Lsb = 19;
						constexpr Reg FieldMask = 0x00380000;
						constexpr Reg Value_stronglyOrderedDeviceNormal = 0x0;
						constexpr Reg ValueMask_stronglyOrderedDeviceNormal = 0x00000000;
						constexpr Reg Value_outerAndInnerNonCacheable = 0x1;
						constexpr Reg ValueMask_outerAndInnerNonCacheable = 0x00080000;
						constexpr Reg Value_deviceNotShared = 0x2;
						constexpr Reg ValueMask_deviceNotShared = 0x00100000;
						constexpr Reg Value_nonCacheable = 0x4;
						constexpr Reg ValueMask_nonCacheable = 0x00200000;
						constexpr Reg Value_writeBackAndReadAllocate = 0x5;
						constexpr Reg ValueMask_writeBackAndReadAllocate = 0x00280000;
						constexpr Reg Value_writeThroughNoWriteAllocate = 0x6;
						constexpr Reg ValueMask_writeThroughNoWriteAllocate = 0x00300000;
						constexpr Reg Value_writeBackNoWriteAllocate = 0x7;
						constexpr Reg ValueMask_writeBackNoWriteAllocate = 0x00380000;
						constexpr Reg Value_level0 = 0x0;
						constexpr Reg ValueMask_level0 = 0x00000000;
						constexpr Reg Value_level1 = 0x1;
						constexpr Reg ValueMask_level1 = 0x00080000;
						constexpr Reg Value_level2 = 0x2;
						constexpr Reg ValueMask_level2 = 0x00100000;
						};
					namespace fAP { // Field Description
						constexpr Reg Lsb = 24;
						constexpr Reg FieldMask = 0x07000000;
						constexpr Reg Value_noAccess = 0x0;
						constexpr Reg ValueMask_noAccess = 0x00000000;
						constexpr Reg Value_privRwUnprivNoAccess = 0x1;
						constexpr Reg ValueMask_privRwUnprivNoAccess = 0x01000000;
						constexpr Reg Value_privRwUnprivReadOnly = 0x2;
						constexpr Reg ValueMask_privRwUnprivReadOnly = 0x02000000;
						constexpr Reg Value_privRwUnprivRw = 0x3;
						constexpr Reg ValueMask_privRwUnprivRw = 0x03000000;
						constexpr Reg Value_privRoUnprivNoAccess = 0x5;
						constexpr Reg ValueMask_privRoUnprivNoAccess = 0x05000000;
						constexpr Reg Value_privRoUnprivRo = 0x6;
						constexpr Reg ValueMask_privRoUnprivRo = 0x06000000;
						constexpr Reg Value_privRoUnprivRoAlso = 0x7;
						constexpr Reg ValueMask_privRoUnprivRoAlso = 0x07000000;
						};
					namespace fXN { // Field Description
						constexpr Reg Lsb = 28;
						constexpr Reg FieldMask = 0x10000000;
						constexpr Reg Value_disable = 0x0;
						constexpr Reg ValueMask_disable = 0x00000000;
						constexpr Reg Value_disabled = 0x0;
						constexpr Reg ValueMask_disabled = 0x00000000;
						constexpr Reg Value_enable = 0x1;
						constexpr Reg ValueMask_enable = 0x10000000;
						constexpr Reg Value_enabled = 0x1;
						constexpr Reg ValueMask_enabled = 0x10000000;
						};
					};
				namespace rRBAR_A1 { // Register description
					typedef uint32_t	Reg;
					};
				namespace rRASR_A1 { // Register description
					typedef uint32_t	Reg;
					};
				namespace rRBAR_A2 { // Register description
					typedef uint32_t	Reg;
					};
				namespace rRASR_A2 { // Register description
					typedef uint32_t	Reg;
					};
				namespace rRBAR_A3 { // Register description
					typedef uint32_t	Reg;
					};
				namespace rRASR_A3 { // Register description
					typedef uint32_t	Reg;
					};
				}
			}
		}
	}
#endif
