/*
   Copyright (C) 2024 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _hw_arm_cortexm7_cacheh_
#define _hw_arm_cortexm7_cacheh_
#include <stdint.h>
namespace Oscl { // Namespace description
	namespace Arm { // Namespace description
		namespace CortexM7 { // Namespace description
			namespace pCACHE { // Namespace description
				namespace rICIALLU { // Register description
					typedef uint32_t	Reg;
					};
				namespace rICIMVAU { // Register description
					typedef uint32_t	Reg;
					namespace fMVA { // Field Description
						constexpr Reg Lsb = 5;
						constexpr Reg FieldMask = 0xFFFFFFE0;
						constexpr Reg Value_minValue = 0x0;
						constexpr Reg ValueMask_minValue = 0x00000000;
						constexpr Reg Value_maxValue = 0x3FFFFFF;
						constexpr Reg ValueMask_maxValue = 0x7FFFFFE0;
						};
					};
				namespace rDCIMVAC { // Register description
					typedef uint32_t	Reg;
					namespace fMVA { // Field Description
						constexpr Reg Lsb = 5;
						constexpr Reg FieldMask = 0xFFFFFFE0;
						constexpr Reg Value_minValue = 0x0;
						constexpr Reg ValueMask_minValue = 0x00000000;
						constexpr Reg Value_maxValue = 0x3FFFFFF;
						constexpr Reg ValueMask_maxValue = 0x7FFFFFE0;
						};
					};
				namespace rDCISW { // Register description
					typedef uint32_t	Reg;
					namespace fSet { // Field Description
						constexpr Reg Lsb = 5;
						constexpr Reg FieldMask = 0x00003FE0;
						constexpr Reg Value_minValue = 0x0;
						constexpr Reg ValueMask_minValue = 0x00000000;
						constexpr Reg Value_maxValue = 0x1FF;
						constexpr Reg ValueMask_maxValue = 0x00003FE0;
						};
					namespace fWay { // Field Description
						constexpr Reg Lsb = 30;
						constexpr Reg FieldMask = 0xC0000000;
						constexpr Reg Value_minValue = 0x0;
						constexpr Reg ValueMask_minValue = 0x00000000;
						constexpr Reg Value_maxValue = 0x3;
						constexpr Reg ValueMask_maxValue = 0xC0000000;
						};
					};
				namespace rDCCMVAU { // Register description
					typedef uint32_t	Reg;
					namespace fMVA { // Field Description
						constexpr Reg Lsb = 5;
						constexpr Reg FieldMask = 0xFFFFFFE0;
						constexpr Reg Value_minValue = 0x0;
						constexpr Reg ValueMask_minValue = 0x00000000;
						constexpr Reg Value_maxValue = 0x3FFFFFF;
						constexpr Reg ValueMask_maxValue = 0x7FFFFFE0;
						};
					};
				namespace rDCCMVAC { // Register description
					typedef uint32_t	Reg;
					namespace fMVA { // Field Description
						constexpr Reg Lsb = 5;
						constexpr Reg FieldMask = 0xFFFFFFE0;
						constexpr Reg Value_minValue = 0x0;
						constexpr Reg ValueMask_minValue = 0x00000000;
						constexpr Reg Value_maxValue = 0x3FFFFFF;
						constexpr Reg ValueMask_maxValue = 0x7FFFFFE0;
						};
					};
				namespace rDCCSW { // Register description
					typedef uint32_t	Reg;
					namespace fSet { // Field Description
						constexpr Reg Lsb = 5;
						constexpr Reg FieldMask = 0x00003FE0;
						constexpr Reg Value_minValue = 0x0;
						constexpr Reg ValueMask_minValue = 0x00000000;
						constexpr Reg Value_maxValue = 0x1FF;
						constexpr Reg ValueMask_maxValue = 0x00003FE0;
						};
					namespace fWay { // Field Description
						constexpr Reg Lsb = 30;
						constexpr Reg FieldMask = 0xC0000000;
						constexpr Reg Value_minValue = 0x0;
						constexpr Reg ValueMask_minValue = 0x00000000;
						constexpr Reg Value_maxValue = 0x3;
						constexpr Reg ValueMask_maxValue = 0xC0000000;
						};
					};
				namespace rDCCIMVAC { // Register description
					typedef uint32_t	Reg;
					namespace fMVA { // Field Description
						constexpr Reg Lsb = 5;
						constexpr Reg FieldMask = 0xFFFFFFE0;
						constexpr Reg Value_minValue = 0x0;
						constexpr Reg ValueMask_minValue = 0x00000000;
						constexpr Reg Value_maxValue = 0x3FFFFFF;
						constexpr Reg ValueMask_maxValue = 0x7FFFFFE0;
						};
					};
				namespace rDCCISW { // Register description
					typedef uint32_t	Reg;
					namespace fSet { // Field Description
						constexpr Reg Lsb = 5;
						constexpr Reg FieldMask = 0x00003FE0;
						constexpr Reg Value_minValue = 0x0;
						constexpr Reg ValueMask_minValue = 0x00000000;
						constexpr Reg Value_maxValue = 0x1FF;
						constexpr Reg ValueMask_maxValue = 0x00003FE0;
						};
					namespace fWay { // Field Description
						constexpr Reg Lsb = 30;
						constexpr Reg FieldMask = 0xC0000000;
						constexpr Reg Value_minValue = 0x0;
						constexpr Reg ValueMask_minValue = 0x00000000;
						constexpr Reg Value_maxValue = 0x3;
						constexpr Reg ValueMask_maxValue = 0xC0000000;
						};
					};
				namespace rBPIALL { // Register description
					typedef uint32_t	Reg;
					};
				}
			}
		}
	}
#endif
