/*
   Copyright (C) 2024 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/
#ifndef _oscl_hw_arm_cortexm7_cache_maph_
#define _oscl_hw_arm_cortexm7_cache_maph_
#include "reg.h"

/** */
namespace Oscl {
/** */
namespace Arm {
/** */
namespace CortexM7 {
/** */
namespace Cache {

/** 0xE000EF50 */
struct Map {
	/** 0xE000EF50, offset 0x00000000 */
	volatile Oscl::Arm::CortexM7::pCACHE::rICIALLU::Reg	iciallu;

	/** 0xE000EF54, offset 0x00000004 */
	uint8_t	reservedE000EF54[0xE000EF58-0xE000EF54];

	/** 0xE000EF58, offset 0x0000008 */
	volatile Oscl::Arm::CortexM7::pCACHE::rICIMVAU::Reg	icimvau;

	/** 0xE000EF5C, offset 0x0000000C */
	volatile Oscl::Arm::CortexM7::pCACHE::rDCIMVAC::Reg	dcimvac;

	/** 0xE000EF60, offset 0x00000010 */
	volatile Oscl::Arm::CortexM7::pCACHE::rDCISW::Reg	dcisw;

	/** 0xE000EF64, offset 0x00000014 */
	volatile Oscl::Arm::CortexM7::pCACHE::rDCCMVAU::Reg	dccmvau;

	/** 0xE000EF68, offset 0x00000018 */
	volatile Oscl::Arm::CortexM7::pCACHE::rDCCMVAC::Reg		dccmvac;

	/** 0xE000EF6C, offset 0x0000001C */
	volatile Oscl::Arm::CortexM7::pCACHE::rDCCSW ::Reg		dccsw;

	/** 0xE000EF70, offset 0x00000020 */
	volatile Oscl::Arm::CortexM7::pCACHE::rDCCIMVAC::Reg	dccimvac;

	/** 0xE000EF74, offset 0x00000024 */
	volatile Oscl::Arm::CortexM7::pCACHE::rDCCISW::Reg		dccisw;

	/** 0xE000EF78, offset 0x00000028 */
	volatile Oscl::Arm::CortexM7::pCACHE::rBPIALL::Reg		bpiall;
	};

}
}
}
}

#endif
