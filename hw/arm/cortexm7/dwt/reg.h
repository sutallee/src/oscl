/*
   Copyright (C) 2024 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _hw_arm_cortexm7_dwt_regh_
#define _hw_arm_cortexm7_dwt_regh_
#include <stdint.h>
namespace Oscl { // Namespace description
	namespace Arm { // Namespace description
		namespace CortexM7 { // Namespace description
			namespace pDWT { // Namespace description
				namespace rCTRL { // Register description
					typedef uint32_t	Reg;
					constexpr Reg	valueAtResetSTM32H7A3B3B0 = 0x40000000;
					namespace fCYCCNTENA { // Field Description
						constexpr Reg Lsb = 0;
						constexpr Reg FieldMask = 0x00000001;
						constexpr Reg Value_valueAtResetSTM32H7A3B3B0 = 0x0;
						constexpr Reg ValueMask_valueAtResetSTM32H7A3B3B0 = 0x00000000;
						constexpr Reg Value_disable = 0x0;
						constexpr Reg ValueMask_disable = 0x00000000;
						constexpr Reg Value_disabled = 0x0;
						constexpr Reg ValueMask_disabled = 0x00000000;
						constexpr Reg Value_enable = 0x1;
						constexpr Reg ValueMask_enable = 0x00000001;
						constexpr Reg Value_enabled = 0x1;
						constexpr Reg ValueMask_enabled = 0x00000001;
						};
					namespace fPOSTRESET { // Field Description
						constexpr Reg Lsb = 1;
						constexpr Reg FieldMask = 0x0000001E;
						constexpr Reg Value_valueAtResetSTM32H7A3B3B0 = 0x0;
						constexpr Reg ValueMask_valueAtResetSTM32H7A3B3B0 = 0x00000000;
						constexpr Reg Value_minValue = 0x0;
						constexpr Reg ValueMask_minValue = 0x00000000;
						constexpr Reg Value_maxValue = 0xF;
						constexpr Reg ValueMask_maxValue = 0x0000001E;
						};
					namespace fPOSTINIT { // Field Description
						constexpr Reg Lsb = 5;
						constexpr Reg FieldMask = 0x000001E0;
						constexpr Reg Value_valueAtResetSTM32H7A3B3B0 = 0x0;
						constexpr Reg ValueMask_valueAtResetSTM32H7A3B3B0 = 0x00000000;
						constexpr Reg Value_minValue = 0x0;
						constexpr Reg ValueMask_minValue = 0x00000000;
						constexpr Reg Value_maxValue = 0xF;
						constexpr Reg ValueMask_maxValue = 0x000001E0;
						};
					namespace fCYCTAP { // Field Description
						constexpr Reg Lsb = 9;
						constexpr Reg FieldMask = 0x00000200;
						constexpr Reg Value_valueAtResetSTM32H7A3B3B0 = 0x0;
						constexpr Reg ValueMask_valueAtResetSTM32H7A3B3B0 = 0x00000000;
						constexpr Reg Value_tapAtCYCCNT6 = 0x0;
						constexpr Reg ValueMask_tapAtCYCCNT6 = 0x00000000;
						constexpr Reg Value_tapAtCYCCNT10 = 0x1;
						constexpr Reg ValueMask_tapAtCYCCNT10 = 0x00000200;
						};
					namespace fSYNCTAP { // Field Description
						constexpr Reg Lsb = 10;
						constexpr Reg FieldMask = 0x00000C00;
						constexpr Reg Value_valueAtResetSTM32H7A3B3B0 = 0x0;
						constexpr Reg ValueMask_valueAtResetSTM32H7A3B3B0 = 0x00000000;
						constexpr Reg Value_disable = 0x0;
						constexpr Reg ValueMask_disable = 0x00000000;
						constexpr Reg Value_disabled = 0x0;
						constexpr Reg ValueMask_disabled = 0x00000000;
						constexpr Reg Value_tapAtCYCCNT24 = 0x1;
						constexpr Reg ValueMask_tapAtCYCCNT24 = 0x00000400;
						constexpr Reg Value_tapAtCYCCNT26 = 0x2;
						constexpr Reg ValueMask_tapAtCYCCNT26 = 0x00000800;
						constexpr Reg Value_tapAtCYCCNT28 = 0x3;
						constexpr Reg ValueMask_tapAtCYCCNT28 = 0x00000C00;
						};
					namespace fPCSAMPLENA { // Field Description
						constexpr Reg Lsb = 12;
						constexpr Reg FieldMask = 0x00001000;
						constexpr Reg Value_valueAtResetSTM32H7A3B3B0 = 0x0;
						constexpr Reg ValueMask_valueAtResetSTM32H7A3B3B0 = 0x00000000;
						constexpr Reg Value_disable = 0x0;
						constexpr Reg ValueMask_disable = 0x00000000;
						constexpr Reg Value_disabled = 0x0;
						constexpr Reg ValueMask_disabled = 0x00000000;
						constexpr Reg Value_enable = 0x1;
						constexpr Reg ValueMask_enable = 0x00001000;
						constexpr Reg Value_enabled = 0x1;
						constexpr Reg ValueMask_enabled = 0x00001000;
						};
					namespace fEXCTRCENA { // Field Description
						constexpr Reg Lsb = 16;
						constexpr Reg FieldMask = 0x00010000;
						constexpr Reg Value_valueAtResetSTM32H7A3B3B0 = 0x0;
						constexpr Reg ValueMask_valueAtResetSTM32H7A3B3B0 = 0x00000000;
						constexpr Reg Value_disable = 0x0;
						constexpr Reg ValueMask_disable = 0x00000000;
						constexpr Reg Value_disabled = 0x0;
						constexpr Reg ValueMask_disabled = 0x00000000;
						constexpr Reg Value_enable = 0x1;
						constexpr Reg ValueMask_enable = 0x00010000;
						constexpr Reg Value_enabled = 0x1;
						constexpr Reg ValueMask_enabled = 0x00010000;
						};
					namespace fCPIEVTENA { // Field Description
						constexpr Reg Lsb = 17;
						constexpr Reg FieldMask = 0x00020000;
						constexpr Reg Value_valueAtResetSTM32H7A3B3B0 = 0x0;
						constexpr Reg ValueMask_valueAtResetSTM32H7A3B3B0 = 0x00000000;
						constexpr Reg Value_disable = 0x0;
						constexpr Reg ValueMask_disable = 0x00000000;
						constexpr Reg Value_disabled = 0x0;
						constexpr Reg ValueMask_disabled = 0x00000000;
						constexpr Reg Value_enable = 0x1;
						constexpr Reg ValueMask_enable = 0x00020000;
						constexpr Reg Value_enabled = 0x1;
						constexpr Reg ValueMask_enabled = 0x00020000;
						};
					namespace fEXCEVTENA { // Field Description
						constexpr Reg Lsb = 18;
						constexpr Reg FieldMask = 0x00040000;
						constexpr Reg Value_valueAtResetSTM32H7A3B3B0 = 0x0;
						constexpr Reg ValueMask_valueAtResetSTM32H7A3B3B0 = 0x00000000;
						constexpr Reg Value_disable = 0x0;
						constexpr Reg ValueMask_disable = 0x00000000;
						constexpr Reg Value_disabled = 0x0;
						constexpr Reg ValueMask_disabled = 0x00000000;
						constexpr Reg Value_enable = 0x1;
						constexpr Reg ValueMask_enable = 0x00040000;
						constexpr Reg Value_enabled = 0x1;
						constexpr Reg ValueMask_enabled = 0x00040000;
						};
					namespace fSLEEPEVTENA { // Field Description
						constexpr Reg Lsb = 19;
						constexpr Reg FieldMask = 0x00080000;
						constexpr Reg Value_valueAtResetSTM32H7A3B3B0 = 0x0;
						constexpr Reg ValueMask_valueAtResetSTM32H7A3B3B0 = 0x00000000;
						constexpr Reg Value_disable = 0x0;
						constexpr Reg ValueMask_disable = 0x00000000;
						constexpr Reg Value_disabled = 0x0;
						constexpr Reg ValueMask_disabled = 0x00000000;
						constexpr Reg Value_enable = 0x1;
						constexpr Reg ValueMask_enable = 0x00080000;
						constexpr Reg Value_enabled = 0x1;
						constexpr Reg ValueMask_enabled = 0x00080000;
						};
					namespace fLSUEVTENA { // Field Description
						constexpr Reg Lsb = 20;
						constexpr Reg FieldMask = 0x00100000;
						constexpr Reg Value_valueAtResetSTM32H7A3B3B0 = 0x0;
						constexpr Reg ValueMask_valueAtResetSTM32H7A3B3B0 = 0x00000000;
						constexpr Reg Value_disable = 0x0;
						constexpr Reg ValueMask_disable = 0x00000000;
						constexpr Reg Value_disabled = 0x0;
						constexpr Reg ValueMask_disabled = 0x00000000;
						constexpr Reg Value_enable = 0x1;
						constexpr Reg ValueMask_enable = 0x00100000;
						constexpr Reg Value_enabled = 0x1;
						constexpr Reg ValueMask_enabled = 0x00100000;
						};
					namespace fFOLDEVTENA { // Field Description
						constexpr Reg Lsb = 21;
						constexpr Reg FieldMask = 0x00200000;
						constexpr Reg Value_valueAtResetSTM32H7A3B3B0 = 0x0;
						constexpr Reg ValueMask_valueAtResetSTM32H7A3B3B0 = 0x00000000;
						constexpr Reg Value_disable = 0x0;
						constexpr Reg ValueMask_disable = 0x00000000;
						constexpr Reg Value_disabled = 0x0;
						constexpr Reg ValueMask_disabled = 0x00000000;
						constexpr Reg Value_enable = 0x1;
						constexpr Reg ValueMask_enable = 0x00200000;
						constexpr Reg Value_enabled = 0x1;
						constexpr Reg ValueMask_enabled = 0x00200000;
						};
					namespace fCYCEVTENA { // Field Description
						constexpr Reg Lsb = 22;
						constexpr Reg FieldMask = 0x00400000;
						constexpr Reg Value_valueAtResetSTM32H7A3B3B0 = 0x0;
						constexpr Reg ValueMask_valueAtResetSTM32H7A3B3B0 = 0x00000000;
						constexpr Reg Value_disable = 0x0;
						constexpr Reg ValueMask_disable = 0x00000000;
						constexpr Reg Value_disabled = 0x0;
						constexpr Reg ValueMask_disabled = 0x00000000;
						constexpr Reg Value_enable = 0x1;
						constexpr Reg ValueMask_enable = 0x00400000;
						constexpr Reg Value_enabled = 0x1;
						constexpr Reg ValueMask_enabled = 0x00400000;
						};
					namespace fNOPRFCNT { // Field Description
						constexpr Reg Lsb = 24;
						constexpr Reg FieldMask = 0x01000000;
						constexpr Reg Value_valueAtResetSTM32H7A3B3B0 = 0x0;
						constexpr Reg ValueMask_valueAtResetSTM32H7A3B3B0 = 0x00000000;
						constexpr Reg Value_supported = 0x0;
						constexpr Reg ValueMask_supported = 0x00000000;
						constexpr Reg Value_notSupported = 0x1;
						constexpr Reg ValueMask_notSupported = 0x01000000;
						};
					namespace fNOCYCCNT { // Field Description
						constexpr Reg Lsb = 25;
						constexpr Reg FieldMask = 0x02000000;
						constexpr Reg Value_valueAtResetSTM32H7A3B3B0 = 0x0;
						constexpr Reg ValueMask_valueAtResetSTM32H7A3B3B0 = 0x00000000;
						constexpr Reg Value_supported = 0x0;
						constexpr Reg ValueMask_supported = 0x00000000;
						constexpr Reg Value_notSupported = 0x1;
						constexpr Reg ValueMask_notSupported = 0x02000000;
						};
					namespace fNOEXTTRIG { // Field Description
						constexpr Reg Lsb = 26;
						constexpr Reg FieldMask = 0x04000000;
						constexpr Reg Value_valueAtResetSTM32H7A3B3B0 = 0x0;
						constexpr Reg ValueMask_valueAtResetSTM32H7A3B3B0 = 0x00000000;
						constexpr Reg Value_supported = 0x0;
						constexpr Reg ValueMask_supported = 0x00000000;
						constexpr Reg Value_notSupported = 0x1;
						constexpr Reg ValueMask_notSupported = 0x04000000;
						};
					namespace fNOTRCPKT { // Field Description
						constexpr Reg Lsb = 27;
						constexpr Reg FieldMask = 0x08000000;
						constexpr Reg Value_valueAtResetSTM32H7A3B3B0 = 0x0;
						constexpr Reg ValueMask_valueAtResetSTM32H7A3B3B0 = 0x00000000;
						constexpr Reg Value_supported = 0x0;
						constexpr Reg ValueMask_supported = 0x00000000;
						constexpr Reg Value_notSupported = 0x1;
						constexpr Reg ValueMask_notSupported = 0x08000000;
						};
					namespace fNUMCOMP { // Field Description
						constexpr Reg Lsb = 28;
						constexpr Reg FieldMask = 0xF0000000;
						constexpr Reg Value_valueAtResetSTM32H7A3B3B0 = 0x4;
						constexpr Reg ValueMask_valueAtResetSTM32H7A3B3B0 = 0x40000000;
						constexpr Reg Value_minValue = 0x0;
						constexpr Reg ValueMask_minValue = 0x00000000;
						constexpr Reg Value_maxValue = 0xF;
						constexpr Reg ValueMask_maxValue = 0xF0000000;
						};
					};
				namespace rCYCCNT { // Register description
					typedef uint32_t	Reg;
					constexpr Reg	valueAtResetSTM32H7A3B3B0 = 0x00000000;
					namespace fCYCCNT { // Field Description
						constexpr Reg Lsb = 0;
						constexpr Reg FieldMask = 0xFFFFFFFF;
						constexpr Reg Value_valueAtResetSTM32H7A3B3B0 = 0x0;
						constexpr Reg ValueMask_valueAtResetSTM32H7A3B3B0 = 0x00000000;
						};
					};
				namespace rCPICNT { // Register description
					typedef uint32_t	Reg;
					constexpr Reg	valueAtResetSTM32H7A3B3B0 = 0x00000000;
					namespace fCPICNT { // Field Description
						constexpr Reg Lsb = 0;
						constexpr Reg FieldMask = 0x000000FF;
						constexpr Reg Value_valueAtResetSTM32H7A3B3B0 = 0x0;
						constexpr Reg ValueMask_valueAtResetSTM32H7A3B3B0 = 0x00000000;
						};
					};
				namespace rEXCCNT { // Register description
					typedef uint32_t	Reg;
					constexpr Reg	valueAtResetSTM32H7A3B3B0 = 0x00000000;
					namespace fEXCCNT { // Field Description
						constexpr Reg Lsb = 0;
						constexpr Reg FieldMask = 0x000000FF;
						constexpr Reg Value_valueAtResetSTM32H7A3B3B0 = 0x0;
						constexpr Reg ValueMask_valueAtResetSTM32H7A3B3B0 = 0x00000000;
						};
					};
				namespace rSLEEPCNT { // Register description
					typedef uint32_t	Reg;
					constexpr Reg	valueAtResetSTM32H7A3B3B0 = 0x00000000;
					namespace fSLEEPCNT { // Field Description
						constexpr Reg Lsb = 0;
						constexpr Reg FieldMask = 0x000000FF;
						constexpr Reg Value_valueAtResetSTM32H7A3B3B0 = 0x0;
						constexpr Reg ValueMask_valueAtResetSTM32H7A3B3B0 = 0x00000000;
						};
					};
				namespace rLSUCNT { // Register description
					typedef uint32_t	Reg;
					constexpr Reg	valueAtResetSTM32H7A3B3B0 = 0x00000000;
					namespace fLSUCNT { // Field Description
						constexpr Reg Lsb = 0;
						constexpr Reg FieldMask = 0x000000FF;
						constexpr Reg Value_valueAtResetSTM32H7A3B3B0 = 0x0;
						constexpr Reg ValueMask_valueAtResetSTM32H7A3B3B0 = 0x00000000;
						};
					};
				namespace rFOLDCNT { // Register description
					typedef uint32_t	Reg;
					constexpr Reg	valueAtResetSTM32H7A3B3B0 = 0x00000000;
					namespace fFOLDCNT { // Field Description
						constexpr Reg Lsb = 0;
						constexpr Reg FieldMask = 0x000000FF;
						constexpr Reg Value_valueAtResetSTM32H7A3B3B0 = 0x0;
						constexpr Reg ValueMask_valueAtResetSTM32H7A3B3B0 = 0x00000000;
						};
					};
				namespace rPCSR { // Register description
					typedef uint32_t	Reg;
					constexpr Reg	valueAtResetSTM32H7A3B3B0 = 0x00000000;
					namespace fEIASAMPLE { // Field Description
						constexpr Reg Lsb = 0;
						constexpr Reg FieldMask = 0xFFFFFFFF;
						constexpr Reg Value_valueAtResetSTM32H7A3B3B0 = 0x0;
						constexpr Reg ValueMask_valueAtResetSTM32H7A3B3B0 = 0x00000000;
						};
					};
				namespace rCOMP { // Register description
					typedef uint32_t	Reg;
					constexpr Reg	valueAtResetSTM32H7A3B3B0 = 0x00000000;
					namespace fCOMP { // Field Description
						constexpr Reg Lsb = 0;
						constexpr Reg FieldMask = 0xFFFFFFFF;
						constexpr Reg Value_valueAtResetSTM32H7A3B3B0 = 0x0;
						constexpr Reg ValueMask_valueAtResetSTM32H7A3B3B0 = 0x00000000;
						};
					};
				namespace rMASK { // Register description
					typedef uint32_t	Reg;
					constexpr Reg	valueAtResetSTM32H7A3B3B0 = 0x00000000;
					namespace fMASK { // Field Description
						constexpr Reg Lsb = 0;
						constexpr Reg FieldMask = 0x0000001F;
						constexpr Reg Value_valueAtResetSTM32H7A3B3B0 = 0x0;
						constexpr Reg ValueMask_valueAtResetSTM32H7A3B3B0 = 0x00000000;
						};
					};
				namespace rFUNCTION { // Register description
					typedef uint32_t	Reg;
					constexpr Reg	valueAtResetSTM32H7A3B3B0 = 0x00000000;
					namespace fFUNCTION { // Field Description
						constexpr Reg Lsb = 0;
						constexpr Reg FieldMask = 0x0000000F;
						constexpr Reg Value_valueAtResetSTM32H7A3B3B0 = 0x0;
						constexpr Reg ValueMask_valueAtResetSTM32H7A3B3B0 = 0x00000000;
						};
					namespace fEMITRANGE { // Field Description
						constexpr Reg Lsb = 5;
						constexpr Reg FieldMask = 0x00000020;
						constexpr Reg Value_valueAtResetSTM32H7A3B3B0 = 0x0;
						constexpr Reg ValueMask_valueAtResetSTM32H7A3B3B0 = 0x00000000;
						constexpr Reg Value_disable = 0x0;
						constexpr Reg ValueMask_disable = 0x00000000;
						constexpr Reg Value_disabled = 0x0;
						constexpr Reg ValueMask_disabled = 0x00000000;
						constexpr Reg Value_enable = 0x1;
						constexpr Reg ValueMask_enable = 0x00000020;
						constexpr Reg Value_enabled = 0x1;
						constexpr Reg ValueMask_enabled = 0x00000020;
						};
					namespace fCYCMATCH { // Field Description
						constexpr Reg Lsb = 7;
						constexpr Reg FieldMask = 0x00000080;
						constexpr Reg Value_valueAtResetSTM32H7A3B3B0 = 0x0;
						constexpr Reg ValueMask_valueAtResetSTM32H7A3B3B0 = 0x00000000;
						constexpr Reg Value_disable = 0x0;
						constexpr Reg ValueMask_disable = 0x00000000;
						constexpr Reg Value_disabled = 0x0;
						constexpr Reg ValueMask_disabled = 0x00000000;
						constexpr Reg Value_enable = 0x1;
						constexpr Reg ValueMask_enable = 0x00000080;
						constexpr Reg Value_enabled = 0x1;
						constexpr Reg ValueMask_enabled = 0x00000080;
						};
					namespace fDATAVMATCH { // Field Description
						constexpr Reg Lsb = 8;
						constexpr Reg FieldMask = 0x00000100;
						constexpr Reg Value_valueAtResetSTM32H7A3B3B0 = 0x0;
						constexpr Reg ValueMask_valueAtResetSTM32H7A3B3B0 = 0x00000000;
						constexpr Reg Value_disable = 0x0;
						constexpr Reg ValueMask_disable = 0x00000000;
						constexpr Reg Value_disabled = 0x0;
						constexpr Reg ValueMask_disabled = 0x00000000;
						constexpr Reg Value_enable = 0x1;
						constexpr Reg ValueMask_enable = 0x00000100;
						constexpr Reg Value_enabled = 0x1;
						constexpr Reg ValueMask_enabled = 0x00000100;
						};
					namespace fLINK1ENA { // Field Description
						constexpr Reg Lsb = 9;
						constexpr Reg FieldMask = 0x00000200;
						constexpr Reg Value_notSupported = 0x0;
						constexpr Reg ValueMask_notSupported = 0x00000000;
						constexpr Reg Value_supported = 0x1;
						constexpr Reg ValueMask_supported = 0x00000200;
						};
					namespace fDATAVSIZE { // Field Description
						constexpr Reg Lsb = 10;
						constexpr Reg FieldMask = 0x00000C00;
						constexpr Reg Value_valueAtResetSTM32H7A3B3B0 = 0x0;
						constexpr Reg ValueMask_valueAtResetSTM32H7A3B3B0 = 0x00000000;
						constexpr Reg Value_byte = 0x0;
						constexpr Reg ValueMask_byte = 0x00000000;
						constexpr Reg Value_halfWord = 0x1;
						constexpr Reg ValueMask_halfWord = 0x00000400;
						constexpr Reg Value_word = 0x2;
						constexpr Reg ValueMask_word = 0x00000800;
						};
					namespace fDATAVADDR0 { // Field Description
						constexpr Reg Lsb = 12;
						constexpr Reg FieldMask = 0x0000F000;
						constexpr Reg Value_valueAtResetSTM32H7A3B3B0 = 0x0;
						constexpr Reg ValueMask_valueAtResetSTM32H7A3B3B0 = 0x00000000;
						constexpr Reg Value_minValue = 0x0;
						constexpr Reg ValueMask_minValue = 0x00000000;
						constexpr Reg Value_maxValue = 0xF;
						constexpr Reg ValueMask_maxValue = 0x0000F000;
						};
					namespace fDATAVADDR1 { // Field Description
						constexpr Reg Lsb = 16;
						constexpr Reg FieldMask = 0x000F0000;
						constexpr Reg Value_valueAtResetSTM32H7A3B3B0 = 0x0;
						constexpr Reg ValueMask_valueAtResetSTM32H7A3B3B0 = 0x00000000;
						constexpr Reg Value_minValue = 0x0;
						constexpr Reg ValueMask_minValue = 0x00000000;
						constexpr Reg Value_maxValue = 0xF;
						constexpr Reg ValueMask_maxValue = 0x000F0000;
						};
					namespace fMATCHED { // Field Description
						constexpr Reg Lsb = 24;
						constexpr Reg FieldMask = 0x01000000;
						constexpr Reg Value_valueAtResetSTM32H7A3B3B0 = 0x0;
						constexpr Reg ValueMask_valueAtResetSTM32H7A3B3B0 = 0x00000000;
						constexpr Reg Value_noMatch = 0x0;
						constexpr Reg ValueMask_noMatch = 0x00000000;
						constexpr Reg Value_matched = 0x0;
						constexpr Reg ValueMask_matched = 0x00000000;
						};
					};
				namespace rLAR { // Register description
					typedef uint32_t	Reg;
					constexpr Reg	valueAtReset = 0x00000000;
					constexpr Reg	key = 0xC5ACCE55;
					};
				namespace rLSR { // Register description
					typedef uint32_t	Reg;
					constexpr Reg	valueAtReset = 0x00000000;
					namespace fLockImplemented { // Field Description
						constexpr Reg Lsb = 0;
						constexpr Reg FieldMask = 0x00000001;
						constexpr Reg Value_ignoresLockRegisters = 0x0;
						constexpr Reg ValueMask_ignoresLockRegisters = 0x00000000;
						constexpr Reg Value_requiresUnlock = 0x1;
						constexpr Reg ValueMask_requiresUnlock = 0x00000001;
						};
					namespace fLocked { // Field Description
						constexpr Reg Lsb = 1;
						constexpr Reg FieldMask = 0x00000002;
						constexpr Reg Value_writesPermitted = 0x0;
						constexpr Reg ValueMask_writesPermitted = 0x00000000;
						constexpr Reg Value_writesIgnored = 0x1;
						constexpr Reg ValueMask_writesIgnored = 0x00000002;
						};
					namespace f32BitAccess { // Field Description
						constexpr Reg Lsb = 2;
						constexpr Reg FieldMask = 0x00000004;
						constexpr Reg Value_lockAccess32bitOnly = 0x0;
						constexpr Reg ValueMask_lockAccess32bitOnly = 0x00000000;
						};
					};
				namespace rPID4 { // Register description
					typedef uint32_t	Reg;
					};
				namespace rPID5 { // Register description
					typedef uint32_t	Reg;
					};
				namespace rPID6 { // Register description
					typedef uint32_t	Reg;
					};
				namespace rPID7 { // Register description
					typedef uint32_t	Reg;
					};
				namespace rPID0 { // Register description
					typedef uint32_t	Reg;
					};
				namespace rPID1 { // Register description
					typedef uint32_t	Reg;
					};
				namespace rPID2 { // Register description
					typedef uint32_t	Reg;
					};
				namespace rPID3 { // Register description
					typedef uint32_t	Reg;
					};
				namespace rCID0 { // Register description
					typedef uint32_t	Reg;
					};
				namespace rCID1 { // Register description
					typedef uint32_t	Reg;
					};
				namespace rCID2 { // Register description
					typedef uint32_t	Reg;
					};
				namespace rCID3 { // Register description
					typedef uint32_t	Reg;
					};
				}
			}
		}
	}
#endif
