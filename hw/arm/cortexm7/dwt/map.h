/*
   Copyright (C) 2024 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/
#ifndef _oscl_hw_arm_cortexm7_dwt_maph_
#define _oscl_hw_arm_cortexm7_dwt_maph_
#include "reg.h"

/** */
namespace Oscl {
/** */
namespace Arm {
/** */
namespace CortexM7 {
/** */
namespace pDWT {

/** */
struct Comp {
	/** offset 0 */
	volatile rCOMP::Reg	comp;

	/** offset 4 */
	volatile rMASK::Reg	mask;

	/** offset 8 */
	volatile rFUNCTION::Reg	function;
	};

/** 0xE0001000 */
struct Map {
	/** 0xE0001000, offset 0x00000000 */
	volatile rCTRL::Reg	ctrl;

	/** 0xE0001004, offset 0x00000004 */
	volatile rCYCCNT::Reg	cyccnt;

	/** 0xE0001008, offset 0x00000008 */
	volatile rCPICNT::Reg	cpicnt;

	/** 0xE000100C, offset 0x0000000C */
	volatile rEXCCNT::Reg	exccnt;

	/** 0xE0001010, offset 0x00000010 */
	volatile rSLEEPCNT::Reg	sleepcnt;

	/** 0xE0001014, offset 0x00000014 */
	volatile rLSUCNT::Reg	lsucnt;

	/** 0xE0001018, offset 0x00000018 */
	volatile rFOLDCNT::Reg	foldcnt;

	/** 0xE000101C, offset 0x0000001C */
	volatile rPCSR::Reg	pcsr;

	/** 0xE0001020, offset 0x00000020 */
	Comp	comp[4];

	/** 0xE0001050, offset 0x00000050 */
	uint8_t	reserved0xE0001050[0xE0001FB0-0xE0001050];

	/** 0xE0001FB0, offset 0x00000FB0 */
	volatile rLAR::Reg	lar;

	/** 0xE0001FB4, offset 0x00000FB4 */
	volatile rLAR::Reg	lsr;

	/** 0xE0001FB8, offset 0x00000FB8 */
	uint8_t	reservedE0001FB8[0xE0001FD0-0xE0001FB8];

	/** 0xE0001FD0, offset 0x00000FD0 */
	volatile rPID4::Reg	pid4;

	/** 0xE0001FD4, offset 0x00000FD4 */
	volatile rPID5::Reg	pid5;

	/** 0xE0001FD8, offset 0x00000FD8 */
	volatile rPID6::Reg	pid6;

	/** 0xE0001FDC, offset 0x00000FDC */
	volatile rPID7::Reg	pid7;

	/** 0xE0001FE0, offset 0x00000FE0 */
	volatile rPID0::Reg	pid0;

	/** 0xE0001FE4, offset 0x00000FE4 */
	volatile rPID1::Reg	pid1;

	/** 0xE0001FE8, offset 0x00000FE8 */
	volatile rPID2::Reg	pid2;

	/** 0xE0001FEC, offset 0x00000FEC */
	volatile rPID3::Reg	pid3;

	/** 0xE0001FF0, offset 0x00000FF0 */
	volatile rCID0::Reg	cid0;

	/** 0xE0001FF4, offset 0x00000FF4 */
	volatile rCID1::Reg	cid1;

	/** 0xE0001FF8, offset 0x00000FF8 */
	volatile rCID2::Reg	cid2;

	/** 0xE0001FFC, offset 0x00000FFC */
	volatile rCID3::Reg	cid3;

	};

}
}
}
}

#endif
