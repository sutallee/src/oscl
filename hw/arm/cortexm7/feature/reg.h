/*
   Copyright (C) 2024 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_hw_arm_cortexm7_feature_regh_
#define _oscl_hw_arm_cortexm7_feature_regh_
#include <stdint.h>
namespace Oscl { // Namespace description
	namespace Arm { // Namespace description
		namespace CortexM7 { // Namespace description
			namespace pFEATURE { // Namespace description
				namespace rCLIDR { // Register description
					typedef uint32_t	Reg;
					constexpr Reg	valueAtReset = 150994947;
					namespace fCL_1 { // Field Description
						constexpr Reg Lsb = 0;
						constexpr Reg FieldMask = 0x00000007;
						namespace fD { // Field Description
							constexpr Reg Lsb = 0;
							constexpr Reg FieldMask = 0x00000001;
							constexpr Reg Value_implemented = 0x1;
							constexpr Reg ValueMask_implemented = 0x00000001;
							};
						namespace fI { // Field Description
							constexpr Reg Lsb = 1;
							constexpr Reg FieldMask = 0x00000002;
							constexpr Reg Value_implemented = 0x1;
							constexpr Reg ValueMask_implemented = 0x00000002;
							};
						namespace fRAZ { // Field Description
							constexpr Reg Lsb = 2;
							constexpr Reg FieldMask = 0x00000004;
							constexpr Reg Value_implemented = 0x1;
							constexpr Reg ValueMask_implemented = 0x00000004;
							};
						};
					namespace fCL_2 { // Field Description
						constexpr Reg Lsb = 3;
						constexpr Reg FieldMask = 0x00000038;
						constexpr Reg Value_none = 0x0;
						constexpr Reg ValueMask_none = 0x00000000;
						};
					namespace fCL_3 { // Field Description
						constexpr Reg Lsb = 6;
						constexpr Reg FieldMask = 0x000001C0;
						constexpr Reg Value_none = 0x0;
						constexpr Reg ValueMask_none = 0x00000000;
						};
					namespace fCL_4 { // Field Description
						constexpr Reg Lsb = 9;
						constexpr Reg FieldMask = 0x00000E00;
						constexpr Reg Value_none = 0x0;
						constexpr Reg ValueMask_none = 0x00000000;
						};
					namespace fCL_5 { // Field Description
						constexpr Reg Lsb = 12;
						constexpr Reg FieldMask = 0x00007000;
						constexpr Reg Value_none = 0x0;
						constexpr Reg ValueMask_none = 0x00000000;
						};
					namespace fCL_6 { // Field Description
						constexpr Reg Lsb = 15;
						constexpr Reg FieldMask = 0x00038000;
						constexpr Reg Value_none = 0x0;
						constexpr Reg ValueMask_none = 0x00000000;
						};
					namespace fCL_7 { // Field Description
						constexpr Reg Lsb = 18;
						constexpr Reg FieldMask = 0x001C0000;
						constexpr Reg Value_none = 0x0;
						constexpr Reg ValueMask_none = 0x00000000;
						};
					namespace fLoUIS { // Field Description
						constexpr Reg Lsb = 21;
						constexpr Reg FieldMask = 0x00E00000;
						constexpr Reg Value_none = 0x0;
						constexpr Reg ValueMask_none = 0x00000000;
						};
					namespace fLoC { // Field Description
						constexpr Reg Lsb = 24;
						constexpr Reg FieldMask = 0x07000000;
						constexpr Reg Value_level1 = 0x0;
						constexpr Reg ValueMask_level1 = 0x00000000;
						constexpr Reg Value_neitherCache = 0x0;
						constexpr Reg ValueMask_neitherCache = 0x00000000;
						constexpr Reg Value_level2 = 0x1;
						constexpr Reg ValueMask_level2 = 0x01000000;
						constexpr Reg Value_eitherCache = 0x1;
						constexpr Reg ValueMask_eitherCache = 0x01000000;
						};
					namespace fLoU { // Field Description
						constexpr Reg Lsb = 24;
						constexpr Reg FieldMask = 0x07000000;
						constexpr Reg Value_level1 = 0x0;
						constexpr Reg ValueMask_level1 = 0x00000000;
						constexpr Reg Value_neitherCache = 0x0;
						constexpr Reg ValueMask_neitherCache = 0x00000000;
						constexpr Reg Value_level2 = 0x1;
						constexpr Reg ValueMask_level2 = 0x01000000;
						constexpr Reg Value_eitherCache = 0x1;
						constexpr Reg ValueMask_eitherCache = 0x01000000;
						};
					};
				namespace rCTR { // Register description
					typedef uint32_t	Reg;
					constexpr Reg	valueAtReset = -2096906237;
					namespace fIminLine { // Field Description
						constexpr Reg Lsb = 0;
						constexpr Reg FieldMask = 0x0000000F;
						constexpr Reg Value_valueAtReset = 0x3;
						constexpr Reg ValueMask_valueAtReset = 0x00000003;
						};
					namespace fRAZ { // Field Description
						constexpr Reg Lsb = 5;
						constexpr Reg FieldMask = 0x00007FE0;
						constexpr Reg Value_valueAtReset = 0x0;
						constexpr Reg ValueMask_valueAtReset = 0x00000000;
						};
					namespace fRAO { // Field Description
						constexpr Reg Lsb = 14;
						constexpr Reg FieldMask = 0x0000C000;
						constexpr Reg Value_valueAtReset = 0x3;
						constexpr Reg ValueMask_valueAtReset = 0x0000C000;
						};
					namespace fDMinLine { // Field Description
						constexpr Reg Lsb = 16;
						constexpr Reg FieldMask = 0x000F0000;
						constexpr Reg Value_valueAtReset = 0x3;
						constexpr Reg ValueMask_valueAtReset = 0x00030000;
						};
					namespace fERG { // Field Description
						constexpr Reg Lsb = 16;
						constexpr Reg FieldMask = 0x000F0000;
						constexpr Reg Value_valueAtReset = 0x0;
						constexpr Reg ValueMask_valueAtReset = 0x00000000;
						};
					namespace fCWG { // Field Description
						constexpr Reg Lsb = 24;
						constexpr Reg FieldMask = 0x0F000000;
						constexpr Reg Value_valueAtReset = 0x3;
						constexpr Reg ValueMask_valueAtReset = 0x03000000;
						};
					namespace fFormat { // Field Description
						constexpr Reg Lsb = 29;
						constexpr Reg FieldMask = 0xE0000000;
						constexpr Reg Value_valueAtReset = 0x4;
						constexpr Reg ValueMask_valueAtReset = 0x80000000;
						};
					};
				namespace rCCSIDR { // Register description
					typedef uint32_t	Reg;
					namespace fLineSize { // Field Description
						constexpr Reg Lsb = 0;
						constexpr Reg FieldMask = 0x00000007;
						constexpr Reg Value_minValue = 0x0;
						constexpr Reg ValueMask_minValue = 0x00000000;
						constexpr Reg Value_maxValue = 0x7;
						constexpr Reg ValueMask_maxValue = 0x00000007;
						};
					namespace fAssociativity { // Field Description
						constexpr Reg Lsb = 3;
						constexpr Reg FieldMask = 0x00001FF8;
						constexpr Reg Value_minValue = 0x0;
						constexpr Reg ValueMask_minValue = 0x00000000;
						constexpr Reg Value_maxValue = 0x3FF;
						constexpr Reg ValueMask_maxValue = 0x00001FF8;
						};
					namespace fNumSets { // Field Description
						constexpr Reg Lsb = 13;
						constexpr Reg FieldMask = 0x0FFFE000;
						constexpr Reg Value_minValue = 0x0;
						constexpr Reg ValueMask_minValue = 0x00000000;
						constexpr Reg Value_maxValue = 0x7FFF;
						constexpr Reg ValueMask_maxValue = 0x0FFFE000;
						};
					namespace fWA { // Field Description
						constexpr Reg Lsb = 28;
						constexpr Reg FieldMask = 0x10000000;
						constexpr Reg Value_writeAllocationSupported = 0x1;
						constexpr Reg ValueMask_writeAllocationSupported = 0x10000000;
						};
					namespace fRA { // Field Description
						constexpr Reg Lsb = 29;
						constexpr Reg FieldMask = 0x20000000;
						constexpr Reg Value_readAllocationSupported = 0x1;
						constexpr Reg ValueMask_readAllocationSupported = 0x20000000;
						};
					namespace fWB { // Field Description
						constexpr Reg Lsb = 30;
						constexpr Reg FieldMask = 0x40000000;
						constexpr Reg Value_writeBackSupported = 0x1;
						constexpr Reg ValueMask_writeBackSupported = 0x40000000;
						};
					namespace fWT { // Field Description
						constexpr Reg Lsb = 30;
						constexpr Reg FieldMask = 0x40000000;
						constexpr Reg Value_writeThroughSupported = 0x1;
						constexpr Reg ValueMask_writeThroughSupported = 0x40000000;
						};
					};
				namespace rCSSELR { // Register description
					typedef uint32_t	Reg;
					namespace fInD { // Field Description
						constexpr Reg Lsb = 0;
						constexpr Reg FieldMask = 0x00000001;
						constexpr Reg Value_dataCache = 0x0;
						constexpr Reg ValueMask_dataCache = 0x00000000;
						constexpr Reg Value_instructionCache = 0x1;
						constexpr Reg ValueMask_instructionCache = 0x00000001;
						};
					namespace fLevel { // Field Description
						constexpr Reg Lsb = 0;
						constexpr Reg FieldMask = 0x00000001;
						constexpr Reg Value_level1Cache = 0x0;
						constexpr Reg ValueMask_level1Cache = 0x00000000;
						};
					};
				}
			}
		}
	}
#endif
