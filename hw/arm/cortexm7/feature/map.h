/*
   Copyright (C) 2024 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/
#ifndef _oscl_hw_arm_cortexm7_feature_maph_
#define _oscl_hw_arm_cortexm7_feature_maph_
#include "reg.h"

/** */
namespace Oscl {
/** */
namespace Arm {
/** */
namespace CortexM7 {
/** */
namespace Feature {

/** 0xE000ED78 */
struct Map {
	/** 0xE000ED78, offset 0x00000000 */
	volatile Oscl::Arm::CortexM7::pFEATURE::rCLIDR::Reg	clidr;

	/** 0xE000ED7C, offset 0x00000004 */
	volatile Oscl::Arm::CortexM7::pFEATURE::rCTR::Reg	ctr;

	/** 0xE000ED80, offset 0x0000008 */
	volatile Oscl::Arm::CortexM7::pFEATURE::rCCSIDR::Reg	ccsidr;

	/** 0xE000ED84, offset 0x0000000C */
	volatile Oscl::Arm::CortexM7::pFEATURE::rCSSELR::Reg	csselr;
	};

}
}
}
}

#endif
