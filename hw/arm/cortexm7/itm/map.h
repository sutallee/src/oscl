/*
   Copyright (C) 2024 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/
#ifndef _oscl_hw_arm_cortexm7_itm_maph_
#define _oscl_hw_arm_cortexm7_itm_maph_
#include "reg.h"

/** */
namespace Oscl {
/** */
namespace Arm {
/** */
namespace CortexM7 {
/** */
namespace pITM {

/** 0xE0000000 */
struct Map {
	/** 0xE0000000, offset 0x00000000 */
	volatile rSTIM::Reg	stim[256];

	/** 0xE0000400, offset 0x00000400 */
	uint8_t	reservedE0000100[0xE0000E00-0xE0000400];

	/** 0xE0000E00, offset 0x00000E00 */
	volatile rTER::Reg	ter[8];

	/** 0xE0000E20, offset 0x00000E20 */
	uint8_t	reservedE0000E20[0xE0000E40-0xE0000E20];

	/** 0xE0000E40, offset 0x00000E40 */
	volatile rTPR::Reg	tpr;

	/** 0xE0000E44, offset 0x00000E44 */
	uint8_t	reservedE0000E44[0xE0000E80-0xE0000E44];

	/** 0xE0000E80, offset 0x00000E80 */
	volatile rTCR::Reg	tcr;

	/** 0xE0000E84, offset 0x00000E84 */
	uint8_t	reservedE0000E84[0xE0000FB0-0xE0000E84];

	/** 0xE0000FB0, offset 0x00000FB0 */
	volatile rLAR::Reg	lar;

	/** 0xE0000FB4, offset 0x00000FB4 */
	volatile rLSR::Reg	lsr;

	/** 0xE0000FB8, offset 0x00000FB8 */
	uint8_t	reservedE00400F4[0xE0001000-0xE0000FB8];
	};

}
}
}
}

#endif
