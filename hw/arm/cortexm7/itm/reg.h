/*
   Copyright (C) 2024 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _hw_arm_cortexm7_itm_regh_
#define _hw_arm_cortexm7_itm_regh_
#include <stdint.h>
namespace Oscl { // Namespace description
	namespace Arm { // Namespace description
		namespace CortexM7 { // Namespace description
			namespace pITM { // Namespace description
				namespace rSTIM { // Register description
					typedef uint32_t	Reg;
					namespace fSTIMULUS { // Field Description
						constexpr Reg Lsb = 0;
						constexpr Reg FieldMask = 0xFFFFFFFF;
						};
					namespace fFIFOREADY { // Field Description
						constexpr Reg Lsb = 0;
						constexpr Reg FieldMask = 0x00000001;
						constexpr Reg Value_fullOrPortDisabled = 0x0;
						constexpr Reg ValueMask_fullOrPortDisabled = 0x00000000;
						constexpr Reg Value_canAcceptAtLeastOneWord = 0x1;
						constexpr Reg ValueMask_canAcceptAtLeastOneWord = 0x00000001;
						};
					};
				namespace rTER { // Register description
					typedef uint32_t	Reg;
					constexpr Reg	valueAtReset = 0x00000000;
					constexpr Reg	disable = 0x00000000;
					constexpr Reg	disabled = 0x00000000;
					constexpr Reg	enable = 0x00000001;
					constexpr Reg	enabled = 0x00000001;
					namespace fSTIMENA0 { // Field Description
						constexpr Reg Lsb = 0;
						constexpr Reg FieldMask = 0x00000001;
						constexpr Reg Value_disable = 0x0;
						constexpr Reg ValueMask_disable = 0x00000000;
						constexpr Reg Value_disabled = 0x0;
						constexpr Reg ValueMask_disabled = 0x00000000;
						constexpr Reg Value_enable = 0x1;
						constexpr Reg ValueMask_enable = 0x00000001;
						constexpr Reg Value_enabled = 0x1;
						constexpr Reg ValueMask_enabled = 0x00000001;
						};
					namespace fSTIMENA1 { // Field Description
						constexpr Reg Lsb = 1;
						constexpr Reg FieldMask = 0x00000002;
						constexpr Reg Value_disable = 0x0;
						constexpr Reg ValueMask_disable = 0x00000000;
						constexpr Reg Value_disabled = 0x0;
						constexpr Reg ValueMask_disabled = 0x00000000;
						constexpr Reg Value_enable = 0x1;
						constexpr Reg ValueMask_enable = 0x00000002;
						constexpr Reg Value_enabled = 0x1;
						constexpr Reg ValueMask_enabled = 0x00000002;
						};
					namespace fSTIMENA2 { // Field Description
						constexpr Reg Lsb = 2;
						constexpr Reg FieldMask = 0x00000004;
						constexpr Reg Value_disable = 0x0;
						constexpr Reg ValueMask_disable = 0x00000000;
						constexpr Reg Value_disabled = 0x0;
						constexpr Reg ValueMask_disabled = 0x00000000;
						constexpr Reg Value_enable = 0x1;
						constexpr Reg ValueMask_enable = 0x00000004;
						constexpr Reg Value_enabled = 0x1;
						constexpr Reg ValueMask_enabled = 0x00000004;
						};
					namespace fSTIMENA3 { // Field Description
						constexpr Reg Lsb = 3;
						constexpr Reg FieldMask = 0x00000008;
						constexpr Reg Value_disable = 0x0;
						constexpr Reg ValueMask_disable = 0x00000000;
						constexpr Reg Value_disabled = 0x0;
						constexpr Reg ValueMask_disabled = 0x00000000;
						constexpr Reg Value_enable = 0x1;
						constexpr Reg ValueMask_enable = 0x00000008;
						constexpr Reg Value_enabled = 0x1;
						constexpr Reg ValueMask_enabled = 0x00000008;
						};
					namespace fSTIMENA4 { // Field Description
						constexpr Reg Lsb = 4;
						constexpr Reg FieldMask = 0x00000010;
						constexpr Reg Value_disable = 0x0;
						constexpr Reg ValueMask_disable = 0x00000000;
						constexpr Reg Value_disabled = 0x0;
						constexpr Reg ValueMask_disabled = 0x00000000;
						constexpr Reg Value_enable = 0x1;
						constexpr Reg ValueMask_enable = 0x00000010;
						constexpr Reg Value_enabled = 0x1;
						constexpr Reg ValueMask_enabled = 0x00000010;
						};
					namespace fSTIMENA5 { // Field Description
						constexpr Reg Lsb = 5;
						constexpr Reg FieldMask = 0x00000020;
						constexpr Reg Value_disable = 0x0;
						constexpr Reg ValueMask_disable = 0x00000000;
						constexpr Reg Value_disabled = 0x0;
						constexpr Reg ValueMask_disabled = 0x00000000;
						constexpr Reg Value_enable = 0x1;
						constexpr Reg ValueMask_enable = 0x00000020;
						constexpr Reg Value_enabled = 0x1;
						constexpr Reg ValueMask_enabled = 0x00000020;
						};
					namespace fSTIMENA6 { // Field Description
						constexpr Reg Lsb = 6;
						constexpr Reg FieldMask = 0x00000040;
						constexpr Reg Value_disable = 0x0;
						constexpr Reg ValueMask_disable = 0x00000000;
						constexpr Reg Value_disabled = 0x0;
						constexpr Reg ValueMask_disabled = 0x00000000;
						constexpr Reg Value_enable = 0x1;
						constexpr Reg ValueMask_enable = 0x00000040;
						constexpr Reg Value_enabled = 0x1;
						constexpr Reg ValueMask_enabled = 0x00000040;
						};
					namespace fSTIMENA7 { // Field Description
						constexpr Reg Lsb = 7;
						constexpr Reg FieldMask = 0x00000080;
						constexpr Reg Value_disable = 0x0;
						constexpr Reg ValueMask_disable = 0x00000000;
						constexpr Reg Value_disabled = 0x0;
						constexpr Reg ValueMask_disabled = 0x00000000;
						constexpr Reg Value_enable = 0x1;
						constexpr Reg ValueMask_enable = 0x00000080;
						constexpr Reg Value_enabled = 0x1;
						constexpr Reg ValueMask_enabled = 0x00000080;
						};
					namespace fSTIMENA8 { // Field Description
						constexpr Reg Lsb = 8;
						constexpr Reg FieldMask = 0x00000100;
						constexpr Reg Value_disable = 0x0;
						constexpr Reg ValueMask_disable = 0x00000000;
						constexpr Reg Value_disabled = 0x0;
						constexpr Reg ValueMask_disabled = 0x00000000;
						constexpr Reg Value_enable = 0x1;
						constexpr Reg ValueMask_enable = 0x00000100;
						constexpr Reg Value_enabled = 0x1;
						constexpr Reg ValueMask_enabled = 0x00000100;
						};
					namespace fSTIMENA9 { // Field Description
						constexpr Reg Lsb = 9;
						constexpr Reg FieldMask = 0x00000200;
						constexpr Reg Value_disable = 0x0;
						constexpr Reg ValueMask_disable = 0x00000000;
						constexpr Reg Value_disabled = 0x0;
						constexpr Reg ValueMask_disabled = 0x00000000;
						constexpr Reg Value_enable = 0x1;
						constexpr Reg ValueMask_enable = 0x00000200;
						constexpr Reg Value_enabled = 0x1;
						constexpr Reg ValueMask_enabled = 0x00000200;
						};
					namespace fSTIMENA10 { // Field Description
						constexpr Reg Lsb = 10;
						constexpr Reg FieldMask = 0x00000400;
						constexpr Reg Value_disable = 0x0;
						constexpr Reg ValueMask_disable = 0x00000000;
						constexpr Reg Value_disabled = 0x0;
						constexpr Reg ValueMask_disabled = 0x00000000;
						constexpr Reg Value_enable = 0x1;
						constexpr Reg ValueMask_enable = 0x00000400;
						constexpr Reg Value_enabled = 0x1;
						constexpr Reg ValueMask_enabled = 0x00000400;
						};
					namespace fSTIMENA11 { // Field Description
						constexpr Reg Lsb = 11;
						constexpr Reg FieldMask = 0x00000800;
						constexpr Reg Value_disable = 0x0;
						constexpr Reg ValueMask_disable = 0x00000000;
						constexpr Reg Value_disabled = 0x0;
						constexpr Reg ValueMask_disabled = 0x00000000;
						constexpr Reg Value_enable = 0x1;
						constexpr Reg ValueMask_enable = 0x00000800;
						constexpr Reg Value_enabled = 0x1;
						constexpr Reg ValueMask_enabled = 0x00000800;
						};
					namespace fSTIMENA12 { // Field Description
						constexpr Reg Lsb = 12;
						constexpr Reg FieldMask = 0x00001000;
						constexpr Reg Value_disable = 0x0;
						constexpr Reg ValueMask_disable = 0x00000000;
						constexpr Reg Value_disabled = 0x0;
						constexpr Reg ValueMask_disabled = 0x00000000;
						constexpr Reg Value_enable = 0x1;
						constexpr Reg ValueMask_enable = 0x00001000;
						constexpr Reg Value_enabled = 0x1;
						constexpr Reg ValueMask_enabled = 0x00001000;
						};
					namespace fSTIMENA13 { // Field Description
						constexpr Reg Lsb = 13;
						constexpr Reg FieldMask = 0x00002000;
						constexpr Reg Value_disable = 0x0;
						constexpr Reg ValueMask_disable = 0x00000000;
						constexpr Reg Value_disabled = 0x0;
						constexpr Reg ValueMask_disabled = 0x00000000;
						constexpr Reg Value_enable = 0x1;
						constexpr Reg ValueMask_enable = 0x00002000;
						constexpr Reg Value_enabled = 0x1;
						constexpr Reg ValueMask_enabled = 0x00002000;
						};
					namespace fSTIMENA14 { // Field Description
						constexpr Reg Lsb = 14;
						constexpr Reg FieldMask = 0x00004000;
						constexpr Reg Value_disable = 0x0;
						constexpr Reg ValueMask_disable = 0x00000000;
						constexpr Reg Value_disabled = 0x0;
						constexpr Reg ValueMask_disabled = 0x00000000;
						constexpr Reg Value_enable = 0x1;
						constexpr Reg ValueMask_enable = 0x00004000;
						constexpr Reg Value_enabled = 0x1;
						constexpr Reg ValueMask_enabled = 0x00004000;
						};
					namespace fSTIMENA15 { // Field Description
						constexpr Reg Lsb = 15;
						constexpr Reg FieldMask = 0x00008000;
						constexpr Reg Value_disable = 0x0;
						constexpr Reg ValueMask_disable = 0x00000000;
						constexpr Reg Value_disabled = 0x0;
						constexpr Reg ValueMask_disabled = 0x00000000;
						constexpr Reg Value_enable = 0x1;
						constexpr Reg ValueMask_enable = 0x00008000;
						constexpr Reg Value_enabled = 0x1;
						constexpr Reg ValueMask_enabled = 0x00008000;
						};
					namespace fSTIMENA16 { // Field Description
						constexpr Reg Lsb = 16;
						constexpr Reg FieldMask = 0x00010000;
						constexpr Reg Value_disable = 0x0;
						constexpr Reg ValueMask_disable = 0x00000000;
						constexpr Reg Value_disabled = 0x0;
						constexpr Reg ValueMask_disabled = 0x00000000;
						constexpr Reg Value_enable = 0x1;
						constexpr Reg ValueMask_enable = 0x00010000;
						constexpr Reg Value_enabled = 0x1;
						constexpr Reg ValueMask_enabled = 0x00010000;
						};
					namespace fSTIMENA17 { // Field Description
						constexpr Reg Lsb = 17;
						constexpr Reg FieldMask = 0x00020000;
						constexpr Reg Value_disable = 0x0;
						constexpr Reg ValueMask_disable = 0x00000000;
						constexpr Reg Value_disabled = 0x0;
						constexpr Reg ValueMask_disabled = 0x00000000;
						constexpr Reg Value_enable = 0x1;
						constexpr Reg ValueMask_enable = 0x00020000;
						constexpr Reg Value_enabled = 0x1;
						constexpr Reg ValueMask_enabled = 0x00020000;
						};
					namespace fSTIMENA18 { // Field Description
						constexpr Reg Lsb = 18;
						constexpr Reg FieldMask = 0x00040000;
						constexpr Reg Value_disable = 0x0;
						constexpr Reg ValueMask_disable = 0x00000000;
						constexpr Reg Value_disabled = 0x0;
						constexpr Reg ValueMask_disabled = 0x00000000;
						constexpr Reg Value_enable = 0x1;
						constexpr Reg ValueMask_enable = 0x00040000;
						constexpr Reg Value_enabled = 0x1;
						constexpr Reg ValueMask_enabled = 0x00040000;
						};
					namespace fSTIMENA19 { // Field Description
						constexpr Reg Lsb = 19;
						constexpr Reg FieldMask = 0x00080000;
						constexpr Reg Value_disable = 0x0;
						constexpr Reg ValueMask_disable = 0x00000000;
						constexpr Reg Value_disabled = 0x0;
						constexpr Reg ValueMask_disabled = 0x00000000;
						constexpr Reg Value_enable = 0x1;
						constexpr Reg ValueMask_enable = 0x00080000;
						constexpr Reg Value_enabled = 0x1;
						constexpr Reg ValueMask_enabled = 0x00080000;
						};
					namespace fSTIMENA20 { // Field Description
						constexpr Reg Lsb = 20;
						constexpr Reg FieldMask = 0x00100000;
						constexpr Reg Value_disable = 0x0;
						constexpr Reg ValueMask_disable = 0x00000000;
						constexpr Reg Value_disabled = 0x0;
						constexpr Reg ValueMask_disabled = 0x00000000;
						constexpr Reg Value_enable = 0x1;
						constexpr Reg ValueMask_enable = 0x00100000;
						constexpr Reg Value_enabled = 0x1;
						constexpr Reg ValueMask_enabled = 0x00100000;
						};
					namespace fSTIMENA21 { // Field Description
						constexpr Reg Lsb = 21;
						constexpr Reg FieldMask = 0x00200000;
						constexpr Reg Value_disable = 0x0;
						constexpr Reg ValueMask_disable = 0x00000000;
						constexpr Reg Value_disabled = 0x0;
						constexpr Reg ValueMask_disabled = 0x00000000;
						constexpr Reg Value_enable = 0x1;
						constexpr Reg ValueMask_enable = 0x00200000;
						constexpr Reg Value_enabled = 0x1;
						constexpr Reg ValueMask_enabled = 0x00200000;
						};
					namespace fSTIMENA22 { // Field Description
						constexpr Reg Lsb = 22;
						constexpr Reg FieldMask = 0x00400000;
						constexpr Reg Value_disable = 0x0;
						constexpr Reg ValueMask_disable = 0x00000000;
						constexpr Reg Value_disabled = 0x0;
						constexpr Reg ValueMask_disabled = 0x00000000;
						constexpr Reg Value_enable = 0x1;
						constexpr Reg ValueMask_enable = 0x00400000;
						constexpr Reg Value_enabled = 0x1;
						constexpr Reg ValueMask_enabled = 0x00400000;
						};
					namespace fSTIMENA23 { // Field Description
						constexpr Reg Lsb = 23;
						constexpr Reg FieldMask = 0x00800000;
						constexpr Reg Value_disable = 0x0;
						constexpr Reg ValueMask_disable = 0x00000000;
						constexpr Reg Value_disabled = 0x0;
						constexpr Reg ValueMask_disabled = 0x00000000;
						constexpr Reg Value_enable = 0x1;
						constexpr Reg ValueMask_enable = 0x00800000;
						constexpr Reg Value_enabled = 0x1;
						constexpr Reg ValueMask_enabled = 0x00800000;
						};
					namespace fSTIMENA24 { // Field Description
						constexpr Reg Lsb = 24;
						constexpr Reg FieldMask = 0x01000000;
						constexpr Reg Value_disable = 0x0;
						constexpr Reg ValueMask_disable = 0x00000000;
						constexpr Reg Value_disabled = 0x0;
						constexpr Reg ValueMask_disabled = 0x00000000;
						constexpr Reg Value_enable = 0x1;
						constexpr Reg ValueMask_enable = 0x01000000;
						constexpr Reg Value_enabled = 0x1;
						constexpr Reg ValueMask_enabled = 0x01000000;
						};
					namespace fSTIMENA25 { // Field Description
						constexpr Reg Lsb = 25;
						constexpr Reg FieldMask = 0x02000000;
						constexpr Reg Value_disable = 0x0;
						constexpr Reg ValueMask_disable = 0x00000000;
						constexpr Reg Value_disabled = 0x0;
						constexpr Reg ValueMask_disabled = 0x00000000;
						constexpr Reg Value_enable = 0x1;
						constexpr Reg ValueMask_enable = 0x02000000;
						constexpr Reg Value_enabled = 0x1;
						constexpr Reg ValueMask_enabled = 0x02000000;
						};
					namespace fSTIMENA26 { // Field Description
						constexpr Reg Lsb = 26;
						constexpr Reg FieldMask = 0x04000000;
						constexpr Reg Value_disable = 0x0;
						constexpr Reg ValueMask_disable = 0x00000000;
						constexpr Reg Value_disabled = 0x0;
						constexpr Reg ValueMask_disabled = 0x00000000;
						constexpr Reg Value_enable = 0x1;
						constexpr Reg ValueMask_enable = 0x04000000;
						constexpr Reg Value_enabled = 0x1;
						constexpr Reg ValueMask_enabled = 0x04000000;
						};
					namespace fSTIMENA27 { // Field Description
						constexpr Reg Lsb = 27;
						constexpr Reg FieldMask = 0x08000000;
						constexpr Reg Value_disable = 0x0;
						constexpr Reg ValueMask_disable = 0x00000000;
						constexpr Reg Value_disabled = 0x0;
						constexpr Reg ValueMask_disabled = 0x00000000;
						constexpr Reg Value_enable = 0x1;
						constexpr Reg ValueMask_enable = 0x08000000;
						constexpr Reg Value_enabled = 0x1;
						constexpr Reg ValueMask_enabled = 0x08000000;
						};
					namespace fSTIMENA28 { // Field Description
						constexpr Reg Lsb = 28;
						constexpr Reg FieldMask = 0x10000000;
						constexpr Reg Value_disable = 0x0;
						constexpr Reg ValueMask_disable = 0x00000000;
						constexpr Reg Value_disabled = 0x0;
						constexpr Reg ValueMask_disabled = 0x00000000;
						constexpr Reg Value_enable = 0x1;
						constexpr Reg ValueMask_enable = 0x10000000;
						constexpr Reg Value_enabled = 0x1;
						constexpr Reg ValueMask_enabled = 0x10000000;
						};
					namespace fSTIMENA29 { // Field Description
						constexpr Reg Lsb = 29;
						constexpr Reg FieldMask = 0x20000000;
						constexpr Reg Value_disable = 0x0;
						constexpr Reg ValueMask_disable = 0x00000000;
						constexpr Reg Value_disabled = 0x0;
						constexpr Reg ValueMask_disabled = 0x00000000;
						constexpr Reg Value_enable = 0x1;
						constexpr Reg ValueMask_enable = 0x20000000;
						constexpr Reg Value_enabled = 0x1;
						constexpr Reg ValueMask_enabled = 0x20000000;
						};
					namespace fSTIMENA30 { // Field Description
						constexpr Reg Lsb = 30;
						constexpr Reg FieldMask = 0x40000000;
						constexpr Reg Value_disable = 0x0;
						constexpr Reg ValueMask_disable = 0x00000000;
						constexpr Reg Value_disabled = 0x0;
						constexpr Reg ValueMask_disabled = 0x00000000;
						constexpr Reg Value_enable = 0x1;
						constexpr Reg ValueMask_enable = 0x40000000;
						constexpr Reg Value_enabled = 0x1;
						constexpr Reg ValueMask_enabled = 0x40000000;
						};
					namespace fSTIMENA31 { // Field Description
						constexpr Reg Lsb = 31;
						constexpr Reg FieldMask = 0x80000000;
						constexpr Reg Value_disable = 0x0;
						constexpr Reg ValueMask_disable = 0x00000000;
						constexpr Reg Value_disabled = 0x0;
						constexpr Reg ValueMask_disabled = 0x00000000;
						constexpr Reg Value_enable = 0x1;
						constexpr Reg ValueMask_enable = 0x80000000;
						constexpr Reg Value_enabled = 0x1;
						constexpr Reg ValueMask_enabled = 0x80000000;
						};
					};
				namespace rTPR { // Register description
					typedef uint32_t	Reg;
					constexpr Reg	valueAtReset = 0x00000000;
					constexpr Reg	unprivilegedPermitted = 0x00000000;
					constexpr Reg	privilegedAccessOnly = 0x00000001;
					namespace fPRIVMASK { // Field Description
						constexpr Reg Lsb = 0;
						constexpr Reg FieldMask = 0x0000000F;
						namespace fPorts0to7 { // Field Description
							constexpr Reg Lsb = 0;
							constexpr Reg FieldMask = 0x00000001;
							constexpr Reg Value_unprivilegedPermitted = 0x0;
							constexpr Reg ValueMask_unprivilegedPermitted = 0x00000000;
							constexpr Reg Value_privilegedAccessOnly = 0x1;
							constexpr Reg ValueMask_privilegedAccessOnly = 0x00000001;
							};
						namespace fPorts8to15 { // Field Description
							constexpr Reg Lsb = 1;
							constexpr Reg FieldMask = 0x00000002;
							constexpr Reg Value_unprivilegedPermitted = 0x0;
							constexpr Reg ValueMask_unprivilegedPermitted = 0x00000000;
							constexpr Reg Value_privilegedAccessOnly = 0x1;
							constexpr Reg ValueMask_privilegedAccessOnly = 0x00000002;
							};
						namespace fPorts16to23 { // Field Description
							constexpr Reg Lsb = 2;
							constexpr Reg FieldMask = 0x00000004;
							constexpr Reg Value_unprivilegedPermitted = 0x0;
							constexpr Reg ValueMask_unprivilegedPermitted = 0x00000000;
							constexpr Reg Value_privilegedAccessOnly = 0x1;
							constexpr Reg ValueMask_privilegedAccessOnly = 0x00000004;
							};
						namespace fPorts24to31 { // Field Description
							constexpr Reg Lsb = 2;
							constexpr Reg FieldMask = 0x00000004;
							constexpr Reg Value_unprivilegedPermitted = 0x0;
							constexpr Reg ValueMask_unprivilegedPermitted = 0x00000000;
							constexpr Reg Value_privilegedAccessOnly = 0x1;
							constexpr Reg ValueMask_privilegedAccessOnly = 0x00000004;
							};
						};
					namespace fPRIVMASK0 { // Field Description
						constexpr Reg Lsb = 0;
						constexpr Reg FieldMask = 0x00000001;
						constexpr Reg Value_valueAtReset = 0x0;
						constexpr Reg ValueMask_valueAtReset = 0x00000000;
						constexpr Reg Value_unprivilegedPermitted = 0x0;
						constexpr Reg ValueMask_unprivilegedPermitted = 0x00000000;
						constexpr Reg Value_privilegedAccessOnly = 0x1;
						constexpr Reg ValueMask_privilegedAccessOnly = 0x00000001;
						};
					namespace fPRIVMASK1 { // Field Description
						constexpr Reg Lsb = 1;
						constexpr Reg FieldMask = 0x00000002;
						constexpr Reg Value_valueAtReset = 0x0;
						constexpr Reg ValueMask_valueAtReset = 0x00000000;
						constexpr Reg Value_unprivilegedPermitted = 0x0;
						constexpr Reg ValueMask_unprivilegedPermitted = 0x00000000;
						constexpr Reg Value_privilegedAccessOnly = 0x1;
						constexpr Reg ValueMask_privilegedAccessOnly = 0x00000002;
						};
					namespace fPRIVMASK2 { // Field Description
						constexpr Reg Lsb = 2;
						constexpr Reg FieldMask = 0x00000004;
						constexpr Reg Value_valueAtReset = 0x0;
						constexpr Reg ValueMask_valueAtReset = 0x00000000;
						constexpr Reg Value_unprivilegedPermitted = 0x0;
						constexpr Reg ValueMask_unprivilegedPermitted = 0x00000000;
						constexpr Reg Value_privilegedAccessOnly = 0x1;
						constexpr Reg ValueMask_privilegedAccessOnly = 0x00000004;
						};
					namespace fPRIVMASK3 { // Field Description
						constexpr Reg Lsb = 3;
						constexpr Reg FieldMask = 0x00000008;
						constexpr Reg Value_valueAtReset = 0x0;
						constexpr Reg ValueMask_valueAtReset = 0x00000000;
						constexpr Reg Value_unprivilegedPermitted = 0x0;
						constexpr Reg ValueMask_unprivilegedPermitted = 0x00000000;
						constexpr Reg Value_privilegedAccessOnly = 0x1;
						constexpr Reg ValueMask_privilegedAccessOnly = 0x00000008;
						};
					namespace fPRIVMASK4 { // Field Description
						constexpr Reg Lsb = 4;
						constexpr Reg FieldMask = 0x00000010;
						constexpr Reg Value_valueAtReset = 0x0;
						constexpr Reg ValueMask_valueAtReset = 0x00000000;
						constexpr Reg Value_unprivilegedPermitted = 0x0;
						constexpr Reg ValueMask_unprivilegedPermitted = 0x00000000;
						constexpr Reg Value_privilegedAccessOnly = 0x1;
						constexpr Reg ValueMask_privilegedAccessOnly = 0x00000010;
						};
					namespace fPRIVMASK5 { // Field Description
						constexpr Reg Lsb = 5;
						constexpr Reg FieldMask = 0x00000020;
						constexpr Reg Value_valueAtReset = 0x0;
						constexpr Reg ValueMask_valueAtReset = 0x00000000;
						constexpr Reg Value_unprivilegedPermitted = 0x0;
						constexpr Reg ValueMask_unprivilegedPermitted = 0x00000000;
						constexpr Reg Value_privilegedAccessOnly = 0x1;
						constexpr Reg ValueMask_privilegedAccessOnly = 0x00000020;
						};
					namespace fPRIVMASK6 { // Field Description
						constexpr Reg Lsb = 6;
						constexpr Reg FieldMask = 0x00000040;
						constexpr Reg Value_valueAtReset = 0x0;
						constexpr Reg ValueMask_valueAtReset = 0x00000000;
						constexpr Reg Value_unprivilegedPermitted = 0x0;
						constexpr Reg ValueMask_unprivilegedPermitted = 0x00000000;
						constexpr Reg Value_privilegedAccessOnly = 0x1;
						constexpr Reg ValueMask_privilegedAccessOnly = 0x00000040;
						};
					namespace fPRIVMASK7 { // Field Description
						constexpr Reg Lsb = 7;
						constexpr Reg FieldMask = 0x00000080;
						constexpr Reg Value_valueAtReset = 0x0;
						constexpr Reg ValueMask_valueAtReset = 0x00000000;
						constexpr Reg Value_unprivilegedPermitted = 0x0;
						constexpr Reg ValueMask_unprivilegedPermitted = 0x00000000;
						constexpr Reg Value_privilegedAccessOnly = 0x1;
						constexpr Reg ValueMask_privilegedAccessOnly = 0x00000080;
						};
					namespace fPRIVMASK8 { // Field Description
						constexpr Reg Lsb = 8;
						constexpr Reg FieldMask = 0x00000100;
						constexpr Reg Value_valueAtReset = 0x0;
						constexpr Reg ValueMask_valueAtReset = 0x00000000;
						constexpr Reg Value_unprivilegedPermitted = 0x0;
						constexpr Reg ValueMask_unprivilegedPermitted = 0x00000000;
						constexpr Reg Value_privilegedAccessOnly = 0x1;
						constexpr Reg ValueMask_privilegedAccessOnly = 0x00000100;
						};
					namespace fPRIVMASK9 { // Field Description
						constexpr Reg Lsb = 9;
						constexpr Reg FieldMask = 0x00000200;
						constexpr Reg Value_valueAtReset = 0x0;
						constexpr Reg ValueMask_valueAtReset = 0x00000000;
						constexpr Reg Value_unprivilegedPermitted = 0x0;
						constexpr Reg ValueMask_unprivilegedPermitted = 0x00000000;
						constexpr Reg Value_privilegedAccessOnly = 0x1;
						constexpr Reg ValueMask_privilegedAccessOnly = 0x00000200;
						};
					namespace fPRIVMASK10 { // Field Description
						constexpr Reg Lsb = 10;
						constexpr Reg FieldMask = 0x00000400;
						constexpr Reg Value_valueAtReset = 0x0;
						constexpr Reg ValueMask_valueAtReset = 0x00000000;
						constexpr Reg Value_unprivilegedPermitted = 0x0;
						constexpr Reg ValueMask_unprivilegedPermitted = 0x00000000;
						constexpr Reg Value_privilegedAccessOnly = 0x1;
						constexpr Reg ValueMask_privilegedAccessOnly = 0x00000400;
						};
					namespace fPRIVMASK11 { // Field Description
						constexpr Reg Lsb = 11;
						constexpr Reg FieldMask = 0x00000800;
						constexpr Reg Value_valueAtReset = 0x0;
						constexpr Reg ValueMask_valueAtReset = 0x00000000;
						constexpr Reg Value_unprivilegedPermitted = 0x0;
						constexpr Reg ValueMask_unprivilegedPermitted = 0x00000000;
						constexpr Reg Value_privilegedAccessOnly = 0x1;
						constexpr Reg ValueMask_privilegedAccessOnly = 0x00000800;
						};
					namespace fPRIVMASK12 { // Field Description
						constexpr Reg Lsb = 12;
						constexpr Reg FieldMask = 0x00001000;
						constexpr Reg Value_valueAtReset = 0x0;
						constexpr Reg ValueMask_valueAtReset = 0x00000000;
						constexpr Reg Value_unprivilegedPermitted = 0x0;
						constexpr Reg ValueMask_unprivilegedPermitted = 0x00000000;
						constexpr Reg Value_privilegedAccessOnly = 0x1;
						constexpr Reg ValueMask_privilegedAccessOnly = 0x00001000;
						};
					namespace fPRIVMASK13 { // Field Description
						constexpr Reg Lsb = 13;
						constexpr Reg FieldMask = 0x00002000;
						constexpr Reg Value_valueAtReset = 0x0;
						constexpr Reg ValueMask_valueAtReset = 0x00000000;
						constexpr Reg Value_unprivilegedPermitted = 0x0;
						constexpr Reg ValueMask_unprivilegedPermitted = 0x00000000;
						constexpr Reg Value_privilegedAccessOnly = 0x1;
						constexpr Reg ValueMask_privilegedAccessOnly = 0x00002000;
						};
					namespace fPRIVMASK14 { // Field Description
						constexpr Reg Lsb = 14;
						constexpr Reg FieldMask = 0x00004000;
						constexpr Reg Value_valueAtReset = 0x0;
						constexpr Reg ValueMask_valueAtReset = 0x00000000;
						constexpr Reg Value_unprivilegedPermitted = 0x0;
						constexpr Reg ValueMask_unprivilegedPermitted = 0x00000000;
						constexpr Reg Value_privilegedAccessOnly = 0x1;
						constexpr Reg ValueMask_privilegedAccessOnly = 0x00004000;
						};
					namespace fPRIVMASK15 { // Field Description
						constexpr Reg Lsb = 15;
						constexpr Reg FieldMask = 0x00008000;
						constexpr Reg Value_valueAtReset = 0x0;
						constexpr Reg ValueMask_valueAtReset = 0x00000000;
						constexpr Reg Value_unprivilegedPermitted = 0x0;
						constexpr Reg ValueMask_unprivilegedPermitted = 0x00000000;
						constexpr Reg Value_privilegedAccessOnly = 0x1;
						constexpr Reg ValueMask_privilegedAccessOnly = 0x00008000;
						};
					namespace fPRIVMASK16 { // Field Description
						constexpr Reg Lsb = 16;
						constexpr Reg FieldMask = 0x00010000;
						constexpr Reg Value_valueAtReset = 0x0;
						constexpr Reg ValueMask_valueAtReset = 0x00000000;
						constexpr Reg Value_unprivilegedPermitted = 0x0;
						constexpr Reg ValueMask_unprivilegedPermitted = 0x00000000;
						constexpr Reg Value_privilegedAccessOnly = 0x1;
						constexpr Reg ValueMask_privilegedAccessOnly = 0x00010000;
						};
					namespace fPRIVMASK17 { // Field Description
						constexpr Reg Lsb = 17;
						constexpr Reg FieldMask = 0x00020000;
						constexpr Reg Value_valueAtReset = 0x0;
						constexpr Reg ValueMask_valueAtReset = 0x00000000;
						constexpr Reg Value_unprivilegedPermitted = 0x0;
						constexpr Reg ValueMask_unprivilegedPermitted = 0x00000000;
						constexpr Reg Value_privilegedAccessOnly = 0x1;
						constexpr Reg ValueMask_privilegedAccessOnly = 0x00020000;
						};
					namespace fPRIVMASK18 { // Field Description
						constexpr Reg Lsb = 18;
						constexpr Reg FieldMask = 0x00040000;
						constexpr Reg Value_valueAtReset = 0x0;
						constexpr Reg ValueMask_valueAtReset = 0x00000000;
						constexpr Reg Value_unprivilegedPermitted = 0x0;
						constexpr Reg ValueMask_unprivilegedPermitted = 0x00000000;
						constexpr Reg Value_privilegedAccessOnly = 0x1;
						constexpr Reg ValueMask_privilegedAccessOnly = 0x00040000;
						};
					namespace fPRIVMASK19 { // Field Description
						constexpr Reg Lsb = 19;
						constexpr Reg FieldMask = 0x00080000;
						constexpr Reg Value_valueAtReset = 0x0;
						constexpr Reg ValueMask_valueAtReset = 0x00000000;
						constexpr Reg Value_unprivilegedPermitted = 0x0;
						constexpr Reg ValueMask_unprivilegedPermitted = 0x00000000;
						constexpr Reg Value_privilegedAccessOnly = 0x1;
						constexpr Reg ValueMask_privilegedAccessOnly = 0x00080000;
						};
					namespace fPRIVMASK20 { // Field Description
						constexpr Reg Lsb = 20;
						constexpr Reg FieldMask = 0x00100000;
						constexpr Reg Value_valueAtReset = 0x0;
						constexpr Reg ValueMask_valueAtReset = 0x00000000;
						constexpr Reg Value_unprivilegedPermitted = 0x0;
						constexpr Reg ValueMask_unprivilegedPermitted = 0x00000000;
						constexpr Reg Value_privilegedAccessOnly = 0x1;
						constexpr Reg ValueMask_privilegedAccessOnly = 0x00100000;
						};
					namespace fPRIVMASK21 { // Field Description
						constexpr Reg Lsb = 21;
						constexpr Reg FieldMask = 0x00200000;
						constexpr Reg Value_valueAtReset = 0x0;
						constexpr Reg ValueMask_valueAtReset = 0x00000000;
						constexpr Reg Value_unprivilegedPermitted = 0x0;
						constexpr Reg ValueMask_unprivilegedPermitted = 0x00000000;
						constexpr Reg Value_privilegedAccessOnly = 0x1;
						constexpr Reg ValueMask_privilegedAccessOnly = 0x00200000;
						};
					namespace fPRIVMASK22 { // Field Description
						constexpr Reg Lsb = 22;
						constexpr Reg FieldMask = 0x00400000;
						constexpr Reg Value_valueAtReset = 0x0;
						constexpr Reg ValueMask_valueAtReset = 0x00000000;
						constexpr Reg Value_unprivilegedPermitted = 0x0;
						constexpr Reg ValueMask_unprivilegedPermitted = 0x00000000;
						constexpr Reg Value_privilegedAccessOnly = 0x1;
						constexpr Reg ValueMask_privilegedAccessOnly = 0x00400000;
						};
					namespace fPRIVMASK23 { // Field Description
						constexpr Reg Lsb = 23;
						constexpr Reg FieldMask = 0x00800000;
						constexpr Reg Value_valueAtReset = 0x0;
						constexpr Reg ValueMask_valueAtReset = 0x00000000;
						constexpr Reg Value_unprivilegedPermitted = 0x0;
						constexpr Reg ValueMask_unprivilegedPermitted = 0x00000000;
						constexpr Reg Value_privilegedAccessOnly = 0x1;
						constexpr Reg ValueMask_privilegedAccessOnly = 0x00800000;
						};
					namespace fPRIVMASK24 { // Field Description
						constexpr Reg Lsb = 24;
						constexpr Reg FieldMask = 0x01000000;
						constexpr Reg Value_valueAtReset = 0x0;
						constexpr Reg ValueMask_valueAtReset = 0x00000000;
						constexpr Reg Value_unprivilegedPermitted = 0x0;
						constexpr Reg ValueMask_unprivilegedPermitted = 0x00000000;
						constexpr Reg Value_privilegedAccessOnly = 0x1;
						constexpr Reg ValueMask_privilegedAccessOnly = 0x01000000;
						};
					namespace fPRIVMASK25 { // Field Description
						constexpr Reg Lsb = 25;
						constexpr Reg FieldMask = 0x02000000;
						constexpr Reg Value_valueAtReset = 0x0;
						constexpr Reg ValueMask_valueAtReset = 0x00000000;
						constexpr Reg Value_unprivilegedPermitted = 0x0;
						constexpr Reg ValueMask_unprivilegedPermitted = 0x00000000;
						constexpr Reg Value_privilegedAccessOnly = 0x1;
						constexpr Reg ValueMask_privilegedAccessOnly = 0x02000000;
						};
					namespace fPRIVMASK26 { // Field Description
						constexpr Reg Lsb = 26;
						constexpr Reg FieldMask = 0x04000000;
						constexpr Reg Value_valueAtReset = 0x0;
						constexpr Reg ValueMask_valueAtReset = 0x00000000;
						constexpr Reg Value_unprivilegedPermitted = 0x0;
						constexpr Reg ValueMask_unprivilegedPermitted = 0x00000000;
						constexpr Reg Value_privilegedAccessOnly = 0x1;
						constexpr Reg ValueMask_privilegedAccessOnly = 0x04000000;
						};
					namespace fPRIVMASK27 { // Field Description
						constexpr Reg Lsb = 27;
						constexpr Reg FieldMask = 0x08000000;
						constexpr Reg Value_valueAtReset = 0x0;
						constexpr Reg ValueMask_valueAtReset = 0x00000000;
						constexpr Reg Value_unprivilegedPermitted = 0x0;
						constexpr Reg ValueMask_unprivilegedPermitted = 0x00000000;
						constexpr Reg Value_privilegedAccessOnly = 0x1;
						constexpr Reg ValueMask_privilegedAccessOnly = 0x08000000;
						};
					namespace fPRIVMASK28 { // Field Description
						constexpr Reg Lsb = 28;
						constexpr Reg FieldMask = 0x10000000;
						constexpr Reg Value_valueAtReset = 0x0;
						constexpr Reg ValueMask_valueAtReset = 0x00000000;
						constexpr Reg Value_unprivilegedPermitted = 0x0;
						constexpr Reg ValueMask_unprivilegedPermitted = 0x00000000;
						constexpr Reg Value_privilegedAccessOnly = 0x1;
						constexpr Reg ValueMask_privilegedAccessOnly = 0x10000000;
						};
					namespace fPRIVMASK29 { // Field Description
						constexpr Reg Lsb = 29;
						constexpr Reg FieldMask = 0x20000000;
						constexpr Reg Value_valueAtReset = 0x0;
						constexpr Reg ValueMask_valueAtReset = 0x00000000;
						constexpr Reg Value_unprivilegedPermitted = 0x0;
						constexpr Reg ValueMask_unprivilegedPermitted = 0x00000000;
						constexpr Reg Value_privilegedAccessOnly = 0x1;
						constexpr Reg ValueMask_privilegedAccessOnly = 0x20000000;
						};
					namespace fPRIVMASK30 { // Field Description
						constexpr Reg Lsb = 30;
						constexpr Reg FieldMask = 0x40000000;
						constexpr Reg Value_valueAtReset = 0x0;
						constexpr Reg ValueMask_valueAtReset = 0x00000000;
						constexpr Reg Value_unprivilegedPermitted = 0x0;
						constexpr Reg ValueMask_unprivilegedPermitted = 0x00000000;
						constexpr Reg Value_privilegedAccessOnly = 0x1;
						constexpr Reg ValueMask_privilegedAccessOnly = 0x40000000;
						};
					namespace fPRIVMASK31 { // Field Description
						constexpr Reg Lsb = 31;
						constexpr Reg FieldMask = 0x80000000;
						constexpr Reg Value_valueAtReset = 0x0;
						constexpr Reg ValueMask_valueAtReset = 0x00000000;
						constexpr Reg Value_unprivilegedPermitted = 0x0;
						constexpr Reg ValueMask_unprivilegedPermitted = 0x00000000;
						constexpr Reg Value_privilegedAccessOnly = 0x1;
						constexpr Reg ValueMask_privilegedAccessOnly = 0x80000000;
						};
					};
				namespace rTCR { // Register description
					typedef uint32_t	Reg;
					constexpr Reg	valueAtReset = 0x00000000;
					namespace fITMENA { // Field Description
						constexpr Reg Lsb = 0;
						constexpr Reg FieldMask = 0x00000001;
						constexpr Reg Value_valueAtReset = 0x0;
						constexpr Reg ValueMask_valueAtReset = 0x00000000;
						constexpr Reg Value_disable = 0x0;
						constexpr Reg ValueMask_disable = 0x00000000;
						constexpr Reg Value_disabled = 0x0;
						constexpr Reg ValueMask_disabled = 0x00000000;
						constexpr Reg Value_enable = 0x1;
						constexpr Reg ValueMask_enable = 0x00000001;
						constexpr Reg Value_enabled = 0x1;
						constexpr Reg ValueMask_enabled = 0x00000001;
						};
					namespace fTSENA { // Field Description
						constexpr Reg Lsb = 1;
						constexpr Reg FieldMask = 0x00000002;
						constexpr Reg Value_valueAtReset = 0x0;
						constexpr Reg ValueMask_valueAtReset = 0x00000000;
						constexpr Reg Value_disable = 0x0;
						constexpr Reg ValueMask_disable = 0x00000000;
						constexpr Reg Value_disabled = 0x0;
						constexpr Reg ValueMask_disabled = 0x00000000;
						constexpr Reg Value_enable = 0x1;
						constexpr Reg ValueMask_enable = 0x00000002;
						constexpr Reg Value_enabled = 0x1;
						constexpr Reg ValueMask_enabled = 0x00000002;
						};
					namespace fSYNCENA { // Field Description
						constexpr Reg Lsb = 2;
						constexpr Reg FieldMask = 0x00000004;
						constexpr Reg Value_valueAtReset = 0x0;
						constexpr Reg ValueMask_valueAtReset = 0x00000000;
						constexpr Reg Value_disable = 0x0;
						constexpr Reg ValueMask_disable = 0x00000000;
						constexpr Reg Value_disabled = 0x0;
						constexpr Reg ValueMask_disabled = 0x00000000;
						constexpr Reg Value_enable = 0x1;
						constexpr Reg ValueMask_enable = 0x00000004;
						constexpr Reg Value_enabled = 0x1;
						constexpr Reg ValueMask_enabled = 0x00000004;
						};
					namespace fTXENA { // Field Description
						constexpr Reg Lsb = 3;
						constexpr Reg FieldMask = 0x00000008;
						constexpr Reg Value_valueAtReset = 0x0;
						constexpr Reg ValueMask_valueAtReset = 0x00000000;
						constexpr Reg Value_disable = 0x0;
						constexpr Reg ValueMask_disable = 0x00000000;
						constexpr Reg Value_disabled = 0x0;
						constexpr Reg ValueMask_disabled = 0x00000000;
						constexpr Reg Value_enable = 0x1;
						constexpr Reg ValueMask_enable = 0x00000008;
						constexpr Reg Value_enabled = 0x1;
						constexpr Reg ValueMask_enabled = 0x00000008;
						};
					namespace fSWOENA { // Field Description
						constexpr Reg Lsb = 4;
						constexpr Reg FieldMask = 0x00000010;
						constexpr Reg Value_valueAtReset = 0x0;
						constexpr Reg ValueMask_valueAtReset = 0x00000000;
						constexpr Reg Value_timestampUsesProcessorClock = 0x0;
						constexpr Reg ValueMask_timestampUsesProcessorClock = 0x00000000;
						};
					namespace fTSPRESCALE { // Field Description
						constexpr Reg Lsb = 8;
						constexpr Reg FieldMask = 0x00000300;
						constexpr Reg Value_valueAtReset = 0x0;
						constexpr Reg ValueMask_valueAtReset = 0x00000000;
						constexpr Reg Value_divideBy1 = 0x0;
						constexpr Reg ValueMask_divideBy1 = 0x00000000;
						constexpr Reg Value_divideBy4 = 0x1;
						constexpr Reg ValueMask_divideBy4 = 0x00000100;
						constexpr Reg Value_divideBy16 = 0x2;
						constexpr Reg ValueMask_divideBy16 = 0x00000200;
						constexpr Reg Value_divideBy64 = 0x3;
						constexpr Reg ValueMask_divideBy64 = 0x00000300;
						};
					namespace fTRACEBUSID { // Field Description
						constexpr Reg Lsb = 16;
						constexpr Reg FieldMask = 0x007F0000;
						constexpr Reg Value_valueAtReset = 0x0;
						constexpr Reg ValueMask_valueAtReset = 0x00000000;
						};
					namespace fBUSY { // Field Description
						constexpr Reg Lsb = 23;
						constexpr Reg FieldMask = 0x00800000;
						constexpr Reg Value_valueAtReset = 0x0;
						constexpr Reg ValueMask_valueAtReset = 0x00000000;
						constexpr Reg Value_notBusy = 0x0;
						constexpr Reg ValueMask_notBusy = 0x00000000;
						constexpr Reg Value_busy = 0x1;
						constexpr Reg ValueMask_busy = 0x00800000;
						};
					};
				namespace rLAR { // Register description
					typedef uint32_t	Reg;
					constexpr Reg	valueAtReset = 0x00000000;
					constexpr Reg	key = 0xC5ACCE55;
					};
				namespace rLSR { // Register description
					typedef uint32_t	Reg;
					constexpr Reg	valueAtReset = 0x00000000;
					namespace fLockImplemented { // Field Description
						constexpr Reg Lsb = 0;
						constexpr Reg FieldMask = 0x00000001;
						constexpr Reg Value_ignoresLockRegisters = 0x0;
						constexpr Reg ValueMask_ignoresLockRegisters = 0x00000000;
						constexpr Reg Value_requiresUnlock = 0x1;
						constexpr Reg ValueMask_requiresUnlock = 0x00000001;
						};
					namespace fLocked { // Field Description
						constexpr Reg Lsb = 1;
						constexpr Reg FieldMask = 0x00000002;
						constexpr Reg Value_writesPermitted = 0x0;
						constexpr Reg ValueMask_writesPermitted = 0x00000000;
						constexpr Reg Value_writesIgnored = 0x1;
						constexpr Reg ValueMask_writesIgnored = 0x00000002;
						};
					namespace f32BitAccess { // Field Description
						constexpr Reg Lsb = 2;
						constexpr Reg FieldMask = 0x00000004;
						constexpr Reg Value_lockAccess32bitOnly = 0x0;
						constexpr Reg ValueMask_lockAccess32bitOnly = 0x00000000;
						};
					};
				}
			}
		}
	}
#endif
