/*
   Copyright (C) 2024 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

PROVIDE(NVIC_ICTR	= 0xE000E004);
PROVIDE(NVIC_ISER	= 0xE000E100);
PROVIDE(NVIC_ICER	= 0xE000E180);
PROVIDE(NVIC_ISPR	= 0xE000E200);
PROVIDE(NVIC_ICPR	= 0xE000E280);
PROVIDE(NVIC_IABR	= 0xE000E300);
PROVIDE(NVIC_IPR	= 0xE000E400);

PROVIDE(NVIC_STIR = 0xE000EF00);

PROVIDE(CortexM7SCB = 0xE000E008);
PROVIDE(CortexM7FPU = 0xE000ED88);
PROVIDE(CortexM7CACHE = 0xE000EF50);
PROVIDE(CortexM7FEATURE = 0xE000ED78);
PROVIDE(CortexM7MPU = 0xE000ED90);
PROVIDE(CortexM7DCR = 0xE000EDF0);
PROVIDE(CortexM7TPIU = 0xE0040000);
PROVIDE(CortexM7ITM = 0xE0000000);
PROVIDE(CortexM7DWT = 0xE0001000);

