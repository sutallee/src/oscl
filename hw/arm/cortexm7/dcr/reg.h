/*
   Copyright (C) 2024 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _hw_arm_cortexm7_dcr_regh_
#define _hw_arm_cortexm7_dcr_regh_
#include <stdint.h>
namespace Oscl { // Namespace description
	namespace Arm { // Namespace description
		namespace CortexM7 { // Namespace description
			namespace pDCR { // Namespace description
				namespace rDHCSR { // Register description
					typedef uint32_t	Reg;
					};
				namespace rDCRSR { // Register description
					typedef uint32_t	Reg;
					};
				namespace rDCRDR { // Register description
					typedef uint32_t	Reg;
					};
				namespace rDEMCR { // Register description
					typedef uint32_t	Reg;
					constexpr Reg	valueAtReset = 0x00000000;
					namespace fVC_CORERESET { // Field Description
						constexpr Reg Lsb = 0;
						constexpr Reg FieldMask = 0x00000001;
						constexpr Reg Value_valueAtReset = 0x0;
						constexpr Reg ValueMask_valueAtReset = 0x00000000;
						constexpr Reg Value_disable = 0x0;
						constexpr Reg ValueMask_disable = 0x00000000;
						constexpr Reg Value_disabled = 0x0;
						constexpr Reg ValueMask_disabled = 0x00000000;
						constexpr Reg Value_enable = 0x1;
						constexpr Reg ValueMask_enable = 0x00000001;
						constexpr Reg Value_enabled = 0x1;
						constexpr Reg ValueMask_enabled = 0x00000001;
						};
					namespace fVC_MMERR { // Field Description
						constexpr Reg Lsb = 4;
						constexpr Reg FieldMask = 0x00000010;
						constexpr Reg Value_valueAtReset = 0x0;
						constexpr Reg ValueMask_valueAtReset = 0x00000000;
						constexpr Reg Value_disable = 0x0;
						constexpr Reg ValueMask_disable = 0x00000000;
						constexpr Reg Value_disabled = 0x0;
						constexpr Reg ValueMask_disabled = 0x00000000;
						constexpr Reg Value_enable = 0x1;
						constexpr Reg ValueMask_enable = 0x00000010;
						constexpr Reg Value_enabled = 0x1;
						constexpr Reg ValueMask_enabled = 0x00000010;
						};
					namespace fVC_NOCPERR { // Field Description
						constexpr Reg Lsb = 5;
						constexpr Reg FieldMask = 0x00000020;
						constexpr Reg Value_valueAtReset = 0x0;
						constexpr Reg ValueMask_valueAtReset = 0x00000000;
						constexpr Reg Value_disable = 0x0;
						constexpr Reg ValueMask_disable = 0x00000000;
						constexpr Reg Value_disabled = 0x0;
						constexpr Reg ValueMask_disabled = 0x00000000;
						constexpr Reg Value_enable = 0x1;
						constexpr Reg ValueMask_enable = 0x00000020;
						constexpr Reg Value_enabled = 0x1;
						constexpr Reg ValueMask_enabled = 0x00000020;
						};
					namespace fVC_CHKERR { // Field Description
						constexpr Reg Lsb = 6;
						constexpr Reg FieldMask = 0x00000040;
						constexpr Reg Value_valueAtReset = 0x0;
						constexpr Reg ValueMask_valueAtReset = 0x00000000;
						constexpr Reg Value_disable = 0x0;
						constexpr Reg ValueMask_disable = 0x00000000;
						constexpr Reg Value_disabled = 0x0;
						constexpr Reg ValueMask_disabled = 0x00000000;
						constexpr Reg Value_enable = 0x1;
						constexpr Reg ValueMask_enable = 0x00000040;
						constexpr Reg Value_enabled = 0x1;
						constexpr Reg ValueMask_enabled = 0x00000040;
						};
					namespace fVC_STATERR { // Field Description
						constexpr Reg Lsb = 7;
						constexpr Reg FieldMask = 0x00000080;
						constexpr Reg Value_valueAtReset = 0x0;
						constexpr Reg ValueMask_valueAtReset = 0x00000000;
						constexpr Reg Value_disable = 0x0;
						constexpr Reg ValueMask_disable = 0x00000000;
						constexpr Reg Value_disabled = 0x0;
						constexpr Reg ValueMask_disabled = 0x00000000;
						constexpr Reg Value_enable = 0x1;
						constexpr Reg ValueMask_enable = 0x00000080;
						constexpr Reg Value_enabled = 0x1;
						constexpr Reg ValueMask_enabled = 0x00000080;
						};
					namespace fVC_BUSERR { // Field Description
						constexpr Reg Lsb = 8;
						constexpr Reg FieldMask = 0x00000100;
						constexpr Reg Value_valueAtReset = 0x0;
						constexpr Reg ValueMask_valueAtReset = 0x00000000;
						constexpr Reg Value_disable = 0x0;
						constexpr Reg ValueMask_disable = 0x00000000;
						constexpr Reg Value_disabled = 0x0;
						constexpr Reg ValueMask_disabled = 0x00000000;
						constexpr Reg Value_enable = 0x1;
						constexpr Reg ValueMask_enable = 0x00000100;
						constexpr Reg Value_enabled = 0x1;
						constexpr Reg ValueMask_enabled = 0x00000100;
						};
					namespace fVC_INTERR { // Field Description
						constexpr Reg Lsb = 9;
						constexpr Reg FieldMask = 0x00000200;
						constexpr Reg Value_valueAtReset = 0x0;
						constexpr Reg ValueMask_valueAtReset = 0x00000000;
						constexpr Reg Value_disable = 0x0;
						constexpr Reg ValueMask_disable = 0x00000000;
						constexpr Reg Value_disabled = 0x0;
						constexpr Reg ValueMask_disabled = 0x00000000;
						constexpr Reg Value_enable = 0x1;
						constexpr Reg ValueMask_enable = 0x00000200;
						constexpr Reg Value_enabled = 0x1;
						constexpr Reg ValueMask_enabled = 0x00000200;
						};
					namespace fVC_HARDERR { // Field Description
						constexpr Reg Lsb = 10;
						constexpr Reg FieldMask = 0x00000400;
						constexpr Reg Value_valueAtReset = 0x0;
						constexpr Reg ValueMask_valueAtReset = 0x00000000;
						constexpr Reg Value_disable = 0x0;
						constexpr Reg ValueMask_disable = 0x00000000;
						constexpr Reg Value_disabled = 0x0;
						constexpr Reg ValueMask_disabled = 0x00000000;
						constexpr Reg Value_enable = 0x1;
						constexpr Reg ValueMask_enable = 0x00000400;
						constexpr Reg Value_enabled = 0x1;
						constexpr Reg ValueMask_enabled = 0x00000400;
						};
					namespace fMON_EN { // Field Description
						constexpr Reg Lsb = 16;
						constexpr Reg FieldMask = 0x00010000;
						constexpr Reg Value_valueAtReset = 0x0;
						constexpr Reg ValueMask_valueAtReset = 0x00000000;
						constexpr Reg Value_disable = 0x0;
						constexpr Reg ValueMask_disable = 0x00000000;
						constexpr Reg Value_disabled = 0x0;
						constexpr Reg ValueMask_disabled = 0x00000000;
						constexpr Reg Value_enable = 0x1;
						constexpr Reg ValueMask_enable = 0x00010000;
						constexpr Reg Value_enabled = 0x1;
						constexpr Reg ValueMask_enabled = 0x00010000;
						};
					namespace fMON_PEND { // Field Description
						constexpr Reg Lsb = 17;
						constexpr Reg FieldMask = 0x00020000;
						constexpr Reg Value_valueAtReset = 0x0;
						constexpr Reg ValueMask_valueAtReset = 0x00000000;
						constexpr Reg Value_disable = 0x0;
						constexpr Reg ValueMask_disable = 0x00000000;
						constexpr Reg Value_disabled = 0x0;
						constexpr Reg ValueMask_disabled = 0x00000000;
						constexpr Reg Value_enable = 0x1;
						constexpr Reg ValueMask_enable = 0x00020000;
						constexpr Reg Value_enabled = 0x1;
						constexpr Reg ValueMask_enabled = 0x00020000;
						};
					namespace fMON_STEP { // Field Description
						constexpr Reg Lsb = 18;
						constexpr Reg FieldMask = 0x00040000;
						constexpr Reg Value_valueAtReset = 0x0;
						constexpr Reg ValueMask_valueAtReset = 0x00000000;
						constexpr Reg Value_noStep = 0x0;
						constexpr Reg ValueMask_noStep = 0x00000000;
						constexpr Reg Value_step = 0x1;
						constexpr Reg ValueMask_step = 0x00040000;
						};
					namespace fMON_REQ { // Field Description
						constexpr Reg Lsb = 19;
						constexpr Reg FieldMask = 0x00080000;
						constexpr Reg Value_valueAtReset = 0x0;
						constexpr Reg ValueMask_valueAtReset = 0x00000000;
						};
					namespace fTRCENA { // Field Description
						constexpr Reg Lsb = 24;
						constexpr Reg FieldMask = 0x01000000;
						constexpr Reg Value_valueAtReset = 0x0;
						constexpr Reg ValueMask_valueAtReset = 0x00000000;
						constexpr Reg Value_disable = 0x0;
						constexpr Reg ValueMask_disable = 0x00000000;
						constexpr Reg Value_disabled = 0x0;
						constexpr Reg ValueMask_disabled = 0x00000000;
						constexpr Reg Value_enable = 0x1;
						constexpr Reg ValueMask_enable = 0x01000000;
						constexpr Reg Value_enabled = 0x1;
						constexpr Reg ValueMask_enabled = 0x01000000;
						};
					};
				}
			}
		}
	}
#endif
