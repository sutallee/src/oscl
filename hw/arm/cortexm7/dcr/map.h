/*
   Copyright (C) 2024 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/
#ifndef _oscl_hw_arm_cortexm7_dcr_maph_
#define _oscl_hw_arm_cortexm7_dcr_maph_
#include "reg.h"

/** */
namespace Oscl {
/** */
namespace Arm {
/** */
namespace CortexM7 {
/** */
namespace pDCR {

/** 0xE000EDF0 */
struct Map {
	/** 0xE000EDF0, offset 0x00000000 */
	volatile rDHCSR::Reg	dhcsr;

	/** 0xE000EDF4, offset 0x00000004 */
	volatile rDCRSR::Reg	dcrsr;

	/** 0xE000EDF8, offset 0x00000008 */
	volatile rDCRDR::Reg	dcrdr;

	/** 0xE000EDFC, offset 0x0000000C */
	volatile pDCR::rDEMCR::Reg	demcr;

	/** 0xE000EE00, offset 0x00000010 */
	uint8_t	reservedE000ED30[0xE000EF00-0xE000EE00];
	};

}
}
}
}

#endif
