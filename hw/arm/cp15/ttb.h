#ifndef _hw_arm_cp15_ttbh_
#define _hw_arm_cp15_ttbh_
#include <stdint.h>
namespace Oscl { // Namespace description
	namespace HW { // Namespace description
		namespace ARM { // Namespace description
			namespace CP15 { // Namespace description
				namespace TransTableBase { // Register description
					typedef uint32_t	Reg;
					namespace PhysicalAddress { // Field Description
						enum {Lsb = 14};
						enum {FieldMask = 0xFFFFC000};
						};
					namespace UnpSbzp { // Field Description
						enum {Lsb = 0};
						enum {FieldMask = 0x00003FFF};
						};
					};
				};
			};
		};
	};
#endif
