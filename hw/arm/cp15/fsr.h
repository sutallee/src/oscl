#ifndef _hw_arm_cp15_ttbh_
#define _hw_arm_cp15_ttbh_
#include <stdint.h>
namespace Oscl { // Namespace description
	namespace HW { // Namespace description
		namespace ARM { // Namespace description
			namespace CP15 { // Namespace description
				namespace FSR { // Register description
					typedef uint32_t	Reg;
					namespace Domain { // Field Description
						enum {Lsb = 4};
						enum {FieldMask = 0x000000F0};
						enum {Value_D0 = 0x0};
						enum {ValueMask_D0 = 0x00000000};
						enum {Value_D1 = 0x1};
						enum {ValueMask_D1 = 0x00000010};
						enum {Value_D2 = 0x2};
						enum {ValueMask_D2 = 0x00000020};
						enum {Value_D3 = 0x3};
						enum {ValueMask_D3 = 0x00000030};
						enum {Value_D4 = 0x4};
						enum {ValueMask_D4 = 0x00000040};
						enum {Value_D5 = 0x5};
						enum {ValueMask_D5 = 0x00000050};
						enum {Value_D6 = 0x6};
						enum {ValueMask_D6 = 0x00000060};
						enum {Value_D7 = 0x7};
						enum {ValueMask_D7 = 0x00000070};
						enum {Value_D8 = 0x8};
						enum {ValueMask_D8 = 0x00000080};
						enum {Value_D9 = 0x9};
						enum {ValueMask_D9 = 0x00000090};
						enum {Value_D10 = 0xA};
						enum {ValueMask_D10 = 0x000000A0};
						enum {Value_D11 = 0xB};
						enum {ValueMask_D11 = 0x000000B0};
						enum {Value_D12 = 0xC};
						enum {ValueMask_D12 = 0x000000C0};
						enum {Value_D13 = 0xD};
						enum {ValueMask_D13 = 0x000000D0};
						enum {Value_D14 = 0xE};
						enum {ValueMask_D14 = 0x000000E0};
						enum {Value_D15 = 0xF};
						enum {ValueMask_D15 = 0x000000F0};
						};
					namespace Status { // Field Description
						enum {Lsb = 0};
						enum {FieldMask = 0x0000000F};
						enum {Value_TerminalException = 0x2};
						enum {ValueMask_TerminalException = 0x00000002};
						enum {Value_VectorException = 0x0};
						enum {ValueMask_VectorException = 0x00000000};
						enum {Value_Alignment01 = 0x1};
						enum {ValueMask_Alignment01 = 0x00000001};
						enum {Value_Alignment11 = 0x3};
						enum {ValueMask_Alignment11 = 0x00000003};
						enum {Value_FirstLevelExternalAbortOnTranslation = 0xC};
						enum {ValueMask_FirstLevelExternalAbortOnTranslation = 0x0000000C};
						enum {Value_SecondLevelExternalAbortOnTranslation = 0xE};
						enum {ValueMask_SecondLevelExternalAbortOnTranslation = 0x0000000E};
						enum {Value_SectionTranslation = 0x5};
						enum {ValueMask_SectionTranslation = 0x00000005};
						enum {Value_PageTranslation = 0x7};
						enum {ValueMask_PageTranslation = 0x00000007};
						enum {Value_SectionDomain = 0x9};
						enum {ValueMask_SectionDomain = 0x00000009};
						enum {Value_PageDomain = 0xB};
						enum {ValueMask_PageDomain = 0x0000000B};
						enum {Value_SectionPermission = 0xD};
						enum {ValueMask_SectionPermission = 0x0000000D};
						enum {Value_PagePermission = 0xF};
						enum {ValueMask_PagePermission = 0x0000000F};
						enum {Value_SectionExternalAbortOnLinefetch = 0x4};
						enum {ValueMask_SectionExternalAbortOnLinefetch = 0x00000004};
						enum {Value_PageExternalAbortOnLinefetch = 0x6};
						enum {ValueMask_PageExternalAbortOnLinefetch = 0x00000006};
						enum {Value_SectionExternalAbortOnNonLinefetch = 0x8};
						enum {ValueMask_SectionExternalAbortOnNonLinefetch = 0x00000008};
						enum {Value_PageExternalAbortOnNonLinefetch = 0xA};
						enum {ValueMask_PageExternalAbortOnNonLinefetch = 0x0000000A};
						};
					};
				};
			};
		};
	};
#endif
