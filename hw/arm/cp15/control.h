#ifndef _hw_arm_cp15_controlh_
#define _hw_arm_cp15_controlh_
#include <stdint.h>
namespace Oscl { // Namespace description
	namespace HW { // Namespace description
		namespace ARM { // Namespace description
			namespace CP15 { // Namespace description
				namespace Control { // Register description
					typedef uint32_t	Reg;
					namespace UnpSbzp { // Field Description
						enum {Lsb = 16};
						enum {FieldMask = 0xFFFF0000};
						};
					namespace L4 { // Field Description
						enum {Lsb = 15};
						enum {FieldMask = 0x00008000};
						enum {Value_NormalBehaviour = 0x0};
						enum {ValueMask_NormalBehaviour = 0x00000000};
						enum {Value_IgnoreThumbPcBitZeroState = 0x1};
						enum {ValueMask_IgnoreThumbPcBitZeroState = 0x00008000};
						};
					namespace RR { // Field Description
						enum {Lsb = 14};
						enum {FieldMask = 0x00004000};
						enum {Value_NormalCacheReplacementStrategy = 0x0};
						enum {ValueMask_NormalCacheReplacementStrategy = 0x00000000};
						enum {Value_PredictableCacheReplacementStrategy = 0x1};
						enum {ValueMask_PredictableCacheReplacementStrategy = 0x00004000};
						};
					namespace V { // Field Description
						enum {Lsb = 13};
						enum {FieldMask = 0x00002000};
						enum {Value_ExceptionVectors00000000 = 0x0};
						enum {ValueMask_ExceptionVectors00000000 = 0x00000000};
						enum {Value_ExceptionVectorsFFFF0000 = 0x1};
						enum {ValueMask_ExceptionVectorsFFFF0000 = 0x00002000};
						};
					namespace I { // Field Description
						enum {Lsb = 12};
						enum {FieldMask = 0x00001000};
						enum {Value_InstructionCacheDisabled = 0x0};
						enum {ValueMask_InstructionCacheDisabled = 0x00000000};
						enum {Value_InstructionCacheEnabled = 0x1};
						enum {ValueMask_InstructionCacheEnabled = 0x00001000};
						};
					namespace Z { // Field Description
						enum {Lsb = 11};
						enum {FieldMask = 0x00000800};
						enum {Value_BranchPredictionDisabled = 0x0};
						enum {ValueMask_BranchPredictionDisabled = 0x00000000};
						enum {Value_BranchPredictionEnabled = 0x1};
						enum {ValueMask_BranchPredictionEnabled = 0x00000800};
						};
					namespace F { // Field Description
						enum {Lsb = 10};
						enum {FieldMask = 0x00000400};
						};
					namespace R { // Field Description
						enum {Lsb = 9};
						enum {FieldMask = 0x00000200};
						enum {Value_NoAccess = 0x0};
						enum {ValueMask_NoAccess = 0x00000000};
						enum {Value_ReadOnly = 0x1};
						enum {ValueMask_ReadOnly = 0x00000200};
						};
					namespace S { // Field Description
						enum {Lsb = 8};
						enum {FieldMask = 0x00000100};
						enum {Value_ReadOnly = 0x0};
						enum {ValueMask_ReadOnly = 0x00000000};
						enum {Value_NoAccess = 0x1};
						enum {ValueMask_NoAccess = 0x00000100};
						};
					namespace B { // Field Description
						enum {Lsb = 7};
						enum {FieldMask = 0x00000080};
						enum {Value_LittleEndian = 0x0};
						enum {ValueMask_LittleEndian = 0x00000000};
						enum {Value_BigEndian = 0x1};
						enum {ValueMask_BigEndian = 0x00000080};
						};
					namespace L { // Field Description
						enum {Lsb = 6};
						enum {FieldMask = 0x00000040};
						enum {Value_EarlyAbortModel = 0x0};
						enum {ValueMask_EarlyAbortModel = 0x00000000};
						enum {Value_LateAbortModel = 0x1};
						enum {ValueMask_LateAbortModel = 0x00000040};
						};
					namespace D { // Field Description
						enum {Lsb = 5};
						enum {FieldMask = 0x00000020};
						enum {Value_AddressException26BitCheckingDisabled = 0x0};
						enum {ValueMask_AddressException26BitCheckingDisabled = 0x00000000};
						enum {Value_AddressException26BitCheckingEnabled = 0x1};
						enum {ValueMask_AddressException26BitCheckingEnabled = 0x00000020};
						};
					namespace P { // Field Description
						enum {Lsb = 4};
						enum {FieldMask = 0x00000010};
						enum {Value_ExceptionHandlersEnteredIn26BitModes = 0x0};
						enum {ValueMask_ExceptionHandlersEnteredIn26BitModes = 0x00000000};
						enum {Value_ExceptionHandlersEnteredIn32BitModes = 0x1};
						enum {ValueMask_ExceptionHandlersEnteredIn32BitModes = 0x00000010};
						};
					namespace W { // Field Description
						enum {Lsb = 3};
						enum {FieldMask = 0x00000008};
						enum {Value_WriteBufferDisabled = 0x0};
						enum {ValueMask_WriteBufferDisabled = 0x00000000};
						enum {Value_WriteBufferEnabled = 0x1};
						enum {ValueMask_WriteBufferEnabled = 0x00000008};
						};
					namespace C { // Field Description
						enum {Lsb = 2};
						enum {FieldMask = 0x00000004};
						enum {Value_CacheDisabled = 0x0};
						enum {ValueMask_CacheDisabled = 0x00000000};
						enum {Value_CacheEnabled = 0x1};
						enum {ValueMask_CacheEnabled = 0x00000004};
						};
					namespace A { // Field Description
						enum {Lsb = 1};
						enum {FieldMask = 0x00000002};
						enum {Value_AlignmentFaultCheckingDisabled = 0x0};
						enum {ValueMask_AlignmentFaultCheckingDisabled = 0x00000000};
						enum {Value_AlignmentFaultCheckingEnabled = 0x1};
						enum {ValueMask_AlignmentFaultCheckingEnabled = 0x00000002};
						};
					namespace M { // Field Description
						enum {Lsb = 0};
						enum {FieldMask = 0x00000001};
						enum {Value_MmuOrProtectionUnitDisabled = 0x0};
						enum {ValueMask_MmuOrProtectionUnitDisabled = 0x00000000};
						enum {Value_MmuOrProtectionUnitEnabled = 0x1};
						enum {ValueMask_MmuOrProtectionUnitEnabled = 0x00000001};
						};
					};
				};
			};
		};
	};
#endif
