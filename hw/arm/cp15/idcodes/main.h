#ifndef _hw_arm_cp15_idcodes_mainh_
#define _hw_arm_cp15_idcodes_mainh_
#include <stdint.h>
namespace Oscl { // Namespace description
	namespace HW { // Namespace description
		namespace ARM { // Namespace description
			namespace CP15 { // Namespace description
				namespace IdCodes { // Namespace description
					namespace Main { // Namespace description
						namespace All { // Register description
							typedef uint32_t	Reg;
							namespace RegisterVersionType { // Field Description
								enum {Lsb = 12};
								enum {FieldMask = 0x0000F000};
								enum {Value_PreArm7 = 0x0};
								enum {ValueMask_PreArm7 = 0x00000000};
								enum {Value_Arm7 = 0x7};
								enum {ValueMask_Arm7 = 0x00007000};
								};
							};
						namespace PostArm7 { // Register description
							typedef uint32_t	Reg;
							namespace Implementor { // Field Description
								enum {Lsb = 24};
								enum {FieldMask = 0xFF000000};
								enum {Value_ArmLtd = 0x41};
								enum {ValueMask_ArmLtd = 0x41000000};
								enum {Value_DEC = 0x44};
								enum {ValueMask_DEC = 0x44000000};
								enum {Value_Intel = 0x69};
								enum {ValueMask_Intel = 0x69000000};
								};
							namespace Variant { // Field Description
								enum {Lsb = 20};
								enum {FieldMask = 0x00F00000};
								};
							namespace Architecture { // Field Description
								enum {Lsb = 16};
								enum {FieldMask = 0x000F0000};
								enum {Value_Architecture4 = 0x1};
								enum {ValueMask_Architecture4 = 0x00010000};
								enum {Value_Architecture4T = 0x2};
								enum {ValueMask_Architecture4T = 0x00020000};
								enum {Value_Architecture5 = 0x3};
								enum {ValueMask_Architecture5 = 0x00030000};
								enum {Value_Architecture5T = 0x4};
								enum {ValueMask_Architecture5T = 0x00040000};
								enum {Value_Architecture5TE = 0x5};
								enum {ValueMask_Architecture5TE = 0x00050000};
								};
							namespace PrimaryPartNumber { // Field Description
								enum {Lsb = 4};
								enum {FieldMask = 0x0000FFF0};
								namespace TopFourBits { // Field Description
									enum {Lsb = 12};
									enum {FieldMask = 0x0000F000};
									enum {Value_InvalidCode1 = 0x0};
									enum {ValueMask_InvalidCode1 = 0x00000000};
									enum {Value_InvalidCode2 = 0x7};
									enum {ValueMask_InvalidCode2 = 0x00007000};
									};
								};
							namespace Revision { // Field Description
								enum {Lsb = 0};
								enum {FieldMask = 0x0000000F};
								};
							};
						namespace PreArm7 { // Register description
							typedef uint32_t	Reg;
							namespace ProcessorID { // Field Description
								enum {Lsb = 4};
								enum {FieldMask = 0xFFFFFFF0};
								enum {Value_ARM3 = 0x4156030};
								enum {ValueMask_ARM3 = 0x41560300};
								enum {Value_ARM600 = 0x4156060};
								enum {ValueMask_ARM600 = 0x41560600};
								enum {Value_ARM610 = 0x4156061};
								enum {ValueMask_ARM610 = 0x41560610};
								enum {Value_ARM620 = 0x4156062};
								enum {ValueMask_ARM620 = 0x41560620};
								};
							namespace Revision { // Field Description
								enum {Lsb = 0};
								enum {FieldMask = 0x0000000F};
								};
							};
						namespace Arm7 { // Register description
							typedef uint32_t	Reg;
							namespace Implementor { // Field Description
								enum {Lsb = 24};
								enum {FieldMask = 0xFF000000};
								enum {Value_ArmLtd = 0x41};
								enum {ValueMask_ArmLtd = 0x41000000};
								enum {Value_DEC = 0x44};
								enum {ValueMask_DEC = 0x44000000};
								enum {Value_Intel = 0x69};
								enum {ValueMask_Intel = 0x69000000};
								};
							namespace A { // Field Description
								enum {Lsb = 23};
								enum {FieldMask = 0x00800000};
								enum {Value_Architecture3 = 0x0};
								enum {ValueMask_Architecture3 = 0x00000000};
								enum {Value_Architecture4T = 0x1};
								enum {ValueMask_Architecture4T = 0x00800000};
								};
							namespace Variant { // Field Description
								enum {Lsb = 16};
								enum {FieldMask = 0x007F0000};
								};
							namespace PrimaryPartNumber { // Field Description
								enum {Lsb = 4};
								enum {FieldMask = 0x0000FFF0};
								};
							namespace Revision { // Field Description
								enum {Lsb = 0};
								enum {FieldMask = 0x0000000F};
								};
							};
						};
					};
				};
			};
		};
	};
#endif
