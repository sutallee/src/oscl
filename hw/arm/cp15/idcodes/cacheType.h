#ifndef _hw_arm_cp15_idcodes_cacheTypeh_
#define _hw_arm_cp15_idcodes_cacheTypeh_
#include <stdint.h>
namespace Oscl { // Namespace description
	namespace HW { // Namespace description
		namespace ARM { // Namespace description
			namespace CP15 { // Namespace description
				namespace IdCodes { // Namespace description
					namespace CacheType { // Register description
						typedef uint32_t	Reg;
						namespace ctype { // Field Description
							enum {Lsb = 25};
							enum {FieldMask = 0x1E000000};
							enum {Value_WriteThrough = 0x0};
							enum {ValueMask_WriteThrough = 0x00000000};
							enum {Value_WriteBackReadBlockNoLockDown = 0x1};
							enum {ValueMask_WriteBackReadBlockNoLockDown = 0x02000000};
							enum {Value_WriteBackRegister7NoLockDown = 0x2};
							enum {ValueMask_WriteBackRegister7NoLockDown = 0x04000000};
							enum {Value_WriteBackRegister7FormatALockDown = 0x6};
							enum {ValueMask_WriteBackRegister7FormatALockDown = 0x0C000000};
							enum {Value_WriteBackRegister7FormatBLockDown = 0x7};
							enum {ValueMask_WriteBackRegister7FormatBLockDown = 0x0E000000};
							};
						namespace S { // Field Description
							enum {Lsb = 24};
							enum {FieldMask = 0x01000000};
							enum {Value_Unified = 0x0};
							enum {ValueMask_Unified = 0x00000000};
							enum {Value_Separate = 0x1};
							enum {ValueMask_Separate = 0x01000000};
							};
						namespace Dsize { // Field Description
							enum {Lsb = 12};
							enum {FieldMask = 0x00FFF000};
							};
						namespace Isize { // Field Description
							enum {Lsb = 0};
							enum {FieldMask = 0x00000FFF};
							namespace size { // Field Description
								enum {Lsb = 6};
								enum {FieldMask = 0x000001C0};
								enum {Value_M0_0_5KB = 0x0};
								enum {ValueMask_M0_0_5KB = 0x00000000};
								enum {Value_M0_1KB = 0x1};
								enum {ValueMask_M0_1KB = 0x00000040};
								enum {Value_M0_2KB = 0x2};
								enum {ValueMask_M0_2KB = 0x00000080};
								enum {Value_M0_4KB = 0x3};
								enum {ValueMask_M0_4KB = 0x000000C0};
								enum {Value_M0_8KB = 0x4};
								enum {ValueMask_M0_8KB = 0x00000100};
								enum {Value_M0_16KB = 0x5};
								enum {ValueMask_M0_16KB = 0x00000140};
								enum {Value_M0_32KB = 0x6};
								enum {ValueMask_M0_32KB = 0x00000180};
								enum {Value_M0_64KB = 0x7};
								enum {ValueMask_M0_64KB = 0x000001C0};
								enum {Value_M1_0_75KB = 0x0};
								enum {ValueMask_M1_0_75KB = 0x00000000};
								enum {Value_M1_1_5KB = 0x1};
								enum {ValueMask_M1_1_5KB = 0x00000040};
								enum {Value_M1_3KB = 0x2};
								enum {ValueMask_M1_3KB = 0x00000080};
								enum {Value_M1_6KB = 0x3};
								enum {ValueMask_M1_6KB = 0x000000C0};
								enum {Value_M1_12KB = 0x4};
								enum {ValueMask_M1_12KB = 0x00000100};
								enum {Value_M1_24KB = 0x5};
								enum {ValueMask_M1_24KB = 0x00000140};
								enum {Value_M1_48KB = 0x6};
								enum {ValueMask_M1_48KB = 0x00000180};
								enum {Value_M1_96KB = 0x7};
								enum {ValueMask_M1_96KB = 0x000001C0};
								};
							namespace assoc { // Field Description
								enum {Lsb = 3};
								enum {FieldMask = 0x00000038};
								enum {Value_M0_1Way = 0x0};
								enum {ValueMask_M0_1Way = 0x00000000};
								enum {Value_M0_2Way = 0x1};
								enum {ValueMask_M0_2Way = 0x00000008};
								enum {Value_M0_4Way = 0x2};
								enum {ValueMask_M0_4Way = 0x00000010};
								enum {Value_M0_8Way = 0x3};
								enum {ValueMask_M0_8Way = 0x00000018};
								enum {Value_M0_16Way = 0x4};
								enum {ValueMask_M0_16Way = 0x00000020};
								enum {Value_M0_32Way = 0x5};
								enum {ValueMask_M0_32Way = 0x00000028};
								enum {Value_M0_64Way = 0x6};
								enum {ValueMask_M0_64Way = 0x00000030};
								enum {Value_M0_128Way = 0x7};
								enum {ValueMask_M0_128Way = 0x00000038};
								enum {Value_M1_NoCache = 0x0};
								enum {ValueMask_M1_NoCache = 0x00000000};
								enum {Value_M1_3Way = 0x1};
								enum {ValueMask_M1_3Way = 0x00000008};
								enum {Value_M1_6Way = 0x2};
								enum {ValueMask_M1_6Way = 0x00000010};
								enum {Value_M1_12Way = 0x3};
								enum {ValueMask_M1_12Way = 0x00000018};
								enum {Value_M1_24Way = 0x4};
								enum {ValueMask_M1_24Way = 0x00000020};
								enum {Value_M1_48Way = 0x5};
								enum {ValueMask_M1_48Way = 0x00000028};
								enum {Value_M1_96Way = 0x6};
								enum {ValueMask_M1_96Way = 0x00000030};
								enum {Value_M1_192Way = 0x7};
								enum {ValueMask_M1_192Way = 0x00000038};
								};
							namespace M { // Field Description
								enum {Lsb = 2};
								enum {FieldMask = 0x00000004};
								};
							namespace len { // Field Description
								enum {Lsb = 0};
								enum {FieldMask = 0x00000003};
								enum {Value_CacheLine8Bytes = 0x0};
								enum {ValueMask_CacheLine8Bytes = 0x00000000};
								enum {Value_CacheLine16Bytes = 0x1};
								enum {ValueMask_CacheLine16Bytes = 0x00000001};
								enum {Value_CacheLine32Bytes = 0x2};
								enum {ValueMask_CacheLine32Bytes = 0x00000002};
								enum {Value_CacheLine64Bytes = 0x3};
								enum {ValueMask_CacheLine64Bytes = 0x00000003};
								};
							};
						};
					};
				};
			};
		};
	};
#endif
