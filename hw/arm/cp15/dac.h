#ifndef _hw_arm_cp15_ttbh_
#define _hw_arm_cp15_ttbh_
#include <stdint.h>
namespace Oscl { // Namespace description
	namespace HW { // Namespace description
		namespace ARM { // Namespace description
			namespace CP15 { // Namespace description
				namespace DomainAccessControl { // Register description
					typedef uint32_t	Reg;
					namespace D15 { // Field Description
						enum {Lsb = 30};
						enum {FieldMask = 0xC0000000};
						enum {Value_NoAccess = 0x0};
						enum {ValueMask_NoAccess = 0x00000000};
						enum {Value_Client = 0x1};
						enum {ValueMask_Client = 0x40000000};
						enum {Value_Manager = 0x3};
						enum {ValueMask_Manager = 0xC0000000};
						};
					namespace D14 { // Field Description
						enum {Lsb = 28};
						enum {FieldMask = 0x30000000};
						enum {Value_NoAccess = 0x0};
						enum {ValueMask_NoAccess = 0x00000000};
						enum {Value_Client = 0x1};
						enum {ValueMask_Client = 0x10000000};
						enum {Value_Manager = 0x3};
						enum {ValueMask_Manager = 0x30000000};
						};
					namespace D13 { // Field Description
						enum {Lsb = 26};
						enum {FieldMask = 0x0C000000};
						enum {Value_NoAccess = 0x0};
						enum {ValueMask_NoAccess = 0x00000000};
						enum {Value_Client = 0x1};
						enum {ValueMask_Client = 0x04000000};
						enum {Value_Manager = 0x3};
						enum {ValueMask_Manager = 0x0C000000};
						};
					namespace D12 { // Field Description
						enum {Lsb = 24};
						enum {FieldMask = 0x03000000};
						enum {Value_NoAccess = 0x0};
						enum {ValueMask_NoAccess = 0x00000000};
						enum {Value_Client = 0x1};
						enum {ValueMask_Client = 0x01000000};
						enum {Value_Manager = 0x3};
						enum {ValueMask_Manager = 0x03000000};
						};
					namespace D11 { // Field Description
						enum {Lsb = 22};
						enum {FieldMask = 0x00C00000};
						enum {Value_NoAccess = 0x0};
						enum {ValueMask_NoAccess = 0x00000000};
						enum {Value_Client = 0x1};
						enum {ValueMask_Client = 0x00400000};
						enum {Value_Manager = 0x3};
						enum {ValueMask_Manager = 0x00C00000};
						};
					namespace D10 { // Field Description
						enum {Lsb = 20};
						enum {FieldMask = 0x00300000};
						enum {Value_NoAccess = 0x0};
						enum {ValueMask_NoAccess = 0x00000000};
						enum {Value_Client = 0x1};
						enum {ValueMask_Client = 0x00100000};
						enum {Value_Manager = 0x3};
						enum {ValueMask_Manager = 0x00300000};
						};
					namespace D9 { // Field Description
						enum {Lsb = 18};
						enum {FieldMask = 0x000C0000};
						enum {Value_NoAccess = 0x0};
						enum {ValueMask_NoAccess = 0x00000000};
						enum {Value_Client = 0x1};
						enum {ValueMask_Client = 0x00040000};
						enum {Value_Manager = 0x3};
						enum {ValueMask_Manager = 0x000C0000};
						};
					namespace D8 { // Field Description
						enum {Lsb = 16};
						enum {FieldMask = 0x00030000};
						enum {Value_NoAccess = 0x0};
						enum {ValueMask_NoAccess = 0x00000000};
						enum {Value_Client = 0x1};
						enum {ValueMask_Client = 0x00010000};
						enum {Value_Manager = 0x3};
						enum {ValueMask_Manager = 0x00030000};
						};
					namespace D7 { // Field Description
						enum {Lsb = 14};
						enum {FieldMask = 0x0000C000};
						enum {Value_NoAccess = 0x0};
						enum {ValueMask_NoAccess = 0x00000000};
						enum {Value_Client = 0x1};
						enum {ValueMask_Client = 0x00004000};
						enum {Value_Manager = 0x3};
						enum {ValueMask_Manager = 0x0000C000};
						};
					namespace D6 { // Field Description
						enum {Lsb = 12};
						enum {FieldMask = 0x00003000};
						enum {Value_NoAccess = 0x0};
						enum {ValueMask_NoAccess = 0x00000000};
						enum {Value_Client = 0x1};
						enum {ValueMask_Client = 0x00001000};
						enum {Value_Manager = 0x3};
						enum {ValueMask_Manager = 0x00003000};
						};
					namespace D5 { // Field Description
						enum {Lsb = 10};
						enum {FieldMask = 0x00000C00};
						enum {Value_NoAccess = 0x0};
						enum {ValueMask_NoAccess = 0x00000000};
						enum {Value_Client = 0x1};
						enum {ValueMask_Client = 0x00000400};
						enum {Value_Manager = 0x3};
						enum {ValueMask_Manager = 0x00000C00};
						};
					namespace D4 { // Field Description
						enum {Lsb = 8};
						enum {FieldMask = 0x00000300};
						enum {Value_NoAccess = 0x0};
						enum {ValueMask_NoAccess = 0x00000000};
						enum {Value_Client = 0x1};
						enum {ValueMask_Client = 0x00000100};
						enum {Value_Manager = 0x3};
						enum {ValueMask_Manager = 0x00000300};
						};
					namespace D3 { // Field Description
						enum {Lsb = 6};
						enum {FieldMask = 0x000000C0};
						enum {Value_NoAccess = 0x0};
						enum {ValueMask_NoAccess = 0x00000000};
						enum {Value_Client = 0x1};
						enum {ValueMask_Client = 0x00000040};
						enum {Value_Manager = 0x3};
						enum {ValueMask_Manager = 0x000000C0};
						};
					namespace D2 { // Field Description
						enum {Lsb = 4};
						enum {FieldMask = 0x00000030};
						enum {Value_NoAccess = 0x0};
						enum {ValueMask_NoAccess = 0x00000000};
						enum {Value_Client = 0x1};
						enum {ValueMask_Client = 0x00000010};
						enum {Value_Manager = 0x3};
						enum {ValueMask_Manager = 0x00000030};
						};
					namespace D1 { // Field Description
						enum {Lsb = 2};
						enum {FieldMask = 0x0000000C};
						enum {Value_NoAccess = 0x0};
						enum {ValueMask_NoAccess = 0x00000000};
						enum {Value_Client = 0x1};
						enum {ValueMask_Client = 0x00000004};
						enum {Value_Manager = 0x3};
						enum {ValueMask_Manager = 0x0000000C};
						};
					namespace D0 { // Field Description
						enum {Lsb = 0};
						enum {FieldMask = 0x00000003};
						enum {Value_NoAccess = 0x0};
						enum {ValueMask_NoAccess = 0x00000000};
						enum {Value_Client = 0x1};
						enum {ValueMask_Client = 0x00000001};
						enum {Value_Manager = 0x3};
						enum {ValueMask_Manager = 0x00000003};
						};
					};
				};
			};
		};
	};
#endif
