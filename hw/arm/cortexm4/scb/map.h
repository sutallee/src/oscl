/*
   Copyright (C) 2024 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/
#ifndef _oscl_hw_arm_cortexm4_scb_maph_
#define _oscl_hw_arm_cortexm4_scb_maph_
#include "reg.h"

/** */
namespace Oscl {
/** */
namespace Arm {
/** */
namespace CortexM4 {
/** */
namespace Scb {

/** 0xE000E008 */
struct Map {
	/** 0xE000E008, offset 0x00000000 */
	volatile Oscl::Arm::CortexM4::pSCB::rACTLR::Reg	actlr;

	/** 0xE000E00C, offset 0x00000004 */
	uint8_t	reservedE000ED10[0xE000ED00-0xE000E00C];

	/** 0xE000ED00, offset 0x00000D00 */
	volatile Oscl::Arm::CortexM4::pSCB::rCPUID::Reg	cpuid;

	/** 0xE000ED04, offset 0x00000CFC */
	volatile Oscl::Arm::CortexM4::pSCB::rICSR::Reg	icsr;

	/** 0xE000ED08, offset 0x00000D00 */
	volatile Oscl::Arm::CortexM4::pSCB::rVTOR::Reg	vtor;

	/** 0xE000ED0C, offset 0x00000D04 */
	volatile Oscl::Arm::CortexM4::pSCB::rAIRCR::Reg	aircr;

	/** 0xE000ED10, offset 0x00000D08 */
	volatile Oscl::Arm::CortexM4::pSCB::rSCR::Reg		scr;

	/** 0xE000ED14, offset 0x00000D0C */
	volatile Oscl::Arm::CortexM4::pSCB::rCCR::Reg		ccr;

	/** 0xE000ED18, offset 0x00000D10 */
	volatile Oscl::Arm::CortexM4::pSCB::rSHPR1::Reg		shpr1;

	/** 0xE000ED1C, offset 0x00000D14 */
	volatile Oscl::Arm::CortexM4::pSCB::rSHPR2::Reg		shpr2;

	/** 0xE000ED20, offset 0x00000D1C */
	volatile Oscl::Arm::CortexM4::pSCB::rSHPR3::Reg		shpr3;

	/** 0xE000ED24, offset 0x00000D20 */
	volatile Oscl::Arm::CortexM4::pSCB::rSHCSR::Reg		shcsr;

	/** 0xE000ED28, offset 0x00000D24 */
	volatile Oscl::Arm::CortexM4::pSCB::rCFSR::Reg		cfsr;

	/** 0xE000ED2C, offset 0x00000D28 */
	volatile Oscl::Arm::CortexM4::pSCB::rHFSR::Reg		hfsr;

	/** 0xE000ED30, offset 0x00000D28 */
	uint8_t	reservedE000ED30[0xE000ED34-0xE000ED30];

	/** 0xE000ED34, offset 0x00000D2C */
	volatile Oscl::Arm::CortexM4::pSCB::rMMFAR::Reg		mmfar;

	/** 0xE000ED38, offset 0x00000D30 */
	volatile Oscl::Arm::CortexM4::pSCB::rBFAR::Reg		bfar;

	/** 0xE000ED3C, offset 0x00000D34 */
	volatile Oscl::Arm::CortexM4::pSCB::rAFSR::Reg		asfr;
	};

}
}
}
}

#endif
