/*
   Copyright (C) 2011 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_hw_arm_cortexm4_maph_
#define _oscl_hw_arm_cortexm4_maph_
#include <stdint.h>
#include "oscl/hw/arm/cortexm4/fpu/map.h"
#include "oscl/hw/arm/cortexm4/scb/map.h"

extern Oscl::Arm::CortexM4::Fpu::Map	CortexM4FPU;
extern Oscl::Arm::CortexM4::Scb::Map	CortexM4SCB;

extern volatile uint32_t	NVIC_ISER[8];
extern volatile uint32_t	NVIC_ICER[8];
extern volatile uint32_t	NVIC_ISPR[8];
extern volatile uint32_t	NVIC_ICPR[8];
extern volatile uint32_t	NVIC_IABR[8];
extern volatile uint32_t	NVIC_IPR[14];
extern volatile uint32_t	NVIC_STIR;

namespace Oscl {
namespace Arm {
namespace CortexM4 {

inline void	enableInterrupt( unsigned n ) {
	unsigned	iserOffset = n/32;
	unsigned	bitOffset	= n%32;

	NVIC_ISER[ iserOffset ]	= (1UL << bitOffset );
	}

inline void	disableInterrupt( unsigned n ) {
	unsigned	iserOffset = n/32;
	unsigned	bitOffset	= n%32;

	NVIC_ICER[ iserOffset ]	= (1UL << bitOffset );
	}

inline void	clearPendingInterrupt( unsigned n ) {
	unsigned	iserOffset = n/32;
	unsigned	bitOffset	= n%32;

	NVIC_ICPR[ iserOffset ]	= (1UL << bitOffset );

	}

}
}
}

#endif
