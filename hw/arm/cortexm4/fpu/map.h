/*
   Copyright (C) 2024 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_hw_arm_cortexm4_fpu_maph_
#define _oscl_hw_arm_cortexm4_fpu_maph_
#include <stdint.h>
#include "fpu.h"

namespace Oscl {
namespace Arm {
namespace CortexM4 {
namespace Fpu {

/* Base address: 0xE000ED88 */
struct Map {
	/** 0xE000ED88, offset: 0x00000000 */
	volatile rCPACR::Reg	cpacr;

	/** 0xE000ED8C, offset: 0x00000004 */
	uint8_t	reservedE000ED8C[0xE000EF34 - 0xE000ED8C];

	/** 0xE000EF34, offset: 0x000001AC */
	volatile rFPCCR::Reg	fpccr;

	/** 0xE000EF38, offset: 0x000001B0 */
	volatile rFPACR::Reg	fpacr;

	/*	NOTE: The FPSCR is NOT memory mapped, but is
		accessed using the VMSR and VMRS instructions.
	 */

	/** 0xE000EF3C, offset: 0x000001AC */
	volatile rFPDSCR::Reg	fpdscr;
	};

}
}
}
}

#endif
