#ifndef _oscl_hw_arm_cortexm4_fpu_regh_
#define _oscl_hw_arm_cortexm4_fpu_regh_
#include <stdint.h>
namespace Oscl { // Namespace description
	namespace Arm { // Namespace description
		namespace CortexM4 { // Namespace description
			namespace Fpu { // Namespace description
				namespace rCPACR { // Register description
					typedef uint32_t	Reg;
					namespace fCP10 { // Field Description
						constexpr Reg Lsb = 20;
						constexpr Reg FieldMask = 0x00300000;
						constexpr Reg Value_AccessDenied = 0x0;
						constexpr Reg ValueMask_AccessDenied = 0x00000000;
						constexpr Reg Value_PrivilegedAccessOnly = 0x1;
						constexpr Reg ValueMask_PrivilegedAccessOnly = 0x00100000;
						constexpr Reg Value_FullAccess = 0x3;
						constexpr Reg ValueMask_FullAccess = 0x00300000;
						};
					namespace fCP11 { // Field Description
						constexpr Reg Lsb = 22;
						constexpr Reg FieldMask = 0x00C00000;
						constexpr Reg Value_AccessDenied = 0x0;
						constexpr Reg ValueMask_AccessDenied = 0x00000000;
						constexpr Reg Value_PrivilegedAccessOnly = 0x1;
						constexpr Reg ValueMask_PrivilegedAccessOnly = 0x00400000;
						constexpr Reg Value_FullAccess = 0x3;
						constexpr Reg ValueMask_FullAccess = 0x00C00000;
						};
					};
				namespace rFPCCR { // Register description
					typedef uint32_t	Reg;
					namespace fLSPACT { // Field Description
						constexpr Reg Lsb = 0;
						constexpr Reg FieldMask = 0x00000001;
						constexpr Reg Value_NotActive = 0x0;
						constexpr Reg ValueMask_NotActive = 0x00000000;
						constexpr Reg Value_Active = 0x1;
						constexpr Reg ValueMask_Active = 0x00000001;
						};
					namespace fUSER { // Field Description
						constexpr Reg Lsb = 1;
						constexpr Reg FieldMask = 0x00000002;
						constexpr Reg Value_NotUserPriveledge = 0x0;
						constexpr Reg ValueMask_NotUserPriveledge = 0x00000000;
						constexpr Reg Value_UserPriveledge = 0x1;
						constexpr Reg ValueMask_UserPriveledge = 0x00000002;
						};
					namespace fTHREAD { // Field Description
						constexpr Reg Lsb = 3;
						constexpr Reg FieldMask = 0x00000008;
						constexpr Reg Value_NotThreadMode = 0x0;
						constexpr Reg ValueMask_NotThreadMode = 0x00000000;
						constexpr Reg Value_ThreadMode = 0x1;
						constexpr Reg ValueMask_ThreadMode = 0x00000008;
						};
					namespace fHFRDY { // Field Description
						constexpr Reg Lsb = 4;
						constexpr Reg FieldMask = 0x00000010;
						constexpr Reg Value_NotPermitted = 0x0;
						constexpr Reg ValueMask_NotPermitted = 0x00000000;
						constexpr Reg Value_Permitted = 0x1;
						constexpr Reg ValueMask_Permitted = 0x00000010;
						};
					namespace fMMRDY { // Field Description
						constexpr Reg Lsb = 5;
						constexpr Reg FieldMask = 0x00000020;
						constexpr Reg Value_DisabledOrNotPermitted = 0x0;
						constexpr Reg ValueMask_DisabledOrNotPermitted = 0x00000000;
						constexpr Reg Value_EnabledAndPermitted = 0x1;
						constexpr Reg ValueMask_EnabledAndPermitted = 0x00000020;
						};
					namespace fBFRDY { // Field Description
						constexpr Reg Lsb = 6;
						constexpr Reg FieldMask = 0x00000040;
						constexpr Reg Value_DisabledOrNotPermitted = 0x0;
						constexpr Reg ValueMask_DisabledOrNotPermitted = 0x00000000;
						constexpr Reg Value_EnabledAndPermitted = 0x1;
						constexpr Reg ValueMask_EnabledAndPermitted = 0x00000040;
						};
					namespace fMONRDY { // Field Description
						constexpr Reg Lsb = 8;
						constexpr Reg FieldMask = 0x00000100;
						constexpr Reg Value_DisabledOrNotPermitted = 0x0;
						constexpr Reg ValueMask_DisabledOrNotPermitted = 0x00000000;
						constexpr Reg Value_EnabledAndPermitted = 0x1;
						constexpr Reg ValueMask_EnabledAndPermitted = 0x00000100;
						};
					namespace fLSPEN { // Field Description
						constexpr Reg Lsb = 30;
						constexpr Reg FieldMask = 0x40000000;
						constexpr Reg Value_Disable = 0x0;
						constexpr Reg ValueMask_Disable = 0x00000000;
						constexpr Reg Value_Enable = 0x1;
						constexpr Reg ValueMask_Enable = 0x40000000;
						constexpr Reg Value_Disabled = 0x0;
						constexpr Reg ValueMask_Disabled = 0x00000000;
						constexpr Reg Value_Enabled = 0x1;
						constexpr Reg ValueMask_Enabled = 0x40000000;
						};
					namespace fASPEN { // Field Description
						constexpr Reg Lsb = 31;
						constexpr Reg FieldMask = 0x80000000;
						constexpr Reg Value_Disable = 0x0;
						constexpr Reg ValueMask_Disable = 0x00000000;
						constexpr Reg Value_Enable = 0x1;
						constexpr Reg ValueMask_Enable = 0x80000000;
						constexpr Reg Value_Disabled = 0x0;
						constexpr Reg ValueMask_Disabled = 0x00000000;
						constexpr Reg Value_Enabled = 0x1;
						constexpr Reg ValueMask_Enabled = 0x80000000;
						};
					};
				namespace rFPACR { // Register description
					typedef uint32_t	Reg;
					namespace fADDRESS { // Field Description
						constexpr Reg Lsb = 3;
						constexpr Reg FieldMask = 0xFFFFFFF8;
						};
					};
				namespace rFPSCR { // Register description
					typedef uint32_t	Reg;
					namespace fIOC { // Field Description
						constexpr Reg Lsb = 0;
						constexpr Reg FieldMask = 0x00000001;
						constexpr Reg Value_InvalidOperationException = 0x1;
						constexpr Reg ValueMask_InvalidOperationException = 0x00000001;
						};
					namespace fDZC { // Field Description
						constexpr Reg Lsb = 1;
						constexpr Reg FieldMask = 0x00000002;
						constexpr Reg Value_DivisionByZeroException = 0x1;
						constexpr Reg ValueMask_DivisionByZeroException = 0x00000002;
						};
					namespace fOFC { // Field Description
						constexpr Reg Lsb = 2;
						constexpr Reg FieldMask = 0x00000004;
						constexpr Reg Value_OverflowException = 0x1;
						constexpr Reg ValueMask_OverflowException = 0x00000004;
						};
					namespace fUFC { // Field Description
						constexpr Reg Lsb = 3;
						constexpr Reg FieldMask = 0x00000008;
						constexpr Reg Value_UnderflowException = 0x1;
						constexpr Reg ValueMask_UnderflowException = 0x00000008;
						};
					namespace fIXC { // Field Description
						constexpr Reg Lsb = 4;
						constexpr Reg FieldMask = 0x00000010;
						constexpr Reg Value_InexactException = 0x1;
						constexpr Reg ValueMask_InexactException = 0x00000010;
						};
					namespace fIDC { // Field Description
						constexpr Reg Lsb = 7;
						constexpr Reg FieldMask = 0x00000080;
						constexpr Reg Value_Exception = 0x1;
						constexpr Reg ValueMask_Exception = 0x00000080;
						};
					namespace fRMode { // Field Description
						constexpr Reg Lsb = 22;
						constexpr Reg FieldMask = 0x00C00000;
						constexpr Reg Value_RoundToNearest = 0x0;
						constexpr Reg ValueMask_RoundToNearest = 0x00000000;
						constexpr Reg Value_RoundTowardsPlusInfinity = 0x1;
						constexpr Reg ValueMask_RoundTowardsPlusInfinity = 0x00400000;
						constexpr Reg Value_RoundTowardsMinusInfinity = 0x2;
						constexpr Reg ValueMask_RoundTowardsMinusInfinity = 0x00800000;
						constexpr Reg Value_RoundTowardsZero = 0x3;
						constexpr Reg ValueMask_RoundTowardsZero = 0x00C00000;
						};
					namespace fFZ { // Field Description
						constexpr Reg Lsb = 24;
						constexpr Reg FieldMask = 0x01000000;
						constexpr Reg Value_FlushToZeroDisable = 0x0;
						constexpr Reg ValueMask_FlushToZeroDisable = 0x00000000;
						constexpr Reg Value_FlushToZeroEnable = 0x1;
						constexpr Reg ValueMask_FlushToZeroEnable = 0x01000000;
						constexpr Reg Value_FlushToZeroDisabled = 0x0;
						constexpr Reg ValueMask_FlushToZeroDisabled = 0x00000000;
						constexpr Reg Value_FlushToZeroEnabled = 0x1;
						constexpr Reg ValueMask_FlushToZeroEnabled = 0x01000000;
						};
					namespace fDN { // Field Description
						constexpr Reg Lsb = 25;
						constexpr Reg FieldMask = 0x02000000;
						constexpr Reg Value_NaNOperandsPropagate = 0x0;
						constexpr Reg ValueMask_NaNOperandsPropagate = 0x00000000;
						constexpr Reg Value_NaNReturnsDefault = 0x1;
						constexpr Reg ValueMask_NaNReturnsDefault = 0x02000000;
						};
					namespace fAHP { // Field Description
						constexpr Reg Lsb = 26;
						constexpr Reg FieldMask = 0x04000000;
						constexpr Reg Value_IEEE = 0x0;
						constexpr Reg ValueMask_IEEE = 0x00000000;
						constexpr Reg Value_Alternative = 0x1;
						constexpr Reg ValueMask_Alternative = 0x04000000;
						};
					namespace fV { // Field Description
						constexpr Reg Lsb = 28;
						constexpr Reg FieldMask = 0x10000000;
						constexpr Reg Value_NotOverflow = 0x0;
						constexpr Reg ValueMask_NotOverflow = 0x00000000;
						constexpr Reg Value_Overflow = 0x1;
						constexpr Reg ValueMask_Overflow = 0x10000000;
						};
					namespace fC { // Field Description
						constexpr Reg Lsb = 29;
						constexpr Reg FieldMask = 0x20000000;
						constexpr Reg Value_NotCarry = 0x0;
						constexpr Reg ValueMask_NotCarry = 0x00000000;
						constexpr Reg Value_Carry = 0x1;
						constexpr Reg ValueMask_Carry = 0x20000000;
						};
					namespace fZ { // Field Description
						constexpr Reg Lsb = 30;
						constexpr Reg FieldMask = 0x40000000;
						constexpr Reg Value_NotZero = 0x0;
						constexpr Reg ValueMask_NotZero = 0x00000000;
						constexpr Reg Value_Zero = 0x1;
						constexpr Reg ValueMask_Zero = 0x40000000;
						};
					namespace fN { // Field Description
						constexpr Reg Lsb = 31;
						constexpr Reg FieldMask = 0x80000000;
						constexpr Reg Value_IsPositive = 0x0;
						constexpr Reg ValueMask_IsPositive = 0x00000000;
						constexpr Reg Value_IsNegative = 0x1;
						constexpr Reg ValueMask_IsNegative = 0x80000000;
						};
					};
				namespace rFPDSCR { // Register description
					typedef uint32_t	Reg;
					namespace fRMode { // Field Description
						constexpr Reg Lsb = 22;
						constexpr Reg FieldMask = 0x00C00000;
						constexpr Reg Value_Default = 0x0;
						constexpr Reg ValueMask_Default = 0x00000000;
						constexpr Reg Value_RoundToNearest = 0x0;
						constexpr Reg ValueMask_RoundToNearest = 0x00000000;
						constexpr Reg Value_RoundTowardsPlusInfinity = 0x1;
						constexpr Reg ValueMask_RoundTowardsPlusInfinity = 0x00400000;
						constexpr Reg Value_RoundTowardsMinusInfinity = 0x2;
						constexpr Reg ValueMask_RoundTowardsMinusInfinity = 0x00800000;
						constexpr Reg Value_RoundTowardsZero = 0x3;
						constexpr Reg ValueMask_RoundTowardsZero = 0x00C00000;
						};
					namespace fFZ { // Field Description
						constexpr Reg Lsb = 24;
						constexpr Reg FieldMask = 0x01000000;
						constexpr Reg Value_Default = 0x0;
						constexpr Reg ValueMask_Default = 0x00000000;
						constexpr Reg Value_FlushToZeroDisabled = 0x0;
						constexpr Reg ValueMask_FlushToZeroDisabled = 0x00000000;
						constexpr Reg Value_FlushToZeroEnabled = 0x1;
						constexpr Reg ValueMask_FlushToZeroEnabled = 0x01000000;
						};
					namespace fDN { // Field Description
						constexpr Reg Lsb = 25;
						constexpr Reg FieldMask = 0x02000000;
						constexpr Reg Value_Default = 0x0;
						constexpr Reg ValueMask_Default = 0x00000000;
						constexpr Reg Value_NaNOperandsPropagate = 0x0;
						constexpr Reg ValueMask_NaNOperandsPropagate = 0x00000000;
						constexpr Reg Value_NaNReturnsDefault = 0x1;
						constexpr Reg ValueMask_NaNReturnsDefault = 0x02000000;
						};
					namespace fAHP { // Field Description
						constexpr Reg Lsb = 26;
						constexpr Reg FieldMask = 0x04000000;
						constexpr Reg Value_Default = 0x0;
						constexpr Reg ValueMask_Default = 0x00000000;
						constexpr Reg Value_IEEE = 0x0;
						constexpr Reg ValueMask_IEEE = 0x00000000;
						constexpr Reg Value_Alternative = 0x1;
						constexpr Reg ValueMask_Alternative = 0x04000000;
						};
					};
				}
			}
		}
	}
#endif
