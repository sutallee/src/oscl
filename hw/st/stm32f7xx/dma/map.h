#warning "This file is obsolete and will be removed. Use oscl/hw/st/stm32/f2f4f7/dma instead."
/*
   Copyright (C) 2021 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_hw_st_stm32f7xx_dma_maph_
#define _oscl_hw_st_stm32f7xx_dma_maph_

#include "reg.h"

/** */
namespace Oscl {

/** */
namespace St {

/** */
namespace Stm32F7xx {

/** */
namespace Dma {

/** */
struct Stream {
	/** Offset 0x000 */
	volatile Oscl::St::Stm32F7xx::Dma::Sxcr::Reg	sxcr;

	/** Offset 0x004 */
	volatile Oscl::St::Stm32F7xx::Dma::Sxndtr::Reg	sxndtr;

	/** Offset 0x008 */
	volatile Oscl::St::Stm32F7xx::Dma::Sxpar::Reg	sxpar;

	/** Offset 0x00C */
	volatile Oscl::St::Stm32F7xx::Dma::Sxm0ar::Reg	sxm0ar;

	/** Offset 0x010 */
	volatile Oscl::St::Stm32F7xx::Dma::Sxm1ar::Reg	sxm1ar;

	/** Offset 0x014 */
	volatile Oscl::St::Stm32F7xx::Dma::Sxfcr::Reg	sxfcr;
	};

/** */
struct Map {
	/** Offset 0x000 */
	volatile Oscl::St::Stm32F7xx::Dma::StreamIsr::Reg	lisr;

	/** Offset 0x004 */
	volatile Oscl::St::Stm32F7xx::Dma::StreamIsr::Reg	hisr;

	/** Offset 0x008 */
	volatile Oscl::St::Stm32F7xx::Dma::StreamIfcr::Reg	lifcr;

	/** Offset 0x00C */
	volatile Oscl::St::Stm32F7xx::Dma::StreamIfcr::Reg	hifcr;

	/** Offset 0x010 */
	Stream	stream[8];
	};

}
}
}
}

#endif
