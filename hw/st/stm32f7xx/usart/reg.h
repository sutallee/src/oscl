#ifndef _oscl_hw_st_stm32f7xx_usart_regh_
#define _oscl_hw_st_stm32f7xx_usart_regh_
#include <stdint.h>
namespace Oscl { // Namespace description
	namespace St { // Namespace description
		namespace Stm32F7xx { // Namespace description
			namespace Usart { // Namespace description
				namespace Cr1 { // Register description
					typedef uint32_t	Reg;
					namespace ue { // Field Description
						constexpr Reg Lsb = 0;
						constexpr Reg FieldMask = 0x00000001;
						constexpr Reg Value_default = 0x0;
						constexpr Reg ValueMask_default = 0x00000000;
						constexpr Reg Value_disable = 0x0;
						constexpr Reg ValueMask_disable = 0x00000000;
						constexpr Reg Value_enable = 0x1;
						constexpr Reg ValueMask_enable = 0x00000001;
						};
					namespace uesm { // Field Description
						constexpr Reg Lsb = 1;
						constexpr Reg FieldMask = 0x00000002;
						constexpr Reg Value_default = 0x0;
						constexpr Reg ValueMask_default = 0x00000000;
						constexpr Reg Value_disableWakeFromStopMode = 0x0;
						constexpr Reg ValueMask_disableWakeFromStopMode = 0x00000000;
						constexpr Reg Value_enableWakeFromStopMode = 0x1;
						constexpr Reg ValueMask_enableWakeFromStopMode = 0x00000002;
						};
					namespace re { // Field Description
						constexpr Reg Lsb = 2;
						constexpr Reg FieldMask = 0x00000004;
						constexpr Reg Value_default = 0x0;
						constexpr Reg ValueMask_default = 0x00000000;
						constexpr Reg Value_receiverDisable = 0x0;
						constexpr Reg ValueMask_receiverDisable = 0x00000000;
						constexpr Reg Value_receiverEnable = 0x1;
						constexpr Reg ValueMask_receiverEnable = 0x00000004;
						};
					namespace te { // Field Description
						constexpr Reg Lsb = 3;
						constexpr Reg FieldMask = 0x00000008;
						constexpr Reg Value_default = 0x0;
						constexpr Reg ValueMask_default = 0x00000000;
						constexpr Reg Value_transmitterDisable = 0x0;
						constexpr Reg ValueMask_transmitterDisable = 0x00000000;
						constexpr Reg Value_transmitterEnable = 0x1;
						constexpr Reg ValueMask_transmitterEnable = 0x00000008;
						};
					namespace idleie { // Field Description
						constexpr Reg Lsb = 4;
						constexpr Reg FieldMask = 0x00000010;
						constexpr Reg Value_default = 0x0;
						constexpr Reg ValueMask_default = 0x00000000;
						constexpr Reg Value_interruptDisable = 0x0;
						constexpr Reg ValueMask_interruptDisable = 0x00000000;
						constexpr Reg Value_interruptEnable = 0x1;
						constexpr Reg ValueMask_interruptEnable = 0x00000010;
						};
					namespace rxneie { // Field Description
						constexpr Reg Lsb = 5;
						constexpr Reg FieldMask = 0x00000020;
						constexpr Reg Value_default = 0x0;
						constexpr Reg ValueMask_default = 0x00000000;
						constexpr Reg Value_interruptDisable = 0x0;
						constexpr Reg ValueMask_interruptDisable = 0x00000000;
						constexpr Reg Value_interruptEnable = 0x1;
						constexpr Reg ValueMask_interruptEnable = 0x00000020;
						};
					namespace tcie { // Field Description
						constexpr Reg Lsb = 6;
						constexpr Reg FieldMask = 0x00000040;
						constexpr Reg Value_default = 0x0;
						constexpr Reg ValueMask_default = 0x00000000;
						constexpr Reg Value_interruptDisable = 0x0;
						constexpr Reg ValueMask_interruptDisable = 0x00000000;
						constexpr Reg Value_interruptEnable = 0x1;
						constexpr Reg ValueMask_interruptEnable = 0x00000040;
						};
					namespace txeie { // Field Description
						constexpr Reg Lsb = 7;
						constexpr Reg FieldMask = 0x00000080;
						constexpr Reg Value_default = 0x0;
						constexpr Reg ValueMask_default = 0x00000000;
						constexpr Reg Value_interruptDisable = 0x0;
						constexpr Reg ValueMask_interruptDisable = 0x00000000;
						constexpr Reg Value_interruptEnable = 0x1;
						constexpr Reg ValueMask_interruptEnable = 0x00000080;
						};
					namespace peie { // Field Description
						constexpr Reg Lsb = 8;
						constexpr Reg FieldMask = 0x00000100;
						constexpr Reg Value_default = 0x0;
						constexpr Reg ValueMask_default = 0x00000000;
						constexpr Reg Value_interruptDisable = 0x0;
						constexpr Reg ValueMask_interruptDisable = 0x00000000;
						constexpr Reg Value_interruptEnable = 0x1;
						constexpr Reg ValueMask_interruptEnable = 0x00000100;
						};
					namespace ps { // Field Description
						constexpr Reg Lsb = 9;
						constexpr Reg FieldMask = 0x00000200;
						constexpr Reg Value_default = 0x0;
						constexpr Reg ValueMask_default = 0x00000000;
						constexpr Reg Value_evenParity = 0x0;
						constexpr Reg ValueMask_evenParity = 0x00000000;
						constexpr Reg Value_oddParity = 0x1;
						constexpr Reg ValueMask_oddParity = 0x00000200;
						};
					namespace pce { // Field Description
						constexpr Reg Lsb = 10;
						constexpr Reg FieldMask = 0x00000400;
						constexpr Reg Value_default = 0x0;
						constexpr Reg ValueMask_default = 0x00000000;
						constexpr Reg Value_parityDisable = 0x0;
						constexpr Reg ValueMask_parityDisable = 0x00000000;
						constexpr Reg Value_parityEnable = 0x1;
						constexpr Reg ValueMask_parityEnable = 0x00000400;
						};
					namespace wake { // Field Description
						constexpr Reg Lsb = 11;
						constexpr Reg FieldMask = 0x00000800;
						constexpr Reg Value_default = 0x0;
						constexpr Reg ValueMask_default = 0x00000000;
						constexpr Reg Value_idleLine = 0x0;
						constexpr Reg ValueMask_idleLine = 0x00000000;
						constexpr Reg Value_addressMark = 0x1;
						constexpr Reg ValueMask_addressMark = 0x00000800;
						};
					namespace m0 { // Field Description
						constexpr Reg Lsb = 12;
						constexpr Reg FieldMask = 0x00001000;
						constexpr Reg Value_default = 0x0;
						constexpr Reg ValueMask_default = 0x00000000;
						constexpr Reg Value_78bits = 0x0;
						constexpr Reg ValueMask_78bits = 0x00000000;
						constexpr Reg Value_9bits = 0x1;
						constexpr Reg ValueMask_9bits = 0x00001000;
						};
					namespace mme { // Field Description
						constexpr Reg Lsb = 13;
						constexpr Reg FieldMask = 0x00002000;
						constexpr Reg Value_default = 0x0;
						constexpr Reg ValueMask_default = 0x00000000;
						constexpr Reg Value_alwaysActive = 0x0;
						constexpr Reg ValueMask_alwaysActive = 0x00000000;
						constexpr Reg Value_muteByWAKE = 0x1;
						constexpr Reg ValueMask_muteByWAKE = 0x00002000;
						};
					namespace cmie { // Field Description
						constexpr Reg Lsb = 14;
						constexpr Reg FieldMask = 0x00004000;
						constexpr Reg Value_default = 0x0;
						constexpr Reg ValueMask_default = 0x00000000;
						constexpr Reg Value_interruptDisable = 0x0;
						constexpr Reg ValueMask_interruptDisable = 0x00000000;
						constexpr Reg Value_interruptEnable = 0x1;
						constexpr Reg ValueMask_interruptEnable = 0x00004000;
						};
					namespace over8 { // Field Description
						constexpr Reg Lsb = 15;
						constexpr Reg FieldMask = 0x00008000;
						constexpr Reg Value_default = 0x0;
						constexpr Reg ValueMask_default = 0x00000000;
						constexpr Reg Value_oversamplingBy16 = 0x0;
						constexpr Reg ValueMask_oversamplingBy16 = 0x00000000;
						constexpr Reg Value_oversamplingBy8 = 0x1;
						constexpr Reg ValueMask_oversamplingBy8 = 0x00008000;
						};
					namespace dedt { // Field Description
						constexpr Reg Lsb = 16;
						constexpr Reg FieldMask = 0x001F0000;
						constexpr Reg Value_default = 0x0;
						constexpr Reg ValueMask_default = 0x00000000;
						constexpr Reg Value_minDriverEnableNegationTimeInSampleUnits = 0x0;
						constexpr Reg ValueMask_minDriverEnableNegationTimeInSampleUnits = 0x00000000;
						constexpr Reg Value_maxDriverEnableNegationTimeInSampleUnits = 0x1F;
						constexpr Reg ValueMask_maxDriverEnableNegationTimeInSampleUnits = 0x001F0000;
						};
					namespace deat { // Field Description
						constexpr Reg Lsb = 21;
						constexpr Reg FieldMask = 0x03E00000;
						constexpr Reg Value_default = 0x0;
						constexpr Reg ValueMask_default = 0x00000000;
						constexpr Reg Value_minDriverEnableAssertionTimeInSampleUnits = 0x0;
						constexpr Reg ValueMask_minDriverEnableAssertionTimeInSampleUnits = 0x00000000;
						constexpr Reg Value_maxDriverEnableAssertionTimeInSampleUnits = 0x1F;
						constexpr Reg ValueMask_maxDriverEnableAssertionTimeInSampleUnits = 0x03E00000;
						};
					namespace rtoie { // Field Description
						constexpr Reg Lsb = 26;
						constexpr Reg FieldMask = 0x04000000;
						constexpr Reg Value_default = 0x0;
						constexpr Reg ValueMask_default = 0x00000000;
						constexpr Reg Value_interruptDisable = 0x0;
						constexpr Reg ValueMask_interruptDisable = 0x00000000;
						constexpr Reg Value_interruptEnable = 0x1;
						constexpr Reg ValueMask_interruptEnable = 0x04000000;
						};
					namespace eobie { // Field Description
						constexpr Reg Lsb = 27;
						constexpr Reg FieldMask = 0x08000000;
						constexpr Reg Value_default = 0x0;
						constexpr Reg ValueMask_default = 0x00000000;
						constexpr Reg Value_interruptDisable = 0x0;
						constexpr Reg ValueMask_interruptDisable = 0x00000000;
						constexpr Reg Value_interruptEnable = 0x1;
						constexpr Reg ValueMask_interruptEnable = 0x08000000;
						};
					namespace m1 { // Field Description
						constexpr Reg Lsb = 28;
						constexpr Reg FieldMask = 0x10000000;
						constexpr Reg Value_default = 0x0;
						constexpr Reg ValueMask_default = 0x00000000;
						constexpr Reg Value_89bits = 0x0;
						constexpr Reg ValueMask_89bits = 0x00000000;
						constexpr Reg Value_7bits = 0x1;
						constexpr Reg ValueMask_7bits = 0x10000000;
						};
					};
				namespace Cr2 { // Register description
					typedef uint32_t	Reg;
					namespace addm7 { // Field Description
						constexpr Reg Lsb = 4;
						constexpr Reg FieldMask = 0x00000010;
						constexpr Reg Value_default = 0x0;
						constexpr Reg ValueMask_default = 0x00000000;
						constexpr Reg Value_4BitAddress = 0x0;
						constexpr Reg ValueMask_4BitAddress = 0x00000000;
						constexpr Reg Value_7BitAddress = 0x1;
						constexpr Reg ValueMask_7BitAddress = 0x00000010;
						};
					namespace lbdl { // Field Description
						constexpr Reg Lsb = 5;
						constexpr Reg FieldMask = 0x00000020;
						constexpr Reg Value_default = 0x0;
						constexpr Reg ValueMask_default = 0x00000000;
						constexpr Reg Value_linBreak10Bit = 0x0;
						constexpr Reg ValueMask_linBreak10Bit = 0x00000000;
						constexpr Reg Value_linBreak11Bit = 0x1;
						constexpr Reg ValueMask_linBreak11Bit = 0x00000020;
						};
					namespace lbdie { // Field Description
						constexpr Reg Lsb = 6;
						constexpr Reg FieldMask = 0x00000040;
						constexpr Reg Value_default = 0x0;
						constexpr Reg ValueMask_default = 0x00000000;
						constexpr Reg Value_linBreakInterruptDisable = 0x0;
						constexpr Reg ValueMask_linBreakInterruptDisable = 0x00000000;
						constexpr Reg Value_linBreakInterruptEnable = 0x1;
						constexpr Reg ValueMask_linBreakInterruptEnable = 0x00000040;
						};
					namespace lbcl { // Field Description
						constexpr Reg Lsb = 8;
						constexpr Reg FieldMask = 0x00000100;
						constexpr Reg Value_default = 0x0;
						constexpr Reg ValueMask_default = 0x00000000;
						constexpr Reg Value_noOutputCkOnLastDataBit = 0x0;
						constexpr Reg ValueMask_noOutputCkOnLastDataBit = 0x00000000;
						constexpr Reg Value_outputCkOnLastDataBit = 0x1;
						constexpr Reg ValueMask_outputCkOnLastDataBit = 0x00000100;
						};
					namespace cpha { // Field Description
						constexpr Reg Lsb = 9;
						constexpr Reg FieldMask = 0x00000200;
						constexpr Reg Value_default = 0x0;
						constexpr Reg ValueMask_default = 0x00000000;
						constexpr Reg Value_firstClock = 0x0;
						constexpr Reg ValueMask_firstClock = 0x00000000;
						constexpr Reg Value_secondClock = 0x1;
						constexpr Reg ValueMask_secondClock = 0x00000200;
						};
					namespace cpol { // Field Description
						constexpr Reg Lsb = 10;
						constexpr Reg FieldMask = 0x00000400;
						constexpr Reg Value_default = 0x0;
						constexpr Reg ValueMask_default = 0x00000000;
						constexpr Reg Value_low = 0x0;
						constexpr Reg ValueMask_low = 0x00000000;
						constexpr Reg Value_high = 0x1;
						constexpr Reg ValueMask_high = 0x00000400;
						};
					namespace clken { // Field Description
						constexpr Reg Lsb = 11;
						constexpr Reg FieldMask = 0x00000800;
						constexpr Reg Value_default = 0x0;
						constexpr Reg ValueMask_default = 0x00000000;
						constexpr Reg Value_ckPinDisable = 0x0;
						constexpr Reg ValueMask_ckPinDisable = 0x00000000;
						constexpr Reg Value_ckPinEnable = 0x1;
						constexpr Reg ValueMask_ckPinEnable = 0x00000800;
						};
					namespace stop { // Field Description
						constexpr Reg Lsb = 12;
						constexpr Reg FieldMask = 0x00003000;
						constexpr Reg Value_default = 0x0;
						constexpr Reg ValueMask_default = 0x00000000;
						constexpr Reg Value_1StopBit = 0x0;
						constexpr Reg ValueMask_1StopBit = 0x00000000;
						constexpr Reg Value_0_5StopBit = 0x1;
						constexpr Reg ValueMask_0_5StopBit = 0x00001000;
						constexpr Reg Value_2StopBit = 0x2;
						constexpr Reg ValueMask_2StopBit = 0x00002000;
						constexpr Reg Value_1_5topBit = 0x3;
						constexpr Reg ValueMask_1_5topBit = 0x00003000;
						};
					namespace linen { // Field Description
						constexpr Reg Lsb = 14;
						constexpr Reg FieldMask = 0x00004000;
						constexpr Reg Value_default = 0x0;
						constexpr Reg ValueMask_default = 0x00000000;
						constexpr Reg Value_linModeDisable = 0x0;
						constexpr Reg ValueMask_linModeDisable = 0x00000000;
						constexpr Reg Value_linModeEnable = 0x0;
						constexpr Reg ValueMask_linModeEnable = 0x00000000;
						};
					namespace swap { // Field Description
						constexpr Reg Lsb = 15;
						constexpr Reg FieldMask = 0x00008000;
						constexpr Reg Value_default = 0x0;
						constexpr Reg ValueMask_default = 0x00000000;
						constexpr Reg Value_standardTxRxPins = 0x0;
						constexpr Reg ValueMask_standardTxRxPins = 0x00000000;
						constexpr Reg Value_swappedTxRxPins = 0x1;
						constexpr Reg ValueMask_swappedTxRxPins = 0x00008000;
						};
					namespace rxinv { // Field Description
						constexpr Reg Lsb = 16;
						constexpr Reg FieldMask = 0x00010000;
						constexpr Reg Value_default = 0x0;
						constexpr Reg ValueMask_default = 0x00000000;
						constexpr Reg Value_standardLogic = 0x0;
						constexpr Reg ValueMask_standardLogic = 0x00000000;
						constexpr Reg Value_invertedLogic = 0x1;
						constexpr Reg ValueMask_invertedLogic = 0x00010000;
						};
					namespace txinv { // Field Description
						constexpr Reg Lsb = 17;
						constexpr Reg FieldMask = 0x00020000;
						constexpr Reg Value_default = 0x0;
						constexpr Reg ValueMask_default = 0x00000000;
						constexpr Reg Value_standardLogic = 0x0;
						constexpr Reg ValueMask_standardLogic = 0x00000000;
						constexpr Reg Value_invertedLogic = 0x1;
						constexpr Reg ValueMask_invertedLogic = 0x00020000;
						};
					namespace datainv { // Field Description
						constexpr Reg Lsb = 18;
						constexpr Reg FieldMask = 0x00040000;
						constexpr Reg Value_default = 0x0;
						constexpr Reg ValueMask_default = 0x00000000;
						constexpr Reg Value_positiveLogic = 0x0;
						constexpr Reg ValueMask_positiveLogic = 0x00000000;
						constexpr Reg Value_negativeLogic = 0x1;
						constexpr Reg ValueMask_negativeLogic = 0x00040000;
						};
					namespace msbfirst { // Field Description
						constexpr Reg Lsb = 19;
						constexpr Reg FieldMask = 0x00080000;
						constexpr Reg Value_default = 0x0;
						constexpr Reg ValueMask_default = 0x00000000;
						constexpr Reg Value_lsbFirst = 0x0;
						constexpr Reg ValueMask_lsbFirst = 0x00000000;
						constexpr Reg Value_msbFirst = 0x1;
						constexpr Reg ValueMask_msbFirst = 0x00080000;
						};
					namespace abren { // Field Description
						constexpr Reg Lsb = 20;
						constexpr Reg FieldMask = 0x00100000;
						constexpr Reg Value_default = 0x0;
						constexpr Reg ValueMask_default = 0x00000000;
						constexpr Reg Value_autoBaudDisable = 0x0;
						constexpr Reg ValueMask_autoBaudDisable = 0x00000000;
						constexpr Reg Value_autoBaudEnable = 0x1;
						constexpr Reg ValueMask_autoBaudEnable = 0x00100000;
						};
					namespace abrmod { // Field Description
						constexpr Reg Lsb = 21;
						constexpr Reg FieldMask = 0x00600000;
						constexpr Reg Value_default = 0x0;
						constexpr Reg ValueMask_default = 0x00000000;
						constexpr Reg Value_startBit = 0x0;
						constexpr Reg ValueMask_startBit = 0x00000000;
						constexpr Reg Value_fallingEdge = 0x1;
						constexpr Reg ValueMask_fallingEdge = 0x00200000;
						constexpr Reg Value_frame0x7F = 0x2;
						constexpr Reg ValueMask_frame0x7F = 0x00400000;
						constexpr Reg Value_frame0x55 = 0x3;
						constexpr Reg ValueMask_frame0x55 = 0x00600000;
						};
					namespace rtoen { // Field Description
						constexpr Reg Lsb = 23;
						constexpr Reg FieldMask = 0x01800000;
						constexpr Reg Value_default = 0x0;
						constexpr Reg ValueMask_default = 0x00000000;
						constexpr Reg Value_rxTimeoutDisable = 0x0;
						constexpr Reg ValueMask_rxTimeoutDisable = 0x00000000;
						constexpr Reg Value_rxTimeoutEnable = 0x1;
						constexpr Reg ValueMask_rxTimeoutEnable = 0x00800000;
						};
					namespace add30 { // Field Description
						constexpr Reg Lsb = 24;
						constexpr Reg FieldMask = 0x0F000000;
						constexpr Reg Value_default = 0x0;
						constexpr Reg ValueMask_default = 0x00000000;
						constexpr Reg Value_min = 0x0;
						constexpr Reg ValueMask_min = 0x00000000;
						constexpr Reg Value_max = 0xF;
						constexpr Reg ValueMask_max = 0x0F000000;
						};
					namespace add74 { // Field Description
						constexpr Reg Lsb = 24;
						constexpr Reg FieldMask = 0x0F000000;
						constexpr Reg Value_default = 0x0;
						constexpr Reg ValueMask_default = 0x00000000;
						constexpr Reg Value_min = 0x0;
						constexpr Reg ValueMask_min = 0x00000000;
						constexpr Reg Value_max = 0xF;
						constexpr Reg ValueMask_max = 0x0F000000;
						};
					};
				namespace Cr3 { // Register description
					typedef uint32_t	Reg;
					namespace eie { // Field Description
						constexpr Reg Lsb = 0;
						constexpr Reg FieldMask = 0x00000001;
						constexpr Reg Value_default = 0x0;
						constexpr Reg ValueMask_default = 0x00000000;
						constexpr Reg Value_errorInterruptDisable = 0x0;
						constexpr Reg ValueMask_errorInterruptDisable = 0x00000000;
						constexpr Reg Value_errorInterruptEnable = 0x1;
						constexpr Reg ValueMask_errorInterruptEnable = 0x00000001;
						};
					namespace iren { // Field Description
						constexpr Reg Lsb = 1;
						constexpr Reg FieldMask = 0x00000002;
						constexpr Reg Value_default = 0x0;
						constexpr Reg ValueMask_default = 0x00000000;
						constexpr Reg Value_irDaModeDisable = 0x0;
						constexpr Reg ValueMask_irDaModeDisable = 0x00000000;
						constexpr Reg Value_irDaModeEnable = 0x1;
						constexpr Reg ValueMask_irDaModeEnable = 0x00000002;
						};
					namespace irlp { // Field Description
						constexpr Reg Lsb = 2;
						constexpr Reg FieldMask = 0x00000004;
						constexpr Reg Value_default = 0x0;
						constexpr Reg ValueMask_default = 0x00000000;
						constexpr Reg Value_irDaNormalMode = 0x0;
						constexpr Reg ValueMask_irDaNormalMode = 0x00000000;
						constexpr Reg Value_irDaLowPowerMode = 0x1;
						constexpr Reg ValueMask_irDaLowPowerMode = 0x00000004;
						};
					namespace hdsel { // Field Description
						constexpr Reg Lsb = 3;
						constexpr Reg FieldMask = 0x00000008;
						constexpr Reg Value_default = 0x0;
						constexpr Reg ValueMask_default = 0x00000000;
						constexpr Reg Value_halfDuplexModeDisable = 0x0;
						constexpr Reg ValueMask_halfDuplexModeDisable = 0x00000000;
						constexpr Reg Value_halfDuplexModeEnable = 0x1;
						constexpr Reg ValueMask_halfDuplexModeEnable = 0x00000008;
						};
					namespace nack { // Field Description
						constexpr Reg Lsb = 4;
						constexpr Reg FieldMask = 0x00000010;
						constexpr Reg Value_default = 0x0;
						constexpr Reg ValueMask_default = 0x00000000;
						constexpr Reg Value_smartcardNackDisable = 0x0;
						constexpr Reg ValueMask_smartcardNackDisable = 0x00000000;
						constexpr Reg Value_smartcardNackEnable = 0x1;
						constexpr Reg ValueMask_smartcardNackEnable = 0x00000010;
						};
					namespace scen { // Field Description
						constexpr Reg Lsb = 5;
						constexpr Reg FieldMask = 0x00000020;
						constexpr Reg Value_default = 0x0;
						constexpr Reg ValueMask_default = 0x00000000;
						constexpr Reg Value_smartcardModeDisable = 0x0;
						constexpr Reg ValueMask_smartcardModeDisable = 0x00000000;
						constexpr Reg Value_smartcardModeEnable = 0x1;
						constexpr Reg ValueMask_smartcardModeEnable = 0x00000020;
						};
					namespace dmar { // Field Description
						constexpr Reg Lsb = 6;
						constexpr Reg FieldMask = 0x00000040;
						constexpr Reg Value_default = 0x0;
						constexpr Reg ValueMask_default = 0x00000000;
						constexpr Reg Value_rxDmaDisable = 0x0;
						constexpr Reg ValueMask_rxDmaDisable = 0x00000000;
						constexpr Reg Value_rxDmaEnable = 0x1;
						constexpr Reg ValueMask_rxDmaEnable = 0x00000040;
						};
					namespace dmat { // Field Description
						constexpr Reg Lsb = 7;
						constexpr Reg FieldMask = 0x00000080;
						constexpr Reg Value_default = 0x0;
						constexpr Reg ValueMask_default = 0x00000000;
						constexpr Reg Value_txDmaDisable = 0x0;
						constexpr Reg ValueMask_txDmaDisable = 0x00000000;
						constexpr Reg Value_txDmaEnable = 0x1;
						constexpr Reg ValueMask_txDmaEnable = 0x00000080;
						};
					namespace rtse { // Field Description
						constexpr Reg Lsb = 8;
						constexpr Reg FieldMask = 0x00000100;
						constexpr Reg Value_default = 0x0;
						constexpr Reg ValueMask_default = 0x00000000;
						constexpr Reg Value_rtsDisable = 0x0;
						constexpr Reg ValueMask_rtsDisable = 0x00000000;
						constexpr Reg Value_rtsEnable = 0x1;
						constexpr Reg ValueMask_rtsEnable = 0x00000100;
						};
					namespace ctse { // Field Description
						constexpr Reg Lsb = 9;
						constexpr Reg FieldMask = 0x00000200;
						constexpr Reg Value_default = 0x0;
						constexpr Reg ValueMask_default = 0x00000000;
						constexpr Reg Value_ctsDisable = 0x0;
						constexpr Reg ValueMask_ctsDisable = 0x00000000;
						constexpr Reg Value_ctsEnable = 0x1;
						constexpr Reg ValueMask_ctsEnable = 0x00000200;
						};
					namespace ctsie { // Field Description
						constexpr Reg Lsb = 10;
						constexpr Reg FieldMask = 0x00000400;
						constexpr Reg Value_default = 0x0;
						constexpr Reg ValueMask_default = 0x00000000;
						constexpr Reg Value_ctsInterruptDisable = 0x0;
						constexpr Reg ValueMask_ctsInterruptDisable = 0x00000000;
						constexpr Reg Value_ctsInterruptEnable = 0x1;
						constexpr Reg ValueMask_ctsInterruptEnable = 0x00000400;
						};
					namespace onebit { // Field Description
						constexpr Reg Lsb = 11;
						constexpr Reg FieldMask = 0x00000800;
						constexpr Reg Value_default = 0x0;
						constexpr Reg ValueMask_default = 0x00000000;
						constexpr Reg Value_threeSampleBitMethod = 0x0;
						constexpr Reg ValueMask_threeSampleBitMethod = 0x00000000;
						constexpr Reg Value_oneSampleBitMethod = 0x1;
						constexpr Reg ValueMask_oneSampleBitMethod = 0x00000800;
						};
					namespace overdis { // Field Description
						constexpr Reg Lsb = 12;
						constexpr Reg FieldMask = 0x00001000;
						constexpr Reg Value_default = 0x0;
						constexpr Reg ValueMask_default = 0x00000000;
						constexpr Reg Value_overrunDisable = 0x0;
						constexpr Reg ValueMask_overrunDisable = 0x00000000;
						constexpr Reg Value_overrunEnable = 0x1;
						constexpr Reg ValueMask_overrunEnable = 0x00001000;
						};
					namespace ddre { // Field Description
						constexpr Reg Lsb = 13;
						constexpr Reg FieldMask = 0x00002000;
						constexpr Reg Value_default = 0x0;
						constexpr Reg ValueMask_default = 0x00000000;
						constexpr Reg Value_dmaEnabledOnRxError = 0x0;
						constexpr Reg ValueMask_dmaEnabledOnRxError = 0x00000000;
						constexpr Reg Value_dmaDisabledOnRxError = 0x1;
						constexpr Reg ValueMask_dmaDisabledOnRxError = 0x00002000;
						};
					namespace dem { // Field Description
						constexpr Reg Lsb = 14;
						constexpr Reg FieldMask = 0x00004000;
						constexpr Reg Value_default = 0x0;
						constexpr Reg ValueMask_default = 0x00000000;
						constexpr Reg Value_deFunctionDisable = 0x0;
						constexpr Reg ValueMask_deFunctionDisable = 0x00000000;
						constexpr Reg Value_deFunctionEnable = 0x1;
						constexpr Reg ValueMask_deFunctionEnable = 0x00004000;
						};
					namespace dep { // Field Description
						constexpr Reg Lsb = 15;
						constexpr Reg FieldMask = 0x00008000;
						constexpr Reg Value_default = 0x0;
						constexpr Reg ValueMask_default = 0x00000000;
						constexpr Reg Value_dePolarityActiveHigh = 0x0;
						constexpr Reg ValueMask_dePolarityActiveHigh = 0x00000000;
						constexpr Reg Value_dePolarityActiveLow = 0x1;
						constexpr Reg ValueMask_dePolarityActiveLow = 0x00008000;
						};
					namespace scarcnt { // Field Description
						constexpr Reg Lsb = 17;
						constexpr Reg FieldMask = 0x000E0000;
						constexpr Reg Value_default = 0x0;
						constexpr Reg ValueMask_default = 0x00000000;
						constexpr Reg Value_retransmissionDisable = 0x0;
						constexpr Reg ValueMask_retransmissionDisable = 0x00000000;
						constexpr Reg Value_maxRetransmissionAttempts = 0x7;
						constexpr Reg ValueMask_maxRetransmissionAttempts = 0x000E0000;
						};
					namespace wus { // Field Description
						constexpr Reg Lsb = 20;
						constexpr Reg FieldMask = 0x00300000;
						constexpr Reg Value_default = 0x0;
						constexpr Reg ValueMask_default = 0x00000000;
						constexpr Reg Value_activeOnAddressMatch = 0x0;
						constexpr Reg ValueMask_activeOnAddressMatch = 0x00000000;
						constexpr Reg Value_activeOnStartBitDetection = 0x2;
						constexpr Reg ValueMask_activeOnStartBitDetection = 0x00200000;
						constexpr Reg Value_activeOnRXNE = 0x3;
						constexpr Reg ValueMask_activeOnRXNE = 0x00300000;
						};
					namespace wufie { // Field Description
						constexpr Reg Lsb = 22;
						constexpr Reg FieldMask = 0x00400000;
						constexpr Reg Value_default = 0x0;
						constexpr Reg ValueMask_default = 0x00000000;
						constexpr Reg Value_wakeFromStopModeInterruptDisable = 0x0;
						constexpr Reg ValueMask_wakeFromStopModeInterruptDisable = 0x00000000;
						constexpr Reg Value_wakeFromStopModeInterruptEnable = 0x1;
						constexpr Reg ValueMask_wakeFromStopModeInterruptEnable = 0x00400000;
						};
					namespace ucesm { // Field Description
						constexpr Reg Lsb = 23;
						constexpr Reg FieldMask = 0x00800000;
						constexpr Reg Value_default = 0x0;
						constexpr Reg ValueMask_default = 0x00000000;
						constexpr Reg Value_clockInStopModeDisable = 0x0;
						constexpr Reg ValueMask_clockInStopModeDisable = 0x00000000;
						constexpr Reg Value_clockInStopModeEnable = 0x1;
						constexpr Reg ValueMask_clockInStopModeEnable = 0x00800000;
						};
					namespace tcbgtie { // Field Description
						constexpr Reg Lsb = 24;
						constexpr Reg FieldMask = 0x01000000;
						constexpr Reg Value_default = 0x0;
						constexpr Reg ValueMask_default = 0x00000000;
						constexpr Reg Value_txCompleteBeforeGuardTimeDisable = 0x0;
						constexpr Reg ValueMask_txCompleteBeforeGuardTimeDisable = 0x00000000;
						constexpr Reg Value_txCompleteBeforeGuardTimeEnable = 0x1;
						constexpr Reg ValueMask_txCompleteBeforeGuardTimeEnable = 0x01000000;
						};
					};
				namespace Brr { // Register description
					typedef uint32_t	Reg;
					namespace brr { // Field Description
						constexpr Reg Lsb = 0;
						constexpr Reg FieldMask = 0x0000FFFF;
						constexpr Reg Value_default = 0x0;
						constexpr Reg ValueMask_default = 0x00000000;
						};
					};
				namespace Gtpr { // Register description
					typedef uint32_t	Reg;
					namespace psc { // Field Description
						constexpr Reg Lsb = 0;
						constexpr Reg FieldMask = 0x000000FF;
						constexpr Reg Value_default = 0x0;
						constexpr Reg ValueMask_default = 0x00000000;
						};
					namespace gt { // Field Description
						constexpr Reg Lsb = 8;
						constexpr Reg FieldMask = 0x0000FF00;
						constexpr Reg Value_default = 0x0;
						constexpr Reg ValueMask_default = 0x00000000;
						};
					};
				namespace Rtor { // Register description
					typedef uint32_t	Reg;
					namespace rto { // Field Description
						constexpr Reg Lsb = 0;
						constexpr Reg FieldMask = 0x00FFFFFF;
						constexpr Reg Value_default = 0x0;
						constexpr Reg ValueMask_default = 0x00000000;
						constexpr Reg Value_min = 0x0;
						constexpr Reg ValueMask_min = 0x00000000;
						constexpr Reg Value_max = 0xFFFFFF;
						constexpr Reg ValueMask_max = 0x00FFFFFF;
						};
					namespace blen { // Field Description
						constexpr Reg Lsb = 24;
						constexpr Reg FieldMask = 0xFF000000;
						constexpr Reg Value_default = 0x0;
						constexpr Reg ValueMask_default = 0x00000000;
						constexpr Reg Value_min = 0x0;
						constexpr Reg ValueMask_min = 0x00000000;
						constexpr Reg Value_max = 0xFF;
						constexpr Reg ValueMask_max = 0xFF000000;
						};
					};
				namespace Rqr { // Register description
					typedef uint32_t	Reg;
					namespace abrrq { // Field Description
						constexpr Reg Lsb = 0;
						constexpr Reg FieldMask = 0x00000001;
						constexpr Reg Value_default = 0x0;
						constexpr Reg ValueMask_default = 0x00000000;
						constexpr Reg Value_triggerAutoBaud = 0x1;
						constexpr Reg ValueMask_triggerAutoBaud = 0x00000001;
						};
					namespace sbkrq { // Field Description
						constexpr Reg Lsb = 1;
						constexpr Reg FieldMask = 0x00000002;
						constexpr Reg Value_default = 0x0;
						constexpr Reg ValueMask_default = 0x00000000;
						constexpr Reg Value_triggerBreak = 0x1;
						constexpr Reg ValueMask_triggerBreak = 0x00000002;
						};
					namespace mmrq { // Field Description
						constexpr Reg Lsb = 2;
						constexpr Reg FieldMask = 0x00000004;
						constexpr Reg Value_default = 0x0;
						constexpr Reg ValueMask_default = 0x00000000;
						constexpr Reg Value_triggerMute = 0x1;
						constexpr Reg ValueMask_triggerMute = 0x00000004;
						};
					namespace rxfrq { // Field Description
						constexpr Reg Lsb = 3;
						constexpr Reg FieldMask = 0x00000008;
						constexpr Reg Value_default = 0x0;
						constexpr Reg ValueMask_default = 0x00000000;
						constexpr Reg Value_triggerRxFlush = 0x1;
						constexpr Reg ValueMask_triggerRxFlush = 0x00000008;
						};
					namespace txfrq { // Field Description
						constexpr Reg Lsb = 4;
						constexpr Reg FieldMask = 0x00000010;
						constexpr Reg Value_default = 0x0;
						constexpr Reg ValueMask_default = 0x00000000;
						constexpr Reg Value_triggerTxFlush = 0x1;
						constexpr Reg ValueMask_triggerTxFlush = 0x00000010;
						};
					};
				namespace Isr { // Register description
					typedef uint32_t	Reg;
					constexpr Reg	default = 33554624;
					namespace pe { // Field Description
						constexpr Reg Lsb = 0;
						constexpr Reg FieldMask = 0x00000001;
						constexpr Reg Value_default = 0x0;
						constexpr Reg ValueMask_default = 0x00000000;
						constexpr Reg Value_noParityError = 0x0;
						constexpr Reg ValueMask_noParityError = 0x00000000;
						constexpr Reg Value_parityError = 0x1;
						constexpr Reg ValueMask_parityError = 0x00000001;
						};
					namespace fe { // Field Description
						constexpr Reg Lsb = 1;
						constexpr Reg FieldMask = 0x00000002;
						constexpr Reg Value_default = 0x0;
						constexpr Reg ValueMask_default = 0x00000000;
						constexpr Reg Value_framingErrorOrBreakDetected = 0x1;
						constexpr Reg ValueMask_framingErrorOrBreakDetected = 0x00000002;
						};
					namespace nf { // Field Description
						constexpr Reg Lsb = 2;
						constexpr Reg FieldMask = 0x00000004;
						constexpr Reg Value_default = 0x0;
						constexpr Reg ValueMask_default = 0x00000000;
						constexpr Reg Value_startBitNoiseDetected = 0x1;
						constexpr Reg ValueMask_startBitNoiseDetected = 0x00000004;
						};
					namespace ore { // Field Description
						constexpr Reg Lsb = 3;
						constexpr Reg FieldMask = 0x00000008;
						constexpr Reg Value_default = 0x0;
						constexpr Reg ValueMask_default = 0x00000000;
						constexpr Reg Value_overrunDetected = 0x1;
						constexpr Reg ValueMask_overrunDetected = 0x00000008;
						};
					namespace idle { // Field Description
						constexpr Reg Lsb = 4;
						constexpr Reg FieldMask = 0x00000010;
						constexpr Reg Value_default = 0x0;
						constexpr Reg ValueMask_default = 0x00000000;
						constexpr Reg Value_idleLineDetected = 0x1;
						constexpr Reg ValueMask_idleLineDetected = 0x00000010;
						};
					namespace rxne { // Field Description
						constexpr Reg Lsb = 5;
						constexpr Reg FieldMask = 0x00000020;
						constexpr Reg Value_default = 0x0;
						constexpr Reg ValueMask_default = 0x00000000;
						constexpr Reg Value_rxDataRegisterNotEmpty = 0x1;
						constexpr Reg ValueMask_rxDataRegisterNotEmpty = 0x00000020;
						};
					namespace tc { // Field Description
						constexpr Reg Lsb = 6;
						constexpr Reg FieldMask = 0x00000040;
						constexpr Reg Value_default = 0x1;
						constexpr Reg ValueMask_default = 0x00000040;
						constexpr Reg Value_txComplete = 0x1;
						constexpr Reg ValueMask_txComplete = 0x00000040;
						};
					namespace txe { // Field Description
						constexpr Reg Lsb = 7;
						constexpr Reg FieldMask = 0x00000080;
						constexpr Reg Value_default = 0x1;
						constexpr Reg ValueMask_default = 0x00000080;
						constexpr Reg Value_txDataRegisterEmpty = 0x1;
						constexpr Reg ValueMask_txDataRegisterEmpty = 0x00000080;
						};
					namespace lbdf { // Field Description
						constexpr Reg Lsb = 8;
						constexpr Reg FieldMask = 0x00000100;
						constexpr Reg Value_default = 0x0;
						constexpr Reg ValueMask_default = 0x00000000;
						constexpr Reg Value_linBreakDetected = 0x1;
						constexpr Reg ValueMask_linBreakDetected = 0x00000100;
						};
					namespace ctsif { // Field Description
						constexpr Reg Lsb = 9;
						constexpr Reg FieldMask = 0x00000200;
						constexpr Reg Value_default = 0x0;
						constexpr Reg ValueMask_default = 0x00000000;
						constexpr Reg Value_ctsStatusChanged = 0x1;
						constexpr Reg ValueMask_ctsStatusChanged = 0x00000200;
						};
					namespace cts { // Field Description
						constexpr Reg Lsb = 10;
						constexpr Reg FieldMask = 0x00000400;
						constexpr Reg Value_default = 0x0;
						constexpr Reg ValueMask_default = 0x00000000;
						constexpr Reg Value_ctsLineSet = 0x0;
						constexpr Reg ValueMask_ctsLineSet = 0x00000000;
						constexpr Reg Value_ctsLineReset = 0x1;
						constexpr Reg ValueMask_ctsLineReset = 0x00000400;
						};
					namespace rtof { // Field Description
						constexpr Reg Lsb = 11;
						constexpr Reg FieldMask = 0x00000800;
						constexpr Reg Value_default = 0x0;
						constexpr Reg ValueMask_default = 0x00000000;
						constexpr Reg Value_timeoutValueReached = 0x1;
						constexpr Reg ValueMask_timeoutValueReached = 0x00000800;
						};
					namespace eobf { // Field Description
						constexpr Reg Lsb = 12;
						constexpr Reg FieldMask = 0x00001000;
						constexpr Reg Value_default = 0x0;
						constexpr Reg ValueMask_default = 0x00000000;
						constexpr Reg Value_endOfBlockReached = 0x1;
						constexpr Reg ValueMask_endOfBlockReached = 0x00001000;
						};
					namespace abre { // Field Description
						constexpr Reg Lsb = 14;
						constexpr Reg FieldMask = 0x00004000;
						constexpr Reg Value_default = 0x0;
						constexpr Reg ValueMask_default = 0x00000000;
						constexpr Reg Value_autoBaudRateError = 0x1;
						constexpr Reg ValueMask_autoBaudRateError = 0x00004000;
						};
					namespace abrf { // Field Description
						constexpr Reg Lsb = 15;
						constexpr Reg FieldMask = 0x00008000;
						constexpr Reg Value_default = 0x0;
						constexpr Reg ValueMask_default = 0x00000000;
						constexpr Reg Value_autoBuadRateSet = 0x1;
						constexpr Reg ValueMask_autoBuadRateSet = 0x00008000;
						};
					namespace busy { // Field Description
						constexpr Reg Lsb = 16;
						constexpr Reg FieldMask = 0x00010000;
						constexpr Reg Value_default = 0x0;
						constexpr Reg ValueMask_default = 0x00000000;
						constexpr Reg Value_idle = 0x0;
						constexpr Reg ValueMask_idle = 0x00000000;
						constexpr Reg Value_rxOnGoing = 0x1;
						constexpr Reg ValueMask_rxOnGoing = 0x00010000;
						};
					namespace cmf { // Field Description
						constexpr Reg Lsb = 17;
						constexpr Reg FieldMask = 0x00020000;
						constexpr Reg Value_default = 0x0;
						constexpr Reg ValueMask_default = 0x00000000;
						constexpr Reg Value_noCharMatchDetected = 0x0;
						constexpr Reg ValueMask_noCharMatchDetected = 0x00000000;
						constexpr Reg Value_charMatchDetected = 0x1;
						constexpr Reg ValueMask_charMatchDetected = 0x00020000;
						};
					namespace sbkf { // Field Description
						constexpr Reg Lsb = 18;
						constexpr Reg FieldMask = 0x00040000;
						constexpr Reg Value_default = 0x0;
						constexpr Reg ValueMask_default = 0x00000000;
						constexpr Reg Value_noBreakCharIsTransmitted = 0x0;
						constexpr Reg ValueMask_noBreakCharIsTransmitted = 0x00000000;
						constexpr Reg Value_breakCharWillBeTransmitted = 0x1;
						constexpr Reg ValueMask_breakCharWillBeTransmitted = 0x00040000;
						};
					namespace rwu { // Field Description
						constexpr Reg Lsb = 19;
						constexpr Reg FieldMask = 0x00080000;
						constexpr Reg Value_default = 0x0;
						constexpr Reg ValueMask_default = 0x00000000;
						constexpr Reg Value_activeMode = 0x0;
						constexpr Reg ValueMask_activeMode = 0x00000000;
						constexpr Reg Value_muteMode = 0x1;
						constexpr Reg ValueMask_muteMode = 0x00080000;
						};
					namespace wuf { // Field Description
						constexpr Reg Lsb = 20;
						constexpr Reg FieldMask = 0x00100000;
						constexpr Reg Value_default = 0x0;
						constexpr Reg ValueMask_default = 0x00000000;
						constexpr Reg Value_wakeupEventDetected = 0x1;
						constexpr Reg ValueMask_wakeupEventDetected = 0x00100000;
						};
					namespace teack { // Field Description
						constexpr Reg Lsb = 21;
						constexpr Reg FieldMask = 0x00200000;
						constexpr Reg Value_default = 0x0;
						constexpr Reg ValueMask_default = 0x00000000;
						constexpr Reg Value_ready = 0x1;
						constexpr Reg ValueMask_ready = 0x00200000;
						};
					namespace reack { // Field Description
						constexpr Reg Lsb = 22;
						constexpr Reg FieldMask = 0x00400000;
						constexpr Reg Value_default = 0x0;
						constexpr Reg ValueMask_default = 0x00000000;
						constexpr Reg Value_ready = 0x1;
						constexpr Reg ValueMask_ready = 0x00400000;
						};
					namespace tcbgt { // Field Description
						constexpr Reg Lsb = 25;
						constexpr Reg FieldMask = 0x02000000;
						constexpr Reg Value_default = 0x1;
						constexpr Reg ValueMask_default = 0x02000000;
						constexpr Reg Value_txCompleteBeforeGuardTime = 0x1;
						constexpr Reg ValueMask_txCompleteBeforeGuardTime = 0x02000000;
						};
					};
				namespace Icr { // Register description
					typedef uint32_t	Reg;
					constexpr Reg	default = 0;
					namespace pecf { // Field Description
						constexpr Reg Lsb = 0;
						constexpr Reg FieldMask = 0x00000001;
						constexpr Reg Value_default = 0x0;
						constexpr Reg ValueMask_default = 0x00000000;
						constexpr Reg Value_clear = 0x1;
						constexpr Reg ValueMask_clear = 0x00000001;
						};
					namespace fecf { // Field Description
						constexpr Reg Lsb = 1;
						constexpr Reg FieldMask = 0x00000002;
						constexpr Reg Value_default = 0x0;
						constexpr Reg ValueMask_default = 0x00000000;
						constexpr Reg Value_clear = 0x1;
						constexpr Reg ValueMask_clear = 0x00000002;
						};
					namespace ncf { // Field Description
						constexpr Reg Lsb = 2;
						constexpr Reg FieldMask = 0x00000004;
						constexpr Reg Value_default = 0x0;
						constexpr Reg ValueMask_default = 0x00000000;
						constexpr Reg Value_clear = 0x1;
						constexpr Reg ValueMask_clear = 0x00000004;
						};
					namespace orecf { // Field Description
						constexpr Reg Lsb = 3;
						constexpr Reg FieldMask = 0x00000008;
						constexpr Reg Value_default = 0x0;
						constexpr Reg ValueMask_default = 0x00000000;
						constexpr Reg Value_clear = 0x1;
						constexpr Reg ValueMask_clear = 0x00000008;
						};
					namespace idlecf { // Field Description
						constexpr Reg Lsb = 4;
						constexpr Reg FieldMask = 0x00000010;
						constexpr Reg Value_default = 0x0;
						constexpr Reg ValueMask_default = 0x00000000;
						constexpr Reg Value_clear = 0x1;
						constexpr Reg ValueMask_clear = 0x00000010;
						};
					namespace tccf { // Field Description
						constexpr Reg Lsb = 6;
						constexpr Reg FieldMask = 0x00000040;
						constexpr Reg Value_default = 0x0;
						constexpr Reg ValueMask_default = 0x00000000;
						constexpr Reg Value_clear = 0x1;
						constexpr Reg ValueMask_clear = 0x00000040;
						};
					namespace tcbgtcf { // Field Description
						constexpr Reg Lsb = 7;
						constexpr Reg FieldMask = 0x00000080;
						constexpr Reg Value_default = 0x0;
						constexpr Reg ValueMask_default = 0x00000000;
						constexpr Reg Value_clear = 0x1;
						constexpr Reg ValueMask_clear = 0x00000080;
						};
					namespace lbdcf { // Field Description
						constexpr Reg Lsb = 8;
						constexpr Reg FieldMask = 0x00000100;
						constexpr Reg Value_default = 0x0;
						constexpr Reg ValueMask_default = 0x00000000;
						constexpr Reg Value_clear = 0x1;
						constexpr Reg ValueMask_clear = 0x00000100;
						};
					namespace ctscf { // Field Description
						constexpr Reg Lsb = 9;
						constexpr Reg FieldMask = 0x00000200;
						constexpr Reg Value_default = 0x0;
						constexpr Reg ValueMask_default = 0x00000000;
						constexpr Reg Value_clear = 0x1;
						constexpr Reg ValueMask_clear = 0x00000200;
						};
					namespace rtocf { // Field Description
						constexpr Reg Lsb = 11;
						constexpr Reg FieldMask = 0x00000800;
						constexpr Reg Value_default = 0x0;
						constexpr Reg ValueMask_default = 0x00000000;
						constexpr Reg Value_clear = 0x1;
						constexpr Reg ValueMask_clear = 0x00000800;
						};
					namespace eobcf { // Field Description
						constexpr Reg Lsb = 12;
						constexpr Reg FieldMask = 0x00001000;
						constexpr Reg Value_default = 0x0;
						constexpr Reg ValueMask_default = 0x00000000;
						constexpr Reg Value_clear = 0x1;
						constexpr Reg ValueMask_clear = 0x00001000;
						};
					namespace cmcf { // Field Description
						constexpr Reg Lsb = 17;
						constexpr Reg FieldMask = 0x00020000;
						constexpr Reg Value_default = 0x0;
						constexpr Reg ValueMask_default = 0x00000000;
						constexpr Reg Value_clear = 0x1;
						constexpr Reg ValueMask_clear = 0x00020000;
						};
					namespace wucf { // Field Description
						constexpr Reg Lsb = 20;
						constexpr Reg FieldMask = 0x00100000;
						constexpr Reg Value_default = 0x0;
						constexpr Reg ValueMask_default = 0x00000000;
						constexpr Reg Value_clear = 0x1;
						constexpr Reg ValueMask_clear = 0x00100000;
						};
					};
				namespace Rdr { // Register description
					typedef uint32_t	Reg;
					constexpr Reg	default = 0;
					namespace rdr { // Field Description
						constexpr Reg Lsb = 0;
						constexpr Reg FieldMask = 0x000000FF;
						constexpr Reg Value_default = 0x0;
						constexpr Reg ValueMask_default = 0x00000000;
						constexpr Reg Value_min = 0x0;
						constexpr Reg ValueMask_min = 0x00000000;
						constexpr Reg Value_max = 0xFF;
						constexpr Reg ValueMask_max = 0x000000FF;
						};
					};
				namespace Tdr { // Register description
					typedef uint32_t	Reg;
					constexpr Reg	default = 0;
					namespace tdr { // Field Description
						constexpr Reg Lsb = 0;
						constexpr Reg FieldMask = 0x000000FF;
						constexpr Reg Value_default = 0x0;
						constexpr Reg ValueMask_default = 0x00000000;
						constexpr Reg Value_min = 0x0;
						constexpr Reg ValueMask_min = 0x00000000;
						constexpr Reg Value_max = 0xFF;
						constexpr Reg ValueMask_max = 0x000000FF;
						};
					};
				}
			}
		}
	}
#endif
