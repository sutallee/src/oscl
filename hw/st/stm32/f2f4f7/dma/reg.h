#ifndef _oscl_hw_st_stm32_f2f4f7_dma_regh_
#define _oscl_hw_st_stm32_f2f4f7_dma_regh_
#include <stdint.h>
namespace Oscl { // Namespace description
	namespace St { // Namespace description
		namespace Stm32 { // Namespace description
			namespace F2F4F7 { // Namespace description
				namespace Dma { // Namespace description
					namespace Lisr { // Register description
						typedef uint32_t	Reg;
						namespace feif0 { // Field Description
							constexpr Reg Lsb = 0;
							constexpr Reg FieldMask = 0x00000001;
							constexpr Reg Value_Default = 0x0;
							constexpr Reg ValueMask_Default = 0x00000000;
							constexpr Reg Value_fifoError = 0x1;
							constexpr Reg ValueMask_fifoError = 0x00000001;
							};
						namespace dmeif0 { // Field Description
							constexpr Reg Lsb = 2;
							constexpr Reg FieldMask = 0x00000004;
							constexpr Reg Value_Default = 0x0;
							constexpr Reg ValueMask_Default = 0x00000000;
							constexpr Reg Value_directModeError = 0x1;
							constexpr Reg ValueMask_directModeError = 0x00000004;
							};
						namespace teif0 { // Field Description
							constexpr Reg Lsb = 3;
							constexpr Reg FieldMask = 0x00000008;
							constexpr Reg Value_Default = 0x0;
							constexpr Reg ValueMask_Default = 0x00000000;
							constexpr Reg Value_transferError = 0x1;
							constexpr Reg ValueMask_transferError = 0x00000008;
							};
						namespace htif0 { // Field Description
							constexpr Reg Lsb = 4;
							constexpr Reg FieldMask = 0x00000010;
							constexpr Reg Value_Default = 0x0;
							constexpr Reg ValueMask_Default = 0x00000000;
							constexpr Reg Value_halfTransferEvent = 0x1;
							constexpr Reg ValueMask_halfTransferEvent = 0x00000010;
							};
						namespace tcif0 { // Field Description
							constexpr Reg Lsb = 5;
							constexpr Reg FieldMask = 0x00000020;
							constexpr Reg Value_Default = 0x0;
							constexpr Reg ValueMask_Default = 0x00000000;
							constexpr Reg Value_transferCompleteEvent = 0x1;
							constexpr Reg ValueMask_transferCompleteEvent = 0x00000020;
							};
						namespace feif1 { // Field Description
							constexpr Reg Lsb = 6;
							constexpr Reg FieldMask = 0x00000040;
							constexpr Reg Value_Default = 0x0;
							constexpr Reg ValueMask_Default = 0x00000000;
							constexpr Reg Value_fifoError = 0x1;
							constexpr Reg ValueMask_fifoError = 0x00000040;
							};
						namespace dmeif1 { // Field Description
							constexpr Reg Lsb = 8;
							constexpr Reg FieldMask = 0x00000100;
							constexpr Reg Value_Default = 0x0;
							constexpr Reg ValueMask_Default = 0x00000000;
							constexpr Reg Value_directModeError = 0x1;
							constexpr Reg ValueMask_directModeError = 0x00000100;
							};
						namespace teif1 { // Field Description
							constexpr Reg Lsb = 9;
							constexpr Reg FieldMask = 0x00000200;
							constexpr Reg Value_Default = 0x0;
							constexpr Reg ValueMask_Default = 0x00000000;
							constexpr Reg Value_transferError = 0x1;
							constexpr Reg ValueMask_transferError = 0x00000200;
							};
						namespace htif1 { // Field Description
							constexpr Reg Lsb = 10;
							constexpr Reg FieldMask = 0x00000400;
							constexpr Reg Value_Default = 0x0;
							constexpr Reg ValueMask_Default = 0x00000000;
							constexpr Reg Value_halfTransferEvent = 0x1;
							constexpr Reg ValueMask_halfTransferEvent = 0x00000400;
							};
						namespace tcif1 { // Field Description
							constexpr Reg Lsb = 11;
							constexpr Reg FieldMask = 0x00000800;
							constexpr Reg Value_Default = 0x0;
							constexpr Reg ValueMask_Default = 0x00000000;
							constexpr Reg Value_transferCompleteEvent = 0x1;
							constexpr Reg ValueMask_transferCompleteEvent = 0x00000800;
							};
						namespace feif2 { // Field Description
							constexpr Reg Lsb = 16;
							constexpr Reg FieldMask = 0x00010000;
							constexpr Reg Value_Default = 0x0;
							constexpr Reg ValueMask_Default = 0x00000000;
							constexpr Reg Value_fifoError = 0x1;
							constexpr Reg ValueMask_fifoError = 0x00010000;
							};
						namespace dmeif2 { // Field Description
							constexpr Reg Lsb = 18;
							constexpr Reg FieldMask = 0x00040000;
							constexpr Reg Value_Default = 0x0;
							constexpr Reg ValueMask_Default = 0x00000000;
							constexpr Reg Value_directModeError = 0x1;
							constexpr Reg ValueMask_directModeError = 0x00040000;
							};
						namespace teif2 { // Field Description
							constexpr Reg Lsb = 19;
							constexpr Reg FieldMask = 0x00080000;
							constexpr Reg Value_Default = 0x0;
							constexpr Reg ValueMask_Default = 0x00000000;
							constexpr Reg Value_transferError = 0x1;
							constexpr Reg ValueMask_transferError = 0x00080000;
							};
						namespace htif2 { // Field Description
							constexpr Reg Lsb = 20;
							constexpr Reg FieldMask = 0x00100000;
							constexpr Reg Value_Default = 0x0;
							constexpr Reg ValueMask_Default = 0x00000000;
							constexpr Reg Value_halfTransferEvent = 0x1;
							constexpr Reg ValueMask_halfTransferEvent = 0x00100000;
							};
						namespace tcif2 { // Field Description
							constexpr Reg Lsb = 21;
							constexpr Reg FieldMask = 0x00200000;
							constexpr Reg Value_Default = 0x0;
							constexpr Reg ValueMask_Default = 0x00000000;
							constexpr Reg Value_transferCompleteEvent = 0x1;
							constexpr Reg ValueMask_transferCompleteEvent = 0x00200000;
							};
						namespace feif3 { // Field Description
							constexpr Reg Lsb = 22;
							constexpr Reg FieldMask = 0x00400000;
							constexpr Reg Value_Default = 0x0;
							constexpr Reg ValueMask_Default = 0x00000000;
							constexpr Reg Value_fifoError = 0x1;
							constexpr Reg ValueMask_fifoError = 0x00400000;
							};
						namespace dmeif3 { // Field Description
							constexpr Reg Lsb = 24;
							constexpr Reg FieldMask = 0x01000000;
							constexpr Reg Value_Default = 0x0;
							constexpr Reg ValueMask_Default = 0x00000000;
							constexpr Reg Value_directModeError = 0x1;
							constexpr Reg ValueMask_directModeError = 0x01000000;
							};
						namespace teif3 { // Field Description
							constexpr Reg Lsb = 25;
							constexpr Reg FieldMask = 0x02000000;
							constexpr Reg Value_Default = 0x0;
							constexpr Reg ValueMask_Default = 0x00000000;
							constexpr Reg Value_transferError = 0x1;
							constexpr Reg ValueMask_transferError = 0x02000000;
							};
						namespace htif3 { // Field Description
							constexpr Reg Lsb = 26;
							constexpr Reg FieldMask = 0x04000000;
							constexpr Reg Value_Default = 0x0;
							constexpr Reg ValueMask_Default = 0x00000000;
							constexpr Reg Value_halfTransferEvent = 0x1;
							constexpr Reg ValueMask_halfTransferEvent = 0x04000000;
							};
						namespace tcif3 { // Field Description
							constexpr Reg Lsb = 27;
							constexpr Reg FieldMask = 0x08000000;
							constexpr Reg Value_Default = 0x0;
							constexpr Reg ValueMask_Default = 0x00000000;
							constexpr Reg Value_transferCompleteEvent = 0x1;
							constexpr Reg ValueMask_transferCompleteEvent = 0x08000000;
							};
						};
					namespace Hisr { // Register description
						typedef uint32_t	Reg;
						namespace feif4 { // Field Description
							constexpr Reg Lsb = 0;
							constexpr Reg FieldMask = 0x00000001;
							constexpr Reg Value_Default = 0x0;
							constexpr Reg ValueMask_Default = 0x00000000;
							constexpr Reg Value_fifoError = 0x1;
							constexpr Reg ValueMask_fifoError = 0x00000001;
							};
						namespace dmeif4 { // Field Description
							constexpr Reg Lsb = 2;
							constexpr Reg FieldMask = 0x00000004;
							constexpr Reg Value_Default = 0x0;
							constexpr Reg ValueMask_Default = 0x00000000;
							constexpr Reg Value_directModeError = 0x1;
							constexpr Reg ValueMask_directModeError = 0x00000004;
							};
						namespace teif4 { // Field Description
							constexpr Reg Lsb = 3;
							constexpr Reg FieldMask = 0x00000008;
							constexpr Reg Value_Default = 0x0;
							constexpr Reg ValueMask_Default = 0x00000000;
							constexpr Reg Value_transferError = 0x1;
							constexpr Reg ValueMask_transferError = 0x00000008;
							};
						namespace htif4 { // Field Description
							constexpr Reg Lsb = 4;
							constexpr Reg FieldMask = 0x00000010;
							constexpr Reg Value_Default = 0x0;
							constexpr Reg ValueMask_Default = 0x00000000;
							constexpr Reg Value_halfTransferEvent = 0x1;
							constexpr Reg ValueMask_halfTransferEvent = 0x00000010;
							};
						namespace tcif4 { // Field Description
							constexpr Reg Lsb = 5;
							constexpr Reg FieldMask = 0x00000020;
							constexpr Reg Value_Default = 0x0;
							constexpr Reg ValueMask_Default = 0x00000000;
							constexpr Reg Value_transferCompleteEvent = 0x1;
							constexpr Reg ValueMask_transferCompleteEvent = 0x00000020;
							};
						namespace feif5 { // Field Description
							constexpr Reg Lsb = 6;
							constexpr Reg FieldMask = 0x00000040;
							constexpr Reg Value_Default = 0x0;
							constexpr Reg ValueMask_Default = 0x00000000;
							constexpr Reg Value_fifoError = 0x1;
							constexpr Reg ValueMask_fifoError = 0x00000040;
							};
						namespace dmeif5 { // Field Description
							constexpr Reg Lsb = 8;
							constexpr Reg FieldMask = 0x00000100;
							constexpr Reg Value_Default = 0x0;
							constexpr Reg ValueMask_Default = 0x00000000;
							constexpr Reg Value_directModeError = 0x1;
							constexpr Reg ValueMask_directModeError = 0x00000100;
							};
						namespace teif5 { // Field Description
							constexpr Reg Lsb = 9;
							constexpr Reg FieldMask = 0x00000200;
							constexpr Reg Value_Default = 0x0;
							constexpr Reg ValueMask_Default = 0x00000000;
							constexpr Reg Value_transferError = 0x1;
							constexpr Reg ValueMask_transferError = 0x00000200;
							};
						namespace htif5 { // Field Description
							constexpr Reg Lsb = 10;
							constexpr Reg FieldMask = 0x00000400;
							constexpr Reg Value_Default = 0x0;
							constexpr Reg ValueMask_Default = 0x00000000;
							constexpr Reg Value_halfTransferEvent = 0x1;
							constexpr Reg ValueMask_halfTransferEvent = 0x00000400;
							};
						namespace tcif5 { // Field Description
							constexpr Reg Lsb = 11;
							constexpr Reg FieldMask = 0x00000800;
							constexpr Reg Value_Default = 0x0;
							constexpr Reg ValueMask_Default = 0x00000000;
							constexpr Reg Value_transferCompleteEvent = 0x1;
							constexpr Reg ValueMask_transferCompleteEvent = 0x00000800;
							};
						namespace feif6 { // Field Description
							constexpr Reg Lsb = 16;
							constexpr Reg FieldMask = 0x00010000;
							constexpr Reg Value_Default = 0x0;
							constexpr Reg ValueMask_Default = 0x00000000;
							constexpr Reg Value_fifoError = 0x1;
							constexpr Reg ValueMask_fifoError = 0x00010000;
							};
						namespace dmeif6 { // Field Description
							constexpr Reg Lsb = 18;
							constexpr Reg FieldMask = 0x00040000;
							constexpr Reg Value_Default = 0x0;
							constexpr Reg ValueMask_Default = 0x00000000;
							constexpr Reg Value_directModeError = 0x1;
							constexpr Reg ValueMask_directModeError = 0x00040000;
							};
						namespace teif6 { // Field Description
							constexpr Reg Lsb = 19;
							constexpr Reg FieldMask = 0x00080000;
							constexpr Reg Value_Default = 0x0;
							constexpr Reg ValueMask_Default = 0x00000000;
							constexpr Reg Value_transferError = 0x1;
							constexpr Reg ValueMask_transferError = 0x00080000;
							};
						namespace htif6 { // Field Description
							constexpr Reg Lsb = 20;
							constexpr Reg FieldMask = 0x00100000;
							constexpr Reg Value_Default = 0x0;
							constexpr Reg ValueMask_Default = 0x00000000;
							constexpr Reg Value_halfTransferEvent = 0x1;
							constexpr Reg ValueMask_halfTransferEvent = 0x00100000;
							};
						namespace tcif6 { // Field Description
							constexpr Reg Lsb = 21;
							constexpr Reg FieldMask = 0x00200000;
							constexpr Reg Value_Default = 0x0;
							constexpr Reg ValueMask_Default = 0x00000000;
							constexpr Reg Value_transferCompleteEvent = 0x1;
							constexpr Reg ValueMask_transferCompleteEvent = 0x00200000;
							};
						namespace feif7 { // Field Description
							constexpr Reg Lsb = 22;
							constexpr Reg FieldMask = 0x00400000;
							constexpr Reg Value_Default = 0x0;
							constexpr Reg ValueMask_Default = 0x00000000;
							constexpr Reg Value_fifoError = 0x1;
							constexpr Reg ValueMask_fifoError = 0x00400000;
							};
						namespace dmeif7 { // Field Description
							constexpr Reg Lsb = 24;
							constexpr Reg FieldMask = 0x01000000;
							constexpr Reg Value_Default = 0x0;
							constexpr Reg ValueMask_Default = 0x00000000;
							constexpr Reg Value_directModeError = 0x1;
							constexpr Reg ValueMask_directModeError = 0x01000000;
							};
						namespace teif7 { // Field Description
							constexpr Reg Lsb = 25;
							constexpr Reg FieldMask = 0x02000000;
							constexpr Reg Value_Default = 0x0;
							constexpr Reg ValueMask_Default = 0x00000000;
							constexpr Reg Value_transferError = 0x1;
							constexpr Reg ValueMask_transferError = 0x02000000;
							};
						namespace htif7 { // Field Description
							constexpr Reg Lsb = 26;
							constexpr Reg FieldMask = 0x04000000;
							constexpr Reg Value_Default = 0x0;
							constexpr Reg ValueMask_Default = 0x00000000;
							constexpr Reg Value_halfTransferEvent = 0x1;
							constexpr Reg ValueMask_halfTransferEvent = 0x04000000;
							};
						namespace tcif7 { // Field Description
							constexpr Reg Lsb = 27;
							constexpr Reg FieldMask = 0x08000000;
							constexpr Reg Value_Default = 0x0;
							constexpr Reg ValueMask_Default = 0x00000000;
							constexpr Reg Value_transferCompleteEvent = 0x1;
							constexpr Reg ValueMask_transferCompleteEvent = 0x08000000;
							};
						};
					namespace StreamIsr { // Register description
						typedef uint32_t	Reg;
						constexpr Reg	OffsetStream0 = 0;
						constexpr Reg	OffsetStream1 = 6;
						constexpr Reg	OffsetStream2 = 16;
						constexpr Reg	OffsetStream3 = 22;
						constexpr Reg	OffsetStream4 = 0;
						constexpr Reg	OffsetStream5 = 6;
						constexpr Reg	OffsetStream6 = 16;
						constexpr Reg	OffsetStream7 = 22;
						namespace feif { // Field Description
							constexpr Reg Lsb = 0;
							constexpr Reg FieldMask = 0x00000001;
							constexpr Reg Value_Default = 0x0;
							constexpr Reg ValueMask_Default = 0x00000000;
							constexpr Reg Value_fifoError = 0x1;
							constexpr Reg ValueMask_fifoError = 0x00000001;
							};
						namespace dmeif { // Field Description
							constexpr Reg Lsb = 2;
							constexpr Reg FieldMask = 0x00000004;
							constexpr Reg Value_Default = 0x0;
							constexpr Reg ValueMask_Default = 0x00000000;
							constexpr Reg Value_directModeError = 0x1;
							constexpr Reg ValueMask_directModeError = 0x00000004;
							};
						namespace teif { // Field Description
							constexpr Reg Lsb = 3;
							constexpr Reg FieldMask = 0x00000008;
							constexpr Reg Value_Default = 0x0;
							constexpr Reg ValueMask_Default = 0x00000000;
							constexpr Reg Value_transferError = 0x1;
							constexpr Reg ValueMask_transferError = 0x00000008;
							};
						namespace htif { // Field Description
							constexpr Reg Lsb = 4;
							constexpr Reg FieldMask = 0x00000010;
							constexpr Reg Value_Default = 0x0;
							constexpr Reg ValueMask_Default = 0x00000000;
							constexpr Reg Value_halfTransferEvent = 0x1;
							constexpr Reg ValueMask_halfTransferEvent = 0x00000010;
							};
						namespace tcif { // Field Description
							constexpr Reg Lsb = 5;
							constexpr Reg FieldMask = 0x00000020;
							constexpr Reg Value_Default = 0x0;
							constexpr Reg ValueMask_Default = 0x00000000;
							constexpr Reg Value_transferCompleteEvent = 0x1;
							constexpr Reg ValueMask_transferCompleteEvent = 0x00000020;
							};
						};
					namespace StreamIfcr { // Register description
						typedef uint32_t	Reg;
						constexpr Reg	OffsetStream0 = 0;
						constexpr Reg	OffsetStream1 = 6;
						constexpr Reg	OffsetStream2 = 16;
						constexpr Reg	OffsetStream3 = 22;
						constexpr Reg	OffsetStream4 = 0;
						constexpr Reg	OffsetStream5 = 6;
						constexpr Reg	OffsetStream6 = 16;
						constexpr Reg	OffsetStream7 = 22;
						namespace cfeif { // Field Description
							constexpr Reg Lsb = 0;
							constexpr Reg FieldMask = 0x00000001;
							constexpr Reg Value_Default = 0x0;
							constexpr Reg ValueMask_Default = 0x00000000;
							constexpr Reg Value_clear = 0x1;
							constexpr Reg ValueMask_clear = 0x00000001;
							};
						namespace cdmeif { // Field Description
							constexpr Reg Lsb = 2;
							constexpr Reg FieldMask = 0x00000004;
							constexpr Reg Value_Default = 0x0;
							constexpr Reg ValueMask_Default = 0x00000000;
							constexpr Reg Value_clear = 0x1;
							constexpr Reg ValueMask_clear = 0x00000004;
							};
						namespace cteif { // Field Description
							constexpr Reg Lsb = 3;
							constexpr Reg FieldMask = 0x00000008;
							constexpr Reg Value_Default = 0x0;
							constexpr Reg ValueMask_Default = 0x00000000;
							constexpr Reg Value_clear = 0x1;
							constexpr Reg ValueMask_clear = 0x00000008;
							};
						namespace chtif { // Field Description
							constexpr Reg Lsb = 4;
							constexpr Reg FieldMask = 0x00000010;
							constexpr Reg Value_Default = 0x0;
							constexpr Reg ValueMask_Default = 0x00000000;
							constexpr Reg Value_clear = 0x1;
							constexpr Reg ValueMask_clear = 0x00000010;
							};
						namespace ctcf { // Field Description
							constexpr Reg Lsb = 5;
							constexpr Reg FieldMask = 0x00000020;
							constexpr Reg Value_Default = 0x0;
							constexpr Reg ValueMask_Default = 0x00000000;
							constexpr Reg Value_clear = 0x1;
							constexpr Reg ValueMask_clear = 0x00000020;
							};
						};
					namespace Sxcr { // Register description
						typedef uint32_t	Reg;
						namespace en { // Field Description
							constexpr Reg Lsb = 0;
							constexpr Reg FieldMask = 0x00000001;
							constexpr Reg Value_Default = 0x0;
							constexpr Reg ValueMask_Default = 0x00000000;
							constexpr Reg Value_disable = 0x0;
							constexpr Reg ValueMask_disable = 0x00000000;
							constexpr Reg Value_enable = 0x1;
							constexpr Reg ValueMask_enable = 0x00000001;
							};
						namespace dmeie { // Field Description
							constexpr Reg Lsb = 1;
							constexpr Reg FieldMask = 0x00000002;
							constexpr Reg Value_Default = 0x0;
							constexpr Reg ValueMask_Default = 0x00000000;
							constexpr Reg Value_interruptDisable = 0x0;
							constexpr Reg ValueMask_interruptDisable = 0x00000000;
							constexpr Reg Value_interruptEnable = 0x1;
							constexpr Reg ValueMask_interruptEnable = 0x00000002;
							};
						namespace teie { // Field Description
							constexpr Reg Lsb = 2;
							constexpr Reg FieldMask = 0x00000004;
							constexpr Reg Value_Default = 0x0;
							constexpr Reg ValueMask_Default = 0x00000000;
							constexpr Reg Value_interruptDisable = 0x0;
							constexpr Reg ValueMask_interruptDisable = 0x00000000;
							constexpr Reg Value_interruptEnable = 0x1;
							constexpr Reg ValueMask_interruptEnable = 0x00000004;
							};
						namespace htie { // Field Description
							constexpr Reg Lsb = 3;
							constexpr Reg FieldMask = 0x00000008;
							constexpr Reg Value_Default = 0x0;
							constexpr Reg ValueMask_Default = 0x00000000;
							constexpr Reg Value_interruptDisable = 0x0;
							constexpr Reg ValueMask_interruptDisable = 0x00000000;
							constexpr Reg Value_interruptEnable = 0x1;
							constexpr Reg ValueMask_interruptEnable = 0x00000008;
							};
						namespace tcie { // Field Description
							constexpr Reg Lsb = 4;
							constexpr Reg FieldMask = 0x00000010;
							constexpr Reg Value_Default = 0x0;
							constexpr Reg ValueMask_Default = 0x00000000;
							constexpr Reg Value_interruptDisable = 0x0;
							constexpr Reg ValueMask_interruptDisable = 0x00000000;
							constexpr Reg Value_interruptEnable = 0x1;
							constexpr Reg ValueMask_interruptEnable = 0x00000010;
							};
						namespace pfctrl { // Field Description
							constexpr Reg Lsb = 5;
							constexpr Reg FieldMask = 0x00000020;
							constexpr Reg Value_Default = 0x0;
							constexpr Reg ValueMask_Default = 0x00000000;
							constexpr Reg Value_dmaFlowController = 0x0;
							constexpr Reg ValueMask_dmaFlowController = 0x00000000;
							constexpr Reg Value_peripheralFlowController = 0x1;
							constexpr Reg ValueMask_peripheralFlowController = 0x00000020;
							};
						namespace dir { // Field Description
							constexpr Reg Lsb = 6;
							constexpr Reg FieldMask = 0x000000C0;
							constexpr Reg Value_Default = 0x0;
							constexpr Reg ValueMask_Default = 0x00000000;
							constexpr Reg Value_peripheralToMemory = 0x0;
							constexpr Reg ValueMask_peripheralToMemory = 0x00000000;
							constexpr Reg Value_memoryToPeripheral = 0x1;
							constexpr Reg ValueMask_memoryToPeripheral = 0x00000040;
							constexpr Reg Value_memoryToMemory = 0x2;
							constexpr Reg ValueMask_memoryToMemory = 0x00000080;
							};
						namespace circ { // Field Description
							constexpr Reg Lsb = 8;
							constexpr Reg FieldMask = 0x00000100;
							constexpr Reg Value_Default = 0x0;
							constexpr Reg ValueMask_Default = 0x00000000;
							constexpr Reg Value_circularModeDisable = 0x0;
							constexpr Reg ValueMask_circularModeDisable = 0x00000000;
							constexpr Reg Value_circularModeEnable = 0x1;
							constexpr Reg ValueMask_circularModeEnable = 0x00000100;
							};
						namespace pinc { // Field Description
							constexpr Reg Lsb = 9;
							constexpr Reg FieldMask = 0x00000200;
							constexpr Reg Value_Default = 0x0;
							constexpr Reg ValueMask_Default = 0x00000000;
							constexpr Reg Value_fixed = 0x0;
							constexpr Reg ValueMask_fixed = 0x00000000;
							constexpr Reg Value_increment = 0x1;
							constexpr Reg ValueMask_increment = 0x00000200;
							};
						namespace minc { // Field Description
							constexpr Reg Lsb = 10;
							constexpr Reg FieldMask = 0x00000400;
							constexpr Reg Value_Default = 0x0;
							constexpr Reg ValueMask_Default = 0x00000000;
							constexpr Reg Value_fixed = 0x0;
							constexpr Reg ValueMask_fixed = 0x00000000;
							constexpr Reg Value_increment = 0x1;
							constexpr Reg ValueMask_increment = 0x00000400;
							};
						namespace psize { // Field Description
							constexpr Reg Lsb = 11;
							constexpr Reg FieldMask = 0x00001800;
							constexpr Reg Value_Default = 0x0;
							constexpr Reg ValueMask_Default = 0x00000000;
							constexpr Reg Value_8Bit = 0x0;
							constexpr Reg ValueMask_8Bit = 0x00000000;
							constexpr Reg Value_16Bit = 0x1;
							constexpr Reg ValueMask_16Bit = 0x00000800;
							constexpr Reg Value_32Bit = 0x2;
							constexpr Reg ValueMask_32Bit = 0x00001000;
							};
						namespace msize { // Field Description
							constexpr Reg Lsb = 13;
							constexpr Reg FieldMask = 0x00006000;
							constexpr Reg Value_Default = 0x0;
							constexpr Reg ValueMask_Default = 0x00000000;
							constexpr Reg Value_8Bit = 0x0;
							constexpr Reg ValueMask_8Bit = 0x00000000;
							constexpr Reg Value_16Bit = 0x1;
							constexpr Reg ValueMask_16Bit = 0x00002000;
							constexpr Reg Value_32Bit = 0x2;
							constexpr Reg ValueMask_32Bit = 0x00004000;
							};
						namespace pincos { // Field Description
							constexpr Reg Lsb = 15;
							constexpr Reg FieldMask = 0x00008000;
							constexpr Reg Value_Default = 0x0;
							constexpr Reg ValueMask_Default = 0x00000000;
							constexpr Reg Value_psize = 0x0;
							constexpr Reg ValueMask_psize = 0x00000000;
							constexpr Reg Value_fixed = 0x1;
							constexpr Reg ValueMask_fixed = 0x00008000;
							};
						namespace pl { // Field Description
							constexpr Reg Lsb = 16;
							constexpr Reg FieldMask = 0x00030000;
							constexpr Reg Value_Default = 0x0;
							constexpr Reg ValueMask_Default = 0x00000000;
							constexpr Reg Value_priorityLow = 0x0;
							constexpr Reg ValueMask_priorityLow = 0x00000000;
							constexpr Reg Value_priorityMedium = 0x1;
							constexpr Reg ValueMask_priorityMedium = 0x00010000;
							constexpr Reg Value_priorityHigh = 0x2;
							constexpr Reg ValueMask_priorityHigh = 0x00020000;
							constexpr Reg Value_priorityVeryHigh = 0x3;
							constexpr Reg ValueMask_priorityVeryHigh = 0x00030000;
							};
						namespace dbm { // Field Description
							constexpr Reg Lsb = 18;
							constexpr Reg FieldMask = 0x00040000;
							constexpr Reg Value_Default = 0x0;
							constexpr Reg ValueMask_Default = 0x00000000;
							constexpr Reg Value_bufferSwitchDisable = 0x0;
							constexpr Reg ValueMask_bufferSwitchDisable = 0x00000000;
							constexpr Reg Value_bufferSwitchEnable = 0x1;
							constexpr Reg ValueMask_bufferSwitchEnable = 0x00040000;
							};
						namespace ct { // Field Description
							constexpr Reg Lsb = 19;
							constexpr Reg FieldMask = 0x00080000;
							constexpr Reg Value_Default = 0x0;
							constexpr Reg ValueMask_Default = 0x00000000;
							constexpr Reg Value_targetMemory0 = 0x0;
							constexpr Reg ValueMask_targetMemory0 = 0x00000000;
							constexpr Reg Value_targetMemory1 = 0x1;
							constexpr Reg ValueMask_targetMemory1 = 0x00080000;
							};
						namespace pburst { // Field Description
							constexpr Reg Lsb = 21;
							constexpr Reg FieldMask = 0x00600000;
							constexpr Reg Value_Default = 0x0;
							constexpr Reg ValueMask_Default = 0x00000000;
							constexpr Reg Value_single = 0x0;
							constexpr Reg ValueMask_single = 0x00000000;
							constexpr Reg Value_incr4 = 0x1;
							constexpr Reg ValueMask_incr4 = 0x00200000;
							constexpr Reg Value_incr8 = 0x2;
							constexpr Reg ValueMask_incr8 = 0x00400000;
							constexpr Reg Value_incr16 = 0x3;
							constexpr Reg ValueMask_incr16 = 0x00600000;
							};
						namespace mburst { // Field Description
							constexpr Reg Lsb = 23;
							constexpr Reg FieldMask = 0x01800000;
							constexpr Reg Value_Default = 0x0;
							constexpr Reg ValueMask_Default = 0x00000000;
							constexpr Reg Value_single = 0x0;
							constexpr Reg ValueMask_single = 0x00000000;
							constexpr Reg Value_incr4 = 0x1;
							constexpr Reg ValueMask_incr4 = 0x00800000;
							constexpr Reg Value_incr8 = 0x2;
							constexpr Reg ValueMask_incr8 = 0x01000000;
							constexpr Reg Value_incr16 = 0x3;
							constexpr Reg ValueMask_incr16 = 0x01800000;
							};
						namespace chsel { // Field Description
							constexpr Reg Lsb = 25;
							constexpr Reg FieldMask = 0x1E000000;
							constexpr Reg Value_Default = 0x0;
							constexpr Reg ValueMask_Default = 0x00000000;
							constexpr Reg Value_channelMin = 0x0;
							constexpr Reg ValueMask_channelMin = 0x00000000;
							constexpr Reg Value_channelMax = 0xF;
							constexpr Reg ValueMask_channelMax = 0x1E000000;
							};
						};
					namespace Sxndtr { // Register description
						typedef uint32_t	Reg;
						namespace ndt { // Field Description
							constexpr Reg Lsb = 0;
							constexpr Reg FieldMask = 0x0000FFFF;
							constexpr Reg Value_Default = 0x0;
							constexpr Reg ValueMask_Default = 0x00000000;
							constexpr Reg Value_min = 0x0;
							constexpr Reg ValueMask_min = 0x00000000;
							constexpr Reg Value_max = 0xFFFF;
							constexpr Reg ValueMask_max = 0x0000FFFF;
							};
						};
					namespace Sxpar { // Register description
						typedef uint32_t	Reg;
						constexpr Reg	min = 0;
						constexpr Reg	max = -1;
						};
					namespace Sxm0ar { // Register description
						typedef uint32_t	Reg;
						constexpr Reg	min = 0;
						constexpr Reg	max = -1;
						};
					namespace Sxm1ar { // Register description
						typedef uint32_t	Reg;
						constexpr Reg	min = 0;
						constexpr Reg	max = -1;
						};
					namespace Sxfcr { // Register description
						typedef uint32_t	Reg;
						constexpr Reg	Default = 33;
						namespace fth { // Field Description
							constexpr Reg Lsb = 0;
							constexpr Reg FieldMask = 0x00000003;
							constexpr Reg Value_Default = 0x1;
							constexpr Reg ValueMask_Default = 0x00000001;
							constexpr Reg Value_fourthFullFIFO = 0x0;
							constexpr Reg ValueMask_fourthFullFIFO = 0x00000000;
							constexpr Reg Value_halfFullFIFO = 0x1;
							constexpr Reg ValueMask_halfFullFIFO = 0x00000001;
							constexpr Reg Value_threeFourthsFullFIFO = 0x2;
							constexpr Reg ValueMask_threeFourthsFullFIFO = 0x00000002;
							constexpr Reg Value_fullFIFO = 0x3;
							constexpr Reg ValueMask_fullFIFO = 0x00000003;
							};
						namespace dmdis { // Field Description
							constexpr Reg Lsb = 2;
							constexpr Reg FieldMask = 0x00000004;
							constexpr Reg Value_Default = 0x0;
							constexpr Reg ValueMask_Default = 0x00000000;
							constexpr Reg Value_directModeEnable = 0x0;
							constexpr Reg ValueMask_directModeEnable = 0x00000000;
							constexpr Reg Value_directModeDisable = 0x1;
							constexpr Reg ValueMask_directModeDisable = 0x00000004;
							};
						namespace fs { // Field Description
							constexpr Reg Lsb = 3;
							constexpr Reg FieldMask = 0x00000038;
							constexpr Reg Value_Default = 0x4;
							constexpr Reg ValueMask_Default = 0x00000020;
							constexpr Reg Value_gtZeroLtOneFourth = 0x0;
							constexpr Reg ValueMask_gtZeroLtOneFourth = 0x00000000;
							constexpr Reg Value_geOneFourthLtOneHalf = 0x1;
							constexpr Reg ValueMask_geOneFourthLtOneHalf = 0x00000008;
							constexpr Reg Value_geOneHalfLtThreeFourths = 0x2;
							constexpr Reg ValueMask_geOneHalfLtThreeFourths = 0x00000010;
							constexpr Reg Value_geThreeFourthsLtFull = 0x3;
							constexpr Reg ValueMask_geThreeFourthsLtFull = 0x00000018;
							constexpr Reg Value_empty = 0x4;
							constexpr Reg ValueMask_empty = 0x00000020;
							constexpr Reg Value_full = 0x5;
							constexpr Reg ValueMask_full = 0x00000028;
							};
						namespace feie { // Field Description
							constexpr Reg Lsb = 7;
							constexpr Reg FieldMask = 0x00000080;
							constexpr Reg Value_Default = 0x0;
							constexpr Reg ValueMask_Default = 0x00000000;
							constexpr Reg Value_interruptDisable = 0x0;
							constexpr Reg ValueMask_interruptDisable = 0x00000000;
							constexpr Reg Value_interruptEnable = 0x1;
							constexpr Reg ValueMask_interruptEnable = 0x00000080;
							};
						};
					}
				}
			}
		}
	}
#endif
