/*
   Copyright (C) 2024 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

module oscl_hw_st_stm32_f2f4_usart_regh {
sysinclude "stdint.h";

namespace Oscl {
	namespace St {
		namespace Stm32 {
			namespace F2F4 {
				namespace Usart {
					register Sr uint32_t {
						/*	This bit is set by hardware when a parity error occurs
							in receiver mode. It is cleared by a software sequence
							(a read from the status register followed by a read or
							write access to the USART_DR data register).
							The software must wait for the RXNE flag to be set before
							clearing the PE bit.
							An interrupt is generated if PEIE = 1 in the USART_CR1
							register.
							[ r ]
						 */
						field pe 0 1 {
							value Default	0
							value noError	0
							value error		1
							}
						/*	This bit is set by hardware when a de-synchronization,
							excessive noise or a break character is detected.
							It is cleared by a software sequence (an read to the
							USART_SR register followed by a read to the USART_DR
							register).

							Note: This bit does not generate interrupt as it appears
							at the same time as the RXNE bit which itself generates
							an interrupt. If the word currently being transferred
							causes both frame error and overrun error, it will be
							transferred and only the ORE bit will be set.
							An interrupt is generated on FE flag in case of Multi
							Buffer communication if the EIE bit is set.
							[ r ]
						 */
						field fe 1 1 {
							value Default	0
							value noError	0
							value error		1
							}
						/*	This bit is set by hardware when noise is detected on a
							received frame. It is cleared by a software sequence
							(an read to the USART_SR register followed by a read to
							the USART_DR register).

							Note: This bit does not generate interrupt as it appears
							at the same time as the RXNE bit which itself generates
							an interrupting interrupt is generated on NF flag in
							case of Multi Buffer communication if the EIE bit is set.

							Note: When the line is noise-free, the NF flag can be
							disabled by programming the ONEBIT bit to 1 to increase
							the USART tolerance to deviations
							[ r ]
						 */
						field nf 2 1 {
							value Default		0
							value notDetected	0
							value detected		1
							}
						/*	This bit is set by hardware when the word currently being
							received in the shift register is ready to be transferred
							into the RDR register while RXNE=1. An interrupt is
							generated if RXNEIE=1 in the USART_CR1 register. It is
							cleared by a software sequence (an read to the USART_SR
							register followed by a read to the USART_DR register).
							[ r ]
						 */
						field ore 3 1 {
							value Default	0
							value noError	0
							value error		1
							}
						/*	This bit is set by hardware when an Idle Line is detected.
							An interrupt is generated if the IDLEIE=1 in the USART_CR1
							register. It is cleared by a software sequence (an read to
							the USART_SR register followed by a read to the USART_DR
							register).
							Note: The IDLE bit will not be set again until the RXNE bit
							has been set itself (i.e. a new idle line occurs).
							[ r ]
						 */
						field idle 4 1 {
							value Default		0
							value notDetected	0
							value detected		1
							}
						/*	This bit is set by hardware when the content of the
							RDR shift register has been transferred to the USART_DR
							register. An interrupt is generated if RXNEIE=1 in the
							USART_CR1 register. It is cleared by a read to the
							USART_DR register. The RXNE flag can also be cleared by
							writing a zero to it. This clearing sequence is
							recommended only for multibuffer communication.
							[ rc_w0 ]
						 */
						field rxne 5 1 {
							value Default		0
							value notReceived	0
							value received		1
							value clear			0
							value keep			1
							}
						/*	This bit is set by hardware if the transmission of a frame
							containing data is complete and if TXE is set. An interrupt
							is generated if TCIE=1 in the USART_CR1 register.
							It is cleared by a software sequence (a read from the USART_SR
							register followed by a write to the USART_DR register).
							The TC bit can also be cleared by writing a '0' to it.
							This clearing sequence is recommended only for multibuffer
							communication.
							[ rc_w0 ]
						 */
						field tc 6 1 {
							value Default			1
							value notComplete		0
							value complete			1
							value clear				0
							value keep				1
							}
						/*	This bit is set by hardware when the content of the TDR register
							has been transferred into the shift register. An interrupt is
							generated if the TXEIE bit =1 in the USART_CR1 register.
							It is cleared by a write to the USART_DR register.
							[ r ]
						 */
						field txe 7 1 {
							value Default			1
							value notTransferred	0
							value transferred		1
							}
						/*	This bit is set by hardware when the LIN break is detected.
							It is cleared by software (by writing it to 0).
							An interrupt is generated if LBDIE = 1 in the USART_CR2 register.
							[ rc_w0 ]
						 */
						field lbd 8 1 {
							value Default			0
							value notDetected		0
							value detected			0
							value clear				0
							value keep				1
							}
						/*	This bit is set by hardware when the CTS input toggles,
							if the CTSE bit is set.  It is cleared by software (by writing it to 0).
							An interrupt is generated if CTSIE=1 in the USART_CR3 register.
							[ rc_w0 ]
						 */
						field cts 9 1 {
							value Default		0
							value notChanged	0
							value changed		1
							value clear			0
							value keep			1
							}
						}
					register Dr uint32_t {
						/*	Contains the Received or Transmitted data character,
							depending on whether it is read from or written to.
							The Data register performs a double function (read and write)
							since it is composed of two registers, one for transmission
							(TDR) and one for reception (RDR).
							The TDR register provides the parallel interface between the
							internal bus and the output shift register.
							The RDR register provides the parallel interface between the
							input shift register and the internal bus.
							When transmitting with the parity enabled (PCE bit set to 1
							in the USART_CR1 register), the value written in the MSB
							(bit 7 or bit 8 depending on the data length) has no effect
							because it is replaced by the parity.
							When receiving with the parity enabled, the value read in
							the MSB bit is the received parity bit.
						 */
						field dr 0 9 {
							value Default		0
							}
						}
					/*	The baud counters stop counting if the TE or RE bits are
						disabled respectively.
					 */
					register Brr uint32_t {
						/*	These 4 bits define the fraction of the USART Divider
							(USARTDIV). When OVER8=1, the DIV_Fraction3 bit is not
							considered and must be kept cleared.
							[ rw ]
						 */
						field Div_Fraction 0 4 {
							value Default	0
							}
						/*	These 12 bits define the mantissa of the USART
							Divider (USARTDIV).
							[ rw ]
						 */
						field DIV_Mantissa 4 12 {
							value Default	0
							}
						}
					register Cr1 uint32_t {
						/*	This bit set is used to send break characters.
							It can be set and cleared by software. It should
							be set by software, and will be reset by hardware
							during the stop bit of break.
							[ rw ]
						 */
						field sbk 0 1 {
							value Default	0
							value notSendingBreak	0
							value sendingBreak		1
							value stopSendingBreak	0
							value sendBreak			1
							}
						/*	Receiver wakeup
							This bit determines if the USART is in mute mode or not.
							It is set and cleared by software and can be cleared by
							hardware when a wakeup sequence is recognized.

							Note: Before selecting Mute mode (by setting the RWU bit)
							the USART must first receive a data byte, otherwise it
							cannot function in Mute mode with wakeup by Idle line detection.
							In Address Mark Detection wakeup configuration (WAKE bit=1)
							the RWU bit cannot be modified by software while the RXNE
							bit is set.
							[ rw ]
						 */
						field rwu 1 1 {
							value Default	0
							value active	0
							value mute		1
							}
						/*	Receiver enable
							This bit enables the receiver.
							It is set and cleared by software.
							[ rw ]
						 */
						field re 2 1 {
							value Default	0
							value disabled	0
							value enabled	1
							value disable	0
							value enable	1
							}
						/*	Transmitter enable
							This bit enables the transmitter.
							It is set and cleared by software.
							[ rw ]
						 */
						field te 3 1 {
							value Default	0
							value disabled	0
							value enabled	1
							value disable	0
							value enable	1
							}
						/*	IDLE interrupt enable
							This bit is set and cleared by software.
							[ rw ]
						 */
						field idleie 4 1 {
							value Default	0
							value disabled	0
							value enabled	1
							value disable	0
							value enable	1
							}
						/*	RXNE interrupt enable
							This bit is set and cleared by software.
							[ rw ]
						 */
						field rxneie 5 1 {
							value Default	0
							value disabled	0
							value enabled	1
							value disable	0
							value enable	1
							}
						/*	Transmission complete interrupt enable.
							This bit is set and cleared by software.
							[ rw ]
						 */
						field tcie 6 1 {
							value Default	0
							value disabled	0
							value enabled	1
							value disable	0
							value enable	1
							}
						/*	TXE interrupt enable.
							This bit is set and cleared by software.
							[ rw ]
						 */
						field txeie 7 1 {
							value Default	0
							value disabled	0
							value enabled	1
							value disable	0
							value enable	1
							}
						/*	This bit is set and cleared by software.
							[ rw ]
						 */
						field peie 8 1 {
							value Default	0
							value disabled	0
							value enabled	1
							value disable	0
							value enable	1
							}
						/*	This bit selects the odd or even parity when the parity
							generation/detection is enabled (PCE bit set).
							It is set and cleared by software.
							The parity will be selected after the current byte.
							[ rw ]
						 */
						field ps 9 1 {
							value Default	0
							value even		0
							value odd		1
							}
						/*	This bit selects the hardware parity control
							(generation and detection). When the parity control is enabled,
							the computed parity is inserted at the MSB position
							(9th bit if M=1; 8th bit if M=0) and parity is checked on the
							received data. This bit is set and cleared by software.
							Once it is set, PCE is active after the current byte
							(in reception and in transmission).
							[ rw ]
						 */
						field pce 10 1 {
							value Default	0
							value disabled	0
							value enabled	1
							value disable	0
							value enable	1
							}
						/*	This bit determines the USART wakeup method,
							it is set or cleared by software.
							[ rw ]
						 */
						field wake 11 1 {
							value Default			0
							value wakeOnIdleLine	0
							value wakeOnAddressMark	0
							}
						/*	This bit determines the word length. It is set or
							cleared by software.
							[ rw ]
						 */
						field m 12 1 {
							value Default		0
							value eightBitWords	0
							value nineBitWords	1
							}
						/*	When this bit is cleared, the USART prescalers and
							outputs are stopped and the end of the current byte
							transfer in order to reduce power consumption.
							This bit is set and cleared by software.
							[ rw ]
						 */
						field ue 13 1 {
							value Default	0
							value disabled	0
							value enabled	1
							value disable	0
							value enable	1
							}
						/*	Oversampling by 8 is not available in the Smartcard,
							IrDA and LIN modes: when SCEN=1,IREN=1 or LINEN=1 then
							OVER8 is forced to 0 by hardware.
							[ rw ]
						 */
						field over8 15 1 {
							value Default	0
							value oversampleBy16	0
							value oversampleBy8		1
							}
						}
					register Cr2 uint32_t {
						/*	Address of the USART node
							This bit-field gives the address of the USART node.
							This is used in multiprocessor communication during
							mute mode, for wake up with address mark detection.
							[ rw ]
						 */
						field add 0 4 {
							value Default	0
							value max		0x0F
							}
						/*	LIN break detection length
							This bit is for selection between 11 bit or
							10 bit break detection.
							[ rw ]
						 */
						field lbdl 5 1 {
							value Default				0
							value tenBitDetection		0
							value elevenBitDetection	1
							}
						/*	LIN break detection interrupt enable
							Break interrupt mask (break detection using break delimiter).
							[ rw ]
						 */
						field lbdie 6 1 {
							value Default	0
							value disabled	0
							value enabled	1
							value disable	0
							value enable	1
							}
						/*	Last bit clock pulse
							This bit allows the user to select whether the clock
							pulse associated with the last data bit transmitted (MSB)
							has to be output on the CK pin in synchronous mode.
							Note:
								1:	The last bit is the 8th or 9th data bit transmitted
									depending on the 8 or 9 bit format selected by the
									M bit in the USART_CR1 register.
								2: This bit is not available for UART4 & UART5.
							[ rw ]
						 */
						field lbcl 8 1 {
							value Default		0
							value noOutputPulse	0
							value outputPulse	1
							}
						/*	Clock phase
							This bit allows the user to select the phase of the
							clock output on the CK pin in synchronous mode.
							It works in conjunction with the CPOL bit to produce
							the desired clock/data relationship.
							This bit is not available for UART4 & UART5.
							[ rw ]
						 */
						field cpha 9 1 {
							value Default	0
							value firstTransitionCapture	0
							value secondTransitionCapture	1
							}
						/*	Clock polarity
							This bit allows the user to select the polarity of the
							clock output on the CK pin in synchronous mode.
							It works in conjunction with the CPHA bit to produce
							the desired clock/data relationship.
							This bit is not available for UART4 & UART5.
							[ rw ]
						 */
						field cpol 10 1 {
							value Default	0
							value steadyLowOutsideTxWindow	0
							value steadyHighOutsideTxWindow	1
							}
						/*	Clock enable
							This bit allows the user to enable the CK pin.
							This bit is not available for UART4 & UART5.
							[ rw ]
						 */
						field clken 11 1 {
							value Default	0
							value disabled	0
							value enabled	1
							value disable	0
							value enable	1
							}
						/*	STOP bits
							These bits are used for programming the stop bits.
							Note: The 0.5 Stop bit and 1.5 Stop bit are not
							available for UART4 & UART5.
							[ rw ]
						 */
						field stop 12 2 {
							value Default	0
							value oneStopBit			0
							value zeroPointFiveStopBits	1
							value twoStopBits			2
							value onePointFiveStopBits	3
							}
						/*	LIN mode enable
							This bit is set and cleared by software.
							The LIN mode enables the capability to send LIN
							Synch Breaks (13 low bits) using the SBK bit in
							the USART_CR1 register, and to detect LIN Sync breaks.
							[ rw ]
						 */
						field linen 14 1 {
							value Default	0
							value disabled	0
							value enabled	1
							value disable	0
							value enable	1
							}
						}
					register Cr3 uint32_t {
						/*	Error interrupt enable
							Error Interrupt Enable Bit is required to enable interrupt
							generation in case of a framing error, overrun error or
							noise flag (FE=1 or ORE=1 or NF=1 in the USART_SR register)
							in case of Multi Buffer Communication
							(DMAR=1 in the USART_CR3 register).
							[ rw ]
						 */
						field eie 0 1 {
							value Default	0
							value disabled	0
							value enabled	1
							value disable	0
							value enable	1
							}
						/*	IrDA mode enable
							This bit is set and cleared by software.
							[ rw ]
						 */
						field iren 1 1 {
							value Default	0
							value disabled	0
							value enabled	1
							value disable	0
							value enable	1
							}
						/*	 IrDA low-power
							This bit is used for selecting between normal and
							low-power IrDA modes
							[ rw ]
						 */
						field irlp 2 1 {
							value Default		0
							value normalMode	0
							value lowPowerMode	1
							}
						/*	Half-duplex selection
							Selection of Single-wire Half-duplex mode.
							[ rw ]
						 */
						field hdsel 3 1 {
							value Default	0
							value disabled	0
							value enabled	1
							value disable	0
							value enable	1
							}
						/*	Smartcard NACK enable
							Note: This bit is not available for UART4 & UART5.
							[ rw ]
						 */
						field nack 4 1 {
							value Default	0
							value disabled	0
							value enabled	1
							value disable	0
							value enable	1
							}
						/*	Smartcard mode enable
							This bit is used for enabling Smartcard mode.
							This bit is not available for UART4 & UART5.
							[ rw ]
						 */
						field scen 5 1 {
							value Default	0
							value disabled	0
							value enabled	1
							value disable	0
							value enable	1
							}
						/*	DMA enable receiver
							This bit is set/reset by software.
							[ rw ]
						 */
						field dmar 6 1 {
							value Default	0
							value disabled	0
							value enabled	1
							value disable	0
							value enable	1
							}
						/*	DMA enable transmitter
							This bit is set/reset by software.
							[ rw ]
						 */
						field dmat 7 1 {
							value Default	0
							value disabled	0
							value enabled	1
							value disable	0
							value enable	1
							}
						/*	RTS enable
							RTS interrupt enabled, data is only requested when
							there is space in the receive buffer.
							The transmission of data is expected to cease after
							the current character has been transmitted.
							The RTS output is asserted (tied to 0) when a data
							can be received.
							Note: This bit is not available for UART4 & UART5.
							[ rw ]
						 */
						field rtse 8 1 {
							value Default	0
							value disabled	0
							value enabled	1
							value disable	0
							value enable	1
							}
						/*	CTS enable
							If the CTS input is deasserted while a data is being transmitted,
							then the transmission is completed before stopping.
							If a data is written into the data register while CTS is
							deasserted, the transmission is postponed until CTS is asserted.
							Note: This bit is not available for UART4 & UART5.
							[ rw ]
						 */
						field ctse 9 1 {
							value Default	0
							value disabled	0
							value enabled	1
							value disable	0
							value enable	1
							}
						/*	CTS interrupt enable
							Note: This bit is not available for UART4 & UART5.
							[ rw ]
						 */
						field ctsie 10 1 {
							value Default	0
							value disabled	0
							value enabled	1
							value disable	0
							value enable	1
							}
						/*	One sample bit method enable
							This bit allows the user to select the sample method.
							When the one sample bit method is selected the noise
							detection flag (NF) is disabled.
							Note: The ONEBIT feature applies only to data bits.
							It does not apply to START bit.
							[ rw ]
						 */
						field onebit 11 1 {
							value Default	0
							value threeSampleBits	0
							value oneSampleBit		1
							}
						}
					register Gtpr uint32_t {
						/*	Prescaler value
							– In IrDA Low-power mode:
								PSC[7:0] = IrDA Low-Power Baud Rate
								Used for programming the prescaler for dividing
								the system clock to achieve the low-power frequency:
								The source clock is divided by the value given in
								the register (8 significant bits):
								00000000: Reserved - do not program this value
								00000001: divides the source clock by 1
								00000010: divides the source clock by 2
								...
							– In normal IrDA mode: PSC must be set to 00000001.
							– In smartcard mode:
								PSC[4:0]: Prescaler value
								Used for programming the prescaler for dividing
								the system clock to provide the smartcard clock.
								The value given in the register (5 significant bits)
								is multiplied by 2 to give the division factor of
								the source clock frequency:
								00000: Reserved - do not program this value
								00001: divides the source clock by 2
								00010: divides the source clock by 4
								00011: divides the source clock by 6
								...
							Note:
								1: Bits [7:5] have no effect if Smartcard mode is used.
								2: This bit is not available for UART4 & UART5.
							[ rw ]
						 */
						field psc 0 8 {
							value Default	0
							value max		0x0FF
							}
						/*	Guard time value
							This bit-field gives the Guard time value in terms of
							number of baud clocks.
							This is used in Smartcard mode.
							The Transmission Complete flag is set after this guard
							time value.
							Note: This bit is not available for UART4 & UART5.
							[ rw ]
						 */
						field gt 8 8 {
							value Default	0
							value max		0x0FF
							}
						}
					}
				}
			}
		}
	}
}

