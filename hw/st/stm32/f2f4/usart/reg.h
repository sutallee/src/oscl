#ifndef _oscl_hw_st_stm32_f2f4_usart_regh_
#define _oscl_hw_st_stm32_f2f4_usart_regh_
#include <stdint.h>
namespace Oscl { // Namespace description
	namespace St { // Namespace description
		namespace Stm32 { // Namespace description
			namespace F2F4 { // Namespace description
				namespace Usart { // Namespace description
					namespace Sr { // Register description
						typedef uint32_t	Reg;
						namespace pe { // Field Description
							constexpr Reg Lsb = 0;
							constexpr Reg FieldMask = 0x00000001;
							constexpr Reg Value_Default = 0x0;
							constexpr Reg ValueMask_Default = 0x00000000;
							constexpr Reg Value_noError = 0x0;
							constexpr Reg ValueMask_noError = 0x00000000;
							constexpr Reg Value_error = 0x1;
							constexpr Reg ValueMask_error = 0x00000001;
							};
						namespace fe { // Field Description
							constexpr Reg Lsb = 1;
							constexpr Reg FieldMask = 0x00000002;
							constexpr Reg Value_Default = 0x0;
							constexpr Reg ValueMask_Default = 0x00000000;
							constexpr Reg Value_noError = 0x0;
							constexpr Reg ValueMask_noError = 0x00000000;
							constexpr Reg Value_error = 0x1;
							constexpr Reg ValueMask_error = 0x00000002;
							};
						namespace nf { // Field Description
							constexpr Reg Lsb = 2;
							constexpr Reg FieldMask = 0x00000004;
							constexpr Reg Value_Default = 0x0;
							constexpr Reg ValueMask_Default = 0x00000000;
							constexpr Reg Value_notDetected = 0x0;
							constexpr Reg ValueMask_notDetected = 0x00000000;
							constexpr Reg Value_detected = 0x1;
							constexpr Reg ValueMask_detected = 0x00000004;
							};
						namespace ore { // Field Description
							constexpr Reg Lsb = 3;
							constexpr Reg FieldMask = 0x00000008;
							constexpr Reg Value_Default = 0x0;
							constexpr Reg ValueMask_Default = 0x00000000;
							constexpr Reg Value_noError = 0x0;
							constexpr Reg ValueMask_noError = 0x00000000;
							constexpr Reg Value_error = 0x1;
							constexpr Reg ValueMask_error = 0x00000008;
							};
						namespace idle { // Field Description
							constexpr Reg Lsb = 4;
							constexpr Reg FieldMask = 0x00000010;
							constexpr Reg Value_Default = 0x0;
							constexpr Reg ValueMask_Default = 0x00000000;
							constexpr Reg Value_notDetected = 0x0;
							constexpr Reg ValueMask_notDetected = 0x00000000;
							constexpr Reg Value_detected = 0x1;
							constexpr Reg ValueMask_detected = 0x00000010;
							};
						namespace rxne { // Field Description
							constexpr Reg Lsb = 5;
							constexpr Reg FieldMask = 0x00000020;
							constexpr Reg Value_Default = 0x0;
							constexpr Reg ValueMask_Default = 0x00000000;
							constexpr Reg Value_notReceived = 0x0;
							constexpr Reg ValueMask_notReceived = 0x00000000;
							constexpr Reg Value_received = 0x1;
							constexpr Reg ValueMask_received = 0x00000020;
							constexpr Reg Value_clear = 0x0;
							constexpr Reg ValueMask_clear = 0x00000000;
							constexpr Reg Value_keep = 0x1;
							constexpr Reg ValueMask_keep = 0x00000020;
							};
						namespace tc { // Field Description
							constexpr Reg Lsb = 6;
							constexpr Reg FieldMask = 0x00000040;
							constexpr Reg Value_Default = 0x1;
							constexpr Reg ValueMask_Default = 0x00000040;
							constexpr Reg Value_notComplete = 0x0;
							constexpr Reg ValueMask_notComplete = 0x00000000;
							constexpr Reg Value_complete = 0x1;
							constexpr Reg ValueMask_complete = 0x00000040;
							constexpr Reg Value_clear = 0x0;
							constexpr Reg ValueMask_clear = 0x00000000;
							constexpr Reg Value_keep = 0x1;
							constexpr Reg ValueMask_keep = 0x00000040;
							};
						namespace txe { // Field Description
							constexpr Reg Lsb = 7;
							constexpr Reg FieldMask = 0x00000080;
							constexpr Reg Value_Default = 0x1;
							constexpr Reg ValueMask_Default = 0x00000080;
							constexpr Reg Value_notTransferred = 0x0;
							constexpr Reg ValueMask_notTransferred = 0x00000000;
							constexpr Reg Value_transferred = 0x1;
							constexpr Reg ValueMask_transferred = 0x00000080;
							};
						namespace lbd { // Field Description
							constexpr Reg Lsb = 8;
							constexpr Reg FieldMask = 0x00000100;
							constexpr Reg Value_Default = 0x0;
							constexpr Reg ValueMask_Default = 0x00000000;
							constexpr Reg Value_notDetected = 0x0;
							constexpr Reg ValueMask_notDetected = 0x00000000;
							constexpr Reg Value_detected = 0x0;
							constexpr Reg ValueMask_detected = 0x00000000;
							constexpr Reg Value_clear = 0x0;
							constexpr Reg ValueMask_clear = 0x00000000;
							constexpr Reg Value_keep = 0x1;
							constexpr Reg ValueMask_keep = 0x00000100;
							};
						namespace cts { // Field Description
							constexpr Reg Lsb = 9;
							constexpr Reg FieldMask = 0x00000200;
							constexpr Reg Value_Default = 0x0;
							constexpr Reg ValueMask_Default = 0x00000000;
							constexpr Reg Value_notChanged = 0x0;
							constexpr Reg ValueMask_notChanged = 0x00000000;
							constexpr Reg Value_changed = 0x1;
							constexpr Reg ValueMask_changed = 0x00000200;
							constexpr Reg Value_clear = 0x0;
							constexpr Reg ValueMask_clear = 0x00000000;
							constexpr Reg Value_keep = 0x1;
							constexpr Reg ValueMask_keep = 0x00000200;
							};
						};
					namespace Dr { // Register description
						typedef uint32_t	Reg;
						namespace dr { // Field Description
							constexpr Reg Lsb = 0;
							constexpr Reg FieldMask = 0x000001FF;
							constexpr Reg Value_Default = 0x0;
							constexpr Reg ValueMask_Default = 0x00000000;
							};
						};
					namespace Brr { // Register description
						typedef uint32_t	Reg;
						namespace Div_Fraction { // Field Description
							constexpr Reg Lsb = 0;
							constexpr Reg FieldMask = 0x0000000F;
							constexpr Reg Value_Default = 0x0;
							constexpr Reg ValueMask_Default = 0x00000000;
							};
						namespace DIV_Mantissa { // Field Description
							constexpr Reg Lsb = 4;
							constexpr Reg FieldMask = 0x0000FFF0;
							constexpr Reg Value_Default = 0x0;
							constexpr Reg ValueMask_Default = 0x00000000;
							};
						};
					namespace Cr1 { // Register description
						typedef uint32_t	Reg;
						namespace sbk { // Field Description
							constexpr Reg Lsb = 0;
							constexpr Reg FieldMask = 0x00000001;
							constexpr Reg Value_Default = 0x0;
							constexpr Reg ValueMask_Default = 0x00000000;
							constexpr Reg Value_notSendingBreak = 0x0;
							constexpr Reg ValueMask_notSendingBreak = 0x00000000;
							constexpr Reg Value_sendingBreak = 0x1;
							constexpr Reg ValueMask_sendingBreak = 0x00000001;
							constexpr Reg Value_stopSendingBreak = 0x0;
							constexpr Reg ValueMask_stopSendingBreak = 0x00000000;
							constexpr Reg Value_sendBreak = 0x1;
							constexpr Reg ValueMask_sendBreak = 0x00000001;
							};
						namespace rwu { // Field Description
							constexpr Reg Lsb = 1;
							constexpr Reg FieldMask = 0x00000002;
							constexpr Reg Value_Default = 0x0;
							constexpr Reg ValueMask_Default = 0x00000000;
							constexpr Reg Value_active = 0x0;
							constexpr Reg ValueMask_active = 0x00000000;
							constexpr Reg Value_mute = 0x1;
							constexpr Reg ValueMask_mute = 0x00000002;
							};
						namespace re { // Field Description
							constexpr Reg Lsb = 2;
							constexpr Reg FieldMask = 0x00000004;
							constexpr Reg Value_Default = 0x0;
							constexpr Reg ValueMask_Default = 0x00000000;
							constexpr Reg Value_disabled = 0x0;
							constexpr Reg ValueMask_disabled = 0x00000000;
							constexpr Reg Value_enabled = 0x1;
							constexpr Reg ValueMask_enabled = 0x00000004;
							constexpr Reg Value_disable = 0x0;
							constexpr Reg ValueMask_disable = 0x00000000;
							constexpr Reg Value_enable = 0x1;
							constexpr Reg ValueMask_enable = 0x00000004;
							};
						namespace te { // Field Description
							constexpr Reg Lsb = 3;
							constexpr Reg FieldMask = 0x00000008;
							constexpr Reg Value_Default = 0x0;
							constexpr Reg ValueMask_Default = 0x00000000;
							constexpr Reg Value_disabled = 0x0;
							constexpr Reg ValueMask_disabled = 0x00000000;
							constexpr Reg Value_enabled = 0x1;
							constexpr Reg ValueMask_enabled = 0x00000008;
							constexpr Reg Value_disable = 0x0;
							constexpr Reg ValueMask_disable = 0x00000000;
							constexpr Reg Value_enable = 0x1;
							constexpr Reg ValueMask_enable = 0x00000008;
							};
						namespace idleie { // Field Description
							constexpr Reg Lsb = 4;
							constexpr Reg FieldMask = 0x00000010;
							constexpr Reg Value_Default = 0x0;
							constexpr Reg ValueMask_Default = 0x00000000;
							constexpr Reg Value_disabled = 0x0;
							constexpr Reg ValueMask_disabled = 0x00000000;
							constexpr Reg Value_enabled = 0x1;
							constexpr Reg ValueMask_enabled = 0x00000010;
							constexpr Reg Value_disable = 0x0;
							constexpr Reg ValueMask_disable = 0x00000000;
							constexpr Reg Value_enable = 0x1;
							constexpr Reg ValueMask_enable = 0x00000010;
							};
						namespace rxneie { // Field Description
							constexpr Reg Lsb = 5;
							constexpr Reg FieldMask = 0x00000020;
							constexpr Reg Value_Default = 0x0;
							constexpr Reg ValueMask_Default = 0x00000000;
							constexpr Reg Value_disabled = 0x0;
							constexpr Reg ValueMask_disabled = 0x00000000;
							constexpr Reg Value_enabled = 0x1;
							constexpr Reg ValueMask_enabled = 0x00000020;
							constexpr Reg Value_disable = 0x0;
							constexpr Reg ValueMask_disable = 0x00000000;
							constexpr Reg Value_enable = 0x1;
							constexpr Reg ValueMask_enable = 0x00000020;
							};
						namespace tcie { // Field Description
							constexpr Reg Lsb = 6;
							constexpr Reg FieldMask = 0x00000040;
							constexpr Reg Value_Default = 0x0;
							constexpr Reg ValueMask_Default = 0x00000000;
							constexpr Reg Value_disabled = 0x0;
							constexpr Reg ValueMask_disabled = 0x00000000;
							constexpr Reg Value_enabled = 0x1;
							constexpr Reg ValueMask_enabled = 0x00000040;
							constexpr Reg Value_disable = 0x0;
							constexpr Reg ValueMask_disable = 0x00000000;
							constexpr Reg Value_enable = 0x1;
							constexpr Reg ValueMask_enable = 0x00000040;
							};
						namespace txeie { // Field Description
							constexpr Reg Lsb = 7;
							constexpr Reg FieldMask = 0x00000080;
							constexpr Reg Value_Default = 0x0;
							constexpr Reg ValueMask_Default = 0x00000000;
							constexpr Reg Value_disabled = 0x0;
							constexpr Reg ValueMask_disabled = 0x00000000;
							constexpr Reg Value_enabled = 0x1;
							constexpr Reg ValueMask_enabled = 0x00000080;
							constexpr Reg Value_disable = 0x0;
							constexpr Reg ValueMask_disable = 0x00000000;
							constexpr Reg Value_enable = 0x1;
							constexpr Reg ValueMask_enable = 0x00000080;
							};
						namespace peie { // Field Description
							constexpr Reg Lsb = 8;
							constexpr Reg FieldMask = 0x00000100;
							constexpr Reg Value_Default = 0x0;
							constexpr Reg ValueMask_Default = 0x00000000;
							constexpr Reg Value_disabled = 0x0;
							constexpr Reg ValueMask_disabled = 0x00000000;
							constexpr Reg Value_enabled = 0x1;
							constexpr Reg ValueMask_enabled = 0x00000100;
							constexpr Reg Value_disable = 0x0;
							constexpr Reg ValueMask_disable = 0x00000000;
							constexpr Reg Value_enable = 0x1;
							constexpr Reg ValueMask_enable = 0x00000100;
							};
						namespace ps { // Field Description
							constexpr Reg Lsb = 9;
							constexpr Reg FieldMask = 0x00000200;
							constexpr Reg Value_Default = 0x0;
							constexpr Reg ValueMask_Default = 0x00000000;
							constexpr Reg Value_even = 0x0;
							constexpr Reg ValueMask_even = 0x00000000;
							constexpr Reg Value_odd = 0x1;
							constexpr Reg ValueMask_odd = 0x00000200;
							};
						namespace pce { // Field Description
							constexpr Reg Lsb = 10;
							constexpr Reg FieldMask = 0x00000400;
							constexpr Reg Value_Default = 0x0;
							constexpr Reg ValueMask_Default = 0x00000000;
							constexpr Reg Value_disabled = 0x0;
							constexpr Reg ValueMask_disabled = 0x00000000;
							constexpr Reg Value_enabled = 0x1;
							constexpr Reg ValueMask_enabled = 0x00000400;
							constexpr Reg Value_disable = 0x0;
							constexpr Reg ValueMask_disable = 0x00000000;
							constexpr Reg Value_enable = 0x1;
							constexpr Reg ValueMask_enable = 0x00000400;
							};
						namespace wake { // Field Description
							constexpr Reg Lsb = 11;
							constexpr Reg FieldMask = 0x00000800;
							constexpr Reg Value_Default = 0x0;
							constexpr Reg ValueMask_Default = 0x00000000;
							constexpr Reg Value_wakeOnIdleLine = 0x0;
							constexpr Reg ValueMask_wakeOnIdleLine = 0x00000000;
							constexpr Reg Value_wakeOnAddressMark = 0x0;
							constexpr Reg ValueMask_wakeOnAddressMark = 0x00000000;
							};
						namespace m { // Field Description
							constexpr Reg Lsb = 12;
							constexpr Reg FieldMask = 0x00001000;
							constexpr Reg Value_Default = 0x0;
							constexpr Reg ValueMask_Default = 0x00000000;
							constexpr Reg Value_eightBitWords = 0x0;
							constexpr Reg ValueMask_eightBitWords = 0x00000000;
							constexpr Reg Value_nineBitWords = 0x1;
							constexpr Reg ValueMask_nineBitWords = 0x00001000;
							};
						namespace ue { // Field Description
							constexpr Reg Lsb = 13;
							constexpr Reg FieldMask = 0x00002000;
							constexpr Reg Value_Default = 0x0;
							constexpr Reg ValueMask_Default = 0x00000000;
							constexpr Reg Value_disabled = 0x0;
							constexpr Reg ValueMask_disabled = 0x00000000;
							constexpr Reg Value_enabled = 0x1;
							constexpr Reg ValueMask_enabled = 0x00002000;
							constexpr Reg Value_disable = 0x0;
							constexpr Reg ValueMask_disable = 0x00000000;
							constexpr Reg Value_enable = 0x1;
							constexpr Reg ValueMask_enable = 0x00002000;
							};
						namespace over8 { // Field Description
							constexpr Reg Lsb = 15;
							constexpr Reg FieldMask = 0x00008000;
							constexpr Reg Value_Default = 0x0;
							constexpr Reg ValueMask_Default = 0x00000000;
							constexpr Reg Value_oversampleBy16 = 0x0;
							constexpr Reg ValueMask_oversampleBy16 = 0x00000000;
							constexpr Reg Value_oversampleBy8 = 0x1;
							constexpr Reg ValueMask_oversampleBy8 = 0x00008000;
							};
						};
					namespace Cr2 { // Register description
						typedef uint32_t	Reg;
						namespace add { // Field Description
							constexpr Reg Lsb = 0;
							constexpr Reg FieldMask = 0x0000000F;
							constexpr Reg Value_Default = 0x0;
							constexpr Reg ValueMask_Default = 0x00000000;
							constexpr Reg Value_max = 0xF;
							constexpr Reg ValueMask_max = 0x0000000F;
							};
						namespace lbdl { // Field Description
							constexpr Reg Lsb = 5;
							constexpr Reg FieldMask = 0x00000020;
							constexpr Reg Value_Default = 0x0;
							constexpr Reg ValueMask_Default = 0x00000000;
							constexpr Reg Value_tenBitDetection = 0x0;
							constexpr Reg ValueMask_tenBitDetection = 0x00000000;
							constexpr Reg Value_elevenBitDetection = 0x1;
							constexpr Reg ValueMask_elevenBitDetection = 0x00000020;
							};
						namespace lbdie { // Field Description
							constexpr Reg Lsb = 6;
							constexpr Reg FieldMask = 0x00000040;
							constexpr Reg Value_Default = 0x0;
							constexpr Reg ValueMask_Default = 0x00000000;
							constexpr Reg Value_disabled = 0x0;
							constexpr Reg ValueMask_disabled = 0x00000000;
							constexpr Reg Value_enabled = 0x1;
							constexpr Reg ValueMask_enabled = 0x00000040;
							constexpr Reg Value_disable = 0x0;
							constexpr Reg ValueMask_disable = 0x00000000;
							constexpr Reg Value_enable = 0x1;
							constexpr Reg ValueMask_enable = 0x00000040;
							};
						namespace lbcl { // Field Description
							constexpr Reg Lsb = 8;
							constexpr Reg FieldMask = 0x00000100;
							constexpr Reg Value_Default = 0x0;
							constexpr Reg ValueMask_Default = 0x00000000;
							constexpr Reg Value_noOutputPulse = 0x0;
							constexpr Reg ValueMask_noOutputPulse = 0x00000000;
							constexpr Reg Value_outputPulse = 0x1;
							constexpr Reg ValueMask_outputPulse = 0x00000100;
							};
						namespace cpha { // Field Description
							constexpr Reg Lsb = 9;
							constexpr Reg FieldMask = 0x00000200;
							constexpr Reg Value_Default = 0x0;
							constexpr Reg ValueMask_Default = 0x00000000;
							constexpr Reg Value_firstTransitionCapture = 0x0;
							constexpr Reg ValueMask_firstTransitionCapture = 0x00000000;
							constexpr Reg Value_secondTransitionCapture = 0x1;
							constexpr Reg ValueMask_secondTransitionCapture = 0x00000200;
							};
						namespace cpol { // Field Description
							constexpr Reg Lsb = 10;
							constexpr Reg FieldMask = 0x00000400;
							constexpr Reg Value_Default = 0x0;
							constexpr Reg ValueMask_Default = 0x00000000;
							constexpr Reg Value_steadyLowOutsideTxWindow = 0x0;
							constexpr Reg ValueMask_steadyLowOutsideTxWindow = 0x00000000;
							constexpr Reg Value_steadyHighOutsideTxWindow = 0x1;
							constexpr Reg ValueMask_steadyHighOutsideTxWindow = 0x00000400;
							};
						namespace clken { // Field Description
							constexpr Reg Lsb = 11;
							constexpr Reg FieldMask = 0x00000800;
							constexpr Reg Value_Default = 0x0;
							constexpr Reg ValueMask_Default = 0x00000000;
							constexpr Reg Value_disabled = 0x0;
							constexpr Reg ValueMask_disabled = 0x00000000;
							constexpr Reg Value_enabled = 0x1;
							constexpr Reg ValueMask_enabled = 0x00000800;
							constexpr Reg Value_disable = 0x0;
							constexpr Reg ValueMask_disable = 0x00000000;
							constexpr Reg Value_enable = 0x1;
							constexpr Reg ValueMask_enable = 0x00000800;
							};
						namespace stop { // Field Description
							constexpr Reg Lsb = 12;
							constexpr Reg FieldMask = 0x00003000;
							constexpr Reg Value_Default = 0x0;
							constexpr Reg ValueMask_Default = 0x00000000;
							constexpr Reg Value_oneStopBit = 0x0;
							constexpr Reg ValueMask_oneStopBit = 0x00000000;
							constexpr Reg Value_zeroPointFiveStopBits = 0x1;
							constexpr Reg ValueMask_zeroPointFiveStopBits = 0x00001000;
							constexpr Reg Value_twoStopBits = 0x2;
							constexpr Reg ValueMask_twoStopBits = 0x00002000;
							constexpr Reg Value_onePointFiveStopBits = 0x3;
							constexpr Reg ValueMask_onePointFiveStopBits = 0x00003000;
							};
						namespace linen { // Field Description
							constexpr Reg Lsb = 14;
							constexpr Reg FieldMask = 0x00004000;
							constexpr Reg Value_Default = 0x0;
							constexpr Reg ValueMask_Default = 0x00000000;
							constexpr Reg Value_disabled = 0x0;
							constexpr Reg ValueMask_disabled = 0x00000000;
							constexpr Reg Value_enabled = 0x1;
							constexpr Reg ValueMask_enabled = 0x00004000;
							constexpr Reg Value_disable = 0x0;
							constexpr Reg ValueMask_disable = 0x00000000;
							constexpr Reg Value_enable = 0x1;
							constexpr Reg ValueMask_enable = 0x00004000;
							};
						};
					namespace Cr3 { // Register description
						typedef uint32_t	Reg;
						namespace eie { // Field Description
							constexpr Reg Lsb = 0;
							constexpr Reg FieldMask = 0x00000001;
							constexpr Reg Value_Default = 0x0;
							constexpr Reg ValueMask_Default = 0x00000000;
							constexpr Reg Value_disabled = 0x0;
							constexpr Reg ValueMask_disabled = 0x00000000;
							constexpr Reg Value_enabled = 0x1;
							constexpr Reg ValueMask_enabled = 0x00000001;
							constexpr Reg Value_disable = 0x0;
							constexpr Reg ValueMask_disable = 0x00000000;
							constexpr Reg Value_enable = 0x1;
							constexpr Reg ValueMask_enable = 0x00000001;
							};
						namespace iren { // Field Description
							constexpr Reg Lsb = 1;
							constexpr Reg FieldMask = 0x00000002;
							constexpr Reg Value_Default = 0x0;
							constexpr Reg ValueMask_Default = 0x00000000;
							constexpr Reg Value_disabled = 0x0;
							constexpr Reg ValueMask_disabled = 0x00000000;
							constexpr Reg Value_enabled = 0x1;
							constexpr Reg ValueMask_enabled = 0x00000002;
							constexpr Reg Value_disable = 0x0;
							constexpr Reg ValueMask_disable = 0x00000000;
							constexpr Reg Value_enable = 0x1;
							constexpr Reg ValueMask_enable = 0x00000002;
							};
						namespace irlp { // Field Description
							constexpr Reg Lsb = 2;
							constexpr Reg FieldMask = 0x00000004;
							constexpr Reg Value_Default = 0x0;
							constexpr Reg ValueMask_Default = 0x00000000;
							constexpr Reg Value_normalMode = 0x0;
							constexpr Reg ValueMask_normalMode = 0x00000000;
							constexpr Reg Value_lowPowerMode = 0x1;
							constexpr Reg ValueMask_lowPowerMode = 0x00000004;
							};
						namespace hdsel { // Field Description
							constexpr Reg Lsb = 3;
							constexpr Reg FieldMask = 0x00000008;
							constexpr Reg Value_Default = 0x0;
							constexpr Reg ValueMask_Default = 0x00000000;
							constexpr Reg Value_disabled = 0x0;
							constexpr Reg ValueMask_disabled = 0x00000000;
							constexpr Reg Value_enabled = 0x1;
							constexpr Reg ValueMask_enabled = 0x00000008;
							constexpr Reg Value_disable = 0x0;
							constexpr Reg ValueMask_disable = 0x00000000;
							constexpr Reg Value_enable = 0x1;
							constexpr Reg ValueMask_enable = 0x00000008;
							};
						namespace nack { // Field Description
							constexpr Reg Lsb = 4;
							constexpr Reg FieldMask = 0x00000010;
							constexpr Reg Value_Default = 0x0;
							constexpr Reg ValueMask_Default = 0x00000000;
							constexpr Reg Value_disabled = 0x0;
							constexpr Reg ValueMask_disabled = 0x00000000;
							constexpr Reg Value_enabled = 0x1;
							constexpr Reg ValueMask_enabled = 0x00000010;
							constexpr Reg Value_disable = 0x0;
							constexpr Reg ValueMask_disable = 0x00000000;
							constexpr Reg Value_enable = 0x1;
							constexpr Reg ValueMask_enable = 0x00000010;
							};
						namespace scen { // Field Description
							constexpr Reg Lsb = 5;
							constexpr Reg FieldMask = 0x00000020;
							constexpr Reg Value_Default = 0x0;
							constexpr Reg ValueMask_Default = 0x00000000;
							constexpr Reg Value_disabled = 0x0;
							constexpr Reg ValueMask_disabled = 0x00000000;
							constexpr Reg Value_enabled = 0x1;
							constexpr Reg ValueMask_enabled = 0x00000020;
							constexpr Reg Value_disable = 0x0;
							constexpr Reg ValueMask_disable = 0x00000000;
							constexpr Reg Value_enable = 0x1;
							constexpr Reg ValueMask_enable = 0x00000020;
							};
						namespace dmar { // Field Description
							constexpr Reg Lsb = 6;
							constexpr Reg FieldMask = 0x00000040;
							constexpr Reg Value_Default = 0x0;
							constexpr Reg ValueMask_Default = 0x00000000;
							constexpr Reg Value_disabled = 0x0;
							constexpr Reg ValueMask_disabled = 0x00000000;
							constexpr Reg Value_enabled = 0x1;
							constexpr Reg ValueMask_enabled = 0x00000040;
							constexpr Reg Value_disable = 0x0;
							constexpr Reg ValueMask_disable = 0x00000000;
							constexpr Reg Value_enable = 0x1;
							constexpr Reg ValueMask_enable = 0x00000040;
							};
						namespace dmat { // Field Description
							constexpr Reg Lsb = 7;
							constexpr Reg FieldMask = 0x00000080;
							constexpr Reg Value_Default = 0x0;
							constexpr Reg ValueMask_Default = 0x00000000;
							constexpr Reg Value_disabled = 0x0;
							constexpr Reg ValueMask_disabled = 0x00000000;
							constexpr Reg Value_enabled = 0x1;
							constexpr Reg ValueMask_enabled = 0x00000080;
							constexpr Reg Value_disable = 0x0;
							constexpr Reg ValueMask_disable = 0x00000000;
							constexpr Reg Value_enable = 0x1;
							constexpr Reg ValueMask_enable = 0x00000080;
							};
						namespace rtse { // Field Description
							constexpr Reg Lsb = 8;
							constexpr Reg FieldMask = 0x00000100;
							constexpr Reg Value_Default = 0x0;
							constexpr Reg ValueMask_Default = 0x00000000;
							constexpr Reg Value_disabled = 0x0;
							constexpr Reg ValueMask_disabled = 0x00000000;
							constexpr Reg Value_enabled = 0x1;
							constexpr Reg ValueMask_enabled = 0x00000100;
							constexpr Reg Value_disable = 0x0;
							constexpr Reg ValueMask_disable = 0x00000000;
							constexpr Reg Value_enable = 0x1;
							constexpr Reg ValueMask_enable = 0x00000100;
							};
						namespace ctse { // Field Description
							constexpr Reg Lsb = 9;
							constexpr Reg FieldMask = 0x00000200;
							constexpr Reg Value_Default = 0x0;
							constexpr Reg ValueMask_Default = 0x00000000;
							constexpr Reg Value_disabled = 0x0;
							constexpr Reg ValueMask_disabled = 0x00000000;
							constexpr Reg Value_enabled = 0x1;
							constexpr Reg ValueMask_enabled = 0x00000200;
							constexpr Reg Value_disable = 0x0;
							constexpr Reg ValueMask_disable = 0x00000000;
							constexpr Reg Value_enable = 0x1;
							constexpr Reg ValueMask_enable = 0x00000200;
							};
						namespace ctsie { // Field Description
							constexpr Reg Lsb = 10;
							constexpr Reg FieldMask = 0x00000400;
							constexpr Reg Value_Default = 0x0;
							constexpr Reg ValueMask_Default = 0x00000000;
							constexpr Reg Value_disabled = 0x0;
							constexpr Reg ValueMask_disabled = 0x00000000;
							constexpr Reg Value_enabled = 0x1;
							constexpr Reg ValueMask_enabled = 0x00000400;
							constexpr Reg Value_disable = 0x0;
							constexpr Reg ValueMask_disable = 0x00000000;
							constexpr Reg Value_enable = 0x1;
							constexpr Reg ValueMask_enable = 0x00000400;
							};
						namespace onebit { // Field Description
							constexpr Reg Lsb = 11;
							constexpr Reg FieldMask = 0x00000800;
							constexpr Reg Value_Default = 0x0;
							constexpr Reg ValueMask_Default = 0x00000000;
							constexpr Reg Value_threeSampleBits = 0x0;
							constexpr Reg ValueMask_threeSampleBits = 0x00000000;
							constexpr Reg Value_oneSampleBit = 0x1;
							constexpr Reg ValueMask_oneSampleBit = 0x00000800;
							};
						};
					namespace Gtpr { // Register description
						typedef uint32_t	Reg;
						namespace psc { // Field Description
							constexpr Reg Lsb = 0;
							constexpr Reg FieldMask = 0x000000FF;
							constexpr Reg Value_Default = 0x0;
							constexpr Reg ValueMask_Default = 0x00000000;
							constexpr Reg Value_max = 0xFF;
							constexpr Reg ValueMask_max = 0x000000FF;
							};
						namespace gt { // Field Description
							constexpr Reg Lsb = 8;
							constexpr Reg FieldMask = 0x0000FF00;
							constexpr Reg Value_Default = 0x0;
							constexpr Reg ValueMask_Default = 0x00000000;
							constexpr Reg Value_max = 0xFF;
							constexpr Reg ValueMask_max = 0x0000FF00;
							};
						};
					}
				}
			}
		}
	}
#endif
