/*
   Copyright (C) 2024 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_hw_st_stm32_f2f4_usart_maph_
#define _oscl_hw_st_stm32_f2f4_usart_maph_

#include "reg.h"

/** */
namespace Oscl {

/** */
namespace St {

/** */
namespace Stm32 {

/** */
namespace F2F4 {

/** */
namespace Usart {

/** */
struct Map {
	/** Offset 0x000 */
	volatile Oscl::St::Stm32::F2F4::Usart::Sr::Reg	sr;

	/** Offset 0x004 */
	volatile Oscl::St::Stm32::F2F4::Usart::Dr::Reg	dr;

	/** Offset 0x008 */
	volatile Oscl::St::Stm32::F2F4::Usart::Brr::Reg	brr;

	/** Offset 0x00C */
	volatile Oscl::St::Stm32::F2F4::Usart::Cr1::Reg	cr1;

	/** Offset 0x010 */
	volatile Oscl::St::Stm32::F2F4::Usart::Cr2::Reg	cr2;

	/** Offset 0x014 */
	volatile Oscl::St::Stm32::F2F4::Usart::Cr3::Reg	cr3;

	/** Offset 0x018 */
	volatile Oscl::St::Stm32::F2F4::Usart::Gtpr::Reg	gtpr;
	};

}
}
}
}
}

#endif
