/*
   Copyright (C) 2024 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_hw_st_stm32_h7a3b3b0_rcc_maph_
#define _oscl_hw_st_stm32_h7a3b3b0_rcc_maph_
#include "reg.h"
namespace Oscl {
namespace St {
namespace Stm32 {
namespace H7A3B3B0 {
namespace pRCC {
struct Map {
	/** offset:0x0 */
	volatile Oscl::St::Stm32::H7A3B3B0::pRCC::rCR::Reg	cr;
	/** offset:0x4 */
	volatile Oscl::St::Stm32::H7A3B3B0::pRCC::rHSICFGR::Reg	hsicfgr;
	/** offset:0x8 */
	volatile Oscl::St::Stm32::H7A3B3B0::pRCC::rCRRCR::Reg	crrcr;
	/** offset:0xc */
	volatile Oscl::St::Stm32::H7A3B3B0::pRCC::rCSICFGR::Reg	csicfgr;
	/** offset:0x10 */
	volatile Oscl::St::Stm32::H7A3B3B0::pRCC::rCFGR::Reg	cfgr;
	const uint8_t	_reserve20[4];
	/** offset:0x18 */
	volatile Oscl::St::Stm32::H7A3B3B0::pRCC::rCDCFGR1::Reg	cdcfgr1;
	/** offset:0x1c */
	volatile Oscl::St::Stm32::H7A3B3B0::pRCC::rCDCFGR2::Reg	cdcfgr2;
	/** offset:0x20 */
	volatile Oscl::St::Stm32::H7A3B3B0::pRCC::rSRDCFGR::Reg	srdcfgr;
	const uint8_t	_reserve36[4];
	/** offset:0x28 */
	volatile Oscl::St::Stm32::H7A3B3B0::pRCC::rPLLCKSELR::Reg	pllckselr;
	/** offset:0x2c */
	volatile Oscl::St::Stm32::H7A3B3B0::pRCC::rPLLCFGR::Reg	pllcfgr;
	/** offset:0x30 */
	volatile Oscl::St::Stm32::H7A3B3B0::pRCC::rPLL1DIVR::Reg	pll1divr;
	/** offset:0x34 */
	volatile Oscl::St::Stm32::H7A3B3B0::pRCC::rPLL1FRACR::Reg	pll1fracr;
	/** offset:0x38 */
	volatile Oscl::St::Stm32::H7A3B3B0::pRCC::rPLL2DIVR::Reg	pll2divr;
	/** offset:0x3c */
	volatile Oscl::St::Stm32::H7A3B3B0::pRCC::rPLL2FRACR::Reg	pll2fracr;
	/** offset:0x40 */
	volatile Oscl::St::Stm32::H7A3B3B0::pRCC::rPLL3DIVR::Reg	pll3divr;
	/** offset:0x44 */
	volatile Oscl::St::Stm32::H7A3B3B0::pRCC::rPLL3FRACR::Reg	pll3fracr;
	const uint8_t	_reserve72[4];
	/** offset:0x4c */
	volatile Oscl::St::Stm32::H7A3B3B0::pRCC::rCDCCIPR::Reg	cdccipr;
	/** offset:0x50 */
	volatile Oscl::St::Stm32::H7A3B3B0::pRCC::rCDCCIP1R::Reg	cdccip1r;
	/** offset:0x54 */
	volatile Oscl::St::Stm32::H7A3B3B0::pRCC::rCDCCIP2R::Reg	cdccip2r;
	/** offset:0x58 */
	volatile Oscl::St::Stm32::H7A3B3B0::pRCC::rSRDCCIPR::Reg	srdccipr;
	const uint8_t	_reserve92[4];
	/** offset:0x60 */
	volatile Oscl::St::Stm32::H7A3B3B0::pRCC::rCIER::Reg	cier;
	/** offset:0x64 */
	volatile Oscl::St::Stm32::H7A3B3B0::pRCC::rCIFR::Reg	cifr;
	/** offset:0x68 */
	volatile Oscl::St::Stm32::H7A3B3B0::pRCC::rCICR::Reg	cicr;
	const uint8_t	_reserve108[4];
	/** offset:0x70 */
	volatile Oscl::St::Stm32::H7A3B3B0::pRCC::rBDCR::Reg	bdcr;
	/** offset:0x74 */
	volatile Oscl::St::Stm32::H7A3B3B0::pRCC::rCSR::Reg	csr;
	const uint8_t	_reserve120[4];
	/** offset:0x7c */
	volatile Oscl::St::Stm32::H7A3B3B0::pRCC::rAHB3RSTR::Reg	ahb3rstr;
	/** offset:0x80 */
	volatile Oscl::St::Stm32::H7A3B3B0::pRCC::rAHB1RSTR::Reg	ahb1rstr;
	/** offset:0x84 */
	volatile Oscl::St::Stm32::H7A3B3B0::pRCC::rAHB2RSTR::Reg	ahb2rstr;
	/** offset:0x88 */
	volatile Oscl::St::Stm32::H7A3B3B0::pRCC::rAHB4RSTR::Reg	ahb4rstr;
	/** offset:0x8c */
	volatile Oscl::St::Stm32::H7A3B3B0::pRCC::rAPB3RSTR::Reg	apb3rstr;
	/** offset:0x90 */
	volatile Oscl::St::Stm32::H7A3B3B0::pRCC::rAPB1LRSTR::Reg	apb1lrstr;
	/** offset:0x94 */
	volatile Oscl::St::Stm32::H7A3B3B0::pRCC::rAPB1HRSTR::Reg	apb1hrstr;
	/** offset:0x98 */
	volatile Oscl::St::Stm32::H7A3B3B0::pRCC::rAPB2RSTR::Reg	apb2rstr;
	/** offset:0x9c */
	volatile Oscl::St::Stm32::H7A3B3B0::pRCC::rAPB4RSTR::Reg	apb4rstr;
	const uint8_t	_reserve160[8];
	/** offset:0xa8 */
	volatile Oscl::St::Stm32::H7A3B3B0::pRCC::rSRDAMR::Reg	srdamr;
	const uint8_t	_reserve172[4];
	/** offset:0xb0 */
	volatile Oscl::St::Stm32::H7A3B3B0::pRCC::rCKGAENR::Reg	ckgaenr;
	const uint8_t	_reserve180[124];
	/** offset:0x130 */
	volatile Oscl::St::Stm32::H7A3B3B0::pRCC::rRSR::Reg	rsr;
	/** offset:0x134 */
	volatile Oscl::St::Stm32::H7A3B3B0::pRCC::rAHB3ENR::Reg	ahb3enr;
	/** offset:0x138 */
	volatile Oscl::St::Stm32::H7A3B3B0::pRCC::rAHB1ENR::Reg	ahb1enr;
	/** offset:0x13c */
	volatile Oscl::St::Stm32::H7A3B3B0::pRCC::rAHB2ENR::Reg	ahb2enr;
	/** offset:0x140 */
	volatile Oscl::St::Stm32::H7A3B3B0::pRCC::rAHB4ENR::Reg	ahb4enr;
	/** offset:0x144 */
	volatile Oscl::St::Stm32::H7A3B3B0::pRCC::rAPB3ENR::Reg	apb3enr;
	/** offset:0x148 */
	volatile Oscl::St::Stm32::H7A3B3B0::pRCC::rAPB1LENR::Reg	apb1lenr;
	/** offset:0x14c */
	volatile Oscl::St::Stm32::H7A3B3B0::pRCC::rAPB1HENR::Reg	apb1henr;
	/** offset:0x150 */
	volatile Oscl::St::Stm32::H7A3B3B0::pRCC::rAPB2ENR::Reg	apb2enr;
	/** offset:0x154 */
	volatile Oscl::St::Stm32::H7A3B3B0::pRCC::rAPB4ENR::Reg	apb4enr;
	const uint8_t	_reserve344[4];
	/** offset:0x15c */
	volatile Oscl::St::Stm32::H7A3B3B0::pRCC::rAHB3LPENR::Reg	ahb3lpenr;
	/** offset:0x160 */
	volatile Oscl::St::Stm32::H7A3B3B0::pRCC::rAHB1LPENR::Reg	ahb1lpenr;
	/** offset:0x164 */
	volatile Oscl::St::Stm32::H7A3B3B0::pRCC::rAHB2LPENR::Reg	ahb2lpenr;
	/** offset:0x168 */
	volatile Oscl::St::Stm32::H7A3B3B0::pRCC::rAHB4LPENR::Reg	ahb4lpenr;
	/** offset:0x16c */
	volatile Oscl::St::Stm32::H7A3B3B0::pRCC::rAPB3LPENR::Reg	apb3lpenr;
	/** offset:0x170 */
	volatile Oscl::St::Stm32::H7A3B3B0::pRCC::rAPB1LLPENR::Reg	apb1llpenr;
	/** offset:0x174 */
	volatile Oscl::St::Stm32::H7A3B3B0::pRCC::rAPB1HLPENR::Reg	apb1hlpenr;
	/** offset:0x178 */
	volatile Oscl::St::Stm32::H7A3B3B0::pRCC::rAPB2LPENR::Reg	apb2lpenr;
	/** offset:0x17c */
	volatile Oscl::St::Stm32::H7A3B3B0::pRCC::rAPB4LPENR::Reg	apb4lpenr;
	};
}
}
}
}
}
#endif
