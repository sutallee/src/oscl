/*
   Copyright (C) 2024 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_hw_st_stm32_h7a3b3b0_syscfg_regh_
#define _oscl_hw_st_stm32_h7a3b3b0_syscfg_regh_
#include <stdint.h>
namespace Oscl { // Namespace description
	namespace St { // Namespace description
		namespace Stm32 { // Namespace description
			namespace H7A3B3B0 { // Namespace description
				namespace pSYSCFG { // Namespace description
					namespace rPMCR { // Register description
						typedef uint32_t	Reg;
						constexpr Reg	valueAtReset = 0x0F000000;
						namespace fI2C1FMP { // Field Description
							constexpr Reg Lsb = 0;
							constexpr Reg FieldMask = 0x00000001;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_disable = 0x0;
							constexpr Reg ValueMask_disable = 0x00000000;
							constexpr Reg Value_disabled = 0x0;
							constexpr Reg ValueMask_disabled = 0x00000000;
							constexpr Reg Value_enable = 0x1;
							constexpr Reg ValueMask_enable = 0x00000001;
							constexpr Reg Value_enabled = 0x1;
							constexpr Reg ValueMask_enabled = 0x00000001;
							};
						namespace fI2C2FMP { // Field Description
							constexpr Reg Lsb = 1;
							constexpr Reg FieldMask = 0x00000002;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_disable = 0x0;
							constexpr Reg ValueMask_disable = 0x00000000;
							constexpr Reg Value_disabled = 0x0;
							constexpr Reg ValueMask_disabled = 0x00000000;
							constexpr Reg Value_enable = 0x1;
							constexpr Reg ValueMask_enable = 0x00000002;
							constexpr Reg Value_enabled = 0x1;
							constexpr Reg ValueMask_enabled = 0x00000002;
							};
						namespace fI2C3FMP { // Field Description
							constexpr Reg Lsb = 2;
							constexpr Reg FieldMask = 0x00000004;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_disable = 0x0;
							constexpr Reg ValueMask_disable = 0x00000000;
							constexpr Reg Value_disabled = 0x0;
							constexpr Reg ValueMask_disabled = 0x00000000;
							constexpr Reg Value_enable = 0x1;
							constexpr Reg ValueMask_enable = 0x00000004;
							constexpr Reg Value_enabled = 0x1;
							constexpr Reg ValueMask_enabled = 0x00000004;
							};
						namespace fI2C4FMP { // Field Description
							constexpr Reg Lsb = 3;
							constexpr Reg FieldMask = 0x00000008;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_disable = 0x0;
							constexpr Reg ValueMask_disable = 0x00000000;
							constexpr Reg Value_disabled = 0x0;
							constexpr Reg ValueMask_disabled = 0x00000000;
							constexpr Reg Value_enable = 0x1;
							constexpr Reg ValueMask_enable = 0x00000008;
							constexpr Reg Value_enabled = 0x1;
							constexpr Reg ValueMask_enabled = 0x00000008;
							};
						namespace fPB6FMP { // Field Description
							constexpr Reg Lsb = 4;
							constexpr Reg FieldMask = 0x00000010;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_disable = 0x0;
							constexpr Reg ValueMask_disable = 0x00000000;
							constexpr Reg Value_disabled = 0x0;
							constexpr Reg ValueMask_disabled = 0x00000000;
							constexpr Reg Value_enable = 0x1;
							constexpr Reg ValueMask_enable = 0x00000010;
							constexpr Reg Value_enabled = 0x1;
							constexpr Reg ValueMask_enabled = 0x00000010;
							};
						namespace fPB7FMP { // Field Description
							constexpr Reg Lsb = 5;
							constexpr Reg FieldMask = 0x00000020;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_standard = 0x0;
							constexpr Reg ValueMask_standard = 0x00000000;
							constexpr Reg Value_fmPlusMode = 0x1;
							constexpr Reg ValueMask_fmPlusMode = 0x00000020;
							};
						namespace fPB8FMP { // Field Description
							constexpr Reg Lsb = 6;
							constexpr Reg FieldMask = 0x00000040;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_standard = 0x0;
							constexpr Reg ValueMask_standard = 0x00000000;
							constexpr Reg Value_fmPlusMode = 0x1;
							constexpr Reg ValueMask_fmPlusMode = 0x00000040;
							};
						namespace fPB9FMP { // Field Description
							constexpr Reg Lsb = 7;
							constexpr Reg FieldMask = 0x00000080;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_standard = 0x0;
							constexpr Reg ValueMask_standard = 0x00000000;
							constexpr Reg Value_fmPlusMode = 0x1;
							constexpr Reg ValueMask_fmPlusMode = 0x00000080;
							};
						namespace fBOOSTE { // Field Description
							constexpr Reg Lsb = 8;
							constexpr Reg FieldMask = 0x00000100;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							};
						namespace fEPIS { // Field Description
							constexpr Reg Lsb = 21;
							constexpr Reg FieldMask = 0x00E00000;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							};
						namespace fPA0SO { // Field Description
							constexpr Reg Lsb = 24;
							constexpr Reg FieldMask = 0x01000000;
							constexpr Reg Value_valueAtReset = 0x1;
							constexpr Reg ValueMask_valueAtReset = 0x01000000;
							constexpr Reg Value_closed = 0x0;
							constexpr Reg ValueMask_closed = 0x00000000;
							constexpr Reg Value_close = 0x0;
							constexpr Reg ValueMask_close = 0x00000000;
							constexpr Reg Value_opened = 0x1;
							constexpr Reg ValueMask_opened = 0x01000000;
							constexpr Reg Value_open = 0x1;
							constexpr Reg ValueMask_open = 0x01000000;
							};
						namespace fPA1SO { // Field Description
							constexpr Reg Lsb = 25;
							constexpr Reg FieldMask = 0x02000000;
							constexpr Reg Value_valueAtReset = 0x1;
							constexpr Reg ValueMask_valueAtReset = 0x02000000;
							constexpr Reg Value_closed = 0x0;
							constexpr Reg ValueMask_closed = 0x00000000;
							constexpr Reg Value_close = 0x0;
							constexpr Reg ValueMask_close = 0x00000000;
							constexpr Reg Value_opened = 0x1;
							constexpr Reg ValueMask_opened = 0x02000000;
							constexpr Reg Value_open = 0x1;
							constexpr Reg ValueMask_open = 0x02000000;
							};
						namespace fPC2SO { // Field Description
							constexpr Reg Lsb = 26;
							constexpr Reg FieldMask = 0x04000000;
							constexpr Reg Value_valueAtReset = 0x1;
							constexpr Reg ValueMask_valueAtReset = 0x04000000;
							constexpr Reg Value_closed = 0x0;
							constexpr Reg ValueMask_closed = 0x00000000;
							constexpr Reg Value_close = 0x0;
							constexpr Reg ValueMask_close = 0x00000000;
							constexpr Reg Value_opened = 0x1;
							constexpr Reg ValueMask_opened = 0x04000000;
							constexpr Reg Value_open = 0x1;
							constexpr Reg ValueMask_open = 0x04000000;
							};
						namespace fPC3SO { // Field Description
							constexpr Reg Lsb = 27;
							constexpr Reg FieldMask = 0x08000000;
							constexpr Reg Value_valueAtReset = 0x1;
							constexpr Reg ValueMask_valueAtReset = 0x08000000;
							constexpr Reg Value_closed = 0x0;
							constexpr Reg ValueMask_closed = 0x00000000;
							constexpr Reg Value_close = 0x0;
							constexpr Reg ValueMask_close = 0x00000000;
							constexpr Reg Value_opened = 0x1;
							constexpr Reg ValueMask_opened = 0x08000000;
							constexpr Reg Value_open = 0x1;
							constexpr Reg ValueMask_open = 0x08000000;
							};
						};
					namespace rEXTICR { // Register description
						typedef uint32_t	Reg;
						constexpr Reg	valueAtReset = 0x00000000;
						constexpr Reg	vPA = 0x00000000;
						constexpr Reg	vPB = 0x00000001;
						constexpr Reg	vPC = 0x00000002;
						constexpr Reg	vPD = 0x00000003;
						constexpr Reg	vPE = 0x00000004;
						constexpr Reg	vPF = 0x00000005;
						constexpr Reg	vPG = 0x00000006;
						constexpr Reg	vPH = 0x00000007;
						constexpr Reg	vPI = 0x00000008;
						constexpr Reg	vPJ = 0x00000009;
						constexpr Reg	vPK = 0x0000000A;
						namespace fEXTI0 { // Field Description
							constexpr Reg Lsb = 0;
							constexpr Reg FieldMask = 0x0000000F;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_vPA = 0x0;
							constexpr Reg ValueMask_vPA = 0x00000000;
							constexpr Reg Value_vPB = 0x1;
							constexpr Reg ValueMask_vPB = 0x00000001;
							constexpr Reg Value_vPC = 0x2;
							constexpr Reg ValueMask_vPC = 0x00000002;
							constexpr Reg Value_vPD = 0x3;
							constexpr Reg ValueMask_vPD = 0x00000003;
							constexpr Reg Value_vPE = 0x4;
							constexpr Reg ValueMask_vPE = 0x00000004;
							constexpr Reg Value_vPF = 0x5;
							constexpr Reg ValueMask_vPF = 0x00000005;
							constexpr Reg Value_vPG = 0x6;
							constexpr Reg ValueMask_vPG = 0x00000006;
							constexpr Reg Value_vPH = 0x7;
							constexpr Reg ValueMask_vPH = 0x00000007;
							constexpr Reg Value_vPI = 0x8;
							constexpr Reg ValueMask_vPI = 0x00000008;
							constexpr Reg Value_vPJ = 0x9;
							constexpr Reg ValueMask_vPJ = 0x00000009;
							constexpr Reg Value_vPK = 0xA;
							constexpr Reg ValueMask_vPK = 0x0000000A;
							};
						namespace fEXTI1 { // Field Description
							constexpr Reg Lsb = 4;
							constexpr Reg FieldMask = 0x000000F0;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_vPA = 0x0;
							constexpr Reg ValueMask_vPA = 0x00000000;
							constexpr Reg Value_vPB = 0x1;
							constexpr Reg ValueMask_vPB = 0x00000010;
							constexpr Reg Value_vPC = 0x2;
							constexpr Reg ValueMask_vPC = 0x00000020;
							constexpr Reg Value_vPD = 0x3;
							constexpr Reg ValueMask_vPD = 0x00000030;
							constexpr Reg Value_vPE = 0x4;
							constexpr Reg ValueMask_vPE = 0x00000040;
							constexpr Reg Value_vPF = 0x5;
							constexpr Reg ValueMask_vPF = 0x00000050;
							constexpr Reg Value_vPG = 0x6;
							constexpr Reg ValueMask_vPG = 0x00000060;
							constexpr Reg Value_vPH = 0x7;
							constexpr Reg ValueMask_vPH = 0x00000070;
							constexpr Reg Value_vPI = 0x8;
							constexpr Reg ValueMask_vPI = 0x00000080;
							constexpr Reg Value_vPJ = 0x9;
							constexpr Reg ValueMask_vPJ = 0x00000090;
							constexpr Reg Value_vPK = 0xA;
							constexpr Reg ValueMask_vPK = 0x000000A0;
							};
						namespace fEXTI2 { // Field Description
							constexpr Reg Lsb = 8;
							constexpr Reg FieldMask = 0x00000F00;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_vPA = 0x0;
							constexpr Reg ValueMask_vPA = 0x00000000;
							constexpr Reg Value_vPB = 0x1;
							constexpr Reg ValueMask_vPB = 0x00000100;
							constexpr Reg Value_vPC = 0x2;
							constexpr Reg ValueMask_vPC = 0x00000200;
							constexpr Reg Value_vPD = 0x3;
							constexpr Reg ValueMask_vPD = 0x00000300;
							constexpr Reg Value_vPE = 0x4;
							constexpr Reg ValueMask_vPE = 0x00000400;
							constexpr Reg Value_vPF = 0x5;
							constexpr Reg ValueMask_vPF = 0x00000500;
							constexpr Reg Value_vPG = 0x6;
							constexpr Reg ValueMask_vPG = 0x00000600;
							constexpr Reg Value_vPH = 0x7;
							constexpr Reg ValueMask_vPH = 0x00000700;
							constexpr Reg Value_vPI = 0x8;
							constexpr Reg ValueMask_vPI = 0x00000800;
							constexpr Reg Value_vPJ = 0x9;
							constexpr Reg ValueMask_vPJ = 0x00000900;
							constexpr Reg Value_vPK = 0xA;
							constexpr Reg ValueMask_vPK = 0x00000A00;
							};
						namespace fEXTI3 { // Field Description
							constexpr Reg Lsb = 12;
							constexpr Reg FieldMask = 0x0000F000;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_vPA = 0x0;
							constexpr Reg ValueMask_vPA = 0x00000000;
							constexpr Reg Value_vPB = 0x1;
							constexpr Reg ValueMask_vPB = 0x00001000;
							constexpr Reg Value_vPC = 0x2;
							constexpr Reg ValueMask_vPC = 0x00002000;
							constexpr Reg Value_vPD = 0x3;
							constexpr Reg ValueMask_vPD = 0x00003000;
							constexpr Reg Value_vPE = 0x4;
							constexpr Reg ValueMask_vPE = 0x00004000;
							constexpr Reg Value_vPF = 0x5;
							constexpr Reg ValueMask_vPF = 0x00005000;
							constexpr Reg Value_vPG = 0x6;
							constexpr Reg ValueMask_vPG = 0x00006000;
							constexpr Reg Value_vPH = 0x7;
							constexpr Reg ValueMask_vPH = 0x00007000;
							constexpr Reg Value_vPI = 0x8;
							constexpr Reg ValueMask_vPI = 0x00008000;
							constexpr Reg Value_vPJ = 0x9;
							constexpr Reg ValueMask_vPJ = 0x00009000;
							constexpr Reg Value_vPK = 0xA;
							constexpr Reg ValueMask_vPK = 0x0000A000;
							};
						};
					namespace rCFGR { // Register description
						typedef uint16_t	Reg;
						constexpr Reg	valueAtReset = 0x00000000;
						namespace fPVDL { // Field Description
							constexpr Reg Lsb = 2;
							constexpr Reg FieldMask = 0x00000004;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_disconnected = 0x0;
							constexpr Reg ValueMask_disconnected = 0x00000000;
							constexpr Reg Value_connected = 0x1;
							constexpr Reg ValueMask_connected = 0x00000004;
							constexpr Reg Value_noEffect = 0x0;
							constexpr Reg ValueMask_noEffect = 0x00000000;
							constexpr Reg Value_set = 0x1;
							constexpr Reg ValueMask_set = 0x00000004;
							};
						namespace fFLASHL { // Field Description
							constexpr Reg Lsb = 3;
							constexpr Reg FieldMask = 0x00000008;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_disconnected = 0x0;
							constexpr Reg ValueMask_disconnected = 0x00000000;
							constexpr Reg Value_connected = 0x1;
							constexpr Reg ValueMask_connected = 0x00000008;
							constexpr Reg Value_noEffect = 0x0;
							constexpr Reg ValueMask_noEffect = 0x00000000;
							constexpr Reg Value_set = 0x1;
							constexpr Reg ValueMask_set = 0x00000008;
							};
						namespace fCM7L { // Field Description
							constexpr Reg Lsb = 6;
							constexpr Reg FieldMask = 0x00000040;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_disconnected = 0x0;
							constexpr Reg ValueMask_disconnected = 0x00000000;
							constexpr Reg Value_connected = 0x1;
							constexpr Reg ValueMask_connected = 0x00000040;
							constexpr Reg Value_noEffect = 0x0;
							constexpr Reg ValueMask_noEffect = 0x00000000;
							constexpr Reg Value_set = 0x1;
							constexpr Reg ValueMask_set = 0x00000040;
							};
						namespace fDTCML { // Field Description
							constexpr Reg Lsb = 13;
							constexpr Reg FieldMask = 0x00002000;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_disconnected = 0x0;
							constexpr Reg ValueMask_disconnected = 0x00000000;
							constexpr Reg Value_connected = 0x1;
							constexpr Reg ValueMask_connected = 0x00002000;
							constexpr Reg Value_noEffect = 0x0;
							constexpr Reg ValueMask_noEffect = 0x00000000;
							constexpr Reg Value_set = 0x1;
							constexpr Reg ValueMask_set = 0x00002000;
							};
						namespace fITCML { // Field Description
							constexpr Reg Lsb = 14;
							constexpr Reg FieldMask = 0x00004000;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_disconnected = 0x0;
							constexpr Reg ValueMask_disconnected = 0x00000000;
							constexpr Reg Value_connected = 0x1;
							constexpr Reg ValueMask_connected = 0x00004000;
							constexpr Reg Value_noEffect = 0x0;
							constexpr Reg ValueMask_noEffect = 0x00000000;
							constexpr Reg Value_set = 0x1;
							constexpr Reg ValueMask_set = 0x00004000;
							};
						};
					namespace rCCCSR { // Register description
						typedef uint32_t	Reg;
						constexpr Reg	valueAtReset = 0x00000000;
						namespace fEN { // Field Description
							constexpr Reg Lsb = 0;
							constexpr Reg FieldMask = 0x00000001;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_disable = 0x0;
							constexpr Reg ValueMask_disable = 0x00000000;
							constexpr Reg Value_disabled = 0x0;
							constexpr Reg ValueMask_disabled = 0x00000000;
							constexpr Reg Value_enable = 0x1;
							constexpr Reg ValueMask_enable = 0x00000001;
							constexpr Reg Value_enabled = 0x1;
							constexpr Reg ValueMask_enabled = 0x00000001;
							};
						namespace fCS { // Field Description
							constexpr Reg Lsb = 1;
							constexpr Reg FieldMask = 0x00000002;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_ccvr = 0x0;
							constexpr Reg ValueMask_ccvr = 0x00000000;
							constexpr Reg Value_cccr = 0x1;
							constexpr Reg ValueMask_cccr = 0x00000002;
							};
						namespace fCS_MMC { // Field Description
							constexpr Reg Lsb = 3;
							constexpr Reg FieldMask = 0x00000008;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_ccvr = 0x0;
							constexpr Reg ValueMask_ccvr = 0x00000000;
							constexpr Reg Value_cccr = 0x1;
							constexpr Reg ValueMask_cccr = 0x00000008;
							};
						namespace fREADY { // Field Description
							constexpr Reg Lsb = 8;
							constexpr Reg FieldMask = 0x00000100;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_notReady = 0x0;
							constexpr Reg ValueMask_notReady = 0x00000000;
							constexpr Reg Value_ready = 0x1;
							constexpr Reg ValueMask_ready = 0x00000100;
							};
						namespace fHSLV0 { // Field Description
							constexpr Reg Lsb = 16;
							constexpr Reg FieldMask = 0x00010000;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_noOptimize = 0x0;
							constexpr Reg ValueMask_noOptimize = 0x00000000;
							constexpr Reg Value_optimize = 0x1;
							constexpr Reg ValueMask_optimize = 0x00010000;
							};
						namespace fHSLV1 { // Field Description
							constexpr Reg Lsb = 17;
							constexpr Reg FieldMask = 0x00020000;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_noOptimize = 0x0;
							constexpr Reg ValueMask_noOptimize = 0x00000000;
							constexpr Reg Value_optimize = 0x1;
							constexpr Reg ValueMask_optimize = 0x00020000;
							};
						namespace fHSLV2 { // Field Description
							constexpr Reg Lsb = 18;
							constexpr Reg FieldMask = 0x00040000;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_noOptimize = 0x0;
							constexpr Reg ValueMask_noOptimize = 0x00000000;
							constexpr Reg Value_optimize = 0x1;
							constexpr Reg ValueMask_optimize = 0x00040000;
							};
						namespace fHSLV3 { // Field Description
							constexpr Reg Lsb = 19;
							constexpr Reg FieldMask = 0x00080000;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_noOptimize = 0x0;
							constexpr Reg ValueMask_noOptimize = 0x00000000;
							constexpr Reg Value_optimize = 0x1;
							constexpr Reg ValueMask_optimize = 0x00080000;
							};
						};
					namespace rCCVR { // Register description
						typedef uint32_t	Reg;
						constexpr Reg	valueAtReset = 0x00000088;
						namespace fNCV { // Field Description
							constexpr Reg Lsb = 0;
							constexpr Reg FieldMask = 0x0000000F;
							constexpr Reg Value_valueAtReset = 0x8;
							constexpr Reg ValueMask_valueAtReset = 0x00000008;
							constexpr Reg Value_expected = 0x8;
							constexpr Reg ValueMask_expected = 0x00000008;
							};
						namespace fPCV { // Field Description
							constexpr Reg Lsb = 4;
							constexpr Reg FieldMask = 0x000000F0;
							constexpr Reg Value_valueAtReset = 0x8;
							constexpr Reg ValueMask_valueAtReset = 0x00000080;
							constexpr Reg Value_expected = 0x8;
							constexpr Reg ValueMask_expected = 0x00000080;
							};
						};
					namespace rCCCR { // Register description
						typedef uint32_t	Reg;
						constexpr Reg	valueAtReset = 0x00000000;
						namespace fNCC { // Field Description
							constexpr Reg Lsb = 0;
							constexpr Reg FieldMask = 0x0000000F;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_minValue = 0x0;
							constexpr Reg ValueMask_minValue = 0x00000000;
							constexpr Reg Value_maxValue = 0xF;
							constexpr Reg ValueMask_maxValue = 0x0000000F;
							};
						namespace fPCC { // Field Description
							constexpr Reg Lsb = 4;
							constexpr Reg FieldMask = 0x000000F0;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_minValue = 0x0;
							constexpr Reg ValueMask_minValue = 0x00000000;
							constexpr Reg Value_maxValue = 0xF;
							constexpr Reg ValueMask_maxValue = 0x000000F0;
							};
						namespace fNCC_MMC { // Field Description
							constexpr Reg Lsb = 8;
							constexpr Reg FieldMask = 0x00000F00;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_minValue = 0x0;
							constexpr Reg ValueMask_minValue = 0x00000000;
							constexpr Reg Value_maxValue = 0xF;
							constexpr Reg ValueMask_maxValue = 0x00000F00;
							};
						namespace fPCC_MMC { // Field Description
							constexpr Reg Lsb = 12;
							constexpr Reg FieldMask = 0x0000F000;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_minValue = 0x0;
							constexpr Reg ValueMask_minValue = 0x00000000;
							constexpr Reg Value_maxValue = 0xF;
							constexpr Reg ValueMask_maxValue = 0x0000F000;
							};
						};
					}
				}
			}
		}
	}
#endif
