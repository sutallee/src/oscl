/*
   Copyright (C) 2024 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_hw_st_stm32_h7a3b3b0_syscfg_maph_
#define _oscl_hw_st_stm32_h7a3b3b0_syscfg_maph_
#include "reg.h"
namespace Oscl {
namespace St {
namespace Stm32 {
namespace H7A3B3B0 {
namespace pSYSCFG {
struct Map {
	const uint8_t	_reserve0[4];
	/** offset:0x4 */
	volatile Oscl::St::Stm32::H7A3B3B0::pSYSCFG::rPMCR::Reg	pmcr;
	/** offset:0x8 */
	volatile Oscl::St::Stm32::H7A3B3B0::pSYSCFG::rEXTICR::Reg	exticr[4];
	/** offset:0x18 */
	volatile Oscl::St::Stm32::H7A3B3B0::pSYSCFG::rCFGR::Reg	cfgr;
	const uint8_t	_reserve24[4];
	/** offset:0x20 */
	volatile Oscl::St::Stm32::H7A3B3B0::pSYSCFG::rCCCSR::Reg	cccsr;
	/** offset:0x24 */
	volatile Oscl::St::Stm32::H7A3B3B0::pSYSCFG::rCCVR::Reg	ccvr;
	/** offset:0x28 */
	volatile Oscl::St::Stm32::H7A3B3B0::pSYSCFG::rCCCR::Reg	cccr;
	const uint8_t	_reserve44[236];
	};
}
}
}
}
}
#endif
