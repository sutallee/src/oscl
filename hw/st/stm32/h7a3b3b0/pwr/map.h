/*
   Copyright (C) 2024 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_hw_st_stm32_h7a3b3b0_pwr_maph_
#define _oscl_hw_st_stm32_h7a3b3b0_pwr_maph_
#include "reg.h"
namespace Oscl {
namespace St {
namespace Stm32 {
namespace H7A3B3B0 {
namespace pPWR {
struct Map {
	/** offset:0x0 */
	volatile H7A3B3B0::pPWR::rCR1::Reg	cr1;
	/** offset:0x4 */
	volatile H7A3B3B0::pPWR::rCSR1::Reg	csr1;
	/** offset:0x8 */
	volatile H7A3B3B0::pPWR::rCR2::Reg	cr2;
	/** offset:0xc */
	volatile H7A3B3B0::pPWR::rCR3::Reg	cr3;
	/** offset:0x10 */
	volatile H7A3B3B0::pPWR::rCPUCR::Reg	cpucr;
	const uint8_t	_reserve20[4];
	/** offset:0x18 */
	volatile H7A3B3B0::pPWR::rSRDCR::Reg	srdcr;
	const uint8_t	_reserve28[4];
	/** offset:0x20 */
	volatile H7A3B3B0::pPWR::rWKUPCR::Reg	wkupcr;
	/** offset:0x24 */
	volatile H7A3B3B0::pPWR::rWKUPFR::Reg	wkupfr;
	/** offset:0x28 */
	volatile H7A3B3B0::pPWR::rWKUPEPR::Reg	wkupepr;
	};
}
}
}
}
}
#endif
