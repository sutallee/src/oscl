/*
   Copyright (C) 2024 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_hw_st_stm32_h7a3b3b0_pwr_regh_
#define _oscl_hw_st_stm32_h7a3b3b0_pwr_regh_
#include <stdint.h>
namespace Oscl { // Namespace description
	namespace St { // Namespace description
		namespace Stm32 { // Namespace description
			namespace H7A3B3B0 { // Namespace description
				namespace pPWR { // Namespace description
					namespace rCR1 { // Register description
						typedef uint32_t	Reg;
						constexpr Reg	valueAtReset = 0xF000C000;
						namespace fLPDS { // Field Description
							constexpr Reg Lsb = 0;
							constexpr Reg FieldMask = 0x00000001;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_mainMode = 0x0;
							constexpr Reg ValueMask_mainMode = 0x00000000;
							constexpr Reg Value_lowPowerMode = 0x1;
							constexpr Reg ValueMask_lowPowerMode = 0x00000001;
							};
						namespace fPVDE { // Field Description
							constexpr Reg Lsb = 4;
							constexpr Reg FieldMask = 0x00000010;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_mainMode = 0x0;
							constexpr Reg ValueMask_mainMode = 0x00000000;
							constexpr Reg Value_disable = 0x0;
							constexpr Reg ValueMask_disable = 0x00000000;
							constexpr Reg Value_disabled = 0x0;
							constexpr Reg ValueMask_disabled = 0x00000000;
							constexpr Reg Value_enable = 0x1;
							constexpr Reg ValueMask_enable = 0x00000010;
							constexpr Reg Value_enabled = 0x1;
							constexpr Reg ValueMask_enabled = 0x00000010;
							};
						namespace fPLS { // Field Description
							constexpr Reg Lsb = 5;
							constexpr Reg FieldMask = 0x000000E0;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_v1_95V = 0x0;
							constexpr Reg ValueMask_v1_95V = 0x00000000;
							constexpr Reg Value_v2_1V = 0x1;
							constexpr Reg ValueMask_v2_1V = 0x00000020;
							constexpr Reg Value_v2_25V = 0x2;
							constexpr Reg ValueMask_v2_25V = 0x00000040;
							constexpr Reg Value_v2_4V = 0x3;
							constexpr Reg ValueMask_v2_4V = 0x00000060;
							constexpr Reg Value_v2_55V = 0x4;
							constexpr Reg ValueMask_v2_55V = 0x00000080;
							constexpr Reg Value_v2_7V = 0x5;
							constexpr Reg ValueMask_v2_7V = 0x000000A0;
							constexpr Reg Value_v2_85V = 0x6;
							constexpr Reg ValueMask_v2_85V = 0x000000C0;
							constexpr Reg Value_vPVD_IN = 0x7;
							constexpr Reg ValueMask_vPVD_IN = 0x000000E0;
							};
						namespace fDBP { // Field Description
							constexpr Reg Lsb = 8;
							constexpr Reg FieldMask = 0x00000100;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_disable = 0x0;
							constexpr Reg ValueMask_disable = 0x00000000;
							constexpr Reg Value_disabled = 0x0;
							constexpr Reg ValueMask_disabled = 0x00000000;
							constexpr Reg Value_enable = 0x1;
							constexpr Reg ValueMask_enable = 0x00000100;
							constexpr Reg Value_enabled = 0x1;
							constexpr Reg ValueMask_enabled = 0x00000100;
							};
						namespace fFLPS { // Field Description
							constexpr Reg Lsb = 9;
							constexpr Reg FieldMask = 0x00000200;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_normalMode = 0x0;
							constexpr Reg ValueMask_normalMode = 0x00000000;
							constexpr Reg Value_lowPowerMode = 0x1;
							constexpr Reg ValueMask_lowPowerMode = 0x00000200;
							};
						namespace fBOOSTE { // Field Description
							constexpr Reg Lsb = 12;
							constexpr Reg FieldMask = 0x00001000;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_disable = 0x0;
							constexpr Reg ValueMask_disable = 0x00000000;
							constexpr Reg Value_disabled = 0x0;
							constexpr Reg ValueMask_disabled = 0x00000000;
							constexpr Reg Value_enable = 0x1;
							constexpr Reg ValueMask_enable = 0x00001000;
							constexpr Reg Value_enabled = 0x1;
							constexpr Reg ValueMask_enabled = 0x00001000;
							};
						namespace fAVD_READY { // Field Description
							constexpr Reg Lsb = 13;
							constexpr Reg FieldMask = 0x00002000;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_notReady = 0x0;
							constexpr Reg ValueMask_notReady = 0x00000000;
							constexpr Reg Value_ready = 0x1;
							constexpr Reg ValueMask_ready = 0x00002000;
							};
						namespace fSVOS { // Field Description
							constexpr Reg Lsb = 14;
							constexpr Reg FieldMask = 0x0000C000;
							constexpr Reg Value_valueAtReset = 0x3;
							constexpr Reg ValueMask_valueAtReset = 0x0000C000;
							constexpr Reg Value_scale5 = 0x1;
							constexpr Reg ValueMask_scale5 = 0x00004000;
							constexpr Reg Value_scale4 = 0x2;
							constexpr Reg ValueMask_scale4 = 0x00008000;
							constexpr Reg Value_scale3 = 0x3;
							constexpr Reg ValueMask_scale3 = 0x0000C000;
							};
						namespace fAVDEN { // Field Description
							constexpr Reg Lsb = 16;
							constexpr Reg FieldMask = 0x00010000;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_disable = 0x0;
							constexpr Reg ValueMask_disable = 0x00000000;
							constexpr Reg Value_disabled = 0x0;
							constexpr Reg ValueMask_disabled = 0x00000000;
							constexpr Reg Value_enable = 0x1;
							constexpr Reg ValueMask_enable = 0x00010000;
							constexpr Reg Value_enabled = 0x1;
							constexpr Reg ValueMask_enabled = 0x00010000;
							};
						namespace fALS { // Field Description
							constexpr Reg Lsb = 17;
							constexpr Reg FieldMask = 0x00060000;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_v1_7V = 0x0;
							constexpr Reg ValueMask_v1_7V = 0x00000000;
							constexpr Reg Value_v2_1V = 0x1;
							constexpr Reg ValueMask_v2_1V = 0x00020000;
							constexpr Reg Value_v2_5V = 0x2;
							constexpr Reg ValueMask_v2_5V = 0x00040000;
							constexpr Reg Value_v2_8V = 0x3;
							constexpr Reg ValueMask_v2_8V = 0x00060000;
							};
						namespace fAXIRAM1SO { // Field Description
							constexpr Reg Lsb = 19;
							constexpr Reg FieldMask = 0x00080000;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_keep = 0x0;
							constexpr Reg ValueMask_keep = 0x00000000;
							constexpr Reg Value_kept = 0x0;
							constexpr Reg ValueMask_kept = 0x00000000;
							constexpr Reg Value_lose = 0x1;
							constexpr Reg ValueMask_lose = 0x00080000;
							constexpr Reg Value_lost = 0x1;
							constexpr Reg ValueMask_lost = 0x00080000;
							};
						namespace fAXIRAM2SO { // Field Description
							constexpr Reg Lsb = 20;
							constexpr Reg FieldMask = 0x00100000;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_keep = 0x0;
							constexpr Reg ValueMask_keep = 0x00000000;
							constexpr Reg Value_kept = 0x0;
							constexpr Reg ValueMask_kept = 0x00000000;
							constexpr Reg Value_lose = 0x1;
							constexpr Reg ValueMask_lose = 0x00100000;
							constexpr Reg Value_lost = 0x1;
							constexpr Reg ValueMask_lost = 0x00100000;
							};
						namespace fAXIRAM3SO { // Field Description
							constexpr Reg Lsb = 21;
							constexpr Reg FieldMask = 0x00200000;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_keep = 0x0;
							constexpr Reg ValueMask_keep = 0x00000000;
							constexpr Reg Value_kept = 0x0;
							constexpr Reg ValueMask_kept = 0x00000000;
							constexpr Reg Value_lose = 0x1;
							constexpr Reg ValueMask_lose = 0x00200000;
							constexpr Reg Value_lost = 0x1;
							constexpr Reg ValueMask_lost = 0x00200000;
							};
						namespace fAHBRAM1SO { // Field Description
							constexpr Reg Lsb = 22;
							constexpr Reg FieldMask = 0x00400000;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_keep = 0x0;
							constexpr Reg ValueMask_keep = 0x00000000;
							constexpr Reg Value_kept = 0x0;
							constexpr Reg ValueMask_kept = 0x00000000;
							constexpr Reg Value_lose = 0x1;
							constexpr Reg ValueMask_lose = 0x00400000;
							constexpr Reg Value_lost = 0x1;
							constexpr Reg ValueMask_lost = 0x00400000;
							};
						namespace fAHBRAM2SO { // Field Description
							constexpr Reg Lsb = 23;
							constexpr Reg FieldMask = 0x00800000;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_keep = 0x0;
							constexpr Reg ValueMask_keep = 0x00000000;
							constexpr Reg Value_kept = 0x0;
							constexpr Reg ValueMask_kept = 0x00000000;
							constexpr Reg Value_lose = 0x1;
							constexpr Reg ValueMask_lose = 0x00800000;
							constexpr Reg Value_lost = 0x1;
							constexpr Reg ValueMask_lost = 0x00800000;
							};
						namespace fITCMSO { // Field Description
							constexpr Reg Lsb = 24;
							constexpr Reg FieldMask = 0x01000000;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_keep = 0x0;
							constexpr Reg ValueMask_keep = 0x00000000;
							constexpr Reg Value_kept = 0x0;
							constexpr Reg ValueMask_kept = 0x00000000;
							constexpr Reg Value_lose = 0x1;
							constexpr Reg ValueMask_lose = 0x01000000;
							constexpr Reg Value_lost = 0x1;
							constexpr Reg ValueMask_lost = 0x01000000;
							};
						namespace fGFXSO { // Field Description
							constexpr Reg Lsb = 25;
							constexpr Reg FieldMask = 0x02000000;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_keep = 0x0;
							constexpr Reg ValueMask_keep = 0x00000000;
							constexpr Reg Value_kept = 0x0;
							constexpr Reg ValueMask_kept = 0x00000000;
							constexpr Reg Value_lose = 0x1;
							constexpr Reg ValueMask_lose = 0x02000000;
							constexpr Reg Value_lost = 0x1;
							constexpr Reg ValueMask_lost = 0x02000000;
							};
						namespace fHSITFSO { // Field Description
							constexpr Reg Lsb = 26;
							constexpr Reg FieldMask = 0x04000000;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_keep = 0x0;
							constexpr Reg ValueMask_keep = 0x00000000;
							constexpr Reg Value_kept = 0x0;
							constexpr Reg ValueMask_kept = 0x00000000;
							constexpr Reg Value_lose = 0x1;
							constexpr Reg ValueMask_lose = 0x04000000;
							constexpr Reg Value_lost = 0x1;
							constexpr Reg ValueMask_lost = 0x04000000;
							};
						namespace fSRDRAMSO { // Field Description
							constexpr Reg Lsb = 27;
							constexpr Reg FieldMask = 0x08000000;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_keep = 0x0;
							constexpr Reg ValueMask_keep = 0x00000000;
							constexpr Reg Value_kept = 0x0;
							constexpr Reg ValueMask_kept = 0x00000000;
							constexpr Reg Value_lose = 0x1;
							constexpr Reg ValueMask_lose = 0x08000000;
							constexpr Reg Value_lost = 0x1;
							constexpr Reg ValueMask_lost = 0x08000000;
							};
						};
					namespace rCSR1 { // Register description
						typedef uint32_t	Reg;
						constexpr Reg	valueAtReset = 0x00004000;
						namespace fPVDO { // Field Description
							constexpr Reg Lsb = 4;
							constexpr Reg FieldMask = 0x00000010;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_vddIsGreaterThanOrEqualToVOS = 0x0;
							constexpr Reg ValueMask_vddIsGreaterThanOrEqualToVOS = 0x00000000;
							constexpr Reg Value_vddIsLessThanVOS = 0x1;
							constexpr Reg ValueMask_vddIsLessThanVOS = 0x00000010;
							};
						namespace fACTVOSRDY { // Field Description
							constexpr Reg Lsb = 13;
							constexpr Reg FieldMask = 0x00002000;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_invalid = 0x0;
							constexpr Reg ValueMask_invalid = 0x00000000;
							constexpr Reg Value_valid = 0x1;
							constexpr Reg ValueMask_valid = 0x00002000;
							};
						namespace fACTVOS { // Field Description
							constexpr Reg Lsb = 14;
							constexpr Reg FieldMask = 0x0000C000;
							constexpr Reg Value_valueAtReset = 0x1;
							constexpr Reg ValueMask_valueAtReset = 0x00004000;
							constexpr Reg Value_minValue = 0x0;
							constexpr Reg ValueMask_minValue = 0x00000000;
							constexpr Reg Value_maxValue = 0x3;
							constexpr Reg ValueMask_maxValue = 0x0000C000;
							};
						namespace fAVDO { // Field Description
							constexpr Reg Lsb = 16;
							constexpr Reg FieldMask = 0x00010000;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_vddaIsGreaterThanOrEqualToALS = 0x0;
							constexpr Reg ValueMask_vddaIsGreaterThanOrEqualToALS = 0x00000000;
							constexpr Reg Value_vddaIsLessThanALS = 0x1;
							constexpr Reg ValueMask_vddaIsLessThanALS = 0x00010000;
							};
						namespace fMMCVDO { // Field Description
							constexpr Reg Lsb = 17;
							constexpr Reg FieldMask = 0x00020000;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_vddmmcIsBelow1_2V = 0x0;
							constexpr Reg ValueMask_vddmmcIsBelow1_2V = 0x00000000;
							constexpr Reg Value_vddmmcIsAtLeast1_2V = 0x1;
							constexpr Reg ValueMask_vddmmcIsAtLeast1_2V = 0x00020000;
							};
						};
					namespace rCR2 { // Register description
						typedef uint32_t	Reg;
						constexpr Reg	valueAtReset = 0x00000000;
						namespace fBREN { // Field Description
							constexpr Reg Lsb = 0;
							constexpr Reg FieldMask = 0x00000001;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_disable = 0x0;
							constexpr Reg ValueMask_disable = 0x00000000;
							constexpr Reg Value_disabled = 0x0;
							constexpr Reg ValueMask_disabled = 0x00000000;
							constexpr Reg Value_enable = 0x1;
							constexpr Reg ValueMask_enable = 0x00000001;
							constexpr Reg Value_enabled = 0x1;
							constexpr Reg ValueMask_enabled = 0x00000001;
							};
						namespace fMONEN { // Field Description
							constexpr Reg Lsb = 4;
							constexpr Reg FieldMask = 0x00000010;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_disable = 0x0;
							constexpr Reg ValueMask_disable = 0x00000000;
							constexpr Reg Value_disabled = 0x0;
							constexpr Reg ValueMask_disabled = 0x00000000;
							constexpr Reg Value_enable = 0x1;
							constexpr Reg ValueMask_enable = 0x00000010;
							constexpr Reg Value_enabled = 0x1;
							constexpr Reg ValueMask_enabled = 0x00000010;
							};
						namespace fBRRDY { // Field Description
							constexpr Reg Lsb = 16;
							constexpr Reg FieldMask = 0x00010000;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_notReady = 0x0;
							constexpr Reg ValueMask_notReady = 0x00000000;
							constexpr Reg Value_ready = 0x1;
							constexpr Reg ValueMask_ready = 0x00010000;
							};
						namespace fTEMPL { // Field Description
							constexpr Reg Lsb = 22;
							constexpr Reg FieldMask = 0x00400000;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_above = 0x0;
							constexpr Reg ValueMask_above = 0x00000000;
							constexpr Reg Value_equalOrBelow = 0x1;
							constexpr Reg ValueMask_equalOrBelow = 0x00400000;
							};
						namespace fTEMPH { // Field Description
							constexpr Reg Lsb = 23;
							constexpr Reg FieldMask = 0x00800000;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_below = 0x0;
							constexpr Reg ValueMask_below = 0x00000000;
							constexpr Reg Value_equalOrAbove = 0x1;
							constexpr Reg ValueMask_equalOrAbove = 0x00800000;
							};
						};
					namespace rCR3 { // Register description
						typedef uint32_t	Reg;
						constexpr Reg	valueAtReset = 0x00000006;
						namespace fBYPASS { // Field Description
							constexpr Reg Lsb = 0;
							constexpr Reg FieldMask = 0x00000001;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_normal = 0x0;
							constexpr Reg ValueMask_normal = 0x00000000;
							constexpr Reg Value_bypass = 0x1;
							constexpr Reg ValueMask_bypass = 0x00000001;
							constexpr Reg Value_bypassed = 0x1;
							constexpr Reg ValueMask_bypassed = 0x00000001;
							};
						namespace fLDOEN { // Field Description
							constexpr Reg Lsb = 1;
							constexpr Reg FieldMask = 0x00000002;
							constexpr Reg Value_valueAtReset = 0x1;
							constexpr Reg ValueMask_valueAtReset = 0x00000002;
							constexpr Reg Value_disable = 0x0;
							constexpr Reg ValueMask_disable = 0x00000000;
							constexpr Reg Value_disabled = 0x0;
							constexpr Reg ValueMask_disabled = 0x00000000;
							constexpr Reg Value_enable = 0x1;
							constexpr Reg ValueMask_enable = 0x00000002;
							constexpr Reg Value_enabled = 0x1;
							constexpr Reg ValueMask_enabled = 0x00000002;
							};
						namespace fSMPSEN { // Field Description
							constexpr Reg Lsb = 2;
							constexpr Reg FieldMask = 0x00000004;
							constexpr Reg Value_valueAtReset = 0x1;
							constexpr Reg ValueMask_valueAtReset = 0x00000004;
							constexpr Reg Value_disable = 0x0;
							constexpr Reg ValueMask_disable = 0x00000000;
							constexpr Reg Value_disabled = 0x0;
							constexpr Reg ValueMask_disabled = 0x00000000;
							constexpr Reg Value_enable = 0x1;
							constexpr Reg ValueMask_enable = 0x00000004;
							constexpr Reg Value_enabled = 0x1;
							constexpr Reg ValueMask_enabled = 0x00000004;
							};
						namespace fSMPSEXTHP { // Field Description
							constexpr Reg Lsb = 3;
							constexpr Reg FieldMask = 0x00000008;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_normal = 0x0;
							constexpr Reg ValueMask_normal = 0x00000000;
							constexpr Reg Value_external = 0x1;
							constexpr Reg ValueMask_external = 0x00000008;
							};
						namespace fSMPSLEVEL { // Field Description
							constexpr Reg Lsb = 4;
							constexpr Reg FieldMask = 0x00000030;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_v1_8Va = 0x0;
							constexpr Reg ValueMask_v1_8Va = 0x00000000;
							constexpr Reg Value_v1_8Vb = 0x1;
							constexpr Reg ValueMask_v1_8Vb = 0x00000010;
							constexpr Reg Value_v2_5Va = 0x2;
							constexpr Reg ValueMask_v2_5Va = 0x00000020;
							constexpr Reg Value_v2_5Vb = 0x3;
							constexpr Reg ValueMask_v2_5Vb = 0x00000030;
							};
						namespace fVBE { // Field Description
							constexpr Reg Lsb = 8;
							constexpr Reg FieldMask = 0x00000100;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_disable = 0x0;
							constexpr Reg ValueMask_disable = 0x00000000;
							constexpr Reg Value_disabled = 0x0;
							constexpr Reg ValueMask_disabled = 0x00000000;
							constexpr Reg Value_enable = 0x1;
							constexpr Reg ValueMask_enable = 0x00000100;
							constexpr Reg Value_enabled = 0x1;
							constexpr Reg ValueMask_enabled = 0x00000100;
							};
						namespace fVBRS { // Field Description
							constexpr Reg Lsb = 9;
							constexpr Reg FieldMask = 0x00000200;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_v5KOhm = 0x0;
							constexpr Reg ValueMask_v5KOhm = 0x00000000;
							constexpr Reg Value_v1_5KOhm = 0x1;
							constexpr Reg ValueMask_v1_5KOhm = 0x00000200;
							};
						namespace fSMPSEXTRDY { // Field Description
							constexpr Reg Lsb = 16;
							constexpr Reg FieldMask = 0x00010000;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_notReady = 0x0;
							constexpr Reg ValueMask_notReady = 0x00000000;
							constexpr Reg Value_ready = 0x1;
							constexpr Reg ValueMask_ready = 0x00010000;
							};
						namespace fUSB33DEN { // Field Description
							constexpr Reg Lsb = 24;
							constexpr Reg FieldMask = 0x01000000;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_disable = 0x0;
							constexpr Reg ValueMask_disable = 0x00000000;
							constexpr Reg Value_disabled = 0x0;
							constexpr Reg ValueMask_disabled = 0x00000000;
							constexpr Reg Value_enable = 0x1;
							constexpr Reg ValueMask_enable = 0x01000000;
							constexpr Reg Value_enabled = 0x1;
							constexpr Reg ValueMask_enabled = 0x01000000;
							};
						namespace fUSBREGEN { // Field Description
							constexpr Reg Lsb = 25;
							constexpr Reg FieldMask = 0x02000000;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_disable = 0x0;
							constexpr Reg ValueMask_disable = 0x00000000;
							constexpr Reg Value_disabled = 0x0;
							constexpr Reg ValueMask_disabled = 0x00000000;
							constexpr Reg Value_enable = 0x1;
							constexpr Reg ValueMask_enable = 0x02000000;
							constexpr Reg Value_enabled = 0x1;
							constexpr Reg ValueMask_enabled = 0x02000000;
							};
						namespace fUSB33RDY { // Field Description
							constexpr Reg Lsb = 26;
							constexpr Reg FieldMask = 0x04000000;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_notReady = 0x0;
							constexpr Reg ValueMask_notReady = 0x00000000;
							constexpr Reg Value_ready = 0x1;
							constexpr Reg ValueMask_ready = 0x04000000;
							};
						};
					namespace rCPUCR { // Register description
						typedef uint32_t	Reg;
						constexpr Reg	valueAtReset = 0x00000000;
						namespace fRETDS_CD { // Field Description
							constexpr Reg Lsb = 0;
							constexpr Reg FieldMask = 0x00000001;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_dStop = 0x0;
							constexpr Reg ValueMask_dStop = 0x00000000;
							constexpr Reg Value_dStop2 = 0x1;
							constexpr Reg ValueMask_dStop2 = 0x00000001;
							};
						namespace fPDDS_SRD { // Field Description
							constexpr Reg Lsb = 2;
							constexpr Reg FieldMask = 0x00000004;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_keepStopMode = 0x0;
							constexpr Reg ValueMask_keepStopMode = 0x00000000;
							constexpr Reg Value_allowStandby = 0x1;
							constexpr Reg ValueMask_allowStandby = 0x00000004;
							};
						namespace fSTOPF { // Field Description
							constexpr Reg Lsb = 5;
							constexpr Reg FieldMask = 0x00000020;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_notBeenInStopMode = 0x0;
							constexpr Reg ValueMask_notBeenInStopMode = 0x00000000;
							constexpr Reg Value_beenInStopMode = 0x1;
							constexpr Reg ValueMask_beenInStopMode = 0x00000020;
							};
						namespace fSBF { // Field Description
							constexpr Reg Lsb = 6;
							constexpr Reg FieldMask = 0x00000040;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_notBeenInStandbyMode = 0x0;
							constexpr Reg ValueMask_notBeenInStandbyMode = 0x00000000;
							constexpr Reg Value_beenInStandbyMode = 0x1;
							constexpr Reg ValueMask_beenInStandbyMode = 0x00000040;
							};
						namespace fCSSF { // Field Description
							constexpr Reg Lsb = 9;
							constexpr Reg FieldMask = 0x00000200;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_noEffect = 0x0;
							constexpr Reg ValueMask_noEffect = 0x00000000;
							constexpr Reg Value_clearFlags = 0x1;
							constexpr Reg ValueMask_clearFlags = 0x00000200;
							};
						namespace fRUN_SRD { // Field Description
							constexpr Reg Lsb = 11;
							constexpr Reg FieldMask = 0x00000800;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_followCpuModes = 0x0;
							constexpr Reg ValueMask_followCpuModes = 0x00000000;
							constexpr Reg Value_remainInRunMode = 0x1;
							constexpr Reg ValueMask_remainInRunMode = 0x00000800;
							};
						};
					namespace rSRDCR { // Register description
						typedef uint32_t	Reg;
						constexpr Reg	valueAtReset = 0x00002000;
						namespace fVOSRDY { // Field Description
							constexpr Reg Lsb = 13;
							constexpr Reg FieldMask = 0x00002000;
							constexpr Reg Value_valueAtReset = 0x1;
							constexpr Reg ValueMask_valueAtReset = 0x00002000;
							constexpr Reg Value_notReady = 0x0;
							constexpr Reg ValueMask_notReady = 0x00000000;
							constexpr Reg Value_ready = 0x1;
							constexpr Reg ValueMask_ready = 0x00002000;
							};
						namespace fVOS { // Field Description
							constexpr Reg Lsb = 14;
							constexpr Reg FieldMask = 0x0000C000;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_scale3 = 0x0;
							constexpr Reg ValueMask_scale3 = 0x00000000;
							constexpr Reg Value_scale2 = 0x1;
							constexpr Reg ValueMask_scale2 = 0x00004000;
							constexpr Reg Value_scale1 = 0x2;
							constexpr Reg ValueMask_scale1 = 0x00008000;
							constexpr Reg Value_scale0 = 0x3;
							constexpr Reg ValueMask_scale0 = 0x0000C000;
							};
						};
					namespace rWKUPCR { // Register description
						typedef uint32_t	Reg;
						constexpr Reg	valueAtReset = 0x00002000;
						namespace fWKUPC1 { // Field Description
							constexpr Reg Lsb = 0;
							constexpr Reg FieldMask = 0x00000001;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_noEffect = 0x0;
							constexpr Reg ValueMask_noEffect = 0x00000000;
							constexpr Reg Value_clear = 0x1;
							constexpr Reg ValueMask_clear = 0x00000001;
							};
						namespace fWKUPC2 { // Field Description
							constexpr Reg Lsb = 1;
							constexpr Reg FieldMask = 0x00000002;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_noEffect = 0x0;
							constexpr Reg ValueMask_noEffect = 0x00000000;
							constexpr Reg Value_clear = 0x1;
							constexpr Reg ValueMask_clear = 0x00000002;
							};
						namespace fWKUPC3 { // Field Description
							constexpr Reg Lsb = 2;
							constexpr Reg FieldMask = 0x00000004;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_noEffect = 0x0;
							constexpr Reg ValueMask_noEffect = 0x00000000;
							constexpr Reg Value_clear = 0x1;
							constexpr Reg ValueMask_clear = 0x00000004;
							};
						namespace fWKUPC4 { // Field Description
							constexpr Reg Lsb = 3;
							constexpr Reg FieldMask = 0x00000008;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_noEffect = 0x0;
							constexpr Reg ValueMask_noEffect = 0x00000000;
							constexpr Reg Value_clear = 0x1;
							constexpr Reg ValueMask_clear = 0x00000008;
							};
						namespace fWKUPC5 { // Field Description
							constexpr Reg Lsb = 4;
							constexpr Reg FieldMask = 0x00000010;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_noEffect = 0x0;
							constexpr Reg ValueMask_noEffect = 0x00000000;
							constexpr Reg Value_clear = 0x1;
							constexpr Reg ValueMask_clear = 0x00000010;
							};
						namespace fWKUPC6 { // Field Description
							constexpr Reg Lsb = 5;
							constexpr Reg FieldMask = 0x00000020;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_noEffect = 0x0;
							constexpr Reg ValueMask_noEffect = 0x00000000;
							constexpr Reg Value_clear = 0x1;
							constexpr Reg ValueMask_clear = 0x00000020;
							};
						};
					namespace rWKUPFR { // Register description
						typedef uint32_t	Reg;
						constexpr Reg	valueAtReset = 0x00000000;
						namespace fWKUPF1 { // Field Description
							constexpr Reg Lsb = 0;
							constexpr Reg FieldMask = 0x00000001;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_noEventReceived = 0x0;
							constexpr Reg ValueMask_noEventReceived = 0x00000000;
							constexpr Reg Value_eventReceived = 0x1;
							constexpr Reg ValueMask_eventReceived = 0x00000001;
							};
						namespace fWKUPF2 { // Field Description
							constexpr Reg Lsb = 1;
							constexpr Reg FieldMask = 0x00000002;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_noEventReceived = 0x0;
							constexpr Reg ValueMask_noEventReceived = 0x00000000;
							constexpr Reg Value_eventReceived = 0x1;
							constexpr Reg ValueMask_eventReceived = 0x00000002;
							};
						namespace fWKUPF3 { // Field Description
							constexpr Reg Lsb = 2;
							constexpr Reg FieldMask = 0x00000004;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_noEventReceived = 0x0;
							constexpr Reg ValueMask_noEventReceived = 0x00000000;
							constexpr Reg Value_eventReceived = 0x1;
							constexpr Reg ValueMask_eventReceived = 0x00000004;
							};
						namespace fWKUPF4 { // Field Description
							constexpr Reg Lsb = 3;
							constexpr Reg FieldMask = 0x00000008;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_noEventReceived = 0x0;
							constexpr Reg ValueMask_noEventReceived = 0x00000000;
							constexpr Reg Value_eventReceived = 0x1;
							constexpr Reg ValueMask_eventReceived = 0x00000008;
							};
						namespace fWKUPF5 { // Field Description
							constexpr Reg Lsb = 4;
							constexpr Reg FieldMask = 0x00000010;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_noEventReceived = 0x0;
							constexpr Reg ValueMask_noEventReceived = 0x00000000;
							constexpr Reg Value_eventReceived = 0x1;
							constexpr Reg ValueMask_eventReceived = 0x00000010;
							};
						namespace fWKUPF6 { // Field Description
							constexpr Reg Lsb = 5;
							constexpr Reg FieldMask = 0x00000020;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_noEventReceived = 0x0;
							constexpr Reg ValueMask_noEventReceived = 0x00000000;
							constexpr Reg Value_eventReceived = 0x1;
							constexpr Reg ValueMask_eventReceived = 0x00000020;
							};
						};
					namespace rWKUPEPR { // Register description
						typedef uint32_t	Reg;
						constexpr Reg	valueAtReset = 0x00000000;
						namespace fWKUPEN1 { // Field Description
							constexpr Reg Lsb = 0;
							constexpr Reg FieldMask = 0x00000001;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_noWakeup = 0x0;
							constexpr Reg ValueMask_noWakeup = 0x00000000;
							constexpr Reg Value_wakeup = 0x1;
							constexpr Reg ValueMask_wakeup = 0x00000001;
							};
						namespace fWKUPEN2 { // Field Description
							constexpr Reg Lsb = 1;
							constexpr Reg FieldMask = 0x00000002;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_noWakeup = 0x0;
							constexpr Reg ValueMask_noWakeup = 0x00000000;
							constexpr Reg Value_wakeup = 0x1;
							constexpr Reg ValueMask_wakeup = 0x00000002;
							};
						namespace fWKUPEN3 { // Field Description
							constexpr Reg Lsb = 2;
							constexpr Reg FieldMask = 0x00000004;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_noWakeup = 0x0;
							constexpr Reg ValueMask_noWakeup = 0x00000000;
							constexpr Reg Value_wakeup = 0x1;
							constexpr Reg ValueMask_wakeup = 0x00000004;
							};
						namespace fWKUPEN4 { // Field Description
							constexpr Reg Lsb = 3;
							constexpr Reg FieldMask = 0x00000008;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_noWakeup = 0x0;
							constexpr Reg ValueMask_noWakeup = 0x00000000;
							constexpr Reg Value_wakeup = 0x1;
							constexpr Reg ValueMask_wakeup = 0x00000008;
							};
						namespace fWKUPEN5 { // Field Description
							constexpr Reg Lsb = 4;
							constexpr Reg FieldMask = 0x00000010;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_noWakeup = 0x0;
							constexpr Reg ValueMask_noWakeup = 0x00000000;
							constexpr Reg Value_wakeup = 0x1;
							constexpr Reg ValueMask_wakeup = 0x00000010;
							};
						namespace fWKUPEN6 { // Field Description
							constexpr Reg Lsb = 5;
							constexpr Reg FieldMask = 0x00000020;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_noWakeup = 0x0;
							constexpr Reg ValueMask_noWakeup = 0x00000000;
							constexpr Reg Value_wakeup = 0x1;
							constexpr Reg ValueMask_wakeup = 0x00000020;
							};
						namespace fWKUPP1 { // Field Description
							constexpr Reg Lsb = 8;
							constexpr Reg FieldMask = 0x00000100;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_risingEdge = 0x0;
							constexpr Reg ValueMask_risingEdge = 0x00000000;
							constexpr Reg Value_fallingEdge = 0x1;
							constexpr Reg ValueMask_fallingEdge = 0x00000100;
							};
						namespace fWKUPP2 { // Field Description
							constexpr Reg Lsb = 9;
							constexpr Reg FieldMask = 0x00000200;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_risingEdge = 0x0;
							constexpr Reg ValueMask_risingEdge = 0x00000000;
							constexpr Reg Value_fallingEdge = 0x1;
							constexpr Reg ValueMask_fallingEdge = 0x00000200;
							};
						namespace fWKUPP3 { // Field Description
							constexpr Reg Lsb = 10;
							constexpr Reg FieldMask = 0x00000400;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_risingEdge = 0x0;
							constexpr Reg ValueMask_risingEdge = 0x00000000;
							constexpr Reg Value_fallingEdge = 0x1;
							constexpr Reg ValueMask_fallingEdge = 0x00000400;
							};
						namespace fWKUPP4 { // Field Description
							constexpr Reg Lsb = 11;
							constexpr Reg FieldMask = 0x00000800;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_risingEdge = 0x0;
							constexpr Reg ValueMask_risingEdge = 0x00000000;
							constexpr Reg Value_fallingEdge = 0x1;
							constexpr Reg ValueMask_fallingEdge = 0x00000800;
							};
						namespace fWKUPP5 { // Field Description
							constexpr Reg Lsb = 12;
							constexpr Reg FieldMask = 0x00001000;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_risingEdge = 0x0;
							constexpr Reg ValueMask_risingEdge = 0x00000000;
							constexpr Reg Value_fallingEdge = 0x1;
							constexpr Reg ValueMask_fallingEdge = 0x00001000;
							};
						namespace fWKUPP6 { // Field Description
							constexpr Reg Lsb = 13;
							constexpr Reg FieldMask = 0x00002000;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_risingEdge = 0x0;
							constexpr Reg ValueMask_risingEdge = 0x00000000;
							constexpr Reg Value_fallingEdge = 0x1;
							constexpr Reg ValueMask_fallingEdge = 0x00002000;
							};
						namespace fWKUPPUPD1 { // Field Description
							constexpr Reg Lsb = 16;
							constexpr Reg FieldMask = 0x00030000;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_noPull = 0x0;
							constexpr Reg ValueMask_noPull = 0x00000000;
							constexpr Reg Value_pullUP = 0x1;
							constexpr Reg ValueMask_pullUP = 0x00010000;
							constexpr Reg Value_pullDOWN = 0x2;
							constexpr Reg ValueMask_pullDOWN = 0x00020000;
							};
						namespace fWKUPPUPD2 { // Field Description
							constexpr Reg Lsb = 18;
							constexpr Reg FieldMask = 0x000C0000;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_noPull = 0x0;
							constexpr Reg ValueMask_noPull = 0x00000000;
							constexpr Reg Value_pullUP = 0x1;
							constexpr Reg ValueMask_pullUP = 0x00040000;
							constexpr Reg Value_pullDOWN = 0x2;
							constexpr Reg ValueMask_pullDOWN = 0x00080000;
							};
						namespace fWKUPPUPD3 { // Field Description
							constexpr Reg Lsb = 20;
							constexpr Reg FieldMask = 0x00300000;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_noPull = 0x0;
							constexpr Reg ValueMask_noPull = 0x00000000;
							constexpr Reg Value_pullUP = 0x1;
							constexpr Reg ValueMask_pullUP = 0x00100000;
							constexpr Reg Value_pullDOWN = 0x2;
							constexpr Reg ValueMask_pullDOWN = 0x00200000;
							};
						namespace fWKUPPUPD4 { // Field Description
							constexpr Reg Lsb = 22;
							constexpr Reg FieldMask = 0x00C00000;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_noPull = 0x0;
							constexpr Reg ValueMask_noPull = 0x00000000;
							constexpr Reg Value_pullUP = 0x1;
							constexpr Reg ValueMask_pullUP = 0x00400000;
							constexpr Reg Value_pullDOWN = 0x2;
							constexpr Reg ValueMask_pullDOWN = 0x00800000;
							};
						namespace fWKUPPUPD5 { // Field Description
							constexpr Reg Lsb = 24;
							constexpr Reg FieldMask = 0x03000000;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_noPull = 0x0;
							constexpr Reg ValueMask_noPull = 0x00000000;
							constexpr Reg Value_pullUP = 0x1;
							constexpr Reg ValueMask_pullUP = 0x01000000;
							constexpr Reg Value_pullDOWN = 0x2;
							constexpr Reg ValueMask_pullDOWN = 0x02000000;
							};
						namespace fWKUPPUPD6 { // Field Description
							constexpr Reg Lsb = 26;
							constexpr Reg FieldMask = 0x0C000000;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_noPull = 0x0;
							constexpr Reg ValueMask_noPull = 0x00000000;
							constexpr Reg Value_pullUP = 0x1;
							constexpr Reg ValueMask_pullUP = 0x04000000;
							constexpr Reg Value_pullDOWN = 0x2;
							constexpr Reg ValueMask_pullDOWN = 0x08000000;
							};
						};
					}
				}
			}
		}
	}
#endif
