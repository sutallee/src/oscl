/*
   Copyright (C) 2024 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_hw_st_stm32_h7a3b3b0_flash_maph_
#define _oscl_hw_st_stm32_h7a3b3b0_flash_maph_
#include "reg.h"
/** */
namespace Oscl {
/** */
namespace St {
/** */
namespace Stm32 {
/** */
namespace H7A3B3B0 {
/** */
namespace pFLASH {
/** */
struct Bank {
	/** offset:0x0 */
	volatile rACR::Reg	acr;
	/** offset:0x4 */
	volatile rKEYR::Reg	keyr;
	/** offset:0x8 */
	volatile rOPTKEYR::Reg	optkeyr;
	/** offset:0xc */
	volatile rCR::Reg	cr;
	/** offset:0x10 */
	volatile rSR::Reg	sr;
	/** offset:0x14 */
	volatile rCCR::Reg	ccr;
	/** offset:0x18 */
	volatile rOPTCR::Reg	optcr;
	/** offset:0x1c */
	volatile rOPTSR_CUR::Reg	optsr_cur;
	/** offset:0x20 */
	volatile rOPTSR_PRG::Reg	optsr_prg;
	/** offset:0x24 */
	volatile rOPTCCR::Reg	optccr;
	/** offset:0x28 */
	volatile rPRAR_CUR::Reg	prar_cur;
	/** offset:0x2c */
	volatile rPRAR_PRG::Reg	prar_prg;
	/** offset:0x30 */
	volatile rSCAR_CUR::Reg	scar_cur;
	/** offset:0x34 */
	volatile rSCAR_PRG::Reg	scar_prg;
	/** offset:0x38 */
	volatile rWPSGN_CUR::Reg	wpsgn_cur;
	/** offset:0x3c */
	volatile rWPSGN_PRG::Reg	wpsgn_prg;
	/** offset:0x40 */
	volatile rBOOT_CURR::Reg	boot_curr;
	/** offset:0x44 */
	volatile rBOOT_PRGR::Reg	boot_prgr;
	const uint8_t	_reserve72[8];
	/** offset:0x50 */
	volatile rCRCCR::Reg	crccr;
	/** offset:0x54 */
	volatile rCRCSADD::Reg	crcsadd;
	/** offset:0x58 */
	volatile rCRCEADD::Reg	crceadd;
	/** offset:0x5c */
	volatile rCRCDATAR::Reg	crcdatar;
	/** offset:0x60 */
	volatile rECC_FA::Reg	ecc_fa;
	const uint8_t	_reserve100[4];
	/** offset:0x68 */
	volatile rOTPBL_CUR::Reg	otpbl_cur;
	/** offset:0x6c */
	volatile rOTPBL_PRG::Reg	otpbl_prg;
	const uint8_t	_reserve112[144];
	};
struct Common {
	/** offset:0x0 */
	volatile rACR::Reg	acr;

	/** offset: 0x4 */
	const uint8_t	_reserve004[4];

	/** offset:0x8 */
	volatile rOPTKEYR::Reg	optkeyr;

	/** offset: 0xC */
	const uint8_t	_reserve00C[0x18 - 0x0C];

	/** offset:0x18 */
	volatile rOPTCR::Reg	optcr;

	/** offset:0x1c */
	volatile rOPTSR_CUR::Reg	optsr_cur;

	/** offset:0x20 */
	volatile rOPTSR_PRG::Reg	optsr_prg;

	/** offset:0x24 */
	volatile rOPTCCR::Reg	optccr;

	/** offset: 0x28 */
	const uint8_t	_reserve028[0x40 - 0x28];

	/** offset:0x40 */
	volatile rBOOT_CURR::Reg	boot_curr;

	/** offset:0x44 */
	volatile rBOOT_PRGR::Reg	boot_prgr;

	/** offset: 0x48 */
	const uint8_t	_reserve048[0x5C - 0x48];

	/** offset:0x5c */
	volatile rCRCDATAR::Reg	crcdatar;
	};
/** */
struct Map {
	/** offset:0x0 */
	union {
		/** */
		Bank	bank[2];
		/** */
		Common	common;
		};
	};
}
}
}
}
}
#endif
