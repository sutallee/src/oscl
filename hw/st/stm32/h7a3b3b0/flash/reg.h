/*
   Copyright (C) 2024 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_hw_st_stm32_h7a3b3b0_flash_regh_
#define _oscl_hw_st_stm32_h7a3b3b0_flash_regh_
#include <stdint.h>
namespace Oscl { // Namespace description
	namespace St { // Namespace description
		namespace Stm32 { // Namespace description
			namespace H7A3B3B0 { // Namespace description
				namespace pFLASH { // Namespace description
					namespace rACR { // Register description
						typedef uint32_t	Reg;
						constexpr Reg	valueAtReset = 0x00000013;
						namespace fLATENCY { // Field Description
							constexpr Reg Lsb = 0;
							constexpr Reg FieldMask = 0x0000000F;
							constexpr Reg Value_valueAtReset = 0x3;
							constexpr Reg ValueMask_valueAtReset = 0x00000003;
							constexpr Reg Value_zeroWaitStates = 0x0;
							constexpr Reg ValueMask_zeroWaitStates = 0x00000000;
							constexpr Reg Value_oneWaitState = 0x1;
							constexpr Reg ValueMask_oneWaitState = 0x00000001;
							constexpr Reg Value_twoWaitStates = 0x2;
							constexpr Reg ValueMask_twoWaitStates = 0x00000002;
							constexpr Reg Value_threeWaitStates = 0x3;
							constexpr Reg ValueMask_threeWaitStates = 0x00000003;
							constexpr Reg Value_fourWaitStates = 0x4;
							constexpr Reg ValueMask_fourWaitStates = 0x00000004;
							constexpr Reg Value_fiveWaitStates = 0x5;
							constexpr Reg ValueMask_fiveWaitStates = 0x00000005;
							constexpr Reg Value_sixWaitStates = 0x6;
							constexpr Reg ValueMask_sixWaitStates = 0x00000006;
							constexpr Reg Value_sevenWaitStates = 0x7;
							constexpr Reg ValueMask_sevenWaitStates = 0x00000007;
							};
						namespace fWRHIGHFREQ { // Field Description
							constexpr Reg Lsb = 4;
							constexpr Reg FieldMask = 0x00000030;
							constexpr Reg Value_valueAtReset = 0x1;
							constexpr Reg ValueMask_valueAtReset = 0x00000010;
							constexpr Reg Value_zero = 0x0;
							constexpr Reg ValueMask_zero = 0x00000000;
							constexpr Reg Value_one = 0x1;
							constexpr Reg ValueMask_one = 0x00000010;
							constexpr Reg Value_two = 0x2;
							constexpr Reg ValueMask_two = 0x00000020;
							constexpr Reg Value_three = 0x3;
							constexpr Reg ValueMask_three = 0x00000030;
							};
						};
					namespace rKEYR { // Register description
						typedef uint32_t	Reg;
						constexpr Reg	valueAtReset = 0x00000000;
						namespace fKEYR { // Field Description
							constexpr Reg Lsb = 0;
							constexpr Reg FieldMask = 0xFFFFFFFF;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_firstKey = 0x45670123;
							constexpr Reg ValueMask_firstKey = 0x45670123;
							constexpr Reg Value_secondKey = 0xCDEF89AB;
							constexpr Reg ValueMask_secondKey = 0xCDEF89AB;
							};
						};
					namespace rOPTKEYR { // Register description
						typedef uint32_t	Reg;
						constexpr Reg	valueAtReset = 0x00000000;
						namespace fOPTKEYR { // Field Description
							constexpr Reg Lsb = 0;
							constexpr Reg FieldMask = 0xFFFFFFFF;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_firstKey = 0x8192A3B;
							constexpr Reg ValueMask_firstKey = 0x08192A3B;
							constexpr Reg Value_secondKey = 0x4C5D6E7F;
							constexpr Reg ValueMask_secondKey = 0x4C5D6E7F;
							};
						};
					namespace rCR { // Register description
						typedef uint32_t	Reg;
						constexpr Reg	valueAtReset = 0x00000001;
						namespace fLOCK { // Field Description
							constexpr Reg Lsb = 0;
							constexpr Reg FieldMask = 0x00000001;
							constexpr Reg Value_valueAtReset = 0x1;
							constexpr Reg ValueMask_valueAtReset = 0x00000001;
							constexpr Reg Value_unlock = 0x0;
							constexpr Reg ValueMask_unlock = 0x00000000;
							constexpr Reg Value_unlocked = 0x0;
							constexpr Reg ValueMask_unlocked = 0x00000000;
							constexpr Reg Value_lock = 0x1;
							constexpr Reg ValueMask_lock = 0x00000001;
							constexpr Reg Value_locked = 0x1;
							constexpr Reg ValueMask_locked = 0x00000001;
							};
						namespace fPG { // Field Description
							constexpr Reg Lsb = 1;
							constexpr Reg FieldMask = 0x00000002;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_disable = 0x0;
							constexpr Reg ValueMask_disable = 0x00000000;
							constexpr Reg Value_disabled = 0x0;
							constexpr Reg ValueMask_disabled = 0x00000000;
							constexpr Reg Value_enable = 0x1;
							constexpr Reg ValueMask_enable = 0x00000002;
							constexpr Reg Value_enabled = 0x1;
							constexpr Reg ValueMask_enabled = 0x00000002;
							};
						namespace fSER { // Field Description
							constexpr Reg Lsb = 2;
							constexpr Reg FieldMask = 0x00000004;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_noAction = 0x0;
							constexpr Reg ValueMask_noAction = 0x00000000;
							constexpr Reg Value_erase = 0x1;
							constexpr Reg ValueMask_erase = 0x00000004;
							};
						namespace fBER { // Field Description
							constexpr Reg Lsb = 3;
							constexpr Reg FieldMask = 0x00000008;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_noAction = 0x0;
							constexpr Reg ValueMask_noAction = 0x00000000;
							constexpr Reg Value_erase = 0x1;
							constexpr Reg ValueMask_erase = 0x00000008;
							};
						namespace fFW { // Field Description
							constexpr Reg Lsb = 4;
							constexpr Reg FieldMask = 0x00000010;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_noAction = 0x0;
							constexpr Reg ValueMask_noAction = 0x00000000;
							constexpr Reg Value_force = 0x1;
							constexpr Reg ValueMask_force = 0x00000010;
							};
						namespace fSTART { // Field Description
							constexpr Reg Lsb = 5;
							constexpr Reg FieldMask = 0x00000020;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_noAction = 0x0;
							constexpr Reg ValueMask_noAction = 0x00000000;
							constexpr Reg Value_start = 0x1;
							constexpr Reg ValueMask_start = 0x00000020;
							};
						namespace fSSN { // Field Description
							constexpr Reg Lsb = 6;
							constexpr Reg FieldMask = 0x00001FC0;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_minValue = 0x0;
							constexpr Reg ValueMask_minValue = 0x00000000;
							constexpr Reg Value_maxValue = 0x7F;
							constexpr Reg ValueMask_maxValue = 0x00001FC0;
							};
						namespace fCRC_EN { // Field Description
							constexpr Reg Lsb = 15;
							constexpr Reg FieldMask = 0x00008000;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_disable = 0x0;
							constexpr Reg ValueMask_disable = 0x00000000;
							constexpr Reg Value_disabled = 0x0;
							constexpr Reg ValueMask_disabled = 0x00000000;
							constexpr Reg Value_enable = 0x1;
							constexpr Reg ValueMask_enable = 0x00008000;
							constexpr Reg Value_enabled = 0x1;
							constexpr Reg ValueMask_enabled = 0x00008000;
							};
						namespace fEOPIE { // Field Description
							constexpr Reg Lsb = 16;
							constexpr Reg FieldMask = 0x00010000;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_disable = 0x0;
							constexpr Reg ValueMask_disable = 0x00000000;
							constexpr Reg Value_disabled = 0x0;
							constexpr Reg ValueMask_disabled = 0x00000000;
							constexpr Reg Value_enable = 0x1;
							constexpr Reg ValueMask_enable = 0x00010000;
							constexpr Reg Value_enabled = 0x1;
							constexpr Reg ValueMask_enabled = 0x00010000;
							};
						namespace fWRPERRIE { // Field Description
							constexpr Reg Lsb = 17;
							constexpr Reg FieldMask = 0x00020000;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_disable = 0x0;
							constexpr Reg ValueMask_disable = 0x00000000;
							constexpr Reg Value_disabled = 0x0;
							constexpr Reg ValueMask_disabled = 0x00000000;
							constexpr Reg Value_enable = 0x1;
							constexpr Reg ValueMask_enable = 0x00020000;
							constexpr Reg Value_enabled = 0x1;
							constexpr Reg ValueMask_enabled = 0x00020000;
							};
						namespace fPGSERRIE { // Field Description
							constexpr Reg Lsb = 18;
							constexpr Reg FieldMask = 0x00040000;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_disable = 0x0;
							constexpr Reg ValueMask_disable = 0x00000000;
							constexpr Reg Value_disabled = 0x0;
							constexpr Reg ValueMask_disabled = 0x00000000;
							constexpr Reg Value_enable = 0x1;
							constexpr Reg ValueMask_enable = 0x00040000;
							constexpr Reg Value_enabled = 0x1;
							constexpr Reg ValueMask_enabled = 0x00040000;
							};
						namespace fSTRBERRIE { // Field Description
							constexpr Reg Lsb = 19;
							constexpr Reg FieldMask = 0x00080000;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_disable = 0x0;
							constexpr Reg ValueMask_disable = 0x00000000;
							constexpr Reg Value_disabled = 0x0;
							constexpr Reg ValueMask_disabled = 0x00000000;
							constexpr Reg Value_enable = 0x1;
							constexpr Reg ValueMask_enable = 0x00080000;
							constexpr Reg Value_enabled = 0x1;
							constexpr Reg ValueMask_enabled = 0x00080000;
							};
						namespace fINCERRIE { // Field Description
							constexpr Reg Lsb = 21;
							constexpr Reg FieldMask = 0x00200000;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_disable = 0x0;
							constexpr Reg ValueMask_disable = 0x00000000;
							constexpr Reg Value_disabled = 0x0;
							constexpr Reg ValueMask_disabled = 0x00000000;
							constexpr Reg Value_enable = 0x1;
							constexpr Reg ValueMask_enable = 0x00200000;
							constexpr Reg Value_enabled = 0x1;
							constexpr Reg ValueMask_enabled = 0x00200000;
							};
						namespace fRDPERRIE { // Field Description
							constexpr Reg Lsb = 23;
							constexpr Reg FieldMask = 0x00800000;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_disable = 0x0;
							constexpr Reg ValueMask_disable = 0x00000000;
							constexpr Reg Value_disabled = 0x0;
							constexpr Reg ValueMask_disabled = 0x00000000;
							constexpr Reg Value_enable = 0x1;
							constexpr Reg ValueMask_enable = 0x00800000;
							constexpr Reg Value_enabled = 0x1;
							constexpr Reg ValueMask_enabled = 0x00800000;
							};
						namespace fRDSERRIE { // Field Description
							constexpr Reg Lsb = 24;
							constexpr Reg FieldMask = 0x01000000;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_disable = 0x0;
							constexpr Reg ValueMask_disable = 0x00000000;
							constexpr Reg Value_disabled = 0x0;
							constexpr Reg ValueMask_disabled = 0x00000000;
							constexpr Reg Value_enable = 0x1;
							constexpr Reg ValueMask_enable = 0x01000000;
							constexpr Reg Value_enabled = 0x1;
							constexpr Reg ValueMask_enabled = 0x01000000;
							};
						namespace fSNECCERRIE { // Field Description
							constexpr Reg Lsb = 25;
							constexpr Reg FieldMask = 0x02000000;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_disable = 0x0;
							constexpr Reg ValueMask_disable = 0x00000000;
							constexpr Reg Value_disabled = 0x0;
							constexpr Reg ValueMask_disabled = 0x00000000;
							constexpr Reg Value_enable = 0x1;
							constexpr Reg ValueMask_enable = 0x02000000;
							constexpr Reg Value_enabled = 0x1;
							constexpr Reg ValueMask_enabled = 0x02000000;
							};
						namespace fDBECCERRIE { // Field Description
							constexpr Reg Lsb = 26;
							constexpr Reg FieldMask = 0x04000000;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_disable = 0x0;
							constexpr Reg ValueMask_disable = 0x00000000;
							constexpr Reg Value_disabled = 0x0;
							constexpr Reg ValueMask_disabled = 0x00000000;
							constexpr Reg Value_enable = 0x1;
							constexpr Reg ValueMask_enable = 0x04000000;
							constexpr Reg Value_enabled = 0x1;
							constexpr Reg ValueMask_enabled = 0x04000000;
							};
						namespace fCRCENDIE { // Field Description
							constexpr Reg Lsb = 27;
							constexpr Reg FieldMask = 0x08000000;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_disable = 0x0;
							constexpr Reg ValueMask_disable = 0x00000000;
							constexpr Reg Value_disabled = 0x0;
							constexpr Reg ValueMask_disabled = 0x00000000;
							constexpr Reg Value_enable = 0x1;
							constexpr Reg ValueMask_enable = 0x08000000;
							constexpr Reg Value_enabled = 0x1;
							constexpr Reg ValueMask_enabled = 0x08000000;
							};
						namespace fCRCRDERRIE { // Field Description
							constexpr Reg Lsb = 28;
							constexpr Reg FieldMask = 0x10000000;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_disable = 0x0;
							constexpr Reg ValueMask_disable = 0x00000000;
							constexpr Reg Value_disabled = 0x0;
							constexpr Reg ValueMask_disabled = 0x00000000;
							constexpr Reg Value_enable = 0x1;
							constexpr Reg ValueMask_enable = 0x10000000;
							constexpr Reg Value_enabled = 0x1;
							constexpr Reg ValueMask_enabled = 0x10000000;
							};
						};
					namespace rSR { // Register description
						typedef uint32_t	Reg;
						constexpr Reg	valueAtReset = 0x00000000;
						namespace fBSY { // Field Description
							constexpr Reg Lsb = 0;
							constexpr Reg FieldMask = 0x00000001;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_notBusy = 0x0;
							constexpr Reg ValueMask_notBusy = 0x00000000;
							constexpr Reg Value_busy = 0x1;
							constexpr Reg ValueMask_busy = 0x00000001;
							};
						namespace fWBNE { // Field Description
							constexpr Reg Lsb = 1;
							constexpr Reg FieldMask = 0x00000002;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_emptied = 0x0;
							constexpr Reg ValueMask_emptied = 0x00000000;
							constexpr Reg Value_notEmpty = 0x1;
							constexpr Reg ValueMask_notEmpty = 0x00000002;
							};
						namespace fQW { // Field Description
							constexpr Reg Lsb = 2;
							constexpr Reg FieldMask = 0x00000004;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_notPending = 0x0;
							constexpr Reg ValueMask_notPending = 0x00000000;
							constexpr Reg Value_pending = 0x1;
							constexpr Reg ValueMask_pending = 0x00000004;
							};
						namespace fCRC_BUSY { // Field Description
							constexpr Reg Lsb = 3;
							constexpr Reg FieldMask = 0x00000008;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_notBusy = 0x0;
							constexpr Reg ValueMask_notBusy = 0x00000000;
							constexpr Reg Value_busy = 0x1;
							constexpr Reg ValueMask_busy = 0x00000008;
							};
						namespace fEOP { // Field Description
							constexpr Reg Lsb = 16;
							constexpr Reg FieldMask = 0x00010000;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_complete = 0x1;
							constexpr Reg ValueMask_complete = 0x00010000;
							constexpr Reg Value_completed = 0x1;
							constexpr Reg ValueMask_completed = 0x00010000;
							};
						namespace fWRPERR { // Field Description
							constexpr Reg Lsb = 17;
							constexpr Reg FieldMask = 0x00020000;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_noError = 0x0;
							constexpr Reg ValueMask_noError = 0x00000000;
							constexpr Reg Value_error = 0x1;
							constexpr Reg ValueMask_error = 0x00020000;
							};
						namespace fPGSERR { // Field Description
							constexpr Reg Lsb = 18;
							constexpr Reg FieldMask = 0x00040000;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_noError = 0x0;
							constexpr Reg ValueMask_noError = 0x00000000;
							constexpr Reg Value_error = 0x1;
							constexpr Reg ValueMask_error = 0x00040000;
							};
						namespace fSTRBERR { // Field Description
							constexpr Reg Lsb = 19;
							constexpr Reg FieldMask = 0x00080000;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_noError = 0x0;
							constexpr Reg ValueMask_noError = 0x00000000;
							constexpr Reg Value_error = 0x1;
							constexpr Reg ValueMask_error = 0x00080000;
							};
						namespace fINCERR { // Field Description
							constexpr Reg Lsb = 21;
							constexpr Reg FieldMask = 0x00200000;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_noError = 0x0;
							constexpr Reg ValueMask_noError = 0x00000000;
							constexpr Reg Value_error = 0x1;
							constexpr Reg ValueMask_error = 0x00200000;
							};
						namespace fRDPERR { // Field Description
							constexpr Reg Lsb = 23;
							constexpr Reg FieldMask = 0x00800000;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_noError = 0x0;
							constexpr Reg ValueMask_noError = 0x00000000;
							constexpr Reg Value_error = 0x1;
							constexpr Reg ValueMask_error = 0x00800000;
							};
						namespace fRDSERR { // Field Description
							constexpr Reg Lsb = 24;
							constexpr Reg FieldMask = 0x01000000;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_noError = 0x0;
							constexpr Reg ValueMask_noError = 0x00000000;
							constexpr Reg Value_error = 0x1;
							constexpr Reg ValueMask_error = 0x01000000;
							};
						namespace fSNECCERR { // Field Description
							constexpr Reg Lsb = 25;
							constexpr Reg FieldMask = 0x02000000;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_noError = 0x0;
							constexpr Reg ValueMask_noError = 0x00000000;
							constexpr Reg Value_error = 0x1;
							constexpr Reg ValueMask_error = 0x02000000;
							};
						namespace fDBECCERR { // Field Description
							constexpr Reg Lsb = 26;
							constexpr Reg FieldMask = 0x04000000;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_noError = 0x0;
							constexpr Reg ValueMask_noError = 0x00000000;
							constexpr Reg Value_error = 0x1;
							constexpr Reg ValueMask_error = 0x04000000;
							};
						namespace fCRCEND { // Field Description
							constexpr Reg Lsb = 27;
							constexpr Reg FieldMask = 0x08000000;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_complete = 0x1;
							constexpr Reg ValueMask_complete = 0x08000000;
							constexpr Reg Value_completed = 0x1;
							constexpr Reg ValueMask_completed = 0x08000000;
							};
						namespace fCRCRDERR { // Field Description
							constexpr Reg Lsb = 28;
							constexpr Reg FieldMask = 0x10000000;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_noError = 0x0;
							constexpr Reg ValueMask_noError = 0x00000000;
							constexpr Reg Value_error = 0x1;
							constexpr Reg ValueMask_error = 0x10000000;
							};
						};
					namespace rCCR { // Register description
						typedef uint32_t	Reg;
						constexpr Reg	valueAtReset = 0x00000000;
						namespace fCLR_EOP { // Field Description
							constexpr Reg Lsb = 16;
							constexpr Reg FieldMask = 0x00010000;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_noAction = 0x0;
							constexpr Reg ValueMask_noAction = 0x00000000;
							constexpr Reg Value_clear = 0x1;
							constexpr Reg ValueMask_clear = 0x00010000;
							};
						namespace fCLR_WRPERR { // Field Description
							constexpr Reg Lsb = 17;
							constexpr Reg FieldMask = 0x00020000;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_noAction = 0x0;
							constexpr Reg ValueMask_noAction = 0x00000000;
							constexpr Reg Value_clear = 0x1;
							constexpr Reg ValueMask_clear = 0x00020000;
							};
						namespace fCLR_PGSERR { // Field Description
							constexpr Reg Lsb = 18;
							constexpr Reg FieldMask = 0x00040000;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_noAction = 0x0;
							constexpr Reg ValueMask_noAction = 0x00000000;
							constexpr Reg Value_clear = 0x1;
							constexpr Reg ValueMask_clear = 0x00040000;
							};
						namespace fCLR_STRBERR { // Field Description
							constexpr Reg Lsb = 19;
							constexpr Reg FieldMask = 0x00080000;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_noAction = 0x0;
							constexpr Reg ValueMask_noAction = 0x00000000;
							constexpr Reg Value_clear = 0x1;
							constexpr Reg ValueMask_clear = 0x00080000;
							};
						namespace fCLR_INCERR { // Field Description
							constexpr Reg Lsb = 21;
							constexpr Reg FieldMask = 0x00200000;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_noAction = 0x0;
							constexpr Reg ValueMask_noAction = 0x00000000;
							constexpr Reg Value_clear = 0x1;
							constexpr Reg ValueMask_clear = 0x00200000;
							};
						namespace fCLR_RDPERR { // Field Description
							constexpr Reg Lsb = 23;
							constexpr Reg FieldMask = 0x00800000;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_noAction = 0x0;
							constexpr Reg ValueMask_noAction = 0x00000000;
							constexpr Reg Value_clear = 0x1;
							constexpr Reg ValueMask_clear = 0x00800000;
							};
						namespace fCLR_RDSERR { // Field Description
							constexpr Reg Lsb = 24;
							constexpr Reg FieldMask = 0x01000000;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_noAction = 0x0;
							constexpr Reg ValueMask_noAction = 0x00000000;
							constexpr Reg Value_clear = 0x1;
							constexpr Reg ValueMask_clear = 0x01000000;
							};
						namespace fCLR_SNECCERR { // Field Description
							constexpr Reg Lsb = 25;
							constexpr Reg FieldMask = 0x02000000;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_noAction = 0x0;
							constexpr Reg ValueMask_noAction = 0x00000000;
							constexpr Reg Value_clear = 0x1;
							constexpr Reg ValueMask_clear = 0x02000000;
							};
						namespace fCLR_DBECCERR { // Field Description
							constexpr Reg Lsb = 26;
							constexpr Reg FieldMask = 0x04000000;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_noAction = 0x0;
							constexpr Reg ValueMask_noAction = 0x00000000;
							constexpr Reg Value_clear = 0x1;
							constexpr Reg ValueMask_clear = 0x04000000;
							};
						namespace fCLR_CRCEND { // Field Description
							constexpr Reg Lsb = 27;
							constexpr Reg FieldMask = 0x08000000;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_noAction = 0x0;
							constexpr Reg ValueMask_noAction = 0x00000000;
							constexpr Reg Value_clear = 0x1;
							constexpr Reg ValueMask_clear = 0x08000000;
							};
						namespace fCLR_CRCRDERR { // Field Description
							constexpr Reg Lsb = 28;
							constexpr Reg FieldMask = 0x10000000;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_noAction = 0x0;
							constexpr Reg ValueMask_noAction = 0x00000000;
							constexpr Reg Value_clear = 0x1;
							constexpr Reg ValueMask_clear = 0x10000000;
							};
						};
					namespace rOPTCR { // Register description
						typedef uint32_t	Reg;
						constexpr Reg	valueAtReset = 0x00000001;
						namespace fOPTLOCK { // Field Description
							constexpr Reg Lsb = 0;
							constexpr Reg FieldMask = 0x00000001;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_unlock = 0x0;
							constexpr Reg ValueMask_unlock = 0x00000000;
							constexpr Reg Value_unlocked = 0x0;
							constexpr Reg ValueMask_unlocked = 0x00000000;
							constexpr Reg Value_lock = 0x1;
							constexpr Reg ValueMask_lock = 0x00000001;
							constexpr Reg Value_locked = 0x1;
							constexpr Reg ValueMask_locked = 0x00000001;
							};
						namespace fOPTSTART { // Field Description
							constexpr Reg Lsb = 1;
							constexpr Reg FieldMask = 0x00000002;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_noAction = 0x0;
							constexpr Reg ValueMask_noAction = 0x00000000;
							constexpr Reg Value_start = 0x1;
							constexpr Reg ValueMask_start = 0x00000002;
							constexpr Reg Value_started = 0x1;
							constexpr Reg ValueMask_started = 0x00000002;
							};
						namespace fMER { // Field Description
							constexpr Reg Lsb = 4;
							constexpr Reg FieldMask = 0x00000010;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_noAction = 0x0;
							constexpr Reg ValueMask_noAction = 0x00000000;
							constexpr Reg Value_erase = 0x1;
							constexpr Reg ValueMask_erase = 0x00000010;
							};
						namespace fPG_OTP { // Field Description
							constexpr Reg Lsb = 5;
							constexpr Reg FieldMask = 0x00000020;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_noAction = 0x0;
							constexpr Reg ValueMask_noAction = 0x00000000;
							constexpr Reg Value_program = 0x1;
							constexpr Reg ValueMask_program = 0x00000020;
							};
						namespace fOPTCHANGEERRIE { // Field Description
							constexpr Reg Lsb = 30;
							constexpr Reg FieldMask = 0x40000000;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_disable = 0x0;
							constexpr Reg ValueMask_disable = 0x00000000;
							constexpr Reg Value_disabled = 0x0;
							constexpr Reg ValueMask_disabled = 0x00000000;
							constexpr Reg Value_enable = 0x1;
							constexpr Reg ValueMask_enable = 0x40000000;
							constexpr Reg Value_enabled = 0x1;
							constexpr Reg ValueMask_enabled = 0x40000000;
							};
						namespace fSWAP_BANK { // Field Description
							constexpr Reg Lsb = 31;
							constexpr Reg FieldMask = 0x80000000;
							constexpr Reg Value_valueAtReset = 0x1;
							constexpr Reg ValueMask_valueAtReset = 0x80000000;
							constexpr Reg Value_notSwapped = 0x0;
							constexpr Reg ValueMask_notSwapped = 0x00000000;
							constexpr Reg Value_swapped = 0x1;
							constexpr Reg ValueMask_swapped = 0x80000000;
							};
						};
					namespace rOPTSR_CUR { // Register description
						typedef uint32_t	Reg;
						namespace fOPT_BUSY { // Field Description
							constexpr Reg Lsb = 0;
							constexpr Reg FieldMask = 0x00000001;
							constexpr Reg Value_notBusy = 0x0;
							constexpr Reg ValueMask_notBusy = 0x00000000;
							constexpr Reg Value_busy = 0x1;
							constexpr Reg ValueMask_busy = 0x00000001;
							};
						namespace fBOR_LEV { // Field Description
							constexpr Reg Lsb = 2;
							constexpr Reg FieldMask = 0x0000000C;
							constexpr Reg Value_off = 0x0;
							constexpr Reg ValueMask_off = 0x00000000;
							constexpr Reg Value_v2_1V = 0x1;
							constexpr Reg ValueMask_v2_1V = 0x00000004;
							constexpr Reg Value_v2_4V = 0x2;
							constexpr Reg ValueMask_v2_4V = 0x00000008;
							constexpr Reg Value_v2_7V = 0x3;
							constexpr Reg ValueMask_v2_7V = 0x0000000C;
							};
						namespace fIWDG_SW { // Field Description
							constexpr Reg Lsb = 4;
							constexpr Reg FieldMask = 0x00000010;
							constexpr Reg Value_hardware = 0x0;
							constexpr Reg ValueMask_hardware = 0x00000000;
							constexpr Reg Value_software = 0x1;
							constexpr Reg ValueMask_software = 0x00000010;
							};
						namespace fNRST_STOP { // Field Description
							constexpr Reg Lsb = 6;
							constexpr Reg FieldMask = 0x00000040;
							constexpr Reg Value_reset = 0x0;
							constexpr Reg ValueMask_reset = 0x00000000;
							constexpr Reg Value_noReset = 0x1;
							constexpr Reg ValueMask_noReset = 0x00000040;
							};
						namespace fNRST_STDY { // Field Description
							constexpr Reg Lsb = 7;
							constexpr Reg FieldMask = 0x00000080;
							constexpr Reg Value_reset = 0x0;
							constexpr Reg ValueMask_reset = 0x00000000;
							constexpr Reg Value_noReset = 0x1;
							constexpr Reg ValueMask_noReset = 0x00000080;
							};
						namespace fRDP { // Field Description
							constexpr Reg Lsb = 8;
							constexpr Reg FieldMask = 0x0000FF00;
							constexpr Reg Value_level0 = 0xAA;
							constexpr Reg ValueMask_level0 = 0x0000AA00;
							constexpr Reg Value_level2 = 0xCC;
							constexpr Reg ValueMask_level2 = 0x0000CC00;
							constexpr Reg Value_level1 = 0x1;
							constexpr Reg ValueMask_level1 = 0x00000100;
							};
						namespace fVDDMMC_HSLV { // Field Description
							constexpr Reg Lsb = 16;
							constexpr Reg FieldMask = 0x00010000;
							constexpr Reg Value_disabled = 0x0;
							constexpr Reg ValueMask_disabled = 0x00000000;
							constexpr Reg Value_enabled = 0x1;
							constexpr Reg ValueMask_enabled = 0x00010000;
							};
						namespace fWDG_FZ_STOP { // Field Description
							constexpr Reg Lsb = 17;
							constexpr Reg FieldMask = 0x00020000;
							constexpr Reg Value_frozen = 0x0;
							constexpr Reg ValueMask_frozen = 0x00000000;
							constexpr Reg Value_running = 0x1;
							constexpr Reg ValueMask_running = 0x00020000;
							};
						namespace fIWDG_FZ_SDBY { // Field Description
							constexpr Reg Lsb = 18;
							constexpr Reg FieldMask = 0x00040000;
							constexpr Reg Value_frozen = 0x0;
							constexpr Reg ValueMask_frozen = 0x00000000;
							constexpr Reg Value_running = 0x1;
							constexpr Reg ValueMask_running = 0x00040000;
							};
						namespace fST_RAM_SIZE { // Field Description
							constexpr Reg Lsb = 19;
							constexpr Reg FieldMask = 0x00180000;
							constexpr Reg Value_v2KB = 0x0;
							constexpr Reg ValueMask_v2KB = 0x00000000;
							constexpr Reg Value_v4KB = 0x1;
							constexpr Reg ValueMask_v4KB = 0x00080000;
							constexpr Reg Value_v8KB = 0x2;
							constexpr Reg ValueMask_v8KB = 0x00100000;
							constexpr Reg Value_v16KB = 0x3;
							constexpr Reg ValueMask_v16KB = 0x00180000;
							};
						namespace fSECURITY { // Field Description
							constexpr Reg Lsb = 21;
							constexpr Reg FieldMask = 0x00200000;
							constexpr Reg Value_disabled = 0x0;
							constexpr Reg ValueMask_disabled = 0x00000000;
							constexpr Reg Value_enabled = 0x1;
							constexpr Reg ValueMask_enabled = 0x00200000;
							};
						namespace fVDDIO_HSLV { // Field Description
							constexpr Reg Lsb = 29;
							constexpr Reg FieldMask = 0x20000000;
							constexpr Reg Value_disabled = 0x0;
							constexpr Reg ValueMask_disabled = 0x00000000;
							constexpr Reg Value_enabled = 0x1;
							constexpr Reg ValueMask_enabled = 0x20000000;
							};
						namespace fOPTCHANGEERR { // Field Description
							constexpr Reg Lsb = 30;
							constexpr Reg FieldMask = 0x40000000;
							constexpr Reg Value_noError = 0x0;
							constexpr Reg ValueMask_noError = 0x00000000;
							constexpr Reg Value_error = 0x1;
							constexpr Reg ValueMask_error = 0x40000000;
							};
						namespace fSWAP_BANK_OPT { // Field Description
							constexpr Reg Lsb = 31;
							constexpr Reg FieldMask = 0x80000000;
							constexpr Reg Value_notSwapped = 0x0;
							constexpr Reg ValueMask_notSwapped = 0x00000000;
							constexpr Reg Value_swapped = 0x1;
							constexpr Reg ValueMask_swapped = 0x80000000;
							};
						};
					namespace rOPTSR_PRG { // Register description
						typedef uint32_t	Reg;
						namespace fBOR_LEV { // Field Description
							constexpr Reg Lsb = 2;
							constexpr Reg FieldMask = 0x0000000C;
							constexpr Reg Value_off = 0x0;
							constexpr Reg ValueMask_off = 0x00000000;
							constexpr Reg Value_v2_1V = 0x1;
							constexpr Reg ValueMask_v2_1V = 0x00000004;
							constexpr Reg Value_v2_4V = 0x2;
							constexpr Reg ValueMask_v2_4V = 0x00000008;
							constexpr Reg Value_v2_7V = 0x3;
							constexpr Reg ValueMask_v2_7V = 0x0000000C;
							};
						namespace fIWDG_SW { // Field Description
							constexpr Reg Lsb = 4;
							constexpr Reg FieldMask = 0x00000010;
							constexpr Reg Value_hardware = 0x0;
							constexpr Reg ValueMask_hardware = 0x00000000;
							constexpr Reg Value_software = 0x1;
							constexpr Reg ValueMask_software = 0x00000010;
							};
						namespace fNRST_STOP { // Field Description
							constexpr Reg Lsb = 6;
							constexpr Reg FieldMask = 0x00000040;
							constexpr Reg Value_hardware = 0x0;
							constexpr Reg ValueMask_hardware = 0x00000000;
							constexpr Reg Value_software = 0x1;
							constexpr Reg ValueMask_software = 0x00000040;
							};
						namespace fNRST_STDY { // Field Description
							constexpr Reg Lsb = 7;
							constexpr Reg FieldMask = 0x00000080;
							constexpr Reg Value_reset = 0x0;
							constexpr Reg ValueMask_reset = 0x00000000;
							constexpr Reg Value_noReset = 0x1;
							constexpr Reg ValueMask_noReset = 0x00000080;
							};
						namespace fRDP { // Field Description
							constexpr Reg Lsb = 8;
							constexpr Reg FieldMask = 0x0000FF00;
							constexpr Reg Value_level0 = 0xAA;
							constexpr Reg ValueMask_level0 = 0x0000AA00;
							constexpr Reg Value_level2 = 0xCC;
							constexpr Reg ValueMask_level2 = 0x0000CC00;
							constexpr Reg Value_level1 = 0x1;
							constexpr Reg ValueMask_level1 = 0x00000100;
							};
						namespace fVDDMMC_HSLV { // Field Description
							constexpr Reg Lsb = 16;
							constexpr Reg FieldMask = 0x00010000;
							constexpr Reg Value_disable = 0x0;
							constexpr Reg ValueMask_disable = 0x00000000;
							constexpr Reg Value_enable = 0x1;
							constexpr Reg ValueMask_enable = 0x00010000;
							};
						namespace fWDG_FZ_STOP { // Field Description
							constexpr Reg Lsb = 17;
							constexpr Reg FieldMask = 0x00020000;
							constexpr Reg Value_frozen = 0x0;
							constexpr Reg ValueMask_frozen = 0x00000000;
							constexpr Reg Value_running = 0x1;
							constexpr Reg ValueMask_running = 0x00020000;
							};
						namespace fIWDG_FZ_SDBY { // Field Description
							constexpr Reg Lsb = 18;
							constexpr Reg FieldMask = 0x00040000;
							constexpr Reg Value_frozen = 0x0;
							constexpr Reg ValueMask_frozen = 0x00000000;
							constexpr Reg Value_running = 0x1;
							constexpr Reg ValueMask_running = 0x00040000;
							};
						namespace fST_RAM_SIZE { // Field Description
							constexpr Reg Lsb = 19;
							constexpr Reg FieldMask = 0x00180000;
							constexpr Reg Value_v2KB = 0x0;
							constexpr Reg ValueMask_v2KB = 0x00000000;
							constexpr Reg Value_v4KB = 0x1;
							constexpr Reg ValueMask_v4KB = 0x00080000;
							constexpr Reg Value_v8KB = 0x2;
							constexpr Reg ValueMask_v8KB = 0x00100000;
							constexpr Reg Value_v16KB = 0x3;
							constexpr Reg ValueMask_v16KB = 0x00180000;
							};
						namespace fSECURITY { // Field Description
							constexpr Reg Lsb = 21;
							constexpr Reg FieldMask = 0x00200000;
							constexpr Reg Value_disable = 0x0;
							constexpr Reg ValueMask_disable = 0x00000000;
							constexpr Reg Value_enable = 0x1;
							constexpr Reg ValueMask_enable = 0x00200000;
							};
						namespace fVDDIO_HSLV { // Field Description
							constexpr Reg Lsb = 29;
							constexpr Reg FieldMask = 0x20000000;
							constexpr Reg Value_disable = 0x0;
							constexpr Reg ValueMask_disable = 0x00000000;
							constexpr Reg Value_enable = 0x1;
							constexpr Reg ValueMask_enable = 0x20000000;
							};
						namespace fSWAP_BANK_OPT { // Field Description
							constexpr Reg Lsb = 31;
							constexpr Reg FieldMask = 0x80000000;
							constexpr Reg Value_notSwapped = 0x0;
							constexpr Reg ValueMask_notSwapped = 0x00000000;
							constexpr Reg Value_swapped = 0x1;
							constexpr Reg ValueMask_swapped = 0x80000000;
							};
						};
					namespace rOPTCCR { // Register description
						typedef uint32_t	Reg;
						constexpr Reg	valueAtReset = 0x00000000;
						namespace fCLR_OPTCHANGEERR { // Field Description
							constexpr Reg Lsb = 30;
							constexpr Reg FieldMask = 0x40000000;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_noAction = 0x0;
							constexpr Reg ValueMask_noAction = 0x00000000;
							constexpr Reg Value_clear = 0x1;
							constexpr Reg ValueMask_clear = 0x40000000;
							};
						};
					namespace rPRAR_CUR { // Register description
						typedef uint32_t	Reg;
						namespace fPROT_AREA_START { // Field Description
							constexpr Reg Lsb = 0;
							constexpr Reg FieldMask = 0x00000FFF;
							constexpr Reg Value_minValue = 0x0;
							constexpr Reg ValueMask_minValue = 0x00000000;
							constexpr Reg Value_maxValue = 0xFFF;
							constexpr Reg ValueMask_maxValue = 0x00000FFF;
							};
						namespace fPROT_AREA_END { // Field Description
							constexpr Reg Lsb = 16;
							constexpr Reg FieldMask = 0x0FFF0000;
							constexpr Reg Value_minValue = 0x0;
							constexpr Reg ValueMask_minValue = 0x00000000;
							constexpr Reg Value_maxValue = 0xFFF;
							constexpr Reg ValueMask_maxValue = 0x0FFF0000;
							};
						namespace fDMEP { // Field Description
							constexpr Reg Lsb = 31;
							constexpr Reg FieldMask = 0x80000000;
							constexpr Reg Value_noAction = 0x0;
							constexpr Reg ValueMask_noAction = 0x00000000;
							constexpr Reg Value_erase = 0x1;
							constexpr Reg ValueMask_erase = 0x80000000;
							};
						};
					namespace rPRAR_PRG { // Register description
						typedef uint32_t	Reg;
						namespace fPROT_AREA_START { // Field Description
							constexpr Reg Lsb = 0;
							constexpr Reg FieldMask = 0x00000FFF;
							constexpr Reg Value_minValue = 0x0;
							constexpr Reg ValueMask_minValue = 0x00000000;
							constexpr Reg Value_maxValue = 0xFFF;
							constexpr Reg ValueMask_maxValue = 0x00000FFF;
							};
						namespace fPROT_AREA_END { // Field Description
							constexpr Reg Lsb = 16;
							constexpr Reg FieldMask = 0x0FFF0000;
							constexpr Reg Value_minValue = 0x0;
							constexpr Reg ValueMask_minValue = 0x00000000;
							constexpr Reg Value_maxValue = 0xFFF;
							constexpr Reg ValueMask_maxValue = 0x0FFF0000;
							};
						namespace fDMEP { // Field Description
							constexpr Reg Lsb = 31;
							constexpr Reg FieldMask = 0x80000000;
							constexpr Reg Value_noAction = 0x0;
							constexpr Reg ValueMask_noAction = 0x00000000;
							constexpr Reg Value_erase = 0x1;
							constexpr Reg ValueMask_erase = 0x80000000;
							};
						};
					namespace rSCAR_CUR { // Register description
						typedef uint32_t	Reg;
						namespace fSEC_AREA_START { // Field Description
							constexpr Reg Lsb = 0;
							constexpr Reg FieldMask = 0x00000FFF;
							constexpr Reg Value_minValue = 0x0;
							constexpr Reg ValueMask_minValue = 0x00000000;
							constexpr Reg Value_maxValue = 0xFFF;
							constexpr Reg ValueMask_maxValue = 0x00000FFF;
							};
						namespace fSEC_AREA_END { // Field Description
							constexpr Reg Lsb = 16;
							constexpr Reg FieldMask = 0x0FFF0000;
							constexpr Reg Value_minValue = 0x0;
							constexpr Reg ValueMask_minValue = 0x00000000;
							constexpr Reg Value_maxValue = 0xFFF;
							constexpr Reg ValueMask_maxValue = 0x0FFF0000;
							};
						namespace fDMES { // Field Description
							constexpr Reg Lsb = 31;
							constexpr Reg FieldMask = 0x80000000;
							constexpr Reg Value_noAction = 0x0;
							constexpr Reg ValueMask_noAction = 0x00000000;
							constexpr Reg Value_erase = 0x1;
							constexpr Reg ValueMask_erase = 0x80000000;
							};
						};
					namespace rSCAR_PRG { // Register description
						typedef uint32_t	Reg;
						namespace fSEC_AREA_START { // Field Description
							constexpr Reg Lsb = 0;
							constexpr Reg FieldMask = 0x00000FFF;
							constexpr Reg Value_minValue = 0x0;
							constexpr Reg ValueMask_minValue = 0x00000000;
							constexpr Reg Value_maxValue = 0xFFF;
							constexpr Reg ValueMask_maxValue = 0x00000FFF;
							};
						namespace fSEC_AREA_END { // Field Description
							constexpr Reg Lsb = 16;
							constexpr Reg FieldMask = 0x0FFF0000;
							constexpr Reg Value_minValue = 0x0;
							constexpr Reg ValueMask_minValue = 0x00000000;
							constexpr Reg Value_maxValue = 0xFFF;
							constexpr Reg ValueMask_maxValue = 0x0FFF0000;
							};
						namespace fDMES { // Field Description
							constexpr Reg Lsb = 31;
							constexpr Reg FieldMask = 0x80000000;
							constexpr Reg Value_noAction = 0x0;
							constexpr Reg ValueMask_noAction = 0x00000000;
							constexpr Reg Value_erase = 0x1;
							constexpr Reg ValueMask_erase = 0x80000000;
							};
						};
					namespace rWPSGN_CUR { // Register description
						typedef uint32_t	Reg;
						namespace fWRPSGn1 { // Field Description
							constexpr Reg Lsb = 0;
							constexpr Reg FieldMask = 0xFFFFFFFF;
							};
						};
					namespace rWPSGN_PRG { // Register description
						typedef uint32_t	Reg;
						namespace fWRPSGn1 { // Field Description
							constexpr Reg Lsb = 0;
							constexpr Reg FieldMask = 0xFFFFFFFF;
							};
						};
					namespace rBOOT_CURR { // Register description
						typedef uint32_t	Reg;
						namespace fBOOT_ADD0 { // Field Description
							constexpr Reg Lsb = 0;
							constexpr Reg FieldMask = 0x0000FFFF;
							};
						namespace fBOOT_ADD1 { // Field Description
							constexpr Reg Lsb = 16;
							constexpr Reg FieldMask = 0xFFFF0000;
							};
						};
					namespace rBOOT_PRGR { // Register description
						typedef uint32_t	Reg;
						namespace fBOOT_ADD0 { // Field Description
							constexpr Reg Lsb = 0;
							constexpr Reg FieldMask = 0x0000FFFF;
							};
						namespace fBOOT_ADD1 { // Field Description
							constexpr Reg Lsb = 16;
							constexpr Reg FieldMask = 0xFFFF0000;
							};
						};
					namespace rCRCCR { // Register description
						typedef uint32_t	Reg;
						constexpr Reg	valueAtReset = 0x001C0000;
						namespace fCRC_SECT { // Field Description
							constexpr Reg Lsb = 0;
							constexpr Reg FieldMask = 0x0000007F;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							};
						namespace fCRC_BY_SECT { // Field Description
							constexpr Reg Lsb = 8;
							constexpr Reg FieldMask = 0x00000100;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_crcOnCRCSADD = 0x0;
							constexpr Reg ValueMask_crcOnCRCSADD = 0x00000000;
							constexpr Reg Value_crcOnCRC_SECT = 0x1;
							constexpr Reg ValueMask_crcOnCRC_SECT = 0x00000100;
							};
						namespace fADD_SECT { // Field Description
							constexpr Reg Lsb = 9;
							constexpr Reg FieldMask = 0x00000200;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_noAction = 0x0;
							constexpr Reg ValueMask_noAction = 0x00000000;
							constexpr Reg Value_addSector = 0x1;
							constexpr Reg ValueMask_addSector = 0x00000200;
							};
						namespace fCLEAN_SECT { // Field Description
							constexpr Reg Lsb = 10;
							constexpr Reg FieldMask = 0x00000400;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_noAction = 0x0;
							constexpr Reg ValueMask_noAction = 0x00000000;
							constexpr Reg Value_clearList = 0x1;
							constexpr Reg ValueMask_clearList = 0x00000400;
							};
						namespace fSTART_CRC { // Field Description
							constexpr Reg Lsb = 16;
							constexpr Reg FieldMask = 0x00010000;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_noAction = 0x0;
							constexpr Reg ValueMask_noAction = 0x00000000;
							constexpr Reg Value_start = 0x1;
							constexpr Reg ValueMask_start = 0x00010000;
							};
						namespace fCLEAN_CRC { // Field Description
							constexpr Reg Lsb = 17;
							constexpr Reg FieldMask = 0x00020000;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_noAction = 0x0;
							constexpr Reg ValueMask_noAction = 0x00000000;
							constexpr Reg Value_clearCRCDATAR = 0x1;
							constexpr Reg ValueMask_clearCRCDATAR = 0x00020000;
							};
						namespace fCRC_BURST { // Field Description
							constexpr Reg Lsb = 20;
							constexpr Reg FieldMask = 0x00300000;
							constexpr Reg Value_valueAtReset = 0x1;
							constexpr Reg ValueMask_valueAtReset = 0x00100000;
							constexpr Reg Value_v4FlashWords = 0x0;
							constexpr Reg ValueMask_v4FlashWords = 0x00000000;
							constexpr Reg Value_v16FlashWords = 0x1;
							constexpr Reg ValueMask_v16FlashWords = 0x00100000;
							constexpr Reg Value_v64FlashWords = 0x2;
							constexpr Reg ValueMask_v64FlashWords = 0x00200000;
							constexpr Reg Value_v256FlashWords = 0x3;
							constexpr Reg ValueMask_v256FlashWords = 0x00300000;
							};
						namespace fALL_BANK { // Field Description
							constexpr Reg Lsb = 22;
							constexpr Reg FieldMask = 0x00400000;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_noAction = 0x0;
							constexpr Reg ValueMask_noAction = 0x00000000;
							constexpr Reg Value_addAllUSerSectors = 0x1;
							constexpr Reg ValueMask_addAllUSerSectors = 0x00400000;
							};
						};
					namespace rCRCSADD { // Register description
						typedef uint32_t	Reg;
						constexpr Reg	valueAtReset = 0x00000000;
						namespace fCRC_START_ADDR { // Field Description
							constexpr Reg Lsb = 2;
							constexpr Reg FieldMask = 0x000FFFFC;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							};
						};
					namespace rCRCEADD { // Register description
						typedef uint32_t	Reg;
						constexpr Reg	valueAtReset = 0x00000000;
						namespace fCRC_END_ADDR { // Field Description
							constexpr Reg Lsb = 2;
							constexpr Reg FieldMask = 0x000FFFFC;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							};
						};
					namespace rCRCDATAR { // Register description
						typedef uint32_t	Reg;
						constexpr Reg	valueAtReset = 0x00000000;
						namespace fCRC_DATA { // Field Description
							constexpr Reg Lsb = 0;
							constexpr Reg FieldMask = 0xFFFFFFFF;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							};
						};
					namespace rECC_FA { // Register description
						typedef uint32_t	Reg;
						constexpr Reg	valueAtReset = 0x00000000;
						namespace fFAIL_ECC_ADDR { // Field Description
							constexpr Reg Lsb = 0;
							constexpr Reg FieldMask = 0x0000FFFF;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							};
						namespace fOTP_FAIL_ECC { // Field Description
							constexpr Reg Lsb = 31;
							constexpr Reg FieldMask = 0x80000000;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_noError = 0x0;
							constexpr Reg ValueMask_noError = 0x00000000;
							constexpr Reg Value_error = 0x1;
							constexpr Reg ValueMask_error = 0x80000000;
							};
						};
					namespace rOTPBL_CUR { // Register description
						typedef uint32_t	Reg;
						constexpr Reg	notLocked = 0x00000000;
						constexpr Reg	locked = 0x00000001;
						namespace fLOCKBL { // Field Description
							constexpr Reg Lsb = 0;
							constexpr Reg FieldMask = 0x0000FFFF;
							};
						};
					namespace rOTPBL_PRG { // Register description
						typedef uint32_t	Reg;
						constexpr Reg	noAction = 0x00000000;
						constexpr Reg	lock = 0x00000001;
						namespace fLOCKBL { // Field Description
							constexpr Reg Lsb = 0;
							constexpr Reg FieldMask = 0x0000FFFF;
							};
						};
					}
				}
			}
		}
	}
#endif
