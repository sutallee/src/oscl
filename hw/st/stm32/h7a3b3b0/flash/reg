module oscl_hw_st_stm32_h7a3b3b0_flash_regh {
sysinclude "stdint.h";
namespace Oscl {
namespace St {
namespace Stm32 {
namespace H7A3B3B0 {
namespace pFLASH {
	register rACR uint32_t {
		/* mapped to offset 0x000 and 0x100 */
		value valueAtReset	0x00000013
		field fLATENCY 0 4 {
			value valueAtReset		0x3
			value zeroWaitStates	0x0
			value oneWaitState		0x1
			value twoWaitStates		0x2
			value threeWaitStates	0x3
			value fourWaitStates	0x4
			value fiveWaitStates	0x5
			value sixWaitStates		0x6
			value sevenWaitStates	0x7
			}
		field fWRHIGHFREQ 4 2 {
			value valueAtReset	0x1
			value zero			0x0
			value one			0x1
			value two			0x2
			value three			0x3
			}
		}
	register rKEYR uint32_t {
		/* write-only */
		value valueAtReset	0x00000000
		field fKEYR 0 32 {
			value valueAtReset	0x00000000
			value firstKey		0x45670123
			value secondKey		0xCDEF89AB
			}
		}
	register rOPTKEYR uint32_t {
		/* mapped to offset 0x008 and 0x108 */
		/* write-only */
		value valueAtReset	0x00000000
		field fOPTKEYR 0 32 {
			value valueAtReset	0x00000000
			value firstKey		0x08192A3B
			value secondKey		0x4C5D6E7F
			}
		}
	register rCR uint32_t {
		value valueAtReset	0x00000001
		field fLOCK 0 1 {
			value valueAtReset	1
			value unlock	0
			value unlocked	0
			value lock		1
			value locked	1
			}
		field fPG 1 1 {
			value valueAtReset	0
			value disable	0
			value disabled	0
			value enable	1
			value enabled	1
			}
		field fSER 2 1 {
			value valueAtReset	0
			value noAction	0
			value erase		1
			}
		field fBER 3 1 {
			value valueAtReset	0
			value noAction	0
			value erase		1
			}
		field fFW 4 1 {
			value valueAtReset	0
			value noAction	0
			value force		1
			}
		field fSTART 5 1 {
			value valueAtReset	0
			value noAction	0
			value start		1
			}
		field fSSN 6 7 {
			value valueAtReset	0x00
			value minValue	0x00
			value maxValue	0x7F
			}
		field fCRC_EN 15 1 {
			value valueAtReset	0
			value disable	0
			value disabled	0
			value enable	1
			value enabled	1
			}
		field fEOPIE 16 1 {
			value valueAtReset	0
			value disable	0
			value disabled	0
			value enable	1
			value enabled	1
			}
		field fWRPERRIE 17 1 {
			value valueAtReset	0
			value disable	0
			value disabled	0
			value enable	1
			value enabled	1
			}
		field fPGSERRIE 18 1 {
			value valueAtReset	0
			value disable	0
			value disabled	0
			value enable	1
			value enabled	1
			}
		field fSTRBERRIE 19 1 {
			value valueAtReset	0
			value disable	0
			value disabled	0
			value enable	1
			value enabled	1
			}
		field fINCERRIE 21 1 {
			value valueAtReset	0
			value disable	0
			value disabled	0
			value enable	1
			value enabled	1
			}
		field fRDPERRIE 23 1 {
			value valueAtReset	0
			value disable	0
			value disabled	0
			value enable	1
			value enabled	1
			}
		field fRDSERRIE 24 1 {
			value valueAtReset	0
			value disable	0
			value disabled	0
			value enable	1
			value enabled	1
			}
		field fSNECCERRIE 25 1 {
			value valueAtReset	0
			value disable	0
			value disabled	0
			value enable	1
			value enabled	1
			}
		field fDBECCERRIE 26 1 {
			value valueAtReset	0
			value disable	0
			value disabled	0
			value enable	1
			value enabled	1
			}
		field fCRCENDIE 27 1 {
			value valueAtReset	0
			value disable	0
			value disabled	0
			value enable	1
			value enabled	1
			}
		field fCRCRDERRIE 28 1 {
			value valueAtReset	0
			value disable	0
			value disabled	0
			value enable	1
			value enabled	1
			}
		}
	register rSR uint32_t {
		/* read-only */
		value valueAtReset	0x00000000
		field fBSY 0 1 {
			value valueAtReset	0
			value notBusy	0
			value busy		1
			}
		field fWBNE 1 1 {
			value valueAtReset	0
			value emptied	0
			value notEmpty	1
			}
		field fQW 2 1 {
			value valueAtReset	0
			value notPending	0
			value pending		1
			}
		field fCRC_BUSY 3 1 {
			value valueAtReset	0
			value notBusy	0
			value busy		1
			}
		field fEOP 16 1 {
			value valueAtReset	0
			value complete		1
			value completed		1
			}
		field fWRPERR 17 1 {
			value valueAtReset	0
			value noError		0
			value error			1
			}
		field fPGSERR 18 1 {
			value valueAtReset	0
			value noError		0
			value error			1
			}
		field fSTRBERR 19 1 {
			value valueAtReset	0
			value noError		0
			value error			1
			}
		field fINCERR 21 1 {
			value valueAtReset	0
			value noError		0
			value error			1
			}
		field fRDPERR 23 1 {
			value valueAtReset	0
			value noError		0
			value error			1
			}
		field fRDSERR 24 1 {
			value valueAtReset	0
			value noError		0
			value error			1
			}
		field fSNECCERR 25 1 {
			value valueAtReset	0
			value noError		0
			value error			1
			}
		field fDBECCERR 26 1 {
			value valueAtReset	0
			value noError		0
			value error			1
			}
		field fCRCEND 27 1 {
			value valueAtReset	0
			value complete		1
			value completed		1
			}
		field fCRCRDERR 28 1 {
			value valueAtReset	0
			value noError		0
			value error			1
			}
		}
	register rCCR uint32_t {
		/* write-only */
		value valueAtReset	0x00000000
		field fCLR_EOP 16 1 {
			value valueAtReset	0
			value noAction		0
			value clear			1
			}
		field fCLR_WRPERR 17 1 {
			value valueAtReset	0
			value noAction		0
			value clear			1
			}
		field fCLR_PGSERR 18 1 {
			value valueAtReset	0
			value noAction		0
			value clear			1
			}
		field fCLR_STRBERR 19 1 {
			value valueAtReset	0
			value noAction		0
			value clear			1
			}
		field fCLR_INCERR 21 1 {
			value valueAtReset	0
			value noAction		0
			value clear			1
			}
		field fCLR_RDPERR 23 1 {
			value valueAtReset	0
			value noAction		0
			value clear			1
			}
		field fCLR_RDSERR 24 1 {
			value valueAtReset	0
			value noAction		0
			value clear			1
			}
		field fCLR_SNECCERR 25 1 {
			value valueAtReset	0
			value noAction		0
			value clear			1
			}
		field fCLR_DBECCERR 26 1 {
			value valueAtReset	0
			value noAction		0
			value clear			1
			}
		field fCLR_CRCEND 27 1 {
			value valueAtReset	0
			value noAction		0
			value clear			1
			}
		field fCLR_CRCRDERR 28 1 {
			value valueAtReset	0
			value noAction		0
			value clear			1
			}
		}
	register rOPTCR uint32_t {
		/* mapped to offset 0x018 and 0x118 */
		value valueAtReset	0x00000001
		field fOPTLOCK 0 1 {
			/* rs */
			value valueAtReset	0
			value unlock	0
			value unlocked	0
			value lock		1
			value locked	1
			}
		field fOPTSTART 1 1 {
			/* write-only */
			value valueAtReset	0
			value noAction	0
			value start		1
			value started	1
			}
		field fMER 4 1 {
			/* write-only */
			value valueAtReset	0
			value noAction	0
			value erase		1
			}
		field fPG_OTP 5 1 {
			/* read-write */
			value valueAtReset	0
			value noAction	0
			value program	1
			}
		field fOPTCHANGEERRIE 30 1 {
			/* read-write */
			value valueAtReset	0
			value disable	0
			value disabled	0
			value enable	1
			value enabled	1
			}
		field fSWAP_BANK 31 1 {
			/* read-only */
			value valueAtReset	1
			value notSwapped	0
			value swapped		1
			}
		}
	register rOPTSR_CUR uint32_t {
		/* mapped to offset 0x01C and 0x11C */
		/* read-only */
		field fOPT_BUSY 0 1 {
			value notBusy	0
			value busy		1
			}
		field fBOR_LEV 2 2 {
			value off		0x0
			value v2_1V		0x1
			value v2_4V		0x2
			value v2_7V		0x3
			}
		field fIWDG_SW 4 1 {
			value hardware	0
			value software	1
			}
		field fNRST_STOP 6 1 {
			value reset		0
			value noReset	1
			}
		field fNRST_STDY 7 1 {
			value reset		0
			value noReset	1
			}
		field fRDP 8 8 {
			/* level 1 for all values except level 0 & 2 */
			value level0	0xAA
			value level2	0xCC
			value level1	0x01
			}
		field fVDDMMC_HSLV 16 1 {
			value disabled	0
			value enabled	1
			}
		field fWDG_FZ_STOP 17 1 {
			value frozen	0
			value running	1
			}
		field fIWDG_FZ_SDBY 18 1 {
			value frozen	0
			value running	1
			}
		field fST_RAM_SIZE 19 2 {
			value v2KB		0x0
			value v4KB		0x1
			value v8KB		0x2
			value v16KB		0x3
			}
		field fSECURITY 21 1 {
			value disabled	0
			value enabled	1
			}
		field fVDDIO_HSLV 29 1 {
			value disabled	0
			value enabled	1
			}
		field fOPTCHANGEERR 30 1 {
			value noError	0
			value error		1
			}
		field fSWAP_BANK_OPT 31 1 {
			value notSwapped	0
			value swapped		1
			}
		}
	register rOPTSR_PRG uint32_t {
		/* mapped to offset 0x020 and 0x120 */
		field fBOR_LEV 2 2 {
			value off		0x0
			value v2_1V		0x1
			value v2_4V		0x2
			value v2_7V		0x3
			}
		field fIWDG_SW 4 1 {
			value hardware	0
			value software	1
			}
		field fNRST_STOP 6 1 {
			value hardware	0
			value software	1
			}
		field fNRST_STDY 7 1 {
			value reset		0
			value noReset	1
			}
		field fRDP 8 8 {
			/* level 1 for all values except level 0 & 2 */
			value level0	0xAA
			value level2	0xCC
			value level1	0x01
			}
		field fVDDMMC_HSLV 16 1 {
			value disable	0
			value enable	1
			}
		field fWDG_FZ_STOP 17 1 {
			value frozen	0
			value running	1
			}
		field fIWDG_FZ_SDBY 18 1 {
			value frozen	0
			value running	1
			}
		field fST_RAM_SIZE 19 2 {
			value v2KB		0x0
			value v4KB		0x1
			value v8KB		0x2
			value v16KB		0x3
			}
		field fSECURITY 21 1 {
			value disable	0
			value enable	1
			}
		field fVDDIO_HSLV 29 1 {
			value disable	0
			value enable	1
			}
		field fSWAP_BANK_OPT 31 1 {
			value notSwapped	0
			value swapped		1
			}
		}
	register rOPTCCR uint32_t {
		/* mapped to offset 0x024 and 0x124 */
		/* write-only */
		value valueAtReset	0x00000000
		field fCLR_OPTCHANGEERR 30 1 {
			value valueAtReset	0
			value noAction	0
			value clear		1
			}
		}
	register rPRAR_CUR uint32_t {
		/* read-only */
		field fPROT_AREA_START 0 12 {
			value minValue	0x000
			value maxValue	0xFFF
			}
		field fPROT_AREA_END 16 12 {
			value minValue	0x000
			value maxValue	0xFFF
			}
		field fDMEP 31 1 {
			value noAction	0
			value erase		1
			}
		}
	register rPRAR_PRG uint32_t {
		/* read-write */
		field fPROT_AREA_START 0 12 {
			value minValue	0x000
			value maxValue	0xFFF
			}
		field fPROT_AREA_END 16 12 {
			value minValue	0x000
			value maxValue	0xFFF
			}
		field fDMEP 31 1 {
			value noAction	0
			value erase		1
			}
		}
	register rSCAR_CUR uint32_t {
		/* read-only */
		field fSEC_AREA_START 0 12 {
			value minValue	0x000
			value maxValue	0xFFF
			}
		field fSEC_AREA_END 16 12 {
			value minValue	0x000
			value maxValue	0xFFF
			}
		field fDMES 31 1 {
			value noAction	0
			value erase		1
			}
		}
	register rSCAR_PRG uint32_t {
		/* read-write */
		field fSEC_AREA_START 0 12 {
			value minValue	0x000
			value maxValue	0xFFF
			}
		field fSEC_AREA_END 16 12 {
			value minValue	0x000
			value maxValue	0xFFF
			}
		field fDMES 31 1 {
			value noAction	0
			value erase		1
			}
		}
	register rWPSGN_CUR uint32_t {
		/* read-only */
		field fWRPSGn1 0 32 {
			}
		}
	register rWPSGN_PRG uint32_t {
		/* read-write */
		field fWRPSGn1 0 32 {
			}
		}
	register rBOOT_CURR uint32_t {
		/* mapped to offset 0x040 and 0x140 */
		/* read-only */
		field fBOOT_ADD0 0 16 {
			}
		field fBOOT_ADD1 16 16 {
			}
		}
	register rBOOT_PRGR uint32_t {
		/* mapped to offset 0x044 and 0x144 */
		/* read-write */
		field fBOOT_ADD0 0 16 {
			}
		field fBOOT_ADD1 16 16 {
			}
		}
	register rCRCCR uint32_t {
		value valueAtReset	0x001C0000
		field fCRC_SECT 0 7 {
			value valueAtReset	0x00
			}
		field fCRC_BY_SECT 8 1 {
			value valueAtReset	0
			value crcOnCRCSADD	0
			value crcOnCRC_SECT	1
			}
		field fADD_SECT 9 1 {
			/* write-only */
			value valueAtReset	0
			value noAction	0
			value addSector	1
			}
		field fCLEAN_SECT 10 1 {
			/* write-only */
			value valueAtReset	0
			value noAction	0
			value clearList	1
			}
		field fSTART_CRC 16 1 {
			value valueAtReset	0
			value noAction	0
			value start		1
			}
		field fCLEAN_CRC 17 1 {
			/* write-only */
			value valueAtReset	0
			value noAction		0
			value clearCRCDATAR	1
			}
		field fCRC_BURST 20 2 {
			value valueAtReset		0x1
			value v4FlashWords		0x0
			value v16FlashWords		0x1
			value v64FlashWords		0x2
			value v256FlashWords	0x3
			}
		field fALL_BANK 22 1 {
			/* write-only */
			value valueAtReset		0
			value noAction			0
			value addAllUSerSectors	1
			}
		}
	register rCRCSADD uint32_t {
		value valueAtReset	0x00000000
		field fCRC_START_ADDR 2 18 {
			value valueAtReset	0x00000
			}
		}
	register rCRCEADD uint32_t {
		value valueAtReset	0x00000000
		field fCRC_END_ADDR 2 18 {
			value valueAtReset	0x00000
			}
		}
	register rCRCDATAR uint32_t {
		/* mapped to offset 0x05C and 0x15C */
		/* read-only */
		value valueAtReset	0x00000000
		field fCRC_DATA 0 32 {
			value valueAtReset	0x00000000
			}
		}
	register rECC_FA uint32_t {
		/* read-only */
		value valueAtReset	0x00000000
		field fFAIL_ECC_ADDR 0 16 {
			value valueAtReset	0x0000
			}
		field fOTP_FAIL_ECC 31 1 {
			value valueAtReset	0
			value noError	0
			value error		1
			}
		}
	register rOTPBL_CUR uint32_t {
		/* read-only */
		value notLocked	0
		value locked	1
		field fLOCKBL 0 16 {
			}
		}
	register rOTPBL_PRG uint32_t {
		value noAction	0
		value lock		1
		field fLOCKBL 0 16 {
			}
		}
}
}
}
}
}
}
