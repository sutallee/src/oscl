/*
   Copyright (C) 2024 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_hw_st_stm32_h7a3b3b0_tim_maph_
#define _oscl_hw_st_stm32_h7a3b3b0_tim_maph_
#include "reg.h"
namespace Oscl {
namespace St {
namespace Stm32 {
namespace H7A3B3B0 {
namespace pTIM {
struct Map {
	/** offset:0x0 */
	volatile Oscl::St::Stm32::H7A3B3B0::pTIM::rCR1::Reg	cr1;
	/** offset:0x4 */
	volatile Oscl::St::Stm32::H7A3B3B0::pTIM::rCR2::Reg	cr2;
	/** offset:0x8 */
	volatile Oscl::St::Stm32::H7A3B3B0::pTIM::rSMCR::Reg	smcr;
	/** offset:0xc */
	volatile Oscl::St::Stm32::H7A3B3B0::pTIM::rDIER::Reg	dier;
	/** offset:0x10 */
	volatile Oscl::St::Stm32::H7A3B3B0::pTIM::rSR::Reg	sr;
	/** offset:0x14 */
	volatile Oscl::St::Stm32::H7A3B3B0::pTIM::rEGR::Reg	egr;
	/** offset:0x18 */
	union {
		volatile Oscl::St::Stm32::H7A3B3B0::pTIM::rCCMR_Input::Reg	input;
		volatile Oscl::St::Stm32::H7A3B3B0::pTIM::rCCMR_Output::Reg	output;
		} ccmr1;
	union {
		volatile Oscl::St::Stm32::H7A3B3B0::pTIM::rCCMR_Input::Reg	input;
		volatile Oscl::St::Stm32::H7A3B3B0::pTIM::rCCMR_Output::Reg	output;
		} ccmr2;
	/** offset:0x20 */
	volatile Oscl::St::Stm32::H7A3B3B0::pTIM::rCCER::Reg	ccer;
	/** offset:0x24 */
	volatile Oscl::St::Stm32::H7A3B3B0::pTIM::rCNT::Reg	cnt;
	/** offset:0x28 */
	volatile Oscl::St::Stm32::H7A3B3B0::pTIM::rPSC::Reg	psc;
	/** offset:0x2c */
	volatile Oscl::St::Stm32::H7A3B3B0::pTIM::rARR::Reg	arr;
	/** offset:0x30 */
	volatile Oscl::St::Stm32::H7A3B3B0::pTIM::rRCR::Reg	rcr;
	/** offset:0x34 */
	volatile Oscl::St::Stm32::H7A3B3B0::pTIM::rCCR::Reg	ccr[4];
	/** offset:0x44 */
	volatile Oscl::St::Stm32::H7A3B3B0::pTIM::rBDTR::Reg	bdtr;
	/** offset:0x48 */
	volatile Oscl::St::Stm32::H7A3B3B0::pTIM::rDCR::Reg	dcr;
	/** offset:0x4c */
	volatile Oscl::St::Stm32::H7A3B3B0::pTIM::rDMAR::Reg	dmar;
	const uint8_t	_reserve80[4];
	/** offset:0x54 */
	union {
		volatile Oscl::St::Stm32::H7A3B3B0::pTIM::rCCMR_Output::Reg	output;
		} ccmr3;
	/** offset:0x58 */
	volatile Oscl::St::Stm32::H7A3B3B0::pTIM::rCCR5::Reg	ccr5;
	/** offset:0x5c */
	volatile Oscl::St::Stm32::H7A3B3B0::pTIM::rCRR6::Reg	crr6;
	/** offset:0x60 */
	volatile Oscl::St::Stm32::H7A3B3B0::pTIM::rAF1::Reg	af1;
	/** offset:0x64 */
	volatile Oscl::St::Stm32::H7A3B3B0::pTIM::rAF2::Reg	af2;
	/** offset:0x68 */
	volatile Oscl::St::Stm32::H7A3B3B0::pTIM::rTISEL::Reg	tisel;
	};
}
}
}
}
}
#endif
