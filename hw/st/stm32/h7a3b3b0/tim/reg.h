/*
   Copyright (C) 2024 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_hw_st_stm32_h7a3b3b0_tim_regh_
#define _oscl_hw_st_stm32_h7a3b3b0_tim_regh_
#include <stdint.h>
namespace Oscl { // Namespace description
	namespace St { // Namespace description
		namespace Stm32 { // Namespace description
			namespace H7A3B3B0 { // Namespace description
				namespace pTIM { // Namespace description
					namespace rCR1 { // Register description
						typedef uint32_t	Reg;
						constexpr Reg	valueAfterReset = 0x00000000;
						namespace fCEN { // Field Description
							constexpr Reg Lsb = 0;
							constexpr Reg FieldMask = 0x00000001;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							constexpr Reg Value_disable = 0x0;
							constexpr Reg ValueMask_disable = 0x00000000;
							constexpr Reg Value_disabled = 0x0;
							constexpr Reg ValueMask_disabled = 0x00000000;
							constexpr Reg Value_enable = 0x1;
							constexpr Reg ValueMask_enable = 0x00000001;
							constexpr Reg Value_enabled = 0x1;
							constexpr Reg ValueMask_enabled = 0x00000001;
							};
						namespace fUDIS { // Field Description
							constexpr Reg Lsb = 1;
							constexpr Reg FieldMask = 0x00000002;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							constexpr Reg Value_disable = 0x1;
							constexpr Reg ValueMask_disable = 0x00000002;
							constexpr Reg Value_disabled = 0x1;
							constexpr Reg ValueMask_disabled = 0x00000002;
							constexpr Reg Value_enable = 0x0;
							constexpr Reg ValueMask_enable = 0x00000000;
							constexpr Reg Value_enabled = 0x0;
							constexpr Reg ValueMask_enabled = 0x00000000;
							};
						namespace fURS { // Field Description
							constexpr Reg Lsb = 2;
							constexpr Reg FieldMask = 0x00000004;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							constexpr Reg Value_any = 0x0;
							constexpr Reg ValueMask_any = 0x00000000;
							constexpr Reg Value_counterOnly = 0x1;
							constexpr Reg ValueMask_counterOnly = 0x00000004;
							};
						namespace fOPM { // Field Description
							constexpr Reg Lsb = 3;
							constexpr Reg FieldMask = 0x00000008;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							constexpr Reg Value_notStopped = 0x0;
							constexpr Reg ValueMask_notStopped = 0x00000000;
							constexpr Reg Value_stopped = 0x1;
							constexpr Reg ValueMask_stopped = 0x00000008;
							};
						namespace fDIR { // Field Description
							constexpr Reg Lsb = 4;
							constexpr Reg FieldMask = 0x00000010;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							constexpr Reg Value_upCounter = 0x0;
							constexpr Reg ValueMask_upCounter = 0x00000000;
							constexpr Reg Value_downCounter = 0x1;
							constexpr Reg ValueMask_downCounter = 0x00000010;
							};
						namespace fCMS { // Field Description
							constexpr Reg Lsb = 5;
							constexpr Reg FieldMask = 0x00000060;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							constexpr Reg Value_edgeAligned = 0x0;
							constexpr Reg ValueMask_edgeAligned = 0x00000000;
							constexpr Reg Value_centerAligned1 = 0x1;
							constexpr Reg ValueMask_centerAligned1 = 0x00000020;
							constexpr Reg Value_centerAligned2 = 0x2;
							constexpr Reg ValueMask_centerAligned2 = 0x00000040;
							constexpr Reg Value_centerAligned3 = 0x3;
							constexpr Reg ValueMask_centerAligned3 = 0x00000060;
							};
						namespace fARPE { // Field Description
							constexpr Reg Lsb = 7;
							constexpr Reg FieldMask = 0x00000080;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							constexpr Reg Value_notBuffered = 0x0;
							constexpr Reg ValueMask_notBuffered = 0x00000000;
							constexpr Reg Value_buffered = 0x1;
							constexpr Reg ValueMask_buffered = 0x00000080;
							};
						namespace fCKD { // Field Description
							constexpr Reg Lsb = 8;
							constexpr Reg FieldMask = 0x00000300;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							constexpr Reg Value_tCK_INT = 0x0;
							constexpr Reg ValueMask_tCK_INT = 0x00000000;
							constexpr Reg Value_tCK_INTx2 = 0x1;
							constexpr Reg ValueMask_tCK_INTx2 = 0x00000100;
							constexpr Reg Value_tCK_INTx4 = 0x2;
							constexpr Reg ValueMask_tCK_INTx4 = 0x00000200;
							};
						namespace fUIFREMAP { // Field Description
							constexpr Reg Lsb = 11;
							constexpr Reg FieldMask = 0x00000800;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							constexpr Reg Value_disable = 0x0;
							constexpr Reg ValueMask_disable = 0x00000000;
							constexpr Reg Value_disabled = 0x0;
							constexpr Reg ValueMask_disabled = 0x00000000;
							constexpr Reg Value_enable = 0x1;
							constexpr Reg ValueMask_enable = 0x00000800;
							constexpr Reg Value_enabled = 0x1;
							constexpr Reg ValueMask_enabled = 0x00000800;
							};
						};
					namespace rCR2 { // Register description
						typedef uint32_t	Reg;
						constexpr Reg	valueAfterReset = 0x00000000;
						namespace fCCPC { // Field Description
							constexpr Reg Lsb = 0;
							constexpr Reg FieldMask = 0x00000001;
							};
						namespace fCCUS { // Field Description
							constexpr Reg Lsb = 2;
							constexpr Reg FieldMask = 0x00000004;
							};
						namespace fCCDS { // Field Description
							constexpr Reg Lsb = 3;
							constexpr Reg FieldMask = 0x00000008;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							constexpr Reg Value_onCCxEvent = 0x0;
							constexpr Reg ValueMask_onCCxEvent = 0x00000000;
							constexpr Reg Value_onUpdateEvent = 0x1;
							constexpr Reg ValueMask_onUpdateEvent = 0x00000008;
							};
						namespace fMMS { // Field Description
							constexpr Reg Lsb = 4;
							constexpr Reg FieldMask = 0x00000070;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							constexpr Reg Value_reset = 0x0;
							constexpr Reg ValueMask_reset = 0x00000000;
							constexpr Reg Value_enable = 0x1;
							constexpr Reg ValueMask_enable = 0x00000010;
							constexpr Reg Value_update = 0x2;
							constexpr Reg ValueMask_update = 0x00000020;
							constexpr Reg Value_comparePulse = 0x3;
							constexpr Reg ValueMask_comparePulse = 0x00000030;
							constexpr Reg Value_oc1refcOutput = 0x4;
							constexpr Reg ValueMask_oc1refcOutput = 0x00000040;
							constexpr Reg Value_oc2refcOutput = 0x5;
							constexpr Reg ValueMask_oc2refcOutput = 0x00000050;
							constexpr Reg Value_oc3refcOutput = 0x6;
							constexpr Reg ValueMask_oc3refcOutput = 0x00000060;
							constexpr Reg Value_oc4refcOutput = 0x7;
							constexpr Reg ValueMask_oc4refcOutput = 0x00000070;
							};
						namespace fTI1S { // Field Description
							constexpr Reg Lsb = 7;
							constexpr Reg FieldMask = 0x00000080;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							constexpr Reg Value_ch1PinToTl1Input = 0x0;
							constexpr Reg ValueMask_ch1PinToTl1Input = 0x00000000;
							constexpr Reg Value_ch123PinToTl1InputXOR = 0x1;
							constexpr Reg ValueMask_ch123PinToTl1InputXOR = 0x00000080;
							};
						namespace fOIS1 { // Field Description
							constexpr Reg Lsb = 8;
							constexpr Reg FieldMask = 0x00000100;
							};
						namespace fOIS1N { // Field Description
							constexpr Reg Lsb = 9;
							constexpr Reg FieldMask = 0x00000200;
							};
						namespace fOIS2 { // Field Description
							constexpr Reg Lsb = 10;
							constexpr Reg FieldMask = 0x00000400;
							};
						namespace fOIS2N { // Field Description
							constexpr Reg Lsb = 11;
							constexpr Reg FieldMask = 0x00000800;
							};
						namespace fOIS3 { // Field Description
							constexpr Reg Lsb = 12;
							constexpr Reg FieldMask = 0x00001000;
							};
						namespace fOIS3N { // Field Description
							constexpr Reg Lsb = 13;
							constexpr Reg FieldMask = 0x00002000;
							};
						namespace fOIS4 { // Field Description
							constexpr Reg Lsb = 14;
							constexpr Reg FieldMask = 0x00004000;
							};
						namespace fOIS5 { // Field Description
							constexpr Reg Lsb = 16;
							constexpr Reg FieldMask = 0x00010000;
							};
						namespace fOIS6 { // Field Description
							constexpr Reg Lsb = 18;
							constexpr Reg FieldMask = 0x00040000;
							};
						namespace fMMS2 { // Field Description
							constexpr Reg Lsb = 20;
							constexpr Reg FieldMask = 0x00F00000;
							};
						};
					namespace rSMCR { // Register description
						typedef uint32_t	Reg;
						constexpr Reg	valueAfterReset = 0x00000000;
						namespace fSMS { // Field Description
							constexpr Reg Lsb = 0;
							constexpr Reg FieldMask = 0x00000007;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							constexpr Reg Value_disable = 0x0;
							constexpr Reg ValueMask_disable = 0x00000000;
							constexpr Reg Value_encoderMode1 = 0x1;
							constexpr Reg ValueMask_encoderMode1 = 0x00000001;
							constexpr Reg Value_encoderMode2 = 0x2;
							constexpr Reg ValueMask_encoderMode2 = 0x00000002;
							constexpr Reg Value_encoderMode3 = 0x3;
							constexpr Reg ValueMask_encoderMode3 = 0x00000003;
							constexpr Reg Value_resetMode = 0x4;
							constexpr Reg ValueMask_resetMode = 0x00000004;
							constexpr Reg Value_gatedMode = 0x5;
							constexpr Reg ValueMask_gatedMode = 0x00000005;
							constexpr Reg Value_triggerMode = 0x6;
							constexpr Reg ValueMask_triggerMode = 0x00000006;
							constexpr Reg Value_externalClockMode = 0x7;
							constexpr Reg ValueMask_externalClockMode = 0x00000007;
							};
						namespace fTS { // Field Description
							constexpr Reg Lsb = 4;
							constexpr Reg FieldMask = 0x00000070;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							constexpr Reg Value_internalTrigger0 = 0x0;
							constexpr Reg ValueMask_internalTrigger0 = 0x00000000;
							constexpr Reg Value_internalTrigger1 = 0x1;
							constexpr Reg ValueMask_internalTrigger1 = 0x00000010;
							constexpr Reg Value_internalTrigger2 = 0x2;
							constexpr Reg ValueMask_internalTrigger2 = 0x00000020;
							constexpr Reg Value_internalTrigger3 = 0x3;
							constexpr Reg ValueMask_internalTrigger3 = 0x00000030;
							constexpr Reg Value_tl1EdgeDetector = 0x4;
							constexpr Reg ValueMask_tl1EdgeDetector = 0x00000040;
							constexpr Reg Value_filteredTimerInput1 = 0x5;
							constexpr Reg ValueMask_filteredTimerInput1 = 0x00000050;
							constexpr Reg Value_filteredTimerInput2 = 0x6;
							constexpr Reg ValueMask_filteredTimerInput2 = 0x00000060;
							constexpr Reg Value_externalTriggerInput = 0x7;
							constexpr Reg ValueMask_externalTriggerInput = 0x00000070;
							};
						namespace fMSM { // Field Description
							constexpr Reg Lsb = 7;
							constexpr Reg FieldMask = 0x00000080;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							constexpr Reg Value_noAction = 0x0;
							constexpr Reg ValueMask_noAction = 0x00000000;
							constexpr Reg Value_synchronize = 0x1;
							constexpr Reg ValueMask_synchronize = 0x00000080;
							};
						namespace fETF { // Field Description
							constexpr Reg Lsb = 8;
							constexpr Reg FieldMask = 0x00000F00;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							constexpr Reg Value_fDTS = 0x0;
							constexpr Reg ValueMask_fDTS = 0x00000000;
							constexpr Reg Value_fCK_INT_N2 = 0x1;
							constexpr Reg ValueMask_fCK_INT_N2 = 0x00000100;
							constexpr Reg Value_fCK_INT_N4 = 0x2;
							constexpr Reg ValueMask_fCK_INT_N4 = 0x00000200;
							constexpr Reg Value_fCK_INT_N8 = 0x3;
							constexpr Reg ValueMask_fCK_INT_N8 = 0x00000300;
							constexpr Reg Value_fDTS_div2_N6 = 0x4;
							constexpr Reg ValueMask_fDTS_div2_N6 = 0x00000400;
							constexpr Reg Value_fDTS_div2_N8 = 0x5;
							constexpr Reg ValueMask_fDTS_div2_N8 = 0x00000500;
							constexpr Reg Value_fDTS_div4_N6 = 0x6;
							constexpr Reg ValueMask_fDTS_div4_N6 = 0x00000600;
							constexpr Reg Value_fDTS_div4_N8 = 0x7;
							constexpr Reg ValueMask_fDTS_div4_N8 = 0x00000700;
							constexpr Reg Value_fDTS_div8_N6 = 0x8;
							constexpr Reg ValueMask_fDTS_div8_N6 = 0x00000800;
							constexpr Reg Value_fDTS_div8_N8 = 0x9;
							constexpr Reg ValueMask_fDTS_div8_N8 = 0x00000900;
							constexpr Reg Value_fDTS_div16_N5 = 0xA;
							constexpr Reg ValueMask_fDTS_div16_N5 = 0x00000A00;
							constexpr Reg Value_fDTS_div16_N6 = 0xB;
							constexpr Reg ValueMask_fDTS_div16_N6 = 0x00000B00;
							constexpr Reg Value_fDTS_div16_N8 = 0xC;
							constexpr Reg ValueMask_fDTS_div16_N8 = 0x00000C00;
							constexpr Reg Value_fDTS_div32_N5 = 0xD;
							constexpr Reg ValueMask_fDTS_div32_N5 = 0x00000D00;
							constexpr Reg Value_fDTS_div32_N6 = 0xE;
							constexpr Reg ValueMask_fDTS_div32_N6 = 0x00000E00;
							constexpr Reg Value_fDTS_div32_N8 = 0xF;
							constexpr Reg ValueMask_fDTS_div32_N8 = 0x00000F00;
							};
						namespace fETPS { // Field Description
							constexpr Reg Lsb = 12;
							constexpr Reg FieldMask = 0x00003000;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							constexpr Reg Value_divBy1 = 0x0;
							constexpr Reg ValueMask_divBy1 = 0x00000000;
							constexpr Reg Value_divBy2 = 0x1;
							constexpr Reg ValueMask_divBy2 = 0x00001000;
							constexpr Reg Value_divBy4 = 0x2;
							constexpr Reg ValueMask_divBy4 = 0x00002000;
							constexpr Reg Value_divBy8 = 0x3;
							constexpr Reg ValueMask_divBy8 = 0x00003000;
							};
						namespace fECE { // Field Description
							constexpr Reg Lsb = 14;
							constexpr Reg FieldMask = 0x00004000;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							constexpr Reg Value_disable = 0x0;
							constexpr Reg ValueMask_disable = 0x00000000;
							constexpr Reg Value_enable = 0x1;
							constexpr Reg ValueMask_enable = 0x00004000;
							};
						namespace fETP { // Field Description
							constexpr Reg Lsb = 15;
							constexpr Reg FieldMask = 0x00008000;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							constexpr Reg Value_nonInverted = 0x0;
							constexpr Reg ValueMask_nonInverted = 0x00000000;
							constexpr Reg Value_inverted = 0x1;
							constexpr Reg ValueMask_inverted = 0x00008000;
							};
						namespace fSMS_3 { // Field Description
							constexpr Reg Lsb = 16;
							constexpr Reg FieldMask = 0x00010000;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							constexpr Reg Value_noCombine = 0x0;
							constexpr Reg ValueMask_noCombine = 0x00000000;
							constexpr Reg Value_combine = 0x1;
							constexpr Reg ValueMask_combine = 0x00010000;
							};
						namespace fTS_4_3 { // Field Description
							constexpr Reg Lsb = 20;
							constexpr Reg FieldMask = 0x00300000;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							constexpr Reg Value_noInternalTrigger45678 = 0x0;
							constexpr Reg ValueMask_noInternalTrigger45678 = 0x00000000;
							constexpr Reg Value_internalTrigger45678 = 0x1;
							constexpr Reg ValueMask_internalTrigger45678 = 0x00100000;
							};
						};
					namespace rDIER { // Register description
						typedef uint32_t	Reg;
						constexpr Reg	valueAfterReset = 0x00000000;
						namespace fUIE { // Field Description
							constexpr Reg Lsb = 0;
							constexpr Reg FieldMask = 0x00000001;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							constexpr Reg Value_disable = 0x0;
							constexpr Reg ValueMask_disable = 0x00000000;
							constexpr Reg Value_disabled = 0x0;
							constexpr Reg ValueMask_disabled = 0x00000000;
							constexpr Reg Value_enable = 0x1;
							constexpr Reg ValueMask_enable = 0x00000001;
							constexpr Reg Value_enabled = 0x1;
							constexpr Reg ValueMask_enabled = 0x00000001;
							};
						namespace fCC1IE { // Field Description
							constexpr Reg Lsb = 1;
							constexpr Reg FieldMask = 0x00000002;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							constexpr Reg Value_disable = 0x0;
							constexpr Reg ValueMask_disable = 0x00000000;
							constexpr Reg Value_disabled = 0x0;
							constexpr Reg ValueMask_disabled = 0x00000000;
							constexpr Reg Value_enable = 0x1;
							constexpr Reg ValueMask_enable = 0x00000002;
							constexpr Reg Value_enabled = 0x1;
							constexpr Reg ValueMask_enabled = 0x00000002;
							};
						namespace fCC2IE { // Field Description
							constexpr Reg Lsb = 2;
							constexpr Reg FieldMask = 0x00000004;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							constexpr Reg Value_disable = 0x0;
							constexpr Reg ValueMask_disable = 0x00000000;
							constexpr Reg Value_disabled = 0x0;
							constexpr Reg ValueMask_disabled = 0x00000000;
							constexpr Reg Value_enable = 0x1;
							constexpr Reg ValueMask_enable = 0x00000004;
							constexpr Reg Value_enabled = 0x1;
							constexpr Reg ValueMask_enabled = 0x00000004;
							};
						namespace fCC3IE { // Field Description
							constexpr Reg Lsb = 3;
							constexpr Reg FieldMask = 0x00000008;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							constexpr Reg Value_disable = 0x0;
							constexpr Reg ValueMask_disable = 0x00000000;
							constexpr Reg Value_disabled = 0x0;
							constexpr Reg ValueMask_disabled = 0x00000000;
							constexpr Reg Value_enable = 0x1;
							constexpr Reg ValueMask_enable = 0x00000008;
							constexpr Reg Value_enabled = 0x1;
							constexpr Reg ValueMask_enabled = 0x00000008;
							};
						namespace fCC4IE { // Field Description
							constexpr Reg Lsb = 4;
							constexpr Reg FieldMask = 0x00000010;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							constexpr Reg Value_disable = 0x0;
							constexpr Reg ValueMask_disable = 0x00000000;
							constexpr Reg Value_disabled = 0x0;
							constexpr Reg ValueMask_disabled = 0x00000000;
							constexpr Reg Value_enable = 0x1;
							constexpr Reg ValueMask_enable = 0x00000010;
							constexpr Reg Value_enabled = 0x1;
							constexpr Reg ValueMask_enabled = 0x00000010;
							};
						namespace fCOMIE { // Field Description
							constexpr Reg Lsb = 5;
							constexpr Reg FieldMask = 0x00000020;
							};
						namespace fTIE { // Field Description
							constexpr Reg Lsb = 6;
							constexpr Reg FieldMask = 0x00000040;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							constexpr Reg Value_disable = 0x0;
							constexpr Reg ValueMask_disable = 0x00000000;
							constexpr Reg Value_disabled = 0x0;
							constexpr Reg ValueMask_disabled = 0x00000000;
							constexpr Reg Value_enable = 0x1;
							constexpr Reg ValueMask_enable = 0x00000040;
							constexpr Reg Value_enabled = 0x1;
							constexpr Reg ValueMask_enabled = 0x00000040;
							};
						namespace fBIE { // Field Description
							constexpr Reg Lsb = 7;
							constexpr Reg FieldMask = 0x00000080;
							};
						namespace fUDE { // Field Description
							constexpr Reg Lsb = 8;
							constexpr Reg FieldMask = 0x00000100;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							constexpr Reg Value_disable = 0x0;
							constexpr Reg ValueMask_disable = 0x00000000;
							constexpr Reg Value_disabled = 0x0;
							constexpr Reg ValueMask_disabled = 0x00000000;
							constexpr Reg Value_enable = 0x1;
							constexpr Reg ValueMask_enable = 0x00000100;
							constexpr Reg Value_enabled = 0x1;
							constexpr Reg ValueMask_enabled = 0x00000100;
							};
						namespace fCC1DE { // Field Description
							constexpr Reg Lsb = 9;
							constexpr Reg FieldMask = 0x00000200;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							constexpr Reg Value_disable = 0x0;
							constexpr Reg ValueMask_disable = 0x00000000;
							constexpr Reg Value_disabled = 0x0;
							constexpr Reg ValueMask_disabled = 0x00000000;
							constexpr Reg Value_enable = 0x1;
							constexpr Reg ValueMask_enable = 0x00000200;
							constexpr Reg Value_enabled = 0x1;
							constexpr Reg ValueMask_enabled = 0x00000200;
							};
						namespace fCC2DE { // Field Description
							constexpr Reg Lsb = 10;
							constexpr Reg FieldMask = 0x00000400;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							constexpr Reg Value_disable = 0x0;
							constexpr Reg ValueMask_disable = 0x00000000;
							constexpr Reg Value_disabled = 0x0;
							constexpr Reg ValueMask_disabled = 0x00000000;
							constexpr Reg Value_enable = 0x1;
							constexpr Reg ValueMask_enable = 0x00000400;
							constexpr Reg Value_enabled = 0x1;
							constexpr Reg ValueMask_enabled = 0x00000400;
							};
						namespace fCC3DE { // Field Description
							constexpr Reg Lsb = 11;
							constexpr Reg FieldMask = 0x00000800;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							constexpr Reg Value_disable = 0x0;
							constexpr Reg ValueMask_disable = 0x00000000;
							constexpr Reg Value_disabled = 0x0;
							constexpr Reg ValueMask_disabled = 0x00000000;
							constexpr Reg Value_enable = 0x1;
							constexpr Reg ValueMask_enable = 0x00000800;
							constexpr Reg Value_enabled = 0x1;
							constexpr Reg ValueMask_enabled = 0x00000800;
							};
						namespace fCC4DE { // Field Description
							constexpr Reg Lsb = 12;
							constexpr Reg FieldMask = 0x00001000;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							constexpr Reg Value_disable = 0x0;
							constexpr Reg ValueMask_disable = 0x00000000;
							constexpr Reg Value_disabled = 0x0;
							constexpr Reg ValueMask_disabled = 0x00000000;
							constexpr Reg Value_enable = 0x1;
							constexpr Reg ValueMask_enable = 0x00001000;
							constexpr Reg Value_enabled = 0x1;
							constexpr Reg ValueMask_enabled = 0x00001000;
							};
						namespace fCOMDE { // Field Description
							constexpr Reg Lsb = 13;
							constexpr Reg FieldMask = 0x00002000;
							};
						namespace fTDE { // Field Description
							constexpr Reg Lsb = 14;
							constexpr Reg FieldMask = 0x00004000;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							constexpr Reg Value_disable = 0x0;
							constexpr Reg ValueMask_disable = 0x00000000;
							constexpr Reg Value_disabled = 0x0;
							constexpr Reg ValueMask_disabled = 0x00000000;
							constexpr Reg Value_enable = 0x1;
							constexpr Reg ValueMask_enable = 0x00004000;
							constexpr Reg Value_enabled = 0x1;
							constexpr Reg ValueMask_enabled = 0x00004000;
							};
						};
					namespace rSR { // Register description
						typedef uint32_t	Reg;
						constexpr Reg	valueAfterReset = 0x00000000;
						constexpr Reg	noEffect = 0xFFFFFFFF;
						namespace fUIF { // Field Description
							constexpr Reg Lsb = 0;
							constexpr Reg FieldMask = 0x00000001;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							constexpr Reg Value_pending = 0x1;
							constexpr Reg ValueMask_pending = 0x00000001;
							constexpr Reg Value_clear = 0x0;
							constexpr Reg ValueMask_clear = 0x00000000;
							constexpr Reg Value_noEffect = 0x1;
							constexpr Reg ValueMask_noEffect = 0x00000001;
							};
						namespace fCC1IF { // Field Description
							constexpr Reg Lsb = 1;
							constexpr Reg FieldMask = 0x00000002;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							constexpr Reg Value_pending = 0x1;
							constexpr Reg ValueMask_pending = 0x00000002;
							constexpr Reg Value_clear = 0x0;
							constexpr Reg ValueMask_clear = 0x00000000;
							constexpr Reg Value_noEffect = 0x1;
							constexpr Reg ValueMask_noEffect = 0x00000002;
							};
						namespace fCC2IF { // Field Description
							constexpr Reg Lsb = 2;
							constexpr Reg FieldMask = 0x00000004;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							constexpr Reg Value_pending = 0x1;
							constexpr Reg ValueMask_pending = 0x00000004;
							constexpr Reg Value_clear = 0x0;
							constexpr Reg ValueMask_clear = 0x00000000;
							constexpr Reg Value_noEffect = 0x1;
							constexpr Reg ValueMask_noEffect = 0x00000004;
							};
						namespace fCC3IF { // Field Description
							constexpr Reg Lsb = 3;
							constexpr Reg FieldMask = 0x00000008;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							constexpr Reg Value_pending = 0x1;
							constexpr Reg ValueMask_pending = 0x00000008;
							constexpr Reg Value_clear = 0x0;
							constexpr Reg ValueMask_clear = 0x00000000;
							constexpr Reg Value_noEffect = 0x1;
							constexpr Reg ValueMask_noEffect = 0x00000008;
							};
						namespace fCC4IF { // Field Description
							constexpr Reg Lsb = 4;
							constexpr Reg FieldMask = 0x00000010;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							constexpr Reg Value_pending = 0x1;
							constexpr Reg ValueMask_pending = 0x00000010;
							constexpr Reg Value_clear = 0x0;
							constexpr Reg ValueMask_clear = 0x00000000;
							constexpr Reg Value_noEffect = 0x1;
							constexpr Reg ValueMask_noEffect = 0x00000010;
							};
						namespace fCOMIF { // Field Description
							constexpr Reg Lsb = 5;
							constexpr Reg FieldMask = 0x00000020;
							};
						namespace fTIF { // Field Description
							constexpr Reg Lsb = 6;
							constexpr Reg FieldMask = 0x00000040;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							constexpr Reg Value_pending = 0x1;
							constexpr Reg ValueMask_pending = 0x00000040;
							constexpr Reg Value_clear = 0x0;
							constexpr Reg ValueMask_clear = 0x00000000;
							constexpr Reg Value_noEffect = 0x1;
							constexpr Reg ValueMask_noEffect = 0x00000040;
							};
						namespace fBIF { // Field Description
							constexpr Reg Lsb = 7;
							constexpr Reg FieldMask = 0x00000080;
							};
						namespace fB2IF { // Field Description
							constexpr Reg Lsb = 8;
							constexpr Reg FieldMask = 0x00000100;
							};
						namespace fCC1OF { // Field Description
							constexpr Reg Lsb = 9;
							constexpr Reg FieldMask = 0x00000200;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							constexpr Reg Value_pending = 0x1;
							constexpr Reg ValueMask_pending = 0x00000200;
							constexpr Reg Value_clear = 0x0;
							constexpr Reg ValueMask_clear = 0x00000000;
							constexpr Reg Value_noEffect = 0x1;
							constexpr Reg ValueMask_noEffect = 0x00000200;
							};
						namespace fCC2OF { // Field Description
							constexpr Reg Lsb = 10;
							constexpr Reg FieldMask = 0x00000400;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							constexpr Reg Value_pending = 0x1;
							constexpr Reg ValueMask_pending = 0x00000400;
							constexpr Reg Value_clear = 0x0;
							constexpr Reg ValueMask_clear = 0x00000000;
							constexpr Reg Value_noEffect = 0x1;
							constexpr Reg ValueMask_noEffect = 0x00000400;
							};
						namespace fCC3OF { // Field Description
							constexpr Reg Lsb = 11;
							constexpr Reg FieldMask = 0x00000800;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							constexpr Reg Value_pending = 0x1;
							constexpr Reg ValueMask_pending = 0x00000800;
							constexpr Reg Value_clear = 0x0;
							constexpr Reg ValueMask_clear = 0x00000000;
							constexpr Reg Value_noEffect = 0x1;
							constexpr Reg ValueMask_noEffect = 0x00000800;
							};
						namespace fCC4OF { // Field Description
							constexpr Reg Lsb = 12;
							constexpr Reg FieldMask = 0x00001000;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							constexpr Reg Value_pending = 0x1;
							constexpr Reg ValueMask_pending = 0x00001000;
							constexpr Reg Value_clear = 0x0;
							constexpr Reg ValueMask_clear = 0x00000000;
							constexpr Reg Value_noEffect = 0x1;
							constexpr Reg ValueMask_noEffect = 0x00001000;
							};
						namespace fSBIF { // Field Description
							constexpr Reg Lsb = 13;
							constexpr Reg FieldMask = 0x00002000;
							};
						namespace fCC5IF { // Field Description
							constexpr Reg Lsb = 16;
							constexpr Reg FieldMask = 0x00010000;
							};
						namespace fCC6IF { // Field Description
							constexpr Reg Lsb = 17;
							constexpr Reg FieldMask = 0x00020000;
							};
						};
					namespace rEGR { // Register description
						typedef uint32_t	Reg;
						constexpr Reg	valueAfterReset = 0x00000000;
						namespace fUG { // Field Description
							constexpr Reg Lsb = 0;
							constexpr Reg FieldMask = 0x00000001;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							constexpr Reg Value_noAction = 0x0;
							constexpr Reg ValueMask_noAction = 0x00000000;
							constexpr Reg Value_trigger = 0x1;
							constexpr Reg ValueMask_trigger = 0x00000001;
							};
						namespace fCC1G { // Field Description
							constexpr Reg Lsb = 1;
							constexpr Reg FieldMask = 0x00000002;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							constexpr Reg Value_noAction = 0x0;
							constexpr Reg ValueMask_noAction = 0x00000000;
							constexpr Reg Value_trigger = 0x1;
							constexpr Reg ValueMask_trigger = 0x00000002;
							};
						namespace fCC2G { // Field Description
							constexpr Reg Lsb = 2;
							constexpr Reg FieldMask = 0x00000004;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							constexpr Reg Value_noAction = 0x0;
							constexpr Reg ValueMask_noAction = 0x00000000;
							constexpr Reg Value_trigger = 0x1;
							constexpr Reg ValueMask_trigger = 0x00000004;
							};
						namespace fCC3G { // Field Description
							constexpr Reg Lsb = 3;
							constexpr Reg FieldMask = 0x00000008;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							constexpr Reg Value_noAction = 0x0;
							constexpr Reg ValueMask_noAction = 0x00000000;
							constexpr Reg Value_trigger = 0x1;
							constexpr Reg ValueMask_trigger = 0x00000008;
							};
						namespace fCC4G { // Field Description
							constexpr Reg Lsb = 4;
							constexpr Reg FieldMask = 0x00000010;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							constexpr Reg Value_noAction = 0x0;
							constexpr Reg ValueMask_noAction = 0x00000000;
							constexpr Reg Value_trigger = 0x1;
							constexpr Reg ValueMask_trigger = 0x00000010;
							};
						namespace fCOMG { // Field Description
							constexpr Reg Lsb = 5;
							constexpr Reg FieldMask = 0x00000020;
							};
						namespace fTG { // Field Description
							constexpr Reg Lsb = 6;
							constexpr Reg FieldMask = 0x00000040;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							constexpr Reg Value_noAction = 0x0;
							constexpr Reg ValueMask_noAction = 0x00000000;
							constexpr Reg Value_trigger = 0x1;
							constexpr Reg ValueMask_trigger = 0x00000040;
							};
						namespace fBG { // Field Description
							constexpr Reg Lsb = 7;
							constexpr Reg FieldMask = 0x00000080;
							};
						namespace fB2G { // Field Description
							constexpr Reg Lsb = 8;
							constexpr Reg FieldMask = 0x00000100;
							};
						};
					namespace rCCMR_Input { // Register description
						typedef uint32_t	Reg;
						constexpr Reg	valueAfterReset = 0x00000000;
						namespace fCClS { // Field Description
							constexpr Reg Lsb = 0;
							constexpr Reg FieldMask = 0x00000003;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							namespace xCC1S { // Field Description
								constexpr Reg Lsb = 0;
								constexpr Reg FieldMask = 0x00000003;
								constexpr Reg Value_output = 0x0;
								constexpr Reg ValueMask_output = 0x00000000;
								constexpr Reg Value_captureEvery2 = 0x1;
								constexpr Reg ValueMask_captureEvery2 = 0x00000001;
								constexpr Reg Value_captureEvery4 = 0x2;
								constexpr Reg ValueMask_captureEvery4 = 0x00000002;
								constexpr Reg Value_captureEvery8 = 0x3;
								constexpr Reg ValueMask_captureEvery8 = 0x00000003;
								};
							namespace xCC3S { // Field Description
								constexpr Reg Lsb = 0;
								constexpr Reg FieldMask = 0x00000003;
								constexpr Reg Value_output = 0x0;
								constexpr Reg ValueMask_output = 0x00000000;
								constexpr Reg Value_captureEvery2 = 0x1;
								constexpr Reg ValueMask_captureEvery2 = 0x00000001;
								constexpr Reg Value_captureEvery4 = 0x2;
								constexpr Reg ValueMask_captureEvery4 = 0x00000002;
								constexpr Reg Value_captureEvery8 = 0x3;
								constexpr Reg ValueMask_captureEvery8 = 0x00000003;
								};
							};
						namespace fIClPSC { // Field Description
							constexpr Reg Lsb = 2;
							constexpr Reg FieldMask = 0x0000000C;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							namespace fIC1PSC { // Field Description
								constexpr Reg Lsb = 2;
								constexpr Reg FieldMask = 0x0000000C;
								constexpr Reg Value_noPrescaler = 0x0;
								constexpr Reg ValueMask_noPrescaler = 0x00000000;
								constexpr Reg Value_captureEvery2 = 0x1;
								constexpr Reg ValueMask_captureEvery2 = 0x00000004;
								constexpr Reg Value_captureEvery4 = 0x2;
								constexpr Reg ValueMask_captureEvery4 = 0x00000008;
								constexpr Reg Value_captureEvery8 = 0x3;
								constexpr Reg ValueMask_captureEvery8 = 0x0000000C;
								};
							namespace fIC3PSC { // Field Description
								constexpr Reg Lsb = 2;
								constexpr Reg FieldMask = 0x0000000C;
								constexpr Reg Value_noPrescaler = 0x0;
								constexpr Reg ValueMask_noPrescaler = 0x00000000;
								constexpr Reg Value_captureEvery2 = 0x1;
								constexpr Reg ValueMask_captureEvery2 = 0x00000004;
								constexpr Reg Value_captureEvery4 = 0x2;
								constexpr Reg ValueMask_captureEvery4 = 0x00000008;
								constexpr Reg Value_captureEvery8 = 0x3;
								constexpr Reg ValueMask_captureEvery8 = 0x0000000C;
								};
							};
						namespace fIClF { // Field Description
							constexpr Reg Lsb = 4;
							constexpr Reg FieldMask = 0x000000F0;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							namespace fIC1F { // Field Description
								constexpr Reg Lsb = 4;
								constexpr Reg FieldMask = 0x000000F0;
								constexpr Reg Value_none = 0x0;
								constexpr Reg ValueMask_none = 0x00000000;
								constexpr Reg Value_fCK_INT_N2 = 0x1;
								constexpr Reg ValueMask_fCK_INT_N2 = 0x00000010;
								constexpr Reg Value_fCK_INT_N4 = 0x2;
								constexpr Reg ValueMask_fCK_INT_N4 = 0x00000020;
								constexpr Reg Value_fCK_INT_N8 = 0x3;
								constexpr Reg ValueMask_fCK_INT_N8 = 0x00000030;
								constexpr Reg Value_fDTS_div2_N6 = 0x4;
								constexpr Reg ValueMask_fDTS_div2_N6 = 0x00000040;
								constexpr Reg Value_fDTS_div2_N8 = 0x5;
								constexpr Reg ValueMask_fDTS_div2_N8 = 0x00000050;
								constexpr Reg Value_fDTS_div4_N6 = 0x6;
								constexpr Reg ValueMask_fDTS_div4_N6 = 0x00000060;
								constexpr Reg Value_fDTS_div4_N8 = 0x7;
								constexpr Reg ValueMask_fDTS_div4_N8 = 0x00000070;
								constexpr Reg Value_fDTS_div8_N6 = 0x8;
								constexpr Reg ValueMask_fDTS_div8_N6 = 0x00000080;
								constexpr Reg Value_fDTS_div8_N8 = 0x9;
								constexpr Reg ValueMask_fDTS_div8_N8 = 0x00000090;
								constexpr Reg Value_fDTS_div16_N5 = 0xA;
								constexpr Reg ValueMask_fDTS_div16_N5 = 0x000000A0;
								constexpr Reg Value_fDTS_div16_N6 = 0xB;
								constexpr Reg ValueMask_fDTS_div16_N6 = 0x000000B0;
								constexpr Reg Value_fDTS_div16_N8 = 0xC;
								constexpr Reg ValueMask_fDTS_div16_N8 = 0x000000C0;
								constexpr Reg Value_fDTS_div32_N5 = 0xD;
								constexpr Reg ValueMask_fDTS_div32_N5 = 0x000000D0;
								constexpr Reg Value_fDTS_div32_N6 = 0xE;
								constexpr Reg ValueMask_fDTS_div32_N6 = 0x000000E0;
								constexpr Reg Value_fDTS_div32_N8 = 0xF;
								constexpr Reg ValueMask_fDTS_div32_N8 = 0x000000F0;
								};
							namespace fIC3F { // Field Description
								constexpr Reg Lsb = 4;
								constexpr Reg FieldMask = 0x000000F0;
								constexpr Reg Value_none = 0x0;
								constexpr Reg ValueMask_none = 0x00000000;
								constexpr Reg Value_fCK_INT_N2 = 0x1;
								constexpr Reg ValueMask_fCK_INT_N2 = 0x00000010;
								constexpr Reg Value_fCK_INT_N4 = 0x2;
								constexpr Reg ValueMask_fCK_INT_N4 = 0x00000020;
								constexpr Reg Value_fCK_INT_N8 = 0x3;
								constexpr Reg ValueMask_fCK_INT_N8 = 0x00000030;
								constexpr Reg Value_fDTS_div2_N6 = 0x4;
								constexpr Reg ValueMask_fDTS_div2_N6 = 0x00000040;
								constexpr Reg Value_fDTS_div2_N8 = 0x5;
								constexpr Reg ValueMask_fDTS_div2_N8 = 0x00000050;
								constexpr Reg Value_fDTS_div4_N6 = 0x6;
								constexpr Reg ValueMask_fDTS_div4_N6 = 0x00000060;
								constexpr Reg Value_fDTS_div4_N8 = 0x7;
								constexpr Reg ValueMask_fDTS_div4_N8 = 0x00000070;
								constexpr Reg Value_fDTS_div8_N6 = 0x8;
								constexpr Reg ValueMask_fDTS_div8_N6 = 0x00000080;
								constexpr Reg Value_fDTS_div8_N8 = 0x9;
								constexpr Reg ValueMask_fDTS_div8_N8 = 0x00000090;
								constexpr Reg Value_fDTS_div16_N5 = 0xA;
								constexpr Reg ValueMask_fDTS_div16_N5 = 0x000000A0;
								constexpr Reg Value_fDTS_div16_N6 = 0xB;
								constexpr Reg ValueMask_fDTS_div16_N6 = 0x000000B0;
								constexpr Reg Value_fDTS_div16_N8 = 0xC;
								constexpr Reg ValueMask_fDTS_div16_N8 = 0x000000C0;
								constexpr Reg Value_fDTS_div32_N5 = 0xD;
								constexpr Reg ValueMask_fDTS_div32_N5 = 0x000000D0;
								constexpr Reg Value_fDTS_div32_N6 = 0xE;
								constexpr Reg ValueMask_fDTS_div32_N6 = 0x000000E0;
								constexpr Reg Value_fDTS_div32_N8 = 0xF;
								constexpr Reg ValueMask_fDTS_div32_N8 = 0x000000F0;
								};
							};
						namespace fCCuS { // Field Description
							constexpr Reg Lsb = 8;
							constexpr Reg FieldMask = 0x00000300;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							namespace fCC2S { // Field Description
								constexpr Reg Lsb = 8;
								constexpr Reg FieldMask = 0x00000300;
								constexpr Reg Value_output = 0x0;
								constexpr Reg ValueMask_output = 0x00000000;
								constexpr Reg Value_cc2InputIcMappedOnTl2 = 0x1;
								constexpr Reg ValueMask_cc2InputIcMappedOnTl2 = 0x00000100;
								constexpr Reg Value_cc2InputIcMappedOnTl1 = 0x2;
								constexpr Reg ValueMask_cc2InputIcMappedOnTl1 = 0x00000200;
								constexpr Reg Value_cc2InputIcMappedOnTRC = 0x3;
								constexpr Reg ValueMask_cc2InputIcMappedOnTRC = 0x00000300;
								};
							namespace fCC4S { // Field Description
								constexpr Reg Lsb = 8;
								constexpr Reg FieldMask = 0x00000300;
								constexpr Reg Value_output = 0x0;
								constexpr Reg ValueMask_output = 0x00000000;
								constexpr Reg Value_cc2InputIcMappedOnTl2 = 0x1;
								constexpr Reg ValueMask_cc2InputIcMappedOnTl2 = 0x00000100;
								constexpr Reg Value_cc2InputIcMappedOnTl1 = 0x2;
								constexpr Reg ValueMask_cc2InputIcMappedOnTl1 = 0x00000200;
								constexpr Reg Value_cc2InputIcMappedOnTRC = 0x3;
								constexpr Reg ValueMask_cc2InputIcMappedOnTRC = 0x00000300;
								};
							};
						namespace fICuPCS { // Field Description
							constexpr Reg Lsb = 10;
							constexpr Reg FieldMask = 0x00000C00;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							namespace fIC2PCS { // Field Description
								constexpr Reg Lsb = 10;
								constexpr Reg FieldMask = 0x00000C00;
								constexpr Reg Value_noPrescaler = 0x0;
								constexpr Reg ValueMask_noPrescaler = 0x00000000;
								constexpr Reg Value_captureEvery2 = 0x1;
								constexpr Reg ValueMask_captureEvery2 = 0x00000400;
								constexpr Reg Value_captureEvery4 = 0x2;
								constexpr Reg ValueMask_captureEvery4 = 0x00000800;
								constexpr Reg Value_captureEvery8 = 0x3;
								constexpr Reg ValueMask_captureEvery8 = 0x00000C00;
								};
							namespace fIC4PCS { // Field Description
								constexpr Reg Lsb = 10;
								constexpr Reg FieldMask = 0x00000C00;
								constexpr Reg Value_noPrescaler = 0x0;
								constexpr Reg ValueMask_noPrescaler = 0x00000000;
								constexpr Reg Value_captureEvery2 = 0x1;
								constexpr Reg ValueMask_captureEvery2 = 0x00000400;
								constexpr Reg Value_captureEvery4 = 0x2;
								constexpr Reg ValueMask_captureEvery4 = 0x00000800;
								constexpr Reg Value_captureEvery8 = 0x3;
								constexpr Reg ValueMask_captureEvery8 = 0x00000C00;
								};
							};
						namespace fICuF { // Field Description
							constexpr Reg Lsb = 12;
							constexpr Reg FieldMask = 0x0000F000;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							namespace fIC2F { // Field Description
								constexpr Reg Lsb = 12;
								constexpr Reg FieldMask = 0x0000F000;
								constexpr Reg Value_none = 0x0;
								constexpr Reg ValueMask_none = 0x00000000;
								constexpr Reg Value_fCK_INT_N2 = 0x1;
								constexpr Reg ValueMask_fCK_INT_N2 = 0x00001000;
								constexpr Reg Value_fCK_INT_N4 = 0x2;
								constexpr Reg ValueMask_fCK_INT_N4 = 0x00002000;
								constexpr Reg Value_fCK_INT_N8 = 0x3;
								constexpr Reg ValueMask_fCK_INT_N8 = 0x00003000;
								constexpr Reg Value_fDTS_div2_N6 = 0x4;
								constexpr Reg ValueMask_fDTS_div2_N6 = 0x00004000;
								constexpr Reg Value_fDTS_div2_N8 = 0x5;
								constexpr Reg ValueMask_fDTS_div2_N8 = 0x00005000;
								constexpr Reg Value_fDTS_div4_N6 = 0x6;
								constexpr Reg ValueMask_fDTS_div4_N6 = 0x00006000;
								constexpr Reg Value_fDTS_div4_N8 = 0x7;
								constexpr Reg ValueMask_fDTS_div4_N8 = 0x00007000;
								constexpr Reg Value_fDTS_div8_N6 = 0x8;
								constexpr Reg ValueMask_fDTS_div8_N6 = 0x00008000;
								constexpr Reg Value_fDTS_div8_N8 = 0x9;
								constexpr Reg ValueMask_fDTS_div8_N8 = 0x00009000;
								constexpr Reg Value_fDTS_div16_N5 = 0xA;
								constexpr Reg ValueMask_fDTS_div16_N5 = 0x0000A000;
								constexpr Reg Value_fDTS_div16_N6 = 0xB;
								constexpr Reg ValueMask_fDTS_div16_N6 = 0x0000B000;
								constexpr Reg Value_fDTS_div16_N8 = 0xC;
								constexpr Reg ValueMask_fDTS_div16_N8 = 0x0000C000;
								constexpr Reg Value_fDTS_div32_N5 = 0xD;
								constexpr Reg ValueMask_fDTS_div32_N5 = 0x0000D000;
								constexpr Reg Value_fDTS_div32_N6 = 0xE;
								constexpr Reg ValueMask_fDTS_div32_N6 = 0x0000E000;
								constexpr Reg Value_fDTS_div32_N8 = 0xF;
								constexpr Reg ValueMask_fDTS_div32_N8 = 0x0000F000;
								};
							namespace fIC4F { // Field Description
								constexpr Reg Lsb = 12;
								constexpr Reg FieldMask = 0x0000F000;
								constexpr Reg Value_none = 0x0;
								constexpr Reg ValueMask_none = 0x00000000;
								constexpr Reg Value_fCK_INT_N2 = 0x1;
								constexpr Reg ValueMask_fCK_INT_N2 = 0x00001000;
								constexpr Reg Value_fCK_INT_N4 = 0x2;
								constexpr Reg ValueMask_fCK_INT_N4 = 0x00002000;
								constexpr Reg Value_fCK_INT_N8 = 0x3;
								constexpr Reg ValueMask_fCK_INT_N8 = 0x00003000;
								constexpr Reg Value_fDTS_div2_N6 = 0x4;
								constexpr Reg ValueMask_fDTS_div2_N6 = 0x00004000;
								constexpr Reg Value_fDTS_div2_N8 = 0x5;
								constexpr Reg ValueMask_fDTS_div2_N8 = 0x00005000;
								constexpr Reg Value_fDTS_div4_N6 = 0x6;
								constexpr Reg ValueMask_fDTS_div4_N6 = 0x00006000;
								constexpr Reg Value_fDTS_div4_N8 = 0x7;
								constexpr Reg ValueMask_fDTS_div4_N8 = 0x00007000;
								constexpr Reg Value_fDTS_div8_N6 = 0x8;
								constexpr Reg ValueMask_fDTS_div8_N6 = 0x00008000;
								constexpr Reg Value_fDTS_div8_N8 = 0x9;
								constexpr Reg ValueMask_fDTS_div8_N8 = 0x00009000;
								constexpr Reg Value_fDTS_div16_N5 = 0xA;
								constexpr Reg ValueMask_fDTS_div16_N5 = 0x0000A000;
								constexpr Reg Value_fDTS_div16_N6 = 0xB;
								constexpr Reg ValueMask_fDTS_div16_N6 = 0x0000B000;
								constexpr Reg Value_fDTS_div16_N8 = 0xC;
								constexpr Reg ValueMask_fDTS_div16_N8 = 0x0000C000;
								constexpr Reg Value_fDTS_div32_N5 = 0xD;
								constexpr Reg ValueMask_fDTS_div32_N5 = 0x0000D000;
								constexpr Reg Value_fDTS_div32_N6 = 0xE;
								constexpr Reg ValueMask_fDTS_div32_N6 = 0x0000E000;
								constexpr Reg Value_fDTS_div32_N8 = 0xF;
								constexpr Reg ValueMask_fDTS_div32_N8 = 0x0000F000;
								};
							};
						};
					namespace rCCMR_Output { // Register description
						typedef uint32_t	Reg;
						constexpr Reg	valueAfterReset = 0x00000000;
						namespace fCClS { // Field Description
							constexpr Reg Lsb = 0;
							constexpr Reg FieldMask = 0x00000003;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							namespace fCC1S { // Field Description
								constexpr Reg Lsb = 0;
								constexpr Reg FieldMask = 0x00000003;
								constexpr Reg Value_output = 0x0;
								constexpr Reg ValueMask_output = 0x00000000;
								constexpr Reg Value_cc1InputIcMappedOnTl2 = 0x1;
								constexpr Reg ValueMask_cc1InputIcMappedOnTl2 = 0x00000001;
								constexpr Reg Value_cc1InputIcMappedOnTl1 = 0x2;
								constexpr Reg ValueMask_cc1InputIcMappedOnTl1 = 0x00000002;
								constexpr Reg Value_cc1InputIcMappedOnTRC = 0x3;
								constexpr Reg ValueMask_cc1InputIcMappedOnTRC = 0x00000003;
								};
							namespace fCC3S { // Field Description
								constexpr Reg Lsb = 0;
								constexpr Reg FieldMask = 0x00000003;
								constexpr Reg Value_output = 0x0;
								constexpr Reg ValueMask_output = 0x00000000;
								constexpr Reg Value_cc1InputIcMappedOnTl2 = 0x1;
								constexpr Reg ValueMask_cc1InputIcMappedOnTl2 = 0x00000001;
								constexpr Reg Value_cc1InputIcMappedOnTl1 = 0x2;
								constexpr Reg ValueMask_cc1InputIcMappedOnTl1 = 0x00000002;
								constexpr Reg Value_cc1InputIcMappedOnTRC = 0x3;
								constexpr Reg ValueMask_cc1InputIcMappedOnTRC = 0x00000003;
								};
							namespace fCC5S { // Field Description
								constexpr Reg Lsb = 0;
								constexpr Reg FieldMask = 0x00000003;
								constexpr Reg Value_output = 0x0;
								constexpr Reg ValueMask_output = 0x00000000;
								constexpr Reg Value_cc1InputIcMappedOnTl2 = 0x1;
								constexpr Reg ValueMask_cc1InputIcMappedOnTl2 = 0x00000001;
								constexpr Reg Value_cc1InputIcMappedOnTl1 = 0x2;
								constexpr Reg ValueMask_cc1InputIcMappedOnTl1 = 0x00000002;
								constexpr Reg Value_cc1InputIcMappedOnTRC = 0x3;
								constexpr Reg ValueMask_cc1InputIcMappedOnTRC = 0x00000003;
								};
							};
						namespace fOClFE { // Field Description
							constexpr Reg Lsb = 2;
							constexpr Reg FieldMask = 0x00000004;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							namespace fOC1FE { // Field Description
								constexpr Reg Lsb = 2;
								constexpr Reg FieldMask = 0x00000004;
								constexpr Reg Value_disable = 0x0;
								constexpr Reg ValueMask_disable = 0x00000000;
								constexpr Reg Value_disabled = 0x0;
								constexpr Reg ValueMask_disabled = 0x00000000;
								constexpr Reg Value_enable = 0x1;
								constexpr Reg ValueMask_enable = 0x00000004;
								constexpr Reg Value_enabled = 0x1;
								constexpr Reg ValueMask_enabled = 0x00000004;
								};
							namespace fOC3FE { // Field Description
								constexpr Reg Lsb = 2;
								constexpr Reg FieldMask = 0x00000004;
								constexpr Reg Value_disable = 0x0;
								constexpr Reg ValueMask_disable = 0x00000000;
								constexpr Reg Value_disabled = 0x0;
								constexpr Reg ValueMask_disabled = 0x00000000;
								constexpr Reg Value_enable = 0x1;
								constexpr Reg ValueMask_enable = 0x00000004;
								constexpr Reg Value_enabled = 0x1;
								constexpr Reg ValueMask_enabled = 0x00000004;
								};
							namespace fOC5FE { // Field Description
								constexpr Reg Lsb = 2;
								constexpr Reg FieldMask = 0x00000004;
								constexpr Reg Value_disable = 0x0;
								constexpr Reg ValueMask_disable = 0x00000000;
								constexpr Reg Value_disabled = 0x0;
								constexpr Reg ValueMask_disabled = 0x00000000;
								constexpr Reg Value_enable = 0x1;
								constexpr Reg ValueMask_enable = 0x00000004;
								constexpr Reg Value_enabled = 0x1;
								constexpr Reg ValueMask_enabled = 0x00000004;
								};
							};
						namespace fOClPE { // Field Description
							constexpr Reg Lsb = 3;
							constexpr Reg FieldMask = 0x00000008;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							namespace fOC1PE { // Field Description
								constexpr Reg Lsb = 3;
								constexpr Reg FieldMask = 0x00000008;
								constexpr Reg Value_disable = 0x0;
								constexpr Reg ValueMask_disable = 0x00000000;
								constexpr Reg Value_disabled = 0x0;
								constexpr Reg ValueMask_disabled = 0x00000000;
								constexpr Reg Value_enable = 0x1;
								constexpr Reg ValueMask_enable = 0x00000008;
								constexpr Reg Value_enabled = 0x1;
								constexpr Reg ValueMask_enabled = 0x00000008;
								};
							namespace fOC3PE { // Field Description
								constexpr Reg Lsb = 3;
								constexpr Reg FieldMask = 0x00000008;
								constexpr Reg Value_disable = 0x0;
								constexpr Reg ValueMask_disable = 0x00000000;
								constexpr Reg Value_disabled = 0x0;
								constexpr Reg ValueMask_disabled = 0x00000000;
								constexpr Reg Value_enable = 0x1;
								constexpr Reg ValueMask_enable = 0x00000008;
								constexpr Reg Value_enabled = 0x1;
								constexpr Reg ValueMask_enabled = 0x00000008;
								};
							namespace fOC5PE { // Field Description
								constexpr Reg Lsb = 3;
								constexpr Reg FieldMask = 0x00000008;
								constexpr Reg Value_disable = 0x0;
								constexpr Reg ValueMask_disable = 0x00000000;
								constexpr Reg Value_disabled = 0x0;
								constexpr Reg ValueMask_disabled = 0x00000000;
								constexpr Reg Value_enable = 0x1;
								constexpr Reg ValueMask_enable = 0x00000008;
								constexpr Reg Value_enabled = 0x1;
								constexpr Reg ValueMask_enabled = 0x00000008;
								};
							};
						namespace fOClM { // Field Description
							constexpr Reg Lsb = 4;
							constexpr Reg FieldMask = 0x00000070;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							namespace fOC1M { // Field Description
								constexpr Reg Lsb = 4;
								constexpr Reg FieldMask = 0x00000070;
								constexpr Reg Value_frozen = 0x0;
								constexpr Reg ValueMask_frozen = 0x00000000;
								constexpr Reg Value_ch1ActiveLevelMatch = 0x1;
								constexpr Reg ValueMask_ch1ActiveLevelMatch = 0x00000010;
								constexpr Reg Value_ch1InactiveLevelMatch = 0x2;
								constexpr Reg ValueMask_ch1InactiveLevelMatch = 0x00000020;
								constexpr Reg Value_toggle = 0x3;
								constexpr Reg ValueMask_toggle = 0x00000030;
								constexpr Reg Value_forceInactive = 0x4;
								constexpr Reg ValueMask_forceInactive = 0x00000040;
								constexpr Reg Value_forceActive = 0x5;
								constexpr Reg ValueMask_forceActive = 0x00000050;
								constexpr Reg Value_pwmMode1 = 0x6;
								constexpr Reg ValueMask_pwmMode1 = 0x00000060;
								constexpr Reg Value_pwmMode2 = 0x7;
								constexpr Reg ValueMask_pwmMode2 = 0x00000070;
								};
							namespace fOC3M { // Field Description
								constexpr Reg Lsb = 4;
								constexpr Reg FieldMask = 0x00000070;
								constexpr Reg Value_frozen = 0x0;
								constexpr Reg ValueMask_frozen = 0x00000000;
								constexpr Reg Value_ch1ActiveLevelMatch = 0x1;
								constexpr Reg ValueMask_ch1ActiveLevelMatch = 0x00000010;
								constexpr Reg Value_ch1InactiveLevelMatch = 0x2;
								constexpr Reg ValueMask_ch1InactiveLevelMatch = 0x00000020;
								constexpr Reg Value_toggle = 0x3;
								constexpr Reg ValueMask_toggle = 0x00000030;
								constexpr Reg Value_forceInactive = 0x4;
								constexpr Reg ValueMask_forceInactive = 0x00000040;
								constexpr Reg Value_forceActive = 0x5;
								constexpr Reg ValueMask_forceActive = 0x00000050;
								constexpr Reg Value_pwmMode1 = 0x6;
								constexpr Reg ValueMask_pwmMode1 = 0x00000060;
								constexpr Reg Value_pwmMode2 = 0x7;
								constexpr Reg ValueMask_pwmMode2 = 0x00000070;
								};
							namespace fOC5M { // Field Description
								constexpr Reg Lsb = 4;
								constexpr Reg FieldMask = 0x00000070;
								constexpr Reg Value_frozen = 0x0;
								constexpr Reg ValueMask_frozen = 0x00000000;
								constexpr Reg Value_ch1ActiveLevelMatch = 0x1;
								constexpr Reg ValueMask_ch1ActiveLevelMatch = 0x00000010;
								constexpr Reg Value_ch1InactiveLevelMatch = 0x2;
								constexpr Reg ValueMask_ch1InactiveLevelMatch = 0x00000020;
								constexpr Reg Value_toggle = 0x3;
								constexpr Reg ValueMask_toggle = 0x00000030;
								constexpr Reg Value_forceInactive = 0x4;
								constexpr Reg ValueMask_forceInactive = 0x00000040;
								constexpr Reg Value_forceActive = 0x5;
								constexpr Reg ValueMask_forceActive = 0x00000050;
								constexpr Reg Value_pwmMode1 = 0x6;
								constexpr Reg ValueMask_pwmMode1 = 0x00000060;
								constexpr Reg Value_pwmMode2 = 0x7;
								constexpr Reg ValueMask_pwmMode2 = 0x00000070;
								};
							};
						namespace fOClCE { // Field Description
							constexpr Reg Lsb = 7;
							constexpr Reg FieldMask = 0x00000080;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							namespace fOC1CE { // Field Description
								constexpr Reg Lsb = 7;
								constexpr Reg FieldMask = 0x00000080;
								constexpr Reg Value_oc1RefUnfazedByEtrf = 0x0;
								constexpr Reg ValueMask_oc1RefUnfazedByEtrf = 0x00000000;
								constexpr Reg Value_oc1RefClearedOnEtrf = 0x1;
								constexpr Reg ValueMask_oc1RefClearedOnEtrf = 0x00000080;
								};
							namespace fOC3CE { // Field Description
								constexpr Reg Lsb = 7;
								constexpr Reg FieldMask = 0x00000080;
								constexpr Reg Value_oc1RefUnfazedByEtrf = 0x0;
								constexpr Reg ValueMask_oc1RefUnfazedByEtrf = 0x00000000;
								constexpr Reg Value_oc1RefClearedOnEtrf = 0x1;
								constexpr Reg ValueMask_oc1RefClearedOnEtrf = 0x00000080;
								};
							namespace fOC5CE { // Field Description
								constexpr Reg Lsb = 7;
								constexpr Reg FieldMask = 0x00000080;
								constexpr Reg Value_oc1RefUnfazedByEtrf = 0x0;
								constexpr Reg ValueMask_oc1RefUnfazedByEtrf = 0x00000000;
								constexpr Reg Value_oc1RefClearedOnEtrf = 0x1;
								constexpr Reg ValueMask_oc1RefClearedOnEtrf = 0x00000080;
								};
							};
						namespace fCCuS { // Field Description
							constexpr Reg Lsb = 8;
							constexpr Reg FieldMask = 0x00000300;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							namespace fCC2S { // Field Description
								constexpr Reg Lsb = 8;
								constexpr Reg FieldMask = 0x00000300;
								constexpr Reg Value_output = 0x0;
								constexpr Reg ValueMask_output = 0x00000000;
								constexpr Reg Value_inputIcMappedonTI2 = 0x1;
								constexpr Reg ValueMask_inputIcMappedonTI2 = 0x00000100;
								constexpr Reg Value_inputIcMappedonTI1 = 0x2;
								constexpr Reg ValueMask_inputIcMappedonTI1 = 0x00000200;
								constexpr Reg Value_inputIcMappedonTRC = 0x3;
								constexpr Reg ValueMask_inputIcMappedonTRC = 0x00000300;
								};
							namespace fCC4S { // Field Description
								constexpr Reg Lsb = 8;
								constexpr Reg FieldMask = 0x00000300;
								constexpr Reg Value_output = 0x0;
								constexpr Reg ValueMask_output = 0x00000000;
								constexpr Reg Value_inputIcMappedonTI2 = 0x1;
								constexpr Reg ValueMask_inputIcMappedonTI2 = 0x00000100;
								constexpr Reg Value_inputIcMappedonTI1 = 0x2;
								constexpr Reg ValueMask_inputIcMappedonTI1 = 0x00000200;
								constexpr Reg Value_inputIcMappedonTRC = 0x3;
								constexpr Reg ValueMask_inputIcMappedonTRC = 0x00000300;
								};
							namespace fCC6S { // Field Description
								constexpr Reg Lsb = 8;
								constexpr Reg FieldMask = 0x00000300;
								constexpr Reg Value_output = 0x0;
								constexpr Reg ValueMask_output = 0x00000000;
								constexpr Reg Value_inputIcMappedonTI2 = 0x1;
								constexpr Reg ValueMask_inputIcMappedonTI2 = 0x00000100;
								constexpr Reg Value_inputIcMappedonTI1 = 0x2;
								constexpr Reg ValueMask_inputIcMappedonTI1 = 0x00000200;
								constexpr Reg Value_inputIcMappedonTRC = 0x3;
								constexpr Reg ValueMask_inputIcMappedonTRC = 0x00000300;
								};
							};
						namespace fOCuFE { // Field Description
							constexpr Reg Lsb = 10;
							constexpr Reg FieldMask = 0x00000400;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							namespace fOC2FE { // Field Description
								constexpr Reg Lsb = 10;
								constexpr Reg FieldMask = 0x00000400;
								constexpr Reg Value_disable = 0x0;
								constexpr Reg ValueMask_disable = 0x00000000;
								constexpr Reg Value_disabled = 0x0;
								constexpr Reg ValueMask_disabled = 0x00000000;
								constexpr Reg Value_enable = 0x1;
								constexpr Reg ValueMask_enable = 0x00000400;
								constexpr Reg Value_enabled = 0x1;
								constexpr Reg ValueMask_enabled = 0x00000400;
								};
							namespace fOC4FE { // Field Description
								constexpr Reg Lsb = 10;
								constexpr Reg FieldMask = 0x00000400;
								constexpr Reg Value_disable = 0x0;
								constexpr Reg ValueMask_disable = 0x00000000;
								constexpr Reg Value_disabled = 0x0;
								constexpr Reg ValueMask_disabled = 0x00000000;
								constexpr Reg Value_enable = 0x1;
								constexpr Reg ValueMask_enable = 0x00000400;
								constexpr Reg Value_enabled = 0x1;
								constexpr Reg ValueMask_enabled = 0x00000400;
								};
							namespace fOC6FE { // Field Description
								constexpr Reg Lsb = 10;
								constexpr Reg FieldMask = 0x00000400;
								constexpr Reg Value_disable = 0x0;
								constexpr Reg ValueMask_disable = 0x00000000;
								constexpr Reg Value_disabled = 0x0;
								constexpr Reg ValueMask_disabled = 0x00000000;
								constexpr Reg Value_enable = 0x1;
								constexpr Reg ValueMask_enable = 0x00000400;
								constexpr Reg Value_enabled = 0x1;
								constexpr Reg ValueMask_enabled = 0x00000400;
								};
							};
						namespace fOCuPE { // Field Description
							constexpr Reg Lsb = 11;
							constexpr Reg FieldMask = 0x00000800;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							namespace fOC2PE { // Field Description
								constexpr Reg Lsb = 11;
								constexpr Reg FieldMask = 0x00000800;
								constexpr Reg Value_disable = 0x0;
								constexpr Reg ValueMask_disable = 0x00000000;
								constexpr Reg Value_disabled = 0x0;
								constexpr Reg ValueMask_disabled = 0x00000000;
								constexpr Reg Value_enable = 0x1;
								constexpr Reg ValueMask_enable = 0x00000800;
								constexpr Reg Value_enabled = 0x1;
								constexpr Reg ValueMask_enabled = 0x00000800;
								};
							namespace fOC4PE { // Field Description
								constexpr Reg Lsb = 11;
								constexpr Reg FieldMask = 0x00000800;
								constexpr Reg Value_disable = 0x0;
								constexpr Reg ValueMask_disable = 0x00000000;
								constexpr Reg Value_disabled = 0x0;
								constexpr Reg ValueMask_disabled = 0x00000000;
								constexpr Reg Value_enable = 0x1;
								constexpr Reg ValueMask_enable = 0x00000800;
								constexpr Reg Value_enabled = 0x1;
								constexpr Reg ValueMask_enabled = 0x00000800;
								};
							namespace fOC6PE { // Field Description
								constexpr Reg Lsb = 11;
								constexpr Reg FieldMask = 0x00000800;
								constexpr Reg Value_disable = 0x0;
								constexpr Reg ValueMask_disable = 0x00000000;
								constexpr Reg Value_disabled = 0x0;
								constexpr Reg ValueMask_disabled = 0x00000000;
								constexpr Reg Value_enable = 0x1;
								constexpr Reg ValueMask_enable = 0x00000800;
								constexpr Reg Value_enabled = 0x1;
								constexpr Reg ValueMask_enabled = 0x00000800;
								};
							};
						namespace fOCuM { // Field Description
							constexpr Reg Lsb = 12;
							constexpr Reg FieldMask = 0x00007000;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							namespace fOC2M { // Field Description
								constexpr Reg Lsb = 12;
								constexpr Reg FieldMask = 0x00007000;
								constexpr Reg Value_frozen = 0x0;
								constexpr Reg ValueMask_frozen = 0x00000000;
								constexpr Reg Value_ch1ActiveLevelMatch = 0x1;
								constexpr Reg ValueMask_ch1ActiveLevelMatch = 0x00001000;
								constexpr Reg Value_ch1InactiveLevelMatch = 0x2;
								constexpr Reg ValueMask_ch1InactiveLevelMatch = 0x00002000;
								constexpr Reg Value_toggle = 0x3;
								constexpr Reg ValueMask_toggle = 0x00003000;
								constexpr Reg Value_forceInactive = 0x4;
								constexpr Reg ValueMask_forceInactive = 0x00004000;
								constexpr Reg Value_forceActive = 0x5;
								constexpr Reg ValueMask_forceActive = 0x00005000;
								constexpr Reg Value_pwmMode1 = 0x6;
								constexpr Reg ValueMask_pwmMode1 = 0x00006000;
								constexpr Reg Value_pwmMode2 = 0x7;
								constexpr Reg ValueMask_pwmMode2 = 0x00007000;
								};
							namespace fOC4M { // Field Description
								constexpr Reg Lsb = 12;
								constexpr Reg FieldMask = 0x00007000;
								constexpr Reg Value_frozen = 0x0;
								constexpr Reg ValueMask_frozen = 0x00000000;
								constexpr Reg Value_ch1ActiveLevelMatch = 0x1;
								constexpr Reg ValueMask_ch1ActiveLevelMatch = 0x00001000;
								constexpr Reg Value_ch1InactiveLevelMatch = 0x2;
								constexpr Reg ValueMask_ch1InactiveLevelMatch = 0x00002000;
								constexpr Reg Value_toggle = 0x3;
								constexpr Reg ValueMask_toggle = 0x00003000;
								constexpr Reg Value_forceInactive = 0x4;
								constexpr Reg ValueMask_forceInactive = 0x00004000;
								constexpr Reg Value_forceActive = 0x5;
								constexpr Reg ValueMask_forceActive = 0x00005000;
								constexpr Reg Value_pwmMode1 = 0x6;
								constexpr Reg ValueMask_pwmMode1 = 0x00006000;
								constexpr Reg Value_pwmMode2 = 0x7;
								constexpr Reg ValueMask_pwmMode2 = 0x00007000;
								};
							namespace fOC6M { // Field Description
								constexpr Reg Lsb = 12;
								constexpr Reg FieldMask = 0x00007000;
								constexpr Reg Value_frozen = 0x0;
								constexpr Reg ValueMask_frozen = 0x00000000;
								constexpr Reg Value_ch1ActiveLevelMatch = 0x1;
								constexpr Reg ValueMask_ch1ActiveLevelMatch = 0x00001000;
								constexpr Reg Value_ch1InactiveLevelMatch = 0x2;
								constexpr Reg ValueMask_ch1InactiveLevelMatch = 0x00002000;
								constexpr Reg Value_toggle = 0x3;
								constexpr Reg ValueMask_toggle = 0x00003000;
								constexpr Reg Value_forceInactive = 0x4;
								constexpr Reg ValueMask_forceInactive = 0x00004000;
								constexpr Reg Value_forceActive = 0x5;
								constexpr Reg ValueMask_forceActive = 0x00005000;
								constexpr Reg Value_pwmMode1 = 0x6;
								constexpr Reg ValueMask_pwmMode1 = 0x00006000;
								constexpr Reg Value_pwmMode2 = 0x7;
								constexpr Reg ValueMask_pwmMode2 = 0x00007000;
								};
							};
						namespace fOCuCE { // Field Description
							constexpr Reg Lsb = 15;
							constexpr Reg FieldMask = 0x00008000;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							namespace fOC2CE { // Field Description
								constexpr Reg Lsb = 15;
								constexpr Reg FieldMask = 0x00008000;
								constexpr Reg Value_disable = 0x0;
								constexpr Reg ValueMask_disable = 0x00000000;
								constexpr Reg Value_disabled = 0x0;
								constexpr Reg ValueMask_disabled = 0x00000000;
								constexpr Reg Value_enable = 0x1;
								constexpr Reg ValueMask_enable = 0x00008000;
								constexpr Reg Value_enabled = 0x1;
								constexpr Reg ValueMask_enabled = 0x00008000;
								};
							namespace fOC4CE { // Field Description
								constexpr Reg Lsb = 15;
								constexpr Reg FieldMask = 0x00008000;
								constexpr Reg Value_disable = 0x0;
								constexpr Reg ValueMask_disable = 0x00000000;
								constexpr Reg Value_disabled = 0x0;
								constexpr Reg ValueMask_disabled = 0x00000000;
								constexpr Reg Value_enable = 0x1;
								constexpr Reg ValueMask_enable = 0x00008000;
								constexpr Reg Value_enabled = 0x1;
								constexpr Reg ValueMask_enabled = 0x00008000;
								};
							namespace fOC6CE { // Field Description
								constexpr Reg Lsb = 15;
								constexpr Reg FieldMask = 0x00008000;
								constexpr Reg Value_disable = 0x0;
								constexpr Reg ValueMask_disable = 0x00000000;
								constexpr Reg Value_disabled = 0x0;
								constexpr Reg ValueMask_disabled = 0x00000000;
								constexpr Reg Value_enable = 0x1;
								constexpr Reg ValueMask_enable = 0x00008000;
								constexpr Reg Value_enabled = 0x1;
								constexpr Reg ValueMask_enabled = 0x00008000;
								};
							};
						namespace fOClM_3 { // Field Description
							constexpr Reg Lsb = 16;
							constexpr Reg FieldMask = 0x00010000;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							namespace fOC1M_3 { // Field Description
								constexpr Reg Lsb = 16;
								constexpr Reg FieldMask = 0x00010000;
								constexpr Reg Value_notRegriggerable = 0x0;
								constexpr Reg ValueMask_notRegriggerable = 0x00000000;
								constexpr Reg Value_retriggerable = 0x1;
								constexpr Reg ValueMask_retriggerable = 0x00010000;
								};
							namespace fOC3M_3 { // Field Description
								constexpr Reg Lsb = 16;
								constexpr Reg FieldMask = 0x00010000;
								constexpr Reg Value_notRegriggerable = 0x0;
								constexpr Reg ValueMask_notRegriggerable = 0x00000000;
								constexpr Reg Value_retriggerable = 0x1;
								constexpr Reg ValueMask_retriggerable = 0x00010000;
								};
							namespace fOC5M_3 { // Field Description
								constexpr Reg Lsb = 16;
								constexpr Reg FieldMask = 0x00010000;
								constexpr Reg Value_notRegriggerable = 0x0;
								constexpr Reg ValueMask_notRegriggerable = 0x00000000;
								constexpr Reg Value_retriggerable = 0x1;
								constexpr Reg ValueMask_retriggerable = 0x00010000;
								};
							};
						namespace fOCuM_3 { // Field Description
							constexpr Reg Lsb = 24;
							constexpr Reg FieldMask = 0x01000000;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							namespace fOC2M_3 { // Field Description
								constexpr Reg Lsb = 24;
								constexpr Reg FieldMask = 0x01000000;
								constexpr Reg Value_notRegriggerable = 0x0;
								constexpr Reg ValueMask_notRegriggerable = 0x00000000;
								constexpr Reg Value_retriggerable = 0x1;
								constexpr Reg ValueMask_retriggerable = 0x01000000;
								};
							namespace fOC4M_3 { // Field Description
								constexpr Reg Lsb = 24;
								constexpr Reg FieldMask = 0x01000000;
								constexpr Reg Value_notRegriggerable = 0x0;
								constexpr Reg ValueMask_notRegriggerable = 0x00000000;
								constexpr Reg Value_retriggerable = 0x1;
								constexpr Reg ValueMask_retriggerable = 0x01000000;
								};
							namespace fOC6M_3 { // Field Description
								constexpr Reg Lsb = 24;
								constexpr Reg FieldMask = 0x01000000;
								constexpr Reg Value_notRegriggerable = 0x0;
								constexpr Reg ValueMask_notRegriggerable = 0x00000000;
								constexpr Reg Value_retriggerable = 0x1;
								constexpr Reg ValueMask_retriggerable = 0x01000000;
								};
							};
						};
					namespace rCCER { // Register description
						typedef uint32_t	Reg;
						constexpr Reg	valueAfterReset = 0x00000000;
						namespace fCC1E { // Field Description
							constexpr Reg Lsb = 0;
							constexpr Reg FieldMask = 0x00000001;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							constexpr Reg Value_disable = 0x0;
							constexpr Reg ValueMask_disable = 0x00000000;
							constexpr Reg Value_disabled = 0x0;
							constexpr Reg ValueMask_disabled = 0x00000000;
							constexpr Reg Value_enable = 0x1;
							constexpr Reg ValueMask_enable = 0x00000001;
							constexpr Reg Value_enabled = 0x1;
							constexpr Reg ValueMask_enabled = 0x00000001;
							};
						namespace fCC1P { // Field Description
							constexpr Reg Lsb = 1;
							constexpr Reg FieldMask = 0x00000002;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							constexpr Reg Value_activeHighOutput = 0x0;
							constexpr Reg ValueMask_activeHighOutput = 0x00000000;
							constexpr Reg Value_nonInvertedInput = 0x0;
							constexpr Reg ValueMask_nonInvertedInput = 0x00000000;
							constexpr Reg Value_activeLowOutput = 0x1;
							constexpr Reg ValueMask_activeLowOutput = 0x00000002;
							constexpr Reg Value_invertedInput = 0x1;
							constexpr Reg ValueMask_invertedInput = 0x00000002;
							};
						namespace fCC1NE { // Field Description
							constexpr Reg Lsb = 2;
							constexpr Reg FieldMask = 0x00000004;
							};
						namespace fCC1NP { // Field Description
							constexpr Reg Lsb = 3;
							constexpr Reg FieldMask = 0x00000008;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							constexpr Reg Value_output = 0x0;
							constexpr Reg ValueMask_output = 0x00000000;
							constexpr Reg Value_input = 0x1;
							constexpr Reg ValueMask_input = 0x00000008;
							};
						namespace fCC2E { // Field Description
							constexpr Reg Lsb = 4;
							constexpr Reg FieldMask = 0x00000010;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							constexpr Reg Value_disable = 0x0;
							constexpr Reg ValueMask_disable = 0x00000000;
							constexpr Reg Value_disabled = 0x0;
							constexpr Reg ValueMask_disabled = 0x00000000;
							constexpr Reg Value_enable = 0x1;
							constexpr Reg ValueMask_enable = 0x00000010;
							constexpr Reg Value_enabled = 0x1;
							constexpr Reg ValueMask_enabled = 0x00000010;
							};
						namespace fCC2P { // Field Description
							constexpr Reg Lsb = 5;
							constexpr Reg FieldMask = 0x00000020;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							constexpr Reg Value_activeHighOutput = 0x0;
							constexpr Reg ValueMask_activeHighOutput = 0x00000000;
							constexpr Reg Value_nonInvertedInput = 0x0;
							constexpr Reg ValueMask_nonInvertedInput = 0x00000000;
							constexpr Reg Value_activeLowOutput = 0x1;
							constexpr Reg ValueMask_activeLowOutput = 0x00000020;
							constexpr Reg Value_invertedInput = 0x1;
							constexpr Reg ValueMask_invertedInput = 0x00000020;
							};
						namespace fCC2NE { // Field Description
							constexpr Reg Lsb = 6;
							constexpr Reg FieldMask = 0x00000040;
							};
						namespace fCC2NP { // Field Description
							constexpr Reg Lsb = 7;
							constexpr Reg FieldMask = 0x00000080;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							constexpr Reg Value_output = 0x0;
							constexpr Reg ValueMask_output = 0x00000000;
							constexpr Reg Value_input = 0x1;
							constexpr Reg ValueMask_input = 0x00000080;
							};
						namespace fCC3E { // Field Description
							constexpr Reg Lsb = 8;
							constexpr Reg FieldMask = 0x00000100;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							constexpr Reg Value_disable = 0x0;
							constexpr Reg ValueMask_disable = 0x00000000;
							constexpr Reg Value_disabled = 0x0;
							constexpr Reg ValueMask_disabled = 0x00000000;
							constexpr Reg Value_enable = 0x1;
							constexpr Reg ValueMask_enable = 0x00000100;
							constexpr Reg Value_enabled = 0x1;
							constexpr Reg ValueMask_enabled = 0x00000100;
							};
						namespace fCC3P { // Field Description
							constexpr Reg Lsb = 9;
							constexpr Reg FieldMask = 0x00000200;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							constexpr Reg Value_activeHighOutput = 0x0;
							constexpr Reg ValueMask_activeHighOutput = 0x00000000;
							constexpr Reg Value_nonInvertedInput = 0x0;
							constexpr Reg ValueMask_nonInvertedInput = 0x00000000;
							constexpr Reg Value_activeLowOutput = 0x1;
							constexpr Reg ValueMask_activeLowOutput = 0x00000200;
							constexpr Reg Value_invertedInput = 0x1;
							constexpr Reg ValueMask_invertedInput = 0x00000200;
							};
						namespace fCC3NE { // Field Description
							constexpr Reg Lsb = 10;
							constexpr Reg FieldMask = 0x00000400;
							};
						namespace fCC3NP { // Field Description
							constexpr Reg Lsb = 11;
							constexpr Reg FieldMask = 0x00000800;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							constexpr Reg Value_output = 0x0;
							constexpr Reg ValueMask_output = 0x00000000;
							constexpr Reg Value_input = 0x1;
							constexpr Reg ValueMask_input = 0x00000800;
							};
						namespace fCC4E { // Field Description
							constexpr Reg Lsb = 12;
							constexpr Reg FieldMask = 0x00001000;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							constexpr Reg Value_disable = 0x0;
							constexpr Reg ValueMask_disable = 0x00000000;
							constexpr Reg Value_disabled = 0x0;
							constexpr Reg ValueMask_disabled = 0x00000000;
							constexpr Reg Value_enable = 0x1;
							constexpr Reg ValueMask_enable = 0x00001000;
							constexpr Reg Value_enabled = 0x1;
							constexpr Reg ValueMask_enabled = 0x00001000;
							};
						namespace fCC4P { // Field Description
							constexpr Reg Lsb = 13;
							constexpr Reg FieldMask = 0x00002000;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							constexpr Reg Value_activeHighOutput = 0x0;
							constexpr Reg ValueMask_activeHighOutput = 0x00000000;
							constexpr Reg Value_nonInvertedInput = 0x0;
							constexpr Reg ValueMask_nonInvertedInput = 0x00000000;
							constexpr Reg Value_activeLowOutput = 0x1;
							constexpr Reg ValueMask_activeLowOutput = 0x00002000;
							constexpr Reg Value_invertedInput = 0x1;
							constexpr Reg ValueMask_invertedInput = 0x00002000;
							};
						namespace fCC4NP { // Field Description
							constexpr Reg Lsb = 15;
							constexpr Reg FieldMask = 0x00008000;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							constexpr Reg Value_output = 0x0;
							constexpr Reg ValueMask_output = 0x00000000;
							constexpr Reg Value_input = 0x1;
							constexpr Reg ValueMask_input = 0x00008000;
							};
						namespace fCC5E { // Field Description
							constexpr Reg Lsb = 16;
							constexpr Reg FieldMask = 0x00010000;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							constexpr Reg Value_disable = 0x0;
							constexpr Reg ValueMask_disable = 0x00000000;
							constexpr Reg Value_disabled = 0x0;
							constexpr Reg ValueMask_disabled = 0x00000000;
							constexpr Reg Value_enable = 0x1;
							constexpr Reg ValueMask_enable = 0x00010000;
							constexpr Reg Value_enabled = 0x1;
							constexpr Reg ValueMask_enabled = 0x00010000;
							};
						namespace fCC5P { // Field Description
							constexpr Reg Lsb = 17;
							constexpr Reg FieldMask = 0x00020000;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							constexpr Reg Value_activeHighOutput = 0x0;
							constexpr Reg ValueMask_activeHighOutput = 0x00000000;
							constexpr Reg Value_nonInvertedInput = 0x0;
							constexpr Reg ValueMask_nonInvertedInput = 0x00000000;
							constexpr Reg Value_activeLowOutput = 0x1;
							constexpr Reg ValueMask_activeLowOutput = 0x00020000;
							constexpr Reg Value_invertedInput = 0x1;
							constexpr Reg ValueMask_invertedInput = 0x00020000;
							};
						namespace fCC6E { // Field Description
							constexpr Reg Lsb = 20;
							constexpr Reg FieldMask = 0x00100000;
							};
						namespace fCC6P { // Field Description
							constexpr Reg Lsb = 21;
							constexpr Reg FieldMask = 0x00200000;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							constexpr Reg Value_output = 0x0;
							constexpr Reg ValueMask_output = 0x00000000;
							constexpr Reg Value_input = 0x1;
							constexpr Reg ValueMask_input = 0x00200000;
							};
						};
					namespace rCNT { // Register description
						typedef uint32_t	Reg;
						constexpr Reg	valueAfterReset = 0x00000000;
						namespace fCNT34 { // Field Description
							constexpr Reg Lsb = 0;
							constexpr Reg FieldMask = 0x0000FFFF;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							};
						namespace fCNT25 { // Field Description
							constexpr Reg Lsb = 0;
							constexpr Reg FieldMask = 0xFFFFFFFF;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							};
						namespace fUIFCPY { // Field Description
							constexpr Reg Lsb = 31;
							constexpr Reg FieldMask = 0x80000000;
							};
						};
					namespace rPSC { // Register description
						typedef uint32_t	Reg;
						constexpr Reg	valueAfterReset = 0x00000000;
						namespace fPSC { // Field Description
							constexpr Reg Lsb = 0;
							constexpr Reg FieldMask = 0x0000FFFF;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							};
						};
					namespace rARR { // Register description
						typedef uint32_t	Reg;
						constexpr Reg	valueAfterReset = 0xFFFFFFFF;
						namespace fARR34 { // Field Description
							constexpr Reg Lsb = 0;
							constexpr Reg FieldMask = 0x0000FFFF;
							constexpr Reg Value_valueAfterReset = 0xFFFF;
							constexpr Reg ValueMask_valueAfterReset = 0x0000FFFF;
							};
						namespace fARR25 { // Field Description
							constexpr Reg Lsb = 0;
							constexpr Reg FieldMask = 0xFFFFFFFF;
							constexpr Reg Value_valueAfterReset = 0xFFFFFFFF;
							constexpr Reg ValueMask_valueAfterReset = 0xFFFFFFFF;
							};
						};
					namespace rCCR { // Register description
						typedef uint32_t	Reg;
						constexpr Reg	valueAfterReset = 0x00000000;
						namespace fCCR34 { // Field Description
							constexpr Reg Lsb = 0;
							constexpr Reg FieldMask = 0x0000FFFF;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							};
						namespace fCCR25 { // Field Description
							constexpr Reg Lsb = 0;
							constexpr Reg FieldMask = 0x0000FFFF;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							};
						};
					namespace rDCR { // Register description
						typedef uint32_t	Reg;
						constexpr Reg	valueAfterReset = 0x00000000;
						namespace fDBA { // Field Description
							constexpr Reg Lsb = 0;
							constexpr Reg FieldMask = 0x0000001F;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							constexpr Reg Value_vCR1 = 0x0;
							constexpr Reg ValueMask_vCR1 = 0x00000000;
							constexpr Reg Value_vCR2 = 0x1;
							constexpr Reg ValueMask_vCR2 = 0x00000001;
							constexpr Reg Value_vSMCR = 0x2;
							constexpr Reg ValueMask_vSMCR = 0x00000002;
							constexpr Reg Value_vDIER = 0x3;
							constexpr Reg ValueMask_vDIER = 0x00000003;
							constexpr Reg Value_vSR = 0x4;
							constexpr Reg ValueMask_vSR = 0x00000004;
							constexpr Reg Value_vEGR = 0x5;
							constexpr Reg ValueMask_vEGR = 0x00000005;
							constexpr Reg Value_vCCMR1 = 0x6;
							constexpr Reg ValueMask_vCCMR1 = 0x00000006;
							constexpr Reg Value_vCCMR2 = 0x7;
							constexpr Reg ValueMask_vCCMR2 = 0x00000007;
							constexpr Reg Value_vCCER = 0x8;
							constexpr Reg ValueMask_vCCER = 0x00000008;
							constexpr Reg Value_vCNT = 0x9;
							constexpr Reg ValueMask_vCNT = 0x00000009;
							constexpr Reg Value_vPSC = 0xA;
							constexpr Reg ValueMask_vPSC = 0x0000000A;
							constexpr Reg Value_vAAR = 0xB;
							constexpr Reg ValueMask_vAAR = 0x0000000B;
							constexpr Reg Value_vCCR1 = 0xD;
							constexpr Reg ValueMask_vCCR1 = 0x0000000D;
							constexpr Reg Value_vCCR2 = 0xE;
							constexpr Reg ValueMask_vCCR2 = 0x0000000E;
							constexpr Reg Value_vCCR3 = 0xF;
							constexpr Reg ValueMask_vCCR3 = 0x0000000F;
							constexpr Reg Value_vCCR4 = 0x10;
							constexpr Reg ValueMask_vCCR4 = 0x00000010;
							constexpr Reg Value_vDCR = 0x12;
							constexpr Reg ValueMask_vDCR = 0x00000012;
							};
						namespace fDBL { // Field Description
							constexpr Reg Lsb = 8;
							constexpr Reg FieldMask = 0x00001F00;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							constexpr Reg Value_v1Transfer = 0x0;
							constexpr Reg ValueMask_v1Transfer = 0x00000000;
							constexpr Reg Value_v2Transfers = 0x1;
							constexpr Reg ValueMask_v2Transfers = 0x00000100;
							constexpr Reg Value_v3Transfers = 0x2;
							constexpr Reg ValueMask_v3Transfers = 0x00000200;
							constexpr Reg Value_v4Transfers = 0x3;
							constexpr Reg ValueMask_v4Transfers = 0x00000300;
							constexpr Reg Value_v5Transfers = 0x4;
							constexpr Reg ValueMask_v5Transfers = 0x00000400;
							constexpr Reg Value_v6Transfers = 0x5;
							constexpr Reg ValueMask_v6Transfers = 0x00000500;
							constexpr Reg Value_v7Transfers = 0x6;
							constexpr Reg ValueMask_v7Transfers = 0x00000600;
							constexpr Reg Value_v8Transfers = 0x7;
							constexpr Reg ValueMask_v8Transfers = 0x00000700;
							constexpr Reg Value_v9Transfers = 0x8;
							constexpr Reg ValueMask_v9Transfers = 0x00000800;
							constexpr Reg Value_v10Transfers = 0x9;
							constexpr Reg ValueMask_v10Transfers = 0x00000900;
							constexpr Reg Value_v11Transfers = 0xA;
							constexpr Reg ValueMask_v11Transfers = 0x00000A00;
							constexpr Reg Value_v12Transfers = 0xB;
							constexpr Reg ValueMask_v12Transfers = 0x00000B00;
							constexpr Reg Value_v13Transfers = 0xC;
							constexpr Reg ValueMask_v13Transfers = 0x00000C00;
							constexpr Reg Value_v14Transfers = 0xD;
							constexpr Reg ValueMask_v14Transfers = 0x00000D00;
							constexpr Reg Value_v15Transfers = 0xE;
							constexpr Reg ValueMask_v15Transfers = 0x00000E00;
							constexpr Reg Value_v16Transfers = 0xF;
							constexpr Reg ValueMask_v16Transfers = 0x00000F00;
							constexpr Reg Value_v17Transfers = 0x10;
							constexpr Reg ValueMask_v17Transfers = 0x00001000;
							constexpr Reg Value_v18Transfers = 0x11;
							constexpr Reg ValueMask_v18Transfers = 0x00001100;
							};
						};
					namespace rDMAR { // Register description
						typedef uint32_t	Reg;
						constexpr Reg	valueAfterReset = 0x00000000;
						namespace fDMAB { // Field Description
							constexpr Reg Lsb = 0;
							constexpr Reg FieldMask = 0x0000FFFF;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							};
						};
					namespace rRCR { // Register description
						typedef uint32_t	Reg;
						namespace fREP { // Field Description
							constexpr Reg Lsb = 0;
							constexpr Reg FieldMask = 0x000000FF;
							};
						};
					namespace rBDTR { // Register description
						typedef uint32_t	Reg;
						namespace fDTG { // Field Description
							constexpr Reg Lsb = 0;
							constexpr Reg FieldMask = 0x000000FF;
							};
						namespace fLOCK { // Field Description
							constexpr Reg Lsb = 8;
							constexpr Reg FieldMask = 0x00000300;
							};
						namespace fOSSI { // Field Description
							constexpr Reg Lsb = 10;
							constexpr Reg FieldMask = 0x00000400;
							};
						namespace fOSSR { // Field Description
							constexpr Reg Lsb = 11;
							constexpr Reg FieldMask = 0x00000800;
							};
						namespace fBKE { // Field Description
							constexpr Reg Lsb = 12;
							constexpr Reg FieldMask = 0x00001000;
							};
						namespace fBKP { // Field Description
							constexpr Reg Lsb = 13;
							constexpr Reg FieldMask = 0x00002000;
							};
						namespace fAOE { // Field Description
							constexpr Reg Lsb = 14;
							constexpr Reg FieldMask = 0x00004000;
							};
						namespace fMOE { // Field Description
							constexpr Reg Lsb = 15;
							constexpr Reg FieldMask = 0x00008000;
							};
						namespace fBKF { // Field Description
							constexpr Reg Lsb = 16;
							constexpr Reg FieldMask = 0x000F0000;
							};
						namespace fBK2F { // Field Description
							constexpr Reg Lsb = 20;
							constexpr Reg FieldMask = 0x00F00000;
							};
						namespace fBK2E { // Field Description
							constexpr Reg Lsb = 24;
							constexpr Reg FieldMask = 0x01000000;
							};
						namespace fBK2P { // Field Description
							constexpr Reg Lsb = 25;
							constexpr Reg FieldMask = 0x02000000;
							};
						};
					namespace rCCR5 { // Register description
						typedef uint32_t	Reg;
						namespace fCCR5 { // Field Description
							constexpr Reg Lsb = 0;
							constexpr Reg FieldMask = 0x0000FFFF;
							};
						namespace fGC5C1 { // Field Description
							constexpr Reg Lsb = 29;
							constexpr Reg FieldMask = 0x20000000;
							};
						namespace fGC5C2 { // Field Description
							constexpr Reg Lsb = 30;
							constexpr Reg FieldMask = 0x40000000;
							};
						namespace fGC5C3 { // Field Description
							constexpr Reg Lsb = 31;
							constexpr Reg FieldMask = 0x80000000;
							};
						};
					namespace rCRR6 { // Register description
						typedef uint32_t	Reg;
						namespace fCCR6 { // Field Description
							constexpr Reg Lsb = 0;
							constexpr Reg FieldMask = 0x0000FFFF;
							};
						};
					namespace rAF1 { // Register description
						typedef uint32_t	Reg;
						constexpr Reg	valueAfterReset = 0x00000000;
						namespace fBKINE { // Field Description
							constexpr Reg Lsb = 0;
							constexpr Reg FieldMask = 0x00000001;
							};
						namespace fBKCMP1E { // Field Description
							constexpr Reg Lsb = 1;
							constexpr Reg FieldMask = 0x00000002;
							};
						namespace fBKCMP2E { // Field Description
							constexpr Reg Lsb = 2;
							constexpr Reg FieldMask = 0x00000004;
							};
						namespace fBKDF1BK0E { // Field Description
							constexpr Reg Lsb = 8;
							constexpr Reg FieldMask = 0x00000100;
							};
						namespace fBKINP { // Field Description
							constexpr Reg Lsb = 9;
							constexpr Reg FieldMask = 0x00000200;
							};
						namespace fBKCMP1P { // Field Description
							constexpr Reg Lsb = 10;
							constexpr Reg FieldMask = 0x00000400;
							};
						namespace fBKCMP2P { // Field Description
							constexpr Reg Lsb = 11;
							constexpr Reg FieldMask = 0x00000800;
							};
						namespace fETRSEL { // Field Description
							constexpr Reg Lsb = 14;
							constexpr Reg FieldMask = 0x0003C000;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							namespace xTIM2 { // Field Description
								constexpr Reg Lsb = 14;
								constexpr Reg FieldMask = 0x0003C000;
								constexpr Reg Value_ETR = 0x0;
								constexpr Reg ValueMask_ETR = 0x00000000;
								constexpr Reg Value_COMP1 = 0x1;
								constexpr Reg ValueMask_COMP1 = 0x00004000;
								constexpr Reg Value_COMP2 = 0x2;
								constexpr Reg ValueMask_COMP2 = 0x00008000;
								constexpr Reg Value_LSE = 0x3;
								constexpr Reg ValueMask_LSE = 0x0000C000;
								constexpr Reg Value_SAI1_FS_A = 0x4;
								constexpr Reg ValueMask_SAI1_FS_A = 0x00010000;
								constexpr Reg Value_SAI1_FS_B = 0x5;
								constexpr Reg ValueMask_SAI1_FS_B = 0x00014000;
								};
							namespace xTIM3 { // Field Description
								constexpr Reg Lsb = 14;
								constexpr Reg FieldMask = 0x0003C000;
								constexpr Reg Value_ETR = 0x0;
								constexpr Reg ValueMask_ETR = 0x00000000;
								};
							namespace xTIM4 { // Field Description
								constexpr Reg Lsb = 14;
								constexpr Reg FieldMask = 0x0003C000;
								constexpr Reg Value_ETR = 0x0;
								constexpr Reg ValueMask_ETR = 0x00000000;
								};
							namespace xTIM5 { // Field Description
								constexpr Reg Lsb = 14;
								constexpr Reg FieldMask = 0x0003C000;
								constexpr Reg Value_ETR = 0x0;
								constexpr Reg ValueMask_ETR = 0x00000000;
								constexpr Reg Value_SAI1_FS_A = 0x1;
								constexpr Reg ValueMask_SAI1_FS_A = 0x00004000;
								constexpr Reg Value_SAI1_FS_B = 0x2;
								constexpr Reg ValueMask_SAI1_FS_B = 0x00008000;
								};
							};
						};
					namespace rAF2 { // Register description
						typedef uint32_t	Reg;
						namespace fBK2INE { // Field Description
							constexpr Reg Lsb = 0;
							constexpr Reg FieldMask = 0x00000001;
							};
						namespace fBK2CMP1E { // Field Description
							constexpr Reg Lsb = 1;
							constexpr Reg FieldMask = 0x00000002;
							};
						namespace fBK2CMP2E { // Field Description
							constexpr Reg Lsb = 2;
							constexpr Reg FieldMask = 0x00000004;
							};
						namespace fBK2DF1BK1E { // Field Description
							constexpr Reg Lsb = 8;
							constexpr Reg FieldMask = 0x00000100;
							};
						namespace fBK2INP { // Field Description
							constexpr Reg Lsb = 9;
							constexpr Reg FieldMask = 0x00000200;
							};
						namespace fBK2CMP1P { // Field Description
							constexpr Reg Lsb = 10;
							constexpr Reg FieldMask = 0x00000400;
							};
						namespace fBK2CMP2P { // Field Description
							constexpr Reg Lsb = 11;
							constexpr Reg FieldMask = 0x00000800;
							};
						};
					namespace rTISEL { // Register description
						typedef uint32_t	Reg;
						constexpr Reg	valueAfterReset = 0x00000000;
						namespace fTI1SEL { // Field Description
							constexpr Reg Lsb = 0;
							constexpr Reg FieldMask = 0x0000000F;
							constexpr Reg Value_CH1 = 0x0;
							constexpr Reg ValueMask_CH1 = 0x00000000;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							namespace xTIM2 { // Field Description
								constexpr Reg Lsb = 0;
								constexpr Reg FieldMask = 0x0000000F;
								constexpr Reg Value_CH1 = 0x0;
								constexpr Reg ValueMask_CH1 = 0x00000000;
								};
							namespace xTIM3 { // Field Description
								constexpr Reg Lsb = 0;
								constexpr Reg FieldMask = 0x0000000F;
								constexpr Reg Value_CH1 = 0x0;
								constexpr Reg ValueMask_CH1 = 0x00000000;
								constexpr Reg Value_COMP1 = 0x1;
								constexpr Reg ValueMask_COMP1 = 0x00000001;
								constexpr Reg Value_COMP2 = 0x2;
								constexpr Reg ValueMask_COMP2 = 0x00000002;
								constexpr Reg Value_COMP1_or_COMP2 = 0x2;
								constexpr Reg ValueMask_COMP1_or_COMP2 = 0x00000002;
								};
							namespace xTIM4 { // Field Description
								constexpr Reg Lsb = 0;
								constexpr Reg FieldMask = 0x0000000F;
								constexpr Reg Value_CH1 = 0x0;
								constexpr Reg ValueMask_CH1 = 0x00000000;
								};
							namespace xTIM5 { // Field Description
								constexpr Reg Lsb = 0;
								constexpr Reg FieldMask = 0x0000000F;
								constexpr Reg Value_CH1 = 0x0;
								constexpr Reg ValueMask_CH1 = 0x00000000;
								constexpr Reg Value_fdcan1_tmp = 0x1;
								constexpr Reg ValueMask_fdcan1_tmp = 0x00000001;
								constexpr Reg Value_fdcan1_rtp = 0x2;
								constexpr Reg ValueMask_fdcan1_rtp = 0x00000002;
								};
							};
						namespace fTI2SEL { // Field Description
							constexpr Reg Lsb = 8;
							constexpr Reg FieldMask = 0x00000F00;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							constexpr Reg Value_CH2 = 0x0;
							constexpr Reg ValueMask_CH2 = 0x00000000;
							namespace xTIM2 { // Field Description
								constexpr Reg Lsb = 8;
								constexpr Reg FieldMask = 0x00000F00;
								constexpr Reg Value_CH2 = 0x0;
								constexpr Reg ValueMask_CH2 = 0x00000000;
								};
							namespace xTIM3 { // Field Description
								constexpr Reg Lsb = 8;
								constexpr Reg FieldMask = 0x00000F00;
								constexpr Reg Value_CH2 = 0x0;
								constexpr Reg ValueMask_CH2 = 0x00000000;
								};
							namespace xTIM4 { // Field Description
								constexpr Reg Lsb = 8;
								constexpr Reg FieldMask = 0x00000F00;
								constexpr Reg Value_CH2 = 0x0;
								constexpr Reg ValueMask_CH2 = 0x00000000;
								};
							namespace xTIM5 { // Field Description
								constexpr Reg Lsb = 8;
								constexpr Reg FieldMask = 0x00000F00;
								constexpr Reg Value_CH2 = 0x0;
								constexpr Reg ValueMask_CH2 = 0x00000000;
								};
							};
						namespace fTI3SEL { // Field Description
							constexpr Reg Lsb = 16;
							constexpr Reg FieldMask = 0x000F0000;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							constexpr Reg Value_CH3 = 0x0;
							constexpr Reg ValueMask_CH3 = 0x00000000;
							namespace xTIM2 { // Field Description
								constexpr Reg Lsb = 16;
								constexpr Reg FieldMask = 0x000F0000;
								constexpr Reg Value_CH3 = 0x0;
								constexpr Reg ValueMask_CH3 = 0x00000000;
								};
							namespace xTIM3 { // Field Description
								constexpr Reg Lsb = 16;
								constexpr Reg FieldMask = 0x000F0000;
								constexpr Reg Value_CH3 = 0x0;
								constexpr Reg ValueMask_CH3 = 0x00000000;
								};
							namespace xTIM4 { // Field Description
								constexpr Reg Lsb = 16;
								constexpr Reg FieldMask = 0x000F0000;
								constexpr Reg Value_CH3 = 0x0;
								constexpr Reg ValueMask_CH3 = 0x00000000;
								};
							namespace xTIM5 { // Field Description
								constexpr Reg Lsb = 16;
								constexpr Reg FieldMask = 0x000F0000;
								constexpr Reg Value_CH3 = 0x0;
								constexpr Reg ValueMask_CH3 = 0x00000000;
								};
							};
						namespace fTI4SEL { // Field Description
							constexpr Reg Lsb = 24;
							constexpr Reg FieldMask = 0x0F000000;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							constexpr Reg Value_CH4 = 0x0;
							constexpr Reg ValueMask_CH4 = 0x00000000;
							namespace xTIM2 { // Field Description
								constexpr Reg Lsb = 24;
								constexpr Reg FieldMask = 0x0F000000;
								constexpr Reg Value_CH4 = 0x0;
								constexpr Reg ValueMask_CH4 = 0x00000000;
								constexpr Reg Value_COMP1 = 0x1;
								constexpr Reg ValueMask_COMP1 = 0x01000000;
								constexpr Reg Value_COMP2 = 0x2;
								constexpr Reg ValueMask_COMP2 = 0x02000000;
								constexpr Reg Value_COMP1_or_COMP2 = 0x2;
								constexpr Reg ValueMask_COMP1_or_COMP2 = 0x02000000;
								};
							namespace xTIM3 { // Field Description
								constexpr Reg Lsb = 24;
								constexpr Reg FieldMask = 0x0F000000;
								constexpr Reg Value_CH4 = 0x0;
								constexpr Reg ValueMask_CH4 = 0x00000000;
								};
							namespace xTIM4 { // Field Description
								constexpr Reg Lsb = 24;
								constexpr Reg FieldMask = 0x0F000000;
								constexpr Reg Value_CH4 = 0x0;
								constexpr Reg ValueMask_CH4 = 0x00000000;
								};
							namespace xTIM5 { // Field Description
								constexpr Reg Lsb = 24;
								constexpr Reg FieldMask = 0x0F000000;
								constexpr Reg Value_CH4 = 0x0;
								constexpr Reg ValueMask_CH4 = 0x00000000;
								};
							};
						};
					}
				}
			}
		}
	}
#endif
