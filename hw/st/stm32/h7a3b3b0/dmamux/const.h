/*
   Copyright (C) 2024 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_hw_st_stm32_h7a3b3b0_dmamux_consth_
#define _oscl_hw_st_stm32_h7a3b3b0_dmamux_consth_
#include <stdint.h>

/** */
namespace Oscl {
/** */
namespace St {
/** */
namespace Stm32 {
/** */
namespace H7A3B3B0 {

/** */
namespace pDMAMUX1 {
	/** */
	namespace Input {
		/*	 DMAMUX1: assignment of multiplexer inputs to resources
		 */
		constexpr uint32_t	req_gen0		= 1;
		constexpr uint32_t	vUSART2_RX		= 43;
		constexpr uint32_t	vSPI5_RX		= 85;
		constexpr uint32_t	req_gen1		= 2;
		constexpr uint32_t	vUSART2_TX		= 44;
		constexpr uint32_t	vSPI5_TX		= 86;
		constexpr uint32_t	req_gen2		= 3;
		constexpr uint32_t	vUSART3_RX		= 45;
		constexpr uint32_t	vSAI1_A			= 87;
		constexpr uint32_t	req_gen3		= 4;
		constexpr uint32_t	vUSART3_TX		= 46;
		constexpr uint32_t	vSAI1_B			= 88;
		constexpr uint32_t	req_gen4		= 5;
		constexpr uint32_t	vTIM8_CH1		= 47;
		constexpr uint32_t	vSAI2_A			= 89;
		constexpr uint32_t	req_gen5		= 6;
		constexpr uint32_t	vTIM8_CH2		= 48;
		constexpr uint32_t	vSAI2_B			= 90;
		constexpr uint32_t	req_gen6		= 7;
		constexpr uint32_t	vTIM8_CH3		= 49;
		constexpr uint32_t	vSWPMI_RX		= 91;
		constexpr uint32_t	req_gen7		= 8;
		constexpr uint32_t	vTIM8_CH4		= 50;
		constexpr uint32_t	vSWPMI_TX		= 92;
		constexpr uint32_t	vADC1			= 9;
		constexpr uint32_t	vTIM8_UP		= 51;
		constexpr uint32_t	vSPDIFRX_DT		= 93;
		constexpr uint32_t	vADC2			= 10;
		constexpr uint32_t	vTIM8_TRIG		= 52;
		constexpr uint32_t	vSPDIFRX_CS		= 94;
		constexpr uint32_t	vTIM1_CH1		= 11;
		constexpr uint32_t	vTIM8_COM		= 53;
		constexpr uint32_t	vTIM1_CH2		= 12;
		constexpr uint32_t	vTIM1_CH3		= 13;
		constexpr uint32_t	vTIM5_CH1		= 55;
		constexpr uint32_t	vTIM1_CH4		= 14;
		constexpr uint32_t	vTIM5_CH2		= 56;
		constexpr uint32_t	vTIM1_UP		= 15;
		constexpr uint32_t	vTIM5_CH3		= 57;
		constexpr uint32_t	vTIM1_TRIG		= 16;
		constexpr uint32_t	vTIM5_CH4		= 58;
		constexpr uint32_t	vTIM1_COM		= 17;
		constexpr uint32_t	vTIM5_UP		= 59;
		constexpr uint32_t	vDFSDM1_dma0	= 101;
		constexpr uint32_t	vTIM2_CH1		= 18;
		constexpr uint32_t	vTIM5_TRIG		= 60;
		constexpr uint32_t	vDFSDM1_dma1	= 102;
		constexpr uint32_t	vTIM2_CH2		= 19;
		constexpr uint32_t	vSPI3_RX		= 61;
		constexpr uint32_t	vDFSDM1_dma2	= 103;
		constexpr uint32_t	vTIM2_CH3		= 20;
		constexpr uint32_t	vSPI3_TX		= 62;
		constexpr uint32_t	vDFSDM1_dma3	= 104;
		constexpr uint32_t	vTIM2_CH4		= 21;
		constexpr uint32_t	vUART4_RX		= 63;
		constexpr uint32_t	vTIM15_CH1		= 105;
		constexpr uint32_t	vTIM2_UP		= 22;
		constexpr uint32_t	vUART4_TX		= 64;
		constexpr uint32_t	vTIM15_UP		= 106;
		constexpr uint32_t	vTIM3_CH1		= 23;
		constexpr uint32_t	vUART5_RX		= 65;
		constexpr uint32_t	vTIM15_TRIG		= 107;
		constexpr uint32_t	vTIM3_CH2		= 24;
		constexpr uint32_t	vUART5_TX		= 66;
		constexpr uint32_t	vTIM15_COM		= 108;
		constexpr uint32_t	vTIM3_CH3		= 25;
		constexpr uint32_t	vDAC1_out1		= 67;
		constexpr uint32_t	vTIM16_CH1		= 109;
		constexpr uint32_t	vTIM3_CH4		= 26;
		constexpr uint32_t	vDAC1_out2		= 68;
		constexpr uint32_t	vTIM16_UP		= 110;
		constexpr uint32_t	vTIM3_UP		= 27;
		constexpr uint32_t	vTIM6_UP		= 69;
		constexpr uint32_t	vTIM17_CH1		= 111;
		constexpr uint32_t	vTIM3_TRIG		= 28;
		constexpr uint32_t	vTIM7_UP		= 70;
		constexpr uint32_t	vTIM17_UP		= 112;
		constexpr uint32_t	vTIM4_CH1		= 29;
		constexpr uint32_t	vUSART6_RX		= 71;
		constexpr uint32_t	vTIM4_CH2		= 30;
		constexpr uint32_t	vUSART6_TX		= 72;
		constexpr uint32_t	vTIM4_CH3		= 31;
		constexpr uint32_t	vI2C3_RX		= 73;
		constexpr uint32_t	vTIM4_UP		= 32;
		constexpr uint32_t	vI2C3_TX		= 74;
		constexpr uint32_t	vUART9_RX		= 116;
		constexpr uint32_t	vI2C1_RX		= 33;
		constexpr uint32_t	vDCMI_PSSI		= 75;
		constexpr uint32_t	vUART9_TX		= 117;
		constexpr uint32_t	vI2C1_TX		= 34;
		constexpr uint32_t	vCRYP_IN		= 76;
		constexpr uint32_t	vUSART10_RX		= 118;
		constexpr uint32_t	vI2C2_RX		= 35;
		constexpr uint32_t	vCRYP_OUT		= 77;
		constexpr uint32_t	vUSART10_TX		= 119;
		constexpr uint32_t	vI2C2_TX		= 36;
		constexpr uint32_t	vHASH_IN		= 78;
		constexpr uint32_t	vSPI1_RX		= 37;
		constexpr uint32_t	vUART7_RX		= 79;
		constexpr uint32_t	vSPI1_TX		= 38;
		constexpr uint32_t	vUART7_TX		= 80;
		constexpr uint32_t	vSPI2_RX		= 39;
		constexpr uint32_t	vUART8_RX		= 81;
		constexpr uint32_t	vSPI2_TX		= 40;
		constexpr uint32_t	vUART8_TX		= 82;
		constexpr uint32_t	vUSART1_RX		= 41;
		constexpr uint32_t	vSPI4_RX		= 83;
		constexpr uint32_t	vUSART1_TX		= 42;
		constexpr uint32_t	vSPI4_TX		= 84;
		}
	namespace Trigger {
		constexpr uint32_t	evt0		= 0;
		constexpr uint32_t	lptim2_out	= 4;
		constexpr uint32_t	evt1		= 1;
		constexpr uint32_t	lptim3_out	= 5;
		constexpr uint32_t	evt2		= 2;
		constexpr uint32_t	extit0		= 6;
		constexpr uint32_t	lptim1_out	= 3;
		constexpr uint32_t	vTIM12_TRGO	= 7;
		}
	namespace SyncInput {
		constexpr uint32_t	evt0		= 0;
		constexpr uint32_t	lptim2_out	= 4;
		constexpr uint32_t	evt1		= 1;
		constexpr uint32_t	lptim3_out	= 5;
		constexpr uint32_t	evt2		= 2;
		constexpr uint32_t	extit0		= 6;
		constexpr uint32_t	lptim1_out	= 3;
		constexpr uint32_t	vTIM12_TRGO	= 7;
		}
	namespace Channel {
		constexpr uint32_t	dma1Stream0		= 0;
		constexpr uint32_t	dma1Stream1		= 1;
		constexpr uint32_t	dma1Stream2		= 2;
		constexpr uint32_t	dma1Stream3		= 3;
		constexpr uint32_t	dma1Stream4		= 4;
		constexpr uint32_t	dma1Stream5		= 5;
		constexpr uint32_t	dma1Stream6		= 6;
		constexpr uint32_t	dma1Stream7		= 7;
		constexpr uint32_t	dma2Stream0		= 8;
		constexpr uint32_t	dma2Stream1		= 9;
		constexpr uint32_t	dma2Stream2		= 10;
		constexpr uint32_t	dma2Stream3		= 11;
		constexpr uint32_t	dma2Stream4		= 12;
		constexpr uint32_t	dma2Stream5		= 13;
		constexpr uint32_t	dma2Stream6		= 14;
		constexpr uint32_t	dma2Stream7		= 15;
		}
	}

// .,'bs/^\([0-9]*\) *\(.*\)/	constexpr uint32_t	v\2	= \1;/

/** */
namespace pDMAMUX2 {
	/** */
	namespace Input {
		constexpr uint32_t	req_gen0		= 1;
		constexpr uint32_t	vDAC2			= 17;
		constexpr uint32_t	req_gen1		= 2;
		constexpr uint32_t	DFSDM2_dma0		= 18;
		constexpr uint32_t	req_gen2		= 3;
		constexpr uint32_t	req_gen3		= 4;
		constexpr uint32_t	req_gen4		= 5;
		constexpr uint32_t	req_gen5		= 6;
		constexpr uint32_t	req_gen6		= 7;
		constexpr uint32_t	req_gen7		= 8;
		constexpr uint32_t	vLP_UART1_RX	= 9;
		constexpr uint32_t	vLP_UART1_TX	= 10;
		constexpr uint32_t	vSPI6_RX		= 11;
		constexpr uint32_t	vSPI6_TX		= 12;
		constexpr uint32_t	vI2C4_RX		= 13;
		constexpr uint32_t	vI2C4_TX		= 14;
	}
	
	namespace Trigger {
		constexpr uint32_t evt0						= 0;
		constexpr uint32_t Spi6_it_async			= 16;
		constexpr uint32_t evt1						= 1;
		constexpr uint32_t Comp1_out				= 17;
		constexpr uint32_t evt2						= 2;
		constexpr uint32_t Comp2_out				= 18;
		constexpr uint32_t evt3						= 3;
		constexpr uint32_t RTC_wkup					= 19;
		constexpr uint32_t evt4						= 4;
		constexpr uint32_t Syscfg_exti0_mux			= 20;
		constexpr uint32_t evt5						= 5;
		constexpr uint32_t Syscfg_exti2_mux			= 21;
		constexpr uint32_t evt6						= 6;
		constexpr uint32_t I2c4_it_event			= 22;
		constexpr uint32_t Lpuart1_it_R_WUP_ASYNC	= 7;
		constexpr uint32_t Spi6_it					= 23;
		constexpr uint32_t Lpuart1_it_T_WUP_ASYNC	= 8;
		constexpr uint32_t Lpuart1_it_T				= 24;
		constexpr uint32_t Lptim2_ait				= 9;
		constexpr uint32_t Lpuart1_it_R				= 25;
		constexpr uint32_t Lptim2_out				= 10;
		constexpr uint32_t Lptim3_ait				= 11;
		constexpr uint32_t Lptim3_out				= 12;
		constexpr uint32_t BDMA2_ch0_it				= 28;
		constexpr uint32_t BDMA2_ch1_it				= 29;
		constexpr uint32_t I2c4_it_async			= 15;
		}
	namespace SyncInput {
		constexpr uint32_t	ch0_evt					= 0;
		constexpr uint32_t	Lptim2_out				= 8;
		constexpr uint32_t	ch1_evt					= 1;
		constexpr uint32_t	Lptim3_out				= 9;
		constexpr uint32_t	ch2_evt					= 2;
		constexpr uint32_t	I2c4_it_async			= 10;
		constexpr uint32_t	ch3_evt					= 3;
		constexpr uint32_t	Spi6_it_async			= 11;
		constexpr uint32_t	ch4_evt					= 4;
		constexpr uint32_t	Comp1_out				= 12;
		constexpr uint32_t	ch5_evt					= 5;
		constexpr uint32_t	RTC_wkup				= 13;
		constexpr uint32_t	Lpuart1_it_R_WUP_ASYNC	= 6;
		constexpr uint32_t	Syscfg_exti0_mux		= 14;
		constexpr uint32_t	Lpuart1_it_T_WUP_ASYNC	= 7;
		constexpr uint32_t	Syscfg_exti2_mux		= 15;
		}

	namespace Channel {
		}
	}

}
}
}
}
#endif
