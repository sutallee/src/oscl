/*
   Copyright (C) 2024 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_hw_st_stm32_h7a3b3b0_nvich_
#define _oscl_hw_st_stm32_h7a3b3b0_nvich_
#include <stdint.h>

/** */
namespace Oscl {
/** */
namespace St {
/** */
namespace Stm32 {
/** */
namespace H7A3B3B0 {
/** */
namespace pNVIC {

// NVIC Priority Levels
/*	The STM32H7A3B3B0 SOCs have 16 programmable priority levels
	(4 bits of interrupt priority are used)

	PM0253
	Only the group priority determines preemption of interrupt exceptions.
	When the processor is executing an interrupt exception handler,
	another interrupt with the same group priority as the interrupt
	being handled does not preempt the handler,

	If multiple pending interrupts have the same group priority,
	the subpriority field determines the order in which they are processed.
	If multiple pending interrupts have the same group
	priority and subpriority, the interrupt with the lowest IRQ number
	is processed first.

	NOTE:
 */
/** */
static constexpr unsigned	nPriorities	= 16;
/** */
static constexpr unsigned	minPriority	= 0;
/** */
static constexpr unsigned	maxPriority	= 15;
/** */
static constexpr unsigned	maxInterrupts	= 154;

/** */
static constexpr unsigned	posWWDG						= 0;
/** */
static constexpr unsigned	posPVD_PVM					= 1;
/** */
static constexpr unsigned	posRTC_TAMP_STAMP_CSS_LSE	= 2;
/** */
static constexpr unsigned	posRTC_WKUP	= 3;
/** */
static constexpr unsigned	posFLASH	= 4;
/** */
static constexpr unsigned	posRCC	= 5;
/** */
static constexpr unsigned	posEXTI0	= 6;
/** */
static constexpr unsigned	posEXTI1	= 7;
/** */
static constexpr unsigned	posEXTI2	= 8;
/** */
static constexpr unsigned	posEXTI3	= 9;
/** */
static constexpr unsigned	posEXTI4	= 10;
/** */
static constexpr unsigned	posDMA1_STR0	= 11;
/** */
static constexpr unsigned	posDMA1_STR1	= 12;
/** */
static constexpr unsigned	posDMA1_STR2	= 13;
/** */
static constexpr unsigned	posDMA1_STR3	= 14;
/** */
static constexpr unsigned	posDMA1_STR4	= 15;
/** */
static constexpr unsigned	posDMA1_STR5	= 16;
/** */
static constexpr unsigned	posDMA1_STR6	= 17;
/** */
static constexpr unsigned	posADC1_2	= 18;
/** */
static constexpr unsigned	posFDCAN1_IT0	= 19;
/** */
static constexpr unsigned	posFDCAN2_IT0	= 20;
/** */
static constexpr unsigned	posFDCAN1_IT1	= 21;
/** */
static constexpr unsigned	posFDCAN2_IT1	= 22;
/** */
static constexpr unsigned	posEXTI9_5	= 23;
/** */
static constexpr unsigned	posTIM1_BRK	= 24;
/** */
static constexpr unsigned	posTIM1_UP	= 25;
/** */
static constexpr unsigned	posTIM1_TRG_COM	= 26;
/** */
static constexpr unsigned	posTIM1_CC	= 27;
/** */
static constexpr unsigned	posTIM2	= 28;
/** */
static constexpr unsigned	posTIM3	= 29;
/** */
static constexpr unsigned	posTIM4	= 30;
/** */
static constexpr unsigned	posI2C1_EV	= 31;
/** */
static constexpr unsigned	posI2C1_ER	= 32;
/** */
static constexpr unsigned	posI2C2_EV	= 33;
/** */
static constexpr unsigned	posI2C2_ER	= 34;
/** */
static constexpr unsigned	posSPI1	= 35;
/** */
static constexpr unsigned	posSPI2	= 36;
/** */
static constexpr unsigned	posUSART1	= 37;
/** */
static constexpr unsigned	posUSART2	= 38;
/** */
static constexpr unsigned	posUSART3	= 39;
/** */
static constexpr unsigned	posEXTI15_10	= 40;
/** */
static constexpr unsigned	posRTC_ALARM	= 41;
/** */
static constexpr unsigned	posDFSDM2	= 42;
/** */
static constexpr unsigned	posTIM8_BRK_TIM12	= 43;
/** */
static constexpr unsigned	posTIM8_UP_TIM13	= 44;
/** */
static constexpr unsigned	posTIM8_TRG_COM_TIM14	= 45;
/** */
static constexpr unsigned	posTIM8_CC	= 46;
/** */
static constexpr unsigned	posDMA1_STR7	= 47;
/** */
static constexpr unsigned	posFMC	= 48;
/** */
static constexpr unsigned	posSDMMC1	= 49;
/** */
static constexpr unsigned	posTIM5	= 50;
/** */
static constexpr unsigned	posSPI3	= 51;
/** */
static constexpr unsigned	posUART4	= 52;
/** */
static constexpr unsigned	posUART5	= 53;
/** */
static constexpr unsigned	posTIM6_DAC1	= 54;
/** */
static constexpr unsigned	posTIM7	= 55;
/** */
static constexpr unsigned	posDMA2_STR0	= 56;
/** */
static constexpr unsigned	posDMA2_STR1	= 57;
/** */
static constexpr unsigned	posDMA2_STR2	= 58;
/** */
static constexpr unsigned	posDMA2_STR3	= 59;
/** */
static constexpr unsigned	posDMA2_STR4	= 60;

/** */
static constexpr unsigned	posFDCAN_CAL	= 63;
/** */
static constexpr unsigned	posDFSDM1_FLT4	= 64;
/** */
static constexpr unsigned	posDFSDM1_FLT5	= 65;
/** */
static constexpr unsigned	posDFSDM1_FLT6	= 66;
/** */
static constexpr unsigned	posDFSDM1_FLT7	= 67;
/** */
static constexpr unsigned	posDMA2_STR5	= 68;
/** */
static constexpr unsigned	posDMA2_STR6	= 69;
/** */
static constexpr unsigned	posDMA2_STR7	= 70;
/** */
static constexpr unsigned	posUSART6	= 71;
/** */
static constexpr unsigned	posI2C3_EV	= 72;
/** */
static constexpr unsigned	posI2C3_ER	= 73;
/** */
static constexpr unsigned	posOTG_HS_EP1_OUT	= 74;
/** */
static constexpr unsigned	posOTG_HS_EP1_IN	= 75;
/** */
static constexpr unsigned	posOTG_HS_WKUP	= 76;
/** */
static constexpr unsigned	posOTG_HS	= 77;
/** */
static constexpr unsigned	posDCMI_PSSI	= 78;
/** */
static constexpr unsigned	posCRYP	= 79;
/** */
static constexpr unsigned	posHASH_RNG	= 80;
/** */
static constexpr unsigned	posFPU	= 81;
/** */
static constexpr unsigned	posUART7	= 82;
/** */
static constexpr unsigned	posUART8	= 83;
/** */
static constexpr unsigned	posSPI4	= 84;
/** */
static constexpr unsigned	posSPI5	= 85;
/** */
static constexpr unsigned	posSPI6	= 86;
/** */
static constexpr unsigned	posSAI1	= 87;
/** */
static constexpr unsigned	posLTDC	= 88;
/** */
static constexpr unsigned	posLTDC_ER	= 89;
/** */
static constexpr unsigned	posDMA2D	= 90;
/** */
static constexpr unsigned	posSAI2	= 91;
/** */
static constexpr unsigned	posOCTOSPI1	= 92;
/** */
static constexpr unsigned	posLPTIM1	= 93;
/** */
static constexpr unsigned	posCEC	= 94;
/** */
static constexpr unsigned	posI2C4_EV	= 95;
/** */
static constexpr unsigned	posI2C4_ER	= 96;
/** */
static constexpr unsigned	posSPDIFRX	= 97;

/** */
static constexpr unsigned	posDFSDM1_FLT0	= 110;
/** */
static constexpr unsigned	posDFSDM1_FLT1	= 111;
/** */
static constexpr unsigned	posDFSDM1_FLT2	= 112;
/** */
static constexpr unsigned	posDFSDM1_FLT3	= 113;

/** */
static constexpr unsigned	posSWPMI1	= 115;
/** */
static constexpr unsigned	posTIM15	= 116;
/** */
static constexpr unsigned	posTIM16	= 117;
/** */
static constexpr unsigned	posTIM17	= 118;
/** */
static constexpr unsigned	posMDIOS_WKUP	= 119;
/** */
static constexpr unsigned	posMDIOS	= 120;
/** */
static constexpr unsigned	posJPEG	= 121;
/** */
static constexpr unsigned	posMDMA	= 122;

/** */
static constexpr unsigned	posSDMMC2	= 124;
/** */
static constexpr unsigned	posHSEM0	= 125;

/** */
static constexpr unsigned	posDAC2	= 127;
/** */
static constexpr unsigned	posDMAMUX2_OVR	= 128;
/** */
static constexpr unsigned	posBDMA2_CH0	= 129;
/** */
static constexpr unsigned	posBDMA2_CH1	= 130;
/** */
static constexpr unsigned	posBDMA2_CH2	= 131;
/** */
static constexpr unsigned	posBDMA2_CH3	= 132;
/** */
static constexpr unsigned	posBDMA2_CH4	= 133;
/** */
static constexpr unsigned	posBDMA2_CH5	= 134;
/** */
static constexpr unsigned	posBDMA2_CH6	= 135;
/** */
static constexpr unsigned	posBDMA2_CH7	= 136;
/** */
static constexpr unsigned	posCOMP	= 137;
/** */
static constexpr unsigned	posLPTIM2	= 138;
/** */
static constexpr unsigned	posLPTIM3	= 139;
/** */
static constexpr unsigned	posUART9	= 140;
/** */
static constexpr unsigned	posUSART10	= 141;
/** */
static constexpr unsigned	posLPUART1	= 142;

/** */
static constexpr unsigned	posCRS	= 144;
/** */
static constexpr unsigned	posECC	= 145;

/** */
static constexpr unsigned	posDTS_IT	= 147;

/** */
static constexpr unsigned	posWKUP	= 149;
/** */
static constexpr unsigned	posOCTOSPI2	= 150;
/** */
static constexpr unsigned	posOTFDEC1	= 151;
/** */
static constexpr unsigned	posOTFDEC2	= 152;
/** */
static constexpr unsigned	posGFXMMU	= 153;
/** */
static constexpr unsigned	posBDMA1	= 154;

}
}
}
}
}

#endif
