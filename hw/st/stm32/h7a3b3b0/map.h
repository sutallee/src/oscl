/*
   Copyright (C) 2011 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_hw_st_stm32_h7a3b3b0_maph_
#define _oscl_hw_st_stm32_h7a3b3b0_maph_
#include <stdint.h>

#include "oscl/hw/arm/cortexm7/map.h"
#include "oscl/hw/st/stm32/h7a3b3b0/rcc/map.h"
#include "oscl/hw/st/stm32/h7a3b3b0/pwr/map.h"
#include "oscl/hw/st/stm32/h7a3b3b0/syscfg/map.h"
#include "oscl/hw/st/stm32/h7a3b3b0/flash/map.h"
#include "oscl/hw/st/stm32/h7a3b3b0/swo/map.h"
#include "oscl/hw/st/stm32/h7a3b3b0/tim/map.h"
#include "oscl/hw/st/stm32/h7/dbgmcu/map.h"
#include "oscl/hw/st/stm32/h7/usart/map.h"
#include "oscl/hw/st/stm32/h7/fmc/map.h"
#include "oscl/hw/st/stm32/h7/dma2d/map.h"
#include "oscl/hw/st/stm32/h7/ltdc/map.h"
#include "oscl/hw/st/stm32/h7/gpio/map.h"
#include "oscl/hw/st/stm32/h7/i2c/map.h"
#include "oscl/hw/st/stm32/h7/exti/map.h"
#include "oscl/hw/st/stm32/h7/spi/map.h"
#include "oscl/hw/st/stm32/h7/dma/map.h"
#include "oscl/hw/st/stm32/dmamux/map.h"
#include "oscl/hw/arm/cortexm7/fpu/map.h"
#include "oscl/hw/arm/cortexm7/scb/map.h"
#include "oscl/hw/arm/cortexm/map.h"

extern uint8_t	STM32H7A3B3B0_ITCM_RAM_BASE[];
extern uint8_t	STM32H7A3B3B0_FLASH_BANK1_BASE[];
extern uint8_t	STM32H7A3B3B0_FLASH_BANK2_BASE[];
extern uint8_t	STM32H7A3B3B0_OTP_BASE[];
extern uint8_t	STM32H7A3B3B0_SYSTEM_MEM_BASE[];
extern uint8_t	STM32H7A3B3B0_DTCM_RAM_BASE[];
extern uint8_t	STM32H7A3B3B0_AXISRAM1_BASE[];
extern uint8_t	STM32H7A3B3B0_AXISRAM2_BASE[];
extern uint8_t	STM32H7A3B3B0_AXISRAM3_BASE[];
extern uint8_t	STM32H7A3B3B0_GFXMMU_VRAM_BASE[];
extern uint8_t	STM32H7A3B3B0_AHBSRAM1_BASE[];
extern uint8_t	STM32H7A3B3B0_AHBSRAM2_BASE[];
extern uint8_t	STM32H7A3B3B0_SDRSRAM_BASE[];
extern uint8_t	STM32H7A3B3B0_BackupSRAM_BASE[];

extern Oscl::St::Stm32::H7::pDBGMCU::Map	STM32H7A3B3B0_DBGMCU;
extern uint8_t	STM32H7A3B3B0_CoreDebug;

extern Oscl::St::Stm32::H7::pFMC::Map	STM32H7A3B3B0_FMC;
extern Oscl::St::Stm32::H7::pDMA2D::Map	STM32H7A3B3B0_DMA2D;
extern Oscl::St::Stm32::H7::pLTDC::Map	STM32H7A3B3B0_LTDC;
extern Oscl::St::Stm32::H7::pGPIO::Map	STM32H7A3B3B0_GPIOA;
extern Oscl::St::Stm32::H7::pGPIO::Map	STM32H7A3B3B0_GPIOB;
extern Oscl::St::Stm32::H7::pGPIO::Map	STM32H7A3B3B0_GPIOC;
extern Oscl::St::Stm32::H7::pGPIO::Map	STM32H7A3B3B0_GPIOD;
extern Oscl::St::Stm32::H7::pGPIO::Map	STM32H7A3B3B0_GPIOE;
extern Oscl::St::Stm32::H7::pGPIO::Map	STM32H7A3B3B0_GPIOF;
extern Oscl::St::Stm32::H7::pGPIO::Map	STM32H7A3B3B0_GPIOG;
extern Oscl::St::Stm32::H7::pGPIO::Map	STM32H7A3B3B0_GPIOH;
extern Oscl::St::Stm32::H7::pGPIO::Map	STM32H7A3B3B0_GPIOI;
extern Oscl::St::Stm32::H7::pGPIO::Map	STM32H7A3B3B0_GPIOJ;
extern Oscl::St::Stm32::H7::pGPIO::Map	STM32H7A3B3B0_GPIOK;
extern Oscl::St::Stm32::H7A3B3B0::pPWR::Map	STM32H7A3B3B0_PWR;
extern Oscl::St::Stm32::H7A3B3B0::pRCC::Map	STM32H7A3B3B0_RCC;
extern Oscl::St::Stm32::H7A3B3B0::pSYSCFG::Map	STM32H7A3B3B0_SYSCFG;
extern Oscl::St::Stm32::H7A3B3B0::pFLASH::Map	STM32H7A3B3B0_FLASH;
extern Oscl::St::Stm32::H7::pUSART::Map	STM32H7A3B3B0_USART2;
extern Oscl::St::Stm32::H7A3B3B0::pSWO::Map	STM32H7A3B3B0_SWO;
extern Oscl::St::Stm32::H7::pI2C::Map	STM32H7A3B3B0_I2C4;
extern Oscl::St::Stm32::H7::pI2C::Map	STM32H7A3B3B0_I2C3;
extern Oscl::St::Stm32::H7::pI2C::Map	STM32H7A3B3B0_I2C2;
extern Oscl::St::Stm32::H7::pI2C::Map	STM32H7A3B3B0_I2C1;
extern Oscl::St::Stm32::H7::pEXTI::Map	STM32H7A3B3B0_EXTI;
extern Oscl::St::Stm32::H7A3B3B0::pTIM::Map	STM32H7A3B3B0_TIM2;
extern Oscl::St::Stm32::H7A3B3B0::pTIM::Map	STM32H7A3B3B0_TIM3;
extern Oscl::St::Stm32::H7A3B3B0::pTIM::Map	STM32H7A3B3B0_TIM4;
extern Oscl::St::Stm32::H7A3B3B0::pTIM::Map	STM32H7A3B3B0_TIM5;
extern Oscl::St::Stm32::H7::pSPI::Map	STM32H7A3B3B0_SPI3_I2S3;
extern Oscl::St::Stm32::H7::pSPI::Map	STM32H7A3B3B0_SPI2_I2S2;
extern Oscl::St::Stm32::H7::pSPI::Map	STM32H7A3B3B0_SPI1;
extern Oscl::St::Stm32::H7::pDMA::Map	STM32H7A3B3B0_DMA1;
extern Oscl::St::Stm32::H7::pDMA::Map	STM32H7A3B3B0_DMA2;
extern Oscl::St::Stm32::pDMAMUX::Map	STM32H7A3B3B0_DMAMUX1;
extern Oscl::St::Stm32::pDMAMUX::Map	STM32H7A3B3B0_DMAMUX2;

extern volatile uint8_t	STM32H7A3B3B0_BDMA1;
extern volatile uint8_t	STM32H7A3B3B0_SDMMC2_DELAY;
extern volatile uint8_t	STM32H7A3B3B0_SDMMC2;
extern volatile uint8_t	STM32H7A3B3B0_RNG;
extern volatile uint8_t	STM32H7A3B3B0_HASH;
extern volatile uint8_t	STM32H7A3B3B0_CRYP;
extern volatile uint8_t	STM32H7A3B3B0_DCMI;
extern volatile uint8_t	STM32H7A3B3B0_USB_OTG_FS;
extern volatile uint8_t	STM32H7A3B3B0_USB_OTG_HS;
extern volatile uint8_t	STM32H7A3B3B0_ETHERNET_MAC;
extern volatile uint8_t	STM32H7A3B3B0_BKPSRAM;
extern volatile uint8_t	STM32H7A3B3B0_CRC;
extern volatile uint8_t	STM32H7A3B3B0_TIM11;
extern volatile uint8_t	STM32H7A3B3B0_TIM10;
extern volatile uint8_t	STM32H7A3B3B0_TIM9;
extern volatile uint8_t	STM32H7A3B3B0_SDIO;
extern volatile uint8_t	STM32H7A3B3B0_ADC;
extern volatile uint8_t	STM32H7A3B3B0_TIM8;
extern volatile uint8_t	STM32H7A3B3B0_TIM1;
extern volatile uint8_t	STM32H7A3B3B0_DAC;
extern volatile uint8_t	STM32H7A3B3B0_CAN2;
extern volatile uint8_t	STM32H7A3B3B0_CAN1;
extern volatile uint8_t	STM32H7A3B3B0_IWDG;
extern volatile uint8_t	STM32H7A3B3B0_WWDG;
extern volatile uint8_t	STM32H7A3B3B0_RTC_BKP;
extern volatile uint8_t	STM32H7A3B3B0_TIM14;
extern volatile uint8_t	STM32H7A3B3B0_TIM13;
extern volatile uint8_t	STM32H7A3B3B0_TIM12;
extern volatile uint8_t	STM32H7A3B3B0_TIM7;
extern volatile uint8_t	STM32H7A3B3B0_TIM6;

extern uint8_t	STM32H7A3B3B0_SHARED_RAM;

#endif
