/*
   Copyright (C) 2024 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_hw_st_stm32_h7a3b3b0_swo_maph_
#define _oscl_hw_st_stm32_h7a3b3b0_swo_maph_
#include "reg.h"
namespace Oscl {
namespace St {
namespace Stm32 {
namespace H7A3B3B0 {
namespace pSWO {
struct Map {
    /** offset 0x000 */
    uint8_t reserved000[0x010-0x000];

	/** offset:0x010 */
	volatile rCODR::Reg	codr;

    /** offset 0x014 */
    uint8_t reserved014[0x0F0-0x014];

	/** offset:0x0F0 */
	volatile rSPPR::Reg	sppr;

    /** offset 0x0F4 */
    uint8_t reserved0F4[0x300-0x0F4];

	/** offset:0x300 */
	volatile rFFSR::Reg	ffsr;

    /** offset 0x304 */
    uint8_t reserved304[0xFA0-0x304];

	/** offset:0xFA0 */
	volatile rCLAIMSET::Reg	claimset;

	/** offset:0xFA4 */
	volatile rCLAIMCLR::Reg	claimclr;

    /** offset 0xFA8 */
    uint8_t reservedFA8[0xFB0-0xFA8];

	/** offset:0xFB0 */
	volatile rLAR::Reg	lar;

	/** offset:0xFB4 */
	volatile rLSR::Reg	lsr;

	/** offset:0xFB8 */
	volatile rAUTHSTAT::Reg	authstat;

    /** offset 0xFBC */
    uint8_t reservedFBC[0xFC8-0xFBC];

	/** offset:0xFC8 */
	volatile rDEVID::Reg	devid;

	/** offset:0xFCC */
	volatile rDEVTYPE::Reg	devtype;

	/** offset:0xFD0 */
	volatile rPIDR4::Reg	pidr4;

    /** offset 0xFD4 */
    uint8_t reservedFD4[0xFE0-0xFD4];

	/** offset:0xFE0 */
	volatile rPIDR0::Reg	pidr0;

	/** offset:0xFE4 */
	volatile rPIDR1::Reg	pidr1;

	/** offset:0xFE8 */
	volatile rPIDR2::Reg	pidr2;

	/** offset:0xFEC */
	volatile rPIDR3::Reg	pidr3;

	/** offset:0xFF0 */
	volatile rCIDR0::Reg	cidr0;

	/** offset:0xFF4 */
	volatile rCIDR1::Reg	cidr1;

	/** offset:0xFF8 */
	volatile rCIDR2::Reg	cidr2;

	/** offset:0xFFC */
	volatile rCIDR3::Reg	cidr3;
	};
}
}
}
}
}
#endif
