/*
   Copyright (C) 2024 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_hw_st_stm32_h7a3b3b0_swo_regh_
#define _oscl_hw_st_stm32_h7a3b3b0_swo_regh_
#include <stdint.h>
namespace Oscl { // Namespace description
	namespace St { // Namespace description
		namespace Stm32 { // Namespace description
			namespace H7A3B3B0 { // Namespace description
				namespace pSWO { // Namespace description
					namespace rCODR { // Register description
						typedef uint32_t	Reg;
						constexpr Reg	valueAtReset = 0x00000000;
						namespace fPRESCALER { // Field Description
							constexpr Reg Lsb = 0;
							constexpr Reg FieldMask = 0x00001FFF;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_minValue = 0x0;
							constexpr Reg ValueMask_minValue = 0x00000000;
							constexpr Reg Value_maxValue = 0x1FFF;
							constexpr Reg ValueMask_maxValue = 0x00001FFF;
							};
						};
					namespace rSPPR { // Register description
						typedef uint32_t	Reg;
						namespace fPPROT { // Field Description
							constexpr Reg Lsb = 0;
							constexpr Reg FieldMask = 0x00000003;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_swoManchester = 0x1;
							constexpr Reg ValueMask_swoManchester = 0x00000001;
							constexpr Reg Value_swoNRZ = 0x2;
							constexpr Reg ValueMask_swoNRZ = 0x00000002;
							};
						};
					namespace rFFSR { // Register description
						typedef uint32_t	Reg;
						constexpr Reg	valueAtReset = 0x00000008;
						namespace fFLINPROG { // Field Description
							constexpr Reg Lsb = 0;
							constexpr Reg FieldMask = 0x00000001;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_notInProgress = 0x0;
							constexpr Reg ValueMask_notInProgress = 0x00000000;
							};
						namespace fFTSTOPPED { // Field Description
							constexpr Reg Lsb = 1;
							constexpr Reg FieldMask = 0x00000002;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_running = 0x0;
							constexpr Reg ValueMask_running = 0x00000000;
							};
						namespace fTCPRESENT { // Field Description
							constexpr Reg Lsb = 2;
							constexpr Reg FieldMask = 0x00000004;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_notPresent = 0x0;
							constexpr Reg ValueMask_notPresent = 0x00000000;
							};
						namespace fFTNONSTOP { // Field Description
							constexpr Reg Lsb = 3;
							constexpr Reg FieldMask = 0x00000008;
							constexpr Reg Value_valueAtReset = 0x1;
							constexpr Reg ValueMask_valueAtReset = 0x00000008;
							constexpr Reg Value_allowed = 0x1;
							constexpr Reg ValueMask_allowed = 0x00000008;
							};
						};
					namespace rCLAIMSET { // Register description
						typedef uint32_t	Reg;
						constexpr Reg	valueAtReset = 0x0000000F;
						namespace fCLAIMSET { // Field Description
							constexpr Reg Lsb = 0;
							constexpr Reg FieldMask = 0x0000000F;
							constexpr Reg Value_valueAtReset = 0xF;
							constexpr Reg ValueMask_valueAtReset = 0x0000000F;
							constexpr Reg Value_noEffect = 0x0;
							constexpr Reg ValueMask_noEffect = 0x00000000;
							constexpr Reg Value_setBit0 = 0x1;
							constexpr Reg ValueMask_setBit0 = 0x00000001;
							constexpr Reg Value_setBit1 = 0x2;
							constexpr Reg ValueMask_setBit1 = 0x00000002;
							constexpr Reg Value_setBit2 = 0x4;
							constexpr Reg ValueMask_setBit2 = 0x00000004;
							constexpr Reg Value_setBit3 = 0x8;
							constexpr Reg ValueMask_setBit3 = 0x00000008;
							};
						};
					namespace rCLAIMCLR { // Register description
						typedef uint32_t	Reg;
						constexpr Reg	valueAtReset = 0x00000000;
						namespace fCLAIMCLR { // Field Description
							constexpr Reg Lsb = 0;
							constexpr Reg FieldMask = 0x0000000F;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_noEffect = 0x0;
							constexpr Reg ValueMask_noEffect = 0x00000000;
							constexpr Reg Value_clearBit0 = 0x1;
							constexpr Reg ValueMask_clearBit0 = 0x00000001;
							constexpr Reg Value_clearBit1 = 0x2;
							constexpr Reg ValueMask_clearBit1 = 0x00000002;
							constexpr Reg Value_clearBit2 = 0x4;
							constexpr Reg ValueMask_clearBit2 = 0x00000004;
							constexpr Reg Value_clearBit3 = 0x8;
							constexpr Reg ValueMask_clearBit3 = 0x00000008;
							};
						};
					namespace rLAR { // Register description
						typedef uint32_t	Reg;
						namespace fACCESS_W { // Field Description
							constexpr Reg Lsb = 0;
							constexpr Reg FieldMask = 0xFFFFFFFF;
							constexpr Reg Value_key = 0xC5ACCE55;
							constexpr Reg ValueMask_key = 0xC5ACCE55;
							};
						};
					namespace rLSR { // Register description
						typedef uint32_t	Reg;
						constexpr Reg	valueAtReset = 0x00000003;
						namespace fLOCKEXIST { // Field Description
							constexpr Reg Lsb = 0;
							constexpr Reg FieldMask = 0x00000001;
							constexpr Reg Value_valueAtReset = 0x1;
							constexpr Reg ValueMask_valueAtReset = 0x00000001;
							constexpr Reg Value_noLock = 0x0;
							constexpr Reg ValueMask_noLock = 0x00000000;
							constexpr Reg Value_hasLock = 0x1;
							constexpr Reg ValueMask_hasLock = 0x00000001;
							};
						namespace fLOCKGRANT { // Field Description
							constexpr Reg Lsb = 1;
							constexpr Reg FieldMask = 0x00000002;
							constexpr Reg Value_valueAtReset = 0x1;
							constexpr Reg ValueMask_valueAtReset = 0x00000002;
							constexpr Reg Value_writePermitted = 0x0;
							constexpr Reg ValueMask_writePermitted = 0x00000000;
							constexpr Reg Value_writeBlock = 0x1;
							constexpr Reg ValueMask_writeBlock = 0x00000002;
							};
						namespace fLOCKTYPE { // Field Description
							constexpr Reg Lsb = 2;
							constexpr Reg FieldMask = 0x00000004;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_v32Bit = 0x0;
							constexpr Reg ValueMask_v32Bit = 0x00000000;
							};
						};
					namespace rAUTHSTAT { // Register description
						typedef uint32_t	Reg;
						constexpr Reg	valueAtReset = 0x00000000;
						namespace fNSID { // Field Description
							constexpr Reg Lsb = 0;
							constexpr Reg FieldMask = 0x00000003;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_notImplemented = 0x0;
							constexpr Reg ValueMask_notImplemented = 0x00000000;
							};
						namespace fNSNID { // Field Description
							constexpr Reg Lsb = 2;
							constexpr Reg FieldMask = 0x0000000C;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_notImplemented = 0x0;
							constexpr Reg ValueMask_notImplemented = 0x00000000;
							};
						namespace fSID { // Field Description
							constexpr Reg Lsb = 4;
							constexpr Reg FieldMask = 0x00000030;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_notImplemented = 0x0;
							constexpr Reg ValueMask_notImplemented = 0x00000000;
							};
						namespace fSNID { // Field Description
							constexpr Reg Lsb = 6;
							constexpr Reg FieldMask = 0x000000C0;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_notImplemented = 0x0;
							constexpr Reg ValueMask_notImplemented = 0x00000000;
							};
						};
					namespace rDEVID { // Register description
						typedef uint32_t	Reg;
						constexpr Reg	valueAtReset = 0x00000EA0;
						namespace fMUXNUM { // Field Description
							constexpr Reg Lsb = 0;
							constexpr Reg FieldMask = 0x0000001F;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_none = 0x0;
							constexpr Reg ValueMask_none = 0x00000000;
							};
						namespace fCLKRELAT { // Field Description
							constexpr Reg Lsb = 5;
							constexpr Reg FieldMask = 0x00000020;
							constexpr Reg Value_valueAtReset = 0x1;
							constexpr Reg ValueMask_valueAtReset = 0x00000020;
							constexpr Reg Value_asynchronousToATB = 0x1;
							constexpr Reg ValueMask_asynchronousToATB = 0x00000020;
							};
						namespace fFIFOSIZE { // Field Description
							constexpr Reg Lsb = 6;
							constexpr Reg FieldMask = 0x000001C0;
							constexpr Reg Value_valueAtReset = 0x2;
							constexpr Reg ValueMask_valueAtReset = 0x00000080;
							constexpr Reg Value_v4Words = 0x2;
							constexpr Reg ValueMask_v4Words = 0x00000080;
							};
						namespace fTCLKDATA { // Field Description
							constexpr Reg Lsb = 9;
							constexpr Reg FieldMask = 0x00000200;
							constexpr Reg Value_valueAtReset = 0x1;
							constexpr Reg ValueMask_valueAtReset = 0x00000200;
							constexpr Reg Value_supported = 0x1;
							constexpr Reg ValueMask_supported = 0x00000200;
							};
						namespace fSWOMAN { // Field Description
							constexpr Reg Lsb = 10;
							constexpr Reg FieldMask = 0x00000400;
							constexpr Reg Value_valueAtReset = 0x1;
							constexpr Reg ValueMask_valueAtReset = 0x00000400;
							constexpr Reg Value_supported = 0x1;
							constexpr Reg ValueMask_supported = 0x00000400;
							};
						namespace fSWOUARTNRZ { // Field Description
							constexpr Reg Lsb = 11;
							constexpr Reg FieldMask = 0x00000800;
							constexpr Reg Value_valueAtReset = 0x1;
							constexpr Reg ValueMask_valueAtReset = 0x00000800;
							constexpr Reg Value_supported = 0x1;
							constexpr Reg ValueMask_supported = 0x00000800;
							};
						};
					namespace rDEVTYPE { // Register description
						typedef uint32_t	Reg;
						constexpr Reg	valueAtReset = 0x00000011;
						namespace fMAJORTYPE { // Field Description
							constexpr Reg Lsb = 0;
							constexpr Reg FieldMask = 0x0000000F;
							constexpr Reg Value_valueAtReset = 0x1;
							constexpr Reg ValueMask_valueAtReset = 0x00000001;
							constexpr Reg Value_traceSinkComponent = 0x1;
							constexpr Reg ValueMask_traceSinkComponent = 0x00000001;
							};
						namespace fSUBTYPE { // Field Description
							constexpr Reg Lsb = 4;
							constexpr Reg FieldMask = 0x000000F0;
							constexpr Reg Value_valueAtReset = 0x1;
							constexpr Reg ValueMask_valueAtReset = 0x00000010;
							constexpr Reg Value_tracePortComponent = 0x1;
							constexpr Reg ValueMask_tracePortComponent = 0x00000010;
							};
						};
					namespace rPIDR4 { // Register description
						typedef uint32_t	Reg;
						constexpr Reg	valueAtReset = 0x00000004;
						namespace fJEP106CONT { // Field Description
							constexpr Reg Lsb = 0;
							constexpr Reg FieldMask = 0x0000000F;
							constexpr Reg Value_valueAtReset = 0x1;
							constexpr Reg ValueMask_valueAtReset = 0x00000001;
							constexpr Reg Value_armJEDEC = 0x4;
							constexpr Reg ValueMask_armJEDEC = 0x00000004;
							};
						namespace f4KCOUNT { // Field Description
							constexpr Reg Lsb = 4;
							constexpr Reg FieldMask = 0x000000F0;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_single4KByteRegion = 0x0;
							constexpr Reg ValueMask_single4KByteRegion = 0x00000000;
							};
						};
					namespace rPIDR0 { // Register description
						typedef uint32_t	Reg;
						};
					namespace rPIDR1 { // Register description
						typedef uint32_t	Reg;
						};
					namespace rPIDR2 { // Register description
						typedef uint32_t	Reg;
						};
					namespace rPIDR3 { // Register description
						typedef uint32_t	Reg;
						};
					namespace rCIDR0 { // Register description
						typedef uint32_t	Reg;
						};
					namespace rCIDR1 { // Register description
						typedef uint32_t	Reg;
						};
					namespace rCIDR2 { // Register description
						typedef uint32_t	Reg;
						};
					namespace rCIDR3 { // Register description
						typedef uint32_t	Reg;
						};
					}
				}
			}
		}
	}
#endif
