/*
   Copyright (C) 2024 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_hw_st_stm32_h7_dbgmcu_regh_
#define _oscl_hw_st_stm32_h7_dbgmcu_regh_
#include <stdint.h>
namespace Oscl { // Namespace description
	namespace St { // Namespace description
		namespace Stm32 { // Namespace description
			namespace H7 { // Namespace description
				namespace pDBGMCU { // Namespace description
					namespace rIDC { // Register description
						typedef uint32_t	Reg;
						constexpr Reg	valueAtReset = 0x10006480;
						namespace fDEV_ID { // Field Description
							constexpr Reg Lsb = 0;
							constexpr Reg FieldMask = 0x00000FFF;
							constexpr Reg Value_vSTM32H7A3B3B0 = 0x480;
							constexpr Reg ValueMask_vSTM32H7A3B3B0 = 0x00000480;
							};
						namespace fREV_ID { // Field Description
							constexpr Reg Lsb = 16;
							constexpr Reg FieldMask = 0xFFFF0000;
							constexpr Reg Value_revisionA = 0x1000;
							constexpr Reg ValueMask_revisionA = 0x10000000;
							constexpr Reg Value_revisionZ = 0x1001;
							constexpr Reg ValueMask_revisionZ = 0x10010000;
							constexpr Reg Value_revisionX = 0x1007;
							constexpr Reg ValueMask_revisionX = 0x10070000;
							};
						};
					namespace rCR { // Register description
						typedef uint32_t	Reg;
						constexpr Reg	valueAtReset = 0x00000000;
						namespace fDBGSLEEP_CD { // Field Description
							constexpr Reg Lsb = 0;
							constexpr Reg FieldMask = 0x00000001;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_normal = 0x0;
							constexpr Reg ValueMask_normal = 0x00000000;
							constexpr Reg Value_autoClockStopDisabled = 0x1;
							constexpr Reg ValueMask_autoClockStopDisabled = 0x00000001;
							};
						namespace fDBGSTOP_CD { // Field Description
							constexpr Reg Lsb = 1;
							constexpr Reg FieldMask = 0x00000002;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_normal = 0x0;
							constexpr Reg ValueMask_normal = 0x00000000;
							constexpr Reg Value_autoClockStopDisabled = 0x1;
							constexpr Reg ValueMask_autoClockStopDisabled = 0x00000002;
							};
						namespace fDBGSTBY_CD { // Field Description
							constexpr Reg Lsb = 2;
							constexpr Reg FieldMask = 0x00000004;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_normal = 0x0;
							constexpr Reg ValueMask_normal = 0x00000000;
							constexpr Reg Value_autoClockStopAndPowerDisabled = 0x1;
							constexpr Reg ValueMask_autoClockStopAndPowerDisabled = 0x00000004;
							};
						namespace fTRACECLKEN { // Field Description
							constexpr Reg Lsb = 20;
							constexpr Reg FieldMask = 0x00100000;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_disable = 0x0;
							constexpr Reg ValueMask_disable = 0x00000000;
							constexpr Reg Value_disabled = 0x0;
							constexpr Reg ValueMask_disabled = 0x00000000;
							constexpr Reg Value_enable = 0x1;
							constexpr Reg ValueMask_enable = 0x00100000;
							constexpr Reg Value_enabled = 0x1;
							constexpr Reg ValueMask_enabled = 0x00100000;
							};
						namespace fCDDBGCKEN { // Field Description
							constexpr Reg Lsb = 21;
							constexpr Reg FieldMask = 0x00200000;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_disable = 0x0;
							constexpr Reg ValueMask_disable = 0x00000000;
							constexpr Reg Value_disabled = 0x0;
							constexpr Reg ValueMask_disabled = 0x00000000;
							constexpr Reg Value_enable = 0x1;
							constexpr Reg ValueMask_enable = 0x00200000;
							constexpr Reg Value_enabled = 0x1;
							constexpr Reg ValueMask_enabled = 0x00200000;
							};
						namespace fSRDDBGCKEN { // Field Description
							constexpr Reg Lsb = 22;
							constexpr Reg FieldMask = 0x00400000;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_disable = 0x0;
							constexpr Reg ValueMask_disable = 0x00000000;
							constexpr Reg Value_disabled = 0x0;
							constexpr Reg ValueMask_disabled = 0x00000000;
							constexpr Reg Value_enable = 0x1;
							constexpr Reg ValueMask_enable = 0x00400000;
							constexpr Reg Value_enabled = 0x1;
							constexpr Reg ValueMask_enabled = 0x00400000;
							};
						namespace fTRGOEN { // Field Description
							constexpr Reg Lsb = 28;
							constexpr Reg FieldMask = 0x10000000;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_input = 0x0;
							constexpr Reg ValueMask_input = 0x00000000;
							constexpr Reg Value_output = 0x1;
							constexpr Reg ValueMask_output = 0x10000000;
							};
						};
					namespace rAPB3FZ1 { // Register description
						typedef uint32_t	Reg;
						namespace fWWDG { // Field Description
							constexpr Reg Lsb = 6;
							constexpr Reg FieldMask = 0x00000040;
							};
						};
					namespace rAPB1LFZ1 { // Register description
						typedef uint32_t	Reg;
						namespace fTIM2 { // Field Description
							constexpr Reg Lsb = 0;
							constexpr Reg FieldMask = 0x00000001;
							};
						namespace fTIM3 { // Field Description
							constexpr Reg Lsb = 1;
							constexpr Reg FieldMask = 0x00000002;
							};
						namespace fTIM4 { // Field Description
							constexpr Reg Lsb = 2;
							constexpr Reg FieldMask = 0x00000004;
							};
						namespace fTIM5 { // Field Description
							constexpr Reg Lsb = 3;
							constexpr Reg FieldMask = 0x00000008;
							};
						namespace fTIM6 { // Field Description
							constexpr Reg Lsb = 4;
							constexpr Reg FieldMask = 0x00000010;
							};
						namespace fTIM7 { // Field Description
							constexpr Reg Lsb = 5;
							constexpr Reg FieldMask = 0x00000020;
							};
						namespace fTIM12 { // Field Description
							constexpr Reg Lsb = 6;
							constexpr Reg FieldMask = 0x00000040;
							};
						namespace fTIM13 { // Field Description
							constexpr Reg Lsb = 7;
							constexpr Reg FieldMask = 0x00000080;
							};
						namespace fTIM14 { // Field Description
							constexpr Reg Lsb = 8;
							constexpr Reg FieldMask = 0x00000100;
							};
						namespace fLPTIM1 { // Field Description
							constexpr Reg Lsb = 9;
							constexpr Reg FieldMask = 0x00000200;
							};
						namespace fI2C1 { // Field Description
							constexpr Reg Lsb = 21;
							constexpr Reg FieldMask = 0x00200000;
							};
						namespace fI2C2 { // Field Description
							constexpr Reg Lsb = 22;
							constexpr Reg FieldMask = 0x00400000;
							};
						namespace fI2C3 { // Field Description
							constexpr Reg Lsb = 23;
							constexpr Reg FieldMask = 0x00800000;
							};
						};
					namespace rAPB2FZ1 { // Register description
						typedef uint32_t	Reg;
						namespace fTIM1 { // Field Description
							constexpr Reg Lsb = 0;
							constexpr Reg FieldMask = 0x00000001;
							};
						namespace fTIM8 { // Field Description
							constexpr Reg Lsb = 1;
							constexpr Reg FieldMask = 0x00000002;
							};
						namespace fTIM15 { // Field Description
							constexpr Reg Lsb = 16;
							constexpr Reg FieldMask = 0x00010000;
							};
						namespace fTIM16 { // Field Description
							constexpr Reg Lsb = 17;
							constexpr Reg FieldMask = 0x00020000;
							};
						namespace fTIM17 { // Field Description
							constexpr Reg Lsb = 18;
							constexpr Reg FieldMask = 0x00040000;
							};
						};
					namespace rAPB4FZ1 { // Register description
						typedef uint32_t	Reg;
						namespace fI2C4 { // Field Description
							constexpr Reg Lsb = 7;
							constexpr Reg FieldMask = 0x00000080;
							};
						namespace fLPTIM2 { // Field Description
							constexpr Reg Lsb = 9;
							constexpr Reg FieldMask = 0x00000200;
							};
						namespace fLPTIM3 { // Field Description
							constexpr Reg Lsb = 10;
							constexpr Reg FieldMask = 0x00000400;
							};
						namespace fRTC { // Field Description
							constexpr Reg Lsb = 16;
							constexpr Reg FieldMask = 0x00010000;
							};
						namespace fWDGLSCD { // Field Description
							constexpr Reg Lsb = 18;
							constexpr Reg FieldMask = 0x00040000;
							};
						};
					}
				}
			}
		}
	}
#endif
