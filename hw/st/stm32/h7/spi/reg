/*
   Copyright (C) 2024 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

module oscl_hw_st_stm32_h7_spi_regh {
sysinclude "stdint.h";
namespace Oscl {
namespace St {
namespace Stm32 {
namespace H7 {
namespace pSPI {
	register rCR1 uint32_t {
		value valueAtReset	0x00000000
		field fSPE 0 1 {
			value valueAtReset	0
			value disable		0
			value disabled		0
			value enable		1
			value enabled		1
			}
		field fMASRX 8 1 {
			value valueAtReset	0
			value continuous	0
			value suspend		1
			}
		field fCSTART 9 1 {
			/* rs */
			value valueAtReset	0
			value idle		0
			value start		1
			}
		field fCSUSP 10 1 {
			/* w */
			value valueAtReset	0
			value noEffect		0
			value suspend		1
			}
		field fHDDIR 11 1 {
			value valueAtReset	0
			value receiver		0
			value transmitter	1
			}
		field fSSI 12 1 {
			value valueAtReset	0
			value low		0
			value high		1
			}
		field fCRC33_17 13 1 {
			value valueAtReset	0
			value notUsed		0
			value used			1
			}
		field fRCRCI 14 1 {
			value valueAtReset	0
			value allZeroPattern		0
			value allOnesPattern		1
			}
		field fTCRCI 15 1 {
			value valueAtReset	0
			value allZeroPattern		0
			value allOnesPattern		1
			}
		field fIOLOCK 16 1 {
			/* rs */
			value valueAtReset	0
			value notLocked		0
			value locked		1
			}
		}
	register rCR2 uint32_t {
		value valueAtReset	0x00000000
		field fTSIZE 0 16 {
			value valueAtReset	0x0000
			value minValue		0x0000
			value maxValue		0xFFFF
			}
		field fTSER 16 16 {
			/* rs */
			value valueAtReset	0x0000
			value minValue		0x0000
			value maxValue		0xFFFF
			}
		}
	register rCFG1 uint32_t {
		value valueAtReset	0x00070007
		field fDSIZE 0 5 {
			/* n-bits - 1 */
			value valueAtReset	0x07
			value minValue		0x03
			value maxValue		0x1F
			value v4Bits		0x03
			value v5Bits		0x04
			value v6Bits		0x05
			value v7Bits		0x06
			value v8Bits		0x07

			value v16Bits		0x0F

			value v32Bits		0x1F
			}
		field fFTHLV 5 4 {
			/* n-frames - 1 */
			value valueAtReset	0x0
			value minValue		0x0
			value maxValue		0xF
			}
		field fUDRCFG 9 2 {
			value valueAtReset	0x0
			value sendUDRDR		0x0
			value sendLastRx	0x1
			value sendLastTx	0x2
			}
		field fUDRDET 11 2 {
			value valueAtReset	0x0
			value beginningOfDataFrame		0x0
			value endOfLastDataFrame		0x1
			value beginningOfActiveSS		0x2
			}
		field fRXDMAEN 14 1 {
			value valueAtReset	0
			value disable		0
			value disabled		0
			value enable		1
			value enabled		1
			}
		field fTXDMAEN 15 1 {
			value valueAtReset	0
			value disable		0
			value disabled		0
			value enable		1
			value enabled		1
			}
		field fCRCSIZE 16 5 {
			/* n-bits - 1 */
			value valueAtReset	0x07
			value minValue		0x03
			value maxValue		0x1F
			value v8Bits		0x07
			value v16Bits		0x0F
			}
		field fCRCEN 22 1 {
			value valueAtReset	0
			value disable		0
			value disabled		0
			value enable		1
			value enabled		1
			}
		field fMBR 28 3 {
			value valueAtReset	0x0
			value divideBy2		0x0
			value divideBy4		0x1
			value divideBy8		0x2
			value divideBy16	0x3
			value divideBy32	0x4
			value divideBy64	0x5
			value divideBy128	0x6
			value divideBy256	0x7
			}
		}
	register rCFG2 uint32_t {
		value valueAtReset	0x00000000
		field fMSSI 0 4 {
			value valueAtReset	0x0
			/* n-clocks */
			value minValue	0x0
			value maxValue	0xF
			value v0Clocks	0x0
			value v15Clocks	0xF
			}
		field fMIDI 4 4 {
			value valueAtReset	0x0
			/* n-clocks */
			value minValue	0x0
			value maxValue	0xF
			value v0Clocks	0x0
			value v15Clocks	0xF
			}
		field fIOSWP 15 1 {
			value valueAtReset	0
			value noSWAP	0
			value swap		1
			}
		field fCOMM 17 2 {
			value valueAtReset	0x0
			value fullDuplex	0x0
			value simplexTx		0x1
			value simplexRx		0x2
			value halfDuplex	0x3
			}
		field fSP 19 3 {
			value valueAtReset	0x0
			value motorola	0x0
			value ti		0x1
			}
		field fMASTER 22 1 {
			value valueAtReset	0
			value slave		0
			value master	1
			}
		field fLSBFRST 23 1 {
			value valueAtReset	0
			value msb		0
			value lsb		1
			}
		field fCPHA 24 1 {
			value valueAtReset	0
			value firstClockEdge	0
			value secondClockEdge	1
			}
		field fCPOL 25 1 {
			value valueAtReset	0
			value idleLOW	0
			value idleHIGH	0
			}
		field fSSM 26 1 {
			value valueAtReset	0
			value pad	0
			value ssi	1
			}
		field fSSIOP 28 1 {
			value valueAtReset	0
			value activeLOW		0
			value activeHIGH	1
			}
		field fSSOE 29 1 {
			value valueAtReset	0
			value disable		0
			value disabled		0
			value enable		1
			value enabled		1
			}
		field fSSOM 30 1 {
			value valueAtReset	0
			value keepActiveUntilEOT	0
			value inactiveBetweenFrames	1
			}
		field fAFCNTR 31 1 {
			value valueAtReset	0
			value disable		0
			value disabled		0
			value enable		1
			value enabled		1
			}
		}
	register rIER uint32_t {
		value valueAtReset	0x00000000
		field fRXPIE 0 1 {
			value valueAtReset	0
			value disable		0
			value disabled		0
			value enable		1
			value enabled		1
			}
		field fTXPIE 1 1 {
			/* rs */
			value valueAtReset	0
			value disable		0
			value disabled		0
			value enable		1
			value enabled		1
			}
		field fDPXPIE 2 1 {
			/* rs */
			value valueAtReset	0
			value disable		0
			value disabled		0
			value enable		1
			value enabled		1
			}
		field fEOTIE 3 1 {
			value valueAtReset	0
			value disable		0
			value disabled		0
			value enable		1
			value enabled		1
			}
		field fTXTFIE 4 1 {
			value valueAtReset	0
			value disable		0
			value disabled		0
			value enable		1
			value enabled		1
			}
		field fUDRIE 5 1 {
			value valueAtReset	0
			value disable		0
			value disabled		0
			value enable		1
			value enabled		1
			}
		field fOVRIE 6 1 {
			value valueAtReset	0
			value disable		0
			value disabled		0
			value enable		1
			value enabled		1
			}
		field fCRCEIE 7 1 {
			value valueAtReset	0
			value disable		0
			value disabled		0
			value enable		1
			value enabled		1
			}
		field fTIFREIE 8 1 {
			value valueAtReset	0
			value disable		0
			value disabled		0
			value enable		1
			value enabled		1
			}
		field fMODFIE 9 1 {
			value valueAtReset	0
			value disable		0
			value disabled		0
			value enable		1
			value enabled		1
			}
		field fTSERFIE 10 1 {
			value valueAtReset	0
			value disable		0
			value disabled		0
			value enable		1
			value enabled		1
			}
		}
	register rSR uint32_t {
		value valueAtReset	0x00001002
		field fRXP 0 1 {
			/* read-only */
			value valueAtReset	0
			value emptyOrIncomplete	0
			value notEmpty			1
			}
		field fTXP 1 1 {
			value valueAtReset	1
			value full		0
			value notFull	1
			}
		field fDXP 2 1 {
			/* read-only */
			/* WTF does this mean? */
			value valueAtReset	0
			value txFullAndOrRxEmpty	0
			value txNotFullAndRxNotFull	1
			}
		field fEOT 3 1 {
			/* read-only */
			value valueAtReset	0
			value busyOrIdle	0
			value complete		1
			}
		field fTXTF 4 1 {
			/* read-only */
			value valueAtReset	0
			value busyOrIdle	0
			value finished		1
			}
		field fUDR 5 1 {
			/* read-only */
			value valueAtReset	0
			value noError	0
			value underrun	1
			}
		field fOVR 6 1 {
			/* read-only */
			value valueAtReset	0
			value noError	0
			value overrun	1
			}
		field fCRCE 7 1 {
			/* read-only */
			value valueAtReset	0
			value noError	0
			value error		1
			}
		field fTIFRE 8 1 {
			/* read-only */
			value valueAtReset	0
			value noError	0
			value error		1
			}
		field fMODF 9 1 {
			/* read-only */
			value valueAtReset	0
			value noFault	0
			value fault		1
			}
		field fTSERF 10 1 {
			/* read-only */
			value valueAtReset	0
			value noAcceptation	0
			value dataAccepted	1
			}
		field fSUSP 11 1 {
			/* read-only */
			value valueAtReset	0
			value notSuspended	0
			value suspended		1
			}
		field fTXC 12 1 {
			/* read-only */
			value valueAtReset	1
			value ongoing	0
			value complete	1
			}
		field fRXPLVL 13 2 {
			/* read-only */
			value valueAtReset	0x0
			value noNextFrameAvailable	0
			value oneFrameAvailable		1
			value twoFramesAvailable	2
			value threeFramesAvailable	3
			}
		field fRXWNE 15 1 {
			/* read-only */
			value valueAtReset	0
			value lessThanFourBytes	0
			value atLeastFourBytes	1
			}
		field fCTSIZE 16 16 {
			/* read-only */
			/* "not quite reliable"? */
			value valueAtReset	0x0000
			value minValue		0x0000
			value maxValue		0x0000
			}
		}
	register rIFCR uint32_t {
		value valueAtReset	0x00000000
		field fEOTC 3 1 {
			/* write-only */
			value valueAtReset	0
			value noEffect		0
			value clear			1
			}
		field fTXTFC 4 1 {
			/* write-only */
			value valueAtReset	0
			value noEffect		0
			value clear			1
			}
		field fUDRC 5 1 {
			/* write-only */
			value valueAtReset	0
			value noEffect		0
			value clear			1
			}
		field fOVRC 6 1 {
			/* write-only */
			value valueAtReset	0
			value noEffect		0
			value clear			1
			}
		field fCRCEC 7 1 {
			/* write-only */
			value valueAtReset	0
			value noEffect		0
			value clear			1
			}
		field fTIFREC 8 1 {
			/* write-only */
			value valueAtReset	0
			value noEffect		0
			value clear			1
			}
		field fMODFC 9 1 {
			/* write-only */
			value valueAtReset	0
			value noEffect		0
			value clear			1
			}
		field fTSERFC 10 1 {
			/* write-only */
			value valueAtReset	0
			value noEffect		0
			value clear			1
			}
		field fSUSPC 11 1 {
			/* write-only */
			value valueAtReset	0
			value noEffect		0
			value clear			1
			}
		}
	register rTXDR uint32_t {
		value valueAtReset	0x00000000
		field fTXDR 0 32 {
			/* write-only */
			value valueAtReset	0x00000000
			value minValue		0x00000000
			value maxValue		0xFFFFFFFF
			}
		}
	register rRXDR uint32_t {
		value valueAtReset	0x00000000
		field fRXDR 0 32 {
			/* read-only */
			value valueAtReset	0x00000000
			}
		}
	register rCRCPOLY uint32_t {
		value valueAtReset	0x00000107
		field fCRCPOLY 0 32 {
			value valueAtReset	0x00000107
			}
		}
	register rTXCRC uint32_t {
		value valueAtReset	0x00000000
		field fTXCRC 0 32 {
			/* read-only */
			value valueAtReset	0x00000000
			}
		}
	register rRXCRC uint32_t {
		value valueAtReset	0x00000000
		field fRXCRC 0 32 {
			/* read-only */
			value valueAtReset	0x00000000
			}
		}
	register rUDRDR uint32_t {
		value valueAtReset	0x00000000
		field fUDRDR 0 32 {
			value valueAtReset	0x00000000
			}
		}
	register rI2SCFGR uint32_t {
		value valueAtReset	0x00000000
		field fI2SMOD 0 1 {
			value valueAtReset	0
			value spi		0
			value i2sPCM	1
			}
		field fI2SCFG 1 3 {
			value valueAtReset	0x0
			value slaveTransmit		0x0
			value slaveReceive		0x1
			value masterTransmit	0x2
			value masterReceive		0x3
			value slaveFullDuplex	0x4
			value masterFullDuplex	0x5
			}
		field fI2SSTD 4 2 {
			value valueAtReset	0x0
			value i2SPhillips	0x0
			value msbJustified	0x1
			value lsbJustified	0x2
			value pcm			0x3
			}
		field fPCMSYNC 7 1 {
			value valueAtReset	0
			value shortFrame	0
			value longFrame		1
			}
		field fDATLEN 8 2 {
			value valueAtReset	0x0
			value v16Bit		0x0
			value v24Bit		0x1
			value v32Bit		0x2
			}
		field fCHLEN 10 1 {
			value valueAtReset	0
			value v16Bit	0
			value v32Bit	1
			}
		field fCKPOL 11 1 {
			value valueAtReset	0
			value changeOnFallReadOnRise	0
			value changeOnRiseReadOnFall	1
			}
		field fFIXCH 12 1 {
			value valueAtReset	0
			value ignoreCHLEN	0
			value useCHLEN		1
			}
		field fWSINV 13 1 {
			value valueAtReset	0
			value noInvert		0
			value invert		1
			value disable		0
			value disabled		0
			value enable		1
			value enabled		1
			}
		field fDATFMT 14 1 {
			value valueAtReset	0
			value rightAlign	0
			value leftAlign		1
			}
		field fI2SDIV 16 8 {
			value valueAtReset	0x00
			value minValue		0x00
			value maxValue		0xFF
			value excludeValueWhenOddIs1	0x01
			}
		field fODD 24 1 {
			value valueAtReset	0
			value dividerIs2xI2sDiv			0
			value dividerIs2xI2sDivPlus1	1
			}
		field fMCKOE 25 1 {
			value valueAtReset	0
			value disable		0
			value disabled		0
			value enable		1
			value enabled		1
			}
		}
}
}
}
}
}
}
