/*
   Copyright (C) 2024 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_hw_st_stm32_h7_spi_maph_
#define _oscl_hw_st_stm32_h7_spi_maph_
#include "reg.h"
namespace Oscl {
namespace St {
namespace Stm32 {
namespace H7 {
namespace pSPI {
struct Map {
	/** offset:0x0 */
	volatile Oscl::St::Stm32::H7::pSPI::rCR1::Reg	cr1;
	/** offset:0x4 */
	volatile Oscl::St::Stm32::H7::pSPI::rCR2::Reg	cr2;
	/** offset:0x8 */
	volatile Oscl::St::Stm32::H7::pSPI::rCFG1::Reg	cfg1;
	/** offset:0xc */
	volatile Oscl::St::Stm32::H7::pSPI::rCFG2::Reg	cfg2;
	/** offset:0x10 */
	volatile Oscl::St::Stm32::H7::pSPI::rIER::Reg	ier;
	/** offset:0x14 */
	volatile Oscl::St::Stm32::H7::pSPI::rSR::Reg	sr;
	/** offset:0x18 */
	volatile Oscl::St::Stm32::H7::pSPI::rIFCR::Reg	ifcr;
	const uint8_t	_reserve28[4];
	/** offset:0x20 */
	volatile Oscl::St::Stm32::H7::pSPI::rTXDR::Reg	txdr;
	const uint8_t	_reserve36[12];
	/** offset:0x30 */
	volatile Oscl::St::Stm32::H7::pSPI::rRXDR::Reg	rxdr;
	const uint8_t	_reserve52[12];
	/** offset:0x40 */
	volatile Oscl::St::Stm32::H7::pSPI::rCRCPOLY::Reg	crcpoly;
	/** offset:0x44 */
	volatile Oscl::St::Stm32::H7::pSPI::rTXCRC::Reg	txcrc;
	/** offset:0x48 */
	volatile Oscl::St::Stm32::H7::pSPI::rRXCRC::Reg	rxcrc;
	/** offset:0x4c */
	volatile Oscl::St::Stm32::H7::pSPI::rUDRDR::Reg	udrdr;
	/** offset:0x50 */
	volatile Oscl::St::Stm32::H7::pSPI::rI2SCFGR::Reg	i2scfgr;
	};
}
}
}
}
}
#endif
