/*
   Copyright (C) 2024 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_hw_st_stm32_h7_spi_regh_
#define _oscl_hw_st_stm32_h7_spi_regh_
#include <stdint.h>
namespace Oscl { // Namespace description
	namespace St { // Namespace description
		namespace Stm32 { // Namespace description
			namespace H7 { // Namespace description
				namespace pSPI { // Namespace description
					namespace rCR1 { // Register description
						typedef uint32_t	Reg;
						constexpr Reg	valueAtReset = 0x00000000;
						namespace fSPE { // Field Description
							constexpr Reg Lsb = 0;
							constexpr Reg FieldMask = 0x00000001;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_disable = 0x0;
							constexpr Reg ValueMask_disable = 0x00000000;
							constexpr Reg Value_disabled = 0x0;
							constexpr Reg ValueMask_disabled = 0x00000000;
							constexpr Reg Value_enable = 0x1;
							constexpr Reg ValueMask_enable = 0x00000001;
							constexpr Reg Value_enabled = 0x1;
							constexpr Reg ValueMask_enabled = 0x00000001;
							};
						namespace fMASRX { // Field Description
							constexpr Reg Lsb = 8;
							constexpr Reg FieldMask = 0x00000100;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_continuous = 0x0;
							constexpr Reg ValueMask_continuous = 0x00000000;
							constexpr Reg Value_suspend = 0x1;
							constexpr Reg ValueMask_suspend = 0x00000100;
							};
						namespace fCSTART { // Field Description
							constexpr Reg Lsb = 9;
							constexpr Reg FieldMask = 0x00000200;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_idle = 0x0;
							constexpr Reg ValueMask_idle = 0x00000000;
							constexpr Reg Value_start = 0x1;
							constexpr Reg ValueMask_start = 0x00000200;
							};
						namespace fCSUSP { // Field Description
							constexpr Reg Lsb = 10;
							constexpr Reg FieldMask = 0x00000400;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_noEffect = 0x0;
							constexpr Reg ValueMask_noEffect = 0x00000000;
							constexpr Reg Value_suspend = 0x1;
							constexpr Reg ValueMask_suspend = 0x00000400;
							};
						namespace fHDDIR { // Field Description
							constexpr Reg Lsb = 11;
							constexpr Reg FieldMask = 0x00000800;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_receiver = 0x0;
							constexpr Reg ValueMask_receiver = 0x00000000;
							constexpr Reg Value_transmitter = 0x1;
							constexpr Reg ValueMask_transmitter = 0x00000800;
							};
						namespace fSSI { // Field Description
							constexpr Reg Lsb = 12;
							constexpr Reg FieldMask = 0x00001000;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_low = 0x0;
							constexpr Reg ValueMask_low = 0x00000000;
							constexpr Reg Value_high = 0x1;
							constexpr Reg ValueMask_high = 0x00001000;
							};
						namespace fCRC33_17 { // Field Description
							constexpr Reg Lsb = 13;
							constexpr Reg FieldMask = 0x00002000;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_notUsed = 0x0;
							constexpr Reg ValueMask_notUsed = 0x00000000;
							constexpr Reg Value_used = 0x1;
							constexpr Reg ValueMask_used = 0x00002000;
							};
						namespace fRCRCI { // Field Description
							constexpr Reg Lsb = 14;
							constexpr Reg FieldMask = 0x00004000;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_allZeroPattern = 0x0;
							constexpr Reg ValueMask_allZeroPattern = 0x00000000;
							constexpr Reg Value_allOnesPattern = 0x1;
							constexpr Reg ValueMask_allOnesPattern = 0x00004000;
							};
						namespace fTCRCI { // Field Description
							constexpr Reg Lsb = 15;
							constexpr Reg FieldMask = 0x00008000;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_allZeroPattern = 0x0;
							constexpr Reg ValueMask_allZeroPattern = 0x00000000;
							constexpr Reg Value_allOnesPattern = 0x1;
							constexpr Reg ValueMask_allOnesPattern = 0x00008000;
							};
						namespace fIOLOCK { // Field Description
							constexpr Reg Lsb = 16;
							constexpr Reg FieldMask = 0x00010000;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_notLocked = 0x0;
							constexpr Reg ValueMask_notLocked = 0x00000000;
							constexpr Reg Value_locked = 0x1;
							constexpr Reg ValueMask_locked = 0x00010000;
							};
						};
					namespace rCR2 { // Register description
						typedef uint32_t	Reg;
						constexpr Reg	valueAtReset = 0x00000000;
						namespace fTSIZE { // Field Description
							constexpr Reg Lsb = 0;
							constexpr Reg FieldMask = 0x0000FFFF;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_minValue = 0x0;
							constexpr Reg ValueMask_minValue = 0x00000000;
							constexpr Reg Value_maxValue = 0xFFFF;
							constexpr Reg ValueMask_maxValue = 0x0000FFFF;
							};
						namespace fTSER { // Field Description
							constexpr Reg Lsb = 16;
							constexpr Reg FieldMask = 0xFFFF0000;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_minValue = 0x0;
							constexpr Reg ValueMask_minValue = 0x00000000;
							constexpr Reg Value_maxValue = 0xFFFF;
							constexpr Reg ValueMask_maxValue = 0xFFFF0000;
							};
						};
					namespace rCFG1 { // Register description
						typedef uint32_t	Reg;
						constexpr Reg	valueAtReset = 0x00070007;
						namespace fDSIZE { // Field Description
							constexpr Reg Lsb = 0;
							constexpr Reg FieldMask = 0x0000001F;
							constexpr Reg Value_valueAtReset = 0x7;
							constexpr Reg ValueMask_valueAtReset = 0x00000007;
							constexpr Reg Value_minValue = 0x3;
							constexpr Reg ValueMask_minValue = 0x00000003;
							constexpr Reg Value_maxValue = 0x1F;
							constexpr Reg ValueMask_maxValue = 0x0000001F;
							constexpr Reg Value_v4Bits = 0x3;
							constexpr Reg ValueMask_v4Bits = 0x00000003;
							constexpr Reg Value_v5Bits = 0x4;
							constexpr Reg ValueMask_v5Bits = 0x00000004;
							constexpr Reg Value_v6Bits = 0x5;
							constexpr Reg ValueMask_v6Bits = 0x00000005;
							constexpr Reg Value_v7Bits = 0x6;
							constexpr Reg ValueMask_v7Bits = 0x00000006;
							constexpr Reg Value_v8Bits = 0x7;
							constexpr Reg ValueMask_v8Bits = 0x00000007;
							constexpr Reg Value_v16Bits = 0xF;
							constexpr Reg ValueMask_v16Bits = 0x0000000F;
							constexpr Reg Value_v32Bits = 0x1F;
							constexpr Reg ValueMask_v32Bits = 0x0000001F;
							};
						namespace fFTHLV { // Field Description
							constexpr Reg Lsb = 5;
							constexpr Reg FieldMask = 0x000001E0;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_minValue = 0x0;
							constexpr Reg ValueMask_minValue = 0x00000000;
							constexpr Reg Value_maxValue = 0xF;
							constexpr Reg ValueMask_maxValue = 0x000001E0;
							};
						namespace fUDRCFG { // Field Description
							constexpr Reg Lsb = 9;
							constexpr Reg FieldMask = 0x00000600;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_sendUDRDR = 0x0;
							constexpr Reg ValueMask_sendUDRDR = 0x00000000;
							constexpr Reg Value_sendLastRx = 0x1;
							constexpr Reg ValueMask_sendLastRx = 0x00000200;
							constexpr Reg Value_sendLastTx = 0x2;
							constexpr Reg ValueMask_sendLastTx = 0x00000400;
							};
						namespace fUDRDET { // Field Description
							constexpr Reg Lsb = 11;
							constexpr Reg FieldMask = 0x00001800;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_beginningOfDataFrame = 0x0;
							constexpr Reg ValueMask_beginningOfDataFrame = 0x00000000;
							constexpr Reg Value_endOfLastDataFrame = 0x1;
							constexpr Reg ValueMask_endOfLastDataFrame = 0x00000800;
							constexpr Reg Value_beginningOfActiveSS = 0x2;
							constexpr Reg ValueMask_beginningOfActiveSS = 0x00001000;
							};
						namespace fRXDMAEN { // Field Description
							constexpr Reg Lsb = 14;
							constexpr Reg FieldMask = 0x00004000;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_disable = 0x0;
							constexpr Reg ValueMask_disable = 0x00000000;
							constexpr Reg Value_disabled = 0x0;
							constexpr Reg ValueMask_disabled = 0x00000000;
							constexpr Reg Value_enable = 0x1;
							constexpr Reg ValueMask_enable = 0x00004000;
							constexpr Reg Value_enabled = 0x1;
							constexpr Reg ValueMask_enabled = 0x00004000;
							};
						namespace fTXDMAEN { // Field Description
							constexpr Reg Lsb = 15;
							constexpr Reg FieldMask = 0x00008000;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_disable = 0x0;
							constexpr Reg ValueMask_disable = 0x00000000;
							constexpr Reg Value_disabled = 0x0;
							constexpr Reg ValueMask_disabled = 0x00000000;
							constexpr Reg Value_enable = 0x1;
							constexpr Reg ValueMask_enable = 0x00008000;
							constexpr Reg Value_enabled = 0x1;
							constexpr Reg ValueMask_enabled = 0x00008000;
							};
						namespace fCRCSIZE { // Field Description
							constexpr Reg Lsb = 16;
							constexpr Reg FieldMask = 0x001F0000;
							constexpr Reg Value_valueAtReset = 0x7;
							constexpr Reg ValueMask_valueAtReset = 0x00070000;
							constexpr Reg Value_minValue = 0x3;
							constexpr Reg ValueMask_minValue = 0x00030000;
							constexpr Reg Value_maxValue = 0x1F;
							constexpr Reg ValueMask_maxValue = 0x001F0000;
							constexpr Reg Value_v8Bits = 0x7;
							constexpr Reg ValueMask_v8Bits = 0x00070000;
							constexpr Reg Value_v16Bits = 0xF;
							constexpr Reg ValueMask_v16Bits = 0x000F0000;
							};
						namespace fCRCEN { // Field Description
							constexpr Reg Lsb = 22;
							constexpr Reg FieldMask = 0x00400000;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_disable = 0x0;
							constexpr Reg ValueMask_disable = 0x00000000;
							constexpr Reg Value_disabled = 0x0;
							constexpr Reg ValueMask_disabled = 0x00000000;
							constexpr Reg Value_enable = 0x1;
							constexpr Reg ValueMask_enable = 0x00400000;
							constexpr Reg Value_enabled = 0x1;
							constexpr Reg ValueMask_enabled = 0x00400000;
							};
						namespace fMBR { // Field Description
							constexpr Reg Lsb = 28;
							constexpr Reg FieldMask = 0x70000000;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_divideBy2 = 0x0;
							constexpr Reg ValueMask_divideBy2 = 0x00000000;
							constexpr Reg Value_divideBy4 = 0x1;
							constexpr Reg ValueMask_divideBy4 = 0x10000000;
							constexpr Reg Value_divideBy8 = 0x2;
							constexpr Reg ValueMask_divideBy8 = 0x20000000;
							constexpr Reg Value_divideBy16 = 0x3;
							constexpr Reg ValueMask_divideBy16 = 0x30000000;
							constexpr Reg Value_divideBy32 = 0x4;
							constexpr Reg ValueMask_divideBy32 = 0x40000000;
							constexpr Reg Value_divideBy64 = 0x5;
							constexpr Reg ValueMask_divideBy64 = 0x50000000;
							constexpr Reg Value_divideBy128 = 0x6;
							constexpr Reg ValueMask_divideBy128 = 0x60000000;
							constexpr Reg Value_divideBy256 = 0x7;
							constexpr Reg ValueMask_divideBy256 = 0x70000000;
							};
						};
					namespace rCFG2 { // Register description
						typedef uint32_t	Reg;
						constexpr Reg	valueAtReset = 0x00000000;
						namespace fMSSI { // Field Description
							constexpr Reg Lsb = 0;
							constexpr Reg FieldMask = 0x0000000F;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_minValue = 0x0;
							constexpr Reg ValueMask_minValue = 0x00000000;
							constexpr Reg Value_maxValue = 0xF;
							constexpr Reg ValueMask_maxValue = 0x0000000F;
							constexpr Reg Value_v0Clocks = 0x0;
							constexpr Reg ValueMask_v0Clocks = 0x00000000;
							constexpr Reg Value_v15Clocks = 0xF;
							constexpr Reg ValueMask_v15Clocks = 0x0000000F;
							};
						namespace fMIDI { // Field Description
							constexpr Reg Lsb = 4;
							constexpr Reg FieldMask = 0x000000F0;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_minValue = 0x0;
							constexpr Reg ValueMask_minValue = 0x00000000;
							constexpr Reg Value_maxValue = 0xF;
							constexpr Reg ValueMask_maxValue = 0x000000F0;
							constexpr Reg Value_v0Clocks = 0x0;
							constexpr Reg ValueMask_v0Clocks = 0x00000000;
							constexpr Reg Value_v15Clocks = 0xF;
							constexpr Reg ValueMask_v15Clocks = 0x000000F0;
							};
						namespace fIOSWP { // Field Description
							constexpr Reg Lsb = 15;
							constexpr Reg FieldMask = 0x00008000;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_noSWAP = 0x0;
							constexpr Reg ValueMask_noSWAP = 0x00000000;
							constexpr Reg Value_swap = 0x1;
							constexpr Reg ValueMask_swap = 0x00008000;
							};
						namespace fCOMM { // Field Description
							constexpr Reg Lsb = 17;
							constexpr Reg FieldMask = 0x00060000;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_fullDuplex = 0x0;
							constexpr Reg ValueMask_fullDuplex = 0x00000000;
							constexpr Reg Value_simplexTx = 0x1;
							constexpr Reg ValueMask_simplexTx = 0x00020000;
							constexpr Reg Value_simplexRx = 0x2;
							constexpr Reg ValueMask_simplexRx = 0x00040000;
							constexpr Reg Value_halfDuplex = 0x3;
							constexpr Reg ValueMask_halfDuplex = 0x00060000;
							};
						namespace fSP { // Field Description
							constexpr Reg Lsb = 19;
							constexpr Reg FieldMask = 0x00380000;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_motorola = 0x0;
							constexpr Reg ValueMask_motorola = 0x00000000;
							constexpr Reg Value_ti = 0x1;
							constexpr Reg ValueMask_ti = 0x00080000;
							};
						namespace fMASTER { // Field Description
							constexpr Reg Lsb = 22;
							constexpr Reg FieldMask = 0x00400000;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_slave = 0x0;
							constexpr Reg ValueMask_slave = 0x00000000;
							constexpr Reg Value_master = 0x1;
							constexpr Reg ValueMask_master = 0x00400000;
							};
						namespace fLSBFRST { // Field Description
							constexpr Reg Lsb = 23;
							constexpr Reg FieldMask = 0x00800000;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_msb = 0x0;
							constexpr Reg ValueMask_msb = 0x00000000;
							constexpr Reg Value_lsb = 0x1;
							constexpr Reg ValueMask_lsb = 0x00800000;
							};
						namespace fCPHA { // Field Description
							constexpr Reg Lsb = 24;
							constexpr Reg FieldMask = 0x01000000;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_firstClockEdge = 0x0;
							constexpr Reg ValueMask_firstClockEdge = 0x00000000;
							constexpr Reg Value_secondClockEdge = 0x1;
							constexpr Reg ValueMask_secondClockEdge = 0x01000000;
							};
						namespace fCPOL { // Field Description
							constexpr Reg Lsb = 25;
							constexpr Reg FieldMask = 0x02000000;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_idleLOW = 0x0;
							constexpr Reg ValueMask_idleLOW = 0x00000000;
							constexpr Reg Value_idleHIGH = 0x0;
							constexpr Reg ValueMask_idleHIGH = 0x00000000;
							};
						namespace fSSM { // Field Description
							constexpr Reg Lsb = 26;
							constexpr Reg FieldMask = 0x04000000;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_pad = 0x0;
							constexpr Reg ValueMask_pad = 0x00000000;
							constexpr Reg Value_ssi = 0x1;
							constexpr Reg ValueMask_ssi = 0x04000000;
							};
						namespace fSSIOP { // Field Description
							constexpr Reg Lsb = 28;
							constexpr Reg FieldMask = 0x10000000;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_activeLOW = 0x0;
							constexpr Reg ValueMask_activeLOW = 0x00000000;
							constexpr Reg Value_activeHIGH = 0x1;
							constexpr Reg ValueMask_activeHIGH = 0x10000000;
							};
						namespace fSSOE { // Field Description
							constexpr Reg Lsb = 29;
							constexpr Reg FieldMask = 0x20000000;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_disable = 0x0;
							constexpr Reg ValueMask_disable = 0x00000000;
							constexpr Reg Value_disabled = 0x0;
							constexpr Reg ValueMask_disabled = 0x00000000;
							constexpr Reg Value_enable = 0x1;
							constexpr Reg ValueMask_enable = 0x20000000;
							constexpr Reg Value_enabled = 0x1;
							constexpr Reg ValueMask_enabled = 0x20000000;
							};
						namespace fSSOM { // Field Description
							constexpr Reg Lsb = 30;
							constexpr Reg FieldMask = 0x40000000;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_keepActiveUntilEOT = 0x0;
							constexpr Reg ValueMask_keepActiveUntilEOT = 0x00000000;
							constexpr Reg Value_inactiveBetweenFrames = 0x1;
							constexpr Reg ValueMask_inactiveBetweenFrames = 0x40000000;
							};
						namespace fAFCNTR { // Field Description
							constexpr Reg Lsb = 31;
							constexpr Reg FieldMask = 0x80000000;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_disable = 0x0;
							constexpr Reg ValueMask_disable = 0x00000000;
							constexpr Reg Value_disabled = 0x0;
							constexpr Reg ValueMask_disabled = 0x00000000;
							constexpr Reg Value_enable = 0x1;
							constexpr Reg ValueMask_enable = 0x80000000;
							constexpr Reg Value_enabled = 0x1;
							constexpr Reg ValueMask_enabled = 0x80000000;
							};
						};
					namespace rIER { // Register description
						typedef uint32_t	Reg;
						constexpr Reg	valueAtReset = 0x00000000;
						namespace fRXPIE { // Field Description
							constexpr Reg Lsb = 0;
							constexpr Reg FieldMask = 0x00000001;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_disable = 0x0;
							constexpr Reg ValueMask_disable = 0x00000000;
							constexpr Reg Value_disabled = 0x0;
							constexpr Reg ValueMask_disabled = 0x00000000;
							constexpr Reg Value_enable = 0x1;
							constexpr Reg ValueMask_enable = 0x00000001;
							constexpr Reg Value_enabled = 0x1;
							constexpr Reg ValueMask_enabled = 0x00000001;
							};
						namespace fTXPIE { // Field Description
							constexpr Reg Lsb = 1;
							constexpr Reg FieldMask = 0x00000002;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_disable = 0x0;
							constexpr Reg ValueMask_disable = 0x00000000;
							constexpr Reg Value_disabled = 0x0;
							constexpr Reg ValueMask_disabled = 0x00000000;
							constexpr Reg Value_enable = 0x1;
							constexpr Reg ValueMask_enable = 0x00000002;
							constexpr Reg Value_enabled = 0x1;
							constexpr Reg ValueMask_enabled = 0x00000002;
							};
						namespace fDPXPIE { // Field Description
							constexpr Reg Lsb = 2;
							constexpr Reg FieldMask = 0x00000004;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_disable = 0x0;
							constexpr Reg ValueMask_disable = 0x00000000;
							constexpr Reg Value_disabled = 0x0;
							constexpr Reg ValueMask_disabled = 0x00000000;
							constexpr Reg Value_enable = 0x1;
							constexpr Reg ValueMask_enable = 0x00000004;
							constexpr Reg Value_enabled = 0x1;
							constexpr Reg ValueMask_enabled = 0x00000004;
							};
						namespace fEOTIE { // Field Description
							constexpr Reg Lsb = 3;
							constexpr Reg FieldMask = 0x00000008;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_disable = 0x0;
							constexpr Reg ValueMask_disable = 0x00000000;
							constexpr Reg Value_disabled = 0x0;
							constexpr Reg ValueMask_disabled = 0x00000000;
							constexpr Reg Value_enable = 0x1;
							constexpr Reg ValueMask_enable = 0x00000008;
							constexpr Reg Value_enabled = 0x1;
							constexpr Reg ValueMask_enabled = 0x00000008;
							};
						namespace fTXTFIE { // Field Description
							constexpr Reg Lsb = 4;
							constexpr Reg FieldMask = 0x00000010;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_disable = 0x0;
							constexpr Reg ValueMask_disable = 0x00000000;
							constexpr Reg Value_disabled = 0x0;
							constexpr Reg ValueMask_disabled = 0x00000000;
							constexpr Reg Value_enable = 0x1;
							constexpr Reg ValueMask_enable = 0x00000010;
							constexpr Reg Value_enabled = 0x1;
							constexpr Reg ValueMask_enabled = 0x00000010;
							};
						namespace fUDRIE { // Field Description
							constexpr Reg Lsb = 5;
							constexpr Reg FieldMask = 0x00000020;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_disable = 0x0;
							constexpr Reg ValueMask_disable = 0x00000000;
							constexpr Reg Value_disabled = 0x0;
							constexpr Reg ValueMask_disabled = 0x00000000;
							constexpr Reg Value_enable = 0x1;
							constexpr Reg ValueMask_enable = 0x00000020;
							constexpr Reg Value_enabled = 0x1;
							constexpr Reg ValueMask_enabled = 0x00000020;
							};
						namespace fOVRIE { // Field Description
							constexpr Reg Lsb = 6;
							constexpr Reg FieldMask = 0x00000040;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_disable = 0x0;
							constexpr Reg ValueMask_disable = 0x00000000;
							constexpr Reg Value_disabled = 0x0;
							constexpr Reg ValueMask_disabled = 0x00000000;
							constexpr Reg Value_enable = 0x1;
							constexpr Reg ValueMask_enable = 0x00000040;
							constexpr Reg Value_enabled = 0x1;
							constexpr Reg ValueMask_enabled = 0x00000040;
							};
						namespace fCRCEIE { // Field Description
							constexpr Reg Lsb = 7;
							constexpr Reg FieldMask = 0x00000080;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_disable = 0x0;
							constexpr Reg ValueMask_disable = 0x00000000;
							constexpr Reg Value_disabled = 0x0;
							constexpr Reg ValueMask_disabled = 0x00000000;
							constexpr Reg Value_enable = 0x1;
							constexpr Reg ValueMask_enable = 0x00000080;
							constexpr Reg Value_enabled = 0x1;
							constexpr Reg ValueMask_enabled = 0x00000080;
							};
						namespace fTIFREIE { // Field Description
							constexpr Reg Lsb = 8;
							constexpr Reg FieldMask = 0x00000100;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_disable = 0x0;
							constexpr Reg ValueMask_disable = 0x00000000;
							constexpr Reg Value_disabled = 0x0;
							constexpr Reg ValueMask_disabled = 0x00000000;
							constexpr Reg Value_enable = 0x1;
							constexpr Reg ValueMask_enable = 0x00000100;
							constexpr Reg Value_enabled = 0x1;
							constexpr Reg ValueMask_enabled = 0x00000100;
							};
						namespace fMODFIE { // Field Description
							constexpr Reg Lsb = 9;
							constexpr Reg FieldMask = 0x00000200;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_disable = 0x0;
							constexpr Reg ValueMask_disable = 0x00000000;
							constexpr Reg Value_disabled = 0x0;
							constexpr Reg ValueMask_disabled = 0x00000000;
							constexpr Reg Value_enable = 0x1;
							constexpr Reg ValueMask_enable = 0x00000200;
							constexpr Reg Value_enabled = 0x1;
							constexpr Reg ValueMask_enabled = 0x00000200;
							};
						namespace fTSERFIE { // Field Description
							constexpr Reg Lsb = 10;
							constexpr Reg FieldMask = 0x00000400;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_disable = 0x0;
							constexpr Reg ValueMask_disable = 0x00000000;
							constexpr Reg Value_disabled = 0x0;
							constexpr Reg ValueMask_disabled = 0x00000000;
							constexpr Reg Value_enable = 0x1;
							constexpr Reg ValueMask_enable = 0x00000400;
							constexpr Reg Value_enabled = 0x1;
							constexpr Reg ValueMask_enabled = 0x00000400;
							};
						};
					namespace rSR { // Register description
						typedef uint32_t	Reg;
						constexpr Reg	valueAtReset = 0x00001002;
						namespace fRXP { // Field Description
							constexpr Reg Lsb = 0;
							constexpr Reg FieldMask = 0x00000001;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_emptyOrIncomplete = 0x0;
							constexpr Reg ValueMask_emptyOrIncomplete = 0x00000000;
							constexpr Reg Value_notEmpty = 0x1;
							constexpr Reg ValueMask_notEmpty = 0x00000001;
							};
						namespace fTXP { // Field Description
							constexpr Reg Lsb = 1;
							constexpr Reg FieldMask = 0x00000002;
							constexpr Reg Value_valueAtReset = 0x1;
							constexpr Reg ValueMask_valueAtReset = 0x00000002;
							constexpr Reg Value_full = 0x0;
							constexpr Reg ValueMask_full = 0x00000000;
							constexpr Reg Value_notFull = 0x1;
							constexpr Reg ValueMask_notFull = 0x00000002;
							};
						namespace fDXP { // Field Description
							constexpr Reg Lsb = 2;
							constexpr Reg FieldMask = 0x00000004;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_txFullAndOrRxEmpty = 0x0;
							constexpr Reg ValueMask_txFullAndOrRxEmpty = 0x00000000;
							constexpr Reg Value_txNotFullAndRxNotFull = 0x1;
							constexpr Reg ValueMask_txNotFullAndRxNotFull = 0x00000004;
							};
						namespace fEOT { // Field Description
							constexpr Reg Lsb = 3;
							constexpr Reg FieldMask = 0x00000008;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_busyOrIdle = 0x0;
							constexpr Reg ValueMask_busyOrIdle = 0x00000000;
							constexpr Reg Value_complete = 0x1;
							constexpr Reg ValueMask_complete = 0x00000008;
							};
						namespace fTXTF { // Field Description
							constexpr Reg Lsb = 4;
							constexpr Reg FieldMask = 0x00000010;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_busyOrIdle = 0x0;
							constexpr Reg ValueMask_busyOrIdle = 0x00000000;
							constexpr Reg Value_finished = 0x1;
							constexpr Reg ValueMask_finished = 0x00000010;
							};
						namespace fUDR { // Field Description
							constexpr Reg Lsb = 5;
							constexpr Reg FieldMask = 0x00000020;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_noError = 0x0;
							constexpr Reg ValueMask_noError = 0x00000000;
							constexpr Reg Value_underrun = 0x1;
							constexpr Reg ValueMask_underrun = 0x00000020;
							};
						namespace fOVR { // Field Description
							constexpr Reg Lsb = 6;
							constexpr Reg FieldMask = 0x00000040;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_noError = 0x0;
							constexpr Reg ValueMask_noError = 0x00000000;
							constexpr Reg Value_overrun = 0x1;
							constexpr Reg ValueMask_overrun = 0x00000040;
							};
						namespace fCRCE { // Field Description
							constexpr Reg Lsb = 7;
							constexpr Reg FieldMask = 0x00000080;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_noError = 0x0;
							constexpr Reg ValueMask_noError = 0x00000000;
							constexpr Reg Value_error = 0x1;
							constexpr Reg ValueMask_error = 0x00000080;
							};
						namespace fTIFRE { // Field Description
							constexpr Reg Lsb = 8;
							constexpr Reg FieldMask = 0x00000100;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_noError = 0x0;
							constexpr Reg ValueMask_noError = 0x00000000;
							constexpr Reg Value_error = 0x1;
							constexpr Reg ValueMask_error = 0x00000100;
							};
						namespace fMODF { // Field Description
							constexpr Reg Lsb = 9;
							constexpr Reg FieldMask = 0x00000200;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_noFault = 0x0;
							constexpr Reg ValueMask_noFault = 0x00000000;
							constexpr Reg Value_fault = 0x1;
							constexpr Reg ValueMask_fault = 0x00000200;
							};
						namespace fTSERF { // Field Description
							constexpr Reg Lsb = 10;
							constexpr Reg FieldMask = 0x00000400;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_noAcceptation = 0x0;
							constexpr Reg ValueMask_noAcceptation = 0x00000000;
							constexpr Reg Value_dataAccepted = 0x1;
							constexpr Reg ValueMask_dataAccepted = 0x00000400;
							};
						namespace fSUSP { // Field Description
							constexpr Reg Lsb = 11;
							constexpr Reg FieldMask = 0x00000800;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_notSuspended = 0x0;
							constexpr Reg ValueMask_notSuspended = 0x00000000;
							constexpr Reg Value_suspended = 0x1;
							constexpr Reg ValueMask_suspended = 0x00000800;
							};
						namespace fTXC { // Field Description
							constexpr Reg Lsb = 12;
							constexpr Reg FieldMask = 0x00001000;
							constexpr Reg Value_valueAtReset = 0x1;
							constexpr Reg ValueMask_valueAtReset = 0x00001000;
							constexpr Reg Value_ongoing = 0x0;
							constexpr Reg ValueMask_ongoing = 0x00000000;
							constexpr Reg Value_complete = 0x1;
							constexpr Reg ValueMask_complete = 0x00001000;
							};
						namespace fRXPLVL { // Field Description
							constexpr Reg Lsb = 13;
							constexpr Reg FieldMask = 0x00006000;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_noNextFrameAvailable = 0x0;
							constexpr Reg ValueMask_noNextFrameAvailable = 0x00000000;
							constexpr Reg Value_oneFrameAvailable = 0x1;
							constexpr Reg ValueMask_oneFrameAvailable = 0x00002000;
							constexpr Reg Value_twoFramesAvailable = 0x2;
							constexpr Reg ValueMask_twoFramesAvailable = 0x00004000;
							constexpr Reg Value_threeFramesAvailable = 0x3;
							constexpr Reg ValueMask_threeFramesAvailable = 0x00006000;
							};
						namespace fRXWNE { // Field Description
							constexpr Reg Lsb = 15;
							constexpr Reg FieldMask = 0x00008000;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_lessThanFourBytes = 0x0;
							constexpr Reg ValueMask_lessThanFourBytes = 0x00000000;
							constexpr Reg Value_atLeastFourBytes = 0x1;
							constexpr Reg ValueMask_atLeastFourBytes = 0x00008000;
							};
						namespace fCTSIZE { // Field Description
							constexpr Reg Lsb = 16;
							constexpr Reg FieldMask = 0xFFFF0000;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_minValue = 0x0;
							constexpr Reg ValueMask_minValue = 0x00000000;
							constexpr Reg Value_maxValue = 0x0;
							constexpr Reg ValueMask_maxValue = 0x00000000;
							};
						};
					namespace rIFCR { // Register description
						typedef uint32_t	Reg;
						constexpr Reg	valueAtReset = 0x00000000;
						namespace fEOTC { // Field Description
							constexpr Reg Lsb = 3;
							constexpr Reg FieldMask = 0x00000008;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_noEffect = 0x0;
							constexpr Reg ValueMask_noEffect = 0x00000000;
							constexpr Reg Value_clear = 0x1;
							constexpr Reg ValueMask_clear = 0x00000008;
							};
						namespace fTXTFC { // Field Description
							constexpr Reg Lsb = 4;
							constexpr Reg FieldMask = 0x00000010;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_noEffect = 0x0;
							constexpr Reg ValueMask_noEffect = 0x00000000;
							constexpr Reg Value_clear = 0x1;
							constexpr Reg ValueMask_clear = 0x00000010;
							};
						namespace fUDRC { // Field Description
							constexpr Reg Lsb = 5;
							constexpr Reg FieldMask = 0x00000020;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_noEffect = 0x0;
							constexpr Reg ValueMask_noEffect = 0x00000000;
							constexpr Reg Value_clear = 0x1;
							constexpr Reg ValueMask_clear = 0x00000020;
							};
						namespace fOVRC { // Field Description
							constexpr Reg Lsb = 6;
							constexpr Reg FieldMask = 0x00000040;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_noEffect = 0x0;
							constexpr Reg ValueMask_noEffect = 0x00000000;
							constexpr Reg Value_clear = 0x1;
							constexpr Reg ValueMask_clear = 0x00000040;
							};
						namespace fCRCEC { // Field Description
							constexpr Reg Lsb = 7;
							constexpr Reg FieldMask = 0x00000080;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_noEffect = 0x0;
							constexpr Reg ValueMask_noEffect = 0x00000000;
							constexpr Reg Value_clear = 0x1;
							constexpr Reg ValueMask_clear = 0x00000080;
							};
						namespace fTIFREC { // Field Description
							constexpr Reg Lsb = 8;
							constexpr Reg FieldMask = 0x00000100;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_noEffect = 0x0;
							constexpr Reg ValueMask_noEffect = 0x00000000;
							constexpr Reg Value_clear = 0x1;
							constexpr Reg ValueMask_clear = 0x00000100;
							};
						namespace fMODFC { // Field Description
							constexpr Reg Lsb = 9;
							constexpr Reg FieldMask = 0x00000200;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_noEffect = 0x0;
							constexpr Reg ValueMask_noEffect = 0x00000000;
							constexpr Reg Value_clear = 0x1;
							constexpr Reg ValueMask_clear = 0x00000200;
							};
						namespace fTSERFC { // Field Description
							constexpr Reg Lsb = 10;
							constexpr Reg FieldMask = 0x00000400;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_noEffect = 0x0;
							constexpr Reg ValueMask_noEffect = 0x00000000;
							constexpr Reg Value_clear = 0x1;
							constexpr Reg ValueMask_clear = 0x00000400;
							};
						namespace fSUSPC { // Field Description
							constexpr Reg Lsb = 11;
							constexpr Reg FieldMask = 0x00000800;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_noEffect = 0x0;
							constexpr Reg ValueMask_noEffect = 0x00000000;
							constexpr Reg Value_clear = 0x1;
							constexpr Reg ValueMask_clear = 0x00000800;
							};
						};
					namespace rTXDR { // Register description
						typedef uint32_t	Reg;
						constexpr Reg	valueAtReset = 0x00000000;
						namespace fTXDR { // Field Description
							constexpr Reg Lsb = 0;
							constexpr Reg FieldMask = 0xFFFFFFFF;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_minValue = 0x0;
							constexpr Reg ValueMask_minValue = 0x00000000;
							constexpr Reg Value_maxValue = 0xFFFFFFFF;
							constexpr Reg ValueMask_maxValue = 0xFFFFFFFF;
							};
						};
					namespace rRXDR { // Register description
						typedef uint32_t	Reg;
						constexpr Reg	valueAtReset = 0x00000000;
						namespace fRXDR { // Field Description
							constexpr Reg Lsb = 0;
							constexpr Reg FieldMask = 0xFFFFFFFF;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							};
						};
					namespace rCRCPOLY { // Register description
						typedef uint32_t	Reg;
						constexpr Reg	valueAtReset = 0x00000107;
						namespace fCRCPOLY { // Field Description
							constexpr Reg Lsb = 0;
							constexpr Reg FieldMask = 0xFFFFFFFF;
							constexpr Reg Value_valueAtReset = 0x107;
							constexpr Reg ValueMask_valueAtReset = 0x00000107;
							};
						};
					namespace rTXCRC { // Register description
						typedef uint32_t	Reg;
						constexpr Reg	valueAtReset = 0x00000000;
						namespace fTXCRC { // Field Description
							constexpr Reg Lsb = 0;
							constexpr Reg FieldMask = 0xFFFFFFFF;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							};
						};
					namespace rRXCRC { // Register description
						typedef uint32_t	Reg;
						constexpr Reg	valueAtReset = 0x00000000;
						namespace fRXCRC { // Field Description
							constexpr Reg Lsb = 0;
							constexpr Reg FieldMask = 0xFFFFFFFF;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							};
						};
					namespace rUDRDR { // Register description
						typedef uint32_t	Reg;
						constexpr Reg	valueAtReset = 0x00000000;
						namespace fUDRDR { // Field Description
							constexpr Reg Lsb = 0;
							constexpr Reg FieldMask = 0xFFFFFFFF;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							};
						};
					namespace rI2SCFGR { // Register description
						typedef uint32_t	Reg;
						constexpr Reg	valueAtReset = 0x00000000;
						namespace fI2SMOD { // Field Description
							constexpr Reg Lsb = 0;
							constexpr Reg FieldMask = 0x00000001;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_spi = 0x0;
							constexpr Reg ValueMask_spi = 0x00000000;
							constexpr Reg Value_i2sPCM = 0x1;
							constexpr Reg ValueMask_i2sPCM = 0x00000001;
							};
						namespace fI2SCFG { // Field Description
							constexpr Reg Lsb = 1;
							constexpr Reg FieldMask = 0x0000000E;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_slaveTransmit = 0x0;
							constexpr Reg ValueMask_slaveTransmit = 0x00000000;
							constexpr Reg Value_slaveReceive = 0x1;
							constexpr Reg ValueMask_slaveReceive = 0x00000002;
							constexpr Reg Value_masterTransmit = 0x2;
							constexpr Reg ValueMask_masterTransmit = 0x00000004;
							constexpr Reg Value_masterReceive = 0x3;
							constexpr Reg ValueMask_masterReceive = 0x00000006;
							constexpr Reg Value_slaveFullDuplex = 0x4;
							constexpr Reg ValueMask_slaveFullDuplex = 0x00000008;
							constexpr Reg Value_masterFullDuplex = 0x5;
							constexpr Reg ValueMask_masterFullDuplex = 0x0000000A;
							};
						namespace fI2SSTD { // Field Description
							constexpr Reg Lsb = 4;
							constexpr Reg FieldMask = 0x00000030;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_i2SPhillips = 0x0;
							constexpr Reg ValueMask_i2SPhillips = 0x00000000;
							constexpr Reg Value_msbJustified = 0x1;
							constexpr Reg ValueMask_msbJustified = 0x00000010;
							constexpr Reg Value_lsbJustified = 0x2;
							constexpr Reg ValueMask_lsbJustified = 0x00000020;
							constexpr Reg Value_pcm = 0x3;
							constexpr Reg ValueMask_pcm = 0x00000030;
							};
						namespace fPCMSYNC { // Field Description
							constexpr Reg Lsb = 7;
							constexpr Reg FieldMask = 0x00000080;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_shortFrame = 0x0;
							constexpr Reg ValueMask_shortFrame = 0x00000000;
							constexpr Reg Value_longFrame = 0x1;
							constexpr Reg ValueMask_longFrame = 0x00000080;
							};
						namespace fDATLEN { // Field Description
							constexpr Reg Lsb = 8;
							constexpr Reg FieldMask = 0x00000300;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_v16Bit = 0x0;
							constexpr Reg ValueMask_v16Bit = 0x00000000;
							constexpr Reg Value_v24Bit = 0x1;
							constexpr Reg ValueMask_v24Bit = 0x00000100;
							constexpr Reg Value_v32Bit = 0x2;
							constexpr Reg ValueMask_v32Bit = 0x00000200;
							};
						namespace fCHLEN { // Field Description
							constexpr Reg Lsb = 10;
							constexpr Reg FieldMask = 0x00000400;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_v16Bit = 0x0;
							constexpr Reg ValueMask_v16Bit = 0x00000000;
							constexpr Reg Value_v32Bit = 0x1;
							constexpr Reg ValueMask_v32Bit = 0x00000400;
							};
						namespace fCKPOL { // Field Description
							constexpr Reg Lsb = 11;
							constexpr Reg FieldMask = 0x00000800;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_changeOnFallReadOnRise = 0x0;
							constexpr Reg ValueMask_changeOnFallReadOnRise = 0x00000000;
							constexpr Reg Value_changeOnRiseReadOnFall = 0x1;
							constexpr Reg ValueMask_changeOnRiseReadOnFall = 0x00000800;
							};
						namespace fFIXCH { // Field Description
							constexpr Reg Lsb = 12;
							constexpr Reg FieldMask = 0x00001000;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_ignoreCHLEN = 0x0;
							constexpr Reg ValueMask_ignoreCHLEN = 0x00000000;
							constexpr Reg Value_useCHLEN = 0x1;
							constexpr Reg ValueMask_useCHLEN = 0x00001000;
							};
						namespace fWSINV { // Field Description
							constexpr Reg Lsb = 13;
							constexpr Reg FieldMask = 0x00002000;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_noInvert = 0x0;
							constexpr Reg ValueMask_noInvert = 0x00000000;
							constexpr Reg Value_invert = 0x1;
							constexpr Reg ValueMask_invert = 0x00002000;
							constexpr Reg Value_disable = 0x0;
							constexpr Reg ValueMask_disable = 0x00000000;
							constexpr Reg Value_disabled = 0x0;
							constexpr Reg ValueMask_disabled = 0x00000000;
							constexpr Reg Value_enable = 0x1;
							constexpr Reg ValueMask_enable = 0x00002000;
							constexpr Reg Value_enabled = 0x1;
							constexpr Reg ValueMask_enabled = 0x00002000;
							};
						namespace fDATFMT { // Field Description
							constexpr Reg Lsb = 14;
							constexpr Reg FieldMask = 0x00004000;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_rightAlign = 0x0;
							constexpr Reg ValueMask_rightAlign = 0x00000000;
							constexpr Reg Value_leftAlign = 0x1;
							constexpr Reg ValueMask_leftAlign = 0x00004000;
							};
						namespace fI2SDIV { // Field Description
							constexpr Reg Lsb = 16;
							constexpr Reg FieldMask = 0x00FF0000;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_minValue = 0x0;
							constexpr Reg ValueMask_minValue = 0x00000000;
							constexpr Reg Value_maxValue = 0xFF;
							constexpr Reg ValueMask_maxValue = 0x00FF0000;
							constexpr Reg Value_excludeValueWhenOddIs1 = 0x1;
							constexpr Reg ValueMask_excludeValueWhenOddIs1 = 0x00010000;
							};
						namespace fODD { // Field Description
							constexpr Reg Lsb = 24;
							constexpr Reg FieldMask = 0x01000000;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_dividerIs2xI2sDiv = 0x0;
							constexpr Reg ValueMask_dividerIs2xI2sDiv = 0x00000000;
							constexpr Reg Value_dividerIs2xI2sDivPlus1 = 0x1;
							constexpr Reg ValueMask_dividerIs2xI2sDivPlus1 = 0x01000000;
							};
						namespace fMCKOE { // Field Description
							constexpr Reg Lsb = 25;
							constexpr Reg FieldMask = 0x02000000;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_disable = 0x0;
							constexpr Reg ValueMask_disable = 0x00000000;
							constexpr Reg Value_disabled = 0x0;
							constexpr Reg ValueMask_disabled = 0x00000000;
							constexpr Reg Value_enable = 0x1;
							constexpr Reg ValueMask_enable = 0x02000000;
							constexpr Reg Value_enabled = 0x1;
							constexpr Reg ValueMask_enabled = 0x02000000;
							};
						};
					}
				}
			}
		}
	}
#endif
