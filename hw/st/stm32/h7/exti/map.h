/*
   Copyright (C) 2024 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_hw_st_stm32_h7_exti_maph_
#define _oscl_hw_st_stm32_h7_exti_maph_
#include "reg.h"
namespace Oscl {
namespace St {
namespace Stm32 {
namespace H7 {
namespace pEXTI {

/** */
struct Exti {
	/** offset:0x0 */
	volatile Oscl::St::Stm32::H7::pEXTI::rRTSR::Reg	rtsr;
	/** offset:0x4 */
	volatile Oscl::St::Stm32::H7::pEXTI::rFTSR::Reg	ftsr;
	/** offset:0x8 */
	volatile Oscl::St::Stm32::H7::pEXTI::rSWIER::Reg	swier;
	/** offset:0xc */
	volatile Oscl::St::Stm32::H7::pEXTI::rD3PMR::Reg	d3pmr;
	/** offset:0x10 */
	volatile Oscl::St::Stm32::H7::pEXTI::rD3PCRL::Reg	d3pcrl;
	/** offset:0x14 */
	volatile Oscl::St::Stm32::H7::pEXTI::rD3PCRH::Reg	d3pcrh;

	const uint8_t	_reserve18[8];
	};

/** */
struct Cpu {
	/** offset:0x80 */
	volatile Oscl::St::Stm32::H7::pEXTI::rCPUIMR::Reg	imr;
	/** offset:0x84 */
	volatile Oscl::St::Stm32::H7::pEXTI::rCPUEMR::Reg	emr;
	/** offset:0x88 */
	volatile Oscl::St::Stm32::H7::pEXTI::rCPUPR::Reg	pr;
	const uint8_t	_reserve8C[4];
	};

/** */
struct Map {
#if 1
	/** offset 0x0 */
	Exti	group[3];
	const uint8_t	_reserve80[32];

	/** offset 0x80 */
	Cpu		cpu[3];
#else
	/** offset:0x0 */
	volatile Oscl::St::Stm32::H7::pEXTI::rRTSR1::Reg	rtsr1;
	/** offset:0x4 */
	volatile Oscl::St::Stm32::H7::pEXTI::rFTSR1::Reg	ftsr1;
	/** offset:0x8 */
	volatile Oscl::St::Stm32::H7::pEXTI::rSWIER1::Reg	swier1;
	/** offset:0xc */
	volatile Oscl::St::Stm32::H7::pEXTI::rD3PMR1::Reg	d3pmr1;
	/** offset:0x10 */
	volatile Oscl::St::Stm32::H7::pEXTI::rD3PCR1L::Reg	d3pcr1l;
	/** offset:0x14 */
	volatile Oscl::St::Stm32::H7::pEXTI::rD3PCR1H::Reg	d3pcr1h;
	const uint8_t	_reserve24[8];
	/** offset:0x20 */
	volatile Oscl::St::Stm32::H7::pEXTI::rRTSR2::Reg	rtsr2;
	/** offset:0x24 */
	volatile Oscl::St::Stm32::H7::pEXTI::rFTSR2::Reg	ftsr2;
	/** offset:0x28 */
	volatile Oscl::St::Stm32::H7::pEXTI::rSWIER2::Reg	swier2;
	/** offset:0x2c */
	volatile Oscl::St::Stm32::H7::pEXTI::rD3PMR2::Reg	d3pmr2;
	/** offset:0x30 */
	volatile Oscl::St::Stm32::H7::pEXTI::rD3PCR2L::Reg	d3pcr2l;
	/** offset:0x34 */
	volatile Oscl::St::Stm32::H7::pEXTI::rD3PCR2H::Reg	d3pcr2h;
	const uint8_t	_reserve56[8];
	/** offset:0x40 */
	volatile Oscl::St::Stm32::H7::pEXTI::rRTSR3::Reg	rtsr3;
	/** offset:0x44 */
	volatile Oscl::St::Stm32::H7::pEXTI::rFTSR3::Reg	ftsr3;
	/** offset:0x48 */
	volatile Oscl::St::Stm32::H7::pEXTI::rSWIER3::Reg	swier3;
	/** offset:0x4c */
	volatile Oscl::St::Stm32::H7::pEXTI::rD3PMR3::Reg	d3pmr3;
	const uint8_t	_reserve80[4];
	/** offset:0x54 */
	volatile Oscl::St::Stm32::H7::pEXTI::rD3PCR3H::Reg	d3pcr3h;
	const uint8_t	_reserve88[40];
	/** offset:0x80 */
	volatile Oscl::St::Stm32::H7::pEXTI::rCPUIMR1::Reg	cpuimr1;
	/** offset:0x84 */
	volatile Oscl::St::Stm32::H7::pEXTI::rCPUEMR1::Reg	cpuemr1;
	/** offset:0x88 */
	volatile Oscl::St::Stm32::H7::pEXTI::rCPUPR1::Reg	cpupr1;
	const uint8_t	_reserve140[4];
	/** offset:0x90 */
	volatile Oscl::St::Stm32::H7::pEXTI::rCPUIMR2::Reg	cpuimr2;
	/** offset:0x94 */
	volatile Oscl::St::Stm32::H7::pEXTI::rCPUEMR2::Reg	cpuemr2;
	/** offset:0x98 */
	volatile Oscl::St::Stm32::H7::pEXTI::rCPUPR2::Reg	cpupr2;
	const uint8_t	_reserve156[4];
	/** offset:0xa0 */
	volatile Oscl::St::Stm32::H7::pEXTI::rCPUIMR3::Reg	cpuimr3;
	/** offset:0xa4 */
	volatile Oscl::St::Stm32::H7::pEXTI::rCPUEMR3::Reg	cpuemr3;
	/** offset:0xa8 */
	volatile Oscl::St::Stm32::H7::pEXTI::rCPUPR3::Reg	cpupr3;
#endif
	};
}
}
}
}
}
#endif
