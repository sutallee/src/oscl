/*
   Copyright (C) 2024 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_hw_st_stm32_h7_exti_regh_
#define _oscl_hw_st_stm32_h7_exti_regh_
#include <stdint.h>
namespace Oscl { // Namespace description
	namespace St { // Namespace description
		namespace Stm32 { // Namespace description
			namespace H7 { // Namespace description
				namespace pEXTI { // Namespace description
					namespace rRTSR { // Register description
						typedef uint32_t	Reg;
						constexpr Reg	disable = 0x00000000;
						constexpr Reg	disabled = 0x00000000;
						constexpr Reg	enable = 0x00000001;
						constexpr Reg	enabled = 0x00000001;
						namespace fTR0 { // Field Description
							constexpr Reg Lsb = 0;
							constexpr Reg FieldMask = 0x00000001;
							};
						namespace fTR1 { // Field Description
							constexpr Reg Lsb = 1;
							constexpr Reg FieldMask = 0x00000002;
							};
						namespace fTR2 { // Field Description
							constexpr Reg Lsb = 2;
							constexpr Reg FieldMask = 0x00000004;
							};
						namespace fTR3 { // Field Description
							constexpr Reg Lsb = 3;
							constexpr Reg FieldMask = 0x00000008;
							};
						namespace fTR4 { // Field Description
							constexpr Reg Lsb = 4;
							constexpr Reg FieldMask = 0x00000010;
							};
						namespace fTR5 { // Field Description
							constexpr Reg Lsb = 5;
							constexpr Reg FieldMask = 0x00000020;
							};
						namespace fTR6 { // Field Description
							constexpr Reg Lsb = 6;
							constexpr Reg FieldMask = 0x00000040;
							};
						namespace fTR7 { // Field Description
							constexpr Reg Lsb = 7;
							constexpr Reg FieldMask = 0x00000080;
							};
						namespace fTR8 { // Field Description
							constexpr Reg Lsb = 8;
							constexpr Reg FieldMask = 0x00000100;
							};
						namespace fTR9 { // Field Description
							constexpr Reg Lsb = 9;
							constexpr Reg FieldMask = 0x00000200;
							};
						namespace fTR10 { // Field Description
							constexpr Reg Lsb = 10;
							constexpr Reg FieldMask = 0x00000400;
							};
						namespace fTR11 { // Field Description
							constexpr Reg Lsb = 11;
							constexpr Reg FieldMask = 0x00000800;
							};
						namespace fTR12 { // Field Description
							constexpr Reg Lsb = 12;
							constexpr Reg FieldMask = 0x00001000;
							};
						namespace fTR13 { // Field Description
							constexpr Reg Lsb = 13;
							constexpr Reg FieldMask = 0x00002000;
							};
						namespace fTR14 { // Field Description
							constexpr Reg Lsb = 14;
							constexpr Reg FieldMask = 0x00004000;
							};
						namespace fTR15 { // Field Description
							constexpr Reg Lsb = 15;
							constexpr Reg FieldMask = 0x00008000;
							};
						namespace fTR16 { // Field Description
							constexpr Reg Lsb = 16;
							constexpr Reg FieldMask = 0x00010000;
							};
						namespace fTR17 { // Field Description
							constexpr Reg Lsb = 17;
							constexpr Reg FieldMask = 0x00020000;
							};
						namespace fTR18 { // Field Description
							constexpr Reg Lsb = 18;
							constexpr Reg FieldMask = 0x00040000;
							};
						namespace fTR19 { // Field Description
							constexpr Reg Lsb = 19;
							constexpr Reg FieldMask = 0x00080000;
							};
						namespace fTR20 { // Field Description
							constexpr Reg Lsb = 20;
							constexpr Reg FieldMask = 0x00100000;
							};
						namespace fTR21 { // Field Description
							constexpr Reg Lsb = 21;
							constexpr Reg FieldMask = 0x00200000;
							};
						};
					namespace rFTSR { // Register description
						typedef uint32_t	Reg;
						constexpr Reg	disable = 0x00000000;
						constexpr Reg	disabled = 0x00000000;
						constexpr Reg	enable = 0x00000001;
						constexpr Reg	enabled = 0x00000001;
						namespace fTR0 { // Field Description
							constexpr Reg Lsb = 0;
							constexpr Reg FieldMask = 0x00000001;
							};
						namespace fTR1 { // Field Description
							constexpr Reg Lsb = 1;
							constexpr Reg FieldMask = 0x00000002;
							};
						namespace fTR2 { // Field Description
							constexpr Reg Lsb = 2;
							constexpr Reg FieldMask = 0x00000004;
							};
						namespace fTR3 { // Field Description
							constexpr Reg Lsb = 3;
							constexpr Reg FieldMask = 0x00000008;
							};
						namespace fTR4 { // Field Description
							constexpr Reg Lsb = 4;
							constexpr Reg FieldMask = 0x00000010;
							};
						namespace fTR5 { // Field Description
							constexpr Reg Lsb = 5;
							constexpr Reg FieldMask = 0x00000020;
							};
						namespace fTR6 { // Field Description
							constexpr Reg Lsb = 6;
							constexpr Reg FieldMask = 0x00000040;
							};
						namespace fTR7 { // Field Description
							constexpr Reg Lsb = 7;
							constexpr Reg FieldMask = 0x00000080;
							};
						namespace fTR8 { // Field Description
							constexpr Reg Lsb = 8;
							constexpr Reg FieldMask = 0x00000100;
							};
						namespace fTR9 { // Field Description
							constexpr Reg Lsb = 9;
							constexpr Reg FieldMask = 0x00000200;
							};
						namespace fTR10 { // Field Description
							constexpr Reg Lsb = 10;
							constexpr Reg FieldMask = 0x00000400;
							};
						namespace fTR11 { // Field Description
							constexpr Reg Lsb = 11;
							constexpr Reg FieldMask = 0x00000800;
							};
						namespace fTR12 { // Field Description
							constexpr Reg Lsb = 12;
							constexpr Reg FieldMask = 0x00001000;
							};
						namespace fTR13 { // Field Description
							constexpr Reg Lsb = 13;
							constexpr Reg FieldMask = 0x00002000;
							};
						namespace fTR14 { // Field Description
							constexpr Reg Lsb = 14;
							constexpr Reg FieldMask = 0x00004000;
							};
						namespace fTR15 { // Field Description
							constexpr Reg Lsb = 15;
							constexpr Reg FieldMask = 0x00008000;
							};
						namespace fTR16 { // Field Description
							constexpr Reg Lsb = 16;
							constexpr Reg FieldMask = 0x00010000;
							};
						namespace fTR17 { // Field Description
							constexpr Reg Lsb = 17;
							constexpr Reg FieldMask = 0x00020000;
							};
						namespace fTR18 { // Field Description
							constexpr Reg Lsb = 18;
							constexpr Reg FieldMask = 0x00040000;
							};
						namespace fTR19 { // Field Description
							constexpr Reg Lsb = 19;
							constexpr Reg FieldMask = 0x00080000;
							};
						namespace fTR20 { // Field Description
							constexpr Reg Lsb = 20;
							constexpr Reg FieldMask = 0x00100000;
							};
						namespace fTR21 { // Field Description
							constexpr Reg Lsb = 21;
							constexpr Reg FieldMask = 0x00200000;
							};
						};
					namespace rSWIER { // Register description
						typedef uint32_t	Reg;
						constexpr Reg	noEffect = 0x00000000;
						constexpr Reg	trigger = 0x00000001;
						namespace fSWIER0 { // Field Description
							constexpr Reg Lsb = 0;
							constexpr Reg FieldMask = 0x00000001;
							};
						namespace fSWIER1 { // Field Description
							constexpr Reg Lsb = 1;
							constexpr Reg FieldMask = 0x00000002;
							};
						namespace fSWIER2 { // Field Description
							constexpr Reg Lsb = 2;
							constexpr Reg FieldMask = 0x00000004;
							};
						namespace fSWIER3 { // Field Description
							constexpr Reg Lsb = 3;
							constexpr Reg FieldMask = 0x00000008;
							};
						namespace fSWIER4 { // Field Description
							constexpr Reg Lsb = 4;
							constexpr Reg FieldMask = 0x00000010;
							};
						namespace fSWIER5 { // Field Description
							constexpr Reg Lsb = 5;
							constexpr Reg FieldMask = 0x00000020;
							};
						namespace fSWIER6 { // Field Description
							constexpr Reg Lsb = 6;
							constexpr Reg FieldMask = 0x00000040;
							};
						namespace fSWIER7 { // Field Description
							constexpr Reg Lsb = 7;
							constexpr Reg FieldMask = 0x00000080;
							};
						namespace fSWIER8 { // Field Description
							constexpr Reg Lsb = 8;
							constexpr Reg FieldMask = 0x00000100;
							};
						namespace fSWIER9 { // Field Description
							constexpr Reg Lsb = 9;
							constexpr Reg FieldMask = 0x00000200;
							};
						namespace fSWIER10 { // Field Description
							constexpr Reg Lsb = 10;
							constexpr Reg FieldMask = 0x00000400;
							};
						namespace fSWIER11 { // Field Description
							constexpr Reg Lsb = 11;
							constexpr Reg FieldMask = 0x00000800;
							};
						namespace fSWIER12 { // Field Description
							constexpr Reg Lsb = 12;
							constexpr Reg FieldMask = 0x00001000;
							};
						namespace fSWIER13 { // Field Description
							constexpr Reg Lsb = 13;
							constexpr Reg FieldMask = 0x00002000;
							};
						namespace fSWIER14 { // Field Description
							constexpr Reg Lsb = 14;
							constexpr Reg FieldMask = 0x00004000;
							};
						namespace fSWIER15 { // Field Description
							constexpr Reg Lsb = 15;
							constexpr Reg FieldMask = 0x00008000;
							};
						namespace fSWIER16 { // Field Description
							constexpr Reg Lsb = 16;
							constexpr Reg FieldMask = 0x00010000;
							};
						namespace fSWIER17 { // Field Description
							constexpr Reg Lsb = 17;
							constexpr Reg FieldMask = 0x00020000;
							};
						namespace fSWIER18 { // Field Description
							constexpr Reg Lsb = 18;
							constexpr Reg FieldMask = 0x00040000;
							};
						namespace fSWIER19 { // Field Description
							constexpr Reg Lsb = 19;
							constexpr Reg FieldMask = 0x00080000;
							};
						namespace fSWIER20 { // Field Description
							constexpr Reg Lsb = 20;
							constexpr Reg FieldMask = 0x00100000;
							};
						namespace fSWIER21 { // Field Description
							constexpr Reg Lsb = 21;
							constexpr Reg FieldMask = 0x00200000;
							};
						};
					namespace rD3PMR { // Register description
						typedef uint32_t	Reg;
						namespace fMR0 { // Field Description
							constexpr Reg Lsb = 0;
							constexpr Reg FieldMask = 0x00000001;
							};
						namespace fMR1 { // Field Description
							constexpr Reg Lsb = 1;
							constexpr Reg FieldMask = 0x00000002;
							};
						namespace fMR2 { // Field Description
							constexpr Reg Lsb = 2;
							constexpr Reg FieldMask = 0x00000004;
							};
						namespace fMR3 { // Field Description
							constexpr Reg Lsb = 3;
							constexpr Reg FieldMask = 0x00000008;
							};
						namespace fMR4 { // Field Description
							constexpr Reg Lsb = 4;
							constexpr Reg FieldMask = 0x00000010;
							};
						namespace fMR5 { // Field Description
							constexpr Reg Lsb = 5;
							constexpr Reg FieldMask = 0x00000020;
							};
						namespace fMR6 { // Field Description
							constexpr Reg Lsb = 6;
							constexpr Reg FieldMask = 0x00000040;
							};
						namespace fMR7 { // Field Description
							constexpr Reg Lsb = 7;
							constexpr Reg FieldMask = 0x00000080;
							};
						namespace fMR8 { // Field Description
							constexpr Reg Lsb = 8;
							constexpr Reg FieldMask = 0x00000100;
							};
						namespace fMR9 { // Field Description
							constexpr Reg Lsb = 9;
							constexpr Reg FieldMask = 0x00000200;
							};
						namespace fMR10 { // Field Description
							constexpr Reg Lsb = 10;
							constexpr Reg FieldMask = 0x00000400;
							};
						namespace fMR11 { // Field Description
							constexpr Reg Lsb = 11;
							constexpr Reg FieldMask = 0x00000800;
							};
						namespace fMR12 { // Field Description
							constexpr Reg Lsb = 12;
							constexpr Reg FieldMask = 0x00001000;
							};
						namespace fMR13 { // Field Description
							constexpr Reg Lsb = 13;
							constexpr Reg FieldMask = 0x00002000;
							};
						namespace fMR14 { // Field Description
							constexpr Reg Lsb = 14;
							constexpr Reg FieldMask = 0x00004000;
							};
						namespace fMR15 { // Field Description
							constexpr Reg Lsb = 15;
							constexpr Reg FieldMask = 0x00008000;
							};
						namespace fMR19 { // Field Description
							constexpr Reg Lsb = 19;
							constexpr Reg FieldMask = 0x00080000;
							};
						namespace fMR20 { // Field Description
							constexpr Reg Lsb = 20;
							constexpr Reg FieldMask = 0x00100000;
							};
						namespace fMR21 { // Field Description
							constexpr Reg Lsb = 21;
							constexpr Reg FieldMask = 0x00200000;
							};
						namespace fMR25 { // Field Description
							constexpr Reg Lsb = 25;
							constexpr Reg FieldMask = 0x02000000;
							};
						};
					namespace rD3PCRL { // Register description
						typedef uint32_t	Reg;
						namespace fPCS0 { // Field Description
							constexpr Reg Lsb = 0;
							constexpr Reg FieldMask = 0x00000003;
							};
						namespace fPCS1 { // Field Description
							constexpr Reg Lsb = 2;
							constexpr Reg FieldMask = 0x0000000C;
							};
						namespace fPCS2 { // Field Description
							constexpr Reg Lsb = 4;
							constexpr Reg FieldMask = 0x00000030;
							};
						namespace fPCS3 { // Field Description
							constexpr Reg Lsb = 6;
							constexpr Reg FieldMask = 0x000000C0;
							};
						namespace fPCS4 { // Field Description
							constexpr Reg Lsb = 8;
							constexpr Reg FieldMask = 0x00000300;
							};
						namespace fPCS5 { // Field Description
							constexpr Reg Lsb = 10;
							constexpr Reg FieldMask = 0x00000C00;
							};
						namespace fPCS6 { // Field Description
							constexpr Reg Lsb = 12;
							constexpr Reg FieldMask = 0x00003000;
							};
						namespace fPCS7 { // Field Description
							constexpr Reg Lsb = 14;
							constexpr Reg FieldMask = 0x0000C000;
							};
						namespace fPCS8 { // Field Description
							constexpr Reg Lsb = 16;
							constexpr Reg FieldMask = 0x00030000;
							};
						namespace fPCS9 { // Field Description
							constexpr Reg Lsb = 18;
							constexpr Reg FieldMask = 0x000C0000;
							};
						namespace fPCS10 { // Field Description
							constexpr Reg Lsb = 20;
							constexpr Reg FieldMask = 0x00300000;
							};
						namespace fPCS11 { // Field Description
							constexpr Reg Lsb = 22;
							constexpr Reg FieldMask = 0x00C00000;
							};
						namespace fPCS12 { // Field Description
							constexpr Reg Lsb = 24;
							constexpr Reg FieldMask = 0x03000000;
							};
						namespace fPCS13 { // Field Description
							constexpr Reg Lsb = 26;
							constexpr Reg FieldMask = 0x0C000000;
							};
						namespace fPCS14 { // Field Description
							constexpr Reg Lsb = 28;
							constexpr Reg FieldMask = 0x30000000;
							};
						namespace fPCS15 { // Field Description
							constexpr Reg Lsb = 30;
							constexpr Reg FieldMask = 0xC0000000;
							};
						};
					namespace rD3PCRH { // Register description
						typedef uint32_t	Reg;
						namespace fPCS19 { // Field Description
							constexpr Reg Lsb = 6;
							constexpr Reg FieldMask = 0x000000C0;
							};
						namespace fPCS20 { // Field Description
							constexpr Reg Lsb = 8;
							constexpr Reg FieldMask = 0x00000300;
							};
						namespace fPCS21 { // Field Description
							constexpr Reg Lsb = 10;
							constexpr Reg FieldMask = 0x00000C00;
							};
						namespace fPCS25 { // Field Description
							constexpr Reg Lsb = 18;
							constexpr Reg FieldMask = 0x000C0000;
							};
						};
					namespace rCPUIMR { // Register description
						typedef uint32_t	Reg;
						constexpr Reg	disable = 0x00000000;
						constexpr Reg	disabled = 0x00000000;
						constexpr Reg	enable = 0x00000001;
						constexpr Reg	enabled = 0x00000001;
						namespace fMR0 { // Field Description
							constexpr Reg Lsb = 0;
							constexpr Reg FieldMask = 0x00000001;
							};
						namespace fMR1 { // Field Description
							constexpr Reg Lsb = 1;
							constexpr Reg FieldMask = 0x00000002;
							};
						namespace fMR2 { // Field Description
							constexpr Reg Lsb = 2;
							constexpr Reg FieldMask = 0x00000004;
							};
						namespace fMR3 { // Field Description
							constexpr Reg Lsb = 3;
							constexpr Reg FieldMask = 0x00000008;
							};
						namespace fMR4 { // Field Description
							constexpr Reg Lsb = 4;
							constexpr Reg FieldMask = 0x00000010;
							};
						namespace fMR5 { // Field Description
							constexpr Reg Lsb = 5;
							constexpr Reg FieldMask = 0x00000020;
							};
						namespace fMR6 { // Field Description
							constexpr Reg Lsb = 6;
							constexpr Reg FieldMask = 0x00000040;
							};
						namespace fMR7 { // Field Description
							constexpr Reg Lsb = 7;
							constexpr Reg FieldMask = 0x00000080;
							};
						namespace fMR8 { // Field Description
							constexpr Reg Lsb = 8;
							constexpr Reg FieldMask = 0x00000100;
							};
						namespace fMR9 { // Field Description
							constexpr Reg Lsb = 9;
							constexpr Reg FieldMask = 0x00000200;
							};
						namespace fMR10 { // Field Description
							constexpr Reg Lsb = 10;
							constexpr Reg FieldMask = 0x00000400;
							};
						namespace fMR11 { // Field Description
							constexpr Reg Lsb = 11;
							constexpr Reg FieldMask = 0x00000800;
							};
						namespace fMR12 { // Field Description
							constexpr Reg Lsb = 12;
							constexpr Reg FieldMask = 0x00001000;
							};
						namespace fMR13 { // Field Description
							constexpr Reg Lsb = 13;
							constexpr Reg FieldMask = 0x00002000;
							};
						namespace fMR14 { // Field Description
							constexpr Reg Lsb = 14;
							constexpr Reg FieldMask = 0x00004000;
							};
						namespace fMR15 { // Field Description
							constexpr Reg Lsb = 15;
							constexpr Reg FieldMask = 0x00008000;
							};
						namespace fMR16 { // Field Description
							constexpr Reg Lsb = 16;
							constexpr Reg FieldMask = 0x00010000;
							};
						namespace fMR17 { // Field Description
							constexpr Reg Lsb = 17;
							constexpr Reg FieldMask = 0x00020000;
							};
						namespace fMR18 { // Field Description
							constexpr Reg Lsb = 18;
							constexpr Reg FieldMask = 0x00040000;
							};
						namespace fMR19 { // Field Description
							constexpr Reg Lsb = 19;
							constexpr Reg FieldMask = 0x00080000;
							};
						namespace fMR20 { // Field Description
							constexpr Reg Lsb = 20;
							constexpr Reg FieldMask = 0x00100000;
							};
						namespace fMR21 { // Field Description
							constexpr Reg Lsb = 21;
							constexpr Reg FieldMask = 0x00200000;
							};
						namespace fMR22 { // Field Description
							constexpr Reg Lsb = 22;
							constexpr Reg FieldMask = 0x00400000;
							};
						namespace fMR23 { // Field Description
							constexpr Reg Lsb = 23;
							constexpr Reg FieldMask = 0x00800000;
							};
						namespace fMR24 { // Field Description
							constexpr Reg Lsb = 24;
							constexpr Reg FieldMask = 0x01000000;
							};
						namespace fMR25 { // Field Description
							constexpr Reg Lsb = 25;
							constexpr Reg FieldMask = 0x02000000;
							};
						namespace fMR26 { // Field Description
							constexpr Reg Lsb = 26;
							constexpr Reg FieldMask = 0x04000000;
							};
						namespace fMR27 { // Field Description
							constexpr Reg Lsb = 27;
							constexpr Reg FieldMask = 0x08000000;
							};
						namespace fMR28 { // Field Description
							constexpr Reg Lsb = 28;
							constexpr Reg FieldMask = 0x10000000;
							};
						namespace fMR29 { // Field Description
							constexpr Reg Lsb = 29;
							constexpr Reg FieldMask = 0x20000000;
							};
						namespace fMR30 { // Field Description
							constexpr Reg Lsb = 30;
							constexpr Reg FieldMask = 0x40000000;
							};
						namespace fMR31 { // Field Description
							constexpr Reg Lsb = 31;
							constexpr Reg FieldMask = 0x80000000;
							};
						};
					namespace rCPUEMR { // Register description
						typedef uint32_t	Reg;
						constexpr Reg	disable = 0x00000000;
						constexpr Reg	disabled = 0x00000000;
						constexpr Reg	enable = 0x00000001;
						constexpr Reg	enabled = 0x00000001;
						namespace fMR0 { // Field Description
							constexpr Reg Lsb = 0;
							constexpr Reg FieldMask = 0x00000001;
							};
						namespace fMR1 { // Field Description
							constexpr Reg Lsb = 1;
							constexpr Reg FieldMask = 0x00000002;
							};
						namespace fMR2 { // Field Description
							constexpr Reg Lsb = 2;
							constexpr Reg FieldMask = 0x00000004;
							};
						namespace fMR3 { // Field Description
							constexpr Reg Lsb = 3;
							constexpr Reg FieldMask = 0x00000008;
							};
						namespace fMR4 { // Field Description
							constexpr Reg Lsb = 4;
							constexpr Reg FieldMask = 0x00000010;
							};
						namespace fMR5 { // Field Description
							constexpr Reg Lsb = 5;
							constexpr Reg FieldMask = 0x00000020;
							};
						namespace fMR6 { // Field Description
							constexpr Reg Lsb = 6;
							constexpr Reg FieldMask = 0x00000040;
							};
						namespace fMR7 { // Field Description
							constexpr Reg Lsb = 7;
							constexpr Reg FieldMask = 0x00000080;
							};
						namespace fMR8 { // Field Description
							constexpr Reg Lsb = 8;
							constexpr Reg FieldMask = 0x00000100;
							};
						namespace fMR9 { // Field Description
							constexpr Reg Lsb = 9;
							constexpr Reg FieldMask = 0x00000200;
							};
						namespace fMR10 { // Field Description
							constexpr Reg Lsb = 10;
							constexpr Reg FieldMask = 0x00000400;
							};
						namespace fMR11 { // Field Description
							constexpr Reg Lsb = 11;
							constexpr Reg FieldMask = 0x00000800;
							};
						namespace fMR12 { // Field Description
							constexpr Reg Lsb = 12;
							constexpr Reg FieldMask = 0x00001000;
							};
						namespace fMR13 { // Field Description
							constexpr Reg Lsb = 13;
							constexpr Reg FieldMask = 0x00002000;
							};
						namespace fMR14 { // Field Description
							constexpr Reg Lsb = 14;
							constexpr Reg FieldMask = 0x00004000;
							};
						namespace fMR15 { // Field Description
							constexpr Reg Lsb = 15;
							constexpr Reg FieldMask = 0x00008000;
							};
						namespace fMR16 { // Field Description
							constexpr Reg Lsb = 16;
							constexpr Reg FieldMask = 0x00010000;
							};
						namespace fMR17 { // Field Description
							constexpr Reg Lsb = 17;
							constexpr Reg FieldMask = 0x00020000;
							};
						namespace fMR18 { // Field Description
							constexpr Reg Lsb = 18;
							constexpr Reg FieldMask = 0x00040000;
							};
						namespace fMR19 { // Field Description
							constexpr Reg Lsb = 19;
							constexpr Reg FieldMask = 0x00080000;
							};
						namespace fMR20 { // Field Description
							constexpr Reg Lsb = 20;
							constexpr Reg FieldMask = 0x00100000;
							};
						namespace fMR21 { // Field Description
							constexpr Reg Lsb = 21;
							constexpr Reg FieldMask = 0x00200000;
							};
						namespace fMR22 { // Field Description
							constexpr Reg Lsb = 22;
							constexpr Reg FieldMask = 0x00400000;
							};
						namespace fMR23 { // Field Description
							constexpr Reg Lsb = 23;
							constexpr Reg FieldMask = 0x00800000;
							};
						namespace fMR24 { // Field Description
							constexpr Reg Lsb = 24;
							constexpr Reg FieldMask = 0x01000000;
							};
						namespace fMR25 { // Field Description
							constexpr Reg Lsb = 25;
							constexpr Reg FieldMask = 0x02000000;
							};
						namespace fMR26 { // Field Description
							constexpr Reg Lsb = 26;
							constexpr Reg FieldMask = 0x04000000;
							};
						namespace fMR27 { // Field Description
							constexpr Reg Lsb = 27;
							constexpr Reg FieldMask = 0x08000000;
							};
						namespace fMR28 { // Field Description
							constexpr Reg Lsb = 28;
							constexpr Reg FieldMask = 0x10000000;
							};
						namespace fMR29 { // Field Description
							constexpr Reg Lsb = 29;
							constexpr Reg FieldMask = 0x20000000;
							};
						namespace fMR30 { // Field Description
							constexpr Reg Lsb = 30;
							constexpr Reg FieldMask = 0x40000000;
							};
						namespace fMR31 { // Field Description
							constexpr Reg Lsb = 31;
							constexpr Reg FieldMask = 0x80000000;
							};
						};
					namespace rCPUPR { // Register description
						typedef uint32_t	Reg;
						constexpr Reg	noEffect = 0x00000000;
						constexpr Reg	clear = 0x00000001;
						namespace fPR0 { // Field Description
							constexpr Reg Lsb = 0;
							constexpr Reg FieldMask = 0x00000001;
							};
						namespace fPR1 { // Field Description
							constexpr Reg Lsb = 1;
							constexpr Reg FieldMask = 0x00000002;
							};
						namespace fPR2 { // Field Description
							constexpr Reg Lsb = 2;
							constexpr Reg FieldMask = 0x00000004;
							};
						namespace fPR3 { // Field Description
							constexpr Reg Lsb = 3;
							constexpr Reg FieldMask = 0x00000008;
							};
						namespace fPR4 { // Field Description
							constexpr Reg Lsb = 4;
							constexpr Reg FieldMask = 0x00000010;
							};
						namespace fPR5 { // Field Description
							constexpr Reg Lsb = 5;
							constexpr Reg FieldMask = 0x00000020;
							};
						namespace fPR6 { // Field Description
							constexpr Reg Lsb = 6;
							constexpr Reg FieldMask = 0x00000040;
							};
						namespace fPR7 { // Field Description
							constexpr Reg Lsb = 7;
							constexpr Reg FieldMask = 0x00000080;
							};
						namespace fPR8 { // Field Description
							constexpr Reg Lsb = 8;
							constexpr Reg FieldMask = 0x00000100;
							};
						namespace fPR9 { // Field Description
							constexpr Reg Lsb = 9;
							constexpr Reg FieldMask = 0x00000200;
							};
						namespace fPR10 { // Field Description
							constexpr Reg Lsb = 10;
							constexpr Reg FieldMask = 0x00000400;
							};
						namespace fPR11 { // Field Description
							constexpr Reg Lsb = 11;
							constexpr Reg FieldMask = 0x00000800;
							};
						namespace fPR12 { // Field Description
							constexpr Reg Lsb = 12;
							constexpr Reg FieldMask = 0x00001000;
							};
						namespace fPR13 { // Field Description
							constexpr Reg Lsb = 13;
							constexpr Reg FieldMask = 0x00002000;
							};
						namespace fPR14 { // Field Description
							constexpr Reg Lsb = 14;
							constexpr Reg FieldMask = 0x00004000;
							};
						namespace fPR15 { // Field Description
							constexpr Reg Lsb = 15;
							constexpr Reg FieldMask = 0x00008000;
							};
						namespace fPR16 { // Field Description
							constexpr Reg Lsb = 16;
							constexpr Reg FieldMask = 0x00010000;
							};
						namespace fPR17 { // Field Description
							constexpr Reg Lsb = 17;
							constexpr Reg FieldMask = 0x00020000;
							};
						namespace fPR18 { // Field Description
							constexpr Reg Lsb = 18;
							constexpr Reg FieldMask = 0x00040000;
							};
						namespace fPR19 { // Field Description
							constexpr Reg Lsb = 19;
							constexpr Reg FieldMask = 0x00080000;
							};
						namespace fPR20 { // Field Description
							constexpr Reg Lsb = 20;
							constexpr Reg FieldMask = 0x00100000;
							};
						namespace fPR21 { // Field Description
							constexpr Reg Lsb = 21;
							constexpr Reg FieldMask = 0x00200000;
							};
						};
					}
				}
			}
		}
	}
#endif
