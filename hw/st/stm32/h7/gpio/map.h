/*
   Copyright (C) 2024 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_hw_st_stm32_h7_gpio_maph_
#define _oscl_hw_st_stm32_h7_gpio_maph_
#include "reg.h"
namespace Oscl {
namespace St {
namespace Stm32 {
namespace H7 {
namespace pGPIO {
struct Map {
	/** Port Mode Register
		offset:0x0
	 */
	volatile Oscl::St::Stm32::H7::pGPIO::rMODER::Reg	moder;

	/** Output Type Register
		offset:0x4
	 */
	volatile Oscl::St::Stm32::H7::pGPIO::rOTYPER::Reg	otyper;

	/** Output Speed Register
		offset:0x8
	 */
	volatile Oscl::St::Stm32::H7::pGPIO::rOSPEEDR::Reg	ospeedr;

	/** Pull Up Pull Down Register
		offset:0xc
	 */
	volatile Oscl::St::Stm32::H7::pGPIO::rPUPDR::Reg	pupdr;

	/** Input Data Register
		offset:0x10
	 */
	volatile Oscl::St::Stm32::H7::pGPIO::rIDR::Reg	idr;

	/** Output Data Register
		offset:0x14
	 */
	volatile Oscl::St::Stm32::H7::pGPIO::rODR::Reg	odr;

	/** Bit Set/Reset Register
		offset:0x18
	 */
	volatile Oscl::St::Stm32::H7::pGPIO::rBSRR::Reg	bsrr;

	/** Configuration Lock Register
		offset:0x1c
	 */
	volatile Oscl::St::Stm32::H7::pGPIO::rLCKR::Reg	lckr;

	/** Alternate Function Low Register
		offset:0x20
	 */
	volatile Oscl::St::Stm32::H7::pGPIO::rAFRL::Reg	afrl;

	/** Alternate Function High Register
		offset:0x24
	 */
	volatile Oscl::St::Stm32::H7::pGPIO::rAFRH::Reg	afrh;
	};
}
}
}
}
}
#endif
