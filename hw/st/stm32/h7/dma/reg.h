/*
   Copyright (C) 2024 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_hw_st_stm32_h7_dma_regh_
#define _oscl_hw_st_stm32_h7_dma_regh_
#include <stdint.h>
namespace Oscl { // Namespace description
	namespace St { // Namespace description
		namespace Stm32 { // Namespace description
			namespace H7 { // Namespace description
				namespace pDMA { // Namespace description
					namespace rISR { // Register description
						typedef uint32_t	Reg;
						constexpr Reg	valueAtReset = 0x00000000;
						namespace fSTREAMx { // Field Description
							constexpr Reg Lsb = 0;
							constexpr Reg FieldMask = 0x0000003F;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							namespace fFEIF { // Field Description
								constexpr Reg Lsb = 0;
								constexpr Reg FieldMask = 0x00000001;
								constexpr Reg Value_valueAtReset = 0x0;
								constexpr Reg ValueMask_valueAtReset = 0x00000000;
								constexpr Reg Value_notPending = 0x0;
								constexpr Reg ValueMask_notPending = 0x00000000;
								constexpr Reg Value_pending = 0x1;
								constexpr Reg ValueMask_pending = 0x00000001;
								};
							namespace fDMEIF { // Field Description
								constexpr Reg Lsb = 2;
								constexpr Reg FieldMask = 0x00000004;
								constexpr Reg Value_valueAtReset = 0x0;
								constexpr Reg ValueMask_valueAtReset = 0x00000000;
								constexpr Reg Value_notPending = 0x0;
								constexpr Reg ValueMask_notPending = 0x00000000;
								constexpr Reg Value_pending = 0x1;
								constexpr Reg ValueMask_pending = 0x00000004;
								};
							namespace fTEIF { // Field Description
								constexpr Reg Lsb = 3;
								constexpr Reg FieldMask = 0x00000008;
								constexpr Reg Value_valueAtReset = 0x0;
								constexpr Reg ValueMask_valueAtReset = 0x00000000;
								constexpr Reg Value_notPending = 0x0;
								constexpr Reg ValueMask_notPending = 0x00000000;
								constexpr Reg Value_pending = 0x1;
								constexpr Reg ValueMask_pending = 0x00000008;
								};
							namespace fHTIF { // Field Description
								constexpr Reg Lsb = 4;
								constexpr Reg FieldMask = 0x00000010;
								constexpr Reg Value_valueAtReset = 0x0;
								constexpr Reg ValueMask_valueAtReset = 0x00000000;
								constexpr Reg Value_notPending = 0x0;
								constexpr Reg ValueMask_notPending = 0x00000000;
								constexpr Reg Value_pending = 0x1;
								constexpr Reg ValueMask_pending = 0x00000010;
								};
							namespace fTCIF { // Field Description
								constexpr Reg Lsb = 5;
								constexpr Reg FieldMask = 0x00000020;
								constexpr Reg Value_valueAtReset = 0x0;
								constexpr Reg ValueMask_valueAtReset = 0x00000000;
								constexpr Reg Value_notPending = 0x0;
								constexpr Reg ValueMask_notPending = 0x00000000;
								constexpr Reg Value_pending = 0x1;
								constexpr Reg ValueMask_pending = 0x00000020;
								};
							};
						namespace fSTREAM0 { // Field Description
							constexpr Reg Lsb = 0;
							constexpr Reg FieldMask = 0x0000003F;
							};
						namespace fSTREAM1 { // Field Description
							constexpr Reg Lsb = 6;
							constexpr Reg FieldMask = 0x00000FC0;
							};
						namespace fSTREAM2 { // Field Description
							constexpr Reg Lsb = 16;
							constexpr Reg FieldMask = 0x003F0000;
							};
						namespace fSTREAM3 { // Field Description
							constexpr Reg Lsb = 22;
							constexpr Reg FieldMask = 0x0FC00000;
							};
						};
					namespace rIFCR { // Register description
						typedef uint32_t	Reg;
						constexpr Reg	valueAtReset = 0x00000000;
						namespace fSTREAMx { // Field Description
							constexpr Reg Lsb = 0;
							constexpr Reg FieldMask = 0x0000003F;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							namespace fCFEIF { // Field Description
								constexpr Reg Lsb = 0;
								constexpr Reg FieldMask = 0x00000001;
								constexpr Reg Value_valueAtReset = 0x0;
								constexpr Reg ValueMask_valueAtReset = 0x00000000;
								constexpr Reg Value_noEffect = 0x0;
								constexpr Reg ValueMask_noEffect = 0x00000000;
								constexpr Reg Value_clear = 0x1;
								constexpr Reg ValueMask_clear = 0x00000001;
								};
							namespace fCDMEIF { // Field Description
								constexpr Reg Lsb = 2;
								constexpr Reg FieldMask = 0x00000004;
								constexpr Reg Value_valueAtReset = 0x0;
								constexpr Reg ValueMask_valueAtReset = 0x00000000;
								constexpr Reg Value_noEffect = 0x0;
								constexpr Reg ValueMask_noEffect = 0x00000000;
								constexpr Reg Value_clear = 0x1;
								constexpr Reg ValueMask_clear = 0x00000004;
								};
							namespace fCTEIF { // Field Description
								constexpr Reg Lsb = 3;
								constexpr Reg FieldMask = 0x00000008;
								constexpr Reg Value_valueAtReset = 0x0;
								constexpr Reg ValueMask_valueAtReset = 0x00000000;
								constexpr Reg Value_noEffect = 0x0;
								constexpr Reg ValueMask_noEffect = 0x00000000;
								constexpr Reg Value_clear = 0x1;
								constexpr Reg ValueMask_clear = 0x00000008;
								};
							namespace fCHTIF { // Field Description
								constexpr Reg Lsb = 4;
								constexpr Reg FieldMask = 0x00000010;
								constexpr Reg Value_valueAtReset = 0x0;
								constexpr Reg ValueMask_valueAtReset = 0x00000000;
								constexpr Reg Value_noEffect = 0x0;
								constexpr Reg ValueMask_noEffect = 0x00000000;
								constexpr Reg Value_clear = 0x1;
								constexpr Reg ValueMask_clear = 0x00000010;
								};
							namespace fCTCIF { // Field Description
								constexpr Reg Lsb = 5;
								constexpr Reg FieldMask = 0x00000020;
								constexpr Reg Value_valueAtReset = 0x0;
								constexpr Reg ValueMask_valueAtReset = 0x00000000;
								constexpr Reg Value_noEffect = 0x0;
								constexpr Reg ValueMask_noEffect = 0x00000000;
								constexpr Reg Value_clear = 0x1;
								constexpr Reg ValueMask_clear = 0x00000020;
								};
							};
						namespace fSTREAM0 { // Field Description
							constexpr Reg Lsb = 0;
							constexpr Reg FieldMask = 0x0000003F;
							};
						namespace fSTREAM1 { // Field Description
							constexpr Reg Lsb = 6;
							constexpr Reg FieldMask = 0x00000FC0;
							};
						namespace fSTREAM2 { // Field Description
							constexpr Reg Lsb = 16;
							constexpr Reg FieldMask = 0x003F0000;
							};
						namespace fSTREAM3 { // Field Description
							constexpr Reg Lsb = 22;
							constexpr Reg FieldMask = 0x0FC00000;
							};
						};
					namespace rSCR { // Register description
						typedef uint32_t	Reg;
						constexpr Reg	valueAtReset = 0x00000000;
						namespace fEN { // Field Description
							constexpr Reg Lsb = 0;
							constexpr Reg FieldMask = 0x00000001;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_disable = 0x0;
							constexpr Reg ValueMask_disable = 0x00000000;
							constexpr Reg Value_disabled = 0x0;
							constexpr Reg ValueMask_disabled = 0x00000000;
							constexpr Reg Value_enable = 0x1;
							constexpr Reg ValueMask_enable = 0x00000001;
							constexpr Reg Value_enabled = 0x1;
							constexpr Reg ValueMask_enabled = 0x00000001;
							};
						namespace fDMEIE { // Field Description
							constexpr Reg Lsb = 1;
							constexpr Reg FieldMask = 0x00000002;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_disable = 0x0;
							constexpr Reg ValueMask_disable = 0x00000000;
							constexpr Reg Value_disabled = 0x0;
							constexpr Reg ValueMask_disabled = 0x00000000;
							constexpr Reg Value_enable = 0x1;
							constexpr Reg ValueMask_enable = 0x00000002;
							constexpr Reg Value_enabled = 0x1;
							constexpr Reg ValueMask_enabled = 0x00000002;
							};
						namespace fTEIE { // Field Description
							constexpr Reg Lsb = 2;
							constexpr Reg FieldMask = 0x00000004;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_disable = 0x0;
							constexpr Reg ValueMask_disable = 0x00000000;
							constexpr Reg Value_disabled = 0x0;
							constexpr Reg ValueMask_disabled = 0x00000000;
							constexpr Reg Value_enable = 0x1;
							constexpr Reg ValueMask_enable = 0x00000004;
							constexpr Reg Value_enabled = 0x1;
							constexpr Reg ValueMask_enabled = 0x00000004;
							};
						namespace fHTIE { // Field Description
							constexpr Reg Lsb = 3;
							constexpr Reg FieldMask = 0x00000008;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_disable = 0x0;
							constexpr Reg ValueMask_disable = 0x00000000;
							constexpr Reg Value_disabled = 0x0;
							constexpr Reg ValueMask_disabled = 0x00000000;
							constexpr Reg Value_enable = 0x1;
							constexpr Reg ValueMask_enable = 0x00000008;
							constexpr Reg Value_enabled = 0x1;
							constexpr Reg ValueMask_enabled = 0x00000008;
							};
						namespace fTCIE { // Field Description
							constexpr Reg Lsb = 4;
							constexpr Reg FieldMask = 0x00000010;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_disable = 0x0;
							constexpr Reg ValueMask_disable = 0x00000000;
							constexpr Reg Value_disabled = 0x0;
							constexpr Reg ValueMask_disabled = 0x00000000;
							constexpr Reg Value_enable = 0x1;
							constexpr Reg ValueMask_enable = 0x00000010;
							constexpr Reg Value_enabled = 0x1;
							constexpr Reg ValueMask_enabled = 0x00000010;
							};
						namespace fPFCTRL { // Field Description
							constexpr Reg Lsb = 5;
							constexpr Reg FieldMask = 0x00000020;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_dma = 0x0;
							constexpr Reg ValueMask_dma = 0x00000000;
							constexpr Reg Value_peripheral = 0x1;
							constexpr Reg ValueMask_peripheral = 0x00000020;
							};
						namespace fDIR { // Field Description
							constexpr Reg Lsb = 6;
							constexpr Reg FieldMask = 0x000000C0;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_peripheralToMemory = 0x0;
							constexpr Reg ValueMask_peripheralToMemory = 0x00000000;
							constexpr Reg Value_memoryToPerhipheral = 0x1;
							constexpr Reg ValueMask_memoryToPerhipheral = 0x00000040;
							constexpr Reg Value_memoryToMemory = 0x2;
							constexpr Reg ValueMask_memoryToMemory = 0x00000080;
							};
						namespace fCIRC { // Field Description
							constexpr Reg Lsb = 8;
							constexpr Reg FieldMask = 0x00000100;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_disable = 0x0;
							constexpr Reg ValueMask_disable = 0x00000000;
							constexpr Reg Value_disabled = 0x0;
							constexpr Reg ValueMask_disabled = 0x00000000;
							constexpr Reg Value_enable = 0x1;
							constexpr Reg ValueMask_enable = 0x00000100;
							constexpr Reg Value_enabled = 0x1;
							constexpr Reg ValueMask_enabled = 0x00000100;
							};
						namespace fPINC { // Field Description
							constexpr Reg Lsb = 9;
							constexpr Reg FieldMask = 0x00000200;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_fixed = 0x0;
							constexpr Reg ValueMask_fixed = 0x00000000;
							constexpr Reg Value_increment = 0x1;
							constexpr Reg ValueMask_increment = 0x00000200;
							};
						namespace fMINC { // Field Description
							constexpr Reg Lsb = 10;
							constexpr Reg FieldMask = 0x00000400;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_fixed = 0x0;
							constexpr Reg ValueMask_fixed = 0x00000000;
							constexpr Reg Value_increment = 0x1;
							constexpr Reg ValueMask_increment = 0x00000400;
							};
						namespace fPSIZE { // Field Description
							constexpr Reg Lsb = 11;
							constexpr Reg FieldMask = 0x00001800;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_byte = 0x0;
							constexpr Reg ValueMask_byte = 0x00000000;
							constexpr Reg Value_halfWord = 0x1;
							constexpr Reg ValueMask_halfWord = 0x00000800;
							constexpr Reg Value_word = 0x2;
							constexpr Reg ValueMask_word = 0x00001000;
							};
						namespace fMSIZE { // Field Description
							constexpr Reg Lsb = 13;
							constexpr Reg FieldMask = 0x00006000;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_byte = 0x0;
							constexpr Reg ValueMask_byte = 0x00000000;
							constexpr Reg Value_halfWord = 0x1;
							constexpr Reg ValueMask_halfWord = 0x00002000;
							constexpr Reg Value_word = 0x2;
							constexpr Reg ValueMask_word = 0x00004000;
							};
						namespace fPINCOS { // Field Description
							constexpr Reg Lsb = 15;
							constexpr Reg FieldMask = 0x00008000;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_psize = 0x0;
							constexpr Reg ValueMask_psize = 0x00000000;
							constexpr Reg Value_fixed32Bit = 0x1;
							constexpr Reg ValueMask_fixed32Bit = 0x00008000;
							};
						namespace fPL { // Field Description
							constexpr Reg Lsb = 16;
							constexpr Reg FieldMask = 0x00030000;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_lowPriority = 0x0;
							constexpr Reg ValueMask_lowPriority = 0x00000000;
							constexpr Reg Value_mediumPriority = 0x1;
							constexpr Reg ValueMask_mediumPriority = 0x00010000;
							constexpr Reg Value_highPriority = 0x2;
							constexpr Reg ValueMask_highPriority = 0x00020000;
							constexpr Reg Value_veryHighPriority = 0x3;
							constexpr Reg ValueMask_veryHighPriority = 0x00030000;
							};
						namespace fDBM { // Field Description
							constexpr Reg Lsb = 18;
							constexpr Reg FieldMask = 0x00040000;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_noSwitch = 0x0;
							constexpr Reg ValueMask_noSwitch = 0x00000000;
							constexpr Reg Value_switch = 0x1;
							constexpr Reg ValueMask_switch = 0x00040000;
							};
						namespace fCT { // Field Description
							constexpr Reg Lsb = 19;
							constexpr Reg FieldMask = 0x00080000;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_memory0 = 0x0;
							constexpr Reg ValueMask_memory0 = 0x00000000;
							constexpr Reg Value_memory1 = 0x1;
							constexpr Reg ValueMask_memory1 = 0x00080000;
							};
						namespace fTRBUFF { // Field Description
							constexpr Reg Lsb = 20;
							constexpr Reg FieldMask = 0x00100000;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_disable = 0x0;
							constexpr Reg ValueMask_disable = 0x00000000;
							constexpr Reg Value_disabled = 0x0;
							constexpr Reg ValueMask_disabled = 0x00000000;
							constexpr Reg Value_enable = 0x1;
							constexpr Reg ValueMask_enable = 0x00100000;
							constexpr Reg Value_enabled = 0x1;
							constexpr Reg ValueMask_enabled = 0x00100000;
							};
						namespace fPBURST { // Field Description
							constexpr Reg Lsb = 21;
							constexpr Reg FieldMask = 0x00600000;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_single = 0x0;
							constexpr Reg ValueMask_single = 0x00000000;
							constexpr Reg Value_incr4 = 0x1;
							constexpr Reg ValueMask_incr4 = 0x00200000;
							constexpr Reg Value_incr8 = 0x2;
							constexpr Reg ValueMask_incr8 = 0x00400000;
							constexpr Reg Value_incr16 = 0x3;
							constexpr Reg ValueMask_incr16 = 0x00600000;
							};
						namespace fMBURST { // Field Description
							constexpr Reg Lsb = 23;
							constexpr Reg FieldMask = 0x01800000;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_single = 0x0;
							constexpr Reg ValueMask_single = 0x00000000;
							constexpr Reg Value_incr4 = 0x1;
							constexpr Reg ValueMask_incr4 = 0x00800000;
							constexpr Reg Value_incr8 = 0x2;
							constexpr Reg ValueMask_incr8 = 0x01000000;
							constexpr Reg Value_incr16 = 0x3;
							constexpr Reg ValueMask_incr16 = 0x01800000;
							};
						};
					namespace rSNDTR { // Register description
						typedef uint32_t	Reg;
						constexpr Reg	valueAtReset = 0x00000000;
						namespace fNDT { // Field Description
							constexpr Reg Lsb = 0;
							constexpr Reg FieldMask = 0x0000FFFF;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_minValue = 0x0;
							constexpr Reg ValueMask_minValue = 0x00000000;
							constexpr Reg Value_maxValue = 0xFFFF;
							constexpr Reg ValueMask_maxValue = 0x0000FFFF;
							};
						};
					namespace rSPAR { // Register description
						typedef uint32_t	Reg;
						constexpr Reg	valueAtReset = 0x00000000;
						namespace fPA { // Field Description
							constexpr Reg Lsb = 0;
							constexpr Reg FieldMask = 0xFFFFFFFF;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_minValue = 0x0;
							constexpr Reg ValueMask_minValue = 0x00000000;
							constexpr Reg Value_maxValue = 0xFFFFFFFF;
							constexpr Reg ValueMask_maxValue = 0xFFFFFFFF;
							};
						};
					namespace rSMAR { // Register description
						typedef uint32_t	Reg;
						constexpr Reg	valueAtReset = 0x00000000;
						namespace fMA { // Field Description
							constexpr Reg Lsb = 0;
							constexpr Reg FieldMask = 0xFFFFFFFF;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_minValue = 0x0;
							constexpr Reg ValueMask_minValue = 0x00000000;
							constexpr Reg Value_maxValue = 0xFFFFFFFF;
							constexpr Reg ValueMask_maxValue = 0xFFFFFFFF;
							};
						};
					namespace rSFCR { // Register description
						typedef uint32_t	Reg;
						constexpr Reg	valueAtReset = 0x00000021;
						namespace fFTH { // Field Description
							constexpr Reg Lsb = 0;
							constexpr Reg FieldMask = 0x00000003;
							constexpr Reg Value_valueAtReset = 0x1;
							constexpr Reg ValueMask_valueAtReset = 0x00000001;
							constexpr Reg Value_quarterFull = 0x0;
							constexpr Reg ValueMask_quarterFull = 0x00000000;
							constexpr Reg Value_halfFull = 0x1;
							constexpr Reg ValueMask_halfFull = 0x00000001;
							constexpr Reg Value_threeQuartersFull = 0x2;
							constexpr Reg ValueMask_threeQuartersFull = 0x00000002;
							constexpr Reg Value_full = 0x3;
							constexpr Reg ValueMask_full = 0x00000003;
							};
						namespace fDMDIS { // Field Description
							constexpr Reg Lsb = 2;
							constexpr Reg FieldMask = 0x00000004;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_enableDirectMode = 0x0;
							constexpr Reg ValueMask_enableDirectMode = 0x00000000;
							constexpr Reg Value_disableDirectMode = 0x1;
							constexpr Reg ValueMask_disableDirectMode = 0x00000004;
							};
						namespace fFS { // Field Description
							constexpr Reg Lsb = 3;
							constexpr Reg FieldMask = 0x00000038;
							constexpr Reg Value_valueAtReset = 0x4;
							constexpr Reg ValueMask_valueAtReset = 0x00000020;
							constexpr Reg Value_gtZeroLtQuarter = 0x0;
							constexpr Reg ValueMask_gtZeroLtQuarter = 0x00000000;
							constexpr Reg Value_geQuarterLtHalf = 0x1;
							constexpr Reg ValueMask_geQuarterLtHalf = 0x00000008;
							constexpr Reg Value_geHalfLtTheeQuarters = 0x2;
							constexpr Reg ValueMask_geHalfLtTheeQuarters = 0x00000010;
							constexpr Reg Value_geThreQuartersLtFull = 0x3;
							constexpr Reg ValueMask_geThreQuartersLtFull = 0x00000018;
							constexpr Reg Value_empty = 0x4;
							constexpr Reg ValueMask_empty = 0x00000020;
							constexpr Reg Value_full = 0x5;
							constexpr Reg ValueMask_full = 0x00000028;
							};
						namespace fFEIE { // Field Description
							constexpr Reg Lsb = 7;
							constexpr Reg FieldMask = 0x00000080;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_disable = 0x0;
							constexpr Reg ValueMask_disable = 0x00000000;
							constexpr Reg Value_disabled = 0x0;
							constexpr Reg ValueMask_disabled = 0x00000000;
							constexpr Reg Value_enable = 0x1;
							constexpr Reg ValueMask_enable = 0x00000080;
							constexpr Reg Value_enabled = 0x1;
							constexpr Reg ValueMask_enabled = 0x00000080;
							};
						};
					}
				}
			}
		}
	}
#endif
