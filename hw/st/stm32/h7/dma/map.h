/*
   Copyright (C) 2024 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_hw_st_stm32_h7_dma_maph_
#define _oscl_hw_st_stm32_h7_dma_maph_
#include "reg.h"
/** */
namespace Oscl {
/** */
namespace St {
/** */
namespace Stm32 {
/** */
namespace H7 {
/** */
namespace pDMA {
/** */
struct Stream {
	/** offset:0x00 */
	volatile Oscl::St::Stm32::H7::pDMA::rSCR::Reg	cr;
	/** offset:0x04 */
	volatile Oscl::St::Stm32::H7::pDMA::rSNDTR::Reg	ndtr;
	/** offset:0x08 */
	volatile Oscl::St::Stm32::H7::pDMA::rSPAR::Reg	par;
	/** offset:0x0c */
	volatile Oscl::St::Stm32::H7::pDMA::rSMAR::Reg	m0ar;
	/** offset:0x10 */
	volatile Oscl::St::Stm32::H7::pDMA::rSMAR::Reg	m1ar;
	/** offset:0x14 */
	volatile Oscl::St::Stm32::H7::pDMA::rSFCR::Reg	fcr;
	};
/** */
struct Map {
	/** offset:0x0 */
	volatile Oscl::St::Stm32::H7::pDMA::rISR::Reg	lisr;
	/** offset:0x4 */
	volatile Oscl::St::Stm32::H7::pDMA::rISR::Reg	hisr;
	/** offset:0x8 */
	volatile Oscl::St::Stm32::H7::pDMA::rIFCR::Reg	lifcr;
	/** offset:0xc */
	volatile Oscl::St::Stm32::H7::pDMA::rIFCR::Reg	hifcr;
	/** offset:0x10 */
	Stream	stream[8];
	};
}
}
}
}
}
#endif
