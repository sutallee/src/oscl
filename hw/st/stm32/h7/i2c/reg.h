/*
   Copyright (C) 2024 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_hw_st_stm32_h7_i2c_regh_
#define _oscl_hw_st_stm32_h7_i2c_regh_
#include <stdint.h>
namespace Oscl { // Namespace description
	namespace St { // Namespace description
		namespace Stm32 { // Namespace description
			namespace H7 { // Namespace description
				namespace pI2C { // Namespace description
					namespace rCR1 { // Register description
						typedef uint32_t	Reg;
						constexpr Reg	valueAtReset = 0x00000000;
						namespace fPE { // Field Description
							constexpr Reg Lsb = 0;
							constexpr Reg FieldMask = 0x00000001;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_disable = 0x0;
							constexpr Reg ValueMask_disable = 0x00000000;
							constexpr Reg Value_disabled = 0x0;
							constexpr Reg ValueMask_disabled = 0x00000000;
							constexpr Reg Value_enable = 0x1;
							constexpr Reg ValueMask_enable = 0x00000001;
							constexpr Reg Value_enabled = 0x1;
							constexpr Reg ValueMask_enabled = 0x00000001;
							};
						namespace fTXIE { // Field Description
							constexpr Reg Lsb = 1;
							constexpr Reg FieldMask = 0x00000002;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_disable = 0x0;
							constexpr Reg ValueMask_disable = 0x00000000;
							constexpr Reg Value_disabled = 0x0;
							constexpr Reg ValueMask_disabled = 0x00000000;
							constexpr Reg Value_enable = 0x1;
							constexpr Reg ValueMask_enable = 0x00000002;
							constexpr Reg Value_enabled = 0x1;
							constexpr Reg ValueMask_enabled = 0x00000002;
							};
						namespace fRXIE { // Field Description
							constexpr Reg Lsb = 2;
							constexpr Reg FieldMask = 0x00000004;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_disable = 0x0;
							constexpr Reg ValueMask_disable = 0x00000000;
							constexpr Reg Value_disabled = 0x0;
							constexpr Reg ValueMask_disabled = 0x00000000;
							constexpr Reg Value_enable = 0x1;
							constexpr Reg ValueMask_enable = 0x00000004;
							constexpr Reg Value_enabled = 0x1;
							constexpr Reg ValueMask_enabled = 0x00000004;
							};
						namespace fADDRIE { // Field Description
							constexpr Reg Lsb = 3;
							constexpr Reg FieldMask = 0x00000008;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_disable = 0x0;
							constexpr Reg ValueMask_disable = 0x00000000;
							constexpr Reg Value_disabled = 0x0;
							constexpr Reg ValueMask_disabled = 0x00000000;
							constexpr Reg Value_enable = 0x1;
							constexpr Reg ValueMask_enable = 0x00000008;
							constexpr Reg Value_enabled = 0x1;
							constexpr Reg ValueMask_enabled = 0x00000008;
							};
						namespace fNACKIE { // Field Description
							constexpr Reg Lsb = 4;
							constexpr Reg FieldMask = 0x00000010;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_disable = 0x0;
							constexpr Reg ValueMask_disable = 0x00000000;
							constexpr Reg Value_disabled = 0x0;
							constexpr Reg ValueMask_disabled = 0x00000000;
							constexpr Reg Value_enable = 0x1;
							constexpr Reg ValueMask_enable = 0x00000010;
							constexpr Reg Value_enabled = 0x1;
							constexpr Reg ValueMask_enabled = 0x00000010;
							};
						namespace fSTOPIE { // Field Description
							constexpr Reg Lsb = 5;
							constexpr Reg FieldMask = 0x00000020;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_disable = 0x0;
							constexpr Reg ValueMask_disable = 0x00000000;
							constexpr Reg Value_disabled = 0x0;
							constexpr Reg ValueMask_disabled = 0x00000000;
							constexpr Reg Value_enable = 0x1;
							constexpr Reg ValueMask_enable = 0x00000020;
							constexpr Reg Value_enabled = 0x1;
							constexpr Reg ValueMask_enabled = 0x00000020;
							};
						namespace fTCIE { // Field Description
							constexpr Reg Lsb = 6;
							constexpr Reg FieldMask = 0x00000040;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_disable = 0x0;
							constexpr Reg ValueMask_disable = 0x00000000;
							constexpr Reg Value_disabled = 0x0;
							constexpr Reg ValueMask_disabled = 0x00000000;
							constexpr Reg Value_enable = 0x1;
							constexpr Reg ValueMask_enable = 0x00000040;
							constexpr Reg Value_enabled = 0x1;
							constexpr Reg ValueMask_enabled = 0x00000040;
							};
						namespace fERRIE { // Field Description
							constexpr Reg Lsb = 7;
							constexpr Reg FieldMask = 0x00000080;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_disable = 0x0;
							constexpr Reg ValueMask_disable = 0x00000000;
							constexpr Reg Value_disabled = 0x0;
							constexpr Reg ValueMask_disabled = 0x00000000;
							constexpr Reg Value_enable = 0x1;
							constexpr Reg ValueMask_enable = 0x00000080;
							constexpr Reg Value_enabled = 0x1;
							constexpr Reg ValueMask_enabled = 0x00000080;
							};
						namespace fDNF { // Field Description
							constexpr Reg Lsb = 8;
							constexpr Reg FieldMask = 0x00000F00;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_disable = 0x0;
							constexpr Reg ValueMask_disable = 0x00000000;
							constexpr Reg Value_v1Clocks = 0x1;
							constexpr Reg ValueMask_v1Clocks = 0x00000100;
							constexpr Reg Value_v2Clocks = 0x2;
							constexpr Reg ValueMask_v2Clocks = 0x00000200;
							constexpr Reg Value_v3Clocks = 0x3;
							constexpr Reg ValueMask_v3Clocks = 0x00000300;
							constexpr Reg Value_v4Clocks = 0x4;
							constexpr Reg ValueMask_v4Clocks = 0x00000400;
							constexpr Reg Value_v5Clocks = 0x5;
							constexpr Reg ValueMask_v5Clocks = 0x00000500;
							constexpr Reg Value_v6Clocks = 0x6;
							constexpr Reg ValueMask_v6Clocks = 0x00000600;
							constexpr Reg Value_v7Clocks = 0x7;
							constexpr Reg ValueMask_v7Clocks = 0x00000700;
							constexpr Reg Value_v8Clocks = 0x8;
							constexpr Reg ValueMask_v8Clocks = 0x00000800;
							constexpr Reg Value_v9Clocks = 0x9;
							constexpr Reg ValueMask_v9Clocks = 0x00000900;
							constexpr Reg Value_v10Clocks = 0xA;
							constexpr Reg ValueMask_v10Clocks = 0x00000A00;
							constexpr Reg Value_v11Clocks = 0xB;
							constexpr Reg ValueMask_v11Clocks = 0x00000B00;
							constexpr Reg Value_v12Clocks = 0xC;
							constexpr Reg ValueMask_v12Clocks = 0x00000C00;
							constexpr Reg Value_v13Clocks = 0xD;
							constexpr Reg ValueMask_v13Clocks = 0x00000D00;
							constexpr Reg Value_v14Clocks = 0xE;
							constexpr Reg ValueMask_v14Clocks = 0x00000E00;
							constexpr Reg Value_v15Clocks = 0xF;
							constexpr Reg ValueMask_v15Clocks = 0x00000F00;
							constexpr Reg Value_minValue = 0x0;
							constexpr Reg ValueMask_minValue = 0x00000000;
							constexpr Reg Value_maxValue = 0xF;
							constexpr Reg ValueMask_maxValue = 0x00000F00;
							};
						namespace fANFOFF { // Field Description
							constexpr Reg Lsb = 12;
							constexpr Reg FieldMask = 0x00001000;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_disable = 0x0;
							constexpr Reg ValueMask_disable = 0x00000000;
							constexpr Reg Value_disabled = 0x0;
							constexpr Reg ValueMask_disabled = 0x00000000;
							constexpr Reg Value_enable = 0x1;
							constexpr Reg ValueMask_enable = 0x00001000;
							constexpr Reg Value_enabled = 0x1;
							constexpr Reg ValueMask_enabled = 0x00001000;
							};
						namespace fTXDMAEN { // Field Description
							constexpr Reg Lsb = 14;
							constexpr Reg FieldMask = 0x00004000;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_disable = 0x0;
							constexpr Reg ValueMask_disable = 0x00000000;
							constexpr Reg Value_disabled = 0x0;
							constexpr Reg ValueMask_disabled = 0x00000000;
							constexpr Reg Value_enable = 0x1;
							constexpr Reg ValueMask_enable = 0x00004000;
							constexpr Reg Value_enabled = 0x1;
							constexpr Reg ValueMask_enabled = 0x00004000;
							};
						namespace fRXDMAEN { // Field Description
							constexpr Reg Lsb = 15;
							constexpr Reg FieldMask = 0x00008000;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_disable = 0x0;
							constexpr Reg ValueMask_disable = 0x00000000;
							constexpr Reg Value_disabled = 0x0;
							constexpr Reg ValueMask_disabled = 0x00000000;
							constexpr Reg Value_enable = 0x1;
							constexpr Reg ValueMask_enable = 0x00008000;
							constexpr Reg Value_enabled = 0x1;
							constexpr Reg ValueMask_enabled = 0x00008000;
							};
						namespace fSBC { // Field Description
							constexpr Reg Lsb = 16;
							constexpr Reg FieldMask = 0x00010000;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_disable = 0x0;
							constexpr Reg ValueMask_disable = 0x00000000;
							constexpr Reg Value_disabled = 0x0;
							constexpr Reg ValueMask_disabled = 0x00000000;
							constexpr Reg Value_enable = 0x1;
							constexpr Reg ValueMask_enable = 0x00010000;
							constexpr Reg Value_enabled = 0x1;
							constexpr Reg ValueMask_enabled = 0x00010000;
							};
						namespace fNOSTRETCH { // Field Description
							constexpr Reg Lsb = 17;
							constexpr Reg FieldMask = 0x00020000;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_disable = 0x0;
							constexpr Reg ValueMask_disable = 0x00000000;
							constexpr Reg Value_disabled = 0x0;
							constexpr Reg ValueMask_disabled = 0x00000000;
							constexpr Reg Value_enable = 0x1;
							constexpr Reg ValueMask_enable = 0x00020000;
							constexpr Reg Value_enabled = 0x1;
							constexpr Reg ValueMask_enabled = 0x00020000;
							};
						namespace fWUPEN { // Field Description
							constexpr Reg Lsb = 18;
							constexpr Reg FieldMask = 0x00040000;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_disable = 0x0;
							constexpr Reg ValueMask_disable = 0x00000000;
							constexpr Reg Value_disabled = 0x0;
							constexpr Reg ValueMask_disabled = 0x00000000;
							constexpr Reg Value_enable = 0x1;
							constexpr Reg ValueMask_enable = 0x00040000;
							constexpr Reg Value_enabled = 0x1;
							constexpr Reg ValueMask_enabled = 0x00040000;
							};
						namespace fGCEN { // Field Description
							constexpr Reg Lsb = 19;
							constexpr Reg FieldMask = 0x00080000;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_disable = 0x0;
							constexpr Reg ValueMask_disable = 0x00000000;
							constexpr Reg Value_disabled = 0x0;
							constexpr Reg ValueMask_disabled = 0x00000000;
							constexpr Reg Value_enable = 0x1;
							constexpr Reg ValueMask_enable = 0x00080000;
							constexpr Reg Value_enabled = 0x1;
							constexpr Reg ValueMask_enabled = 0x00080000;
							};
						namespace fSMBHEN { // Field Description
							constexpr Reg Lsb = 20;
							constexpr Reg FieldMask = 0x00100000;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_disable = 0x0;
							constexpr Reg ValueMask_disable = 0x00000000;
							constexpr Reg Value_disabled = 0x0;
							constexpr Reg ValueMask_disabled = 0x00000000;
							constexpr Reg Value_enable = 0x1;
							constexpr Reg ValueMask_enable = 0x00100000;
							constexpr Reg Value_enabled = 0x1;
							constexpr Reg ValueMask_enabled = 0x00100000;
							};
						namespace fSMBDEN { // Field Description
							constexpr Reg Lsb = 21;
							constexpr Reg FieldMask = 0x00200000;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_disable = 0x0;
							constexpr Reg ValueMask_disable = 0x00000000;
							constexpr Reg Value_disabled = 0x0;
							constexpr Reg ValueMask_disabled = 0x00000000;
							constexpr Reg Value_enable = 0x1;
							constexpr Reg ValueMask_enable = 0x00200000;
							constexpr Reg Value_enabled = 0x1;
							constexpr Reg ValueMask_enabled = 0x00200000;
							};
						namespace fALERTEN { // Field Description
							constexpr Reg Lsb = 22;
							constexpr Reg FieldMask = 0x00400000;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_disable = 0x0;
							constexpr Reg ValueMask_disable = 0x00000000;
							constexpr Reg Value_disabled = 0x0;
							constexpr Reg ValueMask_disabled = 0x00000000;
							constexpr Reg Value_enable = 0x1;
							constexpr Reg ValueMask_enable = 0x00400000;
							constexpr Reg Value_enabled = 0x1;
							constexpr Reg ValueMask_enabled = 0x00400000;
							};
						namespace fPECEN { // Field Description
							constexpr Reg Lsb = 23;
							constexpr Reg FieldMask = 0x00800000;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_disable = 0x0;
							constexpr Reg ValueMask_disable = 0x00000000;
							constexpr Reg Value_disabled = 0x0;
							constexpr Reg ValueMask_disabled = 0x00000000;
							constexpr Reg Value_enable = 0x1;
							constexpr Reg ValueMask_enable = 0x00800000;
							constexpr Reg Value_enabled = 0x1;
							constexpr Reg ValueMask_enabled = 0x00800000;
							};
						};
					namespace rCR2 { // Register description
						typedef uint32_t	Reg;
						constexpr Reg	valueAtReset = 0x00000000;
						namespace fSADD { // Field Description
							constexpr Reg Lsb = 0;
							constexpr Reg FieldMask = 0x000003FF;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_minValue = 0x0;
							constexpr Reg ValueMask_minValue = 0x00000000;
							constexpr Reg Value_maxValue = 0x3FF;
							constexpr Reg ValueMask_maxValue = 0x000003FF;
							};
						namespace fRD_WRN { // Field Description
							constexpr Reg Lsb = 10;
							constexpr Reg FieldMask = 0x00000400;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_writeTransfer = 0x0;
							constexpr Reg ValueMask_writeTransfer = 0x00000000;
							constexpr Reg Value_readTransfer = 0x1;
							constexpr Reg ValueMask_readTransfer = 0x00000400;
							};
						namespace fADD10 { // Field Description
							constexpr Reg Lsb = 11;
							constexpr Reg FieldMask = 0x00000800;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_sevenBitAddressMode = 0x0;
							constexpr Reg ValueMask_sevenBitAddressMode = 0x00000000;
							constexpr Reg Value_tenBitAddressMode = 0x1;
							constexpr Reg ValueMask_tenBitAddressMode = 0x00000800;
							};
						namespace fHEAD10R { // Field Description
							constexpr Reg Lsb = 12;
							constexpr Reg FieldMask = 0x00001000;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_masterSendsComplete10Bits = 0x0;
							constexpr Reg ValueMask_masterSendsComplete10Bits = 0x00000000;
							constexpr Reg Value_masterSendsFirstSevenBits = 0x1;
							constexpr Reg ValueMask_masterSendsFirstSevenBits = 0x00001000;
							};
						namespace fSTART { // Field Description
							constexpr Reg Lsb = 13;
							constexpr Reg FieldMask = 0x00002000;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_noEffect = 0x0;
							constexpr Reg ValueMask_noEffect = 0x00000000;
							constexpr Reg Value_noStartGeneration = 0x0;
							constexpr Reg ValueMask_noStartGeneration = 0x00000000;
							constexpr Reg Value_startRestartGeneration = 0x1;
							constexpr Reg ValueMask_startRestartGeneration = 0x00002000;
							};
						namespace fSTOP { // Field Description
							constexpr Reg Lsb = 14;
							constexpr Reg FieldMask = 0x00004000;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_noEffect = 0x0;
							constexpr Reg ValueMask_noEffect = 0x00000000;
							constexpr Reg Value_noStopGeneration = 0x0;
							constexpr Reg ValueMask_noStopGeneration = 0x00000000;
							constexpr Reg Value_stopGeneration = 0x1;
							constexpr Reg ValueMask_stopGeneration = 0x00004000;
							};
						namespace fNACK { // Field Description
							constexpr Reg Lsb = 15;
							constexpr Reg FieldMask = 0x00008000;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_noEffect = 0x0;
							constexpr Reg ValueMask_noEffect = 0x00000000;
							constexpr Reg Value_ackAfterCurrentRxByte = 0x0;
							constexpr Reg ValueMask_ackAfterCurrentRxByte = 0x00000000;
							constexpr Reg Value_nackAfterCurrentRxByte = 0x1;
							constexpr Reg ValueMask_nackAfterCurrentRxByte = 0x00008000;
							};
						namespace fNBYTES { // Field Description
							constexpr Reg Lsb = 16;
							constexpr Reg FieldMask = 0x00FF0000;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_minValue = 0x0;
							constexpr Reg ValueMask_minValue = 0x00000000;
							constexpr Reg Value_maxValue = 0xFF;
							constexpr Reg ValueMask_maxValue = 0x00FF0000;
							};
						namespace fRELOAD { // Field Description
							constexpr Reg Lsb = 24;
							constexpr Reg FieldMask = 0x01000000;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_transferThenStopOrRestart = 0x0;
							constexpr Reg ValueMask_transferThenStopOrRestart = 0x00000000;
							constexpr Reg Value_transferThenStretchSCL = 0x1;
							constexpr Reg ValueMask_transferThenStretchSCL = 0x01000000;
							};
						namespace fAUTOEND { // Field Description
							constexpr Reg Lsb = 25;
							constexpr Reg FieldMask = 0x02000000;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_afterTransferStretch = 0x0;
							constexpr Reg ValueMask_afterTransferStretch = 0x00000000;
							constexpr Reg Value_afterTransferSTOP = 0x1;
							constexpr Reg ValueMask_afterTransferSTOP = 0x02000000;
							};
						namespace fPECBYTE { // Field Description
							constexpr Reg Lsb = 26;
							constexpr Reg FieldMask = 0x04000000;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_noPacketErrorXfer = 0x0;
							constexpr Reg ValueMask_noPacketErrorXfer = 0x00000000;
							constexpr Reg Value_requestPacketError = 0x1;
							constexpr Reg ValueMask_requestPacketError = 0x04000000;
							};
						};
					namespace rOAR1 { // Register description
						typedef uint32_t	Reg;
						constexpr Reg	valueAtReset = 0x00000000;
						namespace fOA1 { // Field Description
							constexpr Reg Lsb = 0;
							constexpr Reg FieldMask = 0x000003FF;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_minValue = 0x0;
							constexpr Reg ValueMask_minValue = 0x00000000;
							constexpr Reg Value_maxValue = 0x3FF;
							constexpr Reg ValueMask_maxValue = 0x000003FF;
							};
						namespace fOA1MODE { // Field Description
							constexpr Reg Lsb = 10;
							constexpr Reg FieldMask = 0x00000400;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_v7Bit = 0x0;
							constexpr Reg ValueMask_v7Bit = 0x00000000;
							constexpr Reg Value_v10Bit = 0x1;
							constexpr Reg ValueMask_v10Bit = 0x00000400;
							};
						namespace fOA1EN { // Field Description
							constexpr Reg Lsb = 15;
							constexpr Reg FieldMask = 0x00008000;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_disable = 0x0;
							constexpr Reg ValueMask_disable = 0x00000000;
							constexpr Reg Value_disabled = 0x0;
							constexpr Reg ValueMask_disabled = 0x00000000;
							constexpr Reg Value_enable = 0x1;
							constexpr Reg ValueMask_enable = 0x00008000;
							constexpr Reg Value_enabled = 0x1;
							constexpr Reg ValueMask_enabled = 0x00008000;
							};
						};
					namespace rOAR2 { // Register description
						typedef uint32_t	Reg;
						constexpr Reg	valueAtReset = 0x00000000;
						namespace fOA2 { // Field Description
							constexpr Reg Lsb = 1;
							constexpr Reg FieldMask = 0x000000FE;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_minValue = 0x0;
							constexpr Reg ValueMask_minValue = 0x00000000;
							constexpr Reg Value_maxValue = 0x7F;
							constexpr Reg ValueMask_maxValue = 0x000000FE;
							};
						namespace fOA2MSK { // Field Description
							constexpr Reg Lsb = 8;
							constexpr Reg FieldMask = 0x00000700;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_noMask = 0x0;
							constexpr Reg ValueMask_noMask = 0x00000000;
							constexpr Reg Value_maskBit1 = 0x1;
							constexpr Reg ValueMask_maskBit1 = 0x00000100;
							constexpr Reg Value_maskBits2To1 = 0x2;
							constexpr Reg ValueMask_maskBits2To1 = 0x00000200;
							constexpr Reg Value_maskBits3To1 = 0x3;
							constexpr Reg ValueMask_maskBits3To1 = 0x00000300;
							constexpr Reg Value_maskBits4To1 = 0x4;
							constexpr Reg ValueMask_maskBits4To1 = 0x00000400;
							constexpr Reg Value_maskBits5To1 = 0x5;
							constexpr Reg ValueMask_maskBits5To1 = 0x00000500;
							constexpr Reg Value_maskBits6To1 = 0x6;
							constexpr Reg ValueMask_maskBits6To1 = 0x00000600;
							constexpr Reg Value_maskBits7To1 = 0x7;
							constexpr Reg ValueMask_maskBits7To1 = 0x00000700;
							};
						namespace fOA2EN { // Field Description
							constexpr Reg Lsb = 15;
							constexpr Reg FieldMask = 0x00008000;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_disable = 0x0;
							constexpr Reg ValueMask_disable = 0x00000000;
							constexpr Reg Value_disabled = 0x0;
							constexpr Reg ValueMask_disabled = 0x00000000;
							constexpr Reg Value_enable = 0x1;
							constexpr Reg ValueMask_enable = 0x00008000;
							constexpr Reg Value_enabled = 0x1;
							constexpr Reg ValueMask_enabled = 0x00008000;
							};
						};
					namespace rTIMINGR { // Register description
						typedef uint32_t	Reg;
						constexpr Reg	valueAtReset = 0x00000000;
						namespace fSCLL { // Field Description
							constexpr Reg Lsb = 0;
							constexpr Reg FieldMask = 0x000000FF;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_minValue = 0x0;
							constexpr Reg ValueMask_minValue = 0x00000000;
							constexpr Reg Value_maxValue = 0xF;
							constexpr Reg ValueMask_maxValue = 0x0000000F;
							};
						namespace fSCLH { // Field Description
							constexpr Reg Lsb = 8;
							constexpr Reg FieldMask = 0x0000FF00;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_minValue = 0x0;
							constexpr Reg ValueMask_minValue = 0x00000000;
							constexpr Reg Value_maxValue = 0xF;
							constexpr Reg ValueMask_maxValue = 0x00000F00;
							};
						namespace fSDADEL { // Field Description
							constexpr Reg Lsb = 16;
							constexpr Reg FieldMask = 0x000F0000;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_minValue = 0x0;
							constexpr Reg ValueMask_minValue = 0x00000000;
							constexpr Reg Value_maxValue = 0xF;
							constexpr Reg ValueMask_maxValue = 0x000F0000;
							};
						namespace fSCLDEL { // Field Description
							constexpr Reg Lsb = 20;
							constexpr Reg FieldMask = 0x00F00000;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_minValue = 0x0;
							constexpr Reg ValueMask_minValue = 0x00000000;
							constexpr Reg Value_maxValue = 0xF;
							constexpr Reg ValueMask_maxValue = 0x00F00000;
							};
						namespace fPRESC { // Field Description
							constexpr Reg Lsb = 28;
							constexpr Reg FieldMask = 0xF0000000;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_minValue = 0x0;
							constexpr Reg ValueMask_minValue = 0x00000000;
							constexpr Reg Value_maxValue = 0xF;
							constexpr Reg ValueMask_maxValue = 0xF0000000;
							};
						};
					namespace rTIMEOUTR { // Register description
						typedef uint32_t	Reg;
						constexpr Reg	valueAtReset = 0x00000000;
						namespace fTIMEOUTA { // Field Description
							constexpr Reg Lsb = 0;
							constexpr Reg FieldMask = 0x00000FFF;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_minValue = 0x0;
							constexpr Reg ValueMask_minValue = 0x00000000;
							constexpr Reg Value_maxValue = 0xFFF;
							constexpr Reg ValueMask_maxValue = 0x00000FFF;
							};
						namespace fTIDLE { // Field Description
							constexpr Reg Lsb = 12;
							constexpr Reg FieldMask = 0x00001000;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_sclLow = 0x0;
							constexpr Reg ValueMask_sclLow = 0x00000000;
							constexpr Reg Value_sclAndSdaHigh = 0x1;
							constexpr Reg ValueMask_sclAndSdaHigh = 0x00001000;
							};
						namespace fTIMOUTEN { // Field Description
							constexpr Reg Lsb = 15;
							constexpr Reg FieldMask = 0x00008000;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_disable = 0x0;
							constexpr Reg ValueMask_disable = 0x00000000;
							constexpr Reg Value_disabled = 0x0;
							constexpr Reg ValueMask_disabled = 0x00000000;
							constexpr Reg Value_enable = 0x1;
							constexpr Reg ValueMask_enable = 0x00008000;
							constexpr Reg Value_enabled = 0x1;
							constexpr Reg ValueMask_enabled = 0x00008000;
							};
						namespace fTIMEOUTB { // Field Description
							constexpr Reg Lsb = 16;
							constexpr Reg FieldMask = 0x0FFF0000;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_minValue = 0x0;
							constexpr Reg ValueMask_minValue = 0x00000000;
							constexpr Reg Value_maxValue = 0xFFF;
							constexpr Reg ValueMask_maxValue = 0x0FFF0000;
							};
						namespace fTEXTEN { // Field Description
							constexpr Reg Lsb = 31;
							constexpr Reg FieldMask = 0x80000000;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_disable = 0x0;
							constexpr Reg ValueMask_disable = 0x00000000;
							constexpr Reg Value_disabled = 0x0;
							constexpr Reg ValueMask_disabled = 0x00000000;
							constexpr Reg Value_enable = 0x1;
							constexpr Reg ValueMask_enable = 0x80000000;
							constexpr Reg Value_enabled = 0x1;
							constexpr Reg ValueMask_enabled = 0x80000000;
							};
						};
					namespace rISR { // Register description
						typedef uint32_t	Reg;
						constexpr Reg	valueAtReset = 0x00000001;
						namespace fTXE { // Field Description
							constexpr Reg Lsb = 0;
							constexpr Reg FieldMask = 0x00000001;
							constexpr Reg Value_valueAtReset = 0x1;
							constexpr Reg ValueMask_valueAtReset = 0x00000001;
							constexpr Reg Value_notEmpty = 0x0;
							constexpr Reg ValueMask_notEmpty = 0x00000000;
							constexpr Reg Value_empty = 0x1;
							constexpr Reg ValueMask_empty = 0x00000001;
							constexpr Reg Value_flush = 0x1;
							constexpr Reg ValueMask_flush = 0x00000001;
							};
						namespace fTXIS { // Field Description
							constexpr Reg Lsb = 1;
							constexpr Reg FieldMask = 0x00000002;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_notEmpty = 0x0;
							constexpr Reg ValueMask_notEmpty = 0x00000000;
							constexpr Reg Value_empty = 0x1;
							constexpr Reg ValueMask_empty = 0x00000002;
							};
						namespace fRXNE { // Field Description
							constexpr Reg Lsb = 2;
							constexpr Reg FieldMask = 0x00000004;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_empty = 0x0;
							constexpr Reg ValueMask_empty = 0x00000000;
							constexpr Reg Value_notEmpty = 0x1;
							constexpr Reg ValueMask_notEmpty = 0x00000004;
							};
						namespace fADDR { // Field Description
							constexpr Reg Lsb = 3;
							constexpr Reg FieldMask = 0x00000008;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_notMatched = 0x0;
							constexpr Reg ValueMask_notMatched = 0x00000000;
							constexpr Reg Value_matched = 0x1;
							constexpr Reg ValueMask_matched = 0x00000008;
							};
						namespace fNACKF { // Field Description
							constexpr Reg Lsb = 4;
							constexpr Reg FieldMask = 0x00000010;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_notNacked = 0x0;
							constexpr Reg ValueMask_notNacked = 0x00000000;
							constexpr Reg Value_nacked = 0x1;
							constexpr Reg ValueMask_nacked = 0x00000010;
							};
						namespace fSTOPF { // Field Description
							constexpr Reg Lsb = 5;
							constexpr Reg FieldMask = 0x00000020;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_noStop = 0x0;
							constexpr Reg ValueMask_noStop = 0x00000000;
							constexpr Reg Value_stopCondition = 0x1;
							constexpr Reg ValueMask_stopCondition = 0x00000020;
							};
						namespace fTC { // Field Description
							constexpr Reg Lsb = 6;
							constexpr Reg FieldMask = 0x00000040;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_notComplete = 0x0;
							constexpr Reg ValueMask_notComplete = 0x00000000;
							constexpr Reg Value_transferComplete = 0x1;
							constexpr Reg ValueMask_transferComplete = 0x00000040;
							};
						namespace fTCR { // Field Description
							constexpr Reg Lsb = 7;
							constexpr Reg FieldMask = 0x00000080;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_noReload = 0x0;
							constexpr Reg ValueMask_noReload = 0x00000000;
							constexpr Reg Value_reloadComplete = 0x1;
							constexpr Reg ValueMask_reloadComplete = 0x00000080;
							};
						namespace fBERR { // Field Description
							constexpr Reg Lsb = 8;
							constexpr Reg FieldMask = 0x00000100;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_noError = 0x0;
							constexpr Reg ValueMask_noError = 0x00000000;
							constexpr Reg Value_error = 0x1;
							constexpr Reg ValueMask_error = 0x00000100;
							};
						namespace fARLO { // Field Description
							constexpr Reg Lsb = 9;
							constexpr Reg FieldMask = 0x00000200;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_noError = 0x0;
							constexpr Reg ValueMask_noError = 0x00000000;
							constexpr Reg Value_arbLost = 0x1;
							constexpr Reg ValueMask_arbLost = 0x00000200;
							};
						namespace fOVR { // Field Description
							constexpr Reg Lsb = 10;
							constexpr Reg FieldMask = 0x00000400;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_noError = 0x0;
							constexpr Reg ValueMask_noError = 0x00000000;
							constexpr Reg Value_error = 0x1;
							constexpr Reg ValueMask_error = 0x00000400;
							};
						namespace fPECERR { // Field Description
							constexpr Reg Lsb = 11;
							constexpr Reg FieldMask = 0x00000800;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_noError = 0x0;
							constexpr Reg ValueMask_noError = 0x00000000;
							constexpr Reg Value_error = 0x1;
							constexpr Reg ValueMask_error = 0x00000800;
							};
						namespace fTIMEOUT { // Field Description
							constexpr Reg Lsb = 12;
							constexpr Reg FieldMask = 0x00001000;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_noTimeout = 0x0;
							constexpr Reg ValueMask_noTimeout = 0x00000000;
							constexpr Reg Value_timeout = 0x1;
							constexpr Reg ValueMask_timeout = 0x00001000;
							};
						namespace fALERT { // Field Description
							constexpr Reg Lsb = 13;
							constexpr Reg FieldMask = 0x00002000;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_smbusAlertInactive = 0x0;
							constexpr Reg ValueMask_smbusAlertInactive = 0x00000000;
							constexpr Reg Value_smbusAlertActive = 0x1;
							constexpr Reg ValueMask_smbusAlertActive = 0x00002000;
							};
						namespace fBUSY { // Field Description
							constexpr Reg Lsb = 15;
							constexpr Reg FieldMask = 0x00008000;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_notBusy = 0x0;
							constexpr Reg ValueMask_notBusy = 0x00000000;
							constexpr Reg Value_busy = 0x1;
							constexpr Reg ValueMask_busy = 0x00008000;
							};
						namespace fDIR { // Field Description
							constexpr Reg Lsb = 16;
							constexpr Reg FieldMask = 0x00010000;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_writeTransfer = 0x0;
							constexpr Reg ValueMask_writeTransfer = 0x00000000;
							constexpr Reg Value_readTransfer = 0x1;
							constexpr Reg ValueMask_readTransfer = 0x00010000;
							};
						namespace fADDCODE { // Field Description
							constexpr Reg Lsb = 17;
							constexpr Reg FieldMask = 0x00FE0000;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_minValue = 0x0;
							constexpr Reg ValueMask_minValue = 0x00000000;
							constexpr Reg Value_maxValue = 0xFF;
							constexpr Reg ValueMask_maxValue = 0x01FE0000;
							};
						};
					namespace rICR { // Register description
						typedef uint32_t	Reg;
						constexpr Reg	valueAtReset = 0x00000000;
						namespace fADDRCF { // Field Description
							constexpr Reg Lsb = 3;
							constexpr Reg FieldMask = 0x00000008;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_noEffect = 0x0;
							constexpr Reg ValueMask_noEffect = 0x00000000;
							constexpr Reg Value_clear = 0x1;
							constexpr Reg ValueMask_clear = 0x00000008;
							};
						namespace fNACKCF { // Field Description
							constexpr Reg Lsb = 4;
							constexpr Reg FieldMask = 0x00000010;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_noEffect = 0x0;
							constexpr Reg ValueMask_noEffect = 0x00000000;
							constexpr Reg Value_clear = 0x1;
							constexpr Reg ValueMask_clear = 0x00000010;
							};
						namespace fSTOPCF { // Field Description
							constexpr Reg Lsb = 5;
							constexpr Reg FieldMask = 0x00000020;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_noEffect = 0x0;
							constexpr Reg ValueMask_noEffect = 0x00000000;
							constexpr Reg Value_clear = 0x1;
							constexpr Reg ValueMask_clear = 0x00000020;
							};
						namespace fBERRCF { // Field Description
							constexpr Reg Lsb = 8;
							constexpr Reg FieldMask = 0x00000100;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_noEffect = 0x0;
							constexpr Reg ValueMask_noEffect = 0x00000000;
							constexpr Reg Value_clear = 0x1;
							constexpr Reg ValueMask_clear = 0x00000100;
							};
						namespace fARLOCF { // Field Description
							constexpr Reg Lsb = 9;
							constexpr Reg FieldMask = 0x00000200;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_noEffect = 0x0;
							constexpr Reg ValueMask_noEffect = 0x00000000;
							constexpr Reg Value_clear = 0x1;
							constexpr Reg ValueMask_clear = 0x00000200;
							};
						namespace fOVRCF { // Field Description
							constexpr Reg Lsb = 10;
							constexpr Reg FieldMask = 0x00000400;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_noEffect = 0x0;
							constexpr Reg ValueMask_noEffect = 0x00000000;
							constexpr Reg Value_clear = 0x1;
							constexpr Reg ValueMask_clear = 0x00000400;
							};
						namespace fPECCF { // Field Description
							constexpr Reg Lsb = 11;
							constexpr Reg FieldMask = 0x00000800;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_noEffect = 0x0;
							constexpr Reg ValueMask_noEffect = 0x00000000;
							constexpr Reg Value_clear = 0x1;
							constexpr Reg ValueMask_clear = 0x00000800;
							};
						namespace fTIMOUTCF { // Field Description
							constexpr Reg Lsb = 12;
							constexpr Reg FieldMask = 0x00001000;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_noEffect = 0x0;
							constexpr Reg ValueMask_noEffect = 0x00000000;
							constexpr Reg Value_clear = 0x1;
							constexpr Reg ValueMask_clear = 0x00001000;
							};
						namespace fALERTCF { // Field Description
							constexpr Reg Lsb = 13;
							constexpr Reg FieldMask = 0x00002000;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_noEffect = 0x0;
							constexpr Reg ValueMask_noEffect = 0x00000000;
							constexpr Reg Value_clear = 0x1;
							constexpr Reg ValueMask_clear = 0x00002000;
							};
						};
					namespace rPECR { // Register description
						typedef uint32_t	Reg;
						constexpr Reg	valueAtReset = 0x00000000;
						namespace fPEC { // Field Description
							constexpr Reg Lsb = 0;
							constexpr Reg FieldMask = 0x000000FF;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_minValue = 0x0;
							constexpr Reg ValueMask_minValue = 0x00000000;
							constexpr Reg Value_maxValue = 0xFF;
							constexpr Reg ValueMask_maxValue = 0x000000FF;
							};
						};
					namespace rRXDR { // Register description
						typedef uint32_t	Reg;
						constexpr Reg	valueAtReset = 0x00000000;
						namespace fRXDATA { // Field Description
							constexpr Reg Lsb = 0;
							constexpr Reg FieldMask = 0x000000FF;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_minValue = 0x0;
							constexpr Reg ValueMask_minValue = 0x00000000;
							constexpr Reg Value_maxValue = 0xFF;
							constexpr Reg ValueMask_maxValue = 0x000000FF;
							};
						};
					namespace rTXDR { // Register description
						typedef uint32_t	Reg;
						constexpr Reg	valueAtReset = 0x00000000;
						namespace fTXDATA { // Field Description
							constexpr Reg Lsb = 0;
							constexpr Reg FieldMask = 0x000000FF;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_minValue = 0x0;
							constexpr Reg ValueMask_minValue = 0x00000000;
							constexpr Reg Value_maxValue = 0xFF;
							constexpr Reg ValueMask_maxValue = 0x000000FF;
							};
						};
					}
				}
			}
		}
	}
#endif
