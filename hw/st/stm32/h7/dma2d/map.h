/*
   Copyright (C) 2024 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_hw_st_stm32_h7_dma2d_maph_
#define _oscl_hw_st_stm32_h7_dma2d_maph_
#include "reg.h"

/** */
namespace Oscl {
/** */
namespace St {
/** */
namespace Stm32 {
/** */
namespace H7 {

/** DMA2D Peripheral */
namespace pDMA2D {

/** DMA2D Memory Map */
struct Map {
	/** Control Register
		offset:0x0
	 */
	volatile pDMA2D::rCR::Reg	cr;

	/** Interrupt Status Register
		offset:0x4
	 */
	volatile pDMA2D::rISR::Reg	isr;

	/** Interrupt Flag Clear Register
		offset:0x8
	 */
	volatile pDMA2D::rIFCR::Reg	ifcr;

	/** Foreground Memory Address Register
		offset:0xc
	 */
	volatile pDMA2D::rMAR::Reg	fgmar;

	/** Foreground Offset Register
		offset:0x10
	 */
	volatile pDMA2D::rOR::Reg	fgor;

	/** Background Memory Address Register
		offset:0x14
	 */
	volatile pDMA2D::rMAR::Reg	bgmar;

	/** Background Offset Register
		offset:0x18
	 */
	volatile pDMA2D::rOR::Reg	bgor;

	/** Foreground Pixel Format Converter Control Register
		offset:0x1c
	 */
	volatile pDMA2D::rPFCCR::Reg	fgpfccr;

	/** Foreground Color Register
		offset:0x20
	 */
	volatile pDMA2D::rCOLR::Reg	fgcolr;

	/** Background Pixel Format Converter Control Register
		offset:0x24
	 */
	volatile pDMA2D::rPFCCR::Reg	bgpfccr;

	/** Background Color Register
		offset:0x28
	 */
	volatile pDMA2D::rCOLR::Reg	bgcolr;

	/** Background CLUT Memory Address Register
		offset:0x2c
	 */
	volatile pDMA2D::rCMAR::Reg	fgcmar;

	/** Background CLUT Memory Address Register
		offset:0x30
	 */
	volatile pDMA2D::rCMAR::Reg	bgcmar;

	/** Output Pixel Format Control Register
		offset:0x34
	 */
	volatile pDMA2D::rOPFCCR::Reg	opfccr;

	/** Output Color Register
		offset:0x38
	 */
	union {
		volatile pDMA2D::rOCOLR8888::Reg	argb8888;
		volatile pDMA2D::rOCOLR565::Reg		rgb565;
		volatile pDMA2D::rOCOLR1555::Reg	argb1555;
		volatile pDMA2D::rOCOLR4444::Reg	argb4444;
		} ocolr;

	/** Output Memory Address Register
		offset:0x3c
	 */
	volatile pDMA2D::rMAR::Reg	omar;

	/** Output Offset Register
		offset:0x40
	 */
	volatile pDMA2D::rOR::Reg	oor;

	/** Number of Line Register
		offset:0x44
	 */
	volatile pDMA2D::rNLR::Reg	nlr;

	/** Line Watermark Register
		offset:0x48
	 */
	volatile pDMA2D::rLWR::Reg	lwr;

	/** AXI Master Timer Configuration Register
		offset:0x4c
	 */
	volatile pDMA2D::rAMTCR::Reg	amtcr;
	};
}
}
}
}
}
#endif
