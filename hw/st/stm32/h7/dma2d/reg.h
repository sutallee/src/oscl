/*
   Copyright (C) 2024 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_hw_st_stm32_h7_dma2d_dma2dh_
#define _oscl_hw_st_stm32_h7_dma2d_dma2dh_
#include <stdint.h>
namespace Oscl { // Namespace description
	namespace St { // Namespace description
		namespace Stm32 { // Namespace description
			namespace H7 { // Namespace description
				namespace pDMA2D { // Namespace description
					namespace rCR { // Register description
						typedef uint32_t	Reg;
						constexpr Reg	valueAfterReset = 0;
						namespace fSTART { // Field Description
							constexpr Reg Lsb = 0;
							constexpr Reg FieldMask = 0x00000001;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							constexpr Reg Value_start = 0x1;
							constexpr Reg ValueMask_start = 0x00000001;
							};
						namespace fSUSP { // Field Description
							constexpr Reg Lsb = 1;
							constexpr Reg FieldMask = 0x00000002;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							constexpr Reg Value_notSuspended = 0x0;
							constexpr Reg ValueMask_notSuspended = 0x00000000;
							constexpr Reg Value_continue = 0x0;
							constexpr Reg ValueMask_continue = 0x00000000;
							constexpr Reg Value_suspend = 0x1;
							constexpr Reg ValueMask_suspend = 0x00000002;
							constexpr Reg Value_suspended = 0x1;
							constexpr Reg ValueMask_suspended = 0x00000002;
							};
						namespace fABORT { // Field Description
							constexpr Reg Lsb = 2;
							constexpr Reg FieldMask = 0x00000004;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							constexpr Reg Value_noAbortRequested = 0x0;
							constexpr Reg ValueMask_noAbortRequested = 0x00000000;
							constexpr Reg Value_continue = 0x0;
							constexpr Reg ValueMask_continue = 0x00000000;
							constexpr Reg Value_abort = 0x1;
							constexpr Reg ValueMask_abort = 0x00000004;
							constexpr Reg Value_abortRequested = 0x1;
							constexpr Reg ValueMask_abortRequested = 0x00000004;
							};
						namespace fLOM { // Field Description
							constexpr Reg Lsb = 6;
							constexpr Reg FieldMask = 0x00000040;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							constexpr Reg Value_lineOffsetInPixels = 0x0;
							constexpr Reg ValueMask_lineOffsetInPixels = 0x00000000;
							constexpr Reg Value_lineOffsetInBytes = 0x1;
							constexpr Reg ValueMask_lineOffsetInBytes = 0x00000040;
							};
						namespace fTEIE { // Field Description
							constexpr Reg Lsb = 8;
							constexpr Reg FieldMask = 0x00000100;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							constexpr Reg Value_interruptDisabled = 0x0;
							constexpr Reg ValueMask_interruptDisabled = 0x00000000;
							constexpr Reg Value_disableInterrupt = 0x0;
							constexpr Reg ValueMask_disableInterrupt = 0x00000000;
							constexpr Reg Value_interruptEnabled = 0x1;
							constexpr Reg ValueMask_interruptEnabled = 0x00000100;
							constexpr Reg Value_enableInterrupt = 0x1;
							constexpr Reg ValueMask_enableInterrupt = 0x00000100;
							};
						namespace fTCIE { // Field Description
							constexpr Reg Lsb = 9;
							constexpr Reg FieldMask = 0x00000200;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							constexpr Reg Value_interruptDisabled = 0x0;
							constexpr Reg ValueMask_interruptDisabled = 0x00000000;
							constexpr Reg Value_disableInterrupt = 0x0;
							constexpr Reg ValueMask_disableInterrupt = 0x00000000;
							constexpr Reg Value_interruptEnabled = 0x1;
							constexpr Reg ValueMask_interruptEnabled = 0x00000200;
							constexpr Reg Value_enableInterrupt = 0x1;
							constexpr Reg ValueMask_enableInterrupt = 0x00000200;
							};
						namespace fTWIE { // Field Description
							constexpr Reg Lsb = 10;
							constexpr Reg FieldMask = 0x00000400;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							constexpr Reg Value_interruptDisabled = 0x0;
							constexpr Reg ValueMask_interruptDisabled = 0x00000000;
							constexpr Reg Value_disableInterrupt = 0x0;
							constexpr Reg ValueMask_disableInterrupt = 0x00000000;
							constexpr Reg Value_interruptEnabled = 0x1;
							constexpr Reg ValueMask_interruptEnabled = 0x00000400;
							constexpr Reg Value_enableInterrupt = 0x1;
							constexpr Reg ValueMask_enableInterrupt = 0x00000400;
							};
						namespace fCAEIE { // Field Description
							constexpr Reg Lsb = 11;
							constexpr Reg FieldMask = 0x00000800;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							constexpr Reg Value_interruptDisabled = 0x0;
							constexpr Reg ValueMask_interruptDisabled = 0x00000000;
							constexpr Reg Value_disableInterrupt = 0x0;
							constexpr Reg ValueMask_disableInterrupt = 0x00000000;
							constexpr Reg Value_interruptEnabled = 0x1;
							constexpr Reg ValueMask_interruptEnabled = 0x00000800;
							constexpr Reg Value_enableInterrupt = 0x1;
							constexpr Reg ValueMask_enableInterrupt = 0x00000800;
							};
						namespace fCTCIE { // Field Description
							constexpr Reg Lsb = 12;
							constexpr Reg FieldMask = 0x00001000;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							constexpr Reg Value_interruptDisabled = 0x0;
							constexpr Reg ValueMask_interruptDisabled = 0x00000000;
							constexpr Reg Value_disableInterrupt = 0x0;
							constexpr Reg ValueMask_disableInterrupt = 0x00000000;
							constexpr Reg Value_interruptEnabled = 0x1;
							constexpr Reg ValueMask_interruptEnabled = 0x00001000;
							constexpr Reg Value_enableInterrupt = 0x1;
							constexpr Reg ValueMask_enableInterrupt = 0x00001000;
							};
						namespace fCEIE { // Field Description
							constexpr Reg Lsb = 13;
							constexpr Reg FieldMask = 0x00002000;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							constexpr Reg Value_interruptDisabled = 0x0;
							constexpr Reg ValueMask_interruptDisabled = 0x00000000;
							constexpr Reg Value_disableInterrupt = 0x0;
							constexpr Reg ValueMask_disableInterrupt = 0x00000000;
							constexpr Reg Value_interruptEnabled = 0x1;
							constexpr Reg ValueMask_interruptEnabled = 0x00002000;
							constexpr Reg Value_enableInterrupt = 0x1;
							constexpr Reg ValueMask_enableInterrupt = 0x00002000;
							};
						namespace fMODE { // Field Description
							constexpr Reg Lsb = 16;
							constexpr Reg FieldMask = 0x00070000;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							constexpr Reg Value_memoryToMemory = 0x0;
							constexpr Reg ValueMask_memoryToMemory = 0x00000000;
							constexpr Reg Value_memoryToMemoryPFC = 0x1;
							constexpr Reg ValueMask_memoryToMemoryPFC = 0x00010000;
							constexpr Reg Value_memoryToMemoryWithBlending = 0x2;
							constexpr Reg ValueMask_memoryToMemoryWithBlending = 0x00020000;
							constexpr Reg Value_registerToMemory = 0x3;
							constexpr Reg ValueMask_registerToMemory = 0x00030000;
							constexpr Reg Value_memoryToMemoryWithBlendingFixedColorFG = 0x4;
							constexpr Reg ValueMask_memoryToMemoryWithBlendingFixedColorFG = 0x00040000;
							constexpr Reg Value_memoryToMemoryWithBlendingFixedColorBG = 0x5;
							constexpr Reg ValueMask_memoryToMemoryWithBlendingFixedColorBG = 0x00050000;
							};
						};
					namespace rISR { // Register description
						typedef uint32_t	Reg;
						constexpr Reg	valueAfterReset = 0;
						namespace fTEIF { // Field Description
							constexpr Reg Lsb = 0;
							constexpr Reg FieldMask = 0x00000001;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							constexpr Reg Value_interruptNotPending = 0x0;
							constexpr Reg ValueMask_interruptNotPending = 0x00000000;
							constexpr Reg Value_interruptPending = 0x1;
							constexpr Reg ValueMask_interruptPending = 0x00000001;
							};
						namespace fTCIF { // Field Description
							constexpr Reg Lsb = 1;
							constexpr Reg FieldMask = 0x00000002;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							constexpr Reg Value_interruptNotPending = 0x0;
							constexpr Reg ValueMask_interruptNotPending = 0x00000000;
							constexpr Reg Value_interruptPending = 0x1;
							constexpr Reg ValueMask_interruptPending = 0x00000002;
							};
						namespace fTWIF { // Field Description
							constexpr Reg Lsb = 2;
							constexpr Reg FieldMask = 0x00000004;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							constexpr Reg Value_interruptNotPending = 0x0;
							constexpr Reg ValueMask_interruptNotPending = 0x00000000;
							constexpr Reg Value_interruptPending = 0x1;
							constexpr Reg ValueMask_interruptPending = 0x00000004;
							};
						namespace fCAEIF { // Field Description
							constexpr Reg Lsb = 3;
							constexpr Reg FieldMask = 0x00000008;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							constexpr Reg Value_interruptNotPending = 0x0;
							constexpr Reg ValueMask_interruptNotPending = 0x00000000;
							constexpr Reg Value_interruptPending = 0x1;
							constexpr Reg ValueMask_interruptPending = 0x00000008;
							};
						namespace fCTCIF { // Field Description
							constexpr Reg Lsb = 4;
							constexpr Reg FieldMask = 0x00000010;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							constexpr Reg Value_interruptNotPending = 0x0;
							constexpr Reg ValueMask_interruptNotPending = 0x00000000;
							constexpr Reg Value_interruptPending = 0x1;
							constexpr Reg ValueMask_interruptPending = 0x00000010;
							};
						namespace fCEIF { // Field Description
							constexpr Reg Lsb = 5;
							constexpr Reg FieldMask = 0x00000020;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							constexpr Reg Value_interruptNotPending = 0x0;
							constexpr Reg ValueMask_interruptNotPending = 0x00000000;
							constexpr Reg Value_interruptPending = 0x1;
							constexpr Reg ValueMask_interruptPending = 0x00000020;
							};
						};
					namespace rIFCR { // Register description
						typedef uint32_t	Reg;
						constexpr Reg	valueAfterReset = 0;
						namespace fCTEIF { // Field Description
							constexpr Reg Lsb = 0;
							constexpr Reg FieldMask = 0x00000001;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							constexpr Reg Value_notActive = 0x0;
							constexpr Reg ValueMask_notActive = 0x00000000;
							constexpr Reg Value_active = 0x1;
							constexpr Reg ValueMask_active = 0x00000001;
							constexpr Reg Value_clearInterrupt = 0x1;
							constexpr Reg ValueMask_clearInterrupt = 0x00000001;
							constexpr Reg Value_noEffect = 0x0;
							constexpr Reg ValueMask_noEffect = 0x00000000;
							};
						namespace fCTCIF { // Field Description
							constexpr Reg Lsb = 1;
							constexpr Reg FieldMask = 0x00000002;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							constexpr Reg Value_notActive = 0x0;
							constexpr Reg ValueMask_notActive = 0x00000000;
							constexpr Reg Value_active = 0x1;
							constexpr Reg ValueMask_active = 0x00000002;
							constexpr Reg Value_clearInterrupt = 0x1;
							constexpr Reg ValueMask_clearInterrupt = 0x00000002;
							constexpr Reg Value_noEffect = 0x0;
							constexpr Reg ValueMask_noEffect = 0x00000000;
							};
						namespace fCTWIF { // Field Description
							constexpr Reg Lsb = 2;
							constexpr Reg FieldMask = 0x00000004;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							constexpr Reg Value_notActive = 0x0;
							constexpr Reg ValueMask_notActive = 0x00000000;
							constexpr Reg Value_active = 0x1;
							constexpr Reg ValueMask_active = 0x00000004;
							constexpr Reg Value_clearInterrupt = 0x1;
							constexpr Reg ValueMask_clearInterrupt = 0x00000004;
							constexpr Reg Value_noEffect = 0x0;
							constexpr Reg ValueMask_noEffect = 0x00000000;
							};
						namespace fCAECIF { // Field Description
							constexpr Reg Lsb = 3;
							constexpr Reg FieldMask = 0x00000008;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							constexpr Reg Value_notActive = 0x0;
							constexpr Reg ValueMask_notActive = 0x00000000;
							constexpr Reg Value_active = 0x1;
							constexpr Reg ValueMask_active = 0x00000008;
							constexpr Reg Value_clearInterrupt = 0x1;
							constexpr Reg ValueMask_clearInterrupt = 0x00000008;
							constexpr Reg Value_noEffect = 0x0;
							constexpr Reg ValueMask_noEffect = 0x00000000;
							};
						namespace fCCTCIF { // Field Description
							constexpr Reg Lsb = 4;
							constexpr Reg FieldMask = 0x00000010;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							constexpr Reg Value_notActive = 0x0;
							constexpr Reg ValueMask_notActive = 0x00000000;
							constexpr Reg Value_active = 0x1;
							constexpr Reg ValueMask_active = 0x00000010;
							constexpr Reg Value_clearInterrupt = 0x1;
							constexpr Reg ValueMask_clearInterrupt = 0x00000010;
							constexpr Reg Value_noEffect = 0x0;
							constexpr Reg ValueMask_noEffect = 0x00000000;
							};
						namespace fCCEIF { // Field Description
							constexpr Reg Lsb = 5;
							constexpr Reg FieldMask = 0x00000020;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							constexpr Reg Value_notActive = 0x0;
							constexpr Reg ValueMask_notActive = 0x00000000;
							constexpr Reg Value_active = 0x1;
							constexpr Reg ValueMask_active = 0x00000020;
							constexpr Reg Value_clearInterrupt = 0x1;
							constexpr Reg ValueMask_clearInterrupt = 0x00000020;
							constexpr Reg Value_noEffect = 0x0;
							constexpr Reg ValueMask_noEffect = 0x00000000;
							};
						};
					namespace rMAR { // Register description
						typedef uint32_t	Reg;
						constexpr Reg	valueAfterReset = 0;
						namespace fMA { // Field Description
							constexpr Reg Lsb = 0;
							constexpr Reg FieldMask = 0xFFFFFFFF;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							};
						};
					namespace rOR { // Register description
						typedef uint32_t	Reg;
						constexpr Reg	valueAfterReset = 0;
						namespace fLO { // Field Description
							constexpr Reg Lsb = 0;
							constexpr Reg FieldMask = 0x0000FFFF;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							constexpr Reg Value_maxValue = 0x3FFF;
							constexpr Reg ValueMask_maxValue = 0x00003FFF;
							constexpr Reg Value_minValue = 0x0;
							constexpr Reg ValueMask_minValue = 0x00000000;
							};
						};
					namespace rPFCCR { // Register description
						typedef uint32_t	Reg;
						constexpr Reg	valueAfterReset = 0;
						namespace fCM { // Field Description
							constexpr Reg Lsb = 0;
							constexpr Reg FieldMask = 0x0000000F;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							constexpr Reg Value_vARGB8888 = 0x0;
							constexpr Reg ValueMask_vARGB8888 = 0x00000000;
							constexpr Reg Value_vRGB888 = 0x1;
							constexpr Reg ValueMask_vRGB888 = 0x00000001;
							constexpr Reg Value_vRGB565 = 0x2;
							constexpr Reg ValueMask_vRGB565 = 0x00000002;
							constexpr Reg Value_vARGB1555 = 0x3;
							constexpr Reg ValueMask_vARGB1555 = 0x00000003;
							constexpr Reg Value_vARGB1444 = 0x4;
							constexpr Reg ValueMask_vARGB1444 = 0x00000004;
							constexpr Reg Value_vL8 = 0x5;
							constexpr Reg ValueMask_vL8 = 0x00000005;
							constexpr Reg Value_vAL44 = 0x6;
							constexpr Reg ValueMask_vAL44 = 0x00000006;
							constexpr Reg Value_vAL88 = 0x7;
							constexpr Reg ValueMask_vAL88 = 0x00000007;
							constexpr Reg Value_vL4 = 0x8;
							constexpr Reg ValueMask_vL4 = 0x00000008;
							constexpr Reg Value_vA8 = 0x9;
							constexpr Reg ValueMask_vA8 = 0x00000009;
							constexpr Reg Value_vA4 = 0xA;
							constexpr Reg ValueMask_vA4 = 0x0000000A;
							constexpr Reg Value_vYCbCr = 0xB;
							constexpr Reg ValueMask_vYCbCr = 0x0000000B;
							};
						namespace fCCM { // Field Description
							constexpr Reg Lsb = 4;
							constexpr Reg FieldMask = 0x00000010;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							constexpr Reg Value_vARGB8888 = 0x0;
							constexpr Reg ValueMask_vARGB8888 = 0x00000000;
							constexpr Reg Value_vRGB888 = 0x1;
							constexpr Reg ValueMask_vRGB888 = 0x00000010;
							};
						namespace fSTART { // Field Description
							constexpr Reg Lsb = 5;
							constexpr Reg FieldMask = 0x00000020;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							constexpr Reg Value_start = 0x1;
							constexpr Reg ValueMask_start = 0x00000020;
							};
						namespace fCS { // Field Description
							constexpr Reg Lsb = 8;
							constexpr Reg FieldMask = 0x0000FF00;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							constexpr Reg Value_minValue = 0x0;
							constexpr Reg ValueMask_minValue = 0x00000000;
							constexpr Reg Value_maxValue = 0xFF;
							constexpr Reg ValueMask_maxValue = 0x0000FF00;
							};
						namespace fAM { // Field Description
							constexpr Reg Lsb = 16;
							constexpr Reg FieldMask = 0x00030000;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							constexpr Reg Value_noModification = 0x0;
							constexpr Reg ValueMask_noModification = 0x00000000;
							constexpr Reg Value_replaceFgWithAlpha = 0x1;
							constexpr Reg ValueMask_replaceFgWithAlpha = 0x00010000;
							constexpr Reg Value_replaceFgWithAlphaXAlpaha = 0x2;
							constexpr Reg ValueMask_replaceFgWithAlphaXAlpaha = 0x00020000;
							};
						namespace fCSS { // Field Description
							constexpr Reg Lsb = 18;
							constexpr Reg FieldMask = 0x000C0000;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							constexpr Reg Value_noChroma444 = 0x0;
							constexpr Reg ValueMask_noChroma444 = 0x00000000;
							constexpr Reg Value_chroma422 = 0x1;
							constexpr Reg ValueMask_chroma422 = 0x00040000;
							constexpr Reg Value_chroma420 = 0x2;
							constexpr Reg ValueMask_chroma420 = 0x00080000;
							};
						namespace fAI { // Field Description
							constexpr Reg Lsb = 20;
							constexpr Reg FieldMask = 0x00100000;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							constexpr Reg Value_regularAlpha = 0x0;
							constexpr Reg ValueMask_regularAlpha = 0x00000000;
							constexpr Reg Value_invertedAlpha = 0x1;
							constexpr Reg ValueMask_invertedAlpha = 0x00100000;
							};
						namespace fRBS { // Field Description
							constexpr Reg Lsb = 21;
							constexpr Reg FieldMask = 0x00200000;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							constexpr Reg Value_vRGB = 0x0;
							constexpr Reg ValueMask_vRGB = 0x00000000;
							constexpr Reg Value_vARGB = 0x0;
							constexpr Reg ValueMask_vARGB = 0x00000000;
							constexpr Reg Value_vBGR = 0x1;
							constexpr Reg ValueMask_vBGR = 0x00200000;
							constexpr Reg Value_vABGR = 0x1;
							constexpr Reg ValueMask_vABGR = 0x00200000;
							};
						namespace fALPHA { // Field Description
							constexpr Reg Lsb = 24;
							constexpr Reg FieldMask = 0xFF000000;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							constexpr Reg Value_minValue = 0x0;
							constexpr Reg ValueMask_minValue = 0x00000000;
							constexpr Reg Value_maxValue = 0xFF;
							constexpr Reg ValueMask_maxValue = 0xFF000000;
							};
						};
					namespace rCOLR { // Register description
						typedef uint32_t	Reg;
						constexpr Reg	valueAfterReset = 0;
						namespace fBLUE { // Field Description
							constexpr Reg Lsb = 0;
							constexpr Reg FieldMask = 0x000000FF;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							constexpr Reg Value_minValue = 0x0;
							constexpr Reg ValueMask_minValue = 0x00000000;
							constexpr Reg Value_maxValue = 0xFF;
							constexpr Reg ValueMask_maxValue = 0x000000FF;
							};
						namespace fGREEN { // Field Description
							constexpr Reg Lsb = 8;
							constexpr Reg FieldMask = 0x0000FF00;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							constexpr Reg Value_minValue = 0x0;
							constexpr Reg ValueMask_minValue = 0x00000000;
							constexpr Reg Value_maxValue = 0xFF;
							constexpr Reg ValueMask_maxValue = 0x0000FF00;
							};
						namespace fRED { // Field Description
							constexpr Reg Lsb = 16;
							constexpr Reg FieldMask = 0x00FF0000;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							constexpr Reg Value_minValue = 0x0;
							constexpr Reg ValueMask_minValue = 0x00000000;
							constexpr Reg Value_maxValue = 0xFF;
							constexpr Reg ValueMask_maxValue = 0x00FF0000;
							};
						};
					namespace rCMAR { // Register description
						typedef uint32_t	Reg;
						constexpr Reg	valueAfterReset = 0;
						namespace fMA { // Field Description
							constexpr Reg Lsb = 0;
							constexpr Reg FieldMask = 0xFFFFFFFF;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							constexpr Reg Value_minValue = 0x0;
							constexpr Reg ValueMask_minValue = 0x00000000;
							constexpr Reg Value_maxValue = 0xFFFFFFFF;
							constexpr Reg ValueMask_maxValue = 0xFFFFFFFF;
							};
						};
					namespace rOPFCCR { // Register description
						typedef uint32_t	Reg;
						constexpr Reg	valueAfterReset = 0;
						namespace fCM { // Field Description
							constexpr Reg Lsb = 0;
							constexpr Reg FieldMask = 0x0000000F;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							constexpr Reg Value_vARGB8888 = 0x0;
							constexpr Reg ValueMask_vARGB8888 = 0x00000000;
							constexpr Reg Value_vRGB888 = 0x1;
							constexpr Reg ValueMask_vRGB888 = 0x00000001;
							constexpr Reg Value_vRGB565 = 0x2;
							constexpr Reg ValueMask_vRGB565 = 0x00000002;
							constexpr Reg Value_vARGB1555 = 0x3;
							constexpr Reg ValueMask_vARGB1555 = 0x00000003;
							constexpr Reg Value_vARGB1444 = 0x4;
							constexpr Reg ValueMask_vARGB1444 = 0x00000004;
							};
						namespace fSB { // Field Description
							constexpr Reg Lsb = 8;
							constexpr Reg FieldMask = 0x00000100;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							constexpr Reg Value_regularOrder = 0x0;
							constexpr Reg ValueMask_regularOrder = 0x00000000;
							constexpr Reg Value_swapTwoByTwo = 0x1;
							constexpr Reg ValueMask_swapTwoByTwo = 0x00000100;
							};
						namespace fAI { // Field Description
							constexpr Reg Lsb = 20;
							constexpr Reg FieldMask = 0x00100000;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							constexpr Reg Value_regularAlpha = 0x0;
							constexpr Reg ValueMask_regularAlpha = 0x00000000;
							constexpr Reg Value_invertedAlpha = 0x1;
							constexpr Reg ValueMask_invertedAlpha = 0x00100000;
							};
						namespace fRBS { // Field Description
							constexpr Reg Lsb = 21;
							constexpr Reg FieldMask = 0x00200000;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							constexpr Reg Value_vRGB = 0x0;
							constexpr Reg ValueMask_vRGB = 0x00000000;
							constexpr Reg Value_vARGB = 0x0;
							constexpr Reg ValueMask_vARGB = 0x00000000;
							constexpr Reg Value_vBGR = 0x1;
							constexpr Reg ValueMask_vBGR = 0x00200000;
							constexpr Reg Value_vABGR = 0x1;
							constexpr Reg ValueMask_vABGR = 0x00200000;
							};
						};
					namespace rOCOLR8888 { // Register description
						typedef uint32_t	Reg;
						constexpr Reg	valueAfterReset = 0;
						namespace fBLUE { // Field Description
							constexpr Reg Lsb = 0;
							constexpr Reg FieldMask = 0x000000FF;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							constexpr Reg Value_minValue = 0x0;
							constexpr Reg ValueMask_minValue = 0x00000000;
							constexpr Reg Value_maxValue = 0xFF;
							constexpr Reg ValueMask_maxValue = 0x000000FF;
							};
						namespace fGREEN { // Field Description
							constexpr Reg Lsb = 8;
							constexpr Reg FieldMask = 0x0000FF00;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							constexpr Reg Value_minValue = 0x0;
							constexpr Reg ValueMask_minValue = 0x00000000;
							constexpr Reg Value_maxValue = 0xFF;
							constexpr Reg ValueMask_maxValue = 0x0000FF00;
							};
						namespace fRED { // Field Description
							constexpr Reg Lsb = 16;
							constexpr Reg FieldMask = 0x00FF0000;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							constexpr Reg Value_minValue = 0x0;
							constexpr Reg ValueMask_minValue = 0x00000000;
							constexpr Reg Value_maxValue = 0xFF;
							constexpr Reg ValueMask_maxValue = 0x00FF0000;
							};
						namespace fALPHA { // Field Description
							constexpr Reg Lsb = 16;
							constexpr Reg FieldMask = 0x00FF0000;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							constexpr Reg Value_minValue = 0x0;
							constexpr Reg ValueMask_minValue = 0x00000000;
							constexpr Reg Value_maxValue = 0xFF;
							constexpr Reg ValueMask_maxValue = 0x00FF0000;
							};
						};
					namespace rOCOLR565 { // Register description
						typedef uint32_t	Reg;
						constexpr Reg	valueAfterReset = 0;
						namespace fBLUE { // Field Description
							constexpr Reg Lsb = 0;
							constexpr Reg FieldMask = 0x0000001F;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							constexpr Reg Value_minValue = 0x0;
							constexpr Reg ValueMask_minValue = 0x00000000;
							constexpr Reg Value_maxValue = 0x1F;
							constexpr Reg ValueMask_maxValue = 0x0000001F;
							};
						namespace fGREEN { // Field Description
							constexpr Reg Lsb = 5;
							constexpr Reg FieldMask = 0x000007E0;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							constexpr Reg Value_minValue = 0x0;
							constexpr Reg ValueMask_minValue = 0x00000000;
							constexpr Reg Value_maxValue = 0x3F;
							constexpr Reg ValueMask_maxValue = 0x000007E0;
							};
						namespace fRED { // Field Description
							constexpr Reg Lsb = 11;
							constexpr Reg FieldMask = 0x0000F800;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							constexpr Reg Value_minValue = 0x0;
							constexpr Reg ValueMask_minValue = 0x00000000;
							constexpr Reg Value_maxValue = 0x1F;
							constexpr Reg ValueMask_maxValue = 0x0000F800;
							};
						};
					namespace rOCOLR1555 { // Register description
						typedef uint32_t	Reg;
						constexpr Reg	valueAfterReset = 0;
						namespace fBLUE { // Field Description
							constexpr Reg Lsb = 0;
							constexpr Reg FieldMask = 0x0000001F;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							constexpr Reg Value_minValue = 0x0;
							constexpr Reg ValueMask_minValue = 0x00000000;
							constexpr Reg Value_maxValue = 0x1F;
							constexpr Reg ValueMask_maxValue = 0x0000001F;
							};
						namespace fGREEN { // Field Description
							constexpr Reg Lsb = 5;
							constexpr Reg FieldMask = 0x000007E0;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							constexpr Reg Value_minValue = 0x0;
							constexpr Reg ValueMask_minValue = 0x00000000;
							constexpr Reg Value_maxValue = 0x1F;
							constexpr Reg ValueMask_maxValue = 0x000003E0;
							};
						namespace fRED { // Field Description
							constexpr Reg Lsb = 10;
							constexpr Reg FieldMask = 0x00007C00;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							constexpr Reg Value_minValue = 0x0;
							constexpr Reg ValueMask_minValue = 0x00000000;
							constexpr Reg Value_maxValue = 0x1F;
							constexpr Reg ValueMask_maxValue = 0x00007C00;
							};
						namespace fALPHA { // Field Description
							constexpr Reg Lsb = 15;
							constexpr Reg FieldMask = 0x00008000;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							constexpr Reg Value_minValue = 0x0;
							constexpr Reg ValueMask_minValue = 0x00000000;
							constexpr Reg Value_maxValue = 0x1;
							constexpr Reg ValueMask_maxValue = 0x00008000;
							};
						};
					namespace rOCOLR4444 { // Register description
						typedef uint32_t	Reg;
						constexpr Reg	valueAfterReset = 0;
						namespace fBLUE { // Field Description
							constexpr Reg Lsb = 0;
							constexpr Reg FieldMask = 0x0000000F;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							constexpr Reg Value_minValue = 0x0;
							constexpr Reg ValueMask_minValue = 0x00000000;
							constexpr Reg Value_maxValue = 0xF;
							constexpr Reg ValueMask_maxValue = 0x0000000F;
							};
						namespace fGREEN { // Field Description
							constexpr Reg Lsb = 4;
							constexpr Reg FieldMask = 0x000000F0;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							constexpr Reg Value_minValue = 0x0;
							constexpr Reg ValueMask_minValue = 0x00000000;
							constexpr Reg Value_maxValue = 0xF;
							constexpr Reg ValueMask_maxValue = 0x000000F0;
							};
						namespace fRED { // Field Description
							constexpr Reg Lsb = 8;
							constexpr Reg FieldMask = 0x00000F00;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							constexpr Reg Value_minValue = 0x0;
							constexpr Reg ValueMask_minValue = 0x00000000;
							constexpr Reg Value_maxValue = 0xF;
							constexpr Reg ValueMask_maxValue = 0x00000F00;
							};
						namespace fALPHA { // Field Description
							constexpr Reg Lsb = 12;
							constexpr Reg FieldMask = 0x0000F000;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							constexpr Reg Value_minValue = 0x0;
							constexpr Reg ValueMask_minValue = 0x00000000;
							constexpr Reg Value_maxValue = 0xF;
							constexpr Reg ValueMask_maxValue = 0x0000F000;
							};
						};
					namespace rNLR { // Register description
						typedef uint32_t	Reg;
						constexpr Reg	valueAfterReset = 0;
						namespace fNL { // Field Description
							constexpr Reg Lsb = 0;
							constexpr Reg FieldMask = 0x0000FFFF;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							constexpr Reg Value_minValue = 0x0;
							constexpr Reg ValueMask_minValue = 0x00000000;
							constexpr Reg Value_maxValue = 0xFFFF;
							constexpr Reg ValueMask_maxValue = 0x0000FFFF;
							};
						namespace fPL { // Field Description
							constexpr Reg Lsb = 16;
							constexpr Reg FieldMask = 0x3FFF0000;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							constexpr Reg Value_minValue = 0x0;
							constexpr Reg ValueMask_minValue = 0x00000000;
							constexpr Reg Value_maxValue = 0x3FFF;
							constexpr Reg ValueMask_maxValue = 0x3FFF0000;
							};
						};
					namespace rLWR { // Register description
						typedef uint32_t	Reg;
						constexpr Reg	valueAfterReset = 0;
						namespace fLW { // Field Description
							constexpr Reg Lsb = 0;
							constexpr Reg FieldMask = 0x0000FFFF;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							constexpr Reg Value_minValue = 0x0;
							constexpr Reg ValueMask_minValue = 0x00000000;
							constexpr Reg Value_maxValue = 0xFFFF;
							constexpr Reg ValueMask_maxValue = 0x0000FFFF;
							};
						};
					namespace rAMTCR { // Register description
						typedef uint32_t	Reg;
						constexpr Reg	valueAfterReset = 0;
						namespace fEN { // Field Description
							constexpr Reg Lsb = 0;
							constexpr Reg FieldMask = 0x00000001;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							constexpr Reg Value_disabled = 0x0;
							constexpr Reg ValueMask_disabled = 0x00000000;
							constexpr Reg Value_disable = 0x0;
							constexpr Reg ValueMask_disable = 0x00000000;
							constexpr Reg Value_enabled = 0x1;
							constexpr Reg ValueMask_enabled = 0x00000001;
							constexpr Reg Value_enable = 0x1;
							constexpr Reg ValueMask_enable = 0x00000001;
							};
						namespace fDT { // Field Description
							constexpr Reg Lsb = 8;
							constexpr Reg FieldMask = 0x0000FF00;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							constexpr Reg Value_minDeadTimeCycles = 0x0;
							constexpr Reg ValueMask_minDeadTimeCycles = 0x00000000;
							constexpr Reg Value_maxDeadTimeCycles = 0xFF;
							constexpr Reg ValueMask_maxDeadTimeCycles = 0x0000FF00;
							};
						};
					}
				}
			}
		}
	}
#endif
