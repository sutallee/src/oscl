/*
   Copyright (C) 2024 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_hw_st_stm32_h7_usart_regh_
#define _oscl_hw_st_stm32_h7_usart_regh_
#include <stdint.h>
namespace Oscl { // Namespace description
	namespace St { // Namespace description
		namespace Stm32 { // Namespace description
			namespace H7 { // Namespace description
				namespace pUSART { // Namespace description
					namespace rCR1 { // Register description
						typedef uint32_t	Reg;
						constexpr Reg	valueAtReset = 0x00000000;
						namespace fUE { // Field Description
							constexpr Reg Lsb = 0;
							constexpr Reg FieldMask = 0x00000001;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_disable = 0x0;
							constexpr Reg ValueMask_disable = 0x00000000;
							constexpr Reg Value_disabled = 0x0;
							constexpr Reg ValueMask_disabled = 0x00000000;
							constexpr Reg Value_enable = 0x1;
							constexpr Reg ValueMask_enable = 0x00000001;
							constexpr Reg Value_enabled = 0x1;
							constexpr Reg ValueMask_enabled = 0x00000001;
							};
						namespace fUESM { // Field Description
							constexpr Reg Lsb = 1;
							constexpr Reg FieldMask = 0x00000002;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_wakeupDisable = 0x0;
							constexpr Reg ValueMask_wakeupDisable = 0x00000000;
							constexpr Reg Value_wakeupDisabled = 0x0;
							constexpr Reg ValueMask_wakeupDisabled = 0x00000000;
							constexpr Reg Value_wakeupEnable = 0x1;
							constexpr Reg ValueMask_wakeupEnable = 0x00000002;
							constexpr Reg Value_wakeupEnabled = 0x1;
							constexpr Reg ValueMask_wakeupEnabled = 0x00000002;
							};
						namespace fRE { // Field Description
							constexpr Reg Lsb = 2;
							constexpr Reg FieldMask = 0x00000004;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_disable = 0x0;
							constexpr Reg ValueMask_disable = 0x00000000;
							constexpr Reg Value_disabled = 0x0;
							constexpr Reg ValueMask_disabled = 0x00000000;
							constexpr Reg Value_enable = 0x1;
							constexpr Reg ValueMask_enable = 0x00000004;
							constexpr Reg Value_enabled = 0x1;
							constexpr Reg ValueMask_enabled = 0x00000004;
							};
						namespace fTE { // Field Description
							constexpr Reg Lsb = 3;
							constexpr Reg FieldMask = 0x00000008;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_disable = 0x0;
							constexpr Reg ValueMask_disable = 0x00000000;
							constexpr Reg Value_disabled = 0x0;
							constexpr Reg ValueMask_disabled = 0x00000000;
							constexpr Reg Value_enable = 0x1;
							constexpr Reg ValueMask_enable = 0x00000008;
							constexpr Reg Value_enabled = 0x1;
							constexpr Reg ValueMask_enabled = 0x00000008;
							};
						namespace fIDLEIE { // Field Description
							constexpr Reg Lsb = 4;
							constexpr Reg FieldMask = 0x00000010;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_disable = 0x0;
							constexpr Reg ValueMask_disable = 0x00000000;
							constexpr Reg Value_disabled = 0x0;
							constexpr Reg ValueMask_disabled = 0x00000000;
							constexpr Reg Value_enable = 0x1;
							constexpr Reg ValueMask_enable = 0x00000010;
							constexpr Reg Value_enabled = 0x1;
							constexpr Reg ValueMask_enabled = 0x00000010;
							};
						namespace fRXNEIE { // Field Description
							constexpr Reg Lsb = 5;
							constexpr Reg FieldMask = 0x00000020;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_disable = 0x0;
							constexpr Reg ValueMask_disable = 0x00000000;
							constexpr Reg Value_disabled = 0x0;
							constexpr Reg ValueMask_disabled = 0x00000000;
							constexpr Reg Value_enable = 0x1;
							constexpr Reg ValueMask_enable = 0x00000020;
							constexpr Reg Value_enabled = 0x1;
							constexpr Reg ValueMask_enabled = 0x00000020;
							};
						namespace fTCIE { // Field Description
							constexpr Reg Lsb = 6;
							constexpr Reg FieldMask = 0x00000040;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_disable = 0x0;
							constexpr Reg ValueMask_disable = 0x00000000;
							constexpr Reg Value_disabled = 0x0;
							constexpr Reg ValueMask_disabled = 0x00000000;
							constexpr Reg Value_enable = 0x1;
							constexpr Reg ValueMask_enable = 0x00000040;
							constexpr Reg Value_enabled = 0x1;
							constexpr Reg ValueMask_enabled = 0x00000040;
							};
						namespace fTXEIE { // Field Description
							constexpr Reg Lsb = 7;
							constexpr Reg FieldMask = 0x00000080;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_disable = 0x0;
							constexpr Reg ValueMask_disable = 0x00000000;
							constexpr Reg Value_disabled = 0x0;
							constexpr Reg ValueMask_disabled = 0x00000000;
							constexpr Reg Value_enable = 0x1;
							constexpr Reg ValueMask_enable = 0x00000080;
							constexpr Reg Value_enabled = 0x1;
							constexpr Reg ValueMask_enabled = 0x00000080;
							};
						namespace fPEIE { // Field Description
							constexpr Reg Lsb = 8;
							constexpr Reg FieldMask = 0x00000100;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_disable = 0x0;
							constexpr Reg ValueMask_disable = 0x00000000;
							constexpr Reg Value_disabled = 0x0;
							constexpr Reg ValueMask_disabled = 0x00000000;
							constexpr Reg Value_enable = 0x1;
							constexpr Reg ValueMask_enable = 0x00000100;
							constexpr Reg Value_enabled = 0x1;
							constexpr Reg ValueMask_enabled = 0x00000100;
							};
						namespace fPS { // Field Description
							constexpr Reg Lsb = 9;
							constexpr Reg FieldMask = 0x00000200;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_even = 0x0;
							constexpr Reg ValueMask_even = 0x00000000;
							constexpr Reg Value_odd = 0x1;
							constexpr Reg ValueMask_odd = 0x00000200;
							};
						namespace fPCE { // Field Description
							constexpr Reg Lsb = 10;
							constexpr Reg FieldMask = 0x00000400;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_disable = 0x0;
							constexpr Reg ValueMask_disable = 0x00000000;
							constexpr Reg Value_disabled = 0x0;
							constexpr Reg ValueMask_disabled = 0x00000000;
							constexpr Reg Value_enable = 0x1;
							constexpr Reg ValueMask_enable = 0x00000400;
							constexpr Reg Value_enabled = 0x1;
							constexpr Reg ValueMask_enabled = 0x00000400;
							};
						namespace fWAKE { // Field Description
							constexpr Reg Lsb = 11;
							constexpr Reg FieldMask = 0x00000800;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_idleLine = 0x0;
							constexpr Reg ValueMask_idleLine = 0x00000000;
							constexpr Reg Value_addressMark = 0x1;
							constexpr Reg ValueMask_addressMark = 0x00000800;
							};
						namespace fM0 { // Field Description
							constexpr Reg Lsb = 12;
							constexpr Reg FieldMask = 0x00001000;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_v18Nor17N = 0x0;
							constexpr Reg ValueMask_v18Nor17N = 0x00000000;
							constexpr Reg Value_v19N = 0x1;
							constexpr Reg ValueMask_v19N = 0x00001000;
							};
						namespace fMME { // Field Description
							constexpr Reg Lsb = 13;
							constexpr Reg FieldMask = 0x00002000;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_rxAlwaysActive = 0x0;
							constexpr Reg ValueMask_rxAlwaysActive = 0x00000000;
							constexpr Reg Value_rxMuteSwitch = 0x1;
							constexpr Reg ValueMask_rxMuteSwitch = 0x00002000;
							};
						namespace fCMIE { // Field Description
							constexpr Reg Lsb = 14;
							constexpr Reg FieldMask = 0x00004000;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_disable = 0x0;
							constexpr Reg ValueMask_disable = 0x00000000;
							constexpr Reg Value_disabled = 0x0;
							constexpr Reg ValueMask_disabled = 0x00000000;
							constexpr Reg Value_enable = 0x1;
							constexpr Reg ValueMask_enable = 0x00004000;
							constexpr Reg Value_enabled = 0x1;
							constexpr Reg ValueMask_enabled = 0x00004000;
							};
						namespace fOVER8 { // Field Description
							constexpr Reg Lsb = 15;
							constexpr Reg FieldMask = 0x00008000;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_oversample16 = 0x0;
							constexpr Reg ValueMask_oversample16 = 0x00000000;
							constexpr Reg Value_oversample8 = 0x1;
							constexpr Reg ValueMask_oversample8 = 0x00008000;
							};
						namespace fDEDT { // Field Description
							constexpr Reg Lsb = 16;
							constexpr Reg FieldMask = 0x001F0000;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_minSamples = 0x0;
							constexpr Reg ValueMask_minSamples = 0x00000000;
							constexpr Reg Value_maxSamples = 0x1F;
							constexpr Reg ValueMask_maxSamples = 0x001F0000;
							};
						namespace fDEAT { // Field Description
							constexpr Reg Lsb = 21;
							constexpr Reg FieldMask = 0x03E00000;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_minSamples = 0x0;
							constexpr Reg ValueMask_minSamples = 0x00000000;
							constexpr Reg Value_maxSamples = 0x1F;
							constexpr Reg ValueMask_maxSamples = 0x03E00000;
							};
						namespace fRTOIE { // Field Description
							constexpr Reg Lsb = 26;
							constexpr Reg FieldMask = 0x04000000;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_disable = 0x0;
							constexpr Reg ValueMask_disable = 0x00000000;
							constexpr Reg Value_disabled = 0x0;
							constexpr Reg ValueMask_disabled = 0x00000000;
							constexpr Reg Value_enable = 0x1;
							constexpr Reg ValueMask_enable = 0x04000000;
							constexpr Reg Value_enabled = 0x1;
							constexpr Reg ValueMask_enabled = 0x04000000;
							};
						namespace fEOBIE { // Field Description
							constexpr Reg Lsb = 27;
							constexpr Reg FieldMask = 0x08000000;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_disable = 0x0;
							constexpr Reg ValueMask_disable = 0x00000000;
							constexpr Reg Value_disabled = 0x0;
							constexpr Reg ValueMask_disabled = 0x00000000;
							constexpr Reg Value_enable = 0x1;
							constexpr Reg ValueMask_enable = 0x08000000;
							constexpr Reg Value_enabled = 0x1;
							constexpr Reg ValueMask_enabled = 0x08000000;
							};
						namespace fM1 { // Field Description
							constexpr Reg Lsb = 28;
							constexpr Reg FieldMask = 0x10000000;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_v18Nor19N = 0x0;
							constexpr Reg ValueMask_v18Nor19N = 0x00000000;
							constexpr Reg Value_v17N = 0x1;
							constexpr Reg ValueMask_v17N = 0x10000000;
							};
						namespace fFIFOEN { // Field Description
							constexpr Reg Lsb = 29;
							constexpr Reg FieldMask = 0x20000000;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_disable = 0x0;
							constexpr Reg ValueMask_disable = 0x00000000;
							constexpr Reg Value_disabled = 0x0;
							constexpr Reg ValueMask_disabled = 0x00000000;
							constexpr Reg Value_enable = 0x1;
							constexpr Reg ValueMask_enable = 0x20000000;
							constexpr Reg Value_enabled = 0x1;
							constexpr Reg ValueMask_enabled = 0x20000000;
							};
						namespace fTXFEIE { // Field Description
							constexpr Reg Lsb = 30;
							constexpr Reg FieldMask = 0x40000000;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_disable = 0x0;
							constexpr Reg ValueMask_disable = 0x00000000;
							constexpr Reg Value_disabled = 0x0;
							constexpr Reg ValueMask_disabled = 0x00000000;
							constexpr Reg Value_enable = 0x1;
							constexpr Reg ValueMask_enable = 0x40000000;
							constexpr Reg Value_enabled = 0x1;
							constexpr Reg ValueMask_enabled = 0x40000000;
							};
						namespace fRXFFIE { // Field Description
							constexpr Reg Lsb = 31;
							constexpr Reg FieldMask = 0x80000000;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_disable = 0x0;
							constexpr Reg ValueMask_disable = 0x00000000;
							constexpr Reg Value_disabled = 0x0;
							constexpr Reg ValueMask_disabled = 0x00000000;
							constexpr Reg Value_enable = 0x1;
							constexpr Reg ValueMask_enable = 0x80000000;
							constexpr Reg Value_enabled = 0x1;
							constexpr Reg ValueMask_enabled = 0x80000000;
							};
						};
					namespace rCR2 { // Register description
						typedef uint32_t	Reg;
						constexpr Reg	valueAtReset = 0x00000000;
						namespace fSLVEN { // Field Description
							constexpr Reg Lsb = 0;
							constexpr Reg FieldMask = 0x00000001;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_disable = 0x0;
							constexpr Reg ValueMask_disable = 0x00000000;
							constexpr Reg Value_disabled = 0x0;
							constexpr Reg ValueMask_disabled = 0x00000000;
							constexpr Reg Value_enable = 0x1;
							constexpr Reg ValueMask_enable = 0x00000001;
							constexpr Reg Value_enabled = 0x1;
							constexpr Reg ValueMask_enabled = 0x00000001;
							};
						namespace fDIS_NSS { // Field Description
							constexpr Reg Lsb = 3;
							constexpr Reg FieldMask = 0x00000008;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_observeNssPin = 0x0;
							constexpr Reg ValueMask_observeNssPin = 0x00000000;
							constexpr Reg Value_ignoreNssPin = 0x1;
							constexpr Reg ValueMask_ignoreNssPin = 0x00000008;
							};
						namespace fADDM7 { // Field Description
							constexpr Reg Lsb = 4;
							constexpr Reg FieldMask = 0x00000010;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_fourBitAddress = 0x0;
							constexpr Reg ValueMask_fourBitAddress = 0x00000000;
							constexpr Reg Value_sevenBitAddress = 0x1;
							constexpr Reg ValueMask_sevenBitAddress = 0x00000010;
							};
						namespace fLBDL { // Field Description
							constexpr Reg Lsb = 5;
							constexpr Reg FieldMask = 0x00000020;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_v10Bit = 0x0;
							constexpr Reg ValueMask_v10Bit = 0x00000000;
							constexpr Reg Value_v11Bit = 0x1;
							constexpr Reg ValueMask_v11Bit = 0x00000020;
							};
						namespace fLBDIE { // Field Description
							constexpr Reg Lsb = 6;
							constexpr Reg FieldMask = 0x00000040;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_disable = 0x0;
							constexpr Reg ValueMask_disable = 0x00000000;
							constexpr Reg Value_disabled = 0x0;
							constexpr Reg ValueMask_disabled = 0x00000000;
							constexpr Reg Value_enable = 0x1;
							constexpr Reg ValueMask_enable = 0x00000040;
							constexpr Reg Value_enabled = 0x1;
							constexpr Reg ValueMask_enabled = 0x00000040;
							};
						namespace fLBCL { // Field Description
							constexpr Reg Lsb = 8;
							constexpr Reg FieldMask = 0x00000100;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_noClock = 0x0;
							constexpr Reg ValueMask_noClock = 0x00000000;
							constexpr Reg Value_outputClock = 0x1;
							constexpr Reg ValueMask_outputClock = 0x00000100;
							};
						namespace fCPHA { // Field Description
							constexpr Reg Lsb = 9;
							constexpr Reg FieldMask = 0x00000200;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_firstClock = 0x0;
							constexpr Reg ValueMask_firstClock = 0x00000000;
							constexpr Reg Value_secondClock = 0x1;
							constexpr Reg ValueMask_secondClock = 0x00000200;
							};
						namespace fCPOL { // Field Description
							constexpr Reg Lsb = 10;
							constexpr Reg FieldMask = 0x00000400;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_low = 0x0;
							constexpr Reg ValueMask_low = 0x00000000;
							constexpr Reg Value_high = 0x1;
							constexpr Reg ValueMask_high = 0x00000400;
							};
						namespace fCLKEN { // Field Description
							constexpr Reg Lsb = 11;
							constexpr Reg FieldMask = 0x00000800;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_disable = 0x0;
							constexpr Reg ValueMask_disable = 0x00000000;
							constexpr Reg Value_disabled = 0x0;
							constexpr Reg ValueMask_disabled = 0x00000000;
							constexpr Reg Value_enable = 0x1;
							constexpr Reg ValueMask_enable = 0x00000800;
							constexpr Reg Value_enabled = 0x1;
							constexpr Reg ValueMask_enabled = 0x00000800;
							};
						namespace fSTOP { // Field Description
							constexpr Reg Lsb = 12;
							constexpr Reg FieldMask = 0x00003000;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_v1_0StopBits = 0x0;
							constexpr Reg ValueMask_v1_0StopBits = 0x00000000;
							constexpr Reg Value_v0_5StopBits = 0x1;
							constexpr Reg ValueMask_v0_5StopBits = 0x00001000;
							constexpr Reg Value_v2_0StopBits = 0x2;
							constexpr Reg ValueMask_v2_0StopBits = 0x00002000;
							constexpr Reg Value_v1_5StopBits = 0x3;
							constexpr Reg ValueMask_v1_5StopBits = 0x00003000;
							};
						namespace fLINEN { // Field Description
							constexpr Reg Lsb = 14;
							constexpr Reg FieldMask = 0x00004000;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_disable = 0x0;
							constexpr Reg ValueMask_disable = 0x00000000;
							constexpr Reg Value_disabled = 0x0;
							constexpr Reg ValueMask_disabled = 0x00000000;
							constexpr Reg Value_enable = 0x1;
							constexpr Reg ValueMask_enable = 0x00004000;
							constexpr Reg Value_enabled = 0x1;
							constexpr Reg ValueMask_enabled = 0x00004000;
							};
						namespace fSWAP { // Field Description
							constexpr Reg Lsb = 15;
							constexpr Reg FieldMask = 0x00008000;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_standard = 0x0;
							constexpr Reg ValueMask_standard = 0x00000000;
							constexpr Reg Value_swapRxTx = 0x1;
							constexpr Reg ValueMask_swapRxTx = 0x00008000;
							};
						namespace fRXINV { // Field Description
							constexpr Reg Lsb = 16;
							constexpr Reg FieldMask = 0x00010000;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_standard = 0x0;
							constexpr Reg ValueMask_standard = 0x00000000;
							constexpr Reg Value_invert = 0x1;
							constexpr Reg ValueMask_invert = 0x00010000;
							};
						namespace fTXINV { // Field Description
							constexpr Reg Lsb = 17;
							constexpr Reg FieldMask = 0x00020000;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_standard = 0x0;
							constexpr Reg ValueMask_standard = 0x00000000;
							constexpr Reg Value_invert = 0x1;
							constexpr Reg ValueMask_invert = 0x00020000;
							};
						namespace fDATAINV { // Field Description
							constexpr Reg Lsb = 18;
							constexpr Reg FieldMask = 0x00040000;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_standard = 0x0;
							constexpr Reg ValueMask_standard = 0x00000000;
							constexpr Reg Value_invert = 0x1;
							constexpr Reg ValueMask_invert = 0x00040000;
							};
						namespace fMSBFIRST { // Field Description
							constexpr Reg Lsb = 19;
							constexpr Reg FieldMask = 0x00080000;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_lsbFirst = 0x0;
							constexpr Reg ValueMask_lsbFirst = 0x00000000;
							constexpr Reg Value_msbFirst = 0x1;
							constexpr Reg ValueMask_msbFirst = 0x00080000;
							};
						namespace fABREN { // Field Description
							constexpr Reg Lsb = 20;
							constexpr Reg FieldMask = 0x00100000;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_disable = 0x0;
							constexpr Reg ValueMask_disable = 0x00000000;
							constexpr Reg Value_disabled = 0x0;
							constexpr Reg ValueMask_disabled = 0x00000000;
							constexpr Reg Value_enable = 0x1;
							constexpr Reg ValueMask_enable = 0x00100000;
							constexpr Reg Value_enabled = 0x1;
							constexpr Reg ValueMask_enabled = 0x00100000;
							};
						namespace fABRMOD { // Field Description
							constexpr Reg Lsb = 21;
							constexpr Reg FieldMask = 0x00600000;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_measureStartBit = 0x0;
							constexpr Reg ValueMask_measureStartBit = 0x00000000;
							constexpr Reg Value_measureFallingEdges = 0x1;
							constexpr Reg ValueMask_measureFallingEdges = 0x00200000;
							constexpr Reg Value_detect0x7F = 0x2;
							constexpr Reg ValueMask_detect0x7F = 0x00400000;
							constexpr Reg Value_detect0x55 = 0x3;
							constexpr Reg ValueMask_detect0x55 = 0x00600000;
							};
						namespace fRTOEN { // Field Description
							constexpr Reg Lsb = 23;
							constexpr Reg FieldMask = 0x00800000;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_disable = 0x0;
							constexpr Reg ValueMask_disable = 0x00000000;
							constexpr Reg Value_disabled = 0x0;
							constexpr Reg ValueMask_disabled = 0x00000000;
							constexpr Reg Value_enable = 0x1;
							constexpr Reg ValueMask_enable = 0x00800000;
							constexpr Reg Value_enabled = 0x1;
							constexpr Reg ValueMask_enabled = 0x00800000;
							};
						namespace fADD0_3 { // Field Description
							constexpr Reg Lsb = 24;
							constexpr Reg FieldMask = 0xFF000000;
							constexpr Reg Value_valueAtReset = 0x0;
							constexpr Reg ValueMask_valueAtReset = 0x00000000;
							constexpr Reg Value_minValue = 0x0;
							constexpr Reg ValueMask_minValue = 0x00000000;
							constexpr Reg Value_maxValue = 0xFF;
							constexpr Reg ValueMask_maxValue = 0xFF000000;
							};
						};
					namespace rCR3 { // Register description
						typedef uint32_t	Reg;
						namespace fEIE { // Field Description
							constexpr Reg Lsb = 0;
							constexpr Reg FieldMask = 0x00000001;
							};
						namespace fIREN { // Field Description
							constexpr Reg Lsb = 1;
							constexpr Reg FieldMask = 0x00000002;
							};
						namespace fIRLP { // Field Description
							constexpr Reg Lsb = 2;
							constexpr Reg FieldMask = 0x00000004;
							};
						namespace fHDSEL { // Field Description
							constexpr Reg Lsb = 3;
							constexpr Reg FieldMask = 0x00000008;
							};
						namespace fNACK { // Field Description
							constexpr Reg Lsb = 4;
							constexpr Reg FieldMask = 0x00000010;
							};
						namespace fSCEN { // Field Description
							constexpr Reg Lsb = 5;
							constexpr Reg FieldMask = 0x00000020;
							};
						namespace fDMAR { // Field Description
							constexpr Reg Lsb = 6;
							constexpr Reg FieldMask = 0x00000040;
							};
						namespace fDMAT { // Field Description
							constexpr Reg Lsb = 7;
							constexpr Reg FieldMask = 0x00000080;
							};
						namespace fRTSE { // Field Description
							constexpr Reg Lsb = 8;
							constexpr Reg FieldMask = 0x00000100;
							};
						namespace fCTSE { // Field Description
							constexpr Reg Lsb = 9;
							constexpr Reg FieldMask = 0x00000200;
							};
						namespace fCTSIE { // Field Description
							constexpr Reg Lsb = 10;
							constexpr Reg FieldMask = 0x00000400;
							};
						namespace fONEBIT { // Field Description
							constexpr Reg Lsb = 11;
							constexpr Reg FieldMask = 0x00000800;
							};
						namespace fOVRDIS { // Field Description
							constexpr Reg Lsb = 12;
							constexpr Reg FieldMask = 0x00001000;
							};
						namespace fDDRE { // Field Description
							constexpr Reg Lsb = 13;
							constexpr Reg FieldMask = 0x00002000;
							};
						namespace fDEM { // Field Description
							constexpr Reg Lsb = 14;
							constexpr Reg FieldMask = 0x00004000;
							};
						namespace fDEP { // Field Description
							constexpr Reg Lsb = 15;
							constexpr Reg FieldMask = 0x00008000;
							};
						namespace fSCARCNT { // Field Description
							constexpr Reg Lsb = 17;
							constexpr Reg FieldMask = 0x000E0000;
							};
						namespace fWUS { // Field Description
							constexpr Reg Lsb = 20;
							constexpr Reg FieldMask = 0x00300000;
							};
						namespace fWUFIE { // Field Description
							constexpr Reg Lsb = 22;
							constexpr Reg FieldMask = 0x00400000;
							};
						namespace fTXFTIE { // Field Description
							constexpr Reg Lsb = 23;
							constexpr Reg FieldMask = 0x00800000;
							};
						namespace fTCBGTIE { // Field Description
							constexpr Reg Lsb = 24;
							constexpr Reg FieldMask = 0x01000000;
							};
						namespace fRXFTCFG { // Field Description
							constexpr Reg Lsb = 25;
							constexpr Reg FieldMask = 0x0E000000;
							};
						namespace fRXFTIE { // Field Description
							constexpr Reg Lsb = 28;
							constexpr Reg FieldMask = 0x10000000;
							};
						namespace fTXFTCFG { // Field Description
							constexpr Reg Lsb = 29;
							constexpr Reg FieldMask = 0xE0000000;
							};
						};
					namespace rBRR { // Register description
						typedef uint32_t	Reg;
						namespace fBRR_0_3 { // Field Description
							constexpr Reg Lsb = 0;
							constexpr Reg FieldMask = 0x0000000F;
							};
						namespace fBRR_4_15 { // Field Description
							constexpr Reg Lsb = 4;
							constexpr Reg FieldMask = 0x0000FFF0;
							};
						};
					namespace rGTPR { // Register description
						typedef uint32_t	Reg;
						namespace fPSC { // Field Description
							constexpr Reg Lsb = 0;
							constexpr Reg FieldMask = 0x000000FF;
							};
						namespace fGT { // Field Description
							constexpr Reg Lsb = 8;
							constexpr Reg FieldMask = 0x0000FF00;
							};
						};
					namespace rRTOR { // Register description
						typedef uint32_t	Reg;
						namespace fRTO { // Field Description
							constexpr Reg Lsb = 0;
							constexpr Reg FieldMask = 0x00FFFFFF;
							};
						namespace fBLEN { // Field Description
							constexpr Reg Lsb = 24;
							constexpr Reg FieldMask = 0xFF000000;
							};
						};
					namespace rRQR { // Register description
						typedef uint32_t	Reg;
						namespace fABRRQ { // Field Description
							constexpr Reg Lsb = 0;
							constexpr Reg FieldMask = 0x00000001;
							};
						namespace fSBKRQ { // Field Description
							constexpr Reg Lsb = 1;
							constexpr Reg FieldMask = 0x00000002;
							};
						namespace fMMRQ { // Field Description
							constexpr Reg Lsb = 2;
							constexpr Reg FieldMask = 0x00000004;
							};
						namespace fRXFRQ { // Field Description
							constexpr Reg Lsb = 3;
							constexpr Reg FieldMask = 0x00000008;
							};
						namespace fTXFRQ { // Field Description
							constexpr Reg Lsb = 4;
							constexpr Reg FieldMask = 0x00000010;
							};
						};
					namespace rISR { // Register description
						typedef uint32_t	Reg;
						namespace fPE { // Field Description
							constexpr Reg Lsb = 0;
							constexpr Reg FieldMask = 0x00000001;
							};
						namespace fFE { // Field Description
							constexpr Reg Lsb = 1;
							constexpr Reg FieldMask = 0x00000002;
							};
						namespace fNF { // Field Description
							constexpr Reg Lsb = 2;
							constexpr Reg FieldMask = 0x00000004;
							};
						namespace fORE { // Field Description
							constexpr Reg Lsb = 3;
							constexpr Reg FieldMask = 0x00000008;
							};
						namespace fIDLE { // Field Description
							constexpr Reg Lsb = 4;
							constexpr Reg FieldMask = 0x00000010;
							};
						namespace fRXNE { // Field Description
							constexpr Reg Lsb = 5;
							constexpr Reg FieldMask = 0x00000020;
							};
						namespace fTC { // Field Description
							constexpr Reg Lsb = 6;
							constexpr Reg FieldMask = 0x00000040;
							};
						namespace fTXE { // Field Description
							constexpr Reg Lsb = 7;
							constexpr Reg FieldMask = 0x00000080;
							constexpr Reg Value_transferred = 0x1;
							constexpr Reg ValueMask_transferred = 0x00000080;
							};
						namespace fLBDF { // Field Description
							constexpr Reg Lsb = 8;
							constexpr Reg FieldMask = 0x00000100;
							};
						namespace fCTSIF { // Field Description
							constexpr Reg Lsb = 9;
							constexpr Reg FieldMask = 0x00000200;
							};
						namespace fCTS { // Field Description
							constexpr Reg Lsb = 10;
							constexpr Reg FieldMask = 0x00000400;
							};
						namespace fRTOF { // Field Description
							constexpr Reg Lsb = 11;
							constexpr Reg FieldMask = 0x00000800;
							};
						namespace fEOBF { // Field Description
							constexpr Reg Lsb = 12;
							constexpr Reg FieldMask = 0x00001000;
							};
						namespace fUDR { // Field Description
							constexpr Reg Lsb = 13;
							constexpr Reg FieldMask = 0x00002000;
							};
						namespace fABRE { // Field Description
							constexpr Reg Lsb = 14;
							constexpr Reg FieldMask = 0x00004000;
							};
						namespace fABRF { // Field Description
							constexpr Reg Lsb = 15;
							constexpr Reg FieldMask = 0x00008000;
							};
						namespace fBUSY { // Field Description
							constexpr Reg Lsb = 16;
							constexpr Reg FieldMask = 0x00010000;
							};
						namespace fCMF { // Field Description
							constexpr Reg Lsb = 17;
							constexpr Reg FieldMask = 0x00020000;
							};
						namespace fSBKF { // Field Description
							constexpr Reg Lsb = 18;
							constexpr Reg FieldMask = 0x00040000;
							};
						namespace fRWU { // Field Description
							constexpr Reg Lsb = 19;
							constexpr Reg FieldMask = 0x00080000;
							};
						namespace fWUF { // Field Description
							constexpr Reg Lsb = 20;
							constexpr Reg FieldMask = 0x00100000;
							};
						namespace fTEACK { // Field Description
							constexpr Reg Lsb = 21;
							constexpr Reg FieldMask = 0x00200000;
							};
						namespace fREACK { // Field Description
							constexpr Reg Lsb = 22;
							constexpr Reg FieldMask = 0x00400000;
							};
						namespace fTXFE { // Field Description
							constexpr Reg Lsb = 23;
							constexpr Reg FieldMask = 0x00800000;
							};
						namespace fRXFF { // Field Description
							constexpr Reg Lsb = 24;
							constexpr Reg FieldMask = 0x01000000;
							};
						namespace fTCBGT { // Field Description
							constexpr Reg Lsb = 25;
							constexpr Reg FieldMask = 0x02000000;
							};
						namespace fRXFT { // Field Description
							constexpr Reg Lsb = 26;
							constexpr Reg FieldMask = 0x04000000;
							};
						namespace fTXFT { // Field Description
							constexpr Reg Lsb = 27;
							constexpr Reg FieldMask = 0x08000000;
							};
						};
					namespace rICR { // Register description
						typedef uint32_t	Reg;
						namespace fPECF { // Field Description
							constexpr Reg Lsb = 0;
							constexpr Reg FieldMask = 0x00000001;
							};
						namespace fFECF { // Field Description
							constexpr Reg Lsb = 1;
							constexpr Reg FieldMask = 0x00000002;
							};
						namespace fNCF { // Field Description
							constexpr Reg Lsb = 2;
							constexpr Reg FieldMask = 0x00000004;
							};
						namespace fORECF { // Field Description
							constexpr Reg Lsb = 3;
							constexpr Reg FieldMask = 0x00000008;
							};
						namespace fIDLECF { // Field Description
							constexpr Reg Lsb = 4;
							constexpr Reg FieldMask = 0x00000010;
							};
						namespace fTXFECF { // Field Description
							constexpr Reg Lsb = 5;
							constexpr Reg FieldMask = 0x00000020;
							};
						namespace fTCCF { // Field Description
							constexpr Reg Lsb = 6;
							constexpr Reg FieldMask = 0x00000040;
							};
						namespace fTCBGTC { // Field Description
							constexpr Reg Lsb = 7;
							constexpr Reg FieldMask = 0x00000080;
							};
						namespace fLBDCF { // Field Description
							constexpr Reg Lsb = 8;
							constexpr Reg FieldMask = 0x00000100;
							};
						namespace fCTSCF { // Field Description
							constexpr Reg Lsb = 9;
							constexpr Reg FieldMask = 0x00000200;
							};
						namespace fRTOCF { // Field Description
							constexpr Reg Lsb = 11;
							constexpr Reg FieldMask = 0x00000800;
							};
						namespace fEOBCF { // Field Description
							constexpr Reg Lsb = 12;
							constexpr Reg FieldMask = 0x00001000;
							};
						namespace fUDRCF { // Field Description
							constexpr Reg Lsb = 13;
							constexpr Reg FieldMask = 0x00002000;
							};
						namespace fCMCF { // Field Description
							constexpr Reg Lsb = 17;
							constexpr Reg FieldMask = 0x00020000;
							};
						namespace fWUCF { // Field Description
							constexpr Reg Lsb = 20;
							constexpr Reg FieldMask = 0x00100000;
							};
						};
					namespace rRDR { // Register description
						typedef uint32_t	Reg;
						namespace fRDR { // Field Description
							constexpr Reg Lsb = 0;
							constexpr Reg FieldMask = 0x000001FF;
							};
						};
					namespace rTDR { // Register description
						typedef uint32_t	Reg;
						namespace fTDR { // Field Description
							constexpr Reg Lsb = 0;
							constexpr Reg FieldMask = 0x000001FF;
							};
						};
					namespace rPRESC { // Register description
						typedef uint32_t	Reg;
						namespace fPRESCALER { // Field Description
							constexpr Reg Lsb = 0;
							constexpr Reg FieldMask = 0x0000000F;
							};
						};
					}
				}
			}
		}
	}
#endif
