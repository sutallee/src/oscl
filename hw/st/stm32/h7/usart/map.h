/*
   Copyright (C) 2024 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_hw_st_stm32_h7_usart_maph_
#define _oscl_hw_st_stm32_h7_usart_maph_
#include "reg.h"
namespace Oscl {
namespace St {
namespace Stm32 {
namespace H7 {
namespace pUSART {
struct Map {
	/** offset:0x0 */
	volatile rCR1::Reg	cr1;
	/** offset:0x4 */
	volatile rCR2::Reg	cr2;
	/** offset:0x8 */
	volatile rCR3::Reg	cr3;
	/** offset:0xc */
	volatile rBRR::Reg	brr;
	/** offset:0x10 */
	volatile rGTPR::Reg	gtpr;
	/** offset:0x14 */
	volatile rRTOR::Reg	rtor;
	/** offset:0x18 */
	volatile rRQR::Reg	rqr;
	/** offset:0x1c */
	volatile rISR::Reg	isr;
	/** offset:0x20 */
	volatile rICR::Reg	icr;
	/** offset:0x24 */
	volatile rRDR::Reg	rdr;
	/** offset:0x28 */
	volatile rTDR::Reg	tdr;
	/** offset:0x2c */
	volatile rPRESC::Reg	presc;
	};
}
}
}
}
}
#endif
