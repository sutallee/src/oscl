module oscl_hw_st_stm32_h7_usart_regh {
sysinclude "stdint.h";
namespace Oscl {
namespace St {
namespace Stm32 {
namespace H7 {
namespace pUSART {
	register rCR1 uint32_t {
		value valueAtReset	0x00000000
		field fUE 0 1 {
			value valueAtReset	0
			value disable		0
			value disabled		0
			value enable		1
			value enabled		1
			}
		field fUESM 1 1 {
			value valueAtReset	0
			value wakeupDisable		0
			value wakeupDisabled	0
			value wakeupEnable		1
			value wakeupEnabled		1
			}
		field fRE 2 1 {
			value valueAtReset	0
			value disable		0
			value disabled		0
			value enable		1
			value enabled		1
			}
		field fTE 3 1 {
			value valueAtReset	0
			value disable		0
			value disabled		0
			value enable		1
			value enabled		1
			}
		field fIDLEIE 4 1 {
			value valueAtReset	0
			value disable		0
			value disabled		0
			value enable		1
			value enabled		1
			}
		field fRXNEIE 5 1 {
			value valueAtReset	0
			value disable		0
			value disabled		0
			value enable		1
			value enabled		1
			}
		field fTCIE 6 1 {
			value valueAtReset	0
			value disable		0
			value disabled		0
			value enable		1
			value enabled		1
			}
		field fTXEIE 7 1 {
			value valueAtReset	0
			value disable		0
			value disabled		0
			value enable		1
			value enabled		1
			}
		field fPEIE 8 1 {
			value valueAtReset	0
			value disable		0
			value disabled		0
			value enable		1
			value enabled		1
			}
		field fPS 9 1 {
			value valueAtReset	0
			value even		0
			value odd		1
			}
		field fPCE 10 1 {
			value valueAtReset	0
			value disable		0
			value disabled		0
			value enable		1
			value enabled		1
			}
		field fWAKE 11 1 {
			value valueAtReset	0
			value idleLine		0
			value addressMark	1
			}
		field fM0 12 1 {
			value valueAtReset	0
			value v18Nor17N		0
			value v19N			1
			}
		field fMME 13 1 {
			value valueAtReset	0
			value rxAlwaysActive	0
			value rxMuteSwitch		1
			}
		field fCMIE 14 1 {
			value valueAtReset	0
			value disable		0
			value disabled		0
			value enable		1
			value enabled		1
			}
		field fOVER8 15 1 {
			value valueAtReset	0
			value oversample16		0
			value oversample8		1
			}
		field fDEDT 16 5 {
			/* Some USARTs do not support this feature */
			value valueAtReset	0x00
			value minSamples	0x00
			value maxSamples	0x1F
			}
		field fDEAT 21 5 {
			/* Some USARTs do not support this feature */
			value valueAtReset	0x00
			value minSamples	0x00
			value maxSamples	0x1F
			}
		field fRTOIE 26 1 {
			/* Some USARTs do not support this feature */
			value valueAtReset	0
			value disable		0
			value disabled		0
			value enable		1
			value enabled		1
			}
		field fEOBIE 27 1 {
			/* Some USARTs do not support this feature */
			value valueAtReset	0
			value disable		0
			value disabled		0
			value enable		1
			value enabled		1
			}
		field fM1 28 1 {
			value valueAtReset	0
			value v18Nor19N		0
			value v17N			1
			}
		field fFIFOEN 29 1 {
			value valueAtReset	0
			value disable		0
			value disabled		0
			value enable		1
			value enabled		1
			}
		field fTXFEIE 30 1 {
			value valueAtReset	0
			value disable		0
			value disabled		0
			value enable		1
			value enabled		1
			}
		field fRXFFIE 31 1 {
			value valueAtReset	0
			value disable		0
			value disabled		0
			value enable		1
			value enabled		1
			}
		}
	register rCR2 uint32_t {
		value valueAtReset	0x00000000
		field fSLVEN 0 1 {
			/* Some USARTs do not support this feature */
			value valueAtReset	0
			value disable		0
			value disabled		0
			value enable		1
			value enabled		1
			}
		field fDIS_NSS 3 1 {
			/* Some USARTs do not support this feature */
			value valueAtReset	0
			value observeNssPin	0
			value ignoreNssPin	1
			}
		field fADDM7 4 1 {
			value valueAtReset	0
			value fourBitAddress	0
			value sevenBitAddress	1
			}
		field fLBDL 5 1 {
			/* Some USARTs do not support this feature */
			value valueAtReset	0
			value v10Bit	0
			value v11Bit	1
			}
		field fLBDIE 6 1 {
			/* Some USARTs do not support this feature */
			value valueAtReset	0
			value disable		0
			value disabled		0
			value enable		1
			value enabled		1
			}
		field fLBCL 8 1 {
			/* Some USARTs do not support this feature */
			value valueAtReset	0
			value noClock		0
			value outputClock	1
			}
		field fCPHA 9 1 {
			/* Some USARTs do not support this feature */
			value valueAtReset	0
			value firstClock	0
			value secondClock	1
			}
		field fCPOL 10 1 {
			/* Some USARTs do not support this feature */
			value valueAtReset	0
			value low	0
			value high	1
			}
		field fCLKEN 11 1 {
			value valueAtReset	0
			value disable		0
			value disabled		0
			value enable		1
			value enabled		1
			}
		field fSTOP 12 2 {
			value valueAtReset	0x0
			value v1_0StopBits	0x0
			value v0_5StopBits	0x1
			value v2_0StopBits	0x2
			value v1_5StopBits	0x3
			}
		field fLINEN 14 1 {
			value valueAtReset	0
			value disable		0
			value disabled		0
			value enable		1
			value enabled		1
			}
		field fSWAP 15 1 {
			value valueAtReset	0
			value standard		0
			value swapRxTx		1
			}
		field fRXINV 16 1 {
			value valueAtReset	0
			value standard		0
			value invert		1
			}
		field fTXINV 17 1 {
			value valueAtReset	0
			value standard		0
			value invert		1
			}
		field fDATAINV 18 1 {
			value valueAtReset	0
			value standard		0
			value invert		1
			}
		field fMSBFIRST 19 1 {
			value valueAtReset	0
			value lsbFirst		0
			value msbFirst		1
			}
		field fABREN 20 1 {
			value valueAtReset	0
			value disable		0
			value disabled		0
			value enable		1
			value enabled		1
			}
		field fABRMOD 21 2 {
			value valueAtReset	0x0
			value measureStartBit		0x0
			value measureFallingEdges	0x1
			value detect0x7F			0x2
			value detect0x55			0x3
			}
		field fRTOEN 23 1 {
			value valueAtReset	0
			value disable		0
			value disabled		0
			value enable		1
			value enabled		1
			}
		field fADD0_3 24 8 {
			value valueAtReset	0x00
			value minValue		0x00
			value maxValue		0xFF
			}
		}
	register rCR3 uint32_t {
		field fEIE 0 1 {
			}
		field fIREN 1 1 {
			}
		field fIRLP 2 1 {
			}
		field fHDSEL 3 1 {
			}
		field fNACK 4 1 {
			}
		field fSCEN 5 1 {
			}
		field fDMAR 6 1 {
			}
		field fDMAT 7 1 {
			}
		field fRTSE 8 1 {
			}
		field fCTSE 9 1 {
			}
		field fCTSIE 10 1 {
			}
		field fONEBIT 11 1 {
			}
		field fOVRDIS 12 1 {
			}
		field fDDRE 13 1 {
			}
		field fDEM 14 1 {
			}
		field fDEP 15 1 {
			}
		field fSCARCNT 17 3 {
			}
		field fWUS 20 2 {
			}
		field fWUFIE 22 1 {
			}
		field fTXFTIE 23 1 {
			}
		field fTCBGTIE 24 1 {
			}
		field fRXFTCFG 25 3 {
			}
		field fRXFTIE 28 1 {
			}
		field fTXFTCFG 29 3 {
			}
		}
	register rBRR uint32_t {
		field fBRR_0_3 0 4 {
			}
		field fBRR_4_15 4 12 {
			}
		}
	register rGTPR uint32_t {
		field fPSC 0 8 {
			}
		field fGT 8 8 {
			}
		}
	register rRTOR uint32_t {
		field fRTO 0 24 {
			}
		field fBLEN 24 8 {
			}
		}
	register rRQR uint32_t {
		field fABRRQ 0 1 {
			}
		field fSBKRQ 1 1 {
			}
		field fMMRQ 2 1 {
			}
		field fRXFRQ 3 1 {
			}
		field fTXFRQ 4 1 {
			}
		}
	register rISR uint32_t {
		field fPE 0 1 {
			}
		field fFE 1 1 {
			}
		field fNF 2 1 {
			}
		field fORE 3 1 {
			}
		field fIDLE 4 1 {
			}
		field fRXNE 5 1 {
			}
		field fTC 6 1 {
			}
		field fTXE 7 1 {
			value transferred	1
			}
		field fLBDF 8 1 {
			}
		field fCTSIF 9 1 {
			}
		field fCTS 10 1 {
			}
		field fRTOF 11 1 {
			}
		field fEOBF 12 1 {
			}
		field fUDR 13 1 {
			}
		field fABRE 14 1 {
			}
		field fABRF 15 1 {
			}
		field fBUSY 16 1 {
			}
		field fCMF 17 1 {
			}
		field fSBKF 18 1 {
			}
		field fRWU 19 1 {
			}
		field fWUF 20 1 {
			}
		field fTEACK 21 1 {
			}
		field fREACK 22 1 {
			}
		field fTXFE 23 1 {
			}
		field fRXFF 24 1 {
			}
		field fTCBGT 25 1 {
			}
		field fRXFT 26 1 {
			}
		field fTXFT 27 1 {
			}
		}
	register rICR uint32_t {
		field fPECF 0 1 {
			}
		field fFECF 1 1 {
			}
		field fNCF 2 1 {
			}
		field fORECF 3 1 {
			}
		field fIDLECF 4 1 {
			}
		field fTXFECF 5 1 {
			}
		field fTCCF 6 1 {
			}
		field fTCBGTC 7 1 {
			}
		field fLBDCF 8 1 {
			}
		field fCTSCF 9 1 {
			}
		field fRTOCF 11 1 {
			}
		field fEOBCF 12 1 {
			}
		field fUDRCF 13 1 {
			}
		field fCMCF 17 1 {
			}
		field fWUCF 20 1 {
			}
		}
	register rRDR uint32_t {
		field fRDR 0 9 {
			}
		}
	register rTDR uint32_t {
		field fTDR 0 9 {
			}
		}
	register rPRESC uint32_t {
		field fPRESCALER 0 4 {
			}
		}
}
}
}
}
}
}
