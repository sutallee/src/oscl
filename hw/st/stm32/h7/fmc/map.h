/*
   Copyright (C) 2024 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_hw_st_stm32_h7_fmc_maph_
#define _oscl_hw_st_stm32_h7_fmc_maph_
#include "reg.h"
namespace Oscl {
namespace St {
namespace Stm32 {
namespace H7 {
namespace pFMC {
struct Map {
	/** offset:0x0 */
	volatile Oscl::St::Stm32::H7::pFMC::rBCR1::Reg	bcr1;
	/** offset:0x4 */
	volatile Oscl::St::Stm32::H7::pFMC::rBTR1::Reg	btr1;
	/** offset:0x8 */
	volatile Oscl::St::Stm32::H7::pFMC::rBCR2::Reg	bcr2;
	/** offset:0xc */
	volatile Oscl::St::Stm32::H7::pFMC::rBTR2::Reg	btr2;
	/** offset:0x10 */
	volatile Oscl::St::Stm32::H7::pFMC::rBCR3::Reg	bcr3;
	/** offset:0x14 */
	volatile Oscl::St::Stm32::H7::pFMC::rBTR3::Reg	btr3;
	/** offset:0x18 */
	volatile Oscl::St::Stm32::H7::pFMC::rBCR4::Reg	bcr4;
	/** offset:0x1c */
	volatile Oscl::St::Stm32::H7::pFMC::rBTR4::Reg	btr4;
	const uint8_t	_reserve32[96];
	/** offset:0x80 */
	volatile Oscl::St::Stm32::H7::pFMC::rPCR::Reg	pcr;
	/** offset:0x84 */
	volatile Oscl::St::Stm32::H7::pFMC::rSR::Reg	sr;
	/** offset:0x88 */
	volatile Oscl::St::Stm32::H7::pFMC::rPMEM::Reg	pmem;
	/** offset:0x8c */
	volatile Oscl::St::Stm32::H7::pFMC::rPATT::Reg	patt;
	const uint8_t	_reserve144[4];
	/** offset:0x94 */
	volatile Oscl::St::Stm32::H7::pFMC::rECCR::Reg	eccr;
	const uint8_t	_reserve152[108];
	/** offset:0x104 */
	volatile Oscl::St::Stm32::H7::pFMC::rBWTR1::Reg	bwtr1;
	const uint8_t	_reserve264[4];
	/** offset:0x10c */
	volatile Oscl::St::Stm32::H7::pFMC::rBWTR2::Reg	bwtr2;
	const uint8_t	_reserve272[4];
	/** offset:0x114 */
	volatile Oscl::St::Stm32::H7::pFMC::rBWTR3::Reg	bwtr3;
	const uint8_t	_reserve280[4];
	/** offset:0x11c */
	volatile Oscl::St::Stm32::H7::pFMC::rBWTR4::Reg	bwtr4;
	const uint8_t	_reserve288[32];
	/** offset:0x140 */
	volatile Oscl::St::Stm32::H7::pFMC::rSDCR1::Reg	sdcr1;
	/** offset:0x144 */
	volatile Oscl::St::Stm32::H7::pFMC::rSDCR2::Reg	sdcr2;
	/** offset:0x148 */
	volatile Oscl::St::Stm32::H7::pFMC::rSDTR1::Reg	sdtr1;
	/** offset:0x14c */
	volatile Oscl::St::Stm32::H7::pFMC::rSDTR2::Reg	sdtr2;
	/** offset:0x150 */
	volatile Oscl::St::Stm32::H7::pFMC::rSDCMR::Reg	sdcmr;
	/** offset:0x154 */
	volatile Oscl::St::Stm32::H7::pFMC::rSDRTR::Reg	sdrtr;
	/** offset:0x158 */
	volatile Oscl::St::Stm32::H7::pFMC::rSDSR::Reg	sdsr;
	};
}
}
}
}
}
#endif
