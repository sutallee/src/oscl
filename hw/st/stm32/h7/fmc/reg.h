/*
   Copyright (C) 2024 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_hw_st_stm32_h7_fmc_fmch_
#define _oscl_hw_st_stm32_h7_fmc_fmch_
#include <stdint.h>
namespace Oscl { // Namespace description
	namespace St { // Namespace description
		namespace Stm32 { // Namespace description
			namespace H7 { // Namespace description
				namespace pFMC { // Namespace description
					namespace rBCR1 { // Register description
						typedef uint32_t	Reg;
						namespace fMBKEN { // Field Description
							constexpr Reg Lsb = 0;
							constexpr Reg FieldMask = 0x00000001;
							constexpr Reg Value_disable = 0x0;
							constexpr Reg ValueMask_disable = 0x00000000;
							constexpr Reg Value_disabled = 0x0;
							constexpr Reg ValueMask_disabled = 0x00000000;
							constexpr Reg Value_enable = 0x1;
							constexpr Reg ValueMask_enable = 0x00000001;
							constexpr Reg Value_enabled = 0x1;
							constexpr Reg ValueMask_enabled = 0x00000001;
							};
						namespace fMUXEN { // Field Description
							constexpr Reg Lsb = 1;
							constexpr Reg FieldMask = 0x00000002;
							constexpr Reg Value_disable = 0x0;
							constexpr Reg ValueMask_disable = 0x00000000;
							constexpr Reg Value_disabled = 0x0;
							constexpr Reg ValueMask_disabled = 0x00000000;
							constexpr Reg Value_enable = 0x1;
							constexpr Reg ValueMask_enable = 0x00000002;
							constexpr Reg Value_enabled = 0x1;
							constexpr Reg ValueMask_enabled = 0x00000002;
							};
						namespace fMTYP { // Field Description
							constexpr Reg Lsb = 2;
							constexpr Reg FieldMask = 0x0000000C;
							constexpr Reg Value_SRAM = 0x0;
							constexpr Reg ValueMask_SRAM = 0x00000000;
							constexpr Reg Value_PSRAM = 0x1;
							constexpr Reg ValueMask_PSRAM = 0x00000004;
							constexpr Reg Value_CRAM = 0x1;
							constexpr Reg ValueMask_CRAM = 0x00000004;
							constexpr Reg Value_NORFLASH = 0x2;
							constexpr Reg ValueMask_NORFLASH = 0x00000008;
							constexpr Reg Value_OneNandFLASH = 0x2;
							constexpr Reg ValueMask_OneNandFLASH = 0x00000008;
							};
						namespace fMWID { // Field Description
							constexpr Reg Lsb = 4;
							constexpr Reg FieldMask = 0x00000030;
							constexpr Reg Value_width8Bits = 0x0;
							constexpr Reg ValueMask_width8Bits = 0x00000000;
							constexpr Reg Value_width16Bits = 0x1;
							constexpr Reg ValueMask_width16Bits = 0x00000010;
							constexpr Reg Value_width32Bits = 0x2;
							constexpr Reg ValueMask_width32Bits = 0x00000020;
							};
						namespace fFACCEN { // Field Description
							constexpr Reg Lsb = 6;
							constexpr Reg FieldMask = 0x00000040;
							constexpr Reg Value_disable = 0x0;
							constexpr Reg ValueMask_disable = 0x00000000;
							constexpr Reg Value_disabled = 0x0;
							constexpr Reg ValueMask_disabled = 0x00000000;
							constexpr Reg Value_enable = 0x1;
							constexpr Reg ValueMask_enable = 0x00000040;
							constexpr Reg Value_enabled = 0x1;
							constexpr Reg ValueMask_enabled = 0x00000040;
							};
						namespace fBURSTEN { // Field Description
							constexpr Reg Lsb = 8;
							constexpr Reg FieldMask = 0x00000100;
							constexpr Reg Value_disable = 0x0;
							constexpr Reg ValueMask_disable = 0x00000000;
							constexpr Reg Value_disabled = 0x0;
							constexpr Reg ValueMask_disabled = 0x00000000;
							constexpr Reg Value_enable = 0x1;
							constexpr Reg ValueMask_enable = 0x00000100;
							constexpr Reg Value_enabled = 0x1;
							constexpr Reg ValueMask_enabled = 0x00000100;
							};
						namespace fWAITPOL { // Field Description
							constexpr Reg Lsb = 9;
							constexpr Reg FieldMask = 0x00000200;
							constexpr Reg Value_low = 0x0;
							constexpr Reg ValueMask_low = 0x00000000;
							constexpr Reg Value_high = 0x1;
							constexpr Reg ValueMask_high = 0x00000200;
							};
						namespace fWAITCFG { // Field Description
							constexpr Reg Lsb = 11;
							constexpr Reg FieldMask = 0x00000800;
							constexpr Reg Value_oneDataCycleBeforeWaitState = 0x0;
							constexpr Reg ValueMask_oneDataCycleBeforeWaitState = 0x00000000;
							constexpr Reg Value_activeDuringWaitState = 0x1;
							constexpr Reg ValueMask_activeDuringWaitState = 0x00000800;
							};
						namespace fWREN { // Field Description
							constexpr Reg Lsb = 12;
							constexpr Reg FieldMask = 0x00001000;
							constexpr Reg Value_disable = 0x0;
							constexpr Reg ValueMask_disable = 0x00000000;
							constexpr Reg Value_disabled = 0x0;
							constexpr Reg ValueMask_disabled = 0x00000000;
							constexpr Reg Value_enable = 0x1;
							constexpr Reg ValueMask_enable = 0x00001000;
							constexpr Reg Value_enabled = 0x1;
							constexpr Reg ValueMask_enabled = 0x00001000;
							};
						namespace fWAITEN { // Field Description
							constexpr Reg Lsb = 13;
							constexpr Reg FieldMask = 0x00002000;
							constexpr Reg Value_disable = 0x0;
							constexpr Reg ValueMask_disable = 0x00000000;
							constexpr Reg Value_disabled = 0x0;
							constexpr Reg ValueMask_disabled = 0x00000000;
							constexpr Reg Value_enable = 0x1;
							constexpr Reg ValueMask_enable = 0x00002000;
							constexpr Reg Value_enabled = 0x1;
							constexpr Reg ValueMask_enabled = 0x00002000;
							};
						namespace fEXTMOD { // Field Description
							constexpr Reg Lsb = 14;
							constexpr Reg FieldMask = 0x00004000;
							constexpr Reg Value_ignoreBTWR = 0x0;
							constexpr Reg ValueMask_ignoreBTWR = 0x00000000;
							constexpr Reg Value_bwtrIgnored = 0x0;
							constexpr Reg ValueMask_bwtrIgnored = 0x00000000;
							constexpr Reg Value_useBTWR = 0x1;
							constexpr Reg ValueMask_useBTWR = 0x00004000;
							constexpr Reg Value_bwtrUsed = 0x1;
							constexpr Reg ValueMask_bwtrUsed = 0x00004000;
							};
						namespace fASYNCWAIT { // Field Description
							constexpr Reg Lsb = 15;
							constexpr Reg FieldMask = 0x00008000;
							constexpr Reg Value_ignoreNWAIT = 0x0;
							constexpr Reg ValueMask_ignoreNWAIT = 0x00000000;
							constexpr Reg Value_nwaitIgnored = 0x0;
							constexpr Reg ValueMask_nwaitIgnored = 0x00000000;
							constexpr Reg Value_useNWAIT = 0x1;
							constexpr Reg ValueMask_useNWAIT = 0x00008000;
							constexpr Reg Value_nwaitUsed = 0x1;
							constexpr Reg ValueMask_nwaitUsed = 0x00008000;
							};
						namespace fCPSIZE { // Field Description
							constexpr Reg Lsb = 16;
							constexpr Reg FieldMask = 0x00070000;
							constexpr Reg Value_noBurstSplit = 0x0;
							constexpr Reg ValueMask_noBurstSplit = 0x00000000;
							constexpr Reg Value_burst128Bytes = 0x1;
							constexpr Reg ValueMask_burst128Bytes = 0x00010000;
							constexpr Reg Value_burst256Bytes = 0x2;
							constexpr Reg ValueMask_burst256Bytes = 0x00020000;
							constexpr Reg Value_burst1024Bytes = 0x4;
							constexpr Reg ValueMask_burst1024Bytes = 0x00040000;
							};
						namespace fCBURSTRW { // Field Description
							constexpr Reg Lsb = 19;
							constexpr Reg FieldMask = 0x00080000;
							constexpr Reg Value_useAsyncMode = 0x0;
							constexpr Reg ValueMask_useAsyncMode = 0x00000000;
							constexpr Reg Value_useSyncMode = 0x1;
							constexpr Reg ValueMask_useSyncMode = 0x00080000;
							};
						namespace fCCLKEN { // Field Description
							constexpr Reg Lsb = 20;
							constexpr Reg FieldMask = 0x00100000;
							constexpr Reg Value_fmc_clkOnlyGeneratedForSync = 0x0;
							constexpr Reg ValueMask_fmc_clkOnlyGeneratedForSync = 0x00000000;
							constexpr Reg Value_fmc_clkContinuous = 0x1;
							constexpr Reg ValueMask_fmc_clkContinuous = 0x00100000;
							};
						namespace fWFDIS { // Field Description
							constexpr Reg Lsb = 21;
							constexpr Reg FieldMask = 0x00200000;
							constexpr Reg Value_disable = 0x1;
							constexpr Reg ValueMask_disable = 0x00200000;
							constexpr Reg Value_disabled = 0x1;
							constexpr Reg ValueMask_disabled = 0x00200000;
							constexpr Reg Value_enable = 0x0;
							constexpr Reg ValueMask_enable = 0x00000000;
							constexpr Reg Value_enabled = 0x0;
							constexpr Reg ValueMask_enabled = 0x00000000;
							};
						namespace fBMAP { // Field Description
							constexpr Reg Lsb = 24;
							constexpr Reg FieldMask = 0x03000000;
							constexpr Reg Value_defaultMapping = 0x0;
							constexpr Reg ValueMask_defaultMapping = 0x00000000;
							constexpr Reg Value_swapBank1 = 0x1;
							constexpr Reg ValueMask_swapBank1 = 0x01000000;
							};
						namespace fFMCEN { // Field Description
							constexpr Reg Lsb = 31;
							constexpr Reg FieldMask = 0x80000000;
							constexpr Reg Value_disable = 0x0;
							constexpr Reg ValueMask_disable = 0x00000000;
							constexpr Reg Value_disabled = 0x0;
							constexpr Reg ValueMask_disabled = 0x00000000;
							constexpr Reg Value_enable = 0x1;
							constexpr Reg ValueMask_enable = 0x80000000;
							constexpr Reg Value_enabled = 0x1;
							constexpr Reg ValueMask_enabled = 0x80000000;
							};
						};
					namespace rBTR1 { // Register description
						typedef uint32_t	Reg;
						namespace fADDSET { // Field Description
							constexpr Reg Lsb = 0;
							constexpr Reg FieldMask = 0x0000000F;
							};
						namespace fADDHLD { // Field Description
							constexpr Reg Lsb = 4;
							constexpr Reg FieldMask = 0x000000F0;
							};
						namespace fDATAST { // Field Description
							constexpr Reg Lsb = 8;
							constexpr Reg FieldMask = 0x0000FF00;
							};
						namespace fBUSTURN { // Field Description
							constexpr Reg Lsb = 16;
							constexpr Reg FieldMask = 0x000F0000;
							};
						namespace fCLKDIV { // Field Description
							constexpr Reg Lsb = 20;
							constexpr Reg FieldMask = 0x00F00000;
							};
						namespace fDATLAT { // Field Description
							constexpr Reg Lsb = 24;
							constexpr Reg FieldMask = 0x0F000000;
							};
						namespace fACCMOD { // Field Description
							constexpr Reg Lsb = 28;
							constexpr Reg FieldMask = 0x30000000;
							};
						};
					namespace rBCR2 { // Register description
						typedef uint32_t	Reg;
						namespace fMBKEN { // Field Description
							constexpr Reg Lsb = 0;
							constexpr Reg FieldMask = 0x00000001;
							};
						namespace fMUXEN { // Field Description
							constexpr Reg Lsb = 1;
							constexpr Reg FieldMask = 0x00000002;
							};
						namespace fMTYP { // Field Description
							constexpr Reg Lsb = 2;
							constexpr Reg FieldMask = 0x0000000C;
							};
						namespace fMWID { // Field Description
							constexpr Reg Lsb = 4;
							constexpr Reg FieldMask = 0x00000030;
							};
						namespace fFACCEN { // Field Description
							constexpr Reg Lsb = 6;
							constexpr Reg FieldMask = 0x00000040;
							};
						namespace fBURSTEN { // Field Description
							constexpr Reg Lsb = 8;
							constexpr Reg FieldMask = 0x00000100;
							};
						namespace fWAITPOL { // Field Description
							constexpr Reg Lsb = 9;
							constexpr Reg FieldMask = 0x00000200;
							};
						namespace fWAITCFG { // Field Description
							constexpr Reg Lsb = 11;
							constexpr Reg FieldMask = 0x00000800;
							};
						namespace fWREN { // Field Description
							constexpr Reg Lsb = 12;
							constexpr Reg FieldMask = 0x00001000;
							};
						namespace fWAITEN { // Field Description
							constexpr Reg Lsb = 13;
							constexpr Reg FieldMask = 0x00002000;
							};
						namespace fEXTMOD { // Field Description
							constexpr Reg Lsb = 14;
							constexpr Reg FieldMask = 0x00004000;
							};
						namespace fASYNCWAIT { // Field Description
							constexpr Reg Lsb = 15;
							constexpr Reg FieldMask = 0x00008000;
							};
						namespace fCPSIZE { // Field Description
							constexpr Reg Lsb = 16;
							constexpr Reg FieldMask = 0x00070000;
							};
						namespace fCBURSTRW { // Field Description
							constexpr Reg Lsb = 19;
							constexpr Reg FieldMask = 0x00080000;
							};
						namespace fCCLKEN { // Field Description
							constexpr Reg Lsb = 20;
							constexpr Reg FieldMask = 0x00100000;
							};
						namespace fWFDIS { // Field Description
							constexpr Reg Lsb = 21;
							constexpr Reg FieldMask = 0x00200000;
							};
						namespace fBMAP { // Field Description
							constexpr Reg Lsb = 24;
							constexpr Reg FieldMask = 0x03000000;
							};
						namespace fFMCEN { // Field Description
							constexpr Reg Lsb = 31;
							constexpr Reg FieldMask = 0x80000000;
							};
						};
					namespace rBTR2 { // Register description
						typedef uint32_t	Reg;
						namespace fADDSET { // Field Description
							constexpr Reg Lsb = 0;
							constexpr Reg FieldMask = 0x0000000F;
							};
						namespace fADDHLD { // Field Description
							constexpr Reg Lsb = 4;
							constexpr Reg FieldMask = 0x000000F0;
							};
						namespace fDATAST { // Field Description
							constexpr Reg Lsb = 8;
							constexpr Reg FieldMask = 0x0000FF00;
							};
						namespace fBUSTURN { // Field Description
							constexpr Reg Lsb = 16;
							constexpr Reg FieldMask = 0x000F0000;
							};
						namespace fCLKDIV { // Field Description
							constexpr Reg Lsb = 20;
							constexpr Reg FieldMask = 0x00F00000;
							};
						namespace fDATLAT { // Field Description
							constexpr Reg Lsb = 24;
							constexpr Reg FieldMask = 0x0F000000;
							};
						namespace fACCMOD { // Field Description
							constexpr Reg Lsb = 28;
							constexpr Reg FieldMask = 0x30000000;
							};
						};
					namespace rBCR3 { // Register description
						typedef uint32_t	Reg;
						namespace fMBKEN { // Field Description
							constexpr Reg Lsb = 0;
							constexpr Reg FieldMask = 0x00000001;
							};
						namespace fMUXEN { // Field Description
							constexpr Reg Lsb = 1;
							constexpr Reg FieldMask = 0x00000002;
							};
						namespace fMTYP { // Field Description
							constexpr Reg Lsb = 2;
							constexpr Reg FieldMask = 0x0000000C;
							};
						namespace fMWID { // Field Description
							constexpr Reg Lsb = 4;
							constexpr Reg FieldMask = 0x00000030;
							};
						namespace fFACCEN { // Field Description
							constexpr Reg Lsb = 6;
							constexpr Reg FieldMask = 0x00000040;
							};
						namespace fBURSTEN { // Field Description
							constexpr Reg Lsb = 8;
							constexpr Reg FieldMask = 0x00000100;
							};
						namespace fWAITPOL { // Field Description
							constexpr Reg Lsb = 9;
							constexpr Reg FieldMask = 0x00000200;
							};
						namespace fWAITCFG { // Field Description
							constexpr Reg Lsb = 11;
							constexpr Reg FieldMask = 0x00000800;
							};
						namespace fWREN { // Field Description
							constexpr Reg Lsb = 12;
							constexpr Reg FieldMask = 0x00001000;
							};
						namespace fWAITEN { // Field Description
							constexpr Reg Lsb = 13;
							constexpr Reg FieldMask = 0x00002000;
							};
						namespace fEXTMOD { // Field Description
							constexpr Reg Lsb = 14;
							constexpr Reg FieldMask = 0x00004000;
							};
						namespace fASYNCWAIT { // Field Description
							constexpr Reg Lsb = 15;
							constexpr Reg FieldMask = 0x00008000;
							};
						namespace fCPSIZE { // Field Description
							constexpr Reg Lsb = 16;
							constexpr Reg FieldMask = 0x00070000;
							};
						namespace fCBURSTRW { // Field Description
							constexpr Reg Lsb = 19;
							constexpr Reg FieldMask = 0x00080000;
							};
						namespace fCCLKEN { // Field Description
							constexpr Reg Lsb = 20;
							constexpr Reg FieldMask = 0x00100000;
							};
						namespace fWFDIS { // Field Description
							constexpr Reg Lsb = 21;
							constexpr Reg FieldMask = 0x00200000;
							};
						namespace fBMAP { // Field Description
							constexpr Reg Lsb = 24;
							constexpr Reg FieldMask = 0x03000000;
							};
						namespace fFMCEN { // Field Description
							constexpr Reg Lsb = 31;
							constexpr Reg FieldMask = 0x80000000;
							};
						};
					namespace rBTR3 { // Register description
						typedef uint32_t	Reg;
						namespace fADDSET { // Field Description
							constexpr Reg Lsb = 0;
							constexpr Reg FieldMask = 0x0000000F;
							};
						namespace fADDHLD { // Field Description
							constexpr Reg Lsb = 4;
							constexpr Reg FieldMask = 0x000000F0;
							};
						namespace fDATAST { // Field Description
							constexpr Reg Lsb = 8;
							constexpr Reg FieldMask = 0x0000FF00;
							};
						namespace fBUSTURN { // Field Description
							constexpr Reg Lsb = 16;
							constexpr Reg FieldMask = 0x000F0000;
							};
						namespace fCLKDIV { // Field Description
							constexpr Reg Lsb = 20;
							constexpr Reg FieldMask = 0x00F00000;
							};
						namespace fDATLAT { // Field Description
							constexpr Reg Lsb = 24;
							constexpr Reg FieldMask = 0x0F000000;
							};
						namespace fACCMOD { // Field Description
							constexpr Reg Lsb = 28;
							constexpr Reg FieldMask = 0x30000000;
							};
						};
					namespace rBCR4 { // Register description
						typedef uint32_t	Reg;
						namespace fMBKEN { // Field Description
							constexpr Reg Lsb = 0;
							constexpr Reg FieldMask = 0x00000001;
							};
						namespace fMUXEN { // Field Description
							constexpr Reg Lsb = 1;
							constexpr Reg FieldMask = 0x00000002;
							};
						namespace fMTYP { // Field Description
							constexpr Reg Lsb = 2;
							constexpr Reg FieldMask = 0x0000000C;
							};
						namespace fMWID { // Field Description
							constexpr Reg Lsb = 4;
							constexpr Reg FieldMask = 0x00000030;
							};
						namespace fFACCEN { // Field Description
							constexpr Reg Lsb = 6;
							constexpr Reg FieldMask = 0x00000040;
							};
						namespace fBURSTEN { // Field Description
							constexpr Reg Lsb = 8;
							constexpr Reg FieldMask = 0x00000100;
							};
						namespace fWAITPOL { // Field Description
							constexpr Reg Lsb = 9;
							constexpr Reg FieldMask = 0x00000200;
							};
						namespace fWAITCFG { // Field Description
							constexpr Reg Lsb = 11;
							constexpr Reg FieldMask = 0x00000800;
							};
						namespace fWREN { // Field Description
							constexpr Reg Lsb = 12;
							constexpr Reg FieldMask = 0x00001000;
							};
						namespace fWAITEN { // Field Description
							constexpr Reg Lsb = 13;
							constexpr Reg FieldMask = 0x00002000;
							};
						namespace fEXTMOD { // Field Description
							constexpr Reg Lsb = 14;
							constexpr Reg FieldMask = 0x00004000;
							};
						namespace fASYNCWAIT { // Field Description
							constexpr Reg Lsb = 15;
							constexpr Reg FieldMask = 0x00008000;
							};
						namespace fCPSIZE { // Field Description
							constexpr Reg Lsb = 16;
							constexpr Reg FieldMask = 0x00070000;
							};
						namespace fCBURSTRW { // Field Description
							constexpr Reg Lsb = 19;
							constexpr Reg FieldMask = 0x00080000;
							};
						namespace fCCLKEN { // Field Description
							constexpr Reg Lsb = 20;
							constexpr Reg FieldMask = 0x00100000;
							};
						namespace fWFDIS { // Field Description
							constexpr Reg Lsb = 21;
							constexpr Reg FieldMask = 0x00200000;
							};
						namespace fBMAP { // Field Description
							constexpr Reg Lsb = 24;
							constexpr Reg FieldMask = 0x03000000;
							};
						namespace fFMCEN { // Field Description
							constexpr Reg Lsb = 31;
							constexpr Reg FieldMask = 0x80000000;
							};
						};
					namespace rBTR4 { // Register description
						typedef uint32_t	Reg;
						namespace fADDSET { // Field Description
							constexpr Reg Lsb = 0;
							constexpr Reg FieldMask = 0x0000000F;
							};
						namespace fADDHLD { // Field Description
							constexpr Reg Lsb = 4;
							constexpr Reg FieldMask = 0x000000F0;
							};
						namespace fDATAST { // Field Description
							constexpr Reg Lsb = 8;
							constexpr Reg FieldMask = 0x0000FF00;
							};
						namespace fBUSTURN { // Field Description
							constexpr Reg Lsb = 16;
							constexpr Reg FieldMask = 0x000F0000;
							};
						namespace fCLKDIV { // Field Description
							constexpr Reg Lsb = 20;
							constexpr Reg FieldMask = 0x00F00000;
							};
						namespace fDATLAT { // Field Description
							constexpr Reg Lsb = 24;
							constexpr Reg FieldMask = 0x0F000000;
							};
						namespace fACCMOD { // Field Description
							constexpr Reg Lsb = 28;
							constexpr Reg FieldMask = 0x30000000;
							};
						};
					namespace rPCR { // Register description
						typedef uint32_t	Reg;
						namespace fPWAITEN { // Field Description
							constexpr Reg Lsb = 1;
							constexpr Reg FieldMask = 0x00000002;
							};
						namespace fPBKEN { // Field Description
							constexpr Reg Lsb = 2;
							constexpr Reg FieldMask = 0x00000004;
							};
						namespace fPWID { // Field Description
							constexpr Reg Lsb = 4;
							constexpr Reg FieldMask = 0x00000030;
							};
						namespace fECCEN { // Field Description
							constexpr Reg Lsb = 6;
							constexpr Reg FieldMask = 0x00000040;
							};
						namespace fTCLR { // Field Description
							constexpr Reg Lsb = 9;
							constexpr Reg FieldMask = 0x00001E00;
							};
						namespace fTAR { // Field Description
							constexpr Reg Lsb = 13;
							constexpr Reg FieldMask = 0x0001E000;
							};
						namespace fECCPS { // Field Description
							constexpr Reg Lsb = 17;
							constexpr Reg FieldMask = 0x000E0000;
							};
						};
					namespace rSR { // Register description
						typedef uint32_t	Reg;
						namespace fIRS { // Field Description
							constexpr Reg Lsb = 0;
							constexpr Reg FieldMask = 0x00000001;
							};
						namespace fILS { // Field Description
							constexpr Reg Lsb = 1;
							constexpr Reg FieldMask = 0x00000002;
							};
						namespace fIFS { // Field Description
							constexpr Reg Lsb = 2;
							constexpr Reg FieldMask = 0x00000004;
							};
						namespace fIREN { // Field Description
							constexpr Reg Lsb = 3;
							constexpr Reg FieldMask = 0x00000008;
							};
						namespace fILEN { // Field Description
							constexpr Reg Lsb = 4;
							constexpr Reg FieldMask = 0x00000010;
							};
						namespace fIFEN { // Field Description
							constexpr Reg Lsb = 5;
							constexpr Reg FieldMask = 0x00000020;
							};
						namespace fFEMPT { // Field Description
							constexpr Reg Lsb = 6;
							constexpr Reg FieldMask = 0x00000040;
							};
						};
					namespace rPMEM { // Register description
						typedef uint32_t	Reg;
						namespace fMEMSET { // Field Description
							constexpr Reg Lsb = 0;
							constexpr Reg FieldMask = 0x000000FF;
							};
						namespace fMEMWAIT { // Field Description
							constexpr Reg Lsb = 8;
							constexpr Reg FieldMask = 0x0000FF00;
							};
						namespace fMEMHOLD { // Field Description
							constexpr Reg Lsb = 16;
							constexpr Reg FieldMask = 0x00FF0000;
							};
						namespace fMEMHIZ { // Field Description
							constexpr Reg Lsb = 24;
							constexpr Reg FieldMask = 0xFF000000;
							};
						};
					namespace rPATT { // Register description
						typedef uint32_t	Reg;
						namespace fATTSET { // Field Description
							constexpr Reg Lsb = 0;
							constexpr Reg FieldMask = 0x000000FF;
							};
						namespace fATTWAIT { // Field Description
							constexpr Reg Lsb = 8;
							constexpr Reg FieldMask = 0x0000FF00;
							};
						namespace fATTHOLD { // Field Description
							constexpr Reg Lsb = 16;
							constexpr Reg FieldMask = 0x00FF0000;
							};
						namespace fATTHIZ { // Field Description
							constexpr Reg Lsb = 24;
							constexpr Reg FieldMask = 0xFF000000;
							};
						};
					namespace rECCR { // Register description
						typedef uint32_t	Reg;
						namespace fECC { // Field Description
							constexpr Reg Lsb = 0;
							constexpr Reg FieldMask = 0xFFFFFFFF;
							};
						};
					namespace rBWTR1 { // Register description
						typedef uint32_t	Reg;
						namespace fADDSET { // Field Description
							constexpr Reg Lsb = 0;
							constexpr Reg FieldMask = 0x0000000F;
							};
						namespace fADDHLD { // Field Description
							constexpr Reg Lsb = 4;
							constexpr Reg FieldMask = 0x000000F0;
							};
						namespace fDATAST { // Field Description
							constexpr Reg Lsb = 8;
							constexpr Reg FieldMask = 0x0000FF00;
							};
						namespace fBUSTURN { // Field Description
							constexpr Reg Lsb = 16;
							constexpr Reg FieldMask = 0x000F0000;
							};
						namespace fACCMOD { // Field Description
							constexpr Reg Lsb = 28;
							constexpr Reg FieldMask = 0x30000000;
							};
						};
					namespace rBWTR2 { // Register description
						typedef uint32_t	Reg;
						namespace fADDSET { // Field Description
							constexpr Reg Lsb = 0;
							constexpr Reg FieldMask = 0x0000000F;
							};
						namespace fADDHLD { // Field Description
							constexpr Reg Lsb = 4;
							constexpr Reg FieldMask = 0x000000F0;
							};
						namespace fDATAST { // Field Description
							constexpr Reg Lsb = 8;
							constexpr Reg FieldMask = 0x0000FF00;
							};
						namespace fBUSTURN { // Field Description
							constexpr Reg Lsb = 16;
							constexpr Reg FieldMask = 0x000F0000;
							};
						namespace fACCMOD { // Field Description
							constexpr Reg Lsb = 28;
							constexpr Reg FieldMask = 0x30000000;
							};
						};
					namespace rBWTR3 { // Register description
						typedef uint32_t	Reg;
						namespace fADDSET { // Field Description
							constexpr Reg Lsb = 0;
							constexpr Reg FieldMask = 0x0000000F;
							};
						namespace fADDHLD { // Field Description
							constexpr Reg Lsb = 4;
							constexpr Reg FieldMask = 0x000000F0;
							};
						namespace fDATAST { // Field Description
							constexpr Reg Lsb = 8;
							constexpr Reg FieldMask = 0x0000FF00;
							};
						namespace fBUSTURN { // Field Description
							constexpr Reg Lsb = 16;
							constexpr Reg FieldMask = 0x000F0000;
							};
						namespace fACCMOD { // Field Description
							constexpr Reg Lsb = 28;
							constexpr Reg FieldMask = 0x30000000;
							};
						};
					namespace rBWTR4 { // Register description
						typedef uint32_t	Reg;
						namespace fADDSET { // Field Description
							constexpr Reg Lsb = 0;
							constexpr Reg FieldMask = 0x0000000F;
							};
						namespace fADDHLD { // Field Description
							constexpr Reg Lsb = 4;
							constexpr Reg FieldMask = 0x000000F0;
							};
						namespace fDATAST { // Field Description
							constexpr Reg Lsb = 8;
							constexpr Reg FieldMask = 0x0000FF00;
							};
						namespace fBUSTURN { // Field Description
							constexpr Reg Lsb = 16;
							constexpr Reg FieldMask = 0x000F0000;
							};
						namespace fACCMOD { // Field Description
							constexpr Reg Lsb = 28;
							constexpr Reg FieldMask = 0x30000000;
							};
						};
					namespace rSDCR1 { // Register description
						typedef uint32_t	Reg;
						namespace fNC { // Field Description
							constexpr Reg Lsb = 0;
							constexpr Reg FieldMask = 0x00000003;
							};
						namespace fNR { // Field Description
							constexpr Reg Lsb = 2;
							constexpr Reg FieldMask = 0x0000000C;
							};
						namespace fMWID { // Field Description
							constexpr Reg Lsb = 4;
							constexpr Reg FieldMask = 0x00000030;
							};
						namespace fNB { // Field Description
							constexpr Reg Lsb = 6;
							constexpr Reg FieldMask = 0x00000040;
							};
						namespace fCAS { // Field Description
							constexpr Reg Lsb = 7;
							constexpr Reg FieldMask = 0x00000180;
							};
						namespace fWP { // Field Description
							constexpr Reg Lsb = 9;
							constexpr Reg FieldMask = 0x00000200;
							};
						namespace fSDCLK { // Field Description
							constexpr Reg Lsb = 10;
							constexpr Reg FieldMask = 0x00000C00;
							};
						namespace fRBURST { // Field Description
							constexpr Reg Lsb = 12;
							constexpr Reg FieldMask = 0x00001000;
							};
						namespace fRPIPE { // Field Description
							constexpr Reg Lsb = 13;
							constexpr Reg FieldMask = 0x00006000;
							};
						};
					namespace rSDCR2 { // Register description
						typedef uint32_t	Reg;
						namespace fNC { // Field Description
							constexpr Reg Lsb = 0;
							constexpr Reg FieldMask = 0x00000003;
							};
						namespace fNR { // Field Description
							constexpr Reg Lsb = 2;
							constexpr Reg FieldMask = 0x0000000C;
							};
						namespace fMWID { // Field Description
							constexpr Reg Lsb = 4;
							constexpr Reg FieldMask = 0x00000030;
							};
						namespace fNB { // Field Description
							constexpr Reg Lsb = 6;
							constexpr Reg FieldMask = 0x00000040;
							};
						namespace fCAS { // Field Description
							constexpr Reg Lsb = 7;
							constexpr Reg FieldMask = 0x00000180;
							};
						namespace fWP { // Field Description
							constexpr Reg Lsb = 9;
							constexpr Reg FieldMask = 0x00000200;
							};
						namespace fSDCLK { // Field Description
							constexpr Reg Lsb = 10;
							constexpr Reg FieldMask = 0x00000C00;
							};
						namespace fRBURST { // Field Description
							constexpr Reg Lsb = 12;
							constexpr Reg FieldMask = 0x00001000;
							};
						namespace fRPIPE { // Field Description
							constexpr Reg Lsb = 13;
							constexpr Reg FieldMask = 0x00006000;
							};
						};
					namespace rSDTR1 { // Register description
						typedef uint32_t	Reg;
						namespace fTMRD { // Field Description
							constexpr Reg Lsb = 0;
							constexpr Reg FieldMask = 0x0000000F;
							};
						namespace fTXSR { // Field Description
							constexpr Reg Lsb = 4;
							constexpr Reg FieldMask = 0x000000F0;
							};
						namespace fTRAS { // Field Description
							constexpr Reg Lsb = 8;
							constexpr Reg FieldMask = 0x00000F00;
							};
						namespace fTRC { // Field Description
							constexpr Reg Lsb = 12;
							constexpr Reg FieldMask = 0x0000F000;
							};
						namespace fTWR { // Field Description
							constexpr Reg Lsb = 16;
							constexpr Reg FieldMask = 0x000F0000;
							};
						namespace fTRP { // Field Description
							constexpr Reg Lsb = 20;
							constexpr Reg FieldMask = 0x00F00000;
							};
						namespace fTRCD { // Field Description
							constexpr Reg Lsb = 24;
							constexpr Reg FieldMask = 0x0F000000;
							};
						};
					namespace rSDTR2 { // Register description
						typedef uint32_t	Reg;
						namespace fTMRD { // Field Description
							constexpr Reg Lsb = 0;
							constexpr Reg FieldMask = 0x0000000F;
							};
						namespace fTXSR { // Field Description
							constexpr Reg Lsb = 4;
							constexpr Reg FieldMask = 0x000000F0;
							};
						namespace fTRAS { // Field Description
							constexpr Reg Lsb = 8;
							constexpr Reg FieldMask = 0x00000F00;
							};
						namespace fTRC { // Field Description
							constexpr Reg Lsb = 12;
							constexpr Reg FieldMask = 0x0000F000;
							};
						namespace fTWR { // Field Description
							constexpr Reg Lsb = 16;
							constexpr Reg FieldMask = 0x000F0000;
							};
						namespace fTRP { // Field Description
							constexpr Reg Lsb = 20;
							constexpr Reg FieldMask = 0x00F00000;
							};
						namespace fTRCD { // Field Description
							constexpr Reg Lsb = 24;
							constexpr Reg FieldMask = 0x0F000000;
							};
						};
					namespace rSDCMR { // Register description
						typedef uint32_t	Reg;
						namespace fMODE { // Field Description
							constexpr Reg Lsb = 0;
							constexpr Reg FieldMask = 0x00000007;
							};
						namespace fCTB2 { // Field Description
							constexpr Reg Lsb = 3;
							constexpr Reg FieldMask = 0x00000008;
							};
						namespace fCTB1 { // Field Description
							constexpr Reg Lsb = 4;
							constexpr Reg FieldMask = 0x00000010;
							};
						namespace fNRFS { // Field Description
							constexpr Reg Lsb = 5;
							constexpr Reg FieldMask = 0x000001E0;
							};
						namespace fMRD { // Field Description
							constexpr Reg Lsb = 9;
							constexpr Reg FieldMask = 0x007FFE00;
							};
						};
					namespace rSDRTR { // Register description
						typedef uint32_t	Reg;
						namespace fCRE { // Field Description
							constexpr Reg Lsb = 0;
							constexpr Reg FieldMask = 0x00000001;
							};
						namespace fCOUNT { // Field Description
							constexpr Reg Lsb = 1;
							constexpr Reg FieldMask = 0x00003FFE;
							};
						namespace fREIE { // Field Description
							constexpr Reg Lsb = 14;
							constexpr Reg FieldMask = 0x00004000;
							};
						};
					namespace rSDSR { // Register description
						typedef uint32_t	Reg;
						namespace fRE { // Field Description
							constexpr Reg Lsb = 0;
							constexpr Reg FieldMask = 0x00000001;
							};
						namespace fMODES1 { // Field Description
							constexpr Reg Lsb = 1;
							constexpr Reg FieldMask = 0x00000006;
							};
						namespace fMODES2 { // Field Description
							constexpr Reg Lsb = 3;
							constexpr Reg FieldMask = 0x00000018;
							};
						};
					}
				}
			}
		}
	}
#endif
