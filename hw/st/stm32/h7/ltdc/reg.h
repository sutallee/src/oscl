/*
   Copyright (C) 2024 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_hw_st_stm32_h7_ltdc_regh_
#define _oscl_hw_st_stm32_h7_ltdc_regh_
#include <stdint.h>
namespace Oscl { // Namespace description
	namespace St { // Namespace description
		namespace Stm32 { // Namespace description
			namespace H7 { // Namespace description
				namespace pLTDC { // Namespace description
					namespace rSSCR { // Register description
						typedef uint32_t	Reg;
						constexpr Reg	valueAfterReset = 0x00000000;
						namespace fVSH { // Field Description
							constexpr Reg Lsb = 0;
							constexpr Reg FieldMask = 0x000007FF;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							constexpr Reg Value_minValue = 0x0;
							constexpr Reg ValueMask_minValue = 0x00000000;
							constexpr Reg Value_maxValue = 0x7FF;
							constexpr Reg ValueMask_maxValue = 0x000007FF;
							};
						namespace fHSW { // Field Description
							constexpr Reg Lsb = 16;
							constexpr Reg FieldMask = 0x0FFF0000;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							constexpr Reg Value_minValue = 0x0;
							constexpr Reg ValueMask_minValue = 0x00000000;
							constexpr Reg Value_maxValue = 0xFFF;
							constexpr Reg ValueMask_maxValue = 0x0FFF0000;
							};
						};
					namespace rBPCR { // Register description
						typedef uint32_t	Reg;
						constexpr Reg	valueAfterReset = 0x00000000;
						namespace fAVBP { // Field Description
							constexpr Reg Lsb = 0;
							constexpr Reg FieldMask = 0x000007FF;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							constexpr Reg Value_minValue = 0x0;
							constexpr Reg ValueMask_minValue = 0x00000000;
							constexpr Reg Value_maxValue = 0x7FF;
							constexpr Reg ValueMask_maxValue = 0x000007FF;
							};
						namespace fAHBP { // Field Description
							constexpr Reg Lsb = 16;
							constexpr Reg FieldMask = 0x0FFF0000;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							constexpr Reg Value_minValue = 0x0;
							constexpr Reg ValueMask_minValue = 0x00000000;
							constexpr Reg Value_maxValue = 0xFFF;
							constexpr Reg ValueMask_maxValue = 0x0FFF0000;
							};
						};
					namespace rAWCR { // Register description
						typedef uint32_t	Reg;
						constexpr Reg	valueAfterReset = 0x00000000;
						namespace fAAH { // Field Description
							constexpr Reg Lsb = 0;
							constexpr Reg FieldMask = 0x000007FF;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							constexpr Reg Value_minValue = 0x0;
							constexpr Reg ValueMask_minValue = 0x00000000;
							constexpr Reg Value_maxValue = 0x7FF;
							constexpr Reg ValueMask_maxValue = 0x000007FF;
							};
						namespace fAAW { // Field Description
							constexpr Reg Lsb = 16;
							constexpr Reg FieldMask = 0x0FFF0000;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							constexpr Reg Value_minValue = 0x0;
							constexpr Reg ValueMask_minValue = 0x00000000;
							constexpr Reg Value_maxValue = 0xFFF;
							constexpr Reg ValueMask_maxValue = 0x0FFF0000;
							};
						};
					namespace rTWCR { // Register description
						typedef uint32_t	Reg;
						constexpr Reg	valueAfterReset = 0x00000000;
						namespace fTOTALH { // Field Description
							constexpr Reg Lsb = 0;
							constexpr Reg FieldMask = 0x000007FF;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							constexpr Reg Value_minValue = 0x0;
							constexpr Reg ValueMask_minValue = 0x00000000;
							constexpr Reg Value_maxValue = 0x7FF;
							constexpr Reg ValueMask_maxValue = 0x000007FF;
							};
						namespace fTOTALW { // Field Description
							constexpr Reg Lsb = 16;
							constexpr Reg FieldMask = 0x0FFF0000;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							constexpr Reg Value_minValue = 0x0;
							constexpr Reg ValueMask_minValue = 0x00000000;
							constexpr Reg Value_maxValue = 0xFFF;
							constexpr Reg ValueMask_maxValue = 0x0FFF0000;
							};
						};
					namespace rGCR { // Register description
						typedef uint32_t	Reg;
						constexpr Reg	valueAfterReset = 0x00002220;
						namespace fLTDCEN { // Field Description
							constexpr Reg Lsb = 0;
							constexpr Reg FieldMask = 0x00000001;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							constexpr Reg Value_disable = 0x0;
							constexpr Reg ValueMask_disable = 0x00000000;
							constexpr Reg Value_disabled = 0x0;
							constexpr Reg ValueMask_disabled = 0x00000000;
							constexpr Reg Value_enable = 0x1;
							constexpr Reg ValueMask_enable = 0x00000001;
							constexpr Reg Value_enabled = 0x1;
							constexpr Reg ValueMask_enabled = 0x00000001;
							};
						namespace fDBW { // Field Description
							constexpr Reg Lsb = 4;
							constexpr Reg FieldMask = 0x00000070;
							constexpr Reg Value_valueAfterReset = 0x2;
							constexpr Reg ValueMask_valueAfterReset = 0x00000020;
							constexpr Reg Value_disable = 0x0;
							constexpr Reg ValueMask_disable = 0x00000000;
							constexpr Reg Value_minValue = 0x0;
							constexpr Reg ValueMask_minValue = 0x00000000;
							constexpr Reg Value_maxValue = 0x7;
							constexpr Reg ValueMask_maxValue = 0x00000070;
							};
						namespace fDGW { // Field Description
							constexpr Reg Lsb = 8;
							constexpr Reg FieldMask = 0x00000700;
							constexpr Reg Value_valueAfterReset = 0x2;
							constexpr Reg ValueMask_valueAfterReset = 0x00000200;
							constexpr Reg Value_disable = 0x0;
							constexpr Reg ValueMask_disable = 0x00000000;
							constexpr Reg Value_minValue = 0x0;
							constexpr Reg ValueMask_minValue = 0x00000000;
							constexpr Reg Value_maxValue = 0x7;
							constexpr Reg ValueMask_maxValue = 0x00000700;
							};
						namespace fDRW { // Field Description
							constexpr Reg Lsb = 12;
							constexpr Reg FieldMask = 0x00007000;
							constexpr Reg Value_valueAfterReset = 0x2;
							constexpr Reg ValueMask_valueAfterReset = 0x00002000;
							constexpr Reg Value_disable = 0x0;
							constexpr Reg ValueMask_disable = 0x00000000;
							constexpr Reg Value_minValue = 0x0;
							constexpr Reg ValueMask_minValue = 0x00000000;
							constexpr Reg Value_maxValue = 0x7;
							constexpr Reg ValueMask_maxValue = 0x00007000;
							};
						namespace fDEN { // Field Description
							constexpr Reg Lsb = 16;
							constexpr Reg FieldMask = 0x00010000;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							constexpr Reg Value_disable = 0x0;
							constexpr Reg ValueMask_disable = 0x00000000;
							constexpr Reg Value_disabled = 0x0;
							constexpr Reg ValueMask_disabled = 0x00000000;
							constexpr Reg Value_enable = 0x1;
							constexpr Reg ValueMask_enable = 0x00010000;
							constexpr Reg Value_enabled = 0x1;
							constexpr Reg ValueMask_enabled = 0x00010000;
							};
						namespace fPCPOL { // Field Description
							constexpr Reg Lsb = 28;
							constexpr Reg FieldMask = 0x10000000;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							constexpr Reg Value_activeLOW = 0x0;
							constexpr Reg ValueMask_activeLOW = 0x00000000;
							constexpr Reg Value_activeHIGH = 0x1;
							constexpr Reg ValueMask_activeHIGH = 0x10000000;
							};
						namespace fDEPOL { // Field Description
							constexpr Reg Lsb = 29;
							constexpr Reg FieldMask = 0x20000000;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							constexpr Reg Value_activeLOW = 0x0;
							constexpr Reg ValueMask_activeLOW = 0x00000000;
							constexpr Reg Value_activeHIGH = 0x1;
							constexpr Reg ValueMask_activeHIGH = 0x20000000;
							};
						namespace fVSPOL { // Field Description
							constexpr Reg Lsb = 30;
							constexpr Reg FieldMask = 0x40000000;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							constexpr Reg Value_activeLOW = 0x0;
							constexpr Reg ValueMask_activeLOW = 0x00000000;
							constexpr Reg Value_activeHIGH = 0x1;
							constexpr Reg ValueMask_activeHIGH = 0x40000000;
							};
						namespace fHSPOL { // Field Description
							constexpr Reg Lsb = 31;
							constexpr Reg FieldMask = 0x80000000;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							constexpr Reg Value_activeLOW = 0x0;
							constexpr Reg ValueMask_activeLOW = 0x00000000;
							constexpr Reg Value_activeHIGH = 0x1;
							constexpr Reg ValueMask_activeHIGH = 0x80000000;
							};
						};
					namespace rSRCR { // Register description
						typedef uint32_t	Reg;
						constexpr Reg	valueAfterReset = 0x00000000;
						namespace fIMR { // Field Description
							constexpr Reg Lsb = 0;
							constexpr Reg FieldMask = 0x00000001;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							constexpr Reg Value_noEffect = 0x0;
							constexpr Reg ValueMask_noEffect = 0x00000000;
							constexpr Reg Value_reloadImmediately = 0x1;
							constexpr Reg ValueMask_reloadImmediately = 0x00000001;
							};
						namespace fVBR { // Field Description
							constexpr Reg Lsb = 1;
							constexpr Reg FieldMask = 0x00000002;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							constexpr Reg Value_noEffect = 0x0;
							constexpr Reg ValueMask_noEffect = 0x00000000;
							constexpr Reg Value_reloadDuringBlanking = 0x1;
							constexpr Reg ValueMask_reloadDuringBlanking = 0x00000002;
							};
						};
					namespace rBCCR { // Register description
						typedef uint32_t	Reg;
						constexpr Reg	valueAfterReset = 0x00000000;
						namespace fBCBLUE { // Field Description
							constexpr Reg Lsb = 0;
							constexpr Reg FieldMask = 0x000000FF;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							constexpr Reg Value_minValue = 0x0;
							constexpr Reg ValueMask_minValue = 0x00000000;
							constexpr Reg Value_maxValue = 0xFF;
							constexpr Reg ValueMask_maxValue = 0x000000FF;
							};
						namespace fBCGREEN { // Field Description
							constexpr Reg Lsb = 8;
							constexpr Reg FieldMask = 0x0000FF00;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							constexpr Reg Value_minValue = 0x0;
							constexpr Reg ValueMask_minValue = 0x00000000;
							constexpr Reg Value_maxValue = 0xFF;
							constexpr Reg ValueMask_maxValue = 0x0000FF00;
							};
						namespace fBCRED { // Field Description
							constexpr Reg Lsb = 16;
							constexpr Reg FieldMask = 0x00FF0000;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							constexpr Reg Value_minValue = 0x0;
							constexpr Reg ValueMask_minValue = 0x00000000;
							constexpr Reg Value_maxValue = 0xFF;
							constexpr Reg ValueMask_maxValue = 0x00FF0000;
							};
						};
					namespace rIER { // Register description
						typedef uint32_t	Reg;
						constexpr Reg	valueAfterReset = 0x00000000;
						namespace fLIE { // Field Description
							constexpr Reg Lsb = 0;
							constexpr Reg FieldMask = 0x00000001;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							constexpr Reg Value_interruptDisabled = 0x0;
							constexpr Reg ValueMask_interruptDisabled = 0x00000000;
							constexpr Reg Value_disableInterrupt = 0x0;
							constexpr Reg ValueMask_disableInterrupt = 0x00000000;
							constexpr Reg Value_interruptEnabled = 0x1;
							constexpr Reg ValueMask_interruptEnabled = 0x00000001;
							constexpr Reg Value_enableInterrupt = 0x1;
							constexpr Reg ValueMask_enableInterrupt = 0x00000001;
							};
						namespace fFUIE { // Field Description
							constexpr Reg Lsb = 1;
							constexpr Reg FieldMask = 0x00000002;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							constexpr Reg Value_interruptDisabled = 0x0;
							constexpr Reg ValueMask_interruptDisabled = 0x00000000;
							constexpr Reg Value_disableInterrupt = 0x0;
							constexpr Reg ValueMask_disableInterrupt = 0x00000000;
							constexpr Reg Value_interruptEnabled = 0x1;
							constexpr Reg ValueMask_interruptEnabled = 0x00000002;
							constexpr Reg Value_enableInterrupt = 0x1;
							constexpr Reg ValueMask_enableInterrupt = 0x00000002;
							};
						namespace fTERRIE { // Field Description
							constexpr Reg Lsb = 2;
							constexpr Reg FieldMask = 0x00000004;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							constexpr Reg Value_interruptDisabled = 0x0;
							constexpr Reg ValueMask_interruptDisabled = 0x00000000;
							constexpr Reg Value_disableInterrupt = 0x0;
							constexpr Reg ValueMask_disableInterrupt = 0x00000000;
							constexpr Reg Value_interruptEnabled = 0x1;
							constexpr Reg ValueMask_interruptEnabled = 0x00000004;
							constexpr Reg Value_enableInterrupt = 0x1;
							constexpr Reg ValueMask_enableInterrupt = 0x00000004;
							};
						namespace fRRIE { // Field Description
							constexpr Reg Lsb = 3;
							constexpr Reg FieldMask = 0x00000008;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							constexpr Reg Value_interruptDisabled = 0x0;
							constexpr Reg ValueMask_interruptDisabled = 0x00000000;
							constexpr Reg Value_disableInterrupt = 0x0;
							constexpr Reg ValueMask_disableInterrupt = 0x00000000;
							constexpr Reg Value_interruptEnabled = 0x1;
							constexpr Reg ValueMask_interruptEnabled = 0x00000008;
							constexpr Reg Value_enableInterrupt = 0x1;
							constexpr Reg ValueMask_enableInterrupt = 0x00000008;
							};
						};
					namespace rISR { // Register description
						typedef uint32_t	Reg;
						constexpr Reg	valueAfterReset = 0x00000000;
						namespace fLIF { // Field Description
							constexpr Reg Lsb = 0;
							constexpr Reg FieldMask = 0x00000001;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							constexpr Reg Value_interruptNotPending = 0x0;
							constexpr Reg ValueMask_interruptNotPending = 0x00000000;
							constexpr Reg Value_interruptPending = 0x1;
							constexpr Reg ValueMask_interruptPending = 0x00000001;
							};
						namespace fFUIF { // Field Description
							constexpr Reg Lsb = 1;
							constexpr Reg FieldMask = 0x00000002;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							constexpr Reg Value_interruptNotPending = 0x0;
							constexpr Reg ValueMask_interruptNotPending = 0x00000000;
							constexpr Reg Value_interruptPending = 0x1;
							constexpr Reg ValueMask_interruptPending = 0x00000002;
							};
						namespace fTERRIF { // Field Description
							constexpr Reg Lsb = 2;
							constexpr Reg FieldMask = 0x00000004;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							constexpr Reg Value_interruptNotPending = 0x0;
							constexpr Reg ValueMask_interruptNotPending = 0x00000000;
							constexpr Reg Value_interruptPending = 0x1;
							constexpr Reg ValueMask_interruptPending = 0x00000004;
							};
						namespace fRRIF { // Field Description
							constexpr Reg Lsb = 3;
							constexpr Reg FieldMask = 0x00000008;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							constexpr Reg Value_interruptNotPending = 0x0;
							constexpr Reg ValueMask_interruptNotPending = 0x00000000;
							constexpr Reg Value_interruptPending = 0x1;
							constexpr Reg ValueMask_interruptPending = 0x00000008;
							};
						};
					namespace rICR { // Register description
						typedef uint32_t	Reg;
						constexpr Reg	valueAfterReset = 0x00000000;
						namespace fCLIF { // Field Description
							constexpr Reg Lsb = 0;
							constexpr Reg FieldMask = 0x00000001;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							constexpr Reg Value_notActive = 0x0;
							constexpr Reg ValueMask_notActive = 0x00000000;
							constexpr Reg Value_active = 0x1;
							constexpr Reg ValueMask_active = 0x00000001;
							constexpr Reg Value_clearInterrupt = 0x1;
							constexpr Reg ValueMask_clearInterrupt = 0x00000001;
							constexpr Reg Value_noEffect = 0x0;
							constexpr Reg ValueMask_noEffect = 0x00000000;
							};
						namespace fCFUIF { // Field Description
							constexpr Reg Lsb = 1;
							constexpr Reg FieldMask = 0x00000002;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							constexpr Reg Value_notActive = 0x0;
							constexpr Reg ValueMask_notActive = 0x00000000;
							constexpr Reg Value_active = 0x1;
							constexpr Reg ValueMask_active = 0x00000002;
							constexpr Reg Value_clearInterrupt = 0x1;
							constexpr Reg ValueMask_clearInterrupt = 0x00000002;
							constexpr Reg Value_noEffect = 0x0;
							constexpr Reg ValueMask_noEffect = 0x00000000;
							};
						namespace fCTERRIF { // Field Description
							constexpr Reg Lsb = 2;
							constexpr Reg FieldMask = 0x00000004;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							constexpr Reg Value_notActive = 0x0;
							constexpr Reg ValueMask_notActive = 0x00000000;
							constexpr Reg Value_active = 0x1;
							constexpr Reg ValueMask_active = 0x00000004;
							constexpr Reg Value_clearInterrupt = 0x1;
							constexpr Reg ValueMask_clearInterrupt = 0x00000004;
							constexpr Reg Value_noEffect = 0x0;
							constexpr Reg ValueMask_noEffect = 0x00000000;
							};
						namespace fCRRIF { // Field Description
							constexpr Reg Lsb = 3;
							constexpr Reg FieldMask = 0x00000008;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							constexpr Reg Value_notActive = 0x0;
							constexpr Reg ValueMask_notActive = 0x00000000;
							constexpr Reg Value_active = 0x1;
							constexpr Reg ValueMask_active = 0x00000008;
							constexpr Reg Value_clearInterrupt = 0x1;
							constexpr Reg ValueMask_clearInterrupt = 0x00000008;
							constexpr Reg Value_noEffect = 0x0;
							constexpr Reg ValueMask_noEffect = 0x00000000;
							};
						};
					namespace rLIPCR { // Register description
						typedef uint32_t	Reg;
						constexpr Reg	valueAfterReset = 0x00000000;
						namespace fLIPOS { // Field Description
							constexpr Reg Lsb = 0;
							constexpr Reg FieldMask = 0x000007FF;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							constexpr Reg Value_minValue = 0x0;
							constexpr Reg ValueMask_minValue = 0x00000000;
							constexpr Reg Value_maxValue = 0x7FF;
							constexpr Reg ValueMask_maxValue = 0x000007FF;
							};
						};
					namespace rCPSR { // Register description
						typedef uint32_t	Reg;
						constexpr Reg	valueAfterReset = 0x00000000;
						namespace fCYPOS { // Field Description
							constexpr Reg Lsb = 0;
							constexpr Reg FieldMask = 0x0000FFFF;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							constexpr Reg Value_minValue = 0x0;
							constexpr Reg ValueMask_minValue = 0x00000000;
							constexpr Reg Value_maxValue = 0xFFFF;
							constexpr Reg ValueMask_maxValue = 0x0000FFFF;
							};
						namespace fCXPOS { // Field Description
							constexpr Reg Lsb = 16;
							constexpr Reg FieldMask = 0xFFFF0000;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							constexpr Reg Value_minValue = 0x0;
							constexpr Reg ValueMask_minValue = 0x00000000;
							constexpr Reg Value_maxValue = 0xFFFF;
							constexpr Reg ValueMask_maxValue = 0xFFFF0000;
							};
						};
					namespace rCDSR { // Register description
						typedef uint32_t	Reg;
						constexpr Reg	valueAfterReset = 0x0000000F;
						constexpr Reg	activeLOW = 0x00000000;
						constexpr Reg	activeHIGH = 0x00000001;
						namespace fVDES { // Field Description
							constexpr Reg Lsb = 0;
							constexpr Reg FieldMask = 0x00000001;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							constexpr Reg Value_activeLOW = 0x0;
							constexpr Reg ValueMask_activeLOW = 0x00000000;
							constexpr Reg Value_activeHIGH = 0x1;
							constexpr Reg ValueMask_activeHIGH = 0x00000001;
							};
						namespace fHDES { // Field Description
							constexpr Reg Lsb = 1;
							constexpr Reg FieldMask = 0x00000002;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							constexpr Reg Value_activeLOW = 0x0;
							constexpr Reg ValueMask_activeLOW = 0x00000000;
							constexpr Reg Value_activeHIGH = 0x1;
							constexpr Reg ValueMask_activeHIGH = 0x00000002;
							};
						namespace fVSYNCS { // Field Description
							constexpr Reg Lsb = 2;
							constexpr Reg FieldMask = 0x00000004;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							constexpr Reg Value_activeLOW = 0x0;
							constexpr Reg ValueMask_activeLOW = 0x00000000;
							constexpr Reg Value_activeHIGH = 0x1;
							constexpr Reg ValueMask_activeHIGH = 0x00000004;
							};
						namespace fHSYNCS { // Field Description
							constexpr Reg Lsb = 3;
							constexpr Reg FieldMask = 0x00000008;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							constexpr Reg Value_activeLOW = 0x0;
							constexpr Reg ValueMask_activeLOW = 0x00000000;
							constexpr Reg Value_activeHIGH = 0x1;
							constexpr Reg ValueMask_activeHIGH = 0x00000008;
							};
						};
					namespace rLxCR { // Register description
						typedef uint32_t	Reg;
						constexpr Reg	valueAfterReset = 0x00000000;
						namespace fLEN { // Field Description
							constexpr Reg Lsb = 0;
							constexpr Reg FieldMask = 0x00000001;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							constexpr Reg Value_disable = 0x0;
							constexpr Reg ValueMask_disable = 0x00000000;
							constexpr Reg Value_disabled = 0x0;
							constexpr Reg ValueMask_disabled = 0x00000000;
							constexpr Reg Value_enable = 0x1;
							constexpr Reg ValueMask_enable = 0x00000001;
							constexpr Reg Value_enabled = 0x1;
							constexpr Reg ValueMask_enabled = 0x00000001;
							};
						namespace fCOLKEN { // Field Description
							constexpr Reg Lsb = 1;
							constexpr Reg FieldMask = 0x00000002;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							constexpr Reg Value_disable = 0x0;
							constexpr Reg ValueMask_disable = 0x00000000;
							constexpr Reg Value_disabled = 0x0;
							constexpr Reg ValueMask_disabled = 0x00000000;
							constexpr Reg Value_enable = 0x1;
							constexpr Reg ValueMask_enable = 0x00000002;
							constexpr Reg Value_enabled = 0x1;
							constexpr Reg ValueMask_enabled = 0x00000002;
							};
						namespace fCLUTEN { // Field Description
							constexpr Reg Lsb = 4;
							constexpr Reg FieldMask = 0x00000010;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							constexpr Reg Value_disable = 0x0;
							constexpr Reg ValueMask_disable = 0x00000000;
							constexpr Reg Value_disabled = 0x0;
							constexpr Reg ValueMask_disabled = 0x00000000;
							constexpr Reg Value_enable = 0x1;
							constexpr Reg ValueMask_enable = 0x00000010;
							constexpr Reg Value_enabled = 0x1;
							constexpr Reg ValueMask_enabled = 0x00000010;
							};
						};
					namespace rLxWHPCR { // Register description
						typedef uint32_t	Reg;
						constexpr Reg	valueAfterReset = 0x00000000;
						namespace fWHSTPOS { // Field Description
							constexpr Reg Lsb = 0;
							constexpr Reg FieldMask = 0x00000FFF;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							constexpr Reg Value_minValue = 0x0;
							constexpr Reg ValueMask_minValue = 0x00000000;
							constexpr Reg Value_maxValue = 0xFFF;
							constexpr Reg ValueMask_maxValue = 0x00000FFF;
							};
						namespace fWHSPPOS { // Field Description
							constexpr Reg Lsb = 16;
							constexpr Reg FieldMask = 0x0FFF0000;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							constexpr Reg Value_minValue = 0x0;
							constexpr Reg ValueMask_minValue = 0x00000000;
							constexpr Reg Value_maxValue = 0xFFF;
							constexpr Reg ValueMask_maxValue = 0x0FFF0000;
							};
						};
					namespace rLxWVPCR { // Register description
						typedef uint32_t	Reg;
						constexpr Reg	valueAfterReset = 0x00000000;
						namespace fWVSTPOS { // Field Description
							constexpr Reg Lsb = 0;
							constexpr Reg FieldMask = 0x000007FF;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							constexpr Reg Value_minValue = 0x0;
							constexpr Reg ValueMask_minValue = 0x00000000;
							constexpr Reg Value_maxValue = 0x7FF;
							constexpr Reg ValueMask_maxValue = 0x000007FF;
							};
						namespace fWVSPPOS { // Field Description
							constexpr Reg Lsb = 16;
							constexpr Reg FieldMask = 0x07FF0000;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							constexpr Reg Value_minValue = 0x0;
							constexpr Reg ValueMask_minValue = 0x00000000;
							constexpr Reg Value_maxValue = 0x7FF;
							constexpr Reg ValueMask_maxValue = 0x07FF0000;
							};
						};
					namespace rLxCKCR { // Register description
						typedef uint32_t	Reg;
						constexpr Reg	valueAfterReset = 0x00000000;
						namespace fCKBLUE { // Field Description
							constexpr Reg Lsb = 0;
							constexpr Reg FieldMask = 0x000000FF;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							constexpr Reg Value_minValue = 0x0;
							constexpr Reg ValueMask_minValue = 0x00000000;
							constexpr Reg Value_maxValue = 0xFF;
							constexpr Reg ValueMask_maxValue = 0x000000FF;
							};
						namespace fCKGREEN { // Field Description
							constexpr Reg Lsb = 8;
							constexpr Reg FieldMask = 0x0000FF00;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							constexpr Reg Value_minValue = 0x0;
							constexpr Reg ValueMask_minValue = 0x00000000;
							constexpr Reg Value_maxValue = 0xFF;
							constexpr Reg ValueMask_maxValue = 0x0000FF00;
							};
						namespace fCKRED { // Field Description
							constexpr Reg Lsb = 16;
							constexpr Reg FieldMask = 0x00FF0000;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							constexpr Reg Value_minValue = 0x0;
							constexpr Reg ValueMask_minValue = 0x00000000;
							constexpr Reg Value_maxValue = 0xFF;
							constexpr Reg ValueMask_maxValue = 0x00FF0000;
							};
						};
					namespace rLxPFCR { // Register description
						typedef uint32_t	Reg;
						constexpr Reg	valueAfterReset = 0x00000000;
						namespace fPF { // Field Description
							constexpr Reg Lsb = 0;
							constexpr Reg FieldMask = 0x00000007;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							constexpr Reg Value_vARGB8888 = 0x0;
							constexpr Reg ValueMask_vARGB8888 = 0x00000000;
							constexpr Reg Value_vRGB888 = 0x1;
							constexpr Reg ValueMask_vRGB888 = 0x00000001;
							constexpr Reg Value_vRGB565 = 0x2;
							constexpr Reg ValueMask_vRGB565 = 0x00000002;
							constexpr Reg Value_vARGB1555 = 0x4;
							constexpr Reg ValueMask_vARGB1555 = 0x00000004;
							constexpr Reg Value_vARGB4444 = 0x5;
							constexpr Reg ValueMask_vARGB4444 = 0x00000005;
							constexpr Reg Value_vL8 = 0x5;
							constexpr Reg ValueMask_vL8 = 0x00000005;
							constexpr Reg Value_vAL44 = 0x6;
							constexpr Reg ValueMask_vAL44 = 0x00000006;
							constexpr Reg Value_vAL88 = 0x7;
							constexpr Reg ValueMask_vAL88 = 0x00000007;
							};
						};
					namespace rLxCACR { // Register description
						typedef uint32_t	Reg;
						constexpr Reg	valueAfterReset = 0x00000000;
						namespace fCONSTA { // Field Description
							constexpr Reg Lsb = 0;
							constexpr Reg FieldMask = 0x000000FF;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							constexpr Reg Value_minValue = 0x0;
							constexpr Reg ValueMask_minValue = 0x00000000;
							constexpr Reg Value_maxValue = 0xFF;
							constexpr Reg ValueMask_maxValue = 0x000000FF;
							};
						};
					namespace rLxDCCR { // Register description
						typedef uint32_t	Reg;
						constexpr Reg	valueAfterReset = 0x00000000;
						namespace fDCBLUE { // Field Description
							constexpr Reg Lsb = 0;
							constexpr Reg FieldMask = 0x000000FF;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							constexpr Reg Value_minValue = 0x0;
							constexpr Reg ValueMask_minValue = 0x00000000;
							constexpr Reg Value_maxValue = 0xFF;
							constexpr Reg ValueMask_maxValue = 0x000000FF;
							};
						namespace fDCGREEN { // Field Description
							constexpr Reg Lsb = 8;
							constexpr Reg FieldMask = 0x0000FF00;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							constexpr Reg Value_minValue = 0x0;
							constexpr Reg ValueMask_minValue = 0x00000000;
							constexpr Reg Value_maxValue = 0xFF;
							constexpr Reg ValueMask_maxValue = 0x0000FF00;
							};
						namespace fDCRED { // Field Description
							constexpr Reg Lsb = 16;
							constexpr Reg FieldMask = 0x00FF0000;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							constexpr Reg Value_minValue = 0x0;
							constexpr Reg ValueMask_minValue = 0x00000000;
							constexpr Reg Value_maxValue = 0xFF;
							constexpr Reg ValueMask_maxValue = 0x00FF0000;
							};
						namespace fDCALPHA { // Field Description
							constexpr Reg Lsb = 24;
							constexpr Reg FieldMask = 0xFF000000;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							constexpr Reg Value_minValue = 0x0;
							constexpr Reg ValueMask_minValue = 0x00000000;
							constexpr Reg Value_maxValue = 0xFF;
							constexpr Reg ValueMask_maxValue = 0xFF000000;
							};
						};
					namespace rLxBFCR { // Register description
						typedef uint32_t	Reg;
						constexpr Reg	valueAfterReset = 0x00000607;
						namespace fBF2 { // Field Description
							constexpr Reg Lsb = 0;
							constexpr Reg FieldMask = 0x00000007;
							constexpr Reg Value_valueAfterReset = 0x7;
							constexpr Reg ValueMask_valueAfterReset = 0x00000007;
							constexpr Reg Value_constantAlpha = 0x5;
							constexpr Reg ValueMask_constantAlpha = 0x00000005;
							constexpr Reg Value_pixelAlphaxConstant = 0x7;
							constexpr Reg ValueMask_pixelAlphaxConstant = 0x00000007;
							};
						namespace fBF1 { // Field Description
							constexpr Reg Lsb = 8;
							constexpr Reg FieldMask = 0x00000700;
							constexpr Reg Value_valueAfterReset = 0x6;
							constexpr Reg ValueMask_valueAfterReset = 0x00000600;
							constexpr Reg Value_constantAlpha = 0x4;
							constexpr Reg ValueMask_constantAlpha = 0x00000400;
							constexpr Reg Value_pixelAlphaxConstant = 0x6;
							constexpr Reg ValueMask_pixelAlphaxConstant = 0x00000600;
							};
						};
					namespace rLxCFBAR { // Register description
						typedef uint32_t	Reg;
						constexpr Reg	valueAfterReset = 0x00000000;
						namespace fCFBADD { // Field Description
							constexpr Reg Lsb = 0;
							constexpr Reg FieldMask = 0xFFFFFFFF;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							constexpr Reg Value_minValue = 0x0;
							constexpr Reg ValueMask_minValue = 0x00000000;
							constexpr Reg Value_maxValue = 0xFFFFFFFF;
							constexpr Reg ValueMask_maxValue = 0xFFFFFFFF;
							};
						};
					namespace rLxCFBLR { // Register description
						typedef uint32_t	Reg;
						constexpr Reg	valueAfterReset = 0x00000000;
						namespace fCFBLL { // Field Description
							constexpr Reg Lsb = 0;
							constexpr Reg FieldMask = 0x00001FFF;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							constexpr Reg Value_minValue = 0x0;
							constexpr Reg ValueMask_minValue = 0x00000000;
							constexpr Reg Value_maxValue = 0x1FFF;
							constexpr Reg ValueMask_maxValue = 0x00001FFF;
							};
						namespace fCFBP { // Field Description
							constexpr Reg Lsb = 16;
							constexpr Reg FieldMask = 0x1FFF0000;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							constexpr Reg Value_minValue = 0x0;
							constexpr Reg ValueMask_minValue = 0x00000000;
							constexpr Reg Value_maxValue = 0x1FFF;
							constexpr Reg ValueMask_maxValue = 0x1FFF0000;
							};
						};
					namespace rLxCFBLNR { // Register description
						typedef uint32_t	Reg;
						constexpr Reg	valueAfterReset = 0x00000000;
						namespace fCFBLNBR { // Field Description
							constexpr Reg Lsb = 0;
							constexpr Reg FieldMask = 0x000007FF;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							constexpr Reg Value_minValue = 0x0;
							constexpr Reg ValueMask_minValue = 0x00000000;
							constexpr Reg Value_maxValue = 0x7FF;
							constexpr Reg ValueMask_maxValue = 0x000007FF;
							};
						};
					namespace rLxCLUTWR { // Register description
						typedef uint32_t	Reg;
						constexpr Reg	valueAfterReset = 0x00000000;
						namespace fBLUE { // Field Description
							constexpr Reg Lsb = 0;
							constexpr Reg FieldMask = 0x000000FF;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							constexpr Reg Value_minValue = 0x0;
							constexpr Reg ValueMask_minValue = 0x00000000;
							constexpr Reg Value_maxValue = 0xFF;
							constexpr Reg ValueMask_maxValue = 0x000000FF;
							};
						namespace fGREEN { // Field Description
							constexpr Reg Lsb = 8;
							constexpr Reg FieldMask = 0x0000FF00;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							constexpr Reg Value_minValue = 0x0;
							constexpr Reg ValueMask_minValue = 0x00000000;
							constexpr Reg Value_maxValue = 0xFF;
							constexpr Reg ValueMask_maxValue = 0x0000FF00;
							};
						namespace fRED { // Field Description
							constexpr Reg Lsb = 16;
							constexpr Reg FieldMask = 0x00FF0000;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							constexpr Reg Value_minValue = 0x0;
							constexpr Reg ValueMask_minValue = 0x00000000;
							constexpr Reg Value_maxValue = 0xFF;
							constexpr Reg ValueMask_maxValue = 0x00FF0000;
							};
						namespace fCLUTADD { // Field Description
							constexpr Reg Lsb = 24;
							constexpr Reg FieldMask = 0xFF000000;
							constexpr Reg Value_valueAfterReset = 0x0;
							constexpr Reg ValueMask_valueAfterReset = 0x00000000;
							constexpr Reg Value_minValue = 0x0;
							constexpr Reg ValueMask_minValue = 0x00000000;
							constexpr Reg Value_maxValue = 0xFF;
							constexpr Reg ValueMask_maxValue = 0xFF000000;
							};
						};
					}
				}
			}
		}
	}
#endif
