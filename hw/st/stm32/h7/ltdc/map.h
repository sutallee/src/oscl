/*
   Copyright (C) 2024 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_hw_st_stm32_h7_ltdc_maph_
#define _oscl_hw_st_stm32_h7_ltdc_maph_
#include "reg.h"

/** */
namespace Oscl {
/** */
namespace St {
/** */
namespace Stm32 {
/** */
namespace H7 {
/** LCD-TFT Display Controller */
namespace pLTDC {
/** */
struct Layer {
	/** Layer x Control Register
		offset:0x00
	 */
	volatile rLxCR::Reg	cr;

	/** Layer x Window Horizontal Position Configuration Register
		offset:0x04
	 */
	volatile rLxWHPCR::Reg	whpcr;

	/** Layer x Window Vertical Position Configuration Register
		offset:0x08
	 */
	volatile rLxWVPCR::Reg	wvpcr;

	/** Layer x Color Keying Configuration Register
		offset:0x0C
	 */
	volatile rLxCKCR::Reg	ckcr;

	/** Layer x Pixel Format Configuration Register
		offset:0x10
	 */
	volatile rLxPFCR::Reg	pfcr;

	/** Layer x Constant Alpha Configuration Register
		offset:0x14
	 */
	volatile rLxCACR::Reg	cacr;

	/** Layer x Default Color Configuration Register
		offset:0x18
	 */
	volatile rLxDCCR::Reg	dccr;

	/** Layer x Blending Factors Configuration Register
		offset:0x1C
	 */
	volatile rLxBFCR::Reg	bfcr;

	const uint8_t	_reserve164[8];

	/** Layer x Color Frame Buffer Address Register
		offset:0x28
	 */
	volatile rLxCFBAR::Reg	cfbar;

	/** Layer x Color Frame Buffer Length Register
		offset:0x2C
	 */
	volatile rLxCFBLR::Reg	cfblr;

	/** Layer x Color Frame Buffer Line Number Register
		offset:0x30
	 */
	volatile rLxCFBLNR::Reg	cfblnr;

	const uint8_t	_reserve184[12];

	/** Layer x CLUT Write Register
		offset:0x40
	 */
	volatile rLxCLUTWR::Reg	clutwr;
	};

/** */
struct Map {
	const uint8_t	_reserve0[8];
	/** Synchronization Size Configuration Register
		offset:0x8
	 */
	volatile rSSCR::Reg	sscr;

	/** Back Porch Configuration Register
		offset:0xc
	 */
	volatile rBPCR::Reg	bpcr;

	/** Active Width Configration Register
		offset:0x10
	 */
	volatile rAWCR::Reg	awcr;

	/** Total Width Configration Register
		offset:0x14
	 */
	volatile rTWCR::Reg	twcr;

	/** Global Control Register
		offset:0x18
	 */
	volatile rGCR::Reg	gcr;

	const uint8_t	_reserve28[8];

	/** Shadow Reload Configuration Register
		offset:0x24
	 */
	volatile rSRCR::Reg	srcr;

	const uint8_t	_reserve40[4];

	/** Background Color Configuration Register
		offset:0x2c
	 */
	volatile rBCCR::Reg	bccr;

	const uint8_t	_reserve48[4];

	/** Interrupt Enable Register
		offset:0x34
	 */
	volatile rIER::Reg	ier;

	/** Interrupt Status Register
		offset:0x38
	 */
	volatile rISR::Reg	isr;

	/** Interrupt Clear Register
		offset:0x3c
	 */
	volatile rICR::Reg	icr;

	/** Line Interrupt Position Configuration Register
		offset:0x40
	 */
	volatile rLIPCR::Reg	lipcr;

	/** Current Position Status Register
		offset:0x44
	 */
	volatile rCPSR::Reg	cpsr;

	/** Current Display Status Register
		offset:0x48
	 */
	volatile rCDSR::Reg	cdsr;

	const uint8_t	_reserve76[56];

	/** Layer 1 offset: 0x84 */
	Layer	layer1;

	const uint8_t	_reserve200[60];

	/** Layer 1 offset: 0x104 */
	Layer	layer2;
	};
}
}
}
}
}
#endif
