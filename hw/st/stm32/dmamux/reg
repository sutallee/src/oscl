/*
   Copyright (C) 2024 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

module oscl_hw_st_stm32_dmamux_regh {
sysinclude "stdint.h";
namespace Oscl {
namespace St {
namespace Stm32 {
namespace pDMAMUX {
	register rCxCR uint32_t {
		value valueAtReset	0x00000000
		field fDMAREQ_ID 0 8 {
			/*	Selects the peripheral the DMA request
				for this channel.
			 */
			value valueAtReset	0x00
			value minValue	0x00
			value maxValue	0xFF
			}
		field fSOIE 8 1 {
			/*	Enables synchronization
				overrun interrupt for this
				channel.
			 */
			value valueAtReset	0
			value disable		0
			value disabled		0
			value enable		1
			value enabled		1
			}
		field fEGE 9 1 {
			/*	Enables the event generation
				output for this channel.
			 */
			value valueAtReset	0
			value disable		0
			value disabled		0
			value enable		1
			value enabled		1
			}
		field fSE 16 1 {
			/*	Enables the synchronization
				input for this channel.
			 */
			value valueAtReset	0
			value disable		0
			value disabled		0
			value enable		1
			value enabled		1
			}
		field fSPOL 17 2 {
			/*	Configures the synchronization
				edge polarity for this channel.
			 */
			value valueAtReset	0x0
			value noEvent		0
			value risingEdge	1
			value fallingEdge	2
			value bothEdges		3
			}
		field fNBREQ 19 5 {
			/*	Selects the DMA Stream
				to which the DMA request
				for this channel is routed.
			 */
			value valueAtReset	0x00
			value minValue	0x00
			value maxValue	0x1F
			}
		field fSYNC_ID 24 5 {
			/*	Selects the synchronization
				source for this channel.
			 */
			value valueAtReset	0x00
			value minValue	0x00
			value maxValue	0x1F
			}
		}
	register rRGxCR uint32_t {
		value valueAtReset	0x00000000
		field fSIG_ID 0 5 {
			value valueAtReset	0x00
			value minValue		0x00
			value maxValue		0x1F
			}
		field fOIE 8 1 {
			value valueAtReset	0
			value disable		0
			value disabled		0
			value enable		1
			value enabled		1
			}
		field fGE 16 1 {
			value valueAtReset	0
			value disable		0
			value disabled		0
			value enable		1
			value enabled		1
			}
		field fGPOL 17 2 {
			value valueAtReset	0x0
			value noEvent		0
			value risingEdge	1
			value fallingEdge	2
			value bothEdges		3
			}
		field fGNBREQ 19 5 {
			value valueAtReset	0x00
			value minValue		0x00
			value maxValue		0x1F
			}
		}
	register rRGSR uint32_t {
		/* read-only */
		value valueAtReset	0x00000000
		value notPending	0
		value pending		1
		field fOF0 0 1 {
			value notPending	0
			value pending		1
			}
		field fOF1 1 1 {
			value notPending	0
			value pending		1
			}
		field fOF2 2 1 {
			value notPending	0
			value pending		1
			}
		field fOF3 3 1 {
			value notPending	0
			value pending		1
			}
		field fOF4 4 1 {
			value notPending	0
			value pending		1
			}
		field fOF5 5 1 {
			value notPending	0
			value pending		1
			}
		field fOF6 6 1 {
			value notPending	0
			value pending		1
			}
		field fOF7 7 1 {
			value notPending	0
			value pending		1
			}
		}
	register rRGCFR uint32_t {
		/* write-only */
		value valueAtReset	0x00000000
		value noEffect	0
		value clear		1
		field fCOF0 0 1 {
			value noEffect	0
			value clear		1
			}
		field fCOF1 1 1 {
			value noEffect	0
			value clear		1
			}
		field fCOF2 2 1 {
			value noEffect	0
			value clear		1
			}
		field fCOF3 3 1 {
			value noEffect	0
			value clear		1
			}
		field fCOF4 4 1 {
			value noEffect	0
			value clear		1
			}
		field fCOF5 5 1 {
			value noEffect	0
			value clear		1
			}
		field fCOF6 6 1 {
			value noEffect	0
			value clear		1
			}
		field fCOF7 7 1 {
			value noEffect	0
			value clear		1
			}
		}
	register rCSR uint32_t {
		/* read-only */
		value valueAtReset	0x00000000
		value notPending	0
		value pending		1
		field fSOF0 0 1 {
			value notPending	0
			value pending		1
			}
		field fSOF1 1 1 {
			value notPending	0
			value pending		1
			}
		field fSOF2 2 1 {
			value notPending	0
			value pending		1
			}
		field fSOF3 3 1 {
			value notPending	0
			value pending		1
			}
		field fSOF4 4 1 {
			value notPending	0
			value pending		1
			}
		field fSOF5 5 1 {
			value notPending	0
			value pending		1
			}
		field fSOF6 6 1 {
			value notPending	0
			value pending		1
			}
		field fSOF7 7 1 {
			value notPending	0
			value pending		1
			}
		field fSOF8 8 1 {
			value notPending	0
			value pending		1
			}
		field fSOF9 9 1 {
			value notPending	0
			value pending		1
			}
		field fSOF10 10 1 {
			value notPending	0
			value pending		1
			}
		field fSOF11 11 1 {
			value notPending	0
			value pending		1
			}
		field fSOF12 12 1 {
			value notPending	0
			value pending		1
			}
		field fSOF13 13 1 {
			value notPending	0
			value pending		1
			}
		field fSOF14 14 1 {
			value notPending	0
			value pending		1
			}
		field fSOF15 15 1 {
			value notPending	0
			value pending		1
			}
		}
	register rCFR uint32_t {
		/* write-only */
		value valueAtReset	0x00000000
		value noEffect	0
		value clear		1
		field fCSOF0 0 1 {
			value noEffect	0
			value clear		1
			}
		field fCSOF1 1 1 {
			value noEffect	0
			value clear		1
			}
		field fCSOF2 2 1 {
			value noEffect	0
			value clear		1
			}
		field fCSOF3 3 1 {
			value noEffect	0
			value clear		1
			}
		field fCSOF4 4 1 {
			value noEffect	0
			value clear		1
			}
		field fCSOF5 5 1 {
			value noEffect	0
			value clear		1
			}
		field fCSOF6 6 1 {
			value noEffect	0
			value clear		1
			}
		field fCSOF7 7 1 {
			value noEffect	0
			value clear		1
			}
		field fCSOF8 8 1 {
			value noEffect	0
			value clear		1
			}
		field fCSOF9 9 1 {
			value noEffect	0
			value clear		1
			}
		field fCSOF10 10 1 {
			value noEffect	0
			value clear		1
			}
		field fCSOF11 11 1 {
			value noEffect	0
			value clear		1
			}
		field fCSOF12 12 1 {
			value noEffect	0
			value clear		1
			}
		field fCSOF13 13 1 {
			value noEffect	0
			value clear		1
			}
		field fCSOF14 14 1 {
			value noEffect	0
			value clear		1
			}
		field fCSOF15 15 1 {
			value noEffect	0
			value clear		1
			}
		}
}
}
}
}
}
