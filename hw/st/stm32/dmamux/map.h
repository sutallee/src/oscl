/*
   Copyright (C) 2024 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_hw_st_stm32_dmamux_maph_
#define _oscl_hw_st_stm32_dmamux_maph_
#include "reg.h"
/* */
namespace Oscl {
/* */
namespace St {
/* */
namespace Stm32 {
/* */
namespace pDMAMUX {
/* */
struct Map {
	/** offset:0x0 */
	volatile Oscl::St::Stm32::pDMAMUX::rCxCR::Reg	cxcr[16];
	const uint8_t	_reserve32[0x80-0x40];
	/** offset:0x80 */
	volatile Oscl::St::Stm32::pDMAMUX::rCSR::Reg	csr;
	/** offset:0x84 */
	volatile Oscl::St::Stm32::pDMAMUX::rCFR::Reg	cfr;
	const uint8_t	_reserve136[120];
	/** offset:0x100 */
	volatile Oscl::St::Stm32::pDMAMUX::rRGxCR::Reg	rgxcr[8];
	const uint8_t	_reserve288[32];
	/** offset:0x140 */
	volatile Oscl::St::Stm32::pDMAMUX::rRGSR::Reg	rgsr;
	/** offset:0x144 */
	volatile Oscl::St::Stm32::pDMAMUX::rRGCFR::Reg	rgcfr;
	};
}
}
}
}
#endif
