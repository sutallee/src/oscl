/*
   Copyright (C) 2024 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_hw_st_stm32_dmamux_regh_
#define _oscl_hw_st_stm32_dmamux_regh_
#include <stdint.h>
namespace Oscl { // Namespace description
	namespace St { // Namespace description
		namespace Stm32 { // Namespace description
			namespace pDMAMUX { // Namespace description
				namespace rCxCR { // Register description
					typedef uint32_t	Reg;
					constexpr Reg	valueAtReset = 0x00000000;
					namespace fDMAREQ_ID { // Field Description
						constexpr Reg Lsb = 0;
						constexpr Reg FieldMask = 0x000000FF;
						constexpr Reg Value_valueAtReset = 0x0;
						constexpr Reg ValueMask_valueAtReset = 0x00000000;
						constexpr Reg Value_minValue = 0x0;
						constexpr Reg ValueMask_minValue = 0x00000000;
						constexpr Reg Value_maxValue = 0xFF;
						constexpr Reg ValueMask_maxValue = 0x000000FF;
						};
					namespace fSOIE { // Field Description
						constexpr Reg Lsb = 8;
						constexpr Reg FieldMask = 0x00000100;
						constexpr Reg Value_valueAtReset = 0x0;
						constexpr Reg ValueMask_valueAtReset = 0x00000000;
						constexpr Reg Value_disable = 0x0;
						constexpr Reg ValueMask_disable = 0x00000000;
						constexpr Reg Value_disabled = 0x0;
						constexpr Reg ValueMask_disabled = 0x00000000;
						constexpr Reg Value_enable = 0x1;
						constexpr Reg ValueMask_enable = 0x00000100;
						constexpr Reg Value_enabled = 0x1;
						constexpr Reg ValueMask_enabled = 0x00000100;
						};
					namespace fEGE { // Field Description
						constexpr Reg Lsb = 9;
						constexpr Reg FieldMask = 0x00000200;
						constexpr Reg Value_valueAtReset = 0x0;
						constexpr Reg ValueMask_valueAtReset = 0x00000000;
						constexpr Reg Value_disable = 0x0;
						constexpr Reg ValueMask_disable = 0x00000000;
						constexpr Reg Value_disabled = 0x0;
						constexpr Reg ValueMask_disabled = 0x00000000;
						constexpr Reg Value_enable = 0x1;
						constexpr Reg ValueMask_enable = 0x00000200;
						constexpr Reg Value_enabled = 0x1;
						constexpr Reg ValueMask_enabled = 0x00000200;
						};
					namespace fSE { // Field Description
						constexpr Reg Lsb = 16;
						constexpr Reg FieldMask = 0x00010000;
						constexpr Reg Value_valueAtReset = 0x0;
						constexpr Reg ValueMask_valueAtReset = 0x00000000;
						constexpr Reg Value_disable = 0x0;
						constexpr Reg ValueMask_disable = 0x00000000;
						constexpr Reg Value_disabled = 0x0;
						constexpr Reg ValueMask_disabled = 0x00000000;
						constexpr Reg Value_enable = 0x1;
						constexpr Reg ValueMask_enable = 0x00010000;
						constexpr Reg Value_enabled = 0x1;
						constexpr Reg ValueMask_enabled = 0x00010000;
						};
					namespace fSPOL { // Field Description
						constexpr Reg Lsb = 17;
						constexpr Reg FieldMask = 0x00060000;
						constexpr Reg Value_valueAtReset = 0x0;
						constexpr Reg ValueMask_valueAtReset = 0x00000000;
						constexpr Reg Value_noEvent = 0x0;
						constexpr Reg ValueMask_noEvent = 0x00000000;
						constexpr Reg Value_risingEdge = 0x1;
						constexpr Reg ValueMask_risingEdge = 0x00020000;
						constexpr Reg Value_fallingEdge = 0x2;
						constexpr Reg ValueMask_fallingEdge = 0x00040000;
						constexpr Reg Value_bothEdges = 0x3;
						constexpr Reg ValueMask_bothEdges = 0x00060000;
						};
					namespace fNBREQ { // Field Description
						constexpr Reg Lsb = 19;
						constexpr Reg FieldMask = 0x00F80000;
						constexpr Reg Value_valueAtReset = 0x0;
						constexpr Reg ValueMask_valueAtReset = 0x00000000;
						constexpr Reg Value_minValue = 0x0;
						constexpr Reg ValueMask_minValue = 0x00000000;
						constexpr Reg Value_maxValue = 0x1F;
						constexpr Reg ValueMask_maxValue = 0x00F80000;
						};
					namespace fSYNC_ID { // Field Description
						constexpr Reg Lsb = 24;
						constexpr Reg FieldMask = 0x1F000000;
						constexpr Reg Value_valueAtReset = 0x0;
						constexpr Reg ValueMask_valueAtReset = 0x00000000;
						constexpr Reg Value_minValue = 0x0;
						constexpr Reg ValueMask_minValue = 0x00000000;
						constexpr Reg Value_maxValue = 0x1F;
						constexpr Reg ValueMask_maxValue = 0x1F000000;
						};
					};
				namespace rRGxCR { // Register description
					typedef uint32_t	Reg;
					constexpr Reg	valueAtReset = 0x00000000;
					namespace fSIG_ID { // Field Description
						constexpr Reg Lsb = 0;
						constexpr Reg FieldMask = 0x0000001F;
						constexpr Reg Value_valueAtReset = 0x0;
						constexpr Reg ValueMask_valueAtReset = 0x00000000;
						constexpr Reg Value_minValue = 0x0;
						constexpr Reg ValueMask_minValue = 0x00000000;
						constexpr Reg Value_maxValue = 0x1F;
						constexpr Reg ValueMask_maxValue = 0x0000001F;
						};
					namespace fOIE { // Field Description
						constexpr Reg Lsb = 8;
						constexpr Reg FieldMask = 0x00000100;
						constexpr Reg Value_valueAtReset = 0x0;
						constexpr Reg ValueMask_valueAtReset = 0x00000000;
						constexpr Reg Value_disable = 0x0;
						constexpr Reg ValueMask_disable = 0x00000000;
						constexpr Reg Value_disabled = 0x0;
						constexpr Reg ValueMask_disabled = 0x00000000;
						constexpr Reg Value_enable = 0x1;
						constexpr Reg ValueMask_enable = 0x00000100;
						constexpr Reg Value_enabled = 0x1;
						constexpr Reg ValueMask_enabled = 0x00000100;
						};
					namespace fGE { // Field Description
						constexpr Reg Lsb = 16;
						constexpr Reg FieldMask = 0x00010000;
						constexpr Reg Value_valueAtReset = 0x0;
						constexpr Reg ValueMask_valueAtReset = 0x00000000;
						constexpr Reg Value_disable = 0x0;
						constexpr Reg ValueMask_disable = 0x00000000;
						constexpr Reg Value_disabled = 0x0;
						constexpr Reg ValueMask_disabled = 0x00000000;
						constexpr Reg Value_enable = 0x1;
						constexpr Reg ValueMask_enable = 0x00010000;
						constexpr Reg Value_enabled = 0x1;
						constexpr Reg ValueMask_enabled = 0x00010000;
						};
					namespace fGPOL { // Field Description
						constexpr Reg Lsb = 17;
						constexpr Reg FieldMask = 0x00060000;
						constexpr Reg Value_valueAtReset = 0x0;
						constexpr Reg ValueMask_valueAtReset = 0x00000000;
						constexpr Reg Value_noEvent = 0x0;
						constexpr Reg ValueMask_noEvent = 0x00000000;
						constexpr Reg Value_risingEdge = 0x1;
						constexpr Reg ValueMask_risingEdge = 0x00020000;
						constexpr Reg Value_fallingEdge = 0x2;
						constexpr Reg ValueMask_fallingEdge = 0x00040000;
						constexpr Reg Value_bothEdges = 0x3;
						constexpr Reg ValueMask_bothEdges = 0x00060000;
						};
					namespace fGNBREQ { // Field Description
						constexpr Reg Lsb = 19;
						constexpr Reg FieldMask = 0x00F80000;
						constexpr Reg Value_valueAtReset = 0x0;
						constexpr Reg ValueMask_valueAtReset = 0x00000000;
						constexpr Reg Value_minValue = 0x0;
						constexpr Reg ValueMask_minValue = 0x00000000;
						constexpr Reg Value_maxValue = 0x1F;
						constexpr Reg ValueMask_maxValue = 0x00F80000;
						};
					};
				namespace rRGSR { // Register description
					typedef uint32_t	Reg;
					constexpr Reg	valueAtReset = 0x00000000;
					constexpr Reg	notPending = 0x00000000;
					constexpr Reg	pending = 0x00000001;
					namespace fOF0 { // Field Description
						constexpr Reg Lsb = 0;
						constexpr Reg FieldMask = 0x00000001;
						constexpr Reg Value_notPending = 0x0;
						constexpr Reg ValueMask_notPending = 0x00000000;
						constexpr Reg Value_pending = 0x1;
						constexpr Reg ValueMask_pending = 0x00000001;
						};
					namespace fOF1 { // Field Description
						constexpr Reg Lsb = 1;
						constexpr Reg FieldMask = 0x00000002;
						constexpr Reg Value_notPending = 0x0;
						constexpr Reg ValueMask_notPending = 0x00000000;
						constexpr Reg Value_pending = 0x1;
						constexpr Reg ValueMask_pending = 0x00000002;
						};
					namespace fOF2 { // Field Description
						constexpr Reg Lsb = 2;
						constexpr Reg FieldMask = 0x00000004;
						constexpr Reg Value_notPending = 0x0;
						constexpr Reg ValueMask_notPending = 0x00000000;
						constexpr Reg Value_pending = 0x1;
						constexpr Reg ValueMask_pending = 0x00000004;
						};
					namespace fOF3 { // Field Description
						constexpr Reg Lsb = 3;
						constexpr Reg FieldMask = 0x00000008;
						constexpr Reg Value_notPending = 0x0;
						constexpr Reg ValueMask_notPending = 0x00000000;
						constexpr Reg Value_pending = 0x1;
						constexpr Reg ValueMask_pending = 0x00000008;
						};
					namespace fOF4 { // Field Description
						constexpr Reg Lsb = 4;
						constexpr Reg FieldMask = 0x00000010;
						constexpr Reg Value_notPending = 0x0;
						constexpr Reg ValueMask_notPending = 0x00000000;
						constexpr Reg Value_pending = 0x1;
						constexpr Reg ValueMask_pending = 0x00000010;
						};
					namespace fOF5 { // Field Description
						constexpr Reg Lsb = 5;
						constexpr Reg FieldMask = 0x00000020;
						constexpr Reg Value_notPending = 0x0;
						constexpr Reg ValueMask_notPending = 0x00000000;
						constexpr Reg Value_pending = 0x1;
						constexpr Reg ValueMask_pending = 0x00000020;
						};
					namespace fOF6 { // Field Description
						constexpr Reg Lsb = 6;
						constexpr Reg FieldMask = 0x00000040;
						constexpr Reg Value_notPending = 0x0;
						constexpr Reg ValueMask_notPending = 0x00000000;
						constexpr Reg Value_pending = 0x1;
						constexpr Reg ValueMask_pending = 0x00000040;
						};
					namespace fOF7 { // Field Description
						constexpr Reg Lsb = 7;
						constexpr Reg FieldMask = 0x00000080;
						constexpr Reg Value_notPending = 0x0;
						constexpr Reg ValueMask_notPending = 0x00000000;
						constexpr Reg Value_pending = 0x1;
						constexpr Reg ValueMask_pending = 0x00000080;
						};
					};
				namespace rRGCFR { // Register description
					typedef uint32_t	Reg;
					constexpr Reg	valueAtReset = 0x00000000;
					constexpr Reg	noEffect = 0x00000000;
					constexpr Reg	clear = 0x00000001;
					namespace fCOF0 { // Field Description
						constexpr Reg Lsb = 0;
						constexpr Reg FieldMask = 0x00000001;
						constexpr Reg Value_noEffect = 0x0;
						constexpr Reg ValueMask_noEffect = 0x00000000;
						constexpr Reg Value_clear = 0x1;
						constexpr Reg ValueMask_clear = 0x00000001;
						};
					namespace fCOF1 { // Field Description
						constexpr Reg Lsb = 1;
						constexpr Reg FieldMask = 0x00000002;
						constexpr Reg Value_noEffect = 0x0;
						constexpr Reg ValueMask_noEffect = 0x00000000;
						constexpr Reg Value_clear = 0x1;
						constexpr Reg ValueMask_clear = 0x00000002;
						};
					namespace fCOF2 { // Field Description
						constexpr Reg Lsb = 2;
						constexpr Reg FieldMask = 0x00000004;
						constexpr Reg Value_noEffect = 0x0;
						constexpr Reg ValueMask_noEffect = 0x00000000;
						constexpr Reg Value_clear = 0x1;
						constexpr Reg ValueMask_clear = 0x00000004;
						};
					namespace fCOF3 { // Field Description
						constexpr Reg Lsb = 3;
						constexpr Reg FieldMask = 0x00000008;
						constexpr Reg Value_noEffect = 0x0;
						constexpr Reg ValueMask_noEffect = 0x00000000;
						constexpr Reg Value_clear = 0x1;
						constexpr Reg ValueMask_clear = 0x00000008;
						};
					namespace fCOF4 { // Field Description
						constexpr Reg Lsb = 4;
						constexpr Reg FieldMask = 0x00000010;
						constexpr Reg Value_noEffect = 0x0;
						constexpr Reg ValueMask_noEffect = 0x00000000;
						constexpr Reg Value_clear = 0x1;
						constexpr Reg ValueMask_clear = 0x00000010;
						};
					namespace fCOF5 { // Field Description
						constexpr Reg Lsb = 5;
						constexpr Reg FieldMask = 0x00000020;
						constexpr Reg Value_noEffect = 0x0;
						constexpr Reg ValueMask_noEffect = 0x00000000;
						constexpr Reg Value_clear = 0x1;
						constexpr Reg ValueMask_clear = 0x00000020;
						};
					namespace fCOF6 { // Field Description
						constexpr Reg Lsb = 6;
						constexpr Reg FieldMask = 0x00000040;
						constexpr Reg Value_noEffect = 0x0;
						constexpr Reg ValueMask_noEffect = 0x00000000;
						constexpr Reg Value_clear = 0x1;
						constexpr Reg ValueMask_clear = 0x00000040;
						};
					namespace fCOF7 { // Field Description
						constexpr Reg Lsb = 7;
						constexpr Reg FieldMask = 0x00000080;
						constexpr Reg Value_noEffect = 0x0;
						constexpr Reg ValueMask_noEffect = 0x00000000;
						constexpr Reg Value_clear = 0x1;
						constexpr Reg ValueMask_clear = 0x00000080;
						};
					};
				namespace rCSR { // Register description
					typedef uint32_t	Reg;
					constexpr Reg	valueAtReset = 0x00000000;
					constexpr Reg	notPending = 0x00000000;
					constexpr Reg	pending = 0x00000001;
					namespace fSOF0 { // Field Description
						constexpr Reg Lsb = 0;
						constexpr Reg FieldMask = 0x00000001;
						constexpr Reg Value_notPending = 0x0;
						constexpr Reg ValueMask_notPending = 0x00000000;
						constexpr Reg Value_pending = 0x1;
						constexpr Reg ValueMask_pending = 0x00000001;
						};
					namespace fSOF1 { // Field Description
						constexpr Reg Lsb = 1;
						constexpr Reg FieldMask = 0x00000002;
						constexpr Reg Value_notPending = 0x0;
						constexpr Reg ValueMask_notPending = 0x00000000;
						constexpr Reg Value_pending = 0x1;
						constexpr Reg ValueMask_pending = 0x00000002;
						};
					namespace fSOF2 { // Field Description
						constexpr Reg Lsb = 2;
						constexpr Reg FieldMask = 0x00000004;
						constexpr Reg Value_notPending = 0x0;
						constexpr Reg ValueMask_notPending = 0x00000000;
						constexpr Reg Value_pending = 0x1;
						constexpr Reg ValueMask_pending = 0x00000004;
						};
					namespace fSOF3 { // Field Description
						constexpr Reg Lsb = 3;
						constexpr Reg FieldMask = 0x00000008;
						constexpr Reg Value_notPending = 0x0;
						constexpr Reg ValueMask_notPending = 0x00000000;
						constexpr Reg Value_pending = 0x1;
						constexpr Reg ValueMask_pending = 0x00000008;
						};
					namespace fSOF4 { // Field Description
						constexpr Reg Lsb = 4;
						constexpr Reg FieldMask = 0x00000010;
						constexpr Reg Value_notPending = 0x0;
						constexpr Reg ValueMask_notPending = 0x00000000;
						constexpr Reg Value_pending = 0x1;
						constexpr Reg ValueMask_pending = 0x00000010;
						};
					namespace fSOF5 { // Field Description
						constexpr Reg Lsb = 5;
						constexpr Reg FieldMask = 0x00000020;
						constexpr Reg Value_notPending = 0x0;
						constexpr Reg ValueMask_notPending = 0x00000000;
						constexpr Reg Value_pending = 0x1;
						constexpr Reg ValueMask_pending = 0x00000020;
						};
					namespace fSOF6 { // Field Description
						constexpr Reg Lsb = 6;
						constexpr Reg FieldMask = 0x00000040;
						constexpr Reg Value_notPending = 0x0;
						constexpr Reg ValueMask_notPending = 0x00000000;
						constexpr Reg Value_pending = 0x1;
						constexpr Reg ValueMask_pending = 0x00000040;
						};
					namespace fSOF7 { // Field Description
						constexpr Reg Lsb = 7;
						constexpr Reg FieldMask = 0x00000080;
						constexpr Reg Value_notPending = 0x0;
						constexpr Reg ValueMask_notPending = 0x00000000;
						constexpr Reg Value_pending = 0x1;
						constexpr Reg ValueMask_pending = 0x00000080;
						};
					namespace fSOF8 { // Field Description
						constexpr Reg Lsb = 8;
						constexpr Reg FieldMask = 0x00000100;
						constexpr Reg Value_notPending = 0x0;
						constexpr Reg ValueMask_notPending = 0x00000000;
						constexpr Reg Value_pending = 0x1;
						constexpr Reg ValueMask_pending = 0x00000100;
						};
					namespace fSOF9 { // Field Description
						constexpr Reg Lsb = 9;
						constexpr Reg FieldMask = 0x00000200;
						constexpr Reg Value_notPending = 0x0;
						constexpr Reg ValueMask_notPending = 0x00000000;
						constexpr Reg Value_pending = 0x1;
						constexpr Reg ValueMask_pending = 0x00000200;
						};
					namespace fSOF10 { // Field Description
						constexpr Reg Lsb = 10;
						constexpr Reg FieldMask = 0x00000400;
						constexpr Reg Value_notPending = 0x0;
						constexpr Reg ValueMask_notPending = 0x00000000;
						constexpr Reg Value_pending = 0x1;
						constexpr Reg ValueMask_pending = 0x00000400;
						};
					namespace fSOF11 { // Field Description
						constexpr Reg Lsb = 11;
						constexpr Reg FieldMask = 0x00000800;
						constexpr Reg Value_notPending = 0x0;
						constexpr Reg ValueMask_notPending = 0x00000000;
						constexpr Reg Value_pending = 0x1;
						constexpr Reg ValueMask_pending = 0x00000800;
						};
					namespace fSOF12 { // Field Description
						constexpr Reg Lsb = 12;
						constexpr Reg FieldMask = 0x00001000;
						constexpr Reg Value_notPending = 0x0;
						constexpr Reg ValueMask_notPending = 0x00000000;
						constexpr Reg Value_pending = 0x1;
						constexpr Reg ValueMask_pending = 0x00001000;
						};
					namespace fSOF13 { // Field Description
						constexpr Reg Lsb = 13;
						constexpr Reg FieldMask = 0x00002000;
						constexpr Reg Value_notPending = 0x0;
						constexpr Reg ValueMask_notPending = 0x00000000;
						constexpr Reg Value_pending = 0x1;
						constexpr Reg ValueMask_pending = 0x00002000;
						};
					namespace fSOF14 { // Field Description
						constexpr Reg Lsb = 14;
						constexpr Reg FieldMask = 0x00004000;
						constexpr Reg Value_notPending = 0x0;
						constexpr Reg ValueMask_notPending = 0x00000000;
						constexpr Reg Value_pending = 0x1;
						constexpr Reg ValueMask_pending = 0x00004000;
						};
					namespace fSOF15 { // Field Description
						constexpr Reg Lsb = 15;
						constexpr Reg FieldMask = 0x00008000;
						constexpr Reg Value_notPending = 0x0;
						constexpr Reg ValueMask_notPending = 0x00000000;
						constexpr Reg Value_pending = 0x1;
						constexpr Reg ValueMask_pending = 0x00008000;
						};
					};
				namespace rCFR { // Register description
					typedef uint32_t	Reg;
					constexpr Reg	valueAtReset = 0x00000000;
					constexpr Reg	noEffect = 0x00000000;
					constexpr Reg	clear = 0x00000001;
					namespace fCSOF0 { // Field Description
						constexpr Reg Lsb = 0;
						constexpr Reg FieldMask = 0x00000001;
						constexpr Reg Value_noEffect = 0x0;
						constexpr Reg ValueMask_noEffect = 0x00000000;
						constexpr Reg Value_clear = 0x1;
						constexpr Reg ValueMask_clear = 0x00000001;
						};
					namespace fCSOF1 { // Field Description
						constexpr Reg Lsb = 1;
						constexpr Reg FieldMask = 0x00000002;
						constexpr Reg Value_noEffect = 0x0;
						constexpr Reg ValueMask_noEffect = 0x00000000;
						constexpr Reg Value_clear = 0x1;
						constexpr Reg ValueMask_clear = 0x00000002;
						};
					namespace fCSOF2 { // Field Description
						constexpr Reg Lsb = 2;
						constexpr Reg FieldMask = 0x00000004;
						constexpr Reg Value_noEffect = 0x0;
						constexpr Reg ValueMask_noEffect = 0x00000000;
						constexpr Reg Value_clear = 0x1;
						constexpr Reg ValueMask_clear = 0x00000004;
						};
					namespace fCSOF3 { // Field Description
						constexpr Reg Lsb = 3;
						constexpr Reg FieldMask = 0x00000008;
						constexpr Reg Value_noEffect = 0x0;
						constexpr Reg ValueMask_noEffect = 0x00000000;
						constexpr Reg Value_clear = 0x1;
						constexpr Reg ValueMask_clear = 0x00000008;
						};
					namespace fCSOF4 { // Field Description
						constexpr Reg Lsb = 4;
						constexpr Reg FieldMask = 0x00000010;
						constexpr Reg Value_noEffect = 0x0;
						constexpr Reg ValueMask_noEffect = 0x00000000;
						constexpr Reg Value_clear = 0x1;
						constexpr Reg ValueMask_clear = 0x00000010;
						};
					namespace fCSOF5 { // Field Description
						constexpr Reg Lsb = 5;
						constexpr Reg FieldMask = 0x00000020;
						constexpr Reg Value_noEffect = 0x0;
						constexpr Reg ValueMask_noEffect = 0x00000000;
						constexpr Reg Value_clear = 0x1;
						constexpr Reg ValueMask_clear = 0x00000020;
						};
					namespace fCSOF6 { // Field Description
						constexpr Reg Lsb = 6;
						constexpr Reg FieldMask = 0x00000040;
						constexpr Reg Value_noEffect = 0x0;
						constexpr Reg ValueMask_noEffect = 0x00000000;
						constexpr Reg Value_clear = 0x1;
						constexpr Reg ValueMask_clear = 0x00000040;
						};
					namespace fCSOF7 { // Field Description
						constexpr Reg Lsb = 7;
						constexpr Reg FieldMask = 0x00000080;
						constexpr Reg Value_noEffect = 0x0;
						constexpr Reg ValueMask_noEffect = 0x00000000;
						constexpr Reg Value_clear = 0x1;
						constexpr Reg ValueMask_clear = 0x00000080;
						};
					namespace fCSOF8 { // Field Description
						constexpr Reg Lsb = 8;
						constexpr Reg FieldMask = 0x00000100;
						constexpr Reg Value_noEffect = 0x0;
						constexpr Reg ValueMask_noEffect = 0x00000000;
						constexpr Reg Value_clear = 0x1;
						constexpr Reg ValueMask_clear = 0x00000100;
						};
					namespace fCSOF9 { // Field Description
						constexpr Reg Lsb = 9;
						constexpr Reg FieldMask = 0x00000200;
						constexpr Reg Value_noEffect = 0x0;
						constexpr Reg ValueMask_noEffect = 0x00000000;
						constexpr Reg Value_clear = 0x1;
						constexpr Reg ValueMask_clear = 0x00000200;
						};
					namespace fCSOF10 { // Field Description
						constexpr Reg Lsb = 10;
						constexpr Reg FieldMask = 0x00000400;
						constexpr Reg Value_noEffect = 0x0;
						constexpr Reg ValueMask_noEffect = 0x00000000;
						constexpr Reg Value_clear = 0x1;
						constexpr Reg ValueMask_clear = 0x00000400;
						};
					namespace fCSOF11 { // Field Description
						constexpr Reg Lsb = 11;
						constexpr Reg FieldMask = 0x00000800;
						constexpr Reg Value_noEffect = 0x0;
						constexpr Reg ValueMask_noEffect = 0x00000000;
						constexpr Reg Value_clear = 0x1;
						constexpr Reg ValueMask_clear = 0x00000800;
						};
					namespace fCSOF12 { // Field Description
						constexpr Reg Lsb = 12;
						constexpr Reg FieldMask = 0x00001000;
						constexpr Reg Value_noEffect = 0x0;
						constexpr Reg ValueMask_noEffect = 0x00000000;
						constexpr Reg Value_clear = 0x1;
						constexpr Reg ValueMask_clear = 0x00001000;
						};
					namespace fCSOF13 { // Field Description
						constexpr Reg Lsb = 13;
						constexpr Reg FieldMask = 0x00002000;
						constexpr Reg Value_noEffect = 0x0;
						constexpr Reg ValueMask_noEffect = 0x00000000;
						constexpr Reg Value_clear = 0x1;
						constexpr Reg ValueMask_clear = 0x00002000;
						};
					namespace fCSOF14 { // Field Description
						constexpr Reg Lsb = 14;
						constexpr Reg FieldMask = 0x00004000;
						constexpr Reg Value_noEffect = 0x0;
						constexpr Reg ValueMask_noEffect = 0x00000000;
						constexpr Reg Value_clear = 0x1;
						constexpr Reg ValueMask_clear = 0x00004000;
						};
					namespace fCSOF15 { // Field Description
						constexpr Reg Lsb = 15;
						constexpr Reg FieldMask = 0x00008000;
						constexpr Reg Value_noEffect = 0x0;
						constexpr Reg ValueMask_noEffect = 0x00000000;
						constexpr Reg Value_clear = 0x1;
						constexpr Reg ValueMask_clear = 0x00008000;
						};
					};
				}
			}
		}
	}
#endif
