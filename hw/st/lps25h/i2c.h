/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

/* The ST LMS9DS1 module actually contains two diffent
	I2C modules.
	- Accelerometer/Gyroscope/Temperature
	- Magnetometer

	Each module has its own I2C addres.
 */

#ifndef _oscl_hw_st_lps25h_i2ch_
#define _oscl_hw_st_lps25h_i2ch_

#include <stdint.h>

/** */
namespace Oscl {
/** */
namespace HW {
/** */
namespace ST {
/** */
namespace LPS25H {
/** */
namespace I2C {

/**	This template variable evaluates to the I2C address of
	the device based on the state of the SA0 pin.

	The I2C address of the internal Accelerometer/Gyro/Temperature
	sensor block depends upon the state of the SDO_A/G pin.
	SAD[6:1]	= 0b11_0101 = 0x2E
	SAD[0]		= SA0
 */
template<bool sa0>
constexpr uint8_t	sensorAddress	= ((0x2E<<1) | (sa0 << 0));

/** This function returns the I2C address of the 
	sensor device based on the state of the SA0 pin.
 */
uint8_t	getSensorAddress(bool sa0) noexcept;

}
}
}
}
}

#endif
