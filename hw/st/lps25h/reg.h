#ifndef __oscl_hw_st_lps25h_regh__
#define __oscl_hw_st_lps25h_regh__
#include <stdint.h>
namespace Oscl { // Namespace description
	namespace HW { // Namespace description
		namespace ST { // Namespace description
			namespace LPS25H { // Namespace description
				enum {REF_P_XL = 8};
				enum {REF_P_L = 9};
				enum {REF_P_H = 10};
				enum {WHO_AM_I = 15};
				enum {RES_CONF = 16};
				enum {CTRL_REG1 = 32};
				enum {CTRL_REG2 = 33};
				enum {CTRL_REG3 = 34};
				enum {CTRL_REG4 = 35};
				enum {INT_CFG = 36};
				enum {INT_SOURCE = 37};
				enum {STATUS_REG = 39};
				enum {PRESS_OUT = 168};
				enum {PRESS_OUT_XL = 40};
				enum {PRESS_OUT_L = 41};
				enum {PRESS_OUT_H = 42};
				enum {TEMP_OUT = 171};
				enum {TEMP_OUT_L = 43};
				enum {TEMP_OUT_H = 44};
				enum {FIFO_CTRL = 46};
				enum {FIFO_STATUS = 47};
				enum {THS_P_L = 48};
				enum {THS_P_H = 49};
				enum {RPDS_L = 57};
				enum {RPDS_H = 58};
				namespace RefPXl { // Register description
					typedef uint8_t	Reg;
					namespace REFL { // Field Description
						enum {Lsb = 0};
						enum {FieldMask = 0x000000FF};
						enum {Value_Default = 0x0};
						enum {ValueMask_Default = 0x00000000};
						};
					};
				namespace RefPL { // Register description
					typedef uint8_t	Reg;
					namespace REFL { // Field Description
						enum {Lsb = 0};
						enum {FieldMask = 0x000000FF};
						enum {Value_Default = 0x0};
						enum {ValueMask_Default = 0x00000000};
						};
					};
				namespace RefPH { // Register description
					typedef uint8_t	Reg;
					namespace REFL { // Field Description
						enum {Lsb = 0};
						enum {FieldMask = 0x000000FF};
						enum {Value_Default = 0x0};
						enum {ValueMask_Default = 0x00000000};
						};
					};
				namespace WhoAmI { // Register description
					typedef uint8_t	Reg;
					namespace ID { // Field Description
						enum {Lsb = 0};
						enum {FieldMask = 0x000000FF};
						enum {Value_Default = 0xBD};
						enum {ValueMask_Default = 0x000000BD};
						};
					};
				namespace ResConf { // Register description
					typedef uint8_t	Reg;
					namespace AVGT { // Field Description
						enum {Lsb = 2};
						enum {FieldMask = 0x0000000C};
						enum {Value_Default = 0x1};
						enum {ValueMask_Default = 0x00000004};
						enum {Value_N8 = 0x0};
						enum {ValueMask_N8 = 0x00000000};
						enum {Value_N16 = 0x1};
						enum {ValueMask_N16 = 0x00000004};
						enum {Value_N32 = 0x2};
						enum {ValueMask_N32 = 0x00000008};
						enum {Value_N64 = 0x3};
						enum {ValueMask_N64 = 0x0000000C};
						};
					namespace AVGP { // Field Description
						enum {Lsb = 0};
						enum {FieldMask = 0x00000003};
						enum {Value_Default = 0x1};
						enum {ValueMask_Default = 0x00000001};
						enum {Value_N8 = 0x0};
						enum {ValueMask_N8 = 0x00000000};
						enum {Value_N32 = 0x1};
						enum {ValueMask_N32 = 0x00000001};
						enum {Value_N128 = 0x2};
						enum {ValueMask_N128 = 0x00000002};
						enum {Value_N512 = 0x3};
						enum {ValueMask_N512 = 0x00000003};
						};
					};
				namespace CtrlReg1 { // Register description
					typedef uint8_t	Reg;
					namespace PD { // Field Description
						enum {Lsb = 7};
						enum {FieldMask = 0x00000080};
						enum {Value_Default = 0x0};
						enum {ValueMask_Default = 0x00000000};
						enum {Value_PowerDown = 0x0};
						enum {ValueMask_PowerDown = 0x00000000};
						enum {Value_Active = 0x1};
						enum {ValueMask_Active = 0x00000080};
						};
					namespace ODR { // Field Description
						enum {Lsb = 4};
						enum {FieldMask = 0x00000070};
						enum {Value_Default = 0x0};
						enum {ValueMask_Default = 0x00000000};
						enum {Value_OneShot = 0x0};
						enum {ValueMask_OneShot = 0x00000000};
						enum {Value__1Hz = 0x1};
						enum {ValueMask__1Hz = 0x00000010};
						enum {Value__7Hz = 0x2};
						enum {ValueMask__7Hz = 0x00000020};
						enum {Value__12p5Hz = 0x3};
						enum {ValueMask__12p5Hz = 0x00000030};
						enum {Value__25Hz = 0x4};
						enum {ValueMask__25Hz = 0x00000040};
						};
					namespace DIFF_EN { // Field Description
						enum {Lsb = 3};
						enum {FieldMask = 0x00000008};
						enum {Value_Default = 0x0};
						enum {ValueMask_Default = 0x00000000};
						enum {Value_InterruptDisable = 0x0};
						enum {ValueMask_InterruptDisable = 0x00000000};
						enum {Value_InterruptEnable = 0x1};
						enum {ValueMask_InterruptEnable = 0x00000008};
						};
					namespace BDU { // Field Description
						enum {Lsb = 2};
						enum {FieldMask = 0x00000004};
						enum {Value_Default = 0x0};
						enum {ValueMask_Default = 0x00000000};
						enum {Value_Continuous = 0x0};
						enum {ValueMask_Continuous = 0x00000000};
						enum {Value_MsbAndLsbRead = 0x0};
						enum {ValueMask_MsbAndLsbRead = 0x00000000};
						};
					namespace RESET_AZ { // Field Description
						enum {Lsb = 1};
						enum {FieldMask = 0x00000002};
						enum {Value_Default = 0x0};
						enum {ValueMask_Default = 0x00000000};
						enum {Value_Reset = 0x1};
						enum {ValueMask_Reset = 0x00000002};
						};
					namespace SIM { // Field Description
						enum {Lsb = 0};
						enum {FieldMask = 0x00000001};
						enum {Value_Default = 0x0};
						enum {ValueMask_Default = 0x00000000};
						enum {Value_Spi4Wire = 0x0};
						enum {ValueMask_Spi4Wire = 0x00000000};
						enum {Value_Spi3Wire = 0x1};
						enum {ValueMask_Spi3Wire = 0x00000001};
						};
					};
				namespace CtrlReg2 { // Register description
					typedef uint8_t	Reg;
					namespace BOOT { // Field Description
						enum {Lsb = 7};
						enum {FieldMask = 0x00000080};
						enum {Value_Default = 0x0};
						enum {ValueMask_Default = 0x00000000};
						enum {Value_Normal = 0x0};
						enum {ValueMask_Normal = 0x00000000};
						enum {Value_RebootMemoryContent = 0x1};
						enum {ValueMask_RebootMemoryContent = 0x00000080};
						};
					namespace FIFO_EN { // Field Description
						enum {Lsb = 6};
						enum {FieldMask = 0x00000040};
						enum {Value_Default = 0x0};
						enum {ValueMask_Default = 0x00000000};
						enum {Value_Disable = 0x0};
						enum {ValueMask_Disable = 0x00000000};
						enum {Value_Enable = 0x1};
						enum {ValueMask_Enable = 0x00000040};
						};
					namespace WTM_EN { // Field Description
						enum {Lsb = 5};
						enum {FieldMask = 0x00000020};
						enum {Value_Default = 0x0};
						enum {ValueMask_Default = 0x00000000};
						enum {Value_Disable = 0x0};
						enum {ValueMask_Disable = 0x00000000};
						enum {Value_Enable = 0x1};
						enum {ValueMask_Enable = 0x00000020};
						};
					namespace FIFO_MEAN_DEC { // Field Description
						enum {Lsb = 4};
						enum {FieldMask = 0x00000010};
						enum {Value_Default = 0x0};
						enum {ValueMask_Default = 0x00000000};
						enum {Value_Disable = 0x0};
						enum {ValueMask_Disable = 0x00000000};
						enum {Value_Enable = 0x1};
						enum {ValueMask_Enable = 0x00000010};
						};
					namespace SWRESET { // Field Description
						enum {Lsb = 2};
						enum {FieldMask = 0x00000004};
						enum {Value_Default = 0x0};
						enum {ValueMask_Default = 0x00000000};
						enum {Value_Normal = 0x0};
						enum {ValueMask_Normal = 0x00000000};
						enum {Value_Reset = 0x1};
						enum {ValueMask_Reset = 0x00000004};
						};
					namespace AUTO_ZERO { // Field Description
						enum {Lsb = 1};
						enum {FieldMask = 0x00000002};
						enum {Value_Default = 0x0};
						enum {ValueMask_Default = 0x00000000};
						enum {Value_Normal = 0x0};
						enum {ValueMask_Normal = 0x00000000};
						enum {Value_Enable = 0x1};
						enum {ValueMask_Enable = 0x00000002};
						};
					namespace ONE_SHOT { // Field Description
						enum {Lsb = 0};
						enum {FieldMask = 0x00000001};
						enum {Value_Default = 0x0};
						enum {ValueMask_Default = 0x00000000};
						enum {Value_WaitingForStart = 0x0};
						enum {ValueMask_WaitingForStart = 0x00000000};
						enum {Value_Start = 0x1};
						enum {ValueMask_Start = 0x00000001};
						};
					};
				namespace CtrlReg3 { // Register description
					typedef uint8_t	Reg;
					namespace INT_H_L { // Field Description
						enum {Lsb = 7};
						enum {FieldMask = 0x00000080};
						enum {Value_Default = 0x0};
						enum {ValueMask_Default = 0x00000000};
						enum {Value_ActiveHigh = 0x0};
						enum {ValueMask_ActiveHigh = 0x00000000};
						enum {Value_ActiveLow = 0x1};
						enum {ValueMask_ActiveLow = 0x00000080};
						};
					namespace PP_OD { // Field Description
						enum {Lsb = 6};
						enum {FieldMask = 0x00000040};
						enum {Value_Default = 0x0};
						enum {ValueMask_Default = 0x00000000};
						enum {Value_PushPull = 0x0};
						enum {ValueMask_PushPull = 0x00000000};
						enum {Value_OpenDrain = 0x1};
						enum {ValueMask_OpenDrain = 0x00000040};
						};
					namespace INT1_S { // Field Description
						enum {Lsb = 0};
						enum {FieldMask = 0x00000003};
						enum {Value_Default = 0x0};
						enum {ValueMask_Default = 0x00000000};
						enum {Value_DataSignal = 0x0};
						enum {ValueMask_DataSignal = 0x00000000};
						enum {Value_PressureHigh = 0x1};
						enum {ValueMask_PressureHigh = 0x00000001};
						enum {Value_PressureLow = 0x2};
						enum {ValueMask_PressureLow = 0x00000002};
						enum {Value_PressureLowOrHigh = 0x3};
						enum {ValueMask_PressureLowOrHigh = 0x00000003};
						};
					};
				namespace CtrlReg4 { // Register description
					typedef uint8_t	Reg;
					namespace P1_EMPTY { // Field Description
						enum {Lsb = 3};
						enum {FieldMask = 0x00000008};
						enum {Value_Default = 0x0};
						enum {ValueMask_Default = 0x00000000};
						enum {Value_EmptySignalOnINT1 = 0x1};
						enum {ValueMask_EmptySignalOnINT1 = 0x00000008};
						};
					namespace P1_WTM { // Field Description
						enum {Lsb = 2};
						enum {FieldMask = 0x00000004};
						enum {Value_Default = 0x0};
						enum {ValueMask_Default = 0x00000000};
						enum {Value_WatermarkSignalOnINT1 = 0x1};
						enum {ValueMask_WatermarkSignalOnINT1 = 0x00000004};
						};
					namespace P1_Overrun { // Field Description
						enum {Lsb = 1};
						enum {FieldMask = 0x00000002};
						enum {Value_Default = 0x0};
						enum {ValueMask_Default = 0x00000000};
						enum {Value_OverrunSignalOnINT1 = 0x1};
						enum {ValueMask_OverrunSignalOnINT1 = 0x00000002};
						};
					namespace P1_DRDY { // Field Description
						enum {Lsb = 1};
						enum {FieldMask = 0x00000000};
						enum {Value_Default = 0x0};
						enum {ValueMask_Default = 0x00000000};
						enum {Value_DataReadySignalOnINT1 = 0x1};
						enum {ValueMask_DataReadySignalOnINT1 = 0x00000002};
						};
					};
				namespace InterruptCfg { // Register description
					typedef uint8_t	Reg;
					namespace LIR { // Field Description
						enum {Lsb = 2};
						enum {FieldMask = 0x00000004};
						enum {Value_Default = 0x0};
						enum {ValueMask_Default = 0x00000000};
						enum {Value_NotLatched = 0x0};
						enum {ValueMask_NotLatched = 0x00000000};
						enum {Value_Latched = 0x1};
						enum {ValueMask_Latched = 0x00000004};
						};
					namespace PL_E { // Field Description
						enum {Lsb = 1};
						enum {FieldMask = 0x00000002};
						enum {Value_Default = 0x0};
						enum {ValueMask_Default = 0x00000000};
						enum {Value_Disable = 0x0};
						enum {ValueMask_Disable = 0x00000000};
						enum {Value_Enable = 0x1};
						enum {ValueMask_Enable = 0x00000002};
						};
					namespace PH_E { // Field Description
						enum {Lsb = 0};
						enum {FieldMask = 0x00000001};
						enum {Value_Default = 0x0};
						enum {ValueMask_Default = 0x00000000};
						enum {Value_Disable = 0x0};
						enum {ValueMask_Disable = 0x00000000};
						enum {Value_Enable = 0x1};
						enum {ValueMask_Enable = 0x00000001};
						};
					};
				namespace IntSource { // Register description
					typedef uint8_t	Reg;
					namespace IA { // Field Description
						enum {Lsb = 2};
						enum {FieldMask = 0x00000004};
						enum {Value_NoInterrupt = 0x0};
						enum {ValueMask_NoInterrupt = 0x00000000};
						enum {Value_Pending = 0x1};
						enum {ValueMask_Pending = 0x00000004};
						};
					namespace PL { // Field Description
						enum {Lsb = 1};
						enum {FieldMask = 0x00000002};
						enum {Value_NoInterrupt = 0x0};
						enum {ValueMask_NoInterrupt = 0x00000000};
						enum {Value_Pending = 0x1};
						enum {ValueMask_Pending = 0x00000002};
						};
					namespace PH { // Field Description
						enum {Lsb = 0};
						enum {FieldMask = 0x00000001};
						enum {Value_NoInterrupt = 0x0};
						enum {ValueMask_NoInterrupt = 0x00000000};
						enum {Value_Pending = 0x1};
						enum {ValueMask_Pending = 0x00000001};
						};
					};
				namespace StatusReg { // Register description
					typedef uint8_t	Reg;
					namespace RES { // Field Description
						enum {Lsb = 6};
						enum {FieldMask = 0x000000C0};
						enum {Value_Default = 0x0};
						enum {ValueMask_Default = 0x00000000};
						};
					namespace P_OR { // Field Description
						enum {Lsb = 5};
						enum {FieldMask = 0x00000020};
						enum {Value_Default = 0x0};
						enum {ValueMask_Default = 0x00000000};
						enum {Value_Overrun = 0x1};
						enum {ValueMask_Overrun = 0x00000020};
						};
					namespace T_OR { // Field Description
						enum {Lsb = 4};
						enum {FieldMask = 0x00000010};
						enum {Value_Default = 0x0};
						enum {ValueMask_Default = 0x00000000};
						enum {Value_Overrun = 0x1};
						enum {ValueMask_Overrun = 0x00000010};
						};
					namespace P_DA { // Field Description
						enum {Lsb = 1};
						enum {FieldMask = 0x00000002};
						enum {Value_Default = 0x0};
						enum {ValueMask_Default = 0x00000000};
						enum {Value_DataAvailable = 0x1};
						enum {ValueMask_DataAvailable = 0x00000002};
						};
					namespace T_DA { // Field Description
						enum {Lsb = 0};
						enum {FieldMask = 0x00000001};
						enum {Value_Default = 0x0};
						enum {ValueMask_Default = 0x00000000};
						enum {Value_DataAvailable = 0x1};
						enum {ValueMask_DataAvailable = 0x00000001};
						};
					};
				namespace PressOutXL { // Register description
					typedef uint8_t	Reg;
					namespace POUT { // Field Description
						enum {Lsb = 0};
						enum {FieldMask = 0x000000FF};
						};
					};
				namespace PressOutL { // Register description
					typedef uint8_t	Reg;
					namespace POUT { // Field Description
						enum {Lsb = 0};
						enum {FieldMask = 0x000000FF};
						};
					};
				namespace PressOutH { // Register description
					typedef uint8_t	Reg;
					namespace POUT { // Field Description
						enum {Lsb = 0};
						enum {FieldMask = 0x000000FF};
						};
					};
				namespace TempOutL { // Register description
					typedef uint8_t	Reg;
					namespace TOUT { // Field Description
						enum {Lsb = 0};
						enum {FieldMask = 0x000000FF};
						};
					};
				namespace TempOutH { // Register description
					typedef uint8_t	Reg;
					namespace TOUT { // Field Description
						enum {Lsb = 0};
						enum {FieldMask = 0x000000FF};
						};
					};
				namespace FifoCtrl { // Register description
					typedef uint8_t	Reg;
					namespace F_MODE { // Field Description
						enum {Lsb = 5};
						enum {FieldMask = 0x000000E0};
						enum {Value_Default = 0x0};
						enum {ValueMask_Default = 0x00000000};
						enum {Value_Bypass = 0x0};
						enum {ValueMask_Bypass = 0x00000000};
						enum {Value_StopWhenFull = 0x1};
						enum {ValueMask_StopWhenFull = 0x00000020};
						enum {Value_StreamNewestInFIFO = 0x2};
						enum {ValueMask_StreamNewestInFIFO = 0x00000040};
						enum {Value_StreamThenFIFO = 0x3};
						enum {ValueMask_StreamThenFIFO = 0x00000060};
						enum {Value_BypassThenSTREAM = 0x4};
						enum {ValueMask_BypassThenSTREAM = 0x00000080};
						enum {Value_RunningAverage = 0x6};
						enum {ValueMask_RunningAverage = 0x000000C0};
						enum {Value_BypassThenFIFO = 0x7};
						enum {ValueMask_BypassThenFIFO = 0x000000E0};
						};
					namespace WTM_POINT { // Field Description
						enum {Lsb = 0};
						enum {FieldMask = 0x0000001F};
						enum {Value_Default = 0x0};
						enum {ValueMask_Default = 0x00000000};
						enum {Value_MovingAverage2Samples = 0x1};
						enum {ValueMask_MovingAverage2Samples = 0x00000001};
						enum {Value_MovingAverage4Samples = 0x3};
						enum {ValueMask_MovingAverage4Samples = 0x00000003};
						enum {Value_MovingAverage8Samples = 0x7};
						enum {ValueMask_MovingAverage8Samples = 0x00000007};
						enum {Value_MovingAverage16Samples = 0xF};
						enum {ValueMask_MovingAverage16Samples = 0x0000000F};
						enum {Value_MovingAverage32Samples = 0x1F};
						enum {ValueMask_MovingAverage32Samples = 0x0000001F};
						};
					};
				namespace FifoStatus { // Register description
					typedef uint8_t	Reg;
					namespace WTM_FIFO { // Field Description
						enum {Lsb = 7};
						enum {FieldMask = 0x00000080};
						enum {Value_Default = 0x0};
						enum {ValueMask_Default = 0x00000000};
						enum {Value_LowerThanWatermark = 0x0};
						enum {ValueMask_LowerThanWatermark = 0x00000000};
						enum {Value_EqualOrHigherThanWatermark = 0x1};
						enum {ValueMask_EqualOrHigherThanWatermark = 0x00000080};
						};
					namespace FULL_FIFO { // Field Description
						enum {Lsb = 6};
						enum {FieldMask = 0x00000040};
						enum {Value_Default = 0x0};
						enum {ValueMask_Default = 0x00000000};
						enum {Value_NotFull = 0x0};
						enum {ValueMask_NotFull = 0x00000000};
						enum {Value_Full = 0x1};
						enum {ValueMask_Full = 0x00000040};
						};
					namespace DIFF_POINT { // Field Description
						enum {Lsb = 0};
						enum {FieldMask = 0x0000001F};
						enum {Value_Default = 0x0};
						enum {ValueMask_Default = 0x00000000};
						};
					};
				namespace ThsPL { // Register description
					typedef uint8_t	Reg;
					namespace THS { // Field Description
						enum {Lsb = 0};
						enum {FieldMask = 0x000000FF};
						enum {Value_Default = 0x0};
						enum {ValueMask_Default = 0x00000000};
						};
					};
				namespace ThsPH { // Register description
					typedef uint8_t	Reg;
					namespace THS { // Field Description
						enum {Lsb = 0};
						enum {FieldMask = 0x000000FF};
						enum {Value_Default = 0x0};
						enum {ValueMask_Default = 0x00000000};
						};
					};
				namespace RpdsL { // Register description
					typedef uint8_t	Reg;
					namespace RPDS { // Field Description
						enum {Lsb = 0};
						enum {FieldMask = 0x000000FF};
						enum {Value_Default = 0x38};
						enum {ValueMask_Default = 0x00000038};
						};
					};
				namespace RpdsH { // Register description
					typedef uint8_t	Reg;
					namespace RPDS { // Field Description
						enum {Lsb = 0};
						enum {FieldMask = 0x000000FF};
						enum {Value_Default = 0x0};
						enum {ValueMask_Default = 0x00000000};
						};
					};
				}
			}
		}
	}
#endif
