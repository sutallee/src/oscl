/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

/* The ST LMS9DS1 module actually contains two diffent
	I2C modules.
	- Accelerometer/Gyroscope/Temperature
	- Magnetometer

	Each module has its own I2C addres.
 */

#ifndef __oscl_hw_st_lsm9ds1_i2ch_
#define __oscl_hw_st_lsm9ds1_i2ch_

#include <stdint.h>

/** */
namespace Oscl {
/** */
namespace HW {
/** */
namespace ST {
/** */
namespace LSM9DS1 {
/** */
namespace I2C {

/*	The LSM9DS1 is actually two independent devices
	in the same package.
	* The Accelerometer/Gyro
	* The Magnetometer

	The LSM9DS1 can be strapped to work in either
	SPI or I2C mode using pins CS_A/G (Chip-select Accelerometer/Gyro)
	and CS_M (Chip-select Magnetometer).

	This file assumes that the board containing the LSM9DS1
	has strapped the board for I2C operation.
	* CS_A/G is pulled LOW
	* CS_M is pulled LOW

	Additionally, the I2C address for the
	Accelerometer/Gyro and the Magnetometer can be strapped
	using the SDO_A/G and SDO_M pins respectively.

	The SDO_A/G determines:
	* The base address (SAD[0]) of the Accelerometer/Gyro device.

	The SDO__M determines:
	* The base address (SAD[1]) of the Magnetometer device.

	The LSB of the Magnetometer address is always zero.

	Therefore, there are two possible I2C addresses for the
	Accelerometer/Gyro and two possible addresses I2C addresses
	for the Magnetometer.

	Accelerometer/Gyro: 0x6A, 0x6B
	Magnetometer: 0x1C, 0x1E
 */

/**	This template variable evaluates to the I2C address of
	the Accelerometer/Gyro based on the state of the
	SDO_A/G pin.

	The I2C address of the internal Accelerometer/Gyro/Temperature
	sensor block depends upon the state of the SDO_A/G pin.
	SAD[6:1]	= 0b11_0101 = 0x35
	SAD[0]		= SDO_A/G
 */
template<bool sdo_ag>
constexpr uint8_t	accelerometerGyroAddress	= ((0x35<<1) | (sdo_ag << 0));

/**	This template variable evaluates to the I2C address of
	the Magnetometer based on the state of the SDO_A/G
	and SDO_M pins.
	The I2C address of the internal Magnetometer sensor block depends
	on the state of both the SDO_A/G and SDO_M pins.
	SAD[6:2] = 0b0_0111 = 0x07
	SAD[1] = SDO_M
	SAD[0] = 0
 */
template<bool sdo_m>
constexpr uint8_t	magnetometerAddress	= (uint8_t)((0x07<<2) | (sdo_m << 1) | (0 << 0));

/** This function returns the I2C address of the Accelerometer/Gyro
	device based on the state of the SDO_A/G pin.
 */
uint8_t	getAccelerometerGyroAddress(bool sdo_ag) noexcept;

/** This function returns the I2C address of the Magnetometer
	device based on the state of the SDO_A/G and SDO_M pins.
 */
uint8_t	getMagnetometerAddress(bool sdo_m) noexcept;

}
}
}
}
}

#endif
