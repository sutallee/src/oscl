/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

/* The ST LMS9DS1 module actually contains two diffent
	I2C modules.
	- Accelerometer/Gyroscope/Temperature
	- Magnetometer

	Each module has its own I2C addres.
 */

#include "i2c.h"

using namespace Oscl::HW::ST::LSM9DS1::I2C;

/** Magnetometer address based on the
	state of both SDO_M.
 */
struct Sdo_m {
	uint8_t	address;
	};

/** Accelerometer address based on the state of SDO_A/G.
 */
struct Sdo_ag {
	uint8_t	address;
	};

static constexpr Sdo_ag	agAddressTable[2] = {
		{
		// SDO_A/G = 0
		.address	= accelerometerGyroAddress<false>,
		},
		{
		// SDO_A/G = 1
		.address	= accelerometerGyroAddress<true>,
		}
	};

static constexpr Sdo_m	mAddressTable[2] = {
		{
			// SDO_M = 0, 0b0001_1100
			.address	= magnetometerAddress<false>
		},
		{
			// SDO_M = 1, 0b0001_1110
			.address	= magnetometerAddress<true>
		}
	};

uint8_t	Oscl::HW::ST::LSM9DS1::I2C::getAccelerometerGyroAddress(bool cs_ag) noexcept{
	const unsigned	agIndex	= cs_ag?1:0;

	return agAddressTable[agIndex].address;
	}

uint8_t	Oscl::HW::ST::LSM9DS1::I2C::getMagnetometerAddress(bool cs_m) noexcept {
	const unsigned	magnetometerIndex	= cs_m?1:0;

	return mAddressTable[magnetometerIndex].address;
	}

