#ifndef __oscl_hw_st_lsm9ds1_agh__
#define __oscl_hw_st_lsm9ds1_agh__
#include <stdint.h>
namespace Oscl { // Namespace description
	namespace HW { // Namespace description
		namespace ST { // Namespace description
			namespace LSM9DS1 { // Namespace description
				namespace AccelGyro { // Namespace description
					enum {ACT_THS = 4};
					enum {ACT_DUR = 5};
					enum {INT_GEN_CFG_XL = 6};
					enum {INT_GEN_THS_X_XL = 7};
					enum {INT_GEN_THS_Y_XL = 8};
					enum {INT_GEN_THS_Z_XL = 9};
					enum {INT_GEN_DUR_XL = 10};
					enum {REFERENCE_G = 11};
					enum {INT1_CTRL = 12};
					enum {INT2_CTRL = 13};
					enum {WHO_AM_I = 15};
					enum {CTRL_REG1_G = 16};
					enum {CTRL_REG2_G = 17};
					enum {CTRL_REG3_G = 18};
					enum {ORIENT_CFG_G = 19};
					enum {INT_GEN_SRC_G = 20};
					enum {OUT_TEMP_L = 21};
					enum {OUT_TEMP_H = 22};
					enum {STATUS_REG = 23};
					enum {OUT_X_L_G = 24};
					enum {OUT_X_H_G = 25};
					enum {OUT_Y_L_G = 26};
					enum {OUT_Y_H_G = 27};
					enum {OUT_Z_L_G = 28};
					enum {OUT_Z_H_G = 29};
					enum {CTRL_REG4 = 30};
					enum {CTRL_REG5_XL = 31};
					enum {CTRL_REG6_XL = 32};
					enum {CTRL_REG7_XL = 33};
					enum {CTRL_REG8 = 34};
					enum {CTRL_REG9 = 35};
					enum {CTRL_REG10 = 36};
					enum {INT_GEN_SRC_XL = 38};
					enum {STATUS_REG_2 = 39};
					enum {OUT_X_L_XL = 40};
					enum {OUT_X_H_XL = 41};
					enum {OUT_Y_L_XL = 42};
					enum {OUT_Y_H_XL = 43};
					enum {OUT_Z_L_XL = 44};
					enum {OUT_Z_H_XL = 45};
					enum {FIFO_CTRL = 46};
					enum {FIFO_SRC = 47};
					enum {INT_GEN_CFG_G = 48};
					enum {INT_GEN_THS_XH_G = 49};
					enum {INT_GEN_THS_XL_G = 50};
					enum {INT_GEN_THS_YH_G = 51};
					enum {INT_GEN_THS_YL_G = 52};
					enum {INT_GEN_THS_ZH_G = 53};
					enum {INT_GEN_THS_ZL_G = 54};
					enum {INT_GEN_DUR_G = 55};
					namespace ActThs { // Register description
						typedef uint8_t	Reg;
						namespace SLEEP_ON_INACT_EN { // Field Description
							enum {Lsb = 7};
							enum {FieldMask = 0x00000080};
							enum {Value_GyroPowerDown = 0x0};
							enum {ValueMask_GyroPowerDown = 0x00000000};
							enum {Value_GyroInSleep = 0x1};
							enum {ValueMask_GyroInSleep = 0x00000080};
							};
						namespace InactivityThreshold { // Field Description
							enum {Lsb = 0};
							enum {FieldMask = 0x0000007F};
							enum {Value_Default = 0x0};
							enum {ValueMask_Default = 0x00000000};
							};
						};
					namespace ActDur { // Register description
						typedef uint8_t	Reg;
						namespace SLEEP_ON_INACT_EN { // Field Description
							enum {Lsb = 7};
							enum {FieldMask = 0x00000080};
							enum {Value_GyroPowerDown = 0x0};
							enum {ValueMask_GyroPowerDown = 0x00000000};
							enum {Value_GyroInSleep = 0x1};
							enum {ValueMask_GyroInSleep = 0x00000080};
							};
						};
					namespace IntGenCfgXl { // Register description
						typedef uint8_t	Reg;
						namespace AOI_XL { // Field Description
							enum {Lsb = 7};
							enum {FieldMask = 0x00000080};
							enum {Value_OR = 0x0};
							enum {ValueMask_OR = 0x00000000};
							enum {Value_AND = 0x1};
							enum {ValueMask_AND = 0x00000080};
							};
						namespace _6D { // Field Description
							enum {Lsb = 6};
							enum {FieldMask = 0x00000040};
							enum {Value_Disable = 0x0};
							enum {ValueMask_Disable = 0x00000000};
							enum {Value_Enable = 0x1};
							enum {ValueMask_Enable = 0x00000040};
							};
						namespace ZHIE_XL { // Field Description
							enum {Lsb = 5};
							enum {FieldMask = 0x00000020};
							enum {Value_DisableIrq = 0x0};
							enum {ValueMask_DisableIrq = 0x00000000};
							enum {Value_EnableIrq = 0x1};
							enum {ValueMask_EnableIrq = 0x00000020};
							};
						namespace ZLIE_XL { // Field Description
							enum {Lsb = 4};
							enum {FieldMask = 0x00000010};
							enum {Value_DisableIrq = 0x0};
							enum {ValueMask_DisableIrq = 0x00000000};
							enum {Value_EnableIrq = 0x1};
							enum {ValueMask_EnableIrq = 0x00000010};
							};
						namespace YHIE_XL { // Field Description
							enum {Lsb = 3};
							enum {FieldMask = 0x00000008};
							enum {Value_DisableIrq = 0x0};
							enum {ValueMask_DisableIrq = 0x00000000};
							enum {Value_EnableIrq = 0x1};
							enum {ValueMask_EnableIrq = 0x00000008};
							};
						namespace YLIE_XL { // Field Description
							enum {Lsb = 2};
							enum {FieldMask = 0x00000004};
							enum {Value_DisableIrq = 0x0};
							enum {ValueMask_DisableIrq = 0x00000000};
							enum {Value_EnableIrq = 0x1};
							enum {ValueMask_EnableIrq = 0x00000004};
							};
						namespace XHIE_XL { // Field Description
							enum {Lsb = 1};
							enum {FieldMask = 0x00000002};
							enum {Value_DisableIrq = 0x0};
							enum {ValueMask_DisableIrq = 0x00000000};
							enum {Value_EnableIrq = 0x1};
							enum {ValueMask_EnableIrq = 0x00000002};
							};
						namespace XLIE_XL { // Field Description
							enum {Lsb = 0};
							enum {FieldMask = 0x00000001};
							enum {Value_DisableIrq = 0x0};
							enum {ValueMask_DisableIrq = 0x00000000};
							enum {Value_EnableIrq = 0x1};
							enum {ValueMask_EnableIrq = 0x00000001};
							};
						};
					namespace IntGenThsXXl { // Register description
						typedef uint8_t	Reg;
						namespace THS_XL { // Field Description
							enum {Lsb = 0};
							enum {FieldMask = 0x000000FF};
							};
						};
					namespace IntGenThsYXl { // Register description
						typedef uint8_t	Reg;
						namespace THS_YL { // Field Description
							enum {Lsb = 0};
							enum {FieldMask = 0x000000FF};
							};
						};
					namespace IntGenThsZXl { // Register description
						typedef uint8_t	Reg;
						namespace THS_ZL { // Field Description
							enum {Lsb = 0};
							enum {FieldMask = 0x000000FF};
							};
						};
					namespace IntGenDurXl { // Register description
						typedef uint8_t	Reg;
						namespace WAIT_XL { // Field Description
							enum {Lsb = 7};
							enum {FieldMask = 0x00000080};
							enum {Value_Default = 0x0};
							enum {ValueMask_Default = 0x00000000};
							enum {Value_Disable = 0x0};
							enum {ValueMask_Disable = 0x00000000};
							enum {Value_Enable = 0x1};
							enum {ValueMask_Enable = 0x00000080};
							};
						namespace DUR_XL { // Field Description
							enum {Lsb = 0};
							enum {FieldMask = 0x0000007F};
							enum {Value_DefaultSamples = 0x0};
							enum {ValueMask_DefaultSamples = 0x00000000};
							enum {Value_MaxSamples = 0x7F};
							enum {ValueMask_MaxSamples = 0x0000007F};
							};
						};
					namespace ReferenceG { // Register description
						typedef uint8_t	Reg;
						namespace REF_G { // Field Description
							enum {Lsb = 0};
							enum {FieldMask = 0x000000FF};
							enum {Value_Default = 0x0};
							enum {ValueMask_Default = 0x00000000};
							enum {Value_MaxValue = 0xFF};
							enum {ValueMask_MaxValue = 0x000000FF};
							};
						};
					namespace Int1Ctrl { // Register description
						typedef uint8_t	Reg;
						namespace INT1_IG_G { // Field Description
							enum {Lsb = 7};
							enum {FieldMask = 0x00000080};
							enum {Value_Default = 0x0};
							enum {ValueMask_Default = 0x00000000};
							enum {Value_Disable = 0x0};
							enum {ValueMask_Disable = 0x00000000};
							enum {Value_Enable = 0x1};
							enum {ValueMask_Enable = 0x00000080};
							};
						namespace INT1_IG_XL { // Field Description
							enum {Lsb = 6};
							enum {FieldMask = 0x00000040};
							enum {Value_Default = 0x0};
							enum {ValueMask_Default = 0x00000000};
							enum {Value_Disable = 0x0};
							enum {ValueMask_Disable = 0x00000000};
							enum {Value_Enable = 0x1};
							enum {ValueMask_Enable = 0x00000040};
							};
						namespace INT1_FSS5 { // Field Description
							enum {Lsb = 5};
							enum {FieldMask = 0x00000020};
							enum {Value_Default = 0x0};
							enum {ValueMask_Default = 0x00000000};
							enum {Value_Disable = 0x0};
							enum {ValueMask_Disable = 0x00000000};
							enum {Value_Enable = 0x1};
							enum {ValueMask_Enable = 0x00000020};
							};
						namespace INT1_OVR { // Field Description
							enum {Lsb = 4};
							enum {FieldMask = 0x00000010};
							enum {Value_Default = 0x0};
							enum {ValueMask_Default = 0x00000000};
							enum {Value_Disable = 0x0};
							enum {ValueMask_Disable = 0x00000000};
							enum {Value_Enable = 0x1};
							enum {ValueMask_Enable = 0x00000010};
							};
						namespace INT1_FTH { // Field Description
							enum {Lsb = 3};
							enum {FieldMask = 0x00000008};
							enum {Value_Default = 0x0};
							enum {ValueMask_Default = 0x00000000};
							enum {Value_Disable = 0x0};
							enum {ValueMask_Disable = 0x00000000};
							enum {Value_Enable = 0x1};
							enum {ValueMask_Enable = 0x00000008};
							};
						namespace INT1__Boot { // Field Description
							enum {Lsb = 2};
							enum {FieldMask = 0x00000004};
							enum {Value_Default = 0x0};
							enum {ValueMask_Default = 0x00000000};
							enum {Value_Disable = 0x0};
							enum {ValueMask_Disable = 0x00000000};
							enum {Value_Enable = 0x1};
							enum {ValueMask_Enable = 0x00000004};
							};
						namespace INT1__DRDY_G { // Field Description
							enum {Lsb = 1};
							enum {FieldMask = 0x00000002};
							enum {Value_Default = 0x0};
							enum {ValueMask_Default = 0x00000000};
							enum {Value_Disable = 0x0};
							enum {ValueMask_Disable = 0x00000000};
							enum {Value_Enable = 0x1};
							enum {ValueMask_Enable = 0x00000002};
							};
						namespace INT1__DRDY_XL { // Field Description
							enum {Lsb = 0};
							enum {FieldMask = 0x00000001};
							enum {Value_Default = 0x0};
							enum {ValueMask_Default = 0x00000000};
							enum {Value_Disable = 0x0};
							enum {ValueMask_Disable = 0x00000000};
							enum {Value_Enable = 0x1};
							enum {ValueMask_Enable = 0x00000001};
							};
						};
					namespace Int2Ctrl { // Register description
						typedef uint8_t	Reg;
						namespace INT2_INACT { // Field Description
							enum {Lsb = 7};
							enum {FieldMask = 0x00000080};
							enum {Value_Default = 0x0};
							enum {ValueMask_Default = 0x00000000};
							enum {Value_Pending = 0x1};
							enum {ValueMask_Pending = 0x00000080};
							};
						namespace INT2_FSS5 { // Field Description
							enum {Lsb = 5};
							enum {FieldMask = 0x00000020};
							enum {Value_Default = 0x0};
							enum {ValueMask_Default = 0x00000000};
							enum {Value_Disable = 0x0};
							enum {ValueMask_Disable = 0x00000000};
							enum {Value_Enable = 0x1};
							enum {ValueMask_Enable = 0x00000020};
							};
						namespace INT2_OVR { // Field Description
							enum {Lsb = 4};
							enum {FieldMask = 0x00000010};
							enum {Value_Default = 0x0};
							enum {ValueMask_Default = 0x00000000};
							enum {Value_Disable = 0x0};
							enum {ValueMask_Disable = 0x00000000};
							enum {Value_Enable = 0x1};
							enum {ValueMask_Enable = 0x00000010};
							};
						namespace INT2_FTH { // Field Description
							enum {Lsb = 3};
							enum {FieldMask = 0x00000008};
							enum {Value_Default = 0x0};
							enum {ValueMask_Default = 0x00000000};
							enum {Value_Disable = 0x0};
							enum {ValueMask_Disable = 0x00000000};
							enum {Value_Enable = 0x1};
							enum {ValueMask_Enable = 0x00000008};
							};
						namespace INT2_TEMP { // Field Description
							enum {Lsb = 2};
							enum {FieldMask = 0x00000004};
							enum {Value_Default = 0x0};
							enum {ValueMask_Default = 0x00000000};
							enum {Value_Disable = 0x0};
							enum {ValueMask_Disable = 0x00000000};
							enum {Value_Enable = 0x1};
							enum {ValueMask_Enable = 0x00000004};
							};
						namespace INT2_DRDY_G { // Field Description
							enum {Lsb = 1};
							enum {FieldMask = 0x00000002};
							enum {Value_Default = 0x0};
							enum {ValueMask_Default = 0x00000000};
							enum {Value_Disable = 0x0};
							enum {ValueMask_Disable = 0x00000000};
							enum {Value_Enable = 0x1};
							enum {ValueMask_Enable = 0x00000002};
							};
						namespace INT2_DRDY_XL { // Field Description
							enum {Lsb = 0};
							enum {FieldMask = 0x00000001};
							enum {Value_Default = 0x0};
							enum {ValueMask_Default = 0x00000000};
							enum {Value_Disable = 0x0};
							enum {ValueMask_Disable = 0x00000000};
							enum {Value_Enable = 0x1};
							enum {ValueMask_Enable = 0x00000001};
							};
						};
					namespace WhoAmI { // Register description
						typedef uint8_t	Reg;
						namespace ID { // Field Description
							enum {Lsb = 0};
							enum {FieldMask = 0x000000FF};
							enum {Value_AccelerometerGyroID = 0x68};
							enum {ValueMask_AccelerometerGyroID = 0x00000068};
							};
						};
					namespace CtrlReg1G { // Register description
						typedef uint8_t	Reg;
						enum {PowerDown = 0};
						enum {Cutoff5Hz = 0};
						namespace ODR_G { // Field Description
							enum {Lsb = 5};
							enum {FieldMask = 0x000000E0};
							enum {Value_Default = 0x0};
							enum {ValueMask_Default = 0x00000000};
							enum {Value_PowerDown = 0x0};
							enum {ValueMask_PowerDown = 0x00000000};
							enum {Value__14p9Hz_Cutoff5Hz = 0x1};
							enum {ValueMask__14p9Hz_Cutoff5Hz = 0x00000020};
							enum {Value__59p5Hz_Cutoff19Hz = 0x2};
							enum {ValueMask__59p5Hz_Cutoff19Hz = 0x00000040};
							enum {Value__119Hz_Cutoff38Hz = 0x3};
							enum {ValueMask__119Hz_Cutoff38Hz = 0x00000060};
							enum {Value__238Hz_Cutoff76Hz = 0x4};
							enum {ValueMask__238Hz_Cutoff76Hz = 0x00000080};
							enum {Value__476Hz_Cutoff100Hz = 0x5};
							enum {ValueMask__476Hz_Cutoff100Hz = 0x000000A0};
							};
						namespace FS_G { // Field Description
							enum {Lsb = 3};
							enum {FieldMask = 0x00000018};
							enum {Value_Default = 0x0};
							enum {ValueMask_Default = 0x00000000};
							enum {Value__245dps = 0x0};
							enum {ValueMask__245dps = 0x00000000};
							enum {Value__500dps = 0x1};
							enum {ValueMask__500dps = 0x00000008};
							enum {Value__2000dps = 0x3};
							enum {ValueMask__2000dps = 0x00000018};
							};
						namespace BW_G { // Field Description
							enum {Lsb = 0};
							enum {FieldMask = 0x00000003};
							enum {Value_Default = 0x0};
							enum {ValueMask_Default = 0x00000000};
							};
						};
					namespace CtrlReg2G { // Register description
						typedef uint8_t	Reg;
						namespace INT_SEL { // Field Description
							enum {Lsb = 2};
							enum {FieldMask = 0x0000000C};
							enum {Value_Default = 0x0};
							enum {ValueMask_Default = 0x00000000};
							enum {Value_LPF1 = 0x0};
							enum {ValueMask_LPF1 = 0x00000000};
							enum {Value_HPF1 = 0x1};
							enum {ValueMask_HPF1 = 0x00000004};
							enum {Value_LPF2 = 0x2};
							enum {ValueMask_LPF2 = 0x00000008};
							};
						namespace OUT_SEL { // Field Description
							enum {Lsb = 0};
							enum {FieldMask = 0x00000003};
							enum {Value_Default = 0x0};
							enum {ValueMask_Default = 0x00000000};
							enum {Value_LPF1 = 0x0};
							enum {ValueMask_LPF1 = 0x00000000};
							enum {Value_HPF1 = 0x1};
							enum {ValueMask_HPF1 = 0x00000001};
							enum {Value_LPF2 = 0x2};
							enum {ValueMask_LPF2 = 0x00000002};
							};
						};
					namespace CtrlReg3G { // Register description
						typedef uint8_t	Reg;
						namespace LP_mode { // Field Description
							enum {Lsb = 7};
							enum {FieldMask = 0x00000080};
							enum {Value_Default = 0x0};
							enum {ValueMask_Default = 0x00000000};
							enum {Value_Disable = 0x0};
							enum {ValueMask_Disable = 0x00000000};
							enum {Value_Enable = 0x1};
							enum {ValueMask_Enable = 0x00000080};
							};
						namespace HP_EN { // Field Description
							enum {Lsb = 6};
							enum {FieldMask = 0x00000040};
							enum {Value_Default = 0x0};
							enum {ValueMask_Default = 0x00000000};
							enum {Value_Disable = 0x0};
							enum {ValueMask_Disable = 0x00000000};
							enum {Value_Enable = 0x1};
							enum {ValueMask_Enable = 0x00000040};
							};
						namespace HPCF_G { // Field Description
							enum {Lsb = 0};
							enum {FieldMask = 0x0000000F};
							enum {Value_Default = 0x0};
							enum {ValueMask_Default = 0x00000000};
							};
						};
					namespace OrientCfgG { // Register description
						typedef uint8_t	Reg;
						namespace SignX_G { // Field Description
							enum {Lsb = 5};
							enum {FieldMask = 0x00000020};
							enum {Value_Default = 0x0};
							enum {ValueMask_Default = 0x00000000};
							enum {Value_Positive = 0x0};
							enum {ValueMask_Positive = 0x00000000};
							enum {Value_Negative = 0x1};
							enum {ValueMask_Negative = 0x00000020};
							};
						namespace SignY_G { // Field Description
							enum {Lsb = 4};
							enum {FieldMask = 0x00000010};
							enum {Value_Default = 0x0};
							enum {ValueMask_Default = 0x00000000};
							enum {Value_Positive = 0x0};
							enum {ValueMask_Positive = 0x00000000};
							enum {Value_Negative = 0x1};
							enum {ValueMask_Negative = 0x00000010};
							};
						namespace SignZ_G { // Field Description
							enum {Lsb = 3};
							enum {FieldMask = 0x00000008};
							enum {Value_Default = 0x0};
							enum {ValueMask_Default = 0x00000000};
							enum {Value_Positive = 0x0};
							enum {ValueMask_Positive = 0x00000000};
							enum {Value_Negative = 0x1};
							enum {ValueMask_Negative = 0x00000008};
							};
						namespace Orient { // Field Description
							enum {Lsb = 0};
							enum {FieldMask = 0x00000007};
							enum {Value_Default = 0x0};
							enum {ValueMask_Default = 0x00000000};
							};
						};
					namespace IntGenSrcG { // Register description
						typedef uint8_t	Reg;
						namespace IA_G { // Field Description
							enum {Lsb = 6};
							enum {FieldMask = 0x00000040};
							enum {Value_Default = 0x0};
							enum {ValueMask_Default = 0x00000000};
							enum {Value_None = 0x0};
							enum {ValueMask_None = 0x00000000};
							enum {Value_Pending = 0x1};
							enum {ValueMask_Pending = 0x00000040};
							};
						namespace ZH_G { // Field Description
							enum {Lsb = 5};
							enum {FieldMask = 0x00000020};
							enum {Value_Default = 0x0};
							enum {ValueMask_Default = 0x00000000};
							enum {Value_None = 0x0};
							enum {ValueMask_None = 0x00000000};
							enum {Value_Pending = 0x1};
							enum {ValueMask_Pending = 0x00000020};
							};
						namespace ZL_G { // Field Description
							enum {Lsb = 4};
							enum {FieldMask = 0x00000010};
							enum {Value_Default = 0x0};
							enum {ValueMask_Default = 0x00000000};
							enum {Value_None = 0x0};
							enum {ValueMask_None = 0x00000000};
							enum {Value_Pending = 0x1};
							enum {ValueMask_Pending = 0x00000010};
							};
						namespace YH_G { // Field Description
							enum {Lsb = 3};
							enum {FieldMask = 0x00000008};
							enum {Value_Default = 0x0};
							enum {ValueMask_Default = 0x00000000};
							enum {Value_None = 0x0};
							enum {ValueMask_None = 0x00000000};
							enum {Value_Pending = 0x1};
							enum {ValueMask_Pending = 0x00000008};
							};
						namespace YL_G { // Field Description
							enum {Lsb = 2};
							enum {FieldMask = 0x00000004};
							enum {Value_Default = 0x0};
							enum {ValueMask_Default = 0x00000000};
							enum {Value_None = 0x0};
							enum {ValueMask_None = 0x00000000};
							enum {Value_Pending = 0x1};
							enum {ValueMask_Pending = 0x00000004};
							};
						namespace XH_G { // Field Description
							enum {Lsb = 1};
							enum {FieldMask = 0x00000002};
							enum {Value_Default = 0x0};
							enum {ValueMask_Default = 0x00000000};
							enum {Value_None = 0x0};
							enum {ValueMask_None = 0x00000000};
							enum {Value_Pending = 0x1};
							enum {ValueMask_Pending = 0x00000002};
							};
						namespace XL_G { // Field Description
							enum {Lsb = 0};
							enum {FieldMask = 0x00000001};
							enum {Value_Default = 0x0};
							enum {ValueMask_Default = 0x00000000};
							enum {Value_None = 0x0};
							enum {ValueMask_None = 0x00000000};
							enum {Value_Pending = 0x1};
							enum {ValueMask_Pending = 0x00000001};
							};
						};
					namespace OutTempL { // Register description
						typedef uint8_t	Reg;
						namespace Temp { // Field Description
							enum {Lsb = 0};
							enum {FieldMask = 0x000000FF};
							};
						};
					namespace OutTempH { // Register description
						typedef uint8_t	Reg;
						namespace Temp { // Field Description
							enum {Lsb = 0};
							enum {FieldMask = 0x000000FF};
							};
						};
					namespace StatusReg { // Register description
						typedef uint8_t	Reg;
						namespace IG_XL { // Field Description
							enum {Lsb = 6};
							enum {FieldMask = 0x00000040};
							enum {Value_Default = 0x0};
							enum {ValueMask_Default = 0x00000000};
							enum {Value_None = 0x0};
							enum {ValueMask_None = 0x00000000};
							enum {Value_Pending = 0x1};
							enum {ValueMask_Pending = 0x00000040};
							};
						namespace IG_G { // Field Description
							enum {Lsb = 5};
							enum {FieldMask = 0x00000020};
							enum {Value_Default = 0x0};
							enum {ValueMask_Default = 0x00000000};
							enum {Value_None = 0x0};
							enum {ValueMask_None = 0x00000000};
							enum {Value_Pending = 0x1};
							enum {ValueMask_Pending = 0x00000020};
							};
						namespace INACT { // Field Description
							enum {Lsb = 4};
							enum {FieldMask = 0x00000010};
							enum {Value_Default = 0x0};
							enum {ValueMask_Default = 0x00000000};
							enum {Value_None = 0x0};
							enum {ValueMask_None = 0x00000000};
							enum {Value_Pending = 0x1};
							enum {ValueMask_Pending = 0x00000010};
							};
						namespace BOOT_STATUS { // Field Description
							enum {Lsb = 3};
							enum {FieldMask = 0x00000008};
							enum {Value_Default = 0x0};
							enum {ValueMask_Default = 0x00000000};
							enum {Value_NoBootRunning = 0x0};
							enum {ValueMask_NoBootRunning = 0x00000000};
							enum {Value_BootRunning = 0x1};
							enum {ValueMask_BootRunning = 0x00000008};
							};
						namespace TDA { // Field Description
							enum {Lsb = 2};
							enum {FieldMask = 0x00000004};
							enum {Value_Default = 0x0};
							enum {ValueMask_Default = 0x00000000};
							enum {Value_NotYetAvailable = 0x0};
							enum {ValueMask_NotYetAvailable = 0x00000000};
							enum {Value_NewDataAvailable = 0x1};
							enum {ValueMask_NewDataAvailable = 0x00000004};
							};
						namespace GDA { // Field Description
							enum {Lsb = 1};
							enum {FieldMask = 0x00000002};
							enum {Value_Default = 0x0};
							enum {ValueMask_Default = 0x00000000};
							enum {Value_NotYetAvailable = 0x0};
							enum {ValueMask_NotYetAvailable = 0x00000000};
							enum {Value_NewDataAvailable = 0x1};
							enum {ValueMask_NewDataAvailable = 0x00000002};
							};
						namespace XLDA { // Field Description
							enum {Lsb = 0};
							enum {FieldMask = 0x00000001};
							enum {Value_Default = 0x0};
							enum {ValueMask_Default = 0x00000000};
							enum {Value_NotYetAvailable = 0x0};
							enum {ValueMask_NotYetAvailable = 0x00000000};
							enum {Value_NewDataAvailable = 0x1};
							enum {ValueMask_NewDataAvailable = 0x00000001};
							};
						};
					namespace OutXG0 { // Register description
						typedef uint8_t	Reg;
						namespace Rate { // Field Description
							enum {Lsb = 0};
							enum {FieldMask = 0x000000FF};
							};
						};
					namespace OutXG1 { // Register description
						typedef uint8_t	Reg;
						namespace Rate { // Field Description
							enum {Lsb = 0};
							enum {FieldMask = 0x000000FF};
							};
						};
					namespace OutYG0 { // Register description
						typedef uint8_t	Reg;
						namespace Rate { // Field Description
							enum {Lsb = 0};
							enum {FieldMask = 0x000000FF};
							};
						};
					namespace OutYG1 { // Register description
						typedef uint8_t	Reg;
						namespace Rate { // Field Description
							enum {Lsb = 0};
							enum {FieldMask = 0x000000FF};
							};
						};
					namespace OutZG0 { // Register description
						typedef uint8_t	Reg;
						namespace Rate { // Field Description
							enum {Lsb = 0};
							enum {FieldMask = 0x000000FF};
							};
						};
					namespace OutZG1 { // Register description
						typedef uint8_t	Reg;
						namespace Rate { // Field Description
							enum {Lsb = 0};
							enum {FieldMask = 0x000000FF};
							};
						};
					namespace CtrlReg4 { // Register description
						typedef uint8_t	Reg;
						namespace Zen_G { // Field Description
							enum {Lsb = 5};
							enum {FieldMask = 0x00000020};
							enum {Value_Default = 0x1};
							enum {ValueMask_Default = 0x00000020};
							enum {Value_Disable = 0x0};
							enum {ValueMask_Disable = 0x00000000};
							enum {Value_Enable = 0x1};
							enum {ValueMask_Enable = 0x00000020};
							};
						namespace Yen_G { // Field Description
							enum {Lsb = 4};
							enum {FieldMask = 0x00000010};
							enum {Value_Default = 0x1};
							enum {ValueMask_Default = 0x00000010};
							enum {Value_Disable = 0x0};
							enum {ValueMask_Disable = 0x00000000};
							enum {Value_Enable = 0x1};
							enum {ValueMask_Enable = 0x00000010};
							};
						namespace Xen_G { // Field Description
							enum {Lsb = 3};
							enum {FieldMask = 0x00000008};
							enum {Value_Default = 0x1};
							enum {ValueMask_Default = 0x00000008};
							enum {Value_Disable = 0x0};
							enum {ValueMask_Disable = 0x00000000};
							enum {Value_Enable = 0x1};
							enum {ValueMask_Enable = 0x00000008};
							};
						namespace LIR_XL1 { // Field Description
							enum {Lsb = 1};
							enum {FieldMask = 0x00000002};
							enum {Value_Default = 0x0};
							enum {ValueMask_Default = 0x00000000};
							enum {Value_NotLatched = 0x0};
							enum {ValueMask_NotLatched = 0x00000000};
							enum {Value_Latched = 0x1};
							enum {ValueMask_Latched = 0x00000002};
							};
						namespace _4D_XL1 { // Field Description
							enum {Lsb = 0};
							enum {FieldMask = 0x00000001};
							enum {Value_Default = 0x0};
							enum {ValueMask_Default = 0x00000000};
							enum {Value_Use6D = 0x0};
							enum {ValueMask_Use6D = 0x00000000};
							enum {Value_Use4D = 0x1};
							enum {ValueMask_Use4D = 0x00000001};
							};
						};
					namespace CtrlReg5Xl { // Register description
						typedef uint8_t	Reg;
						namespace DEC { // Field Description
							enum {Lsb = 6};
							enum {FieldMask = 0x000000C0};
							enum {Value_Default = 0x0};
							enum {ValueMask_Default = 0x00000000};
							enum {Value_None = 0x0};
							enum {ValueMask_None = 0x00000000};
							enum {Value_UpdateEvery2Samples = 0x1};
							enum {ValueMask_UpdateEvery2Samples = 0x00000040};
							enum {Value_UpdateEvery4Samples = 0x2};
							enum {ValueMask_UpdateEvery4Samples = 0x00000080};
							enum {Value_UpdateEvery8Samples = 0x3};
							enum {ValueMask_UpdateEvery8Samples = 0x000000C0};
							};
						namespace Zen_XL { // Field Description
							enum {Lsb = 5};
							enum {FieldMask = 0x00000020};
							enum {Value_Default = 0x1};
							enum {ValueMask_Default = 0x00000020};
							enum {Value_Disable = 0x0};
							enum {ValueMask_Disable = 0x00000000};
							enum {Value_Enable = 0x1};
							enum {ValueMask_Enable = 0x00000020};
							};
						namespace Yen_XL { // Field Description
							enum {Lsb = 4};
							enum {FieldMask = 0x00000010};
							enum {Value_Default = 0x1};
							enum {ValueMask_Default = 0x00000010};
							enum {Value_Disable = 0x0};
							enum {ValueMask_Disable = 0x00000000};
							enum {Value_Enable = 0x1};
							enum {ValueMask_Enable = 0x00000010};
							};
						namespace Xen_XL { // Field Description
							enum {Lsb = 3};
							enum {FieldMask = 0x00000008};
							enum {Value_Default = 0x1};
							enum {ValueMask_Default = 0x00000008};
							enum {Value_Disable = 0x0};
							enum {ValueMask_Disable = 0x00000000};
							enum {Value_Enable = 0x1};
							enum {ValueMask_Enable = 0x00000008};
							};
						};
					namespace CtrlReg6Xl { // Register description
						typedef uint8_t	Reg;
						namespace ODR_XL { // Field Description
							enum {Lsb = 5};
							enum {FieldMask = 0x000000E0};
							enum {Value_Default = 0x0};
							enum {ValueMask_Default = 0x00000000};
							enum {Value_PowerDown = 0x0};
							enum {ValueMask_PowerDown = 0x00000000};
							enum {Value__10Hz = 0x1};
							enum {ValueMask__10Hz = 0x00000020};
							enum {Value__50Hz = 0x2};
							enum {ValueMask__50Hz = 0x00000040};
							enum {Value__119Hz = 0x3};
							enum {ValueMask__119Hz = 0x00000060};
							enum {Value__238Hz = 0x4};
							enum {ValueMask__238Hz = 0x00000080};
							enum {Value__467Hz = 0x5};
							enum {ValueMask__467Hz = 0x000000A0};
							enum {Value__952Hz = 0x6};
							enum {ValueMask__952Hz = 0x000000C0};
							};
						namespace FS_XL { // Field Description
							enum {Lsb = 3};
							enum {FieldMask = 0x00000018};
							enum {Value_Default = 0x0};
							enum {ValueMask_Default = 0x00000000};
							enum {Value_PlusOrMinus2g = 0x0};
							enum {ValueMask_PlusOrMinus2g = 0x00000000};
							enum {Value_PlusOrMinus16g = 0x1};
							enum {ValueMask_PlusOrMinus16g = 0x00000008};
							enum {Value_PlusOrMinus4g = 0x2};
							enum {ValueMask_PlusOrMinus4g = 0x00000010};
							enum {Value_PlusOrMinus8g = 0x3};
							enum {ValueMask_PlusOrMinus8g = 0x00000018};
							};
						namespace BW_SCAL_ODR { // Field Description
							enum {Lsb = 2};
							enum {FieldMask = 0x00000004};
							enum {Value_Default = 0x0};
							enum {ValueMask_Default = 0x00000000};
							enum {Value_DeterminedByODR = 0x0};
							enum {ValueMask_DeterminedByODR = 0x00000000};
							enum {Value__408HzWhenOdrIs952Hz50Hz10Hz = 0x0};
							enum {ValueMask__408HzWhenOdrIs952Hz50Hz10Hz = 0x00000000};
							enum {Value__211HzWhenOdrIs476Hz = 0x0};
							enum {ValueMask__211HzWhenOdrIs476Hz = 0x00000000};
							enum {Value__105HzWhenOdrIs238Hz = 0x0};
							enum {ValueMask__105HzWhenOdrIs238Hz = 0x00000000};
							enum {Value__50HzWhenOdrIs119Hz = 0x0};
							enum {ValueMask__50HzWhenOdrIs119Hz = 0x00000000};
							enum {Value_UseBW_XL = 0x1};
							enum {ValueMask_UseBW_XL = 0x00000004};
							};
						namespace BW_XL { // Field Description
							enum {Lsb = 1};
							enum {FieldMask = 0x00000006};
							enum {Value_Default = 0x0};
							enum {ValueMask_Default = 0x00000000};
							enum {Value__408Hz = 0x0};
							enum {ValueMask__408Hz = 0x00000000};
							enum {Value__211Hz = 0x1};
							enum {ValueMask__211Hz = 0x00000002};
							enum {Value__105Hz = 0x2};
							enum {ValueMask__105Hz = 0x00000004};
							enum {Value__50Hz = 0x3};
							enum {ValueMask__50Hz = 0x00000006};
							};
						};
					namespace CtrlReg7Xl { // Register description
						typedef uint8_t	Reg;
						namespace HR { // Field Description
							enum {Lsb = 7};
							enum {FieldMask = 0x00000080};
							enum {Value_Default = 0x0};
							enum {ValueMask_Default = 0x00000000};
							enum {Value_Disable = 0x0};
							enum {ValueMask_Disable = 0x00000000};
							enum {Value_Enable = 0x1};
							enum {ValueMask_Enable = 0x00000080};
							};
						namespace DCF { // Field Description
							enum {Lsb = 5};
							enum {FieldMask = 0x00000060};
							enum {Value_Default = 0x0};
							enum {ValueMask_Default = 0x00000000};
							};
						namespace FDS { // Field Description
							enum {Lsb = 2};
							enum {FieldMask = 0x00000004};
							enum {Value_Default = 0x0};
							enum {ValueMask_Default = 0x00000000};
							enum {Value_InternalFilterBypassed = 0x0};
							enum {ValueMask_InternalFilterBypassed = 0x00000000};
							enum {Value_InternalFilterSent = 0x1};
							enum {ValueMask_InternalFilterSent = 0x00000004};
							};
						namespace HPIS1 { // Field Description
							enum {Lsb = 0};
							enum {FieldMask = 0x00000001};
							enum {Value_Default = 0x0};
							enum {ValueMask_Default = 0x00000000};
							enum {Value_FilterBypassed = 0x0};
							enum {ValueMask_FilterBypassed = 0x00000000};
							enum {Value_FilterEnabled = 0x1};
							enum {ValueMask_FilterEnabled = 0x00000001};
							};
						};
					namespace CtrlReg8 { // Register description
						typedef uint8_t	Reg;
						namespace BOOT { // Field Description
							enum {Lsb = 7};
							enum {FieldMask = 0x00000080};
							enum {Value_Default = 0x0};
							enum {ValueMask_Default = 0x00000000};
							enum {Value_Normal = 0x0};
							enum {ValueMask_Normal = 0x00000000};
							enum {Value_RebootMemoryContent = 0x1};
							enum {ValueMask_RebootMemoryContent = 0x00000080};
							};
						namespace BDU { // Field Description
							enum {Lsb = 6};
							enum {FieldMask = 0x00000040};
							enum {Value_Default = 0x0};
							enum {ValueMask_Default = 0x00000000};
							enum {Value_Continuous = 0x0};
							enum {ValueMask_Continuous = 0x00000000};
							enum {Value_MsbAndLsbRead = 0x1};
							enum {ValueMask_MsbAndLsbRead = 0x00000040};
							};
						namespace H_LACTIVE { // Field Description
							enum {Lsb = 5};
							enum {FieldMask = 0x00000020};
							enum {Value_Default = 0x0};
							enum {ValueMask_Default = 0x00000000};
							enum {Value_ActiveHigh = 0x0};
							enum {ValueMask_ActiveHigh = 0x00000000};
							enum {Value_ActiveLow = 0x1};
							enum {ValueMask_ActiveLow = 0x00000020};
							};
						namespace PP_OD { // Field Description
							enum {Lsb = 4};
							enum {FieldMask = 0x00000010};
							enum {Value_Default = 0x0};
							enum {ValueMask_Default = 0x00000000};
							enum {Value_PushPull = 0x0};
							enum {ValueMask_PushPull = 0x00000000};
							enum {Value_OpenDrain = 0x1};
							enum {ValueMask_OpenDrain = 0x00000010};
							};
						namespace SIM { // Field Description
							enum {Lsb = 3};
							enum {FieldMask = 0x00000008};
							enum {Value_Default = 0x0};
							enum {ValueMask_Default = 0x00000000};
							enum {Value_FourWire = 0x0};
							enum {ValueMask_FourWire = 0x00000000};
							enum {Value_ThreeWire = 0x1};
							enum {ValueMask_ThreeWire = 0x00000008};
							};
						namespace IF_ADD_INC { // Field Description
							enum {Lsb = 2};
							enum {FieldMask = 0x00000004};
							enum {Value_Default = 0x1};
							enum {ValueMask_Default = 0x00000004};
							enum {Value_Disable = 0x0};
							enum {ValueMask_Disable = 0x00000000};
							enum {Value_Enable = 0x1};
							enum {ValueMask_Enable = 0x00000004};
							};
						namespace BLE { // Field Description
							enum {Lsb = 1};
							enum {FieldMask = 0x00000002};
							enum {Value_Default = 0x0};
							enum {ValueMask_Default = 0x00000000};
							enum {Value_lsbAtLowerAddress = 0x0};
							enum {ValueMask_lsbAtLowerAddress = 0x00000000};
							enum {Value_msbAtLowerAddress = 0x1};
							enum {ValueMask_msbAtLowerAddress = 0x00000002};
							};
						namespace SW_RESET { // Field Description
							enum {Lsb = 0};
							enum {FieldMask = 0x00000001};
							enum {Value_Default = 0x0};
							enum {ValueMask_Default = 0x00000000};
							enum {Value_Normal = 0x0};
							enum {ValueMask_Normal = 0x00000000};
							enum {Value_Reset = 0x1};
							enum {ValueMask_Reset = 0x00000001};
							};
						};
					namespace CtrlReg9 { // Register description
						typedef uint8_t	Reg;
						namespace SLEEP_G { // Field Description
							enum {Lsb = 6};
							enum {FieldMask = 0x00000040};
							enum {Value_Default = 0x0};
							enum {ValueMask_Default = 0x00000000};
							enum {Value_Disable = 0x0};
							enum {ValueMask_Disable = 0x00000000};
							enum {Value_Enable = 0x1};
							enum {ValueMask_Enable = 0x00000040};
							};
						namespace FIFO_TEMP_EN { // Field Description
							enum {Lsb = 4};
							enum {FieldMask = 0x00000010};
							enum {Value_Default = 0x0};
							enum {ValueMask_Default = 0x00000000};
							enum {Value_Disable = 0x0};
							enum {ValueMask_Disable = 0x00000000};
							enum {Value_Enable = 0x1};
							enum {ValueMask_Enable = 0x00000010};
							};
						namespace DRDY_mask_bit { // Field Description
							enum {Lsb = 3};
							enum {FieldMask = 0x00000008};
							enum {Value_Default = 0x0};
							enum {ValueMask_Default = 0x00000000};
							enum {Value_TimerDisable = 0x0};
							enum {ValueMask_TimerDisable = 0x00000000};
							enum {Value_TimerEnable = 0x1};
							enum {ValueMask_TimerEnable = 0x00000008};
							};
						namespace I2C_DISABLED { // Field Description
							enum {Lsb = 2};
							enum {FieldMask = 0x00000004};
							enum {Value_Default = 0x0};
							enum {ValueMask_Default = 0x00000000};
							enum {Value_SpiAndI2CEnable = 0x0};
							enum {ValueMask_SpiAndI2CEnable = 0x00000000};
							enum {Value_SpiOnly = 0x1};
							enum {ValueMask_SpiOnly = 0x00000004};
							};
						namespace FIFO_EN { // Field Description
							enum {Lsb = 1};
							enum {FieldMask = 0x00000002};
							enum {Value_Default = 0x0};
							enum {ValueMask_Default = 0x00000000};
							enum {Value_Disable = 0x0};
							enum {ValueMask_Disable = 0x00000000};
							enum {Value_Enable = 0x1};
							enum {ValueMask_Enable = 0x00000002};
							};
						namespace STOP_ON_FTH { // Field Description
							enum {Lsb = 0};
							enum {FieldMask = 0x00000001};
							enum {Value_Default = 0x0};
							enum {ValueMask_Default = 0x00000000};
							enum {Value_DepthUnlimited = 0x0};
							enum {ValueMask_DepthUnlimited = 0x00000000};
							enum {Value_DepthLimitedToThreshold = 0x1};
							enum {ValueMask_DepthLimitedToThreshold = 0x00000001};
							};
						};
					namespace CtrlReg10 { // Register description
						typedef uint8_t	Reg;
						namespace ST_G { // Field Description
							enum {Lsb = 2};
							enum {FieldMask = 0x00000004};
							enum {Value_Default = 0x0};
							enum {ValueMask_Default = 0x00000000};
							enum {Value_Disable = 0x0};
							enum {ValueMask_Disable = 0x00000000};
							enum {Value_Enable = 0x1};
							enum {ValueMask_Enable = 0x00000004};
							};
						namespace ST_XL { // Field Description
							enum {Lsb = 2};
							enum {FieldMask = 0x00000004};
							enum {Value_Default = 0x0};
							enum {ValueMask_Default = 0x00000000};
							enum {Value_Disable = 0x0};
							enum {ValueMask_Disable = 0x00000000};
							enum {Value_Enable = 0x1};
							enum {ValueMask_Enable = 0x00000004};
							};
						};
					namespace IntGenSrcXl { // Register description
						typedef uint8_t	Reg;
						namespace IA_XL { // Field Description
							enum {Lsb = 6};
							enum {FieldMask = 0x00000040};
							enum {Value_Default = 0x0};
							enum {ValueMask_Default = 0x00000000};
							enum {Value_None = 0x0};
							enum {ValueMask_None = 0x00000000};
							enum {Value_Pending = 0x1};
							enum {ValueMask_Pending = 0x00000040};
							};
						namespace ZH_XL { // Field Description
							enum {Lsb = 5};
							enum {FieldMask = 0x00000020};
							enum {Value_Default = 0x0};
							enum {ValueMask_Default = 0x00000000};
							enum {Value_None = 0x0};
							enum {ValueMask_None = 0x00000000};
							enum {Value_Pending = 0x1};
							enum {ValueMask_Pending = 0x00000020};
							};
						namespace ZL_XL { // Field Description
							enum {Lsb = 4};
							enum {FieldMask = 0x00000010};
							enum {Value_Default = 0x0};
							enum {ValueMask_Default = 0x00000000};
							enum {Value_None = 0x0};
							enum {ValueMask_None = 0x00000000};
							enum {Value_Pending = 0x1};
							enum {ValueMask_Pending = 0x00000010};
							};
						namespace YH_XL { // Field Description
							enum {Lsb = 3};
							enum {FieldMask = 0x00000008};
							enum {Value_Default = 0x0};
							enum {ValueMask_Default = 0x00000000};
							enum {Value_None = 0x0};
							enum {ValueMask_None = 0x00000000};
							enum {Value_Pending = 0x1};
							enum {ValueMask_Pending = 0x00000008};
							};
						namespace YL_XL { // Field Description
							enum {Lsb = 2};
							enum {FieldMask = 0x00000004};
							enum {Value_Default = 0x0};
							enum {ValueMask_Default = 0x00000000};
							enum {Value_None = 0x0};
							enum {ValueMask_None = 0x00000000};
							enum {Value_Pending = 0x1};
							enum {ValueMask_Pending = 0x00000004};
							};
						namespace XH_XL { // Field Description
							enum {Lsb = 1};
							enum {FieldMask = 0x00000002};
							enum {Value_Default = 0x0};
							enum {ValueMask_Default = 0x00000000};
							enum {Value_None = 0x0};
							enum {ValueMask_None = 0x00000000};
							enum {Value_Pending = 0x1};
							enum {ValueMask_Pending = 0x00000002};
							};
						namespace XL_XL { // Field Description
							enum {Lsb = 0};
							enum {FieldMask = 0x00000001};
							enum {Value_Default = 0x0};
							enum {ValueMask_Default = 0x00000000};
							enum {Value_None = 0x0};
							enum {ValueMask_None = 0x00000000};
							enum {Value_Pending = 0x1};
							enum {ValueMask_Pending = 0x00000001};
							};
						};
					namespace StatusReg27 { // Register description
						typedef uint8_t	Reg;
						namespace IG_XL { // Field Description
							enum {Lsb = 6};
							enum {FieldMask = 0x00000040};
							enum {Value_Default = 0x0};
							enum {ValueMask_Default = 0x00000000};
							enum {Value_None = 0x0};
							enum {ValueMask_None = 0x00000000};
							enum {Value_Pending = 0x1};
							enum {ValueMask_Pending = 0x00000040};
							};
						namespace IG_G { // Field Description
							enum {Lsb = 5};
							enum {FieldMask = 0x00000020};
							enum {Value_Default = 0x0};
							enum {ValueMask_Default = 0x00000000};
							enum {Value_None = 0x0};
							enum {ValueMask_None = 0x00000000};
							enum {Value_Pending = 0x1};
							enum {ValueMask_Pending = 0x00000020};
							};
						namespace INACT { // Field Description
							enum {Lsb = 4};
							enum {FieldMask = 0x00000010};
							enum {Value_Default = 0x0};
							enum {ValueMask_Default = 0x00000000};
							enum {Value_None = 0x0};
							enum {ValueMask_None = 0x00000000};
							enum {Value_Pending = 0x1};
							enum {ValueMask_Pending = 0x00000010};
							};
						namespace BOOT_STATUS { // Field Description
							enum {Lsb = 3};
							enum {FieldMask = 0x00000008};
							enum {Value_Default = 0x0};
							enum {ValueMask_Default = 0x00000000};
							enum {Value_NoBootRunning = 0x0};
							enum {ValueMask_NoBootRunning = 0x00000000};
							enum {Value_BootRunning = 0x1};
							enum {ValueMask_BootRunning = 0x00000008};
							};
						namespace TDA { // Field Description
							enum {Lsb = 2};
							enum {FieldMask = 0x00000004};
							enum {Value_Default = 0x0};
							enum {ValueMask_Default = 0x00000000};
							enum {Value_NotYetAvailable = 0x0};
							enum {ValueMask_NotYetAvailable = 0x00000000};
							enum {Value_NewDataAvailable = 0x1};
							enum {ValueMask_NewDataAvailable = 0x00000004};
							};
						namespace GDA { // Field Description
							enum {Lsb = 1};
							enum {FieldMask = 0x00000002};
							enum {Value_Default = 0x0};
							enum {ValueMask_Default = 0x00000000};
							enum {Value_NotYetAvailable = 0x0};
							enum {ValueMask_NotYetAvailable = 0x00000000};
							enum {Value_NewDataAvailable = 0x1};
							enum {ValueMask_NewDataAvailable = 0x00000002};
							};
						namespace XLDA { // Field Description
							enum {Lsb = 0};
							enum {FieldMask = 0x00000001};
							enum {Value_Default = 0x0};
							enum {ValueMask_Default = 0x00000000};
							enum {Value_NotYetAvailable = 0x0};
							enum {ValueMask_NotYetAvailable = 0x00000000};
							enum {Value_NewDataAvailable = 0x1};
							enum {ValueMask_NewDataAvailable = 0x00000001};
							};
						};
					namespace OutXXl0 { // Register description
						typedef uint8_t	Reg;
						namespace Acceleration { // Field Description
							enum {Lsb = 0};
							enum {FieldMask = 0x000000FF};
							};
						};
					namespace OutXXl1 { // Register description
						typedef uint8_t	Reg;
						namespace Acceleration { // Field Description
							enum {Lsb = 0};
							enum {FieldMask = 0x000000FF};
							};
						};
					namespace OutYXl0 { // Register description
						typedef uint8_t	Reg;
						namespace Acceleration { // Field Description
							enum {Lsb = 0};
							enum {FieldMask = 0x000000FF};
							};
						};
					namespace OutYXl1 { // Register description
						typedef uint8_t	Reg;
						namespace Acceleration { // Field Description
							enum {Lsb = 0};
							enum {FieldMask = 0x000000FF};
							};
						};
					namespace OutZXl0 { // Register description
						typedef uint8_t	Reg;
						namespace Acceleration { // Field Description
							enum {Lsb = 0};
							enum {FieldMask = 0x000000FF};
							};
						};
					namespace OutZXl1 { // Register description
						typedef uint8_t	Reg;
						namespace Acceleration { // Field Description
							enum {Lsb = 0};
							enum {FieldMask = 0x000000FF};
							};
						};
					namespace FifoCtrl { // Register description
						typedef uint8_t	Reg;
						namespace FMODE { // Field Description
							enum {Lsb = 5};
							enum {FieldMask = 0x000000E0};
							enum {Value_Default = 0x0};
							enum {ValueMask_Default = 0x00000000};
							enum {Value_Bypass = 0x0};
							enum {ValueMask_Bypass = 0x00000000};
							enum {Value_StopWhenFull = 0x1};
							enum {ValueMask_StopWhenFull = 0x00000020};
							enum {Value_ContinousUntilTriggerDeassertedThenFifoMode = 0x3};
							enum {ValueMask_ContinousUntilTriggerDeassertedThenFifoMode = 0x00000060};
							enum {Value_BypassUntilTriggerDeassertedThenContinuousMode = 0x4};
							enum {ValueMask_BypassUntilTriggerDeassertedThenContinuousMode = 0x00000080};
							enum {Value_ContinuousMode = 0x6};
							enum {ValueMask_ContinuousMode = 0x000000C0};
							};
						namespace FTH { // Field Description
							enum {Lsb = 0};
							enum {FieldMask = 0x0000001F};
							enum {Value_Default = 0x0};
							enum {ValueMask_Default = 0x00000000};
							};
						};
					namespace FifoSrc { // Register description
						typedef uint8_t	Reg;
						namespace FTH { // Field Description
							enum {Lsb = 7};
							enum {FieldMask = 0x00000080};
							enum {Value_Default = 0x0};
							enum {ValueMask_Default = 0x00000000};
							};
						namespace OVRN { // Field Description
							enum {Lsb = 6};
							enum {FieldMask = 0x00000040};
							enum {Value_Default = 0x0};
							enum {ValueMask_Default = 0x00000000};
							};
						namespace FSS { // Field Description
							enum {Lsb = 0};
							enum {FieldMask = 0x0000003F};
							enum {Value_Default = 0x0};
							enum {ValueMask_Default = 0x00000000};
							enum {Value_Full = 0x20};
							enum {ValueMask_Full = 0x00000020};
							};
						};
					namespace IntGenCfgG { // Register description
						typedef uint8_t	Reg;
						namespace AOI_G { // Field Description
							enum {Lsb = 7};
							enum {FieldMask = 0x00000080};
							enum {Value_Default = 0x0};
							enum {ValueMask_Default = 0x00000000};
							enum {Value_OR = 0x0};
							enum {ValueMask_OR = 0x00000000};
							enum {Value_AND = 0x0};
							enum {ValueMask_AND = 0x00000000};
							};
						namespace LIR_G { // Field Description
							enum {Lsb = 6};
							enum {FieldMask = 0x00000040};
							enum {Value_Default = 0x0};
							enum {ValueMask_Default = 0x00000000};
							enum {Value_NotLatched = 0x0};
							enum {ValueMask_NotLatched = 0x00000000};
							enum {Value_Latched = 0x1};
							enum {ValueMask_Latched = 0x00000040};
							};
						namespace ZHIE_G { // Field Description
							enum {Lsb = 5};
							enum {FieldMask = 0x00000020};
							enum {Value_Default = 0x0};
							enum {ValueMask_Default = 0x00000000};
							enum {Value_Disable = 0x0};
							enum {ValueMask_Disable = 0x00000000};
							enum {Value_Enable = 0x1};
							enum {ValueMask_Enable = 0x00000020};
							};
						namespace ZLIE_G { // Field Description
							enum {Lsb = 4};
							enum {FieldMask = 0x00000010};
							enum {Value_Default = 0x0};
							enum {ValueMask_Default = 0x00000000};
							enum {Value_Disable = 0x0};
							enum {ValueMask_Disable = 0x00000000};
							enum {Value_Enable = 0x1};
							enum {ValueMask_Enable = 0x00000010};
							};
						namespace YHIE_G { // Field Description
							enum {Lsb = 3};
							enum {FieldMask = 0x00000008};
							enum {Value_Default = 0x0};
							enum {ValueMask_Default = 0x00000000};
							enum {Value_Disable = 0x0};
							enum {ValueMask_Disable = 0x00000000};
							enum {Value_Enable = 0x1};
							enum {ValueMask_Enable = 0x00000008};
							};
						namespace YLIE_G { // Field Description
							enum {Lsb = 2};
							enum {FieldMask = 0x00000004};
							enum {Value_Default = 0x0};
							enum {ValueMask_Default = 0x00000000};
							enum {Value_Disable = 0x0};
							enum {ValueMask_Disable = 0x00000000};
							enum {Value_Enable = 0x1};
							enum {ValueMask_Enable = 0x00000004};
							};
						namespace XHIE_G { // Field Description
							enum {Lsb = 1};
							enum {FieldMask = 0x00000002};
							enum {Value_Default = 0x0};
							enum {ValueMask_Default = 0x00000000};
							enum {Value_Disable = 0x0};
							enum {ValueMask_Disable = 0x00000000};
							enum {Value_Enable = 0x1};
							enum {ValueMask_Enable = 0x00000002};
							};
						namespace XLIE_G { // Field Description
							enum {Lsb = 0};
							enum {FieldMask = 0x00000001};
							enum {Value_Default = 0x0};
							enum {ValueMask_Default = 0x00000000};
							enum {Value_Disable = 0x0};
							enum {ValueMask_Disable = 0x00000000};
							enum {Value_Enable = 0x1};
							enum {ValueMask_Enable = 0x00000001};
							};
						};
					namespace IntGenThsXhG { // Register description
						typedef uint8_t	Reg;
						namespace DCRM_G { // Field Description
							enum {Lsb = 7};
							enum {FieldMask = 0x00000080};
							enum {Value_Default = 0x0};
							enum {ValueMask_Default = 0x00000000};
							enum {Value_Reset = 0x0};
							enum {ValueMask_Reset = 0x00000000};
							enum {Value_Decrement = 0x1};
							enum {ValueMask_Decrement = 0x00000080};
							};
						namespace THS_G_X { // Field Description
							enum {Lsb = 0};
							enum {FieldMask = 0x0000007F};
							enum {Value_Default = 0x0};
							enum {ValueMask_Default = 0x00000000};
							};
						};
					namespace IntGenThsXlG { // Register description
						typedef uint8_t	Reg;
						namespace THS_G_X { // Field Description
							enum {Lsb = 0};
							enum {FieldMask = 0x000000FF};
							enum {Value_Default = 0x0};
							enum {ValueMask_Default = 0x00000000};
							};
						};
					namespace IntGenThsYhG { // Register description
						typedef uint8_t	Reg;
						namespace DCRM_G { // Field Description
							enum {Lsb = 7};
							enum {FieldMask = 0x00000080};
							enum {Value_Default = 0x0};
							enum {ValueMask_Default = 0x00000000};
							enum {Value_Reset = 0x0};
							enum {ValueMask_Reset = 0x00000000};
							enum {Value_Decrement = 0x1};
							enum {ValueMask_Decrement = 0x00000080};
							};
						namespace THS_G_Y { // Field Description
							enum {Lsb = 0};
							enum {FieldMask = 0x0000007F};
							enum {Value_Default = 0x0};
							enum {ValueMask_Default = 0x00000000};
							};
						};
					namespace IntGenThsYlG { // Register description
						typedef uint8_t	Reg;
						namespace THS_G_X { // Field Description
							enum {Lsb = 0};
							enum {FieldMask = 0x000000FF};
							enum {Value_Default = 0x0};
							enum {ValueMask_Default = 0x00000000};
							};
						};
					namespace IntGenThsZhG { // Register description
						typedef uint8_t	Reg;
						namespace DCRM_G { // Field Description
							enum {Lsb = 7};
							enum {FieldMask = 0x00000080};
							enum {Value_Default = 0x0};
							enum {ValueMask_Default = 0x00000000};
							enum {Value_Reset = 0x0};
							enum {ValueMask_Reset = 0x00000000};
							enum {Value_Decrement = 0x1};
							enum {ValueMask_Decrement = 0x00000080};
							};
						namespace THS_G_Z { // Field Description
							enum {Lsb = 0};
							enum {FieldMask = 0x0000007F};
							enum {Value_Default = 0x0};
							enum {ValueMask_Default = 0x00000000};
							};
						};
					namespace IntGenThsZlG { // Register description
						typedef uint8_t	Reg;
						namespace THS_G_Z { // Field Description
							enum {Lsb = 0};
							enum {FieldMask = 0x000000FF};
							enum {Value_Default = 0x0};
							enum {ValueMask_Default = 0x00000000};
							};
						};
					namespace IntGenDurG { // Register description
						typedef uint8_t	Reg;
						namespace WAIT_G { // Field Description
							enum {Lsb = 7};
							enum {FieldMask = 0x00000080};
							enum {Value_Default = 0x0};
							enum {ValueMask_Default = 0x00000000};
							enum {Value_WaitOff = 0x0};
							enum {ValueMask_WaitOff = 0x00000000};
							enum {Value_WaitForDurGSamples = 0x1};
							enum {ValueMask_WaitForDurGSamples = 0x00000080};
							};
						namespace DUR_G { // Field Description
							enum {Lsb = 0};
							enum {FieldMask = 0x0000007F};
							enum {Value_Default = 0x0};
							enum {ValueMask_Default = 0x00000000};
							};
						};
					}
				}
			}
		}
	}
#endif
