#ifndef __oscl_hw_st_lsm9ds1_magh__
#define __oscl_hw_st_lsm9ds1_magh__
#include <stdint.h>
namespace Oscl { // Namespace description
	namespace HW { // Namespace description
		namespace ST { // Namespace description
			namespace LSM9DS1 { // Namespace description
				namespace Magnetometer { // Namespace description
					enum {OFFSET_X_REG_L_M = 5};
					enum {OFFSET_X_REG_H_M = 6};
					enum {OFFSET_Y_REG_L_M = 7};
					enum {OFFSET_Y_REG_H_M = 8};
					enum {OFFSET_Z_REG_L_M = 9};
					enum {OFFSET_Z_REG_H_M = 10};
					enum {WHO_AM_I_M = 15};
					enum {CTRL_REG1_M = 32};
					enum {CTRL_REG2_M = 33};
					enum {CTRL_REG3_M = 34};
					enum {CTRL_REG4_M = 35};
					enum {CTRL_REG5_M = 36};
					enum {STATUS_REG_M = 39};
					enum {OUT_X_L_M = 40};
					enum {OUT_X_H_M = 41};
					enum {OUT_Y_L_M = 42};
					enum {OUT_Y_H_M = 43};
					enum {OUT_Z_L_M = 44};
					enum {OUT_Z_H_M = 45};
					enum {INT_CFG_M = 48};
					enum {INT_SRC_M = 49};
					enum {INT_THS_L_M = 50};
					enum {INT_THS_H_M = 51};
					namespace OffsetXRegLM { // Register description
						typedef uint8_t	Reg;
						namespace OFXM { // Field Description
							enum {Lsb = 0};
							enum {FieldMask = 0x000000FF};
							enum {Value_Default = 0x0};
							enum {ValueMask_Default = 0x00000000};
							};
						};
					namespace OffsetXRegHM { // Register description
						typedef uint8_t	Reg;
						namespace OFXM { // Field Description
							enum {Lsb = 0};
							enum {FieldMask = 0x000000FF};
							enum {Value_Default = 0x0};
							enum {ValueMask_Default = 0x00000000};
							};
						};
					namespace OffsetYRegLM { // Register description
						typedef uint8_t	Reg;
						namespace OFYM { // Field Description
							enum {Lsb = 0};
							enum {FieldMask = 0x000000FF};
							enum {Value_Default = 0x0};
							enum {ValueMask_Default = 0x00000000};
							};
						};
					namespace OffsetYRegHM { // Register description
						typedef uint8_t	Reg;
						namespace OFYM { // Field Description
							enum {Lsb = 0};
							enum {FieldMask = 0x000000FF};
							enum {Value_Default = 0x0};
							enum {ValueMask_Default = 0x00000000};
							};
						};
					namespace OffsetZRegLM { // Register description
						typedef uint8_t	Reg;
						namespace OFZM { // Field Description
							enum {Lsb = 0};
							enum {FieldMask = 0x000000FF};
							enum {Value_Default = 0x0};
							enum {ValueMask_Default = 0x00000000};
							};
						};
					namespace OffsetZRegHM { // Register description
						typedef uint8_t	Reg;
						namespace OFZM { // Field Description
							enum {Lsb = 0};
							enum {FieldMask = 0x000000FF};
							enum {Value_Default = 0x0};
							enum {ValueMask_Default = 0x00000000};
							};
						};
					namespace WhoAmI { // Register description
						typedef uint8_t	Reg;
						namespace ID { // Field Description
							enum {Lsb = 0};
							enum {FieldMask = 0x000000FF};
							enum {Value_MagnetometerID = 0x3D};
							enum {ValueMask_MagnetometerID = 0x0000003D};
							};
						};
					namespace CtrlReg1M { // Register description
						typedef uint8_t	Reg;
						namespace TEMP_COMP { // Field Description
							enum {Lsb = 7};
							enum {FieldMask = 0x00000080};
							enum {Value_Default = 0x0};
							enum {ValueMask_Default = 0x00000000};
							enum {Value_Disable = 0x0};
							enum {ValueMask_Disable = 0x00000000};
							enum {Value_Enable = 0x1};
							enum {ValueMask_Enable = 0x00000080};
							};
						namespace OM { // Field Description
							enum {Lsb = 5};
							enum {FieldMask = 0x00000060};
							enum {Value_Default = 0x0};
							enum {ValueMask_Default = 0x00000000};
							enum {Value_LowPower = 0x0};
							enum {ValueMask_LowPower = 0x00000000};
							enum {Value_MediumPerformance = 0x1};
							enum {ValueMask_MediumPerformance = 0x00000020};
							enum {Value_HighPerformance = 0x2};
							enum {ValueMask_HighPerformance = 0x00000040};
							enum {Value_UltraHighPerformance = 0x2};
							enum {ValueMask_UltraHighPerformance = 0x00000040};
							};
						namespace DO { // Field Description
							enum {Lsb = 2};
							enum {FieldMask = 0x0000001C};
							enum {Value_Default = 0x4};
							enum {ValueMask_Default = 0x00000010};
							enum {Value__0p625Hz = 0x0};
							enum {ValueMask__0p625Hz = 0x00000000};
							enum {Value__1p25Hz = 0x1};
							enum {ValueMask__1p25Hz = 0x00000004};
							enum {Value__2p5Hz = 0x2};
							enum {ValueMask__2p5Hz = 0x00000008};
							enum {Value__5Hz = 0x3};
							enum {ValueMask__5Hz = 0x0000000C};
							enum {Value__10Hz = 0x4};
							enum {ValueMask__10Hz = 0x00000010};
							enum {Value__20Hz = 0x5};
							enum {ValueMask__20Hz = 0x00000014};
							enum {Value__40Hz = 0x6};
							enum {ValueMask__40Hz = 0x00000018};
							enum {Value__80Hz = 0x7};
							enum {ValueMask__80Hz = 0x0000001C};
							};
						namespace FAST_ODR { // Field Description
							enum {Lsb = 1};
							enum {FieldMask = 0x00000002};
							enum {Value_Default = 0x0};
							enum {ValueMask_Default = 0x00000000};
							enum {Value_Disable = 0x0};
							enum {ValueMask_Disable = 0x00000000};
							enum {Value_Enable = 0x1};
							enum {ValueMask_Enable = 0x00000002};
							};
						namespace ST { // Field Description
							enum {Lsb = 0};
							enum {FieldMask = 0x00000001};
							enum {Value_Default = 0x0};
							enum {ValueMask_Default = 0x00000000};
							enum {Value_Disable = 0x0};
							enum {ValueMask_Disable = 0x00000000};
							enum {Value_Enable = 0x1};
							enum {ValueMask_Enable = 0x00000001};
							};
						};
					namespace CtrlReg2M { // Register description
						typedef uint8_t	Reg;
						namespace FS { // Field Description
							enum {Lsb = 5};
							enum {FieldMask = 0x00000060};
							enum {Value_Default = 0x0};
							enum {ValueMask_Default = 0x00000000};
							enum {Value__4gauss = 0x0};
							enum {ValueMask__4gauss = 0x00000000};
							enum {Value__8gauss = 0x1};
							enum {ValueMask__8gauss = 0x00000020};
							enum {Value__12gauss = 0x2};
							enum {ValueMask__12gauss = 0x00000040};
							enum {Value__16gauss = 0x3};
							enum {ValueMask__16gauss = 0x00000060};
							};
						namespace REBOOT { // Field Description
							enum {Lsb = 3};
							enum {FieldMask = 0x00000008};
							enum {Value_Default = 0x0};
							enum {ValueMask_Default = 0x00000000};
							enum {Value_Normal = 0x0};
							enum {ValueMask_Normal = 0x00000000};
							enum {Value_RebootMemoryContent = 0x1};
							enum {ValueMask_RebootMemoryContent = 0x00000008};
							};
						namespace SOFT_RST { // Field Description
							enum {Lsb = 2};
							enum {FieldMask = 0x00000004};
							enum {Value_Default = 0x0};
							enum {ValueMask_Default = 0x00000000};
							enum {Value_Reset = 0x1};
							enum {ValueMask_Reset = 0x00000004};
							};
						};
					namespace CtrlReg3M { // Register description
						typedef uint8_t	Reg;
						namespace I2C_DISABLE { // Field Description
							enum {Lsb = 7};
							enum {FieldMask = 0x00000080};
							enum {Value_Default = 0x0};
							enum {ValueMask_Default = 0x00000000};
							};
						namespace LP { // Field Description
							enum {Lsb = 5};
							enum {FieldMask = 0x00000020};
							enum {Value_Default = 0x0};
							enum {ValueMask_Default = 0x00000000};
							enum {Value_DataRateSetByDO = 0x0};
							enum {ValueMask_DataRateSetByDO = 0x00000000};
							enum {Value_Minimum = 0x1};
							enum {ValueMask_Minimum = 0x00000020};
							};
						namespace SIM { // Field Description
							enum {Lsb = 2};
							enum {FieldMask = 0x00000004};
							enum {Value_Default = 0x0};
							enum {ValueMask_Default = 0x00000000};
							enum {Value_SpiWriteOnly = 0x0};
							enum {ValueMask_SpiWriteOnly = 0x00000000};
							enum {Value_SpiReadWrite = 0x1};
							enum {ValueMask_SpiReadWrite = 0x00000004};
							};
						namespace MD { // Field Description
							enum {Lsb = 0};
							enum {FieldMask = 0x00000003};
							enum {Value_Default = 0x3};
							enum {ValueMask_Default = 0x00000003};
							enum {Value_Continuous = 0x0};
							enum {ValueMask_Continuous = 0x00000000};
							enum {Value_SingleConversion = 0x1};
							enum {ValueMask_SingleConversion = 0x00000001};
							enum {Value_PowerDownModeA = 0x2};
							enum {ValueMask_PowerDownModeA = 0x00000002};
							enum {Value_PowerDownModeB = 0x3};
							enum {ValueMask_PowerDownModeB = 0x00000003};
							};
						};
					namespace CtrlReg4M { // Register description
						typedef uint8_t	Reg;
						namespace OMZ { // Field Description
							enum {Lsb = 2};
							enum {FieldMask = 0x0000000C};
							enum {Value_Default = 0x0};
							enum {ValueMask_Default = 0x00000000};
							enum {Value_LowPower = 0x0};
							enum {ValueMask_LowPower = 0x00000000};
							enum {Value_LowPerformance = 0x1};
							enum {ValueMask_LowPerformance = 0x00000004};
							enum {Value_HighPerformance = 0x2};
							enum {ValueMask_HighPerformance = 0x00000008};
							enum {Value_UltraHighPerformance = 0x3};
							enum {ValueMask_UltraHighPerformance = 0x0000000C};
							};
						namespace BLE { // Field Description
							enum {Lsb = 5};
							enum {FieldMask = 0x00000020};
							enum {Value_Default = 0x0};
							enum {ValueMask_Default = 0x00000000};
							enum {Value_LsbAtLowerAddress = 0x0};
							enum {ValueMask_LsbAtLowerAddress = 0x00000000};
							enum {Value_MsbAtLowerAddress = 0x1};
							enum {ValueMask_MsbAtLowerAddress = 0x00000020};
							};
						};
					namespace CtrlReg5M { // Register description
						typedef uint8_t	Reg;
						namespace FAST_READ { // Field Description
							enum {Lsb = 7};
							enum {FieldMask = 0x00000080};
							enum {Value_Default = 0x0};
							enum {ValueMask_Default = 0x00000000};
							enum {Value_Disable = 0x0};
							enum {ValueMask_Disable = 0x00000000};
							enum {Value_Enable = 0x1};
							enum {ValueMask_Enable = 0x00000080};
							};
						namespace BDU { // Field Description
							enum {Lsb = 5};
							enum {FieldMask = 0x00000020};
							enum {Value_Default = 0x0};
							enum {ValueMask_Default = 0x00000000};
							enum {Value_Continuous = 0x0};
							enum {ValueMask_Continuous = 0x00000000};
							enum {Value_OutputAfterMsbLsb = 0x1};
							enum {ValueMask_OutputAfterMsbLsb = 0x00000020};
							};
						};
					namespace StatusRegM { // Register description
						typedef uint8_t	Reg;
						namespace ZYXOR { // Field Description
							enum {Lsb = 7};
							enum {FieldMask = 0x00000080};
							enum {Value_Default = 0x0};
							enum {ValueMask_Default = 0x00000000};
							enum {Value_NoOverrun = 0x0};
							enum {ValueMask_NoOverrun = 0x00000000};
							enum {Value_Overrun = 0x1};
							enum {ValueMask_Overrun = 0x00000080};
							};
						namespace ZOR { // Field Description
							enum {Lsb = 6};
							enum {FieldMask = 0x00000040};
							enum {Value_Default = 0x0};
							enum {ValueMask_Default = 0x00000000};
							enum {Value_NoOverrun = 0x0};
							enum {ValueMask_NoOverrun = 0x00000000};
							enum {Value_Overrun = 0x1};
							enum {ValueMask_Overrun = 0x00000040};
							};
						namespace YOR { // Field Description
							enum {Lsb = 5};
							enum {FieldMask = 0x00000020};
							enum {Value_Default = 0x0};
							enum {ValueMask_Default = 0x00000000};
							enum {Value_NoOverrun = 0x0};
							enum {ValueMask_NoOverrun = 0x00000000};
							enum {Value_Overrun = 0x1};
							enum {ValueMask_Overrun = 0x00000020};
							};
						namespace XOR { // Field Description
							enum {Lsb = 4};
							enum {FieldMask = 0x00000010};
							enum {Value_Default = 0x0};
							enum {ValueMask_Default = 0x00000000};
							enum {Value_NoOverrun = 0x0};
							enum {ValueMask_NoOverrun = 0x00000000};
							enum {Value_Overrun = 0x1};
							enum {ValueMask_Overrun = 0x00000010};
							};
						namespace ZYXDA { // Field Description
							enum {Lsb = 3};
							enum {FieldMask = 0x00000008};
							enum {Value_Default = 0x0};
							enum {ValueMask_Default = 0x00000000};
							enum {Value_NotAvailable = 0x0};
							enum {ValueMask_NotAvailable = 0x00000000};
							enum {Value_Available = 0x1};
							enum {ValueMask_Available = 0x00000008};
							};
						namespace ZDA { // Field Description
							enum {Lsb = 2};
							enum {FieldMask = 0x00000004};
							enum {Value_Default = 0x0};
							enum {ValueMask_Default = 0x00000000};
							enum {Value_NotAvailable = 0x0};
							enum {ValueMask_NotAvailable = 0x00000000};
							enum {Value_Available = 0x1};
							enum {ValueMask_Available = 0x00000004};
							};
						namespace YDA { // Field Description
							enum {Lsb = 1};
							enum {FieldMask = 0x00000002};
							enum {Value_Default = 0x0};
							enum {ValueMask_Default = 0x00000000};
							enum {Value_NotAvailable = 0x0};
							enum {ValueMask_NotAvailable = 0x00000000};
							enum {Value_Available = 0x1};
							enum {ValueMask_Available = 0x00000002};
							};
						namespace XDA { // Field Description
							enum {Lsb = 0};
							enum {FieldMask = 0x00000001};
							enum {Value_Default = 0x0};
							enum {ValueMask_Default = 0x00000000};
							enum {Value_NotAvailable = 0x0};
							enum {ValueMask_NotAvailable = 0x00000000};
							enum {Value_Available = 0x1};
							enum {ValueMask_Available = 0x00000001};
							};
						};
					namespace OutXLM { // Register description
						typedef uint8_t	Reg;
						};
					namespace OutYLM { // Register description
						typedef uint8_t	Reg;
						};
					namespace OutZLM { // Register description
						typedef uint8_t	Reg;
						};
					namespace IntCfgM { // Register description
						typedef uint8_t	Reg;
						namespace XIEN { // Field Description
							enum {Lsb = 7};
							enum {FieldMask = 0x00000080};
							enum {Value_Default = 0x0};
							enum {ValueMask_Default = 0x00000000};
							enum {Value_Disable = 0x0};
							enum {ValueMask_Disable = 0x00000000};
							enum {Value_Enable = 0x1};
							enum {ValueMask_Enable = 0x00000080};
							};
						namespace YIEN { // Field Description
							enum {Lsb = 6};
							enum {FieldMask = 0x00000040};
							enum {Value_Default = 0x0};
							enum {ValueMask_Default = 0x00000000};
							enum {Value_Disable = 0x0};
							enum {ValueMask_Disable = 0x00000000};
							enum {Value_Enable = 0x1};
							enum {ValueMask_Enable = 0x00000040};
							};
						namespace ZIEN { // Field Description
							enum {Lsb = 5};
							enum {FieldMask = 0x00000020};
							enum {Value_Default = 0x0};
							enum {ValueMask_Default = 0x00000000};
							enum {Value_Disable = 0x0};
							enum {ValueMask_Disable = 0x00000000};
							enum {Value_Enable = 0x1};
							enum {ValueMask_Enable = 0x00000020};
							};
						namespace IEA { // Field Description
							enum {Lsb = 2};
							enum {FieldMask = 0x00000004};
							enum {Value_Default = 0x0};
							enum {ValueMask_Default = 0x00000000};
							enum {Value_LOW = 0x0};
							enum {ValueMask_LOW = 0x00000000};
							enum {Value_HIGH = 0x1};
							enum {ValueMask_HIGH = 0x00000004};
							};
						namespace IEL { // Field Description
							enum {Lsb = 1};
							enum {FieldMask = 0x00000002};
							enum {Value_Default = 0x0};
							enum {ValueMask_Default = 0x00000000};
							enum {Value_Latched = 0x0};
							enum {ValueMask_Latched = 0x00000000};
							enum {Value_NotLatched = 0x1};
							enum {ValueMask_NotLatched = 0x00000002};
							};
						namespace IEN { // Field Description
							enum {Lsb = 0};
							enum {FieldMask = 0x00000001};
							enum {Value_Default = 0x0};
							enum {ValueMask_Default = 0x00000000};
							enum {Value_Disable = 0x0};
							enum {ValueMask_Disable = 0x00000000};
							enum {Value_Enable = 0x1};
							enum {ValueMask_Enable = 0x00000001};
							};
						};
					namespace IntSrcM { // Register description
						typedef uint8_t	Reg;
						namespace PTH_X { // Field Description
							enum {Lsb = 7};
							enum {FieldMask = 0x00000080};
							enum {Value_Default = 0x0};
							enum {ValueMask_Default = 0x00000000};
							enum {Value_ThresholdExceeded = 0x1};
							enum {ValueMask_ThresholdExceeded = 0x00000080};
							};
						namespace PTH_Y { // Field Description
							enum {Lsb = 6};
							enum {FieldMask = 0x00000040};
							enum {Value_Default = 0x0};
							enum {ValueMask_Default = 0x00000000};
							enum {Value_ThresholdExceeded = 0x1};
							enum {ValueMask_ThresholdExceeded = 0x00000040};
							};
						namespace PTH_Z { // Field Description
							enum {Lsb = 5};
							enum {FieldMask = 0x00000020};
							enum {Value_Default = 0x0};
							enum {ValueMask_Default = 0x00000000};
							enum {Value_ThresholdExceeded = 0x1};
							enum {ValueMask_ThresholdExceeded = 0x00000020};
							};
						namespace NTH_X { // Field Description
							enum {Lsb = 4};
							enum {FieldMask = 0x00000010};
							enum {Value_Default = 0x0};
							enum {ValueMask_Default = 0x00000000};
							enum {Value_ThresholdExceeded = 0x1};
							enum {ValueMask_ThresholdExceeded = 0x00000010};
							};
						namespace NTH_Y { // Field Description
							enum {Lsb = 3};
							enum {FieldMask = 0x00000008};
							enum {Value_Default = 0x0};
							enum {ValueMask_Default = 0x00000000};
							enum {Value_ThresholdExceeded = 0x1};
							enum {ValueMask_ThresholdExceeded = 0x00000008};
							};
						namespace NTH_Z { // Field Description
							enum {Lsb = 2};
							enum {FieldMask = 0x00000004};
							enum {Value_Default = 0x0};
							enum {ValueMask_Default = 0x00000000};
							enum {Value_ThresholdExceeded = 0x1};
							enum {ValueMask_ThresholdExceeded = 0x00000004};
							};
						namespace MROI { // Field Description
							enum {Lsb = 1};
							enum {FieldMask = 0x00000002};
							enum {Value_Default = 0x0};
							enum {ValueMask_Default = 0x00000000};
							enum {Value_Overflow = 0x1};
							enum {ValueMask_Overflow = 0x00000002};
							};
						namespace INT { // Field Description
							enum {Lsb = 0};
							enum {FieldMask = 0x00000001};
							enum {Value_Default = 0x0};
							enum {ValueMask_Default = 0x00000000};
							enum {Value_InterruptPending = 0x1};
							enum {ValueMask_InterruptPending = 0x00000001};
							};
						};
					namespace IntThsLM { // Register description
						typedef uint8_t	Reg;
						namespace THS { // Field Description
							enum {Lsb = 0};
							enum {FieldMask = 0x000000FF};
							enum {Value_Default = 0x0};
							enum {ValueMask_Default = 0x00000000};
							};
						};
					namespace IntThsHM { // Register description
						typedef uint8_t	Reg;
						namespace THS { // Field Description
							enum {Lsb = 0};
							enum {FieldMask = 0x0000007F};
							enum {Value_Default = 0x0};
							enum {ValueMask_Default = 0x00000000};
							};
						};
					}
				}
			}
		}
	}
#endif
