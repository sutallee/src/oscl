/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/


module _oscl_hw_st_lsm9ds1_magh_ {

sysinclude "stdint.h";

/** */
namespace Oscl {

/** */
namespace HW {

/** */
namespace ST {

/** */
namespace LSM9DS1 {

	/** */
	namespace Magnetometer {
		value OFFSET_X_REG_L_M	0x05
		value OFFSET_X_REG_H_M	0x06
		value OFFSET_Y_REG_L_M	0x07
		value OFFSET_Y_REG_H_M	0x08
		value OFFSET_Z_REG_L_M	0x09
		value OFFSET_Z_REG_H_M	0x0A
		value WHO_AM_I_M		0x0F
		value CTRL_REG1_M		0x20
		value CTRL_REG2_M		0x21
		value CTRL_REG3_M		0x22
		value CTRL_REG4_M		0x23
		value CTRL_REG5_M		0x24
		value STATUS_REG_M		0x27
		value OUT_X_L_M			0x28
		value OUT_X_H_M			0x29
		value OUT_Y_L_M			0x2A
		value OUT_Y_H_M			0x2B
		value OUT_Z_L_M			0x2C
		value OUT_Z_H_M			0x2D
		value INT_CFG_M			0x30
		value INT_SRC_M			0x31
		value INT_THS_L_M		0x32
		value INT_THS_H_M		0x33
		/** */
		register OffsetXRegLM uint8_t {
			field OFXM 0 8 {
				value Default	0
				}
			}
	
		/** */
		register OffsetXRegHM uint8_t {
			field OFXM 0 8 {
				value Default	0
				}
			}
	
		/** */
		register OffsetYRegLM uint8_t {
			field OFYM 0 8 {
				value Default	0
				}
			}
	
		/** */
		register OffsetYRegHM uint8_t {
			field OFYM 0 8 {
				value Default	0
				}
			}
	
		/** */
		register OffsetZRegLM uint8_t {
			field OFZM 0 8 {
				value Default	0
				}
			}
	
		/** */
		register OffsetZRegHM uint8_t {
			field OFZM 0 8 {
				value Default	0
				}
			}
	
		/** */
		register WhoAmI uint8_t {
			field ID 0 8 {
				value MagnetometerID	0x3D
				}
			}

		/** */
		register CtrlReg1M uint8_t {
			field TEMP_COMP 7 1 {
				value Default	0
				value Disable	0
				value Enable	1
				}
			field OM 5 2 {
				value Default				0
				value LowPower				0
				value MediumPerformance		1
				value HighPerformance		2
				value UltraHighPerformance	2
				}
			field DO 2 3 {
				value Default	0x04
				value _0p625Hz	0
				value _1p25Hz	1
				value _2p5Hz	2
				value _5Hz		3
				value _10Hz		4
				value _20Hz		5
				value _40Hz		6
				value _80Hz		7
				}
			field FAST_ODR 1 1 {
				value Default	0
				value Disable	0
				value Enable	1
				}
			field ST 0 1 {
				value Default	0
				value Disable	0
				value Enable	1
				}
			}

		/** */
		register CtrlReg2M uint8_t {
			field FS 5 2 {
				value Default	0
				value _4gauss	0
				value _8gauss	1
				value _12gauss	2
				value _16gauss	3
				}
			field REBOOT 3 1 {
				value Default				0
				value Normal				0
				value RebootMemoryContent	1
				}
			field SOFT_RST 2 1 {
				value Default	0
				value Reset		1
				}
			}
		register CtrlReg3M uint8_t {
			field I2C_DISABLE 7 1 {
				value Default	0
				}
			field LP 5 1 {
				value Default			0
				value DataRateSetByDO	0
				value Minimum			1
				}
			field SIM 2 1 {
				value Default	0
				value SpiWriteOnly	0
				value SpiReadWrite	1
				}
			field MD 0 2 {
				value Default			0x03
				value Continuous		0
				value SingleConversion	1
				value PowerDownModeA	2
				value PowerDownModeB	3
				}
			}	
		register CtrlReg4M uint8_t {
			field OMZ 2 2 {
				value Default				0
				value LowPower				0
				value LowPerformance		1
				value HighPerformance		2
				value UltraHighPerformance	3
				}
			field BLE 5 1 {
				value Default			0
				value LsbAtLowerAddress	0
				value MsbAtLowerAddress	1
				}
			}	
		register CtrlReg5M uint8_t {
			field FAST_READ 7 1 {
				value Default	0
				value Disable	0
				value Enable	1
				}
			field BDU 5 1 {
				value Default			0
				value Continuous		0
				value OutputAfterMsbLsb	1
				}
			}	
		register StatusRegM uint8_t {
			field ZYXOR 7 1 {
				value Default	0
				value NoOverrun	0
				value Overrun	1
				}
			field ZOR 6 1 {
				value Default	0
				value NoOverrun	0
				value Overrun	1
				}
			field YOR 5 1 {
				value Default	0
				value NoOverrun	0
				value Overrun	1
				}
			field XOR 4 1 {
				value Default	0
				value NoOverrun	0
				value Overrun	1
				}
			field ZYXDA 3 1 {
				value Default		0
				value NotAvailable	0
				value Available		1
				}
			field ZDA 2 1 {
				value Default		0
				value NotAvailable	0
				value Available		1
				}
			field YDA 1 1 {
				value Default		0
				value NotAvailable	0
				value Available		1
				}
			field XDA 0 1 {
				value Default		0
				value NotAvailable	0
				value Available		1
				}
			}
		register OutXLM uint8_t {
			}
		register OutYLM uint8_t {
			}
		register OutZLM uint8_t {
			}
		register IntCfgM uint8_t {
			field XIEN 7 1 {
				value Default	0
				value Disable	0
				value Enable	1
				}
			field YIEN 6 1 {
				value Default	0
				value Disable	0
				value Enable	1
				}
			field ZIEN 5 1 {
				value Default	0
				value Disable	0
				value Enable	1
				}
			field IEA 2 1 {
				value Default	0
				value LOW		0
				value HIGH		1
				}
			field IEL 1 1 {
				value Default		0
				value Latched		0
				value NotLatched	1
				}
			field IEN 0 1 {
				value Default	0
				value Disable	0
				value Enable	1
				}
			}
		register IntSrcM uint8_t {
			field PTH_X 7 1 {
				value Default			0
				value ThresholdExceeded	1
				}
			field PTH_Y 6 1 {
				value Default	0
				value ThresholdExceeded	1
				}
			field PTH_Z 5 1 {
				value Default	0
				value ThresholdExceeded	1
				}
			field NTH_X 4 1 {
				value Default	0
				value ThresholdExceeded	1
				}
			field NTH_Y 3 1 {
				value Default		0
				value ThresholdExceeded	1
				}
			field NTH_Z 2 1 {
				value Default		0
				value ThresholdExceeded	1
				}
			field MROI 1 1 {
				value Default	0
				value Overflow	1
				}
			field INT 0 1 {
				value Default			0
				value InterruptPending	1
				}
			}
		register IntThsLM uint8_t {
			field THS 0 8 {
				value Default	0
				}
			}
		register IntThsHM uint8_t {
			field THS 0 7 {
				value Default	0
				}
			}
		}
}
}
}
}
}

