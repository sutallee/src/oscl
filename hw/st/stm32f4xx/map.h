/*
   Copyright (C) 2011 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_hw_st_stm32f2xxx_maph_
#define _oscl_hw_st_stm32f2xxx_maph_
#include <stdint.h>

#include "oscl/hw/st/stm32/f2f4f7/dma/map.h"
#include "oscl/hw/st/stm32/f2f4/usart/map.h"

extern Oscl::St::Stm32::F2F4F7::Dma::Map	STM32F4xx_DMA2;
extern Oscl::St::Stm32::F2F4F7::Dma::Map	STM32F4xx_DMA1;
extern Oscl::St::Stm32::F2F4::Usart::Map	STM32F4xx_USART1;
extern Oscl::St::Stm32::F2F4::Usart::Map	STM32F4xx_USART2;
extern Oscl::St::Stm32::F2F4::Usart::Map	STM32F4xx_USART3;
extern Oscl::St::Stm32::F2F4::Usart::Map	STM32F4xx_UART4;
extern Oscl::St::Stm32::F2F4::Usart::Map	STM32F4xx_UART5;
extern Oscl::St::Stm32::F2F4::Usart::Map	STM32F4xx_USART6;
extern Oscl::St::Stm32::F2F4::Usart::Map	STM32F4xx_UART7;
extern Oscl::St::Stm32::F2F4::Usart::Map	STM32F4xx_UART8;

extern volatile uint8_t	STM32F4xx_FSMC;
extern volatile uint8_t	STM32F4xx_RNG;
extern volatile uint8_t	STM32F4xx_HASH;
extern volatile uint8_t	STM32F4xx_CRYP;
extern volatile uint8_t	STM32F4xx_DCMI;
extern volatile uint8_t	STM32F4xx_USB_OTG_FS;
extern volatile uint8_t	STM32F4xx_USB_OTG_HS;
extern volatile uint8_t	STM32F4xx_ETHERNET_MAC;
extern volatile uint8_t	STM32F4xx_BKPSRAM;
extern volatile uint8_t	STM32F4xx_FLASH;
extern volatile uint8_t	STM32F4xx_RCC;
extern volatile uint8_t	STM32F4xx_CRC;
extern volatile uint8_t	STM32F4xx_GPIOI;
extern volatile uint8_t	STM32F4xx_GPIOH;
extern volatile uint8_t	STM32F4xx_GPIOG;
extern volatile uint8_t	STM32F4xx_GPIOF;
extern volatile uint8_t	STM32F4xx_GPIOE;
extern volatile uint8_t	STM32F4xx_GPIOD;
extern volatile uint8_t	STM32F4xx_GPIOC;
extern volatile uint8_t	STM32F4xx_GPIOB;
extern volatile uint8_t	STM32F4xx_GPIOA;
extern volatile uint8_t	STM32F4xx_TIM11;
extern volatile uint8_t	STM32F4xx_TIM10;
extern volatile uint8_t	STM32F4xx_TIM9;
extern volatile uint8_t	STM32F4xx_EXTI;
extern volatile uint8_t	STM32F4xx_SYSCFG;
extern volatile uint8_t	STM32F4xx_SPI1;
extern volatile uint8_t	STM32F4xx_SDIO;
extern volatile uint8_t	STM32F4xx_ADC;
extern volatile uint8_t	STM32F4xx_TIM8;
extern volatile uint8_t	STM32F4xx_TIM1;
extern volatile uint8_t	STM32F4xx_DAC;
extern volatile uint8_t	STM32F4xx_PWR;
extern volatile uint8_t	STM32F4xx_CAN2;
extern volatile uint8_t	STM32F4xx_CAN1;
extern volatile uint8_t	STM32F4xx_I2C3;
extern volatile uint8_t	STM32F4xx_I2C2;
extern volatile uint8_t	STM32F4xx_I2C1;
extern volatile uint8_t	STM32F4xx_SPI3_I2S3;
extern volatile uint8_t	STM32F4xx_SPI2_I2S2;
extern volatile uint8_t	STM32F4xx_IWDG;
extern volatile uint8_t	STM32F4xx_WWDG;
extern volatile uint8_t	STM32F4xx_RTC_BKP;
extern volatile uint8_t	STM32F4xx_TIM14;
extern volatile uint8_t	STM32F4xx_TIM13;
extern volatile uint8_t	STM32F4xx_TIM12;
extern volatile uint8_t	STM32F4xx_TIM7;
extern volatile uint8_t	STM32F4xx_TIM6;
extern volatile uint8_t	STM32F4xx_TIM5;
extern volatile uint8_t	STM32F4xx_TIM4;
extern volatile uint8_t	STM32F4xx_TIM3;
extern volatile uint8_t	STM32F4xx_TIM2;

extern uint8_t	STM32F4xx_SHARED_RAM;

#endif
