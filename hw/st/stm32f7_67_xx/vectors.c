#include <stdint.h>

uintptr_t _fixedVectorTable[] = {
	_fixedVectorInitialStackPointer,
	_fixedVectorReset,
	_fixedVectorNmi,
	_fixedVectorHardFault,
	_fixedVectorMemManage,
	_fixedVectorBusFault,
	_fixedVectorUsageFault,
	_fixedVectorReserved0x1C,
	_fixedVectorReserved0x20,
	_fixedVectorReserved0x24,
	_fixedVectorReserved0x28,
	_fixedVectorSvCall,
	_fixedVectorDebugMonitor,
	_fixedVectorReserved0x34,
	_fixedVectorPendSV,
	_fixedVectorSysTick
	};

/*	This defines the STM32F766xxx and STM32F77xxx user vectors.
	This does NOT include the immediately preceding fixed vectors.
	
 */
uintptr_t _stm32f7VectorTable[] = {
	_isrWWDG,			// [0]	Window Watchdog interrupt
	_isrPVD,			// [1]	PVD (Programmable Voltage Detector) through EXTI line detection
	_isrTAMP_STAMP,		// [2]	Tamper and TimeStamp interrupts through EXTI line
	_isrRTC_WKUP,		// [3]	RTC (Real Time Clock) Wakeup interrupt through the EXTI line
	_isrFLASH,			// [4]	Flash global interrupt
	_isrRCC,			// [5]	RCC (Reset and Clock Control) global interrupt
	_isrEXTI0,			// [6]	EXTI (External Interrupt) Line0 interrupt
	_isrEXTI1,			// [7]	EXTI (External Interrupt) Line1 interrupt
	_isrEXTI2,			// [8]	EXTI (External Interrupt) Line2 interrupt
	_isrEXTI3,			// [9]	EXTI (External Interrupt) Line3 interrupt
	_isrEXTI4,			// [10]	EXTI (External Interrupt) Line4 interrupt
	_isrDMA1_Stream0,	// [11]	DMA1 Stream0 global interrupt
	_isrDMA1_Stream1,	// [12]	DMA1 Stream1 global interrupt
	_isrDMA1_Stream2,	// [13]	DMA1 Stream2 global interrupt
	_isrDMA1_Stream3,	// [14]	DMA1 Stream3 global interrupt
	_isrDMA1_Stream4,	// [15]	DMA1 Stream4 global interrupt
	_isrDMA1_Stream5,	// [16]	DMA1 Stream5 global interrupt
	_isrDMA1_Stream6,	// [17]	DMA1 Stream6 global interrupt
	_isrADC,			// [18] ADC (Analog to Digital Converter) global interrupts
	_isrCAN1_TX,		// [19] CAN1 TX interrupts
	_isrCAN1_RX0,		// [20] CAN1 RX0 interrupts
	_isrCAN1_RX1,		// [21] CAN1 RX1 interrupt
	_isrCAN1_SCE,		// [22] CAN1 SCE (Status Change Error) interrupt
	_isrEXTI9_5,		// [23]	EXTI (External Interrupt) Line[9:5] interrupts
	_isrTIM1_BRK_TIM9,	// [24] TIM1 Break interrupt and TIM9 global interrupt
	_isrTIM1_UP_TIM10,	// [25] TIM1 Update interrupt and TIM10 global interrupt
	_isrTIM1_TRG_COM_TIM11,	// [26] TIM1 Trigger and Commutation interrupts and TIM11 global interrupt
	_isrTIM1_CC,		// [27] TIM1 Capture Compare interrupt
	_isrTIM2,			// [28] TIM2 global interrupt
	_isrTIM3,			// [29] TIM3 global interrupt
	_isrTIM4,			// [30]	TIM4 global interrupt
	_isrI2C1_EV,		// [31] I2C1 event interrupt
	_isrI2C1_ER,		// [32] I2C1 error interrupt
	_isrI2C2_EV,		// [33] I2C2 event interrupt
	_isrI2C2_ER,		// [34] I2C2 error interrupt
	_isrSPI1,			// [35]	SPI1 global interrupt
	_isrSPI2,			// [36]	SPI2 global interrupt
	_isrUSART1,			// [37]	USART1 global interrupt
	_isrUSART2,			// [38]	USART2 global interrupt
	_isrUSART3,			// [39]	USART3 global interrupt
	_isrEXTI15_10,		// [40]	EXTI (External Interrupt) Line[15:10] interrupts
	_isrRTC_Alarm,		// [41]	RTC (Real Time Clock) Alarms (A and B) through EXTI line interrupt
	_isrOTG_FS_WKUP,	// [42]	USB On-The-Go FS Wakeup through EXTI line interrupt
	_isrTIM8_BRK_TIM12,	// [43] TIM8 Break interrupt and TIM12 global interrupt
	_isrTIM8_UP_TIM13,	// [44] TIM8 Update interrupt and TIM13 global interrupt
	_isrTIM8_TRG_COM_TIM14,	// [45]	TIM8 Trigger and Commutation interrupts and TIM14 global interrupt
	_isrTIM8_CC,			// [46] TIM8 Capture Compare interrupt
	_isrDMA_Stream7,		// [47] DMA1 Stream7 global interrupt
	_isrFMC,				// [48] FMC (Flexible Memory Controller) global interrupt
	_isrSDMMC1,				// [49] SDMMC1 (SD/SDIO/MMC Card) global interrupt
	_isrTIM5,				// [50] TIM5 global interrupt
	_isrSPI3,				// [51] SPI3 global interrupt
	_isrUART4,				// [52]	UART4 global interrupt
	_isrUART5,				// [53]	UART5 global interrupt
	_isrTIM6_DAC,			// [54] TIM6 global interrupr, DAC1 and DAC2 underrun error interrupts
	_isrTIM7,				// [55] TIM7 global interrupt
	_isrDMA2_Stream0,		// [56]	DMA2 Stream0 global interrupt
	_isrDMA2_Stream1,		// [57] DMA2 Stream1 global interrupt
	_isrDMA2_Stream2,		// [58] DMA2 Stream2 global interrupt
	_isrDMA2_Stream3,		// [59] DMA2 Stream3 global interrupt
	_isrDMA2_Stream4,		// [60] DMA2 Stream4 global interrupt
	_isrETH,				// [61] Ethernet global interrupt
	_isrETH_WKUP,			// [62] Ethernet Wakup through EXTI line interrupt
	_isrCAN2_TX,			// [63]	CAN2 TX interrupts
	_isrCAN2_RX0,			// [64]	CAN2 RX0 interrupts
	_isrCAN2_RX1,			// [65]	CAN2 RX1 interrupt
	_isrCAN2_SCE,			// [66]	CAN2 SCE (Status Change Error) interrupt
	_isrOTG_FS,				// [67]	USB On The Go FS global interrupt
	_isrDMA2_Stream5,		// [68]	DMA2 Stream5 global interrupt
	_isrDMA2_Stream6,		// [69]	DMA2 Stream6 global interrupt
	_isrDMA2_Stream7,		// [70]	DMA2 Stream7 global interrupt
	_isrUSART6,				// [71]	USART6 global interrupt
	_isrI2C3_EV,			// [72]	I2C3 event interrupt
	_isrI2C3_ER,			// [73]	I2C3 error interrupt
	_isrOTG_HS_EP1_OUT,		// [74]	USB On The Go HS End Point 1 Out global interrupt
	_isrOTG_HS_EP1_IN,		// [75]	USB On The Go HS End Point 1 In global interrupt
	_isrOTG_HS_WKUP,		// [76] USB On The Go HS Wakeup through EXTI interrupt
	_isrOTG_HS,				// [77] USB On The Go HS global interrupt
	_isrDCMI,				// [78]	DCMI (Digital Camera Interface) global interrupt
	_isrCRYP,				// [79]	CRYP crypto global interrupt
	_isrHASH_RNG,			// [80]	Hash and Rng global interrupt
	_isrFPU,				// [81]	FPU global interrupt
	_isrUART7,				// [82] UART7 global interrupt
	_isrUART8,				// [83] UART8 global interrupt
	_isrSPI4,				// [84]	SPI4 global interrupt
	_isrSPI5,				// [85]	SPI5 global interrupt
	_isrSPI6,				// [86]	SPI6 global interrupt
	_isrSAI1,				// [87]	SAI1 (Serial Audio Interface) global interrupt
	_isrLCD_TFT,			// [88]	LCD-TFT global interrupt
	_isrLCD_TFT_ERR,		// [89]	LCD-TFT global Error interrupt
	_isrDMA2D,				// [90]	DMA2D (Chrom-ART Accelerator) global interrupt
	_isrSAI2,				// [91]	SAI2 (Serial Audio Interface) global interrupt
	_isrQuadSPI,			// [92]	QuadSPI global interrupt
	_isrLP_Timer1,			// [93]	LP (Low Power) Timer1 global interrupt
	_isrHDMI_CEC,			// [94]	HDMI-CEC (High Definition Multimedia Interface - Consumer Electronics Control) global interrupt
	_isrI2C4_EV,			// [95]	I2C4 event interrupt
	_isrI2C4_ER,			// [96]	I2C4 error interrupt
	_isrSPDIFRX,			// [97]	SPDIFRX (SPIDIF Receiver Interface) global interrupt
	_isrDSIHOST,			// [98]	DSI (Display Serial Interface) host global interrupt
	_isrDFSDM1_FLT0,		// [99]		DFSDM1 (Digital Filter for Sigma Delta Modulators) Filter 0 global interrupt
	_isrDFSDM1_FLT1,		// [100]	DFSDM1 (Digital Filter for Sigma Delta Modulators) Filter 1 global interrupt
	_isrDFSDM1_FLT2,		// [101]	DFSDM1 (Digital Filter for Sigma Delta Modulators) Filter 2 global interrupt
	_isrDFSDM1_FLT3,		// [102]	DFSDM1 (Digital Filter for Sigma Delta Modulators) Filter 3 global interrupt
	_isrSDMMC2,				// [103]	SDMMC2 (SD/SDIO/MMC Card) global interrupt
	_isrCAN3_TX,			// [104]	CAN3 TX interrupt
	_isrCAN3_RX0,			// [105]	CAN3 RX0 interrupt
	_isrCAN3_RX1,			// [106]	CAN3 RX1 interrupt
	_isrCAN3_SCE,			// [107]	CAN3 SCE (Status Change Error) interrupt
	_isrJPEG,				// [108]	JPEG global interrupt
	_isrMDIOS,				// [109]	MDIO slave global interrupt
	};

