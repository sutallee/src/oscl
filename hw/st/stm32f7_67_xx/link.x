/*
   Copyright (C) 2021 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

/* Default linker script, for normal executables */
OUTPUT_FORMAT("elf32-littlearm", "elf32-bigarm", "elf32-littlearm")
OUTPUT_ARCH(arm)

ENTRY(__cortexM3Reset)

/* The memory size is 256KB to coincide with the simulator.
   Don't change either without considering the other.  */
MEMORY {
	sysram		: o = 0x20000000, l = 0x00020000
	backram		: o = 0x20000000, l = 0x00001000
	vectors		: o = 0x00000000, l = 0x00000184
	flash		: o = 0x00000184, l = 0x00100000	/* run-time alias of mainflash */
	mainflash	: o = 0x08000000, l = 0x00100000
	sysflash	: o = 0x1FFF0000, l = 0x00007800
	otpflash	: o = 0x1FFF7800, l = 0x00000210
	optflash	: o = 0x1FFFC000, l = 0x00000010
	}

SECTIONS
{
	.vectors : {
	  /* Use something like this to place a specific
	     function's address into the vector table.
	     LONG (ABSOLUTE (_foobar)).  */
	  *(.vectors)
/*	LONG(ABSOLUTE(__cortexM3Reserved000)) */
	LONG(_stack)
	LONG(ABSOLUTE(__cortexM3Reset) + 1)
	LONG(ABSOLUTE(__cortexM3NMI) + 1)
	LONG(ABSOLUTE(__cortexM3HardFault)+1)
	LONG(ABSOLUTE(__cortexM3MemManage)+1)
	LONG(ABSOLUTE(__cortexM3BusFault)+1)
	LONG(ABSOLUTE(__cortexM3UsageFault)+1)
	LONG(ABSOLUTE(__cortexM3Reserved01C)+1)
	LONG(ABSOLUTE(__cortexM3Reserved020)+1)
	LONG(ABSOLUTE(__cortexM3Reserved024)+1)
	LONG(ABSOLUTE(__cortexM3Reserved028)+1)
	LONG(ABSOLUTE(__cortexM3SVCall)+1)
	LONG(ABSOLUTE(__cortexM3Reserved034)+1)
	LONG(ABSOLUTE(__cortexM3PendSV)+1)
	LONG(ABSOLUTE(__cortexM3SysTick)+1)
	LONG(ABSOLUTE(__cortexM3WWDG)+1)
	LONG(ABSOLUTE(__cortexM3PVD)+1)
	LONG(ABSOLUTE(__cortexM3TAMP_STAMP)+1)
	LONG(ABSOLUTE(__cortexM3RTC_WKUP)+1)
	LONG(ABSOLUTE(__cortexM3FLASH)+1)
	LONG(ABSOLUTE(__cortexM3RCC)+1)
	LONG(ABSOLUTE(__cortexM3EXTI0)+1)
	LONG(ABSOLUTE(__cortexM3EXTI1)+1)
	LONG(ABSOLUTE(__cortexM3EXTI2)+1)
	LONG(ABSOLUTE(__cortexM3EXTI3)+1)
	LONG(ABSOLUTE(__cortexM3EXTI4)+1)
	LONG(ABSOLUTE(__cortexM3DMA1_Stream0)+1)
	LONG(ABSOLUTE(__cortexM3DMA1_Stream1)+1)
	LONG(ABSOLUTE(__cortexM3DMA1_Stream2)+1)
	LONG(ABSOLUTE(__cortexM3DMA1_Stream3)+1)
	LONG(ABSOLUTE(__cortexM3DMA1_Stream4)+1)
	LONG(ABSOLUTE(__cortexM3DMA1_Stream5)+1)
	LONG(ABSOLUTE(__cortexM3DMA1_Stream6)+1)
	LONG(ABSOLUTE(__cortexM3ADC)+1)
	LONG(ABSOLUTE(__cortexM3CAN1_TX)+1)
	LONG(ABSOLUTE(__cortexM3CAN1_RX0)+1)
	LONG(ABSOLUTE(__cortexM3CAN1_RX1)+1)
	LONG(ABSOLUTE(__cortexM3CAN1_SCE)+1)
	LONG(ABSOLUTE(__cortexM3EXTI9_5)+1)
	LONG(ABSOLUTE(__cortexM3TIM1_BRK_TIM9)+1)
	LONG(ABSOLUTE(__cortexM3TIM1_UP_TIM10)+1)
	LONG(ABSOLUTE(__cortexM3TIM1_TRG_COM_TIM11)+1)
	LONG(ABSOLUTE(__cortexM3TIM1_CC)+1)
	LONG(ABSOLUTE(__cortexM3TIM2)+1)
	LONG(ABSOLUTE(__cortexM3TIM3)+1)
	LONG(ABSOLUTE(__cortexM3TIM4)+1)
	LONG(ABSOLUTE(__cortexM3I2C1_EV)+1)
	LONG(ABSOLUTE(__cortexM3I2C1_ER)+1)
	LONG(ABSOLUTE(__cortexM3I2C2_EV)+1)
	LONG(ABSOLUTE(__cortexM3I2C2_ER)+1)
	LONG(ABSOLUTE(__cortexM3SPI1)+1)
	LONG(ABSOLUTE(__cortexM3SPI2)+1)
	LONG(ABSOLUTE(__cortexM3USART1)+1)
	LONG(ABSOLUTE(__cortexM3USART2)+1)
	LONG(ABSOLUTE(__cortexM3USART3)+1)
	LONG(ABSOLUTE(__cortexM3USART3)+1)
	LONG(ABSOLUTE(__cortexM3EXTI15_10)+1)
	LONG(ABSOLUTE(__cortexM3RTC_Alarm)+1)
	LONG(ABSOLUTE(__cortexM3OTG_FS_WKUP)+1)
	LONG(ABSOLUTE(__cortexM3TIM8_BRK_TIM12)+1)
	LONG(ABSOLUTE(__cortexM3TIM8_UP_TIM13)+1)
	LONG(ABSOLUTE(__cortexM3TIM8_TRG_COM_TIM14)+1)
	LONG(ABSOLUTE(__cortexM3DMA1_Stream7)+1)
	LONG(ABSOLUTE(__cortexM3FSMC)+1)
	LONG(ABSOLUTE(__cortexM3SDIO)+1)
	LONG(ABSOLUTE(__cortexM3TIM5)+1)
	LONG(ABSOLUTE(__cortexM3SPI3)+1)
	LONG(ABSOLUTE(__cortexM3UART4)+1)
	LONG(ABSOLUTE(__cortexM3UART5)+1)
	LONG(ABSOLUTE(__cortexM3TIM6_DAC)+1)
	LONG(ABSOLUTE(__cortexM3TIM7)+1)
	LONG(ABSOLUTE(__cortexM3DMA2_Stream0)+1)
	LONG(ABSOLUTE(__cortexM3DMA2_Stream1)+1)
	LONG(ABSOLUTE(__cortexM3DMA2_Stream2)+1)
	LONG(ABSOLUTE(__cortexM3DMA2_Stream3)+1)
	LONG(ABSOLUTE(__cortexM3DMA2_Stream4)+1)
	LONG(ABSOLUTE(__cortexM3ETH)+1)
	LONG(ABSOLUTE(__cortexM3ETH_WKUP)+1)
	LONG(ABSOLUTE(__cortexM3CAN2_TX)+1)
	LONG(ABSOLUTE(__cortexM3CAN2_RX0)+1)
	LONG(ABSOLUTE(__cortexM3CAN2_RX1)+1)
	LONG(ABSOLUTE(__cortexM3CAN2_SCE)+1)
	LONG(ABSOLUTE(__cortexM3OTG_FS)+1)
	LONG(ABSOLUTE(__cortexM3DMA2_Stream5)+1)
	LONG(ABSOLUTE(__cortexM3DMA2_Stream6)+1)
	LONG(ABSOLUTE(__cortexM3DMA2_Stream7)+1)
	LONG(ABSOLUTE(__cortexM3USART6)+1)
	LONG(ABSOLUTE(__cortexM3I2C3_EV)+1)
	LONG(ABSOLUTE(__cortexM3I2C3_ER)+1)
	LONG(ABSOLUTE(__cortexM3OTG_HS_EP1_OUT)+1)
	LONG(ABSOLUTE(__cortexM3OTG_HS_EP1_IN)+1)
	LONG(ABSOLUTE(__cortexM3OTG_HS_WKUP)+1)
	LONG(ABSOLUTE(__cortexM3OTG_HS)+1)
	LONG(ABSOLUTE(__cortexM3DCMI)+1)
	LONG(ABSOLUTE(__cortexM3CRYP)+1)
	LONG(ABSOLUTE(__cortexM3HASH_RNG)+1)
	LONG(ABSOLUTE(0))
	} > vectors AT>mainflash

	/* Read-only sections, merged into text segment: */
	.text : {
		*(.interp)
		*(.hash)
		*(.dynsym)
		*(.dynstr)
		*(.gnu.version)
		*(.gnu.version_d)
		*(.gnu.version_r)
		*(.rel.init)
		*(.rela.init)
		*(.rel.text .rel.text.* .rel.gnu.linkonce.t.*)
		*(.rela.text .rela.text.* .rela.gnu.linkonce.t.*)
		*(.rel.fini)
		*(.rela.fini)
		*(.rel.rodata .rel.rodata.* .rel.gnu.linkonce.r.*)
		*(.rela.rodata .rela.rodata.* .rela.gnu.linkonce.r.*)
		*(.rel.data .rel.data.* .rel.gnu.linkonce.d.*)
		*(.rela.data .rela.data.* .rela.gnu.linkonce.d.*)
		*(.gnu.linkonce.d.*)
		*(.rel.tdata .rel.tdata.* .rel.gnu.linkonce.td.*)
		*(.rela.tdata .rela.tdata.* .rela.gnu.linkonce.td.*)
		*(.rel.ctors)
		*(.rela.ctors)
		*(.rel.dtors)
		*(.rela.dtors)
		KEEP (*(.init))
    	*(.text .stub .text.* .gnu.linkonce.t.*)
		/* .gnu.warning sections are handled specially by elf32.em.  */
    	*(.gnu.warning)
		KEEP (*(.fini))
		*(.rodata .rodata.* .gnu.linkonce.r.*)
		*(.rodata1)
		/* Adjust the address for the data segment.  We want to adjust up to
			the same address within the page on the next page up.  */
		. = ALIGN(2) + (. & (2 - 1));
		/* Ensure the __preinit_array_start label is properly aligned.  We
			could instead move the label definition inside the section, but
			the linker would then create the section even if it turns out to
			be empty, which isn't pretty.  */
		. = ALIGN(32 / 8);
		PROVIDE (__preinit_array_start = .);
		*(.preinit_array)
		PROVIDE (__preinit_array_end = .);
		PROVIDE (__init_array_start = .);
		*(.init_array)
		PROVIDE (__init_array_end = .);
		PROVIDE (__fini_array_start = .);
		*(.fini_array)
		PROVIDE (__fini_array_end = .);
		PROVIDE (__etext = .);
		PROVIDE (_etext = .);
		PROVIDE (etext = .);
		} > flash AT>mainflash
	.ctors : {
		/* gcc uses crtbegin.o to find the start of
			the constructors, so we make sure it is
			first.  Because this is a wildcard, it
			doesn't matter if the user does not
			actually link against crtbegin.o; the
			linker won't look for a file to match a
			wildcard.  The wildcard also means that it
			doesn't matter which directory crtbegin.o
			is in.  */
		KEEP (*crtbegin.o(.ctors))
		/* We don't want to include the .ctor section from
			from the crtend.o file until after the sorted ctors.
			The .ctor section from the crtend file contains the
			end of ctors marker and it must be last */
		KEEP (*(EXCLUDE_FILE (*crtend.o ) .ctors))
		KEEP (*(SORT(.ctors.*)))
		KEEP (*(.ctors))
		} > flash AT>mainflash

	.dtors : {
		KEEP (*crtbegin.o(.dtors))
		KEEP (*(EXCLUDE_FILE (*crtend.o ) .dtors))
		KEEP (*(SORT(.dtors.*)))
		KEEP (*(.dtors))
		} > flash AT>mainflash
	.jcr : {
		KEEP (*(.jcr))
		} > flash AT>mainflash

/*	.data : AT(ADDR(.jcr) + SIZEOF(.jcr)) { */
	.data : {
		*(.rel.sdata .rel.sdata.* .rel.gnu.linkonce.s.*)
		*(.rela.sdata .rela.sdata.* .rela.gnu.linkonce.s.*)
		*(.rel.sdata2 .rel.sdata2.* .rel.gnu.linkonce.s2.*)
		*(.rela.sdata2 .rela.sdata2.* .rela.gnu.linkonce.s2.*)
		*(.rel.plt)
		*(.rela.plt)
		*(.plt)
		*(.sdata2 .sdata2.* .gnu.linkonce.s2.*)
		*(.eh_frame_hdr)
		*(.data .data.*)
		SORT(CONSTRUCTORS)
		*(.data1)
		*(.tdata .tdata.* .gnu.linkonce.td.*)
		KEEP (*(.eh_frame))
		*(.gcc_except_table)
		*(.dynamic)
		*(.sdata .sdata.* .gnu.linkonce.s.*)
		_edata = .;
		PROVIDE (edata = .);
		} > sysram AT>mainflash
	PROVIDE( ___text_data_start = ADDR(.jcr) + SIZEOF(.jcr) );
	PROVIDE( ___data_start = ADDR(.data));
	PROVIDE( ___data_end = ADDR(.data) + SIZEOF(.data) );
	PROVIDE( idata_end = ADDR(.jcr) + SIZEOF(.jcr) + SIZEOF(.data) );
	.bss (NOLOAD) : {
		__bss_start = .;
		*(.rel.tbss .rel.tbss.* .rel.gnu.linkonce.tb.*)
		*(.rela.tbss .rela.tbss.* .rela.gnu.linkonce.tb.*)
		*(.rel.sbss .rel.sbss.* .rel.gnu.linkonce.sb.*)
		*(.rela.sbss .rela.sbss.* .rela.gnu.linkonce.sb.*)
		*(.rel.sbss2 .rel.sbss2.* .rel.gnu.linkonce.sb2.*)
		*(.rela.sbss2 .rela.sbss2.* .rela.gnu.linkonce.sb2.*)
		*(.rel.bss .rel.bss.* .rel.gnu.linkonce.b.*)
		*(.rela.bss .rela.bss.* .rela.gnu.linkonce.b.*)
		*(.sbss2 .sbss2.* .gnu.linkonce.sb2.*)
		*(.tbss .tbss.* .gnu.linkonce.tb.*) *(.tcommon)
		PROVIDE (__sbss_start = .);
		PROVIDE (___sbss_start = .);
		*(.dynsbss)
		*(.sbss .sbss.* .gnu.linkonce.sb.*)
		*(.scommon)
		PROVIDE (__sbss_end = .);
		PROVIDE (___sbss_end = .);
		*(.dynbss)
		*(.bss .bss.* .gnu.linkonce.b.*)
		*(COMMON)
		/* Align here to ensure that the .bss section occupies space up to
			_end.  Align after .bss to ensure correct alignment even if the
			.bss section disappears because there are no input sections.  */
		. = ALIGN(32 / 8);
		. = ALIGN(32 / 8);
		_end = .;
		PROVIDE (end = .);
		} > sysram
	/* Stabs debugging sections.  */
	.stab          0 : { *(.stab) }
	.stabstr       0 : { *(.stabstr) }
	.stab.excl     0 : { *(.stab.excl) }
	.stab.exclstr  0 : { *(.stab.exclstr) }
	.stab.index    0 : { *(.stab.index) }
	.stab.indexstr 0 : { *(.stab.indexstr) }
	.comment       0 : { *(.comment) }
	/* DWARF debug sections.
		Symbols in the DWARF debugging sections are relative to the beginning
		of the section so we begin them at 0.  */
	/* DWARF 1 */
	.debug          0 : { *(.debug) }
	.line           0 : { *(.line) }
	/* GNU DWARF 1 extensions */
	.debug_srcinfo  0 : { *(.debug_srcinfo) }
	.debug_sfnames  0 : { *(.debug_sfnames) }
	/* DWARF 1.1 and DWARF 2 */
	.debug_aranges  0 : { *(.debug_aranges) }
	.debug_pubnames 0 : { *(.debug_pubnames) }
	/* DWARF 2 */
	.debug_info     0 : { *(.debug_info .gnu.linkonce.wi.*) }
	.debug_abbrev   0 : { *(.debug_abbrev) }
	.debug_line     0 : { *(.debug_line) }
	.debug_frame    0 : { *(.debug_frame) }
	.debug_str      0 : { *(.debug_str) }
	.debug_loc      0 : { *(.debug_loc) }
	.debug_macinfo  0 : { *(.debug_macinfo) }
	/* SGI/MIPS DWARF 2 extensions */
	.debug_weaknames 0 : { *(.debug_weaknames) }
	.debug_funcnames 0 : { *(.debug_funcnames) }
	.debug_typenames 0 : { *(.debug_typenames) }
	.debug_varnames  0 : { *(.debug_varnames) }
	.stack (NOLOAD) : {
		*(.stack)
		}  > sysram
}
