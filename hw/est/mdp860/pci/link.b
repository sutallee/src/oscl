#
# The project overrides.b should define:
#
#FINAL_LINK_FLAGS=   \
#	-T $(srcroot)/src/oscl/hw/est/mdp8xx/mem4Mram512Kflash.map \
#	$(BSP_LINK_FLAGS)
#
# The first "-T" argument defines the size of RAM and Flash, and
# may vary with the implementation.
#

BSP_LINK_FLAGS=	\
	-T $(srcroot)/src/oscl/hw/est/mdp8xx/link.map \
	-T $(srcroot)/src/oscl/hw/est/mdp860/link.map \
	-T $(srcroot)/src/oscl/hw/est/mdp860/pci/link.map \
	-T $(srcroot)/src/oscl/hw/est/mdp8xx/link.x

