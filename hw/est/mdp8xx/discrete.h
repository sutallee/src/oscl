/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_hw_est_mdp8xx_discreteh_
#define _oscl_hw_est_mdp8xx_discreteh_
#include "cs.h"

/*	The Discrete registers and EEPROM for the MDP8xxPro
	are controlled by a single chip-select (CS5). The hardware
	design is poorly implemented (fucked). The Flash SIMM present
	status bits are attached to the PowerPC Data bus bits D[8:15].
 */

/** */
namespace Oscl {
/** */
namespace Est {
/** */
namespace Mdp8xxPro {

/** */
typedef struct Control {	// Write ONLY
	/** */
	volatile LedControl::Reg			ledControl;
	/** */
	volatile uint8_t					reserved0;
	/** */
	volatile PcmciaPowerControl::Reg	pcmciaPowerControl;
	/** */
	volatile uint8_t					reserved1;
	} Control;

/** */
namespace SixteenBitPort {

/** */
typedef struct Status {	// Read ONLY
	/** */
	volatile FlashSimmPresent::Reg	flashPresent;
	/** */
	volatile DramPciPresent::Reg	dramAndPciPresent;
	/** */
	volatile uint8_t				reserved1;
	/** */
	volatile uint8_t				reserved2;
	}Status;

/** */
typedef union Discrete {
	/** */
	Control		control;	// Write ONLY
	/** */
	Status		status;		// Read ONLY
	/** */
	uint8_t	padding[8*1024];
	} Discrete;

/** */
typedef struct EepromLocation {
	/** */
	uint8_t	reserved;
	/** */
	uint8_t	data;
	}EepromLocation;

enum { EepromSize = (4*1024) };	// Upper 4K of 8K Eeprom not accessible

/** */
typedef struct Map {
	/** */
	Discrete		discrete;
	/** */
	EepromLocation	eeprom[EepromSize];
	} Map;
}

/** */
namespace EightBitPort {

/** */
typedef struct Status {	// Read ONLY
	/** */
	volatile uint8_t				reserved0;
	/** */
	volatile DramPciPresent::Reg	dramAndPciPresent;
	/** */
	volatile uint8_t				reserved1;
	/** */
	volatile uint8_t				reserved2;
	}Status;

/** */
typedef union Discrete {
	/** */
	Control		control;	// Write ONLY
	/** */
	Status		status;		// Read ONLY
	/** */
	uint8_t	padding[8*1024];
	} Discrete;

/** */
typedef struct EepromLocation {
	/** */
	uint8_t	data;
	}EepromLocation;

enum { EepromSize = (8*1024) };

/** */
typedef struct Map {
	/** */
	Discrete		discrete;
	/** */
	EepromLocation	eeprom[EepromSize];
	} Map;
}

/** */
typedef union Discrete {
	/** CS5 set to 8-bit port.
		In this mode:
			- All 8K of the EEPROM is accessible.
			- Flash SIMM status registers are NOT accessible.
	 */
	EightBitPort::Map	eightBitPort;
	/** CS5 set to 16-bit port.
		In this mode:
			- Only 4K of the EEPROM is accessible.
			- All "present" status registers are accessible.
	 */
	SixteenBitPort::Map	sixteenBitPort;
	}Discrete;

}
}
}


#endif
