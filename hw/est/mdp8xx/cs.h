/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _hw_est_mdp8xx_cs_
#define _hw_est_mdp8xx_cs_
#include <stdint.h>
namespace Oscl { // Namespace description
	namespace Est { // Namespace description
		namespace Mdp8xxPro { // Namespace description
			namespace LedControl { // Register description
				typedef uint8_t	Reg;
				namespace LED0 { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x00000001};
					enum {Value_Illuminate = 0x1};
					enum {ValueMask_Illuminate = 0x00000001};
					enum {Value_Extinguish = 0x0};
					enum {ValueMask_Extinguish = 0x00000000};
					};
				namespace LED1 { // Field Description
					enum {Lsb = 1};
					enum {FieldMask = 0x00000002};
					enum {Value_Illuminate = 0x1};
					enum {ValueMask_Illuminate = 0x00000002};
					enum {Value_Extinguish = 0x0};
					enum {ValueMask_Extinguish = 0x00000000};
					};
				namespace LED2 { // Field Description
					enum {Lsb = 2};
					enum {FieldMask = 0x00000004};
					enum {Value_Illuminate = 0x1};
					enum {ValueMask_Illuminate = 0x00000004};
					enum {Value_Extinguish = 0x0};
					enum {ValueMask_Extinguish = 0x00000000};
					};
				namespace LED3 { // Field Description
					enum {Lsb = 3};
					enum {FieldMask = 0x00000008};
					enum {Value_Illuminate = 0x1};
					enum {ValueMask_Illuminate = 0x00000008};
					enum {Value_Extinguish = 0x0};
					enum {ValueMask_Extinguish = 0x00000000};
					};
				namespace LED4 { // Field Description
					enum {Lsb = 4};
					enum {FieldMask = 0x00000010};
					enum {Value_Illuminate = 0x1};
					enum {ValueMask_Illuminate = 0x00000010};
					enum {Value_Extinguish = 0x0};
					enum {ValueMask_Extinguish = 0x00000000};
					};
				namespace LED5 { // Field Description
					enum {Lsb = 5};
					enum {FieldMask = 0x00000020};
					enum {Value_Illuminate = 0x1};
					enum {ValueMask_Illuminate = 0x00000020};
					enum {Value_Extinguish = 0x0};
					enum {ValueMask_Extinguish = 0x00000000};
					};
				namespace LED6 { // Field Description
					enum {Lsb = 6};
					enum {FieldMask = 0x00000040};
					enum {Value_Illuminate = 0x1};
					enum {ValueMask_Illuminate = 0x00000040};
					enum {Value_Extinguish = 0x0};
					enum {ValueMask_Extinguish = 0x00000000};
					};
				namespace LED7 { // Field Description
					enum {Lsb = 7};
					enum {FieldMask = 0x00000080};
					enum {Value_Illuminate = 0x1};
					enum {ValueMask_Illuminate = 0x00000080};
					enum {Value_Extinguish = 0x0};
					enum {ValueMask_Extinguish = 0x00000000};
					};
				};
			namespace DramPciPresent { // Register description
				typedef uint8_t	Reg;
				namespace PD_EDO { // Field Description
					enum {Lsb = 7};
					enum {FieldMask = 0x00000080};
					enum {Value_Present = 0x0};
					enum {ValueMask_Present = 0x00000000};
					enum {Value_Absent = 0x1};
					enum {ValueMask_Absent = 0x00000080};
					};
				namespace DRM_PD1 { // Field Description
					enum {Lsb = 6};
					enum {FieldMask = 0x00000040};
					enum {Value_Present = 0x0};
					enum {ValueMask_Present = 0x00000000};
					enum {Value_Absent = 0x1};
					enum {ValueMask_Absent = 0x00000040};
					};
				namespace DRM_PD2 { // Field Description
					enum {Lsb = 5};
					enum {FieldMask = 0x00000020};
					enum {Value_Present = 0x0};
					enum {ValueMask_Present = 0x00000000};
					enum {Value_Absent = 0x1};
					enum {ValueMask_Absent = 0x00000020};
					};
				namespace DRM_PD3 { // Field Description
					enum {Lsb = 4};
					enum {FieldMask = 0x00000010};
					enum {Value_Present = 0x0};
					enum {ValueMask_Present = 0x00000000};
					enum {Value_Absent = 0x1};
					enum {ValueMask_Absent = 0x00000010};
					};
				namespace DRM_PD4 { // Field Description
					enum {Lsb = 3};
					enum {FieldMask = 0x00000008};
					enum {Value_Present = 0x0};
					enum {ValueMask_Present = 0x00000000};
					enum {Value_Absent = 0x1};
					enum {ValueMask_Absent = 0x00000008};
					};
				namespace PRSNT { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x00000003};
					enum {Value_Empty = 0x3};
					enum {ValueMask_Empty = 0x00000003};
					enum {Value_Power25W = 0x2};
					enum {ValueMask_Power25W = 0x00000002};
					enum {Value_Power15W = 0x1};
					enum {ValueMask_Power15W = 0x00000001};
					enum {Value_Power7_5W = 0x0};
					enum {ValueMask_Power7_5W = 0x00000000};
					};
				};
			namespace FlashSimmPresent { // Register description
				typedef uint8_t	Reg;
				namespace PD1 { // Field Description
					enum {Lsb = 7};
					enum {FieldMask = 0x00000080};
					enum {Value_Present = 0x0};
					enum {ValueMask_Present = 0x00000000};
					enum {Value_Absent = 0x1};
					enum {ValueMask_Absent = 0x00000080};
					};
				namespace PD2 { // Field Description
					enum {Lsb = 6};
					enum {FieldMask = 0x00000040};
					enum {Value_Present = 0x0};
					enum {ValueMask_Present = 0x00000000};
					enum {Value_Absent = 0x1};
					enum {ValueMask_Absent = 0x00000040};
					};
				namespace PD3 { // Field Description
					enum {Lsb = 5};
					enum {FieldMask = 0x00000020};
					enum {Value_Present = 0x0};
					enum {ValueMask_Present = 0x00000000};
					enum {Value_Absent = 0x1};
					enum {ValueMask_Absent = 0x00000020};
					};
				namespace PD4 { // Field Description
					enum {Lsb = 4};
					enum {FieldMask = 0x00000010};
					enum {Value_Present = 0x0};
					enum {ValueMask_Present = 0x00000000};
					enum {Value_Absent = 0x1};
					enum {ValueMask_Absent = 0x00000010};
					};
				namespace PD5 { // Field Description
					enum {Lsb = 3};
					enum {FieldMask = 0x00000008};
					enum {Value_Present = 0x0};
					enum {ValueMask_Present = 0x00000000};
					enum {Value_Absent = 0x1};
					enum {ValueMask_Absent = 0x00000008};
					};
				namespace PD6 { // Field Description
					enum {Lsb = 2};
					enum {FieldMask = 0x00000004};
					enum {Value_Present = 0x0};
					enum {ValueMask_Present = 0x00000000};
					enum {Value_Absent = 0x1};
					enum {ValueMask_Absent = 0x00000004};
					};
				};
			namespace PcmciaPowerControl { // Register description
				typedef uint8_t	Reg;
				namespace SocketA { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x00000007};
					namespace Vcc { // Field Description
						enum {Lsb = 0};
						enum {FieldMask = 0x00000001};
						enum {Value_Enable = 0x1};
						enum {ValueMask_Enable = 0x00000001};
						enum {Value_Disable = 0x0};
						enum {ValueMask_Disable = 0x00000000};
						};
					namespace Vpp { // Field Description
						enum {Lsb = 1};
						enum {FieldMask = 0x00000006};
						enum {Value_ZeroVolts = 0x0};
						enum {ValueMask_ZeroVolts = 0x00000000};
						enum {Value_FiveVolts = 0x1};
						enum {ValueMask_FiveVolts = 0x00000002};
						enum {Value_TwelveVolts = 0x2};
						enum {ValueMask_TwelveVolts = 0x00000004};
						enum {Value_HighImpendence = 0x3};
						enum {ValueMask_HighImpendence = 0x00000006};
						};
					};
				namespace SocketB { // Field Description
					enum {Lsb = 3};
					enum {FieldMask = 0x00000038};
					namespace Vcc { // Field Description
						enum {Lsb = 3};
						enum {FieldMask = 0x00000008};
						enum {Value_Enable = 0x1};
						enum {ValueMask_Enable = 0x00000008};
						enum {Value_Disable = 0x0};
						enum {ValueMask_Disable = 0x00000000};
						};
					namespace Vpp { // Field Description
						enum {Lsb = 4};
						enum {FieldMask = 0x00000030};
						enum {Value_ZeroVolts = 0x0};
						enum {ValueMask_ZeroVolts = 0x00000000};
						enum {Value_FiveVolts = 0x1};
						enum {ValueMask_FiveVolts = 0x00000010};
						enum {Value_TwelveVolts = 0x2};
						enum {ValueMask_TwelveVolts = 0x00000020};
						enum {Value_HighImpendence = 0x3};
						enum {ValueMask_HighImpendence = 0x00000030};
						};
					};
				};
			};
		};
	};
#endif
