/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

/*
 *  This file contains directives for the GNU linker which are specific
 *  to the MDP8xxPro board.
 */
OUTPUT_FORMAT("elf32-powerpc", "elf32-powerpc",
              "elf32-powerpc")
OUTPUT_ARCH(powerpc)
 
ENTRY(__ppc_Reset)

SECTIONS
{
	/* 
	 * The stack will live in this area - between the vectors and
	 * the text section.
	 */
	
	.text 0x00000:
  	{
	     text.start = .;
	     *(.entry)
	     *(.entry2)
	     *(.text)
	     *(.rodata)
		 *(.data.rel.ro.local)
		 *(.rodata.str1.4)
             *(.gnu.linkonce.r*)
	     *(.rodata1)
             *(.gnu.linkonce.t.*)
	     *(.rodata.cst4)
	     *(.descriptors)
             /* .gnu.warning sections are handled specially by elf32.em.  */
             *(.gnu.warning)
	     *(rom_ver)
		 KEEP (*(.eh_frame))
	
	} > ram
 
  .ctors          :
  {
	etext = ALIGN(0x10);
	_etext = .;
	__CTOR_LIST__ = .;
    /* gcc uses crtbegin.o to find the start of
       the constructors, so we make sure it is
       first.  Because this is a wildcard, it
       doesn't matter if the user does not
       actually link against crtbegin.o; the
       linker won't look for a file to match a
       wildcard.  The wildcard also means that it
       doesn't matter which directory crtbegin.o
       is in.  */
    KEEP (*crtbegin.o(.ctors))
    /* We don't want to include the .ctor section from
       from the crtend.o file until after the sorted ctors.
       The .ctor section from the crtend file contains the
       end of ctors marker and it must be last */
    KEEP (*(EXCLUDE_FILE (*crtend.o ) .ctors))
    KEEP (*(SORT(.ctors.*)))
    KEEP (*(.ctors))
	__CTOR_END__ = .;
  } > ram
  .dtors          :
  {
    KEEP (*crtbegin.o(.dtors))
    KEEP (*(EXCLUDE_FILE (*crtend.o ) .dtors))
    KEEP (*(SORT(.dtors.*)))
    KEEP (*(.dtors))
  } > ram
  .init	: {
	     *(.lit)
	     *(.shdata)
		__INIT_BEGIN = .;
	     KEEP(*(.init))
		__INIT_END = .;
		__FINI_BEGIN = .;
	     *(.fini)
		__FINI_END = .;
	} > ram
  .jcr            : { KEEP (*(.jcr)) } > ram

	_endtext = .;
	text.end = .;

	/* R/W Data */
	.data ALIGN(0x1000):
	{
		PROVIDE( _dataStart = . );
	  *(.data)
	  *(.data.rel)
	  *(.data.rel.local)
	  *(.data1)
          *(.gnu.linkonce.d.*)
      *(.exception_ranges)	/* to get rid of warning */
      *(.ex_shared) /* to get rid of warning */
	  PROVIDE (__SDATA_START__ = .);
	*(.sdata .sdata.* .gnu.linkonce.s.*)

	} > ram
	
	PROVIDE (__EXCEPT_START__ = .);
	.gcc_except_table   : { *(.gcc_except_table) } > ram
	PROVIDE (__EXCEPT_END__ = .);
	__GOT_START__ = .;
	.got :
	{
	   s.got = .;
	   *(.got.plt) *(.got)
	} > ram
	__GOT_END__ = .;
	
	.got1		  : { *(.got1) 		} >ram
	PROVIDE (__GOT2_START__ = .);
	PROVIDE (_GOT2_START_ = .);
	.got2		  :  { *(.got2) 	} >ram
	PROVIDE (__GOT2_END__ = .);
	PROVIDE (_GOT2_END_ = .);
	
	PROVIDE (__FIXUP_START__ = .);
	PROVIDE (_FIXUP_START_ = .);
	.fixup	  : { *(.fixup) 	} >ram
	PROVIDE (_FIXUP_END_ = .);
	PROVIDE (__FIXUP_END__ = .);
	 
	PROVIDE (__SDATA2_START__ = .);
	.sdata2   	  : { *(.sdata2) 	} >ram
	.sbss2   	  : { *(.sbss2) 	} >ram
	PROVIDE (__SBSS2_END__ = .);
	
	.sbss2   	  : { *(.sbss2) 	} >ram
	PROVIDE (__SBSS2_END__ = .);
	
	__SBSS_START__ = .;
	.bss ALIGN(0x1000):
	{
	  bss.start = .;
	  *(.bss) *(.sbss) *(COMMON)
	  . = ALIGN(4);
	  bss.end = .;
	} > ram
	__SBSS_END__ = .;

	.sysglobal ALIGN(0x1000) (NOLOAD) :{
		OoosGlobalSystemPage = .;
	*(.sysglobal)
		. += 1;	/* Ensure at least 1 byte in page. */
	} > ram

	.sysstack ALIGN(0x1000) (NOLOAD) :{
		__SMP_SUPER_STACK_PAGE__ = .;
	*(.sysstack)
		. += 1;	/* Ensure at least 1 byte in page. */
	} > ram
	__SMP_SUPER_STACK_END__ = ALIGN(0x1000);
	__FREE_PAGE_START__ = __SMP_SUPER_STACK_END__;

	bss.size = bss.end - bss.start;
	text.size = text.end - text.start;
	PROVIDE(_end = bss.end);
	
/*
        dpram : 
	{
          m860 = .;
          _m860 = .;
          . += (8 * 1024);
        } >dpram
*/

	.line 0 : { *(.line) }
	.debug 0 : { *(.debug) }
	.debug_sfnames 0 : { *(.debug_sfnames) }
	.debug_srcinfo 0 : { *(.debug_srcinfo) }
	.debug_pubnames 0 : { *(.debug_pubnames) }
	.debug_aranges 0 : { *(.debug_aranges) }
	.debug_aregion 0 : { *(.debug_aregion) }
	.debug_macinfo 0 : { *(.debug_macinfo) }
	.stab 0 : { *(.stab) }
	.stabstr 0 : { *(.stabstr) }

}
