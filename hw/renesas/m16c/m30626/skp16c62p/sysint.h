#ifndef _oscl_hw_ti_tnetv105x_sysinth_
#define _oscl_hw_ti_tnetv105x_sysinth_
#include "sysintreg.h"

namespace Oscl {
namespace HW {
namespace TI {
namespace TNETV105X {
namespace SYS_INT {

/** The SYS_INT offset map. */
struct Map {
	volatile STAT_MSK_1::Reg		stat_msk_1;			// 0x2400
	volatile STAT_MSK_2::Reg		stat_msk_2;			// 0x2404
	uint8_t					reserved1[0x10-0x08];
	volatile STAT_RAW_1::Reg		stat_raw_1;			// 0x2410
	volatile STAT_RAW_2::Reg		stat_raw_2;			// 0x2414
	uint8_t					reserved2[0x20-0x18];
	volatile ENBL_SET_1::Reg		enbl_set_1;			// 0x2420
	volatile ENBL_SET_2::Reg		enbl_set_2;			// 0x2424
	uint8_t					reserved3[0x30-0x28];
	volatile ENBL_CLR_1::Reg		enbl_clr_1;			// 0x2430
	volatile ENBL_CLR_2::Reg		enbl_clr_2;			// 0x2434
	uint8_t					reserved4[0x40-0x38];
	volatile PRIORITY_INDEX::Reg	priority_index;		// 0x2440
	PRIORITY_INDEX_MSK::Reg			priority_index_msk;	// 0x2444
	uint8_t					reserved5[0x50-0x48];
	POLARITY_1::Reg					polarity_1;			// 0x2450
	POLARITY_2::Reg					polarity_2;			// 0x2454
	uint8_t					reserved6[0x60-0x58];
	TYPE_1::Reg						type_1;				// 0x2460
	TYPE_2::Reg						type_2;				// 0x2464
	uint8_t					reserved7[0x80-0x68];
	volatile EXCPT_STAT_MSK::Reg	excpt_stat_msk;		// 0x2480
	uint8_t					reserved8[0x88-0x84];
	volatile EXCPT_STAT_RAW::Reg	excpt_stat_raw;		// 0x2488
	uint8_t					reserved9[0x90-0x8C];
	volatile EXCPT_ENBL_SET::Reg	excpt_enbl_set;		// 0x2490
	uint8_t					reserved10[0x98-0x94];
	volatile EXCPT_ENBL_CLR::Reg	excpt_enbl_clr;		// 0x2498
	uint8_t					reserved11[0xA0-0x9C];
	PACE_PRESCALE::Reg				pace_prescale;		// 0x24A0
	PACE_MAP::Reg					pace_map;			// 0x24A4
	PACE_ENBL::Reg					pace_enbl;			// 0x24A8
	uint8_t					reserved12[0x600-0x4AC];
	PRIORITY::Reg					priority[39];		// 0x2600
	};

}
}
}
}
}

#endif
