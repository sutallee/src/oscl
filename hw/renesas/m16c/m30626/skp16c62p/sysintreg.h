#ifndef _hw_ti_tnetv105x_sysintregh_
#define _hw_ti_tnetv105x_sysintregh_
#include <stdint.h>
namespace Oscl { // Namespace description
	namespace HW { // Namespace description
		namespace TI { // Namespace description
			namespace TNETV105X { // Namespace description
				namespace SYS_INT { // Namespace description
					namespace STAT_MSK_1 { // Register description
						typedef uint32_t	Reg;
						enum {Pending = 1};
						enum {Mask = 1};
						enum {Set = 1};
						namespace Secondary { // Field Description
							enum {Lsb = 0};
							enum {FieldMask = 0x00000001};
							enum {Value_Pending = 0x1};
							enum {ValueMask_Pending = 0x00000001};
							enum {Value_Set = 0x1};
							enum {ValueMask_Set = 0x00000001};
							};
						namespace EXT_INT1 { // Field Description
							enum {Lsb = 1};
							enum {FieldMask = 0x00000002};
							enum {Value_Pending = 0x1};
							enum {ValueMask_Pending = 0x00000002};
							enum {Value_Set = 0x1};
							enum {ValueMask_Set = 0x00000002};
							};
						namespace EXT_INT2 { // Field Description
							enum {Lsb = 2};
							enum {FieldMask = 0x00000004};
							enum {Value_Pending = 0x1};
							enum {ValueMask_Pending = 0x00000004};
							enum {Value_Set = 0x1};
							enum {ValueMask_Set = 0x00000004};
							};
						namespace EXT_INT3 { // Field Description
							enum {Lsb = 3};
							enum {FieldMask = 0x00000008};
							enum {Value_Pending = 0x1};
							enum {ValueMask_Pending = 0x00000008};
							enum {Value_Set = 0x1};
							enum {ValueMask_Set = 0x00000008};
							};
						namespace EXT_INT4 { // Field Description
							enum {Lsb = 4};
							enum {FieldMask = 0x00000010};
							enum {Value_Pending = 0x1};
							enum {ValueMask_Pending = 0x00000010};
							enum {Value_Set = 0x1};
							enum {ValueMask_Set = 0x00000010};
							};
						namespace TimerZero { // Field Description
							enum {Lsb = 5};
							enum {FieldMask = 0x00000020};
							enum {Value_Pending = 0x1};
							enum {ValueMask_Pending = 0x00000020};
							enum {Value_Set = 0x1};
							enum {ValueMask_Set = 0x00000020};
							};
						namespace TimerOne { // Field Description
							enum {Lsb = 6};
							enum {FieldMask = 0x00000040};
							enum {Value_Pending = 0x1};
							enum {ValueMask_Pending = 0x00000040};
							enum {Value_Set = 0x1};
							enum {ValueMask_Set = 0x00000040};
							};
						namespace Uart { // Field Description
							enum {Lsb = 7};
							enum {FieldMask = 0x00000080};
							enum {Value_Pending = 0x1};
							enum {ValueMask_Pending = 0x00000080};
							enum {Value_Set = 0x1};
							enum {ValueMask_Set = 0x00000080};
							};
						namespace DmaZero { // Field Description
							enum {Lsb = 9};
							enum {FieldMask = 0x00000200};
							enum {Value_Pending = 0x1};
							enum {ValueMask_Pending = 0x00000200};
							enum {Value_Set = 0x1};
							enum {ValueMask_Set = 0x00000200};
							};
						namespace DmaOne { // Field Description
							enum {Lsb = 10};
							enum {FieldMask = 0x00000400};
							enum {Value_Pending = 0x1};
							enum {ValueMask_Pending = 0x00000400};
							enum {Value_Set = 0x1};
							enum {ValueMask_Set = 0x00000400};
							};
						namespace EthernetMacZero { // Field Description
							enum {Lsb = 19};
							enum {FieldMask = 0x00080000};
							enum {Value_Pending = 0x1};
							enum {ValueMask_Pending = 0x00080000};
							enum {Value_Set = 0x1};
							enum {ValueMask_Set = 0x00080000};
							};
						namespace DSP_GPIO { // Field Description
							enum {Lsb = 21};
							enum {FieldMask = 0x00200000};
							enum {Value_Pending = 0x1};
							enum {ValueMask_Pending = 0x00200000};
							enum {Value_Set = 0x1};
							enum {ValueMask_Set = 0x00200000};
							};
						namespace DspReset { // Field Description
							enum {Lsb = 22};
							enum {FieldMask = 0x00400000};
							enum {Value_Pending = 0x1};
							enum {ValueMask_Pending = 0x00400000};
							enum {Value_Set = 0x1};
							enum {ValueMask_Set = 0x00400000};
							};
						namespace TELE_INT { // Field Description
							enum {Lsb = 23};
							enum {FieldMask = 0x00800000};
							enum {Value_Pending = 0x1};
							enum {ValueMask_Pending = 0x00800000};
							enum {Value_Set = 0x1};
							enum {ValueMask_Set = 0x00800000};
							};
						namespace UsbDevice { // Field Description
							enum {Lsb = 24};
							enum {FieldMask = 0x01000000};
							enum {Value_Pending = 0x1};
							enum {ValueMask_Pending = 0x01000000};
							enum {Value_Set = 0x1};
							enum {ValueMask_Set = 0x01000000};
							};
						namespace VLYNQ5 { // Field Description
							enum {Lsb = 25};
							enum {FieldMask = 0x02000000};
							enum {Value_Pending = 0x1};
							enum {ValueMask_Pending = 0x02000000};
							enum {Value_Set = 0x1};
							enum {ValueMask_Set = 0x02000000};
							};
						namespace VLYNQ3 { // Field Description
							enum {Lsb = 26};
							enum {FieldMask = 0x04000000};
							enum {Value_Pending = 0x1};
							enum {ValueMask_Pending = 0x04000000};
							enum {Value_Set = 0x1};
							enum {ValueMask_Set = 0x04000000};
							};
						namespace EthernetSwitchDMA { // Field Description
							enum {Lsb = 27};
							enum {FieldMask = 0x08000000};
							enum {Value_Pending = 0x1};
							enum {ValueMask_Pending = 0x08000000};
							enum {Value_Set = 0x1};
							enum {ValueMask_Set = 0x08000000};
							};
						namespace EthernetPHYs { // Field Description
							enum {Lsb = 28};
							enum {FieldMask = 0x10000000};
							enum {Value_Pending = 0x1};
							enum {ValueMask_Pending = 0x10000000};
							enum {Value_Set = 0x1};
							enum {ValueMask_Set = 0x10000000};
							};
						namespace SSP { // Field Description
							enum {Lsb = 29};
							enum {FieldMask = 0x20000000};
							enum {Value_Pending = 0x1};
							enum {ValueMask_Pending = 0x20000000};
							enum {Value_Set = 0x1};
							enum {ValueMask_Set = 0x20000000};
							};
						namespace DmaTwo { // Field Description
							enum {Lsb = 30};
							enum {FieldMask = 0x40000000};
							enum {Value_Pending = 0x1};
							enum {ValueMask_Pending = 0x40000000};
							enum {Value_Set = 0x1};
							enum {ValueMask_Set = 0x40000000};
							};
						namespace DmaThree { // Field Description
							enum {Lsb = 31};
							enum {FieldMask = 0x80000000};
							enum {Value_Pending = 0x1};
							enum {ValueMask_Pending = 0x80000000};
							enum {Value_Set = 0x1};
							enum {ValueMask_Set = 0x80000000};
							};
						};
					namespace STAT_MSK_2 { // Register description
						typedef uint32_t	Reg;
						enum {Pending = 1};
						enum {Mask = 1};
						enum {Set = 1};
						namespace Telephony { // Field Description
							enum {Lsb = 0};
							enum {FieldMask = 0x00000001};
							enum {Value_Pending = 0x1};
							enum {ValueMask_Pending = 0x00000001};
							enum {Value_Set = 0x1};
							enum {ValueMask_Set = 0x00000001};
							};
						namespace EthernetMacOne { // Field Description
							enum {Lsb = 1};
							enum {FieldMask = 0x00000002};
							enum {Value_Pending = 0x1};
							enum {ValueMask_Pending = 0x00000002};
							enum {Value_Set = 0x1};
							enum {ValueMask_Set = 0x00000002};
							};
						namespace UsbHost { // Field Description
							enum {Lsb = 3};
							enum {FieldMask = 0x00000008};
							enum {Value_Pending = 0x1};
							enum {ValueMask_Pending = 0x00000008};
							enum {Value_Set = 0x1};
							enum {ValueMask_Set = 0x00000008};
							};
						namespace LCD { // Field Description
							enum {Lsb = 4};
							enum {FieldMask = 0x00000010};
							enum {Value_Pending = 0x1};
							enum {ValueMask_Pending = 0x00000010};
							enum {Value_Set = 0x1};
							enum {ValueMask_Set = 0x00000010};
							};
						namespace Keypad { // Field Description
							enum {Lsb = 5};
							enum {FieldMask = 0x00000020};
							enum {Value_Pending = 0x1};
							enum {ValueMask_Pending = 0x00000020};
							enum {Value_Set = 0x1};
							enum {ValueMask_Set = 0x00000020};
							};
						};
					namespace STAT_RAW_1 { // Register description
						typedef uint32_t	Reg;
						enum {Active = 1};
						enum {Mask = 1};
						enum {Clear = 1};
						namespace Secondary { // Field Description
							enum {Lsb = 0};
							enum {FieldMask = 0x00000001};
							enum {Value_Active = 0x1};
							enum {ValueMask_Active = 0x00000001};
							enum {Value_Clear = 0x1};
							enum {ValueMask_Clear = 0x00000001};
							};
						namespace EXT_INT1 { // Field Description
							enum {Lsb = 1};
							enum {FieldMask = 0x00000002};
							enum {Value_Active = 0x1};
							enum {ValueMask_Active = 0x00000002};
							enum {Value_Clear = 0x1};
							enum {ValueMask_Clear = 0x00000002};
							};
						namespace EXT_INT2 { // Field Description
							enum {Lsb = 2};
							enum {FieldMask = 0x00000004};
							enum {Value_Active = 0x1};
							enum {ValueMask_Active = 0x00000004};
							enum {Value_Clear = 0x1};
							enum {ValueMask_Clear = 0x00000004};
							};
						namespace EXT_INT3 { // Field Description
							enum {Lsb = 3};
							enum {FieldMask = 0x00000008};
							enum {Value_Active = 0x1};
							enum {ValueMask_Active = 0x00000008};
							enum {Value_Clear = 0x1};
							enum {ValueMask_Clear = 0x00000008};
							};
						namespace EXT_INT4 { // Field Description
							enum {Lsb = 4};
							enum {FieldMask = 0x00000010};
							enum {Value_Active = 0x1};
							enum {ValueMask_Active = 0x00000010};
							enum {Value_Clear = 0x1};
							enum {ValueMask_Clear = 0x00000010};
							};
						namespace TimerZero { // Field Description
							enum {Lsb = 5};
							enum {FieldMask = 0x00000020};
							enum {Value_Active = 0x1};
							enum {ValueMask_Active = 0x00000020};
							enum {Value_Clear = 0x1};
							enum {ValueMask_Clear = 0x00000020};
							};
						namespace TimerOne { // Field Description
							enum {Lsb = 6};
							enum {FieldMask = 0x00000040};
							enum {Value_Active = 0x1};
							enum {ValueMask_Active = 0x00000040};
							enum {Value_Clear = 0x1};
							enum {ValueMask_Clear = 0x00000040};
							};
						namespace Uart { // Field Description
							enum {Lsb = 7};
							enum {FieldMask = 0x00000080};
							enum {Value_Active = 0x1};
							enum {ValueMask_Active = 0x00000080};
							enum {Value_Clear = 0x1};
							enum {ValueMask_Clear = 0x00000080};
							};
						namespace DmaZero { // Field Description
							enum {Lsb = 9};
							enum {FieldMask = 0x00000200};
							enum {Value_Active = 0x1};
							enum {ValueMask_Active = 0x00000200};
							enum {Value_Clear = 0x1};
							enum {ValueMask_Clear = 0x00000200};
							};
						namespace DmaOne { // Field Description
							enum {Lsb = 10};
							enum {FieldMask = 0x00000400};
							enum {Value_Active = 0x1};
							enum {ValueMask_Active = 0x00000400};
							enum {Value_Clear = 0x1};
							enum {ValueMask_Clear = 0x00000400};
							};
						namespace EthernetMacZero { // Field Description
							enum {Lsb = 19};
							enum {FieldMask = 0x00080000};
							enum {Value_Active = 0x1};
							enum {ValueMask_Active = 0x00080000};
							enum {Value_Clear = 0x1};
							enum {ValueMask_Clear = 0x00080000};
							};
						namespace DSP_GPIO { // Field Description
							enum {Lsb = 21};
							enum {FieldMask = 0x00200000};
							enum {Value_Active = 0x1};
							enum {ValueMask_Active = 0x00200000};
							enum {Value_Clear = 0x1};
							enum {ValueMask_Clear = 0x00200000};
							};
						namespace DspReset { // Field Description
							enum {Lsb = 22};
							enum {FieldMask = 0x00400000};
							enum {Value_Active = 0x1};
							enum {ValueMask_Active = 0x00400000};
							enum {Value_Clear = 0x1};
							enum {ValueMask_Clear = 0x00400000};
							};
						namespace TELE_INT { // Field Description
							enum {Lsb = 23};
							enum {FieldMask = 0x00800000};
							enum {Value_Active = 0x1};
							enum {ValueMask_Active = 0x00800000};
							enum {Value_Clear = 0x1};
							enum {ValueMask_Clear = 0x00800000};
							};
						namespace UsbDevice { // Field Description
							enum {Lsb = 24};
							enum {FieldMask = 0x01000000};
							enum {Value_Active = 0x1};
							enum {ValueMask_Active = 0x01000000};
							enum {Value_Clear = 0x1};
							enum {ValueMask_Clear = 0x01000000};
							};
						namespace VLYNQ5 { // Field Description
							enum {Lsb = 25};
							enum {FieldMask = 0x02000000};
							enum {Value_Active = 0x1};
							enum {ValueMask_Active = 0x02000000};
							enum {Value_Clear = 0x1};
							enum {ValueMask_Clear = 0x02000000};
							};
						namespace VLYNQ3 { // Field Description
							enum {Lsb = 26};
							enum {FieldMask = 0x04000000};
							enum {Value_Active = 0x1};
							enum {ValueMask_Active = 0x04000000};
							enum {Value_Clear = 0x1};
							enum {ValueMask_Clear = 0x04000000};
							};
						namespace EthernetSwitchDMA { // Field Description
							enum {Lsb = 27};
							enum {FieldMask = 0x08000000};
							enum {Value_Active = 0x1};
							enum {ValueMask_Active = 0x08000000};
							enum {Value_Clear = 0x1};
							enum {ValueMask_Clear = 0x08000000};
							};
						namespace EthernetPHYs { // Field Description
							enum {Lsb = 28};
							enum {FieldMask = 0x10000000};
							enum {Value_Active = 0x1};
							enum {ValueMask_Active = 0x10000000};
							enum {Value_Clear = 0x1};
							enum {ValueMask_Clear = 0x10000000};
							};
						namespace SSP { // Field Description
							enum {Lsb = 29};
							enum {FieldMask = 0x20000000};
							enum {Value_Active = 0x1};
							enum {ValueMask_Active = 0x20000000};
							enum {Value_Clear = 0x1};
							enum {ValueMask_Clear = 0x20000000};
							};
						namespace DmaTwo { // Field Description
							enum {Lsb = 30};
							enum {FieldMask = 0x40000000};
							enum {Value_Active = 0x1};
							enum {ValueMask_Active = 0x40000000};
							enum {Value_Clear = 0x1};
							enum {ValueMask_Clear = 0x40000000};
							};
						namespace DmaThree { // Field Description
							enum {Lsb = 31};
							enum {FieldMask = 0x80000000};
							enum {Value_Active = 0x1};
							enum {ValueMask_Active = 0x80000000};
							enum {Value_Clear = 0x1};
							enum {ValueMask_Clear = 0x80000000};
							};
						};
					namespace STAT_RAW_2 { // Register description
						typedef uint32_t	Reg;
						enum {Active = 1};
						enum {Mask = 1};
						enum {Clear = 1};
						namespace Telephony { // Field Description
							enum {Lsb = 0};
							enum {FieldMask = 0x00000001};
							enum {Value_Active = 0x1};
							enum {ValueMask_Active = 0x00000001};
							enum {Value_Clear = 0x1};
							enum {ValueMask_Clear = 0x00000001};
							};
						namespace EthernetMacOne { // Field Description
							enum {Lsb = 1};
							enum {FieldMask = 0x00000002};
							enum {Value_Active = 0x1};
							enum {ValueMask_Active = 0x00000002};
							enum {Value_Clear = 0x1};
							enum {ValueMask_Clear = 0x00000002};
							};
						namespace UsbHost { // Field Description
							enum {Lsb = 3};
							enum {FieldMask = 0x00000008};
							enum {Value_Active = 0x1};
							enum {ValueMask_Active = 0x00000008};
							enum {Value_Clear = 0x1};
							enum {ValueMask_Clear = 0x00000008};
							};
						namespace LCD { // Field Description
							enum {Lsb = 4};
							enum {FieldMask = 0x00000010};
							enum {Value_Active = 0x1};
							enum {ValueMask_Active = 0x00000010};
							enum {Value_Clear = 0x1};
							enum {ValueMask_Clear = 0x00000010};
							};
						namespace Keypad { // Field Description
							enum {Lsb = 5};
							enum {FieldMask = 0x00000020};
							enum {Value_Active = 0x1};
							enum {ValueMask_Active = 0x00000020};
							enum {Value_Clear = 0x1};
							enum {ValueMask_Clear = 0x00000020};
							};
						};
					namespace ENBL_SET_1 { // Register description
						typedef uint32_t	Reg;
						enum {Enabled = 1};
						enum {Disabled = 0};
						enum {Mask = 1};
						enum {Enable = 1};
						namespace Secondary { // Field Description
							enum {Lsb = 0};
							enum {FieldMask = 0x00000001};
							enum {Value_Enabled = 0x1};
							enum {ValueMask_Enabled = 0x00000001};
							enum {Value_Disabled = 0x0};
							enum {ValueMask_Disabled = 0x00000000};
							enum {Value_Enable = 0x1};
							enum {ValueMask_Enable = 0x00000001};
							};
						namespace EXT_INT1 { // Field Description
							enum {Lsb = 1};
							enum {FieldMask = 0x00000002};
							enum {Value_Enabled = 0x1};
							enum {ValueMask_Enabled = 0x00000002};
							enum {Value_Disabled = 0x0};
							enum {ValueMask_Disabled = 0x00000000};
							enum {Value_Enable = 0x1};
							enum {ValueMask_Enable = 0x00000002};
							};
						namespace EXT_INT2 { // Field Description
							enum {Lsb = 2};
							enum {FieldMask = 0x00000004};
							enum {Value_Enabled = 0x1};
							enum {ValueMask_Enabled = 0x00000004};
							enum {Value_Disabled = 0x0};
							enum {ValueMask_Disabled = 0x00000000};
							enum {Value_Enable = 0x1};
							enum {ValueMask_Enable = 0x00000004};
							};
						namespace EXT_INT3 { // Field Description
							enum {Lsb = 3};
							enum {FieldMask = 0x00000008};
							enum {Value_Enabled = 0x1};
							enum {ValueMask_Enabled = 0x00000008};
							enum {Value_Disabled = 0x0};
							enum {ValueMask_Disabled = 0x00000000};
							enum {Value_Enable = 0x1};
							enum {ValueMask_Enable = 0x00000008};
							};
						namespace EXT_INT4 { // Field Description
							enum {Lsb = 4};
							enum {FieldMask = 0x00000010};
							enum {Value_Enabled = 0x1};
							enum {ValueMask_Enabled = 0x00000010};
							enum {Value_Disabled = 0x0};
							enum {ValueMask_Disabled = 0x00000000};
							enum {Value_Enable = 0x1};
							enum {ValueMask_Enable = 0x00000010};
							};
						namespace TimerZero { // Field Description
							enum {Lsb = 5};
							enum {FieldMask = 0x00000020};
							enum {Value_Enabled = 0x1};
							enum {ValueMask_Enabled = 0x00000020};
							enum {Value_Disabled = 0x0};
							enum {ValueMask_Disabled = 0x00000000};
							enum {Value_Enable = 0x1};
							enum {ValueMask_Enable = 0x00000020};
							};
						namespace TimerOne { // Field Description
							enum {Lsb = 6};
							enum {FieldMask = 0x00000040};
							enum {Value_Enabled = 0x1};
							enum {ValueMask_Enabled = 0x00000040};
							enum {Value_Disabled = 0x0};
							enum {ValueMask_Disabled = 0x00000000};
							enum {Value_Enable = 0x1};
							enum {ValueMask_Enable = 0x00000040};
							};
						namespace Uart { // Field Description
							enum {Lsb = 7};
							enum {FieldMask = 0x00000080};
							enum {Value_Enabled = 0x1};
							enum {ValueMask_Enabled = 0x00000080};
							enum {Value_Disabled = 0x0};
							enum {ValueMask_Disabled = 0x00000000};
							enum {Value_Enable = 0x1};
							enum {ValueMask_Enable = 0x00000080};
							};
						namespace DmaZero { // Field Description
							enum {Lsb = 9};
							enum {FieldMask = 0x00000200};
							enum {Value_Enabled = 0x1};
							enum {ValueMask_Enabled = 0x00000200};
							enum {Value_Disabled = 0x0};
							enum {ValueMask_Disabled = 0x00000000};
							enum {Value_Enable = 0x1};
							enum {ValueMask_Enable = 0x00000200};
							};
						namespace DmaOne { // Field Description
							enum {Lsb = 10};
							enum {FieldMask = 0x00000400};
							enum {Value_Enabled = 0x1};
							enum {ValueMask_Enabled = 0x00000400};
							enum {Value_Disabled = 0x0};
							enum {ValueMask_Disabled = 0x00000000};
							enum {Value_Enable = 0x1};
							enum {ValueMask_Enable = 0x00000400};
							};
						namespace EthernetMacZero { // Field Description
							enum {Lsb = 19};
							enum {FieldMask = 0x00080000};
							enum {Value_Enabled = 0x1};
							enum {ValueMask_Enabled = 0x00080000};
							enum {Value_Disabled = 0x0};
							enum {ValueMask_Disabled = 0x00000000};
							enum {Value_Enable = 0x1};
							enum {ValueMask_Enable = 0x00080000};
							};
						namespace DSP_GPIO { // Field Description
							enum {Lsb = 21};
							enum {FieldMask = 0x00200000};
							enum {Value_Enabled = 0x1};
							enum {ValueMask_Enabled = 0x00200000};
							enum {Value_Disabled = 0x0};
							enum {ValueMask_Disabled = 0x00000000};
							enum {Value_Enable = 0x1};
							enum {ValueMask_Enable = 0x00200000};
							};
						namespace DspReset { // Field Description
							enum {Lsb = 22};
							enum {FieldMask = 0x00400000};
							enum {Value_Enabled = 0x1};
							enum {ValueMask_Enabled = 0x00400000};
							enum {Value_Disabled = 0x0};
							enum {ValueMask_Disabled = 0x00000000};
							enum {Value_Enable = 0x1};
							enum {ValueMask_Enable = 0x00400000};
							};
						namespace TELE_INT { // Field Description
							enum {Lsb = 23};
							enum {FieldMask = 0x00800000};
							enum {Value_Enabled = 0x1};
							enum {ValueMask_Enabled = 0x00800000};
							enum {Value_Disabled = 0x0};
							enum {ValueMask_Disabled = 0x00000000};
							enum {Value_Enable = 0x1};
							enum {ValueMask_Enable = 0x00800000};
							};
						namespace UsbDevice { // Field Description
							enum {Lsb = 24};
							enum {FieldMask = 0x01000000};
							enum {Value_Enabled = 0x1};
							enum {ValueMask_Enabled = 0x01000000};
							enum {Value_Disabled = 0x0};
							enum {ValueMask_Disabled = 0x00000000};
							enum {Value_Enable = 0x1};
							enum {ValueMask_Enable = 0x01000000};
							};
						namespace VLYNQ5 { // Field Description
							enum {Lsb = 25};
							enum {FieldMask = 0x02000000};
							enum {Value_Enabled = 0x1};
							enum {ValueMask_Enabled = 0x02000000};
							enum {Value_Disabled = 0x0};
							enum {ValueMask_Disabled = 0x00000000};
							enum {Value_Enable = 0x1};
							enum {ValueMask_Enable = 0x02000000};
							};
						namespace VLYNQ3 { // Field Description
							enum {Lsb = 26};
							enum {FieldMask = 0x04000000};
							enum {Value_Enabled = 0x1};
							enum {ValueMask_Enabled = 0x04000000};
							enum {Value_Disabled = 0x0};
							enum {ValueMask_Disabled = 0x00000000};
							enum {Value_Enable = 0x1};
							enum {ValueMask_Enable = 0x04000000};
							};
						namespace EthernetSwitchDMA { // Field Description
							enum {Lsb = 27};
							enum {FieldMask = 0x08000000};
							enum {Value_Enabled = 0x1};
							enum {ValueMask_Enabled = 0x08000000};
							enum {Value_Disabled = 0x0};
							enum {ValueMask_Disabled = 0x00000000};
							enum {Value_Enable = 0x1};
							enum {ValueMask_Enable = 0x08000000};
							};
						namespace EthernetPHYs { // Field Description
							enum {Lsb = 28};
							enum {FieldMask = 0x10000000};
							enum {Value_Enabled = 0x1};
							enum {ValueMask_Enabled = 0x10000000};
							enum {Value_Disabled = 0x0};
							enum {ValueMask_Disabled = 0x00000000};
							enum {Value_Enable = 0x1};
							enum {ValueMask_Enable = 0x10000000};
							};
						namespace SSP { // Field Description
							enum {Lsb = 29};
							enum {FieldMask = 0x20000000};
							enum {Value_Enabled = 0x1};
							enum {ValueMask_Enabled = 0x20000000};
							enum {Value_Disabled = 0x0};
							enum {ValueMask_Disabled = 0x00000000};
							enum {Value_Enable = 0x1};
							enum {ValueMask_Enable = 0x20000000};
							};
						namespace DmaTwo { // Field Description
							enum {Lsb = 30};
							enum {FieldMask = 0x40000000};
							enum {Value_Enabled = 0x1};
							enum {ValueMask_Enabled = 0x40000000};
							enum {Value_Disabled = 0x0};
							enum {ValueMask_Disabled = 0x00000000};
							enum {Value_Enable = 0x1};
							enum {ValueMask_Enable = 0x40000000};
							};
						namespace DmaThree { // Field Description
							enum {Lsb = 31};
							enum {FieldMask = 0x80000000};
							enum {Value_Enabled = 0x1};
							enum {ValueMask_Enabled = 0x80000000};
							enum {Value_Disabled = 0x0};
							enum {ValueMask_Disabled = 0x00000000};
							enum {Value_Enable = 0x1};
							enum {ValueMask_Enable = 0x80000000};
							};
						};
					namespace ENBL_SET_2 { // Register description
						typedef uint32_t	Reg;
						enum {Enabled = 1};
						enum {Disabled = 0};
						enum {Mask = 1};
						enum {Enable = 1};
						namespace Telephony { // Field Description
							enum {Lsb = 0};
							enum {FieldMask = 0x00000001};
							enum {Value_Enabled = 0x1};
							enum {ValueMask_Enabled = 0x00000001};
							enum {Value_Disabled = 0x0};
							enum {ValueMask_Disabled = 0x00000000};
							enum {Value_Enable = 0x1};
							enum {ValueMask_Enable = 0x00000001};
							};
						namespace EthernetMacOne { // Field Description
							enum {Lsb = 1};
							enum {FieldMask = 0x00000002};
							enum {Value_Enabled = 0x1};
							enum {ValueMask_Enabled = 0x00000002};
							enum {Value_Disabled = 0x0};
							enum {ValueMask_Disabled = 0x00000000};
							enum {Value_Enable = 0x1};
							enum {ValueMask_Enable = 0x00000002};
							};
						namespace UsbHost { // Field Description
							enum {Lsb = 3};
							enum {FieldMask = 0x00000008};
							enum {Value_Enabled = 0x1};
							enum {ValueMask_Enabled = 0x00000008};
							enum {Value_Disabled = 0x0};
							enum {ValueMask_Disabled = 0x00000000};
							enum {Value_Enable = 0x1};
							enum {ValueMask_Enable = 0x00000008};
							};
						namespace LCD { // Field Description
							enum {Lsb = 4};
							enum {FieldMask = 0x00000010};
							enum {Value_Enabled = 0x1};
							enum {ValueMask_Enabled = 0x00000010};
							enum {Value_Disabled = 0x0};
							enum {ValueMask_Disabled = 0x00000000};
							enum {Value_Enable = 0x1};
							enum {ValueMask_Enable = 0x00000010};
							};
						namespace Keypad { // Field Description
							enum {Lsb = 5};
							enum {FieldMask = 0x00000020};
							enum {Value_Enabled = 0x1};
							enum {ValueMask_Enabled = 0x00000020};
							enum {Value_Disabled = 0x0};
							enum {ValueMask_Disabled = 0x00000000};
							enum {Value_Enable = 0x1};
							enum {ValueMask_Enable = 0x00000020};
							};
						};
					namespace ENBL_CLR_1 { // Register description
						typedef uint32_t	Reg;
						enum {Mask = 1};
						enum {Disable = 1};
						namespace Secondary { // Field Description
							enum {Lsb = 0};
							enum {FieldMask = 0x00000001};
							enum {Value_Disable = 0x1};
							enum {ValueMask_Disable = 0x00000001};
							};
						namespace EXT_INT1 { // Field Description
							enum {Lsb = 1};
							enum {FieldMask = 0x00000002};
							enum {Value_Disable = 0x1};
							enum {ValueMask_Disable = 0x00000002};
							};
						namespace EXT_INT2 { // Field Description
							enum {Lsb = 2};
							enum {FieldMask = 0x00000004};
							enum {Value_Disable = 0x1};
							enum {ValueMask_Disable = 0x00000004};
							};
						namespace EXT_INT3 { // Field Description
							enum {Lsb = 3};
							enum {FieldMask = 0x00000008};
							enum {Value_Disable = 0x1};
							enum {ValueMask_Disable = 0x00000008};
							};
						namespace EXT_INT4 { // Field Description
							enum {Lsb = 4};
							enum {FieldMask = 0x00000010};
							enum {Value_Disable = 0x1};
							enum {ValueMask_Disable = 0x00000010};
							};
						namespace TimerZero { // Field Description
							enum {Lsb = 5};
							enum {FieldMask = 0x00000020};
							enum {Value_Disable = 0x1};
							enum {ValueMask_Disable = 0x00000020};
							};
						namespace TimerOne { // Field Description
							enum {Lsb = 6};
							enum {FieldMask = 0x00000040};
							enum {Value_Disable = 0x1};
							enum {ValueMask_Disable = 0x00000040};
							};
						namespace Uart { // Field Description
							enum {Lsb = 7};
							enum {FieldMask = 0x00000080};
							enum {Value_Disable = 0x1};
							enum {ValueMask_Disable = 0x00000080};
							};
						namespace DmaZero { // Field Description
							enum {Lsb = 9};
							enum {FieldMask = 0x00000200};
							enum {Value_Disable = 0x1};
							enum {ValueMask_Disable = 0x00000200};
							};
						namespace DmaOne { // Field Description
							enum {Lsb = 10};
							enum {FieldMask = 0x00000400};
							enum {Value_Disable = 0x1};
							enum {ValueMask_Disable = 0x00000400};
							};
						namespace EthernetMacZero { // Field Description
							enum {Lsb = 19};
							enum {FieldMask = 0x00080000};
							enum {Value_Disable = 0x1};
							enum {ValueMask_Disable = 0x00080000};
							};
						namespace DSP_GPIO { // Field Description
							enum {Lsb = 21};
							enum {FieldMask = 0x00200000};
							enum {Value_Disable = 0x1};
							enum {ValueMask_Disable = 0x00200000};
							};
						namespace DspReset { // Field Description
							enum {Lsb = 22};
							enum {FieldMask = 0x00400000};
							enum {Value_Disable = 0x1};
							enum {ValueMask_Disable = 0x00400000};
							};
						namespace TELE_INT { // Field Description
							enum {Lsb = 23};
							enum {FieldMask = 0x00800000};
							enum {Value_Disable = 0x1};
							enum {ValueMask_Disable = 0x00800000};
							};
						namespace UsbDevice { // Field Description
							enum {Lsb = 24};
							enum {FieldMask = 0x01000000};
							enum {Value_Disable = 0x1};
							enum {ValueMask_Disable = 0x01000000};
							};
						namespace VLYNQ5 { // Field Description
							enum {Lsb = 25};
							enum {FieldMask = 0x02000000};
							enum {Value_Disable = 0x1};
							enum {ValueMask_Disable = 0x02000000};
							};
						namespace VLYNQ3 { // Field Description
							enum {Lsb = 26};
							enum {FieldMask = 0x04000000};
							enum {Value_Disable = 0x1};
							enum {ValueMask_Disable = 0x04000000};
							};
						namespace EthernetSwitchDMA { // Field Description
							enum {Lsb = 27};
							enum {FieldMask = 0x08000000};
							enum {Value_Disable = 0x1};
							enum {ValueMask_Disable = 0x08000000};
							};
						namespace EthernetPHYs { // Field Description
							enum {Lsb = 28};
							enum {FieldMask = 0x10000000};
							enum {Value_Disable = 0x1};
							enum {ValueMask_Disable = 0x10000000};
							};
						namespace SSP { // Field Description
							enum {Lsb = 29};
							enum {FieldMask = 0x20000000};
							enum {Value_Disable = 0x1};
							enum {ValueMask_Disable = 0x20000000};
							};
						namespace DmaTwo { // Field Description
							enum {Lsb = 30};
							enum {FieldMask = 0x40000000};
							enum {Value_Disable = 0x1};
							enum {ValueMask_Disable = 0x40000000};
							};
						namespace DmaThree { // Field Description
							enum {Lsb = 31};
							enum {FieldMask = 0x80000000};
							enum {Value_Disable = 0x1};
							enum {ValueMask_Disable = 0x80000000};
							};
						};
					namespace ENBL_CLR_2 { // Register description
						typedef uint32_t	Reg;
						enum {Mask = 1};
						enum {Disable = 1};
						namespace Telephony { // Field Description
							enum {Lsb = 0};
							enum {FieldMask = 0x00000001};
							enum {Value_Disable = 0x1};
							enum {ValueMask_Disable = 0x00000001};
							};
						namespace EthernetMacOne { // Field Description
							enum {Lsb = 1};
							enum {FieldMask = 0x00000002};
							enum {Value_Disable = 0x1};
							enum {ValueMask_Disable = 0x00000002};
							};
						namespace UsbHost { // Field Description
							enum {Lsb = 3};
							enum {FieldMask = 0x00000008};
							enum {Value_Disable = 0x1};
							enum {ValueMask_Disable = 0x00000008};
							};
						namespace LCD { // Field Description
							enum {Lsb = 4};
							enum {FieldMask = 0x00000010};
							enum {Value_Disable = 0x1};
							enum {ValueMask_Disable = 0x00000010};
							};
						namespace Keypad { // Field Description
							enum {Lsb = 5};
							enum {FieldMask = 0x00000020};
							enum {Value_Disable = 0x1};
							enum {ValueMask_Disable = 0x00000020};
							};
						};
					namespace PRIORITY_INDEX { // Register description
						typedef uint32_t	Reg;
						namespace SOURCE_VALUE { // Field Description
							enum {Lsb = 16};
							enum {FieldMask = 0x003F0000};
							enum {Value_Secondary = 0x0};
							enum {ValueMask_Secondary = 0x00000000};
							enum {Value_EXT_INT1 = 0x1};
							enum {ValueMask_EXT_INT1 = 0x00010000};
							enum {Value_EXT_INT2 = 0x2};
							enum {ValueMask_EXT_INT2 = 0x00020000};
							enum {Value_EXT_INT3 = 0x3};
							enum {ValueMask_EXT_INT3 = 0x00030000};
							enum {Value_EXT_INT4 = 0x4};
							enum {ValueMask_EXT_INT4 = 0x00040000};
							enum {Value_TimerZero = 0x5};
							enum {ValueMask_TimerZero = 0x00050000};
							enum {Value_TimerOne = 0x6};
							enum {ValueMask_TimerOne = 0x00060000};
							enum {Value_Uart = 0x7};
							enum {ValueMask_Uart = 0x00070000};
							enum {Value_DmaZero = 0x9};
							enum {ValueMask_DmaZero = 0x00090000};
							enum {Value_DmaOne = 0xA};
							enum {ValueMask_DmaOne = 0x000A0000};
							enum {Value_EthernetMacZero = 0x13};
							enum {ValueMask_EthernetMacZero = 0x00130000};
							enum {Value_DSP_GPIO = 0x15};
							enum {ValueMask_DSP_GPIO = 0x00150000};
							enum {Value_DspReset = 0x16};
							enum {ValueMask_DspReset = 0x00160000};
							enum {Value_TELE_INT = 0x17};
							enum {ValueMask_TELE_INT = 0x00170000};
							enum {Value_UsbDevice = 0x18};
							enum {ValueMask_UsbDevice = 0x00180000};
							enum {Value_VLYNQ5 = 0x19};
							enum {ValueMask_VLYNQ5 = 0x00190000};
							enum {Value_VLYNQ3 = 0x1A};
							enum {ValueMask_VLYNQ3 = 0x001A0000};
							enum {Value_EthernetSwitchDMA = 0x1B};
							enum {ValueMask_EthernetSwitchDMA = 0x001B0000};
							enum {Value_EthernetPHYs = 0x1C};
							enum {ValueMask_EthernetPHYs = 0x001C0000};
							enum {Value_SSP = 0x1D};
							enum {ValueMask_SSP = 0x001D0000};
							enum {Value_DmaTwo = 0x1E};
							enum {ValueMask_DmaTwo = 0x001E0000};
							enum {Value_DmaThree = 0x1F};
							enum {ValueMask_DmaThree = 0x001F0000};
							enum {Value_Telephony = 0x20};
							enum {ValueMask_Telephony = 0x00200000};
							enum {Value_EthernetMacOne = 0x21};
							enum {ValueMask_EthernetMacOne = 0x00210000};
							enum {Value_UsbHost = 0x23};
							enum {ValueMask_UsbHost = 0x00230000};
							enum {Value_LCD = 0x24};
							enum {ValueMask_LCD = 0x00240000};
							enum {Value_Keypad = 0x25};
							enum {ValueMask_Keypad = 0x00250000};
							enum {Value_NonePending = 0x28};
							enum {ValueMask_NonePending = 0x00280000};
							};
						namespace PRIORITY_VALUE { // Field Description
							enum {Lsb = 0};
							enum {FieldMask = 0x0000003F};
							enum {Value_NonePending = 0x28};
							enum {ValueMask_NonePending = 0x00000028};
							};
						};
					namespace PRIORITY_INDEX_MSK { // Register description
						typedef uint32_t	Reg;
						namespace PRIORITY_MSK { // Field Description
							enum {Lsb = 0};
							enum {FieldMask = 0x0000003F};
							enum {Value_MaskAll = 0x0};
							enum {ValueMask_MaskAll = 0x00000000};
							enum {Value_DisallowAll = 0x0};
							enum {ValueMask_DisallowAll = 0x00000000};
							enum {Value_UnmaskAll = 0x28};
							enum {ValueMask_UnmaskAll = 0x00000028};
							enum {Value_AllowAll = 0x28};
							enum {ValueMask_AllowAll = 0x00000028};
							};
						};
					namespace POLARITY_1 { // Register description
						typedef uint32_t	Reg;
						enum {ActiveHigh = 1};
						enum {ActiveLow = 0};
						enum {Mask = 1};
						namespace Secondary { // Field Description
							enum {Lsb = 0};
							enum {FieldMask = 0x00000001};
							enum {Value_ActiveHigh = 0x1};
							enum {ValueMask_ActiveHigh = 0x00000001};
							enum {Value_ActiveLow = 0x0};
							enum {ValueMask_ActiveLow = 0x00000000};
							};
						namespace EXT_INT1 { // Field Description
							enum {Lsb = 1};
							enum {FieldMask = 0x00000002};
							enum {Value_ActiveHigh = 0x1};
							enum {ValueMask_ActiveHigh = 0x00000002};
							enum {Value_ActiveLow = 0x0};
							enum {ValueMask_ActiveLow = 0x00000000};
							};
						namespace EXT_INT2 { // Field Description
							enum {Lsb = 2};
							enum {FieldMask = 0x00000004};
							enum {Value_ActiveHigh = 0x1};
							enum {ValueMask_ActiveHigh = 0x00000004};
							enum {Value_ActiveLow = 0x0};
							enum {ValueMask_ActiveLow = 0x00000000};
							};
						namespace EXT_INT3 { // Field Description
							enum {Lsb = 3};
							enum {FieldMask = 0x00000008};
							enum {Value_ActiveHigh = 0x1};
							enum {ValueMask_ActiveHigh = 0x00000008};
							enum {Value_ActiveLow = 0x0};
							enum {ValueMask_ActiveLow = 0x00000000};
							};
						namespace EXT_INT4 { // Field Description
							enum {Lsb = 4};
							enum {FieldMask = 0x00000010};
							enum {Value_ActiveHigh = 0x1};
							enum {ValueMask_ActiveHigh = 0x00000010};
							enum {Value_ActiveLow = 0x0};
							enum {ValueMask_ActiveLow = 0x00000000};
							};
						namespace TimerZero { // Field Description
							enum {Lsb = 5};
							enum {FieldMask = 0x00000020};
							enum {Value_ActiveHigh = 0x1};
							enum {ValueMask_ActiveHigh = 0x00000020};
							enum {Value_ActiveLow = 0x0};
							enum {ValueMask_ActiveLow = 0x00000000};
							};
						namespace TimerOne { // Field Description
							enum {Lsb = 6};
							enum {FieldMask = 0x00000040};
							enum {Value_ActiveHigh = 0x1};
							enum {ValueMask_ActiveHigh = 0x00000040};
							enum {Value_ActiveLow = 0x0};
							enum {ValueMask_ActiveLow = 0x00000000};
							};
						namespace Uart { // Field Description
							enum {Lsb = 7};
							enum {FieldMask = 0x00000080};
							enum {Value_ActiveHigh = 0x1};
							enum {ValueMask_ActiveHigh = 0x00000080};
							enum {Value_ActiveLow = 0x0};
							enum {ValueMask_ActiveLow = 0x00000000};
							};
						namespace DmaZero { // Field Description
							enum {Lsb = 9};
							enum {FieldMask = 0x00000200};
							enum {Value_ActiveHigh = 0x1};
							enum {ValueMask_ActiveHigh = 0x00000200};
							enum {Value_ActiveLow = 0x0};
							enum {ValueMask_ActiveLow = 0x00000000};
							};
						namespace DmaOne { // Field Description
							enum {Lsb = 10};
							enum {FieldMask = 0x00000400};
							enum {Value_ActiveHigh = 0x1};
							enum {ValueMask_ActiveHigh = 0x00000400};
							enum {Value_ActiveLow = 0x0};
							enum {ValueMask_ActiveLow = 0x00000000};
							};
						namespace EthernetMacZero { // Field Description
							enum {Lsb = 19};
							enum {FieldMask = 0x00080000};
							enum {Value_ActiveHigh = 0x1};
							enum {ValueMask_ActiveHigh = 0x00080000};
							enum {Value_ActiveLow = 0x0};
							enum {ValueMask_ActiveLow = 0x00000000};
							};
						namespace DSP_GPIO { // Field Description
							enum {Lsb = 21};
							enum {FieldMask = 0x00200000};
							enum {Value_ActiveHigh = 0x1};
							enum {ValueMask_ActiveHigh = 0x00200000};
							enum {Value_ActiveLow = 0x0};
							enum {ValueMask_ActiveLow = 0x00000000};
							};
						namespace DspReset { // Field Description
							enum {Lsb = 22};
							enum {FieldMask = 0x00400000};
							enum {Value_ActiveHigh = 0x1};
							enum {ValueMask_ActiveHigh = 0x00400000};
							enum {Value_ActiveLow = 0x0};
							enum {ValueMask_ActiveLow = 0x00000000};
							};
						namespace TELE_INT { // Field Description
							enum {Lsb = 23};
							enum {FieldMask = 0x00800000};
							enum {Value_ActiveHigh = 0x1};
							enum {ValueMask_ActiveHigh = 0x00800000};
							enum {Value_ActiveLow = 0x0};
							enum {ValueMask_ActiveLow = 0x00000000};
							};
						namespace UsbDevice { // Field Description
							enum {Lsb = 24};
							enum {FieldMask = 0x01000000};
							enum {Value_ActiveHigh = 0x1};
							enum {ValueMask_ActiveHigh = 0x01000000};
							enum {Value_ActiveLow = 0x0};
							enum {ValueMask_ActiveLow = 0x00000000};
							};
						namespace VLYNQ5 { // Field Description
							enum {Lsb = 25};
							enum {FieldMask = 0x02000000};
							enum {Value_ActiveHigh = 0x1};
							enum {ValueMask_ActiveHigh = 0x02000000};
							enum {Value_ActiveLow = 0x0};
							enum {ValueMask_ActiveLow = 0x00000000};
							};
						namespace VLYNQ3 { // Field Description
							enum {Lsb = 26};
							enum {FieldMask = 0x04000000};
							enum {Value_ActiveHigh = 0x1};
							enum {ValueMask_ActiveHigh = 0x04000000};
							enum {Value_ActiveLow = 0x0};
							enum {ValueMask_ActiveLow = 0x00000000};
							};
						namespace EthernetSwitchDMA { // Field Description
							enum {Lsb = 27};
							enum {FieldMask = 0x08000000};
							enum {Value_ActiveHigh = 0x1};
							enum {ValueMask_ActiveHigh = 0x08000000};
							enum {Value_ActiveLow = 0x0};
							enum {ValueMask_ActiveLow = 0x00000000};
							};
						namespace EthernetPHYs { // Field Description
							enum {Lsb = 28};
							enum {FieldMask = 0x10000000};
							enum {Value_ActiveHigh = 0x1};
							enum {ValueMask_ActiveHigh = 0x10000000};
							enum {Value_ActiveLow = 0x0};
							enum {ValueMask_ActiveLow = 0x00000000};
							};
						namespace SSP { // Field Description
							enum {Lsb = 29};
							enum {FieldMask = 0x20000000};
							enum {Value_ActiveHigh = 0x1};
							enum {ValueMask_ActiveHigh = 0x20000000};
							enum {Value_ActiveLow = 0x0};
							enum {ValueMask_ActiveLow = 0x00000000};
							};
						namespace DmaTwo { // Field Description
							enum {Lsb = 30};
							enum {FieldMask = 0x40000000};
							enum {Value_ActiveHigh = 0x1};
							enum {ValueMask_ActiveHigh = 0x40000000};
							enum {Value_ActiveLow = 0x0};
							enum {ValueMask_ActiveLow = 0x00000000};
							};
						namespace DmaThree { // Field Description
							enum {Lsb = 31};
							enum {FieldMask = 0x80000000};
							enum {Value_ActiveHigh = 0x1};
							enum {ValueMask_ActiveHigh = 0x80000000};
							enum {Value_ActiveLow = 0x0};
							enum {ValueMask_ActiveLow = 0x00000000};
							};
						};
					namespace POLARITY_2 { // Register description
						typedef uint32_t	Reg;
						enum {ActiveHigh = 1};
						enum {ActiveLow = 0};
						enum {Mask = 1};
						namespace Telephony { // Field Description
							enum {Lsb = 0};
							enum {FieldMask = 0x00000001};
							enum {Value_ActiveHigh = 0x1};
							enum {ValueMask_ActiveHigh = 0x00000001};
							enum {Value_ActiveLow = 0x0};
							enum {ValueMask_ActiveLow = 0x00000000};
							};
						namespace EthernetMacOne { // Field Description
							enum {Lsb = 1};
							enum {FieldMask = 0x00000002};
							enum {Value_ActiveHigh = 0x1};
							enum {ValueMask_ActiveHigh = 0x00000002};
							enum {Value_ActiveLow = 0x0};
							enum {ValueMask_ActiveLow = 0x00000000};
							};
						namespace UsbHost { // Field Description
							enum {Lsb = 3};
							enum {FieldMask = 0x00000008};
							enum {Value_ActiveHigh = 0x1};
							enum {ValueMask_ActiveHigh = 0x00000008};
							enum {Value_ActiveLow = 0x0};
							enum {ValueMask_ActiveLow = 0x00000000};
							};
						namespace LCD { // Field Description
							enum {Lsb = 4};
							enum {FieldMask = 0x00000010};
							enum {Value_ActiveHigh = 0x1};
							enum {ValueMask_ActiveHigh = 0x00000010};
							enum {Value_ActiveLow = 0x0};
							enum {ValueMask_ActiveLow = 0x00000000};
							};
						namespace Keypad { // Field Description
							enum {Lsb = 5};
							enum {FieldMask = 0x00000020};
							enum {Value_ActiveHigh = 0x1};
							enum {ValueMask_ActiveHigh = 0x00000020};
							enum {Value_ActiveLow = 0x0};
							enum {ValueMask_ActiveLow = 0x00000000};
							};
						};
					namespace TYPE_1 { // Register description
						typedef uint32_t	Reg;
						enum {Edge = 1};
						enum {Level = 0};
						enum {Mask = 1};
						namespace Secondary { // Field Description
							enum {Lsb = 0};
							enum {FieldMask = 0x00000001};
							enum {Value_Edge = 0x1};
							enum {ValueMask_Edge = 0x00000001};
							enum {Value_Level = 0x0};
							enum {ValueMask_Level = 0x00000000};
							};
						namespace EXT_INT1 { // Field Description
							enum {Lsb = 1};
							enum {FieldMask = 0x00000002};
							enum {Value_Edge = 0x1};
							enum {ValueMask_Edge = 0x00000002};
							enum {Value_Level = 0x0};
							enum {ValueMask_Level = 0x00000000};
							};
						namespace EXT_INT2 { // Field Description
							enum {Lsb = 2};
							enum {FieldMask = 0x00000004};
							enum {Value_Edge = 0x1};
							enum {ValueMask_Edge = 0x00000004};
							enum {Value_Level = 0x0};
							enum {ValueMask_Level = 0x00000000};
							};
						namespace EXT_INT3 { // Field Description
							enum {Lsb = 3};
							enum {FieldMask = 0x00000008};
							enum {Value_Edge = 0x1};
							enum {ValueMask_Edge = 0x00000008};
							enum {Value_Level = 0x0};
							enum {ValueMask_Level = 0x00000000};
							};
						namespace EXT_INT4 { // Field Description
							enum {Lsb = 4};
							enum {FieldMask = 0x00000010};
							enum {Value_Edge = 0x1};
							enum {ValueMask_Edge = 0x00000010};
							enum {Value_Level = 0x0};
							enum {ValueMask_Level = 0x00000000};
							};
						namespace TimerZero { // Field Description
							enum {Lsb = 5};
							enum {FieldMask = 0x00000020};
							enum {Value_Edge = 0x1};
							enum {ValueMask_Edge = 0x00000020};
							enum {Value_Level = 0x0};
							enum {ValueMask_Level = 0x00000000};
							};
						namespace TimerOne { // Field Description
							enum {Lsb = 6};
							enum {FieldMask = 0x00000040};
							enum {Value_Edge = 0x1};
							enum {ValueMask_Edge = 0x00000040};
							enum {Value_Level = 0x0};
							enum {ValueMask_Level = 0x00000000};
							};
						namespace Uart { // Field Description
							enum {Lsb = 7};
							enum {FieldMask = 0x00000080};
							enum {Value_Edge = 0x1};
							enum {ValueMask_Edge = 0x00000080};
							enum {Value_Level = 0x0};
							enum {ValueMask_Level = 0x00000000};
							};
						namespace DmaZero { // Field Description
							enum {Lsb = 9};
							enum {FieldMask = 0x00000200};
							enum {Value_Edge = 0x1};
							enum {ValueMask_Edge = 0x00000200};
							enum {Value_Level = 0x0};
							enum {ValueMask_Level = 0x00000000};
							};
						namespace DmaOne { // Field Description
							enum {Lsb = 10};
							enum {FieldMask = 0x00000400};
							enum {Value_Edge = 0x1};
							enum {ValueMask_Edge = 0x00000400};
							enum {Value_Level = 0x0};
							enum {ValueMask_Level = 0x00000000};
							};
						namespace EthernetMacZero { // Field Description
							enum {Lsb = 19};
							enum {FieldMask = 0x00080000};
							enum {Value_Edge = 0x1};
							enum {ValueMask_Edge = 0x00080000};
							enum {Value_Level = 0x0};
							enum {ValueMask_Level = 0x00000000};
							};
						namespace DSP_GPIO { // Field Description
							enum {Lsb = 21};
							enum {FieldMask = 0x00200000};
							enum {Value_Edge = 0x1};
							enum {ValueMask_Edge = 0x00200000};
							enum {Value_Level = 0x0};
							enum {ValueMask_Level = 0x00000000};
							};
						namespace DspReset { // Field Description
							enum {Lsb = 22};
							enum {FieldMask = 0x00400000};
							enum {Value_Edge = 0x1};
							enum {ValueMask_Edge = 0x00400000};
							enum {Value_Level = 0x0};
							enum {ValueMask_Level = 0x00000000};
							};
						namespace TELE_INT { // Field Description
							enum {Lsb = 23};
							enum {FieldMask = 0x00800000};
							enum {Value_Edge = 0x1};
							enum {ValueMask_Edge = 0x00800000};
							enum {Value_Level = 0x0};
							enum {ValueMask_Level = 0x00000000};
							};
						namespace UsbDevice { // Field Description
							enum {Lsb = 24};
							enum {FieldMask = 0x01000000};
							enum {Value_Edge = 0x1};
							enum {ValueMask_Edge = 0x01000000};
							enum {Value_Level = 0x0};
							enum {ValueMask_Level = 0x00000000};
							};
						namespace VLYNQ5 { // Field Description
							enum {Lsb = 25};
							enum {FieldMask = 0x02000000};
							enum {Value_Edge = 0x1};
							enum {ValueMask_Edge = 0x02000000};
							enum {Value_Level = 0x0};
							enum {ValueMask_Level = 0x00000000};
							};
						namespace VLYNQ3 { // Field Description
							enum {Lsb = 26};
							enum {FieldMask = 0x04000000};
							enum {Value_Edge = 0x1};
							enum {ValueMask_Edge = 0x04000000};
							enum {Value_Level = 0x0};
							enum {ValueMask_Level = 0x00000000};
							};
						namespace EthernetSwitchDMA { // Field Description
							enum {Lsb = 27};
							enum {FieldMask = 0x08000000};
							enum {Value_Edge = 0x1};
							enum {ValueMask_Edge = 0x08000000};
							enum {Value_Level = 0x0};
							enum {ValueMask_Level = 0x00000000};
							};
						namespace EthernetPHYs { // Field Description
							enum {Lsb = 28};
							enum {FieldMask = 0x10000000};
							enum {Value_Edge = 0x1};
							enum {ValueMask_Edge = 0x10000000};
							enum {Value_Level = 0x0};
							enum {ValueMask_Level = 0x00000000};
							};
						namespace SSP { // Field Description
							enum {Lsb = 29};
							enum {FieldMask = 0x20000000};
							enum {Value_Edge = 0x1};
							enum {ValueMask_Edge = 0x20000000};
							enum {Value_Level = 0x0};
							enum {ValueMask_Level = 0x00000000};
							};
						namespace DmaTwo { // Field Description
							enum {Lsb = 30};
							enum {FieldMask = 0x40000000};
							enum {Value_Edge = 0x1};
							enum {ValueMask_Edge = 0x40000000};
							enum {Value_Level = 0x0};
							enum {ValueMask_Level = 0x00000000};
							};
						namespace DmaThree { // Field Description
							enum {Lsb = 31};
							enum {FieldMask = 0x80000000};
							enum {Value_Edge = 0x1};
							enum {ValueMask_Edge = 0x80000000};
							enum {Value_Level = 0x0};
							enum {ValueMask_Level = 0x00000000};
							};
						};
					namespace TYPE_2 { // Register description
						typedef uint32_t	Reg;
						enum {Edge = 1};
						enum {Level = 0};
						enum {Mask = 1};
						namespace Telephony { // Field Description
							enum {Lsb = 0};
							enum {FieldMask = 0x00000001};
							enum {Value_Edge = 0x1};
							enum {ValueMask_Edge = 0x00000001};
							enum {Value_Level = 0x0};
							enum {ValueMask_Level = 0x00000000};
							};
						namespace EthernetMacOne { // Field Description
							enum {Lsb = 1};
							enum {FieldMask = 0x00000002};
							enum {Value_Edge = 0x1};
							enum {ValueMask_Edge = 0x00000002};
							enum {Value_Level = 0x0};
							enum {ValueMask_Level = 0x00000000};
							};
						namespace UsbHost { // Field Description
							enum {Lsb = 3};
							enum {FieldMask = 0x00000008};
							enum {Value_Edge = 0x1};
							enum {ValueMask_Edge = 0x00000008};
							enum {Value_Level = 0x0};
							enum {ValueMask_Level = 0x00000000};
							};
						namespace LCD { // Field Description
							enum {Lsb = 4};
							enum {FieldMask = 0x00000010};
							enum {Value_Edge = 0x1};
							enum {ValueMask_Edge = 0x00000010};
							enum {Value_Level = 0x0};
							enum {ValueMask_Level = 0x00000000};
							};
						namespace Keypad { // Field Description
							enum {Lsb = 5};
							enum {FieldMask = 0x00000020};
							enum {Value_Edge = 0x1};
							enum {ValueMask_Edge = 0x00000020};
							enum {Value_Level = 0x0};
							enum {ValueMask_Level = 0x00000000};
							};
						};
					namespace EXCPT_STAT_MSK { // Register description
						typedef uint32_t	Reg;
						enum {Pending = 1};
						enum {Mask = 1};
						enum {Set = 1};
						namespace EmifError { // Field Description
							enum {Lsb = 7};
							enum {FieldMask = 0x00000080};
							enum {Value_Pending = 0x1};
							enum {ValueMask_Pending = 0x00000080};
							enum {Value_Set = 0x1};
							enum {ValueMask_Set = 0x00000080};
							};
						namespace DspWatchdog { // Field Description
							enum {Lsb = 8};
							enum {FieldMask = 0x00000100};
							enum {Value_Pending = 0x1};
							enum {ValueMask_Pending = 0x00000100};
							enum {Value_Set = 0x1};
							enum {ValueMask_Set = 0x00000100};
							};
						};
					namespace EXCPT_STAT_RAW { // Register description
						typedef uint32_t	Reg;
						enum {Active = 1};
						enum {Mask = 1};
						enum {Clear = 1};
						namespace EmifError { // Field Description
							enum {Lsb = 7};
							enum {FieldMask = 0x00000080};
							enum {Value_Active = 0x1};
							enum {ValueMask_Active = 0x00000080};
							enum {Value_Clear = 0x1};
							enum {ValueMask_Clear = 0x00000080};
							};
						namespace DspWatchdog { // Field Description
							enum {Lsb = 8};
							enum {FieldMask = 0x00000100};
							enum {Value_Active = 0x1};
							enum {ValueMask_Active = 0x00000100};
							enum {Value_Clear = 0x1};
							enum {ValueMask_Clear = 0x00000100};
							};
						};
					namespace EXCPT_ENBL_SET { // Register description
						typedef uint32_t	Reg;
						enum {Enabled = 1};
						enum {Disabled = 0};
						enum {Mask = 1};
						enum {Enable = 1};
						namespace EmifError { // Field Description
							enum {Lsb = 7};
							enum {FieldMask = 0x00000080};
							enum {Value_Enabled = 0x1};
							enum {ValueMask_Enabled = 0x00000080};
							enum {Value_Disabled = 0x0};
							enum {ValueMask_Disabled = 0x00000000};
							enum {Value_Enable = 0x1};
							enum {ValueMask_Enable = 0x00000080};
							};
						namespace DspWatchdog { // Field Description
							enum {Lsb = 8};
							enum {FieldMask = 0x00000100};
							enum {Value_Enabled = 0x1};
							enum {ValueMask_Enabled = 0x00000100};
							enum {Value_Disabled = 0x0};
							enum {ValueMask_Disabled = 0x00000000};
							enum {Value_Enable = 0x1};
							enum {ValueMask_Enable = 0x00000100};
							};
						};
					namespace EXCPT_ENBL_CLR { // Register description
						typedef uint32_t	Reg;
						enum {Mask = 1};
						enum {Disable = 1};
						namespace EmifError { // Field Description
							enum {Lsb = 7};
							enum {FieldMask = 0x00000080};
							enum {Value_Disable = 0x1};
							enum {ValueMask_Disable = 0x00000080};
							};
						namespace DspWatchdog { // Field Description
							enum {Lsb = 8};
							enum {FieldMask = 0x00000100};
							enum {Value_Disable = 0x1};
							enum {ValueMask_Disable = 0x00000100};
							};
						};
					namespace PACE_PRESCALE { // Register description
						typedef uint32_t	Reg;
						namespace PRESCALE { // Field Description
							enum {Lsb = 0};
							enum {FieldMask = 0x00000FFF};
							};
						};
					namespace PACE_MAP { // Register description
						typedef uint32_t	Reg;
						namespace PACE_MAP_0 { // Field Description
							enum {Lsb = 0};
							enum {FieldMask = 0x0000003F};
							};
						namespace PACE_MAP_1 { // Field Description
							enum {Lsb = 8};
							enum {FieldMask = 0x00003F00};
							};
						namespace PACE_MAP_2 { // Field Description
							enum {Lsb = 16};
							enum {FieldMask = 0x003F0000};
							};
						namespace PACE_MAP_3 { // Field Description
							enum {Lsb = 24};
							enum {FieldMask = 0x3F000000};
							};
						};
					namespace PACE_ENBL { // Register description
						typedef uint32_t	Reg;
						enum {Activated = 1};
						enum {Deactivated = 0};
						enum {Activate = 1};
						enum {Deactivate = 0};
						namespace PACE_TARGET_0 { // Field Description
							enum {Lsb = 0};
							enum {FieldMask = 0x0000003F};
							};
						namespace PACE_ENBL_0 { // Field Description
							enum {Lsb = 7};
							enum {FieldMask = 0x00000080};
							enum {Value_Activated = 0x1};
							enum {ValueMask_Activated = 0x00000080};
							enum {Value_Deactivated = 0x0};
							enum {ValueMask_Deactivated = 0x00000000};
							enum {Value_Activate = 0x1};
							enum {ValueMask_Activate = 0x00000080};
							enum {Value_Deactivate = 0x0};
							enum {ValueMask_Deactivate = 0x00000000};
							};
						namespace PACE_TARGET_1 { // Field Description
							enum {Lsb = 8};
							enum {FieldMask = 0x00003F00};
							};
						namespace PACE_ENBL_1 { // Field Description
							enum {Lsb = 15};
							enum {FieldMask = 0x00008000};
							enum {Value_Activated = 0x1};
							enum {ValueMask_Activated = 0x00008000};
							enum {Value_Deactivated = 0x0};
							enum {ValueMask_Deactivated = 0x00000000};
							enum {Value_Activate = 0x1};
							enum {ValueMask_Activate = 0x00008000};
							enum {Value_Deactivate = 0x0};
							enum {ValueMask_Deactivate = 0x00000000};
							};
						namespace PACE_TARGET_2 { // Field Description
							enum {Lsb = 16};
							enum {FieldMask = 0x003F0000};
							};
						namespace PACE_ENBL_2 { // Field Description
							enum {Lsb = 23};
							enum {FieldMask = 0x00800000};
							enum {Value_Activated = 0x1};
							enum {ValueMask_Activated = 0x00800000};
							enum {Value_Deactivated = 0x0};
							enum {ValueMask_Deactivated = 0x00000000};
							enum {Value_Activate = 0x1};
							enum {ValueMask_Activate = 0x00800000};
							enum {Value_Deactivate = 0x0};
							enum {ValueMask_Deactivate = 0x00000000};
							};
						namespace PACE_TARGET_3 { // Field Description
							enum {Lsb = 24};
							enum {FieldMask = 0x3F000000};
							};
						namespace PACE_ENBL_3 { // Field Description
							enum {Lsb = 31};
							enum {FieldMask = 0x80000000};
							enum {Value_Activated = 0x1};
							enum {ValueMask_Activated = 0x80000000};
							enum {Value_Deactivated = 0x0};
							enum {ValueMask_Deactivated = 0x00000000};
							enum {Value_Activate = 0x1};
							enum {ValueMask_Activate = 0x80000000};
							enum {Value_Deactivate = 0x0};
							enum {ValueMask_Deactivate = 0x00000000};
							};
						};
					namespace PRIORITY { // Register description
						typedef uint32_t	Reg;
						namespace INT_NUM { // Field Description
							enum {Lsb = 0};
							enum {FieldMask = 0x0000003F};
							};
						};
					};
				};
			};
		};
	};
#endif
