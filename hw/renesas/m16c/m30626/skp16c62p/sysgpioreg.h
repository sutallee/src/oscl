#ifndef _hw_ti_tnetv105x_sysgpioregh_
#define _hw_ti_tnetv105x_sysgpioregh_
#include <stdint.h>
namespace Oscl { // Namespace description
	namespace HW { // Namespace description
		namespace TI { // Namespace description
			namespace TNETV105X { // Namespace description
				namespace SYS_GPIO { // Namespace description
					namespace DATA_IN { // Register description
						typedef uint32_t	Reg;
						enum {High = 1};
						enum {Low = 0};
						enum {Mask = 1};
						};
					namespace DATA_OUT { // Register description
						typedef uint32_t	Reg;
						enum {High = 1};
						enum {Low = 0};
						enum {Mask = 1};
						};
					namespace DIR { // Register description
						typedef uint32_t	Reg;
						enum {Input = 1};
						enum {Output = 0};
						enum {Mask = 1};
						};
					namespace ENBL { // Register description
						typedef uint32_t	Reg;
						enum {Enabled = 1};
						enum {Enable = 1};
						enum {Disable = 0};
						enum {Disabled = 0};
						enum {Mask = 1};
						};
					namespace CHIP_REV { // Register description
						typedef uint32_t	Reg;
						namespace REV { // Field Description
							enum {Lsb = 16};
							enum {FieldMask = 0xFFFF0000};
							};
						namespace ID { // Field Description
							enum {Lsb = 0};
							enum {FieldMask = 0x0000FFFF};
							enum {Value_TNETV1050 = 0x7};
							enum {ValueMask_TNETV1050 = 0x00000007};
							};
						};
					namespace DIE_ID_LO { // Register description
						typedef uint32_t	Reg;
						namespace MSB { // Field Description
							enum {Lsb = 16};
							enum {FieldMask = 0xFFFF0000};
							};
						namespace LSB { // Field Description
							enum {Lsb = 0};
							enum {FieldMask = 0x0000FFFF};
							};
						};
					namespace DIE_ID_HI { // Register description
						typedef uint32_t	Reg;
						namespace MSB { // Field Description
							enum {Lsb = 16};
							enum {FieldMask = 0xFFFF0000};
							};
						namespace LSB { // Field Description
							enum {Lsb = 0};
							enum {FieldMask = 0x0000FFFF};
							};
						};
					};
				};
			};
		};
	};
#endif
