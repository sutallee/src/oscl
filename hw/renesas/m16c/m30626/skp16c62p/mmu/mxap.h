/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _hw_mot8xx_mmu_mxaph_
#define _hw_mot8xx_mmu_mxaph_
#include <stdint.h>
namespace Oscl { // Namespace description
	namespace Mot8xx { // Namespace description
		namespace Mmu { // Namespace description
			namespace MxAp { // Register description
				typedef uint32_t	Reg;
				namespace Gpm0 { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0xFFFFFFFF};
					namespace GP15 { // Field Description
						enum {Lsb = 0};
						enum {FieldMask = 0x00000003};
						enum {Value_AllAccessSupervisor = 0x0};
						enum {ValueMask_AllAccessSupervisor = 0x00000000};
						enum {Value_PageProtection = 0x1};
						enum {ValueMask_PageProtection = 0x00000001};
						enum {Value_UserAndSuperSwapped = 0x2};
						enum {ValueMask_UserAndSuperSwapped = 0x00000002};
						enum {Value_AllAccessUser = 0x3};
						enum {ValueMask_AllAccessUser = 0x00000003};
						};
					namespace GP14 { // Field Description
						enum {Lsb = 2};
						enum {FieldMask = 0x0000000C};
						enum {Value_AllAccessSupervisor = 0x0};
						enum {ValueMask_AllAccessSupervisor = 0x00000000};
						enum {Value_PageProtection = 0x1};
						enum {ValueMask_PageProtection = 0x00000004};
						enum {Value_UserAndSuperSwapped = 0x2};
						enum {ValueMask_UserAndSuperSwapped = 0x00000008};
						enum {Value_AllAccessUser = 0x3};
						enum {ValueMask_AllAccessUser = 0x0000000C};
						};
					namespace GP13 { // Field Description
						enum {Lsb = 4};
						enum {FieldMask = 0x00000030};
						enum {Value_AllAccessSupervisor = 0x0};
						enum {ValueMask_AllAccessSupervisor = 0x00000000};
						enum {Value_PageProtection = 0x1};
						enum {ValueMask_PageProtection = 0x00000010};
						enum {Value_UserAndSuperSwapped = 0x2};
						enum {ValueMask_UserAndSuperSwapped = 0x00000020};
						enum {Value_AllAccessUser = 0x3};
						enum {ValueMask_AllAccessUser = 0x00000030};
						};
					namespace GP12 { // Field Description
						enum {Lsb = 6};
						enum {FieldMask = 0x000000C0};
						enum {Value_AllAccessSupervisor = 0x0};
						enum {ValueMask_AllAccessSupervisor = 0x00000000};
						enum {Value_PageProtection = 0x1};
						enum {ValueMask_PageProtection = 0x00000040};
						enum {Value_UserAndSuperSwapped = 0x2};
						enum {ValueMask_UserAndSuperSwapped = 0x00000080};
						enum {Value_AllAccessUser = 0x3};
						enum {ValueMask_AllAccessUser = 0x000000C0};
						};
					namespace GP11 { // Field Description
						enum {Lsb = 8};
						enum {FieldMask = 0x00000300};
						enum {Value_AllAccessSupervisor = 0x0};
						enum {ValueMask_AllAccessSupervisor = 0x00000000};
						enum {Value_PageProtection = 0x1};
						enum {ValueMask_PageProtection = 0x00000100};
						enum {Value_UserAndSuperSwapped = 0x2};
						enum {ValueMask_UserAndSuperSwapped = 0x00000200};
						enum {Value_AllAccessUser = 0x3};
						enum {ValueMask_AllAccessUser = 0x00000300};
						};
					namespace GP10 { // Field Description
						enum {Lsb = 10};
						enum {FieldMask = 0x00000C00};
						enum {Value_AllAccessSupervisor = 0x0};
						enum {ValueMask_AllAccessSupervisor = 0x00000000};
						enum {Value_PageProtection = 0x1};
						enum {ValueMask_PageProtection = 0x00000400};
						enum {Value_UserAndSuperSwapped = 0x2};
						enum {ValueMask_UserAndSuperSwapped = 0x00000800};
						enum {Value_AllAccessUser = 0x3};
						enum {ValueMask_AllAccessUser = 0x00000C00};
						};
					namespace GP9 { // Field Description
						enum {Lsb = 12};
						enum {FieldMask = 0x00003000};
						enum {Value_AllAccessSupervisor = 0x0};
						enum {ValueMask_AllAccessSupervisor = 0x00000000};
						enum {Value_PageProtection = 0x1};
						enum {ValueMask_PageProtection = 0x00001000};
						enum {Value_UserAndSuperSwapped = 0x2};
						enum {ValueMask_UserAndSuperSwapped = 0x00002000};
						enum {Value_AllAccessUser = 0x3};
						enum {ValueMask_AllAccessUser = 0x00003000};
						};
					namespace GP8 { // Field Description
						enum {Lsb = 14};
						enum {FieldMask = 0x0000C000};
						enum {Value_AllAccessSupervisor = 0x0};
						enum {ValueMask_AllAccessSupervisor = 0x00000000};
						enum {Value_PageProtection = 0x1};
						enum {ValueMask_PageProtection = 0x00004000};
						enum {Value_UserAndSuperSwapped = 0x2};
						enum {ValueMask_UserAndSuperSwapped = 0x00008000};
						enum {Value_AllAccessUser = 0x3};
						enum {ValueMask_AllAccessUser = 0x0000C000};
						};
					namespace GP7 { // Field Description
						enum {Lsb = 16};
						enum {FieldMask = 0x00030000};
						enum {Value_AllAccessSupervisor = 0x0};
						enum {ValueMask_AllAccessSupervisor = 0x00000000};
						enum {Value_PageProtection = 0x1};
						enum {ValueMask_PageProtection = 0x00010000};
						enum {Value_UserAndSuperSwapped = 0x2};
						enum {ValueMask_UserAndSuperSwapped = 0x00020000};
						enum {Value_AllAccessUser = 0x3};
						enum {ValueMask_AllAccessUser = 0x00030000};
						};
					namespace GP6 { // Field Description
						enum {Lsb = 18};
						enum {FieldMask = 0x000C0000};
						enum {Value_AllAccessSupervisor = 0x0};
						enum {ValueMask_AllAccessSupervisor = 0x00000000};
						enum {Value_PageProtection = 0x1};
						enum {ValueMask_PageProtection = 0x00040000};
						enum {Value_UserAndSuperSwapped = 0x2};
						enum {ValueMask_UserAndSuperSwapped = 0x00080000};
						enum {Value_AllAccessUser = 0x3};
						enum {ValueMask_AllAccessUser = 0x000C0000};
						};
					namespace GP5 { // Field Description
						enum {Lsb = 20};
						enum {FieldMask = 0x00300000};
						enum {Value_AllAccessSupervisor = 0x0};
						enum {ValueMask_AllAccessSupervisor = 0x00000000};
						enum {Value_PageProtection = 0x1};
						enum {ValueMask_PageProtection = 0x00100000};
						enum {Value_UserAndSuperSwapped = 0x2};
						enum {ValueMask_UserAndSuperSwapped = 0x00200000};
						enum {Value_AllAccessUser = 0x3};
						enum {ValueMask_AllAccessUser = 0x00300000};
						};
					namespace GP4 { // Field Description
						enum {Lsb = 22};
						enum {FieldMask = 0x00C00000};
						enum {Value_AllAccessSupervisor = 0x0};
						enum {ValueMask_AllAccessSupervisor = 0x00000000};
						enum {Value_PageProtection = 0x1};
						enum {ValueMask_PageProtection = 0x00400000};
						enum {Value_UserAndSuperSwapped = 0x2};
						enum {ValueMask_UserAndSuperSwapped = 0x00800000};
						enum {Value_AllAccessUser = 0x3};
						enum {ValueMask_AllAccessUser = 0x00C00000};
						};
					namespace GP3 { // Field Description
						enum {Lsb = 24};
						enum {FieldMask = 0x03000000};
						enum {Value_AllAccessSupervisor = 0x0};
						enum {ValueMask_AllAccessSupervisor = 0x00000000};
						enum {Value_PageProtection = 0x1};
						enum {ValueMask_PageProtection = 0x01000000};
						enum {Value_UserAndSuperSwapped = 0x2};
						enum {ValueMask_UserAndSuperSwapped = 0x02000000};
						enum {Value_AllAccessUser = 0x3};
						enum {ValueMask_AllAccessUser = 0x03000000};
						};
					namespace GP2 { // Field Description
						enum {Lsb = 26};
						enum {FieldMask = 0x0C000000};
						enum {Value_AllAccessSupervisor = 0x0};
						enum {ValueMask_AllAccessSupervisor = 0x00000000};
						enum {Value_PageProtection = 0x1};
						enum {ValueMask_PageProtection = 0x04000000};
						enum {Value_UserAndSuperSwapped = 0x2};
						enum {ValueMask_UserAndSuperSwapped = 0x08000000};
						enum {Value_AllAccessUser = 0x3};
						enum {ValueMask_AllAccessUser = 0x0C000000};
						};
					namespace GP1 { // Field Description
						enum {Lsb = 28};
						enum {FieldMask = 0x30000000};
						enum {Value_AllAccessSupervisor = 0x0};
						enum {ValueMask_AllAccessSupervisor = 0x00000000};
						enum {Value_PageProtection = 0x1};
						enum {ValueMask_PageProtection = 0x10000000};
						enum {Value_UserAndSuperSwapped = 0x2};
						enum {ValueMask_UserAndSuperSwapped = 0x20000000};
						enum {Value_AllAccessUser = 0x3};
						enum {ValueMask_AllAccessUser = 0x30000000};
						};
					namespace GP0 { // Field Description
						enum {Lsb = 30};
						enum {FieldMask = 0xC0000000};
						enum {Value_AllAccessSupervisor = 0x0};
						enum {ValueMask_AllAccessSupervisor = 0x00000000};
						enum {Value_PageProtection = 0x1};
						enum {ValueMask_PageProtection = 0x40000000};
						enum {Value_UserAndSuperSwapped = 0x2};
						enum {ValueMask_UserAndSuperSwapped = 0x80000000};
						enum {Value_AllAccessUser = 0x3};
						enum {ValueMask_AllAccessUser = 0xC0000000};
						};
					};
				namespace Gpm1 { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0xFFFFFFFF};
					namespace GP15 { // Field Description
						enum {Lsb = 0};
						enum {FieldMask = 0x00000003};
						enum {Value_NoAccess = 0x0};
						enum {ValueMask_NoAccess = 0x00000000};
						enum {Value_ClientAccess = 0x1};
						enum {ValueMask_ClientAccess = 0x00000001};
						enum {Value_ManagerFree = 0x3};
						enum {ValueMask_ManagerFree = 0x00000003};
						};
					namespace GP14 { // Field Description
						enum {Lsb = 2};
						enum {FieldMask = 0x0000000C};
						enum {Value_NoAccess = 0x0};
						enum {ValueMask_NoAccess = 0x00000000};
						enum {Value_ClientAccess = 0x1};
						enum {ValueMask_ClientAccess = 0x00000004};
						enum {Value_ManagerFree = 0x3};
						enum {ValueMask_ManagerFree = 0x0000000C};
						};
					namespace GP13 { // Field Description
						enum {Lsb = 4};
						enum {FieldMask = 0x00000030};
						enum {Value_NoAccess = 0x0};
						enum {ValueMask_NoAccess = 0x00000000};
						enum {Value_ClientAccess = 0x1};
						enum {ValueMask_ClientAccess = 0x00000010};
						enum {Value_ManagerFree = 0x3};
						enum {ValueMask_ManagerFree = 0x00000030};
						};
					namespace GP12 { // Field Description
						enum {Lsb = 6};
						enum {FieldMask = 0x000000C0};
						enum {Value_NoAccess = 0x0};
						enum {ValueMask_NoAccess = 0x00000000};
						enum {Value_ClientAccess = 0x1};
						enum {ValueMask_ClientAccess = 0x00000040};
						enum {Value_ManagerFree = 0x3};
						enum {ValueMask_ManagerFree = 0x000000C0};
						};
					namespace GP11 { // Field Description
						enum {Lsb = 8};
						enum {FieldMask = 0x00000300};
						enum {Value_NoAccess = 0x0};
						enum {ValueMask_NoAccess = 0x00000000};
						enum {Value_ClientAccess = 0x1};
						enum {ValueMask_ClientAccess = 0x00000100};
						enum {Value_ManagerFree = 0x3};
						enum {ValueMask_ManagerFree = 0x00000300};
						};
					namespace GP10 { // Field Description
						enum {Lsb = 10};
						enum {FieldMask = 0x00000C00};
						enum {Value_NoAccess = 0x0};
						enum {ValueMask_NoAccess = 0x00000000};
						enum {Value_ClientAccess = 0x1};
						enum {ValueMask_ClientAccess = 0x00000400};
						enum {Value_ManagerFree = 0x3};
						enum {ValueMask_ManagerFree = 0x00000C00};
						};
					namespace GP9 { // Field Description
						enum {Lsb = 12};
						enum {FieldMask = 0x00003000};
						enum {Value_NoAccess = 0x0};
						enum {ValueMask_NoAccess = 0x00000000};
						enum {Value_ClientAccess = 0x1};
						enum {ValueMask_ClientAccess = 0x00001000};
						enum {Value_ManagerFree = 0x3};
						enum {ValueMask_ManagerFree = 0x00003000};
						};
					namespace GP8 { // Field Description
						enum {Lsb = 14};
						enum {FieldMask = 0x0000C000};
						enum {Value_NoAccess = 0x0};
						enum {ValueMask_NoAccess = 0x00000000};
						enum {Value_ClientAccess = 0x1};
						enum {ValueMask_ClientAccess = 0x00004000};
						enum {Value_ManagerFree = 0x3};
						enum {ValueMask_ManagerFree = 0x0000C000};
						};
					namespace GP7 { // Field Description
						enum {Lsb = 16};
						enum {FieldMask = 0x00030000};
						enum {Value_NoAccess = 0x0};
						enum {ValueMask_NoAccess = 0x00000000};
						enum {Value_ClientAccess = 0x1};
						enum {ValueMask_ClientAccess = 0x00010000};
						enum {Value_ManagerFree = 0x3};
						enum {ValueMask_ManagerFree = 0x00030000};
						};
					namespace GP6 { // Field Description
						enum {Lsb = 18};
						enum {FieldMask = 0x000C0000};
						enum {Value_NoAccess = 0x0};
						enum {ValueMask_NoAccess = 0x00000000};
						enum {Value_ClientAccess = 0x1};
						enum {ValueMask_ClientAccess = 0x00040000};
						enum {Value_ManagerFree = 0x3};
						enum {ValueMask_ManagerFree = 0x000C0000};
						};
					namespace GP5 { // Field Description
						enum {Lsb = 20};
						enum {FieldMask = 0x00300000};
						enum {Value_NoAccess = 0x0};
						enum {ValueMask_NoAccess = 0x00000000};
						enum {Value_ClientAccess = 0x1};
						enum {ValueMask_ClientAccess = 0x00100000};
						enum {Value_ManagerFree = 0x3};
						enum {ValueMask_ManagerFree = 0x00300000};
						};
					namespace GP4 { // Field Description
						enum {Lsb = 22};
						enum {FieldMask = 0x00C00000};
						enum {Value_NoAccess = 0x0};
						enum {ValueMask_NoAccess = 0x00000000};
						enum {Value_ClientAccess = 0x1};
						enum {ValueMask_ClientAccess = 0x00400000};
						enum {Value_ManagerFree = 0x3};
						enum {ValueMask_ManagerFree = 0x00C00000};
						};
					namespace GP3 { // Field Description
						enum {Lsb = 24};
						enum {FieldMask = 0x03000000};
						enum {Value_NoAccess = 0x0};
						enum {ValueMask_NoAccess = 0x00000000};
						enum {Value_ClientAccess = 0x1};
						enum {ValueMask_ClientAccess = 0x01000000};
						enum {Value_ManagerFree = 0x3};
						enum {ValueMask_ManagerFree = 0x03000000};
						};
					namespace GP2 { // Field Description
						enum {Lsb = 26};
						enum {FieldMask = 0x0C000000};
						enum {Value_NoAccess = 0x0};
						enum {ValueMask_NoAccess = 0x00000000};
						enum {Value_ClientAccess = 0x1};
						enum {ValueMask_ClientAccess = 0x04000000};
						enum {Value_ManagerFree = 0x3};
						enum {ValueMask_ManagerFree = 0x0C000000};
						};
					namespace GP1 { // Field Description
						enum {Lsb = 28};
						enum {FieldMask = 0x30000000};
						enum {Value_NoAccess = 0x0};
						enum {ValueMask_NoAccess = 0x00000000};
						enum {Value_ClientAccess = 0x1};
						enum {ValueMask_ClientAccess = 0x10000000};
						enum {Value_ManagerFree = 0x3};
						enum {ValueMask_ManagerFree = 0x30000000};
						};
					namespace GP0 { // Field Description
						enum {Lsb = 30};
						enum {FieldMask = 0xC0000000};
						enum {Value_NoAccess = 0x0};
						enum {ValueMask_NoAccess = 0x00000000};
						enum {Value_ClientAccess = 0x1};
						enum {ValueMask_ClientAccess = 0x40000000};
						enum {Value_ManagerFree = 0x3};
						enum {ValueMask_ManagerFree = 0xC0000000};
						};
					};
				};
			};
		};
	};
#endif
