/*
   Copyright (C) 2007 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

/*
 *  This file contains directives for the GNU linker which are specific
 *  to the ADI Coyote board.
 */
OUTPUT_FORMAT("elf32-m32c", "elf32-m32c", "elf32-m32c")
OUTPUT_ARCH(m32c)

/* ENTRY(__m32c_Reset) */
ENTRY(_start)

SECTIONS
{
	/* Read-only sections, merged into text segment: */
	.text : {
      _stext = ABSOLUTE(.);
		*(.interp)
		*(.hash)
		*(.dynsym)
		*(.dynstr)
		*(.gnu.version)
		*(.gnu.version_d)
		*(.gnu.version_r)
		*(.rel.init)
		*(.rela.init)
		*(.rel.text .rel.text.* .rel.gnu.linkonce.t.*)
		*(.rela.text .rela.text.* .rela.gnu.linkonce.t.*)
		*(.rel.fini)
		*(.rela.fini)
		*(.rel.rodata .rel.rodata.* .rel.gnu.linkonce.r.*)
		*(.rela.rodata .rela.rodata.* .rela.gnu.linkonce.r.*)
		*(.rel.data .rel.data.* .rel.gnu.linkonce.d.*)
		*(.rela.data .rela.data.* .rela.gnu.linkonce.d.*)
		*(.gnu.linkonce.d.*)
		*(.rel.tdata .rel.tdata.* .rel.gnu.linkonce.td.*)
		*(.rela.tdata .rela.tdata.* .rela.gnu.linkonce.td.*)
		*(.rel.ctors)
		*(.rela.ctors)
		*(.rel.dtors)
		*(.rela.dtors)
		*(.rel.dyn)
      *(.glue_7)
      *(.glue_7t)
    	*(.text .stub .text.* .gnu.linkonce.t.*)
		KEEP (*(.init))
		/* .gnu.warning sections are handled specially by elf32.em.  */
    	*(.gnu.warning)
		KEEP (*(.fini))
		*(.rodata .rodata.* .gnu.linkonce.r.*)
		*(.rodata1)
		/* Adjust the address for the data segment.  We want to adjust up to
			the same address within the page on the next page up.  */
		. = ALIGN(2) + (. & (2 - 1));
		/* Ensure the __preinit_array_start label is properly aligned.  We
			could instead move the label definition inside the section, but
			the linker would then create the section even if it turns out to
			be empty, which isn't pretty.  */
		. = ALIGN(32 / 8);
		PROVIDE (__preinit_array_start = .);
		*(.preinit_array)
		PROVIDE (__preinit_array_end = .);
		PROVIDE (__init_array_start = .);
		*(.init_array)
		PROVIDE (__init_array_end = .);
		PROVIDE (__fini_array_start = .);
		*(.fini_array)
		PROVIDE (__fini_array_end = .);
		PROVIDE (__etext = .);
		PROVIDE (_etext = .);
		PROVIDE (etext = .);
		} > ram
	.ctors : {
		/* gcc uses crtbegin.o to find the start of
			the constructors, so we make sure it is
			first.  Because this is a wildcard, it
			doesn't matter if the user does not
			actually link against crtbegin.o; the
			linker won't look for a file to match a
			wildcard.  The wildcard also means that it
			doesn't matter which directory crtbegin.o
			is in.  */
		KEEP (*crtbegin.o(.ctors))
		/* We don't want to include the .ctor section from
			from the crtend.o file until after the sorted ctors.
			The .ctor section from the crtend file contains the
			end of ctors marker and it must be last */
		KEEP (*(EXCLUDE_FILE (*crtend.o ) .ctors))
		KEEP (*(SORT(.ctors.*)))
		KEEP (*(.ctors))
		} > ram

	.dtors : {
		KEEP (*crtbegin.o(.dtors))
		KEEP (*(EXCLUDE_FILE (*crtend.o ) .dtors))
		KEEP (*(SORT(.dtors.*)))
		KEEP (*(.dtors))
		} > ram

	.jcr : {
		KEEP (*(.jcr))
		LONG(1)	/* This ensures that .jcr is never empty */
		} > ram

	.data : AT(ADDR(.jcr) + SIZEOF(.jcr)) {
		PROVIDE( _dataStart = . );
		*(.rel.sdata .rel.sdata.* .rel.gnu.linkonce.s.*)
		*(.rela.sdata .rela.sdata.* .rela.gnu.linkonce.s.*)
		*(.rel.sdata2 .rel.sdata2.* .rel.gnu.linkonce.s2.*)
		*(.rela.sdata2 .rela.sdata2.* .rela.gnu.linkonce.s2.*)
		*(.rel.plt)
		*(.rela.plt)
		*(.plt)
		*(.sdata2 .sdata2.* .gnu.linkonce.s2.*)
		*(.eh_frame_hdr)
		*(.data .data.*)
		SORT(CONSTRUCTORS)
		*(.data1)
		*(.tdata .tdata.* .gnu.linkonce.td.*)
		KEEP (*(.eh_frame))
		*(.gcc_except_table)
		*(.dynamic)
		*(.sdata .sdata.* .gnu.linkonce.s.*)
      . = ALIGN (4);
      KEEP(*( SORT (.ecos.table.*))) ; 
		_edata = .;
		PROVIDE (edata = .);
		} > ram

	PROVIDE( ___text_data_start = ADDR(.jcr) + SIZEOF(.jcr) );
	PROVIDE( ___data_start = ADDR(.data));
	PROVIDE( ___data_end = ADDR(.data) + SIZEOF(.data) );
	PROVIDE( idata_end = ADDR(.jcr) + SIZEOF(.jcr) + SIZEOF(.data) );

	.bss (NOLOAD) : {
		__bss_start = .;
		__bss_start__ = .;
		__SBSS_START__ = .;
		*(.rel.tbss .rel.tbss.* .rel.gnu.linkonce.tb.*)
		*(.rela.tbss .rela.tbss.* .rela.gnu.linkonce.tb.*)
		*(.rel.sbss .rel.sbss.* .rel.gnu.linkonce.sb.*)
		*(.rela.sbss .rela.sbss.* .rela.gnu.linkonce.sb.*)
		*(.rel.sbss2 .rel.sbss2.* .rel.gnu.linkonce.sb2.*)
		*(.rela.sbss2 .rela.sbss2.* .rela.gnu.linkonce.sb2.*)
		*(.rel.bss .rel.bss.* .rel.gnu.linkonce.b.*)
		*(.rela.bss .rela.bss.* .rela.gnu.linkonce.b.*)
		*(.sbss2 .sbss2.* .gnu.linkonce.sb2.*)
		*(.tbss .tbss.* .gnu.linkonce.tb.*) *(.tcommon)
		PROVIDE (__sbss_start = .);
		PROVIDE (___sbss_start = .);
		*(.dynsbss)
		*(.sbss .sbss.* .gnu.linkonce.sb.*)
		*(.scommon)
		PROVIDE (__sbss_end = .);
		PROVIDE (___sbss_end = .);
		*(.dynbss)
		*(.bss .bss.* .gnu.linkonce.b.*)
		*(COMMON)
		/* Align here to ensure that the .bss section occupies space up to
			_end.  Align after .bss to ensure correct alignment even if the
			.bss section disappears because there are no input sections.  */
		. = ALIGN(32 / 8);
		__SBSS_END__   = .;
		__bss_end__ = .;
		__heap1 = ALIGN (0x8);
		_end = .;
		PROVIDE (end = .);
		} > ram

	.sysglobal ALIGN(0x1000) (NOLOAD) : {
		LONG(0)
		} > ram
	PROVIDE(KruxGlobalSystemPage = ADDR(.sysglobal) );

	.sysstack ALIGN(0x1000) (NOLOAD) : {
		LONG(0)
		} > ram
	PROVIDE(__SUPER_STACK_PAGE__ = ADDR(.sysstack) );

	.freepages ALIGN(0x1000) (NOLOAD) : {
		LONG(0)
		} > ram
	PROVIDE(__SUPER_STACK_END__ = ADDR(.freepages) );
	PROVIDE(__FREE_PAGE_START__ = ADDR(.freepages) );

	/* Stabs debugging sections.  */
	.stab          0 : { *(.stab) }
	.stabstr       0 : { *(.stabstr) }
	.stab.excl     0 : { *(.stab.excl) }
	.stab.exclstr  0 : { *(.stab.exclstr) }
	.stab.index    0 : { *(.stab.index) }
	.stab.indexstr 0 : { *(.stab.indexstr) }
	.comment       0 : { *(.comment) }
	/* DWARF debug sections.
		Symbols in the DWARF debugging sections are relative to the beginning
		of the section so we begin them at 0.  */
	/* DWARF 1 */
	.debug          0 : { *(.debug) }
	.line           0 : { *(.line) }
	/* GNU DWARF 1 extensions */
	.debug_srcinfo  0 : { *(.debug_srcinfo) }
	.debug_sfnames  0 : { *(.debug_sfnames) }
	/* DWARF 1.1 and DWARF 2 */
	.debug_aranges  0 : { *(.debug_aranges) }
	.debug_pubnames 0 : { *(.debug_pubnames) }
	/* DWARF 2 */
	.debug_info     0 : { *(.debug_info .gnu.linkonce.wi.*) }
	.debug_abbrev   0 : { *(.debug_abbrev) }
	.debug_line     0 : { *(.debug_line) }
	.debug_frame    0 : { *(.debug_frame) }
	.debug_str      0 : { *(.debug_str) }
	.debug_loc      0 : { *(.debug_loc) }
	.debug_macinfo  0 : { *(.debug_macinfo) }
	/* SGI/MIPS DWARF 2 extensions */
	.debug_weaknames 0 : { *(.debug_weaknames) }
	.debug_funcnames 0 : { *(.debug_funcnames) }
	.debug_typenames 0 : { *(.debug_typenames) }
	.debug_varnames  0 : { *(.debug_varnames) }
}
