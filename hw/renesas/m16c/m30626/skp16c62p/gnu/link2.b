#
# The project overrides.b should define:
#
#FINAL_LINK_FLAGS=   \
#	-T $(srcroot)/src/oscl/hw/est/mdp8xx/mem8Mram8MflashDebug.map \
#	$(BSP_LINK_FLAGS)
#
# The first "-T" argument defines the size of RAM and Flash, and
# may vary with the implementation.
#

BSP_LINK_FLAGS=	\
	-T $(srcroot)/src/oscl/hw/renesas/m16c/m30626/skp16c62p/gnu/link.map \
	-T $(srcroot)/tools/benvc/Linux/m32c-elf/m32c-elf/lib/ldscripts/elf32m32c.x

