#ifndef _hw_ti_tnetv105x_uart_consoleh_
#define _hw_ti_tnetv105x_uart_consoleh_
#include <stdint.h>
namespace Oscl { // Namespace description
	namespace HW { // Namespace description
		namespace TI { // Namespace description
			namespace TNETV105X { // Namespace description
				namespace UART { // Namespace description
					namespace RBR_THR_DLL { // Register description
						typedef uint32_t	Reg;
						namespace RBR_THR_DLL { // Field Description
							enum {Lsb = 0};
							enum {FieldMask = 0x000000FF};
							};
						};
					namespace IER_DLH { // Register description
						typedef uint32_t	Reg;
						namespace IER_DLH { // Field Description
							enum {Lsb = 0};
							enum {FieldMask = 0x000000FF};
							};
						};
					namespace IIR_FCR { // Register description
						typedef uint32_t	Reg;
						namespace IIR_FCR { // Field Description
							enum {Lsb = 0};
							enum {FieldMask = 0x000000FF};
							};
						};
					namespace LCR { // Register description
						typedef uint32_t	Reg;
						namespace LCR { // Field Description
							enum {Lsb = 0};
							enum {FieldMask = 0x000000FF};
							};
						};
					namespace MCR { // Register description
						typedef uint32_t	Reg;
						namespace MCR { // Field Description
							enum {Lsb = 0};
							enum {FieldMask = 0x000000FF};
							};
						};
					namespace LSR { // Register description
						typedef uint32_t	Reg;
						namespace LSR { // Field Description
							enum {Lsb = 0};
							enum {FieldMask = 0x000000FF};
							};
						};
					namespace MSR { // Register description
						typedef uint32_t	Reg;
						namespace MSR { // Field Description
							enum {Lsb = 0};
							enum {FieldMask = 0x000000FF};
							};
						};
					namespace SPR { // Register description
						typedef uint32_t	Reg;
						namespace SPR { // Field Description
							enum {Lsb = 0};
							enum {FieldMask = 0x000000FF};
							};
						};
					};
				};
			};
		};
	};
#endif
