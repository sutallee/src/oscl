/*
   Copyright (C) 2005 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/
#ifndef _oscl_hw_ti_tnetv105x_pcicsrh_
#define _oscl_hw_ti_tnetv105x_pcicsrh_
#include "pcidma.h"

/** */
namespace Oscl {
/** */
namespace TNETV105X {
/** */
namespace PCI {
/** */
namespace CSR {

/**	This is the structure of an TNETV105X PCI Control/Status Registers
	Starts at 0xC0000000
 */
struct PciCsr {
	// 0xC0000000
	PCI_NP_AD::Reg			pci_np_ad;
	// 0xC0000004
	PCI_NP_CBE::Reg			pci_np_cbe;
	// 0xC0000008
	PCI_NP_WDATA::Reg		pci_np_wdata;
	// 0xC000000C
	PCI_NP_RDATA::Reg		pci_np_rdata;
	// 0xC0000010
	PCI_CRP_AD_CBE::Reg		pci_crp_ad_cbe;
	// 0xC0000014
	PCI_CRP_WDATA::Reg		pci_crp_wdata;
	// 0xC0000018
	PCI_CRP_RDATA::Reg		pci_crp_rdata;
	// 0xC000001C
	PCI_CSR::Reg			pci_csr;
	// 0xC0000020
	PCI_ISR::Reg			pci_isr;
	// 0xC0000024
	PCI_INTEN::Reg			pci_inten;
	// 0xC0000028
	PCI_DMACTRL::Reg		pci_dmactrl;
	// 0xC000002C
	PCI_AHBMEMBASE::Reg		pci_ahbmembase;
	// 0xC0000030
	PCI_AHBIOBASE::Reg		pci_ahbiobase;
	// 0xC0000034
	PCI_PCIMEMBASE::Reg		pci_pcimembase;
	// 0xC0000038
	PCI_AHBDOORBELL::Reg	pci_ahbdoorbell;
	// 0xC000003C
	PCI_PCIDOORBELL::Reg	pci_pcidoorbell;
	// 0xC0000040
	PciDmaController		pci_atpdma0;
	// 0xC000004C
	PciDmaController		pci_atpdma1;
	// 0xC0000058
	PciDmaController		pci_ptadma0;
	// 0xC0000064
	PciDmaController		pci_ptadma1;
	};

}
}
}
}

#endif

