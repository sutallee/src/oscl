/*
   Copyright (C) 2007 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_hw_renesas_m16c_m30626_maph_
#define _oscl_hw_renesas_m16c_m30626_maph_

/** */
namespace Oscl {
/** */
namespace HW {
/** */
namespace RENESAS {
/** */
namespace M16C {
/** */
namespace M30626 {

/** 0xB8610000 */
struct Map {
	/** */
	uint32_t						reserved0[((0x0900-0x0000))/sizeof(uint32_t)];
	/** */
//	Oscl::HW::RENESAS::M16C::M30626::SYS_GPIO::Map	sys_gpio;	// offset: 0x0900 - 0x92F
	};

}
}
}
}
}

#endif
