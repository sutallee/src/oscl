/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

/* Linker script for m68k-elf on sim68k */

SECTIONS
{
	/* Read-only sections, merged into text segment: */
	.text : {
    	*(.text .stub .text.* .gnu.linkonce.t.*)
		*(.rodata .rodata.* .gnu.linkonce.r.*)
		} > rom
	.init : {
		. = ALIGN(32 / 8);
		___init = .;
		KEEP (*(.init))
		LONG(0x4E754E75);	/* RTS */
		} > rom
	.fini : {
		KEEP (*(.fini))
		LONG(0x4E754E75);	/* RTS */
		} > rom
	.ctors : {
		/* gcc uses crtbegin.o to find the start of
			the constructors, so we make sure it is
			first.  Because this is a wildcard, it
			doesn't matter if the user does not
			actually link against crtbegin.o; the
			linker won't look for a file to match a
			wildcard.  The wildcard also means that it
			doesn't matter which directory crtbegin.o
			is in.  */
		KEEP (*crtbegin.o(.ctors))
		/* We don't want to include the .ctor section from
			from the crtend.o file until after the sorted ctors.
			The .ctor section from the crtend file contains the
			end of ctors marker and it must be last */
		KEEP (*(EXCLUDE_FILE (*crtend.o ) .ctors))
		KEEP (*(SORT(.ctors.*)))
		SORT(CONSTRUCTORS)
		KEEP (*(.ctors))
		} > rom

	.dtors : {
		/*  FIXME: Since nothing in this section is ever
			executed (in an embedded system),
			it would be nice to keep it out of
			memory.
		 */
		KEEP (*crtbegin.o(.dtors))
		KEEP (*(EXCLUDE_FILE (*crtend.o ) .dtors))
		KEEP (*(SORT(.dtors.*)))
		KEEP (*(.dtors))
		} > rom

	.jcr : {
		KEEP (*(.jcr))
		} > rom

	.idatacopy : {
		/* This dummy section represents the place
			where initialized data is located in
			the ROM/text segment. The dummy long
			ensures that the section is never
			empty.
		 */
		LONG(0);
		} > rom

	.data : AT(ADDR(.idatacopy) + SIZEOF(.idatacopy)){
		*(.data .data.*)
		KEEP (*(.eh_frame))
		} > ram

	PROVIDE( ___text_data_start = ADDR(.idatacopy) + SIZEOF(.idatacopy) );
	PROVIDE( ___data_start = ADDR(.data));
	PROVIDE( ___data_end = ADDR(.data) + SIZEOF(.data) );
	PROVIDE( idata_end = ADDR(.idatacopy) + SIZEOF(.idatacopy) + SIZEOF(.data) );

	.bss (NOLOAD) : {
		__bss_start = .;
		*(.bss .bss.* .gnu.linkonce.b.*)
		/* Align here to ensure that the .bss section occupies space up to
			_end.  Align after .bss to ensure correct alignment even if the
			.bss section disappears because there are no input sections.  */
		. = ALIGN(32 / 8);
		. = ALIGN(32 / 8);
		_end = .;
		PROVIDE (end = .);
     	__bss_end = .;
		} > ram

	/* Stabs debugging sections.  */
	.stab          0 : { *(.stab) }
	.stabstr       0 : { *(.stabstr) }
	.stab.excl     0 : { *(.stab.excl) }
	.stab.exclstr  0 : { *(.stab.exclstr) }
	.stab.index    0 : { *(.stab.index) }
	.stab.indexstr 0 : { *(.stab.indexstr) }
	.comment       0 : { *(.comment) }
	/* DWARF debug sections.
		Symbols in the DWARF debugging sections are relative to the beginning
		of the section so we begin them at 0.  */
	/* DWARF 1 */
	.debug          0 : { *(.debug) }
	.line           0 : { *(.line) }
	/* GNU DWARF 1 extensions */
	.debug_srcinfo  0 : { *(.debug_srcinfo) }
	.debug_sfnames  0 : { *(.debug_sfnames) }
	/* DWARF 1.1 and DWARF 2 */
	.debug_aranges  0 : { *(.debug_aranges) }
	.debug_pubnames 0 : { *(.debug_pubnames) }
	/* DWARF 2 */
	.debug_info     0 : { *(.debug_info .gnu.linkonce.wi.*) }
	.debug_abbrev   0 : { *(.debug_abbrev) }
	.debug_line     0 : { *(.debug_line) }
	.debug_frame    0 : { *(.debug_frame) }
	.debug_str      0 : { *(.debug_str) }
	.debug_loc      0 : { *(.debug_loc) }
	.debug_macinfo  0 : { *(.debug_macinfo) }
	/* SGI/MIPS DWARF 2 extensions */
	.debug_weaknames 0 : { *(.debug_weaknames) }
	.debug_funcnames 0 : { *(.debug_funcnames) }
	.debug_typenames 0 : { *(.debug_typenames) }
	.debug_varnames  0 : { *(.debug_varnames) }
	}

