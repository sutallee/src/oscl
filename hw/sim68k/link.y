/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

/* Default linker script, for normal executables */
OUTPUT_FORMAT("elf32-m68k")

PROVIDE(_stack = 0xFBC0);
							/* FIXME: The stack should not consume the
							 * special abs8 ("eightbit_data") space from
							 * 0xFF00-0xFFFF. Actually peripherals are
							 * from 0xFF80-0xFFFF and thus only the range
							 * from 0xFF00 to 0xFF7F is available for the
							 * "eightbit_data".
							 */
ENTRY(__h8300hReset)
/* The memory size is 256KB to coincide with the simulator.
   Don't change either without considering the other.  */
MEMORY {
	vectors		: o = 0x0000, l = 0xc4
	flash		: o = 0x0100, l = 0x3fefc
    stack		: o = 0xfb80, l = 0x40
    stackram	: o = 0xfbc0, l = 0x04
	ram			: o = 0xfbc4, l = 0x033C
	fast		: o = 0xff00, l = 0x0080	/* This range is reserved for
											 * variables with the
											 * "eightbit_data" attribute.
										 	 */
	}

SECTIONS
{
	.vectors : {
	  /* Use something like this to place a specific
	     function's address into the vector table.
	     LONG (ABSOLUTE (_foobar)).  */
	  *(.vectors)
	SHORT(ABSOLUTE(__h8300hReset))
	SHORT(ABSOLUTE(__h8300hReserved1))
	SHORT(ABSOLUTE(__h8300hReserved2))
	SHORT(ABSOLUTE(__h8300hReserved3))
	SHORT(ABSOLUTE(__h8300hReserved4))
	SHORT(ABSOLUTE(__h8300hReserved5))
	SHORT(ABSOLUTE(__h8300hReserved6))
	SHORT(ABSOLUTE(__h8300hNMI))
	SHORT(ABSOLUTE(__h8300hTrap0))
	SHORT(ABSOLUTE(__h8300hTrap1))
	SHORT(ABSOLUTE(__h8300hTrap2))
	SHORT(ABSOLUTE(__h8300hTrap3))
	SHORT(ABSOLUTE(__h8300hAddressBreak))
	SHORT(ABSOLUTE(__h8300hCpuSleep))
	SHORT(ABSOLUTE(__h8300hIrq0))
	SHORT(ABSOLUTE(__h8300hIrq1))
	SHORT(ABSOLUTE(__h8300hIrq2))
	SHORT(ABSOLUTE(__h8300hIrq3))
	SHORT(ABSOLUTE(__h8300hWKP))
	SHORT(ABSOLUTE(__h8300hTimerAOverflow))
	SHORT(ABSOLUTE(__h8300hReserved20))
	SHORT(ABSOLUTE(__h8300hTimerW))
	SHORT(ABSOLUTE(__h8300hTimerV))
	SHORT(ABSOLUTE(__h8300hSCI3))
	SHORT(ABSOLUTE(__h8300hIIC))
	SHORT(ABSOLUTE(__h8300hADC))
	} > vectors
	/* Read-only sections, merged into text segment: */
	.text : {
		*(.interp)
		*(.hash)
		*(.dynsym)
		*(.dynstr)
		*(.gnu.version)
		*(.gnu.version_d)
		*(.gnu.version_r)
		*(.rel.init)
		*(.rela.init)
		*(.rel.text .rel.text.* .rel.gnu.linkonce.t.*)
		*(.rela.text .rela.text.* .rela.gnu.linkonce.t.*)
		*(.rel.fini)
		*(.rela.fini)
		*(.rel.rodata .rel.rodata.* .rel.gnu.linkonce.r.*)
		*(.rela.rodata .rela.rodata.* .rela.gnu.linkonce.r.*)
		*(.rel.data .rel.data.* .rel.gnu.linkonce.d.*)
		*(.rela.data .rela.data.* .rela.gnu.linkonce.d.*)
		*(.gnu.linkonce.d.*)
		*(.rel.tdata .rel.tdata.* .rel.gnu.linkonce.td.*)
		*(.rela.tdata .rela.tdata.* .rela.gnu.linkonce.td.*)
		*(.rel.ctors)
		*(.rela.ctors)
		*(.rel.dtors)
		*(.rela.dtors)
		KEEP (*(.init))
    	*(.text .stub .text.* .gnu.linkonce.t.*)
		/* .gnu.warning sections are handled specially by elf32.em.  */
    	*(.gnu.warning)
		KEEP (*(.fini))
		*(.rodata .rodata.* .gnu.linkonce.r.*)
		*(.rodata1)
		/* Adjust the address for the data segment.  We want to adjust up to
			the same address within the page on the next page up.  */
		. = ALIGN(2) + (. & (2 - 1));
		/* Ensure the __preinit_array_start label is properly aligned.  We
			could instead move the label definition inside the section, but
			the linker would then create the section even if it turns out to
			be empty, which isn't pretty.  */
		. = ALIGN(32 / 8);
		PROVIDE (__preinit_array_start = .);
		*(.preinit_array)
		PROVIDE (__preinit_array_end = .);
		PROVIDE (__init_array_start = .);
		*(.init_array)
		PROVIDE (__init_array_end = .);
		PROVIDE (__fini_array_start = .);
		*(.fini_array)
		PROVIDE (__fini_array_end = .);
		PROVIDE (__etext = .);
		PROVIDE (_etext = .);
		PROVIDE (etext = .);
		} > flash
	.ctors : {
		/* gcc uses crtbegin.o to find the start of
			the constructors, so we make sure it is
			first.  Because this is a wildcard, it
			doesn't matter if the user does not
			actually link against crtbegin.o; the
			linker won't look for a file to match a
			wildcard.  The wildcard also means that it
			doesn't matter which directory crtbegin.o
			is in.  */
		KEEP (*crtbegin.o(.ctors))
		/* We don't want to include the .ctor section from
			from the crtend.o file until after the sorted ctors.
			The .ctor section from the crtend file contains the
			end of ctors marker and it must be last */
		KEEP (*(EXCLUDE_FILE (*crtend.o ) .ctors))
		KEEP (*(SORT(.ctors.*)))
		KEEP (*(.ctors))
		} > flash

	.dtors : {
		KEEP (*crtbegin.o(.dtors))
		KEEP (*(EXCLUDE_FILE (*crtend.o ) .dtors))
		KEEP (*(SORT(.dtors.*)))
		KEEP (*(.dtors))
		} > flash
	.jcr : {
		KEEP (*(.jcr))
		} > flash
	.data : AT(ADDR(.jcr) + SIZEOF(.jcr)) {
		*(.rel.sdata .rel.sdata.* .rel.gnu.linkonce.s.*)
		*(.rela.sdata .rela.sdata.* .rela.gnu.linkonce.s.*)
		*(.rel.sdata2 .rel.sdata2.* .rel.gnu.linkonce.s2.*)
		*(.rela.sdata2 .rela.sdata2.* .rela.gnu.linkonce.s2.*)
		*(.rel.plt)
		*(.rela.plt)
		*(.plt)
		*(.sdata2 .sdata2.* .gnu.linkonce.s2.*)
		*(.eh_frame_hdr)
		*(.data .data.*)
		SORT(CONSTRUCTORS)
		*(.data1)
		*(.tdata .tdata.* .gnu.linkonce.td.*)
		KEEP (*(.eh_frame))
		*(.gcc_except_table)
		*(.dynamic)
		*(.sdata .sdata.* .gnu.linkonce.s.*)
		_edata = .;
		PROVIDE (edata = .);
		} > ram
	PROVIDE( ___text_data_start = ADDR(.jcr) + SIZEOF(.jcr) );
	PROVIDE( ___data_start = ADDR(.data));
	PROVIDE( ___data_end = ADDR(.data) + SIZEOF(.data) );
	PROVIDE( idata_end = ADDR(.jcr) + SIZEOF(.jcr) + SIZEOF(.data) );
	.bss (NOLOAD) : {
		__bss_start = .;
		*(.rel.tbss .rel.tbss.* .rel.gnu.linkonce.tb.*)
		*(.rela.tbss .rela.tbss.* .rela.gnu.linkonce.tb.*)
		*(.rel.sbss .rel.sbss.* .rel.gnu.linkonce.sb.*)
		*(.rela.sbss .rela.sbss.* .rela.gnu.linkonce.sb.*)
		*(.rel.sbss2 .rel.sbss2.* .rel.gnu.linkonce.sb2.*)
		*(.rela.sbss2 .rela.sbss2.* .rela.gnu.linkonce.sb2.*)
		*(.rel.bss .rel.bss.* .rel.gnu.linkonce.b.*)
		*(.rela.bss .rela.bss.* .rela.gnu.linkonce.b.*)
		*(.sbss2 .sbss2.* .gnu.linkonce.sb2.*)
		*(.tbss .tbss.* .gnu.linkonce.tb.*) *(.tcommon)
		PROVIDE (__sbss_start = .);
		PROVIDE (___sbss_start = .);
		*(.dynsbss)
		*(.sbss .sbss.* .gnu.linkonce.sb.*)
		*(.scommon)
		PROVIDE (__sbss_end = .);
		PROVIDE (___sbss_end = .);
		*(.dynbss)
		*(.bss .bss.* .gnu.linkonce.b.*)
		*(COMMON)
		/* Align here to ensure that the .bss section occupies space up to
			_end.  Align after .bss to ensure correct alignment even if the
			.bss section disappears because there are no input sections.  */
		. = ALIGN(32 / 8);
		. = ALIGN(32 / 8);
		_end = .;
		PROVIDE (end = .);
		} > ram
	/* Stabs debugging sections.  */
	.stab          0 : { *(.stab) }
	.stabstr       0 : { *(.stabstr) }
	.stab.excl     0 : { *(.stab.excl) }
	.stab.exclstr  0 : { *(.stab.exclstr) }
	.stab.index    0 : { *(.stab.index) }
	.stab.indexstr 0 : { *(.stab.indexstr) }
	.comment       0 : { *(.comment) }
	/* DWARF debug sections.
		Symbols in the DWARF debugging sections are relative to the beginning
		of the section so we begin them at 0.  */
	/* DWARF 1 */
	.debug          0 : { *(.debug) }
	.line           0 : { *(.line) }
	/* GNU DWARF 1 extensions */
	.debug_srcinfo  0 : { *(.debug_srcinfo) }
	.debug_sfnames  0 : { *(.debug_sfnames) }
	/* DWARF 1.1 and DWARF 2 */
	.debug_aranges  0 : { *(.debug_aranges) }
	.debug_pubnames 0 : { *(.debug_pubnames) }
	/* DWARF 2 */
	.debug_info     0 : { *(.debug_info .gnu.linkonce.wi.*) }
	.debug_abbrev   0 : { *(.debug_abbrev) }
	.debug_line     0 : { *(.debug_line) }
	.debug_frame    0 : { *(.debug_frame) }
	.debug_str      0 : { *(.debug_str) }
	.debug_loc      0 : { *(.debug_loc) }
	.debug_macinfo  0 : { *(.debug_macinfo) }
	/* SGI/MIPS DWARF 2 extensions */
	.debug_weaknames 0 : { *(.debug_weaknames) }
	.debug_funcnames 0 : { *(.debug_funcnames) }
	.debug_typenames 0 : { *(.debug_typenames) }
	.debug_varnames  0 : { *(.debug_varnames) }
	.eight : AT(ADDR(.jcr) + SIZEOF(.jcr) + SIZEOF(.data)) {
		*(.eight)
		} > fast
	PROVIDE( ___text_eight_start = ADDR(.jcr) + SIZEOF(.jcr) +SIZEOF(.data) );
	PROVIDE( ___eight_start = DEFINED(.eight)?ADDR(.eight):0);
	PROVIDE( ___eight_end = DEFINED(.eight)?ADDR(.eight)+SIZEOF(.eight):0  );
	.stack (NOLOAD) : {
		*(.stack)
		}  > stackram
}
