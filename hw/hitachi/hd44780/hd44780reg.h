#ifndef _hw_hitachi_hd44780regh_
#define _hw_hitachi_hd44780regh_
#include <stdint.h>
namespace Oscl { // Namespace description
	namespace HW { // Namespace description
		namespace Hitachi { // Namespace description
			namespace HD44780 { // Namespace description
				namespace Command { // Register description
					typedef uint8_t	Reg;
					enum {DisplayClear = 1};
					enum {CursorHome = 2};
					namespace EntryMode { // Field Description
						enum {Lsb = 0};
						enum {FieldMask = 0x000000FF};
						namespace Opcode { // Field Description
							enum {Lsb = 2};
							enum {FieldMask = 0x000000FC};
							enum {Value_Opcode = 0x1};
							enum {ValueMask_Opcode = 0x00000004};
							};
						namespace ID { // Field Description
							enum {Lsb = 1};
							enum {FieldMask = 0x00000002};
							enum {Value_Increment = 0x1};
							enum {ValueMask_Increment = 0x00000002};
							enum {Value_Decrement = 0x0};
							enum {ValueMask_Decrement = 0x00000000};
							};
						namespace S { // Field Description
							enum {Lsb = 0};
							enum {FieldMask = 0x00000001};
							enum {Value_ShiftOn = 0x1};
							enum {ValueMask_ShiftOn = 0x00000001};
							enum {Value_ShiftOff = 0x0};
							enum {ValueMask_ShiftOff = 0x00000000};
							};
						};
					namespace DisplayOnOff { // Field Description
						enum {Lsb = 0};
						enum {FieldMask = 0x000000FF};
						namespace Opcode { // Field Description
							enum {Lsb = 3};
							enum {FieldMask = 0x000000F8};
							enum {Value_Opcode = 0x1};
							enum {ValueMask_Opcode = 0x00000008};
							};
						namespace D { // Field Description
							enum {Lsb = 2};
							enum {FieldMask = 0x00000004};
							enum {Value_DisplayOn = 0x1};
							enum {ValueMask_DisplayOn = 0x00000004};
							enum {Value_DisplayOff = 0x0};
							enum {ValueMask_DisplayOff = 0x00000000};
							};
						namespace C { // Field Description
							enum {Lsb = 1};
							enum {FieldMask = 0x00000002};
							enum {Value_CursorOn = 0x1};
							enum {ValueMask_CursorOn = 0x00000002};
							enum {Value_CursorOff = 0x0};
							enum {ValueMask_CursorOff = 0x00000000};
							};
						namespace B { // Field Description
							enum {Lsb = 0};
							enum {FieldMask = 0x00000001};
							enum {Value_CursorBlinkOn = 0x1};
							enum {ValueMask_CursorBlinkOn = 0x00000001};
							enum {Value_CursorBlinkOff = 0x0};
							enum {ValueMask_CursorBlinkOff = 0x00000000};
							};
						};
					namespace CursorShift { // Field Description
						enum {Lsb = 0};
						enum {FieldMask = 0x000000FF};
						namespace Opcode { // Field Description
							enum {Lsb = 4};
							enum {FieldMask = 0x000000F0};
							enum {Value_Opcode = 0x1};
							enum {ValueMask_Opcode = 0x00000010};
							};
						namespace SC { // Field Description
							enum {Lsb = 3};
							enum {FieldMask = 0x00000008};
							enum {Value_ShiftDisplay = 0x1};
							enum {ValueMask_ShiftDisplay = 0x00000008};
							enum {Value_DontShiftDisplay = 0x0};
							enum {ValueMask_DontShiftDisplay = 0x00000000};
							};
						namespace RL { // Field Description
							enum {Lsb = 2};
							enum {FieldMask = 0x00000004};
							enum {Value_ShiftRight = 0x1};
							enum {ValueMask_ShiftRight = 0x00000004};
							enum {Value_ShiftLeft = 0x0};
							enum {ValueMask_ShiftLeft = 0x00000000};
							};
						};
					namespace FunctionSet { // Field Description
						enum {Lsb = 0};
						enum {FieldMask = 0x000000FF};
						namespace Opcode { // Field Description
							enum {Lsb = 5};
							enum {FieldMask = 0x000000E0};
							enum {Value_Opcode = 0x1};
							enum {ValueMask_Opcode = 0x00000020};
							};
						namespace DL { // Field Description
							enum {Lsb = 4};
							enum {FieldMask = 0x00000010};
							enum {Value_EightBit = 0x1};
							enum {ValueMask_EightBit = 0x00000010};
							enum {Value_FourBit = 0x0};
							enum {ValueMask_FourBit = 0x00000000};
							};
						namespace N { // Field Description
							enum {Lsb = 3};
							enum {FieldMask = 0x00000008};
							enum {Value_DualLine = 0x1};
							enum {ValueMask_DualLine = 0x00000008};
							enum {Value_SingleLine = 0x0};
							enum {ValueMask_SingleLine = 0x00000000};
							};
						namespace F { // Field Description
							enum {Lsb = 2};
							enum {FieldMask = 0x00000004};
							enum {Value_Dots5x10 = 0x1};
							enum {ValueMask_Dots5x10 = 0x00000004};
							enum {Value_Dots5x8 = 0x0};
							enum {ValueMask_Dots5x8 = 0x00000000};
							};
						};
					namespace CgRamAddressSet { // Field Description
						enum {Lsb = 0};
						enum {FieldMask = 0x000000FF};
						namespace Opcode { // Field Description
							enum {Lsb = 6};
							enum {FieldMask = 0x000000C0};
							enum {Value_Opcode = 0x1};
							enum {ValueMask_Opcode = 0x00000040};
							};
						namespace ACG { // Field Description
							enum {Lsb = 0};
							enum {FieldMask = 0x0000003F};
							};
						};
					namespace DdRamAddressSet { // Field Description
						enum {Lsb = 0};
						enum {FieldMask = 0x000000FF};
						namespace Opcode { // Field Description
							enum {Lsb = 7};
							enum {FieldMask = 0x00000080};
							enum {Value_Opcode = 0x1};
							enum {ValueMask_Opcode = 0x00000080};
							};
						namespace ADD { // Field Description
							enum {Lsb = 0};
							enum {FieldMask = 0x0000007F};
							};
						};
					};
				namespace Status { // Register description
					typedef uint8_t	Reg;
					namespace BusyFlag { // Field Description
						enum {Lsb = 7};
						enum {FieldMask = 0x00000080};
						enum {Value_Busy = 0x1};
						enum {ValueMask_Busy = 0x00000080};
						enum {Value_Ready = 0x0};
						enum {ValueMask_Ready = 0x00000000};
						};
					namespace AddressCounter { // Field Description
						enum {Lsb = 0};
						enum {FieldMask = 0x0000007F};
						};
					};
				};
			};
		};
	};
#endif
