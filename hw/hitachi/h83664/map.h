/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_hw_hitachi_h83664_maph_
#define _oscl_hw_hitachi_h83664_maph_
#include <stdint.h>

namespace Oscl{
namespace H83664 {

struct TimerA {
	uint8_t		tma;	// FFA6
	uint8_t		tca;	// FFA7
	};

struct TimerW {
	uint8_t			tmrw;
	uint8_t			tcrw;
	uint8_t			tierw;
	volatile uint8_t	tsrw;
	uint8_t			tior0;
	uint8_t			tior1;
	uint16_t			tcnt;
	uint16_t			gra;
	uint16_t			grb;
	uint16_t			grc;
	uint16_t			grd;
	};

}
}

extern volatile Oscl::H83664::TimerW	H83664_TimerW __attribute__((eightbit_data));
extern volatile Oscl::H83664::TimerA	H83664_TimerA __attribute__((eightbit_data));
extern volatile uint8_t		H83664_PDR5 __attribute__((eightbit_data));
extern volatile uint8_t		H83664_PMR5 __attribute__((eightbit_data));
extern volatile uint8_t		H83664_PCR5 __attribute__((eightbit_data));

extern volatile uint8_t		H83664_PDR8 __attribute__((eightbit_data));
extern volatile uint8_t		H83664_PCR8 __attribute__((eightbit_data));

extern volatile uint8_t		H83664_IRR1 __attribute__((eightbit_data));
extern volatile uint8_t		H83664_IENR1 __attribute__((eightbit_data));

extern volatile uint8_t		H83664_SMR __attribute__((eightbit_data));
extern volatile uint8_t		H83664_BRR __attribute__((eightbit_data));
extern volatile uint8_t		H83664_SCR3 __attribute__((eightbit_data));
extern volatile uint8_t		H83664_TDR __attribute__((eightbit_data));
extern volatile uint8_t		H83664_SSR __attribute__((eightbit_data));
extern volatile uint8_t		H83664_RDR __attribute__((eightbit_data));
extern volatile uint8_t		H83664_ICCR __attribute__((eightbit_data));
extern volatile uint8_t		H83664_TIOR0 __attribute__((eightbit_data));
extern volatile uint8_t		H83664_TIOR1 __attribute__((eightbit_data));
extern volatile uint8_t		H83664_PMR1 __attribute__((eightbit_data));

#endif
