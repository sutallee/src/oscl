/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_hw_motorola_68k_vectorsh_
#define _oscl_hw_motorola_68k_vectorsh_
#include <stdint.h>

typedef void (*M68xxxExceptionHandler)(void);

typedef union M68xxxExceptionVectors{
	struct{
		uint8_t				*initialStackPointer;
		void					(*initialProgramCounter)(void);
		M68xxxExceptionHandler	busError;
		M68xxxExceptionHandler	addressError;
		M68xxxExceptionHandler	illegalInstruction;
		M68xxxExceptionHandler	zeroDivision;
		M68xxxExceptionHandler	chkInstructions;
		M68xxxExceptionHandler	trapInstructions;
		M68xxxExceptionHandler	privilegeViolation;
		M68xxxExceptionHandler	trace;
		M68xxxExceptionHandler	line1010Emulator;
		M68xxxExceptionHandler	line1111Emulator;
		M68xxxExceptionHandler	hardwareBreakpoint;
		M68xxxExceptionHandler	reservedCPV;
		M68xxxExceptionHandler	formatErrorAndUnitializedInterrupt14;
		M68xxxExceptionHandler	formatErrorAndUnitializedInterrupt15;
		M68xxxExceptionHandler	reserved16;
		M68xxxExceptionHandler	reserved17;
		M68xxxExceptionHandler	reserved18;
		M68xxxExceptionHandler	reserved19;
		M68xxxExceptionHandler	reserved20;
		M68xxxExceptionHandler	reserved21;
		M68xxxExceptionHandler	reserved22;
		M68xxxExceptionHandler	reserved23;
		M68xxxExceptionHandler	spuriousInterrupt;
		M68xxxExceptionHandler	level1InterruptAutovector;
		M68xxxExceptionHandler	level2InterruptAutovector;
		M68xxxExceptionHandler	level3InterruptAutovector;
		M68xxxExceptionHandler	level4InterruptAutovector;
		M68xxxExceptionHandler	level5InterruptAutovector;
		M68xxxExceptionHandler	level6InterruptAutovector;
		M68xxxExceptionHandler	level7InterruptAutovector;
		M68xxxExceptionHandler	trapInstruction0;
		M68xxxExceptionHandler	trapInstruction1;
		M68xxxExceptionHandler	trapInstruction2;
		M68xxxExceptionHandler	trapInstruction3;
		M68xxxExceptionHandler	trapInstruction4;
		M68xxxExceptionHandler	trapInstruction5;
		M68xxxExceptionHandler	trapInstruction6;
		M68xxxExceptionHandler	trapInstruction7;
		M68xxxExceptionHandler	trapInstruction8;
		M68xxxExceptionHandler	trapInstruction9;
		M68xxxExceptionHandler	trapInstructionA;
		M68xxxExceptionHandler	trapInstructionB;
		M68xxxExceptionHandler	trapInstructionC;
		M68xxxExceptionHandler	trapInstructionD;
		M68xxxExceptionHandler	trapInstructionE;
		M68xxxExceptionHandler	trapInstructionF;
		M68xxxExceptionHandler	reservedCoprocessor[11];
		M68xxxExceptionHandler	unassignedReserved[5];
		M68xxxExceptionHandler	userDefinedVectors[192];
		}byName;
	struct{
		M68xxxExceptionHandler	vector[256];
		}byNumber;
	}M68xxxExceptionVectors;

#endif
