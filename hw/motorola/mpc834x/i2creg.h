#ifndef _oscl_hw_motorola_mpc834x_i2cregh_
#define _oscl_hw_motorola_mpc834x_i2cregh_
#include <stdint.h>
namespace Oscl { // Namespace description
	namespace Motorola { // Namespace description
		namespace MPC834x { // Namespace description
			namespace I2C { // Namespace description
				namespace I2CADR { // Register description
					typedef uint8_t	Reg;
					namespace ADDR { // Field Description
						enum {Lsb = 1};
						enum {FieldMask = 0x000000FE};
						};
					};
				namespace I2CFDR { // Register description
					typedef uint8_t	Reg;
					namespace FDR { // Field Description
						enum {Lsb = 0};
						enum {FieldMask = 0x0000003F};
						enum {Value_DivideBy384 = 0x0};
						enum {ValueMask_DivideBy384 = 0x00000000};
						enum {Value_DivideBy416 = 0x1};
						enum {ValueMask_DivideBy416 = 0x00000001};
						enum {Value_DivideBy480 = 0x2};
						enum {ValueMask_DivideBy480 = 0x00000002};
						enum {Value_DivideBy576 = 0x3};
						enum {ValueMask_DivideBy576 = 0x00000003};
						enum {Value_DivideBy640 = 0x4};
						enum {ValueMask_DivideBy640 = 0x00000004};
						enum {Value_DivideBy704 = 0x5};
						enum {ValueMask_DivideBy704 = 0x00000005};
						enum {Value_DivideBy832 = 0x6};
						enum {ValueMask_DivideBy832 = 0x00000006};
						enum {Value_DivideBy1024 = 0x7};
						enum {ValueMask_DivideBy1024 = 0x00000007};
						enum {Value_DivideBy1152 = 0x8};
						enum {ValueMask_DivideBy1152 = 0x00000008};
						enum {Value_DivideBy1280 = 0x9};
						enum {ValueMask_DivideBy1280 = 0x00000009};
						enum {Value_DivideBy1536 = 0xA};
						enum {ValueMask_DivideBy1536 = 0x0000000A};
						enum {Value_DivideBy1920 = 0xB};
						enum {ValueMask_DivideBy1920 = 0x0000000B};
						enum {Value_DivideBy2304 = 0xC};
						enum {ValueMask_DivideBy2304 = 0x0000000C};
						enum {Value_DivideBy2560 = 0xD};
						enum {ValueMask_DivideBy2560 = 0x0000000D};
						enum {Value_DivideBy3072 = 0xE};
						enum {ValueMask_DivideBy3072 = 0x0000000E};
						enum {Value_DivideBy3840 = 0xF};
						enum {ValueMask_DivideBy3840 = 0x0000000F};
						enum {Value_DivideBy4608 = 0x10};
						enum {ValueMask_DivideBy4608 = 0x00000010};
						enum {Value_DivideBy5120 = 0x11};
						enum {ValueMask_DivideBy5120 = 0x00000011};
						enum {Value_DivideBy6144 = 0x12};
						enum {ValueMask_DivideBy6144 = 0x00000012};
						enum {Value_DivideBy7680 = 0x13};
						enum {ValueMask_DivideBy7680 = 0x00000013};
						enum {Value_DivideBy9216 = 0x14};
						enum {ValueMask_DivideBy9216 = 0x00000014};
						enum {Value_DivideBy10240 = 0x15};
						enum {ValueMask_DivideBy10240 = 0x00000015};
						enum {Value_DivideBy12288 = 0x16};
						enum {ValueMask_DivideBy12288 = 0x00000016};
						enum {Value_DivideBy15360 = 0x17};
						enum {ValueMask_DivideBy15360 = 0x00000017};
						enum {Value_DivideBy18432 = 0x18};
						enum {ValueMask_DivideBy18432 = 0x00000018};
						enum {Value_DivideBy20480 = 0x19};
						enum {ValueMask_DivideBy20480 = 0x00000019};
						enum {Value_DivideBy24576 = 0x1A};
						enum {ValueMask_DivideBy24576 = 0x0000001A};
						enum {Value_DivideBy30720 = 0x1B};
						enum {ValueMask_DivideBy30720 = 0x0000001B};
						enum {Value_DivideBy36864 = 0x1C};
						enum {ValueMask_DivideBy36864 = 0x0000001C};
						enum {Value_DivideBy40960 = 0x1D};
						enum {ValueMask_DivideBy40960 = 0x0000001D};
						enum {Value_DivideBy49152 = 0x1E};
						enum {ValueMask_DivideBy49152 = 0x0000001E};
						enum {Value_DivideBy61440 = 0x1F};
						enum {ValueMask_DivideBy61440 = 0x0000001F};
						enum {Value_DivideBy256 = 0x20};
						enum {ValueMask_DivideBy256 = 0x00000020};
						enum {Value_DivideBy288 = 0x21};
						enum {ValueMask_DivideBy288 = 0x00000021};
						enum {Value_DivideBy320 = 0x22};
						enum {ValueMask_DivideBy320 = 0x00000022};
						enum {Value_DivideBy352 = 0x23};
						enum {ValueMask_DivideBy352 = 0x00000023};
						enum {Value_DivideBy384b = 0x24};
						enum {ValueMask_DivideBy384b = 0x00000024};
						enum {Value_DivideBy448 = 0x25};
						enum {ValueMask_DivideBy448 = 0x00000025};
						enum {Value_DivideBy512 = 0x26};
						enum {ValueMask_DivideBy512 = 0x00000026};
						enum {Value_DivideBy576b = 0x27};
						enum {ValueMask_DivideBy576b = 0x00000027};
						enum {Value_DivideBy640b = 0x28};
						enum {ValueMask_DivideBy640b = 0x00000028};
						enum {Value_DivideBy768 = 0x29};
						enum {ValueMask_DivideBy768 = 0x00000029};
						enum {Value_DivideBy896 = 0x2A};
						enum {ValueMask_DivideBy896 = 0x0000002A};
						enum {Value_DivideBy1024b = 0x2B};
						enum {ValueMask_DivideBy1024b = 0x0000002B};
						enum {Value_DivideBy1280b = 0x2C};
						enum {ValueMask_DivideBy1280b = 0x0000002C};
						enum {Value_DivideBy1536b = 0x2D};
						enum {ValueMask_DivideBy1536b = 0x0000002D};
						enum {Value_DivideBy1792 = 0x2E};
						enum {ValueMask_DivideBy1792 = 0x0000002E};
						enum {Value_DivideBy2048 = 0x2F};
						enum {ValueMask_DivideBy2048 = 0x0000002F};
						enum {Value_DivideBy2560b = 0x30};
						enum {ValueMask_DivideBy2560b = 0x00000030};
						enum {Value_DivideBy3072b = 0x31};
						enum {ValueMask_DivideBy3072b = 0x00000031};
						enum {Value_DivideBy3584 = 0x32};
						enum {ValueMask_DivideBy3584 = 0x00000032};
						enum {Value_DivideBy4096 = 0x33};
						enum {ValueMask_DivideBy4096 = 0x00000033};
						enum {Value_DivideBy5120b = 0x34};
						enum {ValueMask_DivideBy5120b = 0x00000034};
						enum {Value_DivideBy6144b = 0x35};
						enum {ValueMask_DivideBy6144b = 0x00000035};
						enum {Value_DivideBy7168 = 0x36};
						enum {ValueMask_DivideBy7168 = 0x00000036};
						enum {Value_DivideBy8192 = 0x37};
						enum {ValueMask_DivideBy8192 = 0x00000037};
						enum {Value_DivideBy10240b = 0x38};
						enum {ValueMask_DivideBy10240b = 0x00000038};
						enum {Value_DivideBy12288b = 0x39};
						enum {ValueMask_DivideBy12288b = 0x00000039};
						enum {Value_DivideBy14336 = 0x3A};
						enum {ValueMask_DivideBy14336 = 0x0000003A};
						enum {Value_DivideBy16384 = 0x3B};
						enum {ValueMask_DivideBy16384 = 0x0000003B};
						enum {Value_DivideBy20480b = 0x3C};
						enum {ValueMask_DivideBy20480b = 0x0000003C};
						enum {Value_DivideBy24576b = 0x3D};
						enum {ValueMask_DivideBy24576b = 0x0000003D};
						enum {Value_DivideBy28672 = 0x3E};
						enum {ValueMask_DivideBy28672 = 0x0000003E};
						enum {Value_DivideBy32768 = 0x3F};
						enum {ValueMask_DivideBy32768 = 0x0000003F};
						};
					};
				namespace I2CCR { // Register description
					typedef uint8_t	Reg;
					namespace MEN { // Field Description
						enum {Lsb = 7};
						enum {FieldMask = 0x00000080};
						enum {Value_ModuleDisable = 0x0};
						enum {ValueMask_ModuleDisable = 0x00000000};
						enum {Value_ModuleEnable = 0x1};
						enum {ValueMask_ModuleEnable = 0x00000080};
						};
					namespace MIEN { // Field Description
						enum {Lsb = 6};
						enum {FieldMask = 0x00000040};
						enum {Value_InterruptDisable = 0x0};
						enum {ValueMask_InterruptDisable = 0x00000000};
						enum {Value_InterruptEnable = 0x1};
						enum {ValueMask_InterruptEnable = 0x00000040};
						};
					namespace MSTA { // Field Description
						enum {Lsb = 5};
						enum {FieldMask = 0x00000020};
						enum {Value_SlaveMode = 0x0};
						enum {ValueMask_SlaveMode = 0x00000000};
						enum {Value_MasterMode = 0x1};
						enum {ValueMask_MasterMode = 0x00000020};
						};
					namespace MTX { // Field Description
						enum {Lsb = 4};
						enum {FieldMask = 0x00000010};
						enum {Value_ReceiveMode = 0x0};
						enum {ValueMask_ReceiveMode = 0x00000000};
						enum {Value_TransmitMode = 0x1};
						enum {ValueMask_TransmitMode = 0x00000010};
						};
					namespace TXAK { // Field Description
						enum {Lsb = 3};
						enum {FieldMask = 0x00000008};
						enum {Value_SendAckAt9thBitOnRx = 0x0};
						enum {ValueMask_SendAckAt9thBitOnRx = 0x00000000};
						enum {Value_NoAckSent = 0x1};
						enum {ValueMask_NoAckSent = 0x00000008};
						};
					namespace RSTA { // Field Description
						enum {Lsb = 2};
						enum {FieldMask = 0x00000004};
						enum {Value_NoStartCondition = 0x0};
						enum {ValueMask_NoStartCondition = 0x00000000};
						enum {Value_GenerateRepeatStartCondition = 0x1};
						enum {ValueMask_GenerateRepeatStartCondition = 0x00000004};
						};
					namespace BCST { // Field Description
						enum {Lsb = 0};
						enum {FieldMask = 0x00000001};
						enum {Value_BroadcastRxDisable = 0x0};
						enum {ValueMask_BroadcastRxDisable = 0x00000000};
						enum {Value_BroadcastRxEnable = 0x1};
						enum {ValueMask_BroadcastRxEnable = 0x00000001};
						};
					};
				namespace I2CSR { // Register description
					typedef uint8_t	Reg;
					namespace MCF { // Field Description
						enum {Lsb = 7};
						enum {FieldMask = 0x00000080};
						enum {Value_ByteTransferInProgress = 0x0};
						enum {ValueMask_ByteTransferInProgress = 0x00000000};
						enum {Value_ByteTransferComplete = 0x1};
						enum {ValueMask_ByteTransferComplete = 0x00000080};
						};
					namespace MAAS { // Field Description
						enum {Lsb = 6};
						enum {FieldMask = 0x00000040};
						enum {Value_NotAddressedAsSlave = 0x0};
						enum {ValueMask_NotAddressedAsSlave = 0x00000000};
						enum {Value_AddressedAsSlave = 0x1};
						enum {ValueMask_AddressedAsSlave = 0x00000040};
						};
					namespace MBB { // Field Description
						enum {Lsb = 5};
						enum {FieldMask = 0x00000020};
						enum {Value_BusIsIdle = 0x0};
						enum {ValueMask_BusIsIdle = 0x00000000};
						enum {Value_BusIsBusy = 0x1};
						enum {ValueMask_BusIsBusy = 0x00000020};
						};
					namespace MAL { // Field Description
						enum {Lsb = 4};
						enum {FieldMask = 0x00000010};
						enum {Value_ArbitrationLost = 0x1};
						enum {ValueMask_ArbitrationLost = 0x00000010};
						enum {Value_Clear = 0x0};
						enum {ValueMask_Clear = 0x00000000};
						};
					namespace BCSTM { // Field Description
						enum {Lsb = 3};
						enum {FieldMask = 0x00000008};
						enum {Value_NoBroadcastMatch = 0x0};
						enum {ValueMask_NoBroadcastMatch = 0x00000000};
						enum {Value_BroadcastMatch = 0x1};
						enum {ValueMask_BroadcastMatch = 0x00000008};
						};
					namespace SRW { // Field Description
						enum {Lsb = 2};
						enum {FieldMask = 0x00000004};
						enum {Value_Write = 0x0};
						enum {ValueMask_Write = 0x00000000};
						enum {Value_Read = 0x1};
						enum {ValueMask_Read = 0x00000004};
						};
					namespace MIF { // Field Description
						enum {Lsb = 1};
						enum {FieldMask = 0x00000002};
						enum {Value_Pending = 0x1};
						enum {ValueMask_Pending = 0x00000002};
						enum {Value_Clear = 0x0};
						enum {ValueMask_Clear = 0x00000000};
						};
					namespace RXAK { // Field Description
						enum {Lsb = 0};
						enum {FieldMask = 0x00000001};
						enum {Value_AckReceived = 0x0};
						enum {ValueMask_AckReceived = 0x00000000};
						enum {Value_AckNotReceived = 0x1};
						enum {ValueMask_AckNotReceived = 0x00000001};
						};
					};
				namespace I2CDR { // Register description
					typedef uint8_t	Reg;
					};
				namespace I2CDFSRR { // Register description
					typedef uint8_t	Reg;
					namespace DFSR { // Field Description
						enum {Lsb = 0};
						enum {FieldMask = 0x0000003F};
						enum {Value_Default = 0x10};
						enum {ValueMask_Default = 0x00000010};
						};
					};
				};
			};
		};
	};
#endif
