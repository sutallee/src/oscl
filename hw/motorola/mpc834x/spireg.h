#ifndef _oscl_hw_motorola_mpc834x_spiregh_
#define _oscl_hw_motorola_mpc834x_spiregh_
#include <stdint.h>
namespace Oscl { // Namespace description
	namespace Motorola { // Namespace description
		namespace MPC834x { // Namespace description
			namespace SPI { // Namespace description
				namespace SPMODE { // Register description
					typedef uint32_t	Reg;
					namespace LOOP { // Field Description
						enum {Lsb = 30};
						enum {FieldMask = 0x40000000};
						enum {Value_Normal = 0x0};
						enum {ValueMask_Normal = 0x00000000};
						enum {Value_Loopback = 0x1};
						enum {ValueMask_Loopback = 0x40000000};
						};
					namespace CI { // Field Description
						enum {Lsb = 29};
						enum {FieldMask = 0x20000000};
						enum {Value_Normal = 0x0};
						enum {ValueMask_Normal = 0x00000000};
						enum {Value_Invert = 0x1};
						enum {ValueMask_Invert = 0x20000000};
						};
					namespace CP { // Field Description
						enum {Lsb = 28};
						enum {FieldMask = 0x10000000};
						enum {Value_ClockInMiddle = 0x0};
						enum {ValueMask_ClockInMiddle = 0x00000000};
						enum {Value_ClockAtStart = 0x1};
						enum {ValueMask_ClockAtStart = 0x10000000};
						};
					namespace DIV16 { // Field Description
						enum {Lsb = 27};
						enum {FieldMask = 0x08000000};
						enum {Value_DivideBy1 = 0x0};
						enum {ValueMask_DivideBy1 = 0x00000000};
						enum {Value_DivideBy16 = 0x1};
						enum {ValueMask_DivideBy16 = 0x08000000};
						};
					namespace REV { // Field Description
						enum {Lsb = 26};
						enum {FieldMask = 0x04000000};
						enum {Value_LsbFirst = 0x0};
						enum {ValueMask_LsbFirst = 0x00000000};
						enum {Value_MsbFirst = 0x1};
						enum {ValueMask_MsbFirst = 0x04000000};
						};
					namespace MS { // Field Description
						enum {Lsb = 25};
						enum {FieldMask = 0x02000000};
						enum {Value_Slave = 0x0};
						enum {ValueMask_Slave = 0x00000000};
						enum {Value_Master = 0x1};
						enum {ValueMask_Master = 0x02000000};
						};
					namespace EN { // Field Description
						enum {Lsb = 24};
						enum {FieldMask = 0x01000000};
						enum {Value_Disable = 0x0};
						enum {ValueMask_Disable = 0x00000000};
						enum {Value_Enable = 0x1};
						enum {ValueMask_Enable = 0x01000000};
						};
					namespace LEN { // Field Description
						enum {Lsb = 20};
						enum {FieldMask = 0x00F00000};
						enum {Value_ThirtyTwoBit = 0x0};
						enum {ValueMask_ThirtyTwoBit = 0x00000000};
						enum {Value_FourBit = 0x3};
						enum {ValueMask_FourBit = 0x00300000};
						enum {Value_FiveBit = 0x4};
						enum {ValueMask_FiveBit = 0x00400000};
						enum {Value_SixBit = 0x5};
						enum {ValueMask_SixBit = 0x00500000};
						enum {Value_SevenBit = 0x6};
						enum {ValueMask_SevenBit = 0x00600000};
						enum {Value_EightBit = 0x7};
						enum {ValueMask_EightBit = 0x00700000};
						enum {Value_NineBit = 0x8};
						enum {ValueMask_NineBit = 0x00800000};
						enum {Value_TenBit = 0x9};
						enum {ValueMask_TenBit = 0x00900000};
						enum {Value_ElevenBit = 0xA};
						enum {ValueMask_ElevenBit = 0x00A00000};
						enum {Value_TwelveBit = 0xB};
						enum {ValueMask_TwelveBit = 0x00B00000};
						enum {Value_ThirteenBit = 0xC};
						enum {ValueMask_ThirteenBit = 0x00C00000};
						enum {Value_FourteenBit = 0xD};
						enum {ValueMask_FourteenBit = 0x00D00000};
						enum {Value_FifteenBit = 0xE};
						enum {ValueMask_FifteenBit = 0x00E00000};
						enum {Value_SixteenBit = 0xF};
						enum {ValueMask_SixteenBit = 0x00F00000};
						};
					namespace PM { // Field Description
						enum {Lsb = 16};
						enum {FieldMask = 0x000F0000};
						};
					namespace OD { // Field Description
						enum {Lsb = 12};
						enum {FieldMask = 0x00001000};
						enum {Value_ActiveOutputs = 0x0};
						enum {ValueMask_ActiveOutputs = 0x00000000};
						enum {Value_OpenDrainOutputs = 0x1};
						enum {ValueMask_OpenDrainOutputs = 0x00001000};
						};
					};
				namespace SPIE { // Register description
					typedef uint32_t	Reg;
					namespace LT { // Field Description
						enum {Lsb = 14};
						enum {FieldMask = 0x00004000};
						enum {Value_Clear = 0x1};
						enum {ValueMask_Clear = 0x00004000};
						enum {Value_LastCharacterTransmitted = 0x1};
						enum {ValueMask_LastCharacterTransmitted = 0x00004000};
						};
					namespace DNR { // Field Description
						enum {Lsb = 13};
						enum {FieldMask = 0x00002000};
						enum {Value_Clear = 0x1};
						enum {ValueMask_Clear = 0x00002000};
						enum {Value_DataNotReadyWhenSelected = 0x1};
						enum {ValueMask_DataNotReadyWhenSelected = 0x00002000};
						};
					namespace OV { // Field Description
						enum {Lsb = 12};
						enum {FieldMask = 0x00001000};
						enum {Value_Clear = 0x1};
						enum {ValueMask_Clear = 0x00001000};
						enum {Value_RxOverrun = 0x1};
						enum {ValueMask_RxOverrun = 0x00001000};
						};
					namespace UN { // Field Description
						enum {Lsb = 11};
						enum {FieldMask = 0x00000800};
						enum {Value_Clear = 0x1};
						enum {ValueMask_Clear = 0x00000800};
						enum {Value_TxUnderrun = 0x1};
						enum {ValueMask_TxUnderrun = 0x00000800};
						};
					namespace MME { // Field Description
						enum {Lsb = 10};
						enum {FieldMask = 0x00000400};
						enum {Value_Clear = 0x1};
						enum {ValueMask_Clear = 0x00000400};
						enum {Value_Collision = 0x1};
						enum {ValueMask_Collision = 0x00000400};
						};
					namespace NE { // Field Description
						enum {Lsb = 9};
						enum {FieldMask = 0x00000200};
						enum {Value_RxDataNotEmpty = 0x1};
						enum {ValueMask_RxDataNotEmpty = 0x00000200};
						};
					namespace NF { // Field Description
						enum {Lsb = 8};
						enum {FieldMask = 0x00000100};
						enum {Value_TxDataNotFull = 0x1};
						enum {ValueMask_TxDataNotFull = 0x00000100};
						};
					};
				namespace SPIM { // Register description
					typedef uint32_t	Reg;
					namespace LT { // Field Description
						enum {Lsb = 14};
						enum {FieldMask = 0x00004000};
						enum {Value_DisableInterrupt = 0x0};
						enum {ValueMask_DisableInterrupt = 0x00000000};
						enum {Value_EnableInterrupt = 0x1};
						enum {ValueMask_EnableInterrupt = 0x00004000};
						};
					namespace DNR { // Field Description
						enum {Lsb = 13};
						enum {FieldMask = 0x00002000};
						enum {Value_DisableInterrupt = 0x0};
						enum {ValueMask_DisableInterrupt = 0x00000000};
						enum {Value_EnableInterrupt = 0x1};
						enum {ValueMask_EnableInterrupt = 0x00002000};
						};
					namespace OV { // Field Description
						enum {Lsb = 12};
						enum {FieldMask = 0x00001000};
						enum {Value_DisableInterrupt = 0x0};
						enum {ValueMask_DisableInterrupt = 0x00000000};
						enum {Value_EnableInterrupt = 0x1};
						enum {ValueMask_EnableInterrupt = 0x00001000};
						};
					namespace UN { // Field Description
						enum {Lsb = 11};
						enum {FieldMask = 0x00000800};
						enum {Value_DisableInterrupt = 0x0};
						enum {ValueMask_DisableInterrupt = 0x00000000};
						enum {Value_EnableInterrupt = 0x1};
						enum {ValueMask_EnableInterrupt = 0x00000800};
						};
					namespace MME { // Field Description
						enum {Lsb = 10};
						enum {FieldMask = 0x00000400};
						enum {Value_DisableInterrupt = 0x0};
						enum {ValueMask_DisableInterrupt = 0x00000000};
						enum {Value_EnableInterrupt = 0x1};
						enum {ValueMask_EnableInterrupt = 0x00000400};
						};
					namespace NE { // Field Description
						enum {Lsb = 9};
						enum {FieldMask = 0x00000200};
						enum {Value_DisableInterrupt = 0x0};
						enum {ValueMask_DisableInterrupt = 0x00000000};
						enum {Value_EnableInterrupt = 0x1};
						enum {ValueMask_EnableInterrupt = 0x00000200};
						};
					namespace NF { // Field Description
						enum {Lsb = 8};
						enum {FieldMask = 0x00000100};
						enum {Value_DisableInterrupt = 0x0};
						enum {ValueMask_DisableInterrupt = 0x00000000};
						enum {Value_EnableInterrupt = 0x1};
						enum {ValueMask_EnableInterrupt = 0x00000100};
						};
					};
				namespace SPCOM { // Register description
					typedef uint32_t	Reg;
					namespace LST { // Field Description
						enum {Lsb = 22};
						enum {FieldMask = 0x00400000};
						enum {Value_NotLastCharacterOfFrame = 0x0};
						enum {ValueMask_NotLastCharacterOfFrame = 0x00000000};
						enum {Value_LastCharacterOfFrame = 0x1};
						enum {ValueMask_LastCharacterOfFrame = 0x00400000};
						};
					};
				namespace SPITD { // Register description
					typedef uint32_t	Reg;
					};
				namespace SPIRD { // Register description
					typedef uint32_t	Reg;
					};
				};
			};
		};
	};
#endif
