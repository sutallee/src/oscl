#ifndef _oscl_hw_motorola_mpc834x_picregh_
#define _oscl_hw_motorola_mpc834x_picregh_
#include <stdint.h>
namespace Oscl { // Namespace description
	namespace Motorola { // Namespace description
		namespace MPC834x { // Namespace description
			namespace PIC { // Namespace description
				namespace SICFR { // Register description
					typedef uint32_t	Reg;
					namespace HPI { // Field Description
						enum {Lsb = 24};
						enum {FieldMask = 0x7F000000};
						};
					namespace MPSB { // Field Description
						enum {Lsb = 22};
						enum {FieldMask = 0x00400000};
						enum {Value_MixbGrouped = 0x0};
						enum {ValueMask_MixbGrouped = 0x00000000};
						enum {Value_MixbSpread = 0x1};
						enum {ValueMask_MixbSpread = 0x00400000};
						};
					namespace MPSA { // Field Description
						enum {Lsb = 21};
						enum {FieldMask = 0x00200000};
						enum {Value_MixaGrouped = 0x0};
						enum {ValueMask_MixaGrouped = 0x00000000};
						enum {Value_MixaSpread = 0x1};
						enum {ValueMask_MixaSpread = 0x00200000};
						};
					namespace IPSD { // Field Description
						enum {Lsb = 19};
						enum {FieldMask = 0x00080000};
						enum {Value_SysdGrouped = 0x0};
						enum {ValueMask_SysdGrouped = 0x00000000};
						enum {Value_SysdSpread = 0x1};
						enum {ValueMask_SysdSpread = 0x00080000};
						};
					namespace IPSA { // Field Description
						enum {Lsb = 16};
						enum {FieldMask = 0x00010000};
						enum {Value_SysaGrouped = 0x0};
						enum {ValueMask_SysaGrouped = 0x00000000};
						enum {Value_SysaSpread = 0x1};
						enum {ValueMask_SysaSpread = 0x00010000};
						};
					namespace HPIT { // Field Description
						enum {Lsb = 8};
						enum {FieldMask = 0x00000300};
						enum {Value_intHPI = 0x0};
						enum {ValueMask_intHPI = 0x00000000};
						enum {Value_smiHPI = 0x1};
						enum {ValueMask_smiHPI = 0x00000100};
						enum {Value_cintHPI = 0x2};
						enum {ValueMask_cintHPI = 0x00000200};
						};
					};
				namespace SIVCR { // Register description
					typedef uint32_t	Reg;
					namespace IVECx { // Field Description
						enum {Lsb = 26};
						enum {FieldMask = 0xFC000000};
						};
					namespace IVEC { // Field Description
						enum {Lsb = 0};
						enum {FieldMask = 0x0000007F};
						enum {Value_Error = 0x0};
						enum {ValueMask_Error = 0x00000000};
						enum {Value_UART1 = 0x9};
						enum {ValueMask_UART1 = 0x00000009};
						enum {Value_UART2 = 0xA};
						enum {ValueMask_UART2 = 0x0000000A};
						enum {Value_SEC = 0xB};
						enum {ValueMask_SEC = 0x0000000B};
						enum {Value_I2C1 = 0xE};
						enum {ValueMask_I2C1 = 0x0000000E};
						enum {Value_I2C2 = 0xF};
						enum {ValueMask_I2C2 = 0x0000000F};
						enum {Value_SPI = 0x10};
						enum {ValueMask_SPI = 0x00000010};
						enum {Value_IRQ1 = 0x11};
						enum {ValueMask_IRQ1 = 0x00000011};
						enum {Value_IRQ2 = 0x12};
						enum {ValueMask_IRQ2 = 0x00000012};
						enum {Value_IRQ3 = 0x13};
						enum {ValueMask_IRQ3 = 0x00000013};
						enum {Value_IRQ4 = 0x14};
						enum {ValueMask_IRQ4 = 0x00000014};
						enum {Value_IRQ5 = 0x15};
						enum {ValueMask_IRQ5 = 0x00000015};
						enum {Value_IRQ6 = 0x16};
						enum {ValueMask_IRQ6 = 0x00000016};
						enum {Value_IRQ7 = 0x17};
						enum {ValueMask_IRQ7 = 0x00000017};
						enum {Value_TSEC1Tx = 0x20};
						enum {ValueMask_TSEC1Tx = 0x00000020};
						enum {Value_TSEC1Rx = 0x21};
						enum {ValueMask_TSEC1Rx = 0x00000021};
						enum {Value_TSEC1Err = 0x22};
						enum {ValueMask_TSEC1Err = 0x00000022};
						enum {Value_TSEC2Tx = 0x23};
						enum {ValueMask_TSEC2Tx = 0x00000023};
						enum {Value_TSEC2Rx = 0x24};
						enum {ValueMask_TSEC2Rx = 0x00000024};
						enum {Value_TSEC2Err = 0x25};
						enum {ValueMask_TSEC2Err = 0x00000025};
						enum {Value_USB2DR = 0x26};
						enum {ValueMask_USB2DR = 0x00000026};
						enum {Value_USB2MPH = 0x27};
						enum {ValueMask_USB2MPH = 0x00000027};
						enum {Value_IRQ0 = 0x30};
						enum {ValueMask_IRQ0 = 0x00000030};
						enum {Value_RTCSEC = 0x40};
						enum {ValueMask_RTCSEC = 0x00000040};
						enum {Value_PIT = 0x41};
						enum {ValueMask_PIT = 0x00000041};
						enum {Value_PCI1 = 0x42};
						enum {ValueMask_PCI1 = 0x00000042};
						enum {Value_PCI2 = 0x43};
						enum {ValueMask_PCI2 = 0x00000043};
						enum {Value_RTCALR = 0x44};
						enum {ValueMask_RTCALR = 0x00000044};
						enum {Value_MU = 0x45};
						enum {ValueMask_MU = 0x00000045};
						enum {Value_SBA = 0x46};
						enum {ValueMask_SBA = 0x00000046};
						enum {Value_DMA = 0x47};
						enum {ValueMask_DMA = 0x00000047};
						enum {Value_GTM4 = 0x48};
						enum {ValueMask_GTM4 = 0x00000048};
						enum {Value_GTM8 = 0x49};
						enum {ValueMask_GTM8 = 0x00000049};
						enum {Value_GPIO1 = 0x4A};
						enum {ValueMask_GPIO1 = 0x0000004A};
						enum {Value_GPIO2 = 0x4B};
						enum {ValueMask_GPIO2 = 0x0000004B};
						enum {Value_DDR = 0x4C};
						enum {ValueMask_DDR = 0x0000004C};
						enum {Value_LBC = 0x4D};
						enum {ValueMask_LBC = 0x0000004D};
						enum {Value_GTM2 = 0x4E};
						enum {ValueMask_GTM2 = 0x0000004E};
						enum {Value_GTM6 = 0x4F};
						enum {ValueMask_GTM6 = 0x0000004F};
						enum {Value_PMC = 0x50};
						enum {ValueMask_PMC = 0x00000050};
						enum {Value_GTM3 = 0x54};
						enum {ValueMask_GTM3 = 0x00000054};
						enum {Value_GTM7 = 0x55};
						enum {ValueMask_GTM7 = 0x00000055};
						enum {Value_GTM1 = 0x5A};
						enum {ValueMask_GTM1 = 0x0000005A};
						enum {Value_GTM5 = 0x5B};
						enum {ValueMask_GTM5 = 0x0000005B};
						};
					};
				namespace SIPNR_H { // Register description
					typedef uint32_t	Reg;
					enum {Mask = 1};
					enum {Pending = 1};
					namespace TSEC1Tx { // Field Description
						enum {Lsb = 31};
						enum {FieldMask = 0x80000000};
						enum {Value_Pending = 0x1};
						enum {ValueMask_Pending = 0x80000000};
						};
					namespace TSEC1Rx { // Field Description
						enum {Lsb = 30};
						enum {FieldMask = 0x40000000};
						enum {Value_Pending = 0x1};
						enum {ValueMask_Pending = 0x40000000};
						};
					namespace TSEC1Err { // Field Description
						enum {Lsb = 29};
						enum {FieldMask = 0x20000000};
						enum {Value_Pending = 0x1};
						enum {ValueMask_Pending = 0x20000000};
						};
					namespace TSEC2Tx { // Field Description
						enum {Lsb = 28};
						enum {FieldMask = 0x10000000};
						enum {Value_Pending = 0x1};
						enum {ValueMask_Pending = 0x10000000};
						};
					namespace TSEC2Rx { // Field Description
						enum {Lsb = 27};
						enum {FieldMask = 0x08000000};
						enum {Value_Pending = 0x1};
						enum {ValueMask_Pending = 0x08000000};
						};
					namespace TSEC2Err { // Field Description
						enum {Lsb = 26};
						enum {FieldMask = 0x04000000};
						enum {Value_Pending = 0x1};
						enum {ValueMask_Pending = 0x04000000};
						};
					namespace USB2DR { // Field Description
						enum {Lsb = 25};
						enum {FieldMask = 0x02000000};
						enum {Value_Pending = 0x1};
						enum {ValueMask_Pending = 0x02000000};
						};
					namespace USB2MPH { // Field Description
						enum {Lsb = 24};
						enum {FieldMask = 0x01000000};
						enum {Value_Pending = 0x1};
						enum {ValueMask_Pending = 0x01000000};
						};
					namespace UART1 { // Field Description
						enum {Lsb = 7};
						enum {FieldMask = 0x00000080};
						enum {Value_Pending = 0x1};
						enum {ValueMask_Pending = 0x00000080};
						};
					namespace UART2 { // Field Description
						enum {Lsb = 6};
						enum {FieldMask = 0x00000040};
						enum {Value_Pending = 0x1};
						enum {ValueMask_Pending = 0x00000040};
						};
					namespace SEC { // Field Description
						enum {Lsb = 6};
						enum {FieldMask = 0x00000040};
						enum {Value_Pending = 0x1};
						enum {ValueMask_Pending = 0x00000040};
						};
					namespace I2C1 { // Field Description
						enum {Lsb = 2};
						enum {FieldMask = 0x00000004};
						enum {Value_Pending = 0x1};
						enum {ValueMask_Pending = 0x00000004};
						};
					namespace I2C2 { // Field Description
						enum {Lsb = 1};
						enum {FieldMask = 0x00000002};
						enum {Value_Pending = 0x1};
						enum {ValueMask_Pending = 0x00000002};
						};
					namespace SPI { // Field Description
						enum {Lsb = 0};
						enum {FieldMask = 0x00000001};
						enum {Value_Pending = 0x1};
						enum {ValueMask_Pending = 0x00000001};
						};
					};
				namespace SIPNR_L { // Register description
					typedef uint32_t	Reg;
					enum {Mask = 1};
					enum {Pending = 1};
					namespace RTCSEC { // Field Description
						enum {Lsb = 31};
						enum {FieldMask = 0x80000000};
						enum {Value_Pending = 0x1};
						enum {ValueMask_Pending = 0x80000000};
						};
					namespace PIT { // Field Description
						enum {Lsb = 30};
						enum {FieldMask = 0x40000000};
						enum {Value_Pending = 0x1};
						enum {ValueMask_Pending = 0x40000000};
						};
					namespace PCI1 { // Field Description
						enum {Lsb = 29};
						enum {FieldMask = 0x20000000};
						enum {Value_Pending = 0x1};
						enum {ValueMask_Pending = 0x20000000};
						};
					namespace PCI2 { // Field Description
						enum {Lsb = 28};
						enum {FieldMask = 0x10000000};
						enum {Value_Pending = 0x1};
						enum {ValueMask_Pending = 0x10000000};
						};
					namespace RTCALR { // Field Description
						enum {Lsb = 27};
						enum {FieldMask = 0x08000000};
						enum {Value_Pending = 0x1};
						enum {ValueMask_Pending = 0x08000000};
						};
					namespace MU { // Field Description
						enum {Lsb = 26};
						enum {FieldMask = 0x04000000};
						enum {Value_Pending = 0x1};
						enum {ValueMask_Pending = 0x04000000};
						};
					namespace SBA { // Field Description
						enum {Lsb = 25};
						enum {FieldMask = 0x02000000};
						enum {Value_Pending = 0x1};
						enum {ValueMask_Pending = 0x02000000};
						};
					namespace DMA { // Field Description
						enum {Lsb = 24};
						enum {FieldMask = 0x01000000};
						enum {Value_Pending = 0x1};
						enum {ValueMask_Pending = 0x01000000};
						};
					namespace GTM4 { // Field Description
						enum {Lsb = 23};
						enum {FieldMask = 0x00800000};
						enum {Value_Pending = 0x1};
						enum {ValueMask_Pending = 0x00800000};
						};
					namespace GTM8 { // Field Description
						enum {Lsb = 22};
						enum {FieldMask = 0x00400000};
						enum {Value_Pending = 0x1};
						enum {ValueMask_Pending = 0x00400000};
						};
					namespace GPIO1 { // Field Description
						enum {Lsb = 21};
						enum {FieldMask = 0x00200000};
						enum {Value_Pending = 0x1};
						enum {ValueMask_Pending = 0x00200000};
						};
					namespace GPIO2 { // Field Description
						enum {Lsb = 20};
						enum {FieldMask = 0x00100000};
						enum {Value_Pending = 0x1};
						enum {ValueMask_Pending = 0x00100000};
						};
					namespace DDR { // Field Description
						enum {Lsb = 19};
						enum {FieldMask = 0x00080000};
						enum {Value_Pending = 0x1};
						enum {ValueMask_Pending = 0x00080000};
						};
					namespace LBC { // Field Description
						enum {Lsb = 18};
						enum {FieldMask = 0x00040000};
						enum {Value_Pending = 0x1};
						enum {ValueMask_Pending = 0x00040000};
						};
					namespace GTM2 { // Field Description
						enum {Lsb = 17};
						enum {FieldMask = 0x00020000};
						enum {Value_Pending = 0x1};
						enum {ValueMask_Pending = 0x00020000};
						};
					namespace GTM6 { // Field Description
						enum {Lsb = 16};
						enum {FieldMask = 0x00010000};
						enum {Value_Pending = 0x1};
						enum {ValueMask_Pending = 0x00010000};
						};
					namespace PMC { // Field Description
						enum {Lsb = 15};
						enum {FieldMask = 0x00008000};
						enum {Value_Pending = 0x1};
						enum {ValueMask_Pending = 0x00008000};
						};
					namespace GTM3 { // Field Description
						enum {Lsb = 11};
						enum {FieldMask = 0x00000800};
						enum {Value_Pending = 0x1};
						enum {ValueMask_Pending = 0x00000800};
						};
					namespace GTM7 { // Field Description
						enum {Lsb = 10};
						enum {FieldMask = 0x00000400};
						enum {Value_Pending = 0x1};
						enum {ValueMask_Pending = 0x00000400};
						};
					namespace GTM1 { // Field Description
						enum {Lsb = 5};
						enum {FieldMask = 0x00000020};
						enum {Value_Pending = 0x1};
						enum {ValueMask_Pending = 0x00000020};
						};
					namespace GTM5 { // Field Description
						enum {Lsb = 4};
						enum {FieldMask = 0x00000010};
						enum {Value_Pending = 0x1};
						enum {ValueMask_Pending = 0x00000010};
						};
					};
				namespace SIPRR_A { // Register description
					typedef uint32_t	Reg;
					enum {TSEC1Tx = 0};
					enum {TSEC1Rx = 1};
					enum {TSEC1Err = 2};
					enum {TSEC2Tx = 3};
					enum {TSEC2Rx = 4};
					enum {TSEC2Err = 5};
					enum {USB2DR = 6};
					enum {USB2MPH = 7};
					namespace SYSA0P { // Field Description
						enum {Lsb = 29};
						enum {FieldMask = 0xE0000000};
						enum {Value_TSEC1Tx = 0x0};
						enum {ValueMask_TSEC1Tx = 0x00000000};
						enum {Value_TSEC1Rx = 0x1};
						enum {ValueMask_TSEC1Rx = 0x20000000};
						enum {Value_TSEC1Err = 0x2};
						enum {ValueMask_TSEC1Err = 0x40000000};
						enum {Value_TSEC2Tx = 0x3};
						enum {ValueMask_TSEC2Tx = 0x60000000};
						enum {Value_TSEC2Rx = 0x4};
						enum {ValueMask_TSEC2Rx = 0x80000000};
						enum {Value_TSEC2Err = 0x5};
						enum {ValueMask_TSEC2Err = 0xA0000000};
						enum {Value_USB2DR = 0x6};
						enum {ValueMask_USB2DR = 0xC0000000};
						enum {Value_USB2MPH = 0x7};
						enum {ValueMask_USB2MPH = 0xE0000000};
						};
					namespace SYSA1P { // Field Description
						enum {Lsb = 26};
						enum {FieldMask = 0x1C000000};
						enum {Value_TSEC1Tx = 0x0};
						enum {ValueMask_TSEC1Tx = 0x00000000};
						enum {Value_TSEC1Rx = 0x1};
						enum {ValueMask_TSEC1Rx = 0x04000000};
						enum {Value_TSEC1Err = 0x2};
						enum {ValueMask_TSEC1Err = 0x08000000};
						enum {Value_TSEC2Tx = 0x3};
						enum {ValueMask_TSEC2Tx = 0x0C000000};
						enum {Value_TSEC2Rx = 0x4};
						enum {ValueMask_TSEC2Rx = 0x10000000};
						enum {Value_TSEC2Err = 0x5};
						enum {ValueMask_TSEC2Err = 0x14000000};
						enum {Value_USB2DR = 0x6};
						enum {ValueMask_USB2DR = 0x18000000};
						enum {Value_USB2MPH = 0x7};
						enum {ValueMask_USB2MPH = 0x1C000000};
						};
					namespace SYSA2P { // Field Description
						enum {Lsb = 23};
						enum {FieldMask = 0x03800000};
						enum {Value_TSEC1Tx = 0x0};
						enum {ValueMask_TSEC1Tx = 0x00000000};
						enum {Value_TSEC1Rx = 0x1};
						enum {ValueMask_TSEC1Rx = 0x00800000};
						enum {Value_TSEC1Err = 0x2};
						enum {ValueMask_TSEC1Err = 0x01000000};
						enum {Value_TSEC2Tx = 0x3};
						enum {ValueMask_TSEC2Tx = 0x01800000};
						enum {Value_TSEC2Rx = 0x4};
						enum {ValueMask_TSEC2Rx = 0x02000000};
						enum {Value_TSEC2Err = 0x5};
						enum {ValueMask_TSEC2Err = 0x02800000};
						enum {Value_USB2DR = 0x6};
						enum {ValueMask_USB2DR = 0x03000000};
						enum {Value_USB2MPH = 0x7};
						enum {ValueMask_USB2MPH = 0x03800000};
						};
					namespace SYSA3P { // Field Description
						enum {Lsb = 20};
						enum {FieldMask = 0x00700000};
						enum {Value_TSEC1Tx = 0x0};
						enum {ValueMask_TSEC1Tx = 0x00000000};
						enum {Value_TSEC1Rx = 0x1};
						enum {ValueMask_TSEC1Rx = 0x00100000};
						enum {Value_TSEC1Err = 0x2};
						enum {ValueMask_TSEC1Err = 0x00200000};
						enum {Value_TSEC2Tx = 0x3};
						enum {ValueMask_TSEC2Tx = 0x00300000};
						enum {Value_TSEC2Rx = 0x4};
						enum {ValueMask_TSEC2Rx = 0x00400000};
						enum {Value_TSEC2Err = 0x5};
						enum {ValueMask_TSEC2Err = 0x00500000};
						enum {Value_USB2DR = 0x6};
						enum {ValueMask_USB2DR = 0x00600000};
						enum {Value_USB2MPH = 0x7};
						enum {ValueMask_USB2MPH = 0x00700000};
						};
					namespace SYSA4P { // Field Description
						enum {Lsb = 13};
						enum {FieldMask = 0x0000E000};
						enum {Value_TSEC1Tx = 0x0};
						enum {ValueMask_TSEC1Tx = 0x00000000};
						enum {Value_TSEC1Rx = 0x1};
						enum {ValueMask_TSEC1Rx = 0x00002000};
						enum {Value_TSEC1Err = 0x2};
						enum {ValueMask_TSEC1Err = 0x00004000};
						enum {Value_TSEC2Tx = 0x3};
						enum {ValueMask_TSEC2Tx = 0x00006000};
						enum {Value_TSEC2Rx = 0x4};
						enum {ValueMask_TSEC2Rx = 0x00008000};
						enum {Value_TSEC2Err = 0x5};
						enum {ValueMask_TSEC2Err = 0x0000A000};
						enum {Value_USB2DR = 0x6};
						enum {ValueMask_USB2DR = 0x0000C000};
						enum {Value_USB2MPH = 0x7};
						enum {ValueMask_USB2MPH = 0x0000E000};
						};
					namespace SYSA5P { // Field Description
						enum {Lsb = 10};
						enum {FieldMask = 0x00001C00};
						enum {Value_TSEC1Tx = 0x0};
						enum {ValueMask_TSEC1Tx = 0x00000000};
						enum {Value_TSEC1Rx = 0x1};
						enum {ValueMask_TSEC1Rx = 0x00000400};
						enum {Value_TSEC1Err = 0x2};
						enum {ValueMask_TSEC1Err = 0x00000800};
						enum {Value_TSEC2Tx = 0x3};
						enum {ValueMask_TSEC2Tx = 0x00000C00};
						enum {Value_TSEC2Rx = 0x4};
						enum {ValueMask_TSEC2Rx = 0x00001000};
						enum {Value_TSEC2Err = 0x5};
						enum {ValueMask_TSEC2Err = 0x00001400};
						enum {Value_USB2DR = 0x6};
						enum {ValueMask_USB2DR = 0x00001800};
						enum {Value_USB2MPH = 0x7};
						enum {ValueMask_USB2MPH = 0x00001C00};
						};
					namespace SYSA6P { // Field Description
						enum {Lsb = 7};
						enum {FieldMask = 0x00000380};
						enum {Value_TSEC1Tx = 0x0};
						enum {ValueMask_TSEC1Tx = 0x00000000};
						enum {Value_TSEC1Rx = 0x1};
						enum {ValueMask_TSEC1Rx = 0x00000080};
						enum {Value_TSEC1Err = 0x2};
						enum {ValueMask_TSEC1Err = 0x00000100};
						enum {Value_TSEC2Tx = 0x3};
						enum {ValueMask_TSEC2Tx = 0x00000180};
						enum {Value_TSEC2Rx = 0x4};
						enum {ValueMask_TSEC2Rx = 0x00000200};
						enum {Value_TSEC2Err = 0x5};
						enum {ValueMask_TSEC2Err = 0x00000280};
						enum {Value_USB2DR = 0x6};
						enum {ValueMask_USB2DR = 0x00000300};
						enum {Value_USB2MPH = 0x7};
						enum {ValueMask_USB2MPH = 0x00000380};
						};
					namespace SYSA7P { // Field Description
						enum {Lsb = 4};
						enum {FieldMask = 0x00000070};
						enum {Value_TSEC1Tx = 0x0};
						enum {ValueMask_TSEC1Tx = 0x00000000};
						enum {Value_TSEC1Rx = 0x1};
						enum {ValueMask_TSEC1Rx = 0x00000010};
						enum {Value_TSEC1Err = 0x2};
						enum {ValueMask_TSEC1Err = 0x00000020};
						enum {Value_TSEC2Tx = 0x3};
						enum {ValueMask_TSEC2Tx = 0x00000030};
						enum {Value_TSEC2Rx = 0x4};
						enum {ValueMask_TSEC2Rx = 0x00000040};
						enum {Value_TSEC2Err = 0x5};
						enum {ValueMask_TSEC2Err = 0x00000050};
						enum {Value_USB2DR = 0x6};
						enum {ValueMask_USB2DR = 0x00000060};
						enum {Value_USB2MPH = 0x7};
						enum {ValueMask_USB2MPH = 0x00000070};
						};
					};
				namespace SIPRR_D { // Register description
					typedef uint32_t	Reg;
					enum {UART1 = 0};
					enum {UART2 = 1};
					enum {SEC = 2};
					enum {I2C1 = 5};
					enum {I2C2 = 6};
					enum {SPI = 7};
					namespace SYSD0P { // Field Description
						enum {Lsb = 29};
						enum {FieldMask = 0xE0000000};
						enum {Value_UART1 = 0x0};
						enum {ValueMask_UART1 = 0x00000000};
						enum {Value_UART2 = 0x1};
						enum {ValueMask_UART2 = 0x20000000};
						enum {Value_SEC = 0x2};
						enum {ValueMask_SEC = 0x40000000};
						enum {Value_I2C1 = 0x5};
						enum {ValueMask_I2C1 = 0xA0000000};
						enum {Value_I2C2 = 0x6};
						enum {ValueMask_I2C2 = 0xC0000000};
						enum {Value_SPI = 0x7};
						enum {ValueMask_SPI = 0xE0000000};
						};
					namespace SYSD1P { // Field Description
						enum {Lsb = 26};
						enum {FieldMask = 0x1C000000};
						enum {Value_UART1 = 0x0};
						enum {ValueMask_UART1 = 0x00000000};
						enum {Value_UART2 = 0x1};
						enum {ValueMask_UART2 = 0x04000000};
						enum {Value_SEC = 0x2};
						enum {ValueMask_SEC = 0x08000000};
						enum {Value_I2C1 = 0x5};
						enum {ValueMask_I2C1 = 0x14000000};
						enum {Value_I2C2 = 0x6};
						enum {ValueMask_I2C2 = 0x18000000};
						enum {Value_SPI = 0x7};
						enum {ValueMask_SPI = 0x1C000000};
						};
					namespace SYSD2P { // Field Description
						enum {Lsb = 23};
						enum {FieldMask = 0x03800000};
						enum {Value_UART1 = 0x0};
						enum {ValueMask_UART1 = 0x00000000};
						enum {Value_UART2 = 0x1};
						enum {ValueMask_UART2 = 0x00800000};
						enum {Value_SEC = 0x2};
						enum {ValueMask_SEC = 0x01000000};
						enum {Value_I2C1 = 0x5};
						enum {ValueMask_I2C1 = 0x02800000};
						enum {Value_I2C2 = 0x6};
						enum {ValueMask_I2C2 = 0x03000000};
						enum {Value_SPI = 0x7};
						enum {ValueMask_SPI = 0x03800000};
						};
					namespace SYSD3P { // Field Description
						enum {Lsb = 20};
						enum {FieldMask = 0x00700000};
						enum {Value_UART1 = 0x0};
						enum {ValueMask_UART1 = 0x00000000};
						enum {Value_UART2 = 0x1};
						enum {ValueMask_UART2 = 0x00100000};
						enum {Value_SEC = 0x2};
						enum {ValueMask_SEC = 0x00200000};
						enum {Value_I2C1 = 0x5};
						enum {ValueMask_I2C1 = 0x00500000};
						enum {Value_I2C2 = 0x6};
						enum {ValueMask_I2C2 = 0x00600000};
						enum {Value_SPI = 0x7};
						enum {ValueMask_SPI = 0x00700000};
						};
					namespace SYSD4P { // Field Description
						enum {Lsb = 13};
						enum {FieldMask = 0x0000E000};
						enum {Value_UART1 = 0x0};
						enum {ValueMask_UART1 = 0x00000000};
						enum {Value_UART2 = 0x1};
						enum {ValueMask_UART2 = 0x00002000};
						enum {Value_SEC = 0x2};
						enum {ValueMask_SEC = 0x00004000};
						enum {Value_I2C1 = 0x5};
						enum {ValueMask_I2C1 = 0x0000A000};
						enum {Value_I2C2 = 0x6};
						enum {ValueMask_I2C2 = 0x0000C000};
						enum {Value_SPI = 0x7};
						enum {ValueMask_SPI = 0x0000E000};
						};
					namespace SYSD5P { // Field Description
						enum {Lsb = 10};
						enum {FieldMask = 0x00001C00};
						enum {Value_UART1 = 0x0};
						enum {ValueMask_UART1 = 0x00000000};
						enum {Value_UART2 = 0x1};
						enum {ValueMask_UART2 = 0x00000400};
						enum {Value_SEC = 0x2};
						enum {ValueMask_SEC = 0x00000800};
						enum {Value_I2C1 = 0x5};
						enum {ValueMask_I2C1 = 0x00001400};
						enum {Value_I2C2 = 0x6};
						enum {ValueMask_I2C2 = 0x00001800};
						enum {Value_SPI = 0x7};
						enum {ValueMask_SPI = 0x00001C00};
						};
					namespace SYSD6P { // Field Description
						enum {Lsb = 7};
						enum {FieldMask = 0x00000380};
						enum {Value_UART1 = 0x0};
						enum {ValueMask_UART1 = 0x00000000};
						enum {Value_UART2 = 0x1};
						enum {ValueMask_UART2 = 0x00000080};
						enum {Value_SEC = 0x2};
						enum {ValueMask_SEC = 0x00000100};
						enum {Value_I2C1 = 0x5};
						enum {ValueMask_I2C1 = 0x00000280};
						enum {Value_I2C2 = 0x6};
						enum {ValueMask_I2C2 = 0x00000300};
						enum {Value_SPI = 0x7};
						enum {ValueMask_SPI = 0x00000380};
						};
					namespace SYSD7P { // Field Description
						enum {Lsb = 4};
						enum {FieldMask = 0x00000070};
						enum {Value_UART1 = 0x0};
						enum {ValueMask_UART1 = 0x00000000};
						enum {Value_UART2 = 0x1};
						enum {ValueMask_UART2 = 0x00000010};
						enum {Value_SEC = 0x2};
						enum {ValueMask_SEC = 0x00000020};
						enum {Value_I2C1 = 0x5};
						enum {ValueMask_I2C1 = 0x00000050};
						enum {Value_I2C2 = 0x6};
						enum {ValueMask_I2C2 = 0x00000060};
						enum {Value_SPI = 0x7};
						enum {ValueMask_SPI = 0x00000070};
						};
					};
				namespace SIMSR_H { // Register description
					typedef uint32_t	Reg;
					enum {Disable = 0};
					enum {Enable = 1};
					enum {Mask = 1};
					namespace TSEC1Tx { // Field Description
						enum {Lsb = 31};
						enum {FieldMask = 0x80000000};
						enum {Value_Disable = 0x0};
						enum {ValueMask_Disable = 0x00000000};
						enum {Value_Enable = 0x1};
						enum {ValueMask_Enable = 0x80000000};
						};
					namespace TSEC1Rx { // Field Description
						enum {Lsb = 30};
						enum {FieldMask = 0x40000000};
						enum {Value_Disable = 0x0};
						enum {ValueMask_Disable = 0x00000000};
						enum {Value_Enable = 0x1};
						enum {ValueMask_Enable = 0x40000000};
						};
					namespace TSEC1Err { // Field Description
						enum {Lsb = 29};
						enum {FieldMask = 0x20000000};
						enum {Value_Disable = 0x0};
						enum {ValueMask_Disable = 0x00000000};
						enum {Value_Enable = 0x1};
						enum {ValueMask_Enable = 0x20000000};
						};
					namespace TSEC2Tx { // Field Description
						enum {Lsb = 28};
						enum {FieldMask = 0x10000000};
						enum {Value_Disable = 0x0};
						enum {ValueMask_Disable = 0x00000000};
						enum {Value_Enable = 0x1};
						enum {ValueMask_Enable = 0x10000000};
						};
					namespace TSEC2Rx { // Field Description
						enum {Lsb = 27};
						enum {FieldMask = 0x08000000};
						enum {Value_Disable = 0x0};
						enum {ValueMask_Disable = 0x00000000};
						enum {Value_Enable = 0x1};
						enum {ValueMask_Enable = 0x08000000};
						};
					namespace TSEC2Err { // Field Description
						enum {Lsb = 26};
						enum {FieldMask = 0x04000000};
						enum {Value_Disable = 0x0};
						enum {ValueMask_Disable = 0x00000000};
						enum {Value_Enable = 0x1};
						enum {ValueMask_Enable = 0x04000000};
						};
					namespace USB2DR { // Field Description
						enum {Lsb = 25};
						enum {FieldMask = 0x02000000};
						enum {Value_Disable = 0x0};
						enum {ValueMask_Disable = 0x00000000};
						enum {Value_Enable = 0x1};
						enum {ValueMask_Enable = 0x02000000};
						};
					namespace USB2MPH { // Field Description
						enum {Lsb = 24};
						enum {FieldMask = 0x01000000};
						enum {Value_Disable = 0x0};
						enum {ValueMask_Disable = 0x00000000};
						enum {Value_Enable = 0x1};
						enum {ValueMask_Enable = 0x01000000};
						};
					namespace UART1 { // Field Description
						enum {Lsb = 7};
						enum {FieldMask = 0x00000080};
						enum {Value_Disable = 0x0};
						enum {ValueMask_Disable = 0x00000000};
						enum {Value_Enable = 0x1};
						enum {ValueMask_Enable = 0x00000080};
						};
					namespace UART2 { // Field Description
						enum {Lsb = 6};
						enum {FieldMask = 0x00000040};
						enum {Value_Disable = 0x0};
						enum {ValueMask_Disable = 0x00000000};
						enum {Value_Enable = 0x1};
						enum {ValueMask_Enable = 0x00000040};
						};
					namespace SEC { // Field Description
						enum {Lsb = 6};
						enum {FieldMask = 0x00000040};
						enum {Value_Disable = 0x0};
						enum {ValueMask_Disable = 0x00000000};
						enum {Value_Enable = 0x1};
						enum {ValueMask_Enable = 0x00000040};
						};
					namespace I2C1 { // Field Description
						enum {Lsb = 2};
						enum {FieldMask = 0x00000004};
						enum {Value_Disable = 0x0};
						enum {ValueMask_Disable = 0x00000000};
						enum {Value_Enable = 0x1};
						enum {ValueMask_Enable = 0x00000004};
						};
					namespace I2C2 { // Field Description
						enum {Lsb = 1};
						enum {FieldMask = 0x00000002};
						enum {Value_Disable = 0x0};
						enum {ValueMask_Disable = 0x00000000};
						enum {Value_Enable = 0x1};
						enum {ValueMask_Enable = 0x00000002};
						};
					namespace SPI { // Field Description
						enum {Lsb = 0};
						enum {FieldMask = 0x00000001};
						enum {Value_Disable = 0x0};
						enum {ValueMask_Disable = 0x00000000};
						enum {Value_Enable = 0x1};
						enum {ValueMask_Enable = 0x00000001};
						};
					};
				namespace SIMSR_L { // Register description
					typedef uint32_t	Reg;
					enum {Mask = 1};
					enum {Enable = 1};
					enum {Disable = 0};
					namespace RTCSEC { // Field Description
						enum {Lsb = 31};
						enum {FieldMask = 0x80000000};
						enum {Value_Disable = 0x0};
						enum {ValueMask_Disable = 0x00000000};
						enum {Value_Enable = 0x1};
						enum {ValueMask_Enable = 0x80000000};
						};
					namespace PIT { // Field Description
						enum {Lsb = 30};
						enum {FieldMask = 0x40000000};
						enum {Value_Disable = 0x0};
						enum {ValueMask_Disable = 0x00000000};
						enum {Value_Enable = 0x1};
						enum {ValueMask_Enable = 0x40000000};
						};
					namespace PCI1 { // Field Description
						enum {Lsb = 29};
						enum {FieldMask = 0x20000000};
						enum {Value_Disable = 0x0};
						enum {ValueMask_Disable = 0x00000000};
						enum {Value_Enable = 0x1};
						enum {ValueMask_Enable = 0x20000000};
						};
					namespace PCI2 { // Field Description
						enum {Lsb = 28};
						enum {FieldMask = 0x10000000};
						enum {Value_Disable = 0x0};
						enum {ValueMask_Disable = 0x00000000};
						enum {Value_Enable = 0x1};
						enum {ValueMask_Enable = 0x10000000};
						};
					namespace RTCALR { // Field Description
						enum {Lsb = 27};
						enum {FieldMask = 0x08000000};
						enum {Value_Disable = 0x0};
						enum {ValueMask_Disable = 0x00000000};
						enum {Value_Enable = 0x1};
						enum {ValueMask_Enable = 0x08000000};
						};
					namespace MU { // Field Description
						enum {Lsb = 26};
						enum {FieldMask = 0x04000000};
						enum {Value_Disable = 0x0};
						enum {ValueMask_Disable = 0x00000000};
						enum {Value_Enable = 0x1};
						enum {ValueMask_Enable = 0x04000000};
						};
					namespace SBA { // Field Description
						enum {Lsb = 25};
						enum {FieldMask = 0x02000000};
						enum {Value_Disable = 0x0};
						enum {ValueMask_Disable = 0x00000000};
						enum {Value_Enable = 0x1};
						enum {ValueMask_Enable = 0x02000000};
						};
					namespace DMA { // Field Description
						enum {Lsb = 24};
						enum {FieldMask = 0x01000000};
						enum {Value_Disable = 0x0};
						enum {ValueMask_Disable = 0x00000000};
						enum {Value_Enable = 0x1};
						enum {ValueMask_Enable = 0x01000000};
						};
					namespace GTM4 { // Field Description
						enum {Lsb = 23};
						enum {FieldMask = 0x00800000};
						enum {Value_Disable = 0x0};
						enum {ValueMask_Disable = 0x00000000};
						enum {Value_Enable = 0x1};
						enum {ValueMask_Enable = 0x00800000};
						};
					namespace GTM8 { // Field Description
						enum {Lsb = 22};
						enum {FieldMask = 0x00400000};
						enum {Value_Disable = 0x0};
						enum {ValueMask_Disable = 0x00000000};
						enum {Value_Enable = 0x1};
						enum {ValueMask_Enable = 0x00400000};
						};
					namespace GPIO1 { // Field Description
						enum {Lsb = 21};
						enum {FieldMask = 0x00200000};
						enum {Value_Disable = 0x0};
						enum {ValueMask_Disable = 0x00000000};
						enum {Value_Enable = 0x1};
						enum {ValueMask_Enable = 0x00200000};
						};
					namespace GPIO2 { // Field Description
						enum {Lsb = 20};
						enum {FieldMask = 0x00100000};
						enum {Value_Disable = 0x0};
						enum {ValueMask_Disable = 0x00000000};
						enum {Value_Enable = 0x1};
						enum {ValueMask_Enable = 0x00100000};
						};
					namespace DDR { // Field Description
						enum {Lsb = 19};
						enum {FieldMask = 0x00080000};
						enum {Value_Disable = 0x0};
						enum {ValueMask_Disable = 0x00000000};
						enum {Value_Enable = 0x1};
						enum {ValueMask_Enable = 0x00080000};
						};
					namespace LBC { // Field Description
						enum {Lsb = 18};
						enum {FieldMask = 0x00040000};
						enum {Value_Disable = 0x0};
						enum {ValueMask_Disable = 0x00000000};
						enum {Value_Enable = 0x1};
						enum {ValueMask_Enable = 0x00040000};
						};
					namespace GTM2 { // Field Description
						enum {Lsb = 17};
						enum {FieldMask = 0x00020000};
						enum {Value_Disable = 0x0};
						enum {ValueMask_Disable = 0x00000000};
						enum {Value_Enable = 0x1};
						enum {ValueMask_Enable = 0x00020000};
						};
					namespace GTM6 { // Field Description
						enum {Lsb = 16};
						enum {FieldMask = 0x00010000};
						enum {Value_Disable = 0x0};
						enum {ValueMask_Disable = 0x00000000};
						enum {Value_Enable = 0x1};
						enum {ValueMask_Enable = 0x00010000};
						};
					namespace PMC { // Field Description
						enum {Lsb = 15};
						enum {FieldMask = 0x00008000};
						enum {Value_Disable = 0x0};
						enum {ValueMask_Disable = 0x00000000};
						enum {Value_Enable = 0x1};
						enum {ValueMask_Enable = 0x00008000};
						};
					namespace GTM3 { // Field Description
						enum {Lsb = 11};
						enum {FieldMask = 0x00000800};
						enum {Value_Disable = 0x0};
						enum {ValueMask_Disable = 0x00000000};
						enum {Value_Enable = 0x1};
						enum {ValueMask_Enable = 0x00000800};
						};
					namespace GTM7 { // Field Description
						enum {Lsb = 10};
						enum {FieldMask = 0x00000400};
						enum {Value_Disable = 0x0};
						enum {ValueMask_Disable = 0x00000000};
						enum {Value_Enable = 0x1};
						enum {ValueMask_Enable = 0x00000400};
						};
					namespace GTM1 { // Field Description
						enum {Lsb = 5};
						enum {FieldMask = 0x00000020};
						enum {Value_Disable = 0x0};
						enum {ValueMask_Disable = 0x00000000};
						enum {Value_Enable = 0x1};
						enum {ValueMask_Enable = 0x00000020};
						};
					namespace GTM5 { // Field Description
						enum {Lsb = 4};
						enum {FieldMask = 0x00000010};
						enum {Value_Disable = 0x0};
						enum {ValueMask_Disable = 0x00000000};
						enum {Value_Enable = 0x1};
						enum {ValueMask_Enable = 0x00000010};
						};
					};
				namespace SICNR { // Register description
					typedef uint32_t	Reg;
					enum {intToCore = 0};
					enum {smiToCore = 1};
					enum {cintToCore = 2};
					namespace SYSD0T { // Field Description
						enum {Lsb = 30};
						enum {FieldMask = 0xC0000000};
						enum {Value_intToCore = 0x0};
						enum {ValueMask_intToCore = 0x00000000};
						enum {Value_smiToCore = 0x1};
						enum {ValueMask_smiToCore = 0x40000000};
						enum {Value_cintToCore = 0x2};
						enum {ValueMask_cintToCore = 0x80000000};
						};
					namespace SYSD1T { // Field Description
						enum {Lsb = 28};
						enum {FieldMask = 0x30000000};
						enum {Value_intToCore = 0x0};
						enum {ValueMask_intToCore = 0x00000000};
						enum {Value_smiToCore = 0x1};
						enum {ValueMask_smiToCore = 0x10000000};
						enum {Value_cintToCore = 0x2};
						enum {ValueMask_cintToCore = 0x20000000};
						};
					namespace SYSA0T { // Field Description
						enum {Lsb = 6};
						enum {FieldMask = 0x000000C0};
						enum {Value_intToCore = 0x0};
						enum {ValueMask_intToCore = 0x00000000};
						enum {Value_smiToCore = 0x1};
						enum {ValueMask_smiToCore = 0x00000040};
						enum {Value_cintToCore = 0x2};
						enum {ValueMask_cintToCore = 0x00000080};
						};
					namespace SYSA1T { // Field Description
						enum {Lsb = 4};
						enum {FieldMask = 0x00000030};
						enum {Value_intToCore = 0x0};
						enum {ValueMask_intToCore = 0x00000000};
						enum {Value_smiToCore = 0x1};
						enum {ValueMask_smiToCore = 0x00000010};
						enum {Value_cintToCore = 0x2};
						enum {ValueMask_cintToCore = 0x00000020};
						};
					};
				namespace SEPNR { // Register description
					typedef uint32_t	Reg;
					enum {Mask = 1};
					enum {Pending = 1};
					enum {Clear = 1};
					namespace IRQ0 { // Field Description
						enum {Lsb = 31};
						enum {FieldMask = 0x80000000};
						enum {Value_Pending = 0x1};
						enum {ValueMask_Pending = 0x80000000};
						enum {Value_Clear = 0x1};
						enum {ValueMask_Clear = 0x80000000};
						};
					namespace IRQ1 { // Field Description
						enum {Lsb = 30};
						enum {FieldMask = 0x40000000};
						enum {Value_Pending = 0x1};
						enum {ValueMask_Pending = 0x40000000};
						enum {Value_Clear = 0x1};
						enum {ValueMask_Clear = 0x40000000};
						};
					namespace IRQ2 { // Field Description
						enum {Lsb = 29};
						enum {FieldMask = 0x20000000};
						enum {Value_Pending = 0x1};
						enum {ValueMask_Pending = 0x20000000};
						enum {Value_Clear = 0x1};
						enum {ValueMask_Clear = 0x20000000};
						};
					namespace IRQ3 { // Field Description
						enum {Lsb = 28};
						enum {FieldMask = 0x10000000};
						enum {Value_Pending = 0x1};
						enum {ValueMask_Pending = 0x10000000};
						enum {Value_Clear = 0x1};
						enum {ValueMask_Clear = 0x10000000};
						};
					namespace IRQ4 { // Field Description
						enum {Lsb = 27};
						enum {FieldMask = 0x08000000};
						enum {Value_Pending = 0x1};
						enum {ValueMask_Pending = 0x08000000};
						enum {Value_Clear = 0x1};
						enum {ValueMask_Clear = 0x08000000};
						};
					namespace IRQ5 { // Field Description
						enum {Lsb = 26};
						enum {FieldMask = 0x04000000};
						enum {Value_Pending = 0x1};
						enum {ValueMask_Pending = 0x04000000};
						enum {Value_Clear = 0x1};
						enum {ValueMask_Clear = 0x04000000};
						};
					namespace IRQ6 { // Field Description
						enum {Lsb = 25};
						enum {FieldMask = 0x02000000};
						enum {Value_Pending = 0x1};
						enum {ValueMask_Pending = 0x02000000};
						enum {Value_Clear = 0x1};
						enum {ValueMask_Clear = 0x02000000};
						};
					namespace IRQ7 { // Field Description
						enum {Lsb = 24};
						enum {FieldMask = 0x01000000};
						enum {Value_Pending = 0x1};
						enum {ValueMask_Pending = 0x01000000};
						enum {Value_Clear = 0x1};
						enum {ValueMask_Clear = 0x01000000};
						};
					};
				namespace SMPRR_A { // Register description
					typedef uint32_t	Reg;
					enum {RTCSEC = 0};
					enum {PIT = 1};
					enum {PCI1 = 2};
					enum {PCI2 = 3};
					enum {IRQ0 = 4};
					enum {IRQ1 = 5};
					enum {IRQ2 = 6};
					enum {IRQ3 = 7};
					namespace MIXA0P { // Field Description
						enum {Lsb = 29};
						enum {FieldMask = 0xE0000000};
						enum {Value_RTCSEC = 0x0};
						enum {ValueMask_RTCSEC = 0x00000000};
						enum {Value_PIT = 0x1};
						enum {ValueMask_PIT = 0x20000000};
						enum {Value_PCI1 = 0x2};
						enum {ValueMask_PCI1 = 0x40000000};
						enum {Value_PCI2 = 0x3};
						enum {ValueMask_PCI2 = 0x60000000};
						enum {Value_IRQ0 = 0x4};
						enum {ValueMask_IRQ0 = 0x80000000};
						enum {Value_IRQ1 = 0x5};
						enum {ValueMask_IRQ1 = 0xA0000000};
						enum {Value_IRQ2 = 0x6};
						enum {ValueMask_IRQ2 = 0xC0000000};
						enum {Value_IRQ3 = 0x7};
						enum {ValueMask_IRQ3 = 0xE0000000};
						};
					namespace MIXA1P { // Field Description
						enum {Lsb = 26};
						enum {FieldMask = 0x1C000000};
						enum {Value_RTCSEC = 0x0};
						enum {ValueMask_RTCSEC = 0x00000000};
						enum {Value_PIT = 0x1};
						enum {ValueMask_PIT = 0x04000000};
						enum {Value_PCI1 = 0x2};
						enum {ValueMask_PCI1 = 0x08000000};
						enum {Value_PCI2 = 0x3};
						enum {ValueMask_PCI2 = 0x0C000000};
						enum {Value_IRQ0 = 0x4};
						enum {ValueMask_IRQ0 = 0x10000000};
						enum {Value_IRQ1 = 0x5};
						enum {ValueMask_IRQ1 = 0x14000000};
						enum {Value_IRQ2 = 0x6};
						enum {ValueMask_IRQ2 = 0x18000000};
						enum {Value_IRQ3 = 0x7};
						enum {ValueMask_IRQ3 = 0x1C000000};
						};
					namespace MIXA2P { // Field Description
						enum {Lsb = 23};
						enum {FieldMask = 0x03800000};
						enum {Value_RTCSEC = 0x0};
						enum {ValueMask_RTCSEC = 0x00000000};
						enum {Value_PIT = 0x1};
						enum {ValueMask_PIT = 0x00800000};
						enum {Value_PCI1 = 0x2};
						enum {ValueMask_PCI1 = 0x01000000};
						enum {Value_PCI2 = 0x3};
						enum {ValueMask_PCI2 = 0x01800000};
						enum {Value_IRQ0 = 0x4};
						enum {ValueMask_IRQ0 = 0x02000000};
						enum {Value_IRQ1 = 0x5};
						enum {ValueMask_IRQ1 = 0x02800000};
						enum {Value_IRQ2 = 0x6};
						enum {ValueMask_IRQ2 = 0x03000000};
						enum {Value_IRQ3 = 0x7};
						enum {ValueMask_IRQ3 = 0x03800000};
						};
					namespace MIXA3P { // Field Description
						enum {Lsb = 20};
						enum {FieldMask = 0x00700000};
						enum {Value_RTCSEC = 0x0};
						enum {ValueMask_RTCSEC = 0x00000000};
						enum {Value_PIT = 0x1};
						enum {ValueMask_PIT = 0x00100000};
						enum {Value_PCI1 = 0x2};
						enum {ValueMask_PCI1 = 0x00200000};
						enum {Value_PCI2 = 0x3};
						enum {ValueMask_PCI2 = 0x00300000};
						enum {Value_IRQ0 = 0x4};
						enum {ValueMask_IRQ0 = 0x00400000};
						enum {Value_IRQ1 = 0x5};
						enum {ValueMask_IRQ1 = 0x00500000};
						enum {Value_IRQ2 = 0x6};
						enum {ValueMask_IRQ2 = 0x00600000};
						enum {Value_IRQ3 = 0x7};
						enum {ValueMask_IRQ3 = 0x00700000};
						};
					namespace MIXA4P { // Field Description
						enum {Lsb = 13};
						enum {FieldMask = 0x0000E000};
						enum {Value_RTCSEC = 0x0};
						enum {ValueMask_RTCSEC = 0x00000000};
						enum {Value_PIT = 0x1};
						enum {ValueMask_PIT = 0x00002000};
						enum {Value_PCI1 = 0x2};
						enum {ValueMask_PCI1 = 0x00004000};
						enum {Value_PCI2 = 0x3};
						enum {ValueMask_PCI2 = 0x00006000};
						enum {Value_IRQ0 = 0x4};
						enum {ValueMask_IRQ0 = 0x00008000};
						enum {Value_IRQ1 = 0x5};
						enum {ValueMask_IRQ1 = 0x0000A000};
						enum {Value_IRQ2 = 0x6};
						enum {ValueMask_IRQ2 = 0x0000C000};
						enum {Value_IRQ3 = 0x7};
						enum {ValueMask_IRQ3 = 0x0000E000};
						};
					namespace MIXA5P { // Field Description
						enum {Lsb = 10};
						enum {FieldMask = 0x00001C00};
						enum {Value_RTCSEC = 0x0};
						enum {ValueMask_RTCSEC = 0x00000000};
						enum {Value_PIT = 0x1};
						enum {ValueMask_PIT = 0x00000400};
						enum {Value_PCI1 = 0x2};
						enum {ValueMask_PCI1 = 0x00000800};
						enum {Value_PCI2 = 0x3};
						enum {ValueMask_PCI2 = 0x00000C00};
						enum {Value_IRQ0 = 0x4};
						enum {ValueMask_IRQ0 = 0x00001000};
						enum {Value_IRQ1 = 0x5};
						enum {ValueMask_IRQ1 = 0x00001400};
						enum {Value_IRQ2 = 0x6};
						enum {ValueMask_IRQ2 = 0x00001800};
						enum {Value_IRQ3 = 0x7};
						enum {ValueMask_IRQ3 = 0x00001C00};
						};
					namespace MIXA6P { // Field Description
						enum {Lsb = 7};
						enum {FieldMask = 0x00000380};
						enum {Value_RTCSEC = 0x0};
						enum {ValueMask_RTCSEC = 0x00000000};
						enum {Value_PIT = 0x1};
						enum {ValueMask_PIT = 0x00000080};
						enum {Value_PCI1 = 0x2};
						enum {ValueMask_PCI1 = 0x00000100};
						enum {Value_PCI2 = 0x3};
						enum {ValueMask_PCI2 = 0x00000180};
						enum {Value_IRQ0 = 0x4};
						enum {ValueMask_IRQ0 = 0x00000200};
						enum {Value_IRQ1 = 0x5};
						enum {ValueMask_IRQ1 = 0x00000280};
						enum {Value_IRQ2 = 0x6};
						enum {ValueMask_IRQ2 = 0x00000300};
						enum {Value_IRQ3 = 0x7};
						enum {ValueMask_IRQ3 = 0x00000380};
						};
					};
				namespace SMPRR_B { // Register description
					typedef uint32_t	Reg;
					enum {RTCALR = 0};
					enum {MU = 1};
					enum {SBA = 2};
					enum {DMA = 3};
					enum {IRQ4 = 4};
					enum {IRQ5 = 5};
					enum {IRQ6 = 6};
					enum {IRQ7 = 7};
					namespace MIXB0P { // Field Description
						enum {Lsb = 29};
						enum {FieldMask = 0xE0000000};
						enum {Value_RTCALR = 0x0};
						enum {ValueMask_RTCALR = 0x00000000};
						enum {Value_MU = 0x1};
						enum {ValueMask_MU = 0x20000000};
						enum {Value_SBA = 0x2};
						enum {ValueMask_SBA = 0x40000000};
						enum {Value_DMA = 0x3};
						enum {ValueMask_DMA = 0x60000000};
						enum {Value_IRQ4 = 0x4};
						enum {ValueMask_IRQ4 = 0x80000000};
						enum {Value_IRQ5 = 0x5};
						enum {ValueMask_IRQ5 = 0xA0000000};
						enum {Value_IRQ6 = 0x6};
						enum {ValueMask_IRQ6 = 0xC0000000};
						enum {Value_IRQ7 = 0x7};
						enum {ValueMask_IRQ7 = 0xE0000000};
						};
					namespace MIXB1P { // Field Description
						enum {Lsb = 26};
						enum {FieldMask = 0x1C000000};
						enum {Value_RTCALR = 0x0};
						enum {ValueMask_RTCALR = 0x00000000};
						enum {Value_MU = 0x1};
						enum {ValueMask_MU = 0x04000000};
						enum {Value_SBA = 0x2};
						enum {ValueMask_SBA = 0x08000000};
						enum {Value_DMA = 0x3};
						enum {ValueMask_DMA = 0x0C000000};
						enum {Value_IRQ4 = 0x4};
						enum {ValueMask_IRQ4 = 0x10000000};
						enum {Value_IRQ5 = 0x5};
						enum {ValueMask_IRQ5 = 0x14000000};
						enum {Value_IRQ6 = 0x6};
						enum {ValueMask_IRQ6 = 0x18000000};
						enum {Value_IRQ7 = 0x7};
						enum {ValueMask_IRQ7 = 0x1C000000};
						};
					namespace MIXB2P { // Field Description
						enum {Lsb = 23};
						enum {FieldMask = 0x03800000};
						enum {Value_RTCALR = 0x0};
						enum {ValueMask_RTCALR = 0x00000000};
						enum {Value_MU = 0x1};
						enum {ValueMask_MU = 0x00800000};
						enum {Value_SBA = 0x2};
						enum {ValueMask_SBA = 0x01000000};
						enum {Value_DMA = 0x3};
						enum {ValueMask_DMA = 0x01800000};
						enum {Value_IRQ4 = 0x4};
						enum {ValueMask_IRQ4 = 0x02000000};
						enum {Value_IRQ5 = 0x5};
						enum {ValueMask_IRQ5 = 0x02800000};
						enum {Value_IRQ6 = 0x6};
						enum {ValueMask_IRQ6 = 0x03000000};
						enum {Value_IRQ7 = 0x7};
						enum {ValueMask_IRQ7 = 0x03800000};
						};
					namespace MIXB3P { // Field Description
						enum {Lsb = 20};
						enum {FieldMask = 0x00700000};
						enum {Value_RTCALR = 0x0};
						enum {ValueMask_RTCALR = 0x00000000};
						enum {Value_MU = 0x1};
						enum {ValueMask_MU = 0x00100000};
						enum {Value_SBA = 0x2};
						enum {ValueMask_SBA = 0x00200000};
						enum {Value_DMA = 0x3};
						enum {ValueMask_DMA = 0x00300000};
						enum {Value_IRQ4 = 0x4};
						enum {ValueMask_IRQ4 = 0x00400000};
						enum {Value_IRQ5 = 0x5};
						enum {ValueMask_IRQ5 = 0x00500000};
						enum {Value_IRQ6 = 0x6};
						enum {ValueMask_IRQ6 = 0x00600000};
						enum {Value_IRQ7 = 0x7};
						enum {ValueMask_IRQ7 = 0x00700000};
						};
					namespace MIXB4P { // Field Description
						enum {Lsb = 13};
						enum {FieldMask = 0x0000E000};
						enum {Value_RTCALR = 0x0};
						enum {ValueMask_RTCALR = 0x00000000};
						enum {Value_MU = 0x1};
						enum {ValueMask_MU = 0x00002000};
						enum {Value_SBA = 0x2};
						enum {ValueMask_SBA = 0x00004000};
						enum {Value_DMA = 0x3};
						enum {ValueMask_DMA = 0x00006000};
						enum {Value_IRQ4 = 0x4};
						enum {ValueMask_IRQ4 = 0x00008000};
						enum {Value_IRQ5 = 0x5};
						enum {ValueMask_IRQ5 = 0x0000A000};
						enum {Value_IRQ6 = 0x6};
						enum {ValueMask_IRQ6 = 0x0000C000};
						enum {Value_IRQ7 = 0x7};
						enum {ValueMask_IRQ7 = 0x0000E000};
						};
					namespace MIXB5P { // Field Description
						enum {Lsb = 10};
						enum {FieldMask = 0x00001C00};
						enum {Value_RTCALR = 0x0};
						enum {ValueMask_RTCALR = 0x00000000};
						enum {Value_MU = 0x1};
						enum {ValueMask_MU = 0x00000400};
						enum {Value_SBA = 0x2};
						enum {ValueMask_SBA = 0x00000800};
						enum {Value_DMA = 0x3};
						enum {ValueMask_DMA = 0x00000C00};
						enum {Value_IRQ4 = 0x4};
						enum {ValueMask_IRQ4 = 0x00001000};
						enum {Value_IRQ5 = 0x5};
						enum {ValueMask_IRQ5 = 0x00001400};
						enum {Value_IRQ6 = 0x6};
						enum {ValueMask_IRQ6 = 0x00001800};
						enum {Value_IRQ7 = 0x7};
						enum {ValueMask_IRQ7 = 0x00001C00};
						};
					namespace MIXB6P { // Field Description
						enum {Lsb = 7};
						enum {FieldMask = 0x00000380};
						enum {Value_RTCALR = 0x0};
						enum {ValueMask_RTCALR = 0x00000000};
						enum {Value_MU = 0x1};
						enum {ValueMask_MU = 0x00000080};
						enum {Value_SBA = 0x2};
						enum {ValueMask_SBA = 0x00000100};
						enum {Value_DMA = 0x3};
						enum {ValueMask_DMA = 0x00000180};
						enum {Value_IRQ4 = 0x4};
						enum {ValueMask_IRQ4 = 0x00000200};
						enum {Value_IRQ5 = 0x5};
						enum {ValueMask_IRQ5 = 0x00000280};
						enum {Value_IRQ6 = 0x6};
						enum {ValueMask_IRQ6 = 0x00000300};
						enum {Value_IRQ7 = 0x7};
						enum {ValueMask_IRQ7 = 0x00000380};
						};
					namespace MIXB7P { // Field Description
						enum {Lsb = 4};
						enum {FieldMask = 0x00000070};
						enum {Value_RTCALR = 0x0};
						enum {ValueMask_RTCALR = 0x00000000};
						enum {Value_MU = 0x1};
						enum {ValueMask_MU = 0x00000010};
						enum {Value_SBA = 0x2};
						enum {ValueMask_SBA = 0x00000020};
						enum {Value_DMA = 0x3};
						enum {ValueMask_DMA = 0x00000030};
						enum {Value_IRQ4 = 0x4};
						enum {ValueMask_IRQ4 = 0x00000040};
						enum {Value_IRQ5 = 0x5};
						enum {ValueMask_IRQ5 = 0x00000050};
						enum {Value_IRQ6 = 0x6};
						enum {ValueMask_IRQ6 = 0x00000060};
						enum {Value_IRQ7 = 0x7};
						enum {ValueMask_IRQ7 = 0x00000070};
						};
					};
				namespace SEMSR { // Register description
					typedef uint32_t	Reg;
					enum {Mask = 1};
					enum {Enable = 1};
					enum {Disable = 0};
					namespace IRQ0 { // Field Description
						enum {Lsb = 31};
						enum {FieldMask = 0x80000000};
						enum {Value_Disable = 0x0};
						enum {ValueMask_Disable = 0x00000000};
						enum {Value_Enable = 0x1};
						enum {ValueMask_Enable = 0x80000000};
						};
					namespace IRQ1 { // Field Description
						enum {Lsb = 30};
						enum {FieldMask = 0x40000000};
						enum {Value_Disable = 0x0};
						enum {ValueMask_Disable = 0x00000000};
						enum {Value_Enable = 0x1};
						enum {ValueMask_Enable = 0x40000000};
						};
					namespace IRQ2 { // Field Description
						enum {Lsb = 29};
						enum {FieldMask = 0x20000000};
						enum {Value_Disable = 0x0};
						enum {ValueMask_Disable = 0x00000000};
						enum {Value_Enable = 0x1};
						enum {ValueMask_Enable = 0x20000000};
						};
					namespace IRQ3 { // Field Description
						enum {Lsb = 28};
						enum {FieldMask = 0x10000000};
						enum {Value_Disable = 0x0};
						enum {ValueMask_Disable = 0x00000000};
						enum {Value_Enable = 0x1};
						enum {ValueMask_Enable = 0x10000000};
						};
					namespace IRQ4 { // Field Description
						enum {Lsb = 27};
						enum {FieldMask = 0x08000000};
						enum {Value_Disable = 0x0};
						enum {ValueMask_Disable = 0x00000000};
						enum {Value_Enable = 0x1};
						enum {ValueMask_Enable = 0x08000000};
						};
					namespace IRQ5 { // Field Description
						enum {Lsb = 26};
						enum {FieldMask = 0x04000000};
						enum {Value_Disable = 0x0};
						enum {ValueMask_Disable = 0x00000000};
						enum {Value_Enable = 0x1};
						enum {ValueMask_Enable = 0x04000000};
						};
					namespace IRQ6 { // Field Description
						enum {Lsb = 25};
						enum {FieldMask = 0x02000000};
						enum {Value_Disable = 0x0};
						enum {ValueMask_Disable = 0x00000000};
						enum {Value_Enable = 0x1};
						enum {ValueMask_Enable = 0x02000000};
						};
					namespace IRQ7 { // Field Description
						enum {Lsb = 24};
						enum {FieldMask = 0x01000000};
						enum {Value_Disable = 0x0};
						enum {ValueMask_Disable = 0x00000000};
						enum {Value_Enable = 0x1};
						enum {ValueMask_Enable = 0x01000000};
						};
					namespace SIRQ0 { // Field Description
						enum {Lsb = 15};
						enum {FieldMask = 0x00008000};
						enum {Value_IRQ0ExternalInterrupt = 0x0};
						enum {ValueMask_IRQ0ExternalInterrupt = 0x00000000};
						enum {Value_IRQ0ExternalMCP = 0x1};
						enum {ValueMask_IRQ0ExternalMCP = 0x00008000};
						};
					};
				namespace SECNR { // Register description
					typedef uint32_t	Reg;
					namespace MIXB0T { // Field Description
						enum {Lsb = 30};
						enum {FieldMask = 0xC0000000};
						enum {Value_intToCore = 0x0};
						enum {ValueMask_intToCore = 0x00000000};
						enum {Value_smiToCore = 0x1};
						enum {ValueMask_smiToCore = 0x40000000};
						enum {Value_cintToCore = 0x2};
						enum {ValueMask_cintToCore = 0x80000000};
						};
					namespace MIXB1T { // Field Description
						enum {Lsb = 28};
						enum {FieldMask = 0x30000000};
						enum {Value_intToCore = 0x0};
						enum {ValueMask_intToCore = 0x00000000};
						enum {Value_smiToCore = 0x1};
						enum {ValueMask_smiToCore = 0x10000000};
						enum {Value_cintToCore = 0x2};
						enum {ValueMask_cintToCore = 0x20000000};
						};
					namespace MIXA0T { // Field Description
						enum {Lsb = 22};
						enum {FieldMask = 0x00C00000};
						enum {Value_intToCore = 0x0};
						enum {ValueMask_intToCore = 0x00000000};
						enum {Value_smiToCore = 0x1};
						enum {ValueMask_smiToCore = 0x00400000};
						enum {Value_cintToCore = 0x2};
						enum {ValueMask_cintToCore = 0x00800000};
						};
					namespace MIXA1T { // Field Description
						enum {Lsb = 20};
						enum {FieldMask = 0x00300000};
						enum {Value_intToCore = 0x0};
						enum {ValueMask_intToCore = 0x00000000};
						enum {Value_smiToCore = 0x1};
						enum {ValueMask_smiToCore = 0x00100000};
						enum {Value_cintToCore = 0x2};
						enum {ValueMask_cintToCore = 0x00200000};
						};
					namespace EDI0 { // Field Description
						enum {Lsb = 15};
						enum {FieldMask = 0x00008000};
						enum {Value_LowLevel = 0x0};
						enum {ValueMask_LowLevel = 0x00000000};
						enum {Value_HighToLowEdge = 0x1};
						enum {ValueMask_HighToLowEdge = 0x00008000};
						};
					namespace EDI1 { // Field Description
						enum {Lsb = 14};
						enum {FieldMask = 0x00004000};
						enum {Value_LowLevel = 0x0};
						enum {ValueMask_LowLevel = 0x00000000};
						enum {Value_HighToLowEdge = 0x1};
						enum {ValueMask_HighToLowEdge = 0x00004000};
						};
					namespace EDI2 { // Field Description
						enum {Lsb = 13};
						enum {FieldMask = 0x00002000};
						enum {Value_LowLevel = 0x0};
						enum {ValueMask_LowLevel = 0x00000000};
						enum {Value_HighToLowEdge = 0x1};
						enum {ValueMask_HighToLowEdge = 0x00002000};
						};
					namespace EDI3 { // Field Description
						enum {Lsb = 12};
						enum {FieldMask = 0x00001000};
						enum {Value_LowLevel = 0x0};
						enum {ValueMask_LowLevel = 0x00000000};
						enum {Value_HighToLowEdge = 0x1};
						enum {ValueMask_HighToLowEdge = 0x00001000};
						};
					namespace EDI4 { // Field Description
						enum {Lsb = 11};
						enum {FieldMask = 0x00000800};
						enum {Value_LowLevel = 0x0};
						enum {ValueMask_LowLevel = 0x00000000};
						enum {Value_HighToLowEdge = 0x1};
						enum {ValueMask_HighToLowEdge = 0x00000800};
						};
					namespace EDI5 { // Field Description
						enum {Lsb = 10};
						enum {FieldMask = 0x00000400};
						enum {Value_LowLevel = 0x0};
						enum {ValueMask_LowLevel = 0x00000000};
						enum {Value_HighToLowEdge = 0x1};
						enum {ValueMask_HighToLowEdge = 0x00000400};
						};
					namespace EDI6 { // Field Description
						enum {Lsb = 9};
						enum {FieldMask = 0x00000200};
						enum {Value_LowLevel = 0x0};
						enum {ValueMask_LowLevel = 0x00000000};
						enum {Value_HighToLowEdge = 0x1};
						enum {ValueMask_HighToLowEdge = 0x00000200};
						};
					namespace EDI7 { // Field Description
						enum {Lsb = 8};
						enum {FieldMask = 0x00000100};
						enum {Value_LowLevel = 0x0};
						enum {ValueMask_LowLevel = 0x00000000};
						enum {Value_HighToLowEdge = 0x1};
						enum {ValueMask_HighToLowEdge = 0x00000100};
						};
					};
				namespace SERSR { // Register description
					typedef uint32_t	Reg;
					namespace IRQ0 { // Field Description
						enum {Lsb = 31};
						enum {FieldMask = 0x80000000};
						enum {Value_ErrorPending = 0x1};
						enum {ValueMask_ErrorPending = 0x80000000};
						enum {Value_Clear = 0x1};
						enum {ValueMask_Clear = 0x80000000};
						};
					namespace WDT { // Field Description
						enum {Lsb = 30};
						enum {FieldMask = 0x40000000};
						enum {Value_ErrorPending = 0x1};
						enum {ValueMask_ErrorPending = 0x40000000};
						enum {Value_Clear = 0x1};
						enum {ValueMask_Clear = 0x40000000};
						};
					namespace SBA { // Field Description
						enum {Lsb = 29};
						enum {FieldMask = 0x20000000};
						enum {Value_ErrorPending = 0x1};
						enum {ValueMask_ErrorPending = 0x20000000};
						enum {Value_Clear = 0x1};
						enum {ValueMask_Clear = 0x20000000};
						};
					namespace PCI1 { // Field Description
						enum {Lsb = 26};
						enum {FieldMask = 0x04000000};
						enum {Value_ErrorPending = 0x1};
						enum {ValueMask_ErrorPending = 0x04000000};
						enum {Value_Clear = 0x1};
						enum {ValueMask_Clear = 0x04000000};
						};
					namespace PCI2 { // Field Description
						enum {Lsb = 25};
						enum {FieldMask = 0x02000000};
						enum {Value_ErrorPending = 0x1};
						enum {ValueMask_ErrorPending = 0x02000000};
						enum {Value_Clear = 0x1};
						enum {ValueMask_Clear = 0x02000000};
						};
					namespace MU { // Field Description
						enum {Lsb = 24};
						enum {FieldMask = 0x01000000};
						enum {Value_ErrorPending = 0x1};
						enum {ValueMask_ErrorPending = 0x01000000};
						enum {Value_Clear = 0x1};
						enum {ValueMask_Clear = 0x01000000};
						};
					};
				namespace SERMR { // Register description
					typedef uint32_t	Reg;
					namespace IRQ0 { // Field Description
						enum {Lsb = 31};
						enum {FieldMask = 0x80000000};
						enum {Value_Disable = 0x0};
						enum {ValueMask_Disable = 0x00000000};
						enum {Value_Enable = 0x1};
						enum {ValueMask_Enable = 0x80000000};
						};
					namespace WDT { // Field Description
						enum {Lsb = 30};
						enum {FieldMask = 0x40000000};
						enum {Value_Disable = 0x0};
						enum {ValueMask_Disable = 0x00000000};
						enum {Value_Enable = 0x1};
						enum {ValueMask_Enable = 0x40000000};
						};
					namespace SBA { // Field Description
						enum {Lsb = 29};
						enum {FieldMask = 0x20000000};
						enum {Value_Disable = 0x0};
						enum {ValueMask_Disable = 0x00000000};
						enum {Value_Enable = 0x1};
						enum {ValueMask_Enable = 0x20000000};
						};
					namespace PCI1 { // Field Description
						enum {Lsb = 26};
						enum {FieldMask = 0x04000000};
						enum {Value_Disable = 0x0};
						enum {ValueMask_Disable = 0x00000000};
						enum {Value_Enable = 0x1};
						enum {ValueMask_Enable = 0x04000000};
						};
					namespace PCI2 { // Field Description
						enum {Lsb = 25};
						enum {FieldMask = 0x02000000};
						enum {Value_Disable = 0x0};
						enum {ValueMask_Disable = 0x00000000};
						enum {Value_Enable = 0x1};
						enum {ValueMask_Enable = 0x02000000};
						};
					namespace MU { // Field Description
						enum {Lsb = 24};
						enum {FieldMask = 0x01000000};
						enum {Value_Disable = 0x0};
						enum {ValueMask_Disable = 0x00000000};
						enum {Value_Enable = 0x1};
						enum {ValueMask_Enable = 0x01000000};
						};
					};
				namespace SERCR { // Register description
					typedef uint32_t	Reg;
					namespace MCPR { // Field Description
						enum {Lsb = 0};
						enum {FieldMask = 0x00000001};
						enum {Value_routeMcpToINTA = 0x0};
						enum {ValueMask_routeMcpToINTA = 0x00000000};
						enum {Value_routeMcpToMCP_OUT = 0x1};
						enum {ValueMask_routeMcpToMCP_OUT = 0x00000001};
						};
					};
				namespace SIFCR_H { // Register description
					typedef uint32_t	Reg;
					namespace TSEC1Tx { // Field Description
						enum {Lsb = 31};
						enum {FieldMask = 0x80000000};
						enum {Value_ClearInterrupt = 0x0};
						enum {ValueMask_ClearInterrupt = 0x00000000};
						enum {Value_ForceInterrupt = 0x1};
						enum {ValueMask_ForceInterrupt = 0x80000000};
						};
					namespace TSEC1Rx { // Field Description
						enum {Lsb = 30};
						enum {FieldMask = 0x40000000};
						enum {Value_ClearInterrupt = 0x0};
						enum {ValueMask_ClearInterrupt = 0x00000000};
						enum {Value_ForceInterrupt = 0x1};
						enum {ValueMask_ForceInterrupt = 0x40000000};
						};
					namespace TSEC1Err { // Field Description
						enum {Lsb = 29};
						enum {FieldMask = 0x20000000};
						enum {Value_ClearInterrupt = 0x0};
						enum {ValueMask_ClearInterrupt = 0x00000000};
						enum {Value_ForceInterrupt = 0x1};
						enum {ValueMask_ForceInterrupt = 0x20000000};
						};
					namespace TSEC2Tx { // Field Description
						enum {Lsb = 28};
						enum {FieldMask = 0x10000000};
						enum {Value_ClearInterrupt = 0x0};
						enum {ValueMask_ClearInterrupt = 0x00000000};
						enum {Value_ForceInterrupt = 0x1};
						enum {ValueMask_ForceInterrupt = 0x10000000};
						};
					namespace TSEC2Rx { // Field Description
						enum {Lsb = 27};
						enum {FieldMask = 0x08000000};
						enum {Value_ClearInterrupt = 0x0};
						enum {ValueMask_ClearInterrupt = 0x00000000};
						enum {Value_ForceInterrupt = 0x1};
						enum {ValueMask_ForceInterrupt = 0x08000000};
						};
					namespace TSEC2Err { // Field Description
						enum {Lsb = 26};
						enum {FieldMask = 0x04000000};
						enum {Value_ClearInterrupt = 0x0};
						enum {ValueMask_ClearInterrupt = 0x00000000};
						enum {Value_ForceInterrupt = 0x1};
						enum {ValueMask_ForceInterrupt = 0x04000000};
						};
					namespace USB2DR { // Field Description
						enum {Lsb = 25};
						enum {FieldMask = 0x02000000};
						enum {Value_ClearInterrupt = 0x0};
						enum {ValueMask_ClearInterrupt = 0x00000000};
						enum {Value_ForceInterrupt = 0x1};
						enum {ValueMask_ForceInterrupt = 0x02000000};
						};
					namespace USB2MPH { // Field Description
						enum {Lsb = 24};
						enum {FieldMask = 0x01000000};
						enum {Value_ClearInterrupt = 0x0};
						enum {ValueMask_ClearInterrupt = 0x00000000};
						enum {Value_ForceInterrupt = 0x1};
						enum {ValueMask_ForceInterrupt = 0x01000000};
						};
					};
				namespace SIFCR_L { // Register description
					typedef uint32_t	Reg;
					namespace RTCSEC { // Field Description
						enum {Lsb = 31};
						enum {FieldMask = 0x80000000};
						enum {Value_ClearInterrupt = 0x0};
						enum {ValueMask_ClearInterrupt = 0x00000000};
						enum {Value_ForceInterrupt = 0x1};
						enum {ValueMask_ForceInterrupt = 0x80000000};
						};
					namespace PIT { // Field Description
						enum {Lsb = 30};
						enum {FieldMask = 0x40000000};
						enum {Value_ClearInterrupt = 0x0};
						enum {ValueMask_ClearInterrupt = 0x00000000};
						enum {Value_ForceInterrupt = 0x1};
						enum {ValueMask_ForceInterrupt = 0x40000000};
						};
					namespace PCI1 { // Field Description
						enum {Lsb = 29};
						enum {FieldMask = 0x20000000};
						enum {Value_ClearInterrupt = 0x0};
						enum {ValueMask_ClearInterrupt = 0x00000000};
						enum {Value_ForceInterrupt = 0x1};
						enum {ValueMask_ForceInterrupt = 0x20000000};
						};
					namespace PCI2 { // Field Description
						enum {Lsb = 28};
						enum {FieldMask = 0x10000000};
						enum {Value_ClearInterrupt = 0x0};
						enum {ValueMask_ClearInterrupt = 0x00000000};
						enum {Value_ForceInterrupt = 0x1};
						enum {ValueMask_ForceInterrupt = 0x10000000};
						};
					namespace RTCALR { // Field Description
						enum {Lsb = 27};
						enum {FieldMask = 0x08000000};
						enum {Value_ClearInterrupt = 0x0};
						enum {ValueMask_ClearInterrupt = 0x00000000};
						enum {Value_ForceInterrupt = 0x1};
						enum {ValueMask_ForceInterrupt = 0x08000000};
						};
					namespace MU { // Field Description
						enum {Lsb = 26};
						enum {FieldMask = 0x04000000};
						enum {Value_ClearInterrupt = 0x0};
						enum {ValueMask_ClearInterrupt = 0x00000000};
						enum {Value_ForceInterrupt = 0x1};
						enum {ValueMask_ForceInterrupt = 0x04000000};
						};
					namespace SBA { // Field Description
						enum {Lsb = 25};
						enum {FieldMask = 0x02000000};
						enum {Value_ClearInterrupt = 0x0};
						enum {ValueMask_ClearInterrupt = 0x00000000};
						enum {Value_ForceInterrupt = 0x1};
						enum {ValueMask_ForceInterrupt = 0x02000000};
						};
					namespace DMA { // Field Description
						enum {Lsb = 24};
						enum {FieldMask = 0x01000000};
						enum {Value_ClearInterrupt = 0x0};
						enum {ValueMask_ClearInterrupt = 0x00000000};
						enum {Value_ForceInterrupt = 0x1};
						enum {ValueMask_ForceInterrupt = 0x01000000};
						};
					namespace GTM4 { // Field Description
						enum {Lsb = 23};
						enum {FieldMask = 0x00800000};
						enum {Value_ClearInterrupt = 0x0};
						enum {ValueMask_ClearInterrupt = 0x00000000};
						enum {Value_ForceInterrupt = 0x1};
						enum {ValueMask_ForceInterrupt = 0x00800000};
						};
					namespace GTM8 { // Field Description
						enum {Lsb = 22};
						enum {FieldMask = 0x00400000};
						enum {Value_ClearInterrupt = 0x0};
						enum {ValueMask_ClearInterrupt = 0x00000000};
						enum {Value_ForceInterrupt = 0x1};
						enum {ValueMask_ForceInterrupt = 0x00400000};
						};
					namespace GPIO1 { // Field Description
						enum {Lsb = 21};
						enum {FieldMask = 0x00200000};
						enum {Value_ClearInterrupt = 0x0};
						enum {ValueMask_ClearInterrupt = 0x00000000};
						enum {Value_ForceInterrupt = 0x1};
						enum {ValueMask_ForceInterrupt = 0x00200000};
						};
					namespace GPIO2 { // Field Description
						enum {Lsb = 20};
						enum {FieldMask = 0x00100000};
						enum {Value_ClearInterrupt = 0x0};
						enum {ValueMask_ClearInterrupt = 0x00000000};
						enum {Value_ForceInterrupt = 0x1};
						enum {ValueMask_ForceInterrupt = 0x00100000};
						};
					namespace DDR { // Field Description
						enum {Lsb = 19};
						enum {FieldMask = 0x00080000};
						enum {Value_ClearInterrupt = 0x0};
						enum {ValueMask_ClearInterrupt = 0x00000000};
						enum {Value_ForceInterrupt = 0x1};
						enum {ValueMask_ForceInterrupt = 0x00080000};
						};
					namespace LBC { // Field Description
						enum {Lsb = 18};
						enum {FieldMask = 0x00040000};
						enum {Value_ClearInterrupt = 0x0};
						enum {ValueMask_ClearInterrupt = 0x00000000};
						enum {Value_ForceInterrupt = 0x1};
						enum {ValueMask_ForceInterrupt = 0x00040000};
						};
					namespace GTM2 { // Field Description
						enum {Lsb = 17};
						enum {FieldMask = 0x00020000};
						enum {Value_ClearInterrupt = 0x0};
						enum {ValueMask_ClearInterrupt = 0x00000000};
						enum {Value_ForceInterrupt = 0x1};
						enum {ValueMask_ForceInterrupt = 0x00020000};
						};
					namespace GTM6 { // Field Description
						enum {Lsb = 16};
						enum {FieldMask = 0x00010000};
						enum {Value_ClearInterrupt = 0x0};
						enum {ValueMask_ClearInterrupt = 0x00000000};
						enum {Value_ForceInterrupt = 0x1};
						enum {ValueMask_ForceInterrupt = 0x00010000};
						};
					namespace PMC { // Field Description
						enum {Lsb = 15};
						enum {FieldMask = 0x00008000};
						enum {Value_ClearInterrupt = 0x0};
						enum {ValueMask_ClearInterrupt = 0x00000000};
						enum {Value_ForceInterrupt = 0x1};
						enum {ValueMask_ForceInterrupt = 0x00008000};
						};
					namespace GTM3 { // Field Description
						enum {Lsb = 11};
						enum {FieldMask = 0x00000800};
						enum {Value_ClearInterrupt = 0x0};
						enum {ValueMask_ClearInterrupt = 0x00000000};
						enum {Value_ForceInterrupt = 0x1};
						enum {ValueMask_ForceInterrupt = 0x00000800};
						};
					namespace GTM7 { // Field Description
						enum {Lsb = 10};
						enum {FieldMask = 0x00000400};
						enum {Value_ClearInterrupt = 0x0};
						enum {ValueMask_ClearInterrupt = 0x00000000};
						enum {Value_ForceInterrupt = 0x1};
						enum {ValueMask_ForceInterrupt = 0x00000400};
						};
					namespace GTM1 { // Field Description
						enum {Lsb = 5};
						enum {FieldMask = 0x00000020};
						enum {Value_ClearInterrupt = 0x0};
						enum {ValueMask_ClearInterrupt = 0x00000000};
						enum {Value_ForceInterrupt = 0x1};
						enum {ValueMask_ForceInterrupt = 0x00000020};
						};
					namespace GTM5 { // Field Description
						enum {Lsb = 4};
						enum {FieldMask = 0x00000010};
						enum {Value_ClearInterrupt = 0x0};
						enum {ValueMask_ClearInterrupt = 0x00000000};
						enum {Value_ForceInterrupt = 0x1};
						enum {ValueMask_ForceInterrupt = 0x00000010};
						};
					};
				namespace SEFCR { // Register description
					typedef uint32_t	Reg;
					namespace IRQ0 { // Field Description
						enum {Lsb = 31};
						enum {FieldMask = 0x80000000};
						enum {Value_ClearInterrupt = 0x0};
						enum {ValueMask_ClearInterrupt = 0x00000000};
						enum {Value_ForceInterrupt = 0x1};
						enum {ValueMask_ForceInterrupt = 0x80000000};
						};
					namespace IRQ1 { // Field Description
						enum {Lsb = 30};
						enum {FieldMask = 0x40000000};
						enum {Value_ClearInterrupt = 0x0};
						enum {ValueMask_ClearInterrupt = 0x00000000};
						enum {Value_ForceInterrupt = 0x1};
						enum {ValueMask_ForceInterrupt = 0x40000000};
						};
					namespace IRQ2 { // Field Description
						enum {Lsb = 29};
						enum {FieldMask = 0x20000000};
						enum {Value_ClearInterrupt = 0x0};
						enum {ValueMask_ClearInterrupt = 0x00000000};
						enum {Value_ForceInterrupt = 0x1};
						enum {ValueMask_ForceInterrupt = 0x20000000};
						};
					namespace IRQ3 { // Field Description
						enum {Lsb = 28};
						enum {FieldMask = 0x10000000};
						enum {Value_ClearInterrupt = 0x0};
						enum {ValueMask_ClearInterrupt = 0x00000000};
						enum {Value_ForceInterrupt = 0x1};
						enum {ValueMask_ForceInterrupt = 0x10000000};
						};
					namespace IRQ4 { // Field Description
						enum {Lsb = 27};
						enum {FieldMask = 0x08000000};
						enum {Value_ClearInterrupt = 0x0};
						enum {ValueMask_ClearInterrupt = 0x00000000};
						enum {Value_ForceInterrupt = 0x1};
						enum {ValueMask_ForceInterrupt = 0x08000000};
						};
					namespace IRQ5 { // Field Description
						enum {Lsb = 26};
						enum {FieldMask = 0x04000000};
						enum {Value_ClearInterrupt = 0x0};
						enum {ValueMask_ClearInterrupt = 0x00000000};
						enum {Value_ForceInterrupt = 0x1};
						enum {ValueMask_ForceInterrupt = 0x04000000};
						};
					namespace IRQ6 { // Field Description
						enum {Lsb = 25};
						enum {FieldMask = 0x02000000};
						enum {Value_ClearInterrupt = 0x0};
						enum {ValueMask_ClearInterrupt = 0x00000000};
						enum {Value_ForceInterrupt = 0x1};
						enum {ValueMask_ForceInterrupt = 0x02000000};
						};
					namespace IRQ7 { // Field Description
						enum {Lsb = 24};
						enum {FieldMask = 0x01000000};
						enum {Value_ClearInterrupt = 0x0};
						enum {ValueMask_ClearInterrupt = 0x00000000};
						enum {Value_ForceInterrupt = 0x1};
						enum {ValueMask_ForceInterrupt = 0x01000000};
						};
					};
				namespace SERFR { // Register description
					typedef uint32_t	Reg;
					namespace IRQ0 { // Field Description
						enum {Lsb = 31};
						enum {FieldMask = 0x80000000};
						enum {Value_ClearInterrupt = 0x0};
						enum {ValueMask_ClearInterrupt = 0x00000000};
						enum {Value_ForceInterrupt = 0x1};
						enum {ValueMask_ForceInterrupt = 0x80000000};
						};
					namespace WDT { // Field Description
						enum {Lsb = 30};
						enum {FieldMask = 0x40000000};
						enum {Value_ClearInterrupt = 0x0};
						enum {ValueMask_ClearInterrupt = 0x00000000};
						enum {Value_ForceInterrupt = 0x1};
						enum {ValueMask_ForceInterrupt = 0x40000000};
						};
					namespace SBA { // Field Description
						enum {Lsb = 29};
						enum {FieldMask = 0x20000000};
						enum {Value_ClearInterrupt = 0x0};
						enum {ValueMask_ClearInterrupt = 0x00000000};
						enum {Value_ForceInterrupt = 0x1};
						enum {ValueMask_ForceInterrupt = 0x20000000};
						};
					namespace PCI1 { // Field Description
						enum {Lsb = 26};
						enum {FieldMask = 0x04000000};
						enum {Value_ClearInterrupt = 0x0};
						enum {ValueMask_ClearInterrupt = 0x00000000};
						enum {Value_ForceInterrupt = 0x1};
						enum {ValueMask_ForceInterrupt = 0x04000000};
						};
					namespace PCI2 { // Field Description
						enum {Lsb = 25};
						enum {FieldMask = 0x02000000};
						enum {Value_ClearInterrupt = 0x0};
						enum {ValueMask_ClearInterrupt = 0x00000000};
						enum {Value_ForceInterrupt = 0x1};
						enum {ValueMask_ForceInterrupt = 0x02000000};
						};
					namespace MU { // Field Description
						enum {Lsb = 7};
						enum {FieldMask = 0x00000080};
						enum {Value_ClearInterrupt = 0x0};
						enum {ValueMask_ClearInterrupt = 0x00000000};
						enum {Value_ForceInterrupt = 0x1};
						enum {ValueMask_ForceInterrupt = 0x00000080};
						};
					};
				namespace SCVCR { // Register description
					typedef uint32_t	Reg;
					namespace CVECx { // Field Description
						enum {Lsb = 26};
						enum {FieldMask = 0xFC000000};
						};
					namespace CVEC { // Field Description
						enum {Lsb = 0};
						enum {FieldMask = 0x0000007F};
						enum {Value_Error = 0x0};
						enum {ValueMask_Error = 0x00000000};
						enum {Value_UART1 = 0x9};
						enum {ValueMask_UART1 = 0x00000009};
						enum {Value_UART2 = 0xA};
						enum {ValueMask_UART2 = 0x0000000A};
						enum {Value_SEC = 0xB};
						enum {ValueMask_SEC = 0x0000000B};
						enum {Value_I2C1 = 0xE};
						enum {ValueMask_I2C1 = 0x0000000E};
						enum {Value_I2C2 = 0xF};
						enum {ValueMask_I2C2 = 0x0000000F};
						enum {Value_SPI = 0x10};
						enum {ValueMask_SPI = 0x00000010};
						enum {Value_IRQ1 = 0x11};
						enum {ValueMask_IRQ1 = 0x00000011};
						enum {Value_IRQ2 = 0x12};
						enum {ValueMask_IRQ2 = 0x00000012};
						enum {Value_IRQ3 = 0x13};
						enum {ValueMask_IRQ3 = 0x00000013};
						enum {Value_IRQ4 = 0x14};
						enum {ValueMask_IRQ4 = 0x00000014};
						enum {Value_IRQ5 = 0x15};
						enum {ValueMask_IRQ5 = 0x00000015};
						enum {Value_IRQ6 = 0x16};
						enum {ValueMask_IRQ6 = 0x00000016};
						enum {Value_IRQ7 = 0x17};
						enum {ValueMask_IRQ7 = 0x00000017};
						enum {Value_TSEC1Tx = 0x20};
						enum {ValueMask_TSEC1Tx = 0x00000020};
						enum {Value_TSEC1Rx = 0x21};
						enum {ValueMask_TSEC1Rx = 0x00000021};
						enum {Value_TSEC1Err = 0x22};
						enum {ValueMask_TSEC1Err = 0x00000022};
						enum {Value_TSEC2Tx = 0x23};
						enum {ValueMask_TSEC2Tx = 0x00000023};
						enum {Value_TSEC2Rx = 0x24};
						enum {ValueMask_TSEC2Rx = 0x00000024};
						enum {Value_TSEC2Err = 0x25};
						enum {ValueMask_TSEC2Err = 0x00000025};
						enum {Value_USB2DR = 0x26};
						enum {ValueMask_USB2DR = 0x00000026};
						enum {Value_USB2MPH = 0x27};
						enum {ValueMask_USB2MPH = 0x00000027};
						enum {Value_IRQ0 = 0x30};
						enum {ValueMask_IRQ0 = 0x00000030};
						enum {Value_RTCSEC = 0x40};
						enum {ValueMask_RTCSEC = 0x00000040};
						enum {Value_PIT = 0x41};
						enum {ValueMask_PIT = 0x00000041};
						enum {Value_PCI1 = 0x42};
						enum {ValueMask_PCI1 = 0x00000042};
						enum {Value_PCI2 = 0x43};
						enum {ValueMask_PCI2 = 0x00000043};
						enum {Value_RTCALR = 0x44};
						enum {ValueMask_RTCALR = 0x00000044};
						enum {Value_MU = 0x45};
						enum {ValueMask_MU = 0x00000045};
						enum {Value_SBA = 0x46};
						enum {ValueMask_SBA = 0x00000046};
						enum {Value_DMA = 0x47};
						enum {ValueMask_DMA = 0x00000047};
						enum {Value_GTM4 = 0x48};
						enum {ValueMask_GTM4 = 0x00000048};
						enum {Value_GTM8 = 0x49};
						enum {ValueMask_GTM8 = 0x00000049};
						enum {Value_GPIO1 = 0x4A};
						enum {ValueMask_GPIO1 = 0x0000004A};
						enum {Value_GPIO2 = 0x4B};
						enum {ValueMask_GPIO2 = 0x0000004B};
						enum {Value_DDR = 0x4C};
						enum {ValueMask_DDR = 0x0000004C};
						enum {Value_LBC = 0x4D};
						enum {ValueMask_LBC = 0x0000004D};
						enum {Value_GTM2 = 0x4E};
						enum {ValueMask_GTM2 = 0x0000004E};
						enum {Value_GTM6 = 0x4F};
						enum {ValueMask_GTM6 = 0x0000004F};
						enum {Value_PMC = 0x50};
						enum {ValueMask_PMC = 0x00000050};
						enum {Value_GTM3 = 0x54};
						enum {ValueMask_GTM3 = 0x00000054};
						enum {Value_GTM7 = 0x55};
						enum {ValueMask_GTM7 = 0x00000055};
						enum {Value_GTM1 = 0x5A};
						enum {ValueMask_GTM1 = 0x0000005A};
						enum {Value_GTM5 = 0x5B};
						enum {ValueMask_GTM5 = 0x0000005B};
						};
					};
				namespace SMVCR { // Register description
					typedef uint32_t	Reg;
					namespace MVECx { // Field Description
						enum {Lsb = 26};
						enum {FieldMask = 0xFC000000};
						};
					namespace MVEC { // Field Description
						enum {Lsb = 0};
						enum {FieldMask = 0x0000007F};
						enum {Value_Error = 0x0};
						enum {ValueMask_Error = 0x00000000};
						enum {Value_UART1 = 0x9};
						enum {ValueMask_UART1 = 0x00000009};
						enum {Value_UART2 = 0xA};
						enum {ValueMask_UART2 = 0x0000000A};
						enum {Value_SEC = 0xB};
						enum {ValueMask_SEC = 0x0000000B};
						enum {Value_I2C1 = 0xE};
						enum {ValueMask_I2C1 = 0x0000000E};
						enum {Value_I2C2 = 0xF};
						enum {ValueMask_I2C2 = 0x0000000F};
						enum {Value_SPI = 0x10};
						enum {ValueMask_SPI = 0x00000010};
						enum {Value_IRQ1 = 0x11};
						enum {ValueMask_IRQ1 = 0x00000011};
						enum {Value_IRQ2 = 0x12};
						enum {ValueMask_IRQ2 = 0x00000012};
						enum {Value_IRQ3 = 0x13};
						enum {ValueMask_IRQ3 = 0x00000013};
						enum {Value_IRQ4 = 0x14};
						enum {ValueMask_IRQ4 = 0x00000014};
						enum {Value_IRQ5 = 0x15};
						enum {ValueMask_IRQ5 = 0x00000015};
						enum {Value_IRQ6 = 0x16};
						enum {ValueMask_IRQ6 = 0x00000016};
						enum {Value_IRQ7 = 0x17};
						enum {ValueMask_IRQ7 = 0x00000017};
						enum {Value_TSEC1Tx = 0x20};
						enum {ValueMask_TSEC1Tx = 0x00000020};
						enum {Value_TSEC1Rx = 0x21};
						enum {ValueMask_TSEC1Rx = 0x00000021};
						enum {Value_TSEC1Err = 0x22};
						enum {ValueMask_TSEC1Err = 0x00000022};
						enum {Value_TSEC2Tx = 0x23};
						enum {ValueMask_TSEC2Tx = 0x00000023};
						enum {Value_TSEC2Rx = 0x24};
						enum {ValueMask_TSEC2Rx = 0x00000024};
						enum {Value_TSEC2Err = 0x25};
						enum {ValueMask_TSEC2Err = 0x00000025};
						enum {Value_USB2DR = 0x26};
						enum {ValueMask_USB2DR = 0x00000026};
						enum {Value_USB2MPH = 0x27};
						enum {ValueMask_USB2MPH = 0x00000027};
						enum {Value_IRQ0 = 0x30};
						enum {ValueMask_IRQ0 = 0x00000030};
						enum {Value_RTCSEC = 0x40};
						enum {ValueMask_RTCSEC = 0x00000040};
						enum {Value_PIT = 0x41};
						enum {ValueMask_PIT = 0x00000041};
						enum {Value_PCI1 = 0x42};
						enum {ValueMask_PCI1 = 0x00000042};
						enum {Value_PCI2 = 0x43};
						enum {ValueMask_PCI2 = 0x00000043};
						enum {Value_RTCALR = 0x44};
						enum {ValueMask_RTCALR = 0x00000044};
						enum {Value_MU = 0x45};
						enum {ValueMask_MU = 0x00000045};
						enum {Value_SBA = 0x46};
						enum {ValueMask_SBA = 0x00000046};
						enum {Value_DMA = 0x47};
						enum {ValueMask_DMA = 0x00000047};
						enum {Value_GTM4 = 0x48};
						enum {ValueMask_GTM4 = 0x00000048};
						enum {Value_GTM8 = 0x49};
						enum {ValueMask_GTM8 = 0x00000049};
						enum {Value_GPIO1 = 0x4A};
						enum {ValueMask_GPIO1 = 0x0000004A};
						enum {Value_GPIO2 = 0x4B};
						enum {ValueMask_GPIO2 = 0x0000004B};
						enum {Value_DDR = 0x4C};
						enum {ValueMask_DDR = 0x0000004C};
						enum {Value_LBC = 0x4D};
						enum {ValueMask_LBC = 0x0000004D};
						enum {Value_GTM2 = 0x4E};
						enum {ValueMask_GTM2 = 0x0000004E};
						enum {Value_GTM6 = 0x4F};
						enum {ValueMask_GTM6 = 0x0000004F};
						enum {Value_PMC = 0x50};
						enum {ValueMask_PMC = 0x00000050};
						enum {Value_GTM3 = 0x54};
						enum {ValueMask_GTM3 = 0x00000054};
						enum {Value_GTM7 = 0x55};
						enum {ValueMask_GTM7 = 0x00000055};
						enum {Value_GTM1 = 0x5A};
						enum {ValueMask_GTM1 = 0x0000005A};
						enum {Value_GTM5 = 0x5B};
						enum {ValueMask_GTM5 = 0x0000005B};
						};
					};
				namespace SVCR { // Register description
					typedef uint32_t	Reg;
					namespace VECx { // Field Description
						enum {Lsb = 26};
						enum {FieldMask = 0xFC000000};
						};
					namespace VEC { // Field Description
						enum {Lsb = 0};
						enum {FieldMask = 0x0000007F};
						enum {Value_Error = 0x0};
						enum {ValueMask_Error = 0x00000000};
						enum {Value_UART1 = 0x9};
						enum {ValueMask_UART1 = 0x00000009};
						enum {Value_UART2 = 0xA};
						enum {ValueMask_UART2 = 0x0000000A};
						enum {Value_SEC = 0xB};
						enum {ValueMask_SEC = 0x0000000B};
						enum {Value_I2C1 = 0xE};
						enum {ValueMask_I2C1 = 0x0000000E};
						enum {Value_I2C2 = 0xF};
						enum {ValueMask_I2C2 = 0x0000000F};
						enum {Value_SPI = 0x10};
						enum {ValueMask_SPI = 0x00000010};
						enum {Value_IRQ1 = 0x11};
						enum {ValueMask_IRQ1 = 0x00000011};
						enum {Value_IRQ2 = 0x12};
						enum {ValueMask_IRQ2 = 0x00000012};
						enum {Value_IRQ3 = 0x13};
						enum {ValueMask_IRQ3 = 0x00000013};
						enum {Value_IRQ4 = 0x14};
						enum {ValueMask_IRQ4 = 0x00000014};
						enum {Value_IRQ5 = 0x15};
						enum {ValueMask_IRQ5 = 0x00000015};
						enum {Value_IRQ6 = 0x16};
						enum {ValueMask_IRQ6 = 0x00000016};
						enum {Value_IRQ7 = 0x17};
						enum {ValueMask_IRQ7 = 0x00000017};
						enum {Value_TSEC1Tx = 0x20};
						enum {ValueMask_TSEC1Tx = 0x00000020};
						enum {Value_TSEC1Rx = 0x21};
						enum {ValueMask_TSEC1Rx = 0x00000021};
						enum {Value_TSEC1Err = 0x22};
						enum {ValueMask_TSEC1Err = 0x00000022};
						enum {Value_TSEC2Tx = 0x23};
						enum {ValueMask_TSEC2Tx = 0x00000023};
						enum {Value_TSEC2Rx = 0x24};
						enum {ValueMask_TSEC2Rx = 0x00000024};
						enum {Value_TSEC2Err = 0x25};
						enum {ValueMask_TSEC2Err = 0x00000025};
						enum {Value_USB2DR = 0x26};
						enum {ValueMask_USB2DR = 0x00000026};
						enum {Value_USB2MPH = 0x27};
						enum {ValueMask_USB2MPH = 0x00000027};
						enum {Value_IRQ0 = 0x30};
						enum {ValueMask_IRQ0 = 0x00000030};
						enum {Value_RTCSEC = 0x40};
						enum {ValueMask_RTCSEC = 0x00000040};
						enum {Value_PIT = 0x41};
						enum {ValueMask_PIT = 0x00000041};
						enum {Value_PCI1 = 0x42};
						enum {ValueMask_PCI1 = 0x00000042};
						enum {Value_PCI2 = 0x43};
						enum {ValueMask_PCI2 = 0x00000043};
						enum {Value_RTCALR = 0x44};
						enum {ValueMask_RTCALR = 0x00000044};
						enum {Value_MU = 0x45};
						enum {ValueMask_MU = 0x00000045};
						enum {Value_SBA = 0x46};
						enum {ValueMask_SBA = 0x00000046};
						enum {Value_DMA = 0x47};
						enum {ValueMask_DMA = 0x00000047};
						enum {Value_GTM4 = 0x48};
						enum {ValueMask_GTM4 = 0x00000048};
						enum {Value_GTM8 = 0x49};
						enum {ValueMask_GTM8 = 0x00000049};
						enum {Value_GPIO1 = 0x4A};
						enum {ValueMask_GPIO1 = 0x0000004A};
						enum {Value_GPIO2 = 0x4B};
						enum {ValueMask_GPIO2 = 0x0000004B};
						enum {Value_DDR = 0x4C};
						enum {ValueMask_DDR = 0x0000004C};
						enum {Value_LBC = 0x4D};
						enum {ValueMask_LBC = 0x0000004D};
						enum {Value_GTM2 = 0x4E};
						enum {ValueMask_GTM2 = 0x0000004E};
						enum {Value_GTM6 = 0x4F};
						enum {ValueMask_GTM6 = 0x0000004F};
						enum {Value_PMC = 0x50};
						enum {ValueMask_PMC = 0x00000050};
						enum {Value_GTM3 = 0x54};
						enum {ValueMask_GTM3 = 0x00000054};
						enum {Value_GTM7 = 0x55};
						enum {ValueMask_GTM7 = 0x00000055};
						enum {Value_GTM1 = 0x5A};
						enum {ValueMask_GTM1 = 0x0000005A};
						enum {Value_GTM5 = 0x5B};
						enum {ValueMask_GTM5 = 0x0000005B};
						};
					};
				};
			};
		};
	};
#endif
