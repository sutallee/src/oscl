/*
   Copyright (C) 2006 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_hw_motorola_mpc834x_spih_
#define _oscl_hw_motorola_mpc834x_spih_
#include "spireg.h"

/** */
namespace Oscl {
/** */
namespace Motorola {
/** */
namespace MPC834x {
/** */
namespace SPI {

/** */
struct RegisterMap {
	/** Offset 0x000 */
	uint8_t					reserved0[0x020-0x000];
	/** Offset 0x020 */
	SPMODE::Reg					spmode;
	/** Offset 0x024 */
	volatile SPIE::Reg			spie;
	/** Offset 0x028 */
	SPIM::Reg					spim;
	/** Offset 0x02C */
	volatile SPCOM::Reg			spcom;
	/** Offset 0x030 */
	volatile SPITD::Reg			spitd;
	/** Offset 0x034 */
	volatile SPIRD::Reg			spird;
	/** Offset 0x038 */
	uint8_t					reserved4[0x1000-0x038];
	};

}
}
}
}

#endif
