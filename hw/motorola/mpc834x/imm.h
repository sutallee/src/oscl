/*
   Copyright (C) 2006 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_hw_motorola_mpc834x_immh_
#define _oscl_hw_motorola_mpc834x_immh_
#include "oscl/hw/motorola/mpc834x/pit.h"
#include "oscl/hw/motorola/mpc834x/pic.h"
#include "oscl/hw/motorola/mpc834x/duart.h"
#include "oscl/hw/motorola/mpc834x/gpio.h"
#include "oscl/hw/motorola/mpc834x/i2c.h"
#include "oscl/hw/motorola/mpc834x/spi.h"

/** */
namespace Oscl {
/** */
namespace Motorola {
/** */
namespace MPC834x {

/** System Configuration (512B) */
struct SystemConfiguration {
	/** */
	uint32_t	stuff[512/sizeof(uint32_t)];
	};

/** Watch Dog Timer (256B) */
struct WatchDogTimer {
	/** */
	uint32_t	stuff[256/sizeof(uint32_t)];
	};

/** Real Time Clock (256B) */
struct RealTimeClock {
	/** */
	uint32_t	stuff[256/sizeof(uint32_t)];
	};

/** Periodic Interval Timer (256B) */
struct PeriodicIntervalTimer {
	/** */
	Oscl::Motorola::MPC834x::PIT::RegisterMap	registers;
	/** */
	uint8_t									reserved[256-sizeof(Oscl::Motorola::MPC834x::PIT::RegisterMap)];
	};

/** Global Timers Module (256B) */
struct GlobalTimersModule {
	/** */
	uint32_t								stuff[256/sizeof(uint32_t)];
	};

/** Integrated Programmable Interrupt Controller (256B) */
struct IntegratedPIC {
	/** */
	Oscl::Motorola::MPC834x::PIC::RegisterMap	ipic;
	/** */
	uint8_t								stuff[256-sizeof(Oscl::Motorola::MPC834x::PIC::RegisterMap)];
	};

/** System Arbiter (256B) */
struct SystemArbiter {
	/** */
	uint32_t								stuff[256/sizeof(uint32_t)];
	};

/** Reset Module (256B) */
struct ResetModule {
	/** */
	uint32_t								stuff[256/sizeof(uint32_t)];
	};

/** Clock Module (256B) */
struct ClockModule {
	/** */
	uint32_t								stuff[256/sizeof(uint32_t)];
	};

/** Power Management Control Module (256B) */
struct PowerManagementControlModule {
	/** */
	uint32_t								stuff[256/sizeof(uint32_t)];
	};

/** GPIO Module (256B) */
struct GPIOModule {
	/** */
	Oscl::Motorola::MPC834x::GPIO::RegisterMap	gpio;
	/** */
	uint8_t									stuff[256-sizeof(Oscl::Motorola::MPC834x::GPIO::RegisterMap)];
	};

/** DLL DDR (256B) */
struct DllDDR {
	/** */
	uint32_t								stuff[256/sizeof(uint32_t)];
	};

/** DLL LBIU (256B) */
struct DllLBIU {
	/** */
	uint32_t								stuff[256/sizeof(uint32_t)];
	};

/** DDR MEMC (4KB) */
struct DdrMEMC {
	/** */
	uint32_t								stuff[4096/sizeof(uint32_t)];
	};

/** I2C Controller (256B) */
struct I2CModule {
	/** */
	Oscl::Motorola::MPC834x::I2C::RegisterMap	registers;
	/** */
	uint8_t									stuff[256-sizeof(Oscl::Motorola::MPC834x::I2C::RegisterMap)];
	};

/** DUART Module (4KB) */
struct DUARTModule {
	/** */
	uint8_t									reserved[0x4500-0x4000];
	/** */
	Oscl::Motorola::MPC834x::DUART::RegisterMap	registers;
	/** */
	uint8_t									stuff[(4096-0x500)-sizeof(Oscl::Motorola::MPC834x::DUART::RegisterMap)];
	};

/** LBIU (4KB) */
struct LBIUModule {
	/** */
	uint32_t								stuff[4096/sizeof(uint32_t)];
	};

/** SPI (4KB) */
struct SPIModule {
	/** */
	Oscl::Motorola::MPC834x::SPI::RegisterMap	registers;
	/** */
	uint8_t								stuff[4096-sizeof(Oscl::Motorola::MPC834x::SPI::RegisterMap)];
	};

/** DMA (768B) */
struct DMAModule {
	/** */
	uint32_t								stuff[768/sizeof(uint32_t)];
	};

/** PCI Configuration (128B) */
struct PCIConfig {
	/** */
	uint32_t								stuff[128/sizeof(uint32_t)];
	};

/** IOS (256B) */
struct IOS {
	/** */
	uint32_t								stuff[256/sizeof(uint32_t)];
	};

/** PCIController (256B) */
struct PCIController {
	/** */
	uint32_t								stuff[256/sizeof(uint32_t)];
	};

/** USB MPH Module (4KB) */
struct UsbMphModule {
	/** */
	uint32_t								stuff[4096/sizeof(uint32_t)];
	};

/** USB DR MOdule (4KB) */
struct UsbDrModule {
	/** */
	uint32_t								stuff[4096/sizeof(uint32_t)];
	};

/** TSEC (4KB) */
struct TSECModule {
	/** */
	uint32_t								stuff[4096/sizeof(uint32_t)];
	};

/** Security Core (64KB) */
struct SecurityCore {
	/** */
	uint32_t								stuff[(64*1024)/sizeof(uint32_t)];
	};

/** */
struct Map {
	/** System Configuration 0x00000000 */
	SystemConfiguration				systemConfiguration;

	/** Watchdog Timer 0x00000200 */
	WatchDogTimer					watchDogTimer;

	/** Real Time Clock 0x00000300 */
	RealTimeClock					realTimeClock;

	/** Periodic Interval Timer 0x00000400 */
	PeriodicIntervalTimer			periodicIntervalTimer;

	/** Global Timers Module 1 0x00000500 */
	GlobalTimersModule				globalTimersModule1;

	/** Global Timers Module 2 0x00000600 */
	GlobalTimersModule				globalTimersModule2;

	/** Integrated Priority Interrupt Controller 0x00000700 */
	IntegratedPIC					integratedPIC;

	/** System Arbiter 0x00000800 */
	SystemArbiter					systemArbiter;

	/** Reset Module 0x00000900 */
	ResetModule						resetModule;

	/** Clock Module 0x00000A00 */
	ClockModule						clockModule;

	/** Power Management Control Module 0x00000B00 */
	PowerManagementControlModule	powerManagementControlModule;

	/** General Purpose I/O Module 1 0x00000C00 */
	GPIOModule						gpio1;

	/** General Purpose I/O Module 2 0x00000D00 */
	GPIOModule						gpio2;

	uint8_t						reserved1[0xf00-0xe00];
	uint8_t						reserved2[0x1000-0xf00];

	/** DLL DDR 0x00001000 */
	DllDDR							dllDDR;

	/** DLL LBIU 0x00001100 */
	DllLBIU							dllLBIU;

	uint8_t						reserved3[0x1300-0x1200];
	uint8_t						reserved4[0x2000-0x1300];

	/** DDR MEMC 0x00002000 */
	DdrMEMC							ddrMEMC;

	/** I2C1 Controller 0x00003000 */
	I2CModule						i2c1;

	/** I2C2 Controller 0x00003100 */
	I2CModule						i2c2;

	uint8_t						reserved5[0x4000-0x3200];

	/** DUART 0x00004000 */
	DUARTModule						duart;

	/** LBIU 0x00005000 */
	LBIUModule						lbiu;

	uint8_t						reserved8[0x7000-0x6000];

	/** SPI 0x00007000 */
	SPIModule						spi;

	/** DMA 0x00008000 */
	DMAModule						dma;

	/** PCI1 Configuration 0x00008300 */
	PCIConfig						pci1Config;

	/** PCI2 Configuration 0x00008380 */
	PCIConfig						pci2Config;

	/** IOS 0x00008400 */
	IOS								ios;

	/** PCI1 Controller 0x00008500 */
	PCIController					pci1Controller;

	/** PCI2 Controller 0x00008600 */
	PCIController					pci2Controller;

	uint8_t						reserved9[0x22000-0x8700];

	/** USB MPH Module 0x00022000 */
	UsbMphModule					usbMphModule;

	/** USB DR Module 0x00023000 */
	UsbDrModule						usbDrModule;

	/** TSEC1 0x00024000 */
	TSECModule						tsec1;

	/** TSEC1 0x00025000 */
	TSECModule						tsec2;

	uint8_t						reservedA[0x30000-0x26000];

	/** Security Core 0x00030000 */
	SecurityCore					securityCore;

	uint8_t						reservedB[0x100000-0x40000];
	};

}
}
}

#endif
				;
