/*
   Copyright (C) 2006 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_hw_motorola_mpc834x_i2ch_
#define _oscl_hw_motorola_mpc834x_i2ch_
#include "i2creg.h"

/** */
namespace Oscl {
/** */
namespace Motorola {
/** */
namespace MPC834x {
/** */
namespace I2C {

/** */
struct RegisterMap {
	/** Offset 0x000 */
	volatile I2CADR::Reg		i2cadr;
	/** Offset 0x001 */
	uint8_t					reserved1[0x004-0x001];
	/** Offset 0x004 */
	volatile I2CFDR::Reg		i2cfdr;
	/** Offset 0x005 */
	uint8_t					reserved2[0x008-0x005];
	/** Offset 0x008 */
	volatile I2CCR::Reg			i2ccr;
	/** Offset 0x009 */
	uint8_t					reserved3[0x00C-0x009];
	/** Offset 0x00C */
	volatile I2CSR::Reg			i2csr;
	/** Offset 0x00D */
	uint8_t					reserved4[0x010-0x00D];
	/** Offset 0x010 */
	volatile I2CDR::Reg			i2cdr;
	/** Offset 0x011 */
	uint8_t					reserved5[0x014-0x011];
	/** Offset 0x014 */
	volatile I2CDFSRR::Reg		i2cdfsrr;
	/** Offset 0x015 */
	uint8_t					reserved6[0x100-0x015];
	};

}
}
}
}

#endif
