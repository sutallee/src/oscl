/*
   Copyright (C) 2006 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_hw_motorola_mpc834x_pich_
#define _oscl_hw_motorola_mpc834x_pich_
#include "picreg.h"

/** */
namespace Oscl {
/** */
namespace Motorola {
/** */
namespace MPC834x {
/** */
namespace PIC {

/** */
struct RegisterMap {
	/** Offset 0x00 */
	volatile SICFR::Reg		sicfr;
	/** Offset 0x04 */
	volatile SVCR::Reg		sivcr;
	/** Offset 0x08 */
	volatile SIPNR_H::Reg	sipnr_h;
	/** Offset 0x0C */
	volatile SIPNR_L::Reg	sipnr_l;
	/** Offset 0x10 */
	volatile SIPRR_A::Reg	siprr_a;
	/** Offset 0x14 */
	uint8_t				reserved1[0x1C-0x14];
	/** Offset 0x1C */
	volatile SIPRR_D::Reg	siprr_d;
	/** Offset 0x20 */
	volatile SIMSR_H::Reg	simsr_h;
	/** Offset 0x24 */
	volatile SIMSR_L::Reg	simsr_l;
	/** Offset 0x28 */
	volatile SICNR::Reg		sicnr;
	/** Offset 0x2C */
	volatile SEPNR::Reg		sepnr;
	/** Offset 0x30 */
	volatile SMPRR_A::Reg	smprr_a;
	/** Offset 0x34 */
	volatile SMPRR_B::Reg	smprr_b;
	/** Offset 0x38 */
	volatile SEMSR::Reg		semsr;
	/** Offset 0x3C */
	volatile SECNR::Reg		secnr;
	/** Offset 0x40 */
	volatile SERSR::Reg		sersr;
	/** Offset 0x44 */
	volatile SERMR::Reg		sermr;
	/** Offset 0x48 */
	volatile SERCR::Reg		sercr;
	/** Offset 0x4C */
	uint8_t				reserved2[0x50-0x4C];
	/** Offset 0x50 */
	volatile SIFCR_H::Reg	sifcr_h;
	/** Offset 0x54 */
	volatile SIFCR_L::Reg	sifcr_l;
	/** Offset 0x58 */
	volatile SEFCR::Reg		sefcr;
	/** Offset 0x5C */
	volatile SERFR::Reg		serfr;
	/** Offset 0x60 */
	volatile SVCR::Reg		scvcr;
	/** Offset 0x64 */
	volatile SVCR::Reg		smvcr;
	/** Offset 0x68 */
	uint8_t				reserved3[0x100-0x68];
	};

}
}
}
}

#endif
