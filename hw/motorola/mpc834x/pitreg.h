#ifndef _oscl_hw_motorola_mpc834x_pitregh_
#define _oscl_hw_motorola_mpc834x_pitregh_
#include <stdint.h>
namespace Oscl { // Namespace description
	namespace Motorola { // Namespace description
		namespace MPC834x { // Namespace description
			namespace PIT { // Namespace description
				namespace PTCNR { // Register description
					typedef uint32_t	Reg;
					namespace CLEN { // Field Description
						enum {Lsb = 7};
						enum {FieldMask = 0x00000080};
						enum {Value_DisableCounter = 0x0};
						enum {ValueMask_DisableCounter = 0x00000000};
						enum {Value_EnableCounter = 0x1};
						enum {ValueMask_EnableCounter = 0x00000080};
						};
					namespace CLIN { // Field Description
						enum {Lsb = 6};
						enum {FieldMask = 0x00000040};
						enum {Value_UseSystemClock = 0x0};
						enum {ValueMask_UseSystemClock = 0x00000000};
						enum {Value_UseExternalPitClock = 0x1};
						enum {ValueMask_UseExternalPitClock = 0x00000040};
						};
					namespace PIM { // Field Description
						enum {Lsb = 0};
						enum {FieldMask = 0x00000001};
						enum {Value_InterruptDisabled = 0x0};
						enum {ValueMask_InterruptDisabled = 0x00000000};
						enum {Value_InterruptEnabled = 0x1};
						enum {ValueMask_InterruptEnabled = 0x00000001};
						};
					};
				namespace PTLDR { // Register description
					typedef uint32_t	Reg;
					namespace CLDV { // Field Description
						enum {Lsb = 0};
						enum {FieldMask = 0xFFFFFFFF};
						};
					};
				namespace PTPSR { // Register description
					typedef uint32_t	Reg;
					namespace PRSC { // Field Description
						enum {Lsb = 0};
						enum {FieldMask = 0x0000FFFF};
						};
					};
				namespace PTCTR { // Register description
					typedef uint32_t	Reg;
					namespace CNTV { // Field Description
						enum {Lsb = 0};
						enum {FieldMask = 0xFFFFFFFF};
						};
					};
				namespace PTEVR { // Register description
					typedef uint32_t	Reg;
					namespace PIF { // Field Description
						enum {Lsb = 0};
						enum {FieldMask = 0x00000001};
						enum {Value_Pending = 0x1};
						enum {ValueMask_Pending = 0x00000001};
						enum {Value_Clear = 0x1};
						enum {ValueMask_Clear = 0x00000001};
						};
					};
				};
			};
		};
	};
#endif
