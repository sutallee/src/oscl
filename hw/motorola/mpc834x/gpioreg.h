#ifndef _oscl_hw_motorola_mpc834x_gpioregh_
#define _oscl_hw_motorola_mpc834x_gpioregh_
#include <stdint.h>
namespace Oscl { // Namespace description
	namespace Motorola { // Namespace description
		namespace MPC834x { // Namespace description
			namespace GPIO { // Namespace description
				namespace GPDIR { // Register description
					typedef uint32_t	Reg;
					enum {Mask = 1};
					enum {Input = 0};
					enum {Output = 1};
					};
				namespace GPODR { // Register description
					typedef uint32_t	Reg;
					enum {Mask = 1};
					enum {Driven = 0};
					enum {OpenDrain = 1};
					};
				namespace GPDAT { // Register description
					typedef uint32_t	Reg;
					enum {Mask = 1};
					enum {Low = 0};
					enum {High = 1};
					};
				namespace GPIER { // Register description
					typedef uint32_t	Reg;
					enum {Mask = 1};
					enum {Pending = 1};
					enum {Clear = 1};
					};
				namespace GPIMR { // Register description
					typedef uint32_t	Reg;
					enum {Mask = 1};
					enum {Disable = 0};
					enum {Enable = 1};
					};
				namespace GPICR { // Register description
					typedef uint32_t	Reg;
					enum {Mask = 1};
					enum {AnyEdge = 0};
					enum {HighToLowEdge = 1};
					};
				};
			};
		};
	};
#endif
