/*
   Copyright (C) 2006 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/


module oscl_hw_motorola_mpc834x_i2cregh {
	include "oscl/compiler/types.h";
	namespace Oscl {
		namespace Motorola {
			namespace MPC834x {
				namespace I2C {
					register I2CADR uint8_t {
						field ADDR 1 7 { }
						}
					register I2CFDR uint8_t {
						field FDR 0 6 {
							// From Table 17-5 in the MPC8347/9 document.
							// NOTE: Table 17-5 describes the ratio between
							// the I2C controller internal clock and SCL.
							// However, the default ratio of I2C1 controller
							// clock and CSB is 1:3 (I2C1 controller clock
							// frequency is three times slower than CSB clock
							// frequency). This ratio is set in SCCR[TSEC2CM].
							// The user should consider this factor when
							// selecting FDR value. The I2C2 controller clock
							// equals to CSB clock frequency. Yikes!
							value DivideBy384		0x00
							value DivideBy416		0x01
							value DivideBy480		0x02
							value DivideBy576		0x03
							value DivideBy640		0x04
							value DivideBy704		0x05
							value DivideBy832		0x06
							value DivideBy1024		0x07
							value DivideBy1152		0x08
							value DivideBy1280		0x09
							value DivideBy1536		0x0A
							value DivideBy1920		0x0B
							value DivideBy2304		0x0C
							value DivideBy2560		0x0D
							value DivideBy3072		0x0E
							value DivideBy3840		0x0F
							value DivideBy4608		0x10
							value DivideBy5120		0x11
							value DivideBy6144		0x12
							value DivideBy7680		0x13
							value DivideBy9216		0x14
							value DivideBy10240		0x15
							value DivideBy12288		0x16
							value DivideBy15360		0x17
							value DivideBy18432		0x18
							value DivideBy20480		0x19
							value DivideBy24576		0x1A
							value DivideBy30720		0x1B
							value DivideBy36864		0x1C
							value DivideBy40960		0x1D
							value DivideBy49152		0x1E
							value DivideBy61440	0x1F
							// Seems the MSB (bit 5) shifts a mode
							value DivideBy256		0x20
							value DivideBy288		0x21
							value DivideBy320		0x22
							value DivideBy352		0x23
							value DivideBy384b		0x24
							value DivideBy448		0x25
							value DivideBy512		0x26
							value DivideBy576b		0x27
							value DivideBy640b		0x28
							value DivideBy768		0x29
							value DivideBy896		0x2A
							value DivideBy1024b		0x2B
							value DivideBy1280b		0x2C
							value DivideBy1536b		0x2D
							value DivideBy1792		0x2E
							value DivideBy2048		0x2F
							value DivideBy2560b		0x30
							value DivideBy3072b		0x31
							value DivideBy3584		0x32
							value DivideBy4096		0x33
							value DivideBy5120b		0x34
							value DivideBy6144b		0x35
							value DivideBy7168		0x36
							value DivideBy8192		0x37
							value DivideBy10240b	0x38
							value DivideBy12288b	0x39
							value DivideBy14336		0x3A
							value DivideBy16384		0x3B
							value DivideBy20480b	0x3C
							value DivideBy24576b	0x3D
							value DivideBy28672		0x3E
							value DivideBy32768	0x3F
							}
						}
					register I2CCR uint8_t {
						field MEN 7 1 {
							value ModuleDisable	0
							value ModuleEnable	1
							}
						field MIEN 6 1 {
							value InterruptDisable	0
							value InterruptEnable	1
							}
						field MSTA 5 1 {
							value SlaveMode		0
							value MasterMode	1
							}
						field MTX 4 1 {
							value ReceiveMode	0
							value TransmitMode	1
							}
						field TXAK 3 1 {
							value SendAckAt9thBitOnRx	0
							value NoAckSent				1
							}
						field RSTA 2 1 {
							value NoStartCondition				0
							value GenerateRepeatStartCondition	1
							}
						field BCST 0 1 {
							value BroadcastRxDisable	0
							value BroadcastRxEnable		1
							}
						}
					register I2CSR uint8_t {
						field MCF 7 1 {
							value ByteTransferInProgress	0
							value ByteTransferComplete		1
							}
						field MAAS 6 1 {
							value NotAddressedAsSlave	0
							value AddressedAsSlave		1
							}
						field MBB 5 1 {
							value BusIsIdle		0
							value BusIsBusy		1
							}
						field MAL 4 1 {
							// Write 1 to clear?
							value ArbitrationLost		1
							value Clear					0
							}
						field BCSTM 3 1 {
							value NoBroadcastMatch	0
							value BroadcastMatch	1
							}
						field SRW 2 1 {
							value Write	0
							value Read	1
							}
						field MIF 1 1 {
							// Write 1 to clear?
							value Pending	1
							value Clear		0
							}
						field RXAK 0 1 {
							value AckReceived		0
							value AckNotReceived	1
							}
						}
					register I2CDR uint8_t { }
					register I2CDFSRR uint8_t {
						field DFSR 0 6 {
							value Default	0x10
							}
						}
					}
				}
			}
		}
	}

