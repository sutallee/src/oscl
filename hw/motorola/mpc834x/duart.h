/*
   Copyright (C) 2006 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_hw_motorola_mpc834x_duarth_
#define _oscl_hw_motorola_mpc834x_duarth_
#include "oscl/hw/national/uart/pc16550.h"

/** */
namespace Oscl {
/** */
namespace Motorola {
/** */
namespace MPC834x {
/** */
namespace DUART {

struct UART {
	/** Offset 0x000 */
	union {
		const volatile Oscl::HW::National::UART::PC16550::RBR::Reg	rbr;
		volatile Oscl::HW::National::UART::PC16550::RBR::Reg		thr;
		Oscl::HW::National::UART::PC16550::DLL::Reg					dll;
		} dlab00;

	/** Offset 0x001 */
	union {
		Oscl::HW::National::UART::PC16550::IER::Reg		ier;
		Oscl::HW::National::UART::PC16550::DLH::Reg		dlh;
		} dlab01;

	/** Offset 0x002 */
	union {
		const volatile Oscl::HW::National::UART::PC16550::IIR::Reg	iir;
		Oscl::HW::National::UART::PC16550::FCR::Reg					fcr;
		// also UAFR ... non-standard, later
		} dlab02;

	/** Offset 0x003 */
	Oscl::HW::National::UART::PC16550::LCR::Reg		lcr;
	/** Offset 0x004 */
	Oscl::HW::National::UART::PC16550::MCR::Reg		mcr;
	/** Offset 0x005 */
	const volatile Oscl::HW::National::UART::PC16550::LSR::Reg		lsr;
	/** Offset 0x006 */
	const volatile Oscl::HW::National::UART::PC16550::MSR::Reg		msr;
	/** Offset 0x007 */
	Oscl::HW::National::UART::PC16550::SPR::Reg					spr;
	/** Offset 0x008*/
	uint8_t												reserved1[0x010-0x008];
	/** Offset 0x010*/
	volatile uint8_t									udsr;
	/** Offset 0x011*/
	uint8_t												reserved2[0x100-0x011];
	};

/** */
struct RegisterMap {
	/** */
	UART		uart1;
	/** */
	UART		uart2;
	};

}
}
}
}

#endif
