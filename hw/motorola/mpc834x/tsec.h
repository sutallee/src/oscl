/*
   Copyright (C) 2006 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_hw_motorola_mpc834x_tsech_
#define _oscl_hw_motorola_mpc834x_tsech_
#include "oscl/hw/motorola/tsec/tsecreg.h"

/** */
namespace Oscl {
/** */
namespace Motorola {
/** */
namespace MPC834x {
/** */
namespace TSEC {

/** */
struct GeneralControlAndStatusMap {
	/** Offset 0x000 */
	volatile Oscl::Motorola::TSEC::TSEC_ID::Reg		tsec_id;
	/** Offset 0x004 */
	uint8_t										reserved1[0x010-0x004];
	/** Offset 0x010 */
	volatile Oscl::Motorola::TSEC::IEVENT::Reg		ievent;
	/** Offset 0x014 */
	volatile Oscl::Motorola::TSEC::IMASK::Reg		imask;
	/** Offset 0x018 */
	volatile Oscl::Motorola::TSEC::EDIS::Reg		edis;
	/** Offset 0x01C */
	uint8_t										reserved2[0x020-0x01C];
	/** Offset 0x020 */
	volatile Oscl::Motorola::TSEC::ECNTRL::Reg		ecntrl;
	/** Offset 0x024 */
	volatile Oscl::Motorola::TSEC::MINFLR::Reg		minflr;
	/** Offset 0x028 */
	volatile Oscl::Motorola::TSEC::PTV::Reg			ptv;
	/** Offset 0x02C */
	volatile Oscl::Motorola::TSEC::DMACTRL::Reg		dmactrl;
	/** Offset 0x030 */
	volatile Oscl::Motorola::TSEC::TBIPA::Reg		tbipa;
	/** Offset 0x034 */
	uint8_t										reserved3[0x038-0x034];
	/** Offset 0x038 */
	uint32_t										_unspecified;
	};

/** */
struct FifoControlAndStatusMap {
	/** Offset 0x04C */
	volatile Oscl::Motorola::TSEC::FIFO_PAUSE_CTRL::Reg	fifo_pause_ctrl;
	/** Offset 0x050 */
	uint8_t											reserved1[0x050-0x08C];
	/** Offset 0x08C */
	volatile Oscl::Motorola::TSEC::FIFO_TX_THR::Reg		fifo_tx_thr;
	/** Offset 0x090 */
	uint8_t											reserved2[0x098-0x090];
	/** Offset 0x098 */
	volatile Oscl::Motorola::TSEC::FIFO_TX_STARVE::Reg	fifo_tx_starve;
	/** Offset 0x09C */
	volatile Oscl::Motorola::TSEC::FIFO_TX_STARVE_SHUTOFF::Reg	fifo_tx_starve_shutoff;
	/** Offset 0x0A0 */
	uint8_t											reserved3[0x100-0x0A0];
	};

/** */
struct TxControlAndStatusMap {
	/** Offset 0x100 */
	volatile Oscl::Motorola::TSEC::TCTRL::Reg	tctrl;
	/** Offset 0x104 */
	volatile Oscl::Motorola::TSEC::TSTAT::Reg	tstat;
	/** Offset 0x108 */
	uint8_t									reserved1[0x10C-0x108];
	/** Offset 0x10C */
	volatile Oscl::Motorola::TSEC::TBDLEN::Reg	tbdlen;
	/** Offset 0x110 */
	Oscl::Motorola::TSEC::TXIC::Reg				txic;
	/** Offset 0x114 */
	uint8_t									reserved2[0x120-0x114];
	/** Offset 0x120 (undocumented) */
	uint32_t									ctbptrh;
	/** Offset 0x124 */
	volatile Oscl::Motorola::TSEC::CTBPTR::Reg	ctbptr;
	/** Offset 0x128 */
	uint8_t									reserved3[0x180-0x128];
	/** Offset 0x180 (undocumented) */
	uint32_t									tbptrh;
	/** Offset 0x184 */
	volatile Oscl::Motorola::TSEC::TBPTR::Reg	tbptr;
	/** Offset 0x188 */
	uint8_t									reserved4[0x200-0x188];
	/** Offset 0x200 (undocumented) */
	uint32_t									tbaseh;
	/** Offset 0x204 */
	Oscl::Motorola::TSEC::TBASE::Reg			tbase;
	/** Offset 0x208 */
	uint8_t									reserved4[0x2B0-0x208];
	/** Offset 0x2B0 */
	Oscl::Motorola::TSEC::OSTBD::Reg			ostbd;
	/** Offset 0x2B4 */
	Oscl::Motorola::TSEC::OSTBDP::Reg			ostbdp;
	/** Offset 0x2B8 (undocumented) */
	uint32_t									os32tbdp;
	/** Offset 0x2BC (undocumented) */
	uint32_t									os32iptrh;
	/** Offset 0x2C0 (undocumented) */
	uint32_t									os32iptrl;
	/** Offset 0x2C4 (undocumented) */
	uint32_t									os32tbdr;
	/** Offset 0x2C8 (undocumented) */
	uint32_t									os32iil;
	/** Offset 0x2CC */
	uint8_t									reserved5[0x300-0x2CC];
	};

/** */
struct RxControlAndStatusMap {
	/** Offset 0x300 */
	volatile Oscl::Motorola::TSEC::RCTRL::Reg	rctrl;
	/** Offset 0x304 */
	volatile Oscl::Motorola::TSEC::RSTAT::Reg	rstat;
	/** Offset 0x308 */
	uint8_t									reserved1[0x30C-0x308];
	/** Offset 0x30C */
	volatile Oscl::Motorola::TSEC::RBDLEN::Reg	rbdlen;
	/** Offset 0x310 */
	volatile Oscl::Motorola::TSEC::RXIC::Reg	rxic;
	/** Offset 0x314 */
	uint8_t									reserved2[0x324-0x314];
	/** Offset 0x324 */
	volatile Oscl::Motorola::TSEC::CRBPTR::Reg	crbptr;
	/** Offset 0x328 */
	uint8_t									reserved3[0x340-0x328];
	/** Offset 0x340 */
	volatile Oscl::Motorola::TSEC::MRBLR::Reg	mrblr;
	/** Offset 0x344 */
	uint8_t									reserved4[0x384-0x344];
	/** Offset 0x384 */
	volatile Oscl::Motorola::TSEC::RBPTR::Reg	rbptr;
	/** Offset 0x388 */
	uint8_t									reserved5[0x404-0x388];
	/** Offset 0x404 */
	volatile Oscl::Motorola::TSEC::RBASE::Reg	rbase;
	/** Offset 0x408 */
	uint8_t									reserved5[0x500-0x408];
	};

/** */
struct MacMap {
	/** Offset 0x500 */
	volatile Oscl::Motorola::TSEC::MACCFG1::Reg		maccfg1;
	/** Offset 0x504 */
	volatile Oscl::Motorola::TSEC::MACCFG2::Reg		maccfg2;
	/** Offset 0x508 */
	volatile Oscl::Motorola::TSEC::IPGIFG::Reg		ipgifg;
	/** Offset 0x50C */
	volatile Oscl::Motorola::TSEC::HAFDUP::Reg		hafdup;
	/** Offset 0x510 */
	volatile Oscl::Motorola::TSEC::MAXFRM::Reg		maxfrm;
	/** Offset 0x514 */
	uint8_t										reserved1[0x520-0x514];
	/** Offset 0x520 */
	volatile Oscl::Motorola::TSEC::MIIMCFG::Reg		miimcfg;
	/** Offset 0x524 */
	volatile Oscl::Motorola::TSEC::MIIMCOM::Reg		miimcom;
	/** Offset 0x528 */
	volatile Oscl::Motorola::TSEC::MIIMADD::Reg		miimadd;
	/** Offset 0x52C */
	volatile Oscl::Motorola::TSEC::MIIMCON::Reg		miimcon;
	/** Offset 0x530 */
	volatile Oscl::Motorola::TSEC::MIIMSTAT::Reg	miimstat;
	/** Offset 0x534 */
	volatile Oscl::Motorola::TSEC::MIIMIND::Reg		miimind;
	/** Offset 0x538 */
	uint8_t										reserved2[0x53C-0x538];
	/** Offset 0x53C */
	volatile Oscl::Motorola::TSEC::IFSTAT::Reg		ifstat;
	/** Offset 0x540 */
	volatile Oscl::Motorola::TSEC::MACSTNADDR1::Reg	macstnaddr1;
	/** Offset 0x544 */
	volatile Oscl::Motorola::TSEC::MACSTNADDR2::Reg	macstnaddr2;
	/** Offset 0x548 */
	uint8_t										reserved3[0x680-0x548];
	};

/** */
struct RmonMibMap {
	/** Offset 0x680 */
	volatile Oscl::Motorola::TSEC::TR64::Reg		tr64;
	/** Offset 0x684 */
	volatile Oscl::Motorola::TSEC::TR127::Reg		tr127;
	/** Offset 0x688 */
	volatile Oscl::Motorola::TSEC::TR255::Reg		tr255;
	/** Offset 0x68C */
	volatile Oscl::Motorola::TSEC::TR511::Reg		tr511;
	/** Offset 0x690 */
	volatile Oscl::Motorola::TSEC::TR1K::Reg		tr1K;
	/** Offset 0x694 */
	volatile Oscl::Motorola::TSEC::TRMAX::Reg		trmax;
	/** Offset 0x698 */
	volatile Oscl::Motorola::TSEC::TRMGV::Reg		trmgv;
	};

/** */
struct ReceiveCounterMap {
	/** Offset 0x69C */
	volatile Oscl::Motorola::TSEC::RBYT::Reg		rbyt;
	/** Offset 0x6A0 */
	volatile Oscl::Motorola::TSEC::RPKT::Reg		rpkt;
	/** Offset 0x6A4 */
	volatile Oscl::Motorola::TSEC::RFCS::Reg		rfcs;
	/** Offset 0x6A8 */
	volatile Oscl::Motorola::TSEC::RMCA::Reg		rmca;
	/** Offset 0x6AC */
	volatile Oscl::Motorola::TSEC::RBCA::Reg		rbca;
	/** Offset 0x6B0 */
	volatile Oscl::Motorola::TSEC::RXCF::Reg		rxcf;
	/** Offset 0x6B4 */
	volatile Oscl::Motorola::TSEC::RXPF::Reg		rxpf;
	/** Offset 0x6B8 */
	volatile Oscl::Motorola::TSEC::RXUO::Reg		rxpuo;
	/** Offset 0x6BC */
	volatile Oscl::Motorola::TSEC::RALN::Reg		raln;
	/** Offset 0x6C0 */
	volatile Oscl::Motorola::TSEC::RFLR::Reg		rflr;
	/** Offset 0x6C4 */
	volatile Oscl::Motorola::TSEC::RCDE::Reg		rcde;
	/** Offset 0x6C8 */
	volatile Oscl::Motorola::TSEC::RCSE::Reg		rcse;
	/** Offset 0x6CC */
	volatile Oscl::Motorola::TSEC::RUND::Reg		rund;
	/** Offset 0x6D0 */
	volatile Oscl::Motorola::TSEC::ROVR::Reg		rovr;
	/** Offset 0x6D4 */
	volatile Oscl::Motorola::TSEC::RFRG::Reg		rfrg;
	/** Offset 0x6D8 */
	volatile Oscl::Motorola::TSEC::RJBR::Reg		rjbr;
	/** Offset 0x6DC */
	volatile Oscl::Motorola::TSEC::RDRP::Reg		rdrp;
	};

/** */
struct TransmitCounterMap {
	/** Offset 0x6E0 */
	volatile Oscl::Motorola::TSEC::TBYT::Reg		tbyt;
	/** Offset 0x6E4 */
	volatile Oscl::Motorola::TSEC::TPKT::Reg		tpkt;
	/** Offset 0x6E8 */
	volatile Oscl::Motorola::TSEC::TMCA::Reg		tmca;
	/** Offset 0x6EC */
	volatile Oscl::Motorola::TSEC::TBCA::Reg		tbca;
	/** Offset 0x6F0 */
	volatile Oscl::Motorola::TSEC::TXPF::Reg		txpf;
	/** Offset 0x6F4 */
	volatile Oscl::Motorola::TSEC::TDFR::Reg		tdfr;
	/** Offset 0x6F8 */
	volatile Oscl::Motorola::TSEC::TEDF::Reg		tedf;
	/** Offset 0x6FC */
	volatile Oscl::Motorola::TSEC::TSCL::Reg		tscl;
	/** Offset 0x700 */
	volatile Oscl::Motorola::TSEC::TMCL::Reg		tmcl;
	/** Offset 0x704 */
	volatile Oscl::Motorola::TSEC::TLCL::Reg		tlcl;
	/** Offset 0x708 */
	volatile Oscl::Motorola::TSEC::TXCL::Reg		txcl;
	/** Offset 0x70C */
	volatile Oscl::Motorola::TSEC::TNCL::Reg		tncl;
	/** Offset 0x710 */
	uint8_t										reserved1[0x714-0x710];
	/** Offset 0x714 */
	volatile Oscl::Motorola::TSEC::TDRP::Reg		tdrp;
	/** Offset 0x718 */
	volatile Oscl::Motorola::TSEC::TJBR::Reg		tjbr;
	/** Offset 0x71C */
	volatile Oscl::Motorola::TSEC::TFCS::Reg		tfcs;
	/** Offset 0x720 */
	volatile Oscl::Motorola::TSEC::TXCF::Reg		txcf;
	/** Offset 0x724 */
	volatile Oscl::Motorola::TSEC::TOVR::Reg		tovr;
	/** Offset 0x728 */
	volatile Oscl::Motorola::TSEC::TUND::Reg		tund;
	/** Offset 0x72C */
	volatile Oscl::Motorola::TSEC::TFRG::Reg		tfrg;
	};

/** */
struct GeneralMap {
	/** Offset 0x730 */
	volatile Oscl::Motorola::TSEC::CAR1::Reg		car1;
	/** Offset 0x734 */
	volatile Oscl::Motorola::TSEC::CAR2::Reg		car2;
	/** Offset 0x738 */
	volatile Oscl::Motorola::TSEC::CAM1::Reg		cam1;
	/** Offset 0x73C */
	volatile Oscl::Motorola::TSEC::CAM2::Reg		cam2;
	/** Offset 0x740 */
	uint8_t										reserved1[0x800-0x740];
	};

/** */
struct HashFunctionMap {
	/** Offset 0x800 */
	volatile Oscl::Motorola::TSEC::IADDR::Reg		iaddr0;
	/** Offset 0x804 */
	volatile Oscl::Motorola::TSEC::IADDR::Reg		iaddr1;
	/** Offset 0x808 */
	volatile Oscl::Motorola::TSEC::IADDR::Reg		iaddr2;
	/** Offset 0x80C */
	volatile Oscl::Motorola::TSEC::IADDR::Reg		iaddr3;
	/** Offset 0x810 */
	volatile Oscl::Motorola::TSEC::IADDR::Reg		iaddr4;
	/** Offset 0x814 */
	volatile Oscl::Motorola::TSEC::IADDR::Reg		iaddr5;
	/** Offset 0x818 */
	volatile Oscl::Motorola::TSEC::IADDR::Reg		iaddr6;
	/** Offset 0x81C */
	volatile Oscl::Motorola::TSEC::IADDR::Reg		iaddr7;
	/** Offset 0x820 */
	uint8_t										reserved1[0x880-0x820];
	/** Offset 0x880 */
	volatile Oscl::Motorola::TSEC::GADDR::Reg		gaddr0;
	/** Offset 0x884 */
	volatile Oscl::Motorola::TSEC::GADDR::Reg		gaddr1;
	/** Offset 0x888 */
	volatile Oscl::Motorola::TSEC::GADDR::Reg		gaddr2;
	/** Offset 0x88C */
	volatile Oscl::Motorola::TSEC::GADDR::Reg		gaddr3;
	/** Offset 0x890 */
	volatile Oscl::Motorola::TSEC::GADDR::Reg		gaddr4;
	/** Offset 0x894 */
	volatile Oscl::Motorola::TSEC::GADDR::Reg		gaddr5;
	/** Offset 0x898 */
	volatile Oscl::Motorola::TSEC::GADDR::Reg		gaddr6;
	/** Offset 0x89C */
	volatile Oscl::Motorola::TSEC::GADDR::Reg		gaddr7;
	/** Offset 0x8A0 */
	uint8_t										reserved1[0xBF8-0x8A0];
	};

/** */
struct AttributeMap {
	/** Offset 0xBF8 */
	volatile Oscl::Motorola::TSEC::ATTR::Reg		attr;
	/** Offset 0xBFC */
	volatile Oscl::Motorola::TSEC::ATTRELI::Reg		attreli;
	};

/** */
struct FutureExpansionSpaceMap {
	/** Offset 0x4C00 */
	uint8_t										reserved1[0x50000-0x4C00];
	};

}
}
}
}

#endif
