/*
   Copyright (C) 2006 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_hw_motorola_mpc834x_pith_
#define _oscl_hw_motorola_mpc834x_pith_
#include "pitreg.h"

/** */
namespace Oscl {
/** */
namespace Motorola {
/** */
namespace MPC834x {
/** */
namespace PIT {

/** */
struct RegisterMap {
	/** Offset 0x00 */
	volatile PTCNR::Reg		ptcnr;
	/** Offset 0x04 */
	volatile PTLDR::Reg		ptldr;
	/** Offset 0x08 */
	volatile PTPSR::Reg		ptpsr;
	/** Offset 0x0C */
	volatile PTCTR::Reg		ptctr;
	/** Offset 0x10 */
	volatile PTEVR::Reg		ptevr;
	/** Offset 0x14 */
	uint8_t				reserved[0x20-0x14];
	};

}
}
}
}

#endif
