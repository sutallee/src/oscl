/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _hw_mot_mpc8xx_spih_
#define _hw_mot_mpc8xx_spih_
#include "spireg.h"
#include "pramreg.h"

/** */
namespace Oscl {
/** */
namespace Mot8xx {
/** */
namespace Spi {

/** */
typedef struct Map {
	SPMODE::Reg		spmode;
	uint8_t		reserved1[0xAA6-0xAA2];
	SPIE::Reg		spie;
	uint8_t		reserved2[0xAAA-0xAA7];
	SPIM::Reg		spim;
	uint8_t		reserved3[0xAAD-0xAAB];
	SPCOM::Reg		spcom;
	uint8_t		reserved4[0xAB0-0xAAE];
	}Map;

/** */
typedef struct Pram {
	volatile Oscl::Mot8xx::PRAM::RBASE::Reg			rbase;
	volatile Oscl::Mot8xx::PRAM::TBASE::Reg			tbase;
	volatile Oscl::Mot8xx::Spi::PRAM::RFCR::Reg		rfcr;
	volatile Oscl::Mot8xx::Spi::PRAM::TFCR::Reg		tfcr;
	volatile Oscl::Mot8xx::PRAM::MRBLR::Reg			mrblr;
	volatile Oscl::Mot8xx::PRAM::RSTATE::Reg		rstate;
	volatile Oscl::Mot8xx::PRAM::RxDataPtr::Reg		rxDataPtr;
	volatile Oscl::Mot8xx::PRAM::RBPTR::Reg			rbptr;
	volatile Oscl::Mot8xx::PRAM::RxByteCount::Reg	rxByteCount;
	volatile Oscl::Mot8xx::PRAM::RxTemp::Reg		rxTemp;
	volatile Oscl::Mot8xx::PRAM::TSTATE::Reg		tstate;
	volatile Oscl::Mot8xx::PRAM::TxDataPtr::Reg		txDataPtr;
	volatile Oscl::Mot8xx::PRAM::TBPTR::Reg			tbptr;
	volatile Oscl::Mot8xx::PRAM::TxByteCount::Reg	txByteCount;
	volatile Oscl::Mot8xx::PRAM::TxTemp::Reg		txTemp;
	}Pram;

};
};
};

#endif
