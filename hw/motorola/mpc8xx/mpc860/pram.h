/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _hw_mot_mpc8xx_mpc860_pramh_
#define _hw_mot_mpc8xx_mpc860_pramh_
#include "oscl/hw/motorola/mpc8xx/pram/dsp.h"
#include "oscl/hw/motorola/mpc8xx/pram/i2c.h"
#include "oscl/hw/motorola/mpc8xx/pram/idma.h"
#include "oscl/hw/motorola/mpc8xx/pram/pip.h"
#include "oscl/hw/motorola/mpc8xx/pram/scc.h"
#include "oscl/hw/motorola/mpc8xx/pram/smc.h"
#include "oscl/hw/motorola/mpc8xx/pram/spi.h"
#include "oscl/hw/motorola/mpc8xx/pram/timer.h"

/** */
namespace Oscl {
/** */
namespace Mot8xx {
/** */
namespace Mpc860 {
/** */
namespace Pram {

typedef union Page1 {
	struct {
		Oscl::Mot8xx::Pram::SCC::ENET	scc1;
		unsigned char 					reserved[0xC0-0xA4];
		Oscl::Mot8xx::Pram::IDMA		idma1;
		}enet;
	struct {
		union {
			Oscl::Mot8xx::Pram::SCC::UART	uart;
			Oscl::Mot8xx::Pram::SCC::HDLC	hdlc;
			Oscl::Mot8xx::Pram::SCC::AHDLC	ahdlc;
			Oscl::Mot8xx::Pram::SCC::BISYNC	bisync;
			Oscl::Mot8xx::Pram::SCC::TRANS	trans;
			unsigned char	raw[0x80];
			}scc;
		Oscl::Mot8xx::Pram::I2C		i2c;
		unsigned char				misc[0x10];
		Oscl::Mot8xx::Pram::IDMA	idma1;
		}other;
	unsigned char	raw[0x100];
	}Page1;

typedef union Page2 {
	struct {
		Oscl::Mot8xx::Pram::SCC::ENET	scc1;
		unsigned char 					reserved[0xC0-0xA4];
		Oscl::Mot8xx::Pram::IDMA		idma2;
		}enet;
	struct {
		union {
			Oscl::Mot8xx::Pram::SCC::UART	uart;
			Oscl::Mot8xx::Pram::SCC::HDLC	hdlc;
			Oscl::Mot8xx::Pram::SCC::AHDLC	ahdlc;
			Oscl::Mot8xx::Pram::SCC::BISYNC	bisync;
			Oscl::Mot8xx::Pram::SCC::TRANS	trans;
			unsigned char	raw[0x80];
			}scc;
		Oscl::Mot8xx::Pram::SPI			spi;
		Oscl::Mot8xx::Pram::RiscTimer	timer;
		Oscl::Mot8xx::Pram::IDMA		idma2;
		}other;
	unsigned char	raw[0x100];
	}Page2;

typedef union Page3 {
	struct {
		Oscl::Mot8xx::Pram::SCC::ENET	scc1;
		unsigned char 					reserved[0xC0-0xA4];
		Oscl::Mot8xx::Pram::DSP			dspRx;
		}enet;
	struct {
		union {
			Oscl::Mot8xx::Pram::SCC::UART	uart;
			Oscl::Mot8xx::Pram::SCC::HDLC	hdlc;
			Oscl::Mot8xx::Pram::SCC::AHDLC	ahdlc;
			Oscl::Mot8xx::Pram::SCC::BISYNC	bisync;
			Oscl::Mot8xx::Pram::SCC::TRANS	trans;
			unsigned char	raw[0x80];
			}scc;
		Oscl::Mot8xx::Pram::SMC			smc1;
		Oscl::Mot8xx::Pram::DSP			dspRx;
		}other;
	unsigned char	raw[0x100];
	}Page3;

typedef union Page4 {
	struct {
		Oscl::Mot8xx::Pram::SCC::ENET	scc1;
		unsigned char 					reserved[0xC0-0xA4];
		Oscl::Mot8xx::Pram::DSP			dspRx;
		}enet;
	struct {
		union {
			Oscl::Mot8xx::Pram::SCC::UART	uart;
			Oscl::Mot8xx::Pram::SCC::HDLC	hdlc;
			Oscl::Mot8xx::Pram::SCC::AHDLC	ahdlc;
			Oscl::Mot8xx::Pram::SCC::BISYNC	bisync;
			Oscl::Mot8xx::Pram::SCC::TRANS	trans;
			unsigned char	raw[0x80];
			}scc;
		union {
			Oscl::Mot8xx::Pram::SMC			smc2;
			Oscl::Mot8xx::Pram::PIP			pip;
			}smcpip;
		Oscl::Mot8xx::Pram::DSP			dspTx;
		}other;
	unsigned char	raw[0x100];
	}Page4;

}
}
}
}


#endif
