/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _hw_mot8xx_sitimerregh_
#define _hw_mot8xx_sitimerregh_
#include <stdint.h>
namespace Oscl { // Namespace description
	namespace Mot8xx { // Namespace description
		namespace SiTimer { // Namespace description
			namespace TBSCR { // Register description
				typedef uint16_t	Reg;
				namespace TBIRQ { // Field Description
					enum {Lsb = 8};
					enum {FieldMask = 0x0000FF00};
					enum {Value_Level0 = 0x80};
					enum {ValueMask_Level0 = 0x00008000};
					enum {Value_Level1 = 0x40};
					enum {ValueMask_Level1 = 0x00004000};
					enum {Value_Level2 = 0x20};
					enum {ValueMask_Level2 = 0x00002000};
					enum {Value_Level3 = 0x10};
					enum {ValueMask_Level3 = 0x00001000};
					enum {Value_Level4 = 0x8};
					enum {ValueMask_Level4 = 0x00000800};
					enum {Value_Level5 = 0x4};
					enum {ValueMask_Level5 = 0x00000400};
					enum {Value_Level6 = 0x2};
					enum {ValueMask_Level6 = 0x00000200};
					enum {Value_Level7 = 0x1};
					enum {ValueMask_Level7 = 0x00000100};
					};
				namespace REFA { // Field Description
					enum {Lsb = 7};
					enum {FieldMask = 0x00000080};
					enum {Value_NotPending = 0x0};
					enum {ValueMask_NotPending = 0x00000000};
					enum {Value_Pending = 0x1};
					enum {ValueMask_Pending = 0x00000080};
					enum {Value_Clear = 0x1};
					enum {ValueMask_Clear = 0x00000080};
					};
				namespace REFB { // Field Description
					enum {Lsb = 6};
					enum {FieldMask = 0x00000040};
					enum {Value_NotPending = 0x0};
					enum {ValueMask_NotPending = 0x00000000};
					enum {Value_Pending = 0x1};
					enum {ValueMask_Pending = 0x00000040};
					enum {Value_Clear = 0x1};
					enum {ValueMask_Clear = 0x00000040};
					};
				namespace REFAE { // Field Description
					enum {Lsb = 3};
					enum {FieldMask = 0x00000008};
					enum {Value_Disable = 0x0};
					enum {ValueMask_Disable = 0x00000000};
					enum {Value_Enable = 0x1};
					enum {ValueMask_Enable = 0x00000008};
					};
				namespace REFBE { // Field Description
					enum {Lsb = 2};
					enum {FieldMask = 0x00000004};
					enum {Value_Disable = 0x0};
					enum {ValueMask_Disable = 0x00000000};
					enum {Value_Enable = 0x1};
					enum {ValueMask_Enable = 0x00000004};
					};
				namespace TBF { // Field Description
					enum {Lsb = 1};
					enum {FieldMask = 0x00000002};
					enum {Value_IgnoreFreeze = 0x0};
					enum {ValueMask_IgnoreFreeze = 0x00000000};
					enum {Value_StopOnFreeze = 0x1};
					enum {ValueMask_StopOnFreeze = 0x00000002};
					};
				namespace TBE { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x00000001};
					enum {Value_Disable = 0x0};
					enum {ValueMask_Disable = 0x00000000};
					enum {Value_Enable = 0x1};
					enum {ValueMask_Enable = 0x00000001};
					};
				};
			namespace TBREFA { // Register description
				typedef uint32_t	Reg;
				};
			namespace TBREFB { // Register description
				typedef uint32_t	Reg;
				};
			namespace RTCSC { // Register description
				typedef uint16_t	Reg;
				};
			namespace RTC { // Register description
				typedef uint32_t	Reg;
				};
			namespace RTSEC { // Register description
				typedef uint32_t	Reg;
				};
			namespace RTCAL { // Register description
				typedef uint32_t	Reg;
				};
			namespace PISCR { // Register description
				typedef uint16_t	Reg;
				namespace PIRQ { // Field Description
					enum {Lsb = 8};
					enum {FieldMask = 0x0000FF00};
					enum {Value_Level0 = 0x80};
					enum {ValueMask_Level0 = 0x00008000};
					enum {Value_Level1 = 0x40};
					enum {ValueMask_Level1 = 0x00004000};
					enum {Value_Level2 = 0x20};
					enum {ValueMask_Level2 = 0x00002000};
					enum {Value_Level3 = 0x10};
					enum {ValueMask_Level3 = 0x00001000};
					enum {Value_Level4 = 0x8};
					enum {ValueMask_Level4 = 0x00000800};
					enum {Value_Level5 = 0x4};
					enum {ValueMask_Level5 = 0x00000400};
					enum {Value_Level6 = 0x2};
					enum {ValueMask_Level6 = 0x00000200};
					enum {Value_Level7 = 0x1};
					enum {ValueMask_Level7 = 0x00000100};
					};
				namespace PS { // Field Description
					enum {Lsb = 7};
					enum {FieldMask = 0x00000080};
					enum {Value_Clear = 0x1};
					enum {ValueMask_Clear = 0x00000080};
					enum {Value_Pending = 0x1};
					enum {ValueMask_Pending = 0x00000080};
					enum {Value_NotPending = 0x0};
					enum {ValueMask_NotPending = 0x00000000};
					};
				namespace PIE { // Field Description
					enum {Lsb = 2};
					enum {FieldMask = 0x00000004};
					enum {Value_Disable = 0x0};
					enum {ValueMask_Disable = 0x00000000};
					enum {Value_Enable = 0x1};
					enum {ValueMask_Enable = 0x00000004};
					};
				namespace PITF { // Field Description
					enum {Lsb = 1};
					enum {FieldMask = 0x00000002};
					enum {Value_IgnoreFreeze = 0x0};
					enum {ValueMask_IgnoreFreeze = 0x00000000};
					enum {Value_StopOnFreeze = 0x1};
					enum {ValueMask_StopOnFreeze = 0x00000002};
					};
				namespace PTE { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x00000001};
					enum {Value_Disable = 0x0};
					enum {ValueMask_Disable = 0x00000000};
					enum {Value_Enable = 0x1};
					enum {ValueMask_Enable = 0x00000001};
					};
				};
			namespace PITC { // Register description
				typedef uint16_t	Reg;
				};
			namespace PITR { // Register description
				typedef uint16_t	Reg;
				};
			};
		};
	};
#endif
