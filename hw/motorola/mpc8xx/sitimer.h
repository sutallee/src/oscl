/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _hw_mot_mpc8xx_sitimerh_
#define _hw_mot_mpc8xx_sitimerh_
#include "sitimerreg.h"

/** */
namespace Oscl {
/** */
namespace Mot8xx {
/** */
namespace SiTimer {

/** */
typedef struct Map {
	volatile TBSCR::Reg		tbscr;
	uint8_t				reserved1[0x204-0x202];
	volatile TBREFA::Reg	tbrefa;
	volatile TBREFB::Reg	tbrefb;
	uint8_t				reserved2[0x220-0x20C];
	volatile RTCSC::Reg		rtcsc;
	uint8_t				reserved3[0x224-0x222];
	volatile RTC::Reg		rtc;
	volatile RTSEC::Reg		rtsec;
	volatile RTCAL::Reg		rtcal;
	uint8_t				reserved4[0x240-0x230];
	volatile PISCR::Reg		piscr;
	uint8_t				reserved5[0x244-0x242];
	volatile PITC::Reg		pitc;
	uint8_t				reserved6[0x248-0x246];
	volatile PITR::Reg		pitr;
	uint8_t				reserved7[0x24C-0x24A];
	}Map;

};
};
};

#endif
