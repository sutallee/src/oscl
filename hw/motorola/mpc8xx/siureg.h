/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _hw_mot8xx_siuregh_
#define _hw_mot8xx_siuregh_
#include <stdint.h>
namespace Oscl { // Namespace description
	namespace Mot8xx { // Namespace description
		namespace Siu { // Namespace description
			namespace SIUMCR { // Register description
				typedef uint32_t	Reg;
				namespace EARB { // Field Description
					enum {Lsb = 31};
					enum {FieldMask = 0x80000000};
					enum {Value_InternalArbitration = 0x0};
					enum {ValueMask_InternalArbitration = 0x00000000};
					enum {Value_ExternalArbitration = 0x1};
					enum {ValueMask_ExternalArbitration = 0x80000000};
					};
				namespace EARP { // Field Description
					enum {Lsb = 28};
					enum {FieldMask = 0x70000000};
					enum {Value_LowestPriority = 0x0};
					enum {ValueMask_LowestPriority = 0x00000000};
					enum {Value_HighestPriority = 0x7};
					enum {ValueMask_HighestPriority = 0x70000000};
					};
				namespace DSHW { // Field Description
					enum {Lsb = 23};
					enum {FieldMask = 0x00800000};
					enum {Value_DisableInternalDataShowCycles = 0x0};
					enum {ValueMask_DisableInternalDataShowCycles = 0x00000000};
					enum {Value_EnableInternalDataShowCycles = 0x1};
					enum {ValueMask_EnableInternalDataShowCycles = 0x00800000};
					};
				namespace DBGC { // Field Description
					enum {Lsb = 21};
					enum {FieldMask = 0x00600000};
					enum {Value_IP_B = 0x0};
					enum {ValueMask_IP_B = 0x00000000};
					enum {Value_WP = 0x1};
					enum {ValueMask_WP = 0x00200000};
					enum {Value_VFLS = 0x3};
					enum {ValueMask_VFLS = 0x00600000};
					};
				namespace DBPC { // Field Description
					enum {Lsb = 19};
					enum {FieldMask = 0x00180000};
					enum {Value_Mode00 = 0x0};
					enum {ValueMask_Mode00 = 0x00000000};
					enum {Value_Mode01 = 0x1};
					enum {ValueMask_Mode01 = 0x00080000};
					enum {Value_Mode11 = 0x3};
					enum {ValueMask_Mode11 = 0x00180000};
					};
				namespace FRC { // Field Description
					enum {Lsb = 17};
					enum {FieldMask = 0x00020000};
					enum {Value_FRZ = 0x0};
					enum {ValueMask_FRZ = 0x00000000};
					enum {Value_IRQ6 = 0x1};
					enum {ValueMask_IRQ6 = 0x00020000};
					};
				namespace DLK { // Field Description
					enum {Lsb = 16};
					enum {FieldMask = 0x00010000};
					enum {Value_Unlocked = 0x0};
					enum {ValueMask_Unlocked = 0x00000000};
					enum {Value_Locked = 0x1};
					enum {ValueMask_Locked = 0x00010000};
					};
				namespace OPAR { // Field Description
					enum {Lsb = 15};
					enum {FieldMask = 0x00008000};
					enum {Value_EvenParity = 0x0};
					enum {ValueMask_EvenParity = 0x00000000};
					enum {Value_OddParity = 0x1};
					enum {ValueMask_OddParity = 0x00008000};
					};
				namespace PNCS { // Field Description
					enum {Lsb = 14};
					enum {FieldMask = 0x00004000};
					enum {Value_DisableParity = 0x0};
					enum {ValueMask_DisableParity = 0x00000000};
					enum {Value_EnableParity = 0x1};
					enum {ValueMask_EnableParity = 0x00004000};
					};
				namespace DPC { // Field Description
					enum {Lsb = 13};
					enum {FieldMask = 0x00002000};
					enum {Value_IRQ = 0x0};
					enum {ValueMask_IRQ = 0x00000000};
					enum {Value_Parity = 0x1};
					enum {ValueMask_Parity = 0x00002000};
					};
				namespace MPRE { // Field Description
					enum {Lsb = 12};
					enum {FieldMask = 0x00001000};
					enum {Value_IRQ = 0x0};
					enum {ValueMask_IRQ = 0x00000000};
					enum {Value_RSV = 0x1};
					enum {ValueMask_RSV = 0x00001000};
					};
				namespace MLRC { // Field Description
					enum {Lsb = 10};
					enum {FieldMask = 0x00000C00};
					enum {Value_IRQ4 = 0x0};
					enum {ValueMask_IRQ4 = 0x00000000};
					enum {Value_TriState = 0x1};
					enum {ValueMask_TriState = 0x00000400};
					enum {Value_KrRetry = 0x2};
					enum {ValueMask_KrRetry = 0x00000800};
					enum {Value_SPKROUT = 0x2};
					enum {ValueMask_SPKROUT = 0x00000800};
					};
				namespace AEME { // Field Description
					enum {Lsb = 9};
					enum {FieldMask = 0x00000200};
					enum {Value_ExternalMasterDisabled = 0x0};
					enum {ValueMask_ExternalMasterDisabled = 0x00000000};
					enum {Value_ExternalMasterEnabled = 0x1};
					enum {ValueMask_ExternalMasterEnabled = 0x00000200};
					};
				namespace SEME { // Field Description
					enum {Lsb = 8};
					enum {FieldMask = 0x00000100};
					enum {Value_ExternalMasterDisabled = 0x0};
					enum {ValueMask_ExternalMasterDisabled = 0x00000000};
					enum {Value_ExternalMasterEnabled = 0x1};
					enum {ValueMask_ExternalMasterEnabled = 0x00000100};
					};
				namespace BSC { // Field Description
					enum {Lsb = 7};
					enum {FieldMask = 0x00000080};
					enum {Value_Dedicated = 0x0};
					enum {ValueMask_Dedicated = 0x00000000};
					enum {Value_Dual = 0x1};
					enum {ValueMask_Dual = 0x00000080};
					};
				namespace GB5E { // Field Description
					enum {Lsb = 6};
					enum {FieldMask = 0x00000040};
					enum {Value_BDIP = 0x0};
					enum {ValueMask_BDIP = 0x00000000};
					enum {Value_GPL_B5 = 0x1};
					enum {ValueMask_GPL_B5 = 0x00000040};
					};
				namespace B2DD { // Field Description
					enum {Lsb = 5};
					enum {FieldMask = 0x00000020};
					enum {Value_CS2Only = 0x0};
					enum {ValueMask_CS2Only = 0x00000000};
					enum {Value_CS2AndGPL_x2 = 0x1};
					enum {ValueMask_CS2AndGPL_x2 = 0x00000020};
					};
				namespace B3DD { // Field Description
					enum {Lsb = 4};
					enum {FieldMask = 0x00000010};
					enum {Value_CS3Only = 0x0};
					enum {ValueMask_CS3Only = 0x00000000};
					enum {Value_CS3AndGPL_x3 = 0x1};
					enum {ValueMask_CS3AndGPL_x3 = 0x00000010};
					};
				};
			namespace SYPCR { // Register description
				typedef uint32_t	Reg;
				namespace SWTC { // Field Description
					enum {Lsb = 16};
					enum {FieldMask = 0xFFFF0000};
					enum {Value_Max = 0xFFFF};
					enum {ValueMask_Max = 0xFFFF0000};
					};
				namespace BMT { // Field Description
					enum {Lsb = 8};
					enum {FieldMask = 0x0000FF00};
					enum {Value_Max = 0xFF};
					enum {ValueMask_Max = 0x0000FF00};
					};
				namespace BME { // Field Description
					enum {Lsb = 7};
					enum {FieldMask = 0x00000080};
					enum {Value_BusMonitorDisabled = 0x0};
					enum {ValueMask_BusMonitorDisabled = 0x00000000};
					enum {Value_BusMonitorEnabled = 0x1};
					enum {ValueMask_BusMonitorEnabled = 0x00000080};
					};
				namespace SWF { // Field Description
					enum {Lsb = 3};
					enum {FieldMask = 0x00000008};
					enum {Value_Continue = 0x0};
					enum {ValueMask_Continue = 0x00000000};
					enum {Value_Stop = 0x1};
					enum {ValueMask_Stop = 0x00000008};
					};
				namespace SWE { // Field Description
					enum {Lsb = 2};
					enum {FieldMask = 0x00000004};
					enum {Value_Disabled = 0x0};
					enum {ValueMask_Disabled = 0x00000000};
					enum {Value_Enabled = 0x1};
					enum {ValueMask_Enabled = 0x00000004};
					};
				namespace SWRI { // Field Description
					enum {Lsb = 1};
					enum {FieldMask = 0x00000002};
					enum {Value_NMI = 0x0};
					enum {ValueMask_NMI = 0x00000000};
					enum {Value_HRESET = 0x1};
					enum {ValueMask_HRESET = 0x00000002};
					};
				namespace SWP { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x00000001};
					enum {Value_NotPrescaled = 0x0};
					enum {ValueMask_NotPrescaled = 0x00000000};
					enum {Value_Prescale2048 = 0x1};
					enum {ValueMask_Prescale2048 = 0x00000001};
					};
				};
			namespace SWSR { // Register description
				typedef uint16_t	Reg;
				enum {WatchDogResetValue1 = 21868};
				enum {WatchDogResetValue2 = 43577};
				};
			namespace SIPEND { // Register description
				typedef uint32_t	Reg;
				namespace IRM0 { // Field Description
					enum {Lsb = 31};
					enum {FieldMask = 0x80000000};
					enum {Value_Disabled = 0x0};
					enum {ValueMask_Disabled = 0x00000000};
					enum {Value_Enabled = 0x1};
					enum {ValueMask_Enabled = 0x80000000};
					};
				namespace LVM0 { // Field Description
					enum {Lsb = 30};
					enum {FieldMask = 0x40000000};
					enum {Value_Disabled = 0x0};
					enum {ValueMask_Disabled = 0x00000000};
					enum {Value_Enabled = 0x1};
					enum {ValueMask_Enabled = 0x40000000};
					};
				namespace IRM1 { // Field Description
					enum {Lsb = 29};
					enum {FieldMask = 0x20000000};
					enum {Value_Disabled = 0x0};
					enum {ValueMask_Disabled = 0x00000000};
					enum {Value_Enabled = 0x1};
					enum {ValueMask_Enabled = 0x20000000};
					};
				namespace LVM1 { // Field Description
					enum {Lsb = 28};
					enum {FieldMask = 0x10000000};
					enum {Value_Disabled = 0x0};
					enum {ValueMask_Disabled = 0x00000000};
					enum {Value_Enabled = 0x1};
					enum {ValueMask_Enabled = 0x10000000};
					};
				namespace IRM2 { // Field Description
					enum {Lsb = 27};
					enum {FieldMask = 0x08000000};
					enum {Value_Disabled = 0x0};
					enum {ValueMask_Disabled = 0x00000000};
					enum {Value_Enabled = 0x1};
					enum {ValueMask_Enabled = 0x08000000};
					};
				namespace LVM2 { // Field Description
					enum {Lsb = 26};
					enum {FieldMask = 0x04000000};
					enum {Value_Disabled = 0x0};
					enum {ValueMask_Disabled = 0x00000000};
					enum {Value_Enabled = 0x1};
					enum {ValueMask_Enabled = 0x04000000};
					};
				namespace IRM3 { // Field Description
					enum {Lsb = 25};
					enum {FieldMask = 0x02000000};
					enum {Value_Disabled = 0x0};
					enum {ValueMask_Disabled = 0x00000000};
					enum {Value_Enabled = 0x1};
					enum {ValueMask_Enabled = 0x02000000};
					};
				namespace LVM3 { // Field Description
					enum {Lsb = 24};
					enum {FieldMask = 0x01000000};
					enum {Value_Disabled = 0x0};
					enum {ValueMask_Disabled = 0x00000000};
					enum {Value_Enabled = 0x1};
					enum {ValueMask_Enabled = 0x01000000};
					};
				namespace IRM4 { // Field Description
					enum {Lsb = 23};
					enum {FieldMask = 0x00800000};
					enum {Value_Disabled = 0x0};
					enum {ValueMask_Disabled = 0x00000000};
					enum {Value_Enabled = 0x1};
					enum {ValueMask_Enabled = 0x00800000};
					};
				namespace LVM4 { // Field Description
					enum {Lsb = 22};
					enum {FieldMask = 0x00400000};
					enum {Value_Disabled = 0x0};
					enum {ValueMask_Disabled = 0x00000000};
					enum {Value_Enabled = 0x1};
					enum {ValueMask_Enabled = 0x00400000};
					};
				namespace IRM5 { // Field Description
					enum {Lsb = 21};
					enum {FieldMask = 0x00200000};
					enum {Value_Disabled = 0x0};
					enum {ValueMask_Disabled = 0x00000000};
					enum {Value_Enabled = 0x1};
					enum {ValueMask_Enabled = 0x00200000};
					};
				namespace LVM5 { // Field Description
					enum {Lsb = 20};
					enum {FieldMask = 0x00100000};
					enum {Value_Disabled = 0x0};
					enum {ValueMask_Disabled = 0x00000000};
					enum {Value_Enabled = 0x1};
					enum {ValueMask_Enabled = 0x00100000};
					};
				namespace IRM6 { // Field Description
					enum {Lsb = 19};
					enum {FieldMask = 0x00080000};
					enum {Value_Disabled = 0x0};
					enum {ValueMask_Disabled = 0x00000000};
					enum {Value_Enabled = 0x1};
					enum {ValueMask_Enabled = 0x00080000};
					};
				namespace LVM6 { // Field Description
					enum {Lsb = 18};
					enum {FieldMask = 0x00040000};
					enum {Value_Disabled = 0x0};
					enum {ValueMask_Disabled = 0x00000000};
					enum {Value_Enabled = 0x1};
					enum {ValueMask_Enabled = 0x00040000};
					};
				namespace IRM7 { // Field Description
					enum {Lsb = 17};
					enum {FieldMask = 0x00020000};
					enum {Value_Disabled = 0x0};
					enum {ValueMask_Disabled = 0x00000000};
					enum {Value_Enabled = 0x1};
					enum {ValueMask_Enabled = 0x00020000};
					};
				namespace LVM7 { // Field Description
					enum {Lsb = 16};
					enum {FieldMask = 0x00010000};
					enum {Value_Disabled = 0x0};
					enum {ValueMask_Disabled = 0x00000000};
					enum {Value_Enabled = 0x1};
					enum {ValueMask_Enabled = 0x00010000};
					};
				};
			namespace SIMASK { // Register description
				typedef uint32_t	Reg;
				namespace IRM0 { // Field Description
					enum {Lsb = 31};
					enum {FieldMask = 0x80000000};
					enum {Value_IntDisabled = 0x0};
					enum {ValueMask_IntDisabled = 0x00000000};
					enum {Value_IntEnabled = 0x1};
					enum {ValueMask_IntEnabled = 0x80000000};
					};
				namespace LVM0 { // Field Description
					enum {Lsb = 30};
					enum {FieldMask = 0x40000000};
					enum {Value_IntDisabled = 0x0};
					enum {ValueMask_IntDisabled = 0x00000000};
					enum {Value_IntEnabled = 0x1};
					enum {ValueMask_IntEnabled = 0x40000000};
					};
				namespace IRM1 { // Field Description
					enum {Lsb = 29};
					enum {FieldMask = 0x20000000};
					enum {Value_IntDisabled = 0x0};
					enum {ValueMask_IntDisabled = 0x00000000};
					enum {Value_IntEnabled = 0x1};
					enum {ValueMask_IntEnabled = 0x20000000};
					};
				namespace LVM1 { // Field Description
					enum {Lsb = 28};
					enum {FieldMask = 0x10000000};
					enum {Value_IntDisabled = 0x0};
					enum {ValueMask_IntDisabled = 0x00000000};
					enum {Value_IntEnabled = 0x1};
					enum {ValueMask_IntEnabled = 0x10000000};
					};
				namespace IRM2 { // Field Description
					enum {Lsb = 27};
					enum {FieldMask = 0x08000000};
					enum {Value_IntDisabled = 0x0};
					enum {ValueMask_IntDisabled = 0x00000000};
					enum {Value_IntEnabled = 0x1};
					enum {ValueMask_IntEnabled = 0x08000000};
					};
				namespace LVM2 { // Field Description
					enum {Lsb = 26};
					enum {FieldMask = 0x04000000};
					enum {Value_IntDisabled = 0x0};
					enum {ValueMask_IntDisabled = 0x00000000};
					enum {Value_IntEnabled = 0x1};
					enum {ValueMask_IntEnabled = 0x04000000};
					};
				namespace IRM3 { // Field Description
					enum {Lsb = 25};
					enum {FieldMask = 0x02000000};
					enum {Value_IntDisabled = 0x0};
					enum {ValueMask_IntDisabled = 0x00000000};
					enum {Value_IntEnabled = 0x1};
					enum {ValueMask_IntEnabled = 0x02000000};
					};
				namespace LVM3 { // Field Description
					enum {Lsb = 24};
					enum {FieldMask = 0x01000000};
					enum {Value_IntDisabled = 0x0};
					enum {ValueMask_IntDisabled = 0x00000000};
					enum {Value_IntEnabled = 0x1};
					enum {ValueMask_IntEnabled = 0x01000000};
					};
				namespace IRM4 { // Field Description
					enum {Lsb = 23};
					enum {FieldMask = 0x00800000};
					enum {Value_IntDisabled = 0x0};
					enum {ValueMask_IntDisabled = 0x00000000};
					enum {Value_IntEnabled = 0x1};
					enum {ValueMask_IntEnabled = 0x00800000};
					};
				namespace LVM4 { // Field Description
					enum {Lsb = 22};
					enum {FieldMask = 0x00400000};
					enum {Value_IntDisabled = 0x0};
					enum {ValueMask_IntDisabled = 0x00000000};
					enum {Value_IntEnabled = 0x1};
					enum {ValueMask_IntEnabled = 0x00400000};
					};
				namespace IRM5 { // Field Description
					enum {Lsb = 21};
					enum {FieldMask = 0x00200000};
					enum {Value_IntDisabled = 0x0};
					enum {ValueMask_IntDisabled = 0x00000000};
					enum {Value_IntEnabled = 0x1};
					enum {ValueMask_IntEnabled = 0x00200000};
					};
				namespace LVM5 { // Field Description
					enum {Lsb = 20};
					enum {FieldMask = 0x00100000};
					enum {Value_IntDisabled = 0x0};
					enum {ValueMask_IntDisabled = 0x00000000};
					enum {Value_IntEnabled = 0x1};
					enum {ValueMask_IntEnabled = 0x00100000};
					};
				namespace IRM6 { // Field Description
					enum {Lsb = 19};
					enum {FieldMask = 0x00080000};
					enum {Value_IntDisabled = 0x0};
					enum {ValueMask_IntDisabled = 0x00000000};
					enum {Value_IntEnabled = 0x1};
					enum {ValueMask_IntEnabled = 0x00080000};
					};
				namespace LVM6 { // Field Description
					enum {Lsb = 18};
					enum {FieldMask = 0x00040000};
					enum {Value_IntDisabled = 0x0};
					enum {ValueMask_IntDisabled = 0x00000000};
					enum {Value_IntEnabled = 0x1};
					enum {ValueMask_IntEnabled = 0x00040000};
					};
				namespace IRM7 { // Field Description
					enum {Lsb = 17};
					enum {FieldMask = 0x00020000};
					enum {Value_IntDisabled = 0x0};
					enum {ValueMask_IntDisabled = 0x00000000};
					enum {Value_IntEnabled = 0x1};
					enum {ValueMask_IntEnabled = 0x00020000};
					};
				namespace LVM7 { // Field Description
					enum {Lsb = 16};
					enum {FieldMask = 0x00010000};
					enum {Value_IntDisabled = 0x0};
					enum {ValueMask_IntDisabled = 0x00000000};
					enum {Value_IntEnabled = 0x1};
					enum {ValueMask_IntEnabled = 0x00010000};
					};
				};
			namespace SIEL { // Register description
				typedef uint32_t	Reg;
				namespace ED0 { // Field Description
					enum {Lsb = 31};
					enum {FieldMask = 0x80000000};
					enum {Value_LevelLow = 0x0};
					enum {ValueMask_LevelLow = 0x00000000};
					enum {Value_FallingEdge = 0x1};
					enum {ValueMask_FallingEdge = 0x80000000};
					};
				namespace WM0 { // Field Description
					enum {Lsb = 30};
					enum {FieldMask = 0x40000000};
					enum {Value_NoWakeUp = 0x0};
					enum {ValueMask_NoWakeUp = 0x00000000};
					enum {Value_WakeUp = 0x1};
					enum {ValueMask_WakeUp = 0x40000000};
					};
				namespace ED1 { // Field Description
					enum {Lsb = 29};
					enum {FieldMask = 0x20000000};
					enum {Value_LevelLow = 0x0};
					enum {ValueMask_LevelLow = 0x00000000};
					enum {Value_FallingEdge = 0x1};
					enum {ValueMask_FallingEdge = 0x20000000};
					};
				namespace WM1 { // Field Description
					enum {Lsb = 28};
					enum {FieldMask = 0x10000000};
					enum {Value_NoWakeUp = 0x0};
					enum {ValueMask_NoWakeUp = 0x00000000};
					enum {Value_WakeUp = 0x1};
					enum {ValueMask_WakeUp = 0x10000000};
					};
				namespace ED2 { // Field Description
					enum {Lsb = 27};
					enum {FieldMask = 0x08000000};
					enum {Value_LevelLow = 0x0};
					enum {ValueMask_LevelLow = 0x00000000};
					enum {Value_FallingEdge = 0x1};
					enum {ValueMask_FallingEdge = 0x08000000};
					};
				namespace WM2 { // Field Description
					enum {Lsb = 26};
					enum {FieldMask = 0x04000000};
					enum {Value_NoWakeUp = 0x0};
					enum {ValueMask_NoWakeUp = 0x00000000};
					enum {Value_WakeUp = 0x1};
					enum {ValueMask_WakeUp = 0x04000000};
					};
				namespace ED3 { // Field Description
					enum {Lsb = 25};
					enum {FieldMask = 0x02000000};
					enum {Value_LevelLow = 0x0};
					enum {ValueMask_LevelLow = 0x00000000};
					enum {Value_FallingEdge = 0x1};
					enum {ValueMask_FallingEdge = 0x02000000};
					};
				namespace WM3 { // Field Description
					enum {Lsb = 24};
					enum {FieldMask = 0x01000000};
					enum {Value_NoWakeUp = 0x0};
					enum {ValueMask_NoWakeUp = 0x00000000};
					enum {Value_WakeUp = 0x1};
					enum {ValueMask_WakeUp = 0x01000000};
					};
				namespace ED4 { // Field Description
					enum {Lsb = 23};
					enum {FieldMask = 0x00800000};
					enum {Value_LevelLow = 0x0};
					enum {ValueMask_LevelLow = 0x00000000};
					enum {Value_FallingEdge = 0x1};
					enum {ValueMask_FallingEdge = 0x00800000};
					};
				namespace WM4 { // Field Description
					enum {Lsb = 22};
					enum {FieldMask = 0x00400000};
					enum {Value_NoWakeUp = 0x0};
					enum {ValueMask_NoWakeUp = 0x00000000};
					enum {Value_WakeUp = 0x1};
					enum {ValueMask_WakeUp = 0x00400000};
					};
				namespace ED5 { // Field Description
					enum {Lsb = 21};
					enum {FieldMask = 0x00200000};
					enum {Value_LevelLow = 0x0};
					enum {ValueMask_LevelLow = 0x00000000};
					enum {Value_FallingEdge = 0x1};
					enum {ValueMask_FallingEdge = 0x00200000};
					};
				namespace WM5 { // Field Description
					enum {Lsb = 20};
					enum {FieldMask = 0x00100000};
					enum {Value_NoWakeUp = 0x0};
					enum {ValueMask_NoWakeUp = 0x00000000};
					enum {Value_WakeUp = 0x1};
					enum {ValueMask_WakeUp = 0x00100000};
					};
				namespace ED6 { // Field Description
					enum {Lsb = 19};
					enum {FieldMask = 0x00080000};
					enum {Value_LevelLow = 0x0};
					enum {ValueMask_LevelLow = 0x00000000};
					enum {Value_FallingEdge = 0x1};
					enum {ValueMask_FallingEdge = 0x00080000};
					};
				namespace WM6 { // Field Description
					enum {Lsb = 18};
					enum {FieldMask = 0x00040000};
					enum {Value_NoWakeUp = 0x0};
					enum {ValueMask_NoWakeUp = 0x00000000};
					enum {Value_WakeUp = 0x1};
					enum {ValueMask_WakeUp = 0x00040000};
					};
				namespace ED7 { // Field Description
					enum {Lsb = 17};
					enum {FieldMask = 0x00020000};
					enum {Value_LevelLow = 0x0};
					enum {ValueMask_LevelLow = 0x00000000};
					enum {Value_FallingEdge = 0x1};
					enum {ValueMask_FallingEdge = 0x00020000};
					};
				namespace WM7 { // Field Description
					enum {Lsb = 16};
					enum {FieldMask = 0x00010000};
					enum {Value_NoWakeUp = 0x0};
					enum {ValueMask_NoWakeUp = 0x00000000};
					enum {Value_WakeUp = 0x1};
					enum {ValueMask_WakeUp = 0x00010000};
					};
				};
			namespace SIVEC { // Register description
				typedef uint8_t	Reg;
				enum {MaxValue = 255};
				};
			namespace TESR { // Register description
				typedef uint32_t	Reg;
				namespace IEXT { // Field Description
					enum {Lsb = 13};
					enum {FieldMask = 0x00002000};
					enum {Value_NoError = 0x0};
					enum {ValueMask_NoError = 0x00000000};
					enum {Value_InstructionTeaError = 0x1};
					enum {ValueMask_InstructionTeaError = 0x00002000};
					};
				namespace ITMT { // Field Description
					enum {Lsb = 12};
					enum {FieldMask = 0x00001000};
					enum {Value_NoError = 0x0};
					enum {ValueMask_NoError = 0x00000000};
					enum {Value_InstructionTimeoutError = 0x1};
					enum {ValueMask_InstructionTimeoutError = 0x00001000};
					};
				namespace IPB0 { // Field Description
					enum {Lsb = 11};
					enum {FieldMask = 0x00000800};
					enum {Value_NoError = 0x0};
					enum {ValueMask_NoError = 0x00000000};
					enum {Value_ParityError = 0x1};
					enum {ValueMask_ParityError = 0x00000800};
					};
				namespace IPB1 { // Field Description
					enum {Lsb = 10};
					enum {FieldMask = 0x00000400};
					enum {Value_NoError = 0x0};
					enum {ValueMask_NoError = 0x00000000};
					enum {Value_ParityError = 0x1};
					enum {ValueMask_ParityError = 0x00000400};
					};
				namespace IPB2 { // Field Description
					enum {Lsb = 9};
					enum {FieldMask = 0x00000200};
					enum {Value_NoError = 0x0};
					enum {ValueMask_NoError = 0x00000000};
					enum {Value_ParityError = 0x1};
					enum {ValueMask_ParityError = 0x00000200};
					};
				namespace IPB3 { // Field Description
					enum {Lsb = 8};
					enum {FieldMask = 0x00000100};
					enum {Value_NoError = 0x0};
					enum {ValueMask_NoError = 0x00000000};
					enum {Value_ParityError = 0x1};
					enum {ValueMask_ParityError = 0x00000100};
					};
				namespace DEXT { // Field Description
					enum {Lsb = 5};
					enum {FieldMask = 0x00000020};
					enum {Value_NoError = 0x0};
					enum {ValueMask_NoError = 0x00000000};
					enum {Value_DataTeaError = 0x1};
					enum {ValueMask_DataTeaError = 0x00000020};
					};
				namespace DTMT { // Field Description
					enum {Lsb = 4};
					enum {FieldMask = 0x00000010};
					enum {Value_NoError = 0x0};
					enum {ValueMask_NoError = 0x00000000};
					enum {Value_DataTimeoutError = 0x1};
					enum {ValueMask_DataTimeoutError = 0x00000010};
					};
				namespace DPB0 { // Field Description
					enum {Lsb = 3};
					enum {FieldMask = 0x00000008};
					enum {Value_NoError = 0x0};
					enum {ValueMask_NoError = 0x00000000};
					enum {Value_ParityError = 0x1};
					enum {ValueMask_ParityError = 0x00000008};
					};
				namespace DPB1 { // Field Description
					enum {Lsb = 2};
					enum {FieldMask = 0x00000004};
					enum {Value_NoError = 0x0};
					enum {ValueMask_NoError = 0x00000000};
					enum {Value_ParityError = 0x1};
					enum {ValueMask_ParityError = 0x00000004};
					};
				namespace DPB2 { // Field Description
					enum {Lsb = 1};
					enum {FieldMask = 0x00000002};
					enum {Value_NoError = 0x0};
					enum {ValueMask_NoError = 0x00000000};
					enum {Value_ParityError = 0x1};
					enum {ValueMask_ParityError = 0x00000002};
					};
				namespace DPB3 { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x00000001};
					enum {Value_NoError = 0x0};
					enum {ValueMask_NoError = 0x00000000};
					enum {Value_ParityError = 0x1};
					enum {ValueMask_ParityError = 0x00000001};
					};
				};
			namespace SDCR { // Register description
				typedef uint32_t	Reg;
				namespace FRZ { // Field Description
					enum {Lsb = 13};
					enum {FieldMask = 0x00006000};
					enum {Value_IgnoreFreeze = 0x0};
					enum {ValueMask_IgnoreFreeze = 0x00000000};
					enum {Value_RespondToFreeze = 0x1};
					enum {ValueMask_RespondToFreeze = 0x00002000};
					};
				namespace RAID { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x00000003};
					enum {Value_PriorityLevel6 = 0x0};
					enum {ValueMask_PriorityLevel6 = 0x00000000};
					enum {Value_PriorityLevel5 = 0x1};
					enum {ValueMask_PriorityLevel5 = 0x00000001};
					enum {Value_NormalOperation = 0x1};
					enum {ValueMask_NormalOperation = 0x00000001};
					enum {Value_PriorityLevel2 = 0x2};
					enum {ValueMask_PriorityLevel2 = 0x00000002};
					enum {Value_PriorityLevel1 = 0x3};
					enum {ValueMask_PriorityLevel1 = 0x00000003};
					};
				};
			};
		};
	};
#endif
