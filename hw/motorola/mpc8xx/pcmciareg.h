/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _hw_mot8xx_pcmciaregh_
#define _hw_mot8xx_pcmciaregh_
#include <stdint.h>
namespace Oscl { // Namespace description
	namespace Mot8xx { // Namespace description
		namespace Pcmcia { // Namespace description
			namespace PBR { // Register description
				typedef uint32_t	Reg;
				};
			namespace POR { // Register description
				typedef uint32_t	Reg;
				};
			namespace PGCR { // Register description
				typedef uint32_t	Reg;
				};
			namespace PSCR { // Register description
				typedef uint16_t	Reg;
				namespace VS1_C { // Field Description
					enum {Lsb = 15};
					enum {FieldMask = 0x00008000};
					enum {Value_Unchanged = 0x0};
					enum {ValueMask_Unchanged = 0x00000000};
					enum {Value_Changed = 0x1};
					enum {ValueMask_Changed = 0x00008000};
					};
				namespace VS2_C { // Field Description
					enum {Lsb = 14};
					enum {FieldMask = 0x00004000};
					enum {Value_Unchanged = 0x0};
					enum {ValueMask_Unchanged = 0x00000000};
					enum {Value_Changed = 0x1};
					enum {ValueMask_Changed = 0x00004000};
					};
				namespace WP_C { // Field Description
					enum {Lsb = 13};
					enum {FieldMask = 0x00002000};
					enum {Value_Unchanged = 0x0};
					enum {ValueMask_Unchanged = 0x00000000};
					enum {Value_Changed = 0x1};
					enum {ValueMask_Changed = 0x00002000};
					};
				namespace CD2_C { // Field Description
					enum {Lsb = 12};
					enum {FieldMask = 0x00001000};
					enum {Value_Unchanged = 0x0};
					enum {ValueMask_Unchanged = 0x00000000};
					enum {Value_Changed = 0x1};
					enum {ValueMask_Changed = 0x00001000};
					};
				namespace BDV2_C { // Field Description
					enum {Lsb = 11};
					enum {FieldMask = 0x00000800};
					enum {Value_Unchanged = 0x0};
					enum {ValueMask_Unchanged = 0x00000000};
					enum {Value_Changed = 0x1};
					enum {ValueMask_Changed = 0x00000800};
					};
				namespace BDV1_C { // Field Description
					enum {Lsb = 10};
					enum {FieldMask = 0x00000400};
					enum {Value_Unchanged = 0x0};
					enum {ValueMask_Unchanged = 0x00000000};
					enum {Value_Changed = 0x1};
					enum {ValueMask_Changed = 0x00000400};
					};
				namespace RDY_C { // Field Description
					enum {Lsb = 9};
					enum {FieldMask = 0x00000200};
					enum {Value_Unchanged = 0x0};
					enum {ValueMask_Unchanged = 0x00000000};
					enum {Value_Changed = 0x1};
					enum {ValueMask_Changed = 0x00000200};
					};
				namespace RDY_L { // Field Description
					enum {Lsb = 7};
					enum {FieldMask = 0x00000080};
					enum {Value_Unchanged = 0x0};
					enum {ValueMask_Unchanged = 0x00000000};
					enum {Value_Changed = 0x1};
					enum {ValueMask_Changed = 0x00000080};
					};
				namespace RDY_H { // Field Description
					enum {Lsb = 6};
					enum {FieldMask = 0x00000040};
					enum {Value_Unchanged = 0x0};
					enum {ValueMask_Unchanged = 0x00000000};
					enum {Value_Changed = 0x1};
					enum {ValueMask_Changed = 0x00000040};
					};
				namespace RDY_R { // Field Description
					enum {Lsb = 5};
					enum {FieldMask = 0x00000020};
					enum {Value_Unchanged = 0x0};
					enum {ValueMask_Unchanged = 0x00000000};
					enum {Value_Changed = 0x1};
					enum {ValueMask_Changed = 0x00000020};
					};
				namespace RDY_F { // Field Description
					enum {Lsb = 4};
					enum {FieldMask = 0x00000010};
					enum {Value_Unchanged = 0x0};
					enum {ValueMask_Unchanged = 0x00000000};
					enum {Value_Changed = 0x1};
					enum {ValueMask_Changed = 0x00000010};
					};
				};
			namespace PIPR { // Register description
				typedef uint16_t	Reg;
				namespace VS1 { // Field Description
					enum {Lsb = 15};
					enum {FieldMask = 0x00008000};
					enum {Value_Negated = 0x0};
					enum {ValueMask_Negated = 0x00000000};
					enum {Value_Asserted = 0x1};
					enum {ValueMask_Asserted = 0x00008000};
					};
				namespace VS2 { // Field Description
					enum {Lsb = 14};
					enum {FieldMask = 0x00004000};
					enum {Value_Negated = 0x0};
					enum {ValueMask_Negated = 0x00000000};
					enum {Value_Asserted = 0x1};
					enum {ValueMask_Asserted = 0x00004000};
					};
				namespace WP { // Field Description
					enum {Lsb = 13};
					enum {FieldMask = 0x00002000};
					enum {Value_ReadWrite = 0x0};
					enum {ValueMask_ReadWrite = 0x00000000};
					enum {Value_ReadOnly = 0x1};
					enum {ValueMask_ReadOnly = 0x00002000};
					};
				namespace CD2 { // Field Description
					enum {Lsb = 12};
					enum {FieldMask = 0x00001000};
					enum {Value_Absent = 0x0};
					enum {ValueMask_Absent = 0x00000000};
					enum {Value_Present = 0x1};
					enum {ValueMask_Present = 0x00001000};
					};
				namespace BDV2 { // Field Description
					enum {Lsb = 11};
					enum {FieldMask = 0x00000800};
					enum {Value_Negated = 0x0};
					enum {ValueMask_Negated = 0x00000000};
					enum {Value_Asserted = 0x1};
					enum {ValueMask_Asserted = 0x00000800};
					};
				namespace BDV1 { // Field Description
					enum {Lsb = 10};
					enum {FieldMask = 0x00000400};
					enum {Value_Negated = 0x0};
					enum {ValueMask_Negated = 0x00000000};
					enum {Value_Asserted = 0x1};
					enum {ValueMask_Asserted = 0x00000400};
					};
				namespace RDY { // Field Description
					enum {Lsb = 9};
					enum {FieldMask = 0x00000200};
					enum {Value_Negated = 0x0};
					enum {ValueMask_Negated = 0x00000000};
					enum {Value_Asserted = 0x1};
					enum {ValueMask_Asserted = 0x00000200};
					};
				};
			namespace PER { // Register description
				typedef uint16_t	Reg;
				namespace EVS1_C { // Field Description
					enum {Lsb = 15};
					enum {FieldMask = 0x00008000};
					enum {Value_IntDisable = 0x0};
					enum {ValueMask_IntDisable = 0x00000000};
					enum {Value_IntEnable = 0x1};
					enum {ValueMask_IntEnable = 0x00008000};
					};
				namespace EVS2_C { // Field Description
					enum {Lsb = 14};
					enum {FieldMask = 0x00004000};
					enum {Value_IntDisable = 0x0};
					enum {ValueMask_IntDisable = 0x00000000};
					enum {Value_IntEnable = 0x1};
					enum {ValueMask_IntEnable = 0x00004000};
					};
				namespace EWP_C { // Field Description
					enum {Lsb = 13};
					enum {FieldMask = 0x00002000};
					enum {Value_IntDisable = 0x0};
					enum {ValueMask_IntDisable = 0x00000000};
					enum {Value_IntEnable = 0x1};
					enum {ValueMask_IntEnable = 0x00002000};
					};
				namespace ECD2_C { // Field Description
					enum {Lsb = 12};
					enum {FieldMask = 0x00001000};
					enum {Value_IntDisable = 0x0};
					enum {ValueMask_IntDisable = 0x00000000};
					enum {Value_IntEnable = 0x1};
					enum {ValueMask_IntEnable = 0x00001000};
					};
				namespace EBDV2_C { // Field Description
					enum {Lsb = 11};
					enum {FieldMask = 0x00000800};
					enum {Value_IntDisable = 0x0};
					enum {ValueMask_IntDisable = 0x00000000};
					enum {Value_IntEnable = 0x1};
					enum {ValueMask_IntEnable = 0x00000800};
					};
				namespace EBDV1_C { // Field Description
					enum {Lsb = 10};
					enum {FieldMask = 0x00000400};
					enum {Value_IntDisable = 0x0};
					enum {ValueMask_IntDisable = 0x00000000};
					enum {Value_IntEnable = 0x1};
					enum {ValueMask_IntEnable = 0x00000400};
					};
				namespace ERDY_C { // Field Description
					enum {Lsb = 9};
					enum {FieldMask = 0x00000200};
					enum {Value_IntDisable = 0x0};
					enum {ValueMask_IntDisable = 0x00000000};
					enum {Value_IntEnable = 0x1};
					enum {ValueMask_IntEnable = 0x00000200};
					};
				namespace ERDY_L { // Field Description
					enum {Lsb = 7};
					enum {FieldMask = 0x00000080};
					enum {Value_IntDisable = 0x0};
					enum {ValueMask_IntDisable = 0x00000000};
					enum {Value_IntEnable = 0x1};
					enum {ValueMask_IntEnable = 0x00000080};
					};
				namespace ERDY_H { // Field Description
					enum {Lsb = 6};
					enum {FieldMask = 0x00000040};
					enum {Value_IntDisable = 0x0};
					enum {ValueMask_IntDisable = 0x00000000};
					enum {Value_IntEnable = 0x1};
					enum {ValueMask_IntEnable = 0x00000040};
					};
				namespace ERDY_R { // Field Description
					enum {Lsb = 5};
					enum {FieldMask = 0x00000020};
					enum {Value_IntDisable = 0x0};
					enum {ValueMask_IntDisable = 0x00000000};
					enum {Value_IntEnable = 0x1};
					enum {ValueMask_IntEnable = 0x00000020};
					};
				namespace ERDY_F { // Field Description
					enum {Lsb = 4};
					enum {FieldMask = 0x00000010};
					enum {Value_IntDisable = 0x0};
					enum {ValueMask_IntDisable = 0x00000000};
					enum {Value_IntEnable = 0x1};
					enum {ValueMask_IntEnable = 0x00000010};
					};
				};
			};
		};
	};
#endif
