/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _hw_mot8xx_siregh_
#define _hw_mot8xx_siregh_
#include <stdint.h>
namespace Oscl { // Namespace description
	namespace Mot8xx { // Namespace description
		namespace Si { // Namespace description
			namespace SIMODE { // Register description
				typedef uint32_t	Reg;
				namespace SMC2 { // Field Description
					enum {Lsb = 31};
					enum {FieldMask = 0x80000000};
					enum {Value_NMSI = 0x0};
					enum {ValueMask_NMSI = 0x00000000};
					enum {Value_TDM = 0x1};
					enum {ValueMask_TDM = 0x80000000};
					};
				namespace SMC2CS { // Field Description
					enum {Lsb = 28};
					enum {FieldMask = 0x70000000};
					enum {Value_BRG1 = 0x0};
					enum {ValueMask_BRG1 = 0x00000000};
					enum {Value_BRG2 = 0x1};
					enum {ValueMask_BRG2 = 0x10000000};
					enum {Value_BRG3 = 0x2};
					enum {ValueMask_BRG3 = 0x20000000};
					enum {Value_BRG4 = 0x3};
					enum {ValueMask_BRG4 = 0x30000000};
					enum {Value_CLK5 = 0x4};
					enum {ValueMask_CLK5 = 0x40000000};
					enum {Value_CLK6 = 0x5};
					enum {ValueMask_CLK6 = 0x50000000};
					enum {Value_CLK7 = 0x6};
					enum {ValueMask_CLK7 = 0x60000000};
					enum {Value_CLK8 = 0x7};
					enum {ValueMask_CLK8 = 0x70000000};
					};
				namespace SDMb { // Field Description
					enum {Lsb = 26};
					enum {FieldMask = 0x0C000000};
					enum {Value_Normal = 0x0};
					enum {ValueMask_Normal = 0x00000000};
					enum {Value_AutoEcho = 0x1};
					enum {ValueMask_AutoEcho = 0x04000000};
					enum {Value_InternalLoopback = 0x2};
					enum {ValueMask_InternalLoopback = 0x08000000};
					enum {Value_FullInternalLoopback = 0x3};
					enum {ValueMask_FullInternalLoopback = 0x0C000000};
					};
				namespace RFSDb { // Field Description
					enum {Lsb = 24};
					enum {FieldMask = 0x03000000};
					enum {Value_NoBitDelay = 0x0};
					enum {ValueMask_NoBitDelay = 0x00000000};
					enum {Value_OneBitDelay = 0x1};
					enum {ValueMask_OneBitDelay = 0x01000000};
					enum {Value_TwoBitDelay = 0x2};
					enum {ValueMask_TwoBitDelay = 0x02000000};
					enum {Value_ThreeBitDelay = 0x3};
					enum {ValueMask_ThreeBitDelay = 0x03000000};
					};
				namespace DSCb { // Field Description
					enum {Lsb = 23};
					enum {FieldMask = 0x00800000};
					enum {Value_Clock1X = 0x0};
					enum {ValueMask_Clock1X = 0x00000000};
					enum {Value_Clock2X = 0x1};
					enum {ValueMask_Clock2X = 0x00800000};
					};
				namespace CRTb { // Field Description
					enum {Lsb = 22};
					enum {FieldMask = 0x00400000};
					enum {Value_Separate = 0x0};
					enum {ValueMask_Separate = 0x00000000};
					enum {Value_Common = 0x1};
					enum {ValueMask_Common = 0x00400000};
					};
				namespace STZb { // Field Description
					enum {Lsb = 21};
					enum {FieldMask = 0x00200000};
					enum {Value_Normal = 0x0};
					enum {ValueMask_Normal = 0x00000000};
					enum {Value_WaitForSerialClocks = 0x1};
					enum {ValueMask_WaitForSerialClocks = 0x00200000};
					};
				namespace CEb { // Field Description
					enum {Lsb = 20};
					enum {FieldMask = 0x00100000};
					enum {Value_TxRiseRxRise = 0x0};
					enum {ValueMask_TxRiseRxRise = 0x00000000};
					enum {Value_Dsc0TxFallRxRise = 0x1};
					enum {ValueMask_Dsc0TxFallRxRise = 0x00100000};
					enum {Value_Dsc1TxFallRxFall = 0x1};
					enum {ValueMask_Dsc1TxFallRxFall = 0x00100000};
					};
				namespace FEb { // Field Description
					enum {Lsb = 19};
					enum {FieldMask = 0x00080000};
					enum {Value_FallingEdge = 0x0};
					enum {ValueMask_FallingEdge = 0x00000000};
					enum {Value_RisingEdge = 0x1};
					enum {ValueMask_RisingEdge = 0x00080000};
					};
				namespace GMb { // Field Description
					enum {Lsb = 18};
					enum {FieldMask = 0x00040000};
					enum {Value_GciScitMode = 0x0};
					enum {ValueMask_GciScitMode = 0x00000000};
					enum {Value_IdlMode = 0x1};
					enum {ValueMask_IdlMode = 0x00040000};
					};
				namespace TFSDb { // Field Description
					enum {Lsb = 16};
					enum {FieldMask = 0x00030000};
					enum {Value_NoBitDelay = 0x0};
					enum {ValueMask_NoBitDelay = 0x00000000};
					enum {Value_OneBitDelay = 0x1};
					enum {ValueMask_OneBitDelay = 0x00010000};
					enum {Value_TwoBitDelay = 0x2};
					enum {ValueMask_TwoBitDelay = 0x00020000};
					enum {Value_ThreeBitDelay = 0x3};
					enum {ValueMask_ThreeBitDelay = 0x00030000};
					};
				namespace SMC1 { // Field Description
					enum {Lsb = 15};
					enum {FieldMask = 0x00008000};
					enum {Value_NMSI = 0x0};
					enum {ValueMask_NMSI = 0x00000000};
					enum {Value_TDM = 0x1};
					enum {ValueMask_TDM = 0x00008000};
					};
				namespace SMC1CS { // Field Description
					enum {Lsb = 12};
					enum {FieldMask = 0x00007000};
					enum {Value_BRG1 = 0x0};
					enum {ValueMask_BRG1 = 0x00000000};
					enum {Value_BRG2 = 0x1};
					enum {ValueMask_BRG2 = 0x00001000};
					enum {Value_BRG3 = 0x2};
					enum {ValueMask_BRG3 = 0x00002000};
					enum {Value_BRG4 = 0x3};
					enum {ValueMask_BRG4 = 0x00003000};
					enum {Value_CLK1 = 0x4};
					enum {ValueMask_CLK1 = 0x00004000};
					enum {Value_CLK2 = 0x5};
					enum {ValueMask_CLK2 = 0x00005000};
					enum {Value_CLK3 = 0x6};
					enum {ValueMask_CLK3 = 0x00006000};
					enum {Value_CLK4 = 0x7};
					enum {ValueMask_CLK4 = 0x00007000};
					};
				namespace SDMa { // Field Description
					enum {Lsb = 10};
					enum {FieldMask = 0x00000C00};
					enum {Value_Normal = 0x0};
					enum {ValueMask_Normal = 0x00000000};
					enum {Value_AutoEcho = 0x1};
					enum {ValueMask_AutoEcho = 0x00000400};
					enum {Value_InternalLoopback = 0x2};
					enum {ValueMask_InternalLoopback = 0x00000800};
					enum {Value_FullInternalLoopback = 0x3};
					enum {ValueMask_FullInternalLoopback = 0x00000C00};
					};
				namespace RFSDa { // Field Description
					enum {Lsb = 8};
					enum {FieldMask = 0x00000300};
					enum {Value_NoBitDelay = 0x0};
					enum {ValueMask_NoBitDelay = 0x00000000};
					enum {Value_OneBitDelay = 0x1};
					enum {ValueMask_OneBitDelay = 0x00000100};
					enum {Value_TwoBitDelay = 0x2};
					enum {ValueMask_TwoBitDelay = 0x00000200};
					enum {Value_ThreeBitDelay = 0x3};
					enum {ValueMask_ThreeBitDelay = 0x00000300};
					};
				namespace DSCa { // Field Description
					enum {Lsb = 7};
					enum {FieldMask = 0x00000080};
					enum {Value_Clock1X = 0x0};
					enum {ValueMask_Clock1X = 0x00000000};
					enum {Value_Clock2X = 0x1};
					enum {ValueMask_Clock2X = 0x00000080};
					};
				namespace CRTa { // Field Description
					enum {Lsb = 6};
					enum {FieldMask = 0x00000040};
					enum {Value_Separate = 0x0};
					enum {ValueMask_Separate = 0x00000000};
					enum {Value_Common = 0x1};
					enum {ValueMask_Common = 0x00000040};
					};
				namespace STZa { // Field Description
					enum {Lsb = 5};
					enum {FieldMask = 0x00000020};
					enum {Value_Normal = 0x0};
					enum {ValueMask_Normal = 0x00000000};
					enum {Value_WaitForSerialClocks = 0x1};
					enum {ValueMask_WaitForSerialClocks = 0x00000020};
					};
				namespace CEa { // Field Description
					enum {Lsb = 4};
					enum {FieldMask = 0x00000010};
					enum {Value_TxRiseRxRise = 0x0};
					enum {ValueMask_TxRiseRxRise = 0x00000000};
					enum {Value_Dsc0TxFallRxRise = 0x1};
					enum {ValueMask_Dsc0TxFallRxRise = 0x00000010};
					enum {Value_Dsc1TxFallRxFall = 0x1};
					enum {ValueMask_Dsc1TxFallRxFall = 0x00000010};
					};
				namespace FEa { // Field Description
					enum {Lsb = 3};
					enum {FieldMask = 0x00000008};
					enum {Value_FallingEdge = 0x0};
					enum {ValueMask_FallingEdge = 0x00000000};
					enum {Value_RisingEdge = 0x1};
					enum {ValueMask_RisingEdge = 0x00000008};
					};
				namespace GMa { // Field Description
					enum {Lsb = 2};
					enum {FieldMask = 0x00000004};
					enum {Value_GciScitMode = 0x0};
					enum {ValueMask_GciScitMode = 0x00000000};
					enum {Value_IdlMode = 0x1};
					enum {ValueMask_IdlMode = 0x00000004};
					};
				namespace TFSDa { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x00000003};
					enum {Value_NoBitDelay = 0x0};
					enum {ValueMask_NoBitDelay = 0x00000000};
					enum {Value_OneBitDelay = 0x1};
					enum {ValueMask_OneBitDelay = 0x00000001};
					enum {Value_TwoBitDelay = 0x2};
					enum {ValueMask_TwoBitDelay = 0x00000002};
					enum {Value_ThreeBitDelay = 0x3};
					enum {ValueMask_ThreeBitDelay = 0x00000003};
					};
				};
			namespace SIGMR { // Register description
				typedef uint8_t	Reg;
				namespace ENb { // Field Description
					enum {Lsb = 3};
					enum {FieldMask = 0x00000008};
					enum {Value_Disable = 0x0};
					enum {ValueMask_Disable = 0x00000000};
					enum {Value_Enable = 0x1};
					enum {ValueMask_Enable = 0x00000008};
					};
				namespace ENa { // Field Description
					enum {Lsb = 2};
					enum {FieldMask = 0x00000004};
					enum {Value_Disable = 0x0};
					enum {ValueMask_Disable = 0x00000000};
					enum {Value_Enable = 0x1};
					enum {ValueMask_Enable = 0x00000004};
					};
				namespace RDM { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x00000003};
					enum {Value_Static64 = 0x0};
					enum {ValueMask_Static64 = 0x00000000};
					enum {Value_Dynamic32 = 0x1};
					enum {ValueMask_Dynamic32 = 0x00000001};
					enum {Value_Static32 = 0x2};
					enum {ValueMask_Static32 = 0x00000002};
					enum {Value_Dynamic16 = 0x3};
					enum {ValueMask_Dynamic16 = 0x00000003};
					};
				};
			namespace SISTR { // Register description
				typedef uint8_t	Reg;
				namespace CRORa { // Field Description
					enum {Lsb = 7};
					enum {FieldMask = 0x00000080};
					enum {Value_LowerAddresses = 0x0};
					enum {ValueMask_LowerAddresses = 0x00000000};
					enum {Value_HigherAddresses = 0x1};
					enum {ValueMask_HigherAddresses = 0x00000080};
					};
				namespace CROTa { // Field Description
					enum {Lsb = 6};
					enum {FieldMask = 0x00000040};
					enum {Value_LowerAddresses = 0x0};
					enum {ValueMask_LowerAddresses = 0x00000000};
					enum {Value_HigherAddresses = 0x1};
					enum {ValueMask_HigherAddresses = 0x00000040};
					};
				namespace CRORb { // Field Description
					enum {Lsb = 5};
					enum {FieldMask = 0x00000020};
					enum {Value_LowerAddresses = 0x0};
					enum {ValueMask_LowerAddresses = 0x00000000};
					enum {Value_HigherAddresses = 0x1};
					enum {ValueMask_HigherAddresses = 0x00000020};
					};
				namespace CROTb { // Field Description
					enum {Lsb = 4};
					enum {FieldMask = 0x00000010};
					enum {Value_LowerAddresses = 0x0};
					enum {ValueMask_LowerAddresses = 0x00000000};
					enum {Value_HigherAddresses = 0x1};
					enum {ValueMask_HigherAddresses = 0x00000010};
					};
				};
			namespace SICMR { // Register description
				typedef uint8_t	Reg;
				namespace CSRRa { // Field Description
					enum {Lsb = 7};
					enum {FieldMask = 0x00000080};
					enum {Value_Invalid = 0x0};
					enum {ValueMask_Invalid = 0x00000000};
					enum {Value_Valid = 0x1};
					enum {ValueMask_Valid = 0x00000080};
					};
				namespace CSRTa { // Field Description
					enum {Lsb = 6};
					enum {FieldMask = 0x00000040};
					enum {Value_Invalid = 0x0};
					enum {ValueMask_Invalid = 0x00000000};
					enum {Value_Valid = 0x1};
					enum {ValueMask_Valid = 0x00000040};
					};
				namespace CSRRb { // Field Description
					enum {Lsb = 5};
					enum {FieldMask = 0x00000020};
					enum {Value_Invalid = 0x0};
					enum {ValueMask_Invalid = 0x00000000};
					enum {Value_Valid = 0x1};
					enum {ValueMask_Valid = 0x00000020};
					};
				namespace CSRTb { // Field Description
					enum {Lsb = 4};
					enum {FieldMask = 0x00000010};
					enum {Value_Invalid = 0x0};
					enum {ValueMask_Invalid = 0x00000000};
					enum {Value_Valid = 0x1};
					enum {ValueMask_Valid = 0x00000010};
					};
				};
			namespace SICR { // Register description
				typedef uint32_t	Reg;
				namespace SCC4 { // Field Description
					enum {Lsb = 24};
					enum {FieldMask = 0xFF000000};
					namespace GR { // Field Description
						enum {Lsb = 31};
						enum {FieldMask = 0x80000000};
						enum {Value_Internal = 0x0};
						enum {ValueMask_Internal = 0x00000000};
						enum {Value_SIMODE = 0x1};
						enum {ValueMask_SIMODE = 0x80000000};
						};
					namespace SC { // Field Description
						enum {Lsb = 30};
						enum {FieldMask = 0x40000000};
						enum {Value_NMSI = 0x0};
						enum {ValueMask_NMSI = 0x00000000};
						enum {Value_TSA = 0x1};
						enum {ValueMask_TSA = 0x40000000};
						};
					namespace RCS { // Field Description
						enum {Lsb = 27};
						enum {FieldMask = 0x38000000};
						enum {Value_BRG1 = 0x0};
						enum {ValueMask_BRG1 = 0x00000000};
						enum {Value_BRG2 = 0x1};
						enum {ValueMask_BRG2 = 0x08000000};
						enum {Value_BRG3 = 0x2};
						enum {ValueMask_BRG3 = 0x10000000};
						enum {Value_BRG4 = 0x3};
						enum {ValueMask_BRG4 = 0x18000000};
						enum {Value_CLK5 = 0x4};
						enum {ValueMask_CLK5 = 0x20000000};
						enum {Value_CLK6 = 0x5};
						enum {ValueMask_CLK6 = 0x28000000};
						enum {Value_CLK7 = 0x6};
						enum {ValueMask_CLK7 = 0x30000000};
						enum {Value_CLK8 = 0x7};
						enum {ValueMask_CLK8 = 0x38000000};
						};
					namespace TCS { // Field Description
						enum {Lsb = 24};
						enum {FieldMask = 0x07000000};
						enum {Value_BRG1 = 0x0};
						enum {ValueMask_BRG1 = 0x00000000};
						enum {Value_BRG2 = 0x1};
						enum {ValueMask_BRG2 = 0x01000000};
						enum {Value_BRG3 = 0x2};
						enum {ValueMask_BRG3 = 0x02000000};
						enum {Value_BRG4 = 0x3};
						enum {ValueMask_BRG4 = 0x03000000};
						enum {Value_CLK5 = 0x4};
						enum {ValueMask_CLK5 = 0x04000000};
						enum {Value_CLK6 = 0x5};
						enum {ValueMask_CLK6 = 0x05000000};
						enum {Value_CLK7 = 0x6};
						enum {ValueMask_CLK7 = 0x06000000};
						enum {Value_CLK8 = 0x7};
						enum {ValueMask_CLK8 = 0x07000000};
						};
					};
				namespace SCC3 { // Field Description
					enum {Lsb = 16};
					enum {FieldMask = 0x00FF0000};
					namespace GR { // Field Description
						enum {Lsb = 23};
						enum {FieldMask = 0x00800000};
						enum {Value_Internal = 0x0};
						enum {ValueMask_Internal = 0x00000000};
						enum {Value_SIMODE = 0x1};
						enum {ValueMask_SIMODE = 0x00800000};
						};
					namespace SC { // Field Description
						enum {Lsb = 22};
						enum {FieldMask = 0x00400000};
						enum {Value_NMSI = 0x0};
						enum {ValueMask_NMSI = 0x00000000};
						enum {Value_TSA = 0x1};
						enum {ValueMask_TSA = 0x00400000};
						};
					namespace RCS { // Field Description
						enum {Lsb = 19};
						enum {FieldMask = 0x00380000};
						enum {Value_BRG1 = 0x0};
						enum {ValueMask_BRG1 = 0x00000000};
						enum {Value_BRG2 = 0x1};
						enum {ValueMask_BRG2 = 0x00080000};
						enum {Value_BRG3 = 0x2};
						enum {ValueMask_BRG3 = 0x00100000};
						enum {Value_BRG4 = 0x3};
						enum {ValueMask_BRG4 = 0x00180000};
						enum {Value_CLK5 = 0x4};
						enum {ValueMask_CLK5 = 0x00200000};
						enum {Value_CLK6 = 0x5};
						enum {ValueMask_CLK6 = 0x00280000};
						enum {Value_CLK7 = 0x6};
						enum {ValueMask_CLK7 = 0x00300000};
						enum {Value_CLK8 = 0x7};
						enum {ValueMask_CLK8 = 0x00380000};
						};
					namespace TCS { // Field Description
						enum {Lsb = 16};
						enum {FieldMask = 0x00070000};
						enum {Value_BRG1 = 0x0};
						enum {ValueMask_BRG1 = 0x00000000};
						enum {Value_BRG2 = 0x1};
						enum {ValueMask_BRG2 = 0x00010000};
						enum {Value_BRG3 = 0x2};
						enum {ValueMask_BRG3 = 0x00020000};
						enum {Value_BRG4 = 0x3};
						enum {ValueMask_BRG4 = 0x00030000};
						enum {Value_CLK5 = 0x4};
						enum {ValueMask_CLK5 = 0x00040000};
						enum {Value_CLK6 = 0x5};
						enum {ValueMask_CLK6 = 0x00050000};
						enum {Value_CLK7 = 0x6};
						enum {ValueMask_CLK7 = 0x00060000};
						enum {Value_CLK8 = 0x7};
						enum {ValueMask_CLK8 = 0x00070000};
						};
					};
				namespace SCC2 { // Field Description
					enum {Lsb = 8};
					enum {FieldMask = 0x0000FF00};
					namespace GR { // Field Description
						enum {Lsb = 15};
						enum {FieldMask = 0x00008000};
						enum {Value_Internal = 0x0};
						enum {ValueMask_Internal = 0x00000000};
						enum {Value_SIMODE = 0x1};
						enum {ValueMask_SIMODE = 0x00008000};
						};
					namespace SC { // Field Description
						enum {Lsb = 14};
						enum {FieldMask = 0x00004000};
						enum {Value_NMSI = 0x0};
						enum {ValueMask_NMSI = 0x00000000};
						enum {Value_TSA = 0x1};
						enum {ValueMask_TSA = 0x00004000};
						};
					namespace RCS { // Field Description
						enum {Lsb = 11};
						enum {FieldMask = 0x00003800};
						enum {Value_BRG1 = 0x0};
						enum {ValueMask_BRG1 = 0x00000000};
						enum {Value_BRG2 = 0x1};
						enum {ValueMask_BRG2 = 0x00000800};
						enum {Value_BRG3 = 0x2};
						enum {ValueMask_BRG3 = 0x00001000};
						enum {Value_BRG4 = 0x3};
						enum {ValueMask_BRG4 = 0x00001800};
						enum {Value_CLK1 = 0x4};
						enum {ValueMask_CLK1 = 0x00002000};
						enum {Value_CLK2 = 0x5};
						enum {ValueMask_CLK2 = 0x00002800};
						enum {Value_CLK3 = 0x6};
						enum {ValueMask_CLK3 = 0x00003000};
						enum {Value_CLK4 = 0x7};
						enum {ValueMask_CLK4 = 0x00003800};
						};
					namespace TCS { // Field Description
						enum {Lsb = 8};
						enum {FieldMask = 0x00000700};
						enum {Value_BRG1 = 0x0};
						enum {ValueMask_BRG1 = 0x00000000};
						enum {Value_BRG2 = 0x1};
						enum {ValueMask_BRG2 = 0x00000100};
						enum {Value_BRG3 = 0x2};
						enum {ValueMask_BRG3 = 0x00000200};
						enum {Value_BRG4 = 0x3};
						enum {ValueMask_BRG4 = 0x00000300};
						enum {Value_CLK1 = 0x4};
						enum {ValueMask_CLK1 = 0x00000400};
						enum {Value_CLK2 = 0x5};
						enum {ValueMask_CLK2 = 0x00000500};
						enum {Value_CLK3 = 0x6};
						enum {ValueMask_CLK3 = 0x00000600};
						enum {Value_CLK4 = 0x7};
						enum {ValueMask_CLK4 = 0x00000700};
						};
					};
				namespace SCC1 { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x000000FF};
					namespace GR { // Field Description
						enum {Lsb = 7};
						enum {FieldMask = 0x00000080};
						enum {Value_Internal = 0x0};
						enum {ValueMask_Internal = 0x00000000};
						enum {Value_SIMODE = 0x1};
						enum {ValueMask_SIMODE = 0x00000080};
						};
					namespace SC { // Field Description
						enum {Lsb = 6};
						enum {FieldMask = 0x00000040};
						enum {Value_NMSI = 0x0};
						enum {ValueMask_NMSI = 0x00000000};
						enum {Value_TSA = 0x1};
						enum {ValueMask_TSA = 0x00000040};
						};
					namespace RCS { // Field Description
						enum {Lsb = 3};
						enum {FieldMask = 0x00000038};
						enum {Value_BRG1 = 0x0};
						enum {ValueMask_BRG1 = 0x00000000};
						enum {Value_BRG2 = 0x1};
						enum {ValueMask_BRG2 = 0x00000008};
						enum {Value_BRG3 = 0x2};
						enum {ValueMask_BRG3 = 0x00000010};
						enum {Value_BRG4 = 0x3};
						enum {ValueMask_BRG4 = 0x00000018};
						enum {Value_CLK1 = 0x4};
						enum {ValueMask_CLK1 = 0x00000020};
						enum {Value_CLK2 = 0x5};
						enum {ValueMask_CLK2 = 0x00000028};
						enum {Value_CLK3 = 0x6};
						enum {ValueMask_CLK3 = 0x00000030};
						enum {Value_CLK4 = 0x7};
						enum {ValueMask_CLK4 = 0x00000038};
						};
					namespace TCS { // Field Description
						enum {Lsb = 0};
						enum {FieldMask = 0x00000007};
						enum {Value_BRG1 = 0x0};
						enum {ValueMask_BRG1 = 0x00000000};
						enum {Value_BRG2 = 0x1};
						enum {ValueMask_BRG2 = 0x00000001};
						enum {Value_BRG3 = 0x2};
						enum {ValueMask_BRG3 = 0x00000002};
						enum {Value_BRG4 = 0x3};
						enum {ValueMask_BRG4 = 0x00000003};
						enum {Value_CLK1 = 0x4};
						enum {ValueMask_CLK1 = 0x00000004};
						enum {Value_CLK2 = 0x5};
						enum {ValueMask_CLK2 = 0x00000005};
						enum {Value_CLK3 = 0x6};
						enum {ValueMask_CLK3 = 0x00000006};
						enum {Value_CLK4 = 0x7};
						enum {ValueMask_CLK4 = 0x00000007};
						};
					};
				};
			namespace SIRP { // Register description
				typedef uint32_t	Reg;
				namespace VTb { // Field Description
					enum {Lsb = 29};
					enum {FieldMask = 0x20000000};
					enum {Value_Invalid = 0x0};
					enum {ValueMask_Invalid = 0x00000000};
					enum {Value_Valid = 0x1};
					enum {ValueMask_Valid = 0x20000000};
					};
				namespace TbPTR { // Field Description
					enum {Lsb = 24};
					enum {FieldMask = 0x1F000000};
					enum {Value_Min = 0x0};
					enum {ValueMask_Min = 0x00000000};
					enum {Value_Max = 0x1F};
					enum {ValueMask_Max = 0x1F000000};
					};
				namespace VTa { // Field Description
					enum {Lsb = 21};
					enum {FieldMask = 0x00200000};
					enum {Value_Invalid = 0x0};
					enum {ValueMask_Invalid = 0x00000000};
					enum {Value_Valid = 0x1};
					enum {ValueMask_Valid = 0x00200000};
					};
				namespace TaPTR { // Field Description
					enum {Lsb = 16};
					enum {FieldMask = 0x001F0000};
					enum {Value_Min = 0x0};
					enum {ValueMask_Min = 0x00000000};
					enum {Value_Max = 0x1F};
					enum {ValueMask_Max = 0x001F0000};
					};
				namespace VRb { // Field Description
					enum {Lsb = 13};
					enum {FieldMask = 0x00002000};
					enum {Value_Invalid = 0x0};
					enum {ValueMask_Invalid = 0x00000000};
					enum {Value_Valid = 0x1};
					enum {ValueMask_Valid = 0x00002000};
					};
				namespace RbPTR { // Field Description
					enum {Lsb = 8};
					enum {FieldMask = 0x00001F00};
					enum {Value_Min = 0x0};
					enum {ValueMask_Min = 0x00000000};
					enum {Value_Max = 0x1F};
					enum {ValueMask_Max = 0x00001F00};
					};
				namespace VRa { // Field Description
					enum {Lsb = 5};
					enum {FieldMask = 0x00000020};
					enum {Value_Invalid = 0x0};
					enum {ValueMask_Invalid = 0x00000000};
					enum {Value_Valid = 0x1};
					enum {ValueMask_Valid = 0x00000020};
					};
				namespace RaPTR { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x0000001F};
					enum {Value_Min = 0x0};
					enum {ValueMask_Min = 0x00000000};
					enum {Value_Max = 0x1F};
					enum {ValueMask_Max = 0x0000001F};
					};
				};
			namespace SIRAM { // Register description
				typedef uint32_t	Reg;
				namespace LOOP { // Field Description
					enum {Lsb = 31};
					enum {FieldMask = 0x80000000};
					enum {Value_Normal = 0x0};
					enum {ValueMask_Normal = 0x00000000};
					enum {Value_Loopback = 0x1};
					enum {ValueMask_Loopback = 0x80000000};
					};
				namespace SWTR { // Field Description
					enum {Lsb = 30};
					enum {FieldMask = 0x40000000};
					enum {Value_Normal = 0x0};
					enum {ValueMask_Normal = 0x00000000};
					enum {Value_UseL1RxTxDx = 0x1};
					enum {ValueMask_UseL1RxTxDx = 0x40000000};
					};
				namespace SSEL4 { // Field Description
					enum {Lsb = 29};
					enum {FieldMask = 0x20000000};
					enum {Value_Disable = 0x0};
					enum {ValueMask_Disable = 0x00000000};
					enum {Value_Enable = 0x0};
					enum {ValueMask_Enable = 0x00000000};
					};
				namespace SSEL3 { // Field Description
					enum {Lsb = 28};
					enum {FieldMask = 0x10000000};
					enum {Value_Disable = 0x0};
					enum {ValueMask_Disable = 0x00000000};
					enum {Value_Enable = 0x0};
					enum {ValueMask_Enable = 0x00000000};
					};
				namespace SSEL2 { // Field Description
					enum {Lsb = 27};
					enum {FieldMask = 0x08000000};
					enum {Value_Disable = 0x0};
					enum {ValueMask_Disable = 0x00000000};
					enum {Value_Enable = 0x0};
					enum {ValueMask_Enable = 0x00000000};
					};
				namespace SSEL1 { // Field Description
					enum {Lsb = 26};
					enum {FieldMask = 0x04000000};
					enum {Value_Disable = 0x0};
					enum {ValueMask_Disable = 0x00000000};
					enum {Value_Enable = 0x0};
					enum {ValueMask_Enable = 0x00000000};
					};
				namespace CSEL { // Field Description
					enum {Lsb = 22};
					enum {FieldMask = 0x01C00000};
					enum {Value_TimeSlotNotUsed = 0x0};
					enum {ValueMask_TimeSlotNotUsed = 0x00000000};
					enum {Value_SCC1 = 0x1};
					enum {ValueMask_SCC1 = 0x00400000};
					enum {Value_SCC2 = 0x2};
					enum {ValueMask_SCC2 = 0x00800000};
					enum {Value_SCC3 = 0x3};
					enum {ValueMask_SCC3 = 0x00C00000};
					enum {Value_SCC4 = 0x4};
					enum {ValueMask_SCC4 = 0x01000000};
					enum {Value_SMC1 = 0x5};
					enum {ValueMask_SMC1 = 0x01400000};
					enum {Value_SMC2 = 0x6};
					enum {ValueMask_SMC2 = 0x01800000};
					enum {Value_DChannelGrant = 0x7};
					enum {ValueMask_DChannelGrant = 0x01C00000};
					};
				namespace CNT { // Field Description
					enum {Lsb = 18};
					enum {FieldMask = 0x003C0000};
					enum {Value_Min = 0x0};
					enum {ValueMask_Min = 0x00000000};
					enum {Value_Max = 0xF};
					enum {ValueMask_Max = 0x003C0000};
					};
				namespace BYT { // Field Description
					enum {Lsb = 17};
					enum {FieldMask = 0x00020000};
					enum {Value_BitResolution = 0x0};
					enum {ValueMask_BitResolution = 0x00000000};
					enum {Value_ByteResolution = 0x1};
					enum {ValueMask_ByteResolution = 0x00020000};
					};
				namespace LST { // Field Description
					enum {Lsb = 16};
					enum {FieldMask = 0x00010000};
					enum {Value_NotLastEntry = 0x0};
					enum {ValueMask_NotLastEntry = 0x00000000};
					enum {Value_LastEntry = 0x1};
					enum {ValueMask_LastEntry = 0x00010000};
					};
				};
			};
		};
	};
#endif
