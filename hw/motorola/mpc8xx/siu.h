/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _hw_mot_mpc8xx_siuh_
#define _hw_mot_mpc8xx_siuh_
#include "siureg.h"

/** */
namespace Oscl {
/** */
namespace Mot8xx {
/** */
namespace Siu {

/** */
typedef struct Map {
	volatile SIUMCR::Reg		siumcr;
	volatile SYPCR::Reg			sypcr;
	uint8_t					reserved1[0x00E - 0x008];
	volatile SWSR::Reg			swsr;
	volatile SIPEND::Reg		sipend;
	volatile SIMASK::Reg		simask;
	volatile SIEL::Reg			siel;
	const volatile SIVEC::Reg	sivec;
	uint8_t					reserved2[3];
	const volatile TESR::Reg	tesr;
	uint8_t					reserved3[0x030-0x024];
	volatile SDCR::Reg			sdcr;
	}Map;

};
};
};

#endif
