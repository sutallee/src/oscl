/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _hw_mot_mpc8xx_scch_
#define _hw_mot_mpc8xx_scch_
#include "sccreg.h"
#include "oscl/bits/bitfield.h"

/** */
namespace Oscl {
/** */
namespace Mot8xx {
/** */
namespace Scc {

/** */
typedef struct Map {
	/** */
	Oscl::BitField<GSMR_L::Reg>		gsmrl;
	/** */
	Oscl::BitField<GSMR_H::Reg>		gsmrh;
	/** */
	Oscl::BitField<PSMR::Reg>		psmr;
	/** */
	uint8_t		reserved1[0xA0C-0xA0A];
	/** */
	volatile TODR::Reg				todr;
	/** */
	Oscl::BitField<DSR::Reg>		dsr;
	/** */
	volatile SCCE::Reg				scce;
	/** */
	uint8_t		reserved2[0xA14-0xA12];
	/** */
	Oscl::BitField<SCCM::Reg>		sccm;
	/** */
	uint8_t		reserved3[0xA17-0xA16];
	/** */
	volatile SCCS::Reg				sccs;
	/** */
	uint8_t		reserved4[0xA20-0xA18];
	}Map;

};
};
};

#endif
