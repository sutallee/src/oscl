/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _hw_mot_mpc8xx_mpc850_pcmciah_
#define _hw_mot_mpc8xx_mpc850_pcmciah_
#include "oscl/hw/motorola/mpc8xx/pcmcia.h"

/** */
namespace Oscl {
/** */
namespace Mot8xx {
/** */
namespace Mpc850 {
/** */
namespace Pcmcia {

/** */
typedef struct Map {
	Mot8xx::Pcmcia::Port		port[8];
	uint8_t					reserved1[0x0E4-0x0C0];
	Mot8xx::Pcmcia::PGCR::Reg	pgcrb;
	uint8_t					reserved2[2];
	Mot8xx::Pcmcia::PSCR::Reg	pscrb;
	uint8_t					reserved3[0x0F2-0x0EC];
	Mot8xx::Pcmcia::PIPR::Reg	piprb;
	uint8_t					reserved4[0x0FA-0x0F4];
	Mot8xx::Pcmcia::PER::Reg	perb;
	}Map;

};
};
};
};

#endif
