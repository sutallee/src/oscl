/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _hw_mot8xx_smcregh_
#define _hw_mot8xx_smcregh_
#include <stdint.h>
namespace Oscl { // Namespace description
	namespace Mot8xx { // Namespace description
		namespace Smc { // Namespace description
			namespace SMCMR { // Register description
				typedef uint16_t	Reg;
				namespace CLEN { // Field Description
					enum {Lsb = 11};
					enum {FieldMask = 0x00007800};
					enum {Value_Min = 0x0};
					enum {ValueMask_Min = 0x00000000};
					enum {Value_Max = 0xF};
					enum {ValueMask_Max = 0x00007800};
					};
				namespace SL { // Field Description
					enum {Lsb = 10};
					enum {FieldMask = 0x00000400};
					enum {Value_OneStopBit = 0x0};
					enum {ValueMask_OneStopBit = 0x00000000};
					enum {Value_TwoStopBits = 0x1};
					enum {ValueMask_TwoStopBits = 0x00000400};
					};
				namespace PEN { // Field Description
					enum {Lsb = 9};
					enum {FieldMask = 0x00000200};
					enum {Value_NoParity = 0x0};
					enum {ValueMask_NoParity = 0x00000000};
					enum {Value_ParityEnable = 0x1};
					enum {ValueMask_ParityEnable = 0x00000200};
					};
				namespace PM { // Field Description
					enum {Lsb = 8};
					enum {FieldMask = 0x00000100};
					enum {Value_OddParity = 0x0};
					enum {ValueMask_OddParity = 0x00000000};
					enum {Value_EvenParity = 0x1};
					enum {ValueMask_EvenParity = 0x00000100};
					};
				namespace SM { // Field Description
					enum {Lsb = 4};
					enum {FieldMask = 0x00000030};
					enum {Value_GciOrScit = 0x0};
					enum {ValueMask_GciOrScit = 0x00000000};
					enum {Value_UART = 0x2};
					enum {ValueMask_UART = 0x00000020};
					enum {Value_Transparent = 0x3};
					enum {ValueMask_Transparent = 0x00000030};
					};
				namespace DM { // Field Description
					enum {Lsb = 2};
					enum {FieldMask = 0x0000000C};
					enum {Value_Normal = 0x0};
					enum {ValueMask_Normal = 0x00000000};
					enum {Value_LocalLoop = 0x1};
					enum {ValueMask_LocalLoop = 0x00000004};
					enum {Value_Echo = 0x2};
					enum {ValueMask_Echo = 0x00000008};
					};
				namespace TEN { // Field Description
					enum {Lsb = 1};
					enum {FieldMask = 0x00000002};
					enum {Value_TxDisable = 0x0};
					enum {ValueMask_TxDisable = 0x00000000};
					enum {Value_TxEnable = 0x1};
					enum {ValueMask_TxEnable = 0x00000002};
					};
				namespace REN { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x00000001};
					enum {Value_RxDisable = 0x0};
					enum {ValueMask_RxDisable = 0x00000000};
					enum {Value_RxEnable = 0x1};
					enum {ValueMask_RxEnable = 0x00000001};
					};
				};
			namespace SMCE { // Register description
				typedef uint8_t	Reg;
				namespace RX { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x00000001};
					enum {Value_Clear = 0x1};
					enum {ValueMask_Clear = 0x00000001};
					enum {Value_Pending = 0x1};
					enum {ValueMask_Pending = 0x00000001};
					enum {Value_NotPending = 0x0};
					enum {ValueMask_NotPending = 0x00000000};
					};
				namespace TX { // Field Description
					enum {Lsb = 1};
					enum {FieldMask = 0x00000002};
					enum {Value_Clear = 0x1};
					enum {ValueMask_Clear = 0x00000002};
					enum {Value_Pending = 0x1};
					enum {ValueMask_Pending = 0x00000002};
					enum {Value_NotPending = 0x0};
					enum {ValueMask_NotPending = 0x00000000};
					};
				namespace BSY { // Field Description
					enum {Lsb = 2};
					enum {FieldMask = 0x00000004};
					enum {Value_Clear = 0x1};
					enum {ValueMask_Clear = 0x00000004};
					enum {Value_Pending = 0x1};
					enum {ValueMask_Pending = 0x00000004};
					enum {Value_NotPending = 0x0};
					enum {ValueMask_NotPending = 0x00000000};
					};
				namespace BRK { // Field Description
					enum {Lsb = 4};
					enum {FieldMask = 0x00000010};
					enum {Value_Clear = 0x1};
					enum {ValueMask_Clear = 0x00000010};
					enum {Value_Pending = 0x1};
					enum {ValueMask_Pending = 0x00000010};
					enum {Value_NotPending = 0x0};
					enum {ValueMask_NotPending = 0x00000000};
					};
				namespace BRKE { // Field Description
					enum {Lsb = 6};
					enum {FieldMask = 0x00000040};
					enum {Value_Clear = 0x1};
					enum {ValueMask_Clear = 0x00000040};
					enum {Value_Pending = 0x1};
					enum {ValueMask_Pending = 0x00000040};
					enum {Value_NotPending = 0x0};
					enum {ValueMask_NotPending = 0x00000000};
					};
				};
			namespace SMCM { // Register description
				typedef uint8_t	Reg;
				namespace RX { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x00000001};
					enum {Value_Disable = 0x0};
					enum {ValueMask_Disable = 0x00000000};
					enum {Value_Enable = 0x1};
					enum {ValueMask_Enable = 0x00000001};
					};
				namespace TX { // Field Description
					enum {Lsb = 1};
					enum {FieldMask = 0x00000002};
					enum {Value_Disable = 0x0};
					enum {ValueMask_Disable = 0x00000000};
					enum {Value_Enable = 0x1};
					enum {ValueMask_Enable = 0x00000002};
					};
				namespace BSY { // Field Description
					enum {Lsb = 2};
					enum {FieldMask = 0x00000004};
					enum {Value_Disable = 0x0};
					enum {ValueMask_Disable = 0x00000000};
					enum {Value_Enable = 0x1};
					enum {ValueMask_Enable = 0x00000004};
					};
				namespace BRK { // Field Description
					enum {Lsb = 4};
					enum {FieldMask = 0x00000010};
					enum {Value_Disable = 0x0};
					enum {ValueMask_Disable = 0x00000000};
					enum {Value_Enable = 0x1};
					enum {ValueMask_Enable = 0x00000010};
					};
				namespace BRKE { // Field Description
					enum {Lsb = 6};
					enum {FieldMask = 0x00000040};
					enum {Value_Disable = 0x0};
					enum {ValueMask_Disable = 0x00000000};
					enum {Value_Enable = 0x1};
					enum {ValueMask_Enable = 0x00000040};
					};
				};
			namespace PRAM { // Namespace description
				namespace RBASE { // Register description
					typedef uint16_t	Reg;
					enum {Min = 0};
					enum {Max = 65535};
					};
				namespace TBASE { // Register description
					typedef uint16_t	Reg;
					enum {Min = 0};
					enum {Max = 65535};
					};
				namespace FCR { // Register description
					typedef uint8_t	Reg;
					namespace AT { // Field Description
						enum {Lsb = 0};
						enum {FieldMask = 0x00000007};
						enum {Value_Min = 0x0};
						enum {ValueMask_Min = 0x00000000};
						enum {Value_Max = 0x7};
						enum {ValueMask_Max = 0x00000007};
						};
					namespace BO { // Field Description
						enum {Lsb = 3};
						enum {FieldMask = 0x00000018};
						enum {Value_PpcLittleEndian = 0x1};
						enum {ValueMask_PpcLittleEndian = 0x00000008};
						enum {Value_BigEndian = 0x2};
						enum {ValueMask_BigEndian = 0x00000010};
						enum {Value_TrueLittleEndian = 0x2};
						enum {ValueMask_TrueLittleEndian = 0x00000010};
						};
					};
				namespace MRBLR { // Register description
					typedef uint16_t	Reg;
					enum {Min = 1};
					enum {EtherAlign = 4};
					enum {HdlcAlign = 4};
					enum {Max = 65535};
					};
				namespace UART { // Namespace description
					namespace MAX_IDL { // Register description
						typedef uint16_t	Reg;
						enum {Disable = 0};
						enum {Min = 1};
						enum {Max = 65535};
						};
					namespace IDLC { // Register description
						typedef uint16_t	Reg;
						};
					namespace BRKLN { // Register description
						typedef uint16_t	Reg;
						};
					namespace BRKEC { // Register description
						typedef uint16_t	Reg;
						};
					namespace BRKCR { // Register description
						typedef uint16_t	Reg;
						};
					};
				};
			};
		};
	};
#endif
