/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/


module hw_mot8xx_siuregh {
	include "oscl/compiler/types.h";
	namespace Oscl {
		namespace Mot8xx {
			namespace Siu {
				register SIUMCR uint32_t {
					field EARB 31 1 {
						value InternalArbitration 0
						value ExternalArbitration 1
						}
					field EARP 28 3 {
						value LowestPriority 0
						value HighestPriority 7
						}
					field DSHW 23 1 {
						value DisableInternalDataShowCycles 0
						value EnableInternalDataShowCycles 1
						}
					field DBGC 21 2 {
						value IP_B 0
						value WP 1
						value VFLS 3
						}
					field DBPC 19 2 {
						value Mode00 0
						value Mode01 1
						value Mode11 3
						}
					field FRC 17 1 {
						value FRZ 0
						value IRQ6 1
						}
					field DLK 16 1 {
						value Unlocked 0
						value Locked 1
						}
					field OPAR 15 1 {
						value EvenParity 0
						value OddParity 1
						}
					field PNCS 14 1 {
						value DisableParity 0
						value EnableParity 1
						}
					field DPC 13 1 {
						value IRQ 0
						value Parity 1
						}
					field MPRE 12 1 {
						value IRQ 0
						value RSV 1
						}
					field MLRC 10 2 {
						value IRQ4 0
						value TriState 1
						value KrRetry 2
						value SPKROUT 2
						}
					field AEME 9 1 {
						value ExternalMasterDisabled 0
						value ExternalMasterEnabled 1
						}
					field SEME 8 1 {
						value ExternalMasterDisabled 0
						value ExternalMasterEnabled 1
						}
					field BSC 7 1 {
						value Dedicated 0
						value Dual 1
						}
					field GB5E 6 1 {
						value BDIP 0
						value GPL_B5 1
						}
					field B2DD 5 1 {
						value CS2Only 0
						value CS2AndGPL_x2 1
						}
					field B3DD 4 1 {
						value CS3Only 0
						value CS3AndGPL_x3 1
						}
					}
				register SYPCR uint32_t {
					field SWTC 16 16 {
						value Max	0xFFFF
						}
					field BMT 8 8 {
						value Max	0xFF
						}
					field BME 7 1 {
						value BusMonitorDisabled	0
						value BusMonitorEnabled		1
						}
					field SWF 3 1 {
						value Continue	0
						value Stop		1
						}
					field SWE 2 1 {
						value Disabled	0
						value Enabled	1
						}
					field SWRI 1 1 {
						value NMI		0
						value HRESET	1
						}
					field SWP 0 1 {
						value NotPrescaled	0
						value Prescale2048	1
						}
					}
				register SWSR uint16_t {
					value WatchDogResetValue1	0x556C
					value WatchDogResetValue2	0xAA39
					}
				register SIPEND uint32_t {
					field IRM0 31 1 {
						value Disabled	0
						value Enabled		1
						}
					field LVM0 30 1 {
						value Disabled	0
						value Enabled		1
						}
					field IRM1 29 1 {
						value Disabled	0
						value Enabled		1
						}
					field LVM1 28 1 {
						value Disabled	0
						value Enabled		1
						}
					field IRM2 27 1 {
						value Disabled	0
						value Enabled		1
						}
					field LVM2 26 1 {
						value Disabled	0
						value Enabled		1
						}
					field IRM3 25 1 {
						value Disabled	0
						value Enabled		1
						}
					field LVM3 24 1 {
						value Disabled	0
						value Enabled		1
						}
					field IRM4 23 1 {
						value Disabled	0
						value Enabled		1
						}
					field LVM4 22 1 {
						value Disabled	0
						value Enabled		1
						}
					field IRM5 21 1 {
						value Disabled	0
						value Enabled		1
						}
					field LVM5 20 1 {
						value Disabled	0
						value Enabled		1
						}
					field IRM6 19 1 {
						value Disabled	0
						value Enabled		1
						}
					field LVM6 18 1 {
						value Disabled	0
						value Enabled		1
						}
					field IRM7 17 1 {
						value Disabled	0
						value Enabled		1
						}
					field LVM7 16 1 {
						value Disabled	0
						value Enabled		1
						}
					}
				register SIMASK uint32_t {
					field IRM0 31 1 {
						value IntDisabled	0
						value IntEnabled	1
						}
					field LVM0 30 1 {
						value IntDisabled	0
						value IntEnabled	1
						}
					field IRM1 29 1 {
						value IntDisabled	0
						value IntEnabled	1
						}
					field LVM1 28 1 {
						value IntDisabled	0
						value IntEnabled	1
						}
					field IRM2 27 1 {
						value IntDisabled	0
						value IntEnabled	1
						}
					field LVM2 26 1 {
						value IntDisabled	0
						value IntEnabled	1
						}
					field IRM3 25 1 {
						value IntDisabled	0
						value IntEnabled	1
						}
					field LVM3 24 1 {
						value IntDisabled	0
						value IntEnabled	1
						}
					field IRM4 23 1 {
						value IntDisabled	0
						value IntEnabled	1
						}
					field LVM4 22 1 {
						value IntDisabled	0
						value IntEnabled	1
						}
					field IRM5 21 1 {
						value IntDisabled	0
						value IntEnabled	1
						}
					field LVM5 20 1 {
						value IntDisabled	0
						value IntEnabled	1
						}
					field IRM6 19 1 {
						value IntDisabled	0
						value IntEnabled	1
						}
					field LVM6 18 1 {
						value IntDisabled	0
						value IntEnabled	1
						}
					field IRM7 17 1 {
						value IntDisabled	0
						value IntEnabled	1
						}
					field LVM7 16 1 {
						value IntDisabled	0
						value IntEnabled	1
						}
					}
				register SIEL uint32_t {
					field ED0 31 1 {
						value LevelLow		0
						value FallingEdge	1
						}
					field WM0 30 1 {
						value NoWakeUp	0
						value WakeUp	1
						}
					field ED1 29 1 {
						value LevelLow		0
						value FallingEdge	1
						}
					field WM1 28 1 {
						value NoWakeUp	0
						value WakeUp	1
						}
					field ED2 27 1 {
						value LevelLow		0
						value FallingEdge	1
						}
					field WM2 26 1 {
						value NoWakeUp	0
						value WakeUp	1
						}
					field ED3 25 1 {
						value LevelLow		0
						value FallingEdge	1
						}
					field WM3 24 1 {
						value NoWakeUp	0
						value WakeUp	1
						}
					field ED4 23 1 {
						value LevelLow		0
						value FallingEdge	1
						}
					field WM4 22 1 {
						value NoWakeUp	0
						value WakeUp	1
						}
					field ED5 21 1 {
						value LevelLow		0
						value FallingEdge	1
						}
					field WM5 20 1 {
						value NoWakeUp	0
						value WakeUp	1
						}
					field ED6 19 1 {
						value LevelLow		0
						value FallingEdge	1
						}
					field WM6 18 1 {
						value NoWakeUp	0
						value WakeUp	1
						}
					field ED7 17 1 {
						value LevelLow		0
						value FallingEdge	1
						}
					field WM7 16 1 {
						value NoWakeUp	0
						value WakeUp	1
						}
					}
				register SIVEC uint8_t {
					value MaxValue	0xFF
					}
				register TESR uint32_t {
					field IEXT 13 1 {
						value NoError	0
						value InstructionTeaError	1
						}
					field ITMT 12 1 {
						value NoError					0
						value InstructionTimeoutError	1
						}
					field IPB0 11 1 {
						value NoError		0
						value ParityError	1
						}
					field IPB1 10 1 {
						value NoError		0
						value ParityError	1
						}
					field IPB2 9 1 {
						value NoError		0
						value ParityError	1
						}
					field IPB3 8 1 {
						value NoError		0
						value ParityError	1
						}
					field DEXT 5 1 {
						value NoError		0
						value DataTeaError	1
						}
					field DTMT 4 1 {
						value NoError			0
						value DataTimeoutError	1
						}
					field DPB0 3 1 {
						value NoError		0
						value ParityError	1
						}
					field DPB1 2 1 {
						value NoError		0
						value ParityError	1
						}
					field DPB2 1 1 {
						value NoError		0
						value ParityError	1
						}
					field DPB3 0 1 {
						value NoError		0
						value ParityError	1
						}
					}
				register SDCR uint32_t {
					field FRZ 13 2 {
						value IgnoreFreeze		0
						value RespondToFreeze	1
						}
					field RAID 0 2 {
						value PriorityLevel6	0
						value PriorityLevel5	1
						value NormalOperation	1
						value PriorityLevel2	2
						value PriorityLevel1	3
						}
					}
				}
			}
		}
	}

