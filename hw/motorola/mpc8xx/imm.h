/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _hw_mot_mpc8xx_mpc850_immh_
#define _hw_mot_mpc8xx_mpc850_immh_
#include "siu.h"
#include "memc.h"
#include "sitimer.h"
#include "clkrst.h"
#include "sitkey.h"
#include "clkrstkey.h"
#include "dma.h"
#include "cpmic.h"
#include "cpmtimer.h"
#include "cpm.h"
#include "dpram.h"
#include "xdpram.h"
#include "pram.h"

/** */
namespace Oscl {
/** */
namespace Mot8xx {

/** */
typedef struct Map {
	Mot8xx::Siu::Map		siu;
	uint8_t				reserved1[0x100-0x034];
	Mot8xx::Memc::Map		memc;
	uint8_t				reserved2[0x200-0x180];
	Mot8xx::SiTimer::Map	sitimer;
	uint8_t				reserved3[0x280-0x24C];
	Mot8xx::ClkRst::Map		clkrst;
	uint8_t				reserved4[0x300-0x28C];
	Mot8xx::SitKey::Map		sitkey;
	uint8_t				reserved5[0x380-0x348];
	Mot8xx::ClkRstKey::Map	clkrstkey;
	uint8_t				reserved6[0x904-0x38C];
	Mot8xx::Dma::Map		dma;
	uint8_t				reserved7[0x930-0x91D];
	Mot8xx::Cpmic::Map		cpmic;
	uint8_t				reserved8[0x980-0x950];
	Mot8xx::CpmTimer::Map	cpmtimer;
	uint8_t				reserved9[0x9C0-0x9B8];
	Mot8xx::Cpm::Map		cpm;
	uint8_t				reserved10[0x2000-0x9DC];
	Mot8xx::DpRam::Map		dpram;
	Mot8xx::XDpRam::Map		xdpram;
	Mot8xx::Pram::Map		pram;
	} Map;

};
};

#endif
