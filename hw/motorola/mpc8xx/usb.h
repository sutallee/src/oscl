/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _hw_mot_mpc8xx_usbh_
#define _hw_mot_mpc8xx_usbh_
#include "usbreg.h"

/** */
namespace Oscl {
/** */
namespace Mot8xx {
/** */
namespace Usb {

/** */
typedef struct Map {
	USMOD::Reg		usmod;
	USADR::Reg		usadr;
	USCOM::Reg		uscom;
	uint8_t		reserved1[0xA06-0xA05];
	USEP::Reg		usep0;
	USEP::Reg		usep1;
	USEP::Reg		usep2;
	USEP::Reg		usep3;
	uint8_t		reserved2[0xA10-0xA0C];
	USBER::Reg		usber;
	uint8_t		reserved3[0xA14-0xA12];
	USBMR::Reg		usbmr;
	uint8_t		reserved4[0xA17-0xA16];
	USBS::Reg		usbs;
	}Map;

};
};
};

#endif
