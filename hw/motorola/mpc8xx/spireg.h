/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _hw_mot8xx_spiregh_
#define _hw_mot8xx_spiregh_
#include <stdint.h>
namespace Oscl { // Namespace description
	namespace Mot8xx { // Namespace description
		namespace Spi { // Namespace description
			namespace SPMODE { // Register description
				typedef uint16_t	Reg;
				namespace LOOP { // Field Description
					enum {Lsb = 14};
					enum {FieldMask = 0x00004000};
					enum {Value_Normal = 0x0};
					enum {ValueMask_Normal = 0x00000000};
					enum {Value_Loopback = 0x1};
					enum {ValueMask_Loopback = 0x00004000};
					};
				namespace CI { // Field Description
					enum {Lsb = 13};
					enum {FieldMask = 0x00002000};
					enum {Value_Low = 0x0};
					enum {ValueMask_Low = 0x00000000};
					enum {Value_High = 0x1};
					enum {ValueMask_High = 0x00002000};
					};
				namespace CP { // Field Description
					enum {Lsb = 12};
					enum {FieldMask = 0x00001000};
					enum {Value_Middle = 0x0};
					enum {ValueMask_Middle = 0x00000000};
					enum {Value_Beginning = 0x1};
					enum {ValueMask_Beginning = 0x00001000};
					};
				namespace DIV16 { // Field Description
					enum {Lsb = 11};
					enum {FieldMask = 0x00000800};
					enum {Value_DivideBy1 = 0x0};
					enum {ValueMask_DivideBy1 = 0x00000000};
					enum {Value_DivideBy16 = 0x1};
					enum {ValueMask_DivideBy16 = 0x00000800};
					};
				namespace REV { // Field Description
					enum {Lsb = 10};
					enum {FieldMask = 0x00000400};
					enum {Value_ReverseData = 0x0};
					enum {ValueMask_ReverseData = 0x00000000};
					enum {Value_NormalData = 0x1};
					enum {ValueMask_NormalData = 0x00000400};
					};
				namespace MS { // Field Description
					enum {Lsb = 9};
					enum {FieldMask = 0x00000200};
					enum {Value_Slave = 0x0};
					enum {ValueMask_Slave = 0x00000000};
					enum {Value_Master = 0x1};
					enum {ValueMask_Master = 0x00000200};
					};
				namespace EN { // Field Description
					enum {Lsb = 8};
					enum {FieldMask = 0x00000100};
					enum {Value_Disable = 0x0};
					enum {ValueMask_Disable = 0x00000000};
					enum {Value_Enable = 0x1};
					enum {ValueMask_Enable = 0x00000100};
					};
				namespace LEN { // Field Description
					enum {Lsb = 4};
					enum {FieldMask = 0x000000F0};
					enum {Value_BitsPerCharIs4 = 0x3};
					enum {ValueMask_BitsPerCharIs4 = 0x00000030};
					enum {Value_BitsPerCharIs5 = 0x4};
					enum {ValueMask_BitsPerCharIs5 = 0x00000040};
					enum {Value_BitsPerCharIs6 = 0x5};
					enum {ValueMask_BitsPerCharIs6 = 0x00000050};
					enum {Value_BitsPerCharIs7 = 0x6};
					enum {ValueMask_BitsPerCharIs7 = 0x00000060};
					enum {Value_BitsPerCharIs8 = 0x7};
					enum {ValueMask_BitsPerCharIs8 = 0x00000070};
					enum {Value_BitsPerCharIs9 = 0x8};
					enum {ValueMask_BitsPerCharIs9 = 0x00000080};
					enum {Value_BitsPerCharIs10 = 0x9};
					enum {ValueMask_BitsPerCharIs10 = 0x00000090};
					enum {Value_BitsPerCharIs11 = 0xA};
					enum {ValueMask_BitsPerCharIs11 = 0x000000A0};
					enum {Value_BitsPerCharIs12 = 0xB};
					enum {ValueMask_BitsPerCharIs12 = 0x000000B0};
					enum {Value_BitsPerCharIs13 = 0xC};
					enum {ValueMask_BitsPerCharIs13 = 0x000000C0};
					enum {Value_BitsPerCharIs14 = 0xD};
					enum {ValueMask_BitsPerCharIs14 = 0x000000D0};
					enum {Value_BitsPerCharIs15 = 0xE};
					enum {ValueMask_BitsPerCharIs15 = 0x000000E0};
					enum {Value_BitsPerCharIs16 = 0xF};
					enum {ValueMask_BitsPerCharIs16 = 0x000000F0};
					};
				namespace PM { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x0000000F};
					enum {Value_Max = 0xF};
					enum {ValueMask_Max = 0x0000000F};
					enum {Value_DivideBy4 = 0x0};
					enum {ValueMask_DivideBy4 = 0x00000000};
					enum {Value_DivideBy8 = 0x1};
					enum {ValueMask_DivideBy8 = 0x00000001};
					enum {Value_DivideBy12 = 0x2};
					enum {ValueMask_DivideBy12 = 0x00000002};
					enum {Value_DivideBy16 = 0x3};
					enum {ValueMask_DivideBy16 = 0x00000003};
					enum {Value_DivideBy20 = 0x4};
					enum {ValueMask_DivideBy20 = 0x00000004};
					enum {Value_DivideBy24 = 0x5};
					enum {ValueMask_DivideBy24 = 0x00000005};
					enum {Value_DivideBy28 = 0x6};
					enum {ValueMask_DivideBy28 = 0x00000006};
					enum {Value_DivideBy32 = 0x7};
					enum {ValueMask_DivideBy32 = 0x00000007};
					enum {Value_DivideBy36 = 0x8};
					enum {ValueMask_DivideBy36 = 0x00000008};
					enum {Value_DivideBy40 = 0x9};
					enum {ValueMask_DivideBy40 = 0x00000009};
					enum {Value_DivideBy44 = 0xA};
					enum {ValueMask_DivideBy44 = 0x0000000A};
					enum {Value_DivideBy48 = 0xB};
					enum {ValueMask_DivideBy48 = 0x0000000B};
					enum {Value_DivideBy52 = 0xC};
					enum {ValueMask_DivideBy52 = 0x0000000C};
					enum {Value_DivideBy56 = 0xD};
					enum {ValueMask_DivideBy56 = 0x0000000D};
					enum {Value_DivideBy60 = 0xE};
					enum {ValueMask_DivideBy60 = 0x0000000E};
					enum {Value_DivideBy64 = 0xF};
					enum {ValueMask_DivideBy64 = 0x0000000F};
					};
				};
			namespace SPIE { // Register description
				typedef uint8_t	Reg;
				namespace MME { // Field Description
					enum {Lsb = 5};
					enum {FieldMask = 0x00000020};
					enum {Value_Clear = 0x1};
					enum {ValueMask_Clear = 0x00000020};
					enum {Value_Pending = 0x1};
					enum {ValueMask_Pending = 0x00000020};
					enum {Value_NotPending = 0x0};
					enum {ValueMask_NotPending = 0x00000000};
					};
				namespace TXE { // Field Description
					enum {Lsb = 4};
					enum {FieldMask = 0x00000010};
					enum {Value_Clear = 0x1};
					enum {ValueMask_Clear = 0x00000010};
					enum {Value_Pending = 0x1};
					enum {ValueMask_Pending = 0x00000010};
					enum {Value_NotPending = 0x0};
					enum {ValueMask_NotPending = 0x00000000};
					};
				namespace BSY { // Field Description
					enum {Lsb = 2};
					enum {FieldMask = 0x00000004};
					enum {Value_Clear = 0x1};
					enum {ValueMask_Clear = 0x00000004};
					enum {Value_Pending = 0x1};
					enum {ValueMask_Pending = 0x00000004};
					enum {Value_NotPending = 0x0};
					enum {ValueMask_NotPending = 0x00000000};
					};
				namespace TXB { // Field Description
					enum {Lsb = 1};
					enum {FieldMask = 0x00000002};
					enum {Value_Clear = 0x1};
					enum {ValueMask_Clear = 0x00000002};
					enum {Value_Pending = 0x1};
					enum {ValueMask_Pending = 0x00000002};
					enum {Value_NotPending = 0x0};
					enum {ValueMask_NotPending = 0x00000000};
					};
				namespace RXB { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x00000001};
					enum {Value_Clear = 0x1};
					enum {ValueMask_Clear = 0x00000001};
					enum {Value_Pending = 0x1};
					enum {ValueMask_Pending = 0x00000001};
					enum {Value_NotPending = 0x0};
					enum {ValueMask_NotPending = 0x00000000};
					};
				};
			namespace SPIM { // Register description
				typedef uint8_t	Reg;
				namespace MME { // Field Description
					enum {Lsb = 5};
					enum {FieldMask = 0x00000020};
					enum {Value_Disable = 0x0};
					enum {ValueMask_Disable = 0x00000000};
					enum {Value_Enable = 0x1};
					enum {ValueMask_Enable = 0x00000020};
					};
				namespace TXE { // Field Description
					enum {Lsb = 4};
					enum {FieldMask = 0x00000010};
					enum {Value_Disable = 0x0};
					enum {ValueMask_Disable = 0x00000000};
					enum {Value_Enable = 0x1};
					enum {ValueMask_Enable = 0x00000010};
					};
				namespace BSY { // Field Description
					enum {Lsb = 2};
					enum {FieldMask = 0x00000004};
					enum {Value_Disable = 0x0};
					enum {ValueMask_Disable = 0x00000000};
					enum {Value_Enable = 0x1};
					enum {ValueMask_Enable = 0x00000004};
					};
				namespace TXB { // Field Description
					enum {Lsb = 1};
					enum {FieldMask = 0x00000002};
					enum {Value_Disable = 0x0};
					enum {ValueMask_Disable = 0x00000000};
					enum {Value_Enable = 0x1};
					enum {ValueMask_Enable = 0x00000002};
					};
				namespace RXB { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x00000001};
					enum {Value_Disable = 0x0};
					enum {ValueMask_Disable = 0x00000000};
					enum {Value_Enable = 0x1};
					enum {ValueMask_Enable = 0x00000001};
					};
				};
			namespace SPCOM { // Register description
				typedef uint8_t	Reg;
				namespace STR { // Field Description
					enum {Lsb = 7};
					enum {FieldMask = 0x00000080};
					enum {Value_Start = 0x1};
					enum {ValueMask_Start = 0x00000080};
					};
				};
			namespace PRAM { // Namespace description
				namespace RFCR { // Register description
					typedef uint8_t	Reg;
					namespace BO { // Field Description
						enum {Lsb = 3};
						enum {FieldMask = 0x00000018};
						enum {Value_PpcLittleEndian = 0x0};
						enum {ValueMask_PpcLittleEndian = 0x00000000};
						enum {Value_BigEndian = 0x2};
						enum {ValueMask_BigEndian = 0x00000010};
						enum {Value_TrueLittleEndian = 0x2};
						enum {ValueMask_TrueLittleEndian = 0x00000010};
						};
					namespace AT { // Field Description
						enum {Lsb = 0};
						enum {FieldMask = 0x00000007};
						};
					};
				namespace TFCR { // Register description
					typedef uint8_t	Reg;
					namespace BO { // Field Description
						enum {Lsb = 3};
						enum {FieldMask = 0x00000018};
						enum {Value_PpcLittleEndian = 0x0};
						enum {ValueMask_PpcLittleEndian = 0x00000000};
						enum {Value_BigEndian = 0x2};
						enum {ValueMask_BigEndian = 0x00000010};
						enum {Value_TrueLittleEndian = 0x2};
						enum {ValueMask_TrueLittleEndian = 0x00000010};
						};
					namespace AT { // Field Description
						enum {Lsb = 0};
						enum {FieldMask = 0x00000007};
						};
					};
				};
			};
		};
	};
#endif
