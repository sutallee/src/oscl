/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _hw_mot8xx_fecregh_
#define _hw_mot8xx_fecregh_
#include <stdint.h>
namespace Oscl { // Namespace description
	namespace Mot8xx { // Namespace description
		namespace Fec { // Namespace description
			namespace ADDR_LOW { // Register description
				typedef uint32_t	Reg;
				namespace Byte0 { // Field Description
					enum {Lsb = 24};
					enum {FieldMask = 0xFF000000};
					};
				namespace Byte1 { // Field Description
					enum {Lsb = 16};
					enum {FieldMask = 0x00FF0000};
					};
				namespace Byte2 { // Field Description
					enum {Lsb = 8};
					enum {FieldMask = 0x0000FF00};
					};
				namespace Byte3 { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x000000FF};
					};
				};
			namespace ADDR_HIGH { // Register description
				typedef uint32_t	Reg;
				namespace Byte4 { // Field Description
					enum {Lsb = 24};
					enum {FieldMask = 0xFF000000};
					};
				namespace Byte5 { // Field Description
					enum {Lsb = 16};
					enum {FieldMask = 0x00FF0000};
					};
				};
			namespace HASH_TABLE_HIGH { // Register description
				typedef uint32_t	Reg;
				};
			namespace HASH_TABLE_LOW { // Register description
				typedef uint32_t	Reg;
				};
			namespace R_DES_START { // Register description
				typedef uint32_t	Reg;
				enum {Align = 4};
				};
			namespace X_DES_START { // Register description
				typedef uint32_t	Reg;
				enum {Align = 4};
				};
			namespace R_BUFF_SIZE { // Register description
				typedef uint32_t	Reg;
				enum {Min = 16};
				enum {Max = 1008};
				enum {Align = 16};
				};
			namespace ECNTRL { // Register description
				typedef uint32_t	Reg;
				namespace RESET { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x00000001};
					enum {Value_Reset = 0x1};
					enum {ValueMask_Reset = 0x00000001};
					enum {Value_Busy = 0x1};
					enum {ValueMask_Busy = 0x00000001};
					enum {Value_Done = 0x0};
					enum {ValueMask_Done = 0x00000000};
					};
				namespace ETHER_EN { // Field Description
					enum {Lsb = 1};
					enum {FieldMask = 0x00000002};
					enum {Value_Disable = 0x0};
					enum {ValueMask_Disable = 0x00000000};
					enum {Value_Enable = 0x1};
					enum {ValueMask_Enable = 0x00000002};
					};
				namespace FEC_PIN_MUX { // Field Description
					enum {Lsb = 2};
					enum {FieldMask = 0x00000004};
					enum {Value_Enable = 0x1};
					enum {ValueMask_Enable = 0x00000004};
					};
				};
			namespace IEVENT { // Register description
				typedef uint32_t	Reg;
				namespace HBERR { // Field Description
					enum {Lsb = 31};
					enum {FieldMask = 0x80000000};
					enum {Value_Clear = 0x1};
					enum {ValueMask_Clear = 0x80000000};
					enum {Value_Pending = 0x1};
					enum {ValueMask_Pending = 0x80000000};
					enum {Value_NotPending = 0x0};
					enum {ValueMask_NotPending = 0x00000000};
					};
				namespace BABR { // Field Description
					enum {Lsb = 30};
					enum {FieldMask = 0x40000000};
					enum {Value_Clear = 0x1};
					enum {ValueMask_Clear = 0x40000000};
					enum {Value_Pending = 0x1};
					enum {ValueMask_Pending = 0x40000000};
					enum {Value_NotPending = 0x0};
					enum {ValueMask_NotPending = 0x00000000};
					};
				namespace BABT { // Field Description
					enum {Lsb = 29};
					enum {FieldMask = 0x20000000};
					enum {Value_Clear = 0x1};
					enum {ValueMask_Clear = 0x20000000};
					enum {Value_Pending = 0x1};
					enum {ValueMask_Pending = 0x20000000};
					enum {Value_NotPending = 0x0};
					enum {ValueMask_NotPending = 0x00000000};
					};
				namespace GRA { // Field Description
					enum {Lsb = 28};
					enum {FieldMask = 0x10000000};
					enum {Value_Clear = 0x1};
					enum {ValueMask_Clear = 0x10000000};
					enum {Value_Pending = 0x1};
					enum {ValueMask_Pending = 0x10000000};
					enum {Value_NotPending = 0x0};
					enum {ValueMask_NotPending = 0x00000000};
					};
				namespace TFINT { // Field Description
					enum {Lsb = 27};
					enum {FieldMask = 0x08000000};
					enum {Value_Clear = 0x1};
					enum {ValueMask_Clear = 0x08000000};
					enum {Value_Pending = 0x1};
					enum {ValueMask_Pending = 0x08000000};
					enum {Value_NotPending = 0x0};
					enum {ValueMask_NotPending = 0x00000000};
					};
				namespace TXB { // Field Description
					enum {Lsb = 26};
					enum {FieldMask = 0x04000000};
					enum {Value_Clear = 0x1};
					enum {ValueMask_Clear = 0x04000000};
					enum {Value_Pending = 0x1};
					enum {ValueMask_Pending = 0x04000000};
					enum {Value_NotPending = 0x0};
					enum {ValueMask_NotPending = 0x00000000};
					};
				namespace RFINT { // Field Description
					enum {Lsb = 25};
					enum {FieldMask = 0x02000000};
					enum {Value_Clear = 0x1};
					enum {ValueMask_Clear = 0x02000000};
					enum {Value_Pending = 0x1};
					enum {ValueMask_Pending = 0x02000000};
					enum {Value_NotPending = 0x0};
					enum {ValueMask_NotPending = 0x00000000};
					};
				namespace RXB { // Field Description
					enum {Lsb = 24};
					enum {FieldMask = 0x01000000};
					enum {Value_Clear = 0x1};
					enum {ValueMask_Clear = 0x01000000};
					enum {Value_Pending = 0x1};
					enum {ValueMask_Pending = 0x01000000};
					enum {Value_NotPending = 0x0};
					enum {ValueMask_NotPending = 0x00000000};
					};
				namespace MII { // Field Description
					enum {Lsb = 23};
					enum {FieldMask = 0x00800000};
					enum {Value_Clear = 0x1};
					enum {ValueMask_Clear = 0x00800000};
					enum {Value_Pending = 0x1};
					enum {ValueMask_Pending = 0x00800000};
					enum {Value_NotPending = 0x0};
					enum {ValueMask_NotPending = 0x00000000};
					};
				namespace EBERR { // Field Description
					enum {Lsb = 22};
					enum {FieldMask = 0x00400000};
					enum {Value_Clear = 0x1};
					enum {ValueMask_Clear = 0x00400000};
					enum {Value_Pending = 0x1};
					enum {ValueMask_Pending = 0x00400000};
					enum {Value_NotPending = 0x0};
					enum {ValueMask_NotPending = 0x00000000};
					};
				};
			namespace IMASK { // Register description
				typedef uint32_t	Reg;
				namespace HBERR { // Field Description
					enum {Lsb = 31};
					enum {FieldMask = 0x80000000};
					enum {Value_Disable = 0x0};
					enum {ValueMask_Disable = 0x00000000};
					enum {Value_Enable = 0x1};
					enum {ValueMask_Enable = 0x80000000};
					};
				namespace BABR { // Field Description
					enum {Lsb = 30};
					enum {FieldMask = 0x40000000};
					enum {Value_Disable = 0x0};
					enum {ValueMask_Disable = 0x00000000};
					enum {Value_Enable = 0x1};
					enum {ValueMask_Enable = 0x40000000};
					};
				namespace BABT { // Field Description
					enum {Lsb = 29};
					enum {FieldMask = 0x20000000};
					enum {Value_Disable = 0x0};
					enum {ValueMask_Disable = 0x00000000};
					enum {Value_Enable = 0x1};
					enum {ValueMask_Enable = 0x20000000};
					};
				namespace GRA { // Field Description
					enum {Lsb = 28};
					enum {FieldMask = 0x10000000};
					enum {Value_Disable = 0x0};
					enum {ValueMask_Disable = 0x00000000};
					enum {Value_Enable = 0x1};
					enum {ValueMask_Enable = 0x10000000};
					};
				namespace TFINT { // Field Description
					enum {Lsb = 27};
					enum {FieldMask = 0x08000000};
					enum {Value_Disable = 0x0};
					enum {ValueMask_Disable = 0x00000000};
					enum {Value_Enable = 0x1};
					enum {ValueMask_Enable = 0x08000000};
					};
				namespace TXB { // Field Description
					enum {Lsb = 26};
					enum {FieldMask = 0x04000000};
					enum {Value_Disable = 0x0};
					enum {ValueMask_Disable = 0x00000000};
					enum {Value_Enable = 0x1};
					enum {ValueMask_Enable = 0x04000000};
					};
				namespace RFINT { // Field Description
					enum {Lsb = 25};
					enum {FieldMask = 0x02000000};
					enum {Value_Disable = 0x0};
					enum {ValueMask_Disable = 0x00000000};
					enum {Value_Enable = 0x1};
					enum {ValueMask_Enable = 0x02000000};
					};
				namespace RXB { // Field Description
					enum {Lsb = 24};
					enum {FieldMask = 0x01000000};
					enum {Value_Disable = 0x0};
					enum {ValueMask_Disable = 0x00000000};
					enum {Value_Enable = 0x1};
					enum {ValueMask_Enable = 0x01000000};
					};
				namespace MII { // Field Description
					enum {Lsb = 23};
					enum {FieldMask = 0x00800000};
					enum {Value_Disable = 0x0};
					enum {ValueMask_Disable = 0x00000000};
					enum {Value_Enable = 0x1};
					enum {ValueMask_Enable = 0x00800000};
					};
				namespace EBERR { // Field Description
					enum {Lsb = 22};
					enum {FieldMask = 0x00400000};
					enum {Value_Disable = 0x0};
					enum {ValueMask_Disable = 0x00000000};
					enum {Value_Enable = 0x1};
					enum {ValueMask_Enable = 0x00400000};
					};
				};
			namespace IVEC { // Register description
				typedef uint32_t	Reg;
				namespace ILEVEL { // Field Description
					enum {Lsb = 29};
					enum {FieldMask = 0xE0000000};
					};
				namespace IVEC { // Field Description
					enum {Lsb = 2};
					enum {FieldMask = 0x0000000C};
					enum {Value_None = 0x0};
					enum {ValueMask_None = 0x00000000};
					enum {Value_NonTimeCritical = 0x1};
					enum {ValueMask_NonTimeCritical = 0x00000004};
					enum {Value_Transmit = 0x2};
					enum {ValueMask_Transmit = 0x00000008};
					enum {Value_Receive = 0x3};
					enum {ValueMask_Receive = 0x0000000C};
					};
				};
			namespace R_DES_ACTIVE { // Register description
				typedef uint32_t	Reg;
				namespace R_DES_ACTIVE { // Field Description
					enum {Lsb = 24};
					enum {FieldMask = 0x01000000};
					enum {Value_Activate = 0x1};
					enum {ValueMask_Activate = 0x01000000};
					enum {Value_Active = 0x1};
					enum {ValueMask_Active = 0x01000000};
					enum {Value_NotActive = 0x0};
					enum {ValueMask_NotActive = 0x00000000};
					};
				};
			namespace X_DES_ACTIVE { // Register description
				typedef uint32_t	Reg;
				namespace X_DES_ACTIVE { // Field Description
					enum {Lsb = 24};
					enum {FieldMask = 0x01000000};
					enum {Value_Activate = 0x1};
					enum {ValueMask_Activate = 0x01000000};
					enum {Value_Active = 0x1};
					enum {ValueMask_Active = 0x01000000};
					enum {Value_NotActive = 0x0};
					enum {ValueMask_NotActive = 0x00000000};
					};
				};
			namespace MII_DATA { // Register description
				typedef uint32_t	Reg;
				namespace ST { // Field Description
					enum {Lsb = 30};
					enum {FieldMask = 0xC0000000};
					enum {Value_Valid = 0x1};
					enum {ValueMask_Valid = 0x40000000};
					};
				namespace OP { // Field Description
					enum {Lsb = 28};
					enum {FieldMask = 0x30000000};
					enum {Value_Read = 0x2};
					enum {ValueMask_Read = 0x20000000};
					enum {Value_Write = 0x1};
					enum {ValueMask_Write = 0x10000000};
					};
				namespace PA { // Field Description
					enum {Lsb = 23};
					enum {FieldMask = 0x0F800000};
					enum {Value_Min = 0x0};
					enum {ValueMask_Min = 0x00000000};
					enum {Value_Max = 0x1F};
					enum {ValueMask_Max = 0x0F800000};
					};
				namespace RA { // Field Description
					enum {Lsb = 18};
					enum {FieldMask = 0x007C0000};
					enum {Value_Min = 0x0};
					enum {ValueMask_Min = 0x00000000};
					enum {Value_Max = 0x1F};
					enum {ValueMask_Max = 0x007C0000};
					};
				namespace TA { // Field Description
					enum {Lsb = 16};
					enum {FieldMask = 0x00030000};
					enum {Value_Valid = 0x2};
					enum {ValueMask_Valid = 0x00020000};
					};
				namespace DATA { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x0000FFFF};
					};
				};
			namespace MII_SPEED { // Register description
				typedef uint32_t	Reg;
				namespace DIS_PREAMBLE { // Field Description
					enum {Lsb = 7};
					enum {FieldMask = 0x00000080};
					enum {Value_PreamblePrependToMiiFrame = 0x0};
					enum {ValueMask_PreamblePrependToMiiFrame = 0x00000000};
					enum {Value_NoPreamblePrependToMiiFrame = 0x1};
					enum {ValueMask_NoPreamblePrependToMiiFrame = 0x00000080};
					};
				namespace MII_SPEED { // Field Description
					enum {Lsb = 1};
					enum {FieldMask = 0x0000007E};
					enum {Value_Off = 0x0};
					enum {ValueMask_Off = 0x00000000};
					enum {Value_Min = 0x1};
					enum {ValueMask_Min = 0x00000002};
					enum {Value_Max = 0x3F};
					enum {ValueMask_Max = 0x0000007E};
					};
				};
			namespace R_BOUND { // Register description
				typedef uint32_t	Reg;
				namespace R_BOUND { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x000003FF};
					enum {Value_Align = 0x4};
					enum {ValueMask_Align = 0x00000004};
					};
				};
			namespace R_FSTART { // Register description
				typedef uint32_t	Reg;
				namespace R_FSTART { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x000003FF};
					enum {Value_Align = 0x4};
					enum {ValueMask_Align = 0x00000004};
					};
				};
			namespace X_WMRK { // Register description
				typedef uint32_t	Reg;
				namespace X_WMRK { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x00000003};
					enum {Value_Size64Bytes = 0x0};
					enum {ValueMask_Size64Bytes = 0x00000000};
					enum {Value_Size128Bytes = 0x2};
					enum {ValueMask_Size128Bytes = 0x00000002};
					enum {Value_Size192Bytes = 0x3};
					enum {ValueMask_Size192Bytes = 0x00000003};
					};
				};
			namespace X_FSTART { // Register description
				typedef uint32_t	Reg;
				namespace X_FSTART { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x000003FF};
					enum {Value_Align = 0x4};
					enum {ValueMask_Align = 0x00000004};
					};
				};
			namespace FUN_CODE { // Register description
				typedef uint32_t	Reg;
				namespace DATA_BO { // Field Description
					enum {Lsb = 29};
					enum {FieldMask = 0x60000000};
					enum {Value_PpcLittleEndian = 0x1};
					enum {ValueMask_PpcLittleEndian = 0x20000000};
					enum {Value_BigEndian = 0x2};
					enum {ValueMask_BigEndian = 0x40000000};
					enum {Value_TrueLittleEndian = 0x2};
					enum {ValueMask_TrueLittleEndian = 0x40000000};
					};
				namespace DESC_BO { // Field Description
					enum {Lsb = 27};
					enum {FieldMask = 0x18000000};
					enum {Value_PpcLittleEndian = 0x1};
					enum {ValueMask_PpcLittleEndian = 0x08000000};
					enum {Value_BigEndian = 0x2};
					enum {ValueMask_BigEndian = 0x10000000};
					enum {Value_TrueLittleEndian = 0x2};
					enum {ValueMask_TrueLittleEndian = 0x10000000};
					};
				namespace FC { // Field Description
					enum {Lsb = 24};
					enum {FieldMask = 0x07000000};
					enum {Value_Min = 0x0};
					enum {ValueMask_Min = 0x00000000};
					enum {Value_Max = 0x7};
					enum {ValueMask_Max = 0x07000000};
					};
				};
			namespace R_CNTRL { // Register description
				typedef uint32_t	Reg;
				namespace BC_REJ { // Field Description
					enum {Lsb = 4};
					enum {FieldMask = 0x00000010};
					enum {Value_AcceptBroadcast = 0x0};
					enum {ValueMask_AcceptBroadcast = 0x00000000};
					enum {Value_RejectBroadcast = 0x1};
					enum {ValueMask_RejectBroadcast = 0x00000010};
					};
				namespace PROM { // Field Description
					enum {Lsb = 3};
					enum {FieldMask = 0x00000008};
					enum {Value_Disable = 0x0};
					enum {ValueMask_Disable = 0x00000000};
					enum {Value_Enable = 0x1};
					enum {ValueMask_Enable = 0x00000008};
					};
				namespace MII_MODE { // Field Description
					enum {Lsb = 2};
					enum {FieldMask = 0x00000004};
					enum {Value_SevenWireMode = 0x0};
					enum {ValueMask_SevenWireMode = 0x00000000};
					enum {Value_MiiMode = 0x1};
					enum {ValueMask_MiiMode = 0x00000004};
					};
				namespace DRT { // Field Description
					enum {Lsb = 1};
					enum {FieldMask = 0x00000002};
					enum {Value_IndependentRxTx = 0x0};
					enum {ValueMask_IndependentRxTx = 0x00000000};
					enum {Value_DisableRxOnTx = 0x1};
					enum {ValueMask_DisableRxOnTx = 0x00000002};
					};
				namespace LOOP { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x00000001};
					enum {Value_Disable = 0x0};
					enum {ValueMask_Disable = 0x00000000};
					enum {Value_Enable = 0x1};
					enum {ValueMask_Enable = 0x00000001};
					};
				};
			namespace R_HASH { // Register description
				typedef uint32_t	Reg;
				namespace MAX_FRAME_LENGTH { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x000007FF};
					enum {Value_Default = 0x5EE};
					enum {ValueMask_Default = 0x000005EE};
					enum {Value_VlanTags = 0x5F2};
					enum {ValueMask_VlanTags = 0x000005F2};
					enum {Value_Min = 0x0};
					enum {ValueMask_Min = 0x00000000};
					enum {Value_Max = 0x7FF};
					enum {ValueMask_Max = 0x000007FF};
					};
				};
			namespace X_CNTRL { // Register description
				typedef uint32_t	Reg;
				namespace FDEN { // Field Description
					enum {Lsb = 2};
					enum {FieldMask = 0x00000004};
					enum {Value_Disable = 0x0};
					enum {ValueMask_Disable = 0x00000000};
					enum {Value_Enable = 0x1};
					enum {ValueMask_Enable = 0x00000004};
					};
				namespace HBC { // Field Description
					enum {Lsb = 1};
					enum {FieldMask = 0x00000002};
					enum {Value_Disable = 0x0};
					enum {ValueMask_Disable = 0x00000000};
					enum {Value_Enable = 0x1};
					enum {ValueMask_Enable = 0x00000002};
					};
				namespace GTS { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x00000001};
					enum {Value_Transmit = 0x0};
					enum {ValueMask_Transmit = 0x00000000};
					enum {Value_BeginStop = 0x1};
					enum {ValueMask_BeginStop = 0x00000001};
					};
				};
			};
		};
	};
#endif
