/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _hw_mot_mpc8xx_cpmh_
#define _hw_mot_mpc8xx_cpmh_
#include "cpmreg.h"

/** */
namespace Oscl {
/** */
namespace Mot8xx {
/** */
namespace Cpm {

/** */
typedef struct Map {
	volatile CPCR::Reg		cpcr;
	uint8_t		reserved1[0x9C4-0x9C2];
	RCCR::Reg		rccr;
	uint8_t		reserved2[0x9C7-0x9C6];
	RMDS::Reg		rmds;
	uint8_t		reserved3[0x9CC-0x9C8];
	RCTR1::Reg		rctr1;
	RCTR2::Reg		rctr2;
	RCTR3::Reg		rctr3;
	RCTR4::Reg		rctr4;
	uint8_t		reserved4[0x9D6-0x9D4];
	RTER::Reg		rter;
	uint8_t		reserved5[0x9DA-0x9D8];
	RTMR::Reg		rtmr;
	}Map;

};
};
};

#endif
