/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _hw_mot8xx_cpmicregh_
#define _hw_mot8xx_cpmicregh_
#include <stdint.h>
namespace Oscl { // Namespace description
	namespace Mot8xx { // Namespace description
		namespace Cpmic { // Namespace description
			namespace CIVR { // Register description
				typedef uint16_t	Reg;
				namespace IACK { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x00000001};
					enum {Value_ACK = 0x1};
					enum {ValueMask_ACK = 0x00000001};
					};
				namespace VN { // Field Description
					enum {Lsb = 11};
					enum {FieldMask = 0x0000F800};
					};
				};
			namespace CICR { // Register description
				typedef uint32_t	Reg;
				namespace SCP { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x00000003};
					enum {Value_USB = 0x0};
					enum {ValueMask_USB = 0x00000000};
					enum {Value_SCC1 = 0x0};
					enum {ValueMask_SCC1 = 0x00000000};
					enum {Value_SCC2 = 0x1};
					enum {ValueMask_SCC2 = 0x00000001};
					enum {Value_SCC3 = 0x2};
					enum {ValueMask_SCC3 = 0x00000002};
					enum {Value_SCC4 = 0x3};
					enum {ValueMask_SCC4 = 0x00000003};
					};
				namespace SCdP { // Field Description
					enum {Lsb = 22};
					enum {FieldMask = 0x00C00000};
					enum {Value_USB = 0x0};
					enum {ValueMask_USB = 0x00000000};
					enum {Value_SCC1 = 0x0};
					enum {ValueMask_SCC1 = 0x00000000};
					enum {Value_SCC2 = 0x1};
					enum {ValueMask_SCC2 = 0x00400000};
					enum {Value_SCC3 = 0x2};
					enum {ValueMask_SCC3 = 0x00800000};
					enum {Value_SCC4 = 0x3};
					enum {ValueMask_SCC4 = 0x00C00000};
					};
				namespace SCcP { // Field Description
					enum {Lsb = 20};
					enum {FieldMask = 0x00300000};
					enum {Value_USB = 0x0};
					enum {ValueMask_USB = 0x00000000};
					enum {Value_SCC1 = 0x0};
					enum {ValueMask_SCC1 = 0x00000000};
					enum {Value_SCC2 = 0x1};
					enum {ValueMask_SCC2 = 0x00100000};
					enum {Value_SCC3 = 0x2};
					enum {ValueMask_SCC3 = 0x00200000};
					enum {Value_SCC4 = 0x3};
					enum {ValueMask_SCC4 = 0x00300000};
					};
				namespace SCbP { // Field Description
					enum {Lsb = 18};
					enum {FieldMask = 0x000C0000};
					enum {Value_USB = 0x0};
					enum {ValueMask_USB = 0x00000000};
					enum {Value_SCC1 = 0x0};
					enum {ValueMask_SCC1 = 0x00000000};
					enum {Value_SCC2 = 0x1};
					enum {ValueMask_SCC2 = 0x00040000};
					enum {Value_SCC3 = 0x2};
					enum {ValueMask_SCC3 = 0x00080000};
					enum {Value_SCC4 = 0x3};
					enum {ValueMask_SCC4 = 0x000C0000};
					};
				namespace SCaP { // Field Description
					enum {Lsb = 16};
					enum {FieldMask = 0x00030000};
					enum {Value_USB = 0x0};
					enum {ValueMask_USB = 0x00000000};
					enum {Value_SCC1 = 0x0};
					enum {ValueMask_SCC1 = 0x00000000};
					enum {Value_SCC2 = 0x1};
					enum {ValueMask_SCC2 = 0x00010000};
					enum {Value_SCC3 = 0x2};
					enum {ValueMask_SCC3 = 0x00020000};
					enum {Value_SCC4 = 0x3};
					enum {ValueMask_SCC4 = 0x00030000};
					};
				namespace IRL { // Field Description
					enum {Lsb = 13};
					enum {FieldMask = 0x0000E000};
					enum {Value_Max = 0x7};
					enum {ValueMask_Max = 0x0000E000};
					enum {Value_Typical = 0x4};
					enum {ValueMask_Typical = 0x00008000};
					};
				namespace HP { // Field Description
					enum {Lsb = 8};
					enum {FieldMask = 0x00001F00};
					enum {Value_Max = 0x1F};
					enum {ValueMask_Max = 0x00001F00};
					enum {Value_PC15 = 0x1F};
					enum {ValueMask_PC15 = 0x00001F00};
					enum {Value_SCC1 = 0x1E};
					enum {ValueMask_SCC1 = 0x00001E00};
					enum {Value_USB = 0x1E};
					enum {ValueMask_USB = 0x00001E00};
					enum {Value_SCC2 = 0x1D};
					enum {ValueMask_SCC2 = 0x00001D00};
					enum {Value_SCC3 = 0x1C};
					enum {ValueMask_SCC3 = 0x00001C00};
					enum {Value_SCC4 = 0x1B};
					enum {ValueMask_SCC4 = 0x00001B00};
					enum {Value_PC14 = 0x1A};
					enum {ValueMask_PC14 = 0x00001A00};
					enum {Value_Timer1 = 0x19};
					enum {ValueMask_Timer1 = 0x00001900};
					enum {Value_PC13 = 0x18};
					enum {ValueMask_PC13 = 0x00001800};
					enum {Value_PC12 = 0x17};
					enum {ValueMask_PC12 = 0x00001700};
					enum {Value_SdmaErr = 0x16};
					enum {ValueMask_SdmaErr = 0x00001600};
					enum {Value_IDMA1 = 0x15};
					enum {ValueMask_IDMA1 = 0x00001500};
					enum {Value_IDMA2 = 0x14};
					enum {ValueMask_IDMA2 = 0x00001400};
					enum {Value_Timer2 = 0x12};
					enum {ValueMask_Timer2 = 0x00001200};
					enum {Value_RiscTT = 0x11};
					enum {ValueMask_RiscTT = 0x00001100};
					enum {Value_I2C = 0x10};
					enum {ValueMask_I2C = 0x00001000};
					enum {Value_PC11 = 0xF};
					enum {ValueMask_PC11 = 0x00000F00};
					enum {Value_PC10 = 0xE};
					enum {ValueMask_PC10 = 0x00000E00};
					enum {Value_Timer3 = 0xC};
					enum {ValueMask_Timer3 = 0x00000C00};
					enum {Value_PC9 = 0xB};
					enum {ValueMask_PC9 = 0x00000B00};
					enum {Value_PC8 = 0xA};
					enum {ValueMask_PC8 = 0x00000A00};
					enum {Value_PC7 = 0x9};
					enum {ValueMask_PC7 = 0x00000900};
					enum {Value_Timer4 = 0x7};
					enum {ValueMask_Timer4 = 0x00000700};
					enum {Value_PC6 = 0x6};
					enum {ValueMask_PC6 = 0x00000600};
					enum {Value_SPI = 0x5};
					enum {ValueMask_SPI = 0x00000500};
					enum {Value_SMC1 = 0x4};
					enum {ValueMask_SMC1 = 0x00000400};
					enum {Value_SMC2 = 0x3};
					enum {ValueMask_SMC2 = 0x00000300};
					enum {Value_PC5 = 0x2};
					enum {ValueMask_PC5 = 0x00000200};
					enum {Value_PC4 = 0x1};
					enum {ValueMask_PC4 = 0x00000100};
					enum {Value_Error = 0x0};
					enum {ValueMask_Error = 0x00000000};
					};
				namespace IEN { // Field Description
					enum {Lsb = 7};
					enum {FieldMask = 0x00000080};
					enum {Value_Enable = 0x1};
					enum {ValueMask_Enable = 0x00000080};
					enum {Value_Disable = 0x0};
					enum {ValueMask_Disable = 0x00000000};
					};
				namespace SPS { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x00000001};
					enum {Value_Grouped = 0x0};
					enum {ValueMask_Grouped = 0x00000000};
					enum {Value_Spread = 0x1};
					enum {ValueMask_Spread = 0x00000001};
					};
				};
			namespace CIPR { // Register description
				typedef uint32_t	Reg;
				namespace PC15 { // Field Description
					enum {Lsb = 31};
					enum {FieldMask = 0x80000000};
					enum {Value_Pending = 0x1};
					enum {ValueMask_Pending = 0x80000000};
					};
				namespace SCC1 { // Field Description
					enum {Lsb = 30};
					enum {FieldMask = 0x40000000};
					enum {Value_Pending = 0x1};
					enum {ValueMask_Pending = 0x40000000};
					};
				namespace SCC2 { // Field Description
					enum {Lsb = 29};
					enum {FieldMask = 0x20000000};
					enum {Value_Pending = 0x1};
					enum {ValueMask_Pending = 0x20000000};
					};
				namespace SCC3 { // Field Description
					enum {Lsb = 28};
					enum {FieldMask = 0x10000000};
					enum {Value_Pending = 0x1};
					enum {ValueMask_Pending = 0x10000000};
					};
				namespace SCC4 { // Field Description
					enum {Lsb = 27};
					enum {FieldMask = 0x08000000};
					enum {Value_Pending = 0x1};
					enum {ValueMask_Pending = 0x08000000};
					};
				namespace PC14 { // Field Description
					enum {Lsb = 26};
					enum {FieldMask = 0x04000000};
					enum {Value_Pending = 0x1};
					enum {ValueMask_Pending = 0x04000000};
					};
				namespace TIMER1 { // Field Description
					enum {Lsb = 25};
					enum {FieldMask = 0x02000000};
					enum {Value_Pending = 0x1};
					enum {ValueMask_Pending = 0x02000000};
					};
				namespace PC13 { // Field Description
					enum {Lsb = 24};
					enum {FieldMask = 0x01000000};
					enum {Value_Pending = 0x1};
					enum {ValueMask_Pending = 0x01000000};
					};
				namespace PC12 { // Field Description
					enum {Lsb = 23};
					enum {FieldMask = 0x00800000};
					enum {Value_Pending = 0x1};
					enum {ValueMask_Pending = 0x00800000};
					};
				namespace SDMA { // Field Description
					enum {Lsb = 22};
					enum {FieldMask = 0x00400000};
					enum {Value_Pending = 0x1};
					enum {ValueMask_Pending = 0x00400000};
					};
				namespace IDMA1 { // Field Description
					enum {Lsb = 21};
					enum {FieldMask = 0x00200000};
					enum {Value_Pending = 0x1};
					enum {ValueMask_Pending = 0x00200000};
					};
				namespace IDMA2 { // Field Description
					enum {Lsb = 20};
					enum {FieldMask = 0x00100000};
					enum {Value_Pending = 0x1};
					enum {ValueMask_Pending = 0x00100000};
					};
				namespace TIMER2 { // Field Description
					enum {Lsb = 18};
					enum {FieldMask = 0x00040000};
					enum {Value_Pending = 0x1};
					enum {ValueMask_Pending = 0x00040000};
					};
				namespace RTT { // Field Description
					enum {Lsb = 17};
					enum {FieldMask = 0x00020000};
					enum {Value_Pending = 0x1};
					enum {ValueMask_Pending = 0x00020000};
					};
				namespace I2C { // Field Description
					enum {Lsb = 16};
					enum {FieldMask = 0x00010000};
					enum {Value_Pending = 0x1};
					enum {ValueMask_Pending = 0x00010000};
					};
				namespace PC11 { // Field Description
					enum {Lsb = 15};
					enum {FieldMask = 0x00008000};
					enum {Value_Pending = 0x1};
					enum {ValueMask_Pending = 0x00008000};
					};
				namespace PC10 { // Field Description
					enum {Lsb = 14};
					enum {FieldMask = 0x00004000};
					enum {Value_Pending = 0x1};
					enum {ValueMask_Pending = 0x00004000};
					};
				namespace TIMER3 { // Field Description
					enum {Lsb = 12};
					enum {FieldMask = 0x00001000};
					enum {Value_Pending = 0x1};
					enum {ValueMask_Pending = 0x00001000};
					};
				namespace PC9 { // Field Description
					enum {Lsb = 11};
					enum {FieldMask = 0x00000800};
					enum {Value_Pending = 0x1};
					enum {ValueMask_Pending = 0x00000800};
					};
				namespace PC8 { // Field Description
					enum {Lsb = 10};
					enum {FieldMask = 0x00000400};
					enum {Value_Pending = 0x1};
					enum {ValueMask_Pending = 0x00000400};
					};
				namespace PC7 { // Field Description
					enum {Lsb = 9};
					enum {FieldMask = 0x00000200};
					enum {Value_Pending = 0x1};
					enum {ValueMask_Pending = 0x00000200};
					};
				namespace TIMER4 { // Field Description
					enum {Lsb = 7};
					enum {FieldMask = 0x00000080};
					enum {Value_Pending = 0x1};
					enum {ValueMask_Pending = 0x00000080};
					};
				namespace PC6 { // Field Description
					enum {Lsb = 6};
					enum {FieldMask = 0x00000040};
					enum {Value_Pending = 0x1};
					enum {ValueMask_Pending = 0x00000040};
					};
				namespace SPI { // Field Description
					enum {Lsb = 5};
					enum {FieldMask = 0x00000020};
					enum {Value_Pending = 0x1};
					enum {ValueMask_Pending = 0x00000020};
					};
				namespace SMC1 { // Field Description
					enum {Lsb = 4};
					enum {FieldMask = 0x00000010};
					enum {Value_Pending = 0x1};
					enum {ValueMask_Pending = 0x00000010};
					};
				namespace SMC2 { // Field Description
					enum {Lsb = 3};
					enum {FieldMask = 0x00000008};
					enum {Value_Pending = 0x1};
					enum {ValueMask_Pending = 0x00000008};
					};
				namespace PIP { // Field Description
					enum {Lsb = 3};
					enum {FieldMask = 0x00000008};
					enum {Value_Pending = 0x1};
					enum {ValueMask_Pending = 0x00000008};
					};
				namespace PC5 { // Field Description
					enum {Lsb = 2};
					enum {FieldMask = 0x00000004};
					enum {Value_Pending = 0x1};
					enum {ValueMask_Pending = 0x00000004};
					};
				namespace PC4 { // Field Description
					enum {Lsb = 1};
					enum {FieldMask = 0x00000002};
					enum {Value_Pending = 0x1};
					enum {ValueMask_Pending = 0x00000002};
					};
				};
			namespace CIMR { // Register description
				typedef uint32_t	Reg;
				namespace PC15 { // Field Description
					enum {Lsb = 31};
					enum {FieldMask = 0x80000000};
					enum {Value_Disable = 0x0};
					enum {ValueMask_Disable = 0x00000000};
					enum {Value_Enable = 0x1};
					enum {ValueMask_Enable = 0x80000000};
					};
				namespace SCC1 { // Field Description
					enum {Lsb = 30};
					enum {FieldMask = 0x40000000};
					enum {Value_Disable = 0x0};
					enum {ValueMask_Disable = 0x00000000};
					enum {Value_Enable = 0x1};
					enum {ValueMask_Enable = 0x40000000};
					};
				namespace SCC2 { // Field Description
					enum {Lsb = 29};
					enum {FieldMask = 0x20000000};
					enum {Value_Disable = 0x0};
					enum {ValueMask_Disable = 0x00000000};
					enum {Value_Enable = 0x1};
					enum {ValueMask_Enable = 0x20000000};
					};
				namespace SCC3 { // Field Description
					enum {Lsb = 28};
					enum {FieldMask = 0x10000000};
					enum {Value_Disable = 0x0};
					enum {ValueMask_Disable = 0x00000000};
					enum {Value_Enable = 0x1};
					enum {ValueMask_Enable = 0x10000000};
					};
				namespace SCC4 { // Field Description
					enum {Lsb = 27};
					enum {FieldMask = 0x08000000};
					enum {Value_Disable = 0x0};
					enum {ValueMask_Disable = 0x00000000};
					enum {Value_Enable = 0x1};
					enum {ValueMask_Enable = 0x08000000};
					};
				namespace PC14 { // Field Description
					enum {Lsb = 26};
					enum {FieldMask = 0x04000000};
					enum {Value_Disable = 0x0};
					enum {ValueMask_Disable = 0x00000000};
					enum {Value_Enable = 0x1};
					enum {ValueMask_Enable = 0x04000000};
					};
				namespace TIMER1 { // Field Description
					enum {Lsb = 25};
					enum {FieldMask = 0x02000000};
					enum {Value_Disable = 0x0};
					enum {ValueMask_Disable = 0x00000000};
					enum {Value_Enable = 0x1};
					enum {ValueMask_Enable = 0x02000000};
					};
				namespace PC13 { // Field Description
					enum {Lsb = 24};
					enum {FieldMask = 0x01000000};
					enum {Value_Disable = 0x0};
					enum {ValueMask_Disable = 0x00000000};
					enum {Value_Enable = 0x1};
					enum {ValueMask_Enable = 0x01000000};
					};
				namespace PC12 { // Field Description
					enum {Lsb = 23};
					enum {FieldMask = 0x00800000};
					enum {Value_Disable = 0x0};
					enum {ValueMask_Disable = 0x00000000};
					enum {Value_Enable = 0x1};
					enum {ValueMask_Enable = 0x00800000};
					};
				namespace SDMA { // Field Description
					enum {Lsb = 22};
					enum {FieldMask = 0x00400000};
					enum {Value_Disable = 0x0};
					enum {ValueMask_Disable = 0x00000000};
					enum {Value_Enable = 0x1};
					enum {ValueMask_Enable = 0x00400000};
					};
				namespace IDMA1 { // Field Description
					enum {Lsb = 21};
					enum {FieldMask = 0x00200000};
					enum {Value_Disable = 0x0};
					enum {ValueMask_Disable = 0x00000000};
					enum {Value_Enable = 0x1};
					enum {ValueMask_Enable = 0x00200000};
					};
				namespace IDMA2 { // Field Description
					enum {Lsb = 20};
					enum {FieldMask = 0x00100000};
					enum {Value_Disable = 0x0};
					enum {ValueMask_Disable = 0x00000000};
					enum {Value_Enable = 0x1};
					enum {ValueMask_Enable = 0x00100000};
					};
				namespace TIMER2 { // Field Description
					enum {Lsb = 18};
					enum {FieldMask = 0x00040000};
					enum {Value_Disable = 0x0};
					enum {ValueMask_Disable = 0x00000000};
					enum {Value_Enable = 0x1};
					enum {ValueMask_Enable = 0x00040000};
					};
				namespace RTT { // Field Description
					enum {Lsb = 17};
					enum {FieldMask = 0x00020000};
					enum {Value_Disable = 0x0};
					enum {ValueMask_Disable = 0x00000000};
					enum {Value_Enable = 0x1};
					enum {ValueMask_Enable = 0x00020000};
					};
				namespace I2C { // Field Description
					enum {Lsb = 16};
					enum {FieldMask = 0x00010000};
					enum {Value_Disable = 0x0};
					enum {ValueMask_Disable = 0x00000000};
					enum {Value_Enable = 0x1};
					enum {ValueMask_Enable = 0x00010000};
					};
				namespace PC11 { // Field Description
					enum {Lsb = 15};
					enum {FieldMask = 0x00008000};
					enum {Value_Disable = 0x0};
					enum {ValueMask_Disable = 0x00000000};
					enum {Value_Enable = 0x1};
					enum {ValueMask_Enable = 0x00008000};
					};
				namespace PC10 { // Field Description
					enum {Lsb = 14};
					enum {FieldMask = 0x00004000};
					enum {Value_Disable = 0x0};
					enum {ValueMask_Disable = 0x00000000};
					enum {Value_Enable = 0x1};
					enum {ValueMask_Enable = 0x00004000};
					};
				namespace TIMER3 { // Field Description
					enum {Lsb = 12};
					enum {FieldMask = 0x00001000};
					enum {Value_Disable = 0x0};
					enum {ValueMask_Disable = 0x00000000};
					enum {Value_Enable = 0x1};
					enum {ValueMask_Enable = 0x00001000};
					};
				namespace PC9 { // Field Description
					enum {Lsb = 11};
					enum {FieldMask = 0x00000800};
					enum {Value_Disable = 0x0};
					enum {ValueMask_Disable = 0x00000000};
					enum {Value_Enable = 0x1};
					enum {ValueMask_Enable = 0x00000800};
					};
				namespace PC8 { // Field Description
					enum {Lsb = 10};
					enum {FieldMask = 0x00000400};
					enum {Value_Disable = 0x0};
					enum {ValueMask_Disable = 0x00000000};
					enum {Value_Enable = 0x1};
					enum {ValueMask_Enable = 0x00000400};
					};
				namespace PC7 { // Field Description
					enum {Lsb = 9};
					enum {FieldMask = 0x00000200};
					enum {Value_Disable = 0x0};
					enum {ValueMask_Disable = 0x00000000};
					enum {Value_Enable = 0x1};
					enum {ValueMask_Enable = 0x00000200};
					};
				namespace TIMER4 { // Field Description
					enum {Lsb = 7};
					enum {FieldMask = 0x00000080};
					enum {Value_Disable = 0x0};
					enum {ValueMask_Disable = 0x00000000};
					enum {Value_Enable = 0x1};
					enum {ValueMask_Enable = 0x00000080};
					};
				namespace PC6 { // Field Description
					enum {Lsb = 6};
					enum {FieldMask = 0x00000040};
					enum {Value_Disable = 0x0};
					enum {ValueMask_Disable = 0x00000000};
					enum {Value_Enable = 0x1};
					enum {ValueMask_Enable = 0x00000040};
					};
				namespace SPI { // Field Description
					enum {Lsb = 5};
					enum {FieldMask = 0x00000020};
					enum {Value_Disable = 0x0};
					enum {ValueMask_Disable = 0x00000000};
					enum {Value_Enable = 0x1};
					enum {ValueMask_Enable = 0x00000020};
					};
				namespace SMC1 { // Field Description
					enum {Lsb = 4};
					enum {FieldMask = 0x00000010};
					enum {Value_Disable = 0x0};
					enum {ValueMask_Disable = 0x00000000};
					enum {Value_Enable = 0x1};
					enum {ValueMask_Enable = 0x00000010};
					};
				namespace SMC2 { // Field Description
					enum {Lsb = 3};
					enum {FieldMask = 0x00000008};
					enum {Value_Disable = 0x0};
					enum {ValueMask_Disable = 0x00000000};
					enum {Value_Enable = 0x1};
					enum {ValueMask_Enable = 0x00000008};
					};
				namespace PIP { // Field Description
					enum {Lsb = 3};
					enum {FieldMask = 0x00000008};
					enum {Value_Disable = 0x0};
					enum {ValueMask_Disable = 0x00000000};
					enum {Value_Enable = 0x1};
					enum {ValueMask_Enable = 0x00000008};
					};
				namespace PC5 { // Field Description
					enum {Lsb = 2};
					enum {FieldMask = 0x00000004};
					enum {Value_Disable = 0x0};
					enum {ValueMask_Disable = 0x00000000};
					enum {Value_Enable = 0x1};
					enum {ValueMask_Enable = 0x00000004};
					};
				namespace PC4 { // Field Description
					enum {Lsb = 1};
					enum {FieldMask = 0x00000002};
					enum {Value_Disable = 0x0};
					enum {ValueMask_Disable = 0x00000000};
					enum {Value_Enable = 0x1};
					enum {ValueMask_Enable = 0x00000002};
					};
				};
			namespace CISR { // Register description
				typedef uint32_t	Reg;
				};
			};
		};
	};
#endif
