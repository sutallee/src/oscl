/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _hw_mot_mpc8xx_fech_
#define _hw_mot_mpc8xx_fech_
#include "fecreg.h"

/** */
namespace Oscl {
/** */
namespace Mot8xx {
/** */
namespace Fec {

/** */
typedef struct Device {
	ADDR_LOW::Reg			addr_low;			// 0xE00
	ADDR_HIGH::Reg			addr_high;			// 0xE04
	HASH_TABLE_HIGH::Reg	hash_table_high;	// 0xE08
	HASH_TABLE_LOW::Reg		hash_table_low;		// 0xE0C
	R_DES_START::Reg		r_des_start;		// 0xE10
	X_DES_START::Reg		x_des_start;		// 0xE14
	R_BUFF_SIZE::Reg		r_buff_size;		// 0xE18
	uint8_t				reserved00[0xE40-0xE1C];
	ECNTRL::Reg				ecntrl;				// 0xE40
	IEVENT::Reg				ievent;				// 0xE44
	IMASK::Reg				imask;				// 0xE48
	IVEC::Reg				ivec;				// 0xE4C
	R_DES_ACTIVE::Reg		r_des_active;		// 0xE50
	X_DES_ACTIVE::Reg		x_des_active;		// 0xE54
	uint8_t				reserved01[0xE80-0xE58];
	MII_DATA::Reg			mii_data;			// 0xE80
	MII_SPEED::Reg			mii_speed;			// 0xE84
	uint8_t				reserved02[0xECC-0xE88];
	R_BOUND::Reg			r_bound;			// 0xECC
	R_FSTART::Reg			r_fstart;			// 0xED0
	uint8_t				reserved03[0xEE4-0xED4];
	X_WMRK::Reg				x_wmrk;				// 0xEE4
	uint8_t				reserved04[0xEEC-0xEE8];
	X_FSTART::Reg			x_fstart;			// 0xEEC
	uint8_t				reserved05[0xF34-0xEF0];
	FUN_CODE::Reg			fun_code;			// 0xF34
	uint8_t				reserved06[0xF44-0xF38];
	R_CNTRL::Reg			r_cntrl;			// 0xF44
	R_HASH::Reg				r_hash;				// 0xF48
	uint8_t				reserved07[0xF84-0xF4C];
	X_CNTRL::Reg			x_cntrl;			// 0xF84
	uint8_t				reserved08[0x1000-0xF88];
	} Device;

}
}
}



#endif
