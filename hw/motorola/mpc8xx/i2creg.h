/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _hw_mot8xx_i2cregh_
#define _hw_mot8xx_i2cregh_
#include <stdint.h>
namespace Oscl { // Namespace description
	namespace Mot8xx { // Namespace description
		namespace I2C { // Namespace description
			namespace I2MOD { // Register description
				typedef uint8_t	Reg;
				namespace EN { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x00000001};
					enum {Value_Enable = 0x1};
					enum {ValueMask_Enable = 0x00000001};
					enum {Value_Disable = 0x0};
					enum {ValueMask_Disable = 0x00000000};
					};
				namespace PDIV { // Field Description
					enum {Lsb = 1};
					enum {FieldMask = 0x00000006};
					enum {Value_DivideBy32 = 0x0};
					enum {ValueMask_DivideBy32 = 0x00000000};
					enum {Value_DivideBy16 = 0x1};
					enum {ValueMask_DivideBy16 = 0x00000002};
					enum {Value_DivideBy8 = 0x2};
					enum {ValueMask_DivideBy8 = 0x00000004};
					enum {Value_DivideBy4 = 0x3};
					enum {ValueMask_DivideBy4 = 0x00000006};
					};
				namespace FLT { // Field Description
					enum {Lsb = 3};
					enum {FieldMask = 0x00000008};
					enum {Value_Disable = 0x0};
					enum {ValueMask_Disable = 0x00000000};
					enum {Value_Enable = 0x1};
					enum {ValueMask_Enable = 0x00000008};
					};
				namespace GCD { // Field Description
					enum {Lsb = 4};
					enum {FieldMask = 0x00000010};
					enum {Value_GCEnable = 0x0};
					enum {ValueMask_GCEnable = 0x00000000};
					enum {Value_GCDisable = 0x1};
					enum {ValueMask_GCDisable = 0x00000010};
					};
				namespace REVD { // Field Description
					enum {Lsb = 5};
					enum {FieldMask = 0x00000020};
					enum {Value_Normal = 0x0};
					enum {ValueMask_Normal = 0x00000000};
					enum {Value_Reverse = 0x1};
					enum {ValueMask_Reverse = 0x00000020};
					};
				};
			namespace I2ADD { // Register description
				typedef uint8_t	Reg;
				namespace SAD { // Field Description
					enum {Lsb = 1};
					enum {FieldMask = 0x000000FE};
					enum {Value_Min = 0x0};
					enum {ValueMask_Min = 0x00000000};
					enum {Value_Max = 0x7F};
					enum {ValueMask_Max = 0x000000FE};
					};
				};
			namespace I2BRG { // Register description
				typedef uint8_t	Reg;
				enum {Min = 0};
				enum {Max = 255};
				};
			namespace I2COM { // Register description
				typedef uint8_t	Reg;
				namespace MS { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x00000001};
					enum {Value_Slave = 0x0};
					enum {ValueMask_Slave = 0x00000000};
					enum {Value_Master = 0x1};
					enum {ValueMask_Master = 0x00000001};
					};
				namespace STR { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x00000001};
					enum {Value_Start = 0x1};
					enum {ValueMask_Start = 0x00000001};
					};
				};
			namespace I2CER { // Register description
				typedef uint8_t	Reg;
				namespace RXB { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x00000001};
					enum {Value_Clear = 0x1};
					enum {ValueMask_Clear = 0x00000001};
					enum {Value_Pending = 0x1};
					enum {ValueMask_Pending = 0x00000001};
					enum {Value_NotPending = 0x0};
					enum {ValueMask_NotPending = 0x00000000};
					};
				namespace TXB { // Field Description
					enum {Lsb = 1};
					enum {FieldMask = 0x00000002};
					enum {Value_Clear = 0x1};
					enum {ValueMask_Clear = 0x00000002};
					enum {Value_Pending = 0x1};
					enum {ValueMask_Pending = 0x00000002};
					enum {Value_NotPending = 0x0};
					enum {ValueMask_NotPending = 0x00000000};
					};
				namespace BSY { // Field Description
					enum {Lsb = 2};
					enum {FieldMask = 0x00000004};
					enum {Value_Clear = 0x1};
					enum {ValueMask_Clear = 0x00000004};
					enum {Value_Pending = 0x1};
					enum {ValueMask_Pending = 0x00000004};
					enum {Value_NotPending = 0x0};
					enum {ValueMask_NotPending = 0x00000000};
					};
				namespace TXE { // Field Description
					enum {Lsb = 3};
					enum {FieldMask = 0x00000008};
					enum {Value_Clear = 0x1};
					enum {ValueMask_Clear = 0x00000008};
					enum {Value_Pending = 0x1};
					enum {ValueMask_Pending = 0x00000008};
					enum {Value_NotPending = 0x0};
					enum {ValueMask_NotPending = 0x00000000};
					};
				};
			namespace I2CMR { // Register description
				typedef uint8_t	Reg;
				namespace RXB { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x00000001};
					enum {Value_Disable = 0x0};
					enum {ValueMask_Disable = 0x00000000};
					enum {Value_Enable = 0x1};
					enum {ValueMask_Enable = 0x00000001};
					};
				namespace TXB { // Field Description
					enum {Lsb = 1};
					enum {FieldMask = 0x00000002};
					enum {Value_Disable = 0x0};
					enum {ValueMask_Disable = 0x00000000};
					enum {Value_Enable = 0x1};
					enum {ValueMask_Enable = 0x00000002};
					};
				namespace BSY { // Field Description
					enum {Lsb = 2};
					enum {FieldMask = 0x00000004};
					enum {Value_Disable = 0x0};
					enum {ValueMask_Disable = 0x00000000};
					enum {Value_Enable = 0x1};
					enum {ValueMask_Enable = 0x00000004};
					};
				namespace TXE { // Field Description
					enum {Lsb = 3};
					enum {FieldMask = 0x00000008};
					enum {Value_Disable = 0x0};
					enum {ValueMask_Disable = 0x00000000};
					enum {Value_Enable = 0x1};
					enum {ValueMask_Enable = 0x00000008};
					};
				};
			};
		};
	};
#endif
