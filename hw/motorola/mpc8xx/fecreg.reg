/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/


module hw_mot8xx_fecregh {
include "oscl/compiler/types.h";
namespace Oscl {
namespace Mot8xx {
namespace Fec {
	register ADDR_LOW uint32_t {
		// Perfect Match Address Low
		field Byte0 24 8 {
			}
		field Byte1 16 8 {
			}
		field Byte2 8 8 {
			}
		field Byte3 0 8 {
			}
		}
	register ADDR_HIGH uint32_t {
		// Perfect Match Address High
		field Byte4 24 8 {
			}
		field Byte5 16 8 {
			}
		}
	register HASH_TABLE_HIGH uint32_t {
		}
	register HASH_TABLE_LOW uint32_t {
		}
	register R_DES_START uint32_t {
		// Rx BD ring start
		value Align	4
		}
	register X_DES_START uint32_t {
		// Tx BD ring start
		value Align	4
		}
	register R_BUFF_SIZE uint32_t {
		value Min	0x00000010
		value Max	0x000003F0
		value Align	16
		}
	register ECNTRL uint32_t {
		field RESET 0 1 {
			value Reset	1
			value Busy	1
			value Done	0
			}
		field ETHER_EN 1 1 {
			value Disable	0
			value Enable	1
			}
		field FEC_PIN_MUX 2 1 {
			value Enable	1
			}
		}
	register IEVENT uint32_t {
		field HBERR 31 1 {
			value Clear			1
			value Pending		1
			value NotPending	0
			}
		field BABR 30 1 {
			value Clear			1
			value Pending		1
			value NotPending	0
			}
		field BABT 29 1 {
			value Clear			1
			value Pending		1
			value NotPending	0
			}
		field GRA 28 1 {
			value Clear			1
			value Pending		1
			value NotPending	0
			}
		field TFINT 27 1 {
			value Clear			1
			value Pending		1
			value NotPending	0
			}
		field TXB 26 1 {
			value Clear			1
			value Pending		1
			value NotPending	0
			}
		field RFINT 25 1 {
			value Clear			1
			value Pending		1
			value NotPending	0
			}
		field RXB 24 1 {
			value Clear			1
			value Pending		1
			value NotPending	0
			}
		field MII 23 1 {
			value Clear			1
			value Pending		1
			value NotPending	0
			}
		field EBERR 22 1 {
			value Clear			1
			value Pending		1
			value NotPending	0
			}
		}
	register IMASK uint32_t {
		field HBERR 31 1 {
			value Disable	0
			value Enable	1
			}
		field BABR 30 1 {
			value Disable	0
			value Enable	1
			}
		field BABT 29 1 {
			value Disable	0
			value Enable	1
			}
		field GRA 28 1 {
			value Disable	0
			value Enable	1
			}
		field TFINT 27 1 {
			value Disable	0
			value Enable	1
			}
		field TXB 26 1 {
			value Disable	0
			value Enable	1
			}
		field RFINT 25 1 {
			value Disable	0
			value Enable	1
			}
		field RXB 24 1 {
			value Disable	0
			value Enable	1
			}
		field MII 23 1 {
			value Disable	0
			value Enable	1
			}
		field EBERR 22 1 {
			value Disable	0
			value Enable	1
			}
		}
	register IVEC uint32_t {
		field ILEVEL 29 3 {
			// SIU interrupt priority
			}
		field IVEC 2 2 {
			// Read only
			value None				0
			value NonTimeCritical	1
			value Transmit			2
			value Receive			3
			}
		}
	register R_DES_ACTIVE uint32_t {
		field R_DES_ACTIVE 24 1 {
			value Activate	1
			value Active	1
			value NotActive	0
			}
		}
	register X_DES_ACTIVE uint32_t {
		field X_DES_ACTIVE 24 1 {
			value Activate	1
			value Active	1
			value NotActive	0
			}
		}
	register MII_DATA uint32_t {
		field ST 30 2 {
			value Valid	0x01
			}
		field OP 28 2 {
			value Read	0x02
			value Write	0x01
			}
		field PA 23 5 {
			// Physical device address
			value Min 0
			value Max 31
			}
		field RA 18 5 {
			// Register address
			value Min 0
			value Max 31
			}
		field TA 16 2 {
			// Turnaround
			value Valid	0x02
			}
		field DATA 0 16 {
			// Data to be written to, or
			// data returned from an MII register.
			}
		}
	register MII_SPEED uint32_t {
		field DIS_PREAMBLE 7 1 {
			value PreamblePrependToMiiFrame		0
			value NoPreamblePrependToMiiFrame	1
			}
		field MII_SPEED 1 6 {
			value Off	0
			value Min	1
			value Max	0x3F
			}
		}
	register R_BOUND uint32_t {
		field R_BOUND 0 10 {
			// Read only
			// Highest valid FIFO RAM address
			value Align	4
			}
		}
	register R_FSTART uint32_t {
		field R_FSTART 0 10 {
			// Address of first receive FIFO location.
			// Acts as a delimiter between receive and
			// transmit FIFOs. This only needs to
			// be written to alter the default.
			value Align	4
			}
		}
	register X_WMRK uint32_t {
		field X_WMRK 0 2 {
			// Transmit FIFO watermark. Frame transmission
			// begins when the number of bytes selected
			// by this field have been written into the
			// transmit FIFO or if an end-of-frame has
			// been written to the FIFO or if the FIFO
			// is full before the selected number of bytes
			// have been written.
			value Size64Bytes	0
			value Size128Bytes	2
			value Size192Bytes	3
			}
		}
	register X_FSTART uint32_t {
		field X_FSTART 0 10 {
			// Address of first transmit FIFO location.
			value Align	4
			}
		}
	register FUN_CODE uint32_t {
		field DATA_BO 29 2 {
			value PpcLittleEndian	0x01
			value BigEndian			0x02
			value TrueLittleEndian	0x02
			}
		field DESC_BO 27 2 {
			value PpcLittleEndian	0x01
			value BigEndian			0x02
			value TrueLittleEndian	0x02
			}
		field FC 24 3 {
			value Min	0
			value Max	0x07
			}
		}
	register R_CNTRL uint32_t {
		field BC_REJ 4 1 {
			// Broadcast is Destination address
			// 0xFFFF_FFFF_FFFF
			value AcceptBroadcast	0
			value RejectBroadcast	1
			}
		field PROM 3 1 {
			// Promiscuous mode
			value Disable	0
			value Enable	1
			}
		field MII_MODE 2 1 {
			value SevenWireMode	0
			value MiiMode		1
			}
		field DRT 1 1 {
			value IndependentRxTx	0
			value DisableRxOnTx		1
			}
		field LOOP 0 1 {
			// DRT must be 0 when asserting LOOP
			value Disable	0
			value Enable	1
			}
		}
	register R_HASH uint32_t {
		field MAX_FRAME_LENGTH 0 11 {
			value Default	1518
			value VlanTags	1522
			value Min	0
			value Max	0x7FF
			}
		}
	register X_CNTRL uint32_t {
		field FDEN 2 1 {
			// Full duplex enable
			value Disable	0
			value Enable	1
			}
		field HBC 1 1 {
			// Heartbeat control
			value Disable	0
			value Enable	1
			}
		field GTS 0 1 {
			// Graceful Transmit Stop
			value Transmit		0
			value BeginStop		1
			}
		}
	}
}
}
}
