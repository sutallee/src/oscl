/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _hw_mot8xx_cpmregh_
#define _hw_mot8xx_cpmregh_
#include <stdint.h>
namespace Oscl { // Namespace description
	namespace Mot8xx { // Namespace description
		namespace Cpm { // Namespace description
			namespace CPCR { // Register description
				typedef uint16_t	Reg;
				namespace RST { // Field Description
					enum {Lsb = 15};
					enum {FieldMask = 0x00008000};
					enum {Value_Normal = 0x0};
					enum {ValueMask_Normal = 0x00000000};
					enum {Value_Reset = 0x1};
					enum {ValueMask_Reset = 0x00008000};
					};
				namespace OPCODE { // Field Description
					enum {Lsb = 8};
					enum {FieldMask = 0x00000F00};
					namespace SCC { // Field Description
						enum {Lsb = 8};
						enum {FieldMask = 0x00000F00};
						enum {Value_InitRxAndTxParams = 0x0};
						enum {ValueMask_InitRxAndTxParams = 0x00000000};
						enum {Value_InitRxParams = 0x1};
						enum {ValueMask_InitRxParams = 0x00000100};
						enum {Value_InitTxParams = 0x2};
						enum {ValueMask_InitTxParams = 0x00000200};
						enum {Value_EnterHuntMode = 0x3};
						enum {ValueMask_EnterHuntMode = 0x00000300};
						enum {Value_StopTx = 0x4};
						enum {ValueMask_StopTx = 0x00000400};
						enum {Value_GracefulStopTx = 0x5};
						enum {ValueMask_GracefulStopTx = 0x00000500};
						enum {Value_RestartTx = 0x6};
						enum {ValueMask_RestartTx = 0x00000600};
						enum {Value_CloseRxBd = 0x7};
						enum {ValueMask_CloseRxBd = 0x00000700};
						enum {Value_SetGroupAddress = 0x8};
						enum {ValueMask_SetGroupAddress = 0x00000800};
						enum {Value_ResetBcs = 0xA};
						enum {ValueMask_ResetBcs = 0x00000A00};
						};
					namespace SMC { // Field Description
						enum {Lsb = 8};
						enum {FieldMask = 0x00000F00};
						enum {Value_InitRxAndTxParams = 0x0};
						enum {ValueMask_InitRxAndTxParams = 0x00000000};
						enum {Value_InitRxParams = 0x1};
						enum {ValueMask_InitRxParams = 0x00000100};
						enum {Value_InitTxParams = 0x2};
						enum {ValueMask_InitTxParams = 0x00000200};
						enum {Value_EnterHuntMode = 0x3};
						enum {ValueMask_EnterHuntMode = 0x00000300};
						enum {Value_StopTx = 0x4};
						enum {ValueMask_StopTx = 0x00000400};
						enum {Value_RestartTx = 0x6};
						enum {ValueMask_RestartTx = 0x00000600};
						enum {Value_CloseRxBd = 0x7};
						enum {ValueMask_CloseRxBd = 0x00000700};
						};
					namespace GCI { // Field Description
						enum {Lsb = 8};
						enum {FieldMask = 0x00000F00};
						enum {Value_InitRxAndTxParams = 0x0};
						enum {ValueMask_InitRxAndTxParams = 0x00000000};
						enum {Value_GciTimeout = 0x9};
						enum {ValueMask_GciTimeout = 0x00000900};
						enum {Value_GciAbortRequest = 0xA};
						enum {ValueMask_GciAbortRequest = 0x00000A00};
						};
					namespace SPI { // Field Description
						enum {Lsb = 8};
						enum {FieldMask = 0x00000F00};
						enum {Value_InitRxAndTxParams = 0x0};
						enum {ValueMask_InitRxAndTxParams = 0x00000000};
						enum {Value_InitRxParams = 0x1};
						enum {ValueMask_InitRxParams = 0x00000100};
						enum {Value_InitTxParams = 0x2};
						enum {ValueMask_InitTxParams = 0x00000200};
						enum {Value_CloseRxBd = 0x7};
						enum {ValueMask_CloseRxBd = 0x00000700};
						};
					namespace I2C { // Field Description
						enum {Lsb = 8};
						enum {FieldMask = 0x00000F00};
						enum {Value_InitRxAndTxParams = 0x0};
						enum {ValueMask_InitRxAndTxParams = 0x00000000};
						enum {Value_InitRxParams = 0x1};
						enum {ValueMask_InitRxParams = 0x00000100};
						enum {Value_InitTxParams = 0x2};
						enum {ValueMask_InitTxParams = 0x00000200};
						enum {Value_CloseRxBd = 0x7};
						enum {ValueMask_CloseRxBd = 0x00000700};
						};
					namespace IDMA { // Field Description
						enum {Lsb = 8};
						enum {FieldMask = 0x00000F00};
						enum {Value_InitIdma = 0x5};
						enum {ValueMask_InitIdma = 0x00000500};
						enum {Value_StopIdma = 0xB};
						enum {ValueMask_StopIdma = 0x00000B00};
						};
					namespace DSP { // Field Description
						enum {Lsb = 8};
						enum {FieldMask = 0x00000F00};
						enum {Value_StartDsp = 0xC};
						enum {ValueMask_StartDsp = 0x00000C00};
						enum {Value_InitDsp = 0xD};
						enum {ValueMask_InitDsp = 0x00000D00};
						};
					namespace Timer { // Field Description
						enum {Lsb = 8};
						enum {FieldMask = 0x00000F00};
						enum {Value_SetTimer = 0x8};
						enum {ValueMask_SetTimer = 0x00000800};
						};
					};
				namespace CH_NUM { // Field Description
					enum {Lsb = 4};
					enum {FieldMask = 0x000000F0};
					enum {Value_SCC1 = 0x0};
					enum {ValueMask_SCC1 = 0x00000000};
					enum {Value_I2C = 0x1};
					enum {ValueMask_I2C = 0x00000010};
					enum {Value_IDMA1 = 0x1};
					enum {ValueMask_IDMA1 = 0x00000010};
					enum {Value_SCC2 = 0x4};
					enum {ValueMask_SCC2 = 0x00000040};
					enum {Value_SPI = 0x5};
					enum {ValueMask_SPI = 0x00000050};
					enum {Value_IDMA2 = 0x5};
					enum {ValueMask_IDMA2 = 0x00000050};
					enum {Value_RISCTimers = 0x5};
					enum {ValueMask_RISCTimers = 0x00000050};
					enum {Value_SCC3 = 0x8};
					enum {ValueMask_SCC3 = 0x00000080};
					enum {Value_SMC1 = 0x9};
					enum {ValueMask_SMC1 = 0x00000090};
					enum {Value_DSP1Rx = 0x9};
					enum {ValueMask_DSP1Rx = 0x00000090};
					enum {Value_SCC4 = 0xC};
					enum {ValueMask_SCC4 = 0x000000C0};
					enum {Value_SMC2 = 0xD};
					enum {ValueMask_SMC2 = 0x000000D0};
					enum {Value_DSP2Tx = 0xD};
					enum {ValueMask_DSP2Tx = 0x000000D0};
					enum {Value_PIP = 0xD};
					enum {ValueMask_PIP = 0x000000D0};
					};
				namespace FLG { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x00000001};
					enum {Value_Ready = 0x0};
					enum {ValueMask_Ready = 0x00000000};
					enum {Value_Busy = 0x1};
					enum {ValueMask_Busy = 0x00000001};
					};
				};
			namespace RCCR { // Register description
				typedef uint16_t	Reg;
				namespace TIME { // Field Description
					enum {Lsb = 15};
					enum {FieldMask = 0x00008000};
					enum {Value_Stop = 0x0};
					enum {ValueMask_Stop = 0x00000000};
					enum {Value_Start = 0x1};
					enum {ValueMask_Start = 0x00008000};
					};
				namespace TIMEP { // Field Description
					enum {Lsb = 8};
					enum {FieldMask = 0x00003F00};
					enum {Value_Min = 0x0};
					enum {ValueMask_Min = 0x00000000};
					enum {Value_Max = 0x3F};
					enum {ValueMask_Max = 0x00003F00};
					};
				namespace DR1M { // Field Description
					enum {Lsb = 7};
					enum {FieldMask = 0x00000080};
					enum {Value_Edge = 0x0};
					enum {ValueMask_Edge = 0x00000000};
					enum {Value_Level = 0x1};
					enum {ValueMask_Level = 0x00000080};
					};
				namespace DR0M { // Field Description
					enum {Lsb = 6};
					enum {FieldMask = 0x00000040};
					enum {Value_Edge = 0x0};
					enum {ValueMask_Edge = 0x00000000};
					enum {Value_Level = 0x1};
					enum {ValueMask_Level = 0x00000040};
					};
				namespace DRQP { // Field Description
					enum {Lsb = 4};
					enum {FieldMask = 0x00000030};
					enum {Value_Highest = 0x0};
					enum {ValueMask_Highest = 0x00000000};
					enum {Value_AfterSccs = 0x1};
					enum {ValueMask_AfterSccs = 0x00000010};
					enum {Value_Lowest = 0x2};
					enum {ValueMask_Lowest = 0x00000020};
					};
				namespace EIE { // Field Description
					enum {Lsb = 3};
					enum {FieldMask = 0x00000008};
					enum {Value_Disable = 0x0};
					enum {ValueMask_Disable = 0x00000000};
					enum {Value_Enable = 0x1};
					enum {ValueMask_Enable = 0x00000008};
					};
				namespace SCD { // Field Description
					enum {Lsb = 2};
					enum {FieldMask = 0x00000004};
					enum {Value_Normal = 0x0};
					enum {ValueMask_Normal = 0x00000000};
					enum {Value_Alternate = 0x1};
					enum {ValueMask_Alternate = 0x00000004};
					};
				namespace ERAM { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x00000003};
					enum {Value_Disable = 0x0};
					enum {ValueMask_Disable = 0x00000000};
					enum {Value_First512Bytes = 0x1};
					enum {ValueMask_First512Bytes = 0x00000001};
					enum {Value_First1KBytes = 0x2};
					enum {ValueMask_First1KBytes = 0x00000002};
					enum {Value_First2KBytes = 0x2};
					enum {ValueMask_First2KBytes = 0x00000002};
					};
				};
			namespace RMDS { // Register description
				typedef uint8_t	Reg;
				};
			namespace RCTR1 { // Register description
				typedef uint16_t	Reg;
				};
			namespace RCTR2 { // Register description
				typedef uint16_t	Reg;
				};
			namespace RCTR3 { // Register description
				typedef uint16_t	Reg;
				};
			namespace RCTR4 { // Register description
				typedef uint16_t	Reg;
				};
			namespace RTER { // Register description
				typedef uint16_t	Reg;
				};
			namespace RTMR { // Register description
				typedef uint16_t	Reg;
				};
			};
		};
	};
#endif
