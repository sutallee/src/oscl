/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/


module hw_mot8xx_sccregh {
	include "oscl/compiler/types.h";
	namespace Oscl {
		namespace Mot8xx {
			namespace Scc {
				register GSMR_L uint32_t {
					field SIR 31 1 {
						value Normal	0
						// SCC2 only
						value SerialInfrared	1
						}
					field EDGE 29 2 {
						value BothEdges		0
						value PositiveEdge	1
						value NegativeEdge	2
						}
					field TCI 28 1 {
						value Normal	0
						value Invert	1
						}
					field TSNC 26 2 {
						value Infinite		0
						value Rdcr14or6_5	1
						value Rdcr4or1_5	2
						value Rdcr3or1		3
						}
					field RINV 25 1 {
						value Normal		0
						value InvertData	1
						}
					field TINV 24 1 {
						value Normal		0
						value InvertData	1
						}
					field TPL 21 3 {
						value PreambleNone		0
						value Preamble8Bits		1
						value Preamble16Bits	2
						value Preamble32Bits	3
						value Preamble48Bits	4
						value Preamble64Bits	5
						value Preamble128Bits	6
						}
					field TPP 19 2 {
						value AllZeros		0
						value Repetitive10	1
						value Repetitive01	2
						value AllOnes		3
						}
					field TEND 18 1 {
						value DataOnly		0
						value DataAndIdles	1
						}
					field TDCR 16 2 {
						value Clock1X	0
						value Clock8X	1
						value Clock16X	2
						value Clock32X	3
						}
					field RDCR 14 2 {
						value Clock1X	0
						value Clock8X	1
						value Clock16X	2
						value Clock32X	3
						}
					field RENC 11 3 {
						value	NRZ				0
						value	NRZI			1
						value	FM0				2
						value	Manchester		4
						value	DiffManchester	6
						}
					field TENC 8 3 {
						value	NRZ				0
						value	NRZI			1
						value	FM0				2
						value	Manchester		4
						value	DiffManchester	6
						}
					field DIAG 6 2 {
						value Normal		0
						value LocalLoop		1
						value AutoEcho		2
						value LoopAndEcho	3
						}
					field ENR 5 1 {
						value Disable	0
						value Enable	1
						}
					field ENT 4 1 {
						value Disable	0
						value Enable	1
						}
					field MODE 0 4 {
						value HDLC			0
						value LocalTalk		2
						value SS7			3
						value UART			4
						value Profibus		5
						value AHDLC			6
						value IrDA			6
						value V_14			7
						value BISYNC		8
						value DDCMP			9
						value ENET			12
						}
					}
				register GSMR_H uint32_t {
					field IRP 18 1 {
						value ActiveHigh	0
						value ActiveLow		1
						}
					field GDE 16 1 {
						value NoGlitchDetect	0
						value GlitchDetect		1
						}
					field TCRC 14 2 {
						value NonTransparent	0
						value CcittCrc16		0
						value Hdlc16			0
						value Crc16				1
						value Bisync			1
						value CcittCrc32		2
						value Hdlc32			2
						value ENET				2
						}
					field REVD 13 1 {
						value Normal		0
						value ReverseData	1
						}
					field TRX 12 1 {
						value Normal		0
						value Transparent	0
						}
					field TTX 11 1 {
						value Normal		0
						value Transparent	0
						}
					field CDP 10 1 {
						value Normal	0
						value Pulse		1
						}
					field CTSP 9 1 {
						value Normal	0
						value Pulse		1
						}
					field CDS 8 1 {
						value Asynchronous	0
						value Synchronous	1
						}
					field CTSS 7 1 {
						value Asynchronous	0
						value Synchronous	1
						}
					field TFL 6 1 {
						value Normal		0
						value TxFifo32Bytes	0
						value TxFifo1Byte	1
						}
					field RFW 5 1 {
						value WideFifo		0
						value NarrowFifo	1
						}
					field TXSY 4 1 {
						value TxRxNotSynchronized	0
						value TxRxtSynchronized		1
						}
					field SYNL 2 2 {
						value SyncExternal	0
						value Sync4Bit		1
						value Sync8Bit		2
						value Sync16Bit		3
						}
					field RTSM 1 1 {
						value IdleBetweenFrames		0
						value FlagsBetweenFrames	1
						}
					field RSYN 0 1 {
						value Normal			0
						value DelayCdAssertion	1
						}
					}
				register PSMR uint16_t {
					field HDLC 0 16 {
						field NOF 12 4 {
							value NoFlags		0
							value Max			0xF
							}
						field CRC 10 2 {
							value CcittCrc16	0
							value CcittCrc32	2
							}
						field RTE 9 1 {
							value NoRetransmit		0
							value AutoRetransmit	1
							}
						field FSE 7 1 {
							value Normal		0
							value SharedFlag	1
							}
						field DRT 6 1 {
							value Normal			0
							value DisableRxWhileTx	1
							}
						field BUS 5 1 {
							value Normal	0
							value BusMode	1
							}
						field BRM 4 1 {
							value Normal			0
							value DelayRtsOneBit	1
							}
						field MFF 3 1 {
							value Normal						0
							value TxFifoMayHoldMultipleFrames	1
							}
						}
					field LocalTalk 0 16 {
						field NOF 12 4 {
							value NoFlags		0
							value Max			0xF
							}
						field CRC 10 2 {
							value CcittCrc16	0
							value CcittCrc32	2
							}
						field RTE 9 1 {
							value NoRetransmit		0
							value AutoRetransmit	1
							}
						field FSE 7 1 {
							value Normal		0
							value SharedFlag	1
							}
						field DRT 6 1 {
							value Normal			0
							value DisableRxWhileTx	1
							}
						field BUS 5 1 {
							value Normal	0
							value BusMode	1
							}
						field BRM 4 1 {
							value Normal			0
							value DelayRtsOneBit	1
							}
						field MFF 3 1 {
							value Normal						0
							value TxFifoMayHoldMultipleFrames	1
							}
						}
					field SS7 0 16 {
						}
					field UART 0 16 {
						field FLC 15 1 {
							value Normal		0
							value Asynchronous	1
							}
						field SL 14 1 {
							value OneStopBit	0
							value TwoStopBits	1
							}
						field CL 12 2 {
							value Data5Bits	0
							value Data6Bits	1
							value Data7Bits	2
							value Data8Bits	3
							}
						field UM 10 2 {
							value Normal			0
							value ManualMultidrop	1
							value AutoMultidrop		3
							}
						field FRZ 9 1 {
							value Character		0
							value Fifo			1
							}
						field RZS 8 1 {
							value StopBitRequired	0
							value StopBitOptional	1
							}
						field SYN 7 1 {
							value Oversampled		0
							value Synchronous		1
							}
						field DRT 6 1 {
							value Normal			0
							value DisableRxWhileTx	1
							}
						field PEN 4 1 {
							value NoParity		0
							value ParityEnable	1
							}
						field RPM 2 2 {
							value OddParity		0
							value LowParity		1
							value EventParity	2
							value HighParity	3
							}
						field TPM 0 2 {
							value OddParity		0
							value LowParity		1
							value EventParity	2
							value HighParity	3
							}
						}
					field Profibus 0 16 {
						}
					field AHDLC 0 16 {
						field CHLN 12 2 {
							// Character length
							value Constant	0x03
							}
						field FLC 15 1 {
							value Normal			0
							value CtsFlowControl	1
							}
						}
					field IrDA 0 16 {
						}
					field V_14 0 16 {
						}
					field BISYNC 0 16 {
						field NOS 12 4 {
							value NoFlags		0
							value Max			0xF
							}
						field CRC 10 2 {
							value CRC16			1
							value LRC			3
							}
						field RBCS 9 1 {
							value Disable		0
							value Enable		1
							}
						field RTR 8 1 {
							value Normal		0
							value Transparent	1
							}
						field RVD 7 1 {
							value Normal			0
							value ReverseBitOrder	1
							}
						field DRT 6 1 {
							value Normal			0
							value DisableRxWhileTx	1
							}
						field RPM 2 2 {
							value OddParity		0
							value LowParity		1
							value EventParity	2
							value HighParity	3
							}
						field TPM 0 2 {
							value OddParity		0
							value LowParity		1
							value EventParity	2
							value HighParity	3
							}
						}
					field DDCMP 0 16 {
						}
					field ENET 0 16 {
						field HBC 15 1 {
							value Disable	0
							value Enable	1
							}
						field FC 14 1 {
							value Normal			0
							value ForceCollision	1
							}
						field RSH 13 1 {
							value DiscardShortFrames	0
							value ReceiveShortFrames	1
							}
						field IAM 12 1 {
							value PADDR1	0
							value HashTable	1
							}
						field CRC 10 2 {
							value CcittCrc32	2
							}
						field PRO 9 1 {
							value Normal		0
							value Promiscuous	1
							}
						field BRO 8 1 {
							value ReceiveBroadcasts	0
							value RejectBroadcasts	1
							}
						field SBT 7 1 {
							value Normal		0
							value LessAgressive	1
							}
						field LPB 6 1 {
							value Normal	0
							value Loopback	1
							}
						field SIP 5 1 {
							value Normal		0
							value SampleTagByte	1
							}
						field LCW 4 1 {
							value Late64Byte	0
							value Late56Byte	1
							}
						field NIB 1 3 {
							value Minimum13Bits	0
							value Typical22Bits	5
							value Maximum24Bits	7
							}
						field FDE 0 1 {
							value Disable	0
							value Enable	1
							}
						}
					}
				register TODR uint16_t {
					}
				register DSR uint16_t {
					field SYN2 8 8 {
						field UART 0 8 {
							field Bit7 7 1 {
								value Normal	0
								}
							field FSB 3 4 {
								field Over16x 0 4 {
									value LastStop16_16	0xF
									value LastStop15_16	0xE
									value LastStop14_16	0xD
									value LastStop13_16	0xC
									value LastStop12_16	0xB
									value LastStop11_16	0xA
									value LastStop10_16	0x9
									value LastStop9_16	0x8
									}
								field Over32x 0 4 {	// ????
									value LastStop32_32	0xF
									value LastStop31_32	0xE
									value LastStop30_32	0xD
									value LastStop29_32	0xC
									value LastStop28_32	0xB
									value LastStop27_32	0xA
									value LastStop26_32	0x9
									value LastStop25_32	0x8
									value LastStop24_32	0x7
									value LastStop23_32	0x6
									value LastStop22_32	0x5
									value LastStop11_32	0x4
									value LastStop20_32	0x3
									value LastStop19_32	0x2
									value LastStop18_32	0x1
									value LastStop17_32	0x0
									}
								field Over8x 0 4 {
									value LastStop8_8	0xF
									value LastStop7_8	0xE
									value LastStop6_8	0xD
									value LastStop5_8	0xC
									}
								}
							field Bits2_0 0 3 {
								value Normal	6
								}
							}
						field BISYNC 0 8 {
							}
						field ENET 0 8 {
							value Normal	0xD5
							}
						field HDLC 0 8 {
							value Normal	0x7E
							}
						}
					field SYN1 0 8 {
						field UART 0 8 {
							value Normal	0x7E
							}
						field BISYNC 0 8 {
							}
						field ENET 0 8 {
							value Normal	0x55
							}
						field HDLC 0 8 {
							value Normal	0x7E
							}
						}
					}
				register SCCE uint16_t {
					field UART 0 16 {
						field RX 0 1 {
							value Clear			1
							value Pending		1
							value NotPending	0
							}
						field TX 1 1 {
							value Clear			1
							value Pending		1
							value NotPending	0
							}
						field BSY 2 1 {
							value Clear			1
							value Pending		1
							value NotPending	0
							}
						field CCR 3 1 {
							value Clear			1
							value Pending		1
							value NotPending	0
							}
						field BRKS 5 1 {
							value Clear			1
							value Pending		1
							value NotPending	0
							}
						field BRKE 6 1 {
							value Clear			1
							value Pending		1
							value NotPending	0
							}
						field GRA 7 1 {
							value Clear			1
							value Pending		1
							value NotPending	0
							}
						field IDL 8 1 {
							value Clear			1
							value Pending		1
							value NotPending	0
							}
						field AB 9 1 {
							value Clear			1
							value Pending		1
							value NotPending	0
							}
						field GLT 11 1 {
							value Clear			1
							value Pending		1
							value NotPending	0
							}
						field GLR 12 1 {
							value Clear			1
							value Pending		1
							value NotPending	0
							}
						}
					field HDLC 0 16 {
						field RXB 0 1 {
							value Clear			1
							value Pending		1
							value NotPending	0
							}
						field TXB 1 1 {
							value Clear			1
							value Pending		1
							value NotPending	0
							}
						field BSY 2 1 {
							value Clear			1
							value Pending		1
							value NotPending	0
							}
						field RXF 3 1 {
							value Clear			1
							value Pending		1
							value NotPending	0
							}
						field TXE 4 1 {
							value Clear			1
							value Pending		1
							value NotPending	0
							}
						field GRA 7 1 {
							value Clear			1
							value Pending		1
							value NotPending	0
							}
						field IDL 8 1 {
							value Clear			1
							value Pending		1
							value NotPending	0
							}
						field FLG 9 1 {
							value Clear			1
							value Pending		1
							value NotPending	0
							}
						field DCC 10 1 {
							value Clear			1
							value Pending		1
							value NotPending	0
							}
						field GLT 11 1 {
							value Clear			1
							value Pending		1
							value NotPending	0
							}
						field GLR 12 1 {
							value Clear			1
							value Pending		1
							value NotPending	0
							}
						}
					field AHDLC 0 16 {
						field RXB 0 1 {
							value Clear			1
							value Pending		1
							value NotPending	0
							}
						field TXB 1 1 {
							value Clear			1
							value Pending		1
							value NotPending	0
							}
						field BSY 2 1 {
							value Clear			1
							value Pending		1
							value NotPending	0
							}
						field RXF 3 1 {
							value Clear			1
							value Pending		1
							value NotPending	0
							}
						field TXE 4 1 {
							value Clear			1
							value Pending		1
							value NotPending	0
							}
						field BRKS 5 1 {
							value Clear			1
							value Pending		1
							value NotPending	0
							}
						field BRKE 6 1 {
							value Clear			1
							value Pending		1
							value NotPending	0
							}
						field IDL 8 1 {
							value Clear			1
							value Pending		1
							value NotPending	0
							}
						field GLT 11 1 {
							value Clear			1
							value Pending		1
							value NotPending	0
							}
						field GLR 12 1 {
							value Clear			1
							value Pending		1
							value NotPending	0
							}
						}
					field BISYNC 0 16 {
						field RXB 0 1 {
							value Clear			1
							value Pending		1
							value NotPending	0
							}
						field TXB 1 1 {
							value Clear			1
							value Pending		1
							value NotPending	0
							}
						field BSY 2 1 {
							value Clear			1
							value Pending		1
							value NotPending	0
							}
						field RCH 3 1 {
							value Clear			1
							value Pending		1
							value NotPending	0
							}
						field TXE 4 1 {
							value Clear			1
							value Pending		1
							value NotPending	0
							}
						field GRA 7 1 {
							value Clear			1
							value Pending		1
							value NotPending	0
							}
						field DCC 10 1 {
							value Clear			1
							value Pending		1
							value NotPending	0
							}
						field GLT 11 1 {
							value Clear			1
							value Pending		1
							value NotPending	0
							}
						field GLR 12 1 {
							value Clear			1
							value Pending		1
							value NotPending	0
							}
						}
					field ENET 0 16 {
						field RXB 0 1 {
							value Clear			1
							value Pending		1
							value NotPending	0
							}
						field TXB 1 1 {
							value Clear			1
							value Pending		1
							value NotPending	0
							}
						field BSY 2 1 {
							value Clear			1
							value Pending		1
							value NotPending	0
							}
						field RXF 3 1 {
							value Clear			1
							value Pending		1
							value NotPending	0
							}
						field TXE 4 1 {
							value Clear			1
							value Pending		1
							value NotPending	0
							}
						field GRA 7 1 {
							value Clear			1
							value Pending		1
							value NotPending	0
							}
						}
					field TRANS 0 16 {
						field RXB 0 1 {
							value Clear			1
							value Pending		1
							value NotPending	0
							}
						field TXB 1 1 {
							value Clear			1
							value Pending		1
							value NotPending	0
							}
						field BSY 2 1 {
							value Clear			1
							value Pending		1
							value NotPending	0
							}
						field TXE 4 1 {
							value Clear			1
							value Pending		1
							value NotPending	0
							}
						field GRA 7 1 {
							value Clear			1
							value Pending		1
							value NotPending	0
							}
						field DCC 10 1 {
							value Clear			1
							value Pending		1
							value NotPending	0
							}
						field GLT 11 1 {
							value Clear			1
							value Pending		1
							value NotPending	0
							}
						field GLR 12 1 {
							value Clear			1
							value Pending		1
							value NotPending	0
							}
						}
					}
				register SCCM uint16_t {
					field UART 0 16 {
						field RX 0 1 {
							value Disable	0
							value Enable	1
							}
						field TX 1 1 {
							value Disable	0
							value Enable	1
							}
						field BSY 2 1 {
							value Disable	0
							value Enable	1
							}
						field CCR 3 1 {
							value Disable	0
							value Enable	1
							}
						field BRKS 5 1 {
							value Disable	0
							value Enable	1
							}
						field BRKE 6 1 {
							value Disable	0
							value Enable	1
							}
						field GRA 7 1 {
							value Disable	0
							value Enable	1
							}
						field IDL 8 1 {
							value Disable	0
							value Enable	1
							}
						field AB 9 1 {
							value Disable	0
							value Enable	1
							}
						field GLT 11 1 {
							value Disable	0
							value Enable	1
							}
						field GLR 12 1 {
							value Disable	0
							value Enable	1
							}
						}
					field HDLC 0 16 {
						field RXB 0 1 {
							value Disable	0
							value Enable	1
							}
						field TXB 1 1 {
							value Disable	0
							value Enable	1
							}
						field BSY 2 1 {
							value Disable	0
							value Enable	1
							}
						field RXF 3 1 {
							value Disable	0
							value Enable	1
							}
						field TXE 4 1 {
							value Disable	0
							value Enable	1
							}
						field GRA 7 1 {
							value Disable	0
							value Enable	1
							}
						field IDL 8 1 {
							value Disable	0
							value Enable	1
							}
						field FLG 9 1 {
							value Disable	0
							value Enable	1
							}
						field DCC 10 1 {
							value Disable	0
							value Enable	1
							}
						field GLT 11 1 {
							value Disable	0
							value Enable	1
							}
						field GLR 12 1 {
							value Disable	0
							value Enable	1
							}
						}
					field AHDLC 0 16 {
						field RXB 0 1 {
							value Disable	0
							value Enable	1
							}
						field TXB 1 1 {
							value Disable	0
							value Enable	1
							}
						field BSY 2 1 {
							value Disable	0
							value Enable	1
							}
						field RXF 3 1 {
							value Disable	0
							value Enable	1
							}
						field TXE 4 1 {
							value Disable	0
							value Enable	1
							}
						field BRKS 5 1 {
							value Disable	0
							value Enable	1
							}
						field BRKE 6 1 {
							value Disable	0
							value Enable	1
							}
						field IDL 8 1 {
							value Disable	0
							value Enable	1
							}
						field GLT 11 1 {
							value Disable	0
							value Enable	1
							}
						field GLR 12 1 {
							value Disable	0
							value Enable	1
							}
						}
					field BISYNC 0 16 {
						field RXB 0 1 {
							value Disable	0
							value Enable	1
							}
						field TXB 1 1 {
							value Disable	0
							value Enable	1
							}
						field BSY 2 1 {
							value Disable	0
							value Enable	1
							}
						field RCH 3 1 {
							value Disable	0
							value Enable	1
							}
						field TXE 4 1 {
							value Disable	0
							value Enable	1
							}
						field GRA 7 1 {
							value Disable	0
							value Enable	1
							}
						field DCC 10 1 {
							value Disable	0
							value Enable	1
							}
						field GLT 11 1 {
							value Disable	0
							value Enable	1
							}
						field GLR 12 1 {
							value Disable	0
							value Enable	1
							}
						}
					field TRANS 0 16 {
						field RXB 0 1 {
							value Disable	0
							value Enable	1
							}
						field TXB 1 1 {
							value Disable	0
							value Enable	1
							}
						field BSY 2 1 {
							value Disable	0
							value Enable	1
							}
						field TXE 4 1 {
							value Disable	0
							value Enable	1
							}
						field GRA 7 1 {
							value Disable	0
							value Enable	1
							}
						field DCC 10 1 {
							value Disable	0
							value Enable	1
							}
						field GLT 11 1 {
							value Disable	0
							value Enable	1
							}
						field GLR 12 1 {
							value Disable	0
							value Enable	1
							}
						}
					field ENET 0 16 {
						field RXB 0 1 {
							value Disable	0
							value Enable	1
							}
						field TXB 1 1 {
							value Disable	0
							value Enable	1
							}
						field BSY 2 1 {
							value Disable	0
							value Enable	1
							}
						field RXF 3 1 {
							value Disable	0
							value Enable	1
							}
						field TXE 4 1 {
							value Disable	0
							value Enable	1
							}
						field GRA 7 1 {
							value Disable	0
							value Enable	1
							}
						}
					}
				register SCCS uint8_t {
					field HDLC 0 8 {
						field ID 0 1 {
							value Busy	0
							value Idle	1
							}
						field CS 0 1 {
							// DPLL carrier sense
							value NoCarrierPresent	0
							value CarrierPresent	1
							}
						field FG 0 1 {
							value NoFlagReceived	0
							value FlagReceived		1
							}
						}
					field AHDLC 0 8 {
						field ID 0 1 {
							value Busy	0
							value Idle	1
							}
						}
					field TRANS 0 8 {
						field CS 1 1 {
							// DPLL carrier sense
							value NoCarrierPresent	0
							value CarrierPresent	1
							}
						}
					field ENET 0 8 {
						field CS 1 1 {
							// DPLL carrier sense
							value NoCarrierPresent	0
							value CarrierPresent	1
							}
						}
					}
				register IRMODE uint16_t {
					}
				register IRSIP uint16_t {
					}
				namespace PRAM {
					register RBASE uint16_t {
						value Min	0
						value Max	0xFFFF
						}
					register TBASE uint16_t {
						value Min	0
						value Max	0xFFFF
						}
					register FCR uint8_t {
						// For RFCR and TFCR registers
						field AT 0 3 {
							value Min	0
							value Max	0x07
							}
						field BO 3 2 {
							value PpcLittleEndian	0x01
							value BigEndian			0x02
							value TrueLittleEndian	0x02
							}
						}
					register MRBLR uint16_t {
						value Min			1
						value EtherAlign	4
						value HdlcAlign		4
						value Max			0xFFFF
						}
					register RSTATE uint32_t {
						}
					register RPTR uint32_t {
						}
					register RBPTR uint16_t {
						}
					register RCOUNT uint16_t {
						}
					register RTEMP uint32_t {
						}
					register TSTATE uint32_t {
						}
					register TPTR uint32_t {
						}
					register TBPTR uint16_t {
						}
					register TCOUNT uint16_t {
						}
					register TTEMP uint32_t {
						}
					namespace UART {
						register MAX_IDL uint16_t {
							// Used for idle timeout
							value Disable	0
							value Min		1
							value Max		0xFFFF
							}
						register IDLC uint16_t {
							// Internal idle counter
							}
						register BRKCR uint16_t {
							// Number of break characters transmitted
							// when STOP TRANSMIT command is issued.
							}
						register PAREC uint16_t {
							// Number of parity errors
							}
						register FRMEC uint16_t {
							// Number of framing errors
							}
						register NOSEC uint16_t {
							// Number of noise errors
							}
						register  BRKEC uint16_t {
							// Number of break conditions
							}
						register  BRKLN uint16_t {
							// Length of last break in bit times
							}
						register  UADDR uint16_t {
							// UADDR1 and UADDR2
							// Address to be recognized in
							// multi-drop mode.
							}
						register  RTEMP uint16_t {
							// CP internal
							}
						register  TOSEQ uint16_t {
							// Used to send out-of-sequence
							// control characters.
							field CHARSEND 0 8 {
								// control character to send
								value Min	0
								value Max	0xFF
								}
							field A 8 1 {
								// Control
								value Data		0
								value Address	1
								}
							field CT 11 1 {
								// Status
								value CtsNotLost	0
								value CtsLost		1
								}
							field I 12 1 {
								// Interrupt Enable
								value Disable	0
								value Enable	1
								}
							field REA 13 1 {
								// Command to send control character.
								// Status to tell when character has
								// been sent.
								value Transmit		1
								value Done			0
								value Busy			1
								}
							}
						register  CHARACTER uint16_t {
							// A control character, the reception
							// of which will cause a buffer to be
							// closed and an interrupt generated.
							field CHARACTER 0 8 {
								value Min	0
								value Max	0xFF
								}
							field R 14 1 {
								// Reject character
								value ToRxBuffer	0
								value ToRCCR		1
								}
							field E 15 1 {
								// End of table.
								value Valid		0
								value NotValid	1
								}
							}
						register  RCCM uint16_t {
							field END 14 2 {
								value Always	0x3
								}
							field CHARACTER1 0 1 {
								value Ignore	0
								value Compare	1
								}
							field CHARACTER2 1 1 {
								value Ignore	0
								value Compare	1
								}
							field CHARACTER3 2 1 {
								value Ignore	0
								value Compare	1
								}
							field CHARACTER4 3 1 {
								value Ignore	0
								value Compare	1
								}
							field CHARACTER5 4 1 {
								value Ignore	0
								value Compare	1
								}
							field CHARACTER6 5 1 {
								value Ignore	0
								value Compare	1
								}
							field CHARACTER7 6 1 {
								value Ignore	0
								value Compare	1
								}
							field CHARACTER8 7 1 {
								value Ignore	0
								value Compare	1
								}
							}
						register  RCCR uint16_t {
							field Character 0 8 {
								value Min	0
								value Max	0xFF
								}
							}
						register  RLBC uint16_t {
							// Holds last break character pattern.
							}
						}
					namespace HDLC {
						register C_MASK uint32_t {
							// CRC Mask
							value Ccitt16	0x0000F0B8
							value Ccitt32	0xDEBB20E3
							}
						register C_PRES uint32_t {
							// CRC preset
							value Ccitt16	0x0000FFFF
							value Ccitt32	0xFFFFFFFF
							}
						register DISFC uint16_t {
							// Count of frames discarded
							// due to lack of buffers.
							value Min	0
							value Max	0xFFFF
							}
						register CRCEC uint16_t {
							// Count of frames discarded
							// due to CRC errors.
							value Min	0
							value Max	0xFFFF
							}
						register ABTSC uint16_t {
							// Abort sequence counter.
							value Min	0
							value Max	0xFFFF
							}
						register NMARC uint16_t {
							// Error free frames discarded
							// due to non-matching address.
							value Min	0
							value Max	0xFFFF
							}
						register RETRC uint16_t {
							// Frames resent due to collision.
							value Min	0
							value Max	0xFFFF
							}
						register MFLR uint16_t {
							// Maximum octets in frame
							// between opening and closing flag.
							value Min	0
							value Max	0xFFFF
							}
						register MAX_CNT uint16_t {
							// Internal frame length counter.
							value Min	0
							value Max	0xFFFF
							}
						register RFTHR uint16_t {
							// Threshold determining the receive
							// frame interrupt frequency.
							value Min	0
							value Max	0xFFFF
							}
						register RFCNT uint16_t {
							// Internal RFTHR down counter.
							}
						register HMASK uint16_t {
							// Indicates which bits of HADDR
							// should be compared.
							value Addr16Bit	0xFFFF
							value Addr8Bit	0x00FF
							}
						register HADDR uint16_t {
							field FirstOctet 0 8 {
								value Min	0
								value Max	0xFF
								}
							field SecondOctet 8 8 {
								value Min	0
								value Max	0xFF
								}
							}
						register TMP uint16_t {
							// CP temp
							}
						register TMP_MB uint16_t {
							// CP temp
							}
						}
					namespace AHDLC {
						register C_MASK uint32_t {
							// CRC Mask
							value Ccitt16	0x0000F0B8
							}
						register C_PRES uint32_t {
							// CRC preset
							value Ccitt16	0x0000FFFF
							}
						register BOF uint16_t {
							value PPP	0x007E
							value IrLAP	0x00C0
							}
						register EoF uint16_t {
							value PPP	0x007E
							value IrLAP	0x00C1
							}
						register ESC uint16_t {
							value PPP	0x007D
							value IrLAP	0x007D
							}
						register ZERO uint16_t {
							value Clear	0x0000
							}
						register RFTHR uint16_t {
							// Threshold determining the receive
							// frame interrupt frequency.
							value Min	0
							value Max	0xFFFF
							}
						register TXCTL_TBL uint32_t {
							// RFC 1549 control character
							// table bit map.
							field Char1F 31 1 {
								value MappingNotRequired	0
								value MappingRequired		1
								}
							field Char1E 30 1 {
								value MappingNotRequired	0
								value MappingRequired		1
								}
							field Char1D 29 1 {
								value MappingNotRequired	0
								value MappingRequired		1
								}
							field Char1C 28 1 {
								value MappingNotRequired	0
								value MappingRequired		1
								}
							field Char1B 27 1 {
								value MappingNotRequired	0
								value MappingRequired		1
								}
							field Char1A 26 1 {
								value MappingNotRequired	0
								value MappingRequired		1
								}
							field Char19 25 1 {
								value MappingNotRequired	0
								value MappingRequired		1
								}
							field Char18 24 1 {
								value MappingNotRequired	0
								value MappingRequired		1
								}
							field Char17 23 1 {
								value MappingNotRequired	0
								value MappingRequired		1
								}
							field Char16 22 1 {
								value MappingNotRequired	0
								value MappingRequired		1
								}
							field Char15 21 1 {
								value MappingNotRequired	0
								value MappingRequired		1
								}
							field Char14 20 1 {
								value MappingNotRequired	0
								value MappingRequired		1
								}
							field Char13 19 1 {
								value MappingNotRequired	0
								value MappingRequired		1
								}
							field Char12 18 1 {
								value MappingNotRequired	0
								value MappingRequired		1
								}
							field Char11 17 1 {
								value MappingNotRequired	0
								value MappingRequired		1
								}
							field Char10 16 1 {
								value MappingNotRequired	0
								value MappingRequired		1
								}
							field Char0F 15 1 {
								value MappingNotRequired	0
								value MappingRequired		1
								}
							field Char0E 14 1 {
								value MappingNotRequired	0
								value MappingRequired		1
								}
							field Char0D 13 1 {
								value MappingNotRequired	0
								value MappingRequired		1
								}
							field Char0C 12 1 {
								value MappingNotRequired	0
								value MappingRequired		1
								}
							field Char0B 11 1 {
								value MappingNotRequired	0
								value MappingRequired		1
								}
							field Char0A 10 1 {
								value MappingNotRequired	0
								value MappingRequired		1
								}
							field Char09 9 1 {
								value MappingNotRequired	0
								value MappingRequired		1
								}
							field Char08 8 1 {
								value MappingNotRequired	0
								value MappingRequired		1
								}
							field Char07 7 1 {
								value MappingNotRequired	0
								value MappingRequired		1
								}
							field Char06 6 1 {
								value MappingNotRequired	0
								value MappingRequired		1
								}
							field Char05 5 1 {
								value MappingNotRequired	0
								value MappingRequired		1
								}
							field Char04 4 1 {
								value MappingNotRequired	0
								value MappingRequired		1
								}
							field Char03 3 1 {
								value MappingNotRequired	0
								value MappingRequired		1
								}
							field Char02 2 1 {
								value MappingNotRequired	0
								value MappingRequired		1
								}
							field Char01 1 1 {
								value MappingNotRequired	0
								value MappingRequired		1
								}
							field Char00 0 1 {
								value MappingNotRequired	0
								value MappingRequired		1
								}
							}
						register MAX_CNT uint16_t {
							// Internal frame length counter.
							value Min	0
							value Max	0xFFFF
							}
						register RFCNT uint16_t {
							// Internal RFTHR down counter.
							}
						register HMASK uint16_t {
							// Indicates which bits of HADDR
							// should be compared.
							value Addr16Bit	0xFFFF
							value Addr8Bit	0x00FF
							}
						register HADDR uint16_t {
							field FirstOctet 0 8 {
								value Min	0
								value Max	0xFF
								}
							field SecondOctet 8 8 {
								value Min	0
								value Max	0xFF
								}
							}
						register TMP uint16_t {
							// CP temp
							}
						register TMP_MB uint16_t {
							// CP temp
							}
						}
					namespace BISYNC {
						register CRCC uint32_t {
							// CRC constant temporary value
							}
						register PCRC uint16_t {
							// Preset CRC16/LRC depends on BCS.
							value LRC		0xFFFF
							value Crc16		0x0000
							}
						register PAREC uint16_t {
							// Count of receive parity errors
							value Min		0
							value Max		0xFFFF
							}
						register BSYNC uint16_t {
							field SYNC 0 8 {
								// Second byte of DCS-SYNC pair
								}
							field V 15 1 {
								value NotValid	0
								value Valid		1
								}
							}
						register BDLE uint16_t {
							field DLE 0 8 {
								// Data link s
								}
							field V 15 1 {
								value NotValid	0
								value Valid		1
								}
							}
						register CHARACTER uint16_t {
							}
						register RCCM uint16_t {
							}
						}
					namespace ENET {
						register C_PRES uint32_t {
							value CrcCcitt32	0xFFFFFFFF
							}
						register C_MASK uint32_t {
							value CrcCcitt32	0xDEBB20E3
							}
						register CRCEC uint32_t {
							// Frames discarded due to CRC error.
							value Min	0
							value Max	0xFFFFFFFF
							}
						register ALEC uint32_t {
							// Frames discarded due to alignment errors.
							value Min	0
							value Max	0xFFFFFFFF
							}
						register DISFC uint32_t {
							// Frames discarded due to out of buffer
							// conditions.
							value Min	0
							value Max	0xFFFFFFFF
							}
						register PADS uint16_t {
							// Short frame padding characters.
							// Two bytes must be the same
							value Default	0x0000
							}
						register RET_LIM uint16_t {
							// Transmit retry limit.
							value Typical	15
							}
						register RET_CNT uint16_t {
							// Internal retry down-counter.
							}
						register MFLR uint16_t {
							// Maximum frame length. All octets
							// between start-of-frame delimiter
							// and the end-of-frame.
							value Typical	1518
							}
						register MINFLR uint16_t {
							// Minimum frame length.
							value Typical	64
							}
						register MAXD1 uint16_t {
							// Max DMA length.
							value Typical	1520
							}
						register MAXD2 uint16_t {
							// Max DMA length.
							value Typical	1520
							}
						register MAXD uint16_t {
							// Rx max DMA
							}
						register DMA_CNT uint16_t {
							// Internal Rx DMA down-counter.
							}
						register MAX_B uint16_t {
							// Maximum BD byte count
							}
						register GADDR uint16_t {
							// Group address filter.
							value Initial	0
							}
						register TBUF_DATA0 uint32_t {
							// Save area 0 current frame
							}
						register TBUF_DATA1 uint32_t {
							// Save area 1 current frame
							}
						register TBUF_RBA0 uint32_t {
							}
						register TBUF_CRC uint32_t {
							}
						register TBUF_BCNT uint16_t {
							}
						register PADDR1_H uint16_t {
							// High 2 octets of the Ethernet
							// station address.
							}
						register PADDR1_M uint16_t {
							// Middle 2 octets of the Ethernet
							// station address.
							}
						register PADDR1_L uint16_t {
							// Low 2 octets of the Ethernet
							// station address.
							}
						register P_PER uint16_t {
							// Reduces retry algorithm aggressiveness.
							value Normal			0
							value Min				0
							value Max				9
							value MostAggessive		1
							value LeastAggessive	9
							}
						register RFBD_PTR uint16_t {
							// Rx first BD pointer
							}
						register TFBD_PTR uint16_t {
							// Tx first BD pointer
							}
						register TLBD_PTR uint16_t {
							// Tx last BD pointer
							}
						register TX_LEN uint16_t {
							// Tx frame length counter
							}
						register IADDR uint16_t {
							// Individual address filter
							}
						register BOFF_CNT uint16_t {
							// Backoff counter.
							}
						register TADDR_H uint16_t {
							// High 2 octets of an Ethernet
							// address. Used in conjunction
							// with SET GROUP ADDRESS command
							// to modify hash table.
							}
						register TADDR_M uint16_t {
							// Middle 2 octets of an Ethernet
							// address. Used in conjunction
							// with SET GROUP ADDRESS command
							// to modify hash table.
							}
						register TADDR_L uint16_t {
							// Low 2 octets of an Ethernet
							// address. Used in conjunction
							// with SET GROUP ADDRESS command
							// to modify hash table.
							}
						}
					namespace TRANS {
						register CRC_P uint32_t {
							// CRC preset
							value Ccitt16	0x0000FFFF
							value Ccitt32	0xFFFFFFFF
							}
						register CRC_C uint32_t {
							// CRC Mask
							value Ccitt16	0x0000F0B8
							value Ccitt32	0xDEBB20E3
							}
						}
					}
				}
			}
		}
	}

