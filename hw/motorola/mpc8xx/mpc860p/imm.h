/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _hw_mot_mpc8xx_mpc860p_immh_
#define _hw_mot_mpc8xx_mpc860p_immh_
#include "oscl/hw/motorola/mpc8xx/siu.h"
#include "oscl/hw/motorola/mpc8xx/memc.h"
#include "oscl/hw/motorola/mpc8xx/sitimer.h"
#include "oscl/hw/motorola/mpc8xx/clkrst.h"
#include "oscl/hw/motorola/mpc8xx/sitkey.h"
#include "oscl/hw/motorola/mpc8xx/clkrstkey.h"
#include "oscl/hw/motorola/mpc8xx/i2c.h"
#include "oscl/hw/motorola/mpc8xx/dma.h"
#include "oscl/hw/motorola/mpc8xx/cpmic.h"
#include "oscl/hw/motorola/mpc8xx/port.h"
#include "oscl/hw/motorola/mpc8xx/cpmtimer.h"
#include "oscl/hw/motorola/mpc8xx/cpm.h"
#include "oscl/hw/motorola/mpc8xx/mpc860/brg.h"
#include "oscl/hw/motorola/mpc8xx/scc.h"
#include "oscl/hw/motorola/mpc8xx/smc.h"
#include "oscl/hw/motorola/mpc8xx/spi.h"
#include "oscl/hw/motorola/mpc8xx/pip.h"
#include "oscl/hw/motorola/mpc8xx/portb.h"
#include "oscl/hw/motorola/mpc8xx/si.h"
#include "oscl/hw/motorola/mpc8xx/siram.h"
#include "oscl/hw/motorola/mpc8xx/dpram.h"
#include "oscl/hw/motorola/mpc8xx/xdpram.h"
#include "oscl/hw/motorola/mpc8xx/pram.h"
#include "oscl/hw/motorola/mpc8xx/fec.h"
#include "oscl/hw/motorola/mpc8xx/mpc860/pcmcia.h"
#include "oscl/hw/motorola/mpc8xx/mpc860/pram.h"

/** */
namespace Oscl {
/** */
namespace Mot8xx {
/** */
namespace Mpc860P {

/** */
typedef struct Map {
	/** */
	Oscl::Mot8xx::Siu::Map			siu;
	/** */
	uint8_t					reserved1[0x080-0x034];
	/** */
	Mpc860::Pcmcia::Map			pcmcia;
	/** */
	uint8_t					reserved2[0x100-0x0FC];
	/** */
	Mot8xx::Memc::Map			memc;
	/** */
	uint8_t					reserved3[0x200-0x180];
	/** */
	Mot8xx::SiTimer::Map		sitimer;
	/** */
	uint8_t					reserved4[0x280-0x24C];
	/** */
	Mot8xx::ClkRst::Map			clkrst;
	/** */
	uint8_t					reserved5[0x300-0x28C];
	/** */
	Mot8xx::SitKey::Map			sitkey;
	/** */
	uint8_t					reserved6[0x380-0x348];
	/** */
	Mot8xx::ClkRstKey::Map		clkrstkey;
	/** */
	uint8_t					reserved7[0x860-0x38C];
	/** */
	Mot8xx::I2C::Map			i2c;
	/** */
	uint8_t					reserved8[0x904-0x875];
	/** */
	Mot8xx::Dma::Map			dma;
	/** */
	uint8_t					reserved9[0x930-0x91D];
	/** */
	Mot8xx::Cpmic::Map			cpmic;
	/** */
	Mot8xx::Port::Map			port;
	/** */
	uint8_t					reserved10[0x980-0x978];
	/** */
	Mot8xx::CpmTimer::Map		cpmtimer;
	/** */
	uint8_t					reserved1x[0x9C0-0x9B8];
	/** */
	Mot8xx::Cpm::Map			cpm;
	/** */
	uint8_t					reserved11[0x9F0-0x9DC];
	/** */
	Mot8xx::Mpc860::Brg::Map	brg;
	/** */
	uint8_t					reserved12[0xA00-0x9F8];
	/** */
	Mot8xx::Scc::Map			scc1;
	/** */
	Mot8xx::Scc::Map			scc2;
	/** */
	Mot8xx::Scc::Map			scc3;
	/** */
	Mot8xx::Scc::Map			scc4;
	/** */
	Mot8xx::Smc::Map			smc1;
	/** */
	Mot8xx::Smc::Map			smc2;
	/** */
	Mot8xx::Spi::Map			spi;
	/** */
	Mot8xx::Pip::Map			pip;
	/** */
	Mot8xx::PortB::Map			portb;
	/** */
	uint8_t					reserved15[0xAE0-0xAC8];
	/** */
	Mot8xx::Si::Map				si;
	/** */
	uint8_t					reserved16[0xC00-0xAF4];
	/** */
	Mot8xx::SiRam::Map			siram;
	/** */
	Mot8xx::Fec::Device			fec;
	/** */
	uint8_t					reserved17[0x2000-0x1000];
	/** */
	Mot8xx::DpRam::Map			dpram;
	/** */
	Mot8xx::XDpRam::Map			xdpram;
	/** */
	Mot8xx::Mpc860::Pram::Page1	pram1;
	/** */
	Mot8xx::Mpc860::Pram::Page2	pram2;
	/** */
	Mot8xx::Mpc860::Pram::Page3	pram3;
	/** */
	Mot8xx::Mpc860::Pram::Page4	pram4;
	} Map;

};
};
};

#endif
