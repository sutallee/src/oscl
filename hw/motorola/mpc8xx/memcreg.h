/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _hw_mot8xx_memcregh_
#define _hw_mot8xx_memcregh_
#include <stdint.h>
namespace Oscl { // Namespace description
	namespace Mot8xx { // Namespace description
		namespace Memc { // Namespace description
			namespace BR { // Register description
				typedef uint32_t	Reg;
				namespace BA { // Field Description
					enum {Lsb = 15};
					enum {FieldMask = 0xFFFF8000};
					};
				namespace AT { // Field Description
					enum {Lsb = 12};
					enum {FieldMask = 0x00007000};
					enum {Value_Max = 0x7};
					enum {ValueMask_Max = 0x00007000};
					};
				namespace PS { // Field Description
					enum {Lsb = 10};
					enum {FieldMask = 0x00000C00};
					enum {Value_PortSize32bit = 0x0};
					enum {ValueMask_PortSize32bit = 0x00000000};
					enum {Value_PortSize8bit = 0x1};
					enum {ValueMask_PortSize8bit = 0x00000400};
					enum {Value_PortSize16bit = 0x2};
					enum {ValueMask_PortSize16bit = 0x00000800};
					};
				namespace PARE { // Field Description
					enum {Lsb = 9};
					enum {FieldMask = 0x00000200};
					enum {Value_Disabled = 0x0};
					enum {ValueMask_Disabled = 0x00000000};
					enum {Value_Enabled = 0x1};
					enum {ValueMask_Enabled = 0x00000200};
					};
				namespace WP { // Field Description
					enum {Lsb = 8};
					enum {FieldMask = 0x00000100};
					enum {Value_ReadWrite = 0x0};
					enum {ValueMask_ReadWrite = 0x00000000};
					enum {Value_ReadOnly = 0x1};
					enum {ValueMask_ReadOnly = 0x00000100};
					};
				namespace MS { // Field Description
					enum {Lsb = 6};
					enum {FieldMask = 0x000000C0};
					enum {Value_GPCM = 0x0};
					enum {ValueMask_GPCM = 0x00000000};
					enum {Value_UPMA = 0x2};
					enum {ValueMask_UPMA = 0x00000080};
					enum {Value_UPMB = 0x3};
					enum {ValueMask_UPMB = 0x000000C0};
					};
				namespace V { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x00000001};
					enum {Value_Invalid = 0x0};
					enum {ValueMask_Invalid = 0x00000000};
					enum {Value_Valid = 0x1};
					enum {ValueMask_Valid = 0x00000001};
					};
				};
			namespace OR { // Register description
				typedef uint32_t	Reg;
				namespace AM { // Field Description
					enum {Lsb = 15};
					enum {FieldMask = 0xFFFF8000};
					enum {Value_Size32K = 0x1FFFF};
					enum {ValueMask_Size32K = 0xFFFF8000};
					enum {Value_Size64K = 0x1FFFE};
					enum {ValueMask_Size64K = 0xFFFF0000};
					enum {Value_Size128K = 0x1FFFC};
					enum {ValueMask_Size128K = 0xFFFE0000};
					enum {Value_Size256K = 0x1FFF8};
					enum {ValueMask_Size256K = 0xFFFC0000};
					enum {Value_Size512K = 0x1FFF0};
					enum {ValueMask_Size512K = 0xFFF80000};
					enum {Value_Size1M = 0x1FFE0};
					enum {ValueMask_Size1M = 0xFFF00000};
					enum {Value_Size2M = 0x1FFC0};
					enum {ValueMask_Size2M = 0xFFE00000};
					enum {Value_Size4M = 0x1FF80};
					enum {ValueMask_Size4M = 0xFFC00000};
					enum {Value_Size8M = 0x1FF00};
					enum {ValueMask_Size8M = 0xFF800000};
					enum {Value_Size16M = 0x1FE00};
					enum {ValueMask_Size16M = 0xFF000000};
					enum {Value_Size32M = 0x1FC00};
					enum {ValueMask_Size32M = 0xFE000000};
					enum {Value_Size64M = 0x1F800};
					enum {ValueMask_Size64M = 0xFC000000};
					enum {Value_Size128M = 0x1F000};
					enum {ValueMask_Size128M = 0xF8000000};
					enum {Value_Size256M = 0x1E000};
					enum {ValueMask_Size256M = 0xF0000000};
					enum {Value_Size512M = 0x1C000};
					enum {ValueMask_Size512M = 0xE0000000};
					enum {Value_Size1G = 0x18000};
					enum {ValueMask_Size1G = 0xC0000000};
					enum {Value_Size2G = 0x10000};
					enum {ValueMask_Size2G = 0x80000000};
					enum {Value_Size4G = 0x0};
					enum {ValueMask_Size4G = 0x00000000};
					};
				namespace ATM { // Field Description
					enum {Lsb = 12};
					enum {FieldMask = 0x00007000};
					enum {Value_Max = 0x7};
					enum {ValueMask_Max = 0x00007000};
					};
				namespace GPCM { // Field Description
					enum {Lsb = 9};
					enum {FieldMask = 0x00000E00};
					namespace CSNT { // Field Description
						enum {Lsb = 11};
						enum {FieldMask = 0x00000800};
						enum {Value_Normal = 0x0};
						enum {ValueMask_Normal = 0x00000000};
						enum {Value_Extended = 0x1};
						enum {ValueMask_Extended = 0x00000800};
						};
					namespace ACS { // Field Description
						enum {Lsb = 9};
						enum {FieldMask = 0x00000600};
						enum {Value_Normal = 0x0};
						enum {ValueMask_Normal = 0x00000000};
						enum {Value_QuarterClock = 0x2};
						enum {ValueMask_QuarterClock = 0x00000400};
						enum {Value_HalfClock = 0x3};
						enum {ValueMask_HalfClock = 0x00000600};
						};
					};
				namespace UPM { // Field Description
					enum {Lsb = 9};
					enum {FieldMask = 0x00000E00};
					namespace SAM { // Field Description
						enum {Lsb = 11};
						enum {FieldMask = 0x00000800};
						enum {Value_Normal = 0x0};
						enum {ValueMask_Normal = 0x00000000};
						enum {Value_Multiplexed = 0x1};
						enum {ValueMask_Multiplexed = 0x00000800};
						};
					namespace GPL5 { // Field Description
						enum {Lsb = 10};
						enum {FieldMask = 0x00000400};
						enum {Value_GPL_B5 = 0x0};
						enum {ValueMask_GPL_B5 = 0x00000000};
						enum {Value_GPL_A5 = 0x1};
						enum {ValueMask_GPL_A5 = 0x00000400};
						};
					namespace GPLS { // Field Description
						enum {Lsb = 9};
						enum {FieldMask = 0x00000200};
						enum {Value_Low = 0x0};
						enum {ValueMask_Low = 0x00000000};
						enum {Value_High = 0x1};
						enum {ValueMask_High = 0x00000200};
						};
					};
				namespace BIH { // Field Description
					enum {Lsb = 8};
					enum {FieldMask = 0x00000100};
					enum {Value_AllowBurst = 0x0};
					enum {ValueMask_AllowBurst = 0x00000000};
					enum {Value_InhibitBurst = 0x1};
					enum {ValueMask_InhibitBurst = 0x00000100};
					};
				namespace SCY { // Field Description
					enum {Lsb = 4};
					enum {FieldMask = 0x000000F0};
					enum {Value_Max = 0xF};
					enum {ValueMask_Max = 0x000000F0};
					};
				namespace SETA { // Field Description
					enum {Lsb = 3};
					enum {FieldMask = 0x00000008};
					enum {Value_InternalTA = 0x0};
					enum {ValueMask_InternalTA = 0x00000000};
					enum {Value_ExternalTA = 0x1};
					enum {ValueMask_ExternalTA = 0x00000008};
					};
				namespace TRLX { // Field Description
					enum {Lsb = 2};
					enum {FieldMask = 0x00000004};
					enum {Value_Normal = 0x0};
					enum {ValueMask_Normal = 0x00000000};
					enum {Value_Relaxed = 0x1};
					enum {ValueMask_Relaxed = 0x00000004};
					};
				namespace EHTR { // Field Description
					enum {Lsb = 1};
					enum {FieldMask = 0x00000002};
					enum {Value_Normal = 0x0};
					enum {ValueMask_Normal = 0x00000000};
					enum {Value_Extended = 0x1};
					enum {ValueMask_Extended = 0x00000002};
					};
				};
			namespace MAR { // Register description
				typedef uint32_t	Reg;
				};
			namespace MCR { // Register description
				typedef uint32_t	Reg;
				};
			namespace MAMR { // Register description
				typedef uint32_t	Reg;
				};
			namespace MBMR { // Register description
				typedef uint32_t	Reg;
				};
			namespace MSTAT { // Register description
				typedef uint16_t	Reg;
				};
			namespace MPTPR { // Register description
				typedef uint16_t	Reg;
				};
			namespace MDR { // Register description
				typedef uint32_t	Reg;
				};
			};
		};
	};
#endif
