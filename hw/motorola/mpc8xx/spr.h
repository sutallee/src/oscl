/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _hw_mot8xx_sprh_
#define _hw_mot8xx_sprh_
#include <stdint.h>
namespace Oscl { // Namespace description
	namespace Mot8xx { // Namespace description
		namespace Spr { // Namespace description
			enum {EIE = 80};
			enum {EID = 81};
			enum {NRI = 82};
			enum {DPIR = 631};
			enum {IMMR = 638};
			enum {IC_CST = 560};
			enum {IC_ADR = 561};
			enum {IC_DAT = 562};
			enum {DC_CST = 568};
			enum {DC_ADR = 569};
			enum {DC_DAT = 570};
			enum {MI_CTR = 784};
			enum {MI_AP = 786};
			enum {MI_EPN = 787};
			enum {MI_TWC = 789};
			enum {MI_L1DL2P = 789};
			enum {MI_RPN = 790};
			enum {MI_CAM = 816};
			enum {MI_RAM0 = 817};
			enum {MI_RAM1 = 818};
			enum {MD_CTR = 792};
			enum {M_CASID = 793};
			enum {MD_AP = 794};
			enum {MD_EPN = 795};
			enum {M_TWB = 796};
			enum {MD_L1P = 796};
			enum {MD_TWC = 797};
			enum {MD_L1DL2P = 797};
			enum {MD_RPN = 798};
			enum {M_TW = 799};
			enum {M_SAVE = 799};
			enum {MD_CAM = 824};
			enum {MD_RAM0 = 825};
			enum {MD_RAM1 = 826};
			};
		};
	};
#endif
