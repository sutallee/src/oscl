/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _hw_mot8xx_sprh_
#define _hw_mot8xx_sprh_
#define OsclMot8xxSprEIE	80
#define OsclMot8xxSprEID	81
#define OsclMot8xxSprNRI	82
#define OsclMot8xxSprDPIR	631
#define OsclMot8xxSprIMMR	638
#define OsclMot8xxSprIC_CST	560
#define OsclMot8xxSprIC_ADR	561
#define OsclMot8xxSprIC_DAT	562
#define OsclMot8xxSprDC_CST	568
#define OsclMot8xxSprDC_ADR	569
#define OsclMot8xxSprDC_DAT	570
#define OsclMot8xxSprMI_CTR	784
#define OsclMot8xxSprMI_AP	786
#define OsclMot8xxSprMI_EPN	787
#define OsclMot8xxSprMI_TWC	789
#define OsclMot8xxSprMI_L1DL2P	789
#define OsclMot8xxSprMI_RPN	790
#define OsclMot8xxSprMI_CAM	816
#define OsclMot8xxSprMI_RAM0	817
#define OsclMot8xxSprMI_RAM1	818
#define OsclMot8xxSprMD_CTR	792
#define OsclMot8xxSprM_CASID	793
#define OsclMot8xxSprMD_AP	794
#define OsclMot8xxSprMD_EPN	795
#define OsclMot8xxSprM_TWB	796
#define OsclMot8xxSprMD_L1P	796
#define OsclMot8xxSprMD_TWC	797
#define OsclMot8xxSprMD_L1DL2P	797
#define OsclMot8xxSprMD_RPN	798
#define OsclMot8xxSprM_TW	799
#define OsclMot8xxSprM_SAVE	799
#define OsclMot8xxSprMD_CAM	824
#define OsclMot8xxSprMD_RAM0	825
#define OsclMot8xxSprMD_RAM1	826
#endif
