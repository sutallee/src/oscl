/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _hw_mot8xx_clkrstregh_
#define _hw_mot8xx_clkrstregh_
#include <stdint.h>
namespace Oscl { // Namespace description
	namespace Mot8xx { // Namespace description
		namespace ClkRst { // Namespace description
			namespace SCCR { // Register description
				typedef uint32_t	Reg;
				namespace COM { // Field Description
					enum {Lsb = 29};
					enum {FieldMask = 0x60000000};
					enum {Value_FullStrength = 0x0};
					enum {ValueMask_FullStrength = 0x00000000};
					enum {Value_HalfStrength = 0x1};
					enum {ValueMask_HalfStrength = 0x20000000};
					enum {Value_Disable = 0x3};
					enum {ValueMask_Disable = 0x60000000};
					};
				namespace TBS { // Field Description
					enum {Lsb = 25};
					enum {FieldMask = 0x02000000};
					enum {Value_OscClkDiv = 0x0};
					enum {ValueMask_OscClkDiv = 0x00000000};
					enum {Value_Gclk2Div16 = 0x1};
					enum {ValueMask_Gclk2Div16 = 0x02000000};
					};
				namespace RTDIV { // Field Description
					enum {Lsb = 24};
					enum {FieldMask = 0x01000000};
					enum {Value_DivideBy4 = 0x0};
					enum {ValueMask_DivideBy4 = 0x00000000};
					enum {Value_DivideBy512 = 0x1};
					enum {ValueMask_DivideBy512 = 0x01000000};
					};
				namespace RTSEL { // Field Description
					enum {Lsb = 23};
					enum {FieldMask = 0x00800000};
					enum {Value_OSCM = 0x0};
					enum {ValueMask_OSCM = 0x00000000};
					enum {Value_EXTCLK = 0x1};
					enum {ValueMask_EXTCLK = 0x00800000};
					};
				namespace CRQEN { // Field Description
					enum {Lsb = 22};
					enum {FieldMask = 0x00400000};
					enum {Value_LowFrequency = 0x0};
					enum {ValueMask_LowFrequency = 0x00000000};
					enum {Value_HighFrequency = 0x1};
					enum {ValueMask_HighFrequency = 0x00400000};
					};
				namespace PRQEN { // Field Description
					enum {Lsb = 21};
					enum {FieldMask = 0x00200000};
					enum {Value_LowFrequency = 0x0};
					enum {ValueMask_LowFrequency = 0x00000000};
					enum {Value_HighFrequency = 0x1};
					enum {ValueMask_HighFrequency = 0x00200000};
					};
				namespace EBDF { // Field Description
					enum {Lsb = 17};
					enum {FieldMask = 0x00020000};
					enum {Value_DivideBy1 = 0x0};
					enum {ValueMask_DivideBy1 = 0x00000000};
					enum {Value_DivideBy2 = 0x1};
					enum {ValueMask_DivideBy2 = 0x00020000};
					};
				namespace DFSYNC { // Field Description
					enum {Lsb = 14};
					enum {FieldMask = 0x0000C000};
					enum {Value_DivideBy1 = 0x0};
					enum {ValueMask_DivideBy1 = 0x00000000};
					enum {Value_DivideBy4 = 0x1};
					enum {ValueMask_DivideBy4 = 0x00004000};
					enum {Value_DivideBy16 = 0x2};
					enum {ValueMask_DivideBy16 = 0x00008000};
					enum {Value_DivideBy64 = 0x3};
					enum {ValueMask_DivideBy64 = 0x0000C000};
					};
				namespace DFBRG { // Field Description
					enum {Lsb = 11};
					enum {FieldMask = 0x00001800};
					enum {Value_DivideBy1 = 0x0};
					enum {ValueMask_DivideBy1 = 0x00000000};
					enum {Value_DivideBy4 = 0x1};
					enum {ValueMask_DivideBy4 = 0x00000800};
					enum {Value_DivideBy16 = 0x2};
					enum {ValueMask_DivideBy16 = 0x00001000};
					enum {Value_DivideBy64 = 0x3};
					enum {ValueMask_DivideBy64 = 0x00001800};
					};
				namespace DFNL { // Field Description
					enum {Lsb = 8};
					enum {FieldMask = 0x00000700};
					enum {Value_DivideBy2 = 0x0};
					enum {ValueMask_DivideBy2 = 0x00000000};
					enum {Value_DivideBy4 = 0x1};
					enum {ValueMask_DivideBy4 = 0x00000100};
					enum {Value_DivideBy8 = 0x2};
					enum {ValueMask_DivideBy8 = 0x00000200};
					enum {Value_DivideBy16 = 0x3};
					enum {ValueMask_DivideBy16 = 0x00000300};
					enum {Value_DivideBy32 = 0x4};
					enum {ValueMask_DivideBy32 = 0x00000400};
					enum {Value_DivideBy64 = 0x5};
					enum {ValueMask_DivideBy64 = 0x00000500};
					enum {Value_DivideBy256 = 0x7};
					enum {ValueMask_DivideBy256 = 0x00000700};
					};
				namespace DFNH { // Field Description
					enum {Lsb = 5};
					enum {FieldMask = 0x000000E0};
					enum {Value_DivideBy1 = 0x0};
					enum {ValueMask_DivideBy1 = 0x00000000};
					enum {Value_DivideBy2 = 0x0};
					enum {ValueMask_DivideBy2 = 0x00000000};
					enum {Value_DivideBy4 = 0x2};
					enum {ValueMask_DivideBy4 = 0x00000040};
					enum {Value_DivideBy8 = 0x3};
					enum {ValueMask_DivideBy8 = 0x00000060};
					enum {Value_DivideBy16 = 0x4};
					enum {ValueMask_DivideBy16 = 0x00000080};
					enum {Value_DivideBy32 = 0x5};
					enum {ValueMask_DivideBy32 = 0x000000A0};
					enum {Value_DivideBy64 = 0x6};
					enum {ValueMask_DivideBy64 = 0x000000C0};
					};
				};
			namespace PLPRCR { // Register description
				typedef uint32_t	Reg;
				namespace MF { // Field Description
					enum {Lsb = 20};
					enum {FieldMask = 0xFFF00000};
					enum {Value_Max = 0xFFF};
					enum {ValueMask_Max = 0xFFF00000};
					};
				namespace SPLSS { // Field Description
					enum {Lsb = 15};
					enum {FieldMask = 0x00008000};
					enum {Value_Locked = 0x0};
					enum {ValueMask_Locked = 0x00000000};
					enum {Value_LockLost = 0x1};
					enum {ValueMask_LockLost = 0x00008000};
					enum {Value_Clear = 0x1};
					enum {ValueMask_Clear = 0x00008000};
					};
				namespace TEXPS { // Field Description
					enum {Lsb = 14};
					enum {FieldMask = 0x00004000};
					enum {Value_Negated = 0x0};
					enum {ValueMask_Negated = 0x00000000};
					enum {Value_Asserted = 0x1};
					enum {ValueMask_Asserted = 0x00004000};
					enum {Value_Clear = 0x1};
					enum {ValueMask_Clear = 0x00004000};
					};
				namespace TMIST { // Field Description
					enum {Lsb = 12};
					enum {FieldMask = 0x00001000};
					enum {Value_NotPending = 0x0};
					enum {ValueMask_NotPending = 0x00000000};
					enum {Value_Pending = 0x1};
					enum {ValueMask_Pending = 0x00001000};
					enum {Value_Clear = 0x1};
					enum {ValueMask_Clear = 0x00001000};
					};
				namespace CSRC { // Field Description
					enum {Lsb = 10};
					enum {FieldMask = 0x00000400};
					enum {Value_DFNH = 0x0};
					enum {ValueMask_DFNH = 0x00000000};
					enum {Value_DFNL = 0x1};
					enum {ValueMask_DFNL = 0x00000400};
					};
				namespace LPM { // Field Description
					enum {Lsb = 8};
					enum {FieldMask = 0x00000300};
					enum {Value_NormalHighNormalLow = 0x0};
					enum {ValueMask_NormalHighNormalLow = 0x00000000};
					enum {Value_DozeHighDozeLow = 0x1};
					enum {ValueMask_DozeHighDozeLow = 0x00000100};
					enum {Value_Sleep = 0x2};
					enum {ValueMask_Sleep = 0x00000200};
					enum {Value_DeepSleepPowerDown = 0x3};
					enum {ValueMask_DeepSleepPowerDown = 0x00000300};
					};
				namespace CSR { // Field Description
					enum {Lsb = 7};
					enum {FieldMask = 0x00000080};
					enum {Value_Disable = 0x0};
					enum {ValueMask_Disable = 0x00000000};
					enum {Value_Enable = 0x1};
					enum {ValueMask_Enable = 0x00000080};
					};
				namespace LOLRE { // Field Description
					enum {Lsb = 6};
					enum {FieldMask = 0x00000040};
					enum {Value_Disable = 0x0};
					enum {ValueMask_Disable = 0x00000000};
					enum {Value_Enable = 0x1};
					enum {ValueMask_Enable = 0x00000040};
					};
				namespace FIOPD { // Field Description
					enum {Lsb = 5};
					enum {FieldMask = 0x00000020};
					enum {Value_NoPullDown = 0x0};
					enum {ValueMask_NoPullDown = 0x00000000};
					enum {Value_PullDown = 0x1};
					enum {ValueMask_PullDown = 0x00000020};
					};
				};
			namespace RSR { // Register description
				typedef uint32_t	Reg;
				namespace EHRS { // Field Description
					enum {Lsb = 31};
					enum {FieldMask = 0x80000000};
					enum {Value_NotResetCause = 0x0};
					enum {ValueMask_NotResetCause = 0x00000000};
					enum {Value_ResetCause = 0x1};
					enum {ValueMask_ResetCause = 0x80000000};
					enum {Value_Clear = 0x1};
					enum {ValueMask_Clear = 0x80000000};
					};
				namespace ESRS { // Field Description
					enum {Lsb = 30};
					enum {FieldMask = 0x40000000};
					enum {Value_NotResetCause = 0x0};
					enum {ValueMask_NotResetCause = 0x00000000};
					enum {Value_ResetCause = 0x1};
					enum {ValueMask_ResetCause = 0x40000000};
					enum {Value_Clear = 0x1};
					enum {ValueMask_Clear = 0x40000000};
					};
				namespace LLRS { // Field Description
					enum {Lsb = 29};
					enum {FieldMask = 0x20000000};
					enum {Value_NotResetCause = 0x0};
					enum {ValueMask_NotResetCause = 0x00000000};
					enum {Value_ResetCause = 0x1};
					enum {ValueMask_ResetCause = 0x20000000};
					enum {Value_Clear = 0x1};
					enum {ValueMask_Clear = 0x20000000};
					};
				namespace SWRS { // Field Description
					enum {Lsb = 28};
					enum {FieldMask = 0x10000000};
					enum {Value_NotResetCause = 0x0};
					enum {ValueMask_NotResetCause = 0x00000000};
					enum {Value_ResetCause = 0x1};
					enum {ValueMask_ResetCause = 0x10000000};
					enum {Value_Clear = 0x1};
					enum {ValueMask_Clear = 0x10000000};
					};
				namespace CSRS { // Field Description
					enum {Lsb = 27};
					enum {FieldMask = 0x08000000};
					enum {Value_NotResetCause = 0x0};
					enum {ValueMask_NotResetCause = 0x00000000};
					enum {Value_ResetCause = 0x1};
					enum {ValueMask_ResetCause = 0x08000000};
					enum {Value_Clear = 0x1};
					enum {ValueMask_Clear = 0x08000000};
					};
				namespace DBHRS { // Field Description
					enum {Lsb = 26};
					enum {FieldMask = 0x04000000};
					enum {Value_NotResetCause = 0x0};
					enum {ValueMask_NotResetCause = 0x00000000};
					enum {Value_ResetCause = 0x1};
					enum {ValueMask_ResetCause = 0x04000000};
					enum {Value_Clear = 0x1};
					enum {ValueMask_Clear = 0x04000000};
					};
				namespace DBSRS { // Field Description
					enum {Lsb = 25};
					enum {FieldMask = 0x02000000};
					enum {Value_NotResetCause = 0x0};
					enum {ValueMask_NotResetCause = 0x00000000};
					enum {Value_ResetCause = 0x1};
					enum {ValueMask_ResetCause = 0x02000000};
					enum {Value_Clear = 0x1};
					enum {ValueMask_Clear = 0x02000000};
					};
				namespace JTRS { // Field Description
					enum {Lsb = 24};
					enum {FieldMask = 0x01000000};
					enum {Value_NotResetCause = 0x0};
					enum {ValueMask_NotResetCause = 0x00000000};
					enum {Value_ResetCause = 0x1};
					enum {ValueMask_ResetCause = 0x01000000};
					enum {Value_Clear = 0x1};
					enum {ValueMask_Clear = 0x01000000};
					};
				};
			};
		};
	};
#endif
