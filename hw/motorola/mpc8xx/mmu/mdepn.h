/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _hw_mot8xx_mdepnh_
#define _hw_mot8xx_mdepnh_
#include <stdint.h>
namespace Oscl { // Namespace description
	namespace Mot8xx { // Namespace description
		namespace Mmu { // Namespace description
			namespace MdEpn { // Register description
				typedef uint32_t	Reg;
				namespace EPN { // Field Description
					enum {Lsb = 12};
					enum {FieldMask = 0xFFFFF000};
					};
				namespace EV { // Field Description
					enum {Lsb = 9};
					enum {FieldMask = 0x00000200};
					enum {Value_TlbEntryInvalid = 0x0};
					enum {ValueMask_TlbEntryInvalid = 0x00000000};
					enum {Value_TlbEntryValid = 0x1};
					enum {ValueMask_TlbEntryValid = 0x00000200};
					};
				namespace ASID { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x0000000F};
					enum {Value_Maximum = 0xF};
					enum {ValueMask_Maximum = 0x0000000F};
					enum {Value_Minimum = 0x0};
					enum {ValueMask_Minimum = 0x00000000};
					};
				};
			};
		};
	};
#endif
