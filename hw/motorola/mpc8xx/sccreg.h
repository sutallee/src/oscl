/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _hw_mot8xx_sccregh_
#define _hw_mot8xx_sccregh_
#include <stdint.h>
namespace Oscl { // Namespace description
	namespace Mot8xx { // Namespace description
		namespace Scc { // Namespace description
			namespace GSMR_L { // Register description
				typedef uint32_t	Reg;
				namespace SIR { // Field Description
					enum {Lsb = 31};
					enum {FieldMask = 0x80000000};
					enum {Value_Normal = 0x0};
					enum {ValueMask_Normal = 0x00000000};
					enum {Value_SerialInfrared = 0x1};
					enum {ValueMask_SerialInfrared = 0x80000000};
					};
				namespace EDGE { // Field Description
					enum {Lsb = 29};
					enum {FieldMask = 0x60000000};
					enum {Value_BothEdges = 0x0};
					enum {ValueMask_BothEdges = 0x00000000};
					enum {Value_PositiveEdge = 0x1};
					enum {ValueMask_PositiveEdge = 0x20000000};
					enum {Value_NegativeEdge = 0x2};
					enum {ValueMask_NegativeEdge = 0x40000000};
					};
				namespace TCI { // Field Description
					enum {Lsb = 28};
					enum {FieldMask = 0x10000000};
					enum {Value_Normal = 0x0};
					enum {ValueMask_Normal = 0x00000000};
					enum {Value_Invert = 0x1};
					enum {ValueMask_Invert = 0x10000000};
					};
				namespace TSNC { // Field Description
					enum {Lsb = 26};
					enum {FieldMask = 0x0C000000};
					enum {Value_Infinite = 0x0};
					enum {ValueMask_Infinite = 0x00000000};
					enum {Value_Rdcr14or6_5 = 0x1};
					enum {ValueMask_Rdcr14or6_5 = 0x04000000};
					enum {Value_Rdcr4or1_5 = 0x2};
					enum {ValueMask_Rdcr4or1_5 = 0x08000000};
					enum {Value_Rdcr3or1 = 0x3};
					enum {ValueMask_Rdcr3or1 = 0x0C000000};
					};
				namespace RINV { // Field Description
					enum {Lsb = 25};
					enum {FieldMask = 0x02000000};
					enum {Value_Normal = 0x0};
					enum {ValueMask_Normal = 0x00000000};
					enum {Value_InvertData = 0x1};
					enum {ValueMask_InvertData = 0x02000000};
					};
				namespace TINV { // Field Description
					enum {Lsb = 24};
					enum {FieldMask = 0x01000000};
					enum {Value_Normal = 0x0};
					enum {ValueMask_Normal = 0x00000000};
					enum {Value_InvertData = 0x1};
					enum {ValueMask_InvertData = 0x01000000};
					};
				namespace TPL { // Field Description
					enum {Lsb = 21};
					enum {FieldMask = 0x00E00000};
					enum {Value_PreambleNone = 0x0};
					enum {ValueMask_PreambleNone = 0x00000000};
					enum {Value_Preamble8Bits = 0x1};
					enum {ValueMask_Preamble8Bits = 0x00200000};
					enum {Value_Preamble16Bits = 0x2};
					enum {ValueMask_Preamble16Bits = 0x00400000};
					enum {Value_Preamble32Bits = 0x3};
					enum {ValueMask_Preamble32Bits = 0x00600000};
					enum {Value_Preamble48Bits = 0x4};
					enum {ValueMask_Preamble48Bits = 0x00800000};
					enum {Value_Preamble64Bits = 0x5};
					enum {ValueMask_Preamble64Bits = 0x00A00000};
					enum {Value_Preamble128Bits = 0x6};
					enum {ValueMask_Preamble128Bits = 0x00C00000};
					};
				namespace TPP { // Field Description
					enum {Lsb = 19};
					enum {FieldMask = 0x00180000};
					enum {Value_AllZeros = 0x0};
					enum {ValueMask_AllZeros = 0x00000000};
					enum {Value_Repetitive10 = 0x1};
					enum {ValueMask_Repetitive10 = 0x00080000};
					enum {Value_Repetitive01 = 0x2};
					enum {ValueMask_Repetitive01 = 0x00100000};
					enum {Value_AllOnes = 0x3};
					enum {ValueMask_AllOnes = 0x00180000};
					};
				namespace TEND { // Field Description
					enum {Lsb = 18};
					enum {FieldMask = 0x00040000};
					enum {Value_DataOnly = 0x0};
					enum {ValueMask_DataOnly = 0x00000000};
					enum {Value_DataAndIdles = 0x1};
					enum {ValueMask_DataAndIdles = 0x00040000};
					};
				namespace TDCR { // Field Description
					enum {Lsb = 16};
					enum {FieldMask = 0x00030000};
					enum {Value_Clock1X = 0x0};
					enum {ValueMask_Clock1X = 0x00000000};
					enum {Value_Clock8X = 0x1};
					enum {ValueMask_Clock8X = 0x00010000};
					enum {Value_Clock16X = 0x2};
					enum {ValueMask_Clock16X = 0x00020000};
					enum {Value_Clock32X = 0x3};
					enum {ValueMask_Clock32X = 0x00030000};
					};
				namespace RDCR { // Field Description
					enum {Lsb = 14};
					enum {FieldMask = 0x0000C000};
					enum {Value_Clock1X = 0x0};
					enum {ValueMask_Clock1X = 0x00000000};
					enum {Value_Clock8X = 0x1};
					enum {ValueMask_Clock8X = 0x00004000};
					enum {Value_Clock16X = 0x2};
					enum {ValueMask_Clock16X = 0x00008000};
					enum {Value_Clock32X = 0x3};
					enum {ValueMask_Clock32X = 0x0000C000};
					};
				namespace RENC { // Field Description
					enum {Lsb = 11};
					enum {FieldMask = 0x00003800};
					enum {Value_NRZ = 0x0};
					enum {ValueMask_NRZ = 0x00000000};
					enum {Value_NRZI = 0x1};
					enum {ValueMask_NRZI = 0x00000800};
					enum {Value_FM0 = 0x2};
					enum {ValueMask_FM0 = 0x00001000};
					enum {Value_Manchester = 0x4};
					enum {ValueMask_Manchester = 0x00002000};
					enum {Value_DiffManchester = 0x6};
					enum {ValueMask_DiffManchester = 0x00003000};
					};
				namespace TENC { // Field Description
					enum {Lsb = 8};
					enum {FieldMask = 0x00000700};
					enum {Value_NRZ = 0x0};
					enum {ValueMask_NRZ = 0x00000000};
					enum {Value_NRZI = 0x1};
					enum {ValueMask_NRZI = 0x00000100};
					enum {Value_FM0 = 0x2};
					enum {ValueMask_FM0 = 0x00000200};
					enum {Value_Manchester = 0x4};
					enum {ValueMask_Manchester = 0x00000400};
					enum {Value_DiffManchester = 0x6};
					enum {ValueMask_DiffManchester = 0x00000600};
					};
				namespace DIAG { // Field Description
					enum {Lsb = 6};
					enum {FieldMask = 0x000000C0};
					enum {Value_Normal = 0x0};
					enum {ValueMask_Normal = 0x00000000};
					enum {Value_LocalLoop = 0x1};
					enum {ValueMask_LocalLoop = 0x00000040};
					enum {Value_AutoEcho = 0x2};
					enum {ValueMask_AutoEcho = 0x00000080};
					enum {Value_LoopAndEcho = 0x3};
					enum {ValueMask_LoopAndEcho = 0x000000C0};
					};
				namespace ENR { // Field Description
					enum {Lsb = 5};
					enum {FieldMask = 0x00000020};
					enum {Value_Disable = 0x0};
					enum {ValueMask_Disable = 0x00000000};
					enum {Value_Enable = 0x1};
					enum {ValueMask_Enable = 0x00000020};
					};
				namespace ENT { // Field Description
					enum {Lsb = 4};
					enum {FieldMask = 0x00000010};
					enum {Value_Disable = 0x0};
					enum {ValueMask_Disable = 0x00000000};
					enum {Value_Enable = 0x1};
					enum {ValueMask_Enable = 0x00000010};
					};
				namespace MODE { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x0000000F};
					enum {Value_HDLC = 0x0};
					enum {ValueMask_HDLC = 0x00000000};
					enum {Value_LocalTalk = 0x2};
					enum {ValueMask_LocalTalk = 0x00000002};
					enum {Value_SS7 = 0x3};
					enum {ValueMask_SS7 = 0x00000003};
					enum {Value_UART = 0x4};
					enum {ValueMask_UART = 0x00000004};
					enum {Value_Profibus = 0x5};
					enum {ValueMask_Profibus = 0x00000005};
					enum {Value_AHDLC = 0x6};
					enum {ValueMask_AHDLC = 0x00000006};
					enum {Value_IrDA = 0x6};
					enum {ValueMask_IrDA = 0x00000006};
					enum {Value_V_14 = 0x7};
					enum {ValueMask_V_14 = 0x00000007};
					enum {Value_BISYNC = 0x8};
					enum {ValueMask_BISYNC = 0x00000008};
					enum {Value_DDCMP = 0x9};
					enum {ValueMask_DDCMP = 0x00000009};
					enum {Value_ENET = 0xC};
					enum {ValueMask_ENET = 0x0000000C};
					};
				};
			namespace GSMR_H { // Register description
				typedef uint32_t	Reg;
				namespace IRP { // Field Description
					enum {Lsb = 18};
					enum {FieldMask = 0x00040000};
					enum {Value_ActiveHigh = 0x0};
					enum {ValueMask_ActiveHigh = 0x00000000};
					enum {Value_ActiveLow = 0x1};
					enum {ValueMask_ActiveLow = 0x00040000};
					};
				namespace GDE { // Field Description
					enum {Lsb = 16};
					enum {FieldMask = 0x00010000};
					enum {Value_NoGlitchDetect = 0x0};
					enum {ValueMask_NoGlitchDetect = 0x00000000};
					enum {Value_GlitchDetect = 0x1};
					enum {ValueMask_GlitchDetect = 0x00010000};
					};
				namespace TCRC { // Field Description
					enum {Lsb = 14};
					enum {FieldMask = 0x0000C000};
					enum {Value_NonTransparent = 0x0};
					enum {ValueMask_NonTransparent = 0x00000000};
					enum {Value_CcittCrc16 = 0x0};
					enum {ValueMask_CcittCrc16 = 0x00000000};
					enum {Value_Hdlc16 = 0x0};
					enum {ValueMask_Hdlc16 = 0x00000000};
					enum {Value_Crc16 = 0x1};
					enum {ValueMask_Crc16 = 0x00004000};
					enum {Value_Bisync = 0x1};
					enum {ValueMask_Bisync = 0x00004000};
					enum {Value_CcittCrc32 = 0x2};
					enum {ValueMask_CcittCrc32 = 0x00008000};
					enum {Value_Hdlc32 = 0x2};
					enum {ValueMask_Hdlc32 = 0x00008000};
					enum {Value_ENET = 0x2};
					enum {ValueMask_ENET = 0x00008000};
					};
				namespace REVD { // Field Description
					enum {Lsb = 13};
					enum {FieldMask = 0x00002000};
					enum {Value_Normal = 0x0};
					enum {ValueMask_Normal = 0x00000000};
					enum {Value_ReverseData = 0x1};
					enum {ValueMask_ReverseData = 0x00002000};
					};
				namespace TRX { // Field Description
					enum {Lsb = 12};
					enum {FieldMask = 0x00001000};
					enum {Value_Normal = 0x0};
					enum {ValueMask_Normal = 0x00000000};
					enum {Value_Transparent = 0x0};
					enum {ValueMask_Transparent = 0x00000000};
					};
				namespace TTX { // Field Description
					enum {Lsb = 11};
					enum {FieldMask = 0x00000800};
					enum {Value_Normal = 0x0};
					enum {ValueMask_Normal = 0x00000000};
					enum {Value_Transparent = 0x0};
					enum {ValueMask_Transparent = 0x00000000};
					};
				namespace CDP { // Field Description
					enum {Lsb = 10};
					enum {FieldMask = 0x00000400};
					enum {Value_Normal = 0x0};
					enum {ValueMask_Normal = 0x00000000};
					enum {Value_Pulse = 0x1};
					enum {ValueMask_Pulse = 0x00000400};
					};
				namespace CTSP { // Field Description
					enum {Lsb = 9};
					enum {FieldMask = 0x00000200};
					enum {Value_Normal = 0x0};
					enum {ValueMask_Normal = 0x00000000};
					enum {Value_Pulse = 0x1};
					enum {ValueMask_Pulse = 0x00000200};
					};
				namespace CDS { // Field Description
					enum {Lsb = 8};
					enum {FieldMask = 0x00000100};
					enum {Value_Asynchronous = 0x0};
					enum {ValueMask_Asynchronous = 0x00000000};
					enum {Value_Synchronous = 0x1};
					enum {ValueMask_Synchronous = 0x00000100};
					};
				namespace CTSS { // Field Description
					enum {Lsb = 7};
					enum {FieldMask = 0x00000080};
					enum {Value_Asynchronous = 0x0};
					enum {ValueMask_Asynchronous = 0x00000000};
					enum {Value_Synchronous = 0x1};
					enum {ValueMask_Synchronous = 0x00000080};
					};
				namespace TFL { // Field Description
					enum {Lsb = 6};
					enum {FieldMask = 0x00000040};
					enum {Value_Normal = 0x0};
					enum {ValueMask_Normal = 0x00000000};
					enum {Value_TxFifo32Bytes = 0x0};
					enum {ValueMask_TxFifo32Bytes = 0x00000000};
					enum {Value_TxFifo1Byte = 0x1};
					enum {ValueMask_TxFifo1Byte = 0x00000040};
					};
				namespace RFW { // Field Description
					enum {Lsb = 5};
					enum {FieldMask = 0x00000020};
					enum {Value_WideFifo = 0x0};
					enum {ValueMask_WideFifo = 0x00000000};
					enum {Value_NarrowFifo = 0x1};
					enum {ValueMask_NarrowFifo = 0x00000020};
					};
				namespace TXSY { // Field Description
					enum {Lsb = 4};
					enum {FieldMask = 0x00000010};
					enum {Value_TxRxNotSynchronized = 0x0};
					enum {ValueMask_TxRxNotSynchronized = 0x00000000};
					enum {Value_TxRxtSynchronized = 0x1};
					enum {ValueMask_TxRxtSynchronized = 0x00000010};
					};
				namespace SYNL { // Field Description
					enum {Lsb = 2};
					enum {FieldMask = 0x0000000C};
					enum {Value_SyncExternal = 0x0};
					enum {ValueMask_SyncExternal = 0x00000000};
					enum {Value_Sync4Bit = 0x1};
					enum {ValueMask_Sync4Bit = 0x00000004};
					enum {Value_Sync8Bit = 0x2};
					enum {ValueMask_Sync8Bit = 0x00000008};
					enum {Value_Sync16Bit = 0x3};
					enum {ValueMask_Sync16Bit = 0x0000000C};
					};
				namespace RTSM { // Field Description
					enum {Lsb = 1};
					enum {FieldMask = 0x00000002};
					enum {Value_IdleBetweenFrames = 0x0};
					enum {ValueMask_IdleBetweenFrames = 0x00000000};
					enum {Value_FlagsBetweenFrames = 0x1};
					enum {ValueMask_FlagsBetweenFrames = 0x00000002};
					};
				namespace RSYN { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x00000001};
					enum {Value_Normal = 0x0};
					enum {ValueMask_Normal = 0x00000000};
					enum {Value_DelayCdAssertion = 0x1};
					enum {ValueMask_DelayCdAssertion = 0x00000001};
					};
				};
			namespace PSMR { // Register description
				typedef uint16_t	Reg;
				namespace HDLC { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x0000FFFF};
					namespace NOF { // Field Description
						enum {Lsb = 12};
						enum {FieldMask = 0x0000F000};
						enum {Value_NoFlags = 0x0};
						enum {ValueMask_NoFlags = 0x00000000};
						enum {Value_Max = 0xF};
						enum {ValueMask_Max = 0x0000F000};
						};
					namespace CRC { // Field Description
						enum {Lsb = 10};
						enum {FieldMask = 0x00000C00};
						enum {Value_CcittCrc16 = 0x0};
						enum {ValueMask_CcittCrc16 = 0x00000000};
						enum {Value_CcittCrc32 = 0x2};
						enum {ValueMask_CcittCrc32 = 0x00000800};
						};
					namespace RTE { // Field Description
						enum {Lsb = 9};
						enum {FieldMask = 0x00000200};
						enum {Value_NoRetransmit = 0x0};
						enum {ValueMask_NoRetransmit = 0x00000000};
						enum {Value_AutoRetransmit = 0x1};
						enum {ValueMask_AutoRetransmit = 0x00000200};
						};
					namespace FSE { // Field Description
						enum {Lsb = 7};
						enum {FieldMask = 0x00000080};
						enum {Value_Normal = 0x0};
						enum {ValueMask_Normal = 0x00000000};
						enum {Value_SharedFlag = 0x1};
						enum {ValueMask_SharedFlag = 0x00000080};
						};
					namespace DRT { // Field Description
						enum {Lsb = 6};
						enum {FieldMask = 0x00000040};
						enum {Value_Normal = 0x0};
						enum {ValueMask_Normal = 0x00000000};
						enum {Value_DisableRxWhileTx = 0x1};
						enum {ValueMask_DisableRxWhileTx = 0x00000040};
						};
					namespace BUS { // Field Description
						enum {Lsb = 5};
						enum {FieldMask = 0x00000020};
						enum {Value_Normal = 0x0};
						enum {ValueMask_Normal = 0x00000000};
						enum {Value_BusMode = 0x1};
						enum {ValueMask_BusMode = 0x00000020};
						};
					namespace BRM { // Field Description
						enum {Lsb = 4};
						enum {FieldMask = 0x00000010};
						enum {Value_Normal = 0x0};
						enum {ValueMask_Normal = 0x00000000};
						enum {Value_DelayRtsOneBit = 0x1};
						enum {ValueMask_DelayRtsOneBit = 0x00000010};
						};
					namespace MFF { // Field Description
						enum {Lsb = 3};
						enum {FieldMask = 0x00000008};
						enum {Value_Normal = 0x0};
						enum {ValueMask_Normal = 0x00000000};
						enum {Value_TxFifoMayHoldMultipleFrames = 0x1};
						enum {ValueMask_TxFifoMayHoldMultipleFrames = 0x00000008};
						};
					};
				namespace LocalTalk { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x0000FFFF};
					namespace NOF { // Field Description
						enum {Lsb = 12};
						enum {FieldMask = 0x0000F000};
						enum {Value_NoFlags = 0x0};
						enum {ValueMask_NoFlags = 0x00000000};
						enum {Value_Max = 0xF};
						enum {ValueMask_Max = 0x0000F000};
						};
					namespace CRC { // Field Description
						enum {Lsb = 10};
						enum {FieldMask = 0x00000C00};
						enum {Value_CcittCrc16 = 0x0};
						enum {ValueMask_CcittCrc16 = 0x00000000};
						enum {Value_CcittCrc32 = 0x2};
						enum {ValueMask_CcittCrc32 = 0x00000800};
						};
					namespace RTE { // Field Description
						enum {Lsb = 9};
						enum {FieldMask = 0x00000200};
						enum {Value_NoRetransmit = 0x0};
						enum {ValueMask_NoRetransmit = 0x00000000};
						enum {Value_AutoRetransmit = 0x1};
						enum {ValueMask_AutoRetransmit = 0x00000200};
						};
					namespace FSE { // Field Description
						enum {Lsb = 7};
						enum {FieldMask = 0x00000080};
						enum {Value_Normal = 0x0};
						enum {ValueMask_Normal = 0x00000000};
						enum {Value_SharedFlag = 0x1};
						enum {ValueMask_SharedFlag = 0x00000080};
						};
					namespace DRT { // Field Description
						enum {Lsb = 6};
						enum {FieldMask = 0x00000040};
						enum {Value_Normal = 0x0};
						enum {ValueMask_Normal = 0x00000000};
						enum {Value_DisableRxWhileTx = 0x1};
						enum {ValueMask_DisableRxWhileTx = 0x00000040};
						};
					namespace BUS { // Field Description
						enum {Lsb = 5};
						enum {FieldMask = 0x00000020};
						enum {Value_Normal = 0x0};
						enum {ValueMask_Normal = 0x00000000};
						enum {Value_BusMode = 0x1};
						enum {ValueMask_BusMode = 0x00000020};
						};
					namespace BRM { // Field Description
						enum {Lsb = 4};
						enum {FieldMask = 0x00000010};
						enum {Value_Normal = 0x0};
						enum {ValueMask_Normal = 0x00000000};
						enum {Value_DelayRtsOneBit = 0x1};
						enum {ValueMask_DelayRtsOneBit = 0x00000010};
						};
					namespace MFF { // Field Description
						enum {Lsb = 3};
						enum {FieldMask = 0x00000008};
						enum {Value_Normal = 0x0};
						enum {ValueMask_Normal = 0x00000000};
						enum {Value_TxFifoMayHoldMultipleFrames = 0x1};
						enum {ValueMask_TxFifoMayHoldMultipleFrames = 0x00000008};
						};
					};
				namespace SS7 { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x0000FFFF};
					};
				namespace UART { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x0000FFFF};
					namespace FLC { // Field Description
						enum {Lsb = 15};
						enum {FieldMask = 0x00008000};
						enum {Value_Normal = 0x0};
						enum {ValueMask_Normal = 0x00000000};
						enum {Value_Asynchronous = 0x1};
						enum {ValueMask_Asynchronous = 0x00008000};
						};
					namespace SL { // Field Description
						enum {Lsb = 14};
						enum {FieldMask = 0x00004000};
						enum {Value_OneStopBit = 0x0};
						enum {ValueMask_OneStopBit = 0x00000000};
						enum {Value_TwoStopBits = 0x1};
						enum {ValueMask_TwoStopBits = 0x00004000};
						};
					namespace CL { // Field Description
						enum {Lsb = 12};
						enum {FieldMask = 0x00003000};
						enum {Value_Data5Bits = 0x0};
						enum {ValueMask_Data5Bits = 0x00000000};
						enum {Value_Data6Bits = 0x1};
						enum {ValueMask_Data6Bits = 0x00001000};
						enum {Value_Data7Bits = 0x2};
						enum {ValueMask_Data7Bits = 0x00002000};
						enum {Value_Data8Bits = 0x3};
						enum {ValueMask_Data8Bits = 0x00003000};
						};
					namespace UM { // Field Description
						enum {Lsb = 10};
						enum {FieldMask = 0x00000C00};
						enum {Value_Normal = 0x0};
						enum {ValueMask_Normal = 0x00000000};
						enum {Value_ManualMultidrop = 0x1};
						enum {ValueMask_ManualMultidrop = 0x00000400};
						enum {Value_AutoMultidrop = 0x3};
						enum {ValueMask_AutoMultidrop = 0x00000C00};
						};
					namespace FRZ { // Field Description
						enum {Lsb = 9};
						enum {FieldMask = 0x00000200};
						enum {Value_Character = 0x0};
						enum {ValueMask_Character = 0x00000000};
						enum {Value_Fifo = 0x1};
						enum {ValueMask_Fifo = 0x00000200};
						};
					namespace RZS { // Field Description
						enum {Lsb = 8};
						enum {FieldMask = 0x00000100};
						enum {Value_StopBitRequired = 0x0};
						enum {ValueMask_StopBitRequired = 0x00000000};
						enum {Value_StopBitOptional = 0x1};
						enum {ValueMask_StopBitOptional = 0x00000100};
						};
					namespace SYN { // Field Description
						enum {Lsb = 7};
						enum {FieldMask = 0x00000080};
						enum {Value_Oversampled = 0x0};
						enum {ValueMask_Oversampled = 0x00000000};
						enum {Value_Synchronous = 0x1};
						enum {ValueMask_Synchronous = 0x00000080};
						};
					namespace DRT { // Field Description
						enum {Lsb = 6};
						enum {FieldMask = 0x00000040};
						enum {Value_Normal = 0x0};
						enum {ValueMask_Normal = 0x00000000};
						enum {Value_DisableRxWhileTx = 0x1};
						enum {ValueMask_DisableRxWhileTx = 0x00000040};
						};
					namespace PEN { // Field Description
						enum {Lsb = 4};
						enum {FieldMask = 0x00000010};
						enum {Value_NoParity = 0x0};
						enum {ValueMask_NoParity = 0x00000000};
						enum {Value_ParityEnable = 0x1};
						enum {ValueMask_ParityEnable = 0x00000010};
						};
					namespace RPM { // Field Description
						enum {Lsb = 2};
						enum {FieldMask = 0x0000000C};
						enum {Value_OddParity = 0x0};
						enum {ValueMask_OddParity = 0x00000000};
						enum {Value_LowParity = 0x1};
						enum {ValueMask_LowParity = 0x00000004};
						enum {Value_EventParity = 0x2};
						enum {ValueMask_EventParity = 0x00000008};
						enum {Value_HighParity = 0x3};
						enum {ValueMask_HighParity = 0x0000000C};
						};
					namespace TPM { // Field Description
						enum {Lsb = 0};
						enum {FieldMask = 0x00000003};
						enum {Value_OddParity = 0x0};
						enum {ValueMask_OddParity = 0x00000000};
						enum {Value_LowParity = 0x1};
						enum {ValueMask_LowParity = 0x00000001};
						enum {Value_EventParity = 0x2};
						enum {ValueMask_EventParity = 0x00000002};
						enum {Value_HighParity = 0x3};
						enum {ValueMask_HighParity = 0x00000003};
						};
					};
				namespace Profibus { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x0000FFFF};
					};
				namespace AHDLC { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x0000FFFF};
					namespace CHLN { // Field Description
						enum {Lsb = 12};
						enum {FieldMask = 0x00003000};
						enum {Value_Constant = 0x3};
						enum {ValueMask_Constant = 0x00003000};
						};
					namespace FLC { // Field Description
						enum {Lsb = 15};
						enum {FieldMask = 0x00008000};
						enum {Value_Normal = 0x0};
						enum {ValueMask_Normal = 0x00000000};
						enum {Value_CtsFlowControl = 0x1};
						enum {ValueMask_CtsFlowControl = 0x00008000};
						};
					};
				namespace IrDA { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x0000FFFF};
					};
				namespace V_14 { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x0000FFFF};
					};
				namespace BISYNC { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x0000FFFF};
					namespace NOS { // Field Description
						enum {Lsb = 12};
						enum {FieldMask = 0x0000F000};
						enum {Value_NoFlags = 0x0};
						enum {ValueMask_NoFlags = 0x00000000};
						enum {Value_Max = 0xF};
						enum {ValueMask_Max = 0x0000F000};
						};
					namespace CRC { // Field Description
						enum {Lsb = 10};
						enum {FieldMask = 0x00000C00};
						enum {Value_CRC16 = 0x1};
						enum {ValueMask_CRC16 = 0x00000400};
						enum {Value_LRC = 0x3};
						enum {ValueMask_LRC = 0x00000C00};
						};
					namespace RBCS { // Field Description
						enum {Lsb = 9};
						enum {FieldMask = 0x00000200};
						enum {Value_Disable = 0x0};
						enum {ValueMask_Disable = 0x00000000};
						enum {Value_Enable = 0x1};
						enum {ValueMask_Enable = 0x00000200};
						};
					namespace RTR { // Field Description
						enum {Lsb = 8};
						enum {FieldMask = 0x00000100};
						enum {Value_Normal = 0x0};
						enum {ValueMask_Normal = 0x00000000};
						enum {Value_Transparent = 0x1};
						enum {ValueMask_Transparent = 0x00000100};
						};
					namespace RVD { // Field Description
						enum {Lsb = 7};
						enum {FieldMask = 0x00000080};
						enum {Value_Normal = 0x0};
						enum {ValueMask_Normal = 0x00000000};
						enum {Value_ReverseBitOrder = 0x1};
						enum {ValueMask_ReverseBitOrder = 0x00000080};
						};
					namespace DRT { // Field Description
						enum {Lsb = 6};
						enum {FieldMask = 0x00000040};
						enum {Value_Normal = 0x0};
						enum {ValueMask_Normal = 0x00000000};
						enum {Value_DisableRxWhileTx = 0x1};
						enum {ValueMask_DisableRxWhileTx = 0x00000040};
						};
					namespace RPM { // Field Description
						enum {Lsb = 2};
						enum {FieldMask = 0x0000000C};
						enum {Value_OddParity = 0x0};
						enum {ValueMask_OddParity = 0x00000000};
						enum {Value_LowParity = 0x1};
						enum {ValueMask_LowParity = 0x00000004};
						enum {Value_EventParity = 0x2};
						enum {ValueMask_EventParity = 0x00000008};
						enum {Value_HighParity = 0x3};
						enum {ValueMask_HighParity = 0x0000000C};
						};
					namespace TPM { // Field Description
						enum {Lsb = 0};
						enum {FieldMask = 0x00000003};
						enum {Value_OddParity = 0x0};
						enum {ValueMask_OddParity = 0x00000000};
						enum {Value_LowParity = 0x1};
						enum {ValueMask_LowParity = 0x00000001};
						enum {Value_EventParity = 0x2};
						enum {ValueMask_EventParity = 0x00000002};
						enum {Value_HighParity = 0x3};
						enum {ValueMask_HighParity = 0x00000003};
						};
					};
				namespace DDCMP { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x0000FFFF};
					};
				namespace ENET { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x0000FFFF};
					namespace HBC { // Field Description
						enum {Lsb = 15};
						enum {FieldMask = 0x00008000};
						enum {Value_Disable = 0x0};
						enum {ValueMask_Disable = 0x00000000};
						enum {Value_Enable = 0x1};
						enum {ValueMask_Enable = 0x00008000};
						};
					namespace FC { // Field Description
						enum {Lsb = 14};
						enum {FieldMask = 0x00004000};
						enum {Value_Normal = 0x0};
						enum {ValueMask_Normal = 0x00000000};
						enum {Value_ForceCollision = 0x1};
						enum {ValueMask_ForceCollision = 0x00004000};
						};
					namespace RSH { // Field Description
						enum {Lsb = 13};
						enum {FieldMask = 0x00002000};
						enum {Value_DiscardShortFrames = 0x0};
						enum {ValueMask_DiscardShortFrames = 0x00000000};
						enum {Value_ReceiveShortFrames = 0x1};
						enum {ValueMask_ReceiveShortFrames = 0x00002000};
						};
					namespace IAM { // Field Description
						enum {Lsb = 12};
						enum {FieldMask = 0x00001000};
						enum {Value_PADDR1 = 0x0};
						enum {ValueMask_PADDR1 = 0x00000000};
						enum {Value_HashTable = 0x1};
						enum {ValueMask_HashTable = 0x00001000};
						};
					namespace CRC { // Field Description
						enum {Lsb = 10};
						enum {FieldMask = 0x00000C00};
						enum {Value_CcittCrc32 = 0x2};
						enum {ValueMask_CcittCrc32 = 0x00000800};
						};
					namespace PRO { // Field Description
						enum {Lsb = 9};
						enum {FieldMask = 0x00000200};
						enum {Value_Normal = 0x0};
						enum {ValueMask_Normal = 0x00000000};
						enum {Value_Promiscuous = 0x1};
						enum {ValueMask_Promiscuous = 0x00000200};
						};
					namespace BRO { // Field Description
						enum {Lsb = 8};
						enum {FieldMask = 0x00000100};
						enum {Value_ReceiveBroadcasts = 0x0};
						enum {ValueMask_ReceiveBroadcasts = 0x00000000};
						enum {Value_RejectBroadcasts = 0x1};
						enum {ValueMask_RejectBroadcasts = 0x00000100};
						};
					namespace SBT { // Field Description
						enum {Lsb = 7};
						enum {FieldMask = 0x00000080};
						enum {Value_Normal = 0x0};
						enum {ValueMask_Normal = 0x00000000};
						enum {Value_LessAgressive = 0x1};
						enum {ValueMask_LessAgressive = 0x00000080};
						};
					namespace LPB { // Field Description
						enum {Lsb = 6};
						enum {FieldMask = 0x00000040};
						enum {Value_Normal = 0x0};
						enum {ValueMask_Normal = 0x00000000};
						enum {Value_Loopback = 0x1};
						enum {ValueMask_Loopback = 0x00000040};
						};
					namespace SIP { // Field Description
						enum {Lsb = 5};
						enum {FieldMask = 0x00000020};
						enum {Value_Normal = 0x0};
						enum {ValueMask_Normal = 0x00000000};
						enum {Value_SampleTagByte = 0x1};
						enum {ValueMask_SampleTagByte = 0x00000020};
						};
					namespace LCW { // Field Description
						enum {Lsb = 4};
						enum {FieldMask = 0x00000010};
						enum {Value_Late64Byte = 0x0};
						enum {ValueMask_Late64Byte = 0x00000000};
						enum {Value_Late56Byte = 0x1};
						enum {ValueMask_Late56Byte = 0x00000010};
						};
					namespace NIB { // Field Description
						enum {Lsb = 1};
						enum {FieldMask = 0x0000000E};
						enum {Value_Minimum13Bits = 0x0};
						enum {ValueMask_Minimum13Bits = 0x00000000};
						enum {Value_Typical22Bits = 0x5};
						enum {ValueMask_Typical22Bits = 0x0000000A};
						enum {Value_Maximum24Bits = 0x7};
						enum {ValueMask_Maximum24Bits = 0x0000000E};
						};
					namespace FDE { // Field Description
						enum {Lsb = 0};
						enum {FieldMask = 0x00000001};
						enum {Value_Disable = 0x0};
						enum {ValueMask_Disable = 0x00000000};
						enum {Value_Enable = 0x1};
						enum {ValueMask_Enable = 0x00000001};
						};
					};
				};
			namespace TODR { // Register description
				typedef uint16_t	Reg;
				};
			namespace DSR { // Register description
				typedef uint16_t	Reg;
				namespace SYN2 { // Field Description
					enum {Lsb = 8};
					enum {FieldMask = 0x0000FF00};
					namespace UART { // Field Description
						enum {Lsb = 8};
						enum {FieldMask = 0x0000FF00};
						namespace Bit7 { // Field Description
							enum {Lsb = 15};
							enum {FieldMask = 0x00008000};
							enum {Value_Normal = 0x0};
							enum {ValueMask_Normal = 0x00000000};
							};
						namespace FSB { // Field Description
							enum {Lsb = 11};
							enum {FieldMask = 0x00007800};
							namespace Over16x { // Field Description
								enum {Lsb = 11};
								enum {FieldMask = 0x00007800};
								enum {Value_LastStop16_16 = 0xF};
								enum {ValueMask_LastStop16_16 = 0x00007800};
								enum {Value_LastStop15_16 = 0xE};
								enum {ValueMask_LastStop15_16 = 0x00007000};
								enum {Value_LastStop14_16 = 0xD};
								enum {ValueMask_LastStop14_16 = 0x00006800};
								enum {Value_LastStop13_16 = 0xC};
								enum {ValueMask_LastStop13_16 = 0x00006000};
								enum {Value_LastStop12_16 = 0xB};
								enum {ValueMask_LastStop12_16 = 0x00005800};
								enum {Value_LastStop11_16 = 0xA};
								enum {ValueMask_LastStop11_16 = 0x00005000};
								enum {Value_LastStop10_16 = 0x9};
								enum {ValueMask_LastStop10_16 = 0x00004800};
								enum {Value_LastStop9_16 = 0x8};
								enum {ValueMask_LastStop9_16 = 0x00004000};
								};
							namespace Over32x { // Field Description
								enum {Lsb = 11};
								enum {FieldMask = 0x00007800};
								enum {Value_LastStop32_32 = 0xF};
								enum {ValueMask_LastStop32_32 = 0x00007800};
								enum {Value_LastStop31_32 = 0xE};
								enum {ValueMask_LastStop31_32 = 0x00007000};
								enum {Value_LastStop30_32 = 0xD};
								enum {ValueMask_LastStop30_32 = 0x00006800};
								enum {Value_LastStop29_32 = 0xC};
								enum {ValueMask_LastStop29_32 = 0x00006000};
								enum {Value_LastStop28_32 = 0xB};
								enum {ValueMask_LastStop28_32 = 0x00005800};
								enum {Value_LastStop27_32 = 0xA};
								enum {ValueMask_LastStop27_32 = 0x00005000};
								enum {Value_LastStop26_32 = 0x9};
								enum {ValueMask_LastStop26_32 = 0x00004800};
								enum {Value_LastStop25_32 = 0x8};
								enum {ValueMask_LastStop25_32 = 0x00004000};
								enum {Value_LastStop24_32 = 0x7};
								enum {ValueMask_LastStop24_32 = 0x00003800};
								enum {Value_LastStop23_32 = 0x6};
								enum {ValueMask_LastStop23_32 = 0x00003000};
								enum {Value_LastStop22_32 = 0x5};
								enum {ValueMask_LastStop22_32 = 0x00002800};
								enum {Value_LastStop11_32 = 0x4};
								enum {ValueMask_LastStop11_32 = 0x00002000};
								enum {Value_LastStop20_32 = 0x3};
								enum {ValueMask_LastStop20_32 = 0x00001800};
								enum {Value_LastStop19_32 = 0x2};
								enum {ValueMask_LastStop19_32 = 0x00001000};
								enum {Value_LastStop18_32 = 0x1};
								enum {ValueMask_LastStop18_32 = 0x00000800};
								enum {Value_LastStop17_32 = 0x0};
								enum {ValueMask_LastStop17_32 = 0x00000000};
								};
							namespace Over8x { // Field Description
								enum {Lsb = 11};
								enum {FieldMask = 0x00007800};
								enum {Value_LastStop8_8 = 0xF};
								enum {ValueMask_LastStop8_8 = 0x00007800};
								enum {Value_LastStop7_8 = 0xE};
								enum {ValueMask_LastStop7_8 = 0x00007000};
								enum {Value_LastStop6_8 = 0xD};
								enum {ValueMask_LastStop6_8 = 0x00006800};
								enum {Value_LastStop5_8 = 0xC};
								enum {ValueMask_LastStop5_8 = 0x00006000};
								};
							};
						namespace Bits2_0 { // Field Description
							enum {Lsb = 8};
							enum {FieldMask = 0x00000700};
							enum {Value_Normal = 0x6};
							enum {ValueMask_Normal = 0x00000600};
							};
						};
					namespace BISYNC { // Field Description
						enum {Lsb = 8};
						enum {FieldMask = 0x0000FF00};
						};
					namespace ENET { // Field Description
						enum {Lsb = 8};
						enum {FieldMask = 0x0000FF00};
						enum {Value_Normal = 0xD5};
						enum {ValueMask_Normal = 0x0000D500};
						};
					namespace HDLC { // Field Description
						enum {Lsb = 8};
						enum {FieldMask = 0x0000FF00};
						enum {Value_Normal = 0x7E};
						enum {ValueMask_Normal = 0x00007E00};
						};
					};
				namespace SYN1 { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x000000FF};
					namespace UART { // Field Description
						enum {Lsb = 0};
						enum {FieldMask = 0x000000FF};
						enum {Value_Normal = 0x7E};
						enum {ValueMask_Normal = 0x0000007E};
						};
					namespace BISYNC { // Field Description
						enum {Lsb = 0};
						enum {FieldMask = 0x000000FF};
						};
					namespace ENET { // Field Description
						enum {Lsb = 0};
						enum {FieldMask = 0x000000FF};
						enum {Value_Normal = 0x55};
						enum {ValueMask_Normal = 0x00000055};
						};
					namespace HDLC { // Field Description
						enum {Lsb = 0};
						enum {FieldMask = 0x000000FF};
						enum {Value_Normal = 0x7E};
						enum {ValueMask_Normal = 0x0000007E};
						};
					};
				};
			namespace SCCE { // Register description
				typedef uint16_t	Reg;
				namespace UART { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x0000FFFF};
					namespace RX { // Field Description
						enum {Lsb = 0};
						enum {FieldMask = 0x00000001};
						enum {Value_Clear = 0x1};
						enum {ValueMask_Clear = 0x00000001};
						enum {Value_Pending = 0x1};
						enum {ValueMask_Pending = 0x00000001};
						enum {Value_NotPending = 0x0};
						enum {ValueMask_NotPending = 0x00000000};
						};
					namespace TX { // Field Description
						enum {Lsb = 1};
						enum {FieldMask = 0x00000002};
						enum {Value_Clear = 0x1};
						enum {ValueMask_Clear = 0x00000002};
						enum {Value_Pending = 0x1};
						enum {ValueMask_Pending = 0x00000002};
						enum {Value_NotPending = 0x0};
						enum {ValueMask_NotPending = 0x00000000};
						};
					namespace BSY { // Field Description
						enum {Lsb = 2};
						enum {FieldMask = 0x00000004};
						enum {Value_Clear = 0x1};
						enum {ValueMask_Clear = 0x00000004};
						enum {Value_Pending = 0x1};
						enum {ValueMask_Pending = 0x00000004};
						enum {Value_NotPending = 0x0};
						enum {ValueMask_NotPending = 0x00000000};
						};
					namespace CCR { // Field Description
						enum {Lsb = 3};
						enum {FieldMask = 0x00000008};
						enum {Value_Clear = 0x1};
						enum {ValueMask_Clear = 0x00000008};
						enum {Value_Pending = 0x1};
						enum {ValueMask_Pending = 0x00000008};
						enum {Value_NotPending = 0x0};
						enum {ValueMask_NotPending = 0x00000000};
						};
					namespace BRKS { // Field Description
						enum {Lsb = 5};
						enum {FieldMask = 0x00000020};
						enum {Value_Clear = 0x1};
						enum {ValueMask_Clear = 0x00000020};
						enum {Value_Pending = 0x1};
						enum {ValueMask_Pending = 0x00000020};
						enum {Value_NotPending = 0x0};
						enum {ValueMask_NotPending = 0x00000000};
						};
					namespace BRKE { // Field Description
						enum {Lsb = 6};
						enum {FieldMask = 0x00000040};
						enum {Value_Clear = 0x1};
						enum {ValueMask_Clear = 0x00000040};
						enum {Value_Pending = 0x1};
						enum {ValueMask_Pending = 0x00000040};
						enum {Value_NotPending = 0x0};
						enum {ValueMask_NotPending = 0x00000000};
						};
					namespace GRA { // Field Description
						enum {Lsb = 7};
						enum {FieldMask = 0x00000080};
						enum {Value_Clear = 0x1};
						enum {ValueMask_Clear = 0x00000080};
						enum {Value_Pending = 0x1};
						enum {ValueMask_Pending = 0x00000080};
						enum {Value_NotPending = 0x0};
						enum {ValueMask_NotPending = 0x00000000};
						};
					namespace IDL { // Field Description
						enum {Lsb = 8};
						enum {FieldMask = 0x00000100};
						enum {Value_Clear = 0x1};
						enum {ValueMask_Clear = 0x00000100};
						enum {Value_Pending = 0x1};
						enum {ValueMask_Pending = 0x00000100};
						enum {Value_NotPending = 0x0};
						enum {ValueMask_NotPending = 0x00000000};
						};
					namespace AB { // Field Description
						enum {Lsb = 9};
						enum {FieldMask = 0x00000200};
						enum {Value_Clear = 0x1};
						enum {ValueMask_Clear = 0x00000200};
						enum {Value_Pending = 0x1};
						enum {ValueMask_Pending = 0x00000200};
						enum {Value_NotPending = 0x0};
						enum {ValueMask_NotPending = 0x00000000};
						};
					namespace GLT { // Field Description
						enum {Lsb = 11};
						enum {FieldMask = 0x00000800};
						enum {Value_Clear = 0x1};
						enum {ValueMask_Clear = 0x00000800};
						enum {Value_Pending = 0x1};
						enum {ValueMask_Pending = 0x00000800};
						enum {Value_NotPending = 0x0};
						enum {ValueMask_NotPending = 0x00000000};
						};
					namespace GLR { // Field Description
						enum {Lsb = 12};
						enum {FieldMask = 0x00001000};
						enum {Value_Clear = 0x1};
						enum {ValueMask_Clear = 0x00001000};
						enum {Value_Pending = 0x1};
						enum {ValueMask_Pending = 0x00001000};
						enum {Value_NotPending = 0x0};
						enum {ValueMask_NotPending = 0x00000000};
						};
					};
				namespace HDLC { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x0000FFFF};
					namespace RXB { // Field Description
						enum {Lsb = 0};
						enum {FieldMask = 0x00000001};
						enum {Value_Clear = 0x1};
						enum {ValueMask_Clear = 0x00000001};
						enum {Value_Pending = 0x1};
						enum {ValueMask_Pending = 0x00000001};
						enum {Value_NotPending = 0x0};
						enum {ValueMask_NotPending = 0x00000000};
						};
					namespace TXB { // Field Description
						enum {Lsb = 1};
						enum {FieldMask = 0x00000002};
						enum {Value_Clear = 0x1};
						enum {ValueMask_Clear = 0x00000002};
						enum {Value_Pending = 0x1};
						enum {ValueMask_Pending = 0x00000002};
						enum {Value_NotPending = 0x0};
						enum {ValueMask_NotPending = 0x00000000};
						};
					namespace BSY { // Field Description
						enum {Lsb = 2};
						enum {FieldMask = 0x00000004};
						enum {Value_Clear = 0x1};
						enum {ValueMask_Clear = 0x00000004};
						enum {Value_Pending = 0x1};
						enum {ValueMask_Pending = 0x00000004};
						enum {Value_NotPending = 0x0};
						enum {ValueMask_NotPending = 0x00000000};
						};
					namespace RXF { // Field Description
						enum {Lsb = 3};
						enum {FieldMask = 0x00000008};
						enum {Value_Clear = 0x1};
						enum {ValueMask_Clear = 0x00000008};
						enum {Value_Pending = 0x1};
						enum {ValueMask_Pending = 0x00000008};
						enum {Value_NotPending = 0x0};
						enum {ValueMask_NotPending = 0x00000000};
						};
					namespace TXE { // Field Description
						enum {Lsb = 4};
						enum {FieldMask = 0x00000010};
						enum {Value_Clear = 0x1};
						enum {ValueMask_Clear = 0x00000010};
						enum {Value_Pending = 0x1};
						enum {ValueMask_Pending = 0x00000010};
						enum {Value_NotPending = 0x0};
						enum {ValueMask_NotPending = 0x00000000};
						};
					namespace GRA { // Field Description
						enum {Lsb = 7};
						enum {FieldMask = 0x00000080};
						enum {Value_Clear = 0x1};
						enum {ValueMask_Clear = 0x00000080};
						enum {Value_Pending = 0x1};
						enum {ValueMask_Pending = 0x00000080};
						enum {Value_NotPending = 0x0};
						enum {ValueMask_NotPending = 0x00000000};
						};
					namespace IDL { // Field Description
						enum {Lsb = 8};
						enum {FieldMask = 0x00000100};
						enum {Value_Clear = 0x1};
						enum {ValueMask_Clear = 0x00000100};
						enum {Value_Pending = 0x1};
						enum {ValueMask_Pending = 0x00000100};
						enum {Value_NotPending = 0x0};
						enum {ValueMask_NotPending = 0x00000000};
						};
					namespace FLG { // Field Description
						enum {Lsb = 9};
						enum {FieldMask = 0x00000200};
						enum {Value_Clear = 0x1};
						enum {ValueMask_Clear = 0x00000200};
						enum {Value_Pending = 0x1};
						enum {ValueMask_Pending = 0x00000200};
						enum {Value_NotPending = 0x0};
						enum {ValueMask_NotPending = 0x00000000};
						};
					namespace DCC { // Field Description
						enum {Lsb = 10};
						enum {FieldMask = 0x00000400};
						enum {Value_Clear = 0x1};
						enum {ValueMask_Clear = 0x00000400};
						enum {Value_Pending = 0x1};
						enum {ValueMask_Pending = 0x00000400};
						enum {Value_NotPending = 0x0};
						enum {ValueMask_NotPending = 0x00000000};
						};
					namespace GLT { // Field Description
						enum {Lsb = 11};
						enum {FieldMask = 0x00000800};
						enum {Value_Clear = 0x1};
						enum {ValueMask_Clear = 0x00000800};
						enum {Value_Pending = 0x1};
						enum {ValueMask_Pending = 0x00000800};
						enum {Value_NotPending = 0x0};
						enum {ValueMask_NotPending = 0x00000000};
						};
					namespace GLR { // Field Description
						enum {Lsb = 12};
						enum {FieldMask = 0x00001000};
						enum {Value_Clear = 0x1};
						enum {ValueMask_Clear = 0x00001000};
						enum {Value_Pending = 0x1};
						enum {ValueMask_Pending = 0x00001000};
						enum {Value_NotPending = 0x0};
						enum {ValueMask_NotPending = 0x00000000};
						};
					};
				namespace AHDLC { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x0000FFFF};
					namespace RXB { // Field Description
						enum {Lsb = 0};
						enum {FieldMask = 0x00000001};
						enum {Value_Clear = 0x1};
						enum {ValueMask_Clear = 0x00000001};
						enum {Value_Pending = 0x1};
						enum {ValueMask_Pending = 0x00000001};
						enum {Value_NotPending = 0x0};
						enum {ValueMask_NotPending = 0x00000000};
						};
					namespace TXB { // Field Description
						enum {Lsb = 1};
						enum {FieldMask = 0x00000002};
						enum {Value_Clear = 0x1};
						enum {ValueMask_Clear = 0x00000002};
						enum {Value_Pending = 0x1};
						enum {ValueMask_Pending = 0x00000002};
						enum {Value_NotPending = 0x0};
						enum {ValueMask_NotPending = 0x00000000};
						};
					namespace BSY { // Field Description
						enum {Lsb = 2};
						enum {FieldMask = 0x00000004};
						enum {Value_Clear = 0x1};
						enum {ValueMask_Clear = 0x00000004};
						enum {Value_Pending = 0x1};
						enum {ValueMask_Pending = 0x00000004};
						enum {Value_NotPending = 0x0};
						enum {ValueMask_NotPending = 0x00000000};
						};
					namespace RXF { // Field Description
						enum {Lsb = 3};
						enum {FieldMask = 0x00000008};
						enum {Value_Clear = 0x1};
						enum {ValueMask_Clear = 0x00000008};
						enum {Value_Pending = 0x1};
						enum {ValueMask_Pending = 0x00000008};
						enum {Value_NotPending = 0x0};
						enum {ValueMask_NotPending = 0x00000000};
						};
					namespace TXE { // Field Description
						enum {Lsb = 4};
						enum {FieldMask = 0x00000010};
						enum {Value_Clear = 0x1};
						enum {ValueMask_Clear = 0x00000010};
						enum {Value_Pending = 0x1};
						enum {ValueMask_Pending = 0x00000010};
						enum {Value_NotPending = 0x0};
						enum {ValueMask_NotPending = 0x00000000};
						};
					namespace BRKS { // Field Description
						enum {Lsb = 5};
						enum {FieldMask = 0x00000020};
						enum {Value_Clear = 0x1};
						enum {ValueMask_Clear = 0x00000020};
						enum {Value_Pending = 0x1};
						enum {ValueMask_Pending = 0x00000020};
						enum {Value_NotPending = 0x0};
						enum {ValueMask_NotPending = 0x00000000};
						};
					namespace BRKE { // Field Description
						enum {Lsb = 6};
						enum {FieldMask = 0x00000040};
						enum {Value_Clear = 0x1};
						enum {ValueMask_Clear = 0x00000040};
						enum {Value_Pending = 0x1};
						enum {ValueMask_Pending = 0x00000040};
						enum {Value_NotPending = 0x0};
						enum {ValueMask_NotPending = 0x00000000};
						};
					namespace IDL { // Field Description
						enum {Lsb = 8};
						enum {FieldMask = 0x00000100};
						enum {Value_Clear = 0x1};
						enum {ValueMask_Clear = 0x00000100};
						enum {Value_Pending = 0x1};
						enum {ValueMask_Pending = 0x00000100};
						enum {Value_NotPending = 0x0};
						enum {ValueMask_NotPending = 0x00000000};
						};
					namespace GLT { // Field Description
						enum {Lsb = 11};
						enum {FieldMask = 0x00000800};
						enum {Value_Clear = 0x1};
						enum {ValueMask_Clear = 0x00000800};
						enum {Value_Pending = 0x1};
						enum {ValueMask_Pending = 0x00000800};
						enum {Value_NotPending = 0x0};
						enum {ValueMask_NotPending = 0x00000000};
						};
					namespace GLR { // Field Description
						enum {Lsb = 12};
						enum {FieldMask = 0x00001000};
						enum {Value_Clear = 0x1};
						enum {ValueMask_Clear = 0x00001000};
						enum {Value_Pending = 0x1};
						enum {ValueMask_Pending = 0x00001000};
						enum {Value_NotPending = 0x0};
						enum {ValueMask_NotPending = 0x00000000};
						};
					};
				namespace BISYNC { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x0000FFFF};
					namespace RXB { // Field Description
						enum {Lsb = 0};
						enum {FieldMask = 0x00000001};
						enum {Value_Clear = 0x1};
						enum {ValueMask_Clear = 0x00000001};
						enum {Value_Pending = 0x1};
						enum {ValueMask_Pending = 0x00000001};
						enum {Value_NotPending = 0x0};
						enum {ValueMask_NotPending = 0x00000000};
						};
					namespace TXB { // Field Description
						enum {Lsb = 1};
						enum {FieldMask = 0x00000002};
						enum {Value_Clear = 0x1};
						enum {ValueMask_Clear = 0x00000002};
						enum {Value_Pending = 0x1};
						enum {ValueMask_Pending = 0x00000002};
						enum {Value_NotPending = 0x0};
						enum {ValueMask_NotPending = 0x00000000};
						};
					namespace BSY { // Field Description
						enum {Lsb = 2};
						enum {FieldMask = 0x00000004};
						enum {Value_Clear = 0x1};
						enum {ValueMask_Clear = 0x00000004};
						enum {Value_Pending = 0x1};
						enum {ValueMask_Pending = 0x00000004};
						enum {Value_NotPending = 0x0};
						enum {ValueMask_NotPending = 0x00000000};
						};
					namespace RCH { // Field Description
						enum {Lsb = 3};
						enum {FieldMask = 0x00000008};
						enum {Value_Clear = 0x1};
						enum {ValueMask_Clear = 0x00000008};
						enum {Value_Pending = 0x1};
						enum {ValueMask_Pending = 0x00000008};
						enum {Value_NotPending = 0x0};
						enum {ValueMask_NotPending = 0x00000000};
						};
					namespace TXE { // Field Description
						enum {Lsb = 4};
						enum {FieldMask = 0x00000010};
						enum {Value_Clear = 0x1};
						enum {ValueMask_Clear = 0x00000010};
						enum {Value_Pending = 0x1};
						enum {ValueMask_Pending = 0x00000010};
						enum {Value_NotPending = 0x0};
						enum {ValueMask_NotPending = 0x00000000};
						};
					namespace GRA { // Field Description
						enum {Lsb = 7};
						enum {FieldMask = 0x00000080};
						enum {Value_Clear = 0x1};
						enum {ValueMask_Clear = 0x00000080};
						enum {Value_Pending = 0x1};
						enum {ValueMask_Pending = 0x00000080};
						enum {Value_NotPending = 0x0};
						enum {ValueMask_NotPending = 0x00000000};
						};
					namespace DCC { // Field Description
						enum {Lsb = 10};
						enum {FieldMask = 0x00000400};
						enum {Value_Clear = 0x1};
						enum {ValueMask_Clear = 0x00000400};
						enum {Value_Pending = 0x1};
						enum {ValueMask_Pending = 0x00000400};
						enum {Value_NotPending = 0x0};
						enum {ValueMask_NotPending = 0x00000000};
						};
					namespace GLT { // Field Description
						enum {Lsb = 11};
						enum {FieldMask = 0x00000800};
						enum {Value_Clear = 0x1};
						enum {ValueMask_Clear = 0x00000800};
						enum {Value_Pending = 0x1};
						enum {ValueMask_Pending = 0x00000800};
						enum {Value_NotPending = 0x0};
						enum {ValueMask_NotPending = 0x00000000};
						};
					namespace GLR { // Field Description
						enum {Lsb = 12};
						enum {FieldMask = 0x00001000};
						enum {Value_Clear = 0x1};
						enum {ValueMask_Clear = 0x00001000};
						enum {Value_Pending = 0x1};
						enum {ValueMask_Pending = 0x00001000};
						enum {Value_NotPending = 0x0};
						enum {ValueMask_NotPending = 0x00000000};
						};
					};
				namespace ENET { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x0000FFFF};
					namespace RXB { // Field Description
						enum {Lsb = 0};
						enum {FieldMask = 0x00000001};
						enum {Value_Clear = 0x1};
						enum {ValueMask_Clear = 0x00000001};
						enum {Value_Pending = 0x1};
						enum {ValueMask_Pending = 0x00000001};
						enum {Value_NotPending = 0x0};
						enum {ValueMask_NotPending = 0x00000000};
						};
					namespace TXB { // Field Description
						enum {Lsb = 1};
						enum {FieldMask = 0x00000002};
						enum {Value_Clear = 0x1};
						enum {ValueMask_Clear = 0x00000002};
						enum {Value_Pending = 0x1};
						enum {ValueMask_Pending = 0x00000002};
						enum {Value_NotPending = 0x0};
						enum {ValueMask_NotPending = 0x00000000};
						};
					namespace BSY { // Field Description
						enum {Lsb = 2};
						enum {FieldMask = 0x00000004};
						enum {Value_Clear = 0x1};
						enum {ValueMask_Clear = 0x00000004};
						enum {Value_Pending = 0x1};
						enum {ValueMask_Pending = 0x00000004};
						enum {Value_NotPending = 0x0};
						enum {ValueMask_NotPending = 0x00000000};
						};
					namespace RXF { // Field Description
						enum {Lsb = 3};
						enum {FieldMask = 0x00000008};
						enum {Value_Clear = 0x1};
						enum {ValueMask_Clear = 0x00000008};
						enum {Value_Pending = 0x1};
						enum {ValueMask_Pending = 0x00000008};
						enum {Value_NotPending = 0x0};
						enum {ValueMask_NotPending = 0x00000000};
						};
					namespace TXE { // Field Description
						enum {Lsb = 4};
						enum {FieldMask = 0x00000010};
						enum {Value_Clear = 0x1};
						enum {ValueMask_Clear = 0x00000010};
						enum {Value_Pending = 0x1};
						enum {ValueMask_Pending = 0x00000010};
						enum {Value_NotPending = 0x0};
						enum {ValueMask_NotPending = 0x00000000};
						};
					namespace GRA { // Field Description
						enum {Lsb = 7};
						enum {FieldMask = 0x00000080};
						enum {Value_Clear = 0x1};
						enum {ValueMask_Clear = 0x00000080};
						enum {Value_Pending = 0x1};
						enum {ValueMask_Pending = 0x00000080};
						enum {Value_NotPending = 0x0};
						enum {ValueMask_NotPending = 0x00000000};
						};
					};
				namespace TRANS { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x0000FFFF};
					namespace RXB { // Field Description
						enum {Lsb = 0};
						enum {FieldMask = 0x00000001};
						enum {Value_Clear = 0x1};
						enum {ValueMask_Clear = 0x00000001};
						enum {Value_Pending = 0x1};
						enum {ValueMask_Pending = 0x00000001};
						enum {Value_NotPending = 0x0};
						enum {ValueMask_NotPending = 0x00000000};
						};
					namespace TXB { // Field Description
						enum {Lsb = 1};
						enum {FieldMask = 0x00000002};
						enum {Value_Clear = 0x1};
						enum {ValueMask_Clear = 0x00000002};
						enum {Value_Pending = 0x1};
						enum {ValueMask_Pending = 0x00000002};
						enum {Value_NotPending = 0x0};
						enum {ValueMask_NotPending = 0x00000000};
						};
					namespace BSY { // Field Description
						enum {Lsb = 2};
						enum {FieldMask = 0x00000004};
						enum {Value_Clear = 0x1};
						enum {ValueMask_Clear = 0x00000004};
						enum {Value_Pending = 0x1};
						enum {ValueMask_Pending = 0x00000004};
						enum {Value_NotPending = 0x0};
						enum {ValueMask_NotPending = 0x00000000};
						};
					namespace TXE { // Field Description
						enum {Lsb = 4};
						enum {FieldMask = 0x00000010};
						enum {Value_Clear = 0x1};
						enum {ValueMask_Clear = 0x00000010};
						enum {Value_Pending = 0x1};
						enum {ValueMask_Pending = 0x00000010};
						enum {Value_NotPending = 0x0};
						enum {ValueMask_NotPending = 0x00000000};
						};
					namespace GRA { // Field Description
						enum {Lsb = 7};
						enum {FieldMask = 0x00000080};
						enum {Value_Clear = 0x1};
						enum {ValueMask_Clear = 0x00000080};
						enum {Value_Pending = 0x1};
						enum {ValueMask_Pending = 0x00000080};
						enum {Value_NotPending = 0x0};
						enum {ValueMask_NotPending = 0x00000000};
						};
					namespace DCC { // Field Description
						enum {Lsb = 10};
						enum {FieldMask = 0x00000400};
						enum {Value_Clear = 0x1};
						enum {ValueMask_Clear = 0x00000400};
						enum {Value_Pending = 0x1};
						enum {ValueMask_Pending = 0x00000400};
						enum {Value_NotPending = 0x0};
						enum {ValueMask_NotPending = 0x00000000};
						};
					namespace GLT { // Field Description
						enum {Lsb = 11};
						enum {FieldMask = 0x00000800};
						enum {Value_Clear = 0x1};
						enum {ValueMask_Clear = 0x00000800};
						enum {Value_Pending = 0x1};
						enum {ValueMask_Pending = 0x00000800};
						enum {Value_NotPending = 0x0};
						enum {ValueMask_NotPending = 0x00000000};
						};
					namespace GLR { // Field Description
						enum {Lsb = 12};
						enum {FieldMask = 0x00001000};
						enum {Value_Clear = 0x1};
						enum {ValueMask_Clear = 0x00001000};
						enum {Value_Pending = 0x1};
						enum {ValueMask_Pending = 0x00001000};
						enum {Value_NotPending = 0x0};
						enum {ValueMask_NotPending = 0x00000000};
						};
					};
				};
			namespace SCCM { // Register description
				typedef uint16_t	Reg;
				namespace UART { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x0000FFFF};
					namespace RX { // Field Description
						enum {Lsb = 0};
						enum {FieldMask = 0x00000001};
						enum {Value_Disable = 0x0};
						enum {ValueMask_Disable = 0x00000000};
						enum {Value_Enable = 0x1};
						enum {ValueMask_Enable = 0x00000001};
						};
					namespace TX { // Field Description
						enum {Lsb = 1};
						enum {FieldMask = 0x00000002};
						enum {Value_Disable = 0x0};
						enum {ValueMask_Disable = 0x00000000};
						enum {Value_Enable = 0x1};
						enum {ValueMask_Enable = 0x00000002};
						};
					namespace BSY { // Field Description
						enum {Lsb = 2};
						enum {FieldMask = 0x00000004};
						enum {Value_Disable = 0x0};
						enum {ValueMask_Disable = 0x00000000};
						enum {Value_Enable = 0x1};
						enum {ValueMask_Enable = 0x00000004};
						};
					namespace CCR { // Field Description
						enum {Lsb = 3};
						enum {FieldMask = 0x00000008};
						enum {Value_Disable = 0x0};
						enum {ValueMask_Disable = 0x00000000};
						enum {Value_Enable = 0x1};
						enum {ValueMask_Enable = 0x00000008};
						};
					namespace BRKS { // Field Description
						enum {Lsb = 5};
						enum {FieldMask = 0x00000020};
						enum {Value_Disable = 0x0};
						enum {ValueMask_Disable = 0x00000000};
						enum {Value_Enable = 0x1};
						enum {ValueMask_Enable = 0x00000020};
						};
					namespace BRKE { // Field Description
						enum {Lsb = 6};
						enum {FieldMask = 0x00000040};
						enum {Value_Disable = 0x0};
						enum {ValueMask_Disable = 0x00000000};
						enum {Value_Enable = 0x1};
						enum {ValueMask_Enable = 0x00000040};
						};
					namespace GRA { // Field Description
						enum {Lsb = 7};
						enum {FieldMask = 0x00000080};
						enum {Value_Disable = 0x0};
						enum {ValueMask_Disable = 0x00000000};
						enum {Value_Enable = 0x1};
						enum {ValueMask_Enable = 0x00000080};
						};
					namespace IDL { // Field Description
						enum {Lsb = 8};
						enum {FieldMask = 0x00000100};
						enum {Value_Disable = 0x0};
						enum {ValueMask_Disable = 0x00000000};
						enum {Value_Enable = 0x1};
						enum {ValueMask_Enable = 0x00000100};
						};
					namespace AB { // Field Description
						enum {Lsb = 9};
						enum {FieldMask = 0x00000200};
						enum {Value_Disable = 0x0};
						enum {ValueMask_Disable = 0x00000000};
						enum {Value_Enable = 0x1};
						enum {ValueMask_Enable = 0x00000200};
						};
					namespace GLT { // Field Description
						enum {Lsb = 11};
						enum {FieldMask = 0x00000800};
						enum {Value_Disable = 0x0};
						enum {ValueMask_Disable = 0x00000000};
						enum {Value_Enable = 0x1};
						enum {ValueMask_Enable = 0x00000800};
						};
					namespace GLR { // Field Description
						enum {Lsb = 12};
						enum {FieldMask = 0x00001000};
						enum {Value_Disable = 0x0};
						enum {ValueMask_Disable = 0x00000000};
						enum {Value_Enable = 0x1};
						enum {ValueMask_Enable = 0x00001000};
						};
					};
				namespace HDLC { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x0000FFFF};
					namespace RXB { // Field Description
						enum {Lsb = 0};
						enum {FieldMask = 0x00000001};
						enum {Value_Disable = 0x0};
						enum {ValueMask_Disable = 0x00000000};
						enum {Value_Enable = 0x1};
						enum {ValueMask_Enable = 0x00000001};
						};
					namespace TXB { // Field Description
						enum {Lsb = 1};
						enum {FieldMask = 0x00000002};
						enum {Value_Disable = 0x0};
						enum {ValueMask_Disable = 0x00000000};
						enum {Value_Enable = 0x1};
						enum {ValueMask_Enable = 0x00000002};
						};
					namespace BSY { // Field Description
						enum {Lsb = 2};
						enum {FieldMask = 0x00000004};
						enum {Value_Disable = 0x0};
						enum {ValueMask_Disable = 0x00000000};
						enum {Value_Enable = 0x1};
						enum {ValueMask_Enable = 0x00000004};
						};
					namespace RXF { // Field Description
						enum {Lsb = 3};
						enum {FieldMask = 0x00000008};
						enum {Value_Disable = 0x0};
						enum {ValueMask_Disable = 0x00000000};
						enum {Value_Enable = 0x1};
						enum {ValueMask_Enable = 0x00000008};
						};
					namespace TXE { // Field Description
						enum {Lsb = 4};
						enum {FieldMask = 0x00000010};
						enum {Value_Disable = 0x0};
						enum {ValueMask_Disable = 0x00000000};
						enum {Value_Enable = 0x1};
						enum {ValueMask_Enable = 0x00000010};
						};
					namespace GRA { // Field Description
						enum {Lsb = 7};
						enum {FieldMask = 0x00000080};
						enum {Value_Disable = 0x0};
						enum {ValueMask_Disable = 0x00000000};
						enum {Value_Enable = 0x1};
						enum {ValueMask_Enable = 0x00000080};
						};
					namespace IDL { // Field Description
						enum {Lsb = 8};
						enum {FieldMask = 0x00000100};
						enum {Value_Disable = 0x0};
						enum {ValueMask_Disable = 0x00000000};
						enum {Value_Enable = 0x1};
						enum {ValueMask_Enable = 0x00000100};
						};
					namespace FLG { // Field Description
						enum {Lsb = 9};
						enum {FieldMask = 0x00000200};
						enum {Value_Disable = 0x0};
						enum {ValueMask_Disable = 0x00000000};
						enum {Value_Enable = 0x1};
						enum {ValueMask_Enable = 0x00000200};
						};
					namespace DCC { // Field Description
						enum {Lsb = 10};
						enum {FieldMask = 0x00000400};
						enum {Value_Disable = 0x0};
						enum {ValueMask_Disable = 0x00000000};
						enum {Value_Enable = 0x1};
						enum {ValueMask_Enable = 0x00000400};
						};
					namespace GLT { // Field Description
						enum {Lsb = 11};
						enum {FieldMask = 0x00000800};
						enum {Value_Disable = 0x0};
						enum {ValueMask_Disable = 0x00000000};
						enum {Value_Enable = 0x1};
						enum {ValueMask_Enable = 0x00000800};
						};
					namespace GLR { // Field Description
						enum {Lsb = 12};
						enum {FieldMask = 0x00001000};
						enum {Value_Disable = 0x0};
						enum {ValueMask_Disable = 0x00000000};
						enum {Value_Enable = 0x1};
						enum {ValueMask_Enable = 0x00001000};
						};
					};
				namespace AHDLC { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x0000FFFF};
					namespace RXB { // Field Description
						enum {Lsb = 0};
						enum {FieldMask = 0x00000001};
						enum {Value_Disable = 0x0};
						enum {ValueMask_Disable = 0x00000000};
						enum {Value_Enable = 0x1};
						enum {ValueMask_Enable = 0x00000001};
						};
					namespace TXB { // Field Description
						enum {Lsb = 1};
						enum {FieldMask = 0x00000002};
						enum {Value_Disable = 0x0};
						enum {ValueMask_Disable = 0x00000000};
						enum {Value_Enable = 0x1};
						enum {ValueMask_Enable = 0x00000002};
						};
					namespace BSY { // Field Description
						enum {Lsb = 2};
						enum {FieldMask = 0x00000004};
						enum {Value_Disable = 0x0};
						enum {ValueMask_Disable = 0x00000000};
						enum {Value_Enable = 0x1};
						enum {ValueMask_Enable = 0x00000004};
						};
					namespace RXF { // Field Description
						enum {Lsb = 3};
						enum {FieldMask = 0x00000008};
						enum {Value_Disable = 0x0};
						enum {ValueMask_Disable = 0x00000000};
						enum {Value_Enable = 0x1};
						enum {ValueMask_Enable = 0x00000008};
						};
					namespace TXE { // Field Description
						enum {Lsb = 4};
						enum {FieldMask = 0x00000010};
						enum {Value_Disable = 0x0};
						enum {ValueMask_Disable = 0x00000000};
						enum {Value_Enable = 0x1};
						enum {ValueMask_Enable = 0x00000010};
						};
					namespace BRKS { // Field Description
						enum {Lsb = 5};
						enum {FieldMask = 0x00000020};
						enum {Value_Disable = 0x0};
						enum {ValueMask_Disable = 0x00000000};
						enum {Value_Enable = 0x1};
						enum {ValueMask_Enable = 0x00000020};
						};
					namespace BRKE { // Field Description
						enum {Lsb = 6};
						enum {FieldMask = 0x00000040};
						enum {Value_Disable = 0x0};
						enum {ValueMask_Disable = 0x00000000};
						enum {Value_Enable = 0x1};
						enum {ValueMask_Enable = 0x00000040};
						};
					namespace IDL { // Field Description
						enum {Lsb = 8};
						enum {FieldMask = 0x00000100};
						enum {Value_Disable = 0x0};
						enum {ValueMask_Disable = 0x00000000};
						enum {Value_Enable = 0x1};
						enum {ValueMask_Enable = 0x00000100};
						};
					namespace GLT { // Field Description
						enum {Lsb = 11};
						enum {FieldMask = 0x00000800};
						enum {Value_Disable = 0x0};
						enum {ValueMask_Disable = 0x00000000};
						enum {Value_Enable = 0x1};
						enum {ValueMask_Enable = 0x00000800};
						};
					namespace GLR { // Field Description
						enum {Lsb = 12};
						enum {FieldMask = 0x00001000};
						enum {Value_Disable = 0x0};
						enum {ValueMask_Disable = 0x00000000};
						enum {Value_Enable = 0x1};
						enum {ValueMask_Enable = 0x00001000};
						};
					};
				namespace BISYNC { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x0000FFFF};
					namespace RXB { // Field Description
						enum {Lsb = 0};
						enum {FieldMask = 0x00000001};
						enum {Value_Disable = 0x0};
						enum {ValueMask_Disable = 0x00000000};
						enum {Value_Enable = 0x1};
						enum {ValueMask_Enable = 0x00000001};
						};
					namespace TXB { // Field Description
						enum {Lsb = 1};
						enum {FieldMask = 0x00000002};
						enum {Value_Disable = 0x0};
						enum {ValueMask_Disable = 0x00000000};
						enum {Value_Enable = 0x1};
						enum {ValueMask_Enable = 0x00000002};
						};
					namespace BSY { // Field Description
						enum {Lsb = 2};
						enum {FieldMask = 0x00000004};
						enum {Value_Disable = 0x0};
						enum {ValueMask_Disable = 0x00000000};
						enum {Value_Enable = 0x1};
						enum {ValueMask_Enable = 0x00000004};
						};
					namespace RCH { // Field Description
						enum {Lsb = 3};
						enum {FieldMask = 0x00000008};
						enum {Value_Disable = 0x0};
						enum {ValueMask_Disable = 0x00000000};
						enum {Value_Enable = 0x1};
						enum {ValueMask_Enable = 0x00000008};
						};
					namespace TXE { // Field Description
						enum {Lsb = 4};
						enum {FieldMask = 0x00000010};
						enum {Value_Disable = 0x0};
						enum {ValueMask_Disable = 0x00000000};
						enum {Value_Enable = 0x1};
						enum {ValueMask_Enable = 0x00000010};
						};
					namespace GRA { // Field Description
						enum {Lsb = 7};
						enum {FieldMask = 0x00000080};
						enum {Value_Disable = 0x0};
						enum {ValueMask_Disable = 0x00000000};
						enum {Value_Enable = 0x1};
						enum {ValueMask_Enable = 0x00000080};
						};
					namespace DCC { // Field Description
						enum {Lsb = 10};
						enum {FieldMask = 0x00000400};
						enum {Value_Disable = 0x0};
						enum {ValueMask_Disable = 0x00000000};
						enum {Value_Enable = 0x1};
						enum {ValueMask_Enable = 0x00000400};
						};
					namespace GLT { // Field Description
						enum {Lsb = 11};
						enum {FieldMask = 0x00000800};
						enum {Value_Disable = 0x0};
						enum {ValueMask_Disable = 0x00000000};
						enum {Value_Enable = 0x1};
						enum {ValueMask_Enable = 0x00000800};
						};
					namespace GLR { // Field Description
						enum {Lsb = 12};
						enum {FieldMask = 0x00001000};
						enum {Value_Disable = 0x0};
						enum {ValueMask_Disable = 0x00000000};
						enum {Value_Enable = 0x1};
						enum {ValueMask_Enable = 0x00001000};
						};
					};
				namespace TRANS { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x0000FFFF};
					namespace RXB { // Field Description
						enum {Lsb = 0};
						enum {FieldMask = 0x00000001};
						enum {Value_Disable = 0x0};
						enum {ValueMask_Disable = 0x00000000};
						enum {Value_Enable = 0x1};
						enum {ValueMask_Enable = 0x00000001};
						};
					namespace TXB { // Field Description
						enum {Lsb = 1};
						enum {FieldMask = 0x00000002};
						enum {Value_Disable = 0x0};
						enum {ValueMask_Disable = 0x00000000};
						enum {Value_Enable = 0x1};
						enum {ValueMask_Enable = 0x00000002};
						};
					namespace BSY { // Field Description
						enum {Lsb = 2};
						enum {FieldMask = 0x00000004};
						enum {Value_Disable = 0x0};
						enum {ValueMask_Disable = 0x00000000};
						enum {Value_Enable = 0x1};
						enum {ValueMask_Enable = 0x00000004};
						};
					namespace TXE { // Field Description
						enum {Lsb = 4};
						enum {FieldMask = 0x00000010};
						enum {Value_Disable = 0x0};
						enum {ValueMask_Disable = 0x00000000};
						enum {Value_Enable = 0x1};
						enum {ValueMask_Enable = 0x00000010};
						};
					namespace GRA { // Field Description
						enum {Lsb = 7};
						enum {FieldMask = 0x00000080};
						enum {Value_Disable = 0x0};
						enum {ValueMask_Disable = 0x00000000};
						enum {Value_Enable = 0x1};
						enum {ValueMask_Enable = 0x00000080};
						};
					namespace DCC { // Field Description
						enum {Lsb = 10};
						enum {FieldMask = 0x00000400};
						enum {Value_Disable = 0x0};
						enum {ValueMask_Disable = 0x00000000};
						enum {Value_Enable = 0x1};
						enum {ValueMask_Enable = 0x00000400};
						};
					namespace GLT { // Field Description
						enum {Lsb = 11};
						enum {FieldMask = 0x00000800};
						enum {Value_Disable = 0x0};
						enum {ValueMask_Disable = 0x00000000};
						enum {Value_Enable = 0x1};
						enum {ValueMask_Enable = 0x00000800};
						};
					namespace GLR { // Field Description
						enum {Lsb = 12};
						enum {FieldMask = 0x00001000};
						enum {Value_Disable = 0x0};
						enum {ValueMask_Disable = 0x00000000};
						enum {Value_Enable = 0x1};
						enum {ValueMask_Enable = 0x00001000};
						};
					};
				namespace ENET { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x0000FFFF};
					namespace RXB { // Field Description
						enum {Lsb = 0};
						enum {FieldMask = 0x00000001};
						enum {Value_Disable = 0x0};
						enum {ValueMask_Disable = 0x00000000};
						enum {Value_Enable = 0x1};
						enum {ValueMask_Enable = 0x00000001};
						};
					namespace TXB { // Field Description
						enum {Lsb = 1};
						enum {FieldMask = 0x00000002};
						enum {Value_Disable = 0x0};
						enum {ValueMask_Disable = 0x00000000};
						enum {Value_Enable = 0x1};
						enum {ValueMask_Enable = 0x00000002};
						};
					namespace BSY { // Field Description
						enum {Lsb = 2};
						enum {FieldMask = 0x00000004};
						enum {Value_Disable = 0x0};
						enum {ValueMask_Disable = 0x00000000};
						enum {Value_Enable = 0x1};
						enum {ValueMask_Enable = 0x00000004};
						};
					namespace RXF { // Field Description
						enum {Lsb = 3};
						enum {FieldMask = 0x00000008};
						enum {Value_Disable = 0x0};
						enum {ValueMask_Disable = 0x00000000};
						enum {Value_Enable = 0x1};
						enum {ValueMask_Enable = 0x00000008};
						};
					namespace TXE { // Field Description
						enum {Lsb = 4};
						enum {FieldMask = 0x00000010};
						enum {Value_Disable = 0x0};
						enum {ValueMask_Disable = 0x00000000};
						enum {Value_Enable = 0x1};
						enum {ValueMask_Enable = 0x00000010};
						};
					namespace GRA { // Field Description
						enum {Lsb = 7};
						enum {FieldMask = 0x00000080};
						enum {Value_Disable = 0x0};
						enum {ValueMask_Disable = 0x00000000};
						enum {Value_Enable = 0x1};
						enum {ValueMask_Enable = 0x00000080};
						};
					};
				};
			namespace SCCS { // Register description
				typedef uint8_t	Reg;
				namespace HDLC { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x000000FF};
					namespace ID { // Field Description
						enum {Lsb = 0};
						enum {FieldMask = 0x00000001};
						enum {Value_Busy = 0x0};
						enum {ValueMask_Busy = 0x00000000};
						enum {Value_Idle = 0x1};
						enum {ValueMask_Idle = 0x00000001};
						};
					namespace CS { // Field Description
						enum {Lsb = 0};
						enum {FieldMask = 0x00000001};
						enum {Value_NoCarrierPresent = 0x0};
						enum {ValueMask_NoCarrierPresent = 0x00000000};
						enum {Value_CarrierPresent = 0x1};
						enum {ValueMask_CarrierPresent = 0x00000001};
						};
					namespace FG { // Field Description
						enum {Lsb = 0};
						enum {FieldMask = 0x00000001};
						enum {Value_NoFlagReceived = 0x0};
						enum {ValueMask_NoFlagReceived = 0x00000000};
						enum {Value_FlagReceived = 0x1};
						enum {ValueMask_FlagReceived = 0x00000001};
						};
					};
				namespace AHDLC { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x000000FF};
					namespace ID { // Field Description
						enum {Lsb = 0};
						enum {FieldMask = 0x00000001};
						enum {Value_Busy = 0x0};
						enum {ValueMask_Busy = 0x00000000};
						enum {Value_Idle = 0x1};
						enum {ValueMask_Idle = 0x00000001};
						};
					};
				namespace TRANS { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x000000FF};
					namespace CS { // Field Description
						enum {Lsb = 1};
						enum {FieldMask = 0x00000002};
						enum {Value_NoCarrierPresent = 0x0};
						enum {ValueMask_NoCarrierPresent = 0x00000000};
						enum {Value_CarrierPresent = 0x1};
						enum {ValueMask_CarrierPresent = 0x00000002};
						};
					};
				namespace ENET { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x000000FF};
					namespace CS { // Field Description
						enum {Lsb = 1};
						enum {FieldMask = 0x00000002};
						enum {Value_NoCarrierPresent = 0x0};
						enum {ValueMask_NoCarrierPresent = 0x00000000};
						enum {Value_CarrierPresent = 0x1};
						enum {ValueMask_CarrierPresent = 0x00000002};
						};
					};
				};
			namespace IRMODE { // Register description
				typedef uint16_t	Reg;
				};
			namespace IRSIP { // Register description
				typedef uint16_t	Reg;
				};
			namespace PRAM { // Namespace description
				namespace RBASE { // Register description
					typedef uint16_t	Reg;
					enum {Min = 0};
					enum {Max = 65535};
					};
				namespace TBASE { // Register description
					typedef uint16_t	Reg;
					enum {Min = 0};
					enum {Max = 65535};
					};
				namespace FCR { // Register description
					typedef uint8_t	Reg;
					namespace AT { // Field Description
						enum {Lsb = 0};
						enum {FieldMask = 0x00000007};
						enum {Value_Min = 0x0};
						enum {ValueMask_Min = 0x00000000};
						enum {Value_Max = 0x7};
						enum {ValueMask_Max = 0x00000007};
						};
					namespace BO { // Field Description
						enum {Lsb = 3};
						enum {FieldMask = 0x00000018};
						enum {Value_PpcLittleEndian = 0x1};
						enum {ValueMask_PpcLittleEndian = 0x00000008};
						enum {Value_BigEndian = 0x2};
						enum {ValueMask_BigEndian = 0x00000010};
						enum {Value_TrueLittleEndian = 0x2};
						enum {ValueMask_TrueLittleEndian = 0x00000010};
						};
					};
				namespace MRBLR { // Register description
					typedef uint16_t	Reg;
					enum {Min = 1};
					enum {EtherAlign = 4};
					enum {HdlcAlign = 4};
					enum {Max = 65535};
					};
				namespace RSTATE { // Register description
					typedef uint32_t	Reg;
					};
				namespace RPTR { // Register description
					typedef uint32_t	Reg;
					};
				namespace RBPTR { // Register description
					typedef uint16_t	Reg;
					};
				namespace RCOUNT { // Register description
					typedef uint16_t	Reg;
					};
				namespace RTEMP { // Register description
					typedef uint32_t	Reg;
					};
				namespace TSTATE { // Register description
					typedef uint32_t	Reg;
					};
				namespace TPTR { // Register description
					typedef uint32_t	Reg;
					};
				namespace TBPTR { // Register description
					typedef uint16_t	Reg;
					};
				namespace TCOUNT { // Register description
					typedef uint16_t	Reg;
					};
				namespace TTEMP { // Register description
					typedef uint32_t	Reg;
					};
				namespace UART { // Namespace description
					namespace MAX_IDL { // Register description
						typedef uint16_t	Reg;
						enum {Disable = 0};
						enum {Min = 1};
						enum {Max = 65535};
						};
					namespace IDLC { // Register description
						typedef uint16_t	Reg;
						};
					namespace BRKCR { // Register description
						typedef uint16_t	Reg;
						};
					namespace PAREC { // Register description
						typedef uint16_t	Reg;
						};
					namespace FRMEC { // Register description
						typedef uint16_t	Reg;
						};
					namespace NOSEC { // Register description
						typedef uint16_t	Reg;
						};
					namespace BRKEC { // Register description
						typedef uint16_t	Reg;
						};
					namespace BRKLN { // Register description
						typedef uint16_t	Reg;
						};
					namespace UADDR { // Register description
						typedef uint16_t	Reg;
						};
					namespace RTEMP { // Register description
						typedef uint16_t	Reg;
						};
					namespace TOSEQ { // Register description
						typedef uint16_t	Reg;
						namespace CHARSEND { // Field Description
							enum {Lsb = 0};
							enum {FieldMask = 0x000000FF};
							enum {Value_Min = 0x0};
							enum {ValueMask_Min = 0x00000000};
							enum {Value_Max = 0xFF};
							enum {ValueMask_Max = 0x000000FF};
							};
						namespace A { // Field Description
							enum {Lsb = 8};
							enum {FieldMask = 0x00000100};
							enum {Value_Data = 0x0};
							enum {ValueMask_Data = 0x00000000};
							enum {Value_Address = 0x1};
							enum {ValueMask_Address = 0x00000100};
							};
						namespace CT { // Field Description
							enum {Lsb = 11};
							enum {FieldMask = 0x00000800};
							enum {Value_CtsNotLost = 0x0};
							enum {ValueMask_CtsNotLost = 0x00000000};
							enum {Value_CtsLost = 0x1};
							enum {ValueMask_CtsLost = 0x00000800};
							};
						namespace I { // Field Description
							enum {Lsb = 12};
							enum {FieldMask = 0x00001000};
							enum {Value_Disable = 0x0};
							enum {ValueMask_Disable = 0x00000000};
							enum {Value_Enable = 0x1};
							enum {ValueMask_Enable = 0x00001000};
							};
						namespace REA { // Field Description
							enum {Lsb = 13};
							enum {FieldMask = 0x00002000};
							enum {Value_Transmit = 0x1};
							enum {ValueMask_Transmit = 0x00002000};
							enum {Value_Done = 0x0};
							enum {ValueMask_Done = 0x00000000};
							enum {Value_Busy = 0x1};
							enum {ValueMask_Busy = 0x00002000};
							};
						};
					namespace CHARACTER { // Register description
						typedef uint16_t	Reg;
						namespace CHARACTER { // Field Description
							enum {Lsb = 0};
							enum {FieldMask = 0x000000FF};
							enum {Value_Min = 0x0};
							enum {ValueMask_Min = 0x00000000};
							enum {Value_Max = 0xFF};
							enum {ValueMask_Max = 0x000000FF};
							};
						namespace R { // Field Description
							enum {Lsb = 14};
							enum {FieldMask = 0x00004000};
							enum {Value_ToRxBuffer = 0x0};
							enum {ValueMask_ToRxBuffer = 0x00000000};
							enum {Value_ToRCCR = 0x1};
							enum {ValueMask_ToRCCR = 0x00004000};
							};
						namespace E { // Field Description
							enum {Lsb = 15};
							enum {FieldMask = 0x00008000};
							enum {Value_Valid = 0x0};
							enum {ValueMask_Valid = 0x00000000};
							enum {Value_NotValid = 0x1};
							enum {ValueMask_NotValid = 0x00008000};
							};
						};
					namespace RCCM { // Register description
						typedef uint16_t	Reg;
						namespace END { // Field Description
							enum {Lsb = 14};
							enum {FieldMask = 0x0000C000};
							enum {Value_Always = 0x3};
							enum {ValueMask_Always = 0x0000C000};
							};
						namespace CHARACTER1 { // Field Description
							enum {Lsb = 0};
							enum {FieldMask = 0x00000001};
							enum {Value_Ignore = 0x0};
							enum {ValueMask_Ignore = 0x00000000};
							enum {Value_Compare = 0x1};
							enum {ValueMask_Compare = 0x00000001};
							};
						namespace CHARACTER2 { // Field Description
							enum {Lsb = 1};
							enum {FieldMask = 0x00000002};
							enum {Value_Ignore = 0x0};
							enum {ValueMask_Ignore = 0x00000000};
							enum {Value_Compare = 0x1};
							enum {ValueMask_Compare = 0x00000002};
							};
						namespace CHARACTER3 { // Field Description
							enum {Lsb = 2};
							enum {FieldMask = 0x00000004};
							enum {Value_Ignore = 0x0};
							enum {ValueMask_Ignore = 0x00000000};
							enum {Value_Compare = 0x1};
							enum {ValueMask_Compare = 0x00000004};
							};
						namespace CHARACTER4 { // Field Description
							enum {Lsb = 3};
							enum {FieldMask = 0x00000008};
							enum {Value_Ignore = 0x0};
							enum {ValueMask_Ignore = 0x00000000};
							enum {Value_Compare = 0x1};
							enum {ValueMask_Compare = 0x00000008};
							};
						namespace CHARACTER5 { // Field Description
							enum {Lsb = 4};
							enum {FieldMask = 0x00000010};
							enum {Value_Ignore = 0x0};
							enum {ValueMask_Ignore = 0x00000000};
							enum {Value_Compare = 0x1};
							enum {ValueMask_Compare = 0x00000010};
							};
						namespace CHARACTER6 { // Field Description
							enum {Lsb = 5};
							enum {FieldMask = 0x00000020};
							enum {Value_Ignore = 0x0};
							enum {ValueMask_Ignore = 0x00000000};
							enum {Value_Compare = 0x1};
							enum {ValueMask_Compare = 0x00000020};
							};
						namespace CHARACTER7 { // Field Description
							enum {Lsb = 6};
							enum {FieldMask = 0x00000040};
							enum {Value_Ignore = 0x0};
							enum {ValueMask_Ignore = 0x00000000};
							enum {Value_Compare = 0x1};
							enum {ValueMask_Compare = 0x00000040};
							};
						namespace CHARACTER8 { // Field Description
							enum {Lsb = 7};
							enum {FieldMask = 0x00000080};
							enum {Value_Ignore = 0x0};
							enum {ValueMask_Ignore = 0x00000000};
							enum {Value_Compare = 0x1};
							enum {ValueMask_Compare = 0x00000080};
							};
						};
					namespace RCCR { // Register description
						typedef uint16_t	Reg;
						namespace Character { // Field Description
							enum {Lsb = 0};
							enum {FieldMask = 0x000000FF};
							enum {Value_Min = 0x0};
							enum {ValueMask_Min = 0x00000000};
							enum {Value_Max = 0xFF};
							enum {ValueMask_Max = 0x000000FF};
							};
						};
					namespace RLBC { // Register description
						typedef uint16_t	Reg;
						};
					};
				namespace HDLC { // Namespace description
					namespace C_MASK { // Register description
						typedef uint32_t	Reg;
						enum {Ccitt16 = 61624};
						enum {Ccitt32 = -558161693};
						};
					namespace C_PRES { // Register description
						typedef uint32_t	Reg;
						enum {Ccitt16 = 65535};
						enum {Ccitt32 = -1};
						};
					namespace DISFC { // Register description
						typedef uint16_t	Reg;
						enum {Min = 0};
						enum {Max = 65535};
						};
					namespace CRCEC { // Register description
						typedef uint16_t	Reg;
						enum {Min = 0};
						enum {Max = 65535};
						};
					namespace ABTSC { // Register description
						typedef uint16_t	Reg;
						enum {Min = 0};
						enum {Max = 65535};
						};
					namespace NMARC { // Register description
						typedef uint16_t	Reg;
						enum {Min = 0};
						enum {Max = 65535};
						};
					namespace RETRC { // Register description
						typedef uint16_t	Reg;
						enum {Min = 0};
						enum {Max = 65535};
						};
					namespace MFLR { // Register description
						typedef uint16_t	Reg;
						enum {Min = 0};
						enum {Max = 65535};
						};
					namespace MAX_CNT { // Register description
						typedef uint16_t	Reg;
						enum {Min = 0};
						enum {Max = 65535};
						};
					namespace RFTHR { // Register description
						typedef uint16_t	Reg;
						enum {Min = 0};
						enum {Max = 65535};
						};
					namespace RFCNT { // Register description
						typedef uint16_t	Reg;
						};
					namespace HMASK { // Register description
						typedef uint16_t	Reg;
						enum {Addr16Bit = 65535};
						enum {Addr8Bit = 255};
						};
					namespace HADDR { // Register description
						typedef uint16_t	Reg;
						namespace FirstOctet { // Field Description
							enum {Lsb = 0};
							enum {FieldMask = 0x000000FF};
							enum {Value_Min = 0x0};
							enum {ValueMask_Min = 0x00000000};
							enum {Value_Max = 0xFF};
							enum {ValueMask_Max = 0x000000FF};
							};
						namespace SecondOctet { // Field Description
							enum {Lsb = 8};
							enum {FieldMask = 0x0000FF00};
							enum {Value_Min = 0x0};
							enum {ValueMask_Min = 0x00000000};
							enum {Value_Max = 0xFF};
							enum {ValueMask_Max = 0x0000FF00};
							};
						};
					namespace TMP { // Register description
						typedef uint16_t	Reg;
						};
					namespace TMP_MB { // Register description
						typedef uint16_t	Reg;
						};
					};
				namespace AHDLC { // Namespace description
					namespace C_MASK { // Register description
						typedef uint32_t	Reg;
						enum {Ccitt16 = 61624};
						};
					namespace C_PRES { // Register description
						typedef uint32_t	Reg;
						enum {Ccitt16 = 65535};
						};
					namespace BOF { // Register description
						typedef uint16_t	Reg;
						enum {PPP = 126};
						enum {IrLAP = 192};
						};
					namespace EoF { // Register description
						typedef uint16_t	Reg;
						enum {PPP = 126};
						enum {IrLAP = 193};
						};
					namespace ESC { // Register description
						typedef uint16_t	Reg;
						enum {PPP = 125};
						enum {IrLAP = 125};
						};
					namespace ZERO { // Register description
						typedef uint16_t	Reg;
						enum {Clear = 0};
						};
					namespace RFTHR { // Register description
						typedef uint16_t	Reg;
						enum {Min = 0};
						enum {Max = 65535};
						};
					namespace TXCTL_TBL { // Register description
						typedef uint32_t	Reg;
						namespace Char1F { // Field Description
							enum {Lsb = 31};
							enum {FieldMask = 0x80000000};
							enum {Value_MappingNotRequired = 0x0};
							enum {ValueMask_MappingNotRequired = 0x00000000};
							enum {Value_MappingRequired = 0x1};
							enum {ValueMask_MappingRequired = 0x80000000};
							};
						namespace Char1E { // Field Description
							enum {Lsb = 30};
							enum {FieldMask = 0x40000000};
							enum {Value_MappingNotRequired = 0x0};
							enum {ValueMask_MappingNotRequired = 0x00000000};
							enum {Value_MappingRequired = 0x1};
							enum {ValueMask_MappingRequired = 0x40000000};
							};
						namespace Char1D { // Field Description
							enum {Lsb = 29};
							enum {FieldMask = 0x20000000};
							enum {Value_MappingNotRequired = 0x0};
							enum {ValueMask_MappingNotRequired = 0x00000000};
							enum {Value_MappingRequired = 0x1};
							enum {ValueMask_MappingRequired = 0x20000000};
							};
						namespace Char1C { // Field Description
							enum {Lsb = 28};
							enum {FieldMask = 0x10000000};
							enum {Value_MappingNotRequired = 0x0};
							enum {ValueMask_MappingNotRequired = 0x00000000};
							enum {Value_MappingRequired = 0x1};
							enum {ValueMask_MappingRequired = 0x10000000};
							};
						namespace Char1B { // Field Description
							enum {Lsb = 27};
							enum {FieldMask = 0x08000000};
							enum {Value_MappingNotRequired = 0x0};
							enum {ValueMask_MappingNotRequired = 0x00000000};
							enum {Value_MappingRequired = 0x1};
							enum {ValueMask_MappingRequired = 0x08000000};
							};
						namespace Char1A { // Field Description
							enum {Lsb = 26};
							enum {FieldMask = 0x04000000};
							enum {Value_MappingNotRequired = 0x0};
							enum {ValueMask_MappingNotRequired = 0x00000000};
							enum {Value_MappingRequired = 0x1};
							enum {ValueMask_MappingRequired = 0x04000000};
							};
						namespace Char19 { // Field Description
							enum {Lsb = 25};
							enum {FieldMask = 0x02000000};
							enum {Value_MappingNotRequired = 0x0};
							enum {ValueMask_MappingNotRequired = 0x00000000};
							enum {Value_MappingRequired = 0x1};
							enum {ValueMask_MappingRequired = 0x02000000};
							};
						namespace Char18 { // Field Description
							enum {Lsb = 24};
							enum {FieldMask = 0x01000000};
							enum {Value_MappingNotRequired = 0x0};
							enum {ValueMask_MappingNotRequired = 0x00000000};
							enum {Value_MappingRequired = 0x1};
							enum {ValueMask_MappingRequired = 0x01000000};
							};
						namespace Char17 { // Field Description
							enum {Lsb = 23};
							enum {FieldMask = 0x00800000};
							enum {Value_MappingNotRequired = 0x0};
							enum {ValueMask_MappingNotRequired = 0x00000000};
							enum {Value_MappingRequired = 0x1};
							enum {ValueMask_MappingRequired = 0x00800000};
							};
						namespace Char16 { // Field Description
							enum {Lsb = 22};
							enum {FieldMask = 0x00400000};
							enum {Value_MappingNotRequired = 0x0};
							enum {ValueMask_MappingNotRequired = 0x00000000};
							enum {Value_MappingRequired = 0x1};
							enum {ValueMask_MappingRequired = 0x00400000};
							};
						namespace Char15 { // Field Description
							enum {Lsb = 21};
							enum {FieldMask = 0x00200000};
							enum {Value_MappingNotRequired = 0x0};
							enum {ValueMask_MappingNotRequired = 0x00000000};
							enum {Value_MappingRequired = 0x1};
							enum {ValueMask_MappingRequired = 0x00200000};
							};
						namespace Char14 { // Field Description
							enum {Lsb = 20};
							enum {FieldMask = 0x00100000};
							enum {Value_MappingNotRequired = 0x0};
							enum {ValueMask_MappingNotRequired = 0x00000000};
							enum {Value_MappingRequired = 0x1};
							enum {ValueMask_MappingRequired = 0x00100000};
							};
						namespace Char13 { // Field Description
							enum {Lsb = 19};
							enum {FieldMask = 0x00080000};
							enum {Value_MappingNotRequired = 0x0};
							enum {ValueMask_MappingNotRequired = 0x00000000};
							enum {Value_MappingRequired = 0x1};
							enum {ValueMask_MappingRequired = 0x00080000};
							};
						namespace Char12 { // Field Description
							enum {Lsb = 18};
							enum {FieldMask = 0x00040000};
							enum {Value_MappingNotRequired = 0x0};
							enum {ValueMask_MappingNotRequired = 0x00000000};
							enum {Value_MappingRequired = 0x1};
							enum {ValueMask_MappingRequired = 0x00040000};
							};
						namespace Char11 { // Field Description
							enum {Lsb = 17};
							enum {FieldMask = 0x00020000};
							enum {Value_MappingNotRequired = 0x0};
							enum {ValueMask_MappingNotRequired = 0x00000000};
							enum {Value_MappingRequired = 0x1};
							enum {ValueMask_MappingRequired = 0x00020000};
							};
						namespace Char10 { // Field Description
							enum {Lsb = 16};
							enum {FieldMask = 0x00010000};
							enum {Value_MappingNotRequired = 0x0};
							enum {ValueMask_MappingNotRequired = 0x00000000};
							enum {Value_MappingRequired = 0x1};
							enum {ValueMask_MappingRequired = 0x00010000};
							};
						namespace Char0F { // Field Description
							enum {Lsb = 15};
							enum {FieldMask = 0x00008000};
							enum {Value_MappingNotRequired = 0x0};
							enum {ValueMask_MappingNotRequired = 0x00000000};
							enum {Value_MappingRequired = 0x1};
							enum {ValueMask_MappingRequired = 0x00008000};
							};
						namespace Char0E { // Field Description
							enum {Lsb = 14};
							enum {FieldMask = 0x00004000};
							enum {Value_MappingNotRequired = 0x0};
							enum {ValueMask_MappingNotRequired = 0x00000000};
							enum {Value_MappingRequired = 0x1};
							enum {ValueMask_MappingRequired = 0x00004000};
							};
						namespace Char0D { // Field Description
							enum {Lsb = 13};
							enum {FieldMask = 0x00002000};
							enum {Value_MappingNotRequired = 0x0};
							enum {ValueMask_MappingNotRequired = 0x00000000};
							enum {Value_MappingRequired = 0x1};
							enum {ValueMask_MappingRequired = 0x00002000};
							};
						namespace Char0C { // Field Description
							enum {Lsb = 12};
							enum {FieldMask = 0x00001000};
							enum {Value_MappingNotRequired = 0x0};
							enum {ValueMask_MappingNotRequired = 0x00000000};
							enum {Value_MappingRequired = 0x1};
							enum {ValueMask_MappingRequired = 0x00001000};
							};
						namespace Char0B { // Field Description
							enum {Lsb = 11};
							enum {FieldMask = 0x00000800};
							enum {Value_MappingNotRequired = 0x0};
							enum {ValueMask_MappingNotRequired = 0x00000000};
							enum {Value_MappingRequired = 0x1};
							enum {ValueMask_MappingRequired = 0x00000800};
							};
						namespace Char0A { // Field Description
							enum {Lsb = 10};
							enum {FieldMask = 0x00000400};
							enum {Value_MappingNotRequired = 0x0};
							enum {ValueMask_MappingNotRequired = 0x00000000};
							enum {Value_MappingRequired = 0x1};
							enum {ValueMask_MappingRequired = 0x00000400};
							};
						namespace Char09 { // Field Description
							enum {Lsb = 9};
							enum {FieldMask = 0x00000200};
							enum {Value_MappingNotRequired = 0x0};
							enum {ValueMask_MappingNotRequired = 0x00000000};
							enum {Value_MappingRequired = 0x1};
							enum {ValueMask_MappingRequired = 0x00000200};
							};
						namespace Char08 { // Field Description
							enum {Lsb = 8};
							enum {FieldMask = 0x00000100};
							enum {Value_MappingNotRequired = 0x0};
							enum {ValueMask_MappingNotRequired = 0x00000000};
							enum {Value_MappingRequired = 0x1};
							enum {ValueMask_MappingRequired = 0x00000100};
							};
						namespace Char07 { // Field Description
							enum {Lsb = 7};
							enum {FieldMask = 0x00000080};
							enum {Value_MappingNotRequired = 0x0};
							enum {ValueMask_MappingNotRequired = 0x00000000};
							enum {Value_MappingRequired = 0x1};
							enum {ValueMask_MappingRequired = 0x00000080};
							};
						namespace Char06 { // Field Description
							enum {Lsb = 6};
							enum {FieldMask = 0x00000040};
							enum {Value_MappingNotRequired = 0x0};
							enum {ValueMask_MappingNotRequired = 0x00000000};
							enum {Value_MappingRequired = 0x1};
							enum {ValueMask_MappingRequired = 0x00000040};
							};
						namespace Char05 { // Field Description
							enum {Lsb = 5};
							enum {FieldMask = 0x00000020};
							enum {Value_MappingNotRequired = 0x0};
							enum {ValueMask_MappingNotRequired = 0x00000000};
							enum {Value_MappingRequired = 0x1};
							enum {ValueMask_MappingRequired = 0x00000020};
							};
						namespace Char04 { // Field Description
							enum {Lsb = 4};
							enum {FieldMask = 0x00000010};
							enum {Value_MappingNotRequired = 0x0};
							enum {ValueMask_MappingNotRequired = 0x00000000};
							enum {Value_MappingRequired = 0x1};
							enum {ValueMask_MappingRequired = 0x00000010};
							};
						namespace Char03 { // Field Description
							enum {Lsb = 3};
							enum {FieldMask = 0x00000008};
							enum {Value_MappingNotRequired = 0x0};
							enum {ValueMask_MappingNotRequired = 0x00000000};
							enum {Value_MappingRequired = 0x1};
							enum {ValueMask_MappingRequired = 0x00000008};
							};
						namespace Char02 { // Field Description
							enum {Lsb = 2};
							enum {FieldMask = 0x00000004};
							enum {Value_MappingNotRequired = 0x0};
							enum {ValueMask_MappingNotRequired = 0x00000000};
							enum {Value_MappingRequired = 0x1};
							enum {ValueMask_MappingRequired = 0x00000004};
							};
						namespace Char01 { // Field Description
							enum {Lsb = 1};
							enum {FieldMask = 0x00000002};
							enum {Value_MappingNotRequired = 0x0};
							enum {ValueMask_MappingNotRequired = 0x00000000};
							enum {Value_MappingRequired = 0x1};
							enum {ValueMask_MappingRequired = 0x00000002};
							};
						namespace Char00 { // Field Description
							enum {Lsb = 0};
							enum {FieldMask = 0x00000001};
							enum {Value_MappingNotRequired = 0x0};
							enum {ValueMask_MappingNotRequired = 0x00000000};
							enum {Value_MappingRequired = 0x1};
							enum {ValueMask_MappingRequired = 0x00000001};
							};
						};
					namespace MAX_CNT { // Register description
						typedef uint16_t	Reg;
						enum {Min = 0};
						enum {Max = 65535};
						};
					namespace RFCNT { // Register description
						typedef uint16_t	Reg;
						};
					namespace HMASK { // Register description
						typedef uint16_t	Reg;
						enum {Addr16Bit = 65535};
						enum {Addr8Bit = 255};
						};
					namespace HADDR { // Register description
						typedef uint16_t	Reg;
						namespace FirstOctet { // Field Description
							enum {Lsb = 0};
							enum {FieldMask = 0x000000FF};
							enum {Value_Min = 0x0};
							enum {ValueMask_Min = 0x00000000};
							enum {Value_Max = 0xFF};
							enum {ValueMask_Max = 0x000000FF};
							};
						namespace SecondOctet { // Field Description
							enum {Lsb = 8};
							enum {FieldMask = 0x0000FF00};
							enum {Value_Min = 0x0};
							enum {ValueMask_Min = 0x00000000};
							enum {Value_Max = 0xFF};
							enum {ValueMask_Max = 0x0000FF00};
							};
						};
					namespace TMP { // Register description
						typedef uint16_t	Reg;
						};
					namespace TMP_MB { // Register description
						typedef uint16_t	Reg;
						};
					};
				namespace BISYNC { // Namespace description
					namespace CRCC { // Register description
						typedef uint32_t	Reg;
						};
					namespace PCRC { // Register description
						typedef uint16_t	Reg;
						enum {LRC = 65535};
						enum {Crc16 = 0};
						};
					namespace PAREC { // Register description
						typedef uint16_t	Reg;
						enum {Min = 0};
						enum {Max = 65535};
						};
					namespace BSYNC { // Register description
						typedef uint16_t	Reg;
						namespace SYNC { // Field Description
							enum {Lsb = 0};
							enum {FieldMask = 0x000000FF};
							};
						namespace V { // Field Description
							enum {Lsb = 15};
							enum {FieldMask = 0x00008000};
							enum {Value_NotValid = 0x0};
							enum {ValueMask_NotValid = 0x00000000};
							enum {Value_Valid = 0x1};
							enum {ValueMask_Valid = 0x00008000};
							};
						};
					namespace BDLE { // Register description
						typedef uint16_t	Reg;
						namespace DLE { // Field Description
							enum {Lsb = 0};
							enum {FieldMask = 0x000000FF};
							};
						namespace V { // Field Description
							enum {Lsb = 15};
							enum {FieldMask = 0x00008000};
							enum {Value_NotValid = 0x0};
							enum {ValueMask_NotValid = 0x00000000};
							enum {Value_Valid = 0x1};
							enum {ValueMask_Valid = 0x00008000};
							};
						};
					namespace CHARACTER { // Register description
						typedef uint16_t	Reg;
						};
					namespace RCCM { // Register description
						typedef uint16_t	Reg;
						};
					};
				namespace ENET { // Namespace description
					namespace C_PRES { // Register description
						typedef uint32_t	Reg;
						enum {CrcCcitt32 = -1};
						};
					namespace C_MASK { // Register description
						typedef uint32_t	Reg;
						enum {CrcCcitt32 = -558161693};
						};
					namespace CRCEC { // Register description
						typedef uint32_t	Reg;
						enum {Min = 0};
						enum {Max = -1};
						};
					namespace ALEC { // Register description
						typedef uint32_t	Reg;
						enum {Min = 0};
						enum {Max = -1};
						};
					namespace DISFC { // Register description
						typedef uint32_t	Reg;
						enum {Min = 0};
						enum {Max = -1};
						};
					namespace PADS { // Register description
						typedef uint16_t	Reg;
						enum {Default = 0};
						};
					namespace RET_LIM { // Register description
						typedef uint16_t	Reg;
						enum {Typical = 15};
						};
					namespace RET_CNT { // Register description
						typedef uint16_t	Reg;
						};
					namespace MFLR { // Register description
						typedef uint16_t	Reg;
						enum {Typical = 1518};
						};
					namespace MINFLR { // Register description
						typedef uint16_t	Reg;
						enum {Typical = 64};
						};
					namespace MAXD1 { // Register description
						typedef uint16_t	Reg;
						enum {Typical = 1520};
						};
					namespace MAXD2 { // Register description
						typedef uint16_t	Reg;
						enum {Typical = 1520};
						};
					namespace MAXD { // Register description
						typedef uint16_t	Reg;
						};
					namespace DMA_CNT { // Register description
						typedef uint16_t	Reg;
						};
					namespace MAX_B { // Register description
						typedef uint16_t	Reg;
						};
					namespace GADDR { // Register description
						typedef uint16_t	Reg;
						enum {Initial = 0};
						};
					namespace TBUF_DATA0 { // Register description
						typedef uint32_t	Reg;
						};
					namespace TBUF_DATA1 { // Register description
						typedef uint32_t	Reg;
						};
					namespace TBUF_RBA0 { // Register description
						typedef uint32_t	Reg;
						};
					namespace TBUF_CRC { // Register description
						typedef uint32_t	Reg;
						};
					namespace TBUF_BCNT { // Register description
						typedef uint16_t	Reg;
						};
					namespace PADDR1_H { // Register description
						typedef uint16_t	Reg;
						};
					namespace PADDR1_M { // Register description
						typedef uint16_t	Reg;
						};
					namespace PADDR1_L { // Register description
						typedef uint16_t	Reg;
						};
					namespace P_PER { // Register description
						typedef uint16_t	Reg;
						enum {Normal = 0};
						enum {Min = 0};
						enum {Max = 9};
						enum {MostAggessive = 1};
						enum {LeastAggessive = 9};
						};
					namespace RFBD_PTR { // Register description
						typedef uint16_t	Reg;
						};
					namespace TFBD_PTR { // Register description
						typedef uint16_t	Reg;
						};
					namespace TLBD_PTR { // Register description
						typedef uint16_t	Reg;
						};
					namespace TX_LEN { // Register description
						typedef uint16_t	Reg;
						};
					namespace IADDR { // Register description
						typedef uint16_t	Reg;
						};
					namespace BOFF_CNT { // Register description
						typedef uint16_t	Reg;
						};
					namespace TADDR_H { // Register description
						typedef uint16_t	Reg;
						};
					namespace TADDR_M { // Register description
						typedef uint16_t	Reg;
						};
					namespace TADDR_L { // Register description
						typedef uint16_t	Reg;
						};
					};
				namespace TRANS { // Namespace description
					namespace CRC_P { // Register description
						typedef uint32_t	Reg;
						enum {Ccitt16 = 65535};
						enum {Ccitt32 = -1};
						};
					namespace CRC_C { // Register description
						typedef uint32_t	Reg;
						enum {Ccitt16 = 61624};
						enum {Ccitt32 = -558161693};
						};
					};
				};
			};
		};
	};
#endif
