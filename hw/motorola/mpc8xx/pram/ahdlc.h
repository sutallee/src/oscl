/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_hw_motorola_mpc8xx_pram_ahdlch_
#define _oscl_hw_motorola_mpc8xx_pram_ahdlch_
#include <stdint.h>
#include "oscl/bits/bitfield.h"
#include "header.h"

/** */
namespace Oscl {
/** */
namespace Mot8xx {
/** */
namespace Pram {
/** */
namespace SCC {

/** */
typedef struct AHDLC {
	/** */
	Header		header;
	/** */
	uint32_t	rcrc;
	/** */
	uint32_t	tcrc;
	/** */
	uint32_t	reserved;
	/** */
	uint32_t	c_mask;	// User
	/** */
	uint32_t	c_pres;	// User
	/** */
	uint16_t	bof;	// User
	/** */
	uint16_t	eof;	// User
	/** */
	uint16_t	esc;	// User
	/** */
	uint32_t	reserved2;
	/** */
	uint16_t	zero;	// User
	/** */
	uint16_t	reserved3;
	/** */
	uint16_t	rfthr;	// User
	/** */
	uint32_t	reserved4;
	/** */
	uint32_t	txctl_tbl;	// User
	/** */
	uint32_t	rxctl_tbl;	// User
	/** */
	uint16_t	nof;		// User
	/** */
	uint16_t	reserved5;
	} AHDLC;

}
}
}
}

#endif
