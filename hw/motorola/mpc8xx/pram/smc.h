/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_hw_motorola_mpc8xx_pram_smch_
#define _oscl_hw_motorola_mpc8xx_pram_smch_
#include <stdint.h>
#include "oscl/bits/bitfield.h"
#include "header.h"

/** */
namespace Oscl {
/** */
namespace Mot8xx {
/** */
namespace Pram {

/** */
typedef struct SMC {
	/** */
	Header		header;
	/** */
	union {
		/** */
		struct {
			/** */
			uint16_t	max_idl;	// User
			/** */
			uint16_t	idlc;
			/** */
			uint16_t	brkln;
			/** */
			uint16_t	brkec;	// User
			/** */
			uint16_t	brkcr;	// User
			/** */
			uint16_t	r_mask;
			} UART;
		/** */
		struct {
			/** */
			uint16_t	reserved[0x32-0x28];
			} TRANS;
		/** */
		struct {
			/** */
			uint16_t	m_rxbd;	// User
			/** */
			uint16_t	m_txbd;	// User
			/** */
			uint16_t	ci_rxbd;	// User
			/** */
			uint16_t	ci_txbd;	// User
			/** */
			uint32_t	rstate;
			/** */
			uint16_t	m_rxd;
			/** */
			uint16_t	m_txd;
			/** */
			uint16_t	ci_rxd;
			/** */
			uint16_t	ci_txd;
			} GCI;
		unsigned char	pad[0x40-0x28];
		} psa;
	} SMC;

}
}
}

#endif
