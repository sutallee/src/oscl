/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_hw_motorola_mpc8xx_pram_eneth_
#define _oscl_hw_motorola_mpc8xx_pram_eneth_
#include <stdint.h>
#include "oscl/bits/bitfield.h"
#include "header.h"

/** */
namespace Oscl {
/** */
namespace Mot8xx {
/** */
namespace Pram {
/** */
namespace SCC {

/** */
typedef struct ENET {
	/** */
	Header		header;
	/** */
	uint32_t	rcrc;
	/** */
	uint32_t	tcrc;
	/** */
	uint32_t	c_pres;	// User
	/** */
	uint32_t	c_mask;	// User
	/** */
	uint32_t	crcec;	// User
	/** */
	uint32_t	alec;	// User
	/** */
	uint32_t	disfc;	// User
	/** */
	uint16_t	pads;	// User
	/** */
	uint16_t	ret_lim;	// User
	/** */
	uint16_t	ret_cnt;
	/** */
	uint16_t	mflr;	// User
	/** */
	uint16_t	minflr;	// User
	/** */
	uint16_t	maxd1;	// User
	/** */
	uint16_t	maxd2;	// User
	/** */
	uint16_t	maxd;
	/** */
	uint16_t	dma_cnt;
	/** */
	uint16_t	max_b;
	/** */
	uint16_t	gaddr1;	// User
	/** */
	uint16_t	gaddr2;	// User
	/** */
	uint16_t	gaddr3;	// User
	/** */
	uint16_t	gaddr4;	// User
	/** */
	uint32_t	tbuf0_data0;
	/** */
	uint32_t	tbuf0_data1;
	/** */
	uint32_t	tbuf0_rba0;
	/** */
	uint32_t	tbuf0_crc;
	/** */
	uint16_t	tbuf0_bcnt;
	/** */
	uint16_t	paddr1_h;	// User
	/** */
	uint16_t	paddr1_m;	// User
	/** */
	uint16_t	paddr1_l;	// User
	/** */
	uint16_t	p_per;	// User
	/** */
	uint16_t	rfbd_ptr;
	/** */
	uint16_t	tfbd_ptr;
	/** */
	uint16_t	tlbd_ptr;
	/** */
	uint32_t	tbuf1_data0;
	/** */
	uint32_t	tbuf1_data1;
	/** */
	uint32_t	tbuf1_rba0;
	/** */
	uint32_t	tbuf1_crc;
	/** */
	uint16_t	tbuf1_bcnt;
	/** */
	uint16_t	tx_len;
	/** */
	uint16_t	iaddr1;	// User
	/** */
	uint16_t	iaddr2;	// User
	/** */
	uint16_t	iaddr3;	// User
	/** */
	uint16_t	iaddr4;	// User
	/** */
	uint16_t	boff_cnt;
	/** */
	uint16_t	taddr_h;	// User
	/** */
	uint16_t	taddr_m;	// User
	/** */
	uint16_t	taddr_l;	// User
	}ENET;

}
}
}
}

#endif
