/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_hw_motorola_mpc8xx_pram_piph_
#define _oscl_hw_motorola_mpc8xx_pram_piph_
#include <stdint.h>
#include "oscl/bits/bitfield.h"

/** */
namespace Oscl {
/** */
namespace Mot8xx {
/** */
namespace Pram {

/** */
typedef union PIP {
	/** */
	struct {
		/** */
		uint16_t	reserved1;
		/** */
		uint16_t	tbase;	// User
		/** */
		uint8_t	pfcr;	// User
		/** */
		uint8_t	smask;	// User
		/** */
		uint8_t	reserved2[0x18-0x06];
		/** */
		uint32_t	tstate;
		/** */
		uint32_t	t_ptr;
		/** */
		uint16_t	tbptr;
		/** */
		uint16_t	t_cnt;
		/** */
		uint32_t	ttemp;
		}tx;
	/** */
	struct {
		/** */
		uint16_t	rbase;	// User
		/** */
		uint16_t	reserved1;
		/** */
		uint8_t	pfcr;	// User
		/** */
		uint8_t	reserved2;
		/** */
		uint16_t	mrblr;	// User
		/** */
		uint32_t	rstate;
		/** */
		uint32_t	r_ptr;
		/** */
		uint16_t	rbptr;
		/** */
		uint16_t	r_cnt;
		/** */
		uint32_t	rtemp;
		/** */
		uint8_t	reserved3[0x28-0x18];
		/** */
		uint16_t	max_sl;	// User
		/** */
		uint16_t	sl_cnt;
		/** */
		uint16_t	character1;	// User
		/** */
		uint16_t	character2;	// User
		/** */
		uint16_t	character3;	// User
		/** */
		uint16_t	character4;	// User
		/** */
		uint16_t	character5;	// User
		/** */
		uint16_t	character6;	// User
		/** */
		uint16_t	character7;	// User
		/** */
		uint16_t	character8;	// User
		/** */
		uint16_t	rccm;	// User
		/** */
		uint16_t	rccr;
		} rx;
	} PIP;

}
}
}

#endif
