/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_hw_motorola_mpc8xx_pram_transh_
#define _oscl_hw_motorola_mpc8xx_pram_transh_
#include <stdint.h>
#include "oscl/bits/bitfield.h"
#include "header.h"

/** */
namespace Oscl {
/** */
namespace Mot8xx {
/** */
namespace Pram {
/** */
namespace SCC {

/** */
typedef struct TRANS {
	/** */
	Header		header;
	/** */
	uint32_t	rcrc;
	/** */
	uint32_t	tcrc;
	/** */
	uint32_t	crc_p;	// User
	/** */
	uint32_t	crc_c;	// User
	} TRANS;

}
}
}
}

#endif
