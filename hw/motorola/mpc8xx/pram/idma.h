/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_hw_motorola_mpc8xx_pram_idmah_
#define _oscl_hw_motorola_mpc8xx_pram_idmah_
#include <stdint.h>
#include "oscl/bits/bitfield.h"

/** */
namespace Oscl {
/** */
namespace Mot8xx {
/** */
namespace Pram {

/** */
typedef struct IDMA {
	/** */
	uint16_t							ibase;	// User
	/** */
	Oscl::BitField<uint16_t>			dcmr;	// User
	/** */
	uint32_t	sapr;
	/** */
	uint32_t	dapr;
	/** */
	uint16_t	ibptr;
	/** */
	uint16_t	write_sp;
	/** */
	uint32_t	s_byte_c;
	/** */
	uint32_t	d_byte_c;	// 20
	/** */
	uint32_t	s_state;	// 24
	/** */
	uint32_t	itemp[4];	// 28
	/** */
	uint32_t	sr_mem;		// 44
	/** */
	uint16_t	read_sp;	// 48
	/** */
	uint16_t	diff;		// 50
	/** */
	uint16_t	tempaddr;	// 52
	/** */
	uint16_t	sr_mem_byte_count;	// 54
	/** */
	uint32_t	d_state;	// 56
	/** */
	unsigned char	reserved[0x40-60];
	} IDMA;

/** */
typedef struct IDMA_single_buffer {
	/** */
	uint32_t	bapr;	// User
	/** */
	uint32_t	bcr;	// User
	/** */
	uint32_t	dcmr;	// User
	/** */
	char		reserved[0x40-0x0C];
	} IDMA_single_buffer;

}
}
}

#endif
