/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_hw_motorola_mpc8xx_pram_dsph_
#define _oscl_hw_motorola_mpc8xx_pram_dsph_
#include <stdint.h>
#include "oscl/bits/bitfield.h"

/** */
namespace Oscl {
/** */
namespace Mot8xx {
/** */
namespace Pram {

/** */
typedef struct DSP {
	/** */
	uint32_t	fdbase;	// User
	/** */
	uint32_t	fd_ptr;
	/** */
	uint32_t	dstate;
	/** */
	uint32_t	reserved;
	/** */
	uint16_t	dstatus;
	/** */
	uint16_t	i;
	/** */
	uint16_t	tap;
	/** */
	uint16_t	cbase;
	/** */
	uint16_t	samplesize;
	/** */
	uint16_t	xptr;
	/** */
	uint16_t	outputbuffersize;
	/** */
	uint16_t	yptr;
	/** */
	uint16_t	m;
	/** */
	uint16_t	ibdp;
	/** */
	uint16_t	n;
	/** */
	uint16_t	obdp;
	/** */
	uint16_t	k;
	/** */
	uint16_t	cbdp;
	/** */
	unsigned char	pad[0x40-0x2C];
	} DSP;

}
}
}

#endif
