/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _hw_mot8xx_pramregh_
#define _hw_mot8xx_pramregh_
#include <stdint.h>
namespace Oscl { // Namespace description
	namespace Mot8xx { // Namespace description
		namespace PRAM { // Namespace description
			namespace IDMA { // Namespace description
				namespace IBASE { // Register description
					typedef uint16_t	Reg;
					};
				namespace DCMR { // Register description
					typedef uint16_t	Reg;
					};
				namespace SAPR { // Register description
					typedef uint32_t	Reg;
					};
				namespace DAPR { // Register description
					typedef uint32_t	Reg;
					};
				namespace IBPTR { // Register description
					typedef uint16_t	Reg;
					};
				namespace WRITE_SP { // Register description
					typedef uint16_t	Reg;
					};
				namespace S_BYTE_C { // Register description
					typedef uint32_t	Reg;
					};
				namespace S_STATE { // Register description
					typedef uint32_t	Reg;
					};
				namespace ITEMP { // Register description
					typedef uint32_t	Reg;
					};
				namespace SR_MEM { // Register description
					typedef uint32_t	Reg;
					};
				namespace READ_SP { // Register description
					typedef uint32_t	Reg;
					};
				namespace D_STATE { // Register description
					typedef uint32_t	Reg;
					};
				};
			namespace RBASE { // Register description
				typedef uint16_t	Reg;
				};
			namespace TBASE { // Register description
				typedef uint16_t	Reg;
				};
			namespace FCR { // Register description
				typedef uint8_t	Reg;
				namespace AT { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x00000007};
					};
				namespace BO { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x00000007};
					enum {Value_PowerPCLE = 0x1};
					enum {ValueMask_PowerPCLE = 0x00000001};
					enum {Value_BigEndian = 0x2};
					enum {ValueMask_BigEndian = 0x00000002};
					enum {Value_LittleEndian = 0x2};
					enum {ValueMask_LittleEndian = 0x00000002};
					};
				};
			namespace MRBLR { // Register description
				typedef uint16_t	Reg;
				};
			namespace RSTATE { // Register description
				typedef uint32_t	Reg;
				};
			namespace RxDataPtr { // Register description
				typedef uint32_t	Reg;
				};
			namespace RBPTR { // Register description
				typedef uint16_t	Reg;
				};
			namespace RxByteCount { // Register description
				typedef uint16_t	Reg;
				};
			namespace RxTemp { // Register description
				typedef uint32_t	Reg;
				};
			namespace TSTATE { // Register description
				typedef uint32_t	Reg;
				};
			namespace TxDataPtr { // Register description
				typedef uint32_t	Reg;
				};
			namespace TBPTR { // Register description
				typedef uint16_t	Reg;
				};
			namespace TxByteCount { // Register description
				typedef uint16_t	Reg;
				};
			namespace TxTemp { // Register description
				typedef uint32_t	Reg;
				};
			};
		};
	};
#endif
