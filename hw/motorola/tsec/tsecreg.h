#ifndef _oscl_hw_motorola_tsec_tsecregh_
#define _oscl_hw_motorola_tsec_tsecregh_
#include <stdint.h>
namespace Oscl { // Namespace description
	namespace Motorola { // Namespace description
		namespace TSEC { // Namespace description
			namespace TSEC_ID { // Register description
				typedef uint32_t	Reg;
				};
			namespace IEVENT { // Register description
				typedef uint32_t	Reg;
				namespace BABR { // Field Description
					enum {Lsb = 31};
					enum {FieldMask = 0x80000000};
					enum {Value_ExcessiveFrameReceived = 0x1};
					enum {ValueMask_ExcessiveFrameReceived = 0x80000000};
					enum {Value_Clear = 0x1};
					enum {ValueMask_Clear = 0x80000000};
					};
				namespace RXC { // Field Description
					enum {Lsb = 30};
					enum {FieldMask = 0x40000000};
					enum {Value_ControlFrameReceived = 0x1};
					enum {ValueMask_ControlFrameReceived = 0x40000000};
					enum {Value_Clear = 0x1};
					enum {ValueMask_Clear = 0x40000000};
					};
				namespace BSY { // Field Description
					enum {Lsb = 29};
					enum {FieldMask = 0x20000000};
					enum {Value_FrameReceivedAndDiscarded = 0x1};
					enum {ValueMask_FrameReceivedAndDiscarded = 0x20000000};
					enum {Value_Clear = 0x1};
					enum {ValueMask_Clear = 0x20000000};
					};
				namespace EBERR { // Field Description
					enum {Lsb = 28};
					enum {FieldMask = 0x10000000};
					enum {Value_CsbError = 0x1};
					enum {ValueMask_CsbError = 0x10000000};
					enum {Value_Clear = 0x1};
					enum {ValueMask_Clear = 0x10000000};
					};
				namespace MSRO { // Field Description
					enum {Lsb = 26};
					enum {FieldMask = 0x04000000};
					enum {Value_CountExceedsRegisterSize = 0x1};
					enum {ValueMask_CountExceedsRegisterSize = 0x04000000};
					enum {Value_Clear = 0x1};
					enum {ValueMask_Clear = 0x04000000};
					};
				namespace GTSC { // Field Description
					enum {Lsb = 25};
					enum {FieldMask = 0x02000000};
					enum {Value_GracefulTransmitStopeComplete = 0x1};
					enum {ValueMask_GracefulTransmitStopeComplete = 0x02000000};
					enum {Value_Clear = 0x1};
					enum {ValueMask_Clear = 0x02000000};
					};
				namespace BABT { // Field Description
					enum {Lsb = 24};
					enum {FieldMask = 0x01000000};
					enum {Value_MaxTxFrameLengthExceeded = 0x1};
					enum {ValueMask_MaxTxFrameLengthExceeded = 0x01000000};
					enum {Value_Clear = 0x1};
					enum {ValueMask_Clear = 0x01000000};
					};
				namespace TXC { // Field Description
					enum {Lsb = 23};
					enum {FieldMask = 0x00800000};
					enum {Value_ControlFrameTransmitted = 0x1};
					enum {ValueMask_ControlFrameTransmitted = 0x00800000};
					enum {Value_Clear = 0x1};
					enum {ValueMask_Clear = 0x00800000};
					};
				namespace TXE { // Field Description
					enum {Lsb = 22};
					enum {FieldMask = 0x00400000};
					enum {Value_TransmitChannelError = 0x1};
					enum {ValueMask_TransmitChannelError = 0x00400000};
					enum {Value_Clear = 0x1};
					enum {ValueMask_Clear = 0x00400000};
					};
				namespace TXB { // Field Description
					enum {Lsb = 21};
					enum {FieldMask = 0x00200000};
					enum {Value_TransmitBufferDescUpdated = 0x1};
					enum {ValueMask_TransmitBufferDescUpdated = 0x00200000};
					enum {Value_Clear = 0x1};
					enum {ValueMask_Clear = 0x00200000};
					};
				namespace TXF { // Field Description
					enum {Lsb = 20};
					enum {FieldMask = 0x00100000};
					enum {Value_FrameTxBdUpdated = 0x1};
					enum {ValueMask_FrameTxBdUpdated = 0x00100000};
					enum {Value_Clear = 0x1};
					enum {ValueMask_Clear = 0x00100000};
					};
				namespace LC { // Field Description
					enum {Lsb = 18};
					enum {FieldMask = 0x00040000};
					enum {Value_LateCollision = 0x1};
					enum {ValueMask_LateCollision = 0x00040000};
					enum {Value_Clear = 0x1};
					enum {ValueMask_Clear = 0x00040000};
					};
				namespace CRL_XDA { // Field Description
					enum {Lsb = 17};
					enum {FieldMask = 0x00020000};
					enum {Value_Error = 0x1};
					enum {ValueMask_Error = 0x00020000};
					enum {Value_Clear = 0x1};
					enum {ValueMask_Clear = 0x00020000};
					};
				namespace XFUN { // Field Description
					enum {Lsb = 16};
					enum {FieldMask = 0x00010000};
					enum {Value_TxFifoUnderrun = 0x1};
					enum {ValueMask_TxFifoUnderrun = 0x00010000};
					enum {Value_Clear = 0x1};
					enum {ValueMask_Clear = 0x00010000};
					};
				namespace RXB { // Field Description
					enum {Lsb = 15};
					enum {FieldMask = 0x00008000};
					enum {Value_RxBufferDescUpdated = 0x1};
					enum {ValueMask_RxBufferDescUpdated = 0x00008000};
					enum {Value_Clear = 0x1};
					enum {ValueMask_Clear = 0x00008000};
					};
				namespace MMRD { // Field Description
					enum {Lsb = 10};
					enum {FieldMask = 0x00000400};
					enum {Value_MiiReadCompleted = 0x1};
					enum {ValueMask_MiiReadCompleted = 0x00000400};
					enum {Value_Clear = 0x1};
					enum {ValueMask_Clear = 0x00000400};
					};
				namespace MMWR { // Field Description
					enum {Lsb = 9};
					enum {FieldMask = 0x00000200};
					enum {Value_MiiWriteCompleted = 0x1};
					enum {ValueMask_MiiWriteCompleted = 0x00000200};
					enum {Value_Clear = 0x1};
					enum {ValueMask_Clear = 0x00000200};
					};
				namespace GRSC { // Field Description
					enum {Lsb = 8};
					enum {FieldMask = 0x00000100};
					enum {Value_GracefulStopCompleted = 0x1};
					enum {ValueMask_GracefulStopCompleted = 0x00000100};
					enum {Value_Clear = 0x1};
					enum {ValueMask_Clear = 0x00000100};
					};
				namespace RXF { // Field Description
					enum {Lsb = 7};
					enum {FieldMask = 0x00000080};
					enum {Value_FrameReceived = 0x1};
					enum {ValueMask_FrameReceived = 0x00000080};
					enum {Value_Clear = 0x1};
					enum {ValueMask_Clear = 0x00000080};
					};
				};
			namespace IMASK { // Register description
				typedef uint32_t	Reg;
				namespace BREN { // Field Description
					enum {Lsb = 31};
					enum {FieldMask = 0x80000000};
					enum {Value_Disable = 0x0};
					enum {ValueMask_Disable = 0x00000000};
					enum {Value_Enable = 0x1};
					enum {ValueMask_Enable = 0x80000000};
					};
				namespace RXCEN { // Field Description
					enum {Lsb = 30};
					enum {FieldMask = 0x40000000};
					enum {Value_Disable = 0x0};
					enum {ValueMask_Disable = 0x00000000};
					enum {Value_Enable = 0x1};
					enum {ValueMask_Enable = 0x40000000};
					};
				namespace BSYEN { // Field Description
					enum {Lsb = 29};
					enum {FieldMask = 0x20000000};
					enum {Value_Disable = 0x0};
					enum {ValueMask_Disable = 0x00000000};
					enum {Value_Enable = 0x1};
					enum {ValueMask_Enable = 0x20000000};
					};
				namespace EBERREN { // Field Description
					enum {Lsb = 28};
					enum {FieldMask = 0x10000000};
					enum {Value_Disable = 0x0};
					enum {ValueMask_Disable = 0x00000000};
					enum {Value_Enable = 0x1};
					enum {ValueMask_Enable = 0x10000000};
					};
				namespace MSROEN { // Field Description
					enum {Lsb = 26};
					enum {FieldMask = 0x04000000};
					enum {Value_Disable = 0x0};
					enum {ValueMask_Disable = 0x00000000};
					enum {Value_Enable = 0x1};
					enum {ValueMask_Enable = 0x04000000};
					};
				namespace GTSCEN { // Field Description
					enum {Lsb = 25};
					enum {FieldMask = 0x02000000};
					enum {Value_Disable = 0x0};
					enum {ValueMask_Disable = 0x00000000};
					enum {Value_Enable = 0x1};
					enum {ValueMask_Enable = 0x02000000};
					};
				namespace BTEN { // Field Description
					enum {Lsb = 24};
					enum {FieldMask = 0x01000000};
					enum {Value_Disable = 0x0};
					enum {ValueMask_Disable = 0x00000000};
					enum {Value_Enable = 0x1};
					enum {ValueMask_Enable = 0x01000000};
					};
				namespace TXCEN { // Field Description
					enum {Lsb = 23};
					enum {FieldMask = 0x00800000};
					enum {Value_Disable = 0x0};
					enum {ValueMask_Disable = 0x00000000};
					enum {Value_Enable = 0x1};
					enum {ValueMask_Enable = 0x00800000};
					};
				namespace TXEEN { // Field Description
					enum {Lsb = 22};
					enum {FieldMask = 0x00400000};
					enum {Value_Disable = 0x0};
					enum {ValueMask_Disable = 0x00000000};
					enum {Value_Enable = 0x1};
					enum {ValueMask_Enable = 0x00400000};
					};
				namespace TXBEN { // Field Description
					enum {Lsb = 21};
					enum {FieldMask = 0x00200000};
					enum {Value_Disable = 0x0};
					enum {ValueMask_Disable = 0x00000000};
					enum {Value_Enable = 0x1};
					enum {ValueMask_Enable = 0x00200000};
					};
				namespace TXFEN { // Field Description
					enum {Lsb = 20};
					enum {FieldMask = 0x00100000};
					enum {Value_Disable = 0x0};
					enum {ValueMask_Disable = 0x00000000};
					enum {Value_Enable = 0x1};
					enum {ValueMask_Enable = 0x00100000};
					};
				namespace LCEN { // Field Description
					enum {Lsb = 18};
					enum {FieldMask = 0x00040000};
					enum {Value_Disable = 0x0};
					enum {ValueMask_Disable = 0x00000000};
					enum {Value_Enable = 0x1};
					enum {ValueMask_Enable = 0x00040000};
					};
				namespace CRL_XDAEN { // Field Description
					enum {Lsb = 17};
					enum {FieldMask = 0x00020000};
					enum {Value_Disable = 0x0};
					enum {ValueMask_Disable = 0x00000000};
					enum {Value_Enable = 0x1};
					enum {ValueMask_Enable = 0x00020000};
					};
				namespace XFUNEN { // Field Description
					enum {Lsb = 16};
					enum {FieldMask = 0x00010000};
					enum {Value_Disable = 0x0};
					enum {ValueMask_Disable = 0x00000000};
					enum {Value_Enable = 0x1};
					enum {ValueMask_Enable = 0x00010000};
					};
				namespace RXBEN { // Field Description
					enum {Lsb = 15};
					enum {FieldMask = 0x00008000};
					enum {Value_Disable = 0x0};
					enum {ValueMask_Disable = 0x00000000};
					enum {Value_Enable = 0x1};
					enum {ValueMask_Enable = 0x00008000};
					};
				namespace MMRD { // Field Description
					enum {Lsb = 10};
					enum {FieldMask = 0x00000400};
					enum {Value_Disable = 0x0};
					enum {ValueMask_Disable = 0x00000000};
					enum {Value_Enable = 0x1};
					enum {ValueMask_Enable = 0x00000400};
					};
				namespace MMWR { // Field Description
					enum {Lsb = 9};
					enum {FieldMask = 0x00000200};
					enum {Value_Disable = 0x0};
					enum {ValueMask_Disable = 0x00000000};
					enum {Value_Enable = 0x1};
					enum {ValueMask_Enable = 0x00000200};
					};
				namespace GRSCEN { // Field Description
					enum {Lsb = 8};
					enum {FieldMask = 0x00000100};
					enum {Value_Disable = 0x0};
					enum {ValueMask_Disable = 0x00000000};
					enum {Value_Enable = 0x1};
					enum {ValueMask_Enable = 0x00000100};
					};
				namespace RXFEN { // Field Description
					enum {Lsb = 7};
					enum {FieldMask = 0x00000080};
					enum {Value_Disable = 0x0};
					enum {ValueMask_Disable = 0x00000000};
					enum {Value_Enable = 0x1};
					enum {ValueMask_Enable = 0x00000080};
					};
				};
			namespace EDIS { // Register description
				typedef uint32_t	Reg;
				namespace BSYDIS { // Field Description
					enum {Lsb = 29};
					enum {FieldMask = 0x20000000};
					enum {Value_Allow = 0x0};
					enum {ValueMask_Allow = 0x00000000};
					enum {Value_Disable = 0x1};
					enum {ValueMask_Disable = 0x20000000};
					};
				namespace EBERRDIS { // Field Description
					enum {Lsb = 28};
					enum {FieldMask = 0x10000000};
					enum {Value_Allow = 0x0};
					enum {ValueMask_Allow = 0x00000000};
					enum {Value_Disable = 0x1};
					enum {ValueMask_Disable = 0x10000000};
					};
				namespace TXEDIS { // Field Description
					enum {Lsb = 22};
					enum {FieldMask = 0x00400000};
					enum {Value_Allow = 0x0};
					enum {ValueMask_Allow = 0x00000000};
					enum {Value_Disable = 0x1};
					enum {ValueMask_Disable = 0x00400000};
					};
				namespace LCDIS { // Field Description
					enum {Lsb = 18};
					enum {FieldMask = 0x00040000};
					enum {Value_Allow = 0x0};
					enum {ValueMask_Allow = 0x00000000};
					enum {Value_Disable = 0x1};
					enum {ValueMask_Disable = 0x00040000};
					};
				namespace CRL_XDADIS { // Field Description
					enum {Lsb = 17};
					enum {FieldMask = 0x00020000};
					enum {Value_Allow = 0x0};
					enum {ValueMask_Allow = 0x00000000};
					enum {Value_Disable = 0x1};
					enum {ValueMask_Disable = 0x00020000};
					};
				namespace XFUNDIS { // Field Description
					enum {Lsb = 16};
					enum {FieldMask = 0x00010000};
					enum {Value_Allow = 0x0};
					enum {ValueMask_Allow = 0x00000000};
					enum {Value_Disable = 0x1};
					enum {ValueMask_Disable = 0x00010000};
					};
				};
			namespace ECNTRL { // Register description
				typedef uint32_t	Reg;
				namespace CLRCNT { // Field Description
					enum {Lsb = 14};
					enum {FieldMask = 0x00004000};
					enum {Value_ContinueToIncrement = 0x0};
					enum {ValueMask_ContinueToIncrement = 0x00000000};
					enum {Value_ResetCounters = 0x1};
					enum {ValueMask_ResetCounters = 0x00004000};
					};
				namespace AUTOZ { // Field Description
					enum {Lsb = 13};
					enum {FieldMask = 0x00002000};
					enum {Value_ManualCounterZero = 0x0};
					enum {ValueMask_ManualCounterZero = 0x00000000};
					enum {Value_ZeroCounterOnRead = 0x1};
					enum {ValueMask_ZeroCounterOnRead = 0x00002000};
					};
				namespace STEN { // Field Description
					enum {Lsb = 12};
					enum {FieldMask = 0x00001000};
					enum {Value_Disable = 0x0};
					enum {ValueMask_Disable = 0x00000000};
					enum {Value_Enable = 0x1};
					enum {ValueMask_Enable = 0x00001000};
					};
				namespace TBIM { // Field Description
					enum {Lsb = 5};
					enum {FieldMask = 0x00000020};
					enum {Value_GmiiOrMiiMode = 0x0};
					enum {ValueMask_GmiiOrMiiMode = 0x00000000};
					enum {Value_TbiMode = 0x1};
					enum {ValueMask_TbiMode = 0x00000020};
					};
				namespace RPM { // Field Description
					enum {Lsb = 4};
					enum {FieldMask = 0x00000010};
					enum {Value_GmiiOrTbiMode = 0x0};
					enum {ValueMask_GmiiOrTbiMode = 0x00000000};
					enum {Value_RgmiiOrRtbiMode = 0x1};
					enum {ValueMask_RgmiiOrRtbiMode = 0x00000010};
					};
				namespace R100M { // Field Description
					enum {Lsb = 3};
					enum {FieldMask = 0x00000008};
					enum {Value_Rgmii10MbpsMode = 0x0};
					enum {ValueMask_Rgmii10MbpsMode = 0x00000000};
					enum {Value_Rgmii100MbpsMode = 0x1};
					enum {ValueMask_Rgmii100MbpsMode = 0x00000008};
					};
				};
			namespace MINFLR { // Register description
				typedef uint32_t	Reg;
				namespace MINFLR { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x0000007F};
					enum {Value_Typical = 0x40};
					enum {ValueMask_Typical = 0x00000040};
					};
				};
			namespace PTV { // Register description
				typedef uint32_t	Reg;
				namespace PTE { // Field Description
					enum {Lsb = 16};
					enum {FieldMask = 0xFFFF0000};
					enum {Value_IEEE802_3 = 0x0};
					enum {ValueMask_IEEE802_3 = 0x00000000};
					};
				namespace PT { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x0000FFFF};
					enum {Value_Value512BitTimes = 0x1};
					enum {ValueMask_Value512BitTimes = 0x00000001};
					};
				};
			namespace DMACTRL { // Register description
				typedef uint32_t	Reg;
				namespace TDSEN { // Field Description
					enum {Lsb = 7};
					enum {FieldMask = 0x00000080};
					enum {Value_TxDataSnoopDisable = 0x0};
					enum {ValueMask_TxDataSnoopDisable = 0x00000000};
					enum {Value_TxDataSnoopEnable = 0x1};
					enum {ValueMask_TxDataSnoopEnable = 0x00000080};
					};
				namespace TBDSEN { // Field Description
					enum {Lsb = 6};
					enum {FieldMask = 0x00000040};
					enum {Value_TxBdSnoopDisable = 0x0};
					enum {ValueMask_TxBdSnoopDisable = 0x00000000};
					enum {Value_TxBdSnoopEnable = 0x1};
					enum {ValueMask_TxBdSnoopEnable = 0x00000040};
					};
				namespace GRS { // Field Description
					enum {Lsb = 4};
					enum {FieldMask = 0x00000010};
					enum {Value_Normal = 0x0};
					enum {ValueMask_Normal = 0x00000000};
					enum {Value_StopRxAfterCurrentFrame = 0x1};
					enum {ValueMask_StopRxAfterCurrentFrame = 0x00000010};
					};
				namespace GTS { // Field Description
					enum {Lsb = 3};
					enum {FieldMask = 0x00000008};
					enum {Value_Normal = 0x0};
					enum {ValueMask_Normal = 0x00000000};
					enum {Value_StopTxAfterCurrentFrame = 0x1};
					enum {ValueMask_StopTxAfterCurrentFrame = 0x00000008};
					};
				namespace TOD { // Field Description
					enum {Lsb = 2};
					enum {FieldMask = 0x00000004};
					enum {Value_Normal = 0x0};
					enum {ValueMask_Normal = 0x00000000};
					enum {Value_TxCurrentTxBdNOW = 0x1};
					enum {ValueMask_TxCurrentTxBdNOW = 0x00000004};
					};
				namespace WWR { // Field Description
					enum {Lsb = 1};
					enum {FieldMask = 0x00000002};
					enum {Value_NoWaitForStoreToMemory = 0x0};
					enum {ValueMask_NoWaitForStoreToMemory = 0x00000000};
					enum {Value_WaitForStoreToMemory = 0x1};
					enum {ValueMask_WaitForStoreToMemory = 0x00000002};
					};
				namespace WOP { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x00000001};
					enum {Value_PollTxBD = 0x0};
					enum {ValueMask_PollTxBD = 0x00000000};
					enum {Value_WaitForWriteToThlt = 0x1};
					enum {ValueMask_WaitForWriteToThlt = 0x00000001};
					};
				};
			namespace TBIPA { // Register description
				typedef uint32_t	Reg;
				namespace TBIPA { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x0000001F};
					};
				};
			namespace FIFO_TX_THR { // Register description
				typedef uint32_t	Reg;
				namespace FIFO_TX_THR { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x000001FF};
					};
				};
			namespace FIFO_TX_STARVE { // Register description
				typedef uint32_t	Reg;
				namespace FIFO_TX_STARVE { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x000001FF};
					};
				};
			namespace FIFO_TX_STARVE_SHUTOFF { // Register description
				typedef uint32_t	Reg;
				namespace FIFO_TX_STARVE_SHUTOFF { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x000001FF};
					};
				};
			namespace TCTRL { // Register description
				typedef uint32_t	Reg;
				namespace THDF { // Field Description
					enum {Lsb = 11};
					enum {FieldMask = 0x00000800};
					enum {Value_DisableBackPressure = 0x0};
					enum {ValueMask_DisableBackPressure = 0x00000000};
					enum {Value_ApplyBackPressure = 0x1};
					enum {ValueMask_ApplyBackPressure = 0x00000800};
					};
				namespace RFC_PAUSE { // Field Description
					enum {Lsb = 4};
					enum {FieldMask = 0x00000010};
					enum {Value_PauseDurationComplete = 0x0};
					enum {ValueMask_PauseDurationComplete = 0x00000000};
					enum {Value_PauseDurationInProgress = 0x1};
					enum {ValueMask_PauseDurationInProgress = 0x00000010};
					};
				namespace TFC_PAUSE { // Field Description
					enum {Lsb = 3};
					enum {FieldMask = 0x00000008};
					enum {Value_NoPauseFrameTxRequested = 0x0};
					enum {ValueMask_NoPauseFrameTxRequested = 0x00000000};
					enum {Value_PauseFrameTxRequested = 0x1};
					enum {ValueMask_PauseFrameTxRequested = 0x00000008};
					};
				};
			namespace TSTAT { // Register description
				typedef uint32_t	Reg;
				namespace THLT { // Field Description
					enum {Lsb = 31};
					enum {FieldMask = 0x80000000};
					enum {Value_TxFunctionHalted = 0x1};
					enum {ValueMask_TxFunctionHalted = 0x80000000};
					enum {Value_Clear = 0x1};
					enum {ValueMask_Clear = 0x80000000};
					};
				};
			namespace TBDLEN { // Register description
				typedef uint32_t	Reg;
				namespace TBDLEN { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x0000FFFF};
					};
				};
			namespace TXIC { // Register description
				typedef uint32_t	Reg;
				namespace ICEN { // Field Description
					enum {Lsb = 31};
					enum {FieldMask = 0x80000000};
					enum {Value_InterruptCoalescingDisabled = 0x0};
					enum {ValueMask_InterruptCoalescingDisabled = 0x00000000};
					enum {Value_InterruptCoalescingEnabled = 0x1};
					enum {ValueMask_InterruptCoalescingEnabled = 0x80000000};
					};
				namespace ICFCT { // Field Description
					enum {Lsb = 21};
					enum {FieldMask = 0x1FE00000};
					};
				namespace ICTT { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x0000FFFF};
					};
				};
			namespace CTBPTR { // Register description
				typedef uint32_t	Reg;
				};
			namespace TBPTR { // Register description
				typedef uint32_t	Reg;
				};
			namespace TBASE { // Register description
				typedef uint32_t	Reg;
				};
			namespace OSTBD { // Register description
				typedef uint32_t	Reg;
				namespace R { // Field Description
					enum {Lsb = 31};
					enum {FieldMask = 0x80000000};
					enum {Value_NotReadyForTx = 0x0};
					enum {ValueMask_NotReadyForTx = 0x00000000};
					enum {Value_ReadyForTx = 0x1};
					enum {ValueMask_ReadyForTx = 0x80000000};
					};
				namespace PAD_CRC { // Field Description
					enum {Lsb = 30};
					enum {FieldMask = 0x40000000};
					enum {Value_NoPadding = 0x0};
					enum {ValueMask_NoPadding = 0x00000000};
					enum {Value_PadShortFrames = 0x1};
					enum {ValueMask_PadShortFrames = 0x40000000};
					};
				namespace W { // Field Description
					enum {Lsb = 29};
					enum {FieldMask = 0x20000000};
					enum {Value_NoWrap = 0x0};
					enum {ValueMask_NoWrap = 0x00000000};
					enum {Value_Wrap = 0x1};
					enum {ValueMask_Wrap = 0x20000000};
					};
				namespace I { // Field Description
					enum {Lsb = 28};
					enum {FieldMask = 0x10000000};
					enum {Value_NoInterruptAfterService = 0x0};
					enum {ValueMask_NoInterruptAfterService = 0x00000000};
					enum {Value_InterruptAfterService = 0x1};
					enum {ValueMask_InterruptAfterService = 0x10000000};
					};
				namespace L { // Field Description
					enum {Lsb = 27};
					enum {FieldMask = 0x08000000};
					enum {Value_LastInFrame = 0x1};
					enum {ValueMask_LastInFrame = 0x08000000};
					};
				namespace TC { // Field Description
					enum {Lsb = 26};
					enum {FieldMask = 0x04000000};
					enum {Value_NoCrcAppened = 0x0};
					enum {ValueMask_NoCrcAppened = 0x00000000};
					enum {Value_AppendCRC = 0x1};
					enum {ValueMask_AppendCRC = 0x04000000};
					};
				namespace DEF { // Field Description
					enum {Lsb = 25};
					enum {FieldMask = 0x02000000};
					enum {Value_FrameNotDeferred = 0x0};
					enum {ValueMask_FrameNotDeferred = 0x00000000};
					enum {Value_FrameDeferred = 0x1};
					enum {ValueMask_FrameDeferred = 0x02000000};
					};
				namespace TO1 { // Field Description
					enum {Lsb = 24};
					enum {FieldMask = 0x01000000};
					};
				namespace HFE_LC { // Field Description
					enum {Lsb = 23};
					enum {FieldMask = 0x00800000};
					enum {Value_TruncateHugeFrames = 0x0};
					enum {ValueMask_TruncateHugeFrames = 0x00000000};
					enum {Value_DoNotTruncateHugeFrames = 0x1};
					enum {ValueMask_DoNotTruncateHugeFrames = 0x00800000};
					enum {Value_NoLateCollision = 0x0};
					enum {ValueMask_NoLateCollision = 0x00000000};
					enum {Value_LateCollisionDetected = 0x1};
					enum {ValueMask_LateCollisionDetected = 0x00800000};
					};
				namespace RL { // Field Description
					enum {Lsb = 22};
					enum {FieldMask = 0x00400000};
					enum {Value_TxBeforeMaxRetryLimit = 0x0};
					enum {ValueMask_TxBeforeMaxRetryLimit = 0x00000000};
					enum {Value_MaxTxRetryExceeded = 0x1};
					enum {ValueMask_MaxTxRetryExceeded = 0x00400000};
					};
				namespace RC { // Field Description
					enum {Lsb = 18};
					enum {FieldMask = 0x003C0000};
					enum {Value_FirstTry = 0x0};
					enum {ValueMask_FirstTry = 0x00000000};
					enum {Value_FifteenOrMoreRetries = 0xF};
					enum {ValueMask_FifteenOrMoreRetries = 0x003C0000};
					};
				namespace UN { // Field Description
					enum {Lsb = 17};
					enum {FieldMask = 0x00020000};
					enum {Value_NoTxUnderrun = 0x0};
					enum {ValueMask_NoTxUnderrun = 0x00000000};
					enum {Value_TxUnderrun = 0x1};
					enum {ValueMask_TxUnderrun = 0x00020000};
					};
				namespace OSTBDLEN { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x0000FFFF};
					};
				};
			namespace OSTBDP { // Register description
				typedef uint32_t	Reg;
				};
			namespace RCTRL { // Register description
				typedef uint32_t	Reg;
				namespace BC_REJ { // Field Description
					enum {Lsb = 4};
					enum {FieldMask = 0x00000010};
					enum {Value_AcceptBroadcastFrames = 0x0};
					enum {ValueMask_AcceptBroadcastFrames = 0x00000000};
					enum {Value_RejectBroadcastFrames = 0x1};
					enum {ValueMask_RejectBroadcastFrames = 0x00000010};
					};
				namespace PROM { // Field Description
					enum {Lsb = 3};
					enum {FieldMask = 0x00000008};
					enum {Value_PromiscuousDisable = 0x0};
					enum {ValueMask_PromiscuousDisable = 0x00000000};
					enum {Value_PromiscuousEnable = 0x1};
					enum {ValueMask_PromiscuousEnable = 0x00000008};
					};
				namespace RSF { // Field Description
					enum {Lsb = 2};
					enum {FieldMask = 0x00000004};
					enum {Value_RejectShortFrames = 0x0};
					enum {ValueMask_RejectShortFrames = 0x00000000};
					enum {Value_AcceptShortFrames = 0x1};
					enum {ValueMask_AcceptShortFrames = 0x00000004};
					};
				};
			namespace RSTAT { // Register description
				typedef uint32_t	Reg;
				namespace QHLT { // Field Description
					enum {Lsb = 23};
					enum {FieldMask = 0x00800000};
					enum {Value_RxBdQueueActive = 0x0};
					enum {ValueMask_RxBdQueueActive = 0x00000000};
					enum {Value_RxBdQueueHalted = 0x0};
					enum {ValueMask_RxBdQueueHalted = 0x00000000};
					};
				};
			namespace RBDLEN { // Register description
				typedef uint32_t	Reg;
				namespace RBDLEN { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x0000FFFF};
					};
				};
			namespace RXIC { // Register description
				typedef uint32_t	Reg;
				namespace ICEN { // Field Description
					enum {Lsb = 31};
					enum {FieldMask = 0x80000000};
					enum {Value_InterruptCoalescingDisable = 0x0};
					enum {ValueMask_InterruptCoalescingDisable = 0x00000000};
					enum {Value_InterruptCoalescingEnable = 0x0};
					enum {ValueMask_InterruptCoalescingEnable = 0x00000000};
					};
				namespace ICFCT { // Field Description
					enum {Lsb = 21};
					enum {FieldMask = 0x1FE00000};
					enum {Value_InvalidValue = 0x0};
					enum {ValueMask_InvalidValue = 0x00000000};
					enum {Value_EffectivelyDefeatsCoalescing = 0x1};
					enum {ValueMask_EffectivelyDefeatsCoalescing = 0x00200000};
					enum {Value_Max = 0xFF};
					enum {ValueMask_Max = 0x1FE00000};
					};
				namespace ICTT { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x0000FFFF};
					enum {Value_InvalidValue = 0x0};
					enum {ValueMask_InvalidValue = 0x00000000};
					};
				};
			namespace CRBPTR { // Register description
				typedef uint32_t	Reg;
				};
			namespace MRBL { // Register description
				typedef uint32_t	Reg;
				namespace MRBL { // Field Description
					enum {Lsb = 6};
					enum {FieldMask = 0x0000FFC0};
					};
				};
			namespace RBPTR { // Register description
				typedef uint32_t	Reg;
				};
			namespace RBASE { // Register description
				typedef uint32_t	Reg;
				};
			};
		};
	};
#endif
