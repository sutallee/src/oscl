/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_hw_motorola_m2681h_
#define _oscl_hw_motorola_m2681h_
#include <stdint.h>
#include "oscl/compiler/types.h"

/** This file represents the hardware structure of
	the Motorola M2681 dual channel async serializer.
 */

/*
 * This file describes the Motorola 2681 dual serial controller.
 * This data structure assumes big endian byte ordering, since
 * this is a Motorola part...:-)
 */

typedef uint8_t	M2681MCRH;

enum{	/* M2681MCRH (Module Configuration Register High Byte) Bit definitions */
	m2681McrhStpBit	= 7,
	m2681McrhFrzBit	= 5,
	m2681McrhIccsBit	= 4
	};

enum{	/* M2681MCRH (Module Configuration Register High Byte) masks */
	m2681McrhStpMask			= (int)(0x01<<m2681McrhStpBit),
	m2681McrhStpEnable			= 0x00<<m2681McrhStpBit,
	m2681McrhStpDisable			= (int)(0x01<<m2681McrhStpBit),
	m2681McrhFrzMask			= 0x03<<m2681McrhFrzBit,
	m2681McrhFrzIgnore			= 0x00<<m2681McrhFrzBit,
	m2681McrhFrzOnChar			= 0x03<<m2681McrhFrzBit,
	m2681McrhIccsMask			= 0x01<<m2681McrhIccsBit,
	m2681McrhIccsSCLK			= 0x01<<m2681McrhIccsBit,
	m2681McrhIccsXTAL			= 0x00<<m2681McrhIccsBit
	};

typedef uint8_t	M2681MCRL;

enum{	/* M2681MCRL (Module Configuration Register Low Byte) Bit definitions */
	m2681McrlSupvBit	= 7,
	m2681McrlIarbBit	= 0
	};

enum{	/* M2681MCRL (Module Configuration Register Low Byte) masks */
	m2681McrlSupvMask				= 0x01<<m2681McrlSupvBit,
	m2681McrlSupvSuper				= 0x01<<m2681McrlSupvBit,
	m2681McrlSupvUser				= 0x00<<m2681McrlSupvBit,
	m2681McrlIarbMask				= 0x0F<<m2681McrlIarbBit,
	m2681McrlIarbDisabled			= 0x00<<m2681McrlIarbBit,
	m2681McrlIarbHighestPriority	= 0x0F<<m2681McrlIarbBit,
	m2681McrlIarbLowestPriority		= 0x01<<m2681McrlIarbBit
	};

typedef struct M2681MCR{	/* Byte access only */
	M2681MCRH		h;		/* High byte */
	M2681MCRL		l;		/* Low byte */
	}M2681MCR;

typedef uint8_t			M2681ILR;

enum{	/* M2681ILR (Interrupt Level Register) Bit definitions */
	m2681IlrIlBit	= 0
	};

enum{	/* M2681ILR (Interrupt Level Register) masks */
	m2681IlrIlMask	= 0x07<<m2681IlrIlBit
	};

typedef uint8_t			M2681IVR;

enum{	/* M2681IVR (Interrupt Vector Register) Bit definitions */
	m2681IvrIvrBit	= 0
	};

enum{	/* M2681IVR (Interrupt Vector Register) masks */
	m2681IvrIvrMask	= 0xFF<<m2681IvrIvrBit
	};

typedef uint8_t			M2681MR1;

enum{	/* M2681MR1 (Mode Register 1) Bit definitions */
	m2681Mr1RxRtsBit	= 7,
	m2681Mr1RFBit		= 6,
	m2681Mr1ErrBit		= 5,
	m2681Mr1PmtBit		= 2,
	m2681Mr1BCBit		= 0
	};

enum{	/* M2681MR1 (Mode Register 1) masks */
	m2681Mr1RxRtsMask	= 0x01<<m2681Mr1RxRtsBit,
	m2681Mr1RxRtsAuto	= 0x01<<m2681Mr1RxRtsBit,
	m2681Mr1RxRtsManual	= 0x00<<m2681Mr1RxRtsBit,
	m2681Mr1RFMask		= 0x01<<m2681Mr1RFBit,
	m2681Mr1RFFifoFull	= 0x01<<m2681Mr1RFBit,
	m2681Mr1RFCharReady	= 0x00<<m2681Mr1RFBit,
	m2681Mr1ErrMask		= 0x01<<m2681Mr1ErrBit,
	m2681Mr1ErrBlock	= 0x01<<m2681Mr1ErrBit,
	m2681Mr1ErrChar		= 0x00<<m2681Mr1ErrBit,
	m2681Mr1PmtMask		= 0x07<<m2681Mr1PmtBit,
	m2681Mr1PmtEven		= 0x00<<m2681Mr1PmtBit,
	m2681Mr1PmtOdd		= 0x01<<m2681Mr1PmtBit,
	m2681Mr1PmtLow		= 0x02<<m2681Mr1PmtBit,
	m2681Mr1PmtHigh		= 0x03<<m2681Mr1PmtBit,
	m2681Mr1PmtNone		= 0x04<<m2681Mr1PmtBit,
	m2681Mr1PmtData		= 0x06<<m2681Mr1PmtBit,
	m2681Mr1PmtAddr		= 0x07<<m2681Mr1PmtBit,
	m2681Mr1BCMask		= 0x03<<m2681Mr1BCBit,
	m2681Mr1BCFive		= 0x00<<m2681Mr1BCBit,
	m2681Mr1BCSix		= 0x01<<m2681Mr1BCBit,
	m2681Mr1BCSeven		= 0x02<<m2681Mr1BCBit,
	m2681Mr1BCEight		= 0x03<<m2681Mr1BCBit
	};

typedef uint8_t			M2681SR;

enum{	/* M2681SR (Status Register) Bit definitions */
	m2681SrRbBit	= 7,
	m2681SrFeBit	= 6,
	m2681SrPeBit	= 5,
	m2681SrOeBit	= 4,
	m2681SrTxEmpBit	= 3,
	m2681SrTxRdyBit	= 2,
	m2681SrFFullBit	= 1,
	m2681SrRxRdyBit	= 0
	};

enum{	/* M2681SR (Status Register) masks */
	m2681SrRbMask				= 0x01<<m2681SrRbBit,
	m2681SrRbBreakReceived		= 0x01<<m2681SrRbBit,
	m2681SrRbBreakNotReceived	= 0x00<<m2681SrRbBit,
	m2681SrFeMask				= 0x01<<m2681SrFeBit,
	m2681SrRbFramingError		= 0x01<<m2681SrRbBit,
	m2681SrRbNoFramingError		= 0x00<<m2681SrRbBit,
	m2681SrPeMask				= 0x01<<m2681SrPeBit,
	m2681SrPeParityError		= 0x01<<m2681SrPeBit,
	m2681SrPeNoParityError		= 0x00<<m2681SrPeBit,
	m2681SrOeMask				= 0x01<<m2681SrOeBit,
	m2681SrOeRxOverrun			= 0x01<<m2681SrOeBit,
	m2681SrOeNoRxOverrun		= 0x00<<m2681SrOeBit,
	m2681SrTxEmpMask			= 0x01<<m2681SrTxEmpBit,
	m2681SrTxEmpTxEmpty			= 0x01<<m2681SrTxEmpBit,
	m2681SrTxEmpTxNotEmpty		= 0x00<<m2681SrTxEmpBit,
	m2681SrTxRdyMask			= 0x01<<m2681SrTxRdyBit,
	m2681SrTxRdyTxReady			= 0x01<<m2681SrTxRdyBit,
	m2681SrTxRdyTxNotReady		= 0x00<<m2681SrTxRdyBit,
	m2681SrFFullMask			= 0x01<<m2681SrFFullBit,
	m2681SrFFullFifoFull		= 0x01<<m2681SrFFullBit,
	m2681SrFFullFifoNotFull		= 0x00<<m2681SrFFullBit,
	m2681SrRxRdyMask			= 0x01<<m2681SrRxRdyBit,
	m2681SrRxRdyRxReady			= 0x01<<m2681SrRxRdyBit,
	m2681SrRxRdyRxNotReady		= 0x00<<m2681SrRxRdyBit
	};

typedef uint8_t			M2681CSR;

enum{	/* M2681CSR (Clock Select Register) Bit definitions */
	m2681CsrRcsBit		= 4,
	m2681CsrTcsBit		= 0
	};

enum{	/* M2681CSR (Clock Select Register) Set 1 baud definitions */
	m2681CsrSetOne50baud			= 0,
	m2681CsrSetOne110baud			= 1,
	m2681CsrSetOne134_5baud			= 2,
	m2681CsrSetOne200baud			= 3,
	m2681CsrSetOne300baud			= 4,
	m2681CsrSetOne600baud			= 5,
	m2681CsrSetOne1200baud			= 6,
	m2681CsrSetOne1050baud			= 7,
	m2681CsrSetOne2400baud			= 8,
	m2681CsrSetOne4800baud			= 9,
	m2681CsrSetOne7200baud			= 10,
	m2681CsrSetOne9600baud			= 11,
	m2681CsrSetOne38_4baud			= 12,
	m2681CsrSetOne76_8baud			= 13,
	m2681CsrSetOneSclkOver16baud	= 14,
	m2681CsrSetOneSclkOver1baud		= 15
	};

enum{	/* M2681CSR (Clock Select Register) Set 2 baud definitions */
	m2681CsrSetTwo75baud			= 0,
	m2681CsrSetTwo110baud			= m2681CsrSetOne110baud,
	m2681CsrSetTwo134_5baud			= m2681CsrSetOne134_5baud,
	m2681CsrSetTwo150baud			= 3,
	m2681CsrSetTwo300baud			= m2681CsrSetOne300baud,
	m2681CsrSetTwo600baud			= m2681CsrSetOne600baud,
	m2681CsrSetTwo1200baud			= m2681CsrSetOne1200baud,
	m2681CsrSetTwo2000baud			= 7,
	m2681CsrSetTwo2400baud			= m2681CsrSetOne2400baud,
	m2681CsrSetTwo4800baud			= m2681CsrSetOne4800baud,
	m2681CsrSetTwo1800baud			= 10,
	m2681CsrSetTwo9600baud			= m2681CsrSetOne9600baud,
	m2681CsrSetTwo19_2baud			= 12,
	m2681CsrSetTwo38_4baud			= 13,
	m2681CsrSetTwoSclkOver16baud	= m2681CsrSetOneSclkOver16baud,
	m2681CsrSetTwoSclkOver1baud		= m2681CsrSetOneSclkOver1baud
	};

enum{	/* M2681CSR (Clock Select Register) masks */
	m2681CsrRcsMask			= 0x0F<<m2681CsrRcsBit,	/* Use m2681CsrSet << m2681CsrRcsBit */
	m2681CsrTcsMask			= 0x0F<<m2681CsrTcsBit,	/* Use m2681CsrSet << m2681CsrTcsBit */
	};

typedef uint8_t	M2681CR;

enum{	/* M2681CR (Command Register) Bit definitions */
	m2681CrMiscBit	= 4,
	m2681CrTcBit	= 2,
	m2681CrRcBit	= 0
	};

enum{	/* M2681CR (Command Register) masks */
	m2681CrMiscMask				= 0x0F<<m2681CrMiscBit,
	m2681CrMiscNoCommand		= 0x00<<m2681CrMiscBit,
	m2681CrMiscNoCommand1		= 0x01<<m2681CrMiscBit,
	m2681CrMiscResetReceiver	= 0x02<<m2681CrMiscBit,
	m2681CrMiscResetTransmitter	= 0x03<<m2681CrMiscBit,
	m2681CrMiscResetErrorStatus	= 0x04<<m2681CrMiscBit,
	m2681CrMiscResetBreakChange	= 0x05<<m2681CrMiscBit,
	m2681CrMiscStartBreak		= 0x06<<m2681CrMiscBit,
	m2681CrMiscStopBreak		= 0x07<<m2681CrMiscBit,
	m2681CrMiscAssertRts		= 0x08<<m2681CrMiscBit,
	m2681CrMiscNegateRts		= 0x09<<m2681CrMiscBit,
	m2681CrMiscNoCommandA		= 0x0A<<m2681CrMiscBit,
	m2681CrMiscNoCommandB		= 0x0B<<m2681CrMiscBit,
	m2681CrMiscNoCommandC		= 0x0C<<m2681CrMiscBit,
	m2681CrMiscNoCommandD		= 0x0D<<m2681CrMiscBit,
	m2681CrMiscNoCommandE		= 0x0E<<m2681CrMiscBit,
	m2681CrMiscNoCommandF		= 0x0F<<m2681CrMiscBit,
	m2681CrTcMask				= 0x03<<m2681CrTcBit,
	m2681CrTcNoAction			= 0x00<<m2681CrTcBit,
	m2681CrTcEnableTransmitter	= 0x01<<m2681CrTcBit,
	m2681CrTcDisableTransmitter	= 0x02<<m2681CrTcBit,
	m2681CrRcMask				= 0x03<<m2681CrRcBit,
	m2681CrRcNoAction			= 0x00<<m2681CrRcBit,
	m2681CrRcEnableReceiver		= 0x01<<m2681CrRcBit,
	m2681CrRcDisableReceiver	= 0x02<<m2681CrRcBit
	};

typedef uint8_t	M2681RB;
typedef uint8_t	M2681TB;

typedef union M2681RxTxBuf{
	volatile	M2681RB		rb;
//	READONLY	M2681RB		rb;
	WRITEONLY	M2681TB		tb;
	}M2681RxTxBuf;


typedef uint8_t	M2681IPCR;

enum{	/* M2681IPCR (Input Port Change Register) Bit definitions */
	m2681IpcrCosBBit	= 5,
	m2681IpcrCosABit	= 4,
	m2681IpcrCtsBBit	= 1,
	m2681IpcrCtsABit	= 0
	};

enum{	/* M2681IPCR (Input Port Change Register) masks */
	m2681IpcrCosBMask			= 0x01<<m2681IpcrCosBBit,
	m2681IpcrCosBCtsBChanged	= 0x01<<m2681IpcrCosBBit,
	m2681IpcrCosBCtsBNotChanged	= 0x00<<m2681IpcrCosBBit,
	m2681IpcrCosAMask			= 0x01<<m2681IpcrCosABit,
	m2681IpcrCosACtsAChanged	= 0x01<<m2681IpcrCosABit,
	m2681IpcrCosACtsANotChanged	= 0x00<<m2681IpcrCosABit,
	m2681IpcrCtsBMask			= 0x01<<m2681IpcrCtsBBit,
	m2681IpcrCtsBCtsBNegated	= 0x01<<m2681IpcrCtsBBit,
	m2681IpcrCtsBCtsBAsserted	= 0x00<<m2681IpcrCtsBBit,
	m2681IpcrCtsAMask			= 0x01<<m2681IpcrCtsABit,
	m2681IpcrCtsACtsANegated	= 0x01<<m2681IpcrCtsABit,
	m2681IpcrCtsACtsAAsserted	= 0x00<<m2681IpcrCtsABit
	};

typedef uint8_t	M2681ACR;

enum{	/* M2681ACR (Auxiliary Control Register) Bit definittions */
	m2681AcrBrgBit	= 7,
	m2681AcrIecBBit	= 1,
	m2681AcrIecABit	= 0
	};

enum{	/* M2681ACR (Auxiliary Control Register) masks */
	m2681AcrBrgMask				= 0x01<<m2681AcrBrgBit,
	m2681AcrBrgSelectSet1		= 0x00<<m2681AcrBrgBit,
	m2681AcrBrgSelectSet2		= 0x01<<m2681AcrBrgBit,
	m2681AcrIecAMask			= 0x01<<m2681AcrIecABit,
	m2681AcrIecACosIntEnable	= 0x01<<m2681AcrIecABit,
	m2681AcrIecACosIntDisable	= 0x00<<m2681AcrIecABit,
	m2681AcrIecBMask			= 0x01<<m2681AcrIecBBit,
	m2681AcrIecBCosIntEnable	= 0x01<<m2681AcrIecBBit,
	m2681AcrIecBCosIntDisable	= 0x00<<m2681AcrIecBBit,
	};

typedef uint8_t	M2681ISR;

enum{	/* M2681ISR (Interrupt Status Register) Bit definitions */
	m2681IsrCosBit				= 7,
	m2681IsrDbBBit				= 6,
	m2681IsrRxRdyBBit			= 5,
	m2681IsrTxRdyBBit			= 4,
	m2681IsrXtalRdyBit			= 3,
	m2681IsrDbABit				= 2,
	m2681IsrRxRdyABit			= 1,
	m2681IsrTxRdyABit			= 0
	};

enum{	/* M2681ISR (Interrupt Status Register) masks */
	m2681IsrCosMask							= 0x01<<m2681IsrCosBit,
	m2681IsrCosChangeOfState				= 0x01<<m2681IsrCosBit,
	m2681IsrCosNoChangeOfState				= 0x00<<m2681IsrCosBit,
	m2681IsrDbBMask							= 0x01<<m2681IsrDbBBit,
	m2681IsrDbBDeltaBreak					= 0x01<<m2681IsrDbBBit,
	m2681IsrDbBNoDeltaBreak					= 0x00<<m2681IsrDbBBit,
	m2681IsrRxRdyBMask						= 0x01<<m2681IsrRxRdyBBit,
	m2681IsrRxRdyBReceiverReady				= 0x01<<m2681IsrRxRdyBBit,
	m2681IsrRxRdyBFifoFull					= 0x01<<m2681IsrRxRdyBBit,
	m2681IsrRxRdyBReceiverNotReady			= 0x00<<m2681IsrRxRdyBBit,
	m2681IsrRxRdyBFifoNotFull				= 0x00<<m2681IsrRxRdyBBit,
	m2681IsrTxRdyBMask						= 0x01<<m2681IsrTxRdyBBit,
	m2681IsrTxRdyBTransmitterReady			= 0x01<<m2681IsrTxRdyBBit,
	m2681IsrTxRdyBTransmitterNotReady		= 0x00<<m2681IsrTxRdyBBit,
	m2681IsrXtalRdyMask						= 0x01<<m2681IsrXtalRdyBit,
	m2681IsrXtalRdySerialClockRunning		= 0x01<<m2681IsrXtalRdyBit,
	m2681IsrXtalRdySerialClockNotRunning	= 0x00<<m2681IsrXtalRdyBit,
	m2681IsrDbAMask							= 0x01<<m2681IsrDbABit,
	m2681IsrDbADeltaBreak					= 0x01<<m2681IsrDbABit,
	m2681IsrDbANoDeltaBreak					= 0x00<<m2681IsrDbABit,
	m2681IsrRxRdyAMask						= 0x01<<m2681IsrRxRdyABit,
	m2681IsrRxRdyAReceiverReady				= 0x01<<m2681IsrRxRdyABit,
	m2681IsrRxRdyAFifoFull					= 0x01<<m2681IsrRxRdyABit,
	m2681IsrRxRdyAReceiverNotReady			= 0x00<<m2681IsrRxRdyABit,
	m2681IsrRxRdyAFifoNotFull				= 0x00<<m2681IsrRxRdyABit,
	m2681IsrTxRdyAMask						= 0x01<<m2681IsrTxRdyABit,
	m2681IsrTxRdyATransmitterReady			= 0x01<<m2681IsrTxRdyABit,
	m2681IsrTxRdyATransmitterNotReady		= 0x00<<m2681IsrTxRdyABit
	};

typedef uint8_t	M2681IER;

enum{	/* M2681IER (Interrupt Enable Register) Bit definitions */
	m2681IerCosBit		=	7,
	m2681IerDbBBit		=	6,
	m2681IerRxRdyBBit	=	5,
	m2681IerTxRdyBBit	=	4,
	m2681IerDbABit		=	2,
	m2681IerRxRdyABit	=	1,
	m2681IerTxRdyABit	=	0
	};

enum{	/* M2681IER (Interrupt Enable Register) masks */
	m2681IerCosMask			= 0x01<<m2681IerCosBit,
	m2681IerCosEnable		= 0x01<<m2681IerCosBit,
	m2681IerCosDisable		= 0x00<<m2681IerCosBit,
	m2681IerDbBMask			= 0x01<<m2681IerDbBBit,
	m2681IerDbBEnable		= 0x01<<m2681IerDbBBit,
	m2681IerDbBDisable		= 0x00<<m2681IerDbBBit,
	m2681IerRxRdyBMask		= 0x01<<m2681IerRxRdyBBit,
	m2681IerRxRdyBEnable	= 0x01<<m2681IerRxRdyBBit,
	m2681IerRxRdyBDisable	= 0x00<<m2681IerRxRdyBBit,
	m2681IerTxRdyBMask		= 0x01<<m2681IerTxRdyBBit,
	m2681IerTxRdyBEnable	= 0x01<<m2681IerTxRdyBBit,
	m2681IerTxRdyBDisable	= 0x00<<m2681IerTxRdyBBit,
	m2681IerDbAMask			= 0x01<<m2681IerDbABit,
	m2681IerDbAEnable		= 0x01<<m2681IerDbABit,
	m2681IerDbADisable		= 0x00<<m2681IerDbABit,
	m2681IerRxRdyAMask		= 0x01<<m2681IerRxRdyABit,
	m2681IerRxRdyAEnable	= 0x01<<m2681IerRxRdyABit,
	m2681IerRxRdyADisable	= 0x00<<m2681IerRxRdyABit,
	m2681IerTxRdyAMask		= 0x01<<m2681IerTxRdyABit,
	m2681IerTxRdyAEnable	= 0x01<<m2681IerTxRdyABit,
	m2681IerTxRdyADisable	= 0x00<<m2681IerTxRdyABit
	};

typedef uint8_t	M2681IP;

enum{	/* M2681IP (Input Port) Bit definitions */
	m2681IpCtsBBit	= 1,
	m2681IpCtsABit	= 0
	};

enum{	/* M2681IP (Input Port) masks */
	m2681IpCtsBMask		= 0x01<<m2681IpCtsBBit,
	m2681IpCtsBNegated	= 0x01<<m2681IpCtsBBit,
	m2681IpCtsBAsserted	= 0x00<<m2681IpCtsBBit,
	m2681IpCtsAMask		= 0x01<<m2681IpCtsABit,
	m2681IpCtsANegated	= 0x01<<m2681IpCtsABit,
	m2681IpCtsAAsserted	= 0x00<<m2681IpCtsABit
	};


typedef uint8_t	M2681OPCR;

enum{	/* M2681OPCR (Output Port Control Register) Bit definitions */
	m2681OpcrOp7Bit		=	7,
	m2681OpcrOp6Bit		=	6,
	m2681OpcrOp5Bit		=	5,
	m2681OpcrOp4Bit		=	4,
	m2681OpcrOp3Bit		=	3,
	m2681OpcrOp2Bit		=	2,
	m2681OpcrOp1Bit		=	1,
	m2681OpcrOp0Bit		=	0
	};

enum{	/* M2681OPCR (Output Port Control Register) Bit definitions */
	m2681OpcrOp7Mask	= 0x01<<m2681OpcrOp7Bit,
	m2681OpcrOp7TxRdyB	= 0x01<<m2681OpcrOp7Bit,
	m2681OpcrOp7Op7		= 0x00<<m2681OpcrOp7Bit,
	m2681OpcrOp6Mask	= 0x01<<m2681OpcrOp6Bit,
	m2681OpcrOp6TxRdyA	= 0x01<<m2681OpcrOp6Bit,
	m2681OpcrOp6Op6		= 0x00<<m2681OpcrOp6Bit,
	m2681OpcrOp5Mask	= 0x01<<m2681OpcrOp5Bit,
	m2681OpcrOp5RxRdyB	= 0x01<<m2681OpcrOp5Bit,
	m2681OpcrOp5Op5		= 0x01<<m2681OpcrOp5Bit,
	m2681OpcrOp4Mask	= 0x01<<m2681OpcrOp4Bit,
	m2681OpcrOp4RxRdyA	= 0x01<<m2681OpcrOp4Bit,
	m2681OpcrOp4Op4		= 0x01<<m2681OpcrOp4Bit,
	m2681OpcrOp3Mask	= 0x01<<m2681OpcrOp3Bit,
	m2681OpcrOp2Mask	= 0x01<<m2681OpcrOp2Bit,
	m2681OpcrOp1Mask	= 0x01<<m2681OpcrOp1Bit,
	m2681OpcrOp1RtsB	= 0x01<<m2681OpcrOp1Bit,
	m2681OpcrOp1Op1		= 0x01<<m2681OpcrOp1Bit,
	m2681OpcrOp0Mask	= 0x01<<m2681OpcrOp0Bit,
	m2681OpcrOp0RtsA	= 0x01<<m2681OpcrOp0Bit,
	m2681OpcrOp0Op0		= 0x01<<m2681OpcrOp0Bit
	};

typedef uint8_t	M2681OP;

enum{	/* M2681OP (Output Port Data Register) Bit definitions */
	m2681OpOp7Bit	= 7,
	m2681OpOp6Bit	= 6,
	m2681OpOp5Bit	= 5,
	m2681OpOp4Bit	= 4,
	m2681OpOp3Bit	= 3,
	m2681OpOp2Bit	= 2,
	m2681OpOp1Bit	= 1,
	m2681OpOp0Bit	= 0
	};

enum{	/* M2681OP (Output Port Data Register) masks */
	m2681OpOp7Mask		= 0x01<<m2681OpOp7Bit,
	m2681OpOp7Alter		= 0x01<<m2681OpOp7Bit,
	m2681OpOp7Retain	= 0x00<<m2681OpOp7Bit,
	m2681OpOp6Mask		= 0x01<<m2681OpOp6Bit,
	m2681OpOp6Alter		= 0x01<<m2681OpOp6Bit,
	m2681OpOp6Retain	= 0x00<<m2681OpOp6Bit,
	m2681OpOp5Mask		= 0x01<<m2681OpOp5Bit,
	m2681OpOp5Alter		= 0x01<<m2681OpOp5Bit,
	m2681OpOp5Retain	= 0x00<<m2681OpOp5Bit,
	m2681OpOp4Mask		= 0x01<<m2681OpOp4Bit,
	m2681OpOp4Alter		= 0x01<<m2681OpOp4Bit,
	m2681OpOp4Retain	= 0x00<<m2681OpOp4Bit,
	m2681OpOp3Mask		= 0x01<<m2681OpOp3Bit,
	m2681OpOp3Alter		= 0x01<<m2681OpOp3Bit,
	m2681OpOp3Retain	= 0x00<<m2681OpOp3Bit,
	m2681OpOp2Mask		= 0x01<<m2681OpOp2Bit,
	m2681OpOp2Alter		= 0x01<<m2681OpOp2Bit,
	m2681OpOp2Retain	= 0x00<<m2681OpOp2Bit,
	m2681OpOp1Mask		= 0x01<<m2681OpOp1Bit,
	m2681OpOp1Alter		= 0x01<<m2681OpOp1Bit,
	m2681OpOp1Retain	= 0x00<<m2681OpOp1Bit,
	m2681OpOp0Mask		= 0x01<<m2681OpOp0Bit,
	m2681OpOp0Alter		= 0x01<<m2681OpOp0Bit,
	m2681OpOp0Retain	= 0x00<<m2681OpOp0Bit
	};

typedef uint8_t	M2681MR2;

enum{	/* M2681MR2 (Mode Register 2) Bit definitions */
	m2681Mr2CmBit		= 6,
	m2681Mr2TxRtsBit	= 5,
	m2681Mr2TxCtsBit	= 4,
	m2681Mr2SbBit		= 0
	};

enum{	/* M2681MR2 (Mode Register 2) Bit definitions */
	m2681Mr2CmMask				= 0x03<<m2681Mr2CmBit,
	m2681Mr2CmNormal			= 0x00<<m2681Mr2CmBit,
	m2681Mr2CmAutoEcho			= 0x01<<m2681Mr2CmBit,
	m2681Mr2CmLocalLoop			= 0x02<<m2681Mr2CmBit,
	m2681Mr2CmRemoteLoop		= 0x03<<m2681Mr2CmBit,
	m2681Mr2TxRtsMask			= 0x01<<m2681Mr2TxRtsBit,
	m2681Mr2TxRtsAutoNegateOnTx	= 0x01<<m2681Mr2TxRtsBit,
	m2681Mr2TxRtsNormal			= 0x00<<m2681Mr2TxRtsBit,
	m2681Mr2TxCtsMask			= 0x01<<m2681Mr2TxCtsBit,
	m2681Mr2TxCtsAuto			= 0x01<<m2681Mr2TxCtsBit,
	m2681Mr2TxCtsManual			= 0x01<<m2681Mr2TxCtsBit,
	m2681Mr2SbMask				= 0x0F<<m2681Mr2SbBit,
	m2681Mr2SbL68_0_563			= 0x00<<m2681Mr2SbBit,
	m2681Mr2SbL68_0_625			= 0x01<<m2681Mr2SbBit,
	m2681Mr2SbL68_0_688			= 0x02<<m2681Mr2SbBit,
	m2681Mr2SbL68_0_750			= 0x03<<m2681Mr2SbBit,
	m2681Mr2SbL68_0_813			= 0x04<<m2681Mr2SbBit,
	m2681Mr2SbL68_0_875			= 0x05<<m2681Mr2SbBit,
	m2681Mr2SbL68_0_938			= 0x06<<m2681Mr2SbBit,
	m2681Mr2SbL68_1_000			= 0x07<<m2681Mr2SbBit,
	m2681Mr2SbL68_1_563			= 0x08<<m2681Mr2SbBit,
	m2681Mr2SbL68_1_625			= 0x09<<m2681Mr2SbBit,
	m2681Mr2SbL68_1_688			= 0x0A<<m2681Mr2SbBit,
	m2681Mr2SbL68_1_750			= 0x0B<<m2681Mr2SbBit,
	m2681Mr2SbL68_1_813			= 0x0C<<m2681Mr2SbBit,
	m2681Mr2SbL68_1_875			= 0x0D<<m2681Mr2SbBit,
	m2681Mr2SbL68_1_938			= 0x0E<<m2681Mr2SbBit,
	m2681Mr2SbL68_2_000			= 0x0F<<m2681Mr2SbBit,
	m2681Mr2SbL5_1_063			= 0x00<<m2681Mr2SbBit,
	m2681Mr2SbL5_1_125			= 0x01<<m2681Mr2SbBit,
	m2681Mr2SbL5_1_188			= 0x02<<m2681Mr2SbBit,
	m2681Mr2SbL5_1_250			= 0x03<<m2681Mr2SbBit,
	m2681Mr2SbL5_1_313			= 0x04<<m2681Mr2SbBit,
	m2681Mr2SbL5_1_375			= 0x05<<m2681Mr2SbBit,
	m2681Mr2SbL5_1_438			= 0x06<<m2681Mr2SbBit,
	m2681Mr2SbL5_1_500			= 0x07<<m2681Mr2SbBit,
	m2681Mr2SbL5_1_563			= 0x08<<m2681Mr2SbBit,
	m2681Mr2SbL5_1_625			= 0x09<<m2681Mr2SbBit,
	m2681Mr2SbL5_1_688			= 0x0A<<m2681Mr2SbBit,
	m2681Mr2SbL5_1_750			= 0x0B<<m2681Mr2SbBit,
	m2681Mr2SbL5_1_813			= 0x0C<<m2681Mr2SbBit,
	m2681Mr2SbL5_1_875			= 0x0D<<m2681Mr2SbBit,
	m2681Mr2SbL5_1_938			= 0x0E<<m2681Mr2SbBit,
	m2681Mr2SbL5_2_000			= 0x0F<<m2681Mr2SbBit
	};

typedef struct M2681ReadOnly{
	uint8_t					dna700;		/* MCR (High) in read/write */
	uint8_t					dna701;		/* MCR (Low) in read/write */
	uint8_t					dna702;		/* Do not access,reserved */
	uint8_t					dna703;		/* Do not access,reserved */
	uint8_t					dna704;		/* ILR in read/write */
	uint8_t					dna705;		/* IVR in read/write */
	uint8_t					dna706;		/* Do not access,reserved */
	uint8_t					dna707;		/* Do not access,reserved */
	uint8_t					dna708;		/* Do not access,reserved */
	uint8_t					dna709;		/* Do not access,reserved */
	uint8_t					dna70A;		/* Do not access,reserved */
	uint8_t					dna70B;		/* Do not access,reserved */
	uint8_t					dna70C;		/* Do not access,reserved */
	uint8_t					dna70D;		/* Do not access,reserved */
	uint8_t					dna70E;		/* Do not access,reserved */
	uint8_t					dna70F;		/* Do not access,reserved */
	uint8_t					dna710;		/* MR1 in read/write */
	READONLY		M2681SR		sra;		/* Status Register (SRA) */
	uint8_t					dna712;		/* CRA in write only */
	READONLY		M2681RB		rba;		/* Receive Buffer (RBA) */
	READONLY		M2681IPCR	ipcr;		/* Input Port Change Register (IPCR) */
	READONLY		M2681ISR	isr;		/* Interrupt Status Register (ISR) */
	uint8_t					dna716;		/* Do not access,reserved */
	uint8_t					dna717;		/* Do not access,reserved */
	uint8_t					dna718;		/* MR1B in read/write */
	READONLY		M2681SR		srb;		/* Status Register (SRB) */
	uint8_t					dna71A;		/* CRB in write only */
	READONLY		M2681RB		rbb;		/* Receive Buffer (RBB) */
	uint8_t					dna71C;		/* Do not access,reserved */
	READONLY		M2681IP		ip;			/* Input Port Register (IP) */
	uint8_t					dna71E;		/* OP Bit Set in write only */
	uint8_t					dna71F;		/* OP Bit Clear in write only */
	uint8_t					dna720;		/* MR2A in read/write */
	uint8_t					dna721;		/* MR2B in read/write */
	}M2681ReadOnly;

typedef struct M2681WriteOnly{
	uint8_t			dna700;		/* MCR (High) in read/write */
	uint8_t			dna701;		/* MCR (Low) in read/write */
	uint8_t			dna702;		/* Do not access,reserved */
	uint8_t			dna703;		/* Do not access,reserved */
	uint8_t			dna704;		/* ILR in read/write */
	uint8_t			dna705;		/* IVR in read/write */
	uint8_t			dna706;		/* Do not access,reserved */
	uint8_t			dna707;		/* Do not access,reserved */
	uint8_t			dna708;		/* Do not access,reserved */
	uint8_t			dna709;		/* Do not access,reserved */
	uint8_t			dna70A;		/* Do not access,reserved */
	uint8_t			dna70B;		/* Do not access,reserved */
	uint8_t			dna70C;		/* Do not access,reserved */
	uint8_t			dna70D;		/* Do not access,reserved */
	uint8_t			dna70E;		/* Do not access,reserved */
	uint8_t			dna70F;		/* Do not access,reserved */
	uint8_t			dna710;		/* MR1 in read/write */
	WRITEONLY M2681CSR	csra;		/* Clock Select Register (CSRA) */
	WRITEONLY M2681CR	cra;		/* Command Register (CRA) */
	WRITEONLY M2681TB	tba;		/* Transmit Buffer (TBA) */
	WRITEONLY M2681ACR	acr;		/* Auxiliary Control Register (ACR) */
	WRITEONLY M2681IER	ier;		/* Interrupt Enable Register (IER) */
	uint8_t			dna716;		/* Do not access,reserved */
	uint8_t			dna717;		/* Do not access,reserved */
	uint8_t			dna718;		/* MR1B in read/write */
	WRITEONLY M2681CSR	csrb;		/* Clock Select Register (CSRB) */
	WRITEONLY M2681CR	crb;		/* Command Register (CRB) */
	WRITEONLY M2681TB	tbb;		/* Transmit Buffer (TBB) */
	uint8_t			dna71C;		/* Do not access,reserved */
	WRITEONLY M2681OPCR	opcr;		/* Output Port Control Register (OPCR) */
	WRITEONLY M2681OP	set;		/* Output Port Bit Set (OP) */
	WRITEONLY M2681OP	clear;		/* Output Port Bit Clear (OP) */
	uint8_t			dna720;		/* MR2A in read/write */
	uint8_t			dna721;		/* MR2B in read/write */
	}M2681WriteOnly;

typedef struct M2681ReadWrite{
	M2681MCR			mcr;		/* Module Configuration Register (MCR) */
	uint8_t			dna702;		/* Do not access,reserved */
	uint8_t			dna703;		/* Do not access,reserved */
	M2681ILR			ilr;		/* Interrupt Level Register (ILR) */
	M2681IVR			ivr;		/* Interrupt Vector Register (IVR) */
	uint8_t			dna706;		/* Do not access,reserved */
	uint8_t			dna707;		/* Do not access,reserved */
	uint8_t			dna708;		/* Do not access,reserved */
	uint8_t			dna709;		/* Do not access,reserved */
	uint8_t			dna70A;		/* Do not access,reserved */
	uint8_t			dna70B;		/* Do not access,reserved */
	uint8_t			dna70C;		/* Do not access,reserved */
	uint8_t			dna70D;		/* Do not access,reserved */
	uint8_t			dna70E;		/* Do not access,reserved */
	uint8_t			dna70F;		/* Do not access,reserved */
	M2681MR1			mr1a;		/* Mode Register 1 (MR1A) */
	uint8_t			dna711;		/* SRA/CSRA in read only/write only */
	uint8_t			dna712;		/* CRA in write only */
	uint8_t			dna713;		/* RBA/TBA in read only/write only */
	uint8_t			dna714;		/* IPCR/ACR in read only/write only */
	uint8_t			dna715;		/* ISR/IER in read only/write only */
	uint8_t			dna716;		/* Do not access,reserved */
	uint8_t			dna717;		/* Do not access,reserved */
	M2681MR1			mr1b;		/* Mode Register 1 (MR1B) */
	uint8_t			dna719;		/* SRB/CSRB in read only/write only */
	uint8_t			dna71A;		/* CRB in write only */
	uint8_t			dna71B;		/* RBB/TBB in read only/write only */
	uint8_t			dna71C;		/* Do not access,reserved */
	uint8_t			dna71D;		/* IP/OPCR in read only/write only */
	uint8_t			dna71E;		/* OP Bit Set in write only */
	uint8_t			dna71F;		/* OP Bit Clear in write only */
	M2681MR2			mr2a;		/* Mode Register 2 (MRA) */
	M2681MR2			mr2b;		/* Mode Register 2 (MRB) */
	}M2681ReadWrite;

typedef union M2681{
	READONLY		M2681ReadOnly	ro;	/* Read Only registers */
	WRITEONLY		M2681WriteOnly	wo;	/* Write Only registers */
					M2681ReadWrite	rw;	/* Read/Write registers */
	}M2681;

#endif
