/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _hw_mot_quicc_brgcregh_
#define _hw_mot_quicc_brgcregh_
#include <stdint.h>
namespace Oscl { // Namespace description
	namespace Motorola { // Namespace description
		namespace Quicc { // Namespace description
			namespace BRGC { // Register description
				typedef uint32_t	Reg;
				namespace RST { // Field Description
					enum {Lsb = 17};
					enum {FieldMask = 0x00020000};
					enum {Value_Enable = 0x0};
					enum {ValueMask_Enable = 0x00000000};
					enum {Value_Reset = 0x1};
					enum {ValueMask_Reset = 0x00020000};
					};
				namespace EN { // Field Description
					enum {Lsb = 16};
					enum {FieldMask = 0x00010000};
					enum {Value_Stop = 0x0};
					enum {ValueMask_Stop = 0x00000000};
					enum {Value_Enable = 0x1};
					enum {ValueMask_Enable = 0x00010000};
					};
				namespace EXTC { // Field Description
					enum {Lsb = 14};
					enum {FieldMask = 0x0000C000};
					enum {Value_BRGCLK = 0x0};
					enum {ValueMask_BRGCLK = 0x00000000};
					enum {Value_CLK2 = 0x1};
					enum {ValueMask_CLK2 = 0x00004000};
					enum {Value_CLK6 = 0x2};
					enum {ValueMask_CLK6 = 0x00008000};
					};
				namespace ATB { // Field Description
					enum {Lsb = 13};
					enum {FieldMask = 0x00006000};
					enum {Value_Normal = 0x0};
					enum {ValueMask_Normal = 0x00000000};
					enum {Value_Auto = 0x1};
					enum {ValueMask_Auto = 0x00002000};
					};
				namespace CD { // Field Description
					enum {Lsb = 1};
					enum {FieldMask = 0x00001FFE};
					enum {Value_Min = 0x0};
					enum {ValueMask_Min = 0x00000000};
					enum {Value_Max = 0x3FF};
					enum {ValueMask_Max = 0x000007FE};
					};
				namespace DIV16 { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x00000001};
					enum {Value_DivideBy1 = 0x0};
					enum {ValueMask_DivideBy1 = 0x00000000};
					enum {Value_DivideBy16 = 0x1};
					enum {ValueMask_DivideBy16 = 0x00000001};
					};
				};
			};
		};
	};
#endif
