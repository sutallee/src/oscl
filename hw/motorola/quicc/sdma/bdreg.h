/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _hw_motorola_sdma_bdregh_
#define _hw_motorola_sdma_bdregh_
#include <stdint.h>
namespace Oscl { // Namespace description
	namespace Motorola { // Namespace description
		namespace Quicc { // Namespace description
			namespace Sdma { // Namespace description
				namespace RxBD { // Namespace description
					namespace Status { // Register description
						typedef uint16_t	Reg;
						namespace E { // Field Description
							enum {Lsb = 15};
							enum {FieldMask = 0x00008000};
							enum {Value_Complete = 0x0};
							enum {ValueMask_Complete = 0x00000000};
							enum {Value_Empty = 0x1};
							enum {ValueMask_Empty = 0x00008000};
							};
						namespace W { // Field Description
							enum {Lsb = 13};
							enum {FieldMask = 0x00002000};
							enum {Value_NotLastDesc = 0x0};
							enum {ValueMask_NotLastDesc = 0x00000000};
							enum {Value_LastDesc = 0x1};
							enum {ValueMask_LastDesc = 0x00002000};
							};
						namespace I { // Field Description
							enum {Lsb = 12};
							enum {FieldMask = 0x00001000};
							enum {Value_Disable = 0x0};
							enum {ValueMask_Disable = 0x00000000};
							enum {Value_Enable = 0x1};
							enum {ValueMask_Enable = 0x00001000};
							};
						namespace CM { // Field Description
							enum {Lsb = 9};
							enum {FieldMask = 0x00000200};
							enum {Value_Normal = 0x0};
							enum {ValueMask_Normal = 0x00000000};
							enum {Value_Continuous = 0x1};
							enum {ValueMask_Continuous = 0x00000200};
							};
						namespace Eof { // Field Description
							enum {Lsb = 0};
							enum {FieldMask = 0x0000FFFF};
							namespace L { // Field Description
								enum {Lsb = 11};
								enum {FieldMask = 0x00000800};
								enum {Value_NotLastDesc = 0x0};
								enum {ValueMask_NotLastDesc = 0x00000000};
								enum {Value_LastDesc = 0x1};
								enum {ValueMask_LastDesc = 0x00000800};
								};
							namespace Sof { // Field Description
								enum {Lsb = 0};
								enum {FieldMask = 0x0000FFFF};
								namespace F { // Field Description
									enum {Lsb = 10};
									enum {FieldMask = 0x00000400};
									enum {Value_NotFirstInFrame = 0x0};
									enum {ValueMask_NotFirstInFrame = 0x00000000};
									enum {Value_FirstInFrame = 0x1};
									enum {ValueMask_FirstInFrame = 0x00000400};
									};
								namespace HDLC { // Field Description
									enum {Lsb = 0};
									enum {FieldMask = 0x0000FFFF};
									namespace CM { // Field Description
										enum {Lsb = 9};
										enum {FieldMask = 0x00000200};
										enum {Value_Disable = 0x0};
										enum {ValueMask_Disable = 0x00000000};
										enum {Value_Enable = 0x1};
										enum {ValueMask_Enable = 0x00000200};
										};
									namespace DE { // Field Description
										enum {Lsb = 7};
										enum {FieldMask = 0x00000080};
										enum {Value_NoError = 0x0};
										enum {ValueMask_NoError = 0x00000000};
										enum {Value_Error = 0x1};
										enum {ValueMask_Error = 0x00000080};
										};
									namespace LG { // Field Description
										enum {Lsb = 5};
										enum {FieldMask = 0x00000020};
										enum {Value_NoError = 0x0};
										enum {ValueMask_NoError = 0x00000000};
										enum {Value_Error = 0x1};
										enum {ValueMask_Error = 0x00000020};
										};
									namespace NO { // Field Description
										enum {Lsb = 4};
										enum {FieldMask = 0x00000010};
										enum {Value_NoError = 0x0};
										enum {ValueMask_NoError = 0x00000000};
										enum {Value_Error = 0x1};
										enum {ValueMask_Error = 0x00000010};
										};
									namespace AB { // Field Description
										enum {Lsb = 3};
										enum {FieldMask = 0x00000008};
										enum {Value_NoError = 0x0};
										enum {ValueMask_NoError = 0x00000000};
										enum {Value_RxAbort = 0x1};
										enum {ValueMask_RxAbort = 0x00000008};
										};
									namespace CR { // Field Description
										enum {Lsb = 2};
										enum {FieldMask = 0x00000004};
										enum {Value_NoError = 0x0};
										enum {ValueMask_NoError = 0x00000000};
										enum {Value_Error = 0x1};
										enum {ValueMask_Error = 0x00000004};
										};
									namespace OV { // Field Description
										enum {Lsb = 1};
										enum {FieldMask = 0x00000002};
										enum {Value_NoError = 0x0};
										enum {ValueMask_NoError = 0x00000000};
										enum {Value_Error = 0x1};
										enum {ValueMask_Error = 0x00000002};
										};
									namespace CD { // Field Description
										enum {Lsb = 0};
										enum {FieldMask = 0x00000001};
										enum {Value_NoError = 0x0};
										enum {ValueMask_NoError = 0x00000000};
										enum {Value_LostCarrier = 0x1};
										enum {ValueMask_LostCarrier = 0x00000001};
										};
									};
								namespace AsyncHDLC { // Field Description
									enum {Lsb = 0};
									enum {FieldMask = 0x0000FFFF};
									namespace CM { // Field Description
										enum {Lsb = 9};
										enum {FieldMask = 0x00000200};
										};
									namespace BRK { // Field Description
										enum {Lsb = 7};
										enum {FieldMask = 0x00000080};
										};
									namespace BOF { // Field Description
										enum {Lsb = 6};
										enum {FieldMask = 0x00000040};
										};
									namespace AB { // Field Description
										enum {Lsb = 3};
										enum {FieldMask = 0x00000008};
										};
									namespace CR { // Field Description
										enum {Lsb = 2};
										enum {FieldMask = 0x00000004};
										};
									namespace OV { // Field Description
										enum {Lsb = 1};
										enum {FieldMask = 0x00000002};
										};
									namespace CD { // Field Description
										enum {Lsb = 0};
										enum {FieldMask = 0x00000001};
										};
									};
								namespace BISYNC { // Field Description
									enum {Lsb = 0};
									enum {FieldMask = 0x0000FFFF};
									namespace CM { // Field Description
										enum {Lsb = 9};
										enum {FieldMask = 0x00000200};
										};
									namespace DE { // Field Description
										enum {Lsb = 7};
										enum {FieldMask = 0x00000080};
										};
									namespace NO { // Field Description
										enum {Lsb = 4};
										enum {FieldMask = 0x00000010};
										};
									namespace PR { // Field Description
										enum {Lsb = 3};
										enum {FieldMask = 0x00000008};
										};
									namespace CR { // Field Description
										enum {Lsb = 2};
										enum {FieldMask = 0x00000004};
										};
									namespace OV { // Field Description
										enum {Lsb = 1};
										enum {FieldMask = 0x00000002};
										};
									namespace CD { // Field Description
										enum {Lsb = 0};
										enum {FieldMask = 0x00000001};
										};
									};
								namespace Ethernet { // Field Description
									enum {Lsb = 0};
									enum {FieldMask = 0x0000FFFF};
									namespace M { // Field Description
										enum {Lsb = 8};
										enum {FieldMask = 0x00000100};
										};
									namespace LG { // Field Description
										enum {Lsb = 5};
										enum {FieldMask = 0x00000020};
										};
									namespace NO { // Field Description
										enum {Lsb = 4};
										enum {FieldMask = 0x00000010};
										};
									namespace SH { // Field Description
										enum {Lsb = 3};
										enum {FieldMask = 0x00000008};
										};
									namespace CR { // Field Description
										enum {Lsb = 2};
										enum {FieldMask = 0x00000004};
										};
									namespace OV { // Field Description
										enum {Lsb = 1};
										enum {FieldMask = 0x00000002};
										};
									namespace CD { // Field Description
										enum {Lsb = 0};
										enum {FieldMask = 0x00000001};
										};
									};
								namespace Transparent { // Field Description
									enum {Lsb = 0};
									enum {FieldMask = 0x0000FFFF};
									namespace CM { // Field Description
										enum {Lsb = 9};
										enum {FieldMask = 0x00000200};
										};
									namespace DE { // Field Description
										enum {Lsb = 7};
										enum {FieldMask = 0x00000080};
										};
									namespace NO { // Field Description
										enum {Lsb = 4};
										enum {FieldMask = 0x00000010};
										};
									namespace CR { // Field Description
										enum {Lsb = 2};
										enum {FieldMask = 0x00000004};
										};
									namespace OV { // Field Description
										enum {Lsb = 1};
										enum {FieldMask = 0x00000002};
										};
									namespace CD { // Field Description
										enum {Lsb = 0};
										enum {FieldMask = 0x00000001};
										};
									};
								};
							};
						namespace UART { // Field Description
							enum {Lsb = 0};
							enum {FieldMask = 0x0000FFFF};
							namespace E { // Field Description
								enum {Lsb = 15};
								enum {FieldMask = 0x00008000};
								};
							namespace W { // Field Description
								enum {Lsb = 13};
								enum {FieldMask = 0x00002000};
								};
							namespace I { // Field Description
								enum {Lsb = 12};
								enum {FieldMask = 0x00001000};
								};
							namespace C { // Field Description
								enum {Lsb = 11};
								enum {FieldMask = 0x00000800};
								};
							namespace A { // Field Description
								enum {Lsb = 10};
								enum {FieldMask = 0x00000400};
								};
							namespace CM { // Field Description
								enum {Lsb = 9};
								enum {FieldMask = 0x00000200};
								};
							namespace ID { // Field Description
								enum {Lsb = 8};
								enum {FieldMask = 0x00000100};
								};
							namespace AM { // Field Description
								enum {Lsb = 7};
								enum {FieldMask = 0x00000080};
								};
							namespace BR { // Field Description
								enum {Lsb = 5};
								enum {FieldMask = 0x00000020};
								};
							namespace FR { // Field Description
								enum {Lsb = 4};
								enum {FieldMask = 0x00000010};
								};
							namespace PR { // Field Description
								enum {Lsb = 3};
								enum {FieldMask = 0x00000008};
								};
							namespace OV { // Field Description
								enum {Lsb = 1};
								enum {FieldMask = 0x00000002};
								};
							namespace CD { // Field Description
								enum {Lsb = 0};
								enum {FieldMask = 0x00000001};
								};
							};
						namespace SmcUART { // Field Description
							enum {Lsb = 0};
							enum {FieldMask = 0x0000FFFF};
							namespace E { // Field Description
								enum {Lsb = 15};
								enum {FieldMask = 0x00008000};
								};
							namespace W { // Field Description
								enum {Lsb = 13};
								enum {FieldMask = 0x00002000};
								};
							namespace I { // Field Description
								enum {Lsb = 12};
								enum {FieldMask = 0x00001000};
								};
							namespace CM { // Field Description
								enum {Lsb = 9};
								enum {FieldMask = 0x00000200};
								};
							namespace ID { // Field Description
								enum {Lsb = 8};
								enum {FieldMask = 0x00000100};
								};
							namespace BR { // Field Description
								enum {Lsb = 5};
								enum {FieldMask = 0x00000020};
								};
							namespace FR { // Field Description
								enum {Lsb = 4};
								enum {FieldMask = 0x00000010};
								};
							namespace PR { // Field Description
								enum {Lsb = 3};
								enum {FieldMask = 0x00000008};
								};
							namespace OV { // Field Description
								enum {Lsb = 1};
								enum {FieldMask = 0x00000002};
								};
							};
						namespace SmcTransparent { // Field Description
							enum {Lsb = 0};
							enum {FieldMask = 0x0000FFFF};
							namespace E { // Field Description
								enum {Lsb = 15};
								enum {FieldMask = 0x00008000};
								};
							namespace W { // Field Description
								enum {Lsb = 13};
								enum {FieldMask = 0x00002000};
								};
							namespace I { // Field Description
								enum {Lsb = 12};
								enum {FieldMask = 0x00001000};
								};
							namespace CM { // Field Description
								enum {Lsb = 9};
								enum {FieldMask = 0x00000200};
								};
							namespace OV { // Field Description
								enum {Lsb = 1};
								enum {FieldMask = 0x00000002};
								};
							};
						namespace SPI { // Field Description
							enum {Lsb = 0};
							enum {FieldMask = 0x0000FFFF};
							namespace E { // Field Description
								enum {Lsb = 15};
								enum {FieldMask = 0x00008000};
								};
							namespace W { // Field Description
								enum {Lsb = 13};
								enum {FieldMask = 0x00002000};
								};
							namespace I { // Field Description
								enum {Lsb = 12};
								enum {FieldMask = 0x00001000};
								};
							namespace L { // Field Description
								enum {Lsb = 11};
								enum {FieldMask = 0x00000800};
								};
							namespace CM { // Field Description
								enum {Lsb = 9};
								enum {FieldMask = 0x00000200};
								};
							namespace OV { // Field Description
								enum {Lsb = 1};
								enum {FieldMask = 0x00000002};
								};
							namespace ME { // Field Description
								enum {Lsb = 0};
								enum {FieldMask = 0x00000001};
								};
							};
						namespace I2C { // Field Description
							enum {Lsb = 0};
							enum {FieldMask = 0x0000FFFF};
							namespace E { // Field Description
								enum {Lsb = 15};
								enum {FieldMask = 0x00008000};
								};
							namespace W { // Field Description
								enum {Lsb = 13};
								enum {FieldMask = 0x00002000};
								};
							namespace I { // Field Description
								enum {Lsb = 12};
								enum {FieldMask = 0x00001000};
								};
							namespace L { // Field Description
								enum {Lsb = 11};
								enum {FieldMask = 0x00000800};
								};
							namespace OV { // Field Description
								enum {Lsb = 1};
								enum {FieldMask = 0x00000002};
								};
							};
						namespace PIP { // Field Description
							enum {Lsb = 0};
							enum {FieldMask = 0x0000FFFF};
							namespace E { // Field Description
								enum {Lsb = 15};
								enum {FieldMask = 0x00008000};
								};
							namespace W { // Field Description
								enum {Lsb = 13};
								enum {FieldMask = 0x00002000};
								};
							namespace I { // Field Description
								enum {Lsb = 12};
								enum {FieldMask = 0x00001000};
								};
							namespace C { // Field Description
								enum {Lsb = 11};
								enum {FieldMask = 0x00000800};
								};
							namespace CM { // Field Description
								enum {Lsb = 9};
								enum {FieldMask = 0x00000200};
								};
							namespace SL { // Field Description
								enum {Lsb = 8};
								enum {FieldMask = 0x00000100};
								};
							};
						};
					};
				namespace TxBD { // Namespace description
					namespace Status { // Register description
						typedef uint16_t	Reg;
						namespace R { // Field Description
							enum {Lsb = 15};
							enum {FieldMask = 0x00008000};
							enum {Value_Complete = 0x0};
							enum {ValueMask_Complete = 0x00000000};
							enum {Value_Ready = 0x1};
							enum {ValueMask_Ready = 0x00008000};
							};
						namespace W { // Field Description
							enum {Lsb = 13};
							enum {FieldMask = 0x00002000};
							enum {Value_NotLastDesc = 0x0};
							enum {ValueMask_NotLastDesc = 0x00000000};
							enum {Value_LastDesc = 0x1};
							enum {ValueMask_LastDesc = 0x00002000};
							};
						namespace I { // Field Description
							enum {Lsb = 12};
							enum {FieldMask = 0x00001000};
							enum {Value_Disable = 0x0};
							enum {ValueMask_Disable = 0x00000000};
							enum {Value_Enable = 0x1};
							enum {ValueMask_Enable = 0x00001000};
							};
						namespace L { // Field Description
							enum {Lsb = 11};
							enum {FieldMask = 0x00000800};
							enum {Value_NotLastDesc = 0x0};
							enum {ValueMask_NotLastDesc = 0x00000000};
							enum {Value_LastDesc = 0x1};
							enum {ValueMask_LastDesc = 0x00000800};
							};
						namespace PAD { // Field Description
							enum {Lsb = 14};
							enum {FieldMask = 0x00004000};
							enum {Value_PadFrame = 0x1};
							enum {ValueMask_PadFrame = 0x00004000};
							enum {Value_DontPadFrame = 0x0};
							enum {ValueMask_DontPadFrame = 0x00000000};
							};
						namespace TC { // Field Description
							enum {Lsb = 10};
							enum {FieldMask = 0x00000400};
							enum {Value_NoCRC = 0x0};
							enum {ValueMask_NoCRC = 0x00000000};
							enum {Value_TxCRC = 0x1};
							enum {ValueMask_TxCRC = 0x00000400};
							};
						namespace Ethernet { // Field Description
							enum {Lsb = 0};
							enum {FieldMask = 0x0000FFFF};
							namespace CSL { // Field Description
								enum {Lsb = 0};
								enum {FieldMask = 0x00000001};
								enum {Value_NoError = 0x0};
								enum {ValueMask_NoError = 0x00000000};
								enum {Value_CarrierSenseLost = 0x1};
								enum {ValueMask_CarrierSenseLost = 0x00000001};
								};
							namespace UN { // Field Description
								enum {Lsb = 1};
								enum {FieldMask = 0x00000002};
								enum {Value_NoError = 0x0};
								enum {ValueMask_NoError = 0x00000000};
								enum {Value_Underrun = 0x1};
								enum {ValueMask_Underrun = 0x00000002};
								};
							namespace RC { // Field Description
								enum {Lsb = 2};
								enum {FieldMask = 0x0000003C};
								enum {Value_NoError = 0x0};
								enum {ValueMask_NoError = 0x00000000};
								};
							namespace RL { // Field Description
								enum {Lsb = 6};
								enum {FieldMask = 0x00000040};
								enum {Value_NoError = 0x0};
								enum {ValueMask_NoError = 0x00000000};
								enum {Value_RetryLimitExceeded = 0x1};
								enum {ValueMask_RetryLimitExceeded = 0x00000040};
								};
							namespace LC { // Field Description
								enum {Lsb = 7};
								enum {FieldMask = 0x00000080};
								enum {Value_NoError = 0x0};
								enum {ValueMask_NoError = 0x00000000};
								enum {Value_LateCollision = 0x1};
								enum {ValueMask_LateCollision = 0x00000080};
								};
							namespace HB { // Field Description
								enum {Lsb = 8};
								enum {FieldMask = 0x00000100};
								enum {Value_NoError = 0x0};
								enum {ValueMask_NoError = 0x00000000};
								enum {Value_HeartBeatMissed = 0x1};
								enum {ValueMask_HeartBeatMissed = 0x00000100};
								};
							};
						};
					};
				};
			};
		};
	};
#endif
