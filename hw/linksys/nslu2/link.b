#
# The project overrides.b should define:
#
#FINAL_LINK_FLAGS=   \
#	-T $(srcroot)/src/oscl/hw/est/mdp8xx/mem8Mram8MflashDebug.map \
#	$(BSP_LINK_FLAGS)
#
# The first "-T" argument defines the size of RAM and Flash, and
# may vary with the implementation.
#

BSP_LINK_FLAGS=	\
	-T $(srcroot)/src/oscl/hw/intel/ixp4xx/link.map \
	-T $(srcroot)/src/oscl/hw/intel/ixp4xx/link.x

