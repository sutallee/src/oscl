/*
   Copyright (C) 2024 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_hw_rocktech_rk043fn48hh_
#define _oscl_hw_rocktech_rk043fn48hh_

namespace Oscl {
namespace Rocktech {
namespace RK043FN48H {

static constexpr uint32_t	HorizontalSyncClocks		= 41U;
static constexpr uint32_t	HorizontalBackPorchClocks	= 13U;
static constexpr uint32_t	HorizontalFrontPorchClocks	= 32U;
static constexpr uint32_t	VerticalSyncClocks			= 10U;
static constexpr uint32_t	VerticalBackPorchClocks		= 2U;
static constexpr uint32_t	VerticalFrontPorchClocks	= 2U;
static constexpr uint32_t	WidthInPixels				= 480U;
static constexpr uint32_t	HeightInPixels				= 272U;

}
}
}


#endif
