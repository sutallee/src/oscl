/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _hw_usb_desc_deviceregh_
#define _hw_usb_desc_deviceregh_
#include <stdint.h>
#include "word.h"
namespace Oscl { // Namespace description
	namespace Usb { // Namespace description
		namespace Hw { // Namespace description
			namespace Desc { // Namespace description
				namespace Device { // Namespace description
					enum {Length = 18};
					namespace bcdUSB { // Register description
						typedef Oscl::Usb::Hw::Desc::Word	Reg;
						enum {XXX = 272};
						};
					namespace bDeviceClass { // Register description
						typedef uint8_t	Reg;
						enum {VendorSpecific = 255};
						enum {ConfigSpecified = 0};
						enum {HUB = 9};
						enum {HID = 3};
						};
					namespace bDeviceSubClass { // Register description
						typedef uint8_t	Reg;
						enum {ConfigSpecified = 0};
						enum {HidBoot = 1};
						};
					namespace bDeviceProtocol { // Register description
						typedef uint8_t	Reg;
						enum {StandardProtocol = 0};
						enum {VendorSpecific = 255};
						enum {HidKeyboard = 1};
						enum {HidMouse = 2};
						};
					namespace bMaxPacketSize { // Register description
						typedef uint8_t	Reg;
						enum {Size8 = 8};
						enum {Size16 = 16};
						enum {Size32 = 32};
						enum {Size64 = 64};
						};
					namespace idVendor { // Register description
						typedef Word	Reg;
						enum {MikeMoran = 43981};
						};
					namespace idProduct { // Register description
						typedef Word	Reg;
						namespace MikeMoran { // Field Description
							enum {Lsb = 0};
							enum {FieldMask = 0x0000FFFF};
							enum {Value_UltraCool = 0x0};
							enum {ValueMask_UltraCool = 0x00000000};
							};
						};
					namespace bcdDevice { // Register description
						typedef Word	Reg;
						};
					namespace iManufacturer { // Register description
						typedef uint8_t	Reg;
						};
					namespace iProduct { // Register description
						typedef uint8_t	Reg;
						};
					namespace iSerialNumber { // Register description
						typedef uint8_t	Reg;
						};
					namespace bNumConfigurations { // Register description
						typedef uint8_t	Reg;
						};
					};
				};
			};
		};
	};
#endif
