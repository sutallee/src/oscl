/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _hw_usb_desc_hubregh_
#define _hw_usb_desc_hubregh_
#include <stdint.h>
#include "word.h"
namespace Oscl { // Namespace description
	namespace Usb { // Namespace description
		namespace Hw { // Namespace description
			namespace Desc { // Namespace description
				namespace Hub { // Namespace description
					namespace bNbrPorts { // Register description
						typedef uint8_t	Reg;
						};
					namespace wHubCharacteristics { // Register description
						typedef Word	Reg;
						namespace LogicalPowerSwitchingMode { // Field Description
							enum {Lsb = 0};
							enum {FieldMask = 0x00000003};
							enum {Value_Ganged = 0x0};
							enum {ValueMask_Ganged = 0x00000000};
							enum {Value_Individual = 0x1};
							enum {ValueMask_Individual = 0x00000001};
							};
						namespace CompoundDevice { // Field Description
							enum {Lsb = 2};
							enum {FieldMask = 0x00000004};
							enum {Value_NotCompound = 0x0};
							enum {ValueMask_NotCompound = 0x00000000};
							enum {Value_Compound = 0x1};
							enum {ValueMask_Compound = 0x00000004};
							};
						namespace OverCurrentProtectionMode { // Field Description
							enum {Lsb = 3};
							enum {FieldMask = 0x00000018};
							enum {Value_Global = 0x0};
							enum {ValueMask_Global = 0x00000000};
							enum {Value_Individual = 0x0};
							enum {ValueMask_Individual = 0x00000000};
							enum {Value_None = 0x0};
							enum {ValueMask_None = 0x00000000};
							};
						};
					namespace bPwrOn2PwrGood { // Register description
						typedef uint8_t	Reg;
						};
					namespace bHubContrCurrent { // Register description
						typedef uint8_t	Reg;
						};
					namespace bDeviceRemovable { // Register description
						typedef uint8_t	Reg;
						enum {Removable = 0};
						enum {Fixed = 1};
						};
					namespace bPortPwrCtrlMask { // Register description
						typedef uint8_t	Reg;
						};
					namespace bmAttributes { // Register description
						typedef uint8_t	Reg;
						namespace Historical { // Field Description
							enum {Lsb = 7};
							enum {FieldMask = 0x00000080};
							enum {Value_AlwaysSet = 0x1};
							enum {ValueMask_AlwaysSet = 0x00000080};
							};
						namespace SelfPowered { // Field Description
							enum {Lsb = 6};
							enum {FieldMask = 0x00000040};
							enum {Value_HasLocalPowerSource = 0x1};
							enum {ValueMask_HasLocalPowerSource = 0x00000040};
							enum {Value_UsbBusPowerOnly = 0x0};
							enum {ValueMask_UsbBusPowerOnly = 0x00000000};
							};
						namespace RemoteWakeup { // Field Description
							enum {Lsb = 5};
							enum {FieldMask = 0x00000020};
							enum {Value_NotSupported = 0x0};
							enum {ValueMask_NotSupported = 0x00000000};
							enum {Value_Supported = 0x1};
							enum {ValueMask_Supported = 0x00000020};
							};
						};
					namespace bMaxPower { // Register description
						typedef uint8_t	Reg;
						};
					};
				};
			};
		};
	};
#endif
