/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _hw_usb_desc_hidh_
#define _hw_usb_desc_hidh_
#include <stdint.h>
namespace Oscl { // Namespace description
	namespace Usb { // Namespace description
		namespace Hw { // Namespace description
			namespace Desc { // Namespace description
				namespace HID { // Namespace description
					enum {Length = 9};
					namespace bcdHID { // Register description
						typedef uint16_t	Reg;
						};
					namespace bCountryCode { // Register description
						typedef uint8_t	Reg;
						enum {NotSupported = 0};
						enum {Arabic = 1};
						enum {Belgian = 2};
						enum {CanadianBilingual = 3};
						enum {CanadianFrench = 4};
						enum {CzechRepublic = 5};
						enum {Danish = 6};
						enum {Finnish = 7};
						enum {French = 8};
						enum {German = 9};
						enum {Greek = 10};
						enum {Hebrew = 11};
						enum {Hungary = 12};
						enum {InternationalISO = 13};
						enum {Italian = 14};
						enum {Katakana = 15};
						enum {Korean = 16};
						enum {LatinAmerican = 17};
						enum {NetherlandsDutch = 18};
						enum {Norwegian = 19};
						enum {PersianFarsi = 20};
						enum {Poland = 21};
						enum {Portuguese = 22};
						enum {Russia = 23};
						enum {Slovakia = 24};
						enum {Spanish = 25};
						enum {Swedish = 26};
						enum {SwissFrench = 27};
						enum {SwissGerman = 28};
						enum {Switzerland = 29};
						enum {Taiwan = 30};
						enum {TurkishQ = 31};
						enum {UK = 32};
						enum {US = 33};
						enum {Yugoslavia = 34};
						enum {TurkishF = 35};
						};
					namespace bNumDescriptors { // Register description
						typedef uint8_t	Reg;
						};
					namespace wItemLength { // Register description
						typedef uint16_t	Reg;
						};
					};
				};
			};
		};
	};
#endif
