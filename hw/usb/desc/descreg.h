/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _hw_usb_desc_descregh_
#define _hw_usb_desc_descregh_
#include <stdint.h>
namespace Oscl { // Namespace description
	namespace Usb { // Namespace description
		namespace Hw { // Namespace description
			namespace Desc { // Namespace description
				namespace Offset { // Namespace description
					enum {bLength = 0};
					enum {bDescritptorType = 1};
					};
				namespace bLength { // Register description
					typedef uint8_t	Reg;
					};
				namespace bDescriptorType { // Register description
					typedef uint8_t	Reg;
					enum {DEVICE = 1};
					enum {CONFIGURATION = 2};
					enum {STRING = 3};
					enum {INTERFACE = 4};
					enum {ENDPOINT = 5};
					enum {HUB = 41};
					enum {HID = 33};
					enum {HidReport = 34};
					};
				};
			};
		};
	};
#endif
