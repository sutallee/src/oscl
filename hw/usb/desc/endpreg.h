/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _hw_usb_desc_endpregh_
#define _hw_usb_desc_endpregh_
#include <stdint.h>
#include "word.h"
namespace Oscl { // Namespace description
	namespace Usb { // Namespace description
		namespace Hw { // Namespace description
			namespace Desc { // Namespace description
				namespace Endpoint { // Namespace description
					namespace bEndpointAddress { // Register description
						typedef uint8_t	Reg;
						namespace EndpointNumber { // Field Description
							enum {Lsb = 0};
							enum {FieldMask = 0x0000000F};
							enum {Value_Invalid = 0x0};
							enum {ValueMask_Invalid = 0x00000000};
							enum {Value_Min = 0x1};
							enum {ValueMask_Min = 0x00000001};
							enum {Value_Max = 0xF};
							enum {ValueMask_Max = 0x0000000F};
							};
						namespace Direction { // Field Description
							enum {Lsb = 7};
							enum {FieldMask = 0x00000080};
							enum {Value_OUT = 0x0};
							enum {ValueMask_OUT = 0x00000000};
							enum {Value_IN = 0x1};
							enum {ValueMask_IN = 0x00000080};
							};
						};
					namespace bmAttributes { // Register description
						typedef uint8_t	Reg;
						namespace TransferType { // Field Description
							enum {Lsb = 0};
							enum {FieldMask = 0x00000003};
							enum {Value_Control = 0x0};
							enum {ValueMask_Control = 0x00000000};
							enum {Value_Isochronous = 0x1};
							enum {ValueMask_Isochronous = 0x00000001};
							enum {Value_Bulk = 0x2};
							enum {ValueMask_Bulk = 0x00000002};
							enum {Value_Interrupt = 0x3};
							enum {ValueMask_Interrupt = 0x00000003};
							};
						};
					namespace wMaxPacketSize { // Register description
						typedef Word	Reg;
						};
					namespace bInterval { // Register description
						typedef uint8_t	Reg;
						};
					};
				};
			};
		};
	};
#endif
