/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _hw_usb_desc_ifregh_
#define _hw_usb_desc_ifregh_
#include <stdint.h>
namespace Oscl { // Namespace description
	namespace Usb { // Namespace description
		namespace Hw { // Namespace description
			namespace Desc { // Namespace description
				namespace Interface { // Namespace description
					namespace bInterfaceNumber { // Register description
						typedef uint8_t	Reg;
						};
					namespace bAlternateSetting { // Register description
						typedef uint8_t	Reg;
						};
					namespace bNumEndpoints { // Register description
						typedef uint8_t	Reg;
						};
					namespace bInterfaceClass { // Register description
						typedef uint8_t	Reg;
						enum {Future = 0};
						enum {MassStorage = 8};
						enum {VendorSpecific = 255};
						};
					namespace bInterfaceSubClass { // Register description
						typedef uint8_t	Reg;
						enum {Future = 0};
						enum {MSRBC1240D = 1};
						enum {MSSFF8020i = 2};
						enum {MSQIC157 = 3};
						enum {MSUFI = 4};
						enum {MSSFF8070i = 5};
						enum {MSSCSI = 6};
						};
					namespace bInterfaceProtocol { // Register description
						typedef uint8_t	Reg;
						enum {StandardProtocol = 0};
						enum {ControlBulkInterruptTransport = 0};
						enum {ControlBulkInterruptTransportWoCC = 1};
						enum {BulkOnlyTransport = 80};
						enum {VendorSpecific = 255};
						};
					namespace iInterface { // Register description
						typedef uint8_t	Reg;
						};
					};
				};
			};
		};
	};
#endif
