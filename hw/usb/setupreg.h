/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _hw_usb_setupregh_
#define _hw_usb_setupregh_
#include <stdint.h>
namespace Oscl { // Namespace description
	namespace Usb { // Namespace description
		namespace Hw { // Namespace description
			namespace Setup { // Namespace description
				namespace Offset { // Namespace description
					enum {bmRequestType = 0};
					enum {bRequest = 1};
					enum {wValue = 2};
					enum {wIndex = 4};
					enum {wLength = 6};
					};
				namespace bmRequestType { // Register description
					typedef uint8_t	Reg;
					enum {CLEAR_DEVICE_FEATURE = 0};
					enum {CLEAR_INTERFACE_FEATURE = 1};
					enum {CLEAR_ENDPOINT_FEATURE = 2};
					enum {GET_CONFIGURATION = 128};
					enum {GET_DESCRIPTOR = 128};
					enum {GET_INTERFACE = 129};
					enum {GET_DEVICE_STATUS = 128};
					enum {GET_INTERFACE_STATUS = 129};
					enum {GET_ENDPOINT_STATUS = 130};
					enum {SET_ADDRESS = 0};
					enum {SET_CONFIGURATION = 0};
					enum {SET_DESCRIPTOR = 0};
					enum {SET_DEVICE_FEATURE = 0};
					enum {SET_INTERFACE_FEATURE = 1};
					enum {SET_ENDPOINT_FEATURE = 2};
					enum {SET_INTERFACE = 1};
					enum {SYNC_FRAME = 130};
					namespace DIR { // Field Description
						enum {Lsb = 7};
						enum {FieldMask = 0x00000080};
						enum {Value_HostToDevice = 0x0};
						enum {ValueMask_HostToDevice = 0x00000000};
						enum {Value_DeviceToHost = 0x1};
						enum {ValueMask_DeviceToHost = 0x00000080};
						};
					namespace Type { // Field Description
						enum {Lsb = 5};
						enum {FieldMask = 0x00000060};
						enum {Value_Standard = 0x0};
						enum {ValueMask_Standard = 0x00000000};
						enum {Value_Class = 0x1};
						enum {ValueMask_Class = 0x00000020};
						enum {Value_Vendor = 0x2};
						enum {ValueMask_Vendor = 0x00000040};
						};
					namespace Recipient { // Field Description
						enum {Lsb = 0};
						enum {FieldMask = 0x0000001F};
						enum {Value_Device = 0x0};
						enum {ValueMask_Device = 0x00000000};
						enum {Value_Interface = 0x1};
						enum {ValueMask_Interface = 0x00000001};
						enum {Value_Endpoint = 0x2};
						enum {ValueMask_Endpoint = 0x00000002};
						enum {Value_Other = 0x3};
						enum {ValueMask_Other = 0x00000003};
						};
					};
				namespace bRequest { // Register description
					typedef uint8_t	Reg;
					enum {GET_STATUS = 0};
					enum {CLEAR_FEATURE = 1};
					enum {SET_FEATURE = 3};
					enum {SET_ADDRESS = 5};
					enum {GET_DESCRIPTOR = 6};
					enum {SET_DESCRIPTOR = 7};
					enum {GET_CONFIGURATION = 8};
					enum {SET_CONFIGURATION = 9};
					enum {GET_INTERFACE = 10};
					enum {SET_INTERFACE = 11};
					enum {SYNC_FRAME = 12};
					};
				namespace wValue { // Register description
					typedef uint16_t	Reg;
					namespace Desc { // Field Description
						enum {Lsb = 0};
						enum {FieldMask = 0x0000FFFF};
						namespace Index { // Field Description
							enum {Lsb = 0};
							enum {FieldMask = 0x000000FF};
							};
						namespace Type { // Field Description
							enum {Lsb = 8};
							enum {FieldMask = 0x0000FF00};
							enum {Value_DEVICE = 0x1};
							enum {ValueMask_DEVICE = 0x00000100};
							enum {Value_CONFIGURATION = 0x2};
							enum {ValueMask_CONFIGURATION = 0x00000200};
							enum {Value_STRING = 0x3};
							enum {ValueMask_STRING = 0x00000300};
							enum {Value_INTERFACE = 0x4};
							enum {ValueMask_INTERFACE = 0x00000400};
							enum {Value_ENDPOINT = 0x5};
							enum {ValueMask_ENDPOINT = 0x00000500};
							};
						};
					};
				namespace wIndex { // Register description
					typedef uint16_t	Reg;
					};
				namespace wLength { // Register description
					typedef uint16_t	Reg;
					};
				};
			};
		};
	};
#endif
