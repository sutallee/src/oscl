/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _hw_usb_serial_pl2303_interrupth_
#define _hw_usb_serial_pl2303_interrupth_
#include <stdint.h>
namespace Oscl { // Namespace description
	namespace Usb { // Namespace description
		namespace Hw { // Namespace description
			namespace Serial { // Namespace description
				namespace PL2303 { // Namespace description
					namespace Interrupt { // Namespace description
						namespace Offset { // Namespace description
							enum {unknown0 = 0};
							enum {unknown1 = 1};
							enum {unknown2 = 2};
							enum {unknown3 = 3};
							enum {unknown4 = 4};
							enum {unknown5 = 5};
							enum {unknown6 = 6};
							enum {unknown7 = 7};
							enum {change = 8};
							enum {unknown9 = 9};
							};
						namespace Change { // Register description
							typedef uint8_t	Reg;
							namespace CTS { // Field Description
								enum {Lsb = 7};
								enum {FieldMask = 0x00000080};
								enum {Value_Negated = 0x0};
								enum {ValueMask_Negated = 0x00000000};
								enum {Value_Asserted = 0x1};
								enum {ValueMask_Asserted = 0x00000080};
								};
							namespace BufferOverflow { // Field Description
								enum {Lsb = 6};
								enum {FieldMask = 0x00000040};
								enum {Value_Negated = 0x0};
								enum {ValueMask_Negated = 0x00000000};
								enum {Value_Asserted = 0x1};
								enum {ValueMask_Asserted = 0x00000040};
								};
							namespace DSR { // Field Description
								enum {Lsb = 1};
								enum {FieldMask = 0x00000002};
								enum {Value_Negated = 0x0};
								enum {ValueMask_Negated = 0x00000000};
								enum {Value_Asserted = 0x1};
								enum {ValueMask_Asserted = 0x00000002};
								};
							namespace CD { // Field Description
								enum {Lsb = 0};
								enum {FieldMask = 0x00000001};
								enum {Value_Negated = 0x0};
								enum {ValueMask_Negated = 0x00000000};
								enum {Value_Asserted = 0x1};
								enum {ValueMask_Asserted = 0x00000001};
								};
							namespace RI { // Field Description
								enum {Lsb = 3};
								enum {FieldMask = 0x00000008};
								enum {Value_Negated = 0x0};
								enum {ValueMask_Negated = 0x00000000};
								enum {Value_Asserted = 0x1};
								enum {ValueMask_Asserted = 0x00000008};
								};
							};
						};
					};
				};
			};
		};
	};
#endif
