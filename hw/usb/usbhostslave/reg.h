#ifndef _oscl_hw_usb_usbhostslave_regh_
#define _oscl_hw_usb_usbhostslave_regh_
#include <stdint.h>
namespace Oscl { // Namespace description
	namespace Hw { // Namespace description
		namespace Usb { // Namespace description
			namespace UsbHostSlave { // Namespace description
				namespace HOST_SLAVE_CONTROL { // Register description
					typedef uint8_t	Reg;
					namespace HOST_MODE { // Field Description
						enum {Lsb = 0};
						enum {FieldMask = 0x00000001};
						enum {Value_HostMode = 0x1};
						enum {ValueMask_HostMode = 0x00000001};
						enum {Value_SlaveMode = 0x0};
						enum {ValueMask_SlaveMode = 0x00000000};
						};
					namespace ResetCore { // Field Description
						enum {Lsb = 1};
						enum {FieldMask = 0x00000002};
						enum {Value_Reset = 0x1};
						enum {ValueMask_Reset = 0x00000002};
						};
					};
				namespace HOST_SLAVE_VERSION { // Register description
					typedef uint8_t	Reg;
					namespace Minor { // Field Description
						enum {Lsb = 0};
						enum {FieldMask = 0x0000000F};
						};
					namespace Major { // Field Description
						enum {Lsb = 4};
						enum {FieldMask = 0x000000F0};
						};
					};
				namespace HOST_TX_CONTROL { // Register description
					typedef uint8_t	Reg;
					namespace TRANS_REQ { // Field Description
						enum {Lsb = 0};
						enum {FieldMask = 0x00000001};
						enum {Value_Disable = 0x0};
						enum {ValueMask_Disable = 0x00000000};
						enum {Value_Enable = 0x1};
						enum {ValueMask_Enable = 0x00000001};
						enum {Value_Disabled = 0x0};
						enum {ValueMask_Disabled = 0x00000000};
						enum {Value_Enabled = 0x1};
						enum {ValueMask_Enabled = 0x00000001};
						};
					namespace SOF_SYNC { // Field Description
						enum {Lsb = 1};
						enum {FieldMask = 0x00000002};
						enum {Value_SyncTransWithSOF = 0x1};
						enum {ValueMask_SyncTransWithSOF = 0x00000002};
						enum {Value_DontSyncTransWithSOF = 0x0};
						enum {ValueMask_DontSyncTransWithSOF = 0x00000000};
						};
					namespace PREAMBLE_ENABLE { // Field Description
						enum {Lsb = 2};
						enum {FieldMask = 0x00000004};
						enum {Value_EnablePreamble = 0x1};
						enum {ValueMask_EnablePreamble = 0x00000004};
						enum {Value_DisablePreamble = 0x0};
						enum {ValueMask_DisablePreamble = 0x00000000};
						};
					namespace ISO_ENABLE { // Field Description
						enum {Lsb = 3};
						enum {FieldMask = 0x00000008};
						enum {Value_IsochronousEnable = 0x1};
						enum {ValueMask_IsochronousEnable = 0x00000008};
						enum {Value_IsochronousDisable = 0x0};
						enum {ValueMask_IsochronousDisable = 0x00000000};
						};
					};
				namespace HOST_TX_TRANS_TYPE { // Register description
					typedef uint8_t	Reg;
					namespace TRANSACTION_TYPE { // Field Description
						enum {Lsb = 0};
						enum {FieldMask = 0x00000003};
						enum {Value_SETUP_TRANS = 0x0};
						enum {ValueMask_SETUP_TRANS = 0x00000000};
						enum {Value_IN_TRANS = 0x1};
						enum {ValueMask_IN_TRANS = 0x00000001};
						enum {Value_OUTDATA0_TRANS = 0x2};
						enum {ValueMask_OUTDATA0_TRANS = 0x00000002};
						enum {Value_OUTDATA1_TRANS = 0x3};
						enum {ValueMask_OUTDATA1_TRANS = 0x00000003};
						};
					};
				namespace HOST_TX_LINE_CONTROL { // Register description
					typedef uint8_t	Reg;
					namespace TX_LINE_STATE { // Field Description
						enum {Lsb = 0};
						enum {FieldMask = 0x00000003};
						namespace DMINUS { // Field Description
							enum {Lsb = 0};
							enum {FieldMask = 0x00000001};
							enum {Value_High = 0x1};
							enum {ValueMask_High = 0x00000001};
							enum {Value_Low = 0x0};
							enum {ValueMask_Low = 0x00000000};
							};
						namespace DPLUS { // Field Description
							enum {Lsb = 1};
							enum {FieldMask = 0x00000002};
							enum {Value_High = 0x1};
							enum {ValueMask_High = 0x00000002};
							enum {Value_Low = 0x0};
							enum {ValueMask_Low = 0x00000000};
							};
						};
					namespace DIRECT_CONTROL { // Field Description
						enum {Lsb = 2};
						enum {FieldMask = 0x00000004};
						enum {Value_Normal = 0x0};
						enum {ValueMask_Normal = 0x00000000};
						enum {Value_EnableDirectControl = 0x1};
						enum {ValueMask_EnableDirectControl = 0x00000004};
						};
					namespace FULL_SPEED_LINE_POLARITY { // Field Description
						enum {Lsb = 3};
						enum {FieldMask = 0x00000008};
						enum {Value_FullSpeedLinePolarity = 0x1};
						enum {ValueMask_FullSpeedLinePolarity = 0x00000008};
						enum {Value_LowSpeedLinePolarity = 0x0};
						enum {ValueMask_LowSpeedLinePolarity = 0x00000000};
						};
					namespace FULL_SPEED_LINE_RATE { // Field Description
						enum {Lsb = 4};
						enum {FieldMask = 0x00000010};
						enum {Value_FullSpeedLineRate = 0x1};
						enum {ValueMask_FullSpeedLineRate = 0x00000010};
						enum {Value_LowSpeedLineRate = 0x0};
						enum {ValueMask_LowSpeedLineRate = 0x00000000};
						};
					};
				namespace HOST_TX_SOF_ENABLE { // Register description
					typedef uint8_t	Reg;
					namespace SOF_EN { // Field Description
						enum {Lsb = 0};
						enum {FieldMask = 0x00000001};
						enum {Value_EnableAutoSofTransmit = 0x1};
						enum {ValueMask_EnableAutoSofTransmit = 0x00000001};
						enum {Value_DisableAutoSofTransmit = 0x0};
						enum {ValueMask_DisableAutoSofTransmit = 0x00000000};
						};
					};
				namespace HOST_TX_ADDR { // Register description
					typedef uint8_t	Reg;
					namespace DeviceAddress { // Field Description
						enum {Lsb = 0};
						enum {FieldMask = 0x0000007F};
						};
					};
				namespace HOST_TX_ENDP { // Register description
					typedef uint8_t	Reg;
					namespace EndpointAddress { // Field Description
						enum {Lsb = 0};
						enum {FieldMask = 0x0000000F};
						};
					};
				namespace HOST_FRAME_NUM_MSP { // Register description
					typedef uint8_t	Reg;
					namespace FRAME_NUM_MSP { // Field Description
						enum {Lsb = 0};
						enum {FieldMask = 0x00000007};
						};
					};
				namespace HOST_FRAME_NUM_LSP { // Register description
					typedef uint8_t	Reg;
					namespace FRAME_NUM_LSP { // Field Description
						enum {Lsb = 0};
						enum {FieldMask = 0x0000007F};
						};
					};
				namespace HOST_INTERRUPT_STATUS { // Register description
					typedef uint8_t	Reg;
					namespace TRANS_DONE { // Field Description
						enum {Lsb = 0};
						enum {FieldMask = 0x00000001};
						enum {Value_TransactionComplete = 0x1};
						enum {ValueMask_TransactionComplete = 0x00000001};
						enum {Value_TransactionIncomplete = 0x0};
						enum {ValueMask_TransactionIncomplete = 0x00000000};
						enum {Value_Clear = 0x1};
						enum {ValueMask_Clear = 0x00000001};
						};
					namespace RESUME_INT { // Field Description
						enum {Lsb = 1};
						enum {FieldMask = 0x00000002};
						enum {Value_ResumeStateDetected = 0x1};
						enum {ValueMask_ResumeStateDetected = 0x00000002};
						enum {Value_Clear = 0x1};
						enum {ValueMask_Clear = 0x00000002};
						};
					namespace CONNECTION_EVENT { // Field Description
						enum {Lsb = 2};
						enum {FieldMask = 0x00000004};
						enum {Value_ConnectStateChanged = 0x1};
						enum {ValueMask_ConnectStateChanged = 0x00000004};
						enum {Value_ConnectStateUnchanged = 0x0};
						enum {ValueMask_ConnectStateUnchanged = 0x00000000};
						enum {Value_Clear = 0x1};
						enum {ValueMask_Clear = 0x00000004};
						};
					namespace SOF_SENT { // Field Description
						enum {Lsb = 3};
						enum {FieldMask = 0x00000008};
						enum {Value_SofSent = 0x1};
						enum {ValueMask_SofSent = 0x00000008};
						enum {Value_Clear = 0x1};
						enum {ValueMask_Clear = 0x00000008};
						};
					};
				namespace HOST_INTERRUPT_MASK { // Register description
					typedef uint8_t	Reg;
					namespace TRANS_DONE { // Field Description
						enum {Lsb = 0};
						enum {FieldMask = 0x00000001};
						enum {Value_Enable = 0x1};
						enum {ValueMask_Enable = 0x00000001};
						enum {Value_Disable = 0x0};
						enum {ValueMask_Disable = 0x00000000};
						enum {Value_Enabled = 0x1};
						enum {ValueMask_Enabled = 0x00000001};
						enum {Value_Disabled = 0x0};
						enum {ValueMask_Disabled = 0x00000000};
						};
					namespace RESUME_INT { // Field Description
						enum {Lsb = 1};
						enum {FieldMask = 0x00000002};
						enum {Value_Enable = 0x1};
						enum {ValueMask_Enable = 0x00000002};
						enum {Value_Disable = 0x0};
						enum {ValueMask_Disable = 0x00000000};
						enum {Value_Enabled = 0x1};
						enum {ValueMask_Enabled = 0x00000002};
						enum {Value_Disabled = 0x0};
						enum {ValueMask_Disabled = 0x00000000};
						};
					namespace CONNECTION_EVENT { // Field Description
						enum {Lsb = 2};
						enum {FieldMask = 0x00000004};
						enum {Value_Enable = 0x1};
						enum {ValueMask_Enable = 0x00000004};
						enum {Value_Disable = 0x0};
						enum {ValueMask_Disable = 0x00000000};
						enum {Value_Enabled = 0x1};
						enum {ValueMask_Enabled = 0x00000004};
						enum {Value_Disabled = 0x0};
						enum {ValueMask_Disabled = 0x00000000};
						};
					namespace SOF_SENT { // Field Description
						enum {Lsb = 3};
						enum {FieldMask = 0x00000008};
						enum {Value_Enable = 0x1};
						enum {ValueMask_Enable = 0x00000008};
						enum {Value_Disable = 0x0};
						enum {ValueMask_Disable = 0x00000000};
						enum {Value_Enabled = 0x1};
						enum {ValueMask_Enabled = 0x00000008};
						enum {Value_Disabled = 0x0};
						enum {ValueMask_Disabled = 0x00000000};
						};
					};
				namespace HOST_RX_STATUS { // Register description
					typedef uint8_t	Reg;
					namespace CRC_ERROR { // Field Description
						enum {Lsb = 0};
						enum {FieldMask = 0x00000001};
						enum {Value_Error = 0x1};
						enum {ValueMask_Error = 0x00000001};
						enum {Value_NoError = 0x0};
						enum {ValueMask_NoError = 0x00000000};
						};
					namespace BIT_STUFF_ERROR { // Field Description
						enum {Lsb = 1};
						enum {FieldMask = 0x00000002};
						enum {Value_Error = 0x1};
						enum {ValueMask_Error = 0x00000002};
						enum {Value_NoError = 0x0};
						enum {ValueMask_NoError = 0x00000000};
						};
					namespace RX_OVERFLOW { // Field Description
						enum {Lsb = 2};
						enum {FieldMask = 0x00000004};
						enum {Value_Error = 0x1};
						enum {ValueMask_Error = 0x00000004};
						enum {Value_NoError = 0x0};
						enum {ValueMask_NoError = 0x00000000};
						};
					namespace RX_TIME_OUT { // Field Description
						enum {Lsb = 3};
						enum {FieldMask = 0x00000008};
						enum {Value_Timeout = 0x1};
						enum {ValueMask_Timeout = 0x00000008};
						enum {Value_NoTimeout = 0x0};
						enum {ValueMask_NoTimeout = 0x00000000};
						};
					namespace NAK_RXED { // Field Description
						enum {Lsb = 4};
						enum {FieldMask = 0x00000010};
						enum {Value_ReceivedNAK = 0x1};
						enum {ValueMask_ReceivedNAK = 0x00000010};
						enum {Value_NoNakReceived = 0x0};
						enum {ValueMask_NoNakReceived = 0x00000000};
						};
					namespace STALL_RXED { // Field Description
						enum {Lsb = 5};
						enum {FieldMask = 0x00000020};
						enum {Value_ReceivedSTALL = 0x1};
						enum {ValueMask_ReceivedSTALL = 0x00000020};
						enum {Value_NoStallReceived = 0x0};
						enum {ValueMask_NoStallReceived = 0x00000000};
						};
					namespace ACK_RXED { // Field Description
						enum {Lsb = 6};
						enum {FieldMask = 0x00000040};
						enum {Value_ReceivedACK = 0x1};
						enum {ValueMask_ReceivedACK = 0x00000040};
						enum {Value_NoAckReceived = 0x0};
						enum {ValueMask_NoAckReceived = 0x00000000};
						};
					namespace DATA_SEQUENCE_BIT { // Field Description
						enum {Lsb = 7};
						enum {FieldMask = 0x00000080};
						enum {Value_DATA0 = 0x0};
						enum {ValueMask_DATA0 = 0x00000000};
						enum {Value_DATA1 = 0x1};
						enum {ValueMask_DATA1 = 0x00000080};
						};
					};
				namespace HOST_RX_PID { // Register description
					typedef uint8_t	Reg;
					namespace RECEIVE_PID { // Field Description
						enum {Lsb = 0};
						enum {FieldMask = 0x0000000F};
						enum {Value_OUT = 0x1};
						enum {ValueMask_OUT = 0x00000001};
						enum {Value_IN = 0x9};
						enum {ValueMask_IN = 0x00000009};
						enum {Value_SOF = 0x5};
						enum {ValueMask_SOF = 0x00000005};
						enum {Value_SETUP = 0xD};
						enum {ValueMask_SETUP = 0x0000000D};
						enum {Value_DATA0 = 0x3};
						enum {ValueMask_DATA0 = 0x00000003};
						enum {Value_DATA1 = 0xB};
						enum {ValueMask_DATA1 = 0x0000000B};
						enum {Value_ACK = 0x2};
						enum {ValueMask_ACK = 0x00000002};
						enum {Value_NAK = 0xA};
						enum {ValueMask_NAK = 0x0000000A};
						enum {Value_STALL = 0xE};
						enum {ValueMask_STALL = 0x0000000E};
						enum {Value_PRE = 0xC};
						enum {ValueMask_PRE = 0x0000000C};
						namespace CodingGroup { // Field Description
							enum {Lsb = 0};
							enum {FieldMask = 0x00000003};
							enum {Value_Token = 0x1};
							enum {ValueMask_Token = 0x00000001};
							enum {Value_Data = 0x3};
							enum {ValueMask_Data = 0x00000003};
							enum {Value_Handshake = 0x2};
							enum {ValueMask_Handshake = 0x00000002};
							enum {Value_Special = 0x0};
							enum {ValueMask_Special = 0x00000000};
							};
						};
					};
				namespace HOST_RX_ADDR { // Register description
					typedef uint8_t	Reg;
					namespace RECEIVE_ADDRESS { // Field Description
						enum {Lsb = 0};
						enum {FieldMask = 0x0000007F};
						};
					};
				namespace HOST_RX_ENDP { // Register description
					typedef uint8_t	Reg;
					namespace RECEIVE_ENDP { // Field Description
						enum {Lsb = 0};
						enum {FieldMask = 0x0000000F};
						};
					};
				namespace HOST_RX_CONNECT_STATE { // Register description
					typedef uint8_t	Reg;
					namespace RX_LINE_STATE { // Field Description
						enum {Lsb = 0};
						enum {FieldMask = 0x00000003};
						enum {Value_Disconnect = 0x0};
						enum {ValueMask_Disconnect = 0x00000000};
						enum {Value_LowSpeedConnect = 0x1};
						enum {ValueMask_LowSpeedConnect = 0x00000001};
						enum {Value_FullSpeedConnect = 0x2};
						enum {ValueMask_FullSpeedConnect = 0x00000002};
						};
					};
				namespace HOST_SOF_TIMER_MSB { // Register description
					typedef uint8_t	Reg;
					};
				namespace HOST_RX_FIFO_DATA { // Register description
					typedef uint8_t	Reg;
					};
				namespace HOST_RX_FIFO_COUNT_MSB { // Register description
					typedef uint8_t	Reg;
					};
				namespace HOST_RX_FIFO_COUNT_LSB { // Register description
					typedef uint8_t	Reg;
					};
				namespace HOST_RX_FIFO_CONTROL { // Register description
					typedef uint8_t	Reg;
					namespace FIFO_FORCE_EMPTY { // Field Description
						enum {Lsb = 0};
						enum {FieldMask = 0x00000001};
						enum {Value_DeleteAllData = 0x1};
						enum {ValueMask_DeleteAllData = 0x00000001};
						enum {Value_NoAction = 0x0};
						enum {ValueMask_NoAction = 0x00000000};
						};
					};
				namespace HOST_TX_FIFO_DATA { // Register description
					typedef uint8_t	Reg;
					};
				namespace HOST_TX_FIFO_CONTROL { // Register description
					typedef uint8_t	Reg;
					namespace FIFO_FORCE_EMPTY { // Field Description
						enum {Lsb = 0};
						enum {FieldMask = 0x00000001};
						enum {Value_DeleteAllData = 0x1};
						enum {ValueMask_DeleteAllData = 0x00000001};
						enum {Value_NoAction = 0x0};
						enum {ValueMask_NoAction = 0x00000000};
						};
					};
				};
			};
		};
	};
#endif
