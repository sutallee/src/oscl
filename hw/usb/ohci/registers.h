/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_hw_usb_ohci_registers_
#define _oscl_hw_usb_ohci_registers_
#include <stdint.h>
namespace Oscl { // Namespace description
	namespace Usb { // Namespace description
		namespace Ohci { // Namespace description
			namespace Revision { // Register description
				typedef uint32_t	Reg;
				namespace REV { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x000000FF};
					};
				};
			namespace Control { // Register description
				typedef uint32_t	Reg;
				namespace CBSR { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x00000003};
					enum {Value_OneToOne = 0x0};
					enum {ValueMask_OneToOne = 0x00000000};
					enum {Value_TwoToOne = 0x1};
					enum {ValueMask_TwoToOne = 0x00000001};
					enum {Value_ThreeToOne = 0x2};
					enum {ValueMask_ThreeToOne = 0x00000002};
					enum {Value_FourToOne = 0x3};
					enum {ValueMask_FourToOne = 0x00000003};
					};
				namespace PLE { // Field Description
					enum {Lsb = 2};
					enum {FieldMask = 0x00000004};
					enum {Value_EnablePeriodicListProcessing = 0x1};
					enum {ValueMask_EnablePeriodicListProcessing = 0x00000004};
					enum {Value_DisablePeriodicListProcessing = 0x0};
					enum {ValueMask_DisablePeriodicListProcessing = 0x00000000};
					};
				namespace IE { // Field Description
					enum {Lsb = 3};
					enum {FieldMask = 0x00000008};
					enum {Value_EnableIsochronousListProcessing = 0x1};
					enum {ValueMask_EnableIsochronousListProcessing = 0x00000008};
					enum {Value_DisableIsochronousListProcessing = 0x0};
					enum {ValueMask_DisableIsochronousListProcessing = 0x00000000};
					};
				namespace CLE { // Field Description
					enum {Lsb = 4};
					enum {FieldMask = 0x00000010};
					enum {Value_EnableControlListProcessing = 0x1};
					enum {ValueMask_EnableControlListProcessing = 0x00000010};
					enum {Value_DisableControlListProcessing = 0x0};
					enum {ValueMask_DisableControlListProcessing = 0x00000000};
					};
				namespace BLE { // Field Description
					enum {Lsb = 5};
					enum {FieldMask = 0x00000020};
					enum {Value_EnableBulkListProcessing = 0x1};
					enum {ValueMask_EnableBulkListProcessing = 0x00000020};
					enum {Value_DisableBulkListProcessing = 0x0};
					enum {ValueMask_DisableBulkListProcessing = 0x00000000};
					};
				namespace HCFS { // Field Description
					enum {Lsb = 6};
					enum {FieldMask = 0x000000C0};
					enum {Value_Reset = 0x0};
					enum {ValueMask_Reset = 0x00000000};
					enum {Value_Resume = 0x1};
					enum {ValueMask_Resume = 0x00000040};
					enum {Value_Operational = 0x2};
					enum {ValueMask_Operational = 0x00000080};
					enum {Value_Suspend = 0x3};
					enum {ValueMask_Suspend = 0x000000C0};
					};
				namespace IR { // Field Description
					enum {Lsb = 8};
					enum {FieldMask = 0x00000100};
					enum {Value_RouteIrqToHost = 0x0};
					enum {ValueMask_RouteIrqToHost = 0x00000000};
					enum {Value_RouteIrqToSystemManagement = 0x1};
					enum {ValueMask_RouteIrqToSystemManagement = 0x00000100};
					};
				namespace RWC { // Field Description
					enum {Lsb = 9};
					enum {FieldMask = 0x00000200};
					enum {Value_ConnectRemoteWakeup = 0x1};
					enum {ValueMask_ConnectRemoteWakeup = 0x00000200};
					enum {Value_DisconnectRemoteWakeup = 0x0};
					enum {ValueMask_DisconnectRemoteWakeup = 0x00000000};
					};
				namespace RWE { // Field Description
					enum {Lsb = 10};
					enum {FieldMask = 0x00000400};
					enum {Value_EnableRemoteWakeup = 0x1};
					enum {ValueMask_EnableRemoteWakeup = 0x00000400};
					enum {Value_DisableRemoteWakeup = 0x0};
					enum {ValueMask_DisableRemoteWakeup = 0x00000000};
					};
				};
			namespace CommandStatus { // Register description
				typedef uint32_t	Reg;
				namespace HCR { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x00000001};
					enum {Value_ResetCommand = 0x1};
					enum {ValueMask_ResetCommand = 0x00000001};
					enum {Value_ResetInProgress = 0x1};
					enum {ValueMask_ResetInProgress = 0x00000001};
					enum {Value_ResetComplete = 0x0};
					enum {ValueMask_ResetComplete = 0x00000000};
					};
				namespace CLF { // Field Description
					enum {Lsb = 1};
					enum {FieldMask = 0x00000002};
					enum {Value_ProcessControlList = 0x1};
					enum {ValueMask_ProcessControlList = 0x00000002};
					enum {Value_ProcessingControlList = 0x1};
					enum {ValueMask_ProcessingControlList = 0x00000002};
					enum {Value_NotProcessingControlList = 0x0};
					enum {ValueMask_NotProcessingControlList = 0x00000000};
					};
				namespace BLF { // Field Description
					enum {Lsb = 2};
					enum {FieldMask = 0x00000004};
					enum {Value_ProcessBulkList = 0x1};
					enum {ValueMask_ProcessBulkList = 0x00000004};
					enum {Value_ProcessingBulkList = 0x1};
					enum {ValueMask_ProcessingBulkList = 0x00000004};
					enum {Value_NotProcessingBulkList = 0x0};
					enum {ValueMask_NotProcessingBulkList = 0x00000000};
					};
				namespace OCR { // Field Description
					enum {Lsb = 3};
					enum {FieldMask = 0x00000008};
					enum {Value_RequestOwnershipChange = 0x1};
					enum {ValueMask_RequestOwnershipChange = 0x00000008};
					enum {Value_RequestingOwnershipChange = 0x1};
					enum {ValueMask_RequestingOwnershipChange = 0x00000008};
					enum {Value_OwnershipChanged = 0x0};
					enum {ValueMask_OwnershipChanged = 0x00000000};
					};
				namespace SOC { // Field Description
					enum {Lsb = 16};
					enum {FieldMask = 0x00030000};
					enum {Value_MaxValue = 0x3};
					enum {ValueMask_MaxValue = 0x00030000};
					};
				};
			namespace InterruptStatus { // Register description
				typedef uint32_t	Reg;
				namespace SO { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x00000001};
					enum {Value_Pending = 0x1};
					enum {ValueMask_Pending = 0x00000001};
					enum {Value_NotPending = 0x0};
					enum {ValueMask_NotPending = 0x00000000};
					enum {Value_Clear = 0x1};
					enum {ValueMask_Clear = 0x00000001};
					};
				namespace WDH { // Field Description
					enum {Lsb = 1};
					enum {FieldMask = 0x00000002};
					enum {Value_Pending = 0x1};
					enum {ValueMask_Pending = 0x00000002};
					enum {Value_NotPending = 0x0};
					enum {ValueMask_NotPending = 0x00000000};
					enum {Value_Clear = 0x1};
					enum {ValueMask_Clear = 0x00000002};
					};
				namespace SF { // Field Description
					enum {Lsb = 2};
					enum {FieldMask = 0x00000004};
					enum {Value_Pending = 0x1};
					enum {ValueMask_Pending = 0x00000004};
					enum {Value_NotPending = 0x0};
					enum {ValueMask_NotPending = 0x00000000};
					enum {Value_Clear = 0x1};
					enum {ValueMask_Clear = 0x00000004};
					};
				namespace RD { // Field Description
					enum {Lsb = 3};
					enum {FieldMask = 0x00000008};
					enum {Value_Pending = 0x1};
					enum {ValueMask_Pending = 0x00000008};
					enum {Value_NotPending = 0x0};
					enum {ValueMask_NotPending = 0x00000000};
					enum {Value_Clear = 0x1};
					enum {ValueMask_Clear = 0x00000008};
					};
				namespace UE { // Field Description
					enum {Lsb = 4};
					enum {FieldMask = 0x00000010};
					enum {Value_Pending = 0x1};
					enum {ValueMask_Pending = 0x00000010};
					enum {Value_NotPending = 0x0};
					enum {ValueMask_NotPending = 0x00000000};
					enum {Value_Clear = 0x1};
					enum {ValueMask_Clear = 0x00000010};
					};
				namespace FNO { // Field Description
					enum {Lsb = 5};
					enum {FieldMask = 0x00000020};
					enum {Value_Pending = 0x1};
					enum {ValueMask_Pending = 0x00000020};
					enum {Value_NotPending = 0x0};
					enum {ValueMask_NotPending = 0x00000000};
					enum {Value_Clear = 0x1};
					enum {ValueMask_Clear = 0x00000020};
					};
				namespace RHSC { // Field Description
					enum {Lsb = 6};
					enum {FieldMask = 0x00000040};
					enum {Value_Pending = 0x1};
					enum {ValueMask_Pending = 0x00000040};
					enum {Value_NotPending = 0x0};
					enum {ValueMask_NotPending = 0x00000000};
					enum {Value_Clear = 0x1};
					enum {ValueMask_Clear = 0x00000040};
					};
				namespace OC { // Field Description
					enum {Lsb = 30};
					enum {FieldMask = 0x40000000};
					enum {Value_Pending = 0x1};
					enum {ValueMask_Pending = 0x40000000};
					enum {Value_NotPending = 0x0};
					enum {ValueMask_NotPending = 0x00000000};
					enum {Value_Clear = 0x1};
					enum {ValueMask_Clear = 0x40000000};
					};
				};
			namespace InterruptEnable { // Register description
				typedef uint32_t	Reg;
				namespace SO { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x00000001};
					enum {Value_Enable = 0x1};
					enum {ValueMask_Enable = 0x00000001};
					enum {Value_Enabled = 0x1};
					enum {ValueMask_Enabled = 0x00000001};
					enum {Value_Disabled = 0x0};
					enum {ValueMask_Disabled = 0x00000000};
					};
				namespace WDH { // Field Description
					enum {Lsb = 1};
					enum {FieldMask = 0x00000002};
					enum {Value_Enable = 0x1};
					enum {ValueMask_Enable = 0x00000002};
					enum {Value_Enabled = 0x1};
					enum {ValueMask_Enabled = 0x00000002};
					enum {Value_Disabled = 0x0};
					enum {ValueMask_Disabled = 0x00000000};
					};
				namespace SF { // Field Description
					enum {Lsb = 2};
					enum {FieldMask = 0x00000004};
					enum {Value_Enable = 0x1};
					enum {ValueMask_Enable = 0x00000004};
					enum {Value_Enabled = 0x1};
					enum {ValueMask_Enabled = 0x00000004};
					enum {Value_Disabled = 0x0};
					enum {ValueMask_Disabled = 0x00000000};
					};
				namespace RD { // Field Description
					enum {Lsb = 3};
					enum {FieldMask = 0x00000008};
					enum {Value_Enable = 0x1};
					enum {ValueMask_Enable = 0x00000008};
					enum {Value_Enabled = 0x1};
					enum {ValueMask_Enabled = 0x00000008};
					enum {Value_Disabled = 0x0};
					enum {ValueMask_Disabled = 0x00000000};
					};
				namespace UE { // Field Description
					enum {Lsb = 4};
					enum {FieldMask = 0x00000010};
					enum {Value_Enable = 0x1};
					enum {ValueMask_Enable = 0x00000010};
					enum {Value_Enabled = 0x1};
					enum {ValueMask_Enabled = 0x00000010};
					enum {Value_Disabled = 0x0};
					enum {ValueMask_Disabled = 0x00000000};
					};
				namespace FNO { // Field Description
					enum {Lsb = 5};
					enum {FieldMask = 0x00000020};
					enum {Value_Enable = 0x1};
					enum {ValueMask_Enable = 0x00000020};
					enum {Value_Enabled = 0x1};
					enum {ValueMask_Enabled = 0x00000020};
					enum {Value_Disabled = 0x0};
					enum {ValueMask_Disabled = 0x00000000};
					};
				namespace RHSC { // Field Description
					enum {Lsb = 6};
					enum {FieldMask = 0x00000040};
					enum {Value_Enable = 0x1};
					enum {ValueMask_Enable = 0x00000040};
					enum {Value_Enabled = 0x1};
					enum {ValueMask_Enabled = 0x00000040};
					enum {Value_Disabled = 0x0};
					enum {ValueMask_Disabled = 0x00000000};
					};
				namespace OC { // Field Description
					enum {Lsb = 30};
					enum {FieldMask = 0x40000000};
					enum {Value_Enable = 0x1};
					enum {ValueMask_Enable = 0x40000000};
					enum {Value_Enabled = 0x1};
					enum {ValueMask_Enabled = 0x40000000};
					enum {Value_Disabled = 0x0};
					enum {ValueMask_Disabled = 0x00000000};
					};
				namespace MIE { // Field Description
					enum {Lsb = 31};
					enum {FieldMask = 0x80000000};
					enum {Value_Enable = 0x1};
					enum {ValueMask_Enable = 0x80000000};
					enum {Value_Enabled = 0x1};
					enum {ValueMask_Enabled = 0x80000000};
					enum {Value_Disabled = 0x0};
					enum {ValueMask_Disabled = 0x00000000};
					};
				};
			namespace InterruptDisable { // Register description
				typedef uint32_t	Reg;
				namespace SO { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x00000001};
					enum {Value_Disable = 0x1};
					enum {ValueMask_Disable = 0x00000001};
					enum {Value_Enabled = 0x1};
					enum {ValueMask_Enabled = 0x00000001};
					enum {Value_Disabled = 0x0};
					enum {ValueMask_Disabled = 0x00000000};
					};
				namespace WDH { // Field Description
					enum {Lsb = 1};
					enum {FieldMask = 0x00000002};
					enum {Value_Disable = 0x1};
					enum {ValueMask_Disable = 0x00000002};
					enum {Value_Enabled = 0x1};
					enum {ValueMask_Enabled = 0x00000002};
					enum {Value_Disabled = 0x0};
					enum {ValueMask_Disabled = 0x00000000};
					};
				namespace SF { // Field Description
					enum {Lsb = 2};
					enum {FieldMask = 0x00000004};
					enum {Value_Disable = 0x1};
					enum {ValueMask_Disable = 0x00000004};
					enum {Value_Enabled = 0x1};
					enum {ValueMask_Enabled = 0x00000004};
					enum {Value_Disabled = 0x0};
					enum {ValueMask_Disabled = 0x00000000};
					};
				namespace RD { // Field Description
					enum {Lsb = 3};
					enum {FieldMask = 0x00000008};
					enum {Value_Disable = 0x1};
					enum {ValueMask_Disable = 0x00000008};
					enum {Value_Enabled = 0x1};
					enum {ValueMask_Enabled = 0x00000008};
					enum {Value_Disabled = 0x0};
					enum {ValueMask_Disabled = 0x00000000};
					};
				namespace UE { // Field Description
					enum {Lsb = 4};
					enum {FieldMask = 0x00000010};
					enum {Value_Disable = 0x1};
					enum {ValueMask_Disable = 0x00000010};
					enum {Value_Enabled = 0x1};
					enum {ValueMask_Enabled = 0x00000010};
					enum {Value_Disabled = 0x0};
					enum {ValueMask_Disabled = 0x00000000};
					};
				namespace FNO { // Field Description
					enum {Lsb = 5};
					enum {FieldMask = 0x00000020};
					enum {Value_Disable = 0x1};
					enum {ValueMask_Disable = 0x00000020};
					enum {Value_Enabled = 0x1};
					enum {ValueMask_Enabled = 0x00000020};
					enum {Value_Disabled = 0x0};
					enum {ValueMask_Disabled = 0x00000000};
					};
				namespace RHSC { // Field Description
					enum {Lsb = 6};
					enum {FieldMask = 0x00000040};
					enum {Value_Disable = 0x1};
					enum {ValueMask_Disable = 0x00000040};
					enum {Value_Enabled = 0x1};
					enum {ValueMask_Enabled = 0x00000040};
					enum {Value_Disabled = 0x0};
					enum {ValueMask_Disabled = 0x00000000};
					};
				namespace OC { // Field Description
					enum {Lsb = 30};
					enum {FieldMask = 0x40000000};
					enum {Value_Disable = 0x1};
					enum {ValueMask_Disable = 0x40000000};
					enum {Value_Enabled = 0x1};
					enum {ValueMask_Enabled = 0x40000000};
					enum {Value_Disabled = 0x0};
					enum {ValueMask_Disabled = 0x00000000};
					};
				namespace MIE { // Field Description
					enum {Lsb = 31};
					enum {FieldMask = 0x80000000};
					enum {Value_Disable = 0x1};
					enum {ValueMask_Disable = 0x80000000};
					enum {Value_Enabled = 0x1};
					enum {ValueMask_Enabled = 0x80000000};
					enum {Value_Disabled = 0x0};
					enum {ValueMask_Disabled = 0x00000000};
					};
				};
			namespace HCCA { // Register description
				typedef uint32_t	Reg;
				};
			namespace PeriodCurrentED { // Register description
				typedef uint32_t	Reg;
				namespace PCED { // Field Description
					enum {Lsb = 4};
					enum {FieldMask = 0xFFFFFFF0};
					};
				};
			namespace ControlHeadED { // Register description
				typedef uint32_t	Reg;
				namespace CHED { // Field Description
					enum {Lsb = 4};
					enum {FieldMask = 0xFFFFFFF0};
					};
				};
			namespace ControlCurrentED { // Register description
				typedef uint32_t	Reg;
				namespace CCED { // Field Description
					enum {Lsb = 4};
					enum {FieldMask = 0xFFFFFFF0};
					};
				};
			namespace BulkHeadED { // Register description
				typedef uint32_t	Reg;
				namespace BHED { // Field Description
					enum {Lsb = 4};
					enum {FieldMask = 0xFFFFFFF0};
					};
				};
			namespace BulkCurrentED { // Register description
				typedef uint32_t	Reg;
				namespace BCED { // Field Description
					enum {Lsb = 4};
					enum {FieldMask = 0xFFFFFFF0};
					};
				};
			namespace DoneHead { // Register description
				typedef uint32_t	Reg;
				namespace DH { // Field Description
					enum {Lsb = 4};
					enum {FieldMask = 0xFFFFFFF0};
					};
				};
			namespace FmInterval { // Register description
				typedef uint32_t	Reg;
				namespace FI { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x00003FFF};
					enum {Value_Nominal = 0x2EDF};
					enum {ValueMask_Nominal = 0x00002EDF};
					};
				namespace FSMPS { // Field Description
					enum {Lsb = 16};
					enum {FieldMask = 0x7FFF0000};
					enum {Value_Maximum = 0x3FFF};
					enum {ValueMask_Maximum = 0x3FFF0000};
					};
				namespace FIT { // Field Description
					enum {Lsb = 31};
					enum {FieldMask = 0x80000000};
					};
				};
			namespace FmRemaining { // Register description
				typedef uint32_t	Reg;
				namespace FR { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x00003FFF};
					};
				namespace FRT { // Field Description
					enum {Lsb = 31};
					enum {FieldMask = 0x80000000};
					};
				};
			namespace FmNumber { // Register description
				typedef uint32_t	Reg;
				namespace FN { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x0000FFFF};
					};
				};
			namespace PeriodicStart { // Register description
				typedef uint32_t	Reg;
				namespace PS { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x00003FFF};
					};
				};
			namespace LSThreshold { // Register description
				typedef uint32_t	Reg;
				namespace LST { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x00000FFF};
					};
				};
			namespace RhDescriptorA { // Register description
				typedef uint32_t	Reg;
				namespace NDP { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x000000FF};
					enum {Value_Minimum = 0x1};
					enum {ValueMask_Minimum = 0x00000001};
					enum {Value_Maximum = 0xF};
					enum {ValueMask_Maximum = 0x0000000F};
					};
				namespace PSM { // Field Description
					enum {Lsb = 8};
					enum {FieldMask = 0x00000100};
					enum {Value_AllPortsArePoweredAtTheSameTime = 0x0};
					enum {ValueMask_AllPortsArePoweredAtTheSameTime = 0x00000000};
					enum {Value_PortsAreIndividuallyPowered = 0x1};
					enum {ValueMask_PortsAreIndividuallyPowered = 0x00000100};
					};
				namespace NPS { // Field Description
					enum {Lsb = 9};
					enum {FieldMask = 0x00000200};
					enum {Value_PortsArePowerSwitched = 0x0};
					enum {ValueMask_PortsArePowerSwitched = 0x00000000};
					enum {Value_PortsAreAlwaysPowered = 0x1};
					enum {ValueMask_PortsAreAlwaysPowered = 0x00000200};
					};
				namespace DT { // Field Description
					enum {Lsb = 10};
					enum {FieldMask = 0x00000400};
					enum {Value_CompoundDevice = 0x1};
					enum {ValueMask_CompoundDevice = 0x00000400};
					enum {Value_NotCompoundDevice = 0x0};
					enum {ValueMask_NotCompoundDevice = 0x00000000};
					};
				namespace OCPM { // Field Description
					enum {Lsb = 11};
					enum {FieldMask = 0x00000800};
					enum {Value_CollectiveStatusReporting = 0x0};
					enum {ValueMask_CollectiveStatusReporting = 0x00000000};
					enum {Value_IndividualStatusReporting = 0x1};
					enum {ValueMask_IndividualStatusReporting = 0x00000800};
					};
				namespace NOCP { // Field Description
					enum {Lsb = 12};
					enum {FieldMask = 0x00001000};
					enum {Value_CollectiveStatusReporting = 0x0};
					enum {ValueMask_CollectiveStatusReporting = 0x00000000};
					enum {Value_NoOvercurrentProtection = 0x1};
					enum {ValueMask_NoOvercurrentProtection = 0x00001000};
					};
				namespace POTPGT { // Field Description
					enum {Lsb = 24};
					enum {FieldMask = 0xFF000000};
					enum {Value_Maximum = 0xFF};
					enum {ValueMask_Maximum = 0xFF000000};
					};
				};
			namespace RhDescriptorB { // Register description
				typedef uint32_t	Reg;
				namespace DR { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x0000FFFF};
					enum {Value_Removeable = 0x0};
					enum {ValueMask_Removeable = 0x00000000};
					enum {Value_NotRemoveable = 0x1};
					enum {ValueMask_NotRemoveable = 0x00000001};
					namespace Port1 { // Field Description
						enum {Lsb = 1};
						enum {FieldMask = 0x00000002};
						enum {Value_Removable = 0x0};
						enum {ValueMask_Removable = 0x00000000};
						enum {Value_NotRemovable = 0x1};
						enum {ValueMask_NotRemovable = 0x00000002};
						};
					namespace Port2 { // Field Description
						enum {Lsb = 2};
						enum {FieldMask = 0x00000004};
						enum {Value_Removable = 0x0};
						enum {ValueMask_Removable = 0x00000000};
						enum {Value_NotRemovable = 0x1};
						enum {ValueMask_NotRemovable = 0x00000004};
						};
					namespace Port3 { // Field Description
						enum {Lsb = 3};
						enum {FieldMask = 0x00000008};
						enum {Value_Removable = 0x0};
						enum {ValueMask_Removable = 0x00000000};
						enum {Value_NotRemovable = 0x1};
						enum {ValueMask_NotRemovable = 0x00000008};
						};
					namespace Port4 { // Field Description
						enum {Lsb = 4};
						enum {FieldMask = 0x00000010};
						enum {Value_Removable = 0x0};
						enum {ValueMask_Removable = 0x00000000};
						enum {Value_NotRemovable = 0x1};
						enum {ValueMask_NotRemovable = 0x00000010};
						};
					namespace Port5 { // Field Description
						enum {Lsb = 5};
						enum {FieldMask = 0x00000020};
						enum {Value_Removable = 0x0};
						enum {ValueMask_Removable = 0x00000000};
						enum {Value_NotRemovable = 0x1};
						enum {ValueMask_NotRemovable = 0x00000020};
						};
					namespace Port6 { // Field Description
						enum {Lsb = 6};
						enum {FieldMask = 0x00000040};
						enum {Value_Removable = 0x0};
						enum {ValueMask_Removable = 0x00000000};
						enum {Value_NotRemovable = 0x1};
						enum {ValueMask_NotRemovable = 0x00000040};
						};
					namespace Port7 { // Field Description
						enum {Lsb = 7};
						enum {FieldMask = 0x00000080};
						enum {Value_Removable = 0x0};
						enum {ValueMask_Removable = 0x00000000};
						enum {Value_NotRemovable = 0x1};
						enum {ValueMask_NotRemovable = 0x00000080};
						};
					namespace Port8 { // Field Description
						enum {Lsb = 8};
						enum {FieldMask = 0x00000100};
						enum {Value_Removable = 0x0};
						enum {ValueMask_Removable = 0x00000000};
						enum {Value_NotRemovable = 0x1};
						enum {ValueMask_NotRemovable = 0x00000100};
						};
					namespace Port9 { // Field Description
						enum {Lsb = 9};
						enum {FieldMask = 0x00000200};
						enum {Value_Removable = 0x0};
						enum {ValueMask_Removable = 0x00000000};
						enum {Value_NotRemovable = 0x1};
						enum {ValueMask_NotRemovable = 0x00000200};
						};
					namespace Port10 { // Field Description
						enum {Lsb = 10};
						enum {FieldMask = 0x00000400};
						enum {Value_Removable = 0x0};
						enum {ValueMask_Removable = 0x00000000};
						enum {Value_NotRemovable = 0x1};
						enum {ValueMask_NotRemovable = 0x00000400};
						};
					namespace Port11 { // Field Description
						enum {Lsb = 11};
						enum {FieldMask = 0x00000800};
						enum {Value_Removable = 0x0};
						enum {ValueMask_Removable = 0x00000000};
						enum {Value_NotRemovable = 0x1};
						enum {ValueMask_NotRemovable = 0x00000800};
						};
					namespace Port12 { // Field Description
						enum {Lsb = 12};
						enum {FieldMask = 0x00001000};
						enum {Value_Removable = 0x0};
						enum {ValueMask_Removable = 0x00000000};
						enum {Value_NotRemovable = 0x1};
						enum {ValueMask_NotRemovable = 0x00001000};
						};
					namespace Port13 { // Field Description
						enum {Lsb = 13};
						enum {FieldMask = 0x00002000};
						enum {Value_Removable = 0x0};
						enum {ValueMask_Removable = 0x00000000};
						enum {Value_NotRemovable = 0x1};
						enum {ValueMask_NotRemovable = 0x00002000};
						};
					namespace Port14 { // Field Description
						enum {Lsb = 14};
						enum {FieldMask = 0x00004000};
						enum {Value_Removable = 0x0};
						enum {ValueMask_Removable = 0x00000000};
						enum {Value_NotRemovable = 0x1};
						enum {ValueMask_NotRemovable = 0x00004000};
						};
					namespace Port15 { // Field Description
						enum {Lsb = 15};
						enum {FieldMask = 0x00008000};
						enum {Value_Removable = 0x0};
						enum {ValueMask_Removable = 0x00000000};
						enum {Value_NotRemovable = 0x1};
						enum {ValueMask_NotRemovable = 0x00008000};
						};
					};
				namespace PPCM { // Field Description
					enum {Lsb = 16};
					enum {FieldMask = 0xFFFF0000};
					enum {Value_Individual = 0x1};
					enum {ValueMask_Individual = 0x00010000};
					enum {Value_Global = 0x0};
					enum {ValueMask_Global = 0x00000000};
					namespace Port1 { // Field Description
						enum {Lsb = 17};
						enum {FieldMask = 0x00020000};
						enum {Value_Individual = 0x1};
						enum {ValueMask_Individual = 0x00020000};
						enum {Value_Global = 0x0};
						enum {ValueMask_Global = 0x00000000};
						};
					namespace Port2 { // Field Description
						enum {Lsb = 18};
						enum {FieldMask = 0x00040000};
						enum {Value_Individual = 0x1};
						enum {ValueMask_Individual = 0x00040000};
						enum {Value_Global = 0x0};
						enum {ValueMask_Global = 0x00000000};
						};
					namespace Port3 { // Field Description
						enum {Lsb = 19};
						enum {FieldMask = 0x00080000};
						enum {Value_Individual = 0x1};
						enum {ValueMask_Individual = 0x00080000};
						enum {Value_Global = 0x0};
						enum {ValueMask_Global = 0x00000000};
						};
					namespace Port4 { // Field Description
						enum {Lsb = 20};
						enum {FieldMask = 0x00100000};
						enum {Value_Individual = 0x1};
						enum {ValueMask_Individual = 0x00100000};
						enum {Value_Global = 0x0};
						enum {ValueMask_Global = 0x00000000};
						};
					namespace Port5 { // Field Description
						enum {Lsb = 21};
						enum {FieldMask = 0x00200000};
						enum {Value_Individual = 0x1};
						enum {ValueMask_Individual = 0x00200000};
						enum {Value_Global = 0x0};
						enum {ValueMask_Global = 0x00000000};
						};
					namespace Port6 { // Field Description
						enum {Lsb = 22};
						enum {FieldMask = 0x00400000};
						enum {Value_Individual = 0x1};
						enum {ValueMask_Individual = 0x00400000};
						enum {Value_Global = 0x0};
						enum {ValueMask_Global = 0x00000000};
						};
					namespace Port7 { // Field Description
						enum {Lsb = 23};
						enum {FieldMask = 0x00800000};
						enum {Value_Individual = 0x1};
						enum {ValueMask_Individual = 0x00800000};
						enum {Value_Global = 0x0};
						enum {ValueMask_Global = 0x00000000};
						};
					namespace Port8 { // Field Description
						enum {Lsb = 24};
						enum {FieldMask = 0x01000000};
						enum {Value_Individual = 0x1};
						enum {ValueMask_Individual = 0x01000000};
						enum {Value_Global = 0x0};
						enum {ValueMask_Global = 0x00000000};
						};
					namespace Port9 { // Field Description
						enum {Lsb = 25};
						enum {FieldMask = 0x02000000};
						enum {Value_Individual = 0x1};
						enum {ValueMask_Individual = 0x02000000};
						enum {Value_Global = 0x0};
						enum {ValueMask_Global = 0x00000000};
						};
					namespace Port10 { // Field Description
						enum {Lsb = 26};
						enum {FieldMask = 0x04000000};
						enum {Value_Individual = 0x1};
						enum {ValueMask_Individual = 0x04000000};
						enum {Value_Global = 0x0};
						enum {ValueMask_Global = 0x00000000};
						};
					namespace Port11 { // Field Description
						enum {Lsb = 27};
						enum {FieldMask = 0x08000000};
						enum {Value_Individual = 0x1};
						enum {ValueMask_Individual = 0x08000000};
						enum {Value_Global = 0x0};
						enum {ValueMask_Global = 0x00000000};
						};
					namespace Port12 { // Field Description
						enum {Lsb = 28};
						enum {FieldMask = 0x10000000};
						enum {Value_Individual = 0x1};
						enum {ValueMask_Individual = 0x10000000};
						enum {Value_Global = 0x0};
						enum {ValueMask_Global = 0x00000000};
						};
					namespace Port13 { // Field Description
						enum {Lsb = 29};
						enum {FieldMask = 0x20000000};
						enum {Value_Individual = 0x1};
						enum {ValueMask_Individual = 0x20000000};
						enum {Value_Global = 0x0};
						enum {ValueMask_Global = 0x00000000};
						};
					namespace Port14 { // Field Description
						enum {Lsb = 30};
						enum {FieldMask = 0x40000000};
						enum {Value_Individual = 0x1};
						enum {ValueMask_Individual = 0x40000000};
						enum {Value_Global = 0x0};
						enum {ValueMask_Global = 0x00000000};
						};
					namespace Port15 { // Field Description
						enum {Lsb = 31};
						enum {FieldMask = 0x80000000};
						enum {Value_Individual = 0x1};
						enum {ValueMask_Individual = 0x80000000};
						enum {Value_Global = 0x0};
						enum {ValueMask_Global = 0x00000000};
						};
					};
				};
			namespace RhStatus { // Register description
				typedef uint32_t	Reg;
				namespace LPS { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x00000001};
					enum {Value_LocalPowerError = 0x1};
					enum {ValueMask_LocalPowerError = 0x00000001};
					enum {Value_ClearGlobalPower = 0x1};
					enum {ValueMask_ClearGlobalPower = 0x00000001};
					};
				namespace OCI { // Field Description
					enum {Lsb = 1};
					enum {FieldMask = 0x00000002};
					enum {Value_OverCurrentCondition = 0x1};
					enum {ValueMask_OverCurrentCondition = 0x00000002};
					enum {Value_NotOverCurrentCondition = 0x0};
					enum {ValueMask_NotOverCurrentCondition = 0x00000000};
					};
				namespace DRWE { // Field Description
					enum {Lsb = 15};
					enum {FieldMask = 0x00008000};
					enum {Value_ConnectStatusChangeIsNotARemoteWakeupEvent = 0x0};
					enum {ValueMask_ConnectStatusChangeIsNotARemoteWakeupEvent = 0x00000000};
					enum {Value_ConnectStatusChangeIsARemoteWakeupEvent = 0x1};
					enum {ValueMask_ConnectStatusChangeIsARemoteWakeupEvent = 0x00008000};
					enum {Value_SetRemoteWakeupEnable = 0x1};
					enum {ValueMask_SetRemoteWakeupEnable = 0x00008000};
					};
				namespace LPSC { // Field Description
					enum {Lsb = 16};
					enum {FieldMask = 0x00010000};
					enum {Value_LocalPowerStatusChanged = 0x1};
					enum {ValueMask_LocalPowerStatusChanged = 0x00010000};
					enum {Value_SetGlobalPower = 0x1};
					enum {ValueMask_SetGlobalPower = 0x00010000};
					};
				namespace OCIC { // Field Description
					enum {Lsb = 17};
					enum {FieldMask = 0x00020000};
					enum {Value_OciChanged = 0x1};
					enum {ValueMask_OciChanged = 0x00020000};
					enum {Value_OciNotChanged = 0x0};
					enum {ValueMask_OciNotChanged = 0x00000000};
					enum {Value_Clear = 0x1};
					enum {ValueMask_Clear = 0x00020000};
					};
				namespace CRWE { // Field Description
					enum {Lsb = 31};
					enum {FieldMask = 0x80000000};
					enum {Value_ClearRemoteWakeupEnable = 0x1};
					enum {ValueMask_ClearRemoteWakeupEnable = 0x80000000};
					};
				};
			namespace RhPortStatus { // Register description
				typedef uint32_t	Reg;
				enum {MaxDownstreamPorts = 15};
				namespace CCS { // Field Description
					enum {Lsb = 0};
					enum {FieldMask = 0x00000001};
					enum {Value_NoDeviceConnected = 0x0};
					enum {ValueMask_NoDeviceConnected = 0x00000000};
					enum {Value_DeviceConnected = 0x1};
					enum {ValueMask_DeviceConnected = 0x00000001};
					enum {Value_ClearPortEnable = 0x1};
					enum {ValueMask_ClearPortEnable = 0x00000001};
					};
				namespace PES { // Field Description
					enum {Lsb = 1};
					enum {FieldMask = 0x00000002};
					enum {Value_PortDisabled = 0x1};
					enum {ValueMask_PortDisabled = 0x00000002};
					enum {Value_PortEnabled = 0x1};
					enum {ValueMask_PortEnabled = 0x00000002};
					enum {Value_SetPortEnable = 0x1};
					enum {ValueMask_SetPortEnable = 0x00000002};
					};
				namespace PSS { // Field Description
					enum {Lsb = 2};
					enum {FieldMask = 0x00000004};
					enum {Value_PortNotSuspended = 0x0};
					enum {ValueMask_PortNotSuspended = 0x00000000};
					enum {Value_PortSuspended = 0x1};
					enum {ValueMask_PortSuspended = 0x00000004};
					enum {Value_SetPortSuspend = 0x1};
					enum {ValueMask_SetPortSuspend = 0x00000004};
					};
				namespace POCI { // Field Description
					enum {Lsb = 3};
					enum {FieldMask = 0x00000008};
					enum {Value_NoOvercurrent = 0x0};
					enum {ValueMask_NoOvercurrent = 0x00000000};
					enum {Value_Overcurrent = 0x1};
					enum {ValueMask_Overcurrent = 0x00000008};
					enum {Value_ClearSuspendStatus = 0x1};
					enum {ValueMask_ClearSuspendStatus = 0x00000008};
					};
				namespace PRS { // Field Description
					enum {Lsb = 4};
					enum {FieldMask = 0x00000010};
					enum {Value_PortResetNotActive = 0x0};
					enum {ValueMask_PortResetNotActive = 0x00000000};
					enum {Value_PortResetActive = 0x1};
					enum {ValueMask_PortResetActive = 0x00000010};
					enum {Value_SetPortReset = 0x1};
					enum {ValueMask_SetPortReset = 0x00000010};
					};
				namespace PPS { // Field Description
					enum {Lsb = 8};
					enum {FieldMask = 0x00000100};
					enum {Value_PortPowerIsOff = 0x0};
					enum {ValueMask_PortPowerIsOff = 0x00000000};
					enum {Value_PortPowerIsOn = 0x1};
					enum {ValueMask_PortPowerIsOn = 0x00000100};
					enum {Value_SetPortPower = 0x1};
					enum {ValueMask_SetPortPower = 0x00000100};
					};
				namespace LSDA { // Field Description
					enum {Lsb = 9};
					enum {FieldMask = 0x00000200};
					enum {Value_FullSpeedDeviceAttached = 0x0};
					enum {ValueMask_FullSpeedDeviceAttached = 0x00000000};
					enum {Value_LowSpeedDeviceAttached = 0x1};
					enum {ValueMask_LowSpeedDeviceAttached = 0x00000200};
					enum {Value_ClearPortPower = 0x1};
					enum {ValueMask_ClearPortPower = 0x00000200};
					};
				namespace CSC { // Field Description
					enum {Lsb = 16};
					enum {FieldMask = 0x00010000};
					enum {Value_NoChange = 0x0};
					enum {ValueMask_NoChange = 0x00000000};
					enum {Value_Changed = 0x1};
					enum {ValueMask_Changed = 0x00010000};
					enum {Value_Clear = 0x1};
					enum {ValueMask_Clear = 0x00010000};
					};
				namespace PESC { // Field Description
					enum {Lsb = 17};
					enum {FieldMask = 0x00020000};
					enum {Value_NoChange = 0x0};
					enum {ValueMask_NoChange = 0x00000000};
					enum {Value_Changed = 0x1};
					enum {ValueMask_Changed = 0x00020000};
					enum {Value_Clear = 0x1};
					enum {ValueMask_Clear = 0x00020000};
					};
				namespace PSSC { // Field Description
					enum {Lsb = 18};
					enum {FieldMask = 0x00040000};
					enum {Value_ResumeIsNotComplete = 0x0};
					enum {ValueMask_ResumeIsNotComplete = 0x00000000};
					enum {Value_ResumeIsComplete = 0x1};
					enum {ValueMask_ResumeIsComplete = 0x00040000};
					enum {Value_Clear = 0x1};
					enum {ValueMask_Clear = 0x00040000};
					};
				namespace OCIC { // Field Description
					enum {Lsb = 19};
					enum {FieldMask = 0x00080000};
					enum {Value_NoChange = 0x0};
					enum {ValueMask_NoChange = 0x00000000};
					enum {Value_Changed = 0x1};
					enum {ValueMask_Changed = 0x00080000};
					enum {Value_Clear = 0x1};
					enum {ValueMask_Clear = 0x00080000};
					};
				namespace PRSC { // Field Description
					enum {Lsb = 20};
					enum {FieldMask = 0x00100000};
					enum {Value_ResetNotComplete = 0x0};
					enum {ValueMask_ResetNotComplete = 0x00000000};
					enum {Value_ResetComplete = 0x1};
					enum {ValueMask_ResetComplete = 0x00100000};
					enum {Value_Clear = 0x1};
					enum {ValueMask_Clear = 0x00100000};
					};
				};
			};
		};
	};
#endif
