/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_hw_usb_ohci_memory_
#define _oscl_hw_usb_ohci_memory_
#include <stdint.h>
namespace Oscl { // Namespace description
	namespace Usb { // Namespace description
		namespace Ohci { // Namespace description
			namespace ED { // Namespace description
				namespace Dword0 { // Register description
					typedef uint32_t	Reg;
					namespace FA { // Field Description
						enum {Lsb = 0};
						enum {FieldMask = 0x0000007F};
						enum {Value_Maximum = 0x7F};
						enum {ValueMask_Maximum = 0x0000007F};
						};
					namespace EN { // Field Description
						enum {Lsb = 7};
						enum {FieldMask = 0x00000780};
						enum {Value_Maximum = 0xF};
						enum {ValueMask_Maximum = 0x00000780};
						};
					namespace D { // Field Description
						enum {Lsb = 11};
						enum {FieldMask = 0x00001800};
						enum {Value_GetDirFromTD = 0x0};
						enum {ValueMask_GetDirFromTD = 0x00000000};
						enum {Value_OUT = 0x1};
						enum {ValueMask_OUT = 0x00000800};
						enum {Value_IN = 0x2};
						enum {ValueMask_IN = 0x00001000};
						enum {Value_GetDirFromTDAlt = 0x3};
						enum {ValueMask_GetDirFromTDAlt = 0x00001800};
						};
					namespace S { // Field Description
						enum {Lsb = 13};
						enum {FieldMask = 0x00002000};
						enum {Value_FullSpeed = 0x0};
						enum {ValueMask_FullSpeed = 0x00000000};
						enum {Value_LowSpeed = 0x1};
						enum {ValueMask_LowSpeed = 0x00002000};
						};
					namespace K { // Field Description
						enum {Lsb = 14};
						enum {FieldMask = 0x00004000};
						enum {Value_Normal = 0x0};
						enum {ValueMask_Normal = 0x00000000};
						enum {Value_SkipThisTD = 0x1};
						enum {ValueMask_SkipThisTD = 0x00004000};
						};
					namespace F { // Field Description
						enum {Lsb = 15};
						enum {FieldMask = 0x00008000};
						enum {Value_General = 0x0};
						enum {ValueMask_General = 0x00000000};
						enum {Value_Isochronous = 0x1};
						enum {ValueMask_Isochronous = 0x00008000};
						};
					namespace MPS { // Field Description
						enum {Lsb = 16};
						enum {FieldMask = 0x07FF0000};
						enum {Value_Maximum = 0x7FF};
						enum {ValueMask_Maximum = 0x07FF0000};
						};
					};
				namespace Dword1 { // Register description
					typedef uint32_t	Reg;
					namespace TailP { // Field Description
						enum {Lsb = 4};
						enum {FieldMask = 0xFFFFFFF0};
						};
					};
				namespace Dword2 { // Register description
					typedef uint32_t	Reg;
					namespace H { // Field Description
						enum {Lsb = 0};
						enum {FieldMask = 0x00000001};
						enum {Value_Normal = 0x0};
						enum {ValueMask_Normal = 0x00000000};
						enum {Value_Halted = 0x1};
						enum {ValueMask_Halted = 0x00000001};
						};
					namespace C { // Field Description
						enum {Lsb = 1};
						enum {FieldMask = 0x00000002};
						};
					namespace HeadP { // Field Description
						enum {Lsb = 4};
						enum {FieldMask = 0xFFFFFFF0};
						};
					};
				namespace Dword3 { // Register description
					typedef uint32_t	Reg;
					namespace NextED { // Field Description
						enum {Lsb = 4};
						enum {FieldMask = 0xFFFFFFF0};
						};
					};
				};
			namespace TD { // Namespace description
				namespace Dword0 { // Register description
					typedef uint32_t	Reg;
					namespace DI { // Field Description
						enum {Lsb = 21};
						enum {FieldMask = 0x00E00000};
						enum {Value_Maximum = 0x7};
						enum {ValueMask_Maximum = 0x00E00000};
						enum {Value_OneFrame = 0x0};
						enum {ValueMask_OneFrame = 0x00000000};
						enum {Value_TwoFrame = 0x1};
						enum {ValueMask_TwoFrame = 0x00200000};
						enum {Value_ThreeFrame = 0x2};
						enum {ValueMask_ThreeFrame = 0x00400000};
						enum {Value_FourFrame = 0x3};
						enum {ValueMask_FourFrame = 0x00600000};
						enum {Value_FiveFrame = 0x4};
						enum {ValueMask_FiveFrame = 0x00800000};
						enum {Value_SixFrame = 0x5};
						enum {ValueMask_SixFrame = 0x00A00000};
						enum {Value_SevenFrame = 0x6};
						enum {ValueMask_SevenFrame = 0x00C00000};
						enum {Value_NoInterrupt = 0x7};
						enum {ValueMask_NoInterrupt = 0x00E00000};
						};
					namespace CC { // Field Description
						enum {Lsb = 28};
						enum {FieldMask = 0xF0000000};
						enum {Value_NoError = 0x0};
						enum {ValueMask_NoError = 0x00000000};
						enum {Value_CRC = 0x1};
						enum {ValueMask_CRC = 0x10000000};
						enum {Value_BitStuffing = 0x2};
						enum {ValueMask_BitStuffing = 0x20000000};
						enum {Value_DataToggleMismatch = 0x3};
						enum {ValueMask_DataToggleMismatch = 0x30000000};
						enum {Value_Stall = 0x4};
						enum {ValueMask_Stall = 0x40000000};
						enum {Value_DeviceNotResponding = 0x5};
						enum {ValueMask_DeviceNotResponding = 0x50000000};
						enum {Value_PidCheckFailure = 0x6};
						enum {ValueMask_PidCheckFailure = 0x60000000};
						enum {Value_UnexpectedPID = 0x7};
						enum {ValueMask_UnexpectedPID = 0x70000000};
						enum {Value_DataOverrun = 0x8};
						enum {ValueMask_DataOverrun = 0x80000000};
						enum {Value_DataUnderrun = 0x9};
						enum {ValueMask_DataUnderrun = 0x90000000};
						enum {Value_BufferOverrun = 0xA};
						enum {ValueMask_BufferOverrun = 0xA0000000};
						enum {Value_BufferUnderrun = 0xB};
						enum {ValueMask_BufferUnderrun = 0xB0000000};
						enum {Value_NotAccessed0 = 0xE};
						enum {ValueMask_NotAccessed0 = 0xE0000000};
						enum {Value_NotAccessed1 = 0xF};
						enum {ValueMask_NotAccessed1 = 0xF0000000};
						};
					};
				namespace Dword1 { // Register description
					typedef uint32_t	Reg;
					};
				namespace Dword2 { // Register description
					typedef uint32_t	Reg;
					};
				namespace Dword3 { // Register description
					typedef uint32_t	Reg;
					};
				namespace General { // Namespace description
					namespace Dword0 { // Register description
						typedef uint32_t	Reg;
						namespace R { // Field Description
							enum {Lsb = 18};
							enum {FieldMask = 0x00040000};
							enum {Value_ExactFillRequired = 0x0};
							enum {ValueMask_ExactFillRequired = 0x00000000};
							enum {Value_ShortLastDataPacketOK = 0x1};
							enum {ValueMask_ShortLastDataPacketOK = 0x00040000};
							};
						namespace DP { // Field Description
							enum {Lsb = 19};
							enum {FieldMask = 0x00180000};
							enum {Value_SETUP = 0x0};
							enum {ValueMask_SETUP = 0x00000000};
							enum {Value_OUT = 0x1};
							enum {ValueMask_OUT = 0x00080000};
							enum {Value_IN = 0x2};
							enum {ValueMask_IN = 0x00100000};
							};
						namespace T { // Field Description
							enum {Lsb = 24};
							enum {FieldMask = 0x03000000};
							namespace Source { // Field Description
								enum {Lsb = 25};
								enum {FieldMask = 0x02000000};
								enum {Value_ToggleCary = 0x0};
								enum {ValueMask_ToggleCary = 0x00000000};
								enum {Value_LSB = 0x1};
								enum {ValueMask_LSB = 0x02000000};
								};
							namespace Toggle { // Field Description
								enum {Lsb = 24};
								enum {FieldMask = 0x01000000};
								enum {Value_DATA0 = 0x0};
								enum {ValueMask_DATA0 = 0x00000000};
								enum {Value_DATA1 = 0x1};
								enum {ValueMask_DATA1 = 0x01000000};
								};
							};
						namespace EC { // Field Description
							enum {Lsb = 26};
							enum {FieldMask = 0x0C000000};
							enum {Value_NoError = 0x0};
							enum {ValueMask_NoError = 0x00000000};
							};
						};
					namespace Dword1 { // Register description
						typedef uint32_t	Reg;
						namespace CPB { // Field Description
							enum {Lsb = 0};
							enum {FieldMask = 0xFFFFFFFF};
							};
						};
					namespace Dword2 { // Register description
						typedef uint32_t	Reg;
						namespace NextTD { // Field Description
							enum {Lsb = 4};
							enum {FieldMask = 0xFFFFFFF0};
							};
						};
					namespace Dword3 { // Register description
						typedef uint32_t	Reg;
						namespace BE { // Field Description
							enum {Lsb = 0};
							enum {FieldMask = 0xFFFFFFFF};
							};
						};
					};
				namespace Isochronous { // Namespace description
					namespace Dword0 { // Register description
						typedef uint32_t	Reg;
						namespace SF { // Field Description
							enum {Lsb = 0};
							enum {FieldMask = 0x0000FFFF};
							enum {Value_Maximum = 0xFFFF};
							enum {ValueMask_Maximum = 0x0000FFFF};
							};
						namespace FC { // Field Description
							enum {Lsb = 24};
							enum {FieldMask = 0x07000000};
							enum {Value_Maximum = 0x7};
							enum {ValueMask_Maximum = 0x07000000};
							enum {Value_One = 0x0};
							enum {ValueMask_One = 0x00000000};
							enum {Value_Two = 0x1};
							enum {ValueMask_Two = 0x01000000};
							enum {Value_Three = 0x2};
							enum {ValueMask_Three = 0x02000000};
							enum {Value_Four = 0x3};
							enum {ValueMask_Four = 0x03000000};
							enum {Value_Five = 0x4};
							enum {ValueMask_Five = 0x04000000};
							enum {Value_Six = 0x5};
							enum {ValueMask_Six = 0x05000000};
							enum {Value_Seven = 0x6};
							enum {ValueMask_Seven = 0x06000000};
							enum {Value_Eight = 0x7};
							enum {ValueMask_Eight = 0x07000000};
							};
						};
					namespace Dword1 { // Register description
						typedef uint32_t	Reg;
						namespace BP0 { // Field Description
							enum {Lsb = 12};
							enum {FieldMask = 0xFFFFF000};
							};
						};
					namespace Dword2 { // Register description
						typedef uint32_t	Reg;
						namespace NextTD { // Field Description
							enum {Lsb = 5};
							enum {FieldMask = 0xFFFFFFE0};
							};
						};
					namespace Dword3 { // Register description
						typedef uint32_t	Reg;
						namespace BE { // Field Description
							enum {Lsb = 0};
							enum {FieldMask = 0xFFFFFFFF};
							};
						};
					namespace OffsetPSW { // Register description
						typedef uint16_t	Reg;
						enum {MaximumFrames = 8};
						namespace PSW { // Field Description
							enum {Lsb = 0};
							enum {FieldMask = 0x0000FFFF};
							namespace CC { // Field Description
								enum {Lsb = 12};
								enum {FieldMask = 0x0000F000};
								enum {Value_NoError = 0x0};
								enum {ValueMask_NoError = 0x00000000};
								enum {Value_CRC = 0x1};
								enum {ValueMask_CRC = 0x00001000};
								enum {Value_BitStuffing = 0x2};
								enum {ValueMask_BitStuffing = 0x00002000};
								enum {Value_DataToggleMismatch = 0x3};
								enum {ValueMask_DataToggleMismatch = 0x00003000};
								enum {Value_Stall = 0x4};
								enum {ValueMask_Stall = 0x00004000};
								enum {Value_DeviceNotResponding = 0x5};
								enum {ValueMask_DeviceNotResponding = 0x00005000};
								enum {Value_PidCheckFailure = 0x6};
								enum {ValueMask_PidCheckFailure = 0x00006000};
								enum {Value_UnexpectedPID = 0x7};
								enum {ValueMask_UnexpectedPID = 0x00007000};
								enum {Value_DataOverrun = 0x8};
								enum {ValueMask_DataOverrun = 0x00008000};
								enum {Value_DataUnderrun = 0x9};
								enum {ValueMask_DataUnderrun = 0x00009000};
								enum {Value_BufferOverrun = 0xA};
								enum {ValueMask_BufferOverrun = 0x0000A000};
								enum {Value_BufferUnderrun = 0xB};
								enum {ValueMask_BufferUnderrun = 0x0000B000};
								enum {Value_NotAccessed0 = 0xE};
								enum {ValueMask_NotAccessed0 = 0x0000E000};
								enum {Value_NotAccessed1 = 0xF};
								enum {ValueMask_NotAccessed1 = 0x0000F000};
								};
							namespace SIZE { // Field Description
								enum {Lsb = 0};
								enum {FieldMask = 0x000007FF};
								enum {Value_Maximum = 0x7FF};
								enum {ValueMask_Maximum = 0x000007FF};
								};
							};
						};
					};
				};
			};
		};
	};
#endif
