/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_hw_usb_ohci_ohcih_
#define _oscl_hw_usb_ohci_ohcih_
#include <stdint.h>

/** */
namespace Oscl {
/** */
namespace Usb {
/** */
namespace Ohci {

class EndpointDesc;

class TransferDesc {
	private:
		friend class EndpointDesc;
		TransferDesc*	_next;
	public:
		void	push(TransferDesc* next) noexcept{
			next->_next	= _next;
			_next	= next;
			}
	};

typedef enum {
	getFromTD		= 0,
	output			= 1,
	input			= 2,
	getFromTdAlt	= 3
	} Direction;

class EndpointDesc {
	private:
		volatile uint32_t		_controlStatus;
		TransferDesc*			_tail;
		volatile TransferDesc*	_head;
		EndpointDesc*			_next;
	public:
		OhciUsbEndpointDesc(	uint8_t	functionAddress,// 0->127
								uint8_t	endpointNumber,	// 0->15
								Direction	direction,
								bool		lowSpeed,
								bool		skip,
								bool		isochronousFormat,
								uint16_t	maximumPacketSize,
								
								) noexcept{
			controlStatus	=		((functionAddress & 0x7F)<<0)
								|	((endpointNumber & 0x0F)<<7)
								|	((direction & 0x03)<<11)
								|	((lowSpeed)<<13)
								|	((skip)<<14)
								|	((isochronousFormat)<<15)
								|	((maximumPacketSize & 0x7FF)<<16)
								;
			}
		bool	halted() noexcept{
			return head & (1<<0);
			}
		bool	toggleCarry() noexcept{
			return head & (1<<1);
			}
		void	skipED(){
			skip |= (1<<13);
			}
		void	includeED(){
			skip &= ~(1<<13);
			}
		TransferDesc*	getHeadTD() noexcept{
			return (OhciUsbTransferDesc*)
						(unsigned long)head & (~((unsigned long)0x0F));
			}
		TransferDesc*	getTailTD() noexcept{
			return (OhciUsbTransferDesc*)
						(unsigned long)tail & (~((unsigned long)0x0F));
			}
		EndpointDesc*	getNextED() noexcept{
			return (OhciUsbTransferDesc*)
						(unsigned long)next & (~((unsigned long)0x0F));
			}
		void	put(TransferDesc* desc) noexcept{
			}
		void	insert(EndpointDesc* desc) noexcept{
			desc->next	= _next;
			_next		= desc;
			}
	};

};
};
};

#endif
