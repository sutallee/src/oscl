/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_hw_usb_ohci_memh_
#define _oscl_hw_usb_ohci_memh_
#include <stdint.h>
#include "memory.h"
#include "oscl/bits/bitfield.h"
#include "oscl/endian/type.h"
#include "oscl/cache/line.h"
#include "oscl/compiler/align.h"

/** In the following macro magic, 16 is the
	minimum alignment required by USB.
 */
#if 16 > OsclCacheLineSizeInBytes
#define OsclUsbOhciCacheLineSizeInBytes	16
#else
#define OsclUsbOhciCacheLineSizeInBytes	OsclCacheLineSizeInBytes
#endif

/** */
namespace Oscl {
/** */
namespace Usb {
/** */
namespace Ohci {

using namespace Oscl::Endian;
/** */
namespace ED {
	/** */
	OsclCompilerAlignMacroPre(OsclUsbOhciCacheLineSizeInBytes)
	typedef struct Map {
		/** Offset 0x00 */
		Little::E<Dword0::Reg>	dword0;
		/** Offset 0x04 */
		Little::E< volatile Dword1::Reg >			dword1;
		/** Offset 0x08 */
		Little::E<Dword2::Reg>						dword2;
		/** Offset 0x0C */
		Little::E<Dword3::Reg >						dword3;
		} Map
		OsclCompilerAlignMacroPost(OsclUsbOhciCacheLineSizeInBytes);
	}

/** */
namespace TD {
	/** */
	namespace General {
		/** */
		OsclCompilerAlignMacroPre(OsclUsbOhciCacheLineSizeInBytes)
		typedef struct Map {
			/** Offset 0x00 */
			Little::E< volatile Dword0::Reg >	dword0;
			/** Offset 0x04 */
			Little::E< Dword1::Reg >			dword1;
			/** Offset 0x08 */
			Little::E< Dword2::Reg >			dword2;
			/** Offset 0x0A */
			Little::E< Dword3::Reg >			dword3;
			} Map
			OsclCompilerAlignMacroPost(OsclUsbOhciCacheLineSizeInBytes);
		}
	/** */
	namespace Isochronous {
		/** */
		OsclCompilerAlignMacroPre(OsclUsbOhciCacheLineSizeInBytes)
		typedef struct Map {
			/** Offset 0x00 */
			Little::E<volatile Dword0::Reg>	dword0;
			/** Offset 0x04 */
			Little::E< Dword0::Reg >		dword1;
			/** Offset 0x08 */
			Little::E< Dword0::Reg >		dword2;
			/** Offset 0x0C */
			Little::E< Dword0::Reg >		dword3;
			/** Offset 0x10 */
			Little::E<	volatile OffsetPSW::Reg
						>		offsetPsw[OffsetPSW::MaximumFrames];
			} Map
			OsclCompilerAlignMacroPost(OsclUsbOhciCacheLineSizeInBytes);
		}
	/** */
	OsclCompilerAlignMacroPre(OsclUsbOhciCacheLineSizeInBytes)
	typedef struct Base {
		/** Offset 0x00 */
		Little::E<volatile Dword0::Reg>	dword0;
		/** Offset 0x04 */
		Little::E<Dword1::Reg>			dword1;
		/** Offset 0x08 */
		Little::E<Dword2::Reg>			dword2;
		/** Offset 0x0A */
		Little::E<Dword3::Reg>			dword3;
		} Base
		OsclCompilerAlignMacroPost(OsclUsbOhciCacheLineSizeInBytes);

	/** */
	typedef union Map {
		/** */
		Base				base;
		/** */
		General::Map		general;
		/** */
		Isochronous::Map	isochronous;
		} Map;
	}

}
}
}

#endif
