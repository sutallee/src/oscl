/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_hw_usb_ohci_hccah_
#define _oscl_hw_usb_ohci_hccah_
#include "oscl/endian/type.h"
#include "oscl/compiler/align.h"

/** */
namespace Oscl {
/** */
namespace Usb {
/** */
namespace Ohci {

/** */
OsclCompilerAlignMacroPre(256)
typedef struct Hcca {
	/** Offset 0x00 */
	Oscl::Endian::Little::U32	interruptTable[32];
	/** Offset 0x80 */
	Oscl::Endian::Little::U16	frameNumber;
	/** Offset 0x82 */
	uint16_t					pad1;
	/** Offset 0x84 */
	Oscl::Endian::Little::U32	doneHead;
	/** Offset 0x88 */
	uint8_t	reserved[116];
	}Hcca
OsclCompilerAlignMacroPost(256);

}
}
}


#endif
