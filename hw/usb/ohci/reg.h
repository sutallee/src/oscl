/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_hw_usb_ohci_regh_
#define _oscl_hw_usb_ohci_regh_
#include "registers.h"

/** */
namespace Oscl {
/** */
namespace Usb {
/** */
namespace Ohci {

typedef struct ControlStatus {
	/** Offset 0x00 */
	volatile Revision::Reg			revision;
	/** Offset 0x04 */
	volatile Control::Reg			control;
	/** Offset 0x08 */
	volatile CommandStatus::Reg		commandStatus;
	/** Offset 0x0C */
	volatile InterruptStatus::Reg	interruptStatus;
	/** Offset 0x10 */
	volatile InterruptEnable::Reg	interruptEnable;
	/** Offset 0x14 */
	volatile InterruptDisable::Reg	interruptDisable;
	} ControlStatus;

typedef struct MemoryPointer {
	/** Offset 0x18 */
	volatile HCCA::Reg				hcca;
	/** Offset 0x1C */
	volatile PeriodCurrentED::Reg	periodCurrentED;
	/** Offset 0x20 */
	volatile ControlHeadED::Reg		controlHeadED;
	/** Offset 0x24 */
	volatile ControlCurrentED::Reg	controlCurrentED;
	/** Offset 0x28 */
	volatile BulkHeadED::Reg		bulkHeadED;
	/** Offset 0x30 */
	volatile BulkCurrentED::Reg		bulkCurrentED;
	/** Offset 0x34 */
	volatile DoneHead::Reg			doneHead;
	} MemoryPointer;

typedef struct FrameCounter {
	/** Offset 0x38 */
	volatile FmInterval::Reg		fmInterval;
	/** Offset 0x3C */
	volatile FmRemaining::Reg		fmRemaining;
	/** Offset 0x40 */
	volatile FmNumber::Reg			fmNumber;
	/** Offset 0x44 */
	volatile PeriodicStart::Reg		periodicStart;
	/** Offset 0x48 */
	volatile LSThreshold::Reg		lsThreshold;
	} FrameCounter;

typedef struct RootHub {
	/** Offset 0x4C */
	volatile RhDescriptorA::Reg		rhDescriptorA;
	/** Offset 0x50 */
	volatile RhDescriptorB::Reg		rhDescriptorB;
	/** Offset 0x54 */
	volatile RhStatus::Reg			rhStatus;
	/** Offset 0x58 */
	volatile RhPortStatus::Reg		rhPortStatus[RhPortStatus::MaxDownstreamPorts];
	} RootHub;

/** */
typedef struct Map {
	/** Offset 0x00 */
	ControlStatus	controlStatus;
	/** Offset 0x18 */
	MemoryPointer	memoryPointer;
	/** Offset 0x38 */
	FrameCounter	frameCounter;
	/** Offset 0x4C */
	RootHub			rootHub;
	}Map;

}
}
}

#endif
