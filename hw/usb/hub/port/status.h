/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_hw_usb_hub_port_statush_
#define _oscl_hw_usb_hub_port_statush_
#include <stdint.h>
namespace Oscl { // Namespace description
	namespace Usb { // Namespace description
		namespace Hw { // Namespace description
			namespace Hub { // Namespace description
				namespace Port { // Namespace description
					namespace wPortChange { // Register description
						typedef uint16_t	Reg;
						namespace Connection { // Field Description
							enum {Lsb = 0};
							enum {FieldMask = 0x00000001};
							enum {Value_NoChange = 0x0};
							enum {ValueMask_NoChange = 0x00000000};
							enum {Value_Changed = 0x1};
							enum {ValueMask_Changed = 0x00000001};
							};
						namespace Enable { // Field Description
							enum {Lsb = 1};
							enum {FieldMask = 0x00000002};
							enum {Value_NoChange = 0x0};
							enum {ValueMask_NoChange = 0x00000000};
							enum {Value_Disabled = 0x1};
							enum {ValueMask_Disabled = 0x00000002};
							};
						namespace Suspend { // Field Description
							enum {Lsb = 2};
							enum {FieldMask = 0x00000004};
							enum {Value_NoChange = 0x0};
							enum {ValueMask_NoChange = 0x00000000};
							enum {Value_ResumeDone = 0x1};
							enum {ValueMask_ResumeDone = 0x00000004};
							};
						namespace OverCurrent { // Field Description
							enum {Lsb = 3};
							enum {FieldMask = 0x00000008};
							enum {Value_NoChange = 0x0};
							enum {ValueMask_NoChange = 0x00000000};
							enum {Value_Changed = 0x1};
							enum {ValueMask_Changed = 0x00000008};
							};
						namespace Reset { // Field Description
							enum {Lsb = 4};
							enum {FieldMask = 0x00000010};
							enum {Value_NoChange = 0x0};
							enum {ValueMask_NoChange = 0x00000000};
							enum {Value_ResetDone = 0x1};
							enum {ValueMask_ResetDone = 0x00000010};
							};
						};
					namespace wPortStatus { // Register description
						typedef uint16_t	Reg;
						namespace Connect { // Field Description
							enum {Lsb = 0};
							enum {FieldMask = 0x00000001};
							enum {Value_DeviceAbsent = 0x0};
							enum {ValueMask_DeviceAbsent = 0x00000000};
							enum {Value_DevicePresent = 0x1};
							enum {ValueMask_DevicePresent = 0x00000001};
							};
						namespace Enable { // Field Description
							enum {Lsb = 1};
							enum {FieldMask = 0x00000002};
							enum {Value_Disabled = 0x0};
							enum {ValueMask_Disabled = 0x00000000};
							enum {Value_Enabled = 0x1};
							enum {ValueMask_Enabled = 0x00000002};
							};
						namespace Suspend { // Field Description
							enum {Lsb = 2};
							enum {FieldMask = 0x00000004};
							enum {Value_NotSuspended = 0x0};
							enum {ValueMask_NotSuspended = 0x00000000};
							enum {Value_Suspended = 0x1};
							enum {ValueMask_Suspended = 0x00000004};
							enum {Value_Resuming = 0x1};
							enum {ValueMask_Resuming = 0x00000004};
							};
						namespace OverCurrent { // Field Description
							enum {Lsb = 3};
							enum {FieldMask = 0x00000008};
							enum {Value_Negated = 0x0};
							enum {ValueMask_Negated = 0x00000000};
							enum {Value_Asserted = 0x1};
							enum {ValueMask_Asserted = 0x00000008};
							};
						namespace Reset { // Field Description
							enum {Lsb = 4};
							enum {FieldMask = 0x00000010};
							enum {Value_Negated = 0x0};
							enum {ValueMask_Negated = 0x00000000};
							enum {Value_Asserted = 0x1};
							enum {ValueMask_Asserted = 0x00000010};
							};
						namespace Power { // Field Description
							enum {Lsb = 8};
							enum {FieldMask = 0x00000100};
							enum {Value_Off = 0x0};
							enum {ValueMask_Off = 0x00000000};
							enum {Value_On = 0x1};
							enum {ValueMask_On = 0x00000100};
							};
						namespace DeviceSpeed { // Field Description
							enum {Lsb = 9};
							enum {FieldMask = 0x00000200};
							enum {Value_FullSpeed = 0x0};
							enum {ValueMask_FullSpeed = 0x00000000};
							enum {Value_LowSpeed = 0x1};
							enum {ValueMask_LowSpeed = 0x00000200};
							};
						};
					};
				};
			};
		};
	};
#endif
