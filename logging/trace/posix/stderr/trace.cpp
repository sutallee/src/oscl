/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "oscl/strings/fixed.h"
#include "oscl/mt/thread.h"
#include "oscl/mt/scopesync.h"
#include <stdio.h>
#include <stdarg.h>
#include <string.h>

using namespace Oscl;
using namespace Oscl::Mt;


// Path Delimiter - Assumes UNIX
#ifdef _WIN32 
#define PATHDELIMITER_CHAR '\\';
#else
#define PATHDELIMITER_CHAR '/';
#endif
static char _pathDelimiter = PATHDELIMITER_CHAR;

// Flag that gates the output of the trace log
static bool _enabled = true;

// Flag that gates the outputting of the path with the file name
static bool _omitPath = true;


// Helper Method(s)
static const char* applyPathFilter( const char* fullPathAndFileName );
static void getThreadInfo( Strings::Api& name );


//////////////////////
Oscl::TraceScope::TraceScope( const char* filename, int linenum, const char* funcname )
:_filename(filename),
 _linenum(linenum),
 _funcname(funcname)
    {
    _traceMsg(_filename,_linenum,_funcname,"->ENTER");
    }

Oscl::TraceScope::~TraceScope()
    {
    _traceMsg(_filename,_linenum,_funcname,"<-EXIT ");
    }


//////////////////////
void Oscl::_traceMsg( const char* filename, int linenum, const char* funcname, const char* format, ... )
    {
    va_list ap;
    va_start(ap, format);

    if ( _enabled )
        {
        _Oscl_loggingMediaMutex.lock();
        Strings::Fixed<20> name;
        getThreadInfo( name );
        fprintf(stderr,
                ">> [%s,%s,%d,%s] ",
               (const char*)name,
               applyPathFilter(filename),
               linenum,
               funcname
              );

        vfprintf(stderr,format,ap);

        fprintf(stderr,"\n");
        _Oscl_loggingMediaMutex.unlock();
        }
    va_end(ap);
    }


void Oscl::_enableTracing()
    {
    _enabled = true;    
    }

void Oscl::_disableTracing()
    {
    _enabled = false;    
    }

void Oscl::_enableTracingPath()
    {
    _omitPath = false;
    }
    
void Oscl::_disableTracingPath()
    {
    _omitPath = true;
    }

//////////////////////
const char* applyPathFilter( const char* fullPathAndFileName )
    {
    if (_omitPath == true)
        {
        // find the last occurence of '/' to get a fileName without path
        char* temp = strrchr( fullPathAndFileName, _pathDelimiter );
        if (temp)
            {
            fullPathAndFileName = temp + 1;
            }
        }
    return fullPathAndFileName;
    }   

void getThreadInfo( Strings::Api& name )
    {
    name = Thread::getCurrent().getName();
    }
