/*
   Copyright (C) 2024 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_touch_single_output_parth_
#define _oscl_touch_single_output_parth_

#include "oscl/touch/single/observer/reqapi.h"
#include "oscl/queue/queue.h"
#include "oscl/touch/single/control/api.h"

/** */
namespace Oscl {

/** */
namespace Touch {

/** */
namespace Single {

/** */
namespace Output {

/** */
class Part :
		public Oscl::Touch::Single::Observer::Req::Api,
		public Oscl::Touch::Single::Control::Api
		{
	private:
		/** */
		Oscl::Touch::Single::Observer::Req::Api::ConcreteSAP	_sap;

		/** */
		Oscl::Queue<Oscl::Touch::Single::Observer::Req::Api::ChangeReq>	_noteQ;

		/** This variable contains the current state of the
			monitor point.
		 */
		bool										_pressed;

		/** */
		uint32_t									_x;

		/** */
		uint32_t									_y;

	public:
		/** */
		Part(
			Oscl::Mt::Itc::PostMsgApi&	papi,
			bool						initialPressed,
			uint32_t					initialX,
			uint32_t					initialY
			) noexcept;

		/** */
		Oscl::Touch::Single::Observer::Req::Api::SAP&	getSAP() noexcept;

	private:
		/** */
		void	notify(
					bool		pressed,
					uint32_t	x,
					uint32_t	y
					) noexcept;

	public: // Oscl::Touch::Single::Control::Api
		/** */
		void	update(
					bool		pressed,
					uint32_t	x,
					uint32_t	y
					) noexcept;

	public:	// Oscl::Touch::Single::Observer::Req::Api
		/** */
		void request(Oscl::Touch::Single::Observer::Req::Api::ChangeReq& msg) noexcept;

		/** */
		void request(Oscl::Touch::Single::Observer::Req::Api::CancelChangeReq& msg) noexcept;
	};

}
}
}
}

#endif
