/*
   Copyright (C) 2024 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "part.h"

using namespace Oscl::Touch::Single::Output;

Part::Part(
	Oscl::Mt::Itc::PostMsgApi&	papi,
	bool						initialPressed,
	uint32_t					initialX,
	uint32_t					initialY
	) noexcept:
		_sap(
			*this,
			papi
			),
		_pressed(initialPressed),
		_x(initialX),
		_y(initialX)
		{
	}

Oscl::Touch::Single::Observer::Req::Api::SAP&	Part::getSAP() noexcept{
	return _sap;
	}

void	Part::notify(
			bool		pressed,
			uint32_t	x,
			uint32_t	y
			) noexcept{

	_pressed	= pressed;
	_x			= x;
	_y			= y;

	Oscl::Touch::Single::Observer::Req::Api::ChangeReq* note;

	while((note=_noteQ.get())){
		Oscl::Touch::Single::Observer::Req::Api::
		ChangePayload&	payload	= note->getPayload();

		payload._pressed	= pressed;
		payload._x	= x;
		payload._y	= y;

		note->returnToSender();
		}
	}

void Part::request(Oscl::Touch::Single::Observer::Req::Api::ChangeReq& msg) noexcept{

	Oscl::Touch::Single::Observer::Req::Api::
	ChangePayload&	payload	= msg.getPayload();

	if(		( _pressed == payload._pressed )
		&&	( _x == payload._x )
		&&	( _y == payload._y )
		){
		_noteQ.put(&msg);
		return;
		}

	payload._pressed	= _pressed;
	payload._x			= _x;
	payload._y			= _y;
	msg.returnToSender();
	}

void Part::request(Oscl::Touch::Single::Observer::Req::Api::CancelChangeReq& msg) noexcept{

	for(
		Oscl::Touch::Single::Observer::Req::Api::ChangeReq* next=_noteQ.first();
		next;
		next=_noteQ.next(next)
		){
		if(next == &msg.getPayload()._requestToCancel){
			_noteQ.remove(next);
			next->returnToSender();
			break;
			}
		}

	msg.returnToSender();
	}

void	Part::update(
			bool		pressed,
			uint32_t	x,
			uint32_t	y
			) noexcept{

	if(		( _pressed != pressed )
		||	( _x != x )
		||	( _y != y )
		){
		notify( pressed, x, y );
		}
	}

