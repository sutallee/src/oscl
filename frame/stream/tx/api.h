/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_frame_stream_tx_apih_
#define _oscl_frame_stream_tx_apih_

/** */
namespace Oscl {
/** */
namespace Frame {
/** */
namespace Stream {
/** */
namespace TX {

/** */
class Api {
	public:
		/** */
		virtual ~Api() {}
		/** This operation is invoked when a new frame is being received.
			This operation may be used by the implementation to allocate
			resources for the incomming frame.
		 */
		virtual void		open() noexcept=0;

		/** This operation is invoked to append a single octet to
			the open frame. One is returned if the octet is successfuly
			appended. Zero is returned if insufficient resources are
			available to hold the frame, in which case the client
			may elect to abort() the frame.
		 */
		virtual unsigned	append(unsigned char octet) noexcept=0;

		/** This operation is invoked to append the specified number
			of octets to the open frame. The number of octets actually
			appended are returned. If the return value is less than
			the number of octets specified by the client, there
			were insufficient resources available to contain the frame,
			and the client should then abort() the frame.
		 */
		virtual unsigned	append(	const void*	octets,
									unsigned	nOctets
									) noexcept=0;

		/** This operation is invoked when the last octet in the currently
			open frame has been received and has been determined to be error
			free. The client specifies the number of trailer octets that
			are FCS octets. The implementation may subsequently strip
			the FCS octets from the frame.
		 */
		virtual void		close(unsigned nFcsOctets) noexcept=0;

		/** This operation is invoked instead of the close() when the
			currently open frame contains an error.
		 */
		virtual void		abort() noexcept=0;
	};

}
}
}
}


#endif
