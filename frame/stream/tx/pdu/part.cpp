/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "part.h"
#include "oscl/pdu/leaf.h"
#include "oscl/error/fatal.h"

using namespace Oscl::Frame::Stream::TX::PDU;

Part::Part(	Oscl::Mt::Itc::PostMsgApi&	myPapi,
			Oscl::PDU::TX::Api&			framer
			) noexcept:
		_myPapi(myPapi),
		_framer(framer),
		_closeSAP(*this,myPapi),
		_framerSAP(*this,myPapi),
		_closeReq(0)
		{
	}

Oscl::Mt::Itc::Srv::Close::Req::Api::SAP&	Part::getCloseSAP() noexcept{
	return _closeSAP;
	}

Oscl::Frame::Pdu::Tx::Req::Api::SAP&		Part::getFrameTxSAP() noexcept{
	return _framerSAP;
	}

bool /* stop */	Part::accept(	const Oscl::Pdu::Composite&	p,
								unsigned					position
								) noexcept{
	return false;
	}

bool /* stop */	Part::accept(	const Oscl::Pdu::ReadOnlyComposite&	p,
								unsigned							position
								) noexcept{
	return false;
	}

bool /* stop */ Part::accept(	const Oscl::Pdu::Leaf&	p,
								unsigned				position,
								unsigned				offset,
								unsigned				length
								) noexcept{
	unsigned	actualLength;
	const void*	buffer	= p.getBuffer(offset,actualLength);
	if(!buffer){
		Oscl::ErrorFatal::
		logAndExit("Oscl::Frame::Stream::TX::PDU: buffer error\n");
		}
	return !_framer.append(buffer,length);
	}

bool /* stop */ Part::accept(	const Oscl::Pdu::Fragment&	p,
								unsigned					position
								) noexcept{
	return false;
	}

bool /* stop */ Part::accept(	const Oscl::Pdu::ReadOnlyFragment&	p,
								unsigned							position
								) noexcept{
	return false;
	}

void	Part::request(CloseReq& msg) noexcept{
	_closeReq	= &msg;
	msg.returnToSender();
	}

void	Part::request(OpenReq& msg) noexcept{
	msg.returnToSender();
	}

void	Part::request(	Oscl::Frame::Pdu::
						Tx::Req::Api::TxReq&	msg
						) noexcept{
	Oscl::Frame::Pdu::Tx::Req::Api::TxPayload&
	payload	= msg.getPayload();
	unsigned	len	= payload._pdu->length();
	unsigned	size;
	size	= _framer.open(len);
	if(!size || (size != len)){
		msg.returnToSender();
		return;
		}
	if(payload._pdu->visit(*this,0,0,len)){
		// FIXME:
		// Framer interface should include abort!
		_framer.close();
		msg.returnToSender();
		return;
		}
	_framer.close();
	msg.returnToSender();
	}

