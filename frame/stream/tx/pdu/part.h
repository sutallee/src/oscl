/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_frame_stream_tx_pdu_parth_
#define _oscl_frame_stream_tx_pdu_parth_
#include "oscl/pdu/tx/api.h"
#include "oscl/mt/itc/srv/closeapi.h"
#include "oscl/mt/itc/srv/close.h"
#include "oscl/frame/pdu/tx/reqapi.h"
#include "oscl/pdu/visitor.h"

/** */
namespace Oscl {
/** */
namespace Frame {
/** */
namespace Stream {
/** */
namespace TX {
/** */
namespace PDU {

/** This part is responsible for walking the scatter-gather buffers
	in the PDU and sending each to the framer.
 */
class Part :	private	Oscl::Mt::Itc::Srv::Close::Req::Api,
				private Oscl::Frame::Pdu::Tx::Req::Api,
				private Oscl::Pdu::ReadOnlyVisitor
				{
	private:
		/** */
		Oscl::Mt::Itc::PostMsgApi&		_myPapi;
		/** */
		Oscl::PDU::TX::Api&				_framer;
		/** */
		Oscl::Mt::Itc::Srv::
		Close::Req::Api::ConcreteSAP	_closeSAP;
		/** */
		Oscl::Frame::Pdu::
		Tx::Req::Api::ConcreteSAP		_framerSAP;
		/** */
		CloseReq*						_closeReq;

	public:
		/** */
		Part(	Oscl::Mt::Itc::PostMsgApi&	myPapi,
				Oscl::PDU::TX::Api&			framer
				) noexcept;

		/** */
		~Part();

		/** */
		Oscl::Mt::Itc::Srv::Close::Req::Api::SAP&	getCloseSAP() noexcept;

		/** */
		Oscl::Frame::Pdu::Tx::Req::Api::SAP&		getFrameTxSAP() noexcept;

	private: // Oscl::Pdu::ReadOnlyVisitor
		/** */
		bool /* stop */	accept(	const Oscl::Pdu::Composite&	p,
								unsigned					position
								) noexcept;

		/** */
		bool /* stop */	accept(	const Oscl::Pdu::ReadOnlyComposite&	p,
								unsigned							position
								) noexcept;

		/** */
		bool /* stop */ accept(	const Oscl::Pdu::Leaf&	p,
								unsigned				position,
								unsigned				offset,
								unsigned				length
								) noexcept;

		/** */
		bool /* stop */ accept(	const Oscl::Pdu::Fragment&	p,
								unsigned					position
								) noexcept;

		/** */
		bool /* stop */ accept(	const Oscl::Pdu::ReadOnlyFragment&	p,
								unsigned							position
								) noexcept;

	private:	// Oscl::Mt::Itc::Srv::Close::Req::Api
		/** */
		void	request(CloseReq& msg) noexcept;

	private:	// Oscl::Mt::Itc::Srv::Open::Req::Api
		/** */
		void	request(OpenReq& msg) noexcept;

	private:	// Oscl::Frame::Pdu::Tx::Req::Api
		/** */
		void	request(	Oscl::Frame::Pdu::
							Tx::Req::Api::TxReq&	msg
							) noexcept;

	};
}
}
}
}
}


#endif
