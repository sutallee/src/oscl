/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <new>
#include <string.h>
#include "part.h"
#include "oscl/error/fatal.h"
//#include "oscl/mt/mutex/iss.h"
#include "oscl/memory/align.h"
#include "oscl/cache/operations.h"
#include "oscl/error/info.h"

using namespace Oscl::Frame::Stream::RX::PDU;
using namespace Oscl;

Part::Part(	Oscl::Mt::Itc::PostMsgApi&			myPapi,
			Oscl::Pdu::FWD::Api&				fwdApi,
			CompositeMem						compositeMem[],
			unsigned							nCompositeMem,
			FragmentMem							fragmentMem[],
			FixedMem							fixedMem[],
			unsigned							nFragments,
			void*								bufferMem,
			unsigned							bufferSize
			) noexcept:
		_myPapi(myPapi),
		_fwdApi(fwdApi),
		_bufferSize(bufferSize),
		_fixedFSM(*this),
		_fragmentFSM(*this),
		_compositeFSM(*this),
		_bufferFSM(*this),
		_nFragments(nFragments),
		_nComposites(nCompositeMem),
		_nFreeBuffer(0),
		_nFreeFixed(0),
		_nFreeFragment(0),
		_nFreeComposite(0),
		_closeSAP(*this,myPapi),
		_closeReq(0),
		_currentComposite(0),
		_currentFragmentMem(0),
		_currentFixedMem(0),
		_currentBuffer(0)
		{
	if(bufferSize < sizeof(BufferMem)){
		Oscl::ErrorFatal::logAndExit(	"Oscl::Frame::Stream::RX::PDU::Part:"
										"buffer size too small.\n"
										);
		}
	if(bufferSize % OsclCacheGetLineSize()){
		Oscl::ErrorFatal::logAndExit(	"Oscl::Frame::Stream::RX::PDU::Part:"
										"buffer size not multiple of cache line size.\n"
										);
		}
	unsigned long	bufferMemAddress	= (unsigned long)bufferMem;
	if(bufferMemAddress % OsclCacheGetLineSize()){
		Oscl::ErrorFatal::logAndExit(	"Oscl::Frame::Stream::RX::PDU::Part:"
										"buffer memory not cache aligned.\n"
										);
		}

	unsigned char*	buff	= (unsigned char*)bufferMem;
	for(unsigned i=0;i<nFragments;++i,buff+=bufferSize){
		freeBuffer(buff);
		freeFixed(&fixedMem[i]);
		freeFragment(&fragmentMem[i]);
		}
	for(unsigned i=0;i<nCompositeMem;++i){
		freeComposite(&compositeMem[i]);
		}
	_bufferSize	= bufferSize;
	}

Part::~Part(){
	}

Oscl::Mt::Itc::Srv::Close::Req::Api::SAP&	Part::getCloseSAP() noexcept{
	return _closeSAP;
	}

void	Part::checkCloseComplete() noexcept{
	if(		(_nFreeBuffer == _nFragments)
		&&	(_nFreeFixed == _nFragments)
		&&	(_nFreeFragment == _nFragments)
		&&	(_nFreeComposite == _nComposites)
		){
		_closeReq->returnToSender();
		_closeReq	= 0;
		}
	}

void	Part::freeCurrentPduMem() noexcept{
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	if(_currentBuffer){
		freeBuffer(_currentBuffer);
//		_bufferPool.put((MemBlock*)_currentBuffer);
		_currentBuffer	= 0;
		}
	if(_currentFixedMem){
		freeFixed(_currentFixedMem);
//		_fixedPool.put(_currentFixedMem);
		_currentFixedMem	= 0;
		}
	if(_currentFragmentMem){
		freeFragment(_currentFragmentMem);
//		_fragmentPool.put(_currentFragmentMem);
		_currentFragmentMem	= 0;
		}
	_currentComposite	= 0;
	}

bool	Part::allocCurrentFragMem() noexcept{
	_currentBuffer		= allocBuffer();
	_currentFixedMem	= allocFixed();
	_currentFragmentMem	= allocFragment();
	if(!_currentBuffer || !_currentFixedMem || !_currentFragmentMem){
		Oscl::Error::Info::log(
			"%s: _currentBuffer %p, _currentFixedMem %p, _currentFragmentMem %p\n",
			__PRETTY_FUNCTION__,
			_currentBuffer,
			_currentFixedMem,
			_currentFragmentMem
			);
		freeCurrentPduMem();
		return false;
		}
	_currentLength		= 0;
	return true;
	}

void	Part::appendCurrentFragment() noexcept{
	if(!_currentComposite) return;
	if(!_currentBuffer) return;
	Fixed*	fixed	= new (_currentFixedMem)
						Fixed(	*this,
								_fixedFSM,
								_bufferFSM,
								_currentBuffer,
								_currentLength
								);
	Fragment*	frag	= new (_currentFragmentMem)
							Fragment(	*this,
										_fragmentFSM,
										fixed
										);
	_currentBuffer		= 0;
	_currentLength		= 0;
	_currentFixedMem	= 0;
	_currentFragmentMem	= 0;
	_currentComposite->append(frag);
	}

void		Part::open() noexcept{
	_currentComposite	=	allocComposite();
	if(!_currentComposite) {
		Oscl::Error::Info::log(
			"%s: !_currentComposite\n",
			__PRETTY_FUNCTION__
			);
		return;
		}
	if(!allocCurrentFragMem()) {
		Oscl::Error::Info::log(
			"%s: allocCurrentFragMem() failed\n",
			__PRETTY_FUNCTION__
			);
		_currentComposite	= 0;
		return;
		}
	}

unsigned	Part::append(unsigned char octet) noexcept{
	if(!_currentComposite){
		Oscl::Error::Info::log(
			"%s: !_currentComposite\n",
			__PRETTY_FUNCTION__
			);
		return 0;
		}
	if(!_currentBuffer){
		Oscl::Error::Info::log(
			"%s: !_currentBuffer\n",
			__PRETTY_FUNCTION__
			);
		return 0;
		}
	if(_currentLength >= _bufferSize){
		appendCurrentFragment();
		if(!allocCurrentFragMem()){
			Oscl::Error::Info::log(
				"%s: allocCurrentFragMem() failed\n",
				__PRETTY_FUNCTION__
				);
			_currentComposite	= 0;
			return 0;
			}
		}
	_currentBuffer[_currentLength]	= octet;
	++_currentLength;
	return 1;
	}

unsigned	Part::append(	const void*	octets,
							unsigned	nOctets
							) noexcept{
	if(!_currentComposite){
		Oscl::Error::Info::log(
			"%s: !_currentComposite\n",
			__PRETTY_FUNCTION__
			);
		return 0;
		}
	unsigned				offset	= 0;
	const unsigned char*	p		= (const unsigned char*)octets;
	while(nOctets){
		unsigned	remaining	= _bufferSize-_currentLength;
		if(!remaining){
			appendCurrentFragment();
			if(!allocCurrentFragMem()){
				Oscl::Error::Info::log(
					"%s: allocCurrentFragMem() failed\n",
					__PRETTY_FUNCTION__
					);
				_currentComposite	= 0;
				return offset;
				}
			remaining	= _bufferSize;
			}
		unsigned	length		= nOctets;
		if(remaining < length){
			length	= remaining;
			}
		memcpy(	&_currentBuffer[_currentLength],
				&p[offset],
				length
				);
		offset			+= length;
		_currentLength	+= length;
		nOctets			-= length;
		}
	return offset;
	}

void		Part::close(unsigned nFcsOctets) noexcept{
	appendCurrentFragment();
	_currentComposite->stripTrailer(nFcsOctets);
	_fwdApi.transfer(_currentComposite);
	_currentComposite	= 0;
	}

void		Part::abort() noexcept{
	freeCurrentPduMem();
	}

uint8_t*	Part::allocBuffer() noexcept{
	uint8_t*	buffer	= (uint8_t*)_bufferPool.get();
	Oscl::Error::Info::log(
		"%s: buffer %p\n",
		__PRETTY_FUNCTION__,
		buffer
		);
	if(buffer){
		--_nFreeBuffer;
		}
	return buffer;
	}

void	Part::freeBuffer(void *buff) noexcept{
#if 0
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
#endif
	_bufferPool.put((BufferMem*)buff);
	++_nFreeBuffer;
	if(_closeReq){
		checkCloseComplete();
		}
	}

void	Part::freeFixed(void *fixed) noexcept{
#if 0
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
#endif
	// Do not delete!
	_fixedPool.put((FixedMem*)fixed);
	++_nFreeFixed;
	if(_closeReq){
		checkCloseComplete();
		}
	}

void	Part::freeFragment(void *fragment) noexcept{
#if 0
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
#endif
	// Do not delete!
	_fragmentPool.put((FragmentMem*)fragment);
	++_nFreeFragment;
	if(_closeReq){
		checkCloseComplete();
		}
	}

void	Part::freeComposite(void *composite) noexcept{
#if 0
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
#endif
	// Do not delete!
	_compositePool.put((CompositeMem*)composite);
	++_nFreeComposite;
	if(_closeReq){
		checkCloseComplete();
		}
	}

FixedMem*	Part::allocFixed() noexcept{
	FixedMem*	mem	= _fixedPool.get();
	Oscl::Error::Info::log(
		"%s: mem %p\n",
		__PRETTY_FUNCTION__,
		mem
		);
	if(!mem) return 0;
	--_nFreeFixed;
	return mem;
	}

FragmentMem*	Part::allocFragment() noexcept{
	FragmentMem*	mem		= _fragmentPool.get();
	Oscl::Error::Info::log(
		"%s: mem %p\n",
		__PRETTY_FUNCTION__,
		mem
		);
	if(!mem) {
		return 0;
		}
	--_nFreeFragment;
	return mem;
	}

Composite*	Part::allocComposite() noexcept{
	CompositeMem*	mem	= _compositePool.get();
	Oscl::Error::Info::log(
		"%s: mem %p\n",
		__PRETTY_FUNCTION__,
		mem
		);
	if(!mem) return 0;
	--_nFreeComposite;
	return new (mem) Composite(*this,_compositeFSM);
	}

void	Part::returnBuffer(BufferMsg* buffer) noexcept{
	Oscl::Error::Info::log(
		"%s: %p\n",
		__PRETTY_FUNCTION__,
		buffer
		);
	freeBuffer(buffer);
	}

void	Part::returnFixed(FixedMsg* fixed) noexcept{
	Oscl::Error::Info::log(
		"%s: %p\n",
		__PRETTY_FUNCTION__,
		fixed
		);
	freeFixed(fixed);
	}

void	Part::returnFragment(FragmentMsg* fragment) noexcept{
	Oscl::Error::Info::log(
		"%s: %p\n",
		__PRETTY_FUNCTION__,
		fragment
		);
	freeFragment(fragment);
	}

void	Part::returnComposite(CompositeMsg* composite) noexcept{
	Oscl::Error::Info::log(
		"%s: %p\n",
		__PRETTY_FUNCTION__,
		composite
		);
	freeComposite(composite);
	}

void	Part::free(Fixed* fso) noexcept{
#if 0
	Oscl::Error::Info::log(
		"%s: %p\n",
		__PRETTY_FUNCTION__,
		fso
		);
#endif
	FixedMsg*	msg	= new(fso) FixedMsg(*this);
	_myPapi.post(*msg);
	}

void	Part::free(Fragment* fso) noexcept{
#if 0
	Oscl::Error::Info::log(
		"%s: %p\n",
		__PRETTY_FUNCTION__,
		fso
		);
#endif
	FragmentMsg*	msg	= new(fso) FragmentMsg(*this);
	_myPapi.post(*msg);
	}

void	Part::free(Composite* fso) noexcept{
#if 0
	Oscl::Error::Info::log(
		"%s: %p\n",
		__PRETTY_FUNCTION__,
		fso
		);
#endif
	CompositeMsg*	msg	= new(fso) CompositeMsg(*this);
	_myPapi.post(*msg);
	}

void	Part::free(uint8_t* fso) noexcept{
#if 0
	Oscl::Error::Info::log(
		"%s: %p\n",
		__PRETTY_FUNCTION__,
		fso
		);
#endif
	BufferMsg*	msg	= new(fso) BufferMsg(*this);
	_myPapi.post(*msg);
	}

void	Part::request(CloseReq& msg) noexcept{
	// FIXME:
	// All of the following must be recovered
	// before allowing the close request to
	// complete:
	// o PDU Composites
	// o PDU Fragments
	// o PDU Fixed
	// o Buffers
	_closeReq	= &msg;
	freeCurrentPduMem();
	checkCloseComplete();
	}

void	Part::request(OpenReq& msg) noexcept{
	// Nothing to do here.
	msg.returnToSender();
	}

