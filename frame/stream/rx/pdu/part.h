/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_frame_stream_rx_pdu_parth_
#define _oscl_frame_stream_rx_pdu_parth_
#include "buffer.h"
#include "fixed.h"
#include "frag.h"
#include "composite.h"
#include "fsm.h"
#include "oscl/mt/itc/srv/closeapi.h"
#include "oscl/mt/itc/srv/close.h"
#include "oscl/pdu/fwd/api.h"
#include "oscl/frame/stream/rx/api.h"

/** */
namespace Oscl {
/** */
namespace Frame {
/** */
namespace Stream {
/** */
namespace RX {
/** */
namespace PDU {

/** This sub-service is responsible for managing the buffer
	resources used to build PDUs from an input stream.
 */
class Part :	private	Oscl::Mt::Itc::Srv::Close::Req::Api,
				public	Oscl::Frame::Stream::RX::Api,
				private Oscl::Frame::Stream::RX::PDU::FreeStoreApi
				{
	private:
		/** */
		friend class BufferMsg;
		/** */
		friend class FixedMsg;
		/** */
		friend class FragmentMsg;
		/** */
		friend class CompositeMsg;
		/** */
		friend class FreeStoreMgr<uint8_t>;
		/** */
		friend class FreeStoreMgr<Fixed>;
		/** */
		friend class FreeStoreMgr<Fragment>;
		/** */
		friend class FreeStoreMgr<Composite>;
		/** */
		enum {_minBufferSize=sizeof(BufferMem)};

	private:
		/** */
		Oscl::Mt::Itc::PostMsgApi&	_myPapi;
		/** */
		Oscl::Pdu::FWD::Api&		_fwdApi;
		/** */
		void*						_rawBufferMemory;
		/** */
		unsigned					_bufferSize;
		/** */
		Oscl::Queue<BufferMem>		_bufferPool;
		/** */
		Oscl::Queue<FixedMem>		_fixedPool;
		/** */
		Oscl::Queue<FragmentMem>	_fragmentPool;
		/** */
		Oscl::Queue<CompositeMem>	_compositePool;
		/** */
		FreeStoreMgr<Fixed>			_fixedFSM;
		/** */
		FreeStoreMgr<Fragment>		_fragmentFSM;
		/** */
		FreeStoreMgr<Composite>		_compositeFSM;
		/** */
		FreeStoreMgr<uint8_t>			_bufferFSM;
		/** */
		const unsigned				_nFragments;
		/** */
		const unsigned				_nComposites;
		/** */
		unsigned					_nFreeBuffer;
		/** */
		unsigned					_nFreeFixed;
		/** */
		unsigned					_nFreeFragment;
		/** */
		unsigned					_nFreeComposite;
		/** */
		Oscl::Mt::Itc::Srv::
		Close::Req::Api::ConcreteSAP		_closeSAP;

		/** */
		CloseReq*							_closeReq;

		/** */
		Oscl::Handle<Oscl::Pdu::Composite>	_currentComposite;
		/** */
		FragmentMem*						_currentFragmentMem;
		/** */
		FixedMem*							_currentFixedMem;
		/** */
		uint8_t*								_currentBuffer;
		/** */
		unsigned							_currentLength;

	public:
		/** "nFragments" represents the number of elements
			in the fixedMem[] and fragmentMem[] arrays, and
			also represents the number of buffers of size
			"bufferSize" that are contained in "bufferMem".
			
			"bufferMem" constraints:
				o Must be cache aligned
				o Size must be at least sizeof(BufferMem)
				o Size must be even multiple of cache alignment
				o The number of buffers must be the same as
				o The number of bytes in the memory pointed
					to by "bufferMem" must be at least 
					bufferSize*nFragments bytes.
			If any of these constraints are violated, a run-time
			error will happen during the execution of this
			constructor.
		 */
		Part(	Oscl::Mt::Itc::PostMsgApi&			myPapi,
				Oscl::Pdu::FWD::Api&				fwdApi,
				CompositeMem						compositeMem[],
				unsigned							nCompositeMem,
				FragmentMem							fragmentMem[],
				FixedMem							fixedMem[],
				unsigned							nFragments,
				void*								bufferMem,
				unsigned							bufferSize
				) noexcept;

		/** */
		~Part();

		/** */
		Oscl::Mt::Itc::Srv::Close::Req::Api::SAP&	getCloseSAP() noexcept;

	private:
		/** */
		void	checkCloseComplete() noexcept;

		/** */
		bool	allocCurrentFragMem() noexcept;

		/** */
		void	freeCurrentPduMem() noexcept;

		/** */
		void	appendCurrentFragment() noexcept;

	public: // Oscl::Frame::Stream::RX::Api
		/** This operation is invoked when a new frame is being received.
			This operation may be used by the implementation to allocate
			resources for the incomming frame.
		 */
		void		open() noexcept;

		/** This operation is invoked to append a single octet to
			the open frame. One is returned if the octet is successfuly
			appended. Zero is returned if insufficient resources are
			available to hold the frame, in which case the client
			may elect to abort() the frame.
		 */
		unsigned	append(unsigned char octet) noexcept;

		/** This operation is invoked to append the specified number
			of octets to the open frame. The number of octets actually
			appended are returned. If the return value is less than
			the number of octets specified by the client, there
			were insufficient resources available to contain the frame,
			and the client should then abort() the frame.
		 */
		unsigned	append(	const void*	octets,
									unsigned	nOctets
									) noexcept;

		/** This operation is invoked when the last octet in the currently
			open frame has been received and has been determined to be error
			free. The client specifies the number of trailer octets that
			are FCS octets. The implementation may subsequently strip
			the FCS octets from the frame.
		 */
		void		close(unsigned nFcsOctets) noexcept;

		/** This operation is invoked instead of the close() when the
			currently open frame contains an error.
		 */
		void		abort() noexcept;

	private:	// allocators
		/** */
		uint8_t*			allocBuffer() noexcept;
		/** */
		FixedMem*		allocFixed() noexcept;
		/** */
		FragmentMem*	allocFragment() noexcept;
		/** */
		Composite*		allocComposite() noexcept;

	private:	// deallocation
		/** */
		void	freeBuffer(void *buffer) noexcept;
		/** */
		void	freeFragment(void* fragment) noexcept;
		/** */
		void	freeFixed(void* fixed) noexcept;
		/** */
		void	freeComposite(void* composite) noexcept;

	private:	// deallocation
		/** */
		void	returnBuffer(BufferMsg* buff) noexcept;
		/** */
		void	returnFixed(FixedMsg* fixed) noexcept;
		/** */
		void	returnFragment(FragmentMsg* fragment) noexcept;
		/** */
		void	returnComposite(CompositeMsg* composite) noexcept;

	private:	// Oscl::Frame::Stream::RX::PDU::FreeStoreApi
		/** */
		void	free(Fragment* fragment) noexcept;
		/** */
		void	free(Fixed* fixed) noexcept;
		/** */
		void	free(Composite* composite) noexcept;
		/** */
		void	free(uint8_t* buffer) noexcept;

	private:	// Oscl::Mt::Itc::Srv::Close::Req::Api
		/** */
		void	request(CloseReq& msg) noexcept;

	private:	// Oscl::Mt::Itc::Srv::Open::Req::Api
		/** */
		void	request(OpenReq& msg) noexcept;
	};
}
}
}
}
}


#endif
