/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_frame_stream_rx_pdu_fsmh_
#define _oscl_frame_stream_rx_pdu_fsmh_
#include "oscl/freestore/manager.h"
#include "frag.h"
#include "fixed.h"
#include "composite.h"
#include <stdint.h>

/** */
namespace Oscl {
/** */
namespace Frame {
/** */
namespace Stream {
/** */
namespace RX {
/** */
namespace PDU {

class Part;

class FreeStoreApi {
	public:
		/** */
		virtual void	free(class Fragment* fragment) noexcept=0;

		/** */
		virtual void	free(class Fixed* fixed) noexcept=0;

		/** */
		virtual void	free(class Composite* composite) noexcept=0;

		/** */
		virtual void	free(uint8_t* buffer) noexcept=0;
	};

/** */
template <class Type>
class FreeStoreMgr : public Oscl::FreeStore::Mgr {
	private:
		/** */
		FreeStoreApi&	_rx;
	public:
		/** */
		FreeStoreMgr(FreeStoreApi& rx) noexcept:_rx(rx){}

	private: // FreeStore::Mgr
		/** */
		void	free(void* fso) noexcept{
			_rx.free((Type*)fso);
			}
	};

}
}
}
}
}


#endif
