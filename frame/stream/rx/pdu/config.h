/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_frame_stream_rx_pdu_configh_
#define _oscl_frame_stream_rx_pdu_configh_
#include "part.h"

/** */
namespace Oscl {
/** */
namespace Frame {
/** */
namespace Stream {
/** */
namespace RX {
/** */
namespace PDU {

/** This template is responsible for the proper static
	allocation of the resources required for the
	corresponding part.
 */
template<	unsigned	nCompositeMem,
			unsigned	nFragments,
			unsigned	bufferSize
			>
class Config {
	private:
		/** */
		union BuffMem {
			/** */
			Oscl::Memory::AlignedBlock<bufferSize>	mem;
			};
	private:
		/** */
		CompositeMem	compositeMem[nCompositeMem];
		/** */
		FragmentMem		fragmentMem[nFragments];
		/** */
		FixedMem		fixedMem[nFragments];
		/** */
		BuffMem			bufferMem[nFragments];

	public:
		/** */
		Part			_part;

	public:
		/** */
		Config(	Oscl::Mt::Itc::PostMsgApi&			myPapi,
				Oscl::Pdu::FWD::Api&				fwdApi
				) noexcept:
			_part(	myPapi,
					fwdApi,
					compositeMem,
					nCompositeMem,
					fragmentMem,
					fixedMem,
					nFragments,
					bufferMem,
					sizeof(BuffMem)
					){}
	};
}
}
}
}
}

#endif
