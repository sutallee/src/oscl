/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_frame_fwd_itc_reqapih_
#define _oscl_frame_fwd_itc_reqapih_

#include "oscl/mt/itc/mbox/srvreq.h"
#include "oscl/mt/itc/postmsgapi.h"
#include "oscl/mt/itc/mbox/sap.h"

/** */
namespace Oscl {

/** */
namespace Frame {

/** */
namespace FWD {

/** */
namespace Req {

/**	This class describes an ITC server interface. The
	interface includes the definition of messages and their
	associated payloads, as well as an interface that is to be
	implemented by a server that supports this ITC interface.
 */
/**	
 */
class Api {
	public:
		/** This class contains the data that is transfered
			between the client and server within the ForwardReq
			ITC message.
		 */
		class ForwardPayload {
			public:
				/**	This is a pointer to the buffer that contains the frame.
				 */
				const void*	_frame;

				/**	This is the number of octets contained within the buffer
					referenced by frame.
				 */
				unsigned int	_length;

				/**	This field is returned by the implementation. The
					implementation is expected to return true if it recognizes
					the frame type and processes it. This mechanism *may* be
					used by the client to halt iteration of a list of interfaces
					of this type.
				 */
				bool	_result;

			public:
				/** The ForwardPayload constructor. */
				ForwardPayload(

					const void*	frame,
					unsigned int	length
					) noexcept;

			};

		/**	This message is used to forward by clients that wish to
			allow another layer to examine and perhaps copy some or all
			of a single frame.
		 */
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::Frame::FWD::Req::Api,
					ForwardPayload
					>		ForwardReq;

	public:
		/** This class contains the data that is transfered
			between the client and server within the CancelForwardReq
			ITC message.
		 */
		class CancelForwardPayload {
			public:
				ForwardReq&	_requestToCancel;

			public:
				/** The CancelForwardPayload constructor. */
				CancelForwardPayload(ForwardReq& requestToCancel) noexcept;
			};

		/**	This is a message used to cancel a ForwardReq.
		 */	
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::Frame::FWD::Req::Api,
					CancelForwardPayload
					>		CancelForwardReq;

	public:
		/** This type defines a SAP (Service Access Point) for this
			ITC server interface. A SAP holds server address information
			that is required by clients to send messages to a server.
		 */
		typedef Oscl::Mt::Itc::SAP<	Oscl::Frame::FWD::
							Req::Api
							>			SAP;

	public:
		/** This type defines a SAP (Service Access Point) for this
			ITC server interface. A SAP holds server address information
			that is required by clients to send messages to a server.
		 */
		typedef Oscl::Mt::Itc::ConcreteSAP<	Oscl::Frame::FWD::
							Req::Api
							>			ConcreteSAP;

	public:
		/** Make the compiler happy. */
		virtual ~Api(){}

		/** This operation is invoked within the server thread
			whenever a ForwardReq is received.
		 */
		virtual void	request(	Oscl::Frame::FWD::
									Req::Api::ForwardReq& msg
									) noexcept=0;

		/** This operation is invoked within the server thread
			whenever a CancelForwardReq is received.
		 */
		virtual void	request(	Oscl::Frame::FWD::
									Req::Api::CancelForwardReq& msg
									) noexcept=0;

	};

}
}
}
}
#endif
