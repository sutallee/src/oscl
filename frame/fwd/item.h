/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_frame_fwd_itemh_
#define _oscl_frame_fwd_itemh_
#include "oscl/queue/queueitem.h"

/** */
namespace Oscl {

/** */
namespace Frame {

/** */
namespace FWD {

/**	This class defines the operations that can be performed
	procedurally by the client.
 */
class Item:
	public Oscl::QueueItem,
	public Api
	{
#if 0
	public:
		/**	This operation is used to forward a frame contained in a
			single buffer to another layer. The implementation is
			expected to examine and possibly copy the frame, as the
			caller will reclaim the buffer after the transfer returns.
			RETURN: The implementation is expected to return true if
			it recognizes the frame type and processes it. This *may*
			be used by the client to halt iteration of a list of
			interfaces of this type.
		 */
		virtual bool	forward(	
							const void*	frame,
							unsigned int	length
							) noexcept=0;
#endif
	};

}
}
}
#endif
