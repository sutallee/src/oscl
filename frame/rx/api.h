/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_frame_rx_apih_
#define _oscl_frame_rx_apih_

/** */
namespace Oscl {
/** */
namespace Frame {
/** */
namespace Rx {

/** This interface indicates to the implementation the addressing
	conditions under which the current frame was received.
 */
class AddressTypeApi {
	public:
		/** */
		virtual ~AddressTypeApi() {}

	public:
		/** The destination MAC address contained in the received
			frame exactly matched the unique address of this station.
		 */
		virtual void	stationAddress() noexcept=0;
		/** The destination MAC address contained in the received
			frame exactly matched the broadcast address.
		 */
		virtual void	broadcastAddress() noexcept=0;
		/** The destination MAC address contained in the received
			frame matched one of the alternate (e.g. multicast)
			addresses for this station.
		 */
		virtual void	alternateAddress() noexcept=0;
		/** The destination MAC address contained in the received
			frame matched none of the following:
				- unique station address
				- broadcast address
				- any alternate (e.g. multicast) address
			Nor was the frame received as the result of promiscuous
			receiver operation.
			This operation is typically only invoked as the result
			of alternalte (e.g. multicast) address hash table aliasing.
			The implementation should discard the frame except
			when debugging.
		 */
		virtual void	alternateAddressAlias() noexcept=0;
		/** The destination MAC address contained in the received
			frame did not match the unique station address, the
			broadcast address, or any alternate (e.g. multicast)
			address, and the receiver is operating in promiscuous mode.
		 */
		virtual void	promiscuousAddress() noexcept=0;
	};

/** This interface describes the operations for a packet/frame receiver
	capable of scatter/gather buffer management. Each received frame
	is composed of one or more buffers buffers. The client uses this
	interface to retrieve the sequence of buffers from the receiver.
	Initialization of the receiver begins by giving some number of
	buffers all of the same size to the receiver. An indication of
	frame reception (outside of the scope of this interface) then
	results in the client performing the following typical sequence:
		1. Open the next frame.
		2. Get each buffer of the frame.
		3. Close the frame.
 */
class Api {
	public:
		/** */
		virtual ~Api() {}

	public: // Initialization Operations
		/** This operation returns the minimum buffer size that can be
			accepted by this receiver.
		 */
		virtual unsigned	minimumBufferSize() noexcept=0;

		/** This operation returns the largest size buffer that can
			be used by this receiver.
		 */
		virtual unsigned	maximumBufferSize() noexcept=0;

		/** This operation is used during initialization to give empty
			buffers to the receiver. Buffers must meet the minimum
			and maximum buffer size requirements and must be long word
			aligned. Size requirements can be obtained via minimumBufferSize()
			and maximumBufferSize(). Failure to meet the buffer requirements
			is undefined, but implementations should invoke a system
			error handler.
		 */
		virtual void	supplyBuffer(void* buffer,unsigned size) noexcept=0;

	public:	// Operational Operations
		/** This operation is invoked to begin processing of the
			next frame in the receive queue. The operation
			initializes the internal state of the receiver to
			prepare for processing the buffers contained in the
			frame. The operation counts the number of buffers
			in the current frame and returns the number as a
			result. If there is no complete frame pending, then
			the operation returns zero.
		 */
		virtual unsigned	open() noexcept=0;

		/** This operation returns true if the framer is currently
			receiving promiscuously.
		 */
		virtual bool	operatingPromiscuously() noexcept=0;

		/** This operation returns true if the destination address
			of the currently open frame *exactly* matches either the
			individual station address, broadcast address, or one of the
			group addresses for the framer interface.
		 */
		virtual bool	exactAddressMatch() noexcept=0;

		/** This operation returns true if the currently open frame
			did not match any individual, broadcast, or group address
			and the framer is operating promiscuously.
		 */
		virtual bool	promiscuousFrame() noexcept=0;

		/** This operation retrieves the next buffer in the currently
			opened frame and the number of valid octets in the buffer.
			If there are no further buffers in the current frame, the
			operation returns zero.
		 */
		virtual unsigned	nextBuffer(void** buffer) noexcept=0;

		/** This operation indicates to the receiver that the client
			has completed processing the currently open frame. The
			implementation is then free to reclaim the resources
			used by the frame, and ownership of the buffers is
			relinquished to the client, which is then responsible
			for returning the buffers to the receiver when it is
			finished using them.
		 */
		virtual void	close() noexcept=0;

		/** This operation indicates to the receiver that the client
			has decided to discard the contents of the currently
			open frame. The implementation reclaims all of the resources
			associated with the frame and frees the buffers to be used
			in subsequent frames. This operation is used in lieu of
			the close() operation.
		 */
		virtual void	discard() noexcept=0;

		/** This operation is invoked to close the current frame without
			removing it from the receive queue. The implementation retains
			ownership of the receive buffers associated with this frame.
			This operation may be used in lieu of the close() operation
			when the client does not immediately have sufficient resources
			to receive the frame.
		 */
		virtual void	cancel() noexcept=0;

		/** This operation is used during operation to return buffers
			to the receiver when the client has finished using them.
		 */
		virtual void	freeBuffer(void* buffer) noexcept=0;
	};

}
}
}

#endif
