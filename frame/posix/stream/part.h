/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_frame_posix_rx_parth_
#define _oscl_frame_posix_rx_parth_

#include "oscl/posix/select/trans/read.h"
#include "oscl/posix/select/trans/write.h"
#include "oscl/done/operation.h"
#include "oscl/memory/block.h"
#include "oscl/strings/fixed.h"
#include "oscl/stream/framer/rx/api.h"

#include "oscl/stream/framer/line/config.h"
#include "oscl/print/api.h"

#include "oscl/stream/write/api.h"

/** */
namespace Oscl {

/** */
namespace Frame {

/** */
namespace Posix {

/** */
namespace Stream {

/** */
class Part : public Oscl::Stream::Write::Api {
	private:
		/** */
		Oscl::Posix::Select::Api&			_posixSelectApi;

		/** */
		Oscl::Stream::Framer::RX::Api&		_framer;

		/** */
		Oscl::Done::Api&					_writeComplete;

		/** */
		int									_fd;

	private:
		/** */
		Oscl::Memory::AlignedBlock<
			sizeof(
				Oscl::Posix::Select::
				Transaction::Read
				)
			>								_readMem;

		/** */
		Oscl::Done::Operation<Part>		_readSuccess;

		/** */
		Oscl::Done::Operation<Part>		_readFailed;

		/** */
		unsigned char						_readBuffer[128];

		/** */
		Oscl::Posix::Select::
		Transaction::Read*					_readTrans;

	private:
		/** */
		Oscl::Memory::AlignedBlock<
			sizeof(
				Oscl::Posix::Select::
				Transaction::Write
				)
			>								_writeMem;

		/** */
		Oscl::Done::Operation<Part>			_writeSuccess;

		/** */
		Oscl::Done::Operation<Part>			_writeFailed;

		/** */
		Oscl::Posix::Select::
		Transaction::Write*					_writeTrans;

	private:
		/** */
		Oscl::Done::Api*					_stopDone;

	public:
		/** */
		Part(
			Oscl::Posix::Select::Api&		posixSelectApi,
			Oscl::Stream::Framer::RX::Api&	framer,
			Oscl::Done::Api&				writeComplete,
			int								fd
			) noexcept;

		/** */
		void start() noexcept;

		/** */
		void stop(Oscl::Done::Api& done) noexcept;

	public: // Oscl::Stream::Write::Api
		/** Returns true if a write transaction
			is in progress. Attempts by the client
			to write more data will fail.
			Rather than poll, the client can wait for
			the writeComplete callback.
		 */
		bool	writeBusy() const noexcept;

		/** 
			Begin write of length octets from data to the
			file-descriptor.
			RETURN:
				true if busy. No data is written.
				false if the data is queued. In this case,
				the buffer must not be used by the client
				until the write completes.
		 */
		bool	write(const void* data, unsigned length) noexcept;

	private:
		/** */
		void	stopAll() noexcept;

		/** */
		void	startRead() noexcept;

		/** */
		void	stopNext() noexcept;

	private:
		/** */
		void	readSuccess() noexcept;

		/** */
		void	readFailed() noexcept;

	private:
		/** */
		void	writeSuccess() noexcept;

		/** */
		void	writeFailed() noexcept;
	};
}
}
}
}

#endif
