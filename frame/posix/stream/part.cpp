/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <new>
#include "part.h"
#include "oscl/error/info.h"

using namespace Oscl::Frame::Posix::Stream;

Part::Part(
	Oscl::Posix::Select::Api&		posixSelectApi,
	Oscl::Stream::Framer::RX::Api&	framer,
	Oscl::Done::Api&				writeComplete,
	int								fd
	) noexcept:
		_posixSelectApi(posixSelectApi),
		_framer(framer),
		_writeComplete(writeComplete),
		_fd(fd),
		_readSuccess(
			*this,
			&Part::readSuccess
			),
		_readFailed(
			*this,
			&Part::readFailed
			),
		_readTrans(0),
		_writeSuccess(
			*this,
			&Part::writeSuccess
			),
		_writeFailed(
			*this,
			&Part::writeFailed
			),
		_writeTrans(0),
		_stopDone(0)
		{
	}

void	Part::start() noexcept{
	startRead();
	}

void	Part::stop(Oscl::Done::Api& done) noexcept{
	_stopDone	= &done;
	stopAll();
	}

void	Part::stopAll() noexcept{
	stopNext();
	}

void	Part::stopNext() noexcept{

	if(_readTrans){
		_posixSelectApi.detach(*_readTrans);
		}

	if(_fd > -1){
		_fd	= -1;
		}

	Oscl::Done::Api&
	stopDone	= *_stopDone;
	_stopDone	= 0;
	stopDone.done();
	}

void	Part::startRead() noexcept{

	if(_readTrans){
		return;
		}

	_readTrans =	new (&_readMem)
		Oscl::Posix::Select::Transaction::Read (
			_fd,
			_readSuccess,
			_readFailed,
			_readBuffer,
			sizeof(_readBuffer),
			true
			);

	_readTrans->start(_posixSelectApi);
	}

void	Part::readSuccess() noexcept{

	unsigned	nRead	= _readTrans->_nRemainingToRead;

	_readTrans	= 0;

	if(_stopDone){
		return;
		}

	_framer.input(_readBuffer,nRead);

	startRead();
	}

void	Part::readFailed() noexcept{
	_readTrans	= 0;

	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);

	if(!_stopDone){
		stopAll();
		}
	}

bool	Part::writeBusy() const noexcept{
	return _writeTrans?true:false;
	}

bool	Part::write(const void* data, unsigned length) noexcept{
	if(_writeTrans){
		return true;
		}

	_writeTrans =	new (&_writeMem)
		Oscl::Posix::Select::Transaction::Write (
			_fd,
			_writeSuccess,
			_writeFailed,
			data,
			length
			);

	_writeTrans->start(_posixSelectApi);

	return false;
	}

void	Part::writeSuccess() noexcept{
	_writeTrans	= 0;
	_writeComplete.done();
	}

void	Part::writeFailed() noexcept{
	_writeTrans	= 0;

	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);

	_writeComplete.done();
	}

