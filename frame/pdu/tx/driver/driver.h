/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_frame_pdu_tx_driver_driverh_
#define _oscl_frame_pdu_tx_driver_driverh_
#include "oscl/mt/itc/mbox/server.h"
#include "oscl/frame/tx/api.h"
#include "oscl/mt/itc/srv/closeapi.h"
#include "oscl/mt/itc/srv/close.h"
#include "oscl/frame/pdu/tx/reqapi.h"
#include "oscl/pdu/visitor.h"
#include "oscl/memory/block.h"

/** */
namespace Oscl {
/** */
namespace Frame {
/** */
namespace Pdu {
/** */
namespace Tx {

class Driver;

/** */
class TxDone :	public Oscl::Done::Api,
				public Oscl::Pdu::ReadOnlyVisitor
	{
	private:
		/** */
		Driver&					_context;
		/** */
		Oscl::Frame::Tx::Api&	_frameTx;
		/** */
		Req::Api::TxReq&		_req;
	public:
		/** */
		TxDone(	Driver&					context,
				Oscl::Frame::Tx::Api&	frameTx,
				Req::Api::TxReq&		req
				) noexcept;

		/** Returns false if resources not available. */
		bool	startFrame() noexcept;

	private:	// Oscl::Done::Api
		/** */
		void	done() noexcept;

	private:	// Oscl::Pdu::ReadOnlyVisitor
		/** */
		bool	accept(	const Oscl::Pdu::Composite&	p,
						unsigned					position
						) noexcept;
		/** */
		bool	accept(	const Oscl::Pdu::ReadOnlyComposite&	p,
						unsigned							position
						) noexcept;
		/** */
		bool	accept(	const Oscl::Pdu::Leaf&	p,
						unsigned				position,
						unsigned				offset,
						unsigned				length
						) noexcept;
		/** */
		bool	accept(	const Oscl::Pdu::ReadOnlyFragment&	p,
						unsigned							position
						) noexcept;
		/** */
		bool	accept(	const Oscl::Pdu::Fragment&	p,
						unsigned					position
						) noexcept;
	};

/** */
class Driver :	public Oscl::Mt::Itc::Server,
				public Oscl::Mt::Itc::Srv::CloseSync,
				private Oscl::Mt::Itc::Srv::Close::Req::Api,
				private Req::Api
				{
	private:
		/** */
		friend class TxDone;
	private:
		/** */
		typedef Oscl::Memory::AlignedBlock<sizeof(TxDone)>	TxDoneMem;
		/** */

	private:
		/** */
		Oscl::Frame::Tx::Api&							_frameTx;
		/** */
		void*											_rawBufferMemory;
		/** */
		Oscl::Mt::Itc::Srv::Close::Req::Api::ConcreteSAP	_closeSAP;
		/** */
		Req::Api::ConcreteSAP							_txSAP;
		/** */
		Oscl::Queue<TxDoneMem>							_txDoneList;
		/** */
		Oscl::Queue<Req::Api::TxReq>					_pendingList;

	public:
		/** Allocates memory when created. */
		Driver(	Oscl::Frame::Tx::Api&		frameTx,
				unsigned 					nTxCompletes
				) noexcept;

		/** */
		~Driver();

		/** */
		Oscl::Mt::Itc::Srv::Close::Req::Api::SAP&	getCloseSAP() noexcept;
		/** */
		Req::Api::SAP&							getTxSAP() noexcept;

	private:
		/** */
		void	initialize() noexcept;
		/** */
		void	mboxSignaled() noexcept;

	private:	// Oscl::Mt::Itc::Srv::Close::Req::Api
		/** */
		void	request(CloseReq& msg) noexcept;

	private:	// Oscl::Mt::Itc::Srv::Open::Req::Api
		/** */
		void	request(OpenReq& msg) noexcept;

	private:	// Req::Api
		void	request(Req::Api::TxReq& msg) noexcept;

	private:
		void	done(TxDone& done) noexcept;
	};
}
}
}
}


#endif
