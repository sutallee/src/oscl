/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <new>
#include <stdlib.h>
#include "driver.h"
#include "oscl/error/fatal.h"
#include "oscl/pdu/leaf.h"

using namespace Oscl::Frame::Pdu::Tx;

TxDone::TxDone(	Driver&					context,
				Oscl::Frame::Tx::Api&	frameTx,
				Req::Api::TxReq&		req
				) noexcept:
		_context(context),
		_frameTx(frameTx),
		_req(req)
		{
	}

bool	TxDone::startFrame() noexcept{
	if(!_frameTx.open()) return false;
	unsigned	len	= _req.getPayload()._pdu->length();
	if(_req.getPayload()._pdu->visit(*this,0,0,len)){
		// failed allocation
		_frameTx.cancel();
		return false;
		}
	_frameTx.close(*this);
	return true;
	}

void	TxDone::done() noexcept{
	_req.returnToSender();
	_context.done(*this);
	}

bool	TxDone::accept(	const Oscl::Pdu::Composite&	p,
						unsigned					position
						) noexcept{
	return false;
	}

bool	TxDone::accept(	const Oscl::Pdu::ReadOnlyComposite&	p,
						unsigned							position
						) noexcept{
	return false;
	}

bool	TxDone::accept(	const Oscl::Pdu::Leaf&	p,
						unsigned				position,
						unsigned				offset,
						unsigned				length
						) noexcept{
	unsigned	actualLength;
	const void*	buffer	= p.getBuffer(offset,actualLength);
	if(!buffer){
		Oscl::ErrorFatal::
		logAndExit("Oscl::Frame::Pdu::Tx::Driver: buffer error\n");
		}
	return !_frameTx.append(buffer,length);
	}

bool	TxDone::accept(	const Oscl::Pdu::ReadOnlyFragment&	p,
						unsigned							position
						) noexcept{
	return false;
	}

bool	TxDone::accept(	const Oscl::Pdu::Fragment&	p,
						unsigned					position
						) noexcept{
	return false;
	}


Driver::Driver(	Oscl::Frame::Tx::Api&		frameTx,
				unsigned 					nTxCompletes
				) noexcept:
		Oscl::Mt::Itc::Srv::CloseSync(*this,*this),
		_frameTx(frameTx),
		_closeSAP(*this,*this),
		_txSAP(*this,*this)
		{
	TxDoneMem*	mem	= (TxDoneMem*)malloc(sizeof(TxDoneMem)*nTxCompletes);
	_rawBufferMemory	= mem;
	if(!mem){
		Oscl::ErrorFatal::
		logAndExit("Oscl::Frame::Pdu::Tx::Driver: Out of memory\n");
		}
	for(unsigned i=0;i<nTxCompletes;++i,++mem){
		_txDoneList.put(mem);
		}
	}

Driver::~Driver(){
	if(_rawBufferMemory) free(_rawBufferMemory);
	}

Oscl::Mt::Itc::Srv::Close::Req::Api::SAP&	Driver::getCloseSAP() noexcept{
	return _closeSAP;
	}

Req::Api::SAP&						Driver::getTxSAP() noexcept{
	return _txSAP;
	}

void	Driver::initialize() noexcept{
	}

void	Driver::mboxSignaled() noexcept{
	_frameTx.processCompletedBuffers();
	}

void	Driver::request(Req::Api::TxReq& msg) noexcept{
	TxDoneMem*	mem	= _txDoneList.get();
	if(!mem){
		_pendingList.put(&msg);
		return;
		}
	TxDone*	done	= new(mem) TxDone(*this,_frameTx,msg);
	if(!done->startFrame()){
		done->~TxDone();
		_txDoneList.put(mem);
		_pendingList.put(&msg);
		return;
		}
	}

void	Driver::done(TxDone& done) noexcept{
	done.~TxDone();
	_txDoneList.put((TxDoneMem*)&done);
	Req::Api::TxReq*	req	= _pendingList.get();
	if(req) req->process();
	}

void	Driver::request(CloseReq& msg) noexcept{
	// FIXME: do something real!
	msg.returnToSender();
	}

void	Driver::request(OpenReq& msg) noexcept{
	// FIXME: do something real!
	msg.returnToSender();
	}

