/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_frame_pdu_tx_enet_driverh_
#define _oscl_frame_pdu_tx_enet_driverh_
#include "oscl/mt/itc/srv/closeapi.h"
#include "oscl/mt/itc/srv/close.h"
#include "oscl/frame/pdu/tx/respapi.h"
#include "oscl/pdu/visitor.h"
#include "oscl/memory/block.h"
#include "oscl/protocol/ieee/ethernet/header.h"
#include "oscl/pdu/fixed.h"
#include "oscl/pdu/rofragment.h"
#include "oscl/pdu/rocomposite.h"
#include <stdint.h>
#include "oscl/cache/line.h"

/** */
namespace Oscl {
/** */
namespace Frame {
/** */
namespace Pdu {
/** */
namespace Tx {
/** */
namespace ENET {

class Driver;

/** */
class TxTransaction :	public Oscl::FreeStore::Mgr,
						public Resp::Api
	{
	private:
		/** */
		Oscl::Memory::
			AlignedBlock<sizeof(Oscl::Pdu::Fixed)>		_fixedMem;
		/** */
		Oscl::Memory::
			AlignedBlock<sizeof(Oscl::Pdu::ReadOnlyFragment)>	_headerFragMem;
		/** */
		Oscl::Memory::
			AlignedBlock<sizeof(Oscl::Pdu::ReadOnlyFragment)>	_payloadFragMem;
		/** */
		Oscl::Memory::
			AlignedBlock<sizeof(Oscl::Pdu::ReadOnlyComposite)>	_compositeMem;
		/** */
		Oscl::Memory::
			AlignedBlock<sizeof(Req::Api::TxPayload)>	_payloadMem;
		/** */
		Oscl::Memory::
			AlignedBlock<sizeof(Resp::Api::TxResp)>		_respMem;
		/** */
		Req::Api::TxReq&									_req;
		/** */
		Driver&											_context;
		/** */
		Oscl::Handle<const Oscl::Pdu::ReadOnlyPdu>			_handle;

	public:
		/** */
		TxTransaction(	Driver&						context,
						const Oscl::Protocol::
						IEEE::Ethernet::Header&		header,
						Req::Api::SAP&				rawTxSAP,
						Req::Api::TxReq&				req,
						Oscl::Mt::Itc::PostMsgApi&	myPapi
						) noexcept;

	private:	// Resp::Api
		/** */
		void	response(Resp::Api::TxResp& msg) noexcept;

	private:	// FreeStore::Mgr
		/** */
		void	free(void* fso) noexcept;
	};

/** Sycophant Ethernet Driver. Single type-destination.
 */
class Driver :	public Oscl::Mt::Itc::Srv::CloseSync,
				private Oscl::Mt::Itc::Srv::Close::Req::Api,
				private Req::Api
				{
	private:
		/** */
		friend class TxTransaction;
	private:
		/** */
		typedef Oscl::Memory::
				AlignedBlock<sizeof(TxTransaction)>	TxTransactionMem;
		/** */

	private:
		/** */
		Req::Api::SAP&							_rawTxSAP;
		/** */
		void*									_rawBufferMemory;
		/** */
		Oscl::Mt::Itc::Srv::Close::Req::Api::
		ConcreteSAP								_closeSAP;
		/** */
		Req::Api::ConcreteSAP					_txSAP;
		/** */
		Oscl::Queue<TxTransactionMem>			_transactionList;
		/** */
		Oscl::Queue<Req::Api::TxReq>			_pendingList;
		/** */
		Mt::Itc::Srv::Close::Req::Api::
		CloseReq*								_closeReq;
		/** */
		bool									_open;
		/** */
		bool									_closing;
		/** */
		unsigned								_nTransactionsInProgress;
		/** */
		OsclCacheAlignMacroPre();
		Oscl::Protocol::IEEE::Ethernet::Header	_header
		OsclCacheAlignMacroPost();

	public:
		/** Allocates memory when created. */
		Driver(	Oscl::Mt::Itc::PostMsgApi&				myPapi,
				Req::Api::SAP&							rawTxSAP,
				const Oscl::Protocol::IEEE::MacAddress&	destination,
				const Oscl::Protocol::IEEE::MacAddress&	source,
				uint16_t								type,
				unsigned 								nTransactions
				) noexcept;

		/** */
		~Driver();

		/** */
		Oscl::Mt::Itc::Srv::Close::Req::Api::SAP&	getCloseSAP() noexcept;
		/** */
		Req::Api::SAP&						getTxSAP() noexcept;

	private:	// Oscl::Mt::Itc::Srv::Close::Req::Api
		/** */
		void	request(CloseReq& msg) noexcept;

	private:	// Oscl::Mt::Itc::Srv::Open::Req::Api
		/** */
		void	request(OpenReq& msg) noexcept;

	private:	// Req::Api
		void	request(Req::Api::TxReq& msg) noexcept;

	private:
		/** */
		void	done(TxTransaction& done,Req::Api::TxReq& msg) noexcept;
	};
}
}
}
}
}


#endif
