/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <new>
#include <stdlib.h>
#include "driver.h"
#include "oscl/error/fatal.h"
#include "oscl/pdu/leaf.h"

using namespace Oscl::Frame::Pdu::Tx::ENET;

TxTransaction::TxTransaction(	Driver&							context,
								const Oscl::Protocol::IEEE::
								Ethernet::Header&				header,
								Req::Api::SAP&					rawTxSAP,
								Req::Api::TxReq&				req,
								Oscl::Mt::Itc::PostMsgApi&		myPapi
								) noexcept:
		_req(req),
		_context(context)
		{
	Oscl::Pdu::Fixed*	fixed;
	fixed	= new (&_fixedMem) Oscl::Pdu::Fixed(	*this,
													*this,
													(uint8_t*)&header,
													sizeof(header),
													sizeof(header)
													);
	Oscl::Pdu::ReadOnlyFragment*	headerFrag;
	headerFrag	= new (&_headerFragMem)
					Oscl::Pdu::ReadOnlyFragment(*this,fixed);
	Oscl::Pdu::ReadOnlyFragment*	payloadFrag;
	payloadFrag	= new (&_payloadFragMem)
					Oscl::Pdu::ReadOnlyFragment(	*this,
													req.
													getPayload().
													_pdu
													);
	Oscl::Pdu::ReadOnlyComposite*	composite;
	composite	= new (&_compositeMem) Oscl::Pdu::ReadOnlyComposite(*this);

	Req::Api::TxPayload*	payload;
	payload	= new (&_payloadMem) Req::Api::TxPayload(*composite);

	Resp::Api::TxResp*	resp;
	resp	= new (&_respMem) Resp::Api::TxResp(	rawTxSAP.getReqApi(),
													*this,
													myPapi,
													*payload
													);
	composite->append(headerFrag);
	composite->append(payloadFrag);
	_handle	= composite;

	rawTxSAP.post(resp->getSrvMsg());
	}

void	TxTransaction::response(Resp::Api::TxResp& msg) noexcept{
	_context.done(*this,_req);
	}

void	TxTransaction::free(void* fso) noexcept{
	// Do nothing
	}



Driver::Driver(	Oscl::Mt::Itc::PostMsgApi&				myPapi,
				Req::Api::SAP&						rawTxSAP,
				const Oscl::Protocol::IEEE::MacAddress&	destination,
				const Oscl::Protocol::IEEE::MacAddress&	source,
				uint16_t								type,
				unsigned 								nTransactions
				) noexcept:
		Oscl::Mt::Itc::Srv::CloseSync(*this,myPapi),
		_rawTxSAP(rawTxSAP),
		_closeSAP(*this,myPapi),
		_txSAP(*this,myPapi),
		_open(false),
		_closing(false),
		_nTransactionsInProgress(0)
		{
	_header.destination	= destination;
	_header.source		= source;
	_header.type		= type;

	const unsigned long	cacheLineSize	= 32;
	const unsigned long	memSize	=		(sizeof(TxTransactionMem)*nTransactions)
									+	(cacheLineSize-1);

	_rawBufferMemory	= malloc(memSize);
	if(!_rawBufferMemory){
		Oscl::ErrorFatal::
		logAndExit("Oscl::Frame::Pdu::Tx::ENET::Driver: Out of memory\n");
		}
	unsigned long	memory	= (unsigned long)_rawBufferMemory;
	memory	+= (cacheLineSize-1);
	memory	-= (memory%cacheLineSize);
	TxTransactionMem*	mem	= (TxTransactionMem*)memory;
	for(unsigned i=0;i<nTransactions;++i,++mem){
		_transactionList.put(mem);
		}
	}

Driver::~Driver(){
	if(_rawBufferMemory) free(_rawBufferMemory);
	}

Oscl::Mt::Itc::Srv::Close::Req::Api::SAP&	Driver::getCloseSAP() noexcept{
	return _closeSAP;
	}

Oscl::Frame::Pdu::Tx::Req::Api::SAP&		Driver::getTxSAP() noexcept{
	return _txSAP;
	}

void	Driver::request(Req::Api::TxReq& msg) noexcept{
	if(!_open){
		msg.returnToSender();
		return;
		}
	TxTransactionMem*	mem	= _transactionList.get();
	if(!mem){
		_pendingList.put(&msg);
		return;
		}
	new(mem) TxTransaction(	*this,
							_header,
							_rawTxSAP,
							msg,
							_txSAP
							);
	++_nTransactionsInProgress;
	}

void	Driver::done(TxTransaction& done,Req::Api::TxReq& msg) noexcept{
	done.~TxTransaction();
	_transactionList.put((TxTransactionMem*)&done);
	--_nTransactionsInProgress;
	msg.returnToSender();
	if(_closing){
		if(!_nTransactionsInProgress){
			_closeReq->returnToSender();
			}
		return;
		}
	if(!_open){
		Oscl::ErrorFatal::
		logAndExit("Oscl::Frame::Pdu::Tx::ENET::Driver: protocol error\n");
		}
	Req::Api::TxReq*	req	= _pendingList.get();
	if(req) req->process();
	}

void	Driver::request(CloseReq& msg) noexcept{
	if(!_open || _closing){
		Oscl::ErrorFatal::
		logAndExit("Oscl::Frame::Pdu::Tx::ENET::Driver: protocol error\n");
		}
	if(_nTransactionsInProgress){
		_open		= false;
		_closing	= true;
		_closeReq	= &msg;
		return;
		}
	}

void	Driver::request(OpenReq& msg) noexcept{
	if(_open || _closing){
		Oscl::ErrorFatal::
		logAndExit("Oscl::Frame::Pdu::Tx::ENET::Driver: protocol error\n");
		}
	_open	= true;
	msg.returnToSender();
	}

