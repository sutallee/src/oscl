/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <new>
#include "driver.h"
#include "oscl/error/fatal.h"
#include "oscl/mt/mutex/iss.h"
#include "oscl/memory/align.h"

using namespace Oscl::Frame::Pdu::Rx;
using namespace Oscl;

FixedMsg::FixedMsg(Driver& rx) noexcept:_rx(rx){}

void FixedMsg::process() noexcept{
	_rx.returnFixed(this);
	}

FragmentMsg::FragmentMsg(Driver&	rx) noexcept:_rx(rx){}

void FragmentMsg::process() noexcept{
	_rx.returnFragment(this);
	}

CompositeMsg::CompositeMsg(Driver&	rx) noexcept:_rx(rx){}

void CompositeMsg::process() noexcept{
	_rx.returnComposite(this);
	}

Fixed::Fixed(	Driver&					rx,
				Oscl::FreeStore::Mgr&	mgr,
				Oscl::FreeStore::Mgr&	buffMgr,
				uint8_t*					buffer,
				unsigned				bufferSize
				) noexcept:
		Oscl::Pdu::Fixed(mgr,buffMgr,buffer,bufferSize,bufferSize),
		_rx(rx)
		{
	}

void	Fixed::operator delete(void *p,size_t size) noexcept{
	((Fixed*)p)->fsmfree(p);
	}

Fragment::Fragment(	Driver&					rx,
					Oscl::FreeStore::Mgr&	mgr,
					Oscl::Pdu::Pdu*			pdu
					) noexcept:
		Oscl::Pdu::Fragment(mgr,pdu),
		_rx(rx)
		{
	}

void	Fragment::operator delete(void *p,size_t size) noexcept{
	((Fragment*)p)->fsmfree(p);
	}

Composite::Composite(	Driver&					rx,
						Oscl::FreeStore::Mgr&	mgr
						) noexcept:
		Oscl::Pdu::Composite(mgr),
		_rx(rx)
		{
	}

void	Composite::operator delete(void *p,size_t size) noexcept{
	((Composite*)p)->fsmfree(p);
	}

Driver::Driver(	Oscl::Frame::Rx::Api&	frameRx,
				MediaAccessApi&			mac,
				unsigned 				nBuffers,
				unsigned 				bufferSize,
				unsigned 				pow2BufferAlignment
				) noexcept:
		Oscl::Mt::Itc::Srv::CloseSync(*this,*this),
		_frameRx(frameRx),
		_bufferSize(bufferSize),
		_fixedFSM(*this),
		_fragmentFSM(*this),
		_compositeFSM(*this),
		_bufferFSM(*this),
		_nFixeds(0),
		_nFragments(0),
		_mac(mac),
		_closeSAP(*this,*this),
		_protoMuxSAP(*this,*this),
		_sync(_protoMuxSAP)
		{
	// Adjust allocated memory according to the required
	// alignment.
	const unsigned long	alignment		= (1<<pow2BufferAlignment);
	const unsigned long	alignmentAdj	= alignment-1;
	const unsigned long	alignmentMask	= ~(alignmentAdj);
	bufferSize	= (_minBufferSize > bufferSize)?_minBufferSize:bufferSize;
	bufferSize	= ((alignmentAdj)+bufferSize) & alignmentMask;
	size_t	memSize	= bufferSize * nBuffers + alignmentAdj;
	memSize	&= alignmentMask;
	memSize += sizeof(FixedMem)*nBuffers;
	memSize += sizeof(FragmentMem)*nBuffers;
	memSize += sizeof(CompositeMem)*nBuffers;

	void*	mem	= Oscl::Memory::mallocAligned(	&_rawBufferMemory,
												memSize,
												alignment
												);	
	if(!mem){
		Oscl::ErrorFatal::logAndExit("Oscl::Frame::Pdu::Rx::Driver\n");
		}
	unsigned long memory	= (unsigned long)mem;
	for(unsigned i=0;i<nBuffers;++i,memory+=bufferSize){
		_bufferPool.put((MemBlock*)memory);
		}
	FixedMem*	fixed	= (FixedMem*)memory;
	for(unsigned i=0;i<nBuffers;++i,++fixed){
		freeFixed(fixed);
		}
	FragmentMem*	frag	= (FragmentMem*)fixed;
	for(unsigned i=0;i<nBuffers;++i,++frag){
		freeFragment(frag);
		}
	CompositeMem*	comp	= (CompositeMem*)frag;
	for(unsigned i=0;i<nBuffers;++i,++comp){
		freeComposite(comp);
		}
	_bufferSize	= bufferSize;
	}

Driver::~Driver(){
	}

Oscl::Mt::Itc::Srv::Close::Req::Api::SAP&	Driver::getCloseSAP() noexcept{
	return _closeSAP;
	}

Oscl::Frame::Pdu::Rx::ProtoMux::
Req::Api::SAP&	Driver::getProtoMuxSAP() noexcept{
	return _protoMuxSAP;
	}

Oscl::Frame::Pdu::Rx::
ProtoMux::SyncApi&		Driver::getSyncApi() noexcept{
	return _sync;
	}

void	Driver::initialize() noexcept{
	// Here, I need to give buffers to the
	// frame receiver.
	}

void	Driver::mboxSignaled() noexcept{
	unsigned	nBuffsInFrame;
	while((nBuffsInFrame=_frameRx.open())){
		if(_nFixeds < nBuffsInFrame){
			_frameRx.discard();
			continue;
			}
		if(_nFragments < nBuffsInFrame){
			_frameRx.discard();
			continue;
			}
		Composite*	composite=allocComposite();
		if(!composite){
			_frameRx.discard();
			continue;
			}

		// 1. Preallocate packet buffer chain
		// 2. Attach each buffer to packet
		for(unsigned i=0;i<nBuffsInFrame;++i){
			// 1. Allocate a packet element.
			// 2. If no packet element then
			// 2.1 Discard packet elements
			// 2.2 Discard frame
			// 2.3 Break
			// 3. Attach buffer to packet element
			void*	buffer;
			unsigned length	= _frameRx.nextBuffer(&buffer);
			if(!buffer){
				while(true);
				}
			Fragment*	fragment	= allocFragment((uint8_t*)buffer,length);
			if(!fragment){
				while(true);
				}
			composite->append(fragment);
			}

		bool	exactAddressMatch		= _frameRx.exactAddressMatch();
		bool	operatingPromiscuously;
		bool	promiscuousFrame;
		if(!exactAddressMatch){
			promiscuousFrame		= _frameRx.promiscuousFrame();
			if(!promiscuousFrame){
				operatingPromiscuously	= _frameRx.operatingPromiscuously();
				}
			else {
				operatingPromiscuously	=false;
				}
			}
		else {
			operatingPromiscuously	=false;
			promiscuousFrame		=false;
			}

		_frameRx.close();

		Oscl::Handle<Oscl::Pdu::Composite>	copyPdu;
		Composite*							copyComposite=0;
		if(_snifferList.first()){
			copyComposite	= allocComposite(nBuffsInFrame);
			if(copyComposite){
				Oscl::Pdu::Composite&	copyComp	= *copyComposite;
				copyComp	= *composite;
				copyPdu		= copyComposite;
				}
			}

#if 0
		Fragment*	fragment	= allocFragment(composite);
		if(!fragment){
			delete composite;
			continue;
			}
#endif

		Oscl::Handle<Oscl::Pdu::Composite>	pdu	= composite;

		if(!exactAddressMatch && !promiscuousFrame){
			if(!(exactAddressMatch=_mac.addressMatch(composite))){
				if(!operatingPromiscuously){
					// Discard that packet
					continue;
					}
				}
			}

		if(exactAddressMatch){
			unsigned	protocolType	= _mac.getProtocolType(composite);
			_mac.strip(composite);
			Oscl::Frame::Pdu::Rx::ProtoMux::Api*	proto;
			for(	proto	= _protoMuxList.first();
					proto;
					proto	= _protoMuxList.next(proto)
					){
				if(proto->accept(composite,protocolType)) break;
				}
			}

		if(copyComposite){
			Oscl::Frame::Pdu::Rx::Sniffer::Api*	sniffer;
			for(	sniffer	= _snifferList.first();
					sniffer;
					sniffer	= _snifferList.next(sniffer)
					){
				sniffer->accept(copyComposite);
				}
			}
		}
	}

void	Driver::giveFreeBuffersToFrameReceiver() noexcept{
	uint8_t*	buffer;
	while((buffer = allocBuffer())){
		_frameRx.supplyBuffer(buffer,_bufferSize);
		}
	}

uint8_t*	Driver::allocBuffer() noexcept{
	return (uint8_t*)_bufferPool.get();
	}

void	Driver::freeBuffer(uint8_t *buff) noexcept{
	_bufferPool.put((MemBlock*)buff);
	}

void	Driver::freeFixed(void *fixed) noexcept{
	// Do not delete!
	_fixedPool.put((MemBlock*)fixed);
	++_nFixeds;
	}

void	Driver::freeFragment(void *fragment) noexcept{
	// Do not delete!
	_fragmentPool.put((MemBlock*)fragment);
	++_nFragments;
	}

void	Driver::freeComposite(void *composite) noexcept{
	// Do not delete!
	_compositePool.put((MemBlock*)composite);
	}

Fixed*	Driver::allocFixed(uint8_t* buff,unsigned length) noexcept{
	void*	mem	= _fixedPool.get();
	if(!mem) return 0;
	--_nFixeds;
	return new (mem) Fixed(*this,_fixedFSM,_bufferFSM,buff,length);
	}

Fragment*	Driver::allocFragment(uint8_t* buff,unsigned length) noexcept{
	Fixed*	fixed	= allocFixed(buff,length);
	if(!fixed) {
		return 0;
		}
	void*	mem		= _fragmentPool.get();
	if(!mem) {
		freeFixed(fixed);
		return 0;
		}
	--_nFragments;
	return new (mem) Fragment(*this,_fragmentFSM,fixed);
	}

Composite*	Driver::allocComposite() noexcept{
	void*	mem	= _compositePool.get();
	if(!mem) return 0;
	return new (mem) Composite(*this,_compositeFSM);
	}

Composite*	Driver::allocComposite(unsigned nFragments) noexcept{
	if(!_compositePool.first()) return 0;
	if(nFragments < _nFragments){
		return 0;
		}
	Composite*	composite	= allocComposite();
	for(unsigned i=0;i<nFragments;++i){
		Fragment*	fragment	= allocFragment();
		composite->append(fragment);
		}
	return composite;
	}

Fragment*	Driver::allocFragment(Composite* composite) noexcept{
	void*	mem		= _fragmentPool.get();
	if(!mem) {
		return 0;
		}
	--_nFragments;
	return new (mem) Fragment(*this,_fragmentFSM,composite);
	}

Fragment*	Driver::allocFragment() noexcept{
	void*	mem		= _fragmentPool.get();
	if(!mem) {
		return 0;
		}
	--_nFragments;
	return new (mem) Fragment(*this,_fragmentFSM,0);
	}

void	Driver::returnFixed(FixedMsg* fixed) noexcept{
	freeFixed(fixed);
	giveFreeBuffersToFrameReceiver();
	}

void	Driver::returnFragment(FragmentMsg* fragment) noexcept{
	freeFragment(fragment);
	}

void	Driver::returnComposite(CompositeMsg* composite) noexcept{
	freeComposite(composite);
	}

void	Driver::free(Oscl::Frame::Pdu::Rx::Fixed* fso) noexcept{
	FixedMsg*	msg	= new(fso) FixedMsg(*this);
	post(*msg);
	}

void	Driver::free(Oscl::Frame::Pdu::Rx::Fragment* fso) noexcept{
	FragmentMsg*	msg	= new(fso) FragmentMsg(*this);
	post(*msg);
	}

void	Driver::free(Oscl::Frame::Pdu::Rx::Composite* fso) noexcept{
	CompositeMsg*	msg	= new(fso) CompositeMsg(*this);
	post(*msg);
	}

void	Driver::free(uint8_t* fso) noexcept{
	Oscl::Mt::IntScopeSync	ss;
	freeBuffer(fso);
	}

void	Driver::request(CloseReq& msg) noexcept{
	msg.returnToSender();
	}

void	Driver::request(OpenReq& msg) noexcept{
	giveFreeBuffersToFrameReceiver();
	msg.returnToSender();
	}

void	Driver::request(ProtoMux::Req::Api::AttachReq& msg) noexcept{
	_protoMuxList.put(&msg.getPayload()._protocolMux);
	msg.returnToSender();
	}

void	Driver::request(ProtoMux::Req::Api::DetachReq& msg) noexcept{
	_protoMuxList.remove(&msg.getPayload()._protocolMux);
	msg.returnToSender();
	}

