/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_frame_pdu_rx_driverh_
#define _oscl_frame_pdu_rx_driverh_
#include "oscl/mt/itc/mbox/server.h"
#include "oscl/frame/rx/api.h"
#include "oscl/pdu/fixed.h"
#include "oscl/pdu/fragment.h"
#include "oscl/pdu/composite.h"
#include "oscl/mt/itc/mbox/msg.h"
#include "oscl/mt/itc/srv/closeapi.h"
#include "oscl/mt/itc/srv/close.h"
#include "oscl/memory/block.h"
#include "oscl/frame/pdu/rx/protomux/reqapi.h"
#include "oscl/frame/pdu/rx/sniffer/api.h"
#include "oscl/frame/pdu/rx/mac.h"
#include "oscl/frame/pdu/rx/driverapi.h"
#include "oscl/frame/pdu/rx/protomux/sync/sync.h"

/** */
namespace Oscl {
/** */
namespace Frame {
/** */
namespace Pdu {
/** */
namespace Rx {

class Driver;

/** */
class FixedMsg : public Oscl::Mt::Itc::Msg {
	private:
		/** */
		Driver&			_rx;
	public:
		/** */
		FixedMsg(Driver& drv) noexcept;
	private:
		/** */
		void	process() noexcept;
	};

/** */
class FragmentMsg : public Oscl::Mt::Itc::Msg {
	private:
		/** */
		Driver&			_rx;
	public:
		/** */
		FragmentMsg(Driver& drv) noexcept;
	private:
		/** */
		void	process() noexcept;
	};

/** */
class CompositeMsg : public Oscl::Mt::Itc::Msg {
	private:
		/** */
		Driver&			_rx;
	public:
		/** */
		CompositeMsg(Driver& drv) noexcept;
	private:
		/** */
		void	process() noexcept;
	};

/** */
class Fixed :	public Oscl::Pdu::Fixed {
	private:
		/** */
		Driver&			_rx;
	public:
		/** */
		Fixed(	Driver&			rx,
				FreeStore::Mgr&	mgr,
				FreeStore::Mgr&	buffMgr,
				uint8_t*			buffer,
				unsigned		bufferSize
				) noexcept;
		/** */
		void	operator delete(void *p,size_t size) noexcept;
	private:
		/** */
		void	process() noexcept;
	};

/** */
class Fragment : public Oscl::Pdu::Fragment {
	private:
		/** */
		Driver&			_rx;
	public:
		/** */
		Fragment(	Driver&			rx,
					FreeStore::Mgr&	mgr,
					Oscl::Pdu::Pdu*	pdu
					) noexcept;
		/** */
		void	operator delete(void *p,size_t size) noexcept;
	private:
		/** */
		void	process() noexcept;
	};

/** */
class Composite : public Oscl::Pdu::Composite {
	private:
		/** */
		Driver&			_rx;
	public:
		/** */
		Composite(	Driver&			rx,
					FreeStore::Mgr&	mgr
					) noexcept;
		/** */
		void	operator delete(void *p,size_t size) noexcept;
	private:
		/** */
		void	process() noexcept;
	};

/** */
template <class Type>
class FreeStoreMgr : public Oscl::FreeStore::Mgr {
	private:
		/** */
		Driver&	_rx;
	public:
		/** */
		FreeStoreMgr(Driver& rx) noexcept:_rx(rx){}
	private: // FreeStore::Mgr
		/** */
		void	free(void* fso) noexcept;
	};

typedef union FixedMem {
	Oscl::Memory::AlignedBlock<sizeof(Fixed)>		_pdu;
	Oscl::Memory::AlignedBlock<sizeof(FixedMsg)>	_msg;
	} FixedMem;

typedef union FragmentMem {
	Oscl::Memory::AlignedBlock<sizeof(Fragment)>	_pdu;
	Oscl::Memory::AlignedBlock<sizeof(FragmentMsg)>	_msg;
	} FragmentMem;

typedef union CompositeMem {
	Oscl::Memory::AlignedBlock<sizeof(Composite)>		_pdu;
	Oscl::Memory::AlignedBlock<sizeof(CompositeMsg)>	_msg;
	} CompositeMem;

/** */
class Driver :	public Oscl::Mt::Itc::Server,
				public Oscl::Mt::Itc::Srv::CloseSync,
				private Oscl::Mt::Itc::Srv::Close::Req::Api,
				public Oscl::Frame::Pdu::Rx::ProtoMux::Req::Api,
				public Oscl::Frame::Pdu::Rx::DriverApi
				{
	private:
		/** */
		friend class FixedMsg;
		/** */
		friend class FragmentMsg;
		/** */
		friend class CompositeMsg;
		/** */
		friend class FreeStoreMgr<Fixed>;
		/** */
		friend class FreeStoreMgr<Fragment>;
		/** */
		friend class FreeStoreMgr<Composite>;
		/** */
		friend class FreeStoreMgr<uint8_t>;
		/** */
		enum {_minBufferSize=128};
		/** */
		class MemBlock : public QueueItem { };

	private:
		/** */
		Oscl::Frame::Rx::Api&		_frameRx;
		/** */
		void*						_rawBufferMemory;
		/** */
		unsigned					_bufferSize;
		/** */
		Oscl::Queue<MemBlock>		_bufferPool;
		/** */
		Oscl::Queue<MemBlock>		_fixedPool;
		/** */
		Oscl::Queue<MemBlock>		_fragmentPool;
		/** */
		Oscl::Queue<MemBlock>		_compositePool;
		/** */
		FreeStoreMgr<Fixed>			_fixedFSM;
		/** */
		FreeStoreMgr<Fragment>		_fragmentFSM;
		/** */
		FreeStoreMgr<Composite>		_compositeFSM;
		/** */
		FreeStoreMgr<uint8_t>			_bufferFSM;
		/** */
		unsigned					_nFixeds;
		/** */
		unsigned					_nFragments;
		/** */
		MediaAccessApi&										_mac;
		/** */
		Oscl::Mt::Itc::Srv::Close::Req::Api::ConcreteSAP		_closeSAP;
		/** */
		Oscl::Frame::Pdu::Rx::ProtoMux::Req::Api::ConcreteSAP	_protoMuxSAP;
		/** */
		Oscl::Frame::Pdu::Rx::ProtoMux::Sync				_sync;
		/** */
		Oscl::Queue<Oscl::Frame::Pdu::Rx::ProtoMux::Api>	_protoMuxList;
		/** */
		Oscl::Queue<Oscl::Frame::Pdu::Rx::Sniffer::Api>		_snifferList;

	public:
		/** Allocates memory when created. */
		Driver(	Oscl::Frame::Rx::Api&	frameRx,
				MediaAccessApi&			mac,
				unsigned 				nBuffers,
				unsigned 				bufferSize,
				unsigned 				bufferAlignment
				) noexcept;

		/** */
		~Driver();

	public: // DriverApi
		/** */
		Oscl::Mt::Itc::Srv::Close::Req::Api::SAP&	getCloseSAP() noexcept;

		/** */
		Oscl::Frame::Pdu::Rx::ProtoMux::
		Req::Api::SAP&							getProtoMuxSAP() noexcept;

		/** */
		Oscl::Frame::Pdu::Rx::
		ProtoMux::SyncApi&						getSyncApi() noexcept;

	private:
		/** */
		void	giveFreeBuffersToFrameReceiver() noexcept;

	private:
		/** */
		void	initialize() noexcept;
		/** */
		void	mboxSignaled() noexcept;

	private:	// allocators
		/** */
		uint8_t*		allocBuffer() noexcept;
		/** */
		Fixed*		allocFixed(uint8_t* buff,unsigned length) noexcept;
		/** */
		Fragment*	allocFragment(uint8_t* buff,unsigned length) noexcept;
		/** */
		Composite*	allocComposite() noexcept;
		/** */
		Composite*	allocComposite(unsigned nFragments) noexcept;
		/** */
		Fragment*	allocFragment(Composite* composite) noexcept;
		/** */
		Fragment*	allocFragment() noexcept;
	private:	// deallocation
		/** */
		void	freeBuffer(uint8_t *) noexcept;
		/** */
		void	freeFragment(void* fragment) noexcept;
		/** */
		void	freeFixed(void* fixed) noexcept;
		/** */
		void	freeComposite(void* composite) noexcept;
	private:	// deallocation
		/** */
		void	returnFragment(FragmentMsg* fragment) noexcept;
		/** */
		void	returnFixed(FixedMsg* fixed) noexcept;
		/** */
		void	returnComposite(CompositeMsg* composite) noexcept;
	private:	// pdu deallocation actions
		/** */
		void	free(Fragment* fragment) noexcept;
		/** */
		void	free(Fixed* fixed) noexcept;
		/** */
		void	free(Composite* composite) noexcept;
		/** */
		void	free(uint8_t* buffer) noexcept;

	private:	// Oscl::Mt::Itc::Srv::Close::Req::Api
		/** */
		void	request(CloseReq& msg) noexcept;

	private:	// Oscl::Mt::Itc::Srv::Open::Req::Api
		/** */
		void	request(OpenReq& msg) noexcept;

	private:	// Oscl::Frame::Pdu::Rx::ProtoMux::Req::Api
		/** */
		void	request(ProtoMux::Req::Api::AttachReq& msg) noexcept;
		/** */
		void	request(ProtoMux::Req::Api::DetachReq& msg) noexcept;
	};

/** */
template <class Type>
	inline void	FreeStoreMgr<Type>::free(void* fso) noexcept{
		_rx.free((Type*)fso);
		}

}
}
}
}


#endif
