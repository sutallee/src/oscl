/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_frame_pdu_rx_contexth_
#define _oscl_frame_pdu_rx_contexth_
#include "oscl/pdu/composite.h"

/** */
namespace Oscl {
/** */
namespace Frame {
/** */
namespace Pdu {
/** */
namespace Rx {

/** This interface is to be implemented by the link layer specific
	driver that understands the layout of the link layer header.
	The implementation is NOT directly responsible for deleting
	the frame. The implementation must wrap the frame in a
	Oscl::Pdu::Composite before returning from this operation
	if it decides to forward the frame to upper layers.
 */
class ContextApi {
	public:
		/** Received frame destined for this station. Implementation
			does not need to perform destination address checking.
		 */
		virtual void	exactAddressMatch(Oscl::Pdu::Composite* frame) noexcept=0;

		/** Received frame may be destined this station. The frame
			receiver has determined that this frame partially matched
			one of several possible addresses programmed into the framer.
			The implementation needs to further examine the destination
			address to determine if the frame is destined for this
			station.  The implementation can further assume that this
			frame was not received as the result of promiscuous operation.
		 */
		virtual void	partialMatch(Oscl::Pdu::Composite* frame) noexcept=0;

		/** Received frame not destined for this station. The implementation
			may assume that this frame was NOT destined for this station. The
			destination address matched neither the station address, broadcast
			address nor an explicitly programmed multicast address. Typically,
			only network sniffers make use of this type of frame.
		 */
		virtual void	promiscuousMatch(Oscl::Pdu::Composite* frame) noexcept=0;
	};

}
}
}
}

#endif
