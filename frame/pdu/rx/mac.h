/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_frame_pdu_rx_mach_
#define _oscl_frame_pdu_rx_mach_
#include "oscl/mt/itc/mbox/srvreq.h"
#include "oscl/mt/itc/mbox/sap.h"
#include "oscl/pdu/composite.h"

/** */
namespace Oscl {
/** */
namespace Frame {
/** */
namespace Pdu {
/** */
namespace Rx {

/** This interface is to be implemented by the layer specific
	driver that understands the layout of the layer header.
	The implementation must not store references to the frame
	beyond the life of the operation invocation.
 */
class MediaAccessApi {
	public:
		/** Shut-up GCC. */
		virtual ~MediaAccessApi() {}

		/** This operation returns true if the destination address
			of the frame exactly matches one of the individual,
			group or broadcast addresses for this layer. This
			operation is only called if an address match is
			indicated by the framer, but the address matching
			performed by the framer is only partial.
		 */
		virtual bool		addressMatch(Oscl::Pdu::Pdu* frame) noexcept=0;

		/** This operation returns the protocol type multiplexing field
			from the frame.
		 */
		virtual unsigned long	getProtocolType(	Oscl::Pdu::Pdu*	frame
													) noexcept=0;

		/** This operation strips headers and trailer information
			from the frame.
		 */
		virtual void		strip(Oscl::Pdu::Composite* frame) noexcept=0;
	};

}
}
}
}

#endif
