/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_frame_pdu_rx_protomux_driverh_
#define _oscl_frame_pdu_rx_protomux_driverh_
#include "oscl/pdu/fragment.h"
#include "oscl/mt/itc/srv/closeapi.h"
#include "oscl/mt/itc/srv/close.h"
#include "oscl/frame/pdu/rx/protomux/api.h"
#include "oscl/frame/pdu/rx/reqapi.h"

/** */
namespace Oscl {
/** */
namespace Frame {
/** */
namespace Pdu {
/** */
namespace Rx {
/** */
namespace ProtoMux {

/** */
class Driver :	public Oscl::Mt::Itc::Srv::CloseSync,
				private Oscl::Mt::Itc::Srv::Close::Req::Api,
				public Oscl::Frame::Pdu::Rx::ProtoMux::Api,
				public Oscl::Frame::Pdu::Rx::Req::Api
				{
	private:
		/** */
		Oscl::Mt::Itc::Srv::
		Close::Req::Api::ConcreteSAP	_closeSAP;
		/** */
		Oscl::Frame::Pdu::Rx::
		Req::Api::ConcreteSAP			_rxSAP;
		/** */
		Oscl::Queue<Oscl::Frame::Pdu::Rx::Req::Api::RxReq>	_rxReqList;
		/** */
		unsigned long					_protocolType;
		/** */
		bool							_open;
		/** */
		bool							_closing;
		/** */
		struct {
			/** */
			unsigned long	nReceived;
			/** */
			unsigned long	nDiscards;
			}_stats;

	public:
		/** */
		Driver(	Oscl::Mt::Itc::PostMsgApi&	myPapi,
				unsigned long				protocolType
				) noexcept;

		/** */
		Oscl::Mt::Itc::Srv::Close::Req::Api::SAP&	getCloseSAP() noexcept;

		/** */
		Oscl::Frame::Pdu::Rx::Req::Api::SAP&		getRxSAP() noexcept;

	private:	// ProtoMux::Api
		bool	accept(	Oscl::Pdu::Pdu*	pdu,
						unsigned long	type
						) noexcept;

	private:	// Oscl::Mt::Itc::Srv::Close::Req::Api
		/** */
		void	request(	Oscl::Mt::Itc::Srv::
							Close::Req::Api::CloseReq&	msg
							) noexcept;

	private:	// Oscl::Mt::Itc::Srv::Open::Req::Api
		/** */
		void	request(	Oscl::Mt::Itc::Srv::
							Open::Req::Api::OpenReq&	msg
							) noexcept;

	private:	// Req::Api
		/** */
		void	request(	Oscl::Frame::Pdu::
							Rx::Req::Api::RxReq&	msg
							) noexcept;

		/** */
		void	request(	Oscl::Frame::Pdu::
							Rx::Req::Api::CancelAllReq&	msg
							) noexcept;

	};

}
}
}
}
}

#endif
