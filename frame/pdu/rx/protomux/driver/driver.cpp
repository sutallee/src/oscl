/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "oscl/error/fatal.h"
#include "driver.h"

using namespace Oscl::Frame::Pdu::Rx::ProtoMux;

Driver::Driver(	Oscl::Mt::Itc::PostMsgApi&	myPapi,
				unsigned long				protocolType
				) noexcept:
		Oscl::Mt::Itc::Srv::CloseSync(*this,myPapi),
		_closeSAP(*this,myPapi),
		_rxSAP(*this,myPapi),
		_protocolType(protocolType),
		_open(false),
		_closing(false)
		{
	_stats.nReceived	= 0;
	_stats.nDiscards	= 0;
	}

Oscl::Mt::Itc::Srv::Close::Req::Api::SAP&	Driver::getCloseSAP() noexcept{
	return _closeSAP;
	}

Oscl::Frame::Pdu::Rx::Req::Api::SAP&		Driver::getRxSAP() noexcept{
	return _rxSAP;
	}

bool	Driver::accept(	Oscl::Pdu::Pdu*	pdu,
						unsigned long	type
						) noexcept{
	if(type == _protocolType){
		if(_open){
			Req::Api::RxReq*	msg;
			if((msg=_rxReqList.get())){
				++_stats.nReceived;
				msg->getPayload()._handle	= pdu;
				msg->returnToSender();
				}
			else {
				++_stats.nDiscards;
				}
			}
		return true;
		}
	return false;
	}

void	Driver::request(CloseReq& msg) noexcept{
	Req::Api::RxReq*	rxReq;
	while((rxReq=_rxReqList.get())){
		rxReq->returnToSender();
		}
	msg.returnToSender();
	_open	= false;
	}

void	Driver::request(OpenReq& msg) noexcept{
	_open	= true;
	msg.returnToSender();
	}

void	Driver::request(Req::Api::RxReq& msg) noexcept{
	if(!_open){
		Oscl::ErrorFatal::logAndExit("protocol error\n");
		}
	_rxReqList.put(&msg);
	}

// Is this operation necessary? CloseReq seems to cover this.
void	Driver::request(Req::Api::CancelAllReq& msg) noexcept{
	Req::Api::RxReq*	next;
	while((next=_rxReqList.get())){
		next->returnToSender();
		}
	msg.returnToSender();
	}


