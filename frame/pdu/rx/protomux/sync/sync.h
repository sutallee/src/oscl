/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_frame_pdu_rx_protomux_synch_
#define _oscl_frame_pdu_rx_protomux_synch_
#include "oscl/frame/pdu/rx/protomux/reqapi.h"
#include "oscl/frame/pdu/rx/protomux/syncapi.h"

/** */
namespace Oscl {
/** */
namespace Frame {
/** */
namespace Pdu {
/** */
namespace Rx {
/** */
namespace ProtoMux {

/**	*/
class Sync : public SyncApi {
	private:
		/** */
		Req::Api::SAP&	_sap;

	public:
		/** */
		Sync(Req::Api::SAP& sap) noexcept;

	public:	// SyncApi
		/** */
		void	attach(ProtoMux::Api& acceptor) noexcept;
		/** */
		void	detach(ProtoMux::Api& acceptor) noexcept;
	};

}
}
}
}
}

#endif
