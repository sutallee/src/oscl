/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_frame_pdu_rx_protomux_apih_
#define _oscl_frame_pdu_rx_protomux_apih_
#include "oscl/queue/queueitem.h"
#include "oscl/pdu/pdu.h"

/** */
namespace Oscl {
/** */
namespace Frame {
/** */
namespace Pdu {
/** */
namespace Rx {
/** */
namespace ProtoMux {

/** */
class Api : public Oscl::QueueItem {
	public:
		/** Shut-up GCC. */
		virtual ~Api() {}

		/** The implementation must always return true if the
			type matches, regardless of whether or not the pdu
			is forwarded due to resource constraints.
			The caller maintains a Handle<Pdu> reference to
			the fragment during this execution. The implementation
			that accepts the packet must assign the fragment to a
			Handle<Pdu>.
		 */
		virtual bool	accept(	Oscl::Pdu::Pdu*	pdu,
								unsigned long	type
								) noexcept=0;
	};

}
}
}
}
}

#endif
