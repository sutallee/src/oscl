/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_frame_pdu_rx_sniffer_apih_
#define _oscl_frame_pdu_rx_sniffer_apih_
#include "oscl/queue/queueitem.h"
#include "oscl/pdu/composite.h"

/** */
namespace Oscl {
/** */
namespace Frame {
/** */
namespace Pdu {
/** */
namespace Rx {
/** */
namespace Sniffer {

/** */
class Api : public Oscl::QueueItem {
	public:
		/** Shut-up GCC. */
		virtual ~Api() {}

		/** The implementation *must* deep copy the composite in
			before forwarding or using the PDU. The implementation
			*must* also assign the Pdu::Composite to a Handle<Composite>
			for forwarding or the pdu will be deallocated after this
			routine completes.
		 */
		virtual void	accept(Oscl::Pdu::Composite* pdu) noexcept=0;
	};

}
}
}
}
}

#endif

