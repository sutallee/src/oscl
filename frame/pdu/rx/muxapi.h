/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

/*
1. Define 2 types of receivers:
	1.1	ProtocolMux
		Only one protocol Mux can receive a packet.
		Pdu::Composite is supplied by receiver.
	1.2 Sniffer
		Any number of sniffers can receive a packet.
		Pdu::Composite is supplied by receiver. Sniffer
		is responsible for wrapping the composite in
		a Composite. Thus the sniffer must allocate
		fragments.
2. Each type of receiver is maintained in a separate
	list within the driver. Each pdu is first offerred
	to the protocol mux list, and then to the sniffer
	list. The protocol mux list is traversed until
	one of the protocol muxes accepts the PDU, or
	the end of the list is reached, whichever comes
	first. Sniffers are always assumed to accept each
	PDU (although they may not actually propagate the
	PDU because of resource constraints).

3. Actual receivers (ProtocolMux and Sniffer) should
	probably execute as sycophants within the Rx driver
	thread.

4. ProtocolMuxes receive PDUs that have been stripped of
	lower layer headers and trailers.

5. Sniffers receive PDUs with headers and trailers intact.

6. Because of 4 and 5, the driver first allocates a
	Composite and attaches the received Pdu::Composite
	to the fragment, while maintaining a reference to
	the Pdu::Composite. The Composite is then offered
	to the ProtocolMux list. Finall, the Pdu::Composite
	reference is offered to the sniffer list.

7. Only one sniffer?

8. Only one ProtocolMux per protocol type. Not checked explicitly,
	but only the first one in the list will receive PDUs.

[Off topic]
1. Should I relax the rules on takinig the address of Handle<> for the
	sake of performance. Two cases come to mind.
	1. The ProtocolMux interface may become a burden as Handles are
		passed through the function call.
	2. Transmitters that add Headers and Trailers to composites, and
		must create transaction records to perform this activity
		could be simplified. (Currently, they must do placement new
		operations to get a pointer rather that simply doing
		composition construction.

*/

/*	These operations may be invoked while the receiver is operating.
	Typically attachment happens after the sycophant receiver is
	opened, and detachment happens before the sycophant reciever is
	closed.
 */
class Oscl::Frame::Pdu::Rx::Req::Api {
	public:
		class AttachPayload {
			public:
				ProtocolMuxApi&	_protocolMux;
			public
				AttachPayload(ProtocolMuxApi& protocolMux) noexcept;
			};
		class DetachPayload {
			public:
				ProtocolMuxApi&	_protocolMux;
			public
				DetachPayload(ProtocolMuxApi& protocolMux) noexcept;
			};
	public:
		virtual void	request(AttachReq& msg) noexcept=0;
		virtual void	request(DetachReq& msg) noexcept=0;
	};

// Sychophant
class ProtocolMuxApi : public Oscl::QueueItem {
	public:
		/** The implementation must always return true if the
			type matches, regardless of whether or not the pdu
			is forwarded due to resource constraints.
		 */
		virtual bool	accept(	Handle<Pdu::Composite>	pdu,
								unsigned long			type
								) noexcept=0;
		/* Alternately, this is possible because the caller
			maintains a Handle<Pdu> reference to the fragment,
			and the implementation that accepts the packet
			must assign the fragment to a Handle<Pdu>.
		 */
		virtual bool	accept(	Composite*				pdu,
								unsigned long			type
								) noexcept=0;
	};

// RxReq::Api::AttachSnifferMuxReq
// RxReq::Api::DetachSnifferMuxReq

class SnifferApi : public Oscl::QueueItem {
	public:
		virtual void	accept(Handle<Pdu::Composite> pdu) noexcept=0;
		
	};


class ProtocolMux::Req::Api {
//	RxReq		<< Contains Handle<Composite>
//	CancelRxReq
	public:
		class RxPayload {
			public:
				Oscl::Handle<Pdu>	_handle;
			};
		class CancelRxPayload {
			public:
				RxReq&	_reqToCancel;
			public:
				CancelRxPayload(RxReq& reqToCancel) noexcept;
			}
	public:
		virtual void	request(RxReq& msg) noexcept=0;
		virtual void	request(CancelRxReq& msg) noexcept=0;
	};

class Sniffer::Req::Api {
//	ReceiveReq	<< Contains Composite and Handle<Composite>
//	CancelReq
	public:
		virtual void	request(RxReq& msg) noexcept=0;
		virtual void	request(CancelRxReq& msg) noexcept=0;
	};


class ProtocolMux :	public ProtocolMux::Req::Api,
					public ProtocolMuxApi
					{

	public:	// ProtocolMux::Req::Api
		/** */
		void	request(RxReq& msg) noexcept{
			_rxReqList.put(&msg);
			}
		/** */
		void	request(CancelRxReq& msg) noexcept{
			}
	public:	// ProtocolMuxApi
		bool	accept(	Composite*				pdu,
						unsigned long			type
						) noexcept{
			if(type != myType) return false;
			RxReq*	req	= _rxReqList.get();
			if(req){
				req->getPayload()._handle	= pdu;
				req->returnToSender();
				}
			return true;
			}
	};

class Sniffer :	public Sniffer::Req::Api,
				public SnifferApi
				{
	};
