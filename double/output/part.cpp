#include "part.h"

using namespace Oscl::Double::Output;

Part::Part(
	Oscl::Mt::Itc::PostMsgApi&	papi,
	double						initialState,
	double						threshold
	) noexcept:
		_sap(
			*this,
			papi
			),
		_state(initialState),
		_threshold(threshold)
		{
	}

Oscl::Double::Observer::Req::Api::SAP&	Part::getSAP() noexcept{
	return _sap;
	}

static inline bool	areEqual(double a, double b) noexcept{
	if(isnan(a) && isnan(b)){
		return true;
		}

	bool	aIsInf	= isinf(a);
	bool	bIsInf	= isinf(b);

	if(aIsInf || bIsInf){
		if(aIsInf == bIsInf){
			return true;
			}
		}

	return a == b;
	}

void	Part::notify(double state) noexcept{
	if(areEqual(state,_state)){
		// No change
		return;
		}

	_state	= state;

	Oscl::Double::Observer::Req::Api::ChangeReq* note;

	Oscl::Queue<Oscl::Double::Observer::Req::Api::ChangeReq>	tmpQ = _noteQ;
	
	while((note=tmpQ.get())){
		double	nState	= note->getPayload()._state;

		if(areEqual(nState,_state)){
			_noteQ.put(note);
			continue;
			}

		double	threshold	= note->getPayload()._threshold;

		if((state < (nState + threshold)) && (state > (nState - threshold))){
			_noteQ.put(note);
			continue;
			}

		note->getPayload()._state		= state;
		note->returnToSender();
		}
	}

void Part::request(Oscl::Double::Observer::Req::Api::ChangeReq& msg) noexcept{

	double	state		= msg.getPayload()._state;
	double	threshold	= msg.getPayload()._threshold;

	if(areEqual(_state,state)){
		_noteQ.put(&msg);
		return;
		}

	if( (_state < (state + threshold)) &&	(_state > (state - threshold))){
		_noteQ.put(&msg);
		return;
		}

	msg.getPayload()._state	= _state;
	msg.returnToSender();
	}

void Part::request(Oscl::Double::Observer::Req::Api::CancelChangeReq& msg) noexcept{
	for(
		Oscl::Double::Observer::Req::Api::ChangeReq* next=_noteQ.first();
		next;
		next=_noteQ.next(next)
		){
		if(next == &msg.getPayload()._requestToCancel){
			_noteQ.remove(next);
			next->returnToSender();
			break;
			}
		}
	msg.returnToSender();
	}

void	Part::update(double state) noexcept{
	if(areEqual(state,_state)){
		return;
		}

	if((_state < (state + _threshold)) && (_state > (state - _threshold))){
		return;
		}

	notify(state);
	}

