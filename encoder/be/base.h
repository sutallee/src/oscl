/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_encoder_be_baseh_
#define _oscl_encoder_be_baseh_

#include "oscl/encoder/base.h"

/** */
namespace Oscl {

/** */
namespace Encoder {

/** */
namespace BE {

/** */
class Base : public Oscl::Encoder::Base {
	public:
		/** */
		Base(
			void*		buffer,
			unsigned	bufferLength
			) noexcept;

		/** */
		void	encode(uint16_t v) noexcept;

		/** */
		void	encode(uint32_t v) noexcept;

		/** */
		void	encode(uint64_t v) noexcept;

		/** */
		void	encode24Bit(uint32_t v) noexcept;

	};

}
}
}

#endif
