/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "base.h"

using namespace Oscl::Encoder::BE;

Base::Base(
	void*		buffer,
	unsigned	bufferLength
	) noexcept:
		Oscl::Encoder::Base(
			buffer,
			bufferLength
			)
		{
	}

void	Base::encode(uint16_t v) noexcept{

	if(align(2)){
		return;
		}

	if(_offset+2 > _bufferLength){
		_overflow	= true;
		return;
		}

	_buffer[_offset+0]	= (uint8_t)(((uint8_t)(v>>8)) & 0x0FF);
	_buffer[_offset+1]	= (uint8_t)(((uint8_t)(v>>0)) & 0x0FF);

	_offset	+= 2;
	}

void	Base::encode(uint32_t v) noexcept{

	if(align(4)){
		return;
		}

	if(_offset+4 > _bufferLength){
		_overflow	= true;
		return;
		}

	_buffer[_offset+0]	= (uint8_t)(((uint8_t)(v>>24)) & 0x0FF);
	_buffer[_offset+1]	= (uint8_t)(((uint8_t)(v>>16)) & 0x0FF);
	_buffer[_offset+2]	= (uint8_t)(((uint8_t)(v>>8)) & 0x0FF);
	_buffer[_offset+3]	= (uint8_t)(((uint8_t)(v>>0)) & 0x0FF);

	_offset	+= 4;
	}

void	Base::encode24Bit(uint32_t v) noexcept{

	if(align(4)){
		return;
		}

	if(_offset+3 > _bufferLength){
		_overflow	= true;
		return;
		}

	_buffer[_offset+0]	= (uint8_t)(((uint8_t)(v>>16)) & 0x0FF);
	_buffer[_offset+1]	= (uint8_t)(((uint8_t)(v>>8)) & 0x0FF);
	_buffer[_offset+2]	= (uint8_t)(((uint8_t)(v>>0)) & 0x0FF);

	_offset	+= 3;
	}

void	Base::encode(uint64_t v) noexcept{

	if(align(8)){
		return;
		}

	if(_offset+8 > _bufferLength){
		_overflow	= true;
		return;
		}

	_buffer[_offset+0]	= (uint8_t)(((uint8_t)(v>>56)) & 0x0FF);
	_buffer[_offset+1]	= (uint8_t)(((uint8_t)(v>>48)) & 0x0FF);
	_buffer[_offset+2]	= (uint8_t)(((uint8_t)(v>>40)) & 0x0FF);
	_buffer[_offset+3]	= (uint8_t)(((uint8_t)(v>>32)) & 0x0FF);
	_buffer[_offset+4]	= (uint8_t)(((uint8_t)(v>>24)) & 0x0FF);
	_buffer[_offset+5]	= (uint8_t)(((uint8_t)(v>>16)) & 0x0FF);
	_buffer[_offset+6]	= (uint8_t)(((uint8_t)(v>>8)) & 0x0FF);
	_buffer[_offset+7]	= (uint8_t)(((uint8_t)(v>>0)) & 0x0FF);

	_offset	+= 8;
	}

