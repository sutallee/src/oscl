/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <string.h>
#include "base.h"
#include "oscl/error/info.h"

using namespace Oscl::Encoder;

Base::Base(
	void*		buffer,
	unsigned	bufferLength
	) noexcept:
		_buffer((unsigned char*)buffer),
		_bufferLength(bufferLength),
		_offset(0),
		_overflow(false)
		{
	}

void	Base::push(Oscl::Encoder::State& r) noexcept{

	_stack.push(&r);

	r._offset			= _offset;
	}

unsigned	Base::pop() noexcept{

	Oscl::Encoder::State*
	r	= _stack.pop();

	if(!r){
		Oscl::Error::Info::log(
			"%s: pop without push.\n",
			OSCL_PRETTY_FUNCTION
			);
		_overflow	= true;
		return 0;
		}

	unsigned	length	= _offset-r->_offset;

	_offset			= r->_offset;

	return length;
	}

void	Base::skip(unsigned n) noexcept{

	if((_offset + n) > _bufferLength){
		_overflow	= true;
		return;
		}

	_offset	+= n;
	}

bool	Base::align(unsigned bytes) noexcept{
	return false;
	}

void	Base::reset() noexcept{
	_offset		= 0;
	_overflow	= false;
	}

bool	Base::overflow() noexcept{
	return _overflow;
	}

unsigned	Base::length() noexcept{
	return _offset;
	}

void	Base::forceAlign(unsigned octets) noexcept{

	unsigned 	offset	= _offset;

	offset	+= octets-1;

	offset	/= octets;

	offset	*= octets;

	if(offset > _bufferLength){
		_overflow	=  true;
		return;
		}

	_offset	= offset;
	}

void	Base::encode(uint8_t v) noexcept{

	if(_offset+1 > _bufferLength){
		_overflow	= true;
		return;
		}

	_buffer[_offset]	= (unsigned char)v;

	_offset	+= 1;
	}

void	Base::encode(int8_t v) noexcept{
	encode((uint8_t)v);
	}

void	Base::encode(int16_t v) noexcept{
	encode((uint16_t)v);
	}

void	Base::encode(int32_t v) noexcept{
	encode((uint32_t)v);
	}

void	Base::encode(int64_t v) noexcept{
	encode((uint64_t)v);
	}

void	Base::encode(double v) noexcept{

	union{
		double		d;
		uint64_t	u;
		}	magicUnion;

	magicUnion.d	= v;

	encode(magicUnion.u);
	}

void	Base::encodeString(const char* s) noexcept{

	unsigned	len	= strlen(s);

	if((_offset+len+1) > _bufferLength){
		_overflow	= true;
		return;
		}

	strncpy((char*)(&_buffer[_offset]),s,(_bufferLength-_offset-1));

	_offset	+= len+1;
	}

void	Base::encodeSequence(
			const void*		buffer,
			const uint8_t&	length
			) noexcept{

	if(!length){
		return;
		}

	unsigned	savedOffset	= _offset;

	encode(length);

	if(_overflow){
		return;
		}

	if((_offset+length) > _bufferLength){
		_offset	= savedOffset;
		_overflow	= true;
		return;
		}

	memcpy(&_buffer[_offset],buffer,length);

	_offset	+= length;
	}

void	Base::encodeSequence(
			const void*		buffer,
			const uint32_t&	length
			) noexcept{

	if(!length){
		return;
		}

	unsigned	savedOffset	= _offset;

	encode(length);

	if(_overflow){
		return;
		}

	if((_offset+length) > _bufferLength){
		_offset	= savedOffset;
		_overflow	= true;
		return;
		}

	memcpy(&_buffer[_offset],buffer,length);

	_offset	+= length;
	}

void	Base::copyIn(const void* buffer, unsigned length) noexcept{

	if((_offset+length) > _bufferLength){
		_overflow	= true;
		return;
		}

	memcpy(&_buffer[_offset],buffer,length);

	_offset	+= length;
	}

