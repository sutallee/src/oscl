/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_encoder_baseh_
#define _oscl_encoder_baseh_

#include "api.h"
#include "oscl/queue/queue.h"

/** */
namespace Oscl {

/** */
namespace Encoder {

/** */
class Base : public Api {
	private:
		/** */
		Oscl::Queue<Oscl::Encoder::State>	_stack;

	protected:
		/** */
		unsigned char*			_buffer;

		/** */
		unsigned				_bufferLength;

		/** */
		unsigned				_offset;

		/** */
		bool					_overflow;

	public:
		/** */
		Base(
			void*		buffer,
			unsigned	bufferLength
			) noexcept;

		/** */
		void	reset() noexcept;

		/** */
		void	push(Oscl::Encoder::State& r) noexcept;

		/** */
		unsigned	pop() noexcept;

		/** */
		void	skip(unsigned n) noexcept;

		/** */
		bool	overflow() noexcept;

		/** */
		unsigned	length() noexcept;

		/** */
		void	forceAlign(unsigned octets) noexcept;

		/** */
		void	encode(uint8_t v) noexcept;

		/** */
		void	encode(int8_t v) noexcept;

		/** */
		virtual void	encode(uint16_t v) noexcept=0;

		/** */
		void	encode(int16_t v) noexcept;

		/** */
		virtual void	encode(uint32_t v) noexcept=0;

		/** */
		void	encode(int32_t v) noexcept;

		/** */
		virtual void	encode(uint64_t v) noexcept=0;

		/** */
		void	encode(int64_t v) noexcept;

		/** */
		void	encode(double v) noexcept;

		/** */
		virtual void	encodeString(const char* s) noexcept;

		/** */
		void	encodeSequence(
					const void*		buffer,
					const uint8_t&	length
					) noexcept;

		/** */
		void	encodeSequence(
					const void*		buffer,
					const uint32_t&	length
					) noexcept;

		/** */
		void	copyIn(
					const void*	buffer,
					unsigned	length
					) noexcept;

	protected:
		/** */
		virtual bool	align(unsigned octets) noexcept;
	};

}
}


#endif
