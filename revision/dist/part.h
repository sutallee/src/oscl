/*
   Copyright (C) 2024 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_revision_dist_parth_
#define _oscl_revision_dist_parth_

#include "oscl/revision/control/reqapi.h"
#include "oscl/revision/control/api.h"
#include "oscl/revision/output/part.h"
#include "oscl/mt/itc/postmsgapi.h"

/** */
namespace Oscl {

/** */
namespace Revision {

/** */
namespace Dist {

/** */
class Part :
	public Oscl::Revision::Control::Req::Api,
	public Oscl::Revision::Control::Api
	{
	private:
		/** */
		Oscl::Revision::Output::Part		_output;

	public:
		/** */
		Part(
			Oscl::Mt::Itc::PostMsgApi&	papi
			) noexcept;

		/** */
		Oscl::Revision::Observer::Req::Api::SAP&	getOutputSAP() noexcept;

		/** */
		Oscl::Revision::Control::Api&	getLocalControlApi() noexcept;

	private: // Oscl::Revision::Control::Req::Api
		/** */
		void request(Oscl::Revision::Control::Req::Api::UpdateReq& msg) noexcept;

	public:	// Oscl::Revision::Control::Api
		/** */
		void	update() noexcept;
	};

}
}
}

#endif
