/*
   Copyright (C) 2024 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "part.h"

using namespace Oscl::Revision::Output;

Part::Part(
	Oscl::Mt::Itc::PostMsgApi&	papi
	) noexcept:
		_sap(
			*this,
			papi
			),
		_revision(0)
		{
	}

Oscl::Revision::Observer::Req::Api::SAP&	Part::getSAP() noexcept{
	return _sap;
	}

void Part::request(Oscl::Revision::Observer::Req::Api::ChangeReq& msg) noexcept{
	if(!_revision || ( _revision == msg.getPayload()._revision ) ){
		_noteQ.put(&msg);
		}
	else{
		msg.getPayload()._revision	= _revision;
		msg.returnToSender();
		}
	}

void Part::request(Oscl::Revision::Observer::Req::Api::CancelChangeReq& msg) noexcept{
	for(
		Oscl::Revision::Observer::Req::Api::ChangeReq* next=_noteQ.first();
		next;
		next=_noteQ.next(next)
		){
		if(next == &msg.getPayload()._requestToCancel){
			_noteQ.remove(next);
			next->returnToSender();
			break;
			}
		}
	msg.returnToSender();
	}

void	Part::update() noexcept{
	Oscl::Revision::Observer::Req::Api::ChangeReq* note;

	++_revision;
	if( !_revision ) {
		// Zero is a special value
		++_revision;
		}

	while((note=_noteQ.get())){
		note->getPayload()._revision	= _revision;
		note->returnToSender();
		}
	}

