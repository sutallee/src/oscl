/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_mt_mutex_recursiveh_
#define _oscl_mt_mutex_recursiveh_
#include "oscl/mt/mutex/simple.h"

/** */
namespace Oscl {
/** */
namespace Mt {
/** */
namespace Mutex {
/** */
namespace Recursive {

/** This abstract mutex class represents a mutex which has semantics
	of a recursive mutex. Recursive mutexes allow the thread that
	owns the mutex to re-aquire the mutex without blocking. The
	implementation must count the number of times that the mutex is
	acquired by the owning thread, and finally release the mutex when
	the count is decremented to zero. This class is used
	for the enforcement of type checking since the interface is
	no different from the Mutex class from which it inherits.
 */

class Api : public Oscl::Mt::Mutex::Simple::Api {
	public:
		/** The lock primitive is invoked by the user prior to
		 *	entering a critical section, to prevent other
		 *	threads of execution from manipulating data at
		 *	the same time. Typically, the current exclusion
		 *	state is saved before being changed.
		 */
		virtual void	lock() noexcept =0;

		/** The unlock primitive is invoked by the user at
		 *	the end of a critical section to restore the
		 *	exclusion state using a value saved during
		 *	the lock operation.
		 */
		virtual void	unlock() noexcept =0;
	};

}
}
}
}

#endif
