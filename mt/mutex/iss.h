/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_mt_intscopesynch_
#define _oscl_mt_intscopesynch_
#include <stddef.h>
#include "oscl/kernel/mplock.h"

/** */
namespace Oscl {
/** */
namespace Mt {

/** This class is instantiated as an automatic variable within
	a scope for the purpose of aquiring a mutex lock when the
	scope is entered, and automatically releasing the mutex lock
	when the scope is exited. The constructor aquires the mutex
	lock, and then the destructor relinquishes the mutex lock
	when the scope is exited. Since the mutex is NOT recursive,
	this type of scope synchronization should  not be used where
	the mutex lock holder may attempt to re-aquire the same mutex.
	The idea is that when this object is created, a mutex lock is
	aquired, which protects the following critical section. When
	the object goes out of scope, the mutex is released.
 */
class IntScopeSync {
	private:
		/* */
		Oscl::Kernel::IrqMaskState	_savedLevel;
	public:

		/** This constructor blocks until it aquires the mutex lock.
		 */
		inline IntScopeSync() noexcept{
			OsclKernelGlobalMpLockAquire(_savedLevel);
			}

		/** This destructor relinquishes the mutex lock.
		 */
		inline ~IntScopeSync(){
			OsclKernelGlobalMpLockRestore(_savedLevel);
			}

	private:

		/** This private new operator encourages clients to only instantiate
			these objects as automatic variables.
		 */
		void* operator	new(size_t){return (void*)0;}	// Prevents allocation from heap.
	};

}
}

#endif
