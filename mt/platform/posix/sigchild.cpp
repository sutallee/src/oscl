/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <sys/types.h>
#include <sys/wait.h>
#include <signal.h>
#include <stdio.h>
#include <errno.h>

#include "oscl/mt/posix/sema.h"
#include "oscl/mt/thread.h"
#include "oscl/mt/sigchild/init.h"
#include "oscl/mt/sigchild/wait.h"
#include "oscl/mt/sigchild/pidrec.h"
#include "oscl/mt/sigchild/loop.h"
#include "oscl/queue/queue.h"

namespace Oscl {
namespace Mt {
namespace SigChild {

static constexpr unsigned long	sleepTimeInMs	= 1000;

static Oscl::Mt::Posix::Semaphore	mutex;
static Oscl::Queue<PidRecord>		list;

void	initialize() noexcept{
	sigset_t signals;

	sigemptyset(&signals);

	// Set signal mask to block SIGCHLD event.
	sigaddset(
		&signals,
		SIGCHLD
		);

	int
	result	= pthread_sigmask(
				SIG_BLOCK,
				&signals,
				0
				);

	if(result){
		perror(__PRETTY_FUNCTION__);
		}

	mutex.signal();
	}

void	loop() noexcept{

	for(;;){

		siginfo_t	siginfo;

		mutex.wait();

		for(;;){

			siginfo.si_pid	= 0;

			int
			result	= waitid(
						P_ALL,
						0,
						&siginfo,
						WNOHANG | WEXITED
						);

			if(!result){
				if(siginfo.si_pid){

					PidRecord*	rec;

					for(
							rec	= list.first();
							rec;
							rec	= list.next(rec)
							){

						if(siginfo.si_pid == rec->_pid){

							list.remove(rec);

							Oscl::Mt::Thread*
							waiter	= (Oscl::Mt::Thread*)rec->_waiter;

							rec->_status	= siginfo.si_status;

							waiter->getSyncSignalApi().signal();

							break;
							}
						}
					continue;
					}
				}
			else if(result < 0){
				if((errno != EINTR) && (errno != ECHILD)){
					perror(__PRETTY_FUNCTION__);
					}
				}
			break;
			}

		mutex.signal();

		Oscl::Mt::Thread::sleep(sleepTimeInMs);
		}
	}

void	acquireMutex() noexcept{
	mutex.wait();
	}

void	registerSigChildHandler(PidRecord& pidRecord) noexcept{

	pidRecord._waiter	=	&Oscl::Mt::Thread::getCurrent();

	list.put(&pidRecord);
	}

void	releaseMutex() noexcept{
	mutex.signal();
	}

int		waitForProcess(PidRecord& pidRecord) noexcept{

	Oscl::Mt::Thread::getCurrent().syncWait();

	return pidRecord._status;
	}

}
}
}

