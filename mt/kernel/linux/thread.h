/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_mt_linux_kernel_threadh_
#define _oscl_mt_kernel_linux_threadh_
// I *WISH* this Linux kernel stuff would work,
// but the Linux kernel is simply NOT C++
// ready.

#error "LINUX kernel support is currently impossible."

#include <linux/sched.h>
#include "oscl/mt/runnable.h"
#include "oscl/mt/thread.h"
#include "sema.h"
#include "oscl/strings/fixed.h"

/** */
namespace Oscl {
/** */
namespace Mt {
/** */
namespace Kernel {
/** */
namespace Linux {

/** This concrete class implements a task,
	which provides a concurrency context for a
	Mt::Runnable object, in terms of a Linux kernel thread.
 */

class Thread : public Mt::Thread {
	public:
		/** Runnable interface.
		 */
		Mt::Runnable&			_runnable;

	private:
		/** The pthread identifier.
		 */
		pthread_t				_thread;

		/** The thread synchronized message semaphore.
		 */
		Semaphore				_syncSema;

		/** Thread name.
		 */
		Strings::Fixed<32>			_name;

	public: //Constructors
		/** The constructor requires: a runnable object which is started
			before the constructor completes, a short string describing this
			task instance; a priority ranging from high priority (1) to low
			priority (2); and a stack size that indicates the size of the
			stack in bytes allocated for this task.
		 */
		Thread(	Mt::Runnable&	runnable,
						const char*		name
						) noexcept;

		/** Virtual destructors rock!
		 */
		virtual ~Thread();

		/** Suspend this thread from the task level.
		 */
		void	suspend() noexcept;

		/** Suspend this thread from the interrupt level.
		 */
		void	suSuspend() noexcept;

		/** Resume this thread from the task level.
		 */
		void	resume() noexcept;

		/** Resume this thread from the interrupt level.
		 */
		void	suResume() noexcept;

		/** This operation returns the signal interface for the thread
			semaphore, which is used for synchronous message transactions.
			This is the sister operation to the thread's syncWait()
			operation.
		 */
		Mt::Sema::SignalApi&	getSyncSignalApi() noexcept;

		/** This operation blocks on the thread semaphore, waiting for
			a synchronous message operation to complete. This is the
			sister operation to the thread's getSyncSignalApi() operation.
		 */
		void		syncWait() noexcept;

		/** This operation returns the name (null terminated string)
			of the current thread.
		 */
		virtual const char*	getName() noexcept;
	};

}
}
}
}

#endif
