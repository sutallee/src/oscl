/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "thread.h"
#include "oscl/error/fatal.h"
#include <pthread.h>
#include <unistd.h>

// I *WISH* this Linux kernel stuff would work,
// but the Linux kernel is simply NOT C++
// ready.

#error "LINUX kernel support is currently impossible."

using namespace Oscl::Mt::Linux::Kernel;

static void* entryPoint(void* runnable);

static bool				keyCreated=false;
static pthread_key_t	key;

Thread::Thread(	Oscl::Mt::Runnable&	runnable,
				const char*			name
				) noexcept:
		_runnable(runnable),
		_thread(),
		_syncSema(),
		_name(name)
		{
	int err	= pthread_create(	&_thread,
								(pthread_attr_t*)0,
								&entryPoint,
								this
								);
	if(err){
		Oscl::ErrorFatal::logAndExit("Thread::Thread() cannot create thread.\n");
		}
	if(err){
		Oscl::ErrorFatal::logAndExit("Thread::Thread() cannot set key.\n");
		}
	}

Thread::~Thread(){
	int err	= pthread_cancel(_thread);
	if(err){
		Oscl::ErrorFatal::logAndExit("Thread::~Thread() cannot cancel thread.\n");
		}
	}

void	Thread::suspend() noexcept{
	Oscl::ErrorFatal::logAndExit("Thread::suspend() not implemented.\n");
	}

void	Thread::suSuspend() noexcept{
	Oscl::ErrorFatal::logAndExit("Thread::suSuspend() not implemented.\n");
	}

void	Thread::resume() noexcept{
	Oscl::ErrorFatal::logAndExit("Thread::resume() not implemented.\n");
	}

void	Thread::suResume() noexcept{
	Oscl::ErrorFatal::logAndExit("Thread::suResume() not implemented.\n");
	}

Oscl::Mt::Sema::SignalApi&	Thread::getSyncSignalApi() noexcept{
	return _syncSema;
	}

void		Thread::syncWait() noexcept{
	_syncSema.wait();
	}

const char* Thread::getName() noexcept{
	return _name.getString();
	}

static void* entryPoint(void* vthread){
	Thread*				thread	= (Thread*)vthread;
	Oscl::Mt::Thread*	thisThread	= thread;
	int err	= pthread_setspecific(key,thisThread);
	if(err){
		Oscl::ErrorFatal::logAndExit("entryPoint() cannot set key.\n");
		}
	thread->_runnable.run();
	return 0;
	}

Oscl::Mt::Thread::Thread() noexcept{
	if(!keyCreated){
		int	err;
		err	= pthread_key_create(&key,(void(*)(void*))0);
		if(err){
			Oscl::ErrorFatal::logAndExit("Oscl::Mt::Linux::Kernel::Thread::Thread() cannot create key.\n");
			}
		else{
			keyCreated=true;
			}
		}
	}

Oscl::Mt::Thread::~Thread(){
	}

Oscl::Mt::Thread&	Oscl::Mt::Thread::getCurrent() noexcept {
	Oscl::Mt::Thread*	thread				= (Oscl::Mt::Thread*)pthread_getspecific(key);
	return *thread;
	}

void	Oscl::Mt::Thread::sleep(unsigned long tocks) noexcept{
	// Assume tocks in micro-seconds
	struct timespec	ts;
	ts.tv_nsec	= (tocks%1000000)*1000;
	ts.tv_sec	= tocks/1000000;
	while(nanosleep(&ts,&ts));
	}

