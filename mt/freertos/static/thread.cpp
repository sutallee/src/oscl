/*
   Copyright (C) 2020 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "thread.h"
#include "oscl/error/fatal.h"
#include "oscl/error/info.h"
#include "oscl/memory/block.h"
#include <string.h>
#include <new>
#include <task.h>

using namespace Oscl::Mt::FreeRTOS::Static;


static void entryPoint(void* context){

	Oscl::Mt::FreeRTOS::Static::Thread*	thread	= (Oscl::Mt::FreeRTOS::Static::Thread*)context;

	thread->_runnable.run();
	}

static void	buildThread(
				Thread&			context,
				StaticTask_t&	tcb,
				StackType_t*	stack,
				size_t			stackSize,
				int				priority,
				TaskHandle_t&	thread,
				const char*		name
				) noexcept
	{
	#if 0
	/*
		BaseType_t	MPU_xTaskCreate(
						TaskFunction_t			pxTaskCode,
						const char * const		pcName,
						const uint16_t			usStackDepth,
						void * const			pvParameters,
						UBaseType_t				uxPriority,
						TaskHandle_t * const	pxCreatedTask
						);
	*/
    xTaskCreate(
		&entryPoint,	// typedef void (*TaskFunction_t)( void * );
		name,			// const char * const		pcName,
		stackSize,		// const uint16_t			usStackDepth,
		context,		// void * const				pvParameters,
		priority,		// UBaseType_t				uxPriority,
		&_thread		// TaskHandle_t * const		pxCreatedTask
		);
	#else
	/*
		 TaskHandle_t	xTaskCreateStatic(
							TaskFunction_t		pxTaskCode,
							const char* const	pcName,
							const uint32_t		ulStackDepth,
							void* const			pvParameters,
							UBaseType_t			uxPriority,
							StackType_t* const	puxStackBuffer,
							StaticTask_t* const	pxTaskBuffer
							);
	*/
	thread	= xTaskCreateStatic(
				entryPoint,		// TaskFunction_t		pxTaskCode,
				name,			// const char* const	pcName,
				stackSize/sizeof(StackType_t),		// const uint32_t		ulStackDepth,
				&context,		// void* const			pvParameters,
				priority,		// UBaseType_t			uxPriority,
				stack,			// StackType_t* const	puxStackBuffer,
				&tcb			// StaticTask_t* const	pxTaskBuffer
				);
	/*
		void vTaskSetApplicationTaskTag(
				TaskHandle_t		xTask,
				TaskHookFunction_t	pxTagValue
				);
	 */
	vTaskSetApplicationTaskTag(
		thread,							// TaskHandle_t		xTask,
		(TaskHookFunction_t)&context	// TaskHookFunction_t	pxTagValue
		);
	#endif
	}

Thread::Thread(
	Oscl::Mt::Runnable&	runnable,
	const char*			name,
	StackType_t*		stack,
	unsigned long		stackSizeInBytes,
	int					priority
	) noexcept:
		_runnable(runnable),
		_syncSema()
		{
	TaskHandle_t	thread;
	buildThread(
		*this,
		_tcb,
		stack,
		stackSizeInBytes,
		priority,
		thread,
		name
		);
	}

Thread::~Thread(){
	Oscl::ErrorFatal::logAndExit("Thread::~Thread() cannot cancel thread.\n");
	}

void	Thread::suspend() noexcept{
	/*
		void vTaskSuspend( TaskHandle_t xTaskToSuspend );
	*/
	vTaskSuspend((TaskHandle_t)&_tcb );
	}

void	Thread::suSuspend() noexcept{
	Oscl::ErrorFatal::logAndExit("Thread::suSuspend() not implemented.\n");
	}

void	Thread::resume() noexcept{
	/*
		void vTaskResume( TaskHandle_t xTaskToResume );
	 */
	vTaskResume((TaskHandle_t)&_tcb);
	}

void	Thread::suResume() noexcept{
	/*
		BaseType_t xTaskResumeFromISR( TaskHandle_t xTaskToResume );
	*/
	xTaskResumeFromISR((TaskHandle_t)&_tcb);
	}

Oscl::Mt::Sema::SignalApi&	Thread::getSyncSignalApi() noexcept{
	return _syncSema;
	}

void		Thread::syncWait() noexcept{
	_syncSema.wait();
	}

const char* Thread::getName() noexcept{
	return pcTaskGetName((TaskHandle_t)&_tcb);
	}

Oscl::Mt::Thread::Thread() noexcept{
	}

Oscl::Mt::Thread::~Thread(){
	}

Oscl::Mt::Thread&	Oscl::Mt::Thread::getCurrent() noexcept {
    return *((Thread*)xTaskGetApplicationTaskTag(xTaskGetCurrentTaskHandle()));
	}

void	Oscl::Mt::Thread::sleep(unsigned long tocks) noexcept{
	// FIXME: is tocks correct?
	// Generally sleep users assume tocks in milliseconds
	// FreeRTOS says the taskDelay argument is in "ticks".

	/*
		void vTaskDelay( const TickType_t xTicksToDelay )
	 */

	vTaskDelay(tocks);
	}

