/*
   Copyright (C) 2020 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "sema.h"
#include "oscl/error/fatal.h"

using namespace Oscl::Mt::FreeRTOS::Static;

Semaphore::Semaphore() noexcept{
	/*
		SemaphoreHandle_t	xSemaphoreCreateBinaryStatic(
								StaticSemaphore_t*	pxSemaphoreBuffer
								);
	 */

	_sema	= xSemaphoreCreateBinaryStatic(&_semaMem);
	}

Semaphore::~Semaphore(){
	}

void Semaphore::signal(void) noexcept{
	/*
		xSemaphoreGive(
			SemaphoreHandle_t	xSemaphore
			);
	 */
	xSemaphoreGive(
		_sema	// SemaphoreHandle_t	xSemaphore
		);
	}

void Semaphore::wait(void) noexcept{
	/*
	  xSemaphoreTake(
		SemaphoreHandle_t	xSemaphore,
		TickType_t			xTicksToWait
		);
	*/
	xSemaphoreTake(
		_sema,			// SemaphoreHandle_t	xSemaphore,
		portMAX_DELAY	// TickType_t			xTicksToWait
		);
	}

void Semaphore::suSignal(void) noexcept{
	/*
		xSemaphoreGiveFromISR(
			SemaphoreHandle_t	xSemaphore,
			BaseType_t*			pxHigherPriorityTaskWoken
			)
	 */
	BaseType_t	awakened;

	xSemaphoreGiveFromISR(
		_sema,		// SemaphoreHandle_t xSemaphore,
		&awakened	// BaseType_t*	pxHigherPriorityTaskWoken
		);

	portYIELD_FROM_ISR(awakened);
	}

