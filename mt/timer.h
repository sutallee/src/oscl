/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_mt_timerh_
#define _oscl_mt_timerh_

/** */
namespace Oscl {
/** */
namespace Mt {

class Semaphore;

/** This abstract class represents a general asynchronous timer.
 */

class Timer {
	public:
		/** Virtual destructors are just a good idea.
		 */
		virtual ~Timer(){};
	public:
		/** This function starts the timer, setting the expiration count
			to the specified number of tocks.
		 */
		virtual void start(unsigned long tocks) noexcept=0;

		/** This function restarts a timer that may be already running. If
			the timer is not already running, then it is started. In either
			case, the expiration count is set to the specified number for tocks.
		 */
		virtual void restart(unsigned long tocks) noexcept=0;

		/** This function stops the timer, and removes it from any operating
			system lists.
		 */
		virtual void stop() noexcept=0;
	public:
		/** This function returns true if the timer has expired.
		 */
		virtual bool hasExpired() noexcept=0;
	};

/** This abstract class defines the interface to an unspecified entity
	that is responsible for the dynamic creation of timers. Concrete
	implementations encapsulate the operating system dependent specifics
	about timer creation.
 */

class TimerFactory {
	public:
		/** Make GCC happy */
		virtual ~TimerFactory() {}

		/** Returns a pointer to a timer which can be deleted using
			the delete operator when it is no longer needed by the client.
			The semaphore is signaled whenever the timer expires.
		 */
		virtual Timer*	createTimer(Mt::Semaphore& expired) noexcept=0;
	};

}
}

#endif
