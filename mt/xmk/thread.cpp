/*
   Copyright (C) 2007 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "thread.h"
#include "oscl/error/fatal.h"
#include "oscl/error/info.h"
#include "oscl/memory/block.h"
#include <string.h>
//#include <new>
#include "xmk/tsd.h"
#include "xmk/sch_eidi.h"
#include "xmk/sleep.h"

using namespace Oscl::Mt::XMK;

/** This is a shared xmk Task Variable. Its value is changed
	each time a context switch happens.
 */
static XMK_TSD_KEY			tsdKey = XMK_INVALID_TSD_KEY;

static void entryPoint(void* context){
	Oscl::Mt::XMK::Thread*	thread	= (Thread*)context;
	Xmk_setTsdElement(tsdKey, thread);
	thread->_runnable.run();
	}

static void	buildThread(	Thread*				context,
							void*				alignedStackMemory,
							size_t				stackSize,
							XMK_THREADPRIORITY	priority,
							XMK_THREADHDL&		thread
							) noexcept
	{
	Xmk_disableSwitching();
	if(tsdKey == XMK_INVALID_TSD_KEY){
		tsdKey	= Xmk_requestTsdKey();
		}
	Xmk_enableSwitching();
	thread	= Xmk_createThread(	priority,
								entryPoint,
								context,
								XMK_TOP_OF_STACK(alignedStackMemory,stackSize),
								XMK_THREADSTATE_READY
								);
	}

Thread::Thread(	Oscl::Mt::Runnable&	runnable,
				void*				alignedStackMem,
				unsigned long		stackSizeInBytes,
				XMK_THREADPRIORITY	priority,
				const char*			name
				) noexcept:
		_runnable(runnable),
		_thread(),
		_syncSema(),
		_name(name)
		{
	buildThread(	this,
					alignedStackMem,
					stackSizeInBytes,
					priority,
					_thread
					);
	}

Thread::~Thread(){
	Oscl::ErrorFatal::logAndExit("Thread::~Thread() cannot cancel thread.\n");
	}

void	Thread::suspend() noexcept{
	Oscl::ErrorFatal::logAndExit("Thread::suspend() not implemented.\n");
	}

void	Thread::suSuspend() noexcept{
	Oscl::ErrorFatal::logAndExit("Thread::suSuspend() not implemented.\n");
	}

void	Thread::resume() noexcept{
	Oscl::ErrorFatal::logAndExit("Thread::resume() not implemented.\n");
	}

void	Thread::suResume() noexcept{
	Oscl::ErrorFatal::logAndExit("Thread::suResume() not implemented.\n");
	}

Oscl::Mt::Sema::SignalApi&	Thread::getSyncSignalApi() noexcept{
	return _syncSema;
	}

void		Thread::syncWait() noexcept{
	_syncSema.wait();
	}

Oscl::Mt::Thread::Thread() noexcept{
	}

Oscl::Mt::Thread::~Thread(){
	}

Oscl::Mt::Thread&	Oscl::Mt::Thread::getCurrent() noexcept {
    return *((Oscl::Mt::XMK::Thread*)(Xmk_getTsdElement(tsdKey)));
	}

void	Oscl::Mt::Thread::sleep(unsigned long tocks) noexcept{
	Xmk_sleep(tocks);
	}

const char* Thread::getName() noexcept{
	return _name.getString();
	}

