/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_mt_threadh_
#define _oscl_mt_threadh_
#include "oscl/mt/sema/sigapi.h"

/** */
namespace Oscl {
/** */
namespace Mt {

/** This abstract class represents the operations
	that can be performed on a thread.
 */

class Thread {
	public: //Constructors
		/** The constructor.
		 */
		Thread() noexcept;

		/** Virtual destructors rock!
		 */
		virtual ~Thread();

		/** Suspend this thread from the task level.
		 */
		virtual void	suspend() noexcept =0;

		/** Suspend this thread from the interrupt level.
		 */
		virtual void	suSuspend() noexcept =0;

		/** Resume this thread from the task level.
		 */
		virtual void	resume() noexcept =0;

		/** Resume this thread from the interrupt level.
		 */
		virtual void	suResume() noexcept =0;

		/** Invoked from user mode to cause the current thread
		 *	to be suspended for the specified number of
		 *	milliseconds. This operation is highly system
		 *	dependent. The implementation may significantly
		 *	decrease the resolution to match the minimum
		 *	system tick period. For example, if system tick
		 *	period is 10ms long, the minimum sleep time will
		 *	be at least 10ms, and a 25ms sleep time will be
		 *	at least 30ms.
		 */
		static void	sleep(unsigned long milliseconds) noexcept;

		/** This operation returns the signal interface for the thread
			semaphore, which is used for synchronous message transactions.
			This is the sister operation to the thread's syncWait()
			operation.
		 */
		virtual Sema::SignalApi&	getSyncSignalApi() noexcept = 0;

		/** This operation blocks on the thread semaphore, waiting for
			a synchronous message operation to complete. This is the
			sister operation to the thread's getSyncSignalApi() operation.
		 */
		virtual void		syncWait() noexcept = 0;

		/** This operation returns a reference to the currently executing thread.
		 */
		static Thread&	getCurrent() noexcept ;

		/** This operation returns the name (null terminated string) of the current thread.
		 */
		virtual const char* getName() noexcept = 0;
	};

};
};

#endif
