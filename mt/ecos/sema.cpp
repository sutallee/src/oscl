/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "sema.h"
#include "oscl/error/fatal.h"

using namespace Oscl;

eCosSemaphore::eCosSemaphore() noexcept:
	_signaled(false)
	{
	cyg_drv_mutex_init(&_mutex);
	cyg_drv_cond_init(&_cond,&_mutex);
//	cyg_semaphore_init(&_sema,0);
	}

eCosSemaphore::~eCosSemaphore(){
	cyg_drv_cond_destroy(&_cond);
	cyg_drv_mutex_destroy(&_mutex);
//	cyg_semaphore_destroy(&_sema);
	}

void eCosSemaphore::signal(void) noexcept{
	cyg_drv_mutex_lock(&_mutex);
	_signaled	= true;
	cyg_drv_cond_signal(&_cond);
	cyg_drv_mutex_unlock(&_mutex);
//	cyg_semaphore_post(&_sema);
	}

void eCosSemaphore::wait(void) noexcept{
	cyg_drv_dsr_lock();
	cyg_drv_mutex_lock(&_mutex);
	while(!_signaled){
		cyg_drv_cond_wait(&_cond);
		}
	_signaled	= false;
	cyg_drv_mutex_unlock(&_mutex);
	cyg_drv_dsr_unlock();
//	cyg_semaphore_wait(&_sema);
	}

void eCosSemaphore::suSignal(void) noexcept{
	_signaled	= true;
	cyg_drv_cond_signal(&_cond);
//	cyg_semaphore_post(&_sema);
//	ErrorFatal::logAndExit("eCosSemaphore::suSignal(): Not supported.\n");
	}

