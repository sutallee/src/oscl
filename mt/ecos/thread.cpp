/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <stdlib.h>
#include "thread.h"
#include "oscl/timer/busywait.h"
#include "oscl/timer/tocks.h"
#include "oscl/error/fatal.h"

using namespace Oscl;

static void entryPoint(cyg_addrword_t runnable);

static bool            threadingEnabled;	// false
static bool            keyCreated=false;
static cyg_ucount32    key;

eCosThread::eCosThread(    Mt::Runnable&   runnable,
                           const char*     name,
                           cyg_ucount32    priority,
                           cyg_ucount32    stackSize
                            ) noexcept:
        _runnable(runnable),
        _syncSema(),
        _name(name)
        {

    stackSize = stackSize == 0 ? 1 : stackSize;

    _stack = (char *) malloc (stackSize);

    if(!_stack)
        ErrorFatal::logAndExit(
                "Mt::eCosThread::eCosThread() out of memory.\n");

    cyg_thread_create (priority,              // priority
                       entryPoint,            // entry point
                       (cyg_addrword_t) this, // argument
                       (char *) name,         // overriding constant not good
                       (char*)_stack,         // stack base 
                       stackSize,             // task stack size
                       &_handle,              // handle to task
                       &_thread);             // thread object

    cyg_thread_resume (_handle);

    }

eCosThread::eCosThread(	Mt::Runnable&	runnable,
						void*			stack,
						cyg_ucount32	stackSize,
						cyg_ucount32	priority  = DEFAULT_TASK_PRIORITY,
						const char*		name=""
						) noexcept:
        _runnable(runnable),
        _syncSema(),
        _name(name),
		_stack(stack)
        {
    cyg_thread_create (priority,              // priority
                       entryPoint,            // entry point
                       (cyg_addrword_t) this, // argument
                       (char *) name,         // overriding constant not good
                       _stack,                // stack base 
                       stackSize,             // task stack size
                       &_handle,              // handle to task
                       &_thread);             // thread object

    cyg_thread_resume (_handle);
    }

eCosThread::~eCosThread(){
    cyg_thread_kill(_handle);
    free(_stack);
    }

void    eCosThread::suspend() noexcept{
    ErrorFatal::logAndExit("eCosThread::suspend() not implemented.\n");
    }

void    eCosThread::suSuspend() noexcept{
    ErrorFatal::logAndExit("eCosThread::suSuspend() not implemented.\n");
    }

void    eCosThread::resume() noexcept{
    ErrorFatal::logAndExit("eCosThread::resume() not implemented.\n");
    }

void    eCosThread::suResume() noexcept{
    ErrorFatal::logAndExit("eCosThread::suResume() not implemented.\n");
    }

Mt::SignalApi&    eCosThread::getSyncSignalApi() noexcept{
    return _syncSema;
    }

void        eCosThread::syncWait() noexcept{
    _syncSema.wait();
    }

const char* eCosThread::getName() noexcept{
    return _name.getString();
    }

//==============================================================================

static void entryPoint(cyg_addrword_t vthread){
    eCosThread*        thread    = (eCosThread*)vthread;
    Mt::Thread*            thisThread    = thread;

    cyg_thread_set_data(key, (CYG_ADDRWORD) thisThread);
	threadingEnabled	= true;

    thread->_runnable.run();
    }

Mt::Thread::Thread() noexcept{
    if(!keyCreated){

        key = cyg_thread_new_data_index();

        keyCreated=true;
        }
    }

Mt::Thread::~Thread(){
    }

Mt::Thread&    Mt::Thread::getCurrent() noexcept {
    Mt::Thread*    thread             = (Mt::Thread*)cyg_thread_get_data(key);
    return *thread;
    }

void    Mt::Thread::sleep(unsigned long milliseconds) noexcept{
	// Presumably, keyCreated means the kernel has been initialized
	// and we are multi-threading.
	if(threadingEnabled){
		cyg_thread_delay(OsclTimerMillisecondsToTocks(milliseconds));
		}
	else{
		OsclTimerBusyWaitDelay(milliseconds);
		}
    }

