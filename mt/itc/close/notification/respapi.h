/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_mt_itc_close_notification_itc_respapih_
#define _oscl_mt_itc_close_notification_itc_respapih_

#include "reqapi.h"
#include "oscl/mt/itc/mbox/clirsp.h"

/** */
namespace Oscl {

/** */
namespace Mt {

/** */
namespace Itc {

/** */
namespace Close {

/** */
namespace Notification {

/** */
namespace Resp {

/**	This class describes an ITC client interface. The
	interface includes the definition of client response messages
	based on their corresponding server request messages and
	associated payloads, as well as an interface that is to be
	implemented by a client that uses the corresponding server
	asynchronously.
 */
/**	This interface is used by clients to determine if a
	server is closing.
 */
class Api {
	public:
		/**	This describes a client response message that corresponds
			to the ClosingReq message described in the "reqapi.h"
			header file. This ClosingResp response message actually
			contains a ClosingReq request message.
		 */
		typedef Oscl::Mt::Itc::CliResponse<
					Oscl::Mt::Itc::Close::Notification::Req::Api,
					Api,
					Oscl::Mt::Itc::Close::Notification::
					Req::Api::ClosingPayload
					>		ClosingResp;

		/**	This describes a client response message that corresponds
			to the CancelClosingReq message described in the "reqapi.h"
			header file. This CancelClosingResp response message actually
			contains a CancelClosingReq request message.
		 */
		typedef Oscl::Mt::Itc::CliResponse<
					Oscl::Mt::Itc::Close::Notification::Req::Api,
					Api,
					Oscl::Mt::Itc::Close::Notification::
					Req::Api::CancelClosingPayload
					>		CancelClosingResp;

	public:
		/** Make the compiler happy. */
		virtual ~Api(){}

		/** This operation is invoked within the client thread
			whenever a ClosingResp message is received.
		 */
		virtual void	response(	Oscl::Mt::Itc::Close::Notification::
									Resp::Api::ClosingResp& msg
									) noexcept=0;

		/** This operation is invoked within the client thread
			whenever a CancelClosingResp message is received.
		 */
		virtual void	response(	Oscl::Mt::Itc::Close::Notification::
									Resp::Api::CancelClosingResp& msg
									) noexcept=0;

	};

}
}
}
}
}
}
#endif
