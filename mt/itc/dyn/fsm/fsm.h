/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_mt_itc_dyn_statevarh_
#define _oscl_mt_itc_dyn_statevarh_

/** */
namespace Oscl {
/** */
namespace Mt {
/** */
namespace Itc {
/** */
namespace Dyn {

/** This class implements the behavior of a Dynamic Service.
 */
class StateVar {
	public:
		/** This interface is to be implemented by the Dynamic Service.
			It defines the operations whose sequence is controlled
			by the finite state machine (FSM) of this state variable.
			The FSM is implemented using the GOF State Pattern.
		 */
		class ContextApi {
			public:	// Actions
				/** Shut-up GCC. */
				virtual ~ContextApi() {}
				/** This operation is invoked to cause the Dyanmic
					Service to do whatever is necessary to open or
					attach to its lower layers. This must be carried
					out synchronously.
				 */
				virtual void	activateDynamicService() noexcept=0;
				/** This operation is invoked prior to the
					deactivateDynamicService() operation to allow
					the dynamic service to cause sub-services to
					preempt synchronous upper layer operations
					returning a failure result. This operation
					must be carried out synchrously.
				 */
//				virtual void	prepareForDeactivation() noexcept=0;
				/** This operation is invoked to cause the Dyanmic
					Service to do whatever is necessary to close or
					detach from its lower layers. This must be carried
					out synchronously.
				 */
				virtual void	deactivateDynamicService() noexcept=0;
				/** This operation is invoked to cause the context
					to invoke the returnToSender() operation of the current
					open request.
				 */
				virtual void	finishedWithOpenReq() noexcept=0;
				/** This operation is invoked to cause the context
					to invoke the returnToSender() operation of the current
					prepare-to-close request.
				 */
				virtual void	finishedWithClosePrepReq() noexcept=0;
				/** This operation is invoked to cause the context
					to invoke the returnToSender() operation of the current
					close request.
				 */
				virtual void	finishedWithCloseReq() noexcept=0;
				/** This operation cancels the reference release request
					notification specified by the current cancel request.
				 */
				virtual void	cancelNotificationReq() noexcept=0;
				/** This operation is invoked to cause the context
					to invoke the returnToSender() operation of the current
					increment request.
				 */
				virtual void	returnIncrReq() noexcept=0;
				/** This operation is invoked to cause the context
					to invoke the returnToSender() operation of the current
					decrement request.
				 */
				virtual void	returnDecrReq() noexcept=0;
				/** This operation is invoked to increment the Dynamic
					Service reference count by one.
				 */
				virtual void	incrementRefCount() noexcept=0;
				/** This operation is invoked to decrement the Dynamic
					Service reference count by one.
				 */
				virtual void	decrementRefCount() noexcept=0;
				/** This operation removes all pending reference release
					notification requests from the pending queue and invokes
					returnToSender() on each. This signals all registered
					clients to release their references to this Dynamic
					Service.
				 */
				virtual void	notifyClosing() noexcept=0;
				/** This operation returns true if the Dynamic Service
					reference counter is zero.
				 */
				virtual bool	refCountIsZero() noexcept=0;
			};
	private:
		/** This abstract state represents the events to the
			Dynamic Service finite state machine using
			the GOF State Pattern. The operations implemented
			by this class execute the default event actions.
			In all cases, the default action is to invoke the
			protocolError() operation.
		 */
		class State {
			public:
				/** The cosntructor.
				 */
				State() noexcept;

				/** Shut-up GCC. */
				virtual ~State() {}

			public:
				/** This operation is invoked when the Dynamic Service receives an open
					request.
				 */
				virtual void	open(ContextApi& context,StateVar& internal) const noexcept;
				/** This operation is invoked when the Dynamic Service receives a close
					request.
				 */
				virtual void	close(ContextApi& context,StateVar& internal) const noexcept;
				/** This operation is invoked when the Dynamic Service receives a
					prepare-to-close request.
				 */
				virtual void	deactivateReq(ContextApi& context,StateVar& internal) const noexcept;
				/** This operation is invoked when the Dynamic Service receives a
					reference release notification request.
				 */
				virtual void	notificationReq(ContextApi& context,StateVar& internal) const noexcept;
				/** This operation is invoked when the Dynamic Service receives a
					request to cancel a reference release notification request.
				 */
				virtual void	cancelNotificationReq(ContextApi& context,StateVar& internal) const noexcept;
				/** This operation is invoked when the Dynamic Service receives a
					request to increment its reference count.
				 */
				virtual void	incrementRefCount(ContextApi& context,StateVar& internal) const noexcept;
				/** This operation is invoked when the Dynamic Service receives a
					request to decrement its reference count.
				 */
				virtual void	decrementRefCount(ContextApi& context,StateVar& internal) const noexcept;
			};
		/** This class represents the Dynamic Service state when it is ready to either
			be opened or destroyed.
		 */
		class ClosedState : public State {
			public:
				/** The cosntructor.
				 */
				ClosedState() noexcept;
			public:
				/** This operation causes the Dyamic Service to initialize and move
					to the Opened state.
				 */
				void	open(ContextApi& context,StateVar& internal) const noexcept;
			};

		/** This class represents the Dynamic Service state when it is ready
			to be used by the system.
		 */
		class OpenedState : public State {
			public:
				/** The constructor.
				 */
				OpenedState() noexcept;
			public:
				/** This operation causes the FSM to prepare the Dyanmic Service to be destroyed.
				 */
				void	deactivateReq(ContextApi& context,StateVar& internal) const noexcept;
				/** This operation is invoked when the Dynamic Service receives a
					reference release notification request.
				 */
				void	notificationReq(ContextApi& context,StateVar& internal) const noexcept;
				/** This operation is invoked when the Dynamic Service receives a
					request to cancel a reference release notification request.
				 */
				void	cancelNotificationReq(ContextApi& context,StateVar& internal) const noexcept;
				/** This operation is invoked when the Dynamic Service receives a
					request to increment its reference count.
				 */
				void	incrementRefCount(ContextApi& context,StateVar& internal) const noexcept;
				/** This operation is invoked when the Dynamic Service receives a
					request to decrement its reference count.
				 */
				void	decrementRefCount(ContextApi& context,StateVar& internal) const noexcept;
			};
		/** */
		class ReferencedState : public State {
			public:
				/** */
				ReferencedState() noexcept;
			public:
				/** This operation is invoked when the Dynamic Service receives a
					reference release notification request.
				 */
				void	notificationReq(ContextApi& context,StateVar& internal) const noexcept;
				/** This operation is invoked when the Dynamic Service receives a
					request to cancel a reference release notification request.
				 */
				void	cancelNotificationReq(ContextApi& context,StateVar& internal) const noexcept;
				/** This operation is invoked when the Dynamic Service receives a
					request to increment its reference count.
				 */
				void	incrementRefCount(ContextApi& context,StateVar& internal) const noexcept;
				/** This operation is invoked when the Dynamic Service receives a
					request to decrement its reference count.
				 */
				void	decrementRefCount(ContextApi& context,StateVar& internal) const noexcept;
			};

		/** */
		class UnreferencedState : public State {
			public:
				/** */
				UnreferencedState() noexcept;
			public:
				/** This operation is invoked when the Dynamic Service receives an open
					request.
				 */
				void	open(ContextApi& context,StateVar& internal) const noexcept;

				/** This operation is invoked when the Dynamic Service receives a close
					request.
				 */
				void	close(ContextApi& context,StateVar& internal) const noexcept;
			};
	private:
		/** This stateless variable represents the Dynamic Service ready-to-close
			state and is shared by all instances of this state variable class.
		 */
		const static UnreferencedState		_unreferencedState;
		/** This stateless variable represents the Dynamic Service preparing-to-close
			state and is shared by all instances of this state variable class.
		 */
		const static ReferencedState	_referencedState;
		/** This stateless variable represents the Dynamic Service ready
			state and is shared by all instances of this state variable class.
		 */
		const static OpenedState				_openedState;
		/** This stateless variable represents the Dynamic Service closed
			state and is shared by all instances of this state variable class.
		 */
		const static ClosedState			_closedState;
	private:
		/** This member references the context.
		 */
		ContextApi&							_context;
		/** This member references the current state of the Dyanmic Service.
		 */
		const State*						_state;
	public:
		/** The constructor requires a reference to the Dyanmic Service context.
		 */
		StateVar(ContextApi& context) noexcept;

		/** Shut-up GCC. */
		virtual ~StateVar() {}

	public:
		/** This operation is invoked when the Dynamic Service receives an open
			request.
		 */
		virtual void	open() noexcept;
		/** This operation is invoked when the Dynamic Service receives a close
			request.
		 */
		virtual void	close() noexcept;
		/** This operation is invoked when the Dynamic Service receives a
			prepare-to-close request.
		 */
		virtual void	deactivateReq() noexcept;
		/** This operation is invoked when the Dynamic Service receives a
			reference release notification request.
		 */
		virtual void	notificationReq() noexcept;
		/** This operation is invoked when the Dynamic Service receives a
			request to cancel a reference release notification request.
		 */
		virtual void	cancelNotificationReq() noexcept;
		/** This operation is invoked when the Dynamic Service receives a
			request to increment its reference count.
		 */
		virtual void	incrementRefCount() noexcept;
		/** This operation is invoked when the Dynamic Service receives a
			request to decrement its reference count.
		 */
		virtual void	decrementRefCount() noexcept;
	private:
		friend class State;
		friend class UnreferencedState;
		friend class ReferencedState;
		friend class OpenedState;
		friend class ClosedState;
		/** This operation is invoked when the FSM detects
			a Dynamic Service protocol violation.
		 */
		void	protocolError() noexcept;
		/** This operation is invoked to change the current
			Dynamic Service state to the closed state.
		 */
		void	changeToClosedState() noexcept;
		/** This operation is invoked to change the current
			Dynamic Service state to the ready state.
		 */
		void	changeToOpenedState() noexcept;
		/** This operation is invoked to change the current
			Dynamic Service state to the preparing-to-close state.
		 */
		void	changeToReferencedState() noexcept;
		/** This operation is invoked to change the current
			Dynamic Service state to the ready-to-close state.
		 */
		void	changeToUnreferencedState() noexcept;
	};

};
};
};
};

#endif
