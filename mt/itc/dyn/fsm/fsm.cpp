/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "fsm.h"
#include "oscl/error/fatal.h"

using namespace Oscl;
using namespace Oscl::Mt::Itc::Dyn;

const StateVar::UnreferencedState		StateVar::_unreferencedState;
const StateVar::ReferencedState			StateVar::_referencedState;
const StateVar::OpenedState				StateVar::_openedState;
const StateVar::ClosedState				StateVar::_closedState;

//StateVar

StateVar::StateVar(ContextApi& context) noexcept:
		_context(context),
		_state(&_closedState)
		{
	}

void	StateVar::open() noexcept{
	_state->open(_context,*this);
	}

void	StateVar::close() noexcept{
	_state->close(_context,*this);
	}

void	StateVar::deactivateReq() noexcept{
	_state->deactivateReq(_context,*this);
	}

void	StateVar::notificationReq() noexcept{
	_state->notificationReq(_context,*this);
	}

void	StateVar::cancelNotificationReq() noexcept{
	_state->cancelNotificationReq(_context,*this);
	}

void	StateVar::incrementRefCount() noexcept{
	_state->incrementRefCount(_context,*this);
	}

void	StateVar::decrementRefCount() noexcept{
	_state->decrementRefCount(_context,*this);
	}

void	StateVar::protocolError() noexcept{
	ErrorFatal::logAndExit("StateVar::protocolError\n");
	}

void	StateVar::changeToClosedState() noexcept{
	_state	= &_closedState;
	}

void	StateVar::changeToOpenedState() noexcept{
	_state	= &_openedState;
	}

void	StateVar::changeToReferencedState() noexcept{
	_state	= &_referencedState;
	}

void	StateVar::changeToUnreferencedState() noexcept{
	_state	= &_unreferencedState;
	}

//

StateVar::State::State() noexcept
		{
	}

void	StateVar::State::open(	ContextApi&	context,
								StateVar&	statevar
								) const noexcept{
	statevar.protocolError();
	}

void	StateVar::State::close(	ContextApi&	context,
								StateVar&	statevar
								) const noexcept{
	statevar.protocolError();
	}

void	StateVar::State::deactivateReq(	ContextApi&	context,
											StateVar&	statevar
											) const noexcept{
	statevar.protocolError();
	}

void	StateVar::State::notificationReq(	ContextApi&	context,
												StateVar&	statevar
												) const noexcept{
	statevar.protocolError();
	}

void	StateVar::State::cancelNotificationReq(	ContextApi&	context,
													StateVar&	statevar
													) const noexcept{
	statevar.protocolError();
	}

void	StateVar::State::incrementRefCount(	ContextApi&	context,
											StateVar&	statevar
											) const noexcept{
	statevar.protocolError();
	}

void	StateVar::State::decrementRefCount(	ContextApi&	context,
											StateVar&	statevar
											) const noexcept{
	statevar.protocolError();
	}


//ClosedState

StateVar::ClosedState::ClosedState() noexcept
		{
	}

void	StateVar::ClosedState::open(	ContextApi&	context,
										StateVar&	statevar
										) const noexcept{
	context.activateDynamicService();
	context.finishedWithOpenReq();
	statevar.changeToOpenedState();
	}

//OpenedState

StateVar::OpenedState::OpenedState() noexcept
		{
	}

void	StateVar::OpenedState::deactivateReq(	ContextApi&	context,
												StateVar&	statevar
												) const noexcept{
	if(context.refCountIsZero()){
		context.finishedWithClosePrepReq();
		statevar.changeToUnreferencedState();
		}
	else{
		statevar.changeToReferencedState();
		}
	context.notifyClosing();
	}

void	StateVar::OpenedState::notificationReq(	ContextApi&	context,
													StateVar&	statevar
													) const noexcept{
	}

void	StateVar::OpenedState::cancelNotificationReq(	ContextApi&	context,
															StateVar&	statevar
															) const noexcept{
	context.cancelNotificationReq();
	}

void	StateVar::OpenedState::incrementRefCount(	ContextApi&	context,
													StateVar&	statevar
													) const noexcept{
	context.incrementRefCount();
	context.returnIncrReq();
	}

void	StateVar::OpenedState::decrementRefCount(	ContextApi&	context,
													StateVar&	statevar
													) const noexcept{
	context.decrementRefCount();
	context.returnDecrReq();
	}

//ReferencedState

StateVar::ReferencedState::ReferencedState() noexcept
		{
	}

void	StateVar::ReferencedState::notificationReq(	ContextApi&	context,
																StateVar&	statevar
																) const noexcept{
	context.notifyClosing();
	}

void	StateVar::ReferencedState::cancelNotificationReq(	ContextApi&	context,
																	StateVar&	statevar
																	) const noexcept{
	context.cancelNotificationReq();
	}

void	StateVar::ReferencedState::incrementRefCount(	ContextApi&	context,
														StateVar&	statevar
														) const noexcept{
	context.incrementRefCount();
	context.returnIncrReq();
	}

void	StateVar::ReferencedState::decrementRefCount(	ContextApi&	context,
														StateVar&	statevar
														) const noexcept{
	context.decrementRefCount();
	context.returnDecrReq();
	if(context.refCountIsZero()){
		context.finishedWithClosePrepReq();
		statevar.changeToUnreferencedState();
		}
	}

//UnreferencedState

StateVar::UnreferencedState::UnreferencedState() noexcept
		{
	}

void	StateVar::UnreferencedState::open(	ContextApi&	context,
											StateVar&	statevar
											) const noexcept{
	context.finishedWithOpenReq();
	statevar.changeToOpenedState();
	}

void	StateVar::UnreferencedState::close(	ContextApi&	context,
											StateVar&	statevar
											) const noexcept{
	context.deactivateDynamicService();
	context.finishedWithCloseReq();
	statevar.changeToClosedState();
	}

