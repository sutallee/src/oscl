/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "fsm.h"
#include "oscl/error/fatal.h"

using namespace Oscl::Mt::Itc::Dyn::Unit;

static const StateVar::ClosedState				closedState;
static const StateVar::ListeningState			listeningState;
static const StateVar::ActiveState				activeState;
static const StateVar::OpeningState				openingState;
static const StateVar::ClosingState				closingState;
static const StateVar::DeactivateOpenState		deactivateOpenState;
static const StateVar::DeactivateCloseState		deactivateCloseState;

void	StateVar::State::open(	ContextApi&	context,
								StateVar&	state
								) const noexcept{
	state.protocolError();
	}

void	StateVar::State::close(	ContextApi&	context,
								StateVar&	state
								) const noexcept{
	state.protocolError();
	}

void	StateVar::State::create(	ContextApi&	context,
									StateVar&	state
									) const noexcept{
	state.protocolError();
	}

void	StateVar::State::release(	ContextApi&	context,
									StateVar&	state
									) const noexcept{
	state.protocolError();
	}

void	StateVar::State::createCanceled(	ContextApi&	context,
											StateVar&	state
											) const noexcept{
	state.protocolError();
	}

void	StateVar::State::releaseCanceled(	ContextApi&	context,
											StateVar&	state
											) const noexcept{
	state.protocolError();
	}

void	StateVar::State::deactivateDone(	ContextApi&	context,
											StateVar&	state
											) const noexcept{
	state.protocolError();
	}

void	StateVar::ClosedState::open(	ContextApi&	context,
										StateVar&	state
										) const noexcept{
	context.reqNewCreateNotification();
	context.openComplete();
	state.changeToListeningState();
	}

void	StateVar::ListeningState::close(	ContextApi&	context,
											StateVar&	state
											) const noexcept{
	context.cancelCreateNotification();
	state.changeToClosingState();
	}

void	StateVar::ListeningState::create(	ContextApi&	context,
											StateVar&	state
											) const noexcept{
	if(context.match()) {
		context.activateService();
		context.reqReleaseNotification();
		state.changeToActiveState();
		}
	else {
		context.reqCreateNotification();
		}
	}

void	StateVar::ActiveState::close(	ContextApi&	context,
										StateVar&	state
										) const noexcept{
	// Change state first to allow "synchronous done" callback.
	state.changeToDeactivateCloseState();
	context.deactivateService();
	}

void	StateVar::ActiveState::release(	ContextApi&	context,
										StateVar&	state
										) const noexcept{
	// Change state first to allow "synchronous done" callback.
	state.changeToDeactivateOpenState();
	context.deactivateService();
	}

void	StateVar::
DeactivateCloseState::release(	ContextApi&	context,
								StateVar&	state
								) const noexcept{
	// Ignore this
	}

void	StateVar::
DeactivateCloseState::deactivateDone(	ContextApi&	context,
										StateVar&	state
										) const noexcept{
	context.cancelReleaseNotification();
	state.changeToClosingState();
	}

void	StateVar::
DeactivateOpenState::close(	ContextApi&	context,
							StateVar&	state
							) const noexcept{
	state.changeToDeactivateCloseState();
	}

void	StateVar::
DeactivateOpenState::deactivateDone(	ContextApi&	context,
										StateVar&	state
										) const noexcept{
	context.reqNewCreateNotification();
	state.changeToListeningState();
	}

void	StateVar::OpeningState::close(	ContextApi&	context,
										StateVar&	state
										) const noexcept{
	state.changeToClosingState();
	}

void	StateVar::OpeningState::create(	ContextApi&	context,
										StateVar&	state
										) const noexcept{
	// We're expecting this if we're canceling
	// the creation notification request. Just
	// ignore it.
	}

void	StateVar::OpeningState::release(	ContextApi&	context,
											StateVar&	state
											) const noexcept{
	// We're expecting this if we're canceling
	// the release notification request. Just
	// ignore it.
	}

void	StateVar::OpeningState::createCanceled(	ContextApi&	context,
												StateVar&	state
												) const noexcept{
	context.reqNewCreateNotification();
	context.openComplete();
	state.changeToListeningState();
	}

void	StateVar::OpeningState::releaseCanceled(	ContextApi&	context,
													StateVar&	state
													) const noexcept{
	context.reqNewCreateNotification();
	context.openComplete();
	state.changeToListeningState();
	}

void	StateVar::ClosingState::open(	ContextApi&	context,
										StateVar&	state
										) const noexcept{
	state.changeToOpeningState();
	}

void	StateVar::ClosingState::create(	ContextApi&	context,
										StateVar&	state
										) const noexcept{
	// We're expecting this if we're canceling
	// the creation notification request. Just
	// ignore it.
	}

void	StateVar::ClosingState::release(	ContextApi&	context,
											StateVar&	state
											) const noexcept{
	// We're expecting this if we're canceling
	// the release notification request. Just
	// ignore it.
	}

void	StateVar::ClosingState::createCanceled(	ContextApi&	context,
												StateVar&	state
												) const noexcept{
	context.closeComplete();
	state.changeToClosedState();
	}

void	StateVar::ClosingState::releaseCanceled(	ContextApi&	context,
													StateVar&	state
													) const noexcept{
	context.closeComplete();
	state.changeToClosedState();
	}

StateVar::StateVar(ContextApi& context) noexcept:
		_context(context),
		_state(&closedState)
		{
	}

void	StateVar::open() noexcept{
	_state->open(_context,*this);
	}

void	StateVar::close() noexcept{
	_state->close(_context,*this);
	}

void	StateVar::create() noexcept{
	_state->create(_context,*this);
	}

void	StateVar::release() noexcept{
	_state->release(_context,*this);
	}

void	StateVar::createCanceled() noexcept{
	_state->createCanceled(_context,*this);
	}

void	StateVar::releaseCanceled() noexcept{
	_state->releaseCanceled(_context,*this);
	}

void	StateVar::deactivateDone() noexcept{
	_state->deactivateDone(_context,*this);
	}

void	StateVar::changeToClosedState() noexcept{
	_state	= &closedState;
	}

void	StateVar::changeToListeningState() noexcept{
	_state	= &listeningState;
	}

void	StateVar::changeToActiveState() noexcept{
	_state	= &activeState;
	}

void	StateVar::changeToClosingState() noexcept{
	_state	= &closingState;
	}

void	StateVar::changeToOpeningState() noexcept{
	_state	= &openingState;
	}

void	StateVar::changeToDeactivateOpenState() noexcept{
	_state	= &deactivateOpenState;
	}

void	StateVar::changeToDeactivateCloseState() noexcept{
	_state	= &deactivateCloseState;
	}

void	StateVar::protocolError() noexcept{
	Oscl::ErrorFatal::logAndExit("Oscl::Mt::Itc::Dyn::Unit::StateVar\n");
	}

