/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_mt_itc_dyn_monitor_unit_fsm_fsmh_
#define _oscl_mt_itc_dyn_monitor_unit_fsm_fsmh_

/** */
namespace Oscl {
/** */
namespace Mt {
/** */
namespace Itc {
/** */
namespace Dyn {
/** */
namespace Unit {

/** This class implements the behavior for a Dynamic Object Monitor
	using the GOF State Pattern. The GOF flyweight pattern is also
	applied to the states, which allows a single set of stateless
	state objects to be shared by all instances of this FSM.

	The behavior proceeds normally as follows. The Monitor
	is opened by its context and then begins waiting for creation
	notifications from an Advertiser. If the Dynamic Object created
	is one which matches this Monitor's needs, the Monitor registers
	for release notification with the Dynamic Object, and activates
	its context. The association with the Dynamic Object will be
	deactivated either by a close request from the context or by
	a release notification from the Dynamic Object.
 */
class StateVar {
	public:
		/** This interface is to be implemented by the context
			that uses the StateVar.
		 */
		class ContextApi {
			public:
				/** Make the stupid compiler happy. */
				virtual ~ContextApi() {}

			public:
				/** This operation causes a create notification request
					to be constructed/initialized, and sent to the
					Advertiser asynchronously. Instantiation initializes
					the presumed state of the Advertiser. Subsequent
					uses of the notification request over the life
					of the (reqCreateNotification) Monitor do not
					re-instantiate the request preserving the presumed state.
				 */
				virtual void	reqNewCreateNotification() noexcept=0;

				/** This operation causes the already initialized
					create notification request to be sent back to
					the advertiser asynchronously.
				 */
				virtual void	reqCreateNotification() noexcept=0;

				/** This operation causes a create notification cancel
					request to be sent to the advertiser asynchronously.
				 */
				virtual void	cancelCreateNotification() noexcept=0;

				/** This operation causes a release notification request
					to be sent to the Dynamic Object asynchronously.
				 */
				virtual void	reqReleaseNotification() noexcept=0;

				/** This operation causes a release notification cancel
					request to be sent to the Dynamic Object asynchronously.
				 */
				virtual void	cancelReleaseNotification() noexcept=0;

				/** This operation informs the context that it is finished
					with the open sequence and may return the outstanding
					open request to its sender.
				 */
				virtual void	openComplete() noexcept=0;

				/** This operation informs the context that it is finished
					with the close sequence and may return the outstanding
					close request to its sender.
				 */
				virtual void	closeComplete() noexcept=0;

				/** This operation informs the context that it may
					begin using the Dynamic Object.
				 */
				virtual void	activateService() noexcept=0;

				/** This operation informs the context that it must
					begin to asynchronously disassociate itself
					from the Dynamic Object. When the disassociation
					is complete it should issue the deactivateDone
					event to the FSM,
				 */
				virtual void	deactivateService() noexcept=0;

				/** This operation must examine the current Dynamic Object
					and return true if it is a match to the needs of
					the context. If so, the process of activating
					the relationship with the Dynamic Object will
					proceed.
				 */
				virtual bool	match() noexcept=0;
			};

	public:
		/** This abstract base class represents all of the events
			received by the FSM and implements the "unhandled event"
			behavior of the FSM. All state in this FSM are sub-classes
			to this base class.
		 */
		class State {
			public:
				/** Shut-up GCC. */
				virtual ~State() {}

				/** This event indicates that the context is ready
					to be begin listening for Dynamic Objects.
				 */
				virtual void	open(	ContextApi&	context,
										StateVar&	state
										) const noexcept;

				/** This event indicates that the context is ready
					to be stop listening for Dynamic Objects.
				 */
				virtual void	close(	ContextApi&	context,
										StateVar&	state
										) const noexcept;

				/** This event indicates that a new Dynamic Object
					has come into existence.
				 */
				virtual void	create(	ContextApi&	context,
										StateVar&	state
										) const noexcept;

				/** This event indicates that the Dynamic Object
					is ending its life-cycle.
				 */
				virtual void	release(	ContextApi&	context,
											StateVar&	state
											) const noexcept;

				/** This event indicates that a create notification
					cancellation request has completed.
				 */
				virtual void	createCanceled(	ContextApi&	context,
												StateVar&	state
												) const noexcept;

				/** This event indicates that a release notfication
					cancellation request has complted.
				 */
				virtual void	releaseCanceled(	ContextApi&	context,
													StateVar&	state
													) const noexcept;

				/** This event indicates that the deactivation
					sequence has been completed by the context.
				 */
				virtual void	deactivateDone(	ContextApi&	context,
												StateVar&	state
												) const noexcept;
			};

	public:
		/** This class represents the Closed state of the FSM.
			In this state, the FSM is idle. 
		 */
		class ClosedState : public State {
			public:
				/** This event indicates that the context is ready
					to be begin listening for Dynamic Objects.
				 */
				void	open(	ContextApi&	context,
								StateVar&	state
								) const noexcept;
			};

	public:
		/** This class represents the Listening state of the FSM.
			In this state, the FSM is listening for create notifications
			from the Advertiser.
		 */
		class ListeningState : public State {
			public:
				/** This event indicates that the context is ready
					to be stop listening for Dynamic Objects.
				 */
				void	close(	ContextApi&	context,
								StateVar&	state
								) const noexcept;
				/** This event indicates that a new Dynamic Object
					has come into existence.
				 */
				void	create(	ContextApi&	context,
								StateVar&	state
								) const noexcept;
			};

	public:
		/** This class represents the Active state of the FSM.
			In this state, a Dynamic Object is currently associated
			with this Monitor.
		 */
		class ActiveState : public State {
			public:
				/** This event indicates that the context is ready
					to be stop listening for Dynamic Objects.
				 */
				void	close(	ContextApi&	context,
								StateVar&	state
								) const noexcept;
				/** This event indicates that the Dynamic Object
					is ending its life-cycle.
				 */
				void	release(	ContextApi&	context,
									StateVar&	state
									) const noexcept;
			};

	public:
		/** This class represents the DeactivateClose state of the FSM.
			In this state, the Monitor is in the process of releasing
			its association with the Dynamic Object and is waiting
			for deactivation to complete.
		 */
		class DeactivateCloseState : public State {
			private:
				/** This event indicates that the Dynamic Object
					is ending its life-cycle.
				 */
				void	release(	ContextApi&	context,
									StateVar&	state
									) const noexcept;
				/** This event indicates that the deactivation
					sequence has been completed by the context.
				 */
				void	deactivateDone(	ContextApi&	context,
										StateVar&	state
										) const noexcept;
			};

	public:
		/** This class represents the DeactivateClose state of the FSM.
			In this state, the Monitor is in the process of releasing
			its association with the Dynamic Object and is waiting
			for deactivation to complete.
		 */
		class DeactivateOpenState : public State {
			private:
				/** This event indicates that the context is ready
					to be stop listening for Dynamic Objects.
				 */
				void	close(	ContextApi&	context,
								StateVar&	state
								) const noexcept;
				/** This event indicates that the deactivation
					sequence has been completed by the context.
				 */
				void	deactivateDone(	ContextApi&	context,
										StateVar&	state
										) const noexcept;
			};

	public:
		/** This class represents the Opening state of the FSM.
			In this state, the Monitor has received an open event
			and is preparing to begin listening for Dynamic Object
			creations after it cleans up from a previous close
			event.
		 */
		class OpeningState : public State {
			public:
				/** This event indicates that the context is ready
					to be stop listening for Dynamic Objects.
				 */
				void	close(	ContextApi&	context,
								StateVar&	state
								) const noexcept;
				/** This event indicates that a new Dynamic Object
					has come into existence.
				 */
				void	create(	ContextApi&	context,
								StateVar&	state
								) const noexcept;
				/** This event indicates that the Dynamic Object
					is ending its life-cycle.
				 */
				void	release(	ContextApi&	context,
									StateVar&	state
									) const noexcept;
				/** This event indicates that a create notification
					cancellation request has completed.
				 */
				void	createCanceled(	ContextApi&	context,
										StateVar&	state
										) const noexcept;
				/** This event indicates that a release notfication
					cancellation request has complted.
				 */
				void	releaseCanceled(	ContextApi&	context,
											StateVar&	state
											) const noexcept;
			};

	public:
		/** This class represents the Closing state of the FSM.
			In this state, the Monitor has received a close event
			and is cancelling release notifications with the
			Dynamic Object and create notifications with the
			advertiser.
		 */
		class ClosingState : public State {
			public:
				/** This event indicates that the context is ready
					to be begin listening for Dynamic Objects.
				 */
				void	open(	ContextApi&	context,
								StateVar&	state
								) const noexcept;
				/** This event indicates that a new Dynamic Object
					has come into existence.
				 */
				void	create(	ContextApi&	context,
								StateVar&	state
								) const noexcept;
				/** This event indicates that the Dynamic Object
					is ending its life-cycle.
				 */
				void	release(	ContextApi&	context,
									StateVar&	state
									) const noexcept;
				/** This event indicates that a create notification
					cancellation request has completed.
				 */
				void	createCanceled(	ContextApi&	context,
										StateVar&	state
										) const noexcept;
				/** This event indicates that a release notfication
					cancellation request has complted.
				 */
				void	releaseCanceled(	ContextApi&	context,
											StateVar&	state
											) const noexcept;
			};

	private:
		/** A reference to the context (duh!)
		 */
		ContextApi&		_context;

		/** This is a pointer to the current FSM state.
		 */
		const State*	_state;

	public:
		/** The constructor requires a reference to the
			context.
		 */
		StateVar(ContextApi& context) noexcept;

		/** This event indicates that the context is ready
			to be begin listening for Dynamic Objects.
		 */
		void	open() noexcept;

		/** This event indicates that the context is ready
			to be stop listening for Dynamic Objects.
		 */
		void	close() noexcept;

		/** This event indicates that a new Dynamic Object
			has come into existence.
		 */
		void	create() noexcept;

		/** This event indicates that the Dynamic Object
			is ending its life-cycle.
		 */
		void	release() noexcept;

		/** This event indicates that a create notification
			cancellation request has completed.
		 */
		void	createCanceled() noexcept;

		/** This event indicates that a release notfication
			cancellation request has complted.
		 */
		void	releaseCanceled() noexcept;

		/** This event indicates that the deactivation
			sequence has been completed by the context.
		 */
		void	deactivateDone() noexcept;

	private:
		friend class State;
		friend class ClosedState;
		friend class ListeningState;
		friend class ActiveState;
		friend class ClosingState;
		friend class OpeningState;
		friend class DeactivateOpenState;
		friend class DeactivateCloseState;

		/** Invoked by the individual states to cause
			a transition to the Closed state.
		 */
		void	changeToClosedState() noexcept;

		/** Invoked by the individual states to cause
			a transition to the Listening state.
		 */
		void	changeToListeningState() noexcept;

		/** Invoked by the individual states to cause
			a transition to the Active state.
		 */
		void	changeToActiveState() noexcept;

		/** Invoked by the individual states to cause
			a transition to the Closing state.
		 */
		void	changeToClosingState() noexcept;

		/** Invoked by the individual states to cause
			a transition to the Opening state.
		 */
		void	changeToOpeningState() noexcept;

		/** Invoked by the individual states to cause
			a transition to the DeactivateClose state.
		 */
		void	changeToDeactivateCloseState() noexcept;

		/** Invoked by the individual states to cause
			a transition to the DeactivateOpen state.
		 */
		void	changeToDeactivateOpenState() noexcept;

		/** Invoked by the base State when an unexpected
			event occurs. This should *NEVER* happen, and
			will only happen if the context is incorrectly
			implemented.
		 */
		void	protocolError() noexcept;
	};

}
}
}
}
}


#endif
