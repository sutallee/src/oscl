/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_mt_itc_dyn_monitor_unit_monitor_monitorh_
#define _oscl_mt_itc_dyn_monitor_unit_monitor_monitorh_
#include <new>
#include "part.h"
#include "oscl/mt/itc/srv/close.h"
#include "oscl/done/operation.h"

/** */
namespace Oscl {
/** */
namespace Mt {
/** */
namespace Itc {
/** */
namespace Dyn {
/** */
namespace Unit {

/** This abstract base class builds upon the Monitor Part to
	provide more complete support for Dynamic Object Monitors
	which are themselves dynamic. The intent is for this
	monitor to be constructed and then receive an open
	message from its creator to begin operation. Later,
	when the creator is ready to delete the Monitor
	it sends the Monitor a close request which causes
	the monitor to go through its deactivation procedure
	before returning the close message to the creator,
	at which time the creator may then safely reclaim
	the resources occupied by the Monitor.
 */
template <class dynSrvType>
class Monitor :	public Oscl::Mt::Itc::Dyn::Unit::Part<dynSrvType>,
				public Oscl::Mt::Itc::Srv::CloseSync,
				public Oscl::Mt::Itc::Srv::Close::Req::Api
				{
	private:
		/** This is a callback mechanism used to notifiy
			the Monitor when the start() operation that
			it issues to the Part has completed.
		 */
		Oscl::Done::Operation< Monitor<dynSrvType> >	_startDoneOp;

		/** This is a callback mechanism used to notifiy
			the Monitor when the stop() operation that
			it issues to the Part has completed.
		 */
		Oscl::Done::Operation< Monitor<dynSrvType> >	_stopDoneOp;

		/** This is a pointer to the open request being
			processed when starting the Monitor. The
			open request is returnedToSender() after the
			startup completes.
		 */
		Oscl::Mt::Itc::Srv::Open::Req::Api::OpenReq*	_openReq;

		/** This is a pointer to the close request being
			processed when stoping the Monitor. The
			close request is returnedToSender() after the
			stoping process completes.
		 */
		Oscl::Mt::Itc::Srv::Close::Req::Api::CloseReq*	_closeReq;

	public:
		/** The constructor requires:
			advSAP -
				A reference to the ITC interface of
				the Advertiser.
			advFind -
				A reference to the Advertiser's
				Dynamic Object search mechanism.
			myPapi -
				A reference to the mailbox/thread
				in which this Monitor operates.
			iterator -
				A reference to the iterator used
				to search for matching Dynamic Objects.
		 */
		Monitor(	typename Oscl::Mt::Itc::Dyn::Adv::Cli::
                    Req::Api<dynSrvType>::SAP&		advSAP,
					Oscl::Mt::Itc::Dyn::Adv::Cli::
					SyncApi<dynSrvType>&			advFind,
					Oscl::Mt::Itc::PostMsgApi&		myPapi,
					Oscl::Mt::Itc::Dyn::Adv::
					Iterator<dynSrvType>&			iterator
					) noexcept;

		/** This operation is invoked by the context when
			it has ended all associations with any Dynamic
			Object associated with this Part. The deactivation
			process starts when the sub-class deactivate()
			operation is invoked.
		 */
		void	deactivateDone() noexcept;

	private:	// Oscl::Mt::Itc::Srv::Close::Req::Api
		/** This operation is invoked when the Monitor receives
			an open request message.
		 */
		void	request(	Oscl::Mt::Itc::Srv::
							Open::Req::Api::OpenReq&	msg
							) noexcept;

		/** This operation is invoked when the Monitor receives
			a close request message.
		 */
		void	request(	Oscl::Mt::Itc::Srv::
							Close::Req::Api::CloseReq&	msg
							) noexcept;
	private:
		/** This operation is invoked when the Part finishes
			the startup sequence.
		 */
		void	startDone() noexcept;

		/** This operation is invoked when the Part finishes
			the stop sequence.
		 */
		void	stopDone() noexcept;

	protected: // Part
		/** This operation MUST be implemented by the sub-class.
			This operation is invoked to cause the sub-class
			to synchronously establish its associations with
			the Dynamic Object.
		 */
		virtual void	activate() noexcept=0;

		/** This operation MUST be implemented by the sub-class.
			This operation is invoked to cause the sub-class
			to begin the asynchronous process of severing its
			relationship with the Dynamic Object. When all
			relationships with the Dynamic Object have been
			severed, the context/sub-class invokes the
			deactivateDone() operation of this Part.
		 */
		virtual void	deactivate() noexcept=0;
	};

template <class dynSrvType>
Monitor<dynSrvType>::Monitor(	typename Oscl::Mt::Itc::Dyn::Adv::Cli::
			                    Req::Api<dynSrvType>::SAP&		advSAP,
								Oscl::Mt::Itc::Dyn::Adv::Cli::
								SyncApi<dynSrvType>&			advFind,
								Oscl::Mt::Itc::PostMsgApi&		myPapi,
								Oscl::Mt::Itc::Dyn::Adv::
								Iterator<dynSrvType>&			iterator
								) noexcept:
		Oscl::Mt::Itc::Dyn::Unit::Part<dynSrvType>(advSAP,advFind,myPapi,iterator),
		Oscl::Mt::Itc::Srv::CloseSync(*this,myPapi),
		_startDoneOp(*this,&Monitor<dynSrvType>::startDone),
		_stopDoneOp(*this,&Monitor<dynSrvType>::stopDone),
		_openReq(0),
		_closeReq(0)
		{
	}

template <class dynSrvType>
void	Monitor<dynSrvType>::deactivateDone() noexcept{
	Part<dynSrvType>::deactivateDone();
	}

template <class dynSrvType>
void	Monitor<dynSrvType>::request(	Oscl::Mt::Itc::Srv::
										Open::Req::Api::OpenReq&	msg
										) noexcept{
	_openReq	= &msg;
	Part<dynSrvType>::start(_startDoneOp);
	}

template <class dynSrvType>
void	Monitor<dynSrvType>::request(	Oscl::Mt::Itc::Srv::
										Close::Req::Api::CloseReq&	msg
										) noexcept{
	_closeReq	= &msg;
	Part<dynSrvType>::stop(_stopDoneOp);
	}

template <class dynSrvType>
void	Monitor<dynSrvType>::startDone() noexcept{
	_openReq->returnToSender();
	_openReq	= 0;
	}

template <class dynSrvType>
void	Monitor<dynSrvType>::stopDone() noexcept{
	_closeReq->returnToSender();
	_closeReq	= 0;
	}

}
}
}
}
}

#endif
