/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_mt_itc_dyn_monitor_unit_monitor_parth_
#define _oscl_mt_itc_dyn_monitor_unit_monitor_parth_
#include <new>
#include "oscl/mt/itc/dyn/monitor/unit/fsm/fsm.h"
#include "oscl/mt/itc/dyn/adv/respapi.h"
#include "oscl/mt/itc/dyn/adv/clisync.h"
#include "oscl/mt/itc/dyn/cli/respapi.h"
#include "oscl/mt/itc/srv/close.h"
#include "oscl/memory/block.h"
#include "oscl/done/api.h"

/** */
namespace Oscl {
/** */
namespace Mt {
/** */
namespace Itc {
/** */
namespace Dyn {
/** */
namespace Unit {

/** This abstract base class is intended to capture the behavior
	of a Dynamic Object Monitor within the Dynamic Object framework.

	Sub-classes implement the pure-virtual operations of this
	class are able to track and respond to the life-cycle of
	particular types of Dynamic Objects.

	Each sub-classes instance expects to be associated with one
	and only one Dynamic Object at any given time.
 */
template <class dynSrvType>
class Part :	public Oscl::Mt::Itc::Dyn::Adv::Cli::Resp::Api<dynSrvType>,
				public Oscl::Mt::Itc::Dyn::Cli::Resp::Api<dynSrvType>,
				public Oscl::Mt::Itc::Dyn::Unit::StateVar::ContextApi
				{
	private:
		/** This class is used by the implementation to instantiate
			a create notification request to the Advertiser.
		 */
		class CreateMsg {
			private:
				/** Memory for the request payload.
				 */
				typename Oscl::Mt::Itc::Dyn::Adv::Cli::
				Req::Api<dynSrvType>::CreatePayload	_payload;

				/** Memory for the response/request itself.
				 */
				typename Oscl::Mt::Itc::Dyn::Adv::Cli::
				Resp::Api<dynSrvType>::CreateResp		_resp;

			public:
				/** The constructor requires the usual ITC response
					message parameters.
				 */
				CreateMsg(	Oscl::Mt::Itc::Dyn::Adv::Cli::
							Req::Api<dynSrvType>&			srv,
							Oscl::Mt::Itc::Dyn::Adv::Cli::
							Resp::Api<dynSrvType>&			cli,
							Oscl::Mt::Itc::PostMsgApi&		cliPapi
							) noexcept;

				/** This operation resets the create response/request
					payload ID field. This has the same affect on
					the create notification message as re-constructing
					the payload, causing the Advertiser to believe
					that this client does not know if there are any
					existing Dynamic Objects in its list.
				 */
				void	reset() noexcept;

				/** This operation returns a reference to the
					server request message portion of the create
					request. This is useful when posting the message
					to the Advertiser.
				 */
				typename Oscl::Mt::Itc::Dyn::Adv::Cli::
				Req::Api<dynSrvType>::CreateReq&	getReq() noexcept;
			};

	private:
		/** This class is used by the implementation to instantiate
			a create notification cancel request to the Advertiser.
		 */
		class CancelCreateMsg {
			private:
				/** Memory for the cancel request payload.
				 */
				typename Oscl::Mt::Itc::Dyn::Adv::Cli::
				Req::Api<dynSrvType>::CancelCreatePayload	_payload;

				/** Memory for the cancel response/request itself.
				 */
				typename Oscl::Mt::Itc::Dyn::Adv::Cli::
				Resp::Api<dynSrvType>::CancelResp			_resp;
			public:
				/** The constructor requires the usual ITC response
					message parameters and a reference to the
					create request that is to be canceled.
				 */
				CancelCreateMsg(	Oscl::Mt::Itc::Dyn::Adv::Cli::
									Req::Api<dynSrvType>&				srv,
									Oscl::Mt::Itc::Dyn::Adv::Cli::
									Resp::Api<dynSrvType>&				cli,
									Oscl::Mt::Itc::PostMsgApi&			cliPapi,
									typename Oscl::Mt::Itc::Dyn::Adv::Cli::
									Req::Api<dynSrvType>::CreateReq&	req
									) noexcept;

				/** This operation returns a reference to the
					server request message portion of the create
					request. This is useful when posting the message
					to the Advertiser.
				 */
				typename Oscl::Mt::Itc::Dyn::Adv::Cli::
				Req::Api<dynSrvType>::CancelCreateReq&	getReq() noexcept;
			};

	private:
		/** This class is used by the implementation to instantiate
			a release notification request to the Dynamic Object.
		 */
		class ReleaseMsg {
			private:
				/** Memory for the request payload.
				 */
				typename Oscl::Mt::Itc::Dyn::Cli::
				Req::Api<dynSrvType>::ReleasePayload	_payload;

				/** Memory for the response/request itself.
				 */
				typename Oscl::Mt::Itc::Dyn::Cli::
				Resp::Api<dynSrvType>::ReleaseResp	_resp;
			public:
				/** The constructor requires the usual ITC response
					message parameters.
				 */
				ReleaseMsg(	Oscl::Mt::Itc::Dyn::Cli::
							Req::Api<dynSrvType>&			srv,
							Oscl::Mt::Itc::Dyn::Cli::
							Resp::Api<dynSrvType>&			cli,
							Oscl::Mt::Itc::PostMsgApi&		cliPapi
							) noexcept;

				/** This operation returns a reference to the
					server request message portion of the release
					notification request. This is useful when posting
					the message to the Dynamic Object.
				 */
				typename Oscl::Mt::Itc::Dyn::Cli::
				Req::Api<dynSrvType>::ReleaseReq&	getReq() noexcept;
			};

	private:
		/** This class is used by the implementation to instantiate
			a create notification cancel request to the Advertiser.
		 */
		class CancelReleaseMsg {
			private:
				/** Memory for the cancel request payload.
				 */
				typename Oscl::Mt::Itc::Dyn::Cli::
				Req::Api<dynSrvType>::CancelReleasePayload	_payload;

				/** Memory for the cancel response/request itself.
				 */
				typename Oscl::Mt::Itc::Dyn::Cli::
				Resp::Api<dynSrvType>::CancelReleaseResp		_resp;
			public:
				/** The constructor requires the usual ITC response
					message parameters and a reference to the
					release request that is to be canceled.
				 */
				CancelReleaseMsg(	Oscl::Mt::Itc::Dyn::Cli::
									Req::Api<dynSrvType>&				srv,
									Oscl::Mt::Itc::Dyn::Cli::
									Resp::Api<dynSrvType>&				cli,
									Oscl::Mt::Itc::PostMsgApi&			cliPapi,
									typename Oscl::Mt::Itc::Dyn::Cli::
									Req::Api<dynSrvType>::ReleaseReq&	req
									) noexcept;

				/** This operation returns a reference to the
					server request message portion of the cancelation
					request. This is useful when posting the message
					to the Dynamic Object.
				 */
				typename Oscl::Mt::Itc::Dyn::Cli::
				Req::Api<dynSrvType>::CancelReleaseReq&	getReq() noexcept;
			};

	private:
		/** This union represents the memory required for either
			a create notification message or a release notification
			message.
		 */
		union RequestMem {
			/** Sufficient memory for a create notification message
				and its payload.
			 */
			Oscl::Memory::AlignedBlock<sizeof(CreateMsg)>	create;

			/** Sufficient memory for a release notification message
				and its payload.
			 */
			Oscl::Memory::AlignedBlock<sizeof(ReleaseMsg)>	release;
			};

	private:
		/** This union represents the memory required for either
			a create notification cancelation message or a release
			notification cancelation message.
		 */
		union CancelMem {
			/** Sufficient memory for a create notification cancelation
				message and its payload.
			 */
			Oscl::Memory::AlignedBlock<sizeof(CancelCreateMsg)>		create;

			/** Sufficient memory for a release notification cancelation
				message and its payload.
			 */
			Oscl::Memory::AlignedBlock<sizeof(CancelReleaseMsg)>	release;
			};

	private:
		/** Raw memory for either a create notification or a
			release notification message.
		 */
		RequestMem			_requestMem;

		/** Raw memory for either a create notification cancelation
			or a release notification cancelation message.
		 */
		CancelMem			_cancelMem;

		/** A reference to the Advertiser that is used to
			listen for Dynamic Objects.
		 */
		typename Oscl::Mt::Itc::Dyn::Adv::Cli::
		Req::Api<dynSrvType>::SAP&					_advSAP;

		/** A reference to the Advertiser's interface for
			searching the current list of Dynamic Objects.
		 */
		Oscl::Mt::Itc::Dyn::Adv::Cli::
		SyncApi<dynSrvType>&						_advFind;

		/** A reference to the iterator used when searching
			for particular Dynamic Objects. This search criteria
			is supplied by the context.
		 */
		Oscl::Mt::Itc::Dyn::Adv::
		Iterator<dynSrvType>&						_iterator;


		/** This is a reference to the state variable that
			implements the behavior of this Part.
		 */
		Oscl::Mt::Itc::Dyn::Unit::StateVar			_state;

		/** This is a pointer to the current "Done" operation.
		 */
		Oscl::Done::Api*							_done;

		/** This is a pointer to the create notification message
			after it has been instantiated.
		 */
		CreateMsg*									_createMsg;

		/** This is a pointer to the release notification message
			after it has been instantiated.
		 */
		ReleaseMsg*									_releaseMsg;

		/** This is a reference to the mailbox/thread in which
			this Part is executing.
		 */
		Oscl::Mt::Itc::PostMsgApi&					_myPapi;

	public:
		/** This is a reference counting handle to the
			Dynamic Object after it has been found 
			in the Advertiser's list.
		 */
		Oscl::Mt::Itc::Dyn::
		Handle<dynSrvType>							_handle;

	public:
		/** The constructor requires:
			advSAP - A reference to the Advertiser.
			advFind - A reference to the Advertiser's search
			interface.
			myPapi - A reference to the mailbox/thread in which
			this Part will execute.
			iterator - A reference to the Dynamic Object
			matching/search criteria.
		 */
		Part(	typename
				Oscl::Mt::Itc::Dyn::Adv::Cli::
				Req::Api<dynSrvType>::SAP&		advSAP,
				Oscl::Mt::Itc::Dyn::Adv::Cli::
				SyncApi<dynSrvType>&			advFind,
				Oscl::Mt::Itc::PostMsgApi&		myPapi,
				Oscl::Mt::Itc::Dyn::Adv::
				Iterator<dynSrvType>&			iterator
				) noexcept;

		/** This operation is invoked by the context to
			begin the operation of this Part. It MUST
			only be invoked ONCE until the deactivateDone()
			operation is called at the end of the
			deactivation process.
		 */
		void	start(Oscl::Done::Api& startDone) noexcept;

		/** This operation is invoked by the context to
			deactivate the operation of this Part. At the end
			of the deactivation process started by this
			operation, all associations with any Dynamic Object
			will have been properly ended.
		 */
		void	stop(Oscl::Done::Api& stopDone) noexcept;

		/** This operation is invoked by the context when
			it has ended all associations with any Dynamic
			Object associated with this Part. The deactivation
			process starts when the sub-class deactivate()
			operation is invoked.
		 */
		void	deactivateDone() noexcept;

	private:	// Oscl::Mt::Itc::Dyn::Adv::Cli::Resp::Api
		/** This operation is invoked whenever a create
			notification response message is received.
		 */
		void	response(	typename Oscl::Mt::Itc::Dyn::Adv::Cli::
							Resp::Api<dynSrvType>::CreateResp&	msg
							) noexcept;

		/** This operation is invoked whenever a create
			notification cancelation response message is
			received.
		 */
		void	response(	typename Oscl::Mt::Itc::Dyn::Adv::Cli::
							Resp::Api<dynSrvType>::CancelResp&	msg
							) noexcept;

	private:	// Oscl::Mt::Itc::Dyn::Cli::Resp::Api
		/** This operation is invoked whenever a release
			notification response message is received.
		 */
		void	response(	typename Oscl::Mt::Itc::Dyn::Cli::
							Resp::Api<dynSrvType>::ReleaseResp&	msg
							) noexcept;

		/** This operation is invoked whenever a release
			notification cancelation response message is
			received.
		 */
		void	response(	typename Oscl::Mt::Itc::Dyn::Cli::
							Resp::Api<dynSrvType>::CancelReleaseResp&	msg
							) noexcept;

	private: // Oscl::Mt::Itc::Dyn::Unit::StateVar::ContextApi
		/** This operation causes a create notification request
			to be constructed/initialized, and sent to the
			Advertiser asynchronously. Instantiation initializes
			the presumed state of the Advertiser. Subsequent
			uses of the notification request over the life
			of the (reqCreateNotification) Monitor do not
			re-instantiate the request preserving the presumed state.
		 */
		void	reqNewCreateNotification() noexcept;

		/** This operation causes the already initialized
			create notification request to be sent back to
			the advertiser asynchronously.
		 */
		void	reqCreateNotification() noexcept;

		/** This operation causes a create notification cancel
			request to be sent to the advertiser asynchronously.
		 */
		void	cancelCreateNotification() noexcept;

		/** This operation causes a release notification request
			to be sent to the Dynamic Object asynchronously.
		 */
		void	reqReleaseNotification() noexcept;

		/** This operation causes a release notification cancel
			request to be sent to the Dynamic Object asynchronously.
		 */
		void	cancelReleaseNotification() noexcept;

		/** This operation informs the context that it is finished
			with the open sequence and may return the outstanding
			open request to its sender.
		 */
		void	openComplete() noexcept;

		/** This operation informs the context that it is finished
			with the close sequence and may return the outstanding
			close request to its sender.
		 */
		void	closeComplete() noexcept;

		/** This operation informs the context that it may
			begin using the Dynamic Object.
		 */
		void	activateService() noexcept;

		/** This operation informs the context that it must
			begin to asynchronously disassociate itself
			from the Dynamic Object. When the disassociation
			is complete it should issue the deactivateDone
			event to the FSM,
		 */
		void	deactivateService() noexcept;

		/** This operation must examine the current Dynamic Object
			and return true if it is a match to the needs of
			the context. If so, the process of activating
			the relationship with the Dynamic Object will
			proceed.
		 */
		bool	match() noexcept;

	protected:
		/** This operation MUST be implemented by the sub-class.
			This operation is invoked to cause the sub-class
			to synchronously establish its associations with
			the Dynamic Object.
		 */
		virtual void	activate() noexcept=0;

		/** This operation MUST be implemented by the sub-class.
			This operation is invoked to cause the sub-class
			to begin the asynchronous process of severing its
			relationship with the Dynamic Object. When all
			relationships with the Dynamic Object have been
			severed, the context/sub-class invokes the
			deactivateDone() operation of this Part.
		 */
		virtual void	deactivate() noexcept=0;
	};

template <class dynSrvType>
Part<dynSrvType>::CreateMsg::
CreateMsg(	Oscl::Mt::Itc::Dyn::Adv::Cli::
			Req::Api<dynSrvType>&			srv,
			Oscl::Mt::Itc::Dyn::Adv::Cli::
			Resp::Api<dynSrvType>&			cli,
			Oscl::Mt::Itc::PostMsgApi&		cliPapi
			) noexcept:
		_payload(),
		_resp(srv,cli,cliPapi,_payload)
		{
	}

template <class dynSrvType>
void	Part<dynSrvType>::CreateMsg::reset() noexcept{
	_payload.setLastId(0);
	}

template <class dynSrvType>
typename Oscl::Mt::Itc::Dyn::Adv::Cli::Req::Api<dynSrvType>::
CreateReq&	Part<dynSrvType>::CreateMsg::getReq() noexcept{
	return _resp.getSrvMsg();
	}

template <class dynSrvType>
Part<dynSrvType>::CancelCreateMsg::
CancelCreateMsg(	Oscl::Mt::Itc::Dyn::Adv::Cli::
					Req::Api<dynSrvType>&				srv,
					Oscl::Mt::Itc::Dyn::Adv::Cli::
					Resp::Api<dynSrvType>&				cli,
					Oscl::Mt::Itc::PostMsgApi&			cliPapi,
					typename Oscl::Mt::Itc::Dyn::Adv::Cli::
					Req::Api<dynSrvType>::CreateReq&	req
					) noexcept:
		_payload(req),
		_resp(srv,cli,cliPapi,_payload)
		{
	}

template <class dynSrvType>
typename Oscl::Mt::Itc::Dyn::Adv::Cli::Req::Api<dynSrvType>::
CancelCreateReq&	Part<dynSrvType>::CancelCreateMsg::getReq() noexcept{
	return _resp.getSrvMsg();
	}

template <class dynSrvType>
Part<dynSrvType>::ReleaseMsg::
ReleaseMsg(	Oscl::Mt::Itc::Dyn::Cli::
			Req::Api<dynSrvType>&			srv,
			Oscl::Mt::Itc::Dyn::Cli::
			Resp::Api<dynSrvType>&			cli,
			Oscl::Mt::Itc::PostMsgApi&		cliPapi
			) noexcept:
		_payload(),
		_resp(srv,cli,cliPapi,_payload)
		{
	}

template <class dynSrvType>
typename Oscl::Mt::Itc::Dyn::Cli::Req::Api<dynSrvType>::
ReleaseReq&	Part<dynSrvType>::ReleaseMsg::getReq() noexcept{
	return _resp.getSrvMsg();
	}

template <class dynSrvType>
Part<dynSrvType>::CancelReleaseMsg::
CancelReleaseMsg(	Oscl::Mt::Itc::Dyn::Cli::
					Req::Api<dynSrvType>&				srv,
					Oscl::Mt::Itc::Dyn::Cli::
					Resp::Api<dynSrvType>&				cli,
					Oscl::Mt::Itc::PostMsgApi&			cliPapi,
					typename Oscl::Mt::Itc::Dyn::Cli::
					Req::Api<dynSrvType>::ReleaseReq&	req
					) noexcept:
		_payload(req),
		_resp(srv,cli,cliPapi,_payload)
		{
	}

template <class dynSrvType>
Part<dynSrvType>::Part(	typename
						Oscl::Mt::Itc::Dyn::Adv::Cli::
						Req::Api<dynSrvType>::SAP&		advSAP,
						Oscl::Mt::Itc::Dyn::Adv::Cli::
						SyncApi<dynSrvType>&			advFind,
						Oscl::Mt::Itc::PostMsgApi&		myPapi,
						Oscl::Mt::Itc::Dyn::Adv::
						Iterator<dynSrvType>&			iterator
						) noexcept:
		_advSAP(advSAP),
		_advFind(advFind),
		_iterator(iterator),
		_state(*this),
		_myPapi(myPapi)
		{
	}

template <class dynSrvType>
void	Part<dynSrvType>::start(Oscl::Done::Api& done) noexcept{
	_done	= &done;
	_state.open();
	}

template <class dynSrvType>
void	Part<dynSrvType>::stop(Oscl::Done::Api& done) noexcept{
	_done	= &done;
	_state.close();
	}

template <class dynSrvType>
void	Part<dynSrvType>::deactivateDone() noexcept{
	_state.deactivateDone();
	}

template <class dynSrvType>
typename Oscl::Mt::Itc::Dyn::Cli::Req::Api<dynSrvType>::
CancelReleaseReq&	Part<dynSrvType>::CancelReleaseMsg::getReq() noexcept{
	return _resp.getSrvMsg();
	}

template <class dynSrvType>
void	Part<dynSrvType>::response(	typename Oscl::Mt::Itc::Dyn::Adv::Cli::
										Resp::Api<dynSrvType>::CreateResp&	msg
										) noexcept{
	_state.create();
	}

template <class dynSrvType>
void	Part<dynSrvType>::response(	typename Oscl::Mt::Itc::Dyn::Adv::Cli::
										Resp::Api<dynSrvType>::CancelResp&	msg
										) noexcept{
	_state.createCanceled();
	}

template <class dynSrvType>
void	Part<dynSrvType>::response(	typename Oscl::Mt::Itc::Dyn::Cli::
										Resp::Api<dynSrvType>::ReleaseResp&	msg
										) noexcept{
	_state.release();
	}

template <class dynSrvType>
void	Part<dynSrvType>::response(	typename Oscl::Mt::Itc::Dyn::Cli::
										Resp::Api<dynSrvType>::
										CancelReleaseResp&	msg
										) noexcept{
	_state.releaseCanceled();
	}

template <class dynSrvType>
void	Part<dynSrvType>::reqNewCreateNotification() noexcept{
	_createMsg	= new (&_requestMem)
						CreateMsg(	_advSAP.getReqApi(),
									*this,
									_myPapi
									);
	_advSAP.post(_createMsg->getReq());
	}

template <class dynSrvType>
void	Part<dynSrvType>::reqCreateNotification() noexcept{
	_advSAP.post(_createMsg->getReq());
	}

template <class dynSrvType>
void	Part<dynSrvType>::cancelCreateNotification() noexcept{
	CancelCreateMsg*	msg	= new (&_cancelMem)
								CancelCreateMsg(	_advSAP.getReqApi(),
													*this,
													_myPapi,
													_createMsg->getReq()
													);
	_advSAP.post(msg->getReq());
	}

template <class dynSrvType>
void	Part<dynSrvType>::reqReleaseNotification() noexcept{
	_releaseMsg	= new (&_requestMem)
						ReleaseMsg(	_handle->getDynSrv(),
									*this,
									_myPapi
									);
	_handle->getCliSAP().post(_releaseMsg->getReq());
	}

template <class dynSrvType>
void	Part<dynSrvType>::cancelReleaseNotification() noexcept{
	CancelReleaseMsg*	msg	= new (&_cancelMem)
								CancelReleaseMsg(	_handle->getDynSrv(),
													*this,
													_myPapi,
													_releaseMsg->getReq()
													);
	_handle->getCliSAP().post(msg->getReq());
	}

template <class dynSrvType>
void	Part<dynSrvType>::openComplete() noexcept{
	_handle	= 0;
	_done->done();
	_done	= 0;
	}

template <class dynSrvType>
void	Part<dynSrvType>::closeComplete() noexcept{
	_handle	= 0;
	_done->done();
	_done	= 0;
	}

template <class dynSrvType>
void	Part<dynSrvType>::activateService() noexcept{
	activate();
	}

template <class dynSrvType>
void	Part<dynSrvType>::deactivateService() noexcept{
	deactivate();
	}

template <class dynSrvType>
bool	Part<dynSrvType>::match() noexcept{
	_handle	= _advFind.find(_iterator);
	return (_handle != 0);
	}

}
}
}
}
}

#endif
