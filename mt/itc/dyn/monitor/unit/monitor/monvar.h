/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_mt_itc_dyn_monitor_unit_monitor_monvarh_
#define _oscl_mt_itc_dyn_monitor_unit_monitor_monvarh_
#include "monitor.h"

/** */
namespace Oscl {
/** */
namespace Mt {
/** */
namespace Itc {
/** */
namespace Dyn {
/** */
namespace Unit {

/** This class extends the Monitor by implementing a concrete
	class that may be used as a member variable by a context
	class rather than requiring the context to be a sub-class
	of Monitor. This has advantages in terms of the name-space
	clashes with the operations of the context and promotes
	composition over inheritance. Rather than requiring the
	sub-class to implement the activate() and deactivate()
	operations of the Monitor/Part, the context provides
	equivalent member function pointers through the
	constructor.

	Other benefits include the ability of an ITC context
	to support multiple MonitorVar's since the activate()
	operations deactivate() are no longer inherited and
	therefore will not clash.

	Otherwise, the behavior is the same as the Monitor.
 */
template <class dynSrvType, class Context>
class MonitorVar : public Monitor<dynSrvType> {
	public:
		/** This defines the type of the activate
			member function operation.
		 */
		typedef void	(Context::* ActivateOp )();

		/** This defines the type of the deactivate
			member function operation.
		 */
		typedef void	(Context::* DeactivateOp )();
	private:
		/** This is a reference to the containing
			context.
		 */
		Context&							_context;

		/** This is a reference to the activate
			member function of the containing
			context.
		 */
		ActivateOp							_activate;

		/** This is a reference to the deactivate
			member function of the containing
			context.
		 */
		DeactivateOp						_deactivate;

	public:
		/** The constructor requires:
			advSAP -
				A reference to the ITC interface of
				the Advertiser.
			advFind -
				A reference to the Advertiser's
				Dynamic Object search mechanism.
			myPapi -
				A reference to the mailbox/thread
				in which this Monitor operates.
			iterator -
				A reference to the iterator used
				to search for matching Dynamic Objects.
			context	-
				A reference to the containing context
				(the object that contains the MonitorVar.)
			activate	-
				A reference to the activate member function
				for this MonitorVar within the containing
				context.
			deactivate	-
				A reference to the deactivate member function
				for this MonitorVar within the containing
				context.
		 */
		MonitorVar(	
					typename
					Oscl::Mt::Itc::Dyn::Adv::Cli::
					Req::Api<dynSrvType>::SAP&		advSAP,
					Oscl::Mt::Itc::Dyn::Adv::Cli::
					SyncApi<dynSrvType>&			advFind,
					Oscl::Mt::Itc::PostMsgApi&		myPapi,
					Oscl::Mt::Itc::Dyn::Adv::
					Iterator<dynSrvType>&			iterator,
					Context&						context,
					ActivateOp						activate,
					DeactivateOp					deactivate
					) noexcept:
					Monitor<dynSrvType>(advSAP,advFind,myPapi,iterator),
					_context(context),
					_activate(activate),
					_deactivate(deactivate)
					{}

	private:
		/** This operation is invoked by the Monitor parent class
			when a matching Dynamic Object becomes available. In
			response the context is notified and synchronously
			begins operation with the Dynamic Object.
		 */
		void	activate() noexcept{
			(_context.*_activate)();
			}

		/** This operation is invoked by the Monitor parent class
			when the current Dynamic Object closes. In response
			the context is notified and asynchronously begins
			the process of ending its associations with the
			Dynamic Object. When the context completes the
			process, it invokes the MonitorVar deactivateDone()
			operation.
		 */
		void	deactivate() noexcept{
			(_context.*_deactivate)();
			}
	};

}
}
}
}
}

#endif
