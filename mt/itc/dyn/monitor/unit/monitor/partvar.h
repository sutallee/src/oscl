/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_mt_itc_dyn_monitor_unit_monitor_partvarh_
#define _oscl_mt_itc_dyn_monitor_unit_monitor_partvarh_
#include "oscl/mt/itc/dyn/adv/cli.h"
#include "part.h"

/** */
namespace Oscl {
/** */
namespace Mt {
/** */
namespace Itc {
/** */
namespace Dyn {
/** */
namespace Unit {

template <class dynSrvType, class Context>
class PartVar : public Part<dynSrvType> {
	public:
		/** */
		typedef void	(Context::* ActivateOp )();
		/** */
		typedef void	(Context::* DeactivateOp )();
	private:
		/** */
		Context&							_context;
		/** */
		ActivateOp							_activate;
		/** */
		DeactivateOp						_deactivate;

	public:
		/** */
		PartVar(
					typename
					Oscl::Mt::Itc::Dyn::Adv::Cli::
					Req::Api<dynSrvType>::SAP&		advSAP,
					Oscl::Mt::Itc::Dyn::Adv::Cli::
					SyncApi<dynSrvType>&			advFind,
					Oscl::Mt::Itc::PostMsgApi&		myPapi,
					Oscl::Mt::Itc::Dyn::Adv::
					Iterator<dynSrvType>&			iterator,
					Context&						context,
					ActivateOp						activate,
					DeactivateOp					deactivate
					) noexcept;

	protected:
		/** */
		void	activate() noexcept{
			(_context.*_activate)();
			}
		/** */
		void	deactivate() noexcept{
			(_context.*_deactivate)();
			}
	};

template <class dynSrvType, class Context>
PartVar<dynSrvType,Context>::PartVar(
					typename
					Oscl::Mt::Itc::Dyn::Adv::Cli::
					Req::Api<dynSrvType>::SAP&		advSAP,
					Oscl::Mt::Itc::Dyn::Adv::Cli::
					SyncApi<dynSrvType>&			advFind,
					Oscl::Mt::Itc::PostMsgApi&		myPapi,
					Oscl::Mt::Itc::Dyn::Adv::
					Iterator<dynSrvType>&			iterator,
					Context&						context,
					ActivateOp						activate,
					DeactivateOp					deactivate
					) noexcept:
					Part<dynSrvType>(advSAP,advFind,myPapi,iterator),
					_context(context),
					_activate(activate),
					_deactivate(deactivate)
		{
	}

}
}
}
}
}

#endif
