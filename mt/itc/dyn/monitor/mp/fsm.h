/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_mt_itc_dyn_mp_fsmh_
#define _oscl_mt_itc_dyn_mp_fsmh_

/** */
namespace Oscl {
/** */
namespace Mt {
/** */
namespace Itc {
/** */
namespace Dyn {
/** */
namespace MpMonitor {

/** */
class StateVar {
	public:
		/** */
		class ContextApi {
			public:
				/** */
				virtual ~ContextApi(){}
				/** */
				virtual void	initNotificationReq() noexcept=0;
				/** */
				virtual void	requestChangeNotification() noexcept=0;
				/** */
				virtual void	processChangeNotification() noexcept=0;
				/** */
				virtual void	cancelChangeNotification() noexcept=0;
				/** */
				virtual void	completeDeactivation() noexcept=0;
			};
	private:
		/** */
		class State {
			public:
				/** */
				virtual ~State(){}
			public:
				/** */
				virtual void	activate(	ContextApi&	context,
											StateVar&	statevar
											) const noexcept;
				/** */
				virtual void	deactivate(	ContextApi&	context,
											StateVar&	statevar
											) const noexcept;
				/** */
				virtual void	notified(	ContextApi&	context,
											StateVar&	statevar
											) const noexcept;
				/** */
				virtual void	canceled(	ContextApi&	context,
											StateVar&	statevar
											) const noexcept;
			};
		/** */
		class ClosedState : public State {
			public:
				/** */
				void	activate(	ContextApi&	context,
									StateVar&	statevar
									) const noexcept;
			};
		/** */
		class OpenedState : public State {
			public:
				/** */
				void	deactivate(	ContextApi&	context,
									StateVar&	statevar
									) const noexcept;
				/** */
				void	notified(	ContextApi&	context,
									StateVar&	statevar
									) const noexcept;
			};
		/** */
		class ClosingState : public State {
			public:
				/** */
				void	notified(	ContextApi&	context,
									StateVar&	statevar
									) const noexcept;
				/** */
				void	canceled(	ContextApi&	context,
									StateVar&	statevar
									) const noexcept;
			};
	private:
		/** */
		static const ClosingState	_closingState;
		/** */
		static const ClosedState	_closedState;
		/** */
		static const OpenedState	_openedState;
	private:
		/** */
		ContextApi&		_context;
		/** */
		const State*	_state;
	public:
		/** */
		StateVar(ContextApi& context) noexcept;
		/** */
		void	activate() noexcept;
		/** */
		void	deactivate() noexcept;
		/** */
		void	notified() noexcept;
		/** */
		void	canceled() noexcept;
	private:
		friend class State;
		friend class ClosingState;
		friend class ClosedState;
		friend class OpenedState;
		/** */
		void	changeToClosedState() noexcept;
		/** */
		void	changeToOpenedState() noexcept;
		/** */
		void	changeToClosingState() noexcept;
		/** */
		void	protocolError() noexcept;
	};

};
};
};
};
};

#endif
