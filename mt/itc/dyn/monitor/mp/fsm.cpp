/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "fsm.h"
#include "oscl/error/fatal.h"

using namespace Oscl;
using namespace Oscl::Mt::Itc::Dyn;
using namespace Oscl::Mt::Itc::Dyn::MpMonitor;

////////////////////////////////

void	MpMonitor::StateVar::State::
		activate(	StateVar::ContextApi&	context,
					MpMonitor::StateVar&	statevar
					) const noexcept {
	statevar.protocolError();
	}

void	MpMonitor::StateVar::State::
		deactivate(	StateVar::ContextApi&	context,
					MpMonitor::StateVar&	statevar
					) const noexcept {
	statevar.protocolError();
	}

void	MpMonitor::StateVar::State::
		notified(	StateVar::ContextApi&	context,
					MpMonitor::StateVar&	statevar
					) const noexcept {
	statevar.protocolError();
	}

void	MpMonitor::StateVar::State::
		canceled(	StateVar::ContextApi&	context,
					MpMonitor::StateVar&	statevar
					) const noexcept {
	statevar.protocolError();
	}

////////////////////////////////

void	MpMonitor::StateVar::ClosedState::
		activate(	StateVar::ContextApi&	context,
					MpMonitor::StateVar&	statevar
					) const noexcept {
	context.initNotificationReq();
	context.requestChangeNotification();
	statevar.changeToOpenedState();
	}

////////////////////////////////

void	MpMonitor::StateVar::OpenedState::
		deactivate(	StateVar::ContextApi&	context,
					MpMonitor::StateVar&	statevar
					) const noexcept {
	context.cancelChangeNotification();
	statevar.changeToClosingState();
	}

void	MpMonitor::StateVar::OpenedState::
		notified(	StateVar::ContextApi&	context,
					MpMonitor::StateVar&	statevar
					) const noexcept {
	context.processChangeNotification();
	context.requestChangeNotification();
	}

////////////////////////////////

void	MpMonitor::StateVar::ClosingState::
		notified(	StateVar::ContextApi&	context,
					MpMonitor::StateVar&	statevar
					) const noexcept {
	}

void	MpMonitor::StateVar::ClosingState::
		canceled(	StateVar::ContextApi&	context,
					MpMonitor::StateVar&	statevar
					) const noexcept {
	statevar.changeToClosedState();
	context.completeDeactivation();
	}

////////////////////////////////

const MpMonitor::StateVar::ClosedState	MpMonitor::StateVar::_closedState;
const MpMonitor::StateVar::OpenedState	MpMonitor::StateVar::_openedState;
const MpMonitor::StateVar::ClosingState	MpMonitor::StateVar::_closingState;

MpMonitor::StateVar::StateVar(StateVar::ContextApi& context) noexcept:
		_context(context),
		_state(&_closedState)
		{
	}

void	MpMonitor::StateVar::activate() noexcept{
	_state->activate(_context,*this);
	}

void	MpMonitor::StateVar::deactivate() noexcept{
	_state->deactivate(_context,*this);
	}

void	MpMonitor::StateVar::notified() noexcept{
	_state->notified(_context,*this);
	}

void	MpMonitor::StateVar::canceled() noexcept{
	_state->canceled(_context,*this);
	}

void	MpMonitor::StateVar::changeToClosedState() noexcept{
	_state	= &_closedState;
	}

void	MpMonitor::StateVar::changeToOpenedState() noexcept{
	_state	= &_openedState;
	}

void	MpMonitor::StateVar::changeToClosingState() noexcept{
	_state	= &_closingState;
	}

void	MpMonitor::StateVar::protocolError() noexcept{
	ErrorFatal::logAndExit("MpMonitor::StateVar::protocolError\n");
	}

