/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "fsm.h"
#include "oscl/error/fatal.h"

using namespace Oscl;
using namespace Oscl::Mt::Itc::Dyn::Monitor;

const StateVar::DeactivatedState	StateVar::_deactivatedState;
const StateVar::ActivatedState		StateVar::_activatedState;
const StateVar::CancelingState		StateVar::_cancelingState;
const StateVar::DeactivatingState	StateVar::_deactivatingState;

StateVar::StateVar(ContextApi& context) noexcept:
		_state(&_deactivatedState),
		_context(context)
		{
	}

void	StateVar::activate() noexcept{
	_state->activate(_context,*this);
	}

void	StateVar::releaseNotification() noexcept{
	_state->releaseNotification(_context,*this);
	}

void	StateVar::deactivate() noexcept{
	_state->deactivate(_context,*this);
	}

void	StateVar::deactivated() noexcept{
	_state->deactivated(_context,*this);
	}

void	StateVar::releaseNotificationCanceled() noexcept{
	_state->releaseNotificationCanceled(_context,*this);
	}

void	StateVar::protocolError() noexcept{
	ErrorFatal::logAndExit("StateVar::protocolError\n");
	}

void	StateVar::changeToActivatedState() noexcept{
	_state	= &_activatedState;
	}

void	StateVar::changeToDeactivatingState() noexcept{
	_state	= &_deactivatingState;
	}

void	StateVar::changeToCancelingState() noexcept{
	_state	= &_cancelingState;
	}

void	StateVar::changeToDeactivatedState() noexcept{
	_state	= &_deactivatedState;
	}

// State

void	StateVar::State::activate(	ContextApi&	context,
									StateVar&	statevar
									) const noexcept{
	statevar.protocolError();
	}

void	StateVar::State::deactivate(	ContextApi&	context,
										StateVar&	statevar
										) const noexcept{
	statevar.protocolError();
	}

void	StateVar::State::releaseNotification(	ContextApi&	context,
												StateVar&	statevar
												) const noexcept{
	statevar.protocolError();
	}

void	StateVar::State::releaseNotificationCanceled(
									ContextApi&	context,
									StateVar&	statevar
									) const noexcept{
	statevar.protocolError();
	}

void	StateVar::State::deactivated(	ContextApi&	context,
										StateVar&	statevar
										) const noexcept{
	statevar.protocolError();
	}

// DeactivatedState
void	StateVar::DeactivatedState::activate(	ContextApi&	context,
												StateVar&	statevar
											) const noexcept{
	context.requestReleaseNotification();
	context.activateMonitors();
	statevar.changeToActivatedState();
	}

// ActivatedState
void	StateVar::ActivatedState::deactivate(	ContextApi&	context,
												StateVar&	statevar
												) const noexcept{
	context.cancelReleaseNotification();
	statevar.changeToCancelingState();
	}

void	StateVar::ActivatedState::releaseNotification(	ContextApi&	context,
														StateVar&	statevar
														) const noexcept{
	// Change state before deactivation.
	// This allows for monitor sub-classes that have
	// no monitor point associations to cancel to
	// just call back to indicate that deactivation
	// is complete immediately/synchronously.
	statevar.changeToDeactivatingState();
	context.release();
	}

// CancelingState
void	StateVar::CancelingState::
		releaseNotificationCanceled(	ContextApi&	context,
										StateVar&	statevar
										) const noexcept{
	// Change state before deactivation.
	// This allows for monitor sub-classes that have
	// no monitor point associations to cancel to
	// just call back to indicate that deactivation
	// is complete immediately/synchronously.
	statevar.changeToDeactivatingState();
	context.deactivateMonitors();
	}

void	StateVar::CancelingState::releaseNotification(	ContextApi&	context,
														StateVar&	statevar
														) const noexcept{
	}

// DeactivatingState
void	StateVar::DeactivatingState::deactivated(	ContextApi&	context,
													StateVar&	statevar
													) const noexcept{
	statevar.changeToDeactivatedState();
	context.monitorDeactivated();
	}

void	StateVar::DeactivatingState::deactivate(	ContextApi&	context,
													StateVar&	statevar
													) const noexcept{
	context.deactivateMonitors();
	}

