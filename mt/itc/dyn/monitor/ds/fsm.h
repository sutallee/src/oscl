/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_mt_itc_dyn_monitor_ds_fsmh_
#define _oscl_mt_itc_dyn_monitor_ds_fsmh_

/** */
namespace Oscl {
/** */
namespace Mt {
/** */
namespace Itc {
/** */
namespace Dyn {
/** */
namespace Monitor {

/** This class implements the behavior of a Dynamic Object Monitor
	using the GOF State Pattern. The GOF flyweight pattern is also
	applied to the states, which allows a single set of stateless
	state objects to be shared by all instances of this FSM.

	The behavior proceeds normally as follows. The Monitor
	is created when its associated Dynamic Object is found by the
	context. The context invokes the Monitor's activate() operation,
	causing it to establish its associations with the Dynamic Object.
	Later, either the deactivate() or releaseNotification() event
	is received, which causes the deactivation sequence to start.
	During the deactivation sequence, the context is instructed
	to release all associations with the Dynamic Object. Finally,
	the context invokes the deactivated() event after it has
	severed all ties with the Dynamic Object. At this time,
	the Monitor is in the Deactivated state and it can subsequently
	be destructed and its resources may be reclaimed by the context.
 */
class StateVar {
	public:
		/** This interface must be implemented by the Monitor
			context.
		 */
		class ContextApi {
			public:
				/** Make the stupid compiler happy. */
				virtual ~ContextApi(){}
			public:
				/** The FSM invokes this operation to cause the context
					to send a release notification request to the Dynamic
					Object.
				 */
				virtual void	requestReleaseNotification() noexcept=0;

				/** The FSM invokes this operation to inform the context
					that it may establish relations with the Dynamic Object.
				 */
				virtual void	activateMonitors() noexcept=0;
				/** The FSM invokes this operation to tell the context to
					begin severing its associations with the Dynamic Object.
					This process proceeds asynchronously, and the context
					invokes the deactivated() operation of the StateVar
					when the process completes.
				 */
				virtual void	deactivateMonitors() noexcept=0;
				/** The FSM invokes this operation to 
				 */
				virtual void	release() noexcept=0;
				/** */
				virtual void	cancelReleaseNotification() noexcept=0;
				/** */
				virtual void	monitorDeactivated() noexcept=0;
			};
	private:
		/** */
		class State {
			public:
				virtual ~State(){}
			public:
				/** */
				virtual void	activate(	ContextApi&	context,
											StateVar&	statevar
											) const noexcept;
				/** */
				virtual void	releaseNotification(	ContextApi&	context,
														StateVar&	statevar
														) const noexcept;

				/** */
				virtual void	deactivate(	ContextApi&	context,
											StateVar&	statevar
											) const noexcept;

				/** */
				virtual void	deactivated(	ContextApi&	context,
												StateVar&	statevar
												) const noexcept;
				/** */
				virtual void	releaseNotificationCanceled(
												ContextApi&	context,
												StateVar&	statevar
												) const noexcept;
			};
		/** */
		class DeactivatedState : public State {
			private:
				/** */
				void	activate(	ContextApi&	context,
									StateVar&	statevar
									) const noexcept;
			};
		/** */
		class ActivatedState : public State {
			private:
				/** */
				void	releaseNotification(	ContextApi&	context,
												StateVar&	statevar
												) const noexcept;

				/** */
				void	deactivate(	ContextApi&	context,
									StateVar&	statevar
									) const noexcept;
			};
		/** */
		class DeactivatingState : public State {
			private:
				/** */
				void	deactivated(	ContextApi&	context,
										StateVar&	statevar
										) const noexcept;
				/** */
				void	deactivate(	ContextApi&	context,
									StateVar&	statevar
									) const noexcept;
			};
		/** */
		class CancelingState : public State {
			private:
				/** */
				void	releaseNotification(	ContextApi&	context,
												StateVar&	statevar
												) const noexcept;
				/** */
				void	releaseNotificationCanceled(	ContextApi&	context,
														StateVar&	statevar
														) const noexcept;
			};
	private:
		/** */
		static const DeactivatedState	_deactivatedState;
		/** */
		static const ActivatedState		_activatedState;
		/** */
		static const CancelingState		_cancelingState;
		/** */
		static const DeactivatingState	_deactivatingState;
	private:
		/** */
		const State*					_state;
		/** */
		ContextApi&						_context;
	public:
		/** */
		StateVar(ContextApi& context) noexcept;
		/** */
		void	activate() noexcept;
		/** */
		void	releaseNotification() noexcept;
		/** */
		void	deactivate() noexcept;
		/** */
		void	deactivated() noexcept;
		/** */
		void	releaseNotificationCanceled() noexcept;
	private:
		friend class State;
		friend class ActivatedState;
		friend class CancelingState;
		friend class DeactivatingState;
		friend class DeactivatedState;
	private:
		/** */
		void	protocolError() noexcept;
	private:
		/** */
		void	changeToActivatedState() noexcept;
		/** */
		void	changeToCancelingState() noexcept;
		/** */
		void	changeToDeactivatingState() noexcept;
		/** */
		void	changeToDeactivatedState() noexcept;
	};

};
};
};
};
};

#endif
