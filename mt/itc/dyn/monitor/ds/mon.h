/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_mt_itc_dyn_monitor_ds_monh_
#define _oscl_mt_itc_dyn_monitor_ds_monh_
#include <new>
#include "fsm.h"
#include "oscl/memory/block.h"
#include "oscl/mt/itc/dyn/cli/respapi.h"
#include "oscl/mt/itc/dyn/core/handle.h"

/** */
namespace Oscl {
/** */
namespace Mt {
/** */
namespace Itc {
/** */
namespace Dyn {
/** */
namespace Cli {

/** This class implements the behavior of a Dynamic Object
	monitor. This class is instantiated by its context
	when a new Dynamic Object is discovered by the context.

	There are three actors involved:
		o	The Context - creates instances of this class.
		o	The Dynamic Object - the object of desire.
		o	The sub-class  - the object that actually uses
			the Dynamic Object.
		o	This Monitor - sequences the response to the
			life-cycle of the Dynamic Object and the
			Context.

	This class should be sub-classed as a particular type
	of monitor, and the sub-class must implement the
	pure virtual operations:
		o subClassActivate
		o subClassDeactivate
		o getSubClass

	When the sub-class subClassActivate() operation is
	invoked, it should make any associations it requires
	to begin operation with the Dynamic Object. This
	operation will only be invoked one time some time
	after the context invokes the activate() operation.

	When the sub-class subClassDeactivate() operation
	is invoked, the sub-class must begin the process
	of disassociating with the Dynamic Object. For
	example this may involve canceling notification
	requests one-at-a-time. After all associations
	with the Dynamic Object have been disolved, the
	sub-class must invoke the subClassDeactivated()
	operation.

	When the sub-class getSubClass operation is invoked,
	it should simply return a reference to the
	concrete sub-class itself.

	The typical use case of the Context is:

	o Context monitors the dynamic service's Advertiser.
	o	The context recognizes the creation of a desired
		Dynamic Object of this type.
	o	The context constructs an instance of this Monitor.
	o	The context invokes the activate() operation of this Monitor.
	o	The context uses the monitor to govern its behavior based
		on the life-cycle of the monitored Dynamic Object.

		o	The context may decide to end it's relationship with the
			Dynamic Object by invoking the deactivate() operation
			of the Monitor, and then waiting for the deactivated()
			operation to be invoked indicating that the context
			may destruct the Monitor.

		o	The context may receive an indication that all
			associations with the Dynamic Object have been
			severed, and that the Monitor may be destructed,
			when its deactived() operation is invoked.

		o	Notice that in either of the above two cases
			that the relationship is terminated when the
			deactivated() operation of the context is invoked.

	The template parameters are:
		o subClassType - The name of concrete sub-class.
		o deactMemType - A structure/union that defines the
						 memory required by the context and
						 sub-class for use during deactivation.
		o dynSrvType - The type of the Dynamic Object being monitored.
 */
template <class subClassType, class deactMemType, class dynSrvType>
class Monitor : public Mt::Itc::Dyn::Cli::Resp::Api<dynSrvType>,
				public Mt::Itc::Dyn::Monitor::StateVar::ContextApi
				{
	public:
		/** This interface is to be implemented by the context.
		 */
		class ContextApi {
			public:
				/** Make the compiler happy. */
				virtual ~ContextApi(){}
				/** This operation is invoked to cause the context
					to begin releasing resources related to the
					Dyanmic Object. In response, the context
					must invoke the deactivate() operation of the
					Monitor, providing memory resources for the
					deactivation operation. This allows as single
					block of memory to be used in the asynchronous
					canceling of outstanding ITC operations with
					the Dyamic Object. When the Monitor has comletely
					severed ties with the Dynamic Object, it invokes
					the deactivated() operation of this interface. 
				 */
				virtual void	release(subClassType& thisSc) noexcept=0;
				/** When this operation is invoked, the Monitor
					(and it's sub-class) have been severed all
					associations with the Dynamic Object. The
					Context may reclaim the resources occupied
					by this object after destructing it.
				 */
				virtual void	deactivated(subClassType& thisSc) noexcept=0;
			};
	public:
		/** This structure defines a block of memory that is large
			enough to hold a release notification cancelation message.
		 */
		typedef struct ReleaseMem {
			/** Memory for the message payload.
			 */
			Memory::AlignedBlock<
				sizeof(	typename Mt::Itc::Dyn::Cli::Req::Api<dynSrvType>::
						CancelReleasePayload
						)
				> _payload;

			/** Memory for the message itself.
			 */
			Memory::AlignedBlock<
				sizeof(	typename Mt::Itc::Dyn::Cli::Resp::Api<dynSrvType>::
						CancelReleaseResp
						)
				> _response;
			} ReleaseMem;

	public:
		/** This union defines a block of memory large enough to
			perform the deactivation of the Monitor its sub-class
			and perhaps the context.
		 */
		typedef union DeactivateMem {
			/** Memory required by the Monitor itself.
			 */
			ReleaseMem		_release;
			/** Memory required by the sub-class and the context.
			 */
			deactMemType	_mpDeactivateMem;
			} DeactivateMem;

	private:
		/** This member implements the FSM behavior for the Monitor.
		 */
		Mt::Itc::Dyn::Monitor::StateVar			_state;

		/** This is a reference to the context.
		 */
		ContextApi&								_context;

		/** This is a handle that references the Dynamic Object
			to which this Monitor is bound.
		 */
		Mt::Itc::Dyn::Handle<dynSrvType>		_dynSrv;

		/** This is a reference to the ITC mailbox and thus
			the thread in which this Monitor and its context
			operate.
		 */
		Mt::Itc::PostMsgApi&					_myPapi;

		/** The payload of the ITC notification that is used
			to monitor the life-cycle of the Dyanmic Object.
		 */
		typename Mt::Itc::Dyn::Cli::Req::Api<dynSrvType>::
		ReleasePayload							_releasePayload;

		/** The ITC notification message that is used
			to monitor the life-cycle of the Dyanmic Object.
		 */
		typename Mt::Itc::Dyn::Cli::Resp::Api<dynSrvType>::
		ReleaseResp								_releaseResp;

		/** A pointer to the memory resources supplied by the
			context and used for the deactivation of the
			Monitor.
		 */
		DeactivateMem*							_deactivateMem;
	protected:
		/** The constructor requires a reference to the context,
			which it uses to interact with its creator.
			It also requires a reference to the mailbox/thread
			(myPapi) in which it will operate with the context.
			Finally, it requires a reference to the Dynamic
			Object which it will monitor.
		 */
		Monitor(	ContextApi&				context,
					Mt::Itc::PostMsgApi&	myPapi,
					dynSrvType&				dynSrv
					) noexcept;

		/** Make the compiler happy... stupid compiler. */
		virtual ~Monitor(){}

	public:
		/** This operation is to be invoked by the context
			following construction. This causes the Monitor
			and its sub-class to begin using the Dynamic
			Object.
		 */
		void	activate() noexcept;

		/** This operation is invoked by the context
			when it is ready to sever its association
			with the Dynamic Object and this Monitor.
			This may be the result of the context's
			release() operation being invoked as the
			result of the Dynamic Object's life cycle
			being ended, or the context may invoke it
			because it is simply ending the association.
			Either way, the context must wait until its
			deactivated() operation is invoked before
			reclaiming the resources associated with
			this Monitor.
		 */
		void	deactivate(DeactivateMem& mem) noexcept;

	private: // StateVar::ContextApi
		/** When this operation is invoked by the FSM
			the implementation asynchronously sends a
			release notification ITC request to the
			Dynamic Object.
		 */
		void	requestReleaseNotification() noexcept;

		/** When this operation is invoked by the FSM
			the implementation starts using the
			Dynamic Object. In this case the subClassActivate()
			operation is invoked.
		 */
		void	activateMonitors() noexcept;

		/** When this operation is invoked by the FSM
			the implementation begins the process of severing
			its releationship with the Dynamic Object.
		 */
		void	deactivateMonitors() noexcept;

		/** When this operation is invoked by the FSM
			the implementation invokes the release() operation
			of the context to begin to disassociate itself
			and this Monitor from the Dynamic Object.
			This includes the context invoking the deactive()
			operation of the Monitor.
		 */
		void	release() noexcept;

		/** When this operation is invoked by the FSM
			the implementation asynchronously sends
			a release notification cancellation request
			to the Dynamic Object.
		 */
		void	cancelReleaseNotification() noexcept;

		/** When this operation is invoked by the FSM
			the implementation invokes the contexts
			deactivated() operation indicating that
			the association with the Dynamic Object
			has been severed.
		 */
		void	monitorDeactivated() noexcept;

	private:
		/** This operation when the release notification
			ITC message is returned to the Monitor by
			the Dynamic Object either because the Dynamic
			Object is ending its life cycle, or because
			this Monitor cancelled the release notification
			request.
		 */
		void	response(	typename Mt::Itc::Dyn::Cli::Resp::Api<dynSrvType>::
							ReleaseResp&	msg
							) noexcept;

		/** This operation when the release notification
			cancelation ITC message is returned to the Monitor
			by the Dynamic Object.
		 */
		void	response(	typename Mt::Itc::Dyn::Cli::Resp::Api<dynSrvType>::
							CancelReleaseResp&	msg
							) noexcept;
	private:
		/** This operation must be implemented by the sub-class.
			When invoked the sub-class may begin using the
			Dynamic Object.
		 */
		virtual void		subClassActivate() noexcept=0;

		/** This operation must be implmented by the sub-class.
			When invoked the sub-class should begin the process
			of disassociating itself from the Dynamic Object.
			It may use the memory referenced by the argument
			to send cancellation requests asynchronously
			one-at-a-time to the Dynamic Object as required.
			When the sub-class completes this cancellation
			it must invoke the subClassDeactivated() operation
			of the Monitor.
		 */
		virtual void		subClassDeactivate(deactMemType& mem) noexcept=0;

		/** This operation must be implmented by the sub-class.
			It must return a reference to the concrete sub-class.
			This reference is used to pass to the context such
			that it can identify this Monitor instance.
		 */
		virtual subClassType&	getSubClass() noexcept=0;

	protected:
		/** This operation is to be invoked by the sub-class
			when it has completely disassociated itself from
			the Dynamic Object started as the resulto of the
			subClassDeactivate() operation being invoked.
		 */
		void				subClassDeactivated() noexcept;
	};

template <class subClassType, class deactMemType, class dynSrvType>
Monitor<subClassType,deactMemType,dynSrvType>::
	Monitor(	ContextApi&				context,
				Mt::Itc::PostMsgApi&	myPapi,
				dynSrvType&				dynSrv
				) noexcept:
		_state(*this),
		_context(context),
		_dynSrv(dynSrv),
		_myPapi(myPapi),
		_releaseResp(	dynSrv.getCliSAP().getReqApi(),
						*this,
						myPapi,
						_releasePayload
						),
		_deactivateMem(0)
		{
	}

template <class subClassType, class deactMemType, class dynSrvType>
void	Monitor<subClassType,deactMemType,dynSrvType>::activate() noexcept {
	_state.activate();
	}

template <class subClassType, class deactMemType, class dynSrvType>
void	Monitor<subClassType,deactMemType,dynSrvType>::
		deactivate(DeactivateMem& mem) noexcept{
	_deactivateMem	= &mem;
	_state.deactivate();
	}

template <class subClassType, class deactMemType, class dynSrvType>
void	Monitor<subClassType,deactMemType,dynSrvType>::
		requestReleaseNotification() noexcept{
	_dynSrv->getCliSAP().post(_releaseResp.getSrvMsg());
	}

template <class subClassType, class deactMemType, class dynSrvType>
void	Monitor<subClassType,deactMemType,dynSrvType>::
		activateMonitors() noexcept{
	subClassActivate();
	}

template <class subClassType, class deactMemType, class dynSrvType>
void	Monitor<subClassType,deactMemType,dynSrvType>::
		deactivateMonitors() noexcept{
	subClassDeactivate(_deactivateMem->_mpDeactivateMem);
	}

template <class subClassType, class deactMemType, class dynSrvType>
void	Monitor<subClassType,deactMemType,dynSrvType>::release() noexcept{
	_context.release(getSubClass());
	}
template <class subClassType, class deactMemType, class dynSrvType>
void	Monitor<subClassType,deactMemType,dynSrvType>::
		cancelReleaseNotification() noexcept{
	using namespace Mt::Itc::Dyn::Cli;
	typename Req::Api<dynSrvType>::CancelReleasePayload*	payload	=
		new (&_deactivateMem->_release._payload)
				typename Req::Api<dynSrvType>::CancelReleasePayload(
				_releaseResp.getSrvMsg()
				);
	typename Resp::Api<dynSrvType>::CancelReleaseResp*	response =
		new (&_deactivateMem->_release._response)
				typename Resp::Api<dynSrvType>::CancelReleaseResp(
					_dynSrv->getCliSAP().getReqApi(),
					*this,
					_myPapi,
					*payload
					);
	_dynSrv->getCliSAP().post(response->getSrvMsg());
	}

template <class subClassType, class deactMemType, class dynSrvType>
void	Monitor<subClassType,deactMemType,dynSrvType>::
		monitorDeactivated() noexcept{
	_context.deactivated(getSubClass());
	}

template <class subClassType, class deactMemType, class dynSrvType>
void	Monitor<subClassType,deactMemType,dynSrvType>::
		response(	typename Mt::Itc::Dyn::Cli::Resp::Api<dynSrvType>::
					ReleaseResp&	msg
					) noexcept{
	_state.releaseNotification();
	}

template <class subClassType, class deactMemType, class dynSrvType>
void	Monitor<subClassType,deactMemType,dynSrvType>::
		response(	typename Mt::Itc::Dyn::Cli::Resp::Api<dynSrvType>::
					CancelReleaseResp&	msg
					) noexcept{
	typename Mt::Itc::Dyn::Cli::Req::Api<dynSrvType>::
	CancelReleasePayload& payload = msg.getPayload();
	msg.Mt::Itc::Dyn::Cli::Resp::Api<dynSrvType>::CancelReleaseResp::~CancelReleaseResp();
	payload.Mt::Itc::Dyn::Cli::Req::Api<dynSrvType>::CancelReleasePayload::~CancelReleasePayload();
	_state.releaseNotificationCanceled();
	}

template <class subClassType, class deactMemType, class dynSrvType>
void	Monitor<subClassType,deactMemType,dynSrvType>::
		subClassDeactivated() noexcept{
	_state.deactivated();
	}

}
}
}
}
}

#endif

