//oscl/mt/itc/dyn/monitor/adv/fsm.cpp#1 - branch change 843 (text)
/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "fsm.h"
#include "oscl/error/fatal.h"

using namespace Oscl;
using namespace Oscl::Mt::Itc::Dyn::Adv::Monitor;

const StateVar::ClosedState			StateVar::_closedState;
const StateVar::OpenedState			StateVar::_openedState;
const StateVar::DeactivatingState	StateVar::_deactivatingState;
const StateVar::ClosingState		StateVar::_closingState;
const StateVar::CancelingState		StateVar::_cancelingState;

StateVar::StateVar(ContextApi& context) noexcept:
		_state(&_closedState),
		_context(context)
		{
	}

void	StateVar::open() noexcept{
	_state->open(_context,*this);
	}

void	StateVar::close() noexcept{
	_state->close(_context,*this);
	}

void	StateVar::create() noexcept{
	_state->create(_context,*this);
	}

void	StateVar::deactivate() noexcept{
	_state->deactivate(_context,*this);
	}

void	StateVar::deactivated() noexcept{
	_state->deactivated(_context,*this);
	}

void	StateVar::canceled() noexcept{
	_state->canceled(_context,*this);
	}

void	StateVar::protocolError() noexcept{
	ErrorFatal::logAndExit("StateVar::protocolError\n");
	}

void	StateVar::changeToClosedState() noexcept{
	_state	= &_closedState;
	}

void	StateVar::changeToOpenedState() noexcept{
	_state	= &_openedState;
	}

void	StateVar::changeToDeactivatingState() noexcept{
	_state	= &_deactivatingState;
	}

void	StateVar::changeToClosingState() noexcept{
	_state	= &_closingState;
	}

void	StateVar::changeToCancelingState() noexcept{
	_state	= &_cancelingState;
	}

// State

void	StateVar::State::open(	ContextApi&	context,
								StateVar&	statevar
								) const noexcept{
	statevar.protocolError();
	}

void	StateVar::State::close(	ContextApi&	context,
								StateVar&	statevar
								) const noexcept{
	statevar.protocolError();
	}

void	StateVar::State::create(	ContextApi&	context,
									StateVar&	statevar
									) const noexcept{
	statevar.protocolError();
	}

void	StateVar::State::deactivate(	ContextApi&	context,
										StateVar&	statevar
										) const noexcept{
	statevar.protocolError();
	}

void	StateVar::State::deactivated(	ContextApi&	context,
										StateVar&	statevar
										) const noexcept{
	statevar.protocolError();
	}

void	StateVar::State::canceled(	ContextApi&	context,
									StateVar&	statevar
									) const noexcept{
	statevar.protocolError();
	}

// ClosedState
void	StateVar::ClosedState::open(	ContextApi&	context,
										StateVar&	statevar
										) const noexcept{
	context.sendCreateNotificationReq();
	context.openRequestDone();
	statevar.changeToOpenedState();
	}

// OpenedState
void	StateVar::OpenedState::close(	ContextApi&	context,
										StateVar&	statevar
										) const noexcept{
	if(context.activeListIsEmpty()){
		context.closeRequestDone();
		statevar.changeToClosedState();
		}
	else{
		context.moveActiveToDeactivateList();
		statevar.changeToClosingState();
		context.deactivateNextMonitor();
		}
	}

void	StateVar::OpenedState::create(	ContextApi&	context,
										StateVar&	statevar
										) const noexcept{
	if(context.newDynSrvListNotEmpty()){
		context.activateNewMonitors();
		context.sendCreateNotificationReq();
		}
	}

void	StateVar::OpenedState::deactivate(	ContextApi&	context,
											StateVar&	statevar
											) const noexcept{
	context.queueDeactivateReq();
	statevar.changeToDeactivatingState();
	context.deactivateNextMonitor();
	}


// DeactivatingState
void	StateVar::DeactivatingState::close(	ContextApi&	context,
											StateVar&	statevar
											) const noexcept{
	context.moveActiveToDeactivateList();
	statevar.changeToClosingState();
	}

void	StateVar::DeactivatingState::create(	ContextApi&	context,
												StateVar&	statevar
												) const noexcept{
	if(context.newDynSrvListNotEmpty()){
		context.activateNewMonitors();
		context.sendCreateNotificationReq();
		}
	}

void	StateVar::DeactivatingState::deactivated(	ContextApi&	context,
													StateVar&	statevar
													) const noexcept{
	if(context.deactivateListIsEmpty()){
		statevar.changeToOpenedState();
		}
	else {
		context.deactivateNextMonitor();
		}
	}

void	StateVar::DeactivatingState::deactivate(	ContextApi&	context,
													StateVar&	statevar
													) const noexcept{
	context.queueDeactivateReq();
	}

// ClosingState
void	StateVar::ClosingState::create(	ContextApi&	context,
										StateVar&	statevar
										) const noexcept{
	// Just let the create notification go.
	}

void	StateVar::ClosingState::deactivated(	ContextApi&	context,
												StateVar&	statevar
												) const noexcept{
	if(context.deactivateListIsEmpty()){
		context.cancelCreateNotificationReq();
		statevar.changeToCancelingState();
		}
	else {
		context.deactivateNextMonitor();
		}
	}

void	StateVar::ClosingState::deactivate(	ContextApi&	context,
											StateVar&	statevar
											) const noexcept{
	context.queueDeactivateReq();
	}

// CancelingState
void	StateVar::CancelingState::create(	ContextApi&	context,
											StateVar&	statevar
											) const noexcept{
	}

void	StateVar::CancelingState::canceled(	ContextApi&	context,
											StateVar&	statevar
											) const noexcept{
	context.closeRequestDone();
	statevar.changeToClosedState();
	}


