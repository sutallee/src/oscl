/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_mt_itc_dyn_adv_monitor_fsmh_
#define _oscl_mt_itc_dyn_adv_monitor_fsmh_

/** */
namespace Oscl {
/** */
namespace Mt {
/** */
namespace Itc {
/** */
namespace Dyn {
/** */
namespace Adv {
/** */
namespace Monitor {

/** */
class StateVar {
	public:
		/** */
		class ContextApi {
			public:
				/** */
				virtual ~ContextApi(){}
			public:
				/** */
				virtual void	sendCreateNotificationReq() noexcept=0;
				/** */
				virtual void	openRequestDone() noexcept=0;
				/** */
				virtual void	closeRequestDone() noexcept=0;
				/** */
				virtual void	activateNewMonitors() noexcept=0;
				/** */
				virtual void	queueDeactivateReq() noexcept=0;
				/** */
				virtual void	deactivateNextMonitor() noexcept=0;
				/** */
				virtual void	moveActiveToDeactivateList() noexcept=0;
				/** */
				virtual void	cancelCreateNotificationReq() noexcept=0;
			public:
				/** */
				virtual bool	newDynSrvListNotEmpty() noexcept=0;
				/** */
				virtual bool	deactivateListIsEmpty() noexcept=0;
				/** */
				virtual bool	activeListIsEmpty() noexcept=0;
			};
	private:
		/** */
		class State {
			public:
				virtual ~State(){}
			public:
				/** */
				virtual void	open(	ContextApi&	context,
										StateVar&	internal
										) const noexcept;
				/** */
				virtual void	close(	ContextApi&	context,
										StateVar&	internal
										) const noexcept;

				/** */
				virtual void	create(	ContextApi&	context,
										StateVar&	internal
										) const noexcept;

				/** */
				virtual void	deactivate(	ContextApi&	context,
											StateVar&	internal
											) const noexcept;

				/** */
				virtual void	deactivated(	ContextApi&	context,
												StateVar&	internal
												) const noexcept;

				/** */
				virtual void	canceled(	ContextApi&	context,
											StateVar&	internal
											) const noexcept;
			};
		/** */
		class ClosedState : public State {
			private:
				/** */
				void	open(	ContextApi&	context,
								StateVar&	internal
								) const noexcept;
			};
		/** */
		class OpenedState : public State {
			private:
				/** */
				void	close(	ContextApi&	context,
								StateVar&	internal
								) const noexcept;
				/** */
				void	create(	ContextApi&	context,
								StateVar&	internal
								) const noexcept;
				/** */
				void	deactivate(	ContextApi&	context,
									StateVar&	internal
									) const noexcept;
			};
		/** */
		class DeactivatingState : public State {
			private:
				/** */
				void	close(	ContextApi&	context,
								StateVar&	internal
								) const noexcept;
				/** */
				void	create(	ContextApi&	context,
								StateVar&	internal
								) const noexcept;
				/** */
				void	deactivated(	ContextApi&	context,
										StateVar&	internal
										) const noexcept;
				/** */
				void	deactivate(	ContextApi&	context,
									StateVar&	internal
									) const noexcept;
			};
		/** */
		class ClosingState : public State {
			private:
				/** */
				void	create(	ContextApi&	context,
								StateVar&	internal
								) const noexcept;
				/** */
				void	deactivated(	ContextApi&	context,
										StateVar&	internal
										) const noexcept;
				/** */
				void	deactivate(	ContextApi&	context,
									StateVar&	internal
									) const noexcept;
			};
		/** */
		class CancelingState : public State {
			private:
				/** */
				void	create(	ContextApi&	context,
								StateVar&	internal
								) const noexcept;
				/** */
				void	canceled(	ContextApi&	context,
									StateVar&	internal
									) const noexcept;
			};
	private:
		/** */
		static const ClosedState		_closedState;
		/** */
		static const OpenedState		_openedState;
		/** */
		static const DeactivatingState	_deactivatingState;
		/** */
		static const ClosingState		_closingState;
		/** */
		static const CancelingState		_cancelingState;
	private:
		/** */
		const State*					_state;
		/** */
		ContextApi&						_context;
	public:
		/** */
		StateVar(ContextApi& context) noexcept;
		/** */
		void	open() noexcept;
		/** */
		void	close() noexcept;
		/** */
		void	create() noexcept;
		/** */
		void	deactivate() noexcept;
		/** */
		void	deactivated() noexcept;
		/** */
		void	canceled() noexcept;
	private:
		friend class State;
		friend class ClosedState;
		friend class OpenedState;
		friend class DeactivatingState;
		friend class ClosingState;
		friend class CancelingState;
	private:
		/** */
		void	protocolError() noexcept;
	private:
		/** */
		void	changeToOpenedState() noexcept;
		/** */
		void	changeToClosedState() noexcept;
		/** */
		void	changeToDeactivatingState() noexcept;
		/** */
		void	changeToClosingState() noexcept;
		/** */
		void	changeToCancelingState() noexcept;
	};

};
};
};
};
};
};

#endif
