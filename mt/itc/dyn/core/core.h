/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_mt_itc_dyn_core_coreh_
#define _oscl_mt_itc_dyn_core_coreh_
#include "creator.h"
#include "oscl/mt/itc/dyn/fsm/fsm.h"
#include "oscl/error/fatal.h"
#include "cli.h"
#include "ref.h"

/** */
namespace Oscl {
/** */
namespace Mt {
/** */
namespace Itc {
/** */
namespace Dyn {

/** This abstract template implements the core behavior of
	a Dynamic Service. Concrete Dynamic Service sub classes
	must implement openDynamicService(), closeDynamicService(),
	and getDynSrv().
 */
template <class dynSrvType>
class Core :
		public Creator::Req::Api<dynSrvType>,
		public StateVar::ContextApi
		{
	public:
		/** */
		using Creator::Req::Api<dynSrvType>::request;

	private:
		/** This member implements the FSM behavior for the Monitor.
		 */
		Dyn::StateVar						_state;

		/** This is a list of all of the client release notification
			requests. When this Dynamic Object comes to the end
			of its dynamic life-cycle. These release notifications
			are returned to their senders, and their senders
			then release all references to this Dynamic Object.
		 */
		Queue<typename Dyn::Cli::Req::Api<dynSrvType>::ReleaseReq>	_notificationList;

		/** This is a count of the number of references to this
			Dynamic Object. When this count reaches zero, this
			Dynamic Object is no longer being used by any clients
			and is ready to be closed by its context.
		 */
		unsigned long						_refCount;

		/** This is a reference to the open request which will
			be returned to its sender after the open sequence
			completes.
		 */
		Mt::Itc::Srv::Open::Req::Api::
		OpenReq*							_currentOpenReq;

		/** This is a reference to the current reference count
			increment request which will be returned to its sender
			after the sequence completes.
		 */
		typename Dyn::Ref::Req::Api<dynSrvType>::
		IncrReq*							_currentIncrReq;

		/** This is a reference to the current reference count
			decrement request which will be returned to its sender
			after the sequence completes.
		 */
		typename Dyn::Ref::Req::Api<dynSrvType>::
		DecrReq*							_currentDecrReq;

		/** This is a reference to the current release notification
			cancellation request which will be returned to its sender
			after the cancellation sequence completes.
		 */
		typename Dyn::Cli::Req::Api<dynSrvType>::
		CancelReleaseReq*					_currentCancelReq;

		/** This is a reference to the current deactivate
			request which will be returned to its sender
			after the deactivation sequence completes.
		 */
		typename Dyn::Creator::Req::Api<dynSrvType>::
		DeactivateReq*						_currentDeactivateReq;

		/** This is a reference to the current close
			request which will be returned to its sender
			after the close sequence completes.
		 */
		Mt::Itc::Srv::Close::Req::Api::
		CloseReq*							_currentCloseReq;

	public:
		/** The constructor requires a reference to the mailbox/thread
			in which this Dynamic Object executes.
		 */
		Core(Mt::Itc::PostMsgApi& dynSrv) noexcept;

	public:	// Dyn::StateVar::ContextApi
		/** Invoked by the Dynamic Service FSM.
			Invokes the openDynamicService() operation.
		 */
		void	activateDynamicService() noexcept;
		/** Invoked by the Dynamic Service FSM.
			Invokes the closeDynamicService() operation.
		 */
		void	deactivateDynamicService() noexcept;
		/** Invoked by the Dynamic Service FSM.
			Invokes the returnToSender() of the current
			open request.
		 */
		void	finishedWithOpenReq() noexcept;
		/** Invoked by the Dynamic Service FSM.
			Invokes the returnToSender() of the current
			prepare for close request.
		 */
		void	finishedWithClosePrepReq() noexcept;
		/** Invoked by the Dynamic Service FSM.
			Invokes the returnToSender() of the current
			close request.
		 */
		void	finishedWithCloseReq() noexcept;
		/** Invoked by the Dynamic Service FSM.
			Searches the notification list for the notification
			request specified by the current notification
			cancelation request; removes it from the list; and
			then invokes its returnToSender() operation.
		 */
		void	cancelNotificationReq() noexcept;
		/** Invoked by the Dynamic Service FSM.
			Invokes the returnToSender() operation of the
			current reference count increment request.
		 */
		void	returnIncrReq() noexcept;
		/** Invoked by the Dynamic Service FSM.
			Invokes the returnToSender() operation of the
			current reference count decrement request.
		 */
		void	returnDecrReq() noexcept;
		/** Invoked by the Dynamic Service FSM.
			Increments the reference count.
		 */
		void	incrementRefCount() noexcept;
		/** Invoked by the Dynamic Service FSM.
			Decrements the reference count.
		 */
		void	decrementRefCount() noexcept;
		/** Invoked by the Dynamic Service FSM.
			Removes each notification request from the
			notification list and invokes the returnToSender()
			on each.
		 */
		void	notifyClosing() noexcept;
		/** Invoked by the Dynamic Service FSM.
			Returns true if the reference count is zero.
		 */
		bool	refCountIsZero() noexcept;
	protected:
		/** Invoked when an open request is received.
		 */
		void	request(	Mt::Itc::Srv::Open::Req::Api::
							OpenReq&			msg
							) noexcept;
		/** Invoked when a close request is received.
		 */
		void	request(	Mt::Itc::Srv::Close::Req::Api::
							CloseReq&			msg
							) noexcept;
		/** Invoked when a request to increment the reference
			count is received.
		 */
		void	request(	typename Dyn::Ref::Req::Api<dynSrvType>::
							IncrReq&			msg
							) noexcept;
		/** Invoked when a request to decrement the reference
			count is received.
		 */
		void	request(	typename Dyn::Ref::Req::Api<dynSrvType>::
							DecrReq&			msg
							) noexcept;
		/** Invoked when a release notification request is received.
		 */
		void	request(	typename Dyn::Cli::Req::Api<dynSrvType>::
							ReleaseReq&			msg
							) noexcept;
		/** Invoked when a release notification cancelation
			request is received.
		 */
		void	request(	typename Dyn::Cli::Req::Api<dynSrvType>::
							CancelReleaseReq&	msg
							) noexcept;
		/** Invoked when a prepare to close request is received.
		 */
		void	request(	typename Dyn::Creator::Req::Api<dynSrvType>::
							DeactivateReq&		msg
							) noexcept;
	protected:
		/** This operation is implemented by sub-classes.
			The implementation takes whatever action is necessary
			to "open" the sub-class. This typically entails binding
			to lower layer services. Function call semantics are
			assumed, and thus ITC must be done synchronously.
		 */
		virtual void	openDynamicService() noexcept=0;
		/** This operation is implemented by sub-classes.
			The implementation takes whatever action is necessary
			to "close" the sub-class in preparation for subsequent
			destruction. Function call semantics are assumed,
			and thus ITC must be done synchronously.
		 */
		virtual void	closeDynamicService() noexcept=0;
		/** This operation is implemented by sub-classses.
			It returns a reference to the actual Dynamic Service.
		 */
		virtual dynSrvType&		getDynSrv() noexcept=0;
	};

template <class dynSrvType>
Core<dynSrvType>::Core(Mt::Itc::PostMsgApi& papi) noexcept:
		Creator::Req::Api<dynSrvType>(papi),
		_state(*this),
		_refCount(0)
		{
	}

template <class dynSrvType>
void	Core<dynSrvType>::activateDynamicService() noexcept{
	openDynamicService();
	}

template <class dynSrvType>
void	Core<dynSrvType>::deactivateDynamicService() noexcept{
	closeDynamicService();
	}

template <class dynSrvType>
void	Core<dynSrvType>::finishedWithOpenReq() noexcept{
	_currentOpenReq->returnToSender();
	}

template <class dynSrvType>
void	Core<dynSrvType>::finishedWithClosePrepReq() noexcept{
	_currentDeactivateReq->returnToSender();
	}

template <class dynSrvType>
void	Core<dynSrvType>::finishedWithCloseReq() noexcept{
	_currentCloseReq->returnToSender();
	}

template <class dynSrvType>
void	Core<dynSrvType>::cancelNotificationReq() noexcept{
	_notificationList.remove(&_currentCancelReq->getPayload().getReqToCancel());
	_currentCancelReq->getPayload().getReqToCancel().returnToSender();
	_currentCancelReq->returnToSender();
	}

template <class dynSrvType>
void	Core<dynSrvType>::returnIncrReq() noexcept{
	_currentIncrReq->returnToSender();
	}

template <class dynSrvType>
void	Core<dynSrvType>::returnDecrReq() noexcept{
	_currentDecrReq->returnToSender();
	}

template <class dynSrvType>
void	Core<dynSrvType>::incrementRefCount() noexcept{
	++_refCount;
	}

template <class dynSrvType>
void	Core<dynSrvType>::decrementRefCount() noexcept{
	--_refCount;
	}

template <class dynSrvType>
void	Core<dynSrvType>::notifyClosing() noexcept{
	typename Dyn::Cli::Req::Api<dynSrvType>::ReleaseReq* next;
	while((next=_notificationList.get())){
		next->returnToSender();
		}
	}

template <class dynSrvType>
bool	Core<dynSrvType>::refCountIsZero() noexcept{
	return !_refCount;
	}

template <class dynSrvType>
void	Core<dynSrvType>::request(	Mt::Itc::Srv::Open::
									Req::Api::OpenReq&		msg
									) noexcept{
	_currentOpenReq	= &msg;
	_state.open();
	}

template <class dynSrvType>
void	Core<dynSrvType>::request(	Mt::Itc::Srv::Close::
									Req::Api::CloseReq&		msg
									) noexcept{
	_currentCloseReq	= &msg;
	_state.close();
	}

template <class dynSrvType>
void	Core<dynSrvType>::request(	typename Dyn::Ref::Req::
									Api<dynSrvType>::IncrReq&	msg
									) noexcept{
	_currentIncrReq	= &msg;
	_state.incrementRefCount();
	}

template <class dynSrvType>
void	Core<dynSrvType>::request(	typename Dyn::Ref::Req::
									Api<dynSrvType>::DecrReq&	msg
									) noexcept{
	_currentDecrReq	= &msg;
	_state.decrementRefCount();
	}

template <class dynSrvType>
void	Core<dynSrvType>::request(	typename Dyn::Cli::Req::
									Api<dynSrvType>::ReleaseReq&	msg
									) noexcept{
	_notificationList.put(&msg);
	_state.notificationReq();
	}

template <class dynSrvType>
void	Core<dynSrvType>::request(	typename Dyn::Cli::Req::
									Api<dynSrvType>::CancelReleaseReq&	msg
									) noexcept{
	_currentCancelReq	= &msg;
	_state.cancelNotificationReq();
	}

template <class dynSrvType>
void	Core<dynSrvType>::request(	typename Dyn::Creator::Req::
									Api<dynSrvType>::DeactivateReq&	msg
									) noexcept{
	_currentDeactivateReq	= &msg;
	_state.deactivateReq();
	}

}
}
}
}

#endif
