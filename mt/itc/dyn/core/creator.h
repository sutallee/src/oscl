/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_mt_itc_dyn_creatorh_
#define _oscl_mt_itc_dyn_creatorh_
#include "cli.h"
#include "oscl/mt/itc/mbox/sap.h"
#include "oscl/mt/itc/mbox/srvreq.h"
#include "oscl/mt/itc/srv/close.h"
#include "oscl/mt/itc/srv/closeapi.h"
#include "oscl/mt/itc/mbox/syncrh.h"

/** */
namespace Oscl {
/** */
namespace Mt {
/** */
namespace Itc {
/** */
namespace Dyn {
/** */
namespace Creator {
/** */
namespace Req {

/** This template class defines interface to the Dynamic
	Service creator clients.
 */
template <class dynSrvType>
class Api :
		public Mt::Itc::Srv::CloseSync,
		public Mt::Itc::Srv::Close::Req::Api,
		public Dyn::Cli::Req::Api<dynSrvType>
		{
	public:
		/** */
		typedef Mt::Itc::SAP< Req::Api<dynSrvType> >			SAP;
		/** */
		typedef Mt::Itc::ConcreteSAP< Req::Api<dynSrvType> >	ConcreteSAP;
	public:
		/** */
		class DeactivatePayload {};
		/** */
		typedef
			Mt::Itc::SrvRequest<	Req::Api<dynSrvType>,
									DeactivatePayload
									> DeactivateReq;
	private:
		/** */
		ConcreteSAP		_sap;
	public:
		/** */
		Api(Mt::Itc::PostMsgApi& papi):
			Mt::Itc::Srv::CloseSync(*this,papi),
			Dyn::Cli::Req::Api<dynSrvType>(papi),
			_sap(*this,papi){}
		/** */
		inline SAP&		getCreatorSAP(){return _sap;}
	public:
		using Mt::Itc::Srv::Close::Req::Api::request;
		using Dyn::Cli::Req::Api<dynSrvType>::request;

		/** */
		virtual void	request(DeactivateReq& msg) noexcept=0;
	public:
		/** */
		void	deactivate() noexcept;
	};

template <class dynSrvType>
void	Api<dynSrvType>::deactivate() noexcept {
	DeactivatePayload	payload;
	SyncReturnHandler	srh;
	DeactivateReq		req(*this,payload,srh);
	_sap.postSync(req);
	}

}
}
}
}
}
}

#endif
