/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_mt_itc_dyn_core_refh_
#define _oscl_mt_itc_dyn_core_refh_
#include "oscl/mt/itc/mbox/srvreq.h"
#include "oscl/mt/itc/mbox/sap.h"
#include "oscl/mt/itc/mbox/syncrh.h"

/** */
namespace Oscl {
/** */
namespace Mt {
/** */
namespace Itc {
/** */
namespace Dyn {
/** */
namespace Ref {
/** */
namespace Req {

/** This class implements the reference counting interface of
	the Dynamic Service framework.
 */
template <class dynSrvType>
class Api {
	public:
		/** */
		typedef
			Mt::Itc::SAP< Dyn::Ref::Req::Api<dynSrvType> >
				SAP;
		/** */
		typedef
			Mt::Itc::ConcreteSAP< Dyn::Ref::Req::Api<dynSrvType> >
				ConcreteSAP;
	public:
		/** */
		class IncrPayload {};
		/** */
		typedef
			Mt::Itc::SrvRequest<	Dyn::Ref::Req::Api<dynSrvType>,
									IncrPayload
									> IncrReq;
	public:
		/** */
		class DecrPayload {};
		/** */
		typedef
			Mt::Itc::SrvRequest<	Dyn::Ref::Req::Api<dynSrvType>,
									DecrPayload
									> DecrReq;
	public:
		/** The interface for receiving reference count
			increment requests.
		 */
		virtual void	request(IncrReq& msg) noexcept=0;
		/** The interface for receiving reference count
			decrement requests.
		 */
		virtual void	request(DecrReq& msg) noexcept=0;
	private:
		/** */
		ConcreteSAP		_sap;
	public:
		/** */
		Api(Mt::Itc::PostMsgApi& pma):_sap(*this,pma){}
		/** */
		virtual ~Api(){}
	public:
		/** */
		inline SAP&		getRefSAP() noexcept {return _sap;}
		/** */
		virtual dynSrvType&		getDynSrv() noexcept=0;
	public:
		/** Synchronous ITC interface to increment the
			reference count.
		 */
		void			incrRefCount() noexcept;
		/** Synchronous ITC interface to decrement the
			reference count.
		 */
		void			decrRefCount() noexcept;
	};

template <class dynSrvType>
void	Api<dynSrvType>::incrRefCount() noexcept{
	IncrPayload	payload;
	SyncReturnHandler		srh;
	IncrReq					req(*this,payload,srh);
	_sap.postSync(req);
	}

template <class dynSrvType>
void	Api<dynSrvType>::decrRefCount() noexcept{
	DecrPayload	payload;
	SyncReturnHandler		srh;
	DecrReq					req(*this,payload,srh);
	_sap.postSync(req);
	}

}
}
}
}
}
}

#endif
