/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_mt_itc_dyn_core_clih_
#define _oscl_mt_itc_dyn_core_clih_
#include "ref.h"
#include "oscl/mt/itc/mbox/srvreq.h"
#include "oscl/mt/itc/mbox/sap.h"

/** */
namespace Oscl {
/** */
namespace Mt {
/** */
namespace Itc {
/** */
namespace Dyn {
/** */
namespace Cli {
/** */
namespace Req {

/** This template interface defines the client layer of a
	Dynamic Service.
 */
template <class dynSrvType>
class Api : public Dyn::Ref::Req::Api<dynSrvType> {
	public:
		/** */
		typedef
			Mt::Itc::SAP< Dyn::Cli::Req::Api<dynSrvType> >
				SAP;
		/** */
		typedef
			Mt::Itc::ConcreteSAP< Dyn::Cli::Req::Api<dynSrvType> >
				ConcreteSAP;
	public:
		/** */
		class ReleasePayload {};
		/** */
		typedef
			Mt::Itc::SrvRequest<	Dyn::Cli::Req::Api<dynSrvType>,
									ReleasePayload
									> ReleaseReq;
	public:
		/** */
		class CancelReleasePayload {
			private:
				/** */
				ReleaseReq&		_reqToCancel;
			public:
				/** */
				CancelReleasePayload(ReleaseReq& reqToCancel):
					_reqToCancel(reqToCancel){}
				/** */
				inline ReleaseReq&	getReqToCancel()
					{return _reqToCancel;}
			};
		/** */
		typedef
			Mt::Itc::SrvRequest<	Dyn::Cli::Req::Api<dynSrvType>,
									CancelReleasePayload
									> CancelReleaseReq;
	private:
		/** */
		ConcreteSAP		_sap;
	public:
		/** */
		Api(Mt::Itc::PostMsgApi& papi) noexcept:
			Dyn::Ref::Req::Api<dynSrvType>(papi),
			_sap(*this,papi)
			{}
		/** */
		inline SAP&		getCliSAP() noexcept {return _sap;}
	public:
		using Dyn::Ref::Req::Api<dynSrvType>::request;

		/** The interface for receiving release notification requests.
		 */
		virtual void	request(ReleaseReq& msg) noexcept=0;
		/** The interface for receiving release notification
			cancelation requests.
		 */
		virtual void	request(CancelReleaseReq& msg) noexcept=0;
	};

}
}
}
}
}
}

#endif
