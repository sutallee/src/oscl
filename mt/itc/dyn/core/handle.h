/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_mt_itc_dyn_core_handleh_
#define _oscl_mt_itc_dyn_core_handleh_
#include "cli.h"

/** */
namespace Oscl {
/** */
namespace Mt {
/** */
namespace Itc {
/** */
namespace Dyn {

/** This class implements a "smart pointer" implement a
	reference counting scheme for dynamic services.
 */
template <class dynSrvType>
class Handle {
	private:
		/** */
		Dyn::Cli::Req::Api<dynSrvType>*	_dynSrv;
	public:
		/** */
		Handle(const Handle&) noexcept;
		/** */
		Handle(Dyn::Cli::Req::Api<dynSrvType>& dynSrv) noexcept;
		/** */
		Handle(Dyn::Cli::Req::Api<dynSrvType>* dynSrv) noexcept;
		/** */
		Handle() noexcept;
		/** */
		~Handle();
	public:
		/** */
		Dyn::Cli::Req::Api<dynSrvType>*	operator->() noexcept;
		/** */
		Handle&	operator=(Dyn::Cli::Req::Api<dynSrvType>*) noexcept;
		/** */
		Handle&	operator=(Dyn::Cli::Req::Api<dynSrvType>&) noexcept;
		/** */
		Handle&	operator=(const Handle&) noexcept;
		/** */
		operator bool() const noexcept;
		/** */
		bool	operator==(const Dyn::Cli::Req::Api<dynSrvType>&) noexcept;
	};

template <class dynSrvType>
Handle<dynSrvType>::Handle(const Handle<dynSrvType>& handle) noexcept{
	_dynSrv	= handle._dynSrv;
	if(_dynSrv) _dynSrv->incrRefCount();
	}

template <class dynSrvType>
Handle<dynSrvType>::Handle(Dyn::Cli::Req::Api<dynSrvType>& dynSrv) noexcept{
	_dynSrv	= &dynSrv;
	if(_dynSrv) dynSrv.incrRefCount();
	}

template <class dynSrvType>
Handle<dynSrvType>::Handle(Dyn::Cli::Req::Api<dynSrvType>* dynSrv) noexcept{
	_dynSrv	= dynSrv;
	if(_dynSrv) dynSrv->incrRefCount();
	}

template <class dynSrvType>
Handle<dynSrvType>::Handle() noexcept:_dynSrv(0){
	}

template <class dynSrvType>
Handle<dynSrvType>::~Handle(){
	if(_dynSrv) _dynSrv->decrRefCount();
	}

template <class dynSrvType>
Dyn::Cli::Req::Api<dynSrvType>*	Handle<dynSrvType>::operator->() noexcept{
	return _dynSrv;
	}

template <class dynSrvType>
Handle<dynSrvType>&	Handle<dynSrvType>::
					operator=(Dyn::Cli::Req::Api<dynSrvType>* dynSrv) noexcept{
	if(_dynSrv != dynSrv){
		if(_dynSrv) _dynSrv->decrRefCount();
		_dynSrv	= dynSrv;
		if(_dynSrv) _dynSrv->incrRefCount();
		}
	return *this;
	}

template <class dynSrvType>
Handle<dynSrvType>&	Handle<dynSrvType>::
					operator=(Dyn::Cli::Req::Api<dynSrvType>& dynSrv) noexcept{
	if(_dynSrv != &dynSrv){
		if(_dynSrv) _dynSrv->decrRefCount();
		_dynSrv	= &dynSrv;
		_dynSrv->incrRefCount();
		}
	return *this;
	}

template <class dynSrvType>
Handle<dynSrvType>&	Handle<dynSrvType>::
					operator=(const Handle<dynSrvType>& handle) noexcept{
	if(_dynSrv != handle._dynSrv){
		if(_dynSrv) _dynSrv->decrRefCount();
		_dynSrv	= handle._dynSrv;
		if(_dynSrv) _dynSrv->incrRefCount();
		}
	return *this;
	}

template <class dynSrvType>
Handle<dynSrvType>::operator bool() const noexcept{
	return _dynSrv;
	}

template <class dynSrvType>
bool	Handle<dynSrvType>::
		operator==(const Dyn::Cli::Req::Api<dynSrvType>& dynSrv) noexcept{
	return _dynSrv == &dynSrv;
	}

}
}
}
}

#endif
