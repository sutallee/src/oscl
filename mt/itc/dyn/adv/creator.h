/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_mt_itc_dyn_adv_creatorh_
#define _oscl_mt_itc_dyn_adv_creatorh_
#include "oscl/mt/itc/mbox/srvreq.h"
#include "oscl/mt/itc/dyn/adv/core.h"

/** */
namespace Oscl {
/** */
namespace Mt {
/** */
namespace Itc {
/** */
namespace Dyn {
/** */
namespace Adv {
/** */
namespace Creator {
/** */
namespace Req {

/** */
template <class dynSrvType>
class Api {
	public:
		/** */
		typedef Mt::Itc::SAP< Api<dynSrvType> >			SAP;
		/** */
		typedef Mt::Itc::ConcreteSAP< Api<dynSrvType> >	ConcreteSAP;
	public:
		/** */
		class AddPayload {
			private:
				/** */
				Dyn::Adv::Core<dynSrvType>&	_dynSrv;
			public:
				/** */
				AddPayload(Dyn::Adv::Core<dynSrvType>& dynSrv):
					_dynSrv(dynSrv){}
				/** */
				Dyn::Adv::Core<dynSrvType>&	getDynSrv() noexcept
													{return _dynSrv;}
			};
		/** */
		typedef
			Mt::Itc::SrvRequest<	Api<dynSrvType>,
									AddPayload
									> AddReq;
	public:
		/** Shut-up GCC. */
		virtual ~Api() {}
		/** */
		virtual void	request(AddReq& msg) noexcept=0;
	};

}
}
}
}
}
}
}

#endif
