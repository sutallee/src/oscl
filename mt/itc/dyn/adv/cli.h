/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_mt_itc_dyn_adv_clih_
#define _oscl_mt_itc_dyn_adv_clih_
#include "oscl/mt/itc/dyn/core/handle.h"
#include "oscl/mt/itc/mbox/srvreq.h"
#include "oscl/mt/itc/mbox/sap.h"
#include "iterator.h"

/** */
namespace Oscl {
/** */
namespace Mt {
/** */
namespace Itc {
/** */
namespace Dyn {
/** */
namespace Adv {
/** */
namespace Cli {
/** */
namespace Req {

/** */
template <class dynSrvType>
class Api {
	public:
		/** */
		typedef Mt::Itc::SAP< Api<dynSrvType> >			SAP;
		/** */
		typedef Mt::Itc::ConcreteSAP< Api<dynSrvType> >	ConcreteSAP;
	public:
		/** */
		class CreatePayload {
			private:
				/** */
				unsigned long	_lastNewServiceCount;
			public:
				/** */
				CreatePayload() noexcept: _lastNewServiceCount(0){}
				/** */
				unsigned long	getLastId() const noexcept
									{ return _lastNewServiceCount;}
				/** */
				void	setLastId(unsigned long lastNewServiceCount) noexcept
							{ _lastNewServiceCount = lastNewServiceCount;}
			};
		/** */
		typedef
			Mt::Itc::SrvRequest<	Api<dynSrvType>,
									CreatePayload
									> CreateReq;
	public:
		/** */
		class CancelCreatePayload {
			private:
				/** */
				CreateReq&	_requestToCancel;
			public:
				/** */
				CancelCreatePayload(	CreateReq& requestToCancel
											) noexcept:
										_requestToCancel(requestToCancel){}
				/** */
				CreateReq&	getRequestToCancel() noexcept
										{return _requestToCancel;}
			};
		/** */
		typedef
			SrvRequest<	Api<dynSrvType>,
						CancelCreatePayload
						> CancelCreateReq;
	public:
		/** */
		class FindPayload {
			private:
				/** */
				Dyn::Handle<dynSrvType>		_dynSrv;
				/** */
				Dyn::Adv::Iterator<dynSrvType>&	_visitor;
			public:
				/** */
				FindPayload(Dyn::Adv::Iterator<dynSrvType>& visitor) noexcept:
					_dynSrv((Dyn::Cli::Req::Api<dynSrvType>*)0),
					_visitor(visitor){}
				/** */
				Dyn::Handle<dynSrvType>	getDynSrv() noexcept
												{return _dynSrv;}
				/** */
				void	setDynSrv(Dyn::Cli::Req::Api<dynSrvType>* dynSrv) noexcept
							{_dynSrv=dynSrv;}
				/** */
				Dyn::Adv::Iterator<dynSrvType>&	getIterator() noexcept
												{return _visitor;}
			};
		/** */
		typedef
			Mt::Itc::SrvRequest<	Api<dynSrvType>,
									FindPayload
									> FindReq;
	public:
		/** */
		virtual ~Api(){}
	public:
		/** */
		virtual void	request(CreateReq& msg) noexcept=0;
		/** */
		virtual void	request(CancelCreateReq& msg) noexcept=0;
		/** */
		virtual void	request(FindReq& msg) noexcept=0;
	};

}
}
}
}
}
}
}

#endif
