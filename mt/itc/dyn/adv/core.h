/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_mt_itc_dyn_adv_coreh_
#define _oscl_mt_itc_dyn_adv_coreh_
#include "oscl/mt/itc/dyn/core/core.h"
#include "oscl/mt/itc/mbox/clirsp.h"
#include "oscl/queue/queueitem.h"

/** */
namespace Oscl {
/** */
namespace Mt {
/** */
namespace Itc {
/** */
namespace Dyn {
/** */
namespace Adv {

/** The advertiser template extends the basic Dynamic Service core
	to include resources required by the advertiser service.
	The sub class must implement openDynamicService(),
	closeDynamicService(), and getDynSrv(), which are inherited
	from the Dynamic Service Core.
 */
template <class dynSrvType>
class Core : public Dyn::Core<dynSrvType> {
	public:
		/** */
		using Dyn::Core<dynSrvType>::request;

	public:
		/** */
		typedef
			CliResponse<	Dyn::Cli::Req::Api<dynSrvType>,
							Core<dynSrvType>,
							typename Dyn::Cli::Req::Api<dynSrvType>::ReleasePayload
							> ReleaseResp;
	public:
		/** This interface is called by the core and implemented by the
			advertiser itself.
		 */
		class ReleaseApi {
			public:
				/** Shut-up GCC. */
				virtual ~ReleaseApi() {}

				/** Invoked by the core when the dynamic service has
					been requested to deactivate in preparation for
					being destroyed.
					The implementation should release all references
					to the dynamic service as a result of this operation
					being invoked.
				 */
				virtual void	release(	Dyn::Adv::
											Core<dynSrvType>&	dynSrv
											) noexcept=0;
			};
	public:
		/** This class describes an item that can be used by
			the dynamic service advertiser to keep a list of
			currently active dynamic services.
		 */
		class DynSrvItem : public QueueItem {
			private:
				/** */
				Dyn::Adv::Core<dynSrvType>&		_dynSrv;
			public:
				/** */
				DynSrvItem(Dyn::Adv::Core<dynSrvType>& dynSrv) noexcept;

				/** */
				Dyn::Adv::Core<dynSrvType>&		getDynSrv() noexcept;
			};

	private:
		/** A resource to be used by the dynamic service
			advertiser for maintaining its list of dynamic
			services.
		 */
		DynSrvItem				_queueItem;
		/** */
		typename Dyn::Cli::Req::Api<dynSrvType>::ReleasePayload	_releasePayload;
		/** */
		ReleaseResp				_releaseResp;
		/** */
		ReleaseApi&				_advertiser;
	public:
		/** */
		Core(	Mt::Itc::PostMsgApi&	dynSrvPostApi,
				ReleaseApi&				advertiser,
				Mt::Itc::PostMsgApi&	advPostApi
				) noexcept;
		/** Invoked by the advertiser when this dynamic service
			is added to the advertiser's list of dynamic services.
		 */
		void			notifyOnRelease() noexcept;
		/** */
		DynSrvItem&		getQueueItem() noexcept;
		/** */
		void			response(ReleaseResp& msg) noexcept;
	public:
		/** This operation is implemented by sub-classes.
			The implementation takes whatever action is necessary
			to "open" the sub-class. This typically entails binding
			to lower layer services. Function call semantics are
			assumed, and thus ITC must be done synchronously.
		 */
		virtual void	openDynamicService() noexcept=0;
		/** This operation is implemented by sub-classes.
			The implementation takes whatever action is necessary
			to "close" the sub-class in preparation for subsequent
			destruction. Function call semantics are assumed,
			and thus ITC must be done synchronously.
		 */
		virtual void	closeDynamicService() noexcept=0;
		/** This operation is implemented by sub-classses.
			It returns a reference to the actual Dynamic Service.
		 */
		virtual dynSrvType&		getDynSrv() noexcept=0;
	};

template <class dynSrvType>
Core<dynSrvType>::Core(	Mt::Itc::PostMsgApi&	dynSrvPostApi,
						ReleaseApi&				advertiser,
						Mt::Itc::PostMsgApi&	advPostApi
						) noexcept:
		Dyn::Core<dynSrvType>(dynSrvPostApi),
		_queueItem(*this),
		_releaseResp(	this->getCliSAP().getReqApi(),
						*this,
						advPostApi,
						_releasePayload
						),
		_advertiser(advertiser)
		{
	}

template <class dynSrvType>
void Core<dynSrvType>::notifyOnRelease() noexcept{
	this->getCliSAP().post(_releaseResp.getSrvMsg());
	}

template <class dynSrvType>
typename Core<dynSrvType>::DynSrvItem&	Core<dynSrvType>::getQueueItem() noexcept{
	return _queueItem;
	}

template <class dynSrvType>
void Core<dynSrvType>::response(ReleaseResp& msg) noexcept{
	_advertiser.release(*this);
	}

template <class dynSrvType>
Core<dynSrvType>::DynSrvItem::
DynSrvItem(Dyn::Adv::Core<dynSrvType>& dynSrv) noexcept:
		_dynSrv(dynSrv)
		{
	}

template <class dynSrvType>
Dyn::Adv::Core<dynSrvType>&
	Core<dynSrvType>::DynSrvItem::getDynSrv() noexcept{
	return _dynSrv;
	}
}
}
}
}
}

#endif
