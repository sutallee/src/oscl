/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_mt_itc_dyn_adv_serviceh_
#define _oscl_mt_itc_dyn_adv_serviceh_
#include "oscl/queue/queue.h"
#include "creator.h"
#include "oscl/mt/itc/dyn/adv/core.h"
#include "clisync.h"
#include "creatorsync.h"
#include "cli.h"

/** */
namespace Oscl {
/** */
namespace Mt {
/** */
namespace Itc {
/** */
namespace Dyn {
/** */
namespace Adv {

/** This template implements behavior for the Advertier of Dynamic
	Services.
 */
template <class dynSrvType>
class Service :	public Core<dynSrvType>::ReleaseApi,
				public Creator::SyncApi<dynSrvType>,
				public Cli::SyncApi<dynSrvType>,
				public Creator::Req::Api<dynSrvType>,
				public Cli::Req::Api<dynSrvType>
					{
	private:
		/** */
		Queue< typename Cli::Req::Api<dynSrvType>::CreateReq >
			_newObjNofiticationList;
		/** */
		Queue< typename Core<dynSrvType>::DynSrvItem >		_dynSrvList;
		/** */
		typename Cli::Req::Api<dynSrvType>::ConcreteSAP		_cliSAP;
		/** */
		typename Creator::Req::Api<dynSrvType>::ConcreteSAP	_creatorSAP;
		/** */
		unsigned long								_newObjCount;
	public:
		/** */
		Service(Mt::Itc::PostMsgApi& adv);
		/** */
		typename Cli::Req::Api<dynSrvType>::SAP&		getCliSAP() noexcept;
		/** */
		typename Creator::Req::Api<dynSrvType>::SAP&	getCreatorSAP() noexcept;
		/** */
		void	release(Core<dynSrvType>& dynSrv) noexcept;
	public:
		/** */
		void	add(Core<dynSrvType>& dynSrv) noexcept;
	private:
		/** */
		Dyn::Handle<dynSrvType>	find(Iterator<dynSrvType>& id) noexcept;
	private: // Creator::Req::Api<dynSrvType>
		/** This operation is invoked when the advertiser receives a request
			to add a new dynamic object to its knowledge base.
		 */
		void	request(typename Creator::Req::Api<dynSrvType>::AddReq& msg) noexcept;
	private: // Cli::Req::Api<dynSrvType>
		/** This operation is invoked when the advertiser receives a request
			to notify a client when new dynamic objects become available.
			The message contains an identifier that represents the last
			time the client witnessed a new object creation. If the
			identifier is different from the advertisers copy, then
			the notification request is returned immediately. Otherwise,
			the notification request is held until it is either canceled
			by the client or a new dynamic object becomes available.
		 */
		void	request(typename Cli::Req::Api<dynSrvType>::CreateReq& msg) noexcept;
		/** */
		void	request(	typename Cli::Req::Api<dynSrvType>::
							CancelCreateReq&			msg
							) noexcept;
		/** */
		void	request(typename Cli::Req::Api<dynSrvType>::FindReq& msg) noexcept;
	};

template <class dynSrvType>
Service<dynSrvType>::Service(Mt::Itc::PostMsgApi& adv):
		_cliSAP(*this,adv),
		_creatorSAP(*this,adv),
		_newObjCount(0)
		{
	}

template <class dynSrvType>
typename Cli::Req::Api<dynSrvType>::SAP&	Service<dynSrvType>::getCliSAP() noexcept {
	return _cliSAP;
	}

template <class dynSrvType>
typename Creator::Req::Api<dynSrvType>::SAP&	Service<dynSrvType>::getCreatorSAP() noexcept {
	return _creatorSAP;
	}

template <class dynSrvType>
void	Service<dynSrvType>::release(Core<dynSrvType>& dynSrv) noexcept{
	_dynSrvList.remove(&dynSrv.getQueueItem());
	dynSrv.decrRefCount();
	}

template <class dynSrvType>
void	Service<dynSrvType>::add(Core<dynSrvType>& dynSrv) noexcept{
	typename Creator::Req::Api<dynSrvType>::AddPayload	payload(dynSrv);
	SyncReturnHandler						srh;
	typename Creator::Req::Api<dynSrvType>::AddReq		req(*this,payload,srh);
	_creatorSAP.postSync(req);
	}

template <class dynSrvType>
Dyn::Handle<dynSrvType> Service<dynSrvType>::find(	Iterator<dynSrvType>& id
												) noexcept{
	typename Cli::Req::Api<dynSrvType>::FindPayload	payload(id);
	SyncReturnHandler						srh;
	typename Cli::Req::Api<dynSrvType>::FindReq		req(*this,payload,srh);
	_cliSAP.postSync(req);
	return req.getPayload().getDynSrv();
	}

template <class dynSrvType>
void	Service<dynSrvType>::request(	typename Creator::Req::
										Api<dynSrvType>::AddReq&	msg
										) noexcept{
	Core<dynSrvType>&	dynSrv	= msg.getPayload().getDynSrv();
	_dynSrvList.put(&dynSrv.getQueueItem());
	dynSrv.incrRefCount();
	dynSrv.notifyOnRelease();
	++_newObjCount;
	if(_newObjCount == 0) ++_newObjCount;
	typename Cli::Req::Api<dynSrvType>::CreateReq*	next;
	while((next=_newObjNofiticationList.get())){
		next->getPayload().setLastId(_newObjCount);
		next->returnToSender();
		}
	msg.returnToSender();
	}

template <class dynSrvType>
void	Service<dynSrvType>::request(	typename Cli::Req::Api<dynSrvType>::
										CreateReq&					msg
										) noexcept{
	if(msg.getPayload().getLastId() == _newObjCount){
		_newObjNofiticationList.put(&msg);
		return;
		}
	msg.getPayload().setLastId(_newObjCount);
	msg.returnToSender();
	}

template <class dynSrvType>
void
	Service<dynSrvType>::request(	typename Cli::Req::Api<dynSrvType>::
									CancelCreateReq&			msg
									) noexcept{
	if(_newObjNofiticationList.remove(&msg.getPayload().getRequestToCancel())){
		msg.getPayload().getRequestToCancel().returnToSender();
		}
	msg.returnToSender();
	}

template <class dynSrvType>
void
	Service<dynSrvType>::request(	typename Cli::Req::Api<dynSrvType>::
									FindReq&					msg
									) noexcept{
	typename Core<dynSrvType>::DynSrvItem*			next;
	for(next=_dynSrvList.first();next;next=_dynSrvList.next(next)){
		Dyn::Adv::Iterator<dynSrvType>&	dit	= msg.getPayload().getIterator();
		if(dit.next(next->getDynSrv().getDynSrv())) break;
		}
	if(next){
		msg.getPayload().setDynSrv(&next->getDynSrv());
		}
	else {
		msg.getPayload().setDynSrv((Dyn::Cli::Req::Api<dynSrvType>*)0);
		}
	msg.returnToSender();
	}
}
}
}
}
}

#endif
