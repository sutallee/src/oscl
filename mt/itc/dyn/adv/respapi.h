/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_mt_itc_dyn_adv_cli_respapih_
#define _oscl_mt_itc_dyn_adv_cli_respapih_
#include "cli.h"
#include "oscl/mt/itc/mbox/clirsp.h"

/** */
namespace Oscl {
/** */
namespace Mt {
/** */
namespace Itc {
/** */
namespace Dyn {
/** */
namespace Adv {
/** */
namespace Cli {
/** */
namespace Resp {

/** */
template <class dynSrvType>
class Api {
	public:
		/** */
		typedef
			Mt::Itc::CliResponse<	Dyn::Adv::Cli::Req::Api<dynSrvType>,
									Api<dynSrvType>,
									typename Dyn::Adv::Cli::Req::Api<dynSrvType>::
										CreatePayload
									> CreateResp;
		/** */
		typedef
			Mt::Itc::CliResponse<	Dyn::Adv::Cli::Req::Api<dynSrvType>,
									Api<dynSrvType>,
									typename Dyn::Adv::Cli::Req::Api<dynSrvType>::
										CancelCreatePayload
									> CancelResp;
	public:
		/** Shut-up GCC. */
		virtual ~Api() {}
		/** */
		virtual void	response(CreateResp& msg) noexcept=0;
		/** */
		virtual void	response(CancelResp& msg) noexcept=0;
	};

}
}
}
}
}
}
}

#endif
