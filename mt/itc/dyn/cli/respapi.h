/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_mt_itc_dyn_user_respapih_
#define _oscl_mt_itc_dyn_user_respapih_
#include "oscl/mt/itc/dyn/core/cli.h"
#include "oscl/mt/itc/mbox/clirsp.h"

/** */
namespace Oscl {
/** */
namespace Mt {
/** */
namespace Itc {
/** */
namespace Dyn {
/** */
namespace Cli {
/** */
namespace Resp {

/** This abstract template class describes a response API for receiving
	response messages from the user layer of a dynamic object. The
	user layer of the Dynamic Object framework defines a reference
	release notification request and a request to cancel a reference
	release notification request. The client must implement the two
	response operations defined in this class.
 */
template <class dynSrvType>
class Api {
	public:
		/** This type defines the client response message that is
			returned to the client when the contained reference release
			notification request is returned by the Dyanmic Object.
		 */
		typedef
			CliResponse<	Dyn::Cli::Req::Api<dynSrvType>,
							Api<dynSrvType>,
							typename Dyn::Cli::Req::Api<dynSrvType>::ReleasePayload
							> ReleaseResp;
		/** This type defines the client response message that is
			returned to the client when the contained request to cancel
			the specified reference release notification request is
			returned by the Dyanmic Object.
		 */
		typedef
			CliResponse<	Dyn::Cli::Req::Api<dynSrvType>,
							Api<dynSrvType>,
							typename Dyn::Cli::Req::Api<dynSrvType>::CancelReleasePayload
							> CancelReleaseResp;
	public:
		/** Shut-up GCC. */
		virtual ~Api() {}
		/** This abstract operation defines the interface for receiving
			reference release notification responses. Once received, the
			client must release all references to the corresponding
			dynamic object. Unlike other "state" notifications, this
			notification only happens one time, and the client
			should not use the message to register for notification again.
		 */
		virtual void	response(ReleaseResp& msg) noexcept=0;
		/** This abstract operation defines the interface for receiving
			reference release notification cancelation responses.
			Once received, the client association with the Dynamic
			Object has been terminated. The canceled reference release
			notification request will have been received before this
			operation is invoked.
		 */
		virtual void	response(CancelReleaseResp& msg) noexcept=0;
	};
}
}
}
}
}
}

#endif
