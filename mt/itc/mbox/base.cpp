/*
   Copyright (C) 2020 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "base.h"
#include "oscl/mt/scopesync.h"
#include "oscl/mt/thread.h"

using namespace Oscl::Mt::Itc;

Base::Base(
	Oscl::Mt::Sema::SignalApi&	sigApi
	) noexcept:
		_sigApi(sigApi),
		_mboxQ()
		{
	}

Base::~Base(){
	}

void	Base::processMessages(unsigned nMessages) noexcept{

	bool	unlimited	= (nMessages)?false:true;

	for(;nMessages || unlimited; --nMessages){

		Oscl::Mt::Itc::Msg*	msg;

			{
			Oscl::Mt::ScopeSync sync(_mutex);
			msg = _mboxQ.get();
			}

		if(!msg){
			break;
			}

		msg->process();
		}
	}

void	Base::post(Oscl::Mt::Itc::Msg& msg) noexcept{
		{
			Oscl::Mt::ScopeSync sync(_mutex);
			_mboxQ.put(&msg);
		}
		_sigApi.signal();
	}

void	Base::postSync(Oscl::Mt::Itc::Msg& msg) noexcept{

	post(msg);

	Oscl::Mt::Thread::getCurrent().syncWait();
	}

void	Base::signal(void) noexcept{
	_sigApi.signal();
	}

void	Base::suSignal(void) noexcept{
	_sigApi.suSignal();
	}

