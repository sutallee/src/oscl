/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_mt_itc_mbox_clievth_
#define _oscl_mt_itc_mbox_clievth_
#include "climsg.h"
#include "asyncrh.h"
#include "srvevt.h"

/** */
namespace Oscl {
/** */
namespace Mt {
/** */
namespace Itc {

/** A client event differs from a client response in that its
	payload has no state and therefore no payload instance
	or accessors are required. The payload type is only used to
	identify the response and request type.
 */
template <class ReqApi,class RespApi,class Payload>
class CliEvent : public CliMsg {
	private:
		/** */
		RespApi&					_cli;
		/** */
		AsyncReturnHandler			_arh;
		/** */
		SrvEvent<ReqApi,Payload>	_msg;
	public:
		/** */
		CliEvent(	ReqApi&			srv,
					RespApi&		cli,
					PostMsgApi&		papi
					) noexcept;
		/** */
		virtual ~CliEvent();
		/** */
		void						process() noexcept;
		/** */
		SrvEvent<ReqApi,Payload>&	getSrvMsg() noexcept;
	};

template <class ReqApi,class RespApi,class Payload>
CliEvent<ReqApi,RespApi,Payload>
		::CliEvent(	ReqApi&			srv,
					RespApi&		cli,
					PostMsgApi&		papi
					) noexcept:
		_cli(cli),
		_arh(papi,*this),
		_msg(srv,_arh)
		{
	}

template <class ReqApi,class RespApi,class Payload>
CliEvent<ReqApi,RespApi,Payload>::~CliEvent(){
	}

template <class ReqApi,class RespApi,class Payload>
void CliEvent<ReqApi,RespApi,Payload>::process() noexcept{
	_cli.response(*this);
	}

template <class ReqApi,class RespApi,class Payload>
SrvEvent<ReqApi,Payload>&
		CliEvent<ReqApi,RespApi,Payload>::getSrvMsg() noexcept{
	return _msg;
	}

};
};
};

#endif
