/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_mt_itc_mbox_mboxh_
#define _oscl_mt_itc_mbox_mboxh_
#include "oscl/mt/itc/getmsgapi.h"
#include "oscl/mt/itc/postmsgapi.h"
#include "oscl/mt/sema/sigapi.h"
#include "msg.h"
#include "oscl/mt/thread.h"
#include "mutex.h"
#include "sema.h"

/** */
namespace Oscl {
/** */
namespace Mt {
/** */
namespace Itc {

/** */
class Mailbox :	public GetMsgApi,
				public PostMsgApi,
				public Oscl::Mt::Sema::SignalApi,
				private Queue<Msg>
				{
	private:
		/** */
		volatile bool		_signaled;
		/** */
		MailboxMutex		_mutex;
		/** */
		MailboxSema			_sema;

	public:
		/** */
		Mailbox() noexcept;
	public:	// PostMsgApi
		/** */
		void	post(Msg& msg) noexcept;
		/** */
		void	postSync(Msg& msg) noexcept;
	public:	// GetMsgApi
		/** */
		Msg*	waitNext() noexcept;
	public: // SignalApi interface
		/** */
		void signal(void) noexcept;
		/** */
		void suSignal(void) noexcept;
	};

}
}
}

#endif

