/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_mt_itc_mbox_srvevth_
#define _oscl_mt_itc_mbox_srvevth_
#include "srvmsg.h"

/** */
namespace Oscl {
/** */
namespace Mt {
/** */
namespace Itc {

/** A server event differs from a server request in that its
	payload has no state and therefore no payload instance
	or accessors are required. The payload type is only used to
	identify the request type.
 */
template <class ReqApi,class Payload>
class SrvEvent : public SrvMsg {
	private:
		/** */
		ReqApi&		_reqApi;
	public:
		/** */
		Payload		_payload;
	public:
		/** */
		SrvEvent(	ReqApi&			reqApi,
					ReturnHandler&	returnHandler
					) noexcept;
		/** */
		void		process() noexcept;
	};

template <class ReqApi,class Payload>
SrvEvent<ReqApi,Payload>
		::SrvEvent(	ReqApi&			reqApi,
					ReturnHandler&	returnHandler
					) noexcept:
		SrvMsg(returnHandler),
		_reqApi(reqApi)
		{
	}

template <class ReqApi,class Payload>
void		SrvEvent<ReqApi,Payload>::process() noexcept{
	_reqApi.request(*this);
	}

}
}
}

#endif
