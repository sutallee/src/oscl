/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_mt_itc_mbox_clipayrsph_
#define _oscl_mt_itc_mbox_clipayrsph_
#include "climsg.h"
#include "srvreq.h"
#include "asyncrh.h"

/** */
namespace Oscl {
/** */
namespace Mt {
/** */
namespace Itc {

/** */
template <class ReqApi,class RespApi,class CliPayload,class SrvPayload>
class CliPayloadResponse : public CliMsg {
	private:
		/** */
		RespApi&						_respApi;

		/** */
		AsyncReturnHandler				_arh;

		/** */
		SrvRequest<ReqApi,SrvPayload>	_msg;

		/** */
		CliPayload&						_payload;

	public:

		/** */
		CliPayloadResponse(	ReqApi&				reqApi,
							RespApi&			respApi,
							PostMsgApi&			papi,
							CliPayload&			payload
							) noexcept;

		/** */
		virtual ~CliPayloadResponse();

		/** */
		void			process() noexcept;

		/** */
		SrvMsg&			getSrvMsg() noexcept;

		/** */
		SrvPayload&		getPayload() noexcept;

		/** */
		CliPayload&		getCliPayload() noexcept;
	};

template <class ReqApi,class RespApi,class CliPayload,class SrvPayload>
CliPayloadResponse<ReqApi,RespApi,CliPayload,SrvPayload>
		::CliPayloadResponse(	ReqApi&			reqApi,
								RespApi&		respApi,
								PostMsgApi&		papi,
								CliPayload&		payload
								) noexcept:
		_respApi(respApi),
		_arh(papi,*this),
		_msg(reqApi,payload,_arh),
		_payload(payload)
		{
	}

template <class ReqApi,class RespApi,class CliPayload,class SrvPayload>
CliPayloadResponse<ReqApi,RespApi,CliPayload,SrvPayload>
		::~CliPayloadResponse(){
	}

template <class ReqApi,class RespApi,class CliPayload,class SrvPayload>
void CliPayloadResponse<ReqApi,RespApi,CliPayload,SrvPayload>
		::process() noexcept{
	_respApi.response(*this);
	}

template <class ReqApi,class RespApi,class CliPayload,class SrvPayload>
SrvMsg&	CliPayloadResponse<ReqApi,RespApi,CliPayload,SrvPayload>
		::getSrvMsg() noexcept{
	return _msg;
	}

template <class ReqApi,class RespApi,class CliPayload,class SrvPayload>
SrvPayload&		CliPayloadResponse<ReqApi,RespApi,CliPayload,SrvPayload>
		::getPayload() noexcept {
	return _payload;
	}

template <class ReqApi,class RespApi,class CliPayload,class SrvPayload>
CliPayload&		CliPayloadResponse<ReqApi,RespApi,CliPayload,SrvPayload>
		::getCliPayload() noexcept {
	return _payload;
	}

};
};
};

#endif
