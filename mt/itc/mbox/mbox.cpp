/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "mbox.h"
#include "oscl/mt/scopesync.h"

using namespace Oscl;

Mt::Itc::Mailbox::Mailbox() noexcept:
	_signaled(false),
	_mutex(),
	_sema()
	{
	}

void	Mt::Itc::Mailbox::post(Mt::Itc::Msg& msg) noexcept {
		{
		Mt::ScopeSync	sync(_mutex);
		put(&msg);
		}
	_sema.signal();
	}

void	Mt::Itc::Mailbox::postSync(Mt::Itc::Msg& msg) noexcept {
	post(msg);
	Mt::Thread::getCurrent().syncWait();
	}

Mt::Itc::Msg*	Mt::Itc::Mailbox::waitNext(void) noexcept {
	Mt::Itc::Msg*			msg;

	for(;;) {
		if(_signaled){
			_signaled	= false;
			return (Mt::Itc::Msg*)0;
			}
			{
			Mt::ScopeSync	sync(_mutex);
			if((msg=get())){
				return (msg);
				}
			}
		_sema.wait();
		}

	return msg;
	}

void Mt::Itc::Mailbox::signal(void) noexcept {
	if(_signaled){
		return;
		}
	_signaled	= true;
	_sema.signal();
	}

void Mt::Itc::Mailbox::suSignal(void) noexcept {
	if(_signaled){
		return;
		}
	_signaled	= true;
	_sema.suSignal();
	}

