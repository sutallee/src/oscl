/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_mt_itc_mbox_srvapih_
#define _oscl_mt_itc_mbox_srvapih_
#include "oscl/mt/sema/sigapi.h"
#include "oscl/mt/itc/postmsgapi.h"

/** */
namespace Oscl {
/** */
namespace Mt {
/** */
namespace Itc {

/** The intent of this interface is to allow for driver
	threads/servers to be created before the actual driver
	is instantiated. Such drivers use the semaphore functionality
	of the associated mailbox/server for interrupt processing.
	When the driver is created, it sets the SignaledApi 
	(using setSignaledApi()) that is associated with the
	interrupt handling of the driver.
	When the driver's interrupt handler is invoked, it signals
	the SignalApi that can be obtained using the getSignalApi()
	operation of this interface. When the signal is recognized
	by the thread, the SignaledApi set by the client is invoked.
 */
class ServerApi {
	public:
		/** */
		class SignaledApi{
			public:
				/** Shut-up GCC. */
				virtual ~SignaledApi() {}
				/** */
				virtual void	signaled() noexcept=0;
			};
	public:
		/** Shut-up GCC. */
		virtual ~ServerApi() {}

		/** This operation may be invoked to set a handler for the
			server's mboxSignaled() operation. Only a single such
			handler may be set for each server instance. A null
			pointer may be passed to this operation to disassociate
			any previous handler. Any previously set handler is overriden
			by subsequent invocations of this operation.
			The intent of this functionality is to allow for driver
			threads/servers to be created before the actual driver
			associated with the thread/server is created. When the
			driver is created, it sets the SignaledApi associated
			with the interrupt handling that invokes the corresponding
			SignalApi related to this same ServerApi.
		 */
		virtual void	setSignaledApi(SignaledApi* sapi) noexcept=0;
		/** */
		virtual Oscl::Mt::Sema::SignalApi&		getSignalApi() noexcept=0;
		/** */
		virtual Oscl::Mt::Itc::PostMsgApi&	getPostMsgApi() noexcept=0;
	};

}
}
}

#endif
