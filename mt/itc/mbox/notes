The one point of type-safe vulnerability in the MT
framework is the lack of type safety when sending
messages via the PostMsgApi. It is possible (and has
happened) that a programmer, intending to send a
request to a server, invokes the PostMsgApi
with the client response message instead of the
server request message. This is particularly easy
since the programmer must use the getSrvMsg() of
the CliResponse in order to get the server message
to send. This is complicated because the CliResponse
is a Msg. The result is that the programmer either
deadlocks (by calling postSync()) or sends the response
message to himself (by calling post()).

The problem is unlikely to ocurr in the reverse direction
since the AsyncReturnHandler is typically the only thing
that sends a message from server to client, and the programmer
would have to accidently use the getSrvMsg() operation by
accident.

Thus, to mitigate this somewhat, it may be a good
idea to create a distinction between types of SAPs.
For example a ClientSAP interface might be passed to
clients, and the post operations on this SAP would
not include the generic post(Msg) operations, but
would rather have post(SrvMsg) operations.

The files postsrvapi.h and postcliapi.h describe the
new interfaces, and the sap.h file would be modified
to contain new versions of the SAP templates implementing
the new functionality. The ConcreteSAP would implement
all of the interface variants.


NEW Mailbox design:
After changing the original mailbox concept to accomadate
pthreads, I found some "issues" with the implementation,
where the owning thread's thread semaphore was being signaled
by mailbox post()ers when the thread was waiting on a synchronous
operation, and NOT on the mailbox. Here's the original
justification for the new design (i.e. MailboxSema).

	// FIXME: The problem with this technique is that
	// the expectation of the "waiter" (the owner of the mailbox),
	// is that no thread will signal his "thread semaphore"
	// unless he is actually waiting for a mailbox message.
	// Thus, I need to implement one of these solutions:
	// o Restore the original method, where the _waiter
	//   manipulation was done under a mutex. The problem
	//   with this is that in pthread environments, the
	//	 mutex is not usable at async signal time (i.e. in the
	//   suSignal() operation.
	// o Declare a separate semaphore for the Mailbox.
	//   This will add to the size of each Mailbox, and will
	//   require a MailboxSema similar to the MailboxMutex.
	//	 This new Mailbox member would then be used exclusively
	//   by the Mailbox implementation rather than the _waiter.
	//	 Note that this also means the _waiter member can be
	//   removed from the Mailbox implementation, reducing the
	//   impact of the added MailboxSema.

