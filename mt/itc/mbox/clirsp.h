/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_mt_itc_mbox_clirsph_
#define _oscl_mt_itc_mbox_clirsph_
#include "climsg.h"
#include "asyncrh.h"
#include "srvreq.h"

/** */
namespace Oscl {
/** */
namespace Mt {
/** */
namespace Itc {

/** */
template <class ReqApi,class RespApi,class Payload>
class CliResponse : public CliMsg {
	private:
		/** */
		RespApi&					_cli;
		/** */
		AsyncReturnHandler			_arh;
		/** */
		SrvRequest<ReqApi,Payload>	_msg;
	public:
		/** */
		Payload&					_payload;
	public:
		/** */
		CliResponse(	ReqApi&			srv,
						RespApi&		cli,
						PostMsgApi&		papi,
						Payload&		payload
						) noexcept;
		/** */
		virtual ~CliResponse();
		/** */
		void						process() noexcept;
		/** */
		SrvRequest<ReqApi,Payload>&	getSrvMsg() noexcept;
		/** */
		Payload&					getPayload() const noexcept;
	};

template <class ReqApi,class RespApi,class Payload>
CliResponse<ReqApi,RespApi,Payload>
		::CliResponse(	ReqApi&				srv,
						RespApi&			cli,
						PostMsgApi&	papi,
						Payload&			payload
						) noexcept:
		_cli(cli),
		_arh(papi,*this),
		_msg(srv,payload,_arh),
		_payload(payload)
		{
	}

template <class ReqApi,class RespApi,class Payload>
CliResponse<ReqApi,RespApi,Payload>::~CliResponse(){
	}

template <class ReqApi,class RespApi,class Payload>
void CliResponse<ReqApi,RespApi,Payload>::process() noexcept{
	_cli.response(*this);
	}

template <class ReqApi,class RespApi,class Payload>
SrvRequest<ReqApi,Payload>&
		CliResponse<ReqApi,RespApi,Payload>::getSrvMsg() noexcept{
	return _msg;
	}

template <class ReqApi,class RespApi,class Payload>
Payload&		CliResponse<ReqApi,RespApi,Payload>
		::getPayload() const noexcept{
	return _payload;
	}

};
};
};

#endif
