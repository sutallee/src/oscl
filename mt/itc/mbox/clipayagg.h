/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_mt_itc_mbox_clipayaggh_
#define _oscl_mt_itc_mbox_clipayaggh_
#include "climsg.h"
#include "asyncrh.h"
#include "srvreq.h"

/** */
namespace Oscl {
/** */
namespace Mt {
/** */
namespace Itc {

/** This template supports a single memory entity with an
	extened payload. The payload must be a subclass of
	the payload defined by the server.
	This template relys on the client payload having
	a copy constructor and the C++ mechanism that allows
	the compiler to implicitly invoke a constructor for
	a temporary "prototype" of the payload given the appropriate
	arguments.
 */
template <class ReqApi,class RespApi,class CliPayload,class SrvPayload>
class CliPayloadAggregate : public CliMsg {
	private:
		/** */
		CliPayload						_payload;
	private:
		/** */
		RespApi&						_respApi;
		/** */
		AsyncReturnHandler				_arh;
		/** */
		SrvRequest<ReqApi,SrvPayload>	_msg;
	public:
		/** */
		CliPayloadAggregate(	ReqApi&				reqApi,
								RespApi&			respApi,
								PostMsgApi&			papi,
								const CliPayload&	prototype
								) noexcept;
		/** */
		virtual ~CliPayloadAggregate();
		/** */
		void			process();
		/** */
		SrvRequest<ReqApi,SrvPayload>&			getSrvMsg();
		/** */
		SrvPayload&		getPayload();
		/** */
		CliPayload&		getCliPayload();
	};

template <class ReqApi,class RespApi,class CliPayload,class SrvPayload>
CliPayloadAggregate<ReqApi,RespApi,CliPayload,SrvPayload>
		::CliPayloadAggregate(	ReqApi&				reqApi,
								RespApi&			respApi,
								PostMsgApi&			papi,
								const CliPayload&	prototype
								) noexcept:
		_payload(prototype),
		_respApi(respApi),
		_arh(papi,*this),
		_msg(reqApi,_payload,_arh)
		{
	}

template <class ReqApi,class RespApi,class CliPayload,class SrvPayload>
CliPayloadAggregate<ReqApi,RespApi,CliPayload,SrvPayload>
		::~CliPayloadAggregate(){
	}

template <class ReqApi,class RespApi,class CliPayload,class SrvPayload>
void CliPayloadAggregate<ReqApi,RespApi,CliPayload,SrvPayload>::process(){
	_respApi.response(*this);
	}

template <class ReqApi,class RespApi,class CliPayload,class SrvPayload>
SrvRequest<ReqApi,SrvPayload>&
		CliPayloadAggregate<ReqApi,RespApi,CliPayload,SrvPayload>
		::getSrvMsg(){
	return _msg;
	}

template <class ReqApi,class RespApi,class CliPayload,class SrvPayload>
SrvPayload&		CliPayloadAggregate<ReqApi,RespApi,CliPayload,SrvPayload>
		::getPayload(){
	return _payload;
	}

template <class ReqApi,class RespApi,class CliPayload,class SrvPayload>
CliPayload&		CliPayloadAggregate<ReqApi,RespApi,CliPayload,SrvPayload>
	::getCliPayload() {
	return _payload;
	}

};
};
};

#endif
