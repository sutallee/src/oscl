/*
   Copyright (C) 2020 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_mt_itc_mbox_baseh_
#define _oscl_mt_itc_mbox_baseh_

#include "oscl/mt/runnable.h"
#include "oscl/mt/itc/mbox/srvapi.h"
#include "oscl/mt/itc/mbox/mutex.h"
#include "oscl/queue/queue.h"
#include "msg.h"

/** */
namespace Oscl {
/** */
namespace Mt {
/** */
namespace Itc {

/** This is a base class that encapsulates the fundamental
	behavior of a mailbox without any dependencies on
	the OS threading primitives.
 */
class Base :
	public Oscl::Mt::Itc::PostMsgApi,
	public Oscl::Mt::Sema::SignalApi
	{
	private:
		/** */
		Oscl::Mt::Sema::SignalApi&				_sigApi;

		/** */
		Oscl::Queue<Oscl::Mt::Itc::Msg>			_mboxQ;

		/** */
		Oscl::Mt::Itc::MailboxMutex				_mutex;

	public:
		/** A sigApi is required that is invoked from
			another thread when a message is placed into
			the mailbox. The implementation of this
			sigApi must wakeup the thread hosting
			mailbox and invoke processMessages().
		 */
		Base(
			Oscl::Mt::Sema::SignalApi&	sigApi
			) noexcept;

		/** */
		virtual ~Base();

		/** The default value of zero will cause all
			pending messages to be processed. Otherwise,
			only a maximum of nMessages will be processed.
		 */
		void	processMessages(unsigned nMessages = 0) noexcept;

	private: // Oscl::Mt::Itc::PostMsgApi
		/** */
		void	post(Oscl::Mt::Itc::Msg& msg) noexcept;

		/** */
		void	postSync(Oscl::Mt::Itc::Msg& msg) noexcept;

	private: // Oscl::Mt::Sema::SignalApi
		/** */
		void signal(void) noexcept;

		/** */
		void suSignal(void) noexcept;

	};

}
}
}

#endif
