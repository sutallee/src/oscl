/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_mt_itc_mbox_srvreqh_
#define _oscl_mt_itc_mbox_srvreqh_
#include "srvmsg.h"

/** */
namespace Oscl {
/** */
namespace Mt {
/** */
namespace Itc {

/** */
template <class ReqApi,class Payload>
class SrvRequest : public SrvMsg {
	private:
		/** */
		ReqApi&		_reqApi;
	public:
		/** */
		Payload&	_payload;
	public:
		/** */
		SrvRequest(	ReqApi&			reqApi,
					Payload&		payload,
					ReturnHandler&	returnHandler
					) noexcept;
		/** */
		void		process() noexcept;
		/** */
		Payload&	getPayload() const noexcept;
	};

template <class ReqApi,class Payload>
SrvRequest<ReqApi,Payload>
		::SrvRequest(	ReqApi&			reqApi,
						Payload&		payload,
						ReturnHandler&	returnHandler
						) noexcept:
	SrvMsg(returnHandler),
	_reqApi(reqApi),
	_payload(payload){
	}

template <class ReqApi,class Payload>
void		SrvRequest<ReqApi,Payload>::process() noexcept{
	_reqApi.request(*this);
	}

template <class ReqApi,class Payload>
Payload&		SrvRequest<ReqApi,Payload>::getPayload() const noexcept{
	return _payload;
	}

}
}
}

#endif
