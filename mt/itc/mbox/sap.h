/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_mt_itc_mbox_saph_
#define _oscl_mt_itc_mbox_saph_
#include "oscl/mt/itc/postmsgapi.h"

/** */
namespace Oscl {
/** */
namespace Mt {
/** */
namespace Itc {

/** */
template <class ReqApi>
class SAP : public PostMsgApi {
	public:
		/** */
		virtual ReqApi&			getReqApi() const noexcept=0;
		/** */
		virtual void			post(Msg& msg) noexcept=0;
		/** */
		virtual void			postSync(Msg& msg) noexcept=0;
	};

/** */
template <class ReqApi>
class ConcreteSAP : public SAP<ReqApi> {
	private:
		/** */
		ReqApi&				_api;
		/** */
		PostMsgApi&			_papi;
	public:
		/** */
		ConcreteSAP(ReqApi& api,PostMsgApi& papi) noexcept;
	public:
		/** */
		ReqApi&			getReqApi() const noexcept;
		/** */
		void			post(Msg& msg) noexcept;
		/** */
		void			postSync(Msg& msg) noexcept;
		/** */
		PostMsgApi&		getPostMsgApi() noexcept{return _papi;}
	};

template <class ReqApi>
ConcreteSAP<ReqApi>::ConcreteSAP(	ReqApi&		api,
									PostMsgApi&	papi
									) noexcept:
		_api(api),
		_papi(papi)
		{
	}

template <class ReqApi>
ReqApi& ConcreteSAP<ReqApi>::getReqApi() const noexcept {
	return _api;
	}

template <class ReqApi>
void ConcreteSAP<ReqApi>::post(Msg& msg) noexcept {
	_papi.post(msg);
	}

template <class ReqApi>
void ConcreteSAP<ReqApi>::postSync(Msg& msg) noexcept {
	_papi.postSync(msg);
	}

}
}
}

#endif
