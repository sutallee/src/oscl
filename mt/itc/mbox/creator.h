/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_mt_itc_creatorh_
#define _oscl_mt_itc_creatorh_
#include "server.h"

/** */
namespace Oscl {
/** */
namespace Mt {
/** */
namespace Itc {

/** This class can be used as the root creator for a
	multi-threaded application. There is only one such
	creator in each application, and the implementation
	is different for every application. Therefore, it is
	the responsibility of the application to supply an
	implementation of the initialize() operation.
	 When this creator is attached to a thread via its
	inherited Runnable operation it executes
	the initialize() operation.
 */
class Creator : public Oscl::Mt::Itc::Server {
	public:	// Oscl::Mt::Itc::Server
		/** The concrete implementation of this operation
			is responsible for allocating and initializing
			the application. For example:
			<pre><code>
			void	Oscl::MasterCreator::initialize() noexcept{
				// pass self so the application can use
				// the mailbox services of the creator.
				new MyApplicationCreator(*this);
				}
			</code></pre>
		 */
		void	initialize() noexcept;
	};

}
}
}

#endif
