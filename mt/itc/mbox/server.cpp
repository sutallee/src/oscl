/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "server.h"
#include "oscl/error/fatal.h"

using namespace Oscl;

Mt::Itc::Server::Server() noexcept:
		_signaledApi(0)
		{
	}

Mt::Itc::Server::~Server(){
	}

void Mt::Itc::Server::run() noexcept{
	initialize();
	while(true){
		Mt::Itc::Msg*	msg;
		msg = waitNext();
		if(msg){
			msg->process();
			}
		else {
			mboxSignaled();
			}
		}
	}

void Mt::Itc::Server::initialize() noexcept{
	}

void Mt::Itc::Server::mboxSignaled() noexcept{
	if(_signaledApi){
		_signaledApi->signaled();
		return;
		}
	// This should never happen unless the
	// sub-class overrides this operation.
	// Maybe I should suspend() this thread.
	ErrorFatal::logAndExit("Mt::Itc::Server::mboxSignaled\n");
	}

void	Mt::Itc::Server::setSignaledApi(SignaledApi* sapi) noexcept{
	if(sapi && _signaledApi){
		// To help ensure that only one SignaledApi is set at any
		// given time, the protocol is for the client to set the
		// signal handler, and then set it to zero when it is no
		// longer required. Thus, if there are (by programmer error)
		// two clients that attempt to set the SignaledApi together,
		// the error will be caught at run-time here.
		ErrorFatal::logAndExit("Mt::Itc::Server::setSignaledApi\n");
		}
	_signaledApi	= sapi;
	}

Oscl::Mt::Sema::SignalApi&		Mt::Itc::Server::getSignalApi() noexcept{
	return *this;
	}

Oscl::Mt::Itc::PostMsgApi&	Mt::Itc::Server::getPostMsgApi() noexcept{
	return *this;
	}

