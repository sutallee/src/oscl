/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "part.h"
#include "oscl/timer/counter.h"

using namespace Oscl::Mt::Itc::Delay;

Part::Part(
	Oscl::Mt::Itc::PostMsgApi&	papi,
	unsigned					minimumMillisecondsPerTick
	) noexcept:
		_sap(
			*this,
			papi
			),
		_minimumMillisecondsPerTick(minimumMillisecondsPerTick)
		{
	}

Oscl::Mt::Itc::Delay::Req::Api::SAP&	Part::getSAP() noexcept{
	return _sap;
	}

void	Part::attach(Oscl::Mt::Itc::Delay::Period& period) noexcept{
	_periodList.put(&period);
	}

void	Part::tick(unsigned millisecondsSinceLastTick) noexcept{

	periodTick();

	Oscl::Mt::Itc::Delay::Req::Api::Delay*
	next	= _pending.first();

	if(!next) return;

	returnExpired(millisecondsSinceLastTick);
	}

bool	Part::delayPending() noexcept{
	return _pending.first();
	}

void	Part::periodTick() noexcept {
	unsigned long long	currentTime;

	OsclTimerGetMillisecondCounter(currentTime);

	for(	Period*	next	= _periodList.first();
			next;
			next	= _periodList.next(next)
			) {
		if(currentTime >= next->_expirationTime) {
			next->tick();
			next->_expirationTime	+= next->_periodInMilliseconds;
			}
		}
	}

void	Part::returnExpired(unsigned millisecondsSinceLastTick) noexcept{
	while(true){
		Oscl::Mt::Itc::Delay::Req::Api::Delay*
		next	= _pending.first();

		if(next){
			unsigned long
			value	= next->_milliseconds;

			next->_milliseconds	-= millisecondsSinceLastTick;

			if(!(next->_milliseconds) || (next->_milliseconds > value)){
				Oscl::Mt::Itc::Delay::Req::Api::DelayReq*
				req	= (Oscl::Mt::Itc::Delay::Req::Api::DelayReq*)_pending.get()->_owner;
				req->returnToSender();
				continue;
				}
			}
		break;
		}
	}

void	Part::request(Oscl::Mt::Itc::Delay::Req::Api::DelayReq& msg) noexcept{
	Oscl::Mt::Itc::Delay::Req::Api::Delay&
	timer	= msg._payload._timer;

	Oscl::Mt::Itc::Delay::Req::Api::Delay*	next;

	if(!timer._milliseconds){
		msg.returnToSender();
		return;
		}
	timer._milliseconds	+= _minimumMillisecondsPerTick;
	timer._owner		= &msg;
	for(	next	=_pending.first();
			next;
			next	= _pending.next(next)
			){
		if(timer._milliseconds < next->_milliseconds){
			next->_milliseconds	-= timer._milliseconds;
			_pending.insertBefore(next,&timer);
			return;
			}
		else {
			timer._milliseconds	-= next->_milliseconds;
			}
		}
	_pending.put(&timer);
	}

void	Part::request(Oscl::Mt::Itc::Delay::Req::Api::CancelReq& msg) noexcept{
	Oscl::Mt::Itc::Delay::Req::Api::Delay*
	timer	= &msg._payload._timerToCancel._payload._timer;

	Oscl::Mt::Itc::Delay::Req::Api::Delay*	next;

	for(	next=_pending.first();
			next;
			next = _pending.next(next)
			){
		if(timer == next){
			Req::Api::Delay*	nt	= _pending.next(next);
			if(nt){
				nt->_milliseconds	+= next->_milliseconds;
				}
			_pending.remove(timer);
			DelayReq*	req	= (DelayReq*)next->_owner;
			req->returnToSender();
			}
		}

	msg.returnToSender();
	}

