/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "service.h"

using namespace Oscl::Mt::Itc::Delay;

Service::Service(unsigned minimumMillisecondsPerTick) noexcept:
		_minimumMillisecondsPerTick(minimumMillisecondsPerTick)
		{
	}

void	Service::tick(unsigned millisecondsSinceLastTick) noexcept{
	Req::Api::Delay*	next	= _pending.first();
	if(!next) return;
	returnExpired(millisecondsSinceLastTick);
	}

bool	Service::delayPending() noexcept{
	return _pending.first();
	}

void	Service::returnExpired(unsigned millisecondsSinceLastTick) noexcept{
	while(true){
		Req::Api::Delay*	next	= _pending.first();
		if(next){
			unsigned long	value	= next->_milliseconds;
			next->_milliseconds	-= millisecondsSinceLastTick;
			if(!(next->_milliseconds) || (next->_milliseconds > value)){
				DelayReq*	req	= (DelayReq*)_pending.get()->_owner;
				req->returnToSender();
				continue;
				}
			}
		break;
		}
	}

void	Service::request(DelayReq& msg) noexcept{
	Req::Api::Delay&	timer	= msg._payload._timer;
	Req::Api::Delay*	next;

	if(!timer._milliseconds){
		msg.returnToSender();
		return;
		}
	timer._milliseconds	+= _minimumMillisecondsPerTick;
	timer._owner		= &msg;
	for(	next	=_pending.first();
			next;
			next	= _pending.next(next)
			){
		if(timer._milliseconds < next->_milliseconds){
			next->_milliseconds	-= timer._milliseconds;
			_pending.insertBefore(next,&timer);
			return;
			}
		else {
			timer._milliseconds	-= next->_milliseconds;
			}
		}
	_pending.put(&timer);
	}

void	Service::request(CancelReq& msg) noexcept{
	Req::Api::Delay*	timer	= &msg._payload._timerToCancel._payload._timer;
	Req::Api::Delay*	next;
	for(	next=_pending.first();
			next;
			next = _pending.next(next)
			){
		if(timer == next){
			Req::Api::Delay*	nt	= _pending.next(next);
			if(nt){
				nt->_milliseconds	+= next->_milliseconds;
				}
			_pending.remove(timer);
			DelayReq*	req	= (DelayReq*)next->_owner;
			req->returnToSender();
			}
		}
	msg.returnToSender();
	}

