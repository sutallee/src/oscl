/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_mt_itc_delay_service_parth_
#define _oscl_mt_itc_delay_service_parth_
#include "oscl/mt/itc/delay/reqapi.h"
#include "oscl/queue/dqueue.h"
#include "oscl/queue/queue.h"
#include "oscl/tick/api.h"
#include "oscl/mt/itc/delay/period.h"

/** */
namespace Oscl {
/** */
namespace Mt {
/** */
namespace Itc {
/** */
namespace Delay {

/** */
class Part :
	public Oscl::Mt::Itc::Delay::Req::Api,
	public Oscl::Tick::Api
	{
	private:
		/** */
		Oscl::Mt::Itc::Delay::Req::Api::ConcreteSAP	_sap;
		
		/** */
		Oscl::DQueue<Req::Api::Delay>				_pending;

		/** */
		const unsigned	_minimumMillisecondsPerTick;

		/** */
		Oscl::Queue<Oscl::Mt::Itc::Delay::Period>	_periodList;

	public:
		/** */
		Part(
			Oscl::Mt::Itc::PostMsgApi&	papi,
			unsigned					minimumMillisecondsPerTick = 0
			) noexcept;

		/** */
		Oscl::Mt::Itc::Delay::Req::Api::SAP&	getSAP() noexcept;

		/** Returns true if any delay requests are pending. */
		bool	delayPending() noexcept;

		/** */
		void	attach(Oscl::Mt::Itc::Delay::Period& period) noexcept;

	public: // Oscl::Tick::Api
		/** MUST be invoked from the service's thread */
		void	tick(unsigned millisecondsSinceLastTick) noexcept;

	private:
		/** */
		void	periodTick() noexcept;

		/** */
		void	returnExpired(unsigned millisecondsSinceLastTick) noexcept;

	private:	// Oscl::Mt::Itc::Delay::Req::Api
		/** */
		void	request(Oscl::Mt::Itc::Delay::Req::Api::DelayReq& msg) noexcept;

		/** */
		void	request(Oscl::Mt::Itc::Delay::Req::Api::CancelReq& msg) noexcept;
	};

}
}
}
}

#endif
