/*
   Copyright (C) 2005 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <sys/time.h>
#include "part.h"
#include "oscl/timer/counter.h"
/*
    unsigned long OsclTimerGetMicrosecondCounter(void) noexcept;
    unsigned long OsclTimerGetMillisecondCounter(void) noexcept;
*/

using namespace Oscl::Mt::Itc::Delay;

Part::Part() noexcept:
		_sap(*this,*this),
		_startTime(0)
		{
	}


Oscl::Mt::Itc::Delay::Req::Api::SAP&	Part::getSAP() noexcept{
	return _sap;
	}

void	Part::tick(unsigned millisecondsSinceLastTick) noexcept{
	Req::Api::Delay*	next	= _pending.first();
	if(!next) return;
	returnExpired(millisecondsSinceLastTick);
	}

bool	Part::delayPending() noexcept{
	return _pending.first();
	}

void	Part::returnExpired(unsigned millisecondsSinceLastTick) noexcept{
	while(true){
		Req::Api::Delay*	next	= _pending.first();
		if(next){
			unsigned long	value	= next->_milliseconds;
			next->_milliseconds	-= millisecondsSinceLastTick;
			if(next->_milliseconds)
			if(!(next->_milliseconds) || (next->_milliseconds > value)){
				DelayReq*	req	= (DelayReq*)_pending.get()->_owner;
				req->returnToSender();
				continue;
				}
			}
		break;
		}
	}

static void	timevalToMs(	const struct timeval&	input,
							long long&				milliseconds
							) noexcept{
	milliseconds	= input.tv_sec;
	milliseconds	*= 1000;
	milliseconds	+= input.tv_usec / 1000;
	}

#if 0
static void	msToTimeval(	const long long			milliseconds,
							struct timeval&			output
							) noexcept{
	long		seconds		= milliseconds/1000;
	long long	x			= seconds * 1000;	// milliseconds of seconds
	x			= milliseconds - x;	// leave only milliseconds
	x			*= 1000;	// convert milliseconds to microseconds
	output.tv_sec	= (long)seconds;
	output.tv_usec	= (long)x;
	}

// Subtract the two timeval structures such that
// x - y == normalizedDiff
static void	diff(	const struct timeval&	x,
					const struct timeval&	y,
					struct timeval&			normalizedDiff
					) noexcept{
	signed long long	xusecs;
	signed long long	yusecs;

	// Convert x to a long long representation
	xusecs	= x.tv_sec;
	xusecs	*= 1000000;
	xusecs	+= x.tv_usec;

	// Convert y to a long long representation
	yusecs	= y.tv_sec;
	yusecs	*= 1000000;
	yusecs	+= y.tv_usec;

	// do the math
	xusecs	-= yusecs;

	// Convert diff back to normalized timeval
	normalizedDiff.tv_usec	= (xusecs%1000000);
	normalizedDiff.tv_sec	= (xusecs/1000000);
	}

// Add the two timeval structures such that
// x + y == normalizedSum
static void	sum(	const struct timeval&	x,
					const struct timeval&	y,
					struct timeval&			normalizedSum
					) noexcept{
	signed long long	xusecs;
	signed long long	yusecs;

	// Convert x to a long long representation
	xusecs	= x.tv_sec;
	xusecs	*= 1000000;
	xusecs	+= x.tv_usec;

	// Convert y to a long long representation
	yusecs	= y.tv_sec;
	yusecs	*= 1000000;
	yusecs	+= y.tv_usec;

	// do the math
	xusecs	+= yusecs;

	// Convert diff back to normalized timeval
	normalizedSum.tv_usec	= (xusecs%1000000);
	normalizedSum.tv_sec	= (xusecs/1000000);
	}
#endif

void	Part::initialize() noexcept{
	}

void	Part::mboxSignaled() noexcept{
	if(!_startTime){
		struct timeval	tv;
		gettimeofday(&tv, 0);
		timevalToMs(tv,_startTime);
		return;
		}
	struct timeval	nowtv;
	gettimeofday(&nowtv, 0);
	long long	now;
	timevalToMs(nowtv,now);
	unsigned long	diff	= (unsigned long)(now - _startTime);
	tick(diff);
	_startTime = now;
	}

void	Part::request(DelayReq& msg) noexcept{
	Req::Api::Delay&	timer	= msg._payload._timer;
	Req::Api::Delay*	next;
	if(!timer._milliseconds){
		msg.returnToSender();
		return;
		}
	timer._owner		= &msg;
	for(	next=_pending.first();
			next;
			next = _pending.next(next)
			){
		if(timer._milliseconds < next->_milliseconds){
			next->_milliseconds	-= timer._milliseconds;
			_pending.insertBefore(next,&timer);
			return;
			}
		else {
			timer._milliseconds	-= next->_milliseconds;
			}
		}
	_pending.put(&timer);
	}

void	Part::request(CancelReq& msg) noexcept{
	Req::Api::Delay*	timer	= &msg._payload._timerToCancel._payload._timer;
	Req::Api::Delay*	next;
	for(	next=_pending.first();
			next;
			next = _pending.next(next)
			){
		if(timer == next){
			Req::Api::Delay*	nt	= _pending.next(next);
			if(nt){
				nt->_milliseconds	+= next->_milliseconds;
				}
			_pending.remove(timer);
			DelayReq*	req	= (DelayReq*)next->_owner;
			req->returnToSender();
			}
		}
	msg.returnToSender();
	}

