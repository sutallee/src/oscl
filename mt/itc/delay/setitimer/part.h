/*
   Copyright (C) 2005 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_mt_itc_delay_setitimer_parth_
#define _oscl_mt_itc_delay_setitimer_parth_
#include "oscl/mt/itc/mbox/server.h"
#include "oscl/mt/itc/delay/reqapi.h"
#include "oscl/queue/dqueue.h"

/** */
namespace Oscl {
/** */
namespace Mt {
/** */
namespace Itc {
/** */
namespace Delay {

/**
	This class implements an asynchronous delay timer server
	in terms of the 4.2BSD and SVr4 conforming setitimer service.
 */
class Part :
		public Oscl::Mt::Itc::Server,
		public Req::Api
		{
	private:
		/**	This is a doubly linked list of pending delays ordered
			by remaining delay time.
		 */
		Oscl::DQueue<Req::Api::Delay>	_pending;

		/** */
		Oscl::Mt::Itc::Delay::Req::Api::ConcreteSAP	_sap;

		/** This value is a milli-second scaled timestamp when
			timer was started. This value is compared with the
			current time when the server is signaled, and the
			difference is used to determine which delays have
			expired, and the amount of time to use for the
			next delay if any.
		 */
		long long						_startTime;

	public:
		/** */
		Part() noexcept;

		/** */
		Oscl::Mt::Itc::Delay::Req::Api::SAP&	getSAP() noexcept;

		/** MUST be invoked from the service's thread */
		void	tick(unsigned millisecondsSinceLastTick) noexcept;

		/** Returns true if any delay requests are pending. */
		bool	delayPending() noexcept;

	private:
		/** */
		void	returnExpired(unsigned millisecondsSinceLastTick) noexcept;

	private:	// Oscl::Mt::Itc::Server
		/** This operation is invoked when the thread assigned to this
			delay sever first begins execution.
		 */
		void	initialize() noexcept;

		/** This operation executes when the mailbox signal operation
			is invoked from the process's SIGALARM signal handler.
		 */
		void	mboxSignaled() noexcept;

	private:	// Req::Api
		/** */
		void	request(DelayReq& msg) noexcept;
		/** */
		void	request(CancelReq& msg) noexcept;
	};

}
}
}
}

#endif
