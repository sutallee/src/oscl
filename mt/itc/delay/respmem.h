/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_mt_itc_delay_respmemh_
#define _oscl_mt_itc_delay_respmemh_
#include "respapi.h"
#include "oscl/memory/block.h"

/** */
namespace Oscl {
/** */
namespace Mt {
/** */
namespace Itc {
/** */
namespace Delay {
/** */
namespace Resp {

/** */
struct CancelMem {
	/** */
	Oscl::Memory::
	AlignedBlock<sizeof(Resp::Api::CancelResp)>		resp;
	/** */
	Oscl::Memory::
	AlignedBlock<sizeof(Req::Api::CancelPayload)>	payload;

	/**	This operation creates a response message using this
		 block of memory.
	 */
	Oscl::Mt::Itc::Delay::Resp::Api::CancelResp&
		build(	Oscl::Mt::Itc::Delay::Req::Api::SAP&	sap,
				Oscl::Mt::Itc::Delay::Resp::Api&		respApi,
				Oscl::Mt::Itc::PostMsgApi&		clientPapi,
				Oscl::Mt::Itc::Delay::Req::Api::DelayReq&	requestToCancel
				) noexcept;

	};

/** */
struct DelayMem {
	/** */
	Oscl::Memory::
	AlignedBlock<sizeof(Resp::Api::DelayResp)>		resp;
	/** */
	Oscl::Memory::
	AlignedBlock<sizeof(Req::Api::DelayPayload)>	payload;

	/**	This operation creates a response message using this
		 block of memory.
	 */
	Oscl::Mt::Itc::Delay::Resp::Api::DelayResp&
		build(	Oscl::Mt::Itc::Delay::Req::Api::SAP&	sap,
				Oscl::Mt::Itc::Delay::Resp::Api&		respApi,
				Oscl::Mt::Itc::PostMsgApi&				clientPapi,
				unsigned long							milliseconds
				) noexcept;

	};

}
}
}
}
}

#endif
