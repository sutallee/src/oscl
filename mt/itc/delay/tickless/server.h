/*
   Copyright (C) 2020 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_mt_itc_delay_tickless_serverh_
#define _oscl_mt_itc_delay_tickless_serverh_

#include "oscl/mt/itc/mbox/server.h"
#include "oscl/mt/itc/delay/reqapi.h"
#include "oscl/queue/dqueue.h"

/** */
namespace Oscl {
/** */
namespace Mt {
/** */
namespace Itc {
/** */
namespace Delay {
/** */
namespace Tickless {

/**	This class implements a tickless asynchronous
	delay server.
 */
class Server :
		public Oscl::Mt::Itc::Server,
		public Req::Api
		{
	public:
		/** This enables the server to use a
			platform-specific timer service.
		 */
		class ContextApi {
			public:
				/** Set the expiration semaphore interface.
					This must be called *before* the timer is
					started.
				 */
				virtual void	set(Oscl::Mt::Sema::SignalApi& expire) noexcept=0;

				/** Start the timer such that it will expire in
					the specified number of milliseconds.
				 */
				virtual void	start(long long milliseconds) noexcept=0;

				/** Restart the timer such that it will expire in
					the specified number of milliseconds.
				 */
				virtual void	restart(long long milliseconds) noexcept=0;

				/** Stop the timer.
				 */
				virtual void	stop() noexcept=0;

				/** Get the timer time remaining in millisecons.
					RETURN: 0 if the timer is not running.
				 */
				virtual long long	remainingTimeInMs() noexcept=0;
			};

	private:
		/** */
		ContextApi&		_timer;

	private:
		/**	This is a doubly linked list of pending delays ordered
			by remaining delay time.
		 */
		Oscl::DQueue<Req::Api::Delay>	_pending;

		/** */
		Oscl::Mt::Itc::Delay::Req::Api::ConcreteSAP	_sap;

	public:
		/** */
		Server(
			ContextApi&	timerContext
			) noexcept;

		/** */
		Oscl::Mt::Itc::Delay::Req::Api::SAP&	getSAP() noexcept;

	private:
		/** */
		void	returnExpired() noexcept;

	private:	// Oscl::Mt::Itc::Server
		/** This operation executes when the mailbox signal operation
			is invoked from the timer expiration.
		 */
		void	mboxSignaled() noexcept;

	private:	// Oscl::Mt::Itc::Delay::Req::Api
		/** */
		void	request(Oscl::Mt::Itc::Delay::Req::Api::DelayReq& msg) noexcept;

		/** */
		void	request(Oscl::Mt::Itc::Delay::Req::Api::CancelReq& msg) noexcept;
	};

}
}
}
}
}

#endif
