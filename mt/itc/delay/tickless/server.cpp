/*
   Copyright (C) 2020 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "server.h"

//#define DEBUG_TRACE

#ifdef DEBUG_TRACE
#include "oscl/error/info.h"
#endif

using namespace Oscl::Mt::Itc::Delay::Tickless;

Server::Server(
	ContextApi&		timer
	) noexcept:
		_timer(timer),
		_sap(
			*this,
			*this
			)
		{
	_timer.set(*this);
	}

Oscl::Mt::Itc::Delay::Req::Api::SAP&	Server::getSAP() noexcept{
	return _sap;
	}

void	Server::returnExpired() noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif

	// The first in the queue has definitely expired.
	DelayReq*	req	= (DelayReq*)_pending.get()->_owner;

	req->returnToSender();
	
	while(true){

		Req::Api::Delay*	next	= _pending.first();

		if(next){
			if(!next->_milliseconds) {
				DelayReq*	req	= (DelayReq*)_pending.get()->_owner;
				req->returnToSender();
				continue;
				}
			else {
				_timer.start(next->_milliseconds);
				return;
				}
			}

		break;
		}

	}

void	Server::mboxSignaled() noexcept{
	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif

	long long
	remainingMs	= _timer.remainingTimeInMs();

	if(remainingMs){
		// This can happen if the timer expires
		// while we stop or restart it.
		return;
		}

	returnExpired();
	}

void	Server::request(Oscl::Mt::Itc::Delay::Req::Api::DelayReq& msg) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif

	Req::Api::Delay&
	timer	= msg._payload._timer;

	if(!timer._milliseconds){
		#ifdef DEBUG_TRACE
		Oscl::Error::Info::log(
			"%s: (!timer._milliseconds)\n",
			OSCL_PRETTY_FUNCTION
			);
		#endif

		msg.returnToSender();
		return;
		}

	timer._owner		= &msg;

	Req::Api::Delay*
	next	= _pending.first();

	if(!next){
		_pending.put(&timer);
		_timer.start(timer._milliseconds);
		return;
		}

	long long	remainingMs	= _timer.remainingTimeInMs();

	if( timer._milliseconds < remainingMs ){

		if(timer._milliseconds < next->_milliseconds){
			next->_milliseconds	-= timer._milliseconds;
			remainingMs	= timer._milliseconds;
			_pending.insertBefore( next, &timer );
			}
		else {
			timer._milliseconds	-= next->_milliseconds;
			_pending.insertAfter( next, &timer );
			remainingMs	= next->_milliseconds;
			}

		_timer.restart( remainingMs );

		return;
		}

	timer._milliseconds	-= remainingMs;

	for(
		next = _pending.next(next);
		next;
		next = _pending.next(next)
		){
		if(timer._milliseconds < next->_milliseconds){

			next->_milliseconds	-= timer._milliseconds;

			_pending.insertBefore(next,&timer);

			return;
			}
		else {
			timer._milliseconds	-= next->_milliseconds;
			}
		}

	_pending.put(&timer);
	}

void	Server::request(Oscl::Mt::Itc::Delay::Req::Api::CancelReq& msg) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif

	long long
	remainingMs	= _timer.remainingTimeInMs();

	Req::Api::Delay*
	timer	= &msg._payload._timerToCancel._payload._timer;

	Req::Api::Delay*
	next	=_pending.first();

	if(!next){
		// The timer is not in the list.
		_timer.stop();
		msg.returnToSender();
		return;
		}

	if(timer == next){
		// First timer is special
		_pending.remove(timer);

		next	= _pending.next(next);

		if(next){
			next->_milliseconds	+= remainingMs;
			}
		else {
			_timer.stop();
			}

		DelayReq*
		req	= (DelayReq*)timer->_owner;

		req->returnToSender();

		msg.returnToSender();
		return;
		}

	for(
		next	=_pending.next(next);
		next;
		next	= _pending.next(next)
		){

		if(timer == next){
			Req::Api::Delay*
			nt	= _pending.next(next);

			if(nt){
				nt->_milliseconds	+= next->_milliseconds;
				}
			else {
				_timer.stop();
				}

			_pending.remove(timer);

			DelayReq*	req	= (DelayReq*)next->_owner;

			req->returnToSender();

			break;
			}
		}

	msg.returnToSender();
	}

