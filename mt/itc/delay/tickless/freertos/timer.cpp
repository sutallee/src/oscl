/*
   Copyright (C) 2024 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "timer.h"

using namespace Oscl::Mt::Itc::Delay::Tickless::FreeRTOS;

/*	typedef void (* TimerCallbackFunction_t)( TimerHandle_t xTimer );
 */
static void	timerExpiredCallback(TimerHandle_t timer) noexcept{
	Timer*
	t	= (Timer*)pvTimerGetTimerID(timer);
	t->timerExpired();
	}

Timer::Timer() noexcept:
		_sigApi(0)
		{
	_handle	= 
		xTimerCreateStatic(
			"oscl",					// const char* const		pcTimerName
			1,						// const TickType_t			xTimerPeriodInTicks
			false,					// const BaseType_t			xAutoReload
			this,					// void* const				pvTimerID
			timerExpiredCallback,	// TimerCallbackFunction_t	pxCallbackFunction
			&_timer					// StaticTimer_t*			pxTimerBuffer
			);
	}

void	Timer::timerExpired() noexcept{
	/*	The FreeRTOS callback is invoked
		from a thread.
	 */
	if(_sigApi){
		_sigApi->signal();
		}
	}

void	Timer::set(Oscl::Mt::Sema::SignalApi& expire) noexcept{
	_sigApi	= &expire;
	}

void	Timer::start(long long milliseconds) noexcept{
	/*
		This function will start a dormant/stopped timer.

		BaseType_t	xTimerChangePeriod (
			TimerHandle_t	xTimer,
			TickType_t		xNewPeriod,
			TickType_t		xTicksToWait
			);
	 */
	BaseType_t
	result	= xTimerChangePeriod (
				_handle,						// TimerHandle_t	xTimer,
				pdMS_TO_TICKS( milliseconds ),	// TickType_t	xNewPeriod,
				0								// TickType_t	xTicksToWait FIXME: use portMAX_DELAY?
				);

	if( result == pdFAIL ) {
		/*	This result is returned if the
			FreeRTOS timer command queue is full.
			The configTIMER_QUEUE_LENGTH specifies the
			number of entries in the queue. This should never
			happen.
		 */
		for(;;);	// FIXME: use Oscl::Error ?
		}
	}

void	Timer::restart(long long milliseconds) noexcept{

	TickType_t
	newPeriod	= pdMS_TO_TICKS( milliseconds );

	if( !newPeriod ) {
		newPeriod	= 1;
		}

	BaseType_t
	result	= xTimerChangePeriod (
				_handle,						// TimerHandle_t	xTimer
				newPeriod,	// TickType_t		xNewPeriod
				0								// TickType_t		xTicksToWait
				);

	if( result == pdFAIL ) {
		/*	This result is returned if the
			FreeRTOS timer command queue is full.
			The configTIMER_QUEUE_LENGTH specifies the
			number of entries in the queue. This should never
			happen.
		 */
		for(;;);	// FIXME: use Oscl::Error ?
		}
	}

void	Timer::stop() noexcept{

	BaseType_t
	result	= xTimerStop(
				_handle,	// TimerHandle_t	xTimer
				0			// TickType_t		xTicksToWait
				);

	if( result == pdFAIL ) {
		/*	This result is returned if the
			FreeRTOS timer command queue is full.
			The configTIMER_QUEUE_LENGTH specifies the
			number of entries in the queue. This should never
			happen.
		 */
		for(;;);	// FIXME: use Oscl::Error ?
		}
	}

long long	Timer::remainingTimeInMs() noexcept{
	/*	BaseType_t xTimerIsTimerActive( TimerHandle_t xTimer );
	 */
	BaseType_t
	result	= xTimerIsTimerActive( _handle );
	if( result == pdFALSE ) {
		// timer is dormant (not running)
		return 0;
		}

	/*	TickType_t xTimerGetExpiryTime( TimerHandle_t xTimer );
	 */
	TickType_t
	ticksRemaining	= xTimerGetExpiryTime(
						_handle
						);

	return pdTICKS_TO_MS( ticksRemaining );
	}

