/*
   Copyright (C) 2020 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "timer.h"

using namespace Oscl::Mt::Itc::Delay::Tickless::Zephyr;

		struct k_timer	_timer;
		Oscl::Mt::Sema::SignalApi*	_sigApi;

static void	kTimerExpired(struct k_timer* timer) noexcept{
	Timer*
	t	= (Timer*)k_timer_user_data_get(timer);
	t->timerExpired();
	}

Timer::Timer() noexcept:
		_sigApi(0)
		{
	k_timer_init(
		&_timer,
		kTimerExpired,	// start_fn
		0	// stop_fn
		);

	k_timer_user_data_set(
		&_timer,
		this
		);
	}

void	Timer::timerExpired() noexcept{
	if(_sigApi){
		_sigApi->suSignal();
		}
	}

void	Timer::set(Oscl::Mt::Sema::SignalApi& expire) noexcept{
	_sigApi	= &expire;
	}

void	Timer::start(long long milliseconds) noexcept{
	k_timeout_t	period;
	#ifdef CONFIG_LEGACY_TIMEOUT_API
	period	= 0;
	#else
	period.ticks	= 0;
	#endif
	k_timer_start(
		&_timer,
		K_MSEC(milliseconds),	// k_timeout_t duration
		period					// k_timeout_t period
		);
	}

void	Timer::restart(long long milliseconds) noexcept{
	k_timeout_t	period;
	#ifdef CONFIG_LEGACY_TIMEOUT_API
	period	= 0;
	#else
	period.ticks	= 0;
	#endif

	k_timer_start(
		&_timer,
		K_MSEC(milliseconds),	// k_timeout_t duration
		period					// k_timeout_t period
		);
	}

void	Timer::stop() noexcept{
	k_timer_stop(&_timer);
	}

long long	Timer::remainingTimeInMs() noexcept{
	return k_timer_remaining_get(&_timer);
	}

