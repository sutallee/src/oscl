/*
   Copyright (C) 2020 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_mt_itc_delay_tickless_zephyr_timerh_
#define _oscl_mt_itc_delay_tickless_zephyr_timerh_

#include <kernel.h>
#include "oscl/mt/itc/delay/tickless/server.h"

/** */
namespace Oscl {
/** */
namespace Mt {
/** */
namespace Itc {
/** */
namespace Delay {
/** */
namespace Tickless {
/** */
namespace Zephyr {

class Timer : public Oscl::Mt::Itc::Delay::Tickless::Server::ContextApi {
	private:
		/** */
		struct k_timer	_timer;

		/** */
		Oscl::Mt::Sema::SignalApi*	_sigApi;

	public:
		/** */
		Timer() noexcept;

		/** */
		void	timerExpired() noexcept;

	public:
		/** Set the expiration semaphore interface.
			This must be called *before* the timer is
			started.
		 */
		void	set(Oscl::Mt::Sema::SignalApi& expire) noexcept;

		/** Start the timer such that it will expire in
			the specified number of milliseconds.
		 */
		void	start(long long milliseconds) noexcept;

		/** Restart the timer such that it will expire in
			the specified number of milliseconds.
		 */
		void	restart(long long milliseconds) noexcept;

		/** Stop the timer.
		 */
		void	stop() noexcept;

		/** Get the timer time remaining in millisecons.
			RETURN: 0 if the timer is not running.
		 */
		long long	remainingTimeInMs() noexcept;
	};

}
}
}
}
}
}

#endif
