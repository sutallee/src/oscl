/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_mt_itc_atomic_atomich_
#define _oscl_mt_itc_atomic_atomich_
#include "api.h"
#include "oscl/mt/itc/mbox/srvreq.h"
#include "oscl/mt/itc/mbox/sap.h"

/** */
namespace Oscl {
/** */
namespace Mt {
/** */
namespace Itc {
/** */
namespace Atomic {
/** */
namespace Req {

/** */
template <class AccessApi>
class Api: public AccessApi {
	public:
		/** */
		typedef Oscl::Mt::Itc::SAP<Api>			SAP;
		/** */
		typedef Oscl::Mt::Itc::ConcreteSAP<Api>	ConcreteSAP;
		/** */
//		typedef OperationApi<AccessApi>			OperationApi;

	public:
		/** */
		class AtomicPayload {
		private:
			/** */
			OperationApi<AccessApi>&	_clientAtom;

		public:
			/** */
			AtomicPayload(OperationApi<AccessApi>& clientAtom) noexcept;
			/** */
			OperationApi<AccessApi>&	getClientAtom() noexcept;
		};

	public:
		/** */
		typedef SrvRequest<Api,AtomicPayload>	 AtomicReq;

	private:
		/** */
		friend class SrvRequest<Api,AtomicPayload>;	// a.k.a. AtomicReq;
		/** */
		void request(AtomicReq& msg) noexcept;
	};

template <class AccessApi>
Api<AccessApi>::AtomicPayload::AtomicPayload(OperationApi<AccessApi>& clientAtom) noexcept:
		_clientAtom(clientAtom)
		{
	}

template <class AccessApi>
OperationApi<AccessApi>&
Api<AccessApi>::AtomicPayload::getClientAtom() noexcept{
	return _clientAtom;
	}

template <class AccessApi>
void Api<AccessApi>::request(AtomicReq& msg) noexcept{
	msg.getPayload().getClientAtom().execute(*this);
	msg.returnToSender();
	}

}
}
}
}
}

#endif
