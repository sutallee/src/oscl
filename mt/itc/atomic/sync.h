/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_mt_itc_atomic_synch_
#define _oscl_mt_itc_atomic_synch_
#include "atomic.h"
#include "syncapi.h"
#include "oscl/mt/itc/mbox/syncrh.h"

/** */
namespace Oscl {
/** */
namespace Mt {
/** */
namespace Itc {
/** */
namespace Atomic {


/** */
template <class AccessApi>
class Sync: public SyncApi<AccessApi> {
	private:
		/** */
		typename Req::Api<AccessApi>::SAP&	_sap;

	public:
		/** */
		Sync(typename Req::Api<AccessApi>::SAP& sap) noexcept;

	public:
		/** */
		void executeAtomic( OperationApi<AccessApi>& clientAtom) noexcept;
	};


template <class AccessApi>
Sync<AccessApi>::Sync(typename Req::Api<AccessApi>::SAP& sap) noexcept:
		_sap(sap)
		{
	}

template <class AccessApi>
void Sync<AccessApi>::executeAtomic(	OperationApi<AccessApi>&	clientAtom
										) noexcept{
	typename Req::Api<AccessApi>::AtomicPayload	payload(clientAtom);
	SyncReturnHandler					srh;
	typename Req::Api<AccessApi>::AtomicReq		req(_sap.getReqApi(),payload,srh);
	_sap.postSync(req);
	}

}
}
}
}

#endif
