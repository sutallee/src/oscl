/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "close.h"
#include "oscl/mt/itc/mbox/syncrh.h"

using namespace Oscl;

Mt::Itc::Srv::CloseSync::CloseSync(	Mt::Itc::Srv::Close::Req::Api&	reqApi,
									Mt::Itc::PostMsgApi&			papi
									) noexcept:
		_openSync(reqApi,papi),
		_sap(reqApi,papi)
		{
	}

void	Mt::Itc::Srv::CloseSync::syncClose() noexcept{
	Mt::Itc::Srv::Close::Req::Api::ClosePayload	payload;
	Mt::Itc::SyncReturnHandler					srh;
	Mt::Itc::Srv::Close::Req::Api::CloseReq		msg(	_sap.getReqApi(),
														payload,
														srh
														);
	_sap.postSync(msg);
	}

void	Mt::Itc::Srv::CloseSync::syncOpen() noexcept{
	_openSync.syncOpen();
	}

Mt::Itc::Srv::Close::Req::Api::SAP&	Mt::Itc::Srv::CloseSync::getSAP() noexcept{
	return _sap;
	}

Mt::Itc::Srv::Open::Req::Api::SAP&
Mt::Itc::Srv::CloseSync::getOpenSAP() noexcept{
	return _openSync.getSAP();
	}

