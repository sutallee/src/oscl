/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_itc_server_closeapih_
#define _oscl_itc_server_closeapih_
#include "openapi.h"
#include "oscl/mt/itc/mbox/sap.h"

/** */
namespace Oscl {
/** */
namespace Mt {
/** */
namespace Itc {
/** */
namespace Srv {
/** */
namespace Close {
/** */
namespace Req {

/** */
class Api : public Open::Req::Api {
	public:
		/** */
		typedef Mt::Itc::SAP<Api>			SAP;

		/** */
		typedef Mt::Itc::ConcreteSAP<Api>	ConcreteSAP;
	public:
		/** */
		class ClosePayload { };
	public:
		/** */
		typedef Mt::Itc::SrvRequest<	Api,
										ClosePayload
										>	CloseReq;
	public:
		using Open::Req::Api::request;

		/** "Fix" warning */
		virtual void	request(OpenReq& msg) noexcept=0;

		/** */
		virtual void	request(CloseReq& msg) noexcept=0;
	};

}
}
}
}
}
}

#endif
