/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_mt_semah_
#define _oscl_mt_semah_
#include "oscl/mt/sema/sigapi.h"

/** */
namespace Oscl {
/** */
namespace Mt {
/** */
namespace Sema {

/** Represents an abstract semaphore.
 */
class Api : public SignalApi {
	public:
		/** Called by a thread to set the semaphore.
		 */
		virtual void signal(void) noexcept=0;

		/** Called by a thread to block and wait
			on the semaphore if/until it is set/signaled.
		 */
		virtual void wait(void) noexcept=0;

		/** Called by the supervisor-mode trap handlers
			and interrupt service routines to set the
			semphore. This should never be called by
			threads.
		 */
		virtual void suSignal(void)  noexcept= 0;

	};

}
}
}

#endif
