/*
   Copyright (C) 2007 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_mt_vxworks_threadh_
#define _oscl_mt_vxworks_threadh_
#include "oscl/mt/runnable.h"
#include "oscl/mt/thread.h"
#include "sema.h"
#include "oscl/strings/fixed.h"
#include <taskLib.h>

/** */
namespace Oscl {
/** */
namespace Mt {
/** */
namespace VxWorks {

/** This concrete class implements a task,
	which provides a concurrency context for a
	Mt::Runnable object, in terms of a VxWorks task.
	NOTE: This task must only be dynamically allocated
	since it performs dynamic resource allocation
	and requires kernel services to be operating.
 */

class Thread : public Mt::Thread {
	public:
		/** Runnable interface.
		 */
		Mt::Runnable&			_runnable;

	private:
		/** The VxWorks thread identifier.
		 */
		int						_thread;

		/** The thread synchronized message semaphore.
		 */
		Semaphore				_syncSema;

		/** Thread name.
		 */
		Strings::Fixed<32>		_name;

	private:
		/** This private constructor is used only by the
			Thread::initialize() operation to create the
			appropriate attributes for the "main" thread.
		 */
		Thread() noexcept;

	public: //Constructors
		/** The constructor requires: a runnable object which is started
			before the constructor completes, a short string describing this
			task instance; and a stack size that indicates the minimum size
			of the stack in bytes allocated for this task.
		 */
		Thread(	Mt::Runnable&	runnable,
				const char*		name,
				unsigned long	stackSizeInBytes,
				int				priority
				) noexcept;

		/** Virtual destructors rock!
		 */
		virtual ~Thread();

		/** Suspend this thread from the task level.
		 */
		void	suspend() noexcept;

		/** Suspend this thread from the interrupt level.
		 */
		void	suSuspend() noexcept;

		/** Resume this thread from the task level.
		 */
		void	resume() noexcept;

		/** Resume this thread from the interrupt level.
		 */
		void	suResume() noexcept;

		/** This operation returns the signal interface for the thread
			semaphore, which is used for synchronous message transactions.
			This is the sister operation to the thread's syncWait()
			operation.
		 */
		Mt::Sema::SignalApi&	getSyncSignalApi() noexcept;

		/** This operation blocks on the thread semaphore, waiting for
			a synchronous message operation to complete. This is the
			sister operation to the thread's getSyncSignalApi() operation.
		 */
		void		syncWait() noexcept;

		/** This operation returns the name (null terminated string)
			of the current thread.
		 */
		virtual const char*	getName() noexcept;

		/** This operation MUST be called from the creational
			root process/thread before any instances of this
			class are instantiated.
		 */
		static void initialize() noexcept;
	};

}
}
}

#endif
