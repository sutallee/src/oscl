/*
   Copyright (C) 2007 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "thread.h"
#include "oscl/error/fatal.h"
#include "oscl/error/info.h"
#include "oscl/memory/block.h"
#include <string.h>
#include <new>
#include <taskVarLib.h>

using namespace Oscl::Mt::VxWorks;

static volatile bool	initialized=false;

/** This is a shared vxworks Task Variable. Its value is changed
	each time a context switch happens.
 */
static Oscl::Mt::Thread*	currentThread;

static int entryPoint(Thread*	context){
	Oscl::Mt::VxWorks::Thread*	thread	= context;
	currentThread	= thread;
	taskVarAdd(taskIdSelf(),(int*)&currentThread);
	thread->_runnable.run();
	return 0;
	}

static void	buildThread(	Thread*				context,
							size_t				stackSize,
							int					priority,
							int&				thread,
							const char*			name
							) noexcept
	{
	thread	= taskSpawn(	(char*)name,
							priority,
							0,	// options
							stackSize,
							(FUNCPTR)(entryPoint),
							(int)	context,	// arg 1
							0,	//		arg2,
							0,	//		arg3,
							0,	//		arg4,
							0,	//		arg5,
							0,	//		arg6,
							0,	//		arg7,
							0,	//		arg8,
							0,	//		arg9,
							0	//		arg10
							);
	if(thread == ERROR) for(;;);
	}

Thread::Thread(	Oscl::Mt::Runnable&	runnable,
				const char*			name,
				unsigned long		stackSizeInBytes,
				int					priority
				) noexcept:
		_runnable(runnable),
		_thread(),
		_syncSema(),
		_name(name)
		{
	buildThread(	this,
					stackSizeInBytes,
					priority,
					_thread,
					name
					);
	}

Thread::Thread() noexcept:
		_runnable(*(Oscl::Mt::Runnable*)0),
		_thread(taskIdSelf()),
		_syncSema(),
		_name("Root")
		{
	Oscl::Mt::Thread*			thisThread	= this;
	currentThread	= thisThread;
	taskVarAdd(taskIdSelf(),(int*)&currentThread);
	}

Thread::~Thread(){
	Oscl::ErrorFatal::logAndExit("Thread::~Thread() cannot cancel thread.\n");
	}

void	Thread::suspend() noexcept{
	taskSuspend(taskIdSelf());
	}

void	Thread::suSuspend() noexcept{
	Oscl::ErrorFatal::logAndExit("Thread::suSuspend() not implemented.\n");
	}

void	Thread::resume() noexcept{
	taskResume(taskIdSelf());
	}

void	Thread::suResume() noexcept{
	Oscl::ErrorFatal::logAndExit("Thread::suResume() not implemented.\n");
	}

Oscl::Mt::Sema::SignalApi&	Thread::getSyncSignalApi() noexcept{
	return _syncSema;
	}

void		Thread::syncWait() noexcept{
	_syncSema.wait();
	}

const char* Thread::getName() noexcept{
	return _name.getString();
	}

Oscl::Mt::Thread::Thread() noexcept{
	if(!initialized){
		Oscl::ErrorFatal::logAndExit(	"Oscl::Mt::VxWorks::Thread::Thread() cannot create key.\n"
										"Oscl::Mt::VxWorks::Thread::initialize() must be invoked\n"
										"any of these threads are created.\n"
										);
		}
	}

Oscl::Mt::Thread::~Thread(){
	}

Oscl::Mt::Thread&	Oscl::Mt::Thread::getCurrent() noexcept {
	return *currentThread;
	}

void	Oscl::Mt::Thread::sleep(unsigned long tocks) noexcept{
	// FIXME: is tocks correct?
	// Generally sleep users assume tocks in milliseconds
	// VxWorks says the taskDelay argument is in "ticks".
	taskDelay(tocks);
	}

void Thread::initialize() noexcept{
	if(initialized){
		return;
		}
	static Oscl::Memory::AlignedBlock<sizeof(Oscl::Mt::VxWorks::Thread)>	_rootThreadMem;
	static Oscl::Mt::VxWorks::Thread*						rootThread;
	if(!rootThread) {
		rootThread	= new (&_rootThreadMem) Oscl::Mt::VxWorks::Thread();
		}
	initialized=true;
	}

