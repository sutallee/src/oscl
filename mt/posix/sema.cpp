/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _REENTRANT
#define _REENTRANT
#endif

#include <errno.h>
#include "sema.h"
#include "oscl/error/fatal.h"

using namespace Oscl::Mt::Posix;

Semaphore::Semaphore() noexcept{
	sem_init(&_sema,0,0);
	}

Semaphore::~Semaphore(){
	sem_destroy(&_sema);
	}

void Semaphore::signal(void) noexcept{
	sem_post(&_sema);
	}

void Semaphore::wait(void) noexcept{
	// We have chosen to implement the Posix
	// semaphore using the sem_t variants instead
	// of a pthread_mutex_t. The reason for this
	// is that the sem_t style semaphore is the
	// *only* ASYNC SAFE mechanism. In other words,
	// sem_post can be called from within a signal
	// handler.

	// The problem with sem_wait, however, is that
	// it depends upon errno, which is not necessarily
	// thread-safe. Therefore, it is very important
	// that the reentrant version of errno is used.
	// This is a LIBC issue and thus your chosen
	// LIBC must support reentrant errno.
	//
	// There are several ways to ensure that the
	// errno is reentrant.
	//
	// o Compile the file with the -pthread flag
	// o Include <pthread.h> at the top of the source file.
	// o Define _REENTRANT before including the <errno.h> file.
	//
	// In this case, I am explicitly defining _REENTRANT
	// before including <errno.h> because I can't or don't
	// want to rely on the other mechanisms.

	while(true){
		int err = sem_wait(&_sema);
		if(!err || (errno!=EINTR)){
			break;
			}
		}
	}

void Semaphore::suSignal(void) noexcept{
	// sem_post is signal-handler/async safe.
	sem_post(&_sema);
	}

