/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <new>
#include <stdio.h>
#include <signal.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdint.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/wait.h>
#include "sighandler.h"
#include "tick.h"
#include "oscl/timer/counter.h"

/** */
static Oscl::Mt::Sema::SignalApi*	sigHupSema;

/** */
static Oscl::Mt::Sema::SignalApi*	sigAlarmSema;

//#define LOOP_ON_DEATH

//////////////////////////////////////////////////////
static void sigHupHandler(int sig) {
#ifdef SCC_EXIT_ON_SIGHUP
	exit(0);
#else
	sigHupSema->suSignal();
#endif
	}

// This is the low order word of the long-long millisecond counter
// which is incremented by the periodic SIGALARM signal handler,
// and used by periodic services for rate-monotonic real-time
// calculations. This counter should only be read through
// the OsclTimerGetMillisecondCounter function.
static volatile unsigned long	millisecondCounterLow;

// This is the high order word of the long-long millisecond counter
// which is incremented by the periodic SIGALARM signal handler,
// and used by periodic services for rate-monotonic real-time
// calculations. This counter should only be read through
// the OsclTimerGetMillisecondCounter function.
static volatile unsigned long	millisecondCounterHigh;

///////////////////////////////////////////////////////////////////////////////
//
//	OsclTimerGetMillisecondCounter
//                                                                           
//	This function is an implementation of the OsclTimerGetMillisecondCounter
//	interface that allows the applciation to get the number of
//	"milliseconds-since-initialization" timestamp. Note that the actual
//	resolution is dependent upon the SIGALARM period. Also note that this
//	is not intended to be a low jitter implementation.
//	By using this long long millisecond timestamp, the application need not
//	be concerned with overflow issues since the process will need to run for
//	about 585 million years before an overflow happens. That should be
//	plenty of time to allow an asteroid to reset the process ;-)
//	
///////////////////////////////////////////////////////////////////////////////
void OsclTimerGetMillisecondCounter(unsigned long long& result) noexcept {
	uint32_t		high;
	uint32_t		low;
	uint32_t		n	= millisecondCounterHigh;

	// This loop accounts for the occasion when the signal handler
	// interrupts during the overflow from the high word to the
	// low word.
	n	= millisecondCounterHigh;
	do {
		high	= n;
		low		= millisecondCounterLow;
		n		= millisecondCounterHigh;
		} while(n != high);

	result	= high;
	result	<<= (sizeof(low)*8);
	result	+= low;
	}

unsigned long OsclTimerGetTimerResolutionInMilliseconds() noexcept{
	return Oscl::Mt::Posix::App::millisecondsPerTick;
	}

///////////////////////////////////////////////////////////////////////////////
//
//	sigAlarmHandler
//                                                                           
//	This function is called when this process receives a SIGALARM signal.
//	The alarm signal is used to drive the high resolution delay services.
//	
///////////////////////////////////////////////////////////////////////////////
static void sigAlarmHandler(int sig)
{
	// Increment the millisecond counter.
	unsigned long	prev	= millisecondCounterLow;
	millisecondCounterLow	+= Oscl::Mt::Posix::App::millisecondsPerTick;

	// Deal low word overflow in the millisecond counter.
	if(millisecondCounterLow < prev) {
		millisecondCounterHigh += 1;
		}

#ifdef GPROF_TIME_IN_MILLISECONDS
	if(millisecondCounterLow > GPROF_TIME_IN_MILLISECONDS){
		exit(0);
		}
#endif

	// Signal the delay server as a tick.
	sigAlarmSema->suSignal();
}

#if !defined(LOOP_ON_DEATH) && defined(SCC_DONT_CORE_DUMP)
///////////////////////////////////////////////////////////////////////////////
//
//	sigSegmentationViolationHandler
//                                                                           
//	This function is called when this process receives a SIGSEGV signal.
//	The SIGSEGV signal is generated when data cannot be delivered to a
//	socket. We ignore this signal for now, and rely on result codes returned
//	from the read and write system calls instead.
//	
///////////////////////////////////////////////////////////////////////////////
static void sigSegmentationViolationHandler(int sig)
{
	printf("%s",__PRETTY_FUNCTION__);
	exit(1);
}
#endif

#ifdef LOOP_ON_DEATH
///////////////////////////////////////////////////////////////////////////////
//
//	sigDeathHandler
//
///////////////////////////////////////////////////////////////////////////////
static void sigDeath(int sig)
{
	extern volatile bool			echoDuration;
	printf("Caught Death %d! Debug Me!\n",sig);
	echoDuration	= false;
	for(;;);
}
#endif

/** */
namespace Oscl {

/** */
namespace Mt {

/** */
namespace Posix {

/** */
namespace App {

/** */
namespace SignalHandler {

///////////////////////////////////////////////////////////////////////////////

void	setup(
			Oscl::Mt::Sema::SignalApi&	sigHupSemaphore,
			Oscl::Mt::Sema::SignalApi&	sigAlarmSemaphore
			) noexcept {
	
	sigHupSema		= &sigHupSemaphore;
	sigAlarmSema	= &sigAlarmSemaphore;

	// This section causes SIGPIPE to be ignored.
	sigset_t	signalMask;
	sigemptyset(&signalMask);
	sigaddset(&signalMask,SIGPIPE);
	pthread_sigmask(SIG_BLOCK,&signalMask,0);

	// This section sets up the SIGHUP signal handler.
    struct sigaction    sigHupAction;
    sigHupAction.sa_handler  = sigHupHandler;
    sigemptyset(&sigHupAction.sa_mask);
    sigHupAction.sa_flags    = 0;
    sigaction(SIGHUP,&sigHupAction,0);

	// This section sets up the SIGALARM signal handler.
    struct sigaction    sigAlarmAction;
    sigAlarmAction.sa_handler  = sigAlarmHandler;
    sigemptyset(&sigAlarmAction.sa_mask);
    sigAlarmAction.sa_flags    = 0;
    sigaction(SIGALRM,&sigAlarmAction,0);

#ifdef SCC_DONT_CORE_DUMP
	// This section sets up the SIGSEGV signal handler.
    struct sigaction    sigSegVAction;
#ifndef LOOP_ON_DEATH
    sigSegVAction.sa_handler  = sigSegmentationViolationHandler;
#else
    sigSegVAction.sa_handler  = sigDeath;
#endif
    sigemptyset(&sigSegVAction.sa_mask);
    sigSegVAction.sa_flags    = 0;
    sigaction(SIGSEGV,&sigSegVAction,0);
#endif

#ifdef LOOP_ON_DEATH
	// This section sets up the SIGABRT signal handler.
    struct sigaction    sigAbortAction;
    sigAbortAction.sa_handler  = sigDeath;
    sigemptyset(&sigAbortAction.sa_mask);
    sigAbortAction.sa_flags    = 0;
    sigaction(SIGABRT,&sigAbortAction,0);
#endif

	// Now we start the SIGALRM service, which is
	// responsible for the 100 millisecond tick used by
	// the asynchronous delay sub-system.
	itimerval	timer;
	timer.it_value.tv_sec		= 0;
	timer.it_value.tv_usec		= Oscl::Mt::Posix::App::millisecondsPerTick*1000;

	timer.it_interval.tv_sec	= 0;
	timer.it_interval.tv_usec	= Oscl::Mt::Posix::App::millisecondsPerTick*1000;
	int	err	= setitimer(
				ITIMER_REAL,
				&timer,
				0
				);

	if(err) {
		perror("setitimer");
		exit(1);
		}
	}

}
}
}
}
}

