/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "thread.h"
#include "oscl/error/fatal.h"
#include "oscl/error/info.h"
#include <pthread.h>
#include <string.h>
#include <unistd.h>
#include <bits/local_lim.h>

using namespace Oscl::Mt::Posix;

static void* entryPoint(void* runnable);

static volatile bool	keyCreated=false;
static pthread_key_t	key;

static void	buildThread(	void*				context,
							size_t				stackSize,
							pthread_t&			thread,
							int					priority
							) noexcept
	{
	pthread_attr_t	attributes;

	int
	err	= pthread_attr_init(&attributes);
	if(err){
		Oscl::ErrorFatal::logAndExit(	"Thread::Thread():"
										"pthread_attr_init failed.\n"
										);
		}
	
	err	= pthread_attr_setdetachstate(&attributes,PTHREAD_CREATE_DETACHED);
	if(err){
		Oscl::ErrorFatal::logAndExit(	"Thread::Thread():"
										"pthread_attr_setdetachstate failed.\n"
										);
		}

	err	= pthread_attr_setschedpolicy(&attributes,SCHED_FIFO);
	if(err){
		Oscl::ErrorFatal::logAndExit(	"Thread::Thread():"
										"pthread_attr_setschedpolicy failed.\n"
										);
		}

	struct sched_param   scheduleParameter;

	memset(&scheduleParameter,0,sizeof(scheduleParameter));

	if(priority < 0){
		scheduleParameter.sched_priority	=	(		sched_get_priority_max(SCHED_FIFO)
													-	sched_get_priority_min(SCHED_FIFO)
													) / 2;
		}
	else {
		int	minPriority	= sched_get_priority_min(SCHED_FIFO);
		int	maxPriority	= sched_get_priority_max(SCHED_FIFO);

		int
		thePriority	= priority + minPriority;

		thePriority	= thePriority < maxPriority ? thePriority:maxPriority;
		thePriority	= thePriority > minPriority ? thePriority:minPriority;

		scheduleParameter.sched_priority	=	thePriority;
		}


	err	= pthread_attr_setschedparam(&attributes,&scheduleParameter);
	if(err){
		Oscl::ErrorFatal::logAndExit(	"Thread::Thread():"
										"pthread_attr_setschedparam failed.\n"
										);
		}

	if( stackSize < static_cast<size_t>(PTHREAD_STACK_MIN) ){
		stackSize	= PTHREAD_STACK_MIN;
		}

	err	= pthread_attr_setstacksize(&attributes,stackSize);
	if(err){
		Oscl::ErrorFatal::logAndExit(	"Thread::Thread():"
										"pthread_attr_setstacksize failed.\n"
										);
		}

	err	= pthread_create(	&thread,
							&attributes,
							&entryPoint,
							context
							);
	if(err){
		Oscl::ErrorFatal::logAndExit("Thread::Thread() cannot create thread.\n");
		}
	}

Thread::Thread(	Oscl::Mt::Runnable&	runnable,
				const char*			name,
				unsigned long		stackSizeInBytes,
				int					priority
				) noexcept:
		_runnable(runnable),
		_thread(),
		_syncSema(),
		_name(name)
		{
	buildThread(	this,
					stackSizeInBytes,
					_thread,
					priority
					);
	}

Thread::Thread(	Oscl::Mt::Runnable&	runnable,
				const char*			name,
				int					priority
				) noexcept:
		_runnable(runnable),
		_thread(),
		_syncSema(),
		_name(name)
		{
	buildThread(	this,
					0,
					_thread,
					priority
					);
	}

Thread::Thread() noexcept:
		_runnable(*(Oscl::Mt::Runnable*)0),
		_thread(pthread_self()),
		_syncSema(),
		_name("Root")
		{
	Oscl::Mt::Thread*			thisThread	= this;
	int err	= pthread_setspecific(key,thisThread);
	if(err){
		Oscl::ErrorFatal::logAndExit("entryPoint() cannot set key.\n");
		}
	}

Thread::~Thread(){
	int err	= pthread_cancel(_thread);
	if(err){
		Oscl::ErrorFatal::logAndExit("Thread::~Thread() cannot cancel thread.\n");
		}
	}

void	Thread::suspend() noexcept{
	Oscl::ErrorFatal::logAndExit("Thread::suspend() not implemented.\n");
	}

void	Thread::suSuspend() noexcept{
	Oscl::ErrorFatal::logAndExit("Thread::suSuspend() not implemented.\n");
	}

void	Thread::resume() noexcept{
	Oscl::ErrorFatal::logAndExit("Thread::resume() not implemented.\n");
	}

void	Thread::suResume() noexcept{
	Oscl::ErrorFatal::logAndExit("Thread::suResume() not implemented.\n");
	}

Oscl::Mt::Sema::SignalApi&	Thread::getSyncSignalApi() noexcept{
	return _syncSema;
	}

void		Thread::syncWait() noexcept{
	_syncSema.wait();
	}

const char* Thread::getName() noexcept{
	return _name.getString();
	}

static void* entryPoint(void* vthread){
	Oscl::Mt::Posix::Thread*	thread	= (Thread*)vthread;
	Oscl::Mt::Thread*			thisThread	= thread;
	int err	= pthread_setspecific(key,thisThread);
	if(err){
		Oscl::ErrorFatal::logAndExit("entryPoint() cannot set key.\n");
		}
	thread->_runnable.run();
	return 0;
	}

Oscl::Mt::Thread::Thread() noexcept{
	if(!keyCreated){
		Oscl::ErrorFatal::logAndExit(	"Oscl::Mt::Posix::Thread::Thread() cannot create key.\n"
										"Oscl::Mt::Posix::Thread::initialize() must be invoked\n"
										"any of these threads are created.\n"
										);
		}
	}

Oscl::Mt::Thread::~Thread(){
	}

Oscl::Mt::Thread&	Oscl::Mt::Thread::getCurrent() noexcept {
	if(!keyCreated){
		Oscl::ErrorFatal::logAndExit("Oscl::Mt::Thread::getCurrent(): call Thread::initialize() first.\n");
		}
	Oscl::Mt::Thread*	thread	= (Oscl::Mt::Thread*)pthread_getspecific(key);
	return *thread;
	}

void	Oscl::Mt::Thread::sleep(unsigned long tocks) noexcept{
	// Convert milliseconds to the nanosleep time spec
	const long		nsec2msec	= 1000000;
	const long		msec2sec	= 1000;
	time_t				sec			= tocks / msec2sec;
	long				nsec		= (tocks % msec2sec)*nsec2msec;
	timespec			delay		= {sec,nsec};

	while(nanosleep(&delay,&delay));
	}

void Thread::initialize() noexcept{
	int	err;
	if(keyCreated){
		return;
		}
	err	= pthread_key_create(&key,(void(*)(void*))0);
	if(err){
		Oscl::ErrorFatal::logAndExit("Oscl::Mt::Posix::Thread::Thread() cannot create key.\n");
		}
	else{
		keyCreated=true;
		}
	static Oscl::Mt::Posix::Thread*	rootThread;
	if(!rootThread) {
		rootThread	= new Oscl::Mt::Posix::Thread();
		}
	}

