/*
   Copyright (C) 2020 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "sema.h"
#include "oscl/error/fatal.h"

using namespace Oscl::Mt::Zephyr;

Semaphore::Semaphore() noexcept{
	/*
		k_sem_init(&my_sem, 0, 1);
	 */

	k_sem_init(
		&_sema,
		0,	// initial count
		1	// binary semaphore
		);
	}

Semaphore::~Semaphore(){
	}

void Semaphore::signal(void) noexcept{
	/*
		void k_sem_give(struct k_sem *sem);
	 */
	k_sem_give(&_sema);
	}

void Semaphore::wait(void) noexcept{
	/*
		int k_sem_take(struct k_sem *sem, k_timeout_t timeout);
	 */
	k_sem_take(
		&_sema,
		K_FOREVER
		);
	}

void Semaphore::suSignal(void) noexcept{
	/*
		void k_sem_give(struct k_sem *sem);
	 */
	k_sem_give(&_sema);
	}

