/*
   Copyright (C) 2007 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_mt_zephyr_threadh_
#define _oscl_mt_zephyr_threadh_

#include "oscl/mt/runnable.h"
#include "oscl/mt/thread.h"
#include "sema.h"
#include <kernel.h>

/** */
namespace Oscl {
/** */
namespace Mt {
/** */
namespace Zephyr {

/** This concrete class implements a task/thread,
	which provides a concurrency context for a
	Mt::Runnable object, in terms of a Zephyr task.
 */
class Thread : public Mt::Thread {
	public:
		/** Runnable interface.
		 */
		Oscl::Mt::Runnable&		_runnable;

	private:
		/** The Zephyr statically allocated TCB.
			The Zephyr TaskHandle_t is simply the
			address of this structure. This fact was
			determined empirically.
		 */
		struct k_thread			_tcb;

		/** In normal threads, this is just a pointer
			to the _tcb. In the main system thread,
			this is a pointer to the system threads's
			tcb and the _tcb is not used.
		 */
		k_tid_t					_tid;

		/** The thread synchronized message semaphore.
		 */
		Semaphore				_syncSema;

	public: //Constructors
		/** The constructor requires: a runnable object which is started
			before the constructor completes, a short string describing this
			task instance; a pointer to the memory used as a stack;
			and a stack size that indicates the size
			of the stack in bytes.

			IMPORTANT! The stack memory must be aligned to that
			required by a pointer.
		 */
		Thread(
			Oscl::Mt::Runnable&	runnable,
			const char*			name,
			k_thread_stack_t*	stack,
			unsigned long		stackSizeInBytes,
			int					priority,
			uint32_t			options
			) noexcept;

		/**	This special constructor associates this
			Thread object with the currently running
			thread. This is used to convert the main
			system thread into an Oscl::Mt::Thread.
		 */
		Thread() noexcept;

		/** Virtual destructors were once required.
		 */
		virtual ~Thread();

		/** Suspend this thread from the task level.
		 */
		void	suspend() noexcept;

		/** Suspend this thread from the interrupt level.
		 */
		void	suSuspend() noexcept;

		/** Resume this thread from the task level.
		 */
		void	resume() noexcept;

		/** Resume this thread from the interrupt level.
		 */
		void	suResume() noexcept;

		/** This operation returns the signal interface for the thread
			semaphore, which is used for synchronous message transactions.
			This is the sister operation to the thread's syncWait()
			operation.
		 */
		Oscl::Mt::Sema::SignalApi&	getSyncSignalApi() noexcept;

		/** This operation blocks on the thread semaphore, waiting for
			a synchronous message operation to complete. This is the
			sister operation to the thread's getSyncSignalApi() operation.
		 */
		void		syncWait() noexcept;

		/** This operation returns the name (null terminated string)
			of the current thread.
		 */
		virtual const char*	getName() noexcept;
	};

}
}
}

#endif
