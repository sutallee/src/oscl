/*
   Copyright (C) 2020 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "thread.h"
#include "oscl/error/fatal.h"
#include "oscl/error/info.h"
#include "oscl/memory/block.h"
#include <string.h>
#include <new>

using namespace Oscl::Mt::Zephyr;

//static volatile bool	initialized=false;

static void entryPoint(
				void*	p1,
				void*	p2,
				void*	p3
				){
	Oscl::Mt::Zephyr::Thread*	thread	= (Oscl::Mt::Zephyr::Thread*)p1;

	k_thread_custom_data_set(thread);

	thread->_runnable.run();
	}

static void	buildThread(
				Thread&				context,
				struct k_thread&	tcb,
				k_thread_stack_t*	stack,
				size_t				stackSize,
				int					priority,
				k_tid_t&			thread,
				const char*			name,
				uint32_t			options
				) noexcept
	{
	/*
		k_tid_t	k_thread_create(
					struct k_thread*	new_thread,
					k_thread_stack_t*	stack,
					size_t				stack_size,
					k_thread_entry_t	entry,
					void*				p1,
					void*				p2,
					void*				p3,
					int					prio,
					u32_t				options,
					k_timeout_t			delay
					);
	*/
	thread	= k_thread_create(
				&tcb,
				stack,
				stackSize,
				entryPoint,
				&context, // p1
				0,
				0,
				priority,
				options,
				K_NO_WAIT
				);
	}

Thread::Thread(
	Oscl::Mt::Runnable&	runnable,
	const char*			name,
	k_thread_stack_t*	stack,
	unsigned long		stackSizeInBytes,
	int					priority,
	uint32_t			options
	) noexcept:
		_runnable(runnable),
		_tid(&_tcb),
		_syncSema()
		{

	buildThread(
		*this,
		_tcb,
		stack,
		stackSizeInBytes,
		priority,
		_tid,
		name,
		options
		);
	}

Thread::Thread() noexcept:
		_runnable(*(Oscl::Mt::Runnable*)0),
		_tid(k_current_get()),
		_syncSema()
		{
	Oscl::Mt::Zephyr::Thread*	thread	= this;

	k_thread_custom_data_set(thread);
	}

Thread::~Thread(){
	Oscl::ErrorFatal::logAndExit("Thread::~Thread() cannot cancel thread.\n");
	}

void	Thread::suspend() noexcept{
	/*
		void k_thread_suspend(k_tid_t thread);
	*/
	k_thread_suspend(_tid);
	}

void	Thread::suSuspend() noexcept{
	Oscl::ErrorFatal::logAndExit("Thread::suSuspend() not implemented.\n");
	}

void	Thread::resume() noexcept{
	/*
		void k_thread_resume(k_tid_t thread);
	 */
	k_thread_resume(_tid);
	}

void	Thread::suResume() noexcept{
	Oscl::ErrorFatal::logAndExit("Thread::suResume() not implemented.\n");
	}

Oscl::Mt::Sema::SignalApi&	Thread::getSyncSignalApi() noexcept{
	return _syncSema;
	}

void		Thread::syncWait() noexcept{
	_syncSema.wait();
	}

const char* Thread::getName() noexcept{
	/*
		const char*	k_thread_name_get(k_tid_t thread_id);
	*/
	return k_thread_name_get(_tid);
	}

Oscl::Mt::Thread::Thread() noexcept{
#if 0
	if(!initialized){
		Oscl::ErrorFatal::logAndExit(
			"Oscl::Mt::Zephyr::Thread::Thread() cannot create key.\n"
			"Oscl::Mt::Zephyr::Thread::initialize() must be invoked\n"
			"any of these threads are created.\n"
			);
		}
#endif
	}

Oscl::Mt::Thread::~Thread(){
	}

Oscl::Mt::Thread&	Oscl::Mt::Thread::getCurrent() noexcept {
	/*
		void *k_thread_custom_data_get(void);
	 */
	return *(Thread*)k_thread_custom_data_get();
	}

void	Oscl::Mt::Thread::sleep(unsigned long tocks) noexcept{
	// FIXME:
	// In Oscl::Mt, tocks is dependent upon the system tock rate.
	// Therefore, we should convert from tocks to milliseconds
	// for this to be correct.

	/*
		s32_t k_msleep(s32_t ms);
	 */

	k_msleep(tocks);
	}

