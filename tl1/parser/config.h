/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_tl1_parser_configh_
#define _oscl_tl1_parser_configh_
#include "tokenizer.h"

/** */
namespace Oscl {
/** */
namespace TL1 {
/** */
namespace Parser {

/** This class takes an input string, checks for valid
	TL1 syntax, and breaks the valid TL1 command into
	string tokens. All of these things are done during
	the execution of the constructor. After construction
	is complete, the client may then use the public
	interface to find out if the parsing succeeded,
	and subsequently retrieve the various tokens.
 */
template <unsigned	maxParameters>
class Config : public Tokenizer {
	private:
		/** */
		ParameterMem	_parameterMem[maxParameters];
	public:
		/** */
		Config(
				bool	autoDetectParameterType,
				bool	expectTypedParameters,
				char	tl1Command[]
				) noexcept:
			Tokenizer(	_parameterMem,
						maxParameters,
						autoDetectParameterType,
						expectTypedParameters,
						tl1Command
						)
					{
				}
	};
}
}
}

#endif

