/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_tl1_parser_handlerh_
#define _oscl_tl1_parser_handlerh_

/** */
namespace Oscl {
/** */
namespace TL1 {
/** */
namespace Parser {
/** */
namespace Status {

/** */
class Handler {
	public:
		/** */
		virtual ~Handler() {}
		/** */
		virtual void	verbMissing() const noexcept=0;
		/** */
		virtual void	verbFieldMissing() const noexcept=0;
		/** */
		virtual void	tidFieldMissing() const noexcept=0;
		/** */
		virtual void	aidFieldMissing() const noexcept=0;
		/** */
		virtual void	verbTooLong() const noexcept=0;
		/** */
		virtual void	tidTooLong() const noexcept=0;
		/** */
		virtual void	aidTooLong() const noexcept=0;
		/** */
		virtual void	ctagTooLong() const noexcept=0;
		/** */
		virtual void	ctagFieldMissing() const noexcept=0;
		/** */
		virtual void	ctagInvalid() const noexcept=0;
		/** */
		virtual void	terminatorMissing() const noexcept=0;
	};

}
}
}
}

#endif
