/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <string.h>
#include "stream.h"

/** */
namespace Oscl {
/** */
namespace TL1 {
/** */
namespace Parser {
/** */
namespace Status {

Stream::Stream(Oscl::Stream::Output::Api& out) noexcept:_out(out){}

void	Stream::verbMissing() const noexcept{
	print("TL1 Verb missing\n");
	}

void	Stream::verbFieldMissing() const noexcept{
	print("TL1 Verb field missing\n");
	}

void	Stream::tidFieldMissing() const noexcept{
	print("TL1 TID field missing\n");
	}

void	Stream::aidFieldMissing() const noexcept{
	print("TL1 AID field missing\n");
	}

void	Stream::verbTooLong() const noexcept{
	print("TL1 Verb too long\n");
	}

void	Stream::tidTooLong() const noexcept{
	print("TL1 TID too long\n");
	}

void	Stream::aidTooLong() const noexcept{
	print("TL1 AID too long\n");
	}

void	Stream::ctagTooLong() const noexcept{
	print("TL1 CTAG too long\n");
	}

void	Stream::ctagFieldMissing() const noexcept{
	print("TL1 CTAG field missing\n");
	}

void	Stream::ctagInvalid() const noexcept{
	print("TL1 CTAG invalid\n");
	}

void	Stream::terminatorMissing() const noexcept{
	print("TL1 string terminator missing\n");
	}

void	Stream::print(const char* s) const noexcept{
	_out.write(s,strlen(s));
	}

}
}
}
}

