/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_tl1_parser_tokenizerh_
#define _oscl_tl1_parser_tokenizerh_
#include "parameter.h"

/** */
namespace Oscl {
/** */
namespace TL1 {
/** */
namespace Parser {

/** */
namespace Status {
/** */
class Result;
}

/** This class takes an input string, checks for valid
	TL1 syntax, and breaks the valid TL1 command into
	string tokens. All of these things are done during
	the execution of the constructor. After construction
	is complete, the client may then use the public
	interface to find out if the parsing succeeded,
	and subsequently retrieve the various tokens.
 */
class Tokenizer {
	private:
		/** */
		ParameterMem* const		_parameterMem;
		/** */
		const unsigned			_maxParameters;
		/** */
		unsigned				_nValidParameters;
		/** */
		const char*				_verb;
		/** */
		const char*				_tid;
		/** */
		const char*				_aid;
		/** */
		const char*				_uid;
		/** */
		const char*				_ctag;
		/** */
		const char*				_generalBlock;
		/** */
		const Status::Result*	_result;

	public:
		/** */
		Tokenizer(
				ParameterMem*	parameterMem,
				unsigned		nParametersMems,
				bool			autoDetectParameterType,
				bool			expectTypedParameters,
				char			tl1Command[]
				) noexcept;
		/** Returns a non-zero error pointer if parsing
			failed, otherwise returns zero.
		 */
		const Status::Result*	parseResult() noexcept;

		/** Returns a pointer to the TL1 command verb.
			Returns zero if the verb field was empty.
		 */
		const char*	verb() noexcept;

		/** Returns a pointer to the TL1 command TID.
			Returns zero if the TID field was empty.
		 */
		const char*	tid() noexcept;

		/** Returns a pointer to the TL1 command AID.
			Returns zero if the AID field was empty.
		 */
		const char*	aid() noexcept;

		/** Returns a pointer to the TL1 command UID.
			Returns zero if the UID field was empty.
		 */
		const char*	uid() noexcept;

		/** Returns a pointer to the TL1 command CTAG.
			Returns zero if the TID field was empty.
		 */
		const char*	ctag() noexcept;

		/** Returns a pointer to the TL1 command "General Block".
			Returns zero if the "General Block" field was empty.
		 */
		const char*	generalBlock() noexcept;

		/** Returns a pointer to the parameter
			specified by argument "n", or zero if
			the specified argument was not present
			in the TL1 command. Parameters are
			contiguous beginning with n == 0, and
			thus when the application iterates through
			the parameters using this operation
			a return value of zero indicates there
			are no more parameters at this index
			or beyond.
		 */
		const Parameter*	parameter(unsigned n) noexcept;

		/** Returns a pointer to the "value" of the
			first parameter whose type exactly matches
			the string argument "type", or zero if
			the specified argument was not present
			in the TL1 command.
		 */
		const char*				parameterValue(const char* type) noexcept;
	};
}
}
}

#endif

