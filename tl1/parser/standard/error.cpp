/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "error.h"

using namespace Oscl::TL1::Parser::Error;

Standard::Standard() noexcept
		{
	}

void	Standard::badCommand(	Oscl::TL1::Output::Api&	api,
								const char*				ctag
								) noexcept{
	api.denyResponseID(ctag);
	api.unquotedLine("ICNV");
	api.commentedLine("0001: Command not valid");
	api.terminator();
	}

void	Standard::badGeneralBlock(	Oscl::TL1::Output::Api&	api,
									const char*				ctag
									) noexcept{
	api.denyResponseID(ctag);
	api.unquotedLine("IBEX");
	api.commentedLine("0001: Input, block extra");
	api.terminator();
	}

void	Standard::badParameter(	Oscl::TL1::Output::Api&	api,
								const char*				ctag
								) noexcept{
	api.denyResponseID(ctag);
	api.unquotedLine("IPNV");
	api.commentedLine("0001: Parameter not valid");
	api.terminator();
	}

void	Standard::tooManyParameters(	Oscl::TL1::Output::Api&	api,
										const char*				ctag
										) noexcept{
	api.denyResponseID(ctag);
	api.unquotedLine("IPEX");
	api.commentedLine("0001: Input, parameter extra");
	api.terminator();
	}

void	Standard::missingParameter(	Oscl::TL1::Output::Api&	api,
									const char*				ctag
									) noexcept{
	api.denyResponseID(ctag);
	api.unquotedLine("IPMS");
	api.commentedLine("0001: Input, parameter missing");
	api.terminator();
	}

void	Standard::inconsistentParameter(	Oscl::TL1::Output::Api&	api,
											const char*				ctag
											) noexcept{
	api.denyResponseID(ctag);
	api.unquotedLine("IPNC");
	api.commentedLine("0003: Parameter is inconsistent with this command");
	api.terminator();
	}

void	Standard::badAID(	Oscl::TL1::Output::Api&	api,
							const char*				ctag
							) noexcept{
	api.denyResponseID(ctag);
	api.unquotedLine("IIAC");
	api.commentedLine("0007: Input, invalid access identifier");
	api.terminator();
	}

void	Standard::badEquipmentAID(	Oscl::TL1::Output::Api&	api,
									const char*				ctag
									) noexcept{
	api.denyResponseID(ctag);
	api.unquotedLine("IIAC");
	api.commentedLine("0001: Equipmennt Id is invalid");
	api.terminator();
	}

void	Standard::badFacilityAID(	Oscl::TL1::Output::Api&	api,
									const char*				ctag
									) noexcept{
	api.denyResponseID(ctag);
	api.unquotedLine("IIAC");
	api.commentedLine("0006: Facility Id is invalid");
	api.terminator();
	}

