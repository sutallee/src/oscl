/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_tl1_parser_standard_errorh_
#define _oscl_tl1_parser_standard_errorh_
#include "oscl/tl1/parser/error.h"

/** */
namespace Oscl {
/** */
namespace TL1 {
/** */
namespace Parser {
/** */
namespace Error {

/** */
class Standard : public Oscl::TL1::Parser::Error::Api {
	private:
	public:
		/** */
		Standard() noexcept;

	private: // Oscl::TL1::Parser::Error::Api
		/** */
		void	badCommand(	Oscl::TL1::Output::Api&	api,
							const char*				ctag
							) noexcept;
		/** */
		void	badGeneralBlock(	Oscl::TL1::Output::Api&	api,
									const char*				ctag
									) noexcept;
		/** */
		void	badParameter(	Oscl::TL1::Output::Api&	api,
								const char*				ctag
								) noexcept;
		/** */
		void	tooManyParameters(	Oscl::TL1::Output::Api&	api,
									const char*				ctag
									) noexcept;
		/** */
		void	missingParameter(	Oscl::TL1::Output::Api&	api,
									const char*				ctag
									) noexcept;
		/** */
		void	inconsistentParameter(	Oscl::TL1::Output::Api&	api,
										const char*				ctag
										) noexcept;
		/** */
		void	badAID(	Oscl::TL1::Output::Api&	api,
						const char*				ctag
						) noexcept;
		/** */
		void	badEquipmentAID(	Oscl::TL1::Output::Api&	api,
									const char*				ctag
									) noexcept;
		/** */
		void	badFacilityAID(	Oscl::TL1::Output::Api&	api,
								const char*				ctag
								) noexcept;
	};

}
}
}
}

#endif
