/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "result.h"
#include "status.h"
#include "handler.h"

/** */
namespace Oscl {
/** */
namespace TL1 {
/** */
namespace Parser {
/** */
namespace Status {

//////////////////////////////////////////////////////
class VerbMissing : public Result {
	public:
		void	query(Handler& handler) const noexcept;
	};

void VerbMissing::query(Handler& handler) const noexcept{
	handler.verbMissing();
	}

const Result&	verbMissing() noexcept{
	static const VerbMissing	error;
	return error;
	}

//////////////////////////////////////////////////////
class VerbFieldMissing : public Result {
	public:
		void	query(Handler& handler) const noexcept;
	};

void VerbFieldMissing::query(Handler& handler) const noexcept{
	handler.verbFieldMissing();
	}

const Result&	verbFieldMissing() noexcept{
	static const VerbFieldMissing	error;
	return error;
	}

//////////////////////////////////////////////////////
class VerbTooLong : public Result {
	public:
		void	query(Handler& handler) const noexcept;
	};

void VerbTooLong::query(Handler& handler) const noexcept{
	handler.verbTooLong();
	}

const Result&	verbTooLong() noexcept{
	static const VerbTooLong	error;
	return error;
	}

//////////////////////////////////////////////////////
class TidFieldMissing : public Result {
	public:
		void	query(Handler& handler) const noexcept;
	};

void TidFieldMissing::query(Handler& handler) const noexcept{
	handler.tidFieldMissing();
	}

const Result&	tidFieldMissing() noexcept{
	static const TidFieldMissing	error;
	return error;
	}

//////////////////////////////////////////////////////
class TidTooLong : public Result {
	public:
		void	query(Handler& handler) const noexcept;
	};

void TidTooLong::query(Handler& handler) const noexcept{
	handler.tidTooLong();
	}

const Result&	tidTooLong() noexcept{
	static const TidTooLong	error;
	return error;
	}

//////////////////////////////////////////////////////
class AidFieldMissing : public Result {
	public:
		void	query(Handler& handler) const noexcept;
	};

void AidFieldMissing::query(Handler& handler) const noexcept{
	handler.aidFieldMissing();
	}

const Result&	aidFieldMissing() noexcept{
	static const AidFieldMissing	error;
	return error;
	}

//////////////////////////////////////////////////////
class AidTooLong : public Result {
	public:
		void	query(Handler& handler) const noexcept;
	};

void AidTooLong::query(Handler& handler) const noexcept{
	handler.aidTooLong();
	}

const Result&	aidTooLong() noexcept{
	static const AidTooLong	error;
	return error;
	}

//////////////////////////////////////////////////////
class CtagTooLong : public Result {
	public:
		void	query(Handler& handler) const noexcept;
	};

void CtagTooLong::query(Handler& handler) const noexcept{
	handler.aidTooLong();
	}

const Result&	ctagTooLong() noexcept{
	static const CtagTooLong	error;
	return error;
	}

//////////////////////////////////////////////////////
class CtagFieldMissing : public Result {
	public:
		void	query(Handler& handler) const noexcept;
	};

void CtagFieldMissing::query(Handler& handler) const noexcept{
	handler.ctagFieldMissing();
	}

const Result&	ctagFieldMissing() noexcept{
	static const CtagFieldMissing	error;
	return error;
	}

//////////////////////////////////////////////////////
class CtagInvalid : public Result {
	public:
		void	query(Handler& handler) const noexcept;
	};

void CtagInvalid::query(Handler& handler) const noexcept{
	handler.ctagInvalid();
	}

const Result&	ctagInvalid() noexcept{
	static const CtagInvalid	error;
	return error;
	}

//////////////////////////////////////////////////////
class TerminatorMissing : public Result {
	public:
		void	query(Handler& handler) const noexcept;
	};

void TerminatorMissing::query(Handler& handler) const noexcept{
	handler.terminatorMissing();
	}

const Result&	terminatorMissing() noexcept{
	static const TerminatorMissing	error;
	return error;
	}

}
}
}
}

