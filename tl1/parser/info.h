/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_tl1_parser_infoh_
#define _oscl_tl1_parser_infoh_
#include "handler.h"

/** */
namespace Oscl {
/** */
namespace TL1 {
/** */
namespace Parser {
/** */
namespace Status {

/** */
class Info : public Handler {
	public:
		/** */
		void	verbMissing() const noexcept;
		/** */
		void	verbFieldMissing() const noexcept;
		/** */
		void	tidFieldMissing() const noexcept;
		/** */
		void	aidFieldMissing() const noexcept;
		/** */
		void	verbTooLong() const noexcept;
		/** */
		void	tidTooLong() const noexcept;
		/** */
		void	aidTooLong() const noexcept;
		/** */
		void	ctagTooLong() const noexcept;
		/** */
		void	ctagFieldMissing() const noexcept;
		/** */
		void	ctagInvalid() const noexcept;
		/** */
		void	terminatorMissing() const noexcept;
	};

}
}
}
}

#endif
