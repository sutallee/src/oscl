/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <new>
#include <string.h>
#include <ctype.h>
#include "tokenizer.h"
#include "status.h"
#include "oscl/strings/strip.h"
#include "oscl/strings/token.h"
#include "oscl/strings/case.h"

using namespace Oscl::TL1::Parser;

Tokenizer::Tokenizer(
				ParameterMem*	parameterMem,
				unsigned		nParametersMems,
				bool			autoDetectParameterType,
				bool			expectTypedParameters,
				char			tl1Command[]
				) noexcept:
		_parameterMem(parameterMem),
		_maxParameters(nParametersMems),
		_nValidParameters(0),
		_verb(0),
		_tid(0),
		_aid(0),
		_uid(0),
		_ctag(0),
		_generalBlock(0),
		_result(0)
		{
	char*	p	= Oscl::Strings::stripSpaceWritable(tl1Command);
	char*	token;

	// Ensure that there is a terminating semicolon
	token	= strpbrk(p,";");
	if(!token){
		_result	= &Status::terminatorMissing();
		return;
		}
	*token	= '\0';

	// Get the verb
	if(!*p){
		_result = &Status::verbFieldMissing();
		}
	token	= Oscl::Strings::Token::next(&p,":");
	if(!token){
		_result = &Status::verbFieldMissing();
		return;
		}
	if(!*token){
		_result = &Status::verbMissing();
		return;
		}

	Oscl::Strings::Case::upper(token);
	_verb	= token;

	// Get the TID
	token	= Oscl::Strings::Token::next(&p,":");
	if(!token){
		_result = &Status::tidFieldMissing();
		return;
		}
	_tid	= token;

	// Get the AID
	token	= Oscl::Strings::Token::next(&p,":");
	if(!token){
		_result = &Status::aidFieldMissing();
		return;
		}
	_aid	= token;

	// Get the CTAG
	token	= Oscl::Strings::Token::next(&p,":");
	if(!token){
		_result = &Status::ctagFieldMissing();
		return;
		}

	// Ensure CTAG is alph-numeric ASCII
	_ctag	= token;
	for(;*token;++token){
		if(!isalnum(*token)){
			_result = &Status::ctagInvalid();
			return;
			}
		}

	// Parse the "Gerneral Block"
	_generalBlock	= Oscl::Strings::Token::next(&p,":");
	if(!_generalBlock){
		// There is no general block, but
		// thats OK. However, it also indicates
		// that there are no data parameters.
		return;
		}

	// Parse the data parameter pairs
	if(autoDetectParameterType){
		expectTypedParameters	= strchr(p,'=');
		}

	for(unsigned i=0;i<_maxParameters;++i){
		char* token	= Oscl::Strings::Token::next(&p,",");
		if(!token){
			break;
			}
		char*	data	= 0;
		if(expectTypedParameters){
			char*	dp	= strchr(token,'=');
			if(dp){
				data	= dp;
				*data	= '\0';
				++data;
				}
			new (&_parameterMem[i]) Parameter(token,data?data:0);
			}
		else {
			data	= token;
			new (&_parameterMem[i]) Parameter("",data);
			}
		++_nValidParameters;
		}
	}

const Status::Result*	Tokenizer::parseResult() noexcept{
	return _result;
	}

const char*	Tokenizer::verb() noexcept{
	return _verb;
	}

const char*	Tokenizer::tid() noexcept{
	return _tid;
	}

const char*	Tokenizer::aid() noexcept{
	return _aid;
	}

const char*	Tokenizer::uid() noexcept{
	return _uid;
	}

const char*	Tokenizer::ctag() noexcept{
	return _ctag;
	}

const char*	Tokenizer::generalBlock() noexcept{
	return _generalBlock;
	}

const Parameter*	Tokenizer::parameter(unsigned n) noexcept{
	if(!_nValidParameters){
		return 0;
		}
	if(n > (_nValidParameters-1)){
		return 0;
		}
	return (Parameter*)&_parameterMem[n];
	}

const char*	Tokenizer::parameterValue(const char* type) noexcept{
	const Parameter*	parameters	= (Parameter*)_parameterMem;
	for(unsigned i=0;i<_nValidParameters;++i){
		if(!strcmp(type,parameters[i]._type)){
			return parameters[i]._value;
			}
		}
	return 0;
	}


