/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "info.h"
#include "oscl/error/info.h"

/** */
namespace Oscl {
/** */
namespace TL1 {
/** */
namespace Parser {
/** */
namespace Status {

void	Info::verbMissing() const noexcept{
	Oscl::Error::Info::log("TL1 Verb missing\n");
	}

void	Info::verbFieldMissing() const noexcept{
	Oscl::Error::Info::log("TL1 Verb field missing\n");
	}

void	Info::tidFieldMissing() const noexcept{
	Oscl::Error::Info::log("TL1 TID field missing\n");
	}

void	Info::aidFieldMissing() const noexcept{
	Oscl::Error::Info::log("TL1 AID field missing\n");
	}

void	Info::verbTooLong() const noexcept{
	Oscl::Error::Info::log("TL1 Verb too long\n");
	}

void	Info::tidTooLong() const noexcept{
	Oscl::Error::Info::log("TL1 TID too long\n");
	}

void	Info::aidTooLong() const noexcept{
	Oscl::Error::Info::log("TL1 AID too long\n");
	}

void	Info::ctagTooLong() const noexcept{
	Oscl::Error::Info::log("TL1 CTAG too long\n");
	}

void	Info::ctagFieldMissing() const noexcept{
	Oscl::Error::Info::log("TL1 CTAG field missing\n");
	}

void	Info::ctagInvalid() const noexcept{
	Oscl::Error::Info::log("TL1 CTAG invalid\n");
	}

void	Info::terminatorMissing() const noexcept{
	Oscl::Error::Info::log("TL1 string terminator missing\n");
	}

}
}
}
}

