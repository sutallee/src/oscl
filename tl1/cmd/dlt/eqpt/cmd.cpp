/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "cmd.h"

using namespace Oscl::TL1::Cmd::Dlt::Eqpt;

Command::Command(	Oscl::TL1::Session::Output::Api&	output,
							Oscl::Mt::Itc::PostMsgApi&			myPapi,
							Oscl::TL1::Parser::Tokenizer&		tokens
							) noexcept:
		_output(output),
		_tokens(tokens),
		_payload(*this),
		_resp(	output.outputSAP().getReqApi(),
				*this,
				myPapi,
				_payload
				)
		{
	}

void	Command::execute( Oscl::TL1::Cmd::DoneApi& done ) noexcept{
	_done	= &done;

	_output.outputSAP().post(_resp.getSrvMsg());
	}

void	Command::response(	Oscl::Mt::Itc::Atomic::
								Resp::Api<	Oscl::TL1::
											Output::Api
											>::AtomicResp&	msg
								) noexcept{
	_done->done(*this);
	}

void	Command::execute(Oscl::TL1::Output::Api& api) noexcept{
	api.completedResponseID(_tokens.ctag());
	api.quotedLine("FIXME DLT-EQPT");
	api.terminator();
	}

