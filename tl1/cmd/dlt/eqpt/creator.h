/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_tl1_cmd_dlt_eqpt_creatorh_
#define _oscl_tl1_cmd_dlt_eqpt_creatorh_
#include "oscl/tl1/cmd/creator/api.h"

/** */
namespace Oscl {
/** */
namespace TL1 {
/** */
namespace Cmd {
/** */
namespace Dlt {
/** */
namespace Eqpt {

/** */
class Creator :
	public Oscl::TL1::Cmd::Creator::Api
	{
	public:
		/** */
		Creator() noexcept;

	private: // Oscl::TL1::Cmd::Creator::Api
		/** */
		bool	match(const char* verb) const noexcept;
		/** */
		Oscl::TL1::Cmd::Api*
					create(	void*							mem,
							unsigned						memSize,
							Oscl::TL1::Session::
							Output::Api&					output,
							Oscl::Mt::Itc::PostMsgApi&		myPapi,
							Oscl::TL1::Parser::Tokenizer&	tokenizer,
							Oscl::TL1::Parser::Error::Api&	errorApi,
							Oscl::TL1::Session::State::Api&	sessionApi
							) const noexcept;
	};

}
}
}
}
}

#endif
