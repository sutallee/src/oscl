/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <stdio.h>
#include "cmd.h"
#include "oscl/entropy/random128.h"

using namespace Oscl::TL1::Cmd::Req::Login;

Command::Command(	Oscl::TL1::Session::Output::Api&	output,
					Oscl::Mt::Itc::PostMsgApi&			myPapi,
					Oscl::TL1::Parser::Tokenizer&		tokens,
					Oscl::TL1::Session::State::Api&		sessionApi
					) noexcept:
		_output(output),
		_tokens(tokens),
		_payload(*this),
		_resp(	output.outputSAP().getReqApi(),
				*this,
				myPapi,
				_payload
				),
		_sessionApi(sessionApi)
		{
	}

void	Command::execute( Oscl::TL1::Cmd::DoneApi& done ) noexcept{
	_done	= &done;

	_output.outputSAP().post(_resp.getSrvMsg());
	}

void	Command::response(	Oscl::Mt::Itc::Atomic::
								Resp::Api<	Oscl::TL1::
											Output::Api
											>::AtomicResp&	msg
								) noexcept{
	_done->done(*this);
	}

void	Command::execute(Oscl::TL1::Output::Api& api) noexcept{
	unsigned char	buffer[Oscl::Entropy::Random128::bufferSize];
	Oscl::Entropy::Random128::fetch(buffer);
	_sessionApi.loginKey(buffer);
	char			string[2*Oscl::Entropy::Random128::bufferSize+1];
	// Convert random value to an ASCII representation
	// that is each nibble of the random value is represented
	// by the letter A-P, with A==0 and P==15.
	for(unsigned i=0;i<Oscl::Entropy::Random128::bufferSize;++i){
		string[2*i+0]	= (buffer[i]>>4)+0x41;
		string[2*i+1]	= (buffer[i]&0x0F)+0x41;
		}
	string[2*Oscl::Entropy::Random128::bufferSize]	= '\0';

	char	outBuffer[128];
	sprintf(	outBuffer,
				"KEY=%s",
				string
				);

	api.completedResponseID(_tokens.ctag());
	api.quotedLine(outBuffer);
	api.terminator();
	}

