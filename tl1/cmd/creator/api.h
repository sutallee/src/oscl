/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_tl1_cmd_creator_apih_
#define _oscl_tl1_cmd_creator_apih_
#include "oscl/tl1/cmd/api.h"
#include "oscl/tl1/session/output/api.h"
#include "oscl/tl1/parser/tokenizer.h"
#include "oscl/tl1/parser/error.h"
#include "oscl/tl1/session/state/api.h"

/** */
namespace Oscl {
/** */
namespace TL1 {
/** */
namespace Cmd {
/** */
namespace Creator {

/** This interface is used to identify a particular
	TL1 command by its verb and to create an instance
	of the command.
 */
class Api {
	public:
		/** */
		void*		__qitemlink;
	public:
		/** */
		virtual ~Api() {}
		/** */
		virtual bool	match(const char* verb) const noexcept=0;
		/** */
		virtual Oscl::TL1::Cmd::Api*
					create(	void*							mem,
							unsigned						memSize,
							Oscl::TL1::Session::
							Output::Api&					output,
							Oscl::Mt::Itc::PostMsgApi&		myPapi,
							Oscl::TL1::Parser::Tokenizer&	tokenizer,
							Oscl::TL1::Parser::Error::Api&	errorApi,
							Oscl::TL1::Session::State::Api&	sessionApi
							) const noexcept=0;
	};

}
}
}
}

#endif
