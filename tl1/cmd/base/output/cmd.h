/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_tl1_cmd_base_output_cmdh_
#define _oscl_tl1_cmd_base_output_cmdh_
#include "oscl/tl1/cmd/api.h"
#include "oscl/tl1/parser/tokenizer.h"
#include "oscl/mt/itc/atomic/respapi.h"
#include "oscl/tl1/output/api.h"
#include "oscl/tl1/session/output/api.h"

/** */
namespace Oscl {
/** */
namespace TL1 {
/** */
namespace Cmd {
/** */
namespace Base {
/** */
namespace Output {

/** This base class is intended to simplify the creation
	of new TL1 commands, as well as reduce the amount of
	duplicated code (space savings). The sub-class implementation
	is assumed to do all of its "work" in the execute() operation
	of the Output service. The implemenation *may* override the
	execute() operation of the command service, but it must be
	sure to invoke the inherited operation before finishing.
 */
class Command :
	public Oscl::TL1::Cmd::Api,
	private Oscl::Mt::Itc::Atomic::OperationApi<Oscl::TL1::Output::Api>,
	private Oscl::Mt::Itc::Atomic::Resp::Api<Oscl::TL1::Output::Api>
	{
	private:
		/** */
		Oscl::TL1::Session::Output::Api&				_output;
	protected:
		/** */
		Oscl::TL1::Parser::Tokenizer&					_tokens;
	private:
		/** */
		Oscl::Mt::Itc::
		Atomic::Req::Api<	Oscl::TL1::
							Output::Api
							>::AtomicPayload			_payload;
		/** */
		Oscl::Mt::Itc::
		Atomic::Resp::Api<	Oscl::TL1::
							Output::Api
							>::AtomicResp				_resp;
		/** */
		Oscl::TL1::Cmd::DoneApi*						_done;

	public:
		/** */
		Command(	Oscl::TL1::Session::Output::Api&	output,
					Oscl::Mt::Itc::PostMsgApi&			myPapi,
					Oscl::TL1::Parser::Tokenizer&		tokens
					) noexcept;
	protected:
		/** */
		void	execute( Oscl::TL1::Cmd::DoneApi& done ) noexcept;

	private:
		/** This operation needs to be overridden by the implementation.
		 */
		void	execute(Oscl::TL1::Output::Api& api) noexcept;

	private: // Oscl::Mt::Itc::Atomic::Resp::Api<Oscl::TL1::Output::Api>
		/** */
		void	response(	Oscl::Mt::Itc::Atomic::
							Resp::Api<	Oscl::TL1::
										Output::Api
										>::AtomicResp&	msg
							) noexcept;
	};

}
}
}
}
}

#endif
