/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_tl1_cmd_set_sid_cmdh_
#define _oscl_tl1_cmd_set_sid_cmdh_
#include "oscl/tl1/cmd/api.h"
#include "oscl/tl1/parser/tokenizer.h"
#include "oscl/mt/itc/atomic/respapi.h"
#include "oscl/tl1/output/api.h"
#include "oscl/tl1/session/output/api.h"
#include "oscl/sysid/control/api.h"
#include "oscl/tl1/parser/error.h"
#include "oscl/tl1/cmd/base/output/cmd.h"

/** */
namespace Oscl {
/** */
namespace TL1 {
/** */
namespace Cmd {
/** */
namespace Set {
/** */
namespace SID {

/** */
class Command :
	public Oscl::TL1::Cmd::Base::Output::Command
	{
	private:
		/** */
		typedef void	(Command::* OutputFunc)(Oscl::TL1::Output::Api& api);
		/** */
		Oscl::TL1::Parser::Error::Api&					_errorApi;
		/** */
		Oscl::SysID::Control::Api&						_sysIdApi;
		/** */
		OutputFunc										_outFunc;


	public:
		/** */
		Command(	Oscl::TL1::Session::Output::Api&	output,
					Oscl::Mt::Itc::PostMsgApi&			myPapi,
					Oscl::TL1::Parser::Tokenizer&		tokens,
					Oscl::TL1::Parser::Error::Api&		errorApi,
					Oscl::SysID::Control::Api&			sysIdApi
					) noexcept;
	private:
		/** */
		void	execute( Oscl::TL1::Cmd::DoneApi& done ) noexcept;

	private:
		/** */
		void	execute(Oscl::TL1::Output::Api& api) noexcept;

	private:
		/** */
		void	missingParameter(Oscl::TL1::Output::Api& api) noexcept;
	};

}
}
}
}
}

#endif
