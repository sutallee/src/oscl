/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <string.h>
#include "cmd.h"
#include "oscl/checksum/md5/calculator.h"

enum{encodedValueSize=32};

static void	decode(	const char		encoded[encodedValueSize],
					unsigned char	decoded[Oscl::Checksum::MD5::digestSize]
					) noexcept{
	for(unsigned i=0;i<Oscl::Checksum::MD5::digestSize;++i){
		decoded[i]	=		((encoded[2*i+0]-'A')<<4)
						|	(encoded[2*i+1]-'A')
						;
		}
	}

using namespace Oscl::TL1::Cmd::Act::User;

Command::Command(	Oscl::TL1::Session::Output::Api&	output,
					Oscl::Mt::Itc::PostMsgApi&			myPapi,
					Oscl::TL1::Parser::Tokenizer&		tokens,
					Oscl::TL1::Session::State::Api&		sessionApi
					) noexcept:
		_output(output),
		_tokens(tokens),
		_payload(*this),
		_resp(	output.outputSAP().getReqApi(),
				*this,
				myPapi,
				_payload
				),
		_sessionApi(sessionApi),
		_outFunc(0)
		{
	}

void	Command::execute( Oscl::TL1::Cmd::DoneApi& done ) noexcept{
	_done	= &done;

	static const char	password[] = "password";
	const char*		user	= _tokens.aid();
	const Oscl::TL1::Parser::Parameter*
	parameter				= _tokens.parameter(0);
	const char*		oneWay	= (parameter)?parameter->_value:0;
	const char*		secret	= password;
	if(!user || !oneWay || !secret){
		_outFunc	= &Command::invalidParameters;
		_output.outputSAP().post(_resp.getSrvMsg());
		return;
		}
	unsigned char	value[Oscl::Checksum::MD5::digestSize];
	decode(oneWay,value);

	Oscl::Checksum::MD5::Calculator	csum;

	csum.update(user,strlen(user));
	csum.update(secret,strlen(secret));
	csum.update(_sessionApi.loginKey(),Oscl::Checksum::MD5::digestSize);

	unsigned char	calcValue[Oscl::Checksum::MD5::digestSize];
	csum.final(calcValue);
	if(memcmp(value,calcValue,Oscl::Checksum::MD5::digestSize)){
		_outFunc	= &Command::invalidPassword;
		}

	_output.outputSAP().post(_resp.getSrvMsg());
	}

void	Command::response(	Oscl::Mt::Itc::Atomic::
								Resp::Api<	Oscl::TL1::
											Output::Api
											>::AtomicResp&	msg
								) noexcept{
	_done->done(*this);
	}

void	Command::execute(Oscl::TL1::Output::Api& api) noexcept{
	if(_outFunc){
		(*this.*_outFunc)(api);
		return;
		}
	api.completedResponseID(_tokens.ctag());
	api.quotedLine("ACT-USER: log in success.");
	api.terminator();
	}

void	Command::invalidParameters(Oscl::TL1::Output::Api& api) noexcept{
	api.denyResponseID(_tokens.ctag());
	api.quotedLine("REQ-LOGIN: invalid parameters");
	api.terminator();
	}

void	Command::invalidPassword(Oscl::TL1::Output::Api& api) noexcept{
	api.denyResponseID(_tokens.ctag());
	api.quotedLine("REQ-LOGIN: invalid password");
	api.terminator();
	}

