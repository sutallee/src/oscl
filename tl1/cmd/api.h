/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_tl1_cmd_apih_
#define _oscl_tl1_cmd_apih_

/** */
namespace Oscl {
/** */
namespace TL1 {
/** */
namespace Cmd {

/** */
class Api;

/** */
class DoneApi {
	public:
		/** */
		virtual ~DoneApi(){}
		/** */
		virtual void	done(Api& cmd) noexcept=0;
	};

/** */
class Api {
	public:
		/** Reserved for use by the command execution unit. */
		void*		__qitemlink;

	public:
		/** */
		virtual ~Api(){}
		/** This operation is invoked to begin the asynchronous
			execution of the command. When the command completes,
			it invokes the done callback. When the done callback
			is invoked, the client may then destroy the command
			and reuse its resources.
		 */
		virtual void	execute(DoneApi& done) noexcept=0;
	};

}
}
}

#endif
