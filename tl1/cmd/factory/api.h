/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_tl1_cmd_factory_apih_
#define _oscl_tl1_cmd_factory_apih_
#include "oscl/tl1/cmd/api.h"
#include "oscl/tl1/session/output/api.h"

/** */
namespace Oscl {
/** */
namespace TL1 {
/** */
namespace Cmd {
/** */
namespace Factory {

/** */
class Api {
	public:
		/***/
		virtual ~Api() {}
		/** This operation allocates memory for a new TL1
			command, copies and parses the tl1Command buffer
			and returns a pointer to the new command. If the
			resources for creating the new command are
			unavailable, zero is returned. Otherwise,
			a pointer to the command or a syntax error
			result generating command are returned.
		 */
		virtual Oscl::TL1::Cmd::Api*
					allocate(
								Oscl::TL1::Session::
								Output::Api&				output,
								Oscl::Mt::Itc::PostMsgApi&	myPapi,
								const void*					tl1Command,
								unsigned					length
								) noexcept=0;
		/** */
		virtual void	free( Oscl::TL1::Cmd::Api& cmd ) noexcept=0;
	};

}
}
}
}

#endif
