/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_tl1_output_apih_
#define _oscl_tl1_output_apih_

/** */
namespace Oscl {
/** */
namespace TL1 {
/** */
namespace Output {

/** */
class Api {
	public:
		/** */
		virtual ~Api(){}
		/** */
		virtual void	pfAck(const char* ctag) noexcept=0;
		/** */
		virtual void	ipAck(const char* ctag) noexcept=0;
		/** */
		virtual void	terminator() noexcept=0;
		/** */
		virtual void	continuation() noexcept=0;
		/** */
		virtual void	commentedLine(const char* text) noexcept=0;
		/** */
		virtual void	completedResponseID(const char*	ctag) noexcept=0;
		/** */
		virtual void	denyResponseID(const char*	ctag) noexcept=0;
		/** */
		virtual void	quotedLine(const char* text) noexcept=0;
		/** */
		virtual void	unquotedLine(const char* text) noexcept=0;
		/** */
		virtual void	critical(	const char*	verb,
									const char*	tag
									) noexcept=0;
		/** */
		virtual void	major(	const char*	verb,
								const char*	tag
								) noexcept=0;
		/** */
		virtual void	minor(	const char*	verb,
								const char*	tag
								) noexcept=0;
		/** */
		virtual void	warning(	const char*	verb,
									const char*	tag
									) noexcept=0;

	};

}
}
}

#endif
