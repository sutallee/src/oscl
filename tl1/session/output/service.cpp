/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <stdio.h>
#include <string.h>
#include "service.h"
#include "oscl/tl1/output/codes.h"
#include "oscl/time/epoch.h"
#include "oscl/time/time.h"

using namespace Oscl::TL1::Session::Output;

Service::Service(	Oscl::Mt::Itc::PostMsgApi&				myPapi,
					Oscl::Stream::Output::Req::Api::SAP&	outputSAP,
					Oscl::Stream::Output::Api&				outputApi,
					Oscl::SysID::MP::ITC::Req::Api::SAP&	sysIdMonSAP
					) noexcept:
		CloseSync(*this,myPapi),
		_outputSAP(	*this,
					myPapi
					),
		_outputApi(outputApi),
		_localSysID(),
		_payloadSysID(),
		_sysIdCancelDone(	*this,
							&Service::systemIdMonitorCancelDone
							),
		_sysIdMonitor(	myPapi,
						sysIdMonSAP,
						_localSysID,
						_payloadSysID
						),
		_closeReq(0),
		_released(false)
		{
	}

void	Service::systemIdMonitorCancelDone() noexcept{
	_closeReq->returnToSender();
	}

Oscl::Stream::Output::Api&  Service::outputApi() noexcept{
	return _outputApi;
	}

Oscl::Mt::Itc::Atomic::Req::Api<	Oscl::TL1::
									Output::Api
									>::SAP&		Service::outputSAP() noexcept{
	return _outputSAP;
	}

void	Service::request(	Oscl::Mt::Itc::Srv::
							Open::Req::Api::OpenReq&	msg
							) noexcept{
	static char	tl1OutputOpenStr[] = "TL1 output service is open \n\r";
	_released	= false;
	_sysIdMonitor.start();
	_outputApi.write(tl1OutputOpenStr,sizeof(tl1OutputOpenStr));
	msg.returnToSender();
	}

void	Service::request(	Oscl::Mt::Itc::Srv::
							Close::Req::Api::CloseReq&	msg
							) noexcept{
	// FIXME:
	// wait until all pending writes complete.
	_closeReq	= &msg;
	_sysIdMonitor.stop(	_cancelMem.sysID,
						_sysIdCancelDone
						);
	msg.returnToSender();
	}

void	Service::response(	Oscl::Stream::Output::
							Resp::Api::WriteResp&		msg
							) noexcept{
	// FIXME:
	// o Remove from "outstandingWrites" list.
	// o start next write
	}

void	Service::pfAck(const char* ctag) noexcept{
	char	buffer[32];
	sprintf(	buffer,
				"%s %s \n\r <",
				Oscl::TL1::Output::Codes::pfStr,
				ctag
				);
	_outputApi.write(buffer,strlen(buffer));
	}

void	Service::ipAck(const char* ctag) noexcept{
	char	buffer[32];
	sprintf(	buffer,
				"%s %s \n\r <",
				Oscl::TL1::Output::Codes::ipStr,
				ctag
				);
	_outputApi.write(buffer,strlen(buffer));
	}

void	Service::terminator() noexcept{
	static const char	termStr[]	= "\r\n;";
	_outputApi.write(termStr,strlen(termStr));
	}

void	Service::continuation() noexcept{
	static const char	continuationStr[]	= "\r\n>";
	_outputApi.write(continuationStr,strlen(continuationStr));
	}

void	Service::commentedLine(const char* text) noexcept{
	static const char	openCommentStr[]	= "\r\n   /*";
	static const char	closeCommentStr[]	= " */";
	_outputApi.write(openCommentStr,strlen(openCommentStr));
	_outputApi.write(text,strlen(text));
	_outputApi.write(closeCommentStr,strlen(closeCommentStr));

	}

void	Service::completedResponseID(const char* ctag) noexcept{
	char	buffer[128];
	Oscl::Time::Epoch::Date	date(Oscl::Time::EST);
	date.update();
	sprintf(	buffer,
				"\r\n\n   %s "
				"%4u-%2u-%2u %2u:%2u:%2u"
				"\r\nM  %s %s",
				_localSysID.getString(),
				(unsigned)date._yearOfCEC,
				(unsigned)date._monthOfYear,
				(unsigned)date._dayOfMonth,
				(unsigned)date._hourOfDay,
				(unsigned)date._minute,
				(unsigned)date._second,
				ctag,
				Oscl::TL1::Output::Codes::compldStr
				);
	_outputApi.write(buffer,strlen(buffer));
	}

void	Service::denyResponseID(const char*	ctag) noexcept{
	char	buffer[128];
	Oscl::Time::Epoch::Date	date(Oscl::Time::EST);
	date.update();
	sprintf(	buffer,
				"\r\n\n   %s "
				"%4u-%2u-%2u %2u:%2u:%2u"
				"\r\nM  %s %s",
				_localSysID.getString(),
				(unsigned)date._yearOfCEC,
				(unsigned)date._monthOfYear,
				(unsigned)date._dayOfMonth,
				(unsigned)date._hourOfDay,
				(unsigned)date._minute,
				(unsigned)date._second,
				ctag,
				Oscl::TL1::Output::Codes::denyStr
				);
	_outputApi.write(buffer,strlen(buffer));
	}

void	Service::quotedLine(const char* text) noexcept{
	static const char	preTextStr[]	= "\r\n   \"";
	static const char	postTextStr[]	= "\"";
	_outputApi.write(preTextStr,strlen(preTextStr));
	_outputApi.write(text,strlen(text));
	_outputApi.write(postTextStr,strlen(postTextStr));
	}

void	Service::unquotedLine(const char* text) noexcept{
	static const char	preTextStr[]	= "\r\n   ";
	_outputApi.write(preTextStr,strlen(preTextStr));
	_outputApi.write(text,strlen(text));
	}

void	Service::critical(	const char*	verb,
							const char*	tag
							) noexcept{
	char	buffer[128];
	Oscl::Time::Epoch::Date	date(Oscl::Time::EST);
	date.update();
	sprintf(	buffer,
				"\r\n\n   %s "
				"%4u-%2u-%2u %2u:%2u:%2u"
				"\r\n%s %s %s",
				_localSysID.getString(),
				(unsigned)date._yearOfCEC,
				(unsigned)date._monthOfYear,
				(unsigned)date._dayOfMonth,
				(unsigned)date._hourOfDay,
				(unsigned)date._minute,
				(unsigned)date._second,
				Oscl::TL1::Output::Codes::criticalStr,
				tag,
				verb
				);
	_outputApi.write(buffer,strlen(buffer));
	}

void	Service::major(	const char*	verb,
						const char*	tag
						) noexcept{
	char	buffer[128];
	Oscl::Time::Epoch::Date	date(Oscl::Time::EST);
	date.update();
	sprintf(	buffer,
				"\r\n\n   %s "
				"%4u-%2u-%2u %2u:%2u:%2u"
				"\r\n%s %s %s",
				_localSysID.getString(),
				(unsigned)date._yearOfCEC,
				(unsigned)date._monthOfYear,
				(unsigned)date._dayOfMonth,
				(unsigned)date._hourOfDay,
				(unsigned)date._minute,
				(unsigned)date._second,
				Oscl::TL1::Output::Codes::majorStr,
				tag,
				verb
				);
	_outputApi.write(buffer,strlen(buffer));
	}

void	Service::minor(	const char*	verb,
						const char*	tag
						) noexcept{
	char	buffer[128];
	Oscl::Time::Epoch::Date	date(Oscl::Time::EST);
	date.update();
	sprintf(	buffer,
				"\r\n\n   %s "
				"%4u-%2u-%2u %2u:%2u:%2u"
				"\r\n%s %s %s",
				_localSysID.getString(),
				(unsigned)date._yearOfCEC,
				(unsigned)date._monthOfYear,
				(unsigned)date._dayOfMonth,
				(unsigned)date._hourOfDay,
				(unsigned)date._minute,
				(unsigned)date._second,
				Oscl::TL1::Output::Codes::minorStr,
				tag,
				verb
				);
	_outputApi.write(buffer,strlen(buffer));
	}

void	Service::warning(	const char*	verb,
							const char*	tag
							) noexcept{
	char	buffer[128];
	Oscl::Time::Epoch::Date	date(Oscl::Time::EST);
	date.update();
	sprintf(	buffer,
				"\r\n\n   %s "
				"%4u-%2u-%2u %2u:%2u:%2u"
				"\r\n%s %s %s",
				_localSysID.getString(),
				(unsigned)date._yearOfCEC,
				(unsigned)date._monthOfYear,
				(unsigned)date._dayOfMonth,
				(unsigned)date._hourOfDay,
				(unsigned)date._minute,
				(unsigned)date._second,
				Oscl::TL1::Output::Codes::warningStr,
				tag,
				verb
				);
	_outputApi.write(buffer,strlen(buffer));
	}

