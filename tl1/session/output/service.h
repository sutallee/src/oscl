/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_tl1_session_output_serviceh_
#define _oscl_tl1_session_output_serviceh_
#include "oscl/mt/itc/srv/close.h"
#include "oscl/mt/itc/atomic/atomic.h"
#include "oscl/stream/output/itc/respapi.h"
#include "oscl/stream/output/api.h"
#include "oscl/tl1/output/api.h"
#include "oscl/sysid/monitor/part.h"
#include "oscl/strings/fixed.h"
#include "oscl/done/operation.h"
#include "api.h"

/** */
namespace Oscl {
/** */
namespace TL1 {
/** */
namespace Session {
/** */
namespace Output {

/** */
class Service :
	public Oscl::Mt::Itc::Srv::CloseSync,
	public Oscl::Mt::Itc::Atomic::Req::Api<Oscl::TL1::Output::Api>,
	private Oscl::Mt::Itc::Srv::Close::Req::Api,
	private Oscl::Stream::Output::Resp::Api,
	public Api
	{
	private:
		/** */
		union {
			/** */
			Oscl::SysID::MP::ITC::Resp::CancelMem	sysID;
			}										_cancelMem;

		/** */
		Oscl::Mt::Itc::
		Atomic::Req::Api<	Oscl::TL1::
							Output::Api
							>::ConcreteSAP			_outputSAP;

		/** */
		Oscl::Stream::Output::Api&					_outputApi;

		/** */
		Oscl::Strings::Fixed<64>						_localSysID;

		/** */
		Oscl::Strings::Fixed<64>						_payloadSysID;

		/** */
		Oscl::Done::Operation<Service>				_sysIdCancelDone;

		/** */
		Oscl::SysID::Monitor::Part					_sysIdMonitor;

		/** */
		Oscl::Mt::Itc::Srv::
		Close::Req::Api::CloseReq*					_closeReq;

		/** */
		bool										_released;

	public:
		/** */
		Service(	Oscl::Mt::Itc::PostMsgApi&				myPapi,
					Oscl::Stream::Output::Req::Api::SAP&	outputSAP,
					Oscl::Stream::Output::Api&				outputApi,
					Oscl::SysID::MP::ITC::Req::Api::SAP&	sysIdMonSAP
					) noexcept;

	private:
		/** */
		void	systemIdMonitorCancelDone() noexcept;

	public:	// Api
		/** */
		Oscl::Stream::Output::Api&  outputApi() noexcept;
		/** */
		Oscl::Mt::Itc::
		Atomic::Req::Api<	Oscl::TL1::
							Output::Api
							>::SAP&			outputSAP() noexcept;

	private:	// Oscl::Mt::Itc::Srv::Close::Req::Api
		/** */
		void	request(	Oscl::Mt::Itc::Srv::
							Open::Req::Api::OpenReq&	msg
							) noexcept;
		/** */
		void	request(	Oscl::Mt::Itc::Srv::
							Close::Req::Api::CloseReq&	msg
							) noexcept;
	private: // Oscl::Stream::Output::Resp::Api
		/** */
		void	response(	Oscl::Stream::Output::
							Resp::Api::WriteResp&		msg
							) noexcept;
	private: // Oscl::TL1::Output::Api
		/** */
		void	pfAck(const char* ctag) noexcept;
		/** */
		void	ipAck(const char* ctag) noexcept;
		/** */
		void	terminator() noexcept;
		/** */
		void	continuation() noexcept;
		/** */
		void	commentedLine(const char* text) noexcept;
		/** */
		void	completedResponseID(const char*	ctag) noexcept;
		/** */
		void	denyResponseID(const char*	ctag) noexcept;
		/** */
		void	quotedLine(const char* text) noexcept;
		/** */
		void	unquotedLine(const char* text) noexcept;
		/** */
		void	critical(	const char*	verb,
							const char*	tag
							) noexcept;
		/** */
		void	major(	const char*	verb,
						const char*	tag
						) noexcept;
		/** */
		void	minor(	const char*	verb,
						const char*	tag
						) noexcept;
		/** */
		void	warning(	const char*	verb,
							const char*	tag
							) noexcept;

	};

}
}
}
}

#endif
