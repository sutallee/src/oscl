/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_tl1_session_exec_serviceh_
#define _oscl_tl1_session_exec_serviceh_
#include "oscl/session/release/mp/itc/reqapi.h"
#include "oscl/session/release/control/itc/sync.h"
#include "oscl/mt/itc/srv/close.h"
#include "oscl/stream/output/itc/reqapi.h"
#include "oscl/stream/output/api.h"
#include "oscl/tl1/session/output/api.h"
#include "oscl/tl1/cmd/factory/api.h"
#include "oscl/tl1/cmd/api.h"
#include "oscl/tl1/session/state/api.h"

/** */
namespace Oscl {
/** */
namespace TL1 {
/** */
namespace Session {
/** */
namespace Exec {

/** */
class Service :
	public Oscl::Mt::Itc::Srv::CloseSync,
	private Oscl::Mt::Itc::Srv::Close::Req::Api,
	private Oscl::Session::Release::MP::ITC::Req::Api,
	private Oscl::Session::Release::Control::ITC::Req::Api,
	private Oscl::Stream::Output::Req::Api,
	private Oscl::TL1::Cmd::DoneApi,
	public Oscl::TL1::Session::State::Api
	{
	private:
		/** */
		Oscl::Queue<	Oscl::Session::Release::MP::
						ITC::Req::Api::ReleaseReq
						>							_releaseList;

		/** */
		Oscl::Session::Release::MP::
		ITC::Req::Api::ConcreteSAP					_releaseSAP;

		/** */
		Oscl::Session::Release::Control::
		ITC::Sync									_releaseSync;

		/** */
		Oscl::Stream::Output::
		Req::Api::ConcreteSAP						_outputSAP;

		/** */
		Oscl::Mt::Itc::PostMsgApi&					_myPapi;

		/** */
		Oscl::TL1::Session::Output::Api&			_tl1OutputApi;

		/** */
		Oscl::TL1::Cmd::Factory::Api&				_cmdFactory;

		/** */
		Oscl::Queue<	Oscl::Stream::Output::
						Req::Api::WriteReq
						>							_pendingCommands;
		/** */
		bool										_released;

		/** */
		unsigned char								_loginKey[Oscl::TL1::Session::State::keyLength];

	public:
		/** */
		Service(	Oscl::Mt::Itc::PostMsgApi&			myPapi,
					Oscl::TL1::Session::Output::Api&	tl1OutputApi,
					Oscl::TL1::Cmd::Factory::Api&		cmdFactory
					) noexcept;

		/** */
		Oscl::Session::Release::
		MP::ITC::Req::Api::SAP&					releaseSAP() noexcept;

		/** */
		Oscl::Session::Release::Control::Api&	releaseApi() noexcept;

		/** */
		Oscl::Stream::Output::
		Req::Api::SAP&							outputSAP() noexcept;

	private:
		/** */
		void	tryNextCommand() noexcept;

	private: // Oscl::TL1::Cmd:DoneApi
		/** */
		void	done(Oscl::TL1::Cmd::Api& cmd) noexcept;

	private:	// Oscl::Mt::Itc::Srv::Close::Req::Api
		/** */
		void	request(	Oscl::Mt::Itc::Srv::
							Open::Req::Api::OpenReq&	msg
							) noexcept;
		/** */
		void	request(	Oscl::Mt::Itc::Srv::
							Close::Req::Api::CloseReq&	msg
							) noexcept;

	private:	// Oscl::Session::Release::MP::ITC::Req::Api
		/** */
		void	request(	Oscl::Session::Release::
							MP::ITC::Req::Api::ReleaseReq&	msg
							) noexcept;
		/** */
		void	request(	Oscl::Session::Release::
							MP::ITC::Req::Api::CancelReq&	msg
							) noexcept;

	private: // Oscl::Session::Release::Control::ITC::Req::Api
		/** */
		void	request(	Oscl::Session::Release::
							Control::ITC::Req::Api::ReleaseReq&	msg
							) noexcept;
	private: // Oscl::Stream::Output::Req::Api
		/** */
		void	request(	Oscl::Stream::Output::
							Req::Api::WriteReq&		msg
							) noexcept;
		/** */
		void	request(	Oscl::Stream::Output::
							Req::Api::FlushReq&		msg
							) noexcept;
	private:
		/** */
		void	notifyReleased() noexcept;

	private:
		/** */
		const unsigned char*	loginKey() const noexcept;
		/** */
		void			loginKey(unsigned char	key[Oscl::TL1::Session::State::keyLength]) noexcept;
	};

}
}
}
}

#endif
