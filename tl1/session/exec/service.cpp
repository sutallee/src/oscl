/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <string.h>
#include "service.h"
#include "oscl/error/info.h"

using namespace Oscl::TL1::Session::Exec;


Service::Service(	Oscl::Mt::Itc::PostMsgApi&			myPapi,
					Oscl::TL1::Session::Output::Api&	tl1OutputApi,
					Oscl::TL1::Cmd::Factory::Api&		cmdFactory
					) noexcept:
		CloseSync(*this,myPapi),
		_releaseSAP(*this,myPapi),
		_releaseSync(*this,myPapi),
		_outputSAP(*this,myPapi),
		_myPapi(myPapi),
		_tl1OutputApi(tl1OutputApi),
		_cmdFactory(cmdFactory),
		_released(false)
		{
	}

void	Service::tryNextCommand() noexcept{
	Oscl::Stream::Output::Req::Api::WriteReq*
	next	= _pendingCommands.get();
	if(!next){
		return;
		}
	Oscl::TL1::Cmd::Api*
	cmd	= _cmdFactory.allocate(	_tl1OutputApi,
								_myPapi,
								next->_payload._buffer.startOfData(),
								next->_payload._buffer.length()
								);
	if(!cmd){
		_pendingCommands.push(next);
		return;
		}

	cmd->execute(*this);

	next->returnToSender();
	}

void	Service::done(Oscl::TL1::Cmd::Api& cmd) noexcept{
	_cmdFactory.free(cmd);
	tryNextCommand();
	}

Oscl::Session::Release::MP::ITC::Req::Api::SAP&	Service::releaseSAP() noexcept{
	return _releaseSAP;
	}

Oscl::Session::Release::Control::Api&	Service::releaseApi() noexcept{
	return _releaseSync;
	}

Oscl::Stream::Output::Req::Api::SAP&	Service::outputSAP() noexcept{
	return _outputSAP;
	}

void	Service::request(	Oscl::Mt::Itc::Srv::
							Open::Req::Api::OpenReq&	msg
							) noexcept{
	_released	= false;
	msg.returnToSender();
	}

void	Service::request(	Oscl::Mt::Itc::Srv::
							Close::Req::Api::CloseReq&	msg
							) noexcept{
	if(_releaseList.first()){
		for(;;);	// FIXME: Protocol error
		}
	msg.returnToSender();
	}

void	Service::request(	Oscl::Session::Release::
							MP::ITC::Req::Api::ReleaseReq&	msg
							) noexcept{
	if(_released){
		msg.returnToSender();
		return;
		}
	_releaseList.put(&msg);
	}

void	Service::request(	Oscl::Session::Release::
							MP::ITC::Req::Api::CancelReq&	msg
							) noexcept{
	Oscl::Session::Release::MP::ITC::Req::Api::ReleaseReq*
	removed	= _releaseList.remove(&msg._payload._releaseToCancel);
	if(removed){
		removed->returnToSender();
		}
	msg.returnToSender();
	}

void	Service::request(	Oscl::Session::Release::
							Control::ITC::Req::Api::ReleaseReq&	msg
							) noexcept{
	notifyReleased();
	msg.returnToSender();
	}

void	Service::request(	Oscl::Stream::Output::
							Req::Api::WriteReq&		msg
							) noexcept{
	_pendingCommands.put(&msg);
	tryNextCommand();
	}

void	Service::request(	Oscl::Stream::Output::
							Req::Api::FlushReq&		msg
							) noexcept{
	// FIXME: We should not get this.
	for(;;);
	}

void	Service::notifyReleased() noexcept{
	_released	= true;
	Oscl::Session::Release::MP::ITC::Req::Api::ReleaseReq*	next;
	while((next=_releaseList.get())){
		next->returnToSender();
		}
	}

const unsigned char*	Service::loginKey() const noexcept{
	return _loginKey;
	}

void			Service::loginKey(unsigned char	key[Oscl::TL1::Session::State::keyLength]) noexcept{
	memcpy(_loginKey,key,Oscl::TL1::Session::State::keyLength);
	}

