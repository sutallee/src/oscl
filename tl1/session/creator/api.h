/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_tl1_session_creator_apih_
#define _oscl_tl1_session_creator_apih_
#include "oscl/stream/input/itc/reqapi.h"
#include "oscl/stream/input/api.h"
#include "oscl/stream/output/itc/reqapi.h"
#include "oscl/stream/output/api.h"
#include "oscl/session/release/mp/itc/reqapi.h"

/** */
namespace Oscl {
/** */
namespace TL1 {
/** */
namespace Session {
/** */
namespace Creator {

/** This interface provides an abstract means
	of establishing a TL1 session. It is assumed
	to be called from a creational thread.
	The assumption is that if the client has
	a reference to this API then the resources
	exist to establish the session.
 */
class Api {
	public:
		/** */
		virtual ~Api() {}
		/** Constructs and opens a new TL1 session for the stream.
		 */
		virtual void	start(	
							Oscl::Stream::Input::Req::Api::SAP&		inputSAP,
							Oscl::Stream::Input::Api&				inputApi,
							Oscl::Stream::Output::Req::Api::SAP&	outputSAP,
							Oscl::Stream::Output::Api&				outputApi
							) noexcept=0;
		/** Closes and destructs the TL1 session.
		 */
		virtual void	stop() noexcept=0;
		/** */
		virtual	Oscl::Session::Release::
				MP::ITC::Req::Api::SAP&		releaseSAP() noexcept=0;
	};

}
}
}
}

#endif
