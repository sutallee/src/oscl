/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_tl1_session_tcp_listener_configh_
#define _oscl_tl1_session_tcp_listener_configh_
#include "part.h"

/** */
namespace Oscl {
/** */
namespace TL1 {
/** */
namespace Session {
/** */
namespace TCP {
/** */
namespace Listener {

/** */
template <	unsigned	nTransMem,
			unsigned	nTxSegmentMem,
			unsigned	nTxSegFragMem,
			unsigned	nTextTransMem,
			unsigned	nTxBufferMem,
			unsigned	txBufferSize,
			unsigned	maxTextTxMTU
			>
class Config : public Part {
	private:
		/** */
		Oscl::Protocol::IP::TCP::
		Conn::Srv::Server::TransMem			transMem[nTransMem];
		/** */
		Oscl::Protocol::IP::TCP::
		Conn::Srv::Server::SegTransMem		txSegmentMem[nTxSegmentMem];
		/** */
		Oscl::Protocol::IP::TCP::
		Conn::Srv::Server::SegFragTransMem	txSegFragMem[nTxSegFragMem];
		/** */
		Oscl::Protocol::IP::TCP::
		Conn::Srv::Server::TextTransMem		textTransMem[nTextTransMem];
		/** */
		Oscl::Memory::
		AlignedBlock<txBufferSize>			txBufferMem[nTxBufferMem];

	public:
		/** */
		Config(
				Oscl::Mt::Itc::PostMsgApi&			myPapi,
				Oscl::TL1::Session::Creator::Api&	creator,
				Oscl::Mt::Itc::PostMsgApi&			connectionPapi,
				Oscl::Protocol::IP::
				TX::Srv::Req::Api::SAP&				ipFwdSAP,
				Oscl::Protocol::IP::FWD::Api&		ipFwdApi,
				Oscl::Protocol::IP::
				Socket::TCP::Api&					socketApi
				) noexcept:
				Part(	
						myPapi,
						creator,
						connectionPapi,
						ipFwdSAP,
						ipFwdApi,
						socketApi,
						maxTextTxMTU,
						transMem,
						nTransMem,
						txSegmentMem,
						nTxSegmentMem,
						txSegFragMem,
						nTxSegFragMem,
						textTransMem,
						nTextTransMem,
						txBufferMem,
						sizeof(txBufferMem[0])
						)
				{
			}
	};

}
}
}
}
}

#endif
