/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <new>
#include "part.h"

using namespace Oscl::TL1::Session::TCP::Listener;

Part::Part(
			Oscl::Mt::Itc::PostMsgApi&					myPapi,
			Oscl::TL1::Session::Creator::Api&			creator,
			Oscl::Mt::Itc::PostMsgApi&					connectionPapi,
			Oscl::Protocol::IP::
			TX::Srv::Req::Api::SAP&						ipFwdSAP,
			Oscl::Protocol::IP::FWD::Api&				ipFwdApi,
			Oscl::Protocol::IP::
			Socket::TCP::Api&							socketApi,
			unsigned									maxTextTxMTU,
			Oscl::Protocol::IP::TCP::
			Conn::Srv::Server::TransMem					transMem[],
			unsigned									nTransMem,
			Oscl::Protocol::IP::TCP::
			Conn::Srv::Server::SegTransMem				txSegmentMem[],
			unsigned									nTxSegmentMem,
			Oscl::Protocol::IP::TCP::
			Conn::Srv::Server::SegFragTransMem			txSegFragMem[],
			unsigned									nTxSegFragMem,
			Oscl::Protocol::IP::TCP::
			Conn::Srv::Server::TextTransMem				textTransMem[],
			unsigned									nTextTransMem,
			void*										txBufferMem,
			unsigned									txBufferMemSize
			) noexcept:
		OpenSync(	*this,
					myPapi
					),
		_tcpConnection(
						connectionPapi,
						ipFwdSAP,
						ipFwdApi,
						socketApi,
						maxTextTxMTU,
						transMem,
						nTransMem,
						txSegmentMem,
						nTxSegmentMem,
						txSegFragMem,
						nTxSegFragMem,
						textTransMem,
						nTextTransMem,
						txBufferMem,
						txBufferMemSize
						),
		_tcpSessionSAP(_tcpConnection.getSAP()),
		_myPapi(myPapi),
		_creator(creator),
		_state(*this),
		_listen(0),
		_connectionOpen(false)
		{
	}

void	Part::requestOpen() noexcept{
	Oscl::Mt::Itc::Srv::Open::Req::Api::OpenPayload*
	payload	= new (&_respMem.oc.open.payload)
				Oscl::Mt::Itc::Srv::
				Open::Req::Api::OpenPayload();

	Oscl::Mt::Itc::Srv::Open::Resp::Api::OpenResp*
	resp	= new (&_respMem.oc.open.resp)
				Oscl::Mt::Itc::Srv::
				Open::Resp::Api::OpenResp(	_tcpConnection.getSAP().getReqApi(),
											*this,
											_myPapi,
											*payload
											);
	_tcpConnection.getSAP().post(resp->getSrvMsg());
	}

void	Part::request(	Oscl::Mt::Itc::Srv::
						Open::Req::Api::OpenReq&	msg
						) noexcept{
	_openReq	= &msg;
	_tcpConnection.start();
	_tcpConnection.syncOpen();
	_state.open();
	}

void	Part::response(	Oscl::Mt::Itc::Srv::
						Open::Resp::Api::OpenResp&		msg
						) noexcept{
	// Session has been established!
	_connectionOpen	= true;
	}

void	Part::response(	Oscl::Mt::Itc::Srv::
						Close::Resp::Api::CloseResp&	msg
						) noexcept{
	requestOpen();
	}

void	Part::response(	Oscl::Protocol::IP::TCP::
						Conn::Srv::ITC::Resp::Api::ListenResp&	msg
						) noexcept{
	_state.listenReqDone();
	}

void	Part::response(	Oscl::Protocol::IP::TCP::
						Conn::Srv::ITC::Resp::Api::CancelResp&	msg
						) noexcept{
	_state.listenCancelDone();
	}

void	Part::response(	Oscl::Session::Release::MP::
						ITC::Resp::Api::ReleaseResp&	msg
						) noexcept{
	_state.sessionCloseNotificationDone();
	}

void	Part::response(	Oscl::Session::Release::MP::
						ITC::Resp::Api::CancelResp&		msg
						) noexcept{
	// This message is never sent.
	}

void	Part::sendAsyncListenReq() noexcept{
	Oscl::Protocol::IP::TCP::Conn::
	Srv::ITC::Resp::Api::ListenResp*
	_listen	= new (&_respMem.listen.resp)
				Oscl::Protocol::IP::TCP::Conn::Srv::
				ITC::Resp::Api::ListenResp(	_tcpConnection.getConnSAP().getReqApi(),
											*this,
											_myPapi
											);
	_tcpConnection.getConnSAP().post(_listen->getSrvMsg());
	}

void	Part::sendAsyncListenCancelReq() noexcept{
	Oscl::Protocol::IP::TCP::Conn::Srv::ITC::Req::Api::CancelPayload*
	payload	= new (&_cancelMem.listen.payload)
				Oscl::Protocol::IP::TCP::Conn::Srv::
				ITC::Req::Api::CancelPayload(_listen->getSrvMsg());

	Oscl::Protocol::IP::TCP::Conn::Srv::ITC::Resp::Api::CancelResp*
	resp	= new (&_cancelMem.listen.resp)
				Oscl::Protocol::IP::TCP::Conn::Srv::
				ITC::Resp::Api::CancelResp(	_tcpConnection.getConnSAP().getReqApi(),
											*this,
											_myPapi,
											*payload
											);
	_tcpConnection.getConnSAP().post(resp->getSrvMsg());
	}

void	Part::syncCreateSession() noexcept{
	// nothing to do here.
	}

void	Part::syncOpenSession() noexcept{
	_creator.start(	_tcpConnection.normalInputSAP(),
					_tcpConnection.syncInput(),
					_tcpConnection.outputSAP(),
					_tcpConnection.syncOutput()
					);
	}

void	Part::syncCloseConnection() noexcept{
	_tcpConnection.syncClose();
	_tcpConnection.syncOpen();
	}

void	Part::syncCloseSession() noexcept{
	_creator.stop();
	}

void	Part::syncDestroySession() noexcept{
	// Nothing to do here
	}

void	Part::sendAsyncSessionCloseNotificationReq() noexcept{
	Oscl::Session::Release::MP::ITC::Resp::Api::ReleaseResp*
	resp	= new (&_releaseMem)
				Oscl::Session::Release::MP::
				ITC::Resp::Api::ReleaseResp(	_creator.releaseSAP().getReqApi(),
												*this,
												_myPapi
												);
	_creator.releaseSAP().post(resp->getSrvMsg());
	}

void	Part::returnOpenReq() noexcept{
	_openReq->returnToSender();
	_openReq	= 0;
	}

void	Part::returnCloseReq() noexcept{
	_tcpConnection.stop();
	_closeReq->returnToSender();
	_closeReq	= 0;
	}

