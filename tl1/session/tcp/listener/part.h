/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_tl1_session_tcp_listener_parth_
#define _oscl_tl1_session_tcp_listener_parth_
#include "oscl/tl1/session/creator/api.h"
#include "oscl/mt/itc/srv/crespmem.h"
#include "oscl/done/operation.h"
#include "oscl/mt/itc/srv/open.h"
#include "oscl/protocol/ip/tcp/conn/srv/server/part.h"
#include "oscl/session/creator/fsm/sv.h"
#include "oscl/protocol/ip/tcp/conn/srv/itc/respmem.h"
#include "oscl/session/release/mp/itc/respmem.h"

/** */
namespace Oscl {
/** */
namespace TL1 {
/** */
namespace Session {
/** */
namespace TCP {
/** */
namespace Listener {

/** This service waits for TCP connection and creates
	a TL1 session when the connection is established.
	While the TL1 connection is established, the service
	monitors the TL1 session and closes the connection
	when the session indicates that it is completed.
	Subsequently, the process begins again.
	Additionally, the service may be requested to
	reject future incomming session requests, and to
	administratively close an existing connection.
 */
class Part :	private Oscl::Mt::Itc::Srv::Close::Resp::Api,
				private Oscl::Mt::Itc::Srv::Open::Req::Api,
				public Oscl::Mt::Itc::Srv::OpenSync,
				private Oscl::Session::Creator::FSM::StateVar::ContextApi,
				private Oscl::Protocol::IP::TCP::Conn::Srv::ITC::Resp::Api,
				private Oscl::Session::Release::MP::ITC::Resp::Api
	{
	private:
		/** */
		union {
			/** */
			Oscl::Mt::Itc::Srv::Close::Resp::Mem		oc;
			/** */
			Oscl::Protocol::IP::TCP::
			Conn::Srv::ITC::Resp::ListenMem				listen;
			} _respMem;
		/** */
		union {
			/** */
			Oscl::Protocol::IP::TCP::
			Conn::Srv::ITC::Resp::CancelMem				listen;
			} _cancelMem;
		/** */
		Oscl::Session::Release::MP::
		ITC::Resp::ReleaseMem						_releaseMem;
		/** */
		Oscl::Protocol::IP::TCP::
		Conn::Srv::Server::Part						_tcpConnection;
		/** */
		Oscl::Mt::Itc::Srv::Close::Req::Api::SAP&	_tcpSessionSAP;
		/** */
		Oscl::Mt::Itc::PostMsgApi&					_myPapi;
		/** */
		Oscl::TL1::Session::Creator::Api&			_creator;
		/** */
		Oscl::Session::Creator::FSM::StateVar		_state;
		/** */
		Oscl::Protocol::IP::TCP::Conn::Srv::
		ITC::Resp::Api::ListenResp*					_listen;
		/** */
		Oscl::Mt::Itc::Srv::Open::Req::Api::OpenReq*	_openReq;
		/** */
		Oscl::Mt::Itc::Srv::Close::Req::Api::CloseReq*	_closeReq;
		/** This state variable is true if the
			TCP connection is open.
		 */
		bool										_connectionOpen;

	public:
		/** */
		Part(
			Oscl::Mt::Itc::PostMsgApi&					myPapi,
			Oscl::TL1::Session::Creator::Api&			creator,
			Oscl::Mt::Itc::PostMsgApi&					connectionPapi,
			Oscl::Protocol::IP::
			TX::Srv::Req::Api::SAP&						ipFwdSAP,
			Oscl::Protocol::IP::FWD::Api&				ipFwdApi,
			Oscl::Protocol::IP::
			Socket::TCP::Api&							socketApi,
			unsigned									maxTextTxMTU,
			Oscl::Protocol::IP::TCP::
			Conn::Srv::Server::TransMem					transMem[],
			unsigned									nTransMem,
			Oscl::Protocol::IP::TCP::
			Conn::Srv::Server::SegTransMem				txSegmentMem[],
			unsigned									nTxSegmentMem,
			Oscl::Protocol::IP::TCP::
			Conn::Srv::Server::SegFragTransMem			txSegFragMem[],
			unsigned									nTxSegFragMem,
			Oscl::Protocol::IP::TCP::
			Conn::Srv::Server::TextTransMem				textTransMem[],
			unsigned									nTextTransMem,
			void*										txBufferMem,
			unsigned									txBufferMemSize
			) noexcept;

	private:
		/** */
		void	requestOpen() noexcept;

	private:	// Oscl::Mt::Itc::Srv::Close::Resp::Api
		/** This operation is invoked when this service is
			opened.
		 */
		void	request(	Oscl::Mt::Itc::Srv::
							Open::Req::Api::OpenReq&	msg
							) noexcept;

	private:	// Oscl::Mt::Itc::Srv::Close::Resp::Api
		/** This operation is invoked when the TCP connection
			is established. This begins the procedure for
			creating the TL1 session.
		 */
		void	response(	Oscl::Mt::Itc::Srv::
							Open::Resp::Api::OpenResp&		msg
							) noexcept;

		/** This operation is invoked when the TCP connection
			has been completely closed by this service.
		 */
		void	response(	Oscl::Mt::Itc::Srv::
							Close::Resp::Api::CloseResp&	msg
							) noexcept;
	private:	// Oscl::Protocol::IP::TCP::Conn::Srv::ITC::Resp::Api
		/** */
		void	response(	Oscl::Protocol::IP::TCP::
							Conn::Srv::ITC::Resp::Api::ListenResp&	msg
							) noexcept;
		/** */
		void	response(	Oscl::Protocol::IP::TCP::
							Conn::Srv::ITC::Resp::Api::CancelResp&	msg
							) noexcept;

	private: // Oscl::Session::Release::MP::ITC::Resp::Api
		/** */
		void	response(	Oscl::Session::Release::MP::
							ITC::Resp::Api::ReleaseResp&	msg
							) noexcept;
		/** */
		void	response(	Oscl::Session::Release::MP::
							ITC::Resp::Api::CancelResp&		msg
							) noexcept;

	private: // Oscl::Session::Creator::FSM::ContextApi
		/** */
		void	sendAsyncListenReq() noexcept;
		/** */
		void	sendAsyncListenCancelReq() noexcept;
		/** */
		void	syncCreateSession() noexcept;
		/** */
		void	syncOpenSession() noexcept;
		/** */
		void	syncCloseConnection() noexcept;
		/** */
		void	syncCloseSession() noexcept;
		/** */
		void	syncDestroySession() noexcept;
		/** */
		void	sendAsyncSessionCloseNotificationReq() noexcept;
		/** */
		void	returnOpenReq() noexcept;
		/** */
		void	returnCloseReq() noexcept;
	};

}
}
}
}
}

#endif
