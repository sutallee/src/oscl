/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_tl1_session_input_serviceh_
#define _oscl_tl1_session_input_serviceh_
#include "oscl/stream/input/itc/respmem.h"
#include "oscl/stream/output/itc/respmem.h"
#include "oscl/session/release/control/api.h"
#include "oscl/mt/itc/srv/close.h"
#include "oscl/done/api.h"

/** */
namespace Oscl {
/** */
namespace TL1 {
/** */
namespace Session {
/** */
namespace Input {

/** */
class Service;

/** */
class TransRead : private Oscl::Stream::Input::Resp::Api {
	public:
		/** */
		void*			__qitemlink;
		/** */
		Oscl::Stream::Input::Req::Api::ReadPayload	_payload;

	private:
		/** */
		Service&									_context;
		/** */
		Oscl::Stream::Input::Resp::Api::ReadResp	_resp;

	public:
		/** */
		TransRead(	Service&							context,
					Oscl::Mt::Itc::PostMsgApi&			myPapi,
					void*								buffer,
					unsigned							bufferLength
					) noexcept;

		/** */
		void	start() noexcept;
		/** */
		void	stop(Oscl::Stream::Input::Resp::CancelRespMem& mem) noexcept;

	private: // Oscl::Stream::Input::Resp::Api
		/** */
		void	response(	Oscl::Stream::Input::
							Resp::Api::ReadResp&	msg
							) noexcept;
		/** */
		void	response(	Oscl::Stream::Input::
							Resp::Api::CancelResp&	msg
							) noexcept;
	};

/** */
struct TransReadMem {
	/** */
	Oscl::Memory::
	AlignedBlock<32>				buffer;
	/** */
	Oscl::Memory::
	AlignedBlock<sizeof(TransRead)>	trans;
	};

/** */
class TransWrite : private Oscl::Stream::Output::Resp::Api {
	public:
		/** */
		void*							__qitemlink;

	private:
		/** */
		Oscl::Stream::Output::
		Resp::WriteMem							_writeMem;
		/** */
		Service&								_context;
		/** */
		Oscl::Buffer::Var						_buffer;

	public:
		/** */
		TransWrite(	Service&								context,
					char									lineBuffer[],
					unsigned								lineBufferSize
					) noexcept;

		/** Returns the number of characters appended
			if the terminator character was detected.
			Otherwise, returns zero. If the line buffer
			size is exceeded, the last characters will
			be silently discarded.
		 */
		unsigned		append(	const void*	buffer,
								unsigned	length
								) noexcept;

		/** Asynchronously sends the stream output message to the
			stream output SAP. The appropriate "done" routine
			of the context is invoked when the operation completes.
		 */
		void	start() noexcept;

	private: // Oscl::Stream::Output::Resp::Api
		/** */
		void	response(	Oscl::Stream::Output::
							Resp::Api::WriteResp&	msg
							) noexcept;
	};

/** */
struct TransWriteMem {
	/** */
	Oscl::Memory::
	AlignedBlock<sizeof(TransWrite)>	trans;
	};

/** This part asynchronously reads the source stream
	and looks for a creator specified "line" terminator.
	As characters are read from the source stream they
	are accumulated into a "line" buffer until the
	terminator is found. Subsequently, the line buffer
	is sent asynchronously to the "Stream" destination.
	When the destination returns the message, the process
	continues.
 */
class Service :
	private Oscl::Mt::Itc::Srv::Close::Req::Api,
	public Oscl::Mt::Itc::Srv::CloseSync
	{
	friend class TransWrite;
	friend class TransRead;
	private:
		/** */
		union {
			/** */
			Oscl::Stream::Input::
			Resp::CancelRespMem		input;
			}										_stopMem;
		/** */
		Oscl::Mt::Itc::PostMsgApi&					_myPapi;
		/** */
		Oscl::Queue<TransRead>						_freeTransRead;
		/** */
		Oscl::Queue<TransRead>						_pendingRead;
		/** */
		Oscl::Queue<TransWrite>						_freeTransWrite;
		/** */
		Oscl::Queue<TransWrite>						_pendingWrite;
		/** */
		Oscl::Queue<TransRead>						_pendingReads;
		/** */
		Oscl::Stream::Input::Req::Api::SAP&			_sourceSAP;
		/** */
		Oscl::Stream::Output::Req::Api::SAP&		_destSAP;
		/** */
		Oscl::Session::Release::Control::Api&		_controlApi;
		/** */
		char*										_lineBuffer;
		/** */
		const unsigned								_lineBufferSize;
		/** */
		const char									_terminator;
		/** */
		TransWrite*									_currentLine;
		/** */
		unsigned									_outstandingTransWrites;
		/** */
		Oscl::Mt::Itc::Srv::Close::
		Req::Api::CloseReq*							_closeReq;
		/** */
		bool										_closing;
		
	public:
		/** */
		Service(
				Oscl::Mt::Itc::PostMsgApi&					myPapi,
				Oscl::Stream::Input::Req::Api::SAP&			sourceSAP,
				Oscl::Stream::Output::Req::Api::SAP&		destSAP,
				Oscl::Session::Release::Control::Api&		controlApi,
				TransReadMem								transReadMem[],
				unsigned									nTransReadMem,
				TransWriteMem								transWriteMem[],
				unsigned									nTransWriteMem,
				char										lineBuffer[],
				const unsigned								lineBufferSize,
				char										terminator
				) noexcept;

	private:
		/** Invoked by the TransWrite when the stream
			write operation completes. This operation
			is responsible for deciding whether or not
			the write was successful and taking the
			action to either process the transaction
			and restart it, or to otherwise dispose
			of it.
		 */
		void	done(TransWrite& trans) noexcept;

		/** Invoked by the TransWrite when the stream
			write operation completes. This operation
			is responsible for deciding whether or not
			the write was successful and taking the
			action to either process the transaction
			and restart it, or to otherwise dispose
			of it.
		 */
		void	done(TransRead& trans) noexcept;

		/** */
		void	canceled(TransRead& trans) noexcept;

		/** */
		void	processQueue() noexcept;

		/** */
		void	cancelNext() noexcept;

		/** */
		void	conditionallyFinishClose() noexcept;

	private: // Oscl::Mt::Itc::Srv::Close::Req::Api
		/** */
		void	request(	Oscl::Mt::Itc::Srv::
							Open::Req::Api::OpenReq&	msg
							) noexcept;
		/** */
		void	request(	Oscl::Mt::Itc::Srv::
							Close::Req::Api::CloseReq&	msg
							) noexcept;
	};

}
}
}
}

#endif
