/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <new>
#include "service.h"

using namespace Oscl::TL1::Session::Input;

TransRead::TransRead(	Service&					context,
						Oscl::Mt::Itc::PostMsgApi&	myPapi,
						void*						buffer,
						unsigned					bufferLength
						) noexcept:
		_payload(buffer,bufferLength),
		_context(context),
		_resp(	context._sourceSAP.getReqApi(),
				*this,
				myPapi,
				_payload
				)
		{
	}

void	TransRead::start() noexcept{
	_context._sourceSAP.post(_resp.getSrvMsg());
	}

void	TransRead::stop(Oscl::Stream::Input::Resp::CancelRespMem& mem) noexcept{
	Oscl::Stream::Input::Req::Api::CancelPayload*
	payload	= new (&mem.payload)
				Oscl::Stream::Input::Req::Api::
				CancelPayload(_resp.getSrvMsg());

	Oscl::Stream::Input::Resp::Api::CancelResp*
	resp	= new (&mem.resp)
				Oscl::Stream::Input::Resp::Api::
				CancelResp(	_context._sourceSAP.getReqApi(),
							*this,
							_context._myPapi,
							*payload
							);
	_context._sourceSAP.post(resp->getSrvMsg());
	}

void	TransRead::response(	Oscl::Stream::Input::
								Resp::Api::ReadResp&	msg
								) noexcept{
	_context.done(*this);
	}

void	TransRead::response(	Oscl::Stream::Input::
								Resp::Api::CancelResp&	msg
								) noexcept{
	_context.canceled(*this);
	}


TransWrite::TransWrite(	Service&								context,
						char									lineBuffer[],
						unsigned								lineBufferSize
						) noexcept:
		_context(context),
		_buffer(lineBuffer,lineBufferSize)
		{
	}

unsigned	TransWrite::append(	const void*	buffer,
								unsigned	length
								) noexcept{
	const char*	p	= (const char*)buffer;
	for(unsigned i=0;i<length;++i){
		if(p[i] == _context._terminator){
			++i;
			_buffer.append(buffer,i);
			return i;
			}
		}
	_buffer.append(buffer,length);
	return 0;
	}

void	TransWrite::start() noexcept{
	Oscl::Stream::Output::Req::Api::WritePayload*
	payload	= new (&_writeMem.payload)
				Oscl::Stream::Output::Req::Api::
				WritePayload(_buffer);

	Oscl::Stream::Output::Resp::Api::WriteResp*
	resp	= new (&_writeMem.resp)
				Oscl::Stream::Output::Resp::Api::
				WriteResp(	_context._destSAP.getReqApi(),
							*this,
							_context._myPapi,
							*payload
							);
	_context._destSAP.post(resp->getSrvMsg());
	}

void	TransWrite::response(	Oscl::Stream::Output::
								Resp::Api::WriteResp&	msg
								) noexcept{
	_buffer.empty();
	_context.done(*this);
	}

Service::Service(
				Oscl::Mt::Itc::PostMsgApi&					myPapi,
				Oscl::Stream::Input::Req::Api::SAP&			sourceSAP,
				Oscl::Stream::Output::Req::Api::SAP&		destSAP,
				Oscl::Session::Release::Control::Api&		controlApi,
				TransReadMem								transReadMem[],
				unsigned									nTransReadMem,
				TransWriteMem								transWriteMem[],
				unsigned									nTransWriteMem,
				char										lineBuffer[],
				const unsigned								lineBufferSize,
				char										terminator
				) noexcept:
		CloseSync(*this,myPapi),
		_myPapi(myPapi),
		_sourceSAP(sourceSAP),
		_destSAP(destSAP),
		_controlApi(controlApi),
		_lineBuffer(lineBuffer),
		_lineBufferSize(lineBufferSize),
		_terminator(terminator),
		_currentLine(0),
		_outstandingTransWrites(0),
		_closeReq(0),
		_closing(false)
		{
	for(unsigned i=0;i<nTransReadMem;++i){
		TransRead*	next	= new(&transReadMem[i].trans)
								TransRead(	*this,
											myPapi,
											&transReadMem[i].buffer,
											sizeof(transReadMem[i].buffer)
											);
		
		_freeTransRead.put(next);
		}
	const unsigned	maxLineSize	= lineBufferSize/nTransWriteMem;
	char*	p	= lineBuffer;
	for(unsigned i=0;i<nTransWriteMem;++i,p+=maxLineSize){
		TransWrite*	next	= new(&transWriteMem[i].trans)
								TransWrite(	*this,
											p,
											maxLineSize
											);
		
		_freeTransWrite.put(next);
		}
	}

void	Service::done(TransWrite& trans) noexcept{
	_freeTransWrite.put(&trans);
	--_outstandingTransWrites;
	if(_closing){
		conditionallyFinishClose();
		return;
		}
	processQueue();
	}

void	Service::done(TransRead& trans) noexcept{
	_pendingReads.remove(&trans);
	if(!trans._payload._buffer.length()){
		_closing	= true;
		_freeTransRead.put(&trans);
		_controlApi.release();
		return;
		}
	_pendingRead.put(&trans);
	processQueue();
	}

void	Service::canceled(TransRead& trans) noexcept{
	// At this point, the transaction is already
	// in the _freeTransRead list.
	cancelNext();
	}

void	Service::cancelNext() noexcept{
	TransRead*	next;
	if(_currentLine){
		_freeTransWrite.put(_currentLine);
		_currentLine	= 0;
		}
	while((next=_pendingRead.get())){
		_freeTransRead.put(next);
		}
	next	= _pendingReads.get();
	if(next){
		next->stop(_stopMem.input);
		return;
		}
	if(_outstandingTransWrites){
		return;
		}
	conditionallyFinishClose();
	}

void	Service::conditionallyFinishClose() noexcept{
	if(_pendingRead.first()){
		return;
		}
	if(_pendingReads.first()){
		return;
		}
	if(_outstandingTransWrites){
		return;
		}
	_closeReq->returnToSender();
	}

void	Service::processQueue() noexcept{
	if(!_currentLine){
		_currentLine	= _freeTransWrite.get();
		if(!_currentLine){
			return;
			}
		}
	while(_currentLine){
		TransRead*	rtrans	= _pendingRead.first();
		if(!rtrans){
			return;
			}
		unsigned	nAppended;
		nAppended	= _currentLine->append(	rtrans->_payload._buffer.startOfData(),
											rtrans->_payload._buffer.length()
											);
		if(nAppended){
			_currentLine->start();
			++_outstandingTransWrites;
			_currentLine	= _freeTransWrite.get();
			rtrans->_payload._buffer.stripHeader(nAppended);
			if(!rtrans->_payload._buffer.length()){
				rtrans->_payload._buffer.empty();
				rtrans	= _pendingRead.get();
				if(rtrans){
					_pendingReads.put(rtrans);
					rtrans->start();
					}
				}
			}
		else {
			rtrans->_payload._buffer.empty();
			rtrans	= _pendingRead.get();
			if(rtrans){
				_pendingReads.put(rtrans);
				rtrans->start();
				}
			}
		}
	}

void	Service::request(	Oscl::Mt::Itc::Srv::
							Open::Req::Api::OpenReq&	msg
							) noexcept{
	_closing	= false;
	TransRead*	next;
	while((next=_freeTransRead.get())){
		next->start();
		_pendingReads.put(next);
		}
	msg.returnToSender();
	}

void	Service::request(	Oscl::Mt::Itc::Srv::
							Close::Req::Api::CloseReq&	msg
							) noexcept{
	_closing	= true;
	_closeReq	= &msg;
	cancelNext();
	}


