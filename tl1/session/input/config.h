/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_tl1_session_input_configh_
#define _oscl_tl1_session_input_configh_
#include "service.h"

/** */
namespace Oscl {
/** */
namespace TL1 {
/** */
namespace Session {
/** */
namespace Input {

/** This part asynchronously reads the source stream
	and looks for a creator specified "line" terminator.
	As characters are read from the source stream they
	are accumulated into a "line" buffer until the
	terminator is found. Subsequently, the line buffer
	is sent asynchronously to the "Stream" destination.
	When the destination returns the message, the process
	continues.
 */
template <	unsigned	nTransReadMem,
			unsigned	nTransWriteMem,
			unsigned	maxLineLength
			>
class Config : public Service {
	private:
		/** */
		TransReadMem	_transReadMem[nTransReadMem];
		/** */
		TransWriteMem	_transWriteMem[nTransWriteMem];
		/** */
		char			_lineBuffer[nTransWriteMem*maxLineLength];

	public:
		/** */
		Config(
				Oscl::Mt::Itc::PostMsgApi&					myPapi,
				Oscl::Stream::Input::Req::Api::SAP&			sourceSAP,
				Oscl::Stream::Output::Req::Api::SAP&		destSAP,
				Oscl::Session::Release::Control::Api&		controlApi,
				char										terminator
				) noexcept:
			Service(
				myPapi,
				sourceSAP,
				destSAP,
				controlApi,
				_transReadMem,
				nTransReadMem,
				_transWriteMem,
				nTransWriteMem,
				_lineBuffer,
				nTransWriteMem*maxLineLength,
				terminator
				){}

	};

}
}
}
}

#endif
