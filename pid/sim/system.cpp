/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "system.h"
#include <math.h>

/* Copyright 2003 Michael Nelson Moran
 * This software may be copied and used without restriction.
 */ 

using namespace Oscl::PID::SIM;

System::System(	float	tickPeriodInSeconds,
				float	environmentResistance,
				float	capacitance,
				float	initialOutputPotential
				) noexcept:
		_tickPeriodInSeconds(tickPeriodInSeconds),
		_previousOutputPotential(initialOutputPotential),
		_environmentResistance(environmentResistance),
		_capacitance(capacitance)
		{
	}

void	System::start(float potential) noexcept{
	_peq	= potential;
	_req	= _environmentResistance;
	}

void	System::start(float potential, float resistance) noexcept{
	_peq	= potential;
	_req	= resistance;
	}

void	System::accumulate(float potential,float resistance) noexcept{
	_peq	= ((potential-_peq)*(_req/(resistance+_req)))+_peq;
	_req	= 1.0/((1.0/_req)+(1.0/resistance));
	}

float	System::finish() noexcept{
	float	vdelta	= _peq - _previousOutputPotential;
	float	vchange	= vdelta * (1-exp(-(_tickPeriodInSeconds/(_req*_capacitance))));
	_previousOutputPotential	= vchange+_previousOutputPotential;
	return _previousOutputPotential;
	}

