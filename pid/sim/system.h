/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_pid_sim_systemh_
#define _oscl_pid_sim_systemh_

/* Copyright 2003 Michael Nelson Moran
 * This software may be copied and used without restriction.
 */ 

/** */
namespace Oscl {
/** */
namespace PID {
/** */
namespace SIM {

/** This class simulates a "system" whose state is affected
	by its environment and any number of control systems
	that may change the environment over time. The design of
	this class was inspired by the electrical theory surrounding
	the charging and discharging of a capacitor by a voltage
	source (potential) through a resistance. The environment
	in exactly the same way as the various controlling sources,
	that is as a potenial and a series resistance. Although
	this model is based upon electrical engineering, it is
	essentially unit-less and may be applied to similarly behaving
	systems (e.g. thermal systems).
 */
class System {
	private:
		/** Contains the time in seconds between system
			updtates.
		 */
		const float	_tickPeriodInSeconds;

		/** The potential of the system after the previous
			system update. This value is used as the starting
			point for the next system update calculation,
			and is modified appropriately at that time.
		 */
		float		_previousOutputPotential;

	public:
		/** This value represents the default value of the
			resistance between the system and the environmental
			potential.
		 */
		float	_environmentResistance;

		/** This value represents the capacity of the system.
		 	E.g. in an electrical system this is the value of
			the capacitor. It affects the overall "inertia" of
			the simulated system.
		 */
		float	_capacitance;

		/** This value is used during the start() -> accumulate() ->
			finish() sequence to hold the accumulated "equivalent"
			potential of the combined environment and control
			sources. Its value is calculated in the same way as
			the "open-circuit voltage" in a Thevenin equivalent
			circuit.
		 */
		float	_peq;

		/** This value is used during the start() -> accumulate() ->
			finish() sequence to hold the accumulated "equivalent"
			resistance of the combined environment and control
			sources. Its value is calculated in the same way as
			the "equivalent series resistance" in a Thevenin equivalent
			circuit.
		 */
		float	_req;

	public:
		/*
		 * tickPeriodInSeconds is the time between each
		 * call to the tick() operations.
		 * controlResistance is the "internal" resistance
		 * of the control input to othe network.
		 * environmentResistance is the "internal" resistance
		 * of the environment.
		 * capacitance is the amount of charge that can be
		 * stored in the system.
		 * initialOutputPotential is the initial voltage across
		 * the capacitor/system.
		 */
		System(	float	tickPeriodInSeconds,
				float	environmentResistance,
				float	capacitance,
				float	initialOutputPotential
				) noexcept;

		/** This operation is used to start an accumulation cycle.
			It allows several sources to affect the calculation
			of the system output over the next cycle. In this
			variant, the potential is that of the "environment"
			and the resistance is assumed to be that of the
			environment as specified by the constructor.
		 */
		void	start(float potential) noexcept;

		/** This operation is used to start an accumulation cycle.
			It allows several sources to affect the calculation
			of the system output over the next cycle. In this
			variant, the potential is that of the "environment"
			and the resistance is also that of the "environment"
			and is used instead of the constant supplied in the
			constructor.
		 */
		void	start(float potential, float resistance) noexcept;

		/** This operation is to be invoked zero or more times after
			one of the start() operations and before the finish()
			operation. Each invocation accumulates a new potential
			source into the system calculation. This allows any
			number of sources, each consisting of a potential and
			a resistance to affect the next state of the system.
		 */
		void	accumulate(float potential,float resistance) noexcept;

		/** This operation is issued after one of the start() operations
			and zero or more accumulate() operations to calculate the
			new output potential/state of the system, that is returned as
			a result.
		 */
		float	finish() noexcept;
	};

}
}
}

#endif
