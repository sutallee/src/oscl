/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_pid_int16_calch_
#define _oscl_pid_int16_calch_
#include <stdint.h>

/** */
namespace Oscl {
/** */
namespace PID {
/** */
namespace Int16 {

/** */
class Calc {
	private:
		/**	This member represents the desired
			value. This is where we want to be.
		 */
		int16_t		_setpoint;

		/** This member is the difference between
			the setpoint and the last sample.
		 */
		int16_t		_prevErr;

		/** This member is the sum of the errors.
		 */
		int16_t		_ierror;

		/** This member is the constant multiplier
			for the "proportional" term of the PID.
			Fixed point 8.8.
		 */
		const int16_t	_Kp;

		/** This member is the constant multiplier
			for the "derivative" term of the PID.
			Fixed point 8.8.
		 */
		const int16_t	_Kd;

		/** This member is the constant multiplier
			for the "integral" term of the PID.
			Fixed point 8.8.
		 */
		const int16_t	_Ki;

		/** This constant is the maximum
			positive output value of the PID.
		 */
		const int16_t	_maxOutput;

		/** This constant is the minimum
			negative output value of the PID.
		 */
		const int16_t	_minOutput;

	public:
		/** */
		Calc(	int16_t	Kp,
				int16_t	Kd,
				int16_t	Ki,
				int16_t	maxOutput,
				int16_t	minOutput,
				int16_t	setpoint
				) noexcept;

		/** */
		int16_t	tick(int16_t sample) noexcept;

		/** */
		void	change(int16_t setpoint) noexcept;
	};

}
}
}

#endif
