/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "calc.h"

using namespace Oscl::PID::Int16;
using namespace Oscl;

Calc::Calc(	int16_t	Kp,
			int16_t	Kd,
			int16_t	Ki,
			int16_t	maxOutput,
			int16_t	minOutput,
			int16_t	setpoint
			) noexcept:
	_setpoint(setpoint),
	_prevErr(0),
	_ierror(0),
	_Kp(Kp),
	_Kd(Kd),
	_Ki(Ki),
	_maxOutput(maxOutput),
	_minOutput(minOutput)
	{
	}

void	Calc::change(int16_t setpoint) noexcept{
	_setpoint	= setpoint;
	}

int16_t Calc::tick(int16_t sample) noexcept{
	int32_t Perror;
	int32_t proportional;
	int32_t derivative;
	int32_t integral;
	int32_t output;

	// This calculation *could* result in a 17-bit
	// result.
	Perror = _setpoint - sample;

	// Perhaps, I should limit Perror at this
	// point such that it does not exceed
	// a value that will fit in a signed
	// 16 bit field.
	// if(Perror > 32767){
	//	Perror = 32767;
	// else if(Perror < -32767){
	//	Perror	= -32767;
	//	}

	// This calculation *could* result in a
	// 33 bit (17bit*16bit) result.
	proportional	= _Kp*Perror;

	// At this point, if we assume that _Kp
	// is an 8.8 fixed point quantity,
	// we would need to shift the result
	// right by 8.
	proportional	>>= 8;

	// This calculation *could* result in an
	// 18bit result.
	derivative		= Perror - _prevErr;

	// This calculation *could* result in a
	// 34 bit (18bit*16bit) result.
	derivative		*= _Kd;
	derivative		>>= 8;

	integral	= _ierror;

	// This calculatioin *could* result in
	// a 32 bit (16bit*16bit) result if we
	// assume that _ierror is limited to
	// 16 bits.
	integral	*= _Ki;
	integral	>>= 8;

	// This calculation *could* result in a
	// 17 bit result. Assuming that the
	// operands are only 16 bits.
	output	= proportional + derivative;

	// This calculation *could* result in a
	// 18 bit result.
	output	+= integral;

	// Derivative error is the delta Perror over Td
//	output = (_Kp*Perror + _Kd*(Perror - _prevErr) + _Ki*_ierror)/_Ko;

	_prevErr = Perror;

	// Accumulate Integral error *or* Limit output.
	// Stop accumulating when output saturates.
	if (output >= _maxOutput){
		output = _maxOutput;
		}
	else if (output <= _minOutput){
		output = _minOutput;
		}
	else {
		_ierror += Perror;
		}

	return output;
	}

