/*
   Copyright (C) 2011 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_pid_double_calch_
#define _oscl_pid_double_calch_

/** */
namespace Oscl {
/** */
namespace PID {
/** */
namespace Double {

/** */
class Calc {
	private:
		/**	This member represents the desired
			value. This is where we want to be.
		 */
		double		_setpoint;

		/** This member is the difference between
			the setpoint and the last sample.
		 */
		double		_prevErr;

		/** This member is the sum of the errors.
		 */
		double		_ierror;

		/** This member is the constant multiplier
			for the "proportional" term of the PID.
		 */
		double		_Kp;

		/** This member is the constant multiplier
			for the "derivative" term of the PID.
		 */
		double		_Kd;

		/** This member is the constant multiplier
			for the "integral" term of the PID.
		 */
		double		_Ki;

		/** This member is the constant divisor
			for the output term of the PID.
		 */
		double		_Ko;

		/** This is the time between samples.
		 */
		double		_deltaT;

		/** This constant is the maximum magnitude
			of the accumulated integral errors.
			This prevents the integral value from
			"winding-up".
		 */
		double		_iClamp;

		/** This constant is the maximum
			positive output value of the PID.
		 */
		double		_maxOutput;

		/** This constant is the minimum
			negative output value of the PID.
		 */
		double		_minOutput;

	public:
		/** */
		Calc(
			double	Kp,
			double	Ki,
			double	Kd,
			double	Ko,
			double	deltaT,
			double	iClamp,
			double	maxOutput,
			double	minOutput,
			double	setpoint
			) noexcept;

		/** */
		double	tick(double sample) noexcept;

		/** */
		void	reset() noexcept;

		/** */
		void	setSetpoint(double value) noexcept;

		/** */
		void	setKp(double value) noexcept;

		/** */
		void	setKi(double value) noexcept;

		/** */
		void	setKd(double value) noexcept;

		/** */
		void	setKo(double value) noexcept;

		/** */
		void	setPeriod(double value) noexcept;

		/** */
		void	setIClamp(double value) noexcept;

		/** */
		void	setMaxOutput(double value) noexcept;

		/** */
		void	setMinOutput(double value) noexcept;
	};

}
}
}

#endif
