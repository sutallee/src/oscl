/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "calc.h"
#include <stdio.h>

using namespace Oscl::PID::Double;

Calc::Calc(
			double	Kp,
			double	Ki,
			double	Kd,
			double	Ko,
			double	deltaT,
			double	iClamp,
			double	maxOutput,
			double	minOutput,
			double	setpoint
			) noexcept:
		_setpoint(setpoint),
		_prevErr(0),
		_ierror(0),
		_Kp(Kp),
		_Kd(Kd),
		_Ki(Ki),
		_Ko(Ko),
		_deltaT(deltaT),
		_iClamp(iClamp),
		_maxOutput(maxOutput),
		_minOutput(minOutput)
		{
	}

void	Calc::reset() noexcept{
	_prevErr	= 0;
	_ierror		= 0;
	}

void	Calc::setSetpoint(double value) noexcept{
	_setpoint	= value;
	}

double Calc::tick(double sample) noexcept{
	double	Perror;
	double	proportional;
	double	derivative;
	double	integral;
	double	output;

	Perror			= _setpoint - sample;

	proportional	= _Kp*Perror;

	integral		= _ierror + (Perror * _deltaT);

	_ierror 		+= Perror;

	integral		*= _Ki;

	derivative		= (Perror - _prevErr)/_deltaT;

	derivative		*= _Kd;

	output			= proportional + derivative + integral;

	output			/= _Ko;

#if 0
	printf(	"p:%8f + i:%8f + d:%8f => o:%8f\n",
			proportional,
			integral,
			derivative,
			output
			);
#endif

	// Derivative error is the delta Perror over Td
	//	output = (_Kp*Perror + _Kd*(Perror - _prevErr) + _Ki*_ierror)/_Ko;

	_prevErr		= Perror;

	// Accumulate Integral error *or* Limit output.
	// Stop accumulating when output saturates.
	if (output >= _maxOutput){
		output = _maxOutput;
		}
	else if (output <= _minOutput){
		output = _minOutput;
		}

	if(_ierror > _iClamp){
		_ierror	= _iClamp;
		}
	else if(_ierror < -_iClamp) {
		_ierror	= -_iClamp;
		}

	return output;
	}

void	Calc::setKp(double value) noexcept{
	_Kp	= value;
	}

void	Calc::setKi(double value) noexcept{
	_Ki	= value;
	}

void	Calc::setKd(double value) noexcept{
	_Kd	= value;
	}

void	Calc::setKo(double value) noexcept{
	_Ko	= value;
	}

void	Calc::setPeriod(double value) noexcept{
	_deltaT	= value;
	}

void	Calc::setIClamp(double value) noexcept{
	_iClamp	= value;
	}

void	Calc::setMaxOutput(double value) noexcept{
	_maxOutput	= value;
	}

void	Calc::setMinOutput(double value) noexcept{
	_minOutput	= value;
	}

