#ifndef _oscl_pid_double_itc_reqapih_
#define _oscl_pid_double_itc_reqapih_

#include "oscl/mt/itc/mbox/srvreq.h"
#include "oscl/mt/itc/postmsgapi.h"
#include "oscl/mt/itc/mbox/sap.h"

/** */
namespace Oscl {

/** */
namespace PID {

/** */
namespace Double {

/** */
namespace Req {

/**	This class describes an ITC server interface. The
	interface includes the definition of messages and their
	associated payloads, as well as an interface that is to be
	implemented by a server that supports this ITC interface.
 */
/**	
 */
class Api {
	public:
		/** This class contains the data that is transfered
			between the client and server within the SetMinOutputReq
			ITC message.
		 */
		class SetMinOutputPayload {
			public:
				/**	
				 */
				double	_value;

			public:
				/** The SetMinOutputPayload constructor. */
				SetMinOutputPayload(

					double	value
					) noexcept;

			};

		/**	
		 */
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::PID::Double::Req::Api,
					SetMinOutputPayload
					>		SetMinOutputReq;

	public:
		/** This class contains the data that is transfered
			between the client and server within the SetMaxOutputReq
			ITC message.
		 */
		class SetMaxOutputPayload {
			public:
				/**	
				 */
				double	_value;

			public:
				/** The SetMaxOutputPayload constructor. */
				SetMaxOutputPayload(

					double	value
					) noexcept;

			};

		/**	
		 */
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::PID::Double::Req::Api,
					SetMaxOutputPayload
					>		SetMaxOutputReq;

	public:
		/** This class contains the data that is transfered
			between the client and server within the SetIClampReq
			ITC message.
		 */
		class SetIClampPayload {
			public:
				/**	
				 */
				double	_value;

			public:
				/** The SetIClampPayload constructor. */
				SetIClampPayload(

					double	value
					) noexcept;

			};

		/**	
		 */
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::PID::Double::Req::Api,
					SetIClampPayload
					>		SetIClampReq;

	public:
		/** This class contains the data that is transfered
			between the client and server within the SetPeriodReq
			ITC message.
		 */
		class SetPeriodPayload {
			public:
				/**	
				 */
				double	_value;

			public:
				/** The SetPeriodPayload constructor. */
				SetPeriodPayload(

					double	value
					) noexcept;

			};

		/**	
		 */
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::PID::Double::Req::Api,
					SetPeriodPayload
					>		SetPeriodReq;

	public:
		/** This class contains the data that is transfered
			between the client and server within the SetKoReq
			ITC message.
		 */
		class SetKoPayload {
			public:
				/**	
				 */
				double	_value;

			public:
				/** The SetKoPayload constructor. */
				SetKoPayload(

					double	value
					) noexcept;

			};

		/**	
		 */
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::PID::Double::Req::Api,
					SetKoPayload
					>		SetKoReq;

	public:
		/** This class contains the data that is transfered
			between the client and server within the SetKdReq
			ITC message.
		 */
		class SetKdPayload {
			public:
				/**	
				 */
				double	_value;

			public:
				/** The SetKdPayload constructor. */
				SetKdPayload(

					double	value
					) noexcept;

			};

		/**	
		 */
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::PID::Double::Req::Api,
					SetKdPayload
					>		SetKdReq;

	public:
		/** This class contains the data that is transfered
			between the client and server within the SetKiReq
			ITC message.
		 */
		class SetKiPayload {
			public:
				/**	
				 */
				double	_value;

			public:
				/** The SetKiPayload constructor. */
				SetKiPayload(

					double	value
					) noexcept;

			};

		/**	
		 */
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::PID::Double::Req::Api,
					SetKiPayload
					>		SetKiReq;

	public:
		/** This class contains the data that is transfered
			between the client and server within the SetKpReq
			ITC message.
		 */
		class SetKpPayload {
			public:
				/**	
				 */
				double	_value;

			public:
				/** The SetKpPayload constructor. */
				SetKpPayload(

					double	value
					) noexcept;

			};

		/**	
		 */
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::PID::Double::Req::Api,
					SetKpPayload
					>		SetKpReq;

	public:
		/** This class contains the data that is transfered
			between the client and server within the SetSetpointReq
			ITC message.
		 */
		class SetSetpointPayload {
			public:
				/**	
				 */
				double	_value;

			public:
				/** The SetSetpointPayload constructor. */
				SetSetpointPayload(

					double	value
					) noexcept;

			};

		/**	
		 */
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::PID::Double::Req::Api,
					SetSetpointPayload
					>		SetSetpointReq;

	public:
		/** This class contains the data that is transfered
			between the client and server within the ResetReq
			ITC message.
		 */
		class ResetPayload {
			public:
			public:
				/** The ResetPayload constructor. */
				ResetPayload(
					) noexcept;

			};

		/**	
		 */
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::PID::Double::Req::Api,
					ResetPayload
					>		ResetReq;

	public:
		/** This type defines a SAP (Service Access Point) for this
			ITC server interface. A SAP holds server address information
			that is required by clients to send messages to a server.
		 */
		typedef Oscl::Mt::Itc::SAP<	Oscl::PID::Double::
							Req::Api
							>			SAP;

	public:
		/** This type defines a SAP (Service Access Point) for this
			ITC server interface. A SAP holds server address information
			that is required by clients to send messages to a server.
		 */
		typedef Oscl::Mt::Itc::ConcreteSAP<	Oscl::PID::Double::
							Req::Api
							>			ConcreteSAP;

	public:
		/** Make the compiler happy. */
		virtual ~Api(){}

		/** This operation is invoked within the server thread
			whenever a SetMinOutputReq is received.
		 */
		virtual void	request(	Oscl::PID::Double::
									Req::Api::SetMinOutputReq& msg
									) noexcept=0;

		/** This operation is invoked within the server thread
			whenever a SetMaxOutputReq is received.
		 */
		virtual void	request(	Oscl::PID::Double::
									Req::Api::SetMaxOutputReq& msg
									) noexcept=0;

		/** This operation is invoked within the server thread
			whenever a SetIClampReq is received.
		 */
		virtual void	request(	Oscl::PID::Double::
									Req::Api::SetIClampReq& msg
									) noexcept=0;

		/** This operation is invoked within the server thread
			whenever a SetPeriodReq is received.
		 */
		virtual void	request(	Oscl::PID::Double::
									Req::Api::SetPeriodReq& msg
									) noexcept=0;

		/** This operation is invoked within the server thread
			whenever a SetKoReq is received.
		 */
		virtual void	request(	Oscl::PID::Double::
									Req::Api::SetKoReq& msg
									) noexcept=0;

		/** This operation is invoked within the server thread
			whenever a SetKdReq is received.
		 */
		virtual void	request(	Oscl::PID::Double::
									Req::Api::SetKdReq& msg
									) noexcept=0;

		/** This operation is invoked within the server thread
			whenever a SetKiReq is received.
		 */
		virtual void	request(	Oscl::PID::Double::
									Req::Api::SetKiReq& msg
									) noexcept=0;

		/** This operation is invoked within the server thread
			whenever a SetKpReq is received.
		 */
		virtual void	request(	Oscl::PID::Double::
									Req::Api::SetKpReq& msg
									) noexcept=0;

		/** This operation is invoked within the server thread
			whenever a SetSetpointReq is received.
		 */
		virtual void	request(	Oscl::PID::Double::
									Req::Api::SetSetpointReq& msg
									) noexcept=0;

		/** This operation is invoked within the server thread
			whenever a ResetReq is received.
		 */
		virtual void	request(	Oscl::PID::Double::
									Req::Api::ResetReq& msg
									) noexcept=0;

	};

}
}
}
}
#endif
