#include <new>
#include "respmem.h"

using namespace Oscl::PID::Double;

Resp::Api::SetMinOutputResp&
	Resp::SetMinOutputMem::build(	Req::Api::SAP&			sap,
							Resp::Api&				respApi,
							Oscl::Mt::Itc::PostMsgApi&	clientPapi,
							double	value
							) noexcept{
	Req::Api::SetMinOutputPayload*
		p	=	new(&payload)
				Req::Api::SetMinOutputPayload(
							value
							);
	Resp::Api::SetMinOutputResp*
		r	=	new(&resp)
			Resp::Api::SetMinOutputResp(	sap.getReqApi(),
									respApi,
									clientPapi,
									*p
									);

	return *r;
	}

Resp::Api::SetMaxOutputResp&
	Resp::SetMaxOutputMem::build(	Req::Api::SAP&			sap,
							Resp::Api&				respApi,
							Oscl::Mt::Itc::PostMsgApi&	clientPapi,
							double	value
							) noexcept{
	Req::Api::SetMaxOutputPayload*
		p	=	new(&payload)
				Req::Api::SetMaxOutputPayload(
							value
							);
	Resp::Api::SetMaxOutputResp*
		r	=	new(&resp)
			Resp::Api::SetMaxOutputResp(	sap.getReqApi(),
									respApi,
									clientPapi,
									*p
									);

	return *r;
	}

Resp::Api::SetIClampResp&
	Resp::SetIClampMem::build(	Req::Api::SAP&			sap,
							Resp::Api&				respApi,
							Oscl::Mt::Itc::PostMsgApi&	clientPapi,
							double	value
							) noexcept{
	Req::Api::SetIClampPayload*
		p	=	new(&payload)
				Req::Api::SetIClampPayload(
							value
							);
	Resp::Api::SetIClampResp*
		r	=	new(&resp)
			Resp::Api::SetIClampResp(	sap.getReqApi(),
									respApi,
									clientPapi,
									*p
									);

	return *r;
	}

Resp::Api::SetPeriodResp&
	Resp::SetPeriodMem::build(	Req::Api::SAP&			sap,
							Resp::Api&				respApi,
							Oscl::Mt::Itc::PostMsgApi&	clientPapi,
							double	value
							) noexcept{
	Req::Api::SetPeriodPayload*
		p	=	new(&payload)
				Req::Api::SetPeriodPayload(
							value
							);
	Resp::Api::SetPeriodResp*
		r	=	new(&resp)
			Resp::Api::SetPeriodResp(	sap.getReqApi(),
									respApi,
									clientPapi,
									*p
									);

	return *r;
	}

Resp::Api::SetKoResp&
	Resp::SetKoMem::build(	Req::Api::SAP&			sap,
							Resp::Api&				respApi,
							Oscl::Mt::Itc::PostMsgApi&	clientPapi,
							double	value
							) noexcept{
	Req::Api::SetKoPayload*
		p	=	new(&payload)
				Req::Api::SetKoPayload(
							value
							);
	Resp::Api::SetKoResp*
		r	=	new(&resp)
			Resp::Api::SetKoResp(	sap.getReqApi(),
									respApi,
									clientPapi,
									*p
									);

	return *r;
	}

Resp::Api::SetKdResp&
	Resp::SetKdMem::build(	Req::Api::SAP&			sap,
							Resp::Api&				respApi,
							Oscl::Mt::Itc::PostMsgApi&	clientPapi,
							double	value
							) noexcept{
	Req::Api::SetKdPayload*
		p	=	new(&payload)
				Req::Api::SetKdPayload(
							value
							);
	Resp::Api::SetKdResp*
		r	=	new(&resp)
			Resp::Api::SetKdResp(	sap.getReqApi(),
									respApi,
									clientPapi,
									*p
									);

	return *r;
	}

Resp::Api::SetKiResp&
	Resp::SetKiMem::build(	Req::Api::SAP&			sap,
							Resp::Api&				respApi,
							Oscl::Mt::Itc::PostMsgApi&	clientPapi,
							double	value
							) noexcept{
	Req::Api::SetKiPayload*
		p	=	new(&payload)
				Req::Api::SetKiPayload(
							value
							);
	Resp::Api::SetKiResp*
		r	=	new(&resp)
			Resp::Api::SetKiResp(	sap.getReqApi(),
									respApi,
									clientPapi,
									*p
									);

	return *r;
	}

Resp::Api::SetKpResp&
	Resp::SetKpMem::build(	Req::Api::SAP&			sap,
							Resp::Api&				respApi,
							Oscl::Mt::Itc::PostMsgApi&	clientPapi,
							double	value
							) noexcept{
	Req::Api::SetKpPayload*
		p	=	new(&payload)
				Req::Api::SetKpPayload(
							value
							);
	Resp::Api::SetKpResp*
		r	=	new(&resp)
			Resp::Api::SetKpResp(	sap.getReqApi(),
									respApi,
									clientPapi,
									*p
									);

	return *r;
	}

Resp::Api::SetSetpointResp&
	Resp::SetSetpointMem::build(	Req::Api::SAP&			sap,
							Resp::Api&				respApi,
							Oscl::Mt::Itc::PostMsgApi&	clientPapi,
							double	value
							) noexcept{
	Req::Api::SetSetpointPayload*
		p	=	new(&payload)
				Req::Api::SetSetpointPayload(
							value
							);
	Resp::Api::SetSetpointResp*
		r	=	new(&resp)
			Resp::Api::SetSetpointResp(	sap.getReqApi(),
									respApi,
									clientPapi,
									*p
									);

	return *r;
	}

Resp::Api::ResetResp&
	Resp::ResetMem::build(	Req::Api::SAP&			sap,
							Resp::Api&				respApi,
							Oscl::Mt::Itc::PostMsgApi&	clientPapi
							) noexcept{
	Req::Api::ResetPayload*
		p	=	new(&payload)
				Req::Api::ResetPayload(
);
	Resp::Api::ResetResp*
		r	=	new(&resp)
			Resp::Api::ResetResp(	sap.getReqApi(),
									respApi,
									clientPapi,
									*p
									);

	return *r;
	}

