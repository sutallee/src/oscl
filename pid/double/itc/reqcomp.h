#ifndef _oscl_pid_double_reqcomph_
#define _oscl_pid_double_reqcomph_

#include "reqapi.h"

/** */
namespace Oscl {

/** */
namespace PID {

/** */
namespace Double {

/** */
namespace Req {

/**	This template class implements a kind of GOF decorator
	pattern allows the context to employ composition instead
	of inheritance. Frequently, a context may need to implement
	more than one interface that may result in method name
	clashes. This template solves the problem by implementing
	the interface and then invoking member function pointers
	in the context instead. One instance of this object is
	created in the context for each interface of this type used
	in the context. Each instance is constructed such that it
	refers to different member functions within the context.
	The interface of this object is passed to any object that
	requires the interface. When the object invokes any of the
	operations of the API, the corresponding member function
	of the context will subsequently be invoked.
 */
/**	
 */
template <class Context>
class Composer : public Api {
	private:
		/** A reference to the Context.
		 */
		Context&	_context;

	private:
		/**	
		 */
		void	(Context::*_SetMinOutput)(Oscl::PID::Double::Req::Api::SetMinOutputReq& msg);

		/**	
		 */
		void	(Context::*_SetMaxOutput)(Oscl::PID::Double::Req::Api::SetMaxOutputReq& msg);

		/**	
		 */
		void	(Context::*_SetIClamp)(Oscl::PID::Double::Req::Api::SetIClampReq& msg);

		/**	
		 */
		void	(Context::*_SetPeriod)(Oscl::PID::Double::Req::Api::SetPeriodReq& msg);

		/**	
		 */
		void	(Context::*_SetKo)(Oscl::PID::Double::Req::Api::SetKoReq& msg);

		/**	
		 */
		void	(Context::*_SetKd)(Oscl::PID::Double::Req::Api::SetKdReq& msg);

		/**	
		 */
		void	(Context::*_SetKi)(Oscl::PID::Double::Req::Api::SetKiReq& msg);

		/**	
		 */
		void	(Context::*_SetKp)(Oscl::PID::Double::Req::Api::SetKpReq& msg);

		/**	
		 */
		void	(Context::*_SetSetpoint)(Oscl::PID::Double::Req::Api::SetSetpointReq& msg);

		/**	
		 */
		void	(Context::*_Reset)(Oscl::PID::Double::Req::Api::ResetReq& msg);

	public:
		/** */
		Composer(
			Context&		context,
			void	(Context::*SetMinOutput)(Oscl::PID::Double::Req::Api::SetMinOutputReq& msg),
			void	(Context::*SetMaxOutput)(Oscl::PID::Double::Req::Api::SetMaxOutputReq& msg),
			void	(Context::*SetIClamp)(Oscl::PID::Double::Req::Api::SetIClampReq& msg),
			void	(Context::*SetPeriod)(Oscl::PID::Double::Req::Api::SetPeriodReq& msg),
			void	(Context::*SetKo)(Oscl::PID::Double::Req::Api::SetKoReq& msg),
			void	(Context::*SetKd)(Oscl::PID::Double::Req::Api::SetKdReq& msg),
			void	(Context::*SetKi)(Oscl::PID::Double::Req::Api::SetKiReq& msg),
			void	(Context::*SetKp)(Oscl::PID::Double::Req::Api::SetKpReq& msg),
			void	(Context::*SetSetpoint)(Oscl::PID::Double::Req::Api::SetSetpointReq& msg),
			void	(Context::*Reset)(Oscl::PID::Double::Req::Api::ResetReq& msg)
			) noexcept;

	private:
		/**	
		 */
		void	request(Oscl::PID::Double::Req::Api::SetMinOutputReq& msg) noexcept;

		/**	
		 */
		void	request(Oscl::PID::Double::Req::Api::SetMaxOutputReq& msg) noexcept;

		/**	
		 */
		void	request(Oscl::PID::Double::Req::Api::SetIClampReq& msg) noexcept;

		/**	
		 */
		void	request(Oscl::PID::Double::Req::Api::SetPeriodReq& msg) noexcept;

		/**	
		 */
		void	request(Oscl::PID::Double::Req::Api::SetKoReq& msg) noexcept;

		/**	
		 */
		void	request(Oscl::PID::Double::Req::Api::SetKdReq& msg) noexcept;

		/**	
		 */
		void	request(Oscl::PID::Double::Req::Api::SetKiReq& msg) noexcept;

		/**	
		 */
		void	request(Oscl::PID::Double::Req::Api::SetKpReq& msg) noexcept;

		/**	
		 */
		void	request(Oscl::PID::Double::Req::Api::SetSetpointReq& msg) noexcept;

		/**	
		 */
		void	request(Oscl::PID::Double::Req::Api::ResetReq& msg) noexcept;

	};

template <class Context>
Composer<Context>::Composer(
			Context&		context,
			void	(Context::*SetMinOutput)(Oscl::PID::Double::Req::Api::SetMinOutputReq& msg),
			void	(Context::*SetMaxOutput)(Oscl::PID::Double::Req::Api::SetMaxOutputReq& msg),
			void	(Context::*SetIClamp)(Oscl::PID::Double::Req::Api::SetIClampReq& msg),
			void	(Context::*SetPeriod)(Oscl::PID::Double::Req::Api::SetPeriodReq& msg),
			void	(Context::*SetKo)(Oscl::PID::Double::Req::Api::SetKoReq& msg),
			void	(Context::*SetKd)(Oscl::PID::Double::Req::Api::SetKdReq& msg),
			void	(Context::*SetKi)(Oscl::PID::Double::Req::Api::SetKiReq& msg),
			void	(Context::*SetKp)(Oscl::PID::Double::Req::Api::SetKpReq& msg),
			void	(Context::*SetSetpoint)(Oscl::PID::Double::Req::Api::SetSetpointReq& msg),
			void	(Context::*Reset)(Oscl::PID::Double::Req::Api::ResetReq& msg)
			) noexcept:
		_context(context),
		_SetMinOutput(SetMinOutput),
		_SetMaxOutput(SetMaxOutput),
		_SetIClamp(SetIClamp),
		_SetPeriod(SetPeriod),
		_SetKo(SetKo),
		_SetKd(SetKd),
		_SetKi(SetKi),
		_SetKp(SetKp),
		_SetSetpoint(SetSetpoint),
		_Reset(Reset)
		{
	}

template <class Context>
void	Composer<Context>::request(Oscl::PID::Double::Req::Api::SetMinOutputReq& msg) noexcept{
	(_context.*_SetMinOutput)(msg);
	}

template <class Context>
void	Composer<Context>::request(Oscl::PID::Double::Req::Api::SetMaxOutputReq& msg) noexcept{
	(_context.*_SetMaxOutput)(msg);
	}

template <class Context>
void	Composer<Context>::request(Oscl::PID::Double::Req::Api::SetIClampReq& msg) noexcept{
	(_context.*_SetIClamp)(msg);
	}

template <class Context>
void	Composer<Context>::request(Oscl::PID::Double::Req::Api::SetPeriodReq& msg) noexcept{
	(_context.*_SetPeriod)(msg);
	}

template <class Context>
void	Composer<Context>::request(Oscl::PID::Double::Req::Api::SetKoReq& msg) noexcept{
	(_context.*_SetKo)(msg);
	}

template <class Context>
void	Composer<Context>::request(Oscl::PID::Double::Req::Api::SetKdReq& msg) noexcept{
	(_context.*_SetKd)(msg);
	}

template <class Context>
void	Composer<Context>::request(Oscl::PID::Double::Req::Api::SetKiReq& msg) noexcept{
	(_context.*_SetKi)(msg);
	}

template <class Context>
void	Composer<Context>::request(Oscl::PID::Double::Req::Api::SetKpReq& msg) noexcept{
	(_context.*_SetKp)(msg);
	}

template <class Context>
void	Composer<Context>::request(Oscl::PID::Double::Req::Api::SetSetpointReq& msg) noexcept{
	(_context.*_SetSetpoint)(msg);
	}

template <class Context>
void	Composer<Context>::request(Oscl::PID::Double::Req::Api::ResetReq& msg) noexcept{
	(_context.*_Reset)(msg);
	}

}
}
}
}
#endif
