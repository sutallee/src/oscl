#ifndef _oscl_pid_double_respcomph_
#define _oscl_pid_double_respcomph_

#include "respapi.h"

/** */
namespace Oscl {

/** */
namespace PID {

/** */
namespace Double {

/** */
namespace Resp {

/**	This template class implements a kind of GOF decorator
	pattern allows the context to employ composition instead
	of inheritance. Frequently, a context may need to implement
	more than one interface that may result in method name
	clashes. This template solves the problem by implementing
	the interface and then invoking member function pointers
	in the context instead. One instance of this object is
	created in the context for each interface of this type used
	in the context. Each instance is constructed such that it
	refers to different member functions within the context.
	The interface of this object is passed to any object that
	requires the interface. When the object invokes any of the
	operations of the API, the corresponding member function
	of the context will subsequently be invoked.
 */
/**	
 */
template <class Context>
class Composer : public Api {
	private:
		/** A reference to the Context.
		 */
		Context&	_context;

	private:
		/**	
		 */
		void	(Context::*_SetMinOutput)(Oscl::PID::Double::Resp::Api::SetMinOutputResp& msg);

		/**	
		 */
		void	(Context::*_SetMaxOutput)(Oscl::PID::Double::Resp::Api::SetMaxOutputResp& msg);

		/**	
		 */
		void	(Context::*_SetIClamp)(Oscl::PID::Double::Resp::Api::SetIClampResp& msg);

		/**	
		 */
		void	(Context::*_SetPeriod)(Oscl::PID::Double::Resp::Api::SetPeriodResp& msg);

		/**	
		 */
		void	(Context::*_SetKo)(Oscl::PID::Double::Resp::Api::SetKoResp& msg);

		/**	
		 */
		void	(Context::*_SetKd)(Oscl::PID::Double::Resp::Api::SetKdResp& msg);

		/**	
		 */
		void	(Context::*_SetKi)(Oscl::PID::Double::Resp::Api::SetKiResp& msg);

		/**	
		 */
		void	(Context::*_SetKp)(Oscl::PID::Double::Resp::Api::SetKpResp& msg);

		/**	
		 */
		void	(Context::*_SetSetpoint)(Oscl::PID::Double::Resp::Api::SetSetpointResp& msg);

		/**	
		 */
		void	(Context::*_Reset)(Oscl::PID::Double::Resp::Api::ResetResp& msg);

	public:
		/** */
		Composer(
			Context&		context,
			void	(Context::*SetMinOutput)(Oscl::PID::Double::Resp::Api::SetMinOutputResp& msg),
			void	(Context::*SetMaxOutput)(Oscl::PID::Double::Resp::Api::SetMaxOutputResp& msg),
			void	(Context::*SetIClamp)(Oscl::PID::Double::Resp::Api::SetIClampResp& msg),
			void	(Context::*SetPeriod)(Oscl::PID::Double::Resp::Api::SetPeriodResp& msg),
			void	(Context::*SetKo)(Oscl::PID::Double::Resp::Api::SetKoResp& msg),
			void	(Context::*SetKd)(Oscl::PID::Double::Resp::Api::SetKdResp& msg),
			void	(Context::*SetKi)(Oscl::PID::Double::Resp::Api::SetKiResp& msg),
			void	(Context::*SetKp)(Oscl::PID::Double::Resp::Api::SetKpResp& msg),
			void	(Context::*SetSetpoint)(Oscl::PID::Double::Resp::Api::SetSetpointResp& msg),
			void	(Context::*Reset)(Oscl::PID::Double::Resp::Api::ResetResp& msg)
			) noexcept;

	private:
		/**	
		 */
		void	response(Oscl::PID::Double::Resp::Api::SetMinOutputResp& msg) noexcept;

		/**	
		 */
		void	response(Oscl::PID::Double::Resp::Api::SetMaxOutputResp& msg) noexcept;

		/**	
		 */
		void	response(Oscl::PID::Double::Resp::Api::SetIClampResp& msg) noexcept;

		/**	
		 */
		void	response(Oscl::PID::Double::Resp::Api::SetPeriodResp& msg) noexcept;

		/**	
		 */
		void	response(Oscl::PID::Double::Resp::Api::SetKoResp& msg) noexcept;

		/**	
		 */
		void	response(Oscl::PID::Double::Resp::Api::SetKdResp& msg) noexcept;

		/**	
		 */
		void	response(Oscl::PID::Double::Resp::Api::SetKiResp& msg) noexcept;

		/**	
		 */
		void	response(Oscl::PID::Double::Resp::Api::SetKpResp& msg) noexcept;

		/**	
		 */
		void	response(Oscl::PID::Double::Resp::Api::SetSetpointResp& msg) noexcept;

		/**	
		 */
		void	response(Oscl::PID::Double::Resp::Api::ResetResp& msg) noexcept;

	};

template <class Context>
Composer<Context>::Composer(
			Context&		context,
			void	(Context::*SetMinOutput)(Oscl::PID::Double::Resp::Api::SetMinOutputResp& msg),
			void	(Context::*SetMaxOutput)(Oscl::PID::Double::Resp::Api::SetMaxOutputResp& msg),
			void	(Context::*SetIClamp)(Oscl::PID::Double::Resp::Api::SetIClampResp& msg),
			void	(Context::*SetPeriod)(Oscl::PID::Double::Resp::Api::SetPeriodResp& msg),
			void	(Context::*SetKo)(Oscl::PID::Double::Resp::Api::SetKoResp& msg),
			void	(Context::*SetKd)(Oscl::PID::Double::Resp::Api::SetKdResp& msg),
			void	(Context::*SetKi)(Oscl::PID::Double::Resp::Api::SetKiResp& msg),
			void	(Context::*SetKp)(Oscl::PID::Double::Resp::Api::SetKpResp& msg),
			void	(Context::*SetSetpoint)(Oscl::PID::Double::Resp::Api::SetSetpointResp& msg),
			void	(Context::*Reset)(Oscl::PID::Double::Resp::Api::ResetResp& msg)
			) noexcept:
		_context(context),
		_SetMinOutput(SetMinOutput),
		_SetMaxOutput(SetMaxOutput),
		_SetIClamp(SetIClamp),
		_SetPeriod(SetPeriod),
		_SetKo(SetKo),
		_SetKd(SetKd),
		_SetKi(SetKi),
		_SetKp(SetKp),
		_SetSetpoint(SetSetpoint),
		_Reset(Reset)
		{
	}

template <class Context>
void	Composer<Context>::response(Oscl::PID::Double::Resp::Api::SetMinOutputResp& msg) noexcept{
	(_context.*_SetMinOutput)(msg);
	}

template <class Context>
void	Composer<Context>::response(Oscl::PID::Double::Resp::Api::SetMaxOutputResp& msg) noexcept{
	(_context.*_SetMaxOutput)(msg);
	}

template <class Context>
void	Composer<Context>::response(Oscl::PID::Double::Resp::Api::SetIClampResp& msg) noexcept{
	(_context.*_SetIClamp)(msg);
	}

template <class Context>
void	Composer<Context>::response(Oscl::PID::Double::Resp::Api::SetPeriodResp& msg) noexcept{
	(_context.*_SetPeriod)(msg);
	}

template <class Context>
void	Composer<Context>::response(Oscl::PID::Double::Resp::Api::SetKoResp& msg) noexcept{
	(_context.*_SetKo)(msg);
	}

template <class Context>
void	Composer<Context>::response(Oscl::PID::Double::Resp::Api::SetKdResp& msg) noexcept{
	(_context.*_SetKd)(msg);
	}

template <class Context>
void	Composer<Context>::response(Oscl::PID::Double::Resp::Api::SetKiResp& msg) noexcept{
	(_context.*_SetKi)(msg);
	}

template <class Context>
void	Composer<Context>::response(Oscl::PID::Double::Resp::Api::SetKpResp& msg) noexcept{
	(_context.*_SetKp)(msg);
	}

template <class Context>
void	Composer<Context>::response(Oscl::PID::Double::Resp::Api::SetSetpointResp& msg) noexcept{
	(_context.*_SetSetpoint)(msg);
	}

template <class Context>
void	Composer<Context>::response(Oscl::PID::Double::Resp::Api::ResetResp& msg) noexcept{
	(_context.*_Reset)(msg);
	}

}
}
}
}
#endif
