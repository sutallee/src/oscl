#ifndef _oscl_pid_double_composerh_
#define _oscl_pid_double_composerh_

#include "api.h"

/** */
namespace Oscl {

/** */
namespace PID {

/** */
namespace Double {

/**	This template class implements a kind of GOF decorator
	pattern allows the context to employ composition instead
	of inheritance. Frequently, a context may need to implement
	more than one interface that may result in method name
	clashes. This template solves the problem by implementing
	the interface and then invoking member function pointers
	in the context instead. One instance of this object is
	created in the context for each interface of this type used
	in the context. Each instance is constructed such that it
	refers to different member functions within the context.
	The interface of this object is passed to any object that
	requires the interface. When the object invokes any of the
	operations of the API, the corresponding member function
	of the context will subsequently be invoked.
 */
template <class Context>
class Composer : public Api {
	private:
		/** A reference to the Context.
		 */
		Context&	_context;

	private:
		/**	
		 */
		void	(Context::*_setMinOutput)(	
							double	value
							);

		/**	
		 */
		void	(Context::*_setMaxOutput)(	
							double	value
							);

		/**	
		 */
		void	(Context::*_setIClamp)(	
							double	value
							);

		/**	
		 */
		void	(Context::*_setPeriod)(	
							double	value
							);

		/**	
		 */
		void	(Context::*_setKo)(	
							double	value
							);

		/**	
		 */
		void	(Context::*_setKd)(	
							double	value
							);

		/**	
		 */
		void	(Context::*_setKi)(	
							double	value
							);

		/**	
		 */
		void	(Context::*_setKp)(	
							double	value
							);

		/**	
		 */
		void	(Context::*_setSetpoint)(	
							double	value
							);

	public:
		/** */
		Composer(
			Context&		context,
			void	(Context::*setMinOutput)(	
								double	value
								),
			void	(Context::*setMaxOutput)(	
								double	value
								),
			void	(Context::*setIClamp)(	
								double	value
								),
			void	(Context::*setPeriod)(	
								double	value
								),
			void	(Context::*setKo)(	
								double	value
								),
			void	(Context::*setKd)(	
								double	value
								),
			void	(Context::*setKi)(	
								double	value
								),
			void	(Context::*setKp)(	
								double	value
								),
			void	(Context::*setSetpoint)(	
								double	value
								)
			) noexcept;

	private:
		/**	
		 */
		void	setMinOutput(	
							double	value
							) noexcept;

		/**	
		 */
		void	setMaxOutput(	
							double	value
							) noexcept;

		/**	
		 */
		void	setIClamp(	
							double	value
							) noexcept;

		/**	
		 */
		void	setPeriod(	
							double	value
							) noexcept;

		/**	
		 */
		void	setKo(	
							double	value
							) noexcept;

		/**	
		 */
		void	setKd(	
							double	value
							) noexcept;

		/**	
		 */
		void	setKi(	
							double	value
							) noexcept;

		/**	
		 */
		void	setKp(	
							double	value
							) noexcept;

		/**	
		 */
		void	setSetpoint(	
							double	value
							) noexcept;

	};

template <class Context>
Composer<Context>::Composer(
			Context&		context,
			void	(Context::*setMinOutput)(	
								double	value
								),
			void	(Context::*setMaxOutput)(	
								double	value
								),
			void	(Context::*setIClamp)(	
								double	value
								),
			void	(Context::*setPeriod)(	
								double	value
								),
			void	(Context::*setKo)(	
								double	value
								),
			void	(Context::*setKd)(	
								double	value
								),
			void	(Context::*setKi)(	
								double	value
								),
			void	(Context::*setKp)(	
								double	value
								),
			void	(Context::*setSetpoint)(	
								double	value
								)
			) noexcept:
		_context(context),
		_setMinOutput(setMinOutput),
		_setMaxOutput(setMaxOutput),
		_setIClamp(setIClamp),
		_setPeriod(setPeriod),
		_setKo(setKo),
		_setKd(setKd),
		_setKi(setKi),
		_setKp(setKp),
		_setSetpoint(setSetpoint)
		{
	}

template <class Context>
void	Composer<Context>::setMinOutput(	
							double	value
							) noexcept{

	return (_context.*_setMinOutput)(
				value
				);
	}

template <class Context>
void	Composer<Context>::setMaxOutput(	
							double	value
							) noexcept{

	return (_context.*_setMaxOutput)(
				value
				);
	}

template <class Context>
void	Composer<Context>::setIClamp(	
							double	value
							) noexcept{

	return (_context.*_setIClamp)(
				value
				);
	}

template <class Context>
void	Composer<Context>::setPeriod(	
							double	value
							) noexcept{

	return (_context.*_setPeriod)(
				value
				);
	}

template <class Context>
void	Composer<Context>::setKo(	
							double	value
							) noexcept{

	return (_context.*_setKo)(
				value
				);
	}

template <class Context>
void	Composer<Context>::setKd(	
							double	value
							) noexcept{

	return (_context.*_setKd)(
				value
				);
	}

template <class Context>
void	Composer<Context>::setKi(	
							double	value
							) noexcept{

	return (_context.*_setKi)(
				value
				);
	}

template <class Context>
void	Composer<Context>::setKp(	
							double	value
							) noexcept{

	return (_context.*_setKp)(
				value
				);
	}

template <class Context>
void	Composer<Context>::setSetpoint(	
							double	value
							) noexcept{

	return (_context.*_setSetpoint)(
				value
				);
	}

}
}
}
#endif
