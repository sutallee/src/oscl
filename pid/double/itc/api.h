#ifndef _oscl_pid_double_apih_
#define _oscl_pid_double_apih_


/** */
namespace Oscl {

/** */
namespace PID {

/** */
namespace Double {

/**	This class defines the operations that can be performed
	procedurally by the client.
 */
class Api {
	public:
		/** Make the compiler happy.
		 */
		virtual ~Api(){};

		/**	
		 */
		virtual void	setMinOutput(	
							double	value
							) noexcept=0;

		/**	
		 */
		virtual void	setMaxOutput(	
							double	value
							) noexcept=0;

		/**	
		 */
		virtual void	setIClamp(	
							double	value
							) noexcept=0;

		/**	
		 */
		virtual void	setPeriod(	
							double	value
							) noexcept=0;

		/**	
		 */
		virtual void	setKo(	
							double	value
							) noexcept=0;

		/**	
		 */
		virtual void	setKd(	
							double	value
							) noexcept=0;

		/**	
		 */
		virtual void	setKi(	
							double	value
							) noexcept=0;

		/**	
		 */
		virtual void	setKp(	
							double	value
							) noexcept=0;

		/**	
		 */
		virtual void	setSetpoint(	
							double	value
							) noexcept=0;

	};

}
}
}
#endif
