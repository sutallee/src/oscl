#ifndef _oscl_pid_double_itc_respmemh_
#define _oscl_pid_double_itc_respmemh_

#include "respapi.h"
#include "oscl/memory/block.h"

/**	These record types in this file describe blocks of memory
	that are large enough and appropriately aligned such that
	placement new operations creating instances of their
	corresponding objects can safely be performed. These records
	are useful to most clients that use the server's ITC
	interface asynchronously.
 */

/** */
namespace Oscl {

/** */
namespace PID {

/** */
namespace Double {

/** */
namespace Resp {

/**	This record describes a block of memory that is
	large enough (and properly aligned) to hold a
	SetMinOutputResp and its corresponding Req::Api::SetMinOutputPayload.
	Clients which use the corresponding server asynchronously
	frequently need to reservere/manage memory for for these
	ITC interactions and instances of this record are useful
	for that purpose.
 */
struct SetMinOutputMem {
	/**	This is memory enough for the response message.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Resp::Api::SetMinOutputResp)>	resp;

	/**	This is memory enough for the message payload.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Req::Api::SetMinOutputPayload)>	payload;

	/**	This operation creates a response message using this
		 block of memory.
	 */
	Oscl::PID::Double::Resp::Api::SetMinOutputResp&
		build(	Oscl::PID::Double::Req::Api::SAP&	sap,
				Oscl::PID::Double::Resp::Api&		respApi,
				Oscl::Mt::Itc::PostMsgApi&		clientPapi,
				double	value
				) noexcept;

	};

/**	This record describes a block of memory that is
	large enough (and properly aligned) to hold a
	SetMaxOutputResp and its corresponding Req::Api::SetMaxOutputPayload.
	Clients which use the corresponding server asynchronously
	frequently need to reservere/manage memory for for these
	ITC interactions and instances of this record are useful
	for that purpose.
 */
struct SetMaxOutputMem {
	/**	This is memory enough for the response message.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Resp::Api::SetMaxOutputResp)>	resp;

	/**	This is memory enough for the message payload.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Req::Api::SetMaxOutputPayload)>	payload;

	/**	This operation creates a response message using this
		 block of memory.
	 */
	Oscl::PID::Double::Resp::Api::SetMaxOutputResp&
		build(	Oscl::PID::Double::Req::Api::SAP&	sap,
				Oscl::PID::Double::Resp::Api&		respApi,
				Oscl::Mt::Itc::PostMsgApi&		clientPapi,
				double	value
				) noexcept;

	};

/**	This record describes a block of memory that is
	large enough (and properly aligned) to hold a
	SetIClampResp and its corresponding Req::Api::SetIClampPayload.
	Clients which use the corresponding server asynchronously
	frequently need to reservere/manage memory for for these
	ITC interactions and instances of this record are useful
	for that purpose.
 */
struct SetIClampMem {
	/**	This is memory enough for the response message.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Resp::Api::SetIClampResp)>	resp;

	/**	This is memory enough for the message payload.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Req::Api::SetIClampPayload)>	payload;

	/**	This operation creates a response message using this
		 block of memory.
	 */
	Oscl::PID::Double::Resp::Api::SetIClampResp&
		build(	Oscl::PID::Double::Req::Api::SAP&	sap,
				Oscl::PID::Double::Resp::Api&		respApi,
				Oscl::Mt::Itc::PostMsgApi&		clientPapi,
				double	value
				) noexcept;

	};

/**	This record describes a block of memory that is
	large enough (and properly aligned) to hold a
	SetPeriodResp and its corresponding Req::Api::SetPeriodPayload.
	Clients which use the corresponding server asynchronously
	frequently need to reservere/manage memory for for these
	ITC interactions and instances of this record are useful
	for that purpose.
 */
struct SetPeriodMem {
	/**	This is memory enough for the response message.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Resp::Api::SetPeriodResp)>	resp;

	/**	This is memory enough for the message payload.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Req::Api::SetPeriodPayload)>	payload;

	/**	This operation creates a response message using this
		 block of memory.
	 */
	Oscl::PID::Double::Resp::Api::SetPeriodResp&
		build(	Oscl::PID::Double::Req::Api::SAP&	sap,
				Oscl::PID::Double::Resp::Api&		respApi,
				Oscl::Mt::Itc::PostMsgApi&		clientPapi,
				double	value
				) noexcept;

	};

/**	This record describes a block of memory that is
	large enough (and properly aligned) to hold a
	SetKoResp and its corresponding Req::Api::SetKoPayload.
	Clients which use the corresponding server asynchronously
	frequently need to reservere/manage memory for for these
	ITC interactions and instances of this record are useful
	for that purpose.
 */
struct SetKoMem {
	/**	This is memory enough for the response message.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Resp::Api::SetKoResp)>	resp;

	/**	This is memory enough for the message payload.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Req::Api::SetKoPayload)>	payload;

	/**	This operation creates a response message using this
		 block of memory.
	 */
	Oscl::PID::Double::Resp::Api::SetKoResp&
		build(	Oscl::PID::Double::Req::Api::SAP&	sap,
				Oscl::PID::Double::Resp::Api&		respApi,
				Oscl::Mt::Itc::PostMsgApi&		clientPapi,
				double	value
				) noexcept;

	};

/**	This record describes a block of memory that is
	large enough (and properly aligned) to hold a
	SetKdResp and its corresponding Req::Api::SetKdPayload.
	Clients which use the corresponding server asynchronously
	frequently need to reservere/manage memory for for these
	ITC interactions and instances of this record are useful
	for that purpose.
 */
struct SetKdMem {
	/**	This is memory enough for the response message.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Resp::Api::SetKdResp)>	resp;

	/**	This is memory enough for the message payload.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Req::Api::SetKdPayload)>	payload;

	/**	This operation creates a response message using this
		 block of memory.
	 */
	Oscl::PID::Double::Resp::Api::SetKdResp&
		build(	Oscl::PID::Double::Req::Api::SAP&	sap,
				Oscl::PID::Double::Resp::Api&		respApi,
				Oscl::Mt::Itc::PostMsgApi&		clientPapi,
				double	value
				) noexcept;

	};

/**	This record describes a block of memory that is
	large enough (and properly aligned) to hold a
	SetKiResp and its corresponding Req::Api::SetKiPayload.
	Clients which use the corresponding server asynchronously
	frequently need to reservere/manage memory for for these
	ITC interactions and instances of this record are useful
	for that purpose.
 */
struct SetKiMem {
	/**	This is memory enough for the response message.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Resp::Api::SetKiResp)>	resp;

	/**	This is memory enough for the message payload.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Req::Api::SetKiPayload)>	payload;

	/**	This operation creates a response message using this
		 block of memory.
	 */
	Oscl::PID::Double::Resp::Api::SetKiResp&
		build(	Oscl::PID::Double::Req::Api::SAP&	sap,
				Oscl::PID::Double::Resp::Api&		respApi,
				Oscl::Mt::Itc::PostMsgApi&		clientPapi,
				double	value
				) noexcept;

	};

/**	This record describes a block of memory that is
	large enough (and properly aligned) to hold a
	SetKpResp and its corresponding Req::Api::SetKpPayload.
	Clients which use the corresponding server asynchronously
	frequently need to reservere/manage memory for for these
	ITC interactions and instances of this record are useful
	for that purpose.
 */
struct SetKpMem {
	/**	This is memory enough for the response message.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Resp::Api::SetKpResp)>	resp;

	/**	This is memory enough for the message payload.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Req::Api::SetKpPayload)>	payload;

	/**	This operation creates a response message using this
		 block of memory.
	 */
	Oscl::PID::Double::Resp::Api::SetKpResp&
		build(	Oscl::PID::Double::Req::Api::SAP&	sap,
				Oscl::PID::Double::Resp::Api&		respApi,
				Oscl::Mt::Itc::PostMsgApi&		clientPapi,
				double	value
				) noexcept;

	};

/**	This record describes a block of memory that is
	large enough (and properly aligned) to hold a
	SetSetpointResp and its corresponding Req::Api::SetSetpointPayload.
	Clients which use the corresponding server asynchronously
	frequently need to reservere/manage memory for for these
	ITC interactions and instances of this record are useful
	for that purpose.
 */
struct SetSetpointMem {
	/**	This is memory enough for the response message.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Resp::Api::SetSetpointResp)>	resp;

	/**	This is memory enough for the message payload.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Req::Api::SetSetpointPayload)>	payload;

	/**	This operation creates a response message using this
		 block of memory.
	 */
	Oscl::PID::Double::Resp::Api::SetSetpointResp&
		build(	Oscl::PID::Double::Req::Api::SAP&	sap,
				Oscl::PID::Double::Resp::Api&		respApi,
				Oscl::Mt::Itc::PostMsgApi&		clientPapi,
				double	value
				) noexcept;

	};

/**	This record describes a block of memory that is
	large enough (and properly aligned) to hold a
	ResetResp and its corresponding Req::Api::ResetPayload.
	Clients which use the corresponding server asynchronously
	frequently need to reservere/manage memory for for these
	ITC interactions and instances of this record are useful
	for that purpose.
 */
struct ResetMem {
	/**	This is memory enough for the response message.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Resp::Api::ResetResp)>	resp;

	/**	This is memory enough for the message payload.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Req::Api::ResetPayload)>	payload;

	/**	This operation creates a response message using this
		 block of memory.
	 */
	Oscl::PID::Double::Resp::Api::ResetResp&
		build(	Oscl::PID::Double::Req::Api::SAP&	sap,
				Oscl::PID::Double::Resp::Api&		respApi,
				Oscl::Mt::Itc::PostMsgApi&		clientPapi
				) noexcept;

	};

}
}
}
}
#endif
