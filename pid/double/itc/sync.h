#ifndef _oscl_pid_double_itc_synch_
#define _oscl_pid_double_itc_synch_

#include "api.h"
#include "reqapi.h"

/** */
namespace Oscl {

/** */
namespace PID {

/** */
namespace Double {

/**	This class implements synchronous ITC for the interface.
 */
class Sync : public Api {
	private:
		/** This SAP is used to identify the server.
		 */
		Oscl::PID::Double::Req::Api::ConcreteSAP	_sap;

	public:
		/** The constructor requires a reference to the server's
			thread/mailbox (myPapi), and a reference to the server's
			request interface.
		 */
		Sync(	Oscl::Mt::Itc::PostMsgApi&	myPapi,
				Req::Api&				reqApi
				) noexcept;

		/** It is frequently the case that this Sync class is declared
			within a server class. Since the server's clients need access
			to the server's SAP, and since the Sync class implementation
			needs the same information, this operation exports the SAP
			to the server/context such that it can be distributed to clients
			that use the server asynchronously. In this way, there is
			only a single copy of the SAP for each service.
		 */
		Req::Api::SAP&	getSAP() noexcept;

	public:
		/**	
		 */
		void	setMinOutput(	
						double	value
						) noexcept;

		/**	
		 */
		void	setMaxOutput(	
						double	value
						) noexcept;

		/**	
		 */
		void	setIClamp(	
						double	value
						) noexcept;

		/**	
		 */
		void	setPeriod(	
						double	value
						) noexcept;

		/**	
		 */
		void	setKo(	
						double	value
						) noexcept;

		/**	
		 */
		void	setKd(	
						double	value
						) noexcept;

		/**	
		 */
		void	setKi(	
						double	value
						) noexcept;

		/**	
		 */
		void	setKp(	
						double	value
						) noexcept;

		/**	
		 */
		void	setSetpoint(	
						double	value
						) noexcept;

	};

}
}
}
#endif
