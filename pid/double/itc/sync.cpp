#include "sync.h"
#include "oscl/mt/itc/mbox/syncrh.h"

using namespace Oscl::PID::Double;

Sync::Sync(	Oscl::Mt::Itc::PostMsgApi&	myPapi,
			Req::Api&				reqApi
			) noexcept:
		_sap(reqApi,myPapi)
		{
	}

Req::Api::SAP&	Sync::getSAP() noexcept{
	return _sap;
	}

void	Sync::setMinOutput(	
						double	value
						) noexcept{

	Oscl::Mt::Itc::SyncReturnHandler		srh;

	Req::Api::SetMinOutputPayload		payload(
									value
									);

	Req::Api::SetMinOutputReq	req(	_sap.getReqApi(),
									payload,
									srh
									);

	_sap.postSync(req);
	}

void	Sync::setMaxOutput(	
						double	value
						) noexcept{

	Oscl::Mt::Itc::SyncReturnHandler		srh;

	Req::Api::SetMaxOutputPayload		payload(
									value
									);

	Req::Api::SetMaxOutputReq	req(	_sap.getReqApi(),
									payload,
									srh
									);

	_sap.postSync(req);
	}

void	Sync::setIClamp(	
						double	value
						) noexcept{

	Oscl::Mt::Itc::SyncReturnHandler		srh;

	Req::Api::SetIClampPayload		payload(
									value
									);

	Req::Api::SetIClampReq	req(	_sap.getReqApi(),
									payload,
									srh
									);

	_sap.postSync(req);
	}

void	Sync::setPeriod(	
						double	value
						) noexcept{

	Oscl::Mt::Itc::SyncReturnHandler		srh;

	Req::Api::SetPeriodPayload		payload(
									value
									);

	Req::Api::SetPeriodReq	req(	_sap.getReqApi(),
									payload,
									srh
									);

	_sap.postSync(req);
	}

void	Sync::setKo(	
						double	value
						) noexcept{

	Oscl::Mt::Itc::SyncReturnHandler		srh;

	Req::Api::SetKoPayload		payload(
									value
									);

	Req::Api::SetKoReq	req(	_sap.getReqApi(),
									payload,
									srh
									);

	_sap.postSync(req);
	}

void	Sync::setKd(	
						double	value
						) noexcept{

	Oscl::Mt::Itc::SyncReturnHandler		srh;

	Req::Api::SetKdPayload		payload(
									value
									);

	Req::Api::SetKdReq	req(	_sap.getReqApi(),
									payload,
									srh
									);

	_sap.postSync(req);
	}

void	Sync::setKi(	
						double	value
						) noexcept{

	Oscl::Mt::Itc::SyncReturnHandler		srh;

	Req::Api::SetKiPayload		payload(
									value
									);

	Req::Api::SetKiReq	req(	_sap.getReqApi(),
									payload,
									srh
									);

	_sap.postSync(req);
	}

void	Sync::setKp(	
						double	value
						) noexcept{

	Oscl::Mt::Itc::SyncReturnHandler		srh;

	Req::Api::SetKpPayload		payload(
									value
									);

	Req::Api::SetKpReq	req(	_sap.getReqApi(),
									payload,
									srh
									);

	_sap.postSync(req);
	}

void	Sync::setSetpoint(	
						double	value
						) noexcept{

	Oscl::Mt::Itc::SyncReturnHandler		srh;

	Req::Api::SetSetpointPayload		payload(
									value
									);

	Req::Api::SetSetpointReq	req(	_sap.getReqApi(),
									payload,
									srh
									);

	_sap.postSync(req);
	}

