#ifndef _oscl_pid_double_itc_respapih_
#define _oscl_pid_double_itc_respapih_

#include "reqapi.h"
#include "oscl/mt/itc/mbox/clirsp.h"

/** */
namespace Oscl {

/** */
namespace PID {

/** */
namespace Double {

/** */
namespace Resp {

/**	This class describes an ITC client interface. The
	interface includes the definition of client response messages
	based on their corresponding server request messages and
	associated payloads, as well as an interface that is to be
	implemented by a client that uses the corresponding server
	asynchronously.
 */
/**	
 */
class Api {
	public:
		/**	This describes a client response message that corresponds
			to the SetMinOutputReq message described in the "reqapi.h"
			header file. This SetMinOutputResp response message actually
			contains a SetMinOutputReq request message.
		 */
		typedef Oscl::Mt::Itc::CliResponse<
					Oscl::PID::Double::Req::Api,
					Api,
					Oscl::PID::Double::
					Req::Api::SetMinOutputPayload
					>		SetMinOutputResp;

	public:
		/**	This describes a client response message that corresponds
			to the SetMaxOutputReq message described in the "reqapi.h"
			header file. This SetMaxOutputResp response message actually
			contains a SetMaxOutputReq request message.
		 */
		typedef Oscl::Mt::Itc::CliResponse<
					Oscl::PID::Double::Req::Api,
					Api,
					Oscl::PID::Double::
					Req::Api::SetMaxOutputPayload
					>		SetMaxOutputResp;

	public:
		/**	This describes a client response message that corresponds
			to the SetIClampReq message described in the "reqapi.h"
			header file. This SetIClampResp response message actually
			contains a SetIClampReq request message.
		 */
		typedef Oscl::Mt::Itc::CliResponse<
					Oscl::PID::Double::Req::Api,
					Api,
					Oscl::PID::Double::
					Req::Api::SetIClampPayload
					>		SetIClampResp;

	public:
		/**	This describes a client response message that corresponds
			to the SetPeriodReq message described in the "reqapi.h"
			header file. This SetPeriodResp response message actually
			contains a SetPeriodReq request message.
		 */
		typedef Oscl::Mt::Itc::CliResponse<
					Oscl::PID::Double::Req::Api,
					Api,
					Oscl::PID::Double::
					Req::Api::SetPeriodPayload
					>		SetPeriodResp;

	public:
		/**	This describes a client response message that corresponds
			to the SetKoReq message described in the "reqapi.h"
			header file. This SetKoResp response message actually
			contains a SetKoReq request message.
		 */
		typedef Oscl::Mt::Itc::CliResponse<
					Oscl::PID::Double::Req::Api,
					Api,
					Oscl::PID::Double::
					Req::Api::SetKoPayload
					>		SetKoResp;

	public:
		/**	This describes a client response message that corresponds
			to the SetKdReq message described in the "reqapi.h"
			header file. This SetKdResp response message actually
			contains a SetKdReq request message.
		 */
		typedef Oscl::Mt::Itc::CliResponse<
					Oscl::PID::Double::Req::Api,
					Api,
					Oscl::PID::Double::
					Req::Api::SetKdPayload
					>		SetKdResp;

	public:
		/**	This describes a client response message that corresponds
			to the SetKiReq message described in the "reqapi.h"
			header file. This SetKiResp response message actually
			contains a SetKiReq request message.
		 */
		typedef Oscl::Mt::Itc::CliResponse<
					Oscl::PID::Double::Req::Api,
					Api,
					Oscl::PID::Double::
					Req::Api::SetKiPayload
					>		SetKiResp;

	public:
		/**	This describes a client response message that corresponds
			to the SetKpReq message described in the "reqapi.h"
			header file. This SetKpResp response message actually
			contains a SetKpReq request message.
		 */
		typedef Oscl::Mt::Itc::CliResponse<
					Oscl::PID::Double::Req::Api,
					Api,
					Oscl::PID::Double::
					Req::Api::SetKpPayload
					>		SetKpResp;

	public:
		/**	This describes a client response message that corresponds
			to the SetSetpointReq message described in the "reqapi.h"
			header file. This SetSetpointResp response message actually
			contains a SetSetpointReq request message.
		 */
		typedef Oscl::Mt::Itc::CliResponse<
					Oscl::PID::Double::Req::Api,
					Api,
					Oscl::PID::Double::
					Req::Api::SetSetpointPayload
					>		SetSetpointResp;

	public:
		/**	This describes a client response message that corresponds
			to the ResetReq message described in the "reqapi.h"
			header file. This ResetResp response message actually
			contains a ResetReq request message.
		 */
		typedef Oscl::Mt::Itc::CliResponse<
					Oscl::PID::Double::Req::Api,
					Api,
					Oscl::PID::Double::
					Req::Api::ResetPayload
					>		ResetResp;

	public:
		/** Make the compiler happy. */
		virtual ~Api(){}

		/** This operation is invoked within the client thread
			whenever a SetMinOutputResp message is received.
		 */
		virtual void	response(	Oscl::PID::Double::
									Resp::Api::SetMinOutputResp& msg
									) noexcept=0;

		/** This operation is invoked within the client thread
			whenever a SetMaxOutputResp message is received.
		 */
		virtual void	response(	Oscl::PID::Double::
									Resp::Api::SetMaxOutputResp& msg
									) noexcept=0;

		/** This operation is invoked within the client thread
			whenever a SetIClampResp message is received.
		 */
		virtual void	response(	Oscl::PID::Double::
									Resp::Api::SetIClampResp& msg
									) noexcept=0;

		/** This operation is invoked within the client thread
			whenever a SetPeriodResp message is received.
		 */
		virtual void	response(	Oscl::PID::Double::
									Resp::Api::SetPeriodResp& msg
									) noexcept=0;

		/** This operation is invoked within the client thread
			whenever a SetKoResp message is received.
		 */
		virtual void	response(	Oscl::PID::Double::
									Resp::Api::SetKoResp& msg
									) noexcept=0;

		/** This operation is invoked within the client thread
			whenever a SetKdResp message is received.
		 */
		virtual void	response(	Oscl::PID::Double::
									Resp::Api::SetKdResp& msg
									) noexcept=0;

		/** This operation is invoked within the client thread
			whenever a SetKiResp message is received.
		 */
		virtual void	response(	Oscl::PID::Double::
									Resp::Api::SetKiResp& msg
									) noexcept=0;

		/** This operation is invoked within the client thread
			whenever a SetKpResp message is received.
		 */
		virtual void	response(	Oscl::PID::Double::
									Resp::Api::SetKpResp& msg
									) noexcept=0;

		/** This operation is invoked within the client thread
			whenever a SetSetpointResp message is received.
		 */
		virtual void	response(	Oscl::PID::Double::
									Resp::Api::SetSetpointResp& msg
									) noexcept=0;

		/** This operation is invoked within the client thread
			whenever a ResetResp message is received.
		 */
		virtual void	response(	Oscl::PID::Double::
									Resp::Api::ResetResp& msg
									) noexcept=0;

	};

}
}
}
}
#endif
