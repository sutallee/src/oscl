#include "reqapi.h"

using namespace Oscl::PID::Double;

Oscl::PID::Double::Req::Api::SetMinOutputPayload::
SetMinOutputPayload(
		double	value
		) noexcept:
		_value(value)
		{}

Oscl::PID::Double::Req::Api::SetMaxOutputPayload::
SetMaxOutputPayload(
		double	value
		) noexcept:
		_value(value)
		{}

Oscl::PID::Double::Req::Api::SetIClampPayload::
SetIClampPayload(
		double	value
		) noexcept:
		_value(value)
		{}

Oscl::PID::Double::Req::Api::SetPeriodPayload::
SetPeriodPayload(
		double	value
		) noexcept:
		_value(value)
		{}

Oscl::PID::Double::Req::Api::SetKoPayload::
SetKoPayload(
		double	value
		) noexcept:
		_value(value)
		{}

Oscl::PID::Double::Req::Api::SetKdPayload::
SetKdPayload(
		double	value
		) noexcept:
		_value(value)
		{}

Oscl::PID::Double::Req::Api::SetKiPayload::
SetKiPayload(
		double	value
		) noexcept:
		_value(value)
		{}

Oscl::PID::Double::Req::Api::SetKpPayload::
SetKpPayload(
		double	value
		) noexcept:
		_value(value)
		{}

Oscl::PID::Double::Req::Api::SetSetpointPayload::
SetSetpointPayload(
		double	value
		) noexcept:
		_value(value)
		{}

Oscl::PID::Double::Req::Api::ResetPayload::
ResetPayload(
		) noexcept
		{}

