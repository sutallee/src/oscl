/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_pid_int16_calch_
#define _oscl_pid_int16_calch_
#include <stdint.h>

/** */
namespace Oscl {
/** */
namespace PID {
/** */
namespace Normal {

/** This class implements a PID (Proportional Integral Differential)
	closed loop control algorithm. The input and output of the algorithm
	are normalized fixed point numbers that may be scaled by the
	application as appropriate.
 */
class Calc {
	private:
		/**	This member is a positive binary fixed point number
			with a value between 0.000000000000000 and
			0.111111111111111 inclusive.
			Note that this number *must*
			always be positive. The idea is that it
			represents a "percentage" of full scale.
			Note that full scale cannot be represented,
			however, a very close approximation of full
			scale is possible.
		 */
		int16_t		_setpoint;

		/** This member is the difference between
			the setpoint and the last sample. It is
			a binary fixed point number with a value between
			0.111111111111111 and -0.111111111111111
			inclusive.
		 */
		int16_t		_prevErr;

		/** This member is the integral sum of the errors.
		 */
		int16_t		_ierror;

		/** This member is the constant multiplier
			for the "proportional" term of the PID.
		 */
		const int16_t	_Kp;

		/** This member is the constant multiplier
			for the "derivative" term of the PID.
		 */
		const int16_t	_Kd;

		/** This member is the constant multiplier
			for the "integral" term of the PID.
		 */
		const int16_t	_Ki;

	public:
		/** */
		Calc(	int16_t	Kp,
				int16_t	Kd,
				int16_t	Ki,
				int16_t	setpoint
				) noexcept;

		/** */
		int16_t	tick(int16_t sample) noexcept;

		/** */
		void	change(int16_t setpoint) noexcept;
	};

}
}
}

#endif
