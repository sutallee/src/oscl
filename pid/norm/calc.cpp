/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "calc.h"

using namespace Oscl::PID::Normal;
using namespace Oscl;

Calc::Calc(	int16_t	Kp,
			int16_t	Kd,
			int16_t	Ki,
			int16_t	setpoint
			) noexcept:
	_setpoint(setpoint),
	_prevErr(0),
	_ierror(0),
	_Kp(Kp),
	_Kd(Kd),
	_Ki(Ki)
	{
	}

void	Calc::change(int16_t setpoint) noexcept{
	if(setpoint < 0){
		while(true);
		}
	_setpoint	= setpoint;
	}

int16_t Calc::tick(int16_t sample) noexcept{
//	if(sample < 0){
//		while(true);
//		}
	int32_t Perror;
	int32_t output;

	int32_t setpoint	= _setpoint;
	int32_t esample	= sample;

	Perror = setpoint - esample;
	Perror	>>= 1;

	int32_t	proportional;
	proportional	= _Kp*Perror;
	proportional	>>= 8;

	int32_t	derivative;
	derivative	= (Perror-_prevErr);
	if(derivative > 32767){
		derivative	= 32767;
		}
	else if(derivative < -32767){
		derivative	= -32767;
		}
	derivative	*= _Kd;
	derivative	>>= 8;

	int32_t	integral;
	integral	= (_Ki*_ierror);
	integral	>>= 8;

	output	= proportional + derivative;

	output	+= integral;

	_prevErr = Perror;

	// Accumulate Integral error *or* Limit output.
	// Stop accumulating when output saturates.
	if (output >= 32767){
		output = 32767;
		}
	else if (output <= -32767){
		output = -32767;
		}
	else {
		int32_t	ierror	= _ierror;
		ierror 	+= Perror;
		if(ierror > 32767){
			ierror	= 32767;
			}
		else if(ierror < -32767){
			ierror	= -32767;
			}
		_ierror = (int16_t)ierror;
		}

	return (int16_t)output;
	}

