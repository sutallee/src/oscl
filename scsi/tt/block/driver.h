/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_scsi_tt_block_driverh_
#define _oscl_scsi_tt_block_driverh_
#include "oscl/tt/itc/respmem.h"
#include "oscl/block/read/reqapi.h"
#include "oscl/scsi/cmd/read10.h"
#include "oscl/scsi/cmd/reqsense.h"

/** */
namespace Oscl {
/** */
namespace SCSI {
/** */
namespace TT {
/** */
namespace Block {

/** */
class Driver :	Oscl::TT::ITC::Resp::Api,
				Oscl::Block::Read::Req::Api<512>
				 {
	private:
		/** */
		union CommandMem {
			/** */
			Oscl::Memory::
			AlignedBlock< sizeof(Oscl::SCSI::CMD::Read10::Request) >	read10;
			};
		/** */
		struct ReqMem {
			/** */
			CommandMem						cmd;
			/** */
			Oscl::TT::ITC::Resp::Api::Mem	tt;
			};
		/** */
		union {
			ReqMem		tt;
			} reqMem;

		/** */
		Oscl::TT::ITC::Resp::Api::SAP&		_ttSAP;

		/** */
		const unsigned long					_lbaOffset;

	public:
		/** */
		Driver(	Oscl::TT::ITC::Resp::Api::SAP&	ttSAP,
				unsigned long					lbaOffset
				) noexcept;
		/** */
		Oscl::Block::Read::Req::Api<512>&	getSAP() noexcept;
		/** */
		Oscl::Block::Read::Api<512>&		getSyncApi() noexcept;
		
	private:
		/** */
		void	request(Oscl::Block::Read::Req::Api<512>::ReadReq& msg) noexcept;
	private:
		/** not-used */
		void	response(Oscl::TT::ITC::Resp::Api::ResetResp& msg) noexcept;
		/** not-used */
		void	response(Oscl::TT::ITC::Resp::Api::GetMaxLunResp& msg) noexcept;
		/** not-used yet */
		void	response(Oscl::TT::ITC::Resp::Api::CancelResp& msg) noexcept;
		/** */
		void	response(Oscl::TT::ITC::Resp::Api::ReadResp& msg) noexcept;
		/** not-used yet */
		void	response(Oscl::TT::ITC::Resp::Api::WriteResp& msg) noexcept;
		/** not-used */
		void	response(Oscl::TT::ITC::Resp::Api::CommandResp& msg) noexcept;
	private:
		/** */
		void	startRead(	unsigned long	block,
							unsigned long	nBlocks,
							void*			buffer
							) noexcept;
	};

}
}
}
}

#endif
