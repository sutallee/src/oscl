/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_scsi_cmd_write10h_
#define _oscl_scsi_cmd_write10h_
#include <stdint.h>

/** */
namespace Oscl {
/** */
namespace SCSI {
/** */
namespace CMD {
/** */
namespace Write10 {
/** */
struct Request {
	/** */
	const uint8_t	_opcode;
	/** */
	uint8_t		_flags;
	/** */
	uint8_t		_lba[4];
	/** */
	uint8_t		_reserved6;
	/** */
	uint8_t		_transferLength[2];
	/** */
	uint8_t		_control;
	/** */
	Request(	uint8_t	flags,
				uint32_t	lba,
				uint16_t	transferLength,
				uint8_t	control
				) noexcept:
		_opcode(0x2A),
		_flags(flags),
		_reserved6(0),
		_control(control) {
		_transferLength[1]	= (uint8_t)(transferLength >> 0);
		_transferLength[0]	= (uint8_t)(transferLength >> 8);
		_lba[3]	= (uint8_t)(lba >> 0);
		_lba[2]	= (uint8_t)(lba >> 8);
		_lba[1]	= (uint8_t)(lba >> 16);
		_lba[0]	= (uint8_t)(lba >> 24);
		}
	};
/** */
struct Standard : public Request {
	/** */
	Standard(	uint32_t	lba,
				uint16_t	transferLength
				) noexcept:
		Request(0,lba,transferLength,0)
		{ }
	};
}

}
}
}

#endif
