/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_scsi_cmd_reqsenseh_
#define _oscl_scsi_cmd_reqsenseh_
#include <stdint.h>

/** */
namespace Oscl {
/** */
namespace SCSI {
/** */
namespace CMD {

/** NOT in RBC */
namespace RequestSense {
/** */
struct Request {
	/** */
	const uint8_t	_opcode;
	/** */
	uint8_t		_reserved1;
	/** */
	uint8_t		_reserved2;
	/** */
	uint8_t		_reserved3;
	/** Maximum number of bytes that the client
		has allocated for returned data.
	 */
	uint8_t		_allocationLength;
	/** */
	uint8_t		_control;
	/** */
	Request() noexcept:
		_opcode(0x03),
		_reserved1(0),
		_reserved2(0),
		_reserved3(0),
		_control(0)
		{}
	};

/** */
struct Standard : public Request {
	/** */
	Standard(uint8_t allocationLength) noexcept{
		_allocationLength	= allocationLength;
		}
	};

/** */
struct Response {
	/** */
	enum{size=18};
	/** */
	enum{minSize=8};
	/** */
	uint8_t	_responseCode;
	/** */
	uint8_t	_obsolete;
	/** */
	uint8_t	_flags;
	/** */
	uint8_t	_information[4];
	/** (n-7)*/
	uint8_t	_additionalSenseLength;
	/** */
	uint8_t	_cmdSpecificInfo[4];
	/** */
	uint8_t	_additionalSenseCode;
	/** */
	uint8_t	_additionalSeseCodeQualifier;
	/** */
	uint8_t	_fieldReplaceableUnitCode;
	/** */
	uint8_t	_senseKeySpecific1;
	/** */
	uint8_t	_senseKeySpecific2;
	/** */
	inline bool	valid() const noexcept{
		return _responseCode & 0x80;
		}
	/** */
	inline uint8_t	responseCode() const noexcept{
		return _responseCode & 0x7F;
		}
	/** */
	inline uint8_t	senseKey() const noexcept{
		return _flags & 0x0F;
		}
	};
void print(const Response& resp) noexcept;
}

}
}
}

#endif
