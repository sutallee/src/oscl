/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <stdio.h>
#include "scsi.h"
#include "oscl/error/info.h"

static const char trueStr[]="true";
static const char falseStr[]="false";

/** */
namespace Oscl {
/** */
namespace SCSI {
/** */
namespace CMD {
/** */
namespace Inquiry {
void print(const Response& resp) noexcept{
	char	buff[128];
	Oscl::Error::Info::log("SCSI INQUERY Response:\n");
	sprintf(buff,"\tPeripheral Qualifier: 0x%2.2X\n",(unsigned)((resp._peripheralQualifierAndDeviceType & 0xE0)>>5));
	Oscl::Error::Info::log(buff);
	sprintf(buff,"\tPeripheral Device Type: 0x%2.2X\n",(unsigned)((resp._peripheralQualifierAndDeviceType & 0x1F)>>0));
	Oscl::Error::Info::log(buff);
	sprintf(buff,"\tRemovable Medium: %s\n",(resp._rmb & 0x80)?trueStr:falseStr);
	Oscl::Error::Info::log(buff);
	sprintf(buff,"\tVersion: 0x%2.2X\n",(unsigned)(resp._version));
	Oscl::Error::Info::log(buff);
	sprintf(buff,"\tAERC: %s\n",(resp._aercNacaHisupRdf & 0x80)?trueStr:falseStr);
	Oscl::Error::Info::log(buff);
	sprintf(buff,"\tNACA: %s\n",(resp._aercNacaHisupRdf & 0x20)?trueStr:falseStr);
	Oscl::Error::Info::log(buff);
	sprintf(buff,"\tHiSup: %s\n",(resp._aercNacaHisupRdf & 0x10)?trueStr:falseStr);
	Oscl::Error::Info::log(buff);
	sprintf(buff,"\tResponse Data Format: 0x%2.2X\n",(unsigned)((resp._aercNacaHisupRdf & 0x0F)>>0));
	Oscl::Error::Info::log(buff);
	sprintf(buff,"\tAdditional Length: %u\n",(unsigned)(resp._additionalLength));
	Oscl::Error::Info::log(buff);
	sprintf(buff,"\tSCCS: %s\n",(resp._sccs & 0x80)?trueStr:falseStr);
	Oscl::Error::Info::log(buff);
	sprintf(buff,"\tBQue: %s\n",(resp._bqEsVsMpMcAddr16 & 0x80)?trueStr:falseStr);
	Oscl::Error::Info::log(buff);
	sprintf(buff,"\tEncServ: %s\n",(resp._bqEsVsMpMcAddr16 & 0x40)?trueStr:falseStr);
	Oscl::Error::Info::log(buff);
	sprintf(buff,"\tVS: %s\n",(resp._bqEsVsMpMcAddr16 & 0x20)?trueStr:falseStr);
	Oscl::Error::Info::log(buff);
	sprintf(buff,"\tMultiP: %s\n",(resp._bqEsVsMpMcAddr16 & 0x10)?trueStr:falseStr);
	Oscl::Error::Info::log(buff);
	sprintf(buff,"\tMChngr: %s\n",(resp._bqEsVsMpMcAddr16 & 0x08)?trueStr:falseStr);
	Oscl::Error::Info::log(buff);
	sprintf(buff,"\tAddr16: %s\n",(resp._bqEsVsMpMcAddr16 & 0x01)?trueStr:falseStr);
	Oscl::Error::Info::log(buff);
	sprintf(buff,"\tRelAdr: %s\n",(resp._raWbus16SyncLinkedCqVs & 0x80)?trueStr:falseStr);
	Oscl::Error::Info::log(buff);
	sprintf(buff,"\tWbus16: %s\n",(resp._raWbus16SyncLinkedCqVs & 0x20)?trueStr:falseStr);
	Oscl::Error::Info::log(buff);
	sprintf(buff,"\tSync: %s\n",(resp._raWbus16SyncLinkedCqVs & 0x10)?trueStr:falseStr);
	Oscl::Error::Info::log(buff);
	sprintf(buff,"\tLinked: %s\n",(resp._raWbus16SyncLinkedCqVs & 0x08)?trueStr:falseStr);
	Oscl::Error::Info::log(buff);
	sprintf(buff,"\tCmdQue: %s\n",(resp._raWbus16SyncLinkedCqVs & 0x02)?trueStr:falseStr);
	Oscl::Error::Info::log(buff);
	sprintf(buff,"\tVS: %s\n",(resp._raWbus16SyncLinkedCqVs & 0x01)?trueStr:falseStr);
	Oscl::Error::Info::log(buff);
	sprintf(buff,"\tVendor Identification: %.8s\n",resp._vendorIdentification);
	Oscl::Error::Info::log(buff);
	sprintf(buff,"\tProduct Identification: %.16s\n",resp._productIdentification);
	Oscl::Error::Info::log(buff);
	sprintf(buff,"\tProduct Revision Level: %.4s\n",resp._productRevisionLevel);
	Oscl::Error::Info::log(buff);
	}
}
/** */
namespace ModeSense10 {
void print(const Response& resp) noexcept{
	Oscl::Error::Info::log("SCSI MODE-SENSE(10) Response:\n");
	Oscl::Error::Info::hexDump(&resp,sizeof(Response));
	}
}

/** */
namespace ModeSense6 {
void print(const Response& resp) noexcept{
	Oscl::Error::Info::log("SCSI MODE-SENSE(6) Response:\n");
	Oscl::Error::Info::hexDump(&resp,sizeof(Response));
	}
}

/** */
namespace FormatUnit {
}
/** */
namespace ModeSelect {
}
/** */
namespace Read6 {
void print(const void* resp) noexcept{
	Oscl::Error::Info::log("SCSI READ(6) Response:\n");
	Oscl::Error::Info::hexDump(resp,16);
	}
}
/** */
namespace Read10 {
void print(const void* resp) noexcept{
	Oscl::Error::Info::log("SCSI READ(10) Response:\n");
	Oscl::Error::Info::hexDump(resp,512);
	}
}
/** */
namespace ReadCapacity {

void print(const Response& resp) noexcept{
	char	buff[128];
	Oscl::Error::Info::log("SCSI READ-CAPACITY Response:\n");
	sprintf(buff,"\tLogical Block Address: 0x%2.2X%2.2X%2.2X%2.2X\n",
		(unsigned)(resp._logicalBlockAddress[0]),
		(unsigned)(resp._logicalBlockAddress[1]),
		(unsigned)(resp._logicalBlockAddress[2]),
		(unsigned)(resp._logicalBlockAddress[3])
		);
	Oscl::Error::Info::log(buff);
	sprintf(buff,"\tBlock Length in Bytes: 0x%2.2X%2.2X%2.2X%2.2X\n",
		(unsigned)(resp._blockLengthInBytes[0]),
		(unsigned)(resp._blockLengthInBytes[1]),
		(unsigned)(resp._blockLengthInBytes[2]),
		(unsigned)(resp._blockLengthInBytes[3])
		);
	Oscl::Error::Info::log(buff);
	}

}
/** Prevent/Allow Medium Removal*/
namespace MediumRemoval {
}
/** */
namespace StartStopUnit {
}
/** */
namespace SynchronizeCache {
}
/** */
namespace TestUnitReady {
}
/** */
namespace Verify {
}
/** */
namespace Write {
}
/** */
namespace WriteBuffer {
}
/** NOT in RBC */
namespace RequestSense {
void print(const Response& resp) noexcept{
	char	buff[128];
	Oscl::Error::Info::log("SCSI REQUEST-SENSE Response:\n");
	sprintf(buff,"\tVALID: %s\n",(resp._responseCode & 0x80)?trueStr:falseStr);
	Oscl::Error::Info::log(buff);
	sprintf(buff,"\tResponseCode: 0x%2.2X\n",(unsigned)(resp._responseCode & 0x7F));
	Oscl::Error::Info::log(buff);
	sprintf(buff,"\tFILEMARK: %s\n",(resp._flags & 0x80)?trueStr:falseStr);
	Oscl::Error::Info::log(buff);
	sprintf(buff,"\tEOM: %s\n",(resp._flags & 0x40)?trueStr:falseStr);
	Oscl::Error::Info::log(buff);
	sprintf(buff,"\tILI: %s\n",(resp._flags & 0x20)?trueStr:falseStr);
	Oscl::Error::Info::log(buff);
	sprintf(buff,"\tSENSE KEY: 0x%2.2X\n",(unsigned)((resp._flags & 0x0F)>>0));
	Oscl::Error::Info::log(buff);
	sprintf(buff,"\tINFORMATION: 0x%2.2X 0x%2.2X 0x%2.2X 0x%2.2X\n",
		(unsigned)(resp._information[0]),
		(unsigned)(resp._information[1]),
		(unsigned)(resp._information[2]),
		(unsigned)(resp._information[3])
		);
	Oscl::Error::Info::log(buff);
	sprintf(buff,"\tAdditional Length: %u\n",(unsigned)(resp._additionalSenseLength));
	Oscl::Error::Info::log(buff);
	sprintf(buff,"\tCommand Specific Information: 0x%2.2X 0x%2.2X 0x%2.2X 0x%2.2X\n",
		(unsigned)(resp._cmdSpecificInfo[0]),
		(unsigned)(resp._cmdSpecificInfo[1]),
		(unsigned)(resp._cmdSpecificInfo[2]),
		(unsigned)(resp._cmdSpecificInfo[3])
		);
	Oscl::Error::Info::log(buff);
	sprintf(buff,"\tAdditional Sense Code: 0x%2.2X\n",(unsigned)(resp._additionalSenseCode));
	Oscl::Error::Info::log(buff);
	sprintf(buff,"\tAdditional Sense Code Qualifier: 0x%2.2X\n",(unsigned)(resp._additionalSeseCodeQualifier));
	Oscl::Error::Info::log(buff);
	sprintf(buff,"\tField Replaceable Unit Code: 0x%2.2X\n",(unsigned)(resp._fieldReplaceableUnitCode));
	Oscl::Error::Info::log(buff);
	sprintf(buff,"\tSense Key Specific: 0x%2.2X 0x%2.2X\n",
		(unsigned)(resp._senseKeySpecific1),
		(unsigned)(resp._senseKeySpecific2)
		);
	Oscl::Error::Info::log(buff);
	}
}

}
}
}

