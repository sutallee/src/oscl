/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_tt_lun_scsih_
#define _oscl_tt_lun_scsih_
#include <stdint.h>
#include "read10.h"
#include "write10.h"
#include "reqsense.h"
#include "synccache.h"

/** */
namespace Oscl {
/** */
namespace SCSI {
/** */
namespace CMD {
/** */
namespace Inquiry {

/** */
struct Request {
	/** */
	const uint8_t	_opcode;
	/** */
	uint8_t		_flags;
	/** */
	uint8_t		_pageOrOperationCode;
	/** */
	uint8_t		_reserved;
	/** Maximum number of bytes that the client
		has allocated for returned data.
	 */
	uint8_t		_allocationLength;
	/** */
	uint8_t		_control;
	/** */
	Request() noexcept:
		_opcode(0x12),
		_reserved(0)
		{}
	};

/** */
struct Standard : public Request {
	/** */
	Standard(	uint8_t	allocationLength) noexcept{
		_flags	= 0;
		_pageOrOperationCode	= 0;
		_allocationLength		= allocationLength;
		_control				= 0;
		}
	};

/** */
struct Response {
	/** */
	enum{size=36};
	/** */
	uint8_t	_peripheralQualifierAndDeviceType;
	/** */
	uint8_t	_rmb;
	/** */
	uint8_t	_version;
	/** */
	uint8_t	_aercNacaHisupRdf;
	/** (n-4) */
	uint8_t	_additionalLength;
	/** */
	uint8_t	_sccs;
	/** */
	uint8_t	_bqEsVsMpMcAddr16;
	/** */
	uint8_t	_raWbus16SyncLinkedCqVs;
	/** */
	uint8_t	_vendorIdentification[8];
	/** */
	uint8_t	_productIdentification[16];
	/** */
	uint8_t	_productRevisionLevel[4];
	};

void print(const Response& resp) noexcept;

}
/** */
namespace ModeSense6 {

/** */
struct Request {
	/** */
	const uint8_t	_opcode;
	/** */
	uint8_t		_flags;
	/** */
	uint8_t		_pcPageCode;
	/** */
	uint8_t		_reserved;
	/** Maximum number of bytes that the client
		has allocated for returned data.
	 */
	uint8_t		_allocationLength;
	/** */
	uint8_t		_control;
	/** */
	Request() noexcept:
		_opcode(0x1A),
		_reserved(0)
		{}
	};

/** */
struct RBC : public Request {
	/** */
	RBC(	uint8_t	allocationLength,
			uint8_t	pcPageCode = 0x86,
			bool		dbd	= true
			) noexcept{
		if(dbd){
			_flags	= 0x08;
			}
		else {
			_flags	= 0x00;
			}
		_pcPageCode	= 	pcPageCode;
		_allocationLength		= allocationLength;
		_control				= 0;
		}
	};
/** */
struct Response {
	/** */
	enum{size=13};
	/** */
	uint8_t	_psPageCode;
	/** */
	uint8_t	_pageLength;
	/** */
	uint8_t	_wcd;
	/** */
	uint8_t	_logicalBlockSize[2];
	/** */
	uint8_t	_numberOfLogicalBlocks[5];
	/** */
	uint8_t	_powerPerformance;
	/** */
	uint8_t	_readWriteFormatLock;
	/** */
	uint8_t	_reserved;
	};

void print(const Response& resp) noexcept;

}

/** */
namespace ModeSense10 {

/** */
struct Request {
	/** */
	const uint8_t	_opcode;
	/** */
	uint8_t		_flags;
	/** */
	uint8_t		_pcPageCode;
	/** */
	uint8_t		_reserved3;
	/** */
	uint8_t		_reserved4;
	/** */
	uint8_t		_reserved5;
	/** */
	uint8_t		_reserved6;
	/** Maximum number of bytes that the client
		has allocated for returned data.
	 */
	uint8_t		_allocationLength[2];
	/** */
	uint8_t		_control;
	/** */
	Request(	uint16_t	allocationLength,
				uint8_t	control
				) noexcept:
		_opcode(0x5A),
		_reserved3(0),
		_reserved4(0),
		_reserved5(0),
		_reserved6(0),
		_control(control)
		{
		_allocationLength[0]	= (uint8_t)(allocationLength >> 8);
		_allocationLength[1]	= (uint8_t)(allocationLength >> 0);
		}
	};

/** */
struct Standard : public Request {
	/** */
	Standard(	uint16_t	allocationLength,
				uint8_t	pageCode = 0
				) noexcept:
		Request(	allocationLength,
					0
					)
		{
		_flags	= 0x08;	// Disable Block Descriptors
		_pcPageCode	=		0x80		// Default values Page Control
										// Only Saved and Default are valid for RBC
						|	pageCode	// Specific device types Page Code
						;
		}
	};
/** */
struct Response {
	/** */
	enum{size=13};
	/** */
	uint8_t	_psPageCode;
	/** */
	uint8_t	_pageLength;
	/** */
	uint8_t	_wcd;
	/** */
	uint8_t	_logicalBlockSize[2];
	/** */
	uint8_t	_numberOfLogicalBlocks[5];
	/** */
	uint8_t	_powerPerformance;
	/** */
	uint8_t	_readWriteFormatLock;
	/** */
	uint8_t	_reserved;
	};

void print(const Response& resp) noexcept;

}


/** */
namespace FormatUnit {
/** */
struct Request {
	/** */
	const uint8_t	_opcode;
	/** */
	uint8_t		_vendorSpecific;
	/** */
	uint8_t		_vsFlags;
	/** */
	uint8_t		_reserved1;
	/** */
	uint8_t		_reserved2;
	/** */
	const uint8_t	_control;
	/** */
	Request() noexcept:
		_opcode(0x04),
		_reserved1(0),
		_reserved2(0),
		_control(0)
		{}
	};

/** */
struct Standard : public Request {
	/** */
	Standard() noexcept{
		_vendorSpecific	= 0x00;
		_vsFlags	=		0x00	// Vendor specific
						|	0x00	// Progress
						|	0x00	// Percent/Time
						|	0x00	// Increment
						;
		}
	};

}
/** */
namespace ModeSelect {
/** */
struct Request {
	/** */
	const uint8_t	_opcode;
	/** */
	uint8_t		_flags;
	/** */
	uint8_t		_reserved1;
	/** */
	uint8_t		_reserved2;
	/** */
	uint8_t		_parameterListLength;
	/** */
	uint8_t		_control;
	/** */
	Request() noexcept:
		_opcode(0x15),
		_reserved1(0),
		_reserved2(0)
		{}
	};
/** */
struct RBC : public Request {
	/** */
	RBC() noexcept{
		_flags	=			0x01	// Save Pages
						|	0x10	// Page Format
						;
		_parameterListLength	=		0x00;	// FIXME
		_control				= 0;	// FIXME
		}
	};

}
/** */
namespace Read6 {
/** */
struct Request {
	/** */
	const uint8_t	_opcode;
	/** */
	uint8_t		_lba[3];
	/** */
	uint8_t		_transferLength;
	/** */
	uint8_t		_control;
	/** */
	Request(	uint32_t	lba,
				uint8_t	transferLength,
				uint8_t	control
				) noexcept:
		_opcode(0x08),
		_transferLength(transferLength),
		_control(control) {
		_lba[2]	= (uint8_t)(lba >> 0);
		_lba[1]	= (uint8_t)(lba >> 8);
		_lba[0]	= (uint8_t)(lba >> 16);
		}
	};
/** */
struct Standard : public Request {
	/** */
	Standard(	uint32_t	lba,
				uint8_t	transferLength
				) noexcept:
		Request(lba,transferLength,0)
		{ }
	};
void print(const void* resp) noexcept;
}
/** */
namespace Read12 {
/** */
struct Request {
	/** */
	const uint8_t	_opcode;
	/** */
	uint8_t		_flags;
	/** */
	uint8_t		_lba[4];
	/** */
	uint8_t		_transferLength[4];
	/** */
	uint8_t		_reserved10;
	/** */
	uint8_t		_control;
	/** */
	Request(	uint8_t	flags,
				uint32_t	lba,
				uint32_t	transferLength,
				uint8_t	control
				) noexcept:
		_opcode(0xA8),
		_flags(flags),
		_reserved10(0),
		_control(control) {
		_transferLength[3]	= (uint8_t)(transferLength >> 0);
		_transferLength[2]	= (uint8_t)(transferLength >> 8);
		_transferLength[1]	= (uint8_t)(transferLength >> 16);
		_transferLength[0]	= (uint8_t)(transferLength >> 24);
		_lba[3]	= (uint8_t)(lba >> 0);
		_lba[2]	= (uint8_t)(lba >> 8);
		_lba[1]	= (uint8_t)(lba >> 16);
		_lba[0]	= (uint8_t)(lba >> 24);
		}
	};
/** */
struct Standard : public Request {
	/** */
	Standard(	uint32_t	lba,
				uint32_t	transferLength
				) noexcept:
		Request(0,lba,transferLength,0)
		{ }
	};
}
/** */
namespace ReadCapacity {
/** */
struct Request {
	/** */
	const uint8_t	_opcode;
	/** */
	uint8_t		_reserved1;
	/** */
	uint8_t		_reserved2;
	/** */
	uint8_t		_reserved3;
	/** */
	uint8_t		_reserved4;
	/** */
	uint8_t		_reserved5;
	/** */
	uint8_t		_reserved6;
	/** */
	uint8_t		_reserved7;
	/** */
	uint8_t		_reserved8;
	/** */
	uint8_t		_control;
	/** */
	Request() noexcept:
		_opcode(0x25),
		_reserved1(0),
		_reserved2(0),
		_reserved3(0),
		_reserved4(0),
		_reserved5(0),
		_reserved6(0),
		_reserved7(0),
		_reserved8(0)
		{}
	};
/** */
struct RBC : public Request {
	/** */
	RBC() noexcept{
		_control	= 0;
		}
	};
/** */
struct Response {
	/** */
	enum{size=8};
	/** */
	uint8_t	_logicalBlockAddress[4];
	/** */
	uint8_t	_blockLengthInBytes[4];
	/** */
	inline uint32_t	logicalBlockAddress() const noexcept{
		uint32_t
		value	=	_logicalBlockAddress[0];
		value	<<=	8;
		value	|=	_logicalBlockAddress[1];
		value	<<=	8;
		value	|=	_logicalBlockAddress[2];
		value	<<=	8;
		value	|=	_logicalBlockAddress[3];
		return value;
		}
	/** */
	inline uint32_t	blockLengthInBytes() const noexcept{
		uint32_t
		value	=	_blockLengthInBytes[0];
		value	<<=	8;
		value	|=	_blockLengthInBytes[1];
		value	<<=	8;
		value	|=	_blockLengthInBytes[2];
		value	<<=	8;
		value	|=	_blockLengthInBytes[3];
		return value;
		}
	};

void print(const Response& resp) noexcept;

}
/** Prevent/Allow Medium Removal*/
namespace MediumRemoval {
}
/** */
namespace StartStopUnit {
/** */
struct Request {
	/** */
	const uint8_t	_opcode;
	/** */
	uint8_t		_immed;
	/** */
	uint8_t		_reserved2;
	/** */
	uint8_t		_reserved3;
	/** */
	uint8_t		_flags;
	/** */
	uint8_t		_control;
	/** */
	Request(	bool		immed,
				uint8_t	flags,
				uint8_t	control=0
				) noexcept:
		_opcode(0x1B),
		_immed(immed?0x01:0x00),
		_reserved2(0),
		_reserved3(0),
		_flags(flags),
		_control(control)
		{}
	};

/** */
struct StartSync : public Request {
	/** */
	StartSync() noexcept:
		Request(true,0x01,0x00)
		{}
	};
/** */
struct StopSync : public Request {
	/** */
	StopSync() noexcept:
		Request(true,0x00,0x00)
		{}
	};
/** */
struct GoActive : public Request {
	/** */
	GoActive() noexcept:
		Request(true,0x10,0x00)
		{}
	};
/** */
struct LoadMedium : public Request {
	/** */
	LoadMedium() noexcept:
		Request(true,0x03,0x00)
		{}
	};
}
/** */
namespace TestUnitReady {
/** */
struct Request {
	/** */
	const uint8_t	_opcode;
	/** */
	uint8_t		_reserved1;
	/** */
	uint8_t		_reserved2;
	/** */
	uint8_t		_reserved3;
	/** */
	uint8_t		_reserved4;
	/** */
	uint8_t		_control;
	/** */
	Request(uint8_t control) noexcept:
		_opcode(0x00),
		_reserved1(0),
		_reserved2(0),
		_reserved3(0),
		_reserved4(0),
		_control(0)
		{}
	};
/** */
struct Standard : public Request {
	/** */
	Standard() noexcept:
		Request(0)
		{ }
	};
}
/** */
namespace Verify {
}
/** */
namespace Write {
}
/** */
namespace WriteBuffer {
}

}
}
}

#endif
