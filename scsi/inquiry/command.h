/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_scsi_inquiry_commandh_
#define _oscl_scsi_inquiry_commandh_
#include "oscl/endian/type.h"

/** */
namespace Oscl {
/** */
namespace SCSI {
/** */
namespace Inquiry {

/*
 * There are three basic Inquiry commands:
 * o Inquiry::VPD - requests Vital Product Data
 *	o EVPD == 1
 *	o CmdDt == 0
 *	o Page/OperationCode
 *		o 0x82 = ASCII implemented operating definitionpage
 *		o 0x01-0x7F	= ASCII information page
 *		o 0x83	= Device identification page (Mandatory)
 *		o 0x80 = Unit serial number page
 *		o 0xC0-0xFF = Vendor specific
 * o Inquiry::CmdDt - requests Command Support Data
 * o Inquiry::Standard - requests Standard Inquiry Data.
 *	o Maximum length of 245 bytes.
 */

/** */
struct Command {
	private:
		/** */
		struct {
			/** */
			uint8_t	_operationCode;
			/**
				Contains two mutually exclusive bit flags:
					o EVPD
					o CmdDt
				These flags must never be set at the same time
				or the server withh return CHECK CONDITION.
				In addition, these flags must be zero if the
				_pageOrOperationCode field is non-zero.
			 */
			uint8_t	_flags;
			/** This field must be set to zero if either
				the EVPD or CmdDt fields of the _flags
				are set.
			 */
			uint8_t	_pageOrOperationCode;
			/** */
			uint8_t	_reserved;
			/** */
			uint8_t	_allocationLength;
			/** */
			uint8_t	_control;
			} _command;
	public:
		/** */
		Command(	bool	getVitalProductData,
					bool	getCommandSupportData
					) noexcept;
		/** */
		unsigned	length() const noexcept;
	};

}
}
}
}

#endif
