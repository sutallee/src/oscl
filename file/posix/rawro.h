/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_file_posix_rawroh_
#define _oscl_file_posix_rawroh_
#include "oscl/stream/input/api.h"
#include "openerr.h"
#include "readerr.h"

/** */
namespace Oscl {

/** */
class FilePosixRawReadOnly : public Stream::Input::Api {
	private:
		/** */
		int					_fileDescriptor;

		/** */
		FilePosixOpenError	_openError;

		/** */
		FilePosixReadError	_readError;

	public:
		/** */
		FilePosixRawReadOnly(	const char*	path,
								bool		exitOnOpenFail=false
								) noexcept;

		/** */
		~FilePosixRawReadOnly() noexcept;

	public: // StreamInputApi
		/** Copies at most maxSize number of bytes from the stream
			into the array specified by dest.
			RETURNS:
			On success, returns the number of bytes copied to the destination
			array and the current position is advanced by this number. Zero
			is returned if the end of the stream is reached.
		 */
		unsigned long	read(	void*			dest,
								unsigned long	maxSize
								) noexcept;

		/** Copies at most dest.bufferSize() number of bytes from the stream
			into the buffer specified by dest.
			RETURNS:
			On success, returns the number of bytes copied to the destination
			and the current position is advanced by this number. Zero
			is returned if the end of the stream is reached.
		 */
		unsigned long	read(Buffer::Base& dest) noexcept;
	};

};

#endif
