/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "buffro.h"
#include "oscl/error/fatal.h"
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>	// Posix
#include "oscl/strings/dynamic.h"

using namespace Oscl;

FilePosixBufferedReadOnly::FilePosixBufferedReadOnly(	const char*	path,
														bool		exitOnOpenFail
														) noexcept:

		_file(0)
		{
	_file	= fopen(path,"r");
	if(!_file){
		if(exitOnOpenFail){
			Strings::Dynamic	s("FilePosixBufferedReadOnly:");
			_openError.append(s);
			s	+= " \"";
			s	+= path;
			s	+= "\"";
			ErrorFatal::logAndExit(s);
			}
		}
	}

FilePosixBufferedReadOnly::~FilePosixBufferedReadOnly() noexcept{
	if(!_file) return;
	fclose(_file);
	}

bool	FilePosixBufferedReadOnly::isOpen() noexcept{
	return _file;
	}

unsigned long	FilePosixBufferedReadOnly::read(	void*			dest,
													unsigned long	maxSize
													) noexcept{
	size_t	result	= fread(dest,1,maxSize,_file);
	if(result < 0){
		result	= 0;
		_readError.decode();
		}
	return result;
	}

unsigned long	FilePosixBufferedReadOnly::read(Buffer::Base& dest) noexcept{
	dest.reset();
	while(dest.remaining()){
		unsigned char	c;
		size_t	result	= fread(&c,1,1,_file);
		if(result < 0){
			_readError.decode();
			break;
			}
		else if(result == 0){
			break;
			}
		else {
			dest	+= c;
			}
		}
	return dest.length();
	}

