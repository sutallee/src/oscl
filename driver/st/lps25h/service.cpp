/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/


#include "service.h"
#include "oscl/error/info.h"

using namespace Oscl::Driver::ST::LPS25H;

Service::Service(
	Oscl::Mt::Itc::PostMsgApi&		papi,
	Oscl::Driver::ST::
	LPS25H::Register::Api&			api,
	Oscl::Event::Observer::
	Req::Api::SAP&					tickSAP
	) noexcept:
		_closeSync(
			*this,
			papi
			),
		_part(
			papi,
			api,
			tickSAP
			)
		{
	}

Oscl::Mt::Itc::Srv::OpenSyncApi&	Service::getOpenSyncApi() noexcept{
	return _closeSync;
	}

Oscl::Mt::Itc::Srv::CloseSyncApi&	Service::getCloseSyncApi() noexcept{
	return _closeSync;
	}

Oscl::Mt::Itc::Srv::Open::Req::Api::SAP&    Service::getOpenSAP() noexcept{
	return _closeSync.getOpenSAP();
	}

Oscl::Mt::Itc::Srv::Close::Req::Api::SAP&   Service::getCloseSAP() noexcept{
	return _closeSync.getSAP();
	}

Oscl::Double::Observer::Req::Api::SAP&	Service::getPressureSAP() noexcept{
	return _part.getPressureSAP();
	}

void	Service::request(Oscl::Mt::Itc::Srv::Open::Req::Api::OpenReq& msg) noexcept{
	_part.start();
	msg.returnToSender();
	}

void	Service::request(Oscl::Mt::Itc::Srv::Close::Req::Api::CloseReq& msg) noexcept{
	// Not implemented yet.
	Oscl::Error::Info::log(
		"%s: NOT IMPLEMENTED!\n",
		__PRETTY_FUNCTION__
		);
	for(;;);
	}

