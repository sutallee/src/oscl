/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/


#include <string.h>
#include "part.h"
#include "oscl/error/info.h"
#include "oscl/hw/st/lps25h/reg.h"
#include "oscl/endian/decoder/linear/part.h"
#include "oscl/mt/thread.h"

using namespace Oscl::Driver::ST::LPS25H;

Part::Part(
	Oscl::Mt::Itc::PostMsgApi&		papi,
	Oscl::Driver::ST::
	LPS25H::Register::Api&			api,
	Oscl::Event::Observer::
	Req::Api::SAP&					tickSAP
	) noexcept:
		_papi(papi),
		_api(api),
		_tickSAP(tickSAP),
		_tickRespComposer(
			*this,
			&Part::tickResponse,
			0
			),
		_tickResp(
			tickSAP.getReqApi(),
			_tickRespComposer,
			papi,
			_tickPayload
			),
		_output(papi)
		{
	}

Oscl::Double::Observer::Req::Api::SAP&	Part::getPressureSAP() noexcept{
	return _output.getSAP();
	}

void	Part::start() noexcept{
	checkID();

	if(boot()){
		return;
		}

	if(checkID()){
		return;
		}

	configureCTRL_REG1();
	configureRES_CONF();
	configureFIFO_CTRL();
	configureCTRL_REG2();

//	configureREF_P();
//	configureCTRL_REG3();
//	configureCTRL_REG4();

	_tickSAP.post(_tickResp.getSrvMsg());
	}

bool	Part::checkID() noexcept{
	// Get LPS25H ID
	uint8_t	id	= _api.getWHO_AM_I();

	constexpr uint8_t	expectedID	= (
				Oscl::HW::ST::LPS25H::WhoAmI::ID::Value_Default
				>> Oscl::HW::ST::LPS25H::WhoAmI::ID::Lsb
				);


	if(id != expectedID){
		Oscl::Error::Info::log(
			"BAD LPS25H WHO_AM_I ID:"
			" expected: 0x%2.2X,"
			" read: 0x%2.2X"
			"\n",
			expectedID,
			id
			);
		return true;
		}

	return false;
	}

bool	Part::boot() noexcept{
	uint8_t
	status	= _api.getCTRL_REG2();

	status	&=	(~Oscl::HW::ST::LPS25H::CtrlReg2::BOOT::FieldMask);

	status	|=	(
			Oscl::HW::ST::LPS25H::CtrlReg2::BOOT::Value_RebootMemoryContent
		<<	Oscl::HW::ST::LPS25H::CtrlReg2::BOOT::Lsb
		);

	// Reboot memory content
	_api.setCTRL_REG2(
		status
		);

	// Wait for device to boot
	Oscl::Mt::Thread::sleep(100);

	return false;
	}

#if 0
bool	Part::configureCTRL_REG1_M() noexcept{
	uint8_t
	temp_comp	= Oscl::HW::ST::LPS25H::CtrlReg1M::TEMP_COMP::Value_Default;

	uint8_t
	om	= Oscl::HW::ST::LPS25H::CtrlReg1M::OM::Value_Default;

	uint8_t
	do_field	= Oscl::HW::ST::LPS25H::CtrlReg1M::DO::Value__10Hz;

	uint8_t
	fast_odr	= Oscl::HW::ST::LPS25H::CtrlReg1M::FAST_ODR::Value_Default;

	uint8_t
	st	= Oscl::HW::ST::LPS25H::CtrlReg1M::ST::Value_Default;

	uint8_t
	value	= (temp_comp << Oscl::HW::ST::LPS25H::CtrlReg1M::TEMP_COMP::Lsb);
	value	|= (om << Oscl::HW::ST::LPS25H::CtrlReg1M::OM::Lsb);
	value	|= (do_field << Oscl::HW::ST::LPS25H::CtrlReg1M::DO::Lsb);
	value	|= (fast_odr << Oscl::HW::ST::LPS25H::CtrlReg1M::FAST_ODR::Lsb);
	value	|= (st << Oscl::HW::ST::LPS25H::CtrlReg1M::ST::Lsb);

	_api.setCTRL_REG1_M(value);

	return false;
	}
#endif
bool	Part::configureREF_P() noexcept{
	uint8_t
	ref_p_l	= Oscl::HW::ST::LPS25H::RefPL::REFL::Value_Default;
	ref_p_l	<<= Oscl::HW::ST::LPS25H::RefPL::REFL::Lsb;

	uint8_t
	ref_p_h	= Oscl::HW::ST::LPS25H::RefPH::REFL::Value_Default;
	ref_p_h	<<= Oscl::HW::ST::LPS25H::RefPH::REFL::Lsb;

	_api.setREF_P_L(ref_p_l);

	_api.setREF_P_H(ref_p_h);

	return false;
	}

bool	Part::configureRES_CONF() noexcept{
	uint8_t
	avgt	= Oscl::HW::ST::LPS25H::ResConf::AVGT::Value_N16;
	avgt	<<= Oscl::HW::ST::LPS25H::ResConf::AVGT::Lsb;

	uint8_t
	avgp	= Oscl::HW::ST::LPS25H::ResConf::AVGP::Value_N32;

	avgp	<<= Oscl::HW::ST::LPS25H::ResConf::AVGP::Lsb;

	uint8_t
	res_conf	= (avgt | avgp);

	_api.setRES_CONF(res_conf);

	return false;
	}

bool	Part::configureCTRL_REG1() noexcept{
	uint8_t
	pd	= Oscl::HW::ST::LPS25H::CtrlReg1::PD::Value_Active;
	pd	<<= Oscl::HW::ST::LPS25H::CtrlReg1::PD::Lsb;

	uint8_t
	odr	= Oscl::HW::ST::LPS25H::CtrlReg1::ODR::Value__25Hz;
	odr	<<= Oscl::HW::ST::LPS25H::CtrlReg1::ODR::Lsb;

	uint8_t
	diff_en	= Oscl::HW::ST::LPS25H::CtrlReg1::DIFF_EN::Value_Default;
	diff_en	<<= Oscl::HW::ST::LPS25H::CtrlReg1::DIFF_EN::Lsb;

	uint8_t
//	bdu	= Oscl::HW::ST::LPS25H::CtrlReg1::BDU::Value_Continuous;
	bdu	= Oscl::HW::ST::LPS25H::CtrlReg1::BDU::Value_MsbAndLsbRead;
	bdu	<<= Oscl::HW::ST::LPS25H::CtrlReg1::BDU::Lsb;

	uint8_t
	reset_az	= Oscl::HW::ST::LPS25H::CtrlReg1::RESET_AZ::Value_Default;
	reset_az	<<= Oscl::HW::ST::LPS25H::CtrlReg1::RESET_AZ::Lsb;

	uint8_t
	sim	= Oscl::HW::ST::LPS25H::CtrlReg1::SIM::Value_Default;
	sim	<<= Oscl::HW::ST::LPS25H::CtrlReg1::SIM::Lsb;

	uint8_t
	ctrl_reg1	=
				pd
			|	odr
			|	diff_en
			|	bdu
			|	reset_az
			|	sim
			;

	_api.setCTRL_REG1(ctrl_reg1);

	return false;
	}

bool	Part::configureCTRL_REG2() noexcept{
	uint8_t
	boot	= Oscl::HW::ST::LPS25H::CtrlReg2::BOOT::Value_Default;
	boot	<<= Oscl::HW::ST::LPS25H::CtrlReg2::BOOT::Lsb;

	uint8_t
	fifo_en	= Oscl::HW::ST::LPS25H::CtrlReg2::FIFO_EN::Value_Enable;
	fifo_en	<<= Oscl::HW::ST::LPS25H::CtrlReg2::FIFO_EN::Lsb;

	uint8_t
	wtm_en	= Oscl::HW::ST::LPS25H::CtrlReg2::WTM_EN::Value_Default;
	wtm_en	<<= Oscl::HW::ST::LPS25H::CtrlReg2::WTM_EN::Lsb;

	uint8_t
	fifo_mean_dec	= Oscl::HW::ST::LPS25H::CtrlReg2::FIFO_MEAN_DEC::Value_Default;
	fifo_mean_dec	<<= Oscl::HW::ST::LPS25H::CtrlReg2::FIFO_MEAN_DEC::Lsb;

	uint8_t
	swreset	= Oscl::HW::ST::LPS25H::CtrlReg2::SWRESET::Value_Default;
	swreset	<<= Oscl::HW::ST::LPS25H::CtrlReg2::SWRESET::Lsb;

	uint8_t
	auto_zero	= Oscl::HW::ST::LPS25H::CtrlReg2::AUTO_ZERO::Value_Default;
	auto_zero	<<= Oscl::HW::ST::LPS25H::CtrlReg2::AUTO_ZERO::Lsb;

	uint8_t
	one_shot	= Oscl::HW::ST::LPS25H::CtrlReg2::ONE_SHOT::Value_Default;
	one_shot	<<= Oscl::HW::ST::LPS25H::CtrlReg2::ONE_SHOT::Lsb;

	uint8_t
	ctrl_reg2	=
			boot
		|	fifo_en
		|	wtm_en
		|	fifo_mean_dec
		|	swreset
		|	auto_zero
		|	one_shot
		;

	_api.setCTRL_REG2(ctrl_reg2);

	return false;
	}

bool	Part::configureCTRL_REG3() noexcept{
	uint8_t
	int_h_l	= Oscl::HW::ST::LPS25H::CtrlReg3::INT_H_L::Value_Default;
	int_h_l	<<= Oscl::HW::ST::LPS25H::CtrlReg3::INT_H_L::Lsb;

	uint8_t
	pp_od	= Oscl::HW::ST::LPS25H::CtrlReg3::PP_OD::Value_Default;
	pp_od	<<= Oscl::HW::ST::LPS25H::CtrlReg3::PP_OD::Lsb;

	uint8_t
	int1_s	= Oscl::HW::ST::LPS25H::CtrlReg3::INT1_S::Value_Default;
	int1_s	<<= Oscl::HW::ST::LPS25H::CtrlReg3::INT1_S::Lsb;

	uint8_t
	ctrl_reg3	=
			int_h_l
		|	pp_od
		|	int1_s
		;

	_api.setCTRL_REG3(ctrl_reg3);

	return false;
	}

bool	Part::configureCTRL_REG4() noexcept{
	uint8_t
	p1_empty	= Oscl::HW::ST::LPS25H::CtrlReg4::P1_EMPTY::Value_Default;
	p1_empty	<<= Oscl::HW::ST::LPS25H::CtrlReg4::P1_EMPTY::Lsb;

	uint8_t
	p1_wtm	= Oscl::HW::ST::LPS25H::CtrlReg4::P1_WTM::Value_Default;
	p1_wtm	<<= Oscl::HW::ST::LPS25H::CtrlReg4::P1_WTM::Lsb;

	uint8_t
	p1_overrun	= Oscl::HW::ST::LPS25H::CtrlReg4::P1_Overrun::Value_Default;
	p1_overrun	<<= Oscl::HW::ST::LPS25H::CtrlReg4::P1_Overrun::Lsb;

	uint8_t
	p1_drdy	= Oscl::HW::ST::LPS25H::CtrlReg4::P1_DRDY::Value_Default;
	p1_drdy	<<= Oscl::HW::ST::LPS25H::CtrlReg4::P1_DRDY::Lsb;

	uint8_t
	ctrl_reg4	=
			p1_empty
		|	p1_wtm
		|	p1_overrun
		|	p1_drdy
		;

	_api.setCTRL_REG4(ctrl_reg4);

	return false;
	}

bool	Part::configureFIFO_CTRL() noexcept{
	uint8_t
	f_mode	= Oscl::HW::ST::LPS25H::FifoCtrl::F_MODE::Value_RunningAverage;
	f_mode	<<= Oscl::HW::ST::LPS25H::FifoCtrl::F_MODE::Lsb;

	uint8_t
	wtm_point	= Oscl::HW::ST::LPS25H::FifoCtrl::WTM_POINT::Value_MovingAverage16Samples;
	wtm_point	<<= Oscl::HW::ST::LPS25H::FifoCtrl::WTM_POINT::Lsb;

	uint8_t
	fifo_ctrl	=
			f_mode
		|	wtm_point
		;

	_api.setFIFO_CTRL(fifo_ctrl);

	return false;
	}

bool	Part::readPressure() noexcept{
#if 0
	uint8_t
	status	= _api.getSTATUS_REG();

	uint8_t
	p_da	= status;
	p_da	&= Oscl::HW::ST::LPS25H::StatusReg::P_DA::FieldMask;
	p_da	>>= Oscl::HW::ST::LPS25H::StatusReg::P_DA::Lsb;

#if 0
	Oscl::Error::Info::log(
		"STATUS_REG: 0x%2.2X, P_DA: 0x%2.2X (%s)\n",
		status,
		p_da,
		p_da?"true":"false"
		);
#endif

	if(p_da != Oscl::HW::ST::LPS25H::StatusReg::P_DA::Value_DataAvailable){
		return true;
		}
#endif

	uint8_t	buffer[4];

	memset(buffer,0,sizeof(buffer));

	_api.getPRESS_OUT(
		sizeof(buffer)-1,
		buffer
		);

	if(buffer[2] & 0x80){
		// If MSB is one then we have
		// negative value. Sign extension
		// is required.
		buffer[3]	= 0xFF;
		}

	Oscl::Endian::Decoder::Linear::Part
	leDecoder(
		buffer,
		sizeof(buffer)
		);

	Oscl::Decoder::Api&	decoder	= leDecoder.le();

	int32_t	pressure;

	decoder.decode(pressure);

	double	scale	= 1.0/4096.0;
	double	bias	= 0;

	double
	press	= pressure;
	press	*= scale;
	press	+= bias;

#if 0
	Oscl::Error::Info::log(
		"pressure: %f, 0x%8.8X \n",
		press,
		pressure
		);
#endif

	_output.update(press);

	return false;
	}

void	Part::tickResponse(Oscl::Event::Observer::Resp::Api::OccurrenceResp& msg) noexcept{

	readPressure();

	_tickSAP.post(msg.getSrvMsg());
	}

