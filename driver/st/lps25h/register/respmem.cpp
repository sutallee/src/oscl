/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <new>
#include "respmem.h"

using namespace Oscl::Driver::ST::LPS25H::Register;

Resp::Api::GetTEMP_OUTResp&
	Resp::GetTEMP_OUTMem::build(	Req::Api::SAP&			sap,
							Resp::Api&				respApi,
							Oscl::Mt::Itc::PostMsgApi&	clientPapi,
							uint8_t	maxBufferLength,
							void*	buffer
							) noexcept{
	Req::Api::GetTEMP_OUTPayload*
		p	=	new(&payload)
				Req::Api::GetTEMP_OUTPayload(
							maxBufferLength,
							buffer
							);
	Resp::Api::GetTEMP_OUTResp*
		r	=	new(&resp)
			Resp::Api::GetTEMP_OUTResp(	sap.getReqApi(),
									respApi,
									clientPapi,
									*p
									);

	return *r;
	}

Resp::Api::GetPRESS_OUTResp&
	Resp::GetPRESS_OUTMem::build(	Req::Api::SAP&			sap,
							Resp::Api&				respApi,
							Oscl::Mt::Itc::PostMsgApi&	clientPapi,
							uint8_t	maxBufferLength,
							void*	buffer
							) noexcept{
	Req::Api::GetPRESS_OUTPayload*
		p	=	new(&payload)
				Req::Api::GetPRESS_OUTPayload(
							maxBufferLength,
							buffer
							);
	Resp::Api::GetPRESS_OUTResp*
		r	=	new(&resp)
			Resp::Api::GetPRESS_OUTResp(	sap.getReqApi(),
									respApi,
									clientPapi,
									*p
									);

	return *r;
	}

Resp::Api::SetRPDS_HResp&
	Resp::SetRPDS_HMem::build(	Req::Api::SAP&			sap,
							Resp::Api&				respApi,
							Oscl::Mt::Itc::PostMsgApi&	clientPapi,
							uint8_t	value
							) noexcept{
	Req::Api::SetRPDS_HPayload*
		p	=	new(&payload)
				Req::Api::SetRPDS_HPayload(
							value
							);
	Resp::Api::SetRPDS_HResp*
		r	=	new(&resp)
			Resp::Api::SetRPDS_HResp(	sap.getReqApi(),
									respApi,
									clientPapi,
									*p
									);

	return *r;
	}

Resp::Api::GetRPDS_HResp&
	Resp::GetRPDS_HMem::build(	Req::Api::SAP&			sap,
							Resp::Api&				respApi,
							Oscl::Mt::Itc::PostMsgApi&	clientPapi
							) noexcept{
	Req::Api::GetRPDS_HPayload*
		p	=	new(&payload)
				Req::Api::GetRPDS_HPayload(
);
	Resp::Api::GetRPDS_HResp*
		r	=	new(&resp)
			Resp::Api::GetRPDS_HResp(	sap.getReqApi(),
									respApi,
									clientPapi,
									*p
									);

	return *r;
	}

Resp::Api::SetRPDS_LResp&
	Resp::SetRPDS_LMem::build(	Req::Api::SAP&			sap,
							Resp::Api&				respApi,
							Oscl::Mt::Itc::PostMsgApi&	clientPapi,
							uint8_t	value
							) noexcept{
	Req::Api::SetRPDS_LPayload*
		p	=	new(&payload)
				Req::Api::SetRPDS_LPayload(
							value
							);
	Resp::Api::SetRPDS_LResp*
		r	=	new(&resp)
			Resp::Api::SetRPDS_LResp(	sap.getReqApi(),
									respApi,
									clientPapi,
									*p
									);

	return *r;
	}

Resp::Api::GetRPDS_LResp&
	Resp::GetRPDS_LMem::build(	Req::Api::SAP&			sap,
							Resp::Api&				respApi,
							Oscl::Mt::Itc::PostMsgApi&	clientPapi
							) noexcept{
	Req::Api::GetRPDS_LPayload*
		p	=	new(&payload)
				Req::Api::GetRPDS_LPayload(
);
	Resp::Api::GetRPDS_LResp*
		r	=	new(&resp)
			Resp::Api::GetRPDS_LResp(	sap.getReqApi(),
									respApi,
									clientPapi,
									*p
									);

	return *r;
	}

Resp::Api::SetTHS_P_HResp&
	Resp::SetTHS_P_HMem::build(	Req::Api::SAP&			sap,
							Resp::Api&				respApi,
							Oscl::Mt::Itc::PostMsgApi&	clientPapi,
							uint8_t	value
							) noexcept{
	Req::Api::SetTHS_P_HPayload*
		p	=	new(&payload)
				Req::Api::SetTHS_P_HPayload(
							value
							);
	Resp::Api::SetTHS_P_HResp*
		r	=	new(&resp)
			Resp::Api::SetTHS_P_HResp(	sap.getReqApi(),
									respApi,
									clientPapi,
									*p
									);

	return *r;
	}

Resp::Api::GetTHS_P_HResp&
	Resp::GetTHS_P_HMem::build(	Req::Api::SAP&			sap,
							Resp::Api&				respApi,
							Oscl::Mt::Itc::PostMsgApi&	clientPapi
							) noexcept{
	Req::Api::GetTHS_P_HPayload*
		p	=	new(&payload)
				Req::Api::GetTHS_P_HPayload(
);
	Resp::Api::GetTHS_P_HResp*
		r	=	new(&resp)
			Resp::Api::GetTHS_P_HResp(	sap.getReqApi(),
									respApi,
									clientPapi,
									*p
									);

	return *r;
	}

Resp::Api::SetTHS_P_LResp&
	Resp::SetTHS_P_LMem::build(	Req::Api::SAP&			sap,
							Resp::Api&				respApi,
							Oscl::Mt::Itc::PostMsgApi&	clientPapi,
							uint8_t	value
							) noexcept{
	Req::Api::SetTHS_P_LPayload*
		p	=	new(&payload)
				Req::Api::SetTHS_P_LPayload(
							value
							);
	Resp::Api::SetTHS_P_LResp*
		r	=	new(&resp)
			Resp::Api::SetTHS_P_LResp(	sap.getReqApi(),
									respApi,
									clientPapi,
									*p
									);

	return *r;
	}

Resp::Api::GetTHS_P_LResp&
	Resp::GetTHS_P_LMem::build(	Req::Api::SAP&			sap,
							Resp::Api&				respApi,
							Oscl::Mt::Itc::PostMsgApi&	clientPapi
							) noexcept{
	Req::Api::GetTHS_P_LPayload*
		p	=	new(&payload)
				Req::Api::GetTHS_P_LPayload(
);
	Resp::Api::GetTHS_P_LResp*
		r	=	new(&resp)
			Resp::Api::GetTHS_P_LResp(	sap.getReqApi(),
									respApi,
									clientPapi,
									*p
									);

	return *r;
	}

Resp::Api::GetFIFO_STATUSResp&
	Resp::GetFIFO_STATUSMem::build(	Req::Api::SAP&			sap,
							Resp::Api&				respApi,
							Oscl::Mt::Itc::PostMsgApi&	clientPapi
							) noexcept{
	Req::Api::GetFIFO_STATUSPayload*
		p	=	new(&payload)
				Req::Api::GetFIFO_STATUSPayload(
);
	Resp::Api::GetFIFO_STATUSResp*
		r	=	new(&resp)
			Resp::Api::GetFIFO_STATUSResp(	sap.getReqApi(),
									respApi,
									clientPapi,
									*p
									);

	return *r;
	}

Resp::Api::SetFIFO_CTRLResp&
	Resp::SetFIFO_CTRLMem::build(	Req::Api::SAP&			sap,
							Resp::Api&				respApi,
							Oscl::Mt::Itc::PostMsgApi&	clientPapi,
							uint8_t	value
							) noexcept{
	Req::Api::SetFIFO_CTRLPayload*
		p	=	new(&payload)
				Req::Api::SetFIFO_CTRLPayload(
							value
							);
	Resp::Api::SetFIFO_CTRLResp*
		r	=	new(&resp)
			Resp::Api::SetFIFO_CTRLResp(	sap.getReqApi(),
									respApi,
									clientPapi,
									*p
									);

	return *r;
	}

Resp::Api::GetFIFO_CTRLResp&
	Resp::GetFIFO_CTRLMem::build(	Req::Api::SAP&			sap,
							Resp::Api&				respApi,
							Oscl::Mt::Itc::PostMsgApi&	clientPapi
							) noexcept{
	Req::Api::GetFIFO_CTRLPayload*
		p	=	new(&payload)
				Req::Api::GetFIFO_CTRLPayload(
);
	Resp::Api::GetFIFO_CTRLResp*
		r	=	new(&resp)
			Resp::Api::GetFIFO_CTRLResp(	sap.getReqApi(),
									respApi,
									clientPapi,
									*p
									);

	return *r;
	}

Resp::Api::GetTEMP_OUT_HResp&
	Resp::GetTEMP_OUT_HMem::build(	Req::Api::SAP&			sap,
							Resp::Api&				respApi,
							Oscl::Mt::Itc::PostMsgApi&	clientPapi
							) noexcept{
	Req::Api::GetTEMP_OUT_HPayload*
		p	=	new(&payload)
				Req::Api::GetTEMP_OUT_HPayload(
);
	Resp::Api::GetTEMP_OUT_HResp*
		r	=	new(&resp)
			Resp::Api::GetTEMP_OUT_HResp(	sap.getReqApi(),
									respApi,
									clientPapi,
									*p
									);

	return *r;
	}

Resp::Api::GetTEMP_OUT_LResp&
	Resp::GetTEMP_OUT_LMem::build(	Req::Api::SAP&			sap,
							Resp::Api&				respApi,
							Oscl::Mt::Itc::PostMsgApi&	clientPapi
							) noexcept{
	Req::Api::GetTEMP_OUT_LPayload*
		p	=	new(&payload)
				Req::Api::GetTEMP_OUT_LPayload(
);
	Resp::Api::GetTEMP_OUT_LResp*
		r	=	new(&resp)
			Resp::Api::GetTEMP_OUT_LResp(	sap.getReqApi(),
									respApi,
									clientPapi,
									*p
									);

	return *r;
	}

Resp::Api::GetPRESS_OUT_HResp&
	Resp::GetPRESS_OUT_HMem::build(	Req::Api::SAP&			sap,
							Resp::Api&				respApi,
							Oscl::Mt::Itc::PostMsgApi&	clientPapi
							) noexcept{
	Req::Api::GetPRESS_OUT_HPayload*
		p	=	new(&payload)
				Req::Api::GetPRESS_OUT_HPayload(
);
	Resp::Api::GetPRESS_OUT_HResp*
		r	=	new(&resp)
			Resp::Api::GetPRESS_OUT_HResp(	sap.getReqApi(),
									respApi,
									clientPapi,
									*p
									);

	return *r;
	}

Resp::Api::GetPRESS_OUT_LResp&
	Resp::GetPRESS_OUT_LMem::build(	Req::Api::SAP&			sap,
							Resp::Api&				respApi,
							Oscl::Mt::Itc::PostMsgApi&	clientPapi
							) noexcept{
	Req::Api::GetPRESS_OUT_LPayload*
		p	=	new(&payload)
				Req::Api::GetPRESS_OUT_LPayload(
);
	Resp::Api::GetPRESS_OUT_LResp*
		r	=	new(&resp)
			Resp::Api::GetPRESS_OUT_LResp(	sap.getReqApi(),
									respApi,
									clientPapi,
									*p
									);

	return *r;
	}

Resp::Api::GetPRESS_OUT_XLResp&
	Resp::GetPRESS_OUT_XLMem::build(	Req::Api::SAP&			sap,
							Resp::Api&				respApi,
							Oscl::Mt::Itc::PostMsgApi&	clientPapi
							) noexcept{
	Req::Api::GetPRESS_OUT_XLPayload*
		p	=	new(&payload)
				Req::Api::GetPRESS_OUT_XLPayload(
);
	Resp::Api::GetPRESS_OUT_XLResp*
		r	=	new(&resp)
			Resp::Api::GetPRESS_OUT_XLResp(	sap.getReqApi(),
									respApi,
									clientPapi,
									*p
									);

	return *r;
	}

Resp::Api::GetSTATUS_REGResp&
	Resp::GetSTATUS_REGMem::build(	Req::Api::SAP&			sap,
							Resp::Api&				respApi,
							Oscl::Mt::Itc::PostMsgApi&	clientPapi
							) noexcept{
	Req::Api::GetSTATUS_REGPayload*
		p	=	new(&payload)
				Req::Api::GetSTATUS_REGPayload(
);
	Resp::Api::GetSTATUS_REGResp*
		r	=	new(&resp)
			Resp::Api::GetSTATUS_REGResp(	sap.getReqApi(),
									respApi,
									clientPapi,
									*p
									);

	return *r;
	}

Resp::Api::SetINT_SOURCEResp&
	Resp::SetINT_SOURCEMem::build(	Req::Api::SAP&			sap,
							Resp::Api&				respApi,
							Oscl::Mt::Itc::PostMsgApi&	clientPapi,
							uint8_t	value
							) noexcept{
	Req::Api::SetINT_SOURCEPayload*
		p	=	new(&payload)
				Req::Api::SetINT_SOURCEPayload(
							value
							);
	Resp::Api::SetINT_SOURCEResp*
		r	=	new(&resp)
			Resp::Api::SetINT_SOURCEResp(	sap.getReqApi(),
									respApi,
									clientPapi,
									*p
									);

	return *r;
	}

Resp::Api::GetINT_SOURCEResp&
	Resp::GetINT_SOURCEMem::build(	Req::Api::SAP&			sap,
							Resp::Api&				respApi,
							Oscl::Mt::Itc::PostMsgApi&	clientPapi
							) noexcept{
	Req::Api::GetINT_SOURCEPayload*
		p	=	new(&payload)
				Req::Api::GetINT_SOURCEPayload(
);
	Resp::Api::GetINT_SOURCEResp*
		r	=	new(&resp)
			Resp::Api::GetINT_SOURCEResp(	sap.getReqApi(),
									respApi,
									clientPapi,
									*p
									);

	return *r;
	}

Resp::Api::SetINT_CFGResp&
	Resp::SetINT_CFGMem::build(	Req::Api::SAP&			sap,
							Resp::Api&				respApi,
							Oscl::Mt::Itc::PostMsgApi&	clientPapi,
							uint8_t	value
							) noexcept{
	Req::Api::SetINT_CFGPayload*
		p	=	new(&payload)
				Req::Api::SetINT_CFGPayload(
							value
							);
	Resp::Api::SetINT_CFGResp*
		r	=	new(&resp)
			Resp::Api::SetINT_CFGResp(	sap.getReqApi(),
									respApi,
									clientPapi,
									*p
									);

	return *r;
	}

Resp::Api::GetINT_CFGResp&
	Resp::GetINT_CFGMem::build(	Req::Api::SAP&			sap,
							Resp::Api&				respApi,
							Oscl::Mt::Itc::PostMsgApi&	clientPapi
							) noexcept{
	Req::Api::GetINT_CFGPayload*
		p	=	new(&payload)
				Req::Api::GetINT_CFGPayload(
);
	Resp::Api::GetINT_CFGResp*
		r	=	new(&resp)
			Resp::Api::GetINT_CFGResp(	sap.getReqApi(),
									respApi,
									clientPapi,
									*p
									);

	return *r;
	}

Resp::Api::SetCTRL_REG4Resp&
	Resp::SetCTRL_REG4Mem::build(	Req::Api::SAP&			sap,
							Resp::Api&				respApi,
							Oscl::Mt::Itc::PostMsgApi&	clientPapi,
							uint8_t	value
							) noexcept{
	Req::Api::SetCTRL_REG4Payload*
		p	=	new(&payload)
				Req::Api::SetCTRL_REG4Payload(
							value
							);
	Resp::Api::SetCTRL_REG4Resp*
		r	=	new(&resp)
			Resp::Api::SetCTRL_REG4Resp(	sap.getReqApi(),
									respApi,
									clientPapi,
									*p
									);

	return *r;
	}

Resp::Api::GetCTRL_REG4Resp&
	Resp::GetCTRL_REG4Mem::build(	Req::Api::SAP&			sap,
							Resp::Api&				respApi,
							Oscl::Mt::Itc::PostMsgApi&	clientPapi
							) noexcept{
	Req::Api::GetCTRL_REG4Payload*
		p	=	new(&payload)
				Req::Api::GetCTRL_REG4Payload(
);
	Resp::Api::GetCTRL_REG4Resp*
		r	=	new(&resp)
			Resp::Api::GetCTRL_REG4Resp(	sap.getReqApi(),
									respApi,
									clientPapi,
									*p
									);

	return *r;
	}

Resp::Api::SetCTRL_REG3Resp&
	Resp::SetCTRL_REG3Mem::build(	Req::Api::SAP&			sap,
							Resp::Api&				respApi,
							Oscl::Mt::Itc::PostMsgApi&	clientPapi,
							uint8_t	value
							) noexcept{
	Req::Api::SetCTRL_REG3Payload*
		p	=	new(&payload)
				Req::Api::SetCTRL_REG3Payload(
							value
							);
	Resp::Api::SetCTRL_REG3Resp*
		r	=	new(&resp)
			Resp::Api::SetCTRL_REG3Resp(	sap.getReqApi(),
									respApi,
									clientPapi,
									*p
									);

	return *r;
	}

Resp::Api::GetCTRL_REG3Resp&
	Resp::GetCTRL_REG3Mem::build(	Req::Api::SAP&			sap,
							Resp::Api&				respApi,
							Oscl::Mt::Itc::PostMsgApi&	clientPapi
							) noexcept{
	Req::Api::GetCTRL_REG3Payload*
		p	=	new(&payload)
				Req::Api::GetCTRL_REG3Payload(
);
	Resp::Api::GetCTRL_REG3Resp*
		r	=	new(&resp)
			Resp::Api::GetCTRL_REG3Resp(	sap.getReqApi(),
									respApi,
									clientPapi,
									*p
									);

	return *r;
	}

Resp::Api::SetCTRL_REG2Resp&
	Resp::SetCTRL_REG2Mem::build(	Req::Api::SAP&			sap,
							Resp::Api&				respApi,
							Oscl::Mt::Itc::PostMsgApi&	clientPapi,
							uint8_t	value
							) noexcept{
	Req::Api::SetCTRL_REG2Payload*
		p	=	new(&payload)
				Req::Api::SetCTRL_REG2Payload(
							value
							);
	Resp::Api::SetCTRL_REG2Resp*
		r	=	new(&resp)
			Resp::Api::SetCTRL_REG2Resp(	sap.getReqApi(),
									respApi,
									clientPapi,
									*p
									);

	return *r;
	}

Resp::Api::GetCTRL_REG2Resp&
	Resp::GetCTRL_REG2Mem::build(	Req::Api::SAP&			sap,
							Resp::Api&				respApi,
							Oscl::Mt::Itc::PostMsgApi&	clientPapi
							) noexcept{
	Req::Api::GetCTRL_REG2Payload*
		p	=	new(&payload)
				Req::Api::GetCTRL_REG2Payload(
);
	Resp::Api::GetCTRL_REG2Resp*
		r	=	new(&resp)
			Resp::Api::GetCTRL_REG2Resp(	sap.getReqApi(),
									respApi,
									clientPapi,
									*p
									);

	return *r;
	}

Resp::Api::SetCTRL_REG1Resp&
	Resp::SetCTRL_REG1Mem::build(	Req::Api::SAP&			sap,
							Resp::Api&				respApi,
							Oscl::Mt::Itc::PostMsgApi&	clientPapi,
							uint8_t	value
							) noexcept{
	Req::Api::SetCTRL_REG1Payload*
		p	=	new(&payload)
				Req::Api::SetCTRL_REG1Payload(
							value
							);
	Resp::Api::SetCTRL_REG1Resp*
		r	=	new(&resp)
			Resp::Api::SetCTRL_REG1Resp(	sap.getReqApi(),
									respApi,
									clientPapi,
									*p
									);

	return *r;
	}

Resp::Api::GetCTRL_REG1Resp&
	Resp::GetCTRL_REG1Mem::build(	Req::Api::SAP&			sap,
							Resp::Api&				respApi,
							Oscl::Mt::Itc::PostMsgApi&	clientPapi
							) noexcept{
	Req::Api::GetCTRL_REG1Payload*
		p	=	new(&payload)
				Req::Api::GetCTRL_REG1Payload(
);
	Resp::Api::GetCTRL_REG1Resp*
		r	=	new(&resp)
			Resp::Api::GetCTRL_REG1Resp(	sap.getReqApi(),
									respApi,
									clientPapi,
									*p
									);

	return *r;
	}

Resp::Api::SetRES_CONFResp&
	Resp::SetRES_CONFMem::build(	Req::Api::SAP&			sap,
							Resp::Api&				respApi,
							Oscl::Mt::Itc::PostMsgApi&	clientPapi,
							uint8_t	value
							) noexcept{
	Req::Api::SetRES_CONFPayload*
		p	=	new(&payload)
				Req::Api::SetRES_CONFPayload(
							value
							);
	Resp::Api::SetRES_CONFResp*
		r	=	new(&resp)
			Resp::Api::SetRES_CONFResp(	sap.getReqApi(),
									respApi,
									clientPapi,
									*p
									);

	return *r;
	}

Resp::Api::GetRES_CONFResp&
	Resp::GetRES_CONFMem::build(	Req::Api::SAP&			sap,
							Resp::Api&				respApi,
							Oscl::Mt::Itc::PostMsgApi&	clientPapi
							) noexcept{
	Req::Api::GetRES_CONFPayload*
		p	=	new(&payload)
				Req::Api::GetRES_CONFPayload(
);
	Resp::Api::GetRES_CONFResp*
		r	=	new(&resp)
			Resp::Api::GetRES_CONFResp(	sap.getReqApi(),
									respApi,
									clientPapi,
									*p
									);

	return *r;
	}

Resp::Api::GetWHO_AM_IResp&
	Resp::GetWHO_AM_IMem::build(	Req::Api::SAP&			sap,
							Resp::Api&				respApi,
							Oscl::Mt::Itc::PostMsgApi&	clientPapi
							) noexcept{
	Req::Api::GetWHO_AM_IPayload*
		p	=	new(&payload)
				Req::Api::GetWHO_AM_IPayload(
);
	Resp::Api::GetWHO_AM_IResp*
		r	=	new(&resp)
			Resp::Api::GetWHO_AM_IResp(	sap.getReqApi(),
									respApi,
									clientPapi,
									*p
									);

	return *r;
	}

Resp::Api::SetREF_P_HResp&
	Resp::SetREF_P_HMem::build(	Req::Api::SAP&			sap,
							Resp::Api&				respApi,
							Oscl::Mt::Itc::PostMsgApi&	clientPapi,
							uint8_t	value
							) noexcept{
	Req::Api::SetREF_P_HPayload*
		p	=	new(&payload)
				Req::Api::SetREF_P_HPayload(
							value
							);
	Resp::Api::SetREF_P_HResp*
		r	=	new(&resp)
			Resp::Api::SetREF_P_HResp(	sap.getReqApi(),
									respApi,
									clientPapi,
									*p
									);

	return *r;
	}

Resp::Api::GetREF_P_HResp&
	Resp::GetREF_P_HMem::build(	Req::Api::SAP&			sap,
							Resp::Api&				respApi,
							Oscl::Mt::Itc::PostMsgApi&	clientPapi
							) noexcept{
	Req::Api::GetREF_P_HPayload*
		p	=	new(&payload)
				Req::Api::GetREF_P_HPayload(
);
	Resp::Api::GetREF_P_HResp*
		r	=	new(&resp)
			Resp::Api::GetREF_P_HResp(	sap.getReqApi(),
									respApi,
									clientPapi,
									*p
									);

	return *r;
	}

Resp::Api::SetREF_P_LResp&
	Resp::SetREF_P_LMem::build(	Req::Api::SAP&			sap,
							Resp::Api&				respApi,
							Oscl::Mt::Itc::PostMsgApi&	clientPapi,
							uint8_t	value
							) noexcept{
	Req::Api::SetREF_P_LPayload*
		p	=	new(&payload)
				Req::Api::SetREF_P_LPayload(
							value
							);
	Resp::Api::SetREF_P_LResp*
		r	=	new(&resp)
			Resp::Api::SetREF_P_LResp(	sap.getReqApi(),
									respApi,
									clientPapi,
									*p
									);

	return *r;
	}

Resp::Api::GetREF_P_LResp&
	Resp::GetREF_P_LMem::build(	Req::Api::SAP&			sap,
							Resp::Api&				respApi,
							Oscl::Mt::Itc::PostMsgApi&	clientPapi
							) noexcept{
	Req::Api::GetREF_P_LPayload*
		p	=	new(&payload)
				Req::Api::GetREF_P_LPayload(
);
	Resp::Api::GetREF_P_LResp*
		r	=	new(&resp)
			Resp::Api::GetREF_P_LResp(	sap.getReqApi(),
									respApi,
									clientPapi,
									*p
									);

	return *r;
	}

Resp::Api::SetREF_P_XLResp&
	Resp::SetREF_P_XLMem::build(	Req::Api::SAP&			sap,
							Resp::Api&				respApi,
							Oscl::Mt::Itc::PostMsgApi&	clientPapi,
							uint8_t	value
							) noexcept{
	Req::Api::SetREF_P_XLPayload*
		p	=	new(&payload)
				Req::Api::SetREF_P_XLPayload(
							value
							);
	Resp::Api::SetREF_P_XLResp*
		r	=	new(&resp)
			Resp::Api::SetREF_P_XLResp(	sap.getReqApi(),
									respApi,
									clientPapi,
									*p
									);

	return *r;
	}

Resp::Api::GetREF_P_XLResp&
	Resp::GetREF_P_XLMem::build(	Req::Api::SAP&			sap,
							Resp::Api&				respApi,
							Oscl::Mt::Itc::PostMsgApi&	clientPapi
							) noexcept{
	Req::Api::GetREF_P_XLPayload*
		p	=	new(&payload)
				Req::Api::GetREF_P_XLPayload(
);
	Resp::Api::GetREF_P_XLResp*
		r	=	new(&resp)
			Resp::Api::GetREF_P_XLResp(	sap.getReqApi(),
									respApi,
									clientPapi,
									*p
									);

	return *r;
	}

