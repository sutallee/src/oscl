/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

sysinclude "stdint.h";

/** */
namespace Oscl {
/** */
namespace Driver {
/** */
namespace ST {
/** */
namespace LPS25H {
/** */
namespace Register {

/** This interface defines methods to access the
	LPS25H IMU Magnetometer registers indpendent
	of the type of bus.
 */
itc ITC {

	/**  Read the REF_P_XL register.
	 */
	message	GetREF_P_XL {
		fields{
			/** */
			output	uint8_t	value;
			}

		/** */
		sync	uint8_t getREF_P_XL value;
		}

	/**  Write the REF_P_XL register
	 */
	message	SetREF_P_XL {
		fields{
			/** */
			input	uint8_t	value;
			}

		/** */
		sync	void setREF_P_XL;
		}

	/**  Read the REF_P_L register.
	 */
	message	GetREF_P_L {
		fields{
			/** */
			output	uint8_t	value;
			}

		/** */
		sync	uint8_t getREF_P_L value;
		}

	/**  Write the REF_P_L register
	 */
	message	SetREF_P_L {
		fields{
			/** */
			input	uint8_t	value;
			}

		/** */
		sync	void setREF_P_L;
		}

	/**  Read the REF_P_H register.
	 */
	message	GetREF_P_H {
		fields{
			/** */
			output	uint8_t	value;
			}

		/** */
		sync	uint8_t getREF_P_H value;
		}

	/**  Write the REF_P_H register
	 */
	message	SetREF_P_H {
		fields{
			/** */
			input	uint8_t	value;
			}

		/** */
		sync	void setREF_P_H;
		}

	/**  Read the WHO_AM_I register.
	 */
	message	GetWHO_AM_I {
		fields{
			/** */
			output	uint8_t	value;
			}

		/** */
		sync	uint8_t getWHO_AM_I value;
		}

	/**  Read the RES_CONF register.
	 */
	message	GetRES_CONF {
		fields{
			/** */
			output	uint8_t	value;
			}

		/** */
		sync	uint8_t getRES_CONF value;
		}

	/**  Write the RES_CONF register
	 */
	message	SetRES_CONF {
		fields{
			/** */
			input	uint8_t	value;
			}

		/** */
		sync	void setRES_CONF;
		}

	/**  Read the CTRL_REG1 register.
	 */
	message	GetCTRL_REG1 {
		fields{
			/** */
			output	uint8_t	value;
			}

		/** */
		sync	uint8_t getCTRL_REG1 value;
		}

	/**  Write the CTRL_REG1 register
	 */
	message	SetCTRL_REG1 {
		fields{
			/** */
			input	uint8_t	value;
			}

		/** */
		sync	void setCTRL_REG1;
		}

	/**  Read the CTRL_REG2 register.
	 */
	message	GetCTRL_REG2 {
		fields{
			/** */
			output	uint8_t	value;
			}

		/** */
		sync	uint8_t getCTRL_REG2 value;
		}

	/**  Write the CTRL_REG2 register
	 */
	message	SetCTRL_REG2 {
		fields{
			/** */
			input	uint8_t	value;
			}

		/** */
		sync	void setCTRL_REG2;
		}

	/**  Read the CTRL_REG3 register.
	 */
	message	GetCTRL_REG3 {
		fields{
			/** */
			output	uint8_t	value;
			}

		/** */
		sync	uint8_t getCTRL_REG3 value;
		}

	/**  Write the CTRL_REG3 register
	 */
	message	SetCTRL_REG3 {
		fields{
			/** */
			input	uint8_t	value;
			}

		/** */
		sync	void setCTRL_REG3;
		}

	/**  Read the CTRL_REG4 register.
	 */
	message	GetCTRL_REG4 {
		fields{
			/** */
			output	uint8_t	value;
			}

		/** */
		sync	uint8_t getCTRL_REG4 value;
		}

	/**  Write the CTRL_REG4 register
	 */
	message	SetCTRL_REG4 {
		fields{
			/** */
			input	uint8_t	value;
			}

		/** */
		sync	void setCTRL_REG4;
		}

	/**  Read the INT_CFG register.
	 */
	message	GetINT_CFG {
		fields{
			/** */
			output	uint8_t	value;
			}

		/** */
		sync	uint8_t getINT_CFG value;
		}

	/**  Write the INT_CFG register
	 */
	message	SetINT_CFG {
		fields{
			/** */
			input	uint8_t	value;
			}

		/** */
		sync	void setINT_CFG;
		}

	/**  Read the INT_SOURCE register.
	 */
	message	GetINT_SOURCE {
		fields{
			/** */
			output	uint8_t	value;
			}

		/** */
		sync	uint8_t getINT_SOURCE value;
		}

	/**  Write the INT_SOURCE register
	 */
	message	SetINT_SOURCE {
		fields{
			/** */
			input	uint8_t	value;
			}

		/** */
		sync	void setINT_SOURCE;
		}

	/**  Read the STATUS_REG register.
	 */
	message	GetSTATUS_REG {
		fields{
			/** */
			output	uint8_t	value;
			}

		/** */
		sync	uint8_t getSTATUS_REG value;
		}

	/**  Read the PRESS_OUT_XL register.
	 */
	message	GetPRESS_OUT_XL {
		fields{
			/** */
			output	uint8_t	value;
			}

		/** */
		sync	uint8_t getPRESS_OUT_XL value;
		}

	/**  Read the PRESS_OUT_L register.
	 */
	message	GetPRESS_OUT_L {
		fields{
			/** */
			output	uint8_t	value;
			}

		/** */
		sync	uint8_t getPRESS_OUT_L value;
		}

	/**  Read the PRESS_OUT_H register.
	 */
	message	GetPRESS_OUT_H {
		fields{
			/** */
			output	uint8_t	value;
			}

		/** */
		sync	uint8_t getPRESS_OUT_H value;
		}

	/**  Read the TEMP_OUT_L register.
	 */
	message	GetTEMP_OUT_L {
		fields{
			/** */
			output	uint8_t	value;
			}

		/** */
		sync	uint8_t getTEMP_OUT_L value;
		}

	/**  Read the TEMP_OUT_H register.
	 */
	message	GetTEMP_OUT_H {
		fields{
			/** */
			output	uint8_t	value;
			}

		/** */
		sync	uint8_t getTEMP_OUT_H value;
		}

	/**  Read the FIFO_CTRL register.
	 */
	message	GetFIFO_CTRL {
		fields{
			/** */
			output	uint8_t	value;
			}

		/** */
		sync	uint8_t getFIFO_CTRL value;
		}

	/**  Write the FIFO_CTRL register
	 */
	message	SetFIFO_CTRL {
		fields{
			/** */
			input	uint8_t	value;
			}

		/** */
		sync	void setFIFO_CTRL;
		}

	/**  Read the FIFO_STATUS register.
	 */
	message	GetFIFO_STATUS {
		fields{
			/** */
			output	uint8_t	value;
			}

		/** */
		sync	uint8_t getFIFO_STATUS value;
		}

	/**  Read the THS_P_L register.
	 */
	message	GetTHS_P_L {
		fields{
			/** */
			output	uint8_t	value;
			}

		/** */
		sync	uint8_t getTHS_P_L value;
		}

	/**  Write the THS_P_L register
	 */
	message	SetTHS_P_L {
		fields{
			/** */
			input	uint8_t	value;
			}

		/** */
		sync	void setTHS_P_L;
		}

	/**  Read the THS_P_H register.
	 */
	message	GetTHS_P_H {
		fields{
			/** */
			output	uint8_t	value;
			}

		/** */
		sync	uint8_t getTHS_P_H value;
		}

	/**  Write the THS_P_H register
	 */
	message	SetTHS_P_H {
		fields{
			/** */
			input	uint8_t	value;
			}

		/** */
		sync	void setTHS_P_H;
		}

	/**  Read the RPDS_L register.
	 */
	message	GetRPDS_L {
		fields{
			/** */
			output	uint8_t	value;
			}

		/** */
		sync	uint8_t getRPDS_L value;
		}

	/**  Write the RPDS_L register
	 */
	message	SetRPDS_L {
		fields{
			/** */
			input	uint8_t	value;
			}

		/** */
		sync	void setRPDS_L;
		}

	/**  Read the RPDS_H register.
	 */
	message	GetRPDS_H {
		fields{
			/** */
			output	uint8_t	value;
			}

		/** */
		sync	uint8_t getRPDS_H value;
		}

	/**  Write the RPDS_H register
	 */
	message	SetRPDS_H {
		fields{
			/** */
			input	uint8_t	value;
			}

		/** */
		sync	void setRPDS_H;
		}

	/**  Read the PRESS_OUT registers.
	 */
	message	GetPRESS_OUT {
		fields{
			/** */
			inout	void*	buffer;
			/** */
			input	uint8_t	maxBufferLength;
			}

		/** */
		sync	void getPRESS_OUT;
		}

	/**  Read the TEMP_OUT registers.
	 */
	message	GetTEMP_OUT {
		fields{
			/** */
			inout	void*	buffer;
			/** */
			input	uint8_t	maxBufferLength;
			}

		/** */
		sync	void getTEMP_OUT;
		}

	}
}
}
}
}
}
