/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_driver_st_lps25h_register_itc_respmemh_
#define _oscl_driver_st_lps25h_register_itc_respmemh_

#include "respapi.h"
#include "oscl/memory/block.h"

/**	These record types in this file describe blocks of memory
	that are large enough and appropriately aligned such that
	placement new operations creating instances of their
	corresponding objects can safely be performed. These records
	are useful to most clients that use the server's ITC
	interface asynchronously.
 */

/** */
namespace Oscl {

/** */
namespace Driver {

/** */
namespace ST {

/** */
namespace LPS25H {

/** */
namespace Register {

/** */
namespace Resp {

/**	This record describes a block of memory that is
	large enough (and properly aligned) to hold a
	GetTEMP_OUTResp and its corresponding Req::Api::GetTEMP_OUTPayload.
	Clients which use the corresponding server asynchronously
	frequently need to reservere/manage memory for for these
	ITC interactions and instances of this record are useful
	for that purpose.
 */
struct GetTEMP_OUTMem {
	/**	This is memory enough for the response message.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Resp::Api::GetTEMP_OUTResp)>	resp;

	/**	This is memory enough for the message payload.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Req::Api::GetTEMP_OUTPayload)>	payload;

	/**	This operation creates a response message using this
		 block of memory.
	 */
	Oscl::Driver::ST::LPS25H::Register::Resp::Api::GetTEMP_OUTResp&
		build(	Oscl::Driver::ST::LPS25H::Register::Req::Api::SAP&	sap,
				Oscl::Driver::ST::LPS25H::Register::Resp::Api&		respApi,
				Oscl::Mt::Itc::PostMsgApi&		clientPapi,
				uint8_t	maxBufferLength,
				void*	buffer
				) noexcept;

	};

/**	This record describes a block of memory that is
	large enough (and properly aligned) to hold a
	GetPRESS_OUTResp and its corresponding Req::Api::GetPRESS_OUTPayload.
	Clients which use the corresponding server asynchronously
	frequently need to reservere/manage memory for for these
	ITC interactions and instances of this record are useful
	for that purpose.
 */
struct GetPRESS_OUTMem {
	/**	This is memory enough for the response message.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Resp::Api::GetPRESS_OUTResp)>	resp;

	/**	This is memory enough for the message payload.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Req::Api::GetPRESS_OUTPayload)>	payload;

	/**	This operation creates a response message using this
		 block of memory.
	 */
	Oscl::Driver::ST::LPS25H::Register::Resp::Api::GetPRESS_OUTResp&
		build(	Oscl::Driver::ST::LPS25H::Register::Req::Api::SAP&	sap,
				Oscl::Driver::ST::LPS25H::Register::Resp::Api&		respApi,
				Oscl::Mt::Itc::PostMsgApi&		clientPapi,
				uint8_t	maxBufferLength,
				void*	buffer
				) noexcept;

	};

/**	This record describes a block of memory that is
	large enough (and properly aligned) to hold a
	SetRPDS_HResp and its corresponding Req::Api::SetRPDS_HPayload.
	Clients which use the corresponding server asynchronously
	frequently need to reservere/manage memory for for these
	ITC interactions and instances of this record are useful
	for that purpose.
 */
struct SetRPDS_HMem {
	/**	This is memory enough for the response message.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Resp::Api::SetRPDS_HResp)>	resp;

	/**	This is memory enough for the message payload.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Req::Api::SetRPDS_HPayload)>	payload;

	/**	This operation creates a response message using this
		 block of memory.
	 */
	Oscl::Driver::ST::LPS25H::Register::Resp::Api::SetRPDS_HResp&
		build(	Oscl::Driver::ST::LPS25H::Register::Req::Api::SAP&	sap,
				Oscl::Driver::ST::LPS25H::Register::Resp::Api&		respApi,
				Oscl::Mt::Itc::PostMsgApi&		clientPapi,
				uint8_t	value
				) noexcept;

	};

/**	This record describes a block of memory that is
	large enough (and properly aligned) to hold a
	GetRPDS_HResp and its corresponding Req::Api::GetRPDS_HPayload.
	Clients which use the corresponding server asynchronously
	frequently need to reservere/manage memory for for these
	ITC interactions and instances of this record are useful
	for that purpose.
 */
struct GetRPDS_HMem {
	/**	This is memory enough for the response message.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Resp::Api::GetRPDS_HResp)>	resp;

	/**	This is memory enough for the message payload.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Req::Api::GetRPDS_HPayload)>	payload;

	/**	This operation creates a response message using this
		 block of memory.
	 */
	Oscl::Driver::ST::LPS25H::Register::Resp::Api::GetRPDS_HResp&
		build(	Oscl::Driver::ST::LPS25H::Register::Req::Api::SAP&	sap,
				Oscl::Driver::ST::LPS25H::Register::Resp::Api&		respApi,
				Oscl::Mt::Itc::PostMsgApi&		clientPapi
				) noexcept;

	};

/**	This record describes a block of memory that is
	large enough (and properly aligned) to hold a
	SetRPDS_LResp and its corresponding Req::Api::SetRPDS_LPayload.
	Clients which use the corresponding server asynchronously
	frequently need to reservere/manage memory for for these
	ITC interactions and instances of this record are useful
	for that purpose.
 */
struct SetRPDS_LMem {
	/**	This is memory enough for the response message.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Resp::Api::SetRPDS_LResp)>	resp;

	/**	This is memory enough for the message payload.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Req::Api::SetRPDS_LPayload)>	payload;

	/**	This operation creates a response message using this
		 block of memory.
	 */
	Oscl::Driver::ST::LPS25H::Register::Resp::Api::SetRPDS_LResp&
		build(	Oscl::Driver::ST::LPS25H::Register::Req::Api::SAP&	sap,
				Oscl::Driver::ST::LPS25H::Register::Resp::Api&		respApi,
				Oscl::Mt::Itc::PostMsgApi&		clientPapi,
				uint8_t	value
				) noexcept;

	};

/**	This record describes a block of memory that is
	large enough (and properly aligned) to hold a
	GetRPDS_LResp and its corresponding Req::Api::GetRPDS_LPayload.
	Clients which use the corresponding server asynchronously
	frequently need to reservere/manage memory for for these
	ITC interactions and instances of this record are useful
	for that purpose.
 */
struct GetRPDS_LMem {
	/**	This is memory enough for the response message.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Resp::Api::GetRPDS_LResp)>	resp;

	/**	This is memory enough for the message payload.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Req::Api::GetRPDS_LPayload)>	payload;

	/**	This operation creates a response message using this
		 block of memory.
	 */
	Oscl::Driver::ST::LPS25H::Register::Resp::Api::GetRPDS_LResp&
		build(	Oscl::Driver::ST::LPS25H::Register::Req::Api::SAP&	sap,
				Oscl::Driver::ST::LPS25H::Register::Resp::Api&		respApi,
				Oscl::Mt::Itc::PostMsgApi&		clientPapi
				) noexcept;

	};

/**	This record describes a block of memory that is
	large enough (and properly aligned) to hold a
	SetTHS_P_HResp and its corresponding Req::Api::SetTHS_P_HPayload.
	Clients which use the corresponding server asynchronously
	frequently need to reservere/manage memory for for these
	ITC interactions and instances of this record are useful
	for that purpose.
 */
struct SetTHS_P_HMem {
	/**	This is memory enough for the response message.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Resp::Api::SetTHS_P_HResp)>	resp;

	/**	This is memory enough for the message payload.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Req::Api::SetTHS_P_HPayload)>	payload;

	/**	This operation creates a response message using this
		 block of memory.
	 */
	Oscl::Driver::ST::LPS25H::Register::Resp::Api::SetTHS_P_HResp&
		build(	Oscl::Driver::ST::LPS25H::Register::Req::Api::SAP&	sap,
				Oscl::Driver::ST::LPS25H::Register::Resp::Api&		respApi,
				Oscl::Mt::Itc::PostMsgApi&		clientPapi,
				uint8_t	value
				) noexcept;

	};

/**	This record describes a block of memory that is
	large enough (and properly aligned) to hold a
	GetTHS_P_HResp and its corresponding Req::Api::GetTHS_P_HPayload.
	Clients which use the corresponding server asynchronously
	frequently need to reservere/manage memory for for these
	ITC interactions and instances of this record are useful
	for that purpose.
 */
struct GetTHS_P_HMem {
	/**	This is memory enough for the response message.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Resp::Api::GetTHS_P_HResp)>	resp;

	/**	This is memory enough for the message payload.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Req::Api::GetTHS_P_HPayload)>	payload;

	/**	This operation creates a response message using this
		 block of memory.
	 */
	Oscl::Driver::ST::LPS25H::Register::Resp::Api::GetTHS_P_HResp&
		build(	Oscl::Driver::ST::LPS25H::Register::Req::Api::SAP&	sap,
				Oscl::Driver::ST::LPS25H::Register::Resp::Api&		respApi,
				Oscl::Mt::Itc::PostMsgApi&		clientPapi
				) noexcept;

	};

/**	This record describes a block of memory that is
	large enough (and properly aligned) to hold a
	SetTHS_P_LResp and its corresponding Req::Api::SetTHS_P_LPayload.
	Clients which use the corresponding server asynchronously
	frequently need to reservere/manage memory for for these
	ITC interactions and instances of this record are useful
	for that purpose.
 */
struct SetTHS_P_LMem {
	/**	This is memory enough for the response message.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Resp::Api::SetTHS_P_LResp)>	resp;

	/**	This is memory enough for the message payload.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Req::Api::SetTHS_P_LPayload)>	payload;

	/**	This operation creates a response message using this
		 block of memory.
	 */
	Oscl::Driver::ST::LPS25H::Register::Resp::Api::SetTHS_P_LResp&
		build(	Oscl::Driver::ST::LPS25H::Register::Req::Api::SAP&	sap,
				Oscl::Driver::ST::LPS25H::Register::Resp::Api&		respApi,
				Oscl::Mt::Itc::PostMsgApi&		clientPapi,
				uint8_t	value
				) noexcept;

	};

/**	This record describes a block of memory that is
	large enough (and properly aligned) to hold a
	GetTHS_P_LResp and its corresponding Req::Api::GetTHS_P_LPayload.
	Clients which use the corresponding server asynchronously
	frequently need to reservere/manage memory for for these
	ITC interactions and instances of this record are useful
	for that purpose.
 */
struct GetTHS_P_LMem {
	/**	This is memory enough for the response message.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Resp::Api::GetTHS_P_LResp)>	resp;

	/**	This is memory enough for the message payload.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Req::Api::GetTHS_P_LPayload)>	payload;

	/**	This operation creates a response message using this
		 block of memory.
	 */
	Oscl::Driver::ST::LPS25H::Register::Resp::Api::GetTHS_P_LResp&
		build(	Oscl::Driver::ST::LPS25H::Register::Req::Api::SAP&	sap,
				Oscl::Driver::ST::LPS25H::Register::Resp::Api&		respApi,
				Oscl::Mt::Itc::PostMsgApi&		clientPapi
				) noexcept;

	};

/**	This record describes a block of memory that is
	large enough (and properly aligned) to hold a
	GetFIFO_STATUSResp and its corresponding Req::Api::GetFIFO_STATUSPayload.
	Clients which use the corresponding server asynchronously
	frequently need to reservere/manage memory for for these
	ITC interactions and instances of this record are useful
	for that purpose.
 */
struct GetFIFO_STATUSMem {
	/**	This is memory enough for the response message.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Resp::Api::GetFIFO_STATUSResp)>	resp;

	/**	This is memory enough for the message payload.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Req::Api::GetFIFO_STATUSPayload)>	payload;

	/**	This operation creates a response message using this
		 block of memory.
	 */
	Oscl::Driver::ST::LPS25H::Register::Resp::Api::GetFIFO_STATUSResp&
		build(	Oscl::Driver::ST::LPS25H::Register::Req::Api::SAP&	sap,
				Oscl::Driver::ST::LPS25H::Register::Resp::Api&		respApi,
				Oscl::Mt::Itc::PostMsgApi&		clientPapi
				) noexcept;

	};

/**	This record describes a block of memory that is
	large enough (and properly aligned) to hold a
	SetFIFO_CTRLResp and its corresponding Req::Api::SetFIFO_CTRLPayload.
	Clients which use the corresponding server asynchronously
	frequently need to reservere/manage memory for for these
	ITC interactions and instances of this record are useful
	for that purpose.
 */
struct SetFIFO_CTRLMem {
	/**	This is memory enough for the response message.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Resp::Api::SetFIFO_CTRLResp)>	resp;

	/**	This is memory enough for the message payload.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Req::Api::SetFIFO_CTRLPayload)>	payload;

	/**	This operation creates a response message using this
		 block of memory.
	 */
	Oscl::Driver::ST::LPS25H::Register::Resp::Api::SetFIFO_CTRLResp&
		build(	Oscl::Driver::ST::LPS25H::Register::Req::Api::SAP&	sap,
				Oscl::Driver::ST::LPS25H::Register::Resp::Api&		respApi,
				Oscl::Mt::Itc::PostMsgApi&		clientPapi,
				uint8_t	value
				) noexcept;

	};

/**	This record describes a block of memory that is
	large enough (and properly aligned) to hold a
	GetFIFO_CTRLResp and its corresponding Req::Api::GetFIFO_CTRLPayload.
	Clients which use the corresponding server asynchronously
	frequently need to reservere/manage memory for for these
	ITC interactions and instances of this record are useful
	for that purpose.
 */
struct GetFIFO_CTRLMem {
	/**	This is memory enough for the response message.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Resp::Api::GetFIFO_CTRLResp)>	resp;

	/**	This is memory enough for the message payload.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Req::Api::GetFIFO_CTRLPayload)>	payload;

	/**	This operation creates a response message using this
		 block of memory.
	 */
	Oscl::Driver::ST::LPS25H::Register::Resp::Api::GetFIFO_CTRLResp&
		build(	Oscl::Driver::ST::LPS25H::Register::Req::Api::SAP&	sap,
				Oscl::Driver::ST::LPS25H::Register::Resp::Api&		respApi,
				Oscl::Mt::Itc::PostMsgApi&		clientPapi
				) noexcept;

	};

/**	This record describes a block of memory that is
	large enough (and properly aligned) to hold a
	GetTEMP_OUT_HResp and its corresponding Req::Api::GetTEMP_OUT_HPayload.
	Clients which use the corresponding server asynchronously
	frequently need to reservere/manage memory for for these
	ITC interactions and instances of this record are useful
	for that purpose.
 */
struct GetTEMP_OUT_HMem {
	/**	This is memory enough for the response message.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Resp::Api::GetTEMP_OUT_HResp)>	resp;

	/**	This is memory enough for the message payload.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Req::Api::GetTEMP_OUT_HPayload)>	payload;

	/**	This operation creates a response message using this
		 block of memory.
	 */
	Oscl::Driver::ST::LPS25H::Register::Resp::Api::GetTEMP_OUT_HResp&
		build(	Oscl::Driver::ST::LPS25H::Register::Req::Api::SAP&	sap,
				Oscl::Driver::ST::LPS25H::Register::Resp::Api&		respApi,
				Oscl::Mt::Itc::PostMsgApi&		clientPapi
				) noexcept;

	};

/**	This record describes a block of memory that is
	large enough (and properly aligned) to hold a
	GetTEMP_OUT_LResp and its corresponding Req::Api::GetTEMP_OUT_LPayload.
	Clients which use the corresponding server asynchronously
	frequently need to reservere/manage memory for for these
	ITC interactions and instances of this record are useful
	for that purpose.
 */
struct GetTEMP_OUT_LMem {
	/**	This is memory enough for the response message.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Resp::Api::GetTEMP_OUT_LResp)>	resp;

	/**	This is memory enough for the message payload.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Req::Api::GetTEMP_OUT_LPayload)>	payload;

	/**	This operation creates a response message using this
		 block of memory.
	 */
	Oscl::Driver::ST::LPS25H::Register::Resp::Api::GetTEMP_OUT_LResp&
		build(	Oscl::Driver::ST::LPS25H::Register::Req::Api::SAP&	sap,
				Oscl::Driver::ST::LPS25H::Register::Resp::Api&		respApi,
				Oscl::Mt::Itc::PostMsgApi&		clientPapi
				) noexcept;

	};

/**	This record describes a block of memory that is
	large enough (and properly aligned) to hold a
	GetPRESS_OUT_HResp and its corresponding Req::Api::GetPRESS_OUT_HPayload.
	Clients which use the corresponding server asynchronously
	frequently need to reservere/manage memory for for these
	ITC interactions and instances of this record are useful
	for that purpose.
 */
struct GetPRESS_OUT_HMem {
	/**	This is memory enough for the response message.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Resp::Api::GetPRESS_OUT_HResp)>	resp;

	/**	This is memory enough for the message payload.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Req::Api::GetPRESS_OUT_HPayload)>	payload;

	/**	This operation creates a response message using this
		 block of memory.
	 */
	Oscl::Driver::ST::LPS25H::Register::Resp::Api::GetPRESS_OUT_HResp&
		build(	Oscl::Driver::ST::LPS25H::Register::Req::Api::SAP&	sap,
				Oscl::Driver::ST::LPS25H::Register::Resp::Api&		respApi,
				Oscl::Mt::Itc::PostMsgApi&		clientPapi
				) noexcept;

	};

/**	This record describes a block of memory that is
	large enough (and properly aligned) to hold a
	GetPRESS_OUT_LResp and its corresponding Req::Api::GetPRESS_OUT_LPayload.
	Clients which use the corresponding server asynchronously
	frequently need to reservere/manage memory for for these
	ITC interactions and instances of this record are useful
	for that purpose.
 */
struct GetPRESS_OUT_LMem {
	/**	This is memory enough for the response message.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Resp::Api::GetPRESS_OUT_LResp)>	resp;

	/**	This is memory enough for the message payload.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Req::Api::GetPRESS_OUT_LPayload)>	payload;

	/**	This operation creates a response message using this
		 block of memory.
	 */
	Oscl::Driver::ST::LPS25H::Register::Resp::Api::GetPRESS_OUT_LResp&
		build(	Oscl::Driver::ST::LPS25H::Register::Req::Api::SAP&	sap,
				Oscl::Driver::ST::LPS25H::Register::Resp::Api&		respApi,
				Oscl::Mt::Itc::PostMsgApi&		clientPapi
				) noexcept;

	};

/**	This record describes a block of memory that is
	large enough (and properly aligned) to hold a
	GetPRESS_OUT_XLResp and its corresponding Req::Api::GetPRESS_OUT_XLPayload.
	Clients which use the corresponding server asynchronously
	frequently need to reservere/manage memory for for these
	ITC interactions and instances of this record are useful
	for that purpose.
 */
struct GetPRESS_OUT_XLMem {
	/**	This is memory enough for the response message.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Resp::Api::GetPRESS_OUT_XLResp)>	resp;

	/**	This is memory enough for the message payload.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Req::Api::GetPRESS_OUT_XLPayload)>	payload;

	/**	This operation creates a response message using this
		 block of memory.
	 */
	Oscl::Driver::ST::LPS25H::Register::Resp::Api::GetPRESS_OUT_XLResp&
		build(	Oscl::Driver::ST::LPS25H::Register::Req::Api::SAP&	sap,
				Oscl::Driver::ST::LPS25H::Register::Resp::Api&		respApi,
				Oscl::Mt::Itc::PostMsgApi&		clientPapi
				) noexcept;

	};

/**	This record describes a block of memory that is
	large enough (and properly aligned) to hold a
	GetSTATUS_REGResp and its corresponding Req::Api::GetSTATUS_REGPayload.
	Clients which use the corresponding server asynchronously
	frequently need to reservere/manage memory for for these
	ITC interactions and instances of this record are useful
	for that purpose.
 */
struct GetSTATUS_REGMem {
	/**	This is memory enough for the response message.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Resp::Api::GetSTATUS_REGResp)>	resp;

	/**	This is memory enough for the message payload.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Req::Api::GetSTATUS_REGPayload)>	payload;

	/**	This operation creates a response message using this
		 block of memory.
	 */
	Oscl::Driver::ST::LPS25H::Register::Resp::Api::GetSTATUS_REGResp&
		build(	Oscl::Driver::ST::LPS25H::Register::Req::Api::SAP&	sap,
				Oscl::Driver::ST::LPS25H::Register::Resp::Api&		respApi,
				Oscl::Mt::Itc::PostMsgApi&		clientPapi
				) noexcept;

	};

/**	This record describes a block of memory that is
	large enough (and properly aligned) to hold a
	SetINT_SOURCEResp and its corresponding Req::Api::SetINT_SOURCEPayload.
	Clients which use the corresponding server asynchronously
	frequently need to reservere/manage memory for for these
	ITC interactions and instances of this record are useful
	for that purpose.
 */
struct SetINT_SOURCEMem {
	/**	This is memory enough for the response message.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Resp::Api::SetINT_SOURCEResp)>	resp;

	/**	This is memory enough for the message payload.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Req::Api::SetINT_SOURCEPayload)>	payload;

	/**	This operation creates a response message using this
		 block of memory.
	 */
	Oscl::Driver::ST::LPS25H::Register::Resp::Api::SetINT_SOURCEResp&
		build(	Oscl::Driver::ST::LPS25H::Register::Req::Api::SAP&	sap,
				Oscl::Driver::ST::LPS25H::Register::Resp::Api&		respApi,
				Oscl::Mt::Itc::PostMsgApi&		clientPapi,
				uint8_t	value
				) noexcept;

	};

/**	This record describes a block of memory that is
	large enough (and properly aligned) to hold a
	GetINT_SOURCEResp and its corresponding Req::Api::GetINT_SOURCEPayload.
	Clients which use the corresponding server asynchronously
	frequently need to reservere/manage memory for for these
	ITC interactions and instances of this record are useful
	for that purpose.
 */
struct GetINT_SOURCEMem {
	/**	This is memory enough for the response message.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Resp::Api::GetINT_SOURCEResp)>	resp;

	/**	This is memory enough for the message payload.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Req::Api::GetINT_SOURCEPayload)>	payload;

	/**	This operation creates a response message using this
		 block of memory.
	 */
	Oscl::Driver::ST::LPS25H::Register::Resp::Api::GetINT_SOURCEResp&
		build(	Oscl::Driver::ST::LPS25H::Register::Req::Api::SAP&	sap,
				Oscl::Driver::ST::LPS25H::Register::Resp::Api&		respApi,
				Oscl::Mt::Itc::PostMsgApi&		clientPapi
				) noexcept;

	};

/**	This record describes a block of memory that is
	large enough (and properly aligned) to hold a
	SetINT_CFGResp and its corresponding Req::Api::SetINT_CFGPayload.
	Clients which use the corresponding server asynchronously
	frequently need to reservere/manage memory for for these
	ITC interactions and instances of this record are useful
	for that purpose.
 */
struct SetINT_CFGMem {
	/**	This is memory enough for the response message.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Resp::Api::SetINT_CFGResp)>	resp;

	/**	This is memory enough for the message payload.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Req::Api::SetINT_CFGPayload)>	payload;

	/**	This operation creates a response message using this
		 block of memory.
	 */
	Oscl::Driver::ST::LPS25H::Register::Resp::Api::SetINT_CFGResp&
		build(	Oscl::Driver::ST::LPS25H::Register::Req::Api::SAP&	sap,
				Oscl::Driver::ST::LPS25H::Register::Resp::Api&		respApi,
				Oscl::Mt::Itc::PostMsgApi&		clientPapi,
				uint8_t	value
				) noexcept;

	};

/**	This record describes a block of memory that is
	large enough (and properly aligned) to hold a
	GetINT_CFGResp and its corresponding Req::Api::GetINT_CFGPayload.
	Clients which use the corresponding server asynchronously
	frequently need to reservere/manage memory for for these
	ITC interactions and instances of this record are useful
	for that purpose.
 */
struct GetINT_CFGMem {
	/**	This is memory enough for the response message.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Resp::Api::GetINT_CFGResp)>	resp;

	/**	This is memory enough for the message payload.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Req::Api::GetINT_CFGPayload)>	payload;

	/**	This operation creates a response message using this
		 block of memory.
	 */
	Oscl::Driver::ST::LPS25H::Register::Resp::Api::GetINT_CFGResp&
		build(	Oscl::Driver::ST::LPS25H::Register::Req::Api::SAP&	sap,
				Oscl::Driver::ST::LPS25H::Register::Resp::Api&		respApi,
				Oscl::Mt::Itc::PostMsgApi&		clientPapi
				) noexcept;

	};

/**	This record describes a block of memory that is
	large enough (and properly aligned) to hold a
	SetCTRL_REG4Resp and its corresponding Req::Api::SetCTRL_REG4Payload.
	Clients which use the corresponding server asynchronously
	frequently need to reservere/manage memory for for these
	ITC interactions and instances of this record are useful
	for that purpose.
 */
struct SetCTRL_REG4Mem {
	/**	This is memory enough for the response message.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Resp::Api::SetCTRL_REG4Resp)>	resp;

	/**	This is memory enough for the message payload.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Req::Api::SetCTRL_REG4Payload)>	payload;

	/**	This operation creates a response message using this
		 block of memory.
	 */
	Oscl::Driver::ST::LPS25H::Register::Resp::Api::SetCTRL_REG4Resp&
		build(	Oscl::Driver::ST::LPS25H::Register::Req::Api::SAP&	sap,
				Oscl::Driver::ST::LPS25H::Register::Resp::Api&		respApi,
				Oscl::Mt::Itc::PostMsgApi&		clientPapi,
				uint8_t	value
				) noexcept;

	};

/**	This record describes a block of memory that is
	large enough (and properly aligned) to hold a
	GetCTRL_REG4Resp and its corresponding Req::Api::GetCTRL_REG4Payload.
	Clients which use the corresponding server asynchronously
	frequently need to reservere/manage memory for for these
	ITC interactions and instances of this record are useful
	for that purpose.
 */
struct GetCTRL_REG4Mem {
	/**	This is memory enough for the response message.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Resp::Api::GetCTRL_REG4Resp)>	resp;

	/**	This is memory enough for the message payload.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Req::Api::GetCTRL_REG4Payload)>	payload;

	/**	This operation creates a response message using this
		 block of memory.
	 */
	Oscl::Driver::ST::LPS25H::Register::Resp::Api::GetCTRL_REG4Resp&
		build(	Oscl::Driver::ST::LPS25H::Register::Req::Api::SAP&	sap,
				Oscl::Driver::ST::LPS25H::Register::Resp::Api&		respApi,
				Oscl::Mt::Itc::PostMsgApi&		clientPapi
				) noexcept;

	};

/**	This record describes a block of memory that is
	large enough (and properly aligned) to hold a
	SetCTRL_REG3Resp and its corresponding Req::Api::SetCTRL_REG3Payload.
	Clients which use the corresponding server asynchronously
	frequently need to reservere/manage memory for for these
	ITC interactions and instances of this record are useful
	for that purpose.
 */
struct SetCTRL_REG3Mem {
	/**	This is memory enough for the response message.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Resp::Api::SetCTRL_REG3Resp)>	resp;

	/**	This is memory enough for the message payload.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Req::Api::SetCTRL_REG3Payload)>	payload;

	/**	This operation creates a response message using this
		 block of memory.
	 */
	Oscl::Driver::ST::LPS25H::Register::Resp::Api::SetCTRL_REG3Resp&
		build(	Oscl::Driver::ST::LPS25H::Register::Req::Api::SAP&	sap,
				Oscl::Driver::ST::LPS25H::Register::Resp::Api&		respApi,
				Oscl::Mt::Itc::PostMsgApi&		clientPapi,
				uint8_t	value
				) noexcept;

	};

/**	This record describes a block of memory that is
	large enough (and properly aligned) to hold a
	GetCTRL_REG3Resp and its corresponding Req::Api::GetCTRL_REG3Payload.
	Clients which use the corresponding server asynchronously
	frequently need to reservere/manage memory for for these
	ITC interactions and instances of this record are useful
	for that purpose.
 */
struct GetCTRL_REG3Mem {
	/**	This is memory enough for the response message.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Resp::Api::GetCTRL_REG3Resp)>	resp;

	/**	This is memory enough for the message payload.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Req::Api::GetCTRL_REG3Payload)>	payload;

	/**	This operation creates a response message using this
		 block of memory.
	 */
	Oscl::Driver::ST::LPS25H::Register::Resp::Api::GetCTRL_REG3Resp&
		build(	Oscl::Driver::ST::LPS25H::Register::Req::Api::SAP&	sap,
				Oscl::Driver::ST::LPS25H::Register::Resp::Api&		respApi,
				Oscl::Mt::Itc::PostMsgApi&		clientPapi
				) noexcept;

	};

/**	This record describes a block of memory that is
	large enough (and properly aligned) to hold a
	SetCTRL_REG2Resp and its corresponding Req::Api::SetCTRL_REG2Payload.
	Clients which use the corresponding server asynchronously
	frequently need to reservere/manage memory for for these
	ITC interactions and instances of this record are useful
	for that purpose.
 */
struct SetCTRL_REG2Mem {
	/**	This is memory enough for the response message.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Resp::Api::SetCTRL_REG2Resp)>	resp;

	/**	This is memory enough for the message payload.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Req::Api::SetCTRL_REG2Payload)>	payload;

	/**	This operation creates a response message using this
		 block of memory.
	 */
	Oscl::Driver::ST::LPS25H::Register::Resp::Api::SetCTRL_REG2Resp&
		build(	Oscl::Driver::ST::LPS25H::Register::Req::Api::SAP&	sap,
				Oscl::Driver::ST::LPS25H::Register::Resp::Api&		respApi,
				Oscl::Mt::Itc::PostMsgApi&		clientPapi,
				uint8_t	value
				) noexcept;

	};

/**	This record describes a block of memory that is
	large enough (and properly aligned) to hold a
	GetCTRL_REG2Resp and its corresponding Req::Api::GetCTRL_REG2Payload.
	Clients which use the corresponding server asynchronously
	frequently need to reservere/manage memory for for these
	ITC interactions and instances of this record are useful
	for that purpose.
 */
struct GetCTRL_REG2Mem {
	/**	This is memory enough for the response message.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Resp::Api::GetCTRL_REG2Resp)>	resp;

	/**	This is memory enough for the message payload.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Req::Api::GetCTRL_REG2Payload)>	payload;

	/**	This operation creates a response message using this
		 block of memory.
	 */
	Oscl::Driver::ST::LPS25H::Register::Resp::Api::GetCTRL_REG2Resp&
		build(	Oscl::Driver::ST::LPS25H::Register::Req::Api::SAP&	sap,
				Oscl::Driver::ST::LPS25H::Register::Resp::Api&		respApi,
				Oscl::Mt::Itc::PostMsgApi&		clientPapi
				) noexcept;

	};

/**	This record describes a block of memory that is
	large enough (and properly aligned) to hold a
	SetCTRL_REG1Resp and its corresponding Req::Api::SetCTRL_REG1Payload.
	Clients which use the corresponding server asynchronously
	frequently need to reservere/manage memory for for these
	ITC interactions and instances of this record are useful
	for that purpose.
 */
struct SetCTRL_REG1Mem {
	/**	This is memory enough for the response message.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Resp::Api::SetCTRL_REG1Resp)>	resp;

	/**	This is memory enough for the message payload.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Req::Api::SetCTRL_REG1Payload)>	payload;

	/**	This operation creates a response message using this
		 block of memory.
	 */
	Oscl::Driver::ST::LPS25H::Register::Resp::Api::SetCTRL_REG1Resp&
		build(	Oscl::Driver::ST::LPS25H::Register::Req::Api::SAP&	sap,
				Oscl::Driver::ST::LPS25H::Register::Resp::Api&		respApi,
				Oscl::Mt::Itc::PostMsgApi&		clientPapi,
				uint8_t	value
				) noexcept;

	};

/**	This record describes a block of memory that is
	large enough (and properly aligned) to hold a
	GetCTRL_REG1Resp and its corresponding Req::Api::GetCTRL_REG1Payload.
	Clients which use the corresponding server asynchronously
	frequently need to reservere/manage memory for for these
	ITC interactions and instances of this record are useful
	for that purpose.
 */
struct GetCTRL_REG1Mem {
	/**	This is memory enough for the response message.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Resp::Api::GetCTRL_REG1Resp)>	resp;

	/**	This is memory enough for the message payload.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Req::Api::GetCTRL_REG1Payload)>	payload;

	/**	This operation creates a response message using this
		 block of memory.
	 */
	Oscl::Driver::ST::LPS25H::Register::Resp::Api::GetCTRL_REG1Resp&
		build(	Oscl::Driver::ST::LPS25H::Register::Req::Api::SAP&	sap,
				Oscl::Driver::ST::LPS25H::Register::Resp::Api&		respApi,
				Oscl::Mt::Itc::PostMsgApi&		clientPapi
				) noexcept;

	};

/**	This record describes a block of memory that is
	large enough (and properly aligned) to hold a
	SetRES_CONFResp and its corresponding Req::Api::SetRES_CONFPayload.
	Clients which use the corresponding server asynchronously
	frequently need to reservere/manage memory for for these
	ITC interactions and instances of this record are useful
	for that purpose.
 */
struct SetRES_CONFMem {
	/**	This is memory enough for the response message.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Resp::Api::SetRES_CONFResp)>	resp;

	/**	This is memory enough for the message payload.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Req::Api::SetRES_CONFPayload)>	payload;

	/**	This operation creates a response message using this
		 block of memory.
	 */
	Oscl::Driver::ST::LPS25H::Register::Resp::Api::SetRES_CONFResp&
		build(	Oscl::Driver::ST::LPS25H::Register::Req::Api::SAP&	sap,
				Oscl::Driver::ST::LPS25H::Register::Resp::Api&		respApi,
				Oscl::Mt::Itc::PostMsgApi&		clientPapi,
				uint8_t	value
				) noexcept;

	};

/**	This record describes a block of memory that is
	large enough (and properly aligned) to hold a
	GetRES_CONFResp and its corresponding Req::Api::GetRES_CONFPayload.
	Clients which use the corresponding server asynchronously
	frequently need to reservere/manage memory for for these
	ITC interactions and instances of this record are useful
	for that purpose.
 */
struct GetRES_CONFMem {
	/**	This is memory enough for the response message.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Resp::Api::GetRES_CONFResp)>	resp;

	/**	This is memory enough for the message payload.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Req::Api::GetRES_CONFPayload)>	payload;

	/**	This operation creates a response message using this
		 block of memory.
	 */
	Oscl::Driver::ST::LPS25H::Register::Resp::Api::GetRES_CONFResp&
		build(	Oscl::Driver::ST::LPS25H::Register::Req::Api::SAP&	sap,
				Oscl::Driver::ST::LPS25H::Register::Resp::Api&		respApi,
				Oscl::Mt::Itc::PostMsgApi&		clientPapi
				) noexcept;

	};

/**	This record describes a block of memory that is
	large enough (and properly aligned) to hold a
	GetWHO_AM_IResp and its corresponding Req::Api::GetWHO_AM_IPayload.
	Clients which use the corresponding server asynchronously
	frequently need to reservere/manage memory for for these
	ITC interactions and instances of this record are useful
	for that purpose.
 */
struct GetWHO_AM_IMem {
	/**	This is memory enough for the response message.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Resp::Api::GetWHO_AM_IResp)>	resp;

	/**	This is memory enough for the message payload.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Req::Api::GetWHO_AM_IPayload)>	payload;

	/**	This operation creates a response message using this
		 block of memory.
	 */
	Oscl::Driver::ST::LPS25H::Register::Resp::Api::GetWHO_AM_IResp&
		build(	Oscl::Driver::ST::LPS25H::Register::Req::Api::SAP&	sap,
				Oscl::Driver::ST::LPS25H::Register::Resp::Api&		respApi,
				Oscl::Mt::Itc::PostMsgApi&		clientPapi
				) noexcept;

	};

/**	This record describes a block of memory that is
	large enough (and properly aligned) to hold a
	SetREF_P_HResp and its corresponding Req::Api::SetREF_P_HPayload.
	Clients which use the corresponding server asynchronously
	frequently need to reservere/manage memory for for these
	ITC interactions and instances of this record are useful
	for that purpose.
 */
struct SetREF_P_HMem {
	/**	This is memory enough for the response message.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Resp::Api::SetREF_P_HResp)>	resp;

	/**	This is memory enough for the message payload.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Req::Api::SetREF_P_HPayload)>	payload;

	/**	This operation creates a response message using this
		 block of memory.
	 */
	Oscl::Driver::ST::LPS25H::Register::Resp::Api::SetREF_P_HResp&
		build(	Oscl::Driver::ST::LPS25H::Register::Req::Api::SAP&	sap,
				Oscl::Driver::ST::LPS25H::Register::Resp::Api&		respApi,
				Oscl::Mt::Itc::PostMsgApi&		clientPapi,
				uint8_t	value
				) noexcept;

	};

/**	This record describes a block of memory that is
	large enough (and properly aligned) to hold a
	GetREF_P_HResp and its corresponding Req::Api::GetREF_P_HPayload.
	Clients which use the corresponding server asynchronously
	frequently need to reservere/manage memory for for these
	ITC interactions and instances of this record are useful
	for that purpose.
 */
struct GetREF_P_HMem {
	/**	This is memory enough for the response message.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Resp::Api::GetREF_P_HResp)>	resp;

	/**	This is memory enough for the message payload.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Req::Api::GetREF_P_HPayload)>	payload;

	/**	This operation creates a response message using this
		 block of memory.
	 */
	Oscl::Driver::ST::LPS25H::Register::Resp::Api::GetREF_P_HResp&
		build(	Oscl::Driver::ST::LPS25H::Register::Req::Api::SAP&	sap,
				Oscl::Driver::ST::LPS25H::Register::Resp::Api&		respApi,
				Oscl::Mt::Itc::PostMsgApi&		clientPapi
				) noexcept;

	};

/**	This record describes a block of memory that is
	large enough (and properly aligned) to hold a
	SetREF_P_LResp and its corresponding Req::Api::SetREF_P_LPayload.
	Clients which use the corresponding server asynchronously
	frequently need to reservere/manage memory for for these
	ITC interactions and instances of this record are useful
	for that purpose.
 */
struct SetREF_P_LMem {
	/**	This is memory enough for the response message.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Resp::Api::SetREF_P_LResp)>	resp;

	/**	This is memory enough for the message payload.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Req::Api::SetREF_P_LPayload)>	payload;

	/**	This operation creates a response message using this
		 block of memory.
	 */
	Oscl::Driver::ST::LPS25H::Register::Resp::Api::SetREF_P_LResp&
		build(	Oscl::Driver::ST::LPS25H::Register::Req::Api::SAP&	sap,
				Oscl::Driver::ST::LPS25H::Register::Resp::Api&		respApi,
				Oscl::Mt::Itc::PostMsgApi&		clientPapi,
				uint8_t	value
				) noexcept;

	};

/**	This record describes a block of memory that is
	large enough (and properly aligned) to hold a
	GetREF_P_LResp and its corresponding Req::Api::GetREF_P_LPayload.
	Clients which use the corresponding server asynchronously
	frequently need to reservere/manage memory for for these
	ITC interactions and instances of this record are useful
	for that purpose.
 */
struct GetREF_P_LMem {
	/**	This is memory enough for the response message.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Resp::Api::GetREF_P_LResp)>	resp;

	/**	This is memory enough for the message payload.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Req::Api::GetREF_P_LPayload)>	payload;

	/**	This operation creates a response message using this
		 block of memory.
	 */
	Oscl::Driver::ST::LPS25H::Register::Resp::Api::GetREF_P_LResp&
		build(	Oscl::Driver::ST::LPS25H::Register::Req::Api::SAP&	sap,
				Oscl::Driver::ST::LPS25H::Register::Resp::Api&		respApi,
				Oscl::Mt::Itc::PostMsgApi&		clientPapi
				) noexcept;

	};

/**	This record describes a block of memory that is
	large enough (and properly aligned) to hold a
	SetREF_P_XLResp and its corresponding Req::Api::SetREF_P_XLPayload.
	Clients which use the corresponding server asynchronously
	frequently need to reservere/manage memory for for these
	ITC interactions and instances of this record are useful
	for that purpose.
 */
struct SetREF_P_XLMem {
	/**	This is memory enough for the response message.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Resp::Api::SetREF_P_XLResp)>	resp;

	/**	This is memory enough for the message payload.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Req::Api::SetREF_P_XLPayload)>	payload;

	/**	This operation creates a response message using this
		 block of memory.
	 */
	Oscl::Driver::ST::LPS25H::Register::Resp::Api::SetREF_P_XLResp&
		build(	Oscl::Driver::ST::LPS25H::Register::Req::Api::SAP&	sap,
				Oscl::Driver::ST::LPS25H::Register::Resp::Api&		respApi,
				Oscl::Mt::Itc::PostMsgApi&		clientPapi,
				uint8_t	value
				) noexcept;

	};

/**	This record describes a block of memory that is
	large enough (and properly aligned) to hold a
	GetREF_P_XLResp and its corresponding Req::Api::GetREF_P_XLPayload.
	Clients which use the corresponding server asynchronously
	frequently need to reservere/manage memory for for these
	ITC interactions and instances of this record are useful
	for that purpose.
 */
struct GetREF_P_XLMem {
	/**	This is memory enough for the response message.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Resp::Api::GetREF_P_XLResp)>	resp;

	/**	This is memory enough for the message payload.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Req::Api::GetREF_P_XLPayload)>	payload;

	/**	This operation creates a response message using this
		 block of memory.
	 */
	Oscl::Driver::ST::LPS25H::Register::Resp::Api::GetREF_P_XLResp&
		build(	Oscl::Driver::ST::LPS25H::Register::Req::Api::SAP&	sap,
				Oscl::Driver::ST::LPS25H::Register::Resp::Api&		respApi,
				Oscl::Mt::Itc::PostMsgApi&		clientPapi
				) noexcept;

	};

}
}
}
}
}
}
#endif
