/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "sync.h"
#include "oscl/mt/itc/mbox/syncrh.h"

using namespace Oscl::Driver::ST::LPS25H::Register;

Sync::Sync(	Oscl::Mt::Itc::PostMsgApi&	myPapi,
			Req::Api&				reqApi
			) noexcept:
		_sap(reqApi,myPapi)
		{
	}

Req::Api::SAP&	Sync::getSAP() noexcept{
	return _sap;
	}

void	Sync::getTEMP_OUT(	
						uint8_t	maxBufferLength,
						void*	buffer
						) noexcept{

	Oscl::Mt::Itc::SyncReturnHandler		srh;

	Req::Api::GetTEMP_OUTPayload		payload(
									maxBufferLength,
									buffer
									);

	Req::Api::GetTEMP_OUTReq	req(	_sap.getReqApi(),
									payload,
									srh
									);

	_sap.postSync(req);
	}

void	Sync::getPRESS_OUT(	
						uint8_t	maxBufferLength,
						void*	buffer
						) noexcept{

	Oscl::Mt::Itc::SyncReturnHandler		srh;

	Req::Api::GetPRESS_OUTPayload		payload(
									maxBufferLength,
									buffer
									);

	Req::Api::GetPRESS_OUTReq	req(	_sap.getReqApi(),
									payload,
									srh
									);

	_sap.postSync(req);
	}

void	Sync::setRPDS_H(	
						uint8_t	value
						) noexcept{

	Oscl::Mt::Itc::SyncReturnHandler		srh;

	Req::Api::SetRPDS_HPayload		payload(
									value
									);

	Req::Api::SetRPDS_HReq	req(	_sap.getReqApi(),
									payload,
									srh
									);

	_sap.postSync(req);
	}

uint8_t	Sync::getRPDS_H(	
						) noexcept{

	Oscl::Mt::Itc::SyncReturnHandler		srh;

	Req::Api::GetRPDS_HPayload		payload;

	Req::Api::GetRPDS_HReq	req(	_sap.getReqApi(),
									payload,
									srh
									);

	_sap.postSync(req);

	return payload._value;
	}

void	Sync::setRPDS_L(	
						uint8_t	value
						) noexcept{

	Oscl::Mt::Itc::SyncReturnHandler		srh;

	Req::Api::SetRPDS_LPayload		payload(
									value
									);

	Req::Api::SetRPDS_LReq	req(	_sap.getReqApi(),
									payload,
									srh
									);

	_sap.postSync(req);
	}

uint8_t	Sync::getRPDS_L(	
						) noexcept{

	Oscl::Mt::Itc::SyncReturnHandler		srh;

	Req::Api::GetRPDS_LPayload		payload;

	Req::Api::GetRPDS_LReq	req(	_sap.getReqApi(),
									payload,
									srh
									);

	_sap.postSync(req);

	return payload._value;
	}

void	Sync::setTHS_P_H(	
						uint8_t	value
						) noexcept{

	Oscl::Mt::Itc::SyncReturnHandler		srh;

	Req::Api::SetTHS_P_HPayload		payload(
									value
									);

	Req::Api::SetTHS_P_HReq	req(	_sap.getReqApi(),
									payload,
									srh
									);

	_sap.postSync(req);
	}

uint8_t	Sync::getTHS_P_H(	
						) noexcept{

	Oscl::Mt::Itc::SyncReturnHandler		srh;

	Req::Api::GetTHS_P_HPayload		payload;

	Req::Api::GetTHS_P_HReq	req(	_sap.getReqApi(),
									payload,
									srh
									);

	_sap.postSync(req);

	return payload._value;
	}

void	Sync::setTHS_P_L(	
						uint8_t	value
						) noexcept{

	Oscl::Mt::Itc::SyncReturnHandler		srh;

	Req::Api::SetTHS_P_LPayload		payload(
									value
									);

	Req::Api::SetTHS_P_LReq	req(	_sap.getReqApi(),
									payload,
									srh
									);

	_sap.postSync(req);
	}

uint8_t	Sync::getTHS_P_L(	
						) noexcept{

	Oscl::Mt::Itc::SyncReturnHandler		srh;

	Req::Api::GetTHS_P_LPayload		payload;

	Req::Api::GetTHS_P_LReq	req(	_sap.getReqApi(),
									payload,
									srh
									);

	_sap.postSync(req);

	return payload._value;
	}

uint8_t	Sync::getFIFO_STATUS(	
						) noexcept{

	Oscl::Mt::Itc::SyncReturnHandler		srh;

	Req::Api::GetFIFO_STATUSPayload		payload;

	Req::Api::GetFIFO_STATUSReq	req(	_sap.getReqApi(),
									payload,
									srh
									);

	_sap.postSync(req);

	return payload._value;
	}

void	Sync::setFIFO_CTRL(	
						uint8_t	value
						) noexcept{

	Oscl::Mt::Itc::SyncReturnHandler		srh;

	Req::Api::SetFIFO_CTRLPayload		payload(
									value
									);

	Req::Api::SetFIFO_CTRLReq	req(	_sap.getReqApi(),
									payload,
									srh
									);

	_sap.postSync(req);
	}

uint8_t	Sync::getFIFO_CTRL(	
						) noexcept{

	Oscl::Mt::Itc::SyncReturnHandler		srh;

	Req::Api::GetFIFO_CTRLPayload		payload;

	Req::Api::GetFIFO_CTRLReq	req(	_sap.getReqApi(),
									payload,
									srh
									);

	_sap.postSync(req);

	return payload._value;
	}

uint8_t	Sync::getTEMP_OUT_H(	
						) noexcept{

	Oscl::Mt::Itc::SyncReturnHandler		srh;

	Req::Api::GetTEMP_OUT_HPayload		payload;

	Req::Api::GetTEMP_OUT_HReq	req(	_sap.getReqApi(),
									payload,
									srh
									);

	_sap.postSync(req);

	return payload._value;
	}

uint8_t	Sync::getTEMP_OUT_L(	
						) noexcept{

	Oscl::Mt::Itc::SyncReturnHandler		srh;

	Req::Api::GetTEMP_OUT_LPayload		payload;

	Req::Api::GetTEMP_OUT_LReq	req(	_sap.getReqApi(),
									payload,
									srh
									);

	_sap.postSync(req);

	return payload._value;
	}

uint8_t	Sync::getPRESS_OUT_H(	
						) noexcept{

	Oscl::Mt::Itc::SyncReturnHandler		srh;

	Req::Api::GetPRESS_OUT_HPayload		payload;

	Req::Api::GetPRESS_OUT_HReq	req(	_sap.getReqApi(),
									payload,
									srh
									);

	_sap.postSync(req);

	return payload._value;
	}

uint8_t	Sync::getPRESS_OUT_L(	
						) noexcept{

	Oscl::Mt::Itc::SyncReturnHandler		srh;

	Req::Api::GetPRESS_OUT_LPayload		payload;

	Req::Api::GetPRESS_OUT_LReq	req(	_sap.getReqApi(),
									payload,
									srh
									);

	_sap.postSync(req);

	return payload._value;
	}

uint8_t	Sync::getPRESS_OUT_XL(	
						) noexcept{

	Oscl::Mt::Itc::SyncReturnHandler		srh;

	Req::Api::GetPRESS_OUT_XLPayload		payload;

	Req::Api::GetPRESS_OUT_XLReq	req(	_sap.getReqApi(),
									payload,
									srh
									);

	_sap.postSync(req);

	return payload._value;
	}

uint8_t	Sync::getSTATUS_REG(	
						) noexcept{

	Oscl::Mt::Itc::SyncReturnHandler		srh;

	Req::Api::GetSTATUS_REGPayload		payload;

	Req::Api::GetSTATUS_REGReq	req(	_sap.getReqApi(),
									payload,
									srh
									);

	_sap.postSync(req);

	return payload._value;
	}

void	Sync::setINT_SOURCE(	
						uint8_t	value
						) noexcept{

	Oscl::Mt::Itc::SyncReturnHandler		srh;

	Req::Api::SetINT_SOURCEPayload		payload(
									value
									);

	Req::Api::SetINT_SOURCEReq	req(	_sap.getReqApi(),
									payload,
									srh
									);

	_sap.postSync(req);
	}

uint8_t	Sync::getINT_SOURCE(	
						) noexcept{

	Oscl::Mt::Itc::SyncReturnHandler		srh;

	Req::Api::GetINT_SOURCEPayload		payload;

	Req::Api::GetINT_SOURCEReq	req(	_sap.getReqApi(),
									payload,
									srh
									);

	_sap.postSync(req);

	return payload._value;
	}

void	Sync::setINT_CFG(	
						uint8_t	value
						) noexcept{

	Oscl::Mt::Itc::SyncReturnHandler		srh;

	Req::Api::SetINT_CFGPayload		payload(
									value
									);

	Req::Api::SetINT_CFGReq	req(	_sap.getReqApi(),
									payload,
									srh
									);

	_sap.postSync(req);
	}

uint8_t	Sync::getINT_CFG(	
						) noexcept{

	Oscl::Mt::Itc::SyncReturnHandler		srh;

	Req::Api::GetINT_CFGPayload		payload;

	Req::Api::GetINT_CFGReq	req(	_sap.getReqApi(),
									payload,
									srh
									);

	_sap.postSync(req);

	return payload._value;
	}

void	Sync::setCTRL_REG4(	
						uint8_t	value
						) noexcept{

	Oscl::Mt::Itc::SyncReturnHandler		srh;

	Req::Api::SetCTRL_REG4Payload		payload(
									value
									);

	Req::Api::SetCTRL_REG4Req	req(	_sap.getReqApi(),
									payload,
									srh
									);

	_sap.postSync(req);
	}

uint8_t	Sync::getCTRL_REG4(	
						) noexcept{

	Oscl::Mt::Itc::SyncReturnHandler		srh;

	Req::Api::GetCTRL_REG4Payload		payload;

	Req::Api::GetCTRL_REG4Req	req(	_sap.getReqApi(),
									payload,
									srh
									);

	_sap.postSync(req);

	return payload._value;
	}

void	Sync::setCTRL_REG3(	
						uint8_t	value
						) noexcept{

	Oscl::Mt::Itc::SyncReturnHandler		srh;

	Req::Api::SetCTRL_REG3Payload		payload(
									value
									);

	Req::Api::SetCTRL_REG3Req	req(	_sap.getReqApi(),
									payload,
									srh
									);

	_sap.postSync(req);
	}

uint8_t	Sync::getCTRL_REG3(	
						) noexcept{

	Oscl::Mt::Itc::SyncReturnHandler		srh;

	Req::Api::GetCTRL_REG3Payload		payload;

	Req::Api::GetCTRL_REG3Req	req(	_sap.getReqApi(),
									payload,
									srh
									);

	_sap.postSync(req);

	return payload._value;
	}

void	Sync::setCTRL_REG2(	
						uint8_t	value
						) noexcept{

	Oscl::Mt::Itc::SyncReturnHandler		srh;

	Req::Api::SetCTRL_REG2Payload		payload(
									value
									);

	Req::Api::SetCTRL_REG2Req	req(	_sap.getReqApi(),
									payload,
									srh
									);

	_sap.postSync(req);
	}

uint8_t	Sync::getCTRL_REG2(	
						) noexcept{

	Oscl::Mt::Itc::SyncReturnHandler		srh;

	Req::Api::GetCTRL_REG2Payload		payload;

	Req::Api::GetCTRL_REG2Req	req(	_sap.getReqApi(),
									payload,
									srh
									);

	_sap.postSync(req);

	return payload._value;
	}

void	Sync::setCTRL_REG1(	
						uint8_t	value
						) noexcept{

	Oscl::Mt::Itc::SyncReturnHandler		srh;

	Req::Api::SetCTRL_REG1Payload		payload(
									value
									);

	Req::Api::SetCTRL_REG1Req	req(	_sap.getReqApi(),
									payload,
									srh
									);

	_sap.postSync(req);
	}

uint8_t	Sync::getCTRL_REG1(	
						) noexcept{

	Oscl::Mt::Itc::SyncReturnHandler		srh;

	Req::Api::GetCTRL_REG1Payload		payload;

	Req::Api::GetCTRL_REG1Req	req(	_sap.getReqApi(),
									payload,
									srh
									);

	_sap.postSync(req);

	return payload._value;
	}

void	Sync::setRES_CONF(	
						uint8_t	value
						) noexcept{

	Oscl::Mt::Itc::SyncReturnHandler		srh;

	Req::Api::SetRES_CONFPayload		payload(
									value
									);

	Req::Api::SetRES_CONFReq	req(	_sap.getReqApi(),
									payload,
									srh
									);

	_sap.postSync(req);
	}

uint8_t	Sync::getRES_CONF(	
						) noexcept{

	Oscl::Mt::Itc::SyncReturnHandler		srh;

	Req::Api::GetRES_CONFPayload		payload;

	Req::Api::GetRES_CONFReq	req(	_sap.getReqApi(),
									payload,
									srh
									);

	_sap.postSync(req);

	return payload._value;
	}

uint8_t	Sync::getWHO_AM_I(	
						) noexcept{

	Oscl::Mt::Itc::SyncReturnHandler		srh;

	Req::Api::GetWHO_AM_IPayload		payload;

	Req::Api::GetWHO_AM_IReq	req(	_sap.getReqApi(),
									payload,
									srh
									);

	_sap.postSync(req);

	return payload._value;
	}

void	Sync::setREF_P_H(	
						uint8_t	value
						) noexcept{

	Oscl::Mt::Itc::SyncReturnHandler		srh;

	Req::Api::SetREF_P_HPayload		payload(
									value
									);

	Req::Api::SetREF_P_HReq	req(	_sap.getReqApi(),
									payload,
									srh
									);

	_sap.postSync(req);
	}

uint8_t	Sync::getREF_P_H(	
						) noexcept{

	Oscl::Mt::Itc::SyncReturnHandler		srh;

	Req::Api::GetREF_P_HPayload		payload;

	Req::Api::GetREF_P_HReq	req(	_sap.getReqApi(),
									payload,
									srh
									);

	_sap.postSync(req);

	return payload._value;
	}

void	Sync::setREF_P_L(	
						uint8_t	value
						) noexcept{

	Oscl::Mt::Itc::SyncReturnHandler		srh;

	Req::Api::SetREF_P_LPayload		payload(
									value
									);

	Req::Api::SetREF_P_LReq	req(	_sap.getReqApi(),
									payload,
									srh
									);

	_sap.postSync(req);
	}

uint8_t	Sync::getREF_P_L(	
						) noexcept{

	Oscl::Mt::Itc::SyncReturnHandler		srh;

	Req::Api::GetREF_P_LPayload		payload;

	Req::Api::GetREF_P_LReq	req(	_sap.getReqApi(),
									payload,
									srh
									);

	_sap.postSync(req);

	return payload._value;
	}

void	Sync::setREF_P_XL(	
						uint8_t	value
						) noexcept{

	Oscl::Mt::Itc::SyncReturnHandler		srh;

	Req::Api::SetREF_P_XLPayload		payload(
									value
									);

	Req::Api::SetREF_P_XLReq	req(	_sap.getReqApi(),
									payload,
									srh
									);

	_sap.postSync(req);
	}

uint8_t	Sync::getREF_P_XL(	
						) noexcept{

	Oscl::Mt::Itc::SyncReturnHandler		srh;

	Req::Api::GetREF_P_XLPayload		payload;

	Req::Api::GetREF_P_XLReq	req(	_sap.getReqApi(),
									payload,
									srh
									);

	_sap.postSync(req);

	return payload._value;
	}

