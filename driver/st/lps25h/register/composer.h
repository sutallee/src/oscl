/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_driver_st_lps25h_register_composerh_
#define _oscl_driver_st_lps25h_register_composerh_

#include "api.h"

/** */
namespace Oscl {

/** */
namespace Driver {

/** */
namespace ST {

/** */
namespace LPS25H {

/** */
namespace Register {

/**	This template class implements a kind of GOF decorator
	pattern allows the context to employ composition instead
	of inheritance. Frequently, a context may need to implement
	more than one interface that may result in method name
	clashes. This template solves the problem by implementing
	the interface and then invoking member function pointers
	in the context instead. One instance of this object is
	created in the context for each interface of this type used
	in the context. Each instance is constructed such that it
	refers to different member functions within the context.
	The interface of this object is passed to any object that
	requires the interface. When the object invokes any of the
	operations of the API, the corresponding member function
	of the context will subsequently be invoked.
 */
template <class Context>
class Composer : public Api {
	private:
		/** A reference to the Context.
		 */
		Context&	_context;

	private:
		/**	
		 */
		void	(Context::*_getTEMP_OUT)(	
							uint8_t	maxBufferLength,
							void*	buffer
							);

		/**	
		 */
		void	(Context::*_getPRESS_OUT)(	
							uint8_t	maxBufferLength,
							void*	buffer
							);

		/**	
		 */
		void	(Context::*_setRPDS_H)(	
							uint8_t	value
							);

		/**	
		 */
		uint8_t	(Context::*_getRPDS_H)(	
							);

		/**	
		 */
		void	(Context::*_setRPDS_L)(	
							uint8_t	value
							);

		/**	
		 */
		uint8_t	(Context::*_getRPDS_L)(	
							);

		/**	
		 */
		void	(Context::*_setTHS_P_H)(	
							uint8_t	value
							);

		/**	
		 */
		uint8_t	(Context::*_getTHS_P_H)(	
							);

		/**	
		 */
		void	(Context::*_setTHS_P_L)(	
							uint8_t	value
							);

		/**	
		 */
		uint8_t	(Context::*_getTHS_P_L)(	
							);

		/**	
		 */
		uint8_t	(Context::*_getFIFO_STATUS)(	
							);

		/**	
		 */
		void	(Context::*_setFIFO_CTRL)(	
							uint8_t	value
							);

		/**	
		 */
		uint8_t	(Context::*_getFIFO_CTRL)(	
							);

		/**	
		 */
		uint8_t	(Context::*_getTEMP_OUT_H)(	
							);

		/**	
		 */
		uint8_t	(Context::*_getTEMP_OUT_L)(	
							);

		/**	
		 */
		uint8_t	(Context::*_getPRESS_OUT_H)(	
							);

		/**	
		 */
		uint8_t	(Context::*_getPRESS_OUT_L)(	
							);

		/**	
		 */
		uint8_t	(Context::*_getPRESS_OUT_XL)(	
							);

		/**	
		 */
		uint8_t	(Context::*_getSTATUS_REG)(	
							);

		/**	
		 */
		void	(Context::*_setINT_SOURCE)(	
							uint8_t	value
							);

		/**	
		 */
		uint8_t	(Context::*_getINT_SOURCE)(	
							);

		/**	
		 */
		void	(Context::*_setINT_CFG)(	
							uint8_t	value
							);

		/**	
		 */
		uint8_t	(Context::*_getINT_CFG)(	
							);

		/**	
		 */
		void	(Context::*_setCTRL_REG4)(	
							uint8_t	value
							);

		/**	
		 */
		uint8_t	(Context::*_getCTRL_REG4)(	
							);

		/**	
		 */
		void	(Context::*_setCTRL_REG3)(	
							uint8_t	value
							);

		/**	
		 */
		uint8_t	(Context::*_getCTRL_REG3)(	
							);

		/**	
		 */
		void	(Context::*_setCTRL_REG2)(	
							uint8_t	value
							);

		/**	
		 */
		uint8_t	(Context::*_getCTRL_REG2)(	
							);

		/**	
		 */
		void	(Context::*_setCTRL_REG1)(	
							uint8_t	value
							);

		/**	
		 */
		uint8_t	(Context::*_getCTRL_REG1)(	
							);

		/**	
		 */
		void	(Context::*_setRES_CONF)(	
							uint8_t	value
							);

		/**	
		 */
		uint8_t	(Context::*_getRES_CONF)(	
							);

		/**	
		 */
		uint8_t	(Context::*_getWHO_AM_I)(	
							);

		/**	
		 */
		void	(Context::*_setREF_P_H)(	
							uint8_t	value
							);

		/**	
		 */
		uint8_t	(Context::*_getREF_P_H)(	
							);

		/**	
		 */
		void	(Context::*_setREF_P_L)(	
							uint8_t	value
							);

		/**	
		 */
		uint8_t	(Context::*_getREF_P_L)(	
							);

		/**	
		 */
		void	(Context::*_setREF_P_XL)(	
							uint8_t	value
							);

		/**	
		 */
		uint8_t	(Context::*_getREF_P_XL)(	
							);

	public:
		/** */
		Composer(
			Context&		context,
			void	(Context::*getTEMP_OUT)(	
								uint8_t	maxBufferLength,
								void*	buffer
								),
			void	(Context::*getPRESS_OUT)(	
								uint8_t	maxBufferLength,
								void*	buffer
								),
			void	(Context::*setRPDS_H)(	
								uint8_t	value
								),
			uint8_t	(Context::*getRPDS_H)(	
								),
			void	(Context::*setRPDS_L)(	
								uint8_t	value
								),
			uint8_t	(Context::*getRPDS_L)(	
								),
			void	(Context::*setTHS_P_H)(	
								uint8_t	value
								),
			uint8_t	(Context::*getTHS_P_H)(	
								),
			void	(Context::*setTHS_P_L)(	
								uint8_t	value
								),
			uint8_t	(Context::*getTHS_P_L)(	
								),
			uint8_t	(Context::*getFIFO_STATUS)(	
								),
			void	(Context::*setFIFO_CTRL)(	
								uint8_t	value
								),
			uint8_t	(Context::*getFIFO_CTRL)(	
								),
			uint8_t	(Context::*getTEMP_OUT_H)(	
								),
			uint8_t	(Context::*getTEMP_OUT_L)(	
								),
			uint8_t	(Context::*getPRESS_OUT_H)(	
								),
			uint8_t	(Context::*getPRESS_OUT_L)(	
								),
			uint8_t	(Context::*getPRESS_OUT_XL)(	
								),
			uint8_t	(Context::*getSTATUS_REG)(	
								),
			void	(Context::*setINT_SOURCE)(	
								uint8_t	value
								),
			uint8_t	(Context::*getINT_SOURCE)(	
								),
			void	(Context::*setINT_CFG)(	
								uint8_t	value
								),
			uint8_t	(Context::*getINT_CFG)(	
								),
			void	(Context::*setCTRL_REG4)(	
								uint8_t	value
								),
			uint8_t	(Context::*getCTRL_REG4)(	
								),
			void	(Context::*setCTRL_REG3)(	
								uint8_t	value
								),
			uint8_t	(Context::*getCTRL_REG3)(	
								),
			void	(Context::*setCTRL_REG2)(	
								uint8_t	value
								),
			uint8_t	(Context::*getCTRL_REG2)(	
								),
			void	(Context::*setCTRL_REG1)(	
								uint8_t	value
								),
			uint8_t	(Context::*getCTRL_REG1)(	
								),
			void	(Context::*setRES_CONF)(	
								uint8_t	value
								),
			uint8_t	(Context::*getRES_CONF)(	
								),
			uint8_t	(Context::*getWHO_AM_I)(	
								),
			void	(Context::*setREF_P_H)(	
								uint8_t	value
								),
			uint8_t	(Context::*getREF_P_H)(	
								),
			void	(Context::*setREF_P_L)(	
								uint8_t	value
								),
			uint8_t	(Context::*getREF_P_L)(	
								),
			void	(Context::*setREF_P_XL)(	
								uint8_t	value
								),
			uint8_t	(Context::*getREF_P_XL)(	
								)
			) noexcept;

	private:
		/**	
		 */
		void	getTEMP_OUT(	
							uint8_t	maxBufferLength,
							void*	buffer
							) noexcept;

		/**	
		 */
		void	getPRESS_OUT(	
							uint8_t	maxBufferLength,
							void*	buffer
							) noexcept;

		/**	
		 */
		void	setRPDS_H(	
							uint8_t	value
							) noexcept;

		/**	
		 */
		uint8_t	getRPDS_H(	
							) noexcept;

		/**	
		 */
		void	setRPDS_L(	
							uint8_t	value
							) noexcept;

		/**	
		 */
		uint8_t	getRPDS_L(	
							) noexcept;

		/**	
		 */
		void	setTHS_P_H(	
							uint8_t	value
							) noexcept;

		/**	
		 */
		uint8_t	getTHS_P_H(	
							) noexcept;

		/**	
		 */
		void	setTHS_P_L(	
							uint8_t	value
							) noexcept;

		/**	
		 */
		uint8_t	getTHS_P_L(	
							) noexcept;

		/**	
		 */
		uint8_t	getFIFO_STATUS(	
							) noexcept;

		/**	
		 */
		void	setFIFO_CTRL(	
							uint8_t	value
							) noexcept;

		/**	
		 */
		uint8_t	getFIFO_CTRL(	
							) noexcept;

		/**	
		 */
		uint8_t	getTEMP_OUT_H(	
							) noexcept;

		/**	
		 */
		uint8_t	getTEMP_OUT_L(	
							) noexcept;

		/**	
		 */
		uint8_t	getPRESS_OUT_H(	
							) noexcept;

		/**	
		 */
		uint8_t	getPRESS_OUT_L(	
							) noexcept;

		/**	
		 */
		uint8_t	getPRESS_OUT_XL(	
							) noexcept;

		/**	
		 */
		uint8_t	getSTATUS_REG(	
							) noexcept;

		/**	
		 */
		void	setINT_SOURCE(	
							uint8_t	value
							) noexcept;

		/**	
		 */
		uint8_t	getINT_SOURCE(	
							) noexcept;

		/**	
		 */
		void	setINT_CFG(	
							uint8_t	value
							) noexcept;

		/**	
		 */
		uint8_t	getINT_CFG(	
							) noexcept;

		/**	
		 */
		void	setCTRL_REG4(	
							uint8_t	value
							) noexcept;

		/**	
		 */
		uint8_t	getCTRL_REG4(	
							) noexcept;

		/**	
		 */
		void	setCTRL_REG3(	
							uint8_t	value
							) noexcept;

		/**	
		 */
		uint8_t	getCTRL_REG3(	
							) noexcept;

		/**	
		 */
		void	setCTRL_REG2(	
							uint8_t	value
							) noexcept;

		/**	
		 */
		uint8_t	getCTRL_REG2(	
							) noexcept;

		/**	
		 */
		void	setCTRL_REG1(	
							uint8_t	value
							) noexcept;

		/**	
		 */
		uint8_t	getCTRL_REG1(	
							) noexcept;

		/**	
		 */
		void	setRES_CONF(	
							uint8_t	value
							) noexcept;

		/**	
		 */
		uint8_t	getRES_CONF(	
							) noexcept;

		/**	
		 */
		uint8_t	getWHO_AM_I(	
							) noexcept;

		/**	
		 */
		void	setREF_P_H(	
							uint8_t	value
							) noexcept;

		/**	
		 */
		uint8_t	getREF_P_H(	
							) noexcept;

		/**	
		 */
		void	setREF_P_L(	
							uint8_t	value
							) noexcept;

		/**	
		 */
		uint8_t	getREF_P_L(	
							) noexcept;

		/**	
		 */
		void	setREF_P_XL(	
							uint8_t	value
							) noexcept;

		/**	
		 */
		uint8_t	getREF_P_XL(	
							) noexcept;

	};

template <class Context>
Composer<Context>::Composer(
			Context&		context,
			void	(Context::*getTEMP_OUT)(	
								uint8_t	maxBufferLength,
								void*	buffer
								),
			void	(Context::*getPRESS_OUT)(	
								uint8_t	maxBufferLength,
								void*	buffer
								),
			void	(Context::*setRPDS_H)(	
								uint8_t	value
								),
			uint8_t	(Context::*getRPDS_H)(	
								),
			void	(Context::*setRPDS_L)(	
								uint8_t	value
								),
			uint8_t	(Context::*getRPDS_L)(	
								),
			void	(Context::*setTHS_P_H)(	
								uint8_t	value
								),
			uint8_t	(Context::*getTHS_P_H)(	
								),
			void	(Context::*setTHS_P_L)(	
								uint8_t	value
								),
			uint8_t	(Context::*getTHS_P_L)(	
								),
			uint8_t	(Context::*getFIFO_STATUS)(	
								),
			void	(Context::*setFIFO_CTRL)(	
								uint8_t	value
								),
			uint8_t	(Context::*getFIFO_CTRL)(	
								),
			uint8_t	(Context::*getTEMP_OUT_H)(	
								),
			uint8_t	(Context::*getTEMP_OUT_L)(	
								),
			uint8_t	(Context::*getPRESS_OUT_H)(	
								),
			uint8_t	(Context::*getPRESS_OUT_L)(	
								),
			uint8_t	(Context::*getPRESS_OUT_XL)(	
								),
			uint8_t	(Context::*getSTATUS_REG)(	
								),
			void	(Context::*setINT_SOURCE)(	
								uint8_t	value
								),
			uint8_t	(Context::*getINT_SOURCE)(	
								),
			void	(Context::*setINT_CFG)(	
								uint8_t	value
								),
			uint8_t	(Context::*getINT_CFG)(	
								),
			void	(Context::*setCTRL_REG4)(	
								uint8_t	value
								),
			uint8_t	(Context::*getCTRL_REG4)(	
								),
			void	(Context::*setCTRL_REG3)(	
								uint8_t	value
								),
			uint8_t	(Context::*getCTRL_REG3)(	
								),
			void	(Context::*setCTRL_REG2)(	
								uint8_t	value
								),
			uint8_t	(Context::*getCTRL_REG2)(	
								),
			void	(Context::*setCTRL_REG1)(	
								uint8_t	value
								),
			uint8_t	(Context::*getCTRL_REG1)(	
								),
			void	(Context::*setRES_CONF)(	
								uint8_t	value
								),
			uint8_t	(Context::*getRES_CONF)(	
								),
			uint8_t	(Context::*getWHO_AM_I)(	
								),
			void	(Context::*setREF_P_H)(	
								uint8_t	value
								),
			uint8_t	(Context::*getREF_P_H)(	
								),
			void	(Context::*setREF_P_L)(	
								uint8_t	value
								),
			uint8_t	(Context::*getREF_P_L)(	
								),
			void	(Context::*setREF_P_XL)(	
								uint8_t	value
								),
			uint8_t	(Context::*getREF_P_XL)(	
								)
			) noexcept:
		_context(context),
		_getTEMP_OUT(getTEMP_OUT),
		_getPRESS_OUT(getPRESS_OUT),
		_setRPDS_H(setRPDS_H),
		_getRPDS_H(getRPDS_H),
		_setRPDS_L(setRPDS_L),
		_getRPDS_L(getRPDS_L),
		_setTHS_P_H(setTHS_P_H),
		_getTHS_P_H(getTHS_P_H),
		_setTHS_P_L(setTHS_P_L),
		_getTHS_P_L(getTHS_P_L),
		_getFIFO_STATUS(getFIFO_STATUS),
		_setFIFO_CTRL(setFIFO_CTRL),
		_getFIFO_CTRL(getFIFO_CTRL),
		_getTEMP_OUT_H(getTEMP_OUT_H),
		_getTEMP_OUT_L(getTEMP_OUT_L),
		_getPRESS_OUT_H(getPRESS_OUT_H),
		_getPRESS_OUT_L(getPRESS_OUT_L),
		_getPRESS_OUT_XL(getPRESS_OUT_XL),
		_getSTATUS_REG(getSTATUS_REG),
		_setINT_SOURCE(setINT_SOURCE),
		_getINT_SOURCE(getINT_SOURCE),
		_setINT_CFG(setINT_CFG),
		_getINT_CFG(getINT_CFG),
		_setCTRL_REG4(setCTRL_REG4),
		_getCTRL_REG4(getCTRL_REG4),
		_setCTRL_REG3(setCTRL_REG3),
		_getCTRL_REG3(getCTRL_REG3),
		_setCTRL_REG2(setCTRL_REG2),
		_getCTRL_REG2(getCTRL_REG2),
		_setCTRL_REG1(setCTRL_REG1),
		_getCTRL_REG1(getCTRL_REG1),
		_setRES_CONF(setRES_CONF),
		_getRES_CONF(getRES_CONF),
		_getWHO_AM_I(getWHO_AM_I),
		_setREF_P_H(setREF_P_H),
		_getREF_P_H(getREF_P_H),
		_setREF_P_L(setREF_P_L),
		_getREF_P_L(getREF_P_L),
		_setREF_P_XL(setREF_P_XL),
		_getREF_P_XL(getREF_P_XL)
		{
	}

template <class Context>
void	Composer<Context>::getTEMP_OUT(	
							uint8_t	maxBufferLength,
							void*	buffer
							) noexcept{

	return (_context.*_getTEMP_OUT)(
				maxBufferLength,
				buffer
				);
	}

template <class Context>
void	Composer<Context>::getPRESS_OUT(	
							uint8_t	maxBufferLength,
							void*	buffer
							) noexcept{

	return (_context.*_getPRESS_OUT)(
				maxBufferLength,
				buffer
				);
	}

template <class Context>
void	Composer<Context>::setRPDS_H(	
							uint8_t	value
							) noexcept{

	return (_context.*_setRPDS_H)(
				value
				);
	}

template <class Context>
uint8_t	Composer<Context>::getRPDS_H(	
							) noexcept{

	return (_context.*_getRPDS_H)(
				);
	}

template <class Context>
void	Composer<Context>::setRPDS_L(	
							uint8_t	value
							) noexcept{

	return (_context.*_setRPDS_L)(
				value
				);
	}

template <class Context>
uint8_t	Composer<Context>::getRPDS_L(	
							) noexcept{

	return (_context.*_getRPDS_L)(
				);
	}

template <class Context>
void	Composer<Context>::setTHS_P_H(	
							uint8_t	value
							) noexcept{

	return (_context.*_setTHS_P_H)(
				value
				);
	}

template <class Context>
uint8_t	Composer<Context>::getTHS_P_H(	
							) noexcept{

	return (_context.*_getTHS_P_H)(
				);
	}

template <class Context>
void	Composer<Context>::setTHS_P_L(	
							uint8_t	value
							) noexcept{

	return (_context.*_setTHS_P_L)(
				value
				);
	}

template <class Context>
uint8_t	Composer<Context>::getTHS_P_L(	
							) noexcept{

	return (_context.*_getTHS_P_L)(
				);
	}

template <class Context>
uint8_t	Composer<Context>::getFIFO_STATUS(	
							) noexcept{

	return (_context.*_getFIFO_STATUS)(
				);
	}

template <class Context>
void	Composer<Context>::setFIFO_CTRL(	
							uint8_t	value
							) noexcept{

	return (_context.*_setFIFO_CTRL)(
				value
				);
	}

template <class Context>
uint8_t	Composer<Context>::getFIFO_CTRL(	
							) noexcept{

	return (_context.*_getFIFO_CTRL)(
				);
	}

template <class Context>
uint8_t	Composer<Context>::getTEMP_OUT_H(	
							) noexcept{

	return (_context.*_getTEMP_OUT_H)(
				);
	}

template <class Context>
uint8_t	Composer<Context>::getTEMP_OUT_L(	
							) noexcept{

	return (_context.*_getTEMP_OUT_L)(
				);
	}

template <class Context>
uint8_t	Composer<Context>::getPRESS_OUT_H(	
							) noexcept{

	return (_context.*_getPRESS_OUT_H)(
				);
	}

template <class Context>
uint8_t	Composer<Context>::getPRESS_OUT_L(	
							) noexcept{

	return (_context.*_getPRESS_OUT_L)(
				);
	}

template <class Context>
uint8_t	Composer<Context>::getPRESS_OUT_XL(	
							) noexcept{

	return (_context.*_getPRESS_OUT_XL)(
				);
	}

template <class Context>
uint8_t	Composer<Context>::getSTATUS_REG(	
							) noexcept{

	return (_context.*_getSTATUS_REG)(
				);
	}

template <class Context>
void	Composer<Context>::setINT_SOURCE(	
							uint8_t	value
							) noexcept{

	return (_context.*_setINT_SOURCE)(
				value
				);
	}

template <class Context>
uint8_t	Composer<Context>::getINT_SOURCE(	
							) noexcept{

	return (_context.*_getINT_SOURCE)(
				);
	}

template <class Context>
void	Composer<Context>::setINT_CFG(	
							uint8_t	value
							) noexcept{

	return (_context.*_setINT_CFG)(
				value
				);
	}

template <class Context>
uint8_t	Composer<Context>::getINT_CFG(	
							) noexcept{

	return (_context.*_getINT_CFG)(
				);
	}

template <class Context>
void	Composer<Context>::setCTRL_REG4(	
							uint8_t	value
							) noexcept{

	return (_context.*_setCTRL_REG4)(
				value
				);
	}

template <class Context>
uint8_t	Composer<Context>::getCTRL_REG4(	
							) noexcept{

	return (_context.*_getCTRL_REG4)(
				);
	}

template <class Context>
void	Composer<Context>::setCTRL_REG3(	
							uint8_t	value
							) noexcept{

	return (_context.*_setCTRL_REG3)(
				value
				);
	}

template <class Context>
uint8_t	Composer<Context>::getCTRL_REG3(	
							) noexcept{

	return (_context.*_getCTRL_REG3)(
				);
	}

template <class Context>
void	Composer<Context>::setCTRL_REG2(	
							uint8_t	value
							) noexcept{

	return (_context.*_setCTRL_REG2)(
				value
				);
	}

template <class Context>
uint8_t	Composer<Context>::getCTRL_REG2(	
							) noexcept{

	return (_context.*_getCTRL_REG2)(
				);
	}

template <class Context>
void	Composer<Context>::setCTRL_REG1(	
							uint8_t	value
							) noexcept{

	return (_context.*_setCTRL_REG1)(
				value
				);
	}

template <class Context>
uint8_t	Composer<Context>::getCTRL_REG1(	
							) noexcept{

	return (_context.*_getCTRL_REG1)(
				);
	}

template <class Context>
void	Composer<Context>::setRES_CONF(	
							uint8_t	value
							) noexcept{

	return (_context.*_setRES_CONF)(
				value
				);
	}

template <class Context>
uint8_t	Composer<Context>::getRES_CONF(	
							) noexcept{

	return (_context.*_getRES_CONF)(
				);
	}

template <class Context>
uint8_t	Composer<Context>::getWHO_AM_I(	
							) noexcept{

	return (_context.*_getWHO_AM_I)(
				);
	}

template <class Context>
void	Composer<Context>::setREF_P_H(	
							uint8_t	value
							) noexcept{

	return (_context.*_setREF_P_H)(
				value
				);
	}

template <class Context>
uint8_t	Composer<Context>::getREF_P_H(	
							) noexcept{

	return (_context.*_getREF_P_H)(
				);
	}

template <class Context>
void	Composer<Context>::setREF_P_L(	
							uint8_t	value
							) noexcept{

	return (_context.*_setREF_P_L)(
				value
				);
	}

template <class Context>
uint8_t	Composer<Context>::getREF_P_L(	
							) noexcept{

	return (_context.*_getREF_P_L)(
				);
	}

template <class Context>
void	Composer<Context>::setREF_P_XL(	
							uint8_t	value
							) noexcept{

	return (_context.*_setREF_P_XL)(
				value
				);
	}

template <class Context>
uint8_t	Composer<Context>::getREF_P_XL(	
							) noexcept{

	return (_context.*_getREF_P_XL)(
				);
	}

}
}
}
}
}
#endif
