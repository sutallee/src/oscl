/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_driver_st_lps25h_register_respcomph_
#define _oscl_driver_st_lps25h_register_respcomph_

#include "respapi.h"

/** */
namespace Oscl {

/** */
namespace Driver {

/** */
namespace ST {

/** */
namespace LPS25H {

/** */
namespace Register {

/** */
namespace Resp {

/**	This template class implements a kind of GOF decorator
	pattern allows the context to employ composition instead
	of inheritance. Frequently, a context may need to implement
	more than one interface that may result in method name
	clashes. This template solves the problem by implementing
	the interface and then invoking member function pointers
	in the context instead. One instance of this object is
	created in the context for each interface of this type used
	in the context. Each instance is constructed such that it
	refers to different member functions within the context.
	The interface of this object is passed to any object that
	requires the interface. When the object invokes any of the
	operations of the API, the corresponding member function
	of the context will subsequently be invoked.
 */
/**	This interface defines methods to access the LPS25H IMU
	Magnetometer registers indpendent of the type of bus.
 */
template <class Context>
class Composer : public Api {
	private:
		/** A reference to the Context.
		 */
		Context&	_context;

	private:
		/**	Read the TEMP_OUT registers.
		 */
		void	(Context::*_GetTEMP_OUT)(Oscl::Driver::ST::LPS25H::Register::Resp::Api::GetTEMP_OUTResp& msg);

		/**	Read the PRESS_OUT registers.
		 */
		void	(Context::*_GetPRESS_OUT)(Oscl::Driver::ST::LPS25H::Register::Resp::Api::GetPRESS_OUTResp& msg);

		/**	Write the RPDS_H register
		 */
		void	(Context::*_SetRPDS_H)(Oscl::Driver::ST::LPS25H::Register::Resp::Api::SetRPDS_HResp& msg);

		/**	Read the RPDS_H register.
		 */
		void	(Context::*_GetRPDS_H)(Oscl::Driver::ST::LPS25H::Register::Resp::Api::GetRPDS_HResp& msg);

		/**	Write the RPDS_L register
		 */
		void	(Context::*_SetRPDS_L)(Oscl::Driver::ST::LPS25H::Register::Resp::Api::SetRPDS_LResp& msg);

		/**	Read the RPDS_L register.
		 */
		void	(Context::*_GetRPDS_L)(Oscl::Driver::ST::LPS25H::Register::Resp::Api::GetRPDS_LResp& msg);

		/**	Write the THS_P_H register
		 */
		void	(Context::*_SetTHS_P_H)(Oscl::Driver::ST::LPS25H::Register::Resp::Api::SetTHS_P_HResp& msg);

		/**	Read the THS_P_H register.
		 */
		void	(Context::*_GetTHS_P_H)(Oscl::Driver::ST::LPS25H::Register::Resp::Api::GetTHS_P_HResp& msg);

		/**	Write the THS_P_L register
		 */
		void	(Context::*_SetTHS_P_L)(Oscl::Driver::ST::LPS25H::Register::Resp::Api::SetTHS_P_LResp& msg);

		/**	Read the THS_P_L register.
		 */
		void	(Context::*_GetTHS_P_L)(Oscl::Driver::ST::LPS25H::Register::Resp::Api::GetTHS_P_LResp& msg);

		/**	Read the FIFO_STATUS register.
		 */
		void	(Context::*_GetFIFO_STATUS)(Oscl::Driver::ST::LPS25H::Register::Resp::Api::GetFIFO_STATUSResp& msg);

		/**	Write the FIFO_CTRL register
		 */
		void	(Context::*_SetFIFO_CTRL)(Oscl::Driver::ST::LPS25H::Register::Resp::Api::SetFIFO_CTRLResp& msg);

		/**	Read the FIFO_CTRL register.
		 */
		void	(Context::*_GetFIFO_CTRL)(Oscl::Driver::ST::LPS25H::Register::Resp::Api::GetFIFO_CTRLResp& msg);

		/**	Read the TEMP_OUT_H register.
		 */
		void	(Context::*_GetTEMP_OUT_H)(Oscl::Driver::ST::LPS25H::Register::Resp::Api::GetTEMP_OUT_HResp& msg);

		/**	Read the TEMP_OUT_L register.
		 */
		void	(Context::*_GetTEMP_OUT_L)(Oscl::Driver::ST::LPS25H::Register::Resp::Api::GetTEMP_OUT_LResp& msg);

		/**	Read the PRESS_OUT_H register.
		 */
		void	(Context::*_GetPRESS_OUT_H)(Oscl::Driver::ST::LPS25H::Register::Resp::Api::GetPRESS_OUT_HResp& msg);

		/**	Read the PRESS_OUT_L register.
		 */
		void	(Context::*_GetPRESS_OUT_L)(Oscl::Driver::ST::LPS25H::Register::Resp::Api::GetPRESS_OUT_LResp& msg);

		/**	Read the PRESS_OUT_XL register.
		 */
		void	(Context::*_GetPRESS_OUT_XL)(Oscl::Driver::ST::LPS25H::Register::Resp::Api::GetPRESS_OUT_XLResp& msg);

		/**	Read the STATUS_REG register.
		 */
		void	(Context::*_GetSTATUS_REG)(Oscl::Driver::ST::LPS25H::Register::Resp::Api::GetSTATUS_REGResp& msg);

		/**	Write the INT_SOURCE register
		 */
		void	(Context::*_SetINT_SOURCE)(Oscl::Driver::ST::LPS25H::Register::Resp::Api::SetINT_SOURCEResp& msg);

		/**	Read the INT_SOURCE register.
		 */
		void	(Context::*_GetINT_SOURCE)(Oscl::Driver::ST::LPS25H::Register::Resp::Api::GetINT_SOURCEResp& msg);

		/**	Write the INT_CFG register
		 */
		void	(Context::*_SetINT_CFG)(Oscl::Driver::ST::LPS25H::Register::Resp::Api::SetINT_CFGResp& msg);

		/**	Read the INT_CFG register.
		 */
		void	(Context::*_GetINT_CFG)(Oscl::Driver::ST::LPS25H::Register::Resp::Api::GetINT_CFGResp& msg);

		/**	Write the CTRL_REG4 register
		 */
		void	(Context::*_SetCTRL_REG4)(Oscl::Driver::ST::LPS25H::Register::Resp::Api::SetCTRL_REG4Resp& msg);

		/**	Read the CTRL_REG4 register.
		 */
		void	(Context::*_GetCTRL_REG4)(Oscl::Driver::ST::LPS25H::Register::Resp::Api::GetCTRL_REG4Resp& msg);

		/**	Write the CTRL_REG3 register
		 */
		void	(Context::*_SetCTRL_REG3)(Oscl::Driver::ST::LPS25H::Register::Resp::Api::SetCTRL_REG3Resp& msg);

		/**	Read the CTRL_REG3 register.
		 */
		void	(Context::*_GetCTRL_REG3)(Oscl::Driver::ST::LPS25H::Register::Resp::Api::GetCTRL_REG3Resp& msg);

		/**	Write the CTRL_REG2 register
		 */
		void	(Context::*_SetCTRL_REG2)(Oscl::Driver::ST::LPS25H::Register::Resp::Api::SetCTRL_REG2Resp& msg);

		/**	Read the CTRL_REG2 register.
		 */
		void	(Context::*_GetCTRL_REG2)(Oscl::Driver::ST::LPS25H::Register::Resp::Api::GetCTRL_REG2Resp& msg);

		/**	Write the CTRL_REG1 register
		 */
		void	(Context::*_SetCTRL_REG1)(Oscl::Driver::ST::LPS25H::Register::Resp::Api::SetCTRL_REG1Resp& msg);

		/**	Read the CTRL_REG1 register.
		 */
		void	(Context::*_GetCTRL_REG1)(Oscl::Driver::ST::LPS25H::Register::Resp::Api::GetCTRL_REG1Resp& msg);

		/**	Write the RES_CONF register
		 */
		void	(Context::*_SetRES_CONF)(Oscl::Driver::ST::LPS25H::Register::Resp::Api::SetRES_CONFResp& msg);

		/**	Read the RES_CONF register.
		 */
		void	(Context::*_GetRES_CONF)(Oscl::Driver::ST::LPS25H::Register::Resp::Api::GetRES_CONFResp& msg);

		/**	Read the WHO_AM_I register.
		 */
		void	(Context::*_GetWHO_AM_I)(Oscl::Driver::ST::LPS25H::Register::Resp::Api::GetWHO_AM_IResp& msg);

		/**	Write the REF_P_H register
		 */
		void	(Context::*_SetREF_P_H)(Oscl::Driver::ST::LPS25H::Register::Resp::Api::SetREF_P_HResp& msg);

		/**	Read the REF_P_H register.
		 */
		void	(Context::*_GetREF_P_H)(Oscl::Driver::ST::LPS25H::Register::Resp::Api::GetREF_P_HResp& msg);

		/**	Write the REF_P_L register
		 */
		void	(Context::*_SetREF_P_L)(Oscl::Driver::ST::LPS25H::Register::Resp::Api::SetREF_P_LResp& msg);

		/**	Read the REF_P_L register.
		 */
		void	(Context::*_GetREF_P_L)(Oscl::Driver::ST::LPS25H::Register::Resp::Api::GetREF_P_LResp& msg);

		/**	Write the REF_P_XL register
		 */
		void	(Context::*_SetREF_P_XL)(Oscl::Driver::ST::LPS25H::Register::Resp::Api::SetREF_P_XLResp& msg);

		/**	Read the REF_P_XL register.
		 */
		void	(Context::*_GetREF_P_XL)(Oscl::Driver::ST::LPS25H::Register::Resp::Api::GetREF_P_XLResp& msg);

	public:
		/** */
		Composer(
			Context&		context,
			void	(Context::*GetTEMP_OUT)(Oscl::Driver::ST::LPS25H::Register::Resp::Api::GetTEMP_OUTResp& msg),
			void	(Context::*GetPRESS_OUT)(Oscl::Driver::ST::LPS25H::Register::Resp::Api::GetPRESS_OUTResp& msg),
			void	(Context::*SetRPDS_H)(Oscl::Driver::ST::LPS25H::Register::Resp::Api::SetRPDS_HResp& msg),
			void	(Context::*GetRPDS_H)(Oscl::Driver::ST::LPS25H::Register::Resp::Api::GetRPDS_HResp& msg),
			void	(Context::*SetRPDS_L)(Oscl::Driver::ST::LPS25H::Register::Resp::Api::SetRPDS_LResp& msg),
			void	(Context::*GetRPDS_L)(Oscl::Driver::ST::LPS25H::Register::Resp::Api::GetRPDS_LResp& msg),
			void	(Context::*SetTHS_P_H)(Oscl::Driver::ST::LPS25H::Register::Resp::Api::SetTHS_P_HResp& msg),
			void	(Context::*GetTHS_P_H)(Oscl::Driver::ST::LPS25H::Register::Resp::Api::GetTHS_P_HResp& msg),
			void	(Context::*SetTHS_P_L)(Oscl::Driver::ST::LPS25H::Register::Resp::Api::SetTHS_P_LResp& msg),
			void	(Context::*GetTHS_P_L)(Oscl::Driver::ST::LPS25H::Register::Resp::Api::GetTHS_P_LResp& msg),
			void	(Context::*GetFIFO_STATUS)(Oscl::Driver::ST::LPS25H::Register::Resp::Api::GetFIFO_STATUSResp& msg),
			void	(Context::*SetFIFO_CTRL)(Oscl::Driver::ST::LPS25H::Register::Resp::Api::SetFIFO_CTRLResp& msg),
			void	(Context::*GetFIFO_CTRL)(Oscl::Driver::ST::LPS25H::Register::Resp::Api::GetFIFO_CTRLResp& msg),
			void	(Context::*GetTEMP_OUT_H)(Oscl::Driver::ST::LPS25H::Register::Resp::Api::GetTEMP_OUT_HResp& msg),
			void	(Context::*GetTEMP_OUT_L)(Oscl::Driver::ST::LPS25H::Register::Resp::Api::GetTEMP_OUT_LResp& msg),
			void	(Context::*GetPRESS_OUT_H)(Oscl::Driver::ST::LPS25H::Register::Resp::Api::GetPRESS_OUT_HResp& msg),
			void	(Context::*GetPRESS_OUT_L)(Oscl::Driver::ST::LPS25H::Register::Resp::Api::GetPRESS_OUT_LResp& msg),
			void	(Context::*GetPRESS_OUT_XL)(Oscl::Driver::ST::LPS25H::Register::Resp::Api::GetPRESS_OUT_XLResp& msg),
			void	(Context::*GetSTATUS_REG)(Oscl::Driver::ST::LPS25H::Register::Resp::Api::GetSTATUS_REGResp& msg),
			void	(Context::*SetINT_SOURCE)(Oscl::Driver::ST::LPS25H::Register::Resp::Api::SetINT_SOURCEResp& msg),
			void	(Context::*GetINT_SOURCE)(Oscl::Driver::ST::LPS25H::Register::Resp::Api::GetINT_SOURCEResp& msg),
			void	(Context::*SetINT_CFG)(Oscl::Driver::ST::LPS25H::Register::Resp::Api::SetINT_CFGResp& msg),
			void	(Context::*GetINT_CFG)(Oscl::Driver::ST::LPS25H::Register::Resp::Api::GetINT_CFGResp& msg),
			void	(Context::*SetCTRL_REG4)(Oscl::Driver::ST::LPS25H::Register::Resp::Api::SetCTRL_REG4Resp& msg),
			void	(Context::*GetCTRL_REG4)(Oscl::Driver::ST::LPS25H::Register::Resp::Api::GetCTRL_REG4Resp& msg),
			void	(Context::*SetCTRL_REG3)(Oscl::Driver::ST::LPS25H::Register::Resp::Api::SetCTRL_REG3Resp& msg),
			void	(Context::*GetCTRL_REG3)(Oscl::Driver::ST::LPS25H::Register::Resp::Api::GetCTRL_REG3Resp& msg),
			void	(Context::*SetCTRL_REG2)(Oscl::Driver::ST::LPS25H::Register::Resp::Api::SetCTRL_REG2Resp& msg),
			void	(Context::*GetCTRL_REG2)(Oscl::Driver::ST::LPS25H::Register::Resp::Api::GetCTRL_REG2Resp& msg),
			void	(Context::*SetCTRL_REG1)(Oscl::Driver::ST::LPS25H::Register::Resp::Api::SetCTRL_REG1Resp& msg),
			void	(Context::*GetCTRL_REG1)(Oscl::Driver::ST::LPS25H::Register::Resp::Api::GetCTRL_REG1Resp& msg),
			void	(Context::*SetRES_CONF)(Oscl::Driver::ST::LPS25H::Register::Resp::Api::SetRES_CONFResp& msg),
			void	(Context::*GetRES_CONF)(Oscl::Driver::ST::LPS25H::Register::Resp::Api::GetRES_CONFResp& msg),
			void	(Context::*GetWHO_AM_I)(Oscl::Driver::ST::LPS25H::Register::Resp::Api::GetWHO_AM_IResp& msg),
			void	(Context::*SetREF_P_H)(Oscl::Driver::ST::LPS25H::Register::Resp::Api::SetREF_P_HResp& msg),
			void	(Context::*GetREF_P_H)(Oscl::Driver::ST::LPS25H::Register::Resp::Api::GetREF_P_HResp& msg),
			void	(Context::*SetREF_P_L)(Oscl::Driver::ST::LPS25H::Register::Resp::Api::SetREF_P_LResp& msg),
			void	(Context::*GetREF_P_L)(Oscl::Driver::ST::LPS25H::Register::Resp::Api::GetREF_P_LResp& msg),
			void	(Context::*SetREF_P_XL)(Oscl::Driver::ST::LPS25H::Register::Resp::Api::SetREF_P_XLResp& msg),
			void	(Context::*GetREF_P_XL)(Oscl::Driver::ST::LPS25H::Register::Resp::Api::GetREF_P_XLResp& msg)
			) noexcept;

	private:
		/**	Read the TEMP_OUT registers.
		 */
		void	response(Oscl::Driver::ST::LPS25H::Register::Resp::Api::GetTEMP_OUTResp& msg) noexcept;

		/**	Read the PRESS_OUT registers.
		 */
		void	response(Oscl::Driver::ST::LPS25H::Register::Resp::Api::GetPRESS_OUTResp& msg) noexcept;

		/**	Write the RPDS_H register
		 */
		void	response(Oscl::Driver::ST::LPS25H::Register::Resp::Api::SetRPDS_HResp& msg) noexcept;

		/**	Read the RPDS_H register.
		 */
		void	response(Oscl::Driver::ST::LPS25H::Register::Resp::Api::GetRPDS_HResp& msg) noexcept;

		/**	Write the RPDS_L register
		 */
		void	response(Oscl::Driver::ST::LPS25H::Register::Resp::Api::SetRPDS_LResp& msg) noexcept;

		/**	Read the RPDS_L register.
		 */
		void	response(Oscl::Driver::ST::LPS25H::Register::Resp::Api::GetRPDS_LResp& msg) noexcept;

		/**	Write the THS_P_H register
		 */
		void	response(Oscl::Driver::ST::LPS25H::Register::Resp::Api::SetTHS_P_HResp& msg) noexcept;

		/**	Read the THS_P_H register.
		 */
		void	response(Oscl::Driver::ST::LPS25H::Register::Resp::Api::GetTHS_P_HResp& msg) noexcept;

		/**	Write the THS_P_L register
		 */
		void	response(Oscl::Driver::ST::LPS25H::Register::Resp::Api::SetTHS_P_LResp& msg) noexcept;

		/**	Read the THS_P_L register.
		 */
		void	response(Oscl::Driver::ST::LPS25H::Register::Resp::Api::GetTHS_P_LResp& msg) noexcept;

		/**	Read the FIFO_STATUS register.
		 */
		void	response(Oscl::Driver::ST::LPS25H::Register::Resp::Api::GetFIFO_STATUSResp& msg) noexcept;

		/**	Write the FIFO_CTRL register
		 */
		void	response(Oscl::Driver::ST::LPS25H::Register::Resp::Api::SetFIFO_CTRLResp& msg) noexcept;

		/**	Read the FIFO_CTRL register.
		 */
		void	response(Oscl::Driver::ST::LPS25H::Register::Resp::Api::GetFIFO_CTRLResp& msg) noexcept;

		/**	Read the TEMP_OUT_H register.
		 */
		void	response(Oscl::Driver::ST::LPS25H::Register::Resp::Api::GetTEMP_OUT_HResp& msg) noexcept;

		/**	Read the TEMP_OUT_L register.
		 */
		void	response(Oscl::Driver::ST::LPS25H::Register::Resp::Api::GetTEMP_OUT_LResp& msg) noexcept;

		/**	Read the PRESS_OUT_H register.
		 */
		void	response(Oscl::Driver::ST::LPS25H::Register::Resp::Api::GetPRESS_OUT_HResp& msg) noexcept;

		/**	Read the PRESS_OUT_L register.
		 */
		void	response(Oscl::Driver::ST::LPS25H::Register::Resp::Api::GetPRESS_OUT_LResp& msg) noexcept;

		/**	Read the PRESS_OUT_XL register.
		 */
		void	response(Oscl::Driver::ST::LPS25H::Register::Resp::Api::GetPRESS_OUT_XLResp& msg) noexcept;

		/**	Read the STATUS_REG register.
		 */
		void	response(Oscl::Driver::ST::LPS25H::Register::Resp::Api::GetSTATUS_REGResp& msg) noexcept;

		/**	Write the INT_SOURCE register
		 */
		void	response(Oscl::Driver::ST::LPS25H::Register::Resp::Api::SetINT_SOURCEResp& msg) noexcept;

		/**	Read the INT_SOURCE register.
		 */
		void	response(Oscl::Driver::ST::LPS25H::Register::Resp::Api::GetINT_SOURCEResp& msg) noexcept;

		/**	Write the INT_CFG register
		 */
		void	response(Oscl::Driver::ST::LPS25H::Register::Resp::Api::SetINT_CFGResp& msg) noexcept;

		/**	Read the INT_CFG register.
		 */
		void	response(Oscl::Driver::ST::LPS25H::Register::Resp::Api::GetINT_CFGResp& msg) noexcept;

		/**	Write the CTRL_REG4 register
		 */
		void	response(Oscl::Driver::ST::LPS25H::Register::Resp::Api::SetCTRL_REG4Resp& msg) noexcept;

		/**	Read the CTRL_REG4 register.
		 */
		void	response(Oscl::Driver::ST::LPS25H::Register::Resp::Api::GetCTRL_REG4Resp& msg) noexcept;

		/**	Write the CTRL_REG3 register
		 */
		void	response(Oscl::Driver::ST::LPS25H::Register::Resp::Api::SetCTRL_REG3Resp& msg) noexcept;

		/**	Read the CTRL_REG3 register.
		 */
		void	response(Oscl::Driver::ST::LPS25H::Register::Resp::Api::GetCTRL_REG3Resp& msg) noexcept;

		/**	Write the CTRL_REG2 register
		 */
		void	response(Oscl::Driver::ST::LPS25H::Register::Resp::Api::SetCTRL_REG2Resp& msg) noexcept;

		/**	Read the CTRL_REG2 register.
		 */
		void	response(Oscl::Driver::ST::LPS25H::Register::Resp::Api::GetCTRL_REG2Resp& msg) noexcept;

		/**	Write the CTRL_REG1 register
		 */
		void	response(Oscl::Driver::ST::LPS25H::Register::Resp::Api::SetCTRL_REG1Resp& msg) noexcept;

		/**	Read the CTRL_REG1 register.
		 */
		void	response(Oscl::Driver::ST::LPS25H::Register::Resp::Api::GetCTRL_REG1Resp& msg) noexcept;

		/**	Write the RES_CONF register
		 */
		void	response(Oscl::Driver::ST::LPS25H::Register::Resp::Api::SetRES_CONFResp& msg) noexcept;

		/**	Read the RES_CONF register.
		 */
		void	response(Oscl::Driver::ST::LPS25H::Register::Resp::Api::GetRES_CONFResp& msg) noexcept;

		/**	Read the WHO_AM_I register.
		 */
		void	response(Oscl::Driver::ST::LPS25H::Register::Resp::Api::GetWHO_AM_IResp& msg) noexcept;

		/**	Write the REF_P_H register
		 */
		void	response(Oscl::Driver::ST::LPS25H::Register::Resp::Api::SetREF_P_HResp& msg) noexcept;

		/**	Read the REF_P_H register.
		 */
		void	response(Oscl::Driver::ST::LPS25H::Register::Resp::Api::GetREF_P_HResp& msg) noexcept;

		/**	Write the REF_P_L register
		 */
		void	response(Oscl::Driver::ST::LPS25H::Register::Resp::Api::SetREF_P_LResp& msg) noexcept;

		/**	Read the REF_P_L register.
		 */
		void	response(Oscl::Driver::ST::LPS25H::Register::Resp::Api::GetREF_P_LResp& msg) noexcept;

		/**	Write the REF_P_XL register
		 */
		void	response(Oscl::Driver::ST::LPS25H::Register::Resp::Api::SetREF_P_XLResp& msg) noexcept;

		/**	Read the REF_P_XL register.
		 */
		void	response(Oscl::Driver::ST::LPS25H::Register::Resp::Api::GetREF_P_XLResp& msg) noexcept;

	};

template <class Context>
Composer<Context>::Composer(
			Context&		context,
			void	(Context::*GetTEMP_OUT)(Oscl::Driver::ST::LPS25H::Register::Resp::Api::GetTEMP_OUTResp& msg),
			void	(Context::*GetPRESS_OUT)(Oscl::Driver::ST::LPS25H::Register::Resp::Api::GetPRESS_OUTResp& msg),
			void	(Context::*SetRPDS_H)(Oscl::Driver::ST::LPS25H::Register::Resp::Api::SetRPDS_HResp& msg),
			void	(Context::*GetRPDS_H)(Oscl::Driver::ST::LPS25H::Register::Resp::Api::GetRPDS_HResp& msg),
			void	(Context::*SetRPDS_L)(Oscl::Driver::ST::LPS25H::Register::Resp::Api::SetRPDS_LResp& msg),
			void	(Context::*GetRPDS_L)(Oscl::Driver::ST::LPS25H::Register::Resp::Api::GetRPDS_LResp& msg),
			void	(Context::*SetTHS_P_H)(Oscl::Driver::ST::LPS25H::Register::Resp::Api::SetTHS_P_HResp& msg),
			void	(Context::*GetTHS_P_H)(Oscl::Driver::ST::LPS25H::Register::Resp::Api::GetTHS_P_HResp& msg),
			void	(Context::*SetTHS_P_L)(Oscl::Driver::ST::LPS25H::Register::Resp::Api::SetTHS_P_LResp& msg),
			void	(Context::*GetTHS_P_L)(Oscl::Driver::ST::LPS25H::Register::Resp::Api::GetTHS_P_LResp& msg),
			void	(Context::*GetFIFO_STATUS)(Oscl::Driver::ST::LPS25H::Register::Resp::Api::GetFIFO_STATUSResp& msg),
			void	(Context::*SetFIFO_CTRL)(Oscl::Driver::ST::LPS25H::Register::Resp::Api::SetFIFO_CTRLResp& msg),
			void	(Context::*GetFIFO_CTRL)(Oscl::Driver::ST::LPS25H::Register::Resp::Api::GetFIFO_CTRLResp& msg),
			void	(Context::*GetTEMP_OUT_H)(Oscl::Driver::ST::LPS25H::Register::Resp::Api::GetTEMP_OUT_HResp& msg),
			void	(Context::*GetTEMP_OUT_L)(Oscl::Driver::ST::LPS25H::Register::Resp::Api::GetTEMP_OUT_LResp& msg),
			void	(Context::*GetPRESS_OUT_H)(Oscl::Driver::ST::LPS25H::Register::Resp::Api::GetPRESS_OUT_HResp& msg),
			void	(Context::*GetPRESS_OUT_L)(Oscl::Driver::ST::LPS25H::Register::Resp::Api::GetPRESS_OUT_LResp& msg),
			void	(Context::*GetPRESS_OUT_XL)(Oscl::Driver::ST::LPS25H::Register::Resp::Api::GetPRESS_OUT_XLResp& msg),
			void	(Context::*GetSTATUS_REG)(Oscl::Driver::ST::LPS25H::Register::Resp::Api::GetSTATUS_REGResp& msg),
			void	(Context::*SetINT_SOURCE)(Oscl::Driver::ST::LPS25H::Register::Resp::Api::SetINT_SOURCEResp& msg),
			void	(Context::*GetINT_SOURCE)(Oscl::Driver::ST::LPS25H::Register::Resp::Api::GetINT_SOURCEResp& msg),
			void	(Context::*SetINT_CFG)(Oscl::Driver::ST::LPS25H::Register::Resp::Api::SetINT_CFGResp& msg),
			void	(Context::*GetINT_CFG)(Oscl::Driver::ST::LPS25H::Register::Resp::Api::GetINT_CFGResp& msg),
			void	(Context::*SetCTRL_REG4)(Oscl::Driver::ST::LPS25H::Register::Resp::Api::SetCTRL_REG4Resp& msg),
			void	(Context::*GetCTRL_REG4)(Oscl::Driver::ST::LPS25H::Register::Resp::Api::GetCTRL_REG4Resp& msg),
			void	(Context::*SetCTRL_REG3)(Oscl::Driver::ST::LPS25H::Register::Resp::Api::SetCTRL_REG3Resp& msg),
			void	(Context::*GetCTRL_REG3)(Oscl::Driver::ST::LPS25H::Register::Resp::Api::GetCTRL_REG3Resp& msg),
			void	(Context::*SetCTRL_REG2)(Oscl::Driver::ST::LPS25H::Register::Resp::Api::SetCTRL_REG2Resp& msg),
			void	(Context::*GetCTRL_REG2)(Oscl::Driver::ST::LPS25H::Register::Resp::Api::GetCTRL_REG2Resp& msg),
			void	(Context::*SetCTRL_REG1)(Oscl::Driver::ST::LPS25H::Register::Resp::Api::SetCTRL_REG1Resp& msg),
			void	(Context::*GetCTRL_REG1)(Oscl::Driver::ST::LPS25H::Register::Resp::Api::GetCTRL_REG1Resp& msg),
			void	(Context::*SetRES_CONF)(Oscl::Driver::ST::LPS25H::Register::Resp::Api::SetRES_CONFResp& msg),
			void	(Context::*GetRES_CONF)(Oscl::Driver::ST::LPS25H::Register::Resp::Api::GetRES_CONFResp& msg),
			void	(Context::*GetWHO_AM_I)(Oscl::Driver::ST::LPS25H::Register::Resp::Api::GetWHO_AM_IResp& msg),
			void	(Context::*SetREF_P_H)(Oscl::Driver::ST::LPS25H::Register::Resp::Api::SetREF_P_HResp& msg),
			void	(Context::*GetREF_P_H)(Oscl::Driver::ST::LPS25H::Register::Resp::Api::GetREF_P_HResp& msg),
			void	(Context::*SetREF_P_L)(Oscl::Driver::ST::LPS25H::Register::Resp::Api::SetREF_P_LResp& msg),
			void	(Context::*GetREF_P_L)(Oscl::Driver::ST::LPS25H::Register::Resp::Api::GetREF_P_LResp& msg),
			void	(Context::*SetREF_P_XL)(Oscl::Driver::ST::LPS25H::Register::Resp::Api::SetREF_P_XLResp& msg),
			void	(Context::*GetREF_P_XL)(Oscl::Driver::ST::LPS25H::Register::Resp::Api::GetREF_P_XLResp& msg)
			) noexcept:
		_context(context),
		_GetTEMP_OUT(GetTEMP_OUT),
		_GetPRESS_OUT(GetPRESS_OUT),
		_SetRPDS_H(SetRPDS_H),
		_GetRPDS_H(GetRPDS_H),
		_SetRPDS_L(SetRPDS_L),
		_GetRPDS_L(GetRPDS_L),
		_SetTHS_P_H(SetTHS_P_H),
		_GetTHS_P_H(GetTHS_P_H),
		_SetTHS_P_L(SetTHS_P_L),
		_GetTHS_P_L(GetTHS_P_L),
		_GetFIFO_STATUS(GetFIFO_STATUS),
		_SetFIFO_CTRL(SetFIFO_CTRL),
		_GetFIFO_CTRL(GetFIFO_CTRL),
		_GetTEMP_OUT_H(GetTEMP_OUT_H),
		_GetTEMP_OUT_L(GetTEMP_OUT_L),
		_GetPRESS_OUT_H(GetPRESS_OUT_H),
		_GetPRESS_OUT_L(GetPRESS_OUT_L),
		_GetPRESS_OUT_XL(GetPRESS_OUT_XL),
		_GetSTATUS_REG(GetSTATUS_REG),
		_SetINT_SOURCE(SetINT_SOURCE),
		_GetINT_SOURCE(GetINT_SOURCE),
		_SetINT_CFG(SetINT_CFG),
		_GetINT_CFG(GetINT_CFG),
		_SetCTRL_REG4(SetCTRL_REG4),
		_GetCTRL_REG4(GetCTRL_REG4),
		_SetCTRL_REG3(SetCTRL_REG3),
		_GetCTRL_REG3(GetCTRL_REG3),
		_SetCTRL_REG2(SetCTRL_REG2),
		_GetCTRL_REG2(GetCTRL_REG2),
		_SetCTRL_REG1(SetCTRL_REG1),
		_GetCTRL_REG1(GetCTRL_REG1),
		_SetRES_CONF(SetRES_CONF),
		_GetRES_CONF(GetRES_CONF),
		_GetWHO_AM_I(GetWHO_AM_I),
		_SetREF_P_H(SetREF_P_H),
		_GetREF_P_H(GetREF_P_H),
		_SetREF_P_L(SetREF_P_L),
		_GetREF_P_L(GetREF_P_L),
		_SetREF_P_XL(SetREF_P_XL),
		_GetREF_P_XL(GetREF_P_XL)
		{
	}

template <class Context>
void	Composer<Context>::response(Oscl::Driver::ST::LPS25H::Register::Resp::Api::GetTEMP_OUTResp& msg) noexcept{
	(_context.*_GetTEMP_OUT)(msg);
	}

template <class Context>
void	Composer<Context>::response(Oscl::Driver::ST::LPS25H::Register::Resp::Api::GetPRESS_OUTResp& msg) noexcept{
	(_context.*_GetPRESS_OUT)(msg);
	}

template <class Context>
void	Composer<Context>::response(Oscl::Driver::ST::LPS25H::Register::Resp::Api::SetRPDS_HResp& msg) noexcept{
	(_context.*_SetRPDS_H)(msg);
	}

template <class Context>
void	Composer<Context>::response(Oscl::Driver::ST::LPS25H::Register::Resp::Api::GetRPDS_HResp& msg) noexcept{
	(_context.*_GetRPDS_H)(msg);
	}

template <class Context>
void	Composer<Context>::response(Oscl::Driver::ST::LPS25H::Register::Resp::Api::SetRPDS_LResp& msg) noexcept{
	(_context.*_SetRPDS_L)(msg);
	}

template <class Context>
void	Composer<Context>::response(Oscl::Driver::ST::LPS25H::Register::Resp::Api::GetRPDS_LResp& msg) noexcept{
	(_context.*_GetRPDS_L)(msg);
	}

template <class Context>
void	Composer<Context>::response(Oscl::Driver::ST::LPS25H::Register::Resp::Api::SetTHS_P_HResp& msg) noexcept{
	(_context.*_SetTHS_P_H)(msg);
	}

template <class Context>
void	Composer<Context>::response(Oscl::Driver::ST::LPS25H::Register::Resp::Api::GetTHS_P_HResp& msg) noexcept{
	(_context.*_GetTHS_P_H)(msg);
	}

template <class Context>
void	Composer<Context>::response(Oscl::Driver::ST::LPS25H::Register::Resp::Api::SetTHS_P_LResp& msg) noexcept{
	(_context.*_SetTHS_P_L)(msg);
	}

template <class Context>
void	Composer<Context>::response(Oscl::Driver::ST::LPS25H::Register::Resp::Api::GetTHS_P_LResp& msg) noexcept{
	(_context.*_GetTHS_P_L)(msg);
	}

template <class Context>
void	Composer<Context>::response(Oscl::Driver::ST::LPS25H::Register::Resp::Api::GetFIFO_STATUSResp& msg) noexcept{
	(_context.*_GetFIFO_STATUS)(msg);
	}

template <class Context>
void	Composer<Context>::response(Oscl::Driver::ST::LPS25H::Register::Resp::Api::SetFIFO_CTRLResp& msg) noexcept{
	(_context.*_SetFIFO_CTRL)(msg);
	}

template <class Context>
void	Composer<Context>::response(Oscl::Driver::ST::LPS25H::Register::Resp::Api::GetFIFO_CTRLResp& msg) noexcept{
	(_context.*_GetFIFO_CTRL)(msg);
	}

template <class Context>
void	Composer<Context>::response(Oscl::Driver::ST::LPS25H::Register::Resp::Api::GetTEMP_OUT_HResp& msg) noexcept{
	(_context.*_GetTEMP_OUT_H)(msg);
	}

template <class Context>
void	Composer<Context>::response(Oscl::Driver::ST::LPS25H::Register::Resp::Api::GetTEMP_OUT_LResp& msg) noexcept{
	(_context.*_GetTEMP_OUT_L)(msg);
	}

template <class Context>
void	Composer<Context>::response(Oscl::Driver::ST::LPS25H::Register::Resp::Api::GetPRESS_OUT_HResp& msg) noexcept{
	(_context.*_GetPRESS_OUT_H)(msg);
	}

template <class Context>
void	Composer<Context>::response(Oscl::Driver::ST::LPS25H::Register::Resp::Api::GetPRESS_OUT_LResp& msg) noexcept{
	(_context.*_GetPRESS_OUT_L)(msg);
	}

template <class Context>
void	Composer<Context>::response(Oscl::Driver::ST::LPS25H::Register::Resp::Api::GetPRESS_OUT_XLResp& msg) noexcept{
	(_context.*_GetPRESS_OUT_XL)(msg);
	}

template <class Context>
void	Composer<Context>::response(Oscl::Driver::ST::LPS25H::Register::Resp::Api::GetSTATUS_REGResp& msg) noexcept{
	(_context.*_GetSTATUS_REG)(msg);
	}

template <class Context>
void	Composer<Context>::response(Oscl::Driver::ST::LPS25H::Register::Resp::Api::SetINT_SOURCEResp& msg) noexcept{
	(_context.*_SetINT_SOURCE)(msg);
	}

template <class Context>
void	Composer<Context>::response(Oscl::Driver::ST::LPS25H::Register::Resp::Api::GetINT_SOURCEResp& msg) noexcept{
	(_context.*_GetINT_SOURCE)(msg);
	}

template <class Context>
void	Composer<Context>::response(Oscl::Driver::ST::LPS25H::Register::Resp::Api::SetINT_CFGResp& msg) noexcept{
	(_context.*_SetINT_CFG)(msg);
	}

template <class Context>
void	Composer<Context>::response(Oscl::Driver::ST::LPS25H::Register::Resp::Api::GetINT_CFGResp& msg) noexcept{
	(_context.*_GetINT_CFG)(msg);
	}

template <class Context>
void	Composer<Context>::response(Oscl::Driver::ST::LPS25H::Register::Resp::Api::SetCTRL_REG4Resp& msg) noexcept{
	(_context.*_SetCTRL_REG4)(msg);
	}

template <class Context>
void	Composer<Context>::response(Oscl::Driver::ST::LPS25H::Register::Resp::Api::GetCTRL_REG4Resp& msg) noexcept{
	(_context.*_GetCTRL_REG4)(msg);
	}

template <class Context>
void	Composer<Context>::response(Oscl::Driver::ST::LPS25H::Register::Resp::Api::SetCTRL_REG3Resp& msg) noexcept{
	(_context.*_SetCTRL_REG3)(msg);
	}

template <class Context>
void	Composer<Context>::response(Oscl::Driver::ST::LPS25H::Register::Resp::Api::GetCTRL_REG3Resp& msg) noexcept{
	(_context.*_GetCTRL_REG3)(msg);
	}

template <class Context>
void	Composer<Context>::response(Oscl::Driver::ST::LPS25H::Register::Resp::Api::SetCTRL_REG2Resp& msg) noexcept{
	(_context.*_SetCTRL_REG2)(msg);
	}

template <class Context>
void	Composer<Context>::response(Oscl::Driver::ST::LPS25H::Register::Resp::Api::GetCTRL_REG2Resp& msg) noexcept{
	(_context.*_GetCTRL_REG2)(msg);
	}

template <class Context>
void	Composer<Context>::response(Oscl::Driver::ST::LPS25H::Register::Resp::Api::SetCTRL_REG1Resp& msg) noexcept{
	(_context.*_SetCTRL_REG1)(msg);
	}

template <class Context>
void	Composer<Context>::response(Oscl::Driver::ST::LPS25H::Register::Resp::Api::GetCTRL_REG1Resp& msg) noexcept{
	(_context.*_GetCTRL_REG1)(msg);
	}

template <class Context>
void	Composer<Context>::response(Oscl::Driver::ST::LPS25H::Register::Resp::Api::SetRES_CONFResp& msg) noexcept{
	(_context.*_SetRES_CONF)(msg);
	}

template <class Context>
void	Composer<Context>::response(Oscl::Driver::ST::LPS25H::Register::Resp::Api::GetRES_CONFResp& msg) noexcept{
	(_context.*_GetRES_CONF)(msg);
	}

template <class Context>
void	Composer<Context>::response(Oscl::Driver::ST::LPS25H::Register::Resp::Api::GetWHO_AM_IResp& msg) noexcept{
	(_context.*_GetWHO_AM_I)(msg);
	}

template <class Context>
void	Composer<Context>::response(Oscl::Driver::ST::LPS25H::Register::Resp::Api::SetREF_P_HResp& msg) noexcept{
	(_context.*_SetREF_P_H)(msg);
	}

template <class Context>
void	Composer<Context>::response(Oscl::Driver::ST::LPS25H::Register::Resp::Api::GetREF_P_HResp& msg) noexcept{
	(_context.*_GetREF_P_H)(msg);
	}

template <class Context>
void	Composer<Context>::response(Oscl::Driver::ST::LPS25H::Register::Resp::Api::SetREF_P_LResp& msg) noexcept{
	(_context.*_SetREF_P_L)(msg);
	}

template <class Context>
void	Composer<Context>::response(Oscl::Driver::ST::LPS25H::Register::Resp::Api::GetREF_P_LResp& msg) noexcept{
	(_context.*_GetREF_P_L)(msg);
	}

template <class Context>
void	Composer<Context>::response(Oscl::Driver::ST::LPS25H::Register::Resp::Api::SetREF_P_XLResp& msg) noexcept{
	(_context.*_SetREF_P_XL)(msg);
	}

template <class Context>
void	Composer<Context>::response(Oscl::Driver::ST::LPS25H::Register::Resp::Api::GetREF_P_XLResp& msg) noexcept{
	(_context.*_GetREF_P_XL)(msg);
	}

}
}
}
}
}
}
#endif
