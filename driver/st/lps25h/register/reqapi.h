/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_driver_st_lps25h_register_itc_reqapih_
#define _oscl_driver_st_lps25h_register_itc_reqapih_

#include "oscl/mt/itc/mbox/srvreq.h"
#include "oscl/mt/itc/postmsgapi.h"
#include "oscl/mt/itc/mbox/sap.h"
#include <stdint.h>

/** */
namespace Oscl {

/** */
namespace Driver {

/** */
namespace ST {

/** */
namespace LPS25H {

/** */
namespace Register {

/** */
namespace Req {

/**	This class describes an ITC server interface. The
	interface includes the definition of messages and their
	associated payloads, as well as an interface that is to be
	implemented by a server that supports this ITC interface.
 */
/**	This interface defines methods to access the LPS25H IMU
	Magnetometer registers indpendent of the type of bus.
 */
class Api {
	public:
		/** This class contains the data that is transfered
			between the client and server within the GetTEMP_OUTReq
			ITC message.
		 */
		class GetTEMP_OUTPayload {
			public:
				/**	
				 */
				uint8_t	_maxBufferLength;

				/**	
				 */
				void*	_buffer;

			public:
				/** The GetTEMP_OUTPayload constructor. */
				GetTEMP_OUTPayload(

					uint8_t	maxBufferLength,
					void*	buffer
					) noexcept;

			};

		/**	Read the TEMP_OUT registers.
		 */
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::Driver::ST::LPS25H::Register::Req::Api,
					GetTEMP_OUTPayload
					>		GetTEMP_OUTReq;

	public:
		/** This class contains the data that is transfered
			between the client and server within the GetPRESS_OUTReq
			ITC message.
		 */
		class GetPRESS_OUTPayload {
			public:
				/**	
				 */
				uint8_t	_maxBufferLength;

				/**	
				 */
				void*	_buffer;

			public:
				/** The GetPRESS_OUTPayload constructor. */
				GetPRESS_OUTPayload(

					uint8_t	maxBufferLength,
					void*	buffer
					) noexcept;

			};

		/**	Read the PRESS_OUT registers.
		 */
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::Driver::ST::LPS25H::Register::Req::Api,
					GetPRESS_OUTPayload
					>		GetPRESS_OUTReq;

	public:
		/** This class contains the data that is transfered
			between the client and server within the SetRPDS_HReq
			ITC message.
		 */
		class SetRPDS_HPayload {
			public:
				/**	
				 */
				uint8_t	_value;

			public:
				/** The SetRPDS_HPayload constructor. */
				SetRPDS_HPayload(

					uint8_t	value
					) noexcept;

			};

		/**	Write the RPDS_H register
		 */
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::Driver::ST::LPS25H::Register::Req::Api,
					SetRPDS_HPayload
					>		SetRPDS_HReq;

	public:
		/** This class contains the data that is transfered
			between the client and server within the GetRPDS_HReq
			ITC message.
		 */
		class GetRPDS_HPayload {
			public:
				/**	
				 */
				uint8_t	_value;

			public:
				/** The GetRPDS_HPayload constructor. */
				GetRPDS_HPayload(
					) noexcept;

			};

		/**	Read the RPDS_H register.
		 */
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::Driver::ST::LPS25H::Register::Req::Api,
					GetRPDS_HPayload
					>		GetRPDS_HReq;

	public:
		/** This class contains the data that is transfered
			between the client and server within the SetRPDS_LReq
			ITC message.
		 */
		class SetRPDS_LPayload {
			public:
				/**	
				 */
				uint8_t	_value;

			public:
				/** The SetRPDS_LPayload constructor. */
				SetRPDS_LPayload(

					uint8_t	value
					) noexcept;

			};

		/**	Write the RPDS_L register
		 */
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::Driver::ST::LPS25H::Register::Req::Api,
					SetRPDS_LPayload
					>		SetRPDS_LReq;

	public:
		/** This class contains the data that is transfered
			between the client and server within the GetRPDS_LReq
			ITC message.
		 */
		class GetRPDS_LPayload {
			public:
				/**	
				 */
				uint8_t	_value;

			public:
				/** The GetRPDS_LPayload constructor. */
				GetRPDS_LPayload(
					) noexcept;

			};

		/**	Read the RPDS_L register.
		 */
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::Driver::ST::LPS25H::Register::Req::Api,
					GetRPDS_LPayload
					>		GetRPDS_LReq;

	public:
		/** This class contains the data that is transfered
			between the client and server within the SetTHS_P_HReq
			ITC message.
		 */
		class SetTHS_P_HPayload {
			public:
				/**	
				 */
				uint8_t	_value;

			public:
				/** The SetTHS_P_HPayload constructor. */
				SetTHS_P_HPayload(

					uint8_t	value
					) noexcept;

			};

		/**	Write the THS_P_H register
		 */
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::Driver::ST::LPS25H::Register::Req::Api,
					SetTHS_P_HPayload
					>		SetTHS_P_HReq;

	public:
		/** This class contains the data that is transfered
			between the client and server within the GetTHS_P_HReq
			ITC message.
		 */
		class GetTHS_P_HPayload {
			public:
				/**	
				 */
				uint8_t	_value;

			public:
				/** The GetTHS_P_HPayload constructor. */
				GetTHS_P_HPayload(
					) noexcept;

			};

		/**	Read the THS_P_H register.
		 */
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::Driver::ST::LPS25H::Register::Req::Api,
					GetTHS_P_HPayload
					>		GetTHS_P_HReq;

	public:
		/** This class contains the data that is transfered
			between the client and server within the SetTHS_P_LReq
			ITC message.
		 */
		class SetTHS_P_LPayload {
			public:
				/**	
				 */
				uint8_t	_value;

			public:
				/** The SetTHS_P_LPayload constructor. */
				SetTHS_P_LPayload(

					uint8_t	value
					) noexcept;

			};

		/**	Write the THS_P_L register
		 */
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::Driver::ST::LPS25H::Register::Req::Api,
					SetTHS_P_LPayload
					>		SetTHS_P_LReq;

	public:
		/** This class contains the data that is transfered
			between the client and server within the GetTHS_P_LReq
			ITC message.
		 */
		class GetTHS_P_LPayload {
			public:
				/**	
				 */
				uint8_t	_value;

			public:
				/** The GetTHS_P_LPayload constructor. */
				GetTHS_P_LPayload(
					) noexcept;

			};

		/**	Read the THS_P_L register.
		 */
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::Driver::ST::LPS25H::Register::Req::Api,
					GetTHS_P_LPayload
					>		GetTHS_P_LReq;

	public:
		/** This class contains the data that is transfered
			between the client and server within the GetFIFO_STATUSReq
			ITC message.
		 */
		class GetFIFO_STATUSPayload {
			public:
				/**	
				 */
				uint8_t	_value;

			public:
				/** The GetFIFO_STATUSPayload constructor. */
				GetFIFO_STATUSPayload(
					) noexcept;

			};

		/**	Read the FIFO_STATUS register.
		 */
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::Driver::ST::LPS25H::Register::Req::Api,
					GetFIFO_STATUSPayload
					>		GetFIFO_STATUSReq;

	public:
		/** This class contains the data that is transfered
			between the client and server within the SetFIFO_CTRLReq
			ITC message.
		 */
		class SetFIFO_CTRLPayload {
			public:
				/**	
				 */
				uint8_t	_value;

			public:
				/** The SetFIFO_CTRLPayload constructor. */
				SetFIFO_CTRLPayload(

					uint8_t	value
					) noexcept;

			};

		/**	Write the FIFO_CTRL register
		 */
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::Driver::ST::LPS25H::Register::Req::Api,
					SetFIFO_CTRLPayload
					>		SetFIFO_CTRLReq;

	public:
		/** This class contains the data that is transfered
			between the client and server within the GetFIFO_CTRLReq
			ITC message.
		 */
		class GetFIFO_CTRLPayload {
			public:
				/**	
				 */
				uint8_t	_value;

			public:
				/** The GetFIFO_CTRLPayload constructor. */
				GetFIFO_CTRLPayload(
					) noexcept;

			};

		/**	Read the FIFO_CTRL register.
		 */
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::Driver::ST::LPS25H::Register::Req::Api,
					GetFIFO_CTRLPayload
					>		GetFIFO_CTRLReq;

	public:
		/** This class contains the data that is transfered
			between the client and server within the GetTEMP_OUT_HReq
			ITC message.
		 */
		class GetTEMP_OUT_HPayload {
			public:
				/**	
				 */
				uint8_t	_value;

			public:
				/** The GetTEMP_OUT_HPayload constructor. */
				GetTEMP_OUT_HPayload(
					) noexcept;

			};

		/**	Read the TEMP_OUT_H register.
		 */
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::Driver::ST::LPS25H::Register::Req::Api,
					GetTEMP_OUT_HPayload
					>		GetTEMP_OUT_HReq;

	public:
		/** This class contains the data that is transfered
			between the client and server within the GetTEMP_OUT_LReq
			ITC message.
		 */
		class GetTEMP_OUT_LPayload {
			public:
				/**	
				 */
				uint8_t	_value;

			public:
				/** The GetTEMP_OUT_LPayload constructor. */
				GetTEMP_OUT_LPayload(
					) noexcept;

			};

		/**	Read the TEMP_OUT_L register.
		 */
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::Driver::ST::LPS25H::Register::Req::Api,
					GetTEMP_OUT_LPayload
					>		GetTEMP_OUT_LReq;

	public:
		/** This class contains the data that is transfered
			between the client and server within the GetPRESS_OUT_HReq
			ITC message.
		 */
		class GetPRESS_OUT_HPayload {
			public:
				/**	
				 */
				uint8_t	_value;

			public:
				/** The GetPRESS_OUT_HPayload constructor. */
				GetPRESS_OUT_HPayload(
					) noexcept;

			};

		/**	Read the PRESS_OUT_H register.
		 */
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::Driver::ST::LPS25H::Register::Req::Api,
					GetPRESS_OUT_HPayload
					>		GetPRESS_OUT_HReq;

	public:
		/** This class contains the data that is transfered
			between the client and server within the GetPRESS_OUT_LReq
			ITC message.
		 */
		class GetPRESS_OUT_LPayload {
			public:
				/**	
				 */
				uint8_t	_value;

			public:
				/** The GetPRESS_OUT_LPayload constructor. */
				GetPRESS_OUT_LPayload(
					) noexcept;

			};

		/**	Read the PRESS_OUT_L register.
		 */
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::Driver::ST::LPS25H::Register::Req::Api,
					GetPRESS_OUT_LPayload
					>		GetPRESS_OUT_LReq;

	public:
		/** This class contains the data that is transfered
			between the client and server within the GetPRESS_OUT_XLReq
			ITC message.
		 */
		class GetPRESS_OUT_XLPayload {
			public:
				/**	
				 */
				uint8_t	_value;

			public:
				/** The GetPRESS_OUT_XLPayload constructor. */
				GetPRESS_OUT_XLPayload(
					) noexcept;

			};

		/**	Read the PRESS_OUT_XL register.
		 */
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::Driver::ST::LPS25H::Register::Req::Api,
					GetPRESS_OUT_XLPayload
					>		GetPRESS_OUT_XLReq;

	public:
		/** This class contains the data that is transfered
			between the client and server within the GetSTATUS_REGReq
			ITC message.
		 */
		class GetSTATUS_REGPayload {
			public:
				/**	
				 */
				uint8_t	_value;

			public:
				/** The GetSTATUS_REGPayload constructor. */
				GetSTATUS_REGPayload(
					) noexcept;

			};

		/**	Read the STATUS_REG register.
		 */
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::Driver::ST::LPS25H::Register::Req::Api,
					GetSTATUS_REGPayload
					>		GetSTATUS_REGReq;

	public:
		/** This class contains the data that is transfered
			between the client and server within the SetINT_SOURCEReq
			ITC message.
		 */
		class SetINT_SOURCEPayload {
			public:
				/**	
				 */
				uint8_t	_value;

			public:
				/** The SetINT_SOURCEPayload constructor. */
				SetINT_SOURCEPayload(

					uint8_t	value
					) noexcept;

			};

		/**	Write the INT_SOURCE register
		 */
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::Driver::ST::LPS25H::Register::Req::Api,
					SetINT_SOURCEPayload
					>		SetINT_SOURCEReq;

	public:
		/** This class contains the data that is transfered
			between the client and server within the GetINT_SOURCEReq
			ITC message.
		 */
		class GetINT_SOURCEPayload {
			public:
				/**	
				 */
				uint8_t	_value;

			public:
				/** The GetINT_SOURCEPayload constructor. */
				GetINT_SOURCEPayload(
					) noexcept;

			};

		/**	Read the INT_SOURCE register.
		 */
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::Driver::ST::LPS25H::Register::Req::Api,
					GetINT_SOURCEPayload
					>		GetINT_SOURCEReq;

	public:
		/** This class contains the data that is transfered
			between the client and server within the SetINT_CFGReq
			ITC message.
		 */
		class SetINT_CFGPayload {
			public:
				/**	
				 */
				uint8_t	_value;

			public:
				/** The SetINT_CFGPayload constructor. */
				SetINT_CFGPayload(

					uint8_t	value
					) noexcept;

			};

		/**	Write the INT_CFG register
		 */
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::Driver::ST::LPS25H::Register::Req::Api,
					SetINT_CFGPayload
					>		SetINT_CFGReq;

	public:
		/** This class contains the data that is transfered
			between the client and server within the GetINT_CFGReq
			ITC message.
		 */
		class GetINT_CFGPayload {
			public:
				/**	
				 */
				uint8_t	_value;

			public:
				/** The GetINT_CFGPayload constructor. */
				GetINT_CFGPayload(
					) noexcept;

			};

		/**	Read the INT_CFG register.
		 */
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::Driver::ST::LPS25H::Register::Req::Api,
					GetINT_CFGPayload
					>		GetINT_CFGReq;

	public:
		/** This class contains the data that is transfered
			between the client and server within the SetCTRL_REG4Req
			ITC message.
		 */
		class SetCTRL_REG4Payload {
			public:
				/**	
				 */
				uint8_t	_value;

			public:
				/** The SetCTRL_REG4Payload constructor. */
				SetCTRL_REG4Payload(

					uint8_t	value
					) noexcept;

			};

		/**	Write the CTRL_REG4 register
		 */
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::Driver::ST::LPS25H::Register::Req::Api,
					SetCTRL_REG4Payload
					>		SetCTRL_REG4Req;

	public:
		/** This class contains the data that is transfered
			between the client and server within the GetCTRL_REG4Req
			ITC message.
		 */
		class GetCTRL_REG4Payload {
			public:
				/**	
				 */
				uint8_t	_value;

			public:
				/** The GetCTRL_REG4Payload constructor. */
				GetCTRL_REG4Payload(
					) noexcept;

			};

		/**	Read the CTRL_REG4 register.
		 */
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::Driver::ST::LPS25H::Register::Req::Api,
					GetCTRL_REG4Payload
					>		GetCTRL_REG4Req;

	public:
		/** This class contains the data that is transfered
			between the client and server within the SetCTRL_REG3Req
			ITC message.
		 */
		class SetCTRL_REG3Payload {
			public:
				/**	
				 */
				uint8_t	_value;

			public:
				/** The SetCTRL_REG3Payload constructor. */
				SetCTRL_REG3Payload(

					uint8_t	value
					) noexcept;

			};

		/**	Write the CTRL_REG3 register
		 */
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::Driver::ST::LPS25H::Register::Req::Api,
					SetCTRL_REG3Payload
					>		SetCTRL_REG3Req;

	public:
		/** This class contains the data that is transfered
			between the client and server within the GetCTRL_REG3Req
			ITC message.
		 */
		class GetCTRL_REG3Payload {
			public:
				/**	
				 */
				uint8_t	_value;

			public:
				/** The GetCTRL_REG3Payload constructor. */
				GetCTRL_REG3Payload(
					) noexcept;

			};

		/**	Read the CTRL_REG3 register.
		 */
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::Driver::ST::LPS25H::Register::Req::Api,
					GetCTRL_REG3Payload
					>		GetCTRL_REG3Req;

	public:
		/** This class contains the data that is transfered
			between the client and server within the SetCTRL_REG2Req
			ITC message.
		 */
		class SetCTRL_REG2Payload {
			public:
				/**	
				 */
				uint8_t	_value;

			public:
				/** The SetCTRL_REG2Payload constructor. */
				SetCTRL_REG2Payload(

					uint8_t	value
					) noexcept;

			};

		/**	Write the CTRL_REG2 register
		 */
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::Driver::ST::LPS25H::Register::Req::Api,
					SetCTRL_REG2Payload
					>		SetCTRL_REG2Req;

	public:
		/** This class contains the data that is transfered
			between the client and server within the GetCTRL_REG2Req
			ITC message.
		 */
		class GetCTRL_REG2Payload {
			public:
				/**	
				 */
				uint8_t	_value;

			public:
				/** The GetCTRL_REG2Payload constructor. */
				GetCTRL_REG2Payload(
					) noexcept;

			};

		/**	Read the CTRL_REG2 register.
		 */
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::Driver::ST::LPS25H::Register::Req::Api,
					GetCTRL_REG2Payload
					>		GetCTRL_REG2Req;

	public:
		/** This class contains the data that is transfered
			between the client and server within the SetCTRL_REG1Req
			ITC message.
		 */
		class SetCTRL_REG1Payload {
			public:
				/**	
				 */
				uint8_t	_value;

			public:
				/** The SetCTRL_REG1Payload constructor. */
				SetCTRL_REG1Payload(

					uint8_t	value
					) noexcept;

			};

		/**	Write the CTRL_REG1 register
		 */
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::Driver::ST::LPS25H::Register::Req::Api,
					SetCTRL_REG1Payload
					>		SetCTRL_REG1Req;

	public:
		/** This class contains the data that is transfered
			between the client and server within the GetCTRL_REG1Req
			ITC message.
		 */
		class GetCTRL_REG1Payload {
			public:
				/**	
				 */
				uint8_t	_value;

			public:
				/** The GetCTRL_REG1Payload constructor. */
				GetCTRL_REG1Payload(
					) noexcept;

			};

		/**	Read the CTRL_REG1 register.
		 */
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::Driver::ST::LPS25H::Register::Req::Api,
					GetCTRL_REG1Payload
					>		GetCTRL_REG1Req;

	public:
		/** This class contains the data that is transfered
			between the client and server within the SetRES_CONFReq
			ITC message.
		 */
		class SetRES_CONFPayload {
			public:
				/**	
				 */
				uint8_t	_value;

			public:
				/** The SetRES_CONFPayload constructor. */
				SetRES_CONFPayload(

					uint8_t	value
					) noexcept;

			};

		/**	Write the RES_CONF register
		 */
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::Driver::ST::LPS25H::Register::Req::Api,
					SetRES_CONFPayload
					>		SetRES_CONFReq;

	public:
		/** This class contains the data that is transfered
			between the client and server within the GetRES_CONFReq
			ITC message.
		 */
		class GetRES_CONFPayload {
			public:
				/**	
				 */
				uint8_t	_value;

			public:
				/** The GetRES_CONFPayload constructor. */
				GetRES_CONFPayload(
					) noexcept;

			};

		/**	Read the RES_CONF register.
		 */
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::Driver::ST::LPS25H::Register::Req::Api,
					GetRES_CONFPayload
					>		GetRES_CONFReq;

	public:
		/** This class contains the data that is transfered
			between the client and server within the GetWHO_AM_IReq
			ITC message.
		 */
		class GetWHO_AM_IPayload {
			public:
				/**	
				 */
				uint8_t	_value;

			public:
				/** The GetWHO_AM_IPayload constructor. */
				GetWHO_AM_IPayload(
					) noexcept;

			};

		/**	Read the WHO_AM_I register.
		 */
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::Driver::ST::LPS25H::Register::Req::Api,
					GetWHO_AM_IPayload
					>		GetWHO_AM_IReq;

	public:
		/** This class contains the data that is transfered
			between the client and server within the SetREF_P_HReq
			ITC message.
		 */
		class SetREF_P_HPayload {
			public:
				/**	
				 */
				uint8_t	_value;

			public:
				/** The SetREF_P_HPayload constructor. */
				SetREF_P_HPayload(

					uint8_t	value
					) noexcept;

			};

		/**	Write the REF_P_H register
		 */
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::Driver::ST::LPS25H::Register::Req::Api,
					SetREF_P_HPayload
					>		SetREF_P_HReq;

	public:
		/** This class contains the data that is transfered
			between the client and server within the GetREF_P_HReq
			ITC message.
		 */
		class GetREF_P_HPayload {
			public:
				/**	
				 */
				uint8_t	_value;

			public:
				/** The GetREF_P_HPayload constructor. */
				GetREF_P_HPayload(
					) noexcept;

			};

		/**	Read the REF_P_H register.
		 */
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::Driver::ST::LPS25H::Register::Req::Api,
					GetREF_P_HPayload
					>		GetREF_P_HReq;

	public:
		/** This class contains the data that is transfered
			between the client and server within the SetREF_P_LReq
			ITC message.
		 */
		class SetREF_P_LPayload {
			public:
				/**	
				 */
				uint8_t	_value;

			public:
				/** The SetREF_P_LPayload constructor. */
				SetREF_P_LPayload(

					uint8_t	value
					) noexcept;

			};

		/**	Write the REF_P_L register
		 */
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::Driver::ST::LPS25H::Register::Req::Api,
					SetREF_P_LPayload
					>		SetREF_P_LReq;

	public:
		/** This class contains the data that is transfered
			between the client and server within the GetREF_P_LReq
			ITC message.
		 */
		class GetREF_P_LPayload {
			public:
				/**	
				 */
				uint8_t	_value;

			public:
				/** The GetREF_P_LPayload constructor. */
				GetREF_P_LPayload(
					) noexcept;

			};

		/**	Read the REF_P_L register.
		 */
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::Driver::ST::LPS25H::Register::Req::Api,
					GetREF_P_LPayload
					>		GetREF_P_LReq;

	public:
		/** This class contains the data that is transfered
			between the client and server within the SetREF_P_XLReq
			ITC message.
		 */
		class SetREF_P_XLPayload {
			public:
				/**	
				 */
				uint8_t	_value;

			public:
				/** The SetREF_P_XLPayload constructor. */
				SetREF_P_XLPayload(

					uint8_t	value
					) noexcept;

			};

		/**	Write the REF_P_XL register
		 */
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::Driver::ST::LPS25H::Register::Req::Api,
					SetREF_P_XLPayload
					>		SetREF_P_XLReq;

	public:
		/** This class contains the data that is transfered
			between the client and server within the GetREF_P_XLReq
			ITC message.
		 */
		class GetREF_P_XLPayload {
			public:
				/**	
				 */
				uint8_t	_value;

			public:
				/** The GetREF_P_XLPayload constructor. */
				GetREF_P_XLPayload(
					) noexcept;

			};

		/**	Read the REF_P_XL register.
		 */
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::Driver::ST::LPS25H::Register::Req::Api,
					GetREF_P_XLPayload
					>		GetREF_P_XLReq;

	public:
		/** This type defines a SAP (Service Access Point) for this
			ITC server interface. A SAP holds server address information
			that is required by clients to send messages to a server.
		 */
		typedef Oscl::Mt::Itc::SAP<	Oscl::Driver::ST::LPS25H::Register::
							Req::Api
							>			SAP;

	public:
		/** This type defines a SAP (Service Access Point) for this
			ITC server interface. A SAP holds server address information
			that is required by clients to send messages to a server.
		 */
		typedef Oscl::Mt::Itc::ConcreteSAP<	Oscl::Driver::ST::LPS25H::Register::
							Req::Api
							>			ConcreteSAP;

	public:
		/** Make the compiler happy. */
		virtual ~Api(){}

		/** This operation is invoked within the server thread
			whenever a GetTEMP_OUTReq is received.
		 */
		virtual void	request(	Oscl::Driver::ST::LPS25H::Register::
									Req::Api::GetTEMP_OUTReq& msg
									) noexcept=0;

		/** This operation is invoked within the server thread
			whenever a GetPRESS_OUTReq is received.
		 */
		virtual void	request(	Oscl::Driver::ST::LPS25H::Register::
									Req::Api::GetPRESS_OUTReq& msg
									) noexcept=0;

		/** This operation is invoked within the server thread
			whenever a SetRPDS_HReq is received.
		 */
		virtual void	request(	Oscl::Driver::ST::LPS25H::Register::
									Req::Api::SetRPDS_HReq& msg
									) noexcept=0;

		/** This operation is invoked within the server thread
			whenever a GetRPDS_HReq is received.
		 */
		virtual void	request(	Oscl::Driver::ST::LPS25H::Register::
									Req::Api::GetRPDS_HReq& msg
									) noexcept=0;

		/** This operation is invoked within the server thread
			whenever a SetRPDS_LReq is received.
		 */
		virtual void	request(	Oscl::Driver::ST::LPS25H::Register::
									Req::Api::SetRPDS_LReq& msg
									) noexcept=0;

		/** This operation is invoked within the server thread
			whenever a GetRPDS_LReq is received.
		 */
		virtual void	request(	Oscl::Driver::ST::LPS25H::Register::
									Req::Api::GetRPDS_LReq& msg
									) noexcept=0;

		/** This operation is invoked within the server thread
			whenever a SetTHS_P_HReq is received.
		 */
		virtual void	request(	Oscl::Driver::ST::LPS25H::Register::
									Req::Api::SetTHS_P_HReq& msg
									) noexcept=0;

		/** This operation is invoked within the server thread
			whenever a GetTHS_P_HReq is received.
		 */
		virtual void	request(	Oscl::Driver::ST::LPS25H::Register::
									Req::Api::GetTHS_P_HReq& msg
									) noexcept=0;

		/** This operation is invoked within the server thread
			whenever a SetTHS_P_LReq is received.
		 */
		virtual void	request(	Oscl::Driver::ST::LPS25H::Register::
									Req::Api::SetTHS_P_LReq& msg
									) noexcept=0;

		/** This operation is invoked within the server thread
			whenever a GetTHS_P_LReq is received.
		 */
		virtual void	request(	Oscl::Driver::ST::LPS25H::Register::
									Req::Api::GetTHS_P_LReq& msg
									) noexcept=0;

		/** This operation is invoked within the server thread
			whenever a GetFIFO_STATUSReq is received.
		 */
		virtual void	request(	Oscl::Driver::ST::LPS25H::Register::
									Req::Api::GetFIFO_STATUSReq& msg
									) noexcept=0;

		/** This operation is invoked within the server thread
			whenever a SetFIFO_CTRLReq is received.
		 */
		virtual void	request(	Oscl::Driver::ST::LPS25H::Register::
									Req::Api::SetFIFO_CTRLReq& msg
									) noexcept=0;

		/** This operation is invoked within the server thread
			whenever a GetFIFO_CTRLReq is received.
		 */
		virtual void	request(	Oscl::Driver::ST::LPS25H::Register::
									Req::Api::GetFIFO_CTRLReq& msg
									) noexcept=0;

		/** This operation is invoked within the server thread
			whenever a GetTEMP_OUT_HReq is received.
		 */
		virtual void	request(	Oscl::Driver::ST::LPS25H::Register::
									Req::Api::GetTEMP_OUT_HReq& msg
									) noexcept=0;

		/** This operation is invoked within the server thread
			whenever a GetTEMP_OUT_LReq is received.
		 */
		virtual void	request(	Oscl::Driver::ST::LPS25H::Register::
									Req::Api::GetTEMP_OUT_LReq& msg
									) noexcept=0;

		/** This operation is invoked within the server thread
			whenever a GetPRESS_OUT_HReq is received.
		 */
		virtual void	request(	Oscl::Driver::ST::LPS25H::Register::
									Req::Api::GetPRESS_OUT_HReq& msg
									) noexcept=0;

		/** This operation is invoked within the server thread
			whenever a GetPRESS_OUT_LReq is received.
		 */
		virtual void	request(	Oscl::Driver::ST::LPS25H::Register::
									Req::Api::GetPRESS_OUT_LReq& msg
									) noexcept=0;

		/** This operation is invoked within the server thread
			whenever a GetPRESS_OUT_XLReq is received.
		 */
		virtual void	request(	Oscl::Driver::ST::LPS25H::Register::
									Req::Api::GetPRESS_OUT_XLReq& msg
									) noexcept=0;

		/** This operation is invoked within the server thread
			whenever a GetSTATUS_REGReq is received.
		 */
		virtual void	request(	Oscl::Driver::ST::LPS25H::Register::
									Req::Api::GetSTATUS_REGReq& msg
									) noexcept=0;

		/** This operation is invoked within the server thread
			whenever a SetINT_SOURCEReq is received.
		 */
		virtual void	request(	Oscl::Driver::ST::LPS25H::Register::
									Req::Api::SetINT_SOURCEReq& msg
									) noexcept=0;

		/** This operation is invoked within the server thread
			whenever a GetINT_SOURCEReq is received.
		 */
		virtual void	request(	Oscl::Driver::ST::LPS25H::Register::
									Req::Api::GetINT_SOURCEReq& msg
									) noexcept=0;

		/** This operation is invoked within the server thread
			whenever a SetINT_CFGReq is received.
		 */
		virtual void	request(	Oscl::Driver::ST::LPS25H::Register::
									Req::Api::SetINT_CFGReq& msg
									) noexcept=0;

		/** This operation is invoked within the server thread
			whenever a GetINT_CFGReq is received.
		 */
		virtual void	request(	Oscl::Driver::ST::LPS25H::Register::
									Req::Api::GetINT_CFGReq& msg
									) noexcept=0;

		/** This operation is invoked within the server thread
			whenever a SetCTRL_REG4Req is received.
		 */
		virtual void	request(	Oscl::Driver::ST::LPS25H::Register::
									Req::Api::SetCTRL_REG4Req& msg
									) noexcept=0;

		/** This operation is invoked within the server thread
			whenever a GetCTRL_REG4Req is received.
		 */
		virtual void	request(	Oscl::Driver::ST::LPS25H::Register::
									Req::Api::GetCTRL_REG4Req& msg
									) noexcept=0;

		/** This operation is invoked within the server thread
			whenever a SetCTRL_REG3Req is received.
		 */
		virtual void	request(	Oscl::Driver::ST::LPS25H::Register::
									Req::Api::SetCTRL_REG3Req& msg
									) noexcept=0;

		/** This operation is invoked within the server thread
			whenever a GetCTRL_REG3Req is received.
		 */
		virtual void	request(	Oscl::Driver::ST::LPS25H::Register::
									Req::Api::GetCTRL_REG3Req& msg
									) noexcept=0;

		/** This operation is invoked within the server thread
			whenever a SetCTRL_REG2Req is received.
		 */
		virtual void	request(	Oscl::Driver::ST::LPS25H::Register::
									Req::Api::SetCTRL_REG2Req& msg
									) noexcept=0;

		/** This operation is invoked within the server thread
			whenever a GetCTRL_REG2Req is received.
		 */
		virtual void	request(	Oscl::Driver::ST::LPS25H::Register::
									Req::Api::GetCTRL_REG2Req& msg
									) noexcept=0;

		/** This operation is invoked within the server thread
			whenever a SetCTRL_REG1Req is received.
		 */
		virtual void	request(	Oscl::Driver::ST::LPS25H::Register::
									Req::Api::SetCTRL_REG1Req& msg
									) noexcept=0;

		/** This operation is invoked within the server thread
			whenever a GetCTRL_REG1Req is received.
		 */
		virtual void	request(	Oscl::Driver::ST::LPS25H::Register::
									Req::Api::GetCTRL_REG1Req& msg
									) noexcept=0;

		/** This operation is invoked within the server thread
			whenever a SetRES_CONFReq is received.
		 */
		virtual void	request(	Oscl::Driver::ST::LPS25H::Register::
									Req::Api::SetRES_CONFReq& msg
									) noexcept=0;

		/** This operation is invoked within the server thread
			whenever a GetRES_CONFReq is received.
		 */
		virtual void	request(	Oscl::Driver::ST::LPS25H::Register::
									Req::Api::GetRES_CONFReq& msg
									) noexcept=0;

		/** This operation is invoked within the server thread
			whenever a GetWHO_AM_IReq is received.
		 */
		virtual void	request(	Oscl::Driver::ST::LPS25H::Register::
									Req::Api::GetWHO_AM_IReq& msg
									) noexcept=0;

		/** This operation is invoked within the server thread
			whenever a SetREF_P_HReq is received.
		 */
		virtual void	request(	Oscl::Driver::ST::LPS25H::Register::
									Req::Api::SetREF_P_HReq& msg
									) noexcept=0;

		/** This operation is invoked within the server thread
			whenever a GetREF_P_HReq is received.
		 */
		virtual void	request(	Oscl::Driver::ST::LPS25H::Register::
									Req::Api::GetREF_P_HReq& msg
									) noexcept=0;

		/** This operation is invoked within the server thread
			whenever a SetREF_P_LReq is received.
		 */
		virtual void	request(	Oscl::Driver::ST::LPS25H::Register::
									Req::Api::SetREF_P_LReq& msg
									) noexcept=0;

		/** This operation is invoked within the server thread
			whenever a GetREF_P_LReq is received.
		 */
		virtual void	request(	Oscl::Driver::ST::LPS25H::Register::
									Req::Api::GetREF_P_LReq& msg
									) noexcept=0;

		/** This operation is invoked within the server thread
			whenever a SetREF_P_XLReq is received.
		 */
		virtual void	request(	Oscl::Driver::ST::LPS25H::Register::
									Req::Api::SetREF_P_XLReq& msg
									) noexcept=0;

		/** This operation is invoked within the server thread
			whenever a GetREF_P_XLReq is received.
		 */
		virtual void	request(	Oscl::Driver::ST::LPS25H::Register::
									Req::Api::GetREF_P_XLReq& msg
									) noexcept=0;

	};

}
}
}
}
}
}
#endif
