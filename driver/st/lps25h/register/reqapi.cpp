/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "reqapi.h"

using namespace Oscl::Driver::ST::LPS25H::Register;

Oscl::Driver::ST::LPS25H::Register::Req::Api::GetTEMP_OUTPayload::
GetTEMP_OUTPayload(
		uint8_t	maxBufferLength,
		void*	buffer
		) noexcept:
		_maxBufferLength(maxBufferLength),
		_buffer(buffer)
		{}

Oscl::Driver::ST::LPS25H::Register::Req::Api::GetPRESS_OUTPayload::
GetPRESS_OUTPayload(
		uint8_t	maxBufferLength,
		void*	buffer
		) noexcept:
		_maxBufferLength(maxBufferLength),
		_buffer(buffer)
		{}

Oscl::Driver::ST::LPS25H::Register::Req::Api::SetRPDS_HPayload::
SetRPDS_HPayload(
		uint8_t	value
		) noexcept:
		_value(value)
		{}

Oscl::Driver::ST::LPS25H::Register::Req::Api::GetRPDS_HPayload::
GetRPDS_HPayload(
		) noexcept
		{}

Oscl::Driver::ST::LPS25H::Register::Req::Api::SetRPDS_LPayload::
SetRPDS_LPayload(
		uint8_t	value
		) noexcept:
		_value(value)
		{}

Oscl::Driver::ST::LPS25H::Register::Req::Api::GetRPDS_LPayload::
GetRPDS_LPayload(
		) noexcept
		{}

Oscl::Driver::ST::LPS25H::Register::Req::Api::SetTHS_P_HPayload::
SetTHS_P_HPayload(
		uint8_t	value
		) noexcept:
		_value(value)
		{}

Oscl::Driver::ST::LPS25H::Register::Req::Api::GetTHS_P_HPayload::
GetTHS_P_HPayload(
		) noexcept
		{}

Oscl::Driver::ST::LPS25H::Register::Req::Api::SetTHS_P_LPayload::
SetTHS_P_LPayload(
		uint8_t	value
		) noexcept:
		_value(value)
		{}

Oscl::Driver::ST::LPS25H::Register::Req::Api::GetTHS_P_LPayload::
GetTHS_P_LPayload(
		) noexcept
		{}

Oscl::Driver::ST::LPS25H::Register::Req::Api::GetFIFO_STATUSPayload::
GetFIFO_STATUSPayload(
		) noexcept
		{}

Oscl::Driver::ST::LPS25H::Register::Req::Api::SetFIFO_CTRLPayload::
SetFIFO_CTRLPayload(
		uint8_t	value
		) noexcept:
		_value(value)
		{}

Oscl::Driver::ST::LPS25H::Register::Req::Api::GetFIFO_CTRLPayload::
GetFIFO_CTRLPayload(
		) noexcept
		{}

Oscl::Driver::ST::LPS25H::Register::Req::Api::GetTEMP_OUT_HPayload::
GetTEMP_OUT_HPayload(
		) noexcept
		{}

Oscl::Driver::ST::LPS25H::Register::Req::Api::GetTEMP_OUT_LPayload::
GetTEMP_OUT_LPayload(
		) noexcept
		{}

Oscl::Driver::ST::LPS25H::Register::Req::Api::GetPRESS_OUT_HPayload::
GetPRESS_OUT_HPayload(
		) noexcept
		{}

Oscl::Driver::ST::LPS25H::Register::Req::Api::GetPRESS_OUT_LPayload::
GetPRESS_OUT_LPayload(
		) noexcept
		{}

Oscl::Driver::ST::LPS25H::Register::Req::Api::GetPRESS_OUT_XLPayload::
GetPRESS_OUT_XLPayload(
		) noexcept
		{}

Oscl::Driver::ST::LPS25H::Register::Req::Api::GetSTATUS_REGPayload::
GetSTATUS_REGPayload(
		) noexcept
		{}

Oscl::Driver::ST::LPS25H::Register::Req::Api::SetINT_SOURCEPayload::
SetINT_SOURCEPayload(
		uint8_t	value
		) noexcept:
		_value(value)
		{}

Oscl::Driver::ST::LPS25H::Register::Req::Api::GetINT_SOURCEPayload::
GetINT_SOURCEPayload(
		) noexcept
		{}

Oscl::Driver::ST::LPS25H::Register::Req::Api::SetINT_CFGPayload::
SetINT_CFGPayload(
		uint8_t	value
		) noexcept:
		_value(value)
		{}

Oscl::Driver::ST::LPS25H::Register::Req::Api::GetINT_CFGPayload::
GetINT_CFGPayload(
		) noexcept
		{}

Oscl::Driver::ST::LPS25H::Register::Req::Api::SetCTRL_REG4Payload::
SetCTRL_REG4Payload(
		uint8_t	value
		) noexcept:
		_value(value)
		{}

Oscl::Driver::ST::LPS25H::Register::Req::Api::GetCTRL_REG4Payload::
GetCTRL_REG4Payload(
		) noexcept
		{}

Oscl::Driver::ST::LPS25H::Register::Req::Api::SetCTRL_REG3Payload::
SetCTRL_REG3Payload(
		uint8_t	value
		) noexcept:
		_value(value)
		{}

Oscl::Driver::ST::LPS25H::Register::Req::Api::GetCTRL_REG3Payload::
GetCTRL_REG3Payload(
		) noexcept
		{}

Oscl::Driver::ST::LPS25H::Register::Req::Api::SetCTRL_REG2Payload::
SetCTRL_REG2Payload(
		uint8_t	value
		) noexcept:
		_value(value)
		{}

Oscl::Driver::ST::LPS25H::Register::Req::Api::GetCTRL_REG2Payload::
GetCTRL_REG2Payload(
		) noexcept
		{}

Oscl::Driver::ST::LPS25H::Register::Req::Api::SetCTRL_REG1Payload::
SetCTRL_REG1Payload(
		uint8_t	value
		) noexcept:
		_value(value)
		{}

Oscl::Driver::ST::LPS25H::Register::Req::Api::GetCTRL_REG1Payload::
GetCTRL_REG1Payload(
		) noexcept
		{}

Oscl::Driver::ST::LPS25H::Register::Req::Api::SetRES_CONFPayload::
SetRES_CONFPayload(
		uint8_t	value
		) noexcept:
		_value(value)
		{}

Oscl::Driver::ST::LPS25H::Register::Req::Api::GetRES_CONFPayload::
GetRES_CONFPayload(
		) noexcept
		{}

Oscl::Driver::ST::LPS25H::Register::Req::Api::GetWHO_AM_IPayload::
GetWHO_AM_IPayload(
		) noexcept
		{}

Oscl::Driver::ST::LPS25H::Register::Req::Api::SetREF_P_HPayload::
SetREF_P_HPayload(
		uint8_t	value
		) noexcept:
		_value(value)
		{}

Oscl::Driver::ST::LPS25H::Register::Req::Api::GetREF_P_HPayload::
GetREF_P_HPayload(
		) noexcept
		{}

Oscl::Driver::ST::LPS25H::Register::Req::Api::SetREF_P_LPayload::
SetREF_P_LPayload(
		uint8_t	value
		) noexcept:
		_value(value)
		{}

Oscl::Driver::ST::LPS25H::Register::Req::Api::GetREF_P_LPayload::
GetREF_P_LPayload(
		) noexcept
		{}

Oscl::Driver::ST::LPS25H::Register::Req::Api::SetREF_P_XLPayload::
SetREF_P_XLPayload(
		uint8_t	value
		) noexcept:
		_value(value)
		{}

Oscl::Driver::ST::LPS25H::Register::Req::Api::GetREF_P_XLPayload::
GetREF_P_XLPayload(
		) noexcept
		{}

