/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_driver_st_lps25h_register_itc_respapih_
#define _oscl_driver_st_lps25h_register_itc_respapih_

#include "reqapi.h"
#include "oscl/mt/itc/mbox/clirsp.h"

/** */
namespace Oscl {

/** */
namespace Driver {

/** */
namespace ST {

/** */
namespace LPS25H {

/** */
namespace Register {

/** */
namespace Resp {

/**	This class describes an ITC client interface. The
	interface includes the definition of client response messages
	based on their corresponding server request messages and
	associated payloads, as well as an interface that is to be
	implemented by a client that uses the corresponding server
	asynchronously.
 */
/**	This interface defines methods to access the LPS25H IMU
	Magnetometer registers indpendent of the type of bus.
 */
class Api {
	public:
		/**	This describes a client response message that corresponds
			to the GetTEMP_OUTReq message described in the "reqapi.h"
			header file. This GetTEMP_OUTResp response message actually
			contains a GetTEMP_OUTReq request message.
		 */
		typedef Oscl::Mt::Itc::CliResponse<
					Oscl::Driver::ST::LPS25H::Register::Req::Api,
					Api,
					Oscl::Driver::ST::LPS25H::Register::
					Req::Api::GetTEMP_OUTPayload
					>		GetTEMP_OUTResp;

	public:
		/**	This describes a client response message that corresponds
			to the GetPRESS_OUTReq message described in the "reqapi.h"
			header file. This GetPRESS_OUTResp response message actually
			contains a GetPRESS_OUTReq request message.
		 */
		typedef Oscl::Mt::Itc::CliResponse<
					Oscl::Driver::ST::LPS25H::Register::Req::Api,
					Api,
					Oscl::Driver::ST::LPS25H::Register::
					Req::Api::GetPRESS_OUTPayload
					>		GetPRESS_OUTResp;

	public:
		/**	This describes a client response message that corresponds
			to the SetRPDS_HReq message described in the "reqapi.h"
			header file. This SetRPDS_HResp response message actually
			contains a SetRPDS_HReq request message.
		 */
		typedef Oscl::Mt::Itc::CliResponse<
					Oscl::Driver::ST::LPS25H::Register::Req::Api,
					Api,
					Oscl::Driver::ST::LPS25H::Register::
					Req::Api::SetRPDS_HPayload
					>		SetRPDS_HResp;

	public:
		/**	This describes a client response message that corresponds
			to the GetRPDS_HReq message described in the "reqapi.h"
			header file. This GetRPDS_HResp response message actually
			contains a GetRPDS_HReq request message.
		 */
		typedef Oscl::Mt::Itc::CliResponse<
					Oscl::Driver::ST::LPS25H::Register::Req::Api,
					Api,
					Oscl::Driver::ST::LPS25H::Register::
					Req::Api::GetRPDS_HPayload
					>		GetRPDS_HResp;

	public:
		/**	This describes a client response message that corresponds
			to the SetRPDS_LReq message described in the "reqapi.h"
			header file. This SetRPDS_LResp response message actually
			contains a SetRPDS_LReq request message.
		 */
		typedef Oscl::Mt::Itc::CliResponse<
					Oscl::Driver::ST::LPS25H::Register::Req::Api,
					Api,
					Oscl::Driver::ST::LPS25H::Register::
					Req::Api::SetRPDS_LPayload
					>		SetRPDS_LResp;

	public:
		/**	This describes a client response message that corresponds
			to the GetRPDS_LReq message described in the "reqapi.h"
			header file. This GetRPDS_LResp response message actually
			contains a GetRPDS_LReq request message.
		 */
		typedef Oscl::Mt::Itc::CliResponse<
					Oscl::Driver::ST::LPS25H::Register::Req::Api,
					Api,
					Oscl::Driver::ST::LPS25H::Register::
					Req::Api::GetRPDS_LPayload
					>		GetRPDS_LResp;

	public:
		/**	This describes a client response message that corresponds
			to the SetTHS_P_HReq message described in the "reqapi.h"
			header file. This SetTHS_P_HResp response message actually
			contains a SetTHS_P_HReq request message.
		 */
		typedef Oscl::Mt::Itc::CliResponse<
					Oscl::Driver::ST::LPS25H::Register::Req::Api,
					Api,
					Oscl::Driver::ST::LPS25H::Register::
					Req::Api::SetTHS_P_HPayload
					>		SetTHS_P_HResp;

	public:
		/**	This describes a client response message that corresponds
			to the GetTHS_P_HReq message described in the "reqapi.h"
			header file. This GetTHS_P_HResp response message actually
			contains a GetTHS_P_HReq request message.
		 */
		typedef Oscl::Mt::Itc::CliResponse<
					Oscl::Driver::ST::LPS25H::Register::Req::Api,
					Api,
					Oscl::Driver::ST::LPS25H::Register::
					Req::Api::GetTHS_P_HPayload
					>		GetTHS_P_HResp;

	public:
		/**	This describes a client response message that corresponds
			to the SetTHS_P_LReq message described in the "reqapi.h"
			header file. This SetTHS_P_LResp response message actually
			contains a SetTHS_P_LReq request message.
		 */
		typedef Oscl::Mt::Itc::CliResponse<
					Oscl::Driver::ST::LPS25H::Register::Req::Api,
					Api,
					Oscl::Driver::ST::LPS25H::Register::
					Req::Api::SetTHS_P_LPayload
					>		SetTHS_P_LResp;

	public:
		/**	This describes a client response message that corresponds
			to the GetTHS_P_LReq message described in the "reqapi.h"
			header file. This GetTHS_P_LResp response message actually
			contains a GetTHS_P_LReq request message.
		 */
		typedef Oscl::Mt::Itc::CliResponse<
					Oscl::Driver::ST::LPS25H::Register::Req::Api,
					Api,
					Oscl::Driver::ST::LPS25H::Register::
					Req::Api::GetTHS_P_LPayload
					>		GetTHS_P_LResp;

	public:
		/**	This describes a client response message that corresponds
			to the GetFIFO_STATUSReq message described in the "reqapi.h"
			header file. This GetFIFO_STATUSResp response message actually
			contains a GetFIFO_STATUSReq request message.
		 */
		typedef Oscl::Mt::Itc::CliResponse<
					Oscl::Driver::ST::LPS25H::Register::Req::Api,
					Api,
					Oscl::Driver::ST::LPS25H::Register::
					Req::Api::GetFIFO_STATUSPayload
					>		GetFIFO_STATUSResp;

	public:
		/**	This describes a client response message that corresponds
			to the SetFIFO_CTRLReq message described in the "reqapi.h"
			header file. This SetFIFO_CTRLResp response message actually
			contains a SetFIFO_CTRLReq request message.
		 */
		typedef Oscl::Mt::Itc::CliResponse<
					Oscl::Driver::ST::LPS25H::Register::Req::Api,
					Api,
					Oscl::Driver::ST::LPS25H::Register::
					Req::Api::SetFIFO_CTRLPayload
					>		SetFIFO_CTRLResp;

	public:
		/**	This describes a client response message that corresponds
			to the GetFIFO_CTRLReq message described in the "reqapi.h"
			header file. This GetFIFO_CTRLResp response message actually
			contains a GetFIFO_CTRLReq request message.
		 */
		typedef Oscl::Mt::Itc::CliResponse<
					Oscl::Driver::ST::LPS25H::Register::Req::Api,
					Api,
					Oscl::Driver::ST::LPS25H::Register::
					Req::Api::GetFIFO_CTRLPayload
					>		GetFIFO_CTRLResp;

	public:
		/**	This describes a client response message that corresponds
			to the GetTEMP_OUT_HReq message described in the "reqapi.h"
			header file. This GetTEMP_OUT_HResp response message actually
			contains a GetTEMP_OUT_HReq request message.
		 */
		typedef Oscl::Mt::Itc::CliResponse<
					Oscl::Driver::ST::LPS25H::Register::Req::Api,
					Api,
					Oscl::Driver::ST::LPS25H::Register::
					Req::Api::GetTEMP_OUT_HPayload
					>		GetTEMP_OUT_HResp;

	public:
		/**	This describes a client response message that corresponds
			to the GetTEMP_OUT_LReq message described in the "reqapi.h"
			header file. This GetTEMP_OUT_LResp response message actually
			contains a GetTEMP_OUT_LReq request message.
		 */
		typedef Oscl::Mt::Itc::CliResponse<
					Oscl::Driver::ST::LPS25H::Register::Req::Api,
					Api,
					Oscl::Driver::ST::LPS25H::Register::
					Req::Api::GetTEMP_OUT_LPayload
					>		GetTEMP_OUT_LResp;

	public:
		/**	This describes a client response message that corresponds
			to the GetPRESS_OUT_HReq message described in the "reqapi.h"
			header file. This GetPRESS_OUT_HResp response message actually
			contains a GetPRESS_OUT_HReq request message.
		 */
		typedef Oscl::Mt::Itc::CliResponse<
					Oscl::Driver::ST::LPS25H::Register::Req::Api,
					Api,
					Oscl::Driver::ST::LPS25H::Register::
					Req::Api::GetPRESS_OUT_HPayload
					>		GetPRESS_OUT_HResp;

	public:
		/**	This describes a client response message that corresponds
			to the GetPRESS_OUT_LReq message described in the "reqapi.h"
			header file. This GetPRESS_OUT_LResp response message actually
			contains a GetPRESS_OUT_LReq request message.
		 */
		typedef Oscl::Mt::Itc::CliResponse<
					Oscl::Driver::ST::LPS25H::Register::Req::Api,
					Api,
					Oscl::Driver::ST::LPS25H::Register::
					Req::Api::GetPRESS_OUT_LPayload
					>		GetPRESS_OUT_LResp;

	public:
		/**	This describes a client response message that corresponds
			to the GetPRESS_OUT_XLReq message described in the "reqapi.h"
			header file. This GetPRESS_OUT_XLResp response message actually
			contains a GetPRESS_OUT_XLReq request message.
		 */
		typedef Oscl::Mt::Itc::CliResponse<
					Oscl::Driver::ST::LPS25H::Register::Req::Api,
					Api,
					Oscl::Driver::ST::LPS25H::Register::
					Req::Api::GetPRESS_OUT_XLPayload
					>		GetPRESS_OUT_XLResp;

	public:
		/**	This describes a client response message that corresponds
			to the GetSTATUS_REGReq message described in the "reqapi.h"
			header file. This GetSTATUS_REGResp response message actually
			contains a GetSTATUS_REGReq request message.
		 */
		typedef Oscl::Mt::Itc::CliResponse<
					Oscl::Driver::ST::LPS25H::Register::Req::Api,
					Api,
					Oscl::Driver::ST::LPS25H::Register::
					Req::Api::GetSTATUS_REGPayload
					>		GetSTATUS_REGResp;

	public:
		/**	This describes a client response message that corresponds
			to the SetINT_SOURCEReq message described in the "reqapi.h"
			header file. This SetINT_SOURCEResp response message actually
			contains a SetINT_SOURCEReq request message.
		 */
		typedef Oscl::Mt::Itc::CliResponse<
					Oscl::Driver::ST::LPS25H::Register::Req::Api,
					Api,
					Oscl::Driver::ST::LPS25H::Register::
					Req::Api::SetINT_SOURCEPayload
					>		SetINT_SOURCEResp;

	public:
		/**	This describes a client response message that corresponds
			to the GetINT_SOURCEReq message described in the "reqapi.h"
			header file. This GetINT_SOURCEResp response message actually
			contains a GetINT_SOURCEReq request message.
		 */
		typedef Oscl::Mt::Itc::CliResponse<
					Oscl::Driver::ST::LPS25H::Register::Req::Api,
					Api,
					Oscl::Driver::ST::LPS25H::Register::
					Req::Api::GetINT_SOURCEPayload
					>		GetINT_SOURCEResp;

	public:
		/**	This describes a client response message that corresponds
			to the SetINT_CFGReq message described in the "reqapi.h"
			header file. This SetINT_CFGResp response message actually
			contains a SetINT_CFGReq request message.
		 */
		typedef Oscl::Mt::Itc::CliResponse<
					Oscl::Driver::ST::LPS25H::Register::Req::Api,
					Api,
					Oscl::Driver::ST::LPS25H::Register::
					Req::Api::SetINT_CFGPayload
					>		SetINT_CFGResp;

	public:
		/**	This describes a client response message that corresponds
			to the GetINT_CFGReq message described in the "reqapi.h"
			header file. This GetINT_CFGResp response message actually
			contains a GetINT_CFGReq request message.
		 */
		typedef Oscl::Mt::Itc::CliResponse<
					Oscl::Driver::ST::LPS25H::Register::Req::Api,
					Api,
					Oscl::Driver::ST::LPS25H::Register::
					Req::Api::GetINT_CFGPayload
					>		GetINT_CFGResp;

	public:
		/**	This describes a client response message that corresponds
			to the SetCTRL_REG4Req message described in the "reqapi.h"
			header file. This SetCTRL_REG4Resp response message actually
			contains a SetCTRL_REG4Req request message.
		 */
		typedef Oscl::Mt::Itc::CliResponse<
					Oscl::Driver::ST::LPS25H::Register::Req::Api,
					Api,
					Oscl::Driver::ST::LPS25H::Register::
					Req::Api::SetCTRL_REG4Payload
					>		SetCTRL_REG4Resp;

	public:
		/**	This describes a client response message that corresponds
			to the GetCTRL_REG4Req message described in the "reqapi.h"
			header file. This GetCTRL_REG4Resp response message actually
			contains a GetCTRL_REG4Req request message.
		 */
		typedef Oscl::Mt::Itc::CliResponse<
					Oscl::Driver::ST::LPS25H::Register::Req::Api,
					Api,
					Oscl::Driver::ST::LPS25H::Register::
					Req::Api::GetCTRL_REG4Payload
					>		GetCTRL_REG4Resp;

	public:
		/**	This describes a client response message that corresponds
			to the SetCTRL_REG3Req message described in the "reqapi.h"
			header file. This SetCTRL_REG3Resp response message actually
			contains a SetCTRL_REG3Req request message.
		 */
		typedef Oscl::Mt::Itc::CliResponse<
					Oscl::Driver::ST::LPS25H::Register::Req::Api,
					Api,
					Oscl::Driver::ST::LPS25H::Register::
					Req::Api::SetCTRL_REG3Payload
					>		SetCTRL_REG3Resp;

	public:
		/**	This describes a client response message that corresponds
			to the GetCTRL_REG3Req message described in the "reqapi.h"
			header file. This GetCTRL_REG3Resp response message actually
			contains a GetCTRL_REG3Req request message.
		 */
		typedef Oscl::Mt::Itc::CliResponse<
					Oscl::Driver::ST::LPS25H::Register::Req::Api,
					Api,
					Oscl::Driver::ST::LPS25H::Register::
					Req::Api::GetCTRL_REG3Payload
					>		GetCTRL_REG3Resp;

	public:
		/**	This describes a client response message that corresponds
			to the SetCTRL_REG2Req message described in the "reqapi.h"
			header file. This SetCTRL_REG2Resp response message actually
			contains a SetCTRL_REG2Req request message.
		 */
		typedef Oscl::Mt::Itc::CliResponse<
					Oscl::Driver::ST::LPS25H::Register::Req::Api,
					Api,
					Oscl::Driver::ST::LPS25H::Register::
					Req::Api::SetCTRL_REG2Payload
					>		SetCTRL_REG2Resp;

	public:
		/**	This describes a client response message that corresponds
			to the GetCTRL_REG2Req message described in the "reqapi.h"
			header file. This GetCTRL_REG2Resp response message actually
			contains a GetCTRL_REG2Req request message.
		 */
		typedef Oscl::Mt::Itc::CliResponse<
					Oscl::Driver::ST::LPS25H::Register::Req::Api,
					Api,
					Oscl::Driver::ST::LPS25H::Register::
					Req::Api::GetCTRL_REG2Payload
					>		GetCTRL_REG2Resp;

	public:
		/**	This describes a client response message that corresponds
			to the SetCTRL_REG1Req message described in the "reqapi.h"
			header file. This SetCTRL_REG1Resp response message actually
			contains a SetCTRL_REG1Req request message.
		 */
		typedef Oscl::Mt::Itc::CliResponse<
					Oscl::Driver::ST::LPS25H::Register::Req::Api,
					Api,
					Oscl::Driver::ST::LPS25H::Register::
					Req::Api::SetCTRL_REG1Payload
					>		SetCTRL_REG1Resp;

	public:
		/**	This describes a client response message that corresponds
			to the GetCTRL_REG1Req message described in the "reqapi.h"
			header file. This GetCTRL_REG1Resp response message actually
			contains a GetCTRL_REG1Req request message.
		 */
		typedef Oscl::Mt::Itc::CliResponse<
					Oscl::Driver::ST::LPS25H::Register::Req::Api,
					Api,
					Oscl::Driver::ST::LPS25H::Register::
					Req::Api::GetCTRL_REG1Payload
					>		GetCTRL_REG1Resp;

	public:
		/**	This describes a client response message that corresponds
			to the SetRES_CONFReq message described in the "reqapi.h"
			header file. This SetRES_CONFResp response message actually
			contains a SetRES_CONFReq request message.
		 */
		typedef Oscl::Mt::Itc::CliResponse<
					Oscl::Driver::ST::LPS25H::Register::Req::Api,
					Api,
					Oscl::Driver::ST::LPS25H::Register::
					Req::Api::SetRES_CONFPayload
					>		SetRES_CONFResp;

	public:
		/**	This describes a client response message that corresponds
			to the GetRES_CONFReq message described in the "reqapi.h"
			header file. This GetRES_CONFResp response message actually
			contains a GetRES_CONFReq request message.
		 */
		typedef Oscl::Mt::Itc::CliResponse<
					Oscl::Driver::ST::LPS25H::Register::Req::Api,
					Api,
					Oscl::Driver::ST::LPS25H::Register::
					Req::Api::GetRES_CONFPayload
					>		GetRES_CONFResp;

	public:
		/**	This describes a client response message that corresponds
			to the GetWHO_AM_IReq message described in the "reqapi.h"
			header file. This GetWHO_AM_IResp response message actually
			contains a GetWHO_AM_IReq request message.
		 */
		typedef Oscl::Mt::Itc::CliResponse<
					Oscl::Driver::ST::LPS25H::Register::Req::Api,
					Api,
					Oscl::Driver::ST::LPS25H::Register::
					Req::Api::GetWHO_AM_IPayload
					>		GetWHO_AM_IResp;

	public:
		/**	This describes a client response message that corresponds
			to the SetREF_P_HReq message described in the "reqapi.h"
			header file. This SetREF_P_HResp response message actually
			contains a SetREF_P_HReq request message.
		 */
		typedef Oscl::Mt::Itc::CliResponse<
					Oscl::Driver::ST::LPS25H::Register::Req::Api,
					Api,
					Oscl::Driver::ST::LPS25H::Register::
					Req::Api::SetREF_P_HPayload
					>		SetREF_P_HResp;

	public:
		/**	This describes a client response message that corresponds
			to the GetREF_P_HReq message described in the "reqapi.h"
			header file. This GetREF_P_HResp response message actually
			contains a GetREF_P_HReq request message.
		 */
		typedef Oscl::Mt::Itc::CliResponse<
					Oscl::Driver::ST::LPS25H::Register::Req::Api,
					Api,
					Oscl::Driver::ST::LPS25H::Register::
					Req::Api::GetREF_P_HPayload
					>		GetREF_P_HResp;

	public:
		/**	This describes a client response message that corresponds
			to the SetREF_P_LReq message described in the "reqapi.h"
			header file. This SetREF_P_LResp response message actually
			contains a SetREF_P_LReq request message.
		 */
		typedef Oscl::Mt::Itc::CliResponse<
					Oscl::Driver::ST::LPS25H::Register::Req::Api,
					Api,
					Oscl::Driver::ST::LPS25H::Register::
					Req::Api::SetREF_P_LPayload
					>		SetREF_P_LResp;

	public:
		/**	This describes a client response message that corresponds
			to the GetREF_P_LReq message described in the "reqapi.h"
			header file. This GetREF_P_LResp response message actually
			contains a GetREF_P_LReq request message.
		 */
		typedef Oscl::Mt::Itc::CliResponse<
					Oscl::Driver::ST::LPS25H::Register::Req::Api,
					Api,
					Oscl::Driver::ST::LPS25H::Register::
					Req::Api::GetREF_P_LPayload
					>		GetREF_P_LResp;

	public:
		/**	This describes a client response message that corresponds
			to the SetREF_P_XLReq message described in the "reqapi.h"
			header file. This SetREF_P_XLResp response message actually
			contains a SetREF_P_XLReq request message.
		 */
		typedef Oscl::Mt::Itc::CliResponse<
					Oscl::Driver::ST::LPS25H::Register::Req::Api,
					Api,
					Oscl::Driver::ST::LPS25H::Register::
					Req::Api::SetREF_P_XLPayload
					>		SetREF_P_XLResp;

	public:
		/**	This describes a client response message that corresponds
			to the GetREF_P_XLReq message described in the "reqapi.h"
			header file. This GetREF_P_XLResp response message actually
			contains a GetREF_P_XLReq request message.
		 */
		typedef Oscl::Mt::Itc::CliResponse<
					Oscl::Driver::ST::LPS25H::Register::Req::Api,
					Api,
					Oscl::Driver::ST::LPS25H::Register::
					Req::Api::GetREF_P_XLPayload
					>		GetREF_P_XLResp;

	public:
		/** Make the compiler happy. */
		virtual ~Api(){}

		/** This operation is invoked within the client thread
			whenever a GetTEMP_OUTResp message is received.
		 */
		virtual void	response(	Oscl::Driver::ST::LPS25H::Register::
									Resp::Api::GetTEMP_OUTResp& msg
									) noexcept=0;

		/** This operation is invoked within the client thread
			whenever a GetPRESS_OUTResp message is received.
		 */
		virtual void	response(	Oscl::Driver::ST::LPS25H::Register::
									Resp::Api::GetPRESS_OUTResp& msg
									) noexcept=0;

		/** This operation is invoked within the client thread
			whenever a SetRPDS_HResp message is received.
		 */
		virtual void	response(	Oscl::Driver::ST::LPS25H::Register::
									Resp::Api::SetRPDS_HResp& msg
									) noexcept=0;

		/** This operation is invoked within the client thread
			whenever a GetRPDS_HResp message is received.
		 */
		virtual void	response(	Oscl::Driver::ST::LPS25H::Register::
									Resp::Api::GetRPDS_HResp& msg
									) noexcept=0;

		/** This operation is invoked within the client thread
			whenever a SetRPDS_LResp message is received.
		 */
		virtual void	response(	Oscl::Driver::ST::LPS25H::Register::
									Resp::Api::SetRPDS_LResp& msg
									) noexcept=0;

		/** This operation is invoked within the client thread
			whenever a GetRPDS_LResp message is received.
		 */
		virtual void	response(	Oscl::Driver::ST::LPS25H::Register::
									Resp::Api::GetRPDS_LResp& msg
									) noexcept=0;

		/** This operation is invoked within the client thread
			whenever a SetTHS_P_HResp message is received.
		 */
		virtual void	response(	Oscl::Driver::ST::LPS25H::Register::
									Resp::Api::SetTHS_P_HResp& msg
									) noexcept=0;

		/** This operation is invoked within the client thread
			whenever a GetTHS_P_HResp message is received.
		 */
		virtual void	response(	Oscl::Driver::ST::LPS25H::Register::
									Resp::Api::GetTHS_P_HResp& msg
									) noexcept=0;

		/** This operation is invoked within the client thread
			whenever a SetTHS_P_LResp message is received.
		 */
		virtual void	response(	Oscl::Driver::ST::LPS25H::Register::
									Resp::Api::SetTHS_P_LResp& msg
									) noexcept=0;

		/** This operation is invoked within the client thread
			whenever a GetTHS_P_LResp message is received.
		 */
		virtual void	response(	Oscl::Driver::ST::LPS25H::Register::
									Resp::Api::GetTHS_P_LResp& msg
									) noexcept=0;

		/** This operation is invoked within the client thread
			whenever a GetFIFO_STATUSResp message is received.
		 */
		virtual void	response(	Oscl::Driver::ST::LPS25H::Register::
									Resp::Api::GetFIFO_STATUSResp& msg
									) noexcept=0;

		/** This operation is invoked within the client thread
			whenever a SetFIFO_CTRLResp message is received.
		 */
		virtual void	response(	Oscl::Driver::ST::LPS25H::Register::
									Resp::Api::SetFIFO_CTRLResp& msg
									) noexcept=0;

		/** This operation is invoked within the client thread
			whenever a GetFIFO_CTRLResp message is received.
		 */
		virtual void	response(	Oscl::Driver::ST::LPS25H::Register::
									Resp::Api::GetFIFO_CTRLResp& msg
									) noexcept=0;

		/** This operation is invoked within the client thread
			whenever a GetTEMP_OUT_HResp message is received.
		 */
		virtual void	response(	Oscl::Driver::ST::LPS25H::Register::
									Resp::Api::GetTEMP_OUT_HResp& msg
									) noexcept=0;

		/** This operation is invoked within the client thread
			whenever a GetTEMP_OUT_LResp message is received.
		 */
		virtual void	response(	Oscl::Driver::ST::LPS25H::Register::
									Resp::Api::GetTEMP_OUT_LResp& msg
									) noexcept=0;

		/** This operation is invoked within the client thread
			whenever a GetPRESS_OUT_HResp message is received.
		 */
		virtual void	response(	Oscl::Driver::ST::LPS25H::Register::
									Resp::Api::GetPRESS_OUT_HResp& msg
									) noexcept=0;

		/** This operation is invoked within the client thread
			whenever a GetPRESS_OUT_LResp message is received.
		 */
		virtual void	response(	Oscl::Driver::ST::LPS25H::Register::
									Resp::Api::GetPRESS_OUT_LResp& msg
									) noexcept=0;

		/** This operation is invoked within the client thread
			whenever a GetPRESS_OUT_XLResp message is received.
		 */
		virtual void	response(	Oscl::Driver::ST::LPS25H::Register::
									Resp::Api::GetPRESS_OUT_XLResp& msg
									) noexcept=0;

		/** This operation is invoked within the client thread
			whenever a GetSTATUS_REGResp message is received.
		 */
		virtual void	response(	Oscl::Driver::ST::LPS25H::Register::
									Resp::Api::GetSTATUS_REGResp& msg
									) noexcept=0;

		/** This operation is invoked within the client thread
			whenever a SetINT_SOURCEResp message is received.
		 */
		virtual void	response(	Oscl::Driver::ST::LPS25H::Register::
									Resp::Api::SetINT_SOURCEResp& msg
									) noexcept=0;

		/** This operation is invoked within the client thread
			whenever a GetINT_SOURCEResp message is received.
		 */
		virtual void	response(	Oscl::Driver::ST::LPS25H::Register::
									Resp::Api::GetINT_SOURCEResp& msg
									) noexcept=0;

		/** This operation is invoked within the client thread
			whenever a SetINT_CFGResp message is received.
		 */
		virtual void	response(	Oscl::Driver::ST::LPS25H::Register::
									Resp::Api::SetINT_CFGResp& msg
									) noexcept=0;

		/** This operation is invoked within the client thread
			whenever a GetINT_CFGResp message is received.
		 */
		virtual void	response(	Oscl::Driver::ST::LPS25H::Register::
									Resp::Api::GetINT_CFGResp& msg
									) noexcept=0;

		/** This operation is invoked within the client thread
			whenever a SetCTRL_REG4Resp message is received.
		 */
		virtual void	response(	Oscl::Driver::ST::LPS25H::Register::
									Resp::Api::SetCTRL_REG4Resp& msg
									) noexcept=0;

		/** This operation is invoked within the client thread
			whenever a GetCTRL_REG4Resp message is received.
		 */
		virtual void	response(	Oscl::Driver::ST::LPS25H::Register::
									Resp::Api::GetCTRL_REG4Resp& msg
									) noexcept=0;

		/** This operation is invoked within the client thread
			whenever a SetCTRL_REG3Resp message is received.
		 */
		virtual void	response(	Oscl::Driver::ST::LPS25H::Register::
									Resp::Api::SetCTRL_REG3Resp& msg
									) noexcept=0;

		/** This operation is invoked within the client thread
			whenever a GetCTRL_REG3Resp message is received.
		 */
		virtual void	response(	Oscl::Driver::ST::LPS25H::Register::
									Resp::Api::GetCTRL_REG3Resp& msg
									) noexcept=0;

		/** This operation is invoked within the client thread
			whenever a SetCTRL_REG2Resp message is received.
		 */
		virtual void	response(	Oscl::Driver::ST::LPS25H::Register::
									Resp::Api::SetCTRL_REG2Resp& msg
									) noexcept=0;

		/** This operation is invoked within the client thread
			whenever a GetCTRL_REG2Resp message is received.
		 */
		virtual void	response(	Oscl::Driver::ST::LPS25H::Register::
									Resp::Api::GetCTRL_REG2Resp& msg
									) noexcept=0;

		/** This operation is invoked within the client thread
			whenever a SetCTRL_REG1Resp message is received.
		 */
		virtual void	response(	Oscl::Driver::ST::LPS25H::Register::
									Resp::Api::SetCTRL_REG1Resp& msg
									) noexcept=0;

		/** This operation is invoked within the client thread
			whenever a GetCTRL_REG1Resp message is received.
		 */
		virtual void	response(	Oscl::Driver::ST::LPS25H::Register::
									Resp::Api::GetCTRL_REG1Resp& msg
									) noexcept=0;

		/** This operation is invoked within the client thread
			whenever a SetRES_CONFResp message is received.
		 */
		virtual void	response(	Oscl::Driver::ST::LPS25H::Register::
									Resp::Api::SetRES_CONFResp& msg
									) noexcept=0;

		/** This operation is invoked within the client thread
			whenever a GetRES_CONFResp message is received.
		 */
		virtual void	response(	Oscl::Driver::ST::LPS25H::Register::
									Resp::Api::GetRES_CONFResp& msg
									) noexcept=0;

		/** This operation is invoked within the client thread
			whenever a GetWHO_AM_IResp message is received.
		 */
		virtual void	response(	Oscl::Driver::ST::LPS25H::Register::
									Resp::Api::GetWHO_AM_IResp& msg
									) noexcept=0;

		/** This operation is invoked within the client thread
			whenever a SetREF_P_HResp message is received.
		 */
		virtual void	response(	Oscl::Driver::ST::LPS25H::Register::
									Resp::Api::SetREF_P_HResp& msg
									) noexcept=0;

		/** This operation is invoked within the client thread
			whenever a GetREF_P_HResp message is received.
		 */
		virtual void	response(	Oscl::Driver::ST::LPS25H::Register::
									Resp::Api::GetREF_P_HResp& msg
									) noexcept=0;

		/** This operation is invoked within the client thread
			whenever a SetREF_P_LResp message is received.
		 */
		virtual void	response(	Oscl::Driver::ST::LPS25H::Register::
									Resp::Api::SetREF_P_LResp& msg
									) noexcept=0;

		/** This operation is invoked within the client thread
			whenever a GetREF_P_LResp message is received.
		 */
		virtual void	response(	Oscl::Driver::ST::LPS25H::Register::
									Resp::Api::GetREF_P_LResp& msg
									) noexcept=0;

		/** This operation is invoked within the client thread
			whenever a SetREF_P_XLResp message is received.
		 */
		virtual void	response(	Oscl::Driver::ST::LPS25H::Register::
									Resp::Api::SetREF_P_XLResp& msg
									) noexcept=0;

		/** This operation is invoked within the client thread
			whenever a GetREF_P_XLResp message is received.
		 */
		virtual void	response(	Oscl::Driver::ST::LPS25H::Register::
									Resp::Api::GetREF_P_XLResp& msg
									) noexcept=0;

	};

}
}
}
}
}
}
#endif
