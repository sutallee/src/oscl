/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/


#ifndef _oscl_driver_st_lps25h_i2c_adapterh_
#define _oscl_driver_st_lps25h_i2c_adapterh_

#include "oscl/driver/st/lps25h/register/api.h"
#include "oscl/i2c/device/api.h"

/** */
namespace Oscl {

/** */
namespace Driver {

/** */
namespace ST {

/** */
namespace LPS25H {

/** */
namespace I2C {

/** */
class Adapter :
	public Oscl::Driver::ST::LPS25H::Register::Api
	{
	private:
		/** */
		Oscl::I2C::Device::Api&	_api;

	public:
		/** */
		Adapter(
			Oscl::I2C::Device::Api&	api
			) noexcept;

	private:
		/** */
		void	selectThenWriteReg(uint8_t regNum,uint8_t value) noexcept;

		/** */
		uint8_t	selectThenReadReg(uint8_t regNum) noexcept;

	private:
		/**	*/
		void	getTEMP_OUT(
					uint8_t	maxBufferLength,
					void*	buffer
					) noexcept;

		/**	*/
		void	getPRESS_OUT(
					uint8_t	maxBufferLength,
					void*	buffer
					) noexcept;

		/**	*/
		void	setRPDS_H(
					uint8_t	value
					) noexcept;

		/**	*/
		uint8_t	getRPDS_H() noexcept;

		/**
		 */
		void	setRPDS_L(
					uint8_t	value
					) noexcept;

		/**	*/
		uint8_t	getRPDS_L() noexcept;

		/**	*/
		void	setTHS_P_H(
					uint8_t	value
					) noexcept;

		/**	*/
		uint8_t	getTHS_P_H() noexcept;

		/**	*/
		void	setTHS_P_L(
					uint8_t	value
					) noexcept;

		/**	*/
		uint8_t	getTHS_P_L() noexcept;

		/**	*/
		uint8_t	getFIFO_STATUS() noexcept;

		/**	*/
		void	setFIFO_CTRL(
					uint8_t	value
					) noexcept;

		/**	*/
		uint8_t	getFIFO_CTRL() noexcept;

		/**	*/
		uint8_t	getTEMP_OUT_H() noexcept;

		/**	*/
		uint8_t	getTEMP_OUT_L() noexcept;

		/**	*/
		uint8_t	getPRESS_OUT_H() noexcept;

		/**	*/
		uint8_t	getPRESS_OUT_L() noexcept;

		/**	*/
		uint8_t	getPRESS_OUT_XL() noexcept;

		/**	*/
		uint8_t	getSTATUS_REG() noexcept;

		/**	*/
		void	setINT_SOURCE(
					uint8_t	value
					) noexcept;

		/**	*/
		uint8_t	getINT_SOURCE() noexcept;

		/**	*/
		void	setINT_CFG(
					uint8_t	value
					) noexcept;

		/**	*/
		uint8_t	getINT_CFG() noexcept;

		/**	*/
		void	setCTRL_REG4(
					uint8_t	value
					) noexcept;

		/**	*/
		uint8_t	getCTRL_REG4() noexcept;

		/**	*/
		void	setCTRL_REG3(
					uint8_t	value
					) noexcept;

		/**	*/
		uint8_t	getCTRL_REG3() noexcept;

		/**	*/
		void	setCTRL_REG2(
					uint8_t	value
					) noexcept;

		/**	*/
		uint8_t	getCTRL_REG2() noexcept;

		/**	*/
		void	setCTRL_REG1(
					uint8_t	value
					) noexcept;

		/**	*/
		uint8_t	getCTRL_REG1() noexcept;

		/**	*/
		void	setRES_CONF(
					uint8_t	value
					) noexcept;

		/**	*/
		uint8_t	getRES_CONF() noexcept;

		/**	*/
		uint8_t	getWHO_AM_I() noexcept;

		/**	*/
		void	setREF_P_H(
					uint8_t	value
					) noexcept;

		/**	*/
		uint8_t	getREF_P_H() noexcept;

		/**	*/
		void	setREF_P_L(
					uint8_t	value
					) noexcept;

		/**	*/
		uint8_t	getREF_P_L() noexcept;

		/**	*/
		void	setREF_P_XL(
					uint8_t	value
					) noexcept;

		/**	*/
		uint8_t	getREF_P_XL() noexcept;

	};

}
}
}
}
}

#endif
