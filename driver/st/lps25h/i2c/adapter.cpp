/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/


#include "adapter.h"
#include "oscl/hw/st/lps25h/reg.h"
#include "oscl/error/info.h"

using namespace Oscl::Driver::ST::LPS25H::I2C;

Adapter::Adapter(
			Oscl::I2C::Device::Api&	api
			) noexcept:
			_api(api)
		{
	}

void	Adapter::selectThenWriteReg(uint8_t regNum,uint8_t value) noexcept{
	uint8_t	buff[2];
	buff[0]	= regNum;
	buff[1]	= value;
	_api.write(&buff,2);
	}

uint8_t	Adapter::selectThenReadReg(uint8_t regNum) noexcept{

	_api.write(&regNum,1);

	uint8_t	value;
	_api.read(&value,1);

	return value;
	}

void	Adapter::getTEMP_OUT(
			uint8_t	maxBufferLength,
			void*	buffer
			) noexcept{

	uint8_t	regNum	= Oscl::HW::ST::LPS25H::TEMP_OUT;

	_api.write(&regNum,1);

	constexpr unsigned	len	= 6;

	if(maxBufferLength < len){
		Oscl::Error::Info::log(
			"%s: maxBufferLength(%u) < %u\n",
			__PRETTY_FUNCTION__,
			maxBufferLength,
			len
			);
		return;
		}

	_api.read(buffer,len);
	}

void	Adapter::getPRESS_OUT(
			uint8_t	maxBufferLength,
			void*	buffer
			) noexcept{
	uint8_t	regNum	= Oscl::HW::ST::LPS25H::PRESS_OUT;
	_api.write(&regNum,1);

	constexpr unsigned	len	= 3;

	if(maxBufferLength < len){
		Oscl::Error::Info::log(
			"%s: maxBufferLength(%u) < %u\n",
			__PRETTY_FUNCTION__,
			maxBufferLength,
			len
			);
		return;
		}

	_api.read(buffer,len);
	}

void	Adapter::setRPDS_H(
					uint8_t	value
					) noexcept{
	selectThenWriteReg(
		Oscl::HW::ST::LPS25H::RPDS_H,
		value
		);
	}

uint8_t	Adapter::getRPDS_H() noexcept{
	return selectThenReadReg(Oscl::HW::ST::LPS25H::RPDS_H);
	}

void	Adapter::setRPDS_L(
					uint8_t	value
					) noexcept{
	selectThenWriteReg(
		Oscl::HW::ST::LPS25H::RPDS_L,
		value
		);
	}

uint8_t	Adapter::getRPDS_L() noexcept{
	return selectThenReadReg(Oscl::HW::ST::LPS25H::RPDS_L);
	}

void	Adapter::setTHS_P_H(
					uint8_t	value
					) noexcept{
	selectThenWriteReg(
		Oscl::HW::ST::LPS25H::THS_P_H,
		value
		);
	}

uint8_t	Adapter::getTHS_P_H() noexcept{
	return selectThenReadReg(Oscl::HW::ST::LPS25H::THS_P_H);
	}

void	Adapter::setTHS_P_L(
					uint8_t	value
					) noexcept{
	selectThenWriteReg(
		Oscl::HW::ST::LPS25H::THS_P_L,
		value
		);
	}

uint8_t	Adapter::getTHS_P_L() noexcept{
	return selectThenReadReg(Oscl::HW::ST::LPS25H::THS_P_L);
	}

uint8_t	Adapter::getFIFO_STATUS() noexcept{
	return selectThenReadReg(Oscl::HW::ST::LPS25H::FIFO_STATUS);
	}

void	Adapter::setFIFO_CTRL(
					uint8_t	value
					) noexcept{
	selectThenWriteReg(
		Oscl::HW::ST::LPS25H::FIFO_CTRL,
		value
		);
	}

uint8_t	Adapter::getFIFO_CTRL() noexcept{
	return selectThenReadReg(Oscl::HW::ST::LPS25H::FIFO_CTRL);
	}

uint8_t	Adapter::getTEMP_OUT_H() noexcept{
	return selectThenReadReg(Oscl::HW::ST::LPS25H::TEMP_OUT_H);
	}

uint8_t	Adapter::getTEMP_OUT_L() noexcept{
	return selectThenReadReg(Oscl::HW::ST::LPS25H::TEMP_OUT_L);
	}

uint8_t	Adapter::getPRESS_OUT_H() noexcept{
	return selectThenReadReg(Oscl::HW::ST::LPS25H::PRESS_OUT_H);
	}

uint8_t	Adapter::getPRESS_OUT_L() noexcept{
	return selectThenReadReg(Oscl::HW::ST::LPS25H::PRESS_OUT_L);
	}

uint8_t	Adapter::getPRESS_OUT_XL() noexcept{
	return selectThenReadReg(Oscl::HW::ST::LPS25H::PRESS_OUT_XL);
	}

uint8_t	Adapter::getSTATUS_REG() noexcept{
	return selectThenReadReg(Oscl::HW::ST::LPS25H::STATUS_REG);
	}

void	Adapter::setINT_SOURCE(
					uint8_t	value
					) noexcept{
	selectThenWriteReg(
		Oscl::HW::ST::LPS25H::INT_SOURCE,
		value
		);
	}

uint8_t	Adapter::getINT_SOURCE() noexcept{
	return selectThenReadReg(Oscl::HW::ST::LPS25H::INT_SOURCE);
	}

void	Adapter::setINT_CFG(
					uint8_t	value
					) noexcept{
	selectThenWriteReg(
		Oscl::HW::ST::LPS25H::INT_CFG,
		value
		);
	}

uint8_t	Adapter::getINT_CFG() noexcept{
	return selectThenReadReg(Oscl::HW::ST::LPS25H::INT_CFG);
	}

void	Adapter::setCTRL_REG4(
					uint8_t	value
					) noexcept{
	selectThenWriteReg(
		Oscl::HW::ST::LPS25H::CTRL_REG4,
		value
		);
	}

uint8_t	Adapter::getCTRL_REG4() noexcept{
	return selectThenReadReg(Oscl::HW::ST::LPS25H::CTRL_REG4);
	}

void	Adapter::setCTRL_REG3(
					uint8_t	value
					) noexcept{
	selectThenWriteReg(
		Oscl::HW::ST::LPS25H::CTRL_REG3,
		value
		);
	}

uint8_t	Adapter::getCTRL_REG3() noexcept{
	return selectThenReadReg(Oscl::HW::ST::LPS25H::CTRL_REG3);
	}

void	Adapter::setCTRL_REG2(
					uint8_t	value
					) noexcept{
	selectThenWriteReg(
		Oscl::HW::ST::LPS25H::CTRL_REG2,
		value
		);
	}

uint8_t	Adapter::getCTRL_REG2() noexcept{
	return selectThenReadReg(Oscl::HW::ST::LPS25H::CTRL_REG2);
	}

void	Adapter::setCTRL_REG1(
					uint8_t	value
					) noexcept{
	selectThenWriteReg(
		Oscl::HW::ST::LPS25H::CTRL_REG1,
		value
		);
	}

uint8_t	Adapter::getCTRL_REG1() noexcept{
	return selectThenReadReg(Oscl::HW::ST::LPS25H::CTRL_REG1);
	}

void	Adapter::setRES_CONF(
					uint8_t	value
					) noexcept{
	selectThenWriteReg(
		Oscl::HW::ST::LPS25H::RES_CONF,
		value
		);
	}

uint8_t	Adapter::getRES_CONF() noexcept{
	return selectThenReadReg(Oscl::HW::ST::LPS25H::RES_CONF);
	}

uint8_t	Adapter::getWHO_AM_I() noexcept{
	return selectThenReadReg(Oscl::HW::ST::LPS25H::WHO_AM_I);
	}

void	Adapter::setREF_P_H(
					uint8_t	value
					) noexcept{
	selectThenWriteReg(
		Oscl::HW::ST::LPS25H::REF_P_H,
		value
		);
	}

uint8_t	Adapter::getREF_P_H() noexcept{
	return selectThenReadReg(Oscl::HW::ST::LPS25H::REF_P_H);
	}

void	Adapter::setREF_P_L(
					uint8_t	value
					) noexcept{
	selectThenWriteReg(
		Oscl::HW::ST::LPS25H::REF_P_L,
		value
		);
	}

uint8_t	Adapter::getREF_P_L() noexcept{
	return selectThenReadReg(Oscl::HW::ST::LPS25H::REF_P_L);
	}

void	Adapter::setREF_P_XL(
					uint8_t	value
					) noexcept{
	selectThenWriteReg(
		Oscl::HW::ST::LPS25H::REF_P_XL,
		value
		);
	}

uint8_t	Adapter::getREF_P_XL() noexcept{
	return selectThenReadReg(Oscl::HW::ST::LPS25H::REF_P_XL);
	}

