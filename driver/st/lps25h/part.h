/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/


#ifndef _oscl_driver_st_lps25h_parth_
#define _oscl_driver_st_lps25h_parth_

#include "oscl/driver/st/lps25h/register/api.h"
#include "oscl/mt/itc/postmsgapi.h"
#include "oscl/event/observer/respcomp.h"
#include "oscl/double/output/part.h"

/** */
namespace Oscl {

/** */
namespace Driver {

/** */
namespace ST {

/** */
namespace LPS25H {

/** */
class Part {
	private:
		/** */
		Oscl::Mt::Itc::PostMsgApi&						_papi;

		/** */
		Oscl::Driver::ST::LPS25H::Register::Api&		_api;

		/** */
		Oscl::Event::Observer::Req::Api::SAP&			_tickSAP;

	private:
		/** */
		Oscl::Event::Observer::Resp::Composer<Part>			_tickRespComposer;

		/** */
		Oscl::Event::Observer::Req::Api::OccurrencePayload	_tickPayload;

		/** */
		Oscl::Event::Observer::Resp::Api::OccurrenceResp	_tickResp;

	private:
		/** */
		Oscl::Double::Output::Part							_output;

	public:
		/** */
		Part(
			Oscl::Mt::Itc::PostMsgApi&	papi,
			Oscl::Driver::ST::
			LPS25H::Register::Api&		agApi,
			Oscl::Event::Observer::
			Req::Api::SAP&				tickSAP
			) noexcept;

		/** */
		void	start() noexcept;

		/** */
		Oscl::Double::Observer::Req::Api::SAP&	getPressureSAP() noexcept;

	private:
		/** RETURN: true on failure. */
		bool	checkID() noexcept;

		/** RETURN: true on failure. */
		bool	boot() noexcept;

		/** RETURN: true on failure. */
		bool	configureREF_P() noexcept;

		/** RETURN: true on failure. */
		bool	configureRES_CONF() noexcept;

		/** RETURN: true on failure. */
		bool	configureCTRL_REG1() noexcept;

		/** RETURN: true on failure. */
		bool	configureCTRL_REG2() noexcept;

		/** RETURN: true on failure. */
		bool	configureCTRL_REG3() noexcept;

		/** RETURN: true on failure. */
		bool	configureCTRL_REG4() noexcept;

		/** RETURN: true on failure. */
		bool	configureFIFO_CTRL() noexcept;

		/** RETURN: true on failure. */
		bool	readPressure() noexcept;

	private: // Oscl::Event::Observer::Resp::Composer _tickRespComposer
		/**	*/
		void	tickResponse(Oscl::Event::Observer::Resp::Api::OccurrenceResp& msg) noexcept;

	};

}
}
}
}

#endif
