/*
   Copyright (C) 2024 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_driver_st_stm32_h7a3b3b0_pflash_bank_control_apih_
#define _oscl_driver_st_stm32_h7a3b3b0_pflash_bank_control_apih_

#include <stdint.h>

/** */
namespace Oscl {

/** */
namespace Driver {

/** */
namespace St {

/** */
namespace Stm32 {

/** */
namespace H7A3B3B0 {

/** */
namespace pFLASH {

/** */
namespace Bank {

/** */
namespace Control {

/**	This class defines the operations that can be performed
	procedurally by the client.
 */
class Api {
	public:
		/** Make the compiler happy.
		 */
		virtual ~Api(){};

		/**	Erase an 8K sector of flash.
		 */
		virtual bool	eraseSector(	
							const uint32_t	sector
							) noexcept=0;

		/**	Program four 32-bit words of data to the FLASH bank at
			the specified word address.
		 */
		virtual bool	programWord(	
							void*	wordAddress,
							const uint32_t*	wordData
							) noexcept=0;

	};

}
}
}
}
}
}
}
}
#endif
