/*
   Copyright (C) 2024 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_driver_st_stm32_h7a3b3b0_pflash_bank_control_itc_reqapih_
#define _oscl_driver_st_stm32_h7a3b3b0_pflash_bank_control_itc_reqapih_

#include "oscl/mt/itc/mbox/srvreq.h"
#include "oscl/mt/itc/postmsgapi.h"
#include "oscl/mt/itc/mbox/sap.h"
#include <stdint.h>

/** */
namespace Oscl {

/** */
namespace Driver {

/** */
namespace St {

/** */
namespace Stm32 {

/** */
namespace H7A3B3B0 {

/** */
namespace pFLASH {

/** */
namespace Bank {

/** */
namespace Control {

/** */
namespace Req {

/**	This class describes an ITC server interface. The
	interface includes the definition of messages and their
	associated payloads, as well as an interface that is to be
	implemented by a server that supports this ITC interface.
 */
/**	
 */
class Api {
	public:
		/** This class contains the data that is transfered
			between the client and server within the EraseSectorReq
			ITC message.
		 */
		class EraseSectorPayload {
			public:
				/**	The sector to be erased. This should be a number in the
					range of 0 to 127 for this device.
				 */
				const uint32_t	_sector;

				/**	
				 */
				bool	_failed;

			public:
				/** The EraseSectorPayload constructor. */
				EraseSectorPayload(

					const uint32_t	sector
					) noexcept;

			};

		/**	This message is a request to erase an 8KB Flash sector.
		 */
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::Driver::St::Stm32::H7A3B3B0::pFLASH::Bank::Control::Req::Api,
					EraseSectorPayload
					>		EraseSectorReq;

	public:
		/** This class contains the data that is transfered
			between the client and server within the ProgramWordReq
			ITC message.
		 */
		class ProgramWordPayload {
			public:
				/**	The address must be aligned on a 16-byte boundary.
				 */
				void*	_wordAddress;

				/**	This is a pointer to the first of four (4) words that
					will be written to the wordAddress.
				 */
				const uint32_t*	_wordData;

				/**	
				 */
				bool	_failed;

			public:
				/** The ProgramWordPayload constructor. */
				ProgramWordPayload(

					void*	wordAddress,

					const uint32_t*	wordData
					) noexcept;

			};

		/**	This message is a request to program a 128-bit (16-byte)
			FLASH word.
		 */
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::Driver::St::Stm32::H7A3B3B0::pFLASH::Bank::Control::Req::Api,
					ProgramWordPayload
					>		ProgramWordReq;

	public:
		/** This type defines a SAP (Service Access Point) for this
			ITC server interface. A SAP holds server address information
			that is required by clients to send messages to a server.
		 */
		typedef Oscl::Mt::Itc::SAP<	Oscl::Driver::St::Stm32::H7A3B3B0::pFLASH::Bank::Control::
							Req::Api
							>			SAP;

	public:
		/** This type defines a SAP (Service Access Point) for this
			ITC server interface. A SAP holds server address information
			that is required by clients to send messages to a server.
		 */
		typedef Oscl::Mt::Itc::ConcreteSAP<	Oscl::Driver::St::Stm32::H7A3B3B0::pFLASH::Bank::Control::
							Req::Api
							>			ConcreteSAP;

	public:
		/** Make the compiler happy. */
		virtual ~Api(){}

		/** This operation is invoked within the server thread
			whenever a EraseSectorReq is received.
		 */
		virtual void	request(	Oscl::Driver::St::Stm32::H7A3B3B0::pFLASH::Bank::Control::
									Req::Api::EraseSectorReq& msg
									) noexcept=0;

		/** This operation is invoked within the server thread
			whenever a ProgramWordReq is received.
		 */
		virtual void	request(	Oscl::Driver::St::Stm32::H7A3B3B0::pFLASH::Bank::Control::
									Req::Api::ProgramWordReq& msg
									) noexcept=0;

	};

}
}
}
}
}
}
}
}
}
#endif
