/*
   Copyright (C) 2024 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_driver_st_stm32_h7a3b3b0_pflash_bank_control_itc_respapih_
#define _oscl_driver_st_stm32_h7a3b3b0_pflash_bank_control_itc_respapih_

#include "reqapi.h"
#include "oscl/mt/itc/mbox/clirsp.h"

/** */
namespace Oscl {

/** */
namespace Driver {

/** */
namespace St {

/** */
namespace Stm32 {

/** */
namespace H7A3B3B0 {

/** */
namespace pFLASH {

/** */
namespace Bank {

/** */
namespace Control {

/** */
namespace Resp {

/**	This class describes an ITC client interface. The
	interface includes the definition of client response messages
	based on their corresponding server request messages and
	associated payloads, as well as an interface that is to be
	implemented by a client that uses the corresponding server
	asynchronously.
 */
/**	
 */
class Api {
	public:
		/**	This describes a client response message that corresponds
			to the EraseSectorReq message described in the "reqapi.h"
			header file. This EraseSectorResp response message actually
			contains a EraseSectorReq request message.
		 */
		typedef Oscl::Mt::Itc::CliResponse<
					Oscl::Driver::St::Stm32::H7A3B3B0::pFLASH::Bank::Control::Req::Api,
					Api,
					Oscl::Driver::St::Stm32::H7A3B3B0::pFLASH::Bank::Control::
					Req::Api::EraseSectorPayload
					>		EraseSectorResp;

	public:
		/**	This describes a client response message that corresponds
			to the ProgramWordReq message described in the "reqapi.h"
			header file. This ProgramWordResp response message actually
			contains a ProgramWordReq request message.
		 */
		typedef Oscl::Mt::Itc::CliResponse<
					Oscl::Driver::St::Stm32::H7A3B3B0::pFLASH::Bank::Control::Req::Api,
					Api,
					Oscl::Driver::St::Stm32::H7A3B3B0::pFLASH::Bank::Control::
					Req::Api::ProgramWordPayload
					>		ProgramWordResp;

	public:
		/** Make the compiler happy. */
		virtual ~Api(){}

		/** This operation is invoked within the client thread
			whenever a EraseSectorResp message is received.
		 */
		virtual void	response(	Oscl::Driver::St::Stm32::H7A3B3B0::pFLASH::Bank::Control::
									Resp::Api::EraseSectorResp& msg
									) noexcept=0;

		/** This operation is invoked within the client thread
			whenever a ProgramWordResp message is received.
		 */
		virtual void	response(	Oscl::Driver::St::Stm32::H7A3B3B0::pFLASH::Bank::Control::
									Resp::Api::ProgramWordResp& msg
									) noexcept=0;

	};

}
}
}
}
}
}
}
}
}
#endif
