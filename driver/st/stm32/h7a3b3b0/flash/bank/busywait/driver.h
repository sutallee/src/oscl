/*
   Copyright (C) 2024 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_driver_st_stm32_h7a3b3b0_flash_bank_busywait_driverh_
#define _oscl_driver_st_stm32_h7a3b3b0_flash_bank_busywait_driverh_

#include "oscl/hw/st/stm32/h7a3b3b0/flash/map.h"
#include "oscl/driver/st/stm32/h7a3b3b0/flash/bank/control/api.h"

/** */
namespace Oscl {
/** */
namespace Driver {
/** */
namespace St {
/** */
namespace Stm32 {
/** */
namespace H7A3B3B0 {
/** */
namespace pFLASH {
/** */
namespace Bank {
/** */
namespace BusyWait {

/** A busy-wait driver for erasing and programming
	the FLASH on a STM32H7 A3,B3,B0 device.
 */
class Driver : Oscl::Driver::St::Stm32::H7A3B3B0::pFLASH::Bank::Control::Api {
	private:
		/** */
		Oscl::St::Stm32::H7A3B3B0::pFLASH::Bank&	_bank;

	public:
		/** */
		Driver(
			Oscl::St::Stm32::H7A3B3B0::pFLASH::Bank&	map
			) noexcept;

		/** */
		virtual ~Driver(){}

		/** */
		void	initialize() noexcept;

	private:
		/** */
		void	clearAllPendingErrors() noexcept;

		/** */
		void	unlockBankIfRequired() noexcept;

		/** */
		void	startSectorErase( uint32_t sector ) noexcept;

		/** */
		void	startProgrammingSequence() noexcept;

	public: // Oscl::Driver::St::Stm32::H7A3B3B0::pFLASH::Bank::Control::Api
		/**	Erase an 8K sector of flash.
		 */
		bool	eraseSector(	
					const uint32_t	sector
					) noexcept override;

		/**	Program four 32-bit words of data to the FLASH bank at
			the specified word address.
		 */
		bool	programWord(	
					void*	wordAddress,
					const uint32_t*	wordData
					) noexcept override;
	};

}
}
}
}
}
}
}
}

#endif
