/*
   Copyright (C) 2024 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "driver.h"
#include "oscl/driver/arm/cortex/m7/cache.h"

static constexpr unsigned	flashWordSizeInBytes	= 4 * sizeof( uint32_t );
static constexpr unsigned	nFlashSectorsInBank	= 128;

using namespace Oscl::Driver::St::Stm32::H7A3B3B0::pFLASH::Bank::BusyWait;

Driver::Driver(
	Oscl::St::Stm32::H7A3B3B0::pFLASH::Bank&	bank
	) noexcept:
		_bank(bank)
		{
	initialize();
	}

void	Driver::clearAllPendingErrors() noexcept {
	using namespace Oscl::St::Stm32::H7A3B3B0::pFLASH;

	// Clear pending errors
	_bank.ccr	=
			0
		|	rCCR::fCLR_EOP::ValueMask_clear
		|	rCCR::fCLR_WRPERR::ValueMask_clear
		|	rCCR::fCLR_PGSERR::ValueMask_clear
		|	rCCR::fCLR_STRBERR::ValueMask_clear
		|	rCCR::fCLR_INCERR::ValueMask_clear
		|	rCCR::fCLR_RDPERR::ValueMask_clear
		|	rCCR::fCLR_RDSERR::ValueMask_clear
		|	rCCR::fCLR_SNECCERR::ValueMask_clear
		|	rCCR::fCLR_DBECCERR::ValueMask_clear
		|	rCCR::fCLR_CRCEND::ValueMask_clear
		|	rCCR::fCLR_CRCRDERR::ValueMask_clear
		;
	}

void Driver::initialize() noexcept{

	clearAllPendingErrors();

	unlockBankIfRequired();
	}

void	Driver::startSectorErase( uint32_t sector ) noexcept {
	using namespace Oscl::St::Stm32::H7A3B3B0::pFLASH;

	auto
	cr	= _bank.cr;

	cr	&= ~(
			0
		|	rCR::fSER::FieldMask
		|	rCR::fSSN::FieldMask
		|	rCR::fSTART::FieldMask
		);

	cr	|=
			0
		|	rCR::fSER::ValueMask_erase
		|	(sector << rCR::fSSN::Lsb)
		|	rCR::fSTART::ValueMask_start
		;

	_bank.cr	= cr;
	}

bool	Driver::eraseSector(	
			const uint32_t	sector
			) noexcept {
	using namespace Oscl::St::Stm32::H7A3B3B0::pFLASH;

	if( sector > (nFlashSectorsInBank-1) ) {
		return true;
		}

	startSectorErase( sector );

	// Wait for QW High
	while( (_bank.sr & rSR::fQW::FieldMask) != rSR::fQW::ValueMask_pending );

	/*	Wait for the QW1/2 bit to be cleared in the corresponding FLASH_SR1/2 register.
	 */
	while( (_bank.sr & rSR::fQW::FieldMask) == rSR::fQW::ValueMask_pending );

	return true;
	}

void	Driver::startProgrammingSequence() noexcept {
	using namespace Oscl::St::Stm32::H7A3B3B0::pFLASH;

	auto
	cr	= _bank.cr;
	cr	&= ~(
			0
		|	rCR::fPG::FieldMask
		);

	cr	|=
			0
		|	rCR::fPG::ValueMask_enable
		;
	_bank.cr	= cr;
	}

bool	Driver::programWord(	
			void*			inWordAddress,
			const uint32_t*	inWordData
			) noexcept {
	using namespace Oscl::St::Stm32::H7A3B3B0::pFLASH;

	startProgrammingSequence();

	// Write one flash-word corresponding to 16-byte data starting at 16-byte aligned address.

	uint32_t*
	wordAddress	= (uint32_t*)inWordAddress;

	const uint32_t*
	wordData	= inWordData;

	for( unsigned i=0; i < 4; ++i ) {
		wordAddress[i] = wordData[i];
		}

	/*	The STM32H7B3 has a cache-line size of 256 bits (32 bytes).
		Since the FLASH word size is 128 bits (16 bytes), I'm
		concerned that this may cause the FLASH to see 32 bytes
		( 8 words ) written instead of the desired 16 bytes ( 4 words).
	 */

	Oscl::Arm::Cortex::M7::cleanDataCacheAddressRange(
		wordAddress,
		flashWordSizeInBytes
		);

	/*	Wait for the QW bit to be set in the corresponding FLASH_SR register.
	 */
	while( (_bank.sr & rSR::fQW::FieldMask) != rSR::fQW::ValueMask_pending );

	/*	Wait for the QW bit to be cleared in the corresponding FLASH_SR register.
	 */
	while( (_bank.sr & rSR::fQW::FieldMask) == rSR::fQW::ValueMask_pending );

	return false;
	}

void	Driver::unlockBankIfRequired() noexcept{
	using namespace Oscl::St::Stm32::H7A3B3B0::pFLASH;

	if( _bank.cr & rCR::fLOCK::FieldMask == rCR::fLOCK::ValueMask_locked ) {
		_bank.keyr = rKEYR::fKEYR::ValueMask_firstKey;
		_bank.keyr = rKEYR::fKEYR::ValueMask_secondKey;
		}
	}

