/*
   Copyright (C) 2024 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_driver_st_stm32_h7a3b3b0_flash_bank_interrupt_sharedh_
#define _oscl_driver_st_stm32_h7a3b3b0_flash_bank_interrupt_sharedh_

#include "oscl/driver/shared/symbiont.h"
#include "oscl/hw/st/stm32/h7a3b3b0/flash/map.h"
#include "oscl/driver/st/stm32/h7a3b3b0/flash/bank/control/reqcomp.h"
#include "oscl/driver/st/stm32/h7a3b3b0/flash/bank/control/sync.h"

/** */
namespace Oscl {
/** */
namespace Driver {
/** */
namespace St {
/** */
namespace Stm32 {
/** */
namespace H7A3B3B0 {
/** */
namespace pFLASH {
/** */
namespace Bank {
/** */
namespace Interrupt {

/** An interrupt driven driver for erasing and programming
	the FLASH on a STM32H7 A3,B3,B0 device, allowing erase
	and program operations to proceed without blocking other
	threads (busy waiting).

	From the STM32H7B3xl 6.3.12 device specification, FLASH parameters include:

	- tPROG		Word program time (user area 128 bits):	20us max.
	- tPROG		Word program time (OTP area 16 bits):	20us max.
	- tERASE8KB	Sector erase time:						2.2ms max.
	- tME		Single-bank erase time:					10ms max.
	- tME		Dual-bank erase time:					10ms max.
 */
class Shared :	public Oscl::Driver::Shared::Symbiont::Api {
	private:
		/** */
		Oscl::St::Stm32::H7A3B3B0::pFLASH::Bank&	_bank;

		/** */
		const unsigned						_interruptNumber;

	private:
		/** */
		Oscl::Driver::St::Stm32::H7A3B3B0::pFLASH::Bank::Control::Req::Composer< Shared >	_reqComposer;

		/** */
		Oscl::Driver::St::Stm32::H7A3B3B0::pFLASH::Bank::Control::Sync	_flashSync;

		/** */
		Oscl::Queue< Oscl::Mt::Itc::SrvMsg >		_pendingReqQ;

		/** */
		Oscl::Driver::St::Stm32::H7A3B3B0::pFLASH::Bank::Control::Req::Api::EraseSectorReq*	_currentEraseReq;

		/** */
		Oscl::Driver::St::Stm32::H7A3B3B0::pFLASH::Bank::Control::Req::Api::ProgramWordReq*	_currentProgramReq;

	private:
		/** */
		unsigned	_eopCounter;

		/** */
		unsigned	_wrperrCounter;

		/** */
		unsigned	_pgserrCounter;

		/** */
		unsigned	_strberrCounter;

		/** */
		unsigned	_incerrCounter;

		/** */
		unsigned	_rdperrCounter;

		/** */
		unsigned	_rdserrCounter;

		/** */
		unsigned	_sneccerrCounter;

		/** */
		unsigned	_dbeccerrCounter;

		/** */
		unsigned	_crcendCounter;

		/** */
		unsigned	_crcrderrCounter;

	public:
		/** */
		Shared(
			Oscl::Mt::Itc::PostMsgApi&					papi,
			Oscl::St::Stm32::H7A3B3B0::pFLASH::Bank&	map,
			unsigned									interruptNumber
			) noexcept;

		/** */
		virtual ~Shared(){}

		/** */
		Oscl::Driver::St::Stm32::H7A3B3B0::pFLASH::Bank::Control::Req::Api::SAP&	getControlSAP() noexcept;
		
		/** */
		Oscl::Driver::St::Stm32::H7A3B3B0::pFLASH::Bank::Control::Api&	getSyncApi() noexcept;

	private:	// Oscl::Driver::Shared::Symbiont::Api
		/** */
		void	initialize() noexcept;

		/** */
		void	process() noexcept;

	private:
		/** */
		void	clearAllPendingErrors() noexcept;

		/** */
		void	enableInterrupts() noexcept;

		/** */
		void	unlockBankIfRequired() noexcept;

		/** */
		bool	requestExecuting() const noexcept;

		/** */
		void	returnCurrentRequest( bool failed ) noexcept;

		/** */
		void	startSectorErase( uint32_t sector ) noexcept;

		/** */
		void	startProgrammingSequence() noexcept;

	private: // Oscl::Driver::St::Stm32::H7A3B3B0::pFLASH::Bank::Control::Req::Composer

		/**	This message is a request to erase an 8KB Flash sector.
		 */
		void	eraseRequest(Oscl::Driver::St::Stm32::H7A3B3B0::pFLASH::Bank::Control::Req::Api::EraseSectorReq& msg) noexcept;

		/**	This message is a request to program a 128-bit (16-byte)
			FLASH word.
		 */
		void	programWordRequest(Oscl::Driver::St::Stm32::H7A3B3B0::pFLASH::Bank::Control::Req::Api::ProgramWordReq& msg) noexcept;
	};

}
}
}
}
}
}
}
}

#endif
