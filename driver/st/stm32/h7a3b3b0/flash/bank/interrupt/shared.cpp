/*
   Copyright (C) 2024 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "oscl/error/info.h"
#include "shared.h"
#include "oscl/hw/arm/cortexm7/nvic.h"
#include "oscl/driver/arm/cortex/m7/cache.h"

using namespace Oscl::Driver::St::Stm32::H7A3B3B0::pFLASH::Bank::Interrupt;

Shared::Shared(
	Oscl::Mt::Itc::PostMsgApi&					papi,
	Oscl::St::Stm32::H7A3B3B0::pFLASH::Bank&	bank,
	unsigned									interruptNumber
	) noexcept:
		_bank(bank),
		_interruptNumber(interruptNumber),
		_reqComposer(
			*this,
			&Shared::eraseRequest,
			&Shared::programWordRequest
			),
		_flashSync(
			papi,
			_reqComposer
			),
		_currentEraseReq( nullptr ),
		_currentProgramReq( nullptr ),
		_eopCounter(0),
		_wrperrCounter(0),
		_pgserrCounter(0),
		_strberrCounter(0),
		_incerrCounter(0),
		_rdperrCounter(0),
		_rdserrCounter(0),
		_sneccerrCounter(0),
		_dbeccerrCounter(0),
		_crcendCounter(0),
		_crcrderrCounter(0)
		{
	}

Oscl::Driver::St::Stm32::H7A3B3B0::pFLASH::Bank::Control::Req::Api::SAP&	Shared::getControlSAP() noexcept {
	return _flashSync.getSAP();
	}

Oscl::Driver::St::Stm32::H7A3B3B0::pFLASH::Bank::Control::Api&	Shared::getSyncApi() noexcept {
	return _flashSync;
	}

void	Shared::clearAllPendingErrors() noexcept {
	using namespace Oscl::St::Stm32::H7A3B3B0::pFLASH;

	// Clear pending errors
	_bank.ccr	=
			0
		|	rCCR::fCLR_EOP::ValueMask_clear
		|	rCCR::fCLR_WRPERR::ValueMask_clear
		|	rCCR::fCLR_PGSERR::ValueMask_clear
		|	rCCR::fCLR_STRBERR::ValueMask_clear
		|	rCCR::fCLR_INCERR::ValueMask_clear
		|	rCCR::fCLR_RDPERR::ValueMask_clear
		|	rCCR::fCLR_RDSERR::ValueMask_clear
		|	rCCR::fCLR_SNECCERR::ValueMask_clear
		|	rCCR::fCLR_DBECCERR::ValueMask_clear
		|	rCCR::fCLR_CRCEND::ValueMask_clear
		|	rCCR::fCLR_CRCRDERR::ValueMask_clear
		;
	}

void	Shared::enableInterrupts() noexcept {
	using namespace Oscl::St::Stm32::H7A3B3B0::pFLASH;

	auto
	cr	= _bank.cr;

	cr	&= ~(
			0
		|	rCR::fEOPIE::FieldMask
		|	rCR::fWRPERRIE::FieldMask
		|	rCR::fPGSERRIE::FieldMask
		|	rCR::fSTRBERRIE::FieldMask
		|	rCR::fINCERRIE::FieldMask
		|	rCR::fRDPERRIE::FieldMask
		|	rCR::fRDSERRIE::FieldMask
		|	rCR::fSNECCERRIE::FieldMask
		|	rCR::fDBECCERRIE::FieldMask
		|	rCR::fCRCENDIE::FieldMask
		|	rCR::fCRCRDERRIE::FieldMask
		);

	cr	|=
			0
		|	rCR::fEOPIE::ValueMask_enable
		|	rCR::fWRPERRIE::ValueMask_enable
		|	rCR::fPGSERRIE::ValueMask_enable
		|	rCR::fSTRBERRIE::ValueMask_enable
		|	rCR::fINCERRIE::ValueMask_enable
		|	rCR::fRDPERRIE::ValueMask_enable
		|	rCR::fRDSERRIE::ValueMask_enable
		|	rCR::fSNECCERRIE::ValueMask_enable
		|	rCR::fDBECCERRIE::ValueMask_enable
		|	rCR::fCRCENDIE::ValueMask_enable
		|	rCR::fCRCRDERRIE::ValueMask_enable
		;

	_bank.cr	= cr;
	}

void Shared::initialize() noexcept{

	clearAllPendingErrors();

	unlockBankIfRequired();

	enableInterrupts();
	}

static inline void	incrementOrClamp( unsigned& counter ) {
	static constexpr unsigned	maxCounter = ~0;

	if( counter < maxCounter ) {
		++counter;
		}
	}

void	Shared::process() noexcept{
	using namespace Oscl::St::Stm32::H7A3B3B0::pFLASH;

	uint32_t	ccr	= 0;

	auto
	sr	= _bank.sr;

	auto
	interruptStatus	= sr;

	interruptStatus		&=
			0
		|	rSR::fEOP::FieldMask
		|	rSR::fWRPERR::FieldMask
		|	rSR::fPGSERR::FieldMask
		|	rSR::fSTRBERR::FieldMask
		|	rSR::fINCERR::FieldMask
		|	rSR::fRDPERR::FieldMask
		|	rSR::fRDSERR::FieldMask
		|	rSR::fSNECCERR::FieldMask
		|	rSR::fDBECCERR::FieldMask
		|	rSR::fCRCEND::FieldMask
		|	rSR::fCRCRDERR::FieldMask
		;

	if( ! interruptStatus ) {
		Oscl::Arm::CortexM7::pNVIC::enableInterrupt( _interruptNumber );
		return;
		}

	auto
	errorStatus	= interruptStatus;

	errorStatus &= ~(rSR::fEOP::FieldMask);

	if( errorStatus ) {
		Oscl::Error::Info::log(
			"%s: errorStatus 0x%8.8X\n",
			__PRETTY_FUNCTION__,
			errorStatus
			);
		}

	if( (sr & rSR::fEOP::FieldMask) == rSR::fEOP::ValueMask_completed ) {
		ccr	|= rCCR::fCLR_EOP::ValueMask_clear;
		incrementOrClamp( _eopCounter );
		}
	if( (sr & rSR::fWRPERR::FieldMask) == rSR::fWRPERR::ValueMask_error ) {
		ccr	|= rCCR::fCLR_WRPERR::ValueMask_clear;
		incrementOrClamp( _wrperrCounter );
		}
	if( (sr & rSR::fPGSERR::FieldMask) == rSR::fPGSERR::ValueMask_error ) {
		ccr	|= rCCR::fCLR_PGSERR::ValueMask_clear;
		incrementOrClamp( _pgserrCounter );
		}
	if( (sr & rSR::fSTRBERR::FieldMask) == rSR::fSTRBERR::ValueMask_error ) {
		ccr	|= rCCR::fCLR_STRBERR::ValueMask_clear;
		incrementOrClamp( _strberrCounter );
		}
	if( (sr & rSR::fINCERR::FieldMask) == rSR::fINCERR::ValueMask_error ) {
		ccr	|= rCCR::fCLR_INCERR::ValueMask_clear;
		incrementOrClamp( _incerrCounter );
		}
	if( (sr & rSR::fRDPERR::FieldMask) == rSR::fRDPERR::ValueMask_error ) {
		ccr	|= rCCR::fCLR_RDPERR::ValueMask_clear;
		incrementOrClamp( _rdperrCounter );
		}
	if( (sr & rSR::fRDSERR::FieldMask) == rSR::fRDSERR::ValueMask_error ) {
		ccr	|= rCCR::fCLR_RDSERR::ValueMask_clear;
		incrementOrClamp( _rdserrCounter );
		}
	if( (sr & rSR::fSNECCERR::FieldMask) == rSR::fSNECCERR::ValueMask_error ) {
		ccr	|= rCCR::fCLR_SNECCERR::ValueMask_clear;
		incrementOrClamp( _sneccerrCounter );
		}
	if( (sr & rSR::fDBECCERR::FieldMask) == rSR::fDBECCERR::ValueMask_error ) {
		ccr	|= rCCR::fCLR_DBECCERR::ValueMask_clear;
		incrementOrClamp( _dbeccerrCounter );
		}
	if( (sr & rSR::fCRCEND::FieldMask) == rSR::fCRCEND::ValueMask_completed ) {
		ccr	|= rCCR::fCLR_CRCEND::ValueMask_clear;
		incrementOrClamp( _crcendCounter );
		}
	if( (sr & rSR::fCRCRDERR::FieldMask) == rSR::fCRCRDERR::ValueMask_error ) {
		ccr	|= rCCR::fCLR_CRCRDERR::ValueMask_clear;
		incrementOrClamp( _crcrderrCounter );
		}

	_bank.ccr	= ccr;

	Oscl::Arm::CortexM7::pNVIC::enableInterrupt( _interruptNumber );

	if( (sr & rSR::fEOP::FieldMask) == rSR::fEOP::ValueMask_completed ) {
		returnCurrentRequest( errorStatus?true:false );
		}
	}

void	Shared::startSectorErase( uint32_t sector ) noexcept {
	using namespace Oscl::St::Stm32::H7A3B3B0::pFLASH;

	auto
	cr	= _bank.cr;

	cr	&= ~(
			0
		|	rCR::fSER::FieldMask
		|	rCR::fSSN::FieldMask
		|	rCR::fSTART::FieldMask
		);

	cr	|=
			0
		|	rCR::fSER::ValueMask_erase
		|	(sector << rCR::fSSN::Lsb)
		|	rCR::fSTART::ValueMask_start
		;

	_bank.cr	= cr;
	}

void	Shared::eraseRequest(Oscl::Driver::St::Stm32::H7A3B3B0::pFLASH::Bank::Control::Req::Api::EraseSectorReq& msg) noexcept {

	if( requestExecuting() ) {
		_pendingReqQ.put( &msg );
		return;
		}

	_currentEraseReq	= &msg;

	auto&
	payload	= msg.getPayload();

	static constexpr unsigned	nFlashSectorsInBank	= 128;

	if( payload._sector > (nFlashSectorsInBank-1) ) {
		returnCurrentRequest( true );
		return;
		}

	startSectorErase( payload._sector );
	}

void	Shared::startProgrammingSequence() noexcept {
	using namespace Oscl::St::Stm32::H7A3B3B0::pFLASH;

	auto
	cr	= _bank.cr;
	cr	&= ~(
			0
		|	rCR::fPG::FieldMask
		);

	cr	|=
			0
		|	rCR::fPG::ValueMask_enable
		;
	_bank.cr	= cr;
	}

void	Shared::programWordRequest(Oscl::Driver::St::Stm32::H7A3B3B0::pFLASH::Bank::Control::Req::Api::ProgramWordReq& msg) noexcept {
	using namespace Oscl::St::Stm32::H7A3B3B0::pFLASH;

	if( requestExecuting() ) {
		_pendingReqQ.put( &msg );
		return;
		}

	_currentProgramReq	= &msg;

	auto&
	payload	= msg.getPayload();

	startProgrammingSequence();

	// Write one flash-word corresponding to 16-byte data starting at 16-byte aligned address.

	uint32_t*
	wordAddress	= (uint32_t*)payload._wordAddress;

	const uint32_t*
	wordData	= payload._wordData;

	for( unsigned i=0; i < 4; ++i ) {
		wordAddress[i] = wordData[i];
		}

	/*	The STM32H7B3 has a cache-line size of 256 bits (32 bytes).
		Since the FLASH word size is 128 bits (16 bytes), I'm
		concerned that this may cause the FLASH to see 32 bytes
		( 8 words ) written instead of the desired 16 bytes ( 4 words).
	 */

	static constexpr unsigned	flashWordSizeInBytes	= 4 * sizeof( uint32_t );

	Oscl::Arm::Cortex::M7::cleanDataCacheAddressRange(
		wordAddress,
		flashWordSizeInBytes
		);
	}

void	Shared::unlockBankIfRequired() noexcept{
	using namespace Oscl::St::Stm32::H7A3B3B0::pFLASH;

	if( _bank.cr & rCR::fLOCK::FieldMask == rCR::fLOCK::ValueMask_locked ) {
		_bank.keyr = rKEYR::fKEYR::ValueMask_firstKey;
		_bank.keyr = rKEYR::fKEYR::ValueMask_secondKey;
		}
	}

bool	Shared::requestExecuting() const noexcept {
	return ( _currentEraseReq || _currentProgramReq );
	}

void	Shared::returnCurrentRequest( bool failed ) noexcept {
	if( _currentEraseReq ) {
		_currentEraseReq->getPayload()._failed	= failed;
		_currentEraseReq->returnToSender();
		_currentEraseReq	= nullptr;
		}
	else if( _currentProgramReq ) {
		_currentProgramReq->getPayload()._failed	= failed;
		_currentProgramReq->returnToSender();
		_currentProgramReq	= nullptr;
		}
	else {
		return;
		}
	// When we get here, we finished the last request
	auto
	req	= _pendingReqQ.get();
	if( req ) {
		req->process();
		}
	}
