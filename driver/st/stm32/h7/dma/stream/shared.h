/*
   Copyright (C) 2024 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_driver_st_stm32_h7_dma_stream_sharedh_
#define _oscl_driver_st_stm32_h7_dma_stream_sharedh_

#include "api.h"
#include "oscl/driver/shared/symbiont.h"
#include "oscl/hw/st/stm32/h7/dma/map.h"
#include "oscl/done/api.h"

/** */
namespace Oscl {
/** */
namespace Driver {
/** */
namespace St {
/** */
namespace Stm32 {
/** */
namespace H7 {
/** */
namespace pDMA {
/** */
namespace Stream {

/** A shared interrupt driver for a single DMA Stream.
	This class is intended to be used by other peripheral
	drivers that wish to use a DMA stream for I/O.
 */
class Shared :	
	public Api,
	public Oscl::Driver::Shared::Symbiont::Api
	{
	private:
		/** A reference to the register map for this stream.
		 */
		Oscl::St::Stm32::H7::pDMA::Stream&				_stream;

		/** A reference to the ISR register for this stream.
		 */
		volatile Oscl::St::Stm32::H7::pDMA::rISR::Reg&	_isr;

		/** A reference to the FCR register for this stream.
		 */
		volatile Oscl::St::Stm32::H7::pDMA::rIFCR::Reg&	_fcr;

		/** The least-significant-bit offset for the fSTREAMx
			group of bits in the ISR and FSR for this stream.
		 */
		const unsigned									_fieldLsb;

		/** The NVIC interrupt number for this stream.
		 */
		const unsigned									_nvicInterruptNumber;

	private:
		/** */
		Oscl::Done::Api*								_transferCompleteCallback;

		/** Bitmask where each 1 indicates an error during
			the last transfer.
		 */
		Oscl::St::Stm32::H7::pDMA::rISR::Reg			_normalizedTransferErrorMask;

	public:
		/** */
		Shared(
			Oscl::St::Stm32::H7::pDMA::Stream&				stream,
			volatile Oscl::St::Stm32::H7::pDMA::rISR::Reg&	isr,
			volatile Oscl::St::Stm32::H7::pDMA::rIFCR::Reg&	fcr,
			unsigned										fieldLsb,
			unsigned										nvicInterruptNumber
			) noexcept;

		/** */
		virtual ~Shared(){}

	private: // Oscl::Driver::Shared::Symbiont::Api
		/** */
		void	initialize() noexcept override;

		/** */
		void	process() noexcept override;

	private:
		/** */
		void	startTransfer() noexcept;

	public: // Stream::Api
		/**	WARNING! Some memories are not accessible by DMA1 and DMA2.
			In particular, the DTCM and ITCM cannot be accessed.
		 */
		void	peripheralToMemory(
					const void*			peripheralAddress,
					void*				destinationAddress,
					uint16_t			sizeInBytes,
					Oscl::Done::Api&	transferCompleteCallback
					) noexcept override;

		/**	WARNING! Some memories are not accessible by DMA1 and DMA2.
			In particular, the DTCM and ITCM cannot be accessed.
		 */
		void	memoryToPeripheral(
					const void*			sourceAddress,
					void*				peripheralAddress,
					uint16_t			sizeInBytes,
					Oscl::Done::Api&	transferCompleteCallback
					) noexcept override;

		/**	WARNING! Some memories are not accessible by DMA1 and DMA2.
			In particular, the DTCM and ITCM cannot be accessed.
		 */
		void	memoryToMemory(
					const void*			sourceAddress,
					void*				destinationAddress,
					uint16_t			sizeInBytes,
					Oscl::Done::Api&	transferCompleteCallback
					) noexcept override;
	};

}
}
}
}
}
}
}

#endif
