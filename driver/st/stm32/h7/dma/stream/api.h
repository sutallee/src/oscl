/*
   Copyright (C) 2024 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_driver_st_stm32_h7_dma_stream_apih_
#define _oscl_driver_st_stm32_h7_dma_stream_apih_

#include "oscl/hw/st/stm32/h7/dma/map.h"
#include "oscl/done/api.h"

/** */
namespace Oscl {
/** */
namespace Driver {
/** */
namespace St {
/** */
namespace Stm32 {
/** */
namespace H7 {
/** */
namespace pDMA {
/** */
namespace Stream {

/** An API for a single DMA stream.
 */
class Api {
	public:
		/**	WARNING! Some memories are not accessible by DMA1 and DMA2.
			In particular, the DTCM and ITCM cannot be accessed.
		 */
		virtual void	peripheralToMemory(
							const void*			peripheralAddress,
							void*				destinationAddress,
							uint16_t			sizeInBytes,
							Oscl::Done::Api&	transferCompleteCallbak
							) noexcept = 0;

		/**	WARNING! Some memories are not accessible by DMA1 and DMA2.
			In particular, the DTCM and ITCM cannot be accessed.
		 */
		virtual void	memoryToPeripheral(
							const void*			sourceAddress,
							void*				peripheralAddress,
							uint16_t			sizeInBytes,
							Oscl::Done::Api&	transferCompleteCallbak
							) noexcept = 0;
		/**	WARNING! Some memories are not accessible by DMA1 and DMA2.
			In particular, the DTCM and ITCM cannot be accessed.
		 */
		virtual void	memoryToMemory(
							const void*			sourceAddress,
							void*				destinationAddress,
							uint16_t			sizeInBytes,
							Oscl::Done::Api&	transferCompleteCallbak
							) noexcept = 0;
	};

}
}
}
}
}
}
}

#endif
