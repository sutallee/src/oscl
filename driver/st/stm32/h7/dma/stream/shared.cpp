/*
   Copyright (C) 2024 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <stdio.h>
#include "shared.h"
#include "oscl/hw/st/stm32/h7/dma/reg.h"
#include "oscl/hw/arm/cortexm7/nvic.h"
#include "oscl/driver/cortexm/asm.h"
#include "oscl/driver/arm/cortex/m7/cache.h"

using namespace Oscl::Driver::St::Stm32::H7::pDMA::Stream;

Shared::Shared(
	Oscl::St::Stm32::H7::pDMA::Stream&				stream,
	volatile Oscl::St::Stm32::H7::pDMA::rISR::Reg&	isr,
	volatile Oscl::St::Stm32::H7::pDMA::rIFCR::Reg&	fcr,
	unsigned										fieldLsb,
	unsigned										nvicInterruptNumber
	) noexcept:
		_stream( stream ),
		_isr( isr ),
		_fcr( fcr ),
		_fieldLsb( fieldLsb ),
		_nvicInterruptNumber( nvicInterruptNumber ),
		_transferCompleteCallback( nullptr ),
		_normalizedTransferErrorMask( 0 )
		{
	}

void	Shared::initialize() noexcept {
	// Nothing to do
	// Use DMAMUX1 to route a DMA request line to the DMA channel.
	}

static constexpr Oscl::St::Stm32::H7::pDMA::rISR::Reg
normalizedFieldMask	= 
		0
	|	Oscl::St::Stm32::H7::pDMA::rISR::fSTREAMx::fFEIF::FieldMask
	|	Oscl::St::Stm32::H7::pDMA::rISR::fSTREAMx::fDMEIF::FieldMask
	|	Oscl::St::Stm32::H7::pDMA::rISR::fSTREAMx::fTEIF::FieldMask
	|	Oscl::St::Stm32::H7::pDMA::rISR::fSTREAMx::fHTIF::FieldMask
	|	Oscl::St::Stm32::H7::pDMA::rISR::fSTREAMx::fTCIF::FieldMask
	;

static constexpr Oscl::St::Stm32::H7::pDMA::rISR::Reg
normalizedPendingMask	= 
		0
	|	Oscl::St::Stm32::H7::pDMA::rISR::fSTREAMx::fFEIF::ValueMask_pending
	|	Oscl::St::Stm32::H7::pDMA::rISR::fSTREAMx::fDMEIF::ValueMask_pending
	|	Oscl::St::Stm32::H7::pDMA::rISR::fSTREAMx::fTEIF::ValueMask_pending
	|	Oscl::St::Stm32::H7::pDMA::rISR::fSTREAMx::fHTIF::ValueMask_pending
	|	Oscl::St::Stm32::H7::pDMA::rISR::fSTREAMx::fTCIF::ValueMask_pending
	;

static constexpr Oscl::St::Stm32::H7::pDMA::rISR::Reg
normalizedNotPendingMask	= 
		0
	|	Oscl::St::Stm32::H7::pDMA::rISR::fSTREAMx::fFEIF::ValueMask_notPending
	|	Oscl::St::Stm32::H7::pDMA::rISR::fSTREAMx::fDMEIF::ValueMask_notPending
	|	Oscl::St::Stm32::H7::pDMA::rISR::fSTREAMx::fTEIF::ValueMask_notPending
	|	Oscl::St::Stm32::H7::pDMA::rISR::fSTREAMx::fHTIF::ValueMask_notPending
	|	Oscl::St::Stm32::H7::pDMA::rISR::fSTREAMx::fTCIF::ValueMask_notPending
	;

static constexpr Oscl::St::Stm32::H7::pDMA::rISR::Reg
normalizedTransferErrorMask	= 
		0
	|	Oscl::St::Stm32::H7::pDMA::rISR::fSTREAMx::fFEIF::ValueMask_pending
	|	Oscl::St::Stm32::H7::pDMA::rISR::fSTREAMx::fDMEIF::ValueMask_pending
	|	Oscl::St::Stm32::H7::pDMA::rISR::fSTREAMx::fTEIF::ValueMask_pending
	|	Oscl::St::Stm32::H7::pDMA::rISR::fSTREAMx::fHTIF::ValueMask_pending
	;

void	Shared::process() noexcept {
	using namespace Oscl::St::Stm32::H7::pDMA;

	auto
	isr	= _isr;

	isr	&= (normalizedFieldMask << _fieldLsb);

	auto
	normalizedStatus	= (isr >> _fieldLsb);

	if( normalizedStatus == normalizedNotPendingMask ) {
		return;
		}

	_normalizedTransferErrorMask	= (normalizedStatus & normalizedTransferErrorMask);

	/*	Clear handled interrupts
		Write 1 to clear.
		This asumes
			- all pending interrupt statuses are pending 1
			- all rISR status bits are the same position as rFCR bits.
	 */
	_fcr	= isr;

	if( (normalizedStatus & rISR::fSTREAMx::fTCIF::FieldMask) ==  rISR::fSTREAMx::fTCIF::ValueMask_pending ) {
		// Transfer Complete
		Oscl::Done::Api*	callback	= _transferCompleteCallback;
		printf("Transfer Complete\n");
		if( callback ) {
			_transferCompleteCallback	= nullptr;
			Oscl::Arm::CortexM7::pNVIC::enableInterrupt(_nvicInterruptNumber);
			callback->done();
			}
		}
	if( (normalizedStatus & rISR::fSTREAMx::fFEIF::FieldMask) ==  rISR::fSTREAMx::fFEIF::ValueMask_pending ) {
		printf("FIFO Error\n");
		}
	if( (normalizedStatus & rISR::fSTREAMx::fDMEIF::FieldMask) ==  rISR::fSTREAMx::fDMEIF::ValueMask_pending ) {
		printf("Direct Mode Error\n");
		}
	if( (normalizedStatus & rISR::fSTREAMx::fTEIF::FieldMask) ==  rISR::fSTREAMx::fTEIF::ValueMask_pending ) {
		printf("Transfer Error\n");
		}
	if( (normalizedStatus & rISR::fSTREAMx::fHTIF::FieldMask) ==  rISR::fSTREAMx::fHTIF::ValueMask_pending ) {
		printf("Half Transfer\n");
		}
	}

void	Shared::startTransfer() noexcept {
	using namespace Oscl::St::Stm32::H7::pDMA;

	// Ensure all pending interrupts are cleared
	_fcr	= (normalizedPendingMask << _fieldLsb);

	auto
	cr	= _stream.cr;

	cr	&= ~(
			0
		|	rSCR::fEN::FieldMask
		);
	cr	|=
			0
		|	rSCR::fEN::ValueMask_enable
		;

	_stream.cr	= cr;

	Oscl::Arm::CortexM7::pNVIC::enableInterrupt( _nvicInterruptNumber );
	}

static unsigned	sizeNormalizedValue(
					const void*		addr1,
					const void*		addr2,
					uint16_t		sizeInBytes
					) {
	using namespace Oscl::St::Stm32::H7::pDMA;

	if( sizeInBytes & 0x01 ) {
		return rSCR::fMSIZE::Value_byte;
		}

	auto
	a1	= reinterpret_cast<uintptr_t>(addr1);

	auto
	a2	= reinterpret_cast<uintptr_t>(addr2);

	if( ( a1 & 0x01 ) || ( a2 & 0x01 ) ) {
		return rSCR::fMSIZE::Value_byte;
		}

	if( ( a1 & 0x03 ) || ( a2 & 0x03 ) ) {
		return rSCR::fMSIZE::Value_halfWord;
		}

	return rSCR::fMSIZE::Value_word;
	}

/*
		The burst configuration has to be selected in order to respect the AHB protocol, where
		bursts must not cross the 1 Kbyte address boundary because the minimum address space
		that can be allocated to a single slave is 1 Kbyte. This means that the 1-Kbyte address
		boundary must not be crossed by a burst block transfer, otherwise an AHB error is
		generated, that is not reported by the DMA registers.

	RETURN 0 (single ), 1 ( incr4 ), 2 (incr8), 3 (incr 16)
 */
static unsigned	maximumBurstSize(
					const void*	startAddress,
					uint16_t	sizeInBytes,	// transfer size
					unsigned	perBeatShift	// 0 (byte), 1 (halfWord), or 2 (word)
					) noexcept {

	auto
	start	= reinterpret_cast<uintptr_t>(startAddress);

	auto
	startPage	= start >> 10;	// divide by 1024

	auto
	end		= start + sizeInBytes;

	auto
	endPage	= end >> 10;	// divide by 1024

	if( startPage == endPage ) {
		// Impossible to cross a 1K page boundary
		// 16 beats
		return 3;
		}

	static constexpr unsigned	nFifoEntries = 4;
	static constexpr unsigned	fifoEntryWidth = 4;
	static constexpr unsigned	fifoSizeInBytes = nFifoEntries * fifoEntryWidth;

	const unsigned
	maxBeatsInFifo	= fifoSizeInBytes >> perBeatShift;

#if 0
//	unsigned
//	beatMask	= (16 << perBeatShift) - 1;

	if( ! (start & 0x0F) ) {
		// Everything is 16-byte aligned
		}

	if( diff < 1024 ) {
		}
#endif

	return 0;
	}

void	Shared::peripheralToMemory(
			const void*			peripheralAddress,
			void*				destinationAddress,
			uint16_t			sizeInBytes,
			Oscl::Done::Api&	transferCompleteCallback
			) noexcept {
	}

void	Shared::memoryToPeripheral(
			const void*			sourceAddress,
			void*				peripheralAddress,
			uint16_t			sizeInBytes,
			Oscl::Done::Api&	transferCompleteCallback
			) noexcept {
#if 0
	using namespace Oscl::St::Stm32::H7::pDMA;

	_transferCompleteCallback	= &transferCompleteCallback;

	auto
	cr	= _stream.cr;

	cr	&= ~(
			0
		|	rSCR::fEN::FieldMask
		|	rSCR::fDMEIE::FieldMask
		|	rSCR::fTEIE::FieldMask
		|	rSCR::fHTIE::FieldMask
		|	rSCR::fTCIE::FieldMask
		|	rSCR::fPFCTRL::FieldMask
		|	rSCR::fDIR::FieldMask
		|	rSCR::fCIRC::FieldMask
		|	rSCR::fPINC::FieldMask
		|	rSCR::fMINC::FieldMask
		|	rSCR::fPSIZE::FieldMask
		|	rSCR::fMSIZE::FieldMask
		|	rSCR::fPINCOS::FieldMask
		|	rSCR::fPL::FieldMask
		|	rSCR::fDBM::FieldMask
		|	rSCR::fCT::FieldMask
		|	rSCR::fTRBUFF::FieldMask
		|	rSCR::fPBURST::FieldMask
		|	rSCR::fMBURST::FieldMask
		);

	/*	The size of the burst is configured by software independently for the two AHB ports by using
		the MBURST[1:0] and PBURST[1:0] bits in the DMA_SxCR register.

		The burst size indicates the number of beats in the burst, not the number of bytes
		transferred.

		To ensure data coherence, each group of transfers that form a burst are indivisible: AHB
		transfers are locked and the arbiter of the AHB bus matrix does not degrant the DMA master
		during the sequence of the burst transfer.

		Depending on the single or burst configuration, each DMA request initiates a different
		number of transfers on the AHB peripheral port:

		-	When the AHB peripheral port is configured for single transfers, each DMA request
			generates a data transfer of a byte, half-word or word depending on the PSIZE[1:0] bits
			in the DMA_SxCR register

		-	When the AHB peripheral port is configured for burst transfers, each DMA request
			generates 4,8 or 16 beats of byte, half word or word transfers depending on the
			PBURST[1:0] and PSIZE[1:0] bits in the DMA_SxCR register.

		The same as above has to be considered for the AHB memory port considering the
		MBURST and MSIZE bits.

		In direct mode, the stream can only generate single transfers and the MBURST[1:0] and
		PBURST[1:0] bits are forced by hardware.

		The address pointers (DMA_SxPAR or DMA_SxM0AR registers) must be chosen so as to
		ensure that all transfers within a burst block are aligned on the address boundary equal to
		the size of the transfer.

		The burst configuration has to be selected in order to respect the AHB protocol, where
		bursts must not cross the 1 Kbyte address boundary because the minimum address space
		that can be allocated to a single slave is 1 Kbyte. This means that the 1-Kbyte address
		boundary must not be crossed by a burst block transfer, otherwise an AHB error is
		generated, that is not reported by the DMA registers.
	 */

	auto
	sizeValue	= sizeNormalizedValue( sourceAddress, destinationAddress, sizeInBytes );

	auto
	msizeValueMask	= ( sizeValue << rSCR::fMSIZE::Lsb );

	auto
	psizeValueMask	= ( sizeValue << rSCR::fPSIZE::Lsb );

	auto
	pburst	= maximumBurstSize(
				sourceAddress,
				sizeInBytes,
				sizeValue
				);

	auto
	mburst	= maximumBurstSize(
				destinationAddress,
				sizeInBytes,
				sizeValue
				);

#if 0
	unsigned	nBeats	= 0;

	switch( sizeValue ) {
		case rSCR::fMSIZE::Value_byte:
			nBeats	= 1;
			break;
		case rSCR::fMSIZE::Value_halfWord:
			burst	= rSCR::fPBURST::Value_;
			break;
		case rSCR::fMSIZE::Value_word:
			break;
		default:
			for(;;);
		}
#endif

	// cr == 0x1e15690
	// 0001 1110 0001 0101 0110 1001 0000
	// 0001111000010101011010010000
	// 00 0 11 11 0 0 0 01 0 10 10 1 1 0 10 0 1 0 0 0 0
	// fEN 		0
	// fDMEIE 	0
	// fTEIE 	0
	// fHTIE 	0
	// fTCIE 	1
	// fPFCTRL 	0
	// fDIR 	10
	// fCIRC 	0
	// fPINC 	1
	// fMINC 	1
	// fPSIZE 	10
	// fMSIZE 	10
	// fPINCOS 	0
	// fPL 		01
	// fDBM		0
	// fCT		0
	// fTRBUFF		0
	// fPBURST		11
	// fMBURST		11
	// 

	cr	|=
			0
		|	rSCR::fEN::ValueMask_disable	//0
		|	rSCR::fDMEIE::ValueMask_disable	//0
		|	rSCR::fTEIE::ValueMask_enable	//0
		|	rSCR::fHTIE::ValueMask_disable	//0
		|	rSCR::fTCIE::ValueMask_enable	//1
		|	rSCR::fPFCTRL::ValueMask_dma	//0
		|	rSCR::fDIR::ValueMask_memoryToMemory	//10
		|	rSCR::fCIRC::ValueMask_disable	//0
		|	rSCR::fPINC::ValueMask_increment//1
		|	rSCR::fMINC::ValueMask_increment//1
//		|	psizeValueMask	// rSCR::fPSIZE	10
		|	rSCR::fMSIZE::ValueMask_byte	// rSCR::fPSIZE
//		|	msizeValueMask	// rSCR::fMSIZE	10
		|	rSCR::fPSIZE::ValueMask_byte	// rSCR::fMSIZE
		|	rSCR::fPINCOS::ValueMask_psize	// 0
		|	rSCR::fPL::ValueMask_mediumPriority	// FIXME: API addition	// 01
		|	rSCR::fDBM::ValueMask_noSwitch	// FIXME: API?	// 0
		|	rSCR::fCT::ValueMask_memory0	// What is "target"? (double buffered only) // 0
		|	rSCR::fTRBUFF::ValueMask_disable	// What is bufferable transfer? (enable for UART/USART/LPUART transfers) //0
//		|	(pburst << rSCR::fPBURST::Lsb )	// rSCR::fPBURST::ValueMask_incr16		// FIXME: Nonsense for memory to memory?	//11
		|	(1 << rSCR::fPBURST::Lsb )	// rSCR::fPBURST::ValueMask_incr16		// FIXME: Nonsense for memory to memory?	//11
//		|	(mburst << rSCR::fMBURST::Lsb )	// rSCR::fMBURST::ValueMask_incr16		// FIXME: Is this only possible for beat aligned transfers?//11
		|	(1 << rSCR::fMBURST::Lsb )	// rSCR::fMBURST::ValueMask_incr16		// FIXME: Is this only possible for beat aligned transfers?//11
		;

	_stream.cr	= cr;

	/* FIXME:
		This assumes:
			the NDT field LSB is zero.
			the size is not greater than 65535
	 */
	_stream.ndtr	= sizeInBytes;

	auto
	src		= reinterpret_cast<uintptr_t>(sourceAddress);

	_stream.par	= src;

	auto
	dest	= reinterpret_cast<uintptr_t>(destinationAddress);

	_stream.m0ar	= dest;

	auto
	fcr	= _stream.fcr;

	fcr	&= ~(
			0
		|	rSFCR::fFTH::FieldMask
		|	rSFCR::fDMDIS::FieldMask
		|	rSFCR::fFS::FieldMask
		|	rSFCR::fFEIE::FieldMask
		);
/*
	Caution is required when choosing the FIFO threshold (bits FTH[1:0] of the DMA_SxFCR
	register) and the size of the memory burst (MBURST[1:0] of the DMA_SxCR register): The
	content pointed by the FIFO threshold must exactly match an integer number of memory
	burst transfers. If this is not in the case, a FIFO error (flag FEIFx of the DMA_HISR or
	DMA_LISR register) is generated when the stream is enabled, then the stream is
	automatically disabled. The allowed and forbidden configurations are described in the table
	below. The forbidden configurations are highlighted in gray in the table.

	In all cases, the burst size multiplied by the data size must not exceed the FIFO size (data
	size can be: 1 (byte), 2 (half-word) or 4 (word)).

	Incomplete burst transfer at the end of a DMA transfer may happen if one of the following
	conditions occurs:

	-	For the AHB peripheral port configuration: the total number of data items (set in the
		DMA_SxNDTR register) is not a multiple of the burst size multiplied by the data size.

	-	For the AHB memory port configuration: the number of remaining data items in the
		FIFO to be transferred to the memory is not a multiple of the burst size multiplied by the
		data size.

	In such cases, the remaining data to be transferred is managed in single mode by the DMA,
	even if a burst transaction is requested during the DMA stream configuration.

	Note:
		When burst transfers are requested on the peripheral AHB port and the FIFO is used
		(DMDIS = 1 in the DMA_SxCR register), it is mandatory to respect the following rule to
		avoid permanent underrun or overrun conditions, depending on the DMA stream direction:

		If (PBURST × PSIZE) = FIFO_SIZE (4 words), FIFO_Threshold = 3/4 is forbidden with
		PSIZE = 1, 2 or 4 and PBURST = 4, 8 or 16.

		This rule ensures that enough FIFO space at a time is free to serve the request from the
		peripheral.
 */

	fcr	|=
			0
		|	rSFCR::fFTH::ValueMask_full	// FIXME: idk?
		|	rSFCR::fDMDIS::ValueMask_disableDirectMode	// No direct mode for memory-to-memory
		|	rSFCR::fFS::ValueMask_gtZeroLtQuarter		// FIXME: IDK?
		|	rSFCR::fFEIE::ValueMask_enable
		;

	// fcr 0xA5
	// 0x000000A5
	// 0000 0000 0000 0000 0000 0000 1010 0101
	// 00000000 0000000000000000 1 0 100 1 01
	_stream.fcr	= fcr;

	startTransfer();
#endif
	}

void	Shared::memoryToMemory(
			const void*			sourceAddress,
			void*				destinationAddress,
			uint16_t			sizeInBytes,
			Oscl::Done::Api&	transferCompleteCallback
			) noexcept {
	using namespace Oscl::St::Stm32::H7::pDMA;

	_transferCompleteCallback	= &transferCompleteCallback;

	auto
	cr	= _stream.cr;

	cr	&= ~(
			0
		|	rSCR::fEN::FieldMask
		|	rSCR::fDMEIE::FieldMask
		|	rSCR::fTEIE::FieldMask
		|	rSCR::fHTIE::FieldMask
		|	rSCR::fTCIE::FieldMask
		|	rSCR::fPFCTRL::FieldMask
		|	rSCR::fDIR::FieldMask
		|	rSCR::fCIRC::FieldMask
		|	rSCR::fPINC::FieldMask
		|	rSCR::fMINC::FieldMask
		|	rSCR::fPSIZE::FieldMask
		|	rSCR::fMSIZE::FieldMask
		|	rSCR::fPINCOS::FieldMask
		|	rSCR::fPL::FieldMask
		|	rSCR::fDBM::FieldMask
		|	rSCR::fCT::FieldMask
		|	rSCR::fTRBUFF::FieldMask
		|	rSCR::fPBURST::FieldMask
		|	rSCR::fMBURST::FieldMask
		);

	/*	The size of the burst is configured by software independently for the two AHB ports by using
		the MBURST[1:0] and PBURST[1:0] bits in the DMA_SxCR register.

		The burst size indicates the number of beats in the burst, not the number of bytes
		transferred.

		To ensure data coherence, each group of transfers that form a burst are indivisible: AHB
		transfers are locked and the arbiter of the AHB bus matrix does not degrant the DMA master
		during the sequence of the burst transfer.

		Depending on the single or burst configuration, each DMA request initiates a different
		number of transfers on the AHB peripheral port:

		-	When the AHB peripheral port is configured for single transfers, each DMA request
			generates a data transfer of a byte, half-word or word depending on the PSIZE[1:0] bits
			in the DMA_SxCR register

		-	When the AHB peripheral port is configured for burst transfers, each DMA request
			generates 4,8 or 16 beats of byte, half word or word transfers depending on the
			PBURST[1:0] and PSIZE[1:0] bits in the DMA_SxCR register.

		The same as above has to be considered for the AHB memory port considering the
		MBURST and MSIZE bits.

		In direct mode, the stream can only generate single transfers and the MBURST[1:0] and
		PBURST[1:0] bits are forced by hardware.

		The address pointers (DMA_SxPAR or DMA_SxM0AR registers) must be chosen so as to
		ensure that all transfers within a burst block are aligned on the address boundary equal to
		the size of the transfer.

		The burst configuration has to be selected in order to respect the AHB protocol, where
		bursts must not cross the 1 Kbyte address boundary because the minimum address space
		that can be allocated to a single slave is 1 Kbyte. This means that the 1-Kbyte address
		boundary must not be crossed by a burst block transfer, otherwise an AHB error is
		generated, that is not reported by the DMA registers.
	 */

	auto
	sizeValue	= sizeNormalizedValue( sourceAddress, destinationAddress, sizeInBytes );

	auto
	msizeValueMask	= ( sizeValue << rSCR::fMSIZE::Lsb );

	auto
	psizeValueMask	= ( sizeValue << rSCR::fPSIZE::Lsb );

	auto
	pburst	= maximumBurstSize(
				sourceAddress,
				sizeInBytes,
				sizeValue
				);

	auto
	mburst	= maximumBurstSize(
				destinationAddress,
				sizeInBytes,
				sizeValue
				);

#if 0
	unsigned	nBeats	= 0;

	switch( sizeValue ) {
		case rSCR::fMSIZE::Value_byte:
			nBeats	= 1;
			break;
		case rSCR::fMSIZE::Value_halfWord:
			burst	= rSCR::fPBURST::Value_;
			break;
		case rSCR::fMSIZE::Value_word:
			break;
		default:
			for(;;);
		}
#endif

	// cr == 0x1e15690
	// 0001 1110 0001 0101 0110 1001 0000
	// 0001111000010101011010010000
	// 00 0 11 11 0 0 0 01 0 10 10 1 1 0 10 0 1 0 0 0 0
	// fEN 		0
	// fDMEIE 	0
	// fTEIE 	0
	// fHTIE 	0
	// fTCIE 	1
	// fPFCTRL 	0
	// fDIR 	10
	// fCIRC 	0
	// fPINC 	1
	// fMINC 	1
	// fPSIZE 	10
	// fMSIZE 	10
	// fPINCOS 	0
	// fPL 		01
	// fDBM		0
	// fCT		0
	// fTRBUFF		0
	// fPBURST		11
	// fMBURST		11
	// 

	cr	|=
			0
		|	rSCR::fEN::ValueMask_disable	//0
		|	rSCR::fDMEIE::ValueMask_disable	//0
		|	rSCR::fTEIE::ValueMask_enable	//0
		|	rSCR::fHTIE::ValueMask_disable	//0
		|	rSCR::fTCIE::ValueMask_enable	//1
		|	rSCR::fPFCTRL::ValueMask_dma	//0
		|	rSCR::fDIR::ValueMask_memoryToMemory	//10
		|	rSCR::fCIRC::ValueMask_disable	//0
		|	rSCR::fPINC::ValueMask_increment//1
		|	rSCR::fMINC::ValueMask_increment//1
//		|	psizeValueMask	// rSCR::fPSIZE	10
		|	rSCR::fMSIZE::ValueMask_byte	// rSCR::fPSIZE
//		|	msizeValueMask	// rSCR::fMSIZE	10
		|	rSCR::fPSIZE::ValueMask_byte	// rSCR::fMSIZE
		|	rSCR::fPINCOS::ValueMask_psize	// 0
		|	rSCR::fPL::ValueMask_mediumPriority	// FIXME: API addition	// 01
		|	rSCR::fDBM::ValueMask_noSwitch	// FIXME: API?	// 0
		|	rSCR::fCT::ValueMask_memory0	// What is "target"? (double buffered only) // 0
		|	rSCR::fTRBUFF::ValueMask_disable	// What is bufferable transfer? (enable for UART/USART/LPUART transfers) //0
//		|	(pburst << rSCR::fPBURST::Lsb )	// rSCR::fPBURST::ValueMask_incr16		// FIXME: Nonsense for memory to memory?	//11
		|	(1 << rSCR::fPBURST::Lsb )	// rSCR::fPBURST::ValueMask_incr16		// FIXME: Nonsense for memory to memory?	//11
//		|	(mburst << rSCR::fMBURST::Lsb )	// rSCR::fMBURST::ValueMask_incr16		// FIXME: Is this only possible for beat aligned transfers?//11
		|	(1 << rSCR::fMBURST::Lsb )	// rSCR::fMBURST::ValueMask_incr16		// FIXME: Is this only possible for beat aligned transfers?//11
		;

	_stream.cr	= cr;

	/* FIXME:
		This assumes:
			the NDT field LSB is zero.
			the size is not greater than 65535
	 */
	_stream.ndtr	= sizeInBytes;

	auto
	src		= reinterpret_cast<uintptr_t>(sourceAddress);

	_stream.par	= src;

	auto
	dest	= reinterpret_cast<uintptr_t>(destinationAddress);

	_stream.m0ar	= dest;

	auto
	fcr	= _stream.fcr;

	fcr	&= ~(
			0
		|	rSFCR::fFTH::FieldMask
		|	rSFCR::fDMDIS::FieldMask
		|	rSFCR::fFS::FieldMask
		|	rSFCR::fFEIE::FieldMask
		);
/*
	Caution is required when choosing the FIFO threshold (bits FTH[1:0] of the DMA_SxFCR
	register) and the size of the memory burst (MBURST[1:0] of the DMA_SxCR register): The
	content pointed by the FIFO threshold must exactly match an integer number of memory
	burst transfers. If this is not in the case, a FIFO error (flag FEIFx of the DMA_HISR or
	DMA_LISR register) is generated when the stream is enabled, then the stream is
	automatically disabled. The allowed and forbidden configurations are described in the table
	below. The forbidden configurations are highlighted in gray in the table.

	In all cases, the burst size multiplied by the data size must not exceed the FIFO size (data
	size can be: 1 (byte), 2 (half-word) or 4 (word)).

	Incomplete burst transfer at the end of a DMA transfer may happen if one of the following
	conditions occurs:

	-	For the AHB peripheral port configuration: the total number of data items (set in the
		DMA_SxNDTR register) is not a multiple of the burst size multiplied by the data size.

	-	For the AHB memory port configuration: the number of remaining data items in the
		FIFO to be transferred to the memory is not a multiple of the burst size multiplied by the
		data size.

	In such cases, the remaining data to be transferred is managed in single mode by the DMA,
	even if a burst transaction is requested during the DMA stream configuration.

	Note:
		When burst transfers are requested on the peripheral AHB port and the FIFO is used
		(DMDIS = 1 in the DMA_SxCR register), it is mandatory to respect the following rule to
		avoid permanent underrun or overrun conditions, depending on the DMA stream direction:

		If (PBURST × PSIZE) = FIFO_SIZE (4 words), FIFO_Threshold = 3/4 is forbidden with
		PSIZE = 1, 2 or 4 and PBURST = 4, 8 or 16.

		This rule ensures that enough FIFO space at a time is free to serve the request from the
		peripheral.
 */

	fcr	|=
			0
		|	rSFCR::fFTH::ValueMask_full	// FIXME: idk?
		|	rSFCR::fDMDIS::ValueMask_disableDirectMode	// No direct mode for memory-to-memory
		|	rSFCR::fFS::ValueMask_gtZeroLtQuarter		// FIXME: IDK?
		|	rSFCR::fFEIE::ValueMask_enable
		;

	// fcr 0xA5
	// 0x000000A5
	// 0000 0000 0000 0000 0000 0000 1010 0101
	// 00000000 0000000000000000 1 0 100 1 01
	_stream.fcr	= fcr;

	startTransfer();
	}
