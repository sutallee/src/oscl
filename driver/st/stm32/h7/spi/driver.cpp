/*
   Copyright (C) 2024 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "oscl/hw/st/stm32/h7/gpio/map.h"
#include "oscl/hw/st/stm32/h7/spi/reg.h"
#include "driver.h"
#include "oscl/hw/arm/cortexm7/nvic.h"
#include "oscl/error/fatal.h"
#include <stdio.h>

using namespace Oscl::St::Stm32::H7::pSPI;

//#define DEBUG_TRACE

#ifdef DEBUG_TRACE
#include "oscl/error/info.h"
#endif

Driver::Driver(
	Oscl::St::Stm32::H7::pSPI::Map&	map,
	Oscl::Driver::St::Stm32::H7::
	pDMA::Stream::Api&				stream,
	unsigned long					clockFrequencyInHz,
	unsigned						interruptNumber
	) noexcept:
		OpenSync(
			*this,
			*this
			),
		_map( map ),
		_stream( stream ),
		_clockFrequencyInHz( clockFrequencyInHz ),
		_interruptNumber( interruptNumber ),
		_sync(
			*this,
			*this
			),
		_currentTransferMsg( nullptr ),
		_txDataBuffer( nullptr ),
		_txBytesRemaining( 0 ),
		_rxDataBuffer( nullptr ),
		_rxBytesRemaining( 0 ),
		_mbrValue( ~0 ),
		_lsbFirst( false ),
		_cpol( false ),
		_cpha( false )
		{
	clearAllInterrupts();
	}

void	Driver::clearAllInterrupts() noexcept {

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif

		using namespace Oscl::St::Stm32::H7::pSPI;

		_map.ifcr	=
				0
			|	rIFCR::fEOTC::ValueMask_clear
			|	rIFCR::fTXTFC::ValueMask_clear
			|	rIFCR::fUDRC::ValueMask_clear
			|	rIFCR::fOVRC::ValueMask_clear
			|	rIFCR::fCRCEC::ValueMask_clear
			|	rIFCR::fTIFREC::ValueMask_clear
			|	rIFCR::fMODFC::ValueMask_clear
			|	rIFCR::fTSERFC::ValueMask_clear
			|	rIFCR::fSUSPC::ValueMask_clear
			;
	}

Oscl::Mt::Runnable&	Driver::getDriverRunnable() noexcept{
	return *this;
	}

Oscl::Mt::Sema::SignalApi&	Driver::getSignalApi() noexcept{
	return *this;
	}

Oscl::SPI::Master::Api&		Driver::getSyncApi() noexcept{
	return _sync;
	}

Oscl::SPI::Master::Req::Api::SAP&	Driver::getSAP() noexcept{
	return _sync.getSAP();
	}

void    Driver::start() noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif

	using namespace Oscl::St::Stm32::H7::pSPI;
	}

void    Driver::request(OpenReq& msg) noexcept{

	start();

	msg.returnToSender();
	}

void	Driver::initialize() noexcept {
	}

void	Driver::mboxSignaled() noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif

	using namespace Oscl::St::Stm32::H7::pSPI;

	/*	At this point all interrupts have been
		disabled by the ISR.
	 */

	auto&
	sr	= _map.sr;

	auto
	srv	= sr;

	uint8_t*	rxBuffer		= (uint8_t*)_rxDataBuffer;
	volatile uint8_t*	rxdr	= (volatile uint8_t*)(&_map.rxdr);

	while( _rxBytesRemaining ) {
		if( (sr & rSR::fRXP::FieldMask) != rSR::fRXP::ValueMask_notEmpty) {
			_rxDataBuffer	= rxBuffer;
			break;
			}
		*rxBuffer	= *rxdr;
		++rxBuffer;
		--_rxBytesRemaining;
		}

	const uint8_t*	txBuffer		= (const uint8_t*)_txDataBuffer;
	volatile uint8_t*	txdr	= (volatile uint8_t*)(&_map.txdr);

	while( _txBytesRemaining ) {
		if( (sr & rSR::fTXP::FieldMask) != rSR::fTXP::ValueMask_notFull) {
			_txDataBuffer	= txBuffer;
			break;
			}
		*txdr	= *txBuffer;
		++txBuffer;
		--_txBytesRemaining;
		}

	if( srv != 0 ) {
		uint32_t	ifcr	= 0;
		if( srv & rSR::fEOT::FieldMask ) {
			ifcr	|= rIFCR::fEOTC::ValueMask_clear;
			}
		if( srv & rSR::fTXTF::FieldMask ) {
			ifcr	|= rIFCR::fTXTFC::ValueMask_clear;
			}
		if( srv & rSR::fUDR::FieldMask ) {
			ifcr	|= rIFCR::fUDRC::ValueMask_clear;
			}
		if( srv & rSR::fOVR::FieldMask ) {
			ifcr	|= rIFCR::fOVRC::ValueMask_clear;
			}
		if( srv & rSR::fCRCE::FieldMask ) {
			ifcr	|= rIFCR::fCRCEC::ValueMask_clear;
			}
		if( srv & rSR::fTIFRE::FieldMask ) {
			ifcr	|= rIFCR::fTIFREC::ValueMask_clear;
			}
		if( srv & rSR::fMODF::FieldMask ) {
			ifcr	|= rIFCR::fMODFC::ValueMask_clear;
			}
		if( srv & rSR::fTSERF::FieldMask ) {
			ifcr	|= rIFCR::fTSERFC::ValueMask_clear;
			}
		if( srv & rSR::fSUSP::FieldMask ) {
			ifcr	|= rIFCR::fSUSPC::ValueMask_clear;
			}
		_map.ifcr	=ifcr;
		} 

	Oscl::Arm::CortexM7::pNVIC::enableInterrupt( _interruptNumber );

	if( !_txBytesRemaining && !_rxBytesRemaining ) {
		// All done
		if( _currentTransferMsg ) {
			_currentTransferMsg->getPayload()._failed	= false;
			_currentTransferMsg->returnToSender();
			_currentTransferMsg	= 0;
			}
		}

	processNextReq();
	}

/**	RETURN: true for failure to support
	required values.
 */
bool	Driver::processConfig(
			unsigned long			frequencyInHz,
			unsigned&				mbrValue
			) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif

	unsigned long
	divisor	= _clockFrequencyInHz / frequencyInHz;

	unsigned long
	remainder	= _clockFrequencyInHz % frequencyInHz;

	if( remainder ) {
		++divisor;
		}

	using namespace Oscl::St::Stm32::H7::pSPI;

	if( divisor <= 2 ) {
		mbrValue	= rCFG1::fMBR::Value_divideBy2;
		return false;
		}
	if( divisor <= 4 ) {
		mbrValue	= rCFG1::fMBR::Value_divideBy4;
		return false;
		}
	if( divisor <= 8 ) {
		mbrValue	= rCFG1::fMBR::Value_divideBy8;
		return false;
		}
	if( divisor <= 16 ) {
		mbrValue	= rCFG1::fMBR::Value_divideBy16;
		return false;
		}
	if( divisor <= 32 ) {
		mbrValue	= rCFG1::fMBR::Value_divideBy32;
		return false;
		}
	if( divisor <= 64 ) {
		mbrValue	= rCFG1::fMBR::Value_divideBy64;
		return false;
		}
	if( divisor <= 128 ) {
		mbrValue	= rCFG1::fMBR::Value_divideBy128;
		return false;
		}
	if( divisor <= 256 ) {
		mbrValue	= rCFG1::fMBR::Value_divideBy256;
		return false;
		}

	/* At this point, we are being asked to provide
		a frequency that is lower than our clock
		can provide.
	 */

	return true;
	}

void	Driver::processNextReq() noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif

	if(_currentTransferMsg){
		return;
		}

	using namespace Oscl::St::Stm32::H7::pSPI;

	// Disable first
		auto
		cr1	= _map.cr1;

		cr1	&= ~(
				0
			|	rCR1::fSPE::FieldMask
			|	rCR1::fMASRX::FieldMask
			|	rCR1::fCSTART::FieldMask
			|	rCR1::fCSUSP::FieldMask
			|	rCR1::fHDDIR::FieldMask
			|	rCR1::fSSI::FieldMask
			|	rCR1::fCRC33_17::FieldMask
			|	rCR1::fRCRCI::FieldMask
			|	rCR1::fTCRCI::FieldMask
			|	rCR1::fIOLOCK::FieldMask
			);

		cr1	|=	rCR1::fSPE::ValueMask_disable;

		_map.cr1	= cr1;

		printf("I2SCFGR: 0x%8.8x\n",_map.i2scfgr);
		_map.i2scfgr	= 0;

	while( ( _currentTransferMsg = _pending.get() ) ){

		/* FIXME: IMPLEMENT
			1. Setup SPI configuration
				a. FREQUENCY
				b. CONFIG.CPOL
				c. CONFIG.CPHA
				d. CONFIG.ORDER
			2. Select device
				msg.getPayload()._deviceSelectApi.select();
			3. Start transfer
		 */

		Oscl::SPI::Master::Req::Api::TransferPayload&
		payload	= _currentTransferMsg->getPayload();

		if(payload._frequency){

			unsigned	mbrValue;

			bool
			failed	= processConfig(
						payload._frequency,
						mbrValue
						);

			if(failed){

				#ifdef DEBUG_TRACE
				Oscl::Error::Info::log(
					"%s: processConfig() failed.\n",
					OSCL_PRETTY_FUNCTION
					);
				#endif

				payload._failed	= true;
				_currentTransferMsg->returnToSender();
				continue;
				}
			else {
				printf(
					"SPI mbrValue: %u, frequency: %lu\n",
					mbrValue,
					_clockFrequencyInHz / ( 2U << mbrValue )
					);
				}

			updateConfig(
				mbrValue,
				payload._lsbFirst,
				payload._cpol,
				payload._cpha
				);
			}

		/** At this point, we are ready to start
			the new transaction.
		 */
		payload._deviceSelectApi.select();

		using namespace Oscl::St::Stm32::H7::pSPI;

		_txDataBuffer		= payload._txDataBuffer;
		_rxDataBuffer		= payload._rxDataBuffer;

		_rxBytesRemaining	= ( _rxDataBuffer )? payload._nDataBytesToTransfer: 0;
		_txBytesRemaining	= ( _txDataBuffer )? payload._nDataBytesToTransfer: 0;

		_map.cr2	=
				0
			|	( _txBytesRemaining << rCR2::fTSIZE::Lsb)
			|	( 0 << rCR2::fTSER::Lsb)
			;

		cr1	|=
				0
			|	rCR1::fSPE::ValueMask_disable
			|	rCR1::fMASRX::ValueMask_suspend
			|	rCR1::fCSTART::ValueMask_idle
			|	rCR1::fCSUSP::ValueMask_noEffect
			|	rCR1::fHDDIR::ValueMask_valueAtReset	// Valid only for half-duplex
			|	rCR1::fSSI::ValueMask_valueAtReset		// Only valid if SSM is set
			|	rCR1::fCRC33_17::ValueMask_valueAtReset	// Valid only if CRC is used
			|	rCR1::fRCRCI::ValueMask_valueAtReset	// Valid only if CRC is used
			|	rCR1::fTCRCI::ValueMask_valueAtReset	// Valid only if CRC is used
			|	rCR1::fIOLOCK::ValueMask_notLocked
			;

		_map.cr1	= cr1;

		clearAllInterrupts();

		cr1	|=	rCR1::fSPE::ValueMask_enable;

		_map.cr1	= cr1;

		cr1	|=	rCR1::fCSTART::ValueMask_start;

		_map.cr1	= cr1;

		_map.ier	=
				0
			|	rIER::fRXPIE::ValueMask_enable	// RxFIFO contains less than FTHLV samples
			|	rIER::fTXPIE::ValueMask_enable	// TxFIFO contains less than FTHLV samples
			|	rIER::fDPXPIE::ValueMask_enable
			|	rIER::fEOTIE::ValueMask_enable
			|	rIER::fTXTFIE::ValueMask_enable
			|	rIER::fUDRIE::ValueMask_enable		// Underrun
			|	rIER::fOVRIE::ValueMask_enable		// Overrun
			|	rIER::fCRCEIE::ValueMask_enable
			|	rIER::fTIFREIE::ValueMask_enable	// Frame error
			|	rIER::fMODFIE::ValueMask_enable
			|	rIER::fTSERFIE::ValueMask_disable
			;

		Oscl::Arm::CortexM7::pNVIC::enableInterrupt( _interruptNumber );

		/*
			The END event will be generated after both
			the ENDRX and ENDTX events have been generated.
		 */

		#ifdef DEBUG_TRACE
		Oscl::Error::Info::log(
			"%s: started.\n",
			OSCL_PRETTY_FUNCTION
			);
		#endif

		return;
		}

	}

void	Driver::updateConfig(
			unsigned	mbrValue,
			bool		lsbFirst,
			bool		cpol,
			bool		cpha
			) noexcept{

	if(		( mbrValue  == _mbrValue )
		&&	( lsbFirst  == _lsbFirst )
		&&	( cpol  == _cpol )
		&&	( cpha  == _cpha )
		) {
		// No change in config.
		return;
		}

	/*
		At this point, the config has changed,
		so update the cache and the registers.
	 */

	_mbrValue	= mbrValue;
	_lsbFirst	= lsbFirst;
	_cpol		= cpol;
	_cpha		= cpha;

	using namespace Oscl::St::Stm32::H7::pSPI;

	auto
	cfg1	= _map.cfg1;

	cfg1	&= ~(
			0
		|	rCFG1::fDSIZE::FieldMask
		|	rCFG1::fFTHLV::FieldMask
		|	rCFG1::fUDRCFG::FieldMask
		|	rCFG1::fUDRDET::FieldMask
		|	rCFG1::fRXDMAEN::FieldMask
		|	rCFG1::fTXDMAEN::FieldMask
		|	rCFG1::fCRCSIZE::FieldMask
		|	rCFG1::fCRCEN::FieldMask
		|	rCFG1::fMBR::FieldMask
		);

	cfg1	|=
			0
		|	rCFG1::fDSIZE::ValueMask_v8Bits
		|	( 0 << rCFG1::fFTHLV::Lsb )	// FIXME
		|	rCFG1::fUDRCFG::ValueMask_sendUDRDR
		|	rCFG1::fUDRDET::ValueMask_beginningOfDataFrame
//		|	rCFG1::fRXDMAEN::ValueMask_enable
//		|	rCFG1::fTXDMAEN::ValueMask_enable
		|	rCFG1::fCRCSIZE::ValueMask_valueAtReset
		|	rCFG1::fCRCEN::ValueMask_disable
		|	( mbrValue << rCFG1::fMBR::Lsb )
		;

	_map.cfg1	= cfg1;

	auto
	cfg2	= _map.cfg2;

	cfg2	&= ~(
			0
		|	rCFG2::fMSSI::FieldMask
		|	rCFG2::fMIDI::FieldMask
		|	rCFG2::fIOSWP::FieldMask
		|	rCFG2::fCOMM::FieldMask
		|	rCFG2::fSP::FieldMask
		|	rCFG2::fMASTER::FieldMask
		|	rCFG2::fLSBFRST::FieldMask
		|	rCFG2::fCPHA::FieldMask
		|	rCFG2::fCPOL::FieldMask
		|	rCFG2::fSSM::FieldMask
		|	rCFG2::fSSIOP::FieldMask
		|	rCFG2::fSSOE::FieldMask
		|	rCFG2::fSSOM::FieldMask
		|	rCFG2::fAFCNTR::FieldMask
		);

	cfg2	|=
			0
		|	( rCFG2::fMSSI::Value_valueAtReset << rCFG2::fMSSI::Lsb )
		|	( rCFG2::fMIDI::Value_valueAtReset << rCFG2::fMIDI::Lsb )
		|	rCFG2::fIOSWP::ValueMask_noSWAP
		|	rCFG2::fCOMM::ValueMask_fullDuplex
		|	rCFG2::fSP::ValueMask_motorola		// ??
		|	rCFG2::fMASTER::ValueMask_master
		|	(lsbFirst?
				rCFG2::fLSBFRST::ValueMask_lsb:
				rCFG2::fLSBFRST::ValueMask_msb)
		|	(cpha?											// FIXME
				rCFG2::fCPHA::ValueMask_secondClockEdge:
				rCFG2::fCPHA::ValueMask_firstClockEdge )
		|	(cpol?											// FIXME
				rCFG2::fCPOL::ValueMask_idleHIGH
				:rCFG2::fCPOL::ValueMask_idleLOW)
		|	rCFG2::fSSM::ValueMask_pad			// software controlled SS ?
		|	rCFG2::fSSIOP::ValueMask_activeLOW	// ??
		|	rCFG2::fSSOE::ValueMask_enable		// ??
		|	rCFG2::fSSOM::ValueMask_keepActiveUntilEOT
		|	rCFG2::fAFCNTR::ValueMask_enable	// SPI keeps control of AF GPIO
		;

	printf("cfg2: 0x%8.8X\n",cfg2);

	_map.cfg2	= cfg2;

	printf("_map.cfg2: 0x%8.8X\n",_map.cfg2);
	}

void	Driver::request(Oscl::SPI::Master::Req::Api::TransferReq& msg) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif

	_pending.put(&msg);

	processNextReq();
	}

void	Driver::request(Oscl::SPI::Master::Req::Api::CancelTransferReq& msg) noexcept{
	// FIXME:
	Oscl::ErrorFatal::logAndExit(
		"%s: NOT IMPLEMENTED.\n",
		OSCL_PRETTY_FUNCTION
		);
	}

