/*
   Copyright (C) 2024 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_driver_st_stm32_h7_spi_driverh_
#define _oscl_driver_st_stm32_h7_spi_driverh_

#include "oscl/mt/itc/mbox/server.h"
//#include "oscl/driver/shared/symbiont.h"
#include "oscl/hw/st/stm32/h7/spi/map.h"
#include "oscl/mt/itc/srv/openapi.h"
#include "oscl/mt/itc/srv/open.h"
#include "oscl/mt/itc/mbox/server.h"
#include "oscl/spi/master/itc/reqapi.h"
#include "oscl/spi/master/itc/sync.h"
#include "oscl/spi/master/itc/sync.h"
#include "oscl/driver/st/stm32/h7/dma/stream/api.h"

/** */
namespace Oscl {
/** */
namespace St {
/** */
namespace Stm32 {
/** */
namespace H7 {
/** */
namespace pSPI {

/**
 */
class Driver :	
//	public Oscl::Driver::Shared::Symbiont::Api,
	public Oscl::Mt::Itc::Server,
	public Oscl::Mt::Itc::Srv::OpenSync,
	private Oscl::Mt::Itc::Srv::Open::Req::Api,
	private Oscl::SPI::Master::Req::Api
	{
	private:
		/** */
		Oscl::St::Stm32::H7::pSPI::Map&		_map;

		/** */
		Oscl::Driver::St::Stm32::H7::
		pDMA::Stream::Api&					_stream;

		/** */
		const unsigned long					_clockFrequencyInHz;

		/** */
		const unsigned 						_interruptNumber;

	private:
		/** */
		Oscl::SPI::Master::Sync			_sync;

	private:
        /** */
		Oscl::Queue<
			Oscl::SPI::Master::
			Req::Api::TransferReq
			>						_pending;
        
        /** */
		Oscl::SPI::Master::
		Req::Api::TransferReq*		_currentTransferMsg;

	private:
        /** */
		const void*					_txDataBuffer;

        /** */
		unsigned					_txBytesRemaining;

        /** */
		void*						_rxDataBuffer;

        /** */
		unsigned					_rxBytesRemaining;

	private:
		/** */
		unsigned	_mbrValue;
		/** */
		bool		_lsbFirst;
		/** */
		bool		_cpol;
		/** */
		bool		_cpha;

	public:
		/** */
		Driver(
			Oscl::St::Stm32::H7::pSPI::Map&		reg,
			Oscl::Driver::St::Stm32::
			H7::pDMA::Stream::Api&				stream,
			unsigned long						clockFrequencyInHz,
			unsigned 							interruptNumber
			) noexcept;

		/** */
		virtual ~Driver(){}

		/** */
		Oscl::Mt::Runnable&		getDriverRunnable() noexcept;

		/** RETURN: Reference to the SignalApi to
			be used to notify the driver when a
			transmit interrupt has happened.
		 */
		Oscl::Mt::Sema::SignalApi&	getSignalApi() noexcept;

		/** */
		Oscl::SPI::Master::Api&		getSyncApi() noexcept;

		/** */
		Oscl::SPI::Master::Req::Api::SAP&	getSAP() noexcept;

	private: // Oscl::Mt::Itc::Server
		/** */
		void	mboxSignaled() noexcept override;

		/** */
		void	initialize() noexcept override;

	private:
		/** */
		void	start() noexcept;

	private:
		/** */
		void	clearAllInterrupts() noexcept;

		/** */
		void	processNextReq() noexcept;

		/** */
		bool	processConfig(
					unsigned long			frequencyInHz,
					unsigned&				mbrValue
					) noexcept;

		/** */
		void	updateConfig(
					unsigned	mbrValue,
					bool		lsbFirst,
					bool		cpol,
					bool		cpha
					) noexcept;

	private: // Oscl::Mt::Itc::Srv::Open::Req::Api
		/** */
		void    request(Oscl::Mt::Itc::Srv::Open::Req::Api::OpenReq& msg) noexcept;

	private: // Oscl::SPI::Master::Req::Api
		/** */
		void	request(Oscl::SPI::Master::Req::Api::TransferReq& msg) noexcept;

		/** */
		void	request(Oscl::SPI::Master::Req::Api::CancelTransferReq& msg) noexcept;

	};

}
}
}
}
}

#endif
