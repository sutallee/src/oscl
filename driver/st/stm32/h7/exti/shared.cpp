/*
   Copyright (C) 2024 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "shared.h"
#include "oscl/hw/arm/cortexm7/nvic.h"
#include "oscl/driver/cortexm/asm.h"

using namespace Oscl::St::Stm32::H7::pEXTI;

Shared::Shared(
	Oscl::Revision::Control::Api&					controlApi,
	Oscl::St::Stm32::H7::pEXTI::Map&				map,
	volatile Oscl::St::Stm32::H7::pGPIO::rIDR::Reg&	idr,
	unsigned										interruptNumber,
	unsigned										extiGroupIndex,
	unsigned										extiCpuIndex,
	unsigned										rtsrLsb,
	unsigned										ftsrLsb,
	unsigned										emrLsb,
	unsigned										imrLsb,
	unsigned										prLsb,
	unsigned										idrLsb,
	bool											risingEdgeEnabled,
	bool											fallingEdgeEnabled
	) noexcept:
		_controlApi(controlApi),
		_map(map),
		_idr(idr),
		_interruptNumber(interruptNumber),
		_extiGroupIndex(extiGroupIndex),
		_extiCpuIndex(extiCpuIndex),
		_rtsrLsb(rtsrLsb),
		_ftsrLsb(ftsrLsb),
		_emrLsb(emrLsb),
		_imrLsb(imrLsb),
		_prLsb(prLsb),
		_idrLsb(idrLsb),
		_risingEdgeEnabled( risingEdgeEnabled ),
		_fallingEdgeEnabled( fallingEdgeEnabled )
		{
	}

void Shared::initialize() noexcept{
	using namespace Oscl::St::Stm32::H7::pEXTI;

	const uint32_t	risingEdgeEnable	= _risingEdgeEnabled?rRTSR::enable:rRTSR::disable;
	const uint32_t	fallingEdgeEnable	= _fallingEdgeEnabled?rFTSR::enable:rFTSR::disable;

	auto
	rtsr	= _map.group[_extiGroupIndex].rtsr;
	rtsr	&= ~(
			0
		|	(rRTSR::fTR0::FieldMask << _rtsrLsb)
		);
	rtsr	|=
			0
		|	( risingEdgeEnable << _rtsrLsb)
		;
	_map.group[_extiGroupIndex].rtsr	= rtsr;

	auto
	ftsr	= _map.group[_extiGroupIndex].ftsr;
	ftsr	&= ~(
			0
		|	(rFTSR::fTR0::FieldMask << _ftsrLsb)
		);
	ftsr	|=
			0
		|	( fallingEdgeEnable << _ftsrLsb)
		;
	_map.group[_extiGroupIndex].ftsr	= ftsr;

	auto
	imr	= _map.cpu[_extiCpuIndex].imr;
	imr	&= ~(
			0
		|	(rCPUIMR::fMR0::FieldMask << _imrLsb)
		);
	imr	|=
			0
		|	( rCPUIMR::enable << _imrLsb)
		;
	_map.cpu[_extiCpuIndex].imr	= imr;

	auto
	emr	= _map.cpu[_extiCpuIndex].emr;
	emr	&= ~(
			0
		|	(rCPUEMR::fMR0::FieldMask << _emrLsb)
		);
	emr	|=
			0
		|	( rCPUEMR::enable << _emrLsb)
		;
	_map.cpu[_extiCpuIndex].emr	= emr;
	}

void	Shared::process() noexcept{

	using namespace Oscl::St::Stm32::H7::pEXTI;

	using namespace Oscl::St::Stm32::H7::pGPIO;

	auto
	pending = _map.cpu[_extiCpuIndex].pr;

	pending	&= (rCPUPR::fPR0::FieldMask << _prLsb);

	if( pending ) {

		_map.cpu[_extiCpuIndex].pr	= rCPUPR::clear << _prLsb;

		_controlApi.update();
		}
	
	Oscl::Arm::CortexM7::pNVIC::enableInterrupt( _interruptNumber );
	}

