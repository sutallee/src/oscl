/*
   Copyright (C) 2024 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_driver_st_stm32_h7_exti_sharedh_
#define _oscl_driver_st_stm32_h7_exti_sharedh_

#include "oscl/driver/shared/symbiont.h"
#include "oscl/hw/st/stm32/h7/exti/map.h"
#include "oscl/hw/st/stm32/h7/gpio/reg.h"
#include "oscl/revision/control/api.h"

/** */
namespace Oscl {
/** */
namespace St {
/** */
namespace Stm32 {
/** */
namespace H7 {
/** */
namespace pEXTI {

/** */
class Shared :	public Oscl::Driver::Shared::Symbiont::Api {
	private:
		/** */
		Oscl::Revision::Control::Api&		_controlApi;

		/** */
		Oscl::St::Stm32::H7::pEXTI::Map&	_map;

		volatile Oscl::St::Stm32::H7::pGPIO::rIDR::Reg&	_idr;

		/** */
		const unsigned						_interruptNumber;

		/** */
		const unsigned						_extiGroupIndex;

		/** */
		const unsigned						_extiCpuIndex;

		/** */
		const unsigned						_rtsrLsb;

		/** */
		const unsigned						_ftsrLsb;

		/** */
		const unsigned						_emrLsb;

		/** */
		const unsigned						_imrLsb;

		/** */
		const unsigned						_prLsb;

		/** */
		const unsigned						_idrLsb;

		/** */
		const bool							_risingEdgeEnabled;

		/** */
		const bool							_fallingEdgeEnabled;

	public:
		/** */
		Shared(
			Oscl::Revision::Control::Api&					controlApi,
			Oscl::St::Stm32::H7::pEXTI::Map&				map,
			volatile Oscl::St::Stm32::H7::pGPIO::rIDR::Reg&	idr,
			unsigned										interruptNumber,
			unsigned										extiGroupIndex,
			unsigned										extiCpuIndex,
			unsigned										rtsrLsb,
			unsigned										ftsrLsb,
			unsigned										emrLsb,
			unsigned										imrLsb,
			unsigned										prLsb,
			unsigned										idrLsb,
			bool											risingEdgeEnabled	= true,
			bool											fallingEdgeEnabled	= true
			) noexcept;

		/** */
		virtual ~Shared(){}

	private: // Oscl::Driver::Shared::Symbiont::Api
		/** */
		void	initialize() noexcept override;

		/** */
		void	process() noexcept override;

	};

}
}
}
}
}

#endif
