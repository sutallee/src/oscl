/*
   Copyright (C) 2024 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_driver_st_stm32_h7_i2c_master_server_driverh_
#define _oscl_driver_st_stm32_h7_i2c_master_server_driverh_

#include "oscl/mt/itc/srv/ocsyncapi.h"
#include "oscl/mt/itc/mbox/server.h"
#include "oscl/hw/st/stm32/h7/i2c/map.h"
#include "oscl/i2c/master/sevenbit/itc/reqapi.h"
#include "oscl/i2c/master/sevenbit/itc/sync.h"
#include "interrupt.h"

/** */
namespace Oscl {
/** */
namespace St {
/** */
namespace Stm32 {
/** */
namespace H7 {
/** */
namespace I2C {
/** */
namespace Master {
/** */
namespace Server {
/** */
class Driver :	public Oscl::Mt::Itc::Server,
				public Oscl::Mt::Itc::Srv::OpenCloseSyncApi,
				private Oscl::I2C::Master::SevenBit::Req::Api
				{
	private:
		/** */
		Oscl::St::Stm32::H7::pI2C::Map&						_map;

		/** */
		const unsigned										_interruptNumber;

	private:
		/** */
		Interrupt											_interrupt;

		/** */
		Oscl::I2C::Master::SevenBit::Sync					_sync;

		/** temp */
		Oscl::I2C::Master::SevenBit::Req::Api::ReadReq*		_readMsg;

		/** temp */
		Oscl::I2C::Master::SevenBit::Req::Api::WriteReq*	_writeMsg;

		/** pending */
		Oscl::Queue<Oscl::Mt::Itc::Msg>						_pendingQ;

		/** */
		unsigned char*										_currentData;

		/** */
		unsigned 											_currentRemaining;

		/** */
		bool												_read;

		/** */
		unsigned long										_count;

	public:
		/** */
		Driver(
			Oscl::St::Stm32::H7::pI2C::Map&	mapl,
			unsigned						interruptNumber
			) noexcept;

		/** */
		virtual ~Driver(){}

		/** */
		Oscl::Interrupt::StatusHandlerApi&		getISR() noexcept;
		/** */
		Oscl::Interrupt::IsrDsrApi&				getIsrDsr() noexcept;
		/** */
		Oscl::Mt::Runnable&						getRunnable() noexcept;
		/** */
		Oscl::I2C::Master::SevenBit::
		Req::Api::SAP&							getSevenBitSAP() noexcept;
		/** */
		Oscl::I2C::Master::SevenBit::Api&		getSevenBitSyncApi() noexcept;

	private:
		/** */
		void	processNextPending() noexcept;

	private:	// Server
		/** */
		void	initialize() noexcept;
		/** */
		void	mboxSignaled() noexcept;

	public:	// Oscl::Mt::Itc::Srv::OpenCloseSyncApi
		/** */
		void	syncOpen() noexcept;
		/** */
		void	syncClose() noexcept;

	private: // Oscl::I2C::Master::SevenBit::Req::Api
		/** */
		void	request(ReadReq& msg) noexcept;
		/** */
		void	request(CancelReadReq& msg) noexcept;
		/** */
		void	request(WriteReq& msg) noexcept;
		/** */
		void	request(CancelWriteReq& msg) noexcept;
	};

}
}
}
}
}
}
}

#endif
