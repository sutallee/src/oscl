/*
   Copyright (C) 2024 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_driver_st_stm32_h7_i2c_master_server_interrupth_
#define _oscl_driver_st_stm32_h7_i2c_master_server_interrupth_

#include "oscl/mt/sema/sigapi.h"
#include "oscl/interrupt/shandler.h"
#include "oscl/interrupt/isrdsr.h"
#include "oscl/hw/st/stm32/h7/i2c/map.h"

/** */
namespace Oscl {
/** */
namespace St {
/** */
namespace Stm32 {
/** */
namespace H7 {
/** */
namespace I2C {
/** */
namespace Master {
/** */
namespace Server {

/** */
class Interrupt :	public Oscl::Interrupt::StatusHandlerApi,
					public Oscl::Interrupt::IsrDsrApi
					{
	private:
		/** */
		Oscl::St::Stm32::H7::pI2C::Map&			_map;

		/** */
		Oscl::Mt::Sema::SignalApi&				_drvSema;

	public:
		/** */
		Interrupt(
			Oscl::St::Stm32::H7::pI2C::Map&	map,
			Oscl::Mt::Sema::SignalApi&		drvSema
			) noexcept;

	private:	// Oscl::Interrupt::StatusHandlerApi
		/** */
		bool	interrupt() noexcept;

	private:	// Oscl::Interrupt::IsrDsrApi
		/** */
		bool	isr() noexcept;

		/** */
		void	dsr() noexcept;
	};

}
}
}
}
}
}
}

#endif
