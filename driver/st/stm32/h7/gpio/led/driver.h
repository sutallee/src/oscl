/*
   Copyright (C) 2024 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_driver_st_stm32_h7_gpio_led_driverh_
#define _oscl_driver_st_stm32_h7_gpio_led_driverh_

#include "oscl/led/simple/toggle/api.h"
#include "oscl/hw/st/stm32/h7/gpio/reg.h"

/** */
namespace Oscl {
/** */
namespace Driver {
/** */
namespace St {
/** */
namespace Stm32 {
/** */
namespace H7 {
/** */
namespace Gpio {
/** */
namespace Led {

/** This LED driver assumes that the GPIO pin specified
	in the constructor has been initialized as an output
	with the appropriate characteristics for the implementation.
 */
class Driver: public Oscl::Led::Simple::Toggle::Api {
	private:
		/** */
		volatile Oscl::St::Stm32::H7::pGPIO::rBSRR::Reg&	_bsrr;

		/** */
		const unsigned										_illuminateBitPosition;

		/** */
		const unsigned										_extinguishBitPosition;

		/** */
		bool	_illuminated;

	public:
		/** The illuminate and extinguish bit positions
			correspond to bits within the BSSR register,
			which have fields for driving individual pins
			HIGH or LOW.
			They must be selected according to the type
			of LED circuit that the GPIO is driving.
			An active-LOW LED would have a bit position
			such that when the LED is illuminated, the
			illuminateBitPosition would correspond to the
			BSSR bit that would "reset" the output LOW (a BRn bit)
			and when the LED is extinguished, the extinguishBitPosition
			would correspond to the BSSR bit that would set the
			output HIGH (a BSn bit).
		 */
		Driver(
			volatile Oscl::St::Stm32::H7::pGPIO::rBSRR::Reg&	bsrr,
			unsigned											illuminateBitPosition,
			unsigned											extinguishBitPosition,
			bool												initiallyIlluminated=false
			) noexcept;

		/** */
		void	start() noexcept;

	private: // Oscl::Led::Simple::Api
		/** */
		void	illuminate() const noexcept override;

		/** */
		void	extinguish() const noexcept override;

	private: // Oscl::Led::Simple::Toggle::Api
		/** */
		void	toggle() noexcept override;
	};

}
}
}
}
}
}
}

#endif
