/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/


#include "part.h"
#include "oscl/error/info.h"

using namespace Oscl::Driver::ST::LSM9DS1;

Part::Part(
	Oscl::Mt::Itc::PostMsgApi&		papi,
	Oscl::Driver::ST::
	LSM9DS1::AG::Register::Api&		agApi,
	Oscl::Driver::ST::
	LSM9DS1::MAG::Register::Api&	magApi,
	Oscl::Event::Observer::
	Req::Api::SAP&					agTickSAP,
	Oscl::Event::Observer::
	Req::Api::SAP&					magTickSAP
	) noexcept:
		_agPart(
			papi,
			agApi,
			agTickSAP
			),
		_magPart(
			papi,
			magApi,
			magTickSAP
			)
		{
	}

void	Part::start() noexcept{
	_agPart.start();
	_magPart.start();
	}

Oscl::XYZ::Observer::Req::Api::SAP&	Part::getAccelSAP() noexcept{
	return _agPart.getAccelSAP();
	}

Oscl::XYZ::Observer::Req::Api::SAP&	Part::getGyroSAP() noexcept{
	return _agPart.getGyroSAP();
	}

Oscl::Double::Observer::Req::Api::SAP&	Part::getTemperatureSAP() noexcept{
	return _agPart.getTemperatureSAP();
	}

Oscl::XYZ::Observer::Req::Api::SAP&	Part::getMagnetometerSAP() noexcept{
	return _magPart.getMagnetometerSAP();
	}

