/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/


#include "adapter.h"
#include "oscl/hw/st/lsm9ds1/mag.h"
#include "oscl/error/info.h"

using namespace Oscl::Driver::ST::LSM9DS1::MAG::I2C;

Adapter::Adapter(
			Oscl::I2C::Device::Api&	api
			) noexcept:
			_api(api)
		{
	}

void	Adapter::selectThenWriteReg(uint8_t regNum,uint8_t value) noexcept{
	uint8_t	buff[2];
	buff[0]	= regNum;
	buff[1]	= value;
	_api.write(&buff,2);
	}

uint8_t	Adapter::selectThenReadReg(uint8_t regNum) noexcept{
	_api.write(&regNum,1);
	uint8_t	value;
	_api.read(&value,1);
	return value;
	}

uint8_t	Adapter::getINT_THS_H_M() noexcept{
	return selectThenReadReg(Oscl::HW::ST::LSM9DS1::Magnetometer::INT_THS_H_M);
	}

uint8_t	Adapter::getINT_THS_L_M() noexcept{
	return selectThenReadReg(Oscl::HW::ST::LSM9DS1::Magnetometer::INT_THS_L_M);
	}

uint8_t	Adapter::getINT_SRC_M() noexcept{
	return selectThenReadReg(Oscl::HW::ST::LSM9DS1::Magnetometer::INT_SRC_M);
	}

void	Adapter::setINT_CFG_M(	
							uint8_t	value
							) noexcept{
	selectThenWriteReg(
		Oscl::HW::ST::LSM9DS1::Magnetometer::INT_CFG_M,
		value
		);
	}

uint8_t	Adapter::getINT_CFG_M() noexcept{
	return selectThenReadReg(Oscl::HW::ST::LSM9DS1::Magnetometer::INT_CFG_M);
	}

void	Adapter::getOUT_M(
			uint8_t	maxBufferLength,
			void*	buffer
			) noexcept{

	uint8_t	regNum	= Oscl::HW::ST::LSM9DS1::Magnetometer::OUT_X_L_M;
	_api.write(&regNum,1);

	constexpr unsigned	len	= 6;

	if(maxBufferLength < len){
		Oscl::Error::Info::log(
			"%s: maxBufferLength(%u) < %u\n",
			__PRETTY_FUNCTION__,
			maxBufferLength,
			len
			);
		return;
		}

	_api.read(buffer,len);
	}

uint8_t	Adapter::getOUT_Z_H_M() noexcept{
	return selectThenReadReg(Oscl::HW::ST::LSM9DS1::Magnetometer::OUT_Z_H_M);
	}

uint8_t	Adapter::getOUT_Z_L_M() noexcept{
	return selectThenReadReg(Oscl::HW::ST::LSM9DS1::Magnetometer::OUT_Z_L_M);
	}

uint8_t	Adapter::getOUT_Y_H_M() noexcept{
	return selectThenReadReg(Oscl::HW::ST::LSM9DS1::Magnetometer::OUT_Y_H_M);
	}

uint8_t	Adapter::getOUT_Y_L_M() noexcept{
	return selectThenReadReg(Oscl::HW::ST::LSM9DS1::Magnetometer::OUT_Y_L_M);
	}

uint8_t	Adapter::getOUT_X_H_M() noexcept{
	return selectThenReadReg(Oscl::HW::ST::LSM9DS1::Magnetometer::OUT_X_H_M);
	}

uint8_t	Adapter::getOUT_X_L_M() noexcept{
	return selectThenReadReg(Oscl::HW::ST::LSM9DS1::Magnetometer::OUT_X_L_M);
	}

uint8_t	Adapter::getSTATUS_REG_M() noexcept{
	return selectThenReadReg(Oscl::HW::ST::LSM9DS1::Magnetometer::STATUS_REG_M);
	}

void	Adapter::setCTRL_REG5_M(	
							uint8_t	value
							) noexcept{
	selectThenWriteReg(
		Oscl::HW::ST::LSM9DS1::Magnetometer::CTRL_REG5_M,
		value
		);
	}

uint8_t	Adapter::getCTRL_REG5_M() noexcept{
	return selectThenReadReg(Oscl::HW::ST::LSM9DS1::Magnetometer::CTRL_REG5_M);
	}

void	Adapter::setCTRL_REG4_M(	
							uint8_t	value
							) noexcept{
	selectThenWriteReg(
		Oscl::HW::ST::LSM9DS1::Magnetometer::CTRL_REG4_M,
		value
		);
	}

uint8_t	Adapter::getCTRL_REG4_M() noexcept{
	return selectThenReadReg(Oscl::HW::ST::LSM9DS1::Magnetometer::CTRL_REG4_M);
	}

void	Adapter::setCTRL_REG3_M(	
							uint8_t	value
							) noexcept{
	selectThenWriteReg(
		Oscl::HW::ST::LSM9DS1::Magnetometer::CTRL_REG3_M,
		value
		);
	}

uint8_t	Adapter::getCTRL_REG3_M() noexcept{
	return selectThenReadReg(Oscl::HW::ST::LSM9DS1::Magnetometer::CTRL_REG3_M);
	}

void	Adapter::setCTRL_REG2_M(	
							uint8_t	value
							) noexcept{
	selectThenWriteReg(
		Oscl::HW::ST::LSM9DS1::Magnetometer::CTRL_REG2_M,
		value
		);
	}

uint8_t	Adapter::getCTRL_REG2_M() noexcept{
	return selectThenReadReg(Oscl::HW::ST::LSM9DS1::Magnetometer::CTRL_REG2_M);
	}

void	Adapter::setCTRL_REG1_M(	
							uint8_t	value
							) noexcept{
	selectThenWriteReg(
		Oscl::HW::ST::LSM9DS1::Magnetometer::CTRL_REG1_M,
		value
		);
	}

uint8_t	Adapter::getCTRL_REG1_M() noexcept{
	return selectThenReadReg(Oscl::HW::ST::LSM9DS1::Magnetometer::CTRL_REG1_M);
	}

uint8_t	Adapter::getWHO_AM_I_M() noexcept{
	return selectThenReadReg(Oscl::HW::ST::LSM9DS1::Magnetometer::WHO_AM_I_M);
	}

void	Adapter::setOFFSET_Z_REG_H_M(	
							uint8_t	value
							) noexcept{
	selectThenWriteReg(
		Oscl::HW::ST::LSM9DS1::Magnetometer::OFFSET_Z_REG_H_M,
		value
		);
	}

uint8_t	Adapter::getOFFSET_Z_REG_H_M() noexcept{
	return selectThenReadReg(Oscl::HW::ST::LSM9DS1::Magnetometer::OFFSET_Z_REG_H_M);
	}

void	Adapter::setOFFSET_Z_REG_L_M(	
							uint8_t	value
							) noexcept{
	selectThenWriteReg(
		Oscl::HW::ST::LSM9DS1::Magnetometer::OFFSET_Z_REG_L_M,
		value
		);
	}

uint8_t	Adapter::getOFFSET_Z_REG_L_M() noexcept{
	return selectThenReadReg(Oscl::HW::ST::LSM9DS1::Magnetometer::OFFSET_Z_REG_L_M);
	}

uint8_t	Adapter::getOFFSET_Y_REG_H_M() noexcept{
	return selectThenReadReg(Oscl::HW::ST::LSM9DS1::Magnetometer::OFFSET_Y_REG_H_M);
	}

void	Adapter::setOFFSET_Y_REG_H_M(	
							uint8_t	value
							) noexcept{
	selectThenWriteReg(
		Oscl::HW::ST::LSM9DS1::Magnetometer::OFFSET_Y_REG_H_M,
		value
		);
	}

uint8_t	Adapter::getOFFSET_Y_REG_L_M() noexcept{
	return selectThenReadReg(Oscl::HW::ST::LSM9DS1::Magnetometer::OFFSET_Y_REG_L_M);
	}

void	Adapter::setOFFSET_Y_REG_L_M(	
							uint8_t	value
							) noexcept{
	selectThenWriteReg(
		Oscl::HW::ST::LSM9DS1::Magnetometer::OFFSET_Y_REG_L_M,
		value
		);
	}

void	Adapter::setOFFSET_X_REG_H_M(	
							uint8_t	value
							) noexcept{
	selectThenWriteReg(
		Oscl::HW::ST::LSM9DS1::Magnetometer::OFFSET_X_REG_H_M,
		value
		);
	}

uint8_t	Adapter::getOFFSET_X_REG_H_M() noexcept{
	return selectThenReadReg(Oscl::HW::ST::LSM9DS1::Magnetometer::OFFSET_X_REG_H_M);
	}

void	Adapter::setOFFSET_X_REG_L_M(	
							uint8_t	value
							) noexcept{
	selectThenWriteReg(
		Oscl::HW::ST::LSM9DS1::Magnetometer::OFFSET_X_REG_L_M,
		value
		);
	}

uint8_t	Adapter::getOFFSET_X_REG_L_M() noexcept{
	return selectThenReadReg(Oscl::HW::ST::LSM9DS1::Magnetometer::OFFSET_X_REG_L_M);
	}

