/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <string.h>
#include "part.h"
#include "oscl/error/info.h"
#include "oscl/hw/st/lsm9ds1/mag.h"
#include "oscl/endian/decoder/linear/part.h"
#include "oscl/mt/thread.h"

using namespace Oscl::Driver::ST::LSM9DS1::MAG;

Part::Part(
	Oscl::Mt::Itc::PostMsgApi&	papi,
	Oscl::Driver::ST::
	LSM9DS1::MAG::Register::Api&	api,
	Oscl::Event::Observer::
	Req::Api::SAP&				tickSAP
	) noexcept:
		_papi(papi),
		_api(api),
		_tickSAP(tickSAP),
		_magTickRespComposer(
			*this,
			&Part::magTickResponse,
			0
			),
		_magTickResp(
			tickSAP.getReqApi(),
			_magTickRespComposer,
			papi,
			_magTickPayload
			),
		_magOutput(papi)
		{
	}

bool	Part::checkID() noexcept{
	// Get Magnetometer ID
	uint8_t	id	= _api.getWHO_AM_I_M();

	constexpr uint8_t	expectedID	= (
				Oscl::HW::ST::LSM9DS1::Magnetometer::WhoAmI::ID::Value_MagnetometerID
				>> Oscl::HW::ST::LSM9DS1::Magnetometer::WhoAmI::ID::Lsb
				);


	if(id != expectedID){
		Oscl::Error::Info::log(
			"BAD LSM9DS1 Magnetometer WHO_AM_I ID:"
			" expected: Value_MagnetometerID (0x%2.2X),"
			" read: 0x%2.2X"
			"\n",
			expectedID,
			id
			);
		return true;
		}

	return false;
	}

bool	Part::boot() noexcept{
	uint8_t
	status	= _api.getCTRL_REG2_M();

	status	&=	(~Oscl::HW::ST::LSM9DS1::Magnetometer::CtrlReg2M::REBOOT::FieldMask);

	status	|=	(
			Oscl::HW::ST::LSM9DS1::Magnetometer::CtrlReg2M::REBOOT::Value_RebootMemoryContent
		<<	Oscl::HW::ST::LSM9DS1::Magnetometer::CtrlReg2M::REBOOT::Lsb
		);

	// Reboot memory content
	_api.setCTRL_REG2_M(
		status
		);

	// Wait for Magnetometer to boot
	Oscl::Mt::Thread::sleep(100);
	return false;
	}

bool	Part::configureCTRL_REG1_M() noexcept{
	uint8_t
	temp_comp	= Oscl::HW::ST::LSM9DS1::Magnetometer::CtrlReg1M::TEMP_COMP::Value_Default;

	uint8_t
	om	= Oscl::HW::ST::LSM9DS1::Magnetometer::CtrlReg1M::OM::Value_Default;

	uint8_t
	do_field	= Oscl::HW::ST::LSM9DS1::Magnetometer::CtrlReg1M::DO::Value__10Hz;

	uint8_t
	fast_odr	= Oscl::HW::ST::LSM9DS1::Magnetometer::CtrlReg1M::FAST_ODR::Value_Default;

	uint8_t
	st	= Oscl::HW::ST::LSM9DS1::Magnetometer::CtrlReg1M::ST::Value_Default;

	uint8_t
	value	= (temp_comp << Oscl::HW::ST::LSM9DS1::Magnetometer::CtrlReg1M::TEMP_COMP::Lsb);
	value	|= (om << Oscl::HW::ST::LSM9DS1::Magnetometer::CtrlReg1M::OM::Lsb);
	value	|= (do_field << Oscl::HW::ST::LSM9DS1::Magnetometer::CtrlReg1M::DO::Lsb);
	value	|= (fast_odr << Oscl::HW::ST::LSM9DS1::Magnetometer::CtrlReg1M::FAST_ODR::Lsb);
	value	|= (st << Oscl::HW::ST::LSM9DS1::Magnetometer::CtrlReg1M::ST::Lsb);

	_api.setCTRL_REG1_M(value);

	return false;
	}

bool	Part::configureCTRL_REG2_M() noexcept{
	uint8_t
	fs	= Oscl::HW::ST::LSM9DS1::Magnetometer::CtrlReg2M::FS::Value__8gauss;

	uint8_t
	reboot	= Oscl::HW::ST::LSM9DS1::Magnetometer::CtrlReg2M::REBOOT::Value_Default;

	uint8_t
	soft_rst	= Oscl::HW::ST::LSM9DS1::Magnetometer::CtrlReg2M::SOFT_RST::Value_Default;

	uint8_t
	value	= (fs << Oscl::HW::ST::LSM9DS1::Magnetometer::CtrlReg2M::FS::Lsb);
	value	|= (reboot << Oscl::HW::ST::LSM9DS1::Magnetometer::CtrlReg2M::REBOOT::Lsb);
	value	|= (soft_rst << Oscl::HW::ST::LSM9DS1::Magnetometer::CtrlReg2M::SOFT_RST::Lsb);

	_api.setCTRL_REG2_M(value);

	return false;
	}

bool	Part::configureCTRL_REG3_M() noexcept{
	uint8_t
	i2c_disable	= Oscl::HW::ST::LSM9DS1::Magnetometer::CtrlReg3M::I2C_DISABLE::Value_Default;

	uint8_t
	lp	= Oscl::HW::ST::LSM9DS1::Magnetometer::CtrlReg3M::LP::Value_Default;

	uint8_t
	sim	= Oscl::HW::ST::LSM9DS1::Magnetometer::CtrlReg3M::SIM::Value_Default;

	uint8_t
	md	= Oscl::HW::ST::LSM9DS1::Magnetometer::CtrlReg3M::MD::Value_Continuous;

	uint8_t
	value	= (i2c_disable << Oscl::HW::ST::LSM9DS1::Magnetometer::CtrlReg3M::I2C_DISABLE::Lsb);
	value	|= (lp << Oscl::HW::ST::LSM9DS1::Magnetometer::CtrlReg3M::LP::Lsb);
	value	|= (sim << Oscl::HW::ST::LSM9DS1::Magnetometer::CtrlReg3M::SIM::Lsb);
	value	|= (md << Oscl::HW::ST::LSM9DS1::Magnetometer::CtrlReg3M::MD::Lsb);

	_api.setCTRL_REG3_M(value);

	return false;
	}

bool	Part::configureCTRL_REG4_M() noexcept{
	uint8_t
	omz	= Oscl::HW::ST::LSM9DS1::Magnetometer::CtrlReg4M::OMZ::Value_Default;

	uint8_t
	ble	= Oscl::HW::ST::LSM9DS1::Magnetometer::CtrlReg4M::BLE::Value_Default;

	uint8_t
	value	= (omz << Oscl::HW::ST::LSM9DS1::Magnetometer::CtrlReg4M::OMZ::Lsb);
	value	|= (ble << Oscl::HW::ST::LSM9DS1::Magnetometer::CtrlReg4M::BLE::Lsb);

	_api.setCTRL_REG4_M(value);

	return false;
	}

bool	Part::configureCTRL_REG5_M() noexcept{
	uint8_t
	fast_read	= Oscl::HW::ST::LSM9DS1::Magnetometer::CtrlReg5M::FAST_READ::Value_Enable;

	uint8_t
	bdu	= Oscl::HW::ST::LSM9DS1::Magnetometer::CtrlReg5M::BDU::Value_OutputAfterMsbLsb;

	uint8_t
	value	= (fast_read << Oscl::HW::ST::LSM9DS1::Magnetometer::CtrlReg5M::FAST_READ::Lsb);
	value	|= (bdu << Oscl::HW::ST::LSM9DS1::Magnetometer::CtrlReg5M::BDU::Lsb);

	_api.setCTRL_REG5_M(value);

	return false;
	}

void	Part::start() noexcept{
	checkID();

	if(boot()){
		return;
		}

	if(checkID()){
		return;
		}

	configureCTRL_REG1_M();
	configureCTRL_REG2_M();
	configureCTRL_REG3_M();
	configureCTRL_REG4_M();
	configureCTRL_REG5_M();

	_tickSAP.post(_magTickResp.getSrvMsg());
	}

Oscl::XYZ::Observer::Req::Api::SAP&	Part::getMagnetometerSAP() noexcept{
	return _magOutput.getSAP();
	}

bool	Part::readMagnetometer() noexcept{
	uint8_t	buffer[6];

	memset(buffer,0,sizeof(buffer));

	_api.getOUT_M(
		sizeof(buffer),
		buffer
		);

	Oscl::Endian::Decoder::Linear::Part
	leDecoder(
		buffer,
		sizeof(buffer)
		);

	Oscl::Decoder::Api&	decoder	= leDecoder.le();

	int16_t	x;
	int16_t	y;
	int16_t	z;

	decoder.decode(x);
	decoder.decode(y);
	decoder.decode(z);

	Oscl::XYZ::Vector	v(
		x,
		y,
		z
		);

	double	fs	= 8.0;	// see CTRL_REG2_M FS field
	constexpr double	bits = 32768.0; // 2^15
	double	scale = fs/bits;

	v._x	*= scale;
	v._y	*= scale;
	v._z	*= scale;

	_magOutput.update(v);

	return false;
	}

void	Part::magTickResponse(Oscl::Event::Observer::Resp::Api::OccurrenceResp& msg) noexcept{

	readMagnetometer();

	_tickSAP.post(msg.getSrvMsg());
	}

