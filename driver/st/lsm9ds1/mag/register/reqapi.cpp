/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "reqapi.h"

using namespace Oscl::Driver::ST::LSM9DS1::MAG::Register;

Oscl::Driver::ST::LSM9DS1::MAG::Register::Req::Api::GetINT_THS_H_MPayload::
GetINT_THS_H_MPayload(
		) noexcept
		{}

Oscl::Driver::ST::LSM9DS1::MAG::Register::Req::Api::GetINT_THS_L_MPayload::
GetINT_THS_L_MPayload(
		) noexcept
		{}

Oscl::Driver::ST::LSM9DS1::MAG::Register::Req::Api::GetINT_SRC_MPayload::
GetINT_SRC_MPayload(
		) noexcept
		{}

Oscl::Driver::ST::LSM9DS1::MAG::Register::Req::Api::SetINT_CFG_MPayload::
SetINT_CFG_MPayload(
		uint8_t	value
		) noexcept:
		_value(value)
		{}

Oscl::Driver::ST::LSM9DS1::MAG::Register::Req::Api::GetINT_CFG_MPayload::
GetINT_CFG_MPayload(
		) noexcept
		{}

Oscl::Driver::ST::LSM9DS1::MAG::Register::Req::Api::GetOUT_MPayload::
GetOUT_MPayload(
		uint8_t	maxBufferLength,
		void*	buffer
		) noexcept:
		_maxBufferLength(maxBufferLength),
		_buffer(buffer)
		{}

Oscl::Driver::ST::LSM9DS1::MAG::Register::Req::Api::GetOUT_Z_H_MPayload::
GetOUT_Z_H_MPayload(
		) noexcept
		{}

Oscl::Driver::ST::LSM9DS1::MAG::Register::Req::Api::GetOUT_Z_L_MPayload::
GetOUT_Z_L_MPayload(
		) noexcept
		{}

Oscl::Driver::ST::LSM9DS1::MAG::Register::Req::Api::GetOUT_Y_H_MPayload::
GetOUT_Y_H_MPayload(
		) noexcept
		{}

Oscl::Driver::ST::LSM9DS1::MAG::Register::Req::Api::GetOUT_Y_L_MPayload::
GetOUT_Y_L_MPayload(
		) noexcept
		{}

Oscl::Driver::ST::LSM9DS1::MAG::Register::Req::Api::GetOUT_X_H_MPayload::
GetOUT_X_H_MPayload(
		) noexcept
		{}

Oscl::Driver::ST::LSM9DS1::MAG::Register::Req::Api::GetOUT_X_L_MPayload::
GetOUT_X_L_MPayload(
		) noexcept
		{}

Oscl::Driver::ST::LSM9DS1::MAG::Register::Req::Api::GetSTATUS_REG_MPayload::
GetSTATUS_REG_MPayload(
		) noexcept
		{}

Oscl::Driver::ST::LSM9DS1::MAG::Register::Req::Api::SetCTRL_REG5_MPayload::
SetCTRL_REG5_MPayload(
		uint8_t	value
		) noexcept:
		_value(value)
		{}

Oscl::Driver::ST::LSM9DS1::MAG::Register::Req::Api::GetCTRL_REG5_MPayload::
GetCTRL_REG5_MPayload(
		) noexcept
		{}

Oscl::Driver::ST::LSM9DS1::MAG::Register::Req::Api::SetCTRL_REG4_MPayload::
SetCTRL_REG4_MPayload(
		uint8_t	value
		) noexcept:
		_value(value)
		{}

Oscl::Driver::ST::LSM9DS1::MAG::Register::Req::Api::GetCTRL_REG4_MPayload::
GetCTRL_REG4_MPayload(
		) noexcept
		{}

Oscl::Driver::ST::LSM9DS1::MAG::Register::Req::Api::SetCTRL_REG3_MPayload::
SetCTRL_REG3_MPayload(
		uint8_t	value
		) noexcept:
		_value(value)
		{}

Oscl::Driver::ST::LSM9DS1::MAG::Register::Req::Api::GetCTRL_REG3_MPayload::
GetCTRL_REG3_MPayload(
		) noexcept
		{}

Oscl::Driver::ST::LSM9DS1::MAG::Register::Req::Api::SetCTRL_REG2_MPayload::
SetCTRL_REG2_MPayload(
		uint8_t	value
		) noexcept:
		_value(value)
		{}

Oscl::Driver::ST::LSM9DS1::MAG::Register::Req::Api::GetCTRL_REG2_MPayload::
GetCTRL_REG2_MPayload(
		) noexcept
		{}

Oscl::Driver::ST::LSM9DS1::MAG::Register::Req::Api::SetCTRL_REG1_MPayload::
SetCTRL_REG1_MPayload(
		uint8_t	value
		) noexcept:
		_value(value)
		{}

Oscl::Driver::ST::LSM9DS1::MAG::Register::Req::Api::GetCTRL_REG1_MPayload::
GetCTRL_REG1_MPayload(
		) noexcept
		{}

Oscl::Driver::ST::LSM9DS1::MAG::Register::Req::Api::GetWHO_AM_I_MPayload::
GetWHO_AM_I_MPayload(
		) noexcept
		{}

Oscl::Driver::ST::LSM9DS1::MAG::Register::Req::Api::SetOFFSET_Z_REG_H_MPayload::
SetOFFSET_Z_REG_H_MPayload(
		uint8_t	value
		) noexcept:
		_value(value)
		{}

Oscl::Driver::ST::LSM9DS1::MAG::Register::Req::Api::GetOFFSET_Z_REG_H_MPayload::
GetOFFSET_Z_REG_H_MPayload(
		) noexcept
		{}

Oscl::Driver::ST::LSM9DS1::MAG::Register::Req::Api::SetOFFSET_Z_REG_L_MPayload::
SetOFFSET_Z_REG_L_MPayload(
		uint8_t	value
		) noexcept:
		_value(value)
		{}

Oscl::Driver::ST::LSM9DS1::MAG::Register::Req::Api::GetOFFSET_Z_REG_L_MPayload::
GetOFFSET_Z_REG_L_MPayload(
		) noexcept
		{}

Oscl::Driver::ST::LSM9DS1::MAG::Register::Req::Api::SetOFFSET_Y_REG_H_MPayload::
SetOFFSET_Y_REG_H_MPayload(
		uint8_t	value
		) noexcept:
		_value(value)
		{}

Oscl::Driver::ST::LSM9DS1::MAG::Register::Req::Api::GetOFFSET_Y_REG_H_MPayload::
GetOFFSET_Y_REG_H_MPayload(
		) noexcept
		{}

Oscl::Driver::ST::LSM9DS1::MAG::Register::Req::Api::SetOFFSET_Y_REG_L_MPayload::
SetOFFSET_Y_REG_L_MPayload(
		uint8_t	value
		) noexcept:
		_value(value)
		{}

Oscl::Driver::ST::LSM9DS1::MAG::Register::Req::Api::GetOFFSET_Y_REG_L_MPayload::
GetOFFSET_Y_REG_L_MPayload(
		) noexcept
		{}

Oscl::Driver::ST::LSM9DS1::MAG::Register::Req::Api::SetOFFSET_X_REG_H_MPayload::
SetOFFSET_X_REG_H_MPayload(
		uint8_t	value
		) noexcept:
		_value(value)
		{}

Oscl::Driver::ST::LSM9DS1::MAG::Register::Req::Api::GetOFFSET_X_REG_H_MPayload::
GetOFFSET_X_REG_H_MPayload(
		) noexcept
		{}

Oscl::Driver::ST::LSM9DS1::MAG::Register::Req::Api::SetOFFSET_X_REG_L_MPayload::
SetOFFSET_X_REG_L_MPayload(
		uint8_t	value
		) noexcept:
		_value(value)
		{}

Oscl::Driver::ST::LSM9DS1::MAG::Register::Req::Api::GetOFFSET_X_REG_L_MPayload::
GetOFFSET_X_REG_L_MPayload(
		) noexcept
		{}

