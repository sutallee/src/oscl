/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "sync.h"
#include "oscl/mt/itc/mbox/syncrh.h"

using namespace Oscl::Driver::ST::LSM9DS1::MAG::Register;

Sync::Sync(	Oscl::Mt::Itc::PostMsgApi&	myPapi,
			Req::Api&				reqApi
			) noexcept:
		_sap(reqApi,myPapi)
		{
	}

Req::Api::SAP&	Sync::getSAP() noexcept{
	return _sap;
	}

uint8_t	Sync::getINT_THS_H_M(	
						) noexcept{

	Oscl::Mt::Itc::SyncReturnHandler		srh;

	Req::Api::GetINT_THS_H_MPayload		payload;

	Req::Api::GetINT_THS_H_MReq	req(	_sap.getReqApi(),
									payload,
									srh
									);

	_sap.postSync(req);

	return payload._value;
	}

uint8_t	Sync::getINT_THS_L_M(	
						) noexcept{

	Oscl::Mt::Itc::SyncReturnHandler		srh;

	Req::Api::GetINT_THS_L_MPayload		payload;

	Req::Api::GetINT_THS_L_MReq	req(	_sap.getReqApi(),
									payload,
									srh
									);

	_sap.postSync(req);

	return payload._value;
	}

uint8_t	Sync::getINT_SRC_M(	
						) noexcept{

	Oscl::Mt::Itc::SyncReturnHandler		srh;

	Req::Api::GetINT_SRC_MPayload		payload;

	Req::Api::GetINT_SRC_MReq	req(	_sap.getReqApi(),
									payload,
									srh
									);

	_sap.postSync(req);

	return payload._value;
	}

void	Sync::setINT_CFG_M(	
						uint8_t	value
						) noexcept{

	Oscl::Mt::Itc::SyncReturnHandler		srh;

	Req::Api::SetINT_CFG_MPayload		payload(
									value
									);

	Req::Api::SetINT_CFG_MReq	req(	_sap.getReqApi(),
									payload,
									srh
									);

	_sap.postSync(req);
	}

uint8_t	Sync::getINT_CFG_M(	
						) noexcept{

	Oscl::Mt::Itc::SyncReturnHandler		srh;

	Req::Api::GetINT_CFG_MPayload		payload;

	Req::Api::GetINT_CFG_MReq	req(	_sap.getReqApi(),
									payload,
									srh
									);

	_sap.postSync(req);

	return payload._value;
	}

void	Sync::getOUT_M(	
						uint8_t	maxBufferLength,
						void*	buffer
						) noexcept{

	Oscl::Mt::Itc::SyncReturnHandler		srh;

	Req::Api::GetOUT_MPayload		payload(
									maxBufferLength,
									buffer
									);

	Req::Api::GetOUT_MReq	req(	_sap.getReqApi(),
									payload,
									srh
									);

	_sap.postSync(req);
	}

uint8_t	Sync::getOUT_Z_H_M(	
						) noexcept{

	Oscl::Mt::Itc::SyncReturnHandler		srh;

	Req::Api::GetOUT_Z_H_MPayload		payload;

	Req::Api::GetOUT_Z_H_MReq	req(	_sap.getReqApi(),
									payload,
									srh
									);

	_sap.postSync(req);

	return payload._value;
	}

uint8_t	Sync::getOUT_Z_L_M(	
						) noexcept{

	Oscl::Mt::Itc::SyncReturnHandler		srh;

	Req::Api::GetOUT_Z_L_MPayload		payload;

	Req::Api::GetOUT_Z_L_MReq	req(	_sap.getReqApi(),
									payload,
									srh
									);

	_sap.postSync(req);

	return payload._value;
	}

uint8_t	Sync::getOUT_Y_H_M(	
						) noexcept{

	Oscl::Mt::Itc::SyncReturnHandler		srh;

	Req::Api::GetOUT_Y_H_MPayload		payload;

	Req::Api::GetOUT_Y_H_MReq	req(	_sap.getReqApi(),
									payload,
									srh
									);

	_sap.postSync(req);

	return payload._value;
	}

uint8_t	Sync::getOUT_Y_L_M(	
						) noexcept{

	Oscl::Mt::Itc::SyncReturnHandler		srh;

	Req::Api::GetOUT_Y_L_MPayload		payload;

	Req::Api::GetOUT_Y_L_MReq	req(	_sap.getReqApi(),
									payload,
									srh
									);

	_sap.postSync(req);

	return payload._value;
	}

uint8_t	Sync::getOUT_X_H_M(	
						) noexcept{

	Oscl::Mt::Itc::SyncReturnHandler		srh;

	Req::Api::GetOUT_X_H_MPayload		payload;

	Req::Api::GetOUT_X_H_MReq	req(	_sap.getReqApi(),
									payload,
									srh
									);

	_sap.postSync(req);

	return payload._value;
	}

uint8_t	Sync::getOUT_X_L_M(	
						) noexcept{

	Oscl::Mt::Itc::SyncReturnHandler		srh;

	Req::Api::GetOUT_X_L_MPayload		payload;

	Req::Api::GetOUT_X_L_MReq	req(	_sap.getReqApi(),
									payload,
									srh
									);

	_sap.postSync(req);

	return payload._value;
	}

uint8_t	Sync::getSTATUS_REG_M(	
						) noexcept{

	Oscl::Mt::Itc::SyncReturnHandler		srh;

	Req::Api::GetSTATUS_REG_MPayload		payload;

	Req::Api::GetSTATUS_REG_MReq	req(	_sap.getReqApi(),
									payload,
									srh
									);

	_sap.postSync(req);

	return payload._value;
	}

void	Sync::setCTRL_REG5_M(	
						uint8_t	value
						) noexcept{

	Oscl::Mt::Itc::SyncReturnHandler		srh;

	Req::Api::SetCTRL_REG5_MPayload		payload(
									value
									);

	Req::Api::SetCTRL_REG5_MReq	req(	_sap.getReqApi(),
									payload,
									srh
									);

	_sap.postSync(req);
	}

uint8_t	Sync::getCTRL_REG5_M(	
						) noexcept{

	Oscl::Mt::Itc::SyncReturnHandler		srh;

	Req::Api::GetCTRL_REG5_MPayload		payload;

	Req::Api::GetCTRL_REG5_MReq	req(	_sap.getReqApi(),
									payload,
									srh
									);

	_sap.postSync(req);

	return payload._value;
	}

void	Sync::setCTRL_REG4_M(	
						uint8_t	value
						) noexcept{

	Oscl::Mt::Itc::SyncReturnHandler		srh;

	Req::Api::SetCTRL_REG4_MPayload		payload(
									value
									);

	Req::Api::SetCTRL_REG4_MReq	req(	_sap.getReqApi(),
									payload,
									srh
									);

	_sap.postSync(req);
	}

uint8_t	Sync::getCTRL_REG4_M(	
						) noexcept{

	Oscl::Mt::Itc::SyncReturnHandler		srh;

	Req::Api::GetCTRL_REG4_MPayload		payload;

	Req::Api::GetCTRL_REG4_MReq	req(	_sap.getReqApi(),
									payload,
									srh
									);

	_sap.postSync(req);

	return payload._value;
	}

void	Sync::setCTRL_REG3_M(	
						uint8_t	value
						) noexcept{

	Oscl::Mt::Itc::SyncReturnHandler		srh;

	Req::Api::SetCTRL_REG3_MPayload		payload(
									value
									);

	Req::Api::SetCTRL_REG3_MReq	req(	_sap.getReqApi(),
									payload,
									srh
									);

	_sap.postSync(req);
	}

uint8_t	Sync::getCTRL_REG3_M(	
						) noexcept{

	Oscl::Mt::Itc::SyncReturnHandler		srh;

	Req::Api::GetCTRL_REG3_MPayload		payload;

	Req::Api::GetCTRL_REG3_MReq	req(	_sap.getReqApi(),
									payload,
									srh
									);

	_sap.postSync(req);

	return payload._value;
	}

void	Sync::setCTRL_REG2_M(	
						uint8_t	value
						) noexcept{

	Oscl::Mt::Itc::SyncReturnHandler		srh;

	Req::Api::SetCTRL_REG2_MPayload		payload(
									value
									);

	Req::Api::SetCTRL_REG2_MReq	req(	_sap.getReqApi(),
									payload,
									srh
									);

	_sap.postSync(req);
	}

uint8_t	Sync::getCTRL_REG2_M(	
						) noexcept{

	Oscl::Mt::Itc::SyncReturnHandler		srh;

	Req::Api::GetCTRL_REG2_MPayload		payload;

	Req::Api::GetCTRL_REG2_MReq	req(	_sap.getReqApi(),
									payload,
									srh
									);

	_sap.postSync(req);

	return payload._value;
	}

void	Sync::setCTRL_REG1_M(	
						uint8_t	value
						) noexcept{

	Oscl::Mt::Itc::SyncReturnHandler		srh;

	Req::Api::SetCTRL_REG1_MPayload		payload(
									value
									);

	Req::Api::SetCTRL_REG1_MReq	req(	_sap.getReqApi(),
									payload,
									srh
									);

	_sap.postSync(req);
	}

uint8_t	Sync::getCTRL_REG1_M(	
						) noexcept{

	Oscl::Mt::Itc::SyncReturnHandler		srh;

	Req::Api::GetCTRL_REG1_MPayload		payload;

	Req::Api::GetCTRL_REG1_MReq	req(	_sap.getReqApi(),
									payload,
									srh
									);

	_sap.postSync(req);

	return payload._value;
	}

uint8_t	Sync::getWHO_AM_I_M(	
						) noexcept{

	Oscl::Mt::Itc::SyncReturnHandler		srh;

	Req::Api::GetWHO_AM_I_MPayload		payload;

	Req::Api::GetWHO_AM_I_MReq	req(	_sap.getReqApi(),
									payload,
									srh
									);

	_sap.postSync(req);

	return payload._value;
	}

void	Sync::setOFFSET_Z_REG_H_M(	
						uint8_t	value
						) noexcept{

	Oscl::Mt::Itc::SyncReturnHandler		srh;

	Req::Api::SetOFFSET_Z_REG_H_MPayload		payload(
									value
									);

	Req::Api::SetOFFSET_Z_REG_H_MReq	req(	_sap.getReqApi(),
									payload,
									srh
									);

	_sap.postSync(req);
	}

uint8_t	Sync::getOFFSET_Z_REG_H_M(	
						) noexcept{

	Oscl::Mt::Itc::SyncReturnHandler		srh;

	Req::Api::GetOFFSET_Z_REG_H_MPayload		payload;

	Req::Api::GetOFFSET_Z_REG_H_MReq	req(	_sap.getReqApi(),
									payload,
									srh
									);

	_sap.postSync(req);

	return payload._value;
	}

void	Sync::setOFFSET_Z_REG_L_M(	
						uint8_t	value
						) noexcept{

	Oscl::Mt::Itc::SyncReturnHandler		srh;

	Req::Api::SetOFFSET_Z_REG_L_MPayload		payload(
									value
									);

	Req::Api::SetOFFSET_Z_REG_L_MReq	req(	_sap.getReqApi(),
									payload,
									srh
									);

	_sap.postSync(req);
	}

uint8_t	Sync::getOFFSET_Z_REG_L_M(	
						) noexcept{

	Oscl::Mt::Itc::SyncReturnHandler		srh;

	Req::Api::GetOFFSET_Z_REG_L_MPayload		payload;

	Req::Api::GetOFFSET_Z_REG_L_MReq	req(	_sap.getReqApi(),
									payload,
									srh
									);

	_sap.postSync(req);

	return payload._value;
	}

void	Sync::setOFFSET_Y_REG_H_M(	
						uint8_t	value
						) noexcept{

	Oscl::Mt::Itc::SyncReturnHandler		srh;

	Req::Api::SetOFFSET_Y_REG_H_MPayload		payload(
									value
									);

	Req::Api::SetOFFSET_Y_REG_H_MReq	req(	_sap.getReqApi(),
									payload,
									srh
									);

	_sap.postSync(req);
	}

uint8_t	Sync::getOFFSET_Y_REG_H_M(	
						) noexcept{

	Oscl::Mt::Itc::SyncReturnHandler		srh;

	Req::Api::GetOFFSET_Y_REG_H_MPayload		payload;

	Req::Api::GetOFFSET_Y_REG_H_MReq	req(	_sap.getReqApi(),
									payload,
									srh
									);

	_sap.postSync(req);

	return payload._value;
	}

void	Sync::setOFFSET_Y_REG_L_M(	
						uint8_t	value
						) noexcept{

	Oscl::Mt::Itc::SyncReturnHandler		srh;

	Req::Api::SetOFFSET_Y_REG_L_MPayload		payload(
									value
									);

	Req::Api::SetOFFSET_Y_REG_L_MReq	req(	_sap.getReqApi(),
									payload,
									srh
									);

	_sap.postSync(req);
	}

uint8_t	Sync::getOFFSET_Y_REG_L_M(	
						) noexcept{

	Oscl::Mt::Itc::SyncReturnHandler		srh;

	Req::Api::GetOFFSET_Y_REG_L_MPayload		payload;

	Req::Api::GetOFFSET_Y_REG_L_MReq	req(	_sap.getReqApi(),
									payload,
									srh
									);

	_sap.postSync(req);

	return payload._value;
	}

void	Sync::setOFFSET_X_REG_H_M(	
						uint8_t	value
						) noexcept{

	Oscl::Mt::Itc::SyncReturnHandler		srh;

	Req::Api::SetOFFSET_X_REG_H_MPayload		payload(
									value
									);

	Req::Api::SetOFFSET_X_REG_H_MReq	req(	_sap.getReqApi(),
									payload,
									srh
									);

	_sap.postSync(req);
	}

uint8_t	Sync::getOFFSET_X_REG_H_M(	
						) noexcept{

	Oscl::Mt::Itc::SyncReturnHandler		srh;

	Req::Api::GetOFFSET_X_REG_H_MPayload		payload;

	Req::Api::GetOFFSET_X_REG_H_MReq	req(	_sap.getReqApi(),
									payload,
									srh
									);

	_sap.postSync(req);

	return payload._value;
	}

void	Sync::setOFFSET_X_REG_L_M(	
						uint8_t	value
						) noexcept{

	Oscl::Mt::Itc::SyncReturnHandler		srh;

	Req::Api::SetOFFSET_X_REG_L_MPayload		payload(
									value
									);

	Req::Api::SetOFFSET_X_REG_L_MReq	req(	_sap.getReqApi(),
									payload,
									srh
									);

	_sap.postSync(req);
	}

uint8_t	Sync::getOFFSET_X_REG_L_M(	
						) noexcept{

	Oscl::Mt::Itc::SyncReturnHandler		srh;

	Req::Api::GetOFFSET_X_REG_L_MPayload		payload;

	Req::Api::GetOFFSET_X_REG_L_MReq	req(	_sap.getReqApi(),
									payload,
									srh
									);

	_sap.postSync(req);

	return payload._value;
	}

