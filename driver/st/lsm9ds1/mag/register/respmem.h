/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_driver_st_lsm9ds1_mag_register_itc_respmemh_
#define _oscl_driver_st_lsm9ds1_mag_register_itc_respmemh_

#include "respapi.h"
#include "oscl/memory/block.h"

/**	These record types in this file describe blocks of memory
	that are large enough and appropriately aligned such that
	placement new operations creating instances of their
	corresponding objects can safely be performed. These records
	are useful to most clients that use the server's ITC
	interface asynchronously.
 */

/** */
namespace Oscl {

/** */
namespace Driver {

/** */
namespace ST {

/** */
namespace LSM9DS1 {

/** */
namespace MAG {

/** */
namespace Register {

/** */
namespace Resp {

/**	This record describes a block of memory that is
	large enough (and properly aligned) to hold a
	GetINT_THS_H_MResp and its corresponding Req::Api::GetINT_THS_H_MPayload.
	Clients which use the corresponding server asynchronously
	frequently need to reservere/manage memory for for these
	ITC interactions and instances of this record are useful
	for that purpose.
 */
struct GetINT_THS_H_MMem {
	/**	This is memory enough for the response message.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Resp::Api::GetINT_THS_H_MResp)>	resp;

	/**	This is memory enough for the message payload.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Req::Api::GetINT_THS_H_MPayload)>	payload;

	/**	This operation creates a response message using this
		 block of memory.
	 */
	Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::GetINT_THS_H_MResp&
		build(	Oscl::Driver::ST::LSM9DS1::MAG::Register::Req::Api::SAP&	sap,
				Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api&		respApi,
				Oscl::Mt::Itc::PostMsgApi&		clientPapi
				) noexcept;

	};

/**	This record describes a block of memory that is
	large enough (and properly aligned) to hold a
	GetINT_THS_L_MResp and its corresponding Req::Api::GetINT_THS_L_MPayload.
	Clients which use the corresponding server asynchronously
	frequently need to reservere/manage memory for for these
	ITC interactions and instances of this record are useful
	for that purpose.
 */
struct GetINT_THS_L_MMem {
	/**	This is memory enough for the response message.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Resp::Api::GetINT_THS_L_MResp)>	resp;

	/**	This is memory enough for the message payload.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Req::Api::GetINT_THS_L_MPayload)>	payload;

	/**	This operation creates a response message using this
		 block of memory.
	 */
	Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::GetINT_THS_L_MResp&
		build(	Oscl::Driver::ST::LSM9DS1::MAG::Register::Req::Api::SAP&	sap,
				Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api&		respApi,
				Oscl::Mt::Itc::PostMsgApi&		clientPapi
				) noexcept;

	};

/**	This record describes a block of memory that is
	large enough (and properly aligned) to hold a
	GetINT_SRC_MResp and its corresponding Req::Api::GetINT_SRC_MPayload.
	Clients which use the corresponding server asynchronously
	frequently need to reservere/manage memory for for these
	ITC interactions and instances of this record are useful
	for that purpose.
 */
struct GetINT_SRC_MMem {
	/**	This is memory enough for the response message.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Resp::Api::GetINT_SRC_MResp)>	resp;

	/**	This is memory enough for the message payload.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Req::Api::GetINT_SRC_MPayload)>	payload;

	/**	This operation creates a response message using this
		 block of memory.
	 */
	Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::GetINT_SRC_MResp&
		build(	Oscl::Driver::ST::LSM9DS1::MAG::Register::Req::Api::SAP&	sap,
				Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api&		respApi,
				Oscl::Mt::Itc::PostMsgApi&		clientPapi
				) noexcept;

	};

/**	This record describes a block of memory that is
	large enough (and properly aligned) to hold a
	SetINT_CFG_MResp and its corresponding Req::Api::SetINT_CFG_MPayload.
	Clients which use the corresponding server asynchronously
	frequently need to reservere/manage memory for for these
	ITC interactions and instances of this record are useful
	for that purpose.
 */
struct SetINT_CFG_MMem {
	/**	This is memory enough for the response message.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Resp::Api::SetINT_CFG_MResp)>	resp;

	/**	This is memory enough for the message payload.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Req::Api::SetINT_CFG_MPayload)>	payload;

	/**	This operation creates a response message using this
		 block of memory.
	 */
	Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::SetINT_CFG_MResp&
		build(	Oscl::Driver::ST::LSM9DS1::MAG::Register::Req::Api::SAP&	sap,
				Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api&		respApi,
				Oscl::Mt::Itc::PostMsgApi&		clientPapi,
				uint8_t	value
				) noexcept;

	};

/**	This record describes a block of memory that is
	large enough (and properly aligned) to hold a
	GetINT_CFG_MResp and its corresponding Req::Api::GetINT_CFG_MPayload.
	Clients which use the corresponding server asynchronously
	frequently need to reservere/manage memory for for these
	ITC interactions and instances of this record are useful
	for that purpose.
 */
struct GetINT_CFG_MMem {
	/**	This is memory enough for the response message.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Resp::Api::GetINT_CFG_MResp)>	resp;

	/**	This is memory enough for the message payload.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Req::Api::GetINT_CFG_MPayload)>	payload;

	/**	This operation creates a response message using this
		 block of memory.
	 */
	Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::GetINT_CFG_MResp&
		build(	Oscl::Driver::ST::LSM9DS1::MAG::Register::Req::Api::SAP&	sap,
				Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api&		respApi,
				Oscl::Mt::Itc::PostMsgApi&		clientPapi
				) noexcept;

	};

/**	This record describes a block of memory that is
	large enough (and properly aligned) to hold a
	GetOUT_MResp and its corresponding Req::Api::GetOUT_MPayload.
	Clients which use the corresponding server asynchronously
	frequently need to reservere/manage memory for for these
	ITC interactions and instances of this record are useful
	for that purpose.
 */
struct GetOUT_MMem {
	/**	This is memory enough for the response message.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Resp::Api::GetOUT_MResp)>	resp;

	/**	This is memory enough for the message payload.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Req::Api::GetOUT_MPayload)>	payload;

	/**	This operation creates a response message using this
		 block of memory.
	 */
	Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::GetOUT_MResp&
		build(	Oscl::Driver::ST::LSM9DS1::MAG::Register::Req::Api::SAP&	sap,
				Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api&		respApi,
				Oscl::Mt::Itc::PostMsgApi&		clientPapi,
				uint8_t	maxBufferLength,
				void*	buffer
				) noexcept;

	};

/**	This record describes a block of memory that is
	large enough (and properly aligned) to hold a
	GetOUT_Z_H_MResp and its corresponding Req::Api::GetOUT_Z_H_MPayload.
	Clients which use the corresponding server asynchronously
	frequently need to reservere/manage memory for for these
	ITC interactions and instances of this record are useful
	for that purpose.
 */
struct GetOUT_Z_H_MMem {
	/**	This is memory enough for the response message.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Resp::Api::GetOUT_Z_H_MResp)>	resp;

	/**	This is memory enough for the message payload.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Req::Api::GetOUT_Z_H_MPayload)>	payload;

	/**	This operation creates a response message using this
		 block of memory.
	 */
	Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::GetOUT_Z_H_MResp&
		build(	Oscl::Driver::ST::LSM9DS1::MAG::Register::Req::Api::SAP&	sap,
				Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api&		respApi,
				Oscl::Mt::Itc::PostMsgApi&		clientPapi
				) noexcept;

	};

/**	This record describes a block of memory that is
	large enough (and properly aligned) to hold a
	GetOUT_Z_L_MResp and its corresponding Req::Api::GetOUT_Z_L_MPayload.
	Clients which use the corresponding server asynchronously
	frequently need to reservere/manage memory for for these
	ITC interactions and instances of this record are useful
	for that purpose.
 */
struct GetOUT_Z_L_MMem {
	/**	This is memory enough for the response message.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Resp::Api::GetOUT_Z_L_MResp)>	resp;

	/**	This is memory enough for the message payload.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Req::Api::GetOUT_Z_L_MPayload)>	payload;

	/**	This operation creates a response message using this
		 block of memory.
	 */
	Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::GetOUT_Z_L_MResp&
		build(	Oscl::Driver::ST::LSM9DS1::MAG::Register::Req::Api::SAP&	sap,
				Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api&		respApi,
				Oscl::Mt::Itc::PostMsgApi&		clientPapi
				) noexcept;

	};

/**	This record describes a block of memory that is
	large enough (and properly aligned) to hold a
	GetOUT_Y_H_MResp and its corresponding Req::Api::GetOUT_Y_H_MPayload.
	Clients which use the corresponding server asynchronously
	frequently need to reservere/manage memory for for these
	ITC interactions and instances of this record are useful
	for that purpose.
 */
struct GetOUT_Y_H_MMem {
	/**	This is memory enough for the response message.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Resp::Api::GetOUT_Y_H_MResp)>	resp;

	/**	This is memory enough for the message payload.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Req::Api::GetOUT_Y_H_MPayload)>	payload;

	/**	This operation creates a response message using this
		 block of memory.
	 */
	Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::GetOUT_Y_H_MResp&
		build(	Oscl::Driver::ST::LSM9DS1::MAG::Register::Req::Api::SAP&	sap,
				Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api&		respApi,
				Oscl::Mt::Itc::PostMsgApi&		clientPapi
				) noexcept;

	};

/**	This record describes a block of memory that is
	large enough (and properly aligned) to hold a
	GetOUT_Y_L_MResp and its corresponding Req::Api::GetOUT_Y_L_MPayload.
	Clients which use the corresponding server asynchronously
	frequently need to reservere/manage memory for for these
	ITC interactions and instances of this record are useful
	for that purpose.
 */
struct GetOUT_Y_L_MMem {
	/**	This is memory enough for the response message.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Resp::Api::GetOUT_Y_L_MResp)>	resp;

	/**	This is memory enough for the message payload.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Req::Api::GetOUT_Y_L_MPayload)>	payload;

	/**	This operation creates a response message using this
		 block of memory.
	 */
	Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::GetOUT_Y_L_MResp&
		build(	Oscl::Driver::ST::LSM9DS1::MAG::Register::Req::Api::SAP&	sap,
				Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api&		respApi,
				Oscl::Mt::Itc::PostMsgApi&		clientPapi
				) noexcept;

	};

/**	This record describes a block of memory that is
	large enough (and properly aligned) to hold a
	GetOUT_X_H_MResp and its corresponding Req::Api::GetOUT_X_H_MPayload.
	Clients which use the corresponding server asynchronously
	frequently need to reservere/manage memory for for these
	ITC interactions and instances of this record are useful
	for that purpose.
 */
struct GetOUT_X_H_MMem {
	/**	This is memory enough for the response message.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Resp::Api::GetOUT_X_H_MResp)>	resp;

	/**	This is memory enough for the message payload.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Req::Api::GetOUT_X_H_MPayload)>	payload;

	/**	This operation creates a response message using this
		 block of memory.
	 */
	Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::GetOUT_X_H_MResp&
		build(	Oscl::Driver::ST::LSM9DS1::MAG::Register::Req::Api::SAP&	sap,
				Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api&		respApi,
				Oscl::Mt::Itc::PostMsgApi&		clientPapi
				) noexcept;

	};

/**	This record describes a block of memory that is
	large enough (and properly aligned) to hold a
	GetOUT_X_L_MResp and its corresponding Req::Api::GetOUT_X_L_MPayload.
	Clients which use the corresponding server asynchronously
	frequently need to reservere/manage memory for for these
	ITC interactions and instances of this record are useful
	for that purpose.
 */
struct GetOUT_X_L_MMem {
	/**	This is memory enough for the response message.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Resp::Api::GetOUT_X_L_MResp)>	resp;

	/**	This is memory enough for the message payload.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Req::Api::GetOUT_X_L_MPayload)>	payload;

	/**	This operation creates a response message using this
		 block of memory.
	 */
	Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::GetOUT_X_L_MResp&
		build(	Oscl::Driver::ST::LSM9DS1::MAG::Register::Req::Api::SAP&	sap,
				Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api&		respApi,
				Oscl::Mt::Itc::PostMsgApi&		clientPapi
				) noexcept;

	};

/**	This record describes a block of memory that is
	large enough (and properly aligned) to hold a
	GetSTATUS_REG_MResp and its corresponding Req::Api::GetSTATUS_REG_MPayload.
	Clients which use the corresponding server asynchronously
	frequently need to reservere/manage memory for for these
	ITC interactions and instances of this record are useful
	for that purpose.
 */
struct GetSTATUS_REG_MMem {
	/**	This is memory enough for the response message.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Resp::Api::GetSTATUS_REG_MResp)>	resp;

	/**	This is memory enough for the message payload.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Req::Api::GetSTATUS_REG_MPayload)>	payload;

	/**	This operation creates a response message using this
		 block of memory.
	 */
	Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::GetSTATUS_REG_MResp&
		build(	Oscl::Driver::ST::LSM9DS1::MAG::Register::Req::Api::SAP&	sap,
				Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api&		respApi,
				Oscl::Mt::Itc::PostMsgApi&		clientPapi
				) noexcept;

	};

/**	This record describes a block of memory that is
	large enough (and properly aligned) to hold a
	SetCTRL_REG5_MResp and its corresponding Req::Api::SetCTRL_REG5_MPayload.
	Clients which use the corresponding server asynchronously
	frequently need to reservere/manage memory for for these
	ITC interactions and instances of this record are useful
	for that purpose.
 */
struct SetCTRL_REG5_MMem {
	/**	This is memory enough for the response message.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Resp::Api::SetCTRL_REG5_MResp)>	resp;

	/**	This is memory enough for the message payload.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Req::Api::SetCTRL_REG5_MPayload)>	payload;

	/**	This operation creates a response message using this
		 block of memory.
	 */
	Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::SetCTRL_REG5_MResp&
		build(	Oscl::Driver::ST::LSM9DS1::MAG::Register::Req::Api::SAP&	sap,
				Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api&		respApi,
				Oscl::Mt::Itc::PostMsgApi&		clientPapi,
				uint8_t	value
				) noexcept;

	};

/**	This record describes a block of memory that is
	large enough (and properly aligned) to hold a
	GetCTRL_REG5_MResp and its corresponding Req::Api::GetCTRL_REG5_MPayload.
	Clients which use the corresponding server asynchronously
	frequently need to reservere/manage memory for for these
	ITC interactions and instances of this record are useful
	for that purpose.
 */
struct GetCTRL_REG5_MMem {
	/**	This is memory enough for the response message.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Resp::Api::GetCTRL_REG5_MResp)>	resp;

	/**	This is memory enough for the message payload.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Req::Api::GetCTRL_REG5_MPayload)>	payload;

	/**	This operation creates a response message using this
		 block of memory.
	 */
	Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::GetCTRL_REG5_MResp&
		build(	Oscl::Driver::ST::LSM9DS1::MAG::Register::Req::Api::SAP&	sap,
				Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api&		respApi,
				Oscl::Mt::Itc::PostMsgApi&		clientPapi
				) noexcept;

	};

/**	This record describes a block of memory that is
	large enough (and properly aligned) to hold a
	SetCTRL_REG4_MResp and its corresponding Req::Api::SetCTRL_REG4_MPayload.
	Clients which use the corresponding server asynchronously
	frequently need to reservere/manage memory for for these
	ITC interactions and instances of this record are useful
	for that purpose.
 */
struct SetCTRL_REG4_MMem {
	/**	This is memory enough for the response message.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Resp::Api::SetCTRL_REG4_MResp)>	resp;

	/**	This is memory enough for the message payload.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Req::Api::SetCTRL_REG4_MPayload)>	payload;

	/**	This operation creates a response message using this
		 block of memory.
	 */
	Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::SetCTRL_REG4_MResp&
		build(	Oscl::Driver::ST::LSM9DS1::MAG::Register::Req::Api::SAP&	sap,
				Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api&		respApi,
				Oscl::Mt::Itc::PostMsgApi&		clientPapi,
				uint8_t	value
				) noexcept;

	};

/**	This record describes a block of memory that is
	large enough (and properly aligned) to hold a
	GetCTRL_REG4_MResp and its corresponding Req::Api::GetCTRL_REG4_MPayload.
	Clients which use the corresponding server asynchronously
	frequently need to reservere/manage memory for for these
	ITC interactions and instances of this record are useful
	for that purpose.
 */
struct GetCTRL_REG4_MMem {
	/**	This is memory enough for the response message.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Resp::Api::GetCTRL_REG4_MResp)>	resp;

	/**	This is memory enough for the message payload.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Req::Api::GetCTRL_REG4_MPayload)>	payload;

	/**	This operation creates a response message using this
		 block of memory.
	 */
	Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::GetCTRL_REG4_MResp&
		build(	Oscl::Driver::ST::LSM9DS1::MAG::Register::Req::Api::SAP&	sap,
				Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api&		respApi,
				Oscl::Mt::Itc::PostMsgApi&		clientPapi
				) noexcept;

	};

/**	This record describes a block of memory that is
	large enough (and properly aligned) to hold a
	SetCTRL_REG3_MResp and its corresponding Req::Api::SetCTRL_REG3_MPayload.
	Clients which use the corresponding server asynchronously
	frequently need to reservere/manage memory for for these
	ITC interactions and instances of this record are useful
	for that purpose.
 */
struct SetCTRL_REG3_MMem {
	/**	This is memory enough for the response message.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Resp::Api::SetCTRL_REG3_MResp)>	resp;

	/**	This is memory enough for the message payload.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Req::Api::SetCTRL_REG3_MPayload)>	payload;

	/**	This operation creates a response message using this
		 block of memory.
	 */
	Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::SetCTRL_REG3_MResp&
		build(	Oscl::Driver::ST::LSM9DS1::MAG::Register::Req::Api::SAP&	sap,
				Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api&		respApi,
				Oscl::Mt::Itc::PostMsgApi&		clientPapi,
				uint8_t	value
				) noexcept;

	};

/**	This record describes a block of memory that is
	large enough (and properly aligned) to hold a
	GetCTRL_REG3_MResp and its corresponding Req::Api::GetCTRL_REG3_MPayload.
	Clients which use the corresponding server asynchronously
	frequently need to reservere/manage memory for for these
	ITC interactions and instances of this record are useful
	for that purpose.
 */
struct GetCTRL_REG3_MMem {
	/**	This is memory enough for the response message.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Resp::Api::GetCTRL_REG3_MResp)>	resp;

	/**	This is memory enough for the message payload.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Req::Api::GetCTRL_REG3_MPayload)>	payload;

	/**	This operation creates a response message using this
		 block of memory.
	 */
	Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::GetCTRL_REG3_MResp&
		build(	Oscl::Driver::ST::LSM9DS1::MAG::Register::Req::Api::SAP&	sap,
				Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api&		respApi,
				Oscl::Mt::Itc::PostMsgApi&		clientPapi
				) noexcept;

	};

/**	This record describes a block of memory that is
	large enough (and properly aligned) to hold a
	SetCTRL_REG2_MResp and its corresponding Req::Api::SetCTRL_REG2_MPayload.
	Clients which use the corresponding server asynchronously
	frequently need to reservere/manage memory for for these
	ITC interactions and instances of this record are useful
	for that purpose.
 */
struct SetCTRL_REG2_MMem {
	/**	This is memory enough for the response message.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Resp::Api::SetCTRL_REG2_MResp)>	resp;

	/**	This is memory enough for the message payload.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Req::Api::SetCTRL_REG2_MPayload)>	payload;

	/**	This operation creates a response message using this
		 block of memory.
	 */
	Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::SetCTRL_REG2_MResp&
		build(	Oscl::Driver::ST::LSM9DS1::MAG::Register::Req::Api::SAP&	sap,
				Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api&		respApi,
				Oscl::Mt::Itc::PostMsgApi&		clientPapi,
				uint8_t	value
				) noexcept;

	};

/**	This record describes a block of memory that is
	large enough (and properly aligned) to hold a
	GetCTRL_REG2_MResp and its corresponding Req::Api::GetCTRL_REG2_MPayload.
	Clients which use the corresponding server asynchronously
	frequently need to reservere/manage memory for for these
	ITC interactions and instances of this record are useful
	for that purpose.
 */
struct GetCTRL_REG2_MMem {
	/**	This is memory enough for the response message.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Resp::Api::GetCTRL_REG2_MResp)>	resp;

	/**	This is memory enough for the message payload.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Req::Api::GetCTRL_REG2_MPayload)>	payload;

	/**	This operation creates a response message using this
		 block of memory.
	 */
	Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::GetCTRL_REG2_MResp&
		build(	Oscl::Driver::ST::LSM9DS1::MAG::Register::Req::Api::SAP&	sap,
				Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api&		respApi,
				Oscl::Mt::Itc::PostMsgApi&		clientPapi
				) noexcept;

	};

/**	This record describes a block of memory that is
	large enough (and properly aligned) to hold a
	SetCTRL_REG1_MResp and its corresponding Req::Api::SetCTRL_REG1_MPayload.
	Clients which use the corresponding server asynchronously
	frequently need to reservere/manage memory for for these
	ITC interactions and instances of this record are useful
	for that purpose.
 */
struct SetCTRL_REG1_MMem {
	/**	This is memory enough for the response message.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Resp::Api::SetCTRL_REG1_MResp)>	resp;

	/**	This is memory enough for the message payload.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Req::Api::SetCTRL_REG1_MPayload)>	payload;

	/**	This operation creates a response message using this
		 block of memory.
	 */
	Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::SetCTRL_REG1_MResp&
		build(	Oscl::Driver::ST::LSM9DS1::MAG::Register::Req::Api::SAP&	sap,
				Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api&		respApi,
				Oscl::Mt::Itc::PostMsgApi&		clientPapi,
				uint8_t	value
				) noexcept;

	};

/**	This record describes a block of memory that is
	large enough (and properly aligned) to hold a
	GetCTRL_REG1_MResp and its corresponding Req::Api::GetCTRL_REG1_MPayload.
	Clients which use the corresponding server asynchronously
	frequently need to reservere/manage memory for for these
	ITC interactions and instances of this record are useful
	for that purpose.
 */
struct GetCTRL_REG1_MMem {
	/**	This is memory enough for the response message.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Resp::Api::GetCTRL_REG1_MResp)>	resp;

	/**	This is memory enough for the message payload.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Req::Api::GetCTRL_REG1_MPayload)>	payload;

	/**	This operation creates a response message using this
		 block of memory.
	 */
	Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::GetCTRL_REG1_MResp&
		build(	Oscl::Driver::ST::LSM9DS1::MAG::Register::Req::Api::SAP&	sap,
				Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api&		respApi,
				Oscl::Mt::Itc::PostMsgApi&		clientPapi
				) noexcept;

	};

/**	This record describes a block of memory that is
	large enough (and properly aligned) to hold a
	GetWHO_AM_I_MResp and its corresponding Req::Api::GetWHO_AM_I_MPayload.
	Clients which use the corresponding server asynchronously
	frequently need to reservere/manage memory for for these
	ITC interactions and instances of this record are useful
	for that purpose.
 */
struct GetWHO_AM_I_MMem {
	/**	This is memory enough for the response message.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Resp::Api::GetWHO_AM_I_MResp)>	resp;

	/**	This is memory enough for the message payload.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Req::Api::GetWHO_AM_I_MPayload)>	payload;

	/**	This operation creates a response message using this
		 block of memory.
	 */
	Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::GetWHO_AM_I_MResp&
		build(	Oscl::Driver::ST::LSM9DS1::MAG::Register::Req::Api::SAP&	sap,
				Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api&		respApi,
				Oscl::Mt::Itc::PostMsgApi&		clientPapi
				) noexcept;

	};

/**	This record describes a block of memory that is
	large enough (and properly aligned) to hold a
	SetOFFSET_Z_REG_H_MResp and its corresponding Req::Api::SetOFFSET_Z_REG_H_MPayload.
	Clients which use the corresponding server asynchronously
	frequently need to reservere/manage memory for for these
	ITC interactions and instances of this record are useful
	for that purpose.
 */
struct SetOFFSET_Z_REG_H_MMem {
	/**	This is memory enough for the response message.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Resp::Api::SetOFFSET_Z_REG_H_MResp)>	resp;

	/**	This is memory enough for the message payload.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Req::Api::SetOFFSET_Z_REG_H_MPayload)>	payload;

	/**	This operation creates a response message using this
		 block of memory.
	 */
	Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::SetOFFSET_Z_REG_H_MResp&
		build(	Oscl::Driver::ST::LSM9DS1::MAG::Register::Req::Api::SAP&	sap,
				Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api&		respApi,
				Oscl::Mt::Itc::PostMsgApi&		clientPapi,
				uint8_t	value
				) noexcept;

	};

/**	This record describes a block of memory that is
	large enough (and properly aligned) to hold a
	GetOFFSET_Z_REG_H_MResp and its corresponding Req::Api::GetOFFSET_Z_REG_H_MPayload.
	Clients which use the corresponding server asynchronously
	frequently need to reservere/manage memory for for these
	ITC interactions and instances of this record are useful
	for that purpose.
 */
struct GetOFFSET_Z_REG_H_MMem {
	/**	This is memory enough for the response message.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Resp::Api::GetOFFSET_Z_REG_H_MResp)>	resp;

	/**	This is memory enough for the message payload.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Req::Api::GetOFFSET_Z_REG_H_MPayload)>	payload;

	/**	This operation creates a response message using this
		 block of memory.
	 */
	Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::GetOFFSET_Z_REG_H_MResp&
		build(	Oscl::Driver::ST::LSM9DS1::MAG::Register::Req::Api::SAP&	sap,
				Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api&		respApi,
				Oscl::Mt::Itc::PostMsgApi&		clientPapi
				) noexcept;

	};

/**	This record describes a block of memory that is
	large enough (and properly aligned) to hold a
	SetOFFSET_Z_REG_L_MResp and its corresponding Req::Api::SetOFFSET_Z_REG_L_MPayload.
	Clients which use the corresponding server asynchronously
	frequently need to reservere/manage memory for for these
	ITC interactions and instances of this record are useful
	for that purpose.
 */
struct SetOFFSET_Z_REG_L_MMem {
	/**	This is memory enough for the response message.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Resp::Api::SetOFFSET_Z_REG_L_MResp)>	resp;

	/**	This is memory enough for the message payload.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Req::Api::SetOFFSET_Z_REG_L_MPayload)>	payload;

	/**	This operation creates a response message using this
		 block of memory.
	 */
	Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::SetOFFSET_Z_REG_L_MResp&
		build(	Oscl::Driver::ST::LSM9DS1::MAG::Register::Req::Api::SAP&	sap,
				Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api&		respApi,
				Oscl::Mt::Itc::PostMsgApi&		clientPapi,
				uint8_t	value
				) noexcept;

	};

/**	This record describes a block of memory that is
	large enough (and properly aligned) to hold a
	GetOFFSET_Z_REG_L_MResp and its corresponding Req::Api::GetOFFSET_Z_REG_L_MPayload.
	Clients which use the corresponding server asynchronously
	frequently need to reservere/manage memory for for these
	ITC interactions and instances of this record are useful
	for that purpose.
 */
struct GetOFFSET_Z_REG_L_MMem {
	/**	This is memory enough for the response message.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Resp::Api::GetOFFSET_Z_REG_L_MResp)>	resp;

	/**	This is memory enough for the message payload.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Req::Api::GetOFFSET_Z_REG_L_MPayload)>	payload;

	/**	This operation creates a response message using this
		 block of memory.
	 */
	Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::GetOFFSET_Z_REG_L_MResp&
		build(	Oscl::Driver::ST::LSM9DS1::MAG::Register::Req::Api::SAP&	sap,
				Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api&		respApi,
				Oscl::Mt::Itc::PostMsgApi&		clientPapi
				) noexcept;

	};

/**	This record describes a block of memory that is
	large enough (and properly aligned) to hold a
	SetOFFSET_Y_REG_H_MResp and its corresponding Req::Api::SetOFFSET_Y_REG_H_MPayload.
	Clients which use the corresponding server asynchronously
	frequently need to reservere/manage memory for for these
	ITC interactions and instances of this record are useful
	for that purpose.
 */
struct SetOFFSET_Y_REG_H_MMem {
	/**	This is memory enough for the response message.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Resp::Api::SetOFFSET_Y_REG_H_MResp)>	resp;

	/**	This is memory enough for the message payload.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Req::Api::SetOFFSET_Y_REG_H_MPayload)>	payload;

	/**	This operation creates a response message using this
		 block of memory.
	 */
	Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::SetOFFSET_Y_REG_H_MResp&
		build(	Oscl::Driver::ST::LSM9DS1::MAG::Register::Req::Api::SAP&	sap,
				Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api&		respApi,
				Oscl::Mt::Itc::PostMsgApi&		clientPapi,
				uint8_t	value
				) noexcept;

	};

/**	This record describes a block of memory that is
	large enough (and properly aligned) to hold a
	GetOFFSET_Y_REG_H_MResp and its corresponding Req::Api::GetOFFSET_Y_REG_H_MPayload.
	Clients which use the corresponding server asynchronously
	frequently need to reservere/manage memory for for these
	ITC interactions and instances of this record are useful
	for that purpose.
 */
struct GetOFFSET_Y_REG_H_MMem {
	/**	This is memory enough for the response message.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Resp::Api::GetOFFSET_Y_REG_H_MResp)>	resp;

	/**	This is memory enough for the message payload.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Req::Api::GetOFFSET_Y_REG_H_MPayload)>	payload;

	/**	This operation creates a response message using this
		 block of memory.
	 */
	Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::GetOFFSET_Y_REG_H_MResp&
		build(	Oscl::Driver::ST::LSM9DS1::MAG::Register::Req::Api::SAP&	sap,
				Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api&		respApi,
				Oscl::Mt::Itc::PostMsgApi&		clientPapi
				) noexcept;

	};

/**	This record describes a block of memory that is
	large enough (and properly aligned) to hold a
	SetOFFSET_Y_REG_L_MResp and its corresponding Req::Api::SetOFFSET_Y_REG_L_MPayload.
	Clients which use the corresponding server asynchronously
	frequently need to reservere/manage memory for for these
	ITC interactions and instances of this record are useful
	for that purpose.
 */
struct SetOFFSET_Y_REG_L_MMem {
	/**	This is memory enough for the response message.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Resp::Api::SetOFFSET_Y_REG_L_MResp)>	resp;

	/**	This is memory enough for the message payload.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Req::Api::SetOFFSET_Y_REG_L_MPayload)>	payload;

	/**	This operation creates a response message using this
		 block of memory.
	 */
	Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::SetOFFSET_Y_REG_L_MResp&
		build(	Oscl::Driver::ST::LSM9DS1::MAG::Register::Req::Api::SAP&	sap,
				Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api&		respApi,
				Oscl::Mt::Itc::PostMsgApi&		clientPapi,
				uint8_t	value
				) noexcept;

	};

/**	This record describes a block of memory that is
	large enough (and properly aligned) to hold a
	GetOFFSET_Y_REG_L_MResp and its corresponding Req::Api::GetOFFSET_Y_REG_L_MPayload.
	Clients which use the corresponding server asynchronously
	frequently need to reservere/manage memory for for these
	ITC interactions and instances of this record are useful
	for that purpose.
 */
struct GetOFFSET_Y_REG_L_MMem {
	/**	This is memory enough for the response message.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Resp::Api::GetOFFSET_Y_REG_L_MResp)>	resp;

	/**	This is memory enough for the message payload.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Req::Api::GetOFFSET_Y_REG_L_MPayload)>	payload;

	/**	This operation creates a response message using this
		 block of memory.
	 */
	Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::GetOFFSET_Y_REG_L_MResp&
		build(	Oscl::Driver::ST::LSM9DS1::MAG::Register::Req::Api::SAP&	sap,
				Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api&		respApi,
				Oscl::Mt::Itc::PostMsgApi&		clientPapi
				) noexcept;

	};

/**	This record describes a block of memory that is
	large enough (and properly aligned) to hold a
	SetOFFSET_X_REG_H_MResp and its corresponding Req::Api::SetOFFSET_X_REG_H_MPayload.
	Clients which use the corresponding server asynchronously
	frequently need to reservere/manage memory for for these
	ITC interactions and instances of this record are useful
	for that purpose.
 */
struct SetOFFSET_X_REG_H_MMem {
	/**	This is memory enough for the response message.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Resp::Api::SetOFFSET_X_REG_H_MResp)>	resp;

	/**	This is memory enough for the message payload.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Req::Api::SetOFFSET_X_REG_H_MPayload)>	payload;

	/**	This operation creates a response message using this
		 block of memory.
	 */
	Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::SetOFFSET_X_REG_H_MResp&
		build(	Oscl::Driver::ST::LSM9DS1::MAG::Register::Req::Api::SAP&	sap,
				Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api&		respApi,
				Oscl::Mt::Itc::PostMsgApi&		clientPapi,
				uint8_t	value
				) noexcept;

	};

/**	This record describes a block of memory that is
	large enough (and properly aligned) to hold a
	GetOFFSET_X_REG_H_MResp and its corresponding Req::Api::GetOFFSET_X_REG_H_MPayload.
	Clients which use the corresponding server asynchronously
	frequently need to reservere/manage memory for for these
	ITC interactions and instances of this record are useful
	for that purpose.
 */
struct GetOFFSET_X_REG_H_MMem {
	/**	This is memory enough for the response message.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Resp::Api::GetOFFSET_X_REG_H_MResp)>	resp;

	/**	This is memory enough for the message payload.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Req::Api::GetOFFSET_X_REG_H_MPayload)>	payload;

	/**	This operation creates a response message using this
		 block of memory.
	 */
	Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::GetOFFSET_X_REG_H_MResp&
		build(	Oscl::Driver::ST::LSM9DS1::MAG::Register::Req::Api::SAP&	sap,
				Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api&		respApi,
				Oscl::Mt::Itc::PostMsgApi&		clientPapi
				) noexcept;

	};

/**	This record describes a block of memory that is
	large enough (and properly aligned) to hold a
	SetOFFSET_X_REG_L_MResp and its corresponding Req::Api::SetOFFSET_X_REG_L_MPayload.
	Clients which use the corresponding server asynchronously
	frequently need to reservere/manage memory for for these
	ITC interactions and instances of this record are useful
	for that purpose.
 */
struct SetOFFSET_X_REG_L_MMem {
	/**	This is memory enough for the response message.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Resp::Api::SetOFFSET_X_REG_L_MResp)>	resp;

	/**	This is memory enough for the message payload.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Req::Api::SetOFFSET_X_REG_L_MPayload)>	payload;

	/**	This operation creates a response message using this
		 block of memory.
	 */
	Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::SetOFFSET_X_REG_L_MResp&
		build(	Oscl::Driver::ST::LSM9DS1::MAG::Register::Req::Api::SAP&	sap,
				Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api&		respApi,
				Oscl::Mt::Itc::PostMsgApi&		clientPapi,
				uint8_t	value
				) noexcept;

	};

/**	This record describes a block of memory that is
	large enough (and properly aligned) to hold a
	GetOFFSET_X_REG_L_MResp and its corresponding Req::Api::GetOFFSET_X_REG_L_MPayload.
	Clients which use the corresponding server asynchronously
	frequently need to reservere/manage memory for for these
	ITC interactions and instances of this record are useful
	for that purpose.
 */
struct GetOFFSET_X_REG_L_MMem {
	/**	This is memory enough for the response message.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Resp::Api::GetOFFSET_X_REG_L_MResp)>	resp;

	/**	This is memory enough for the message payload.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Req::Api::GetOFFSET_X_REG_L_MPayload)>	payload;

	/**	This operation creates a response message using this
		 block of memory.
	 */
	Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::GetOFFSET_X_REG_L_MResp&
		build(	Oscl::Driver::ST::LSM9DS1::MAG::Register::Req::Api::SAP&	sap,
				Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api&		respApi,
				Oscl::Mt::Itc::PostMsgApi&		clientPapi
				) noexcept;

	};

}
}
}
}
}
}
}
#endif
