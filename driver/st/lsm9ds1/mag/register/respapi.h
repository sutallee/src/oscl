/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_driver_st_lsm9ds1_mag_register_itc_respapih_
#define _oscl_driver_st_lsm9ds1_mag_register_itc_respapih_

#include "reqapi.h"
#include "oscl/mt/itc/mbox/clirsp.h"

/** */
namespace Oscl {

/** */
namespace Driver {

/** */
namespace ST {

/** */
namespace LSM9DS1 {

/** */
namespace MAG {

/** */
namespace Register {

/** */
namespace Resp {

/**	This class describes an ITC client interface. The
	interface includes the definition of client response messages
	based on their corresponding server request messages and
	associated payloads, as well as an interface that is to be
	implemented by a client that uses the corresponding server
	asynchronously.
 */
/**	This interface defines methods to access the LSM9DS1 IMU
	Magnetometer registers indpendent of the type of bus.
 */
class Api {
	public:
		/**	This describes a client response message that corresponds
			to the GetINT_THS_H_MReq message described in the "reqapi.h"
			header file. This GetINT_THS_H_MResp response message actually
			contains a GetINT_THS_H_MReq request message.
		 */
		typedef Oscl::Mt::Itc::CliResponse<
					Oscl::Driver::ST::LSM9DS1::MAG::Register::Req::Api,
					Api,
					Oscl::Driver::ST::LSM9DS1::MAG::Register::
					Req::Api::GetINT_THS_H_MPayload
					>		GetINT_THS_H_MResp;

	public:
		/**	This describes a client response message that corresponds
			to the GetINT_THS_L_MReq message described in the "reqapi.h"
			header file. This GetINT_THS_L_MResp response message actually
			contains a GetINT_THS_L_MReq request message.
		 */
		typedef Oscl::Mt::Itc::CliResponse<
					Oscl::Driver::ST::LSM9DS1::MAG::Register::Req::Api,
					Api,
					Oscl::Driver::ST::LSM9DS1::MAG::Register::
					Req::Api::GetINT_THS_L_MPayload
					>		GetINT_THS_L_MResp;

	public:
		/**	This describes a client response message that corresponds
			to the GetINT_SRC_MReq message described in the "reqapi.h"
			header file. This GetINT_SRC_MResp response message actually
			contains a GetINT_SRC_MReq request message.
		 */
		typedef Oscl::Mt::Itc::CliResponse<
					Oscl::Driver::ST::LSM9DS1::MAG::Register::Req::Api,
					Api,
					Oscl::Driver::ST::LSM9DS1::MAG::Register::
					Req::Api::GetINT_SRC_MPayload
					>		GetINT_SRC_MResp;

	public:
		/**	This describes a client response message that corresponds
			to the SetINT_CFG_MReq message described in the "reqapi.h"
			header file. This SetINT_CFG_MResp response message actually
			contains a SetINT_CFG_MReq request message.
		 */
		typedef Oscl::Mt::Itc::CliResponse<
					Oscl::Driver::ST::LSM9DS1::MAG::Register::Req::Api,
					Api,
					Oscl::Driver::ST::LSM9DS1::MAG::Register::
					Req::Api::SetINT_CFG_MPayload
					>		SetINT_CFG_MResp;

	public:
		/**	This describes a client response message that corresponds
			to the GetINT_CFG_MReq message described in the "reqapi.h"
			header file. This GetINT_CFG_MResp response message actually
			contains a GetINT_CFG_MReq request message.
		 */
		typedef Oscl::Mt::Itc::CliResponse<
					Oscl::Driver::ST::LSM9DS1::MAG::Register::Req::Api,
					Api,
					Oscl::Driver::ST::LSM9DS1::MAG::Register::
					Req::Api::GetINT_CFG_MPayload
					>		GetINT_CFG_MResp;

	public:
		/**	This describes a client response message that corresponds
			to the GetOUT_MReq message described in the "reqapi.h"
			header file. This GetOUT_MResp response message actually
			contains a GetOUT_MReq request message.
		 */
		typedef Oscl::Mt::Itc::CliResponse<
					Oscl::Driver::ST::LSM9DS1::MAG::Register::Req::Api,
					Api,
					Oscl::Driver::ST::LSM9DS1::MAG::Register::
					Req::Api::GetOUT_MPayload
					>		GetOUT_MResp;

	public:
		/**	This describes a client response message that corresponds
			to the GetOUT_Z_H_MReq message described in the "reqapi.h"
			header file. This GetOUT_Z_H_MResp response message actually
			contains a GetOUT_Z_H_MReq request message.
		 */
		typedef Oscl::Mt::Itc::CliResponse<
					Oscl::Driver::ST::LSM9DS1::MAG::Register::Req::Api,
					Api,
					Oscl::Driver::ST::LSM9DS1::MAG::Register::
					Req::Api::GetOUT_Z_H_MPayload
					>		GetOUT_Z_H_MResp;

	public:
		/**	This describes a client response message that corresponds
			to the GetOUT_Z_L_MReq message described in the "reqapi.h"
			header file. This GetOUT_Z_L_MResp response message actually
			contains a GetOUT_Z_L_MReq request message.
		 */
		typedef Oscl::Mt::Itc::CliResponse<
					Oscl::Driver::ST::LSM9DS1::MAG::Register::Req::Api,
					Api,
					Oscl::Driver::ST::LSM9DS1::MAG::Register::
					Req::Api::GetOUT_Z_L_MPayload
					>		GetOUT_Z_L_MResp;

	public:
		/**	This describes a client response message that corresponds
			to the GetOUT_Y_H_MReq message described in the "reqapi.h"
			header file. This GetOUT_Y_H_MResp response message actually
			contains a GetOUT_Y_H_MReq request message.
		 */
		typedef Oscl::Mt::Itc::CliResponse<
					Oscl::Driver::ST::LSM9DS1::MAG::Register::Req::Api,
					Api,
					Oscl::Driver::ST::LSM9DS1::MAG::Register::
					Req::Api::GetOUT_Y_H_MPayload
					>		GetOUT_Y_H_MResp;

	public:
		/**	This describes a client response message that corresponds
			to the GetOUT_Y_L_MReq message described in the "reqapi.h"
			header file. This GetOUT_Y_L_MResp response message actually
			contains a GetOUT_Y_L_MReq request message.
		 */
		typedef Oscl::Mt::Itc::CliResponse<
					Oscl::Driver::ST::LSM9DS1::MAG::Register::Req::Api,
					Api,
					Oscl::Driver::ST::LSM9DS1::MAG::Register::
					Req::Api::GetOUT_Y_L_MPayload
					>		GetOUT_Y_L_MResp;

	public:
		/**	This describes a client response message that corresponds
			to the GetOUT_X_H_MReq message described in the "reqapi.h"
			header file. This GetOUT_X_H_MResp response message actually
			contains a GetOUT_X_H_MReq request message.
		 */
		typedef Oscl::Mt::Itc::CliResponse<
					Oscl::Driver::ST::LSM9DS1::MAG::Register::Req::Api,
					Api,
					Oscl::Driver::ST::LSM9DS1::MAG::Register::
					Req::Api::GetOUT_X_H_MPayload
					>		GetOUT_X_H_MResp;

	public:
		/**	This describes a client response message that corresponds
			to the GetOUT_X_L_MReq message described in the "reqapi.h"
			header file. This GetOUT_X_L_MResp response message actually
			contains a GetOUT_X_L_MReq request message.
		 */
		typedef Oscl::Mt::Itc::CliResponse<
					Oscl::Driver::ST::LSM9DS1::MAG::Register::Req::Api,
					Api,
					Oscl::Driver::ST::LSM9DS1::MAG::Register::
					Req::Api::GetOUT_X_L_MPayload
					>		GetOUT_X_L_MResp;

	public:
		/**	This describes a client response message that corresponds
			to the GetSTATUS_REG_MReq message described in the "reqapi.h"
			header file. This GetSTATUS_REG_MResp response message actually
			contains a GetSTATUS_REG_MReq request message.
		 */
		typedef Oscl::Mt::Itc::CliResponse<
					Oscl::Driver::ST::LSM9DS1::MAG::Register::Req::Api,
					Api,
					Oscl::Driver::ST::LSM9DS1::MAG::Register::
					Req::Api::GetSTATUS_REG_MPayload
					>		GetSTATUS_REG_MResp;

	public:
		/**	This describes a client response message that corresponds
			to the SetCTRL_REG5_MReq message described in the "reqapi.h"
			header file. This SetCTRL_REG5_MResp response message actually
			contains a SetCTRL_REG5_MReq request message.
		 */
		typedef Oscl::Mt::Itc::CliResponse<
					Oscl::Driver::ST::LSM9DS1::MAG::Register::Req::Api,
					Api,
					Oscl::Driver::ST::LSM9DS1::MAG::Register::
					Req::Api::SetCTRL_REG5_MPayload
					>		SetCTRL_REG5_MResp;

	public:
		/**	This describes a client response message that corresponds
			to the GetCTRL_REG5_MReq message described in the "reqapi.h"
			header file. This GetCTRL_REG5_MResp response message actually
			contains a GetCTRL_REG5_MReq request message.
		 */
		typedef Oscl::Mt::Itc::CliResponse<
					Oscl::Driver::ST::LSM9DS1::MAG::Register::Req::Api,
					Api,
					Oscl::Driver::ST::LSM9DS1::MAG::Register::
					Req::Api::GetCTRL_REG5_MPayload
					>		GetCTRL_REG5_MResp;

	public:
		/**	This describes a client response message that corresponds
			to the SetCTRL_REG4_MReq message described in the "reqapi.h"
			header file. This SetCTRL_REG4_MResp response message actually
			contains a SetCTRL_REG4_MReq request message.
		 */
		typedef Oscl::Mt::Itc::CliResponse<
					Oscl::Driver::ST::LSM9DS1::MAG::Register::Req::Api,
					Api,
					Oscl::Driver::ST::LSM9DS1::MAG::Register::
					Req::Api::SetCTRL_REG4_MPayload
					>		SetCTRL_REG4_MResp;

	public:
		/**	This describes a client response message that corresponds
			to the GetCTRL_REG4_MReq message described in the "reqapi.h"
			header file. This GetCTRL_REG4_MResp response message actually
			contains a GetCTRL_REG4_MReq request message.
		 */
		typedef Oscl::Mt::Itc::CliResponse<
					Oscl::Driver::ST::LSM9DS1::MAG::Register::Req::Api,
					Api,
					Oscl::Driver::ST::LSM9DS1::MAG::Register::
					Req::Api::GetCTRL_REG4_MPayload
					>		GetCTRL_REG4_MResp;

	public:
		/**	This describes a client response message that corresponds
			to the SetCTRL_REG3_MReq message described in the "reqapi.h"
			header file. This SetCTRL_REG3_MResp response message actually
			contains a SetCTRL_REG3_MReq request message.
		 */
		typedef Oscl::Mt::Itc::CliResponse<
					Oscl::Driver::ST::LSM9DS1::MAG::Register::Req::Api,
					Api,
					Oscl::Driver::ST::LSM9DS1::MAG::Register::
					Req::Api::SetCTRL_REG3_MPayload
					>		SetCTRL_REG3_MResp;

	public:
		/**	This describes a client response message that corresponds
			to the GetCTRL_REG3_MReq message described in the "reqapi.h"
			header file. This GetCTRL_REG3_MResp response message actually
			contains a GetCTRL_REG3_MReq request message.
		 */
		typedef Oscl::Mt::Itc::CliResponse<
					Oscl::Driver::ST::LSM9DS1::MAG::Register::Req::Api,
					Api,
					Oscl::Driver::ST::LSM9DS1::MAG::Register::
					Req::Api::GetCTRL_REG3_MPayload
					>		GetCTRL_REG3_MResp;

	public:
		/**	This describes a client response message that corresponds
			to the SetCTRL_REG2_MReq message described in the "reqapi.h"
			header file. This SetCTRL_REG2_MResp response message actually
			contains a SetCTRL_REG2_MReq request message.
		 */
		typedef Oscl::Mt::Itc::CliResponse<
					Oscl::Driver::ST::LSM9DS1::MAG::Register::Req::Api,
					Api,
					Oscl::Driver::ST::LSM9DS1::MAG::Register::
					Req::Api::SetCTRL_REG2_MPayload
					>		SetCTRL_REG2_MResp;

	public:
		/**	This describes a client response message that corresponds
			to the GetCTRL_REG2_MReq message described in the "reqapi.h"
			header file. This GetCTRL_REG2_MResp response message actually
			contains a GetCTRL_REG2_MReq request message.
		 */
		typedef Oscl::Mt::Itc::CliResponse<
					Oscl::Driver::ST::LSM9DS1::MAG::Register::Req::Api,
					Api,
					Oscl::Driver::ST::LSM9DS1::MAG::Register::
					Req::Api::GetCTRL_REG2_MPayload
					>		GetCTRL_REG2_MResp;

	public:
		/**	This describes a client response message that corresponds
			to the SetCTRL_REG1_MReq message described in the "reqapi.h"
			header file. This SetCTRL_REG1_MResp response message actually
			contains a SetCTRL_REG1_MReq request message.
		 */
		typedef Oscl::Mt::Itc::CliResponse<
					Oscl::Driver::ST::LSM9DS1::MAG::Register::Req::Api,
					Api,
					Oscl::Driver::ST::LSM9DS1::MAG::Register::
					Req::Api::SetCTRL_REG1_MPayload
					>		SetCTRL_REG1_MResp;

	public:
		/**	This describes a client response message that corresponds
			to the GetCTRL_REG1_MReq message described in the "reqapi.h"
			header file. This GetCTRL_REG1_MResp response message actually
			contains a GetCTRL_REG1_MReq request message.
		 */
		typedef Oscl::Mt::Itc::CliResponse<
					Oscl::Driver::ST::LSM9DS1::MAG::Register::Req::Api,
					Api,
					Oscl::Driver::ST::LSM9DS1::MAG::Register::
					Req::Api::GetCTRL_REG1_MPayload
					>		GetCTRL_REG1_MResp;

	public:
		/**	This describes a client response message that corresponds
			to the GetWHO_AM_I_MReq message described in the "reqapi.h"
			header file. This GetWHO_AM_I_MResp response message actually
			contains a GetWHO_AM_I_MReq request message.
		 */
		typedef Oscl::Mt::Itc::CliResponse<
					Oscl::Driver::ST::LSM9DS1::MAG::Register::Req::Api,
					Api,
					Oscl::Driver::ST::LSM9DS1::MAG::Register::
					Req::Api::GetWHO_AM_I_MPayload
					>		GetWHO_AM_I_MResp;

	public:
		/**	This describes a client response message that corresponds
			to the SetOFFSET_Z_REG_H_MReq message described in the "reqapi.h"
			header file. This SetOFFSET_Z_REG_H_MResp response message actually
			contains a SetOFFSET_Z_REG_H_MReq request message.
		 */
		typedef Oscl::Mt::Itc::CliResponse<
					Oscl::Driver::ST::LSM9DS1::MAG::Register::Req::Api,
					Api,
					Oscl::Driver::ST::LSM9DS1::MAG::Register::
					Req::Api::SetOFFSET_Z_REG_H_MPayload
					>		SetOFFSET_Z_REG_H_MResp;

	public:
		/**	This describes a client response message that corresponds
			to the GetOFFSET_Z_REG_H_MReq message described in the "reqapi.h"
			header file. This GetOFFSET_Z_REG_H_MResp response message actually
			contains a GetOFFSET_Z_REG_H_MReq request message.
		 */
		typedef Oscl::Mt::Itc::CliResponse<
					Oscl::Driver::ST::LSM9DS1::MAG::Register::Req::Api,
					Api,
					Oscl::Driver::ST::LSM9DS1::MAG::Register::
					Req::Api::GetOFFSET_Z_REG_H_MPayload
					>		GetOFFSET_Z_REG_H_MResp;

	public:
		/**	This describes a client response message that corresponds
			to the SetOFFSET_Z_REG_L_MReq message described in the "reqapi.h"
			header file. This SetOFFSET_Z_REG_L_MResp response message actually
			contains a SetOFFSET_Z_REG_L_MReq request message.
		 */
		typedef Oscl::Mt::Itc::CliResponse<
					Oscl::Driver::ST::LSM9DS1::MAG::Register::Req::Api,
					Api,
					Oscl::Driver::ST::LSM9DS1::MAG::Register::
					Req::Api::SetOFFSET_Z_REG_L_MPayload
					>		SetOFFSET_Z_REG_L_MResp;

	public:
		/**	This describes a client response message that corresponds
			to the GetOFFSET_Z_REG_L_MReq message described in the "reqapi.h"
			header file. This GetOFFSET_Z_REG_L_MResp response message actually
			contains a GetOFFSET_Z_REG_L_MReq request message.
		 */
		typedef Oscl::Mt::Itc::CliResponse<
					Oscl::Driver::ST::LSM9DS1::MAG::Register::Req::Api,
					Api,
					Oscl::Driver::ST::LSM9DS1::MAG::Register::
					Req::Api::GetOFFSET_Z_REG_L_MPayload
					>		GetOFFSET_Z_REG_L_MResp;

	public:
		/**	This describes a client response message that corresponds
			to the SetOFFSET_Y_REG_H_MReq message described in the "reqapi.h"
			header file. This SetOFFSET_Y_REG_H_MResp response message actually
			contains a SetOFFSET_Y_REG_H_MReq request message.
		 */
		typedef Oscl::Mt::Itc::CliResponse<
					Oscl::Driver::ST::LSM9DS1::MAG::Register::Req::Api,
					Api,
					Oscl::Driver::ST::LSM9DS1::MAG::Register::
					Req::Api::SetOFFSET_Y_REG_H_MPayload
					>		SetOFFSET_Y_REG_H_MResp;

	public:
		/**	This describes a client response message that corresponds
			to the GetOFFSET_Y_REG_H_MReq message described in the "reqapi.h"
			header file. This GetOFFSET_Y_REG_H_MResp response message actually
			contains a GetOFFSET_Y_REG_H_MReq request message.
		 */
		typedef Oscl::Mt::Itc::CliResponse<
					Oscl::Driver::ST::LSM9DS1::MAG::Register::Req::Api,
					Api,
					Oscl::Driver::ST::LSM9DS1::MAG::Register::
					Req::Api::GetOFFSET_Y_REG_H_MPayload
					>		GetOFFSET_Y_REG_H_MResp;

	public:
		/**	This describes a client response message that corresponds
			to the SetOFFSET_Y_REG_L_MReq message described in the "reqapi.h"
			header file. This SetOFFSET_Y_REG_L_MResp response message actually
			contains a SetOFFSET_Y_REG_L_MReq request message.
		 */
		typedef Oscl::Mt::Itc::CliResponse<
					Oscl::Driver::ST::LSM9DS1::MAG::Register::Req::Api,
					Api,
					Oscl::Driver::ST::LSM9DS1::MAG::Register::
					Req::Api::SetOFFSET_Y_REG_L_MPayload
					>		SetOFFSET_Y_REG_L_MResp;

	public:
		/**	This describes a client response message that corresponds
			to the GetOFFSET_Y_REG_L_MReq message described in the "reqapi.h"
			header file. This GetOFFSET_Y_REG_L_MResp response message actually
			contains a GetOFFSET_Y_REG_L_MReq request message.
		 */
		typedef Oscl::Mt::Itc::CliResponse<
					Oscl::Driver::ST::LSM9DS1::MAG::Register::Req::Api,
					Api,
					Oscl::Driver::ST::LSM9DS1::MAG::Register::
					Req::Api::GetOFFSET_Y_REG_L_MPayload
					>		GetOFFSET_Y_REG_L_MResp;

	public:
		/**	This describes a client response message that corresponds
			to the SetOFFSET_X_REG_H_MReq message described in the "reqapi.h"
			header file. This SetOFFSET_X_REG_H_MResp response message actually
			contains a SetOFFSET_X_REG_H_MReq request message.
		 */
		typedef Oscl::Mt::Itc::CliResponse<
					Oscl::Driver::ST::LSM9DS1::MAG::Register::Req::Api,
					Api,
					Oscl::Driver::ST::LSM9DS1::MAG::Register::
					Req::Api::SetOFFSET_X_REG_H_MPayload
					>		SetOFFSET_X_REG_H_MResp;

	public:
		/**	This describes a client response message that corresponds
			to the GetOFFSET_X_REG_H_MReq message described in the "reqapi.h"
			header file. This GetOFFSET_X_REG_H_MResp response message actually
			contains a GetOFFSET_X_REG_H_MReq request message.
		 */
		typedef Oscl::Mt::Itc::CliResponse<
					Oscl::Driver::ST::LSM9DS1::MAG::Register::Req::Api,
					Api,
					Oscl::Driver::ST::LSM9DS1::MAG::Register::
					Req::Api::GetOFFSET_X_REG_H_MPayload
					>		GetOFFSET_X_REG_H_MResp;

	public:
		/**	This describes a client response message that corresponds
			to the SetOFFSET_X_REG_L_MReq message described in the "reqapi.h"
			header file. This SetOFFSET_X_REG_L_MResp response message actually
			contains a SetOFFSET_X_REG_L_MReq request message.
		 */
		typedef Oscl::Mt::Itc::CliResponse<
					Oscl::Driver::ST::LSM9DS1::MAG::Register::Req::Api,
					Api,
					Oscl::Driver::ST::LSM9DS1::MAG::Register::
					Req::Api::SetOFFSET_X_REG_L_MPayload
					>		SetOFFSET_X_REG_L_MResp;

	public:
		/**	This describes a client response message that corresponds
			to the GetOFFSET_X_REG_L_MReq message described in the "reqapi.h"
			header file. This GetOFFSET_X_REG_L_MResp response message actually
			contains a GetOFFSET_X_REG_L_MReq request message.
		 */
		typedef Oscl::Mt::Itc::CliResponse<
					Oscl::Driver::ST::LSM9DS1::MAG::Register::Req::Api,
					Api,
					Oscl::Driver::ST::LSM9DS1::MAG::Register::
					Req::Api::GetOFFSET_X_REG_L_MPayload
					>		GetOFFSET_X_REG_L_MResp;

	public:
		/** Make the compiler happy. */
		virtual ~Api(){}

		/** This operation is invoked within the client thread
			whenever a GetINT_THS_H_MResp message is received.
		 */
		virtual void	response(	Oscl::Driver::ST::LSM9DS1::MAG::Register::
									Resp::Api::GetINT_THS_H_MResp& msg
									) noexcept=0;

		/** This operation is invoked within the client thread
			whenever a GetINT_THS_L_MResp message is received.
		 */
		virtual void	response(	Oscl::Driver::ST::LSM9DS1::MAG::Register::
									Resp::Api::GetINT_THS_L_MResp& msg
									) noexcept=0;

		/** This operation is invoked within the client thread
			whenever a GetINT_SRC_MResp message is received.
		 */
		virtual void	response(	Oscl::Driver::ST::LSM9DS1::MAG::Register::
									Resp::Api::GetINT_SRC_MResp& msg
									) noexcept=0;

		/** This operation is invoked within the client thread
			whenever a SetINT_CFG_MResp message is received.
		 */
		virtual void	response(	Oscl::Driver::ST::LSM9DS1::MAG::Register::
									Resp::Api::SetINT_CFG_MResp& msg
									) noexcept=0;

		/** This operation is invoked within the client thread
			whenever a GetINT_CFG_MResp message is received.
		 */
		virtual void	response(	Oscl::Driver::ST::LSM9DS1::MAG::Register::
									Resp::Api::GetINT_CFG_MResp& msg
									) noexcept=0;

		/** This operation is invoked within the client thread
			whenever a GetOUT_MResp message is received.
		 */
		virtual void	response(	Oscl::Driver::ST::LSM9DS1::MAG::Register::
									Resp::Api::GetOUT_MResp& msg
									) noexcept=0;

		/** This operation is invoked within the client thread
			whenever a GetOUT_Z_H_MResp message is received.
		 */
		virtual void	response(	Oscl::Driver::ST::LSM9DS1::MAG::Register::
									Resp::Api::GetOUT_Z_H_MResp& msg
									) noexcept=0;

		/** This operation is invoked within the client thread
			whenever a GetOUT_Z_L_MResp message is received.
		 */
		virtual void	response(	Oscl::Driver::ST::LSM9DS1::MAG::Register::
									Resp::Api::GetOUT_Z_L_MResp& msg
									) noexcept=0;

		/** This operation is invoked within the client thread
			whenever a GetOUT_Y_H_MResp message is received.
		 */
		virtual void	response(	Oscl::Driver::ST::LSM9DS1::MAG::Register::
									Resp::Api::GetOUT_Y_H_MResp& msg
									) noexcept=0;

		/** This operation is invoked within the client thread
			whenever a GetOUT_Y_L_MResp message is received.
		 */
		virtual void	response(	Oscl::Driver::ST::LSM9DS1::MAG::Register::
									Resp::Api::GetOUT_Y_L_MResp& msg
									) noexcept=0;

		/** This operation is invoked within the client thread
			whenever a GetOUT_X_H_MResp message is received.
		 */
		virtual void	response(	Oscl::Driver::ST::LSM9DS1::MAG::Register::
									Resp::Api::GetOUT_X_H_MResp& msg
									) noexcept=0;

		/** This operation is invoked within the client thread
			whenever a GetOUT_X_L_MResp message is received.
		 */
		virtual void	response(	Oscl::Driver::ST::LSM9DS1::MAG::Register::
									Resp::Api::GetOUT_X_L_MResp& msg
									) noexcept=0;

		/** This operation is invoked within the client thread
			whenever a GetSTATUS_REG_MResp message is received.
		 */
		virtual void	response(	Oscl::Driver::ST::LSM9DS1::MAG::Register::
									Resp::Api::GetSTATUS_REG_MResp& msg
									) noexcept=0;

		/** This operation is invoked within the client thread
			whenever a SetCTRL_REG5_MResp message is received.
		 */
		virtual void	response(	Oscl::Driver::ST::LSM9DS1::MAG::Register::
									Resp::Api::SetCTRL_REG5_MResp& msg
									) noexcept=0;

		/** This operation is invoked within the client thread
			whenever a GetCTRL_REG5_MResp message is received.
		 */
		virtual void	response(	Oscl::Driver::ST::LSM9DS1::MAG::Register::
									Resp::Api::GetCTRL_REG5_MResp& msg
									) noexcept=0;

		/** This operation is invoked within the client thread
			whenever a SetCTRL_REG4_MResp message is received.
		 */
		virtual void	response(	Oscl::Driver::ST::LSM9DS1::MAG::Register::
									Resp::Api::SetCTRL_REG4_MResp& msg
									) noexcept=0;

		/** This operation is invoked within the client thread
			whenever a GetCTRL_REG4_MResp message is received.
		 */
		virtual void	response(	Oscl::Driver::ST::LSM9DS1::MAG::Register::
									Resp::Api::GetCTRL_REG4_MResp& msg
									) noexcept=0;

		/** This operation is invoked within the client thread
			whenever a SetCTRL_REG3_MResp message is received.
		 */
		virtual void	response(	Oscl::Driver::ST::LSM9DS1::MAG::Register::
									Resp::Api::SetCTRL_REG3_MResp& msg
									) noexcept=0;

		/** This operation is invoked within the client thread
			whenever a GetCTRL_REG3_MResp message is received.
		 */
		virtual void	response(	Oscl::Driver::ST::LSM9DS1::MAG::Register::
									Resp::Api::GetCTRL_REG3_MResp& msg
									) noexcept=0;

		/** This operation is invoked within the client thread
			whenever a SetCTRL_REG2_MResp message is received.
		 */
		virtual void	response(	Oscl::Driver::ST::LSM9DS1::MAG::Register::
									Resp::Api::SetCTRL_REG2_MResp& msg
									) noexcept=0;

		/** This operation is invoked within the client thread
			whenever a GetCTRL_REG2_MResp message is received.
		 */
		virtual void	response(	Oscl::Driver::ST::LSM9DS1::MAG::Register::
									Resp::Api::GetCTRL_REG2_MResp& msg
									) noexcept=0;

		/** This operation is invoked within the client thread
			whenever a SetCTRL_REG1_MResp message is received.
		 */
		virtual void	response(	Oscl::Driver::ST::LSM9DS1::MAG::Register::
									Resp::Api::SetCTRL_REG1_MResp& msg
									) noexcept=0;

		/** This operation is invoked within the client thread
			whenever a GetCTRL_REG1_MResp message is received.
		 */
		virtual void	response(	Oscl::Driver::ST::LSM9DS1::MAG::Register::
									Resp::Api::GetCTRL_REG1_MResp& msg
									) noexcept=0;

		/** This operation is invoked within the client thread
			whenever a GetWHO_AM_I_MResp message is received.
		 */
		virtual void	response(	Oscl::Driver::ST::LSM9DS1::MAG::Register::
									Resp::Api::GetWHO_AM_I_MResp& msg
									) noexcept=0;

		/** This operation is invoked within the client thread
			whenever a SetOFFSET_Z_REG_H_MResp message is received.
		 */
		virtual void	response(	Oscl::Driver::ST::LSM9DS1::MAG::Register::
									Resp::Api::SetOFFSET_Z_REG_H_MResp& msg
									) noexcept=0;

		/** This operation is invoked within the client thread
			whenever a GetOFFSET_Z_REG_H_MResp message is received.
		 */
		virtual void	response(	Oscl::Driver::ST::LSM9DS1::MAG::Register::
									Resp::Api::GetOFFSET_Z_REG_H_MResp& msg
									) noexcept=0;

		/** This operation is invoked within the client thread
			whenever a SetOFFSET_Z_REG_L_MResp message is received.
		 */
		virtual void	response(	Oscl::Driver::ST::LSM9DS1::MAG::Register::
									Resp::Api::SetOFFSET_Z_REG_L_MResp& msg
									) noexcept=0;

		/** This operation is invoked within the client thread
			whenever a GetOFFSET_Z_REG_L_MResp message is received.
		 */
		virtual void	response(	Oscl::Driver::ST::LSM9DS1::MAG::Register::
									Resp::Api::GetOFFSET_Z_REG_L_MResp& msg
									) noexcept=0;

		/** This operation is invoked within the client thread
			whenever a SetOFFSET_Y_REG_H_MResp message is received.
		 */
		virtual void	response(	Oscl::Driver::ST::LSM9DS1::MAG::Register::
									Resp::Api::SetOFFSET_Y_REG_H_MResp& msg
									) noexcept=0;

		/** This operation is invoked within the client thread
			whenever a GetOFFSET_Y_REG_H_MResp message is received.
		 */
		virtual void	response(	Oscl::Driver::ST::LSM9DS1::MAG::Register::
									Resp::Api::GetOFFSET_Y_REG_H_MResp& msg
									) noexcept=0;

		/** This operation is invoked within the client thread
			whenever a SetOFFSET_Y_REG_L_MResp message is received.
		 */
		virtual void	response(	Oscl::Driver::ST::LSM9DS1::MAG::Register::
									Resp::Api::SetOFFSET_Y_REG_L_MResp& msg
									) noexcept=0;

		/** This operation is invoked within the client thread
			whenever a GetOFFSET_Y_REG_L_MResp message is received.
		 */
		virtual void	response(	Oscl::Driver::ST::LSM9DS1::MAG::Register::
									Resp::Api::GetOFFSET_Y_REG_L_MResp& msg
									) noexcept=0;

		/** This operation is invoked within the client thread
			whenever a SetOFFSET_X_REG_H_MResp message is received.
		 */
		virtual void	response(	Oscl::Driver::ST::LSM9DS1::MAG::Register::
									Resp::Api::SetOFFSET_X_REG_H_MResp& msg
									) noexcept=0;

		/** This operation is invoked within the client thread
			whenever a GetOFFSET_X_REG_H_MResp message is received.
		 */
		virtual void	response(	Oscl::Driver::ST::LSM9DS1::MAG::Register::
									Resp::Api::GetOFFSET_X_REG_H_MResp& msg
									) noexcept=0;

		/** This operation is invoked within the client thread
			whenever a SetOFFSET_X_REG_L_MResp message is received.
		 */
		virtual void	response(	Oscl::Driver::ST::LSM9DS1::MAG::Register::
									Resp::Api::SetOFFSET_X_REG_L_MResp& msg
									) noexcept=0;

		/** This operation is invoked within the client thread
			whenever a GetOFFSET_X_REG_L_MResp message is received.
		 */
		virtual void	response(	Oscl::Driver::ST::LSM9DS1::MAG::Register::
									Resp::Api::GetOFFSET_X_REG_L_MResp& msg
									) noexcept=0;

	};

}
}
}
}
}
}
}
#endif
