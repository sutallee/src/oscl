/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_driver_st_lsm9ds1_mag_register_itc_reqapih_
#define _oscl_driver_st_lsm9ds1_mag_register_itc_reqapih_

#include "oscl/mt/itc/mbox/srvreq.h"
#include "oscl/mt/itc/postmsgapi.h"
#include "oscl/mt/itc/mbox/sap.h"
#include <stdint.h>

/** */
namespace Oscl {

/** */
namespace Driver {

/** */
namespace ST {

/** */
namespace LSM9DS1 {

/** */
namespace MAG {

/** */
namespace Register {

/** */
namespace Req {

/**	This class describes an ITC server interface. The
	interface includes the definition of messages and their
	associated payloads, as well as an interface that is to be
	implemented by a server that supports this ITC interface.
 */
/**	This interface defines methods to access the LSM9DS1 IMU
	Magnetometer registers indpendent of the type of bus.
 */
class Api {
	public:
		/** This class contains the data that is transfered
			between the client and server within the GetINT_THS_H_MReq
			ITC message.
		 */
		class GetINT_THS_H_MPayload {
			public:
				/**	
				 */
				uint8_t	_value;

			public:
				/** The GetINT_THS_H_MPayload constructor. */
				GetINT_THS_H_MPayload(
					) noexcept;

			};

		/**	Read the INT_THS_H_M register.
		 */
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::Driver::ST::LSM9DS1::MAG::Register::Req::Api,
					GetINT_THS_H_MPayload
					>		GetINT_THS_H_MReq;

	public:
		/** This class contains the data that is transfered
			between the client and server within the GetINT_THS_L_MReq
			ITC message.
		 */
		class GetINT_THS_L_MPayload {
			public:
				/**	
				 */
				uint8_t	_value;

			public:
				/** The GetINT_THS_L_MPayload constructor. */
				GetINT_THS_L_MPayload(
					) noexcept;

			};

		/**	Read the INT_THS_L_M register.
		 */
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::Driver::ST::LSM9DS1::MAG::Register::Req::Api,
					GetINT_THS_L_MPayload
					>		GetINT_THS_L_MReq;

	public:
		/** This class contains the data that is transfered
			between the client and server within the GetINT_SRC_MReq
			ITC message.
		 */
		class GetINT_SRC_MPayload {
			public:
				/**	
				 */
				uint8_t	_value;

			public:
				/** The GetINT_SRC_MPayload constructor. */
				GetINT_SRC_MPayload(
					) noexcept;

			};

		/**	Read the INT_SRC_M register.
		 */
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::Driver::ST::LSM9DS1::MAG::Register::Req::Api,
					GetINT_SRC_MPayload
					>		GetINT_SRC_MReq;

	public:
		/** This class contains the data that is transfered
			between the client and server within the SetINT_CFG_MReq
			ITC message.
		 */
		class SetINT_CFG_MPayload {
			public:
				/**	
				 */
				uint8_t	_value;

			public:
				/** The SetINT_CFG_MPayload constructor. */
				SetINT_CFG_MPayload(

					uint8_t	value
					) noexcept;

			};

		/**	Write the INT_CFG_M register
		 */
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::Driver::ST::LSM9DS1::MAG::Register::Req::Api,
					SetINT_CFG_MPayload
					>		SetINT_CFG_MReq;

	public:
		/** This class contains the data that is transfered
			between the client and server within the GetINT_CFG_MReq
			ITC message.
		 */
		class GetINT_CFG_MPayload {
			public:
				/**	
				 */
				uint8_t	_value;

			public:
				/** The GetINT_CFG_MPayload constructor. */
				GetINT_CFG_MPayload(
					) noexcept;

			};

		/**	Read the INT_CFG_M register.
		 */
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::Driver::ST::LSM9DS1::MAG::Register::Req::Api,
					GetINT_CFG_MPayload
					>		GetINT_CFG_MReq;

	public:
		/** This class contains the data that is transfered
			between the client and server within the GetOUT_MReq
			ITC message.
		 */
		class GetOUT_MPayload {
			public:
				/**	
				 */
				uint8_t	_maxBufferLength;

				/**	
				 */
				void*	_buffer;

			public:
				/** The GetOUT_MPayload constructor. */
				GetOUT_MPayload(

					uint8_t	maxBufferLength,
					void*	buffer
					) noexcept;

			};

		/**	Read the OUT_<axis>_<octet>_M registers.
		 */
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::Driver::ST::LSM9DS1::MAG::Register::Req::Api,
					GetOUT_MPayload
					>		GetOUT_MReq;

	public:
		/** This class contains the data that is transfered
			between the client and server within the GetOUT_Z_H_MReq
			ITC message.
		 */
		class GetOUT_Z_H_MPayload {
			public:
				/**	
				 */
				uint8_t	_value;

			public:
				/** The GetOUT_Z_H_MPayload constructor. */
				GetOUT_Z_H_MPayload(
					) noexcept;

			};

		/**	Read the OUT_Z_H_M register.
		 */
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::Driver::ST::LSM9DS1::MAG::Register::Req::Api,
					GetOUT_Z_H_MPayload
					>		GetOUT_Z_H_MReq;

	public:
		/** This class contains the data that is transfered
			between the client and server within the GetOUT_Z_L_MReq
			ITC message.
		 */
		class GetOUT_Z_L_MPayload {
			public:
				/**	
				 */
				uint8_t	_value;

			public:
				/** The GetOUT_Z_L_MPayload constructor. */
				GetOUT_Z_L_MPayload(
					) noexcept;

			};

		/**	Read the OUT_Z_L_M register.
		 */
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::Driver::ST::LSM9DS1::MAG::Register::Req::Api,
					GetOUT_Z_L_MPayload
					>		GetOUT_Z_L_MReq;

	public:
		/** This class contains the data that is transfered
			between the client and server within the GetOUT_Y_H_MReq
			ITC message.
		 */
		class GetOUT_Y_H_MPayload {
			public:
				/**	
				 */
				uint8_t	_value;

			public:
				/** The GetOUT_Y_H_MPayload constructor. */
				GetOUT_Y_H_MPayload(
					) noexcept;

			};

		/**	Read the OUT_Y_H_M register.
		 */
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::Driver::ST::LSM9DS1::MAG::Register::Req::Api,
					GetOUT_Y_H_MPayload
					>		GetOUT_Y_H_MReq;

	public:
		/** This class contains the data that is transfered
			between the client and server within the GetOUT_Y_L_MReq
			ITC message.
		 */
		class GetOUT_Y_L_MPayload {
			public:
				/**	
				 */
				uint8_t	_value;

			public:
				/** The GetOUT_Y_L_MPayload constructor. */
				GetOUT_Y_L_MPayload(
					) noexcept;

			};

		/**	Read the OUT_Y_L_M register.
		 */
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::Driver::ST::LSM9DS1::MAG::Register::Req::Api,
					GetOUT_Y_L_MPayload
					>		GetOUT_Y_L_MReq;

	public:
		/** This class contains the data that is transfered
			between the client and server within the GetOUT_X_H_MReq
			ITC message.
		 */
		class GetOUT_X_H_MPayload {
			public:
				/**	
				 */
				uint8_t	_value;

			public:
				/** The GetOUT_X_H_MPayload constructor. */
				GetOUT_X_H_MPayload(
					) noexcept;

			};

		/**	Read the OUT_X_H_M register.
		 */
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::Driver::ST::LSM9DS1::MAG::Register::Req::Api,
					GetOUT_X_H_MPayload
					>		GetOUT_X_H_MReq;

	public:
		/** This class contains the data that is transfered
			between the client and server within the GetOUT_X_L_MReq
			ITC message.
		 */
		class GetOUT_X_L_MPayload {
			public:
				/**	
				 */
				uint8_t	_value;

			public:
				/** The GetOUT_X_L_MPayload constructor. */
				GetOUT_X_L_MPayload(
					) noexcept;

			};

		/**	Read the OUT_X_L_M register.
		 */
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::Driver::ST::LSM9DS1::MAG::Register::Req::Api,
					GetOUT_X_L_MPayload
					>		GetOUT_X_L_MReq;

	public:
		/** This class contains the data that is transfered
			between the client and server within the GetSTATUS_REG_MReq
			ITC message.
		 */
		class GetSTATUS_REG_MPayload {
			public:
				/**	
				 */
				uint8_t	_value;

			public:
				/** The GetSTATUS_REG_MPayload constructor. */
				GetSTATUS_REG_MPayload(
					) noexcept;

			};

		/**	Read the STATUS_REG_M register.
		 */
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::Driver::ST::LSM9DS1::MAG::Register::Req::Api,
					GetSTATUS_REG_MPayload
					>		GetSTATUS_REG_MReq;

	public:
		/** This class contains the data that is transfered
			between the client and server within the SetCTRL_REG5_MReq
			ITC message.
		 */
		class SetCTRL_REG5_MPayload {
			public:
				/**	
				 */
				uint8_t	_value;

			public:
				/** The SetCTRL_REG5_MPayload constructor. */
				SetCTRL_REG5_MPayload(

					uint8_t	value
					) noexcept;

			};

		/**	Write the CTRL_REG5_M register
		 */
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::Driver::ST::LSM9DS1::MAG::Register::Req::Api,
					SetCTRL_REG5_MPayload
					>		SetCTRL_REG5_MReq;

	public:
		/** This class contains the data that is transfered
			between the client and server within the GetCTRL_REG5_MReq
			ITC message.
		 */
		class GetCTRL_REG5_MPayload {
			public:
				/**	
				 */
				uint8_t	_value;

			public:
				/** The GetCTRL_REG5_MPayload constructor. */
				GetCTRL_REG5_MPayload(
					) noexcept;

			};

		/**	Read the CTRL_REG5_M register.
		 */
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::Driver::ST::LSM9DS1::MAG::Register::Req::Api,
					GetCTRL_REG5_MPayload
					>		GetCTRL_REG5_MReq;

	public:
		/** This class contains the data that is transfered
			between the client and server within the SetCTRL_REG4_MReq
			ITC message.
		 */
		class SetCTRL_REG4_MPayload {
			public:
				/**	
				 */
				uint8_t	_value;

			public:
				/** The SetCTRL_REG4_MPayload constructor. */
				SetCTRL_REG4_MPayload(

					uint8_t	value
					) noexcept;

			};

		/**	Write the CTRL_REG4_M register
		 */
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::Driver::ST::LSM9DS1::MAG::Register::Req::Api,
					SetCTRL_REG4_MPayload
					>		SetCTRL_REG4_MReq;

	public:
		/** This class contains the data that is transfered
			between the client and server within the GetCTRL_REG4_MReq
			ITC message.
		 */
		class GetCTRL_REG4_MPayload {
			public:
				/**	
				 */
				uint8_t	_value;

			public:
				/** The GetCTRL_REG4_MPayload constructor. */
				GetCTRL_REG4_MPayload(
					) noexcept;

			};

		/**	Read the CTRL_REG4_M register.
		 */
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::Driver::ST::LSM9DS1::MAG::Register::Req::Api,
					GetCTRL_REG4_MPayload
					>		GetCTRL_REG4_MReq;

	public:
		/** This class contains the data that is transfered
			between the client and server within the SetCTRL_REG3_MReq
			ITC message.
		 */
		class SetCTRL_REG3_MPayload {
			public:
				/**	
				 */
				uint8_t	_value;

			public:
				/** The SetCTRL_REG3_MPayload constructor. */
				SetCTRL_REG3_MPayload(

					uint8_t	value
					) noexcept;

			};

		/**	Write the CTRL_REG3_M register
		 */
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::Driver::ST::LSM9DS1::MAG::Register::Req::Api,
					SetCTRL_REG3_MPayload
					>		SetCTRL_REG3_MReq;

	public:
		/** This class contains the data that is transfered
			between the client and server within the GetCTRL_REG3_MReq
			ITC message.
		 */
		class GetCTRL_REG3_MPayload {
			public:
				/**	
				 */
				uint8_t	_value;

			public:
				/** The GetCTRL_REG3_MPayload constructor. */
				GetCTRL_REG3_MPayload(
					) noexcept;

			};

		/**	Read the CTRL_REG3_M register.
		 */
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::Driver::ST::LSM9DS1::MAG::Register::Req::Api,
					GetCTRL_REG3_MPayload
					>		GetCTRL_REG3_MReq;

	public:
		/** This class contains the data that is transfered
			between the client and server within the SetCTRL_REG2_MReq
			ITC message.
		 */
		class SetCTRL_REG2_MPayload {
			public:
				/**	
				 */
				uint8_t	_value;

			public:
				/** The SetCTRL_REG2_MPayload constructor. */
				SetCTRL_REG2_MPayload(

					uint8_t	value
					) noexcept;

			};

		/**	Write the CTRL_REG2_M register
		 */
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::Driver::ST::LSM9DS1::MAG::Register::Req::Api,
					SetCTRL_REG2_MPayload
					>		SetCTRL_REG2_MReq;

	public:
		/** This class contains the data that is transfered
			between the client and server within the GetCTRL_REG2_MReq
			ITC message.
		 */
		class GetCTRL_REG2_MPayload {
			public:
				/**	
				 */
				uint8_t	_value;

			public:
				/** The GetCTRL_REG2_MPayload constructor. */
				GetCTRL_REG2_MPayload(
					) noexcept;

			};

		/**	Read the CTRL_REG2_M register.
		 */
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::Driver::ST::LSM9DS1::MAG::Register::Req::Api,
					GetCTRL_REG2_MPayload
					>		GetCTRL_REG2_MReq;

	public:
		/** This class contains the data that is transfered
			between the client and server within the SetCTRL_REG1_MReq
			ITC message.
		 */
		class SetCTRL_REG1_MPayload {
			public:
				/**	
				 */
				uint8_t	_value;

			public:
				/** The SetCTRL_REG1_MPayload constructor. */
				SetCTRL_REG1_MPayload(

					uint8_t	value
					) noexcept;

			};

		/**	Write the CTRL_REG1_M register
		 */
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::Driver::ST::LSM9DS1::MAG::Register::Req::Api,
					SetCTRL_REG1_MPayload
					>		SetCTRL_REG1_MReq;

	public:
		/** This class contains the data that is transfered
			between the client and server within the GetCTRL_REG1_MReq
			ITC message.
		 */
		class GetCTRL_REG1_MPayload {
			public:
				/**	
				 */
				uint8_t	_value;

			public:
				/** The GetCTRL_REG1_MPayload constructor. */
				GetCTRL_REG1_MPayload(
					) noexcept;

			};

		/**	Read the CTRL_REG1_M register.
		 */
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::Driver::ST::LSM9DS1::MAG::Register::Req::Api,
					GetCTRL_REG1_MPayload
					>		GetCTRL_REG1_MReq;

	public:
		/** This class contains the data that is transfered
			between the client and server within the GetWHO_AM_I_MReq
			ITC message.
		 */
		class GetWHO_AM_I_MPayload {
			public:
				/**	
				 */
				uint8_t	_value;

			public:
				/** The GetWHO_AM_I_MPayload constructor. */
				GetWHO_AM_I_MPayload(
					) noexcept;

			};

		/**	Read the WHO_AM_I_M register.
		 */
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::Driver::ST::LSM9DS1::MAG::Register::Req::Api,
					GetWHO_AM_I_MPayload
					>		GetWHO_AM_I_MReq;

	public:
		/** This class contains the data that is transfered
			between the client and server within the SetOFFSET_Z_REG_H_MReq
			ITC message.
		 */
		class SetOFFSET_Z_REG_H_MPayload {
			public:
				/**	
				 */
				uint8_t	_value;

			public:
				/** The SetOFFSET_Z_REG_H_MPayload constructor. */
				SetOFFSET_Z_REG_H_MPayload(

					uint8_t	value
					) noexcept;

			};

		/**	Write the OFFSET_Z_REG_H_M register
		 */
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::Driver::ST::LSM9DS1::MAG::Register::Req::Api,
					SetOFFSET_Z_REG_H_MPayload
					>		SetOFFSET_Z_REG_H_MReq;

	public:
		/** This class contains the data that is transfered
			between the client and server within the GetOFFSET_Z_REG_H_MReq
			ITC message.
		 */
		class GetOFFSET_Z_REG_H_MPayload {
			public:
				/**	
				 */
				uint8_t	_value;

			public:
				/** The GetOFFSET_Z_REG_H_MPayload constructor. */
				GetOFFSET_Z_REG_H_MPayload(
					) noexcept;

			};

		/**	Read the OFFSET_Z_REG_H_M register.
		 */
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::Driver::ST::LSM9DS1::MAG::Register::Req::Api,
					GetOFFSET_Z_REG_H_MPayload
					>		GetOFFSET_Z_REG_H_MReq;

	public:
		/** This class contains the data that is transfered
			between the client and server within the SetOFFSET_Z_REG_L_MReq
			ITC message.
		 */
		class SetOFFSET_Z_REG_L_MPayload {
			public:
				/**	
				 */
				uint8_t	_value;

			public:
				/** The SetOFFSET_Z_REG_L_MPayload constructor. */
				SetOFFSET_Z_REG_L_MPayload(

					uint8_t	value
					) noexcept;

			};

		/**	Write the OFFSET_Z_REG_L_M register
		 */
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::Driver::ST::LSM9DS1::MAG::Register::Req::Api,
					SetOFFSET_Z_REG_L_MPayload
					>		SetOFFSET_Z_REG_L_MReq;

	public:
		/** This class contains the data that is transfered
			between the client and server within the GetOFFSET_Z_REG_L_MReq
			ITC message.
		 */
		class GetOFFSET_Z_REG_L_MPayload {
			public:
				/**	
				 */
				uint8_t	_value;

			public:
				/** The GetOFFSET_Z_REG_L_MPayload constructor. */
				GetOFFSET_Z_REG_L_MPayload(
					) noexcept;

			};

		/**	Read the OFFSET_Z_REG_L_M register.
		 */
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::Driver::ST::LSM9DS1::MAG::Register::Req::Api,
					GetOFFSET_Z_REG_L_MPayload
					>		GetOFFSET_Z_REG_L_MReq;

	public:
		/** This class contains the data that is transfered
			between the client and server within the SetOFFSET_Y_REG_H_MReq
			ITC message.
		 */
		class SetOFFSET_Y_REG_H_MPayload {
			public:
				/**	
				 */
				uint8_t	_value;

			public:
				/** The SetOFFSET_Y_REG_H_MPayload constructor. */
				SetOFFSET_Y_REG_H_MPayload(

					uint8_t	value
					) noexcept;

			};

		/**	Write the OFFSET_Y_REG_H_M register
		 */
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::Driver::ST::LSM9DS1::MAG::Register::Req::Api,
					SetOFFSET_Y_REG_H_MPayload
					>		SetOFFSET_Y_REG_H_MReq;

	public:
		/** This class contains the data that is transfered
			between the client and server within the GetOFFSET_Y_REG_H_MReq
			ITC message.
		 */
		class GetOFFSET_Y_REG_H_MPayload {
			public:
				/**	
				 */
				uint8_t	_value;

			public:
				/** The GetOFFSET_Y_REG_H_MPayload constructor. */
				GetOFFSET_Y_REG_H_MPayload(
					) noexcept;

			};

		/**	Read the OFFSET_Y_REG_H_M register.
		 */
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::Driver::ST::LSM9DS1::MAG::Register::Req::Api,
					GetOFFSET_Y_REG_H_MPayload
					>		GetOFFSET_Y_REG_H_MReq;

	public:
		/** This class contains the data that is transfered
			between the client and server within the SetOFFSET_Y_REG_L_MReq
			ITC message.
		 */
		class SetOFFSET_Y_REG_L_MPayload {
			public:
				/**	
				 */
				uint8_t	_value;

			public:
				/** The SetOFFSET_Y_REG_L_MPayload constructor. */
				SetOFFSET_Y_REG_L_MPayload(

					uint8_t	value
					) noexcept;

			};

		/**	Write the OFFSET_Y_REG_L_M register
		 */
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::Driver::ST::LSM9DS1::MAG::Register::Req::Api,
					SetOFFSET_Y_REG_L_MPayload
					>		SetOFFSET_Y_REG_L_MReq;

	public:
		/** This class contains the data that is transfered
			between the client and server within the GetOFFSET_Y_REG_L_MReq
			ITC message.
		 */
		class GetOFFSET_Y_REG_L_MPayload {
			public:
				/**	
				 */
				uint8_t	_value;

			public:
				/** The GetOFFSET_Y_REG_L_MPayload constructor. */
				GetOFFSET_Y_REG_L_MPayload(
					) noexcept;

			};

		/**	Read the OFFSET_Y_REG_L_M register.
		 */
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::Driver::ST::LSM9DS1::MAG::Register::Req::Api,
					GetOFFSET_Y_REG_L_MPayload
					>		GetOFFSET_Y_REG_L_MReq;

	public:
		/** This class contains the data that is transfered
			between the client and server within the SetOFFSET_X_REG_H_MReq
			ITC message.
		 */
		class SetOFFSET_X_REG_H_MPayload {
			public:
				/**	
				 */
				uint8_t	_value;

			public:
				/** The SetOFFSET_X_REG_H_MPayload constructor. */
				SetOFFSET_X_REG_H_MPayload(

					uint8_t	value
					) noexcept;

			};

		/**	Write the OFFSET_X_REG_H_M register
		 */
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::Driver::ST::LSM9DS1::MAG::Register::Req::Api,
					SetOFFSET_X_REG_H_MPayload
					>		SetOFFSET_X_REG_H_MReq;

	public:
		/** This class contains the data that is transfered
			between the client and server within the GetOFFSET_X_REG_H_MReq
			ITC message.
		 */
		class GetOFFSET_X_REG_H_MPayload {
			public:
				/**	
				 */
				uint8_t	_value;

			public:
				/** The GetOFFSET_X_REG_H_MPayload constructor. */
				GetOFFSET_X_REG_H_MPayload(
					) noexcept;

			};

		/**	Read the OFFSET_X_REG_H_M register.
		 */
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::Driver::ST::LSM9DS1::MAG::Register::Req::Api,
					GetOFFSET_X_REG_H_MPayload
					>		GetOFFSET_X_REG_H_MReq;

	public:
		/** This class contains the data that is transfered
			between the client and server within the SetOFFSET_X_REG_L_MReq
			ITC message.
		 */
		class SetOFFSET_X_REG_L_MPayload {
			public:
				/**	
				 */
				uint8_t	_value;

			public:
				/** The SetOFFSET_X_REG_L_MPayload constructor. */
				SetOFFSET_X_REG_L_MPayload(

					uint8_t	value
					) noexcept;

			};

		/**	Write the OFFSET_X_REG_L_M register
		 */
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::Driver::ST::LSM9DS1::MAG::Register::Req::Api,
					SetOFFSET_X_REG_L_MPayload
					>		SetOFFSET_X_REG_L_MReq;

	public:
		/** This class contains the data that is transfered
			between the client and server within the GetOFFSET_X_REG_L_MReq
			ITC message.
		 */
		class GetOFFSET_X_REG_L_MPayload {
			public:
				/**	
				 */
				uint8_t	_value;

			public:
				/** The GetOFFSET_X_REG_L_MPayload constructor. */
				GetOFFSET_X_REG_L_MPayload(
					) noexcept;

			};

		/**	Read the OFFSET_X_REG_L_M register.
		 */
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::Driver::ST::LSM9DS1::MAG::Register::Req::Api,
					GetOFFSET_X_REG_L_MPayload
					>		GetOFFSET_X_REG_L_MReq;

	public:
		/** This type defines a SAP (Service Access Point) for this
			ITC server interface. A SAP holds server address information
			that is required by clients to send messages to a server.
		 */
		typedef Oscl::Mt::Itc::SAP<	Oscl::Driver::ST::LSM9DS1::MAG::Register::
							Req::Api
							>			SAP;

	public:
		/** This type defines a SAP (Service Access Point) for this
			ITC server interface. A SAP holds server address information
			that is required by clients to send messages to a server.
		 */
		typedef Oscl::Mt::Itc::ConcreteSAP<	Oscl::Driver::ST::LSM9DS1::MAG::Register::
							Req::Api
							>			ConcreteSAP;

	public:
		/** Make the compiler happy. */
		virtual ~Api(){}

		/** This operation is invoked within the server thread
			whenever a GetINT_THS_H_MReq is received.
		 */
		virtual void	request(	Oscl::Driver::ST::LSM9DS1::MAG::Register::
									Req::Api::GetINT_THS_H_MReq& msg
									) noexcept=0;

		/** This operation is invoked within the server thread
			whenever a GetINT_THS_L_MReq is received.
		 */
		virtual void	request(	Oscl::Driver::ST::LSM9DS1::MAG::Register::
									Req::Api::GetINT_THS_L_MReq& msg
									) noexcept=0;

		/** This operation is invoked within the server thread
			whenever a GetINT_SRC_MReq is received.
		 */
		virtual void	request(	Oscl::Driver::ST::LSM9DS1::MAG::Register::
									Req::Api::GetINT_SRC_MReq& msg
									) noexcept=0;

		/** This operation is invoked within the server thread
			whenever a SetINT_CFG_MReq is received.
		 */
		virtual void	request(	Oscl::Driver::ST::LSM9DS1::MAG::Register::
									Req::Api::SetINT_CFG_MReq& msg
									) noexcept=0;

		/** This operation is invoked within the server thread
			whenever a GetINT_CFG_MReq is received.
		 */
		virtual void	request(	Oscl::Driver::ST::LSM9DS1::MAG::Register::
									Req::Api::GetINT_CFG_MReq& msg
									) noexcept=0;

		/** This operation is invoked within the server thread
			whenever a GetOUT_MReq is received.
		 */
		virtual void	request(	Oscl::Driver::ST::LSM9DS1::MAG::Register::
									Req::Api::GetOUT_MReq& msg
									) noexcept=0;

		/** This operation is invoked within the server thread
			whenever a GetOUT_Z_H_MReq is received.
		 */
		virtual void	request(	Oscl::Driver::ST::LSM9DS1::MAG::Register::
									Req::Api::GetOUT_Z_H_MReq& msg
									) noexcept=0;

		/** This operation is invoked within the server thread
			whenever a GetOUT_Z_L_MReq is received.
		 */
		virtual void	request(	Oscl::Driver::ST::LSM9DS1::MAG::Register::
									Req::Api::GetOUT_Z_L_MReq& msg
									) noexcept=0;

		/** This operation is invoked within the server thread
			whenever a GetOUT_Y_H_MReq is received.
		 */
		virtual void	request(	Oscl::Driver::ST::LSM9DS1::MAG::Register::
									Req::Api::GetOUT_Y_H_MReq& msg
									) noexcept=0;

		/** This operation is invoked within the server thread
			whenever a GetOUT_Y_L_MReq is received.
		 */
		virtual void	request(	Oscl::Driver::ST::LSM9DS1::MAG::Register::
									Req::Api::GetOUT_Y_L_MReq& msg
									) noexcept=0;

		/** This operation is invoked within the server thread
			whenever a GetOUT_X_H_MReq is received.
		 */
		virtual void	request(	Oscl::Driver::ST::LSM9DS1::MAG::Register::
									Req::Api::GetOUT_X_H_MReq& msg
									) noexcept=0;

		/** This operation is invoked within the server thread
			whenever a GetOUT_X_L_MReq is received.
		 */
		virtual void	request(	Oscl::Driver::ST::LSM9DS1::MAG::Register::
									Req::Api::GetOUT_X_L_MReq& msg
									) noexcept=0;

		/** This operation is invoked within the server thread
			whenever a GetSTATUS_REG_MReq is received.
		 */
		virtual void	request(	Oscl::Driver::ST::LSM9DS1::MAG::Register::
									Req::Api::GetSTATUS_REG_MReq& msg
									) noexcept=0;

		/** This operation is invoked within the server thread
			whenever a SetCTRL_REG5_MReq is received.
		 */
		virtual void	request(	Oscl::Driver::ST::LSM9DS1::MAG::Register::
									Req::Api::SetCTRL_REG5_MReq& msg
									) noexcept=0;

		/** This operation is invoked within the server thread
			whenever a GetCTRL_REG5_MReq is received.
		 */
		virtual void	request(	Oscl::Driver::ST::LSM9DS1::MAG::Register::
									Req::Api::GetCTRL_REG5_MReq& msg
									) noexcept=0;

		/** This operation is invoked within the server thread
			whenever a SetCTRL_REG4_MReq is received.
		 */
		virtual void	request(	Oscl::Driver::ST::LSM9DS1::MAG::Register::
									Req::Api::SetCTRL_REG4_MReq& msg
									) noexcept=0;

		/** This operation is invoked within the server thread
			whenever a GetCTRL_REG4_MReq is received.
		 */
		virtual void	request(	Oscl::Driver::ST::LSM9DS1::MAG::Register::
									Req::Api::GetCTRL_REG4_MReq& msg
									) noexcept=0;

		/** This operation is invoked within the server thread
			whenever a SetCTRL_REG3_MReq is received.
		 */
		virtual void	request(	Oscl::Driver::ST::LSM9DS1::MAG::Register::
									Req::Api::SetCTRL_REG3_MReq& msg
									) noexcept=0;

		/** This operation is invoked within the server thread
			whenever a GetCTRL_REG3_MReq is received.
		 */
		virtual void	request(	Oscl::Driver::ST::LSM9DS1::MAG::Register::
									Req::Api::GetCTRL_REG3_MReq& msg
									) noexcept=0;

		/** This operation is invoked within the server thread
			whenever a SetCTRL_REG2_MReq is received.
		 */
		virtual void	request(	Oscl::Driver::ST::LSM9DS1::MAG::Register::
									Req::Api::SetCTRL_REG2_MReq& msg
									) noexcept=0;

		/** This operation is invoked within the server thread
			whenever a GetCTRL_REG2_MReq is received.
		 */
		virtual void	request(	Oscl::Driver::ST::LSM9DS1::MAG::Register::
									Req::Api::GetCTRL_REG2_MReq& msg
									) noexcept=0;

		/** This operation is invoked within the server thread
			whenever a SetCTRL_REG1_MReq is received.
		 */
		virtual void	request(	Oscl::Driver::ST::LSM9DS1::MAG::Register::
									Req::Api::SetCTRL_REG1_MReq& msg
									) noexcept=0;

		/** This operation is invoked within the server thread
			whenever a GetCTRL_REG1_MReq is received.
		 */
		virtual void	request(	Oscl::Driver::ST::LSM9DS1::MAG::Register::
									Req::Api::GetCTRL_REG1_MReq& msg
									) noexcept=0;

		/** This operation is invoked within the server thread
			whenever a GetWHO_AM_I_MReq is received.
		 */
		virtual void	request(	Oscl::Driver::ST::LSM9DS1::MAG::Register::
									Req::Api::GetWHO_AM_I_MReq& msg
									) noexcept=0;

		/** This operation is invoked within the server thread
			whenever a SetOFFSET_Z_REG_H_MReq is received.
		 */
		virtual void	request(	Oscl::Driver::ST::LSM9DS1::MAG::Register::
									Req::Api::SetOFFSET_Z_REG_H_MReq& msg
									) noexcept=0;

		/** This operation is invoked within the server thread
			whenever a GetOFFSET_Z_REG_H_MReq is received.
		 */
		virtual void	request(	Oscl::Driver::ST::LSM9DS1::MAG::Register::
									Req::Api::GetOFFSET_Z_REG_H_MReq& msg
									) noexcept=0;

		/** This operation is invoked within the server thread
			whenever a SetOFFSET_Z_REG_L_MReq is received.
		 */
		virtual void	request(	Oscl::Driver::ST::LSM9DS1::MAG::Register::
									Req::Api::SetOFFSET_Z_REG_L_MReq& msg
									) noexcept=0;

		/** This operation is invoked within the server thread
			whenever a GetOFFSET_Z_REG_L_MReq is received.
		 */
		virtual void	request(	Oscl::Driver::ST::LSM9DS1::MAG::Register::
									Req::Api::GetOFFSET_Z_REG_L_MReq& msg
									) noexcept=0;

		/** This operation is invoked within the server thread
			whenever a SetOFFSET_Y_REG_H_MReq is received.
		 */
		virtual void	request(	Oscl::Driver::ST::LSM9DS1::MAG::Register::
									Req::Api::SetOFFSET_Y_REG_H_MReq& msg
									) noexcept=0;

		/** This operation is invoked within the server thread
			whenever a GetOFFSET_Y_REG_H_MReq is received.
		 */
		virtual void	request(	Oscl::Driver::ST::LSM9DS1::MAG::Register::
									Req::Api::GetOFFSET_Y_REG_H_MReq& msg
									) noexcept=0;

		/** This operation is invoked within the server thread
			whenever a SetOFFSET_Y_REG_L_MReq is received.
		 */
		virtual void	request(	Oscl::Driver::ST::LSM9DS1::MAG::Register::
									Req::Api::SetOFFSET_Y_REG_L_MReq& msg
									) noexcept=0;

		/** This operation is invoked within the server thread
			whenever a GetOFFSET_Y_REG_L_MReq is received.
		 */
		virtual void	request(	Oscl::Driver::ST::LSM9DS1::MAG::Register::
									Req::Api::GetOFFSET_Y_REG_L_MReq& msg
									) noexcept=0;

		/** This operation is invoked within the server thread
			whenever a SetOFFSET_X_REG_H_MReq is received.
		 */
		virtual void	request(	Oscl::Driver::ST::LSM9DS1::MAG::Register::
									Req::Api::SetOFFSET_X_REG_H_MReq& msg
									) noexcept=0;

		/** This operation is invoked within the server thread
			whenever a GetOFFSET_X_REG_H_MReq is received.
		 */
		virtual void	request(	Oscl::Driver::ST::LSM9DS1::MAG::Register::
									Req::Api::GetOFFSET_X_REG_H_MReq& msg
									) noexcept=0;

		/** This operation is invoked within the server thread
			whenever a SetOFFSET_X_REG_L_MReq is received.
		 */
		virtual void	request(	Oscl::Driver::ST::LSM9DS1::MAG::Register::
									Req::Api::SetOFFSET_X_REG_L_MReq& msg
									) noexcept=0;

		/** This operation is invoked within the server thread
			whenever a GetOFFSET_X_REG_L_MReq is received.
		 */
		virtual void	request(	Oscl::Driver::ST::LSM9DS1::MAG::Register::
									Req::Api::GetOFFSET_X_REG_L_MReq& msg
									) noexcept=0;

	};

}
}
}
}
}
}
}
#endif
