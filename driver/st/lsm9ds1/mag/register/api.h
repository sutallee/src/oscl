/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_driver_st_lsm9ds1_mag_register_apih_
#define _oscl_driver_st_lsm9ds1_mag_register_apih_

#include <stdint.h>

/** */
namespace Oscl {

/** */
namespace Driver {

/** */
namespace ST {

/** */
namespace LSM9DS1 {

/** */
namespace MAG {

/** */
namespace Register {

/**	This class defines the operations that can be performed
	procedurally by the client.
 */
class Api {
	public:
		/** Make the compiler happy.
		 */
		virtual ~Api(){};

		/**	
		 */
		virtual uint8_t	getINT_THS_H_M(	
							) noexcept=0;

		/**	
		 */
		virtual uint8_t	getINT_THS_L_M(	
							) noexcept=0;

		/**	
		 */
		virtual uint8_t	getINT_SRC_M(	
							) noexcept=0;

		/**	
		 */
		virtual void	setINT_CFG_M(	
							uint8_t	value
							) noexcept=0;

		/**	
		 */
		virtual uint8_t	getINT_CFG_M(	
							) noexcept=0;

		/**	
		 */
		virtual void	getOUT_M(	
							uint8_t	maxBufferLength,
							void*	buffer
							) noexcept=0;

		/**	
		 */
		virtual uint8_t	getOUT_Z_H_M(	
							) noexcept=0;

		/**	
		 */
		virtual uint8_t	getOUT_Z_L_M(	
							) noexcept=0;

		/**	
		 */
		virtual uint8_t	getOUT_Y_H_M(	
							) noexcept=0;

		/**	
		 */
		virtual uint8_t	getOUT_Y_L_M(	
							) noexcept=0;

		/**	
		 */
		virtual uint8_t	getOUT_X_H_M(	
							) noexcept=0;

		/**	
		 */
		virtual uint8_t	getOUT_X_L_M(	
							) noexcept=0;

		/**	
		 */
		virtual uint8_t	getSTATUS_REG_M(	
							) noexcept=0;

		/**	
		 */
		virtual void	setCTRL_REG5_M(	
							uint8_t	value
							) noexcept=0;

		/**	
		 */
		virtual uint8_t	getCTRL_REG5_M(	
							) noexcept=0;

		/**	
		 */
		virtual void	setCTRL_REG4_M(	
							uint8_t	value
							) noexcept=0;

		/**	
		 */
		virtual uint8_t	getCTRL_REG4_M(	
							) noexcept=0;

		/**	
		 */
		virtual void	setCTRL_REG3_M(	
							uint8_t	value
							) noexcept=0;

		/**	
		 */
		virtual uint8_t	getCTRL_REG3_M(	
							) noexcept=0;

		/**	
		 */
		virtual void	setCTRL_REG2_M(	
							uint8_t	value
							) noexcept=0;

		/**	
		 */
		virtual uint8_t	getCTRL_REG2_M(	
							) noexcept=0;

		/**	
		 */
		virtual void	setCTRL_REG1_M(	
							uint8_t	value
							) noexcept=0;

		/**	
		 */
		virtual uint8_t	getCTRL_REG1_M(	
							) noexcept=0;

		/**	
		 */
		virtual uint8_t	getWHO_AM_I_M(	
							) noexcept=0;

		/**	
		 */
		virtual void	setOFFSET_Z_REG_H_M(	
							uint8_t	value
							) noexcept=0;

		/**	
		 */
		virtual uint8_t	getOFFSET_Z_REG_H_M(	
							) noexcept=0;

		/**	
		 */
		virtual void	setOFFSET_Z_REG_L_M(	
							uint8_t	value
							) noexcept=0;

		/**	
		 */
		virtual uint8_t	getOFFSET_Z_REG_L_M(	
							) noexcept=0;

		/**	
		 */
		virtual void	setOFFSET_Y_REG_H_M(	
							uint8_t	value
							) noexcept=0;

		/**	
		 */
		virtual uint8_t	getOFFSET_Y_REG_H_M(	
							) noexcept=0;

		/**	
		 */
		virtual void	setOFFSET_Y_REG_L_M(	
							uint8_t	value
							) noexcept=0;

		/**	
		 */
		virtual uint8_t	getOFFSET_Y_REG_L_M(	
							) noexcept=0;

		/**	
		 */
		virtual void	setOFFSET_X_REG_H_M(	
							uint8_t	value
							) noexcept=0;

		/**	
		 */
		virtual uint8_t	getOFFSET_X_REG_H_M(	
							) noexcept=0;

		/**	
		 */
		virtual void	setOFFSET_X_REG_L_M(	
							uint8_t	value
							) noexcept=0;

		/**	
		 */
		virtual uint8_t	getOFFSET_X_REG_L_M(	
							) noexcept=0;

	};

}
}
}
}
}
}
#endif
