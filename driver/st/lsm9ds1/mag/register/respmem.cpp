/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <new>
#include "respmem.h"

using namespace Oscl::Driver::ST::LSM9DS1::MAG::Register;

Resp::Api::GetINT_THS_H_MResp&
	Resp::GetINT_THS_H_MMem::build(	Req::Api::SAP&			sap,
							Resp::Api&				respApi,
							Oscl::Mt::Itc::PostMsgApi&	clientPapi
							) noexcept{
	Req::Api::GetINT_THS_H_MPayload*
		p	=	new(&payload)
				Req::Api::GetINT_THS_H_MPayload(
);
	Resp::Api::GetINT_THS_H_MResp*
		r	=	new(&resp)
			Resp::Api::GetINT_THS_H_MResp(	sap.getReqApi(),
									respApi,
									clientPapi,
									*p
									);

	return *r;
	}

Resp::Api::GetINT_THS_L_MResp&
	Resp::GetINT_THS_L_MMem::build(	Req::Api::SAP&			sap,
							Resp::Api&				respApi,
							Oscl::Mt::Itc::PostMsgApi&	clientPapi
							) noexcept{
	Req::Api::GetINT_THS_L_MPayload*
		p	=	new(&payload)
				Req::Api::GetINT_THS_L_MPayload(
);
	Resp::Api::GetINT_THS_L_MResp*
		r	=	new(&resp)
			Resp::Api::GetINT_THS_L_MResp(	sap.getReqApi(),
									respApi,
									clientPapi,
									*p
									);

	return *r;
	}

Resp::Api::GetINT_SRC_MResp&
	Resp::GetINT_SRC_MMem::build(	Req::Api::SAP&			sap,
							Resp::Api&				respApi,
							Oscl::Mt::Itc::PostMsgApi&	clientPapi
							) noexcept{
	Req::Api::GetINT_SRC_MPayload*
		p	=	new(&payload)
				Req::Api::GetINT_SRC_MPayload(
);
	Resp::Api::GetINT_SRC_MResp*
		r	=	new(&resp)
			Resp::Api::GetINT_SRC_MResp(	sap.getReqApi(),
									respApi,
									clientPapi,
									*p
									);

	return *r;
	}

Resp::Api::SetINT_CFG_MResp&
	Resp::SetINT_CFG_MMem::build(	Req::Api::SAP&			sap,
							Resp::Api&				respApi,
							Oscl::Mt::Itc::PostMsgApi&	clientPapi,
							uint8_t	value
							) noexcept{
	Req::Api::SetINT_CFG_MPayload*
		p	=	new(&payload)
				Req::Api::SetINT_CFG_MPayload(
							value
							);
	Resp::Api::SetINT_CFG_MResp*
		r	=	new(&resp)
			Resp::Api::SetINT_CFG_MResp(	sap.getReqApi(),
									respApi,
									clientPapi,
									*p
									);

	return *r;
	}

Resp::Api::GetINT_CFG_MResp&
	Resp::GetINT_CFG_MMem::build(	Req::Api::SAP&			sap,
							Resp::Api&				respApi,
							Oscl::Mt::Itc::PostMsgApi&	clientPapi
							) noexcept{
	Req::Api::GetINT_CFG_MPayload*
		p	=	new(&payload)
				Req::Api::GetINT_CFG_MPayload(
);
	Resp::Api::GetINT_CFG_MResp*
		r	=	new(&resp)
			Resp::Api::GetINT_CFG_MResp(	sap.getReqApi(),
									respApi,
									clientPapi,
									*p
									);

	return *r;
	}

Resp::Api::GetOUT_MResp&
	Resp::GetOUT_MMem::build(	Req::Api::SAP&			sap,
							Resp::Api&				respApi,
							Oscl::Mt::Itc::PostMsgApi&	clientPapi,
							uint8_t	maxBufferLength,
							void*	buffer
							) noexcept{
	Req::Api::GetOUT_MPayload*
		p	=	new(&payload)
				Req::Api::GetOUT_MPayload(
							maxBufferLength,
							buffer
							);
	Resp::Api::GetOUT_MResp*
		r	=	new(&resp)
			Resp::Api::GetOUT_MResp(	sap.getReqApi(),
									respApi,
									clientPapi,
									*p
									);

	return *r;
	}

Resp::Api::GetOUT_Z_H_MResp&
	Resp::GetOUT_Z_H_MMem::build(	Req::Api::SAP&			sap,
							Resp::Api&				respApi,
							Oscl::Mt::Itc::PostMsgApi&	clientPapi
							) noexcept{
	Req::Api::GetOUT_Z_H_MPayload*
		p	=	new(&payload)
				Req::Api::GetOUT_Z_H_MPayload(
);
	Resp::Api::GetOUT_Z_H_MResp*
		r	=	new(&resp)
			Resp::Api::GetOUT_Z_H_MResp(	sap.getReqApi(),
									respApi,
									clientPapi,
									*p
									);

	return *r;
	}

Resp::Api::GetOUT_Z_L_MResp&
	Resp::GetOUT_Z_L_MMem::build(	Req::Api::SAP&			sap,
							Resp::Api&				respApi,
							Oscl::Mt::Itc::PostMsgApi&	clientPapi
							) noexcept{
	Req::Api::GetOUT_Z_L_MPayload*
		p	=	new(&payload)
				Req::Api::GetOUT_Z_L_MPayload(
);
	Resp::Api::GetOUT_Z_L_MResp*
		r	=	new(&resp)
			Resp::Api::GetOUT_Z_L_MResp(	sap.getReqApi(),
									respApi,
									clientPapi,
									*p
									);

	return *r;
	}

Resp::Api::GetOUT_Y_H_MResp&
	Resp::GetOUT_Y_H_MMem::build(	Req::Api::SAP&			sap,
							Resp::Api&				respApi,
							Oscl::Mt::Itc::PostMsgApi&	clientPapi
							) noexcept{
	Req::Api::GetOUT_Y_H_MPayload*
		p	=	new(&payload)
				Req::Api::GetOUT_Y_H_MPayload(
);
	Resp::Api::GetOUT_Y_H_MResp*
		r	=	new(&resp)
			Resp::Api::GetOUT_Y_H_MResp(	sap.getReqApi(),
									respApi,
									clientPapi,
									*p
									);

	return *r;
	}

Resp::Api::GetOUT_Y_L_MResp&
	Resp::GetOUT_Y_L_MMem::build(	Req::Api::SAP&			sap,
							Resp::Api&				respApi,
							Oscl::Mt::Itc::PostMsgApi&	clientPapi
							) noexcept{
	Req::Api::GetOUT_Y_L_MPayload*
		p	=	new(&payload)
				Req::Api::GetOUT_Y_L_MPayload(
);
	Resp::Api::GetOUT_Y_L_MResp*
		r	=	new(&resp)
			Resp::Api::GetOUT_Y_L_MResp(	sap.getReqApi(),
									respApi,
									clientPapi,
									*p
									);

	return *r;
	}

Resp::Api::GetOUT_X_H_MResp&
	Resp::GetOUT_X_H_MMem::build(	Req::Api::SAP&			sap,
							Resp::Api&				respApi,
							Oscl::Mt::Itc::PostMsgApi&	clientPapi
							) noexcept{
	Req::Api::GetOUT_X_H_MPayload*
		p	=	new(&payload)
				Req::Api::GetOUT_X_H_MPayload(
);
	Resp::Api::GetOUT_X_H_MResp*
		r	=	new(&resp)
			Resp::Api::GetOUT_X_H_MResp(	sap.getReqApi(),
									respApi,
									clientPapi,
									*p
									);

	return *r;
	}

Resp::Api::GetOUT_X_L_MResp&
	Resp::GetOUT_X_L_MMem::build(	Req::Api::SAP&			sap,
							Resp::Api&				respApi,
							Oscl::Mt::Itc::PostMsgApi&	clientPapi
							) noexcept{
	Req::Api::GetOUT_X_L_MPayload*
		p	=	new(&payload)
				Req::Api::GetOUT_X_L_MPayload(
);
	Resp::Api::GetOUT_X_L_MResp*
		r	=	new(&resp)
			Resp::Api::GetOUT_X_L_MResp(	sap.getReqApi(),
									respApi,
									clientPapi,
									*p
									);

	return *r;
	}

Resp::Api::GetSTATUS_REG_MResp&
	Resp::GetSTATUS_REG_MMem::build(	Req::Api::SAP&			sap,
							Resp::Api&				respApi,
							Oscl::Mt::Itc::PostMsgApi&	clientPapi
							) noexcept{
	Req::Api::GetSTATUS_REG_MPayload*
		p	=	new(&payload)
				Req::Api::GetSTATUS_REG_MPayload(
);
	Resp::Api::GetSTATUS_REG_MResp*
		r	=	new(&resp)
			Resp::Api::GetSTATUS_REG_MResp(	sap.getReqApi(),
									respApi,
									clientPapi,
									*p
									);

	return *r;
	}

Resp::Api::SetCTRL_REG5_MResp&
	Resp::SetCTRL_REG5_MMem::build(	Req::Api::SAP&			sap,
							Resp::Api&				respApi,
							Oscl::Mt::Itc::PostMsgApi&	clientPapi,
							uint8_t	value
							) noexcept{
	Req::Api::SetCTRL_REG5_MPayload*
		p	=	new(&payload)
				Req::Api::SetCTRL_REG5_MPayload(
							value
							);
	Resp::Api::SetCTRL_REG5_MResp*
		r	=	new(&resp)
			Resp::Api::SetCTRL_REG5_MResp(	sap.getReqApi(),
									respApi,
									clientPapi,
									*p
									);

	return *r;
	}

Resp::Api::GetCTRL_REG5_MResp&
	Resp::GetCTRL_REG5_MMem::build(	Req::Api::SAP&			sap,
							Resp::Api&				respApi,
							Oscl::Mt::Itc::PostMsgApi&	clientPapi
							) noexcept{
	Req::Api::GetCTRL_REG5_MPayload*
		p	=	new(&payload)
				Req::Api::GetCTRL_REG5_MPayload(
);
	Resp::Api::GetCTRL_REG5_MResp*
		r	=	new(&resp)
			Resp::Api::GetCTRL_REG5_MResp(	sap.getReqApi(),
									respApi,
									clientPapi,
									*p
									);

	return *r;
	}

Resp::Api::SetCTRL_REG4_MResp&
	Resp::SetCTRL_REG4_MMem::build(	Req::Api::SAP&			sap,
							Resp::Api&				respApi,
							Oscl::Mt::Itc::PostMsgApi&	clientPapi,
							uint8_t	value
							) noexcept{
	Req::Api::SetCTRL_REG4_MPayload*
		p	=	new(&payload)
				Req::Api::SetCTRL_REG4_MPayload(
							value
							);
	Resp::Api::SetCTRL_REG4_MResp*
		r	=	new(&resp)
			Resp::Api::SetCTRL_REG4_MResp(	sap.getReqApi(),
									respApi,
									clientPapi,
									*p
									);

	return *r;
	}

Resp::Api::GetCTRL_REG4_MResp&
	Resp::GetCTRL_REG4_MMem::build(	Req::Api::SAP&			sap,
							Resp::Api&				respApi,
							Oscl::Mt::Itc::PostMsgApi&	clientPapi
							) noexcept{
	Req::Api::GetCTRL_REG4_MPayload*
		p	=	new(&payload)
				Req::Api::GetCTRL_REG4_MPayload(
);
	Resp::Api::GetCTRL_REG4_MResp*
		r	=	new(&resp)
			Resp::Api::GetCTRL_REG4_MResp(	sap.getReqApi(),
									respApi,
									clientPapi,
									*p
									);

	return *r;
	}

Resp::Api::SetCTRL_REG3_MResp&
	Resp::SetCTRL_REG3_MMem::build(	Req::Api::SAP&			sap,
							Resp::Api&				respApi,
							Oscl::Mt::Itc::PostMsgApi&	clientPapi,
							uint8_t	value
							) noexcept{
	Req::Api::SetCTRL_REG3_MPayload*
		p	=	new(&payload)
				Req::Api::SetCTRL_REG3_MPayload(
							value
							);
	Resp::Api::SetCTRL_REG3_MResp*
		r	=	new(&resp)
			Resp::Api::SetCTRL_REG3_MResp(	sap.getReqApi(),
									respApi,
									clientPapi,
									*p
									);

	return *r;
	}

Resp::Api::GetCTRL_REG3_MResp&
	Resp::GetCTRL_REG3_MMem::build(	Req::Api::SAP&			sap,
							Resp::Api&				respApi,
							Oscl::Mt::Itc::PostMsgApi&	clientPapi
							) noexcept{
	Req::Api::GetCTRL_REG3_MPayload*
		p	=	new(&payload)
				Req::Api::GetCTRL_REG3_MPayload(
);
	Resp::Api::GetCTRL_REG3_MResp*
		r	=	new(&resp)
			Resp::Api::GetCTRL_REG3_MResp(	sap.getReqApi(),
									respApi,
									clientPapi,
									*p
									);

	return *r;
	}

Resp::Api::SetCTRL_REG2_MResp&
	Resp::SetCTRL_REG2_MMem::build(	Req::Api::SAP&			sap,
							Resp::Api&				respApi,
							Oscl::Mt::Itc::PostMsgApi&	clientPapi,
							uint8_t	value
							) noexcept{
	Req::Api::SetCTRL_REG2_MPayload*
		p	=	new(&payload)
				Req::Api::SetCTRL_REG2_MPayload(
							value
							);
	Resp::Api::SetCTRL_REG2_MResp*
		r	=	new(&resp)
			Resp::Api::SetCTRL_REG2_MResp(	sap.getReqApi(),
									respApi,
									clientPapi,
									*p
									);

	return *r;
	}

Resp::Api::GetCTRL_REG2_MResp&
	Resp::GetCTRL_REG2_MMem::build(	Req::Api::SAP&			sap,
							Resp::Api&				respApi,
							Oscl::Mt::Itc::PostMsgApi&	clientPapi
							) noexcept{
	Req::Api::GetCTRL_REG2_MPayload*
		p	=	new(&payload)
				Req::Api::GetCTRL_REG2_MPayload(
);
	Resp::Api::GetCTRL_REG2_MResp*
		r	=	new(&resp)
			Resp::Api::GetCTRL_REG2_MResp(	sap.getReqApi(),
									respApi,
									clientPapi,
									*p
									);

	return *r;
	}

Resp::Api::SetCTRL_REG1_MResp&
	Resp::SetCTRL_REG1_MMem::build(	Req::Api::SAP&			sap,
							Resp::Api&				respApi,
							Oscl::Mt::Itc::PostMsgApi&	clientPapi,
							uint8_t	value
							) noexcept{
	Req::Api::SetCTRL_REG1_MPayload*
		p	=	new(&payload)
				Req::Api::SetCTRL_REG1_MPayload(
							value
							);
	Resp::Api::SetCTRL_REG1_MResp*
		r	=	new(&resp)
			Resp::Api::SetCTRL_REG1_MResp(	sap.getReqApi(),
									respApi,
									clientPapi,
									*p
									);

	return *r;
	}

Resp::Api::GetCTRL_REG1_MResp&
	Resp::GetCTRL_REG1_MMem::build(	Req::Api::SAP&			sap,
							Resp::Api&				respApi,
							Oscl::Mt::Itc::PostMsgApi&	clientPapi
							) noexcept{
	Req::Api::GetCTRL_REG1_MPayload*
		p	=	new(&payload)
				Req::Api::GetCTRL_REG1_MPayload(
);
	Resp::Api::GetCTRL_REG1_MResp*
		r	=	new(&resp)
			Resp::Api::GetCTRL_REG1_MResp(	sap.getReqApi(),
									respApi,
									clientPapi,
									*p
									);

	return *r;
	}

Resp::Api::GetWHO_AM_I_MResp&
	Resp::GetWHO_AM_I_MMem::build(	Req::Api::SAP&			sap,
							Resp::Api&				respApi,
							Oscl::Mt::Itc::PostMsgApi&	clientPapi
							) noexcept{
	Req::Api::GetWHO_AM_I_MPayload*
		p	=	new(&payload)
				Req::Api::GetWHO_AM_I_MPayload(
);
	Resp::Api::GetWHO_AM_I_MResp*
		r	=	new(&resp)
			Resp::Api::GetWHO_AM_I_MResp(	sap.getReqApi(),
									respApi,
									clientPapi,
									*p
									);

	return *r;
	}

Resp::Api::SetOFFSET_Z_REG_H_MResp&
	Resp::SetOFFSET_Z_REG_H_MMem::build(	Req::Api::SAP&			sap,
							Resp::Api&				respApi,
							Oscl::Mt::Itc::PostMsgApi&	clientPapi,
							uint8_t	value
							) noexcept{
	Req::Api::SetOFFSET_Z_REG_H_MPayload*
		p	=	new(&payload)
				Req::Api::SetOFFSET_Z_REG_H_MPayload(
							value
							);
	Resp::Api::SetOFFSET_Z_REG_H_MResp*
		r	=	new(&resp)
			Resp::Api::SetOFFSET_Z_REG_H_MResp(	sap.getReqApi(),
									respApi,
									clientPapi,
									*p
									);

	return *r;
	}

Resp::Api::GetOFFSET_Z_REG_H_MResp&
	Resp::GetOFFSET_Z_REG_H_MMem::build(	Req::Api::SAP&			sap,
							Resp::Api&				respApi,
							Oscl::Mt::Itc::PostMsgApi&	clientPapi
							) noexcept{
	Req::Api::GetOFFSET_Z_REG_H_MPayload*
		p	=	new(&payload)
				Req::Api::GetOFFSET_Z_REG_H_MPayload(
);
	Resp::Api::GetOFFSET_Z_REG_H_MResp*
		r	=	new(&resp)
			Resp::Api::GetOFFSET_Z_REG_H_MResp(	sap.getReqApi(),
									respApi,
									clientPapi,
									*p
									);

	return *r;
	}

Resp::Api::SetOFFSET_Z_REG_L_MResp&
	Resp::SetOFFSET_Z_REG_L_MMem::build(	Req::Api::SAP&			sap,
							Resp::Api&				respApi,
							Oscl::Mt::Itc::PostMsgApi&	clientPapi,
							uint8_t	value
							) noexcept{
	Req::Api::SetOFFSET_Z_REG_L_MPayload*
		p	=	new(&payload)
				Req::Api::SetOFFSET_Z_REG_L_MPayload(
							value
							);
	Resp::Api::SetOFFSET_Z_REG_L_MResp*
		r	=	new(&resp)
			Resp::Api::SetOFFSET_Z_REG_L_MResp(	sap.getReqApi(),
									respApi,
									clientPapi,
									*p
									);

	return *r;
	}

Resp::Api::GetOFFSET_Z_REG_L_MResp&
	Resp::GetOFFSET_Z_REG_L_MMem::build(	Req::Api::SAP&			sap,
							Resp::Api&				respApi,
							Oscl::Mt::Itc::PostMsgApi&	clientPapi
							) noexcept{
	Req::Api::GetOFFSET_Z_REG_L_MPayload*
		p	=	new(&payload)
				Req::Api::GetOFFSET_Z_REG_L_MPayload(
);
	Resp::Api::GetOFFSET_Z_REG_L_MResp*
		r	=	new(&resp)
			Resp::Api::GetOFFSET_Z_REG_L_MResp(	sap.getReqApi(),
									respApi,
									clientPapi,
									*p
									);

	return *r;
	}

Resp::Api::SetOFFSET_Y_REG_H_MResp&
	Resp::SetOFFSET_Y_REG_H_MMem::build(	Req::Api::SAP&			sap,
							Resp::Api&				respApi,
							Oscl::Mt::Itc::PostMsgApi&	clientPapi,
							uint8_t	value
							) noexcept{
	Req::Api::SetOFFSET_Y_REG_H_MPayload*
		p	=	new(&payload)
				Req::Api::SetOFFSET_Y_REG_H_MPayload(
							value
							);
	Resp::Api::SetOFFSET_Y_REG_H_MResp*
		r	=	new(&resp)
			Resp::Api::SetOFFSET_Y_REG_H_MResp(	sap.getReqApi(),
									respApi,
									clientPapi,
									*p
									);

	return *r;
	}

Resp::Api::GetOFFSET_Y_REG_H_MResp&
	Resp::GetOFFSET_Y_REG_H_MMem::build(	Req::Api::SAP&			sap,
							Resp::Api&				respApi,
							Oscl::Mt::Itc::PostMsgApi&	clientPapi
							) noexcept{
	Req::Api::GetOFFSET_Y_REG_H_MPayload*
		p	=	new(&payload)
				Req::Api::GetOFFSET_Y_REG_H_MPayload(
);
	Resp::Api::GetOFFSET_Y_REG_H_MResp*
		r	=	new(&resp)
			Resp::Api::GetOFFSET_Y_REG_H_MResp(	sap.getReqApi(),
									respApi,
									clientPapi,
									*p
									);

	return *r;
	}

Resp::Api::SetOFFSET_Y_REG_L_MResp&
	Resp::SetOFFSET_Y_REG_L_MMem::build(	Req::Api::SAP&			sap,
							Resp::Api&				respApi,
							Oscl::Mt::Itc::PostMsgApi&	clientPapi,
							uint8_t	value
							) noexcept{
	Req::Api::SetOFFSET_Y_REG_L_MPayload*
		p	=	new(&payload)
				Req::Api::SetOFFSET_Y_REG_L_MPayload(
							value
							);
	Resp::Api::SetOFFSET_Y_REG_L_MResp*
		r	=	new(&resp)
			Resp::Api::SetOFFSET_Y_REG_L_MResp(	sap.getReqApi(),
									respApi,
									clientPapi,
									*p
									);

	return *r;
	}

Resp::Api::GetOFFSET_Y_REG_L_MResp&
	Resp::GetOFFSET_Y_REG_L_MMem::build(	Req::Api::SAP&			sap,
							Resp::Api&				respApi,
							Oscl::Mt::Itc::PostMsgApi&	clientPapi
							) noexcept{
	Req::Api::GetOFFSET_Y_REG_L_MPayload*
		p	=	new(&payload)
				Req::Api::GetOFFSET_Y_REG_L_MPayload(
);
	Resp::Api::GetOFFSET_Y_REG_L_MResp*
		r	=	new(&resp)
			Resp::Api::GetOFFSET_Y_REG_L_MResp(	sap.getReqApi(),
									respApi,
									clientPapi,
									*p
									);

	return *r;
	}

Resp::Api::SetOFFSET_X_REG_H_MResp&
	Resp::SetOFFSET_X_REG_H_MMem::build(	Req::Api::SAP&			sap,
							Resp::Api&				respApi,
							Oscl::Mt::Itc::PostMsgApi&	clientPapi,
							uint8_t	value
							) noexcept{
	Req::Api::SetOFFSET_X_REG_H_MPayload*
		p	=	new(&payload)
				Req::Api::SetOFFSET_X_REG_H_MPayload(
							value
							);
	Resp::Api::SetOFFSET_X_REG_H_MResp*
		r	=	new(&resp)
			Resp::Api::SetOFFSET_X_REG_H_MResp(	sap.getReqApi(),
									respApi,
									clientPapi,
									*p
									);

	return *r;
	}

Resp::Api::GetOFFSET_X_REG_H_MResp&
	Resp::GetOFFSET_X_REG_H_MMem::build(	Req::Api::SAP&			sap,
							Resp::Api&				respApi,
							Oscl::Mt::Itc::PostMsgApi&	clientPapi
							) noexcept{
	Req::Api::GetOFFSET_X_REG_H_MPayload*
		p	=	new(&payload)
				Req::Api::GetOFFSET_X_REG_H_MPayload(
);
	Resp::Api::GetOFFSET_X_REG_H_MResp*
		r	=	new(&resp)
			Resp::Api::GetOFFSET_X_REG_H_MResp(	sap.getReqApi(),
									respApi,
									clientPapi,
									*p
									);

	return *r;
	}

Resp::Api::SetOFFSET_X_REG_L_MResp&
	Resp::SetOFFSET_X_REG_L_MMem::build(	Req::Api::SAP&			sap,
							Resp::Api&				respApi,
							Oscl::Mt::Itc::PostMsgApi&	clientPapi,
							uint8_t	value
							) noexcept{
	Req::Api::SetOFFSET_X_REG_L_MPayload*
		p	=	new(&payload)
				Req::Api::SetOFFSET_X_REG_L_MPayload(
							value
							);
	Resp::Api::SetOFFSET_X_REG_L_MResp*
		r	=	new(&resp)
			Resp::Api::SetOFFSET_X_REG_L_MResp(	sap.getReqApi(),
									respApi,
									clientPapi,
									*p
									);

	return *r;
	}

Resp::Api::GetOFFSET_X_REG_L_MResp&
	Resp::GetOFFSET_X_REG_L_MMem::build(	Req::Api::SAP&			sap,
							Resp::Api&				respApi,
							Oscl::Mt::Itc::PostMsgApi&	clientPapi
							) noexcept{
	Req::Api::GetOFFSET_X_REG_L_MPayload*
		p	=	new(&payload)
				Req::Api::GetOFFSET_X_REG_L_MPayload(
);
	Resp::Api::GetOFFSET_X_REG_L_MResp*
		r	=	new(&resp)
			Resp::Api::GetOFFSET_X_REG_L_MResp(	sap.getReqApi(),
									respApi,
									clientPapi,
									*p
									);

	return *r;
	}

