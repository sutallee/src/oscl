/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_driver_st_lsm9ds1_mag_register_respcomph_
#define _oscl_driver_st_lsm9ds1_mag_register_respcomph_

#include "respapi.h"

/** */
namespace Oscl {

/** */
namespace Driver {

/** */
namespace ST {

/** */
namespace LSM9DS1 {

/** */
namespace MAG {

/** */
namespace Register {

/** */
namespace Resp {

/**	This template class implements a kind of GOF decorator
	pattern allows the context to employ composition instead
	of inheritance. Frequently, a context may need to implement
	more than one interface that may result in method name
	clashes. This template solves the problem by implementing
	the interface and then invoking member function pointers
	in the context instead. One instance of this object is
	created in the context for each interface of this type used
	in the context. Each instance is constructed such that it
	refers to different member functions within the context.
	The interface of this object is passed to any object that
	requires the interface. When the object invokes any of the
	operations of the API, the corresponding member function
	of the context will subsequently be invoked.
 */
/**	This interface defines methods to access the LSM9DS1 IMU
	Magnetometer registers indpendent of the type of bus.
 */
template <class Context>
class Composer : public Api {
	private:
		/** A reference to the Context.
		 */
		Context&	_context;

	private:
		/**	Read the INT_THS_H_M register.
		 */
		void	(Context::*_GetINT_THS_H_M)(Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::GetINT_THS_H_MResp& msg);

		/**	Read the INT_THS_L_M register.
		 */
		void	(Context::*_GetINT_THS_L_M)(Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::GetINT_THS_L_MResp& msg);

		/**	Read the INT_SRC_M register.
		 */
		void	(Context::*_GetINT_SRC_M)(Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::GetINT_SRC_MResp& msg);

		/**	Write the INT_CFG_M register
		 */
		void	(Context::*_SetINT_CFG_M)(Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::SetINT_CFG_MResp& msg);

		/**	Read the INT_CFG_M register.
		 */
		void	(Context::*_GetINT_CFG_M)(Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::GetINT_CFG_MResp& msg);

		/**	Read the OUT_<axis>_<octet>_M registers.
		 */
		void	(Context::*_GetOUT_M)(Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::GetOUT_MResp& msg);

		/**	Read the OUT_Z_H_M register.
		 */
		void	(Context::*_GetOUT_Z_H_M)(Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::GetOUT_Z_H_MResp& msg);

		/**	Read the OUT_Z_L_M register.
		 */
		void	(Context::*_GetOUT_Z_L_M)(Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::GetOUT_Z_L_MResp& msg);

		/**	Read the OUT_Y_H_M register.
		 */
		void	(Context::*_GetOUT_Y_H_M)(Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::GetOUT_Y_H_MResp& msg);

		/**	Read the OUT_Y_L_M register.
		 */
		void	(Context::*_GetOUT_Y_L_M)(Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::GetOUT_Y_L_MResp& msg);

		/**	Read the OUT_X_H_M register.
		 */
		void	(Context::*_GetOUT_X_H_M)(Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::GetOUT_X_H_MResp& msg);

		/**	Read the OUT_X_L_M register.
		 */
		void	(Context::*_GetOUT_X_L_M)(Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::GetOUT_X_L_MResp& msg);

		/**	Read the STATUS_REG_M register.
		 */
		void	(Context::*_GetSTATUS_REG_M)(Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::GetSTATUS_REG_MResp& msg);

		/**	Write the CTRL_REG5_M register
		 */
		void	(Context::*_SetCTRL_REG5_M)(Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::SetCTRL_REG5_MResp& msg);

		/**	Read the CTRL_REG5_M register.
		 */
		void	(Context::*_GetCTRL_REG5_M)(Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::GetCTRL_REG5_MResp& msg);

		/**	Write the CTRL_REG4_M register
		 */
		void	(Context::*_SetCTRL_REG4_M)(Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::SetCTRL_REG4_MResp& msg);

		/**	Read the CTRL_REG4_M register.
		 */
		void	(Context::*_GetCTRL_REG4_M)(Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::GetCTRL_REG4_MResp& msg);

		/**	Write the CTRL_REG3_M register
		 */
		void	(Context::*_SetCTRL_REG3_M)(Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::SetCTRL_REG3_MResp& msg);

		/**	Read the CTRL_REG3_M register.
		 */
		void	(Context::*_GetCTRL_REG3_M)(Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::GetCTRL_REG3_MResp& msg);

		/**	Write the CTRL_REG2_M register
		 */
		void	(Context::*_SetCTRL_REG2_M)(Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::SetCTRL_REG2_MResp& msg);

		/**	Read the CTRL_REG2_M register.
		 */
		void	(Context::*_GetCTRL_REG2_M)(Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::GetCTRL_REG2_MResp& msg);

		/**	Write the CTRL_REG1_M register
		 */
		void	(Context::*_SetCTRL_REG1_M)(Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::SetCTRL_REG1_MResp& msg);

		/**	Read the CTRL_REG1_M register.
		 */
		void	(Context::*_GetCTRL_REG1_M)(Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::GetCTRL_REG1_MResp& msg);

		/**	Read the WHO_AM_I_M register.
		 */
		void	(Context::*_GetWHO_AM_I_M)(Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::GetWHO_AM_I_MResp& msg);

		/**	Write the OFFSET_Z_REG_H_M register
		 */
		void	(Context::*_SetOFFSET_Z_REG_H_M)(Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::SetOFFSET_Z_REG_H_MResp& msg);

		/**	Read the OFFSET_Z_REG_H_M register.
		 */
		void	(Context::*_GetOFFSET_Z_REG_H_M)(Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::GetOFFSET_Z_REG_H_MResp& msg);

		/**	Write the OFFSET_Z_REG_L_M register
		 */
		void	(Context::*_SetOFFSET_Z_REG_L_M)(Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::SetOFFSET_Z_REG_L_MResp& msg);

		/**	Read the OFFSET_Z_REG_L_M register.
		 */
		void	(Context::*_GetOFFSET_Z_REG_L_M)(Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::GetOFFSET_Z_REG_L_MResp& msg);

		/**	Write the OFFSET_Y_REG_H_M register
		 */
		void	(Context::*_SetOFFSET_Y_REG_H_M)(Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::SetOFFSET_Y_REG_H_MResp& msg);

		/**	Read the OFFSET_Y_REG_H_M register.
		 */
		void	(Context::*_GetOFFSET_Y_REG_H_M)(Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::GetOFFSET_Y_REG_H_MResp& msg);

		/**	Write the OFFSET_Y_REG_L_M register
		 */
		void	(Context::*_SetOFFSET_Y_REG_L_M)(Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::SetOFFSET_Y_REG_L_MResp& msg);

		/**	Read the OFFSET_Y_REG_L_M register.
		 */
		void	(Context::*_GetOFFSET_Y_REG_L_M)(Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::GetOFFSET_Y_REG_L_MResp& msg);

		/**	Write the OFFSET_X_REG_H_M register
		 */
		void	(Context::*_SetOFFSET_X_REG_H_M)(Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::SetOFFSET_X_REG_H_MResp& msg);

		/**	Read the OFFSET_X_REG_H_M register.
		 */
		void	(Context::*_GetOFFSET_X_REG_H_M)(Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::GetOFFSET_X_REG_H_MResp& msg);

		/**	Write the OFFSET_X_REG_L_M register
		 */
		void	(Context::*_SetOFFSET_X_REG_L_M)(Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::SetOFFSET_X_REG_L_MResp& msg);

		/**	Read the OFFSET_X_REG_L_M register.
		 */
		void	(Context::*_GetOFFSET_X_REG_L_M)(Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::GetOFFSET_X_REG_L_MResp& msg);

	public:
		/** */
		Composer(
			Context&		context,
			void	(Context::*GetINT_THS_H_M)(Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::GetINT_THS_H_MResp& msg),
			void	(Context::*GetINT_THS_L_M)(Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::GetINT_THS_L_MResp& msg),
			void	(Context::*GetINT_SRC_M)(Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::GetINT_SRC_MResp& msg),
			void	(Context::*SetINT_CFG_M)(Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::SetINT_CFG_MResp& msg),
			void	(Context::*GetINT_CFG_M)(Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::GetINT_CFG_MResp& msg),
			void	(Context::*GetOUT_M)(Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::GetOUT_MResp& msg),
			void	(Context::*GetOUT_Z_H_M)(Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::GetOUT_Z_H_MResp& msg),
			void	(Context::*GetOUT_Z_L_M)(Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::GetOUT_Z_L_MResp& msg),
			void	(Context::*GetOUT_Y_H_M)(Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::GetOUT_Y_H_MResp& msg),
			void	(Context::*GetOUT_Y_L_M)(Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::GetOUT_Y_L_MResp& msg),
			void	(Context::*GetOUT_X_H_M)(Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::GetOUT_X_H_MResp& msg),
			void	(Context::*GetOUT_X_L_M)(Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::GetOUT_X_L_MResp& msg),
			void	(Context::*GetSTATUS_REG_M)(Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::GetSTATUS_REG_MResp& msg),
			void	(Context::*SetCTRL_REG5_M)(Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::SetCTRL_REG5_MResp& msg),
			void	(Context::*GetCTRL_REG5_M)(Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::GetCTRL_REG5_MResp& msg),
			void	(Context::*SetCTRL_REG4_M)(Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::SetCTRL_REG4_MResp& msg),
			void	(Context::*GetCTRL_REG4_M)(Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::GetCTRL_REG4_MResp& msg),
			void	(Context::*SetCTRL_REG3_M)(Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::SetCTRL_REG3_MResp& msg),
			void	(Context::*GetCTRL_REG3_M)(Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::GetCTRL_REG3_MResp& msg),
			void	(Context::*SetCTRL_REG2_M)(Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::SetCTRL_REG2_MResp& msg),
			void	(Context::*GetCTRL_REG2_M)(Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::GetCTRL_REG2_MResp& msg),
			void	(Context::*SetCTRL_REG1_M)(Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::SetCTRL_REG1_MResp& msg),
			void	(Context::*GetCTRL_REG1_M)(Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::GetCTRL_REG1_MResp& msg),
			void	(Context::*GetWHO_AM_I_M)(Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::GetWHO_AM_I_MResp& msg),
			void	(Context::*SetOFFSET_Z_REG_H_M)(Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::SetOFFSET_Z_REG_H_MResp& msg),
			void	(Context::*GetOFFSET_Z_REG_H_M)(Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::GetOFFSET_Z_REG_H_MResp& msg),
			void	(Context::*SetOFFSET_Z_REG_L_M)(Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::SetOFFSET_Z_REG_L_MResp& msg),
			void	(Context::*GetOFFSET_Z_REG_L_M)(Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::GetOFFSET_Z_REG_L_MResp& msg),
			void	(Context::*SetOFFSET_Y_REG_H_M)(Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::SetOFFSET_Y_REG_H_MResp& msg),
			void	(Context::*GetOFFSET_Y_REG_H_M)(Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::GetOFFSET_Y_REG_H_MResp& msg),
			void	(Context::*SetOFFSET_Y_REG_L_M)(Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::SetOFFSET_Y_REG_L_MResp& msg),
			void	(Context::*GetOFFSET_Y_REG_L_M)(Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::GetOFFSET_Y_REG_L_MResp& msg),
			void	(Context::*SetOFFSET_X_REG_H_M)(Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::SetOFFSET_X_REG_H_MResp& msg),
			void	(Context::*GetOFFSET_X_REG_H_M)(Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::GetOFFSET_X_REG_H_MResp& msg),
			void	(Context::*SetOFFSET_X_REG_L_M)(Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::SetOFFSET_X_REG_L_MResp& msg),
			void	(Context::*GetOFFSET_X_REG_L_M)(Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::GetOFFSET_X_REG_L_MResp& msg)
			) noexcept;

	private:
		/**	Read the INT_THS_H_M register.
		 */
		void	response(Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::GetINT_THS_H_MResp& msg) noexcept;

		/**	Read the INT_THS_L_M register.
		 */
		void	response(Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::GetINT_THS_L_MResp& msg) noexcept;

		/**	Read the INT_SRC_M register.
		 */
		void	response(Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::GetINT_SRC_MResp& msg) noexcept;

		/**	Write the INT_CFG_M register
		 */
		void	response(Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::SetINT_CFG_MResp& msg) noexcept;

		/**	Read the INT_CFG_M register.
		 */
		void	response(Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::GetINT_CFG_MResp& msg) noexcept;

		/**	Read the OUT_<axis>_<octet>_M registers.
		 */
		void	response(Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::GetOUT_MResp& msg) noexcept;

		/**	Read the OUT_Z_H_M register.
		 */
		void	response(Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::GetOUT_Z_H_MResp& msg) noexcept;

		/**	Read the OUT_Z_L_M register.
		 */
		void	response(Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::GetOUT_Z_L_MResp& msg) noexcept;

		/**	Read the OUT_Y_H_M register.
		 */
		void	response(Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::GetOUT_Y_H_MResp& msg) noexcept;

		/**	Read the OUT_Y_L_M register.
		 */
		void	response(Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::GetOUT_Y_L_MResp& msg) noexcept;

		/**	Read the OUT_X_H_M register.
		 */
		void	response(Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::GetOUT_X_H_MResp& msg) noexcept;

		/**	Read the OUT_X_L_M register.
		 */
		void	response(Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::GetOUT_X_L_MResp& msg) noexcept;

		/**	Read the STATUS_REG_M register.
		 */
		void	response(Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::GetSTATUS_REG_MResp& msg) noexcept;

		/**	Write the CTRL_REG5_M register
		 */
		void	response(Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::SetCTRL_REG5_MResp& msg) noexcept;

		/**	Read the CTRL_REG5_M register.
		 */
		void	response(Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::GetCTRL_REG5_MResp& msg) noexcept;

		/**	Write the CTRL_REG4_M register
		 */
		void	response(Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::SetCTRL_REG4_MResp& msg) noexcept;

		/**	Read the CTRL_REG4_M register.
		 */
		void	response(Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::GetCTRL_REG4_MResp& msg) noexcept;

		/**	Write the CTRL_REG3_M register
		 */
		void	response(Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::SetCTRL_REG3_MResp& msg) noexcept;

		/**	Read the CTRL_REG3_M register.
		 */
		void	response(Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::GetCTRL_REG3_MResp& msg) noexcept;

		/**	Write the CTRL_REG2_M register
		 */
		void	response(Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::SetCTRL_REG2_MResp& msg) noexcept;

		/**	Read the CTRL_REG2_M register.
		 */
		void	response(Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::GetCTRL_REG2_MResp& msg) noexcept;

		/**	Write the CTRL_REG1_M register
		 */
		void	response(Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::SetCTRL_REG1_MResp& msg) noexcept;

		/**	Read the CTRL_REG1_M register.
		 */
		void	response(Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::GetCTRL_REG1_MResp& msg) noexcept;

		/**	Read the WHO_AM_I_M register.
		 */
		void	response(Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::GetWHO_AM_I_MResp& msg) noexcept;

		/**	Write the OFFSET_Z_REG_H_M register
		 */
		void	response(Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::SetOFFSET_Z_REG_H_MResp& msg) noexcept;

		/**	Read the OFFSET_Z_REG_H_M register.
		 */
		void	response(Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::GetOFFSET_Z_REG_H_MResp& msg) noexcept;

		/**	Write the OFFSET_Z_REG_L_M register
		 */
		void	response(Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::SetOFFSET_Z_REG_L_MResp& msg) noexcept;

		/**	Read the OFFSET_Z_REG_L_M register.
		 */
		void	response(Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::GetOFFSET_Z_REG_L_MResp& msg) noexcept;

		/**	Write the OFFSET_Y_REG_H_M register
		 */
		void	response(Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::SetOFFSET_Y_REG_H_MResp& msg) noexcept;

		/**	Read the OFFSET_Y_REG_H_M register.
		 */
		void	response(Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::GetOFFSET_Y_REG_H_MResp& msg) noexcept;

		/**	Write the OFFSET_Y_REG_L_M register
		 */
		void	response(Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::SetOFFSET_Y_REG_L_MResp& msg) noexcept;

		/**	Read the OFFSET_Y_REG_L_M register.
		 */
		void	response(Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::GetOFFSET_Y_REG_L_MResp& msg) noexcept;

		/**	Write the OFFSET_X_REG_H_M register
		 */
		void	response(Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::SetOFFSET_X_REG_H_MResp& msg) noexcept;

		/**	Read the OFFSET_X_REG_H_M register.
		 */
		void	response(Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::GetOFFSET_X_REG_H_MResp& msg) noexcept;

		/**	Write the OFFSET_X_REG_L_M register
		 */
		void	response(Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::SetOFFSET_X_REG_L_MResp& msg) noexcept;

		/**	Read the OFFSET_X_REG_L_M register.
		 */
		void	response(Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::GetOFFSET_X_REG_L_MResp& msg) noexcept;

	};

template <class Context>
Composer<Context>::Composer(
			Context&		context,
			void	(Context::*GetINT_THS_H_M)(Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::GetINT_THS_H_MResp& msg),
			void	(Context::*GetINT_THS_L_M)(Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::GetINT_THS_L_MResp& msg),
			void	(Context::*GetINT_SRC_M)(Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::GetINT_SRC_MResp& msg),
			void	(Context::*SetINT_CFG_M)(Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::SetINT_CFG_MResp& msg),
			void	(Context::*GetINT_CFG_M)(Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::GetINT_CFG_MResp& msg),
			void	(Context::*GetOUT_M)(Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::GetOUT_MResp& msg),
			void	(Context::*GetOUT_Z_H_M)(Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::GetOUT_Z_H_MResp& msg),
			void	(Context::*GetOUT_Z_L_M)(Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::GetOUT_Z_L_MResp& msg),
			void	(Context::*GetOUT_Y_H_M)(Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::GetOUT_Y_H_MResp& msg),
			void	(Context::*GetOUT_Y_L_M)(Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::GetOUT_Y_L_MResp& msg),
			void	(Context::*GetOUT_X_H_M)(Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::GetOUT_X_H_MResp& msg),
			void	(Context::*GetOUT_X_L_M)(Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::GetOUT_X_L_MResp& msg),
			void	(Context::*GetSTATUS_REG_M)(Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::GetSTATUS_REG_MResp& msg),
			void	(Context::*SetCTRL_REG5_M)(Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::SetCTRL_REG5_MResp& msg),
			void	(Context::*GetCTRL_REG5_M)(Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::GetCTRL_REG5_MResp& msg),
			void	(Context::*SetCTRL_REG4_M)(Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::SetCTRL_REG4_MResp& msg),
			void	(Context::*GetCTRL_REG4_M)(Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::GetCTRL_REG4_MResp& msg),
			void	(Context::*SetCTRL_REG3_M)(Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::SetCTRL_REG3_MResp& msg),
			void	(Context::*GetCTRL_REG3_M)(Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::GetCTRL_REG3_MResp& msg),
			void	(Context::*SetCTRL_REG2_M)(Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::SetCTRL_REG2_MResp& msg),
			void	(Context::*GetCTRL_REG2_M)(Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::GetCTRL_REG2_MResp& msg),
			void	(Context::*SetCTRL_REG1_M)(Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::SetCTRL_REG1_MResp& msg),
			void	(Context::*GetCTRL_REG1_M)(Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::GetCTRL_REG1_MResp& msg),
			void	(Context::*GetWHO_AM_I_M)(Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::GetWHO_AM_I_MResp& msg),
			void	(Context::*SetOFFSET_Z_REG_H_M)(Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::SetOFFSET_Z_REG_H_MResp& msg),
			void	(Context::*GetOFFSET_Z_REG_H_M)(Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::GetOFFSET_Z_REG_H_MResp& msg),
			void	(Context::*SetOFFSET_Z_REG_L_M)(Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::SetOFFSET_Z_REG_L_MResp& msg),
			void	(Context::*GetOFFSET_Z_REG_L_M)(Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::GetOFFSET_Z_REG_L_MResp& msg),
			void	(Context::*SetOFFSET_Y_REG_H_M)(Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::SetOFFSET_Y_REG_H_MResp& msg),
			void	(Context::*GetOFFSET_Y_REG_H_M)(Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::GetOFFSET_Y_REG_H_MResp& msg),
			void	(Context::*SetOFFSET_Y_REG_L_M)(Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::SetOFFSET_Y_REG_L_MResp& msg),
			void	(Context::*GetOFFSET_Y_REG_L_M)(Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::GetOFFSET_Y_REG_L_MResp& msg),
			void	(Context::*SetOFFSET_X_REG_H_M)(Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::SetOFFSET_X_REG_H_MResp& msg),
			void	(Context::*GetOFFSET_X_REG_H_M)(Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::GetOFFSET_X_REG_H_MResp& msg),
			void	(Context::*SetOFFSET_X_REG_L_M)(Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::SetOFFSET_X_REG_L_MResp& msg),
			void	(Context::*GetOFFSET_X_REG_L_M)(Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::GetOFFSET_X_REG_L_MResp& msg)
			) noexcept:
		_context(context),
		_GetINT_THS_H_M(GetINT_THS_H_M),
		_GetINT_THS_L_M(GetINT_THS_L_M),
		_GetINT_SRC_M(GetINT_SRC_M),
		_SetINT_CFG_M(SetINT_CFG_M),
		_GetINT_CFG_M(GetINT_CFG_M),
		_GetOUT_M(GetOUT_M),
		_GetOUT_Z_H_M(GetOUT_Z_H_M),
		_GetOUT_Z_L_M(GetOUT_Z_L_M),
		_GetOUT_Y_H_M(GetOUT_Y_H_M),
		_GetOUT_Y_L_M(GetOUT_Y_L_M),
		_GetOUT_X_H_M(GetOUT_X_H_M),
		_GetOUT_X_L_M(GetOUT_X_L_M),
		_GetSTATUS_REG_M(GetSTATUS_REG_M),
		_SetCTRL_REG5_M(SetCTRL_REG5_M),
		_GetCTRL_REG5_M(GetCTRL_REG5_M),
		_SetCTRL_REG4_M(SetCTRL_REG4_M),
		_GetCTRL_REG4_M(GetCTRL_REG4_M),
		_SetCTRL_REG3_M(SetCTRL_REG3_M),
		_GetCTRL_REG3_M(GetCTRL_REG3_M),
		_SetCTRL_REG2_M(SetCTRL_REG2_M),
		_GetCTRL_REG2_M(GetCTRL_REG2_M),
		_SetCTRL_REG1_M(SetCTRL_REG1_M),
		_GetCTRL_REG1_M(GetCTRL_REG1_M),
		_GetWHO_AM_I_M(GetWHO_AM_I_M),
		_SetOFFSET_Z_REG_H_M(SetOFFSET_Z_REG_H_M),
		_GetOFFSET_Z_REG_H_M(GetOFFSET_Z_REG_H_M),
		_SetOFFSET_Z_REG_L_M(SetOFFSET_Z_REG_L_M),
		_GetOFFSET_Z_REG_L_M(GetOFFSET_Z_REG_L_M),
		_SetOFFSET_Y_REG_H_M(SetOFFSET_Y_REG_H_M),
		_GetOFFSET_Y_REG_H_M(GetOFFSET_Y_REG_H_M),
		_SetOFFSET_Y_REG_L_M(SetOFFSET_Y_REG_L_M),
		_GetOFFSET_Y_REG_L_M(GetOFFSET_Y_REG_L_M),
		_SetOFFSET_X_REG_H_M(SetOFFSET_X_REG_H_M),
		_GetOFFSET_X_REG_H_M(GetOFFSET_X_REG_H_M),
		_SetOFFSET_X_REG_L_M(SetOFFSET_X_REG_L_M),
		_GetOFFSET_X_REG_L_M(GetOFFSET_X_REG_L_M)
		{
	}

template <class Context>
void	Composer<Context>::response(Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::GetINT_THS_H_MResp& msg) noexcept{
	(_context.*_GetINT_THS_H_M)(msg);
	}

template <class Context>
void	Composer<Context>::response(Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::GetINT_THS_L_MResp& msg) noexcept{
	(_context.*_GetINT_THS_L_M)(msg);
	}

template <class Context>
void	Composer<Context>::response(Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::GetINT_SRC_MResp& msg) noexcept{
	(_context.*_GetINT_SRC_M)(msg);
	}

template <class Context>
void	Composer<Context>::response(Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::SetINT_CFG_MResp& msg) noexcept{
	(_context.*_SetINT_CFG_M)(msg);
	}

template <class Context>
void	Composer<Context>::response(Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::GetINT_CFG_MResp& msg) noexcept{
	(_context.*_GetINT_CFG_M)(msg);
	}

template <class Context>
void	Composer<Context>::response(Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::GetOUT_MResp& msg) noexcept{
	(_context.*_GetOUT_M)(msg);
	}

template <class Context>
void	Composer<Context>::response(Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::GetOUT_Z_H_MResp& msg) noexcept{
	(_context.*_GetOUT_Z_H_M)(msg);
	}

template <class Context>
void	Composer<Context>::response(Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::GetOUT_Z_L_MResp& msg) noexcept{
	(_context.*_GetOUT_Z_L_M)(msg);
	}

template <class Context>
void	Composer<Context>::response(Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::GetOUT_Y_H_MResp& msg) noexcept{
	(_context.*_GetOUT_Y_H_M)(msg);
	}

template <class Context>
void	Composer<Context>::response(Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::GetOUT_Y_L_MResp& msg) noexcept{
	(_context.*_GetOUT_Y_L_M)(msg);
	}

template <class Context>
void	Composer<Context>::response(Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::GetOUT_X_H_MResp& msg) noexcept{
	(_context.*_GetOUT_X_H_M)(msg);
	}

template <class Context>
void	Composer<Context>::response(Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::GetOUT_X_L_MResp& msg) noexcept{
	(_context.*_GetOUT_X_L_M)(msg);
	}

template <class Context>
void	Composer<Context>::response(Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::GetSTATUS_REG_MResp& msg) noexcept{
	(_context.*_GetSTATUS_REG_M)(msg);
	}

template <class Context>
void	Composer<Context>::response(Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::SetCTRL_REG5_MResp& msg) noexcept{
	(_context.*_SetCTRL_REG5_M)(msg);
	}

template <class Context>
void	Composer<Context>::response(Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::GetCTRL_REG5_MResp& msg) noexcept{
	(_context.*_GetCTRL_REG5_M)(msg);
	}

template <class Context>
void	Composer<Context>::response(Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::SetCTRL_REG4_MResp& msg) noexcept{
	(_context.*_SetCTRL_REG4_M)(msg);
	}

template <class Context>
void	Composer<Context>::response(Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::GetCTRL_REG4_MResp& msg) noexcept{
	(_context.*_GetCTRL_REG4_M)(msg);
	}

template <class Context>
void	Composer<Context>::response(Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::SetCTRL_REG3_MResp& msg) noexcept{
	(_context.*_SetCTRL_REG3_M)(msg);
	}

template <class Context>
void	Composer<Context>::response(Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::GetCTRL_REG3_MResp& msg) noexcept{
	(_context.*_GetCTRL_REG3_M)(msg);
	}

template <class Context>
void	Composer<Context>::response(Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::SetCTRL_REG2_MResp& msg) noexcept{
	(_context.*_SetCTRL_REG2_M)(msg);
	}

template <class Context>
void	Composer<Context>::response(Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::GetCTRL_REG2_MResp& msg) noexcept{
	(_context.*_GetCTRL_REG2_M)(msg);
	}

template <class Context>
void	Composer<Context>::response(Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::SetCTRL_REG1_MResp& msg) noexcept{
	(_context.*_SetCTRL_REG1_M)(msg);
	}

template <class Context>
void	Composer<Context>::response(Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::GetCTRL_REG1_MResp& msg) noexcept{
	(_context.*_GetCTRL_REG1_M)(msg);
	}

template <class Context>
void	Composer<Context>::response(Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::GetWHO_AM_I_MResp& msg) noexcept{
	(_context.*_GetWHO_AM_I_M)(msg);
	}

template <class Context>
void	Composer<Context>::response(Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::SetOFFSET_Z_REG_H_MResp& msg) noexcept{
	(_context.*_SetOFFSET_Z_REG_H_M)(msg);
	}

template <class Context>
void	Composer<Context>::response(Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::GetOFFSET_Z_REG_H_MResp& msg) noexcept{
	(_context.*_GetOFFSET_Z_REG_H_M)(msg);
	}

template <class Context>
void	Composer<Context>::response(Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::SetOFFSET_Z_REG_L_MResp& msg) noexcept{
	(_context.*_SetOFFSET_Z_REG_L_M)(msg);
	}

template <class Context>
void	Composer<Context>::response(Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::GetOFFSET_Z_REG_L_MResp& msg) noexcept{
	(_context.*_GetOFFSET_Z_REG_L_M)(msg);
	}

template <class Context>
void	Composer<Context>::response(Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::SetOFFSET_Y_REG_H_MResp& msg) noexcept{
	(_context.*_SetOFFSET_Y_REG_H_M)(msg);
	}

template <class Context>
void	Composer<Context>::response(Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::GetOFFSET_Y_REG_H_MResp& msg) noexcept{
	(_context.*_GetOFFSET_Y_REG_H_M)(msg);
	}

template <class Context>
void	Composer<Context>::response(Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::SetOFFSET_Y_REG_L_MResp& msg) noexcept{
	(_context.*_SetOFFSET_Y_REG_L_M)(msg);
	}

template <class Context>
void	Composer<Context>::response(Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::GetOFFSET_Y_REG_L_MResp& msg) noexcept{
	(_context.*_GetOFFSET_Y_REG_L_M)(msg);
	}

template <class Context>
void	Composer<Context>::response(Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::SetOFFSET_X_REG_H_MResp& msg) noexcept{
	(_context.*_SetOFFSET_X_REG_H_M)(msg);
	}

template <class Context>
void	Composer<Context>::response(Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::GetOFFSET_X_REG_H_MResp& msg) noexcept{
	(_context.*_GetOFFSET_X_REG_H_M)(msg);
	}

template <class Context>
void	Composer<Context>::response(Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::SetOFFSET_X_REG_L_MResp& msg) noexcept{
	(_context.*_SetOFFSET_X_REG_L_M)(msg);
	}

template <class Context>
void	Composer<Context>::response(Oscl::Driver::ST::LSM9DS1::MAG::Register::Resp::Api::GetOFFSET_X_REG_L_MResp& msg) noexcept{
	(_context.*_GetOFFSET_X_REG_L_M)(msg);
	}

}
}
}
}
}
}
}
#endif
