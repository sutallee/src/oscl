/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_driver_st_lsm9ds1_mag_register_composerh_
#define _oscl_driver_st_lsm9ds1_mag_register_composerh_

#include "api.h"

/** */
namespace Oscl {

/** */
namespace Driver {

/** */
namespace ST {

/** */
namespace LSM9DS1 {

/** */
namespace MAG {

/** */
namespace Register {

/**	This template class implements a kind of GOF decorator
	pattern allows the context to employ composition instead
	of inheritance. Frequently, a context may need to implement
	more than one interface that may result in method name
	clashes. This template solves the problem by implementing
	the interface and then invoking member function pointers
	in the context instead. One instance of this object is
	created in the context for each interface of this type used
	in the context. Each instance is constructed such that it
	refers to different member functions within the context.
	The interface of this object is passed to any object that
	requires the interface. When the object invokes any of the
	operations of the API, the corresponding member function
	of the context will subsequently be invoked.
 */
template <class Context>
class Composer : public Api {
	private:
		/** A reference to the Context.
		 */
		Context&	_context;

	private:
		/**	
		 */
		uint8_t	(Context::*_getINT_THS_H_M)(	
							);

		/**	
		 */
		uint8_t	(Context::*_getINT_THS_L_M)(	
							);

		/**	
		 */
		uint8_t	(Context::*_getINT_SRC_M)(	
							);

		/**	
		 */
		void	(Context::*_setINT_CFG_M)(	
							uint8_t	value
							);

		/**	
		 */
		uint8_t	(Context::*_getINT_CFG_M)(	
							);

		/**	
		 */
		void	(Context::*_getOUT_M)(	
							uint8_t	maxBufferLength,
							void*	buffer
							);

		/**	
		 */
		uint8_t	(Context::*_getOUT_Z_H_M)(	
							);

		/**	
		 */
		uint8_t	(Context::*_getOUT_Z_L_M)(	
							);

		/**	
		 */
		uint8_t	(Context::*_getOUT_Y_H_M)(	
							);

		/**	
		 */
		uint8_t	(Context::*_getOUT_Y_L_M)(	
							);

		/**	
		 */
		uint8_t	(Context::*_getOUT_X_H_M)(	
							);

		/**	
		 */
		uint8_t	(Context::*_getOUT_X_L_M)(	
							);

		/**	
		 */
		uint8_t	(Context::*_getSTATUS_REG_M)(	
							);

		/**	
		 */
		void	(Context::*_setCTRL_REG5_M)(	
							uint8_t	value
							);

		/**	
		 */
		uint8_t	(Context::*_getCTRL_REG5_M)(	
							);

		/**	
		 */
		void	(Context::*_setCTRL_REG4_M)(	
							uint8_t	value
							);

		/**	
		 */
		uint8_t	(Context::*_getCTRL_REG4_M)(	
							);

		/**	
		 */
		void	(Context::*_setCTRL_REG3_M)(	
							uint8_t	value
							);

		/**	
		 */
		uint8_t	(Context::*_getCTRL_REG3_M)(	
							);

		/**	
		 */
		void	(Context::*_setCTRL_REG2_M)(	
							uint8_t	value
							);

		/**	
		 */
		uint8_t	(Context::*_getCTRL_REG2_M)(	
							);

		/**	
		 */
		void	(Context::*_setCTRL_REG1_M)(	
							uint8_t	value
							);

		/**	
		 */
		uint8_t	(Context::*_getCTRL_REG1_M)(	
							);

		/**	
		 */
		uint8_t	(Context::*_getWHO_AM_I_M)(	
							);

		/**	
		 */
		void	(Context::*_setOFFSET_Z_REG_H_M)(	
							uint8_t	value
							);

		/**	
		 */
		uint8_t	(Context::*_getOFFSET_Z_REG_H_M)(	
							);

		/**	
		 */
		void	(Context::*_setOFFSET_Z_REG_L_M)(	
							uint8_t	value
							);

		/**	
		 */
		uint8_t	(Context::*_getOFFSET_Z_REG_L_M)(	
							);

		/**	
		 */
		void	(Context::*_setOFFSET_Y_REG_H_M)(	
							uint8_t	value
							);

		/**	
		 */
		uint8_t	(Context::*_getOFFSET_Y_REG_H_M)(	
							);

		/**	
		 */
		void	(Context::*_setOFFSET_Y_REG_L_M)(	
							uint8_t	value
							);

		/**	
		 */
		uint8_t	(Context::*_getOFFSET_Y_REG_L_M)(	
							);

		/**	
		 */
		void	(Context::*_setOFFSET_X_REG_H_M)(	
							uint8_t	value
							);

		/**	
		 */
		uint8_t	(Context::*_getOFFSET_X_REG_H_M)(	
							);

		/**	
		 */
		void	(Context::*_setOFFSET_X_REG_L_M)(	
							uint8_t	value
							);

		/**	
		 */
		uint8_t	(Context::*_getOFFSET_X_REG_L_M)(	
							);

	public:
		/** */
		Composer(
			Context&		context,
			uint8_t	(Context::*getINT_THS_H_M)(	
								),
			uint8_t	(Context::*getINT_THS_L_M)(	
								),
			uint8_t	(Context::*getINT_SRC_M)(	
								),
			void	(Context::*setINT_CFG_M)(	
								uint8_t	value
								),
			uint8_t	(Context::*getINT_CFG_M)(	
								),
			void	(Context::*getOUT_M)(	
								uint8_t	maxBufferLength,
								void*	buffer
								),
			uint8_t	(Context::*getOUT_Z_H_M)(	
								),
			uint8_t	(Context::*getOUT_Z_L_M)(	
								),
			uint8_t	(Context::*getOUT_Y_H_M)(	
								),
			uint8_t	(Context::*getOUT_Y_L_M)(	
								),
			uint8_t	(Context::*getOUT_X_H_M)(	
								),
			uint8_t	(Context::*getOUT_X_L_M)(	
								),
			uint8_t	(Context::*getSTATUS_REG_M)(	
								),
			void	(Context::*setCTRL_REG5_M)(	
								uint8_t	value
								),
			uint8_t	(Context::*getCTRL_REG5_M)(	
								),
			void	(Context::*setCTRL_REG4_M)(	
								uint8_t	value
								),
			uint8_t	(Context::*getCTRL_REG4_M)(	
								),
			void	(Context::*setCTRL_REG3_M)(	
								uint8_t	value
								),
			uint8_t	(Context::*getCTRL_REG3_M)(	
								),
			void	(Context::*setCTRL_REG2_M)(	
								uint8_t	value
								),
			uint8_t	(Context::*getCTRL_REG2_M)(	
								),
			void	(Context::*setCTRL_REG1_M)(	
								uint8_t	value
								),
			uint8_t	(Context::*getCTRL_REG1_M)(	
								),
			uint8_t	(Context::*getWHO_AM_I_M)(	
								),
			void	(Context::*setOFFSET_Z_REG_H_M)(	
								uint8_t	value
								),
			uint8_t	(Context::*getOFFSET_Z_REG_H_M)(	
								),
			void	(Context::*setOFFSET_Z_REG_L_M)(	
								uint8_t	value
								),
			uint8_t	(Context::*getOFFSET_Z_REG_L_M)(	
								),
			void	(Context::*setOFFSET_Y_REG_H_M)(	
								uint8_t	value
								),
			uint8_t	(Context::*getOFFSET_Y_REG_H_M)(	
								),
			void	(Context::*setOFFSET_Y_REG_L_M)(	
								uint8_t	value
								),
			uint8_t	(Context::*getOFFSET_Y_REG_L_M)(	
								),
			void	(Context::*setOFFSET_X_REG_H_M)(	
								uint8_t	value
								),
			uint8_t	(Context::*getOFFSET_X_REG_H_M)(	
								),
			void	(Context::*setOFFSET_X_REG_L_M)(	
								uint8_t	value
								),
			uint8_t	(Context::*getOFFSET_X_REG_L_M)(	
								)
			) noexcept;

	private:
		/**	
		 */
		uint8_t	getINT_THS_H_M(	
							) noexcept;

		/**	
		 */
		uint8_t	getINT_THS_L_M(	
							) noexcept;

		/**	
		 */
		uint8_t	getINT_SRC_M(	
							) noexcept;

		/**	
		 */
		void	setINT_CFG_M(	
							uint8_t	value
							) noexcept;

		/**	
		 */
		uint8_t	getINT_CFG_M(	
							) noexcept;

		/**	
		 */
		void	getOUT_M(	
							uint8_t	maxBufferLength,
							void*	buffer
							) noexcept;

		/**	
		 */
		uint8_t	getOUT_Z_H_M(	
							) noexcept;

		/**	
		 */
		uint8_t	getOUT_Z_L_M(	
							) noexcept;

		/**	
		 */
		uint8_t	getOUT_Y_H_M(	
							) noexcept;

		/**	
		 */
		uint8_t	getOUT_Y_L_M(	
							) noexcept;

		/**	
		 */
		uint8_t	getOUT_X_H_M(	
							) noexcept;

		/**	
		 */
		uint8_t	getOUT_X_L_M(	
							) noexcept;

		/**	
		 */
		uint8_t	getSTATUS_REG_M(	
							) noexcept;

		/**	
		 */
		void	setCTRL_REG5_M(	
							uint8_t	value
							) noexcept;

		/**	
		 */
		uint8_t	getCTRL_REG5_M(	
							) noexcept;

		/**	
		 */
		void	setCTRL_REG4_M(	
							uint8_t	value
							) noexcept;

		/**	
		 */
		uint8_t	getCTRL_REG4_M(	
							) noexcept;

		/**	
		 */
		void	setCTRL_REG3_M(	
							uint8_t	value
							) noexcept;

		/**	
		 */
		uint8_t	getCTRL_REG3_M(	
							) noexcept;

		/**	
		 */
		void	setCTRL_REG2_M(	
							uint8_t	value
							) noexcept;

		/**	
		 */
		uint8_t	getCTRL_REG2_M(	
							) noexcept;

		/**	
		 */
		void	setCTRL_REG1_M(	
							uint8_t	value
							) noexcept;

		/**	
		 */
		uint8_t	getCTRL_REG1_M(	
							) noexcept;

		/**	
		 */
		uint8_t	getWHO_AM_I_M(	
							) noexcept;

		/**	
		 */
		void	setOFFSET_Z_REG_H_M(	
							uint8_t	value
							) noexcept;

		/**	
		 */
		uint8_t	getOFFSET_Z_REG_H_M(	
							) noexcept;

		/**	
		 */
		void	setOFFSET_Z_REG_L_M(	
							uint8_t	value
							) noexcept;

		/**	
		 */
		uint8_t	getOFFSET_Z_REG_L_M(	
							) noexcept;

		/**	
		 */
		void	setOFFSET_Y_REG_H_M(	
							uint8_t	value
							) noexcept;

		/**	
		 */
		uint8_t	getOFFSET_Y_REG_H_M(	
							) noexcept;

		/**	
		 */
		void	setOFFSET_Y_REG_L_M(	
							uint8_t	value
							) noexcept;

		/**	
		 */
		uint8_t	getOFFSET_Y_REG_L_M(	
							) noexcept;

		/**	
		 */
		void	setOFFSET_X_REG_H_M(	
							uint8_t	value
							) noexcept;

		/**	
		 */
		uint8_t	getOFFSET_X_REG_H_M(	
							) noexcept;

		/**	
		 */
		void	setOFFSET_X_REG_L_M(	
							uint8_t	value
							) noexcept;

		/**	
		 */
		uint8_t	getOFFSET_X_REG_L_M(	
							) noexcept;

	};

template <class Context>
Composer<Context>::Composer(
			Context&		context,
			uint8_t	(Context::*getINT_THS_H_M)(	
								),
			uint8_t	(Context::*getINT_THS_L_M)(	
								),
			uint8_t	(Context::*getINT_SRC_M)(	
								),
			void	(Context::*setINT_CFG_M)(	
								uint8_t	value
								),
			uint8_t	(Context::*getINT_CFG_M)(	
								),
			void	(Context::*getOUT_M)(	
								uint8_t	maxBufferLength,
								void*	buffer
								),
			uint8_t	(Context::*getOUT_Z_H_M)(	
								),
			uint8_t	(Context::*getOUT_Z_L_M)(	
								),
			uint8_t	(Context::*getOUT_Y_H_M)(	
								),
			uint8_t	(Context::*getOUT_Y_L_M)(	
								),
			uint8_t	(Context::*getOUT_X_H_M)(	
								),
			uint8_t	(Context::*getOUT_X_L_M)(	
								),
			uint8_t	(Context::*getSTATUS_REG_M)(	
								),
			void	(Context::*setCTRL_REG5_M)(	
								uint8_t	value
								),
			uint8_t	(Context::*getCTRL_REG5_M)(	
								),
			void	(Context::*setCTRL_REG4_M)(	
								uint8_t	value
								),
			uint8_t	(Context::*getCTRL_REG4_M)(	
								),
			void	(Context::*setCTRL_REG3_M)(	
								uint8_t	value
								),
			uint8_t	(Context::*getCTRL_REG3_M)(	
								),
			void	(Context::*setCTRL_REG2_M)(	
								uint8_t	value
								),
			uint8_t	(Context::*getCTRL_REG2_M)(	
								),
			void	(Context::*setCTRL_REG1_M)(	
								uint8_t	value
								),
			uint8_t	(Context::*getCTRL_REG1_M)(	
								),
			uint8_t	(Context::*getWHO_AM_I_M)(	
								),
			void	(Context::*setOFFSET_Z_REG_H_M)(	
								uint8_t	value
								),
			uint8_t	(Context::*getOFFSET_Z_REG_H_M)(	
								),
			void	(Context::*setOFFSET_Z_REG_L_M)(	
								uint8_t	value
								),
			uint8_t	(Context::*getOFFSET_Z_REG_L_M)(	
								),
			void	(Context::*setOFFSET_Y_REG_H_M)(	
								uint8_t	value
								),
			uint8_t	(Context::*getOFFSET_Y_REG_H_M)(	
								),
			void	(Context::*setOFFSET_Y_REG_L_M)(	
								uint8_t	value
								),
			uint8_t	(Context::*getOFFSET_Y_REG_L_M)(	
								),
			void	(Context::*setOFFSET_X_REG_H_M)(	
								uint8_t	value
								),
			uint8_t	(Context::*getOFFSET_X_REG_H_M)(	
								),
			void	(Context::*setOFFSET_X_REG_L_M)(	
								uint8_t	value
								),
			uint8_t	(Context::*getOFFSET_X_REG_L_M)(	
								)
			) noexcept:
		_context(context),
		_getINT_THS_H_M(getINT_THS_H_M),
		_getINT_THS_L_M(getINT_THS_L_M),
		_getINT_SRC_M(getINT_SRC_M),
		_setINT_CFG_M(setINT_CFG_M),
		_getINT_CFG_M(getINT_CFG_M),
		_getOUT_M(getOUT_M),
		_getOUT_Z_H_M(getOUT_Z_H_M),
		_getOUT_Z_L_M(getOUT_Z_L_M),
		_getOUT_Y_H_M(getOUT_Y_H_M),
		_getOUT_Y_L_M(getOUT_Y_L_M),
		_getOUT_X_H_M(getOUT_X_H_M),
		_getOUT_X_L_M(getOUT_X_L_M),
		_getSTATUS_REG_M(getSTATUS_REG_M),
		_setCTRL_REG5_M(setCTRL_REG5_M),
		_getCTRL_REG5_M(getCTRL_REG5_M),
		_setCTRL_REG4_M(setCTRL_REG4_M),
		_getCTRL_REG4_M(getCTRL_REG4_M),
		_setCTRL_REG3_M(setCTRL_REG3_M),
		_getCTRL_REG3_M(getCTRL_REG3_M),
		_setCTRL_REG2_M(setCTRL_REG2_M),
		_getCTRL_REG2_M(getCTRL_REG2_M),
		_setCTRL_REG1_M(setCTRL_REG1_M),
		_getCTRL_REG1_M(getCTRL_REG1_M),
		_getWHO_AM_I_M(getWHO_AM_I_M),
		_setOFFSET_Z_REG_H_M(setOFFSET_Z_REG_H_M),
		_getOFFSET_Z_REG_H_M(getOFFSET_Z_REG_H_M),
		_setOFFSET_Z_REG_L_M(setOFFSET_Z_REG_L_M),
		_getOFFSET_Z_REG_L_M(getOFFSET_Z_REG_L_M),
		_setOFFSET_Y_REG_H_M(setOFFSET_Y_REG_H_M),
		_getOFFSET_Y_REG_H_M(getOFFSET_Y_REG_H_M),
		_setOFFSET_Y_REG_L_M(setOFFSET_Y_REG_L_M),
		_getOFFSET_Y_REG_L_M(getOFFSET_Y_REG_L_M),
		_setOFFSET_X_REG_H_M(setOFFSET_X_REG_H_M),
		_getOFFSET_X_REG_H_M(getOFFSET_X_REG_H_M),
		_setOFFSET_X_REG_L_M(setOFFSET_X_REG_L_M),
		_getOFFSET_X_REG_L_M(getOFFSET_X_REG_L_M)
		{
	}

template <class Context>
uint8_t	Composer<Context>::getINT_THS_H_M(	
							) noexcept{

	return (_context.*_getINT_THS_H_M)(
				);
	}

template <class Context>
uint8_t	Composer<Context>::getINT_THS_L_M(	
							) noexcept{

	return (_context.*_getINT_THS_L_M)(
				);
	}

template <class Context>
uint8_t	Composer<Context>::getINT_SRC_M(	
							) noexcept{

	return (_context.*_getINT_SRC_M)(
				);
	}

template <class Context>
void	Composer<Context>::setINT_CFG_M(	
							uint8_t	value
							) noexcept{

	return (_context.*_setINT_CFG_M)(
				value
				);
	}

template <class Context>
uint8_t	Composer<Context>::getINT_CFG_M(	
							) noexcept{

	return (_context.*_getINT_CFG_M)(
				);
	}

template <class Context>
void	Composer<Context>::getOUT_M(	
							uint8_t	maxBufferLength,
							void*	buffer
							) noexcept{

	return (_context.*_getOUT_M)(
				maxBufferLength,
				buffer
				);
	}

template <class Context>
uint8_t	Composer<Context>::getOUT_Z_H_M(	
							) noexcept{

	return (_context.*_getOUT_Z_H_M)(
				);
	}

template <class Context>
uint8_t	Composer<Context>::getOUT_Z_L_M(	
							) noexcept{

	return (_context.*_getOUT_Z_L_M)(
				);
	}

template <class Context>
uint8_t	Composer<Context>::getOUT_Y_H_M(	
							) noexcept{

	return (_context.*_getOUT_Y_H_M)(
				);
	}

template <class Context>
uint8_t	Composer<Context>::getOUT_Y_L_M(	
							) noexcept{

	return (_context.*_getOUT_Y_L_M)(
				);
	}

template <class Context>
uint8_t	Composer<Context>::getOUT_X_H_M(	
							) noexcept{

	return (_context.*_getOUT_X_H_M)(
				);
	}

template <class Context>
uint8_t	Composer<Context>::getOUT_X_L_M(	
							) noexcept{

	return (_context.*_getOUT_X_L_M)(
				);
	}

template <class Context>
uint8_t	Composer<Context>::getSTATUS_REG_M(	
							) noexcept{

	return (_context.*_getSTATUS_REG_M)(
				);
	}

template <class Context>
void	Composer<Context>::setCTRL_REG5_M(	
							uint8_t	value
							) noexcept{

	return (_context.*_setCTRL_REG5_M)(
				value
				);
	}

template <class Context>
uint8_t	Composer<Context>::getCTRL_REG5_M(	
							) noexcept{

	return (_context.*_getCTRL_REG5_M)(
				);
	}

template <class Context>
void	Composer<Context>::setCTRL_REG4_M(	
							uint8_t	value
							) noexcept{

	return (_context.*_setCTRL_REG4_M)(
				value
				);
	}

template <class Context>
uint8_t	Composer<Context>::getCTRL_REG4_M(	
							) noexcept{

	return (_context.*_getCTRL_REG4_M)(
				);
	}

template <class Context>
void	Composer<Context>::setCTRL_REG3_M(	
							uint8_t	value
							) noexcept{

	return (_context.*_setCTRL_REG3_M)(
				value
				);
	}

template <class Context>
uint8_t	Composer<Context>::getCTRL_REG3_M(	
							) noexcept{

	return (_context.*_getCTRL_REG3_M)(
				);
	}

template <class Context>
void	Composer<Context>::setCTRL_REG2_M(	
							uint8_t	value
							) noexcept{

	return (_context.*_setCTRL_REG2_M)(
				value
				);
	}

template <class Context>
uint8_t	Composer<Context>::getCTRL_REG2_M(	
							) noexcept{

	return (_context.*_getCTRL_REG2_M)(
				);
	}

template <class Context>
void	Composer<Context>::setCTRL_REG1_M(	
							uint8_t	value
							) noexcept{

	return (_context.*_setCTRL_REG1_M)(
				value
				);
	}

template <class Context>
uint8_t	Composer<Context>::getCTRL_REG1_M(	
							) noexcept{

	return (_context.*_getCTRL_REG1_M)(
				);
	}

template <class Context>
uint8_t	Composer<Context>::getWHO_AM_I_M(	
							) noexcept{

	return (_context.*_getWHO_AM_I_M)(
				);
	}

template <class Context>
void	Composer<Context>::setOFFSET_Z_REG_H_M(	
							uint8_t	value
							) noexcept{

	return (_context.*_setOFFSET_Z_REG_H_M)(
				value
				);
	}

template <class Context>
uint8_t	Composer<Context>::getOFFSET_Z_REG_H_M(	
							) noexcept{

	return (_context.*_getOFFSET_Z_REG_H_M)(
				);
	}

template <class Context>
void	Composer<Context>::setOFFSET_Z_REG_L_M(	
							uint8_t	value
							) noexcept{

	return (_context.*_setOFFSET_Z_REG_L_M)(
				value
				);
	}

template <class Context>
uint8_t	Composer<Context>::getOFFSET_Z_REG_L_M(	
							) noexcept{

	return (_context.*_getOFFSET_Z_REG_L_M)(
				);
	}

template <class Context>
void	Composer<Context>::setOFFSET_Y_REG_H_M(	
							uint8_t	value
							) noexcept{

	return (_context.*_setOFFSET_Y_REG_H_M)(
				value
				);
	}

template <class Context>
uint8_t	Composer<Context>::getOFFSET_Y_REG_H_M(	
							) noexcept{

	return (_context.*_getOFFSET_Y_REG_H_M)(
				);
	}

template <class Context>
void	Composer<Context>::setOFFSET_Y_REG_L_M(	
							uint8_t	value
							) noexcept{

	return (_context.*_setOFFSET_Y_REG_L_M)(
				value
				);
	}

template <class Context>
uint8_t	Composer<Context>::getOFFSET_Y_REG_L_M(	
							) noexcept{

	return (_context.*_getOFFSET_Y_REG_L_M)(
				);
	}

template <class Context>
void	Composer<Context>::setOFFSET_X_REG_H_M(	
							uint8_t	value
							) noexcept{

	return (_context.*_setOFFSET_X_REG_H_M)(
				value
				);
	}

template <class Context>
uint8_t	Composer<Context>::getOFFSET_X_REG_H_M(	
							) noexcept{

	return (_context.*_getOFFSET_X_REG_H_M)(
				);
	}

template <class Context>
void	Composer<Context>::setOFFSET_X_REG_L_M(	
							uint8_t	value
							) noexcept{

	return (_context.*_setOFFSET_X_REG_L_M)(
				value
				);
	}

template <class Context>
uint8_t	Composer<Context>::getOFFSET_X_REG_L_M(	
							) noexcept{

	return (_context.*_getOFFSET_X_REG_L_M)(
				);
	}

}
}
}
}
}
}
#endif
