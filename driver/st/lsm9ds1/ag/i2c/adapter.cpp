/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/


#include "adapter.h"
#include "oscl/hw/st/lsm9ds1/ag.h"
#include "oscl/error/info.h"

using namespace Oscl::Driver::ST::LSM9DS1::AG::I2C;

Adapter::Adapter(
			Oscl::I2C::Device::Api&	api
			) noexcept:
			_api(api)
		{
	}

void	Adapter::selectThenWriteReg(uint8_t regNum,uint8_t value) noexcept{
	uint8_t	buff[2];
	buff[0]	= regNum;
	buff[1]	= value;
	_api.write(&buff,2);
	}

uint8_t	Adapter::selectThenReadReg(uint8_t regNum) noexcept{
	_api.write(&regNum,1);
	uint8_t	value;
	_api.read(&value,1);
	return value;
	}

void	Adapter::getOUT_TEMP(	
			uint8_t	maxBufferLength,
			void*	buffer
			) noexcept{

	uint8_t	regNum	= Oscl::HW::ST::LSM9DS1::AccelGyro::OUT_TEMP_L;
	_api.write(&regNum,1);

	constexpr unsigned	len	= 2;

	if(maxBufferLength < len){
		Oscl::Error::Info::log(
			"%s: maxBufferLength(%u) < %u\n",
			__PRETTY_FUNCTION__,
			maxBufferLength,
			len
			);
		return;
		}

	_api.read(buffer,len);
	}

void	Adapter::getOUT_G(	
			uint8_t	maxBufferLength,
			void*	buffer
			) noexcept{

	uint8_t	regNum	= Oscl::HW::ST::LSM9DS1::AccelGyro::OUT_X_L_G;
	_api.write(&regNum,1);

	constexpr unsigned	len	= 6;

	if(maxBufferLength < len){
		Oscl::Error::Info::log(
			"%s: maxBufferLength(%u) < %u\n",
			__PRETTY_FUNCTION__,
			maxBufferLength,
			len
			);
		return;
		}

	_api.read(buffer,len);
	}

void	Adapter::getOUT_XL(	
			uint8_t	maxBufferLength,
			void*	buffer
			) noexcept{

	uint8_t	regNum	= Oscl::HW::ST::LSM9DS1::AccelGyro::OUT_X_L_XL;
	_api.write(&regNum,1);

	constexpr unsigned	len	= 6;

	if(maxBufferLength < len){
		Oscl::Error::Info::log(
			"%s: maxBufferLength(%u) < %u\n",
			__PRETTY_FUNCTION__,
			maxBufferLength,
			len
			);
		return;
		}

	_api.read(buffer,len);
	}

void	Adapter::setINT_GEN_DUR_G(
							uint8_t	value
							) noexcept{
	selectThenWriteReg(
		Oscl::HW::ST::LSM9DS1::AccelGyro::INT_GEN_DUR_G,
		value
		);
	}

uint8_t	Adapter::getINT_GEN_DUR_G() noexcept{
	return selectThenReadReg(Oscl::HW::ST::LSM9DS1::AccelGyro::INT_GEN_DUR_G);
	}

void	Adapter::setINT_GEN_THS_ZL_G(
							uint8_t	value
							) noexcept{
	selectThenWriteReg(
		Oscl::HW::ST::LSM9DS1::AccelGyro::INT_GEN_THS_ZL_G,
		value
		);
	}

uint8_t	Adapter::getINT_GEN_THS_ZL_G() noexcept{
	return selectThenReadReg(Oscl::HW::ST::LSM9DS1::AccelGyro::INT_GEN_THS_ZL_G);
	}

void	Adapter::setINT_GEN_THS_ZH_G(
							uint8_t	value
							) noexcept{
	selectThenWriteReg(
		Oscl::HW::ST::LSM9DS1::AccelGyro::INT_GEN_THS_ZH_G,
		value
		);
	}

uint8_t	Adapter::getINT_GEN_THS_ZH_G() noexcept{
	return selectThenReadReg(Oscl::HW::ST::LSM9DS1::AccelGyro::INT_GEN_THS_ZH_G);
	}

void	Adapter::setINT_GEN_THS_YL_G(
							uint8_t	value
							) noexcept{
	selectThenWriteReg(
		Oscl::HW::ST::LSM9DS1::AccelGyro::INT_GEN_THS_YL_G,
		value
		);
	}

uint8_t	Adapter::getINT_GEN_THS_YL_G() noexcept{
	return selectThenReadReg(Oscl::HW::ST::LSM9DS1::AccelGyro::INT_GEN_THS_YL_G);
	}

void	Adapter::setINT_GEN_THS_YH_G(
							uint8_t	value
							) noexcept{
	selectThenWriteReg(
		Oscl::HW::ST::LSM9DS1::AccelGyro::INT_GEN_THS_YH_G,
		value
		);
	}
uint8_t	Adapter::getINT_GEN_THS_YH_G() noexcept{
	return selectThenReadReg(Oscl::HW::ST::LSM9DS1::AccelGyro::INT_GEN_THS_YH_G);
	}

void	Adapter::setINT_GEN_THS_XL_G(
							uint8_t	value
							) noexcept{
	selectThenWriteReg(
		Oscl::HW::ST::LSM9DS1::AccelGyro::INT_GEN_THS_XL_G,
		value
		);
	}

uint8_t	Adapter::getINT_GEN_THS_XL_G() noexcept{
	return selectThenReadReg(Oscl::HW::ST::LSM9DS1::AccelGyro::INT_GEN_THS_XL_G);
	}

void	Adapter::setINT_GEN_THS_XH_G(
							uint8_t	value
							) noexcept{
	selectThenWriteReg(
		Oscl::HW::ST::LSM9DS1::AccelGyro::INT_GEN_THS_XH_G,
		value
		);
	}

uint8_t	Adapter::getINT_GEN_THS_XH_G() noexcept{
	return selectThenReadReg(Oscl::HW::ST::LSM9DS1::AccelGyro::INT_GEN_THS_XH_G);
	}

void	Adapter::setINT_GEN_CFG_G(
							uint8_t	value
							) noexcept{
	selectThenWriteReg(
		Oscl::HW::ST::LSM9DS1::AccelGyro::INT_GEN_CFG_G,
		value
		);
	}

uint8_t	Adapter::getINT_GEN_CFG_G() noexcept{
	return selectThenReadReg(Oscl::HW::ST::LSM9DS1::AccelGyro::INT_GEN_CFG_G);
	}

uint8_t	Adapter::getFIFO_SRC() noexcept{
	return selectThenReadReg(Oscl::HW::ST::LSM9DS1::AccelGyro::FIFO_SRC);
	}

void	Adapter::setFIFO_CTRL(
							uint8_t	value
							) noexcept{
	selectThenWriteReg(
		Oscl::HW::ST::LSM9DS1::AccelGyro::FIFO_CTRL,
		value
		);
	}

uint8_t	Adapter::getFIFO_CTRL() noexcept{
	return selectThenReadReg(Oscl::HW::ST::LSM9DS1::AccelGyro::FIFO_CTRL);
	}

uint8_t	Adapter::getOUT_Z_H_XL() noexcept{
	return selectThenReadReg(Oscl::HW::ST::LSM9DS1::AccelGyro::OUT_Z_H_XL);
	}

uint8_t	Adapter::getOUT_Z_L_XL() noexcept{
	return selectThenReadReg(Oscl::HW::ST::LSM9DS1::AccelGyro::OUT_Z_L_XL);
	}

uint8_t	Adapter::getOUT_Y_H_XL() noexcept{
	return selectThenReadReg(Oscl::HW::ST::LSM9DS1::AccelGyro::OUT_Y_H_XL);
	}

uint8_t	Adapter::getOUT_Y_L_XL() noexcept{
	return selectThenReadReg(Oscl::HW::ST::LSM9DS1::AccelGyro::OUT_Y_L_XL);
	}

uint8_t	Adapter::getOUT_X_H_XL() noexcept{
	return selectThenReadReg(Oscl::HW::ST::LSM9DS1::AccelGyro::OUT_X_H_XL);
	}

uint8_t	Adapter::getOUT_X_L_XL() noexcept{
	return selectThenReadReg(Oscl::HW::ST::LSM9DS1::AccelGyro::OUT_X_L_XL);
	}

uint8_t	Adapter::getSTATUS_REG_2() noexcept{
	return selectThenReadReg(Oscl::HW::ST::LSM9DS1::AccelGyro::STATUS_REG_2);
	}

uint8_t	Adapter::getINT_GEN_SRC_XL() noexcept{
	return selectThenReadReg(Oscl::HW::ST::LSM9DS1::AccelGyro::INT_GEN_SRC_XL);
	}

void	Adapter::setCTRL_REG10(
							uint8_t	value
							) noexcept{
	selectThenWriteReg(
		Oscl::HW::ST::LSM9DS1::AccelGyro::CTRL_REG10,
		value
		);
	}

uint8_t	Adapter::getCTRL_REG10() noexcept{
	return selectThenReadReg(Oscl::HW::ST::LSM9DS1::AccelGyro::CTRL_REG10);
	}

void	Adapter::setCTRL_REG9(
							uint8_t	value
							) noexcept{
	selectThenWriteReg(
		Oscl::HW::ST::LSM9DS1::AccelGyro::CTRL_REG9,
		value
		);
	}

uint8_t	Adapter::getCTRL_REG9() noexcept{
	return selectThenReadReg(Oscl::HW::ST::LSM9DS1::AccelGyro::CTRL_REG9);
	}

void	Adapter::setCTRL_REG8(
							uint8_t	value
							) noexcept{
	selectThenWriteReg(
		Oscl::HW::ST::LSM9DS1::AccelGyro::CTRL_REG8,
		value
		);
	}

uint8_t	Adapter::getCTRL_REG8() noexcept{
	return selectThenReadReg(Oscl::HW::ST::LSM9DS1::AccelGyro::CTRL_REG8);
	}

void	Adapter::setCTRL_REG7_XL(
							uint8_t	value
							) noexcept{
	selectThenWriteReg(
		Oscl::HW::ST::LSM9DS1::AccelGyro::CTRL_REG7_XL,
		value
		);
	}

uint8_t	Adapter::getCTRL_REG7_XL() noexcept{
	return selectThenReadReg(Oscl::HW::ST::LSM9DS1::AccelGyro::CTRL_REG7_XL);
	}

void	Adapter::setCTRL_REG6_XL(
							uint8_t	value
							) noexcept{
	selectThenWriteReg(
		Oscl::HW::ST::LSM9DS1::AccelGyro::CTRL_REG6_XL,
		value
		);
	}

uint8_t	Adapter::getCTRL_REG6_XL() noexcept{
	return selectThenReadReg(Oscl::HW::ST::LSM9DS1::AccelGyro::CTRL_REG6_XL);
	}

void	Adapter::setCTRL_REG5_XL(
							uint8_t	value
							) noexcept{
	selectThenWriteReg(
		Oscl::HW::ST::LSM9DS1::AccelGyro::CTRL_REG5_XL,
		value
		);
	}

uint8_t	Adapter::getCTRL_REG5_XL(
							) noexcept{
	return selectThenReadReg(Oscl::HW::ST::LSM9DS1::AccelGyro::CTRL_REG5_XL);
	}

void	Adapter::setCTRL_REG4(
							uint8_t	value
							) noexcept{
	selectThenWriteReg(
		Oscl::HW::ST::LSM9DS1::AccelGyro::CTRL_REG4,
		value
		);
	}

uint8_t	Adapter::getCTRL_REG4() noexcept{
	return selectThenReadReg(Oscl::HW::ST::LSM9DS1::AccelGyro::CTRL_REG4);
	}

uint8_t	Adapter::getOUT_Z_H_G() noexcept{
	return selectThenReadReg(Oscl::HW::ST::LSM9DS1::AccelGyro::OUT_Z_H_G);
	}

uint8_t	Adapter::getOUT_Z_L_G() noexcept{
	return selectThenReadReg(Oscl::HW::ST::LSM9DS1::AccelGyro::OUT_Z_H_G);
	}

uint8_t	Adapter::getOUT_Y_H_G() noexcept{
	return selectThenReadReg(Oscl::HW::ST::LSM9DS1::AccelGyro::OUT_Y_H_G);
	}

uint8_t	Adapter::getOUT_Y_L_G() noexcept{
	return selectThenReadReg(Oscl::HW::ST::LSM9DS1::AccelGyro::OUT_Y_L_G);
	}

uint8_t	Adapter::getOUT_X_L_G() noexcept{
	return selectThenReadReg(Oscl::HW::ST::LSM9DS1::AccelGyro::OUT_X_L_G);
	}

uint8_t	Adapter::getSTATUS_REG() noexcept{
	return selectThenReadReg(Oscl::HW::ST::LSM9DS1::AccelGyro::STATUS_REG);
	}

uint8_t	Adapter::getOUT_TEMP_H() noexcept{
	return selectThenReadReg(Oscl::HW::ST::LSM9DS1::AccelGyro::OUT_TEMP_H);
	}

uint8_t	Adapter::getOUT_TEMP_L() noexcept{
	return selectThenReadReg(Oscl::HW::ST::LSM9DS1::AccelGyro::OUT_TEMP_L);
	}

uint8_t	Adapter::getINT_GEN_SRC_G() noexcept{
	return selectThenReadReg(Oscl::HW::ST::LSM9DS1::AccelGyro::INT_GEN_SRC_G);
	}

void	Adapter::setORIENT_CFG_G(
							uint8_t	value
							) noexcept{
	selectThenWriteReg(
		Oscl::HW::ST::LSM9DS1::AccelGyro::ORIENT_CFG_G,
		value
		);
	}

uint8_t	Adapter::getORIENT_CFG_G() noexcept{
	return selectThenReadReg(Oscl::HW::ST::LSM9DS1::AccelGyro::ORIENT_CFG_G);
	}

void	Adapter::setCTRL_REG3_G(
							uint8_t	value
							) noexcept{
	selectThenWriteReg(
		Oscl::HW::ST::LSM9DS1::AccelGyro::CTRL_REG3_G,
		value
		);
	}

uint8_t	Adapter::getCTRL_REG3_G() noexcept{
	return selectThenReadReg(Oscl::HW::ST::LSM9DS1::AccelGyro::CTRL_REG3_G);
	}

void	Adapter::setCTRL_REG2_G(
							uint8_t	value
							) noexcept{
	selectThenWriteReg(
		Oscl::HW::ST::LSM9DS1::AccelGyro::CTRL_REG2_G,
		value
		);
	}

uint8_t	Adapter::getCTRL_REG2_G() noexcept{
	return selectThenReadReg(Oscl::HW::ST::LSM9DS1::AccelGyro::CTRL_REG2_G);
	}

void	Adapter::setCTRL_REG1_G(
							uint8_t	value
							) noexcept{
	selectThenWriteReg(
		Oscl::HW::ST::LSM9DS1::AccelGyro::CTRL_REG1_G,
		value
		);
	}

uint8_t	Adapter::getCTRL_REG1_G() noexcept{
	return selectThenReadReg(Oscl::HW::ST::LSM9DS1::AccelGyro::CTRL_REG1_G);
	}

uint8_t	Adapter::getWHO_AM_I() noexcept{
	return selectThenReadReg(Oscl::HW::ST::LSM9DS1::AccelGyro::WHO_AM_I);
	}

void	Adapter::setINT2_CTRL(
							uint8_t	value
							) noexcept{
	selectThenWriteReg(
		Oscl::HW::ST::LSM9DS1::AccelGyro::INT2_CTRL,
		value
		);
	}

uint8_t	Adapter::getINT2_CTRL() noexcept{
	return selectThenReadReg(Oscl::HW::ST::LSM9DS1::AccelGyro::INT2_CTRL);
	}

void	Adapter::setINT1_CTRL(
							uint8_t	value
							) noexcept{
	selectThenWriteReg(
		Oscl::HW::ST::LSM9DS1::AccelGyro::INT1_CTRL,
		value
		);
	}

uint8_t	Adapter::getINT1_CTRL() noexcept{
	return selectThenReadReg(Oscl::HW::ST::LSM9DS1::AccelGyro::INT1_CTRL);
	}

void	Adapter::setREFERENCE_G(
							uint8_t	value
							) noexcept{
	selectThenWriteReg(
		Oscl::HW::ST::LSM9DS1::AccelGyro::REFERENCE_G,
		value
		);
	}

uint8_t	Adapter::getREFERENCE_G() noexcept{
	return selectThenReadReg(Oscl::HW::ST::LSM9DS1::AccelGyro::REFERENCE_G);
	}

void	Adapter::setINT_GEN_DUR_XL(
							uint8_t	value
							) noexcept{
	selectThenWriteReg(
		Oscl::HW::ST::LSM9DS1::AccelGyro::INT_GEN_DUR_XL,
		value
		);
	}

uint8_t	Adapter::getINT_GEN_DUR_XL() noexcept{
	return selectThenReadReg(Oscl::HW::ST::LSM9DS1::AccelGyro::INT_GEN_DUR_XL);
	}

void	Adapter::setINT_GEN_THS_Z_XL(
							uint8_t	value
							) noexcept{
	selectThenWriteReg(
		Oscl::HW::ST::LSM9DS1::AccelGyro::INT_GEN_THS_Z_XL,
		value
		);
	}

uint8_t	Adapter::getINT_GEN_THS_Z_XL() noexcept{
	return selectThenReadReg(Oscl::HW::ST::LSM9DS1::AccelGyro::INT_GEN_THS_Z_XL);
	}

void	Adapter::setINT_GEN_THS_Y_XL(
							uint8_t	value
							) noexcept{
	selectThenWriteReg(
		Oscl::HW::ST::LSM9DS1::AccelGyro::INT_GEN_THS_Y_XL,
		value
		);
	}

uint8_t	Adapter::getINT_GEN_THS_Y_XL() noexcept{
	return selectThenReadReg(Oscl::HW::ST::LSM9DS1::AccelGyro::INT_GEN_THS_Y_XL);
	}

void	Adapter::setINT_GEN_THS_X_XL(
							uint8_t	value
							) noexcept{
	selectThenWriteReg(
		Oscl::HW::ST::LSM9DS1::AccelGyro::INT_GEN_THS_X_XL,
		value
		);
	}

uint8_t	Adapter::getINT_GEN_THS_X_XL() noexcept{
	return selectThenReadReg(Oscl::HW::ST::LSM9DS1::AccelGyro::INT_GEN_THS_X_XL);
	}

void	Adapter::setINT_GEN_CFG_XL(
							uint8_t	value
							) noexcept{
	selectThenWriteReg(
		Oscl::HW::ST::LSM9DS1::AccelGyro::INT_GEN_CFG_XL,
		value
		);
	}

uint8_t	Adapter::getINT_GEN_CFG_XL() noexcept{
	return selectThenReadReg(Oscl::HW::ST::LSM9DS1::AccelGyro::INT_GEN_CFG_XL);
	}

void	Adapter::setACT_DUR(
							uint8_t	value
							) noexcept{
	selectThenWriteReg(
		Oscl::HW::ST::LSM9DS1::AccelGyro::ACT_DUR,
		value
		);
	}

uint8_t	Adapter::getACT_DUR() noexcept{
	return selectThenReadReg(Oscl::HW::ST::LSM9DS1::AccelGyro::ACT_DUR);
	}

void	Adapter::setACT_THS(
							uint8_t	value
							) noexcept{
	selectThenWriteReg(
		Oscl::HW::ST::LSM9DS1::AccelGyro::ACT_THS,
		value
		);
	}

uint8_t	Adapter::getACT_THS() noexcept{
	return selectThenReadReg(Oscl::HW::ST::LSM9DS1::AccelGyro::ACT_THS);
	}

