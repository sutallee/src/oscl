/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/


#ifndef _oscl_driver_st_lsm9ds1_ag_i2c_linux_parth_
#define _oscl_driver_st_lsm9ds1_ag_i2c_linux_parth_

#include "oscl/driver/st/lsm9ds1/ag/register/sync.h"
#include "oscl/mt/itc/postmsgapi.h"

/** */
namespace Oscl {

/** */
namespace Driver {

/** */
namespace ST {

/** */
namespace LSM9DS1 {

/** */
namespace AG {

/** */
namespace I2C {

/** */
namespace LinuxOS {

/** */
class Part :
	private Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api
	{
	private:
		/** */
		Oscl::Mt::Itc::PostMsgApi&						_papi;

		const char*										_deviceFileName;
	private:
		/** */
		Oscl::Driver::ST::LSM9DS1::AG::Register::Sync	_sync;

	public:
		/** */
		Part(
			Oscl::Mt::Itc::PostMsgApi&	papi,
			const char*					deviceFileName
			) noexcept;

		/** */
		void	start() noexcept;

	private: // Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api
		/** This operation is invoked within the server thread
			whenever a SetINT_GEN_DUR_GReq is received.
		 */
		void	request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Req::Api::SetINT_GEN_DUR_GReq& msg
									) noexcept;

		/** This operation is invoked within the server thread
			whenever a GetINT_GEN_DUR_GReq is received.
		 */
		void	request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Req::Api::GetINT_GEN_DUR_GReq& msg
									) noexcept;

		/** This operation is invoked within the server thread
			whenever a SetINT_GEN_THS_ZL_GReq is received.
		 */
		void	request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Req::Api::SetINT_GEN_THS_ZL_GReq& msg
									) noexcept;

		/** This operation is invoked within the server thread
			whenever a GetINT_GEN_THS_ZL_GReq is received.
		 */
		void	request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Req::Api::GetINT_GEN_THS_ZL_GReq& msg
									) noexcept;

		/** This operation is invoked within the server thread
			whenever a SetINT_GEN_THS_ZH_GReq is received.
		 */
		void	request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Req::Api::SetINT_GEN_THS_ZH_GReq& msg
									) noexcept;

		/** This operation is invoked within the server thread
			whenever a GetINT_GEN_THS_ZH_GReq is received.
		 */
		void	request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Req::Api::GetINT_GEN_THS_ZH_GReq& msg
									) noexcept;

		/** This operation is invoked within the server thread
			whenever a SetINT_GEN_THS_YL_GReq is received.
		 */
		void	request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Req::Api::SetINT_GEN_THS_YL_GReq& msg
									) noexcept;

		/** This operation is invoked within the server thread
			whenever a GetINT_GEN_THS_YL_GReq is received.
		 */
		void	request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Req::Api::GetINT_GEN_THS_YL_GReq& msg
									) noexcept;

		/** This operation is invoked within the server thread
			whenever a SetINT_GEN_THS_YH_GReq is received.
		 */
		void	request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Req::Api::SetINT_GEN_THS_YH_GReq& msg
									) noexcept;

		/** This operation is invoked within the server thread
			whenever a GetINT_GEN_THS_YH_GReq is received.
		 */
		void	request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Req::Api::GetINT_GEN_THS_YH_GReq& msg
									) noexcept;

		/** This operation is invoked within the server thread
			whenever a SetINT_GEN_THS_XL_GReq is received.
		 */
		void	request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Req::Api::SetINT_GEN_THS_XL_GReq& msg
									) noexcept;

		/** This operation is invoked within the server thread
			whenever a GetINT_GEN_THS_XL_GReq is received.
		 */
		void	request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Req::Api::GetINT_GEN_THS_XL_GReq& msg
									) noexcept;

		/** This operation is invoked within the server thread
			whenever a SetINT_GEN_THS_XH_GReq is received.
		 */
		void	request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Req::Api::SetINT_GEN_THS_XH_GReq& msg
									) noexcept;

		/** This operation is invoked within the server thread
			whenever a GetINT_GEN_THS_XH_GReq is received.
		 */
		void	request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Req::Api::GetINT_GEN_THS_XH_GReq& msg
									) noexcept;

		/** This operation is invoked within the server thread
			whenever a SetINT_GEN_CFG_GReq is received.
		 */
		void	request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Req::Api::SetINT_GEN_CFG_GReq& msg
									) noexcept;

		/** This operation is invoked within the server thread
			whenever a GetINT_GEN_CFG_GReq is received.
		 */
		void	request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Req::Api::GetINT_GEN_CFG_GReq& msg
									) noexcept;

		/** This operation is invoked within the server thread
			whenever a GetFIFO_SRCReq is received.
		 */
		void	request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Req::Api::GetFIFO_SRCReq& msg
									) noexcept;

		/** This operation is invoked within the server thread
			whenever a SetFIFO_CTRLReq is received.
		 */
		void	request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Req::Api::SetFIFO_CTRLReq& msg
									) noexcept;

		/** This operation is invoked within the server thread
			whenever a GetFIFO_CTRLReq is received.
		 */
		void	request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Req::Api::GetFIFO_CTRLReq& msg
									) noexcept;

		/** This operation is invoked within the server thread
			whenever a GetOUT_Z_H_XLReq is received.
		 */
		void	request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Req::Api::GetOUT_Z_H_XLReq& msg
									) noexcept;

		/** This operation is invoked within the server thread
			whenever a GetOUT_Z_L_XLReq is received.
		 */
		void	request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Req::Api::GetOUT_Z_L_XLReq& msg
									) noexcept;

		/** This operation is invoked within the server thread
			whenever a GetOUT_Y_H_XLReq is received.
		 */
		void	request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Req::Api::GetOUT_Y_H_XLReq& msg
									) noexcept;

		/** This operation is invoked within the server thread
			whenever a GetOUT_Y_L_XLReq is received.
		 */
		void	request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Req::Api::GetOUT_Y_L_XLReq& msg
									) noexcept;

		/** This operation is invoked within the server thread
			whenever a GetOUT_X_H_XLReq is received.
		 */
		void	request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Req::Api::GetOUT_X_H_XLReq& msg
									) noexcept;

		/** This operation is invoked within the server thread
			whenever a GetOUT_X_L_XLReq is received.
		 */
		void	request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Req::Api::GetOUT_X_L_XLReq& msg
									) noexcept;

		/** This operation is invoked within the server thread
			whenever a GetSTATUS_REG_2Req is received.
		 */
		void	request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Req::Api::GetSTATUS_REG_2Req& msg
									) noexcept;

		/** This operation is invoked within the server thread
			whenever a GetINT_GEN_SRC_XLReq is received.
		 */
		void	request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Req::Api::GetINT_GEN_SRC_XLReq& msg
									) noexcept;

		/** This operation is invoked within the server thread
			whenever a SetCTRL_REG10Req is received.
		 */
		void	request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Req::Api::SetCTRL_REG10Req& msg
									) noexcept;

		/** This operation is invoked within the server thread
			whenever a GetCTRL_REG10Req is received.
		 */
		void	request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Req::Api::GetCTRL_REG10Req& msg
									) noexcept;

		/** This operation is invoked within the server thread
			whenever a SetCTRL_REG9Req is received.
		 */
		void	request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Req::Api::SetCTRL_REG9Req& msg
									) noexcept;

		/** This operation is invoked within the server thread
			whenever a GetCTRL_REG9Req is received.
		 */
		void	request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Req::Api::GetCTRL_REG9Req& msg
									) noexcept;

		/** This operation is invoked within the server thread
			whenever a SetCTRL_REG8Req is received.
		 */
		void	request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Req::Api::SetCTRL_REG8Req& msg
									) noexcept;

		/** This operation is invoked within the server thread
			whenever a GetCTRL_REG8Req is received.
		 */
		void	request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Req::Api::GetCTRL_REG8Req& msg
									) noexcept;

		/** This operation is invoked within the server thread
			whenever a SetCTRL_REG7_XLReq is received.
		 */
		void	request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Req::Api::SetCTRL_REG7_XLReq& msg
									) noexcept;

		/** This operation is invoked within the server thread
			whenever a GetCTRL_REG7_XLReq is received.
		 */
		void	request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Req::Api::GetCTRL_REG7_XLReq& msg
									) noexcept;

		/** This operation is invoked within the server thread
			whenever a SetCTRL_REG6_XLReq is received.
		 */
		void	request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Req::Api::SetCTRL_REG6_XLReq& msg
									) noexcept;

		/** This operation is invoked within the server thread
			whenever a GetCTRL_REG6_XLReq is received.
		 */
		void	request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Req::Api::GetCTRL_REG6_XLReq& msg
									) noexcept;

		/** This operation is invoked within the server thread
			whenever a SetCTRL_REG5_XLReq is received.
		 */
		void	request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Req::Api::SetCTRL_REG5_XLReq& msg
									) noexcept;

		/** This operation is invoked within the server thread
			whenever a GetCTRL_REG5_XLReq is received.
		 */
		void	request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Req::Api::GetCTRL_REG5_XLReq& msg
									) noexcept;

		/** This operation is invoked within the server thread
			whenever a SetCTRL_REG4Req is received.
		 */
		void	request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Req::Api::SetCTRL_REG4Req& msg
									) noexcept;

		/** This operation is invoked within the server thread
			whenever a GetCTRL_REG4Req is received.
		 */
		void	request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Req::Api::GetCTRL_REG4Req& msg
									) noexcept;

		/** This operation is invoked within the server thread
			whenever a GetOUT_Z_H_GReq is received.
		 */
		void	request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Req::Api::GetOUT_Z_H_GReq& msg
									) noexcept;

		/** This operation is invoked within the server thread
			whenever a GetOUT_Z_L_GReq is received.
		 */
		void	request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Req::Api::GetOUT_Z_L_GReq& msg
									) noexcept;

		/** This operation is invoked within the server thread
			whenever a GetOUT_Y_H_GReq is received.
		 */
		void	request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Req::Api::GetOUT_Y_H_GReq& msg
									) noexcept;

		/** This operation is invoked within the server thread
			whenever a GetOUT_Y_L_GReq is received.
		 */
		void	request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Req::Api::GetOUT_Y_L_GReq& msg
									) noexcept;

		/** This operation is invoked within the server thread
			whenever a GetOUT_X_L_GReq is received.
		 */
		void	request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Req::Api::GetOUT_X_L_GReq& msg
									) noexcept;

		/** This operation is invoked within the server thread
			whenever a GetSTATUS_REGReq is received.
		 */
		void	request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Req::Api::GetSTATUS_REGReq& msg
									) noexcept;

		/** This operation is invoked within the server thread
			whenever a GetOUT_TEMP_HReq is received.
		 */
		void	request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Req::Api::GetOUT_TEMP_HReq& msg
									) noexcept;

		/** This operation is invoked within the server thread
			whenever a GetOUT_TEMP_LReq is received.
		 */
		void	request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Req::Api::GetOUT_TEMP_LReq& msg
									) noexcept;

		/** This operation is invoked within the server thread
			whenever a GetINT_GEN_SRC_GReq is received.
		 */
		void	request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Req::Api::GetINT_GEN_SRC_GReq& msg
									) noexcept;

		/** This operation is invoked within the server thread
			whenever a SetORIENT_CFG_GReq is received.
		 */
		void	request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Req::Api::SetORIENT_CFG_GReq& msg
									) noexcept;

		/** This operation is invoked within the server thread
			whenever a GetORIENT_CFG_GReq is received.
		 */
		void	request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Req::Api::GetORIENT_CFG_GReq& msg
									) noexcept;

		/** This operation is invoked within the server thread
			whenever a SetCTRL_REG3_GReq is received.
		 */
		void	request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Req::Api::SetCTRL_REG3_GReq& msg
									) noexcept;

		/** This operation is invoked within the server thread
			whenever a GetCTRL_REG3_GReq is received.
		 */
		void	request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Req::Api::GetCTRL_REG3_GReq& msg
									) noexcept;

		/** This operation is invoked within the server thread
			whenever a SetCTRL_REG2_GReq is received.
		 */
		void	request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Req::Api::SetCTRL_REG2_GReq& msg
									) noexcept;

		/** This operation is invoked within the server thread
			whenever a GetCTRL_REG2_GReq is received.
		 */
		void	request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Req::Api::GetCTRL_REG2_GReq& msg
									) noexcept;

		/** This operation is invoked within the server thread
			whenever a SetCTRL_REG1_GReq is received.
		 */
		void	request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Req::Api::SetCTRL_REG1_GReq& msg
									) noexcept;

		/** This operation is invoked within the server thread
			whenever a GetCTRL_REG1_GReq is received.
		 */
		void	request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Req::Api::GetCTRL_REG1_GReq& msg
									) noexcept;

		/** This operation is invoked within the server thread
			whenever a GetWHO_AM_IReq is received.
		 */
		void	request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Req::Api::GetWHO_AM_IReq& msg
									) noexcept;

		/** This operation is invoked within the server thread
			whenever a SetINT2_CTRLReq is received.
		 */
		void	request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Req::Api::SetINT2_CTRLReq& msg
									) noexcept;

		/** This operation is invoked within the server thread
			whenever a GetINT2_CTRLReq is received.
		 */
		void	request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Req::Api::GetINT2_CTRLReq& msg
									) noexcept;

		/** This operation is invoked within the server thread
			whenever a SetINT1_CTRLReq is received.
		 */
		void	request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Req::Api::SetINT1_CTRLReq& msg
									) noexcept;

		/** This operation is invoked within the server thread
			whenever a GetINT1_CTRLReq is received.
		 */
		void	request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Req::Api::GetINT1_CTRLReq& msg
									) noexcept;

		/** This operation is invoked within the server thread
			whenever a SetREFERENCE_GReq is received.
		 */
		void	request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Req::Api::SetREFERENCE_GReq& msg
									) noexcept;

		/** This operation is invoked within the server thread
			whenever a GetREFERENCE_GReq is received.
		 */
		void	request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Req::Api::GetREFERENCE_GReq& msg
									) noexcept;

		/** This operation is invoked within the server thread
			whenever a SetINT_GEN_DUR_XLReq is received.
		 */
		void	request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Req::Api::SetINT_GEN_DUR_XLReq& msg
									) noexcept;

		/** This operation is invoked within the server thread
			whenever a GetINT_GEN_DUR_XLReq is received.
		 */
		void	request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Req::Api::GetINT_GEN_DUR_XLReq& msg
									) noexcept;

		/** This operation is invoked within the server thread
			whenever a SetINT_GEN_THS_Z_XLReq is received.
		 */
		void	request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Req::Api::SetINT_GEN_THS_Z_XLReq& msg
									) noexcept;

		/** This operation is invoked within the server thread
			whenever a GetINT_GEN_THS_Z_XLReq is received.
		 */
		void	request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Req::Api::GetINT_GEN_THS_Z_XLReq& msg
									) noexcept;

		/** This operation is invoked within the server thread
			whenever a SetINT_GEN_THS_Y_XLReq is received.
		 */
		void	request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Req::Api::SetINT_GEN_THS_Y_XLReq& msg
									) noexcept;

		/** This operation is invoked within the server thread
			whenever a GetINT_GEN_THS_Y_XLReq is received.
		 */
		void	request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Req::Api::GetINT_GEN_THS_Y_XLReq& msg
									) noexcept;

		/** This operation is invoked within the server thread
			whenever a SetINT_GEN_THS_X_XLReq is received.
		 */
		void	request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Req::Api::SetINT_GEN_THS_X_XLReq& msg
									) noexcept;

		/** This operation is invoked within the server thread
			whenever a GetINT_GEN_THS_X_XLReq is received.
		 */
		void	request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Req::Api::GetINT_GEN_THS_X_XLReq& msg
									) noexcept;

		/** This operation is invoked within the server thread
			whenever a SetINT_GEN_CFG_XLReq is received.
		 */
		void	request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Req::Api::SetINT_GEN_CFG_XLReq& msg
									) noexcept;

		/** This operation is invoked within the server thread
			whenever a GetINT_GEN_CFG_XLReq is received.
		 */
		void	request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Req::Api::GetINT_GEN_CFG_XLReq& msg
									) noexcept;

		/** This operation is invoked within the server thread
			whenever a SetACT_DURReq is received.
		 */
		void	request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Req::Api::SetACT_DURReq& msg
									) noexcept;

		/** This operation is invoked within the server thread
			whenever a GetACT_DURReq is received.
		 */
		void	request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Req::Api::GetACT_DURReq& msg
									) noexcept;

		/** This operation is invoked within the server thread
			whenever a SetACT_THSReq is received.
		 */
		void	request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Req::Api::SetACT_THSReq& msg
									) noexcept;

		/** This operation is invoked within the server thread
			whenever a GetACT_THSReq is received.
		 */
		void	request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Req::Api::GetACT_THSReq& msg
									) noexcept;

	};

}
}
}
}
}
}
}

#endif
