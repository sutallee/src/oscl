/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/


#include "part.h"

/** */
using namespace Oscl::Driver::ST::LSM9DS1::AG::I2C::LinuxOS;

Part::Part(
	Oscl::Mt::Itc::PostMsgApi&	papi,
	const char*					deviceFileName
	) noexcept:
		_papi(papi),
		_deviceFileName(deviceFileName),
		_sync(
			papi,
			*this
			)
		{
	}

void	Part::start() noexcept{
	}

void	Part::request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
						Req::Api::SetINT_GEN_DUR_GReq& msg
						) noexcept{
	msg.returnToSender();
	}

void	Part::request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
						Req::Api::GetINT_GEN_DUR_GReq& msg
						) noexcept{
	msg.returnToSender();
	}

void	Part::request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
						Req::Api::SetINT_GEN_THS_ZL_GReq& msg
						) noexcept{
	msg.returnToSender();
	}


void	Part::request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
						Req::Api::GetINT_GEN_THS_ZL_GReq& msg
						) noexcept{
	msg.returnToSender();
	}


void	Part::request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
						Req::Api::SetINT_GEN_THS_ZH_GReq& msg
						) noexcept{
	msg.returnToSender();
	}


void	Part::request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
						Req::Api::GetINT_GEN_THS_ZH_GReq& msg
						) noexcept{
	msg.returnToSender();
	}

void	Part::request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
						Req::Api::SetINT_GEN_THS_YL_GReq& msg
						) noexcept{
	msg.returnToSender();
	}

void	Part::request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
						Req::Api::GetINT_GEN_THS_YL_GReq& msg
						) noexcept{
	msg.returnToSender();
	}

void	Part::request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
						Req::Api::SetINT_GEN_THS_YH_GReq& msg
						) noexcept{
	msg.returnToSender();
	}

void	Part::request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
						Req::Api::GetINT_GEN_THS_YH_GReq& msg
						) noexcept{
	msg.returnToSender();
	}

void	Part::request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
						Req::Api::SetINT_GEN_THS_XL_GReq& msg
						) noexcept{
	msg.returnToSender();
	}

void	Part::request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
						Req::Api::GetINT_GEN_THS_XL_GReq& msg
						) noexcept{
	msg.returnToSender();
	}

void	Part::request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
						Req::Api::SetINT_GEN_THS_XH_GReq& msg
						) noexcept{
	msg.returnToSender();
	}

void	Part::request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
						Req::Api::GetINT_GEN_THS_XH_GReq& msg
						) noexcept{
	msg.returnToSender();
	}

void	Part::request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
						Req::Api::SetINT_GEN_CFG_GReq& msg
						) noexcept{
	msg.returnToSender();
	}

void	Part::request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
						Req::Api::GetINT_GEN_CFG_GReq& msg
						) noexcept{
	msg.returnToSender();
	}

void	Part::request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
						Req::Api::GetFIFO_SRCReq& msg
						) noexcept{
	msg.returnToSender();
	}

void	Part::request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
						Req::Api::SetFIFO_CTRLReq& msg
						) noexcept{
	msg.returnToSender();
	}

void	Part::request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
						Req::Api::GetFIFO_CTRLReq& msg
						) noexcept{
	msg.returnToSender();
	}

void	Part::request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
						Req::Api::GetOUT_Z_H_XLReq& msg
						) noexcept{
	msg.returnToSender();
	}

void	Part::request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
						Req::Api::GetOUT_Z_L_XLReq& msg
						) noexcept{
	msg.returnToSender();
	}

void	Part::request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
						Req::Api::GetOUT_Y_H_XLReq& msg
						) noexcept{
	msg.returnToSender();
	}

void	Part::request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
						Req::Api::GetOUT_Y_L_XLReq& msg
						) noexcept{
	msg.returnToSender();
	}

void	Part::request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
						Req::Api::GetOUT_X_H_XLReq& msg
						) noexcept{
	msg.returnToSender();
	}

void	Part::request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
						Req::Api::GetOUT_X_L_XLReq& msg
						) noexcept{
	msg.returnToSender();
	}

void	Part::request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
						Req::Api::GetSTATUS_REG_2Req& msg
						) noexcept{
	msg.returnToSender();
	}

void	Part::request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
						Req::Api::GetINT_GEN_SRC_XLReq& msg
						) noexcept{
	msg.returnToSender();
	}

void	Part::request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
						Req::Api::SetCTRL_REG10Req& msg
						) noexcept{
	msg.returnToSender();
	}

void	Part::request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
						Req::Api::GetCTRL_REG10Req& msg
						) noexcept{
	msg.returnToSender();
	}

void	Part::request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
						Req::Api::SetCTRL_REG9Req& msg
						) noexcept{
	msg.returnToSender();
	}

void	Part::request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
						Req::Api::GetCTRL_REG9Req& msg
						) noexcept{
	msg.returnToSender();
	}

void	Part::request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
						Req::Api::SetCTRL_REG8Req& msg
						) noexcept{
	msg.returnToSender();
	}

void	Part::request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
						Req::Api::GetCTRL_REG8Req& msg
						) noexcept{
	msg.returnToSender();
	}

void	Part::request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
						Req::Api::SetCTRL_REG7_XLReq& msg
						) noexcept{
	msg.returnToSender();
	}

void	Part::request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
						Req::Api::GetCTRL_REG7_XLReq& msg
						) noexcept{
	msg.returnToSender();
	}

void	Part::request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
						Req::Api::SetCTRL_REG6_XLReq& msg
						) noexcept{
	msg.returnToSender();
	}

void	Part::request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
						Req::Api::GetCTRL_REG6_XLReq& msg
						) noexcept{
	msg.returnToSender();
	}

void	Part::request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
						Req::Api::SetCTRL_REG5_XLReq& msg
						) noexcept{
	msg.returnToSender();
	}

void	Part::request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
						Req::Api::GetCTRL_REG5_XLReq& msg
						) noexcept{
	msg.returnToSender();
	}

void	Part::request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
						Req::Api::SetCTRL_REG4Req& msg
						) noexcept{
	msg.returnToSender();
	}

void	Part::request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
						Req::Api::GetCTRL_REG4Req& msg
						) noexcept{
	msg.returnToSender();
	}

void	Part::request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
						Req::Api::GetOUT_Z_H_GReq& msg
						) noexcept{
	msg.returnToSender();
	}

void	Part::request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
						Req::Api::GetOUT_Z_L_GReq& msg
						) noexcept{
	msg.returnToSender();
	}

void	Part::request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
						Req::Api::GetOUT_Y_H_GReq& msg
						) noexcept{
	msg.returnToSender();
	}

void	Part::request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
						Req::Api::GetOUT_Y_L_GReq& msg
						) noexcept{
	msg.returnToSender();
	}

void	Part::request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
						Req::Api::GetOUT_X_L_GReq& msg
						) noexcept{
	msg.returnToSender();
	}

void	Part::request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
						Req::Api::GetSTATUS_REGReq& msg
						) noexcept{
	msg.returnToSender();
	}

void	Part::request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
						Req::Api::GetOUT_TEMP_HReq& msg
						) noexcept{
	msg.returnToSender();
	}

void	Part::request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
						Req::Api::GetOUT_TEMP_LReq& msg
						) noexcept{
	msg.returnToSender();
	}

void	Part::request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
						Req::Api::GetINT_GEN_SRC_GReq& msg
						) noexcept{
	msg.returnToSender();
	}

void	Part::request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
						Req::Api::SetORIENT_CFG_GReq& msg
						) noexcept{
	msg.returnToSender();
	}

void	Part::request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
						Req::Api::GetORIENT_CFG_GReq& msg
						) noexcept{
	msg.returnToSender();
	}

void	Part::request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
						Req::Api::SetCTRL_REG3_GReq& msg
						) noexcept{
	msg.returnToSender();
	}

void	Part::request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
						Req::Api::GetCTRL_REG3_GReq& msg
						) noexcept{
	msg.returnToSender();
	}

void	Part::request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
						Req::Api::SetCTRL_REG2_GReq& msg
						) noexcept{
	msg.returnToSender();
	}

void	Part::request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
						Req::Api::GetCTRL_REG2_GReq& msg
						) noexcept{
	msg.returnToSender();
	}

void	Part::request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
						Req::Api::SetCTRL_REG1_GReq& msg
						) noexcept{
	msg.returnToSender();
	}

void	Part::request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
						Req::Api::GetCTRL_REG1_GReq& msg
						) noexcept{
	msg.returnToSender();
	}

void	Part::request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
						Req::Api::GetWHO_AM_IReq& msg
						) noexcept{
	msg.returnToSender();
	}

void	Part::request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
						Req::Api::SetINT2_CTRLReq& msg
						) noexcept{
	msg.returnToSender();
	}

void	Part::request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
						Req::Api::GetINT2_CTRLReq& msg
						) noexcept{
	msg.returnToSender();
	}

void	Part::request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
						Req::Api::SetINT1_CTRLReq& msg
						) noexcept{
	msg.returnToSender();
	}

void	Part::request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
						Req::Api::GetINT1_CTRLReq& msg
						) noexcept{
	msg.returnToSender();
	}

void	Part::request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
						Req::Api::SetREFERENCE_GReq& msg
						) noexcept{
	msg.returnToSender();
	}

void	Part::request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
						Req::Api::GetREFERENCE_GReq& msg
						) noexcept{
	msg.returnToSender();
	}

void	Part::request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
						Req::Api::SetINT_GEN_DUR_XLReq& msg
						) noexcept{
	msg.returnToSender();
	}

void	Part::request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
						Req::Api::GetINT_GEN_DUR_XLReq& msg
						) noexcept{
	msg.returnToSender();
	}

void	Part::request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
						Req::Api::SetINT_GEN_THS_Z_XLReq& msg
						) noexcept{
	msg.returnToSender();
	}

void	Part::request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
						Req::Api::GetINT_GEN_THS_Z_XLReq& msg
						) noexcept{
	msg.returnToSender();
	}

void	Part::request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
						Req::Api::SetINT_GEN_THS_Y_XLReq& msg
						) noexcept{
	msg.returnToSender();
	}

void	Part::request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
						Req::Api::GetINT_GEN_THS_Y_XLReq& msg
						) noexcept{
	msg.returnToSender();
	}

void	Part::request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
						Req::Api::SetINT_GEN_THS_X_XLReq& msg
						) noexcept{
	msg.returnToSender();
	}

void	Part::request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
						Req::Api::GetINT_GEN_THS_X_XLReq& msg
						) noexcept{
	msg.returnToSender();
	}

void	Part::request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
						Req::Api::SetINT_GEN_CFG_XLReq& msg
						) noexcept{
	msg.returnToSender();
	}

void	Part::request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
						Req::Api::GetINT_GEN_CFG_XLReq& msg
						) noexcept{
	msg.returnToSender();
	}

void	Part::request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
						Req::Api::SetACT_DURReq& msg
						) noexcept{
	msg.returnToSender();
	}

void	Part::request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
						Req::Api::GetACT_DURReq& msg
						) noexcept{
	msg.returnToSender();
	}

void	Part::request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
						Req::Api::SetACT_THSReq& msg
						) noexcept{
	msg.returnToSender();
	}

void	Part::request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
						Req::Api::GetACT_THSReq& msg
						) noexcept{
	msg.returnToSender();
	}

