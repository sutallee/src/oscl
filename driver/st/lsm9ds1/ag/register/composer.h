/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_driver_st_lsm9ds1_ag_register_composerh_
#define _oscl_driver_st_lsm9ds1_ag_register_composerh_

#include "api.h"

/** */
namespace Oscl {

/** */
namespace Driver {

/** */
namespace ST {

/** */
namespace LSM9DS1 {

/** */
namespace AG {

/** */
namespace Register {

/**	This template class implements a kind of GOF decorator
	pattern allows the context to employ composition instead
	of inheritance. Frequently, a context may need to implement
	more than one interface that may result in method name
	clashes. This template solves the problem by implementing
	the interface and then invoking member function pointers
	in the context instead. One instance of this object is
	created in the context for each interface of this type used
	in the context. Each instance is constructed such that it
	refers to different member functions within the context.
	The interface of this object is passed to any object that
	requires the interface. When the object invokes any of the
	operations of the API, the corresponding member function
	of the context will subsequently be invoked.
 */
template <class Context>
class Composer : public Api {
	private:
		/** A reference to the Context.
		 */
		Context&	_context;

	private:
		/**	
		 */
		void	(Context::*_getOUT_TEMP)(	
							uint8_t	maxBufferLength,
							void*	buffer
							);

		/**	
		 */
		void	(Context::*_getOUT_G)(	
							uint8_t	maxBufferLength,
							void*	buffer
							);

		/**	
		 */
		void	(Context::*_getOUT_XL)(	
							uint8_t	maxBufferLength,
							void*	buffer
							);

		/**	
		 */
		void	(Context::*_setINT_GEN_DUR_G)(	
							uint8_t	value
							);

		/**	
		 */
		uint8_t	(Context::*_getINT_GEN_DUR_G)(	
							);

		/**	
		 */
		void	(Context::*_setINT_GEN_THS_ZL_G)(	
							uint8_t	value
							);

		/**	
		 */
		uint8_t	(Context::*_getINT_GEN_THS_ZL_G)(	
							);

		/**	
		 */
		void	(Context::*_setINT_GEN_THS_ZH_G)(	
							uint8_t	value
							);

		/**	
		 */
		uint8_t	(Context::*_getINT_GEN_THS_ZH_G)(	
							);

		/**	
		 */
		void	(Context::*_setINT_GEN_THS_YL_G)(	
							uint8_t	value
							);

		/**	
		 */
		uint8_t	(Context::*_getINT_GEN_THS_YL_G)(	
							);

		/**	
		 */
		void	(Context::*_setINT_GEN_THS_YH_G)(	
							uint8_t	value
							);

		/**	
		 */
		uint8_t	(Context::*_getINT_GEN_THS_YH_G)(	
							);

		/**	
		 */
		void	(Context::*_setINT_GEN_THS_XL_G)(	
							uint8_t	value
							);

		/**	
		 */
		uint8_t	(Context::*_getINT_GEN_THS_XL_G)(	
							);

		/**	
		 */
		void	(Context::*_setINT_GEN_THS_XH_G)(	
							uint8_t	value
							);

		/**	
		 */
		uint8_t	(Context::*_getINT_GEN_THS_XH_G)(	
							);

		/**	
		 */
		void	(Context::*_setINT_GEN_CFG_G)(	
							uint8_t	value
							);

		/**	
		 */
		uint8_t	(Context::*_getINT_GEN_CFG_G)(	
							);

		/**	
		 */
		uint8_t	(Context::*_getFIFO_SRC)(	
							);

		/**	
		 */
		void	(Context::*_setFIFO_CTRL)(	
							uint8_t	value
							);

		/**	
		 */
		uint8_t	(Context::*_getFIFO_CTRL)(	
							);

		/**	
		 */
		uint8_t	(Context::*_getOUT_Z_H_XL)(	
							);

		/**	
		 */
		uint8_t	(Context::*_getOUT_Z_L_XL)(	
							);

		/**	
		 */
		uint8_t	(Context::*_getOUT_Y_H_XL)(	
							);

		/**	
		 */
		uint8_t	(Context::*_getOUT_Y_L_XL)(	
							);

		/**	
		 */
		uint8_t	(Context::*_getOUT_X_H_XL)(	
							);

		/**	
		 */
		uint8_t	(Context::*_getOUT_X_L_XL)(	
							);

		/**	
		 */
		uint8_t	(Context::*_getSTATUS_REG_2)(	
							);

		/**	
		 */
		uint8_t	(Context::*_getINT_GEN_SRC_XL)(	
							);

		/**	
		 */
		void	(Context::*_setCTRL_REG10)(	
							uint8_t	value
							);

		/**	
		 */
		uint8_t	(Context::*_getCTRL_REG10)(	
							);

		/**	
		 */
		void	(Context::*_setCTRL_REG9)(	
							uint8_t	value
							);

		/**	
		 */
		uint8_t	(Context::*_getCTRL_REG9)(	
							);

		/**	
		 */
		void	(Context::*_setCTRL_REG8)(	
							uint8_t	value
							);

		/**	
		 */
		uint8_t	(Context::*_getCTRL_REG8)(	
							);

		/**	
		 */
		void	(Context::*_setCTRL_REG7_XL)(	
							uint8_t	value
							);

		/**	
		 */
		uint8_t	(Context::*_getCTRL_REG7_XL)(	
							);

		/**	
		 */
		void	(Context::*_setCTRL_REG6_XL)(	
							uint8_t	value
							);

		/**	
		 */
		uint8_t	(Context::*_getCTRL_REG6_XL)(	
							);

		/**	
		 */
		void	(Context::*_setCTRL_REG5_XL)(	
							uint8_t	value
							);

		/**	
		 */
		uint8_t	(Context::*_getCTRL_REG5_XL)(	
							);

		/**	
		 */
		void	(Context::*_setCTRL_REG4)(	
							uint8_t	value
							);

		/**	
		 */
		uint8_t	(Context::*_getCTRL_REG4)(	
							);

		/**	
		 */
		uint8_t	(Context::*_getOUT_Z_H_G)(	
							);

		/**	
		 */
		uint8_t	(Context::*_getOUT_Z_L_G)(	
							);

		/**	
		 */
		uint8_t	(Context::*_getOUT_Y_H_G)(	
							);

		/**	
		 */
		uint8_t	(Context::*_getOUT_Y_L_G)(	
							);

		/**	
		 */
		uint8_t	(Context::*_getOUT_X_L_G)(	
							);

		/**	
		 */
		uint8_t	(Context::*_getSTATUS_REG)(	
							);

		/**	
		 */
		uint8_t	(Context::*_getOUT_TEMP_H)(	
							);

		/**	
		 */
		uint8_t	(Context::*_getOUT_TEMP_L)(	
							);

		/**	
		 */
		uint8_t	(Context::*_getINT_GEN_SRC_G)(	
							);

		/**	
		 */
		void	(Context::*_setORIENT_CFG_G)(	
							uint8_t	value
							);

		/**	
		 */
		uint8_t	(Context::*_getORIENT_CFG_G)(	
							);

		/**	
		 */
		void	(Context::*_setCTRL_REG3_G)(	
							uint8_t	value
							);

		/**	
		 */
		uint8_t	(Context::*_getCTRL_REG3_G)(	
							);

		/**	
		 */
		void	(Context::*_setCTRL_REG2_G)(	
							uint8_t	value
							);

		/**	
		 */
		uint8_t	(Context::*_getCTRL_REG2_G)(	
							);

		/**	
		 */
		void	(Context::*_setCTRL_REG1_G)(	
							uint8_t	value
							);

		/**	
		 */
		uint8_t	(Context::*_getCTRL_REG1_G)(	
							);

		/**	
		 */
		uint8_t	(Context::*_getWHO_AM_I)(	
							);

		/**	
		 */
		void	(Context::*_setINT2_CTRL)(	
							uint8_t	value
							);

		/**	
		 */
		uint8_t	(Context::*_getINT2_CTRL)(	
							);

		/**	
		 */
		void	(Context::*_setINT1_CTRL)(	
							uint8_t	value
							);

		/**	
		 */
		uint8_t	(Context::*_getINT1_CTRL)(	
							);

		/**	
		 */
		void	(Context::*_setREFERENCE_G)(	
							uint8_t	value
							);

		/**	
		 */
		uint8_t	(Context::*_getREFERENCE_G)(	
							);

		/**	
		 */
		void	(Context::*_setINT_GEN_DUR_XL)(	
							uint8_t	value
							);

		/**	
		 */
		uint8_t	(Context::*_getINT_GEN_DUR_XL)(	
							);

		/**	
		 */
		void	(Context::*_setINT_GEN_THS_Z_XL)(	
							uint8_t	value
							);

		/**	
		 */
		uint8_t	(Context::*_getINT_GEN_THS_Z_XL)(	
							);

		/**	
		 */
		void	(Context::*_setINT_GEN_THS_Y_XL)(	
							uint8_t	value
							);

		/**	
		 */
		uint8_t	(Context::*_getINT_GEN_THS_Y_XL)(	
							);

		/**	
		 */
		void	(Context::*_setINT_GEN_THS_X_XL)(	
							uint8_t	value
							);

		/**	
		 */
		uint8_t	(Context::*_getINT_GEN_THS_X_XL)(	
							);

		/**	
		 */
		void	(Context::*_setINT_GEN_CFG_XL)(	
							uint8_t	value
							);

		/**	
		 */
		uint8_t	(Context::*_getINT_GEN_CFG_XL)(	
							);

		/**	
		 */
		void	(Context::*_setACT_DUR)(	
							uint8_t	value
							);

		/**	
		 */
		uint8_t	(Context::*_getACT_DUR)(	
							);

		/**	
		 */
		void	(Context::*_setACT_THS)(	
							uint8_t	value
							);

		/**	
		 */
		uint8_t	(Context::*_getACT_THS)(	
							);

	public:
		/** */
		Composer(
			Context&		context,
			void	(Context::*getOUT_TEMP)(	
								uint8_t	maxBufferLength,
								void*	buffer
								),
			void	(Context::*getOUT_G)(	
								uint8_t	maxBufferLength,
								void*	buffer
								),
			void	(Context::*getOUT_XL)(	
								uint8_t	maxBufferLength,
								void*	buffer
								),
			void	(Context::*setINT_GEN_DUR_G)(	
								uint8_t	value
								),
			uint8_t	(Context::*getINT_GEN_DUR_G)(	
								),
			void	(Context::*setINT_GEN_THS_ZL_G)(	
								uint8_t	value
								),
			uint8_t	(Context::*getINT_GEN_THS_ZL_G)(	
								),
			void	(Context::*setINT_GEN_THS_ZH_G)(	
								uint8_t	value
								),
			uint8_t	(Context::*getINT_GEN_THS_ZH_G)(	
								),
			void	(Context::*setINT_GEN_THS_YL_G)(	
								uint8_t	value
								),
			uint8_t	(Context::*getINT_GEN_THS_YL_G)(	
								),
			void	(Context::*setINT_GEN_THS_YH_G)(	
								uint8_t	value
								),
			uint8_t	(Context::*getINT_GEN_THS_YH_G)(	
								),
			void	(Context::*setINT_GEN_THS_XL_G)(	
								uint8_t	value
								),
			uint8_t	(Context::*getINT_GEN_THS_XL_G)(	
								),
			void	(Context::*setINT_GEN_THS_XH_G)(	
								uint8_t	value
								),
			uint8_t	(Context::*getINT_GEN_THS_XH_G)(	
								),
			void	(Context::*setINT_GEN_CFG_G)(	
								uint8_t	value
								),
			uint8_t	(Context::*getINT_GEN_CFG_G)(	
								),
			uint8_t	(Context::*getFIFO_SRC)(	
								),
			void	(Context::*setFIFO_CTRL)(	
								uint8_t	value
								),
			uint8_t	(Context::*getFIFO_CTRL)(	
								),
			uint8_t	(Context::*getOUT_Z_H_XL)(	
								),
			uint8_t	(Context::*getOUT_Z_L_XL)(	
								),
			uint8_t	(Context::*getOUT_Y_H_XL)(	
								),
			uint8_t	(Context::*getOUT_Y_L_XL)(	
								),
			uint8_t	(Context::*getOUT_X_H_XL)(	
								),
			uint8_t	(Context::*getOUT_X_L_XL)(	
								),
			uint8_t	(Context::*getSTATUS_REG_2)(	
								),
			uint8_t	(Context::*getINT_GEN_SRC_XL)(	
								),
			void	(Context::*setCTRL_REG10)(	
								uint8_t	value
								),
			uint8_t	(Context::*getCTRL_REG10)(	
								),
			void	(Context::*setCTRL_REG9)(	
								uint8_t	value
								),
			uint8_t	(Context::*getCTRL_REG9)(	
								),
			void	(Context::*setCTRL_REG8)(	
								uint8_t	value
								),
			uint8_t	(Context::*getCTRL_REG8)(	
								),
			void	(Context::*setCTRL_REG7_XL)(	
								uint8_t	value
								),
			uint8_t	(Context::*getCTRL_REG7_XL)(	
								),
			void	(Context::*setCTRL_REG6_XL)(	
								uint8_t	value
								),
			uint8_t	(Context::*getCTRL_REG6_XL)(	
								),
			void	(Context::*setCTRL_REG5_XL)(	
								uint8_t	value
								),
			uint8_t	(Context::*getCTRL_REG5_XL)(	
								),
			void	(Context::*setCTRL_REG4)(	
								uint8_t	value
								),
			uint8_t	(Context::*getCTRL_REG4)(	
								),
			uint8_t	(Context::*getOUT_Z_H_G)(	
								),
			uint8_t	(Context::*getOUT_Z_L_G)(	
								),
			uint8_t	(Context::*getOUT_Y_H_G)(	
								),
			uint8_t	(Context::*getOUT_Y_L_G)(	
								),
			uint8_t	(Context::*getOUT_X_L_G)(	
								),
			uint8_t	(Context::*getSTATUS_REG)(	
								),
			uint8_t	(Context::*getOUT_TEMP_H)(	
								),
			uint8_t	(Context::*getOUT_TEMP_L)(	
								),
			uint8_t	(Context::*getINT_GEN_SRC_G)(	
								),
			void	(Context::*setORIENT_CFG_G)(	
								uint8_t	value
								),
			uint8_t	(Context::*getORIENT_CFG_G)(	
								),
			void	(Context::*setCTRL_REG3_G)(	
								uint8_t	value
								),
			uint8_t	(Context::*getCTRL_REG3_G)(	
								),
			void	(Context::*setCTRL_REG2_G)(	
								uint8_t	value
								),
			uint8_t	(Context::*getCTRL_REG2_G)(	
								),
			void	(Context::*setCTRL_REG1_G)(	
								uint8_t	value
								),
			uint8_t	(Context::*getCTRL_REG1_G)(	
								),
			uint8_t	(Context::*getWHO_AM_I)(	
								),
			void	(Context::*setINT2_CTRL)(	
								uint8_t	value
								),
			uint8_t	(Context::*getINT2_CTRL)(	
								),
			void	(Context::*setINT1_CTRL)(	
								uint8_t	value
								),
			uint8_t	(Context::*getINT1_CTRL)(	
								),
			void	(Context::*setREFERENCE_G)(	
								uint8_t	value
								),
			uint8_t	(Context::*getREFERENCE_G)(	
								),
			void	(Context::*setINT_GEN_DUR_XL)(	
								uint8_t	value
								),
			uint8_t	(Context::*getINT_GEN_DUR_XL)(	
								),
			void	(Context::*setINT_GEN_THS_Z_XL)(	
								uint8_t	value
								),
			uint8_t	(Context::*getINT_GEN_THS_Z_XL)(	
								),
			void	(Context::*setINT_GEN_THS_Y_XL)(	
								uint8_t	value
								),
			uint8_t	(Context::*getINT_GEN_THS_Y_XL)(	
								),
			void	(Context::*setINT_GEN_THS_X_XL)(	
								uint8_t	value
								),
			uint8_t	(Context::*getINT_GEN_THS_X_XL)(	
								),
			void	(Context::*setINT_GEN_CFG_XL)(	
								uint8_t	value
								),
			uint8_t	(Context::*getINT_GEN_CFG_XL)(	
								),
			void	(Context::*setACT_DUR)(	
								uint8_t	value
								),
			uint8_t	(Context::*getACT_DUR)(	
								),
			void	(Context::*setACT_THS)(	
								uint8_t	value
								),
			uint8_t	(Context::*getACT_THS)(	
								)
			) noexcept;

	private:
		/**	
		 */
		void	getOUT_TEMP(	
							uint8_t	maxBufferLength,
							void*	buffer
							) noexcept;

		/**	
		 */
		void	getOUT_G(	
							uint8_t	maxBufferLength,
							void*	buffer
							) noexcept;

		/**	
		 */
		void	getOUT_XL(	
							uint8_t	maxBufferLength,
							void*	buffer
							) noexcept;

		/**	
		 */
		void	setINT_GEN_DUR_G(	
							uint8_t	value
							) noexcept;

		/**	
		 */
		uint8_t	getINT_GEN_DUR_G(	
							) noexcept;

		/**	
		 */
		void	setINT_GEN_THS_ZL_G(	
							uint8_t	value
							) noexcept;

		/**	
		 */
		uint8_t	getINT_GEN_THS_ZL_G(	
							) noexcept;

		/**	
		 */
		void	setINT_GEN_THS_ZH_G(	
							uint8_t	value
							) noexcept;

		/**	
		 */
		uint8_t	getINT_GEN_THS_ZH_G(	
							) noexcept;

		/**	
		 */
		void	setINT_GEN_THS_YL_G(	
							uint8_t	value
							) noexcept;

		/**	
		 */
		uint8_t	getINT_GEN_THS_YL_G(	
							) noexcept;

		/**	
		 */
		void	setINT_GEN_THS_YH_G(	
							uint8_t	value
							) noexcept;

		/**	
		 */
		uint8_t	getINT_GEN_THS_YH_G(	
							) noexcept;

		/**	
		 */
		void	setINT_GEN_THS_XL_G(	
							uint8_t	value
							) noexcept;

		/**	
		 */
		uint8_t	getINT_GEN_THS_XL_G(	
							) noexcept;

		/**	
		 */
		void	setINT_GEN_THS_XH_G(	
							uint8_t	value
							) noexcept;

		/**	
		 */
		uint8_t	getINT_GEN_THS_XH_G(	
							) noexcept;

		/**	
		 */
		void	setINT_GEN_CFG_G(	
							uint8_t	value
							) noexcept;

		/**	
		 */
		uint8_t	getINT_GEN_CFG_G(	
							) noexcept;

		/**	
		 */
		uint8_t	getFIFO_SRC(	
							) noexcept;

		/**	
		 */
		void	setFIFO_CTRL(	
							uint8_t	value
							) noexcept;

		/**	
		 */
		uint8_t	getFIFO_CTRL(	
							) noexcept;

		/**	
		 */
		uint8_t	getOUT_Z_H_XL(	
							) noexcept;

		/**	
		 */
		uint8_t	getOUT_Z_L_XL(	
							) noexcept;

		/**	
		 */
		uint8_t	getOUT_Y_H_XL(	
							) noexcept;

		/**	
		 */
		uint8_t	getOUT_Y_L_XL(	
							) noexcept;

		/**	
		 */
		uint8_t	getOUT_X_H_XL(	
							) noexcept;

		/**	
		 */
		uint8_t	getOUT_X_L_XL(	
							) noexcept;

		/**	
		 */
		uint8_t	getSTATUS_REG_2(	
							) noexcept;

		/**	
		 */
		uint8_t	getINT_GEN_SRC_XL(	
							) noexcept;

		/**	
		 */
		void	setCTRL_REG10(	
							uint8_t	value
							) noexcept;

		/**	
		 */
		uint8_t	getCTRL_REG10(	
							) noexcept;

		/**	
		 */
		void	setCTRL_REG9(	
							uint8_t	value
							) noexcept;

		/**	
		 */
		uint8_t	getCTRL_REG9(	
							) noexcept;

		/**	
		 */
		void	setCTRL_REG8(	
							uint8_t	value
							) noexcept;

		/**	
		 */
		uint8_t	getCTRL_REG8(	
							) noexcept;

		/**	
		 */
		void	setCTRL_REG7_XL(	
							uint8_t	value
							) noexcept;

		/**	
		 */
		uint8_t	getCTRL_REG7_XL(	
							) noexcept;

		/**	
		 */
		void	setCTRL_REG6_XL(	
							uint8_t	value
							) noexcept;

		/**	
		 */
		uint8_t	getCTRL_REG6_XL(	
							) noexcept;

		/**	
		 */
		void	setCTRL_REG5_XL(	
							uint8_t	value
							) noexcept;

		/**	
		 */
		uint8_t	getCTRL_REG5_XL(	
							) noexcept;

		/**	
		 */
		void	setCTRL_REG4(	
							uint8_t	value
							) noexcept;

		/**	
		 */
		uint8_t	getCTRL_REG4(	
							) noexcept;

		/**	
		 */
		uint8_t	getOUT_Z_H_G(	
							) noexcept;

		/**	
		 */
		uint8_t	getOUT_Z_L_G(	
							) noexcept;

		/**	
		 */
		uint8_t	getOUT_Y_H_G(	
							) noexcept;

		/**	
		 */
		uint8_t	getOUT_Y_L_G(	
							) noexcept;

		/**	
		 */
		uint8_t	getOUT_X_L_G(	
							) noexcept;

		/**	
		 */
		uint8_t	getSTATUS_REG(	
							) noexcept;

		/**	
		 */
		uint8_t	getOUT_TEMP_H(	
							) noexcept;

		/**	
		 */
		uint8_t	getOUT_TEMP_L(	
							) noexcept;

		/**	
		 */
		uint8_t	getINT_GEN_SRC_G(	
							) noexcept;

		/**	
		 */
		void	setORIENT_CFG_G(	
							uint8_t	value
							) noexcept;

		/**	
		 */
		uint8_t	getORIENT_CFG_G(	
							) noexcept;

		/**	
		 */
		void	setCTRL_REG3_G(	
							uint8_t	value
							) noexcept;

		/**	
		 */
		uint8_t	getCTRL_REG3_G(	
							) noexcept;

		/**	
		 */
		void	setCTRL_REG2_G(	
							uint8_t	value
							) noexcept;

		/**	
		 */
		uint8_t	getCTRL_REG2_G(	
							) noexcept;

		/**	
		 */
		void	setCTRL_REG1_G(	
							uint8_t	value
							) noexcept;

		/**	
		 */
		uint8_t	getCTRL_REG1_G(	
							) noexcept;

		/**	
		 */
		uint8_t	getWHO_AM_I(	
							) noexcept;

		/**	
		 */
		void	setINT2_CTRL(	
							uint8_t	value
							) noexcept;

		/**	
		 */
		uint8_t	getINT2_CTRL(	
							) noexcept;

		/**	
		 */
		void	setINT1_CTRL(	
							uint8_t	value
							) noexcept;

		/**	
		 */
		uint8_t	getINT1_CTRL(	
							) noexcept;

		/**	
		 */
		void	setREFERENCE_G(	
							uint8_t	value
							) noexcept;

		/**	
		 */
		uint8_t	getREFERENCE_G(	
							) noexcept;

		/**	
		 */
		void	setINT_GEN_DUR_XL(	
							uint8_t	value
							) noexcept;

		/**	
		 */
		uint8_t	getINT_GEN_DUR_XL(	
							) noexcept;

		/**	
		 */
		void	setINT_GEN_THS_Z_XL(	
							uint8_t	value
							) noexcept;

		/**	
		 */
		uint8_t	getINT_GEN_THS_Z_XL(	
							) noexcept;

		/**	
		 */
		void	setINT_GEN_THS_Y_XL(	
							uint8_t	value
							) noexcept;

		/**	
		 */
		uint8_t	getINT_GEN_THS_Y_XL(	
							) noexcept;

		/**	
		 */
		void	setINT_GEN_THS_X_XL(	
							uint8_t	value
							) noexcept;

		/**	
		 */
		uint8_t	getINT_GEN_THS_X_XL(	
							) noexcept;

		/**	
		 */
		void	setINT_GEN_CFG_XL(	
							uint8_t	value
							) noexcept;

		/**	
		 */
		uint8_t	getINT_GEN_CFG_XL(	
							) noexcept;

		/**	
		 */
		void	setACT_DUR(	
							uint8_t	value
							) noexcept;

		/**	
		 */
		uint8_t	getACT_DUR(	
							) noexcept;

		/**	
		 */
		void	setACT_THS(	
							uint8_t	value
							) noexcept;

		/**	
		 */
		uint8_t	getACT_THS(	
							) noexcept;

	};

template <class Context>
Composer<Context>::Composer(
			Context&		context,
			void	(Context::*getOUT_TEMP)(	
								uint8_t	maxBufferLength,
								void*	buffer
								),
			void	(Context::*getOUT_G)(	
								uint8_t	maxBufferLength,
								void*	buffer
								),
			void	(Context::*getOUT_XL)(	
								uint8_t	maxBufferLength,
								void*	buffer
								),
			void	(Context::*setINT_GEN_DUR_G)(	
								uint8_t	value
								),
			uint8_t	(Context::*getINT_GEN_DUR_G)(	
								),
			void	(Context::*setINT_GEN_THS_ZL_G)(	
								uint8_t	value
								),
			uint8_t	(Context::*getINT_GEN_THS_ZL_G)(	
								),
			void	(Context::*setINT_GEN_THS_ZH_G)(	
								uint8_t	value
								),
			uint8_t	(Context::*getINT_GEN_THS_ZH_G)(	
								),
			void	(Context::*setINT_GEN_THS_YL_G)(	
								uint8_t	value
								),
			uint8_t	(Context::*getINT_GEN_THS_YL_G)(	
								),
			void	(Context::*setINT_GEN_THS_YH_G)(	
								uint8_t	value
								),
			uint8_t	(Context::*getINT_GEN_THS_YH_G)(	
								),
			void	(Context::*setINT_GEN_THS_XL_G)(	
								uint8_t	value
								),
			uint8_t	(Context::*getINT_GEN_THS_XL_G)(	
								),
			void	(Context::*setINT_GEN_THS_XH_G)(	
								uint8_t	value
								),
			uint8_t	(Context::*getINT_GEN_THS_XH_G)(	
								),
			void	(Context::*setINT_GEN_CFG_G)(	
								uint8_t	value
								),
			uint8_t	(Context::*getINT_GEN_CFG_G)(	
								),
			uint8_t	(Context::*getFIFO_SRC)(	
								),
			void	(Context::*setFIFO_CTRL)(	
								uint8_t	value
								),
			uint8_t	(Context::*getFIFO_CTRL)(	
								),
			uint8_t	(Context::*getOUT_Z_H_XL)(	
								),
			uint8_t	(Context::*getOUT_Z_L_XL)(	
								),
			uint8_t	(Context::*getOUT_Y_H_XL)(	
								),
			uint8_t	(Context::*getOUT_Y_L_XL)(	
								),
			uint8_t	(Context::*getOUT_X_H_XL)(	
								),
			uint8_t	(Context::*getOUT_X_L_XL)(	
								),
			uint8_t	(Context::*getSTATUS_REG_2)(	
								),
			uint8_t	(Context::*getINT_GEN_SRC_XL)(	
								),
			void	(Context::*setCTRL_REG10)(	
								uint8_t	value
								),
			uint8_t	(Context::*getCTRL_REG10)(	
								),
			void	(Context::*setCTRL_REG9)(	
								uint8_t	value
								),
			uint8_t	(Context::*getCTRL_REG9)(	
								),
			void	(Context::*setCTRL_REG8)(	
								uint8_t	value
								),
			uint8_t	(Context::*getCTRL_REG8)(	
								),
			void	(Context::*setCTRL_REG7_XL)(	
								uint8_t	value
								),
			uint8_t	(Context::*getCTRL_REG7_XL)(	
								),
			void	(Context::*setCTRL_REG6_XL)(	
								uint8_t	value
								),
			uint8_t	(Context::*getCTRL_REG6_XL)(	
								),
			void	(Context::*setCTRL_REG5_XL)(	
								uint8_t	value
								),
			uint8_t	(Context::*getCTRL_REG5_XL)(	
								),
			void	(Context::*setCTRL_REG4)(	
								uint8_t	value
								),
			uint8_t	(Context::*getCTRL_REG4)(	
								),
			uint8_t	(Context::*getOUT_Z_H_G)(	
								),
			uint8_t	(Context::*getOUT_Z_L_G)(	
								),
			uint8_t	(Context::*getOUT_Y_H_G)(	
								),
			uint8_t	(Context::*getOUT_Y_L_G)(	
								),
			uint8_t	(Context::*getOUT_X_L_G)(	
								),
			uint8_t	(Context::*getSTATUS_REG)(	
								),
			uint8_t	(Context::*getOUT_TEMP_H)(	
								),
			uint8_t	(Context::*getOUT_TEMP_L)(	
								),
			uint8_t	(Context::*getINT_GEN_SRC_G)(	
								),
			void	(Context::*setORIENT_CFG_G)(	
								uint8_t	value
								),
			uint8_t	(Context::*getORIENT_CFG_G)(	
								),
			void	(Context::*setCTRL_REG3_G)(	
								uint8_t	value
								),
			uint8_t	(Context::*getCTRL_REG3_G)(	
								),
			void	(Context::*setCTRL_REG2_G)(	
								uint8_t	value
								),
			uint8_t	(Context::*getCTRL_REG2_G)(	
								),
			void	(Context::*setCTRL_REG1_G)(	
								uint8_t	value
								),
			uint8_t	(Context::*getCTRL_REG1_G)(	
								),
			uint8_t	(Context::*getWHO_AM_I)(	
								),
			void	(Context::*setINT2_CTRL)(	
								uint8_t	value
								),
			uint8_t	(Context::*getINT2_CTRL)(	
								),
			void	(Context::*setINT1_CTRL)(	
								uint8_t	value
								),
			uint8_t	(Context::*getINT1_CTRL)(	
								),
			void	(Context::*setREFERENCE_G)(	
								uint8_t	value
								),
			uint8_t	(Context::*getREFERENCE_G)(	
								),
			void	(Context::*setINT_GEN_DUR_XL)(	
								uint8_t	value
								),
			uint8_t	(Context::*getINT_GEN_DUR_XL)(	
								),
			void	(Context::*setINT_GEN_THS_Z_XL)(	
								uint8_t	value
								),
			uint8_t	(Context::*getINT_GEN_THS_Z_XL)(	
								),
			void	(Context::*setINT_GEN_THS_Y_XL)(	
								uint8_t	value
								),
			uint8_t	(Context::*getINT_GEN_THS_Y_XL)(	
								),
			void	(Context::*setINT_GEN_THS_X_XL)(	
								uint8_t	value
								),
			uint8_t	(Context::*getINT_GEN_THS_X_XL)(	
								),
			void	(Context::*setINT_GEN_CFG_XL)(	
								uint8_t	value
								),
			uint8_t	(Context::*getINT_GEN_CFG_XL)(	
								),
			void	(Context::*setACT_DUR)(	
								uint8_t	value
								),
			uint8_t	(Context::*getACT_DUR)(	
								),
			void	(Context::*setACT_THS)(	
								uint8_t	value
								),
			uint8_t	(Context::*getACT_THS)(	
								)
			) noexcept:
		_context(context),
		_getOUT_TEMP(getOUT_TEMP),
		_getOUT_G(getOUT_G),
		_getOUT_XL(getOUT_XL),
		_setINT_GEN_DUR_G(setINT_GEN_DUR_G),
		_getINT_GEN_DUR_G(getINT_GEN_DUR_G),
		_setINT_GEN_THS_ZL_G(setINT_GEN_THS_ZL_G),
		_getINT_GEN_THS_ZL_G(getINT_GEN_THS_ZL_G),
		_setINT_GEN_THS_ZH_G(setINT_GEN_THS_ZH_G),
		_getINT_GEN_THS_ZH_G(getINT_GEN_THS_ZH_G),
		_setINT_GEN_THS_YL_G(setINT_GEN_THS_YL_G),
		_getINT_GEN_THS_YL_G(getINT_GEN_THS_YL_G),
		_setINT_GEN_THS_YH_G(setINT_GEN_THS_YH_G),
		_getINT_GEN_THS_YH_G(getINT_GEN_THS_YH_G),
		_setINT_GEN_THS_XL_G(setINT_GEN_THS_XL_G),
		_getINT_GEN_THS_XL_G(getINT_GEN_THS_XL_G),
		_setINT_GEN_THS_XH_G(setINT_GEN_THS_XH_G),
		_getINT_GEN_THS_XH_G(getINT_GEN_THS_XH_G),
		_setINT_GEN_CFG_G(setINT_GEN_CFG_G),
		_getINT_GEN_CFG_G(getINT_GEN_CFG_G),
		_getFIFO_SRC(getFIFO_SRC),
		_setFIFO_CTRL(setFIFO_CTRL),
		_getFIFO_CTRL(getFIFO_CTRL),
		_getOUT_Z_H_XL(getOUT_Z_H_XL),
		_getOUT_Z_L_XL(getOUT_Z_L_XL),
		_getOUT_Y_H_XL(getOUT_Y_H_XL),
		_getOUT_Y_L_XL(getOUT_Y_L_XL),
		_getOUT_X_H_XL(getOUT_X_H_XL),
		_getOUT_X_L_XL(getOUT_X_L_XL),
		_getSTATUS_REG_2(getSTATUS_REG_2),
		_getINT_GEN_SRC_XL(getINT_GEN_SRC_XL),
		_setCTRL_REG10(setCTRL_REG10),
		_getCTRL_REG10(getCTRL_REG10),
		_setCTRL_REG9(setCTRL_REG9),
		_getCTRL_REG9(getCTRL_REG9),
		_setCTRL_REG8(setCTRL_REG8),
		_getCTRL_REG8(getCTRL_REG8),
		_setCTRL_REG7_XL(setCTRL_REG7_XL),
		_getCTRL_REG7_XL(getCTRL_REG7_XL),
		_setCTRL_REG6_XL(setCTRL_REG6_XL),
		_getCTRL_REG6_XL(getCTRL_REG6_XL),
		_setCTRL_REG5_XL(setCTRL_REG5_XL),
		_getCTRL_REG5_XL(getCTRL_REG5_XL),
		_setCTRL_REG4(setCTRL_REG4),
		_getCTRL_REG4(getCTRL_REG4),
		_getOUT_Z_H_G(getOUT_Z_H_G),
		_getOUT_Z_L_G(getOUT_Z_L_G),
		_getOUT_Y_H_G(getOUT_Y_H_G),
		_getOUT_Y_L_G(getOUT_Y_L_G),
		_getOUT_X_L_G(getOUT_X_L_G),
		_getSTATUS_REG(getSTATUS_REG),
		_getOUT_TEMP_H(getOUT_TEMP_H),
		_getOUT_TEMP_L(getOUT_TEMP_L),
		_getINT_GEN_SRC_G(getINT_GEN_SRC_G),
		_setORIENT_CFG_G(setORIENT_CFG_G),
		_getORIENT_CFG_G(getORIENT_CFG_G),
		_setCTRL_REG3_G(setCTRL_REG3_G),
		_getCTRL_REG3_G(getCTRL_REG3_G),
		_setCTRL_REG2_G(setCTRL_REG2_G),
		_getCTRL_REG2_G(getCTRL_REG2_G),
		_setCTRL_REG1_G(setCTRL_REG1_G),
		_getCTRL_REG1_G(getCTRL_REG1_G),
		_getWHO_AM_I(getWHO_AM_I),
		_setINT2_CTRL(setINT2_CTRL),
		_getINT2_CTRL(getINT2_CTRL),
		_setINT1_CTRL(setINT1_CTRL),
		_getINT1_CTRL(getINT1_CTRL),
		_setREFERENCE_G(setREFERENCE_G),
		_getREFERENCE_G(getREFERENCE_G),
		_setINT_GEN_DUR_XL(setINT_GEN_DUR_XL),
		_getINT_GEN_DUR_XL(getINT_GEN_DUR_XL),
		_setINT_GEN_THS_Z_XL(setINT_GEN_THS_Z_XL),
		_getINT_GEN_THS_Z_XL(getINT_GEN_THS_Z_XL),
		_setINT_GEN_THS_Y_XL(setINT_GEN_THS_Y_XL),
		_getINT_GEN_THS_Y_XL(getINT_GEN_THS_Y_XL),
		_setINT_GEN_THS_X_XL(setINT_GEN_THS_X_XL),
		_getINT_GEN_THS_X_XL(getINT_GEN_THS_X_XL),
		_setINT_GEN_CFG_XL(setINT_GEN_CFG_XL),
		_getINT_GEN_CFG_XL(getINT_GEN_CFG_XL),
		_setACT_DUR(setACT_DUR),
		_getACT_DUR(getACT_DUR),
		_setACT_THS(setACT_THS),
		_getACT_THS(getACT_THS)
		{
	}

template <class Context>
void	Composer<Context>::getOUT_TEMP(	
							uint8_t	maxBufferLength,
							void*	buffer
							) noexcept{

	return (_context.*_getOUT_TEMP)(
				maxBufferLength,
				buffer
				);
	}

template <class Context>
void	Composer<Context>::getOUT_G(	
							uint8_t	maxBufferLength,
							void*	buffer
							) noexcept{

	return (_context.*_getOUT_G)(
				maxBufferLength,
				buffer
				);
	}

template <class Context>
void	Composer<Context>::getOUT_XL(	
							uint8_t	maxBufferLength,
							void*	buffer
							) noexcept{

	return (_context.*_getOUT_XL)(
				maxBufferLength,
				buffer
				);
	}

template <class Context>
void	Composer<Context>::setINT_GEN_DUR_G(	
							uint8_t	value
							) noexcept{

	return (_context.*_setINT_GEN_DUR_G)(
				value
				);
	}

template <class Context>
uint8_t	Composer<Context>::getINT_GEN_DUR_G(	
							) noexcept{

	return (_context.*_getINT_GEN_DUR_G)(
				);
	}

template <class Context>
void	Composer<Context>::setINT_GEN_THS_ZL_G(	
							uint8_t	value
							) noexcept{

	return (_context.*_setINT_GEN_THS_ZL_G)(
				value
				);
	}

template <class Context>
uint8_t	Composer<Context>::getINT_GEN_THS_ZL_G(	
							) noexcept{

	return (_context.*_getINT_GEN_THS_ZL_G)(
				);
	}

template <class Context>
void	Composer<Context>::setINT_GEN_THS_ZH_G(	
							uint8_t	value
							) noexcept{

	return (_context.*_setINT_GEN_THS_ZH_G)(
				value
				);
	}

template <class Context>
uint8_t	Composer<Context>::getINT_GEN_THS_ZH_G(	
							) noexcept{

	return (_context.*_getINT_GEN_THS_ZH_G)(
				);
	}

template <class Context>
void	Composer<Context>::setINT_GEN_THS_YL_G(	
							uint8_t	value
							) noexcept{

	return (_context.*_setINT_GEN_THS_YL_G)(
				value
				);
	}

template <class Context>
uint8_t	Composer<Context>::getINT_GEN_THS_YL_G(	
							) noexcept{

	return (_context.*_getINT_GEN_THS_YL_G)(
				);
	}

template <class Context>
void	Composer<Context>::setINT_GEN_THS_YH_G(	
							uint8_t	value
							) noexcept{

	return (_context.*_setINT_GEN_THS_YH_G)(
				value
				);
	}

template <class Context>
uint8_t	Composer<Context>::getINT_GEN_THS_YH_G(	
							) noexcept{

	return (_context.*_getINT_GEN_THS_YH_G)(
				);
	}

template <class Context>
void	Composer<Context>::setINT_GEN_THS_XL_G(	
							uint8_t	value
							) noexcept{

	return (_context.*_setINT_GEN_THS_XL_G)(
				value
				);
	}

template <class Context>
uint8_t	Composer<Context>::getINT_GEN_THS_XL_G(	
							) noexcept{

	return (_context.*_getINT_GEN_THS_XL_G)(
				);
	}

template <class Context>
void	Composer<Context>::setINT_GEN_THS_XH_G(	
							uint8_t	value
							) noexcept{

	return (_context.*_setINT_GEN_THS_XH_G)(
				value
				);
	}

template <class Context>
uint8_t	Composer<Context>::getINT_GEN_THS_XH_G(	
							) noexcept{

	return (_context.*_getINT_GEN_THS_XH_G)(
				);
	}

template <class Context>
void	Composer<Context>::setINT_GEN_CFG_G(	
							uint8_t	value
							) noexcept{

	return (_context.*_setINT_GEN_CFG_G)(
				value
				);
	}

template <class Context>
uint8_t	Composer<Context>::getINT_GEN_CFG_G(	
							) noexcept{

	return (_context.*_getINT_GEN_CFG_G)(
				);
	}

template <class Context>
uint8_t	Composer<Context>::getFIFO_SRC(	
							) noexcept{

	return (_context.*_getFIFO_SRC)(
				);
	}

template <class Context>
void	Composer<Context>::setFIFO_CTRL(	
							uint8_t	value
							) noexcept{

	return (_context.*_setFIFO_CTRL)(
				value
				);
	}

template <class Context>
uint8_t	Composer<Context>::getFIFO_CTRL(	
							) noexcept{

	return (_context.*_getFIFO_CTRL)(
				);
	}

template <class Context>
uint8_t	Composer<Context>::getOUT_Z_H_XL(	
							) noexcept{

	return (_context.*_getOUT_Z_H_XL)(
				);
	}

template <class Context>
uint8_t	Composer<Context>::getOUT_Z_L_XL(	
							) noexcept{

	return (_context.*_getOUT_Z_L_XL)(
				);
	}

template <class Context>
uint8_t	Composer<Context>::getOUT_Y_H_XL(	
							) noexcept{

	return (_context.*_getOUT_Y_H_XL)(
				);
	}

template <class Context>
uint8_t	Composer<Context>::getOUT_Y_L_XL(	
							) noexcept{

	return (_context.*_getOUT_Y_L_XL)(
				);
	}

template <class Context>
uint8_t	Composer<Context>::getOUT_X_H_XL(	
							) noexcept{

	return (_context.*_getOUT_X_H_XL)(
				);
	}

template <class Context>
uint8_t	Composer<Context>::getOUT_X_L_XL(	
							) noexcept{

	return (_context.*_getOUT_X_L_XL)(
				);
	}

template <class Context>
uint8_t	Composer<Context>::getSTATUS_REG_2(	
							) noexcept{

	return (_context.*_getSTATUS_REG_2)(
				);
	}

template <class Context>
uint8_t	Composer<Context>::getINT_GEN_SRC_XL(	
							) noexcept{

	return (_context.*_getINT_GEN_SRC_XL)(
				);
	}

template <class Context>
void	Composer<Context>::setCTRL_REG10(	
							uint8_t	value
							) noexcept{

	return (_context.*_setCTRL_REG10)(
				value
				);
	}

template <class Context>
uint8_t	Composer<Context>::getCTRL_REG10(	
							) noexcept{

	return (_context.*_getCTRL_REG10)(
				);
	}

template <class Context>
void	Composer<Context>::setCTRL_REG9(	
							uint8_t	value
							) noexcept{

	return (_context.*_setCTRL_REG9)(
				value
				);
	}

template <class Context>
uint8_t	Composer<Context>::getCTRL_REG9(	
							) noexcept{

	return (_context.*_getCTRL_REG9)(
				);
	}

template <class Context>
void	Composer<Context>::setCTRL_REG8(	
							uint8_t	value
							) noexcept{

	return (_context.*_setCTRL_REG8)(
				value
				);
	}

template <class Context>
uint8_t	Composer<Context>::getCTRL_REG8(	
							) noexcept{

	return (_context.*_getCTRL_REG8)(
				);
	}

template <class Context>
void	Composer<Context>::setCTRL_REG7_XL(	
							uint8_t	value
							) noexcept{

	return (_context.*_setCTRL_REG7_XL)(
				value
				);
	}

template <class Context>
uint8_t	Composer<Context>::getCTRL_REG7_XL(	
							) noexcept{

	return (_context.*_getCTRL_REG7_XL)(
				);
	}

template <class Context>
void	Composer<Context>::setCTRL_REG6_XL(	
							uint8_t	value
							) noexcept{

	return (_context.*_setCTRL_REG6_XL)(
				value
				);
	}

template <class Context>
uint8_t	Composer<Context>::getCTRL_REG6_XL(	
							) noexcept{

	return (_context.*_getCTRL_REG6_XL)(
				);
	}

template <class Context>
void	Composer<Context>::setCTRL_REG5_XL(	
							uint8_t	value
							) noexcept{

	return (_context.*_setCTRL_REG5_XL)(
				value
				);
	}

template <class Context>
uint8_t	Composer<Context>::getCTRL_REG5_XL(	
							) noexcept{

	return (_context.*_getCTRL_REG5_XL)(
				);
	}

template <class Context>
void	Composer<Context>::setCTRL_REG4(	
							uint8_t	value
							) noexcept{

	return (_context.*_setCTRL_REG4)(
				value
				);
	}

template <class Context>
uint8_t	Composer<Context>::getCTRL_REG4(	
							) noexcept{

	return (_context.*_getCTRL_REG4)(
				);
	}

template <class Context>
uint8_t	Composer<Context>::getOUT_Z_H_G(	
							) noexcept{

	return (_context.*_getOUT_Z_H_G)(
				);
	}

template <class Context>
uint8_t	Composer<Context>::getOUT_Z_L_G(	
							) noexcept{

	return (_context.*_getOUT_Z_L_G)(
				);
	}

template <class Context>
uint8_t	Composer<Context>::getOUT_Y_H_G(	
							) noexcept{

	return (_context.*_getOUT_Y_H_G)(
				);
	}

template <class Context>
uint8_t	Composer<Context>::getOUT_Y_L_G(	
							) noexcept{

	return (_context.*_getOUT_Y_L_G)(
				);
	}

template <class Context>
uint8_t	Composer<Context>::getOUT_X_L_G(	
							) noexcept{

	return (_context.*_getOUT_X_L_G)(
				);
	}

template <class Context>
uint8_t	Composer<Context>::getSTATUS_REG(	
							) noexcept{

	return (_context.*_getSTATUS_REG)(
				);
	}

template <class Context>
uint8_t	Composer<Context>::getOUT_TEMP_H(	
							) noexcept{

	return (_context.*_getOUT_TEMP_H)(
				);
	}

template <class Context>
uint8_t	Composer<Context>::getOUT_TEMP_L(	
							) noexcept{

	return (_context.*_getOUT_TEMP_L)(
				);
	}

template <class Context>
uint8_t	Composer<Context>::getINT_GEN_SRC_G(	
							) noexcept{

	return (_context.*_getINT_GEN_SRC_G)(
				);
	}

template <class Context>
void	Composer<Context>::setORIENT_CFG_G(	
							uint8_t	value
							) noexcept{

	return (_context.*_setORIENT_CFG_G)(
				value
				);
	}

template <class Context>
uint8_t	Composer<Context>::getORIENT_CFG_G(	
							) noexcept{

	return (_context.*_getORIENT_CFG_G)(
				);
	}

template <class Context>
void	Composer<Context>::setCTRL_REG3_G(	
							uint8_t	value
							) noexcept{

	return (_context.*_setCTRL_REG3_G)(
				value
				);
	}

template <class Context>
uint8_t	Composer<Context>::getCTRL_REG3_G(	
							) noexcept{

	return (_context.*_getCTRL_REG3_G)(
				);
	}

template <class Context>
void	Composer<Context>::setCTRL_REG2_G(	
							uint8_t	value
							) noexcept{

	return (_context.*_setCTRL_REG2_G)(
				value
				);
	}

template <class Context>
uint8_t	Composer<Context>::getCTRL_REG2_G(	
							) noexcept{

	return (_context.*_getCTRL_REG2_G)(
				);
	}

template <class Context>
void	Composer<Context>::setCTRL_REG1_G(	
							uint8_t	value
							) noexcept{

	return (_context.*_setCTRL_REG1_G)(
				value
				);
	}

template <class Context>
uint8_t	Composer<Context>::getCTRL_REG1_G(	
							) noexcept{

	return (_context.*_getCTRL_REG1_G)(
				);
	}

template <class Context>
uint8_t	Composer<Context>::getWHO_AM_I(	
							) noexcept{

	return (_context.*_getWHO_AM_I)(
				);
	}

template <class Context>
void	Composer<Context>::setINT2_CTRL(	
							uint8_t	value
							) noexcept{

	return (_context.*_setINT2_CTRL)(
				value
				);
	}

template <class Context>
uint8_t	Composer<Context>::getINT2_CTRL(	
							) noexcept{

	return (_context.*_getINT2_CTRL)(
				);
	}

template <class Context>
void	Composer<Context>::setINT1_CTRL(	
							uint8_t	value
							) noexcept{

	return (_context.*_setINT1_CTRL)(
				value
				);
	}

template <class Context>
uint8_t	Composer<Context>::getINT1_CTRL(	
							) noexcept{

	return (_context.*_getINT1_CTRL)(
				);
	}

template <class Context>
void	Composer<Context>::setREFERENCE_G(	
							uint8_t	value
							) noexcept{

	return (_context.*_setREFERENCE_G)(
				value
				);
	}

template <class Context>
uint8_t	Composer<Context>::getREFERENCE_G(	
							) noexcept{

	return (_context.*_getREFERENCE_G)(
				);
	}

template <class Context>
void	Composer<Context>::setINT_GEN_DUR_XL(	
							uint8_t	value
							) noexcept{

	return (_context.*_setINT_GEN_DUR_XL)(
				value
				);
	}

template <class Context>
uint8_t	Composer<Context>::getINT_GEN_DUR_XL(	
							) noexcept{

	return (_context.*_getINT_GEN_DUR_XL)(
				);
	}

template <class Context>
void	Composer<Context>::setINT_GEN_THS_Z_XL(	
							uint8_t	value
							) noexcept{

	return (_context.*_setINT_GEN_THS_Z_XL)(
				value
				);
	}

template <class Context>
uint8_t	Composer<Context>::getINT_GEN_THS_Z_XL(	
							) noexcept{

	return (_context.*_getINT_GEN_THS_Z_XL)(
				);
	}

template <class Context>
void	Composer<Context>::setINT_GEN_THS_Y_XL(	
							uint8_t	value
							) noexcept{

	return (_context.*_setINT_GEN_THS_Y_XL)(
				value
				);
	}

template <class Context>
uint8_t	Composer<Context>::getINT_GEN_THS_Y_XL(	
							) noexcept{

	return (_context.*_getINT_GEN_THS_Y_XL)(
				);
	}

template <class Context>
void	Composer<Context>::setINT_GEN_THS_X_XL(	
							uint8_t	value
							) noexcept{

	return (_context.*_setINT_GEN_THS_X_XL)(
				value
				);
	}

template <class Context>
uint8_t	Composer<Context>::getINT_GEN_THS_X_XL(	
							) noexcept{

	return (_context.*_getINT_GEN_THS_X_XL)(
				);
	}

template <class Context>
void	Composer<Context>::setINT_GEN_CFG_XL(	
							uint8_t	value
							) noexcept{

	return (_context.*_setINT_GEN_CFG_XL)(
				value
				);
	}

template <class Context>
uint8_t	Composer<Context>::getINT_GEN_CFG_XL(	
							) noexcept{

	return (_context.*_getINT_GEN_CFG_XL)(
				);
	}

template <class Context>
void	Composer<Context>::setACT_DUR(	
							uint8_t	value
							) noexcept{

	return (_context.*_setACT_DUR)(
				value
				);
	}

template <class Context>
uint8_t	Composer<Context>::getACT_DUR(	
							) noexcept{

	return (_context.*_getACT_DUR)(
				);
	}

template <class Context>
void	Composer<Context>::setACT_THS(	
							uint8_t	value
							) noexcept{

	return (_context.*_setACT_THS)(
				value
				);
	}

template <class Context>
uint8_t	Composer<Context>::getACT_THS(	
							) noexcept{

	return (_context.*_getACT_THS)(
				);
	}

}
}
}
}
}
}
#endif
