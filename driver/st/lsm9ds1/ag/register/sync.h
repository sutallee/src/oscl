/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_driver_st_lsm9ds1_ag_register_itc_synch_
#define _oscl_driver_st_lsm9ds1_ag_register_itc_synch_

#include "api.h"
#include "reqapi.h"

/** */
namespace Oscl {

/** */
namespace Driver {

/** */
namespace ST {

/** */
namespace LSM9DS1 {

/** */
namespace AG {

/** */
namespace Register {

/**	This class implements synchronous ITC for the interface.
 */
class Sync : public Api {
	private:
		/** This SAP is used to identify the server.
		 */
		Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::ConcreteSAP	_sap;

	public:
		/** The constructor requires a reference to the server's
			thread/mailbox (myPapi), and a reference to the server's
			request interface.
		 */
		Sync(	Oscl::Mt::Itc::PostMsgApi&	myPapi,
				Req::Api&				reqApi
				) noexcept;

		/** It is frequently the case that this Sync class is declared
			within a server class. Since the server's clients need access
			to the server's SAP, and since the Sync class implementation
			needs the same information, this operation exports the SAP
			to the server/context such that it can be distributed to clients
			that use the server asynchronously. In this way, there is
			only a single copy of the SAP for each service.
		 */
		Req::Api::SAP&	getSAP() noexcept;

	public:
		/**	
		 */
		void	getOUT_TEMP(	
						uint8_t	maxBufferLength,
						void*	buffer
						) noexcept;

		/**	
		 */
		void	getOUT_G(	
						uint8_t	maxBufferLength,
						void*	buffer
						) noexcept;

		/**	
		 */
		void	getOUT_XL(	
						uint8_t	maxBufferLength,
						void*	buffer
						) noexcept;

		/**	
		 */
		void	setINT_GEN_DUR_G(	
						uint8_t	value
						) noexcept;

		/**	
		 */
		uint8_t	getINT_GEN_DUR_G(	
						) noexcept;

		/**	
		 */
		void	setINT_GEN_THS_ZL_G(	
						uint8_t	value
						) noexcept;

		/**	
		 */
		uint8_t	getINT_GEN_THS_ZL_G(	
						) noexcept;

		/**	
		 */
		void	setINT_GEN_THS_ZH_G(	
						uint8_t	value
						) noexcept;

		/**	
		 */
		uint8_t	getINT_GEN_THS_ZH_G(	
						) noexcept;

		/**	
		 */
		void	setINT_GEN_THS_YL_G(	
						uint8_t	value
						) noexcept;

		/**	
		 */
		uint8_t	getINT_GEN_THS_YL_G(	
						) noexcept;

		/**	
		 */
		void	setINT_GEN_THS_YH_G(	
						uint8_t	value
						) noexcept;

		/**	
		 */
		uint8_t	getINT_GEN_THS_YH_G(	
						) noexcept;

		/**	
		 */
		void	setINT_GEN_THS_XL_G(	
						uint8_t	value
						) noexcept;

		/**	
		 */
		uint8_t	getINT_GEN_THS_XL_G(	
						) noexcept;

		/**	
		 */
		void	setINT_GEN_THS_XH_G(	
						uint8_t	value
						) noexcept;

		/**	
		 */
		uint8_t	getINT_GEN_THS_XH_G(	
						) noexcept;

		/**	
		 */
		void	setINT_GEN_CFG_G(	
						uint8_t	value
						) noexcept;

		/**	
		 */
		uint8_t	getINT_GEN_CFG_G(	
						) noexcept;

		/**	
		 */
		uint8_t	getFIFO_SRC(	
						) noexcept;

		/**	
		 */
		void	setFIFO_CTRL(	
						uint8_t	value
						) noexcept;

		/**	
		 */
		uint8_t	getFIFO_CTRL(	
						) noexcept;

		/**	
		 */
		uint8_t	getOUT_Z_H_XL(	
						) noexcept;

		/**	
		 */
		uint8_t	getOUT_Z_L_XL(	
						) noexcept;

		/**	
		 */
		uint8_t	getOUT_Y_H_XL(	
						) noexcept;

		/**	
		 */
		uint8_t	getOUT_Y_L_XL(	
						) noexcept;

		/**	
		 */
		uint8_t	getOUT_X_H_XL(	
						) noexcept;

		/**	
		 */
		uint8_t	getOUT_X_L_XL(	
						) noexcept;

		/**	
		 */
		uint8_t	getSTATUS_REG_2(	
						) noexcept;

		/**	
		 */
		uint8_t	getINT_GEN_SRC_XL(	
						) noexcept;

		/**	
		 */
		void	setCTRL_REG10(	
						uint8_t	value
						) noexcept;

		/**	
		 */
		uint8_t	getCTRL_REG10(	
						) noexcept;

		/**	
		 */
		void	setCTRL_REG9(	
						uint8_t	value
						) noexcept;

		/**	
		 */
		uint8_t	getCTRL_REG9(	
						) noexcept;

		/**	
		 */
		void	setCTRL_REG8(	
						uint8_t	value
						) noexcept;

		/**	
		 */
		uint8_t	getCTRL_REG8(	
						) noexcept;

		/**	
		 */
		void	setCTRL_REG7_XL(	
						uint8_t	value
						) noexcept;

		/**	
		 */
		uint8_t	getCTRL_REG7_XL(	
						) noexcept;

		/**	
		 */
		void	setCTRL_REG6_XL(	
						uint8_t	value
						) noexcept;

		/**	
		 */
		uint8_t	getCTRL_REG6_XL(	
						) noexcept;

		/**	
		 */
		void	setCTRL_REG5_XL(	
						uint8_t	value
						) noexcept;

		/**	
		 */
		uint8_t	getCTRL_REG5_XL(	
						) noexcept;

		/**	
		 */
		void	setCTRL_REG4(	
						uint8_t	value
						) noexcept;

		/**	
		 */
		uint8_t	getCTRL_REG4(	
						) noexcept;

		/**	
		 */
		uint8_t	getOUT_Z_H_G(	
						) noexcept;

		/**	
		 */
		uint8_t	getOUT_Z_L_G(	
						) noexcept;

		/**	
		 */
		uint8_t	getOUT_Y_H_G(	
						) noexcept;

		/**	
		 */
		uint8_t	getOUT_Y_L_G(	
						) noexcept;

		/**	
		 */
		uint8_t	getOUT_X_L_G(	
						) noexcept;

		/**	
		 */
		uint8_t	getSTATUS_REG(	
						) noexcept;

		/**	
		 */
		uint8_t	getOUT_TEMP_H(	
						) noexcept;

		/**	
		 */
		uint8_t	getOUT_TEMP_L(	
						) noexcept;

		/**	
		 */
		uint8_t	getINT_GEN_SRC_G(	
						) noexcept;

		/**	
		 */
		void	setORIENT_CFG_G(	
						uint8_t	value
						) noexcept;

		/**	
		 */
		uint8_t	getORIENT_CFG_G(	
						) noexcept;

		/**	
		 */
		void	setCTRL_REG3_G(	
						uint8_t	value
						) noexcept;

		/**	
		 */
		uint8_t	getCTRL_REG3_G(	
						) noexcept;

		/**	
		 */
		void	setCTRL_REG2_G(	
						uint8_t	value
						) noexcept;

		/**	
		 */
		uint8_t	getCTRL_REG2_G(	
						) noexcept;

		/**	
		 */
		void	setCTRL_REG1_G(	
						uint8_t	value
						) noexcept;

		/**	
		 */
		uint8_t	getCTRL_REG1_G(	
						) noexcept;

		/**	
		 */
		uint8_t	getWHO_AM_I(	
						) noexcept;

		/**	
		 */
		void	setINT2_CTRL(	
						uint8_t	value
						) noexcept;

		/**	
		 */
		uint8_t	getINT2_CTRL(	
						) noexcept;

		/**	
		 */
		void	setINT1_CTRL(	
						uint8_t	value
						) noexcept;

		/**	
		 */
		uint8_t	getINT1_CTRL(	
						) noexcept;

		/**	
		 */
		void	setREFERENCE_G(	
						uint8_t	value
						) noexcept;

		/**	
		 */
		uint8_t	getREFERENCE_G(	
						) noexcept;

		/**	
		 */
		void	setINT_GEN_DUR_XL(	
						uint8_t	value
						) noexcept;

		/**	
		 */
		uint8_t	getINT_GEN_DUR_XL(	
						) noexcept;

		/**	
		 */
		void	setINT_GEN_THS_Z_XL(	
						uint8_t	value
						) noexcept;

		/**	
		 */
		uint8_t	getINT_GEN_THS_Z_XL(	
						) noexcept;

		/**	
		 */
		void	setINT_GEN_THS_Y_XL(	
						uint8_t	value
						) noexcept;

		/**	
		 */
		uint8_t	getINT_GEN_THS_Y_XL(	
						) noexcept;

		/**	
		 */
		void	setINT_GEN_THS_X_XL(	
						uint8_t	value
						) noexcept;

		/**	
		 */
		uint8_t	getINT_GEN_THS_X_XL(	
						) noexcept;

		/**	
		 */
		void	setINT_GEN_CFG_XL(	
						uint8_t	value
						) noexcept;

		/**	
		 */
		uint8_t	getINT_GEN_CFG_XL(	
						) noexcept;

		/**	
		 */
		void	setACT_DUR(	
						uint8_t	value
						) noexcept;

		/**	
		 */
		uint8_t	getACT_DUR(	
						) noexcept;

		/**	
		 */
		void	setACT_THS(	
						uint8_t	value
						) noexcept;

		/**	
		 */
		uint8_t	getACT_THS(	
						) noexcept;

	};

}
}
}
}
}
}
#endif
