/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_driver_st_lsm9ds1_ag_register_itc_reqapih_
#define _oscl_driver_st_lsm9ds1_ag_register_itc_reqapih_

#include "oscl/mt/itc/mbox/srvreq.h"
#include "oscl/mt/itc/postmsgapi.h"
#include "oscl/mt/itc/mbox/sap.h"
#include <stdint.h>

/** */
namespace Oscl {

/** */
namespace Driver {

/** */
namespace ST {

/** */
namespace LSM9DS1 {

/** */
namespace AG {

/** */
namespace Register {

/** */
namespace Req {

/**	This class describes an ITC server interface. The
	interface includes the definition of messages and their
	associated payloads, as well as an interface that is to be
	implemented by a server that supports this ITC interface.
 */
/**	This interface defines methods to access the LSM9DS1 IMU
	registers indpendent of the type of bus.
 */
class Api {
	public:
		/** This class contains the data that is transfered
			between the client and server within the GetOUT_TEMPReq
			ITC message.
		 */
		class GetOUT_TEMPPayload {
			public:
				/**	
				 */
				uint8_t	_maxBufferLength;

				/**	
				 */
				void*	_buffer;

			public:
				/** The GetOUT_TEMPPayload constructor. */
				GetOUT_TEMPPayload(

					uint8_t	maxBufferLength,
					void*	buffer
					) noexcept;

			};

		/**	Read the OUT_TEMP registers.
		 */
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api,
					GetOUT_TEMPPayload
					>		GetOUT_TEMPReq;

	public:
		/** This class contains the data that is transfered
			between the client and server within the GetOUT_GReq
			ITC message.
		 */
		class GetOUT_GPayload {
			public:
				/**	
				 */
				uint8_t	_maxBufferLength;

				/**	
				 */
				void*	_buffer;

			public:
				/** The GetOUT_GPayload constructor. */
				GetOUT_GPayload(

					uint8_t	maxBufferLength,
					void*	buffer
					) noexcept;

			};

		/**	Read the OUT_<axis>_<octet>_G registers.
		 */
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api,
					GetOUT_GPayload
					>		GetOUT_GReq;

	public:
		/** This class contains the data that is transfered
			between the client and server within the GetOUT_XLReq
			ITC message.
		 */
		class GetOUT_XLPayload {
			public:
				/**	
				 */
				uint8_t	_maxBufferLength;

				/**	
				 */
				void*	_buffer;

			public:
				/** The GetOUT_XLPayload constructor. */
				GetOUT_XLPayload(

					uint8_t	maxBufferLength,
					void*	buffer
					) noexcept;

			};

		/**	Read the OUT_<axis>_<octet>_XL registers.
		 */
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api,
					GetOUT_XLPayload
					>		GetOUT_XLReq;

	public:
		/** This class contains the data that is transfered
			between the client and server within the SetINT_GEN_DUR_GReq
			ITC message.
		 */
		class SetINT_GEN_DUR_GPayload {
			public:
				/**	
				 */
				uint8_t	_value;

			public:
				/** The SetINT_GEN_DUR_GPayload constructor. */
				SetINT_GEN_DUR_GPayload(

					uint8_t	value
					) noexcept;

			};

		/**	Write the INT_GEN_DUR_G register
		 */
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api,
					SetINT_GEN_DUR_GPayload
					>		SetINT_GEN_DUR_GReq;

	public:
		/** This class contains the data that is transfered
			between the client and server within the GetINT_GEN_DUR_GReq
			ITC message.
		 */
		class GetINT_GEN_DUR_GPayload {
			public:
				/**	
				 */
				uint8_t	_value;

			public:
				/** The GetINT_GEN_DUR_GPayload constructor. */
				GetINT_GEN_DUR_GPayload(
					) noexcept;

			};

		/**	Read the INT_GEN_DUR_G register.
		 */
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api,
					GetINT_GEN_DUR_GPayload
					>		GetINT_GEN_DUR_GReq;

	public:
		/** This class contains the data that is transfered
			between the client and server within the SetINT_GEN_THS_ZL_GReq
			ITC message.
		 */
		class SetINT_GEN_THS_ZL_GPayload {
			public:
				/**	
				 */
				uint8_t	_value;

			public:
				/** The SetINT_GEN_THS_ZL_GPayload constructor. */
				SetINT_GEN_THS_ZL_GPayload(

					uint8_t	value
					) noexcept;

			};

		/**	Write the INT_GEN_THS_ZL_G register
		 */
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api,
					SetINT_GEN_THS_ZL_GPayload
					>		SetINT_GEN_THS_ZL_GReq;

	public:
		/** This class contains the data that is transfered
			between the client and server within the GetINT_GEN_THS_ZL_GReq
			ITC message.
		 */
		class GetINT_GEN_THS_ZL_GPayload {
			public:
				/**	
				 */
				uint8_t	_value;

			public:
				/** The GetINT_GEN_THS_ZL_GPayload constructor. */
				GetINT_GEN_THS_ZL_GPayload(
					) noexcept;

			};

		/**	Read the INT_GEN_THS_ZL_G register.
		 */
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api,
					GetINT_GEN_THS_ZL_GPayload
					>		GetINT_GEN_THS_ZL_GReq;

	public:
		/** This class contains the data that is transfered
			between the client and server within the SetINT_GEN_THS_ZH_GReq
			ITC message.
		 */
		class SetINT_GEN_THS_ZH_GPayload {
			public:
				/**	
				 */
				uint8_t	_value;

			public:
				/** The SetINT_GEN_THS_ZH_GPayload constructor. */
				SetINT_GEN_THS_ZH_GPayload(

					uint8_t	value
					) noexcept;

			};

		/**	Write the INT_GEN_THS_ZH_G register
		 */
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api,
					SetINT_GEN_THS_ZH_GPayload
					>		SetINT_GEN_THS_ZH_GReq;

	public:
		/** This class contains the data that is transfered
			between the client and server within the GetINT_GEN_THS_ZH_GReq
			ITC message.
		 */
		class GetINT_GEN_THS_ZH_GPayload {
			public:
				/**	
				 */
				uint8_t	_value;

			public:
				/** The GetINT_GEN_THS_ZH_GPayload constructor. */
				GetINT_GEN_THS_ZH_GPayload(
					) noexcept;

			};

		/**	Read the INT_GEN_THS_ZH_G register.
		 */
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api,
					GetINT_GEN_THS_ZH_GPayload
					>		GetINT_GEN_THS_ZH_GReq;

	public:
		/** This class contains the data that is transfered
			between the client and server within the SetINT_GEN_THS_YL_GReq
			ITC message.
		 */
		class SetINT_GEN_THS_YL_GPayload {
			public:
				/**	
				 */
				uint8_t	_value;

			public:
				/** The SetINT_GEN_THS_YL_GPayload constructor. */
				SetINT_GEN_THS_YL_GPayload(

					uint8_t	value
					) noexcept;

			};

		/**	Write the INT_GEN_THS_YL_G register
		 */
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api,
					SetINT_GEN_THS_YL_GPayload
					>		SetINT_GEN_THS_YL_GReq;

	public:
		/** This class contains the data that is transfered
			between the client and server within the GetINT_GEN_THS_YL_GReq
			ITC message.
		 */
		class GetINT_GEN_THS_YL_GPayload {
			public:
				/**	
				 */
				uint8_t	_value;

			public:
				/** The GetINT_GEN_THS_YL_GPayload constructor. */
				GetINT_GEN_THS_YL_GPayload(
					) noexcept;

			};

		/**	Read the INT_GEN_THS_YL_G register.
		 */
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api,
					GetINT_GEN_THS_YL_GPayload
					>		GetINT_GEN_THS_YL_GReq;

	public:
		/** This class contains the data that is transfered
			between the client and server within the SetINT_GEN_THS_YH_GReq
			ITC message.
		 */
		class SetINT_GEN_THS_YH_GPayload {
			public:
				/**	
				 */
				uint8_t	_value;

			public:
				/** The SetINT_GEN_THS_YH_GPayload constructor. */
				SetINT_GEN_THS_YH_GPayload(

					uint8_t	value
					) noexcept;

			};

		/**	Write the INT_GEN_THS_YH_G register
		 */
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api,
					SetINT_GEN_THS_YH_GPayload
					>		SetINT_GEN_THS_YH_GReq;

	public:
		/** This class contains the data that is transfered
			between the client and server within the GetINT_GEN_THS_YH_GReq
			ITC message.
		 */
		class GetINT_GEN_THS_YH_GPayload {
			public:
				/**	
				 */
				uint8_t	_value;

			public:
				/** The GetINT_GEN_THS_YH_GPayload constructor. */
				GetINT_GEN_THS_YH_GPayload(
					) noexcept;

			};

		/**	Read the INT_GEN_THS_YH_G register.
		 */
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api,
					GetINT_GEN_THS_YH_GPayload
					>		GetINT_GEN_THS_YH_GReq;

	public:
		/** This class contains the data that is transfered
			between the client and server within the SetINT_GEN_THS_XL_GReq
			ITC message.
		 */
		class SetINT_GEN_THS_XL_GPayload {
			public:
				/**	
				 */
				uint8_t	_value;

			public:
				/** The SetINT_GEN_THS_XL_GPayload constructor. */
				SetINT_GEN_THS_XL_GPayload(

					uint8_t	value
					) noexcept;

			};

		/**	Write the INT_GEN_THS_XL_G register
		 */
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api,
					SetINT_GEN_THS_XL_GPayload
					>		SetINT_GEN_THS_XL_GReq;

	public:
		/** This class contains the data that is transfered
			between the client and server within the GetINT_GEN_THS_XL_GReq
			ITC message.
		 */
		class GetINT_GEN_THS_XL_GPayload {
			public:
				/**	
				 */
				uint8_t	_value;

			public:
				/** The GetINT_GEN_THS_XL_GPayload constructor. */
				GetINT_GEN_THS_XL_GPayload(
					) noexcept;

			};

		/**	Read the INT_GEN_THS_XL_G register.
		 */
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api,
					GetINT_GEN_THS_XL_GPayload
					>		GetINT_GEN_THS_XL_GReq;

	public:
		/** This class contains the data that is transfered
			between the client and server within the SetINT_GEN_THS_XH_GReq
			ITC message.
		 */
		class SetINT_GEN_THS_XH_GPayload {
			public:
				/**	
				 */
				uint8_t	_value;

			public:
				/** The SetINT_GEN_THS_XH_GPayload constructor. */
				SetINT_GEN_THS_XH_GPayload(

					uint8_t	value
					) noexcept;

			};

		/**	Write the INT_GEN_THS_XH_G register
		 */
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api,
					SetINT_GEN_THS_XH_GPayload
					>		SetINT_GEN_THS_XH_GReq;

	public:
		/** This class contains the data that is transfered
			between the client and server within the GetINT_GEN_THS_XH_GReq
			ITC message.
		 */
		class GetINT_GEN_THS_XH_GPayload {
			public:
				/**	
				 */
				uint8_t	_value;

			public:
				/** The GetINT_GEN_THS_XH_GPayload constructor. */
				GetINT_GEN_THS_XH_GPayload(
					) noexcept;

			};

		/**	Read the INT_GEN_THS_XH_G register.
		 */
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api,
					GetINT_GEN_THS_XH_GPayload
					>		GetINT_GEN_THS_XH_GReq;

	public:
		/** This class contains the data that is transfered
			between the client and server within the SetINT_GEN_CFG_GReq
			ITC message.
		 */
		class SetINT_GEN_CFG_GPayload {
			public:
				/**	
				 */
				uint8_t	_value;

			public:
				/** The SetINT_GEN_CFG_GPayload constructor. */
				SetINT_GEN_CFG_GPayload(

					uint8_t	value
					) noexcept;

			};

		/**	Write the INT_GEN_CFG_G register
		 */
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api,
					SetINT_GEN_CFG_GPayload
					>		SetINT_GEN_CFG_GReq;

	public:
		/** This class contains the data that is transfered
			between the client and server within the GetINT_GEN_CFG_GReq
			ITC message.
		 */
		class GetINT_GEN_CFG_GPayload {
			public:
				/**	
				 */
				uint8_t	_value;

			public:
				/** The GetINT_GEN_CFG_GPayload constructor. */
				GetINT_GEN_CFG_GPayload(
					) noexcept;

			};

		/**	Read the INT_GEN_CFG_G register.
		 */
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api,
					GetINT_GEN_CFG_GPayload
					>		GetINT_GEN_CFG_GReq;

	public:
		/** This class contains the data that is transfered
			between the client and server within the GetFIFO_SRCReq
			ITC message.
		 */
		class GetFIFO_SRCPayload {
			public:
				/**	
				 */
				uint8_t	_value;

			public:
				/** The GetFIFO_SRCPayload constructor. */
				GetFIFO_SRCPayload(
					) noexcept;

			};

		/**	Read the FIFO_SRC register.
		 */
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api,
					GetFIFO_SRCPayload
					>		GetFIFO_SRCReq;

	public:
		/** This class contains the data that is transfered
			between the client and server within the SetFIFO_CTRLReq
			ITC message.
		 */
		class SetFIFO_CTRLPayload {
			public:
				/**	
				 */
				uint8_t	_value;

			public:
				/** The SetFIFO_CTRLPayload constructor. */
				SetFIFO_CTRLPayload(

					uint8_t	value
					) noexcept;

			};

		/**	Write the FIFO_CTRL register
		 */
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api,
					SetFIFO_CTRLPayload
					>		SetFIFO_CTRLReq;

	public:
		/** This class contains the data that is transfered
			between the client and server within the GetFIFO_CTRLReq
			ITC message.
		 */
		class GetFIFO_CTRLPayload {
			public:
				/**	
				 */
				uint8_t	_value;

			public:
				/** The GetFIFO_CTRLPayload constructor. */
				GetFIFO_CTRLPayload(
					) noexcept;

			};

		/**	Read the FIFO_CTRL register.
		 */
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api,
					GetFIFO_CTRLPayload
					>		GetFIFO_CTRLReq;

	public:
		/** This class contains the data that is transfered
			between the client and server within the GetOUT_Z_H_XLReq
			ITC message.
		 */
		class GetOUT_Z_H_XLPayload {
			public:
				/**	
				 */
				uint8_t	_value;

			public:
				/** The GetOUT_Z_H_XLPayload constructor. */
				GetOUT_Z_H_XLPayload(
					) noexcept;

			};

		/**	Read the OUT_Z_H_XL register.
		 */
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api,
					GetOUT_Z_H_XLPayload
					>		GetOUT_Z_H_XLReq;

	public:
		/** This class contains the data that is transfered
			between the client and server within the GetOUT_Z_L_XLReq
			ITC message.
		 */
		class GetOUT_Z_L_XLPayload {
			public:
				/**	
				 */
				uint8_t	_value;

			public:
				/** The GetOUT_Z_L_XLPayload constructor. */
				GetOUT_Z_L_XLPayload(
					) noexcept;

			};

		/**	Read the OUT_Z_L_XL register.
		 */
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api,
					GetOUT_Z_L_XLPayload
					>		GetOUT_Z_L_XLReq;

	public:
		/** This class contains the data that is transfered
			between the client and server within the GetOUT_Y_H_XLReq
			ITC message.
		 */
		class GetOUT_Y_H_XLPayload {
			public:
				/**	
				 */
				uint8_t	_value;

			public:
				/** The GetOUT_Y_H_XLPayload constructor. */
				GetOUT_Y_H_XLPayload(
					) noexcept;

			};

		/**	Read the OUT_Y_H_XL register.
		 */
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api,
					GetOUT_Y_H_XLPayload
					>		GetOUT_Y_H_XLReq;

	public:
		/** This class contains the data that is transfered
			between the client and server within the GetOUT_Y_L_XLReq
			ITC message.
		 */
		class GetOUT_Y_L_XLPayload {
			public:
				/**	
				 */
				uint8_t	_value;

			public:
				/** The GetOUT_Y_L_XLPayload constructor. */
				GetOUT_Y_L_XLPayload(
					) noexcept;

			};

		/**	Read the OUT_Y_L_XL register.
		 */
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api,
					GetOUT_Y_L_XLPayload
					>		GetOUT_Y_L_XLReq;

	public:
		/** This class contains the data that is transfered
			between the client and server within the GetOUT_X_H_XLReq
			ITC message.
		 */
		class GetOUT_X_H_XLPayload {
			public:
				/**	
				 */
				uint8_t	_value;

			public:
				/** The GetOUT_X_H_XLPayload constructor. */
				GetOUT_X_H_XLPayload(
					) noexcept;

			};

		/**	Read the OUT_X_H_XL register.
		 */
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api,
					GetOUT_X_H_XLPayload
					>		GetOUT_X_H_XLReq;

	public:
		/** This class contains the data that is transfered
			between the client and server within the GetOUT_X_L_XLReq
			ITC message.
		 */
		class GetOUT_X_L_XLPayload {
			public:
				/**	
				 */
				uint8_t	_value;

			public:
				/** The GetOUT_X_L_XLPayload constructor. */
				GetOUT_X_L_XLPayload(
					) noexcept;

			};

		/**	Read the OUT_X_L_XL register.
		 */
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api,
					GetOUT_X_L_XLPayload
					>		GetOUT_X_L_XLReq;

	public:
		/** This class contains the data that is transfered
			between the client and server within the GetSTATUS_REG_2Req
			ITC message.
		 */
		class GetSTATUS_REG_2Payload {
			public:
				/**	
				 */
				uint8_t	_value;

			public:
				/** The GetSTATUS_REG_2Payload constructor. */
				GetSTATUS_REG_2Payload(
					) noexcept;

			};

		/**	Read the STATUS_REG_2 register.
		 */
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api,
					GetSTATUS_REG_2Payload
					>		GetSTATUS_REG_2Req;

	public:
		/** This class contains the data that is transfered
			between the client and server within the GetINT_GEN_SRC_XLReq
			ITC message.
		 */
		class GetINT_GEN_SRC_XLPayload {
			public:
				/**	
				 */
				uint8_t	_value;

			public:
				/** The GetINT_GEN_SRC_XLPayload constructor. */
				GetINT_GEN_SRC_XLPayload(
					) noexcept;

			};

		/**	Read the INT_GEN_SRC_XL register.
		 */
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api,
					GetINT_GEN_SRC_XLPayload
					>		GetINT_GEN_SRC_XLReq;

	public:
		/** This class contains the data that is transfered
			between the client and server within the SetCTRL_REG10Req
			ITC message.
		 */
		class SetCTRL_REG10Payload {
			public:
				/**	
				 */
				uint8_t	_value;

			public:
				/** The SetCTRL_REG10Payload constructor. */
				SetCTRL_REG10Payload(

					uint8_t	value
					) noexcept;

			};

		/**	Write the CTRL_REG10 register
		 */
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api,
					SetCTRL_REG10Payload
					>		SetCTRL_REG10Req;

	public:
		/** This class contains the data that is transfered
			between the client and server within the GetCTRL_REG10Req
			ITC message.
		 */
		class GetCTRL_REG10Payload {
			public:
				/**	
				 */
				uint8_t	_value;

			public:
				/** The GetCTRL_REG10Payload constructor. */
				GetCTRL_REG10Payload(
					) noexcept;

			};

		/**	Read the CTRL_REG10 register.
		 */
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api,
					GetCTRL_REG10Payload
					>		GetCTRL_REG10Req;

	public:
		/** This class contains the data that is transfered
			between the client and server within the SetCTRL_REG9Req
			ITC message.
		 */
		class SetCTRL_REG9Payload {
			public:
				/**	
				 */
				uint8_t	_value;

			public:
				/** The SetCTRL_REG9Payload constructor. */
				SetCTRL_REG9Payload(

					uint8_t	value
					) noexcept;

			};

		/**	Write the CTRL_REG9 register
		 */
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api,
					SetCTRL_REG9Payload
					>		SetCTRL_REG9Req;

	public:
		/** This class contains the data that is transfered
			between the client and server within the GetCTRL_REG9Req
			ITC message.
		 */
		class GetCTRL_REG9Payload {
			public:
				/**	
				 */
				uint8_t	_value;

			public:
				/** The GetCTRL_REG9Payload constructor. */
				GetCTRL_REG9Payload(
					) noexcept;

			};

		/**	Read the CTRL_REG9 register.
		 */
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api,
					GetCTRL_REG9Payload
					>		GetCTRL_REG9Req;

	public:
		/** This class contains the data that is transfered
			between the client and server within the SetCTRL_REG8Req
			ITC message.
		 */
		class SetCTRL_REG8Payload {
			public:
				/**	
				 */
				uint8_t	_value;

			public:
				/** The SetCTRL_REG8Payload constructor. */
				SetCTRL_REG8Payload(

					uint8_t	value
					) noexcept;

			};

		/**	Write the CTRL_REG8 register
		 */
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api,
					SetCTRL_REG8Payload
					>		SetCTRL_REG8Req;

	public:
		/** This class contains the data that is transfered
			between the client and server within the GetCTRL_REG8Req
			ITC message.
		 */
		class GetCTRL_REG8Payload {
			public:
				/**	
				 */
				uint8_t	_value;

			public:
				/** The GetCTRL_REG8Payload constructor. */
				GetCTRL_REG8Payload(
					) noexcept;

			};

		/**	Read the CTRL_REG8 register.
		 */
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api,
					GetCTRL_REG8Payload
					>		GetCTRL_REG8Req;

	public:
		/** This class contains the data that is transfered
			between the client and server within the SetCTRL_REG7_XLReq
			ITC message.
		 */
		class SetCTRL_REG7_XLPayload {
			public:
				/**	
				 */
				uint8_t	_value;

			public:
				/** The SetCTRL_REG7_XLPayload constructor. */
				SetCTRL_REG7_XLPayload(

					uint8_t	value
					) noexcept;

			};

		/**	Write the CTRL_REG7_XL register
		 */
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api,
					SetCTRL_REG7_XLPayload
					>		SetCTRL_REG7_XLReq;

	public:
		/** This class contains the data that is transfered
			between the client and server within the GetCTRL_REG7_XLReq
			ITC message.
		 */
		class GetCTRL_REG7_XLPayload {
			public:
				/**	
				 */
				uint8_t	_value;

			public:
				/** The GetCTRL_REG7_XLPayload constructor. */
				GetCTRL_REG7_XLPayload(
					) noexcept;

			};

		/**	Read the CTRL_REG7_XL register.
		 */
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api,
					GetCTRL_REG7_XLPayload
					>		GetCTRL_REG7_XLReq;

	public:
		/** This class contains the data that is transfered
			between the client and server within the SetCTRL_REG6_XLReq
			ITC message.
		 */
		class SetCTRL_REG6_XLPayload {
			public:
				/**	
				 */
				uint8_t	_value;

			public:
				/** The SetCTRL_REG6_XLPayload constructor. */
				SetCTRL_REG6_XLPayload(

					uint8_t	value
					) noexcept;

			};

		/**	Write the CTRL_REG6_XL register
		 */
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api,
					SetCTRL_REG6_XLPayload
					>		SetCTRL_REG6_XLReq;

	public:
		/** This class contains the data that is transfered
			between the client and server within the GetCTRL_REG6_XLReq
			ITC message.
		 */
		class GetCTRL_REG6_XLPayload {
			public:
				/**	
				 */
				uint8_t	_value;

			public:
				/** The GetCTRL_REG6_XLPayload constructor. */
				GetCTRL_REG6_XLPayload(
					) noexcept;

			};

		/**	Read the CTRL_REG6_XL register.
		 */
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api,
					GetCTRL_REG6_XLPayload
					>		GetCTRL_REG6_XLReq;

	public:
		/** This class contains the data that is transfered
			between the client and server within the SetCTRL_REG5_XLReq
			ITC message.
		 */
		class SetCTRL_REG5_XLPayload {
			public:
				/**	
				 */
				uint8_t	_value;

			public:
				/** The SetCTRL_REG5_XLPayload constructor. */
				SetCTRL_REG5_XLPayload(

					uint8_t	value
					) noexcept;

			};

		/**	Write the CTRL_REG5_XL register
		 */
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api,
					SetCTRL_REG5_XLPayload
					>		SetCTRL_REG5_XLReq;

	public:
		/** This class contains the data that is transfered
			between the client and server within the GetCTRL_REG5_XLReq
			ITC message.
		 */
		class GetCTRL_REG5_XLPayload {
			public:
				/**	
				 */
				uint8_t	_value;

			public:
				/** The GetCTRL_REG5_XLPayload constructor. */
				GetCTRL_REG5_XLPayload(
					) noexcept;

			};

		/**	Read the CTRL_REG5_XL register.
		 */
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api,
					GetCTRL_REG5_XLPayload
					>		GetCTRL_REG5_XLReq;

	public:
		/** This class contains the data that is transfered
			between the client and server within the SetCTRL_REG4Req
			ITC message.
		 */
		class SetCTRL_REG4Payload {
			public:
				/**	
				 */
				uint8_t	_value;

			public:
				/** The SetCTRL_REG4Payload constructor. */
				SetCTRL_REG4Payload(

					uint8_t	value
					) noexcept;

			};

		/**	Write the CTRL_REG4 register
		 */
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api,
					SetCTRL_REG4Payload
					>		SetCTRL_REG4Req;

	public:
		/** This class contains the data that is transfered
			between the client and server within the GetCTRL_REG4Req
			ITC message.
		 */
		class GetCTRL_REG4Payload {
			public:
				/**	
				 */
				uint8_t	_value;

			public:
				/** The GetCTRL_REG4Payload constructor. */
				GetCTRL_REG4Payload(
					) noexcept;

			};

		/**	Read the CTRL_REG4 register.
		 */
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api,
					GetCTRL_REG4Payload
					>		GetCTRL_REG4Req;

	public:
		/** This class contains the data that is transfered
			between the client and server within the GetOUT_Z_H_GReq
			ITC message.
		 */
		class GetOUT_Z_H_GPayload {
			public:
				/**	
				 */
				uint8_t	_value;

			public:
				/** The GetOUT_Z_H_GPayload constructor. */
				GetOUT_Z_H_GPayload(
					) noexcept;

			};

		/**	Read the OUT_Z_H_G register.
		 */
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api,
					GetOUT_Z_H_GPayload
					>		GetOUT_Z_H_GReq;

	public:
		/** This class contains the data that is transfered
			between the client and server within the GetOUT_Z_L_GReq
			ITC message.
		 */
		class GetOUT_Z_L_GPayload {
			public:
				/**	
				 */
				uint8_t	_value;

			public:
				/** The GetOUT_Z_L_GPayload constructor. */
				GetOUT_Z_L_GPayload(
					) noexcept;

			};

		/**	Read the OUT_Z_L_G register.
		 */
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api,
					GetOUT_Z_L_GPayload
					>		GetOUT_Z_L_GReq;

	public:
		/** This class contains the data that is transfered
			between the client and server within the GetOUT_Y_H_GReq
			ITC message.
		 */
		class GetOUT_Y_H_GPayload {
			public:
				/**	
				 */
				uint8_t	_value;

			public:
				/** The GetOUT_Y_H_GPayload constructor. */
				GetOUT_Y_H_GPayload(
					) noexcept;

			};

		/**	Read the OUT_Y_H_G register.
		 */
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api,
					GetOUT_Y_H_GPayload
					>		GetOUT_Y_H_GReq;

	public:
		/** This class contains the data that is transfered
			between the client and server within the GetOUT_Y_L_GReq
			ITC message.
		 */
		class GetOUT_Y_L_GPayload {
			public:
				/**	
				 */
				uint8_t	_value;

			public:
				/** The GetOUT_Y_L_GPayload constructor. */
				GetOUT_Y_L_GPayload(
					) noexcept;

			};

		/**	Read the OUT_Y_L_G register
		 */
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api,
					GetOUT_Y_L_GPayload
					>		GetOUT_Y_L_GReq;

	public:
		/** This class contains the data that is transfered
			between the client and server within the GetOUT_X_L_GReq
			ITC message.
		 */
		class GetOUT_X_L_GPayload {
			public:
				/**	
				 */
				uint8_t	_value;

			public:
				/** The GetOUT_X_L_GPayload constructor. */
				GetOUT_X_L_GPayload(
					) noexcept;

			};

		/**	Read the OUT_X_L_G register
		 */
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api,
					GetOUT_X_L_GPayload
					>		GetOUT_X_L_GReq;

	public:
		/** This class contains the data that is transfered
			between the client and server within the GetSTATUS_REGReq
			ITC message.
		 */
		class GetSTATUS_REGPayload {
			public:
				/**	
				 */
				uint8_t	_value;

			public:
				/** The GetSTATUS_REGPayload constructor. */
				GetSTATUS_REGPayload(
					) noexcept;

			};

		/**	Read the STATUS_REG register
		 */
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api,
					GetSTATUS_REGPayload
					>		GetSTATUS_REGReq;

	public:
		/** This class contains the data that is transfered
			between the client and server within the GetOUT_TEMP_HReq
			ITC message.
		 */
		class GetOUT_TEMP_HPayload {
			public:
				/**	
				 */
				uint8_t	_value;

			public:
				/** The GetOUT_TEMP_HPayload constructor. */
				GetOUT_TEMP_HPayload(
					) noexcept;

			};

		/**	Read the OUT_TEMP_H register.
		 */
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api,
					GetOUT_TEMP_HPayload
					>		GetOUT_TEMP_HReq;

	public:
		/** This class contains the data that is transfered
			between the client and server within the GetOUT_TEMP_LReq
			ITC message.
		 */
		class GetOUT_TEMP_LPayload {
			public:
				/**	
				 */
				uint8_t	_value;

			public:
				/** The GetOUT_TEMP_LPayload constructor. */
				GetOUT_TEMP_LPayload(
					) noexcept;

			};

		/**	Read the OUT_TEMP_L register.
		 */
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api,
					GetOUT_TEMP_LPayload
					>		GetOUT_TEMP_LReq;

	public:
		/** This class contains the data that is transfered
			between the client and server within the GetINT_GEN_SRC_GReq
			ITC message.
		 */
		class GetINT_GEN_SRC_GPayload {
			public:
				/**	
				 */
				uint8_t	_value;

			public:
				/** The GetINT_GEN_SRC_GPayload constructor. */
				GetINT_GEN_SRC_GPayload(
					) noexcept;

			};

		/**	Read the INT_GEN_SRC_G register.
		 */
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api,
					GetINT_GEN_SRC_GPayload
					>		GetINT_GEN_SRC_GReq;

	public:
		/** This class contains the data that is transfered
			between the client and server within the SetORIENT_CFG_GReq
			ITC message.
		 */
		class SetORIENT_CFG_GPayload {
			public:
				/**	
				 */
				uint8_t	_value;

			public:
				/** The SetORIENT_CFG_GPayload constructor. */
				SetORIENT_CFG_GPayload(

					uint8_t	value
					) noexcept;

			};

		/**	Write the ORIENT_CFG_G register
		 */
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api,
					SetORIENT_CFG_GPayload
					>		SetORIENT_CFG_GReq;

	public:
		/** This class contains the data that is transfered
			between the client and server within the GetORIENT_CFG_GReq
			ITC message.
		 */
		class GetORIENT_CFG_GPayload {
			public:
				/**	
				 */
				uint8_t	_value;

			public:
				/** The GetORIENT_CFG_GPayload constructor. */
				GetORIENT_CFG_GPayload(
					) noexcept;

			};

		/**	Read the ORIENT_CFG_G register.
		 */
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api,
					GetORIENT_CFG_GPayload
					>		GetORIENT_CFG_GReq;

	public:
		/** This class contains the data that is transfered
			between the client and server within the SetCTRL_REG3_GReq
			ITC message.
		 */
		class SetCTRL_REG3_GPayload {
			public:
				/**	
				 */
				uint8_t	_value;

			public:
				/** The SetCTRL_REG3_GPayload constructor. */
				SetCTRL_REG3_GPayload(

					uint8_t	value
					) noexcept;

			};

		/**	Write the CTRL_REG3_G register
		 */
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api,
					SetCTRL_REG3_GPayload
					>		SetCTRL_REG3_GReq;

	public:
		/** This class contains the data that is transfered
			between the client and server within the GetCTRL_REG3_GReq
			ITC message.
		 */
		class GetCTRL_REG3_GPayload {
			public:
				/**	
				 */
				uint8_t	_value;

			public:
				/** The GetCTRL_REG3_GPayload constructor. */
				GetCTRL_REG3_GPayload(
					) noexcept;

			};

		/**	Read the CTRL_REG3_G register.
		 */
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api,
					GetCTRL_REG3_GPayload
					>		GetCTRL_REG3_GReq;

	public:
		/** This class contains the data that is transfered
			between the client and server within the SetCTRL_REG2_GReq
			ITC message.
		 */
		class SetCTRL_REG2_GPayload {
			public:
				/**	
				 */
				uint8_t	_value;

			public:
				/** The SetCTRL_REG2_GPayload constructor. */
				SetCTRL_REG2_GPayload(

					uint8_t	value
					) noexcept;

			};

		/**	Write the CTRL_REG2_G register
		 */
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api,
					SetCTRL_REG2_GPayload
					>		SetCTRL_REG2_GReq;

	public:
		/** This class contains the data that is transfered
			between the client and server within the GetCTRL_REG2_GReq
			ITC message.
		 */
		class GetCTRL_REG2_GPayload {
			public:
				/**	
				 */
				uint8_t	_value;

			public:
				/** The GetCTRL_REG2_GPayload constructor. */
				GetCTRL_REG2_GPayload(
					) noexcept;

			};

		/**	Read the CTRL_REG2_G register.
		 */
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api,
					GetCTRL_REG2_GPayload
					>		GetCTRL_REG2_GReq;

	public:
		/** This class contains the data that is transfered
			between the client and server within the SetCTRL_REG1_GReq
			ITC message.
		 */
		class SetCTRL_REG1_GPayload {
			public:
				/**	
				 */
				uint8_t	_value;

			public:
				/** The SetCTRL_REG1_GPayload constructor. */
				SetCTRL_REG1_GPayload(

					uint8_t	value
					) noexcept;

			};

		/**	Write the CTRL_REG1_G register
		 */
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api,
					SetCTRL_REG1_GPayload
					>		SetCTRL_REG1_GReq;

	public:
		/** This class contains the data that is transfered
			between the client and server within the GetCTRL_REG1_GReq
			ITC message.
		 */
		class GetCTRL_REG1_GPayload {
			public:
				/**	
				 */
				uint8_t	_value;

			public:
				/** The GetCTRL_REG1_GPayload constructor. */
				GetCTRL_REG1_GPayload(
					) noexcept;

			};

		/**	Read the CTRL_REG1_G register.
		 */
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api,
					GetCTRL_REG1_GPayload
					>		GetCTRL_REG1_GReq;

	public:
		/** This class contains the data that is transfered
			between the client and server within the GetWHO_AM_IReq
			ITC message.
		 */
		class GetWHO_AM_IPayload {
			public:
				/**	
				 */
				uint8_t	_value;

			public:
				/** The GetWHO_AM_IPayload constructor. */
				GetWHO_AM_IPayload(
					) noexcept;

			};

		/**	Read the WHO_AM_I register.
		 */
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api,
					GetWHO_AM_IPayload
					>		GetWHO_AM_IReq;

	public:
		/** This class contains the data that is transfered
			between the client and server within the SetINT2_CTRLReq
			ITC message.
		 */
		class SetINT2_CTRLPayload {
			public:
				/**	
				 */
				uint8_t	_value;

			public:
				/** The SetINT2_CTRLPayload constructor. */
				SetINT2_CTRLPayload(

					uint8_t	value
					) noexcept;

			};

		/**	Write the INT2_CTRL register
		 */
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api,
					SetINT2_CTRLPayload
					>		SetINT2_CTRLReq;

	public:
		/** This class contains the data that is transfered
			between the client and server within the GetINT2_CTRLReq
			ITC message.
		 */
		class GetINT2_CTRLPayload {
			public:
				/**	
				 */
				uint8_t	_value;

			public:
				/** The GetINT2_CTRLPayload constructor. */
				GetINT2_CTRLPayload(
					) noexcept;

			};

		/**	Read the INT2_CTRL register.
		 */
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api,
					GetINT2_CTRLPayload
					>		GetINT2_CTRLReq;

	public:
		/** This class contains the data that is transfered
			between the client and server within the SetINT1_CTRLReq
			ITC message.
		 */
		class SetINT1_CTRLPayload {
			public:
				/**	
				 */
				uint8_t	_value;

			public:
				/** The SetINT1_CTRLPayload constructor. */
				SetINT1_CTRLPayload(

					uint8_t	value
					) noexcept;

			};

		/**	Write the INT1_CTRL register
		 */
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api,
					SetINT1_CTRLPayload
					>		SetINT1_CTRLReq;

	public:
		/** This class contains the data that is transfered
			between the client and server within the GetINT1_CTRLReq
			ITC message.
		 */
		class GetINT1_CTRLPayload {
			public:
				/**	
				 */
				uint8_t	_value;

			public:
				/** The GetINT1_CTRLPayload constructor. */
				GetINT1_CTRLPayload(
					) noexcept;

			};

		/**	Read the INT1_CTRL register.
		 */
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api,
					GetINT1_CTRLPayload
					>		GetINT1_CTRLReq;

	public:
		/** This class contains the data that is transfered
			between the client and server within the SetREFERENCE_GReq
			ITC message.
		 */
		class SetREFERENCE_GPayload {
			public:
				/**	
				 */
				uint8_t	_value;

			public:
				/** The SetREFERENCE_GPayload constructor. */
				SetREFERENCE_GPayload(

					uint8_t	value
					) noexcept;

			};

		/**	Write the REFERENCE_G register
		 */
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api,
					SetREFERENCE_GPayload
					>		SetREFERENCE_GReq;

	public:
		/** This class contains the data that is transfered
			between the client and server within the GetREFERENCE_GReq
			ITC message.
		 */
		class GetREFERENCE_GPayload {
			public:
				/**	
				 */
				uint8_t	_value;

			public:
				/** The GetREFERENCE_GPayload constructor. */
				GetREFERENCE_GPayload(
					) noexcept;

			};

		/**	Read the REFERENCE_G register.
		 */
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api,
					GetREFERENCE_GPayload
					>		GetREFERENCE_GReq;

	public:
		/** This class contains the data that is transfered
			between the client and server within the SetINT_GEN_DUR_XLReq
			ITC message.
		 */
		class SetINT_GEN_DUR_XLPayload {
			public:
				/**	
				 */
				uint8_t	_value;

			public:
				/** The SetINT_GEN_DUR_XLPayload constructor. */
				SetINT_GEN_DUR_XLPayload(

					uint8_t	value
					) noexcept;

			};

		/**	Write the INT_GEN_DUR_XL register
		 */
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api,
					SetINT_GEN_DUR_XLPayload
					>		SetINT_GEN_DUR_XLReq;

	public:
		/** This class contains the data that is transfered
			between the client and server within the GetINT_GEN_DUR_XLReq
			ITC message.
		 */
		class GetINT_GEN_DUR_XLPayload {
			public:
				/**	
				 */
				uint8_t	_value;

			public:
				/** The GetINT_GEN_DUR_XLPayload constructor. */
				GetINT_GEN_DUR_XLPayload(
					) noexcept;

			};

		/**	Read the INT_GEN_DUR_XL register.
		 */
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api,
					GetINT_GEN_DUR_XLPayload
					>		GetINT_GEN_DUR_XLReq;

	public:
		/** This class contains the data that is transfered
			between the client and server within the SetINT_GEN_THS_Z_XLReq
			ITC message.
		 */
		class SetINT_GEN_THS_Z_XLPayload {
			public:
				/**	
				 */
				uint8_t	_value;

			public:
				/** The SetINT_GEN_THS_Z_XLPayload constructor. */
				SetINT_GEN_THS_Z_XLPayload(

					uint8_t	value
					) noexcept;

			};

		/**	Write the INT_GEN_THS_Z_XL register
		 */
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api,
					SetINT_GEN_THS_Z_XLPayload
					>		SetINT_GEN_THS_Z_XLReq;

	public:
		/** This class contains the data that is transfered
			between the client and server within the GetINT_GEN_THS_Z_XLReq
			ITC message.
		 */
		class GetINT_GEN_THS_Z_XLPayload {
			public:
				/**	
				 */
				uint8_t	_value;

			public:
				/** The GetINT_GEN_THS_Z_XLPayload constructor. */
				GetINT_GEN_THS_Z_XLPayload(
					) noexcept;

			};

		/**	Read the INT_GEN_THS_Z_XL register.
		 */
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api,
					GetINT_GEN_THS_Z_XLPayload
					>		GetINT_GEN_THS_Z_XLReq;

	public:
		/** This class contains the data that is transfered
			between the client and server within the SetINT_GEN_THS_Y_XLReq
			ITC message.
		 */
		class SetINT_GEN_THS_Y_XLPayload {
			public:
				/**	
				 */
				uint8_t	_value;

			public:
				/** The SetINT_GEN_THS_Y_XLPayload constructor. */
				SetINT_GEN_THS_Y_XLPayload(

					uint8_t	value
					) noexcept;

			};

		/**	Write the INT_GEN_THS_Y_XL register
		 */
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api,
					SetINT_GEN_THS_Y_XLPayload
					>		SetINT_GEN_THS_Y_XLReq;

	public:
		/** This class contains the data that is transfered
			between the client and server within the GetINT_GEN_THS_Y_XLReq
			ITC message.
		 */
		class GetINT_GEN_THS_Y_XLPayload {
			public:
				/**	
				 */
				uint8_t	_value;

			public:
				/** The GetINT_GEN_THS_Y_XLPayload constructor. */
				GetINT_GEN_THS_Y_XLPayload(
					) noexcept;

			};

		/**	Read the INT_GEN_THS_Y_XL register.
		 */
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api,
					GetINT_GEN_THS_Y_XLPayload
					>		GetINT_GEN_THS_Y_XLReq;

	public:
		/** This class contains the data that is transfered
			between the client and server within the SetINT_GEN_THS_X_XLReq
			ITC message.
		 */
		class SetINT_GEN_THS_X_XLPayload {
			public:
				/**	
				 */
				uint8_t	_value;

			public:
				/** The SetINT_GEN_THS_X_XLPayload constructor. */
				SetINT_GEN_THS_X_XLPayload(

					uint8_t	value
					) noexcept;

			};

		/**	Write the INT_GEN_THS_X_XL register
		 */
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api,
					SetINT_GEN_THS_X_XLPayload
					>		SetINT_GEN_THS_X_XLReq;

	public:
		/** This class contains the data that is transfered
			between the client and server within the GetINT_GEN_THS_X_XLReq
			ITC message.
		 */
		class GetINT_GEN_THS_X_XLPayload {
			public:
				/**	
				 */
				uint8_t	_value;

			public:
				/** The GetINT_GEN_THS_X_XLPayload constructor. */
				GetINT_GEN_THS_X_XLPayload(
					) noexcept;

			};

		/**	Read the INT_GEN_THS_X_XL register.
		 */
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api,
					GetINT_GEN_THS_X_XLPayload
					>		GetINT_GEN_THS_X_XLReq;

	public:
		/** This class contains the data that is transfered
			between the client and server within the SetINT_GEN_CFG_XLReq
			ITC message.
		 */
		class SetINT_GEN_CFG_XLPayload {
			public:
				/**	
				 */
				uint8_t	_value;

			public:
				/** The SetINT_GEN_CFG_XLPayload constructor. */
				SetINT_GEN_CFG_XLPayload(

					uint8_t	value
					) noexcept;

			};

		/**	Write the INT_GEN_CFG_XL register
		 */
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api,
					SetINT_GEN_CFG_XLPayload
					>		SetINT_GEN_CFG_XLReq;

	public:
		/** This class contains the data that is transfered
			between the client and server within the GetINT_GEN_CFG_XLReq
			ITC message.
		 */
		class GetINT_GEN_CFG_XLPayload {
			public:
				/**	
				 */
				uint8_t	_value;

			public:
				/** The GetINT_GEN_CFG_XLPayload constructor. */
				GetINT_GEN_CFG_XLPayload(
					) noexcept;

			};

		/**	Read the INT_GEN_CFG_XL register.
		 */
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api,
					GetINT_GEN_CFG_XLPayload
					>		GetINT_GEN_CFG_XLReq;

	public:
		/** This class contains the data that is transfered
			between the client and server within the SetACT_DURReq
			ITC message.
		 */
		class SetACT_DURPayload {
			public:
				/**	
				 */
				uint8_t	_value;

			public:
				/** The SetACT_DURPayload constructor. */
				SetACT_DURPayload(

					uint8_t	value
					) noexcept;

			};

		/**	Write the ACT_DUR register
		 */
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api,
					SetACT_DURPayload
					>		SetACT_DURReq;

	public:
		/** This class contains the data that is transfered
			between the client and server within the GetACT_DURReq
			ITC message.
		 */
		class GetACT_DURPayload {
			public:
				/**	
				 */
				uint8_t	_value;

			public:
				/** The GetACT_DURPayload constructor. */
				GetACT_DURPayload(
					) noexcept;

			};

		/**	Read the ACT_DUR register.
		 */
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api,
					GetACT_DURPayload
					>		GetACT_DURReq;

	public:
		/** This class contains the data that is transfered
			between the client and server within the SetACT_THSReq
			ITC message.
		 */
		class SetACT_THSPayload {
			public:
				/**	
				 */
				uint8_t	_value;

			public:
				/** The SetACT_THSPayload constructor. */
				SetACT_THSPayload(

					uint8_t	value
					) noexcept;

			};

		/**	Write the ACT_THS register
		 */
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api,
					SetACT_THSPayload
					>		SetACT_THSReq;

	public:
		/** This class contains the data that is transfered
			between the client and server within the GetACT_THSReq
			ITC message.
		 */
		class GetACT_THSPayload {
			public:
				/**	
				 */
				uint8_t	_value;

			public:
				/** The GetACT_THSPayload constructor. */
				GetACT_THSPayload(
					) noexcept;

			};

		/**	Read the ACT_THS register.
		 */
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api,
					GetACT_THSPayload
					>		GetACT_THSReq;

	public:
		/** This type defines a SAP (Service Access Point) for this
			ITC server interface. A SAP holds server address information
			that is required by clients to send messages to a server.
		 */
		typedef Oscl::Mt::Itc::SAP<	Oscl::Driver::ST::LSM9DS1::AG::Register::
							Req::Api
							>			SAP;

	public:
		/** This type defines a SAP (Service Access Point) for this
			ITC server interface. A SAP holds server address information
			that is required by clients to send messages to a server.
		 */
		typedef Oscl::Mt::Itc::ConcreteSAP<	Oscl::Driver::ST::LSM9DS1::AG::Register::
							Req::Api
							>			ConcreteSAP;

	public:
		/** Make the compiler happy. */
		virtual ~Api(){}

		/** This operation is invoked within the server thread
			whenever a GetOUT_TEMPReq is received.
		 */
		virtual void	request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Req::Api::GetOUT_TEMPReq& msg
									) noexcept=0;

		/** This operation is invoked within the server thread
			whenever a GetOUT_GReq is received.
		 */
		virtual void	request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Req::Api::GetOUT_GReq& msg
									) noexcept=0;

		/** This operation is invoked within the server thread
			whenever a GetOUT_XLReq is received.
		 */
		virtual void	request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Req::Api::GetOUT_XLReq& msg
									) noexcept=0;

		/** This operation is invoked within the server thread
			whenever a SetINT_GEN_DUR_GReq is received.
		 */
		virtual void	request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Req::Api::SetINT_GEN_DUR_GReq& msg
									) noexcept=0;

		/** This operation is invoked within the server thread
			whenever a GetINT_GEN_DUR_GReq is received.
		 */
		virtual void	request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Req::Api::GetINT_GEN_DUR_GReq& msg
									) noexcept=0;

		/** This operation is invoked within the server thread
			whenever a SetINT_GEN_THS_ZL_GReq is received.
		 */
		virtual void	request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Req::Api::SetINT_GEN_THS_ZL_GReq& msg
									) noexcept=0;

		/** This operation is invoked within the server thread
			whenever a GetINT_GEN_THS_ZL_GReq is received.
		 */
		virtual void	request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Req::Api::GetINT_GEN_THS_ZL_GReq& msg
									) noexcept=0;

		/** This operation is invoked within the server thread
			whenever a SetINT_GEN_THS_ZH_GReq is received.
		 */
		virtual void	request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Req::Api::SetINT_GEN_THS_ZH_GReq& msg
									) noexcept=0;

		/** This operation is invoked within the server thread
			whenever a GetINT_GEN_THS_ZH_GReq is received.
		 */
		virtual void	request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Req::Api::GetINT_GEN_THS_ZH_GReq& msg
									) noexcept=0;

		/** This operation is invoked within the server thread
			whenever a SetINT_GEN_THS_YL_GReq is received.
		 */
		virtual void	request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Req::Api::SetINT_GEN_THS_YL_GReq& msg
									) noexcept=0;

		/** This operation is invoked within the server thread
			whenever a GetINT_GEN_THS_YL_GReq is received.
		 */
		virtual void	request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Req::Api::GetINT_GEN_THS_YL_GReq& msg
									) noexcept=0;

		/** This operation is invoked within the server thread
			whenever a SetINT_GEN_THS_YH_GReq is received.
		 */
		virtual void	request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Req::Api::SetINT_GEN_THS_YH_GReq& msg
									) noexcept=0;

		/** This operation is invoked within the server thread
			whenever a GetINT_GEN_THS_YH_GReq is received.
		 */
		virtual void	request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Req::Api::GetINT_GEN_THS_YH_GReq& msg
									) noexcept=0;

		/** This operation is invoked within the server thread
			whenever a SetINT_GEN_THS_XL_GReq is received.
		 */
		virtual void	request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Req::Api::SetINT_GEN_THS_XL_GReq& msg
									) noexcept=0;

		/** This operation is invoked within the server thread
			whenever a GetINT_GEN_THS_XL_GReq is received.
		 */
		virtual void	request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Req::Api::GetINT_GEN_THS_XL_GReq& msg
									) noexcept=0;

		/** This operation is invoked within the server thread
			whenever a SetINT_GEN_THS_XH_GReq is received.
		 */
		virtual void	request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Req::Api::SetINT_GEN_THS_XH_GReq& msg
									) noexcept=0;

		/** This operation is invoked within the server thread
			whenever a GetINT_GEN_THS_XH_GReq is received.
		 */
		virtual void	request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Req::Api::GetINT_GEN_THS_XH_GReq& msg
									) noexcept=0;

		/** This operation is invoked within the server thread
			whenever a SetINT_GEN_CFG_GReq is received.
		 */
		virtual void	request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Req::Api::SetINT_GEN_CFG_GReq& msg
									) noexcept=0;

		/** This operation is invoked within the server thread
			whenever a GetINT_GEN_CFG_GReq is received.
		 */
		virtual void	request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Req::Api::GetINT_GEN_CFG_GReq& msg
									) noexcept=0;

		/** This operation is invoked within the server thread
			whenever a GetFIFO_SRCReq is received.
		 */
		virtual void	request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Req::Api::GetFIFO_SRCReq& msg
									) noexcept=0;

		/** This operation is invoked within the server thread
			whenever a SetFIFO_CTRLReq is received.
		 */
		virtual void	request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Req::Api::SetFIFO_CTRLReq& msg
									) noexcept=0;

		/** This operation is invoked within the server thread
			whenever a GetFIFO_CTRLReq is received.
		 */
		virtual void	request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Req::Api::GetFIFO_CTRLReq& msg
									) noexcept=0;

		/** This operation is invoked within the server thread
			whenever a GetOUT_Z_H_XLReq is received.
		 */
		virtual void	request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Req::Api::GetOUT_Z_H_XLReq& msg
									) noexcept=0;

		/** This operation is invoked within the server thread
			whenever a GetOUT_Z_L_XLReq is received.
		 */
		virtual void	request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Req::Api::GetOUT_Z_L_XLReq& msg
									) noexcept=0;

		/** This operation is invoked within the server thread
			whenever a GetOUT_Y_H_XLReq is received.
		 */
		virtual void	request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Req::Api::GetOUT_Y_H_XLReq& msg
									) noexcept=0;

		/** This operation is invoked within the server thread
			whenever a GetOUT_Y_L_XLReq is received.
		 */
		virtual void	request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Req::Api::GetOUT_Y_L_XLReq& msg
									) noexcept=0;

		/** This operation is invoked within the server thread
			whenever a GetOUT_X_H_XLReq is received.
		 */
		virtual void	request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Req::Api::GetOUT_X_H_XLReq& msg
									) noexcept=0;

		/** This operation is invoked within the server thread
			whenever a GetOUT_X_L_XLReq is received.
		 */
		virtual void	request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Req::Api::GetOUT_X_L_XLReq& msg
									) noexcept=0;

		/** This operation is invoked within the server thread
			whenever a GetSTATUS_REG_2Req is received.
		 */
		virtual void	request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Req::Api::GetSTATUS_REG_2Req& msg
									) noexcept=0;

		/** This operation is invoked within the server thread
			whenever a GetINT_GEN_SRC_XLReq is received.
		 */
		virtual void	request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Req::Api::GetINT_GEN_SRC_XLReq& msg
									) noexcept=0;

		/** This operation is invoked within the server thread
			whenever a SetCTRL_REG10Req is received.
		 */
		virtual void	request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Req::Api::SetCTRL_REG10Req& msg
									) noexcept=0;

		/** This operation is invoked within the server thread
			whenever a GetCTRL_REG10Req is received.
		 */
		virtual void	request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Req::Api::GetCTRL_REG10Req& msg
									) noexcept=0;

		/** This operation is invoked within the server thread
			whenever a SetCTRL_REG9Req is received.
		 */
		virtual void	request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Req::Api::SetCTRL_REG9Req& msg
									) noexcept=0;

		/** This operation is invoked within the server thread
			whenever a GetCTRL_REG9Req is received.
		 */
		virtual void	request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Req::Api::GetCTRL_REG9Req& msg
									) noexcept=0;

		/** This operation is invoked within the server thread
			whenever a SetCTRL_REG8Req is received.
		 */
		virtual void	request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Req::Api::SetCTRL_REG8Req& msg
									) noexcept=0;

		/** This operation is invoked within the server thread
			whenever a GetCTRL_REG8Req is received.
		 */
		virtual void	request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Req::Api::GetCTRL_REG8Req& msg
									) noexcept=0;

		/** This operation is invoked within the server thread
			whenever a SetCTRL_REG7_XLReq is received.
		 */
		virtual void	request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Req::Api::SetCTRL_REG7_XLReq& msg
									) noexcept=0;

		/** This operation is invoked within the server thread
			whenever a GetCTRL_REG7_XLReq is received.
		 */
		virtual void	request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Req::Api::GetCTRL_REG7_XLReq& msg
									) noexcept=0;

		/** This operation is invoked within the server thread
			whenever a SetCTRL_REG6_XLReq is received.
		 */
		virtual void	request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Req::Api::SetCTRL_REG6_XLReq& msg
									) noexcept=0;

		/** This operation is invoked within the server thread
			whenever a GetCTRL_REG6_XLReq is received.
		 */
		virtual void	request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Req::Api::GetCTRL_REG6_XLReq& msg
									) noexcept=0;

		/** This operation is invoked within the server thread
			whenever a SetCTRL_REG5_XLReq is received.
		 */
		virtual void	request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Req::Api::SetCTRL_REG5_XLReq& msg
									) noexcept=0;

		/** This operation is invoked within the server thread
			whenever a GetCTRL_REG5_XLReq is received.
		 */
		virtual void	request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Req::Api::GetCTRL_REG5_XLReq& msg
									) noexcept=0;

		/** This operation is invoked within the server thread
			whenever a SetCTRL_REG4Req is received.
		 */
		virtual void	request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Req::Api::SetCTRL_REG4Req& msg
									) noexcept=0;

		/** This operation is invoked within the server thread
			whenever a GetCTRL_REG4Req is received.
		 */
		virtual void	request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Req::Api::GetCTRL_REG4Req& msg
									) noexcept=0;

		/** This operation is invoked within the server thread
			whenever a GetOUT_Z_H_GReq is received.
		 */
		virtual void	request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Req::Api::GetOUT_Z_H_GReq& msg
									) noexcept=0;

		/** This operation is invoked within the server thread
			whenever a GetOUT_Z_L_GReq is received.
		 */
		virtual void	request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Req::Api::GetOUT_Z_L_GReq& msg
									) noexcept=0;

		/** This operation is invoked within the server thread
			whenever a GetOUT_Y_H_GReq is received.
		 */
		virtual void	request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Req::Api::GetOUT_Y_H_GReq& msg
									) noexcept=0;

		/** This operation is invoked within the server thread
			whenever a GetOUT_Y_L_GReq is received.
		 */
		virtual void	request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Req::Api::GetOUT_Y_L_GReq& msg
									) noexcept=0;

		/** This operation is invoked within the server thread
			whenever a GetOUT_X_L_GReq is received.
		 */
		virtual void	request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Req::Api::GetOUT_X_L_GReq& msg
									) noexcept=0;

		/** This operation is invoked within the server thread
			whenever a GetSTATUS_REGReq is received.
		 */
		virtual void	request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Req::Api::GetSTATUS_REGReq& msg
									) noexcept=0;

		/** This operation is invoked within the server thread
			whenever a GetOUT_TEMP_HReq is received.
		 */
		virtual void	request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Req::Api::GetOUT_TEMP_HReq& msg
									) noexcept=0;

		/** This operation is invoked within the server thread
			whenever a GetOUT_TEMP_LReq is received.
		 */
		virtual void	request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Req::Api::GetOUT_TEMP_LReq& msg
									) noexcept=0;

		/** This operation is invoked within the server thread
			whenever a GetINT_GEN_SRC_GReq is received.
		 */
		virtual void	request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Req::Api::GetINT_GEN_SRC_GReq& msg
									) noexcept=0;

		/** This operation is invoked within the server thread
			whenever a SetORIENT_CFG_GReq is received.
		 */
		virtual void	request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Req::Api::SetORIENT_CFG_GReq& msg
									) noexcept=0;

		/** This operation is invoked within the server thread
			whenever a GetORIENT_CFG_GReq is received.
		 */
		virtual void	request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Req::Api::GetORIENT_CFG_GReq& msg
									) noexcept=0;

		/** This operation is invoked within the server thread
			whenever a SetCTRL_REG3_GReq is received.
		 */
		virtual void	request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Req::Api::SetCTRL_REG3_GReq& msg
									) noexcept=0;

		/** This operation is invoked within the server thread
			whenever a GetCTRL_REG3_GReq is received.
		 */
		virtual void	request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Req::Api::GetCTRL_REG3_GReq& msg
									) noexcept=0;

		/** This operation is invoked within the server thread
			whenever a SetCTRL_REG2_GReq is received.
		 */
		virtual void	request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Req::Api::SetCTRL_REG2_GReq& msg
									) noexcept=0;

		/** This operation is invoked within the server thread
			whenever a GetCTRL_REG2_GReq is received.
		 */
		virtual void	request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Req::Api::GetCTRL_REG2_GReq& msg
									) noexcept=0;

		/** This operation is invoked within the server thread
			whenever a SetCTRL_REG1_GReq is received.
		 */
		virtual void	request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Req::Api::SetCTRL_REG1_GReq& msg
									) noexcept=0;

		/** This operation is invoked within the server thread
			whenever a GetCTRL_REG1_GReq is received.
		 */
		virtual void	request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Req::Api::GetCTRL_REG1_GReq& msg
									) noexcept=0;

		/** This operation is invoked within the server thread
			whenever a GetWHO_AM_IReq is received.
		 */
		virtual void	request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Req::Api::GetWHO_AM_IReq& msg
									) noexcept=0;

		/** This operation is invoked within the server thread
			whenever a SetINT2_CTRLReq is received.
		 */
		virtual void	request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Req::Api::SetINT2_CTRLReq& msg
									) noexcept=0;

		/** This operation is invoked within the server thread
			whenever a GetINT2_CTRLReq is received.
		 */
		virtual void	request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Req::Api::GetINT2_CTRLReq& msg
									) noexcept=0;

		/** This operation is invoked within the server thread
			whenever a SetINT1_CTRLReq is received.
		 */
		virtual void	request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Req::Api::SetINT1_CTRLReq& msg
									) noexcept=0;

		/** This operation is invoked within the server thread
			whenever a GetINT1_CTRLReq is received.
		 */
		virtual void	request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Req::Api::GetINT1_CTRLReq& msg
									) noexcept=0;

		/** This operation is invoked within the server thread
			whenever a SetREFERENCE_GReq is received.
		 */
		virtual void	request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Req::Api::SetREFERENCE_GReq& msg
									) noexcept=0;

		/** This operation is invoked within the server thread
			whenever a GetREFERENCE_GReq is received.
		 */
		virtual void	request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Req::Api::GetREFERENCE_GReq& msg
									) noexcept=0;

		/** This operation is invoked within the server thread
			whenever a SetINT_GEN_DUR_XLReq is received.
		 */
		virtual void	request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Req::Api::SetINT_GEN_DUR_XLReq& msg
									) noexcept=0;

		/** This operation is invoked within the server thread
			whenever a GetINT_GEN_DUR_XLReq is received.
		 */
		virtual void	request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Req::Api::GetINT_GEN_DUR_XLReq& msg
									) noexcept=0;

		/** This operation is invoked within the server thread
			whenever a SetINT_GEN_THS_Z_XLReq is received.
		 */
		virtual void	request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Req::Api::SetINT_GEN_THS_Z_XLReq& msg
									) noexcept=0;

		/** This operation is invoked within the server thread
			whenever a GetINT_GEN_THS_Z_XLReq is received.
		 */
		virtual void	request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Req::Api::GetINT_GEN_THS_Z_XLReq& msg
									) noexcept=0;

		/** This operation is invoked within the server thread
			whenever a SetINT_GEN_THS_Y_XLReq is received.
		 */
		virtual void	request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Req::Api::SetINT_GEN_THS_Y_XLReq& msg
									) noexcept=0;

		/** This operation is invoked within the server thread
			whenever a GetINT_GEN_THS_Y_XLReq is received.
		 */
		virtual void	request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Req::Api::GetINT_GEN_THS_Y_XLReq& msg
									) noexcept=0;

		/** This operation is invoked within the server thread
			whenever a SetINT_GEN_THS_X_XLReq is received.
		 */
		virtual void	request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Req::Api::SetINT_GEN_THS_X_XLReq& msg
									) noexcept=0;

		/** This operation is invoked within the server thread
			whenever a GetINT_GEN_THS_X_XLReq is received.
		 */
		virtual void	request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Req::Api::GetINT_GEN_THS_X_XLReq& msg
									) noexcept=0;

		/** This operation is invoked within the server thread
			whenever a SetINT_GEN_CFG_XLReq is received.
		 */
		virtual void	request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Req::Api::SetINT_GEN_CFG_XLReq& msg
									) noexcept=0;

		/** This operation is invoked within the server thread
			whenever a GetINT_GEN_CFG_XLReq is received.
		 */
		virtual void	request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Req::Api::GetINT_GEN_CFG_XLReq& msg
									) noexcept=0;

		/** This operation is invoked within the server thread
			whenever a SetACT_DURReq is received.
		 */
		virtual void	request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Req::Api::SetACT_DURReq& msg
									) noexcept=0;

		/** This operation is invoked within the server thread
			whenever a GetACT_DURReq is received.
		 */
		virtual void	request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Req::Api::GetACT_DURReq& msg
									) noexcept=0;

		/** This operation is invoked within the server thread
			whenever a SetACT_THSReq is received.
		 */
		virtual void	request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Req::Api::SetACT_THSReq& msg
									) noexcept=0;

		/** This operation is invoked within the server thread
			whenever a GetACT_THSReq is received.
		 */
		virtual void	request(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Req::Api::GetACT_THSReq& msg
									) noexcept=0;

	};

}
}
}
}
}
}
}
#endif
