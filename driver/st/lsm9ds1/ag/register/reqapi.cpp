/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "reqapi.h"

using namespace Oscl::Driver::ST::LSM9DS1::AG::Register;

Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetOUT_TEMPPayload::
GetOUT_TEMPPayload(
		uint8_t	maxBufferLength,
		void*	buffer
		) noexcept:
		_maxBufferLength(maxBufferLength),
		_buffer(buffer)
		{}

Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetOUT_GPayload::
GetOUT_GPayload(
		uint8_t	maxBufferLength,
		void*	buffer
		) noexcept:
		_maxBufferLength(maxBufferLength),
		_buffer(buffer)
		{}

Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetOUT_XLPayload::
GetOUT_XLPayload(
		uint8_t	maxBufferLength,
		void*	buffer
		) noexcept:
		_maxBufferLength(maxBufferLength),
		_buffer(buffer)
		{}

Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SetINT_GEN_DUR_GPayload::
SetINT_GEN_DUR_GPayload(
		uint8_t	value
		) noexcept:
		_value(value)
		{}

Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetINT_GEN_DUR_GPayload::
GetINT_GEN_DUR_GPayload(
		) noexcept
		{}

Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SetINT_GEN_THS_ZL_GPayload::
SetINT_GEN_THS_ZL_GPayload(
		uint8_t	value
		) noexcept:
		_value(value)
		{}

Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetINT_GEN_THS_ZL_GPayload::
GetINT_GEN_THS_ZL_GPayload(
		) noexcept
		{}

Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SetINT_GEN_THS_ZH_GPayload::
SetINT_GEN_THS_ZH_GPayload(
		uint8_t	value
		) noexcept:
		_value(value)
		{}

Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetINT_GEN_THS_ZH_GPayload::
GetINT_GEN_THS_ZH_GPayload(
		) noexcept
		{}

Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SetINT_GEN_THS_YL_GPayload::
SetINT_GEN_THS_YL_GPayload(
		uint8_t	value
		) noexcept:
		_value(value)
		{}

Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetINT_GEN_THS_YL_GPayload::
GetINT_GEN_THS_YL_GPayload(
		) noexcept
		{}

Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SetINT_GEN_THS_YH_GPayload::
SetINT_GEN_THS_YH_GPayload(
		uint8_t	value
		) noexcept:
		_value(value)
		{}

Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetINT_GEN_THS_YH_GPayload::
GetINT_GEN_THS_YH_GPayload(
		) noexcept
		{}

Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SetINT_GEN_THS_XL_GPayload::
SetINT_GEN_THS_XL_GPayload(
		uint8_t	value
		) noexcept:
		_value(value)
		{}

Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetINT_GEN_THS_XL_GPayload::
GetINT_GEN_THS_XL_GPayload(
		) noexcept
		{}

Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SetINT_GEN_THS_XH_GPayload::
SetINT_GEN_THS_XH_GPayload(
		uint8_t	value
		) noexcept:
		_value(value)
		{}

Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetINT_GEN_THS_XH_GPayload::
GetINT_GEN_THS_XH_GPayload(
		) noexcept
		{}

Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SetINT_GEN_CFG_GPayload::
SetINT_GEN_CFG_GPayload(
		uint8_t	value
		) noexcept:
		_value(value)
		{}

Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetINT_GEN_CFG_GPayload::
GetINT_GEN_CFG_GPayload(
		) noexcept
		{}

Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetFIFO_SRCPayload::
GetFIFO_SRCPayload(
		) noexcept
		{}

Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SetFIFO_CTRLPayload::
SetFIFO_CTRLPayload(
		uint8_t	value
		) noexcept:
		_value(value)
		{}

Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetFIFO_CTRLPayload::
GetFIFO_CTRLPayload(
		) noexcept
		{}

Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetOUT_Z_H_XLPayload::
GetOUT_Z_H_XLPayload(
		) noexcept
		{}

Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetOUT_Z_L_XLPayload::
GetOUT_Z_L_XLPayload(
		) noexcept
		{}

Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetOUT_Y_H_XLPayload::
GetOUT_Y_H_XLPayload(
		) noexcept
		{}

Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetOUT_Y_L_XLPayload::
GetOUT_Y_L_XLPayload(
		) noexcept
		{}

Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetOUT_X_H_XLPayload::
GetOUT_X_H_XLPayload(
		) noexcept
		{}

Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetOUT_X_L_XLPayload::
GetOUT_X_L_XLPayload(
		) noexcept
		{}

Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetSTATUS_REG_2Payload::
GetSTATUS_REG_2Payload(
		) noexcept
		{}

Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetINT_GEN_SRC_XLPayload::
GetINT_GEN_SRC_XLPayload(
		) noexcept
		{}

Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SetCTRL_REG10Payload::
SetCTRL_REG10Payload(
		uint8_t	value
		) noexcept:
		_value(value)
		{}

Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetCTRL_REG10Payload::
GetCTRL_REG10Payload(
		) noexcept
		{}

Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SetCTRL_REG9Payload::
SetCTRL_REG9Payload(
		uint8_t	value
		) noexcept:
		_value(value)
		{}

Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetCTRL_REG9Payload::
GetCTRL_REG9Payload(
		) noexcept
		{}

Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SetCTRL_REG8Payload::
SetCTRL_REG8Payload(
		uint8_t	value
		) noexcept:
		_value(value)
		{}

Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetCTRL_REG8Payload::
GetCTRL_REG8Payload(
		) noexcept
		{}

Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SetCTRL_REG7_XLPayload::
SetCTRL_REG7_XLPayload(
		uint8_t	value
		) noexcept:
		_value(value)
		{}

Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetCTRL_REG7_XLPayload::
GetCTRL_REG7_XLPayload(
		) noexcept
		{}

Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SetCTRL_REG6_XLPayload::
SetCTRL_REG6_XLPayload(
		uint8_t	value
		) noexcept:
		_value(value)
		{}

Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetCTRL_REG6_XLPayload::
GetCTRL_REG6_XLPayload(
		) noexcept
		{}

Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SetCTRL_REG5_XLPayload::
SetCTRL_REG5_XLPayload(
		uint8_t	value
		) noexcept:
		_value(value)
		{}

Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetCTRL_REG5_XLPayload::
GetCTRL_REG5_XLPayload(
		) noexcept
		{}

Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SetCTRL_REG4Payload::
SetCTRL_REG4Payload(
		uint8_t	value
		) noexcept:
		_value(value)
		{}

Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetCTRL_REG4Payload::
GetCTRL_REG4Payload(
		) noexcept
		{}

Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetOUT_Z_H_GPayload::
GetOUT_Z_H_GPayload(
		) noexcept
		{}

Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetOUT_Z_L_GPayload::
GetOUT_Z_L_GPayload(
		) noexcept
		{}

Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetOUT_Y_H_GPayload::
GetOUT_Y_H_GPayload(
		) noexcept
		{}

Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetOUT_Y_L_GPayload::
GetOUT_Y_L_GPayload(
		) noexcept
		{}

Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetOUT_X_L_GPayload::
GetOUT_X_L_GPayload(
		) noexcept
		{}

Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetSTATUS_REGPayload::
GetSTATUS_REGPayload(
		) noexcept
		{}

Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetOUT_TEMP_HPayload::
GetOUT_TEMP_HPayload(
		) noexcept
		{}

Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetOUT_TEMP_LPayload::
GetOUT_TEMP_LPayload(
		) noexcept
		{}

Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetINT_GEN_SRC_GPayload::
GetINT_GEN_SRC_GPayload(
		) noexcept
		{}

Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SetORIENT_CFG_GPayload::
SetORIENT_CFG_GPayload(
		uint8_t	value
		) noexcept:
		_value(value)
		{}

Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetORIENT_CFG_GPayload::
GetORIENT_CFG_GPayload(
		) noexcept
		{}

Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SetCTRL_REG3_GPayload::
SetCTRL_REG3_GPayload(
		uint8_t	value
		) noexcept:
		_value(value)
		{}

Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetCTRL_REG3_GPayload::
GetCTRL_REG3_GPayload(
		) noexcept
		{}

Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SetCTRL_REG2_GPayload::
SetCTRL_REG2_GPayload(
		uint8_t	value
		) noexcept:
		_value(value)
		{}

Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetCTRL_REG2_GPayload::
GetCTRL_REG2_GPayload(
		) noexcept
		{}

Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SetCTRL_REG1_GPayload::
SetCTRL_REG1_GPayload(
		uint8_t	value
		) noexcept:
		_value(value)
		{}

Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetCTRL_REG1_GPayload::
GetCTRL_REG1_GPayload(
		) noexcept
		{}

Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetWHO_AM_IPayload::
GetWHO_AM_IPayload(
		) noexcept
		{}

Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SetINT2_CTRLPayload::
SetINT2_CTRLPayload(
		uint8_t	value
		) noexcept:
		_value(value)
		{}

Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetINT2_CTRLPayload::
GetINT2_CTRLPayload(
		) noexcept
		{}

Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SetINT1_CTRLPayload::
SetINT1_CTRLPayload(
		uint8_t	value
		) noexcept:
		_value(value)
		{}

Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetINT1_CTRLPayload::
GetINT1_CTRLPayload(
		) noexcept
		{}

Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SetREFERENCE_GPayload::
SetREFERENCE_GPayload(
		uint8_t	value
		) noexcept:
		_value(value)
		{}

Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetREFERENCE_GPayload::
GetREFERENCE_GPayload(
		) noexcept
		{}

Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SetINT_GEN_DUR_XLPayload::
SetINT_GEN_DUR_XLPayload(
		uint8_t	value
		) noexcept:
		_value(value)
		{}

Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetINT_GEN_DUR_XLPayload::
GetINT_GEN_DUR_XLPayload(
		) noexcept
		{}

Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SetINT_GEN_THS_Z_XLPayload::
SetINT_GEN_THS_Z_XLPayload(
		uint8_t	value
		) noexcept:
		_value(value)
		{}

Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetINT_GEN_THS_Z_XLPayload::
GetINT_GEN_THS_Z_XLPayload(
		) noexcept
		{}

Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SetINT_GEN_THS_Y_XLPayload::
SetINT_GEN_THS_Y_XLPayload(
		uint8_t	value
		) noexcept:
		_value(value)
		{}

Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetINT_GEN_THS_Y_XLPayload::
GetINT_GEN_THS_Y_XLPayload(
		) noexcept
		{}

Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SetINT_GEN_THS_X_XLPayload::
SetINT_GEN_THS_X_XLPayload(
		uint8_t	value
		) noexcept:
		_value(value)
		{}

Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetINT_GEN_THS_X_XLPayload::
GetINT_GEN_THS_X_XLPayload(
		) noexcept
		{}

Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SetINT_GEN_CFG_XLPayload::
SetINT_GEN_CFG_XLPayload(
		uint8_t	value
		) noexcept:
		_value(value)
		{}

Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetINT_GEN_CFG_XLPayload::
GetINT_GEN_CFG_XLPayload(
		) noexcept
		{}

Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SetACT_DURPayload::
SetACT_DURPayload(
		uint8_t	value
		) noexcept:
		_value(value)
		{}

Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetACT_DURPayload::
GetACT_DURPayload(
		) noexcept
		{}

Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SetACT_THSPayload::
SetACT_THSPayload(
		uint8_t	value
		) noexcept:
		_value(value)
		{}

Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetACT_THSPayload::
GetACT_THSPayload(
		) noexcept
		{}

