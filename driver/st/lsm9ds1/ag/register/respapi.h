/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_driver_st_lsm9ds1_ag_register_itc_respapih_
#define _oscl_driver_st_lsm9ds1_ag_register_itc_respapih_

#include "reqapi.h"
#include "oscl/mt/itc/mbox/clirsp.h"

/** */
namespace Oscl {

/** */
namespace Driver {

/** */
namespace ST {

/** */
namespace LSM9DS1 {

/** */
namespace AG {

/** */
namespace Register {

/** */
namespace Resp {

/**	This class describes an ITC client interface. The
	interface includes the definition of client response messages
	based on their corresponding server request messages and
	associated payloads, as well as an interface that is to be
	implemented by a client that uses the corresponding server
	asynchronously.
 */
/**	This interface defines methods to access the LSM9DS1 IMU
	registers indpendent of the type of bus.
 */
class Api {
	public:
		/**	This describes a client response message that corresponds
			to the GetOUT_TEMPReq message described in the "reqapi.h"
			header file. This GetOUT_TEMPResp response message actually
			contains a GetOUT_TEMPReq request message.
		 */
		typedef Oscl::Mt::Itc::CliResponse<
					Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api,
					Api,
					Oscl::Driver::ST::LSM9DS1::AG::Register::
					Req::Api::GetOUT_TEMPPayload
					>		GetOUT_TEMPResp;

	public:
		/**	This describes a client response message that corresponds
			to the GetOUT_GReq message described in the "reqapi.h"
			header file. This GetOUT_GResp response message actually
			contains a GetOUT_GReq request message.
		 */
		typedef Oscl::Mt::Itc::CliResponse<
					Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api,
					Api,
					Oscl::Driver::ST::LSM9DS1::AG::Register::
					Req::Api::GetOUT_GPayload
					>		GetOUT_GResp;

	public:
		/**	This describes a client response message that corresponds
			to the GetOUT_XLReq message described in the "reqapi.h"
			header file. This GetOUT_XLResp response message actually
			contains a GetOUT_XLReq request message.
		 */
		typedef Oscl::Mt::Itc::CliResponse<
					Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api,
					Api,
					Oscl::Driver::ST::LSM9DS1::AG::Register::
					Req::Api::GetOUT_XLPayload
					>		GetOUT_XLResp;

	public:
		/**	This describes a client response message that corresponds
			to the SetINT_GEN_DUR_GReq message described in the "reqapi.h"
			header file. This SetINT_GEN_DUR_GResp response message actually
			contains a SetINT_GEN_DUR_GReq request message.
		 */
		typedef Oscl::Mt::Itc::CliResponse<
					Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api,
					Api,
					Oscl::Driver::ST::LSM9DS1::AG::Register::
					Req::Api::SetINT_GEN_DUR_GPayload
					>		SetINT_GEN_DUR_GResp;

	public:
		/**	This describes a client response message that corresponds
			to the GetINT_GEN_DUR_GReq message described in the "reqapi.h"
			header file. This GetINT_GEN_DUR_GResp response message actually
			contains a GetINT_GEN_DUR_GReq request message.
		 */
		typedef Oscl::Mt::Itc::CliResponse<
					Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api,
					Api,
					Oscl::Driver::ST::LSM9DS1::AG::Register::
					Req::Api::GetINT_GEN_DUR_GPayload
					>		GetINT_GEN_DUR_GResp;

	public:
		/**	This describes a client response message that corresponds
			to the SetINT_GEN_THS_ZL_GReq message described in the "reqapi.h"
			header file. This SetINT_GEN_THS_ZL_GResp response message actually
			contains a SetINT_GEN_THS_ZL_GReq request message.
		 */
		typedef Oscl::Mt::Itc::CliResponse<
					Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api,
					Api,
					Oscl::Driver::ST::LSM9DS1::AG::Register::
					Req::Api::SetINT_GEN_THS_ZL_GPayload
					>		SetINT_GEN_THS_ZL_GResp;

	public:
		/**	This describes a client response message that corresponds
			to the GetINT_GEN_THS_ZL_GReq message described in the "reqapi.h"
			header file. This GetINT_GEN_THS_ZL_GResp response message actually
			contains a GetINT_GEN_THS_ZL_GReq request message.
		 */
		typedef Oscl::Mt::Itc::CliResponse<
					Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api,
					Api,
					Oscl::Driver::ST::LSM9DS1::AG::Register::
					Req::Api::GetINT_GEN_THS_ZL_GPayload
					>		GetINT_GEN_THS_ZL_GResp;

	public:
		/**	This describes a client response message that corresponds
			to the SetINT_GEN_THS_ZH_GReq message described in the "reqapi.h"
			header file. This SetINT_GEN_THS_ZH_GResp response message actually
			contains a SetINT_GEN_THS_ZH_GReq request message.
		 */
		typedef Oscl::Mt::Itc::CliResponse<
					Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api,
					Api,
					Oscl::Driver::ST::LSM9DS1::AG::Register::
					Req::Api::SetINT_GEN_THS_ZH_GPayload
					>		SetINT_GEN_THS_ZH_GResp;

	public:
		/**	This describes a client response message that corresponds
			to the GetINT_GEN_THS_ZH_GReq message described in the "reqapi.h"
			header file. This GetINT_GEN_THS_ZH_GResp response message actually
			contains a GetINT_GEN_THS_ZH_GReq request message.
		 */
		typedef Oscl::Mt::Itc::CliResponse<
					Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api,
					Api,
					Oscl::Driver::ST::LSM9DS1::AG::Register::
					Req::Api::GetINT_GEN_THS_ZH_GPayload
					>		GetINT_GEN_THS_ZH_GResp;

	public:
		/**	This describes a client response message that corresponds
			to the SetINT_GEN_THS_YL_GReq message described in the "reqapi.h"
			header file. This SetINT_GEN_THS_YL_GResp response message actually
			contains a SetINT_GEN_THS_YL_GReq request message.
		 */
		typedef Oscl::Mt::Itc::CliResponse<
					Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api,
					Api,
					Oscl::Driver::ST::LSM9DS1::AG::Register::
					Req::Api::SetINT_GEN_THS_YL_GPayload
					>		SetINT_GEN_THS_YL_GResp;

	public:
		/**	This describes a client response message that corresponds
			to the GetINT_GEN_THS_YL_GReq message described in the "reqapi.h"
			header file. This GetINT_GEN_THS_YL_GResp response message actually
			contains a GetINT_GEN_THS_YL_GReq request message.
		 */
		typedef Oscl::Mt::Itc::CliResponse<
					Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api,
					Api,
					Oscl::Driver::ST::LSM9DS1::AG::Register::
					Req::Api::GetINT_GEN_THS_YL_GPayload
					>		GetINT_GEN_THS_YL_GResp;

	public:
		/**	This describes a client response message that corresponds
			to the SetINT_GEN_THS_YH_GReq message described in the "reqapi.h"
			header file. This SetINT_GEN_THS_YH_GResp response message actually
			contains a SetINT_GEN_THS_YH_GReq request message.
		 */
		typedef Oscl::Mt::Itc::CliResponse<
					Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api,
					Api,
					Oscl::Driver::ST::LSM9DS1::AG::Register::
					Req::Api::SetINT_GEN_THS_YH_GPayload
					>		SetINT_GEN_THS_YH_GResp;

	public:
		/**	This describes a client response message that corresponds
			to the GetINT_GEN_THS_YH_GReq message described in the "reqapi.h"
			header file. This GetINT_GEN_THS_YH_GResp response message actually
			contains a GetINT_GEN_THS_YH_GReq request message.
		 */
		typedef Oscl::Mt::Itc::CliResponse<
					Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api,
					Api,
					Oscl::Driver::ST::LSM9DS1::AG::Register::
					Req::Api::GetINT_GEN_THS_YH_GPayload
					>		GetINT_GEN_THS_YH_GResp;

	public:
		/**	This describes a client response message that corresponds
			to the SetINT_GEN_THS_XL_GReq message described in the "reqapi.h"
			header file. This SetINT_GEN_THS_XL_GResp response message actually
			contains a SetINT_GEN_THS_XL_GReq request message.
		 */
		typedef Oscl::Mt::Itc::CliResponse<
					Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api,
					Api,
					Oscl::Driver::ST::LSM9DS1::AG::Register::
					Req::Api::SetINT_GEN_THS_XL_GPayload
					>		SetINT_GEN_THS_XL_GResp;

	public:
		/**	This describes a client response message that corresponds
			to the GetINT_GEN_THS_XL_GReq message described in the "reqapi.h"
			header file. This GetINT_GEN_THS_XL_GResp response message actually
			contains a GetINT_GEN_THS_XL_GReq request message.
		 */
		typedef Oscl::Mt::Itc::CliResponse<
					Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api,
					Api,
					Oscl::Driver::ST::LSM9DS1::AG::Register::
					Req::Api::GetINT_GEN_THS_XL_GPayload
					>		GetINT_GEN_THS_XL_GResp;

	public:
		/**	This describes a client response message that corresponds
			to the SetINT_GEN_THS_XH_GReq message described in the "reqapi.h"
			header file. This SetINT_GEN_THS_XH_GResp response message actually
			contains a SetINT_GEN_THS_XH_GReq request message.
		 */
		typedef Oscl::Mt::Itc::CliResponse<
					Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api,
					Api,
					Oscl::Driver::ST::LSM9DS1::AG::Register::
					Req::Api::SetINT_GEN_THS_XH_GPayload
					>		SetINT_GEN_THS_XH_GResp;

	public:
		/**	This describes a client response message that corresponds
			to the GetINT_GEN_THS_XH_GReq message described in the "reqapi.h"
			header file. This GetINT_GEN_THS_XH_GResp response message actually
			contains a GetINT_GEN_THS_XH_GReq request message.
		 */
		typedef Oscl::Mt::Itc::CliResponse<
					Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api,
					Api,
					Oscl::Driver::ST::LSM9DS1::AG::Register::
					Req::Api::GetINT_GEN_THS_XH_GPayload
					>		GetINT_GEN_THS_XH_GResp;

	public:
		/**	This describes a client response message that corresponds
			to the SetINT_GEN_CFG_GReq message described in the "reqapi.h"
			header file. This SetINT_GEN_CFG_GResp response message actually
			contains a SetINT_GEN_CFG_GReq request message.
		 */
		typedef Oscl::Mt::Itc::CliResponse<
					Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api,
					Api,
					Oscl::Driver::ST::LSM9DS1::AG::Register::
					Req::Api::SetINT_GEN_CFG_GPayload
					>		SetINT_GEN_CFG_GResp;

	public:
		/**	This describes a client response message that corresponds
			to the GetINT_GEN_CFG_GReq message described in the "reqapi.h"
			header file. This GetINT_GEN_CFG_GResp response message actually
			contains a GetINT_GEN_CFG_GReq request message.
		 */
		typedef Oscl::Mt::Itc::CliResponse<
					Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api,
					Api,
					Oscl::Driver::ST::LSM9DS1::AG::Register::
					Req::Api::GetINT_GEN_CFG_GPayload
					>		GetINT_GEN_CFG_GResp;

	public:
		/**	This describes a client response message that corresponds
			to the GetFIFO_SRCReq message described in the "reqapi.h"
			header file. This GetFIFO_SRCResp response message actually
			contains a GetFIFO_SRCReq request message.
		 */
		typedef Oscl::Mt::Itc::CliResponse<
					Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api,
					Api,
					Oscl::Driver::ST::LSM9DS1::AG::Register::
					Req::Api::GetFIFO_SRCPayload
					>		GetFIFO_SRCResp;

	public:
		/**	This describes a client response message that corresponds
			to the SetFIFO_CTRLReq message described in the "reqapi.h"
			header file. This SetFIFO_CTRLResp response message actually
			contains a SetFIFO_CTRLReq request message.
		 */
		typedef Oscl::Mt::Itc::CliResponse<
					Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api,
					Api,
					Oscl::Driver::ST::LSM9DS1::AG::Register::
					Req::Api::SetFIFO_CTRLPayload
					>		SetFIFO_CTRLResp;

	public:
		/**	This describes a client response message that corresponds
			to the GetFIFO_CTRLReq message described in the "reqapi.h"
			header file. This GetFIFO_CTRLResp response message actually
			contains a GetFIFO_CTRLReq request message.
		 */
		typedef Oscl::Mt::Itc::CliResponse<
					Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api,
					Api,
					Oscl::Driver::ST::LSM9DS1::AG::Register::
					Req::Api::GetFIFO_CTRLPayload
					>		GetFIFO_CTRLResp;

	public:
		/**	This describes a client response message that corresponds
			to the GetOUT_Z_H_XLReq message described in the "reqapi.h"
			header file. This GetOUT_Z_H_XLResp response message actually
			contains a GetOUT_Z_H_XLReq request message.
		 */
		typedef Oscl::Mt::Itc::CliResponse<
					Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api,
					Api,
					Oscl::Driver::ST::LSM9DS1::AG::Register::
					Req::Api::GetOUT_Z_H_XLPayload
					>		GetOUT_Z_H_XLResp;

	public:
		/**	This describes a client response message that corresponds
			to the GetOUT_Z_L_XLReq message described in the "reqapi.h"
			header file. This GetOUT_Z_L_XLResp response message actually
			contains a GetOUT_Z_L_XLReq request message.
		 */
		typedef Oscl::Mt::Itc::CliResponse<
					Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api,
					Api,
					Oscl::Driver::ST::LSM9DS1::AG::Register::
					Req::Api::GetOUT_Z_L_XLPayload
					>		GetOUT_Z_L_XLResp;

	public:
		/**	This describes a client response message that corresponds
			to the GetOUT_Y_H_XLReq message described in the "reqapi.h"
			header file. This GetOUT_Y_H_XLResp response message actually
			contains a GetOUT_Y_H_XLReq request message.
		 */
		typedef Oscl::Mt::Itc::CliResponse<
					Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api,
					Api,
					Oscl::Driver::ST::LSM9DS1::AG::Register::
					Req::Api::GetOUT_Y_H_XLPayload
					>		GetOUT_Y_H_XLResp;

	public:
		/**	This describes a client response message that corresponds
			to the GetOUT_Y_L_XLReq message described in the "reqapi.h"
			header file. This GetOUT_Y_L_XLResp response message actually
			contains a GetOUT_Y_L_XLReq request message.
		 */
		typedef Oscl::Mt::Itc::CliResponse<
					Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api,
					Api,
					Oscl::Driver::ST::LSM9DS1::AG::Register::
					Req::Api::GetOUT_Y_L_XLPayload
					>		GetOUT_Y_L_XLResp;

	public:
		/**	This describes a client response message that corresponds
			to the GetOUT_X_H_XLReq message described in the "reqapi.h"
			header file. This GetOUT_X_H_XLResp response message actually
			contains a GetOUT_X_H_XLReq request message.
		 */
		typedef Oscl::Mt::Itc::CliResponse<
					Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api,
					Api,
					Oscl::Driver::ST::LSM9DS1::AG::Register::
					Req::Api::GetOUT_X_H_XLPayload
					>		GetOUT_X_H_XLResp;

	public:
		/**	This describes a client response message that corresponds
			to the GetOUT_X_L_XLReq message described in the "reqapi.h"
			header file. This GetOUT_X_L_XLResp response message actually
			contains a GetOUT_X_L_XLReq request message.
		 */
		typedef Oscl::Mt::Itc::CliResponse<
					Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api,
					Api,
					Oscl::Driver::ST::LSM9DS1::AG::Register::
					Req::Api::GetOUT_X_L_XLPayload
					>		GetOUT_X_L_XLResp;

	public:
		/**	This describes a client response message that corresponds
			to the GetSTATUS_REG_2Req message described in the "reqapi.h"
			header file. This GetSTATUS_REG_2Resp response message actually
			contains a GetSTATUS_REG_2Req request message.
		 */
		typedef Oscl::Mt::Itc::CliResponse<
					Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api,
					Api,
					Oscl::Driver::ST::LSM9DS1::AG::Register::
					Req::Api::GetSTATUS_REG_2Payload
					>		GetSTATUS_REG_2Resp;

	public:
		/**	This describes a client response message that corresponds
			to the GetINT_GEN_SRC_XLReq message described in the "reqapi.h"
			header file. This GetINT_GEN_SRC_XLResp response message actually
			contains a GetINT_GEN_SRC_XLReq request message.
		 */
		typedef Oscl::Mt::Itc::CliResponse<
					Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api,
					Api,
					Oscl::Driver::ST::LSM9DS1::AG::Register::
					Req::Api::GetINT_GEN_SRC_XLPayload
					>		GetINT_GEN_SRC_XLResp;

	public:
		/**	This describes a client response message that corresponds
			to the SetCTRL_REG10Req message described in the "reqapi.h"
			header file. This SetCTRL_REG10Resp response message actually
			contains a SetCTRL_REG10Req request message.
		 */
		typedef Oscl::Mt::Itc::CliResponse<
					Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api,
					Api,
					Oscl::Driver::ST::LSM9DS1::AG::Register::
					Req::Api::SetCTRL_REG10Payload
					>		SetCTRL_REG10Resp;

	public:
		/**	This describes a client response message that corresponds
			to the GetCTRL_REG10Req message described in the "reqapi.h"
			header file. This GetCTRL_REG10Resp response message actually
			contains a GetCTRL_REG10Req request message.
		 */
		typedef Oscl::Mt::Itc::CliResponse<
					Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api,
					Api,
					Oscl::Driver::ST::LSM9DS1::AG::Register::
					Req::Api::GetCTRL_REG10Payload
					>		GetCTRL_REG10Resp;

	public:
		/**	This describes a client response message that corresponds
			to the SetCTRL_REG9Req message described in the "reqapi.h"
			header file. This SetCTRL_REG9Resp response message actually
			contains a SetCTRL_REG9Req request message.
		 */
		typedef Oscl::Mt::Itc::CliResponse<
					Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api,
					Api,
					Oscl::Driver::ST::LSM9DS1::AG::Register::
					Req::Api::SetCTRL_REG9Payload
					>		SetCTRL_REG9Resp;

	public:
		/**	This describes a client response message that corresponds
			to the GetCTRL_REG9Req message described in the "reqapi.h"
			header file. This GetCTRL_REG9Resp response message actually
			contains a GetCTRL_REG9Req request message.
		 */
		typedef Oscl::Mt::Itc::CliResponse<
					Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api,
					Api,
					Oscl::Driver::ST::LSM9DS1::AG::Register::
					Req::Api::GetCTRL_REG9Payload
					>		GetCTRL_REG9Resp;

	public:
		/**	This describes a client response message that corresponds
			to the SetCTRL_REG8Req message described in the "reqapi.h"
			header file. This SetCTRL_REG8Resp response message actually
			contains a SetCTRL_REG8Req request message.
		 */
		typedef Oscl::Mt::Itc::CliResponse<
					Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api,
					Api,
					Oscl::Driver::ST::LSM9DS1::AG::Register::
					Req::Api::SetCTRL_REG8Payload
					>		SetCTRL_REG8Resp;

	public:
		/**	This describes a client response message that corresponds
			to the GetCTRL_REG8Req message described in the "reqapi.h"
			header file. This GetCTRL_REG8Resp response message actually
			contains a GetCTRL_REG8Req request message.
		 */
		typedef Oscl::Mt::Itc::CliResponse<
					Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api,
					Api,
					Oscl::Driver::ST::LSM9DS1::AG::Register::
					Req::Api::GetCTRL_REG8Payload
					>		GetCTRL_REG8Resp;

	public:
		/**	This describes a client response message that corresponds
			to the SetCTRL_REG7_XLReq message described in the "reqapi.h"
			header file. This SetCTRL_REG7_XLResp response message actually
			contains a SetCTRL_REG7_XLReq request message.
		 */
		typedef Oscl::Mt::Itc::CliResponse<
					Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api,
					Api,
					Oscl::Driver::ST::LSM9DS1::AG::Register::
					Req::Api::SetCTRL_REG7_XLPayload
					>		SetCTRL_REG7_XLResp;

	public:
		/**	This describes a client response message that corresponds
			to the GetCTRL_REG7_XLReq message described in the "reqapi.h"
			header file. This GetCTRL_REG7_XLResp response message actually
			contains a GetCTRL_REG7_XLReq request message.
		 */
		typedef Oscl::Mt::Itc::CliResponse<
					Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api,
					Api,
					Oscl::Driver::ST::LSM9DS1::AG::Register::
					Req::Api::GetCTRL_REG7_XLPayload
					>		GetCTRL_REG7_XLResp;

	public:
		/**	This describes a client response message that corresponds
			to the SetCTRL_REG6_XLReq message described in the "reqapi.h"
			header file. This SetCTRL_REG6_XLResp response message actually
			contains a SetCTRL_REG6_XLReq request message.
		 */
		typedef Oscl::Mt::Itc::CliResponse<
					Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api,
					Api,
					Oscl::Driver::ST::LSM9DS1::AG::Register::
					Req::Api::SetCTRL_REG6_XLPayload
					>		SetCTRL_REG6_XLResp;

	public:
		/**	This describes a client response message that corresponds
			to the GetCTRL_REG6_XLReq message described in the "reqapi.h"
			header file. This GetCTRL_REG6_XLResp response message actually
			contains a GetCTRL_REG6_XLReq request message.
		 */
		typedef Oscl::Mt::Itc::CliResponse<
					Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api,
					Api,
					Oscl::Driver::ST::LSM9DS1::AG::Register::
					Req::Api::GetCTRL_REG6_XLPayload
					>		GetCTRL_REG6_XLResp;

	public:
		/**	This describes a client response message that corresponds
			to the SetCTRL_REG5_XLReq message described in the "reqapi.h"
			header file. This SetCTRL_REG5_XLResp response message actually
			contains a SetCTRL_REG5_XLReq request message.
		 */
		typedef Oscl::Mt::Itc::CliResponse<
					Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api,
					Api,
					Oscl::Driver::ST::LSM9DS1::AG::Register::
					Req::Api::SetCTRL_REG5_XLPayload
					>		SetCTRL_REG5_XLResp;

	public:
		/**	This describes a client response message that corresponds
			to the GetCTRL_REG5_XLReq message described in the "reqapi.h"
			header file. This GetCTRL_REG5_XLResp response message actually
			contains a GetCTRL_REG5_XLReq request message.
		 */
		typedef Oscl::Mt::Itc::CliResponse<
					Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api,
					Api,
					Oscl::Driver::ST::LSM9DS1::AG::Register::
					Req::Api::GetCTRL_REG5_XLPayload
					>		GetCTRL_REG5_XLResp;

	public:
		/**	This describes a client response message that corresponds
			to the SetCTRL_REG4Req message described in the "reqapi.h"
			header file. This SetCTRL_REG4Resp response message actually
			contains a SetCTRL_REG4Req request message.
		 */
		typedef Oscl::Mt::Itc::CliResponse<
					Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api,
					Api,
					Oscl::Driver::ST::LSM9DS1::AG::Register::
					Req::Api::SetCTRL_REG4Payload
					>		SetCTRL_REG4Resp;

	public:
		/**	This describes a client response message that corresponds
			to the GetCTRL_REG4Req message described in the "reqapi.h"
			header file. This GetCTRL_REG4Resp response message actually
			contains a GetCTRL_REG4Req request message.
		 */
		typedef Oscl::Mt::Itc::CliResponse<
					Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api,
					Api,
					Oscl::Driver::ST::LSM9DS1::AG::Register::
					Req::Api::GetCTRL_REG4Payload
					>		GetCTRL_REG4Resp;

	public:
		/**	This describes a client response message that corresponds
			to the GetOUT_Z_H_GReq message described in the "reqapi.h"
			header file. This GetOUT_Z_H_GResp response message actually
			contains a GetOUT_Z_H_GReq request message.
		 */
		typedef Oscl::Mt::Itc::CliResponse<
					Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api,
					Api,
					Oscl::Driver::ST::LSM9DS1::AG::Register::
					Req::Api::GetOUT_Z_H_GPayload
					>		GetOUT_Z_H_GResp;

	public:
		/**	This describes a client response message that corresponds
			to the GetOUT_Z_L_GReq message described in the "reqapi.h"
			header file. This GetOUT_Z_L_GResp response message actually
			contains a GetOUT_Z_L_GReq request message.
		 */
		typedef Oscl::Mt::Itc::CliResponse<
					Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api,
					Api,
					Oscl::Driver::ST::LSM9DS1::AG::Register::
					Req::Api::GetOUT_Z_L_GPayload
					>		GetOUT_Z_L_GResp;

	public:
		/**	This describes a client response message that corresponds
			to the GetOUT_Y_H_GReq message described in the "reqapi.h"
			header file. This GetOUT_Y_H_GResp response message actually
			contains a GetOUT_Y_H_GReq request message.
		 */
		typedef Oscl::Mt::Itc::CliResponse<
					Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api,
					Api,
					Oscl::Driver::ST::LSM9DS1::AG::Register::
					Req::Api::GetOUT_Y_H_GPayload
					>		GetOUT_Y_H_GResp;

	public:
		/**	This describes a client response message that corresponds
			to the GetOUT_Y_L_GReq message described in the "reqapi.h"
			header file. This GetOUT_Y_L_GResp response message actually
			contains a GetOUT_Y_L_GReq request message.
		 */
		typedef Oscl::Mt::Itc::CliResponse<
					Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api,
					Api,
					Oscl::Driver::ST::LSM9DS1::AG::Register::
					Req::Api::GetOUT_Y_L_GPayload
					>		GetOUT_Y_L_GResp;

	public:
		/**	This describes a client response message that corresponds
			to the GetOUT_X_L_GReq message described in the "reqapi.h"
			header file. This GetOUT_X_L_GResp response message actually
			contains a GetOUT_X_L_GReq request message.
		 */
		typedef Oscl::Mt::Itc::CliResponse<
					Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api,
					Api,
					Oscl::Driver::ST::LSM9DS1::AG::Register::
					Req::Api::GetOUT_X_L_GPayload
					>		GetOUT_X_L_GResp;

	public:
		/**	This describes a client response message that corresponds
			to the GetSTATUS_REGReq message described in the "reqapi.h"
			header file. This GetSTATUS_REGResp response message actually
			contains a GetSTATUS_REGReq request message.
		 */
		typedef Oscl::Mt::Itc::CliResponse<
					Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api,
					Api,
					Oscl::Driver::ST::LSM9DS1::AG::Register::
					Req::Api::GetSTATUS_REGPayload
					>		GetSTATUS_REGResp;

	public:
		/**	This describes a client response message that corresponds
			to the GetOUT_TEMP_HReq message described in the "reqapi.h"
			header file. This GetOUT_TEMP_HResp response message actually
			contains a GetOUT_TEMP_HReq request message.
		 */
		typedef Oscl::Mt::Itc::CliResponse<
					Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api,
					Api,
					Oscl::Driver::ST::LSM9DS1::AG::Register::
					Req::Api::GetOUT_TEMP_HPayload
					>		GetOUT_TEMP_HResp;

	public:
		/**	This describes a client response message that corresponds
			to the GetOUT_TEMP_LReq message described in the "reqapi.h"
			header file. This GetOUT_TEMP_LResp response message actually
			contains a GetOUT_TEMP_LReq request message.
		 */
		typedef Oscl::Mt::Itc::CliResponse<
					Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api,
					Api,
					Oscl::Driver::ST::LSM9DS1::AG::Register::
					Req::Api::GetOUT_TEMP_LPayload
					>		GetOUT_TEMP_LResp;

	public:
		/**	This describes a client response message that corresponds
			to the GetINT_GEN_SRC_GReq message described in the "reqapi.h"
			header file. This GetINT_GEN_SRC_GResp response message actually
			contains a GetINT_GEN_SRC_GReq request message.
		 */
		typedef Oscl::Mt::Itc::CliResponse<
					Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api,
					Api,
					Oscl::Driver::ST::LSM9DS1::AG::Register::
					Req::Api::GetINT_GEN_SRC_GPayload
					>		GetINT_GEN_SRC_GResp;

	public:
		/**	This describes a client response message that corresponds
			to the SetORIENT_CFG_GReq message described in the "reqapi.h"
			header file. This SetORIENT_CFG_GResp response message actually
			contains a SetORIENT_CFG_GReq request message.
		 */
		typedef Oscl::Mt::Itc::CliResponse<
					Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api,
					Api,
					Oscl::Driver::ST::LSM9DS1::AG::Register::
					Req::Api::SetORIENT_CFG_GPayload
					>		SetORIENT_CFG_GResp;

	public:
		/**	This describes a client response message that corresponds
			to the GetORIENT_CFG_GReq message described in the "reqapi.h"
			header file. This GetORIENT_CFG_GResp response message actually
			contains a GetORIENT_CFG_GReq request message.
		 */
		typedef Oscl::Mt::Itc::CliResponse<
					Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api,
					Api,
					Oscl::Driver::ST::LSM9DS1::AG::Register::
					Req::Api::GetORIENT_CFG_GPayload
					>		GetORIENT_CFG_GResp;

	public:
		/**	This describes a client response message that corresponds
			to the SetCTRL_REG3_GReq message described in the "reqapi.h"
			header file. This SetCTRL_REG3_GResp response message actually
			contains a SetCTRL_REG3_GReq request message.
		 */
		typedef Oscl::Mt::Itc::CliResponse<
					Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api,
					Api,
					Oscl::Driver::ST::LSM9DS1::AG::Register::
					Req::Api::SetCTRL_REG3_GPayload
					>		SetCTRL_REG3_GResp;

	public:
		/**	This describes a client response message that corresponds
			to the GetCTRL_REG3_GReq message described in the "reqapi.h"
			header file. This GetCTRL_REG3_GResp response message actually
			contains a GetCTRL_REG3_GReq request message.
		 */
		typedef Oscl::Mt::Itc::CliResponse<
					Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api,
					Api,
					Oscl::Driver::ST::LSM9DS1::AG::Register::
					Req::Api::GetCTRL_REG3_GPayload
					>		GetCTRL_REG3_GResp;

	public:
		/**	This describes a client response message that corresponds
			to the SetCTRL_REG2_GReq message described in the "reqapi.h"
			header file. This SetCTRL_REG2_GResp response message actually
			contains a SetCTRL_REG2_GReq request message.
		 */
		typedef Oscl::Mt::Itc::CliResponse<
					Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api,
					Api,
					Oscl::Driver::ST::LSM9DS1::AG::Register::
					Req::Api::SetCTRL_REG2_GPayload
					>		SetCTRL_REG2_GResp;

	public:
		/**	This describes a client response message that corresponds
			to the GetCTRL_REG2_GReq message described in the "reqapi.h"
			header file. This GetCTRL_REG2_GResp response message actually
			contains a GetCTRL_REG2_GReq request message.
		 */
		typedef Oscl::Mt::Itc::CliResponse<
					Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api,
					Api,
					Oscl::Driver::ST::LSM9DS1::AG::Register::
					Req::Api::GetCTRL_REG2_GPayload
					>		GetCTRL_REG2_GResp;

	public:
		/**	This describes a client response message that corresponds
			to the SetCTRL_REG1_GReq message described in the "reqapi.h"
			header file. This SetCTRL_REG1_GResp response message actually
			contains a SetCTRL_REG1_GReq request message.
		 */
		typedef Oscl::Mt::Itc::CliResponse<
					Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api,
					Api,
					Oscl::Driver::ST::LSM9DS1::AG::Register::
					Req::Api::SetCTRL_REG1_GPayload
					>		SetCTRL_REG1_GResp;

	public:
		/**	This describes a client response message that corresponds
			to the GetCTRL_REG1_GReq message described in the "reqapi.h"
			header file. This GetCTRL_REG1_GResp response message actually
			contains a GetCTRL_REG1_GReq request message.
		 */
		typedef Oscl::Mt::Itc::CliResponse<
					Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api,
					Api,
					Oscl::Driver::ST::LSM9DS1::AG::Register::
					Req::Api::GetCTRL_REG1_GPayload
					>		GetCTRL_REG1_GResp;

	public:
		/**	This describes a client response message that corresponds
			to the GetWHO_AM_IReq message described in the "reqapi.h"
			header file. This GetWHO_AM_IResp response message actually
			contains a GetWHO_AM_IReq request message.
		 */
		typedef Oscl::Mt::Itc::CliResponse<
					Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api,
					Api,
					Oscl::Driver::ST::LSM9DS1::AG::Register::
					Req::Api::GetWHO_AM_IPayload
					>		GetWHO_AM_IResp;

	public:
		/**	This describes a client response message that corresponds
			to the SetINT2_CTRLReq message described in the "reqapi.h"
			header file. This SetINT2_CTRLResp response message actually
			contains a SetINT2_CTRLReq request message.
		 */
		typedef Oscl::Mt::Itc::CliResponse<
					Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api,
					Api,
					Oscl::Driver::ST::LSM9DS1::AG::Register::
					Req::Api::SetINT2_CTRLPayload
					>		SetINT2_CTRLResp;

	public:
		/**	This describes a client response message that corresponds
			to the GetINT2_CTRLReq message described in the "reqapi.h"
			header file. This GetINT2_CTRLResp response message actually
			contains a GetINT2_CTRLReq request message.
		 */
		typedef Oscl::Mt::Itc::CliResponse<
					Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api,
					Api,
					Oscl::Driver::ST::LSM9DS1::AG::Register::
					Req::Api::GetINT2_CTRLPayload
					>		GetINT2_CTRLResp;

	public:
		/**	This describes a client response message that corresponds
			to the SetINT1_CTRLReq message described in the "reqapi.h"
			header file. This SetINT1_CTRLResp response message actually
			contains a SetINT1_CTRLReq request message.
		 */
		typedef Oscl::Mt::Itc::CliResponse<
					Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api,
					Api,
					Oscl::Driver::ST::LSM9DS1::AG::Register::
					Req::Api::SetINT1_CTRLPayload
					>		SetINT1_CTRLResp;

	public:
		/**	This describes a client response message that corresponds
			to the GetINT1_CTRLReq message described in the "reqapi.h"
			header file. This GetINT1_CTRLResp response message actually
			contains a GetINT1_CTRLReq request message.
		 */
		typedef Oscl::Mt::Itc::CliResponse<
					Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api,
					Api,
					Oscl::Driver::ST::LSM9DS1::AG::Register::
					Req::Api::GetINT1_CTRLPayload
					>		GetINT1_CTRLResp;

	public:
		/**	This describes a client response message that corresponds
			to the SetREFERENCE_GReq message described in the "reqapi.h"
			header file. This SetREFERENCE_GResp response message actually
			contains a SetREFERENCE_GReq request message.
		 */
		typedef Oscl::Mt::Itc::CliResponse<
					Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api,
					Api,
					Oscl::Driver::ST::LSM9DS1::AG::Register::
					Req::Api::SetREFERENCE_GPayload
					>		SetREFERENCE_GResp;

	public:
		/**	This describes a client response message that corresponds
			to the GetREFERENCE_GReq message described in the "reqapi.h"
			header file. This GetREFERENCE_GResp response message actually
			contains a GetREFERENCE_GReq request message.
		 */
		typedef Oscl::Mt::Itc::CliResponse<
					Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api,
					Api,
					Oscl::Driver::ST::LSM9DS1::AG::Register::
					Req::Api::GetREFERENCE_GPayload
					>		GetREFERENCE_GResp;

	public:
		/**	This describes a client response message that corresponds
			to the SetINT_GEN_DUR_XLReq message described in the "reqapi.h"
			header file. This SetINT_GEN_DUR_XLResp response message actually
			contains a SetINT_GEN_DUR_XLReq request message.
		 */
		typedef Oscl::Mt::Itc::CliResponse<
					Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api,
					Api,
					Oscl::Driver::ST::LSM9DS1::AG::Register::
					Req::Api::SetINT_GEN_DUR_XLPayload
					>		SetINT_GEN_DUR_XLResp;

	public:
		/**	This describes a client response message that corresponds
			to the GetINT_GEN_DUR_XLReq message described in the "reqapi.h"
			header file. This GetINT_GEN_DUR_XLResp response message actually
			contains a GetINT_GEN_DUR_XLReq request message.
		 */
		typedef Oscl::Mt::Itc::CliResponse<
					Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api,
					Api,
					Oscl::Driver::ST::LSM9DS1::AG::Register::
					Req::Api::GetINT_GEN_DUR_XLPayload
					>		GetINT_GEN_DUR_XLResp;

	public:
		/**	This describes a client response message that corresponds
			to the SetINT_GEN_THS_Z_XLReq message described in the "reqapi.h"
			header file. This SetINT_GEN_THS_Z_XLResp response message actually
			contains a SetINT_GEN_THS_Z_XLReq request message.
		 */
		typedef Oscl::Mt::Itc::CliResponse<
					Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api,
					Api,
					Oscl::Driver::ST::LSM9DS1::AG::Register::
					Req::Api::SetINT_GEN_THS_Z_XLPayload
					>		SetINT_GEN_THS_Z_XLResp;

	public:
		/**	This describes a client response message that corresponds
			to the GetINT_GEN_THS_Z_XLReq message described in the "reqapi.h"
			header file. This GetINT_GEN_THS_Z_XLResp response message actually
			contains a GetINT_GEN_THS_Z_XLReq request message.
		 */
		typedef Oscl::Mt::Itc::CliResponse<
					Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api,
					Api,
					Oscl::Driver::ST::LSM9DS1::AG::Register::
					Req::Api::GetINT_GEN_THS_Z_XLPayload
					>		GetINT_GEN_THS_Z_XLResp;

	public:
		/**	This describes a client response message that corresponds
			to the SetINT_GEN_THS_Y_XLReq message described in the "reqapi.h"
			header file. This SetINT_GEN_THS_Y_XLResp response message actually
			contains a SetINT_GEN_THS_Y_XLReq request message.
		 */
		typedef Oscl::Mt::Itc::CliResponse<
					Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api,
					Api,
					Oscl::Driver::ST::LSM9DS1::AG::Register::
					Req::Api::SetINT_GEN_THS_Y_XLPayload
					>		SetINT_GEN_THS_Y_XLResp;

	public:
		/**	This describes a client response message that corresponds
			to the GetINT_GEN_THS_Y_XLReq message described in the "reqapi.h"
			header file. This GetINT_GEN_THS_Y_XLResp response message actually
			contains a GetINT_GEN_THS_Y_XLReq request message.
		 */
		typedef Oscl::Mt::Itc::CliResponse<
					Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api,
					Api,
					Oscl::Driver::ST::LSM9DS1::AG::Register::
					Req::Api::GetINT_GEN_THS_Y_XLPayload
					>		GetINT_GEN_THS_Y_XLResp;

	public:
		/**	This describes a client response message that corresponds
			to the SetINT_GEN_THS_X_XLReq message described in the "reqapi.h"
			header file. This SetINT_GEN_THS_X_XLResp response message actually
			contains a SetINT_GEN_THS_X_XLReq request message.
		 */
		typedef Oscl::Mt::Itc::CliResponse<
					Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api,
					Api,
					Oscl::Driver::ST::LSM9DS1::AG::Register::
					Req::Api::SetINT_GEN_THS_X_XLPayload
					>		SetINT_GEN_THS_X_XLResp;

	public:
		/**	This describes a client response message that corresponds
			to the GetINT_GEN_THS_X_XLReq message described in the "reqapi.h"
			header file. This GetINT_GEN_THS_X_XLResp response message actually
			contains a GetINT_GEN_THS_X_XLReq request message.
		 */
		typedef Oscl::Mt::Itc::CliResponse<
					Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api,
					Api,
					Oscl::Driver::ST::LSM9DS1::AG::Register::
					Req::Api::GetINT_GEN_THS_X_XLPayload
					>		GetINT_GEN_THS_X_XLResp;

	public:
		/**	This describes a client response message that corresponds
			to the SetINT_GEN_CFG_XLReq message described in the "reqapi.h"
			header file. This SetINT_GEN_CFG_XLResp response message actually
			contains a SetINT_GEN_CFG_XLReq request message.
		 */
		typedef Oscl::Mt::Itc::CliResponse<
					Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api,
					Api,
					Oscl::Driver::ST::LSM9DS1::AG::Register::
					Req::Api::SetINT_GEN_CFG_XLPayload
					>		SetINT_GEN_CFG_XLResp;

	public:
		/**	This describes a client response message that corresponds
			to the GetINT_GEN_CFG_XLReq message described in the "reqapi.h"
			header file. This GetINT_GEN_CFG_XLResp response message actually
			contains a GetINT_GEN_CFG_XLReq request message.
		 */
		typedef Oscl::Mt::Itc::CliResponse<
					Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api,
					Api,
					Oscl::Driver::ST::LSM9DS1::AG::Register::
					Req::Api::GetINT_GEN_CFG_XLPayload
					>		GetINT_GEN_CFG_XLResp;

	public:
		/**	This describes a client response message that corresponds
			to the SetACT_DURReq message described in the "reqapi.h"
			header file. This SetACT_DURResp response message actually
			contains a SetACT_DURReq request message.
		 */
		typedef Oscl::Mt::Itc::CliResponse<
					Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api,
					Api,
					Oscl::Driver::ST::LSM9DS1::AG::Register::
					Req::Api::SetACT_DURPayload
					>		SetACT_DURResp;

	public:
		/**	This describes a client response message that corresponds
			to the GetACT_DURReq message described in the "reqapi.h"
			header file. This GetACT_DURResp response message actually
			contains a GetACT_DURReq request message.
		 */
		typedef Oscl::Mt::Itc::CliResponse<
					Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api,
					Api,
					Oscl::Driver::ST::LSM9DS1::AG::Register::
					Req::Api::GetACT_DURPayload
					>		GetACT_DURResp;

	public:
		/**	This describes a client response message that corresponds
			to the SetACT_THSReq message described in the "reqapi.h"
			header file. This SetACT_THSResp response message actually
			contains a SetACT_THSReq request message.
		 */
		typedef Oscl::Mt::Itc::CliResponse<
					Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api,
					Api,
					Oscl::Driver::ST::LSM9DS1::AG::Register::
					Req::Api::SetACT_THSPayload
					>		SetACT_THSResp;

	public:
		/**	This describes a client response message that corresponds
			to the GetACT_THSReq message described in the "reqapi.h"
			header file. This GetACT_THSResp response message actually
			contains a GetACT_THSReq request message.
		 */
		typedef Oscl::Mt::Itc::CliResponse<
					Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api,
					Api,
					Oscl::Driver::ST::LSM9DS1::AG::Register::
					Req::Api::GetACT_THSPayload
					>		GetACT_THSResp;

	public:
		/** Make the compiler happy. */
		virtual ~Api(){}

		/** This operation is invoked within the client thread
			whenever a GetOUT_TEMPResp message is received.
		 */
		virtual void	response(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Resp::Api::GetOUT_TEMPResp& msg
									) noexcept=0;

		/** This operation is invoked within the client thread
			whenever a GetOUT_GResp message is received.
		 */
		virtual void	response(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Resp::Api::GetOUT_GResp& msg
									) noexcept=0;

		/** This operation is invoked within the client thread
			whenever a GetOUT_XLResp message is received.
		 */
		virtual void	response(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Resp::Api::GetOUT_XLResp& msg
									) noexcept=0;

		/** This operation is invoked within the client thread
			whenever a SetINT_GEN_DUR_GResp message is received.
		 */
		virtual void	response(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Resp::Api::SetINT_GEN_DUR_GResp& msg
									) noexcept=0;

		/** This operation is invoked within the client thread
			whenever a GetINT_GEN_DUR_GResp message is received.
		 */
		virtual void	response(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Resp::Api::GetINT_GEN_DUR_GResp& msg
									) noexcept=0;

		/** This operation is invoked within the client thread
			whenever a SetINT_GEN_THS_ZL_GResp message is received.
		 */
		virtual void	response(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Resp::Api::SetINT_GEN_THS_ZL_GResp& msg
									) noexcept=0;

		/** This operation is invoked within the client thread
			whenever a GetINT_GEN_THS_ZL_GResp message is received.
		 */
		virtual void	response(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Resp::Api::GetINT_GEN_THS_ZL_GResp& msg
									) noexcept=0;

		/** This operation is invoked within the client thread
			whenever a SetINT_GEN_THS_ZH_GResp message is received.
		 */
		virtual void	response(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Resp::Api::SetINT_GEN_THS_ZH_GResp& msg
									) noexcept=0;

		/** This operation is invoked within the client thread
			whenever a GetINT_GEN_THS_ZH_GResp message is received.
		 */
		virtual void	response(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Resp::Api::GetINT_GEN_THS_ZH_GResp& msg
									) noexcept=0;

		/** This operation is invoked within the client thread
			whenever a SetINT_GEN_THS_YL_GResp message is received.
		 */
		virtual void	response(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Resp::Api::SetINT_GEN_THS_YL_GResp& msg
									) noexcept=0;

		/** This operation is invoked within the client thread
			whenever a GetINT_GEN_THS_YL_GResp message is received.
		 */
		virtual void	response(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Resp::Api::GetINT_GEN_THS_YL_GResp& msg
									) noexcept=0;

		/** This operation is invoked within the client thread
			whenever a SetINT_GEN_THS_YH_GResp message is received.
		 */
		virtual void	response(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Resp::Api::SetINT_GEN_THS_YH_GResp& msg
									) noexcept=0;

		/** This operation is invoked within the client thread
			whenever a GetINT_GEN_THS_YH_GResp message is received.
		 */
		virtual void	response(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Resp::Api::GetINT_GEN_THS_YH_GResp& msg
									) noexcept=0;

		/** This operation is invoked within the client thread
			whenever a SetINT_GEN_THS_XL_GResp message is received.
		 */
		virtual void	response(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Resp::Api::SetINT_GEN_THS_XL_GResp& msg
									) noexcept=0;

		/** This operation is invoked within the client thread
			whenever a GetINT_GEN_THS_XL_GResp message is received.
		 */
		virtual void	response(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Resp::Api::GetINT_GEN_THS_XL_GResp& msg
									) noexcept=0;

		/** This operation is invoked within the client thread
			whenever a SetINT_GEN_THS_XH_GResp message is received.
		 */
		virtual void	response(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Resp::Api::SetINT_GEN_THS_XH_GResp& msg
									) noexcept=0;

		/** This operation is invoked within the client thread
			whenever a GetINT_GEN_THS_XH_GResp message is received.
		 */
		virtual void	response(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Resp::Api::GetINT_GEN_THS_XH_GResp& msg
									) noexcept=0;

		/** This operation is invoked within the client thread
			whenever a SetINT_GEN_CFG_GResp message is received.
		 */
		virtual void	response(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Resp::Api::SetINT_GEN_CFG_GResp& msg
									) noexcept=0;

		/** This operation is invoked within the client thread
			whenever a GetINT_GEN_CFG_GResp message is received.
		 */
		virtual void	response(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Resp::Api::GetINT_GEN_CFG_GResp& msg
									) noexcept=0;

		/** This operation is invoked within the client thread
			whenever a GetFIFO_SRCResp message is received.
		 */
		virtual void	response(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Resp::Api::GetFIFO_SRCResp& msg
									) noexcept=0;

		/** This operation is invoked within the client thread
			whenever a SetFIFO_CTRLResp message is received.
		 */
		virtual void	response(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Resp::Api::SetFIFO_CTRLResp& msg
									) noexcept=0;

		/** This operation is invoked within the client thread
			whenever a GetFIFO_CTRLResp message is received.
		 */
		virtual void	response(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Resp::Api::GetFIFO_CTRLResp& msg
									) noexcept=0;

		/** This operation is invoked within the client thread
			whenever a GetOUT_Z_H_XLResp message is received.
		 */
		virtual void	response(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Resp::Api::GetOUT_Z_H_XLResp& msg
									) noexcept=0;

		/** This operation is invoked within the client thread
			whenever a GetOUT_Z_L_XLResp message is received.
		 */
		virtual void	response(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Resp::Api::GetOUT_Z_L_XLResp& msg
									) noexcept=0;

		/** This operation is invoked within the client thread
			whenever a GetOUT_Y_H_XLResp message is received.
		 */
		virtual void	response(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Resp::Api::GetOUT_Y_H_XLResp& msg
									) noexcept=0;

		/** This operation is invoked within the client thread
			whenever a GetOUT_Y_L_XLResp message is received.
		 */
		virtual void	response(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Resp::Api::GetOUT_Y_L_XLResp& msg
									) noexcept=0;

		/** This operation is invoked within the client thread
			whenever a GetOUT_X_H_XLResp message is received.
		 */
		virtual void	response(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Resp::Api::GetOUT_X_H_XLResp& msg
									) noexcept=0;

		/** This operation is invoked within the client thread
			whenever a GetOUT_X_L_XLResp message is received.
		 */
		virtual void	response(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Resp::Api::GetOUT_X_L_XLResp& msg
									) noexcept=0;

		/** This operation is invoked within the client thread
			whenever a GetSTATUS_REG_2Resp message is received.
		 */
		virtual void	response(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Resp::Api::GetSTATUS_REG_2Resp& msg
									) noexcept=0;

		/** This operation is invoked within the client thread
			whenever a GetINT_GEN_SRC_XLResp message is received.
		 */
		virtual void	response(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Resp::Api::GetINT_GEN_SRC_XLResp& msg
									) noexcept=0;

		/** This operation is invoked within the client thread
			whenever a SetCTRL_REG10Resp message is received.
		 */
		virtual void	response(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Resp::Api::SetCTRL_REG10Resp& msg
									) noexcept=0;

		/** This operation is invoked within the client thread
			whenever a GetCTRL_REG10Resp message is received.
		 */
		virtual void	response(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Resp::Api::GetCTRL_REG10Resp& msg
									) noexcept=0;

		/** This operation is invoked within the client thread
			whenever a SetCTRL_REG9Resp message is received.
		 */
		virtual void	response(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Resp::Api::SetCTRL_REG9Resp& msg
									) noexcept=0;

		/** This operation is invoked within the client thread
			whenever a GetCTRL_REG9Resp message is received.
		 */
		virtual void	response(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Resp::Api::GetCTRL_REG9Resp& msg
									) noexcept=0;

		/** This operation is invoked within the client thread
			whenever a SetCTRL_REG8Resp message is received.
		 */
		virtual void	response(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Resp::Api::SetCTRL_REG8Resp& msg
									) noexcept=0;

		/** This operation is invoked within the client thread
			whenever a GetCTRL_REG8Resp message is received.
		 */
		virtual void	response(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Resp::Api::GetCTRL_REG8Resp& msg
									) noexcept=0;

		/** This operation is invoked within the client thread
			whenever a SetCTRL_REG7_XLResp message is received.
		 */
		virtual void	response(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Resp::Api::SetCTRL_REG7_XLResp& msg
									) noexcept=0;

		/** This operation is invoked within the client thread
			whenever a GetCTRL_REG7_XLResp message is received.
		 */
		virtual void	response(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Resp::Api::GetCTRL_REG7_XLResp& msg
									) noexcept=0;

		/** This operation is invoked within the client thread
			whenever a SetCTRL_REG6_XLResp message is received.
		 */
		virtual void	response(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Resp::Api::SetCTRL_REG6_XLResp& msg
									) noexcept=0;

		/** This operation is invoked within the client thread
			whenever a GetCTRL_REG6_XLResp message is received.
		 */
		virtual void	response(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Resp::Api::GetCTRL_REG6_XLResp& msg
									) noexcept=0;

		/** This operation is invoked within the client thread
			whenever a SetCTRL_REG5_XLResp message is received.
		 */
		virtual void	response(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Resp::Api::SetCTRL_REG5_XLResp& msg
									) noexcept=0;

		/** This operation is invoked within the client thread
			whenever a GetCTRL_REG5_XLResp message is received.
		 */
		virtual void	response(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Resp::Api::GetCTRL_REG5_XLResp& msg
									) noexcept=0;

		/** This operation is invoked within the client thread
			whenever a SetCTRL_REG4Resp message is received.
		 */
		virtual void	response(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Resp::Api::SetCTRL_REG4Resp& msg
									) noexcept=0;

		/** This operation is invoked within the client thread
			whenever a GetCTRL_REG4Resp message is received.
		 */
		virtual void	response(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Resp::Api::GetCTRL_REG4Resp& msg
									) noexcept=0;

		/** This operation is invoked within the client thread
			whenever a GetOUT_Z_H_GResp message is received.
		 */
		virtual void	response(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Resp::Api::GetOUT_Z_H_GResp& msg
									) noexcept=0;

		/** This operation is invoked within the client thread
			whenever a GetOUT_Z_L_GResp message is received.
		 */
		virtual void	response(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Resp::Api::GetOUT_Z_L_GResp& msg
									) noexcept=0;

		/** This operation is invoked within the client thread
			whenever a GetOUT_Y_H_GResp message is received.
		 */
		virtual void	response(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Resp::Api::GetOUT_Y_H_GResp& msg
									) noexcept=0;

		/** This operation is invoked within the client thread
			whenever a GetOUT_Y_L_GResp message is received.
		 */
		virtual void	response(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Resp::Api::GetOUT_Y_L_GResp& msg
									) noexcept=0;

		/** This operation is invoked within the client thread
			whenever a GetOUT_X_L_GResp message is received.
		 */
		virtual void	response(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Resp::Api::GetOUT_X_L_GResp& msg
									) noexcept=0;

		/** This operation is invoked within the client thread
			whenever a GetSTATUS_REGResp message is received.
		 */
		virtual void	response(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Resp::Api::GetSTATUS_REGResp& msg
									) noexcept=0;

		/** This operation is invoked within the client thread
			whenever a GetOUT_TEMP_HResp message is received.
		 */
		virtual void	response(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Resp::Api::GetOUT_TEMP_HResp& msg
									) noexcept=0;

		/** This operation is invoked within the client thread
			whenever a GetOUT_TEMP_LResp message is received.
		 */
		virtual void	response(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Resp::Api::GetOUT_TEMP_LResp& msg
									) noexcept=0;

		/** This operation is invoked within the client thread
			whenever a GetINT_GEN_SRC_GResp message is received.
		 */
		virtual void	response(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Resp::Api::GetINT_GEN_SRC_GResp& msg
									) noexcept=0;

		/** This operation is invoked within the client thread
			whenever a SetORIENT_CFG_GResp message is received.
		 */
		virtual void	response(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Resp::Api::SetORIENT_CFG_GResp& msg
									) noexcept=0;

		/** This operation is invoked within the client thread
			whenever a GetORIENT_CFG_GResp message is received.
		 */
		virtual void	response(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Resp::Api::GetORIENT_CFG_GResp& msg
									) noexcept=0;

		/** This operation is invoked within the client thread
			whenever a SetCTRL_REG3_GResp message is received.
		 */
		virtual void	response(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Resp::Api::SetCTRL_REG3_GResp& msg
									) noexcept=0;

		/** This operation is invoked within the client thread
			whenever a GetCTRL_REG3_GResp message is received.
		 */
		virtual void	response(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Resp::Api::GetCTRL_REG3_GResp& msg
									) noexcept=0;

		/** This operation is invoked within the client thread
			whenever a SetCTRL_REG2_GResp message is received.
		 */
		virtual void	response(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Resp::Api::SetCTRL_REG2_GResp& msg
									) noexcept=0;

		/** This operation is invoked within the client thread
			whenever a GetCTRL_REG2_GResp message is received.
		 */
		virtual void	response(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Resp::Api::GetCTRL_REG2_GResp& msg
									) noexcept=0;

		/** This operation is invoked within the client thread
			whenever a SetCTRL_REG1_GResp message is received.
		 */
		virtual void	response(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Resp::Api::SetCTRL_REG1_GResp& msg
									) noexcept=0;

		/** This operation is invoked within the client thread
			whenever a GetCTRL_REG1_GResp message is received.
		 */
		virtual void	response(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Resp::Api::GetCTRL_REG1_GResp& msg
									) noexcept=0;

		/** This operation is invoked within the client thread
			whenever a GetWHO_AM_IResp message is received.
		 */
		virtual void	response(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Resp::Api::GetWHO_AM_IResp& msg
									) noexcept=0;

		/** This operation is invoked within the client thread
			whenever a SetINT2_CTRLResp message is received.
		 */
		virtual void	response(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Resp::Api::SetINT2_CTRLResp& msg
									) noexcept=0;

		/** This operation is invoked within the client thread
			whenever a GetINT2_CTRLResp message is received.
		 */
		virtual void	response(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Resp::Api::GetINT2_CTRLResp& msg
									) noexcept=0;

		/** This operation is invoked within the client thread
			whenever a SetINT1_CTRLResp message is received.
		 */
		virtual void	response(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Resp::Api::SetINT1_CTRLResp& msg
									) noexcept=0;

		/** This operation is invoked within the client thread
			whenever a GetINT1_CTRLResp message is received.
		 */
		virtual void	response(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Resp::Api::GetINT1_CTRLResp& msg
									) noexcept=0;

		/** This operation is invoked within the client thread
			whenever a SetREFERENCE_GResp message is received.
		 */
		virtual void	response(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Resp::Api::SetREFERENCE_GResp& msg
									) noexcept=0;

		/** This operation is invoked within the client thread
			whenever a GetREFERENCE_GResp message is received.
		 */
		virtual void	response(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Resp::Api::GetREFERENCE_GResp& msg
									) noexcept=0;

		/** This operation is invoked within the client thread
			whenever a SetINT_GEN_DUR_XLResp message is received.
		 */
		virtual void	response(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Resp::Api::SetINT_GEN_DUR_XLResp& msg
									) noexcept=0;

		/** This operation is invoked within the client thread
			whenever a GetINT_GEN_DUR_XLResp message is received.
		 */
		virtual void	response(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Resp::Api::GetINT_GEN_DUR_XLResp& msg
									) noexcept=0;

		/** This operation is invoked within the client thread
			whenever a SetINT_GEN_THS_Z_XLResp message is received.
		 */
		virtual void	response(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Resp::Api::SetINT_GEN_THS_Z_XLResp& msg
									) noexcept=0;

		/** This operation is invoked within the client thread
			whenever a GetINT_GEN_THS_Z_XLResp message is received.
		 */
		virtual void	response(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Resp::Api::GetINT_GEN_THS_Z_XLResp& msg
									) noexcept=0;

		/** This operation is invoked within the client thread
			whenever a SetINT_GEN_THS_Y_XLResp message is received.
		 */
		virtual void	response(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Resp::Api::SetINT_GEN_THS_Y_XLResp& msg
									) noexcept=0;

		/** This operation is invoked within the client thread
			whenever a GetINT_GEN_THS_Y_XLResp message is received.
		 */
		virtual void	response(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Resp::Api::GetINT_GEN_THS_Y_XLResp& msg
									) noexcept=0;

		/** This operation is invoked within the client thread
			whenever a SetINT_GEN_THS_X_XLResp message is received.
		 */
		virtual void	response(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Resp::Api::SetINT_GEN_THS_X_XLResp& msg
									) noexcept=0;

		/** This operation is invoked within the client thread
			whenever a GetINT_GEN_THS_X_XLResp message is received.
		 */
		virtual void	response(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Resp::Api::GetINT_GEN_THS_X_XLResp& msg
									) noexcept=0;

		/** This operation is invoked within the client thread
			whenever a SetINT_GEN_CFG_XLResp message is received.
		 */
		virtual void	response(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Resp::Api::SetINT_GEN_CFG_XLResp& msg
									) noexcept=0;

		/** This operation is invoked within the client thread
			whenever a GetINT_GEN_CFG_XLResp message is received.
		 */
		virtual void	response(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Resp::Api::GetINT_GEN_CFG_XLResp& msg
									) noexcept=0;

		/** This operation is invoked within the client thread
			whenever a SetACT_DURResp message is received.
		 */
		virtual void	response(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Resp::Api::SetACT_DURResp& msg
									) noexcept=0;

		/** This operation is invoked within the client thread
			whenever a GetACT_DURResp message is received.
		 */
		virtual void	response(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Resp::Api::GetACT_DURResp& msg
									) noexcept=0;

		/** This operation is invoked within the client thread
			whenever a SetACT_THSResp message is received.
		 */
		virtual void	response(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Resp::Api::SetACT_THSResp& msg
									) noexcept=0;

		/** This operation is invoked within the client thread
			whenever a GetACT_THSResp message is received.
		 */
		virtual void	response(	Oscl::Driver::ST::LSM9DS1::AG::Register::
									Resp::Api::GetACT_THSResp& msg
									) noexcept=0;

	};

}
}
}
}
}
}
}
#endif
