/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_driver_st_lsm9ds1_ag_register_apih_
#define _oscl_driver_st_lsm9ds1_ag_register_apih_

#include <stdint.h>

/** */
namespace Oscl {

/** */
namespace Driver {

/** */
namespace ST {

/** */
namespace LSM9DS1 {

/** */
namespace AG {

/** */
namespace Register {

/**	This class defines the operations that can be performed
	procedurally by the client.
 */
class Api {
	public:
		/** Make the compiler happy.
		 */
		virtual ~Api(){};

		/**	
		 */
		virtual void	getOUT_TEMP(	
							uint8_t	maxBufferLength,
							void*	buffer
							) noexcept=0;

		/**	
		 */
		virtual void	getOUT_G(	
							uint8_t	maxBufferLength,
							void*	buffer
							) noexcept=0;

		/**	
		 */
		virtual void	getOUT_XL(	
							uint8_t	maxBufferLength,
							void*	buffer
							) noexcept=0;

		/**	
		 */
		virtual void	setINT_GEN_DUR_G(	
							uint8_t	value
							) noexcept=0;

		/**	
		 */
		virtual uint8_t	getINT_GEN_DUR_G(	
							) noexcept=0;

		/**	
		 */
		virtual void	setINT_GEN_THS_ZL_G(	
							uint8_t	value
							) noexcept=0;

		/**	
		 */
		virtual uint8_t	getINT_GEN_THS_ZL_G(	
							) noexcept=0;

		/**	
		 */
		virtual void	setINT_GEN_THS_ZH_G(	
							uint8_t	value
							) noexcept=0;

		/**	
		 */
		virtual uint8_t	getINT_GEN_THS_ZH_G(	
							) noexcept=0;

		/**	
		 */
		virtual void	setINT_GEN_THS_YL_G(	
							uint8_t	value
							) noexcept=0;

		/**	
		 */
		virtual uint8_t	getINT_GEN_THS_YL_G(	
							) noexcept=0;

		/**	
		 */
		virtual void	setINT_GEN_THS_YH_G(	
							uint8_t	value
							) noexcept=0;

		/**	
		 */
		virtual uint8_t	getINT_GEN_THS_YH_G(	
							) noexcept=0;

		/**	
		 */
		virtual void	setINT_GEN_THS_XL_G(	
							uint8_t	value
							) noexcept=0;

		/**	
		 */
		virtual uint8_t	getINT_GEN_THS_XL_G(	
							) noexcept=0;

		/**	
		 */
		virtual void	setINT_GEN_THS_XH_G(	
							uint8_t	value
							) noexcept=0;

		/**	
		 */
		virtual uint8_t	getINT_GEN_THS_XH_G(	
							) noexcept=0;

		/**	
		 */
		virtual void	setINT_GEN_CFG_G(	
							uint8_t	value
							) noexcept=0;

		/**	
		 */
		virtual uint8_t	getINT_GEN_CFG_G(	
							) noexcept=0;

		/**	
		 */
		virtual uint8_t	getFIFO_SRC(	
							) noexcept=0;

		/**	
		 */
		virtual void	setFIFO_CTRL(	
							uint8_t	value
							) noexcept=0;

		/**	
		 */
		virtual uint8_t	getFIFO_CTRL(	
							) noexcept=0;

		/**	
		 */
		virtual uint8_t	getOUT_Z_H_XL(	
							) noexcept=0;

		/**	
		 */
		virtual uint8_t	getOUT_Z_L_XL(	
							) noexcept=0;

		/**	
		 */
		virtual uint8_t	getOUT_Y_H_XL(	
							) noexcept=0;

		/**	
		 */
		virtual uint8_t	getOUT_Y_L_XL(	
							) noexcept=0;

		/**	
		 */
		virtual uint8_t	getOUT_X_H_XL(	
							) noexcept=0;

		/**	
		 */
		virtual uint8_t	getOUT_X_L_XL(	
							) noexcept=0;

		/**	
		 */
		virtual uint8_t	getSTATUS_REG_2(	
							) noexcept=0;

		/**	
		 */
		virtual uint8_t	getINT_GEN_SRC_XL(	
							) noexcept=0;

		/**	
		 */
		virtual void	setCTRL_REG10(	
							uint8_t	value
							) noexcept=0;

		/**	
		 */
		virtual uint8_t	getCTRL_REG10(	
							) noexcept=0;

		/**	
		 */
		virtual void	setCTRL_REG9(	
							uint8_t	value
							) noexcept=0;

		/**	
		 */
		virtual uint8_t	getCTRL_REG9(	
							) noexcept=0;

		/**	
		 */
		virtual void	setCTRL_REG8(	
							uint8_t	value
							) noexcept=0;

		/**	
		 */
		virtual uint8_t	getCTRL_REG8(	
							) noexcept=0;

		/**	
		 */
		virtual void	setCTRL_REG7_XL(	
							uint8_t	value
							) noexcept=0;

		/**	
		 */
		virtual uint8_t	getCTRL_REG7_XL(	
							) noexcept=0;

		/**	
		 */
		virtual void	setCTRL_REG6_XL(	
							uint8_t	value
							) noexcept=0;

		/**	
		 */
		virtual uint8_t	getCTRL_REG6_XL(	
							) noexcept=0;

		/**	
		 */
		virtual void	setCTRL_REG5_XL(	
							uint8_t	value
							) noexcept=0;

		/**	
		 */
		virtual uint8_t	getCTRL_REG5_XL(	
							) noexcept=0;

		/**	
		 */
		virtual void	setCTRL_REG4(	
							uint8_t	value
							) noexcept=0;

		/**	
		 */
		virtual uint8_t	getCTRL_REG4(	
							) noexcept=0;

		/**	
		 */
		virtual uint8_t	getOUT_Z_H_G(	
							) noexcept=0;

		/**	
		 */
		virtual uint8_t	getOUT_Z_L_G(	
							) noexcept=0;

		/**	
		 */
		virtual uint8_t	getOUT_Y_H_G(	
							) noexcept=0;

		/**	
		 */
		virtual uint8_t	getOUT_Y_L_G(	
							) noexcept=0;

		/**	
		 */
		virtual uint8_t	getOUT_X_L_G(	
							) noexcept=0;

		/**	
		 */
		virtual uint8_t	getSTATUS_REG(	
							) noexcept=0;

		/**	
		 */
		virtual uint8_t	getOUT_TEMP_H(	
							) noexcept=0;

		/**	
		 */
		virtual uint8_t	getOUT_TEMP_L(	
							) noexcept=0;

		/**	
		 */
		virtual uint8_t	getINT_GEN_SRC_G(	
							) noexcept=0;

		/**	
		 */
		virtual void	setORIENT_CFG_G(	
							uint8_t	value
							) noexcept=0;

		/**	
		 */
		virtual uint8_t	getORIENT_CFG_G(	
							) noexcept=0;

		/**	
		 */
		virtual void	setCTRL_REG3_G(	
							uint8_t	value
							) noexcept=0;

		/**	
		 */
		virtual uint8_t	getCTRL_REG3_G(	
							) noexcept=0;

		/**	
		 */
		virtual void	setCTRL_REG2_G(	
							uint8_t	value
							) noexcept=0;

		/**	
		 */
		virtual uint8_t	getCTRL_REG2_G(	
							) noexcept=0;

		/**	
		 */
		virtual void	setCTRL_REG1_G(	
							uint8_t	value
							) noexcept=0;

		/**	
		 */
		virtual uint8_t	getCTRL_REG1_G(	
							) noexcept=0;

		/**	
		 */
		virtual uint8_t	getWHO_AM_I(	
							) noexcept=0;

		/**	
		 */
		virtual void	setINT2_CTRL(	
							uint8_t	value
							) noexcept=0;

		/**	
		 */
		virtual uint8_t	getINT2_CTRL(	
							) noexcept=0;

		/**	
		 */
		virtual void	setINT1_CTRL(	
							uint8_t	value
							) noexcept=0;

		/**	
		 */
		virtual uint8_t	getINT1_CTRL(	
							) noexcept=0;

		/**	
		 */
		virtual void	setREFERENCE_G(	
							uint8_t	value
							) noexcept=0;

		/**	
		 */
		virtual uint8_t	getREFERENCE_G(	
							) noexcept=0;

		/**	
		 */
		virtual void	setINT_GEN_DUR_XL(	
							uint8_t	value
							) noexcept=0;

		/**	
		 */
		virtual uint8_t	getINT_GEN_DUR_XL(	
							) noexcept=0;

		/**	
		 */
		virtual void	setINT_GEN_THS_Z_XL(	
							uint8_t	value
							) noexcept=0;

		/**	
		 */
		virtual uint8_t	getINT_GEN_THS_Z_XL(	
							) noexcept=0;

		/**	
		 */
		virtual void	setINT_GEN_THS_Y_XL(	
							uint8_t	value
							) noexcept=0;

		/**	
		 */
		virtual uint8_t	getINT_GEN_THS_Y_XL(	
							) noexcept=0;

		/**	
		 */
		virtual void	setINT_GEN_THS_X_XL(	
							uint8_t	value
							) noexcept=0;

		/**	
		 */
		virtual uint8_t	getINT_GEN_THS_X_XL(	
							) noexcept=0;

		/**	
		 */
		virtual void	setINT_GEN_CFG_XL(	
							uint8_t	value
							) noexcept=0;

		/**	
		 */
		virtual uint8_t	getINT_GEN_CFG_XL(	
							) noexcept=0;

		/**	
		 */
		virtual void	setACT_DUR(	
							uint8_t	value
							) noexcept=0;

		/**	
		 */
		virtual uint8_t	getACT_DUR(	
							) noexcept=0;

		/**	
		 */
		virtual void	setACT_THS(	
							uint8_t	value
							) noexcept=0;

		/**	
		 */
		virtual uint8_t	getACT_THS(	
							) noexcept=0;

	};

}
}
}
}
}
}
#endif
