/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_driver_st_lsm9ds1_ag_register_itc_respmemh_
#define _oscl_driver_st_lsm9ds1_ag_register_itc_respmemh_

#include "respapi.h"
#include "oscl/memory/block.h"

/**	These record types in this file describe blocks of memory
	that are large enough and appropriately aligned such that
	placement new operations creating instances of their
	corresponding objects can safely be performed. These records
	are useful to most clients that use the server's ITC
	interface asynchronously.
 */

/** */
namespace Oscl {

/** */
namespace Driver {

/** */
namespace ST {

/** */
namespace LSM9DS1 {

/** */
namespace AG {

/** */
namespace Register {

/** */
namespace Resp {

/**	This record describes a block of memory that is
	large enough (and properly aligned) to hold a
	GetOUT_TEMPResp and its corresponding Req::Api::GetOUT_TEMPPayload.
	Clients which use the corresponding server asynchronously
	frequently need to reservere/manage memory for for these
	ITC interactions and instances of this record are useful
	for that purpose.
 */
struct GetOUT_TEMPMem {
	/**	This is memory enough for the response message.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Resp::Api::GetOUT_TEMPResp)>	resp;

	/**	This is memory enough for the message payload.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Req::Api::GetOUT_TEMPPayload)>	payload;

	/**	This operation creates a response message using this
		 block of memory.
	 */
	Oscl::Driver::ST::LSM9DS1::AG::Register::Resp::Api::GetOUT_TEMPResp&
		build(	Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SAP&	sap,
				Oscl::Driver::ST::LSM9DS1::AG::Register::Resp::Api&		respApi,
				Oscl::Mt::Itc::PostMsgApi&		clientPapi,
				uint8_t	maxBufferLength,
				void*	buffer
				) noexcept;

	};

/**	This record describes a block of memory that is
	large enough (and properly aligned) to hold a
	GetOUT_GResp and its corresponding Req::Api::GetOUT_GPayload.
	Clients which use the corresponding server asynchronously
	frequently need to reservere/manage memory for for these
	ITC interactions and instances of this record are useful
	for that purpose.
 */
struct GetOUT_GMem {
	/**	This is memory enough for the response message.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Resp::Api::GetOUT_GResp)>	resp;

	/**	This is memory enough for the message payload.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Req::Api::GetOUT_GPayload)>	payload;

	/**	This operation creates a response message using this
		 block of memory.
	 */
	Oscl::Driver::ST::LSM9DS1::AG::Register::Resp::Api::GetOUT_GResp&
		build(	Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SAP&	sap,
				Oscl::Driver::ST::LSM9DS1::AG::Register::Resp::Api&		respApi,
				Oscl::Mt::Itc::PostMsgApi&		clientPapi,
				uint8_t	maxBufferLength,
				void*	buffer
				) noexcept;

	};

/**	This record describes a block of memory that is
	large enough (and properly aligned) to hold a
	GetOUT_XLResp and its corresponding Req::Api::GetOUT_XLPayload.
	Clients which use the corresponding server asynchronously
	frequently need to reservere/manage memory for for these
	ITC interactions and instances of this record are useful
	for that purpose.
 */
struct GetOUT_XLMem {
	/**	This is memory enough for the response message.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Resp::Api::GetOUT_XLResp)>	resp;

	/**	This is memory enough for the message payload.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Req::Api::GetOUT_XLPayload)>	payload;

	/**	This operation creates a response message using this
		 block of memory.
	 */
	Oscl::Driver::ST::LSM9DS1::AG::Register::Resp::Api::GetOUT_XLResp&
		build(	Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SAP&	sap,
				Oscl::Driver::ST::LSM9DS1::AG::Register::Resp::Api&		respApi,
				Oscl::Mt::Itc::PostMsgApi&		clientPapi,
				uint8_t	maxBufferLength,
				void*	buffer
				) noexcept;

	};

/**	This record describes a block of memory that is
	large enough (and properly aligned) to hold a
	SetINT_GEN_DUR_GResp and its corresponding Req::Api::SetINT_GEN_DUR_GPayload.
	Clients which use the corresponding server asynchronously
	frequently need to reservere/manage memory for for these
	ITC interactions and instances of this record are useful
	for that purpose.
 */
struct SetINT_GEN_DUR_GMem {
	/**	This is memory enough for the response message.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Resp::Api::SetINT_GEN_DUR_GResp)>	resp;

	/**	This is memory enough for the message payload.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Req::Api::SetINT_GEN_DUR_GPayload)>	payload;

	/**	This operation creates a response message using this
		 block of memory.
	 */
	Oscl::Driver::ST::LSM9DS1::AG::Register::Resp::Api::SetINT_GEN_DUR_GResp&
		build(	Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SAP&	sap,
				Oscl::Driver::ST::LSM9DS1::AG::Register::Resp::Api&		respApi,
				Oscl::Mt::Itc::PostMsgApi&		clientPapi,
				uint8_t	value
				) noexcept;

	};

/**	This record describes a block of memory that is
	large enough (and properly aligned) to hold a
	GetINT_GEN_DUR_GResp and its corresponding Req::Api::GetINT_GEN_DUR_GPayload.
	Clients which use the corresponding server asynchronously
	frequently need to reservere/manage memory for for these
	ITC interactions and instances of this record are useful
	for that purpose.
 */
struct GetINT_GEN_DUR_GMem {
	/**	This is memory enough for the response message.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Resp::Api::GetINT_GEN_DUR_GResp)>	resp;

	/**	This is memory enough for the message payload.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Req::Api::GetINT_GEN_DUR_GPayload)>	payload;

	/**	This operation creates a response message using this
		 block of memory.
	 */
	Oscl::Driver::ST::LSM9DS1::AG::Register::Resp::Api::GetINT_GEN_DUR_GResp&
		build(	Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SAP&	sap,
				Oscl::Driver::ST::LSM9DS1::AG::Register::Resp::Api&		respApi,
				Oscl::Mt::Itc::PostMsgApi&		clientPapi
				) noexcept;

	};

/**	This record describes a block of memory that is
	large enough (and properly aligned) to hold a
	SetINT_GEN_THS_ZL_GResp and its corresponding Req::Api::SetINT_GEN_THS_ZL_GPayload.
	Clients which use the corresponding server asynchronously
	frequently need to reservere/manage memory for for these
	ITC interactions and instances of this record are useful
	for that purpose.
 */
struct SetINT_GEN_THS_ZL_GMem {
	/**	This is memory enough for the response message.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Resp::Api::SetINT_GEN_THS_ZL_GResp)>	resp;

	/**	This is memory enough for the message payload.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Req::Api::SetINT_GEN_THS_ZL_GPayload)>	payload;

	/**	This operation creates a response message using this
		 block of memory.
	 */
	Oscl::Driver::ST::LSM9DS1::AG::Register::Resp::Api::SetINT_GEN_THS_ZL_GResp&
		build(	Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SAP&	sap,
				Oscl::Driver::ST::LSM9DS1::AG::Register::Resp::Api&		respApi,
				Oscl::Mt::Itc::PostMsgApi&		clientPapi,
				uint8_t	value
				) noexcept;

	};

/**	This record describes a block of memory that is
	large enough (and properly aligned) to hold a
	GetINT_GEN_THS_ZL_GResp and its corresponding Req::Api::GetINT_GEN_THS_ZL_GPayload.
	Clients which use the corresponding server asynchronously
	frequently need to reservere/manage memory for for these
	ITC interactions and instances of this record are useful
	for that purpose.
 */
struct GetINT_GEN_THS_ZL_GMem {
	/**	This is memory enough for the response message.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Resp::Api::GetINT_GEN_THS_ZL_GResp)>	resp;

	/**	This is memory enough for the message payload.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Req::Api::GetINT_GEN_THS_ZL_GPayload)>	payload;

	/**	This operation creates a response message using this
		 block of memory.
	 */
	Oscl::Driver::ST::LSM9DS1::AG::Register::Resp::Api::GetINT_GEN_THS_ZL_GResp&
		build(	Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SAP&	sap,
				Oscl::Driver::ST::LSM9DS1::AG::Register::Resp::Api&		respApi,
				Oscl::Mt::Itc::PostMsgApi&		clientPapi
				) noexcept;

	};

/**	This record describes a block of memory that is
	large enough (and properly aligned) to hold a
	SetINT_GEN_THS_ZH_GResp and its corresponding Req::Api::SetINT_GEN_THS_ZH_GPayload.
	Clients which use the corresponding server asynchronously
	frequently need to reservere/manage memory for for these
	ITC interactions and instances of this record are useful
	for that purpose.
 */
struct SetINT_GEN_THS_ZH_GMem {
	/**	This is memory enough for the response message.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Resp::Api::SetINT_GEN_THS_ZH_GResp)>	resp;

	/**	This is memory enough for the message payload.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Req::Api::SetINT_GEN_THS_ZH_GPayload)>	payload;

	/**	This operation creates a response message using this
		 block of memory.
	 */
	Oscl::Driver::ST::LSM9DS1::AG::Register::Resp::Api::SetINT_GEN_THS_ZH_GResp&
		build(	Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SAP&	sap,
				Oscl::Driver::ST::LSM9DS1::AG::Register::Resp::Api&		respApi,
				Oscl::Mt::Itc::PostMsgApi&		clientPapi,
				uint8_t	value
				) noexcept;

	};

/**	This record describes a block of memory that is
	large enough (and properly aligned) to hold a
	GetINT_GEN_THS_ZH_GResp and its corresponding Req::Api::GetINT_GEN_THS_ZH_GPayload.
	Clients which use the corresponding server asynchronously
	frequently need to reservere/manage memory for for these
	ITC interactions and instances of this record are useful
	for that purpose.
 */
struct GetINT_GEN_THS_ZH_GMem {
	/**	This is memory enough for the response message.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Resp::Api::GetINT_GEN_THS_ZH_GResp)>	resp;

	/**	This is memory enough for the message payload.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Req::Api::GetINT_GEN_THS_ZH_GPayload)>	payload;

	/**	This operation creates a response message using this
		 block of memory.
	 */
	Oscl::Driver::ST::LSM9DS1::AG::Register::Resp::Api::GetINT_GEN_THS_ZH_GResp&
		build(	Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SAP&	sap,
				Oscl::Driver::ST::LSM9DS1::AG::Register::Resp::Api&		respApi,
				Oscl::Mt::Itc::PostMsgApi&		clientPapi
				) noexcept;

	};

/**	This record describes a block of memory that is
	large enough (and properly aligned) to hold a
	SetINT_GEN_THS_YL_GResp and its corresponding Req::Api::SetINT_GEN_THS_YL_GPayload.
	Clients which use the corresponding server asynchronously
	frequently need to reservere/manage memory for for these
	ITC interactions and instances of this record are useful
	for that purpose.
 */
struct SetINT_GEN_THS_YL_GMem {
	/**	This is memory enough for the response message.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Resp::Api::SetINT_GEN_THS_YL_GResp)>	resp;

	/**	This is memory enough for the message payload.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Req::Api::SetINT_GEN_THS_YL_GPayload)>	payload;

	/**	This operation creates a response message using this
		 block of memory.
	 */
	Oscl::Driver::ST::LSM9DS1::AG::Register::Resp::Api::SetINT_GEN_THS_YL_GResp&
		build(	Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SAP&	sap,
				Oscl::Driver::ST::LSM9DS1::AG::Register::Resp::Api&		respApi,
				Oscl::Mt::Itc::PostMsgApi&		clientPapi,
				uint8_t	value
				) noexcept;

	};

/**	This record describes a block of memory that is
	large enough (and properly aligned) to hold a
	GetINT_GEN_THS_YL_GResp and its corresponding Req::Api::GetINT_GEN_THS_YL_GPayload.
	Clients which use the corresponding server asynchronously
	frequently need to reservere/manage memory for for these
	ITC interactions and instances of this record are useful
	for that purpose.
 */
struct GetINT_GEN_THS_YL_GMem {
	/**	This is memory enough for the response message.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Resp::Api::GetINT_GEN_THS_YL_GResp)>	resp;

	/**	This is memory enough for the message payload.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Req::Api::GetINT_GEN_THS_YL_GPayload)>	payload;

	/**	This operation creates a response message using this
		 block of memory.
	 */
	Oscl::Driver::ST::LSM9DS1::AG::Register::Resp::Api::GetINT_GEN_THS_YL_GResp&
		build(	Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SAP&	sap,
				Oscl::Driver::ST::LSM9DS1::AG::Register::Resp::Api&		respApi,
				Oscl::Mt::Itc::PostMsgApi&		clientPapi
				) noexcept;

	};

/**	This record describes a block of memory that is
	large enough (and properly aligned) to hold a
	SetINT_GEN_THS_YH_GResp and its corresponding Req::Api::SetINT_GEN_THS_YH_GPayload.
	Clients which use the corresponding server asynchronously
	frequently need to reservere/manage memory for for these
	ITC interactions and instances of this record are useful
	for that purpose.
 */
struct SetINT_GEN_THS_YH_GMem {
	/**	This is memory enough for the response message.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Resp::Api::SetINT_GEN_THS_YH_GResp)>	resp;

	/**	This is memory enough for the message payload.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Req::Api::SetINT_GEN_THS_YH_GPayload)>	payload;

	/**	This operation creates a response message using this
		 block of memory.
	 */
	Oscl::Driver::ST::LSM9DS1::AG::Register::Resp::Api::SetINT_GEN_THS_YH_GResp&
		build(	Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SAP&	sap,
				Oscl::Driver::ST::LSM9DS1::AG::Register::Resp::Api&		respApi,
				Oscl::Mt::Itc::PostMsgApi&		clientPapi,
				uint8_t	value
				) noexcept;

	};

/**	This record describes a block of memory that is
	large enough (and properly aligned) to hold a
	GetINT_GEN_THS_YH_GResp and its corresponding Req::Api::GetINT_GEN_THS_YH_GPayload.
	Clients which use the corresponding server asynchronously
	frequently need to reservere/manage memory for for these
	ITC interactions and instances of this record are useful
	for that purpose.
 */
struct GetINT_GEN_THS_YH_GMem {
	/**	This is memory enough for the response message.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Resp::Api::GetINT_GEN_THS_YH_GResp)>	resp;

	/**	This is memory enough for the message payload.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Req::Api::GetINT_GEN_THS_YH_GPayload)>	payload;

	/**	This operation creates a response message using this
		 block of memory.
	 */
	Oscl::Driver::ST::LSM9DS1::AG::Register::Resp::Api::GetINT_GEN_THS_YH_GResp&
		build(	Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SAP&	sap,
				Oscl::Driver::ST::LSM9DS1::AG::Register::Resp::Api&		respApi,
				Oscl::Mt::Itc::PostMsgApi&		clientPapi
				) noexcept;

	};

/**	This record describes a block of memory that is
	large enough (and properly aligned) to hold a
	SetINT_GEN_THS_XL_GResp and its corresponding Req::Api::SetINT_GEN_THS_XL_GPayload.
	Clients which use the corresponding server asynchronously
	frequently need to reservere/manage memory for for these
	ITC interactions and instances of this record are useful
	for that purpose.
 */
struct SetINT_GEN_THS_XL_GMem {
	/**	This is memory enough for the response message.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Resp::Api::SetINT_GEN_THS_XL_GResp)>	resp;

	/**	This is memory enough for the message payload.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Req::Api::SetINT_GEN_THS_XL_GPayload)>	payload;

	/**	This operation creates a response message using this
		 block of memory.
	 */
	Oscl::Driver::ST::LSM9DS1::AG::Register::Resp::Api::SetINT_GEN_THS_XL_GResp&
		build(	Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SAP&	sap,
				Oscl::Driver::ST::LSM9DS1::AG::Register::Resp::Api&		respApi,
				Oscl::Mt::Itc::PostMsgApi&		clientPapi,
				uint8_t	value
				) noexcept;

	};

/**	This record describes a block of memory that is
	large enough (and properly aligned) to hold a
	GetINT_GEN_THS_XL_GResp and its corresponding Req::Api::GetINT_GEN_THS_XL_GPayload.
	Clients which use the corresponding server asynchronously
	frequently need to reservere/manage memory for for these
	ITC interactions and instances of this record are useful
	for that purpose.
 */
struct GetINT_GEN_THS_XL_GMem {
	/**	This is memory enough for the response message.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Resp::Api::GetINT_GEN_THS_XL_GResp)>	resp;

	/**	This is memory enough for the message payload.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Req::Api::GetINT_GEN_THS_XL_GPayload)>	payload;

	/**	This operation creates a response message using this
		 block of memory.
	 */
	Oscl::Driver::ST::LSM9DS1::AG::Register::Resp::Api::GetINT_GEN_THS_XL_GResp&
		build(	Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SAP&	sap,
				Oscl::Driver::ST::LSM9DS1::AG::Register::Resp::Api&		respApi,
				Oscl::Mt::Itc::PostMsgApi&		clientPapi
				) noexcept;

	};

/**	This record describes a block of memory that is
	large enough (and properly aligned) to hold a
	SetINT_GEN_THS_XH_GResp and its corresponding Req::Api::SetINT_GEN_THS_XH_GPayload.
	Clients which use the corresponding server asynchronously
	frequently need to reservere/manage memory for for these
	ITC interactions and instances of this record are useful
	for that purpose.
 */
struct SetINT_GEN_THS_XH_GMem {
	/**	This is memory enough for the response message.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Resp::Api::SetINT_GEN_THS_XH_GResp)>	resp;

	/**	This is memory enough for the message payload.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Req::Api::SetINT_GEN_THS_XH_GPayload)>	payload;

	/**	This operation creates a response message using this
		 block of memory.
	 */
	Oscl::Driver::ST::LSM9DS1::AG::Register::Resp::Api::SetINT_GEN_THS_XH_GResp&
		build(	Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SAP&	sap,
				Oscl::Driver::ST::LSM9DS1::AG::Register::Resp::Api&		respApi,
				Oscl::Mt::Itc::PostMsgApi&		clientPapi,
				uint8_t	value
				) noexcept;

	};

/**	This record describes a block of memory that is
	large enough (and properly aligned) to hold a
	GetINT_GEN_THS_XH_GResp and its corresponding Req::Api::GetINT_GEN_THS_XH_GPayload.
	Clients which use the corresponding server asynchronously
	frequently need to reservere/manage memory for for these
	ITC interactions and instances of this record are useful
	for that purpose.
 */
struct GetINT_GEN_THS_XH_GMem {
	/**	This is memory enough for the response message.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Resp::Api::GetINT_GEN_THS_XH_GResp)>	resp;

	/**	This is memory enough for the message payload.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Req::Api::GetINT_GEN_THS_XH_GPayload)>	payload;

	/**	This operation creates a response message using this
		 block of memory.
	 */
	Oscl::Driver::ST::LSM9DS1::AG::Register::Resp::Api::GetINT_GEN_THS_XH_GResp&
		build(	Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SAP&	sap,
				Oscl::Driver::ST::LSM9DS1::AG::Register::Resp::Api&		respApi,
				Oscl::Mt::Itc::PostMsgApi&		clientPapi
				) noexcept;

	};

/**	This record describes a block of memory that is
	large enough (and properly aligned) to hold a
	SetINT_GEN_CFG_GResp and its corresponding Req::Api::SetINT_GEN_CFG_GPayload.
	Clients which use the corresponding server asynchronously
	frequently need to reservere/manage memory for for these
	ITC interactions and instances of this record are useful
	for that purpose.
 */
struct SetINT_GEN_CFG_GMem {
	/**	This is memory enough for the response message.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Resp::Api::SetINT_GEN_CFG_GResp)>	resp;

	/**	This is memory enough for the message payload.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Req::Api::SetINT_GEN_CFG_GPayload)>	payload;

	/**	This operation creates a response message using this
		 block of memory.
	 */
	Oscl::Driver::ST::LSM9DS1::AG::Register::Resp::Api::SetINT_GEN_CFG_GResp&
		build(	Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SAP&	sap,
				Oscl::Driver::ST::LSM9DS1::AG::Register::Resp::Api&		respApi,
				Oscl::Mt::Itc::PostMsgApi&		clientPapi,
				uint8_t	value
				) noexcept;

	};

/**	This record describes a block of memory that is
	large enough (and properly aligned) to hold a
	GetINT_GEN_CFG_GResp and its corresponding Req::Api::GetINT_GEN_CFG_GPayload.
	Clients which use the corresponding server asynchronously
	frequently need to reservere/manage memory for for these
	ITC interactions and instances of this record are useful
	for that purpose.
 */
struct GetINT_GEN_CFG_GMem {
	/**	This is memory enough for the response message.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Resp::Api::GetINT_GEN_CFG_GResp)>	resp;

	/**	This is memory enough for the message payload.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Req::Api::GetINT_GEN_CFG_GPayload)>	payload;

	/**	This operation creates a response message using this
		 block of memory.
	 */
	Oscl::Driver::ST::LSM9DS1::AG::Register::Resp::Api::GetINT_GEN_CFG_GResp&
		build(	Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SAP&	sap,
				Oscl::Driver::ST::LSM9DS1::AG::Register::Resp::Api&		respApi,
				Oscl::Mt::Itc::PostMsgApi&		clientPapi
				) noexcept;

	};

/**	This record describes a block of memory that is
	large enough (and properly aligned) to hold a
	GetFIFO_SRCResp and its corresponding Req::Api::GetFIFO_SRCPayload.
	Clients which use the corresponding server asynchronously
	frequently need to reservere/manage memory for for these
	ITC interactions and instances of this record are useful
	for that purpose.
 */
struct GetFIFO_SRCMem {
	/**	This is memory enough for the response message.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Resp::Api::GetFIFO_SRCResp)>	resp;

	/**	This is memory enough for the message payload.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Req::Api::GetFIFO_SRCPayload)>	payload;

	/**	This operation creates a response message using this
		 block of memory.
	 */
	Oscl::Driver::ST::LSM9DS1::AG::Register::Resp::Api::GetFIFO_SRCResp&
		build(	Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SAP&	sap,
				Oscl::Driver::ST::LSM9DS1::AG::Register::Resp::Api&		respApi,
				Oscl::Mt::Itc::PostMsgApi&		clientPapi
				) noexcept;

	};

/**	This record describes a block of memory that is
	large enough (and properly aligned) to hold a
	SetFIFO_CTRLResp and its corresponding Req::Api::SetFIFO_CTRLPayload.
	Clients which use the corresponding server asynchronously
	frequently need to reservere/manage memory for for these
	ITC interactions and instances of this record are useful
	for that purpose.
 */
struct SetFIFO_CTRLMem {
	/**	This is memory enough for the response message.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Resp::Api::SetFIFO_CTRLResp)>	resp;

	/**	This is memory enough for the message payload.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Req::Api::SetFIFO_CTRLPayload)>	payload;

	/**	This operation creates a response message using this
		 block of memory.
	 */
	Oscl::Driver::ST::LSM9DS1::AG::Register::Resp::Api::SetFIFO_CTRLResp&
		build(	Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SAP&	sap,
				Oscl::Driver::ST::LSM9DS1::AG::Register::Resp::Api&		respApi,
				Oscl::Mt::Itc::PostMsgApi&		clientPapi,
				uint8_t	value
				) noexcept;

	};

/**	This record describes a block of memory that is
	large enough (and properly aligned) to hold a
	GetFIFO_CTRLResp and its corresponding Req::Api::GetFIFO_CTRLPayload.
	Clients which use the corresponding server asynchronously
	frequently need to reservere/manage memory for for these
	ITC interactions and instances of this record are useful
	for that purpose.
 */
struct GetFIFO_CTRLMem {
	/**	This is memory enough for the response message.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Resp::Api::GetFIFO_CTRLResp)>	resp;

	/**	This is memory enough for the message payload.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Req::Api::GetFIFO_CTRLPayload)>	payload;

	/**	This operation creates a response message using this
		 block of memory.
	 */
	Oscl::Driver::ST::LSM9DS1::AG::Register::Resp::Api::GetFIFO_CTRLResp&
		build(	Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SAP&	sap,
				Oscl::Driver::ST::LSM9DS1::AG::Register::Resp::Api&		respApi,
				Oscl::Mt::Itc::PostMsgApi&		clientPapi
				) noexcept;

	};

/**	This record describes a block of memory that is
	large enough (and properly aligned) to hold a
	GetOUT_Z_H_XLResp and its corresponding Req::Api::GetOUT_Z_H_XLPayload.
	Clients which use the corresponding server asynchronously
	frequently need to reservere/manage memory for for these
	ITC interactions and instances of this record are useful
	for that purpose.
 */
struct GetOUT_Z_H_XLMem {
	/**	This is memory enough for the response message.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Resp::Api::GetOUT_Z_H_XLResp)>	resp;

	/**	This is memory enough for the message payload.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Req::Api::GetOUT_Z_H_XLPayload)>	payload;

	/**	This operation creates a response message using this
		 block of memory.
	 */
	Oscl::Driver::ST::LSM9DS1::AG::Register::Resp::Api::GetOUT_Z_H_XLResp&
		build(	Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SAP&	sap,
				Oscl::Driver::ST::LSM9DS1::AG::Register::Resp::Api&		respApi,
				Oscl::Mt::Itc::PostMsgApi&		clientPapi
				) noexcept;

	};

/**	This record describes a block of memory that is
	large enough (and properly aligned) to hold a
	GetOUT_Z_L_XLResp and its corresponding Req::Api::GetOUT_Z_L_XLPayload.
	Clients which use the corresponding server asynchronously
	frequently need to reservere/manage memory for for these
	ITC interactions and instances of this record are useful
	for that purpose.
 */
struct GetOUT_Z_L_XLMem {
	/**	This is memory enough for the response message.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Resp::Api::GetOUT_Z_L_XLResp)>	resp;

	/**	This is memory enough for the message payload.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Req::Api::GetOUT_Z_L_XLPayload)>	payload;

	/**	This operation creates a response message using this
		 block of memory.
	 */
	Oscl::Driver::ST::LSM9DS1::AG::Register::Resp::Api::GetOUT_Z_L_XLResp&
		build(	Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SAP&	sap,
				Oscl::Driver::ST::LSM9DS1::AG::Register::Resp::Api&		respApi,
				Oscl::Mt::Itc::PostMsgApi&		clientPapi
				) noexcept;

	};

/**	This record describes a block of memory that is
	large enough (and properly aligned) to hold a
	GetOUT_Y_H_XLResp and its corresponding Req::Api::GetOUT_Y_H_XLPayload.
	Clients which use the corresponding server asynchronously
	frequently need to reservere/manage memory for for these
	ITC interactions and instances of this record are useful
	for that purpose.
 */
struct GetOUT_Y_H_XLMem {
	/**	This is memory enough for the response message.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Resp::Api::GetOUT_Y_H_XLResp)>	resp;

	/**	This is memory enough for the message payload.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Req::Api::GetOUT_Y_H_XLPayload)>	payload;

	/**	This operation creates a response message using this
		 block of memory.
	 */
	Oscl::Driver::ST::LSM9DS1::AG::Register::Resp::Api::GetOUT_Y_H_XLResp&
		build(	Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SAP&	sap,
				Oscl::Driver::ST::LSM9DS1::AG::Register::Resp::Api&		respApi,
				Oscl::Mt::Itc::PostMsgApi&		clientPapi
				) noexcept;

	};

/**	This record describes a block of memory that is
	large enough (and properly aligned) to hold a
	GetOUT_Y_L_XLResp and its corresponding Req::Api::GetOUT_Y_L_XLPayload.
	Clients which use the corresponding server asynchronously
	frequently need to reservere/manage memory for for these
	ITC interactions and instances of this record are useful
	for that purpose.
 */
struct GetOUT_Y_L_XLMem {
	/**	This is memory enough for the response message.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Resp::Api::GetOUT_Y_L_XLResp)>	resp;

	/**	This is memory enough for the message payload.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Req::Api::GetOUT_Y_L_XLPayload)>	payload;

	/**	This operation creates a response message using this
		 block of memory.
	 */
	Oscl::Driver::ST::LSM9DS1::AG::Register::Resp::Api::GetOUT_Y_L_XLResp&
		build(	Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SAP&	sap,
				Oscl::Driver::ST::LSM9DS1::AG::Register::Resp::Api&		respApi,
				Oscl::Mt::Itc::PostMsgApi&		clientPapi
				) noexcept;

	};

/**	This record describes a block of memory that is
	large enough (and properly aligned) to hold a
	GetOUT_X_H_XLResp and its corresponding Req::Api::GetOUT_X_H_XLPayload.
	Clients which use the corresponding server asynchronously
	frequently need to reservere/manage memory for for these
	ITC interactions and instances of this record are useful
	for that purpose.
 */
struct GetOUT_X_H_XLMem {
	/**	This is memory enough for the response message.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Resp::Api::GetOUT_X_H_XLResp)>	resp;

	/**	This is memory enough for the message payload.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Req::Api::GetOUT_X_H_XLPayload)>	payload;

	/**	This operation creates a response message using this
		 block of memory.
	 */
	Oscl::Driver::ST::LSM9DS1::AG::Register::Resp::Api::GetOUT_X_H_XLResp&
		build(	Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SAP&	sap,
				Oscl::Driver::ST::LSM9DS1::AG::Register::Resp::Api&		respApi,
				Oscl::Mt::Itc::PostMsgApi&		clientPapi
				) noexcept;

	};

/**	This record describes a block of memory that is
	large enough (and properly aligned) to hold a
	GetOUT_X_L_XLResp and its corresponding Req::Api::GetOUT_X_L_XLPayload.
	Clients which use the corresponding server asynchronously
	frequently need to reservere/manage memory for for these
	ITC interactions and instances of this record are useful
	for that purpose.
 */
struct GetOUT_X_L_XLMem {
	/**	This is memory enough for the response message.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Resp::Api::GetOUT_X_L_XLResp)>	resp;

	/**	This is memory enough for the message payload.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Req::Api::GetOUT_X_L_XLPayload)>	payload;

	/**	This operation creates a response message using this
		 block of memory.
	 */
	Oscl::Driver::ST::LSM9DS1::AG::Register::Resp::Api::GetOUT_X_L_XLResp&
		build(	Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SAP&	sap,
				Oscl::Driver::ST::LSM9DS1::AG::Register::Resp::Api&		respApi,
				Oscl::Mt::Itc::PostMsgApi&		clientPapi
				) noexcept;

	};

/**	This record describes a block of memory that is
	large enough (and properly aligned) to hold a
	GetSTATUS_REG_2Resp and its corresponding Req::Api::GetSTATUS_REG_2Payload.
	Clients which use the corresponding server asynchronously
	frequently need to reservere/manage memory for for these
	ITC interactions and instances of this record are useful
	for that purpose.
 */
struct GetSTATUS_REG_2Mem {
	/**	This is memory enough for the response message.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Resp::Api::GetSTATUS_REG_2Resp)>	resp;

	/**	This is memory enough for the message payload.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Req::Api::GetSTATUS_REG_2Payload)>	payload;

	/**	This operation creates a response message using this
		 block of memory.
	 */
	Oscl::Driver::ST::LSM9DS1::AG::Register::Resp::Api::GetSTATUS_REG_2Resp&
		build(	Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SAP&	sap,
				Oscl::Driver::ST::LSM9DS1::AG::Register::Resp::Api&		respApi,
				Oscl::Mt::Itc::PostMsgApi&		clientPapi
				) noexcept;

	};

/**	This record describes a block of memory that is
	large enough (and properly aligned) to hold a
	GetINT_GEN_SRC_XLResp and its corresponding Req::Api::GetINT_GEN_SRC_XLPayload.
	Clients which use the corresponding server asynchronously
	frequently need to reservere/manage memory for for these
	ITC interactions and instances of this record are useful
	for that purpose.
 */
struct GetINT_GEN_SRC_XLMem {
	/**	This is memory enough for the response message.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Resp::Api::GetINT_GEN_SRC_XLResp)>	resp;

	/**	This is memory enough for the message payload.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Req::Api::GetINT_GEN_SRC_XLPayload)>	payload;

	/**	This operation creates a response message using this
		 block of memory.
	 */
	Oscl::Driver::ST::LSM9DS1::AG::Register::Resp::Api::GetINT_GEN_SRC_XLResp&
		build(	Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SAP&	sap,
				Oscl::Driver::ST::LSM9DS1::AG::Register::Resp::Api&		respApi,
				Oscl::Mt::Itc::PostMsgApi&		clientPapi
				) noexcept;

	};

/**	This record describes a block of memory that is
	large enough (and properly aligned) to hold a
	SetCTRL_REG10Resp and its corresponding Req::Api::SetCTRL_REG10Payload.
	Clients which use the corresponding server asynchronously
	frequently need to reservere/manage memory for for these
	ITC interactions and instances of this record are useful
	for that purpose.
 */
struct SetCTRL_REG10Mem {
	/**	This is memory enough for the response message.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Resp::Api::SetCTRL_REG10Resp)>	resp;

	/**	This is memory enough for the message payload.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Req::Api::SetCTRL_REG10Payload)>	payload;

	/**	This operation creates a response message using this
		 block of memory.
	 */
	Oscl::Driver::ST::LSM9DS1::AG::Register::Resp::Api::SetCTRL_REG10Resp&
		build(	Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SAP&	sap,
				Oscl::Driver::ST::LSM9DS1::AG::Register::Resp::Api&		respApi,
				Oscl::Mt::Itc::PostMsgApi&		clientPapi,
				uint8_t	value
				) noexcept;

	};

/**	This record describes a block of memory that is
	large enough (and properly aligned) to hold a
	GetCTRL_REG10Resp and its corresponding Req::Api::GetCTRL_REG10Payload.
	Clients which use the corresponding server asynchronously
	frequently need to reservere/manage memory for for these
	ITC interactions and instances of this record are useful
	for that purpose.
 */
struct GetCTRL_REG10Mem {
	/**	This is memory enough for the response message.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Resp::Api::GetCTRL_REG10Resp)>	resp;

	/**	This is memory enough for the message payload.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Req::Api::GetCTRL_REG10Payload)>	payload;

	/**	This operation creates a response message using this
		 block of memory.
	 */
	Oscl::Driver::ST::LSM9DS1::AG::Register::Resp::Api::GetCTRL_REG10Resp&
		build(	Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SAP&	sap,
				Oscl::Driver::ST::LSM9DS1::AG::Register::Resp::Api&		respApi,
				Oscl::Mt::Itc::PostMsgApi&		clientPapi
				) noexcept;

	};

/**	This record describes a block of memory that is
	large enough (and properly aligned) to hold a
	SetCTRL_REG9Resp and its corresponding Req::Api::SetCTRL_REG9Payload.
	Clients which use the corresponding server asynchronously
	frequently need to reservere/manage memory for for these
	ITC interactions and instances of this record are useful
	for that purpose.
 */
struct SetCTRL_REG9Mem {
	/**	This is memory enough for the response message.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Resp::Api::SetCTRL_REG9Resp)>	resp;

	/**	This is memory enough for the message payload.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Req::Api::SetCTRL_REG9Payload)>	payload;

	/**	This operation creates a response message using this
		 block of memory.
	 */
	Oscl::Driver::ST::LSM9DS1::AG::Register::Resp::Api::SetCTRL_REG9Resp&
		build(	Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SAP&	sap,
				Oscl::Driver::ST::LSM9DS1::AG::Register::Resp::Api&		respApi,
				Oscl::Mt::Itc::PostMsgApi&		clientPapi,
				uint8_t	value
				) noexcept;

	};

/**	This record describes a block of memory that is
	large enough (and properly aligned) to hold a
	GetCTRL_REG9Resp and its corresponding Req::Api::GetCTRL_REG9Payload.
	Clients which use the corresponding server asynchronously
	frequently need to reservere/manage memory for for these
	ITC interactions and instances of this record are useful
	for that purpose.
 */
struct GetCTRL_REG9Mem {
	/**	This is memory enough for the response message.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Resp::Api::GetCTRL_REG9Resp)>	resp;

	/**	This is memory enough for the message payload.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Req::Api::GetCTRL_REG9Payload)>	payload;

	/**	This operation creates a response message using this
		 block of memory.
	 */
	Oscl::Driver::ST::LSM9DS1::AG::Register::Resp::Api::GetCTRL_REG9Resp&
		build(	Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SAP&	sap,
				Oscl::Driver::ST::LSM9DS1::AG::Register::Resp::Api&		respApi,
				Oscl::Mt::Itc::PostMsgApi&		clientPapi
				) noexcept;

	};

/**	This record describes a block of memory that is
	large enough (and properly aligned) to hold a
	SetCTRL_REG8Resp and its corresponding Req::Api::SetCTRL_REG8Payload.
	Clients which use the corresponding server asynchronously
	frequently need to reservere/manage memory for for these
	ITC interactions and instances of this record are useful
	for that purpose.
 */
struct SetCTRL_REG8Mem {
	/**	This is memory enough for the response message.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Resp::Api::SetCTRL_REG8Resp)>	resp;

	/**	This is memory enough for the message payload.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Req::Api::SetCTRL_REG8Payload)>	payload;

	/**	This operation creates a response message using this
		 block of memory.
	 */
	Oscl::Driver::ST::LSM9DS1::AG::Register::Resp::Api::SetCTRL_REG8Resp&
		build(	Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SAP&	sap,
				Oscl::Driver::ST::LSM9DS1::AG::Register::Resp::Api&		respApi,
				Oscl::Mt::Itc::PostMsgApi&		clientPapi,
				uint8_t	value
				) noexcept;

	};

/**	This record describes a block of memory that is
	large enough (and properly aligned) to hold a
	GetCTRL_REG8Resp and its corresponding Req::Api::GetCTRL_REG8Payload.
	Clients which use the corresponding server asynchronously
	frequently need to reservere/manage memory for for these
	ITC interactions and instances of this record are useful
	for that purpose.
 */
struct GetCTRL_REG8Mem {
	/**	This is memory enough for the response message.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Resp::Api::GetCTRL_REG8Resp)>	resp;

	/**	This is memory enough for the message payload.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Req::Api::GetCTRL_REG8Payload)>	payload;

	/**	This operation creates a response message using this
		 block of memory.
	 */
	Oscl::Driver::ST::LSM9DS1::AG::Register::Resp::Api::GetCTRL_REG8Resp&
		build(	Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SAP&	sap,
				Oscl::Driver::ST::LSM9DS1::AG::Register::Resp::Api&		respApi,
				Oscl::Mt::Itc::PostMsgApi&		clientPapi
				) noexcept;

	};

/**	This record describes a block of memory that is
	large enough (and properly aligned) to hold a
	SetCTRL_REG7_XLResp and its corresponding Req::Api::SetCTRL_REG7_XLPayload.
	Clients which use the corresponding server asynchronously
	frequently need to reservere/manage memory for for these
	ITC interactions and instances of this record are useful
	for that purpose.
 */
struct SetCTRL_REG7_XLMem {
	/**	This is memory enough for the response message.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Resp::Api::SetCTRL_REG7_XLResp)>	resp;

	/**	This is memory enough for the message payload.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Req::Api::SetCTRL_REG7_XLPayload)>	payload;

	/**	This operation creates a response message using this
		 block of memory.
	 */
	Oscl::Driver::ST::LSM9DS1::AG::Register::Resp::Api::SetCTRL_REG7_XLResp&
		build(	Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SAP&	sap,
				Oscl::Driver::ST::LSM9DS1::AG::Register::Resp::Api&		respApi,
				Oscl::Mt::Itc::PostMsgApi&		clientPapi,
				uint8_t	value
				) noexcept;

	};

/**	This record describes a block of memory that is
	large enough (and properly aligned) to hold a
	GetCTRL_REG7_XLResp and its corresponding Req::Api::GetCTRL_REG7_XLPayload.
	Clients which use the corresponding server asynchronously
	frequently need to reservere/manage memory for for these
	ITC interactions and instances of this record are useful
	for that purpose.
 */
struct GetCTRL_REG7_XLMem {
	/**	This is memory enough for the response message.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Resp::Api::GetCTRL_REG7_XLResp)>	resp;

	/**	This is memory enough for the message payload.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Req::Api::GetCTRL_REG7_XLPayload)>	payload;

	/**	This operation creates a response message using this
		 block of memory.
	 */
	Oscl::Driver::ST::LSM9DS1::AG::Register::Resp::Api::GetCTRL_REG7_XLResp&
		build(	Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SAP&	sap,
				Oscl::Driver::ST::LSM9DS1::AG::Register::Resp::Api&		respApi,
				Oscl::Mt::Itc::PostMsgApi&		clientPapi
				) noexcept;

	};

/**	This record describes a block of memory that is
	large enough (and properly aligned) to hold a
	SetCTRL_REG6_XLResp and its corresponding Req::Api::SetCTRL_REG6_XLPayload.
	Clients which use the corresponding server asynchronously
	frequently need to reservere/manage memory for for these
	ITC interactions and instances of this record are useful
	for that purpose.
 */
struct SetCTRL_REG6_XLMem {
	/**	This is memory enough for the response message.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Resp::Api::SetCTRL_REG6_XLResp)>	resp;

	/**	This is memory enough for the message payload.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Req::Api::SetCTRL_REG6_XLPayload)>	payload;

	/**	This operation creates a response message using this
		 block of memory.
	 */
	Oscl::Driver::ST::LSM9DS1::AG::Register::Resp::Api::SetCTRL_REG6_XLResp&
		build(	Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SAP&	sap,
				Oscl::Driver::ST::LSM9DS1::AG::Register::Resp::Api&		respApi,
				Oscl::Mt::Itc::PostMsgApi&		clientPapi,
				uint8_t	value
				) noexcept;

	};

/**	This record describes a block of memory that is
	large enough (and properly aligned) to hold a
	GetCTRL_REG6_XLResp and its corresponding Req::Api::GetCTRL_REG6_XLPayload.
	Clients which use the corresponding server asynchronously
	frequently need to reservere/manage memory for for these
	ITC interactions and instances of this record are useful
	for that purpose.
 */
struct GetCTRL_REG6_XLMem {
	/**	This is memory enough for the response message.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Resp::Api::GetCTRL_REG6_XLResp)>	resp;

	/**	This is memory enough for the message payload.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Req::Api::GetCTRL_REG6_XLPayload)>	payload;

	/**	This operation creates a response message using this
		 block of memory.
	 */
	Oscl::Driver::ST::LSM9DS1::AG::Register::Resp::Api::GetCTRL_REG6_XLResp&
		build(	Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SAP&	sap,
				Oscl::Driver::ST::LSM9DS1::AG::Register::Resp::Api&		respApi,
				Oscl::Mt::Itc::PostMsgApi&		clientPapi
				) noexcept;

	};

/**	This record describes a block of memory that is
	large enough (and properly aligned) to hold a
	SetCTRL_REG5_XLResp and its corresponding Req::Api::SetCTRL_REG5_XLPayload.
	Clients which use the corresponding server asynchronously
	frequently need to reservere/manage memory for for these
	ITC interactions and instances of this record are useful
	for that purpose.
 */
struct SetCTRL_REG5_XLMem {
	/**	This is memory enough for the response message.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Resp::Api::SetCTRL_REG5_XLResp)>	resp;

	/**	This is memory enough for the message payload.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Req::Api::SetCTRL_REG5_XLPayload)>	payload;

	/**	This operation creates a response message using this
		 block of memory.
	 */
	Oscl::Driver::ST::LSM9DS1::AG::Register::Resp::Api::SetCTRL_REG5_XLResp&
		build(	Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SAP&	sap,
				Oscl::Driver::ST::LSM9DS1::AG::Register::Resp::Api&		respApi,
				Oscl::Mt::Itc::PostMsgApi&		clientPapi,
				uint8_t	value
				) noexcept;

	};

/**	This record describes a block of memory that is
	large enough (and properly aligned) to hold a
	GetCTRL_REG5_XLResp and its corresponding Req::Api::GetCTRL_REG5_XLPayload.
	Clients which use the corresponding server asynchronously
	frequently need to reservere/manage memory for for these
	ITC interactions and instances of this record are useful
	for that purpose.
 */
struct GetCTRL_REG5_XLMem {
	/**	This is memory enough for the response message.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Resp::Api::GetCTRL_REG5_XLResp)>	resp;

	/**	This is memory enough for the message payload.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Req::Api::GetCTRL_REG5_XLPayload)>	payload;

	/**	This operation creates a response message using this
		 block of memory.
	 */
	Oscl::Driver::ST::LSM9DS1::AG::Register::Resp::Api::GetCTRL_REG5_XLResp&
		build(	Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SAP&	sap,
				Oscl::Driver::ST::LSM9DS1::AG::Register::Resp::Api&		respApi,
				Oscl::Mt::Itc::PostMsgApi&		clientPapi
				) noexcept;

	};

/**	This record describes a block of memory that is
	large enough (and properly aligned) to hold a
	SetCTRL_REG4Resp and its corresponding Req::Api::SetCTRL_REG4Payload.
	Clients which use the corresponding server asynchronously
	frequently need to reservere/manage memory for for these
	ITC interactions and instances of this record are useful
	for that purpose.
 */
struct SetCTRL_REG4Mem {
	/**	This is memory enough for the response message.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Resp::Api::SetCTRL_REG4Resp)>	resp;

	/**	This is memory enough for the message payload.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Req::Api::SetCTRL_REG4Payload)>	payload;

	/**	This operation creates a response message using this
		 block of memory.
	 */
	Oscl::Driver::ST::LSM9DS1::AG::Register::Resp::Api::SetCTRL_REG4Resp&
		build(	Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SAP&	sap,
				Oscl::Driver::ST::LSM9DS1::AG::Register::Resp::Api&		respApi,
				Oscl::Mt::Itc::PostMsgApi&		clientPapi,
				uint8_t	value
				) noexcept;

	};

/**	This record describes a block of memory that is
	large enough (and properly aligned) to hold a
	GetCTRL_REG4Resp and its corresponding Req::Api::GetCTRL_REG4Payload.
	Clients which use the corresponding server asynchronously
	frequently need to reservere/manage memory for for these
	ITC interactions and instances of this record are useful
	for that purpose.
 */
struct GetCTRL_REG4Mem {
	/**	This is memory enough for the response message.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Resp::Api::GetCTRL_REG4Resp)>	resp;

	/**	This is memory enough for the message payload.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Req::Api::GetCTRL_REG4Payload)>	payload;

	/**	This operation creates a response message using this
		 block of memory.
	 */
	Oscl::Driver::ST::LSM9DS1::AG::Register::Resp::Api::GetCTRL_REG4Resp&
		build(	Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SAP&	sap,
				Oscl::Driver::ST::LSM9DS1::AG::Register::Resp::Api&		respApi,
				Oscl::Mt::Itc::PostMsgApi&		clientPapi
				) noexcept;

	};

/**	This record describes a block of memory that is
	large enough (and properly aligned) to hold a
	GetOUT_Z_H_GResp and its corresponding Req::Api::GetOUT_Z_H_GPayload.
	Clients which use the corresponding server asynchronously
	frequently need to reservere/manage memory for for these
	ITC interactions and instances of this record are useful
	for that purpose.
 */
struct GetOUT_Z_H_GMem {
	/**	This is memory enough for the response message.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Resp::Api::GetOUT_Z_H_GResp)>	resp;

	/**	This is memory enough for the message payload.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Req::Api::GetOUT_Z_H_GPayload)>	payload;

	/**	This operation creates a response message using this
		 block of memory.
	 */
	Oscl::Driver::ST::LSM9DS1::AG::Register::Resp::Api::GetOUT_Z_H_GResp&
		build(	Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SAP&	sap,
				Oscl::Driver::ST::LSM9DS1::AG::Register::Resp::Api&		respApi,
				Oscl::Mt::Itc::PostMsgApi&		clientPapi
				) noexcept;

	};

/**	This record describes a block of memory that is
	large enough (and properly aligned) to hold a
	GetOUT_Z_L_GResp and its corresponding Req::Api::GetOUT_Z_L_GPayload.
	Clients which use the corresponding server asynchronously
	frequently need to reservere/manage memory for for these
	ITC interactions and instances of this record are useful
	for that purpose.
 */
struct GetOUT_Z_L_GMem {
	/**	This is memory enough for the response message.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Resp::Api::GetOUT_Z_L_GResp)>	resp;

	/**	This is memory enough for the message payload.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Req::Api::GetOUT_Z_L_GPayload)>	payload;

	/**	This operation creates a response message using this
		 block of memory.
	 */
	Oscl::Driver::ST::LSM9DS1::AG::Register::Resp::Api::GetOUT_Z_L_GResp&
		build(	Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SAP&	sap,
				Oscl::Driver::ST::LSM9DS1::AG::Register::Resp::Api&		respApi,
				Oscl::Mt::Itc::PostMsgApi&		clientPapi
				) noexcept;

	};

/**	This record describes a block of memory that is
	large enough (and properly aligned) to hold a
	GetOUT_Y_H_GResp and its corresponding Req::Api::GetOUT_Y_H_GPayload.
	Clients which use the corresponding server asynchronously
	frequently need to reservere/manage memory for for these
	ITC interactions and instances of this record are useful
	for that purpose.
 */
struct GetOUT_Y_H_GMem {
	/**	This is memory enough for the response message.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Resp::Api::GetOUT_Y_H_GResp)>	resp;

	/**	This is memory enough for the message payload.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Req::Api::GetOUT_Y_H_GPayload)>	payload;

	/**	This operation creates a response message using this
		 block of memory.
	 */
	Oscl::Driver::ST::LSM9DS1::AG::Register::Resp::Api::GetOUT_Y_H_GResp&
		build(	Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SAP&	sap,
				Oscl::Driver::ST::LSM9DS1::AG::Register::Resp::Api&		respApi,
				Oscl::Mt::Itc::PostMsgApi&		clientPapi
				) noexcept;

	};

/**	This record describes a block of memory that is
	large enough (and properly aligned) to hold a
	GetOUT_Y_L_GResp and its corresponding Req::Api::GetOUT_Y_L_GPayload.
	Clients which use the corresponding server asynchronously
	frequently need to reservere/manage memory for for these
	ITC interactions and instances of this record are useful
	for that purpose.
 */
struct GetOUT_Y_L_GMem {
	/**	This is memory enough for the response message.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Resp::Api::GetOUT_Y_L_GResp)>	resp;

	/**	This is memory enough for the message payload.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Req::Api::GetOUT_Y_L_GPayload)>	payload;

	/**	This operation creates a response message using this
		 block of memory.
	 */
	Oscl::Driver::ST::LSM9DS1::AG::Register::Resp::Api::GetOUT_Y_L_GResp&
		build(	Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SAP&	sap,
				Oscl::Driver::ST::LSM9DS1::AG::Register::Resp::Api&		respApi,
				Oscl::Mt::Itc::PostMsgApi&		clientPapi
				) noexcept;

	};

/**	This record describes a block of memory that is
	large enough (and properly aligned) to hold a
	GetOUT_X_L_GResp and its corresponding Req::Api::GetOUT_X_L_GPayload.
	Clients which use the corresponding server asynchronously
	frequently need to reservere/manage memory for for these
	ITC interactions and instances of this record are useful
	for that purpose.
 */
struct GetOUT_X_L_GMem {
	/**	This is memory enough for the response message.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Resp::Api::GetOUT_X_L_GResp)>	resp;

	/**	This is memory enough for the message payload.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Req::Api::GetOUT_X_L_GPayload)>	payload;

	/**	This operation creates a response message using this
		 block of memory.
	 */
	Oscl::Driver::ST::LSM9DS1::AG::Register::Resp::Api::GetOUT_X_L_GResp&
		build(	Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SAP&	sap,
				Oscl::Driver::ST::LSM9DS1::AG::Register::Resp::Api&		respApi,
				Oscl::Mt::Itc::PostMsgApi&		clientPapi
				) noexcept;

	};

/**	This record describes a block of memory that is
	large enough (and properly aligned) to hold a
	GetSTATUS_REGResp and its corresponding Req::Api::GetSTATUS_REGPayload.
	Clients which use the corresponding server asynchronously
	frequently need to reservere/manage memory for for these
	ITC interactions and instances of this record are useful
	for that purpose.
 */
struct GetSTATUS_REGMem {
	/**	This is memory enough for the response message.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Resp::Api::GetSTATUS_REGResp)>	resp;

	/**	This is memory enough for the message payload.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Req::Api::GetSTATUS_REGPayload)>	payload;

	/**	This operation creates a response message using this
		 block of memory.
	 */
	Oscl::Driver::ST::LSM9DS1::AG::Register::Resp::Api::GetSTATUS_REGResp&
		build(	Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SAP&	sap,
				Oscl::Driver::ST::LSM9DS1::AG::Register::Resp::Api&		respApi,
				Oscl::Mt::Itc::PostMsgApi&		clientPapi
				) noexcept;

	};

/**	This record describes a block of memory that is
	large enough (and properly aligned) to hold a
	GetOUT_TEMP_HResp and its corresponding Req::Api::GetOUT_TEMP_HPayload.
	Clients which use the corresponding server asynchronously
	frequently need to reservere/manage memory for for these
	ITC interactions and instances of this record are useful
	for that purpose.
 */
struct GetOUT_TEMP_HMem {
	/**	This is memory enough for the response message.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Resp::Api::GetOUT_TEMP_HResp)>	resp;

	/**	This is memory enough for the message payload.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Req::Api::GetOUT_TEMP_HPayload)>	payload;

	/**	This operation creates a response message using this
		 block of memory.
	 */
	Oscl::Driver::ST::LSM9DS1::AG::Register::Resp::Api::GetOUT_TEMP_HResp&
		build(	Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SAP&	sap,
				Oscl::Driver::ST::LSM9DS1::AG::Register::Resp::Api&		respApi,
				Oscl::Mt::Itc::PostMsgApi&		clientPapi
				) noexcept;

	};

/**	This record describes a block of memory that is
	large enough (and properly aligned) to hold a
	GetOUT_TEMP_LResp and its corresponding Req::Api::GetOUT_TEMP_LPayload.
	Clients which use the corresponding server asynchronously
	frequently need to reservere/manage memory for for these
	ITC interactions and instances of this record are useful
	for that purpose.
 */
struct GetOUT_TEMP_LMem {
	/**	This is memory enough for the response message.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Resp::Api::GetOUT_TEMP_LResp)>	resp;

	/**	This is memory enough for the message payload.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Req::Api::GetOUT_TEMP_LPayload)>	payload;

	/**	This operation creates a response message using this
		 block of memory.
	 */
	Oscl::Driver::ST::LSM9DS1::AG::Register::Resp::Api::GetOUT_TEMP_LResp&
		build(	Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SAP&	sap,
				Oscl::Driver::ST::LSM9DS1::AG::Register::Resp::Api&		respApi,
				Oscl::Mt::Itc::PostMsgApi&		clientPapi
				) noexcept;

	};

/**	This record describes a block of memory that is
	large enough (and properly aligned) to hold a
	GetINT_GEN_SRC_GResp and its corresponding Req::Api::GetINT_GEN_SRC_GPayload.
	Clients which use the corresponding server asynchronously
	frequently need to reservere/manage memory for for these
	ITC interactions and instances of this record are useful
	for that purpose.
 */
struct GetINT_GEN_SRC_GMem {
	/**	This is memory enough for the response message.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Resp::Api::GetINT_GEN_SRC_GResp)>	resp;

	/**	This is memory enough for the message payload.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Req::Api::GetINT_GEN_SRC_GPayload)>	payload;

	/**	This operation creates a response message using this
		 block of memory.
	 */
	Oscl::Driver::ST::LSM9DS1::AG::Register::Resp::Api::GetINT_GEN_SRC_GResp&
		build(	Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SAP&	sap,
				Oscl::Driver::ST::LSM9DS1::AG::Register::Resp::Api&		respApi,
				Oscl::Mt::Itc::PostMsgApi&		clientPapi
				) noexcept;

	};

/**	This record describes a block of memory that is
	large enough (and properly aligned) to hold a
	SetORIENT_CFG_GResp and its corresponding Req::Api::SetORIENT_CFG_GPayload.
	Clients which use the corresponding server asynchronously
	frequently need to reservere/manage memory for for these
	ITC interactions and instances of this record are useful
	for that purpose.
 */
struct SetORIENT_CFG_GMem {
	/**	This is memory enough for the response message.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Resp::Api::SetORIENT_CFG_GResp)>	resp;

	/**	This is memory enough for the message payload.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Req::Api::SetORIENT_CFG_GPayload)>	payload;

	/**	This operation creates a response message using this
		 block of memory.
	 */
	Oscl::Driver::ST::LSM9DS1::AG::Register::Resp::Api::SetORIENT_CFG_GResp&
		build(	Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SAP&	sap,
				Oscl::Driver::ST::LSM9DS1::AG::Register::Resp::Api&		respApi,
				Oscl::Mt::Itc::PostMsgApi&		clientPapi,
				uint8_t	value
				) noexcept;

	};

/**	This record describes a block of memory that is
	large enough (and properly aligned) to hold a
	GetORIENT_CFG_GResp and its corresponding Req::Api::GetORIENT_CFG_GPayload.
	Clients which use the corresponding server asynchronously
	frequently need to reservere/manage memory for for these
	ITC interactions and instances of this record are useful
	for that purpose.
 */
struct GetORIENT_CFG_GMem {
	/**	This is memory enough for the response message.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Resp::Api::GetORIENT_CFG_GResp)>	resp;

	/**	This is memory enough for the message payload.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Req::Api::GetORIENT_CFG_GPayload)>	payload;

	/**	This operation creates a response message using this
		 block of memory.
	 */
	Oscl::Driver::ST::LSM9DS1::AG::Register::Resp::Api::GetORIENT_CFG_GResp&
		build(	Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SAP&	sap,
				Oscl::Driver::ST::LSM9DS1::AG::Register::Resp::Api&		respApi,
				Oscl::Mt::Itc::PostMsgApi&		clientPapi
				) noexcept;

	};

/**	This record describes a block of memory that is
	large enough (and properly aligned) to hold a
	SetCTRL_REG3_GResp and its corresponding Req::Api::SetCTRL_REG3_GPayload.
	Clients which use the corresponding server asynchronously
	frequently need to reservere/manage memory for for these
	ITC interactions and instances of this record are useful
	for that purpose.
 */
struct SetCTRL_REG3_GMem {
	/**	This is memory enough for the response message.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Resp::Api::SetCTRL_REG3_GResp)>	resp;

	/**	This is memory enough for the message payload.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Req::Api::SetCTRL_REG3_GPayload)>	payload;

	/**	This operation creates a response message using this
		 block of memory.
	 */
	Oscl::Driver::ST::LSM9DS1::AG::Register::Resp::Api::SetCTRL_REG3_GResp&
		build(	Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SAP&	sap,
				Oscl::Driver::ST::LSM9DS1::AG::Register::Resp::Api&		respApi,
				Oscl::Mt::Itc::PostMsgApi&		clientPapi,
				uint8_t	value
				) noexcept;

	};

/**	This record describes a block of memory that is
	large enough (and properly aligned) to hold a
	GetCTRL_REG3_GResp and its corresponding Req::Api::GetCTRL_REG3_GPayload.
	Clients which use the corresponding server asynchronously
	frequently need to reservere/manage memory for for these
	ITC interactions and instances of this record are useful
	for that purpose.
 */
struct GetCTRL_REG3_GMem {
	/**	This is memory enough for the response message.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Resp::Api::GetCTRL_REG3_GResp)>	resp;

	/**	This is memory enough for the message payload.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Req::Api::GetCTRL_REG3_GPayload)>	payload;

	/**	This operation creates a response message using this
		 block of memory.
	 */
	Oscl::Driver::ST::LSM9DS1::AG::Register::Resp::Api::GetCTRL_REG3_GResp&
		build(	Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SAP&	sap,
				Oscl::Driver::ST::LSM9DS1::AG::Register::Resp::Api&		respApi,
				Oscl::Mt::Itc::PostMsgApi&		clientPapi
				) noexcept;

	};

/**	This record describes a block of memory that is
	large enough (and properly aligned) to hold a
	SetCTRL_REG2_GResp and its corresponding Req::Api::SetCTRL_REG2_GPayload.
	Clients which use the corresponding server asynchronously
	frequently need to reservere/manage memory for for these
	ITC interactions and instances of this record are useful
	for that purpose.
 */
struct SetCTRL_REG2_GMem {
	/**	This is memory enough for the response message.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Resp::Api::SetCTRL_REG2_GResp)>	resp;

	/**	This is memory enough for the message payload.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Req::Api::SetCTRL_REG2_GPayload)>	payload;

	/**	This operation creates a response message using this
		 block of memory.
	 */
	Oscl::Driver::ST::LSM9DS1::AG::Register::Resp::Api::SetCTRL_REG2_GResp&
		build(	Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SAP&	sap,
				Oscl::Driver::ST::LSM9DS1::AG::Register::Resp::Api&		respApi,
				Oscl::Mt::Itc::PostMsgApi&		clientPapi,
				uint8_t	value
				) noexcept;

	};

/**	This record describes a block of memory that is
	large enough (and properly aligned) to hold a
	GetCTRL_REG2_GResp and its corresponding Req::Api::GetCTRL_REG2_GPayload.
	Clients which use the corresponding server asynchronously
	frequently need to reservere/manage memory for for these
	ITC interactions and instances of this record are useful
	for that purpose.
 */
struct GetCTRL_REG2_GMem {
	/**	This is memory enough for the response message.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Resp::Api::GetCTRL_REG2_GResp)>	resp;

	/**	This is memory enough for the message payload.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Req::Api::GetCTRL_REG2_GPayload)>	payload;

	/**	This operation creates a response message using this
		 block of memory.
	 */
	Oscl::Driver::ST::LSM9DS1::AG::Register::Resp::Api::GetCTRL_REG2_GResp&
		build(	Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SAP&	sap,
				Oscl::Driver::ST::LSM9DS1::AG::Register::Resp::Api&		respApi,
				Oscl::Mt::Itc::PostMsgApi&		clientPapi
				) noexcept;

	};

/**	This record describes a block of memory that is
	large enough (and properly aligned) to hold a
	SetCTRL_REG1_GResp and its corresponding Req::Api::SetCTRL_REG1_GPayload.
	Clients which use the corresponding server asynchronously
	frequently need to reservere/manage memory for for these
	ITC interactions and instances of this record are useful
	for that purpose.
 */
struct SetCTRL_REG1_GMem {
	/**	This is memory enough for the response message.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Resp::Api::SetCTRL_REG1_GResp)>	resp;

	/**	This is memory enough for the message payload.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Req::Api::SetCTRL_REG1_GPayload)>	payload;

	/**	This operation creates a response message using this
		 block of memory.
	 */
	Oscl::Driver::ST::LSM9DS1::AG::Register::Resp::Api::SetCTRL_REG1_GResp&
		build(	Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SAP&	sap,
				Oscl::Driver::ST::LSM9DS1::AG::Register::Resp::Api&		respApi,
				Oscl::Mt::Itc::PostMsgApi&		clientPapi,
				uint8_t	value
				) noexcept;

	};

/**	This record describes a block of memory that is
	large enough (and properly aligned) to hold a
	GetCTRL_REG1_GResp and its corresponding Req::Api::GetCTRL_REG1_GPayload.
	Clients which use the corresponding server asynchronously
	frequently need to reservere/manage memory for for these
	ITC interactions and instances of this record are useful
	for that purpose.
 */
struct GetCTRL_REG1_GMem {
	/**	This is memory enough for the response message.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Resp::Api::GetCTRL_REG1_GResp)>	resp;

	/**	This is memory enough for the message payload.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Req::Api::GetCTRL_REG1_GPayload)>	payload;

	/**	This operation creates a response message using this
		 block of memory.
	 */
	Oscl::Driver::ST::LSM9DS1::AG::Register::Resp::Api::GetCTRL_REG1_GResp&
		build(	Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SAP&	sap,
				Oscl::Driver::ST::LSM9DS1::AG::Register::Resp::Api&		respApi,
				Oscl::Mt::Itc::PostMsgApi&		clientPapi
				) noexcept;

	};

/**	This record describes a block of memory that is
	large enough (and properly aligned) to hold a
	GetWHO_AM_IResp and its corresponding Req::Api::GetWHO_AM_IPayload.
	Clients which use the corresponding server asynchronously
	frequently need to reservere/manage memory for for these
	ITC interactions and instances of this record are useful
	for that purpose.
 */
struct GetWHO_AM_IMem {
	/**	This is memory enough for the response message.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Resp::Api::GetWHO_AM_IResp)>	resp;

	/**	This is memory enough for the message payload.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Req::Api::GetWHO_AM_IPayload)>	payload;

	/**	This operation creates a response message using this
		 block of memory.
	 */
	Oscl::Driver::ST::LSM9DS1::AG::Register::Resp::Api::GetWHO_AM_IResp&
		build(	Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SAP&	sap,
				Oscl::Driver::ST::LSM9DS1::AG::Register::Resp::Api&		respApi,
				Oscl::Mt::Itc::PostMsgApi&		clientPapi
				) noexcept;

	};

/**	This record describes a block of memory that is
	large enough (and properly aligned) to hold a
	SetINT2_CTRLResp and its corresponding Req::Api::SetINT2_CTRLPayload.
	Clients which use the corresponding server asynchronously
	frequently need to reservere/manage memory for for these
	ITC interactions and instances of this record are useful
	for that purpose.
 */
struct SetINT2_CTRLMem {
	/**	This is memory enough for the response message.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Resp::Api::SetINT2_CTRLResp)>	resp;

	/**	This is memory enough for the message payload.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Req::Api::SetINT2_CTRLPayload)>	payload;

	/**	This operation creates a response message using this
		 block of memory.
	 */
	Oscl::Driver::ST::LSM9DS1::AG::Register::Resp::Api::SetINT2_CTRLResp&
		build(	Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SAP&	sap,
				Oscl::Driver::ST::LSM9DS1::AG::Register::Resp::Api&		respApi,
				Oscl::Mt::Itc::PostMsgApi&		clientPapi,
				uint8_t	value
				) noexcept;

	};

/**	This record describes a block of memory that is
	large enough (and properly aligned) to hold a
	GetINT2_CTRLResp and its corresponding Req::Api::GetINT2_CTRLPayload.
	Clients which use the corresponding server asynchronously
	frequently need to reservere/manage memory for for these
	ITC interactions and instances of this record are useful
	for that purpose.
 */
struct GetINT2_CTRLMem {
	/**	This is memory enough for the response message.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Resp::Api::GetINT2_CTRLResp)>	resp;

	/**	This is memory enough for the message payload.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Req::Api::GetINT2_CTRLPayload)>	payload;

	/**	This operation creates a response message using this
		 block of memory.
	 */
	Oscl::Driver::ST::LSM9DS1::AG::Register::Resp::Api::GetINT2_CTRLResp&
		build(	Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SAP&	sap,
				Oscl::Driver::ST::LSM9DS1::AG::Register::Resp::Api&		respApi,
				Oscl::Mt::Itc::PostMsgApi&		clientPapi
				) noexcept;

	};

/**	This record describes a block of memory that is
	large enough (and properly aligned) to hold a
	SetINT1_CTRLResp and its corresponding Req::Api::SetINT1_CTRLPayload.
	Clients which use the corresponding server asynchronously
	frequently need to reservere/manage memory for for these
	ITC interactions and instances of this record are useful
	for that purpose.
 */
struct SetINT1_CTRLMem {
	/**	This is memory enough for the response message.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Resp::Api::SetINT1_CTRLResp)>	resp;

	/**	This is memory enough for the message payload.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Req::Api::SetINT1_CTRLPayload)>	payload;

	/**	This operation creates a response message using this
		 block of memory.
	 */
	Oscl::Driver::ST::LSM9DS1::AG::Register::Resp::Api::SetINT1_CTRLResp&
		build(	Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SAP&	sap,
				Oscl::Driver::ST::LSM9DS1::AG::Register::Resp::Api&		respApi,
				Oscl::Mt::Itc::PostMsgApi&		clientPapi,
				uint8_t	value
				) noexcept;

	};

/**	This record describes a block of memory that is
	large enough (and properly aligned) to hold a
	GetINT1_CTRLResp and its corresponding Req::Api::GetINT1_CTRLPayload.
	Clients which use the corresponding server asynchronously
	frequently need to reservere/manage memory for for these
	ITC interactions and instances of this record are useful
	for that purpose.
 */
struct GetINT1_CTRLMem {
	/**	This is memory enough for the response message.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Resp::Api::GetINT1_CTRLResp)>	resp;

	/**	This is memory enough for the message payload.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Req::Api::GetINT1_CTRLPayload)>	payload;

	/**	This operation creates a response message using this
		 block of memory.
	 */
	Oscl::Driver::ST::LSM9DS1::AG::Register::Resp::Api::GetINT1_CTRLResp&
		build(	Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SAP&	sap,
				Oscl::Driver::ST::LSM9DS1::AG::Register::Resp::Api&		respApi,
				Oscl::Mt::Itc::PostMsgApi&		clientPapi
				) noexcept;

	};

/**	This record describes a block of memory that is
	large enough (and properly aligned) to hold a
	SetREFERENCE_GResp and its corresponding Req::Api::SetREFERENCE_GPayload.
	Clients which use the corresponding server asynchronously
	frequently need to reservere/manage memory for for these
	ITC interactions and instances of this record are useful
	for that purpose.
 */
struct SetREFERENCE_GMem {
	/**	This is memory enough for the response message.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Resp::Api::SetREFERENCE_GResp)>	resp;

	/**	This is memory enough for the message payload.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Req::Api::SetREFERENCE_GPayload)>	payload;

	/**	This operation creates a response message using this
		 block of memory.
	 */
	Oscl::Driver::ST::LSM9DS1::AG::Register::Resp::Api::SetREFERENCE_GResp&
		build(	Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SAP&	sap,
				Oscl::Driver::ST::LSM9DS1::AG::Register::Resp::Api&		respApi,
				Oscl::Mt::Itc::PostMsgApi&		clientPapi,
				uint8_t	value
				) noexcept;

	};

/**	This record describes a block of memory that is
	large enough (and properly aligned) to hold a
	GetREFERENCE_GResp and its corresponding Req::Api::GetREFERENCE_GPayload.
	Clients which use the corresponding server asynchronously
	frequently need to reservere/manage memory for for these
	ITC interactions and instances of this record are useful
	for that purpose.
 */
struct GetREFERENCE_GMem {
	/**	This is memory enough for the response message.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Resp::Api::GetREFERENCE_GResp)>	resp;

	/**	This is memory enough for the message payload.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Req::Api::GetREFERENCE_GPayload)>	payload;

	/**	This operation creates a response message using this
		 block of memory.
	 */
	Oscl::Driver::ST::LSM9DS1::AG::Register::Resp::Api::GetREFERENCE_GResp&
		build(	Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SAP&	sap,
				Oscl::Driver::ST::LSM9DS1::AG::Register::Resp::Api&		respApi,
				Oscl::Mt::Itc::PostMsgApi&		clientPapi
				) noexcept;

	};

/**	This record describes a block of memory that is
	large enough (and properly aligned) to hold a
	SetINT_GEN_DUR_XLResp and its corresponding Req::Api::SetINT_GEN_DUR_XLPayload.
	Clients which use the corresponding server asynchronously
	frequently need to reservere/manage memory for for these
	ITC interactions and instances of this record are useful
	for that purpose.
 */
struct SetINT_GEN_DUR_XLMem {
	/**	This is memory enough for the response message.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Resp::Api::SetINT_GEN_DUR_XLResp)>	resp;

	/**	This is memory enough for the message payload.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Req::Api::SetINT_GEN_DUR_XLPayload)>	payload;

	/**	This operation creates a response message using this
		 block of memory.
	 */
	Oscl::Driver::ST::LSM9DS1::AG::Register::Resp::Api::SetINT_GEN_DUR_XLResp&
		build(	Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SAP&	sap,
				Oscl::Driver::ST::LSM9DS1::AG::Register::Resp::Api&		respApi,
				Oscl::Mt::Itc::PostMsgApi&		clientPapi,
				uint8_t	value
				) noexcept;

	};

/**	This record describes a block of memory that is
	large enough (and properly aligned) to hold a
	GetINT_GEN_DUR_XLResp and its corresponding Req::Api::GetINT_GEN_DUR_XLPayload.
	Clients which use the corresponding server asynchronously
	frequently need to reservere/manage memory for for these
	ITC interactions and instances of this record are useful
	for that purpose.
 */
struct GetINT_GEN_DUR_XLMem {
	/**	This is memory enough for the response message.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Resp::Api::GetINT_GEN_DUR_XLResp)>	resp;

	/**	This is memory enough for the message payload.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Req::Api::GetINT_GEN_DUR_XLPayload)>	payload;

	/**	This operation creates a response message using this
		 block of memory.
	 */
	Oscl::Driver::ST::LSM9DS1::AG::Register::Resp::Api::GetINT_GEN_DUR_XLResp&
		build(	Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SAP&	sap,
				Oscl::Driver::ST::LSM9DS1::AG::Register::Resp::Api&		respApi,
				Oscl::Mt::Itc::PostMsgApi&		clientPapi
				) noexcept;

	};

/**	This record describes a block of memory that is
	large enough (and properly aligned) to hold a
	SetINT_GEN_THS_Z_XLResp and its corresponding Req::Api::SetINT_GEN_THS_Z_XLPayload.
	Clients which use the corresponding server asynchronously
	frequently need to reservere/manage memory for for these
	ITC interactions and instances of this record are useful
	for that purpose.
 */
struct SetINT_GEN_THS_Z_XLMem {
	/**	This is memory enough for the response message.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Resp::Api::SetINT_GEN_THS_Z_XLResp)>	resp;

	/**	This is memory enough for the message payload.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Req::Api::SetINT_GEN_THS_Z_XLPayload)>	payload;

	/**	This operation creates a response message using this
		 block of memory.
	 */
	Oscl::Driver::ST::LSM9DS1::AG::Register::Resp::Api::SetINT_GEN_THS_Z_XLResp&
		build(	Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SAP&	sap,
				Oscl::Driver::ST::LSM9DS1::AG::Register::Resp::Api&		respApi,
				Oscl::Mt::Itc::PostMsgApi&		clientPapi,
				uint8_t	value
				) noexcept;

	};

/**	This record describes a block of memory that is
	large enough (and properly aligned) to hold a
	GetINT_GEN_THS_Z_XLResp and its corresponding Req::Api::GetINT_GEN_THS_Z_XLPayload.
	Clients which use the corresponding server asynchronously
	frequently need to reservere/manage memory for for these
	ITC interactions and instances of this record are useful
	for that purpose.
 */
struct GetINT_GEN_THS_Z_XLMem {
	/**	This is memory enough for the response message.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Resp::Api::GetINT_GEN_THS_Z_XLResp)>	resp;

	/**	This is memory enough for the message payload.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Req::Api::GetINT_GEN_THS_Z_XLPayload)>	payload;

	/**	This operation creates a response message using this
		 block of memory.
	 */
	Oscl::Driver::ST::LSM9DS1::AG::Register::Resp::Api::GetINT_GEN_THS_Z_XLResp&
		build(	Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SAP&	sap,
				Oscl::Driver::ST::LSM9DS1::AG::Register::Resp::Api&		respApi,
				Oscl::Mt::Itc::PostMsgApi&		clientPapi
				) noexcept;

	};

/**	This record describes a block of memory that is
	large enough (and properly aligned) to hold a
	SetINT_GEN_THS_Y_XLResp and its corresponding Req::Api::SetINT_GEN_THS_Y_XLPayload.
	Clients which use the corresponding server asynchronously
	frequently need to reservere/manage memory for for these
	ITC interactions and instances of this record are useful
	for that purpose.
 */
struct SetINT_GEN_THS_Y_XLMem {
	/**	This is memory enough for the response message.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Resp::Api::SetINT_GEN_THS_Y_XLResp)>	resp;

	/**	This is memory enough for the message payload.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Req::Api::SetINT_GEN_THS_Y_XLPayload)>	payload;

	/**	This operation creates a response message using this
		 block of memory.
	 */
	Oscl::Driver::ST::LSM9DS1::AG::Register::Resp::Api::SetINT_GEN_THS_Y_XLResp&
		build(	Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SAP&	sap,
				Oscl::Driver::ST::LSM9DS1::AG::Register::Resp::Api&		respApi,
				Oscl::Mt::Itc::PostMsgApi&		clientPapi,
				uint8_t	value
				) noexcept;

	};

/**	This record describes a block of memory that is
	large enough (and properly aligned) to hold a
	GetINT_GEN_THS_Y_XLResp and its corresponding Req::Api::GetINT_GEN_THS_Y_XLPayload.
	Clients which use the corresponding server asynchronously
	frequently need to reservere/manage memory for for these
	ITC interactions and instances of this record are useful
	for that purpose.
 */
struct GetINT_GEN_THS_Y_XLMem {
	/**	This is memory enough for the response message.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Resp::Api::GetINT_GEN_THS_Y_XLResp)>	resp;

	/**	This is memory enough for the message payload.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Req::Api::GetINT_GEN_THS_Y_XLPayload)>	payload;

	/**	This operation creates a response message using this
		 block of memory.
	 */
	Oscl::Driver::ST::LSM9DS1::AG::Register::Resp::Api::GetINT_GEN_THS_Y_XLResp&
		build(	Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SAP&	sap,
				Oscl::Driver::ST::LSM9DS1::AG::Register::Resp::Api&		respApi,
				Oscl::Mt::Itc::PostMsgApi&		clientPapi
				) noexcept;

	};

/**	This record describes a block of memory that is
	large enough (and properly aligned) to hold a
	SetINT_GEN_THS_X_XLResp and its corresponding Req::Api::SetINT_GEN_THS_X_XLPayload.
	Clients which use the corresponding server asynchronously
	frequently need to reservere/manage memory for for these
	ITC interactions and instances of this record are useful
	for that purpose.
 */
struct SetINT_GEN_THS_X_XLMem {
	/**	This is memory enough for the response message.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Resp::Api::SetINT_GEN_THS_X_XLResp)>	resp;

	/**	This is memory enough for the message payload.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Req::Api::SetINT_GEN_THS_X_XLPayload)>	payload;

	/**	This operation creates a response message using this
		 block of memory.
	 */
	Oscl::Driver::ST::LSM9DS1::AG::Register::Resp::Api::SetINT_GEN_THS_X_XLResp&
		build(	Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SAP&	sap,
				Oscl::Driver::ST::LSM9DS1::AG::Register::Resp::Api&		respApi,
				Oscl::Mt::Itc::PostMsgApi&		clientPapi,
				uint8_t	value
				) noexcept;

	};

/**	This record describes a block of memory that is
	large enough (and properly aligned) to hold a
	GetINT_GEN_THS_X_XLResp and its corresponding Req::Api::GetINT_GEN_THS_X_XLPayload.
	Clients which use the corresponding server asynchronously
	frequently need to reservere/manage memory for for these
	ITC interactions and instances of this record are useful
	for that purpose.
 */
struct GetINT_GEN_THS_X_XLMem {
	/**	This is memory enough for the response message.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Resp::Api::GetINT_GEN_THS_X_XLResp)>	resp;

	/**	This is memory enough for the message payload.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Req::Api::GetINT_GEN_THS_X_XLPayload)>	payload;

	/**	This operation creates a response message using this
		 block of memory.
	 */
	Oscl::Driver::ST::LSM9DS1::AG::Register::Resp::Api::GetINT_GEN_THS_X_XLResp&
		build(	Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SAP&	sap,
				Oscl::Driver::ST::LSM9DS1::AG::Register::Resp::Api&		respApi,
				Oscl::Mt::Itc::PostMsgApi&		clientPapi
				) noexcept;

	};

/**	This record describes a block of memory that is
	large enough (and properly aligned) to hold a
	SetINT_GEN_CFG_XLResp and its corresponding Req::Api::SetINT_GEN_CFG_XLPayload.
	Clients which use the corresponding server asynchronously
	frequently need to reservere/manage memory for for these
	ITC interactions and instances of this record are useful
	for that purpose.
 */
struct SetINT_GEN_CFG_XLMem {
	/**	This is memory enough for the response message.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Resp::Api::SetINT_GEN_CFG_XLResp)>	resp;

	/**	This is memory enough for the message payload.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Req::Api::SetINT_GEN_CFG_XLPayload)>	payload;

	/**	This operation creates a response message using this
		 block of memory.
	 */
	Oscl::Driver::ST::LSM9DS1::AG::Register::Resp::Api::SetINT_GEN_CFG_XLResp&
		build(	Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SAP&	sap,
				Oscl::Driver::ST::LSM9DS1::AG::Register::Resp::Api&		respApi,
				Oscl::Mt::Itc::PostMsgApi&		clientPapi,
				uint8_t	value
				) noexcept;

	};

/**	This record describes a block of memory that is
	large enough (and properly aligned) to hold a
	GetINT_GEN_CFG_XLResp and its corresponding Req::Api::GetINT_GEN_CFG_XLPayload.
	Clients which use the corresponding server asynchronously
	frequently need to reservere/manage memory for for these
	ITC interactions and instances of this record are useful
	for that purpose.
 */
struct GetINT_GEN_CFG_XLMem {
	/**	This is memory enough for the response message.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Resp::Api::GetINT_GEN_CFG_XLResp)>	resp;

	/**	This is memory enough for the message payload.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Req::Api::GetINT_GEN_CFG_XLPayload)>	payload;

	/**	This operation creates a response message using this
		 block of memory.
	 */
	Oscl::Driver::ST::LSM9DS1::AG::Register::Resp::Api::GetINT_GEN_CFG_XLResp&
		build(	Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SAP&	sap,
				Oscl::Driver::ST::LSM9DS1::AG::Register::Resp::Api&		respApi,
				Oscl::Mt::Itc::PostMsgApi&		clientPapi
				) noexcept;

	};

/**	This record describes a block of memory that is
	large enough (and properly aligned) to hold a
	SetACT_DURResp and its corresponding Req::Api::SetACT_DURPayload.
	Clients which use the corresponding server asynchronously
	frequently need to reservere/manage memory for for these
	ITC interactions and instances of this record are useful
	for that purpose.
 */
struct SetACT_DURMem {
	/**	This is memory enough for the response message.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Resp::Api::SetACT_DURResp)>	resp;

	/**	This is memory enough for the message payload.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Req::Api::SetACT_DURPayload)>	payload;

	/**	This operation creates a response message using this
		 block of memory.
	 */
	Oscl::Driver::ST::LSM9DS1::AG::Register::Resp::Api::SetACT_DURResp&
		build(	Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SAP&	sap,
				Oscl::Driver::ST::LSM9DS1::AG::Register::Resp::Api&		respApi,
				Oscl::Mt::Itc::PostMsgApi&		clientPapi,
				uint8_t	value
				) noexcept;

	};

/**	This record describes a block of memory that is
	large enough (and properly aligned) to hold a
	GetACT_DURResp and its corresponding Req::Api::GetACT_DURPayload.
	Clients which use the corresponding server asynchronously
	frequently need to reservere/manage memory for for these
	ITC interactions and instances of this record are useful
	for that purpose.
 */
struct GetACT_DURMem {
	/**	This is memory enough for the response message.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Resp::Api::GetACT_DURResp)>	resp;

	/**	This is memory enough for the message payload.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Req::Api::GetACT_DURPayload)>	payload;

	/**	This operation creates a response message using this
		 block of memory.
	 */
	Oscl::Driver::ST::LSM9DS1::AG::Register::Resp::Api::GetACT_DURResp&
		build(	Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SAP&	sap,
				Oscl::Driver::ST::LSM9DS1::AG::Register::Resp::Api&		respApi,
				Oscl::Mt::Itc::PostMsgApi&		clientPapi
				) noexcept;

	};

/**	This record describes a block of memory that is
	large enough (and properly aligned) to hold a
	SetACT_THSResp and its corresponding Req::Api::SetACT_THSPayload.
	Clients which use the corresponding server asynchronously
	frequently need to reservere/manage memory for for these
	ITC interactions and instances of this record are useful
	for that purpose.
 */
struct SetACT_THSMem {
	/**	This is memory enough for the response message.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Resp::Api::SetACT_THSResp)>	resp;

	/**	This is memory enough for the message payload.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Req::Api::SetACT_THSPayload)>	payload;

	/**	This operation creates a response message using this
		 block of memory.
	 */
	Oscl::Driver::ST::LSM9DS1::AG::Register::Resp::Api::SetACT_THSResp&
		build(	Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SAP&	sap,
				Oscl::Driver::ST::LSM9DS1::AG::Register::Resp::Api&		respApi,
				Oscl::Mt::Itc::PostMsgApi&		clientPapi,
				uint8_t	value
				) noexcept;

	};

/**	This record describes a block of memory that is
	large enough (and properly aligned) to hold a
	GetACT_THSResp and its corresponding Req::Api::GetACT_THSPayload.
	Clients which use the corresponding server asynchronously
	frequently need to reservere/manage memory for for these
	ITC interactions and instances of this record are useful
	for that purpose.
 */
struct GetACT_THSMem {
	/**	This is memory enough for the response message.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Resp::Api::GetACT_THSResp)>	resp;

	/**	This is memory enough for the message payload.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Req::Api::GetACT_THSPayload)>	payload;

	/**	This operation creates a response message using this
		 block of memory.
	 */
	Oscl::Driver::ST::LSM9DS1::AG::Register::Resp::Api::GetACT_THSResp&
		build(	Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SAP&	sap,
				Oscl::Driver::ST::LSM9DS1::AG::Register::Resp::Api&		respApi,
				Oscl::Mt::Itc::PostMsgApi&		clientPapi
				) noexcept;

	};

}
}
}
}
}
}
}
#endif
