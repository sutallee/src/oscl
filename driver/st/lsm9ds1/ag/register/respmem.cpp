/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <new>
#include "respmem.h"

using namespace Oscl::Driver::ST::LSM9DS1::AG::Register;

Resp::Api::GetOUT_TEMPResp&
	Resp::GetOUT_TEMPMem::build(	Req::Api::SAP&			sap,
							Resp::Api&				respApi,
							Oscl::Mt::Itc::PostMsgApi&	clientPapi,
							uint8_t	maxBufferLength,
							void*	buffer
							) noexcept{
	Req::Api::GetOUT_TEMPPayload*
		p	=	new(&payload)
				Req::Api::GetOUT_TEMPPayload(
							maxBufferLength,
							buffer
							);
	Resp::Api::GetOUT_TEMPResp*
		r	=	new(&resp)
			Resp::Api::GetOUT_TEMPResp(	sap.getReqApi(),
									respApi,
									clientPapi,
									*p
									);

	return *r;
	}

Resp::Api::GetOUT_GResp&
	Resp::GetOUT_GMem::build(	Req::Api::SAP&			sap,
							Resp::Api&				respApi,
							Oscl::Mt::Itc::PostMsgApi&	clientPapi,
							uint8_t	maxBufferLength,
							void*	buffer
							) noexcept{
	Req::Api::GetOUT_GPayload*
		p	=	new(&payload)
				Req::Api::GetOUT_GPayload(
							maxBufferLength,
							buffer
							);
	Resp::Api::GetOUT_GResp*
		r	=	new(&resp)
			Resp::Api::GetOUT_GResp(	sap.getReqApi(),
									respApi,
									clientPapi,
									*p
									);

	return *r;
	}

Resp::Api::GetOUT_XLResp&
	Resp::GetOUT_XLMem::build(	Req::Api::SAP&			sap,
							Resp::Api&				respApi,
							Oscl::Mt::Itc::PostMsgApi&	clientPapi,
							uint8_t	maxBufferLength,
							void*	buffer
							) noexcept{
	Req::Api::GetOUT_XLPayload*
		p	=	new(&payload)
				Req::Api::GetOUT_XLPayload(
							maxBufferLength,
							buffer
							);
	Resp::Api::GetOUT_XLResp*
		r	=	new(&resp)
			Resp::Api::GetOUT_XLResp(	sap.getReqApi(),
									respApi,
									clientPapi,
									*p
									);

	return *r;
	}

Resp::Api::SetINT_GEN_DUR_GResp&
	Resp::SetINT_GEN_DUR_GMem::build(	Req::Api::SAP&			sap,
							Resp::Api&				respApi,
							Oscl::Mt::Itc::PostMsgApi&	clientPapi,
							uint8_t	value
							) noexcept{
	Req::Api::SetINT_GEN_DUR_GPayload*
		p	=	new(&payload)
				Req::Api::SetINT_GEN_DUR_GPayload(
							value
							);
	Resp::Api::SetINT_GEN_DUR_GResp*
		r	=	new(&resp)
			Resp::Api::SetINT_GEN_DUR_GResp(	sap.getReqApi(),
									respApi,
									clientPapi,
									*p
									);

	return *r;
	}

Resp::Api::GetINT_GEN_DUR_GResp&
	Resp::GetINT_GEN_DUR_GMem::build(	Req::Api::SAP&			sap,
							Resp::Api&				respApi,
							Oscl::Mt::Itc::PostMsgApi&	clientPapi
							) noexcept{
	Req::Api::GetINT_GEN_DUR_GPayload*
		p	=	new(&payload)
				Req::Api::GetINT_GEN_DUR_GPayload(
);
	Resp::Api::GetINT_GEN_DUR_GResp*
		r	=	new(&resp)
			Resp::Api::GetINT_GEN_DUR_GResp(	sap.getReqApi(),
									respApi,
									clientPapi,
									*p
									);

	return *r;
	}

Resp::Api::SetINT_GEN_THS_ZL_GResp&
	Resp::SetINT_GEN_THS_ZL_GMem::build(	Req::Api::SAP&			sap,
							Resp::Api&				respApi,
							Oscl::Mt::Itc::PostMsgApi&	clientPapi,
							uint8_t	value
							) noexcept{
	Req::Api::SetINT_GEN_THS_ZL_GPayload*
		p	=	new(&payload)
				Req::Api::SetINT_GEN_THS_ZL_GPayload(
							value
							);
	Resp::Api::SetINT_GEN_THS_ZL_GResp*
		r	=	new(&resp)
			Resp::Api::SetINT_GEN_THS_ZL_GResp(	sap.getReqApi(),
									respApi,
									clientPapi,
									*p
									);

	return *r;
	}

Resp::Api::GetINT_GEN_THS_ZL_GResp&
	Resp::GetINT_GEN_THS_ZL_GMem::build(	Req::Api::SAP&			sap,
							Resp::Api&				respApi,
							Oscl::Mt::Itc::PostMsgApi&	clientPapi
							) noexcept{
	Req::Api::GetINT_GEN_THS_ZL_GPayload*
		p	=	new(&payload)
				Req::Api::GetINT_GEN_THS_ZL_GPayload(
);
	Resp::Api::GetINT_GEN_THS_ZL_GResp*
		r	=	new(&resp)
			Resp::Api::GetINT_GEN_THS_ZL_GResp(	sap.getReqApi(),
									respApi,
									clientPapi,
									*p
									);

	return *r;
	}

Resp::Api::SetINT_GEN_THS_ZH_GResp&
	Resp::SetINT_GEN_THS_ZH_GMem::build(	Req::Api::SAP&			sap,
							Resp::Api&				respApi,
							Oscl::Mt::Itc::PostMsgApi&	clientPapi,
							uint8_t	value
							) noexcept{
	Req::Api::SetINT_GEN_THS_ZH_GPayload*
		p	=	new(&payload)
				Req::Api::SetINT_GEN_THS_ZH_GPayload(
							value
							);
	Resp::Api::SetINT_GEN_THS_ZH_GResp*
		r	=	new(&resp)
			Resp::Api::SetINT_GEN_THS_ZH_GResp(	sap.getReqApi(),
									respApi,
									clientPapi,
									*p
									);

	return *r;
	}

Resp::Api::GetINT_GEN_THS_ZH_GResp&
	Resp::GetINT_GEN_THS_ZH_GMem::build(	Req::Api::SAP&			sap,
							Resp::Api&				respApi,
							Oscl::Mt::Itc::PostMsgApi&	clientPapi
							) noexcept{
	Req::Api::GetINT_GEN_THS_ZH_GPayload*
		p	=	new(&payload)
				Req::Api::GetINT_GEN_THS_ZH_GPayload(
);
	Resp::Api::GetINT_GEN_THS_ZH_GResp*
		r	=	new(&resp)
			Resp::Api::GetINT_GEN_THS_ZH_GResp(	sap.getReqApi(),
									respApi,
									clientPapi,
									*p
									);

	return *r;
	}

Resp::Api::SetINT_GEN_THS_YL_GResp&
	Resp::SetINT_GEN_THS_YL_GMem::build(	Req::Api::SAP&			sap,
							Resp::Api&				respApi,
							Oscl::Mt::Itc::PostMsgApi&	clientPapi,
							uint8_t	value
							) noexcept{
	Req::Api::SetINT_GEN_THS_YL_GPayload*
		p	=	new(&payload)
				Req::Api::SetINT_GEN_THS_YL_GPayload(
							value
							);
	Resp::Api::SetINT_GEN_THS_YL_GResp*
		r	=	new(&resp)
			Resp::Api::SetINT_GEN_THS_YL_GResp(	sap.getReqApi(),
									respApi,
									clientPapi,
									*p
									);

	return *r;
	}

Resp::Api::GetINT_GEN_THS_YL_GResp&
	Resp::GetINT_GEN_THS_YL_GMem::build(	Req::Api::SAP&			sap,
							Resp::Api&				respApi,
							Oscl::Mt::Itc::PostMsgApi&	clientPapi
							) noexcept{
	Req::Api::GetINT_GEN_THS_YL_GPayload*
		p	=	new(&payload)
				Req::Api::GetINT_GEN_THS_YL_GPayload(
);
	Resp::Api::GetINT_GEN_THS_YL_GResp*
		r	=	new(&resp)
			Resp::Api::GetINT_GEN_THS_YL_GResp(	sap.getReqApi(),
									respApi,
									clientPapi,
									*p
									);

	return *r;
	}

Resp::Api::SetINT_GEN_THS_YH_GResp&
	Resp::SetINT_GEN_THS_YH_GMem::build(	Req::Api::SAP&			sap,
							Resp::Api&				respApi,
							Oscl::Mt::Itc::PostMsgApi&	clientPapi,
							uint8_t	value
							) noexcept{
	Req::Api::SetINT_GEN_THS_YH_GPayload*
		p	=	new(&payload)
				Req::Api::SetINT_GEN_THS_YH_GPayload(
							value
							);
	Resp::Api::SetINT_GEN_THS_YH_GResp*
		r	=	new(&resp)
			Resp::Api::SetINT_GEN_THS_YH_GResp(	sap.getReqApi(),
									respApi,
									clientPapi,
									*p
									);

	return *r;
	}

Resp::Api::GetINT_GEN_THS_YH_GResp&
	Resp::GetINT_GEN_THS_YH_GMem::build(	Req::Api::SAP&			sap,
							Resp::Api&				respApi,
							Oscl::Mt::Itc::PostMsgApi&	clientPapi
							) noexcept{
	Req::Api::GetINT_GEN_THS_YH_GPayload*
		p	=	new(&payload)
				Req::Api::GetINT_GEN_THS_YH_GPayload(
);
	Resp::Api::GetINT_GEN_THS_YH_GResp*
		r	=	new(&resp)
			Resp::Api::GetINT_GEN_THS_YH_GResp(	sap.getReqApi(),
									respApi,
									clientPapi,
									*p
									);

	return *r;
	}

Resp::Api::SetINT_GEN_THS_XL_GResp&
	Resp::SetINT_GEN_THS_XL_GMem::build(	Req::Api::SAP&			sap,
							Resp::Api&				respApi,
							Oscl::Mt::Itc::PostMsgApi&	clientPapi,
							uint8_t	value
							) noexcept{
	Req::Api::SetINT_GEN_THS_XL_GPayload*
		p	=	new(&payload)
				Req::Api::SetINT_GEN_THS_XL_GPayload(
							value
							);
	Resp::Api::SetINT_GEN_THS_XL_GResp*
		r	=	new(&resp)
			Resp::Api::SetINT_GEN_THS_XL_GResp(	sap.getReqApi(),
									respApi,
									clientPapi,
									*p
									);

	return *r;
	}

Resp::Api::GetINT_GEN_THS_XL_GResp&
	Resp::GetINT_GEN_THS_XL_GMem::build(	Req::Api::SAP&			sap,
							Resp::Api&				respApi,
							Oscl::Mt::Itc::PostMsgApi&	clientPapi
							) noexcept{
	Req::Api::GetINT_GEN_THS_XL_GPayload*
		p	=	new(&payload)
				Req::Api::GetINT_GEN_THS_XL_GPayload(
);
	Resp::Api::GetINT_GEN_THS_XL_GResp*
		r	=	new(&resp)
			Resp::Api::GetINT_GEN_THS_XL_GResp(	sap.getReqApi(),
									respApi,
									clientPapi,
									*p
									);

	return *r;
	}

Resp::Api::SetINT_GEN_THS_XH_GResp&
	Resp::SetINT_GEN_THS_XH_GMem::build(	Req::Api::SAP&			sap,
							Resp::Api&				respApi,
							Oscl::Mt::Itc::PostMsgApi&	clientPapi,
							uint8_t	value
							) noexcept{
	Req::Api::SetINT_GEN_THS_XH_GPayload*
		p	=	new(&payload)
				Req::Api::SetINT_GEN_THS_XH_GPayload(
							value
							);
	Resp::Api::SetINT_GEN_THS_XH_GResp*
		r	=	new(&resp)
			Resp::Api::SetINT_GEN_THS_XH_GResp(	sap.getReqApi(),
									respApi,
									clientPapi,
									*p
									);

	return *r;
	}

Resp::Api::GetINT_GEN_THS_XH_GResp&
	Resp::GetINT_GEN_THS_XH_GMem::build(	Req::Api::SAP&			sap,
							Resp::Api&				respApi,
							Oscl::Mt::Itc::PostMsgApi&	clientPapi
							) noexcept{
	Req::Api::GetINT_GEN_THS_XH_GPayload*
		p	=	new(&payload)
				Req::Api::GetINT_GEN_THS_XH_GPayload(
);
	Resp::Api::GetINT_GEN_THS_XH_GResp*
		r	=	new(&resp)
			Resp::Api::GetINT_GEN_THS_XH_GResp(	sap.getReqApi(),
									respApi,
									clientPapi,
									*p
									);

	return *r;
	}

Resp::Api::SetINT_GEN_CFG_GResp&
	Resp::SetINT_GEN_CFG_GMem::build(	Req::Api::SAP&			sap,
							Resp::Api&				respApi,
							Oscl::Mt::Itc::PostMsgApi&	clientPapi,
							uint8_t	value
							) noexcept{
	Req::Api::SetINT_GEN_CFG_GPayload*
		p	=	new(&payload)
				Req::Api::SetINT_GEN_CFG_GPayload(
							value
							);
	Resp::Api::SetINT_GEN_CFG_GResp*
		r	=	new(&resp)
			Resp::Api::SetINT_GEN_CFG_GResp(	sap.getReqApi(),
									respApi,
									clientPapi,
									*p
									);

	return *r;
	}

Resp::Api::GetINT_GEN_CFG_GResp&
	Resp::GetINT_GEN_CFG_GMem::build(	Req::Api::SAP&			sap,
							Resp::Api&				respApi,
							Oscl::Mt::Itc::PostMsgApi&	clientPapi
							) noexcept{
	Req::Api::GetINT_GEN_CFG_GPayload*
		p	=	new(&payload)
				Req::Api::GetINT_GEN_CFG_GPayload(
);
	Resp::Api::GetINT_GEN_CFG_GResp*
		r	=	new(&resp)
			Resp::Api::GetINT_GEN_CFG_GResp(	sap.getReqApi(),
									respApi,
									clientPapi,
									*p
									);

	return *r;
	}

Resp::Api::GetFIFO_SRCResp&
	Resp::GetFIFO_SRCMem::build(	Req::Api::SAP&			sap,
							Resp::Api&				respApi,
							Oscl::Mt::Itc::PostMsgApi&	clientPapi
							) noexcept{
	Req::Api::GetFIFO_SRCPayload*
		p	=	new(&payload)
				Req::Api::GetFIFO_SRCPayload(
);
	Resp::Api::GetFIFO_SRCResp*
		r	=	new(&resp)
			Resp::Api::GetFIFO_SRCResp(	sap.getReqApi(),
									respApi,
									clientPapi,
									*p
									);

	return *r;
	}

Resp::Api::SetFIFO_CTRLResp&
	Resp::SetFIFO_CTRLMem::build(	Req::Api::SAP&			sap,
							Resp::Api&				respApi,
							Oscl::Mt::Itc::PostMsgApi&	clientPapi,
							uint8_t	value
							) noexcept{
	Req::Api::SetFIFO_CTRLPayload*
		p	=	new(&payload)
				Req::Api::SetFIFO_CTRLPayload(
							value
							);
	Resp::Api::SetFIFO_CTRLResp*
		r	=	new(&resp)
			Resp::Api::SetFIFO_CTRLResp(	sap.getReqApi(),
									respApi,
									clientPapi,
									*p
									);

	return *r;
	}

Resp::Api::GetFIFO_CTRLResp&
	Resp::GetFIFO_CTRLMem::build(	Req::Api::SAP&			sap,
							Resp::Api&				respApi,
							Oscl::Mt::Itc::PostMsgApi&	clientPapi
							) noexcept{
	Req::Api::GetFIFO_CTRLPayload*
		p	=	new(&payload)
				Req::Api::GetFIFO_CTRLPayload(
);
	Resp::Api::GetFIFO_CTRLResp*
		r	=	new(&resp)
			Resp::Api::GetFIFO_CTRLResp(	sap.getReqApi(),
									respApi,
									clientPapi,
									*p
									);

	return *r;
	}

Resp::Api::GetOUT_Z_H_XLResp&
	Resp::GetOUT_Z_H_XLMem::build(	Req::Api::SAP&			sap,
							Resp::Api&				respApi,
							Oscl::Mt::Itc::PostMsgApi&	clientPapi
							) noexcept{
	Req::Api::GetOUT_Z_H_XLPayload*
		p	=	new(&payload)
				Req::Api::GetOUT_Z_H_XLPayload(
);
	Resp::Api::GetOUT_Z_H_XLResp*
		r	=	new(&resp)
			Resp::Api::GetOUT_Z_H_XLResp(	sap.getReqApi(),
									respApi,
									clientPapi,
									*p
									);

	return *r;
	}

Resp::Api::GetOUT_Z_L_XLResp&
	Resp::GetOUT_Z_L_XLMem::build(	Req::Api::SAP&			sap,
							Resp::Api&				respApi,
							Oscl::Mt::Itc::PostMsgApi&	clientPapi
							) noexcept{
	Req::Api::GetOUT_Z_L_XLPayload*
		p	=	new(&payload)
				Req::Api::GetOUT_Z_L_XLPayload(
);
	Resp::Api::GetOUT_Z_L_XLResp*
		r	=	new(&resp)
			Resp::Api::GetOUT_Z_L_XLResp(	sap.getReqApi(),
									respApi,
									clientPapi,
									*p
									);

	return *r;
	}

Resp::Api::GetOUT_Y_H_XLResp&
	Resp::GetOUT_Y_H_XLMem::build(	Req::Api::SAP&			sap,
							Resp::Api&				respApi,
							Oscl::Mt::Itc::PostMsgApi&	clientPapi
							) noexcept{
	Req::Api::GetOUT_Y_H_XLPayload*
		p	=	new(&payload)
				Req::Api::GetOUT_Y_H_XLPayload(
);
	Resp::Api::GetOUT_Y_H_XLResp*
		r	=	new(&resp)
			Resp::Api::GetOUT_Y_H_XLResp(	sap.getReqApi(),
									respApi,
									clientPapi,
									*p
									);

	return *r;
	}

Resp::Api::GetOUT_Y_L_XLResp&
	Resp::GetOUT_Y_L_XLMem::build(	Req::Api::SAP&			sap,
							Resp::Api&				respApi,
							Oscl::Mt::Itc::PostMsgApi&	clientPapi
							) noexcept{
	Req::Api::GetOUT_Y_L_XLPayload*
		p	=	new(&payload)
				Req::Api::GetOUT_Y_L_XLPayload(
);
	Resp::Api::GetOUT_Y_L_XLResp*
		r	=	new(&resp)
			Resp::Api::GetOUT_Y_L_XLResp(	sap.getReqApi(),
									respApi,
									clientPapi,
									*p
									);

	return *r;
	}

Resp::Api::GetOUT_X_H_XLResp&
	Resp::GetOUT_X_H_XLMem::build(	Req::Api::SAP&			sap,
							Resp::Api&				respApi,
							Oscl::Mt::Itc::PostMsgApi&	clientPapi
							) noexcept{
	Req::Api::GetOUT_X_H_XLPayload*
		p	=	new(&payload)
				Req::Api::GetOUT_X_H_XLPayload(
);
	Resp::Api::GetOUT_X_H_XLResp*
		r	=	new(&resp)
			Resp::Api::GetOUT_X_H_XLResp(	sap.getReqApi(),
									respApi,
									clientPapi,
									*p
									);

	return *r;
	}

Resp::Api::GetOUT_X_L_XLResp&
	Resp::GetOUT_X_L_XLMem::build(	Req::Api::SAP&			sap,
							Resp::Api&				respApi,
							Oscl::Mt::Itc::PostMsgApi&	clientPapi
							) noexcept{
	Req::Api::GetOUT_X_L_XLPayload*
		p	=	new(&payload)
				Req::Api::GetOUT_X_L_XLPayload(
);
	Resp::Api::GetOUT_X_L_XLResp*
		r	=	new(&resp)
			Resp::Api::GetOUT_X_L_XLResp(	sap.getReqApi(),
									respApi,
									clientPapi,
									*p
									);

	return *r;
	}

Resp::Api::GetSTATUS_REG_2Resp&
	Resp::GetSTATUS_REG_2Mem::build(	Req::Api::SAP&			sap,
							Resp::Api&				respApi,
							Oscl::Mt::Itc::PostMsgApi&	clientPapi
							) noexcept{
	Req::Api::GetSTATUS_REG_2Payload*
		p	=	new(&payload)
				Req::Api::GetSTATUS_REG_2Payload(
);
	Resp::Api::GetSTATUS_REG_2Resp*
		r	=	new(&resp)
			Resp::Api::GetSTATUS_REG_2Resp(	sap.getReqApi(),
									respApi,
									clientPapi,
									*p
									);

	return *r;
	}

Resp::Api::GetINT_GEN_SRC_XLResp&
	Resp::GetINT_GEN_SRC_XLMem::build(	Req::Api::SAP&			sap,
							Resp::Api&				respApi,
							Oscl::Mt::Itc::PostMsgApi&	clientPapi
							) noexcept{
	Req::Api::GetINT_GEN_SRC_XLPayload*
		p	=	new(&payload)
				Req::Api::GetINT_GEN_SRC_XLPayload(
);
	Resp::Api::GetINT_GEN_SRC_XLResp*
		r	=	new(&resp)
			Resp::Api::GetINT_GEN_SRC_XLResp(	sap.getReqApi(),
									respApi,
									clientPapi,
									*p
									);

	return *r;
	}

Resp::Api::SetCTRL_REG10Resp&
	Resp::SetCTRL_REG10Mem::build(	Req::Api::SAP&			sap,
							Resp::Api&				respApi,
							Oscl::Mt::Itc::PostMsgApi&	clientPapi,
							uint8_t	value
							) noexcept{
	Req::Api::SetCTRL_REG10Payload*
		p	=	new(&payload)
				Req::Api::SetCTRL_REG10Payload(
							value
							);
	Resp::Api::SetCTRL_REG10Resp*
		r	=	new(&resp)
			Resp::Api::SetCTRL_REG10Resp(	sap.getReqApi(),
									respApi,
									clientPapi,
									*p
									);

	return *r;
	}

Resp::Api::GetCTRL_REG10Resp&
	Resp::GetCTRL_REG10Mem::build(	Req::Api::SAP&			sap,
							Resp::Api&				respApi,
							Oscl::Mt::Itc::PostMsgApi&	clientPapi
							) noexcept{
	Req::Api::GetCTRL_REG10Payload*
		p	=	new(&payload)
				Req::Api::GetCTRL_REG10Payload(
);
	Resp::Api::GetCTRL_REG10Resp*
		r	=	new(&resp)
			Resp::Api::GetCTRL_REG10Resp(	sap.getReqApi(),
									respApi,
									clientPapi,
									*p
									);

	return *r;
	}

Resp::Api::SetCTRL_REG9Resp&
	Resp::SetCTRL_REG9Mem::build(	Req::Api::SAP&			sap,
							Resp::Api&				respApi,
							Oscl::Mt::Itc::PostMsgApi&	clientPapi,
							uint8_t	value
							) noexcept{
	Req::Api::SetCTRL_REG9Payload*
		p	=	new(&payload)
				Req::Api::SetCTRL_REG9Payload(
							value
							);
	Resp::Api::SetCTRL_REG9Resp*
		r	=	new(&resp)
			Resp::Api::SetCTRL_REG9Resp(	sap.getReqApi(),
									respApi,
									clientPapi,
									*p
									);

	return *r;
	}

Resp::Api::GetCTRL_REG9Resp&
	Resp::GetCTRL_REG9Mem::build(	Req::Api::SAP&			sap,
							Resp::Api&				respApi,
							Oscl::Mt::Itc::PostMsgApi&	clientPapi
							) noexcept{
	Req::Api::GetCTRL_REG9Payload*
		p	=	new(&payload)
				Req::Api::GetCTRL_REG9Payload(
);
	Resp::Api::GetCTRL_REG9Resp*
		r	=	new(&resp)
			Resp::Api::GetCTRL_REG9Resp(	sap.getReqApi(),
									respApi,
									clientPapi,
									*p
									);

	return *r;
	}

Resp::Api::SetCTRL_REG8Resp&
	Resp::SetCTRL_REG8Mem::build(	Req::Api::SAP&			sap,
							Resp::Api&				respApi,
							Oscl::Mt::Itc::PostMsgApi&	clientPapi,
							uint8_t	value
							) noexcept{
	Req::Api::SetCTRL_REG8Payload*
		p	=	new(&payload)
				Req::Api::SetCTRL_REG8Payload(
							value
							);
	Resp::Api::SetCTRL_REG8Resp*
		r	=	new(&resp)
			Resp::Api::SetCTRL_REG8Resp(	sap.getReqApi(),
									respApi,
									clientPapi,
									*p
									);

	return *r;
	}

Resp::Api::GetCTRL_REG8Resp&
	Resp::GetCTRL_REG8Mem::build(	Req::Api::SAP&			sap,
							Resp::Api&				respApi,
							Oscl::Mt::Itc::PostMsgApi&	clientPapi
							) noexcept{
	Req::Api::GetCTRL_REG8Payload*
		p	=	new(&payload)
				Req::Api::GetCTRL_REG8Payload(
);
	Resp::Api::GetCTRL_REG8Resp*
		r	=	new(&resp)
			Resp::Api::GetCTRL_REG8Resp(	sap.getReqApi(),
									respApi,
									clientPapi,
									*p
									);

	return *r;
	}

Resp::Api::SetCTRL_REG7_XLResp&
	Resp::SetCTRL_REG7_XLMem::build(	Req::Api::SAP&			sap,
							Resp::Api&				respApi,
							Oscl::Mt::Itc::PostMsgApi&	clientPapi,
							uint8_t	value
							) noexcept{
	Req::Api::SetCTRL_REG7_XLPayload*
		p	=	new(&payload)
				Req::Api::SetCTRL_REG7_XLPayload(
							value
							);
	Resp::Api::SetCTRL_REG7_XLResp*
		r	=	new(&resp)
			Resp::Api::SetCTRL_REG7_XLResp(	sap.getReqApi(),
									respApi,
									clientPapi,
									*p
									);

	return *r;
	}

Resp::Api::GetCTRL_REG7_XLResp&
	Resp::GetCTRL_REG7_XLMem::build(	Req::Api::SAP&			sap,
							Resp::Api&				respApi,
							Oscl::Mt::Itc::PostMsgApi&	clientPapi
							) noexcept{
	Req::Api::GetCTRL_REG7_XLPayload*
		p	=	new(&payload)
				Req::Api::GetCTRL_REG7_XLPayload(
);
	Resp::Api::GetCTRL_REG7_XLResp*
		r	=	new(&resp)
			Resp::Api::GetCTRL_REG7_XLResp(	sap.getReqApi(),
									respApi,
									clientPapi,
									*p
									);

	return *r;
	}

Resp::Api::SetCTRL_REG6_XLResp&
	Resp::SetCTRL_REG6_XLMem::build(	Req::Api::SAP&			sap,
							Resp::Api&				respApi,
							Oscl::Mt::Itc::PostMsgApi&	clientPapi,
							uint8_t	value
							) noexcept{
	Req::Api::SetCTRL_REG6_XLPayload*
		p	=	new(&payload)
				Req::Api::SetCTRL_REG6_XLPayload(
							value
							);
	Resp::Api::SetCTRL_REG6_XLResp*
		r	=	new(&resp)
			Resp::Api::SetCTRL_REG6_XLResp(	sap.getReqApi(),
									respApi,
									clientPapi,
									*p
									);

	return *r;
	}

Resp::Api::GetCTRL_REG6_XLResp&
	Resp::GetCTRL_REG6_XLMem::build(	Req::Api::SAP&			sap,
							Resp::Api&				respApi,
							Oscl::Mt::Itc::PostMsgApi&	clientPapi
							) noexcept{
	Req::Api::GetCTRL_REG6_XLPayload*
		p	=	new(&payload)
				Req::Api::GetCTRL_REG6_XLPayload(
);
	Resp::Api::GetCTRL_REG6_XLResp*
		r	=	new(&resp)
			Resp::Api::GetCTRL_REG6_XLResp(	sap.getReqApi(),
									respApi,
									clientPapi,
									*p
									);

	return *r;
	}

Resp::Api::SetCTRL_REG5_XLResp&
	Resp::SetCTRL_REG5_XLMem::build(	Req::Api::SAP&			sap,
							Resp::Api&				respApi,
							Oscl::Mt::Itc::PostMsgApi&	clientPapi,
							uint8_t	value
							) noexcept{
	Req::Api::SetCTRL_REG5_XLPayload*
		p	=	new(&payload)
				Req::Api::SetCTRL_REG5_XLPayload(
							value
							);
	Resp::Api::SetCTRL_REG5_XLResp*
		r	=	new(&resp)
			Resp::Api::SetCTRL_REG5_XLResp(	sap.getReqApi(),
									respApi,
									clientPapi,
									*p
									);

	return *r;
	}

Resp::Api::GetCTRL_REG5_XLResp&
	Resp::GetCTRL_REG5_XLMem::build(	Req::Api::SAP&			sap,
							Resp::Api&				respApi,
							Oscl::Mt::Itc::PostMsgApi&	clientPapi
							) noexcept{
	Req::Api::GetCTRL_REG5_XLPayload*
		p	=	new(&payload)
				Req::Api::GetCTRL_REG5_XLPayload(
);
	Resp::Api::GetCTRL_REG5_XLResp*
		r	=	new(&resp)
			Resp::Api::GetCTRL_REG5_XLResp(	sap.getReqApi(),
									respApi,
									clientPapi,
									*p
									);

	return *r;
	}

Resp::Api::SetCTRL_REG4Resp&
	Resp::SetCTRL_REG4Mem::build(	Req::Api::SAP&			sap,
							Resp::Api&				respApi,
							Oscl::Mt::Itc::PostMsgApi&	clientPapi,
							uint8_t	value
							) noexcept{
	Req::Api::SetCTRL_REG4Payload*
		p	=	new(&payload)
				Req::Api::SetCTRL_REG4Payload(
							value
							);
	Resp::Api::SetCTRL_REG4Resp*
		r	=	new(&resp)
			Resp::Api::SetCTRL_REG4Resp(	sap.getReqApi(),
									respApi,
									clientPapi,
									*p
									);

	return *r;
	}

Resp::Api::GetCTRL_REG4Resp&
	Resp::GetCTRL_REG4Mem::build(	Req::Api::SAP&			sap,
							Resp::Api&				respApi,
							Oscl::Mt::Itc::PostMsgApi&	clientPapi
							) noexcept{
	Req::Api::GetCTRL_REG4Payload*
		p	=	new(&payload)
				Req::Api::GetCTRL_REG4Payload(
);
	Resp::Api::GetCTRL_REG4Resp*
		r	=	new(&resp)
			Resp::Api::GetCTRL_REG4Resp(	sap.getReqApi(),
									respApi,
									clientPapi,
									*p
									);

	return *r;
	}

Resp::Api::GetOUT_Z_H_GResp&
	Resp::GetOUT_Z_H_GMem::build(	Req::Api::SAP&			sap,
							Resp::Api&				respApi,
							Oscl::Mt::Itc::PostMsgApi&	clientPapi
							) noexcept{
	Req::Api::GetOUT_Z_H_GPayload*
		p	=	new(&payload)
				Req::Api::GetOUT_Z_H_GPayload(
);
	Resp::Api::GetOUT_Z_H_GResp*
		r	=	new(&resp)
			Resp::Api::GetOUT_Z_H_GResp(	sap.getReqApi(),
									respApi,
									clientPapi,
									*p
									);

	return *r;
	}

Resp::Api::GetOUT_Z_L_GResp&
	Resp::GetOUT_Z_L_GMem::build(	Req::Api::SAP&			sap,
							Resp::Api&				respApi,
							Oscl::Mt::Itc::PostMsgApi&	clientPapi
							) noexcept{
	Req::Api::GetOUT_Z_L_GPayload*
		p	=	new(&payload)
				Req::Api::GetOUT_Z_L_GPayload(
);
	Resp::Api::GetOUT_Z_L_GResp*
		r	=	new(&resp)
			Resp::Api::GetOUT_Z_L_GResp(	sap.getReqApi(),
									respApi,
									clientPapi,
									*p
									);

	return *r;
	}

Resp::Api::GetOUT_Y_H_GResp&
	Resp::GetOUT_Y_H_GMem::build(	Req::Api::SAP&			sap,
							Resp::Api&				respApi,
							Oscl::Mt::Itc::PostMsgApi&	clientPapi
							) noexcept{
	Req::Api::GetOUT_Y_H_GPayload*
		p	=	new(&payload)
				Req::Api::GetOUT_Y_H_GPayload(
);
	Resp::Api::GetOUT_Y_H_GResp*
		r	=	new(&resp)
			Resp::Api::GetOUT_Y_H_GResp(	sap.getReqApi(),
									respApi,
									clientPapi,
									*p
									);

	return *r;
	}

Resp::Api::GetOUT_Y_L_GResp&
	Resp::GetOUT_Y_L_GMem::build(	Req::Api::SAP&			sap,
							Resp::Api&				respApi,
							Oscl::Mt::Itc::PostMsgApi&	clientPapi
							) noexcept{
	Req::Api::GetOUT_Y_L_GPayload*
		p	=	new(&payload)
				Req::Api::GetOUT_Y_L_GPayload(
);
	Resp::Api::GetOUT_Y_L_GResp*
		r	=	new(&resp)
			Resp::Api::GetOUT_Y_L_GResp(	sap.getReqApi(),
									respApi,
									clientPapi,
									*p
									);

	return *r;
	}

Resp::Api::GetOUT_X_L_GResp&
	Resp::GetOUT_X_L_GMem::build(	Req::Api::SAP&			sap,
							Resp::Api&				respApi,
							Oscl::Mt::Itc::PostMsgApi&	clientPapi
							) noexcept{
	Req::Api::GetOUT_X_L_GPayload*
		p	=	new(&payload)
				Req::Api::GetOUT_X_L_GPayload(
);
	Resp::Api::GetOUT_X_L_GResp*
		r	=	new(&resp)
			Resp::Api::GetOUT_X_L_GResp(	sap.getReqApi(),
									respApi,
									clientPapi,
									*p
									);

	return *r;
	}

Resp::Api::GetSTATUS_REGResp&
	Resp::GetSTATUS_REGMem::build(	Req::Api::SAP&			sap,
							Resp::Api&				respApi,
							Oscl::Mt::Itc::PostMsgApi&	clientPapi
							) noexcept{
	Req::Api::GetSTATUS_REGPayload*
		p	=	new(&payload)
				Req::Api::GetSTATUS_REGPayload(
);
	Resp::Api::GetSTATUS_REGResp*
		r	=	new(&resp)
			Resp::Api::GetSTATUS_REGResp(	sap.getReqApi(),
									respApi,
									clientPapi,
									*p
									);

	return *r;
	}

Resp::Api::GetOUT_TEMP_HResp&
	Resp::GetOUT_TEMP_HMem::build(	Req::Api::SAP&			sap,
							Resp::Api&				respApi,
							Oscl::Mt::Itc::PostMsgApi&	clientPapi
							) noexcept{
	Req::Api::GetOUT_TEMP_HPayload*
		p	=	new(&payload)
				Req::Api::GetOUT_TEMP_HPayload(
);
	Resp::Api::GetOUT_TEMP_HResp*
		r	=	new(&resp)
			Resp::Api::GetOUT_TEMP_HResp(	sap.getReqApi(),
									respApi,
									clientPapi,
									*p
									);

	return *r;
	}

Resp::Api::GetOUT_TEMP_LResp&
	Resp::GetOUT_TEMP_LMem::build(	Req::Api::SAP&			sap,
							Resp::Api&				respApi,
							Oscl::Mt::Itc::PostMsgApi&	clientPapi
							) noexcept{
	Req::Api::GetOUT_TEMP_LPayload*
		p	=	new(&payload)
				Req::Api::GetOUT_TEMP_LPayload(
);
	Resp::Api::GetOUT_TEMP_LResp*
		r	=	new(&resp)
			Resp::Api::GetOUT_TEMP_LResp(	sap.getReqApi(),
									respApi,
									clientPapi,
									*p
									);

	return *r;
	}

Resp::Api::GetINT_GEN_SRC_GResp&
	Resp::GetINT_GEN_SRC_GMem::build(	Req::Api::SAP&			sap,
							Resp::Api&				respApi,
							Oscl::Mt::Itc::PostMsgApi&	clientPapi
							) noexcept{
	Req::Api::GetINT_GEN_SRC_GPayload*
		p	=	new(&payload)
				Req::Api::GetINT_GEN_SRC_GPayload(
);
	Resp::Api::GetINT_GEN_SRC_GResp*
		r	=	new(&resp)
			Resp::Api::GetINT_GEN_SRC_GResp(	sap.getReqApi(),
									respApi,
									clientPapi,
									*p
									);

	return *r;
	}

Resp::Api::SetORIENT_CFG_GResp&
	Resp::SetORIENT_CFG_GMem::build(	Req::Api::SAP&			sap,
							Resp::Api&				respApi,
							Oscl::Mt::Itc::PostMsgApi&	clientPapi,
							uint8_t	value
							) noexcept{
	Req::Api::SetORIENT_CFG_GPayload*
		p	=	new(&payload)
				Req::Api::SetORIENT_CFG_GPayload(
							value
							);
	Resp::Api::SetORIENT_CFG_GResp*
		r	=	new(&resp)
			Resp::Api::SetORIENT_CFG_GResp(	sap.getReqApi(),
									respApi,
									clientPapi,
									*p
									);

	return *r;
	}

Resp::Api::GetORIENT_CFG_GResp&
	Resp::GetORIENT_CFG_GMem::build(	Req::Api::SAP&			sap,
							Resp::Api&				respApi,
							Oscl::Mt::Itc::PostMsgApi&	clientPapi
							) noexcept{
	Req::Api::GetORIENT_CFG_GPayload*
		p	=	new(&payload)
				Req::Api::GetORIENT_CFG_GPayload(
);
	Resp::Api::GetORIENT_CFG_GResp*
		r	=	new(&resp)
			Resp::Api::GetORIENT_CFG_GResp(	sap.getReqApi(),
									respApi,
									clientPapi,
									*p
									);

	return *r;
	}

Resp::Api::SetCTRL_REG3_GResp&
	Resp::SetCTRL_REG3_GMem::build(	Req::Api::SAP&			sap,
							Resp::Api&				respApi,
							Oscl::Mt::Itc::PostMsgApi&	clientPapi,
							uint8_t	value
							) noexcept{
	Req::Api::SetCTRL_REG3_GPayload*
		p	=	new(&payload)
				Req::Api::SetCTRL_REG3_GPayload(
							value
							);
	Resp::Api::SetCTRL_REG3_GResp*
		r	=	new(&resp)
			Resp::Api::SetCTRL_REG3_GResp(	sap.getReqApi(),
									respApi,
									clientPapi,
									*p
									);

	return *r;
	}

Resp::Api::GetCTRL_REG3_GResp&
	Resp::GetCTRL_REG3_GMem::build(	Req::Api::SAP&			sap,
							Resp::Api&				respApi,
							Oscl::Mt::Itc::PostMsgApi&	clientPapi
							) noexcept{
	Req::Api::GetCTRL_REG3_GPayload*
		p	=	new(&payload)
				Req::Api::GetCTRL_REG3_GPayload(
);
	Resp::Api::GetCTRL_REG3_GResp*
		r	=	new(&resp)
			Resp::Api::GetCTRL_REG3_GResp(	sap.getReqApi(),
									respApi,
									clientPapi,
									*p
									);

	return *r;
	}

Resp::Api::SetCTRL_REG2_GResp&
	Resp::SetCTRL_REG2_GMem::build(	Req::Api::SAP&			sap,
							Resp::Api&				respApi,
							Oscl::Mt::Itc::PostMsgApi&	clientPapi,
							uint8_t	value
							) noexcept{
	Req::Api::SetCTRL_REG2_GPayload*
		p	=	new(&payload)
				Req::Api::SetCTRL_REG2_GPayload(
							value
							);
	Resp::Api::SetCTRL_REG2_GResp*
		r	=	new(&resp)
			Resp::Api::SetCTRL_REG2_GResp(	sap.getReqApi(),
									respApi,
									clientPapi,
									*p
									);

	return *r;
	}

Resp::Api::GetCTRL_REG2_GResp&
	Resp::GetCTRL_REG2_GMem::build(	Req::Api::SAP&			sap,
							Resp::Api&				respApi,
							Oscl::Mt::Itc::PostMsgApi&	clientPapi
							) noexcept{
	Req::Api::GetCTRL_REG2_GPayload*
		p	=	new(&payload)
				Req::Api::GetCTRL_REG2_GPayload(
);
	Resp::Api::GetCTRL_REG2_GResp*
		r	=	new(&resp)
			Resp::Api::GetCTRL_REG2_GResp(	sap.getReqApi(),
									respApi,
									clientPapi,
									*p
									);

	return *r;
	}

Resp::Api::SetCTRL_REG1_GResp&
	Resp::SetCTRL_REG1_GMem::build(	Req::Api::SAP&			sap,
							Resp::Api&				respApi,
							Oscl::Mt::Itc::PostMsgApi&	clientPapi,
							uint8_t	value
							) noexcept{
	Req::Api::SetCTRL_REG1_GPayload*
		p	=	new(&payload)
				Req::Api::SetCTRL_REG1_GPayload(
							value
							);
	Resp::Api::SetCTRL_REG1_GResp*
		r	=	new(&resp)
			Resp::Api::SetCTRL_REG1_GResp(	sap.getReqApi(),
									respApi,
									clientPapi,
									*p
									);

	return *r;
	}

Resp::Api::GetCTRL_REG1_GResp&
	Resp::GetCTRL_REG1_GMem::build(	Req::Api::SAP&			sap,
							Resp::Api&				respApi,
							Oscl::Mt::Itc::PostMsgApi&	clientPapi
							) noexcept{
	Req::Api::GetCTRL_REG1_GPayload*
		p	=	new(&payload)
				Req::Api::GetCTRL_REG1_GPayload(
);
	Resp::Api::GetCTRL_REG1_GResp*
		r	=	new(&resp)
			Resp::Api::GetCTRL_REG1_GResp(	sap.getReqApi(),
									respApi,
									clientPapi,
									*p
									);

	return *r;
	}

Resp::Api::GetWHO_AM_IResp&
	Resp::GetWHO_AM_IMem::build(	Req::Api::SAP&			sap,
							Resp::Api&				respApi,
							Oscl::Mt::Itc::PostMsgApi&	clientPapi
							) noexcept{
	Req::Api::GetWHO_AM_IPayload*
		p	=	new(&payload)
				Req::Api::GetWHO_AM_IPayload(
);
	Resp::Api::GetWHO_AM_IResp*
		r	=	new(&resp)
			Resp::Api::GetWHO_AM_IResp(	sap.getReqApi(),
									respApi,
									clientPapi,
									*p
									);

	return *r;
	}

Resp::Api::SetINT2_CTRLResp&
	Resp::SetINT2_CTRLMem::build(	Req::Api::SAP&			sap,
							Resp::Api&				respApi,
							Oscl::Mt::Itc::PostMsgApi&	clientPapi,
							uint8_t	value
							) noexcept{
	Req::Api::SetINT2_CTRLPayload*
		p	=	new(&payload)
				Req::Api::SetINT2_CTRLPayload(
							value
							);
	Resp::Api::SetINT2_CTRLResp*
		r	=	new(&resp)
			Resp::Api::SetINT2_CTRLResp(	sap.getReqApi(),
									respApi,
									clientPapi,
									*p
									);

	return *r;
	}

Resp::Api::GetINT2_CTRLResp&
	Resp::GetINT2_CTRLMem::build(	Req::Api::SAP&			sap,
							Resp::Api&				respApi,
							Oscl::Mt::Itc::PostMsgApi&	clientPapi
							) noexcept{
	Req::Api::GetINT2_CTRLPayload*
		p	=	new(&payload)
				Req::Api::GetINT2_CTRLPayload(
);
	Resp::Api::GetINT2_CTRLResp*
		r	=	new(&resp)
			Resp::Api::GetINT2_CTRLResp(	sap.getReqApi(),
									respApi,
									clientPapi,
									*p
									);

	return *r;
	}

Resp::Api::SetINT1_CTRLResp&
	Resp::SetINT1_CTRLMem::build(	Req::Api::SAP&			sap,
							Resp::Api&				respApi,
							Oscl::Mt::Itc::PostMsgApi&	clientPapi,
							uint8_t	value
							) noexcept{
	Req::Api::SetINT1_CTRLPayload*
		p	=	new(&payload)
				Req::Api::SetINT1_CTRLPayload(
							value
							);
	Resp::Api::SetINT1_CTRLResp*
		r	=	new(&resp)
			Resp::Api::SetINT1_CTRLResp(	sap.getReqApi(),
									respApi,
									clientPapi,
									*p
									);

	return *r;
	}

Resp::Api::GetINT1_CTRLResp&
	Resp::GetINT1_CTRLMem::build(	Req::Api::SAP&			sap,
							Resp::Api&				respApi,
							Oscl::Mt::Itc::PostMsgApi&	clientPapi
							) noexcept{
	Req::Api::GetINT1_CTRLPayload*
		p	=	new(&payload)
				Req::Api::GetINT1_CTRLPayload(
);
	Resp::Api::GetINT1_CTRLResp*
		r	=	new(&resp)
			Resp::Api::GetINT1_CTRLResp(	sap.getReqApi(),
									respApi,
									clientPapi,
									*p
									);

	return *r;
	}

Resp::Api::SetREFERENCE_GResp&
	Resp::SetREFERENCE_GMem::build(	Req::Api::SAP&			sap,
							Resp::Api&				respApi,
							Oscl::Mt::Itc::PostMsgApi&	clientPapi,
							uint8_t	value
							) noexcept{
	Req::Api::SetREFERENCE_GPayload*
		p	=	new(&payload)
				Req::Api::SetREFERENCE_GPayload(
							value
							);
	Resp::Api::SetREFERENCE_GResp*
		r	=	new(&resp)
			Resp::Api::SetREFERENCE_GResp(	sap.getReqApi(),
									respApi,
									clientPapi,
									*p
									);

	return *r;
	}

Resp::Api::GetREFERENCE_GResp&
	Resp::GetREFERENCE_GMem::build(	Req::Api::SAP&			sap,
							Resp::Api&				respApi,
							Oscl::Mt::Itc::PostMsgApi&	clientPapi
							) noexcept{
	Req::Api::GetREFERENCE_GPayload*
		p	=	new(&payload)
				Req::Api::GetREFERENCE_GPayload(
);
	Resp::Api::GetREFERENCE_GResp*
		r	=	new(&resp)
			Resp::Api::GetREFERENCE_GResp(	sap.getReqApi(),
									respApi,
									clientPapi,
									*p
									);

	return *r;
	}

Resp::Api::SetINT_GEN_DUR_XLResp&
	Resp::SetINT_GEN_DUR_XLMem::build(	Req::Api::SAP&			sap,
							Resp::Api&				respApi,
							Oscl::Mt::Itc::PostMsgApi&	clientPapi,
							uint8_t	value
							) noexcept{
	Req::Api::SetINT_GEN_DUR_XLPayload*
		p	=	new(&payload)
				Req::Api::SetINT_GEN_DUR_XLPayload(
							value
							);
	Resp::Api::SetINT_GEN_DUR_XLResp*
		r	=	new(&resp)
			Resp::Api::SetINT_GEN_DUR_XLResp(	sap.getReqApi(),
									respApi,
									clientPapi,
									*p
									);

	return *r;
	}

Resp::Api::GetINT_GEN_DUR_XLResp&
	Resp::GetINT_GEN_DUR_XLMem::build(	Req::Api::SAP&			sap,
							Resp::Api&				respApi,
							Oscl::Mt::Itc::PostMsgApi&	clientPapi
							) noexcept{
	Req::Api::GetINT_GEN_DUR_XLPayload*
		p	=	new(&payload)
				Req::Api::GetINT_GEN_DUR_XLPayload(
);
	Resp::Api::GetINT_GEN_DUR_XLResp*
		r	=	new(&resp)
			Resp::Api::GetINT_GEN_DUR_XLResp(	sap.getReqApi(),
									respApi,
									clientPapi,
									*p
									);

	return *r;
	}

Resp::Api::SetINT_GEN_THS_Z_XLResp&
	Resp::SetINT_GEN_THS_Z_XLMem::build(	Req::Api::SAP&			sap,
							Resp::Api&				respApi,
							Oscl::Mt::Itc::PostMsgApi&	clientPapi,
							uint8_t	value
							) noexcept{
	Req::Api::SetINT_GEN_THS_Z_XLPayload*
		p	=	new(&payload)
				Req::Api::SetINT_GEN_THS_Z_XLPayload(
							value
							);
	Resp::Api::SetINT_GEN_THS_Z_XLResp*
		r	=	new(&resp)
			Resp::Api::SetINT_GEN_THS_Z_XLResp(	sap.getReqApi(),
									respApi,
									clientPapi,
									*p
									);

	return *r;
	}

Resp::Api::GetINT_GEN_THS_Z_XLResp&
	Resp::GetINT_GEN_THS_Z_XLMem::build(	Req::Api::SAP&			sap,
							Resp::Api&				respApi,
							Oscl::Mt::Itc::PostMsgApi&	clientPapi
							) noexcept{
	Req::Api::GetINT_GEN_THS_Z_XLPayload*
		p	=	new(&payload)
				Req::Api::GetINT_GEN_THS_Z_XLPayload(
);
	Resp::Api::GetINT_GEN_THS_Z_XLResp*
		r	=	new(&resp)
			Resp::Api::GetINT_GEN_THS_Z_XLResp(	sap.getReqApi(),
									respApi,
									clientPapi,
									*p
									);

	return *r;
	}

Resp::Api::SetINT_GEN_THS_Y_XLResp&
	Resp::SetINT_GEN_THS_Y_XLMem::build(	Req::Api::SAP&			sap,
							Resp::Api&				respApi,
							Oscl::Mt::Itc::PostMsgApi&	clientPapi,
							uint8_t	value
							) noexcept{
	Req::Api::SetINT_GEN_THS_Y_XLPayload*
		p	=	new(&payload)
				Req::Api::SetINT_GEN_THS_Y_XLPayload(
							value
							);
	Resp::Api::SetINT_GEN_THS_Y_XLResp*
		r	=	new(&resp)
			Resp::Api::SetINT_GEN_THS_Y_XLResp(	sap.getReqApi(),
									respApi,
									clientPapi,
									*p
									);

	return *r;
	}

Resp::Api::GetINT_GEN_THS_Y_XLResp&
	Resp::GetINT_GEN_THS_Y_XLMem::build(	Req::Api::SAP&			sap,
							Resp::Api&				respApi,
							Oscl::Mt::Itc::PostMsgApi&	clientPapi
							) noexcept{
	Req::Api::GetINT_GEN_THS_Y_XLPayload*
		p	=	new(&payload)
				Req::Api::GetINT_GEN_THS_Y_XLPayload(
);
	Resp::Api::GetINT_GEN_THS_Y_XLResp*
		r	=	new(&resp)
			Resp::Api::GetINT_GEN_THS_Y_XLResp(	sap.getReqApi(),
									respApi,
									clientPapi,
									*p
									);

	return *r;
	}

Resp::Api::SetINT_GEN_THS_X_XLResp&
	Resp::SetINT_GEN_THS_X_XLMem::build(	Req::Api::SAP&			sap,
							Resp::Api&				respApi,
							Oscl::Mt::Itc::PostMsgApi&	clientPapi,
							uint8_t	value
							) noexcept{
	Req::Api::SetINT_GEN_THS_X_XLPayload*
		p	=	new(&payload)
				Req::Api::SetINT_GEN_THS_X_XLPayload(
							value
							);
	Resp::Api::SetINT_GEN_THS_X_XLResp*
		r	=	new(&resp)
			Resp::Api::SetINT_GEN_THS_X_XLResp(	sap.getReqApi(),
									respApi,
									clientPapi,
									*p
									);

	return *r;
	}

Resp::Api::GetINT_GEN_THS_X_XLResp&
	Resp::GetINT_GEN_THS_X_XLMem::build(	Req::Api::SAP&			sap,
							Resp::Api&				respApi,
							Oscl::Mt::Itc::PostMsgApi&	clientPapi
							) noexcept{
	Req::Api::GetINT_GEN_THS_X_XLPayload*
		p	=	new(&payload)
				Req::Api::GetINT_GEN_THS_X_XLPayload(
);
	Resp::Api::GetINT_GEN_THS_X_XLResp*
		r	=	new(&resp)
			Resp::Api::GetINT_GEN_THS_X_XLResp(	sap.getReqApi(),
									respApi,
									clientPapi,
									*p
									);

	return *r;
	}

Resp::Api::SetINT_GEN_CFG_XLResp&
	Resp::SetINT_GEN_CFG_XLMem::build(	Req::Api::SAP&			sap,
							Resp::Api&				respApi,
							Oscl::Mt::Itc::PostMsgApi&	clientPapi,
							uint8_t	value
							) noexcept{
	Req::Api::SetINT_GEN_CFG_XLPayload*
		p	=	new(&payload)
				Req::Api::SetINT_GEN_CFG_XLPayload(
							value
							);
	Resp::Api::SetINT_GEN_CFG_XLResp*
		r	=	new(&resp)
			Resp::Api::SetINT_GEN_CFG_XLResp(	sap.getReqApi(),
									respApi,
									clientPapi,
									*p
									);

	return *r;
	}

Resp::Api::GetINT_GEN_CFG_XLResp&
	Resp::GetINT_GEN_CFG_XLMem::build(	Req::Api::SAP&			sap,
							Resp::Api&				respApi,
							Oscl::Mt::Itc::PostMsgApi&	clientPapi
							) noexcept{
	Req::Api::GetINT_GEN_CFG_XLPayload*
		p	=	new(&payload)
				Req::Api::GetINT_GEN_CFG_XLPayload(
);
	Resp::Api::GetINT_GEN_CFG_XLResp*
		r	=	new(&resp)
			Resp::Api::GetINT_GEN_CFG_XLResp(	sap.getReqApi(),
									respApi,
									clientPapi,
									*p
									);

	return *r;
	}

Resp::Api::SetACT_DURResp&
	Resp::SetACT_DURMem::build(	Req::Api::SAP&			sap,
							Resp::Api&				respApi,
							Oscl::Mt::Itc::PostMsgApi&	clientPapi,
							uint8_t	value
							) noexcept{
	Req::Api::SetACT_DURPayload*
		p	=	new(&payload)
				Req::Api::SetACT_DURPayload(
							value
							);
	Resp::Api::SetACT_DURResp*
		r	=	new(&resp)
			Resp::Api::SetACT_DURResp(	sap.getReqApi(),
									respApi,
									clientPapi,
									*p
									);

	return *r;
	}

Resp::Api::GetACT_DURResp&
	Resp::GetACT_DURMem::build(	Req::Api::SAP&			sap,
							Resp::Api&				respApi,
							Oscl::Mt::Itc::PostMsgApi&	clientPapi
							) noexcept{
	Req::Api::GetACT_DURPayload*
		p	=	new(&payload)
				Req::Api::GetACT_DURPayload(
);
	Resp::Api::GetACT_DURResp*
		r	=	new(&resp)
			Resp::Api::GetACT_DURResp(	sap.getReqApi(),
									respApi,
									clientPapi,
									*p
									);

	return *r;
	}

Resp::Api::SetACT_THSResp&
	Resp::SetACT_THSMem::build(	Req::Api::SAP&			sap,
							Resp::Api&				respApi,
							Oscl::Mt::Itc::PostMsgApi&	clientPapi,
							uint8_t	value
							) noexcept{
	Req::Api::SetACT_THSPayload*
		p	=	new(&payload)
				Req::Api::SetACT_THSPayload(
							value
							);
	Resp::Api::SetACT_THSResp*
		r	=	new(&resp)
			Resp::Api::SetACT_THSResp(	sap.getReqApi(),
									respApi,
									clientPapi,
									*p
									);

	return *r;
	}

Resp::Api::GetACT_THSResp&
	Resp::GetACT_THSMem::build(	Req::Api::SAP&			sap,
							Resp::Api&				respApi,
							Oscl::Mt::Itc::PostMsgApi&	clientPapi
							) noexcept{
	Req::Api::GetACT_THSPayload*
		p	=	new(&payload)
				Req::Api::GetACT_THSPayload(
);
	Resp::Api::GetACT_THSResp*
		r	=	new(&resp)
			Resp::Api::GetACT_THSResp(	sap.getReqApi(),
									respApi,
									clientPapi,
									*p
									);

	return *r;
	}

