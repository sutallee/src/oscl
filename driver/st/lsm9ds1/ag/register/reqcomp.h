/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_driver_st_lsm9ds1_ag_register_reqcomph_
#define _oscl_driver_st_lsm9ds1_ag_register_reqcomph_

#include "reqapi.h"

/** */
namespace Oscl {

/** */
namespace Driver {

/** */
namespace ST {

/** */
namespace LSM9DS1 {

/** */
namespace AG {

/** */
namespace Register {

/** */
namespace Req {

/**	This template class implements a kind of GOF decorator
	pattern allows the context to employ composition instead
	of inheritance. Frequently, a context may need to implement
	more than one interface that may result in method name
	clashes. This template solves the problem by implementing
	the interface and then invoking member function pointers
	in the context instead. One instance of this object is
	created in the context for each interface of this type used
	in the context. Each instance is constructed such that it
	refers to different member functions within the context.
	The interface of this object is passed to any object that
	requires the interface. When the object invokes any of the
	operations of the API, the corresponding member function
	of the context will subsequently be invoked.
 */
/**	This interface defines methods to access the LSM9DS1 IMU
	registers indpendent of the type of bus.
 */
template <class Context>
class Composer : public Api {
	private:
		/** A reference to the Context.
		 */
		Context&	_context;

	private:
		/**	Read the OUT_TEMP registers.
		 */
		void	(Context::*_GetOUT_TEMP)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetOUT_TEMPReq& msg);

		/**	Read the OUT_<axis>_<octet>_G registers.
		 */
		void	(Context::*_GetOUT_G)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetOUT_GReq& msg);

		/**	Read the OUT_<axis>_<octet>_XL registers.
		 */
		void	(Context::*_GetOUT_XL)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetOUT_XLReq& msg);

		/**	Write the INT_GEN_DUR_G register
		 */
		void	(Context::*_SetINT_GEN_DUR_G)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SetINT_GEN_DUR_GReq& msg);

		/**	Read the INT_GEN_DUR_G register.
		 */
		void	(Context::*_GetINT_GEN_DUR_G)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetINT_GEN_DUR_GReq& msg);

		/**	Write the INT_GEN_THS_ZL_G register
		 */
		void	(Context::*_SetINT_GEN_THS_ZL_G)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SetINT_GEN_THS_ZL_GReq& msg);

		/**	Read the INT_GEN_THS_ZL_G register.
		 */
		void	(Context::*_GetINT_GEN_THS_ZL_G)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetINT_GEN_THS_ZL_GReq& msg);

		/**	Write the INT_GEN_THS_ZH_G register
		 */
		void	(Context::*_SetINT_GEN_THS_ZH_G)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SetINT_GEN_THS_ZH_GReq& msg);

		/**	Read the INT_GEN_THS_ZH_G register.
		 */
		void	(Context::*_GetINT_GEN_THS_ZH_G)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetINT_GEN_THS_ZH_GReq& msg);

		/**	Write the INT_GEN_THS_YL_G register
		 */
		void	(Context::*_SetINT_GEN_THS_YL_G)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SetINT_GEN_THS_YL_GReq& msg);

		/**	Read the INT_GEN_THS_YL_G register.
		 */
		void	(Context::*_GetINT_GEN_THS_YL_G)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetINT_GEN_THS_YL_GReq& msg);

		/**	Write the INT_GEN_THS_YH_G register
		 */
		void	(Context::*_SetINT_GEN_THS_YH_G)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SetINT_GEN_THS_YH_GReq& msg);

		/**	Read the INT_GEN_THS_YH_G register.
		 */
		void	(Context::*_GetINT_GEN_THS_YH_G)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetINT_GEN_THS_YH_GReq& msg);

		/**	Write the INT_GEN_THS_XL_G register
		 */
		void	(Context::*_SetINT_GEN_THS_XL_G)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SetINT_GEN_THS_XL_GReq& msg);

		/**	Read the INT_GEN_THS_XL_G register.
		 */
		void	(Context::*_GetINT_GEN_THS_XL_G)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetINT_GEN_THS_XL_GReq& msg);

		/**	Write the INT_GEN_THS_XH_G register
		 */
		void	(Context::*_SetINT_GEN_THS_XH_G)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SetINT_GEN_THS_XH_GReq& msg);

		/**	Read the INT_GEN_THS_XH_G register.
		 */
		void	(Context::*_GetINT_GEN_THS_XH_G)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetINT_GEN_THS_XH_GReq& msg);

		/**	Write the INT_GEN_CFG_G register
		 */
		void	(Context::*_SetINT_GEN_CFG_G)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SetINT_GEN_CFG_GReq& msg);

		/**	Read the INT_GEN_CFG_G register.
		 */
		void	(Context::*_GetINT_GEN_CFG_G)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetINT_GEN_CFG_GReq& msg);

		/**	Read the FIFO_SRC register.
		 */
		void	(Context::*_GetFIFO_SRC)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetFIFO_SRCReq& msg);

		/**	Write the FIFO_CTRL register
		 */
		void	(Context::*_SetFIFO_CTRL)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SetFIFO_CTRLReq& msg);

		/**	Read the FIFO_CTRL register.
		 */
		void	(Context::*_GetFIFO_CTRL)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetFIFO_CTRLReq& msg);

		/**	Read the OUT_Z_H_XL register.
		 */
		void	(Context::*_GetOUT_Z_H_XL)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetOUT_Z_H_XLReq& msg);

		/**	Read the OUT_Z_L_XL register.
		 */
		void	(Context::*_GetOUT_Z_L_XL)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetOUT_Z_L_XLReq& msg);

		/**	Read the OUT_Y_H_XL register.
		 */
		void	(Context::*_GetOUT_Y_H_XL)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetOUT_Y_H_XLReq& msg);

		/**	Read the OUT_Y_L_XL register.
		 */
		void	(Context::*_GetOUT_Y_L_XL)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetOUT_Y_L_XLReq& msg);

		/**	Read the OUT_X_H_XL register.
		 */
		void	(Context::*_GetOUT_X_H_XL)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetOUT_X_H_XLReq& msg);

		/**	Read the OUT_X_L_XL register.
		 */
		void	(Context::*_GetOUT_X_L_XL)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetOUT_X_L_XLReq& msg);

		/**	Read the STATUS_REG_2 register.
		 */
		void	(Context::*_GetSTATUS_REG_2)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetSTATUS_REG_2Req& msg);

		/**	Read the INT_GEN_SRC_XL register.
		 */
		void	(Context::*_GetINT_GEN_SRC_XL)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetINT_GEN_SRC_XLReq& msg);

		/**	Write the CTRL_REG10 register
		 */
		void	(Context::*_SetCTRL_REG10)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SetCTRL_REG10Req& msg);

		/**	Read the CTRL_REG10 register.
		 */
		void	(Context::*_GetCTRL_REG10)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetCTRL_REG10Req& msg);

		/**	Write the CTRL_REG9 register
		 */
		void	(Context::*_SetCTRL_REG9)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SetCTRL_REG9Req& msg);

		/**	Read the CTRL_REG9 register.
		 */
		void	(Context::*_GetCTRL_REG9)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetCTRL_REG9Req& msg);

		/**	Write the CTRL_REG8 register
		 */
		void	(Context::*_SetCTRL_REG8)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SetCTRL_REG8Req& msg);

		/**	Read the CTRL_REG8 register.
		 */
		void	(Context::*_GetCTRL_REG8)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetCTRL_REG8Req& msg);

		/**	Write the CTRL_REG7_XL register
		 */
		void	(Context::*_SetCTRL_REG7_XL)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SetCTRL_REG7_XLReq& msg);

		/**	Read the CTRL_REG7_XL register.
		 */
		void	(Context::*_GetCTRL_REG7_XL)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetCTRL_REG7_XLReq& msg);

		/**	Write the CTRL_REG6_XL register
		 */
		void	(Context::*_SetCTRL_REG6_XL)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SetCTRL_REG6_XLReq& msg);

		/**	Read the CTRL_REG6_XL register.
		 */
		void	(Context::*_GetCTRL_REG6_XL)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetCTRL_REG6_XLReq& msg);

		/**	Write the CTRL_REG5_XL register
		 */
		void	(Context::*_SetCTRL_REG5_XL)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SetCTRL_REG5_XLReq& msg);

		/**	Read the CTRL_REG5_XL register.
		 */
		void	(Context::*_GetCTRL_REG5_XL)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetCTRL_REG5_XLReq& msg);

		/**	Write the CTRL_REG4 register
		 */
		void	(Context::*_SetCTRL_REG4)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SetCTRL_REG4Req& msg);

		/**	Read the CTRL_REG4 register.
		 */
		void	(Context::*_GetCTRL_REG4)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetCTRL_REG4Req& msg);

		/**	Read the OUT_Z_H_G register.
		 */
		void	(Context::*_GetOUT_Z_H_G)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetOUT_Z_H_GReq& msg);

		/**	Read the OUT_Z_L_G register.
		 */
		void	(Context::*_GetOUT_Z_L_G)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetOUT_Z_L_GReq& msg);

		/**	Read the OUT_Y_H_G register.
		 */
		void	(Context::*_GetOUT_Y_H_G)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetOUT_Y_H_GReq& msg);

		/**	Read the OUT_Y_L_G register
		 */
		void	(Context::*_GetOUT_Y_L_G)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetOUT_Y_L_GReq& msg);

		/**	Read the OUT_X_L_G register
		 */
		void	(Context::*_GetOUT_X_L_G)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetOUT_X_L_GReq& msg);

		/**	Read the STATUS_REG register
		 */
		void	(Context::*_GetSTATUS_REG)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetSTATUS_REGReq& msg);

		/**	Read the OUT_TEMP_H register.
		 */
		void	(Context::*_GetOUT_TEMP_H)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetOUT_TEMP_HReq& msg);

		/**	Read the OUT_TEMP_L register.
		 */
		void	(Context::*_GetOUT_TEMP_L)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetOUT_TEMP_LReq& msg);

		/**	Read the INT_GEN_SRC_G register.
		 */
		void	(Context::*_GetINT_GEN_SRC_G)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetINT_GEN_SRC_GReq& msg);

		/**	Write the ORIENT_CFG_G register
		 */
		void	(Context::*_SetORIENT_CFG_G)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SetORIENT_CFG_GReq& msg);

		/**	Read the ORIENT_CFG_G register.
		 */
		void	(Context::*_GetORIENT_CFG_G)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetORIENT_CFG_GReq& msg);

		/**	Write the CTRL_REG3_G register
		 */
		void	(Context::*_SetCTRL_REG3_G)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SetCTRL_REG3_GReq& msg);

		/**	Read the CTRL_REG3_G register.
		 */
		void	(Context::*_GetCTRL_REG3_G)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetCTRL_REG3_GReq& msg);

		/**	Write the CTRL_REG2_G register
		 */
		void	(Context::*_SetCTRL_REG2_G)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SetCTRL_REG2_GReq& msg);

		/**	Read the CTRL_REG2_G register.
		 */
		void	(Context::*_GetCTRL_REG2_G)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetCTRL_REG2_GReq& msg);

		/**	Write the CTRL_REG1_G register
		 */
		void	(Context::*_SetCTRL_REG1_G)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SetCTRL_REG1_GReq& msg);

		/**	Read the CTRL_REG1_G register.
		 */
		void	(Context::*_GetCTRL_REG1_G)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetCTRL_REG1_GReq& msg);

		/**	Read the WHO_AM_I register.
		 */
		void	(Context::*_GetWHO_AM_I)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetWHO_AM_IReq& msg);

		/**	Write the INT2_CTRL register
		 */
		void	(Context::*_SetINT2_CTRL)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SetINT2_CTRLReq& msg);

		/**	Read the INT2_CTRL register.
		 */
		void	(Context::*_GetINT2_CTRL)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetINT2_CTRLReq& msg);

		/**	Write the INT1_CTRL register
		 */
		void	(Context::*_SetINT1_CTRL)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SetINT1_CTRLReq& msg);

		/**	Read the INT1_CTRL register.
		 */
		void	(Context::*_GetINT1_CTRL)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetINT1_CTRLReq& msg);

		/**	Write the REFERENCE_G register
		 */
		void	(Context::*_SetREFERENCE_G)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SetREFERENCE_GReq& msg);

		/**	Read the REFERENCE_G register.
		 */
		void	(Context::*_GetREFERENCE_G)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetREFERENCE_GReq& msg);

		/**	Write the INT_GEN_DUR_XL register
		 */
		void	(Context::*_SetINT_GEN_DUR_XL)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SetINT_GEN_DUR_XLReq& msg);

		/**	Read the INT_GEN_DUR_XL register.
		 */
		void	(Context::*_GetINT_GEN_DUR_XL)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetINT_GEN_DUR_XLReq& msg);

		/**	Write the INT_GEN_THS_Z_XL register
		 */
		void	(Context::*_SetINT_GEN_THS_Z_XL)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SetINT_GEN_THS_Z_XLReq& msg);

		/**	Read the INT_GEN_THS_Z_XL register.
		 */
		void	(Context::*_GetINT_GEN_THS_Z_XL)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetINT_GEN_THS_Z_XLReq& msg);

		/**	Write the INT_GEN_THS_Y_XL register
		 */
		void	(Context::*_SetINT_GEN_THS_Y_XL)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SetINT_GEN_THS_Y_XLReq& msg);

		/**	Read the INT_GEN_THS_Y_XL register.
		 */
		void	(Context::*_GetINT_GEN_THS_Y_XL)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetINT_GEN_THS_Y_XLReq& msg);

		/**	Write the INT_GEN_THS_X_XL register
		 */
		void	(Context::*_SetINT_GEN_THS_X_XL)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SetINT_GEN_THS_X_XLReq& msg);

		/**	Read the INT_GEN_THS_X_XL register.
		 */
		void	(Context::*_GetINT_GEN_THS_X_XL)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetINT_GEN_THS_X_XLReq& msg);

		/**	Write the INT_GEN_CFG_XL register
		 */
		void	(Context::*_SetINT_GEN_CFG_XL)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SetINT_GEN_CFG_XLReq& msg);

		/**	Read the INT_GEN_CFG_XL register.
		 */
		void	(Context::*_GetINT_GEN_CFG_XL)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetINT_GEN_CFG_XLReq& msg);

		/**	Write the ACT_DUR register
		 */
		void	(Context::*_SetACT_DUR)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SetACT_DURReq& msg);

		/**	Read the ACT_DUR register.
		 */
		void	(Context::*_GetACT_DUR)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetACT_DURReq& msg);

		/**	Write the ACT_THS register
		 */
		void	(Context::*_SetACT_THS)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SetACT_THSReq& msg);

		/**	Read the ACT_THS register.
		 */
		void	(Context::*_GetACT_THS)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetACT_THSReq& msg);

	public:
		/** */
		Composer(
			Context&		context,
			void	(Context::*GetOUT_TEMP)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetOUT_TEMPReq& msg),
			void	(Context::*GetOUT_G)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetOUT_GReq& msg),
			void	(Context::*GetOUT_XL)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetOUT_XLReq& msg),
			void	(Context::*SetINT_GEN_DUR_G)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SetINT_GEN_DUR_GReq& msg),
			void	(Context::*GetINT_GEN_DUR_G)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetINT_GEN_DUR_GReq& msg),
			void	(Context::*SetINT_GEN_THS_ZL_G)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SetINT_GEN_THS_ZL_GReq& msg),
			void	(Context::*GetINT_GEN_THS_ZL_G)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetINT_GEN_THS_ZL_GReq& msg),
			void	(Context::*SetINT_GEN_THS_ZH_G)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SetINT_GEN_THS_ZH_GReq& msg),
			void	(Context::*GetINT_GEN_THS_ZH_G)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetINT_GEN_THS_ZH_GReq& msg),
			void	(Context::*SetINT_GEN_THS_YL_G)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SetINT_GEN_THS_YL_GReq& msg),
			void	(Context::*GetINT_GEN_THS_YL_G)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetINT_GEN_THS_YL_GReq& msg),
			void	(Context::*SetINT_GEN_THS_YH_G)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SetINT_GEN_THS_YH_GReq& msg),
			void	(Context::*GetINT_GEN_THS_YH_G)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetINT_GEN_THS_YH_GReq& msg),
			void	(Context::*SetINT_GEN_THS_XL_G)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SetINT_GEN_THS_XL_GReq& msg),
			void	(Context::*GetINT_GEN_THS_XL_G)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetINT_GEN_THS_XL_GReq& msg),
			void	(Context::*SetINT_GEN_THS_XH_G)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SetINT_GEN_THS_XH_GReq& msg),
			void	(Context::*GetINT_GEN_THS_XH_G)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetINT_GEN_THS_XH_GReq& msg),
			void	(Context::*SetINT_GEN_CFG_G)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SetINT_GEN_CFG_GReq& msg),
			void	(Context::*GetINT_GEN_CFG_G)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetINT_GEN_CFG_GReq& msg),
			void	(Context::*GetFIFO_SRC)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetFIFO_SRCReq& msg),
			void	(Context::*SetFIFO_CTRL)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SetFIFO_CTRLReq& msg),
			void	(Context::*GetFIFO_CTRL)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetFIFO_CTRLReq& msg),
			void	(Context::*GetOUT_Z_H_XL)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetOUT_Z_H_XLReq& msg),
			void	(Context::*GetOUT_Z_L_XL)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetOUT_Z_L_XLReq& msg),
			void	(Context::*GetOUT_Y_H_XL)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetOUT_Y_H_XLReq& msg),
			void	(Context::*GetOUT_Y_L_XL)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetOUT_Y_L_XLReq& msg),
			void	(Context::*GetOUT_X_H_XL)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetOUT_X_H_XLReq& msg),
			void	(Context::*GetOUT_X_L_XL)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetOUT_X_L_XLReq& msg),
			void	(Context::*GetSTATUS_REG_2)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetSTATUS_REG_2Req& msg),
			void	(Context::*GetINT_GEN_SRC_XL)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetINT_GEN_SRC_XLReq& msg),
			void	(Context::*SetCTRL_REG10)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SetCTRL_REG10Req& msg),
			void	(Context::*GetCTRL_REG10)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetCTRL_REG10Req& msg),
			void	(Context::*SetCTRL_REG9)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SetCTRL_REG9Req& msg),
			void	(Context::*GetCTRL_REG9)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetCTRL_REG9Req& msg),
			void	(Context::*SetCTRL_REG8)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SetCTRL_REG8Req& msg),
			void	(Context::*GetCTRL_REG8)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetCTRL_REG8Req& msg),
			void	(Context::*SetCTRL_REG7_XL)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SetCTRL_REG7_XLReq& msg),
			void	(Context::*GetCTRL_REG7_XL)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetCTRL_REG7_XLReq& msg),
			void	(Context::*SetCTRL_REG6_XL)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SetCTRL_REG6_XLReq& msg),
			void	(Context::*GetCTRL_REG6_XL)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetCTRL_REG6_XLReq& msg),
			void	(Context::*SetCTRL_REG5_XL)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SetCTRL_REG5_XLReq& msg),
			void	(Context::*GetCTRL_REG5_XL)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetCTRL_REG5_XLReq& msg),
			void	(Context::*SetCTRL_REG4)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SetCTRL_REG4Req& msg),
			void	(Context::*GetCTRL_REG4)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetCTRL_REG4Req& msg),
			void	(Context::*GetOUT_Z_H_G)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetOUT_Z_H_GReq& msg),
			void	(Context::*GetOUT_Z_L_G)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetOUT_Z_L_GReq& msg),
			void	(Context::*GetOUT_Y_H_G)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetOUT_Y_H_GReq& msg),
			void	(Context::*GetOUT_Y_L_G)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetOUT_Y_L_GReq& msg),
			void	(Context::*GetOUT_X_L_G)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetOUT_X_L_GReq& msg),
			void	(Context::*GetSTATUS_REG)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetSTATUS_REGReq& msg),
			void	(Context::*GetOUT_TEMP_H)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetOUT_TEMP_HReq& msg),
			void	(Context::*GetOUT_TEMP_L)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetOUT_TEMP_LReq& msg),
			void	(Context::*GetINT_GEN_SRC_G)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetINT_GEN_SRC_GReq& msg),
			void	(Context::*SetORIENT_CFG_G)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SetORIENT_CFG_GReq& msg),
			void	(Context::*GetORIENT_CFG_G)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetORIENT_CFG_GReq& msg),
			void	(Context::*SetCTRL_REG3_G)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SetCTRL_REG3_GReq& msg),
			void	(Context::*GetCTRL_REG3_G)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetCTRL_REG3_GReq& msg),
			void	(Context::*SetCTRL_REG2_G)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SetCTRL_REG2_GReq& msg),
			void	(Context::*GetCTRL_REG2_G)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetCTRL_REG2_GReq& msg),
			void	(Context::*SetCTRL_REG1_G)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SetCTRL_REG1_GReq& msg),
			void	(Context::*GetCTRL_REG1_G)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetCTRL_REG1_GReq& msg),
			void	(Context::*GetWHO_AM_I)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetWHO_AM_IReq& msg),
			void	(Context::*SetINT2_CTRL)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SetINT2_CTRLReq& msg),
			void	(Context::*GetINT2_CTRL)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetINT2_CTRLReq& msg),
			void	(Context::*SetINT1_CTRL)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SetINT1_CTRLReq& msg),
			void	(Context::*GetINT1_CTRL)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetINT1_CTRLReq& msg),
			void	(Context::*SetREFERENCE_G)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SetREFERENCE_GReq& msg),
			void	(Context::*GetREFERENCE_G)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetREFERENCE_GReq& msg),
			void	(Context::*SetINT_GEN_DUR_XL)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SetINT_GEN_DUR_XLReq& msg),
			void	(Context::*GetINT_GEN_DUR_XL)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetINT_GEN_DUR_XLReq& msg),
			void	(Context::*SetINT_GEN_THS_Z_XL)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SetINT_GEN_THS_Z_XLReq& msg),
			void	(Context::*GetINT_GEN_THS_Z_XL)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetINT_GEN_THS_Z_XLReq& msg),
			void	(Context::*SetINT_GEN_THS_Y_XL)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SetINT_GEN_THS_Y_XLReq& msg),
			void	(Context::*GetINT_GEN_THS_Y_XL)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetINT_GEN_THS_Y_XLReq& msg),
			void	(Context::*SetINT_GEN_THS_X_XL)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SetINT_GEN_THS_X_XLReq& msg),
			void	(Context::*GetINT_GEN_THS_X_XL)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetINT_GEN_THS_X_XLReq& msg),
			void	(Context::*SetINT_GEN_CFG_XL)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SetINT_GEN_CFG_XLReq& msg),
			void	(Context::*GetINT_GEN_CFG_XL)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetINT_GEN_CFG_XLReq& msg),
			void	(Context::*SetACT_DUR)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SetACT_DURReq& msg),
			void	(Context::*GetACT_DUR)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetACT_DURReq& msg),
			void	(Context::*SetACT_THS)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SetACT_THSReq& msg),
			void	(Context::*GetACT_THS)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetACT_THSReq& msg)
			) noexcept;

	private:
		/**	Read the OUT_TEMP registers.
		 */
		void	request(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetOUT_TEMPReq& msg) noexcept;

		/**	Read the OUT_<axis>_<octet>_G registers.
		 */
		void	request(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetOUT_GReq& msg) noexcept;

		/**	Read the OUT_<axis>_<octet>_XL registers.
		 */
		void	request(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetOUT_XLReq& msg) noexcept;

		/**	Write the INT_GEN_DUR_G register
		 */
		void	request(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SetINT_GEN_DUR_GReq& msg) noexcept;

		/**	Read the INT_GEN_DUR_G register.
		 */
		void	request(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetINT_GEN_DUR_GReq& msg) noexcept;

		/**	Write the INT_GEN_THS_ZL_G register
		 */
		void	request(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SetINT_GEN_THS_ZL_GReq& msg) noexcept;

		/**	Read the INT_GEN_THS_ZL_G register.
		 */
		void	request(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetINT_GEN_THS_ZL_GReq& msg) noexcept;

		/**	Write the INT_GEN_THS_ZH_G register
		 */
		void	request(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SetINT_GEN_THS_ZH_GReq& msg) noexcept;

		/**	Read the INT_GEN_THS_ZH_G register.
		 */
		void	request(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetINT_GEN_THS_ZH_GReq& msg) noexcept;

		/**	Write the INT_GEN_THS_YL_G register
		 */
		void	request(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SetINT_GEN_THS_YL_GReq& msg) noexcept;

		/**	Read the INT_GEN_THS_YL_G register.
		 */
		void	request(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetINT_GEN_THS_YL_GReq& msg) noexcept;

		/**	Write the INT_GEN_THS_YH_G register
		 */
		void	request(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SetINT_GEN_THS_YH_GReq& msg) noexcept;

		/**	Read the INT_GEN_THS_YH_G register.
		 */
		void	request(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetINT_GEN_THS_YH_GReq& msg) noexcept;

		/**	Write the INT_GEN_THS_XL_G register
		 */
		void	request(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SetINT_GEN_THS_XL_GReq& msg) noexcept;

		/**	Read the INT_GEN_THS_XL_G register.
		 */
		void	request(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetINT_GEN_THS_XL_GReq& msg) noexcept;

		/**	Write the INT_GEN_THS_XH_G register
		 */
		void	request(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SetINT_GEN_THS_XH_GReq& msg) noexcept;

		/**	Read the INT_GEN_THS_XH_G register.
		 */
		void	request(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetINT_GEN_THS_XH_GReq& msg) noexcept;

		/**	Write the INT_GEN_CFG_G register
		 */
		void	request(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SetINT_GEN_CFG_GReq& msg) noexcept;

		/**	Read the INT_GEN_CFG_G register.
		 */
		void	request(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetINT_GEN_CFG_GReq& msg) noexcept;

		/**	Read the FIFO_SRC register.
		 */
		void	request(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetFIFO_SRCReq& msg) noexcept;

		/**	Write the FIFO_CTRL register
		 */
		void	request(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SetFIFO_CTRLReq& msg) noexcept;

		/**	Read the FIFO_CTRL register.
		 */
		void	request(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetFIFO_CTRLReq& msg) noexcept;

		/**	Read the OUT_Z_H_XL register.
		 */
		void	request(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetOUT_Z_H_XLReq& msg) noexcept;

		/**	Read the OUT_Z_L_XL register.
		 */
		void	request(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetOUT_Z_L_XLReq& msg) noexcept;

		/**	Read the OUT_Y_H_XL register.
		 */
		void	request(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetOUT_Y_H_XLReq& msg) noexcept;

		/**	Read the OUT_Y_L_XL register.
		 */
		void	request(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetOUT_Y_L_XLReq& msg) noexcept;

		/**	Read the OUT_X_H_XL register.
		 */
		void	request(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetOUT_X_H_XLReq& msg) noexcept;

		/**	Read the OUT_X_L_XL register.
		 */
		void	request(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetOUT_X_L_XLReq& msg) noexcept;

		/**	Read the STATUS_REG_2 register.
		 */
		void	request(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetSTATUS_REG_2Req& msg) noexcept;

		/**	Read the INT_GEN_SRC_XL register.
		 */
		void	request(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetINT_GEN_SRC_XLReq& msg) noexcept;

		/**	Write the CTRL_REG10 register
		 */
		void	request(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SetCTRL_REG10Req& msg) noexcept;

		/**	Read the CTRL_REG10 register.
		 */
		void	request(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetCTRL_REG10Req& msg) noexcept;

		/**	Write the CTRL_REG9 register
		 */
		void	request(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SetCTRL_REG9Req& msg) noexcept;

		/**	Read the CTRL_REG9 register.
		 */
		void	request(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetCTRL_REG9Req& msg) noexcept;

		/**	Write the CTRL_REG8 register
		 */
		void	request(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SetCTRL_REG8Req& msg) noexcept;

		/**	Read the CTRL_REG8 register.
		 */
		void	request(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetCTRL_REG8Req& msg) noexcept;

		/**	Write the CTRL_REG7_XL register
		 */
		void	request(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SetCTRL_REG7_XLReq& msg) noexcept;

		/**	Read the CTRL_REG7_XL register.
		 */
		void	request(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetCTRL_REG7_XLReq& msg) noexcept;

		/**	Write the CTRL_REG6_XL register
		 */
		void	request(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SetCTRL_REG6_XLReq& msg) noexcept;

		/**	Read the CTRL_REG6_XL register.
		 */
		void	request(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetCTRL_REG6_XLReq& msg) noexcept;

		/**	Write the CTRL_REG5_XL register
		 */
		void	request(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SetCTRL_REG5_XLReq& msg) noexcept;

		/**	Read the CTRL_REG5_XL register.
		 */
		void	request(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetCTRL_REG5_XLReq& msg) noexcept;

		/**	Write the CTRL_REG4 register
		 */
		void	request(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SetCTRL_REG4Req& msg) noexcept;

		/**	Read the CTRL_REG4 register.
		 */
		void	request(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetCTRL_REG4Req& msg) noexcept;

		/**	Read the OUT_Z_H_G register.
		 */
		void	request(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetOUT_Z_H_GReq& msg) noexcept;

		/**	Read the OUT_Z_L_G register.
		 */
		void	request(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetOUT_Z_L_GReq& msg) noexcept;

		/**	Read the OUT_Y_H_G register.
		 */
		void	request(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetOUT_Y_H_GReq& msg) noexcept;

		/**	Read the OUT_Y_L_G register
		 */
		void	request(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetOUT_Y_L_GReq& msg) noexcept;

		/**	Read the OUT_X_L_G register
		 */
		void	request(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetOUT_X_L_GReq& msg) noexcept;

		/**	Read the STATUS_REG register
		 */
		void	request(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetSTATUS_REGReq& msg) noexcept;

		/**	Read the OUT_TEMP_H register.
		 */
		void	request(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetOUT_TEMP_HReq& msg) noexcept;

		/**	Read the OUT_TEMP_L register.
		 */
		void	request(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetOUT_TEMP_LReq& msg) noexcept;

		/**	Read the INT_GEN_SRC_G register.
		 */
		void	request(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetINT_GEN_SRC_GReq& msg) noexcept;

		/**	Write the ORIENT_CFG_G register
		 */
		void	request(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SetORIENT_CFG_GReq& msg) noexcept;

		/**	Read the ORIENT_CFG_G register.
		 */
		void	request(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetORIENT_CFG_GReq& msg) noexcept;

		/**	Write the CTRL_REG3_G register
		 */
		void	request(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SetCTRL_REG3_GReq& msg) noexcept;

		/**	Read the CTRL_REG3_G register.
		 */
		void	request(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetCTRL_REG3_GReq& msg) noexcept;

		/**	Write the CTRL_REG2_G register
		 */
		void	request(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SetCTRL_REG2_GReq& msg) noexcept;

		/**	Read the CTRL_REG2_G register.
		 */
		void	request(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetCTRL_REG2_GReq& msg) noexcept;

		/**	Write the CTRL_REG1_G register
		 */
		void	request(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SetCTRL_REG1_GReq& msg) noexcept;

		/**	Read the CTRL_REG1_G register.
		 */
		void	request(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetCTRL_REG1_GReq& msg) noexcept;

		/**	Read the WHO_AM_I register.
		 */
		void	request(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetWHO_AM_IReq& msg) noexcept;

		/**	Write the INT2_CTRL register
		 */
		void	request(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SetINT2_CTRLReq& msg) noexcept;

		/**	Read the INT2_CTRL register.
		 */
		void	request(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetINT2_CTRLReq& msg) noexcept;

		/**	Write the INT1_CTRL register
		 */
		void	request(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SetINT1_CTRLReq& msg) noexcept;

		/**	Read the INT1_CTRL register.
		 */
		void	request(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetINT1_CTRLReq& msg) noexcept;

		/**	Write the REFERENCE_G register
		 */
		void	request(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SetREFERENCE_GReq& msg) noexcept;

		/**	Read the REFERENCE_G register.
		 */
		void	request(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetREFERENCE_GReq& msg) noexcept;

		/**	Write the INT_GEN_DUR_XL register
		 */
		void	request(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SetINT_GEN_DUR_XLReq& msg) noexcept;

		/**	Read the INT_GEN_DUR_XL register.
		 */
		void	request(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetINT_GEN_DUR_XLReq& msg) noexcept;

		/**	Write the INT_GEN_THS_Z_XL register
		 */
		void	request(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SetINT_GEN_THS_Z_XLReq& msg) noexcept;

		/**	Read the INT_GEN_THS_Z_XL register.
		 */
		void	request(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetINT_GEN_THS_Z_XLReq& msg) noexcept;

		/**	Write the INT_GEN_THS_Y_XL register
		 */
		void	request(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SetINT_GEN_THS_Y_XLReq& msg) noexcept;

		/**	Read the INT_GEN_THS_Y_XL register.
		 */
		void	request(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetINT_GEN_THS_Y_XLReq& msg) noexcept;

		/**	Write the INT_GEN_THS_X_XL register
		 */
		void	request(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SetINT_GEN_THS_X_XLReq& msg) noexcept;

		/**	Read the INT_GEN_THS_X_XL register.
		 */
		void	request(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetINT_GEN_THS_X_XLReq& msg) noexcept;

		/**	Write the INT_GEN_CFG_XL register
		 */
		void	request(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SetINT_GEN_CFG_XLReq& msg) noexcept;

		/**	Read the INT_GEN_CFG_XL register.
		 */
		void	request(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetINT_GEN_CFG_XLReq& msg) noexcept;

		/**	Write the ACT_DUR register
		 */
		void	request(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SetACT_DURReq& msg) noexcept;

		/**	Read the ACT_DUR register.
		 */
		void	request(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetACT_DURReq& msg) noexcept;

		/**	Write the ACT_THS register
		 */
		void	request(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SetACT_THSReq& msg) noexcept;

		/**	Read the ACT_THS register.
		 */
		void	request(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetACT_THSReq& msg) noexcept;

	};

template <class Context>
Composer<Context>::Composer(
			Context&		context,
			void	(Context::*GetOUT_TEMP)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetOUT_TEMPReq& msg),
			void	(Context::*GetOUT_G)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetOUT_GReq& msg),
			void	(Context::*GetOUT_XL)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetOUT_XLReq& msg),
			void	(Context::*SetINT_GEN_DUR_G)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SetINT_GEN_DUR_GReq& msg),
			void	(Context::*GetINT_GEN_DUR_G)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetINT_GEN_DUR_GReq& msg),
			void	(Context::*SetINT_GEN_THS_ZL_G)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SetINT_GEN_THS_ZL_GReq& msg),
			void	(Context::*GetINT_GEN_THS_ZL_G)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetINT_GEN_THS_ZL_GReq& msg),
			void	(Context::*SetINT_GEN_THS_ZH_G)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SetINT_GEN_THS_ZH_GReq& msg),
			void	(Context::*GetINT_GEN_THS_ZH_G)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetINT_GEN_THS_ZH_GReq& msg),
			void	(Context::*SetINT_GEN_THS_YL_G)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SetINT_GEN_THS_YL_GReq& msg),
			void	(Context::*GetINT_GEN_THS_YL_G)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetINT_GEN_THS_YL_GReq& msg),
			void	(Context::*SetINT_GEN_THS_YH_G)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SetINT_GEN_THS_YH_GReq& msg),
			void	(Context::*GetINT_GEN_THS_YH_G)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetINT_GEN_THS_YH_GReq& msg),
			void	(Context::*SetINT_GEN_THS_XL_G)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SetINT_GEN_THS_XL_GReq& msg),
			void	(Context::*GetINT_GEN_THS_XL_G)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetINT_GEN_THS_XL_GReq& msg),
			void	(Context::*SetINT_GEN_THS_XH_G)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SetINT_GEN_THS_XH_GReq& msg),
			void	(Context::*GetINT_GEN_THS_XH_G)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetINT_GEN_THS_XH_GReq& msg),
			void	(Context::*SetINT_GEN_CFG_G)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SetINT_GEN_CFG_GReq& msg),
			void	(Context::*GetINT_GEN_CFG_G)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetINT_GEN_CFG_GReq& msg),
			void	(Context::*GetFIFO_SRC)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetFIFO_SRCReq& msg),
			void	(Context::*SetFIFO_CTRL)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SetFIFO_CTRLReq& msg),
			void	(Context::*GetFIFO_CTRL)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetFIFO_CTRLReq& msg),
			void	(Context::*GetOUT_Z_H_XL)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetOUT_Z_H_XLReq& msg),
			void	(Context::*GetOUT_Z_L_XL)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetOUT_Z_L_XLReq& msg),
			void	(Context::*GetOUT_Y_H_XL)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetOUT_Y_H_XLReq& msg),
			void	(Context::*GetOUT_Y_L_XL)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetOUT_Y_L_XLReq& msg),
			void	(Context::*GetOUT_X_H_XL)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetOUT_X_H_XLReq& msg),
			void	(Context::*GetOUT_X_L_XL)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetOUT_X_L_XLReq& msg),
			void	(Context::*GetSTATUS_REG_2)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetSTATUS_REG_2Req& msg),
			void	(Context::*GetINT_GEN_SRC_XL)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetINT_GEN_SRC_XLReq& msg),
			void	(Context::*SetCTRL_REG10)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SetCTRL_REG10Req& msg),
			void	(Context::*GetCTRL_REG10)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetCTRL_REG10Req& msg),
			void	(Context::*SetCTRL_REG9)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SetCTRL_REG9Req& msg),
			void	(Context::*GetCTRL_REG9)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetCTRL_REG9Req& msg),
			void	(Context::*SetCTRL_REG8)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SetCTRL_REG8Req& msg),
			void	(Context::*GetCTRL_REG8)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetCTRL_REG8Req& msg),
			void	(Context::*SetCTRL_REG7_XL)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SetCTRL_REG7_XLReq& msg),
			void	(Context::*GetCTRL_REG7_XL)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetCTRL_REG7_XLReq& msg),
			void	(Context::*SetCTRL_REG6_XL)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SetCTRL_REG6_XLReq& msg),
			void	(Context::*GetCTRL_REG6_XL)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetCTRL_REG6_XLReq& msg),
			void	(Context::*SetCTRL_REG5_XL)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SetCTRL_REG5_XLReq& msg),
			void	(Context::*GetCTRL_REG5_XL)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetCTRL_REG5_XLReq& msg),
			void	(Context::*SetCTRL_REG4)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SetCTRL_REG4Req& msg),
			void	(Context::*GetCTRL_REG4)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetCTRL_REG4Req& msg),
			void	(Context::*GetOUT_Z_H_G)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetOUT_Z_H_GReq& msg),
			void	(Context::*GetOUT_Z_L_G)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetOUT_Z_L_GReq& msg),
			void	(Context::*GetOUT_Y_H_G)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetOUT_Y_H_GReq& msg),
			void	(Context::*GetOUT_Y_L_G)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetOUT_Y_L_GReq& msg),
			void	(Context::*GetOUT_X_L_G)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetOUT_X_L_GReq& msg),
			void	(Context::*GetSTATUS_REG)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetSTATUS_REGReq& msg),
			void	(Context::*GetOUT_TEMP_H)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetOUT_TEMP_HReq& msg),
			void	(Context::*GetOUT_TEMP_L)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetOUT_TEMP_LReq& msg),
			void	(Context::*GetINT_GEN_SRC_G)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetINT_GEN_SRC_GReq& msg),
			void	(Context::*SetORIENT_CFG_G)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SetORIENT_CFG_GReq& msg),
			void	(Context::*GetORIENT_CFG_G)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetORIENT_CFG_GReq& msg),
			void	(Context::*SetCTRL_REG3_G)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SetCTRL_REG3_GReq& msg),
			void	(Context::*GetCTRL_REG3_G)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetCTRL_REG3_GReq& msg),
			void	(Context::*SetCTRL_REG2_G)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SetCTRL_REG2_GReq& msg),
			void	(Context::*GetCTRL_REG2_G)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetCTRL_REG2_GReq& msg),
			void	(Context::*SetCTRL_REG1_G)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SetCTRL_REG1_GReq& msg),
			void	(Context::*GetCTRL_REG1_G)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetCTRL_REG1_GReq& msg),
			void	(Context::*GetWHO_AM_I)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetWHO_AM_IReq& msg),
			void	(Context::*SetINT2_CTRL)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SetINT2_CTRLReq& msg),
			void	(Context::*GetINT2_CTRL)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetINT2_CTRLReq& msg),
			void	(Context::*SetINT1_CTRL)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SetINT1_CTRLReq& msg),
			void	(Context::*GetINT1_CTRL)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetINT1_CTRLReq& msg),
			void	(Context::*SetREFERENCE_G)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SetREFERENCE_GReq& msg),
			void	(Context::*GetREFERENCE_G)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetREFERENCE_GReq& msg),
			void	(Context::*SetINT_GEN_DUR_XL)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SetINT_GEN_DUR_XLReq& msg),
			void	(Context::*GetINT_GEN_DUR_XL)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetINT_GEN_DUR_XLReq& msg),
			void	(Context::*SetINT_GEN_THS_Z_XL)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SetINT_GEN_THS_Z_XLReq& msg),
			void	(Context::*GetINT_GEN_THS_Z_XL)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetINT_GEN_THS_Z_XLReq& msg),
			void	(Context::*SetINT_GEN_THS_Y_XL)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SetINT_GEN_THS_Y_XLReq& msg),
			void	(Context::*GetINT_GEN_THS_Y_XL)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetINT_GEN_THS_Y_XLReq& msg),
			void	(Context::*SetINT_GEN_THS_X_XL)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SetINT_GEN_THS_X_XLReq& msg),
			void	(Context::*GetINT_GEN_THS_X_XL)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetINT_GEN_THS_X_XLReq& msg),
			void	(Context::*SetINT_GEN_CFG_XL)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SetINT_GEN_CFG_XLReq& msg),
			void	(Context::*GetINT_GEN_CFG_XL)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetINT_GEN_CFG_XLReq& msg),
			void	(Context::*SetACT_DUR)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SetACT_DURReq& msg),
			void	(Context::*GetACT_DUR)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetACT_DURReq& msg),
			void	(Context::*SetACT_THS)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SetACT_THSReq& msg),
			void	(Context::*GetACT_THS)(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetACT_THSReq& msg)
			) noexcept:
		_context(context),
		_GetOUT_TEMP(GetOUT_TEMP),
		_GetOUT_G(GetOUT_G),
		_GetOUT_XL(GetOUT_XL),
		_SetINT_GEN_DUR_G(SetINT_GEN_DUR_G),
		_GetINT_GEN_DUR_G(GetINT_GEN_DUR_G),
		_SetINT_GEN_THS_ZL_G(SetINT_GEN_THS_ZL_G),
		_GetINT_GEN_THS_ZL_G(GetINT_GEN_THS_ZL_G),
		_SetINT_GEN_THS_ZH_G(SetINT_GEN_THS_ZH_G),
		_GetINT_GEN_THS_ZH_G(GetINT_GEN_THS_ZH_G),
		_SetINT_GEN_THS_YL_G(SetINT_GEN_THS_YL_G),
		_GetINT_GEN_THS_YL_G(GetINT_GEN_THS_YL_G),
		_SetINT_GEN_THS_YH_G(SetINT_GEN_THS_YH_G),
		_GetINT_GEN_THS_YH_G(GetINT_GEN_THS_YH_G),
		_SetINT_GEN_THS_XL_G(SetINT_GEN_THS_XL_G),
		_GetINT_GEN_THS_XL_G(GetINT_GEN_THS_XL_G),
		_SetINT_GEN_THS_XH_G(SetINT_GEN_THS_XH_G),
		_GetINT_GEN_THS_XH_G(GetINT_GEN_THS_XH_G),
		_SetINT_GEN_CFG_G(SetINT_GEN_CFG_G),
		_GetINT_GEN_CFG_G(GetINT_GEN_CFG_G),
		_GetFIFO_SRC(GetFIFO_SRC),
		_SetFIFO_CTRL(SetFIFO_CTRL),
		_GetFIFO_CTRL(GetFIFO_CTRL),
		_GetOUT_Z_H_XL(GetOUT_Z_H_XL),
		_GetOUT_Z_L_XL(GetOUT_Z_L_XL),
		_GetOUT_Y_H_XL(GetOUT_Y_H_XL),
		_GetOUT_Y_L_XL(GetOUT_Y_L_XL),
		_GetOUT_X_H_XL(GetOUT_X_H_XL),
		_GetOUT_X_L_XL(GetOUT_X_L_XL),
		_GetSTATUS_REG_2(GetSTATUS_REG_2),
		_GetINT_GEN_SRC_XL(GetINT_GEN_SRC_XL),
		_SetCTRL_REG10(SetCTRL_REG10),
		_GetCTRL_REG10(GetCTRL_REG10),
		_SetCTRL_REG9(SetCTRL_REG9),
		_GetCTRL_REG9(GetCTRL_REG9),
		_SetCTRL_REG8(SetCTRL_REG8),
		_GetCTRL_REG8(GetCTRL_REG8),
		_SetCTRL_REG7_XL(SetCTRL_REG7_XL),
		_GetCTRL_REG7_XL(GetCTRL_REG7_XL),
		_SetCTRL_REG6_XL(SetCTRL_REG6_XL),
		_GetCTRL_REG6_XL(GetCTRL_REG6_XL),
		_SetCTRL_REG5_XL(SetCTRL_REG5_XL),
		_GetCTRL_REG5_XL(GetCTRL_REG5_XL),
		_SetCTRL_REG4(SetCTRL_REG4),
		_GetCTRL_REG4(GetCTRL_REG4),
		_GetOUT_Z_H_G(GetOUT_Z_H_G),
		_GetOUT_Z_L_G(GetOUT_Z_L_G),
		_GetOUT_Y_H_G(GetOUT_Y_H_G),
		_GetOUT_Y_L_G(GetOUT_Y_L_G),
		_GetOUT_X_L_G(GetOUT_X_L_G),
		_GetSTATUS_REG(GetSTATUS_REG),
		_GetOUT_TEMP_H(GetOUT_TEMP_H),
		_GetOUT_TEMP_L(GetOUT_TEMP_L),
		_GetINT_GEN_SRC_G(GetINT_GEN_SRC_G),
		_SetORIENT_CFG_G(SetORIENT_CFG_G),
		_GetORIENT_CFG_G(GetORIENT_CFG_G),
		_SetCTRL_REG3_G(SetCTRL_REG3_G),
		_GetCTRL_REG3_G(GetCTRL_REG3_G),
		_SetCTRL_REG2_G(SetCTRL_REG2_G),
		_GetCTRL_REG2_G(GetCTRL_REG2_G),
		_SetCTRL_REG1_G(SetCTRL_REG1_G),
		_GetCTRL_REG1_G(GetCTRL_REG1_G),
		_GetWHO_AM_I(GetWHO_AM_I),
		_SetINT2_CTRL(SetINT2_CTRL),
		_GetINT2_CTRL(GetINT2_CTRL),
		_SetINT1_CTRL(SetINT1_CTRL),
		_GetINT1_CTRL(GetINT1_CTRL),
		_SetREFERENCE_G(SetREFERENCE_G),
		_GetREFERENCE_G(GetREFERENCE_G),
		_SetINT_GEN_DUR_XL(SetINT_GEN_DUR_XL),
		_GetINT_GEN_DUR_XL(GetINT_GEN_DUR_XL),
		_SetINT_GEN_THS_Z_XL(SetINT_GEN_THS_Z_XL),
		_GetINT_GEN_THS_Z_XL(GetINT_GEN_THS_Z_XL),
		_SetINT_GEN_THS_Y_XL(SetINT_GEN_THS_Y_XL),
		_GetINT_GEN_THS_Y_XL(GetINT_GEN_THS_Y_XL),
		_SetINT_GEN_THS_X_XL(SetINT_GEN_THS_X_XL),
		_GetINT_GEN_THS_X_XL(GetINT_GEN_THS_X_XL),
		_SetINT_GEN_CFG_XL(SetINT_GEN_CFG_XL),
		_GetINT_GEN_CFG_XL(GetINT_GEN_CFG_XL),
		_SetACT_DUR(SetACT_DUR),
		_GetACT_DUR(GetACT_DUR),
		_SetACT_THS(SetACT_THS),
		_GetACT_THS(GetACT_THS)
		{
	}

template <class Context>
void	Composer<Context>::request(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetOUT_TEMPReq& msg) noexcept{
	(_context.*_GetOUT_TEMP)(msg);
	}

template <class Context>
void	Composer<Context>::request(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetOUT_GReq& msg) noexcept{
	(_context.*_GetOUT_G)(msg);
	}

template <class Context>
void	Composer<Context>::request(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetOUT_XLReq& msg) noexcept{
	(_context.*_GetOUT_XL)(msg);
	}

template <class Context>
void	Composer<Context>::request(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SetINT_GEN_DUR_GReq& msg) noexcept{
	(_context.*_SetINT_GEN_DUR_G)(msg);
	}

template <class Context>
void	Composer<Context>::request(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetINT_GEN_DUR_GReq& msg) noexcept{
	(_context.*_GetINT_GEN_DUR_G)(msg);
	}

template <class Context>
void	Composer<Context>::request(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SetINT_GEN_THS_ZL_GReq& msg) noexcept{
	(_context.*_SetINT_GEN_THS_ZL_G)(msg);
	}

template <class Context>
void	Composer<Context>::request(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetINT_GEN_THS_ZL_GReq& msg) noexcept{
	(_context.*_GetINT_GEN_THS_ZL_G)(msg);
	}

template <class Context>
void	Composer<Context>::request(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SetINT_GEN_THS_ZH_GReq& msg) noexcept{
	(_context.*_SetINT_GEN_THS_ZH_G)(msg);
	}

template <class Context>
void	Composer<Context>::request(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetINT_GEN_THS_ZH_GReq& msg) noexcept{
	(_context.*_GetINT_GEN_THS_ZH_G)(msg);
	}

template <class Context>
void	Composer<Context>::request(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SetINT_GEN_THS_YL_GReq& msg) noexcept{
	(_context.*_SetINT_GEN_THS_YL_G)(msg);
	}

template <class Context>
void	Composer<Context>::request(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetINT_GEN_THS_YL_GReq& msg) noexcept{
	(_context.*_GetINT_GEN_THS_YL_G)(msg);
	}

template <class Context>
void	Composer<Context>::request(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SetINT_GEN_THS_YH_GReq& msg) noexcept{
	(_context.*_SetINT_GEN_THS_YH_G)(msg);
	}

template <class Context>
void	Composer<Context>::request(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetINT_GEN_THS_YH_GReq& msg) noexcept{
	(_context.*_GetINT_GEN_THS_YH_G)(msg);
	}

template <class Context>
void	Composer<Context>::request(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SetINT_GEN_THS_XL_GReq& msg) noexcept{
	(_context.*_SetINT_GEN_THS_XL_G)(msg);
	}

template <class Context>
void	Composer<Context>::request(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetINT_GEN_THS_XL_GReq& msg) noexcept{
	(_context.*_GetINT_GEN_THS_XL_G)(msg);
	}

template <class Context>
void	Composer<Context>::request(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SetINT_GEN_THS_XH_GReq& msg) noexcept{
	(_context.*_SetINT_GEN_THS_XH_G)(msg);
	}

template <class Context>
void	Composer<Context>::request(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetINT_GEN_THS_XH_GReq& msg) noexcept{
	(_context.*_GetINT_GEN_THS_XH_G)(msg);
	}

template <class Context>
void	Composer<Context>::request(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SetINT_GEN_CFG_GReq& msg) noexcept{
	(_context.*_SetINT_GEN_CFG_G)(msg);
	}

template <class Context>
void	Composer<Context>::request(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetINT_GEN_CFG_GReq& msg) noexcept{
	(_context.*_GetINT_GEN_CFG_G)(msg);
	}

template <class Context>
void	Composer<Context>::request(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetFIFO_SRCReq& msg) noexcept{
	(_context.*_GetFIFO_SRC)(msg);
	}

template <class Context>
void	Composer<Context>::request(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SetFIFO_CTRLReq& msg) noexcept{
	(_context.*_SetFIFO_CTRL)(msg);
	}

template <class Context>
void	Composer<Context>::request(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetFIFO_CTRLReq& msg) noexcept{
	(_context.*_GetFIFO_CTRL)(msg);
	}

template <class Context>
void	Composer<Context>::request(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetOUT_Z_H_XLReq& msg) noexcept{
	(_context.*_GetOUT_Z_H_XL)(msg);
	}

template <class Context>
void	Composer<Context>::request(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetOUT_Z_L_XLReq& msg) noexcept{
	(_context.*_GetOUT_Z_L_XL)(msg);
	}

template <class Context>
void	Composer<Context>::request(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetOUT_Y_H_XLReq& msg) noexcept{
	(_context.*_GetOUT_Y_H_XL)(msg);
	}

template <class Context>
void	Composer<Context>::request(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetOUT_Y_L_XLReq& msg) noexcept{
	(_context.*_GetOUT_Y_L_XL)(msg);
	}

template <class Context>
void	Composer<Context>::request(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetOUT_X_H_XLReq& msg) noexcept{
	(_context.*_GetOUT_X_H_XL)(msg);
	}

template <class Context>
void	Composer<Context>::request(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetOUT_X_L_XLReq& msg) noexcept{
	(_context.*_GetOUT_X_L_XL)(msg);
	}

template <class Context>
void	Composer<Context>::request(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetSTATUS_REG_2Req& msg) noexcept{
	(_context.*_GetSTATUS_REG_2)(msg);
	}

template <class Context>
void	Composer<Context>::request(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetINT_GEN_SRC_XLReq& msg) noexcept{
	(_context.*_GetINT_GEN_SRC_XL)(msg);
	}

template <class Context>
void	Composer<Context>::request(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SetCTRL_REG10Req& msg) noexcept{
	(_context.*_SetCTRL_REG10)(msg);
	}

template <class Context>
void	Composer<Context>::request(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetCTRL_REG10Req& msg) noexcept{
	(_context.*_GetCTRL_REG10)(msg);
	}

template <class Context>
void	Composer<Context>::request(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SetCTRL_REG9Req& msg) noexcept{
	(_context.*_SetCTRL_REG9)(msg);
	}

template <class Context>
void	Composer<Context>::request(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetCTRL_REG9Req& msg) noexcept{
	(_context.*_GetCTRL_REG9)(msg);
	}

template <class Context>
void	Composer<Context>::request(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SetCTRL_REG8Req& msg) noexcept{
	(_context.*_SetCTRL_REG8)(msg);
	}

template <class Context>
void	Composer<Context>::request(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetCTRL_REG8Req& msg) noexcept{
	(_context.*_GetCTRL_REG8)(msg);
	}

template <class Context>
void	Composer<Context>::request(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SetCTRL_REG7_XLReq& msg) noexcept{
	(_context.*_SetCTRL_REG7_XL)(msg);
	}

template <class Context>
void	Composer<Context>::request(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetCTRL_REG7_XLReq& msg) noexcept{
	(_context.*_GetCTRL_REG7_XL)(msg);
	}

template <class Context>
void	Composer<Context>::request(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SetCTRL_REG6_XLReq& msg) noexcept{
	(_context.*_SetCTRL_REG6_XL)(msg);
	}

template <class Context>
void	Composer<Context>::request(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetCTRL_REG6_XLReq& msg) noexcept{
	(_context.*_GetCTRL_REG6_XL)(msg);
	}

template <class Context>
void	Composer<Context>::request(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SetCTRL_REG5_XLReq& msg) noexcept{
	(_context.*_SetCTRL_REG5_XL)(msg);
	}

template <class Context>
void	Composer<Context>::request(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetCTRL_REG5_XLReq& msg) noexcept{
	(_context.*_GetCTRL_REG5_XL)(msg);
	}

template <class Context>
void	Composer<Context>::request(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SetCTRL_REG4Req& msg) noexcept{
	(_context.*_SetCTRL_REG4)(msg);
	}

template <class Context>
void	Composer<Context>::request(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetCTRL_REG4Req& msg) noexcept{
	(_context.*_GetCTRL_REG4)(msg);
	}

template <class Context>
void	Composer<Context>::request(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetOUT_Z_H_GReq& msg) noexcept{
	(_context.*_GetOUT_Z_H_G)(msg);
	}

template <class Context>
void	Composer<Context>::request(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetOUT_Z_L_GReq& msg) noexcept{
	(_context.*_GetOUT_Z_L_G)(msg);
	}

template <class Context>
void	Composer<Context>::request(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetOUT_Y_H_GReq& msg) noexcept{
	(_context.*_GetOUT_Y_H_G)(msg);
	}

template <class Context>
void	Composer<Context>::request(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetOUT_Y_L_GReq& msg) noexcept{
	(_context.*_GetOUT_Y_L_G)(msg);
	}

template <class Context>
void	Composer<Context>::request(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetOUT_X_L_GReq& msg) noexcept{
	(_context.*_GetOUT_X_L_G)(msg);
	}

template <class Context>
void	Composer<Context>::request(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetSTATUS_REGReq& msg) noexcept{
	(_context.*_GetSTATUS_REG)(msg);
	}

template <class Context>
void	Composer<Context>::request(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetOUT_TEMP_HReq& msg) noexcept{
	(_context.*_GetOUT_TEMP_H)(msg);
	}

template <class Context>
void	Composer<Context>::request(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetOUT_TEMP_LReq& msg) noexcept{
	(_context.*_GetOUT_TEMP_L)(msg);
	}

template <class Context>
void	Composer<Context>::request(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetINT_GEN_SRC_GReq& msg) noexcept{
	(_context.*_GetINT_GEN_SRC_G)(msg);
	}

template <class Context>
void	Composer<Context>::request(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SetORIENT_CFG_GReq& msg) noexcept{
	(_context.*_SetORIENT_CFG_G)(msg);
	}

template <class Context>
void	Composer<Context>::request(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetORIENT_CFG_GReq& msg) noexcept{
	(_context.*_GetORIENT_CFG_G)(msg);
	}

template <class Context>
void	Composer<Context>::request(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SetCTRL_REG3_GReq& msg) noexcept{
	(_context.*_SetCTRL_REG3_G)(msg);
	}

template <class Context>
void	Composer<Context>::request(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetCTRL_REG3_GReq& msg) noexcept{
	(_context.*_GetCTRL_REG3_G)(msg);
	}

template <class Context>
void	Composer<Context>::request(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SetCTRL_REG2_GReq& msg) noexcept{
	(_context.*_SetCTRL_REG2_G)(msg);
	}

template <class Context>
void	Composer<Context>::request(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetCTRL_REG2_GReq& msg) noexcept{
	(_context.*_GetCTRL_REG2_G)(msg);
	}

template <class Context>
void	Composer<Context>::request(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SetCTRL_REG1_GReq& msg) noexcept{
	(_context.*_SetCTRL_REG1_G)(msg);
	}

template <class Context>
void	Composer<Context>::request(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetCTRL_REG1_GReq& msg) noexcept{
	(_context.*_GetCTRL_REG1_G)(msg);
	}

template <class Context>
void	Composer<Context>::request(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetWHO_AM_IReq& msg) noexcept{
	(_context.*_GetWHO_AM_I)(msg);
	}

template <class Context>
void	Composer<Context>::request(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SetINT2_CTRLReq& msg) noexcept{
	(_context.*_SetINT2_CTRL)(msg);
	}

template <class Context>
void	Composer<Context>::request(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetINT2_CTRLReq& msg) noexcept{
	(_context.*_GetINT2_CTRL)(msg);
	}

template <class Context>
void	Composer<Context>::request(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SetINT1_CTRLReq& msg) noexcept{
	(_context.*_SetINT1_CTRL)(msg);
	}

template <class Context>
void	Composer<Context>::request(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetINT1_CTRLReq& msg) noexcept{
	(_context.*_GetINT1_CTRL)(msg);
	}

template <class Context>
void	Composer<Context>::request(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SetREFERENCE_GReq& msg) noexcept{
	(_context.*_SetREFERENCE_G)(msg);
	}

template <class Context>
void	Composer<Context>::request(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetREFERENCE_GReq& msg) noexcept{
	(_context.*_GetREFERENCE_G)(msg);
	}

template <class Context>
void	Composer<Context>::request(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SetINT_GEN_DUR_XLReq& msg) noexcept{
	(_context.*_SetINT_GEN_DUR_XL)(msg);
	}

template <class Context>
void	Composer<Context>::request(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetINT_GEN_DUR_XLReq& msg) noexcept{
	(_context.*_GetINT_GEN_DUR_XL)(msg);
	}

template <class Context>
void	Composer<Context>::request(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SetINT_GEN_THS_Z_XLReq& msg) noexcept{
	(_context.*_SetINT_GEN_THS_Z_XL)(msg);
	}

template <class Context>
void	Composer<Context>::request(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetINT_GEN_THS_Z_XLReq& msg) noexcept{
	(_context.*_GetINT_GEN_THS_Z_XL)(msg);
	}

template <class Context>
void	Composer<Context>::request(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SetINT_GEN_THS_Y_XLReq& msg) noexcept{
	(_context.*_SetINT_GEN_THS_Y_XL)(msg);
	}

template <class Context>
void	Composer<Context>::request(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetINT_GEN_THS_Y_XLReq& msg) noexcept{
	(_context.*_GetINT_GEN_THS_Y_XL)(msg);
	}

template <class Context>
void	Composer<Context>::request(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SetINT_GEN_THS_X_XLReq& msg) noexcept{
	(_context.*_SetINT_GEN_THS_X_XL)(msg);
	}

template <class Context>
void	Composer<Context>::request(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetINT_GEN_THS_X_XLReq& msg) noexcept{
	(_context.*_GetINT_GEN_THS_X_XL)(msg);
	}

template <class Context>
void	Composer<Context>::request(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SetINT_GEN_CFG_XLReq& msg) noexcept{
	(_context.*_SetINT_GEN_CFG_XL)(msg);
	}

template <class Context>
void	Composer<Context>::request(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetINT_GEN_CFG_XLReq& msg) noexcept{
	(_context.*_GetINT_GEN_CFG_XL)(msg);
	}

template <class Context>
void	Composer<Context>::request(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SetACT_DURReq& msg) noexcept{
	(_context.*_SetACT_DUR)(msg);
	}

template <class Context>
void	Composer<Context>::request(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetACT_DURReq& msg) noexcept{
	(_context.*_GetACT_DUR)(msg);
	}

template <class Context>
void	Composer<Context>::request(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::SetACT_THSReq& msg) noexcept{
	(_context.*_SetACT_THS)(msg);
	}

template <class Context>
void	Composer<Context>::request(Oscl::Driver::ST::LSM9DS1::AG::Register::Req::Api::GetACT_THSReq& msg) noexcept{
	(_context.*_GetACT_THS)(msg);
	}

}
}
}
}
}
}
}
#endif
