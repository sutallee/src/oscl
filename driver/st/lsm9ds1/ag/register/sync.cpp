/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "sync.h"
#include "oscl/mt/itc/mbox/syncrh.h"

using namespace Oscl::Driver::ST::LSM9DS1::AG::Register;

Sync::Sync(	Oscl::Mt::Itc::PostMsgApi&	myPapi,
			Req::Api&				reqApi
			) noexcept:
		_sap(reqApi,myPapi)
		{
	}

Req::Api::SAP&	Sync::getSAP() noexcept{
	return _sap;
	}

void	Sync::getOUT_TEMP(	
						uint8_t	maxBufferLength,
						void*	buffer
						) noexcept{

	Oscl::Mt::Itc::SyncReturnHandler		srh;

	Req::Api::GetOUT_TEMPPayload		payload(
									maxBufferLength,
									buffer
									);

	Req::Api::GetOUT_TEMPReq	req(	_sap.getReqApi(),
									payload,
									srh
									);

	_sap.postSync(req);
	}

void	Sync::getOUT_G(	
						uint8_t	maxBufferLength,
						void*	buffer
						) noexcept{

	Oscl::Mt::Itc::SyncReturnHandler		srh;

	Req::Api::GetOUT_GPayload		payload(
									maxBufferLength,
									buffer
									);

	Req::Api::GetOUT_GReq	req(	_sap.getReqApi(),
									payload,
									srh
									);

	_sap.postSync(req);
	}

void	Sync::getOUT_XL(	
						uint8_t	maxBufferLength,
						void*	buffer
						) noexcept{

	Oscl::Mt::Itc::SyncReturnHandler		srh;

	Req::Api::GetOUT_XLPayload		payload(
									maxBufferLength,
									buffer
									);

	Req::Api::GetOUT_XLReq	req(	_sap.getReqApi(),
									payload,
									srh
									);

	_sap.postSync(req);
	}

void	Sync::setINT_GEN_DUR_G(	
						uint8_t	value
						) noexcept{

	Oscl::Mt::Itc::SyncReturnHandler		srh;

	Req::Api::SetINT_GEN_DUR_GPayload		payload(
									value
									);

	Req::Api::SetINT_GEN_DUR_GReq	req(	_sap.getReqApi(),
									payload,
									srh
									);

	_sap.postSync(req);
	}

uint8_t	Sync::getINT_GEN_DUR_G(	
						) noexcept{

	Oscl::Mt::Itc::SyncReturnHandler		srh;

	Req::Api::GetINT_GEN_DUR_GPayload		payload;

	Req::Api::GetINT_GEN_DUR_GReq	req(	_sap.getReqApi(),
									payload,
									srh
									);

	_sap.postSync(req);

	return payload._value;
	}

void	Sync::setINT_GEN_THS_ZL_G(	
						uint8_t	value
						) noexcept{

	Oscl::Mt::Itc::SyncReturnHandler		srh;

	Req::Api::SetINT_GEN_THS_ZL_GPayload		payload(
									value
									);

	Req::Api::SetINT_GEN_THS_ZL_GReq	req(	_sap.getReqApi(),
									payload,
									srh
									);

	_sap.postSync(req);
	}

uint8_t	Sync::getINT_GEN_THS_ZL_G(	
						) noexcept{

	Oscl::Mt::Itc::SyncReturnHandler		srh;

	Req::Api::GetINT_GEN_THS_ZL_GPayload		payload;

	Req::Api::GetINT_GEN_THS_ZL_GReq	req(	_sap.getReqApi(),
									payload,
									srh
									);

	_sap.postSync(req);

	return payload._value;
	}

void	Sync::setINT_GEN_THS_ZH_G(	
						uint8_t	value
						) noexcept{

	Oscl::Mt::Itc::SyncReturnHandler		srh;

	Req::Api::SetINT_GEN_THS_ZH_GPayload		payload(
									value
									);

	Req::Api::SetINT_GEN_THS_ZH_GReq	req(	_sap.getReqApi(),
									payload,
									srh
									);

	_sap.postSync(req);
	}

uint8_t	Sync::getINT_GEN_THS_ZH_G(	
						) noexcept{

	Oscl::Mt::Itc::SyncReturnHandler		srh;

	Req::Api::GetINT_GEN_THS_ZH_GPayload		payload;

	Req::Api::GetINT_GEN_THS_ZH_GReq	req(	_sap.getReqApi(),
									payload,
									srh
									);

	_sap.postSync(req);

	return payload._value;
	}

void	Sync::setINT_GEN_THS_YL_G(	
						uint8_t	value
						) noexcept{

	Oscl::Mt::Itc::SyncReturnHandler		srh;

	Req::Api::SetINT_GEN_THS_YL_GPayload		payload(
									value
									);

	Req::Api::SetINT_GEN_THS_YL_GReq	req(	_sap.getReqApi(),
									payload,
									srh
									);

	_sap.postSync(req);
	}

uint8_t	Sync::getINT_GEN_THS_YL_G(	
						) noexcept{

	Oscl::Mt::Itc::SyncReturnHandler		srh;

	Req::Api::GetINT_GEN_THS_YL_GPayload		payload;

	Req::Api::GetINT_GEN_THS_YL_GReq	req(	_sap.getReqApi(),
									payload,
									srh
									);

	_sap.postSync(req);

	return payload._value;
	}

void	Sync::setINT_GEN_THS_YH_G(	
						uint8_t	value
						) noexcept{

	Oscl::Mt::Itc::SyncReturnHandler		srh;

	Req::Api::SetINT_GEN_THS_YH_GPayload		payload(
									value
									);

	Req::Api::SetINT_GEN_THS_YH_GReq	req(	_sap.getReqApi(),
									payload,
									srh
									);

	_sap.postSync(req);
	}

uint8_t	Sync::getINT_GEN_THS_YH_G(	
						) noexcept{

	Oscl::Mt::Itc::SyncReturnHandler		srh;

	Req::Api::GetINT_GEN_THS_YH_GPayload		payload;

	Req::Api::GetINT_GEN_THS_YH_GReq	req(	_sap.getReqApi(),
									payload,
									srh
									);

	_sap.postSync(req);

	return payload._value;
	}

void	Sync::setINT_GEN_THS_XL_G(	
						uint8_t	value
						) noexcept{

	Oscl::Mt::Itc::SyncReturnHandler		srh;

	Req::Api::SetINT_GEN_THS_XL_GPayload		payload(
									value
									);

	Req::Api::SetINT_GEN_THS_XL_GReq	req(	_sap.getReqApi(),
									payload,
									srh
									);

	_sap.postSync(req);
	}

uint8_t	Sync::getINT_GEN_THS_XL_G(	
						) noexcept{

	Oscl::Mt::Itc::SyncReturnHandler		srh;

	Req::Api::GetINT_GEN_THS_XL_GPayload		payload;

	Req::Api::GetINT_GEN_THS_XL_GReq	req(	_sap.getReqApi(),
									payload,
									srh
									);

	_sap.postSync(req);

	return payload._value;
	}

void	Sync::setINT_GEN_THS_XH_G(	
						uint8_t	value
						) noexcept{

	Oscl::Mt::Itc::SyncReturnHandler		srh;

	Req::Api::SetINT_GEN_THS_XH_GPayload		payload(
									value
									);

	Req::Api::SetINT_GEN_THS_XH_GReq	req(	_sap.getReqApi(),
									payload,
									srh
									);

	_sap.postSync(req);
	}

uint8_t	Sync::getINT_GEN_THS_XH_G(	
						) noexcept{

	Oscl::Mt::Itc::SyncReturnHandler		srh;

	Req::Api::GetINT_GEN_THS_XH_GPayload		payload;

	Req::Api::GetINT_GEN_THS_XH_GReq	req(	_sap.getReqApi(),
									payload,
									srh
									);

	_sap.postSync(req);

	return payload._value;
	}

void	Sync::setINT_GEN_CFG_G(	
						uint8_t	value
						) noexcept{

	Oscl::Mt::Itc::SyncReturnHandler		srh;

	Req::Api::SetINT_GEN_CFG_GPayload		payload(
									value
									);

	Req::Api::SetINT_GEN_CFG_GReq	req(	_sap.getReqApi(),
									payload,
									srh
									);

	_sap.postSync(req);
	}

uint8_t	Sync::getINT_GEN_CFG_G(	
						) noexcept{

	Oscl::Mt::Itc::SyncReturnHandler		srh;

	Req::Api::GetINT_GEN_CFG_GPayload		payload;

	Req::Api::GetINT_GEN_CFG_GReq	req(	_sap.getReqApi(),
									payload,
									srh
									);

	_sap.postSync(req);

	return payload._value;
	}

uint8_t	Sync::getFIFO_SRC(	
						) noexcept{

	Oscl::Mt::Itc::SyncReturnHandler		srh;

	Req::Api::GetFIFO_SRCPayload		payload;

	Req::Api::GetFIFO_SRCReq	req(	_sap.getReqApi(),
									payload,
									srh
									);

	_sap.postSync(req);

	return payload._value;
	}

void	Sync::setFIFO_CTRL(	
						uint8_t	value
						) noexcept{

	Oscl::Mt::Itc::SyncReturnHandler		srh;

	Req::Api::SetFIFO_CTRLPayload		payload(
									value
									);

	Req::Api::SetFIFO_CTRLReq	req(	_sap.getReqApi(),
									payload,
									srh
									);

	_sap.postSync(req);
	}

uint8_t	Sync::getFIFO_CTRL(	
						) noexcept{

	Oscl::Mt::Itc::SyncReturnHandler		srh;

	Req::Api::GetFIFO_CTRLPayload		payload;

	Req::Api::GetFIFO_CTRLReq	req(	_sap.getReqApi(),
									payload,
									srh
									);

	_sap.postSync(req);

	return payload._value;
	}

uint8_t	Sync::getOUT_Z_H_XL(	
						) noexcept{

	Oscl::Mt::Itc::SyncReturnHandler		srh;

	Req::Api::GetOUT_Z_H_XLPayload		payload;

	Req::Api::GetOUT_Z_H_XLReq	req(	_sap.getReqApi(),
									payload,
									srh
									);

	_sap.postSync(req);

	return payload._value;
	}

uint8_t	Sync::getOUT_Z_L_XL(	
						) noexcept{

	Oscl::Mt::Itc::SyncReturnHandler		srh;

	Req::Api::GetOUT_Z_L_XLPayload		payload;

	Req::Api::GetOUT_Z_L_XLReq	req(	_sap.getReqApi(),
									payload,
									srh
									);

	_sap.postSync(req);

	return payload._value;
	}

uint8_t	Sync::getOUT_Y_H_XL(	
						) noexcept{

	Oscl::Mt::Itc::SyncReturnHandler		srh;

	Req::Api::GetOUT_Y_H_XLPayload		payload;

	Req::Api::GetOUT_Y_H_XLReq	req(	_sap.getReqApi(),
									payload,
									srh
									);

	_sap.postSync(req);

	return payload._value;
	}

uint8_t	Sync::getOUT_Y_L_XL(	
						) noexcept{

	Oscl::Mt::Itc::SyncReturnHandler		srh;

	Req::Api::GetOUT_Y_L_XLPayload		payload;

	Req::Api::GetOUT_Y_L_XLReq	req(	_sap.getReqApi(),
									payload,
									srh
									);

	_sap.postSync(req);

	return payload._value;
	}

uint8_t	Sync::getOUT_X_H_XL(	
						) noexcept{

	Oscl::Mt::Itc::SyncReturnHandler		srh;

	Req::Api::GetOUT_X_H_XLPayload		payload;

	Req::Api::GetOUT_X_H_XLReq	req(	_sap.getReqApi(),
									payload,
									srh
									);

	_sap.postSync(req);

	return payload._value;
	}

uint8_t	Sync::getOUT_X_L_XL(	
						) noexcept{

	Oscl::Mt::Itc::SyncReturnHandler		srh;

	Req::Api::GetOUT_X_L_XLPayload		payload;

	Req::Api::GetOUT_X_L_XLReq	req(	_sap.getReqApi(),
									payload,
									srh
									);

	_sap.postSync(req);

	return payload._value;
	}

uint8_t	Sync::getSTATUS_REG_2(	
						) noexcept{

	Oscl::Mt::Itc::SyncReturnHandler		srh;

	Req::Api::GetSTATUS_REG_2Payload		payload;

	Req::Api::GetSTATUS_REG_2Req	req(	_sap.getReqApi(),
									payload,
									srh
									);

	_sap.postSync(req);

	return payload._value;
	}

uint8_t	Sync::getINT_GEN_SRC_XL(	
						) noexcept{

	Oscl::Mt::Itc::SyncReturnHandler		srh;

	Req::Api::GetINT_GEN_SRC_XLPayload		payload;

	Req::Api::GetINT_GEN_SRC_XLReq	req(	_sap.getReqApi(),
									payload,
									srh
									);

	_sap.postSync(req);

	return payload._value;
	}

void	Sync::setCTRL_REG10(	
						uint8_t	value
						) noexcept{

	Oscl::Mt::Itc::SyncReturnHandler		srh;

	Req::Api::SetCTRL_REG10Payload		payload(
									value
									);

	Req::Api::SetCTRL_REG10Req	req(	_sap.getReqApi(),
									payload,
									srh
									);

	_sap.postSync(req);
	}

uint8_t	Sync::getCTRL_REG10(	
						) noexcept{

	Oscl::Mt::Itc::SyncReturnHandler		srh;

	Req::Api::GetCTRL_REG10Payload		payload;

	Req::Api::GetCTRL_REG10Req	req(	_sap.getReqApi(),
									payload,
									srh
									);

	_sap.postSync(req);

	return payload._value;
	}

void	Sync::setCTRL_REG9(	
						uint8_t	value
						) noexcept{

	Oscl::Mt::Itc::SyncReturnHandler		srh;

	Req::Api::SetCTRL_REG9Payload		payload(
									value
									);

	Req::Api::SetCTRL_REG9Req	req(	_sap.getReqApi(),
									payload,
									srh
									);

	_sap.postSync(req);
	}

uint8_t	Sync::getCTRL_REG9(	
						) noexcept{

	Oscl::Mt::Itc::SyncReturnHandler		srh;

	Req::Api::GetCTRL_REG9Payload		payload;

	Req::Api::GetCTRL_REG9Req	req(	_sap.getReqApi(),
									payload,
									srh
									);

	_sap.postSync(req);

	return payload._value;
	}

void	Sync::setCTRL_REG8(	
						uint8_t	value
						) noexcept{

	Oscl::Mt::Itc::SyncReturnHandler		srh;

	Req::Api::SetCTRL_REG8Payload		payload(
									value
									);

	Req::Api::SetCTRL_REG8Req	req(	_sap.getReqApi(),
									payload,
									srh
									);

	_sap.postSync(req);
	}

uint8_t	Sync::getCTRL_REG8(	
						) noexcept{

	Oscl::Mt::Itc::SyncReturnHandler		srh;

	Req::Api::GetCTRL_REG8Payload		payload;

	Req::Api::GetCTRL_REG8Req	req(	_sap.getReqApi(),
									payload,
									srh
									);

	_sap.postSync(req);

	return payload._value;
	}

void	Sync::setCTRL_REG7_XL(	
						uint8_t	value
						) noexcept{

	Oscl::Mt::Itc::SyncReturnHandler		srh;

	Req::Api::SetCTRL_REG7_XLPayload		payload(
									value
									);

	Req::Api::SetCTRL_REG7_XLReq	req(	_sap.getReqApi(),
									payload,
									srh
									);

	_sap.postSync(req);
	}

uint8_t	Sync::getCTRL_REG7_XL(	
						) noexcept{

	Oscl::Mt::Itc::SyncReturnHandler		srh;

	Req::Api::GetCTRL_REG7_XLPayload		payload;

	Req::Api::GetCTRL_REG7_XLReq	req(	_sap.getReqApi(),
									payload,
									srh
									);

	_sap.postSync(req);

	return payload._value;
	}

void	Sync::setCTRL_REG6_XL(	
						uint8_t	value
						) noexcept{

	Oscl::Mt::Itc::SyncReturnHandler		srh;

	Req::Api::SetCTRL_REG6_XLPayload		payload(
									value
									);

	Req::Api::SetCTRL_REG6_XLReq	req(	_sap.getReqApi(),
									payload,
									srh
									);

	_sap.postSync(req);
	}

uint8_t	Sync::getCTRL_REG6_XL(	
						) noexcept{

	Oscl::Mt::Itc::SyncReturnHandler		srh;

	Req::Api::GetCTRL_REG6_XLPayload		payload;

	Req::Api::GetCTRL_REG6_XLReq	req(	_sap.getReqApi(),
									payload,
									srh
									);

	_sap.postSync(req);

	return payload._value;
	}

void	Sync::setCTRL_REG5_XL(	
						uint8_t	value
						) noexcept{

	Oscl::Mt::Itc::SyncReturnHandler		srh;

	Req::Api::SetCTRL_REG5_XLPayload		payload(
									value
									);

	Req::Api::SetCTRL_REG5_XLReq	req(	_sap.getReqApi(),
									payload,
									srh
									);

	_sap.postSync(req);
	}

uint8_t	Sync::getCTRL_REG5_XL(	
						) noexcept{

	Oscl::Mt::Itc::SyncReturnHandler		srh;

	Req::Api::GetCTRL_REG5_XLPayload		payload;

	Req::Api::GetCTRL_REG5_XLReq	req(	_sap.getReqApi(),
									payload,
									srh
									);

	_sap.postSync(req);

	return payload._value;
	}

void	Sync::setCTRL_REG4(	
						uint8_t	value
						) noexcept{

	Oscl::Mt::Itc::SyncReturnHandler		srh;

	Req::Api::SetCTRL_REG4Payload		payload(
									value
									);

	Req::Api::SetCTRL_REG4Req	req(	_sap.getReqApi(),
									payload,
									srh
									);

	_sap.postSync(req);
	}

uint8_t	Sync::getCTRL_REG4(	
						) noexcept{

	Oscl::Mt::Itc::SyncReturnHandler		srh;

	Req::Api::GetCTRL_REG4Payload		payload;

	Req::Api::GetCTRL_REG4Req	req(	_sap.getReqApi(),
									payload,
									srh
									);

	_sap.postSync(req);

	return payload._value;
	}

uint8_t	Sync::getOUT_Z_H_G(	
						) noexcept{

	Oscl::Mt::Itc::SyncReturnHandler		srh;

	Req::Api::GetOUT_Z_H_GPayload		payload;

	Req::Api::GetOUT_Z_H_GReq	req(	_sap.getReqApi(),
									payload,
									srh
									);

	_sap.postSync(req);

	return payload._value;
	}

uint8_t	Sync::getOUT_Z_L_G(	
						) noexcept{

	Oscl::Mt::Itc::SyncReturnHandler		srh;

	Req::Api::GetOUT_Z_L_GPayload		payload;

	Req::Api::GetOUT_Z_L_GReq	req(	_sap.getReqApi(),
									payload,
									srh
									);

	_sap.postSync(req);

	return payload._value;
	}

uint8_t	Sync::getOUT_Y_H_G(	
						) noexcept{

	Oscl::Mt::Itc::SyncReturnHandler		srh;

	Req::Api::GetOUT_Y_H_GPayload		payload;

	Req::Api::GetOUT_Y_H_GReq	req(	_sap.getReqApi(),
									payload,
									srh
									);

	_sap.postSync(req);

	return payload._value;
	}

uint8_t	Sync::getOUT_Y_L_G(	
						) noexcept{

	Oscl::Mt::Itc::SyncReturnHandler		srh;

	Req::Api::GetOUT_Y_L_GPayload		payload;

	Req::Api::GetOUT_Y_L_GReq	req(	_sap.getReqApi(),
									payload,
									srh
									);

	_sap.postSync(req);

	return payload._value;
	}

uint8_t	Sync::getOUT_X_L_G(	
						) noexcept{

	Oscl::Mt::Itc::SyncReturnHandler		srh;

	Req::Api::GetOUT_X_L_GPayload		payload;

	Req::Api::GetOUT_X_L_GReq	req(	_sap.getReqApi(),
									payload,
									srh
									);

	_sap.postSync(req);

	return payload._value;
	}

uint8_t	Sync::getSTATUS_REG(	
						) noexcept{

	Oscl::Mt::Itc::SyncReturnHandler		srh;

	Req::Api::GetSTATUS_REGPayload		payload;

	Req::Api::GetSTATUS_REGReq	req(	_sap.getReqApi(),
									payload,
									srh
									);

	_sap.postSync(req);

	return payload._value;
	}

uint8_t	Sync::getOUT_TEMP_H(	
						) noexcept{

	Oscl::Mt::Itc::SyncReturnHandler		srh;

	Req::Api::GetOUT_TEMP_HPayload		payload;

	Req::Api::GetOUT_TEMP_HReq	req(	_sap.getReqApi(),
									payload,
									srh
									);

	_sap.postSync(req);

	return payload._value;
	}

uint8_t	Sync::getOUT_TEMP_L(	
						) noexcept{

	Oscl::Mt::Itc::SyncReturnHandler		srh;

	Req::Api::GetOUT_TEMP_LPayload		payload;

	Req::Api::GetOUT_TEMP_LReq	req(	_sap.getReqApi(),
									payload,
									srh
									);

	_sap.postSync(req);

	return payload._value;
	}

uint8_t	Sync::getINT_GEN_SRC_G(	
						) noexcept{

	Oscl::Mt::Itc::SyncReturnHandler		srh;

	Req::Api::GetINT_GEN_SRC_GPayload		payload;

	Req::Api::GetINT_GEN_SRC_GReq	req(	_sap.getReqApi(),
									payload,
									srh
									);

	_sap.postSync(req);

	return payload._value;
	}

void	Sync::setORIENT_CFG_G(	
						uint8_t	value
						) noexcept{

	Oscl::Mt::Itc::SyncReturnHandler		srh;

	Req::Api::SetORIENT_CFG_GPayload		payload(
									value
									);

	Req::Api::SetORIENT_CFG_GReq	req(	_sap.getReqApi(),
									payload,
									srh
									);

	_sap.postSync(req);
	}

uint8_t	Sync::getORIENT_CFG_G(	
						) noexcept{

	Oscl::Mt::Itc::SyncReturnHandler		srh;

	Req::Api::GetORIENT_CFG_GPayload		payload;

	Req::Api::GetORIENT_CFG_GReq	req(	_sap.getReqApi(),
									payload,
									srh
									);

	_sap.postSync(req);

	return payload._value;
	}

void	Sync::setCTRL_REG3_G(	
						uint8_t	value
						) noexcept{

	Oscl::Mt::Itc::SyncReturnHandler		srh;

	Req::Api::SetCTRL_REG3_GPayload		payload(
									value
									);

	Req::Api::SetCTRL_REG3_GReq	req(	_sap.getReqApi(),
									payload,
									srh
									);

	_sap.postSync(req);
	}

uint8_t	Sync::getCTRL_REG3_G(	
						) noexcept{

	Oscl::Mt::Itc::SyncReturnHandler		srh;

	Req::Api::GetCTRL_REG3_GPayload		payload;

	Req::Api::GetCTRL_REG3_GReq	req(	_sap.getReqApi(),
									payload,
									srh
									);

	_sap.postSync(req);

	return payload._value;
	}

void	Sync::setCTRL_REG2_G(	
						uint8_t	value
						) noexcept{

	Oscl::Mt::Itc::SyncReturnHandler		srh;

	Req::Api::SetCTRL_REG2_GPayload		payload(
									value
									);

	Req::Api::SetCTRL_REG2_GReq	req(	_sap.getReqApi(),
									payload,
									srh
									);

	_sap.postSync(req);
	}

uint8_t	Sync::getCTRL_REG2_G(	
						) noexcept{

	Oscl::Mt::Itc::SyncReturnHandler		srh;

	Req::Api::GetCTRL_REG2_GPayload		payload;

	Req::Api::GetCTRL_REG2_GReq	req(	_sap.getReqApi(),
									payload,
									srh
									);

	_sap.postSync(req);

	return payload._value;
	}

void	Sync::setCTRL_REG1_G(	
						uint8_t	value
						) noexcept{

	Oscl::Mt::Itc::SyncReturnHandler		srh;

	Req::Api::SetCTRL_REG1_GPayload		payload(
									value
									);

	Req::Api::SetCTRL_REG1_GReq	req(	_sap.getReqApi(),
									payload,
									srh
									);

	_sap.postSync(req);
	}

uint8_t	Sync::getCTRL_REG1_G(	
						) noexcept{

	Oscl::Mt::Itc::SyncReturnHandler		srh;

	Req::Api::GetCTRL_REG1_GPayload		payload;

	Req::Api::GetCTRL_REG1_GReq	req(	_sap.getReqApi(),
									payload,
									srh
									);

	_sap.postSync(req);

	return payload._value;
	}

uint8_t	Sync::getWHO_AM_I(	
						) noexcept{

	Oscl::Mt::Itc::SyncReturnHandler		srh;

	Req::Api::GetWHO_AM_IPayload		payload;

	Req::Api::GetWHO_AM_IReq	req(	_sap.getReqApi(),
									payload,
									srh
									);

	_sap.postSync(req);

	return payload._value;
	}

void	Sync::setINT2_CTRL(	
						uint8_t	value
						) noexcept{

	Oscl::Mt::Itc::SyncReturnHandler		srh;

	Req::Api::SetINT2_CTRLPayload		payload(
									value
									);

	Req::Api::SetINT2_CTRLReq	req(	_sap.getReqApi(),
									payload,
									srh
									);

	_sap.postSync(req);
	}

uint8_t	Sync::getINT2_CTRL(	
						) noexcept{

	Oscl::Mt::Itc::SyncReturnHandler		srh;

	Req::Api::GetINT2_CTRLPayload		payload;

	Req::Api::GetINT2_CTRLReq	req(	_sap.getReqApi(),
									payload,
									srh
									);

	_sap.postSync(req);

	return payload._value;
	}

void	Sync::setINT1_CTRL(	
						uint8_t	value
						) noexcept{

	Oscl::Mt::Itc::SyncReturnHandler		srh;

	Req::Api::SetINT1_CTRLPayload		payload(
									value
									);

	Req::Api::SetINT1_CTRLReq	req(	_sap.getReqApi(),
									payload,
									srh
									);

	_sap.postSync(req);
	}

uint8_t	Sync::getINT1_CTRL(	
						) noexcept{

	Oscl::Mt::Itc::SyncReturnHandler		srh;

	Req::Api::GetINT1_CTRLPayload		payload;

	Req::Api::GetINT1_CTRLReq	req(	_sap.getReqApi(),
									payload,
									srh
									);

	_sap.postSync(req);

	return payload._value;
	}

void	Sync::setREFERENCE_G(	
						uint8_t	value
						) noexcept{

	Oscl::Mt::Itc::SyncReturnHandler		srh;

	Req::Api::SetREFERENCE_GPayload		payload(
									value
									);

	Req::Api::SetREFERENCE_GReq	req(	_sap.getReqApi(),
									payload,
									srh
									);

	_sap.postSync(req);
	}

uint8_t	Sync::getREFERENCE_G(	
						) noexcept{

	Oscl::Mt::Itc::SyncReturnHandler		srh;

	Req::Api::GetREFERENCE_GPayload		payload;

	Req::Api::GetREFERENCE_GReq	req(	_sap.getReqApi(),
									payload,
									srh
									);

	_sap.postSync(req);

	return payload._value;
	}

void	Sync::setINT_GEN_DUR_XL(	
						uint8_t	value
						) noexcept{

	Oscl::Mt::Itc::SyncReturnHandler		srh;

	Req::Api::SetINT_GEN_DUR_XLPayload		payload(
									value
									);

	Req::Api::SetINT_GEN_DUR_XLReq	req(	_sap.getReqApi(),
									payload,
									srh
									);

	_sap.postSync(req);
	}

uint8_t	Sync::getINT_GEN_DUR_XL(	
						) noexcept{

	Oscl::Mt::Itc::SyncReturnHandler		srh;

	Req::Api::GetINT_GEN_DUR_XLPayload		payload;

	Req::Api::GetINT_GEN_DUR_XLReq	req(	_sap.getReqApi(),
									payload,
									srh
									);

	_sap.postSync(req);

	return payload._value;
	}

void	Sync::setINT_GEN_THS_Z_XL(	
						uint8_t	value
						) noexcept{

	Oscl::Mt::Itc::SyncReturnHandler		srh;

	Req::Api::SetINT_GEN_THS_Z_XLPayload		payload(
									value
									);

	Req::Api::SetINT_GEN_THS_Z_XLReq	req(	_sap.getReqApi(),
									payload,
									srh
									);

	_sap.postSync(req);
	}

uint8_t	Sync::getINT_GEN_THS_Z_XL(	
						) noexcept{

	Oscl::Mt::Itc::SyncReturnHandler		srh;

	Req::Api::GetINT_GEN_THS_Z_XLPayload		payload;

	Req::Api::GetINT_GEN_THS_Z_XLReq	req(	_sap.getReqApi(),
									payload,
									srh
									);

	_sap.postSync(req);

	return payload._value;
	}

void	Sync::setINT_GEN_THS_Y_XL(	
						uint8_t	value
						) noexcept{

	Oscl::Mt::Itc::SyncReturnHandler		srh;

	Req::Api::SetINT_GEN_THS_Y_XLPayload		payload(
									value
									);

	Req::Api::SetINT_GEN_THS_Y_XLReq	req(	_sap.getReqApi(),
									payload,
									srh
									);

	_sap.postSync(req);
	}

uint8_t	Sync::getINT_GEN_THS_Y_XL(	
						) noexcept{

	Oscl::Mt::Itc::SyncReturnHandler		srh;

	Req::Api::GetINT_GEN_THS_Y_XLPayload		payload;

	Req::Api::GetINT_GEN_THS_Y_XLReq	req(	_sap.getReqApi(),
									payload,
									srh
									);

	_sap.postSync(req);

	return payload._value;
	}

void	Sync::setINT_GEN_THS_X_XL(	
						uint8_t	value
						) noexcept{

	Oscl::Mt::Itc::SyncReturnHandler		srh;

	Req::Api::SetINT_GEN_THS_X_XLPayload		payload(
									value
									);

	Req::Api::SetINT_GEN_THS_X_XLReq	req(	_sap.getReqApi(),
									payload,
									srh
									);

	_sap.postSync(req);
	}

uint8_t	Sync::getINT_GEN_THS_X_XL(	
						) noexcept{

	Oscl::Mt::Itc::SyncReturnHandler		srh;

	Req::Api::GetINT_GEN_THS_X_XLPayload		payload;

	Req::Api::GetINT_GEN_THS_X_XLReq	req(	_sap.getReqApi(),
									payload,
									srh
									);

	_sap.postSync(req);

	return payload._value;
	}

void	Sync::setINT_GEN_CFG_XL(	
						uint8_t	value
						) noexcept{

	Oscl::Mt::Itc::SyncReturnHandler		srh;

	Req::Api::SetINT_GEN_CFG_XLPayload		payload(
									value
									);

	Req::Api::SetINT_GEN_CFG_XLReq	req(	_sap.getReqApi(),
									payload,
									srh
									);

	_sap.postSync(req);
	}

uint8_t	Sync::getINT_GEN_CFG_XL(	
						) noexcept{

	Oscl::Mt::Itc::SyncReturnHandler		srh;

	Req::Api::GetINT_GEN_CFG_XLPayload		payload;

	Req::Api::GetINT_GEN_CFG_XLReq	req(	_sap.getReqApi(),
									payload,
									srh
									);

	_sap.postSync(req);

	return payload._value;
	}

void	Sync::setACT_DUR(	
						uint8_t	value
						) noexcept{

	Oscl::Mt::Itc::SyncReturnHandler		srh;

	Req::Api::SetACT_DURPayload		payload(
									value
									);

	Req::Api::SetACT_DURReq	req(	_sap.getReqApi(),
									payload,
									srh
									);

	_sap.postSync(req);
	}

uint8_t	Sync::getACT_DUR(	
						) noexcept{

	Oscl::Mt::Itc::SyncReturnHandler		srh;

	Req::Api::GetACT_DURPayload		payload;

	Req::Api::GetACT_DURReq	req(	_sap.getReqApi(),
									payload,
									srh
									);

	_sap.postSync(req);

	return payload._value;
	}

void	Sync::setACT_THS(	
						uint8_t	value
						) noexcept{

	Oscl::Mt::Itc::SyncReturnHandler		srh;

	Req::Api::SetACT_THSPayload		payload(
									value
									);

	Req::Api::SetACT_THSReq	req(	_sap.getReqApi(),
									payload,
									srh
									);

	_sap.postSync(req);
	}

uint8_t	Sync::getACT_THS(	
						) noexcept{

	Oscl::Mt::Itc::SyncReturnHandler		srh;

	Req::Api::GetACT_THSPayload		payload;

	Req::Api::GetACT_THSReq	req(	_sap.getReqApi(),
									payload,
									srh
									);

	_sap.postSync(req);

	return payload._value;
	}

