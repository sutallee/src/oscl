/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/


#include <string.h>
#include "part.h"
#include "oscl/error/info.h"
#include "oscl/hw/st/lsm9ds1/ag.h"
#include "oscl/decoder/linear/le/base.h"
#include "oscl/mt/thread.h"

using namespace Oscl::Driver::ST::LSM9DS1::AG;

Part::Part(
	Oscl::Mt::Itc::PostMsgApi&	papi,
	Oscl::Driver::ST::
	LSM9DS1::AG::Register::Api&	api,
	Oscl::Event::Observer::
	Req::Api::SAP&				tickSAP
	) noexcept:
		_papi(papi),
		_api(api),
		_tickSAP(tickSAP),
		_accelTickRespComposer(
			*this,
			&Part::accelTickResponse,
			0
			),
		_accelTickResp(
			tickSAP.getReqApi(),
			_accelTickRespComposer,
			papi,
			_accelTickPayload
			),
		_accelOutput(papi),
		_gyroOutput(papi),
		_temperatureOutput(
			papi,
			nan(""),
			0.1
			)
		{
	}

bool	Part::checkID() noexcept{
	// Get A/G ID
	uint8_t	id	= _api.getWHO_AM_I();

	if(id != (Oscl::HW::ST::LSM9DS1::AccelGyro::WhoAmI::ID::Value_AccelerometerGyroID >> Oscl::HW::ST::LSM9DS1::AccelGyro::WhoAmI::ID::Lsb)){
		Oscl::Error::Info::log(
			"BAD LSM9DS1 WHO_AM_I ID:"
			" expected: AccelerometerGyroID (0x%2.2X),"
			" read: 0x%2.2X"
			"\n",
			Oscl::HW::ST::LSM9DS1::AccelGyro::WhoAmI::ID::Value_AccelerometerGyroID >> Oscl::HW::ST::LSM9DS1::AccelGyro::WhoAmI::ID::Lsb,
			id
			);
		return true;
		}

	return false;
	}

bool	Part::boot() noexcept{
	// Reboot memory content
	_api.setCTRL_REG8(
		Oscl::HW::ST::LSM9DS1::AccelGyro::CtrlReg8::BOOT::Value_RebootMemoryContent << Oscl::HW::ST::LSM9DS1::AccelGyro::CtrlReg8::BOOT::Lsb
		);

	// Wait 100ms for IMU boot
	Oscl::Mt::Thread::sleep(100);

	return false;
	}

bool	Part::configureCTRL_REG1_G() noexcept {
	uint8_t
	odr_g	= Oscl::HW::ST::LSM9DS1::AccelGyro::CtrlReg1G::ODR_G::Value__119Hz_Cutoff38Hz;

	uint8_t
	fs_g	= Oscl::HW::ST::LSM9DS1::AccelGyro::CtrlReg1G::FS_G::Value__245dps;

	uint8_t
	bw_g	= Oscl::HW::ST::LSM9DS1::AccelGyro::CtrlReg1G::BW_G::Value_Default;

	uint8_t
	value	= (odr_g << Oscl::HW::ST::LSM9DS1::AccelGyro::CtrlReg1G::ODR_G::Lsb);
	value	|= (fs_g << Oscl::HW::ST::LSM9DS1::AccelGyro::CtrlReg1G::FS_G::Lsb);
	value	|= (bw_g << Oscl::HW::ST::LSM9DS1::AccelGyro::CtrlReg1G::BW_G::Lsb);

	_api.setCTRL_REG1_G(value);

	return false;
	}

bool	Part::configureCTRL_REG6_XL() noexcept {
	uint8_t
	odr_xl	= Oscl::HW::ST::LSM9DS1::AccelGyro::CtrlReg6Xl::ODR_XL::Value__50Hz;

	uint8_t
	bw_scal_odr	= Oscl::HW::ST::LSM9DS1::AccelGyro::CtrlReg6Xl::BW_SCAL_ODR::Value_DeterminedByODR;

	uint8_t
	fs_xl	= Oscl::HW::ST::LSM9DS1::AccelGyro::CtrlReg6Xl::FS_XL::Value_PlusOrMinus2g;

	uint8_t
	bw_xl	= Oscl::HW::ST::LSM9DS1::AccelGyro::CtrlReg6Xl::BW_XL::Value__211Hz;

	uint8_t
	value	= (odr_xl << Oscl::HW::ST::LSM9DS1::AccelGyro::CtrlReg6Xl::ODR_XL::Lsb);
	value	|= (bw_scal_odr << Oscl::HW::ST::LSM9DS1::AccelGyro::CtrlReg6Xl::BW_SCAL_ODR::Lsb);
	value	|= (fs_xl << Oscl::HW::ST::LSM9DS1::AccelGyro::CtrlReg6Xl::FS_XL::Lsb);
	value	|= (bw_xl << Oscl::HW::ST::LSM9DS1::AccelGyro::CtrlReg6Xl::BW_XL::Lsb);

	_api.setCTRL_REG6_XL(value);

	return false;
	}

bool	Part::configureCTRL_REG7_XL() noexcept {
	// ctrl7 = 0; GOOD
	//Bug: Bad things happen.
	//ctrl7 = 0x05; BAD
	// HPIS1 == 1, FDS == 1

	uint8_t
	hr	= Oscl::HW::ST::LSM9DS1::AccelGyro::CtrlReg7Xl::HR::Value_Disable;

	uint8_t
	dcf	= Oscl::HW::ST::LSM9DS1::AccelGyro::CtrlReg7Xl::DCF::Value_Default;

	uint8_t
	fds	= Oscl::HW::ST::LSM9DS1::AccelGyro::CtrlReg7Xl::FDS::Value_InternalFilterBypassed;

	uint8_t
	hpis1	= Oscl::HW::ST::LSM9DS1::AccelGyro::CtrlReg7Xl::HPIS1::Value_FilterBypassed;

	uint8_t
	value	= (hr << Oscl::HW::ST::LSM9DS1::AccelGyro::CtrlReg7Xl::HR::Lsb);
	value	|= (dcf << Oscl::HW::ST::LSM9DS1::AccelGyro::CtrlReg7Xl::DCF::Lsb);
	value	|= (fds << Oscl::HW::ST::LSM9DS1::AccelGyro::CtrlReg7Xl::FDS::Lsb);
	value	|= (hpis1 << Oscl::HW::ST::LSM9DS1::AccelGyro::CtrlReg7Xl::HPIS1::Lsb);

	_api.setCTRL_REG7_XL(value);

	return false;
	}

void	Part::start() noexcept{
	checkID();

	if(boot()){
		return;
		}

	if(checkID()){
		return;
		}

	configureCTRL_REG6_XL();
	configureCTRL_REG7_XL();
	configureCTRL_REG8();
	configureCTRL_REG1_G();

	_tickSAP.post(_accelTickResp.getSrvMsg());
	}

Oscl::XYZ::Observer::Req::Api::SAP&	Part::getAccelSAP() noexcept{
	return _accelOutput.getSAP();
	}

Oscl::XYZ::Observer::Req::Api::SAP&	Part::getGyroSAP() noexcept{
	return _gyroOutput.getSAP();
	}

Oscl::Double::Observer::Req::Api::SAP&	Part::getTemperatureSAP() noexcept{
	return _temperatureOutput.getSAP();
	}

bool	Part::configureCTRL_REG8() noexcept{
	// The main purpose of this function is to
	// inable the auto-increment for register access.
	// This improves the throughput over the I2C bus.

	constexpr uint8_t	ctrl_reg8	=
			(	Oscl::HW::ST::LSM9DS1::AccelGyro::CtrlReg8::BOOT::Value_Default
				<< Oscl::HW::ST::LSM9DS1::AccelGyro::CtrlReg8::BOOT::Lsb
				)
		|	(	Oscl::HW::ST::LSM9DS1::AccelGyro::CtrlReg8::BDU::Value_Default
				<< Oscl::HW::ST::LSM9DS1::AccelGyro::CtrlReg8::BDU::Lsb
				)
		|	(	Oscl::HW::ST::LSM9DS1::AccelGyro::CtrlReg8::H_LACTIVE::Value_Default
				<< Oscl::HW::ST::LSM9DS1::AccelGyro::CtrlReg8::H_LACTIVE::Lsb
				)
		|	(	Oscl::HW::ST::LSM9DS1::AccelGyro::CtrlReg8::PP_OD::Value_Default
				<< Oscl::HW::ST::LSM9DS1::AccelGyro::CtrlReg8::PP_OD::Lsb
				)
		|	(	Oscl::HW::ST::LSM9DS1::AccelGyro::CtrlReg8::SIM::Value_Default
				<< Oscl::HW::ST::LSM9DS1::AccelGyro::CtrlReg8::SIM::Lsb
				)
		|	(	Oscl::HW::ST::LSM9DS1::AccelGyro::CtrlReg8::IF_ADD_INC::Value_Enable
				<< Oscl::HW::ST::LSM9DS1::AccelGyro::CtrlReg8::IF_ADD_INC::Lsb
				)
		|	(	Oscl::HW::ST::LSM9DS1::AccelGyro::CtrlReg8::BLE::Value_Default
				<< Oscl::HW::ST::LSM9DS1::AccelGyro::CtrlReg8::BLE::Lsb
				)
		|	(	Oscl::HW::ST::LSM9DS1::AccelGyro::CtrlReg8::SW_RESET::Value_Default
				<< Oscl::HW::ST::LSM9DS1::AccelGyro::CtrlReg8::SW_RESET::Lsb
				)
		;

	_api.setCTRL_REG8(
		ctrl_reg8
		);

	return false;
	}

bool	Part::readAccelerometer() noexcept{
	uint8_t	buffer[6];

	memset(buffer,0,sizeof(buffer));

	_api.getOUT_XL(
		sizeof(buffer),
		buffer
		);

	Oscl::Decoder::Linear::LE::Base	leDecoder(
		buffer,
		sizeof(buffer)
		);

	Oscl::Decoder::Api&	decoder	= leDecoder;

	int16_t	x;
	int16_t	y;
	int16_t	z;

	decoder.decode(x);
	decoder.decode(y);
	decoder.decode(z);

	Oscl::XYZ::Vector	v(
		x,
		y,
		z
		);

	double	fs	= 2.0;	// CTRL_REG6_XL FS_XL field
	constexpr double	bits = 32768.0; // 2^15
	double	scale = fs/bits;

	v._x	*= scale;
	v._y	*= scale;
	v._z	*= scale;

	_accelOutput.update(v);

	return false;
	}

bool	Part::readGyro() noexcept{
	uint8_t	buffer[6];

	memset(buffer,0,sizeof(buffer));

	_api.getOUT_G(
		sizeof(buffer),
		buffer
		);

	Oscl::Decoder::Linear::LE::Base	leDecoder(
		buffer,
		sizeof(buffer)
		);

	Oscl::Decoder::Api&	decoder	= leDecoder;

	int16_t	x;
	int16_t	y;
	int16_t	z;

	decoder.decode(x);
	decoder.decode(y);
	decoder.decode(z);

	Oscl::XYZ::Vector	v(
		x,
		y,
		z
		);

	double	fs	= 245;	// dps CTRL_REG1_G FS field
	constexpr double	bits = 32768.0; // 2^15
	double	scale = fs/bits;

	v._x	*= scale;
	v._y	*= scale;
	v._z	*= scale;

	_gyroOutput.update(v);

	return false;
	}

bool	Part::readTemperature() noexcept{
	uint8_t	buffer[2];

	memset(buffer,0,sizeof(buffer));

	_api.getOUT_TEMP(
		sizeof(buffer),
		buffer
		);

	Oscl::Decoder::Linear::LE::Base	leDecoder(
		buffer,
		sizeof(buffer)
		);

	Oscl::Decoder::Api&	decoder	= leDecoder;

	int16_t	temperature;

	decoder.decode(temperature);

	double	scale	= 1.0/16.0;
	double	bias	= 27.5;

	_temperatureOutput.update(
		(temperature * scale) + bias
		);

	return false;
	}

void	Part::accelTickResponse(Oscl::Event::Observer::Resp::Api::OccurrenceResp& msg) noexcept{

	readAccelerometer();
	readGyro();
	readTemperature();

	_tickSAP.post(msg.getSrvMsg());
	}

