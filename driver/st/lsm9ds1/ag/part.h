/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/


#ifndef _oscl_driver_st_lsm9ds1_ag_parth_
#define _oscl_driver_st_lsm9ds1_ag_parth_

#include "oscl/driver/st/lsm9ds1/ag/register/api.h"
#include "oscl/mt/itc/postmsgapi.h"
#include "oscl/event/observer/respcomp.h"
#include "oscl/xyz/output/part.h"
#include "oscl/double/output/part.h"

/** */
namespace Oscl {

/** */
namespace Driver {

/** */
namespace ST {

/** */
namespace LSM9DS1 {

/** */
namespace AG {

/** */
class Part {
	private:
		/** */
		Oscl::Mt::Itc::PostMsgApi&						_papi;

		/** */
		Oscl::Driver::ST::LSM9DS1::AG::Register::Api&	_api;

		/** */
		Oscl::Event::Observer::Req::Api::SAP&			_tickSAP;

	private:
		/** */
		Oscl::Event::Observer::Resp::Composer<Part>		_accelTickRespComposer;

		/** */
		Oscl::Event::Observer::Req::Api::OccurrencePayload	_accelTickPayload;

		/** */
		Oscl::Event::Observer::Resp::Api::OccurrenceResp		_accelTickResp;

	private:
		/** */
		Oscl::XYZ::Output::Part							_accelOutput;

		/** */
		Oscl::XYZ::Output::Part							_gyroOutput;

		/** */
		Oscl::Double::Output::Part						_temperatureOutput;

	public:
		/** */
		Part(
			Oscl::Mt::Itc::PostMsgApi&	papi,
			Oscl::Driver::ST::
			LSM9DS1::AG::Register::Api&	api,
			Oscl::Event::Observer::
			Req::Api::SAP&				tickSAP
			) noexcept;

		/** */
		void	start() noexcept;

		/** */
		Oscl::XYZ::Observer::Req::Api::SAP&	getAccelSAP() noexcept;

		/** */
		Oscl::XYZ::Observer::Req::Api::SAP&	getGyroSAP() noexcept;

		/** */
		Oscl::Double::Observer::Req::Api::SAP&	getTemperatureSAP() noexcept;

	private:
		/** RETURN: true on failure. */
		bool	checkID() noexcept;

		/** RETURN: true on failure. */
		bool	boot() noexcept;

		/** RETURN: true on failure. */
		bool	configureCTRL_REG1_G() noexcept;

		/** RETURN: true on failure. */
		bool	configureCTRL_REG6_XL() noexcept;

		/** RETURN: true on failure. */
		bool	configureCTRL_REG7_XL() noexcept;

		/** */
		bool	configureCTRL_REG8() noexcept;

		/** RETURN: true on failure. */
		bool	readAccelerometer() noexcept;

		/** RETURN: true on failure. */
		bool	readGyro() noexcept;

		/** RETURN: true on failure. */
		bool	readTemperature() noexcept;

	private: // Oscl::Event::Observer::Resp::Composer _accelTickRespComposer
		/**	*/
		void	accelTickResponse(Oscl::Event::Observer::Resp::Api::OccurrenceResp& msg) noexcept;

		/**	*/
//		void	accelTickResponse(Oscl::Event::Observer::Resp::Api::CancelOccurrenceResp& msg) noexcept;
	};

}
}
}
}
}

#endif
