/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/


#ifndef _oscl_driver_st_lsm9ds1_parth_
#define _oscl_driver_st_lsm9ds1_parth_

#include "oscl/driver/st/lsm9ds1/ag/part.h"
#include "oscl/driver/st/lsm9ds1/mag/part.h"

/** */
namespace Oscl {

/** */
namespace Driver {

/** */
namespace ST {

/** */
namespace LSM9DS1 {

/** */
class Part {
	private:
		/** */
		Oscl::Driver::ST::LSM9DS1::AG::Part		_agPart;

		/** */
		Oscl::Driver::ST::LSM9DS1::MAG::Part	_magPart;

	public:
		/** */
		Part(
			Oscl::Mt::Itc::PostMsgApi&		papi,
			Oscl::Driver::ST::
			LSM9DS1::AG::Register::Api&		agApi,
			Oscl::Driver::ST::
			LSM9DS1::MAG::Register::Api&	magApi,
			Oscl::Event::Observer::
			Req::Api::SAP&					agTickSAP,
			Oscl::Event::Observer::
			Req::Api::SAP&					magTickSAP
			) noexcept;

		/** */
		void	start() noexcept;

		/** */
		Oscl::XYZ::Observer::Req::Api::SAP&	getAccelSAP() noexcept;

		/** */
		Oscl::XYZ::Observer::Req::Api::SAP&	getGyroSAP() noexcept;

		/** */
		Oscl::Double::Observer::Req::Api::SAP&	getTemperatureSAP() noexcept;

		/** */
		Oscl::XYZ::Observer::Req::Api::SAP&	getMagnetometerSAP() noexcept;
	};

}
}
}
}

#endif
