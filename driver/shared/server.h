/*
   Copyright (C) 2024 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_driver_shared_serverh_
#define _oscl_driver_shared_serverh_

#include "oscl/mt/itc/mbox/server.h"
#include "symbiont.h"
#include "oscl/queue/queue.h"

/** */
namespace Oscl {
/** */
namespace Driver {
/** */
namespace Shared {

// ~/root/src/oscl/bluetooth/mesh/element/symbiont/host/api.h
/** This server is intended to allow multiple drivers
	to share a single thread (stack). Each driver registers/attaches
	a symbiont to this server and then has its interrupt service
	routine (ISR) signal this Server. When this server is awakened
	by the any of the interrupts, it invokes the process() operation
	of each of the symbionts, giving each the opportunity to take
	appropriate action as necessary.
 */
class Server :	public Oscl::Mt::Itc::Server {
	private:
		/** */
		Oscl::Queue< Oscl::Driver::Shared::Symbiont::Api >	_symbionts;

	public:
		/** */
		Server() noexcept;

		/** */
		virtual ~Server(){}

		/** */
		Oscl::Mt::Runnable&						getRunnable() noexcept;

		/**	This operation attaches the symbiont to the
			host server.
			IMPORTANT: This must only be invoked before
			the host server thread is started. It is NOT
			thread-safe.
		 */
		void	attach(	
					Oscl::Driver::Shared::Symbiont::Api&	symbiont
					) noexcept;

	private:	// Server
		/** */
		void	initialize() noexcept;
		/** */
		void	mboxSignaled() noexcept;

	};

}
}
}

#endif
