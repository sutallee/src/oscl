/*
   Copyright (C) 2024 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_driver_shared_symbionth_
#define _oscl_driver_shared_symbionth_

#include <stdint.h>
#include "oscl/queue/queueitem.h"

/** */
namespace Oscl {
/** */
namespace Driver {
/** */
namespace Shared {
/** */
namespace Symbiont {

/** */
class Api : public Oscl::QueueItem {
	public:
		/** This is invoked once during the host
			server initialize() operation.
		 */
		virtual void	initialize() noexcept=0;

		/** This is invoked each time the HOST
			server mboxSignaled() operation
			is invoked.
		 */
		virtual void	process() noexcept=0;

	};

}
}
}
}

#endif

