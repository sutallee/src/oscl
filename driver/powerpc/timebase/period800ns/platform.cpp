/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "oscl/timer/counter.h"
#include "oscl/driver/powerpc/asm.h"

/** This operation divides the 64-bit value by a
	16-bit divisor to produce a 32-bit result.
 */
inline unsigned long	divide(	unsigned long	msw,
								unsigned long	lsw,
								unsigned short	divisor
								) noexcept{
	unsigned long	digit;

	digit	= (msw >> 16);					// digit 1	upper of msw
	digit	%= divisor;
	digit	<<= 16;

	digit	+= (msw & 0x0000FFFF);			// digit 2	lower of msw
	digit	%= divisor;
	digit	<<= 16;

	digit	+= (lsw >> 16);					// digit 3	upper of lsw
	unsigned long	r3		= digit / divisor;
	digit	%= divisor;
	digit	<<= 16;

	digit	+= (lsw & 0x0000FFFF);			// digit 4 lower of lsw
	unsigned long	r4		= digit / divisor;

	unsigned long	result	= (r3<<16)+(r4<<0);

	return result;
	}

/** This operation performs a scaling function on a
	64-bit value where:
	- mUpper is the most significant 32 bits of a 64 bit value
	- mLower is the least significant 32 bits of a 64 bit value
	Only the least significant 32 bits of the result is returned.
	The 32 bit result "Y" is a function of the 64-bit value "X"
	according to the function:

		Y	= (X * multiplier)/divisor
 */
static inline unsigned long scale(		unsigned short	multiplier,
										unsigned long	mUpper,
										unsigned long	mLower,
										unsigned short	divisor
										){
	unsigned long long	iUpper	= mUpper;
	unsigned long long	iLower	= mLower;
	iUpper	*= multiplier;
	iLower	*= multiplier;

	unsigned long		iLowerUpper	= (unsigned long)(iLower>>32);
	unsigned long		iLowerLower	= (unsigned long)iLower;

	unsigned long long	sumUpper	= iUpper + iLowerUpper;

	unsigned long	sw2	= (unsigned long)(sumUpper>>32);
	unsigned long	sw1	= (unsigned long)sumUpper;
	unsigned long	sw0	= iLowerLower;

	unsigned long	digit	= (sw2<<16);	// digit 1	lower of sw2
	digit	%= divisor;
	digit	<<= 16;

	digit	+= (sw1 >> 16);					// digit 2	upper of sw1
	digit	%= divisor;
	digit	<<= 16;

	digit	+= (sw1 & 0x0000FFFF);			// digit 3	lower of sw1
	digit	%= divisor;
	digit	<<= 16;

	digit	+= (sw0 >> 16);					// digit 4	upper of sw0
	unsigned long	r3		= digit / divisor;
	digit	%= divisor;
	digit	<<= 16;

	digit	+= (sw0 & 0x0000FFFF);			// digit 5 lower of sw0
	unsigned long	r4		= digit / divisor;

	unsigned long	result	= (r3<<16)+(r4<<0);

	return result;
	}

unsigned long	OsclTimerGetMicrosecondCounter() noexcept{
	unsigned long	upper	= Oscl::Ppc::mftbu();
	unsigned long	lower;
	do{
		lower	= Oscl::Ppc::mftbl();
		unsigned long	upper2	= Oscl::Ppc::mftbu();
		if(upper == upper2){
			break;
			}
		upper	= upper2;
		} while(true);
	// Convert 800ns resolution to 1us equivalent
	return scale(800,upper,lower,1000);
	}

unsigned long	OsclTimerGetMillisecondCounter() noexcept{
	unsigned long	upper	= Oscl::Ppc::mftbu();
	unsigned long	lower;
	do{
		lower	= Oscl::Ppc::mftbl();
		unsigned long	upper2	= Oscl::Ppc::mftbu();
		if(upper == upper2){
			break;
			}
		upper	= upper2;
		} while(true);
	// Convert 800ns resolution to 1ms equivalent
	return divide(upper,lower,1250);
	}

unsigned long	PlatMicroSecondDiff(	unsigned long	prev,
										unsigned long	current
										) noexcept{
	if(current >= prev){
		return current-prev;
		}
	return prev-current;
	}
