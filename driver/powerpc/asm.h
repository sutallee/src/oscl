/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_powerpc_asmh_
#define _oscl_drv_powerpc_asmh_
#include "oscl/hw/powerpc/uisa.h"
#include "oscl/hw/powerpc/oea.h"

/** */
namespace Oscl {

/** */
namespace Ppc {

// OEA super
inline void mtdec(unsigned long value) noexcept{
    asm volatile("mtdec %0" : : "r" (value));
    }

// OEA user
inline unsigned long mfdec() noexcept{
	unsigned long	reg;
    asm volatile("mfdec %0" : "=r" (reg));
	return reg;
    }

// OEA	super
inline Ppc::Oea::Msr::Reg	mfmsr() noexcept{
	unsigned int rval;
	asm volatile("mfmsr %0" : "=r" (rval));
	return rval;
	}

// OEA	super
inline void	mtmsr(Ppc::Oea::Msr::Reg msr) noexcept{
	asm volatile("mtmsr %0" : : "r" (msr));
	}

// OEA	super
inline void	mtdsisr(Ppc::Oea::Msr::Reg dsisr) noexcept{
	asm volatile("mtdsisr %0" : : "r" (dsisr));
	}

// OEA	super
inline Ppc::Oea::Dsisr::Reg	mfdsisr() noexcept{
	unsigned int rval;
	asm volatile("mfdsisr %0" : "=r" (rval));
	return rval;
	}

// OEA	super
inline void	mtdar(Ppc::Oea::Dar::Reg dar) noexcept{
	asm volatile("mtdar %0" : : "r" (dar));
	}

// OEA	super
inline Ppc::Oea::Dar::Reg	mfdar() noexcept{
	unsigned int rval;
	asm volatile("mfdar %0" : "=r" (rval));
	return rval;
	}

// VEA	user
inline void	eieio() noexcept{
	asm volatile ("eieio" : : : "memory");	// memory?
	}

// UISA	user
inline void	sync() noexcept{
	asm volatile ("sync" : : : "memory");
	}

// VEA	user
inline void	isync() noexcept{
	asm volatile ("isync" : : : "memory");
	}

// OEA	super	optional
inline void	tlbia() noexcept{
	asm volatile ("tlbia" : : );
	}

// OEA	super	optional
inline void	tlbie(void* address) noexcept{
	asm volatile ("tlbie %0" : : "r" (address));
	}

// OEA	super	optional
inline void	tlbsync() noexcept{
	asm volatile ("tlbsync" : : );
	}

// OEA	super
inline void	rfi() noexcept{
	asm volatile ("rfi" : : );
	}

// VEA	user
inline unsigned long mftbl() noexcept{
	unsigned long	reg;
    asm volatile("mftb %0,268" : "=r" (reg));
	return reg;
    }

// VEA	user
inline unsigned long mftbu() noexcept{
	unsigned long	reg;
    asm volatile("mftb %0,269" : "=r" (reg));
	return reg;
    }

// VEA	user	optional
inline void	dcba(void *address) noexcept{
	asm volatile("dcba 0,%0" : : "r" (address));
	}

// VEA	user
inline void	dcbf(void *address) noexcept{
	asm volatile("dcbf 0,%0" : : "r" (address));
	}

// OEA	super
inline void	dcbi(void *address) noexcept{
	asm volatile("dcbi 0,%0" : : "r" (address));
	}

// VEA	user
inline void	dcbst(void *address) noexcept{
	asm volatile("dcbst 0,%0" : : "r" (address));
	}

// VEA	user
inline void	dcbt(void *address) noexcept{
	asm volatile("dcbt 0,%0" : : "r" (address));
	}

// VEA	user
inline void	dcbtst(void *address) noexcept{
	asm volatile("dcbtst 0,%0" : : "r" (address));
	}

// VEA	user
inline void	dcbz(void *address) noexcept{
	asm volatile("dcbz 0,%0" : : "r" (address));
	}

// VEA	user
inline void	icbi(void *address) noexcept{
	asm volatile("icbi 0,%0" : : "r" (address));
	}

// UISA	user
// Sets reservation for the "address" within the processor specific
// granularity (typically cache line). Some implementations only allow
// a single oustanding reservation per processor.
inline unsigned long	lwarx(volatile unsigned long *address) noexcept{
	unsigned long	result;
	asm volatile (	"lwarx %0,0,%1\n"
					: "=r" (result)
					: "r" (address)
					);
	return result;
	}

// UISA	user
// This operation returns true if the conditional store succeeds.
// The store will succeed if no other processor/procedure has
// stored to the cache line (granularity) associated with "address".
inline bool	stwcx(volatile unsigned long* address,unsigned long value) noexcept{
	unsigned long	result;
#if 0
	asm volatile ("stwcx. %2,0,%1; beq 0f; li %0,0; b 1f;0:; li %0,1;1:"
					: "=r" (result)
					: "r" (address), "r" (value)
					: "cc"
					);
#else
	asm volatile (	"stwcx. 	%2,0,%1;"
					" mfcr		%0;"
					" rlwinm 	%0,%0,3,31,31;"
					: "=r" (result)
					: "r" (address), "r" (value)
					: "cc"
					);
#endif
	return result;
	}

// UISA	user
inline unsigned long	mulhwu(	unsigned long	multiplicand,
								unsigned long	multiplier
								) noexcept{
	unsigned long	result;
	asm volatile (	"mulhwu %0,%1,%2\n"
					: "=r" (result)
					: "r" (multiplicand), "r" (multiplier)
					);
	return result;
	}

// UISA	user
inline unsigned long	mullw(	unsigned long	multiplicand,
								unsigned long	multiplier
								) noexcept{
	unsigned long	result;
	asm volatile (	"mullw %0,%1,%2\n"
					: "=r" (result)
					: "r" (multiplicand), "r" (multiplier)
					);
	return result;
	}
}
}

#endif
