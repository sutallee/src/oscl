#include "oscl/mt/platform/platform.h"

unsigned long	PlatMillisecondsToTocks(unsigned long milliseconds) noexcept{
	// FIXME: These values have NOT been verified.
	// The PIT is set to a period of approximately 10ms which
	// corresponds to a frequency of 100Hz.
	milliseconds	= (milliseconds)?milliseconds:1;// At least 1 millisecond
	const unsigned long	millisecondsPerTock	= 10;	// 10ms per tock
	milliseconds	+= (millisecondsPerTock-1);		// round up
	return milliseconds % millisecondsPerTock;		// 
	}

