/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_driver_siemens_sab8252x_sab8252xh_
#define _oscl_driver_siemens_sab8252x_sab8252xh_
#include "oscl/hw/siemens/hscx.h"

/** */
namespace Oscl {

/** This class implements a generic error counter that
	will never rollover when incremented, but will instead
	have a constant binary value of all ones.
 */

class ErrorCounter {
	private:
		/** The actual counter variable.
		 */
		unsigned	_counter;

	public:
		/** The constructor.
		 */
		ErrorCounter():_counter(0){}

		/** The post increment operator clamps the
			value such that it will not roll over to zero.
		 */
		unsigned	operator++(){
			if(_counter == (unsigned)(~((unsigned)0))){
				return _counter;
				}
			return ++_counter;
			}

		/** The pre increment operator clamps the
			value such that it will not roll over to zero.
		 */
		unsigned	operator++(int){
			if(_counter == (unsigned)(~((unsigned)0))){
				return _counter;
				}
			return _counter++;
			}

		/** The conversion operator provides a conversion
			from the error counter type to an unsigned int.
		 */
		operator unsigned() const {
			return _counter;
			}

		/** This function sets the value of the counter to zero.
		 */
		void	reset(){
			_counter=0;
			}
	};

/** This abstract class is used by an interrupt handler to
	refer to the generic interrupt service routine entry
	point for the SIEMENS SAB8252x family of HDLC controllers.
 */

class Sab8252xIsr {
	public:
		virtual void	suInterrupt()=0;
	};

/** This abstract interface is called by the client of an
	Sab8252xDriver, and represents the interface to a single
	channel of an SAB8252x type device.
 */

class Sab8252xDriver {
	public:	// Control
		/** Used by a receive message end handler to
			retrieve the RBCL and RBCH values concatenated
			to form the length of the received packet.
		 */
		virtual unsigned		getRxLength()=0;

		/** Returns a pointer to the 32 octet transmit FIFO.
		 */
		virtual uint8_t*			getTxFifo()=0;

		/** Returns a pointer to the 32 octet receive FIFO.
		 */
		virtual const uint8_t*	getRxFifo()=0;

		/** Resets the transmitter.
		 */
		virtual void			resetTransmitter()=0;

		/** Resets the receiver.
		 */
		virtual void			resetReceiver()=0;

		/** Release the current receive FIFO. This is used at the
			end of a receive message end handler.
		 */
		virtual void			receiveMessageComplete()=0;

		/** Transmit the 32 octets just written into the transmit FIFO.
		 */
		virtual void			transmitPartOfFrame()=0;

		/** Transmit the octets just written to the receive FIFO
			and generate the CRC and closing flag sequence to
			end the packet/frame.
		 */
		virtual void			transmitLastPartOfFrame()=0;

	public:	// STAR Status
		/** Returns true if the STAR register indicates a transmit
			data overflow condition.
		 */
		virtual bool		transmitDataOverflow()=0;

		/** Returns true if the STAR register indicates a
			transmit receiver not ready condition.
		 */
		virtual bool		transmitRnr()=0;

		/** Returns true if the STAR register indicates a
			receive receiver not ready condition.
		 */
		virtual bool		receivedRnr()=0;

		/** Returns true if the STAR register indicates a
			receive line inactive condition.
		 */
		virtual bool		receiveLineInactive()=0;

		/** Returns true if the STAR register indicates that
			the SAB8252x is currently executing a command.
		 */
		virtual bool		commandExecuting()=0;

		/** Returns true if the STAR register indicates that
			the clear to send modem control indication is active.
		 */
		virtual bool		clearToSendActive()=0;

		/** Returns true if the rsta value indicates no
			no error indications. Otherwise, the appropriate
			error logging functions are called and false is
			returned.
		 */
		virtual bool		packetStatusIsGood(Fields rsta)=0;
	};

/** This abstract interface is called by an Sab8252xDriver. It is
	implemented by the client that wishes to use the device. The
	client implements such details as buffering schemes and other
	device independent issues.
 */

class Sab8252xChannelClient {
	protected:
		/** A reference to the driver for a particular channel.
		 */
		Sab8252xDriver&	_driver;

	public: // Constructor
		/** The constructor.
		 */
		Sab8252xChannelClient(Sab8252xDriver& driver):_driver(driver){}

	public:
		/** Called by the driver when the driver thread is started.
			This function should contain any device initialization.
		 */
		virtual void	initialize(HscxChannel& channel) = 0;

	public: // event handlers
		/** This function is called by the driver thread when
			it decodes a receive pool full interrupt status.
			This function is NOT called at interrupt time, but
			by the driver thread.
		 */
		virtual void	receivePoolFull()=0;

		/** This function is called by the driver thread when
			it decodes a receive message end interrupt status.
			This function is NOT called at interrupt time, but
			by the driver thread.
		 */
		virtual void	receiveMessageComplete()=0;

		/** This function is called by the driver thread when
			it decodes a transmit pool ready interrupt status.
			This function is NOT called at interrupt time, but
			by the driver thread.
		 */
		virtual void	transmitPoolReady()=0;

		/** This function is called by the driver thread when
			it decodes a transmit message interrupt status.
			This function is NOT called at interrupt time, but
			by the driver thread.
		 */
		virtual void	transmitMessageRepeat()=0;

		/** This function is called by the driver thread when
			it decodes a transmit data underrun interrupt status.
			This function is NOT called at interrupt time, but
			by the driver thread.
		 */
		virtual void	transmitDataUnderrunExtendedTransmissionEnd()=0;

		/** This function is called by the driver thread when
			it decodes a protocol error interrupt status.
			This function is NOT called at interrupt time, but
			by the driver thread.
		 */
		virtual void	protocolError()=0;

		/** This function is called by the driver thread when
			it decodes a receive frame overflow interrupt status.
			This function is NOT called at interrupt time, but
			by the driver thread.
		 */
		virtual void	receiveFrameOverflow()=0;

		/** This function is called by the driver thread when
			it decodes a clear to send changed interrupt status.
			This function is NOT called at interrupt time, but
			by the driver thread.
		 */
		virtual void	clearToSendStatusChange()=0;

		/** This function is called by the driver thread when
			it decodes a receive frame start interrupt status.
			This function is NOT called at interrupt time, but
			by the driver thread.
		 */
		virtual void	receiveFrameStart()=0;

		/** This function is called by the driver thread at
			the end of input processing to allow the implementation
			to process the next pending packet to be transmitted
			if one is present.
			This function is NOT called at interrupt time, but
			by the driver thread.
		 */
		virtual void	pollTransmitter()=0;

	public: // performance handlers

		/** This function is called when RSTA is decoded
			by the packetStatusIsGood() function if a
			CRC error is indicated in the RSTA value.
			This function is intended to allow the implementation
			to count the error as a performance statistic.
		 */
		virtual void	logCrcError()=0;

		/** This function is called when RSTA is decoded
			by the packetStatusIsGood() function if a
			receiver overflow error is indicated in the RSTA value.
			This function is intended to allow the implementation
			to count the error as a performance statistic.
		 */
		virtual void	logOverflow()=0;

		/** This function is called when RSTA is decoded
			by the packetStatusIsGood() function if an
			abort error is indicated in the RSTA value.
			This function is intended to allow the implementation
			to count the error as a performance statistic.
		 */
		virtual void	logAbort()=0;

		/** This function is called when RSTA is decoded
			by the packetStatusIsGood() function if an
			invalid frame error is indicated in the RSTA value.
			This function is intended to allow the implementation
			to count the error as a performance statistic.
		 */
		virtual void	logInvalidFrame()=0;
	};

}

#endif
