/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_driver_siemens_sab82525_sab82525h_
#define _oscl_driver_siemens_sab82525_sab82525h_
#include "oscl/hw/siemens/hscx.h"
#include "oscl/driver/siemens/sab8252x/sab8252x.h"
#include "oscl/mt/runnable.h"

/** */
namespace Oscl {

/** */
namespace Mt {
/** */
namespace Sema {
	/** */
	class Api;
}
}

/** This class implements a driver for the SIEMENS SAB82525
	dual channel HDLC controller.
 */

class Sab82525Driver : public Sab8252xIsr , public Mt::Runnable {
	private:
		/** This is a reference to the upper layer driver
			implementation for channel A.
		 */
		Sab8252xChannelClient&		_clientA;

		/** This is a reference to the upper layer driver
			implementation for channel B.
		 */
		Sab8252xChannelClient&		_clientB;

		/** This semaphore is signaled by external sources (such as
			an interrupt handler) to indicate the arrival of an
			event and to wake up the driver thread such that it
			services the input sources.
		 */
		Mt::Sema::Api&				_input;

		/** This is a reference to the hardware for the A
			channel of the SAB82525.
		 */
		HscxChannel&				_channelA;

		/** This is a reference to the hardware for the B
			channel of the SAB82525.
		 */
		HscxChannel&				_channelB;

		/** This field contains a shadow copy of the ISTA register
			for channel A.
		 */
		Fields						_istaA;

		/** This field contains a shadow copy of the EXIR register
			for channel B.
		 */
		Fields						_istaB;

		/** This field contains a shadow copy of the EXIR register
			for channel A.
		 */
		Fields						_exirA;

		/** This field contains a shadow copy of the EXIR register
			for channel B.
		 */
		Fields						_exirB;

	public:	// Constructor
		/** The constructor.
		 */
		Sab82525Driver(	Sab8252xChannelClient&	clientA,
						Sab8252xChannelClient&	clientB,
						SAB82525&				chip,
						Mt::Sema::Api&			input
						);

	public:	// Mt::Runnable interface
		/** This function is called when the driver thread is started.
			The main processing loop of the driver is contained within
			this function.
		 */
		void	run() noexcept;

	private:
		/** This function is called by the run function to perform
			any run-time initialization from the driver thread.
		 */
		void	initialize();

		/** This function waits for the Command Execution Complete flag
			of the specified channel to be set and then writes the
			command to the channel command register of the SAB82525.
		 */
		static void	executeCmd(HscxChannel& channel,Flags cmd);

		/** This routine parses the contents the the rsta register
			and calls any error indication routines in the client
			if required. True is returned if there are no error
			indications.
		 */
		static bool	packetStatusIsGood(Sab8252xChannelClient& client,Fields rsta);

	public:	// Sab8252xIsr interface
		/** This routine is called from the interrupt handler for
			the SAB82525 interrupt.
		 */
		void	suInterrupt();

	public: // Channel A driver interface
		/** This function executes the specified command for channel A.
		 */
		void			executeCmdA(Flags cmd);

		/** This function returns true if the tranmit overflow bit of the
			channel A STAR register is set.
		 */
		bool			transmitDataOverflowA();

		/** This function returns true if the transmit receiver not ready bit
			of the channel A STAR register is set.
		 */
		bool			transmitRnrA();

		/** This function returns true if the receiver's receiver not ready bit
			of the channel A STAR register is set.
		 */
		bool			receivedRnrA();

		/** This function returns true if the receive line inactive bit
			of the channel A STAR register is set.
		 */
		bool			receiveLineInactiveA();

		/** This function returns true if the command executing bit
			of the channel A STAR register is set.
		 */
		bool			commandExecutingA();

		/** This function returns true if the clear to send active bit
			of the channel A STAR register is set.
		 */
		bool			clearToSendActiveA();

		/** This function returns a pointer to the 32 byte receive FIFO for channel A.
		 */
		unsigned		getRxLengthA();

		/** This function returns a pointer to the 32 byte transmit FIFO for channel A.
		 */
		uint8_t*			getTxFifoA();

		/** This function returns a pointer to the 32 byte receive FIFO for channel A.
		 */
		const uint8_t*	getRxFifoA();

		/** This function decodes rsta, counts any error indications to channel A, and returns true
			if the status indicates an error free condition.
		 */
		bool			packetStatusIsGoodA(Fields rsta);

	public: // Channel B driver interface
		/** This function executes the specified command for channel B.
		 */
		void			executeCmdB(Flags cmd);

		/** This function returns true if the tranmit overflow bit of the
			channel B STAR register is set.
		 */
		bool			transmitDataOverflowB();

		/** This function returns true if the transmit receiver not ready bit
			of the channel B STAR register is set.
		 */
		bool			transmitRnrB();

		/** This function returns true if the receiver's receiver not ready bit
			of the channel B STAR register is set.
		 */
		bool			receivedRnrB();

		/** This function returns true if the receive line inactive bit
			of the channel B STAR register is set.
		 */
		bool			receiveLineInactiveB();

		/** This function returns true if the command executing bit
			of the channel B STAR register is set.
		 */
		bool			commandExecutingB();

		/** This function returns true if the clear to send active bit
			of the channel B STAR register is set.
		 */
		bool			clearToSendActiveB();

		/** This function returns the receive byte count of channel B.
		 */
		unsigned		getRxLengthB();

		/** This function returns a pointer to the 32 byte transmit FIFO for channel B.
		 */
		uint8_t*			getTxFifoB();

		/** This function returns a pointer to the 32 byte receive FIFO for channel B.
		 */
		const uint8_t*	getRxFifoB();

		/** This function decodes rsta, counts any error indications to channel B, and returns true
			if the status indicates an error free condition.
		 */
		bool			packetStatusIsGoodB(Fields rsta);
	};

/** This class is the implementation of the abstract channel interface for channel A.
 */

class Sab8252xDriverChannelA : public Sab8252xDriver {
	private:
		/** A reference to the driver interface.
		 */
		Sab82525Driver&	_driver;

	public:	// Constructor
		/** The constructor.
		 */
		Sab8252xDriverChannelA(Sab82525Driver& driver);

	public:	// Sab8252xDriver Control

		/** This function returns the receive byte count for channel A.
		 */
		unsigned		getRxLength();

		/** This function returns a pointer to the 32 byte transmit FIFO for channel A.
		 */
		uint8_t*			getTxFifo();

		/** This function returns a pointer to the 32 byte receive FIFO for channel A.
		 */
		const uint8_t*	getRxFifo();

		/** Resets the transmitter for channel A.
		 */
		void			resetTransmitter();

		/** Resets the receiver for channel A.
		 */
		void			resetReceiver();

		/** Issues the receive message complete indication to
			channel A to release the current receive FIFO.
		 */
		void			receiveMessageComplete();

		/** Issues the transmit transparent frame (XTF) command
			to channel A.
		 */
		void			transmitPartOfFrame();

		/** Issues the transmit transparent last part of frame and transmit
			message end (XTF+XME) command to channel A. This function is
			called to send the last data in a packet.
		 */
		void			transmitLastPartOfFrame();

		/** This function decodes rsta, counts any error indications to channel A, and returns true
			if the status indicates an error free condition.
		 */
		bool			packetStatusIsGood(Fields rsta);

	public:	// Status

		/** This function returns true if the tranmit overflow bit of the
			channel A STAR register is set.
		 */
		bool			transmitDataOverflow();

		/** This function returns true if the transmit receiver not ready bit
			of the channel A STAR register is set.
		 */
		bool			transmitRnr();

		/** This function returns true if the receiver's receiver not ready bit
			of the channel A STAR register is set.
		 */
		bool			receivedRnr();

		/** This function returns true if the receive line inactive bit
			of the channel A STAR register is set.
		 */
		bool			receiveLineInactive();

		/** This function returns true if the command executing bit
			of the channel A STAR register is set.
		 */
		bool			commandExecuting();

		/** This function returns true if the clear to send active bit
			of the channel A STAR register is set.
		 */
		bool			clearToSendActive();
	};

/** This class is the implementation of the abstract channel interface for channel B.
 */

class Sab8252xDriverChannelB : public Sab8252xDriver {
	private:
		Sab82525Driver&	_driver;

	public:	// Constructor
		Sab8252xDriverChannelB(Sab82525Driver& driver);

	public:	// Sab8252xDriver Control

		/** This function returns the receive byte count for channel B.
		 */
		unsigned		getRxLength();

		/** This function returns a pointer to the 32 byte transmit FIFO for channel B.
		 */
		uint8_t*			getTxFifo();

		/** This function returns a pointer to the 32 byte receive FIFO for channel B.
		 */
		const uint8_t*	getRxFifo();

		/** Resets the transmitter for channel B.
		 */
		void			resetTransmitter();

		/** Resets the receiver for channel B.
		 */
		void			resetReceiver();

		/** Issues the receive message complete indication to
			channel B to release the current receive FIFO.
		 */
		void			receiveMessageComplete();

		/** Issues the transmit transparent frame (XTF) command
			to channel B.
		 */
		void			transmitPartOfFrame();

		/** Issues the transmit transparent last part of frame and transmit
			message end (XTF+XME) command to channel B. This function is
			called to send the last data in a packet.
		 */
		void			transmitLastPartOfFrame();

		/** This function decodes rsta, counts any error indications to channel B, and returns true
			if the status indicates an error free condition.
		 */
		bool			packetStatusIsGood(Fields rsta);

	public:	// Sab8252xDriver Status

		/** This function returns true if the tranmit overflow bit of the
			channel B STAR register is set.
		 */
		bool			transmitDataOverflow();

		/** This function returns true if the transmit receiver not ready bit
			of the channel B STAR register is set.
		 */
		bool			transmitRnr();

		/** This function returns true if the receiver's receiver not ready bit
			of the channel B STAR register is set.
		 */
		bool			receivedRnr();

		/** This function returns true if the receive line inactive bit
			of the channel B STAR register is set.
		 */
		bool			receiveLineInactive();

		/** This function returns true if the command executing bit
			of the channel B STAR register is set.
		 */
		bool			commandExecuting();

		/** This function returns true if the clear to send active bit
			of the channel B STAR register is set.
		 */
		bool			clearToSendActive();
	};

};

#endif
