/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "oscl/macros/bits.h"
#include "oscl/kernel/interrupt.h"
#include "sab82525.h"
#include "oscl/mt/sema/api.h"

using namespace Oscl;

//////////////////// Sab82525Driver ////////////////////

Sab82525Driver::Sab82525Driver(	Sab8252xChannelClient&	clientA,
								Sab8252xChannelClient&	clientB,
								SAB82525&				chip,
								Mt::Sema::Api&			input
								):
						_clientA(clientA),
						_clientB(clientB),
						_input(input),
						_channelA(chip[0]),
						_channelB(chip[1]),
						_istaA(0),
						_istaB(0),
						_exirA(0),
						_exirB(0)
						{
	_channelA.write.MASK	= (Flags)0xFF;
	_channelB.write.MASK	= (Flags)0xFF;
	}

//---------------- Common interfaces -------------------//

void	Sab82525Driver::suInterrupt(){
	_istaA	|= _channelA.read.ISTA;
	_istaB	|= _channelB.read.ISTA;
	if(_istaB & OsclMacroBitMask(ISTA_EXA)){
		_exirA	|= _channelA.read.EXIR;
		}
	if(_istaB & OsclMacroBitMask(ISTA_EXB)){
		_exirB	|= _channelB.read.EXIR;
		}
	_input.suSignal();
	}

void	Sab82525Driver::run() noexcept{
	Oscl::Kernel::IrqMaskState	savedLevel;
	initialize();
	while(1){
		_input.wait();
		while(1){
			OsclKernelDisableInterrupts(savedLevel);
			if(_istaA & OsclMacroBitMask(ISTA_RPF)){
				_istaA &= ~OsclMacroBitMask(ISTA_RPF);
				OsclKernelRestoreInterruptState(savedLevel);
				_clientA.receivePoolFull();
				}
			else if(_istaB & OsclMacroBitMask(ISTA_RPF)){
				_istaB &= ~OsclMacroBitMask(ISTA_RPF);
				OsclKernelRestoreInterruptState(savedLevel);
				_clientB.receivePoolFull();
				}
			else if(_istaA & OsclMacroBitMask(ISTA_RME)){
				_istaA &= ~OsclMacroBitMask(ISTA_RME);
				OsclKernelRestoreInterruptState(savedLevel);
				_clientA.receiveMessageComplete();
				}
			else if(_istaB & OsclMacroBitMask(ISTA_RME)){
				_istaB &= ~OsclMacroBitMask(ISTA_RME);
				OsclKernelRestoreInterruptState(savedLevel);
				_clientB.receiveMessageComplete();
				}
			else if(_istaA & OsclMacroBitMask(ISTA_XPR)){
				_istaA &= ~OsclMacroBitMask(ISTA_XPR);
				OsclKernelRestoreInterruptState(savedLevel);
				_clientA.transmitPoolReady();
				}
			else if(_istaB & OsclMacroBitMask(ISTA_XPR)){
				_istaB &= ~OsclMacroBitMask(ISTA_XPR);
				OsclKernelRestoreInterruptState(savedLevel);
				_clientB.transmitPoolReady();
				}
			else if(_istaB & OsclMacroBitMask(ISTA_EXA)){
				if(_exirA & OsclMacroBitMask(EXIR_XMR)){
					_exirA &= ~OsclMacroBitMask(EXIR_XMR);
					OsclKernelRestoreInterruptState(savedLevel);
					_clientA.transmitMessageRepeat();
					continue;
					}
				else if(_exirA & OsclMacroBitMask(EXIR_XDU)){
					_exirA &= ~OsclMacroBitMask(EXIR_XDU);
					OsclKernelRestoreInterruptState(savedLevel);
					_clientA.transmitDataUnderrunExtendedTransmissionEnd();
					continue;
					}
				else if(_exirA & OsclMacroBitMask(EXIR_PCE)){
					_exirA &= ~OsclMacroBitMask(EXIR_PCE);
					OsclKernelRestoreInterruptState(savedLevel);
					_clientA.protocolError();
					continue;
					}
				else if(_exirA & OsclMacroBitMask(EXIR_RFO)){
					_exirA &= ~OsclMacroBitMask(EXIR_RFO);
					OsclKernelRestoreInterruptState(savedLevel);
					_clientA.receiveFrameOverflow();
					continue;
					}
				else if(_exirA & OsclMacroBitMask(EXIR_CSC)){
					_exirA &= ~OsclMacroBitMask(EXIR_CSC);
					OsclKernelRestoreInterruptState(savedLevel);
					_clientA.clearToSendStatusChange();
					continue;
					}
				else if(_exirA & OsclMacroBitMask(EXIR_RFS)){
					_exirA &= ~OsclMacroBitMask(EXIR_RFS);
					OsclKernelRestoreInterruptState(savedLevel);
					_clientA.receiveFrameStart();
					continue;
					}
				else{
					_istaB &= ~OsclMacroBitMask(ISTA_EXA);
					}
				}
			else if(_istaB & OsclMacroBitMask(ISTA_EXB)){
				if(_exirB & OsclMacroBitMask(EXIR_XMR)){
					_exirB &= ~OsclMacroBitMask(EXIR_XMR);
					OsclKernelRestoreInterruptState(savedLevel);
					_clientB.transmitMessageRepeat();
					continue;
					}
				else if(_exirB & OsclMacroBitMask(EXIR_XDU)){
					_exirB &= ~OsclMacroBitMask(EXIR_XDU);
					OsclKernelRestoreInterruptState(savedLevel);
					_clientB.transmitDataUnderrunExtendedTransmissionEnd();
					continue;
					}
				else if(_exirB & OsclMacroBitMask(EXIR_PCE)){
					_exirB &= ~OsclMacroBitMask(EXIR_PCE);
					OsclKernelRestoreInterruptState(savedLevel);
					_clientB.protocolError();
					continue;
					}
				else if(_exirB & OsclMacroBitMask(EXIR_RFO)){
					_exirB &= ~OsclMacroBitMask(EXIR_RFO);
					OsclKernelRestoreInterruptState(savedLevel);
					_clientB.receiveFrameOverflow();
					continue;
					}
				else if(_exirB & OsclMacroBitMask(EXIR_CSC)){
					_exirB &= ~OsclMacroBitMask(EXIR_CSC);
					OsclKernelRestoreInterruptState(savedLevel);
					_clientB.clearToSendStatusChange();
					continue;
					}
				else if(_exirB & OsclMacroBitMask(EXIR_RFS)){
					_exirB &= ~OsclMacroBitMask(EXIR_RFS);
					OsclKernelRestoreInterruptState(savedLevel);
					_clientB.receiveFrameStart();
					continue;
					}
				else{
					_istaB &= ~OsclMacroBitMask(ISTA_EXB);
					}
				}
			else{
				OsclKernelRestoreInterruptState(savedLevel);
				break;
				}
			}
		_clientA.pollTransmitter();
		_clientB.pollTransmitter();
		}
	}

void	Sab82525Driver::initialize(){
	_clientA.initialize(_channelA);
	_clientB.initialize(_channelB);
	}

inline void Sab82525Driver::executeCmd(HscxChannel& channel,Flags cmd){
	Oscl::Kernel::IrqMaskState	savedLevel;
	while(true){
		OsclKernelDisableInterrupts(savedLevel);
		if(!(channel.read.STAR & OsclMacroBitMask(STAR_CEC))){
			channel.write.CMDR  = cmd;
			OsclKernelRestoreInterruptState(savedLevel);
			break;
			}
		OsclKernelRestoreInterruptState(savedLevel);
		}
	}

bool	Sab82525Driver::packetStatusIsGood(Sab8252xChannelClient& client,Fields rsta){
	if((rsta & (OsclMacroBitMask(RSTA_CRC) | OsclMacroBitMask(RSTA_RDO) | OsclMacroBitMask(RSTA_VFR))) != (OsclMacroBitMask(RSTA_VFR) | OsclMacroBitMask(RSTA_CRC))){
		if(!(rsta & OsclMacroBitMask(RSTA_CRC))){
			client.logCrcError();
			}
		if(rsta & OsclMacroBitMask(RSTA_RDO)){
			client.logOverflow();
			}
		if(rsta & OsclMacroBitMask(RSTA_RAB)){
			client.logAbort();
			}
		if(!(rsta & OsclMacroBitMask(RSTA_VFR))){
			client.logInvalidFrame();
			}
		return false;
		}

	return true;
	}

//---------------- Channel A interfaces -------------------//

void	Sab82525Driver::executeCmdA(Flags cmd){
	executeCmd(_channelA,cmd);
	}

bool	Sab82525Driver::transmitDataOverflowA(){
	return (_channelA.read.STAR & OsclMacroBitMask(STAR_XDOV))?true:false;
	}

bool	Sab82525Driver::transmitRnrA(){
	return (_channelA.read.STAR & OsclMacroBitMask(STAR_XFW))?true:false;
	}

bool	Sab82525Driver::receivedRnrA(){
	return (_channelA.read.STAR & OsclMacroBitMask(STAR_XRNR))?true:false;
	}

bool	Sab82525Driver::receiveLineInactiveA(){
	return (_channelA.read.STAR & OsclMacroBitMask(STAR_RRNR))?true:false;
	}

bool	Sab82525Driver::commandExecutingA(){
	return (_channelA.read.STAR & OsclMacroBitMask(STAR_RLI))?true:false;
	}

bool	Sab82525Driver::clearToSendActiveA(){
	return (_channelA.read.STAR & OsclMacroBitMask(STAR_CTS))?true:false;
	}

inline unsigned	Sab82525Driver::getRxLengthA(){
	return (((_channelA.read.RBCH & RBCH_RBC_MASK)<<8) | _channelA.read.RBCL);
	}

inline uint8_t*		Sab82525Driver::getTxFifoA(){
	return (uint8_t*)_channelA.write.XFIFO;
	}

inline const uint8_t*	Sab82525Driver::getRxFifoA(){
	return (uint8_t*)_channelA.read.RFIFO;
	}

bool	Sab82525Driver::packetStatusIsGoodA(Fields rsta){
	return packetStatusIsGood(_clientA,rsta);
	}

//---------------- Channel B interfaces -------------------//

void	Sab82525Driver::executeCmdB(Flags cmd){
	executeCmd(_channelB,cmd);
	}

bool	Sab82525Driver::transmitDataOverflowB(){
	return (_channelB.read.STAR & OsclMacroBitMask(STAR_XDOV))?true:false;
	}

bool	Sab82525Driver::transmitRnrB(){
	return (_channelB.read.STAR & OsclMacroBitMask(STAR_XFW))?true:false;
	}

bool	Sab82525Driver::receivedRnrB(){
	return (_channelB.read.STAR & OsclMacroBitMask(STAR_XRNR))?true:false;
	}

bool	Sab82525Driver::receiveLineInactiveB(){
	return (_channelB.read.STAR & OsclMacroBitMask(STAR_RRNR))?true:false;
	}

bool	Sab82525Driver::commandExecutingB(){
	return (_channelB.read.STAR & OsclMacroBitMask(STAR_RLI))?true:false;
	}

bool	Sab82525Driver::clearToSendActiveB(){
	return (_channelB.read.STAR & OsclMacroBitMask(STAR_CTS))?true:false;
	}

inline unsigned	Sab82525Driver::getRxLengthB(){
	return (((_channelB.read.RBCH & RBCH_RBC_MASK)<<8) | _channelB.read.RBCL);
	}

inline uint8_t*		Sab82525Driver::getTxFifoB(){
	return (uint8_t*)_channelB.write.XFIFO;
	}

inline const uint8_t*	Sab82525Driver::getRxFifoB(){
	return (uint8_t*)_channelB.read.RFIFO;
	}

bool	Sab82525Driver::packetStatusIsGoodB(Fields rsta){
	return packetStatusIsGood(_clientB,rsta);
	}

//////////////////// Sab8252xDriverChannelA ////////////////////

Sab8252xDriverChannelA::Sab8252xDriverChannelA(Sab82525Driver& driver):
		_driver(driver)
		{
	}

unsigned	Sab8252xDriverChannelA::getRxLength(){
	return _driver.getRxLengthA();
	}

Octet*		Sab8252xDriverChannelA::getTxFifo(){
	return _driver.getTxFifoA();
	}

const uint8_t*		Sab8252xDriverChannelA::getRxFifo(){
	return _driver.getRxFifoA();
	}

void		Sab8252xDriverChannelA::resetTransmitter(){
	_driver.executeCmdA((Flags)(OsclMacroBitMask(CMDR_XRES)));
	}

void		Sab8252xDriverChannelA::resetReceiver(){
	_driver.executeCmdA((Flags)(OsclMacroBitMask(CMDR_RHR)));
	}

void		Sab8252xDriverChannelA::receiveMessageComplete(){
	_driver.executeCmdA((Flags)(OsclMacroBitMask(CMDR_RMC)));
	}

void		Sab8252xDriverChannelA::transmitPartOfFrame(){
	_driver.executeCmdA((Flags)(OsclMacroBitMask(CMDR_XTF)));
	}

void		Sab8252xDriverChannelA::transmitLastPartOfFrame(){
	_driver.executeCmdA((Flags)(OsclMacroBitMask(CMDR_XME)+OsclMacroBitMask(CMDR_XTF)));
	}

bool		Sab8252xDriverChannelA::packetStatusIsGood(Fields rsta){
	return _driver.packetStatusIsGoodA(rsta);
	}

bool	Sab8252xDriverChannelA::transmitDataOverflow(){
	return _driver.transmitDataOverflowA();
	}

bool	Sab8252xDriverChannelA::transmitRnr(){
	return _driver.transmitRnrA();
	}

bool	Sab8252xDriverChannelA::receivedRnr(){
	return _driver.receivedRnrA();
	}

bool	Sab8252xDriverChannelA::receiveLineInactive(){
	return _driver.receiveLineInactiveA();
	}

bool	Sab8252xDriverChannelA::commandExecuting(){
	return _driver.commandExecutingA();
	}

bool	Sab8252xDriverChannelA::clearToSendActive(){
	return _driver.clearToSendActiveA();
	}

//////////////////// Sab8252xDriverChannelB ////////////////////

Sab8252xDriverChannelB::Sab8252xDriverChannelB(Sab82525Driver& driver):
		_driver(driver)
		{
	}

unsigned	Sab8252xDriverChannelB::getRxLength(){
	return _driver.getRxLengthB();
	}

Octet*		Sab8252xDriverChannelB::getTxFifo(){
	return _driver.getTxFifoB();
	}

const uint8_t*		Sab8252xDriverChannelB::getRxFifo(){
	return _driver.getRxFifoB();
	}

void		Sab8252xDriverChannelB::resetTransmitter(){
	_driver.executeCmdB((Flags)(OsclMacroBitMask(CMDR_XRES)));
	}

void		Sab8252xDriverChannelB::resetReceiver(){
	_driver.executeCmdB((Flags)(OsclMacroBitMask(CMDR_RHR)));
	}

void		Sab8252xDriverChannelB::receiveMessageComplete(){
	_driver.executeCmdB((Flags)(OsclMacroBitMask(CMDR_RMC)));
	}

void		Sab8252xDriverChannelB::transmitPartOfFrame(){
	_driver.executeCmdB((Flags)(OsclMacroBitMask(CMDR_XTF)));
	}

void		Sab8252xDriverChannelB::transmitLastPartOfFrame(){
	_driver.executeCmdB((Flags)(OsclMacroBitMask(CMDR_XME)+OsclMacroBitMask(CMDR_XTF)));
	}

bool		Sab8252xDriverChannelB::packetStatusIsGood(Fields rsta){
	return _driver.packetStatusIsGoodB(rsta);
	}

bool	Sab8252xDriverChannelB::transmitDataOverflow(){
	return _driver.transmitDataOverflowB();
	}

bool	Sab8252xDriverChannelB::transmitRnr(){
	return _driver.transmitRnrB();
	}

bool	Sab8252xDriverChannelB::receivedRnr(){
	return _driver.receivedRnrB();
	}

bool	Sab8252xDriverChannelB::receiveLineInactive(){
	return _driver.receiveLineInactiveB();
	}

bool	Sab8252xDriverChannelB::commandExecuting(){
	return _driver.commandExecutingB();
	}

bool	Sab8252xDriverChannelB::clearToSendActive(){
	return _driver.clearToSendActiveB();
	}


