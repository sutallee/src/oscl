/*
   Copyright (C) 2024 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_cortexm_asmh_
#define _oscl_drv_cortexm_asmh_

/** */
namespace Oscl {

/** */
namespace CortexM {

inline void isb() noexcept{
    asm volatile("isb" :::"memory");
    }

inline void dsb() noexcept{
    asm volatile("dsb" :::"memory");
    }

inline void dmb() noexcept{
    asm volatile("dmb":::"memory");
    }

inline void msrPRIMASK(unsigned long value) noexcept{
    asm volatile("msr PRIMASK,%0" : : "r" (value));
    }

inline unsigned long mrsPRIMASK() noexcept{
	unsigned long	reg;
    asm volatile("mrs %0,PRIMASK" : "=r" (reg));
	return reg;
    }

inline void wfi() noexcept{
    asm volatile("wfi" : : );
    }

}
}

#endif
