/*
   Copyright (C) 2006 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_ti_tnetv105x_device_apih_
#define _oscl_drv_ti_tnetv105x_device_apih_
#include "oscl/extalloc/api.h"
#include "oscl/driver/ti/tnetv105x/systimer/api.h"
#include "oscl/driver/ti/tnetv105x/sysint/api.h"
#include "oscl/driver/ti/tnetv105x/sysint/drvapi.h"
//#include "oscl/driver/ti/tnetv105x/gpio/api.h"
#include "oscl/driver/national/uart/pc16550/regapi.h"
#include "oscl/driver/national/uart/pc16550/api.h"

/** */
namespace Oscl {
/** */
namespace TI {
/** */
namespace TNETV105X {

/** */
class Api {
	public:
		/** */
		virtual ~Api(){}

		/** */
		virtual Oscl::TI::TNETV105X::SYS_INT::DrvApi&	getSysIntDriverApi() noexcept=0;
		/** */
		virtual Oscl::TI::TNETV105X::SYS_INT::Api&		getSysIntApi() noexcept=0;
		/** */
		virtual Oscl::TI::TNETV105X::SYS_TIMER::Api&	getSysTimer0Api() noexcept=0;
		/** */
		virtual Oscl::TI::TNETV105X::SYS_TIMER::Api&	getSysTimer1Api() noexcept=0;
		/** */
		virtual Oscl::National::UART::PC16550::RegisterApi&		getRawUartApi() noexcept=0;
		/** */
		virtual Oscl::National::UART::PC16550::Api&		getUartApi() noexcept=0;
	};

}
}
}


#endif
