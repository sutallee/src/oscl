/*
   Copyright (C) 2006 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "device.h"
#include "oscl/error/fatal.h"

/** */
namespace Oscl {
/** */
namespace TI {
/** */
namespace TNETV105X {

/** The purpose of this class is to provide an action for the
	SYS_INT when an attempt is made to use an uninitialized SYS_INT
	vector.
 */
class UnusedVector : public Oscl::TI::TNETV105X::SYS_INT::IrqApi {
	public:
		/** */
		UnusedVector() noexcept;
	public: // Oscl::TI::TNETV105X::SYS_INT::IrqApi
		/** */
		void	reserve(	Oscl::Interrupt::
							StatusHandlerApi&	handler
							) noexcept;
		/** */
		void	release() noexcept;

	public: // Oscl::Interrupt::StatusHandlerApi
		/** */
        bool    interrupt() noexcept;

	};

}
}
}

using namespace Oscl::TI::TNETV105X;

static UnusedVector	unusedVector;

Device::Device(	Oscl::HW::TI::TNETV105X::Map&	registers,
				Oscl::Mt::Mutex::Simple::Api&	mutex
				) noexcept:
		_sysIntDriver(registers.sys_int),
		_irqSecondary(_sysIntDriver.getSecondary()),
		_irqEXT_INT1(_sysIntDriver.getEXT_INT1()),
		_irqEXT_INT2(_sysIntDriver.getEXT_INT2()),
		_irqEXT_INT3(_sysIntDriver.getEXT_INT3()),
		_irqEXT_INT4(_sysIntDriver.getEXT_INT4()),
		_irqTimerZero(_sysIntDriver.getTimerZero()),
		_irqTimerOne(_sysIntDriver.getTimerOne()),
		_irqUart(_sysIntDriver.getUart()),
		_irqINT_8(_sysIntDriver.getINT_8()),
		_irqDmaZero(_sysIntDriver.getDmaZero()),
		_irqDmaOne(_sysIntDriver.getDmaOne()),
		_irqINT_11(_sysIntDriver.getINT_11()),
		_irqINT_12(_sysIntDriver.getINT_12()),
		_irqINT_13(_sysIntDriver.getINT_13()),
		_irqINT_14(_sysIntDriver.getINT_14()),
		_irqINT_15(_sysIntDriver.getINT_15()),
		_irqINT_16(_sysIntDriver.getINT_16()),
		_irqINT_17(_sysIntDriver.getINT_17()),
		_irqINT_18(_sysIntDriver.getINT_18()),
		_irqEthernetMacZero(_sysIntDriver.getEthernetMacZero()),
		_irqINT_20(_sysIntDriver.getINT_20()),
		_irqDSP_GPIO(_sysIntDriver.getDSP_GPIO()),
		_irqDspReset(_sysIntDriver.getDspReset()),
		_irqTELE_INT(_sysIntDriver.getTELE_INT()),
		_irqUsbDevice(_sysIntDriver.getUsbDevice()),
		_irqVLYNQ5(_sysIntDriver.getVLYNQ5()),
		_irqVLYNQ3(_sysIntDriver.getVLYNQ3()),
		_irqEthernetSwitchDMA(_sysIntDriver.getEthernetSwitchDMA()),
		_irqEthernetPHYs(_sysIntDriver.getEthernetPHYs()),
		_irqSSP(_sysIntDriver.getSSP()),
		_irqDmaTwo(_sysIntDriver.getDmaTwo()),
		_irqDmaThree(_sysIntDriver.getDmaThree()),
		_irqTelephony(_sysIntDriver.getTelephony()),
		_irqEthernetMacOne(_sysIntDriver.getEthernetMacOne()),
		_irqINT_34(_sysIntDriver.getINT_34()),
		_irqUsbHost(_sysIntDriver.getUsbHost()),
		_irqLCD(_sysIntDriver.getLCD()),
		_irqKeypad(_sysIntDriver.getKeypad()),
		_irqINT_38(_sysIntDriver.getINT_38()),
		_irqINT_39(_sysIntDriver.getINT_39()),
		_sysIntPic(	registers.sys_int.priority_index,
					_irqSecondary,
					_irqEXT_INT1,
					_irqEXT_INT2,
					_irqEXT_INT3,
					_irqEXT_INT4,
					_irqTimerZero,
					_irqTimerOne,
					_irqUart,
					_irqINT_8,
					_irqDmaZero,
					_irqDmaOne,
					_irqINT_11,
					_irqINT_12,
					_irqINT_13,
					_irqINT_14,
					_irqINT_15,
					_irqINT_16,
					_irqINT_17,
					_irqINT_18,
					_irqEthernetMacZero,
					_irqINT_20,
					_irqDSP_GPIO,
					_irqDspReset,
					_irqTELE_INT,
					_irqUsbDevice,
					_irqVLYNQ5,
					_irqVLYNQ3,
					_irqEthernetSwitchDMA,
					_irqEthernetPHYs,
					_irqSSP,
					_irqDmaTwo,
					_irqDmaThree,
					_irqTelephony,
					_irqEthernetMacOne,
					_irqINT_34,
					_irqUsbHost,
					_irqLCD,
					_irqKeypad,
					_irqINT_38,
					_irqINT_39
					),
		_sysIntMap(_sysIntPic),
		_sysTimer0Driver(	registers.sys_timer_0,
							(125000000U/100)/32,	// FIXME: This should be determined
										// by the system. The SYS_TIMER is clocked
										// at 125MHz. The value here attempts to achieve
										// a period of at least 10ms (a frequency of 100Hz).
					4	// Prescaler to divide by 32
					),
		_sysTimer1Driver(	registers.sys_timer_1,
							(125000000UL/100)/32,	// FIXME: This should be determined
										// by the system. The SYS_TIMER is clocked
										// at 125MHz. The value here attempts to achieve
										// a period of at least 10ms (a frequency of 100Hz).
					4	// Prescaler to divide by 32
					),
		_uart(	registers.uart.rbr_thr_dll,
				registers.uart.rbr_thr_dll,
				registers.uart.rbr_thr_dll,
				registers.uart.ier_dlh,
				registers.uart.ier_dlh,
				registers.uart.iir_fcr,
				registers.uart.iir_fcr,
				registers.uart.lcr,
				registers.uart.mcr,
				registers.uart.lsr,
				registers.uart.msr,
				registers.uart.spr
				),
			_uartDriver(_uart)
		{
	}

Oscl::TI::TNETV105X::SYS_INT::DrvApi&	Device::getSysIntDriverApi() noexcept{
	return _sysIntDriver;
	}

Oscl::TI::TNETV105X::SYS_INT::Api&		Device::getSysIntApi() noexcept{
	return _sysIntMap;
	}

Oscl::TI::TNETV105X::SYS_TIMER::Api&	Device::getSysTimer0Api() noexcept{
	return _sysTimer0Driver;
	}

Oscl::TI::TNETV105X::SYS_TIMER::Api&	Device::getSysTimer1Api() noexcept{
	return _sysTimer1Driver;
	}

Oscl::National::UART::PC16550::RegisterApi&		Device::getRawUartApi() noexcept{
	return _uart;
	}

Oscl::National::UART::PC16550::Api&		Device::getUartApi() noexcept{
	return _uartDriver;
	}

UnusedVector::UnusedVector() noexcept
		{
	}

void	UnusedVector::reserve(	Oscl::Interrupt::StatusHandlerApi&	handler
								) noexcept{
	Oscl::ErrorFatal::logAndExit("Attempt to reserve and unused SYS_INT vector.");
	}

void	UnusedVector::release() noexcept{
	Oscl::ErrorFatal::logAndExit("Attempt to release and unused SYS_INT vector.");
	}

bool    UnusedVector::interrupt() noexcept{
	Oscl::ErrorFatal::logAndExit("Interrupt caught by unused SYS_INT vector.");
	return false;
	}

