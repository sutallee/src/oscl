/*
   Copyright (C) 2006 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_ti_tnetv105x_device_deviceh_
#define _oscl_drv_ti_tnetv105x_device_deviceh_

#include "api.h"
#include "oscl/driver/ti/tnetv105x/systimer/driver.h"
#include "oscl/driver/ti/tnetv105x/sysint/drv.h"
#include "oscl/driver/ti/tnetv105x/sysint/interrupt.h"
#include "oscl/extalloc/driver.h"
#include "oscl/extalloc/mt/mutex.h"
#include "oscl/mt/mutex/simple.h"
#include "oscl/hw/ti/tnetv105x/map.h"
#include "oscl/driver/ti/tnetv105x/sysint/pic.h"
#include "oscl/driver/ti/tnetv105x/sysint/map.h"
//#include "oscl/driver/ti/tnetv105x/gpio/drv.h"
#include "oscl/driver/national/uart/pc16550/thirtytwobit.h"
#include "oscl/driver/national/uart/pc16550/driver.h"
#include "oscl/driver/national/uart/pc16550/api.h"

/** */
namespace Oscl {
/** */
namespace TI {
/** */
namespace TNETV105X {

/** This class implements the low level drivers for an TI
	TNETV105X processor and attending peripherals. The drivers
	contained in this layer make no use of kernel services.
 */
class Device : public Api {
	private:
		/** */
		Oscl::TI::TNETV105X::SYS_INT::Driver		_sysIntDriver;
		/** */
		Oscl::TI::TNETV105X::SYS_INT::Interrupt	_irqSecondary;
		/** */
		Oscl::TI::TNETV105X::SYS_INT::Interrupt	_irqEXT_INT1;
		/** */
		Oscl::TI::TNETV105X::SYS_INT::Interrupt	_irqEXT_INT2;
		/** */
		Oscl::TI::TNETV105X::SYS_INT::Interrupt	_irqEXT_INT3;
		/** */
		Oscl::TI::TNETV105X::SYS_INT::Interrupt	_irqEXT_INT4;
		/** */
		Oscl::TI::TNETV105X::SYS_INT::Interrupt	_irqTimerZero;
		/** */
		Oscl::TI::TNETV105X::SYS_INT::Interrupt	_irqTimerOne;
		/** */
		Oscl::TI::TNETV105X::SYS_INT::Interrupt	_irqUart;
		/** */
		Oscl::TI::TNETV105X::SYS_INT::Interrupt	_irqINT_8;
		/** */
		Oscl::TI::TNETV105X::SYS_INT::Interrupt	_irqDmaZero;
		/** */
		Oscl::TI::TNETV105X::SYS_INT::Interrupt	_irqDmaOne;
		/** */
		Oscl::TI::TNETV105X::SYS_INT::Interrupt	_irqINT_11;
		/** */
		Oscl::TI::TNETV105X::SYS_INT::Interrupt	_irqINT_12;
		/** */
		Oscl::TI::TNETV105X::SYS_INT::Interrupt	_irqINT_13;
		/** */
		Oscl::TI::TNETV105X::SYS_INT::Interrupt	_irqINT_14;
		/** */
		Oscl::TI::TNETV105X::SYS_INT::Interrupt	_irqINT_15;
		/** */
		Oscl::TI::TNETV105X::SYS_INT::Interrupt	_irqINT_16;
		/** */
		Oscl::TI::TNETV105X::SYS_INT::Interrupt	_irqINT_17;
		/** */
		Oscl::TI::TNETV105X::SYS_INT::Interrupt	_irqINT_18;
		/** */
		Oscl::TI::TNETV105X::SYS_INT::Interrupt	_irqEthernetMacZero;
		/** */
		Oscl::TI::TNETV105X::SYS_INT::Interrupt	_irqINT_20;
		/** */
		Oscl::TI::TNETV105X::SYS_INT::Interrupt	_irqDSP_GPIO;
		/** */
		Oscl::TI::TNETV105X::SYS_INT::Interrupt	_irqDspReset;
		/** */
		Oscl::TI::TNETV105X::SYS_INT::Interrupt	_irqTELE_INT;
		/** */
		Oscl::TI::TNETV105X::SYS_INT::Interrupt	_irqUsbDevice;
		/** */
		Oscl::TI::TNETV105X::SYS_INT::Interrupt	_irqVLYNQ5;
		/** */
		Oscl::TI::TNETV105X::SYS_INT::Interrupt	_irqVLYNQ3;
		/** */
		Oscl::TI::TNETV105X::SYS_INT::Interrupt	_irqEthernetSwitchDMA;
		/** */
		Oscl::TI::TNETV105X::SYS_INT::Interrupt	_irqEthernetPHYs;
		/** */
		Oscl::TI::TNETV105X::SYS_INT::Interrupt	_irqSSP;
		/** */
		Oscl::TI::TNETV105X::SYS_INT::Interrupt	_irqDmaTwo;
		/** */
		Oscl::TI::TNETV105X::SYS_INT::Interrupt	_irqDmaThree;
		/** */
		Oscl::TI::TNETV105X::SYS_INT::Interrupt	_irqTelephony;
		/** */
		Oscl::TI::TNETV105X::SYS_INT::Interrupt	_irqEthernetMacOne;
		/** */
		Oscl::TI::TNETV105X::SYS_INT::Interrupt	_irqINT_34;
		/** */
		Oscl::TI::TNETV105X::SYS_INT::Interrupt	_irqUsbHost;
		/** */
		Oscl::TI::TNETV105X::SYS_INT::Interrupt	_irqLCD;
		/** */
		Oscl::TI::TNETV105X::SYS_INT::Interrupt	_irqKeypad;
		/** */
		Oscl::TI::TNETV105X::SYS_INT::Interrupt	_irqINT_38;
		/** */
		Oscl::TI::TNETV105X::SYS_INT::Interrupt	_irqINT_39;
		/** */
		Oscl::TI::TNETV105X::SYS_INT::PIC			_sysIntPic;
		/** */
		Oscl::TI::TNETV105X::SYS_INT::Map			_sysIntMap;
		/** */
		Oscl::TI::TNETV105X::SYS_TIMER::Driver		_sysTimer0Driver;
		/** */
		Oscl::TI::TNETV105X::SYS_TIMER::Driver		_sysTimer1Driver;
		/** */
		Oscl::National::UART::PC16550::ThirtyTwoBit		_uart;
		/** */
		Oscl::National::UART::PC16550::Driver			_uartDriver;

	public:
		/** */
		Device(	Oscl::HW::TI::TNETV105X::Map&	registers,
				Oscl::Mt::Mutex::Simple::Api&	mutex
				) noexcept;

	public:	// TNETV105X::Api
		/** */
		Oscl::TI::TNETV105X::SYS_INT::DrvApi&	getSysIntDriverApi() noexcept;

		/** */
		Oscl::TI::TNETV105X::SYS_INT::Api&		getSysIntApi() noexcept;

		/** */
		Oscl::TI::TNETV105X::SYS_TIMER::Api&		getSysTimer0Api() noexcept;

		/** */
		Oscl::TI::TNETV105X::SYS_TIMER::Api&		getSysTimer1Api() noexcept;

		/** */
		Oscl::National::UART::PC16550::RegisterApi&		getRawUartApi() noexcept;

		/** */
		Oscl::National::UART::PC16550::Api&		getUartApi() noexcept;

	};

}
}
}


#endif
