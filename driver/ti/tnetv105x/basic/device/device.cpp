#include "device.h"
#include "oscl/hw/ti/tnetv105x/gnu/map.h"

using namespace Oscl::TI::TNETV105X::Basic;

Device::Device(	Oscl::HW::TI::TNETV105X::Map&	tnetv105x,
				Oscl::Mt::Mutex::Simple::Api&	mutex
				) noexcept :
		_tnetv105x(tnetv105x,mutex)
		{
	}

Oscl::TI::TNETV105X::Api&	Device::getTNETV105x() noexcept{
	return _tnetv105x;
	}

