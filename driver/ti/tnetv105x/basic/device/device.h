#ifndef _oscl_driver_ti_tnetv105x_basic_device_deviceh_
#define _oscl_driver_ti_tnetv105x_basic_device_deviceh_
#include "oscl/driver/ti/tnetv105x/device/device.h"
#include "oscl/hw/ti/tnetv105x/map.h"
#include "api.h"

/** */
namespace Oscl {
/** */
namespace TI {
/** */
namespace TNETV105X {
/** */
namespace Basic {

/** This class implements the low level devices available on
	the Basic development board. It contains only primitive
	device drivers that make no assumptions about the existence
	of a kernel. No policy or configuration options are to be
	assumed at this level either.
 */
class Device : public Api {
	private:
		/** */
		Oscl::TI::TNETV105X::Device				_tnetv105x;

	public:
		/** */
		Device(	Oscl::HW::TI::TNETV105X::Map&	tnetv105x,
				Oscl::Mt::Mutex::Simple::Api&	mutex
				) noexcept;

	public:	// Api
		/** */
		Oscl::TI::TNETV105X::Api&			getTNETV105x() noexcept;
	};

}
}
}
}

#endif
