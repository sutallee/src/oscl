#ifndef _oscl_driver_ti_tnetv105x_basic_device_apih_
#define _oscl_driver_ti_tnetv105x_basic_device_apih_
#include "oscl/driver/ti/tnetv105x/device/api.h"

/** */
namespace Oscl {
/** */
namespace TI {
/** */
namespace TNETV105X {
/** */
namespace Basic {

/** This is the interface to the Basic hardware drivers.
	All drivers available through this interface are independent
	of operating syste/environment.
 */
class Api {
	public:
		/** */
		virtual ~Api(){}
		/** */
		virtual Oscl::TI::TNETV105X::Api&		getTNETV105x() noexcept=0;
	};

}
}
}
}


#endif
