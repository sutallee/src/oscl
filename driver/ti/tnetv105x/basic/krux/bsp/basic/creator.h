#ifndef _oscl_driver_ti_tnetv105x_basic_krux_bsp_basic_creatorh_
#define _oscl_driver_ti_tnetv105x_basic_krux_bsp_basic_creatorh_
#include "oscl/memory/transapi.h"
#include "oscl/krux/sema/bbsema.h"
#include "oscl/bits/port.h"
#include "oscl/bits/mt/mutexedbp.h"
#include "oscl/bits/mt/mutexedfield.h"
#include "oscl/driver/ti/tnetv105x/basic/device/device.h"
#include "oscl/mt/itc/dyn/adv/service.h"
#include "oscl/mt/mutex/simple.h"
#include "oscl/interrupt/sadaptor.h"
#include "oscl/interrupt/shared/handler.h"
#include "oscl/driver/ti/tnetv105x/systimer/delay/server.h"
#include "oscl/driver/national/uart/pc16550/serial/stream/driver.h"
#include "oscl/driver/national/uart/pc16550/serial/stream/interrupt.h"

/** */
namespace Oscl {
/** */
namespace TI {
/** */
namespace TNETV105X {
/** */
namespace Basic {

/** */
class SystemTimer : public Oscl::TI::TNETV105X::SYS_TIMER::Observer {
	public:
		/** */
		void	tick() noexcept;
	};

/** This class implements the high level drivers that are common
	to a basic configuration of the MDP860PPro development board
	under OOOS kernel. This level of the BSP includes policy
	as well as ITC services.
 */
class Creator {
	private:
		/** */
		TI::TNETV105X::Basic::Device			_basic;

		/** */
		Oscl::Interrupt::StatusAdaptor			_statusIrqHandlerAdaptor;

		/** */
		Oscl::Memory::Bus::NullTranslation		_memTranslator;

		/** */
		SystemTimer								_pitSysTimer;

		/** */
		Oscl::TI::TNETV105X::SYS_TIMER::Delay::Server	_delayServer;

		/** */
		Oscl::National::UART::PC16550::
		Serial::Stream::Driver						_uartDriver;

		/** */
		Oscl::National::UART::PC16550::
		Serial::Stream::Interrupt                   _uartInterrupt;


	public:
		/** */
		Creator(	Oscl::Mt::
					Mutex::Simple::Api&		mutex
					) noexcept;
		/** */
		void	initialize() noexcept;

		/** */
		TI::TNETV105X::Basic::Api&			getBasic() noexcept;

		/** */
		Oscl::Mt::Itc::
		Delay::Req::Api::SAP&				getDelayServiceSAP() noexcept;
		/** */
		Oscl::Mt::Runnable&					getDelayServiceRunnable() noexcept;

		/** */
		Oscl::Mt::Runnable&					getUartRunnable() noexcept;
		/** */
		Oscl::Mt::Itc::Srv::OpenCloseSyncApi&	getUartOpenCloseSyncApi() noexcept;
		/** */
		Oscl::Stream::Output::Api&				getUartTxSyncApi() noexcept;
		/** */
		Oscl::Stream::Output::Req::Api::SAP&	getUartTxSAP() noexcept;
		/** */
		Oscl::Stream::Input::Api&				getUartRxSyncApi() noexcept;
		/** */
		Oscl::Stream::Input::Req::Api::SAP&		getUartRxSAP() noexcept;
		/** */
		Oscl::Stream::Output::Api&				getUuartTxSyncApi() noexcept;
	};

}
}
}
}

#endif
