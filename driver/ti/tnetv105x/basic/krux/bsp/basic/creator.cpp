#include "creator.h"
#include "oscl/interrupt/null.h"
#include "oscl/hw/ti/tnetv105x/gnu/map.h"

Oscl::Interrupt::HandlerApi* ExternalInterruptHandler;

using namespace Oscl::TI::TNETV105X::Basic;

void	SystemTimer::tick() noexcept{
	extern Oscl::Krux::Sema::BroadcastBinary	UKClockSema;
	UKClockSema.suSignal();
	}

static Oscl::Interrupt::NullHandler				nullHandler;

Creator::Creator(	Oscl::Mt::Mutex::Simple::Api&		mutex
					) noexcept:
		_basic(TiTnet105x_IoMap,mutex),
		_statusIrqHandlerAdaptor(_basic.getTNETV105x().getSysIntApi().getISR()),
		_delayServer(100),	// FIXME: need to correlate with PIT init.
		_uartDriver(	_basic.getTNETV105x().getRawUartApi(),
						_basic.getTNETV105x().getUartApi(),
						_delayServer.getSAP()
						),
		_uartInterrupt(	_basic.getTNETV105x().getRawUartApi(),
						_uartDriver
						)

		{
	ExternalInterruptHandler	= &_statusIrqHandlerAdaptor;
	_basic.getTNETV105x().getSysTimer0Api().attach(_delayServer);
	}

void	Creator::initialize() noexcept{
	Oscl::TI::TNETV105X::SYS_INT::Api&	pic	= _basic.getTNETV105x().getSysIntApi();
    pic.getTimerZero().reserve(_basic.getTNETV105x().getSysTimer0Api());
    pic.getUart().reserve(_uartInterrupt);
	_basic.getTNETV105x().getSysTimer0Api().attach(_pitSysTimer);
	_basic.getTNETV105x().getSysIntDriverApi().getTimerZero().enable();
	_basic.getTNETV105x().getSysTimer0Api().start();
	_basic.getTNETV105x().getSysIntDriverApi().getUart().enable();
	}

Oscl::TI::TNETV105X::Basic::Api&		Creator::getBasic() noexcept{
	return _basic;
	}

Oscl::Mt::Itc::Delay::Req::Api::SAP&	Creator::getDelayServiceSAP() noexcept{
	return _delayServer.getSAP();
	}

Oscl::Mt::Runnable&	Creator::getDelayServiceRunnable() noexcept{
	return _delayServer;
	}

Oscl::Mt::Runnable&	Creator::getUartRunnable() noexcept{
	return _uartDriver;
	}

Oscl::Mt::Itc::Srv::OpenCloseSyncApi&	Creator::getUartOpenCloseSyncApi() noexcept{
	return _uartDriver;
	}

Oscl::Stream::Output::Api&	Creator::getUuartTxSyncApi() noexcept{
	return _uartDriver.getTxSyncApi();
	}

Oscl::Stream::Output::Req::Api::SAP&	Creator::getUartTxSAP() noexcept{
	return _uartDriver.getTxDriverSAP();
	}

Oscl::Stream::Input::Api&	Creator::getUartRxSyncApi() noexcept{
	return _uartDriver.getRxSyncApi();
	}

Oscl::Stream::Input::Req::Api::SAP&	Creator::getUartRxSAP() noexcept{
	return _uartDriver.getRxDriverSAP();
	}

