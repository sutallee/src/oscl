#ifndef _oscl_driver_ti_tnetv105x_basic_krux_mmuh_
#define _oscl_driver_ti_tnetv105x_basic_krux_mmuh_

/** */
namespace Oscl {
/** */
namespace TI {
/** */
namespace TNETV105X {
/** */
namespace Basic {

/** This routine is called to initialize and map the fixed
	memory and I/O resources using the MMU. It must be called
	after TNETV105x chip selects have been initialized.
 */
void MmuMapFixed() noexcept;

}
}
}
}

#endif
