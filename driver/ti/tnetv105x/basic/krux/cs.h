#ifndef _oscl_driver_ti_tnetv105x_basic_krux_csh_
#define _oscl_driver_ti_tnetv105x_basic_krux_csh_

/** */
namespace Oscl {
/** */
namespace TI {
/** */
namespace TNETV105X {
/** */
namespace Basic {

/** This routine is called at startup to initialize
	the TNETV105x chip selects for the PCI configuration
	of the Basic mother-board.
 */
void ConfigChipSelects() noexcept;

}
}
}
}

#endif
