#include "oscl/kernel/map.h"
#include "oscl/krux/port/platform.h"
#include "cs.h"
#include "mmu.h"
#include "oscl/hw/mips/cp0/reg.h"
#include "oscl/hw/ti/tnetv105x/gnu/map.h"
#include "oscl/driver/mips/cp0/asm.h"

#include "oscl/error/info.h"

void OsclKernelMemoryMapInit() noexcept{
	// FIXME: No MMU for now
	}

void OsclKruxPreConstructorInit() noexcept{
//	Oscl::Error::Info::log("OsclKruxPreConstructorInit\n\r");
	TiTnet105x_IoMap.sys_int.enbl_clr_1	= 0xFFFFFFFF;
	TiTnet105x_IoMap.sys_int.enbl_clr_2	= 0xFFFFFFFF;

	Oscl::HW::MIPS::CP0::Status::Reg	status	= Oscl::MIPS::CP0::status();
	status	&= ~(		Oscl::HW::MIPS::CP0::Status::IM::IM7::FieldMask
					|	Oscl::HW::MIPS::CP0::Status::IM::IM6::FieldMask
					|	Oscl::HW::MIPS::CP0::Status::IM::IM5::FieldMask
					|	Oscl::HW::MIPS::CP0::Status::IM::IM4::FieldMask
					|	Oscl::HW::MIPS::CP0::Status::IM::IM3::FieldMask
					|	Oscl::HW::MIPS::CP0::Status::IM::IM2::FieldMask
					|	Oscl::HW::MIPS::CP0::Status::IM::IM1::FieldMask
					|	Oscl::HW::MIPS::CP0::Status::IM::IM0::FieldMask
					);
	status	|= (		Oscl::HW::MIPS::CP0::Status::IM::IM7::ValueMask_Disable
					|	Oscl::HW::MIPS::CP0::Status::IM::IM6::ValueMask_Disable
					|	Oscl::HW::MIPS::CP0::Status::IM::IM5::ValueMask_Disable
					|	Oscl::HW::MIPS::CP0::Status::IM::IM4::ValueMask_Disable
					|	Oscl::HW::MIPS::CP0::Status::IM::IM3::ValueMask_Disable
					|	Oscl::HW::MIPS::CP0::Status::IM::IM2::ValueMask_Disable
					|	Oscl::HW::MIPS::CP0::Status::IM::IM1::ValueMask_Disable
					|	Oscl::HW::MIPS::CP0::Status::IM::IM0::ValueMask_Disable
					);

	Oscl::TI::TNETV105X::Basic::ConfigChipSelects();
	}

