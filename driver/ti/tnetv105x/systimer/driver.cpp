/*
   Copyright (C) 2006 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "driver.h"

//#include "oscl/error/info.h"

using namespace Oscl::TI::TNETV105X::SYS_TIMER;

Driver::Driver(	Oscl::HW::TI::TNETV105X::SYS_TIMER::Map&	registers,
				uint16_t							period,
				uint8_t								prescaler
				) noexcept:
		_timer(	registers,prescaler),
		_period(period)
		{
	}

#include "oscl/error/info.h"

void	Driver::start() noexcept{
	_timer.clearInterrupt();
	_timer.startAutoLoadMode(_period);
	}

void	Driver::stop() noexcept{
	_timer.stopTimer();
	}

void	Driver::attach(Observer& observer) noexcept{
	_observers.put(&observer);
	}

void	Driver::detach(Observer& observer) noexcept{
	_observers.remove(&observer);
	}

bool	Driver::interrupt() noexcept{
	if(!_timer.interruptPending()){
		return false;
		}
	_timer.clearInterrupt();
	Observer*	observer;
	for(	observer=_observers.first();
			observer;
			observer=_observers.next(observer)
			){
		observer->tick();
		}
	return true;
	}

