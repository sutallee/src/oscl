/*
   Copyright (C) 2006 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "timer.h"
#include "oscl/error/info.h"

using namespace Oscl::TI::TNETV105X::SYS_TIMER;

Timer::Timer(	Oscl::HW::TI::TNETV105X::SYS_TIMER::Map&	registers,
				uint8_t								prescaler
				) noexcept:
		_registers(registers)
		{
	_prescaler	= prescaler;
	_prescaler	<<=	Oscl::HW::TI::TNETV105X::SYS_TIMER::CTRL::CLOCK_PRESCALER::Lsb;
	_prescaler	&=	Oscl::HW::TI::TNETV105X::SYS_TIMER::CTRL::CLOCK_PRESCALER::FieldMask;
	if(_prescaler){
		_prescaler	|= Oscl::HW::TI::TNETV105X::SYS_TIMER::CTRL::ENABLE_PRESCALER::ValueMask_Enable;
		}
	// Initialize mode and disable timer
	_registers.ctrl	=	Oscl::HW::TI::TNETV105X::SYS_TIMER::CTRL::START_STOP::ValueMask_Stop;
	// Clear pending interrupts
	_registers.intr	= Oscl::HW::TI::TNETV105X::SYS_TIMER::INT::COUNT_INT::ValueMask_Clear;
	}

#include "oscl/error/info.h"

bool	Timer::interruptPending() noexcept{
	bool	result	= ((_registers.intr & Oscl::HW::TI::TNETV105X::SYS_TIMER::INT::COUNT_INT::FieldMask) == Oscl::HW::TI::TNETV105X::SYS_TIMER::INT::COUNT_INT::ValueMask_Active);
	return result;
	}

void	Timer::clearInterrupt() noexcept{
	_registers.intr	= Oscl::HW::TI::TNETV105X::SYS_TIMER::INT::COUNT_INT::ValueMask_Clear;
	}

void	Timer::startOneShotMode(uint16_t nTicks) noexcept{
	_registers.ctrl	= Oscl::HW::TI::TNETV105X::SYS_TIMER::CTRL::START_STOP::ValueMask_Stop;
	clearInterrupt();
	_registers.load	=		(nTicks & Oscl::HW::TI::TNETV105X::SYS_TIMER::LOAD::START_LOAD::FieldMask);
	_registers.ctrl	=		Oscl::HW::TI::TNETV105X::SYS_TIMER::CTRL::AUTO_LOAD::ValueMask_SingleShot
						|	Oscl::HW::TI::TNETV105X::SYS_TIMER::CTRL::START_STOP::ValueMask_Start
						|	_prescaler
						;
	}

void	Timer::startAutoLoadMode(uint16_t nTicks) noexcept{
	_registers.ctrl	= Oscl::HW::TI::TNETV105X::SYS_TIMER::CTRL::START_STOP::ValueMask_Stop;
	clearInterrupt();
	_registers.load	=		(nTicks & Oscl::HW::TI::TNETV105X::SYS_TIMER::LOAD::START_LOAD::FieldMask);
	_registers.ctrl	=		Oscl::HW::TI::TNETV105X::SYS_TIMER::CTRL::AUTO_LOAD::ValueMask_AutoLoad
						|	Oscl::HW::TI::TNETV105X::SYS_TIMER::CTRL::START_STOP::ValueMask_Start
						|	_prescaler
						;
	}

void	Timer::restartTimer() noexcept{
	uint32_t	value	= _registers.ctrl;
	value	&= ~Oscl::HW::TI::TNETV105X::SYS_TIMER::CTRL::START_STOP::FieldMask;
	value	|= Oscl::HW::TI::TNETV105X::SYS_TIMER::CTRL::START_STOP::ValueMask_Start;
	_registers.ctrl	= value;
	}

void	Timer::stopTimer() noexcept{
	uint32_t	value	= _registers.ctrl;
	value	&= ~Oscl::HW::TI::TNETV105X::SYS_TIMER::CTRL::START_STOP::FieldMask;
	value	|= Oscl::HW::TI::TNETV105X::SYS_TIMER::CTRL::START_STOP::ValueMask_Stop;
	_registers.ctrl	= value;
	}

Oscl::HW::TI::TNETV105X::SYS_TIMER::VALUE::Reg	Timer::getCurrentCounterValue() noexcept{
	return _registers.value;
	}

