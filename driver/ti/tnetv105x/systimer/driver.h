/*
   Copyright (C) 2006 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_ti_tnetv105x_systimer_driverh_
#define _oscl_drv_ti_tnetv105x_systimer_driverh_
#include "api.h"
#include "oscl/hw/ti/tnetv105x/systimer.h"
#include "oscl/interrupt/shandler.h"
#include "oscl/queue/queue.h"
#include "oscl/queue/queueitem.h"
#include "timer.h"

/** */
namespace Oscl {
/** */
namespace TI {
/** */
namespace TNETV105X {
/** */
namespace SYS_TIMER {

/** */
class Driver : public Api {
	private:
		/** */
		Timer											_timer;

		/** */
		Oscl::Queue<Observer>							_observers;

		/** */
		uint16_t								_period;

	public:
		/** */
		Driver(	Oscl::HW::TI::TNETV105X::SYS_TIMER::Map&	registers,
				uint16_t							period,
				uint8_t								prescaler
				) noexcept;

		/** */
		void	start() noexcept;

		/** */
		void	stop() noexcept;

		/** */
		void	attach(Observer& observer) noexcept;

		/** */
		void	detach(Observer& observer) noexcept;

	private:
		/** */
		bool	interrupt() noexcept;
	};

}
}
}
}

#endif
