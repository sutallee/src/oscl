/*
   Copyright (C) 2006 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_ti_tnetv105x_systimer_timerh_
#define _oscl_drv_ti_tnetv105x_systimer_timerh_
#include "oscl/hw/ti/tnetv105x/systimer.h"

/** */
namespace Oscl {
/** */
namespace TI {
/** */
namespace TNETV105X {
/** */
namespace SYS_TIMER {

/** */
class Timer {
	private:
		/** */
		Oscl::HW::TI::TNETV105X::SYS_TIMER::Map&		_registers;
		/** */
		Oscl::HW::TI::TNETV105X::SYS_TIMER::CTRL::Reg	_prescaler;

	public:
		/** */
		Timer(	Oscl::HW::TI::TNETV105X::SYS_TIMER::Map&	registers,
				uint8_t								prescaler
				) noexcept;
		/** */
		bool	interruptPending() noexcept;
		/** */
		void	clearInterrupt() noexcept;
		/** */
		void	startAutoLoadMode(	uint16_t nTicks) noexcept;
		/** */
		void	startOneShotMode(	uint16_t nTicks) noexcept;
		/** */
		void	restartTimer() noexcept;
		/** */
		void	stopTimer() noexcept;
		/** */
		Oscl::HW::TI::TNETV105X::SYS_TIMER::VALUE::Reg	getCurrentCounterValue() noexcept;
	};

}
}
}
}

#endif
