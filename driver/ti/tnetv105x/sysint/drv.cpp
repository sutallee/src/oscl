/*
   Copyright (C) 2006 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "drv.h"

#include "oscl/error/info.h"

using namespace Oscl::TI::TNETV105X::SYS_INT;

Driver::Driver(Oscl::HW::TI::TNETV105X::SYS_INT::Map& sysint) noexcept:
		secondary(sysint),
		ext_int1(sysint),
		ext_int2(sysint),
		ext_int3(sysint),
		ext_int4(sysint),
		timerZero(sysint),
		timerOne(sysint),
		uart(sysint),
		int_8(sysint),
		dmaZero(sysint),
		dmaOne(sysint),
		int_11(sysint),
		int_12(sysint),
		int_13(sysint),
		int_14(sysint),
		int_15(sysint),
		int_16(sysint),
		int_17(sysint),
		int_18(sysint),
		ethernetMacZero(sysint),
		int_20(sysint),
		dsp_gpio(sysint),
		dspReset(sysint),
		tele_int(sysint),
		usbDevice(sysint),
		vlynq5(sysint),
		vlynq3(sysint),
		ethernetSwitchDMA(sysint),
		ethernetPHYs(sysint),
		ssp(sysint),
		dmaTwo(sysint),
		dmaThree(sysint),
		telephony(sysint),
		ethernetMacOne(sysint),
		int_34(sysint),
		usbHost(sysint),
		lcd(sysint),
		keypad(sysint),
		int_38(sysint),
		int_39(sysint)
		{
	}

Oscl::TI::TNETV105X::SYS_INT::IrqDrvApi&	Driver::getSecondary() noexcept{
	return secondary;
	}

Oscl::TI::TNETV105X::SYS_INT::IrqDrvApi&	Driver::getEXT_INT1() noexcept{
	return ext_int1;
	}

Oscl::TI::TNETV105X::SYS_INT::IrqDrvApi&	Driver::getEXT_INT2() noexcept{
	return ext_int2;
	}

Oscl::TI::TNETV105X::SYS_INT::IrqDrvApi&	Driver::getEXT_INT3() noexcept{
	return ext_int3;
	}

Oscl::TI::TNETV105X::SYS_INT::IrqDrvApi&	Driver::getEXT_INT4() noexcept{
	return ext_int4;
	}

Oscl::TI::TNETV105X::SYS_INT::IrqDrvApi&	Driver::getTimerZero() noexcept{
	return timerZero;
	}

Oscl::TI::TNETV105X::SYS_INT::IrqDrvApi&	Driver::getTimerOne() noexcept{
	return timerOne;
	}

Oscl::TI::TNETV105X::SYS_INT::IrqDrvApi&	Driver::getUart() noexcept{
	return uart;
	}

Oscl::TI::TNETV105X::SYS_INT::IrqDrvApi&	Driver::getINT_8() noexcept{
	return int_8;
	}

Oscl::TI::TNETV105X::SYS_INT::IrqDrvApi&	Driver::getDmaZero() noexcept{
	return dmaZero;
	}

Oscl::TI::TNETV105X::SYS_INT::IrqDrvApi&	Driver::getDmaOne() noexcept{
	return dmaOne;
	}

Oscl::TI::TNETV105X::SYS_INT::IrqDrvApi&	Driver::getINT_11() noexcept{
	return int_11;
	}

Oscl::TI::TNETV105X::SYS_INT::IrqDrvApi&	Driver::getINT_12() noexcept{
	return int_12;
	}

Oscl::TI::TNETV105X::SYS_INT::IrqDrvApi&	Driver::getINT_13() noexcept{
	return int_13;
	}

Oscl::TI::TNETV105X::SYS_INT::IrqDrvApi&	Driver::getINT_14() noexcept{
	return int_14;
	}

Oscl::TI::TNETV105X::SYS_INT::IrqDrvApi&	Driver::getINT_15() noexcept{
	return int_15;
	}

Oscl::TI::TNETV105X::SYS_INT::IrqDrvApi&	Driver::getINT_16() noexcept{
	return int_16;
	}

Oscl::TI::TNETV105X::SYS_INT::IrqDrvApi&	Driver::getINT_17() noexcept{
	return int_17;
	}

Oscl::TI::TNETV105X::SYS_INT::IrqDrvApi&	Driver::getINT_18() noexcept{
	return int_18;
	}

Oscl::TI::TNETV105X::SYS_INT::IrqDrvApi&	Driver::getEthernetMacZero() noexcept{
	return ethernetMacZero;
	}

Oscl::TI::TNETV105X::SYS_INT::IrqDrvApi&	Driver::getINT_20() noexcept{
	return int_20;
	}

Oscl::TI::TNETV105X::SYS_INT::IrqDrvApi&	Driver::getDSP_GPIO() noexcept{
	return dsp_gpio;
	}

Oscl::TI::TNETV105X::SYS_INT::IrqDrvApi&	Driver::getDspReset() noexcept{
	return dspReset;
	}

Oscl::TI::TNETV105X::SYS_INT::IrqDrvApi&	Driver::getTELE_INT() noexcept{
	return tele_int;
	}

Oscl::TI::TNETV105X::SYS_INT::IrqDrvApi&	Driver::getUsbDevice() noexcept{
	return usbDevice;
	}

Oscl::TI::TNETV105X::SYS_INT::IrqDrvApi&	Driver::getVLYNQ5() noexcept{
	return vlynq5;
	}

Oscl::TI::TNETV105X::SYS_INT::IrqDrvApi&	Driver::getVLYNQ3() noexcept{
	return vlynq3;
	}

Oscl::TI::TNETV105X::SYS_INT::IrqDrvApi&	Driver::getEthernetSwitchDMA() noexcept{
	return ethernetSwitchDMA;
	}

Oscl::TI::TNETV105X::SYS_INT::IrqDrvApi&	Driver::getEthernetPHYs() noexcept{
	return ethernetPHYs;
	}

Oscl::TI::TNETV105X::SYS_INT::IrqDrvApi&	Driver::getSSP() noexcept{
	return ssp;
	}

Oscl::TI::TNETV105X::SYS_INT::IrqDrvApi&	Driver::getDmaTwo() noexcept{
	return dmaTwo;
	}

Oscl::TI::TNETV105X::SYS_INT::IrqDrvApi&	Driver::getDmaThree() noexcept{
	return dmaThree;
	}

Oscl::TI::TNETV105X::SYS_INT::IrqDrvApi&	Driver::getTelephony() noexcept{
	return telephony;
	}

Oscl::TI::TNETV105X::SYS_INT::IrqDrvApi&	Driver::getEthernetMacOne() noexcept{
	return ethernetMacOne;
	}

Oscl::TI::TNETV105X::SYS_INT::IrqDrvApi&	Driver::getINT_34() noexcept{
	return int_34;
	}

Oscl::TI::TNETV105X::SYS_INT::IrqDrvApi&	Driver::getUsbHost() noexcept{
	return usbHost;
	}

Oscl::TI::TNETV105X::SYS_INT::IrqDrvApi&	Driver::getLCD() noexcept{
	return lcd;
	}

Oscl::TI::TNETV105X::SYS_INT::IrqDrvApi&	Driver::getKeypad() noexcept{
	return keypad;
	}

Oscl::TI::TNETV105X::SYS_INT::IrqDrvApi&	Driver::getINT_38() noexcept{
	return int_38;
	}

Oscl::TI::TNETV105X::SYS_INT::IrqDrvApi&	Driver::getINT_39() noexcept{
	return int_38;
	}
