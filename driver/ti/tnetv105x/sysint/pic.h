/*
   Copyright (C) 2006 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_ti_tnetv105x_sysint_pich_
#define _oscl_drv_ti_tnetv105x_sysint_pich_
#include "picapi.h"
#include "irqapi.h"
#include "oscl/hw/ti/tnetv105x/sysintreg.h"

/** */
namespace Oscl {
/** */
namespace TI {
/** */
namespace TNETV105X {
/** */
namespace SYS_INT {

/** The TNETV105X actually has two priority interrupt controllers: one for
	for the IRQ interrupt input; and one for the FIQ interrupt input.
	This class encompasses both since they have the same interface.
 */
class PIC : public PicApi{
	private:
		class NoInterrupt : public Oscl::TI::TNETV105X::SYS_INT::IrqApi {
			public:
				/** */
				explicit NoInterrupt() noexcept{}

			private:	// Oscl::TI::TNETV105X::SYS_INT::IrqApi
				/** */
				void	reserve(	Oscl::Interrupt::
									StatusHandlerApi&	handler
									) noexcept{}
				/** */
				void	release() noexcept{}

			private:	// Oscl::Interrupt::StatusHandlerApi
				/** */
				bool	interrupt() noexcept{return false;}
			};
	private:
		/** */
		volatile Oscl::HW::TI::TNETV105X::SYS_INT::PRIORITY_INDEX::Reg&	_priority_index;
		/** */
		NoInterrupt										_noInterrupt;
		/** */
		enum{maxVectors=40+1};
		/** */
		IrqApi*											_handler[maxVectors];
	public:
		/** */
		PIC(	volatile Oscl::HW::TI::TNETV105X::SYS_INT::PRIORITY_INDEX::Reg&		priority_index,
				IrqApi&												vect00,
				IrqApi&												vect01,
				IrqApi&												vect02,
				IrqApi&												vect03,
				IrqApi&												vect04,
				IrqApi&												vect05,
				IrqApi&												vect06,
				IrqApi&												vect07,
				IrqApi&												vect08,
				IrqApi&												vect09,
				IrqApi&												vect0A,
				IrqApi&												vect0B,
				IrqApi&												vect0C,
				IrqApi&												vect0D,
				IrqApi&												vect0E,
				IrqApi&												vect0F,
				IrqApi&												vect10,
				IrqApi&												vect11,
				IrqApi&												vect12,
				IrqApi&												vect13,
				IrqApi&												vect14,
				IrqApi&												vect15,
				IrqApi&												vect16,
				IrqApi&												vect17,
				IrqApi&												vect18,
				IrqApi&												vect19,
				IrqApi&												vect1A,
				IrqApi&												vect1B,
				IrqApi&												vect1C,
				IrqApi&												vect1D,
				IrqApi&												vect1E,
				IrqApi&												vect1F,
				IrqApi&												vect20,
				IrqApi&												vect21,
				IrqApi&												vect22,
				IrqApi&												vect23,
				IrqApi&												vect24,
				IrqApi&												vect25,
				IrqApi&												vect26,
				IrqApi&												vect27
				) noexcept;

	public:	// PicApi
		/** */
		void	reserve(	unsigned vector,
							Oscl::Interrupt::StatusHandlerApi&	handler
							) noexcept;
		/** */
		void	release(unsigned vector) noexcept;

		/** */
		IrqApi&	getIrqApi(unsigned vector) noexcept;

		/** */
		bool	interrupt() noexcept;
	};

}
}
}
}

#endif
