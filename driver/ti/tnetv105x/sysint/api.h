/*
   Copyright (C) 2006 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_ti_tnetv105x_sysint_apih_
#define _oscl_drv_ti_tnetv105x_sysint_apih_
#include "oscl/interrupt/shandler.h"
#include "irqapi.h"

/** */
namespace Oscl {
/** */
namespace TI {
/** */
namespace TNETV105X {
/** */
namespace SYS_INT {

/** */
class Api {
	public:
		/** */
		virtual ~Api() {}
		/** */
		virtual Oscl::Interrupt::StatusHandlerApi&	getISR() noexcept=0;
		/** */
		virtual Oscl::TI::TNETV105X::SYS_INT::IrqApi&	getSecondary() noexcept=0;
		/** */
		virtual Oscl::TI::TNETV105X::SYS_INT::IrqApi&	getEXT_INT1() noexcept=0;
		/** */
		virtual Oscl::TI::TNETV105X::SYS_INT::IrqApi&	getEXT_INT2() noexcept=0;
		/** */
		virtual Oscl::TI::TNETV105X::SYS_INT::IrqApi&	getEXT_INT3() noexcept=0;
		/** */
		virtual Oscl::TI::TNETV105X::SYS_INT::IrqApi&	getEXT_INT4() noexcept=0;
		/** */
		virtual Oscl::TI::TNETV105X::SYS_INT::IrqApi&	getTimerZero() noexcept=0;
		/** */
		virtual Oscl::TI::TNETV105X::SYS_INT::IrqApi&	getTimerOne() noexcept=0;
		/** */
		virtual Oscl::TI::TNETV105X::SYS_INT::IrqApi&	getUart() noexcept=0;
		/** */
		virtual Oscl::TI::TNETV105X::SYS_INT::IrqApi&	getDmaZero() noexcept=0;
		/** */
		virtual Oscl::TI::TNETV105X::SYS_INT::IrqApi&	getDmaOne() noexcept=0;
		/** */
		virtual Oscl::TI::TNETV105X::SYS_INT::IrqApi&	getEthernetMacZero() noexcept=0;
		/** */
		virtual Oscl::TI::TNETV105X::SYS_INT::IrqApi&	getDSP_GPIO() noexcept=0;
		/** */
		virtual Oscl::TI::TNETV105X::SYS_INT::IrqApi&	getDspReset() noexcept=0;
		/** */
		virtual Oscl::TI::TNETV105X::SYS_INT::IrqApi&	getTELE_INT() noexcept=0;
		/** */
		virtual Oscl::TI::TNETV105X::SYS_INT::IrqApi&	getUsbDevice() noexcept=0;
		/** */
		virtual Oscl::TI::TNETV105X::SYS_INT::IrqApi&	getVLYNQ5() noexcept=0;
		/** */
		virtual Oscl::TI::TNETV105X::SYS_INT::IrqApi&	getVLYNQ3() noexcept=0;
		/** */
		virtual Oscl::TI::TNETV105X::SYS_INT::IrqApi&	getEthernetSwitchDMA() noexcept=0;
		/** */
		virtual Oscl::TI::TNETV105X::SYS_INT::IrqApi&	getEthernetPHYs() noexcept=0;
		/** */
		virtual Oscl::TI::TNETV105X::SYS_INT::IrqApi&	getSSP() noexcept=0;
		/** */
		virtual Oscl::TI::TNETV105X::SYS_INT::IrqApi&	getDmaTwo() noexcept=0;
		/** */
		virtual Oscl::TI::TNETV105X::SYS_INT::IrqApi&	getDmaThree() noexcept=0;
		/** */
		virtual Oscl::TI::TNETV105X::SYS_INT::IrqApi&	getTelephony() noexcept=0;
		/** */
		virtual Oscl::TI::TNETV105X::SYS_INT::IrqApi&	getEthernetMacOne() noexcept=0;
		/** */
		virtual Oscl::TI::TNETV105X::SYS_INT::IrqApi&	getUsbHost() noexcept=0;
		/** */
		virtual Oscl::TI::TNETV105X::SYS_INT::IrqApi&	getLCD() noexcept=0;
		/** */
		virtual Oscl::TI::TNETV105X::SYS_INT::IrqApi&	getKeypad() noexcept=0;
	};

}
}
}
}

#endif
