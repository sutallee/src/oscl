/*
   Copyright (C) 2006 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "map.h"

using namespace Oscl::TI::TNETV105X::SYS_INT;

Map::Map(Oscl::TI::TNETV105X::SYS_INT::PicApi& pic) noexcept:
		_pic(pic)
		{
	}

Oscl::Interrupt::StatusHandlerApi&	Map::getISR() noexcept{
	return _pic;
	}

Oscl::TI::TNETV105X::SYS_INT::IrqApi&	Map::getSecondary() noexcept{
	return _pic.getIrqApi(0);
	}

Oscl::TI::TNETV105X::SYS_INT::IrqApi&	Map::getEXT_INT1() noexcept{
	return _pic.getIrqApi(1);
	}

Oscl::TI::TNETV105X::SYS_INT::IrqApi&	Map::getEXT_INT2() noexcept{
	return _pic.getIrqApi(2);
	}

Oscl::TI::TNETV105X::SYS_INT::IrqApi&	Map::getEXT_INT3() noexcept{
	return _pic.getIrqApi(3);
	}

Oscl::TI::TNETV105X::SYS_INT::IrqApi&	Map::getEXT_INT4() noexcept{
	return _pic.getIrqApi(4);
	}

Oscl::TI::TNETV105X::SYS_INT::IrqApi&	Map::getTimerZero() noexcept{
	return _pic.getIrqApi(5);
	}

Oscl::TI::TNETV105X::SYS_INT::IrqApi&	Map::getTimerOne() noexcept{
	return _pic.getIrqApi(6);
	}

Oscl::TI::TNETV105X::SYS_INT::IrqApi&	Map::getUart() noexcept{
	return _pic.getIrqApi(7);
	}

Oscl::TI::TNETV105X::SYS_INT::IrqApi&	Map::getDmaZero() noexcept{
	return _pic.getIrqApi(9);
	}

Oscl::TI::TNETV105X::SYS_INT::IrqApi&	Map::getDmaOne() noexcept{
	return _pic.getIrqApi(10);
	}

Oscl::TI::TNETV105X::SYS_INT::IrqApi&	Map::getEthernetMacZero() noexcept{
	return _pic.getIrqApi(19);
	}

Oscl::TI::TNETV105X::SYS_INT::IrqApi&	Map::getDSP_GPIO() noexcept{
	return _pic.getIrqApi(21);
	}

Oscl::TI::TNETV105X::SYS_INT::IrqApi&	Map::getDspReset() noexcept{
	return _pic.getIrqApi(22);
	}

Oscl::TI::TNETV105X::SYS_INT::IrqApi&	Map::getTELE_INT() noexcept{
	return _pic.getIrqApi(23);
	}

Oscl::TI::TNETV105X::SYS_INT::IrqApi&	Map::getUsbDevice() noexcept{
	return _pic.getIrqApi(24);
	}

Oscl::TI::TNETV105X::SYS_INT::IrqApi&	Map::getVLYNQ5() noexcept{
	return _pic.getIrqApi(25);
	}

Oscl::TI::TNETV105X::SYS_INT::IrqApi&	Map::getVLYNQ3() noexcept{
	return _pic.getIrqApi(26);
	}

Oscl::TI::TNETV105X::SYS_INT::IrqApi&	Map::getEthernetSwitchDMA() noexcept{
	return _pic.getIrqApi(27);
	}

Oscl::TI::TNETV105X::SYS_INT::IrqApi&	Map::getEthernetPHYs() noexcept{
	return _pic.getIrqApi(28);
	}

Oscl::TI::TNETV105X::SYS_INT::IrqApi&	Map::getSSP() noexcept{
	return _pic.getIrqApi(29);
	}

Oscl::TI::TNETV105X::SYS_INT::IrqApi&	Map::getDmaTwo() noexcept{
	return _pic.getIrqApi(30);
	}

Oscl::TI::TNETV105X::SYS_INT::IrqApi&	Map::getDmaThree() noexcept{
	return _pic.getIrqApi(31);
	}

Oscl::TI::TNETV105X::SYS_INT::IrqApi&	Map::getTelephony() noexcept{
	return _pic.getIrqApi(32);
	}

Oscl::TI::TNETV105X::SYS_INT::IrqApi&	Map::getEthernetMacOne() noexcept{
	return _pic.getIrqApi(33);
	}

Oscl::TI::TNETV105X::SYS_INT::IrqApi&	Map::getUsbHost() noexcept{
	return _pic.getIrqApi(35);
	}

Oscl::TI::TNETV105X::SYS_INT::IrqApi&	Map::getLCD() noexcept{
	return _pic.getIrqApi(36);
	}

Oscl::TI::TNETV105X::SYS_INT::IrqApi&	Map::getKeypad() noexcept{
	return _pic.getIrqApi(37);
	}

