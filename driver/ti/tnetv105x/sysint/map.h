/*
   Copyright (C) 2006 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_ti_tnetv105x_sysint_maph_
#define _oscl_drv_ti_tnetv105x_sysint_maph_
#include "api.h"
#include "oscl/driver/ti/tnetv105x/sysint/picapi.h"

/** */
namespace Oscl {
/** */
namespace TI {
/** */
namespace TNETV105X {
/** */
namespace SYS_INT {

/** */
class Map : public Api {
	private:
		/** */
		Oscl::TI::TNETV105X::SYS_INT::PicApi&	_pic;

	public:
		/** */
		Map(Oscl::TI::TNETV105X::SYS_INT::PicApi& pic) noexcept;

	private:	// SYS_INT::PicApi
		/** */
		Oscl::Interrupt::StatusHandlerApi&	getISR() noexcept;
		/** */
		Oscl::TI::TNETV105X::SYS_INT::IrqApi&	getSecondary() noexcept;
		/** */
		Oscl::TI::TNETV105X::SYS_INT::IrqApi&	getEXT_INT1() noexcept;
		/** */
		Oscl::TI::TNETV105X::SYS_INT::IrqApi&	getEXT_INT2() noexcept;
		/** */
		Oscl::TI::TNETV105X::SYS_INT::IrqApi&	getEXT_INT3() noexcept;
		/** */
		Oscl::TI::TNETV105X::SYS_INT::IrqApi&	getEXT_INT4() noexcept;
		/** */
		Oscl::TI::TNETV105X::SYS_INT::IrqApi&	getTimerZero() noexcept;
		/** */
		Oscl::TI::TNETV105X::SYS_INT::IrqApi&	getTimerOne() noexcept;
		/** */
		Oscl::TI::TNETV105X::SYS_INT::IrqApi&	getUart() noexcept;
		/** */
		Oscl::TI::TNETV105X::SYS_INT::IrqApi&	getDmaZero() noexcept;
		/** */
		Oscl::TI::TNETV105X::SYS_INT::IrqApi&	getDmaOne() noexcept;
		/** */
		Oscl::TI::TNETV105X::SYS_INT::IrqApi&	getEthernetMacZero() noexcept;
		/** */
		Oscl::TI::TNETV105X::SYS_INT::IrqApi&	getDSP_GPIO() noexcept;
		/** */
		Oscl::TI::TNETV105X::SYS_INT::IrqApi&	getDspReset() noexcept;
		/** */
		Oscl::TI::TNETV105X::SYS_INT::IrqApi&	getTELE_INT() noexcept;
		/** */
		Oscl::TI::TNETV105X::SYS_INT::IrqApi&	getUsbDevice() noexcept;
		/** */
		Oscl::TI::TNETV105X::SYS_INT::IrqApi&	getVLYNQ5() noexcept;
		/** */
		Oscl::TI::TNETV105X::SYS_INT::IrqApi&	getVLYNQ3() noexcept;
		/** */
		Oscl::TI::TNETV105X::SYS_INT::IrqApi&	getEthernetSwitchDMA() noexcept;
		/** */
		Oscl::TI::TNETV105X::SYS_INT::IrqApi&	getEthernetPHYs() noexcept;
		/** */
		Oscl::TI::TNETV105X::SYS_INT::IrqApi&	getSSP() noexcept;
		/** */
		Oscl::TI::TNETV105X::SYS_INT::IrqApi&	getDmaTwo() noexcept;
		/** */
		Oscl::TI::TNETV105X::SYS_INT::IrqApi&	getDmaThree() noexcept;
		/** */
		Oscl::TI::TNETV105X::SYS_INT::IrqApi&	getTelephony() noexcept;
		/** */
		Oscl::TI::TNETV105X::SYS_INT::IrqApi&	getEthernetMacOne() noexcept;
		/** */
		Oscl::TI::TNETV105X::SYS_INT::IrqApi&	getUsbHost() noexcept;
		/** */
		Oscl::TI::TNETV105X::SYS_INT::IrqApi&	getLCD() noexcept;
		/** */
		Oscl::TI::TNETV105X::SYS_INT::IrqApi&	getKeypad() noexcept;
	};

}
}
}
}

#endif
