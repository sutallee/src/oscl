/*
   Copyright (C) 2006 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_ti_tnetv105x_sysint_irqdrvapih_
#define _oscl_drv_ti_tnetv105x_sysint_irqdrvapih_

/** */
namespace Oscl {
/** */
namespace TI {
/** */
namespace TNETV105X {
/** */
namespace SYS_INT {

/** */
class IrqDrvApi {
	public:
		/** Be Happy GCC */
		virtual ~IrqDrvApi() {}

		/** */
		virtual void	enable() noexcept=0;

		/** */
		virtual void	disable() noexcept=0;

		/** Value of STAT_MSK_x bit */
		virtual bool	pending() noexcept=0;

		/** Value of STAT_RAW_x bit */
		virtual bool	rawActive() noexcept=0;

		/** */
		virtual void	clear() noexcept=0;

	public: // TYPE
		/** */
		virtual void	selectTypeEdge() noexcept=0;

		/** */
		virtual void	selectTypeLevel() noexcept=0;

	public: // POLARITY
		/** */
		virtual void	selectPolarityHIGH() noexcept=0;
		/** */
		virtual void	selectPolarityLOW() noexcept=0;

	};

}
}
}
}

#endif
