/*
   Copyright (C) 2006 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_ti_tnetv105x_sysint_drvapih_
#define _oscl_drv_ti_tnetv105x_sysint_drvapih_
#include "irqdrvapi.h"

/** */
namespace Oscl {
/** */
namespace TI {
/** */
namespace TNETV105X {
/** */
namespace SYS_INT {

/** */
class DrvApi {
	public:
		/** */
		virtual ~DrvApi() {}

	public:
		/** */
		virtual Oscl::TI::TNETV105X::SYS_INT::IrqDrvApi&	getSecondary() noexcept=0;
		/** */
		virtual Oscl::TI::TNETV105X::SYS_INT::IrqDrvApi&	getEXT_INT1() noexcept=0;
		/** */
		virtual Oscl::TI::TNETV105X::SYS_INT::IrqDrvApi&	getEXT_INT2() noexcept=0;
		/** */
		virtual Oscl::TI::TNETV105X::SYS_INT::IrqDrvApi&	getEXT_INT3() noexcept=0;
		/** */
		virtual Oscl::TI::TNETV105X::SYS_INT::IrqDrvApi&	getEXT_INT4() noexcept=0;
		/** */
		virtual Oscl::TI::TNETV105X::SYS_INT::IrqDrvApi&	getTimerZero() noexcept=0;
		/** */
		virtual Oscl::TI::TNETV105X::SYS_INT::IrqDrvApi&	getTimerOne() noexcept=0;
		/** */
		virtual Oscl::TI::TNETV105X::SYS_INT::IrqDrvApi&	getUart() noexcept=0;
		/** */
		virtual Oscl::TI::TNETV105X::SYS_INT::IrqDrvApi&	getINT_8() noexcept=0;
		/** */
		virtual Oscl::TI::TNETV105X::SYS_INT::IrqDrvApi&	getDmaZero() noexcept=0;
		/** */
		virtual Oscl::TI::TNETV105X::SYS_INT::IrqDrvApi&	getDmaOne() noexcept=0;
		/** */
		virtual Oscl::TI::TNETV105X::SYS_INT::IrqDrvApi&	getINT_11() noexcept=0;
		/** */
		virtual Oscl::TI::TNETV105X::SYS_INT::IrqDrvApi&	getINT_12() noexcept=0;
		/** */
		virtual Oscl::TI::TNETV105X::SYS_INT::IrqDrvApi&	getINT_13() noexcept=0;
		/** */
		virtual Oscl::TI::TNETV105X::SYS_INT::IrqDrvApi&	getINT_14() noexcept=0;
		/** */
		virtual Oscl::TI::TNETV105X::SYS_INT::IrqDrvApi&	getINT_15() noexcept=0;
		/** */
		virtual Oscl::TI::TNETV105X::SYS_INT::IrqDrvApi&	getINT_16() noexcept=0;
		/** */
		virtual Oscl::TI::TNETV105X::SYS_INT::IrqDrvApi&	getINT_17() noexcept=0;
		/** */
		virtual Oscl::TI::TNETV105X::SYS_INT::IrqDrvApi&	getINT_18() noexcept=0;
		/** */
		virtual Oscl::TI::TNETV105X::SYS_INT::IrqDrvApi&	getEthernetMacZero() noexcept=0;
		/** */
		virtual Oscl::TI::TNETV105X::SYS_INT::IrqDrvApi&	getINT_20() noexcept=0;
		/** */
		virtual Oscl::TI::TNETV105X::SYS_INT::IrqDrvApi&	getDSP_GPIO() noexcept=0;
		/** */
		virtual Oscl::TI::TNETV105X::SYS_INT::IrqDrvApi&	getDspReset() noexcept=0;
		/** */
		virtual Oscl::TI::TNETV105X::SYS_INT::IrqDrvApi&	getTELE_INT() noexcept=0;
		/** */
		virtual Oscl::TI::TNETV105X::SYS_INT::IrqDrvApi&	getUsbDevice() noexcept=0;
		/** */
		virtual Oscl::TI::TNETV105X::SYS_INT::IrqDrvApi&	getVLYNQ5() noexcept=0;
		/** */
		virtual Oscl::TI::TNETV105X::SYS_INT::IrqDrvApi&	getVLYNQ3() noexcept=0;
		/** */
		virtual Oscl::TI::TNETV105X::SYS_INT::IrqDrvApi&	getEthernetSwitchDMA() noexcept=0;
		/** */
		virtual Oscl::TI::TNETV105X::SYS_INT::IrqDrvApi&	getEthernetPHYs() noexcept=0;
		/** */
		virtual Oscl::TI::TNETV105X::SYS_INT::IrqDrvApi&	getSSP() noexcept=0;
		/** */
		virtual Oscl::TI::TNETV105X::SYS_INT::IrqDrvApi&	getDmaTwo() noexcept=0;
		/** */
		virtual Oscl::TI::TNETV105X::SYS_INT::IrqDrvApi&	getDmaThree() noexcept=0;
		/** */
		virtual Oscl::TI::TNETV105X::SYS_INT::IrqDrvApi&	getTelephony() noexcept=0;
		/** */
		virtual Oscl::TI::TNETV105X::SYS_INT::IrqDrvApi&	getEthernetMacOne() noexcept=0;
		/** */
		virtual Oscl::TI::TNETV105X::SYS_INT::IrqDrvApi&	getINT_34() noexcept=0;
		/** */
		virtual Oscl::TI::TNETV105X::SYS_INT::IrqDrvApi&	getUsbHost() noexcept=0;
		/** */
		virtual Oscl::TI::TNETV105X::SYS_INT::IrqDrvApi&	getLCD() noexcept=0;
		/** */
		virtual Oscl::TI::TNETV105X::SYS_INT::IrqDrvApi&	getKeypad() noexcept=0;
		/** */
		virtual Oscl::TI::TNETV105X::SYS_INT::IrqDrvApi&	getINT_38() noexcept=0;
		/** */
		virtual Oscl::TI::TNETV105X::SYS_INT::IrqDrvApi&	getINT_39() noexcept=0;
	};

}
}
}
}

#endif
