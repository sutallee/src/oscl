/*
   Copyright (C) 2006 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_ti_tnetv105x_sysint_drvh_
#define _oscl_drv_ti_tnetv105x_sysint_drvh_
#include "drvapi.h"
#include "irqdrv.h"

/** */
namespace Oscl {
/** */
namespace TI {
/** */
namespace TNETV105X {
/** */
namespace SYS_INT {

/** */
class Driver : public DrvApi {
	private:
		/** */
		IrqDriver1<0>	secondary;
		/** */
		IrqDriver1<1>	ext_int1;
		/** */
		IrqDriver1<2>	ext_int2;
		/** */
		IrqDriver1<3>	ext_int3;
		/** */
		IrqDriver1<4>	ext_int4;
		/** */
		IrqDriver1<5>	timerZero;
		/** */
		IrqDriver1<6>	timerOne;
		/** */
		IrqDriver1<7>	uart;
		/** */
		IrqDriver1<8>	int_8;
		/** */
		IrqDriver1<9>	dmaZero;
		/** */
		IrqDriver1<10>	dmaOne;
		/** */
		IrqDriver1<11>	int_11;
		/** */
		IrqDriver1<12>	int_12;
		/** */
		IrqDriver1<13>	int_13;
		/** */
		IrqDriver1<14>	int_14;
		/** */
		IrqDriver1<15>	int_15;
		/** */
		IrqDriver1<16>	int_16;
		/** */
		IrqDriver1<17>	int_17;
		/** */
		IrqDriver1<18>	int_18;
		/** */
		IrqDriver1<19>	ethernetMacZero;
		/** */
		IrqDriver1<20>	int_20;
		/** */
		IrqDriver1<21>	dsp_gpio;
		/** */
		IrqDriver1<22>	dspReset;
		/** */
		IrqDriver1<23>	tele_int;
		/** */
		IrqDriver1<24>	usbDevice;
		/** */
		IrqDriver1<25>	vlynq5;
		/** */
		IrqDriver1<26>	vlynq3;
		/** */
		IrqDriver1<27>	ethernetSwitchDMA;
		/** */
		IrqDriver1<28>	ethernetPHYs;
		/** */
		IrqDriver1<29>	ssp;
		/** */
		IrqDriver1<30>	dmaTwo;
		/** */
		IrqDriver1<31>	dmaThree;
		/** */
		IrqDriver2<0>	telephony;
		/** */
		IrqDriver2<1>	ethernetMacOne;
		/** */
		IrqDriver2<2>	int_34;
		/** */
		IrqDriver2<3>	usbHost;
		/** */
		IrqDriver2<4>	lcd;
		/** */
		IrqDriver2<5>	keypad;
		/** */
		IrqDriver2<6>	int_38;
		/** */
		IrqDriver2<7>	int_39;

	public:
		/** */
		Driver(Oscl::HW::TI::TNETV105X::SYS_INT::Map& sysint) noexcept;

	public:	// SYS_INT::DrvApi
		/** */
		Oscl::TI::TNETV105X::SYS_INT::IrqDrvApi&	getSecondary() noexcept;
		/** */
		Oscl::TI::TNETV105X::SYS_INT::IrqDrvApi&	getEXT_INT1() noexcept;
		/** */
		Oscl::TI::TNETV105X::SYS_INT::IrqDrvApi&	getEXT_INT2() noexcept;
		/** */
		Oscl::TI::TNETV105X::SYS_INT::IrqDrvApi&	getEXT_INT3() noexcept;
		/** */
		Oscl::TI::TNETV105X::SYS_INT::IrqDrvApi&	getEXT_INT4() noexcept;
		/** */
		Oscl::TI::TNETV105X::SYS_INT::IrqDrvApi&	getTimerZero() noexcept;
		/** */
		Oscl::TI::TNETV105X::SYS_INT::IrqDrvApi&	getTimerOne() noexcept;
		/** */
		Oscl::TI::TNETV105X::SYS_INT::IrqDrvApi&	getUart() noexcept;
		/** */
		Oscl::TI::TNETV105X::SYS_INT::IrqDrvApi&	getINT_8() noexcept;
		/** */
		Oscl::TI::TNETV105X::SYS_INT::IrqDrvApi&	getDmaZero() noexcept;
		/** */
		Oscl::TI::TNETV105X::SYS_INT::IrqDrvApi&	getDmaOne() noexcept;
		/** */
		Oscl::TI::TNETV105X::SYS_INT::IrqDrvApi&	getINT_11() noexcept;
		/** */
		Oscl::TI::TNETV105X::SYS_INT::IrqDrvApi&	getINT_12() noexcept;
		/** */
		Oscl::TI::TNETV105X::SYS_INT::IrqDrvApi&	getINT_13() noexcept;
		/** */
		Oscl::TI::TNETV105X::SYS_INT::IrqDrvApi&	getINT_14() noexcept;
		/** */
		Oscl::TI::TNETV105X::SYS_INT::IrqDrvApi&	getINT_15() noexcept;
		/** */
		Oscl::TI::TNETV105X::SYS_INT::IrqDrvApi&	getINT_16() noexcept;
		/** */
		Oscl::TI::TNETV105X::SYS_INT::IrqDrvApi&	getINT_17() noexcept;
		/** */
		Oscl::TI::TNETV105X::SYS_INT::IrqDrvApi&	getINT_18() noexcept;
		/** */
		Oscl::TI::TNETV105X::SYS_INT::IrqDrvApi&	getEthernetMacZero() noexcept;
		/** */
		Oscl::TI::TNETV105X::SYS_INT::IrqDrvApi&	getINT_20() noexcept;
		/** */
		Oscl::TI::TNETV105X::SYS_INT::IrqDrvApi&	getDSP_GPIO() noexcept;
		/** */
		Oscl::TI::TNETV105X::SYS_INT::IrqDrvApi&	getDspReset() noexcept;
		/** */
		Oscl::TI::TNETV105X::SYS_INT::IrqDrvApi&	getTELE_INT() noexcept;
		/** */
		Oscl::TI::TNETV105X::SYS_INT::IrqDrvApi&	getUsbDevice() noexcept;
		/** */
		Oscl::TI::TNETV105X::SYS_INT::IrqDrvApi&	getVLYNQ5() noexcept;
		/** */
		Oscl::TI::TNETV105X::SYS_INT::IrqDrvApi&	getVLYNQ3() noexcept;
		/** */
		Oscl::TI::TNETV105X::SYS_INT::IrqDrvApi&	getEthernetSwitchDMA() noexcept;
		/** */
		Oscl::TI::TNETV105X::SYS_INT::IrqDrvApi&	getEthernetPHYs() noexcept;
		/** */
		Oscl::TI::TNETV105X::SYS_INT::IrqDrvApi&	getSSP() noexcept;
		/** */
		Oscl::TI::TNETV105X::SYS_INT::IrqDrvApi&	getDmaTwo() noexcept;
		/** */
		Oscl::TI::TNETV105X::SYS_INT::IrqDrvApi&	getDmaThree() noexcept;
		/** */
		Oscl::TI::TNETV105X::SYS_INT::IrqDrvApi&	getTelephony() noexcept;
		/** */
		Oscl::TI::TNETV105X::SYS_INT::IrqDrvApi&	getEthernetMacOne() noexcept;
		/** */
		Oscl::TI::TNETV105X::SYS_INT::IrqDrvApi&	getINT_34() noexcept;
		/** */
		Oscl::TI::TNETV105X::SYS_INT::IrqDrvApi&	getUsbHost() noexcept;
		/** */
		Oscl::TI::TNETV105X::SYS_INT::IrqDrvApi&	getLCD() noexcept;
		/** */
		Oscl::TI::TNETV105X::SYS_INT::IrqDrvApi&	getKeypad() noexcept;
		/** */
		Oscl::TI::TNETV105X::SYS_INT::IrqDrvApi&	getINT_38() noexcept;
		/** */
		Oscl::TI::TNETV105X::SYS_INT::IrqDrvApi&	getINT_39() noexcept;
	};

}
}
}
}

#endif
