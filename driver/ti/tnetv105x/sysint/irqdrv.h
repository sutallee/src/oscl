/*
   Copyright (C) 2006 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_ti_tnetv105x_sysint_irqdrvh_
#define _oscl_drv_ti_tnetv105x_sysint_irqdrvh_
#include "irqdrvapi.h"
#include "oscl/hw/ti/tnetv105x/sysint.h"

/** */
namespace Oscl {
/** */
namespace TI {
/** */
namespace TNETV105X {
/** */
namespace SYS_INT {

/** */
template <unsigned IrqNumber>
class IrqDriver1 : public IrqDrvApi {
	private:
		/** */
		Oscl::HW::TI::TNETV105X::SYS_INT::Map&		_sysint;

	public:
		/** */
		IrqDriver1(Oscl::HW::TI::TNETV105X::SYS_INT::Map& sysint) noexcept:
				_sysint(sysint)
				{
			disable();
			}

		/** */
		void	enable() noexcept{
			_sysint.enbl_set_1	= Oscl::HW::TI::TNETV105X::SYS_INT::ENBL_SET_1::Enable << IrqNumber;
			}

		/** */
		void	disable() noexcept{
			_sysint.enbl_clr_1	= Oscl::HW::TI::TNETV105X::SYS_INT::ENBL_CLR_1::Disable << IrqNumber;
			}

		/** */
		bool	pending() noexcept{
			return (		_sysint.stat_msk_1
						&	(((Oscl::HW::TI::TNETV105X::SYS_INT::STAT_MSK_1::Reg)Oscl::HW::TI::TNETV105X::SYS_INT::STAT_MSK_1::Mask) << IrqNumber)
						)
					==	(((Oscl::HW::TI::TNETV105X::SYS_INT::STAT_MSK_1::Reg)Oscl::HW::TI::TNETV105X::SYS_INT::STAT_MSK_1::Pending) << IrqNumber);
			}
		/** */
		bool	rawActive() noexcept{
			return (		_sysint.stat_raw_1
						& (((Oscl::HW::TI::TNETV105X::SYS_INT::STAT_RAW_1::Reg)Oscl::HW::TI::TNETV105X::SYS_INT::STAT_RAW_1::Mask) << IrqNumber)
						)
					== (((Oscl::HW::TI::TNETV105X::SYS_INT::STAT_RAW_1::Reg)Oscl::HW::TI::TNETV105X::SYS_INT::STAT_RAW_1::Active) << IrqNumber);
			}
		/** */
		void	clear() noexcept{
			_sysint.stat_raw_1	= Oscl::HW::TI::TNETV105X::SYS_INT::STAT_RAW_1::Clear << IrqNumber;
			}

	public: // TYPE
		/** */
		void	selectTypeEdge() noexcept{
			Oscl::HW::TI::TNETV105X::SYS_INT::TYPE_1::Reg value	= _sysint.type_1;
			value	&= ~(Oscl::HW::TI::TNETV105X::SYS_INT::TYPE_1::Mask << IrqNumber);
			value	|= (Oscl::HW::TI::TNETV105X::SYS_INT::TYPE_1::Edge << IrqNumber);
			_sysint.type_1	= value;
			}

		/** */
		void	selectTypeLevel() noexcept{
			Oscl::HW::TI::TNETV105X::SYS_INT::TYPE_1::Reg value	= _sysint.type_1;
			value	&= ~(Oscl::HW::TI::TNETV105X::SYS_INT::TYPE_1::Mask << IrqNumber);
			value	|= (Oscl::HW::TI::TNETV105X::SYS_INT::TYPE_1::Level << IrqNumber);
			_sysint.type_1	= value;
			}

	public: // POLARITY
		/** */
		void	selectPolarityHIGH() noexcept{
			Oscl::HW::TI::TNETV105X::SYS_INT::POLARITY_1::Reg value	= _sysint.polarity_1;
			value	&= ~(Oscl::HW::TI::TNETV105X::SYS_INT::POLARITY_1::Mask << IrqNumber);
			value	|= (Oscl::HW::TI::TNETV105X::SYS_INT::POLARITY_1::ActiveHigh << IrqNumber);
			_sysint.polarity_1	= value;
			}

		/** */
		void	selectPolarityLOW() noexcept{
			Oscl::HW::TI::TNETV105X::SYS_INT::POLARITY_1::Reg value	= _sysint.polarity_1;
			value	&= ~(Oscl::HW::TI::TNETV105X::SYS_INT::POLARITY_1::Mask << IrqNumber);
			value	|= (Oscl::HW::TI::TNETV105X::SYS_INT::POLARITY_1::ActiveLow << IrqNumber);
			_sysint.polarity_1	= value;
			}

	};

/** */
template <unsigned IrqNumber>
class IrqDriver2 : public IrqDrvApi {
	private:
		/** */
		Oscl::HW::TI::TNETV105X::SYS_INT::Map&		_sysint;

	public:
		/** */
		IrqDriver2(Oscl::HW::TI::TNETV105X::SYS_INT::Map& sysint) noexcept:
				_sysint(sysint)
				{
			disable();
			}

		/** */
		void	enable() noexcept{
			_sysint.enbl_set_2	= Oscl::HW::TI::TNETV105X::SYS_INT::ENBL_SET_2::Enable << IrqNumber;
			}

		/** */
		void	disable() noexcept{
			_sysint.enbl_clr_2	= Oscl::HW::TI::TNETV105X::SYS_INT::ENBL_CLR_2::Disable << IrqNumber;
			}

		/** */
		bool	pending() noexcept{
			return (		_sysint.stat_msk_2
						&	(((Oscl::HW::TI::TNETV105X::SYS_INT::STAT_MSK_2::Reg)Oscl::HW::TI::TNETV105X::SYS_INT::STAT_MSK_2::Mask) << IrqNumber)
						)
					==	(((Oscl::HW::TI::TNETV105X::SYS_INT::STAT_MSK_2::Reg)Oscl::HW::TI::TNETV105X::SYS_INT::STAT_MSK_2::Pending) << IrqNumber);
			}
		/** */
		bool	rawActive() noexcept{
			return (		_sysint.stat_raw_2
						& (((Oscl::HW::TI::TNETV105X::SYS_INT::STAT_RAW_2::Reg)Oscl::HW::TI::TNETV105X::SYS_INT::STAT_RAW_2::Mask) << IrqNumber)
						)
					== (((Oscl::HW::TI::TNETV105X::SYS_INT::STAT_RAW_2::Reg)Oscl::HW::TI::TNETV105X::SYS_INT::STAT_RAW_2::Active) << IrqNumber);
			}
		/** */
		void	clear() noexcept{
			_sysint.stat_raw_1	= Oscl::HW::TI::TNETV105X::SYS_INT::STAT_RAW_2::Clear;
			}

	public: // TYPE
		/** */
		void	selectTypeEdge() noexcept{
			Oscl::HW::TI::TNETV105X::SYS_INT::TYPE_2::Reg value	= _sysint.type_2;
			value	&= ~(Oscl::HW::TI::TNETV105X::SYS_INT::TYPE_2::Mask << IrqNumber);
			value	|= (Oscl::HW::TI::TNETV105X::SYS_INT::TYPE_2::Edge << IrqNumber);
			_sysint.type_2	= value;
			}

		/** */
		void	selectTypeLevel() noexcept{
			Oscl::HW::TI::TNETV105X::SYS_INT::TYPE_2::Reg value	= _sysint.type_2;
			value	&= ~(Oscl::HW::TI::TNETV105X::SYS_INT::TYPE_2::Mask << IrqNumber);
			value	|= (Oscl::HW::TI::TNETV105X::SYS_INT::TYPE_2::Level << IrqNumber);
			_sysint.type_2	= value;
			}

	public: // POLARITY
		/** */
		void	selectPolarityHIGH() noexcept{
			Oscl::HW::TI::TNETV105X::SYS_INT::POLARITY_2::Reg value	= _sysint.polarity_2;
			value	&= ~(Oscl::HW::TI::TNETV105X::SYS_INT::POLARITY_2::Mask << IrqNumber);
			value	|= (Oscl::HW::TI::TNETV105X::SYS_INT::POLARITY_2::ActiveHigh << IrqNumber);
			_sysint.polarity_2	= value;
			}

		/** */
		void	selectPolarityLOW() noexcept{
			Oscl::HW::TI::TNETV105X::SYS_INT::POLARITY_2::Reg value	= _sysint.polarity_2;
			value	&= ~(Oscl::HW::TI::TNETV105X::SYS_INT::POLARITY_2::Mask << IrqNumber);
			value	|= (Oscl::HW::TI::TNETV105X::SYS_INT::POLARITY_2::ActiveLow << IrqNumber);
			_sysint.polarity_2	= value;
			}

	};
}
}
}
}

#endif
