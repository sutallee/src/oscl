/*
   Copyright (C) 2006 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <new>
#include "interrupt.h"
#include "oscl/memory/block.h"
#include "oscl/error/fatal.h"
#include "oscl/error/info.h"

/** */
namespace Oscl {
/** */
namespace TI {
/** */
namespace TNETV105X {
/** */
namespace SYS_INT {
/** */
class NullHandler : public Oscl::Interrupt::StatusHandlerApi {
	private:
		/** */
		NullHandler() noexcept;
	private:	// Oscl::Interrupt::StatusHandlerApi
		/** */
		bool	interrupt() noexcept;
	public:
		/** Singleton operation */
		static Oscl::Interrupt::StatusHandlerApi*	handler;
		/** Singleton operation */
		static Oscl::Interrupt::StatusHandlerApi&	getNull() noexcept;
	};
}
}
}
}

using namespace Oscl::TI::TNETV105X::SYS_INT;

Oscl::Interrupt::StatusHandlerApi*	NullHandler::handler;

static Oscl::Memory::
AlignedBlock<sizeof(Oscl::TI::TNETV105X::SYS_INT::NullHandler)>	nullHandlerMem;

Oscl::Interrupt::StatusHandlerApi&	NullHandler::getNull() noexcept{
	if(!NullHandler::handler){
		NullHandler::handler	= new(&nullHandlerMem)
									NullHandler();
		}
	return *handler;
	}

NullHandler::NullHandler() noexcept{
	}

bool NullHandler::interrupt() noexcept{
	Oscl::ErrorFatal::logAndExit("Interrupt: unreserved vector used.\n");
	return false;
	}

Interrupt::Interrupt(Oscl::TI::TNETV105X::SYS_INT::IrqDrvApi&	irqDrvApi ) noexcept:
		_isr(&NullHandler::getNull()),
		_irqDrvApi(irqDrvApi)
		{
	}

Interrupt::Interrupt(	Oscl::TI::TNETV105X::SYS_INT::IrqDrvApi&	irqDrvApi,
						Oscl::Interrupt::StatusHandlerApi&			isr
						) noexcept:
		_isr(&isr),
		_irqDrvApi(irqDrvApi)
		{
	}

void	Interrupt::reserve(	Oscl::Interrupt::
							StatusHandlerApi&	handler
							) noexcept{
//	Oscl::Error::Info::log("Interrupt::reserve : handler : ");
//	Oscl::Error::Info::hexDump((unsigned long)&handler);
//	Oscl::Error::Info::log("\n\r");
	if(_isr != &NullHandler::getNull()){
		Oscl::ErrorFatal::logAndExit("Interrupt: vector already reserved.\n");
		}
	_isr	= &handler;
	}

void	Interrupt::release() noexcept{
	_isr	= &NullHandler::getNull();
	}

bool	Interrupt::interrupt() noexcept{
	bool	result	= _isr->interrupt();
	if(result){
		_irqDrvApi.clear();
		}
	return result;
	}

