/*
   Copyright (C) 2006 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_driver_national_uart_pc16550_apih_
#define _oscl_driver_national_uart_pc16550_apih_

#include "oscl/hw/national/uart/pc16550.h"

/** */
namespace Oscl {
/** */
namespace National {
/** */
namespace UART {
/** */
namespace PC16550 {

/** This interface provides a low level abstraction of
	to the PC16550 UART functionality.
 */
class Api {
	public:
		/** Be happy GCC */
		virtual ~Api(){}

	public: // Auto CTS/RTS Mode
		/** */
		virtual void	autoCtsAndAutoRts() noexcept=0;
		/** */
		virtual void	autoCtsOnly() noexcept=0;
		/** */
		virtual void	disableAutoCtsAndRts() noexcept=0;

	public: // Auto CTS/RTS Mode

		/** Force DTR LOW (typically DTR assert) */
		virtual void	forceDtrLOW() noexcept=0;

		/** Force DTR HIGH (typically DTR negate) */
		virtual void	forceDtrHIGH() noexcept=0;

		/** Force RTS LOW (typically RTS assert.)
			Do not use this operation when using one
			of the auto CTS/RTS modes.
		 */
		virtual void	forceRtsLOW() noexcept=0;

		/** Force RTS HIGH (typically RTS negate)
			Do not use this operation when using one
			of the auto CTS/RTS modes.
		 */
		virtual void	forceRtsHIGH() noexcept=0;

		/** */
		virtual void	enableExternalSerialChannelInterrupt() noexcept=0;

		/** */
		virtual void	disableExternalSerialChannelInterrupt() noexcept=0;

		/** */
		virtual void	enableLoopback() noexcept=0;

		/** */
		virtual void	disableLoopback() noexcept=0;

		/** This operation loads the baud rate divisor. LSR[DLAB] is
			manipulated as required, and the restored to its previous
			value.
		 */
		virtual void	setBaudDivisor(unsigned divisor) noexcept=0;

	public: // LSR related operations
		/** This operation reads the LSR and caches it for use with
			subsequent LSR status operations. The reason that this
			is done is because reading the LSR has side effects
			such as the clearing of the OE (overrun error) indication.
		 */
		virtual void	updateLsrCache() noexcept=0;

		/** Returns true when the RDH may be read to retrieve a received
			character.
			This operation is based on the cached value of the LSR, which
			is only changed/updated by the updateLsrCache() operation.
		 */
		virtual bool	rxDataReady() const noexcept=0;

		/** Returns true when the a receiver overrun condition is
			encountered.
			This operation is based on the cached value of the LSR, which
			is only changed/updated by the updateLsrCache() operation.
		 */
		virtual bool	overrunError() const noexcept=0;

		/** Returns true when the character at the top of the RX FIFO
			contained a parity error.
			This operation is based on the cached value of the LSR, which
			is only changed/updated by the updateLsrCache() operation.
		 */
		virtual bool	parityError() const noexcept=0;

		/** Returns true when the character at the top of the RX FIFO
			contained a framing error.
			This operation is based on the cached value of the LSR, which
			is only changed/updated by the updateLsrCache() operation.
		 */
		virtual bool	framingError() const noexcept=0;

		/** Returns true when the character at the top of the RX FIFO
			is the result of a BREAK interrupt.
			This operation is based on the cached value of the LSR, which
			is only changed/updated by the updateLsrCache() operation.
		 */
		virtual bool	breakInterrupt() const noexcept=0;

		/** Returns true when the transmit holding register or FIFO is
			empty.
			This operation is based on the cached value of the LSR, which
			is only changed/updated by the updateLsrCache() operation.
		 */
		virtual bool	txHoldingRegisterEmpty() const noexcept=0;

		/** Returns true when both the transmit shift register and the
			the transmit holding register or FIFO is empty.
			This operation is based on the cached value of the LSR, which
			is only changed/updated by the updateLsrCache() operation.
		 */
		virtual bool	txRegisterEmpty() const noexcept=0;

		/** Returns true when the character at the top of the RX FIFO
			contains a parity error, framing error, or break interrupt
			indicatior.
			This operation is based on the cached value of the LSR, which
			is only changed/updated by the updateLsrCache() operation.
		 */
		virtual bool	fifoError() const noexcept=0;

	public: // MSR related operations
		/** This operation reads the MSR and caches it for use with
			subsequent LSR status operations. The reason that this
			is done is because reading the MSR has side effects
			such as clearing the delta CTS, DSR, and DCD bits.
		 */
		virtual void	updateMsrCache() noexcept=0;

		/** Returns true when the CTS pin has changed state since
			the last call to updateMsrCache().
			This operation is based on the cached value of the MSR, which
			is only changed/updated by the updateLsrCache() operation.
		 */
		virtual bool	ctsChanged() const noexcept=0;

		/** Returns true when the DSR pin has changed state since
			the last call to updateMsrCache().
			This operation is based on the cached value of the MSR, which
			is only changed/updated by the updateLsrCache() operation.
		 */
		virtual bool	dsrChanged() const noexcept=0;

		/** Returns true when the RI pin has changed state from the asserted
			(LOW) to the negated (HIGH) state at least once since the last
			call to updateMsrCache().
			This operation is based on the cached value of the MSR, which
			is only changed/updated by the updateLsrCache() operation.
		 */
		virtual bool	riTrailingEdgeDetected() const noexcept=0;

		/** Returns true when the DCD pin has changed state since
			the last call to updateMsrCache().
			This operation is based on the cached value of the MSR, which
			is only changed/updated by the updateLsrCache() operation.
		 */
		virtual bool	dcdChanged() const noexcept=0;

		/** Returns true when the CTS pin was in the LOW state the
			last time the MSR was sampled by a call to updateMsrCache().
			This operation is based on the cached value of the MSR, which
			is only changed/updated by the updateLsrCache() operation.
		 */
		virtual bool	ctsInputIsLOW() const noexcept=0;

		/** Returns true when the DSR pin was in the LOW state the
			last time the MSR was sampled by a call to updateMsrCache().
			This operation is based on the cached value of the MSR, which
			is only changed/updated by the updateLsrCache() operation.
		 */
		virtual bool	dsrInputIsLOW() const noexcept=0;

		/** Returns true when the RI pin was in the LOW state the
			last time the MSR was sampled by a call to updateMsrCache().
			This operation is based on the cached value of the MSR, which
			is only changed/updated by the updateLsrCache() operation.
		 */
		virtual bool	riInputIsLOW() const noexcept=0;

		/** Returns true when the DCD pin was in the LOW state the
			last time the MSR was sampled by a call to updateMsrCache().
			This operation is based on the cached value of the MSR, which
			is only changed/updated by the updateLsrCache() operation.
		 */
		virtual bool	dcdInputIsLOW() const noexcept=0;

	public: // Data interface
		/*	Reads byte from FIFO or RBR depending on mode. Caller is
			responsible for ensuring that valid data is ready.
		 */
		virtual unsigned char	readReceiveData() noexcept=0;

		/*	Writes byte to FIFO or THR depending on mode. Caller is
			responsible for ensuring that the transmitter is ready
			to accept the byte.
		 */
		virtual void	writeTransmitData(unsigned char data) noexcept=0;
	};

}
}
}
}

#endif
