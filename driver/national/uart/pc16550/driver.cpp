/*
   Copyright (C) 2006 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "driver.h"
#include "oscl/hw/national/uart/pc16550.h"
#include "oscl/cpu/sync.h"

using namespace Oscl::National::UART::PC16550;

Driver::Driver(Oscl::National::UART::PC16550::RegisterApi& regApi) noexcept:
		_regApi(regApi),
		_lsrCache(0),
		_msrCache(0)
		{
	}

void	Driver::autoCtsAndAutoRts() noexcept{
	Oscl::HW::National::UART::PC16550::MCR::Reg	value;
	value	= _regApi.readMCR();
	value	&= ~(		Oscl::HW::National::UART::PC16550::MCR::RTS::FieldMask
					|	Oscl::HW::National::UART::PC16550::MCR::AFE::FieldMask
					);
	value	|= (		Oscl::HW::National::UART::PC16550::MCR::RTS::ValueMask_Assert
					|	Oscl::HW::National::UART::PC16550::MCR::AFE::ValueMask_AutoFlowEnable
					);
	_regApi.writeMCR(value);
	}

void	Driver::autoCtsOnly() noexcept{
	Oscl::HW::National::UART::PC16550::MCR::Reg	value;
	value	= _regApi.readMCR();
	value	&= ~(		Oscl::HW::National::UART::PC16550::MCR::RTS::FieldMask
					|	Oscl::HW::National::UART::PC16550::MCR::AFE::FieldMask
					);
	value	|= (		Oscl::HW::National::UART::PC16550::MCR::RTS::ValueMask_Negate
					|	Oscl::HW::National::UART::PC16550::MCR::AFE::ValueMask_AutoFlowEnable
					);
	_regApi.writeMCR(value);
	}

void	Driver::disableAutoCtsAndRts() noexcept{
	Oscl::HW::National::UART::PC16550::MCR::Reg	value;
	value	= _regApi.readMCR();
	value	&= ~(		Oscl::HW::National::UART::PC16550::MCR::RTS::FieldMask
					|	Oscl::HW::National::UART::PC16550::MCR::AFE::FieldMask
					);
	value	|= (		Oscl::HW::National::UART::PC16550::MCR::RTS::ValueMask_Negate
					|	Oscl::HW::National::UART::PC16550::MCR::AFE::ValueMask_AutoFlowDisable
					);
	_regApi.writeMCR(value);
	}

void	Driver::forceDtrLOW() noexcept{
	Oscl::HW::National::UART::PC16550::MCR::Reg	value;
	value	= _regApi.readMCR();
	value	&= ~Oscl::HW::National::UART::PC16550::MCR::DTR::FieldMask;
	value	|= Oscl::HW::National::UART::PC16550::MCR::DTR::ValueMask_Assert;
	_regApi.writeMCR(value);
	}

void	Driver::forceDtrHIGH() noexcept{
	Oscl::HW::National::UART::PC16550::MCR::Reg	value;
	value	= _regApi.readMCR();
	value	&= ~Oscl::HW::National::UART::PC16550::MCR::DTR::FieldMask;
	value	|= Oscl::HW::National::UART::PC16550::MCR::DTR::ValueMask_Negate;
	_regApi.writeMCR(value);
	}

void	Driver::forceRtsLOW() noexcept{
	Oscl::HW::National::UART::PC16550::MCR::Reg	value;
	value	= _regApi.readMCR();
	value	&= ~Oscl::HW::National::UART::PC16550::MCR::RTS::FieldMask;
	value	|= Oscl::HW::National::UART::PC16550::MCR::RTS::ValueMask_Assert;
	_regApi.writeMCR(value);
	}

void	Driver::forceRtsHIGH() noexcept{
	Oscl::HW::National::UART::PC16550::MCR::Reg	value;
	value	= _regApi.readMCR();
	value	&= ~Oscl::HW::National::UART::PC16550::MCR::RTS::FieldMask;
	value	|= Oscl::HW::National::UART::PC16550::MCR::RTS::ValueMask_Negate;
	_regApi.writeMCR(value);
	}

void	Driver::enableExternalSerialChannelInterrupt() noexcept{
	Oscl::HW::National::UART::PC16550::MCR::Reg	value;
	value	= _regApi.readMCR();
	value	&= ~Oscl::HW::National::UART::PC16550::MCR::OUT2::FieldMask;
	value	|= Oscl::HW::National::UART::PC16550::MCR::OUT2::ValueMask_NotMasked;
	_regApi.writeMCR(value);
	}

void	Driver::disableExternalSerialChannelInterrupt() noexcept{
	Oscl::HW::National::UART::PC16550::MCR::Reg	value;
	value	= _regApi.readMCR();
	value	&= ~Oscl::HW::National::UART::PC16550::MCR::OUT2::FieldMask;
	value	|= Oscl::HW::National::UART::PC16550::MCR::OUT2::ValueMask_Masked;
	_regApi.writeMCR(value);
	}

void	Driver::enableLoopback() noexcept{
	Oscl::HW::National::UART::PC16550::MCR::Reg	value;
	value	= _regApi.readMCR();
	value	&= ~Oscl::HW::National::UART::PC16550::MCR::LOOP::FieldMask;
	value	|= Oscl::HW::National::UART::PC16550::MCR::LOOP::ValueMask_TestMode;
	_regApi.writeMCR(value);
	}

void	Driver::disableLoopback() noexcept{
	Oscl::HW::National::UART::PC16550::MCR::Reg	value;
	value	= _regApi.readMCR();
	value	&= ~Oscl::HW::National::UART::PC16550::MCR::LOOP::FieldMask;
	value	|= Oscl::HW::National::UART::PC16550::MCR::LOOP::ValueMask_Normal;
	_regApi.writeMCR(value);
	}

void	Driver::setBaudDivisor(unsigned divisor) noexcept{
	Oscl::HW::National::UART::PC16550::LCR::Reg	savedLCR;
	Oscl::HW::National::UART::PC16550::LCR::Reg	value;
	savedLCR	= _regApi.readLCR();
	value		= savedLCR;
	value		&= ~Oscl::HW::National::UART::PC16550::LCR::DLAB::FieldMask;
	value		|= Oscl::HW::National::UART::PC16550::LCR::DLAB::ValueMask_DivisorLatch;
	_regApi.writeLCR(value);
	OsclCpuInOrderMemoryAccessBarrier();
	_regApi.writeDLL((Oscl::HW::National::UART::PC16550::DLL::Reg)(divisor & 0x00FF));
	_regApi.writeDLH((Oscl::HW::National::UART::PC16550::DLH::Reg)((divisor>>8) & 0x00FF));
	OsclCpuInOrderMemoryAccessBarrier();
	_regApi.writeLCR(savedLCR);
	}

void	Driver::updateLsrCache() noexcept{
	_lsrCache	= _regApi.readLSR();
	}

bool	Driver::rxDataReady() const noexcept{
	return 		(_lsrCache & Oscl::HW::National::UART::PC16550::LSR::DR::FieldMask)
			==	Oscl::HW::National::UART::PC16550::LSR::DR::ValueMask_DataAvailableInRbrOrFifo
			;
	}

bool	Driver::overrunError() const noexcept{
	return 		(_lsrCache & Oscl::HW::National::UART::PC16550::LSR::OE::FieldMask)
			==	Oscl::HW::National::UART::PC16550::LSR::OE::ValueMask_OverrunError
			;
	}

bool	Driver::parityError() const noexcept{
	return 		(_lsrCache & Oscl::HW::National::UART::PC16550::LSR::PE::FieldMask)
			==	Oscl::HW::National::UART::PC16550::LSR::PE::ValueMask_ParityError
			;
	}

bool	Driver::framingError() const noexcept{
	return 		(_lsrCache & Oscl::HW::National::UART::PC16550::LSR::FE::FieldMask)
			==	Oscl::HW::National::UART::PC16550::LSR::FE::ValueMask_FramingError
			;
	}

bool	Driver::breakInterrupt() const noexcept{
	return 		(_lsrCache & Oscl::HW::National::UART::PC16550::LSR::BI::FieldMask)
			==	Oscl::HW::National::UART::PC16550::LSR::BI::ValueMask_BreakReceived
			;
	}

bool	Driver::txHoldingRegisterEmpty() const noexcept{
	return 		(_lsrCache & Oscl::HW::National::UART::PC16550::LSR::TDRQ::FieldMask)
			==	Oscl::HW::National::UART::PC16550::LSR::TDRQ::ValueMask_TxReady
			;
	}

bool	Driver::txRegisterEmpty() const noexcept{
	return 		(_lsrCache & Oscl::HW::National::UART::PC16550::LSR::TEMT::FieldMask)
			==	Oscl::HW::National::UART::PC16550::LSR::TEMT::ValueMask_HoldAndShiftRegEmpty
			;
	}

bool	Driver::fifoError() const noexcept{
	return 		(_lsrCache & Oscl::HW::National::UART::PC16550::LSR::FIFOE::FieldMask)
			==	Oscl::HW::National::UART::PC16550::LSR::FIFOE::ValueMask_AtLeastOneCharacterInRxFifoHasErrors
			;
	}

void	Driver::updateMsrCache() noexcept{
	_msrCache	= _regApi.readMSR();
	}

bool	Driver::ctsChanged() const noexcept{
	return 		(_msrCache & Oscl::HW::National::UART::PC16550::MSR::DCTS::FieldMask)
			==	Oscl::HW::National::UART::PC16550::MSR::DCTS::ValueMask_ChangedState
			;
	}

bool	Driver::dsrChanged() const noexcept{
	return 		(_msrCache & Oscl::HW::National::UART::PC16550::MSR::DDSR::FieldMask)
			==	Oscl::HW::National::UART::PC16550::MSR::DDSR::ValueMask_ChangedState
			;
	}

bool	Driver::riTrailingEdgeDetected() const noexcept{
	return 		(_msrCache & Oscl::HW::National::UART::PC16550::MSR::TERI::FieldMask)
			==	Oscl::HW::National::UART::PC16550::MSR::TERI::ValueMask_ChangedFromAssertToNegate
			;
	}

bool	Driver::dcdChanged() const noexcept{
	return 		(_msrCache & Oscl::HW::National::UART::PC16550::MSR::DDCD::FieldMask)
			==	Oscl::HW::National::UART::PC16550::MSR::DDCD::ValueMask_ChangedState
			;
	}

bool	Driver::ctsInputIsLOW() const noexcept{
	return 		(_msrCache & Oscl::HW::National::UART::PC16550::MSR::CTS::FieldMask)
			==	Oscl::HW::National::UART::PC16550::MSR::CTS::ValueMask_Asserted
			;
	}

bool	Driver::dsrInputIsLOW() const noexcept{
	return 		(_msrCache & Oscl::HW::National::UART::PC16550::MSR::DSR::FieldMask)
			==	Oscl::HW::National::UART::PC16550::MSR::DSR::ValueMask_Asserted
			;
	}

bool	Driver::riInputIsLOW() const noexcept{
	return 		(_msrCache & Oscl::HW::National::UART::PC16550::MSR::RI::FieldMask)
			==	Oscl::HW::National::UART::PC16550::MSR::RI::ValueMask_Asserted
			;
	}

bool	Driver::dcdInputIsLOW() const noexcept{
	return 		(_msrCache & Oscl::HW::National::UART::PC16550::MSR::DCD::FieldMask)
			==	Oscl::HW::National::UART::PC16550::MSR::DCD::ValueMask_Asserted
			;
	}


unsigned char	Driver::readReceiveData() noexcept{
	return _regApi.readRBR();
	}

void	Driver::writeTransmitData(unsigned char data) noexcept{
	_regApi.writeTHR(data);
	}

