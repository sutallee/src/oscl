/*
   Copyright (C) 2006 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <new>
#include "driver.h"
#include "oscl/error/fatal.h"
#include "oscl/cpu/sync.h"
#include "oscl/mt/mutex/iss.h"

#include "oscl/error/info.h"

using namespace Oscl::National::UART::PC16550::Serial::Stream;

Driver::Driver(	Oscl::National::UART::PC16550::RegisterApi&	regApi,
				Oscl::National::UART::PC16550::Api&			driverApi,
				Oscl::Mt::Itc::Delay::Req::Api::SAP&		delaySAP
				) noexcept:
		CloseSync(*this,*this),
		_regApi(regApi),
		_pc16550DriverApi(driverApi),
		_delaySAP(delaySAP),
		_interrupt(regApi,*this),
		_syncRx(*this,*this),
		_pendingRx(),
		_currentRxReq(0),
		_ringHeader(),
		_ringDesc(_ringHeader,sizeof(_ringBuffer)),
		_ringInput(_ringDesc),
		_ringOutput(_ringDesc),
		_syncTx(*this,*this),
		_pendingTx(),
		_pendingFlush(),
		_currentTxReq(0),
		_remaining(0),
		_offsetTx(0)
		{
	}

void Driver::initialize() noexcept{
	OsclCpuInOrderMemoryAccessBarrier();
	}

Oscl::Interrupt::StatusHandlerApi&	Driver::getISR() noexcept{
	return _interrupt;
	}

Oscl::Interrupt::IsrDsrApi&	Driver::getIsrDsr() noexcept{
	return _interrupt;
	}

Oscl::Mt::Runnable&	Driver::getRunnable() noexcept{
	return *this;
	}

Oscl::Stream::Output::Req::Api::SAP&	Driver::getTxDriverSAP() noexcept{
	return _syncTx.getSAP();
	}

Oscl::Stream::Output::Api&	Driver::getTxSyncApi() noexcept{
	return _syncTx;
	}

Oscl::Stream::Input::Req::Api::SAP&	Driver::getRxDriverSAP() noexcept{
	return _syncRx.getSAP();
	}

Oscl::Stream::Input::Api&	Driver::getRxSyncApi() noexcept{
	return _syncRx;
	}

void	Driver::mboxSignaled() noexcept{
	bool	moreToTransmit = true;
	for(;;){
		_pc16550DriverApi.updateLsrCache();
		if(_pc16550DriverApi.rxDataReady()){
			unsigned char	c	= _pc16550DriverApi.readReceiveData();
			if(_pc16550DriverApi.fifoError()){
				if(_pc16550DriverApi.overrunError()){
					Oscl::Error::Info::log("overrunError\n\r");
					}
				if(_pc16550DriverApi.parityError()){
					Oscl::Error::Info::log("parityError\n\r");
					}
				if(_pc16550DriverApi.framingError()){
					Oscl::Error::Info::log("framingError\n\r");
					}
				}
			else {
				uint32_t	index;
				if(_ringInput.alloc(index)){
					_ringBuffer[index]	= c;
					_ringInput.commit();
					receiveCharacters();
					}
				}
			continue;
			}
		if(moreToTransmit){
			if(_pc16550DriverApi.txHoldingRegisterEmpty()){
				moreToTransmit	= transmitCharacter();
				continue;
				}
			}
		break;
		}
	if(_currentRxReq){
		if(_currentRxReq->_payload._buffer.length()){
			_currentRxReq->returnToSender();
			_currentRxReq	= 0;
			}
		}

	if(!_currentTxReq){
		emptyFlushQueue();
		if(_pc16550DriverApi.breakInterrupt()){
			startBreakDelay();
			}
		else {
			enableRxInterrupt();
			}
		}
	else {
		if(_pc16550DriverApi.breakInterrupt()){
			startBreakDelay();
			enableTxInterrupt();
			}
		else {
			enableRxAndTxInterrupt();
			}
		}
	}

void    Driver::request(CloseReq& msg) noexcept{
	disableAllInterrupts();
	msg.returnToSender();
	}

void    Driver::request(OpenReq& msg) noexcept{
	initialize();
	enableRxInterrupt();
	msg.returnToSender();
	}

void	Driver::request(Oscl::Stream::Output::Req::Api::WriteReq& msg) noexcept{
	_pendingTx.put(&msg);
	enableRxAndTxInterrupt();
	}

void	Driver::request(Oscl::Stream::Output::Req::Api::FlushReq& msg) noexcept{
	if(_currentTxReq || _pendingTx.first()){
		_pendingFlush.put(&msg);
		}
	else {
		msg.returnToSender();
		}
	}

bool	Driver::transmitCharacter() noexcept{
	if(!_currentTxReq){
		while(!_currentTxReq){
			_currentTxReq	= _pendingTx.get();
			if(!_currentTxReq){
				return false;
				}
			_remaining	= _currentTxReq->_payload._buffer.length();
			_offsetTx		= 0;
			if(_remaining){
				break;
				}
			_currentTxReq->returnToSender();
			_currentTxReq	= 0;
			}
		}

	_pc16550DriverApi.writeTransmitData(_currentTxReq->_payload._buffer.copyOut(_offsetTx));
	--_remaining;
	++_offsetTx;

	if(!_remaining){
		_currentTxReq->returnToSender();
		_currentTxReq	= 0;
		while(!_currentTxReq){
			_currentTxReq	= _pendingTx.get();
			if(!_currentTxReq){
				return false;
				}
			_remaining	= _currentTxReq->_payload._buffer.length();
			_offsetTx		= 0;
			if(_remaining){
				break;
				}
			_currentTxReq->returnToSender();
			_currentTxReq	= 0;
			}
		}
	return true;
	}

void	Driver::request(Oscl::Stream::Input::Req::Api::ReadReq& msg) noexcept{
	_pendingRx.put(&msg);
	receiveCharacters();
	if(_currentRxReq){
		if(_currentRxReq->_payload._buffer.length()){
			_currentRxReq->returnToSender();
			_currentRxReq	= 0;
			}
		}
	}

void	Driver::request(Oscl::Stream::Input::Req::Api::CancelReq& msg) noexcept{
	Oscl::Stream::Input::Req::Api::ReadReq*
	readToCancel	= _pendingRx.remove(&msg._payload._readToCancel);
	if(readToCancel){
		readToCancel->returnToSender();
		}
	else if(&msg._payload._readToCancel == _currentRxReq){
		_currentRxReq->returnToSender();
		_currentRxReq	= 0;
		}
	msg.returnToSender();
	}

void	Driver::response(Oscl::Mt::Itc::Delay::Resp::Api::DelayResp& msg) noexcept{
	enableRxInterrupt();
	_delayResp	= 0;
	}

void	Driver::response(Oscl::Mt::Itc::Delay::Resp::Api::CancelResp& msg) noexcept{
	// not used.
	}

void	Driver::receiveCharacters() noexcept{
	uint32_t	index;
	for(;;){
		if(!_currentRxReq){
			while(!_currentRxReq){
				_currentRxReq	= _pendingRx.get();
				if(!_currentRxReq){
					return;
					}
				if(_currentRxReq->_payload._buffer.remaining()){
					break;
					}
				_currentRxReq->returnToSender();
				_currentRxReq	= 0;
				}
			}

		// At this point we have a buffer
		if(!_ringOutput.alloc(index)){
			return;
			}

		// We have stuff in the FIFO
		_currentRxReq->getPayload()._buffer	+= _ringBuffer[index];
		_ringOutput.commit();

		if(!_currentRxReq->_payload._buffer.remaining()){
			_currentRxReq->returnToSender();
			_currentRxReq	= 0;
			}
		}
	}

void	Driver::enableRxInterrupt() noexcept{
	Oscl::Mt::IntScopeSync	iss;
	Oscl::HW::National::UART::PC16550::IER::Reg	ier;
	ier	= _regApi.readIER();
	ier	&=	~Oscl::HW::National::UART::PC16550::IER::RAVIE::FieldMask;
	ier	|=	Oscl::HW::National::UART::PC16550::IER::RAVIE::ValueMask_Enable;
	_regApi.writeIER(ier);
	}

void	Driver::enableTxInterrupt() noexcept{
	Oscl::Mt::IntScopeSync	iss;
	Oscl::HW::National::UART::PC16550::IER::Reg	ier;
	ier	= _regApi.readIER();
	ier	&=	~Oscl::HW::National::UART::PC16550::IER::TIE::FieldMask;
	ier	|=	Oscl::HW::National::UART::PC16550::IER::TIE::ValueMask_Enable;
	_regApi.writeIER(ier);
	}

void	Driver::enableRxAndTxInterrupt() noexcept{
	Oscl::Mt::IntScopeSync	iss;
	Oscl::HW::National::UART::PC16550::IER::Reg	ier;
	ier	= _regApi.readIER();
	ier	&=	~(		Oscl::HW::National::UART::PC16550::IER::TIE::FieldMask
				|	Oscl::HW::National::UART::PC16550::IER::RAVIE::FieldMask
				);
	ier	|=	(		Oscl::HW::National::UART::PC16550::IER::TIE::ValueMask_Enable
				|	Oscl::HW::National::UART::PC16550::IER::RAVIE::ValueMask_Enable
				);
	_regApi.writeIER(ier);
	}

void	Driver::disableAllInterrupts() noexcept{
	Oscl::Mt::IntScopeSync	iss;
	Oscl::HW::National::UART::PC16550::IER::Reg	ier;
	ier	= _regApi.readIER();
	ier	&=	~(		Oscl::HW::National::UART::PC16550::IER::TIE::FieldMask
				|	Oscl::HW::National::UART::PC16550::IER::RAVIE::FieldMask
				);
	ier	|=	(		Oscl::HW::National::UART::PC16550::IER::TIE::ValueMask_Disable
				|	Oscl::HW::National::UART::PC16550::IER::RAVIE::ValueMask_Disable
				);
	_regApi.writeIER(ier);
	}

void	Driver::emptyFlushQueue() noexcept{
	Oscl::Stream::Output::Req::Api::FlushReq*	msg;
	while((msg=_pendingFlush.get())){
		msg->returnToSender();
		}
	}

void	Driver::startBreakDelay() noexcept{
	if(_delayResp) return;
	Oscl::Mt::Itc::Delay::
		Req::Api::DelayPayload*
	payload	= new(&_delayMem.payload)
					Oscl::Mt::Itc::Delay::
					Req::Api::DelayPayload(500);
	_delayResp	= new(&_delayMem.resp)
					Oscl::Mt::Itc::Delay::
					Resp::Api::DelayResp(	_delaySAP.getReqApi(),
											*this,
											*this,
											*payload
											);
	_delaySAP.post(_delayResp->getSrvMsg());
	}

