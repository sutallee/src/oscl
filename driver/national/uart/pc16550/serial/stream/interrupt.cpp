/*
   Copyright (C) 2006 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "interrupt.h"
#include "oscl/bits/bitfield.h"
#include "oscl/mt/mutex/iss.h"

using namespace Oscl::National::UART::PC16550::Serial::Stream;

Interrupt::Interrupt(	Oscl::National::UART::PC16550::RegisterApi&		regApi,
						Oscl::Mt::Sema::SignalApi&						driverSema
						) noexcept:
		_regApi(regApi),
		_driverSema(driverSema)
		{
	}

bool	Interrupt::interrupt() noexcept{
	if(isr()){
		dsr();
		return true;
		}
	return false;
	}

bool	Interrupt::isr() noexcept{
	Oscl::HW::National::UART::PC16550::IER::Reg
	ier	= _regApi.readIER();
	ier	&=	~(		Oscl::HW::National::UART::PC16550::IER::DMAE::FieldMask
				|	Oscl::HW::National::UART::PC16550::IER::UUE::FieldMask
				|	Oscl::HW::National::UART::PC16550::IER::NRZE::FieldMask
				|	Oscl::HW::National::UART::PC16550::IER::RTOIE::FieldMask
				|	Oscl::HW::National::UART::PC16550::IER::RIE::FieldMask
				|	Oscl::HW::National::UART::PC16550::IER::RLSE::FieldMask
				|	Oscl::HW::National::UART::PC16550::IER::TIE::FieldMask
				|	Oscl::HW::National::UART::PC16550::IER::RAVIE::FieldMask
				);
	ier	|=	(		Oscl::HW::National::UART::PC16550::IER::DMAE::ValueMask_Disable
				|	Oscl::HW::National::UART::PC16550::IER::UUE::ValueMask_Disable
				|	Oscl::HW::National::UART::PC16550::IER::NRZE::ValueMask_Disable
				|	Oscl::HW::National::UART::PC16550::IER::RTOIE::ValueMask_Disable
				|	Oscl::HW::National::UART::PC16550::IER::RIE::ValueMask_Disable
				|	Oscl::HW::National::UART::PC16550::IER::RLSE::ValueMask_Disable
				|	Oscl::HW::National::UART::PC16550::IER::TIE::ValueMask_Disable
				|	Oscl::HW::National::UART::PC16550::IER::RAVIE::ValueMask_Disable
				);
	_regApi.writeIER(ier);
	return true;
	}

void	Interrupt::dsr() noexcept{
	_driverSema.suSignal();
	}

