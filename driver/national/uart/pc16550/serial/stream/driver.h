/*
   Copyright (C) 2006 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_national_uart_pc16550_serial_stream_driverh_
#define _oscl_drv_national_uart_pc16550_serial_stream_driverh_

#include "oscl/mt/itc/srv/close.h"
#include "oscl/mt/itc/mbox/server.h"
#include "oscl/mt/itc/srv/closeapi.h"
#include "oscl/mt/itc/srv/close.h"
#include "oscl/stream/input/itc/reqapi.h"
#include "oscl/stream/input/itc/sync.h"
#include "oscl/stream/output/itc/sync.h"
#include "oscl/stream/output/itc/reqapi.h"
#include "oscl/driver/national/uart/pc16550/regapi.h"
#include "oscl/driver/national/uart/pc16550/api.h"
#include "interrupt.h"
#include "oscl/buffer/ring/ring.h"
#include "oscl/mt/itc/delay/respmem.h"

/** */
namespace Oscl {
/** */
namespace National {
/** */
namespace UART {
/** */
namespace PC16550 {
/** */
namespace Serial {
/** */
namespace Stream {

/** */
class Driver :
	public Oscl::Mt::Itc::Server,
	public Oscl::Mt::Itc::Srv::CloseSync,
	private Oscl::Mt::Itc::Srv::Close::Req::Api,
	private Oscl::Stream::Output::Req::Api,
	private Oscl::Stream::Input::Req::Api,
	private Oscl::Mt::Itc::Delay::Resp::Api
	{
	private:
		/** */
		Oscl::National::UART::PC16550::RegisterApi&			_regApi;
		/** */
		Oscl::National::UART::PC16550::Api&					_pc16550DriverApi;
		/** */
		Oscl::Mt::Itc::Delay::Req::Api::SAP&				_delaySAP;
		/** */
		Interrupt											_interrupt;

	private:	// RX
		/** */
		Oscl::Stream::Input::Sync								_syncRx;
		/** */
		Oscl::Queue<Oscl::Stream::Input::Req::Api::ReadReq>		_pendingRx;
		/** */
		Oscl::Stream::Input::Req::Api::ReadReq*					_currentRxReq;
		/** */
		unsigned char											_ringBuffer[32];
		/** */
		Oscl::Buffer::Ring::Header								_ringHeader;
		/** */
		Oscl::Buffer::Ring::BigEndianDesc						_ringDesc;
		/** */
		Oscl::Buffer::Ring::TxDesc								_ringInput;
		/** */
		Oscl::Buffer::Ring::RxDesc								_ringOutput;
		/** */
		Oscl::Mt::Itc::Delay::Resp::DelayMem					_delayMem;
		/** */
		Oscl::Mt::Itc::Delay::Resp::Api::DelayResp*				_delayResp;
		

	private:	// TX
		/** */
		Oscl::Stream::Output::Sync								_syncTx;
		/** */
		Oscl::Queue<Oscl::Stream::Output::Req::Api::WriteReq>	_pendingTx;
		/** */
		Oscl::Queue<Oscl::Stream::Output::Req::Api::FlushReq>	_pendingFlush;
		/** */
		Oscl::Stream::Output::Req::Api::WriteReq*				_currentTxReq;
		/** */
		unsigned												_remaining;
		/** */
		unsigned												_offsetTx;

	public:
		/** */
		Driver(	Oscl::National::UART::PC16550::RegisterApi&	regApi,
				Oscl::National::UART::PC16550::Api&			driverApi,
				Oscl::Mt::Itc::Delay::Req::Api::SAP&		delaySAP
				) noexcept;
		/** */
		virtual ~Driver(){}
		/** */
		void	initialize() noexcept;
		/** */
		Oscl::Interrupt::StatusHandlerApi&		getISR() noexcept;
		/** */
		Oscl::Interrupt::IsrDsrApi&				getIsrDsr() noexcept;
		/** */
		Oscl::Mt::Runnable&						getRunnable() noexcept;
		/** */
		Oscl::Stream::Output::Req::Api::SAP&	getTxDriverSAP() noexcept;
		/** */
		Oscl::Stream::Output::Api&				getTxSyncApi() noexcept;
		/** */
		Oscl::Stream::Input::Req::Api::SAP&		getRxDriverSAP() noexcept;
		/** */
		Oscl::Stream::Input::Api&				getRxSyncApi() noexcept;

	private:	// Oscl::Mt::Itc::Server
		/** */
		void	mboxSignaled() noexcept;

	private:	// Oscl::Mt::Itc::Srv::Close::Req::Api
		/** */
		void    request(CloseReq& msg) noexcept;

	private:	// Oscl::Mt::Itc::Srv::Open::Req::Api
		/** */
		void    request(OpenReq& msg) noexcept;

	private: // Oscl::Stream::Output::Req::Api
		/** */
		void	request(Oscl::Stream::Output::Req::Api::WriteReq& msg) noexcept;
		/** */
		void	request(Oscl::Stream::Output::Req::Api::FlushReq& msg) noexcept;

	private: // Oscl::Stream::Input::Req::Api
		/** */
		void	request(Oscl::Stream::Input::Req::Api::ReadReq& msg) noexcept;
		/** */
		void	request(Oscl::Stream::Input::Req::Api::CancelReq& msg) noexcept;

	private: // Oscl::Mt::Itc::Delay::Resp::Api
		/** */
		void	response(Oscl::Mt::Itc::Delay::Resp::Api::DelayResp& msg) noexcept;
		/** */
		void	response(Oscl::Mt::Itc::Delay::Resp::Api::CancelResp& msg) noexcept;

	private:
		/** */
		void	enableRxInterrupt() noexcept;
		/** */
		void	enableTxInterrupt() noexcept;
		/** */
		void	enableRxAndTxInterrupt() noexcept;
		/** */
		void	disableAllInterrupts() noexcept;
		/** */
		void	receiveCharacters() noexcept;
		/** Returns true if more to be transmitted */
		bool	transmitCharacter() noexcept;
		/** */
		void	emptyFlushQueue() noexcept;
		/** */
		void	startBreakDelay() noexcept;

	};

}
}
}
}
}
}

#endif
