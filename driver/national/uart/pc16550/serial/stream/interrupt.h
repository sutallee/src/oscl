/*
   Copyright (C) 2006 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_national_uart_pc16550_stream_interrupth__
#define _oscl_drv_national_uart_pc16550_stream_interrupth__
#include "oscl/mt/sema/sigapi.h"
#include "oscl/interrupt/shandler.h"
#include "oscl/interrupt/isrdsr.h"
#include "oscl/driver/national/uart/pc16550/regapi.h"

/** */
namespace Oscl {
/** */
namespace National {
/** */
namespace UART {
/** */
namespace PC16550 {
/** */
namespace Serial {
/** */
namespace Stream {

/** */
class Interrupt :	public Oscl::Interrupt::StatusHandlerApi,
					public Oscl::Interrupt::IsrDsrApi
					{
	private:
		/** */
		Oscl::National::UART::PC16550::RegisterApi&		_regApi;
		/** */
		Oscl::Mt::Sema::SignalApi&						_driverSema;

	public:
		/** */
		Interrupt(	Oscl::National::UART::PC16550::RegisterApi&		regApi,
					Oscl::Mt::Sema::SignalApi&						driverSema
					) noexcept;

	private:	// Oscl::Interrupt::StatusHandlerApi
		/** */
		bool	interrupt() noexcept;

	private:	// Oscl::Interrupt::IsrDsrApi
		/** */
		bool	isr() noexcept;

		/** */
		void	dsr() noexcept;
	};

}
}
}
}
}
}


#endif
