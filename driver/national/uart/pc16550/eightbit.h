/*
   Copyright (C) 2006 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_driver_national_uart_pc16550_eightbith_
#define _oscl_driver_national_uart_pc16550_eightbith_

#include "regapi.h"

/** */
namespace Oscl {
/** */
namespace National {
/** */
namespace UART {
/** */
namespace PC16550 {

/** This implementation of the PC16550 RegisterApi is for systems
	in which access to the UART registers are memory mapped to
	eight bit wide locations.
 */
class EightBit : public RegisterApi {
	private:
		/** */
		const volatile uint8_t&		_rbr;
		/** */
		volatile uint8_t&			_thr;
		/** */
		uint8_t&					_dll;
		/** */
		uint8_t&					_ier;
		/** */
		uint8_t&					_dlh;
		/** */
		const volatile uint8_t&		_iir;
		/** */
		uint8_t&					_fcr;
		/** */
		uint8_t&					_lcr;
		/** */
		uint8_t&					_mcr;
		/** */
		const volatile uint8_t&		_lsr;
		/** */
		const volatile uint8_t&		_msr;
		/** */
		uint8_t&					_spr;

	public:
		/** Even though some registers share memory addresses in most
			or even ALL implementations, we require typically redundant
			addresses to cover the case where the registers are at separate
			address locations.
		 */
		EightBit(	const volatile uint8_t&		rbr,
					volatile uint8_t&			thr,
					uint8_t&					ll,
					uint8_t&					ier,
					uint8_t&					dlh,
					const volatile uint8_t&		iir,
					uint8_t&					fcr,
					uint8_t&					lcr,
					uint8_t&					mcr,
					const volatile uint8_t&		lsr,
					const volatile uint8_t&		msr,
					uint8_t&					spr
					) noexcept;
	public:
		/** Returns value from volatile read-only Receive Buffer Register
		 */
		Oscl::HW::National::UART::PC16550::RBR::Reg	readRBR() volatile noexcept;

		/** Writes value to the volatile write-only Transmit Holding Register
			Note: This operation writes the same address as the
			writeDLL() operation. The actual register written depends upon
			the state of the DLAB bit in the LCR. Both operations are provided
			for code clarity, but neither operation manipulates the DLAB bit
			of the LCR.
		 */
		void	writeTHR(Oscl::HW::National::UART::PC16550::THR::Reg value) noexcept;

		/** Writes value to Divisor Latch Low Register
			Note: This operation writes the same address as the
			writeTHR() operation. The actual register written depends upon
			the state of the DLAB bit in the LCR. Both operations are provided
			for code clarity, but neither operation manipulates the DLAB bit
			of the LCR.
		 */
		void	writeDLL(Oscl::HW::National::UART::PC16550::DLL::Reg value) noexcept;

		/** Returns value from Divisor Latch Low Register
			Note: This operation MUST only be invoked if the DLAB bit in the LCR register
			is set to a ONE.
		 */
		Oscl::HW::National::UART::PC16550::DLL::Reg	readDLL() const noexcept;

		/** Writes value to the Interrupt Enable register.
			Note: This operation writes the same address as the
			writeDLH() operation. The actual register written depends upon
			the state of the DLAB bit in the LCR. Both operations are provided
			for code clarity, but neither operation manipulates the DLAB bit
			of the LCR.
		 */
		void	writeIER(Oscl::HW::National::UART::PC16550::IER::Reg value) noexcept;

		/** Returns value from Interrupt Enable Register
			Note: This operation MUST only be invoked if the DLAB bit in the LCR register
			is set to a ZERO.
		 */
		Oscl::HW::National::UART::PC16550::IER::Reg	readIER() const noexcept;

		/** Writes value to the Divisor Latch High Register
			Note: This operation writes the same address as the
			writeIER() operation. The actual register written depends upon
			the state of the DLAB bit in the LCR. Both operations are provided
			for code clarity, but neither operation manipulates the DLAB bit
			of the LCR.
		 */
		void	writeDLH(Oscl::HW::National::UART::PC16550::DLH::Reg value) noexcept;

		/** Returns value from Divisor Latch High Register
			Note: This operation MUST only be invoked if the DLAB bit in the LCR register
			is set to a ONE.
		 */
		Oscl::HW::National::UART::PC16550::DLH::Reg	readDLH() const noexcept;

		/** Returns value from Interrupt Identification Register.
		 */
		Oscl::HW::National::UART::PC16550::IIR::Reg	readIIR() const volatile noexcept;

		/** Writes value to the write-only FIFO Control Register.
		 */
		void	writeFCR(Oscl::HW::National::UART::PC16550::FCR::Reg value) noexcept;

		/** Writes value to the Line Control Register.
		 */
		void	writeLCR(Oscl::HW::National::UART::PC16550::LCR::Reg value) noexcept;

		/** Returns value from Line Control Register.
		 */
		Oscl::HW::National::UART::PC16550::LCR::Reg	readLCR() const noexcept;

		/** Writes value to the Modem Control Register.
		 */
		void	writeMCR(Oscl::HW::National::UART::PC16550::MCR::Reg value) noexcept;

		/** Returns value from Modem Control Register.
		 */
		Oscl::HW::National::UART::PC16550::MCR::Reg	readMCR() noexcept;

		/** Returns value from read-only Line Status Register.
		 */
		Oscl::HW::National::UART::PC16550::LSR::Reg	readLSR() const volatile noexcept;

		/** Returns value from read-only Modem Status Register.
		 */
		Oscl::HW::National::UART::PC16550::MSR::Reg	readMSR() const volatile noexcept;

		/** Writes value to the Scratch Pad Register.
		 */
		void	writeSPR(Oscl::HW::National::UART::PC16550::SPR::Reg value) noexcept;

		/** Returns value from Scratch Pad Register.
		 */
		Oscl::HW::National::UART::PC16550::SPR::Reg	readSPR() noexcept;
	};

}
}
}
}

#endif
