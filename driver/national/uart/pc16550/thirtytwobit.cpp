/*
   Copyright (C) 2006 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "thirtytwobit.h"

using namespace Oscl::National::UART::PC16550;

ThirtyTwoBit::ThirtyTwoBit(	uint32_t&					rbr,
							uint32_t&					thr,
							uint32_t&					dll,
							uint32_t&					ier,
							uint32_t&					dlh,
							uint32_t&					iir,
							uint32_t&					fcr,
							uint32_t&					lcr,
							uint32_t&					mcr,
							uint32_t&					lsr,
							uint32_t&					msr,
							uint32_t&					spr
							) noexcept:
		_rbr(rbr),
		_thr(thr),
		_dll(dll),
		_ier(ier),
		_dlh(dlh),
		_iir(iir),
		_fcr(fcr),
		_lcr(lcr),
		_mcr(mcr),
		_lsr(lsr),
		_msr(msr),
		_spr(spr)
		{
	}

Oscl::HW::National::UART::PC16550::RBR::Reg	ThirtyTwoBit::readRBR() volatile noexcept{
	return (Oscl::HW::National::UART::PC16550::RBR::Reg)_rbr;
	}

void	ThirtyTwoBit::writeTHR(Oscl::HW::National::UART::PC16550::THR::Reg value) noexcept{
	_thr	= (uint32_t)value;
	}

void	ThirtyTwoBit::writeDLL(Oscl::HW::National::UART::PC16550::DLL::Reg value) noexcept{
	_dll	= (uint32_t)value;
	}

Oscl::HW::National::UART::PC16550::DLL::Reg	ThirtyTwoBit::readDLL() const noexcept{
	return (Oscl::HW::National::UART::PC16550::DLL::Reg)_dll;
	}

void	ThirtyTwoBit::writeIER(Oscl::HW::National::UART::PC16550::IER::Reg value) noexcept{
	_ier	= (uint32_t)value;
	}

Oscl::HW::National::UART::PC16550::IER::Reg	ThirtyTwoBit::readIER() const noexcept{
	return (Oscl::HW::National::UART::PC16550::IER::Reg)_ier;
	}

void	ThirtyTwoBit::writeDLH(Oscl::HW::National::UART::PC16550::DLH::Reg value) noexcept{
	_dlh	= (uint32_t)value;
	}

Oscl::HW::National::UART::PC16550::DLH::Reg	ThirtyTwoBit::readDLH() const noexcept{
	return (Oscl::HW::National::UART::PC16550::DLH::Reg)_dlh;
	}

Oscl::HW::National::UART::PC16550::IIR::Reg	ThirtyTwoBit::readIIR() const volatile noexcept{
	return (Oscl::HW::National::UART::PC16550::IIR::Reg)_iir;
	}

void	ThirtyTwoBit::writeFCR(Oscl::HW::National::UART::PC16550::FCR::Reg value) noexcept{
	_fcr	= (uint32_t)value;
	}

void	ThirtyTwoBit::writeLCR(Oscl::HW::National::UART::PC16550::LCR::Reg value) noexcept{
	_lcr	= (uint32_t)value;
	}

Oscl::HW::National::UART::PC16550::LCR::Reg	ThirtyTwoBit::readLCR() const noexcept{
	return (Oscl::HW::National::UART::PC16550::LCR::Reg)_lcr;
	}

void	ThirtyTwoBit::writeMCR(Oscl::HW::National::UART::PC16550::MCR::Reg value) noexcept{
	_mcr	= (uint32_t)value;
	}

Oscl::HW::National::UART::PC16550::MCR::Reg	ThirtyTwoBit::readMCR() noexcept{
	return (Oscl::HW::National::UART::PC16550::MCR::Reg)_mcr;
	}

Oscl::HW::National::UART::PC16550::LSR::Reg	ThirtyTwoBit::readLSR() const volatile noexcept{
	return (Oscl::HW::National::UART::PC16550::LSR::Reg)_lsr;
	}

Oscl::HW::National::UART::PC16550::MSR::Reg	ThirtyTwoBit::readMSR() const volatile noexcept{
	return (Oscl::HW::National::UART::PC16550::MSR::Reg)_msr;
	}

void	ThirtyTwoBit::writeSPR(Oscl::HW::National::UART::PC16550::SPR::Reg value) noexcept{
	_spr	= (uint32_t)value;
	}

Oscl::HW::National::UART::PC16550::SPR::Reg	ThirtyTwoBit::readSPR() noexcept{
	return (Oscl::HW::National::UART::PC16550::SPR::Reg)_spr;
	}

