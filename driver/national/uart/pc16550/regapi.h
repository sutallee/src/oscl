/*
   Copyright (C) 2006 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_driver_national_uart_pc16550_regapih_
#define _oscl_driver_national_uart_pc16550_regapih_

#include "oscl/hw/national/uart/pc16550.h"

/** */
namespace Oscl {
/** */
namespace National {
/** */
namespace UART {
/** */
namespace PC16550 {

/** Because the PC16550 is sucha a ubiquitous device whose
	registers have been mapped into the I/O / memory space of
	various systems, this interface allows upper layer
	drivers to be written in a way that is independent of these
	mappings.
 */
class RegisterApi {
	public:
		/** Be happy GCC */
		virtual ~RegisterApi(){}

		/** Returns value from volatile read-only Receive Buffer Register
		 */
		virtual Oscl::HW::National::UART::PC16550::RBR::Reg	readRBR() volatile noexcept=0;

		/** Writes value to the volatile write-only Transmit Holding Register
			Note: This operation writes the same address as the
			writeDLL() operation. The actual register written depends upon
			the state of the DLAB bit in the LCR. Both operations are provided
			for code clarity, but neither operation manipulates the DLAB bit
			of the LCR.
		 */
		virtual void	writeTHR(Oscl::HW::National::UART::PC16550::THR::Reg value) noexcept=0;

		/** Writes value to Divisor Latch Low Register
			Note: This operation writes the same address as the
			writeTHR() operation. The actual register written depends upon
			the state of the DLAB bit in the LCR. Both operations are provided
			for code clarity, but neither operation manipulates the DLAB bit
			of the LCR.
		 */
		virtual void	writeDLL(Oscl::HW::National::UART::PC16550::DLL::Reg value) noexcept=0;

		/** Returns value from Divisor Latch Low Register
			Note: This operation MUST only be invoked if the DLAB bit in the LCR register
			is set to a ONE.
		 */
		virtual Oscl::HW::National::UART::PC16550::DLL::Reg	readDLL() const noexcept=0;

		/** Writes value to the Interrupt Enable register.
			Note: This operation writes the same address as the
			writeDLH() operation. The actual register written depends upon
			the state of the DLAB bit in the LCR. Both operations are provided
			for code clarity, but neither operation manipulates the DLAB bit
			of the LCR.
		 */
		virtual void	writeIER(Oscl::HW::National::UART::PC16550::IER::Reg value) noexcept=0;

		/** Returns value from Interrupt Enable Register
			Note: This operation MUST only be invoked if the DLAB bit in the LCR register
			is set to a ZERO.
		 */
		virtual Oscl::HW::National::UART::PC16550::IER::Reg	readIER() const noexcept=0;

		/** Writes value to the Divisor Latch High Register
			Note: This operation writes the same address as the
			writeIER() operation. The actual register written depends upon
			the state of the DLAB bit in the LCR. Both operations are provided
			for code clarity, but neither operation manipulates the DLAB bit
			of the LCR.
		 */
		virtual void	writeDLH(Oscl::HW::National::UART::PC16550::DLH::Reg value) noexcept=0;

		/** Returns value from Divisor Latch High Register
			Note: This operation MUST only be invoked if the DLAB bit in the LCR register
			is set to a ONE.
		 */
		virtual Oscl::HW::National::UART::PC16550::DLH::Reg	readDLH() const noexcept=0;

		/** Returns value from Interrupt Identification Register.
		 */
		virtual Oscl::HW::National::UART::PC16550::IIR::Reg	readIIR() const volatile noexcept=0;

		/** Writes value to the write-only FIFO Control Register.
		 */
		virtual void	writeFCR(Oscl::HW::National::UART::PC16550::FCR::Reg value) noexcept=0;

		/** Writes value to the Line Control Register.
		 */
		virtual void	writeLCR(Oscl::HW::National::UART::PC16550::LCR::Reg value) noexcept=0;

		/** Returns value from Line Control Register.
		 */
		virtual Oscl::HW::National::UART::PC16550::LCR::Reg	readLCR() const noexcept=0;

		/** Writes value to the Modem Control Register.
		 */
		virtual void	writeMCR(Oscl::HW::National::UART::PC16550::MCR::Reg value) noexcept=0;

		/** Returns value from Modem Control Register.
		 */
		virtual Oscl::HW::National::UART::PC16550::MCR::Reg	readMCR() noexcept=0;

		/** Returns value from read-only Line Status Register.
		 */
		virtual Oscl::HW::National::UART::PC16550::LSR::Reg	readLSR() const volatile noexcept=0;

		/** Returns value from read-only Modem Status Register.
		 */
		virtual Oscl::HW::National::UART::PC16550::MSR::Reg	readMSR() const volatile noexcept=0;

		/** Writes value to the Scratch Pad Register.
		 */
		virtual void	writeSPR(Oscl::HW::National::UART::PC16550::SPR::Reg value) noexcept=0;

		/** Returns value from Scratch Pad Register.
		 */
		virtual Oscl::HW::National::UART::PC16550::SPR::Reg	readSPR() noexcept=0;
	};

}
}
}
}

#endif
