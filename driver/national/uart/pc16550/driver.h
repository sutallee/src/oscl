/*
   Copyright (C) 2006 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_driver_national_uart_pc16550_driverh_
#define _oscl_driver_national_uart_pc16550_driverh_

#include "api.h"
#include "regapi.h"

/** */
namespace Oscl {
/** */
namespace National {
/** */
namespace UART {
/** */
namespace PC16550 {

/** Because the PC16550 is sucha a ubiquitous device whose
	registers have been mapped into the I/O / memory space of
	various systems, this interface allows upper layer
	drivers to be written in a way that is independent of these
	mappings.
 */
class Driver : public Api {
	private:
		/** */
		RegisterApi&								_regApi;

		/** */
		Oscl::HW::National::UART::PC16550::LSR::Reg	_lsrCache;

		/** */
		Oscl::HW::National::UART::PC16550::MSR::Reg	_msrCache;

	public:
		/** */
		Driver(RegisterApi& regApi) noexcept;

	public: // Auto CTS/RTS Mode
		/** */
		void	autoCtsAndAutoRts() noexcept;
		/** */
		void	autoCtsOnly() noexcept;
		/** */
		void	disableAutoCtsAndRts() noexcept;

	public: // Auto CTS/RTS Mode

		/** Force DTR LOW (typically DTR assert) */
		void	forceDtrLOW() noexcept;

		/** Force DTR HIGH (typically DTR negate) */
		void	forceDtrHIGH() noexcept;

		/** Force RTS LOW (typically RTS assert.)
			Do not use this operation when using one
			of the auto CTS/RTS modes.
		 */
		void	forceRtsLOW() noexcept;

		/** Force RTS HIGH (typically RTS negate)
			Do not use this operation when using one
			of the auto CTS/RTS modes.
		 */
		void	forceRtsHIGH() noexcept;

		/** */
		void	enableExternalSerialChannelInterrupt() noexcept;

		/** */
		void	disableExternalSerialChannelInterrupt() noexcept;

		/** */
		void	enableLoopback() noexcept;

		/** */
		void	disableLoopback() noexcept;

		/** This operation loads the baud rate divisor. LSR[DLAB] is
			manipulated as required, and the restored to its previous
			value.
		 */
		void	setBaudDivisor(unsigned divisor) noexcept;

	public: // LSR related operations
		/** This operation reads the LSR and caches it for use with
			subsequent LSR status operations. The reason that this
			is done is because reading the LSR has side effects
			such as the clearing of the OE (overrun error) indication.
		 */
		void	updateLsrCache() noexcept;

		/** Returns true when the RDH may be read to retrieve a received
			character.
			This operation is based on the cached value of the LSR, which
			is only changed/updated by the updateLsrCache() operation.
		 */
		bool	rxDataReady() const noexcept;

		/** Returns true when the a receiver overrun condition is
			encountered.
			This operation is based on the cached value of the LSR, which
			is only changed/updated by the updateLsrCache() operation.
		 */
		bool	overrunError() const noexcept;

		/** Returns true when the character at the top of the RX FIFO
			contained a parity error.
			This operation is based on the cached value of the LSR, which
			is only changed/updated by the updateLsrCache() operation.
		 */
		bool	parityError() const noexcept;

		/** Returns true when the character at the top of the RX FIFO
			contained a framing error.
			This operation is based on the cached value of the LSR, which
			is only changed/updated by the updateLsrCache() operation.
		 */
		bool	framingError() const noexcept;

		/** Returns true when the character at the top of the RX FIFO
			is the result of a BREAK interrupt.
			This operation is based on the cached value of the LSR, which
			is only changed/updated by the updateLsrCache() operation.
		 */
		bool	breakInterrupt() const noexcept;

		/** Returns true when the transmit holding register or FIFO is
			empty.
			This operation is based on the cached value of the LSR, which
			is only changed/updated by the updateLsrCache() operation.
		 */
		bool	txHoldingRegisterEmpty() const noexcept;

		/** Returns true when both the transmit shift register and the
			the transmit holding register or FIFO is empty.
			This operation is based on the cached value of the LSR, which
			is only changed/updated by the updateLsrCache() operation.
		 */
		bool	txRegisterEmpty() const noexcept;

		/** Returns true when the character at the top of the RX FIFO
			contains a parity error, framing error, or break interrupt
			indicatior.
			This operation is based on the cached value of the LSR, which
			is only changed/updated by the updateLsrCache() operation.
		 */
		bool	fifoError() const noexcept;

	public: // MSR related operations
		/** This operation reads the MSR and caches it for use with
			subsequent LSR status operations. The reason that this
			is done is because reading the MSR has side effects
			such as clearing the delta CTS, DSR, and DCD bits.
		 */
		void	updateMsrCache() noexcept;

		/** Returns true when the CTS pin has changed state since
			the last call to updateMsrCache().
			This operation is based on the cached value of the MSR, which
			is only changed/updated by the updateLsrCache() operation.
		 */
		bool	ctsChanged() const noexcept;

		/** Returns true when the DSR pin has changed state since
			the last call to updateMsrCache().
			This operation is based on the cached value of the MSR, which
			is only changed/updated by the updateLsrCache() operation.
		 */
		bool	dsrChanged() const noexcept;

		/** Returns true when the RI pin has changed state from the asserted
			(LOW) to the negated (HIGH) state at least once since the last
			call to updateMsrCache().
			This operation is based on the cached value of the MSR, which
			is only changed/updated by the updateLsrCache() operation.
		 */
		bool	riTrailingEdgeDetected() const noexcept;

		/** Returns true when the DCD pin has changed state since
			the last call to updateMsrCache().
			This operation is based on the cached value of the MSR, which
			is only changed/updated by the updateLsrCache() operation.
		 */
		bool	dcdChanged() const noexcept;

		/** Returns true when the CTS pin was in the LOW state the
			last time the MSR was sampled by a call to updateMsrCache().
			This operation is based on the cached value of the MSR, which
			is only changed/updated by the updateLsrCache() operation.
		 */
		bool	ctsInputIsLOW() const noexcept;

		/** Returns true when the DSR pin was in the LOW state the
			last time the MSR was sampled by a call to updateMsrCache().
			This operation is based on the cached value of the MSR, which
			is only changed/updated by the updateLsrCache() operation.
		 */
		bool	dsrInputIsLOW() const noexcept;

		/** Returns true when the RI pin was in the LOW state the
			last time the MSR was sampled by a call to updateMsrCache().
			This operation is based on the cached value of the MSR, which
			is only changed/updated by the updateLsrCache() operation.
		 */
		bool	riInputIsLOW() const noexcept;

		/** Returns true when the DCD pin was in the LOW state the
			last time the MSR was sampled by a call to updateMsrCache().
			This operation is based on the cached value of the MSR, which
			is only changed/updated by the updateLsrCache() operation.
		 */
		bool	dcdInputIsLOW() const noexcept;

	public: // Data interface
		/*	Reads byte from FIFO or RBR depending on mode. Caller is
			responsible for ensuring that valid data is ready.
		 */
		unsigned char	readReceiveData() noexcept;

		/*	Writes byte to FIFO or THR depending on mode. Caller is
			responsible for ensuring that the transmitter is ready
			to accept the byte.
		 */
		void	writeTransmitData(unsigned char data) noexcept;
	};

}
}
}
}

#endif
