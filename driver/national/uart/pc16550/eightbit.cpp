/*
   Copyright (C) 2006 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "eightbit.h"

using namespace Oscl::National::UART::PC16550;

EightBit::EightBit(	const volatile uint8_t&		rbr,
					volatile uint8_t&			thr,
					uint8_t&					dll,
					uint8_t&					ier,
					uint8_t&					dlh,
					const volatile uint8_t&		iir,
					uint8_t&					fcr,
					uint8_t&					lcr,
					uint8_t&					mcr,
					const volatile uint8_t&		lsr,
					const volatile uint8_t&		msr,
					uint8_t&					spr
					) noexcept:
		_rbr(rbr),
		_thr(thr),
		_dll(dll),
		_ier(ier),
		_dlh(dlh),
		_iir(iir),
		_fcr(fcr),
		_lcr(lcr),
		_mcr(mcr),
		_lsr(lsr),
		_msr(msr),
		_spr(spr)
		{
	}

Oscl::HW::National::UART::PC16550::RBR::Reg	EightBit::readRBR() volatile noexcept{
	return (Oscl::HW::National::UART::PC16550::RBR::Reg)_rbr;
	}

void	EightBit::writeTHR(Oscl::HW::National::UART::PC16550::THR::Reg value) noexcept{
	_thr	= (uint8_t)value;
	}

void	EightBit::writeDLL(Oscl::HW::National::UART::PC16550::DLL::Reg value) noexcept{
	_dll	= (uint8_t)value;
	}

Oscl::HW::National::UART::PC16550::DLL::Reg	EightBit::readDLL() const noexcept{
	return (Oscl::HW::National::UART::PC16550::DLL::Reg)_dll;
	}

void	EightBit::writeIER(Oscl::HW::National::UART::PC16550::IER::Reg value) noexcept{
	_ier	= (uint8_t)value;
	}

Oscl::HW::National::UART::PC16550::IER::Reg	EightBit::readIER() const noexcept{
	return (Oscl::HW::National::UART::PC16550::IER::Reg)_ier;
	}

void	EightBit::writeDLH(Oscl::HW::National::UART::PC16550::DLH::Reg value) noexcept{
	_dlh	= (uint8_t)value;
	}

Oscl::HW::National::UART::PC16550::DLH::Reg	EightBit::readDLH() const noexcept{
	return (Oscl::HW::National::UART::PC16550::DLH::Reg)_dlh;
	}

Oscl::HW::National::UART::PC16550::IIR::Reg	EightBit::readIIR() const volatile noexcept{
	return (Oscl::HW::National::UART::PC16550::IIR::Reg)_iir;
	}

void	EightBit::writeFCR(Oscl::HW::National::UART::PC16550::FCR::Reg value) noexcept{
	_fcr	= (uint8_t)value;
	}

void	EightBit::writeLCR(Oscl::HW::National::UART::PC16550::LCR::Reg value) noexcept{
	_lcr	= (uint8_t)value;
	}

Oscl::HW::National::UART::PC16550::LCR::Reg	EightBit::readLCR() const noexcept{
	return (Oscl::HW::National::UART::PC16550::LCR::Reg)_lcr;
	}

void	EightBit::writeMCR(Oscl::HW::National::UART::PC16550::MCR::Reg value) noexcept{
	_mcr	= (uint8_t)value;
	}

Oscl::HW::National::UART::PC16550::MCR::Reg	EightBit::readMCR() noexcept{
	return (Oscl::HW::National::UART::PC16550::MCR::Reg)_mcr;
	}

Oscl::HW::National::UART::PC16550::LSR::Reg	EightBit::readLSR() const volatile noexcept{
	return (Oscl::HW::National::UART::PC16550::LSR::Reg)_lsr;
	}

Oscl::HW::National::UART::PC16550::MSR::Reg	EightBit::readMSR() const volatile noexcept{
	return (Oscl::HW::National::UART::PC16550::MSR::Reg)_msr;
	}

void	EightBit::writeSPR(Oscl::HW::National::UART::PC16550::SPR::Reg value) noexcept{
	_spr	= (uint8_t)value;
	}

Oscl::HW::National::UART::PC16550::SPR::Reg	EightBit::readSPR() noexcept{
	return (Oscl::HW::National::UART::PC16550::SPR::Reg)_spr;
	}

