/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

/*
This file will contain the interface to the abstract PCMCIA Enabler.
These services are specified by the PCMCIA standard, which can be
purchased from "www.pc-card.com".

For X windows, use the command "xview layers.gif" to view the PCMCIA
software architecture.

Enablers are built upon PCMCIA Card Services.

Generic Enablers:
	Generic enablers are capable of configuring a wide range of the
	most common card types (such as modems and network adapters).

Specific Enablers:
	A specific enabler is a program to configure a single type of
	PC Card, and may be provided by the vendor of the PC Card or
	by a third party software company. 

	There is the potential for some problems when both the generic
	enabler and a specific enabler are loaded, and both recognize
	and can configure a particular type of I/O card. Because of an
	oversight in the PCMCIA specification, the first enabler loaded
	will configure the card if it is installed when the machine is
	booted; if the card is inserted after the machine is booted,
	however, the last enabler loaded will configure the card.
	This can create problems if the two enablers have different
	ideas about how the card should be configured, or if the
	application software depends on one enabler or the other to
	configure the card. 

Point Enablers:
	Unlike generic enablers or specific enablers, point enablers
	do not require Socket Services or Card Services to be loaded.
	Instead, they talk directly to the PCMCIA adapter hardware.

*/

