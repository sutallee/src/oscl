/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

/*
This file will contain the interface to the abstract ATA PCMCIA Driver.
These services are specified by the PCMCIA standard, which can be
purchased from "www.pc-card.com".

For X windows, use the command "xview layers.gif" to view the PCMCIA
software architecture.

ATA PCMCIA drivers are built upon PCMCIA Card Services.

	ATA is a sort of nested acronym for AT Attachment (the "AT" refers
	to the IBM AT computer), and is an interface which is electrically
	identical to a common hard disk interface. ATA mass storage devices,
	whether mechanical hard disks or solid-state memory cards which
	appear as disk drives, require another driver to be loaded in the
	system. ATA drivers must be loaded after Socket Services and Card
	Services. 

	Some systems are able to boot off an ATA PCMCIA device. This is
	possible if the manufacturer of the computer includes an ATA device
	driver in the system's BIOS ROM. After the system is booted, this ROM
	based driver can relinquish control of the slot to allow another
	ATA driver to be installed on top of Card Services; this allows
	other devices to be swapped in and out of the slot. 
*/

