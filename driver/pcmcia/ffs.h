/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

/*
This file will contain the interface to the abstract FFS PCMCIA Driver.
These services are specified by the PCMCIA standard, which can be
purchased from "www.pc-card.com".

For X windows, use the command "xview layers.gif" to view the PCMCIA
software architecture.

FSF PCMCIA drivers are built upon PCMCIA Card Services.

	FFS, the obligatory acronym for Flash Filing Systems, refers to
	drivers which handle flash memory cards. Flash memory cards also
	appear to the system as disk drives, but have some peculiarities
	which are handled by an FFS driver. For example, flash memory
	has a limited write cycle, often on the order of 10,000 writes
	or so; after that, the card wears out. Erasing and rewriting
	information on these cards is also a relatively slow procedure.
	The FFS driver performs wear-balancing to avoid wearing out the
	media prematurely, and works to hide performance delays in writing
	to the card. 

	It is interesting to note that the flash memory used on these cards
	is often the same as that used on solid-state ATA devices. The
	difference is that an ATA solid-state disk has intelligence on
	the card to deal with the characteristics of flash memory, while
	FFS flash memory cards require the FFS driver to perform this service.
	Not all Flash Filing Systems are compatible with one another,
	making an ATA device a better choice if you'll be using a memory
	card to transfer data between different systems. ATA solid-state
	disk cards are generally more expensive, however. 

	Like ATA, the FFS is a driver which you should only load if you
	have a card that requires it. An FFS may require another memory
	card driver to be loaded first, such as the one for SRAM cards.
*/

