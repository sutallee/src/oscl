/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

/*
This file will contain the interface to the abstract PCMCIA Card
Services. These services are specified by the PCMCIA standard,
which can be purchased from "www.pc-card.com".

For X windows, use the command "xview layers.gif" to view the PCMCIA
software architecture.

Card services act "... as sort of a librarian of system resources."
Deals with I/O ranges, memory ranges, and interrupts.

Card Services are built upon PCMCIA Socket Services.

	As each PC Card is inserted in the system, Card Services hands
	out these resources as needed to configure the card; when cards
	are removed, these resources are returned to Card Services to reuse.

	This allows any combination of cards to work without conflict,
	as long as you've given Card Services the resources it needs
	to accomodate any two of them (assuming you have two PCMCIA slots). 


From Linux	"include/pcmcia/cs.h" :

int pcmcia_access_configuration_register(client_handle_t handle, conf_reg_t *reg);
int pcmcia_bind_device(bind_req_t *req);
int pcmcia_bind_mtd(mtd_bind_t *req);
int pcmcia_deregister_client(client_handle_t handle);
int pcmcia_get_configuration_info(client_handle_t handle, config_info_t *config);
int pcmcia_get_card_services_info(servinfo_t *info);
int pcmcia_get_first_client(client_handle_t *handle, client_req_t *req);
int pcmcia_get_next_client(client_handle_t *handle, client_req_t *req);

int pcmcia_get_first_window(window_handle_t *win, win_req_t *req);
int pcmcia_get_next_window(window_handle_t *win, win_req_t *req);
int pcmcia_get_status(client_handle_t handle, cs_status_t *status);
int pcmcia_get_mem_page(window_handle_t win, memreq_t *req);
int pcmcia_map_mem_page(window_handle_t win, memreq_t *req);
int pcmcia_modify_configuration(client_handle_t handle, modconf_t *mod);
int pcmcia_modify_window(window_handle_t win, modwin_t *req);
int pcmcia_register_client(client_handle_t *handle, client_reg_t *req);
int pcmcia_release_configuration(client_handle_t handle);
int pcmcia_release_io(client_handle_t handle, io_req_t *req);
int pcmcia_release_irq(client_handle_t handle, irq_req_t *req);
int pcmcia_release_window(window_handle_t win);
int pcmcia_request_configuration(client_handle_t handle, config_req_t *req);
int pcmcia_request_io(client_handle_t handle, io_req_t *req);
int pcmcia_request_irq(client_handle_t handle, irq_req_t *req);
int pcmcia_request_window(client_handle_t *handle, win_req_t *req, window_handle_t *wh);
int pcmcia_reset_card(client_handle_t handle, client_req_t *req);
int pcmcia_suspend_card(client_handle_t handle, client_req_t *req);
int pcmcia_resume_card(client_handle_t handle, client_req_t *req);
int pcmcia_eject_card(client_handle_t handle, client_req_t *req);
int pcmcia_insert_card(client_handle_t handle, client_req_t *req);
int pcmcia_set_event_mask(client_handle_t handle, eventmask_t *mask);
int pcmcia_report_error(client_handle_t handle, error_info_t *err);
struct pci_bus *pcmcia_lookup_bus(client_handle_t handle);
int pcmcia_adjust_resource_info(client_handle_t handle, adjust_t *adj);

*/

namespace Oscl {
namespace Pcmcia {

int bind_device(bind_req_t *req);
int bind_mtd(mtd_bind_t *req);
int get_card_services_info(servinfo_t *info);

class CardService {
	public:
		int request_configuration(	u_int       Attributes
									u_int       Vcc, Vpp1, Vpp2
									u_int       IntType
									u_int       ConfigBase
									u_char      Status, Pin, Copy, ExtStatus
									u_char      ConfigIndex
									u_int       Present
									) noexcept;

		int release_configuration();

	public:
		int readConfigurationRegister(	unsigned char	function,
										unsigned		offset,
										unsigned&		value
										) noexcept;
		int writeConfigurationRegister(	unsigned char	function,
										unsigned		offset,
										unsigned		value
										) noexcept;
/*
Must request_configuration before any operations.
This results in a CONFIG_LOCKED.
Must release_io before release_config
These are the return values from
x		CS_BAD_HANDLE null or uninitialize CardService object
x		CS_BAD_ARGS out of range argument
x		CS_CONFIGURATION_LOCKED	locked
			locked by:
				request_configuration
				request_io
				request_irq
			unlocked by:
				release_configuration
				release_io
		CS_SUCCESS
*/
		
		int deregister_client();
		int get_configuration_info(config_info_t *config);
		int get_status(cs_status_t *status);
		int modify_configuration(modconf_t *mod);
		int release_io(io_req_t *req);
		int release_irq(irq_req_t *req);
		int request_io(io_req_t *req);
		int request_irq(irq_req_t *req);
		int reset_card(client_req_t *req);
		int suspend_card(client_req_t *req);
		int resume_card(client_req_t *req);
		int eject_card(client_req_t *req);
		int insert_card(client_req_t *req);
		int set_event_mask(eventmask_t *mask);
		int report_error(error_info_t *err);
		struct pci_bus *lookup_bus();
		int adjust_resource_info(adjust_t *adj);
	public:
		static int get_first_client(client_handle_t *handle, client_req_t *req);
		static int get_next_client(client_handle_t *handle, client_req_t *req);
		static int register_client(client_handle_t *handle, client_reg_t *req);
	};

int request_window(client_handle_t *handle, win_req_t *req, window_handle_t *wh);

class Window {
	public:
		int get_mem_page(memreq_t *req);
		int map_mem_page(memreq_t *req);
		int modify_window(modwin_t *req);
		int release_window();

	public:
		static int get_first_window(window_handle_t *win, win_req_t *req);
		static int get_next_window(window_handle_t *win, win_req_t *req);
	};

};
};
