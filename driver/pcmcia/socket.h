/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

/*
This file will contain the interface to the abstract PCMCIA Socket
Services. These services are specified by the PCMCIA standard,
which can be purchased from "www.pc-card.com".

For X windows, use the command "xview layers.gif" to view the PCMCIA
software architecture.

Socket Services abstract the PCMCIA socket hardware interface.

From Linux	"include/pcmcia/ss.h" :
struct pccard_operations {
    int (*init)(unsigned int sock);
    int (*suspend)(unsigned int sock);
    int (*register_callback)(unsigned int sock, void (*handler)(void *, unsigned int), void * info);
    int (*inquire_socket)(unsigned int sock, socket_cap_t *cap);
    int (*get_status)(unsigned int sock, u_int *value);
    int (*get_socket)(unsigned int sock, socket_state_t *state);
    int (*set_socket)(unsigned int sock, socket_state_t *state);
    int (*get_io_map)(unsigned int sock, struct pccard_io_map *io);
    int (*set_io_map)(unsigned int sock, struct pccard_io_map *io);
    int (*get_mem_map)(unsigned int sock, struct pccard_mem_map *mem);
    int (*set_mem_map)(unsigned int sock, struct pccard_mem_map *mem);
    void (*proc_setup)(unsigned int sock, struct proc_dir_entry *base);
};

extern int register_ss_entry(int nsock, struct pccard_operations *ops);
extern void unregister_ss_entry(struct pccard_operations *ops);

*/

