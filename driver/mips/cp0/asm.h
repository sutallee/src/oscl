/*
   Copyright (C) 2006 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_driver_mips_cp0_asmh_
#define _oscl_driver_mips_cp0_asmh_
#include "oscl/hw/mips/cp0/reg.h"

/** */
namespace Oscl {

/** */
namespace MIPS {

/** */
namespace CP0 {

inline Oscl::HW::MIPS::CP0::Index::Reg	index() noexcept{
	Oscl::HW::MIPS::CP0::Index::Reg	reg;
    asm volatile(	" ssnop\n"
					" ssnop\n"
					" mfc0 %0, $0, 0"
					: "=r" (reg));
	return reg;
	}

inline Oscl::HW::MIPS::CP0::Random::Reg	random() noexcept{
	Oscl::HW::MIPS::CP0::Random::Reg	reg;
    asm volatile(" mfc0 %0, $1, 0" : "=r" (reg));
	return reg;
	}

inline Oscl::HW::MIPS::CP0::EntryLo::Reg	entryLo0() noexcept{
	Oscl::HW::MIPS::CP0::EntryLo::Reg	reg;
    asm volatile(" mfc0 %0, $2, 0" : "=r" (reg));
	return reg;
	}

inline Oscl::HW::MIPS::CP0::EntryLo::Reg	entryLo1() noexcept{
	Oscl::HW::MIPS::CP0::EntryLo::Reg	reg;
    asm volatile(" mfc0 %0, $3, 0" : "=r" (reg));
	return reg;
	}

inline Oscl::HW::MIPS::CP0::Context::Reg	context() noexcept{
	Oscl::HW::MIPS::CP0::Context::Reg	reg;
    asm volatile(" mfc0 %0, $4, 0" : "=r" (reg));
	return reg;
	}

inline Oscl::HW::MIPS::CP0::PageMask::Reg	pageMask() noexcept{
	Oscl::HW::MIPS::CP0::PageMask::Reg	reg;
    asm volatile(" mfc0 %0, $5, 0" : "=r" (reg));
	return reg;
	}

inline Oscl::HW::MIPS::CP0::Wired::Reg	wired() noexcept{
	Oscl::HW::MIPS::CP0::Wired::Reg	reg;
    asm volatile(" mfc0 %0, $6, 0" : "=r" (reg));
	return reg;
	}

inline Oscl::HW::MIPS::CP0::BadVAddr::Reg	badVAddr() noexcept{
	Oscl::HW::MIPS::CP0::BadVAddr::Reg	reg;
    asm volatile(" mfc0 %0, $8, 0" : "=r" (reg));
	return reg;
	}

inline Oscl::HW::MIPS::CP0::Count::Reg	count() noexcept{
	Oscl::HW::MIPS::CP0::Count::Reg	reg;
    asm volatile(" mfc0 %0, $9, 0" : "=r" (reg));
	return reg;
	}

inline Oscl::HW::MIPS::CP0::EntryHi::Reg	entryHi() noexcept{
	Oscl::HW::MIPS::CP0::EntryHi::Reg	reg;
    asm volatile(" mfc0 %0, $10, 0" : "=r" (reg));
	return reg;
	}

inline Oscl::HW::MIPS::CP0::Compare::Reg	compare() noexcept{
	Oscl::HW::MIPS::CP0::Compare::Reg	reg;
    asm volatile(" mfc0 %0, $11, 0" : "=r" (reg));
	return reg;
	}

inline Oscl::HW::MIPS::CP0::Status::Reg	status() noexcept{
	Oscl::HW::MIPS::CP0::Status::Reg	reg;
    asm volatile(" mfc0 %0, $12, 0" : "=r" (reg));
	return reg;
	}

inline Oscl::HW::MIPS::CP0::SRSCtl::Reg	srsCtl() noexcept{
	Oscl::HW::MIPS::CP0::Status::Reg	reg;
    asm volatile(	" mfc0 %0, $12, 2\n"
					 : "=r" (reg)
					);
	return reg;
	}

inline Oscl::HW::MIPS::CP0::Cause::Reg	cause() noexcept{
	Oscl::HW::MIPS::CP0::Cause::Reg	reg;
    asm volatile(" mfc0 %0, $13, 0" : "=r" (reg));
	return reg;
	}

inline Oscl::HW::MIPS::CP0::EPC::Reg	ePC() noexcept{
	Oscl::HW::MIPS::CP0::EPC::Reg	reg;
    asm volatile(" mfc0 %0, $14, 0" : "=r" (reg));
	return reg;
	}

inline Oscl::HW::MIPS::CP0::PRId::Reg	pRId() noexcept{
	Oscl::HW::MIPS::CP0::PRId::Reg	reg;
    asm volatile(" mfc0 %0, $15, 0" : "=r" (reg));
	return reg;
	}

inline Oscl::HW::MIPS::CP0::Config::Reg	config() noexcept{
	Oscl::HW::MIPS::CP0::Config::Reg	reg;
    asm volatile(" mfc0 %0, $16, 0" : "=r" (reg));
	return reg;
	}

inline Oscl::HW::MIPS::CP0::Config1::Reg	config1() noexcept{
	Oscl::HW::MIPS::CP0::Config1::Reg	reg;
    asm volatile(" mfc0 %0, $16, 1" : "=r" (reg));
	return reg;
	}

inline Oscl::HW::MIPS::CP0::Config2::Reg	config2() noexcept{
	Oscl::HW::MIPS::CP0::Config2::Reg	reg;
    asm volatile(" mfc0 %0, $16, 2" : "=r" (reg));
	return reg;
	}

inline Oscl::HW::MIPS::CP0::Config3::Reg	config3() noexcept{
	Oscl::HW::MIPS::CP0::Config3::Reg	reg;
    asm volatile(" mfc0 %0, $16, 3" : "=r" (reg));
	return reg;
	}

inline Oscl::HW::MIPS::CP0::LLAddr::Reg	lLAddr() noexcept{
	Oscl::HW::MIPS::CP0::LLAddr::Reg	reg;
    asm volatile(" mfc0 %0, $17, 0" : "=r" (reg));
	return reg;
	}

inline Oscl::HW::MIPS::CP0::WatchLo::Reg	watchLo0() noexcept{
	Oscl::HW::MIPS::CP0::WatchLo::Reg	reg;
    asm volatile(" mfc0 %0, $18, 0" : "=r" (reg));
	return reg;
	}

inline Oscl::HW::MIPS::CP0::WatchLo::Reg	watchLo1() noexcept{
	Oscl::HW::MIPS::CP0::WatchLo::Reg	reg;
    asm volatile(" mfc0 %0, $18, 1" : "=r" (reg));
	return reg;
	}

inline Oscl::HW::MIPS::CP0::WatchLo::Reg	watchLo2() noexcept{
	Oscl::HW::MIPS::CP0::WatchLo::Reg	reg;
    asm volatile(" mfc0 %0, $18, 2" : "=r" (reg));
	return reg;
	}

inline Oscl::HW::MIPS::CP0::WatchLo::Reg	watchLo3() noexcept{
	Oscl::HW::MIPS::CP0::WatchLo::Reg	reg;
    asm volatile(" mfc0 %0, $18, 3" : "=r" (reg));
	return reg;
	}

inline Oscl::HW::MIPS::CP0::WatchHi::Reg	watchHi0() noexcept{
	Oscl::HW::MIPS::CP0::WatchHi::Reg	reg;
    asm volatile(" mfc0 %0, $19, 0" : "=r" (reg));
	return reg;
	}

inline Oscl::HW::MIPS::CP0::WatchHi::Reg	watchHi1() noexcept{
	Oscl::HW::MIPS::CP0::WatchHi::Reg	reg;
    asm volatile(" mfc0 %0, $19, 1" : "=r" (reg));
	return reg;
	}

inline Oscl::HW::MIPS::CP0::WatchHi::Reg	watchHi2() noexcept{
	Oscl::HW::MIPS::CP0::WatchHi::Reg	reg;
    asm volatile(" mfc0 %0, $19, 2" : "=r" (reg));
	return reg;
	}

inline Oscl::HW::MIPS::CP0::WatchHi::Reg	watchHi3() noexcept{
	Oscl::HW::MIPS::CP0::WatchHi::Reg	reg;
    asm volatile(" mfc0 %0, $19, 3" : "=r" (reg));
	return reg;
	}

inline Oscl::HW::MIPS::CP0::Debug::Reg	debug() noexcept{
	Oscl::HW::MIPS::CP0::Debug::Reg	reg;
    asm volatile(" mfc0 %0, $23, 0" : "=r" (reg));
	return reg;
	}

inline Oscl::HW::MIPS::CP0::DEPC::Reg	dEPC() noexcept{
	Oscl::HW::MIPS::CP0::DEPC::Reg	reg;
    asm volatile(" mfc0 %0, $24, 0" : "=r" (reg));
	return reg;
	}

inline Oscl::HW::MIPS::CP0::PerfCntCtl::Reg	perfCntCtl0() noexcept{
	Oscl::HW::MIPS::CP0::PerfCntCtl::Reg	reg;
    asm volatile(" mfc0 %0, $25, 0" : "=r" (reg));
	return reg;
	}

inline Oscl::HW::MIPS::CP0::PerfCntCnt::Reg	perfCntCnt0() noexcept{
	Oscl::HW::MIPS::CP0::PerfCntCnt::Reg	reg;
    asm volatile(" mfc0 %0, $25, 1" : "=r" (reg));
	return reg;
	}

inline Oscl::HW::MIPS::CP0::PerfCntCtl::Reg	perfCntCtl1() noexcept{
	Oscl::HW::MIPS::CP0::PerfCntCtl::Reg	reg;
    asm volatile(" mfc0 %0, $26, 2" : "=r" (reg));
	return reg;
	}

inline Oscl::HW::MIPS::CP0::PerfCntCnt::Reg	perfCntCnt1() noexcept{
	Oscl::HW::MIPS::CP0::PerfCntCnt::Reg	reg;
    asm volatile(" mfc0 %0, $25, 3" : "=r" (reg));
	return reg;
	}

inline Oscl::HW::MIPS::CP0::PerfCntCtl::Reg	perfCntCtl2() noexcept{
	Oscl::HW::MIPS::CP0::PerfCntCtl::Reg	reg;
    asm volatile(" mfc0 %0, $25, 4" : "=r" (reg));
	return reg;
	}

inline Oscl::HW::MIPS::CP0::PerfCntCnt::Reg	perfCntCnt2() noexcept{
	Oscl::HW::MIPS::CP0::PerfCntCnt::Reg	reg;
    asm volatile(" mfc0 %0, $25, 5" : "=r" (reg));
	return reg;
	}

inline Oscl::HW::MIPS::CP0::PerfCntCtl::Reg	perfCntCtl3() noexcept{
	Oscl::HW::MIPS::CP0::PerfCntCtl::Reg	reg;
    asm volatile(" mfc0 %0, $25, 6" : "=r" (reg));
	return reg;
	}

inline Oscl::HW::MIPS::CP0::PerfCntCnt::Reg	perfCntCnt3() noexcept{
	Oscl::HW::MIPS::CP0::PerfCntCnt::Reg	reg;
    asm volatile(" mfc0 %0, $25, 7" : "=r" (reg));
	return reg;
	}

inline Oscl::HW::MIPS::CP0::ErrCtl::Reg	errCtl() noexcept{
	Oscl::HW::MIPS::CP0::ErrCtl::Reg	reg;
    asm volatile(" mfc0 %0, $26, 0" : "=r" (reg));
	return reg;
	}

inline Oscl::HW::MIPS::CP0::CacheErr::Reg	cacheErr0() noexcept{
	Oscl::HW::MIPS::CP0::CacheErr::Reg	reg;
    asm volatile(" mfc0 %0, $27, 0" : "=r" (reg));
	return reg;
	}

inline Oscl::HW::MIPS::CP0::CacheErr::Reg	cacheErr1() noexcept{
	Oscl::HW::MIPS::CP0::CacheErr::Reg	reg;
    asm volatile(" mfc0 %0, $27, 1" : "=r" (reg));
	return reg;
	}


inline Oscl::HW::MIPS::CP0::CacheErr::Reg	cacheErr2() noexcept{
	Oscl::HW::MIPS::CP0::CacheErr::Reg	reg;
    asm volatile(" mfc0 %0, $27, 2" : "=r" (reg));
	return reg;
	}

inline Oscl::HW::MIPS::CP0::CacheErr::Reg	cacheErr3() noexcept{
	Oscl::HW::MIPS::CP0::CacheErr::Reg	reg;
    asm volatile(" mfc0 %0, $27, 3" : "=r" (reg));
	return reg;
	}

inline Oscl::HW::MIPS::CP0::TagLo::Reg	tagLo() noexcept{
	Oscl::HW::MIPS::CP0::TagLo::Reg	reg;
    asm volatile(" mfc0 %0, $28, 0" : "=r" (reg));
	return reg;
	}

inline Oscl::HW::MIPS::CP0::DataLo::Reg	dataLo() noexcept{
	Oscl::HW::MIPS::CP0::DataLo::Reg	reg;
    asm volatile(" mfc0 %0, $28, 1" : "=r" (reg));
	return reg;
	}

inline Oscl::HW::MIPS::CP0::TagHi::Reg	tagHi() noexcept{
	Oscl::HW::MIPS::CP0::TagHi::Reg	reg;
    asm volatile(" mfc0 %0, $29, 0" : "=r" (reg));
	return reg;
	}

inline Oscl::HW::MIPS::CP0::DataHi::Reg	dataHi() noexcept{
	Oscl::HW::MIPS::CP0::DataHi::Reg	reg;
    asm volatile(" mfc0 %0, $29, 1" : "=r" (reg));
	return reg;
	}

inline Oscl::HW::MIPS::CP0::ErrorEPC::Reg	errorEPC() noexcept{
	Oscl::HW::MIPS::CP0::ErrorEPC::Reg	reg;
    asm volatile(" mfc0 %0, $30, 0" : "=r" (reg));
	return reg;
	}

inline Oscl::HW::MIPS::CP0::DESAVE::Reg	dESAVE() noexcept{
	Oscl::HW::MIPS::CP0::DESAVE::Reg	reg;
    asm volatile(" mfc0 %0, $31, 0" : "=r" (reg));
	return reg;
	}





inline void	index(Oscl::HW::MIPS::CP0::Index::Reg reg) noexcept{
    asm volatile(" mtc0 %0, $0, 0" : : "r" (reg));
	}

inline void	random(Oscl::HW::MIPS::CP0::Random::Reg reg) noexcept{
    asm volatile(" mtc0 %0, $1, 0" : : "r" (reg));
	}

inline void	entryLo0(Oscl::HW::MIPS::CP0::EntryLo::Reg reg) noexcept{
    asm volatile(" mtc0 %0, $2, 0" : : "r" (reg));
	}

inline void	entryLo1(Oscl::HW::MIPS::CP0::EntryLo::Reg reg) noexcept{
    asm volatile(" mtc0 %0, $3, 0" : : "r" (reg));
	}

inline void	context(Oscl::HW::MIPS::CP0::Context::Reg reg) noexcept{
    asm volatile(" mtc0 %0, $4, 0" : : "r" (reg));
	}

inline void pageMask(Oscl::HW::MIPS::CP0::PageMask::Reg reg) noexcept{
    asm volatile(" mtc0 %0, $5, 0" : : "r" (reg));
	}

inline void wired(Oscl::HW::MIPS::CP0::Wired::Reg reg) noexcept{
    asm volatile(" mtc0 %0, $6, 0" : : "r" (reg));
	}

inline void badVAddr(Oscl::HW::MIPS::CP0::BadVAddr::Reg reg) noexcept{
    asm volatile(" mtc0 %0, $8, 0" : : "r" (reg));
	}

inline void count(Oscl::HW::MIPS::CP0::Count::Reg reg) noexcept{
    asm volatile(" mtc0 %0, $9, 0" : : "r" (reg));
	}

inline void entryHi(Oscl::HW::MIPS::CP0::EntryHi::Reg reg) noexcept{
    asm volatile(" mtc0 %0, $10, 0" : : "r" (reg));
	}

inline void compare(Oscl::HW::MIPS::CP0::Compare::Reg reg) noexcept{
    asm volatile(" mtc0 %0, $11, 0" : : "r" (reg));
	}

inline void status(Oscl::HW::MIPS::CP0::Status::Reg reg) noexcept{
    asm volatile(	" mtc0 %0, $12, 0\n"
					" ssnop\n"
					" ssnop\n"
					" ssnop\n"
					" ssnop\n"
					: : "r" (reg));
	}

inline void srsCtl(Oscl::HW::MIPS::CP0::SRSCtl::Reg reg) noexcept{
    asm volatile(" mtc0 %0, $12, 2" : : "r" (reg));
	}

inline void cause(Oscl::HW::MIPS::CP0::Cause::Reg reg) noexcept{
    asm volatile(" mtc0 %0, $13, 0" : : "r" (reg));
	}

inline void ePC(Oscl::HW::MIPS::CP0::EPC::Reg reg) noexcept{
    asm volatile(	" mtc0 %0, $14, 0\n"
					" ssnop\n"
					" ssnop\n"
					: : "r" (reg));
	}

inline void pRId(Oscl::HW::MIPS::CP0::PRId::Reg reg) noexcept{
    asm volatile(" mtc0 %0, $15, 0" : : "r" (reg));
	}

inline void config(Oscl::HW::MIPS::CP0::Config::Reg reg) noexcept{
    asm volatile(" mtc0 %0, $16, 0" : : "r" (reg));
	}

inline void config1(Oscl::HW::MIPS::CP0::Config1::Reg reg) noexcept{
    asm volatile(" mtc0 %0, $16, 1" : : "r" (reg));
	}

inline void config2(Oscl::HW::MIPS::CP0::Config2::Reg reg) noexcept{
    asm volatile(" mtc0 %0, $16, 2" : : "r" (reg));
	}

inline void config3(Oscl::HW::MIPS::CP0::Config3::Reg reg) noexcept{
    asm volatile(" mtc0 %0, $16, 3" : : "r" (reg));
	}

inline void lLAddr(Oscl::HW::MIPS::CP0::LLAddr::Reg reg) noexcept{
    asm volatile(" mtc0 %0, $17, 0" : : "r" (reg));
	}

inline void watchLo0(Oscl::HW::MIPS::CP0::WatchLo::Reg reg) noexcept{
    asm volatile(" mtc0 %0, $18, 0" : : "r" (reg));
	}

inline void watchLo1(Oscl::HW::MIPS::CP0::WatchLo::Reg reg) noexcept{
    asm volatile(" mtc0 %0, $18, 1" : : "r" (reg));
	}

inline void watchLo2(Oscl::HW::MIPS::CP0::WatchLo::Reg reg) noexcept{
    asm volatile(" mtc0 %0, $18, 2" : : "r" (reg));
	}

inline void watchLo3(Oscl::HW::MIPS::CP0::WatchLo::Reg reg) noexcept{
    asm volatile(" mtc0 %0, $18, 3" : : "r" (reg));
	}

inline void watchHi0(Oscl::HW::MIPS::CP0::WatchHi::Reg reg) noexcept{
    asm volatile(" mtc0 %0, $19, 0" : : "r" (reg));
	}

inline void watchHi1(Oscl::HW::MIPS::CP0::WatchHi::Reg reg) noexcept{
    asm volatile(" mtc0 %0, $19, 1" : : "r" (reg));
	}

inline void watchHi2(Oscl::HW::MIPS::CP0::WatchHi::Reg reg) noexcept{
    asm volatile(" mtc0 %0, $19, 2" : : "r" (reg));
	}

inline void watchHi3(Oscl::HW::MIPS::CP0::WatchHi::Reg reg) noexcept{
    asm volatile(" mtc0 %0, $19, 3" : : "r" (reg));
	}

inline void debug(Oscl::HW::MIPS::CP0::Debug::Reg reg) noexcept{
    asm volatile(" mtc0 %0, $23, 0" : : "r" (reg));
	}

inline void dEPC(Oscl::HW::MIPS::CP0::DEPC::Reg reg) noexcept{
    asm volatile(" mtc0 %0, $24, 0" : : "r" (reg));
	}

inline void perfCntCtl0(Oscl::HW::MIPS::CP0::PerfCntCtl::Reg reg) noexcept{
    asm volatile(" mtc0 %0, $25, 0" : : "r" (reg));
	}

inline void perfCntCnt0(Oscl::HW::MIPS::CP0::PerfCntCnt::Reg reg) noexcept{
    asm volatile(" mtc0 %0, $25, 1" : : "r" (reg));
	}

inline void perfCntCtl1(Oscl::HW::MIPS::CP0::PerfCntCtl::Reg reg) noexcept{
    asm volatile(" mtc0 %0, $26, 2" : : "r" (reg));
	}

inline void perfCntCnt1(Oscl::HW::MIPS::CP0::PerfCntCnt::Reg reg) noexcept{
    asm volatile(" mtc0 %0, $25, 3" : : "r" (reg));
	}

inline void perfCntCtl2(Oscl::HW::MIPS::CP0::PerfCntCtl::Reg reg) noexcept{
    asm volatile(" mtc0 %0, $25, 4" : : "r" (reg));
	}

inline void perfCntCnt2(Oscl::HW::MIPS::CP0::PerfCntCnt::Reg reg) noexcept{
    asm volatile(" mtc0 %0, $25, 5" : : "r" (reg));
	}

inline void perfCntCtl3(Oscl::HW::MIPS::CP0::PerfCntCtl::Reg reg) noexcept{
    asm volatile(" mtc0 %0, $25, 6" : : "r" (reg));
	}

inline void perfCntCnt3(Oscl::HW::MIPS::CP0::PerfCntCnt::Reg reg) noexcept{
    asm volatile(" mtc0 %0, $25, 7" : : "r" (reg));
	}

inline void errCtl(Oscl::HW::MIPS::CP0::ErrCtl::Reg reg) noexcept{
    asm volatile(" mtc0 %0, $26, 0" : : "r" (reg));
	}

inline void cacheErr0(Oscl::HW::MIPS::CP0::CacheErr::Reg reg) noexcept{
    asm volatile(" mtc0 %0, $27, 0" : : "r" (reg));
	}

inline void cacheErr1(Oscl::HW::MIPS::CP0::CacheErr::Reg reg) noexcept{
    asm volatile(" mtc0 %0, $27, 1" : : "r" (reg));
	}

inline void cacheErr2(Oscl::HW::MIPS::CP0::CacheErr::Reg reg) noexcept{
    asm volatile(" mtc0 %0, $27, 2" : : "r" (reg));
	}

inline void cacheErr3(Oscl::HW::MIPS::CP0::CacheErr::Reg reg) noexcept{
    asm volatile(" mtc0 %0, $27, 3" : : "r" (reg));
	}

inline void tagLo(Oscl::HW::MIPS::CP0::TagLo::Reg reg) noexcept{
    asm volatile(" mtc0 %0, $28, 0" : : "r" (reg));
	}

inline void dataLo(Oscl::HW::MIPS::CP0::DataLo::Reg reg) noexcept{
    asm volatile(" mtc0 %0, $28, 1" : : "r" (reg));
	}

inline void tagHi(Oscl::HW::MIPS::CP0::TagHi::Reg reg) noexcept{
    asm volatile(" mtc0 %0, $29, 0" : : "r" (reg));
	}

inline void dataHi(Oscl::HW::MIPS::CP0::DataHi::Reg reg) noexcept{
    asm volatile(" mtc0 %0, $29, 1" : : "r" (reg));
	}

inline void errorEPC(Oscl::HW::MIPS::CP0::ErrorEPC::Reg reg) noexcept{
    asm volatile(" mtc0 %0, $30, 0" : : "r" (reg));
	}

inline void dESAVE(Oscl::HW::MIPS::CP0::DESAVE::Reg reg) noexcept{
    asm volatile(" mtc0 %0, $31, 0" : : "r" (reg));
	}
}
}
}

#endif
