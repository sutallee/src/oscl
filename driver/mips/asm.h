/*
   Copyright (C) 2006 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_mips_asmh_
#define _oscl_drv_mips_asmh_

/** */
namespace Oscl {

/** */
namespace MIPS {

inline void sync(void) noexcept{
    asm volatile(	" sync "
						: : : "memory"
						);
	}

inline void ssnop(void) noexcept{
    asm volatile(	" ssnop "
						: : : "memory"
						);
	}

inline void cachePrimaryDataHitWriteback(unsigned long address) noexcept{
	// Opcode:
	//		[20:18]	- 2#110
	//		[17:16]	- 2#01
	//		Therfore 2#11001 is 0x19
    asm volatile(	" cache 0x19,0(%0)"
						: : "r" (address)
						: "memory"
						);
    }

inline void cachePrimaryDataInstructionHitInvalidate(unsigned long address) noexcept{
	// Opcode:
	//		[20:18]	- 2#100
	//		[17:16]	- 2#00
	//		Therfore 2#11001 is 0x19
    asm volatile(	" cache 0x10,0(%0)"
						: : "r" (address)
						: "memory"
						);
    }

}
}

#endif
