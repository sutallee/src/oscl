/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "device.h"

using namespace Oscl::Mot860P;
using namespace Oscl::Mot860;
using namespace Oscl::Mot860Fec;
using namespace Oscl::Mot8xx;

Device::Device(	Mot8xx::Mpc860P::Map&			registers,
				Oscl::Mt::Mutex::Simple::Api&	mutex
				) noexcept:
		_scc1CPCR(registers.cpm.cpcr),
		_scc2CPCR(registers.cpm.cpcr),
		_scc3CPCR(registers.cpm.cpcr),
		_scc4CPCR(registers.cpm.cpcr),
		_smc1CPCR(registers.cpm.cpcr),
		_smc2CPCR(registers.cpm.cpcr),
		_portA(	mutex,
				registers.port.padat,
				registers.port.padir,
				registers.port.papar,
				registers.port.paodr
				),
		_portB(	mutex,
				registers.portb.pbdat,
				registers.portb.pbdir,
				registers.portb.pbpar,
				registers.portb.pbodr
				),
		_portC(	mutex,
				registers.port.pcdat,
				registers.port.pcdir,
				registers.port.pcpar,
				registers.port.pcso
				),
		_portD(	mutex,
				registers.port.pddat,
				registers.port.pddir,
				registers.port.pdpar
				),
		_sicr(	mutex,
				registers.si.sicr
				),
		_simode(	mutex,
					registers.si.simode
					),
		_dpram(	(unsigned long)registers.dpram,
				sizeof(registers.dpram)+sizeof(registers.xdpram),
				0x0000000F	// 16 byte alignment
				),
		_mutexedDPRAM(_dpram,mutex),
		_siuPic(	registers.siu.sivec,
					registers.siu.simask,
					registers.siu.sipend,
					registers.siu.siel
					),
		_cpmPicError(registers.cpmic.cisr),
		_cpmPicPC4(registers.cpmic.cisr),
		_cpmPicPC5(registers.cpmic.cisr),
		_cpmPicSMC2(registers.cpmic.cisr),
		_cpmPicSMC1(registers.cpmic.cisr),
		_cpmPicSPI(registers.cpmic.cisr),
		_cpmPicPC6(registers.cpmic.cisr),
		_cpmPicTIMER4(registers.cpmic.cisr),
		_cpmPicBit8(registers.cpmic.cisr),
		_cpmPicPC7(registers.cpmic.cisr),
		_cpmPicPC8(registers.cpmic.cisr),
		_cpmPicPC9(registers.cpmic.cisr),
		_cpmPicTIMER3(registers.cpmic.cisr),
		_cpmPicBit13(registers.cpmic.cisr),
		_cpmPicPC10(registers.cpmic.cisr),
		_cpmPicPC11(registers.cpmic.cisr),
		_cpmPicI2C(registers.cpmic.cisr),
		_cpmPicRTT(registers.cpmic.cisr),
		_cpmPicTIMER2(registers.cpmic.cisr),
		_cpmPicBit19(registers.cpmic.cisr),
		_cpmPicIDMA2(registers.cpmic.cisr),
		_cpmPicIDMA1(registers.cpmic.cisr),
		_cpmPicSDMA(registers.cpmic.cisr),
		_cpmPicPC12(registers.cpmic.cisr),
		_cpmPicPC13(registers.cpmic.cisr),
		_cpmPicTIMER1(registers.cpmic.cisr),
		_cpmPicPC14(registers.cpmic.cisr),
		_cpmPicSCC4(registers.cpmic.cisr),
		_cpmPicSCC3(registers.cpmic.cisr),
		_cpmPicSCC2(registers.cpmic.cisr),
		_cpmPicSCC1(registers.cpmic.cisr),
		_cpmPicPC15(registers.cpmic.cisr),
		_cpmPIC(	registers.cpmic.civr,
					_cpmPicError,
					_cpmPicPC4,
					_cpmPicPC5,
					_cpmPicSMC2,
					_cpmPicSMC1,
					_cpmPicSPI,
					_cpmPicPC6,
					_cpmPicTIMER4,
					_cpmPicBit8,
					_cpmPicPC7,
					_cpmPicPC8,
					_cpmPicPC9,
					_cpmPicTIMER3,
					_cpmPicBit13,
					_cpmPicPC10,
					_cpmPicPC11,
					_cpmPicI2C,
					_cpmPicRTT,
					_cpmPicTIMER2,
					_cpmPicBit19,
					_cpmPicIDMA2,
					_cpmPicIDMA1,
					_cpmPicSDMA,
					_cpmPicPC12,
					_cpmPicPC13,
					_cpmPicTIMER1,
					_cpmPicPC14,
					_cpmPicSCC4,
					_cpmPicSCC3,
					_cpmPicSCC2,
					_cpmPicSCC1,
					_cpmPicPC15
					),
		_cpmPicMap(_cpmPIC),
		_pitDriver(	_siuPic.getLevelDriverApi0(),
					registers.sitkey.piscrk,
					registers.sitimer.piscr,
					registers.sitimer.pitc,
					registers.sitimer.pitr,
					(8192/100)+1	// FIXME: This should be determined
									// by the system. The value is written
									// to the PITC register which is typically
									// clocked by either 8192Hz or 9600Hz,
									// depending on whether a 32.768K, or
									// 38.4K crystal is used. The value
									// here attempts to achieve a period
									// approximating 10ms or a frequency
									// of 100Hz.
					),
		_brgc1(registers.brg.brgc1),
		_mutextedBRGC1(_brgc1,mutex),
		_brgcDriver1(_mutextedBRGC1),
		_brgc2(registers.brg.brgc2),
		_mutextedBRGC2(_brgc2,mutex),
		_brgcDriver2(_mutextedBRGC2)
		{
	}

Oscl::Mot860::PortA::PortApi&		Device::getPortA() noexcept{
	return _portA;
	}

Oscl::Mot860::PortB::PortApi&		Device::getPortB() noexcept{
	return _portB;
	}

Oscl::Mot860::PortC::PortApi&		Device::getPortC() noexcept{
	return _portC;
	}

Oscl::Mot860Fec::PortD::PortApi&	Device::getPortD() noexcept{
	return _portD;
	}

Oscl::Mot860::SI::CR::DriverApi&	Device::getSICR() noexcept{
	return _sicr;
	}

Oscl::Mot860::SI::MR::DriverApi&	Device::getSIMODE() noexcept{
	return _simode;
	}

Oscl::ExtAlloc::Api&		Device::getDPRAM() noexcept{
	return _mutexedDPRAM;
	}

Oscl::Mot8xx::Siu::PicApi&	Device::getSiuPicApi() noexcept{
	return _siuPic;
	}

Oscl::Mot8xx::PIT::Api&		Device::getPitApi() noexcept{
	return _pitDriver;
	}

Oscl::Mot860::CI::PIC::Api&	Device::getPicApi() noexcept{
	return _cpmPicMap;
	}

Oscl::Mot8xx::CP::CR::SCC::Api&		Device::getScc1CpcrApi() noexcept{
	return _scc1CPCR;
	}

Oscl::Mot8xx::CP::CR::SCC::Api&		Device::getScc2CpcrApi() noexcept{
	return _scc2CPCR;
	}

Oscl::Mot8xx::CP::CR::SCC::Api&		Device::getScc3CpcrApi() noexcept{
	return _scc3CPCR;
	}

Oscl::Mot8xx::CP::CR::SCC::Api&		Device::getScc4CpcrApi() noexcept{
	return _scc4CPCR;
	}

Oscl::Mot8xx::CP::CR::SMC::Api&		Device::getSmc1CpcrApi() noexcept{
	return _smc1CPCR;
	}

Oscl::Mot8xx::CP::CR::SMC::Api&		Device::getSmc2CpcrApi() noexcept{
	return _smc2CPCR;
	}

Oscl::Motorola::Quicc::Brgc::Api&	Device::getBRGC1() noexcept{
	return _brgcDriver1;
	}

Oscl::Motorola::Quicc::Brgc::Api&	Device::getBRGC2() noexcept{
	return _brgcDriver2;
	}

