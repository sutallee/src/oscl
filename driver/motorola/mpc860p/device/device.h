/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_mot_mpc860p_device_deviceh_
#define _oscl_drv_mot_mpc860p_device_deviceh_
#include "oscl/driver/motorola/mpc8xx/pit/driver.h"
#include "oscl/driver/motorola/mpc8xx/siu/pic.h"
#include "oscl/driver/motorola/mpc860/si/cr/driver.h"
#include "oscl/driver/motorola/mpc860/si/mr/driver.h"
#include "oscl/driver/motorola/mpc860p/device/api.h"
#include "oscl/driver/motorola/mpc860/porta/port.h"
#include "oscl/driver/motorola/mpc860/portb/port.h"
#include "oscl/driver/motorola/mpc860/portc/port.h"
#include "oscl/driver/motorola/mpc860fec/portd/port.h"
#include "oscl/hw/motorola/mpc8xx/mpc860p/imm.h"
#include "oscl/extalloc/driver.h"
#include "oscl/extalloc/mt/mutex.h"
#include "oscl/driver/motorola/mpc8xx/ci/pic.h"
#include "oscl/driver/motorola/mpc8xx/ci/sr/bitint.h"
#include "oscl/driver/motorola/mpc860/ci/pic/map.h"
#include "oscl/driver/motorola/mpc8xx/cp/cr/scc/scc1.h"
#include "oscl/driver/motorola/mpc8xx/cp/cr/scc/scc2.h"
#include "oscl/driver/motorola/mpc8xx/cp/cr/scc/scc3.h"
#include "oscl/driver/motorola/mpc8xx/cp/cr/scc/scc4.h"
#include "oscl/driver/motorola/mpc8xx/cp/cr/smc/smc1.h"
#include "oscl/driver/motorola/mpc8xx/cp/cr/smc/smc2.h"
#include "oscl/driver/motorola/quicc/brgc/driver.h"
#include "oscl/mt/mutex/simple.h"

/** */
namespace Oscl{
/** */
namespace Mot860P{

/** This class implements the low level drivers for a Motorola
	MPC860P processor and attending peripherals. The drivers
	contained in this layer make no use of kernel services.
 */
class Device : public Api {
	private:
		/** */
		Oscl::Mot8xx::CP::CR::SCC::SCC1			_scc1CPCR;
		/** */
		Oscl::Mot8xx::CP::CR::SCC::SCC2			_scc2CPCR;
		/** */
		Oscl::Mot8xx::CP::CR::SCC::SCC3			_scc3CPCR;
		/** */
		Oscl::Mot8xx::CP::CR::SCC::SCC4			_scc4CPCR;
		/** */
		Oscl::Mot8xx::CP::CR::SMC::SMC1			_smc1CPCR;
		/** */
		Oscl::Mot8xx::CP::CR::SMC::SMC2			_smc2CPCR;
		/** */
		Mot860::PortA::Port						_portA;
		/** */
		Mot860::PortB::Port						_portB;
		/** */
		Mot860::PortC::Port						_portC;
		/** */
		Mot860Fec::PortD::Port					_portD;
		/** */
		Mot860::SI::CR::Driver					_sicr;
		/** */
		Mot860::SI::MR::Driver					_simode;
		/** */
		Oscl::ExtAlloc::Driver					_dpram;
		/** */
		Oscl::ExtAlloc::Mutex					_mutexedDPRAM;
		/** */
		Oscl::Mot8xx::Siu::Pic					_siuPic;
		/** */
		Oscl::Mot8xx::CI::SR::BitInterrupt<0>	_cpmPicError;
		/** */
		Oscl::Mot8xx::CI::SR::BitInterrupt<1>	_cpmPicPC4;
		/** */
		Oscl::Mot8xx::CI::SR::BitInterrupt<2>	_cpmPicPC5;
		/** */
		Oscl::Mot8xx::CI::SR::BitInterrupt<3>	_cpmPicSMC2;
		/** */
		Oscl::Mot8xx::CI::SR::BitInterrupt<4>	_cpmPicSMC1;
		/** */
		Oscl::Mot8xx::CI::SR::BitInterrupt<5>	_cpmPicSPI;
		/** */
		Oscl::Mot8xx::CI::SR::BitInterrupt<6>	_cpmPicPC6;
		/** */
		Oscl::Mot8xx::CI::SR::BitInterrupt<7>	_cpmPicTIMER4;
		/** */
		Oscl::Mot8xx::CI::SR::BitInterrupt<8>	_cpmPicBit8;
		/** */
		Oscl::Mot8xx::CI::SR::BitInterrupt<9>	_cpmPicPC7;
		/** */
		Oscl::Mot8xx::CI::SR::BitInterrupt<10>	_cpmPicPC8;
		/** */
		Oscl::Mot8xx::CI::SR::BitInterrupt<11>	_cpmPicPC9;
		/** */
		Oscl::Mot8xx::CI::SR::BitInterrupt<12>	_cpmPicTIMER3;
		/** */
		Oscl::Mot8xx::CI::SR::BitInterrupt<13>	_cpmPicBit13;
		/** */
		Oscl::Mot8xx::CI::SR::BitInterrupt<14>	_cpmPicPC10;
		/** */
		Oscl::Mot8xx::CI::SR::BitInterrupt<15>	_cpmPicPC11;
		/** */
		Oscl::Mot8xx::CI::SR::BitInterrupt<16>	_cpmPicI2C;
		/** */
		Oscl::Mot8xx::CI::SR::BitInterrupt<17>	_cpmPicRTT;
		/** */
		Oscl::Mot8xx::CI::SR::BitInterrupt<18>	_cpmPicTIMER2;
		/** */
		Oscl::Mot8xx::CI::SR::BitInterrupt<19>	_cpmPicBit19;
		/** */
		Oscl::Mot8xx::CI::SR::BitInterrupt<20>	_cpmPicIDMA2;
		/** */
		Oscl::Mot8xx::CI::SR::BitInterrupt<21>	_cpmPicIDMA1;
		/** */
		Oscl::Mot8xx::CI::SR::BitInterrupt<22>	_cpmPicSDMA;
		/** */
		Oscl::Mot8xx::CI::SR::BitInterrupt<23>	_cpmPicPC12;
		/** */
		Oscl::Mot8xx::CI::SR::BitInterrupt<24>	_cpmPicPC13;
		/** */
		Oscl::Mot8xx::CI::SR::BitInterrupt<25>	_cpmPicTIMER1;
		/** */
		Oscl::Mot8xx::CI::SR::BitInterrupt<26>	_cpmPicPC14;
		/** */
		Oscl::Mot8xx::CI::SR::BitInterrupt<27>	_cpmPicSCC4;
		/** */
		Oscl::Mot8xx::CI::SR::BitInterrupt<28>	_cpmPicSCC3;
		/** */
		Oscl::Mot8xx::CI::SR::BitInterrupt<29>	_cpmPicSCC2;
		/** */
		Oscl::Mot8xx::CI::SR::BitInterrupt<30>	_cpmPicSCC1;
		/** */
		Oscl::Mot8xx::CI::SR::BitInterrupt<31>	_cpmPicPC15;
		/** */
		Oscl::Mot8xx::CI::PIC					_cpmPIC;
		/** */
		Oscl::Mot860::CI::PIC::Map				_cpmPicMap;
		/** */
		Oscl::Mot8xx::PIT::Driver				_pitDriver;
		/** */
		Oscl::Bits::
		Port<Oscl::Motorola::Quicc::BRGC::Reg>			_brgc1;
		/** */
		Oscl::Bits::
		MutexedPort<Oscl::Motorola::Quicc::BRGC::Reg>	_mutextedBRGC1;
		/** */
		Oscl::Motorola::Quicc::Brgc::Driver				_brgcDriver1;
		/** */
		Oscl::Bits::
		Port<Oscl::Motorola::Quicc::BRGC::Reg>			_brgc2;
		/** */
		Oscl::Bits::
		MutexedPort<Oscl::Motorola::Quicc::BRGC::Reg>	_mutextedBRGC2;
		/** */
		Oscl::Motorola::Quicc::Brgc::Driver				_brgcDriver2;

	public:
		/** */
		Device(	Mot8xx::Mpc860P::Map&			registers,
				Oscl::Mt::Mutex::Simple::Api&	mutex
				) noexcept;

	public:	// Mot860P::Api
		/** */
		Mot860::PortA::PortApi&			getPortA() noexcept;
		/** */
		Mot860::PortB::PortApi&			getPortB() noexcept;
		/** */
		Mot860::PortC::PortApi&			getPortC() noexcept;
		/** */
		Mot860Fec::PortD::PortApi&		getPortD() noexcept;
		/** */
		Mot860::SI::CR::DriverApi&		getSICR() noexcept;
		/** */
		Mot860::SI::MR::DriverApi&		getSIMODE() noexcept;
		/** */
		Oscl::ExtAlloc::Api&			getDPRAM() noexcept;
		/** */
		Oscl::Mot8xx::Siu::PicApi&		getSiuPicApi() noexcept;
		/** */
		Oscl::Mot8xx::PIT::Api&			getPitApi() noexcept;
		/** */
		Oscl::Mot860::CI::PIC::Api&		getPicApi() noexcept;
		/** */
		Oscl::Mot8xx::CP::CR::SCC::Api&	getScc1CpcrApi() noexcept;
		/** */
		Oscl::Mot8xx::CP::CR::SCC::Api&	getScc2CpcrApi() noexcept;
		/** */
		Oscl::Mot8xx::CP::CR::SCC::Api&	getScc3CpcrApi() noexcept;
		/** */
		Oscl::Mot8xx::CP::CR::SCC::Api&	getScc4CpcrApi() noexcept;
		/** */
		Oscl::Mot8xx::CP::CR::SMC::Api&	getSmc1CpcrApi() noexcept;
		/** */
		Oscl::Mot8xx::CP::CR::SMC::Api&	getSmc2CpcrApi() noexcept;
		/** */
		Oscl::Motorola::Quicc::Brgc::Api&	getBRGC1() noexcept;
		/** */
		Oscl::Motorola::Quicc::Brgc::Api&	getBRGC2() noexcept;
	};

}
}


#endif
