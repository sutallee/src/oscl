/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_mot_mpc860p_device_apih_
#define _oscl_drv_mot_mpc860p_device_apih_
#include "oscl/driver/motorola/mpc8xx/pit/api.h"
#include "oscl/driver/motorola/mpc8xx/siu/picapi.h"
#include "oscl/driver/motorola/mpc860/si/cr/driverapi.h"
#include "oscl/driver/motorola/mpc860/porta/portapi.h"
#include "oscl/driver/motorola/mpc860/portb/portapi.h"
#include "oscl/driver/motorola/mpc860/portc/portapi.h"
#include "oscl/driver/motorola/mpc860fec/portd/portapi.h"
#include "oscl/extalloc/api.h"
#include "oscl/driver/motorola/mpc860/ci/pic/api.h"
#include "oscl/driver/motorola/mpc8xx/cp/cr/scc/api.h"
#include "oscl/driver/motorola/mpc8xx/cp/cr/smc/api.h"
#include "oscl/driver/motorola/quicc/brgc/api.h"

/** */
namespace Oscl{
/** */
namespace Mot860P{

/** */
class Api {
	public:
		/** */
		virtual ~Api(){}
		/** */
		virtual Mot860::PortA::PortApi&		getPortA() noexcept=0;
		/** */
		virtual Mot860::PortB::PortApi&		getPortB() noexcept=0;
		/** */
		virtual Mot860::PortC::PortApi&		getPortC() noexcept=0;
		/** */
		virtual Mot860Fec::PortD::PortApi&	getPortD() noexcept=0;
		/** */
		virtual Mot860::SI::CR::DriverApi&	getSICR() noexcept=0;
		/** */
		virtual Mot860::SI::MR::DriverApi&	getSIMODE() noexcept=0;
		/** */
		virtual Oscl::ExtAlloc::Api&		getDPRAM() noexcept=0;
		/** */
		virtual Oscl::Mot8xx::Siu::PicApi&	getSiuPicApi() noexcept=0;
		/** */
		virtual Oscl::Mot8xx::PIT::Api&		getPitApi() noexcept=0;
		/** */
		virtual Oscl::Mot860::CI::PIC::Api&	getPicApi() noexcept=0;
		/** */
		virtual Oscl::Mot8xx::CP::CR::SCC::Api&	getScc1CpcrApi() noexcept=0;
		/** */
		virtual Oscl::Mot8xx::CP::CR::SCC::Api&	getScc2CpcrApi() noexcept=0;
		/** */
		virtual Oscl::Mot8xx::CP::CR::SCC::Api&	getScc3CpcrApi() noexcept=0;
		/** */
		virtual Oscl::Mot8xx::CP::CR::SCC::Api&	getScc4CpcrApi() noexcept=0;
		/** */
		virtual Oscl::Mot8xx::CP::CR::SMC::Api&	getSmc1CpcrApi() noexcept=0;
		/** */
		virtual Oscl::Mot8xx::CP::CR::SMC::Api&	getSmc2CpcrApi() noexcept=0;
		/** */
		virtual Oscl::Motorola::Quicc::Brgc::Api&	getBRGC1() noexcept=0;
		/** */
		virtual Oscl::Motorola::Quicc::Brgc::Api&	getBRGC2() noexcept=0;
	};

}
}


#endif
