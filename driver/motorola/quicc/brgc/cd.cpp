/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "cd.h"

using namespace Oscl::Motorola::Quicc::Brgc;

CD::CD(	Oscl::Bits::FieldApi<Oscl::Motorola::Quicc::BRGC::Reg>& brgc
				) noexcept:
		_brgc(brgc)
		{
	}

void	CD::setDivisor(unsigned short value) noexcept{
	_brgc.changeBits(	Oscl::Motorola::Quicc::BRGC::CD::FieldMask,
						(		(value << Oscl::Motorola::Quicc::BRGC::CD::Lsb)
							&	Oscl::Motorola::Quicc::BRGC::CD::FieldMask
							)
						);
	}

unsigned short	CD::getDivisor() const noexcept{
	Oscl::Motorola::Quicc::BRGC::Reg	value;
	value	= _brgc.read();
	value	&= Oscl::Motorola::Quicc::BRGC::CD::FieldMask;
	value	>>= Oscl::Motorola::Quicc::BRGC::CD::Lsb;
	return value;
	}

