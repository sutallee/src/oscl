/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_mot_quicc_brgc_driverh_
#define _oscl_drv_mot_quicc_brgc_driverh_
#include "api.h"
#include "rst.h"
#include "en.h"
#include "extc.h"
#include "atb.h"
#include "cd.h"
#include "div.h"

/** */
namespace Oscl {
/** */
namespace Motorola {
/** */
namespace Quicc {
/** */
namespace Brgc {

/** */
class Driver : public Api {
	private:
		/** */
		Oscl::Motorola::Quicc::Brgc::RST	_rst;
		/** */
		Oscl::Motorola::Quicc::Brgc::EN		_en;
		/** */
		Oscl::Motorola::Quicc::Brgc::EXTC	_extc;
		/** */
		Oscl::Motorola::Quicc::Brgc::ATB	_atb;
		/** */
		Oscl::Motorola::Quicc::Brgc::CD		_cd;
		/** */
		Oscl::Motorola::Quicc::Brgc::DIV	_div;

	public:
		/** */
		Driver(	Oscl::Bits::FieldApi<Oscl::Motorola::Quicc::BRGC::Reg>& brgc
				) noexcept;
		
	public:
		/** */
		RSTApi&		getRST() noexcept;
		/** */
		ENApi&		getEN() noexcept;
		/** */
		EXTCApi&	getEXTC() noexcept;
		/** */
		ATBApi&		getATB() noexcept;
		/** */
		CDApi&		getCD() noexcept;
		/** */
		DIVApi&		getDIV() noexcept;
	};

}
}
}
}

#endif
