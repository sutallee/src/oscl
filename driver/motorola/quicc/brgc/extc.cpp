/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "extc.h"

using namespace Oscl::Motorola::Quicc::Brgc;

EXTC::EXTC(	Oscl::Bits::FieldApi<Oscl::Motorola::Quicc::BRGC::Reg>& brgc
				) noexcept:
		_brgc(brgc)
		{
	}

void	EXTC::selectBRGCLK() noexcept{
	_brgc.changeBits(	Oscl::Motorola::Quicc::BRGC::EXTC::FieldMask,
						Oscl::Motorola::Quicc::BRGC::EXTC::ValueMask_BRGCLK
						);
	}

void	EXTC::selectCLK2() noexcept{
	_brgc.changeBits(	Oscl::Motorola::Quicc::BRGC::EXTC::FieldMask,
						Oscl::Motorola::Quicc::BRGC::EXTC::ValueMask_CLK2
						);
	}

void	EXTC::selectCLK6() noexcept{
	_brgc.changeBits(	Oscl::Motorola::Quicc::BRGC::EXTC::FieldMask,
						Oscl::Motorola::Quicc::BRGC::EXTC::ValueMask_CLK6
						);
	}

