/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_mot_quicc_sdma_tx_txbdh_
#define _oscl_drv_mot_quicc_sdma_tx_txbdh_
#include "oscl/bits/bitfield.h"
#include "oscl/hw/motorola/quicc/sdma/bdreg.h"

/** */
namespace Oscl{
/** */
namespace Motorola {
/** */
namespace Quicc {
/** */
namespace Sdma {
/** */
namespace Tx {

/** */
class BufferDesc {
	public:
		/** */
		Oscl::BitField<Oscl::Motorola::Quicc::Sdma::TxBD::Status::Reg>	_cs;
		/** */
		uint16_t		_length;
		/** */
		const void*		_buffer;

	public:
		/** */
		inline bool	transmitterDone() noexcept{
			using namespace Oscl::Motorola::Quicc::Sdma::TxBD::Status;
			return _cs.equal(R::FieldMask,R::ValueMask_Complete);
			}
		/** */
		inline void	transmit() noexcept{
			using namespace Oscl::Motorola::Quicc::Sdma::TxBD::Status;
			_cs.changeBits(R::FieldMask,R::ValueMask_Ready);
			}
		/** */
		inline void	lastDescInRing() noexcept{
			using namespace Oscl::Motorola::Quicc::Sdma::TxBD::Status;
			_cs.changeBits(W::FieldMask,W::ValueMask_LastDesc);
			}
		/** */
		inline void	notLastDescInRing() noexcept{
			using namespace Oscl::Motorola::Quicc::Sdma::TxBD::Status;
			_cs.changeBits(W::FieldMask,W::ValueMask_NotLastDesc);
			}
		/** */
		inline void enableInterrupt() noexcept{
			using namespace Oscl::Motorola::Quicc::Sdma::TxBD::Status;
			_cs.changeBits(I::FieldMask,I::ValueMask_Enable);
			}
		/** */
		inline void disableInterrupt() noexcept{
			using namespace Oscl::Motorola::Quicc::Sdma::TxBD::Status;
			_cs.changeBits(I::FieldMask,I::ValueMask_Disable);
			}
	};

}
}
}
}
}

#endif
