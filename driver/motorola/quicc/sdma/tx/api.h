/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_mot_quicc_sdma_tx_apih_
#define _oscl_drv_mot_quicc_sdma_tx_apih_
#include "txbd.h"
#include "oscl/done/api.h"

/** */
namespace Oscl{
/** */
namespace Motorola {
/** */
namespace Quicc {
/** */
namespace Sdma {
/** */
namespace Tx {

class Api {
	public:
		/** */
		virtual ~Api(){}

		/** */
		virtual void	initialize() noexcept=0;
		/** */
		virtual void	processDescriptors() noexcept=0;
		/** */
		virtual unsigned	nAvailableDescs() noexcept=0;
		/** */
		virtual unsigned	nDescriptors() noexcept=0;

		/** Invoked to prepare a block of contiguous descriptors for
			transmission. Returns false if a block is already open.
		 */
		virtual bool		openBlock() noexcept=0;
		/** */
		virtual bool	appendBufferToBlock(	const uint8_t*	buffer,
												uint16_t		count
												) noexcept=0;
		/** */
		virtual bool	appendBufferToBlock(	const uint8_t*		buffer,
												uint16_t			count,
												Oscl::Done::Api&	callback
												) noexcept=0;

		/** Give all descriptors in block to the SDMA transmitter for
			transmission.
		 */
		virtual void	closeBlock() noexcept=0;

		/** Give all descriptors in block to the SDMA transmitter for
			transmission, and associate the specified transmit complete
			callback with the last buffer in the block.
		 */
		virtual void	closeBlock(Oscl::Done::Api& callback) noexcept=0;

		/** Reclaims all of the descriptors associated with the open
			block and closes the block. This is typically called when
			the client cannot get enough resources to finish the block.
		 */
		virtual void	cancelBlock() noexcept=0;
	};

}
}
}
}
}

#endif
