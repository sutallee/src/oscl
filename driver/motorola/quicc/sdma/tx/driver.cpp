/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "oscl/cache/operations.h"
#include "driver.h"

using namespace Oscl::Motorola::Quicc::Sdma::Tx;
using namespace Oscl::Motorola::Quicc::Sdma::TxBD;

Driver::Driver(	ErrorHandler&		errHandler,
				BufferDesc			descriptors[],
				Oscl::Done::Api*	completes[],
				unsigned			nDescriptors,
				Status::Reg			lastDescInBlockOptsFieldMask,
				Status::Reg			lastDescInBlockOpts
				) noexcept:
		_errHandler(errHandler),
		_descriptors(descriptors),
		_completes(completes),
		_nDescs(nDescriptors),
		_lastDescInBlockOptsFieldMask(lastDescInBlockOptsFieldMask),
		_lastDescInBlockOpts(lastDescInBlockOpts),
		_currentDesc(0),
		_nBuffsInBlock(0),
		_nextDescExpected(0),
		_nDescsTransmitting(0),
		_blockCurrentlyOpen(false),
		_initialized(false)
		{
	}

void	Driver::initialize() noexcept{
	_initialized	= true;
	for(unsigned i=0;i<_nDescs;i++){
		_descriptors[i]._cs		= 0;
		_descriptors[i]._buffer	= 0;
		_completes[i]			= 0;
		}
	_descriptors[_nDescs-1].lastDescInRing();
	}

void	Driver::reinitialize() noexcept{
	for(unsigned i=0;i<_nDescs;i++){
		_descriptors[i]._cs		= 0;
		_descriptors[i]._buffer	= 0;
		_completes[i]			= 0;
		}
	_descriptors[_nDescs-1].lastDescInRing();
	++_nextDescExpected;
	_nextDescExpected	%= _nDescs;
	_currentDesc		= _nextDescExpected;
	_nBuffsInBlock		= 0;
	_nDescsTransmitting	= 0;
	_blockCurrentlyOpen	= false;
	}

void	Driver::processDescriptors() noexcept{
	for(;_nDescsTransmitting;--_nDescsTransmitting){
		OsclCacheDataInvalidateRange(	&_descriptors[_nextDescExpected],
										sizeof(BufferDesc)
										);
		if(_descriptors[_nextDescExpected].transmitterDone()){
			if(_errHandler.sdmaIsStopped(_descriptors[_nextDescExpected]._cs)){
				completePendingAllDescriptors();
				reinitialize();
				_errHandler.restart();
				return;
				}
			if(_completes[_nextDescExpected]) {
				_completes[_nextDescExpected]->done();
				_completes[_nextDescExpected]	= 0;
				}
			++_nextDescExpected;
			_nextDescExpected	%= _nDescs;
			}
		else break;
		}
	}

unsigned	Driver::nAvailableDescs() noexcept{
	return _nDescs-_nDescsTransmitting;
	}

unsigned	Driver::nDescriptors() noexcept{
	return _nDescs;
	}

bool		Driver::openBlock() noexcept{
	if(_blockCurrentlyOpen) return false;
	if(_nDescs == _nDescsTransmitting) return false;
	_blockCurrentlyOpen	= true;
	_nBuffsInBlock		= 0;
	return true;
	}

bool	Driver::appendBufferToBlock(	const uint8_t*	buffer,
										uint16_t		count
										) noexcept{
	return appendBufferToBlock(buffer,count,(Oscl::Done::Api*)0);
	}

bool	Driver::appendBufferToBlock(	const uint8_t*		buffer,
										uint16_t			count,
										Oscl::Done::Api&	callback
										) noexcept{
	return appendBufferToBlock(buffer,count,&callback);
	}

void	Driver::closeBlock() noexcept{
	closeBlock((Oscl::Done::Api*)0);
	}

void	Driver::closeBlock(Oscl::Done::Api& callback) noexcept{
	closeBlock(&callback);
	}

void	Driver::cancelBlock() noexcept{
	_nBuffsInBlock		= 0;
	_blockCurrentlyOpen	= false;
	}

bool	Driver::appendBufferToBlock(	const uint8_t*		buffer,
										uint16_t			count,
										Oscl::Done::Api*	callback
										) noexcept{
	if(!_blockCurrentlyOpen) return false;
	if(currentDescriptorIsBusy()){
		_nBuffsInBlock	= 0;
		return false;
		}
	unsigned	descIndex	= (_currentDesc+_nBuffsInBlock)%_nDescs;
	if(callback){
		_descriptors[descIndex].enableInterrupt();
		}
	else{
		_descriptors[descIndex].disableInterrupt();
		}
	_completes[descIndex]			= callback;
	_descriptors[descIndex]._buffer	= buffer;
	_descriptors[descIndex]._length	= count;
	++_nBuffsInBlock;
	OsclCacheDataFlushRange((void*)buffer,count);
	return true;
	}

void	Driver::closeBlock(Oscl::Done::Api* callback) noexcept{
	if(!_blockCurrentlyOpen) return;
	txAllBuffersInBlock(callback);
	_nDescsTransmitting	+= _nBuffsInBlock;
	_currentDesc	= (_currentDesc+_nBuffsInBlock)%_nDescs;
	_nBuffsInBlock		= 0;
	_blockCurrentlyOpen	= false;
	}

bool	Driver::currentDescriptorIsBusy() noexcept{
	unsigned	desc	= (_currentDesc+_nBuffsInBlock)%_nDescs;
	return !_descriptors[desc].transmitterDone();
	}

void	Driver::txAllBuffersInBlock(Oscl::Done::Api* callback) noexcept{
	if(!_nBuffsInBlock) return;
	// First process the last descriptor in the open block.
	unsigned	desc	= ((_nBuffsInBlock-1)+_currentDesc)%_nDescs;
	if(callback){
		_completes[desc]	= callback;
		_descriptors[desc].enableInterrupt();
		}
	_descriptors[desc]._cs.changeBits(	_lastDescInBlockOptsFieldMask,
										_lastDescInBlockOpts
										);
	_descriptors[desc].transmit();
	OsclCacheDataFlushRange(&_descriptors[desc],sizeof(BufferDesc));
	if(_nBuffsInBlock == 1) return;
	// Process remaining descriptors in the open block in
	// reverse order.
	for(unsigned i=(_nBuffsInBlock-1);i;--i){
		using namespace Oscl::Motorola::Quicc::Sdma;
		desc	= ((i-1)+_currentDesc)%_nDescs;
		_descriptors[desc]._cs.changeBits(
			(		TxBD::Status::R::FieldMask
				|	TxBD::Status::PAD::FieldMask
				|	TxBD::Status::I::FieldMask
				|	TxBD::Status::L::FieldMask
				|	TxBD::Status::TC::FieldMask
				),
			(
					TxBD::Status::R::ValueMask_Ready
				|	TxBD::Status::PAD::ValueMask_DontPadFrame
				|	TxBD::Status::I::ValueMask_Disable
				|	TxBD::Status::L::ValueMask_NotLastDesc
				|	TxBD::Status::TC::ValueMask_NoCRC
				)
			);
		OsclCacheDataFlushRange(&_descriptors[desc],sizeof(BufferDesc));
		}
	}

void	Driver::completePendingAllDescriptors() noexcept{
	unsigned	nextDescExpected	= _nextDescExpected;
	for(;_nDescsTransmitting;--_nDescsTransmitting){
		if(_completes[nextDescExpected]) {
			_completes[nextDescExpected]->done();
			_completes[nextDescExpected]	= 0;
			}
		++nextDescExpected;
		nextDescExpected	%= _nDescs;
		}
	}
