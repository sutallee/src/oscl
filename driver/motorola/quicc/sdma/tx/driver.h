/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_mot_quicc_sdma_tx_driverh_
#define _oscl_drv_mot_quicc_sdma_tx_driverh_
#include "txbd.h"
#include "oscl/done/api.h"
#include "api.h"

/** */
namespace Oscl{
/** */
namespace Motorola {
/** */
namespace Quicc {
/** */
namespace Sdma {
/** */
namespace Tx {

/** */
class ErrorHandler {
	public:
		/** Shut-up GCC. */
		virtual ~ErrorHandler() {}
		/** This operation is implemented by the upper layer.
			Its primary function is to examine the descriptor
			status bits and determin if the SDMA transmitter
			needs to be restarted. The implementation may
			also use this operation to accumulate errro statistics.
			It is invoked for each completed buffer descriptor.
		 */
		virtual bool	sdmaIsStopped(unsigned descStatus) noexcept=0;

		/** This operation is invoked by the driver to instruct the
			upper layer to restart the SDMA transmitter. It is only
			invoked by the driver if the sdmaTxIsStopped() operation
			returns true.
		 */
		virtual void	restart() noexcept=0;
	};

/** */
class Driver : public Api {
	private:
		/** */
		ErrorHandler&					_errHandler;
		/** */
		BufferDesc*						const _descriptors;
		/** */
		Oscl::Done::Api**				const _completes;
		/** */
		const unsigned					_nDescs;
		/** */
		const TxBD::Status::Reg			_lastDescInBlockOptsFieldMask;
		/** */
		const TxBD::Status::Reg			_lastDescInBlockOpts;
		/** */
		unsigned						_currentDesc;
		/** */
		unsigned						_nBuffsInBlock;
		/** */
		unsigned						_nextDescExpected;
		/** */
		unsigned						_nDescsTransmitting;
		/** */
		bool							_blockCurrentlyOpen;
		/** */
		bool							_initialized;

	public:
		/** */
		Driver(	ErrorHandler&				errHandler,
				BufferDesc					descriptors[],
				Oscl::Done::Api*			completes[],
				unsigned					nDescriptors,
				TxBD::Status::Reg			lastDescInBlockOptsFieldMask,
				TxBD::Status::Reg			lastDescInBlockOpts
				) noexcept;
		/** */
		void	initialize() noexcept;
		/** */
		void	reinitialize() noexcept;
		/** */
		void	processDescriptors() noexcept;
		/** */
		unsigned	nAvailableDescs() noexcept;
		/** */
		unsigned	nDescriptors() noexcept;
		/** Invoked to prepare a block of contiguous descriptors for
			transmission. Returns false if a block is already open.
		 */
		bool		openBlock() noexcept;
		/** */
		bool	appendBufferToBlock(	const uint8_t*	buffer,
										uint16_t		count
										) noexcept;
		/** */
		bool	appendBufferToBlock(	const uint8_t*		buffer,
										uint16_t			count,
										Oscl::Done::Api&	callback
										) noexcept;
		/** Give all descriptors in block to the SDMA transmitter for
			transmission.
		 */
		void	closeBlock() noexcept;
		/** Give all descriptors in block to the SDMA transmitter for
			transmission, and associate the specified transmit complete
			callback with the last buffer in the block.
		 */
		void	closeBlock(Oscl::Done::Api& callback) noexcept;
		/** Reclaims all of the descriptors associated with the open
			block and closes the block. This is typically called when
			the client cannot get enough resources to finish the block.
		 */
		void	cancelBlock() noexcept;

	private:
		/** */
		bool	appendBufferToBlock(	const uint8_t*		buffer,
										uint16_t			count,
										Oscl::Done::Api*	callback
										) noexcept;
		/** */
		void	closeBlock(Oscl::Done::Api* callback) noexcept;
		/** */
		bool	currentDescriptorIsBusy() noexcept;
		/** */
		void	txAllBuffersInBlock(Oscl::Done::Api* callback) noexcept;
		/** */
		void	completePendingAllDescriptors() noexcept;
	};

}
}
}
}
}

#endif
