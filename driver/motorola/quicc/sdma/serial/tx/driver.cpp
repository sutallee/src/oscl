/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "driver.h"

using namespace Oscl::Motorola::Quicc::Sdma::Serial::Tx;

Driver::Driver(Oscl::Motorola::Quicc::Sdma::Tx::Api& driver) noexcept:
		_driver(driver)
		{
	}

bool	Driver::sendBuffer(	Oscl::Done::Api&	complete,
							const void*			buffer,
							unsigned			length
							) noexcept{
	if(!_driver.openBlock()){
		// Huston ... we have a problem
		for(;;);
		}
	if(!_driver.appendBufferToBlock(	(const uint8_t*)buffer,
										length,
										complete
										)
			){
		// Insufficient resources
		_driver.cancelBlock();
		return false;
		}
	_driver.closeBlock();
	return true;
	}

void	Driver::processTransmitter() noexcept{
	_driver.processDescriptors();
	}

void	Driver::initialize() noexcept{
	_driver.initialize();
	}

