/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_mot_quicc_sdma_serial_rx_driverh_
#define _oscl_drv_mot_quicc_sdma_serial_rx_driverh_
#include "oscl/driver/motorola/quicc/sdma/rx/driver.h"
#include "oscl/serial/driver/rx/api.h"

/** */
namespace Oscl{
/** */
namespace Motorola {
/** */
namespace Quicc {
/** */
namespace Sdma {
/** */
namespace Serial {
/** */
namespace Rx {

/** */
class DescErrorObserver {
	public:
		/** Shut-up GCC. */
		virtual ~DescErrorObserver() {}

		/** This operation must return true if any error bits are
			set that should cause the frame to be discarded.
		 */
		virtual bool	hasError(unsigned descIndex) noexcept=0;
	};

/** */
class Driver :
	public Oscl::Motorola::Quicc::Sdma::Rx::DescStatusMatch,
	public Oscl::Serial::Driver::RX::Api
	{
	private:
		/** */
		Oscl::Motorola::Quicc::Sdma::Rx::Driver			_driver;
		/** */
		Oscl::Motorola::Quicc::Sdma::Rx::BufferDesc*	const _descs;
		/** */
		DescErrorObserver&					_errorObserver;
		/** */
		unsigned							_octetsReceivedInFrame;
		/** */
		unsigned							_nBuffersToProcessInFrame;
		/** */
		unsigned							_nZeroLengthBuffersInDiscardLoop;
		/** */
		unsigned							_nBuffersDiscarded;
		/** */
		unsigned							_discardedFrames;
		/** */
		bool								_discardBuffer;

	public:
		/** */
		Driver(	Oscl::Motorola::Quicc::Sdma::Rx::BufferDesc	descriptors[],
				unsigned									nDescriptors,
				DescErrorObserver&							errorObserver
				) noexcept;

		/** */
		void	initialize() noexcept;

	private:	// Oscl::Serial::Driver::RX::Api
		/** */
		unsigned	minimumBufferSize() const noexcept;

		/** */
		void		supplyBuffer(void* buffer,unsigned size) noexcept;
	
		/** */
		void		reuseBuffer(void* buffer) noexcept;

		/** */
		unsigned	nextCompletedBuffer(void** buffer) noexcept;

	private: // Oscl::Motorola::Quicc::Sdma::Rx::DescStatusMatch
		/** */
		bool	match(unsigned desc) noexcept;
	};

}
}
}
}
}
}
#endif
