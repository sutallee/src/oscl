/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "driver.h"
#include "oscl/error/fatal.h"

class FrameStatus : public Oscl::Motorola::Quicc::Sdma::Rx::DescStatusMatch{
	public:
		bool	match(unsigned desc) noexcept;
	};

using namespace Oscl::Motorola::Quicc::Sdma::Serial::Rx;

Driver::Driver(	Oscl::Motorola::Quicc::Sdma::Rx::BufferDesc	descriptors[],
				unsigned									nDescriptors,
				DescErrorObserver&							errorObserver
				) noexcept:
		_driver(descriptors,nDescriptors,true),
		_descs(descriptors),
		_errorObserver(errorObserver),
		_nZeroLengthBuffersInDiscardLoop(0),
		_nBuffersDiscarded(0),
		_discardedFrames(0)
		{
	}

void	Driver::initialize() noexcept{
	_driver.initialize();
	}

unsigned	Driver::minimumBufferSize() const noexcept{
	return _driver.minimumBufferSize();
	}

void	Driver::supplyBuffer(void* buffer,unsigned size) noexcept{
	_driver.giveBuffer((uint8_t*)buffer,size);
	}

void	Driver::reuseBuffer(void* buffer) noexcept{
	_driver.reuseBuffer((uint8_t*)buffer);
	}

unsigned	Driver::nextCompletedBuffer(void** buffer) noexcept{
	uint8_t*	tBuffer;
	if(!_driver.openBlock()){
		return 0;
		}
	_discardBuffer	= false;
	_driver.notEmptyDescIterate(*this);
	if(_discardBuffer){
		if(_driver.getNextBuffer(&tBuffer)){
			_driver.reuseBuffer(tBuffer);
			}
		_driver.closeBlock();
		return 0;
		}

	unsigned	length	= _driver.getNextBuffer(&tBuffer);
	if(length){
		*buffer	= tBuffer;
		}
	_driver.closeBlock();
	return length;
	}

bool	Driver::match(unsigned desc) noexcept{
	using namespace Oscl::Motorola::Quicc::Sdma::RxBD::Status;
	_discardBuffer	|= _errorObserver.hasError(desc);
	return true;
	}

