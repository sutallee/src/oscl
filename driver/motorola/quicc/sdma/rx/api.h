/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "oscl/frame/rx/return.h"
#include "rxbd.h"

/** */
namespace Oscl{
/** */
namespace Motorola {
/** */
namespace Quicc {
/** */
namespace Sdma {
/** */
namespace Rx {

/** */
class DescStatusMatch {
	public:
		/** Shut-up GCC. */
		virtual ~DescStatusMatch() {}
		/** */
		virtual bool	match(unsigned desc) noexcept=0;
	};

/** */
class Api {
	public:
		/** */
		virtual ~Api() {}
		/** */
		virtual void		initialize() noexcept=0;
		/** */
		virtual bool		openBlock() noexcept=0;
		/** */
		virtual unsigned	getNextBuffer(uint8_t** buffer) noexcept=0;
		/** */
		virtual void		closeBlock() noexcept=0;
		/** */
		virtual void		cancelBlock() noexcept=0;
		/** */
		virtual unsigned	minimumBufferSize() const noexcept=0;
		/** */
		virtual unsigned	maximumBufferSize() const noexcept=0;
		/** */
		virtual bool		giveBuffer(	uint8_t*		buffer,
										uint16_t	size
										) noexcept=0;
		/** */
		virtual void		returnBuffer(uint8_t*	 buffer) noexcept=0;

		/** */
		virtual void		reclaimBuffers(Oscl::Frame::Rx::Return& callback) noexcept=0;

		/** */
		virtual unsigned	notEmptyDescIterate(	DescStatusMatch&	endConditon
													) noexcept=0;
		virtual unsigned	currentDesc() const noexcept=0;

		/** */
		virtual void		reuseBuffer(uint8_t* buffer) noexcept=0;
	};

}
}
}
}
}
