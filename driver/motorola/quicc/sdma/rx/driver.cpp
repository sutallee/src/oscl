/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "oscl/mt/mutex/iss.h"
#include "oscl/cache/operations.h"
#include "driver.h"

using namespace Oscl::Motorola::Quicc::Sdma::Rx;
using namespace Oscl::Motorola::Quicc::Sdma::RxBD;
using namespace Oscl;

Driver::Driver(	BufferDesc	descriptors[],
				unsigned	nDescriptors,
				bool		interruptEveryBuffer
				) noexcept:
		_freeBuffers(0),
		_nDescs(nDescriptors),
		_nextDescExpected(0),
		_firstDescSansBuffer(0),
		_nDescsSansBuffer(nDescriptors),
		_nextDescInBlock(0),
		_bufferSize(0),
		_nBuffsInBlock(0),
		_interruptEveryBuffer(interruptEveryBuffer),
		_initialized(false),
		_blockCurrentlyOpen(false),
		_descriptors(descriptors)
		{
	}

void		Driver::initialize() noexcept{
	_initialized	= true;
	for(unsigned i=0;i<_nDescs;++i){
		_descriptors[i]._cs		= 0;
		_descriptors[i]._length	= 0x5A5A;
		OsclCacheDataFlushRange(&_descriptors[i],sizeof(BufferDesc));
		}
	_descriptors[_nDescs-1].lastDescInRing();
	OsclCacheDataFlushRange(&_descriptors[_nDescs-1],sizeof(BufferDesc));
	}

bool		Driver::openBlock() noexcept{
	if(!_initialized) return false;
	if(_blockCurrentlyOpen) return false;
	unsigned nDescsWithBuffers	= _nDescs-_nDescsSansBuffer;
	if(nDescsWithBuffers ==0) return false;
	if(!_descriptors[_nextDescExpected].receiverDone()) return false;
	_blockCurrentlyOpen	= true;
	_nextDescInBlock	= 0;
	return true;
	}

unsigned	Driver::getNextBuffer(uint8_t** buffer) noexcept{
	if(!_blockCurrentlyOpen) return 0;
	unsigned desc	= (_nextDescExpected+_nextDescInBlock)%_nDescs;
	OsclCacheDataInvalidateRange(&_descriptors[desc],sizeof(BufferDesc));
	if(!_descriptors[desc].receiverDone()) return 0;
	++_nextDescInBlock;
	*buffer	= (uint8_t*)_descriptors[desc]._buffer;
	unsigned length	= _descriptors[desc]._length;
	OsclCacheDataInvalidateRange(*buffer,length);
	return length;
	}

void		Driver::closeBlock() noexcept{
	if(!_blockCurrentlyOpen) return;
	_blockCurrentlyOpen	= false;
	_nextDescExpected	+= _nextDescInBlock;
	_nextDescExpected	%= _nDescs;
	_nDescsSansBuffer	+= _nextDescInBlock;
	while(_nDescsSansBuffer){
		uint8_t*	buffer	= allocateBuffer();
		if(!buffer) break;
		attachToNeedyDesc(buffer);
		}
	}

void		Driver::cancelBlock() noexcept{
	if(!_blockCurrentlyOpen) return;
	_blockCurrentlyOpen	= false;
	}

unsigned	Driver::minimumBufferSize() const noexcept{
	return sizeof(Buffer);
	}

unsigned	Driver::maximumBufferSize() const noexcept{
	return 0xFFFF;
	}

bool		Driver::giveBuffer(	uint8_t*		buffer,
								uint16_t	size
								) noexcept{
	if(size < minimumBufferSize()) return false;
	if(!_bufferSize){
		_bufferSize	= size;
		}
	else if(_bufferSize != size) return false;

	unsigned long	buff	= (unsigned long) buffer;
	if(buff != (buff & (~0x03)) ) return false;
	reuseBuffer(buffer);
	return true;
	}

void		Driver::returnBuffer(uint8_t*	 buffer) noexcept{
	freeBuffer(buffer);
	}

void		Driver::reclaimBuffers(Oscl::Frame::Rx::Return& callback) noexcept{
	uint8_t*	buffer;
	while((buffer=allocateBuffer())){
		callback.release(buffer);
		}
	for(unsigned i=0;i<_nDescs;++i){
		if(descriptorHasNoBuffer(i)) continue;
		if((buffer=(uint8_t*)_descriptors[i]._buffer)){
			callback.release(buffer);
			}
		}
	}

void		Driver::reuseBuffer(uint8_t* buffer) noexcept{
	if(attachToNeedyDesc(buffer)) return;
	freeBuffer(buffer);
	}

unsigned	Driver::notEmptyDescIterate(	DescStatusMatch&	endCondition
												) noexcept{
	for(unsigned i=0;i<_nDescs;++i){
		unsigned desc	= (_nextDescExpected+i)%_nDescs;
		OsclCacheDataInvalidateRange(&_descriptors[desc],sizeof(BufferDesc));
		if(_descriptors[desc].receiverDone()){
			// The client returns true to match if the status of the
			// descriptor matches its requirements. Typically, this would
			// be end-of-frame, or unconditionally for async protocols.
			// Additionally, the higher level detailed status bits of
			// the descriptor can be accumulated, and used after the
			// notEmptyDescsUpToStatus() operation returns to make
			// the decision to discard the results, or to maintain
			// performance statistics.
			if(endCondition.match(desc)){
				return i+1;
				}
			}
		else return 0;
		}
	return 0;
	}

void		Driver::freeBuffer(uint8_t* buffer) noexcept{
	Oscl::Mt::IntScopeSync	ss;
	Buffer*	b	= (Buffer*)buffer;
	b->_link	= _freeBuffers;
	_freeBuffers	= b;
	}

uint8_t*		Driver::allocateBuffer() noexcept{
	Oscl::Mt::IntScopeSync	ss;
	Buffer*	b	= _freeBuffers;
	if(b){
		_freeBuffers	= b->_link;
		}
	return (uint8_t*)b;
	}

bool		Driver::attachToNeedyDesc(uint8_t* buffer) noexcept{
	if(_nDescsSansBuffer){
		--_nDescsSansBuffer;
		_descriptors[_firstDescSansBuffer]._buffer	= buffer;
		_descriptors[_firstDescSansBuffer].clearStatus();
		if(_interruptEveryBuffer){
			_descriptors[_firstDescSansBuffer].enableInterrupt();
			}
		else {
			_descriptors[_firstDescSansBuffer].disableInterrupt();
			}
		_descriptors[_firstDescSansBuffer].empty();
		OsclCacheDataFlushRange(	&_descriptors[_firstDescSansBuffer],
									sizeof(BufferDesc)
									);
		++_firstDescSansBuffer;
		_firstDescSansBuffer	%= _nDescs;
		return true;
		}
	return false;
	}

bool		Driver::descriptorHasNoBuffer(unsigned desc) noexcept{
	if(_nDescsSansBuffer){
		unsigned lastDescSansBuffer	=
			(_firstDescSansBuffer+_nDescsSansBuffer-1)%_nDescs;
		if(_firstDescSansBuffer < lastDescSansBuffer){
			if((desc <= lastDescSansBuffer) && (desc >= _firstDescSansBuffer)){
				return true;
				}
			}
		else {
			if(desc >= _firstDescSansBuffer) return true;
			if(desc <= lastDescSansBuffer) return true;
			}
		}
	return false;
	}

unsigned	Driver::currentDesc() const noexcept{
	return (_nextDescExpected+_nextDescInBlock)%_nDescs;
	}
