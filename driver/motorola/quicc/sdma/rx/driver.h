/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_mot_quicc_sdma_rx_driverh_
#define _oscl_drv_mot_quicc_sdma_rx_driverh_
#include "api.h"
#include "oscl/frame/rx/return.h"
#include "rxbd.h"

/** */
namespace Oscl{
/** */
namespace Motorola {
/** */
namespace Quicc {
/** */
namespace Sdma {
/** */
namespace Rx {

/** */
class Driver : public Api {
	private:
		/** */
		typedef struct Buffer {
			/** */
			struct Buffer*	_link;
			} Buffer;
		/** */
		Buffer*							_freeBuffers;
		/** */
		const unsigned					_nDescs;
		/** */
		unsigned						_nextDescExpected;
		/** */
		unsigned						_firstDescSansBuffer;
		/** */
		unsigned						_nDescsSansBuffer;
		/** */
		unsigned						_nextDescInBlock;
		/** */
		unsigned						_bufferSize;
		/** */
		unsigned						_nBuffsInBlock;
		/** */
		const bool						_interruptEveryBuffer;
		/** */
		bool							_initialized;
		/** */
		bool							_blockCurrentlyOpen;

	protected:
		/** */
		BufferDesc*						const _descriptors;

	public:
		/** */
		Driver(	BufferDesc					descriptors[],
				unsigned					nDescriptors,
				bool						interruptEveryBuffer
				) noexcept;
		/** */
		void		initialize() noexcept;
		/** */
		bool		openBlock() noexcept;
		/** */
		unsigned	getNextBuffer(uint8_t** buffer) noexcept;
		/** */
		void		closeBlock() noexcept;
		/** */
		void		cancelBlock() noexcept;
		/** */
		unsigned	minimumBufferSize() const noexcept;
		/** */
		unsigned	maximumBufferSize() const noexcept;
		/** */
		bool		giveBuffer(	uint8_t*		buffer,
								uint16_t	size
								) noexcept;
		/** */
		void		returnBuffer(uint8_t*	 buffer) noexcept;
		/** */
		void		reclaimBuffers(Oscl::Frame::Rx::Return& callback) noexcept;
		/** */
		unsigned	notEmptyDescIterate(	DescStatusMatch&	endConditon
											) noexcept;
		unsigned	currentDesc() const noexcept;

		/** */
		void		reuseBuffer(uint8_t* buffer) noexcept;

	private:
		/** */
		void		freeBuffer(uint8_t* buffer) noexcept;
		/** */
		uint8_t*		allocateBuffer() noexcept;
		/** */
		bool		attachToNeedyDesc(uint8_t* buffer) noexcept;
		/** */
		bool		descriptorHasNoBuffer(unsigned desc) noexcept;
		/** */
	};

}
}
}
}
}
#endif
