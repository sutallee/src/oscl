/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_mot_quicc_sdma_rx_rxbdh_
#define _oscl_drv_mot_quicc_sdma_rx_rxbdh_
#include "oscl/bits/bitfield.h"
#include "oscl/hw/motorola/quicc/sdma/bdreg.h"

/** */
namespace Oscl{
/** */
namespace Motorola {
/** */
namespace Quicc {
/** */
namespace Sdma {
/** */
namespace Rx {

/** */
class BufferDesc {
	public:
		/** */
		Oscl::BitField<Oscl::Motorola::Quicc::Sdma::RxBD::Status::Reg>	_cs;
		/** */
		uint16_t		_length;
		/** */
		void*			_buffer;

	public:
		/** */
		inline void	clearStatus() noexcept{
			using namespace Oscl::Motorola::Quicc::Sdma::RxBD::Status;
			_cs.clearBits(	(Reg)~(
									E::FieldMask
								|	W::FieldMask
								|	I::FieldMask
								|	CM::FieldMask
								)
							);
			}
		/** */
		inline bool	receiverDone() noexcept{
			using namespace Oscl::Motorola::Quicc::Sdma::RxBD::Status;
			return _cs.equal(E::FieldMask,E::ValueMask_Complete);
			}
		/** */
		inline void	empty() noexcept{
			using namespace Oscl::Motorola::Quicc::Sdma::RxBD::Status;
			_cs.changeBits(E::FieldMask,E::ValueMask_Empty);
			}
		/** */
		inline void	lastDescInRing() noexcept{
			using namespace Oscl::Motorola::Quicc::Sdma::RxBD::Status;
			_cs.changeBits(W::FieldMask,W::ValueMask_LastDesc);
			}
		/** */
		inline void	notLastDescInRing() noexcept{
			using namespace Oscl::Motorola::Quicc::Sdma::RxBD::Status;
			_cs.changeBits(W::FieldMask,W::ValueMask_NotLastDesc);
			}
		/** */
		inline void	enableInterrupt() noexcept{
			using namespace Oscl::Motorola::Quicc::Sdma::RxBD::Status;
			_cs.changeBits(I::FieldMask,I::ValueMask_Enable);
			}
		/** */
		inline void	disableInterrupt() noexcept{
			using namespace Oscl::Motorola::Quicc::Sdma::RxBD::Status;
			_cs.changeBits(I::FieldMask,I::ValueMask_Disable);
			}
	};

}
}
}
}
}
#endif

