/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "driver.h"

using namespace Oscl::Motorola::Quicc::Sdma::Frame::Tx;

Driver::Driver(Oscl::Motorola::Quicc::Sdma::Tx::Api& driver) noexcept:
		_driver(driver)
		{
	}

void	Driver::initialize() noexcept{
	_driver.initialize();
	}

void	Driver::processCompletedBuffers() noexcept{
	_driver.processDescriptors();
	}

unsigned	Driver::maxBuffersInFrame() noexcept{
	return _driver.nDescriptors();
	}

bool	Driver::resourcesAvailable(unsigned nBuffers) noexcept{
	if(nBuffers > _driver.nDescriptors()) return false;
	return _driver.nAvailableDescs() >= nBuffers;
	}

bool	Driver::open() noexcept{
	return _driver.openBlock();
	}

bool	Driver::append(const void* buffer,unsigned count) noexcept{
	return _driver.appendBufferToBlock((uint8_t*)buffer,count);
	}

void	Driver::close(Oscl::Done::Api& callback) noexcept{
	_driver.closeBlock(callback);
	}

void	Driver::cancel() noexcept{
	_driver.cancelBlock();
	}

