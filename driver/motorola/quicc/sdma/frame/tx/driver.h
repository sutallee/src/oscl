/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_mot_quicc_sdma_frame_tx_driverh_
#define _oscl_drv_mot_quicc_sdma_frame_tx_driverh_
#include "oscl/driver/motorola/quicc/sdma/tx/api.h"
#include "oscl/frame/tx/api.h"

/** */
namespace Oscl{
/** */
namespace Motorola {
/** */
namespace Quicc {
/** */
namespace Sdma {
/** */
namespace Frame {
/** */
namespace Tx {

/** */
class Driver : public Oscl::Frame::Tx::Api {
	private:
		/** */
		Oscl::Motorola::Quicc::Sdma::Tx::Api&	_driver;

	public:
		/** */
		Driver(Oscl::Motorola::Quicc::Sdma::Tx::Api& driver) noexcept;

		/** */
		void	initialize() noexcept;

	public:	// Oscl::Frame::Tx::Api
		/** This operation is invoked by the containing driver to
			locate descriptors that have completed, and to release
			their associated resources.
		 */
		void	processCompletedBuffers() noexcept;

		/** This operation returns the maximum number of buffers that can
			are allowed in a single frame by this transmitter.
		 */
		unsigned	maxBuffersInFrame() noexcept;

		/** This operation returns true if a frame with the specified number of 
			buffers can currently be accommodated by the transmitter.
		 */
		bool	resourcesAvailable(unsigned nBuffers) noexcept;

		/** This operation is invoked to initiate the transfer of a new
			frame to the transmitter.
		 */
		bool	open() noexcept;

		/** This operation is invoked repeatedly to append a buffer to
			the current frame. True is returned unless the transmitter
			has insufficient resources to accept the buffer. This condition
			can be detected before opening the frame by using the
			maxBuffersInFrame() operation and comparing against the
			client needs.
		 */
		bool	append(const void* buffer,unsigned count) noexcept;

		/** This operation completes the currently open frame and schedules
			it for transmission. The argument callback is associated with
			the frame and invoked when transmission of the frame is complete.
			After invoking this operation, the client relinquishes ownership
			of the buffer resources untill the callback is invoked.
		 */
		void	close(Oscl::Done::Api& callback) noexcept;

		/** This operation cancels construction of the currently opened
			frame. This operation may be invoked in lieu of close().
			The client retains ownership of all buffer resources.
		 */
		void	cancel() noexcept;
	};

}
}
}
}
}
}
#endif
