/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "driver.h"
#include "oscl/error/fatal.h"

class FrameStatus : public Oscl::Motorola::Quicc::Sdma::Rx::DescStatusMatch{
	public:
		bool	match(unsigned desc) noexcept;
	};

using namespace Oscl::Motorola::Quicc::Sdma::Frame::Rx;

Driver::Driver(	Oscl::Motorola::Quicc::Sdma::Rx::BufferDesc	descriptors[],
				unsigned									nDescriptors,
				DescErrorObserver&							errorObserver
				) noexcept:
		_driver(descriptors,nDescriptors,false),
		_descs(descriptors),
		_errorObserver(errorObserver),
		_nZeroLengthBuffersInDiscardLoop(0)
		{
	_stats.nBuffersDiscarded	= 0;
	_stats.nDiscardedFrames		= 0;
	}

void	Driver::initialize() noexcept{
	_driver.initialize();
	}

unsigned	Driver::minimumBufferSize() noexcept{
	return _driver.minimumBufferSize();
	}

unsigned	Driver::maximumBufferSize() noexcept{
	return _driver.maximumBufferSize();
	}

void	Driver::supplyBuffer(void* buffer,unsigned size) noexcept{
	_driver.giveBuffer((uint8_t*)buffer,size);
	}

unsigned	Driver::open() noexcept{
	unsigned	nBuffersInFrame;
	while(_driver.openBlock()){
		_discardFrame	= false;
		nBuffersInFrame	=  _driver.notEmptyDescIterate(*this);
		if(_discardFrame){
			discard();
			continue;
			}
		_octetsReceivedInFrame		= 0;
		_nBuffersToProcessInFrame	= nBuffersInFrame;
		return nBuffersInFrame;
		}
	_driver.cancelBlock();
	return 0;
	}

bool	Driver::operatingPromiscuously() noexcept{
	// FIXME:
	return false;
	}

bool	Driver::exactAddressMatch() noexcept{
	// FIXME: should only return true if the
	// M bit in the last BD of the current frame
	// is zero and only a single individual address
	// and/or broadcast address is being used.
	// Returns false if the hash table for group
	// addresses causes the match.
	return true;
	}

bool	Driver::promiscuousFrame() noexcept{
	// FIXME: should look at the current operating
	// mode of the ethernet framer and the M bit
	// in the last BD of the current frame.
	return false;
	}

unsigned	Driver::nextBuffer(void** buffer) noexcept{
	using namespace Oscl::Motorola::Quicc::Sdma::RxBD::Status;
//	if(!_nBuffersToProcessInFrame) return 0;
	unsigned length	= _driver.getNextBuffer((uint8_t**)buffer);
	if(!length) return 0;
	--_nBuffersToProcessInFrame;
	if(	_descs[_driver.currentDesc()]._cs.
		equal(Eof::L::FieldMask,Eof::L::ValueMask_LastDesc)
		){
		return length-_octetsReceivedInFrame;
		}
	_octetsReceivedInFrame	+= length;
	return length;
	}

void	Driver::close() noexcept{
	_nBuffersToProcessInFrame	= 0;
	_octetsReceivedInFrame	= 0;
	_driver.closeBlock();
	}

void	Driver::discard() noexcept{
	using namespace Oscl::Motorola::Quicc::Sdma::RxBD::Status;
	void*	buffer;
	++_stats.nDiscardedFrames;
	_nBuffersToProcessInFrame	= 0;
	_octetsReceivedInFrame	= 0;
	if(_driver.openBlock()){
		_driver.cancelBlock();
		return;
		}
	_driver.cancelBlock();
	if(!_driver.openBlock()){
		Oscl::ErrorFatal::logAndExit("Uh Oh!\n");
		return;
		}
	bool	lastDescInFrame;
	do {
		lastDescInFrame	=	_descs[_driver.currentDesc()]._cs.
									equal(	Eof::L::FieldMask,
											Eof::L::ValueMask_LastDesc
											);
		if(nextBuffer(&buffer)){
			++_stats.nBuffersDiscarded;
			_driver.reuseBuffer((uint8_t*)buffer);
			}
		else{
			++_nZeroLengthBuffersInDiscardLoop;
			// If descriptor is empty, there may
			// be a forever loop here! Should not
			// happen, so we assume a length issue
			// and let it slide.
			}
		} while(!lastDescInFrame);
	_driver.closeBlock();
	}

void	Driver::cancel() noexcept{
	_nBuffersToProcessInFrame	= 0;
	_octetsReceivedInFrame	= 0;
	_driver.cancelBlock();
	}

void	Driver::freeBuffer(void* buffer) noexcept{
	_driver.returnBuffer((uint8_t*) buffer);
	}

bool	Driver::match(unsigned desc) noexcept{
	using namespace Oscl::Motorola::Quicc::Sdma::RxBD::Status;
	_discardFrame	|= _errorObserver.hasError(desc);
	if(_descs[desc]._cs.equal(Eof::L::FieldMask,Eof::L::ValueMask_LastDesc)){
		return true;
		}
	return false;
	}

