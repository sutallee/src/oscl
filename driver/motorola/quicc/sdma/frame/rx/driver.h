/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_mot_quicc_sdma_frame_rx_driverh_
#define _oscl_drv_mot_quicc_sdma_frame_rx_driverh_
#include "oscl/driver/motorola/quicc/sdma/rx/driver.h"
#include "oscl/frame/rx/api.h"

/** */
namespace Oscl{
/** */
namespace Motorola {
/** */
namespace Quicc {
/** */
namespace Sdma {
/** */
namespace Frame {
/** */
namespace Rx {

/** */
class DescErrorObserver {
	public:
		/** Shut-up GCC. */
		virtual ~DescErrorObserver() {}
		/** This operation must return true if any error bits are
			set that should cause the frame to be discarded.
		 */
		virtual bool	hasError(unsigned descIndex) noexcept=0;
	};

/** */
class Driver :
	public Oscl::Motorola::Quicc::Sdma::Rx::DescStatusMatch,
	public Oscl::Frame::Rx::Api
	{
	private:
		/** */
		Oscl::Motorola::Quicc::Sdma::Rx::Driver			_driver;
		/** */
		Oscl::Motorola::Quicc::Sdma::Rx::BufferDesc*	const _descs;
		/** */
		DescErrorObserver&					_errorObserver;
		/** */
		unsigned							_octetsReceivedInFrame;
		/** */
		unsigned							_nBuffersToProcessInFrame;
		/** */
		unsigned							_nZeroLengthBuffersInDiscardLoop;

		/** */
		bool								_discardFrame;

		/** */
		struct {
			/** */
			unsigned long	nBuffersDiscarded;
			/** */
			unsigned long	nDiscardedFrames;
			} _stats;


	public:
		/** */
		Driver(	Oscl::Motorola::Quicc::Sdma::Rx::BufferDesc	descriptors[],
				unsigned									nDescriptors,
				DescErrorObserver&							errorObserver
				) noexcept;

		/** */
		void	initialize() noexcept;

	public:	// Oscl::Frame::Rx::Api
		/** This operation returns the minimum buffer size that can be
			accepted by this receiver.
		 */
		unsigned	minimumBufferSize() noexcept;

		/** This operation returns the largest size buffer that can
			be used by this receiver.
		 */
		unsigned	maximumBufferSize() noexcept;

		/** This operation is used during initialization to give empty
			buffers to the receiver. Buffers must meet the minimum
			and maximum buffer size requirements and must be long word
			aligned. Size requirements can be obtained via minimumBufferSize()
			and maximumBufferSize(). Failure to meet the buffer requirements
			is undefined, but implementations should invoke a system
			error handler.
		 */
		void	supplyBuffer(void* buffer,unsigned size) noexcept;

		/** This operation is invoked to begin processing of the
			next frame in the receive queue. The operation
			initializes the internal state of the receiver to
			prepare for processing the buffers contained in the
			frame. The operation counts the number of buffers
			in the current frame and returns the number as a
			result. If there is no complete frame pending, then
			the operation returns zero.
		 */
		unsigned	open() noexcept;

		/** This operation returns true if the framer is currently
			receiving promiscuously.
		 */
		bool	operatingPromiscuously() noexcept;

		/** This operation returns true if the destination address
			of the currently open frame *exactly* matches either the
			individual station address, broadcast address, or one of the
			group addresses for the framer interface.
		 */
		bool	exactAddressMatch() noexcept;

		/** This operation returns true if the currently open frame
			did not match any individual, broadcast, or group address
			and the framer is operating promiscuously.
		 */
		bool	promiscuousFrame() noexcept;

		/** This operation retrieves the next buffer in the currently
			opened frame and the number of valid octets in the buffer.
			If there are no further buffers in the current frame, the
			operation returns zero.
		 */
		unsigned	nextBuffer(void** buffer) noexcept;

		/** This operation indicates to the receiver that the client
			has completed processing the currently open frame. The
			implementation is then free to reclaim the resources
			used by the frame, and ownership of the buffers is
			relinquished to the client, which is then responsible
			for returning the buffers to the receiver when it is
			finished using them.
		 */
		void	close() noexcept;

		/** This operation indicates to the receiver that the client
			has decided to discard the contents of the currently
			open frame. The implementation reclaims all of the resources
			associated with the frame and frees the buffers to be used
			in subsequent frames. This operation is used in lieu of
			the close() operation.
		 */
		void	discard() noexcept;

		/** This operation is invoked to close the current frame without
			removing it from the receive queue. The implementation retains
			ownership of the receive buffers associated with this frame.
			This operation may be used in lieu of the close() operation
			when the client does not immediately have sufficient resources
			to receive the frame.
		 */
		void	cancel() noexcept;

		/** This operation is used during operation to return buffers
			to the receiver when the client has finished using them.
		 */
		void	freeBuffer(void* buffer) noexcept;

	private: // Oscl::Motorola::Quicc::Sdma::Rx::DescStatusMatch
		/** */
		bool	match(unsigned desc) noexcept;
	};

}
}
}
}
}
}
#endif
