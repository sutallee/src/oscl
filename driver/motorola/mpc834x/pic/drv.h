/*
   Copyright (C) 2006 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_motorola_mpc834x_pic_drvh_
#define _oscl_drv_motorola_mpc834x_pic_drvh_
#include "drvapi.h"
#include "irqdrv.h"

/** */
namespace Oscl {
/** */
namespace Motorola {
/** */
namespace MPC834x {
/** */
namespace PIC {

/** */
class Driver : public DrvApi {
	private:
		/** */
		InternalIrqDriverH<	Oscl::Motorola::MPC834x::PIC::SIMSR_H::UART1::Lsb,
							Oscl::Motorola::MPC834x::PIC::SIPNR_H::UART1::Lsb
							>	_uart1;
		/** */
		InternalIrqDriverH<	Oscl::Motorola::MPC834x::PIC::SIMSR_H::UART2::Lsb,
							Oscl::Motorola::MPC834x::PIC::SIPNR_H::UART2::Lsb
							>	_uart2;
		/** */
		InternalIrqDriverH<	Oscl::Motorola::MPC834x::PIC::SIMSR_H::SEC::Lsb,
							Oscl::Motorola::MPC834x::PIC::SIPNR_H::SEC::Lsb
							>	_sec;
		/** */
		InternalIrqDriverH<	Oscl::Motorola::MPC834x::PIC::SIMSR_H::I2C1::Lsb,
							Oscl::Motorola::MPC834x::PIC::SIPNR_H::I2C1::Lsb
							>	_i2c1;
		/** */
		InternalIrqDriverH<	Oscl::Motorola::MPC834x::PIC::SIMSR_H::I2C2::Lsb,
							Oscl::Motorola::MPC834x::PIC::SIPNR_H::I2C2::Lsb
							>	_i2c2;
		/** */
		InternalIrqDriverH<	Oscl::Motorola::MPC834x::PIC::SIMSR_H::SPI::Lsb,
							Oscl::Motorola::MPC834x::PIC::SIPNR_H::SPI::Lsb
							>	_spi;
		/** */
		ExternalIrqDriver<	Oscl::Motorola::MPC834x::PIC::SEMSR::IRQ1::Lsb,
							Oscl::Motorola::MPC834x::PIC::SEPNR::IRQ1::Lsb
							>	_irq1;
		/** */
		ExternalIrqDriver<	Oscl::Motorola::MPC834x::PIC::SEMSR::IRQ2::Lsb,
							Oscl::Motorola::MPC834x::PIC::SEPNR::IRQ2::Lsb
							>	_irq2;
		/** */
		ExternalIrqDriver<	Oscl::Motorola::MPC834x::PIC::SEMSR::IRQ3::Lsb,
							Oscl::Motorola::MPC834x::PIC::SEPNR::IRQ3::Lsb
							>	_irq3;
		/** */
		ExternalIrqDriver<	Oscl::Motorola::MPC834x::PIC::SEMSR::IRQ4::Lsb,
							Oscl::Motorola::MPC834x::PIC::SEPNR::IRQ4::Lsb
							>	_irq4;
		/** */
		ExternalIrqDriver<	Oscl::Motorola::MPC834x::PIC::SEMSR::IRQ5::Lsb,
							Oscl::Motorola::MPC834x::PIC::SEPNR::IRQ5::Lsb
							>	_irq5;
		/** */
		ExternalIrqDriver<	Oscl::Motorola::MPC834x::PIC::SEMSR::IRQ6::Lsb,
							Oscl::Motorola::MPC834x::PIC::SEPNR::IRQ6::Lsb
							>	_irq6;
		/** */
		ExternalIrqDriver<	Oscl::Motorola::MPC834x::PIC::SEMSR::IRQ7::Lsb,
							Oscl::Motorola::MPC834x::PIC::SEPNR::IRQ7::Lsb
							>	_irq7;
		/** */
		InternalIrqDriverH<	Oscl::Motorola::MPC834x::PIC::SIMSR_H::TSEC1Tx::Lsb,
							Oscl::Motorola::MPC834x::PIC::SIPNR_H::TSEC1Tx::Lsb
							>	_tsec1tx;
		/** */
		InternalIrqDriverH<	Oscl::Motorola::MPC834x::PIC::SIMSR_H::TSEC1Rx::Lsb,
							Oscl::Motorola::MPC834x::PIC::SIPNR_H::TSEC1Rx::Lsb
							>	_tsec1rx;
		/** */
		InternalIrqDriverH<	Oscl::Motorola::MPC834x::PIC::SIMSR_H::TSEC1Err::Lsb,
							Oscl::Motorola::MPC834x::PIC::SIPNR_H::TSEC1Err::Lsb
							>	_tsec1err;
		/** */
		InternalIrqDriverH<	Oscl::Motorola::MPC834x::PIC::SIMSR_H::TSEC2Tx::Lsb,
							Oscl::Motorola::MPC834x::PIC::SIPNR_H::TSEC2Tx::Lsb
							>	_tsec2tx;
		/** */
		InternalIrqDriverH<	Oscl::Motorola::MPC834x::PIC::SIMSR_H::TSEC2Rx::Lsb,
							Oscl::Motorola::MPC834x::PIC::SIPNR_H::TSEC2Rx::Lsb
							>	_tsec2rx;
		/** */
		InternalIrqDriverH<	Oscl::Motorola::MPC834x::PIC::SIMSR_H::TSEC2Err::Lsb,
							Oscl::Motorola::MPC834x::PIC::SIPNR_H::TSEC2Err::Lsb
							>	_tsec2err;
		/** */
		InternalIrqDriverH<	Oscl::Motorola::MPC834x::PIC::SIMSR_H::USB2DR::Lsb,
							Oscl::Motorola::MPC834x::PIC::SIPNR_H::USB2DR::Lsb
							>	_usb2dr;
		/** */
		InternalIrqDriverH<	Oscl::Motorola::MPC834x::PIC::SIMSR_H::USB2MPH::Lsb,
							Oscl::Motorola::MPC834x::PIC::SIPNR_H::USB2MPH::Lsb
							>	_usb2mph;
		/** */
		ExternalIrqDriver<	Oscl::Motorola::MPC834x::PIC::SEMSR::IRQ0::Lsb,
							Oscl::Motorola::MPC834x::PIC::SEPNR::IRQ0::Lsb
							>	_irq0;
		/** */
		InternalIrqDriverL<	Oscl::Motorola::MPC834x::PIC::SIMSR_L::RTCSEC::Lsb,
							Oscl::Motorola::MPC834x::PIC::SIPNR_L::RTCSEC::Lsb
							>	_rtcsec;
		/** */
		InternalIrqDriverL<	Oscl::Motorola::MPC834x::PIC::SIMSR_L::PIT::Lsb,
							Oscl::Motorola::MPC834x::PIC::SIPNR_L::PIT::Lsb
							>	_pit;
		/** */
		InternalIrqDriverL<	Oscl::Motorola::MPC834x::PIC::SIMSR_L::PCI1::Lsb,
							Oscl::Motorola::MPC834x::PIC::SIPNR_L::PCI1::Lsb
							>	_pci1;
		/** */
		InternalIrqDriverL<	Oscl::Motorola::MPC834x::PIC::SIMSR_L::PCI2::Lsb,
							Oscl::Motorola::MPC834x::PIC::SIPNR_L::PCI2::Lsb
							>	_pci2;
		/** */
		InternalIrqDriverL<	Oscl::Motorola::MPC834x::PIC::SIMSR_L::RTCALR::Lsb,
							Oscl::Motorola::MPC834x::PIC::SIPNR_L::RTCALR::Lsb
							>	_rtcalr;
		/** */
		InternalIrqDriverL<	Oscl::Motorola::MPC834x::PIC::SIMSR_L::MU::Lsb,
							Oscl::Motorola::MPC834x::PIC::SIPNR_L::MU::Lsb
							>	_mu;
		/** */
		InternalIrqDriverL<	Oscl::Motorola::MPC834x::PIC::SIMSR_L::SBA::Lsb,
							Oscl::Motorola::MPC834x::PIC::SIPNR_L::SBA::Lsb
							>	_sba;
		/** */
		InternalIrqDriverL<	Oscl::Motorola::MPC834x::PIC::SIMSR_L::DMA::Lsb,
							Oscl::Motorola::MPC834x::PIC::SIPNR_L::DMA::Lsb
							>	_dma;
		/** */
		InternalIrqDriverL<	Oscl::Motorola::MPC834x::PIC::SIMSR_L::GTM4::Lsb,
							Oscl::Motorola::MPC834x::PIC::SIPNR_L::GTM4::Lsb
							>	_gtm4;
		/** */
		InternalIrqDriverL<	Oscl::Motorola::MPC834x::PIC::SIMSR_L::GTM8::Lsb,
							Oscl::Motorola::MPC834x::PIC::SIPNR_L::GTM8::Lsb
							>	_gtm8;
		/** */
		InternalIrqDriverL<	Oscl::Motorola::MPC834x::PIC::SIMSR_L::GPIO1::Lsb,
							Oscl::Motorola::MPC834x::PIC::SIPNR_L::GPIO1::Lsb
							>	_gpio1;
		/** */
		InternalIrqDriverL<	Oscl::Motorola::MPC834x::PIC::SIMSR_L::GPIO2::Lsb,
							Oscl::Motorola::MPC834x::PIC::SIPNR_L::GPIO2::Lsb
							>	_gpio2;
		/** */
		InternalIrqDriverL<	Oscl::Motorola::MPC834x::PIC::SIMSR_L::DDR::Lsb,
							Oscl::Motorola::MPC834x::PIC::SIPNR_L::DDR::Lsb
							>	_ddr;
		/** */
		InternalIrqDriverL<	Oscl::Motorola::MPC834x::PIC::SIMSR_L::LBC::Lsb,
							Oscl::Motorola::MPC834x::PIC::SIPNR_L::LBC::Lsb
							>	_lbc;
		/** */
		InternalIrqDriverL<	Oscl::Motorola::MPC834x::PIC::SIMSR_L::GTM2::Lsb,
							Oscl::Motorola::MPC834x::PIC::SIPNR_L::GTM2::Lsb
							>	_gtm2;
		/** */
		InternalIrqDriverL<	Oscl::Motorola::MPC834x::PIC::SIMSR_L::GTM6::Lsb,
							Oscl::Motorola::MPC834x::PIC::SIPNR_L::GTM6::Lsb
							>	_gtm6;
		/** */
		InternalIrqDriverL<	Oscl::Motorola::MPC834x::PIC::SIMSR_L::PMC::Lsb,
							Oscl::Motorola::MPC834x::PIC::SIPNR_L::PMC::Lsb
							>	_pmc;
		/** */
		InternalIrqDriverL<	Oscl::Motorola::MPC834x::PIC::SIMSR_L::GTM3::Lsb,
							Oscl::Motorola::MPC834x::PIC::SIPNR_L::GTM3::Lsb
							>	_gtm3;
		/** */
		InternalIrqDriverL<	Oscl::Motorola::MPC834x::PIC::SIMSR_L::GTM7::Lsb,
							Oscl::Motorola::MPC834x::PIC::SIPNR_L::GTM7::Lsb
							>	_gtm7;
		/** */
		InternalIrqDriverL<	Oscl::Motorola::MPC834x::PIC::SIMSR_L::GTM1::Lsb,
							Oscl::Motorola::MPC834x::PIC::SIPNR_L::GTM1::Lsb
							>	_gtm1;
		/** */
		InternalIrqDriverL<	Oscl::Motorola::MPC834x::PIC::SIMSR_L::GTM5::Lsb,
							Oscl::Motorola::MPC834x::PIC::SIPNR_L::GTM5::Lsb
							>	_gtm5;

	public:
		/** */
		Driver(Oscl::Motorola::MPC834x::PIC::RegisterMap& picMap) noexcept;

	private:	// PIC::DrvApi
		/** */
		Oscl::Motorola::MPC834x::PIC::IrqDrvApi&	getUART1() noexcept;
		/** */
		Oscl::Motorola::MPC834x::PIC::IrqDrvApi&	getUART2() noexcept;
		/** */
		Oscl::Motorola::MPC834x::PIC::IrqDrvApi&	getSEC() noexcept;
		/** */
		Oscl::Motorola::MPC834x::PIC::IrqDrvApi&	getI2C1() noexcept;
		/** */
		Oscl::Motorola::MPC834x::PIC::IrqDrvApi&	getI2C2() noexcept;
		/** */
		Oscl::Motorola::MPC834x::PIC::IrqDrvApi&	getSPI() noexcept;
		/** */
		Oscl::Motorola::MPC834x::PIC::IrqDrvApi&	getIRQ1() noexcept;
		/** */
		Oscl::Motorola::MPC834x::PIC::IrqDrvApi&	getIRQ2() noexcept;
		/** */
		Oscl::Motorola::MPC834x::PIC::IrqDrvApi&	getIRQ3() noexcept;
		/** */
		Oscl::Motorola::MPC834x::PIC::IrqDrvApi&	getIRQ4() noexcept;
		/** */
		Oscl::Motorola::MPC834x::PIC::IrqDrvApi&	getIRQ5() noexcept;
		/** */
		Oscl::Motorola::MPC834x::PIC::IrqDrvApi&	getIRQ6() noexcept;
		/** */
		Oscl::Motorola::MPC834x::PIC::IrqDrvApi&	getIRQ7() noexcept;
		/** */
		Oscl::Motorola::MPC834x::PIC::IrqDrvApi&	getTSEC1Tx() noexcept;
		/** */
		Oscl::Motorola::MPC834x::PIC::IrqDrvApi&	getTSEC1Rx() noexcept;
		/** */
		Oscl::Motorola::MPC834x::PIC::IrqDrvApi&	getTSEC1Err() noexcept;
		/** */
		Oscl::Motorola::MPC834x::PIC::IrqDrvApi&	getTSEC2Tx() noexcept;
		/** */
		Oscl::Motorola::MPC834x::PIC::IrqDrvApi&	getTSEC2Rx() noexcept;
		/** */
		Oscl::Motorola::MPC834x::PIC::IrqDrvApi&	getTSEC2Err() noexcept;
		/** */
		Oscl::Motorola::MPC834x::PIC::IrqDrvApi&	getUSB2DR() noexcept;
		/** */
		Oscl::Motorola::MPC834x::PIC::IrqDrvApi&	getUSB2MPH() noexcept;
		/** */
		Oscl::Motorola::MPC834x::PIC::IrqDrvApi&	getIRQ0() noexcept;
		/** */
		Oscl::Motorola::MPC834x::PIC::IrqDrvApi&	getRTCSEC() noexcept;
		/** */
		Oscl::Motorola::MPC834x::PIC::IrqDrvApi&	getPIT() noexcept;
		/** */
		Oscl::Motorola::MPC834x::PIC::IrqDrvApi&	getPCI1() noexcept;
		/** */
		Oscl::Motorola::MPC834x::PIC::IrqDrvApi&	getPCI2() noexcept;
		/** */
		Oscl::Motorola::MPC834x::PIC::IrqDrvApi&	getRTCALR() noexcept;
		/** */
		Oscl::Motorola::MPC834x::PIC::IrqDrvApi&	getMU() noexcept;
		/** */
		Oscl::Motorola::MPC834x::PIC::IrqDrvApi&	getSBA() noexcept;
		/** */
		Oscl::Motorola::MPC834x::PIC::IrqDrvApi&	getDMA() noexcept;
		/** */
		Oscl::Motorola::MPC834x::PIC::IrqDrvApi&	getGTM4() noexcept;
		/** */
		Oscl::Motorola::MPC834x::PIC::IrqDrvApi&	getGTM8() noexcept;
		/** */
		Oscl::Motorola::MPC834x::PIC::IrqDrvApi&	getGPIO1() noexcept;
		/** */
		Oscl::Motorola::MPC834x::PIC::IrqDrvApi&	getGPIO2() noexcept;
		/** */
		Oscl::Motorola::MPC834x::PIC::IrqDrvApi&	getDDR() noexcept;
		/** */
		Oscl::Motorola::MPC834x::PIC::IrqDrvApi&	getLBC() noexcept;
		/** */
		Oscl::Motorola::MPC834x::PIC::IrqDrvApi&	getGTM2() noexcept;
		/** */
		Oscl::Motorola::MPC834x::PIC::IrqDrvApi&	getGTM6() noexcept;
		/** */
		Oscl::Motorola::MPC834x::PIC::IrqDrvApi&	getPMC() noexcept;
		/** */
		Oscl::Motorola::MPC834x::PIC::IrqDrvApi&	getGTM3() noexcept;
		/** */
		Oscl::Motorola::MPC834x::PIC::IrqDrvApi&	getGTM7() noexcept;
		/** */
		Oscl::Motorola::MPC834x::PIC::IrqDrvApi&	getGTM1() noexcept;
		/** */
		Oscl::Motorola::MPC834x::PIC::IrqDrvApi&	getGTM5() noexcept;

		/** */
		Oscl::Interrupt::EdgeApi&	getIRQ0EdgeApi() noexcept;
		/** */
		Oscl::Interrupt::EdgeApi&	getIRQ1EdgeApi() noexcept;
		/** */
		Oscl::Interrupt::EdgeApi&	getIRQ2EdgeApi() noexcept;
		/** */
		Oscl::Interrupt::EdgeApi&	getIRQ3EdgeApi() noexcept;
		/** */
		Oscl::Interrupt::EdgeApi&	getIRQ4EdgeApi() noexcept;
		/** */
		Oscl::Interrupt::EdgeApi&	getIRQ5EdgeApi() noexcept;
		/** */
		Oscl::Interrupt::EdgeApi&	getIRQ6EdgeApi() noexcept;
		/** */
		Oscl::Interrupt::EdgeApi&	getIRQ7EdgeApi() noexcept;
	};

}
}
}
}

#endif
