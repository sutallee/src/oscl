/*
   Copyright (C) 2006 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "drv.h"

using namespace Oscl::Motorola::MPC834x::PIC;

Driver::Driver(Oscl::Motorola::MPC834x::PIC::RegisterMap& picMap) noexcept:
		_uart1(picMap),
		_uart2(picMap),
		_sec(picMap),
		_i2c1(picMap),
		_i2c2(picMap),
		_spi(picMap),
		_irq1(picMap),
		_irq2(picMap),
		_irq3(picMap),
		_irq4(picMap),
		_irq5(picMap),
		_irq6(picMap),
		_irq7(picMap),
		_tsec1tx(picMap),
		_tsec1rx(picMap),
		_tsec1err(picMap),
		_tsec2tx(picMap),
		_tsec2rx(picMap),
		_tsec2err(picMap),
		_usb2dr(picMap),
		_usb2mph(picMap),
		_irq0(picMap),
		_rtcsec(picMap),
		_pit(picMap),
		_pci1(picMap),
		_pci2(picMap),
		_rtcalr(picMap),
		_mu(picMap),
		_sba(picMap),
		_dma(picMap),
		_gtm4(picMap),
		_gtm8(picMap),
		_gpio1(picMap),
		_gpio2(picMap),
		_ddr(picMap),
		_lbc(picMap),
		_gtm2(picMap),
		_gtm6(picMap),
		_pmc(picMap),
		_gtm3(picMap),
		_gtm7(picMap),
		_gtm1(picMap),
		_gtm5(picMap)
		{
	}

Oscl::Motorola::MPC834x::PIC::IrqDrvApi&	Driver::getUART1() noexcept{
	return _uart1;
	}

Oscl::Motorola::MPC834x::PIC::IrqDrvApi&	Driver::getUART2() noexcept{
	return _uart2;
	}

Oscl::Motorola::MPC834x::PIC::IrqDrvApi&	Driver::getSEC() noexcept{
	return _sec;
	}

Oscl::Motorola::MPC834x::PIC::IrqDrvApi&	Driver::getI2C1() noexcept{
	return _i2c1;
	}

Oscl::Motorola::MPC834x::PIC::IrqDrvApi&	Driver::getI2C2() noexcept{
	return _i2c2;
	}

Oscl::Motorola::MPC834x::PIC::IrqDrvApi&	Driver::getSPI() noexcept{
	return _spi;
	}

Oscl::Motorola::MPC834x::PIC::IrqDrvApi&	Driver::getIRQ1() noexcept{
	return _irq1;
	}

Oscl::Motorola::MPC834x::PIC::IrqDrvApi&	Driver::getIRQ2() noexcept{
	return _irq2;
	}

Oscl::Motorola::MPC834x::PIC::IrqDrvApi&	Driver::getIRQ3() noexcept{
	return _irq3;
	}

Oscl::Motorola::MPC834x::PIC::IrqDrvApi&	Driver::getIRQ4() noexcept{
	return _irq4;
	}

Oscl::Motorola::MPC834x::PIC::IrqDrvApi&	Driver::getIRQ5() noexcept{
	return _irq5;
	}

Oscl::Motorola::MPC834x::PIC::IrqDrvApi&	Driver::getIRQ6() noexcept{
	return _irq6;
	}

Oscl::Motorola::MPC834x::PIC::IrqDrvApi&	Driver::getIRQ7() noexcept{
	return _irq7;
	}

Oscl::Motorola::MPC834x::PIC::IrqDrvApi&	Driver::getTSEC1Tx() noexcept{
	return _tsec1tx;
	}

Oscl::Motorola::MPC834x::PIC::IrqDrvApi&	Driver::getTSEC1Rx() noexcept{
	return _tsec1rx;
	}

Oscl::Motorola::MPC834x::PIC::IrqDrvApi&	Driver::getTSEC1Err() noexcept{
	return _tsec1err;
	}

Oscl::Motorola::MPC834x::PIC::IrqDrvApi&	Driver::getTSEC2Tx() noexcept{
	return _tsec2tx;
	}

Oscl::Motorola::MPC834x::PIC::IrqDrvApi&	Driver::getTSEC2Rx() noexcept{
	return _tsec2rx;
	}

Oscl::Motorola::MPC834x::PIC::IrqDrvApi&	Driver::getTSEC2Err() noexcept{
	return _tsec2err;
	}

Oscl::Motorola::MPC834x::PIC::IrqDrvApi&	Driver::getUSB2DR() noexcept{
	return _usb2dr;
	}

Oscl::Motorola::MPC834x::PIC::IrqDrvApi&	Driver::getUSB2MPH() noexcept{
	return _usb2mph;
	}

Oscl::Motorola::MPC834x::PIC::IrqDrvApi&	Driver::getIRQ0() noexcept{
	return _irq0;
	}

Oscl::Motorola::MPC834x::PIC::IrqDrvApi&	Driver::getRTCSEC() noexcept{
	return _rtcsec;
	}

Oscl::Motorola::MPC834x::PIC::IrqDrvApi&	Driver::getPIT() noexcept{
	return _pit;
	}

Oscl::Motorola::MPC834x::PIC::IrqDrvApi&	Driver::getPCI1() noexcept{
	return _pci1;
	}

Oscl::Motorola::MPC834x::PIC::IrqDrvApi&	Driver::getPCI2() noexcept{
	return _pci2;
	}

Oscl::Motorola::MPC834x::PIC::IrqDrvApi&	Driver::getRTCALR() noexcept{
	return _rtcalr;
	}

Oscl::Motorola::MPC834x::PIC::IrqDrvApi&	Driver::getMU() noexcept{
	return _mu;
	}

Oscl::Motorola::MPC834x::PIC::IrqDrvApi&	Driver::getSBA() noexcept{
	return _sba;
	}

Oscl::Motorola::MPC834x::PIC::IrqDrvApi&	Driver::getDMA() noexcept{
	return _dma;
	}

Oscl::Motorola::MPC834x::PIC::IrqDrvApi&	Driver::getGTM4() noexcept{
	return _gtm4;
	}

Oscl::Motorola::MPC834x::PIC::IrqDrvApi&	Driver::getGTM8() noexcept{
	return _gtm8;
	}

Oscl::Motorola::MPC834x::PIC::IrqDrvApi&	Driver::getGPIO1() noexcept{
	return _gpio1;
	}

Oscl::Motorola::MPC834x::PIC::IrqDrvApi&	Driver::getGPIO2() noexcept{
	return _gpio2;
	}

Oscl::Motorola::MPC834x::PIC::IrqDrvApi&	Driver::getDDR() noexcept{
	return _ddr;
	}

Oscl::Motorola::MPC834x::PIC::IrqDrvApi&	Driver::getLBC() noexcept{
	return _lbc;
	}

Oscl::Motorola::MPC834x::PIC::IrqDrvApi&	Driver::getGTM2() noexcept{
	return _gtm2;
	}

Oscl::Motorola::MPC834x::PIC::IrqDrvApi&	Driver::getGTM6() noexcept{
	return _gtm6;
	}

Oscl::Motorola::MPC834x::PIC::IrqDrvApi&	Driver::getPMC() noexcept{
	return _pmc;
	}

Oscl::Motorola::MPC834x::PIC::IrqDrvApi&	Driver::getGTM3() noexcept{
	return _gtm3;
	}

Oscl::Motorola::MPC834x::PIC::IrqDrvApi&	Driver::getGTM7() noexcept{
	return _gtm7;
	}

Oscl::Motorola::MPC834x::PIC::IrqDrvApi&	Driver::getGTM1() noexcept{
	return _gtm1;
	}

Oscl::Motorola::MPC834x::PIC::IrqDrvApi&	Driver::getGTM5() noexcept{
	return _gtm5;
	}

Oscl::Interrupt::EdgeApi&	Driver::getIRQ0EdgeApi() noexcept{
	return _irq0.getEdgeApi();
	}

Oscl::Interrupt::EdgeApi&	Driver::getIRQ1EdgeApi() noexcept{
	return _irq1.getEdgeApi();
	}

Oscl::Interrupt::EdgeApi&	Driver::getIRQ2EdgeApi() noexcept{
	return _irq2.getEdgeApi();
	}

Oscl::Interrupt::EdgeApi&	Driver::getIRQ3EdgeApi() noexcept{
	return _irq3.getEdgeApi();
	}

Oscl::Interrupt::EdgeApi&	Driver::getIRQ4EdgeApi() noexcept{
	return _irq4.getEdgeApi();
	}

Oscl::Interrupt::EdgeApi&	Driver::getIRQ5EdgeApi() noexcept{
	return _irq5.getEdgeApi();
	}

Oscl::Interrupt::EdgeApi&	Driver::getIRQ6EdgeApi() noexcept{
	return _irq6.getEdgeApi();
	}

Oscl::Interrupt::EdgeApi&	Driver::getIRQ7EdgeApi() noexcept{
	return _irq7.getEdgeApi();
	}
