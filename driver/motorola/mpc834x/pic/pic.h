/*
   Copyright (C) 2006 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_motorola_mpc834x_pic_pich_
#define _oscl_drv_motorola_mpc834x_pic_pich_
#include "picapi.h"
#include "irqapi.h"
#include "oscl/hw/motorola/mpc834x/picreg.h"

/** */
namespace Oscl {
/** */
namespace Motorola {
/** */
namespace MPC834x {
/** */
namespace PIC {

/** The MPC834x actually has three priority interrupt controllers: one each
	for the INT, SMI, and CINT interrupts.
	This class encompasses both since they have the same interface.
 */
class PIC : public PicApi{
	private:
		class NoInterrupt : public Oscl::Motorola::MPC834x::PIC::IrqApi {
			public:
				/** */
				explicit NoInterrupt() noexcept{}

			private:	// Oscl::Motorola::MPC834x::PIC::IrqApi
				/** */
				void	reserve(	Oscl::Interrupt::
									StatusHandlerApi&	handler
									) noexcept{}
				/** */
				void	release() noexcept{}

			private:	// Oscl::Interrupt::StatusHandlerApi
				/** */
				bool	interrupt() noexcept{
					return true;
					}
			};
	private:
		/** */
		volatile Oscl::Motorola::MPC834x::PIC::SVCR::Reg&	_svcr;
		/** */
		NoInterrupt											_noInterrupt;
		/** */
		enum{maxVectors=128};
		/** */
		IrqApi*											_handler[maxVectors];

	public:
		/** */
		PIC(	volatile Oscl::Motorola::MPC834x::PIC::SVCR::Reg&	svcr,
				IrqApi&												unusedVector
				) noexcept;

	public:	// PicApi
		/** */
		void	reserve(	unsigned vector,
							Oscl::Interrupt::StatusHandlerApi&	handler
							) noexcept;
		/** */
		void	release(unsigned vector) noexcept;

		/** */
		IrqApi&	getIrqApi(unsigned vector) noexcept;

		/** */
		bool	interrupt() noexcept;

		/** Rather than pass individual IrqApi's through the constructor,
			this operation is called post constructor to populate the
			individual vectors. The reason this is twofold:
			1) It reduces the amount of stack space by the constructor.
			2) The vector table is usually only sparsely populated, thus
			   an single invalid vector can be used to populate the
			   unused vectors.
		 */
		void	setIrqApi(IrqApi& irqApi,unsigned vector) noexcept;

	};

}
}
}
}

#endif
