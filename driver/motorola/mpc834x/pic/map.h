/*
   Copyright (C) 2006 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_motorola_mpc834x_pic_maph_
#define _oscl_drv_motorola_mpc834x_pic_maph_
#include "api.h"
#include "picapi.h"
#include "interrupt.h"
#include "oscl/driver/motorola/mpc834x/pic/picapi.h"

/** */
namespace Oscl {
/** */
namespace Motorola {
/** */
namespace MPC834x {
/** */
namespace PIC {

/** */
class Map : public Api {
	private:
		/** */
		Oscl::Motorola::MPC834x::PIC::PicApi&	_pic;

		/** */
		Oscl::Motorola::MPC834x::PIC::Interrupt		_irqUart1;
		/** */
		Oscl::Motorola::MPC834x::PIC::Interrupt		_irqUart2;
		/** */
		Oscl::Motorola::MPC834x::PIC::Interrupt		_irqSec;
		/** */
		Oscl::Motorola::MPC834x::PIC::Interrupt		_irqI2c1;
		/** */
		Oscl::Motorola::MPC834x::PIC::Interrupt		_irqI2c2;
		/** */
		Oscl::Motorola::MPC834x::PIC::Interrupt		_irqSpi;
		/** */
		Oscl::Motorola::MPC834x::PIC::Interrupt		_irqIrq1;
		/** */
		Oscl::Motorola::MPC834x::PIC::Interrupt		_irqIrq2;
		/** */
		Oscl::Motorola::MPC834x::PIC::Interrupt		_irqIrq3;
		/** */
		Oscl::Motorola::MPC834x::PIC::Interrupt		_irqIrq4;
		/** */
		Oscl::Motorola::MPC834x::PIC::Interrupt		_irqIrq5;
		/** */
		Oscl::Motorola::MPC834x::PIC::Interrupt		_irqIrq6;
		/** */
		Oscl::Motorola::MPC834x::PIC::Interrupt		_irqIrq7;
		/** */
		Oscl::Motorola::MPC834x::PIC::Interrupt		_irqTsec1Tx;
		/** */
		Oscl::Motorola::MPC834x::PIC::Interrupt		_irqTsec1Rx;
		/** */
		Oscl::Motorola::MPC834x::PIC::Interrupt		_irqTsec1Err;
		/** */
		Oscl::Motorola::MPC834x::PIC::Interrupt		_irqTsec2Tx;
		/** */
		Oscl::Motorola::MPC834x::PIC::Interrupt		_irqTsec2Rx;
		/** */
		Oscl::Motorola::MPC834x::PIC::Interrupt		_irqTsec2Err;
		/** */
		Oscl::Motorola::MPC834x::PIC::Interrupt		_irqUsb2DR;
		/** */
		Oscl::Motorola::MPC834x::PIC::Interrupt		_irqUsb2MPH;
		/** */
		Oscl::Motorola::MPC834x::PIC::Interrupt		_irqIrq0;
		/** */
		Oscl::Motorola::MPC834x::PIC::Interrupt		_irqRtcSec;
		/** */
		Oscl::Motorola::MPC834x::PIC::Interrupt		_irqPit;
		/** */
		Oscl::Motorola::MPC834x::PIC::Interrupt		_irqPci1;
		/** */
		Oscl::Motorola::MPC834x::PIC::Interrupt		_irqPci2;
		/** */
		Oscl::Motorola::MPC834x::PIC::Interrupt		_irqRtcAlr;
		/** */
		Oscl::Motorola::MPC834x::PIC::Interrupt		_irqMu;
		/** */
		Oscl::Motorola::MPC834x::PIC::Interrupt		_irqSba;
		/** */
		Oscl::Motorola::MPC834x::PIC::Interrupt		_irqDma;
		/** */
		Oscl::Motorola::MPC834x::PIC::Interrupt		_irqGtm4;
		/** */
		Oscl::Motorola::MPC834x::PIC::Interrupt		_irqGtm8;
		/** */
		Oscl::Motorola::MPC834x::PIC::Interrupt		_irqGpio1;
		/** */
		Oscl::Motorola::MPC834x::PIC::Interrupt		_irqGpio2;
		/** */
		Oscl::Motorola::MPC834x::PIC::Interrupt		_irqDdr;
		/** */
		Oscl::Motorola::MPC834x::PIC::Interrupt		_irqLbc;
		/** */
		Oscl::Motorola::MPC834x::PIC::Interrupt		_irqGtm2;
		/** */
		Oscl::Motorola::MPC834x::PIC::Interrupt		_irqGtm6;
		/** */
		Oscl::Motorola::MPC834x::PIC::Interrupt		_irqPmc;
		/** */
		Oscl::Motorola::MPC834x::PIC::Interrupt		_irqGtm3;
		/** */
		Oscl::Motorola::MPC834x::PIC::Interrupt		_irqGtm7;
		/** */
		Oscl::Motorola::MPC834x::PIC::Interrupt		_irqGtm1;
		/** */
		Oscl::Motorola::MPC834x::PIC::Interrupt		_irqGtm5;

	public:
		/** */
		Map(Oscl::Motorola::MPC834x::PIC::PicApi& pic) noexcept;

	private:	// PIC::Api
		/** */
		Oscl::Interrupt::StatusHandlerApi&	getISR() noexcept;
		/** */
		Oscl::Motorola::MPC834x::PIC::IrqApi&	getUART1() noexcept;
		/** */
		Oscl::Motorola::MPC834x::PIC::IrqApi&	getUART2() noexcept;
		/** */
		Oscl::Motorola::MPC834x::PIC::IrqApi&	getSEC() noexcept;
		/** */
		Oscl::Motorola::MPC834x::PIC::IrqApi&	getI2C1() noexcept;
		/** */
		Oscl::Motorola::MPC834x::PIC::IrqApi&	getI2C2() noexcept;
		/** */
		Oscl::Motorola::MPC834x::PIC::IrqApi&	getSPI() noexcept;
		/** */
		Oscl::Motorola::MPC834x::PIC::IrqApi&	getIRQ1() noexcept;
		/** */
		Oscl::Motorola::MPC834x::PIC::IrqApi&	getIRQ2() noexcept;
		/** */
		Oscl::Motorola::MPC834x::PIC::IrqApi&	getIRQ3() noexcept;
		/** */
		Oscl::Motorola::MPC834x::PIC::IrqApi&	getIRQ4() noexcept;
		/** */
		Oscl::Motorola::MPC834x::PIC::IrqApi&	getIRQ5() noexcept;
		/** */
		Oscl::Motorola::MPC834x::PIC::IrqApi&	getIRQ6() noexcept;
		/** */
		Oscl::Motorola::MPC834x::PIC::IrqApi&	getIRQ7() noexcept;
		/** */
		Oscl::Motorola::MPC834x::PIC::IrqApi&	getTSEC1Tx() noexcept;
		/** */
		Oscl::Motorola::MPC834x::PIC::IrqApi&	getTSEC1Rx() noexcept;
		/** */
		Oscl::Motorola::MPC834x::PIC::IrqApi&	getTSEC1Err() noexcept;
		/** */
		Oscl::Motorola::MPC834x::PIC::IrqApi&	getTSEC2Tx() noexcept;
		/** */
		Oscl::Motorola::MPC834x::PIC::IrqApi&	getTSEC2Rx() noexcept;
		/** */
		Oscl::Motorola::MPC834x::PIC::IrqApi&	getTSEC2Err() noexcept;
		/** */
		Oscl::Motorola::MPC834x::PIC::IrqApi&	getUSB2DR() noexcept;
		/** */
		Oscl::Motorola::MPC834x::PIC::IrqApi&	getUSB2MPH() noexcept;
		/** */
		Oscl::Motorola::MPC834x::PIC::IrqApi&	getIRQ0() noexcept;
		/** */
		Oscl::Motorola::MPC834x::PIC::IrqApi&	getRTCSEC() noexcept;
		/** */
		Oscl::Motorola::MPC834x::PIC::IrqApi&	getPIT() noexcept;
		/** */
		Oscl::Motorola::MPC834x::PIC::IrqApi&	getPCI1() noexcept;
		/** */
		Oscl::Motorola::MPC834x::PIC::IrqApi&	getPCI2() noexcept;
		/** */
		Oscl::Motorola::MPC834x::PIC::IrqApi&	getRTCALR() noexcept;
		/** */
		Oscl::Motorola::MPC834x::PIC::IrqApi&	getMU() noexcept;
		/** */
		Oscl::Motorola::MPC834x::PIC::IrqApi&	getSBA() noexcept;
		/** */
		Oscl::Motorola::MPC834x::PIC::IrqApi&	getDMA() noexcept;
		/** */
		Oscl::Motorola::MPC834x::PIC::IrqApi&	getGTM4() noexcept;
		/** */
		Oscl::Motorola::MPC834x::PIC::IrqApi&	getGTM8() noexcept;
		/** */
		Oscl::Motorola::MPC834x::PIC::IrqApi&	getGPIO1() noexcept;
		/** */
		Oscl::Motorola::MPC834x::PIC::IrqApi&	getGPIO2() noexcept;
		/** */
		Oscl::Motorola::MPC834x::PIC::IrqApi&	getDDR() noexcept;
		/** */
		Oscl::Motorola::MPC834x::PIC::IrqApi&	getLBC() noexcept;
		/** */
		Oscl::Motorola::MPC834x::PIC::IrqApi&	getGTM2() noexcept;
		/** */
		Oscl::Motorola::MPC834x::PIC::IrqApi&	getGTM6() noexcept;
		/** */
		Oscl::Motorola::MPC834x::PIC::IrqApi&	getPMC() noexcept;
		/** */
		Oscl::Motorola::MPC834x::PIC::IrqApi&	getGTM3() noexcept;
		/** */
		Oscl::Motorola::MPC834x::PIC::IrqApi&	getGTM7() noexcept;
		/** */
		Oscl::Motorola::MPC834x::PIC::IrqApi&	getGTM1() noexcept;
		/** */
		Oscl::Motorola::MPC834x::PIC::IrqApi&	getGTM5() noexcept;
	};

}
}
}
}

#endif
