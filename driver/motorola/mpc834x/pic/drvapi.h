/*
   Copyright (C) 2006 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_motorola_mpc834x_pic_drvapih_
#define _oscl_drv_motorola_mpc834x_pic_drvapih_
#include "irqdrvapi.h"
#include "oscl/interrupt/edge.h"

/** */
namespace Oscl {
/** */
namespace Motorola {
/** */
namespace MPC834x {
/** */
namespace PIC {

/** */
class DrvApi {
	public:
		/** */
		virtual ~DrvApi() {}

	public:
		/** */
		virtual Oscl::Motorola::MPC834x::PIC::IrqDrvApi&	getUART1() noexcept=0;
		/** */
		virtual Oscl::Motorola::MPC834x::PIC::IrqDrvApi&	getUART2() noexcept=0;
		/** */
		virtual Oscl::Motorola::MPC834x::PIC::IrqDrvApi&	getSEC() noexcept=0;
		/** */
		virtual Oscl::Motorola::MPC834x::PIC::IrqDrvApi&	getI2C1() noexcept=0;
		/** */
		virtual Oscl::Motorola::MPC834x::PIC::IrqDrvApi&	getI2C2() noexcept=0;
		/** */
		virtual Oscl::Motorola::MPC834x::PIC::IrqDrvApi&	getSPI() noexcept=0;
		/** */
		virtual Oscl::Motorola::MPC834x::PIC::IrqDrvApi&	getIRQ1() noexcept=0;
		/** */
		virtual Oscl::Motorola::MPC834x::PIC::IrqDrvApi&	getIRQ2() noexcept=0;
		/** */
		virtual Oscl::Motorola::MPC834x::PIC::IrqDrvApi&	getIRQ3() noexcept=0;
		/** */
		virtual Oscl::Motorola::MPC834x::PIC::IrqDrvApi&	getIRQ4() noexcept=0;
		/** */
		virtual Oscl::Motorola::MPC834x::PIC::IrqDrvApi&	getIRQ5() noexcept=0;
		/** */
		virtual Oscl::Motorola::MPC834x::PIC::IrqDrvApi&	getIRQ6() noexcept=0;
		/** */
		virtual Oscl::Motorola::MPC834x::PIC::IrqDrvApi&	getIRQ7() noexcept=0;
		/** */
		virtual Oscl::Motorola::MPC834x::PIC::IrqDrvApi&	getTSEC1Tx() noexcept=0;
		/** */
		virtual Oscl::Motorola::MPC834x::PIC::IrqDrvApi&	getTSEC1Rx() noexcept=0;
		/** */
		virtual Oscl::Motorola::MPC834x::PIC::IrqDrvApi&	getTSEC1Err() noexcept=0;
		/** */
		virtual Oscl::Motorola::MPC834x::PIC::IrqDrvApi&	getTSEC2Tx() noexcept=0;
		/** */
		virtual Oscl::Motorola::MPC834x::PIC::IrqDrvApi&	getTSEC2Rx() noexcept=0;
		/** */
		virtual Oscl::Motorola::MPC834x::PIC::IrqDrvApi&	getTSEC2Err() noexcept=0;
		/** */
		virtual Oscl::Motorola::MPC834x::PIC::IrqDrvApi&	getUSB2DR() noexcept=0;
		/** */
		virtual Oscl::Motorola::MPC834x::PIC::IrqDrvApi&	getUSB2MPH() noexcept=0;
		/** */
		virtual Oscl::Motorola::MPC834x::PIC::IrqDrvApi&	getIRQ0() noexcept=0;
		/** */
		virtual Oscl::Motorola::MPC834x::PIC::IrqDrvApi&	getRTCSEC() noexcept=0;
		/** */
		virtual Oscl::Motorola::MPC834x::PIC::IrqDrvApi&	getPIT() noexcept=0;
		/** */
		virtual Oscl::Motorola::MPC834x::PIC::IrqDrvApi&	getPCI1() noexcept=0;
		/** */
		virtual Oscl::Motorola::MPC834x::PIC::IrqDrvApi&	getPCI2() noexcept=0;
		/** */
		virtual Oscl::Motorola::MPC834x::PIC::IrqDrvApi&	getRTCALR() noexcept=0;
		/** */
		virtual Oscl::Motorola::MPC834x::PIC::IrqDrvApi&	getMU() noexcept=0;
		/** */
		virtual Oscl::Motorola::MPC834x::PIC::IrqDrvApi&	getSBA() noexcept=0;
		/** */
		virtual Oscl::Motorola::MPC834x::PIC::IrqDrvApi&	getDMA() noexcept=0;
		/** */
		virtual Oscl::Motorola::MPC834x::PIC::IrqDrvApi&	getGTM4() noexcept=0;
		/** */
		virtual Oscl::Motorola::MPC834x::PIC::IrqDrvApi&	getGTM8() noexcept=0;
		/** */
		virtual Oscl::Motorola::MPC834x::PIC::IrqDrvApi&	getGPIO1() noexcept=0;
		/** */
		virtual Oscl::Motorola::MPC834x::PIC::IrqDrvApi&	getGPIO2() noexcept=0;
		/** */
		virtual Oscl::Motorola::MPC834x::PIC::IrqDrvApi&	getDDR() noexcept=0;
		/** */
		virtual Oscl::Motorola::MPC834x::PIC::IrqDrvApi&	getLBC() noexcept=0;
		/** */
		virtual Oscl::Motorola::MPC834x::PIC::IrqDrvApi&	getGTM2() noexcept=0;
		/** */
		virtual Oscl::Motorola::MPC834x::PIC::IrqDrvApi&	getGTM6() noexcept=0;
		/** */
		virtual Oscl::Motorola::MPC834x::PIC::IrqDrvApi&	getPMC() noexcept=0;
		/** */
		virtual Oscl::Motorola::MPC834x::PIC::IrqDrvApi&	getGTM3() noexcept=0;
		/** */
		virtual Oscl::Motorola::MPC834x::PIC::IrqDrvApi&	getGTM7() noexcept=0;
		/** */
		virtual Oscl::Motorola::MPC834x::PIC::IrqDrvApi&	getGTM1() noexcept=0;
		/** */
		virtual Oscl::Motorola::MPC834x::PIC::IrqDrvApi&	getGTM5() noexcept=0;

		/** */
		virtual Oscl::Interrupt::EdgeApi&	getIRQ0EdgeApi() noexcept=0;
		/** */
		virtual Oscl::Interrupt::EdgeApi&	getIRQ1EdgeApi() noexcept=0;
		/** */
		virtual Oscl::Interrupt::EdgeApi&	getIRQ2EdgeApi() noexcept=0;
		/** */
		virtual Oscl::Interrupt::EdgeApi&	getIRQ3EdgeApi() noexcept=0;
		/** */
		virtual Oscl::Interrupt::EdgeApi&	getIRQ4EdgeApi() noexcept=0;
		/** */
		virtual Oscl::Interrupt::EdgeApi&	getIRQ5EdgeApi() noexcept=0;
		/** */
		virtual Oscl::Interrupt::EdgeApi&	getIRQ6EdgeApi() noexcept=0;
		/** */
		virtual Oscl::Interrupt::EdgeApi&	getIRQ7EdgeApi() noexcept=0;
	};

}
}
}
}

#endif
