/*
   Copyright (C) 2006 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_motorola_mpc834x_pic_irqdrvh_
#define _oscl_drv_motorola_mpc834x_pic_irqdrvh_
#include "irqdrvapi.h"
#include "oscl/hw/motorola/mpc834x/pic.h"
#include "oscl/interrupt/edge.h"

/** */
namespace Oscl {
/** */
namespace Motorola {
/** */
namespace MPC834x {
/** */
namespace PIC {

/** */
template <unsigned SimsrhLsb, unsigned SipnrhIrqLsb>
class InternalIrqDriverH : public IrqDrvApi {
	private:
		/** */
		Oscl::Motorola::MPC834x::PIC::RegisterMap&		_picMap;

	public:
		/** */
		InternalIrqDriverH(Oscl::Motorola::MPC834x::PIC::RegisterMap& picMap) noexcept:
				_picMap(picMap)
				{
			disable();
			}

		/** */
		void	enable() noexcept{
			Oscl::Motorola::MPC834x::PIC::SIMSR_H::Reg	value	= _picMap.simsr_h;
			value	&= ~(		Oscl::Motorola::MPC834x::PIC::SIMSR_H::Mask
							<<	SimsrhLsb
							);
			value	|= (		Oscl::Motorola::MPC834x::PIC::SIMSR_H::Enable
							<<	SimsrhLsb
							);
			_picMap.simsr_h	= value;
			}

		/** */
		void	disable() noexcept{
			Oscl::Motorola::MPC834x::PIC::SIMSR_H::Reg	value	= _picMap.simsr_h;
			value	&= ~(		Oscl::Motorola::MPC834x::PIC::SIMSR_H::Mask
							<<	SimsrhLsb
							);
			value	|= (		Oscl::Motorola::MPC834x::PIC::SIMSR_H::Disable
							<<	SimsrhLsb
							);
			_picMap.simsr_h	= value;
			}

		/** */
		bool	pending() noexcept{
			return (		_picMap.sipnr_h
						&	(((Oscl::Motorola::MPC834x::PIC::SIPNR_H::Reg)Oscl::Motorola::MPC834x::PIC::SIPNR_H::Mask) << SipnrhIrqLsb)
						)
					==	(((Oscl::Motorola::MPC834x::PIC::SIPNR_H::Reg)Oscl::Motorola::MPC834x::PIC::SIPNR_H::Pending) << SipnrhIrqLsb);
			}
	};

/** */
template <unsigned SimsrlLsb, unsigned SipnrlIrqLsb>
class InternalIrqDriverL : public IrqDrvApi {
	private:
		/** */
		Oscl::Motorola::MPC834x::PIC::RegisterMap&		_picMap;

	public:
		/** */
		InternalIrqDriverL(Oscl::Motorola::MPC834x::PIC::RegisterMap& picMap) noexcept:
				_picMap(picMap)
				{
			disable();
			}

		/** */
		void	enable() noexcept{
			Oscl::Motorola::MPC834x::PIC::SIMSR_L::Reg	value	= _picMap.simsr_l;
			value	&= ~(		Oscl::Motorola::MPC834x::PIC::SIMSR_L::Mask
							<<	SimsrlLsb
							);
			value	|= (		Oscl::Motorola::MPC834x::PIC::SIMSR_L::Enable
							<<	SimsrlLsb
							);
			_picMap.simsr_l	= value;
			}

		/** */
		void	disable() noexcept{
			Oscl::Motorola::MPC834x::PIC::SIMSR_L::Reg	value	= _picMap.simsr_l;
			value	&= ~(		Oscl::Motorola::MPC834x::PIC::SIMSR_L::Mask
							<<	SimsrlLsb
							);
			value	|= (		Oscl::Motorola::MPC834x::PIC::SIMSR_L::Disable
							<<	SimsrlLsb
							);
			_picMap.simsr_l	= value;
			}

		/** */
		bool	pending() noexcept{
			return (		_picMap.sipnr_l
						&	(((Oscl::Motorola::MPC834x::PIC::SIPNR_L::Reg)Oscl::Motorola::MPC834x::PIC::SIPNR_L::Mask) << SipnrlIrqLsb)
						)
					==	(((Oscl::Motorola::MPC834x::PIC::SIPNR_L::Reg)Oscl::Motorola::MPC834x::PIC::SIPNR_L::Pending) << SipnrlIrqLsb);
			}
	};

/** */
template <unsigned SepnrIrqLsb>
class ExternalIrqEdge : public Oscl::Interrupt::EdgeApi {
	private:
		/** */
		IrqDrvApi&									_irqDrvApi;
		/** */
		Oscl::Motorola::MPC834x::PIC::SEPNR::Reg	_sepnr;

	public:	// Oscl::Interrupt::EdgeApi
		/** */
		ExternalIrqEdge(	IrqDrvApi&									irqDrvApi,
							Oscl::Motorola::MPC834x::PIC::SEPNR::Reg	sepnr
							) noexcept:
				_irqDrvApi(irqDrvApi),
				_sepnr(sepnr)
				{
			}

		/** */
		void	ack() noexcept{
			_sepnr	= (Oscl::Motorola::MPC834x::PIC::SEPNR::Reg)(
								Oscl::Motorola::MPC834x::PIC::SEPNR::Mask
							<< SepnrIrqLsb
							)
						;
			}


		/** */
		void	enable() noexcept{
			_irqDrvApi.enable();
			}

		/** */
		void	disable() noexcept{
			_irqDrvApi.disable();
			}
	};

/** */
template <unsigned SemsrLsb, unsigned SepnrIrqLsb>
class ExternalIrqDriver : public IrqDrvApi {
	private:
		/** */
		Oscl::Motorola::MPC834x::PIC::RegisterMap&		_picMap;

		/** */
		ExternalIrqEdge<SepnrIrqLsb>					_edge;

	public:
		/** */
		ExternalIrqDriver(Oscl::Motorola::MPC834x::PIC::RegisterMap& picMap) noexcept:
				_picMap(picMap),
				_edge(*this,picMap.sepnr)
				{
			disable();
			}

	Oscl::Interrupt::EdgeApi&	getEdgeApi() noexcept{
		return _edge;
		}

	public:
		/** */
		void	enable() noexcept{
			Oscl::Motorola::MPC834x::PIC::SEMSR::Reg	value	= _picMap.semsr;
			value	&= ~(		Oscl::Motorola::MPC834x::PIC::SEMSR::Mask
							<<	SemsrLsb
							);
			value	|= (		Oscl::Motorola::MPC834x::PIC::SEMSR::Enable
							<<	SemsrLsb
							);
			_picMap.semsr	= value;
			}

		/** */
		void	disable() noexcept{
			Oscl::Motorola::MPC834x::PIC::SEMSR::Reg	value	= _picMap.semsr;
			value	&= ~(		Oscl::Motorola::MPC834x::PIC::SEMSR::Mask
							<<	SemsrLsb
							);
			value	|= (		Oscl::Motorola::MPC834x::PIC::SEMSR::Disable
							<<	SemsrLsb
							);
			_picMap.semsr	= value;
			}

		/** */
		bool	pending() noexcept{
			return (		_picMap.sepnr
						&	(((Oscl::Motorola::MPC834x::PIC::SEPNR::Reg)Oscl::Motorola::MPC834x::PIC::SEPNR::Mask) << SepnrIrqLsb)
						)
					==	(((Oscl::Motorola::MPC834x::PIC::SEPNR::Reg)Oscl::Motorola::MPC834x::PIC::SEPNR::Pending) << SepnrIrqLsb);
			}
	};
}
}
}
}

#endif
