/*
   Copyright (C) 2006 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_motorola_mpc834x_pic_apih_
#define _oscl_drv_motorola_mpc834x_pic_apih_
#include "oscl/interrupt/shandler.h"
#include "irqapi.h"

/** */
namespace Oscl {
/** */
namespace Motorola {
/** */
namespace MPC834x {
/** */
namespace PIC {

/** */
class Api {
	public:
		/** */
		virtual ~Api() {}
		/** */
		virtual Oscl::Interrupt::StatusHandlerApi&	getISR() noexcept=0;
		/** */
		virtual Oscl::Motorola::MPC834x::PIC::IrqApi&	getUART1() noexcept=0;
		/** */
		virtual Oscl::Motorola::MPC834x::PIC::IrqApi&	getUART2() noexcept=0;
		/** */
		virtual Oscl::Motorola::MPC834x::PIC::IrqApi&	getSEC() noexcept=0;
		/** */
		virtual Oscl::Motorola::MPC834x::PIC::IrqApi&	getI2C1() noexcept=0;
		/** */
		virtual Oscl::Motorola::MPC834x::PIC::IrqApi&	getI2C2() noexcept=0;
		/** */
		virtual Oscl::Motorola::MPC834x::PIC::IrqApi&	getSPI() noexcept=0;
		/** */
		virtual Oscl::Motorola::MPC834x::PIC::IrqApi&	getIRQ1() noexcept=0;
		/** */
		virtual Oscl::Motorola::MPC834x::PIC::IrqApi&	getIRQ2() noexcept=0;
		/** */
		virtual Oscl::Motorola::MPC834x::PIC::IrqApi&	getIRQ3() noexcept=0;
		/** */
		virtual Oscl::Motorola::MPC834x::PIC::IrqApi&	getIRQ4() noexcept=0;
		/** */
		virtual Oscl::Motorola::MPC834x::PIC::IrqApi&	getIRQ5() noexcept=0;
		/** */
		virtual Oscl::Motorola::MPC834x::PIC::IrqApi&	getIRQ6() noexcept=0;
		/** */
		virtual Oscl::Motorola::MPC834x::PIC::IrqApi&	getIRQ7() noexcept=0;
		/** */
		virtual Oscl::Motorola::MPC834x::PIC::IrqApi&	getTSEC1Tx() noexcept=0;
		/** */
		virtual Oscl::Motorola::MPC834x::PIC::IrqApi&	getTSEC1Rx() noexcept=0;
		/** */
		virtual Oscl::Motorola::MPC834x::PIC::IrqApi&	getTSEC1Err() noexcept=0;
		/** */
		virtual Oscl::Motorola::MPC834x::PIC::IrqApi&	getTSEC2Tx() noexcept=0;
		/** */
		virtual Oscl::Motorola::MPC834x::PIC::IrqApi&	getTSEC2Rx() noexcept=0;
		/** */
		virtual Oscl::Motorola::MPC834x::PIC::IrqApi&	getTSEC2Err() noexcept=0;
		/** */
		virtual Oscl::Motorola::MPC834x::PIC::IrqApi&	getUSB2DR() noexcept=0;
		/** */
		virtual Oscl::Motorola::MPC834x::PIC::IrqApi&	getUSB2MPH() noexcept=0;
		/** */
		virtual Oscl::Motorola::MPC834x::PIC::IrqApi&	getIRQ0() noexcept=0;
		/** */
		virtual Oscl::Motorola::MPC834x::PIC::IrqApi&	getRTCSEC() noexcept=0;
		/** */
		virtual Oscl::Motorola::MPC834x::PIC::IrqApi&	getPIT() noexcept=0;
		/** */
		virtual Oscl::Motorola::MPC834x::PIC::IrqApi&	getPCI1() noexcept=0;
		/** */
		virtual Oscl::Motorola::MPC834x::PIC::IrqApi&	getPCI2() noexcept=0;
		/** */
		virtual Oscl::Motorola::MPC834x::PIC::IrqApi&	getRTCALR() noexcept=0;
		/** */
		virtual Oscl::Motorola::MPC834x::PIC::IrqApi&	getMU() noexcept=0;
		/** */
		virtual Oscl::Motorola::MPC834x::PIC::IrqApi&	getSBA() noexcept=0;
		/** */
		virtual Oscl::Motorola::MPC834x::PIC::IrqApi&	getDMA() noexcept=0;
		/** */
		virtual Oscl::Motorola::MPC834x::PIC::IrqApi&	getGTM4() noexcept=0;
		/** */
		virtual Oscl::Motorola::MPC834x::PIC::IrqApi&	getGTM8() noexcept=0;
		/** */
		virtual Oscl::Motorola::MPC834x::PIC::IrqApi&	getGPIO1() noexcept=0;
		/** */
		virtual Oscl::Motorola::MPC834x::PIC::IrqApi&	getGPIO2() noexcept=0;
		/** */
		virtual Oscl::Motorola::MPC834x::PIC::IrqApi&	getDDR() noexcept=0;
		/** */
		virtual Oscl::Motorola::MPC834x::PIC::IrqApi&	getLBC() noexcept=0;
		/** */
		virtual Oscl::Motorola::MPC834x::PIC::IrqApi&	getGTM2() noexcept=0;
		/** */
		virtual Oscl::Motorola::MPC834x::PIC::IrqApi&	getGTM6() noexcept=0;
		/** */
		virtual Oscl::Motorola::MPC834x::PIC::IrqApi&	getPMC() noexcept=0;
		/** */
		virtual Oscl::Motorola::MPC834x::PIC::IrqApi&	getGTM3() noexcept=0;
		/** */
		virtual Oscl::Motorola::MPC834x::PIC::IrqApi&	getGTM7() noexcept=0;
		/** */
		virtual Oscl::Motorola::MPC834x::PIC::IrqApi&	getGTM1() noexcept=0;
		/** */
		virtual Oscl::Motorola::MPC834x::PIC::IrqApi&	getGTM5() noexcept=0;
	};

}
}
}
}

#endif
