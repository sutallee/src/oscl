/*
   Copyright (C) 2006 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "map.h"
#include "oscl/hw/motorola/mpc834x/picreg.h"

using namespace Oscl::Motorola::MPC834x::PIC;

Map::Map(Oscl::Motorola::MPC834x::PIC::PicApi& pic) noexcept:
		_pic(pic),
		_irqUart1(),
		_irqUart2(),
		_irqSec(),
		_irqI2c1(),
		_irqI2c2(),
		_irqSpi(),
		_irqIrq1(),
		_irqIrq2(),
		_irqIrq3(),
		_irqIrq4(),
		_irqIrq5(),
		_irqIrq6(),
		_irqIrq7(),
		_irqTsec1Tx(),
		_irqTsec1Rx(),
		_irqTsec1Err(),
		_irqTsec2Tx(),
		_irqTsec2Rx(),
		_irqTsec2Err(),
		_irqUsb2DR(),
		_irqUsb2MPH(),
		_irqIrq0(),
		_irqRtcSec(),
		_irqPit(),
		_irqPci1(),
		_irqPci2(),
		_irqRtcAlr(),
		_irqMu(),
		_irqSba(),
		_irqDma(),
		_irqGtm4(),
		_irqGtm8(),
		_irqGpio1(),
		_irqGpio2(),
		_irqDdr(),
		_irqLbc(),
		_irqGtm2(),
		_irqGtm6(),
		_irqPmc(),
		_irqGtm3(),
		_irqGtm7(),
		_irqGtm1(),
		_irqGtm5()
		{
		_pic.setIrqApi(_irqUart1,Oscl::Motorola::MPC834x::PIC::SVCR::VEC::Value_UART1);
		_pic.setIrqApi(_irqUart2,Oscl::Motorola::MPC834x::PIC::SVCR::VEC::Value_UART2);
		_pic.setIrqApi(_irqSec,Oscl::Motorola::MPC834x::PIC::SVCR::VEC::Value_SEC);
		_pic.setIrqApi(_irqI2c1,Oscl::Motorola::MPC834x::PIC::SVCR::VEC::Value_I2C1);
		_pic.setIrqApi(_irqI2c2,Oscl::Motorola::MPC834x::PIC::SVCR::VEC::Value_I2C2);
		_pic.setIrqApi(_irqSpi,Oscl::Motorola::MPC834x::PIC::SVCR::VEC::Value_SPI);
		_pic.setIrqApi(_irqIrq1,Oscl::Motorola::MPC834x::PIC::SVCR::VEC::Value_IRQ1);
		_pic.setIrqApi(_irqIrq2,Oscl::Motorola::MPC834x::PIC::SVCR::VEC::Value_IRQ2);
		_pic.setIrqApi(_irqIrq3,Oscl::Motorola::MPC834x::PIC::SVCR::VEC::Value_IRQ3);
		_pic.setIrqApi(_irqIrq4,Oscl::Motorola::MPC834x::PIC::SVCR::VEC::Value_IRQ4);
		_pic.setIrqApi(_irqIrq5,Oscl::Motorola::MPC834x::PIC::SVCR::VEC::Value_IRQ5);
		_pic.setIrqApi(_irqIrq6,Oscl::Motorola::MPC834x::PIC::SVCR::VEC::Value_IRQ6);
		_pic.setIrqApi(_irqIrq7,Oscl::Motorola::MPC834x::PIC::SVCR::VEC::Value_IRQ7);
		_pic.setIrqApi(_irqTsec1Tx,Oscl::Motorola::MPC834x::PIC::SVCR::VEC::Value_TSEC1Tx);
		_pic.setIrqApi(_irqTsec1Rx,Oscl::Motorola::MPC834x::PIC::SVCR::VEC::Value_TSEC1Rx);
		_pic.setIrqApi(_irqTsec1Err,Oscl::Motorola::MPC834x::PIC::SVCR::VEC::Value_TSEC1Err);
		_pic.setIrqApi(_irqTsec2Tx,Oscl::Motorola::MPC834x::PIC::SVCR::VEC::Value_TSEC2Tx);
		_pic.setIrqApi(_irqTsec2Rx,Oscl::Motorola::MPC834x::PIC::SVCR::VEC::Value_TSEC2Rx);
		_pic.setIrqApi(_irqTsec2Err,Oscl::Motorola::MPC834x::PIC::SVCR::VEC::Value_TSEC2Err);
		_pic.setIrqApi(_irqUsb2DR,Oscl::Motorola::MPC834x::PIC::SVCR::VEC::Value_USB2DR);
		_pic.setIrqApi(_irqUsb2MPH,Oscl::Motorola::MPC834x::PIC::SVCR::VEC::Value_USB2MPH);
		_pic.setIrqApi(_irqIrq0,Oscl::Motorola::MPC834x::PIC::SVCR::VEC::Value_IRQ0);
		_pic.setIrqApi(_irqRtcSec,Oscl::Motorola::MPC834x::PIC::SVCR::VEC::Value_RTCSEC);
		_pic.setIrqApi(_irqPit,Oscl::Motorola::MPC834x::PIC::SVCR::VEC::Value_PIT);
		_pic.setIrqApi(_irqPci1,Oscl::Motorola::MPC834x::PIC::SVCR::VEC::Value_PCI1);
		_pic.setIrqApi(_irqPci2,Oscl::Motorola::MPC834x::PIC::SVCR::VEC::Value_PCI2);
		_pic.setIrqApi(_irqRtcAlr,Oscl::Motorola::MPC834x::PIC::SVCR::VEC::Value_RTCALR);
		_pic.setIrqApi(_irqMu,Oscl::Motorola::MPC834x::PIC::SVCR::VEC::Value_MU);
		_pic.setIrqApi(_irqSba,Oscl::Motorola::MPC834x::PIC::SVCR::VEC::Value_SBA);
		_pic.setIrqApi(_irqDma,Oscl::Motorola::MPC834x::PIC::SVCR::VEC::Value_DMA);
		_pic.setIrqApi(_irqGtm4,Oscl::Motorola::MPC834x::PIC::SVCR::VEC::Value_GTM4);
		_pic.setIrqApi(_irqGtm8,Oscl::Motorola::MPC834x::PIC::SVCR::VEC::Value_GTM8);
		_pic.setIrqApi(_irqGpio1,Oscl::Motorola::MPC834x::PIC::SVCR::VEC::Value_GPIO1);
		_pic.setIrqApi(_irqGpio2,Oscl::Motorola::MPC834x::PIC::SVCR::VEC::Value_GPIO2);
		_pic.setIrqApi(_irqDdr,Oscl::Motorola::MPC834x::PIC::SVCR::VEC::Value_DDR);
		_pic.setIrqApi(_irqLbc,Oscl::Motorola::MPC834x::PIC::SVCR::VEC::Value_LBC);
		_pic.setIrqApi(_irqGtm2,Oscl::Motorola::MPC834x::PIC::SVCR::VEC::Value_GTM2);
		_pic.setIrqApi(_irqGtm6,Oscl::Motorola::MPC834x::PIC::SVCR::VEC::Value_GTM6);
		_pic.setIrqApi(_irqPmc,Oscl::Motorola::MPC834x::PIC::SVCR::VEC::Value_PMC);
		_pic.setIrqApi(_irqGtm3,Oscl::Motorola::MPC834x::PIC::SVCR::VEC::Value_GTM3);
		_pic.setIrqApi(_irqGtm7,Oscl::Motorola::MPC834x::PIC::SVCR::VEC::Value_GTM7);
		_pic.setIrqApi(_irqGtm1,Oscl::Motorola::MPC834x::PIC::SVCR::VEC::Value_GTM1);
		_pic.setIrqApi(_irqGtm5,Oscl::Motorola::MPC834x::PIC::SVCR::VEC::Value_GTM5);
	}

Oscl::Interrupt::StatusHandlerApi&	Map::getISR() noexcept{
	return _pic;
	}

Oscl::Motorola::MPC834x::PIC::IrqApi&	Map::getUART1() noexcept{
	return _irqUart1;
	}

Oscl::Motorola::MPC834x::PIC::IrqApi&	Map::getUART2() noexcept{
	return _irqUart2;
	}

Oscl::Motorola::MPC834x::PIC::IrqApi&	Map::getSEC() noexcept{
	return _irqSec;
	}

Oscl::Motorola::MPC834x::PIC::IrqApi&	Map::getI2C1() noexcept{
	return _irqI2c1;
	}

Oscl::Motorola::MPC834x::PIC::IrqApi&	Map::getI2C2() noexcept{
	return _irqI2c2;
	}

Oscl::Motorola::MPC834x::PIC::IrqApi&	Map::getSPI() noexcept{
	return _irqSpi;
	}

Oscl::Motorola::MPC834x::PIC::IrqApi&	Map::getIRQ1() noexcept{
	return _irqIrq1;
	}

Oscl::Motorola::MPC834x::PIC::IrqApi&	Map::getIRQ2() noexcept{
	return _irqIrq2;
	}

Oscl::Motorola::MPC834x::PIC::IrqApi&	Map::getIRQ3() noexcept{
	return _irqIrq3;
	}

Oscl::Motorola::MPC834x::PIC::IrqApi&	Map::getIRQ4() noexcept{
	return _irqIrq4;
	}

Oscl::Motorola::MPC834x::PIC::IrqApi&	Map::getIRQ5() noexcept{
	return _irqIrq5;
	}

Oscl::Motorola::MPC834x::PIC::IrqApi&	Map::getIRQ6() noexcept{
	return _irqIrq6;
	}

Oscl::Motorola::MPC834x::PIC::IrqApi&	Map::getIRQ7() noexcept{
	return _irqIrq7;
	}

Oscl::Motorola::MPC834x::PIC::IrqApi&	Map::getTSEC1Tx() noexcept{
	return _irqTsec1Tx;
	}

Oscl::Motorola::MPC834x::PIC::IrqApi&	Map::getTSEC1Rx() noexcept{
	return _irqTsec1Rx;
	}

Oscl::Motorola::MPC834x::PIC::IrqApi&	Map::getTSEC1Err() noexcept{
	return _irqTsec1Err;
	}

Oscl::Motorola::MPC834x::PIC::IrqApi&	Map::getTSEC2Tx() noexcept{
	return _irqTsec2Tx;
	}

Oscl::Motorola::MPC834x::PIC::IrqApi&	Map::getTSEC2Rx() noexcept{
	return _irqTsec2Rx;
	}

Oscl::Motorola::MPC834x::PIC::IrqApi&	Map::getTSEC2Err() noexcept{
	return _irqTsec2Err;
	}

Oscl::Motorola::MPC834x::PIC::IrqApi&	Map::getUSB2DR() noexcept{
	return _irqUsb2DR;
	}

Oscl::Motorola::MPC834x::PIC::IrqApi&	Map::getUSB2MPH() noexcept{
	return _irqUsb2MPH;
	}

Oscl::Motorola::MPC834x::PIC::IrqApi&	Map::getIRQ0() noexcept{
	return _irqIrq0;
	}

Oscl::Motorola::MPC834x::PIC::IrqApi&	Map::getRTCSEC() noexcept{
	return _irqRtcSec;
	}

Oscl::Motorola::MPC834x::PIC::IrqApi&	Map::getPIT() noexcept{
	return _irqPit;
	}

Oscl::Motorola::MPC834x::PIC::IrqApi&	Map::getPCI1() noexcept{
	return _irqPci1;
	}

Oscl::Motorola::MPC834x::PIC::IrqApi&	Map::getPCI2() noexcept{
	return _irqPci2;
	}

Oscl::Motorola::MPC834x::PIC::IrqApi&	Map::getRTCALR() noexcept{
	return _irqRtcAlr;
	}

Oscl::Motorola::MPC834x::PIC::IrqApi&	Map::getMU() noexcept{
	return _irqMu;
	}

Oscl::Motorola::MPC834x::PIC::IrqApi&	Map::getSBA() noexcept{
	return _irqSba;
	}

Oscl::Motorola::MPC834x::PIC::IrqApi&	Map::getDMA() noexcept{
	return _irqDma;
	}

Oscl::Motorola::MPC834x::PIC::IrqApi&	Map::getGTM4() noexcept{
	return _irqGtm4;
	}

Oscl::Motorola::MPC834x::PIC::IrqApi&	Map::getGTM8() noexcept{
	return _irqGtm8;
	}

Oscl::Motorola::MPC834x::PIC::IrqApi&	Map::getGPIO1() noexcept{
	return _irqGpio1;
	}

Oscl::Motorola::MPC834x::PIC::IrqApi&	Map::getGPIO2() noexcept{
	return _irqGpio2;
	}

Oscl::Motorola::MPC834x::PIC::IrqApi&	Map::getDDR() noexcept{
	return _irqDdr;
	}

Oscl::Motorola::MPC834x::PIC::IrqApi&	Map::getLBC() noexcept{
	return _irqLbc;
	}

Oscl::Motorola::MPC834x::PIC::IrqApi&	Map::getGTM2() noexcept{
	return _irqGtm2;
	}

Oscl::Motorola::MPC834x::PIC::IrqApi&	Map::getGTM6() noexcept{
	return _irqGtm6;
	}

Oscl::Motorola::MPC834x::PIC::IrqApi&	Map::getPMC() noexcept{
	return _irqPmc;
	}

Oscl::Motorola::MPC834x::PIC::IrqApi&	Map::getGTM3() noexcept{
	return _irqGtm3;
	}

Oscl::Motorola::MPC834x::PIC::IrqApi&	Map::getGTM7() noexcept{
	return _irqGtm7;
	}

Oscl::Motorola::MPC834x::PIC::IrqApi&	Map::getGTM1() noexcept{
	return _irqGtm1;
	}

Oscl::Motorola::MPC834x::PIC::IrqApi&	Map::getGTM5() noexcept{
	return _irqGtm5;
	}

