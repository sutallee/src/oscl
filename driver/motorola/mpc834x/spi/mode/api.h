/*
   Copyright (C) 2006 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_motorola_mpc834x_spi_mode_apih_
#define _oscl_drv_motorola_mpc834x_spi_mode_apih_

/** */
namespace Oscl {
/** */
namespace Motorola {
/** */
namespace MPC834x {
/** */
namespace SPI {
/** */
namespace Mode {

/** */
class Api {
	public:
		/** */
		virtual ~Api(){}

		/** */
		virtual bool	loopbackEnabled() const noexcept=0;
		/** */
		virtual void	enableLoopback() noexcept=0;
		/** */
		virtual void	disableLoopback() noexcept=0;

		/** */
		virtual bool	clockInvertEnabled() const noexcept=0;
		/** */
		virtual void	enableClockInvert() noexcept=0;
		/** */
		virtual void	disableClockInvert() noexcept=0;

		/** */
		virtual bool	clockPhaseMiddleSelected() const noexcept=0;
		/** */
		virtual void	selectClockPhaseMiddle() noexcept=0;
		/** */
		virtual void	selectClockPhaseBeginning() noexcept=0;

		/** */
		virtual bool	divideBy16Selected() const noexcept=0;
		/** */
		virtual void	selectDivideBy16() noexcept=0;
		/** */
		virtual void	selectDivideBy1() noexcept=0;

		/** */
		virtual bool	lsbFirstModeSelected() const noexcept=0;
		/** */
		virtual void	selectLsbFirstMode() noexcept=0;
		/** */
		virtual void	selectMsbFirstMode() noexcept=0;

		/** */
		virtual bool	masterModeSelected() const noexcept=0;
		/** */
		virtual void	selectMasterMode() noexcept=0;
		/** */
		virtual void	selectSlaveMode() noexcept=0;

		/** */
		virtual bool	spiEnabled() const noexcept=0;
		/** */
		virtual void	enableSpi() noexcept=0;
		/** */
		virtual void	disableSpi() noexcept=0;

		/** */
		virtual void	set32BitCharacterLength() noexcept=0;
		/** */
		virtual void	set4BitCharacterLength() noexcept=0;
		/** */
		virtual void	set5BitCharacterLength() noexcept=0;
		/** */
		virtual void	set6BitCharacterLength() noexcept=0;
		/** */
		virtual void	set7BitCharacterLength() noexcept=0;
		/** */
		virtual void	set8BitCharacterLength() noexcept=0;
		/** */
		virtual void	set9BitCharacterLength() noexcept=0;
		/** */
		virtual void	set10BitCharacterLength() noexcept=0;
		/** */
		virtual void	set11BitCharacterLength() noexcept=0;
		/** */
		virtual void	set12BitCharacterLength() noexcept=0;
		/** */
		virtual void	set13BitCharacterLength() noexcept=0;
		/** */
		virtual void	set14BitCharacterLength() noexcept=0;
		/** */
		virtual void	set15BitCharacterLength() noexcept=0;
		/** */
		virtual void	set16BitCharacterLength() noexcept=0;

		/** */
		virtual unsigned char	getPrescaleModulus() const noexcept=0;
		/** */
		virtual void	setPrescaleModulus(unsigned char value) noexcept=0;

		/** */
		virtual bool	openDrainOutputModeSelected() const noexcept=0;
		/** */
		virtual void	selectOpenDrainOutputMode() noexcept=0;
		/** */
		virtual void	selectActiveOutputMode() noexcept=0;

	};

}
}
}
}
}


#endif
