/*
   Copyright (C) 2006 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "driver.h"

using namespace Oscl::Motorola::MPC834x::SPI::Mode;

Driver::Driver(volatile Oscl::Motorola::MPC834x::SPI::SPMODE::Reg& reg) noexcept:
		_reg(reg)
		{
	}

bool	Driver::loopbackEnabled() const noexcept{
	return		(_reg & Oscl::Motorola::MPC834x::SPI::SPMODE::LOOP::FieldMask)
			==	Oscl::Motorola::MPC834x::SPI::SPMODE::LOOP::ValueMask_Loopback;
	}

void	Driver::enableLoopback() noexcept{
	Oscl::Motorola::MPC834x::SPI::SPMODE::Reg	value;
	value	= _reg;
	value	&= ~Oscl::Motorola::MPC834x::SPI::SPMODE::LOOP::FieldMask;
	value	|= Oscl::Motorola::MPC834x::SPI::SPMODE::LOOP::ValueMask_Loopback;
	_reg	= value;
	}

void	Driver::disableLoopback() noexcept{
	Oscl::Motorola::MPC834x::SPI::SPMODE::Reg	value;
	value	= _reg;
	value	&= ~Oscl::Motorola::MPC834x::SPI::SPMODE::LOOP::FieldMask;
	value	|= Oscl::Motorola::MPC834x::SPI::SPMODE::LOOP::ValueMask_Normal;
	_reg	= value;
	}

bool	Driver::clockInvertEnabled() const noexcept{
	return		(_reg & Oscl::Motorola::MPC834x::SPI::SPMODE::CI::FieldMask)
			==	Oscl::Motorola::MPC834x::SPI::SPMODE::CI::ValueMask_Invert;
	}

void	Driver::enableClockInvert() noexcept{
	Oscl::Motorola::MPC834x::SPI::SPMODE::Reg	value;
	value	= _reg;
	value	&= ~Oscl::Motorola::MPC834x::SPI::SPMODE::CI::FieldMask;
	value	|= Oscl::Motorola::MPC834x::SPI::SPMODE::CI::ValueMask_Invert;
	_reg	= value;
	}

void	Driver::disableClockInvert() noexcept{
	Oscl::Motorola::MPC834x::SPI::SPMODE::Reg	value;
	value	= _reg;
	value	&= ~Oscl::Motorola::MPC834x::SPI::SPMODE::CI::FieldMask;
	value	|= Oscl::Motorola::MPC834x::SPI::SPMODE::CI::ValueMask_Normal;
	_reg	= value;
	}

bool	Driver::clockPhaseMiddleSelected() const noexcept{
	return		(_reg & Oscl::Motorola::MPC834x::SPI::SPMODE::CP::FieldMask)
			==	Oscl::Motorola::MPC834x::SPI::SPMODE::CP::ValueMask_ClockInMiddle;
	}

void	Driver::selectClockPhaseMiddle() noexcept{
	Oscl::Motorola::MPC834x::SPI::SPMODE::Reg	value;
	value	= _reg;
	value	&= ~Oscl::Motorola::MPC834x::SPI::SPMODE::CP::FieldMask;
	value	|= Oscl::Motorola::MPC834x::SPI::SPMODE::CP::ValueMask_ClockInMiddle;
	_reg	= value;
	}

void	Driver::selectClockPhaseBeginning() noexcept{
	Oscl::Motorola::MPC834x::SPI::SPMODE::Reg	value;
	value	= _reg;
	value	&= ~Oscl::Motorola::MPC834x::SPI::SPMODE::CP::FieldMask;
	value	|= Oscl::Motorola::MPC834x::SPI::SPMODE::CP::ValueMask_ClockAtStart;
	_reg	= value;
	}

bool	Driver::divideBy16Selected() const noexcept{
	return		(_reg & Oscl::Motorola::MPC834x::SPI::SPMODE::DIV16::FieldMask)
			==	Oscl::Motorola::MPC834x::SPI::SPMODE::DIV16::ValueMask_DivideBy16;
	}

void	Driver::selectDivideBy16() noexcept{
	Oscl::Motorola::MPC834x::SPI::SPMODE::Reg	value;
	value	= _reg;
	value	&= ~Oscl::Motorola::MPC834x::SPI::SPMODE::DIV16::FieldMask;
	value	|= Oscl::Motorola::MPC834x::SPI::SPMODE::DIV16::ValueMask_DivideBy16;
	_reg	= value;
	}

void	Driver::selectDivideBy1() noexcept{
	Oscl::Motorola::MPC834x::SPI::SPMODE::Reg	value;
	value	= _reg;
	value	&= ~Oscl::Motorola::MPC834x::SPI::SPMODE::DIV16::FieldMask;
	value	|= Oscl::Motorola::MPC834x::SPI::SPMODE::DIV16::ValueMask_DivideBy1;
	_reg	= value;
	}

bool	Driver::lsbFirstModeSelected() const noexcept{
	return		(_reg & Oscl::Motorola::MPC834x::SPI::SPMODE::REV::FieldMask)
			==	Oscl::Motorola::MPC834x::SPI::SPMODE::REV::ValueMask_LsbFirst;
	}

void	Driver::selectLsbFirstMode() noexcept{
	Oscl::Motorola::MPC834x::SPI::SPMODE::Reg	value;
	value	= _reg;
	value	&= ~Oscl::Motorola::MPC834x::SPI::SPMODE::REV::FieldMask;
	value	|= Oscl::Motorola::MPC834x::SPI::SPMODE::REV::ValueMask_LsbFirst;
	_reg	= value;
	}

void	Driver::selectMsbFirstMode() noexcept{
	Oscl::Motorola::MPC834x::SPI::SPMODE::Reg	value;
	value	= _reg;
	value	&= ~Oscl::Motorola::MPC834x::SPI::SPMODE::REV::FieldMask;
	value	|= Oscl::Motorola::MPC834x::SPI::SPMODE::REV::ValueMask_MsbFirst;
	_reg	= value;
	}

bool	Driver::masterModeSelected() const noexcept{
	return		(_reg & Oscl::Motorola::MPC834x::SPI::SPMODE::MS::FieldMask)
			==	Oscl::Motorola::MPC834x::SPI::SPMODE::MS::ValueMask_Master;
	}

void	Driver::selectMasterMode() noexcept{
	Oscl::Motorola::MPC834x::SPI::SPMODE::Reg	value;
	value	= _reg;
	value	&= ~Oscl::Motorola::MPC834x::SPI::SPMODE::MS::FieldMask;
	value	|= Oscl::Motorola::MPC834x::SPI::SPMODE::MS::ValueMask_Master;
	_reg	= value;
	}

void	Driver::selectSlaveMode() noexcept{
	Oscl::Motorola::MPC834x::SPI::SPMODE::Reg	value;
	value	= _reg;
	value	&= ~Oscl::Motorola::MPC834x::SPI::SPMODE::MS::FieldMask;
	value	|= Oscl::Motorola::MPC834x::SPI::SPMODE::MS::ValueMask_Slave;
	_reg	= value;
	}

bool	Driver::spiEnabled() const noexcept{
	return		(_reg & Oscl::Motorola::MPC834x::SPI::SPMODE::EN::FieldMask)
			==	Oscl::Motorola::MPC834x::SPI::SPMODE::EN::ValueMask_Enable;
	}

void	Driver::enableSpi() noexcept{
	Oscl::Motorola::MPC834x::SPI::SPMODE::Reg	value;
	value	= _reg;
	value	&= ~Oscl::Motorola::MPC834x::SPI::SPMODE::EN::FieldMask;
	value	|= Oscl::Motorola::MPC834x::SPI::SPMODE::EN::ValueMask_Enable;
	_reg	= value;
	}

void	Driver::disableSpi() noexcept{
	Oscl::Motorola::MPC834x::SPI::SPMODE::Reg	value;
	value	= _reg;
	value	&= ~Oscl::Motorola::MPC834x::SPI::SPMODE::EN::FieldMask;
	value	|= Oscl::Motorola::MPC834x::SPI::SPMODE::EN::ValueMask_Disable;
	_reg	= value;
	}

void	Driver::set32BitCharacterLength() noexcept{
	Oscl::Motorola::MPC834x::SPI::SPMODE::Reg	value;
	value	= _reg;
	value	&= ~Oscl::Motorola::MPC834x::SPI::SPMODE::LEN::FieldMask;
	value	|= Oscl::Motorola::MPC834x::SPI::SPMODE::LEN::ValueMask_ThirtyTwoBit;
	_reg	= value;
	}

void	Driver::set4BitCharacterLength() noexcept{
	Oscl::Motorola::MPC834x::SPI::SPMODE::Reg	value;
	value	= _reg;
	value	&= ~Oscl::Motorola::MPC834x::SPI::SPMODE::LEN::FieldMask;
	value	|= Oscl::Motorola::MPC834x::SPI::SPMODE::LEN::ValueMask_FourBit;
	_reg	= value;
	}

void	Driver::set5BitCharacterLength() noexcept{
	Oscl::Motorola::MPC834x::SPI::SPMODE::Reg	value;
	value	= _reg;
	value	&= ~Oscl::Motorola::MPC834x::SPI::SPMODE::LEN::FieldMask;
	value	|= Oscl::Motorola::MPC834x::SPI::SPMODE::LEN::ValueMask_FiveBit;
	_reg	= value;
	}

void	Driver::set6BitCharacterLength() noexcept{
	Oscl::Motorola::MPC834x::SPI::SPMODE::Reg	value;
	value	= _reg;
	value	&= ~Oscl::Motorola::MPC834x::SPI::SPMODE::LEN::FieldMask;
	value	|= Oscl::Motorola::MPC834x::SPI::SPMODE::LEN::ValueMask_SixBit;
	_reg	= value;
	}

void	Driver::set7BitCharacterLength() noexcept{
	Oscl::Motorola::MPC834x::SPI::SPMODE::Reg	value;
	value	= _reg;
	value	&= ~Oscl::Motorola::MPC834x::SPI::SPMODE::LEN::FieldMask;
	value	|= Oscl::Motorola::MPC834x::SPI::SPMODE::LEN::ValueMask_SevenBit;
	_reg	= value;
	}

void	Driver::set8BitCharacterLength() noexcept{
	Oscl::Motorola::MPC834x::SPI::SPMODE::Reg	value;
	value	= _reg;
	value	&= ~Oscl::Motorola::MPC834x::SPI::SPMODE::LEN::FieldMask;
	value	|= Oscl::Motorola::MPC834x::SPI::SPMODE::LEN::ValueMask_EightBit;
	_reg	= value;
	}

void	Driver::set9BitCharacterLength() noexcept{
	Oscl::Motorola::MPC834x::SPI::SPMODE::Reg	value;
	value	= _reg;
	value	&= ~Oscl::Motorola::MPC834x::SPI::SPMODE::LEN::FieldMask;
	value	|= Oscl::Motorola::MPC834x::SPI::SPMODE::LEN::ValueMask_NineBit;
	_reg	= value;
	}

void	Driver::set10BitCharacterLength() noexcept{
	Oscl::Motorola::MPC834x::SPI::SPMODE::Reg	value;
	value	= _reg;
	value	&= ~Oscl::Motorola::MPC834x::SPI::SPMODE::LEN::FieldMask;
	value	|= Oscl::Motorola::MPC834x::SPI::SPMODE::LEN::ValueMask_TenBit;
	_reg	= value;
	}

void	Driver::set11BitCharacterLength() noexcept{
	Oscl::Motorola::MPC834x::SPI::SPMODE::Reg	value;
	value	= _reg;
	value	&= ~Oscl::Motorola::MPC834x::SPI::SPMODE::LEN::FieldMask;
	value	|= Oscl::Motorola::MPC834x::SPI::SPMODE::LEN::ValueMask_ElevenBit;
	_reg	= value;
	}

void	Driver::set12BitCharacterLength() noexcept{
	Oscl::Motorola::MPC834x::SPI::SPMODE::Reg	value;
	value	= _reg;
	value	&= ~Oscl::Motorola::MPC834x::SPI::SPMODE::LEN::FieldMask;
	value	|= Oscl::Motorola::MPC834x::SPI::SPMODE::LEN::ValueMask_TwelveBit;
	_reg	= value;
	}

void	Driver::set13BitCharacterLength() noexcept{
	Oscl::Motorola::MPC834x::SPI::SPMODE::Reg	value;
	value	= _reg;
	value	&= ~Oscl::Motorola::MPC834x::SPI::SPMODE::LEN::FieldMask;
	value	|= Oscl::Motorola::MPC834x::SPI::SPMODE::LEN::ValueMask_ThirteenBit;
	_reg	= value;
	}

void	Driver::set14BitCharacterLength() noexcept{
	Oscl::Motorola::MPC834x::SPI::SPMODE::Reg	value;
	value	= _reg;
	value	&= ~Oscl::Motorola::MPC834x::SPI::SPMODE::LEN::FieldMask;
	value	|= Oscl::Motorola::MPC834x::SPI::SPMODE::LEN::ValueMask_FourteenBit;
	_reg	= value;
	}

void	Driver::set15BitCharacterLength() noexcept{
	Oscl::Motorola::MPC834x::SPI::SPMODE::Reg	value;
	value	= _reg;
	value	&= ~Oscl::Motorola::MPC834x::SPI::SPMODE::LEN::FieldMask;
	value	|= Oscl::Motorola::MPC834x::SPI::SPMODE::LEN::ValueMask_FifteenBit;
	_reg	= value;
	}

void	Driver::set16BitCharacterLength() noexcept{
	Oscl::Motorola::MPC834x::SPI::SPMODE::Reg	value;
	value	= _reg;
	value	&= ~Oscl::Motorola::MPC834x::SPI::SPMODE::LEN::FieldMask;
	value	|= Oscl::Motorola::MPC834x::SPI::SPMODE::LEN::ValueMask_SixteenBit;
	_reg	= value;
	}

unsigned char	Driver::getPrescaleModulus() const noexcept{
	return	(unsigned char)
			(
					(_reg & Oscl::Motorola::MPC834x::SPI::SPMODE::PM::FieldMask)
				>> Oscl::Motorola::MPC834x::SPI::SPMODE::PM::Lsb
				)
			;
	}

void	Driver::setPrescaleModulus(unsigned char v) noexcept{
	Oscl::Motorola::MPC834x::SPI::SPMODE::Reg	pm;
	Oscl::Motorola::MPC834x::SPI::SPMODE::Reg	value;
	pm		= v;
	pm		<<= Oscl::Motorola::MPC834x::SPI::SPMODE::PM::Lsb;
	value	= _reg;
	value	&= ~Oscl::Motorola::MPC834x::SPI::SPMODE::PM::FieldMask;
	value	|= pm;
	_reg	= value;
	}

bool	Driver::openDrainOutputModeSelected() const noexcept{
	return		(_reg & Oscl::Motorola::MPC834x::SPI::SPMODE::OD::FieldMask)
			==	Oscl::Motorola::MPC834x::SPI::SPMODE::OD::ValueMask_OpenDrainOutputs;
	}

void	Driver::selectOpenDrainOutputMode() noexcept{
	Oscl::Motorola::MPC834x::SPI::SPMODE::Reg	value;
	value	= _reg;
	value	&= ~Oscl::Motorola::MPC834x::SPI::SPMODE::OD::FieldMask;
	value	|= Oscl::Motorola::MPC834x::SPI::SPMODE::OD::ValueMask_OpenDrainOutputs;
	_reg	= value;
	}

void	Driver::selectActiveOutputMode() noexcept{
	Oscl::Motorola::MPC834x::SPI::SPMODE::Reg	value;
	value	= _reg;
	value	&= ~Oscl::Motorola::MPC834x::SPI::SPMODE::OD::FieldMask;
	value	|= Oscl::Motorola::MPC834x::SPI::SPMODE::OD::ValueMask_ActiveOutputs;
	_reg	= value;
	}

