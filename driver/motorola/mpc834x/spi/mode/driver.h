/*
   Copyright (C) 2006 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_motorola_mpc834x_spi_mode_driverh_
#define _oscl_drv_motorola_mpc834x_spi_mode_driverh_
#include "api.h"
#include "oscl/hw/motorola/mpc834x/spireg.h"

/** */
namespace Oscl {
/** */
namespace Motorola {
/** */
namespace MPC834x {
/** */
namespace SPI {
/** */
namespace Mode {

/** */
class Driver : public Api {
	private:
		/** */
		volatile Oscl::Motorola::MPC834x::SPI::SPMODE::Reg&	_reg;

	public:
		/** */
		Driver(volatile Oscl::Motorola::MPC834x::SPI::SPMODE::Reg& reg) noexcept;

	public: // Api
		/** */
		bool	loopbackEnabled() const noexcept;
		/** */
		void	enableLoopback() noexcept;
		/** */
		void	disableLoopback() noexcept;

		/** */
		bool	clockInvertEnabled() const noexcept;
		/** */
		void	enableClockInvert() noexcept;
		/** */
		void	disableClockInvert() noexcept;

		/** */
		bool	clockPhaseMiddleSelected() const noexcept;
		/** */
		void	selectClockPhaseMiddle() noexcept;
		/** */
		void	selectClockPhaseBeginning() noexcept;

		/** */
		bool	divideBy16Selected() const noexcept;
		/** */
		void	selectDivideBy16() noexcept;
		/** */
		void	selectDivideBy1() noexcept;

		/** */
		bool	lsbFirstModeSelected() const noexcept;
		/** */
		void	selectLsbFirstMode() noexcept;
		/** */
		void	selectMsbFirstMode() noexcept;

		/** */
		bool	masterModeSelected() const noexcept;
		/** */
		void	selectMasterMode() noexcept;
		/** */
		void	selectSlaveMode() noexcept;

		/** */
		bool	spiEnabled() const noexcept;
		/** */
		void	enableSpi() noexcept;
		/** */
		void	disableSpi() noexcept;

		/** */
		void	set32BitCharacterLength() noexcept;
		/** */
		void	set4BitCharacterLength() noexcept;
		/** */
		void	set5BitCharacterLength() noexcept;
		/** */
		void	set6BitCharacterLength() noexcept;
		/** */
		void	set7BitCharacterLength() noexcept;
		/** */
		void	set8BitCharacterLength() noexcept;
		/** */
		void	set9BitCharacterLength() noexcept;
		/** */
		void	set10BitCharacterLength() noexcept;
		/** */
		void	set11BitCharacterLength() noexcept;
		/** */
		void	set12BitCharacterLength() noexcept;
		/** */
		void	set13BitCharacterLength() noexcept;
		/** */
		void	set14BitCharacterLength() noexcept;
		/** */
		void	set15BitCharacterLength() noexcept;
		/** */
		void	set16BitCharacterLength() noexcept;

		/** */
		unsigned char	getPrescaleModulus() const noexcept;
		/** */
		void	setPrescaleModulus(unsigned char value) noexcept;

		/** */
		bool	openDrainOutputModeSelected() const noexcept;
		/** */
		void	selectOpenDrainOutputMode() noexcept;
		/** */
		void	selectActiveOutputMode() noexcept;
	};

}
}
}
}
}


#endif
