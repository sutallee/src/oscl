/*
   Copyright (C) 2006 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_motorola_mpc834x_spi_ie_driverh_
#define _oscl_drv_motorola_mpc834x_spi_ie_driverh_
#include "api.h"
#include "oscl/hw/motorola/mpc834x/spireg.h"

/** */
namespace Oscl {
/** */
namespace Motorola {
/** */
namespace MPC834x {
/** */
namespace SPI {
/** */
namespace IE {

/** */
class Driver : public Api {
	private:
		/** */
		volatile Oscl::Motorola::MPC834x::SPI::SPIE::Reg&	_reg;

	public:
		/** */
		Driver(volatile Oscl::Motorola::MPC834x::SPI::SPIE::Reg& reg) noexcept;

	public:
		/** */
		bool	lastCharacterTransmitted() const noexcept;
		/** */
		void	clearLastCharacterTransmitted() noexcept;

		/** */
		bool	dataNotReadyWhenSelected() const noexcept;
		/** */
		void	clearDataNotReadyWhenSelected() noexcept;

		/** */
		bool	overrun() const noexcept;
		/** */
		void	clearOverrun() noexcept;

		/** */
		bool	underrun() const noexcept;
		/** */
		void	clearUnderrun() noexcept;

		/** */
		bool	multipleMasterError() const noexcept;
		/** */
		void	clearMultipleMasterError() noexcept;

		/** */
		bool	receiverNotEmpty() const noexcept;

		/** */
		bool	transmitterNotFull() const noexcept;
	};

}
}
}
}
}


#endif
