/*
   Copyright (C) 2006 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "driver.h"

using namespace Oscl::Motorola::MPC834x::SPI::IE;

Driver::Driver(volatile Oscl::Motorola::MPC834x::SPI::SPIE::Reg& reg) noexcept:
		_reg(reg)
		{
	}

bool	Driver::lastCharacterTransmitted() const noexcept{
	return		(_reg & Oscl::Motorola::MPC834x::SPI::SPIE::LT::FieldMask)
			==	Oscl::Motorola::MPC834x::SPI::SPIE::LT::ValueMask_LastCharacterTransmitted;
	}

void	Driver::clearLastCharacterTransmitted() noexcept{
	_reg	= Oscl::Motorola::MPC834x::SPI::SPIE::LT::ValueMask_Clear;
	}

bool	Driver::dataNotReadyWhenSelected() const noexcept{
	return		(_reg & Oscl::Motorola::MPC834x::SPI::SPIE::DNR::FieldMask)
			==	Oscl::Motorola::MPC834x::SPI::SPIE::DNR::ValueMask_DataNotReadyWhenSelected;
	}

void	Driver::clearDataNotReadyWhenSelected() noexcept{
	_reg	= Oscl::Motorola::MPC834x::SPI::SPIE::DNR::ValueMask_Clear;
	}

bool	Driver::overrun() const noexcept{
	return		(_reg & Oscl::Motorola::MPC834x::SPI::SPIE::OV::FieldMask)
			==	Oscl::Motorola::MPC834x::SPI::SPIE::OV::ValueMask_RxOverrun;
	}

void	Driver::clearOverrun() noexcept{
	_reg	= Oscl::Motorola::MPC834x::SPI::SPIE::OV::ValueMask_Clear;
	}

bool	Driver::underrun() const noexcept{
	return		(_reg & Oscl::Motorola::MPC834x::SPI::SPIE::UN::FieldMask)
			==	Oscl::Motorola::MPC834x::SPI::SPIE::UN::ValueMask_TxUnderrun;
	}

void	Driver::clearUnderrun() noexcept{
	_reg	= Oscl::Motorola::MPC834x::SPI::SPIE::UN::ValueMask_Clear;
	}

bool	Driver::multipleMasterError() const noexcept{
	return		(_reg & Oscl::Motorola::MPC834x::SPI::SPIE::MME::FieldMask)
			==	Oscl::Motorola::MPC834x::SPI::SPIE::MME::ValueMask_Collision;
	}

void	Driver::clearMultipleMasterError() noexcept{
	_reg	= Oscl::Motorola::MPC834x::SPI::SPIE::MME::ValueMask_Clear;
	}

bool	Driver::receiverNotEmpty() const noexcept{
	return		(_reg & Oscl::Motorola::MPC834x::SPI::SPIE::NE::FieldMask)
			==	Oscl::Motorola::MPC834x::SPI::SPIE::NE::ValueMask_RxDataNotEmpty;
	}

bool	Driver::transmitterNotFull() const noexcept{
	return		(_reg & Oscl::Motorola::MPC834x::SPI::SPIE::NF::FieldMask)
			==	Oscl::Motorola::MPC834x::SPI::SPIE::NF::ValueMask_TxDataNotFull;
	}

