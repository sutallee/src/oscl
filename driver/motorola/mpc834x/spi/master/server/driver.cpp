/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "driver.h"
#include "oscl/hw/motorola/mpc834x/spireg.h"
#include "oscl/error/info.h"
#include "oscl/error/fatal.h"

using namespace Oscl::Motorola::MPC834x::SPI::Master::Server;

Driver::Driver(	Oscl::Motorola::MPC834x::SPI::RegisterMap&	reg
				) noexcept:
		_reg(reg),
		_modeDriver(reg.spmode),
		_ieDriver(reg.spie),
		_imDriver(reg.spim),
		_interrupt(	_imDriver,
					*this,
					(		Oscl::Motorola::MPC834x::SPI::SPIM::LT::ValueMask_EnableInterrupt
						|	Oscl::Motorola::MPC834x::SPI::SPIM::DNR::ValueMask_EnableInterrupt
						|	Oscl::Motorola::MPC834x::SPI::SPIM::OV::ValueMask_EnableInterrupt
						|	Oscl::Motorola::MPC834x::SPI::SPIM::UN::ValueMask_EnableInterrupt
						|	Oscl::Motorola::MPC834x::SPI::SPIM::MME::ValueMask_EnableInterrupt
						|	Oscl::Motorola::MPC834x::SPI::SPIM::NE::ValueMask_EnableInterrupt
						|	Oscl::Motorola::MPC834x::SPI::SPIM::NF::ValueMask_DisableInterrupt
						)
					),
		_sync(*this,*this),
		_pending(),
		_currentTransferMsg(0),
		_nRemainingToReceive(0),
		_currentRxDataPtr(0),
		_nRemainingToTransmit(0),
		_currentTxDataPtr(0)
		{
	}

void Driver::initialize() noexcept{
	// Initialize hardware
	// CSB is 128MHz
	// Standard maximum SCLK freq is 100KHz
	// Thus choose a divider greather than 128MHz/100KHz = 1280
	// According to the MPC834x user manual table 17-5,
	// the nearest divisor is 1280. To give myself a margin of
	// error, I'll choose somethinig that is less than this, say half.
	// The table says 2560 is available for 50KHz.
	_modeDriver.disableSpi();
	_modeDriver.disableLoopback();
	_modeDriver.disableClockInvert();
	_modeDriver.selectClockPhaseMiddle();
//	_modeDriver.selectClockPhaseBeginning();
	_modeDriver.selectDivideBy16();
	_modeDriver.selectMsbFirstMode();
	_modeDriver.selectMasterMode();
	_modeDriver.set8BitCharacterLength();
	// divider = divideBy * 4*(modulus+1)
	// since dividBy is "selectDivideBy16":
	// divider = 16 * 4 * (modulus + 1)
	// divider = 64 * (modulus + 1)
	// Freq = csb / divider
	// Freq = 128MHz / divider
	// Assume desired Freq is 100KHz
	// 100KHz = 128MHz / divider
	// divider = 128MHz / 100KHz
	// 64*(modulus+1)= 128MHz / 100KHz
	// (modulus+1) = 128MHz / (100KHz * 64)
	// modulus = (128MHz/(100KHz*64))-1
	// modulus = (128MHz/6400KHz)-1
	// modulus = 20 - 1
	// modulus = 19
	// However, since the PM field is only
	// 4 bits wide, the largest modulus value
	// is 15, and thus, the slowest bit rate
	// we can achieve is:
	// 128MHz / 64(15+1) = 125KHz
	_modeDriver.setPrescaleModulus(15);
	_modeDriver.enableSpi();
	_ieDriver.clearLastCharacterTransmitted();
	_ieDriver.clearDataNotReadyWhenSelected();
	_ieDriver.clearOverrun();
	_ieDriver.clearUnderrun();	// Only valid in slave mode
	}

void	Driver::mboxSignaled() noexcept{
	if(_ieDriver.lastCharacterTransmitted()){
		_ieDriver.clearLastCharacterTransmitted();
		}
	if(_ieDriver.dataNotReadyWhenSelected()){
		Oscl::ErrorFatal::logAndExit("SPI Driver, Unexpected DNR event\n\r");
		_ieDriver.clearDataNotReadyWhenSelected();
		}
	if(_ieDriver.overrun()){
		Oscl::ErrorFatal::logAndExit("SPI Driver, Unexpected overrun event\n\r");
		_ieDriver.clearOverrun();
		}
	if(_ieDriver.underrun()){
		Oscl::ErrorFatal::logAndExit("SPI Driver, Unexpected underrun event\n\r");
		_ieDriver.clearUnderrun();
		}
	if(_ieDriver.receiverNotEmpty()){
		if(_currentTransferMsg){
			*_currentRxDataPtr	= _reg.spird;
			++_currentRxDataPtr;
			--_nRemainingToReceive;
			if(!_nRemainingToReceive){
				_currentTransferMsg->_payload._failed	= false;
				_currentTransferMsg->returnToSender();
				_currentTransferMsg	= 0;
				}
			}
		else {
			// Presumably the transfer request was canceled
			_reg.spird;	// dummy read to clear interrupt
			}
		}
	if(_ieDriver.transmitterNotFull()){
		if(_currentTransferMsg){
			if(_nRemainingToTransmit == 1) {
				_reg.spcom	= Oscl::Motorola::MPC834x::SPI::SPCOM::LST::ValueMask_LastCharacterOfFrame;
				_reg.spitd	= *_currentTxDataPtr;
				++_currentTxDataPtr;
				--_nRemainingToTransmit;
				}
			else if(_nRemainingToTransmit){
				_reg.spitd	= *_currentTxDataPtr;
				++_currentTxDataPtr;
				--_nRemainingToTransmit;
				}
			else {
				Oscl::ErrorFatal::logAndExit("SPI Driver, unexpected transmitterNotFull on last character.\n\r");
				// This should not happen, instead we should get "last character"
				for(;;);
				}
			}
		else {
//			Oscl::ErrorFatal::logAndExit("SPI Driver, transfer cancellation not implemented\n\r");
			// Presumably the transfer got canceled.
			}
		}
	_interrupt.interruptHandled();
	}

Oscl::Interrupt::StatusHandlerApi&	Driver::getISR() noexcept{
	return _interrupt;
	}

Oscl::Interrupt::IsrDsrApi&	Driver::getIsrDsr() noexcept{
	return _interrupt;
	}

Oscl::Mt::Runnable&	Driver::getRunnable() noexcept{
	return *this;
	}

Oscl::SPI::Master::Req::Api::SAP& Driver::getSAP() noexcept{
	return _sync.getSAP();
	}

Oscl::SPI::Master::Api&	Driver::getSyncApi() noexcept{
	return _sync;
	}

void	Driver::syncOpen() noexcept{
	}

void	Driver::syncClose() noexcept{
	}

void	Driver::request(Oscl::SPI::Master::Req::Api::TransferReq& msg) noexcept{

	if(msg.getPayload()._frequency){
		/* This driver does not currently support
			changing the SPI bus parameters frequency,
			cpol, and cpha.
		 */
		Oscl::Error::Info::log("SPI Driver: SPI parameters not supported!\n\r");
		msg.getPayload()._failed	= true;
		msg.returnToSender();
		return;
		}

	if(_currentTransferMsg){
		_pending.put(&msg);
		return;
		}
	_currentTransferMsg	= &msg;
	msg._payload._deviceSelectApi.select();
	_nRemainingToReceive	= msg._payload._nDataBytesToTransfer;
	_nRemainingToTransmit	= msg._payload._nDataBytesToTransfer;
	_currentRxDataPtr		= (unsigned char*)msg._payload._rxDataBuffer;
	_currentTxDataPtr		= (const unsigned char*)msg._payload._txDataBuffer;

	// Send the first character
	if(_nRemainingToTransmit == 1){
		_reg.spcom	= Oscl::Motorola::MPC834x::SPI::SPCOM::LST::ValueMask_LastCharacterOfFrame;
		}
	else {
		_reg.spcom	= Oscl::Motorola::MPC834x::SPI::SPCOM::LST::ValueMask_NotLastCharacterOfFrame;
		}
	_reg.spitd	= *_currentTxDataPtr;
	++_currentTxDataPtr;
	--_nRemainingToTransmit;
	_interrupt.interruptHandled();
	}

void	Driver::request(Oscl::SPI::Master::Req::Api::CancelTransferReq& msg) noexcept{
	// The cancel operation is not yet implemented. I tried quick-n-dirty but
	// it leaves the SPI event hardware in a wierd state under some conditions.
	// I need to do a proper state machine for this driver, but at the moment
	// I'm still trying to understand the behavior of the @#*$($ Frescale
	// hardware.
	Oscl::Error::Info::log("SPI Driver: CancelTransferReq not properly implemented!\n\r");
	if(&msg._payload._transferToCancel == _currentTransferMsg){
		_currentTransferMsg->returnToSender();
		_currentTransferMsg	= 0;
		msg.returnToSender();
		return;
		}
	Oscl::SPI::Master::Req::Api::TransferReq*	next;
	for(next=_pending.first();next;next=_pending.next(next)){
		if(&msg._payload._transferToCancel == next){
			_pending.remove(next);
			next->returnToSender();
			return;
			}
		}
	msg.returnToSender();
	}

