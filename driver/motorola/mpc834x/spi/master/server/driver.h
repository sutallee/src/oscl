/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_motorola_mpc834x_spi_master_server_driverh_
#define _oscl_drv_motorola_mpc834x_spi_master_server_driverh_

#include "oscl/mt/itc/srv/ocsyncapi.h"
#include "oscl/mt/itc/mbox/server.h"
#include "oscl/hw/motorola/mpc834x/spi.h"
#include "oscl/spi/master/itc/reqapi.h"
#include "oscl/spi/master/itc/sync.h"
#include "oscl/driver/motorola/mpc834x/spi/mode/driver.h"
#include "oscl/driver/motorola/mpc834x/spi/ie/driver.h"
#include "oscl/driver/motorola/mpc834x/spi/im/driver.h"
#include "interrupt.h"

/** */
namespace Oscl {
/** */
namespace Motorola {
/** */
namespace MPC834x {
/** */
namespace SPI {
/** */
namespace Master {
/** */
namespace Server {

/** */
class Driver :	public Oscl::Mt::Itc::Server,
				public Oscl::Mt::Itc::Srv::OpenCloseSyncApi,
				private Oscl::SPI::Master::Req::Api
				{
	private:
		/** */
		Oscl::Motorola::MPC834x::SPI::RegisterMap&				_reg;
		/** */
		Oscl::Motorola::MPC834x::SPI::Mode::Driver				_modeDriver;
		/** */
		Oscl::Motorola::MPC834x::SPI::IE::Driver				_ieDriver;
		/** */
		Oscl::Motorola::MPC834x::SPI::IM::Driver				_imDriver;
		/** */
		Interrupt												_interrupt;
		/** */
		Oscl::SPI::Master::Sync									_sync;

		/** temp */
		Oscl::Queue<Oscl::SPI::Master::Req::Api::TransferReq>	_pending;

		/** temp */
		Oscl::SPI::Master::Req::Api::TransferReq*				_currentTransferMsg;

		/** */
		unsigned												_nRemainingToReceive;

		/** */
		unsigned char*											_currentRxDataPtr;

		/** */
		unsigned												_nRemainingToTransmit;

		/** */
		const unsigned char*									_currentTxDataPtr;

	public:
		/** */
		Driver(	Oscl::Motorola::MPC834x::SPI::RegisterMap&	reg
				) noexcept;
		/** */
		virtual ~Driver(){}

		/** */
		Oscl::Interrupt::StatusHandlerApi&		getISR() noexcept;
		/** */
		Oscl::Interrupt::IsrDsrApi&				getIsrDsr() noexcept;
		/** */
		Oscl::Mt::Runnable&						getRunnable() noexcept;
		/** */
		Oscl::SPI::Master::
		Req::Api::SAP&							getSAP() noexcept;
		/** */
		Oscl::SPI::Master::Api&					getSyncApi() noexcept;

	private:	// Server
		/** */
		void	initialize() noexcept;
		/** */
		void	mboxSignaled() noexcept;

	public:	// Oscl::Mt::Itc::Srv::OpenCloseSyncApi
		/** */
		void	syncOpen() noexcept;
		/** */
		void	syncClose() noexcept;

	private: // Oscl::SPI::Master::Req::Api
		/** */
		void	request(TransferReq& msg) noexcept;
		/** */
		void	request(CancelTransferReq& msg) noexcept;
	};

}
}
}
}
}
}

#endif
