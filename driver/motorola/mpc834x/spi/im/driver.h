/*
   Copyright (C) 2006 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_motorola_mpc834x_spi_im_driverh_
#define _oscl_drv_motorola_mpc834x_spi_im_driverh_
#include "api.h"
#include "oscl/hw/motorola/mpc834x/spireg.h"

/** */
namespace Oscl {
/** */
namespace Motorola {
/** */
namespace MPC834x {
/** */
namespace SPI {
/** */
namespace IM {

/** */
class Driver : public Api {
	private:
		/** */
		volatile Oscl::Motorola::MPC834x::SPI::SPIM::Reg&	_reg;

	public:
		/** */
		Driver(volatile Oscl::Motorola::MPC834x::SPI::SPIM::Reg& reg) noexcept;

	public: // Api
		/** */
		unsigned long	getCurrentMaskState() const noexcept;
		/** */
		void	restoreMaskState(unsigned long state) noexcept;

		/** */
		void	enableAllInterrupts() noexcept;
		/** */
		void	disableAllInterrupts() noexcept;

		/** */
		bool	lastCharacterTransmittedInterruptEnabled() const noexcept;
		/** */
		void	enableLastCharacterTransmittedInterrupt() noexcept;
		/** */
		void	disableLastCharacterTransmittedInterrupt() noexcept;

		/** */
		bool	dataNotReadyWhenSelectedInterruptEnabled() const noexcept;
		/** */
		void	enableDataNotReadyWhenSelectedInterrupt() noexcept;
		/** */
		void	disableDataNotReadyWhenSelectedInterrupt() noexcept;

		/** */
		bool	overrunInterruptEnabled() const noexcept;
		/** */
		void	enableOverrunInterrupt() noexcept;
		/** */
		void	disableOverrunInterrupt() noexcept;

		/** */
		bool	underrunInterruptEnabled() const noexcept;
		/** */
		void	enableUnderrunInterrupt() noexcept;
		/** */
		void	disableUnderrunInterrupt() noexcept;

		/** */
		bool	multipleMasterErrorInterruptEnabled() const noexcept;
		/** */
		void	enableMultipleMasterErrorInterrupt() noexcept;
		/** */
		void	disableMultipleMasterErrorInterrupt() noexcept;

		/** */
		bool	receiverNotReadyInterruptEnabled() const noexcept;
		/** */
		void	enableReceiverNotReadyInterrupt() noexcept;
		/** */
		void	disableReceiverNotReadyInterrupt() noexcept;

		/** */
		bool	transmitterNotFullInterruptEnabled() const noexcept;
		/** */
		void	enableTransmitterNotFullInterrupt() noexcept;
		/** */
		void	disableTransmitterNotFullInterrupt() noexcept;
	};

}
}
}
}
}


#endif
