/*
   Copyright (C) 2006 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "driver.h"

using namespace Oscl::Motorola::MPC834x::SPI::IM;

Driver::Driver(volatile Oscl::Motorola::MPC834x::SPI::SPIM::Reg& reg) noexcept:
		_reg(reg)
		{
	}

unsigned long	Driver::getCurrentMaskState() const noexcept{
	return (unsigned long)_reg;
	}

void	Driver::restoreMaskState(unsigned long state) noexcept{
	_reg	= (Oscl::Motorola::MPC834x::SPI::SPIM::Reg)state;
	}

void	Driver::enableAllInterrupts() noexcept{
	Oscl::Motorola::MPC834x::SPI::SPIM::Reg	value;
	value	= _reg;
	value	&= ~(		Oscl::Motorola::MPC834x::SPI::SPIM::LT::FieldMask
					|	Oscl::Motorola::MPC834x::SPI::SPIM::DNR::FieldMask
					|	Oscl::Motorola::MPC834x::SPI::SPIM::OV::FieldMask
					|	Oscl::Motorola::MPC834x::SPI::SPIM::UN::FieldMask
					|	Oscl::Motorola::MPC834x::SPI::SPIM::MME::FieldMask
					|	Oscl::Motorola::MPC834x::SPI::SPIM::NE::FieldMask
					|	Oscl::Motorola::MPC834x::SPI::SPIM::NF::FieldMask
					);
	value	|= (		Oscl::Motorola::MPC834x::SPI::SPIM::LT::ValueMask_EnableInterrupt
					|	Oscl::Motorola::MPC834x::SPI::SPIM::DNR::ValueMask_EnableInterrupt
					|	Oscl::Motorola::MPC834x::SPI::SPIM::OV::ValueMask_EnableInterrupt
					|	Oscl::Motorola::MPC834x::SPI::SPIM::UN::ValueMask_EnableInterrupt
					|	Oscl::Motorola::MPC834x::SPI::SPIM::MME::ValueMask_EnableInterrupt
					|	Oscl::Motorola::MPC834x::SPI::SPIM::NE::ValueMask_EnableInterrupt
					|	Oscl::Motorola::MPC834x::SPI::SPIM::NF::ValueMask_EnableInterrupt
					);
	_reg	= value;
	}

void	Driver::disableAllInterrupts() noexcept{
	Oscl::Motorola::MPC834x::SPI::SPIM::Reg	value;
	value	= _reg;
	value	&= ~(		Oscl::Motorola::MPC834x::SPI::SPIM::LT::FieldMask
					|	Oscl::Motorola::MPC834x::SPI::SPIM::DNR::FieldMask
					|	Oscl::Motorola::MPC834x::SPI::SPIM::OV::FieldMask
					|	Oscl::Motorola::MPC834x::SPI::SPIM::UN::FieldMask
					|	Oscl::Motorola::MPC834x::SPI::SPIM::MME::FieldMask
					|	Oscl::Motorola::MPC834x::SPI::SPIM::NE::FieldMask
					|	Oscl::Motorola::MPC834x::SPI::SPIM::NF::FieldMask
					);
	value	|= (		Oscl::Motorola::MPC834x::SPI::SPIM::LT::ValueMask_DisableInterrupt
					|	Oscl::Motorola::MPC834x::SPI::SPIM::DNR::ValueMask_DisableInterrupt
					|	Oscl::Motorola::MPC834x::SPI::SPIM::OV::ValueMask_DisableInterrupt
					|	Oscl::Motorola::MPC834x::SPI::SPIM::UN::ValueMask_DisableInterrupt
					|	Oscl::Motorola::MPC834x::SPI::SPIM::MME::ValueMask_DisableInterrupt
					|	Oscl::Motorola::MPC834x::SPI::SPIM::NE::ValueMask_DisableInterrupt
					|	Oscl::Motorola::MPC834x::SPI::SPIM::NF::ValueMask_DisableInterrupt
					);
	_reg	= value;
	}

bool	Driver::lastCharacterTransmittedInterruptEnabled() const noexcept{
	return		(_reg & Oscl::Motorola::MPC834x::SPI::SPIM::LT::FieldMask)
			==	Oscl::Motorola::MPC834x::SPI::SPIM::LT::ValueMask_EnableInterrupt;
	}

void	Driver::enableLastCharacterTransmittedInterrupt() noexcept{
	Oscl::Motorola::MPC834x::SPI::SPIM::Reg	value;
	value	= _reg;
	value	&= ~Oscl::Motorola::MPC834x::SPI::SPIM::LT::FieldMask;
	value	|= Oscl::Motorola::MPC834x::SPI::SPIM::LT::ValueMask_EnableInterrupt;
	_reg	= value;
	}

void	Driver::disableLastCharacterTransmittedInterrupt() noexcept{
	Oscl::Motorola::MPC834x::SPI::SPIM::Reg	value;
	value	= _reg;
	value	&= ~Oscl::Motorola::MPC834x::SPI::SPIM::LT::FieldMask;
	value	|= Oscl::Motorola::MPC834x::SPI::SPIM::LT::ValueMask_DisableInterrupt;
	_reg	= value;
	}

bool	Driver::dataNotReadyWhenSelectedInterruptEnabled() const noexcept{
	return		(_reg & Oscl::Motorola::MPC834x::SPI::SPIM::DNR::FieldMask)
			==	Oscl::Motorola::MPC834x::SPI::SPIM::DNR::ValueMask_EnableInterrupt;
	}

void	Driver::enableDataNotReadyWhenSelectedInterrupt() noexcept{
	Oscl::Motorola::MPC834x::SPI::SPIM::Reg	value;
	value	= _reg;
	value	&= ~Oscl::Motorola::MPC834x::SPI::SPIM::DNR::FieldMask;
	value	|= Oscl::Motorola::MPC834x::SPI::SPIM::DNR::ValueMask_EnableInterrupt;
	_reg	= value;
	}

void	Driver::disableDataNotReadyWhenSelectedInterrupt() noexcept{
	Oscl::Motorola::MPC834x::SPI::SPIM::Reg	value;
	value	= _reg;
	value	&= ~Oscl::Motorola::MPC834x::SPI::SPIM::DNR::FieldMask;
	value	|= Oscl::Motorola::MPC834x::SPI::SPIM::DNR::ValueMask_DisableInterrupt;
	_reg	= value;
	}

bool	Driver::overrunInterruptEnabled() const noexcept{
	return		(_reg & Oscl::Motorola::MPC834x::SPI::SPIM::OV::FieldMask)
			==	Oscl::Motorola::MPC834x::SPI::SPIM::OV::ValueMask_EnableInterrupt;
	}

void	Driver::enableOverrunInterrupt() noexcept{
	Oscl::Motorola::MPC834x::SPI::SPIM::Reg	value;
	value	= _reg;
	value	&= ~Oscl::Motorola::MPC834x::SPI::SPIM::OV::FieldMask;
	value	|= Oscl::Motorola::MPC834x::SPI::SPIM::OV::ValueMask_EnableInterrupt;
	_reg	= value;
	}

void	Driver::disableOverrunInterrupt() noexcept{
	Oscl::Motorola::MPC834x::SPI::SPIM::Reg	value;
	value	= _reg;
	value	&= ~Oscl::Motorola::MPC834x::SPI::SPIM::OV::FieldMask;
	value	|= Oscl::Motorola::MPC834x::SPI::SPIM::OV::ValueMask_DisableInterrupt;
	_reg	= value;
	}

bool	Driver::underrunInterruptEnabled() const noexcept{
	return		(_reg & Oscl::Motorola::MPC834x::SPI::SPIM::UN::FieldMask)
			==	Oscl::Motorola::MPC834x::SPI::SPIM::UN::ValueMask_EnableInterrupt;
	}

void	Driver::enableUnderrunInterrupt() noexcept{
	Oscl::Motorola::MPC834x::SPI::SPIM::Reg	value;
	value	= _reg;
	value	&= ~Oscl::Motorola::MPC834x::SPI::SPIM::UN::FieldMask;
	value	|= Oscl::Motorola::MPC834x::SPI::SPIM::UN::ValueMask_EnableInterrupt;
	_reg	= value;
	}

void	Driver::disableUnderrunInterrupt() noexcept{
	Oscl::Motorola::MPC834x::SPI::SPIM::Reg	value;
	value	= _reg;
	value	&= ~Oscl::Motorola::MPC834x::SPI::SPIM::UN::FieldMask;
	value	|= Oscl::Motorola::MPC834x::SPI::SPIM::UN::ValueMask_DisableInterrupt;
	_reg	= value;
	}

bool	Driver::multipleMasterErrorInterruptEnabled() const noexcept{
	return		(_reg & Oscl::Motorola::MPC834x::SPI::SPIM::MME::FieldMask)
			==	Oscl::Motorola::MPC834x::SPI::SPIM::MME::ValueMask_EnableInterrupt;
	}

void	Driver::enableMultipleMasterErrorInterrupt() noexcept{
	Oscl::Motorola::MPC834x::SPI::SPIM::Reg	value;
	value	= _reg;
	value	&= ~Oscl::Motorola::MPC834x::SPI::SPIM::MME::FieldMask;
	value	|= Oscl::Motorola::MPC834x::SPI::SPIM::MME::ValueMask_EnableInterrupt;
	_reg	= value;
	}

void	Driver::disableMultipleMasterErrorInterrupt() noexcept{
	Oscl::Motorola::MPC834x::SPI::SPIM::Reg	value;
	value	= _reg;
	value	&= ~Oscl::Motorola::MPC834x::SPI::SPIM::MME::FieldMask;
	value	|= Oscl::Motorola::MPC834x::SPI::SPIM::MME::ValueMask_DisableInterrupt;
	_reg	= value;
	}

bool	Driver::receiverNotReadyInterruptEnabled() const noexcept{
	return		(_reg & Oscl::Motorola::MPC834x::SPI::SPIM::NE::FieldMask)
			==	Oscl::Motorola::MPC834x::SPI::SPIM::NE::ValueMask_EnableInterrupt;
	}

void	Driver::enableReceiverNotReadyInterrupt() noexcept{
	Oscl::Motorola::MPC834x::SPI::SPIM::Reg	value;
	value	= _reg;
	value	&= ~Oscl::Motorola::MPC834x::SPI::SPIM::NE::FieldMask;
	value	|= Oscl::Motorola::MPC834x::SPI::SPIM::NE::ValueMask_EnableInterrupt;
	_reg	= value;
	}

void	Driver::disableReceiverNotReadyInterrupt() noexcept{
	Oscl::Motorola::MPC834x::SPI::SPIM::Reg	value;
	value	= _reg;
	value	&= ~Oscl::Motorola::MPC834x::SPI::SPIM::NE::FieldMask;
	value	|= Oscl::Motorola::MPC834x::SPI::SPIM::NE::ValueMask_DisableInterrupt;
	_reg	= value;
	}

bool	Driver::transmitterNotFullInterruptEnabled() const noexcept{
	return		(_reg & Oscl::Motorola::MPC834x::SPI::SPIM::NF::FieldMask)
			==	Oscl::Motorola::MPC834x::SPI::SPIM::NF::ValueMask_EnableInterrupt;
	}

void	Driver::enableTransmitterNotFullInterrupt() noexcept{
	Oscl::Motorola::MPC834x::SPI::SPIM::Reg	value;
	value	= _reg;
	value	&= ~Oscl::Motorola::MPC834x::SPI::SPIM::NF::FieldMask;
	value	|= Oscl::Motorola::MPC834x::SPI::SPIM::NF::ValueMask_EnableInterrupt;
	_reg	= value;
	}

void	Driver::disableTransmitterNotFullInterrupt() noexcept{
	Oscl::Motorola::MPC834x::SPI::SPIM::Reg	value;
	value	= _reg;
	value	&= ~Oscl::Motorola::MPC834x::SPI::SPIM::NF::FieldMask;
	value	|= Oscl::Motorola::MPC834x::SPI::SPIM::NF::ValueMask_DisableInterrupt;
	_reg	= value;
	}


