/*
   Copyright (C) 2006 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_motorola_mpc834x_spi_im_apih_
#define _oscl_drv_motorola_mpc834x_spi_im_apih_

/** */
namespace Oscl {
/** */
namespace Motorola {
/** */
namespace MPC834x {
/** */
namespace SPI {
/** */
namespace IM {

/** */
class Api {
	public:
		/** */
		virtual ~Api(){}

		/** */
		virtual unsigned long	getCurrentMaskState() const noexcept=0;
		/** */
		virtual void	restoreMaskState(unsigned long state) noexcept=0;

		/** */
		virtual void	enableAllInterrupts() noexcept=0;
		/** */
		virtual void	disableAllInterrupts() noexcept=0;

		/** */
		virtual bool	lastCharacterTransmittedInterruptEnabled() const noexcept=0;
		/** */
		virtual void	enableLastCharacterTransmittedInterrupt() noexcept=0;
		/** */
		virtual void	disableLastCharacterTransmittedInterrupt() noexcept=0;

		/** */
		virtual bool	dataNotReadyWhenSelectedInterruptEnabled() const noexcept=0;
		/** */
		virtual void	enableDataNotReadyWhenSelectedInterrupt() noexcept=0;
		/** */
		virtual void	disableDataNotReadyWhenSelectedInterrupt() noexcept=0;

		/** */
		virtual bool	overrunInterruptEnabled() const noexcept=0;
		/** */
		virtual void	enableOverrunInterrupt() noexcept=0;
		/** */
		virtual void	disableOverrunInterrupt() noexcept=0;

		/** */
		virtual bool	underrunInterruptEnabled() const noexcept=0;
		/** */
		virtual void	enableUnderrunInterrupt() noexcept=0;
		/** */
		virtual void	disableUnderrunInterrupt() noexcept=0;

		/** */
		virtual bool	multipleMasterErrorInterruptEnabled() const noexcept=0;
		/** */
		virtual void	enableMultipleMasterErrorInterrupt() noexcept=0;
		/** */
		virtual void	disableMultipleMasterErrorInterrupt() noexcept=0;

		/** */
		virtual bool	receiverNotReadyInterruptEnabled() const noexcept=0;
		/** */
		virtual void	enableReceiverNotReadyInterrupt() noexcept=0;
		/** */
		virtual void	disableReceiverNotReadyInterrupt() noexcept=0;

		/** */
		virtual bool	transmitterNotFullInterruptEnabled() const noexcept=0;
		/** */
		virtual void	enableTransmitterNotFullInterrupt() noexcept=0;
		/** */
		virtual void	disableTransmitterNotFullInterrupt() noexcept=0;
	};

}
}
}
}
}


#endif
