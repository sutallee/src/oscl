/*
   Copyright (C) 2006 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_motorola_mpc834x_pit_ptcnrh_
#define _oscl_drv_motorola_mpc834x_pit_ptcnrh_
#include "oscl/hw/motorola/mpc834x/pitreg.h"

/** */
namespace Oscl {
/** */
namespace Motorola {
/** */
namespace MPC834x {
/** */
namespace PIT {

/** */
class Ptcnr {
	private:
		/** */
		volatile Oscl::Motorola::MPC834x::PIT::PTCNR::Reg&	_ptcnr;

	public:
		/** */
		Ptcnr(	volatile Oscl::Motorola::MPC834x::PIT::PTCNR::Reg&	ptcnr
				) noexcept;
		/** */
		bool	counterIsEnabled() const noexcept;
		/** */
		void	enableCounter() noexcept;
		/** */
		void	disableCounter() noexcept;
		/** */
		bool	inputClockIsTheExternalPitClock() const noexcept;
		/** */
		void	inputFromInternalSystemClock() noexcept;
		/** */
		void	inputFromExternalPitClock() noexcept;
		/** */
		bool	periodicInterruptEnabled() const noexcept;
		/** */
		void	enablePeriodicInterrupt() noexcept;
		/** */
		void	disablePeriodicInterrupt() noexcept;
	};

}
}
}
}

#endif
