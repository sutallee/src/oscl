/*
   Copyright (C) 2006 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "driver.h"

using namespace Oscl::Motorola::MPC834x::PIT;

Driver::Driver(	volatile Oscl::Motorola::MPC834x::PIT::PTCNR::Reg&	ptcnr,
				volatile Oscl::Motorola::MPC834x::PIT::PTLDR::Reg&	ptldr,
				volatile Oscl::Motorola::MPC834x::PIT::PTPSR::Reg&	ptpsr,
				volatile Oscl::Motorola::MPC834x::PIT::PTCTR::Reg&	ptctr,
				volatile Oscl::Motorola::MPC834x::PIT::PTEVR::Reg&	ptevr,
				uint32_t									period,
				uint16_t									prescaler,
				bool												useExternalClock
				) noexcept:
		_ptcnr(ptcnr),
		_ptevr(ptevr)
		{
	stop();
	if(useExternalClock){
		_ptcnr.inputFromExternalPitClock();
		}
	else {
		_ptcnr.inputFromInternalSystemClock();
		}
	Oscl::Motorola::MPC834x::PIT::PTPSR::Reg	psr;
	psr	= ptpsr;
	psr	&= ~Oscl::Motorola::MPC834x::PIT::PTPSR::PRSC::FieldMask;
	psr	|=		( prescaler << Oscl::Motorola::MPC834x::PIT::PTPSR::PRSC::Lsb)
			&	Oscl::Motorola::MPC834x::PIT::PTPSR::PRSC::FieldMask
			;
	ptpsr	= psr;

	Oscl::Motorola::MPC834x::PIT::PTLDR::Reg	ldr;
	ldr	= ptldr;
	ldr	&= ~Oscl::Motorola::MPC834x::PIT::PTLDR::CLDV::FieldMask;
	ldr	|=		( period << Oscl::Motorola::MPC834x::PIT::PTLDR::CLDV::Lsb)
			&	Oscl::Motorola::MPC834x::PIT::PTLDR::CLDV::FieldMask
			;
	ptldr	= ldr;
	}

void	Driver::start() noexcept{
	stop();
	_ptcnr.enablePeriodicInterrupt();
	_ptcnr.enableCounter();
	}

void	Driver::stop() noexcept{
	_ptcnr.disablePeriodicInterrupt();
	_ptcnr.disableCounter();
	_ptevr.clearPeriodicInterrupt();
	}

void	Driver::attach(Observer& observer) noexcept{
	_observers.put(&observer);
	}

void	Driver::detach(Observer& observer) noexcept{
	_observers.remove(&observer);
	}

bool	Driver::interrupt() noexcept{
	if(!_ptevr.periodicInterruptIsPending()){
		return false;
		}
	_ptevr.clearPeriodicInterrupt();
	Observer*	observer;
	for(	observer=_observers.first();
			observer;
			observer=_observers.next(observer)
			){
		observer->tick();
		}
	return true;
	}

