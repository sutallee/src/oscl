/*
   Copyright (C) 2006 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_motorola_mpc834x_pit_driverh_
#define _oscl_drv_motorola_mpc834x_pit_driverh_
#include "api.h"
#include "oscl/hw/motorola/mpc834x/pitreg.h"
#include "oscl/interrupt/shandler.h"
#include "oscl/queue/queue.h"
#include "oscl/queue/queueitem.h"
#include "ptcnr.h"
#include "ptevr.h"

/** */
namespace Oscl {
/** */
namespace Motorola {
/** */
namespace MPC834x {
/** */
namespace PIT {

/** */
class Driver : public Api {
	private:
		/** */
		Ptcnr					_ptcnr;

		/** */
		Ptevr					_ptevr;

		/** */
		Oscl::Queue<Observer>	_observers;

	public:
		/** */
		Driver(	volatile Oscl::Motorola::MPC834x::PIT::PTCNR::Reg&	ptcnr,
				volatile Oscl::Motorola::MPC834x::PIT::PTLDR::Reg&	ptldr,
				volatile Oscl::Motorola::MPC834x::PIT::PTPSR::Reg&	ptpsr,
				volatile Oscl::Motorola::MPC834x::PIT::PTCTR::Reg&	ptctr,
				volatile Oscl::Motorola::MPC834x::PIT::PTEVR::Reg&	ptevr,
				uint32_t									period,
				uint16_t									prescaler,
				bool												useExternalClock
				) noexcept;

		/** */
		void	start() noexcept;

		/** */
		void	stop() noexcept;

		/** */
		void	attach(Observer& observer) noexcept;

		/** */
		void	detach(Observer& observer) noexcept;

	private:
		/** */
		bool	interrupt() noexcept;
	};

}
}
}
}

#endif
