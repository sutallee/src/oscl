/*
   Copyright (C) 2006 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_motorola_mpc834x_i2c_cr_driverh_
#define _oscl_drv_motorola_mpc834x_i2c_cr_driverh_
#include "api.h"
#include "oscl/hw/motorola/mpc834x/i2creg.h"

/** */
namespace Oscl {
/** */
namespace Motorola {
/** */
namespace MPC834x {
/** */
namespace I2C {
/** */
namespace CR {

/** */
class Driver : public Api {
	private:
		/** */
		volatile Oscl::Motorola::MPC834x::I2C::I2CCR::Reg&	_reg;

	public:
		/** */
		Driver(volatile Oscl::Motorola::MPC834x::I2C::I2CCR::Reg& reg) noexcept;

	public: // Api
		/** */
		bool	moduleEnabled() const noexcept;
		/** */
		void	enableModule() noexcept;
		/** */
		void	disableModule() noexcept;
		/** */
		bool	interruptEnabled() const noexcept;
		/** */
		void	enableInterrupt() noexcept;
		/** */
		void	disableInterrupt() noexcept;
		/** */
		bool	masterModeSelected() const noexcept;
		/** */
		void	selectMasterMode() noexcept;
		/** */
		void	selectSlaveMode() noexcept;
		/** */
		bool	transmitModeSelected() const noexcept;
		/** */
		void	selectReceiveMode() noexcept;
		/** */
		void	selectTransmitMode() noexcept;
		/** */
		bool	sendAckModeEnabled() const noexcept;
		/** */
		void	enableSendAckMode() noexcept;
		/** */
		void	disableSendAckMode() noexcept;
		/** */
		void	sendRepeatStartCondition() noexcept;
		/** */
		bool	broadcastAcceptModeEnabled() const noexcept;
		/** */
		void	enableBroadcastAcceptMode() noexcept;
		/** */
		void	disabledBroadcastAcceptMode() noexcept;
	};

}
}
}
}
}


#endif
