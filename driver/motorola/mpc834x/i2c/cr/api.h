/*
   Copyright (C) 2006 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_motorola_mpc834x_i2c_cr_apih_
#define _oscl_drv_motorola_mpc834x_i2c_cr_apih_

/** */
namespace Oscl {
/** */
namespace Motorola {
/** */
namespace MPC834x {
/** */
namespace I2C {
/** */
namespace CR {

/** */
class Api {
	public:
		/** */
		virtual ~Api(){}

		/** */
		virtual bool	moduleEnabled() const noexcept=0;
		/** */
		virtual void	enableModule() noexcept=0;
		/** */
		virtual void	disableModule() noexcept=0;
		/** */
		virtual bool	interruptEnabled() const noexcept=0;
		/** */
		virtual void	enableInterrupt() noexcept=0;
		/** */
		virtual void	disableInterrupt() noexcept=0;
		/** */
		virtual bool	masterModeSelected() const noexcept=0;
		/** */
		virtual void	selectMasterMode() noexcept=0;
		/** */
		virtual void	selectSlaveMode() noexcept=0;
		/** */
		virtual bool	transmitModeSelected() const noexcept=0;
		/** */
		virtual void	selectReceiveMode() noexcept=0;
		/** */
		virtual void	selectTransmitMode() noexcept=0;
		/** */
		virtual bool	sendAckModeEnabled() const noexcept=0;
		/** */
		virtual void	enableSendAckMode() noexcept=0;
		/** */
		virtual void	disableSendAckMode() noexcept=0;
		/** */
		virtual void	sendRepeatStartCondition() noexcept=0;
		/** */
		virtual bool	broadcastAcceptModeEnabled() const noexcept=0;
		/** */
		virtual void	enableBroadcastAcceptMode() noexcept=0;
		/** */
		virtual void	disabledBroadcastAcceptMode() noexcept=0;
	};

}
}
}
}
}


#endif
