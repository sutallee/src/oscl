/*
   Copyright (C) 2006 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "driver.h"

using namespace Oscl::Motorola::MPC834x::I2C::CR;

Driver::Driver(volatile Oscl::Motorola::MPC834x::I2C::I2CCR::Reg& reg) noexcept:
		_reg(reg)
		{
	}

bool	Driver::moduleEnabled() const noexcept{
	return		(_reg & Oscl::Motorola::MPC834x::I2C::I2CCR::MEN::FieldMask)
			==	Oscl::Motorola::MPC834x::I2C::I2CCR::MEN::ValueMask_ModuleEnable;
	}

void	Driver::enableModule() noexcept{
	Oscl::Motorola::MPC834x::I2C::I2CCR::Reg	value;
	value	= _reg;
	value	&= ~Oscl::Motorola::MPC834x::I2C::I2CCR::MEN::FieldMask;
	value	|= Oscl::Motorola::MPC834x::I2C::I2CCR::MEN::ValueMask_ModuleEnable;
	_reg	= value;
	}

void	Driver::disableModule() noexcept{
	Oscl::Motorola::MPC834x::I2C::I2CCR::Reg	value;
	value	= _reg;
	value	&= ~Oscl::Motorola::MPC834x::I2C::I2CCR::MEN::FieldMask;
	value	|= Oscl::Motorola::MPC834x::I2C::I2CCR::MEN::ValueMask_ModuleDisable;
	_reg	= value;
	}

bool	Driver::interruptEnabled() const noexcept{
	return		(_reg & Oscl::Motorola::MPC834x::I2C::I2CCR::MIEN::FieldMask)
			==	Oscl::Motorola::MPC834x::I2C::I2CCR::MIEN::ValueMask_InterruptEnable;
	}

void	Driver::enableInterrupt() noexcept{
	Oscl::Motorola::MPC834x::I2C::I2CCR::Reg	value;
	value	= _reg;
	value	&= ~Oscl::Motorola::MPC834x::I2C::I2CCR::MIEN::FieldMask;
	value	|= Oscl::Motorola::MPC834x::I2C::I2CCR::MIEN::ValueMask_InterruptEnable;
	_reg	= value;
	}

void	Driver::disableInterrupt() noexcept{
	Oscl::Motorola::MPC834x::I2C::I2CCR::Reg	value;
	value	= _reg;
	value	&= ~Oscl::Motorola::MPC834x::I2C::I2CCR::MIEN::FieldMask;
	value	|= Oscl::Motorola::MPC834x::I2C::I2CCR::MIEN::ValueMask_InterruptDisable;
	_reg	= value;
	}

bool	Driver::masterModeSelected() const noexcept{
	return		(_reg & Oscl::Motorola::MPC834x::I2C::I2CCR::MSTA::FieldMask)
			==	Oscl::Motorola::MPC834x::I2C::I2CCR::MSTA::ValueMask_MasterMode;
	}

void	Driver::selectMasterMode() noexcept{
	Oscl::Motorola::MPC834x::I2C::I2CCR::Reg	value;
	value	= _reg;
	value	&= ~Oscl::Motorola::MPC834x::I2C::I2CCR::MSTA::FieldMask;
	value	|= Oscl::Motorola::MPC834x::I2C::I2CCR::MSTA::ValueMask_MasterMode;
	_reg	= value;
	}

void	Driver::selectSlaveMode() noexcept{
	Oscl::Motorola::MPC834x::I2C::I2CCR::Reg	value;
	value	= _reg;
	value	&= ~Oscl::Motorola::MPC834x::I2C::I2CCR::MSTA::FieldMask;
	value	|= Oscl::Motorola::MPC834x::I2C::I2CCR::MSTA::ValueMask_SlaveMode;
	_reg	= value;
	}

bool	Driver::transmitModeSelected() const noexcept{
	return		(_reg & Oscl::Motorola::MPC834x::I2C::I2CCR::MTX::FieldMask)
			==	Oscl::Motorola::MPC834x::I2C::I2CCR::MTX::ValueMask_TransmitMode;
	}

void	Driver::selectReceiveMode() noexcept{
	Oscl::Motorola::MPC834x::I2C::I2CCR::Reg	value;
	value	= _reg;
	value	&= ~Oscl::Motorola::MPC834x::I2C::I2CCR::MTX::FieldMask;
	value	|= Oscl::Motorola::MPC834x::I2C::I2CCR::MTX::ValueMask_ReceiveMode;
	_reg	= value;
	}

void	Driver::selectTransmitMode() noexcept{
	Oscl::Motorola::MPC834x::I2C::I2CCR::Reg	value;
	value	= _reg;
	value	&= ~Oscl::Motorola::MPC834x::I2C::I2CCR::MTX::FieldMask;
	value	|= Oscl::Motorola::MPC834x::I2C::I2CCR::MTX::ValueMask_TransmitMode;
	_reg	= value;
	}

bool	Driver::sendAckModeEnabled() const noexcept{
	return		(_reg & Oscl::Motorola::MPC834x::I2C::I2CCR::TXAK::FieldMask)
			==	Oscl::Motorola::MPC834x::I2C::I2CCR::TXAK::ValueMask_SendAckAt9thBitOnRx;
	}

void	Driver::enableSendAckMode() noexcept{
	Oscl::Motorola::MPC834x::I2C::I2CCR::Reg	value;
	value	= _reg;
	value	&= ~Oscl::Motorola::MPC834x::I2C::I2CCR::TXAK::FieldMask;
	value	|= Oscl::Motorola::MPC834x::I2C::I2CCR::TXAK::ValueMask_SendAckAt9thBitOnRx;
	_reg	= value;
	}

void	Driver::disableSendAckMode() noexcept{
	Oscl::Motorola::MPC834x::I2C::I2CCR::Reg	value;
	value	= _reg;
	value	&= ~Oscl::Motorola::MPC834x::I2C::I2CCR::TXAK::FieldMask;
	value	|= Oscl::Motorola::MPC834x::I2C::I2CCR::TXAK::ValueMask_NoAckSent;
	_reg	= value;
	}

void	Driver::sendRepeatStartCondition() noexcept{
	Oscl::Motorola::MPC834x::I2C::I2CCR::Reg	value;
	value	= _reg;
	value	&= ~Oscl::Motorola::MPC834x::I2C::I2CCR::RSTA::FieldMask;
	value	|= Oscl::Motorola::MPC834x::I2C::I2CCR::RSTA::ValueMask_GenerateRepeatStartCondition;
	_reg	= value;
	}

bool	Driver::broadcastAcceptModeEnabled() const noexcept{
	return		(_reg & Oscl::Motorola::MPC834x::I2C::I2CCR::BCST::FieldMask)
			==	Oscl::Motorola::MPC834x::I2C::I2CCR::BCST::ValueMask_BroadcastRxEnable;
	}

void	Driver::enableBroadcastAcceptMode() noexcept{
	Oscl::Motorola::MPC834x::I2C::I2CCR::Reg	value;
	value	= _reg;
	value	&= ~Oscl::Motorola::MPC834x::I2C::I2CCR::BCST::FieldMask;
	value	|= Oscl::Motorola::MPC834x::I2C::I2CCR::BCST::ValueMask_BroadcastRxEnable;
	_reg	= value;
	}

void	Driver::disabledBroadcastAcceptMode() noexcept{
	Oscl::Motorola::MPC834x::I2C::I2CCR::Reg	value;
	value	= _reg;
	value	&= ~Oscl::Motorola::MPC834x::I2C::I2CCR::BCST::FieldMask;
	value	|= Oscl::Motorola::MPC834x::I2C::I2CCR::BCST::ValueMask_BroadcastRxDisable;
	_reg	= value;
	}

