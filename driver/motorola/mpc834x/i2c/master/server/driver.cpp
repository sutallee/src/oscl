/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "driver.h"
#include "oscl/hw/motorola/mpc834x/i2creg.h"

using namespace Oscl::Motorola::MPC834x::I2C::Master::Server;

Driver::Driver(	Oscl::Motorola::MPC834x::I2C::RegisterMap&	reg
				) noexcept:
		_reg(reg),
		_srDriver(reg.i2csr),
		_crDriver(reg.i2ccr),
		_interrupt(_srDriver,*this),
		_sync(*this,*this),
		_readMsg(0),
		_writeMsg(0),
		_currentData(0),
		_currentRemaining(0),
		_read(true),
		_count(0)
		{
	}

void Driver::initialize() noexcept{
	// Initialize hardware
	// CSB is 128MHz
	// Standard maximum SCLK freq is 100KHz
	// Thus choose a divider greather than 128MHz/100KHz = 1280
	// According to the MPC834x user manual table 17-5,
	// the nearest divisor is 1280. To give myself a margin of
	// error, I'll choose somethinig that is less than this, say half.
	// The table says 2560 is available for 50KHz.
	_reg.i2cfdr	= Oscl::Motorola::MPC834x::I2C::I2CFDR::FDR::ValueMask_DivideBy2560;
	_crDriver.disableModule();
	_crDriver.disabledBroadcastAcceptMode();
	_crDriver.enableInterrupt();
	_crDriver.selectTransmitMode();
	_crDriver.enableSendAckMode();
	_crDriver.selectSlaveMode();	// Start/Stop condition setup
	_srDriver.clearArbitrationLost();
	_srDriver.clearInterrupt();
	_crDriver.enableModule();
	}

void	Driver::mboxSignaled() noexcept{
	++_count;
	if(!_crDriver.masterModeSelected()){
		if(_srDriver.arbitrationLost()){
			_srDriver.clearArbitrationLost();
			if(_srDriver.addressedAsSlave()){
				}
			else {
				// NOT addressed as slave
				}
			return;
			}
		else {
			// Arbitration NOT lost
			}
		// nuttin to do. assumed to be end of send STOP condition
		return;
		}
	if(_crDriver.transmitModeSelected()){
		// End of a byte transmit
		if(_srDriver.receivedByteAcknowleged()){
			if(_read){
				_crDriver.selectReceiveMode();
				if(_currentRemaining){
					if(_currentRemaining==1){
						_crDriver.disableSendAckMode();
						}
					else {
						_crDriver.enableSendAckMode();
						}
					*_currentData		= _reg.i2cdr;	// dummy read
					return;
					}
				else {
					_crDriver.selectSlaveMode(); // Generate STOP
					if(_readMsg){
						_readMsg->_payload._failed	= false;
						_readMsg->returnToSender();
						_readMsg	= 0;
						return;
						}
					}
				}
			else {
				if(_currentRemaining == 0){
					_crDriver.selectSlaveMode(); // Generate STOP
					if(_writeMsg){
						_writeMsg->_payload._failed	= false;
						_writeMsg->returnToSender();
						}
					return;
					}
				_reg.i2cdr	= *_currentData;
				++_currentData;
				--_currentRemaining;
				}
			}
		else {
			// byte NOT acknowledged
			_crDriver.selectSlaveMode(); // Generate STOP
			if(_readMsg){
				_readMsg->_payload._failed	= true;
				_readMsg->returnToSender();
				_readMsg	= 0;
				return;
				}
			if(_writeMsg){
				_writeMsg->_payload._failed	= true;
				_writeMsg->returnToSender();
				_writeMsg	= 0;
				return;
				}
			}
		}
	else {
		// End of a byte receive
		if(_currentRemaining==1){
			_crDriver.selectSlaveMode(); // Generate STOP
			}
		if(_currentRemaining==2){
			_crDriver.disableSendAckMode();
			}
		*_currentData	= _reg.i2cdr;
		if(_currentRemaining > 1){
			--_currentRemaining;
			++_currentData;
			}
		else {
			if(_readMsg){
				_readMsg->_payload._failed	= false;
				_readMsg->returnToSender();
				_readMsg	= 0;
				}
			}
		}
	}

Oscl::Interrupt::StatusHandlerApi&	Driver::getISR() noexcept{
	return _interrupt;
	}

Oscl::Interrupt::IsrDsrApi&	Driver::getIsrDsr() noexcept{
	return _interrupt;
	}

Oscl::Mt::Runnable&	Driver::getRunnable() noexcept{
	return *this;
	}

Oscl::I2C::Master::SevenBit::Req::Api::SAP& Driver::getSevenBitSAP() noexcept{
	return _sync.getSAP();
	}

Oscl::I2C::Master::SevenBit::Api&	Driver::getSevenBitSyncApi() noexcept{
	return _sync;
	}

void	Driver::syncOpen() noexcept{
	}

void	Driver::syncClose() noexcept{
	}

void	Driver::request(Oscl::I2C::Master::SevenBit::Req::Api::ReadReq& msg) noexcept{
	_count	= 0;
	_readMsg	= &msg;
	while(_srDriver.busIsBusy());
	_crDriver.selectMasterMode();
	_crDriver.selectTransmitMode();
	_currentData		= (unsigned char*)msg._payload._dataBuffer;
	_currentRemaining	= msg._payload._nDataBytesToRead;
	_read				= true;
	_reg.i2cdr			= msg._payload._address | 0x01;
	}

void	Driver::request(Oscl::I2C::Master::SevenBit::Req::Api::CancelReadReq& msg) noexcept{
	}

void	Driver::request(Oscl::I2C::Master::SevenBit::Req::Api::WriteReq& msg) noexcept{
	_count	= 0;
	_writeMsg	= &msg;
	while(_srDriver.busIsBusy());
	_crDriver.selectMasterMode();
	_crDriver.selectTransmitMode();
	_currentData		= (unsigned char*)msg._payload._data;
	_currentRemaining	= msg._payload._nDataBytes;
	_read				= false;
	_reg.i2cdr			= msg._payload._address;
	}

void	Driver::request(Oscl::I2C::Master::SevenBit::Req::Api::CancelWriteReq& msg) noexcept{
	}

