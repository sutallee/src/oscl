/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_motorola_mpc834x_i2c_master_server_driverh_
#define _oscl_drv_motorola_mpc834x_i2c_master_server_driverh_

#include "oscl/mt/itc/srv/ocsyncapi.h"
#include "oscl/mt/itc/mbox/server.h"
#include "oscl/hw/motorola/mpc834x/i2c.h"
#include "oscl/i2c/master/sevenbit/itc/reqapi.h"
#include "oscl/i2c/master/sevenbit/itc/sync.h"
#include "oscl/driver/motorola/mpc834x/i2c/cr/driver.h"
#include "oscl/driver/motorola/mpc834x/i2c/sr/driver.h"
#include "interrupt.h"

/** */
namespace Oscl {
/** */
namespace Motorola {
/** */
namespace MPC834x {
/** */
namespace I2C {
/** */
namespace Master {
/** */
namespace Server {

/** */
class Driver :	public Oscl::Mt::Itc::Server,
				public Oscl::Mt::Itc::Srv::OpenCloseSyncApi,
				private Oscl::I2C::Master::SevenBit::Req::Api
				{
	private:
		/** */
		Oscl::Motorola::MPC834x::I2C::RegisterMap&			_reg;
		/** */
		Oscl::Motorola::MPC834x::I2C::SR::Driver			_srDriver;
		/** */
		Oscl::Motorola::MPC834x::I2C::CR::Driver			_crDriver;
		/** */
		Interrupt											_interrupt;
		/** */
		Oscl::I2C::Master::SevenBit::Sync					_sync;

		/** temp */
		Oscl::I2C::Master::SevenBit::Req::Api::ReadReq*		_readMsg;

		/** temp */
		Oscl::I2C::Master::SevenBit::Req::Api::WriteReq*	_writeMsg;

		/** */
		unsigned char*										_currentData;

		/** */
		unsigned 											_currentRemaining;

		/** */
		bool												_read;

		/** */
		unsigned long										_count;
	public:
		/** */
		Driver(	Oscl::Motorola::MPC834x::I2C::RegisterMap&	reg
				) noexcept;
		/** */
		virtual ~Driver(){}

		/** */
		Oscl::Interrupt::StatusHandlerApi&		getISR() noexcept;
		/** */
		Oscl::Interrupt::IsrDsrApi&				getIsrDsr() noexcept;
		/** */
		Oscl::Mt::Runnable&						getRunnable() noexcept;
		/** */
		Oscl::I2C::Master::SevenBit::
		Req::Api::SAP&							getSevenBitSAP() noexcept;
		/** */
		Oscl::I2C::Master::SevenBit::Api&		getSevenBitSyncApi() noexcept;

	private:	// Server
		/** */
		void	initialize() noexcept;
		/** */
		void	mboxSignaled() noexcept;

	public:	// Oscl::Mt::Itc::Srv::OpenCloseSyncApi
		/** */
		void	syncOpen() noexcept;
		/** */
		void	syncClose() noexcept;

	private: // Oscl::I2C::Master::SevenBit::Req::Api
		/** */
		void	request(ReadReq& msg) noexcept;
		/** */
		void	request(CancelReadReq& msg) noexcept;
		/** */
		void	request(WriteReq& msg) noexcept;
		/** */
		void	request(CancelWriteReq& msg) noexcept;
	};

}
}
}
}
}
}

#endif
