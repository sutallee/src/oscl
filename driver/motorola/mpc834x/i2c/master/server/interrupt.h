/*
   Copyright (C) 2006 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_motorola_mpc834x_i2c_master_server_interrupth_
#define _oscl_drv_motorola_mpc834x_i2c_master_server_interrupth_
#include "oscl/mt/sema/sigapi.h"
#include "oscl/interrupt/shandler.h"
#include "oscl/interrupt/isrdsr.h"
#include "oscl/driver/motorola/mpc834x/i2c/sr/api.h"

/** */
namespace Oscl {
/** */
namespace Motorola {
/** */
namespace MPC834x {
/** */
namespace I2C {
/** */
namespace Master {
/** */
namespace Server {

/** */
class Interrupt :	public Oscl::Interrupt::StatusHandlerApi,
					public Oscl::Interrupt::IsrDsrApi
					{
	private:
		/** */
		Oscl::Motorola::MPC834x::I2C::SR::Api&	_srApi;
		/** */
		Oscl::Mt::Sema::SignalApi&				_drvSema;

	public:
		/** */
		Interrupt(	Oscl::Motorola::MPC834x::I2C::SR::Api&	srApi,
					Oscl::Mt::Sema::SignalApi&				drvSema
					) noexcept;

	private:	// Oscl::Interrupt::StatusHandlerApi
		/** */
		bool	interrupt() noexcept;

	private:	// Oscl::Interrupt::IsrDsrApi
		/** */
		bool	isr() noexcept;

		/** */
		void	dsr() noexcept;
	};

}
}
}
}
}
}

#endif
