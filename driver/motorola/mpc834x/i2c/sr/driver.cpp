/*
   Copyright (C) 2006 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "driver.h"

using namespace Oscl::Motorola::MPC834x::I2C::SR;

Driver::Driver(volatile Oscl::Motorola::MPC834x::I2C::I2CSR::Reg& reg) noexcept:
		_reg(reg)
		{
	}

bool	Driver::byteTransferComplete() const noexcept{
	return		(_reg & Oscl::Motorola::MPC834x::I2C::I2CSR::MCF::FieldMask)
			==	Oscl::Motorola::MPC834x::I2C::I2CSR::MCF::ValueMask_ByteTransferComplete;
	}

bool	Driver::addressedAsSlave() const noexcept{
	return		(_reg & Oscl::Motorola::MPC834x::I2C::I2CSR::MAAS::FieldMask)
			==	Oscl::Motorola::MPC834x::I2C::I2CSR::MAAS::ValueMask_AddressedAsSlave;
	}

bool	Driver::busIsBusy() const noexcept{
	return		(_reg & Oscl::Motorola::MPC834x::I2C::I2CSR::MBB::FieldMask)
			==	Oscl::Motorola::MPC834x::I2C::I2CSR::MBB::ValueMask_BusIsBusy;
	}

bool	Driver::arbitrationLost() const noexcept{
	return		(_reg & Oscl::Motorola::MPC834x::I2C::I2CSR::MAL::FieldMask)
			==	Oscl::Motorola::MPC834x::I2C::I2CSR::MAL::ValueMask_ArbitrationLost;
	}

void	Driver::clearArbitrationLost() noexcept{
	Oscl::Motorola::MPC834x::I2C::I2CSR::Reg	value;
	value	= _reg;
	value	&= ~Oscl::Motorola::MPC834x::I2C::I2CSR::MAL::FieldMask;
	value	|= Oscl::Motorola::MPC834x::I2C::I2CSR::MAL::ValueMask_Clear;
	_reg	= value;
	}

bool	Driver::broadcastMatch() const noexcept{
	return		(_reg & Oscl::Motorola::MPC834x::I2C::I2CSR::BCSTM::FieldMask)
			==	Oscl::Motorola::MPC834x::I2C::I2CSR::BCSTM::ValueMask_BroadcastMatch;
	}

bool	Driver::isSlaveReadRequest() const noexcept{
	return		(_reg & Oscl::Motorola::MPC834x::I2C::I2CSR::SRW::FieldMask)
			==	Oscl::Motorola::MPC834x::I2C::I2CSR::SRW::ValueMask_Read;
	}

bool	Driver::interruptPending() const noexcept{
	return		(_reg & Oscl::Motorola::MPC834x::I2C::I2CSR::MIF::FieldMask)
			==	Oscl::Motorola::MPC834x::I2C::I2CSR::MIF::ValueMask_Pending;
	}

void	Driver::clearInterrupt() noexcept{
	Oscl::Motorola::MPC834x::I2C::I2CSR::Reg	value;
	value	= _reg;
	value	&= ~Oscl::Motorola::MPC834x::I2C::I2CSR::MIF::FieldMask;
	value	|= Oscl::Motorola::MPC834x::I2C::I2CSR::MIF::ValueMask_Clear;
	_reg	= value;
	}

bool	Driver::receivedByteAcknowleged() const noexcept{
	return		(_reg & Oscl::Motorola::MPC834x::I2C::I2CSR::RXAK::FieldMask)
			==	Oscl::Motorola::MPC834x::I2C::I2CSR::RXAK::ValueMask_AckReceived;
	}

