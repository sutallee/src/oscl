/*
   Copyright (C) 2006 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_motorola_mpc834x_gpio_apih_
#define _oscl_drv_motorola_mpc834x_gpio_apih_
#include "bitapi.h"

/** */
namespace Oscl {
/** */
namespace Motorola {
/** */
namespace MPC834x {
/** */
namespace GPIO {

/** This interface represents the GPIO functionality.
 */
class Api {
	public:
		/** Make GCC happy */
		virtual ~Api() {}

		/** */
		virtual BitApi&	getGPIO0() noexcept=0;
		/** */
		virtual BitApi&	getGPIO1() noexcept=0;
		/** */
		virtual BitApi&	getGPIO2() noexcept=0;
		/** */
		virtual BitApi&	getGPIO3() noexcept=0;
		/** */
		virtual BitApi&	getGPIO4() noexcept=0;
		/** */
		virtual BitApi&	getGPIO5() noexcept=0;
		/** */
		virtual BitApi&	getGPIO6() noexcept=0;
		/** */
		virtual BitApi&	getGPIO7() noexcept=0;
		/** */
		virtual BitApi&	getGPIO8() noexcept=0;
		/** */
		virtual BitApi&	getGPIO9() noexcept=0;
		/** */
		virtual BitApi&	getGPIO10() noexcept=0;
		/** */
		virtual BitApi&	getGPIO11() noexcept=0;
		/** */
		virtual BitApi&	getGPIO12() noexcept=0;
		/** */
		virtual BitApi&	getGPIO13() noexcept=0;
		/** */
		virtual BitApi&	getGPIO14() noexcept=0;
		/** */
		virtual BitApi&	getGPIO15() noexcept=0;
		/** */
		virtual BitApi&	getGPIO16() noexcept=0;
		/** */
		virtual BitApi&	getGPIO17() noexcept=0;
		/** */
		virtual BitApi&	getGPIO18() noexcept=0;
		/** */
		virtual BitApi&	getGPIO19() noexcept=0;
		/** */
		virtual BitApi&	getGPIO20() noexcept=0;
		/** */
		virtual BitApi&	getGPIO21() noexcept=0;
		/** */
		virtual BitApi&	getGPIO22() noexcept=0;
		/** */
		virtual BitApi&	getGPIO23() noexcept=0;
		/** */
		virtual BitApi&	getGPIO24() noexcept=0;
		/** */
		virtual BitApi&	getGPIO25() noexcept=0;
		/** */
		virtual BitApi&	getGPIO26() noexcept=0;
		/** */
		virtual BitApi&	getGPIO27() noexcept=0;
		/** */
		virtual BitApi&	getGPIO28() noexcept=0;
		/** */
		virtual BitApi&	getGPIO29() noexcept=0;
		/** */
		virtual BitApi&	getGPIO30() noexcept=0;
		/** */
		virtual BitApi&	getGPIO31() noexcept=0;
	};

}
}
}
}

#endif
