/*
   Copyright (C) 2006 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_motorola_mpc834x_gpio_bith_
#define _oscl_drv_motorola_mpc834x_gpio_bith_
#include "oscl/hw/motorola/mpc834x/gpio.h"
#include "bitapi.h"

/** */
namespace Oscl {
/** */
namespace Motorola {
/** */
namespace MPC834x {
/** */
namespace GPIO {

/** This interface represents the operations that can
	be performed on GPIO bits 0 through 7.
 */
template < unsigned bitNumber >
class Bit : public BitApi {
	private:
		/** */
		Oscl::Motorola::MPC834x::GPIO::RegisterMap&	_registers;

	public:
		/** Make GCC happy */
		Bit(Oscl::Motorola::MPC834x::GPIO::RegisterMap& registers) noexcept:
			_registers(registers)
			{}

	public: // Oscl::Bits::InOutApi
		/** */
		void	high() noexcept{
			Oscl::Motorola::MPC834x::GPIO::GPDAT::Reg	value	= _registers.gpdat;
			value	&= ~(Oscl::Motorola::MPC834x::GPIO::GPDAT::Mask << bitNumber);
			value	|= (Oscl::Motorola::MPC834x::GPIO::GPDAT::High << bitNumber);
			_registers.gpdat	= value;
			}
		/** */
		void	low() noexcept{
			Oscl::Motorola::MPC834x::GPIO::GPDAT::Reg	value	= _registers.gpdat;
			value	&= ~(Oscl::Motorola::MPC834x::GPIO::GPDAT::Mask << bitNumber);
			value	|= (Oscl::Motorola::MPC834x::GPIO::GPDAT::Low << bitNumber);
			_registers.gpdat	= value;
			}
		/** */
		bool	state() noexcept{
			return (_registers.gpdat & (Oscl::Motorola::MPC834x::GPIO::GPDAT::Mask<<bitNumber)) == ((Oscl::Motorola::MPC834x::GPIO::GPDAT::Reg)(Oscl::Motorola::MPC834x::GPIO::GPDAT::High << bitNumber));
			}

	public:
		/** This operation configures the GPIO pin as an input.
		 */
		void	configureAsInput() noexcept{
			Oscl::Motorola::MPC834x::GPIO::GPDIR::Reg	value	= _registers.gpdir;
			value	&= ~(Oscl::Motorola::MPC834x::GPIO::GPDIR::Mask << bitNumber);
			value	|= (Oscl::Motorola::MPC834x::GPIO::GPDIR::Input << bitNumber);
			_registers.gpdir	= value;
			}

		/** This operation configures the GPIO pin as an output.
		 */
		void	configureAsOutput() noexcept{
			Oscl::Motorola::MPC834x::GPIO::GPDIR::Reg	value	= _registers.gpdir;
			value	&= ~(Oscl::Motorola::MPC834x::GPIO::GPDIR::Mask << bitNumber);
			value	|= (Oscl::Motorola::MPC834x::GPIO::GPDIR::Output << bitNumber);
			_registers.gpdir	= value;
			}

	public:
		/** This operation configures the GPIO output to be
			actively driven (not open drain.)
		 */
		void	configureActivelyDriven() noexcept{
			Oscl::Motorola::MPC834x::GPIO::GPODR::Reg	value	= _registers.gpodr;
			value	&= ~(Oscl::Motorola::MPC834x::GPIO::GPODR::Mask << bitNumber);
			value	|= (Oscl::Motorola::MPC834x::GPIO::GPODR::Driven << bitNumber);
			_registers.gpodr	= value;
			}

		/** This operation configures the GPIO output as an
			open drain output.
		 */
		void	configureOpenDrain() noexcept{
			Oscl::Motorola::MPC834x::GPIO::GPODR::Reg	value	= _registers.gpodr;
			value	&= ~(Oscl::Motorola::MPC834x::GPIO::GPODR::Mask << bitNumber);
			value	|= (Oscl::Motorola::MPC834x::GPIO::GPODR::OpenDrain << bitNumber);
			_registers.gpodr	= value;
			}

	public:
		/** */
		bool	interruptPending() noexcept{
			return (_registers.gpier & (Oscl::Motorola::MPC834x::GPIO::GPIER::Mask<<bitNumber)) == (Oscl::Motorola::MPC834x::GPIO::GPIER::Pending << bitNumber);
			}
		/** */
		void	clearInterrupt() noexcept{
			_registers.gpier	= (Oscl::Motorola::MPC834x::GPIO::GPIER::Clear << bitNumber);
			}
		/** */
		void	interruptOnAnyEdge() noexcept{
			Oscl::Motorola::MPC834x::GPIO::GPICR::Reg	value	= _registers.gpicr;
			value	&= ~(Oscl::Motorola::MPC834x::GPIO::GPICR::Mask << bitNumber);
			value	|= (Oscl::Motorola::MPC834x::GPIO::GPICR::AnyEdge << bitNumber);
			_registers.gpicr	= value;
			}
		/** */
		void	interruptOnHighToLowEdge() noexcept{
			Oscl::Motorola::MPC834x::GPIO::GPICR::Reg	value	= _registers.gpicr;
			value	&= ~(Oscl::Motorola::MPC834x::GPIO::GPICR::Mask << bitNumber);
			value	|= (Oscl::Motorola::MPC834x::GPIO::GPICR::HighToLowEdge << bitNumber);
			_registers.gpicr	= value;
			}
	};

}
}
}
}

#endif
