/*
   Copyright (C) 2006 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_motorola_mpc834x_gpio_drvh_
#define _oscl_drv_motorola_mpc834x_gpio_drvh_
#include "api.h"
#include "bit.h"

/** */
namespace Oscl {
/** */
namespace Motorola {
/** */
namespace MPC834x {
/** */
namespace GPIO {

/** This interface represents the GPIO functionality.
 */
class Driver : public Api {
	private:
		/** */
		Bit<0>	_gpio0;
		/** */
		Bit<1>	_gpio1;
		/** */
		Bit<2>	_gpio2;
		/** */
		Bit<3>	_gpio3;
		/** */
		Bit<4>	_gpio4;
		/** */
		Bit<5>	_gpio5;
		/** */
		Bit<6>	_gpio6;
		/** */
		Bit<7>	_gpio7;
		/** */
		Bit<8>	_gpio8;
		/** */
		Bit<9>	_gpio9;
		/** */
		Bit<10>	_gpio10;
		/** */
		Bit<11>	_gpio11;
		/** */
		Bit<12>	_gpio12;
		/** */
		Bit<13>	_gpio13;
		/** */
		Bit<14>	_gpio14;
		/** */
		Bit<15>	_gpio15;
		/** */
		Bit<16>	_gpio16;
		/** */
		Bit<17>	_gpio17;
		/** */
		Bit<18>	_gpio18;
		/** */
		Bit<19>	_gpio19;
		/** */
		Bit<20>	_gpio20;
		/** */
		Bit<21>	_gpio21;
		/** */
		Bit<22>	_gpio22;
		/** */
		Bit<23>	_gpio23;
		/** */
		Bit<24>	_gpio24;
		/** */
		Bit<25>	_gpio25;
		/** */
		Bit<26>	_gpio26;
		/** */
		Bit<27>	_gpio27;
		/** */
		Bit<28>	_gpio28;
		/** */
		Bit<29>	_gpio29;
		/** */
		Bit<30>	_gpio30;
		/** */
		Bit<31>	_gpio31;

	public:
		/** */
		Driver(Oscl::Motorola::MPC834x::GPIO::RegisterMap& registers) noexcept;

	public:
		/** */
		BitApi&	getGPIO0() noexcept;
		/** */
		BitApi&	getGPIO1() noexcept;
		/** */
		BitApi&	getGPIO2() noexcept;
		/** */
		BitApi&	getGPIO3() noexcept;
		/** */
		BitApi&	getGPIO4() noexcept;
		/** */
		BitApi&	getGPIO5() noexcept;
		/** */
		BitApi&	getGPIO6() noexcept;
		/** */
		BitApi&	getGPIO7() noexcept;
		/** */
		BitApi&	getGPIO8() noexcept;
		/** */
		BitApi&	getGPIO9() noexcept;
		/** */
		BitApi&	getGPIO10() noexcept;
		/** */
		BitApi&	getGPIO11() noexcept;
		/** */
		BitApi&	getGPIO12() noexcept;
		/** */
		BitApi&	getGPIO13() noexcept;
		/** */
		BitApi&	getGPIO14() noexcept;
		/** */
		BitApi&	getGPIO15() noexcept;
		/** */
		BitApi&	getGPIO16() noexcept;
		/** */
		BitApi&	getGPIO17() noexcept;
		/** */
		BitApi&	getGPIO18() noexcept;
		/** */
		BitApi&	getGPIO19() noexcept;
		/** */
		BitApi&	getGPIO20() noexcept;
		/** */
		BitApi&	getGPIO21() noexcept;
		/** */
		BitApi&	getGPIO22() noexcept;
		/** */
		BitApi&	getGPIO23() noexcept;
		/** */
		BitApi&	getGPIO24() noexcept;
		/** */
		BitApi&	getGPIO25() noexcept;
		/** */
		BitApi&	getGPIO26() noexcept;
		/** */
		BitApi&	getGPIO27() noexcept;
		/** */
		BitApi&	getGPIO28() noexcept;
		/** */
		BitApi&	getGPIO29() noexcept;
		/** */
		BitApi&	getGPIO30() noexcept;
		/** */
		BitApi&	getGPIO31() noexcept;
	};

}
}
}
}

#endif
