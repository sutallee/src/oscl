/*
   Copyright (C) 2006 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "drv.h"

using namespace Oscl::Motorola::MPC834x::GPIO;


Driver::Driver(Oscl::Motorola::MPC834x::GPIO::RegisterMap& registers) noexcept:
		_gpio0(registers),
		_gpio1(registers),
		_gpio2(registers),
		_gpio3(registers),
		_gpio4(registers),
		_gpio5(registers),
		_gpio6(registers),
		_gpio7(registers),
		_gpio8(registers),
		_gpio9(registers),
		_gpio10(registers),
		_gpio11(registers),
		_gpio12(registers),
		_gpio13(registers),
		_gpio14(registers),
		_gpio15(registers),
		_gpio16(registers),
		_gpio17(registers),
		_gpio18(registers),
		_gpio19(registers),
		_gpio20(registers),
		_gpio21(registers),
		_gpio22(registers),
		_gpio23(registers),
		_gpio24(registers),
		_gpio25(registers),
		_gpio26(registers),
		_gpio27(registers),
		_gpio28(registers),
		_gpio29(registers),
		_gpio30(registers),
		_gpio31(registers)
		{
	}

Oscl::Motorola::MPC834x::GPIO::BitApi&	Driver::getGPIO0() noexcept{
	return _gpio0;
	}

Oscl::Motorola::MPC834x::GPIO::BitApi&	Driver::getGPIO1() noexcept{
	return _gpio1;
	}

Oscl::Motorola::MPC834x::GPIO::BitApi&	Driver::getGPIO2() noexcept{
	return _gpio2;
	}

Oscl::Motorola::MPC834x::GPIO::BitApi&	Driver::getGPIO3() noexcept{
	return _gpio3;
	}

Oscl::Motorola::MPC834x::GPIO::BitApi&	Driver::getGPIO4() noexcept{
	return _gpio4;
	}

Oscl::Motorola::MPC834x::GPIO::BitApi&	Driver::getGPIO5() noexcept{
	return _gpio5;
	}

Oscl::Motorola::MPC834x::GPIO::BitApi&	Driver::getGPIO6() noexcept{
	return _gpio6;
	}

Oscl::Motorola::MPC834x::GPIO::BitApi&	Driver::getGPIO7() noexcept{
	return _gpio7;
	}

Oscl::Motorola::MPC834x::GPIO::BitApi&	Driver::getGPIO8() noexcept{
	return _gpio8;
	}

Oscl::Motorola::MPC834x::GPIO::BitApi&	Driver::getGPIO9() noexcept{
	return _gpio9;
	}

Oscl::Motorola::MPC834x::GPIO::BitApi&	Driver::getGPIO10() noexcept{
	return _gpio10;
	}

Oscl::Motorola::MPC834x::GPIO::BitApi&	Driver::getGPIO11() noexcept{
	return _gpio11;
	}

Oscl::Motorola::MPC834x::GPIO::BitApi&	Driver::getGPIO12() noexcept{
	return _gpio12;
	}

Oscl::Motorola::MPC834x::GPIO::BitApi&	Driver::getGPIO13() noexcept{
	return _gpio13;
	}

Oscl::Motorola::MPC834x::GPIO::BitApi&	Driver::getGPIO14() noexcept{
	return _gpio14;
	}

Oscl::Motorola::MPC834x::GPIO::BitApi&	Driver::getGPIO15() noexcept{
	return _gpio15;
	}

Oscl::Motorola::MPC834x::GPIO::BitApi&	Driver::getGPIO16() noexcept{
	return _gpio16;
	}

Oscl::Motorola::MPC834x::GPIO::BitApi&	Driver::getGPIO17() noexcept{
	return _gpio17;
	}

Oscl::Motorola::MPC834x::GPIO::BitApi&	Driver::getGPIO18() noexcept{
	return _gpio18;
	}

Oscl::Motorola::MPC834x::GPIO::BitApi&	Driver::getGPIO19() noexcept{
	return _gpio19;
	}

Oscl::Motorola::MPC834x::GPIO::BitApi&	Driver::getGPIO20() noexcept{
	return _gpio20;
	}

Oscl::Motorola::MPC834x::GPIO::BitApi&	Driver::getGPIO21() noexcept{
	return _gpio21;
	}

Oscl::Motorola::MPC834x::GPIO::BitApi&	Driver::getGPIO22() noexcept{
	return _gpio22;
	}

Oscl::Motorola::MPC834x::GPIO::BitApi&	Driver::getGPIO23() noexcept{
	return _gpio23;
	}

Oscl::Motorola::MPC834x::GPIO::BitApi&	Driver::getGPIO24() noexcept{
	return _gpio24;
	}

Oscl::Motorola::MPC834x::GPIO::BitApi&	Driver::getGPIO25() noexcept{
	return _gpio25;
	}

Oscl::Motorola::MPC834x::GPIO::BitApi&	Driver::getGPIO26() noexcept{
	return _gpio26;
	}

Oscl::Motorola::MPC834x::GPIO::BitApi&	Driver::getGPIO27() noexcept{
	return _gpio27;
	}

Oscl::Motorola::MPC834x::GPIO::BitApi&	Driver::getGPIO28() noexcept{
	return _gpio28;
	}

Oscl::Motorola::MPC834x::GPIO::BitApi&	Driver::getGPIO29() noexcept{
	return _gpio29;
	}

Oscl::Motorola::MPC834x::GPIO::BitApi&	Driver::getGPIO30() noexcept{
	return _gpio30;
	}

Oscl::Motorola::MPC834x::GPIO::BitApi&	Driver::getGPIO31() noexcept{
	return _gpio31;
	}


