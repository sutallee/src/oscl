/*
   Copyright (C) 2006 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_motorola_mpc834x_gpio_bitapih_
#define _oscl_drv_motorola_mpc834x_gpio_bitapih_
#include "oscl/bits/ioapi.h"

/** */
namespace Oscl {
/** */
namespace Motorola {
/** */
namespace MPC834x {
/** */
namespace GPIO {

/** This interface represents the operations that can
	be performed on all GPIO bits.
 */
class BitApi : public Oscl::Bits::InOutApi {
	public:
		/** Make GCC happy */
		virtual ~BitApi() {}

	public: // Oscl::Bits::InOutApi
		/** */
		virtual void	high() noexcept=0;
		/** */
		virtual void	low() noexcept=0;
		/** */
		virtual bool	state() noexcept=0;

	public:
		/** This operation configures the GPIO pin as an input.
		 */
		virtual void	configureAsInput() noexcept=0;
		/** This operation configures the GPIO pin as an output.
		 */
		virtual void	configureAsOutput() noexcept=0;

	public:
		/** This operation configures the GPIO output to be
			actively driven (not open drain.)
		 */
		virtual void	configureActivelyDriven() noexcept=0;
		/** This operation configures the GPIO output as an
			open drain output.
		 */
		virtual void	configureOpenDrain() noexcept=0;

	public:
		/** Returns true if interrupt is pending */
		virtual bool	interruptPending() noexcept=0;
		/** Clears pending interrupt */
		virtual void	clearInterrupt() noexcept=0;
		/** Interrupt on any edge (high-to-low or low-to-high*/
		virtual void	interruptOnAnyEdge() noexcept=0;
		/** Interrupt on high-to-low edge */
		virtual void	interruptOnHighToLowEdge() noexcept=0;
	};

}
}
}
}

#endif
