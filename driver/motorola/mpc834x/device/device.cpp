/*
   Copyright (C) 2006 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "device.h"
#include "oscl/error/fatal.h"

/** */
namespace Oscl {
/** */
namespace Motorola {
/** */
namespace MPC834x {

/** The purpose of this class is to provide an action for the
	PIC when an attempt is made to use an uninitialized PIC
	vector.
 */
class UnusedVector : public Oscl::Motorola::MPC834x::PIC::IrqApi {
	public:
		/** */
		UnusedVector() noexcept;
	public: // Oscl::Motorola::MPC834x::PIC::IrqApi
		/** */
		void	reserve(	Oscl::Interrupt::
							StatusHandlerApi&	handler
							) noexcept;
		/** */
		void	release() noexcept;

	public: // Oscl::Interrupt::StatusHandlerApi
		/** */
        bool    interrupt() noexcept;

	};

}
}
}

using namespace Oscl::Motorola::MPC834x;

static UnusedVector	unusedVector;

Device::Device(	Oscl::Motorola::MPC834x::Map&		registers,
				Oscl::Mt::Mutex::Simple::Api&	mutex
				) noexcept:
		_irqPic(	registers.integratedPIC.ipic.sivcr,
					unusedVector
					),
		_irqPicMap(_irqPic),
		_picDriver(registers.integratedPIC.ipic),
		_pitDriver(	registers.periodicIntervalTimer.registers.ptcnr,
					registers.periodicIntervalTimer.registers.ptldr,
					registers.periodicIntervalTimer.registers.ptpsr,
					registers.periodicIntervalTimer.registers.ptctr,
					registers.periodicIntervalTimer.registers.ptevr,
					(128000000/100),	// FIXME: This should be determined
										// by the system. The value is written
										// to the PITC register is clocked
										// at 128MHz.
										// The value here attempts to achieve
										// a period of at least 10ms
										// (a frequency of 100Hz).
					0,	// Prescaler to divide by one (n+1)
					false	// Use internal clock for PIT
					),
		_gpio1Driver(registers.gpio1.gpio),
		_gpio2Driver(registers.gpio2.gpio),
		_duartChannel1(	registers.duart.registers.uart1.dlab00.rbr,
						registers.duart.registers.uart1.dlab00.thr,
						registers.duart.registers.uart1.dlab00.dll,
						registers.duart.registers.uart1.dlab01.ier,
						registers.duart.registers.uart1.dlab01.dlh,
						registers.duart.registers.uart1.dlab02.iir,
						registers.duart.registers.uart1.dlab02.fcr,
						registers.duart.registers.uart1.lcr,
						registers.duart.registers.uart1.mcr,
						registers.duart.registers.uart1.lsr,
						registers.duart.registers.uart1.msr,
						registers.duart.registers.uart1.spr
						),
		_duartChannel2(	registers.duart.registers.uart2.dlab00.rbr,
						registers.duart.registers.uart2.dlab00.thr,
						registers.duart.registers.uart2.dlab00.dll,
						registers.duart.registers.uart2.dlab01.ier,
						registers.duart.registers.uart2.dlab01.dlh,
						registers.duart.registers.uart2.dlab02.iir,
						registers.duart.registers.uart2.dlab02.fcr,
						registers.duart.registers.uart2.lcr,
						registers.duart.registers.uart2.mcr,
						registers.duart.registers.uart2.lsr,
						registers.duart.registers.uart2.msr,
						registers.duart.registers.uart2.spr
						),
			_duartChannel1Driver(_duartChannel1),
			_duartChannel2Driver(_duartChannel2)
		{
	}

Oscl::Motorola::MPC834x::PIC::DrvApi&	Device::getPicDriverApi() noexcept{
	return _picDriver;
	}

Oscl::Motorola::MPC834x::PIT::Api&		Device::getPitApi() noexcept{
	return _pitDriver;
	}

Oscl::Motorola::MPC834x::PIC::Api&		Device::getPicApi() noexcept{
	return _irqPicMap;
	}

Oscl::Motorola::MPC834x::GPIO::Api&		Device::getGpio1Api() noexcept{
	return _gpio1Driver;
	}

Oscl::Motorola::MPC834x::GPIO::Api&		Device::getGpio2Api() noexcept{
	return _gpio2Driver;
	}

Oscl::National::UART::PC16550::RegisterApi&		Device::getRawDuartChannel1Api() noexcept{
	return _duartChannel1;
	}

Oscl::National::UART::PC16550::RegisterApi&		Device::getRawDuartChannel2Api() noexcept{
	return _duartChannel2;
	}

Oscl::National::UART::PC16550::Api&		Device::getDuartChannel1Api() noexcept{
	return _duartChannel1Driver;
	}

Oscl::National::UART::PC16550::Api&		Device::getDuartChannel2Api() noexcept{
	return _duartChannel2Driver;
	}

UnusedVector::UnusedVector() noexcept
		{
	}

void	UnusedVector::reserve(	Oscl::Interrupt::StatusHandlerApi&	handler
								) noexcept{
	Oscl::ErrorFatal::logAndExit("Attempt to reserve and unused PIC vector.");
	}

void	UnusedVector::release() noexcept{
	Oscl::ErrorFatal::logAndExit("Attempt to release and unused PIC vector.");
	}

bool    UnusedVector::interrupt() noexcept{
	Oscl::ErrorFatal::logAndExit("Interrupt caught by unused PIC vector.");
	return false;
	}

