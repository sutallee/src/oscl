/*
   Copyright (C) 2006 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_motorola_mpc834x_device_deviceh_
#define _oscl_drv_motorola_mpc834x_device_deviceh_

#include "api.h"
#include "oscl/driver/motorola/mpc834x/pit/driver.h"
#include "oscl/driver/motorola/mpc834x/pic/drv.h"
#include "oscl/extalloc/driver.h"
#include "oscl/extalloc/mt/mutex.h"
#include "oscl/mt/mutex/simple.h"
#include "oscl/hw/motorola/mpc834x/imm.h"
#include "oscl/driver/motorola/mpc834x/pic/pic.h"
#include "oscl/driver/motorola/mpc834x/pic/map.h"
#include "oscl/driver/motorola/mpc834x/gpio/drv.h"
#include "oscl/driver/national/uart/pc16550/eightbit.h"
#include "oscl/driver/national/uart/pc16550/driver.h"
#include "oscl/driver/national/uart/pc16550/api.h"

/** */
namespace Oscl {
/** */
namespace Motorola {
/** */
namespace MPC834x {

/** This class implements the low level drivers for an Motorola
	MPC834x processor and attending peripherals. The drivers
	contained in this layer make no use of kernel services.
 */
class Device : public Api {
	private:
		/** */
		Oscl::Motorola::MPC834x::PIC::PIC			_irqPic;
		/** */
		Oscl::Motorola::MPC834x::PIC::Map			_irqPicMap;
		/** */
		Oscl::Motorola::MPC834x::PIC::Driver		_picDriver;
		/** */
		Oscl::Motorola::MPC834x::PIT::Driver		_pitDriver;
		/** */
		Oscl::Motorola::MPC834x::GPIO::Driver		_gpio1Driver;
		/** */
		Oscl::Motorola::MPC834x::GPIO::Driver		_gpio2Driver;
		/** */
		Oscl::National::UART::PC16550::EightBit		_duartChannel1;
		/** */
		Oscl::National::UART::PC16550::EightBit		_duartChannel2;
		/** */
		Oscl::National::UART::PC16550::Driver		_duartChannel1Driver;
		/** */
		Oscl::National::UART::PC16550::Driver		_duartChannel2Driver;

	public:
		/** */
		Device(	Oscl::Motorola::MPC834x::Map&	registers,
				Oscl::Mt::Mutex::Simple::Api&	mutex
				) noexcept;

	public:	// MPC834x::Api
		/** */
		Oscl::Motorola::MPC834x::PIC::DrvApi&	getPicDriverApi() noexcept;

		/** */
		Oscl::Motorola::MPC834x::PIT::Api&		getPitApi() noexcept;

		/** */
		Oscl::Motorola::MPC834x::PIC::Api&		getPicApi() noexcept;

		/** */
		Oscl::Motorola::MPC834x::GPIO::Api&		getGpio1Api() noexcept;

		/** */
		Oscl::Motorola::MPC834x::GPIO::Api&		getGpio2Api() noexcept;

		/** */
		Oscl::National::UART::PC16550::RegisterApi&		getRawDuartChannel1Api() noexcept;

		/** */
		Oscl::National::UART::PC16550::RegisterApi&		getRawDuartChannel2Api() noexcept;

		/** */
		Oscl::National::UART::PC16550::Api&		getDuartChannel1Api() noexcept;

		/** */
		Oscl::National::UART::PC16550::Api&		getDuartChannel2Api() noexcept;
	};

}
}
}


#endif
