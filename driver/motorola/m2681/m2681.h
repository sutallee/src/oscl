/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_driver_motorola_m2681h_
#define _oscl_driver_motorola_m2681h_
#include <stdint.h>
#include "oscl/hw/motorola/m2681.h"
#include "oscl/mt/runnable.h"
#include "oscl/mt/sema/api.h"

/** */
namespace Oscl {

/** */
class SerialRxObserver;
/** */
class SerialTxObserver;

/** */
class M2681Driver : public Mt::Runnable {
	private:
		/** */
		M2681&						_chip;
		/** */
		Mt::Sema::Api&				_input;
		/** */
		unsigned					_overflowCount;
		/** */
		unsigned					_abortCount;
		/** */
		SerialTxObserver*			_channelATxObserver;
		/** */
		SerialRxObserver*			_channelARxObserver;
		/** */
		SerialTxObserver*			_channelBTxObserver;
		/** */
		SerialRxObserver*			_channelBRxObserver;
		/** */
		Fields						_ierShadow;	// Protect from interrupts
		/** */
		Fields						_txAIntEnabled;
		/** */
		Fields						_txBIntEnabled;
		/** */
		Fields						_rxAIntEnabled;
		/** */
		Fields						_rxBIntEnabled;
		/** */
		unsigned char				_txBuff;
	public: // Constructor
		/** */
		M2681Driver(	M2681&						chip,
						Mt::Sema::Api&				input
						);
	public: // main thread loop
		/** */
		void		run(void) noexcept;
	public: // interrupt service routine
		/** */
		void		suInterrupt();
	private: // interrupt event handlers
		/** */
		void	channelATransmitterReady();
		/** */
		void	channelAReceiverReady();
		/** */
		void	channelBTransmitterReady();
		/** */
		void	channelBReceiverReady();
	private: // utility routines
		/** */
		void		initialize();
		/** */
		void		enableChannelTxAInterrupts();
		/** */
		void		enableChannelTxBInterrupts();
	private: // performance measurement utilities
		/** */
		void		logOverflow();
		/** */
		void		logAbort();
		/** */
		void		logInvalidFrame();
	public:	// SerialRxObserver interface
		/** */
		SerialRxObserver*	attachA(SerialRxObserver* sis);
		/** */
		SerialRxObserver*	attachB(SerialRxObserver* sis);
	public: // SerialTransmitStream interface
		/** */
		SerialTxObserver*	attachA(SerialTxObserver* observer);
		/** */
		SerialTxObserver*	attachB(SerialTxObserver* observer);
		/** */
		unsigned			writeA(const uint8_t* data,unsigned len);
		/** */
		unsigned			writeB(const uint8_t* data,unsigned len);
	};

}

#endif
