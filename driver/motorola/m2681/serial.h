/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_driver_motorola_m2681_serialh_
#define _oscl_driver_motorola_m2681_serialh_
#include "oscl/preitc/serial/transmitter.h"
#include "oscl/preitc/serial/receiver.h"

/** */
namespace Oscl {

/** */
class M2681Driver;

/** */
class M2681SerialTransmitterA : public SerialTransmitter {
	private:
		/** */
		M2681Driver&	_m2681;

	public:
		/** */
		M2681SerialTransmitterA(M2681Driver& m2681);

		/** This routine attaches the SerialTxObserver observer to the
			transmit stream, replacing any previous observer. The observer
			is used to notify the client of changes in the serial transmitter
			state. The previous observer, if any, is returned.
		 */
		SerialTxObserver*	attach(SerialTxObserver* ready);

		/** This function is called by the client in an attempt to
			transmit the specified number of octets from the octet
			array to the transmitter. The function writes as many
			octets as possible to the transmitter and returns the
			actual number of octets written. This function does
			not block.
		 */
		unsigned		write(const uint8_t* data,unsigned len);
	};

/** */
class M2681SerialTransmitterB : public SerialTransmitter {
	private:
		/** */
		M2681Driver&	_m2681;

	public:
		/** */
		M2681SerialTransmitterB(M2681Driver& m2681);

		/** This routine attaches the SerialTxObserver observer to the
			transmit stream, replacing any previous observer. The observer
			is used to notify the client of changes in the serial transmitter
			state. The previous observer, if any, is returned.
		 */
		SerialTxObserver*	attach(SerialTxObserver* ready);

		/** This function is called by the client in an attempt to
			transmit the specified number of octets from the octet
			array to the transmitter. The function writes as many
			octets as possible to the transmitter and returns the
			actual number of octets written. This function does
			not block.
		 */
		unsigned		write(const uint8_t* data,unsigned len);
	};

/** */
class M2681SerialReceiverA : public SerialReceiver {
	private:
		/** */
		M2681Driver&	_m2681;

	public:
		/** */
		M2681SerialReceiverA(M2681Driver& m2681);

		/** This routine attaches the SerialRxObserver observer to the
			receive stream, replacing any previous observer on that
			receiver. The observer is used to notify the client
			of changes in the serial receiver state. The previous
			observer, if any, is returned.
		 */
		SerialRxObserver*	attach(SerialRxObserver* observer);
	};

/** */
class M2681SerialReceiverB : public SerialReceiver {
	private:
		/** */
		M2681Driver&	_m2681;

	public:
		/** */
		M2681SerialReceiverB(M2681Driver& m2681);

		/** This routine attaches the SerialRxObserver observer to the
			receive stream, replacing any previous observer on that
			receiver. The observer is used to notify the client
			of changes in the serial receiver state. The previous
			observer, if any, is returned.
		 */
		SerialRxObserver*	attach(SerialRxObserver* observer);
	};

}

#endif
