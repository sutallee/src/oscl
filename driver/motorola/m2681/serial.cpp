/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "serial.h"
#include "m2681.h"

using namespace Oscl;

////////////////// M2681SerialTransmitterA ////////////////////

M2681SerialTransmitterA::M2681SerialTransmitterA(M2681Driver& m2681):_m2681(m2681){
	}

SerialTxObserver*	M2681SerialTransmitterA::attach(SerialTxObserver* ready){
	return _m2681.attachA(ready);
	}

unsigned		M2681SerialTransmitterA::write(const uint8_t* data,unsigned len){
	return _m2681.writeA(data,len);
	}

////////////////// M2681SerialTransmitterB ////////////////////

M2681SerialTransmitterB::M2681SerialTransmitterB(M2681Driver& m2681):_m2681(m2681){
	}

SerialTxObserver*	M2681SerialTransmitterB::attach(SerialTxObserver* ready){
	return _m2681.attachB(ready);
	}

unsigned		M2681SerialTransmitterB::write(const uint8_t* data,unsigned len){
	return _m2681.writeB(data,len);
	}

////////////////// M2681SerialReceiverA ////////////////////

M2681SerialReceiverA::M2681SerialReceiverA(M2681Driver& m2681):_m2681(m2681){
	}

SerialRxObserver*	M2681SerialReceiverA::attach(SerialRxObserver* observer){
	return _m2681.attachA(observer);
	}

////////////////// M2681SerialReceiverB ////////////////////

M2681SerialReceiverB::M2681SerialReceiverB(M2681Driver& m2681):_m2681(m2681){
	}

SerialRxObserver*	M2681SerialReceiverB::attach(SerialRxObserver* observer){
	return _m2681.attachB(observer);
	}

