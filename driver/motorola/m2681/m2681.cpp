/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "m2681.h"
#include "oscl/mt/mutex/iss.h"
#include "oscl/preitc/serial/rxobserver.h"
#include "oscl/preitc/serial/txobserver.h"

using namespace Oscl;

M2681Driver::M2681Driver(	M2681&						chip,
							Mt::Sema::Api&				input
							):
							_chip(chip),
							_input(input),
							_overflowCount(0),
							_abortCount(0),
							_channelATxObserver(0),
							_channelARxObserver(0),
							_channelBTxObserver(0),
							_channelBRxObserver(0)
							{
}
void		M2681Driver::run(void) noexcept{
	initialize();
	while(true){
		_input.wait();
		if((_chip.ro.isr & m2681IsrRxRdyAMask) == m2681IsrRxRdyAReceiverReady){
			channelAReceiverReady();
			}
		if((_chip.ro.isr & m2681IsrRxRdyBMask) == m2681IsrRxRdyBReceiverReady){
			channelBReceiverReady();
			}
		if((_chip.ro.isr & m2681IsrTxRdyAMask) == m2681IsrTxRdyATransmitterReady){
			channelATransmitterReady();
			}
		if((_chip.ro.isr & m2681IsrTxRdyBMask) == m2681IsrTxRdyBTransmitterReady){
			channelBTransmitterReady();
			}
			{
			Oscl::Mt::IntScopeSync	iss;
			_ierShadow		&= ~(m2681IerTxRdyAMask | m2681IerRxRdyAMask | m2681IerTxRdyBMask | m2681IerRxRdyBMask);
			_ierShadow		|= (_txAIntEnabled | _txBIntEnabled | m2681IerRxRdyAEnable | m2681IerRxRdyBEnable);
			_chip.wo.ier	= _ierShadow;
			}
		}
	}

void		M2681Driver::suInterrupt(){
	_ierShadow	&=	~(		m2681IerCosMask
						|	m2681IerDbBMask
						|	m2681IerRxRdyBMask
						|	m2681IerTxRdyBMask
						|	m2681IerDbAMask
						|	m2681IerRxRdyAMask
						|	m2681IerTxRdyAMask
						);
	_ierShadow	|= 	(		m2681IerCosDisable
						|	m2681IerDbBDisable
						|	m2681IerRxRdyBDisable
						|	m2681IerTxRdyBDisable
						|	m2681IerDbADisable
						|	m2681IerRxRdyADisable
						|	m2681IerTxRdyADisable
						);
	_chip.wo.ier	=	_ierShadow;
	_input.suSignal();
	}

void		M2681Driver::initialize(){
	_txAIntEnabled	= m2681IerTxRdyBDisable;
	_txBIntEnabled	= m2681IerTxRdyBDisable;
	_rxAIntEnabled	= m2681IerRxRdyAEnable;
	_rxBIntEnabled	= m2681IerRxRdyBEnable;

	_ierShadow		= 0;
	_ierShadow		= (		m2681IerCosDisable
						|	m2681IerDbBDisable
						|	m2681IerDbADisable
						);
	_ierShadow		&= ~(m2681IerTxRdyAMask | m2681IerRxRdyAMask | m2681IerTxRdyBMask | m2681IerRxRdyBMask);
	_ierShadow		|= (_txAIntEnabled | _txBIntEnabled | _rxAIntEnabled | _rxBIntEnabled);
	_chip.wo.cra	= m2681CrTcEnableTransmitter | m2681CrRcEnableReceiver;
	_chip.wo.crb	= m2681CrTcEnableTransmitter | m2681CrRcEnableReceiver;
	_chip.wo.ier	= _ierShadow;
	}

void		M2681Driver::logOverflow(){
	}

void		M2681Driver::logAbort(){
	}

void		M2681Driver::logInvalidFrame(){
	}

void	M2681Driver::channelATransmitterReady(){
	_txAIntEnabled	= m2681IerTxRdyADisable;
	if(_channelATxObserver)
		_channelATxObserver->transmitterReady();
	}

void	M2681Driver::channelAReceiverReady(){
	if(_channelARxObserver){
		while((_chip.ro.isr & m2681IsrRxRdyAMask) == m2681IsrRxRdyAReceiverReady){
			_channelARxObserver->forward(_chip.ro.rba);
			}
		}
	else{
		while((_chip.ro.isr & m2681IsrRxRdyAMask) == m2681IsrRxRdyAReceiverReady){
			_txBuff		= _chip.ro.rba;	// just read it
			}
		}
	}

void	M2681Driver::channelBTransmitterReady(){
	_txBIntEnabled	= m2681IerTxRdyBDisable;
	if(_channelBTxObserver)
		_channelBTxObserver->transmitterReady();
	}

void	M2681Driver::channelBReceiverReady(){
	if(_channelBRxObserver){
		while((_chip.ro.isr & m2681IsrRxRdyBMask) == m2681IsrRxRdyBReceiverReady){
			_channelBRxObserver->forward(_chip.ro.rbb);
			}
		}
	else{
		while((_chip.ro.isr & m2681IsrRxRdyBMask) == m2681IsrRxRdyBReceiverReady){
			_txBuff		= _chip.ro.rbb;	// just read it
			}
		}
	}

SerialRxObserver*	M2681Driver::attachA(SerialRxObserver* sis){
	SerialRxObserver*	old		= _channelARxObserver;
	_channelARxObserver	= sis;
	return old;
	}

SerialRxObserver*	M2681Driver::attachB(SerialRxObserver* sis){
	SerialRxObserver*	old		= _channelBRxObserver;
	_channelBRxObserver	= sis;
	return old;
	}

SerialTxObserver*	M2681Driver::attachA(SerialTxObserver* observer){
	SerialTxObserver*	old	= _channelATxObserver;
	_channelATxObserver	= observer;
	return old;
	}

unsigned		M2681Driver::writeA(const uint8_t* data,unsigned len){
	unsigned		transmitted;
	for(	transmitted=0;
			((_chip.ro.isr & m2681IsrTxRdyAMask) == m2681IsrTxRdyATransmitterReady) && (transmitted < len);
			transmitted++
			){
		_chip.wo.tba	= data[transmitted];
		}
	enableChannelTxAInterrupts();
	return transmitted;
	}

void	M2681Driver::enableChannelTxAInterrupts(){
	Oscl::Mt::IntScopeSync	iss;
	_txAIntEnabled	= m2681IerTxRdyAEnable;
	_ierShadow		&= ~m2681IerTxRdyAMask;
	_ierShadow		|= m2681IerTxRdyAEnable;
	_chip.wo.ier	= _ierShadow;
	}

SerialTxObserver*	M2681Driver::attachB(SerialTxObserver* observer){
	SerialTxObserver*	old	= _channelBTxObserver;
	_channelBTxObserver	= observer;
	return old;
	}

unsigned		M2681Driver::writeB(const uint8_t* data,unsigned len){
	unsigned		transmitted;
	for(	transmitted=0;
			((_chip.ro.isr & m2681IsrTxRdyBMask) == m2681IsrTxRdyBTransmitterReady) && (transmitted < len);
			transmitted++
			){
		_chip.wo.tbb	= data[transmitted];
		}
	enableChannelTxBInterrupts();
	return transmitted;
	}

void	M2681Driver::enableChannelTxBInterrupts(){
	Oscl::Mt::IntScopeSync	iss;
	_txBIntEnabled	= m2681IerTxRdyBEnable;
	_ierShadow		&= ~m2681IerTxRdyBMask;
	_ierShadow		|= m2681IerTxRdyBEnable;
	_chip.wo.ier	= _ierShadow;
	}

