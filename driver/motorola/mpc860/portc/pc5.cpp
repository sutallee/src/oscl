/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "pc5.h"
#include "oscl/error/fatal.h"

using namespace Oscl;
using namespace Oscl::Mot860::PortC;

PC5::PC5(	Bits::FieldApi<Mot8xx::Port::PCDAT::Reg>&	pcdat,
			Bits::FieldApi<Mot8xx::Port::PCSO::Reg>&	pcdir,
			Bits::FieldApi<Mot8xx::Port::PCPAR::Reg>&	pcpar,
			Bits::FieldApi<Mot8xx::Port::PCSO::Reg>&	pcso,
			Bits::FieldApi<Mot8xx::Port::PCDAT::Reg>&	reservation
			) noexcept:
		_pcdat(pcdat),
		_pcdir(pcdir),
		_pcpar(pcpar),
		_pcso(pcso),
		_reservation(reservation)
		{
	}

Bits::InputApi&		PC5::reserveInput() noexcept{
	reserve();
	_pcpar.low();
	_pcdir.low();
	_pcso.low();
	return _pcdat;
	}

Bits::OutputApi&	PC5::reserveOutput() noexcept{
	reserve();
	_pcpar.low();
	_pcdir.high();
	_pcso.low();
	return _pcdat;
	}

Bits::InOutApi&		PC5::reserveInOut() noexcept{
	reserve();
	_pcpar.low();
	_pcdir.high();
	_pcso.low();
	return _pcdat;
	}

Bits::InputApi&	PC5::reserveCTS4() noexcept{
	reserve();
	_pcpar.low();
	_pcdir.low();
	_pcso.high();
	return _pcdat;
	}

Bits::InputApi&	PC5::reserveL1TSYNCA() noexcept{
	reserve();
	_pcpar.high();
	_pcdir.low();
	_pcso.low();
	return _pcdat;
	}

Bits::InputApi&	PC5::reserveSDACK1() noexcept{
	reserve();
	_pcpar.high();
	_pcdir.high();
	_pcso.low();
	return _pcdat;
	}

void	PC5::reserve() noexcept{
	if(_reservation.testAndSet(_mask)) return;
	ErrorFatal::logAndExit("PC5 already reserved.");
	}

