/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_mot_mpc860_portc_porth_
#define _oscl_drv_mot_mpc860_portc_porth_
#include "oscl/bits/port.h"
#include "oscl/bits/mt/mutexedbp.h"
#include "oscl/bits/mt/mutexedfield.h"
#include "oscl/hw/motorola/mpc8xx/portreg.h"
#include "portapi.h"
#include "pc15.h"
#include "pc14.h"
#include "pc13.h"
#include "pc12.h"
#include "pc11.h"
#include "pc10.h"
#include "pc9.h"
#include "pc8.h"
#include "pc7.h"
#include "pc6.h"
#include "pc5.h"
#include "pc4.h"

/** */
namespace Oscl{
/** */
namespace Mot860{
/** */
namespace PortC {

/** */
class Port : public PortApi {
	private:
		/** */
		Oscl::Bits::Port<Mot8xx::Port::PCDAT::Reg>			_pcdat;
		/** */
		Oscl::Bits::MutexedPort<Mot8xx::Port::PCDAT::Reg>	_mutexedPCDAT;
		/** */
		Oscl::Bits::Port<Mot8xx::Port::PCDIR::Reg>			_pcdir;
		/** */
		Oscl::Bits::MutexedPort<Mot8xx::Port::PCDAT::Reg>	_mutexedPCDIR;
		/** */
		Oscl::Bits::Port<Mot8xx::Port::PCPAR::Reg>			_pcpar;
		/** */
		Oscl::Bits::MutexedPort<Mot8xx::Port::PCPAR::Reg>	_mutexedPCPAR;
		/** */
		Oscl::Bits::Port<Mot8xx::Port::PCSO::Reg>			_pcso;
		/** */
		Oscl::Bits::MutexedPort<Mot8xx::Port::PCSO::Reg>	_mutexedPCSO;
		/** */
		Oscl::Bits::MutexedField<Mot8xx::Port::PCDAT::Reg>	_reservations;
		/** */
		PortC::PC15											_pc15;
		/** */
		PortC::PC14											_pc14;
		/** */
		PortC::PC13											_pc13;
		/** */
		PortC::PC12											_pc12;
		/** */
		PortC::PC11											_pc11;
		/** */
		PortC::PC10											_pc10;
		/** */
		PortC::PC9											_pc9;
		/** */
		PortC::PC8											_pc8;
		/** */
		PortC::PC7											_pc7;
		/** */
		PortC::PC6											_pc6;
		/** */
		PortC::PC5											_pc5;
		/** */
		PortC::PC4											_pc4;

	public:
		/** */
		Port(	Oscl::Mt::Mutex::Simple::Api&		mutex,
				volatile Mot8xx::Port::PCDAT::Reg&	pcdat,
				Mot8xx::Port::PCDIR::Reg&			pcdir,
				Mot8xx::Port::PCPAR::Reg&			pcpar,
				Mot8xx::Port::PCSO::Reg&			pcso
				) noexcept;
	public:	// PortApi
		/** */
		PC15Api&	getPC15() noexcept;
		/** */
		PC14Api&	getPC14() noexcept;
		/** */
		PC13Api&	getPC13() noexcept;
		/** */
		PC12Api&	getPC12() noexcept;
		/** */
		PC11Api&	getPC11() noexcept;
		/** */
		PC10Api&	getPC10() noexcept;
		/** */
		PC9Api&		getPC9() noexcept;
		/** */
		PC8Api&		getPC8() noexcept;
		/** */
		PC7Api&		getPC7() noexcept;
		/** */
		PC6Api&		getPC6() noexcept;
		/** */
		PC5Api&		getPC5() noexcept;
		/** */
		PC4Api&		getPC4() noexcept;
	};

}
}
}


#endif
