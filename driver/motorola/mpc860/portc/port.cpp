/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "port.h"

using namespace Oscl::Mot860::PortC;

Port::Port(	Oscl::Mt::Mutex::Simple::Api&		mutex,
			volatile Mot8xx::Port::PCDAT::Reg&	pcdat,
			Mot8xx::Port::PCDIR::Reg&			pcdir,
			Mot8xx::Port::PCPAR::Reg&			pcpar,
			Mot8xx::Port::PCSO::Reg&			pcso
			) noexcept:
		_pcdat(pcdat,0),
		_mutexedPCDAT(_pcdat,mutex),
		_pcdir(pcdir,0),
		_mutexedPCDIR(_pcdir,mutex),
		_pcpar(pcpar,0),
		_mutexedPCPAR(_pcpar,mutex),
		_pcso(pcso,0),
		_mutexedPCSO(_pcso,mutex),
		_reservations(mutex,0),
		_pc15(_mutexedPCDAT,_mutexedPCDIR,_mutexedPCPAR,_mutexedPCSO,_reservations),
		_pc14(_mutexedPCDAT,_mutexedPCDIR,_mutexedPCPAR,_mutexedPCSO,_reservations),
		_pc13(_mutexedPCDAT,_mutexedPCDIR,_mutexedPCPAR,_mutexedPCSO,_reservations),
		_pc12(_mutexedPCDAT,_mutexedPCDIR,_mutexedPCPAR,_mutexedPCSO,_reservations),
		_pc11(_mutexedPCDAT,_mutexedPCDIR,_mutexedPCPAR,_mutexedPCSO,_reservations),
		_pc10(_mutexedPCDAT,_mutexedPCDIR,_mutexedPCPAR,_mutexedPCSO,_reservations),
		_pc9(_mutexedPCDAT,_mutexedPCDIR,_mutexedPCPAR,_mutexedPCSO,_reservations),
		_pc8(_mutexedPCDAT,_mutexedPCDIR,_mutexedPCPAR,_mutexedPCSO,_reservations),
		_pc7(_mutexedPCDAT,_mutexedPCDIR,_mutexedPCPAR,_mutexedPCSO,_reservations),
		_pc6(_mutexedPCDAT,_mutexedPCDIR,_mutexedPCPAR,_mutexedPCSO,_reservations),
		_pc5(_mutexedPCDAT,_mutexedPCDIR,_mutexedPCPAR,_mutexedPCSO,_reservations),
		_pc4(_mutexedPCDAT,_mutexedPCDIR,_mutexedPCPAR,_mutexedPCSO,_reservations)
		{
	}

PC15Api&	Port::getPC15() noexcept{
	return _pc15;
	}

PC14Api&	Port::getPC14() noexcept{
	return _pc14;
	}

PC13Api&	Port::getPC13() noexcept{
	return _pc13;
	}

PC12Api&	Port::getPC12() noexcept{
	return _pc12;
	}

PC11Api&	Port::getPC11() noexcept{
	return _pc11;
	}

PC10Api&	Port::getPC10() noexcept{
	return _pc10;
	}

PC9Api&	Port::getPC9() noexcept{
	return _pc9;
	}

PC8Api&	Port::getPC8() noexcept{
	return _pc8;
	}

PC7Api&	Port::getPC7() noexcept{
	return _pc7;
	}

PC6Api&	Port::getPC6() noexcept{
	return _pc6;
	}

PC5Api&	Port::getPC5() noexcept{
	return _pc5;
	}

PC4Api&	Port::getPC4() noexcept{
	return _pc4;
	}

