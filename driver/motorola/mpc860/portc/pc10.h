/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_mot_mpc860_portc_pc10h_
#define _oscl_drv_mot_mpc860_portc_pc10h_
#include "pc10api.h"
#include "oscl/bits/port.h"
#include "oscl/bits/iobit.h"
#include "oscl/hw/motorola/mpc8xx/portreg.h"

/** */
namespace Oscl{
/** */
namespace Mot860{
/** */
namespace PortC {

/** */
class PC10 : public PC10Api {
	private:
		/** */
		enum{_bitNum=((sizeof(Mot8xx::Port::PCDAT::Reg)*8)-1-10)};
		/** */
		enum{_mask=(1<<_bitNum)};
		/** */
		Bits::IoBit<Mot8xx::Port::PCDAT::Reg,_bitNum,true>	_pcdat;
		/** */
		Bits::IoBit<Mot8xx::Port::PCDIR::Reg,_bitNum,true>	_pcdir;
		/** */
		Bits::IoBit<Mot8xx::Port::PCPAR::Reg,_bitNum,true>	_pcpar;
		/** */
		Bits::IoBit<Mot8xx::Port::PCSO::Reg,_bitNum,true>	_pcso;
		/** */
		Bits::FieldApi<Mot8xx::Port::PCDAT::Reg>&			_reservation;
	public:
		/** */
		PC10(	Bits::FieldApi<Mot8xx::Port::PCDAT::Reg>&	pcdat,
				Bits::FieldApi<Mot8xx::Port::PCDIR::Reg>&	pcdir,
				Bits::FieldApi<Mot8xx::Port::PCPAR::Reg>&	pcpar,
				Bits::FieldApi<Mot8xx::Port::PCSO::Reg>&	pcso,
				Bits::FieldApi<Mot8xx::Port::PCDAT::Reg>&	reservation
				) noexcept;

	public: // Mpc8xx::IoReserve::IOApi
		/** */
		Bits::InputApi&		reserveInput() noexcept;
		/** */
		Bits::OutputApi&	reserveOutput() noexcept;
		/** */
		Bits::InOutApi&		reserveInOut() noexcept;

	public:	// SCC1::CDApi
		/** */
		Bits::InputApi&	reserveCD1() noexcept;

	public:	// TGATE::TGATE1Api
		/** */
		Bits::InputApi&	reserveTGATE1() noexcept;

	private:
		/** */
		void	reserve() noexcept;
	};

}
}
}


#endif
