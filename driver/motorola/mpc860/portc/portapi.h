/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_mot_mpc860_portc_portapih_
#define _oscl_drv_mot_mpc860_portc_portapih_
#include "pc15api.h"
#include "pc14api.h"
#include "pc13api.h"
#include "pc12api.h"
#include "pc11api.h"
#include "pc10api.h"
#include "pc9api.h"
#include "pc8api.h"
#include "pc7api.h"
#include "pc6api.h"
#include "pc5api.h"
#include "pc4api.h"

/** */
namespace Oscl{
/** */
namespace Mot860{
/** */
namespace PortC {

/** */
class PortApi {
	public:
		/** Shut-up GCC. */
		virtual ~PortApi() {}
		/** */
		virtual PC15Api&	getPC15() noexcept=0;
		/** */
		virtual PC14Api&	getPC14() noexcept=0;
		/** */
		virtual PC13Api&	getPC13() noexcept=0;
		/** */
		virtual PC12Api&	getPC12() noexcept=0;
		/** */
		virtual PC11Api&	getPC11() noexcept=0;
		/** */
		virtual PC10Api&	getPC10() noexcept=0;
		/** */
		virtual PC9Api&		getPC9() noexcept=0;
		/** */
		virtual PC8Api&		getPC8() noexcept=0;
		/** */
		virtual PC7Api&		getPC7() noexcept=0;
		/** */
		virtual PC6Api&		getPC6() noexcept=0;
		/** */
		virtual PC5Api&		getPC5() noexcept=0;
		/** */
		virtual PC4Api&		getPC4() noexcept=0;
	};

}
}
}

#endif
