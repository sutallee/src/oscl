/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "pc11.h"
#include "oscl/error/fatal.h"

using namespace Oscl;
using namespace Oscl::Mot860::PortC;

PC11::PC11(	Bits::FieldApi<Mot8xx::Port::PCDAT::Reg>&	pcdat,
			Bits::FieldApi<Mot8xx::Port::PCSO::Reg>&	pcdir,
			Bits::FieldApi<Mot8xx::Port::PCPAR::Reg>&	pcpar,
			Bits::FieldApi<Mot8xx::Port::PCSO::Reg>&	pcso,
			Bits::FieldApi<Mot8xx::Port::PCDAT::Reg>&	reservation
			) noexcept:
		_pcdat(pcdat),
		_pcdir(pcdir),
		_pcpar(pcpar),
		_pcso(pcso),
		_reservation(reservation)
		{
	}

Bits::InputApi&		PC11::reserveInput() noexcept{
	reserve();
	_pcpar.low();
	_pcdir.low();
	_pcso.low();
	return _pcdat;
	}

Bits::OutputApi&	PC11::reserveOutput() noexcept{
	reserve();
	_pcpar.low();
	_pcdir.high();
	_pcso.low();
	return _pcdat;
	}

Bits::InOutApi&		PC11::reserveInOut() noexcept{
	reserve();
	_pcpar.low();
	_pcdir.high();
	_pcso.low();
	return _pcdat;
	}

Bits::InputApi&	PC11::reserveCTS1() noexcept{
	reserve();
	_pcpar.low();
	_pcdir.low();
	_pcso.high();
	return _pcdat;
	}

void	PC11::reserve() noexcept{
	if(_reservation.testAndSet(_mask)) return;
	ErrorFatal::logAndExit("PC11 already reserved.");
	}

