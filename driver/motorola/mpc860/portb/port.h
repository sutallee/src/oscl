/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_mot_mpc860_portb_porth_
#define _oscl_drv_mot_mpc860_portb_porth_
#include "oscl/bits/port.h"
#include "oscl/bits/mt/mutexedbp.h"
#include "oscl/bits/mt/mutexedfield.h"
#include "oscl/hw/motorola/mpc8xx/portreg.h"
#include "portapi.h"
#include "pb31.h"
#include "pb30.h"
#include "pb29.h"
#include "pb28.h"
#include "pb27.h"
#include "pb26.h"
#include "pb25.h"
#include "pb24.h"
#include "pb23.h"
#include "pb22.h"
#include "pb21.h"
#include "pb20.h"
#include "pb19.h"
#include "pb18.h"
#include "pb17.h"
#include "pb16.h"
#include "pb15.h"
#include "pb14.h"

/** */
namespace Oscl{
/** */
namespace Mot860{
/** */
namespace PortB {

/** */
class Port : public PortApi {
	private:
		/** */
		Oscl::Bits::Port<Mot8xx::Port::PBDAT::Reg>			_pbdat;
		/** */
		Oscl::Bits::MutexedPort<Mot8xx::Port::PBDAT::Reg>	_mutexedPBDAT;
		/** */
		Oscl::Bits::Port<Mot8xx::Port::PBDIR::Reg>			_pbdir;
		/** */
		Oscl::Bits::MutexedPort<Mot8xx::Port::PBDAT::Reg>	_mutexedPBDIR;
		/** */
		Oscl::Bits::Port<Mot8xx::Port::PBPAR::Reg>			_pbpar;
		/** */
		Oscl::Bits::MutexedPort<Mot8xx::Port::PBPAR::Reg>	_mutexedPBPAR;
		/** */
		Oscl::Bits::Port<Mot8xx::Port::PBODR::Reg>			_pbodr;
		/** */
		Oscl::Bits::MutexedPort<Mot8xx::Port::PBODR::Reg>	_mutexedPBODR;
		/** */
		Oscl::Bits::MutexedField<Mot8xx::Port::PBDAT::Reg>	_reservations;
		/** */
		PortB::PB31											_pb31;
		/** */
		PortB::PB30											_pb30;
		/** */
		PortB::PB29											_pb29;
		/** */
		PortB::PB28											_pb28;
		/** */
		PortB::PB27											_pb27;
		/** */
		PortB::PB26											_pb26;
		/** */
		PortB::PB25											_pb25;
		/** */
		PortB::PB24											_pb24;
		/** */
		PortB::PB23											_pb23;
		/** */
		PortB::PB22											_pb22;
		/** */
		PortB::PB21											_pb21;
		/** */
		PortB::PB20											_pb20;
		/** */
		PortB::PB19											_pb19;
		/** */
		PortB::PB18											_pb18;
		/** */
		PortB::PB17											_pb17;
		/** */
		PortB::PB16											_pb16;
		/** */
		PortB::PB15											_pb15;
		/** */
		PortB::PB14											_pb14;
	public:
		/** */
		Port(	Oscl::Mt::Mutex::Simple::Api&		mutex,
				volatile Mot8xx::Port::PBDAT::Reg&	pbdat,
				Mot8xx::Port::PBDIR::Reg&			pbdir,
				Mot8xx::Port::PBPAR::Reg&			pbpar,
				Mot8xx::Port::PBODR::Reg&			pbodr
				) noexcept;
	public:	// PortApi
		/** */
		PB31Api&	getPB31() noexcept;
		/** */
		PB30Api&	getPB30() noexcept;
		/** */
		PB29Api&	getPB29() noexcept;
		/** */
		PB28Api&	getPB28() noexcept;
		/** */
		PB27Api&	getPB27() noexcept;
		/** */
		PB26Api&	getPB26() noexcept;
		/** */
		PB25Api&	getPB25() noexcept;
		/** */
		PB24Api&	getPB24() noexcept;
		/** */
		PB23Api&	getPB23() noexcept;
		/** */
		PB22Api&	getPB22() noexcept;
		/** */
		PB21Api&	getPB21() noexcept;
		/** */
		PB20Api&	getPB20() noexcept;
		/** */
		PB19Api&	getPB19() noexcept;
		/** */
		PB18Api&	getPB18() noexcept;
		/** */
		PB17Api&	getPB17() noexcept;
		/** */
		PB16Api&	getPB16() noexcept;
		/** */
		PB15Api&	getPB15() noexcept;
		/** */
		PB14Api&	getPB14() noexcept;
	};

}
}
}


#endif
