/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_mot_mpc860_portb_pb15h_
#define _oscl_drv_mot_mpc860_portb_pb15h_
#include "pb15api.h"
#include "oscl/bits/port.h"
#include "oscl/bits/iobit.h"
#include "oscl/hw/motorola/mpc8xx/portreg.h"

/** */
namespace Oscl{
/** */
namespace Mot860{
/** */
namespace PortB {

/** */
class PB15 : public PB15Api {
	private:
		/** */
		enum{_bitNum=((sizeof(Mot8xx::Port::PBDAT::Reg)*8)-1-15)};
		/** */
		enum{_mask=(1<<_bitNum)};
		/** */
		Bits::IoBit<Mot8xx::Port::PBDAT::Reg,_bitNum,true>	_port;
		/** */
		Bits::IoBit<Mot8xx::Port::PBDIR::Reg,_bitNum,true>	_pbdir;
		/** */
		Bits::IoBit<Mot8xx::Port::PBPAR::Reg,_bitNum,true>	_pbpar;
		/** */
		Bits::IoBit<Mot8xx::Port::PBODR::Reg,_bitNum,true>	_pbodr;
		/** */
		Bits::FieldApi<Mot8xx::Port::PBDAT::Reg>&			_reservation;
	public:
		/** */
		PB15(	Bits::FieldApi<Mot8xx::Port::PBDAT::Reg>&	port,
				Bits::FieldApi<Mot8xx::Port::PBDIR::Reg>&	pbdir,
				Bits::FieldApi<Mot8xx::Port::PBPAR::Reg>&	pbpar,
				Bits::FieldApi<Mot8xx::Port::PBODR::Reg>&	pbodr,
				Bits::FieldApi<Mot8xx::Port::PBDAT::Reg>&	reservation
				) noexcept;

	public: // Mpc8xx::IoReserve::IOApi
		/** */
		Bits::InputApi&		reserveInput() noexcept;
		/** */
		Bits::OutputApi&	reserveOutput() noexcept;
		/** */
		Bits::InOutApi&		reserveInOut() noexcept;

	public:	// OpenDrainOutputApi
		/** */
		Bits::OutputApi&	reserveOpenDrainOutput() noexcept;
		/** */
		Bits::InOutApi&		reserveOpenDrainInOut() noexcept;

	public:	// BRGO::BRGO3Api
		/** */
		Bits::InputApi&	reserveBRGO3() noexcept;

	private:
		/** */
		void	reserve() noexcept;
	};

}
}
}


#endif
