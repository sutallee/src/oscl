/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "pb30.h"
#include "oscl/error/fatal.h"

using namespace Oscl;
using namespace Oscl::Mot860::PortB;

PB30::PB30(	Bits::FieldApi<Mot8xx::Port::PBDAT::Reg>&	port,
			Bits::FieldApi<Mot8xx::Port::PBODR::Reg>&	pbdir,
			Bits::FieldApi<Mot8xx::Port::PBPAR::Reg>&	pbpar,
			Bits::FieldApi<Mot8xx::Port::PBODR::Reg>&	pbodr,
			Bits::FieldApi<Mot8xx::Port::PBDAT::Reg>&	reservation
			) noexcept:
		_port(port),
		_pbdir(pbdir),
		_pbpar(pbpar),
		_pbodr(pbodr),
		_reservation(reservation)
		{
	}

Bits::InputApi&		PB30::reserveInput() noexcept{
	reserve();
	_pbpar.low();
	_pbdir.low();
	_pbodr.low();
	return _port;
	}

Bits::OutputApi&	PB30::reserveOutput() noexcept{
	reserve();
	_pbpar.low();
	_pbdir.high();
	_pbodr.low();
	return _port;
	}

Bits::InOutApi&		PB30::reserveInOut() noexcept{
	reserve();
	_pbpar.low();
	_pbdir.high();
	_pbodr.low();
	return _port;
	}

Bits::OutputApi&	PB30::reserveOpenDrainOutput() noexcept{
	reserve();
	_pbpar.low();
	_pbdir.low();
	_pbodr.high();
	return _port;
	}

Bits::InOutApi&		PB30::reserveOpenDrainInOut() noexcept{
	reserve();
	_pbpar.low();
	_pbdir.high();
	_pbodr.high();
	return _port;
	}

Bits::InputApi&	PB30::reserveRSTRT2() noexcept{
	reserve();
	_pbpar.high();
	_pbdir.low();
	_pbodr.low();
	return _port;
	}

Bits::InputApi&	PB30::reserveSPICLK() noexcept{
	reserve();
	_pbpar.high();
	_pbdir.high();
	_pbodr.low();
	return _port;
	}

void	PB30::reserve() noexcept{
	if(_reservation.testAndSet(_mask)) return;
	ErrorFatal::logAndExit("PB30 already reserved.");
	}

