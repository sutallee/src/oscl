/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_mot_mpc860_portb_portapih_
#define _oscl_drv_mot_mpc860_portb_portapih_
#include "pb31api.h"
#include "pb30api.h"
#include "pb29api.h"
#include "pb28api.h"
#include "pb27api.h"
#include "pb26api.h"
#include "pb25api.h"
#include "pb24api.h"
#include "pb23api.h"
#include "pb22api.h"
#include "pb21api.h"
#include "pb20api.h"
#include "pb19api.h"
#include "pb18api.h"
#include "pb17api.h"
#include "pb16api.h"
#include "pb15api.h"
#include "pb14api.h"

/** */
namespace Oscl{
/** */
namespace Mot860{
/** */
namespace PortB {

/** */
class PortApi {
	public:
		/** Shut-up GCC. */
		virtual ~PortApi() {}
		/** */
		virtual PB31Api&	getPB31() noexcept=0;
		/** */
		virtual PB30Api&	getPB30() noexcept=0;
		/** */
		virtual PB29Api&	getPB29() noexcept=0;
		/** */
		virtual PB28Api&	getPB28() noexcept=0;
		/** */
		virtual PB27Api&	getPB27() noexcept=0;
		/** */
		virtual PB26Api&	getPB26() noexcept=0;
		/** */
		virtual PB25Api&	getPB25() noexcept=0;
		/** */
		virtual PB24Api&	getPB24() noexcept=0;
		/** */
		virtual PB23Api&	getPB23() noexcept=0;
		/** */
		virtual PB22Api&	getPB22() noexcept=0;
		/** */
		virtual PB21Api&	getPB21() noexcept=0;
		/** */
		virtual PB20Api&	getPB20() noexcept=0;
		/** */
		virtual PB19Api&	getPB19() noexcept=0;
		/** */
		virtual PB18Api&	getPB18() noexcept=0;
		/** */
		virtual PB17Api&	getPB17() noexcept=0;
		/** */
		virtual PB16Api&	getPB16() noexcept=0;
		/** */
		virtual PB15Api&	getPB15() noexcept=0;
		/** */
		virtual PB14Api&	getPB14() noexcept=0;
	};

}
}
}

#endif
