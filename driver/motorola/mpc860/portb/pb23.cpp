/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "pb23.h"
#include "oscl/error/fatal.h"

using namespace Oscl;
using namespace Oscl::Mot860::PortB;

PB23::PB23(	Bits::FieldApi<Mot8xx::Port::PBDAT::Reg>&	port,
			Bits::FieldApi<Mot8xx::Port::PBODR::Reg>&	pbdir,
			Bits::FieldApi<Mot8xx::Port::PBPAR::Reg>&	pbpar,
			Bits::FieldApi<Mot8xx::Port::PBODR::Reg>&	pbodr,
			Bits::FieldApi<Mot8xx::Port::PBDAT::Reg>&	reservation
			) noexcept:
		_port(port),
		_pbdir(pbdir),
		_pbpar(pbpar),
		_pbodr(pbodr),
		_reservation(reservation)
		{
	}

Bits::InputApi&		PB23::reserveInput() noexcept{
	reserve();
	_pbpar.low();
	_pbdir.low();
	_pbodr.low();
	return _port;
	}

Bits::OutputApi&	PB23::reserveOutput() noexcept{
	reserve();
	_pbpar.low();
	_pbdir.high();
	_pbodr.low();
	return _port;
	}

Bits::InOutApi&		PB23::reserveInOut() noexcept{
	reserve();
	_pbpar.low();
	_pbdir.high();
	_pbodr.low();
	return _port;
	}

Bits::OutputApi&	PB23::reserveOpenDrainOutput() noexcept{
	reserve();
	_pbpar.low();
	_pbdir.low();
	_pbodr.high();
	return _port;
	}

Bits::InOutApi&		PB23::reserveOpenDrainInOut() noexcept{
	reserve();
	_pbpar.low();
	_pbdir.high();
	_pbodr.high();
	return _port;
	}

Bits::InputApi&	PB23::reserveSMSYN1() noexcept{
	reserve();
	_pbpar.high();
	_pbdir.low();
	_pbodr.low();
	return _port;
	}

Bits::InputApi&	PB23::reserveSDACK1() noexcept{
	reserve();
	_pbpar.high();
	_pbdir.high();
	_pbodr.low();
	return _port;
	}

void	PB23::reserve() noexcept{
	if(_reservation.testAndSet(_mask)) return;
	ErrorFatal::logAndExit("PB23 already reserved.");
	}

