/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "port.h"

using namespace Oscl::Mot860::PortB;

Port::Port(	Oscl::Mt::Mutex::Simple::Api&		mutex,
			volatile Mot8xx::Port::PBDAT::Reg&	pbdat,
			Mot8xx::Port::PBDIR::Reg&			pbdir,
			Mot8xx::Port::PBPAR::Reg&			pbpar,
			Mot8xx::Port::PBODR::Reg&			pbodr
			) noexcept:
		_pbdat(pbdat,0),
		_mutexedPBDAT(_pbdat,mutex),
		_pbdir(pbdir,0),
		_mutexedPBDIR(_pbdir,mutex),
		_pbpar(pbpar,0),
		_mutexedPBPAR(_pbpar,mutex),
		_pbodr(pbodr,0),
		_mutexedPBODR(_pbodr,mutex),
		_reservations(mutex,0),
		_pb31(_mutexedPBDAT,_mutexedPBDIR,_mutexedPBPAR,_mutexedPBODR,_reservations),
		_pb30(_mutexedPBDAT,_mutexedPBDIR,_mutexedPBPAR,_mutexedPBODR,_reservations),
		_pb29(_mutexedPBDAT,_mutexedPBDIR,_mutexedPBPAR,_mutexedPBODR,_reservations),
		_pb28(_mutexedPBDAT,_mutexedPBDIR,_mutexedPBPAR,_mutexedPBODR,_reservations),
		_pb27(_mutexedPBDAT,_mutexedPBDIR,_mutexedPBPAR,_mutexedPBODR,_reservations),
		_pb26(_mutexedPBDAT,_mutexedPBDIR,_mutexedPBPAR,_mutexedPBODR,_reservations),
		_pb25(_mutexedPBDAT,_mutexedPBDIR,_mutexedPBPAR,_mutexedPBODR,_reservations),
		_pb24(_mutexedPBDAT,_mutexedPBDIR,_mutexedPBPAR,_mutexedPBODR,_reservations),
		_pb23(_mutexedPBDAT,_mutexedPBDIR,_mutexedPBPAR,_mutexedPBODR,_reservations),
		_pb22(_mutexedPBDAT,_mutexedPBDIR,_mutexedPBPAR,_mutexedPBODR,_reservations),
		_pb21(_mutexedPBDAT,_mutexedPBDIR,_mutexedPBPAR,_mutexedPBODR,_reservations),
		_pb20(_mutexedPBDAT,_mutexedPBDIR,_mutexedPBPAR,_mutexedPBODR,_reservations),
		_pb19(_mutexedPBDAT,_mutexedPBDIR,_mutexedPBPAR,_mutexedPBODR,_reservations),
		_pb18(_mutexedPBDAT,_mutexedPBDIR,_mutexedPBPAR,_mutexedPBODR,_reservations),
		_pb17(_mutexedPBDAT,_mutexedPBDIR,_mutexedPBPAR,_mutexedPBODR,_reservations),
		_pb16(_mutexedPBDAT,_mutexedPBDIR,_mutexedPBPAR,_mutexedPBODR,_reservations),
		_pb15(_mutexedPBDAT,_mutexedPBDIR,_mutexedPBPAR,_mutexedPBODR,_reservations),
		_pb14(_mutexedPBDAT,_mutexedPBDIR,_mutexedPBPAR,_mutexedPBODR,_reservations)
		{
	}

PB31Api&	Port::getPB31() noexcept{
	return _pb31;
	}

PB30Api&	Port::getPB30() noexcept{
	return _pb30;
	}

PB29Api&	Port::getPB29() noexcept{
	return _pb29;
	}

PB28Api&	Port::getPB28() noexcept{
	return _pb28;
	}

PB27Api&	Port::getPB27() noexcept{
	return _pb27;
	}

PB26Api&	Port::getPB26() noexcept{
	return _pb26;
	}

PB25Api&	Port::getPB25() noexcept{
	return _pb25;
	}

PB24Api&	Port::getPB24() noexcept{
	return _pb24;
	}

PB23Api&	Port::getPB23() noexcept{
	return _pb23;
	}

PB22Api&	Port::getPB22() noexcept{
	return _pb22;
	}

PB21Api&	Port::getPB21() noexcept{
	return _pb21;
	}

PB20Api&	Port::getPB20() noexcept{
	return _pb20;
	}

PB19Api&	Port::getPB19() noexcept{
	return _pb19;
	}

PB18Api&	Port::getPB18() noexcept{
	return _pb18;
	}

PB17Api&	Port::getPB17() noexcept{
	return _pb17;
	}

PB16Api&	Port::getPB16() noexcept{
	return _pb16;
	}

PB15Api&	Port::getPB15() noexcept{
	return _pb15;
	}

PB14Api&	Port::getPB14() noexcept{
	return _pb14;
	}

