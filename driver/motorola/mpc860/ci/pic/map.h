/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_mot_mpc860_ci_pic_maph_
#define _oscl_drv_mot_mpc860_ci_pic_maph_
#include "api.h"
#include "oscl/driver/motorola/mpc8xx/ci/picapi.h"

/** */
namespace Oscl {
/** */
namespace Mot860 {
/** */
namespace CI {
/** */
namespace PIC {

/** */
class Map : public Api {
	private:
		/** */
		Oscl::Mot8xx::CI::PicApi&	_pic;
	public:
		/** */
		Map(Oscl::Mot8xx::CI::PicApi& pic) noexcept;

	private:	// PIC::Api
		/** */
		Oscl::Interrupt::StatusHandlerApi&	getISR() noexcept;
		/** */
		Oscl::Mot8xx::CI::IrqApi&	getPC4() noexcept;
		/** */
		Oscl::Mot8xx::CI::IrqApi&	getPC5() noexcept;
		/** */
		Oscl::Mot8xx::CI::IrqApi&	getSMC2() noexcept;
		/** */
		Oscl::Mot8xx::CI::IrqApi&	getSMC1() noexcept;
		/** */
		Oscl::Mot8xx::CI::IrqApi&	getSPI() noexcept;
		/** */
		Oscl::Mot8xx::CI::IrqApi&	getPC6() noexcept;
		/** */
		Oscl::Mot8xx::CI::IrqApi&	getTIMER4() noexcept;
		/** */
		Oscl::Mot8xx::CI::IrqApi&	getBit8() noexcept;
		/** */
		Oscl::Mot8xx::CI::IrqApi&	getPC7() noexcept;
		/** */
		Oscl::Mot8xx::CI::IrqApi&	getPC8() noexcept;
		/** */
		Oscl::Mot8xx::CI::IrqApi&	getPC9() noexcept;
		/** */
		Oscl::Mot8xx::CI::IrqApi&	getTIMER3() noexcept;
		/** */
		Oscl::Mot8xx::CI::IrqApi&	getBit13() noexcept;
		/** */
		Oscl::Mot8xx::CI::IrqApi&	getPC10() noexcept;
		/** */
		Oscl::Mot8xx::CI::IrqApi&	getPC11() noexcept;
		/** */
		Oscl::Mot8xx::CI::IrqApi&	getI2C() noexcept;
		/** */
		Oscl::Mot8xx::CI::IrqApi&	getRTT() noexcept;
		/** */
		Oscl::Mot8xx::CI::IrqApi&	getTIMER2() noexcept;
		/** */
		Oscl::Mot8xx::CI::IrqApi&	getBit19() noexcept;
		/** */
		Oscl::Mot8xx::CI::IrqApi&	getIDMA2() noexcept;
		/** */
		Oscl::Mot8xx::CI::IrqApi&	getIDMA1() noexcept;
		/** */
		Oscl::Mot8xx::CI::IrqApi&	getSDMA() noexcept;
		/** */
		Oscl::Mot8xx::CI::IrqApi&	getPC12() noexcept;
		/** */
		Oscl::Mot8xx::CI::IrqApi&	getPC13() noexcept;
		/** */
		Oscl::Mot8xx::CI::IrqApi&	getTIMER1() noexcept;
		/** */
		Oscl::Mot8xx::CI::IrqApi&	getPC14() noexcept;
		/** */
		Oscl::Mot8xx::CI::IrqApi&	getSCC4() noexcept;
		/** */
		Oscl::Mot8xx::CI::IrqApi&	getSCC3() noexcept;
		/** */
		Oscl::Mot8xx::CI::IrqApi&	getSCC2() noexcept;
		/** */
		Oscl::Mot8xx::CI::IrqApi&	getSCC1() noexcept;
		/** */
		Oscl::Mot8xx::CI::IrqApi&	getPC15() noexcept;
	};

}
}
}
}

#endif
