/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_mot_mpc860_ci_pic_apih_
#define _oscl_drv_mot_mpc860_ci_pic_apih_
#include "oscl/driver/motorola/mpc8xx/ci/irqapi.h"

/** */
namespace Oscl {
/** */
namespace Mot860 {
/** */
namespace CI {
/** */
namespace PIC {

/** */
class Api {
	public:
		/** */
		virtual ~Api() {}
		/** */
		virtual Oscl::Interrupt::StatusHandlerApi&	getISR() noexcept=0;
		/** */
		virtual Oscl::Mot8xx::CI::IrqApi&	getPC4() noexcept=0;
		/** */
		virtual Oscl::Mot8xx::CI::IrqApi&	getPC5() noexcept=0;
		/** */
		virtual Oscl::Mot8xx::CI::IrqApi&	getSMC2() noexcept=0;
		/** */
		virtual Oscl::Mot8xx::CI::IrqApi&	getSMC1() noexcept=0;
		/** */
		virtual Oscl::Mot8xx::CI::IrqApi&	getSPI() noexcept=0;
		/** */
		virtual Oscl::Mot8xx::CI::IrqApi&	getPC6() noexcept=0;
		/** */
		virtual Oscl::Mot8xx::CI::IrqApi&	getTIMER4() noexcept=0;
		/** */
		virtual Oscl::Mot8xx::CI::IrqApi&	getBit8() noexcept=0;
		/** */
		virtual Oscl::Mot8xx::CI::IrqApi&	getPC7() noexcept=0;
		/** */
		virtual Oscl::Mot8xx::CI::IrqApi&	getPC8() noexcept=0;
		/** */
		virtual Oscl::Mot8xx::CI::IrqApi&	getPC9() noexcept=0;
		/** */
		virtual Oscl::Mot8xx::CI::IrqApi&	getTIMER3() noexcept=0;
		/** */
		virtual Oscl::Mot8xx::CI::IrqApi&	getBit13() noexcept=0;
		/** */
		virtual Oscl::Mot8xx::CI::IrqApi&	getPC10() noexcept=0;
		/** */
		virtual Oscl::Mot8xx::CI::IrqApi&	getPC11() noexcept=0;
		/** */
		virtual Oscl::Mot8xx::CI::IrqApi&	getI2C() noexcept=0;
		/** */
		virtual Oscl::Mot8xx::CI::IrqApi&	getRTT() noexcept=0;
		/** */
		virtual Oscl::Mot8xx::CI::IrqApi&	getTIMER2() noexcept=0;
		/** */
		virtual Oscl::Mot8xx::CI::IrqApi&	getBit19() noexcept=0;
		/** */
		virtual Oscl::Mot8xx::CI::IrqApi&	getIDMA2() noexcept=0;
		/** */
		virtual Oscl::Mot8xx::CI::IrqApi&	getIDMA1() noexcept=0;
		/** */
		virtual Oscl::Mot8xx::CI::IrqApi&	getSDMA() noexcept=0;
		/** */
		virtual Oscl::Mot8xx::CI::IrqApi&	getPC12() noexcept=0;
		/** */
		virtual Oscl::Mot8xx::CI::IrqApi&	getPC13() noexcept=0;
		/** */
		virtual Oscl::Mot8xx::CI::IrqApi&	getTIMER1() noexcept=0;
		/** */
		virtual Oscl::Mot8xx::CI::IrqApi&	getPC14() noexcept=0;
		/** */
		virtual Oscl::Mot8xx::CI::IrqApi&	getSCC4() noexcept=0;
		/** */
		virtual Oscl::Mot8xx::CI::IrqApi&	getSCC3() noexcept=0;
		/** */
		virtual Oscl::Mot8xx::CI::IrqApi&	getSCC2() noexcept=0;
		/** */
		virtual Oscl::Mot8xx::CI::IrqApi&	getSCC1() noexcept=0;
		/** */
		virtual Oscl::Mot8xx::CI::IrqApi&	getPC15() noexcept=0;
	};

}
}
}
}

#endif
