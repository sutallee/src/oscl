/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "map.h"

using namespace Oscl::Mot860::CI::PIC;

Map::Map(Oscl::Mot8xx::CI::PicApi& pic) noexcept:
		_pic(pic)
		{
	}

Oscl::Interrupt::StatusHandlerApi&	Map::getISR() noexcept{
	return _pic;
	}

Oscl::Mot8xx::CI::IrqApi&	Map::getPC4() noexcept{
	return _pic.getIrqApi(1);
	}

Oscl::Mot8xx::CI::IrqApi&	Map::getPC5() noexcept{
	return _pic.getIrqApi(2);
	}

Oscl::Mot8xx::CI::IrqApi&	Map::getSMC2() noexcept{
	return _pic.getIrqApi(3);
	}

Oscl::Mot8xx::CI::IrqApi&	Map::getSMC1() noexcept{
	return _pic.getIrqApi(4);
	}

Oscl::Mot8xx::CI::IrqApi&	Map::getSPI() noexcept{
	return _pic.getIrqApi(5);
	}

Oscl::Mot8xx::CI::IrqApi&	Map::getPC6() noexcept{
	return _pic.getIrqApi(6);
	}

Oscl::Mot8xx::CI::IrqApi&	Map::getTIMER4() noexcept{
	return _pic.getIrqApi(7);
	}

Oscl::Mot8xx::CI::IrqApi&	Map::getBit8() noexcept{
	return _pic.getIrqApi(8);
	}

Oscl::Mot8xx::CI::IrqApi&	Map::getPC7() noexcept{
	return _pic.getIrqApi(9);
	}

Oscl::Mot8xx::CI::IrqApi&	Map::getPC8() noexcept{
	return _pic.getIrqApi(10);
	}

Oscl::Mot8xx::CI::IrqApi&	Map::getPC9() noexcept{
	return _pic.getIrqApi(11);
	}

Oscl::Mot8xx::CI::IrqApi&	Map::getTIMER3() noexcept{
	return _pic.getIrqApi(12);
	}

Oscl::Mot8xx::CI::IrqApi&	Map::getBit13() noexcept{
	return _pic.getIrqApi(13);
	}

Oscl::Mot8xx::CI::IrqApi&	Map::getPC10() noexcept{
	return _pic.getIrqApi(14);
	}

Oscl::Mot8xx::CI::IrqApi&	Map::getPC11() noexcept{
	return _pic.getIrqApi(15);
	}

Oscl::Mot8xx::CI::IrqApi&	Map::getI2C() noexcept{
	return _pic.getIrqApi(16);
	}

Oscl::Mot8xx::CI::IrqApi&	Map::getRTT() noexcept{
	return _pic.getIrqApi(17);
	}

Oscl::Mot8xx::CI::IrqApi&	Map::getTIMER2() noexcept{
	return _pic.getIrqApi(18);
	}

Oscl::Mot8xx::CI::IrqApi&	Map::getBit19() noexcept{
	return _pic.getIrqApi(19);
	}

Oscl::Mot8xx::CI::IrqApi&	Map::getIDMA2() noexcept{
	return _pic.getIrqApi(20);
	}

Oscl::Mot8xx::CI::IrqApi&	Map::getIDMA1() noexcept{
	return _pic.getIrqApi(21);
	}

Oscl::Mot8xx::CI::IrqApi&	Map::getSDMA() noexcept{
	return _pic.getIrqApi(22);
	}

Oscl::Mot8xx::CI::IrqApi&	Map::getPC12() noexcept{
	return _pic.getIrqApi(23);
	}

Oscl::Mot8xx::CI::IrqApi&	Map::getPC13() noexcept{
	return _pic.getIrqApi(24);
	}

Oscl::Mot8xx::CI::IrqApi&	Map::getTIMER1() noexcept{
	return _pic.getIrqApi(25);
	}

Oscl::Mot8xx::CI::IrqApi&	Map::getPC14() noexcept{
	return _pic.getIrqApi(26);
	}

Oscl::Mot8xx::CI::IrqApi&	Map::getSCC4() noexcept{
	return _pic.getIrqApi(27);
	}

Oscl::Mot8xx::CI::IrqApi&	Map::getSCC3() noexcept{
	return _pic.getIrqApi(28);
	}

Oscl::Mot8xx::CI::IrqApi&	Map::getSCC2() noexcept{
	return _pic.getIrqApi(29);
	}

Oscl::Mot8xx::CI::IrqApi&	Map::getSCC1() noexcept{
	return _pic.getIrqApi(30);
	}

Oscl::Mot8xx::CI::IrqApi&	Map::getPC15() noexcept{
	return _pic.getIrqApi(31);
	}

