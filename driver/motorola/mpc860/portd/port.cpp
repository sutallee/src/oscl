/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "port.h"

using namespace Oscl::Mot860::PortD;

Port::Port(	Oscl::Mt::Mutex::Simple::Api&		mutex,
			volatile Mot8xx::Port::PDDAT::Reg&	pddat,
			Mot8xx::Port::PDDIR::Reg&			pddir,
			Mot8xx::Port::PDPAR::Reg&			pdpar
			) noexcept:
		_pddat(pddat,0),
		_mutexedPDDAT(_pddat,mutex),
		_pddir(pddir,0),
		_mutexedPDDIR(_pddir,mutex),
		_pdpar(pdpar,0),
		_mutexedPDPAR(_pdpar,mutex),
		_reservations(mutex,0),
		_pd15(_mutexedPDDAT,_mutexedPDDIR,_mutexedPDPAR,_reservations),
		_pd14(_mutexedPDDAT,_mutexedPDDIR,_mutexedPDPAR,_reservations),
		_pd13(_mutexedPDDAT,_mutexedPDDIR,_mutexedPDPAR,_reservations),
		_pd12(_mutexedPDDAT,_mutexedPDDIR,_mutexedPDPAR,_reservations),
		_pd11(_mutexedPDDAT,_mutexedPDDIR,_mutexedPDPAR,_reservations),
		_pd10(_mutexedPDDAT,_mutexedPDDIR,_mutexedPDPAR,_reservations),
		_pd9(_mutexedPDDAT,_mutexedPDDIR,_mutexedPDPAR,_reservations),
		_pd8(_mutexedPDDAT,_mutexedPDDIR,_mutexedPDPAR,_reservations),
		_pd7(_mutexedPDDAT,_mutexedPDDIR,_mutexedPDPAR,_reservations),
		_pd6(_mutexedPDDAT,_mutexedPDDIR,_mutexedPDPAR,_reservations),
		_pd5(_mutexedPDDAT,_mutexedPDDIR,_mutexedPDPAR,_reservations),
		_pd4(_mutexedPDDAT,_mutexedPDDIR,_mutexedPDPAR,_reservations),
		_pd3(_mutexedPDDAT,_mutexedPDDIR,_mutexedPDPAR,_reservations)
		{
	}

PD15Api&	Port::getPD15() noexcept{
	return _pd15;
	}

PD14Api&	Port::getPD14() noexcept{
	return _pd14;
	}

PD13Api&	Port::getPD13() noexcept{
	return _pd13;
	}

PD12Api&	Port::getPD12() noexcept{
	return _pd12;
	}

PD11Api&	Port::getPD11() noexcept{
	return _pd11;
	}

PD10Api&	Port::getPD10() noexcept{
	return _pd10;
	}

PD9Api&	Port::getPD9() noexcept{
	return _pd9;
	}

PD8Api&	Port::getPD8() noexcept{
	return _pd8;
	}

PD7Api&	Port::getPD7() noexcept{
	return _pd7;
	}

PD6Api&	Port::getPD6() noexcept{
	return _pd6;
	}

PD5Api&	Port::getPD5() noexcept{
	return _pd5;
	}

PD4Api&	Port::getPD4() noexcept{
	return _pd4;
	}

PD3Api&	Port::getPD3() noexcept{
	return _pd3;
	}

