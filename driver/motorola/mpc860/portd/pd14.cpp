/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "pd14.h"
#include "oscl/error/fatal.h"

using namespace Oscl;
using namespace Oscl::Mot860::PortD;

PD14::PD14(	Bits::FieldApi<Mot8xx::Port::PDDAT::Reg>&	pddat,
			Bits::FieldApi<Mot8xx::Port::PCSO::Reg>&	pddir,
			Bits::FieldApi<Mot8xx::Port::PDPAR::Reg>&	pdpar,
			Bits::FieldApi<Mot8xx::Port::PDDAT::Reg>&	reservation
			) noexcept:
		_pddat(pddat),
		_pddir(pddir),
		_pdpar(pdpar),
		_reservation(reservation)
		{
	}

Bits::InputApi&		PD14::reserveInput() noexcept{
	reserve();
	_pdpar.low();
	_pddir.low();
	return _pddat;
	}

Bits::OutputApi&	PD14::reserveOutput() noexcept{
	reserve();
	_pdpar.low();
	_pddir.high();
	return _pddat;
	}

Bits::InOutApi&		PD14::reserveInOut() noexcept{
	reserve();
	_pdpar.low();
	_pddir.high();
	return _pddat;
	}

Bits::InputApi&	PD14::reserveL1RSYNCA() noexcept{
	reserve();
	_pdpar.high();
	_pddir.low();
	return _pddat;
	}

void	PD14::reserve() noexcept{
	if(_reservation.testAndSet(_mask)) return;
	ErrorFatal::logAndExit("PD14 already reserved.");
	}

