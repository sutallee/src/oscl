/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_mot_mpc860_portd_porth_
#define _oscl_drv_mot_mpc860_portd_porth_
#include "oscl/bits/port.h"
#include "oscl/bits/mt/mutexedbp.h"
#include "oscl/bits/mt/mutexedfield.h"
#include "oscl/hw/motorola/mpc8xx/portreg.h"
#include "portapi.h"
#include "pd15.h"
#include "pd14.h"
#include "pd13.h"
#include "pd12.h"
#include "pd11.h"
#include "pd10.h"
#include "pd9.h"
#include "pd8.h"
#include "pd7.h"
#include "pd6.h"
#include "pd5.h"
#include "pd4.h"
#include "pd3.h"

/** */
namespace Oscl{
/** */
namespace Mot860{
/** */
namespace PortD {

/** */
class Port : public PortApi {
	private:
		/** */
		Oscl::Bits::Port<Mot8xx::Port::PDDAT::Reg>			_pddat;
		/** */
		Oscl::Bits::MutexedPort<Mot8xx::Port::PDDAT::Reg>	_mutexedPDDAT;
		/** */
		Oscl::Bits::Port<Mot8xx::Port::PDDIR::Reg>			_pddir;
		/** */
		Oscl::Bits::MutexedPort<Mot8xx::Port::PDDAT::Reg>	_mutexedPDDIR;
		/** */
		Oscl::Bits::Port<Mot8xx::Port::PDPAR::Reg>			_pdpar;
		/** */
		Oscl::Bits::MutexedPort<Mot8xx::Port::PDPAR::Reg>	_mutexedPDPAR;
		/** */
		Oscl::Bits::MutexedField<Mot8xx::Port::PDDAT::Reg>	_reservations;
		/** */
		PortD::PD15											_pd15;
		/** */
		PortD::PD14											_pd14;
		/** */
		PortD::PD13											_pd13;
		/** */
		PortD::PD12											_pd12;
		/** */
		PortD::PD11											_pd11;
		/** */
		PortD::PD10											_pd10;
		/** */
		PortD::PD9											_pd9;
		/** */
		PortD::PD8											_pd8;
		/** */
		PortD::PD7											_pd7;
		/** */
		PortD::PD6											_pd6;
		/** */
		PortD::PD5											_pd5;
		/** */
		PortD::PD4											_pd4;
		/** */
		PortD::PD3											_pd3;

	public:
		/** */
		Port(	Oscl::Mt::Mutex::Simple::Api&		mutex,
				volatile Mot8xx::Port::PDDAT::Reg&	pddat,
				Mot8xx::Port::PDDIR::Reg&			pddir,
				Mot8xx::Port::PDPAR::Reg&			pdpar
				) noexcept;
	public:	// PortApi
		/** */
		PD15Api&	getPD15() noexcept;
		/** */
		PD14Api&	getPD14() noexcept;
		/** */
		PD13Api&	getPD13() noexcept;
		/** */
		PD12Api&	getPD12() noexcept;
		/** */
		PD11Api&	getPD11() noexcept;
		/** */
		PD10Api&	getPD10() noexcept;
		/** */
		PD9Api&		getPD9() noexcept;
		/** */
		PD8Api&		getPD8() noexcept;
		/** */
		PD7Api&		getPD7() noexcept;
		/** */
		PD6Api&		getPD6() noexcept;
		/** */
		PD5Api&		getPD5() noexcept;
		/** */
		PD4Api&		getPD4() noexcept;
		/** */
		PD3Api&		getPD3() noexcept;
	};

}
}
}


#endif
