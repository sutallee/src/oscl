/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_mot_mpc860_si_mr_driverh_
#define _oscl_drv_mot_mpc860_si_mr_driverh_
#include "driverapi.h"
#include "oscl/bits/port.h"
#include "oscl/bits/mt/mutexedbp.h"
#include "oscl/bits/mt/mutexedfield.h"
#include "smc1.h"
#include "smc2.h"
#include "oscl/driver/motorola/mpc8xx/si/mr/tdm.h"

/** */
namespace Oscl{
/** */
namespace Mot860{
/** */
namespace SI {
/** */
namespace MR {

/** */
class Driver : public DriverApi {
	private:
		/** */
		Oscl::Mt::Mutex::Simple::Api&							_mutex;
		/** */
		Oscl::Bits::Port<Oscl::Mot8xx::Si::SIMODE::Reg>			_simr;
		/** */
		Oscl::Bits::MutexedPort<Oscl::Mot8xx::Si::SIMODE::Reg>	_mutexedSIMODE;
		/** */
		bool							_reservedSMC1;
		/** */
		bool							_reservedSMC2;
		/** */
		bool							_reservedTDMA;
		/** */
		bool							_reservedTDMB;
		/** */
		Oscl::Mot860::SI::MR::SMC1		_smc1;
		/** */
		Oscl::Mot860::SI::MR::SMC2		_smc2;
		/** */
		Oscl::Mot8xx::SI::MR::
		TDM<Oscl::Mot8xx::Si::SIMODE::Reg,0>		_tdma;
		/** */
		Oscl::Mot8xx::SI::MR::
		TDM<Oscl::Mot8xx::Si::SIMODE::Reg,8>		_tdmb;

	public:
		/** */
		Driver(	Oscl::Mt::Mutex::Simple::Api&			mutex,
				volatile Oscl::Mot8xx::Si::SIMODE::Reg&	simr
				) noexcept;

	public:	// Api
		/** */
		Oscl::Mot860::SI::MR::SMC1Api&	reserveSMC1() noexcept;
		/** */
		Oscl::Mot860::SI::MR::SMC2Api&	reserveSMC2() noexcept;
		/** */
		Oscl::Mot8xx::SI::MR::TDMApi&	reserveTDMA() noexcept;
		/** */
		Oscl::Mot8xx::SI::MR::TDMApi&	reserveTDMB() noexcept;
	};

}
}
}
}

#endif
