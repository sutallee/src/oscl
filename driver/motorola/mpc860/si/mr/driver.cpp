/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "driver.h"
#include "oscl/mt/scopesync.h"
#include "oscl/error/fatal.h"

using namespace Oscl::Mot860::SI::MR;

Driver::Driver(	Oscl::Mt::Mutex::Simple::Api&			mutex,
				volatile Oscl::Mot8xx::Si::SIMODE::Reg&	simr
				) noexcept:
		_mutex(mutex),
		_simr(simr),
		_mutexedSIMODE(_simr,mutex),
		_reservedSMC1(false),
		_reservedSMC2(false),
		_reservedTDMA(false),
		_reservedTDMB(false),
		_smc1(_mutexedSIMODE),
		_smc2(_mutexedSIMODE),
		_tdma(_mutexedSIMODE),
		_tdmb(_mutexedSIMODE)
		{
	}

Oscl::Mot860::SI::MR::SMC1Api&	Driver::reserveSMC1() noexcept{
		{
		Oscl::Mt::ScopeSync	ss(_mutex);
		if(_reservedSMC1){
			ErrorFatal::logAndExit("SIMODE::SMC1 is already reserved.");
			}
		_reservedSMC1	= true;
		}
	return _smc1;
	}

Oscl::Mot860::SI::MR::SMC2Api&	Driver::reserveSMC2() noexcept{
		{
		Oscl::Mt::ScopeSync	ss(_mutex);
		if(_reservedSMC2){
			ErrorFatal::logAndExit("SIMODE::SMC2 is already reserved.");
			}
		_reservedSMC2	= true;
		}
	return _smc2;
	}

Oscl::Mot8xx::SI::MR::TDMApi&	Driver::reserveTDMA() noexcept{
		{
		Oscl::Mt::ScopeSync	ss(_mutex);
		if(_reservedTDMA){
			ErrorFatal::logAndExit("SIMODE::TDMA is already reserved.");
			}
		_reservedTDMA	= true;
		}
	return _tdma;
	}

Oscl::Mot8xx::SI::MR::TDMApi&	Driver::reserveTDMB() noexcept{
		{
		Oscl::Mt::ScopeSync	ss(_mutex);
		if(_reservedTDMB){
			ErrorFatal::logAndExit("SIMODE::TDMB is already reserved.");
			}
		_reservedTDMB	= true;
		}
	return _tdmb;
	}

