/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_mot_mpc860_si_mr_smc2h_
#define _oscl_drv_mot_mpc860_si_mr_smc2h_
#include "smc2api.h"
#include "cs2.h"
#include "oscl/driver/motorola/mpc8xx/si/mr/smc.h"

/** */
namespace Oscl {
/** */
namespace Mot860 {
/** */
namespace SI {
/** */
namespace MR {

/** */
class SMC2 : public SMC2Api {
	private:
		/** */
		Oscl::Mot8xx::SI::MR::SMC<	Oscl::Mot8xx::Si::SIMODE::Reg,
									Oscl::Mot8xx::Si::SIMODE::SMC2::Lsb
			>			_smc;
		/** */
		Oscl::Mot860::SI::MR::CS2<	Oscl::Mot8xx::Si::SIMODE::Reg,
									Oscl::Mot8xx::Si::SIMODE::SMC2CS::Lsb
			>			_cs;
	public:
		/** */
		SMC2(Oscl::Bits::FieldApi<Oscl::Mot8xx::Si::SIMODE::Reg>& simr) noexcept;

	public:
		/** */
		Oscl::Mot8xx::SI::MR::SMCApi&	getSMC() noexcept;
		/** */
		CS2Api&							getCS() noexcept;
	};

}
}
}
}


#endif
