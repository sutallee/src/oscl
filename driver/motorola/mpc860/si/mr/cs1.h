/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_mot_mpc860_si_mr_cs1h_
#define _oscl_drv_mot_mpc860_si_mr_cs1h_
#include "cs1api.h"
#include "oscl/bits/fieldapi.h"
#include "oscl/hw/motorola/mpc8xx/sireg.h"

/** */
namespace Oscl {
/** */
namespace Mot860 {
/** */
namespace SI {
/** */
namespace MR {

/** */
template <class Type, unsigned Lsb>
class CS1 : public CS1Api {
	private:
		/** */
		Oscl::Bits::FieldApi<Type>&		_simr;
	public:
		/** */
		CS1(Oscl::Bits::FieldApi<Type>& simr) noexcept;

	public:	// BRGApi
		/** */
		void	selectBRG1() noexcept;
		/** */
		void	selectBRG2() noexcept;
		/** */
		void	selectBRG3() noexcept;
		/** */
		void	selectBRG4() noexcept;

	public: // Clk1234Api
		/** */
		void	selectCLK1() noexcept;
		/** */
		void	selectCLK2() noexcept;
		/** */
		void	selectCLK3() noexcept;
		/** */
		void	selectCLK4() noexcept;
	};

template <class Type,unsigned Lsb>
CS1<Type,Lsb> ::CS1(Oscl::Bits::FieldApi<Type>& simr) noexcept:
		_simr(simr)
		{
	}

template <class Type,unsigned Lsb>
void	CS1<Type,Lsb> ::selectBRG1() noexcept{
	_simr.changeBits((0x7<<Lsb),0<<Lsb);
	}

template <class Type,unsigned Lsb>
void	CS1<Type,Lsb> ::selectBRG2() noexcept{
	_simr.changeBits((0x7<<Lsb),1<<Lsb);
	}

template <class Type,unsigned Lsb>
void	CS1<Type,Lsb> ::selectBRG3() noexcept{
	_simr.changeBits((0x7<<Lsb),2<<Lsb);
	}

template <class Type,unsigned Lsb>
void	CS1<Type,Lsb> ::selectBRG4() noexcept{
	_simr.changeBits((0x7<<Lsb),3<<Lsb);
	}

template <class Type,unsigned Lsb>
void	CS1<Type,Lsb> ::selectCLK1() noexcept{
	_simr.changeBits((0x7<<Lsb),4<<Lsb);
	}

template <class Type,unsigned Lsb>
void	CS1<Type,Lsb> ::selectCLK2() noexcept{
	_simr.changeBits((0x7<<Lsb),5<<Lsb);
	}

template <class Type,unsigned Lsb>
void	CS1<Type,Lsb> ::selectCLK3() noexcept{
	_simr.changeBits((0x7<<Lsb),6<<Lsb);
	}

template <class Type,unsigned Lsb>
void	CS1<Type,Lsb> ::selectCLK4() noexcept{
	_simr.changeBits((0x7<<Lsb),7<<Lsb);
	}

}
}
}
}


#endif
