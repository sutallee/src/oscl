/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "driver.h"
#include "oscl/mt/scopesync.h"
#include "oscl/error/fatal.h"

using namespace Oscl::Mot860::SI::CR;

Driver::Driver(	Oscl::Mt::Mutex::Simple::Api&			mutex,
				volatile Oscl::Mot8xx::Si::SICR::Reg&	sicr
				) noexcept:
		_mutex(mutex),
		_sicr(sicr),
		_mutexedSICR(_sicr,mutex),
		_reservedSCC1(false),
		_reservedSCC2(false),
		_reservedSCC3(false),
		_reservedSCC4(false),
		_scc1(_mutexedSICR),
		_scc2(_mutexedSICR),
		_scc3(_mutexedSICR),
		_scc4(_mutexedSICR)
		{
	}

Oscl::Mot860::SI::CR::SCC1Api&	Driver::reserveSCC1() noexcept{
		{
		Oscl::Mt::ScopeSync	ss(_mutex);
		if(_reservedSCC1){
			ErrorFatal::logAndExit("SICR::SCC1 is already reserved.");
			}
		_reservedSCC1	= true;
		}
	return _scc1;
	}

Oscl::Mot860::SI::CR::SCC2Api&	Driver::reserveSCC2() noexcept{
		{
		Oscl::Mt::ScopeSync	ss(_mutex);
		if(_reservedSCC2){
			ErrorFatal::logAndExit("SICR::SCC2 is already reserved.");
			}
		_reservedSCC2	= true;
		}
	return _scc2;
	}

Oscl::Mot860::SI::CR::SCC3Api&	Driver::reserveSCC3() noexcept{
		{
		Oscl::Mt::ScopeSync	ss(_mutex);
		if(_reservedSCC3){
			ErrorFatal::logAndExit("SICR::SCC3 is already reserved.");
			}
		_reservedSCC3	= true;
		}
	return _scc3;
	}

Oscl::Mot860::SI::CR::SCC4Api&	Driver::reserveSCC4() noexcept{
		{
		Oscl::Mt::ScopeSync	ss(_mutex);
		if(_reservedSCC4){
			ErrorFatal::logAndExit("SICR::SCC4 is already reserved.");
			}
		_reservedSCC4	= true;
		}
	return _scc4;
	}

