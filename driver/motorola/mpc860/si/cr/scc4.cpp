/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "scc4.h"

using namespace Oscl::Mot860::SI::CR;

SCC4::SCC4(Oscl::Bits::FieldApi<Oscl::Mot8xx::Si::SICR::Reg>& sicr) noexcept:
		_gr(sicr),
		_sc(sicr),
		_txcs(sicr),
		_rxcs(sicr)
		{
	}

Oscl::Mot8xx::SI::CR::GRApi&		SCC4::getGR() noexcept{
	return _gr;
	}

Oscl::Mot8xx::SI::CR::SCApi&		SCC4::getSC() noexcept{
	return _sc;
	}

Oscl::Mot860::SI::CR::CS34Api&	SCC4::getTCS() noexcept{
	return _txcs;
	}

Oscl::Mot860::SI::CR::CS34Api&	SCC4::getRCS() noexcept{
	return _rxcs;
	}

