/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_mot_mpc860_si_cr_driverh_
#define _oscl_drv_mot_mpc860_si_cr_driverh_
#include "driverapi.h"
#include "oscl/bits/port.h"
#include "oscl/bits/mt/mutexedbp.h"
#include "oscl/bits/mt/mutexedfield.h"
#include "scc1.h"
#include "scc2.h"
#include "scc3.h"
#include "scc4.h"

/** */
namespace Oscl{
/** */
namespace Mot860{
/** */
namespace SI {
/** */
namespace CR {

/** */
class Driver : public DriverApi {
	private:
		/** */
		Oscl::Mt::Mutex::Simple::Api&							_mutex;
		/** */
		Oscl::Bits::Port<Oscl::Mot8xx::Si::SICR::Reg>			_sicr;
		/** */
		Oscl::Bits::MutexedPort<Oscl::Mot8xx::Si::SICR::Reg>	_mutexedSICR;
		/** */
		bool							_reservedSCC1;
		/** */
		bool							_reservedSCC2;
		/** */
		bool							_reservedSCC3;
		/** */
		bool							_reservedSCC4;
		/** */
		Oscl::Mot860::SI::CR::SCC1		_scc1;
		/** */
		Oscl::Mot860::SI::CR::SCC2		_scc2;
		/** */
		Oscl::Mot860::SI::CR::SCC3		_scc3;
		/** */
		Oscl::Mot860::SI::CR::SCC4		_scc4;

	public:
		/** */
		Driver(	Oscl::Mt::Mutex::Simple::Api&			mutex,
				volatile Oscl::Mot8xx::Si::SICR::Reg&	sicr
				) noexcept;
	public:	// Api
		/** */
		Mot860::SI::CR::SCC1Api&	reserveSCC1() noexcept;
		/** */
		Mot860::SI::CR::SCC2Api&	reserveSCC2() noexcept;
		/** */
		Mot860::SI::CR::SCC3Api&	reserveSCC3() noexcept;
		/** */
		Mot860::SI::CR::SCC4Api&	reserveSCC4() noexcept;
	};

}
}
}
}

#endif
