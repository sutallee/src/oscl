/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_mot_mpc860_si_cr_scc4h_
#define _oscl_drv_mot_mpc860_si_cr_scc4h_
#include "scc4api.h"
#include "oscl/driver/motorola/mpc8xx/si/cr/gr.h"
#include "oscl/driver/motorola/mpc8xx/si/cr/sc.h"
#include "cs34.h"

/** */
namespace Oscl {
/** */
namespace Mot860 {
/** */
namespace SI {
/** */
namespace CR {

/** */
class SCC4 : public SCC4Api {
	private:
		/** */
		Oscl::Mot8xx::SI::CR::GR<	Oscl::Mot8xx::Si::SICR::Reg,
			Oscl::Mot8xx::Si::SICR::SCC4::GR::Lsb
			>			_gr;
		/** */
		Oscl::Mot8xx::SI::CR::SC<	Oscl::Mot8xx::Si::SICR::Reg,
			Oscl::Mot8xx::Si::SICR::SCC4::SC::Lsb
			>			_sc;
		/** */
		CS34<	Oscl::Mot8xx::Si::SICR::Reg,
				Oscl::Mot8xx::Si::SICR::SCC4::TCS::Lsb
				>		_txcs;
		/** */
		CS34<	Oscl::Mot8xx::Si::SICR::Reg,
				Oscl::Mot8xx::Si::SICR::SCC4::RCS::Lsb
				>		_rxcs;

	public:
		/** */
		SCC4(Oscl::Bits::FieldApi<Oscl::Mot8xx::Si::SICR::Reg>& sicr) noexcept;

	public:
		/** */
		Oscl::Mot8xx::SI::CR::GRApi&	getGR() noexcept;
		/** */
		Oscl::Mot8xx::SI::CR::SCApi&	getSC() noexcept;
		/** */
		Oscl::Mot860::SI::CR::CS34Api&	getTCS() noexcept;
		/** */
		Oscl::Mot860::SI::CR::CS34Api&	getRCS() noexcept;
	};

}
}
}
}


#endif
