/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_mot_mpc860_si_cr_cs12apih_
#define _oscl_drv_mot_mpc860_si_cr_cs12apih_
#include "oscl/driver/motorola/mpc8xx/si/cr/brgapi.h"
#include "oscl/driver/motorola/mpc8xx/si/cr/clk1234api.h"

/** */
namespace Oscl {
/** */
namespace Mot860 {
/** */
namespace SI {
/** */
namespace CR {

/** */
class CS12Api :
		public Oscl::Mot8xx::SI::CR::BRGApi,
		public Oscl::Mot8xx::SI::CR::Clk1234Api
		{
	public:	// Oscl::Mot8xx::SI::CR::BRGApi
		/** */
		virtual void	selectBRG1() noexcept=0;
		/** */
		virtual void	selectBRG2() noexcept=0;
		/** */
		virtual void	selectBRG3() noexcept=0;
		/** */
		virtual void	selectBRG4() noexcept=0;

	public: // Oscl::Mot8xx::SI::CR::Clk1234Api
		/** */
		virtual void	selectCLK1() noexcept=0;
		/** */
		virtual void	selectCLK2() noexcept=0;
		/** */
		virtual void	selectCLK3() noexcept=0;
		/** */
		virtual void	selectCLK4() noexcept=0;
	};

}
}
}
}


#endif
