/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_mot_mpc860_si_cr_cs34h_
#define _oscl_drv_mot_mpc860_si_cr_cs34h_
#include "cs34api.h"
#include "oscl/bits/fieldapi.h"
#include "oscl/hw/motorola/mpc8xx/sireg.h"

/** */
namespace Oscl {
/** */
namespace Mot860 {
/** */
namespace SI {
/** */
namespace CR {

/** */
template <class Type, unsigned Lsb>
class CS34 : public CS34Api {
	private:
		/** */
		Oscl::Bits::FieldApi<Type>&		_sicr;
	public:
		/** */
		CS34(Oscl::Bits::FieldApi<Type>& sicr) noexcept;

	public:	// Oscl::Mot8xx::SI::CR::BRGApi
		/** */
		void	selectBRG1() noexcept;
		/** */
		void	selectBRG2() noexcept;
		/** */
		void	selectBRG3() noexcept;
		/** */
		void	selectBRG4() noexcept;

	public: // Oscl::Mot860::SI::CR::Clk3434Api
		/** */
		void	selectCLK5() noexcept;
		/** */
		void	selectCLK6() noexcept;
		/** */
		void	selectCLK7() noexcept;
		/** */
		void	selectCLK8() noexcept;
	};

template <class Type,unsigned Lsb>
CS34<Type,Lsb> ::CS34(Oscl::Bits::FieldApi<Type>& sicr) noexcept:
		_sicr(sicr)
		{
	}

template <class Type,unsigned Lsb>
void	CS34<Type,Lsb> ::selectBRG1() noexcept{
	_sicr.changeBits((0x7<<Lsb),0<<Lsb);
	}

template <class Type,unsigned Lsb>
void	CS34<Type,Lsb> ::selectBRG2() noexcept{
	_sicr.changeBits((0x7<<Lsb),1<<Lsb);
	}

template <class Type,unsigned Lsb>
void	CS34<Type,Lsb> ::selectBRG3() noexcept{
	_sicr.changeBits((0x7<<Lsb),2<<Lsb);
	}

template <class Type,unsigned Lsb>
void	CS34<Type,Lsb> ::selectBRG4() noexcept{
	_sicr.changeBits((0x7<<Lsb),3<<Lsb);
	}

template <class Type,unsigned Lsb>
void	CS34<Type,Lsb> ::selectCLK5() noexcept{
	_sicr.changeBits((0x7<<Lsb),4<<Lsb);
	}

template <class Type,unsigned Lsb>
void	CS34<Type,Lsb> ::selectCLK6() noexcept{
	_sicr.changeBits((0x7<<Lsb),5<<Lsb);
	}

template <class Type,unsigned Lsb>
void	CS34<Type,Lsb> ::selectCLK7() noexcept{
	_sicr.changeBits((0x7<<Lsb),6<<Lsb);
	}

template <class Type,unsigned Lsb>
void	CS34<Type,Lsb> ::selectCLK8() noexcept{
	_sicr.changeBits((0x7<<Lsb),7<<Lsb);
	}

}
}
}
}


#endif
