/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_mot_mpc860_porta_pa0apih_
#define _oscl_drv_mot_mpc860_porta_pa0apih_
#include "oscl/driver/motorola/mpc8xx/ioreserve/io.h"
#include "oscl/driver/motorola/mpc8xx/ioreserve/clk/clk8.h"
#include "oscl/driver/motorola/mpc8xx/ioreserve/l1b/tclk.h"
#include "oscl/driver/motorola/mpc8xx/ioreserve/tout/tout4.h"

/** */
namespace Oscl{
/** */
namespace Mot860{
/** */
namespace PortA {

/** */
class PA0Api :
	public Mpc8xx::IoReserve::IOApi,
	public Mpc8xx::IoReserve::CLK::CLK8Api,
	public Mpc8xx::IoReserve::L1B::TCLKApi,
	public Mpc8xx::IoReserve::TOUT::TOUT4Api
	{
	public: // IOApi
		/** */
		virtual Bits::InputApi&		reserveInput() noexcept=0;
		/** */
		virtual Bits::OutputApi&	reserveOutput() noexcept=0;
		/** */
		virtual Bits::InOutApi&		reserveInOut() noexcept=0;

	public:	// CLK::CLK8Api
		/** */
		virtual Bits::InputApi&	reserveCLK8() noexcept=0;

	public:	// L1B::TCLKApi
		/** */
		virtual Bits::InputApi&	reserveL1TCLKB() noexcept=0;

	public:	// TOUT::TOUT4Api
		/** */
		virtual Bits::InputApi&	reserveTOUT4() noexcept=0;
	};

}
}
}

#endif
