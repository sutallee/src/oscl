/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "pa10.h"
#include "oscl/error/fatal.h"

using namespace Oscl;
using namespace Oscl::Mot860::PortA;

PA10::PA10(	Bits::FieldApi<Mot8xx::Port::PADAT::Reg>&	port,
			Bits::FieldApi<Mot8xx::Port::PAODR::Reg>&	padir,
			Bits::FieldApi<Mot8xx::Port::PAPAR::Reg>&	papar,
			Bits::FieldApi<Mot8xx::Port::PAODR::Reg>&	paodr,
			Bits::FieldApi<Mot8xx::Port::PADAT::Reg>&	reservation
			) noexcept:
		_port(port),
		_padir(padir),
		_papar(papar),
		_paodr(paodr),
		_reservation(reservation)
		{
	}

Bits::InputApi&		PA10::reserveInput() noexcept{
	reserve();
	_papar.low();
	_padir.low();
	_paodr.low();
	return _port;
	}

Bits::OutputApi&	PA10::reserveOutput() noexcept{
	reserve();
	_paodr.low();
	_papar.low();
	_padir.high();
	return _port;
	}

Bits::InOutApi&		PA10::reserveInOut() noexcept{
	reserve();
	_paodr.low();
	_papar.low();
	_padir.high();
	return _port;
	}

Bits::OutputApi&	PA10::reserveOpenDrainOutput() noexcept{
	reserve();
	_paodr.high();
	_papar.low();
	_padir.high();
	return _port;
	}

Bits::InOutApi&		PA10::reserveOpenDrainInOut() noexcept{
	reserve();
	_paodr.high();
	_papar.low();
	_padir.high();
	return _port;
	}

Bits::InputApi&	PA10::reserveTXD3() noexcept{
	reserve();
	_papar.high();
	_padir.low();
	_paodr.low();
	return _port;
	}

Bits::InputApi&	PA10::reserveL1RXDB() noexcept{
	reserve();
	_papar.high();
	_padir.high();
	_paodr.low();
	return _port;
	}

void	PA10::reserve() noexcept{
	if(_reservation.testAndSet(_mask)) return;
	ErrorFatal::logAndExit("PA10 already reserved.");
	}

