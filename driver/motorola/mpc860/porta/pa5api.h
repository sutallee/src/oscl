/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_mot_mpc860_porta_pa5apih_
#define _oscl_drv_mot_mpc860_porta_pa5apih_
#include "oscl/driver/motorola/mpc8xx/ioreserve/io.h"
#include "oscl/driver/motorola/mpc8xx/ioreserve/clk/clk3.h"
#include "oscl/driver/motorola/mpc8xx/ioreserve/tin/tin2.h"
#include "oscl/driver/motorola/mpc8xx/ioreserve/brgo/brgo2.h"
#include "oscl/driver/motorola/mpc8xx/ioreserve/l1a/tclk.h"

/** */
namespace Oscl{
/** */
namespace Mot860{
/** */
namespace PortA {

/** */
class PA5Api :
	public Mpc8xx::IoReserve::IOApi,
	public Mpc8xx::IoReserve::CLK::CLK3Api,
	public Mpc8xx::IoReserve::TIN::TIN2Api,
	public Mpc8xx::IoReserve::L1A::TCLKApi,
	public Mpc8xx::IoReserve::BRGO::BRGO2Api
	{
	public: // IOApi
		/** */
		virtual Bits::InputApi&		reserveInput() noexcept=0;
		/** */
		virtual Bits::OutputApi&	reserveOutput() noexcept=0;
		/** */
		virtual Bits::InOutApi&		reserveInOut() noexcept=0;

	public:	// CLK::CLK3Api
		/** */
		virtual Bits::InputApi&	reserveCLK3() noexcept=0;

	public:	// TIN::TIN2Api
		/** */
		virtual Bits::InputApi&	reserveTIN2() noexcept=0;

	public:	// L1A::TCLKApi
		/** */
		virtual Bits::InputApi&	reserveL1TCLKA() noexcept=0;

	public:	// BRGO::BRGO2Api
		/** */
		virtual Bits::InputApi&	reserveBRGO2() noexcept=0;
	};

}
}
}

#endif
