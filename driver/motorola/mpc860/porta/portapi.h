/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_mot_mpc860_porta_portapih_
#define _oscl_drv_mot_mpc860_porta_portapih_
#include "pa15api.h"
#include "pa14api.h"
#include "pa13api.h"
#include "pa12api.h"
#include "pa11api.h"
#include "pa10api.h"
#include "pa9api.h"
#include "pa8api.h"
#include "pa7api.h"
#include "pa6api.h"
#include "pa5api.h"
#include "pa4api.h"
#include "pa3api.h"
#include "pa2api.h"
#include "pa1api.h"
#include "pa0api.h"

/** */
namespace Oscl{
/** */
namespace Mot860{
/** */
namespace PortA {

/** */
class PortApi {
	public:
		/** Shut-up GCC. */
		virtual ~PortApi() {}
		/** */
		virtual PA15Api&	getPA15() noexcept=0;
		/** */
		virtual PA14Api&	getPA14() noexcept=0;
		/** */
		virtual PA13Api&	getPA13() noexcept=0;
		/** */
		virtual PA12Api&	getPA12() noexcept=0;
		/** */
		virtual PA11Api&	getPA11() noexcept=0;
		/** */
		virtual PA10Api&	getPA10() noexcept=0;
		/** */
		virtual PA9Api&	getPA9() noexcept=0;
		/** */
		virtual PA8Api&	getPA8() noexcept=0;
		/** */
		virtual PA7Api&	getPA7() noexcept=0;
		/** */
		virtual PA6Api&	getPA6() noexcept=0;
		/** */
		virtual PA5Api&	getPA5() noexcept=0;
		/** */
		virtual PA4Api&	getPA4() noexcept=0;
		/** */
		virtual PA3Api&	getPA3() noexcept=0;
		/** */
		virtual PA2Api&	getPA2() noexcept=0;
		/** */
		virtual PA1Api&	getPA1() noexcept=0;
		/** */
		virtual PA0Api&	getPA0() noexcept=0;
	};

}
}
}

#endif
