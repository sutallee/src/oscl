/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_mot_mpc860_porta_pa14h_
#define _oscl_drv_mot_mpc860_porta_pa14h_
#include "pa14api.h"
#include "oscl/bits/port.h"
#include "oscl/bits/iobit.h"
#include "oscl/hw/motorola/mpc8xx/portreg.h"

/** */
namespace Oscl{
/** */
namespace Mot860{
/** */
namespace PortA {

/** */
class PA14 : public PA14Api {
	private:
		/** */
		enum{_bitNum=((sizeof(Mot8xx::Port::PADAT::Reg)*8)-1-14)};
		/** */
		enum{_mask=(1<<_bitNum)};
		/** */
		Bits::IoBit<Mot8xx::Port::PADAT::Reg,_bitNum,true>	_port;
		/** */
		Bits::IoBit<Mot8xx::Port::PADIR::Reg,_bitNum,true>	_padir;
		/** */
		Bits::IoBit<Mot8xx::Port::PAPAR::Reg,_bitNum,true>	_papar;
		/** */
		Bits::IoBit<Mot8xx::Port::PAODR::Reg,_bitNum,true>	_paodr;
		/** */
		Bits::FieldApi<Mot8xx::Port::PADAT::Reg>&			_reservation;
	public:
		/** */
		PA14(	Bits::FieldApi<Mot8xx::Port::PADAT::Reg>&	port,
				Bits::FieldApi<Mot8xx::Port::PADIR::Reg>&	padir,
				Bits::FieldApi<Mot8xx::Port::PAPAR::Reg>&	papar,
				Bits::FieldApi<Mot8xx::Port::PAODR::Reg>&	paodr,
				Bits::FieldApi<Mot8xx::Port::PADAT::Reg>&	reservation
				) noexcept;

	public: // Mpc8xx::IoReserve::IOApi
		/** */
		Bits::InputApi&		reserveInput() noexcept;
		/** */
		Bits::OutputApi&	reserveOutput() noexcept;
		/** */
		Bits::InOutApi&		reserveInOut() noexcept;

	public: // Mpc8xx::IoReserve::OpenDrainOutputApi
		/** */
		Bits::OutputApi&	reserveOpenDrainOutput() noexcept;
		/** */
		Bits::InOutApi&		reserveOpenDrainInOut() noexcept;

	public: // Mpc8xx::IoReserve::SCC1::TXDApi
		/** */
		Bits::InputApi&	reserveTXD1() noexcept;

	public: // Mpc8xx::IoReserve::SCC4::TXDApi
		/** */
		Bits::InputApi&	reserveTXD4() noexcept;

	private:
		/** */
		void	reserve() noexcept;
	};

}
}
}


#endif
