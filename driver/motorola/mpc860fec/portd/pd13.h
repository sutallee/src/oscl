/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_mot_mpc860fec_portd_pd13h_
#define _oscl_drv_mot_mpc860fec_portd_pd13h_
#include "pd13api.h"
#include "oscl/bits/port.h"
#include "oscl/bits/iobit.h"
#include "oscl/hw/motorola/mpc8xx/portreg.h"

/** */
namespace Oscl{
/** */
namespace Mot860Fec{
/** */
namespace PortD {

/** */
class PD13 : public PD13Api {
	private:
		/** */
		enum{_bitNum=((sizeof(Mot8xx::Port::PDDAT::Reg)*8)-1-13)};
		/** */
		enum{_mask=(1<<_bitNum)};
		/** */
		Bits::IoBit<Mot8xx::Port::PDDAT::Reg,_bitNum,true>	_pddat;
		/** */
		Bits::IoBit<Mot8xx::Port::PDDIR::Reg,_bitNum,true>	_pddir;
		/** */
		Bits::IoBit<Mot8xx::Port::PDPAR::Reg,_bitNum,true>	_pdpar;
		/** */
		Bits::FieldApi<Mot8xx::Port::PDDAT::Reg>&			_reservation;
	public:
		/** */
		PD13(	Bits::FieldApi<Mot8xx::Port::PDDAT::Reg>&	pddat,
				Bits::FieldApi<Mot8xx::Port::PDDIR::Reg>&	pddir,
				Bits::FieldApi<Mot8xx::Port::PDPAR::Reg>&	pdpar,
				Bits::FieldApi<Mot8xx::Port::PDDAT::Reg>&	reservation
				) noexcept;

	public: // Mpc8xx::IoReserve::IOApi
		/** */
		Bits::InputApi&		reserveInput() noexcept;
		/** */
		Bits::OutputApi&	reserveOutput() noexcept;
		/** */
		Bits::InOutApi&		reserveInOut() noexcept;

	public:	// L1B::TSYNCApi
		/** */
		Bits::InputApi&	reserveL1TSYNCB() noexcept;

	public:	// MII::RXD1Api
		/** */
		Bits::InputApi&	reserveMIIRXD1() noexcept;

	private:
		/** */
		void	reserve() noexcept;
	};

}
}
}


#endif
