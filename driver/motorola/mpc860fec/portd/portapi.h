/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_mot_mpc860fec_portd_portapih_
#define _oscl_drv_mot_mpc860fec_portd_portapih_
#include "pd15api.h"
#include "pd14api.h"
#include "pd13api.h"
#include "pd12api.h"
#include "pd11api.h"
#include "pd10api.h"
#include "pd9api.h"
#include "pd8api.h"
#include "pd7api.h"
#include "pd6api.h"
#include "pd5api.h"
#include "pd4api.h"
#include "pd3api.h"

/** */
namespace Oscl{
/** */
namespace Mot860Fec{
/** */
namespace PortD {

/** */
class PortApi {
	public:
		/** Shut-up GCC. */
		virtual ~PortApi() {}
		/** */
		virtual PD15Api&	getPD15() noexcept=0;
		/** */
		virtual PD14Api&	getPD14() noexcept=0;
		/** */
		virtual PD13Api&	getPD13() noexcept=0;
		/** */
		virtual PD12Api&	getPD12() noexcept=0;
		/** */
		virtual PD11Api&	getPD11() noexcept=0;
		/** */
		virtual PD10Api&	getPD10() noexcept=0;
		/** */
		virtual PD9Api&		getPD9() noexcept=0;
		/** */
		virtual PD8Api&		getPD8() noexcept=0;
		/** */
		virtual PD7Api&		getPD7() noexcept=0;
		/** */
		virtual PD6Api&		getPD6() noexcept=0;
		/** */
		virtual PD5Api&		getPD5() noexcept=0;
		/** */
		virtual PD4Api&		getPD4() noexcept=0;
		/** */
		virtual PD3Api&		getPD3() noexcept=0;
	};

}
}
}

#endif
