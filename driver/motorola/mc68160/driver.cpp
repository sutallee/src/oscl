/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "driver.h"
#include "oscl/mt/thread.h"

using namespace Oscl::Motorola::MC68160;

Driver::Driver(	ControllerSelect		cs,
				AuiTwistedPairSelect	atpConfig,
				bool					autoPolarityCorrect,
				bool					heartbeatEnable,
				bool					fullDuplex,
				Oscl::Bits::InOutApi&	tpen,
				Oscl::Bits::InOutApi&	aport,
				Oscl::Bits::InOutApi&	tpacpe,
				Oscl::Bits::InOutApi&	tpsqel,
				Oscl::Bits::InOutApi&	tpfuldl,
				Oscl::Bits::InOutApi&	loop
				) noexcept:
		_cs(cs),
		_tpen(tpen),
		_aport(aport),
		_tpacpe(tpacpe),
		_tpsqel(tpsqel),
		_tpfuldl(tpfuldl),
		_loop(loop),
		_mfgTesting(true),
		_testMode(false),
		_tpacpeState(autoPolarityCorrect),
		_tpsqelState(heartbeatEnable),
		_tpfuldlState(fullDuplex)
		{
	switch(atpConfig){
		case AutoSelect:
			_aportState	= true;
			_tpenState	= true;
			break;
		case TwistedPair:
			_aportState	= false;
			_tpenState	= true;
			break;
		case AUI:
			_aportState	= false;
			_tpenState	= false;
			break;
		default:
			while(true);
		}
//	reset();
	}

void	Driver::reset() noexcept{
	_loop.low();						// Set Loop LOW
	_tpfuldl.high();					// Disable Full Duplex
	_tpsqel.low();						// Enable Heartbeat
	_loop.high();						// Set Loop HIGH
	Oscl::Mt::Thread::sleep(300/10+1);	// 300ms delay
	_tpfuldl.low();						// Enable Full Dulex
	Oscl::Mt::Thread::sleep(500/10+1);	// 500ms delay
//	_loop.low();						// Set Loop LOW
	disableLoopMode();
	_testMode	= false;
	restoreConfig();
	}

void	Driver::restoreConfig() noexcept{
	// Except full duplex and loop
	// 
	if(_loopState){
		enableLoopMode();
		}
	else{
		disableLoopMode();
		}
	if(_aportState){
		_tpen.high();	// Assumed open collector
		_tpenState	= true;
		_aport.high();
		}
	else {
		_aport.low();
		if(_tpenState){
			_tpen.high();
			}
		else{
			_tpen.low();
			}
		}
	if(_tpacpeState){
		_tpacpe.high();
		}
	else{
		_tpacpe.low();
		}
	if(_tpsqelState){
		_tpsqel.high();
		}
	else{
		_tpsqel.low();
		}
	}

void	Driver::selectAUI() noexcept{
	_aport.low();
	_tpen.low();
	_aportState	= false;
	_tpenState	= false;
	}

void	Driver::selectTwistedPair() noexcept{
	_aport.low();
	_tpen.high();
	_aportState	= false;
	_tpenState	= true;
	}

void	Driver::selectAuto() noexcept{
	_tpen.high();	// Assumption TPEN is Open Collector
	_aport.high();
	_aportState	= true;
	_tpenState	= false;
	}

void	Driver::enableHeartbeat() noexcept{
	_tpsqel.low();	// Assumes Open Collector
	_tpsqelState	= false;
	}

void	Driver::disableHeartbeat() noexcept{
	_tpsqel.high();	// Assumes Open Collector
	_tpsqelState	= true;
	}

void	Driver::enableAutoPolarityCorrection() noexcept{
	_tpacpe.high();
	_tpacpeState	= true;
	}

void	Driver::disableAutoPolarityCorrection() noexcept{
	_tpacpe.low();
	_tpacpeState	= false;
	}

void	Driver::enableFullDuplex() noexcept{
	_tpfuldl.low();
	_tpfuldlState	= false;
	}

void	Driver::disableFullDuplex() noexcept{
	_tpfuldl.high();
	_tpfuldlState	= true;
	}

void	Driver::enableLoopMode() noexcept{
	_tpfuldl.high();
	if(_cs == Motorola){
		_loop.high();
		_loopState	= true;
		}
	else {
		_loop.low();
		_loopState	= false;
		}
	_loopState	= true;
	}

void	Driver::disableLoopMode() noexcept{
	if(_cs == Motorola){
		_loop.low();
		_loopState	= false;
		}
	else {
		_loop.high();
		_loopState	= true;
		}
	/** Restore full duplex if so configured. */
	if(_tpfuldlState){
		_tpfuldl.low();
		}
	else {
		_tpfuldl.high();
		}
	_loopState	= false;
	}

bool	Driver::auiPortSelected() const noexcept{
	return _tpen.state();
	}


#if 0
/////////////////////////////////////////////////////////////////////
// This routine starts the MC68160 test mode.
//

void startMC68160TestMode(Oscl::Est::Mdp860Pro::SCC1::PhyApi& phy){
	phy.getMc68160TpfuldlPin().low();	// Disable Full Duplex
	phy.getMc68160LoopPin().high();		// ?Polarity, Enable Loop
	phy.getMc68160LoopPin().low();		// ?Polarity, Enable Loop
	}

/////////////////////////////////////////////////////////////////////
// This routine resets the MC68160 taking it out of test mode.
//

void haltMC68160TestMode(Oscl::Est::Mdp860Pro::SCC1::PhyApi& phy){
	phy.getMc68160LoopPin().low();		// Set Loop LOW
	phy.getMc68160TpfuldlPin().high();	// Disable Full Duplex
	phy.getMc68160TpsqelPin().low();	// Enable Heartbeat
	phy.getMc68160LoopPin().high();		// Set Loop HIGH
	Oscl::Mt::Thread::sleep(300/10+1);		// 300ms delay
	phy.getMc68160TpfuldlPin().low();	// Enable Full Duplex
	Oscl::Mt::Thread::sleep(500/10+1);		// 500ms delay
	phy.getMc68160LoopPin().low();		// Set Loop LOW
	}

/////////////////////////////////////////////////////////////////////
// Setup MC68160 operating mode.
//
void configureMC68160(Oscl::Est::Mdp860Pro::SCC1::PhyApi& phy){
	haltMC68160TestMode(phy);
	phy.getMc68160LoopPin().low();		// Disable Loop
	phy.getMc68160AportPin().low();		// Disable Automatic Port Select
	phy.getMc68160TpenPin().high();		// Manually select TP port
	phy.getMc68160TpsqelPin().high();	// ? Enable heartbeat
	phy.getMc68160TpacpePin().high();	// Enable Auto Polarity Correction
	phy.getMc68160TpfuldlPin().high();	// Disable Full Duplex
	}

/////////////////////////////////////////////////////////////////////
// Test ability to move MC68160 in and out of test mode.
//
void mc68160Test(	Oscl::Bits::FieldApi<Oscl::Est::Mdp8xxPro::LedControl::Reg>&	mutexedLedControlPort,
					Oscl::Est::Mdp860Pro::SCC1::PhyApi&								phy
					){
	startMC68160TestMode(phy);
	mutexedLedControlPort.changeBits(0xFE,(1<<1));
	Oscl::Mt::Thread::sleep(5000/10+1);			// 5s delay
	haltMC68160TestMode(phy);
	mutexedLedControlPort.changeBits(0xFE,(2<<1));
	Oscl::Mt::Thread::sleep(5000/10+1);			// 5s delay
	configureMC68160(phy);
	mutexedLedControlPort.changeBits(0xFE,(3<<1));
	Oscl::Mt::Thread::sleep(5000/10+1);			// 5s delay
	startMC68160TestMode(phy);
	mutexedLedControlPort.changeBits(0xFE,(4<<1));
	Oscl::Mt::Thread::sleep(5000/10+1);			// 5s delay
	haltMC68160TestMode(phy);
	mutexedLedControlPort.changeBits(0xFE,(5<<1));
	Oscl::Mt::Thread::sleep(5000/10+1);			// 5s delay
	configureMC68160(phy);
	mutexedLedControlPort.changeBits(0xFE,(6<<1));
	Oscl::Mt::Thread::sleep(5000/10+1);			// 5s delay

	phy.getMc68160TpfuldlPin().high();	// Disable Full Duplex
	mutexedLedControlPort.changeBits(0xFE,(7<<1));
	Oscl::Mt::Thread::sleep(5000/10+1);			// 5s delay

	phy.getMc68160LoopPin().high();		// Enable Loop
	mutexedLedControlPort.changeBits(0xFE,(8<<1));
	Oscl::Mt::Thread::sleep(5000/10+1);			// 5s delay

	phy.getMc68160LoopPin().low();		// Disable Loop
	mutexedLedControlPort.changeBits(0xFE,(9<<1));
	Oscl::Mt::Thread::sleep(5000/10+1);			// 5s delay

	phy.getMc68160TpfuldlPin().low();	// Enable Full Duplex
	mutexedLedControlPort.changeBits(0xFE,(10<<1));
	Oscl::Mt::Thread::sleep(5000/10+1);			// 5s delay

	phy.getMc68160LoopPin().high();		// Enable Loop
	mutexedLedControlPort.changeBits(0xFE,(11<<1));
	Oscl::Mt::Thread::sleep(5000/10+1);			// 5s delay

	phy.getMc68160LoopPin().low();		// Disable Loop
	mutexedLedControlPort.changeBits(0xFE,(12<<1));
	Oscl::Mt::Thread::sleep(5000/10+1);			// 5s delay

	haltMC68160TestMode(phy);
	mutexedLedControlPort.changeBits(0xFE,(13<<1));
	Oscl::Mt::Thread::sleep(5000/10+1);			// 5s delay

	configureMC68160(phy);
	mutexedLedControlPort.changeBits(0xFE,(14<<1));
	Oscl::Mt::Thread::sleep(5000/10+1);			// 5s delay
	}

#endif

