/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_mot_mc68160_driverh_
#define _oscl_drv_mot_mc68160_driverh_
#include "api.h"
#include "oscl/bits/ioapi.h"

/** */
namespace Oscl {
/** */
namespace Motorola {
/** */
namespace MC68160 {

/** */
class Driver : public Api {
	public:
		typedef enum {
			AutoSelect,
			TwistedPair,
			AUI
			}AuiTwistedPairSelect;
		typedef enum {
			Motorola	= 0,
			Intel		= 1,
			Fujitsu		= 2,
			National	= 3
			}ControllerSelect;
	private:
		/** */
		const ControllerSelect	_cs;
		/** */
		Oscl::Bits::InOutApi&	_tpen;
		/** */
		Oscl::Bits::InOutApi&	_aport;
		/** */
		Oscl::Bits::InOutApi&	_tpacpe;
		/** */
		Oscl::Bits::InOutApi&	_tpsqel;
		/** */
		Oscl::Bits::InOutApi&	_tpfuldl;
		/** */
		Oscl::Bits::InOutApi&	_loop;
		/** */
		bool					_mfgTesting;
		/** */
		bool					_testMode;
		/** */
		bool					_tpenState;
		/** */
		bool					_aportState;
		/** */
		bool					_tpacpeState;
		/** */
		bool					_tpsqelState;
		/** */
		bool					_tpfuldlState;
		/** */
		bool					_loopState;

	public:
		/** */
		Driver(	ControllerSelect		cs,
				AuiTwistedPairSelect	atpConfig,
				bool					autoPolarityCorrect,
				bool					heartbeat,
				bool					fullDuplex,
				Oscl::Bits::InOutApi&	tpen,
				Oscl::Bits::InOutApi&	aport,
				Oscl::Bits::InOutApi&	tpacpe,
				Oscl::Bits::InOutApi&	tpsqel,
				Oscl::Bits::InOutApi&	tpfuldl,
				Oscl::Bits::InOutApi&	loop
				) noexcept;
	public:
		/** */
		void	reset() noexcept;

		/** */
		void	selectAUI() noexcept;
		/** */
		void	selectTwistedPair() noexcept;
		/** */
		void	selectAuto() noexcept;

		/** */
		void	enableHeartbeat() noexcept;
		/** */
		void	disableHeartbeat() noexcept;

		/** */
		void	enableAutoPolarityCorrection() noexcept;
		/** */
		void	disableAutoPolarityCorrection() noexcept;

		/** */
		void	enableFullDuplex() noexcept;
		/** */
		void	disableFullDuplex() noexcept;

		/** */
		void	enableLoopMode() noexcept;
		/** */
		void	disableLoopMode() noexcept;

		/** */
		bool	auiPortSelected() const noexcept;
	private:
		/** */
		void	restoreConfig() noexcept;
	};

}
}
}

#endif
