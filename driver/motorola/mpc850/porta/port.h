/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_mot_mpc850_porta_porth_
#define _oscl_drv_mot_mpc850_porta_porth_
#include "oscl/bits/port.h"
#include "oscl/bits/mt/mutexedbp.h"
#include "oscl/bits/mt/mutexedfield.h"
#include "oscl/hw/motorola/mpc8xx/portreg.h"
#include "portapi.h"
#include "pa15.h"
#include "pa14.h"
#include "pa13.h"
#include "pa12.h"
#include "pa9.h"
#include "pa8.h"
#include "pa7.h"
#include "pa6.h"
#include "pa5.h"
#include "pa4.h"

/** */
namespace Oscl{
/** */
namespace Mot850{
/** */
namespace PortA {

/** */
class Port : public PortApi {
	private:
		/** */
		Oscl::Bits::Port<Mot8xx::Port::PADAT::Reg>			_padat;
		/** */
		Oscl::Bits::MutexedPort<Mot8xx::Port::PADAT::Reg>	_mutexedPADAT;
		/** */
		Oscl::Bits::Port<Mot8xx::Port::PADIR::Reg>			_padir;
		/** */
		Oscl::Bits::MutexedPort<Mot8xx::Port::PADAT::Reg>	_mutexedPADIR;
		/** */
		Oscl::Bits::Port<Mot8xx::Port::PAPAR::Reg>			_papar;
		/** */
		Oscl::Bits::MutexedPort<Mot8xx::Port::PAPAR::Reg>	_mutexedPAPAR;
		/** */
		Oscl::Bits::Port<Mot8xx::Port::PAODR::Reg>			_paodr;
		/** */
		Oscl::Bits::MutexedPort<Mot8xx::Port::PAODR::Reg>	_mutexedPAODR;
		/** */
		Oscl::Bits::MutexedField<Mot8xx::Port::PADAT::Reg>	_reservations;
		/** */
		PortA::PA15											_pa15;
		/** */
		PortA::PA14											_pa14;
		/** */
		PortA::PA13											_pa13;
		/** */
		PortA::PA12											_pa12;
		/** */
		PortA::PA9											_pa9;
		/** */
		PortA::PA8											_pa8;
		/** */
		PortA::PA7											_pa7;
		/** */
		PortA::PA6											_pa6;
		/** */
		PortA::PA5											_pa5;
		/** */
		PortA::PA4											_pa4;
	public:
		/** */
		Port(	Oscl::Mt::Mutex::Simple::Api&		mutex,
				volatile Mot8xx::Port::PADAT::Reg&	padat,
				Mot8xx::Port::PADIR::Reg&			padir,
				Mot8xx::Port::PAPAR::Reg&			papar,
				Mot8xx::Port::PAODR::Reg&			paodr
				) noexcept;
	public:	// PortApi
		/** */
		PA15Api&	getPA15() noexcept;
		/** */
		PA14Api&	getPA14() noexcept;
		/** */
		PA13Api&	getPA13() noexcept;
		/** */
		PA12Api&	getPA12() noexcept;
		/** */
		PA9Api&		getPA9() noexcept;
		/** */
		PA8Api&		getPA8() noexcept;
		/** */
		PA7Api&		getPA7() noexcept;
		/** */
		PA6Api&		getPA6() noexcept;
		/** */
		PA5Api&		getPA5() noexcept;
		/** */
		PA4Api&		getPA4() noexcept;
	};

}
}
}


#endif
