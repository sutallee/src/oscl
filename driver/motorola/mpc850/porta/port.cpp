/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "port.h"

using namespace Oscl::Mot850::PortA;

Port::Port(	Oscl::Mt::Mutex::Simple::Api&		mutex,
			volatile Mot8xx::Port::PADAT::Reg&	padat,
			Mot8xx::Port::PADIR::Reg&			padir,
			Mot8xx::Port::PAPAR::Reg&			papar,
			Mot8xx::Port::PAODR::Reg&			paodr
			) noexcept:
		_padat(padat,0),
		_mutexedPADAT(_padat,mutex),
		_padir(padir,0),
		_mutexedPADIR(_padir,mutex),
		_papar(papar,0),
		_mutexedPAPAR(_papar,mutex),
		_paodr(paodr,0),
		_mutexedPAODR(_paodr,mutex),
		_reservations(mutex,0),
		_pa15(_mutexedPADAT,_mutexedPADIR,_mutexedPAPAR,_mutexedPAODR,_reservations),
		_pa14(_mutexedPADAT,_mutexedPADIR,_mutexedPAPAR,_mutexedPAODR,_reservations),
		_pa13(_mutexedPADAT,_mutexedPADIR,_mutexedPAPAR,_mutexedPAODR,_reservations),
		_pa12(_mutexedPADAT,_mutexedPADIR,_mutexedPAPAR,_mutexedPAODR,_reservations),
		_pa9(_mutexedPADAT,_mutexedPADIR,_mutexedPAPAR,_mutexedPAODR,_reservations),
		_pa8(_mutexedPADAT,_mutexedPADIR,_mutexedPAPAR,_mutexedPAODR,_reservations),
		_pa7(_mutexedPADAT,_mutexedPADIR,_mutexedPAPAR,_mutexedPAODR,_reservations),
		_pa6(_mutexedPADAT,_mutexedPADIR,_mutexedPAPAR,_mutexedPAODR,_reservations),
		_pa5(_mutexedPADAT,_mutexedPADIR,_mutexedPAPAR,_mutexedPAODR,_reservations),
		_pa4(_mutexedPADAT,_mutexedPADIR,_mutexedPAPAR,_mutexedPAODR,_reservations)
		{
	}

PA15Api&	Port::getPA15() noexcept{
	return _pa15;
	}

PA14Api&	Port::getPA14() noexcept{
	return _pa14;
	}

PA13Api&	Port::getPA13() noexcept{
	return _pa13;
	}

PA12Api&	Port::getPA12() noexcept{
	return _pa12;
	}

PA9Api&	Port::getPA9() noexcept{
	return _pa9;
	}

PA8Api&	Port::getPA8() noexcept{
	return _pa8;
	}

PA7Api&	Port::getPA7() noexcept{
	return _pa7;
	}

PA6Api&	Port::getPA6() noexcept{
	return _pa6;
	}

PA5Api&	Port::getPA5() noexcept{
	return _pa5;
	}

PA4Api&	Port::getPA4() noexcept{
	return _pa4;
	}

