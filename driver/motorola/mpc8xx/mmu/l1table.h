/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _drv_motorola_mpc8xx_mmu_l1tableh_
#define _drv_motorola_mpc8xx_mmu_l1tableh_
#include "oscl/hw/motorola/mpc8xx/mmu/desc.h"
#include "l2table.h"

/** */
namespace Oscl {
/** */
namespace Mot8xx {
/** */
namespace Mmu {

/** */
class Level1Descriptor {
	private:
		/** */
		Level1Desc::Reg		_desc;
	public:
		/** */
		Level1Descriptor() noexcept;
		/** */
		void			setLevel2Table(Level2Table& level2Table) noexcept;
		/** */
		Level2Table*	getLevel2Table() noexcept;
		/** */
		void			setAccessProtectionGroup(unsigned char apg) noexcept;
		/** */
		void			setGuarded() noexcept;
		/** */
		void			setNonGuarded() noexcept;
		/** */
		void			setPageSize4KByte() noexcept;
		/** */
		void			setPageSize16KByte() noexcept;
		/** */
		void			setPageSize512KByte() noexcept;
		/** */
		void			setPageSize8MByte() noexcept;
		/** */
		void			setCacheWritethrough() noexcept;
		/** */
		void			setCacheCopyback() noexcept;
		/** */
		void			setDescriptorValid() noexcept;
		/** */
		void			setDescriptorInvalid() noexcept;
		/** */
		bool			isValid() noexcept;
	};

/** */
class Level1Twam1Table {
	private:
		/** */
		Level1Descriptor	_table[nEntriesInLevel1Twam1Table];
	public:
		/** */
		Level1Twam1Table() noexcept;
		/** */
		Level1Descriptor* getDesc(unsigned entry) noexcept;
		/** */
		Level1Descriptor* getDesc(void* virtualAddress) noexcept;
	};

}
}
}

#endif
