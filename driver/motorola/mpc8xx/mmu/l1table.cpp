/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "l1table.h"

using namespace Oscl::Mot8xx::Mmu;

Level1Descriptor::Level1Descriptor() noexcept:
		_desc(Mot8xx::Mmu::Level1Desc::V::Value_Invalid)
		{
	}

void	Level1Descriptor::setLevel2Table(Level2Table& level2Table) noexcept{
	_desc	&= ~(Level1Desc::L2BA::FieldMask);
	_desc	|= (((unsigned long)&level2Table) & Level1Desc::L2BA::FieldMask);
	}

Level2Table*	Level1Descriptor::getLevel2Table() noexcept{
	return (Level2Table*)(_desc & Level1Desc::L2BA::FieldMask);
	}

void	Level1Descriptor::setAccessProtectionGroup(unsigned char apg) noexcept{
	_desc	&= ~(Level1Desc::APG::FieldMask);
	_desc	|= ((apg<<Level1Desc::APG::Lsb) & Level1Desc::APG::FieldMask);
	}

void	Level1Descriptor::setGuarded() noexcept{
	_desc	&= ~(Level1Desc::G::FieldMask);
	_desc	|= Level1Desc::G::ValueMask_Guarded;
	}

void	Level1Descriptor::setNonGuarded() noexcept{
	_desc	&= ~(Level1Desc::G::FieldMask);
	_desc	|= Level1Desc::G::ValueMask_NonGuarded;
	}

void	Level1Descriptor::setPageSize4KByte() noexcept{
	_desc	&= ~(Level1Desc::PS::FieldMask);
	_desc	|= Level1Desc::PS::ValueMask_Small;
	}

void	Level1Descriptor::setPageSize16KByte() noexcept{
	_desc	&= ~(Level1Desc::PS::FieldMask);
	_desc	|= Level1Desc::PS::ValueMask_Small;
	}

void	Level1Descriptor::setPageSize512KByte() noexcept{
	_desc	&= ~(Level1Desc::PS::FieldMask);
	_desc	|= Level1Desc::PS::ValueMask_512K;
	}

void	Level1Descriptor::setPageSize8MByte() noexcept{
	_desc	&= ~(Level1Desc::PS::FieldMask);
	_desc	|= Level1Desc::PS::ValueMask_8M;
	}

void	Level1Descriptor::setCacheWritethrough() noexcept{
	_desc	&= ~(Level1Desc::WT::FieldMask);
	_desc	|= Level1Desc::WT::ValueMask_Writethrough;
	}

void	Level1Descriptor::setCacheCopyback() noexcept{
	_desc	&= ~(Level1Desc::WT::FieldMask);
	_desc	|= Level1Desc::WT::ValueMask_Copyback;
	}

void	Level1Descriptor::setDescriptorValid() noexcept{
	_desc	&= ~(Level1Desc::V::FieldMask);
	_desc	|= Level1Desc::V::ValueMask_Valid;
	}

void	Level1Descriptor::setDescriptorInvalid() noexcept{
	_desc	&= ~(Level1Desc::V::FieldMask);
	_desc	|= Level1Desc::V::ValueMask_Invalid;
	}

bool	Level1Descriptor::isValid() noexcept{
	return	(_desc & Level1Desc::V::FieldMask)
			== Level1Desc::V::ValueMask_Valid;
	}

Level1Twam1Table::Level1Twam1Table() noexcept {
	}

Level1Descriptor* Level1Twam1Table::getDesc(unsigned entry) noexcept{
	if(entry > (nEntriesInLevel1Twam1Table-1)){
		return 0;
		}
	return &_table[entry];
	}

Level1Descriptor* Level1Twam1Table::getDesc(void* virtualAddr) noexcept{
	return getDesc(	(unsigned)(
					(	((unsigned long)virtualAddr)
							& Mot8xx::Mmu::Twam1::EA::Level1Index::FieldMask)
							>> Mot8xx::Mmu::Twam1::EA::Level1Index::Lsb
						)
					);
	}

