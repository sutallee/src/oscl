/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _drv_motorola_mpc8xx_mmu_sprh_
#define _drv_motorola_mpc8xx_mmu_sprh_
#include "oscl/hw/motorola/mpc8xx/mmu/mictr.h"
#include "oscl/hw/motorola/mpc8xx/mmu/mdctr.h"
#include "oscl/hw/motorola/mpc8xx/mmu/miepn.h"
#include "oscl/hw/motorola/mpc8xx/mmu/mdepn.h"
#include "oscl/hw/motorola/mpc8xx/mmu/mirpn.h"
#include "oscl/hw/motorola/mpc8xx/mmu/mdrpn.h"
#include "oscl/hw/motorola/mpc8xx/mmu/mitwc.h"
#include "oscl/hw/motorola/mpc8xx/mmu/mdtwc.h"
#include "oscl/hw/motorola/mpc8xx/mmu/mxap.h"
#include "oscl/hw/motorola/mpc8xx/mmu/mtwb.h"


/** */
namespace Oscl {
/** */
namespace Mot8xx {
/** */
namespace Mmu {

/** */
inline void writeMD_AP(MxAp::Reg reg) noexcept{
	// MD_AP
    asm volatile("mtspr 794,%0" : : "r" (reg));
    }

/** */
inline MxAp::Reg readMD_AP() noexcept{
	MxAp::Reg	reg;
    asm volatile("mfspr %0,794" : "=r" (reg));
	return reg;
    }

/** */
inline void writeMI_AP(MxAp::Reg reg) noexcept{
	// MI_AP
    asm volatile("mtspr 786,%0" : : "r" (reg));
    }

/** */
inline MxAp::Reg readMI_AP() noexcept{
	MxAp::Reg	reg;
    asm volatile("mfspr %0,786" : "=r" (reg));
	return reg;
    }

/** */
inline void writeM_TWB(void* table) noexcept{
	// M_TWB
    asm volatile("mtspr 796,%0" : : "r" (table));
    }

/** */
inline Mtwb::Reg readM_TWB() noexcept{
	Mtwb::Reg	table;
    asm volatile("mfspr %0,796" : "=r" (table));
	return table;
    }

/** */
inline void writeMI_CTR(MiCtr::Reg reg) noexcept{
    asm volatile("mtspr 784,%0" : : "r" (reg));
    }

/** */
inline MiCtr::Reg readMI_CTR() noexcept{
	MiCtr::Reg	reg;
    asm volatile("mfspr %0,784" : "=r" (reg));
	return reg;
    }

/** */
inline void writeMD_CTR(MdCtr::Reg reg) noexcept{
    asm volatile("mtspr 792,%0" : : "r" (reg));
    }

/** */
inline MdCtr::Reg readMD_CTR() noexcept{
	MiCtr::Reg	reg;
    asm volatile("mfspr %0,792" : "=r" (reg));
	return reg;
    }

/** */
inline void writeMI_EPN(MiEpn::Reg reg) noexcept{
    asm volatile("mtspr 787,%0" : : "r" (reg));
    }

/** */
inline MiEpn::Reg readMI_EPN() noexcept{
	MiEpn::Reg	reg;
    asm volatile("mfspr %0,787" : "=r" (reg));
	return reg;
    }

/** */
inline void writeMD_EPN(MdEpn::Reg reg) noexcept{
    asm volatile("mtspr 795,%0" : : "r" (reg));
    }

/** */
inline MdEpn::Reg readMD_EPN() noexcept{
	MdEpn::Reg	reg;
    asm volatile("mfspr %0,795" : "=r" (reg));
	return reg;
    }

/** */
inline void writeMI_TWC(MiTwc::Reg reg) noexcept{
    asm volatile("mtspr 789,%0" : : "r" (reg));
    }

/** */
inline MiTwc::Reg readMI_TWC() noexcept{
	MiTwc::Reg	reg;
    asm volatile("mfspr %0,789" : "=r" (reg));
	return reg;
    }

/** */
inline void writeMD_TWC(Write::MdTwc::Reg reg) noexcept{
    asm volatile("mtspr 797,%0" : : "r" (reg));
    }

/** */
inline Read::MdTwc::Reg readMD_TWC() noexcept{
	Read::MdTwc::Reg	reg;
    asm volatile("mfspr %0,797" : "=r" (reg));
	return reg;
    }

/** */
inline void writeMI_RPN(Write::MiRpn::Reg reg) noexcept{
    asm volatile("mtspr 790,%0" : : "r" (reg));
    }

/** */
inline Read::MiRpn::Reg readMI_RPN() noexcept{
	Read::MiRpn::Reg	reg;
    asm volatile("mfspr %0,790" : "=r" (reg));
	return reg;
    }

/** */
inline void writeMD_RPN(Write::MdRpn::Reg reg) noexcept{
    asm volatile("mtspr 798,%0" : : "r" (reg));
    }

/** */
inline Read::MdRpn::Reg readMD_RPN() noexcept{
	Read::MdRpn::Reg	reg;
    asm volatile("mfspr %0,798" : "=r" (reg));
	return reg;
    }

/** */
inline void writeM_CASID(uint32_t casid) noexcept{
    asm volatile("mtspr 793,%0" : : "r" (casid));
    }

/** */
inline uint32_t readM_CASID() noexcept{
	uint32_t	reg;
    asm volatile("mfspr %0,793" : "=r" (reg));
	return reg;
    }

}
}
}

#endif

