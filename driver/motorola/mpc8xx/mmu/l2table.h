/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _drv_motorola_mpc8xx_mmu_l2tableh_
#define _drv_motorola_mpc8xx_mmu_l2tableh_
#include "oscl/hw/motorola/mpc8xx/mmu/desc.h"

/** */
namespace Oscl {
/** */
namespace Mot8xx {
/** */
namespace Mmu {

/** */
class Level2Descriptor {
	private:
		/** */
		Level2Desc::Reg		_desc;
	public:
		/** */
		Level2Descriptor() noexcept;
		/** */
		void	setRealPage(void* realPage) noexcept;
		/** */
		unsigned long	getRealPage() noexcept;
		/** */
		void	setProtectionNoAccess() noexcept;
		/** */
		void	setProtectionSuperReadOnly() noexcept;
		/** */
		void	setProtectionSuperReadWriteOnly() noexcept;
		/** */
		void	setProtectionSuperReadWriteUserReadOnly() noexcept;
		/** */
		void	setProtectionAllReadWrite() noexcept;
		/** */
		void	setProtectionAllReadOnly() noexcept;
		/** */
		void	setChanged() noexcept;
		/** */
		void	setUnchanged() noexcept;
		/** */
		bool	isChanged() noexcept;
		/** */
		void	set4KPageSize() noexcept;
		/** */
		void	set16KPageSize() noexcept;
		/** */
		unsigned long	getPageSizeMask() noexcept;
		/** */
		void	setSharedPage() noexcept;
		/** */
		void	setUnsharedPage() noexcept;
		/** */
		bool	isShared() noexcept;
		/** */
		void	setCacheInhibit() noexcept;
		/** */
		void	setCacheAllow() noexcept;
		/** */
		bool	cacheInhibited() noexcept;
		/** */
		void	setPageValid() noexcept;
		/** */
		void	setPageInvalid() noexcept;
		/** */
		bool	isValid() noexcept;
	};

/** */
class Level2Table {
	private:
		/** */
		Level2Descriptor	_table[nEntriesInLevel2Table];
	public:
		/** */
		Level2Table() noexcept;
		/** */
		Level2Descriptor* getDesc(unsigned entry) noexcept;
		/** */
		Level2Descriptor* getDescTwam1(void* virtAddr) noexcept;
	};

}
}
}

#endif
