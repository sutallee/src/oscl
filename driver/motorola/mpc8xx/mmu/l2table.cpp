/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "l2table.h"

using namespace Oscl::Mot8xx::Mmu;

Level2Descriptor::Level2Descriptor() noexcept:
		_desc(		Level2Desc::Ppm0::Ppcs0::Other::ValueMask_Fixed
				|	Level2Desc::V::ValueMask_Invalid
				)
		{
	}

void	Level2Descriptor::setRealPage(void* realPage) noexcept{
	_desc	&= ~(Level2Desc::RPN::FieldMask);
	_desc	|= (((Level2Desc::Reg)realPage) & Level2Desc::RPN::FieldMask);
	}

unsigned long	Level2Descriptor::getRealPage() noexcept{
	unsigned long	rpn	=	_desc & Level2Desc::RPN::FieldMask;
	return rpn;
	}

void	Level2Descriptor::setProtectionNoAccess() noexcept{
	_desc	&= ~(Level2Desc::Ppm0::ExtEnc::PP::FieldMask);
	_desc	|= Level2Desc::Ppm0::ExtEnc::PP::ValueMask_NoAccess;
	}

void	Level2Descriptor::setProtectionSuperReadOnly() noexcept{
	_desc	&= ~(Level2Desc::Ppm0::ExtEnc::PP::FieldMask);
	_desc	|= Level2Desc::Ppm0::ExtEnc::PP::ValueMask_SuperReadOnly;
	}

void	Level2Descriptor::setProtectionSuperReadWriteOnly() noexcept{
	_desc	&= ~(Level2Desc::Ppm0::ExtEnc::PP::FieldMask);
	_desc	|= Level2Desc::Ppm0::ExtEnc::PP::ValueMask_SuperReadWriteOnly;
	}

void	Level2Descriptor::setProtectionSuperReadWriteUserReadOnly() noexcept{
	_desc	&= ~(Level2Desc::Ppm0::ExtEnc::PP::FieldMask);
	_desc	|= Level2Desc::Ppm0::ExtEnc::PP::ValueMask_SuperReadWriteUserReadOnly;
	}

void	Level2Descriptor::setProtectionAllReadWrite() noexcept{
	_desc	&= ~(Level2Desc::Ppm0::ExtEnc::PP::FieldMask);
	_desc	|= Level2Desc::Ppm0::ExtEnc::PP::ValueMask_AllReadWrite;
	}

void	Level2Descriptor::setProtectionAllReadOnly() noexcept{
	_desc	&= ~(Level2Desc::Ppm0::ExtEnc::PP::FieldMask);
	_desc	|= Level2Desc::Ppm0::ExtEnc::PP::ValueMask_AllReadOnly;
	}

void	Level2Descriptor::setChanged() noexcept{
	_desc	&= ~(Level2Desc::Ppm0::Change::FieldMask);
	_desc	|= Level2Desc::Ppm0::Change::ValueMask_Changed;
	}

void	Level2Descriptor::setUnchanged() noexcept{
	_desc	&= ~(Level2Desc::Ppm0::Change::FieldMask);
	_desc	|= Level2Desc::Ppm0::Change::ValueMask_Unchanged;
	}

bool	Level2Descriptor::isChanged() noexcept{
	return	(_desc & Level2Desc::Ppm0::Change::FieldMask)
			== Level2Desc::Ppm0::Change::ValueMask_Changed;
	}

void	Level2Descriptor::set4KPageSize() noexcept{
	_desc	&= ~(Level2Desc::Ppm0::SPS::FieldMask);
	_desc	|= Level2Desc::Ppm0::SPS::ValueMask_Size4Kbyte;
	}

void	Level2Descriptor::set16KPageSize() noexcept{
	_desc	&= ~(Level2Desc::Ppm0::SPS::FieldMask);
	_desc	|= Level2Desc::Ppm0::SPS::ValueMask_Size16Kbyte;
	}

unsigned long	Level2Descriptor::getPageSizeMask() noexcept{
	if(		(_desc & Level2Desc::Ppm0::SPS::FieldMask)
		==	Level2Desc::Ppm0::SPS::ValueMask_Size4Kbyte
		){
		return 0x00000FFF;
		}
	return 0x00003FFF;
	}

void	Level2Descriptor::setSharedPage() noexcept{
	_desc	&= ~(Level2Desc::SH::FieldMask);
	_desc	|= Level2Desc::SH::ValueMask_IgnoreASID;
	}

void	Level2Descriptor::setUnsharedPage() noexcept{
	_desc	&= ~(Level2Desc::SH::FieldMask);
	_desc	|= Level2Desc::SH::ValueMask_MatchASID;
	}

bool	Level2Descriptor::isShared() noexcept{
	return	(_desc & Level2Desc::SH::FieldMask)
			== Level2Desc::SH::ValueMask_IgnoreASID;
	}

void	Level2Descriptor::setCacheInhibit() noexcept{
	_desc	&= ~(Level2Desc::CI::FieldMask);
	_desc	|= Level2Desc::CI::ValueMask_CacheInhibited;
	}

void	Level2Descriptor::setCacheAllow() noexcept{
	_desc	&= ~(Level2Desc::CI::FieldMask);
	_desc	|= Level2Desc::CI::ValueMask_CacheEnabled;
	}

bool	Level2Descriptor::cacheInhibited() noexcept{
	return	(_desc & Level2Desc::CI::FieldMask)
			== Level2Desc::CI::ValueMask_CacheInhibited;
	}

void	Level2Descriptor::setPageValid() noexcept{
	_desc	&= ~(Level2Desc::V::FieldMask);
	_desc	|= Level2Desc::V::ValueMask_Valid;
	}

void	Level2Descriptor::setPageInvalid() noexcept{
	_desc	&= ~(Level2Desc::V::FieldMask);
	_desc	|= Level2Desc::V::ValueMask_Invalid;
	}

bool	Level2Descriptor::isValid() noexcept{
	return	(_desc & Level2Desc::V::FieldMask)
			== Level2Desc::V::ValueMask_Valid;
	}


Level2Table::Level2Table() noexcept{
	}

Level2Descriptor* Level2Table::getDesc(unsigned entry) noexcept{
	if(entry > (nEntriesInLevel2Table-1)){
		return 0;
		}
	return &_table[entry];
	}

Level2Descriptor* Level2Table::getDescTwam1(void* virtAddr) noexcept{
	return getDesc(	(unsigned)(
					(((unsigned long)virtAddr)
						& Mot8xx::Mmu::Twam1::EA::Level2Index::FieldMask
						) >> Mot8xx::Mmu::Twam1::EA::Level2Index::Lsb
						)
					);
	}

