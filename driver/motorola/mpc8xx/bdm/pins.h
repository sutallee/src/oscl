/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_motorolla_mpc8xx_sprh_
#define _oscl_drv_motorolla_mpc8xx_sprh_
#include "oscl/bits/inputapi.h"
#include "oscl/bits/outputapi.h"

/** */
namespace Oscl {
/** */
namespace Mot8xx {
/** */
namespace Bdm {

using namespace Oscl::Bits;

/**	Normally, the references provided by this
	class would be accessed via "accessor" functions. 
	The abstraction is provided by the type of the
	individual members rather than by the interface.
	Is this good?  bad? Using the public members,
	this class becomes concrete.
 */
class PinDriverApi {
	public:
		/** */
		InOutApi&	dsck;
		/** */
		InOutApi&	dsdi;
		/** */
		InputApi&	dsdo;
		/** */
		InOutApi&	sreset;
		/** */
		InOutApi&	hreset;
		/** */
		InputApi&	vfls0;
		/** */
		InputApi&	vfls1;
	public:
		/** */
		PinDriverApi(	InOutApi&	dsck,
						InOutApi&	dsdi,
						InputApi&	dsdo,
						InOutApi&	sreset,
						InOutApi&	hreset,
						InputApi&	vfls0,
						InputApi&	vfls1
						) noexcept;

	public:
		/** Development Serial Clock */
		virtual OutputApi&	getDSCK() noexcept=0;
		/** Development Serial Data In */
		virtual OutputApi&	getDSDI() noexcept=0;
		/** Development Serial Data Out */
		virtual InputApi&	getDSDO() noexcept=0;
		/** Soft Reset - Open Drain Driver */
		virtual OutputApi&	getSRESET() noexcept=0;
		/** Hard Reset - Open Drain Driver */
		virtual OutputApi&	getHRESET() noexcept=0;
		/** */
		virtual InputApi&	getSRESET() noexcept=0;
		/** */
		virtual InputApi&	getHRESET() noexcept=0;
		/** */
		virtual InputApi&	getVFLS0FRZ() noexcept=0;
		/** */
		virtual InputApi&	getVFSL1FRZ() noexcept=0;
	};

}
}
}

#endif
