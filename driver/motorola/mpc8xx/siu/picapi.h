/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_mot_mpc8xx_siu_picapih_
#define _oscl_drv_mot_mpc8xx_siu_picapih_
#include "oscl/interrupt/handler.h"
#include "oscl/interrupt/shared/api.h"
#include "irqdrvapi.h"
#include "lvldrvapi.h"

/** */
namespace Oscl {
/** */
namespace Mot8xx {
/** */
namespace Siu {

/** */
class PicApi : public Oscl::Interrupt::HandlerApi {
	public:
		/** */
		virtual Interrupt::Shared::SourceApi&
			getInternalLevel0SourceApi() noexcept=0;
		/** */
		virtual Interrupt::Shared::SourceApi&
			getInternalLevel1SourceApi() noexcept=0;
		/** */
		virtual Interrupt::Shared::SourceApi&
			getInternalLevel2SourceApi() noexcept=0;
		/** */
		virtual Interrupt::Shared::SourceApi&
			getInternalLevel3SourceApi() noexcept=0;
		/** */
		virtual Interrupt::Shared::SourceApi&
			getInternalLevel4SourceApi() noexcept=0;
		/** */
		virtual Interrupt::Shared::SourceApi&
			getInternalLevel5SourceApi() noexcept=0;
		/** */
		virtual Interrupt::Shared::SourceApi&
			getInternalLevel6SourceApi() noexcept=0;
		/** */
		virtual Interrupt::Shared::SourceApi&
			getInternalLevel7SourceApi() noexcept=0;

		/** */
		virtual IrqDriverApi&	getIrqDriverApi0() noexcept=0;
		/** */
		virtual IrqDriverApi&	getIrqDriverApi1() noexcept=0;
		/** */
		virtual IrqDriverApi&	getIrqDriverApi2() noexcept=0;
		/** */
		virtual IrqDriverApi&	getIrqDriverApi3() noexcept=0;
		/** */
		virtual IrqDriverApi&	getIrqDriverApi4() noexcept=0;
		/** */
		virtual IrqDriverApi&	getIrqDriverApi5() noexcept=0;
		/** */
		virtual IrqDriverApi&	getIrqDriverApi6() noexcept=0;
		/** */
		virtual IrqDriverApi&	getIrqDriverApi7() noexcept=0;
		/** */
		virtual LevelDriverApi&	getLevelDriverApi0() noexcept=0;
		/** */
		virtual LevelDriverApi&	getLevelDriverApi1() noexcept=0;
		/** */
		virtual LevelDriverApi&	getLevelDriverApi2() noexcept=0;
		/** */
		virtual LevelDriverApi&	getLevelDriverApi3() noexcept=0;
		/** */
		virtual LevelDriverApi&	getLevelDriverApi4() noexcept=0;
		/** */
		virtual LevelDriverApi&	getLevelDriverApi5() noexcept=0;
		/** */
		virtual LevelDriverApi&	getLevelDriverApi6() noexcept=0;
		/** */
		virtual LevelDriverApi&	getLevelDriverApi7() noexcept=0;
		/** */
		virtual void	reserveIrq0(	Oscl::Interrupt::
										HandlerApi&			handler
										) noexcept=0;
		/** */
		virtual void	releaseIrq0() noexcept=0;
		/** */
		virtual void	reserveIrq1(	Oscl::Interrupt::
										HandlerApi&			handler
										) noexcept=0;
		/** */
		virtual void	releaseIrq1() noexcept=0;
		/** */
		virtual void	reserveIrq2(	Oscl::Interrupt::
										HandlerApi&			handler
										) noexcept=0;
		/** */
		virtual void	releaseIrq2() noexcept=0;
		/** */
		virtual void	reserveIrq3(	Oscl::Interrupt::
										HandlerApi&			handler
										) noexcept=0;
		/** */
		virtual void	releaseIrq3() noexcept=0;
		/** */
		virtual void	reserveIrq4(	Oscl::Interrupt::
										HandlerApi&			handler
										) noexcept=0;
		/** */
		virtual void	releaseIrq4() noexcept=0;
		/** */
		virtual void	reserveIrq5(	Oscl::Interrupt::
										HandlerApi&			handler
										) noexcept=0;
		/** */
		virtual void	releaseIrq5() noexcept=0;
		/** */
		virtual void	reserveIrq6(	Oscl::Interrupt::
										HandlerApi&			handler
										) noexcept=0;
		/** */
		virtual void	releaseIrq6() noexcept=0;
		/** */
		virtual void	reserveIrq7(	Oscl::Interrupt::
										HandlerApi&			handler
										) noexcept=0;
		/** */
		virtual void	releaseIrq7() noexcept=0;
	};

}
}
}


#endif
