/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_mot_mpc8xx_siu_pich_
#define _oscl_drv_mot_mpc8xx_siu_pich_
#include "oscl/interrupt/shared/shared.h"
#include "oscl/interrupt/edge.h"
#include "irqedge.h"
#include "irqlevel.h"
#include "vector.h"

/** */
namespace Oscl {
/** */
namespace Mot8xx {
/** */
namespace Siu {

/** */
class tSource {
	private:
		/** */
		Oscl::Interrupt::Shared::Source	_source;
		/** */
		SIVEC::Reg						_vector;
	public:
		/** */
		IntSource(PriorityValue priority) noexcept:
				_priority(prority)
				{
			}
		/** */
		void		attach(Handler& handler) noexcept;
		/** */
		void		detach(Handler& handler) noexcept;
		/** */
		SIVEC::Reg	vector() noexcept;
	};

/** */
class SourceApi :	public Oscl::Interrupt::Shared::SourceApi,
					public Oscl::Interrupt::HandlerApi {
	};

/** */
class LevelSource : public SourceApi {
	};

/** This class uses delegation to read the PEND register
	before servicing the interrupt and then bits read from
	the PEND register after those interrupts are serviced.
 */
class EdgeSource : public SourceApi {
	private:
		/** */
		Oscl::Interrupt::Shared::Source	_source;
		/** */
		write to clear register pointer.
		value to write to the register after the interrupt is handled.
		Oscl::Interrupt::EdgeApi&	_edgeApi;
	public:
		/** */
		LevelSource(Oscl::Interrupt::EdgeApi& edgeApi) noexcept:
				_edgeApi(edgeApi)
				{
			}
		/** */
		void	attach(Handler& handler) noexcept{
			_source.attach(handler);
			}
		/** */
		void	detach(Handler& handler) noexcept;
	public:
		/** */
		void	interrupt() noexcept{
			_source.interrupt();
			_edgeApi.ack();
			}
	};
}
}
}


#endif
