/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_motorola_mpc8xxx_siu_edgedrvh_
#define _oscl_drv_motorola_mpc8xxx_siu_edgedrvh_
#include "oscl/hw/motorola/mpc8xx/siureg.h"
#include "oscl/interrupt/edge.h"
#include "oscl/driver/motorola/mpc8xx/siu/irqnum.h"

/** */
namespace Oscl {
/** */
namespace Mot8xx {
/** */
namespace Siu {
/** */
namespace Irq {

/**/
template <IrqEnum IrqNumber>
class EdgeDriver : Oscl::Interrupt::EdgeApi {
	private:
		/**/
		volatile Oscl::Mot8xx::Siu::SIPEND::Reg&	_pend;
		/**/
		volatile Oscl::Mot8xx::Siu::SIMASK::Reg&	_mask;
	public:
		/**/
		EdgeDriver(	volatile Oscl::Mot8xx::Siu::SIPEND::Reg&	pend,
					volatile Oscl::Mot8xx::Siu::SIMASK::Reg&	mask,
					volatile Oscl::Mot8xx::Siu::SIEL::Reg&		el
					) noexcept;
	public:
		/**/
		void	ack() noexcept;
	public:
		/**/
		void	enable() noexcept;
		/**/
		void	disable() noexcept;
	};

template <IrqEnum IrqNumber>
EdgeDriver<IrqNumber>::
EdgeDriver(	volatile Oscl::Mot8xx::Siu::SIPEND::Reg&	pend,
			volatile Oscl::Mot8xx::Siu::SIMASK::Reg&	mask,
			volatile Oscl::Mot8xx::Siu::SIEL::Reg&		el
			) noexcept:
		_pend(pend),
		_mask(mask)
		{
	el	&= ~(((Oscl::Mot8xx::Siu::SIEL::Reg)0x80000000)>>(IrqNumber*2));
	}

template <IrqEnum IrqNumber>
void	EdgeDriver<IrqNumber>::ack() noexcept{
	_pend	|= (((Oscl::Mot8xx::Siu::SIPEND::Reg)0x80000000)>>(IrqNumber*2));
	}

template <IrqEnum IrqNumber>
void	EdgeDriver<IrqNumber>::enable() noexcept{
	_mask	|= (((Oscl::Mot8xx::Siu::SIMASK::Reg)0x80000000)>>(IrqNumber*2));
	}

template <IrqEnum IrqNumber>
void	EdgeDriver<IrqNumber>::disable() noexcept{
	_mask	&= ~(((Oscl::Mot8xx::Siu::SIMASK::Reg)0x80000000)>>(IrqNumber*2));
	}

/** */
typedef EdgeDriver<Irq0>	EdgeDriver0;
/** */
typedef EdgeDriver<Irq1>	EdgeDriver1;
/** */
typedef EdgeDriver<Irq2>	EdgeDriver2;
/** */
typedef EdgeDriver<Irq3>	EdgeDriver3;
/** */
typedef EdgeDriver<Irq4>	EdgeDriver4;
/** */
typedef EdgeDriver<Irq5>	EdgeDriver5;
/** */
typedef EdgeDriver<Irq6>	EdgeDriver6;
/** */
typedef EdgeDriver<Irq7>	EdgeDriver7;

}
}
}
}


#endif
