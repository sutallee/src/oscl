/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_motorola_mpc8xxx_siu_irqlevelh_
#define _oscl_drv_motorola_mpc8xxx_siu_irqlevelh_
#include "oscl/hw/motorola/mpc8xx/siureg.h"
#include "oscl/interrupt/mask.h"
#include "oscl/driver/motorola/mpc8xx/siu/irqnum.h"

/** */
namespace Oscl {
/** */
namespace Mot8xx {
/** */
namespace Siu {
/** */
namespace Irq {

/** */
template <Oscl::Mot8xx::Siu::IrqEnum  IrqNumber>
class LevelDriver : public Oscl::Interrupt::MaskApi {
	private:
		/** */
		volatile Oscl::Mot8xx::Siu::SIMASK::Reg&	_mask;
	public:
		/** */
		LevelDriver(	volatile Oscl::Mot8xx::Siu::SIMASK::Reg&	mask,
						volatile Oscl::Mot8xx::Siu::SIEL::Reg&		el
						) noexcept;
	public:
		/** */
		void	enable() noexcept;
		/** */
		void	disable() noexcept;
	};

template <Oscl::Mot8xx::Siu::IrqEnum  IrqNumber>
LevelDriver<IrqNumber>::
LevelDriver(	volatile Oscl::Mot8xx::Siu::SIMASK::Reg&	mask,
				volatile Oscl::Mot8xx::Siu::SIEL::Reg&		el
				) noexcept:
		_mask(mask)
		{
	el	|= (((Oscl::Mot8xx::Siu::SIEL::Reg)0x80000000)>>(IrqNumber*2));
	}

template <Oscl::Mot8xx::Siu::IrqEnum  IrqNumber>
void	LevelDriver<IrqNumber>::enable() noexcept{
	_mask	|= (((Oscl::Mot8xx::Siu::SIMASK::Reg)0x80000000)>>(IrqNumber*2));
	}

template <Oscl::Mot8xx::Siu::IrqEnum  IrqNumber>
void	LevelDriver<IrqNumber>::disable() noexcept{
	_mask	&= ~(((Oscl::Mot8xx::Siu::SIMASK::Reg)0x80000000)>>(IrqNumber*2));
	}

/** */
typedef LevelDriver<Irq0>	LevelDriver0;
/** */
typedef LevelDriver<Irq1>	LevelDriver1;
/** */
typedef LevelDriver<Irq2>	LevelDriver2;
/** */
typedef LevelDriver<Irq3>	LevelDriver3;
/** */
typedef LevelDriver<Irq4>	LevelDriver4;
/** */
typedef LevelDriver<Irq5>	LevelDriver5;
/** */
typedef LevelDriver<Irq6>	LevelDriver6;
/** */
typedef LevelDriver<Irq7>	LevelDriver7;

}
}
}
}

#endif
