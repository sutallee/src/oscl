/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_mot_mpc8xx_siu_internal_sourceh_
#define _oscl_drv_mot_mpc8xx_siu_internal_sourceh_
#include "oscl/interrupt/shared/source.h"
#include "oscl/interrupt/handler.h"
#include "oscl/interrupt/mask.h"
#include "oscl/driver/motorola/mpc8xx/siu/irqapi.h"
#include "maskdrv.h"

/** */
namespace Oscl {
/** */
namespace Mot8xx {
/** */
namespace Siu {
/** */
namespace Irq {

/** */
template <IrqEnum IrqNumber>
class EdgeSource :	public Oscl::Mot8xx::Siu::IrqApi<IrqNumber>,
				public Oscl::Interrupt::Shared::SourceApi,
				private Oscl::Interrupt::Shared::ListStateMonitorApi
				{
	private:
		/** */
		Oscl::Interrupt::Shared::EdgeSource	_source;
		/** */
		EdgeDriver<IrqNumber>				_edgeDrv;
	public:
		/** */
		EdgeSource(	volatile Oscl::Mot8xx::Siu::SIPEND::Reg&	pend,
					volatile Oscl::Mot8xx::Siu::SIMASK::Reg&	mask,
					volatile Oscl::Mot8xx::Siu::SIEL::Reg&		el
					) noexcept:
				_source(*this),
				_edgeDrv(pend,mask,el)
				{
			}
		/** */
		void	listIsNowEmpty() noexcept{
			_edgeDrv.disable();
			}
		/** */
		void	listIsNowNotEmpty() noexcept{
			_edgeDrv.enable();
			}
		/** */
		void		attach(Oscl::Interrupt::Shared::Handler& handler) noexcept{
			_source.attach(handler);
			}
		/** */
		void		detach(Oscl::Interrupt::Shared::Handler& handler) noexcept{
			_source.detach(handler);
			}
		/** */
		void		interrupt() noexcept{
			_edgeDrv.ack();
			if(!_source.interrupt()){
				while(true);	// Interrupts should always be
								// caught by a handler.
				}
			}
	};

}
}
}
}

#endif
