/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <new>
#include "pic.h"
#include "oscl/memory/block.h"
#include "oscl/error/fatal.h"

/** */
namespace Oscl {
/** */
namespace Mot8xx {
/** */
namespace Siu {
/** */
class NullHandler : public Oscl::Interrupt::HandlerApi {
	private:
		/** */
		NullHandler() noexcept;
	private:	// Oscl::Interrupt::HandlerApi
		/** */
		void	interrupt() noexcept;
	public:
		/** Singleton operation */
		static Oscl::Interrupt::HandlerApi*	handler;
		/** Singleton operation */
		static Oscl::Interrupt::HandlerApi&	getNull() noexcept;
	};
}
}
}

using namespace Oscl::Mot8xx::Siu;

Oscl::Interrupt::HandlerApi*	NullHandler::handler;

static Oscl::Memory::
AlignedBlock<sizeof(Oscl::Mot8xx::Siu::NullHandler)>	nullHandlerMem;

Oscl::Interrupt::HandlerApi&	NullHandler::getNull() noexcept{
	if(!NullHandler::handler){
		NullHandler::handler	= new(&nullHandlerMem)
									NullHandler();
		}
	return *handler;
	}

NullHandler::NullHandler() noexcept{
	}

void NullHandler::interrupt() noexcept{
	Oscl::ErrorFatal::logAndExit("Pic: unreserved vector used.\n");
	}

Pic::Pic(	const volatile SIVEC::Reg&		vect,
			volatile SIMASK::Reg&			mask,
			volatile SIPEND::Reg&			pend,
			volatile SIEL::Reg&				el,
			Oscl::Interrupt::HandlerApi&	irq0,
			Oscl::Interrupt::HandlerApi&	irq1,
			Oscl::Interrupt::HandlerApi&	irq2,
			Oscl::Interrupt::HandlerApi&	irq3,
			Oscl::Interrupt::HandlerApi&	irq4,
			Oscl::Interrupt::HandlerApi&	irq5,
			Oscl::Interrupt::HandlerApi&	irq6,
			Oscl::Interrupt::HandlerApi&	irq7
			) noexcept:
		_vect(vect),
		_level0(mask),
		_level1(mask),
		_level2(mask),
		_level3(mask),
		_level4(mask),
		_level5(mask),
		_level6(mask),
		_level7(mask),
		_irq0(mask,pend,el),
		_irq1(mask,pend,el),
		_irq2(mask,pend,el),
		_irq3(mask,pend,el),
		_irq4(mask,pend,el),
		_irq5(mask,pend,el),
		_irq6(mask,pend,el),
		_irq7(mask,pend,el)
		{
	_vectorTable.handler[0]		= &irq0;
	_vectorTable.handler[1]		= &_level0;
	_vectorTable.handler[2]		= &irq1;
	_vectorTable.handler[3]		= &_level1;
	_vectorTable.handler[4]		= &irq2;
	_vectorTable.handler[5]		= &_level2;
	_vectorTable.handler[6]		= &irq3;
	_vectorTable.handler[7]		= &_level3;
	_vectorTable.handler[8]		= &irq4;
	_vectorTable.handler[9]		= &_level4;
	_vectorTable.handler[10]	= &irq5;
	_vectorTable.handler[11]	= &_level5;
	_vectorTable.handler[12]	= &irq6;
	_vectorTable.handler[13]	= &_level6;
	_vectorTable.handler[14]	= &irq7;
	_vectorTable.handler[15]	= &_level7;
	}

Pic::Pic(	const volatile SIVEC::Reg&		vect,
			volatile SIMASK::Reg&			mask,
			volatile SIPEND::Reg&			pend,
			volatile SIEL::Reg&				el
			) noexcept:
		_vect(vect),
		_level0(mask),
		_level1(mask),
		_level2(mask),
		_level3(mask),
		_level4(mask),
		_level5(mask),
		_level6(mask),
		_level7(mask),
		_irq0(mask,pend,el),
		_irq1(mask,pend,el),
		_irq2(mask,pend,el),
		_irq3(mask,pend,el),
		_irq4(mask,pend,el),
		_irq5(mask,pend,el),
		_irq6(mask,pend,el),
		_irq7(mask,pend,el)
		{
	_vectorTable.handler[0]		= &NullHandler::getNull();
	_vectorTable.handler[1]		= &_level0;
	_vectorTable.handler[2]		= &NullHandler::getNull();
	_vectorTable.handler[3]		= &_level1;
	_vectorTable.handler[4]		= &NullHandler::getNull();
	_vectorTable.handler[5]		= &_level2;
	_vectorTable.handler[6]		= &NullHandler::getNull();
	_vectorTable.handler[7]		= &_level3;
	_vectorTable.handler[8]		= &NullHandler::getNull();
	_vectorTable.handler[9]		= &_level4;
	_vectorTable.handler[10]	= &NullHandler::getNull();
	_vectorTable.handler[11]	= &_level5;
	_vectorTable.handler[12]	= &NullHandler::getNull();
	_vectorTable.handler[13]	= &_level6;
	_vectorTable.handler[14]	= &NullHandler::getNull();
	_vectorTable.handler[15]	= &_level7;
	}

void	Pic::interrupt() noexcept{
	_vectorTable.handler[_vect>>2]->interrupt();
	}

Oscl::Interrupt::Shared::SourceApi&
Pic::getInternalLevel0SourceApi() noexcept{
	return _level0;
	}

Oscl::Interrupt::Shared::SourceApi&
Pic::getInternalLevel1SourceApi() noexcept{
	return _level1;
	}

Oscl::Interrupt::Shared::SourceApi&
Pic::getInternalLevel2SourceApi() noexcept{
	return _level2;
	}

Oscl::Interrupt::Shared::SourceApi&
Pic::getInternalLevel3SourceApi() noexcept{
	return _level3;
	}

Oscl::Interrupt::Shared::SourceApi&
Pic::getInternalLevel4SourceApi() noexcept{
	return _level4;
	}

Oscl::Interrupt::Shared::SourceApi&
Pic::getInternalLevel5SourceApi() noexcept{
	return _level5;
	}

Oscl::Interrupt::Shared::SourceApi&
Pic::getInternalLevel6SourceApi() noexcept{
	return _level6;
	}

Oscl::Interrupt::Shared::SourceApi&
Pic::getInternalLevel7SourceApi() noexcept{
	return _level7;
	}

Oscl::Mot8xx::Siu::IrqDriverApi&
Pic::getIrqDriverApi0() noexcept{
	return _irq0;
	}

Oscl::Mot8xx::Siu::IrqDriverApi&
Pic::getIrqDriverApi1() noexcept{
	return _irq1;
	}

Oscl::Mot8xx::Siu::IrqDriverApi&
Pic::getIrqDriverApi2() noexcept{
	return _irq2;
	}

Oscl::Mot8xx::Siu::IrqDriverApi&
Pic::getIrqDriverApi3() noexcept{
	return _irq3;
	}

Oscl::Mot8xx::Siu::IrqDriverApi&
Pic::getIrqDriverApi4() noexcept{
	return _irq4;
	}

Oscl::Mot8xx::Siu::IrqDriverApi&
Pic::getIrqDriverApi5() noexcept{
	return _irq5;
	}

Oscl::Mot8xx::Siu::IrqDriverApi&
Pic::getIrqDriverApi6() noexcept{
	return _irq6;
	}

Oscl::Mot8xx::Siu::IrqDriverApi&
Pic::getIrqDriverApi7() noexcept{
	return _irq7;
	}

Oscl::Mot8xx::Siu::LevelDriverApi&
Pic::getLevelDriverApi0() noexcept{
	return _level0.getLevelDriverApi();
	}

Oscl::Mot8xx::Siu::LevelDriverApi&
Pic::getLevelDriverApi1() noexcept{
	return _level1.getLevelDriverApi();
	}

Oscl::Mot8xx::Siu::LevelDriverApi&
Pic::getLevelDriverApi2() noexcept{
	return _level2.getLevelDriverApi();
	}

Oscl::Mot8xx::Siu::LevelDriverApi&
Pic::getLevelDriverApi3() noexcept{
	return _level3.getLevelDriverApi();
	}

Oscl::Mot8xx::Siu::LevelDriverApi&
Pic::getLevelDriverApi4() noexcept{
	return _level4.getLevelDriverApi();
	}

Oscl::Mot8xx::Siu::LevelDriverApi&
Pic::getLevelDriverApi5() noexcept{
	return _level5.getLevelDriverApi();
	}

Oscl::Mot8xx::Siu::LevelDriverApi&
Pic::getLevelDriverApi6() noexcept{
	return _level6.getLevelDriverApi();
	}

Oscl::Mot8xx::Siu::LevelDriverApi&
Pic::getLevelDriverApi7() noexcept{
	return _level7.getLevelDriverApi();
	}


void	Pic::reserveIrq0(	Oscl::Interrupt::
							HandlerApi&			handler
							) noexcept{
	if(_vectorTable.handler[0<<1] != &NullHandler::getNull()){
		Oscl::ErrorFatal::logAndExit("Pic: vector already reserved.\n");
		}
	_vectorTable.handler[0<<1]	= &handler;
	}

void	Pic::releaseIrq0() noexcept{
	_vectorTable.handler[0<<1]	= &NullHandler::getNull();
	}

void	Pic::reserveIrq1(	Oscl::Interrupt::
							HandlerApi&			handler
							) noexcept{
	if(_vectorTable.handler[1<<1] != &NullHandler::getNull()){
		Oscl::ErrorFatal::logAndExit("Pic: vector already reserved.\n");
		}
	_vectorTable.handler[1<<1]	= &handler;
	}

void	Pic::releaseIrq1() noexcept{
	_vectorTable.handler[1<<1]	= &NullHandler::getNull();
	}

void	Pic::reserveIrq2(	Oscl::Interrupt::
							HandlerApi&			handler
							) noexcept{
	if(_vectorTable.handler[2<<1] != &NullHandler::getNull()){
		Oscl::ErrorFatal::logAndExit("Pic: vector already reserved.\n");
		}
	_vectorTable.handler[2<<1]	= &handler;
	}

void	Pic::releaseIrq2() noexcept{
	_vectorTable.handler[2<<1]	= &NullHandler::getNull();
	}

void	Pic::reserveIrq3(	Oscl::Interrupt::
							HandlerApi&			handler
							) noexcept{
	if(_vectorTable.handler[3<<1] != &NullHandler::getNull()){
		Oscl::ErrorFatal::logAndExit("Pic: vector already reserved.\n");
		}
	_vectorTable.handler[3<<1]	= &handler;
	}

void	Pic::releaseIrq3() noexcept{
	_vectorTable.handler[3<<1]	= &NullHandler::getNull();
	}

void	Pic::reserveIrq4(	Oscl::Interrupt::
							HandlerApi&			handler
							) noexcept{
	if(_vectorTable.handler[4<<1] != &NullHandler::getNull()){
		Oscl::ErrorFatal::logAndExit("Pic: vector already reserved.\n");
		}
	_vectorTable.handler[4<<1]	= &handler;
	}

void	Pic::releaseIrq4() noexcept{
	_vectorTable.handler[4<<1]	= &NullHandler::getNull();
	}

void	Pic::reserveIrq5(	Oscl::Interrupt::
							HandlerApi&			handler
							) noexcept{
	if(_vectorTable.handler[5<<1] != &NullHandler::getNull()){
		Oscl::ErrorFatal::logAndExit("Pic: vector already reserved.\n");
		}
	_vectorTable.handler[5<<1]	= &handler;
	}

void	Pic::releaseIrq5() noexcept{
	_vectorTable.handler[5<<1]	= &NullHandler::getNull();
	}

void	Pic::reserveIrq6(	Oscl::Interrupt::
							HandlerApi&			handler
							) noexcept{
	if(_vectorTable.handler[6<<1] != &NullHandler::getNull()){
		Oscl::ErrorFatal::logAndExit("Pic: vector already reserved.\n");
		}
	_vectorTable.handler[6<<1]	= &handler;
	}

void	Pic::releaseIrq6() noexcept{
	_vectorTable.handler[6<<1]	= &NullHandler::getNull();
	}

void	Pic::reserveIrq7(	Oscl::Interrupt::
							HandlerApi&			handler
							) noexcept{
	if(_vectorTable.handler[7<<1] != &NullHandler::getNull()){
		Oscl::ErrorFatal::logAndExit("Pic: vector already reserved.\n");
		}
	_vectorTable.handler[7<<1]	= &handler;
	}

void	Pic::releaseIrq7() noexcept{
	_vectorTable.handler[7<<1]	= &NullHandler::getNull();
	}

