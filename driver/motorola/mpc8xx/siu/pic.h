/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_mot_mpc8xx_siu_pich_
#define _oscl_drv_mot_mpc8xx_siu_pich_
#include "picapi.h"
#include "oscl/hw/motorola/mpc8xx/siureg.h"
#include "oscl/interrupt/shared/source.h"
#include "oscl/driver/motorola/mpc8xx/siu/internal/source.h"
#include "irqdrv.h"
#include "lvldrv.h"

/** */
namespace Oscl {
/** */
namespace Mot8xx {
/** */
namespace Siu {

/** */
class Pic :	public PicApi
			{
	private:
		/** */
		const volatile Oscl::Mot8xx::Siu::SIVEC::Reg&	_vect;
		/** */
		union {
			/** */
			Oscl::Interrupt::HandlerApi*	handler[32];
			} _vectorTable;
		/** */
		Internal::Source<PriorityLevel0>		_level0;
		/** */
		Internal::Source<PriorityLevel1>		_level1;
		/** */
		Internal::Source<PriorityLevel2>		_level2;
		/** */
		Internal::Source<PriorityLevel3>		_level3;
		/** */
		Internal::Source<PriorityLevel4>		_level4;
		/** */
		Internal::Source<PriorityLevel5>		_level5;
		/** */
		Internal::Source<PriorityLevel6>		_level6;
		/** */
		Internal::Source<PriorityLevel7>		_level7;
		/** */
		IrqDriver<Irq0>							_irq0;
		/** */
		IrqDriver<Irq1>							_irq1;
		/** */
		IrqDriver<Irq2>							_irq2;
		/** */
		IrqDriver<Irq3>							_irq3;
		/** */
		IrqDriver<Irq4>							_irq4;
		/** */
		IrqDriver<Irq5>							_irq5;
		/** */
		IrqDriver<Irq6>							_irq6;
		/** */
		IrqDriver<Irq7>							_irq7;

	public:
		/** */
		Pic(	const volatile SIVEC::Reg&		vect,
				volatile SIMASK::Reg&			mask,
				volatile SIPEND::Reg&			pend,
				volatile SIEL::Reg&				el,
				Oscl::Interrupt::HandlerApi&	irq0,
				Oscl::Interrupt::HandlerApi&	irq1,
				Oscl::Interrupt::HandlerApi&	irq2,
				Oscl::Interrupt::HandlerApi&	irq3,
				Oscl::Interrupt::HandlerApi&	irq4,
				Oscl::Interrupt::HandlerApi&	irq5,
				Oscl::Interrupt::HandlerApi&	irq6,
				Oscl::Interrupt::HandlerApi&	irq7
				) noexcept;
		/** */
		Pic(	const volatile SIVEC::Reg&		vect,
				volatile SIMASK::Reg&			mask,
				volatile SIPEND::Reg&			pend,
				volatile SIEL::Reg&				el
				) noexcept;

		/** */
		void	interrupt() noexcept;

		/** */
		Interrupt::Shared::SourceApi&	getInternalLevel0SourceApi() noexcept;
		/** */
		Interrupt::Shared::SourceApi&	getInternalLevel1SourceApi() noexcept;
		/** */
		Interrupt::Shared::SourceApi&	getInternalLevel2SourceApi() noexcept;
		/** */
		Interrupt::Shared::SourceApi&	getInternalLevel3SourceApi() noexcept;
		/** */
		Interrupt::Shared::SourceApi&	getInternalLevel4SourceApi() noexcept;
		/** */
		Interrupt::Shared::SourceApi&	getInternalLevel5SourceApi() noexcept;
		/** */
		Interrupt::Shared::SourceApi&	getInternalLevel6SourceApi() noexcept;
		/** */
		Interrupt::Shared::SourceApi&	getInternalLevel7SourceApi() noexcept;

		/** */
		IrqDriverApi&	getIrqDriverApi0() noexcept;
		/** */
		IrqDriverApi&	getIrqDriverApi1() noexcept;
		/** */
		IrqDriverApi&	getIrqDriverApi2() noexcept;
		/** */
		IrqDriverApi&	getIrqDriverApi3() noexcept;
		/** */
		IrqDriverApi&	getIrqDriverApi4() noexcept;
		/** */
		IrqDriverApi&	getIrqDriverApi5() noexcept;
		/** */
		IrqDriverApi&	getIrqDriverApi6() noexcept;
		/** */
		IrqDriverApi&	getIrqDriverApi7() noexcept;
		/** */
		LevelDriverApi&	getLevelDriverApi0() noexcept;
		/** */
		LevelDriverApi&	getLevelDriverApi1() noexcept;
		/** */
		LevelDriverApi&	getLevelDriverApi2() noexcept;
		/** */
		LevelDriverApi&	getLevelDriverApi3() noexcept;
		/** */
		LevelDriverApi&	getLevelDriverApi4() noexcept;
		/** */
		LevelDriverApi&	getLevelDriverApi5() noexcept;
		/** */
		LevelDriverApi&	getLevelDriverApi6() noexcept;
		/** */
		LevelDriverApi&	getLevelDriverApi7() noexcept;
		/** */
		void	reserveIrq0(	Oscl::Interrupt::
								HandlerApi&			handler
								) noexcept;
		/** */
		void	releaseIrq0() noexcept;
		/** */
		void	reserveIrq1(	Oscl::Interrupt::
								HandlerApi&			handler
								) noexcept;
		/** */
		void	releaseIrq1() noexcept;
		/** */
		void	reserveIrq2(	Oscl::Interrupt::
								HandlerApi&			handler
								) noexcept;
		/** */
		void	releaseIrq2() noexcept;
		/** */
		void	reserveIrq3(	Oscl::Interrupt::
								HandlerApi&			handler
								) noexcept;
		/** */
		void	releaseIrq3() noexcept;
		/** */
		void	reserveIrq4(	Oscl::Interrupt::
								HandlerApi&			handler
								) noexcept;
		/** */
		void	releaseIrq4() noexcept;
		/** */
		void	reserveIrq5(	Oscl::Interrupt::
								HandlerApi&			handler
								) noexcept;
		/** */
		void	releaseIrq5() noexcept;
		/** */
		void	reserveIrq6(	Oscl::Interrupt::
								HandlerApi&			handler
								) noexcept;
		/** */
		void	releaseIrq6() noexcept;
		/** */
		void	reserveIrq7(	Oscl::Interrupt::
								HandlerApi&			handler
								) noexcept;
		/** */
		void	releaseIrq7() noexcept;
	};

}
}
}


#endif
