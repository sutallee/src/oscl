/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_motorola_mpc8xxx_siu_priorityh_
#define _oscl_drv_motorola_mpc8xxx_siu_priorityh_

/** */
namespace Oscl {
/** */
namespace Mot8xx {
/** */
namespace Siu {

/** */
enum PriorityValue {
	/** */
	PriorityLevel0=0,
	/** */
	PriorityLevel1,
	/** */
	PriorityLevel2,
	/** */
	PriorityLevel3,
	/** */
	PriorityLevel4,
	/** */
	PriorityLevel5,
	/** */
	PriorityLevel6,
	/** */
	PriorityLevel7
	};

/** */
typedef unsigned char	PriorityMask;

/** */
class InterruptPriorityApi {
	public:
		/** Shut-up GCC. */
		virtual ~InterruptPriorityApi() {}
		/** */
		virtual PriorityValue	value() const noexcept=0;
		/** */
		virtual PriorityMask	mask() const noexcept=0;
	};

/** */
template <PriorityValue Priority>
class InterruptPriority : public InterruptPriorityApi{
	public:
		/** */
		PriorityValue	value() const noexcept;
		/** */
		PriorityMask	mask() const noexcept;
	};

template <PriorityValue Priority>
PriorityValue	InterruptPriority<Priority>::value() const noexcept{
	return Priority;
	}

template <PriorityValue Priority>
PriorityMask	InterruptPriority<Priority>::mask() const noexcept{
	return (0x80>>Priority);
	}

/** */
typedef InterruptPriority<PriorityLevel0>	InterruptPriority0;
/** */
typedef InterruptPriority<PriorityLevel1>	InterruptPriority1;
/** */
typedef InterruptPriority<PriorityLevel2>	InterruptPriority2;
/** */
typedef InterruptPriority<PriorityLevel3>	InterruptPriority3;
/** */
typedef InterruptPriority<PriorityLevel4>	InterruptPriority4;
/** */
typedef InterruptPriority<PriorityLevel5>	InterruptPriority5;
/** */
typedef InterruptPriority<PriorityLevel6>	InterruptPriority6;
/** */
typedef InterruptPriority<PriorityLevel7>	InterruptPriority7;

}
}
}

#endif
