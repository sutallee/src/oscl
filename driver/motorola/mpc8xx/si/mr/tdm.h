/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_mot_mpc8xx_si_mr_tdmh_
#define _oscl_drv_mot_mpc8xx_si_mr_tdmh_
#include "oscl/bits/fieldapi.h"
#include "oscl/hw/motorola/mpc8xx/sireg.h"
#include "sdm.h"
#include "rfsd.h"
#include "dsc.h"
#include "crt.h"
#include "stz.h"
#include "ce.h"
#include "fe.h"
#include "gm.h"
#include "tfsd.h"

/** */
namespace Oscl {
/** */
namespace Mot8xx {
/** */
namespace SI {
/** */
namespace MR {

/** */
template <class Type, unsigned Lsb>
class TDM : public TDMApi {
		/** */
		SDM<Type,Lsb+(10)>	_sdm;
		/** */
		RFSD<Type,Lsb+(8)>	_rfsd;
		/** */
		DSC<Type,Lsb+(7)>	_dsc;
		/** */
		CRT<Type,Lsb+(6)>	_crt;
		/** */
		STZ<Type,Lsb+(5)>	_stz;
		/** */
		CE<Type,Lsb+(4)>	_ce;
		/** */
		FE<Type,Lsb+(3)>	_fe;
		/** */
		GM<Type,Lsb+(2)>	_gm;
		/** */
		TFSD<Type,Lsb+(0)>	_tfsd;

	public:
		/** */
		TDM(Oscl::Bits::FieldApi<Type>& simode) noexcept;

	public:	// TDMApi
		/** */
		SDMApi&		getSDM() noexcept;
		/** */
		RFSDApi&	getRFSD() noexcept;
		/** */
		DSCApi&		getDSC() noexcept;
		/** */
		CRTApi&		getCRT() noexcept;
		/** */
		STZApi&		getSTZ() noexcept;
		/** */
		CEApi&		getCE() noexcept;
		/** */
		FEApi&		getFE() noexcept;
		/** */
		GMApi&		getGM() noexcept;
		/** */
		TFSDApi&	getTFSD() noexcept;
	};

template <class Type,unsigned Lsb>
TDM<Type,Lsb> ::TDM(Oscl::Bits::FieldApi<Type>& simode) noexcept:
		_sdm(simode),
		_rfsd(simode),
		_dsc(simode),
		_crt(simode),
		_stz(simode),
		_ce(simode),
		_fe(simode),
		_gm(simode),
		_tfsd(simode)
		{
	}

template <class Type,unsigned Lsb>
SDMApi&		TDM<Type,Lsb>::getSDM() noexcept{
	return _sdm;
	}

template <class Type,unsigned Lsb>
RFSDApi&	TDM<Type,Lsb>::getRFSD() noexcept{
	return _rfsd;
	}

template <class Type,unsigned Lsb>
DSCApi&		TDM<Type,Lsb>::getDSC() noexcept{
	return _dsc;
	}

template <class Type,unsigned Lsb>
CRTApi&		TDM<Type,Lsb>::getCRT() noexcept{
	return _crt;
	}

template <class Type,unsigned Lsb>
STZApi&		TDM<Type,Lsb>::getSTZ() noexcept{
	return _stz;
	}

template <class Type,unsigned Lsb>
CEApi&		TDM<Type,Lsb>::getCE() noexcept{
	return _ce;
	}

template <class Type,unsigned Lsb>
FEApi&		TDM<Type,Lsb>::getFE() noexcept{
	return _fe;
	}

template <class Type,unsigned Lsb>
GMApi&		TDM<Type,Lsb>::getGM() noexcept{
	return _gm;
	}

template <class Type,unsigned Lsb>
TFSDApi&	TDM<Type,Lsb>::getTFSD() noexcept{
	return _tfsd;
	}

}
}
}
}

#endif
