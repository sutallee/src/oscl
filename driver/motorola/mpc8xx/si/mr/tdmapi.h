/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_mot_mpc8xx_si_mr_tdmapih_
#define _oscl_drv_mot_mpc8xx_si_mr_tdmapih_
#include "sdmapi.h"
#include "rfsdapi.h"
#include "dscapi.h"
#include "crtapi.h"
#include "stzapi.h"
#include "ceapi.h"
#include "feapi.h"
#include "gmapi.h"
#include "tfsdapi.h"

/** */
namespace Oscl {
/** */
namespace Mot8xx {
/** */
namespace SI {
/** */
namespace MR {

/** */
class TDMApi {
	public:	// TDMApi
		/** Shut-up GCC. */
		virtual ~TDMApi() {}
		/** */
		virtual SDMApi&	getSDM() noexcept=0;
		/** */
		virtual RFSDApi&	getRFSD() noexcept=0;
		/** */
		virtual DSCApi&		getDSC() noexcept=0;
		/** */
		virtual CRTApi&		getCRT() noexcept=0;
		/** */
		virtual STZApi&		getSTZ() noexcept=0;
		/** */
		virtual CEApi&		getCE() noexcept=0;
		/** */
		virtual FEApi&		getFE() noexcept=0;
		/** */
		virtual GMApi&		getGM() noexcept=0;
		/** */
		virtual TFSDApi&	getTFSD() noexcept=0;
	};

}
}
}
}

#endif
