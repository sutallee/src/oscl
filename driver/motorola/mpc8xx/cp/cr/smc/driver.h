/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_mot8xx_cp_cr_smc_driverh_
#define _oscl_drv_mot8xx_cp_cr_smc_driverh_
#include "api.h"
#include "oscl/hw/motorola/mpc8xx/cpmreg.h"
#include "oscl/error/fatal.h"

/** */
namespace Oscl {
/** */
namespace Mot8xx {
/** */
namespace CP {
/** */
namespace CR {
/** */
namespace SMC {

/** */
template <unsigned smcNum>
class Driver : public Api {
	private:
		/** */
		volatile Oscl::Mot8xx::Cpm::CPCR::Reg&	_cpcr;
	public:
		/** */
		Driver(volatile Oscl::Mot8xx::Cpm::CPCR::Reg& cpcr) noexcept:
			_cpcr(cpcr){
				if((smcNum < 0) || (smcNum > 1)){
					Oscl::ErrorFatal::logAndExit(
						"Oscl::Mot8xx::CP::CR::SMC::Invalid SMC number.\n"
						);
					}
				}
	public:	// Api
		/** */
		void	initRxAndTxParams() noexcept{
			using namespace Oscl::Mot8xx::Cpm::CPCR;
			waitForReady();
			_cpcr	=		OPCODE::SMC::ValueMask_InitRxAndTxParams
						|	((smcNum<<(2+CH_NUM::Lsb))|CH_NUM::ValueMask_SMC1)
						|	FLG::ValueMask_Busy
						;
			}
		/** */
		void	initRxParams() noexcept{
			using namespace Oscl::Mot8xx::Cpm::CPCR;
			waitForReady();
			_cpcr	=		OPCODE::SMC::ValueMask_InitRxParams
						|	((smcNum<<(2+CH_NUM::Lsb))|CH_NUM::ValueMask_SMC1)
						|	FLG::ValueMask_Busy
						;
			}
		/** */
		void	initTxParms() noexcept{
			using namespace Oscl::Mot8xx::Cpm::CPCR;
			waitForReady();
			_cpcr	=		OPCODE::SMC::ValueMask_InitTxParams
						|	((smcNum<<(2+CH_NUM::Lsb))|CH_NUM::ValueMask_SMC1)
						|	FLG::ValueMask_Busy
						;
			}

		/** */
		void	enterHuntMode() noexcept{
			using namespace Oscl::Mot8xx::Cpm::CPCR;
			waitForReady();
			_cpcr	=		OPCODE::SMC::ValueMask_EnterHuntMode
						|	((smcNum<<(2+CH_NUM::Lsb))|CH_NUM::ValueMask_SMC1)
						|	FLG::ValueMask_Busy
						;
			}

		/** */
		void	stopTx() noexcept{
			using namespace Oscl::Mot8xx::Cpm::CPCR;
			waitForReady();
			_cpcr	=		OPCODE::SMC::ValueMask_StopTx
						|	((smcNum<<(2+CH_NUM::Lsb))|CH_NUM::ValueMask_SMC1)
						|	FLG::ValueMask_Busy
						;
			}

		/** */
		void	restartTx() noexcept{
			using namespace Oscl::Mot8xx::Cpm::CPCR;
			waitForReady();
			_cpcr	=		OPCODE::SMC::ValueMask_RestartTx
						|	((smcNum<<(2+CH_NUM::Lsb))|CH_NUM::ValueMask_SMC1)
						|	FLG::ValueMask_Busy
						;
			}

		/** */
		void	closeRxBd() noexcept{
			using namespace Oscl::Mot8xx::Cpm::CPCR;
			waitForReady();
			_cpcr	=		OPCODE::SMC::ValueMask_CloseRxBd
						|	((smcNum<<(2+CH_NUM::Lsb))|CH_NUM::ValueMask_SMC1)
						|	FLG::ValueMask_Busy
						;
			}

	private:
		/** */
		void	waitForReady() noexcept{
			using namespace Oscl::Mot8xx::Cpm::CPCR;
			while((_cpcr & FLG::FieldMask)==FLG::ValueMask_Busy);
			}
	};

}
}
}
}
}

#endif
