/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "driver.h"
#include "oscl/hw/motorola/mpc8xx/pramreg.h"
#include "oscl/error/fatal.h"
#include "oscl/hw/motorola/quicc/sdma/bdreg.h"
#include "oscl/cpu/sync.h"

// FIXME
#include "oscl/driver/motorola/mpc8xx/cp/cr/api.h"

using namespace Oscl::Mot8xx::Serial::SMC;
using namespace Oscl::Motorola::Quicc::Sdma;

Driver::Driver(	Oscl::Mot8xx::Pram::SMC&					pram,
				Oscl::Mot8xx::Smc::Map&						reg,
				Oscl::Mot8xx::CP::CR::SMC::Api&				cpcr,
				Oscl::Motorola::Quicc::Sdma::Tx::BufferDesc	txDescs[],
				Oscl::Done::Api*							txDescCompletes[],
				unsigned									nTxDescs,
				Oscl::Motorola::Quicc::Sdma::Rx::BufferDesc	rxDescs[],
				unsigned									nRxDescs,
				const char*							const	dpramBase,
				Oscl::Serial::Itc::RX::BufferMem			bufferMem[],
				unsigned									nBuffers,
				Oscl::Serial::Itc::TX::DoneMem				doneMem[],
				unsigned									nDoneMem
				) noexcept:
		_pram(pram),
		_reg(reg),
		_cpcr(cpcr),
		_rxBufferSize(sizeof(Oscl::Serial::Itc::RX::BufferMem)),
		_txDescs(txDescs),
		_nTxDescs(nTxDescs),
		_rxDescs(rxDescs),
		_nRxDescs(nRxDescs),
		_sdmaTxDriver(	*this,
						txDescs,
						txDescCompletes,
						nTxDescs,
						0,
						0
						),
		_sdmaRxDriver(	rxDescs,
						nRxDescs,
						true	// interruptEveryBuffer
						),
		_sdmaTxSerialDriver(_sdmaTxDriver),
		_sdmaRxSerialDriver(rxDescs,nRxDescs,*this),
		_txDriver(_sdmaTxSerialDriver,doneMem,nDoneMem),
		_rxDriver(_sdmaRxSerialDriver,bufferMem,nBuffers),
		_interrupt(reg.smce,_txDriver,_rxDriver),
		_dpramBase(dpramBase)
		{
	if(_rxBufferSize & 0x003){
		Oscl::ErrorFatal::logAndExit(
			"Oscl::Mot8xx::Serial::SMC::Driver: Buffer size must be multiple of 4.\n"
			);
		}
	}

void Driver::initialize() noexcept{
	using namespace Oscl::Mot8xx::Smc;
	// Configure according to docs:
	// 1. PortA TXD,RXD
	// 2. PortC CTS/CLSN and CD/RENA
	// 3. Don't enable RTS/TENA yet.
	// 4. PortA CLK1,CLK2
	// 5. SICR RxCS=CLK1,TxCS=CLK2
	// 6. SICR SC=NMSI
	// 7. SDCR=0x0001
	// 8. RBASE and TBASE
	_pram.header.rbptr	= (uint16_t)0;
	_pram.header.tbptr	= (uint16_t)0;
	_pram.header.rbase	= (uint16_t)0;
	_pram.header.tbase	= (uint16_t)0;
	OsclCpuInOrderMemoryAccessBarrier();
	_pram.header.rbase	= (uint16_t)
		((unsigned long)_rxDescs-(unsigned long)_dpramBase);
	_pram.header.tbase	= (uint16_t)
		((unsigned long)_txDescs-(unsigned long)_dpramBase);
	// 9. CPCR = InitRXandRx
	OsclCpuInOrderMemoryAccessBarrier();
	_cpcr.initRxAndTxParams();
	// 10. RFCR=0x10 ,TFCR = 0x10
	_pram.header.rfcr.changeBits(~0,Smc::PRAM::FCR::BO::ValueMask_BigEndian);
	_pram.header.tfcr.changeBits(~0,Smc::PRAM::FCR::BO::ValueMask_BigEndian);
	// 11. MRBLR=1520
	_pram.header.mrblr	= _rxBufferSize;
	_pram.psa.UART.max_idl	= 2;	// FIXME: 2 idle characters (parameterize)
	_pram.psa.UART.idlc		= 0;
	_pram.psa.UART.brkln	= 0;
	_pram.psa.UART.brkec	= 0;
	_pram.psa.UART.brkcr	= 10;	// FIXME: 8 data,no parity,1 stop 1 start
	_pram.psa.UART.r_mask	= 0;
	// 25. RXBD init
	_sdmaRxSerialDriver.initialize();
	// 26. TXBD init
	_sdmaTxSerialDriver.initialize();
//	_txDescs[0]._cs.clearBits(~0);
	// 27. SMCE=0xFFFF
	OsclCpuInOrderMemoryAccessBarrier();
	_reg.smce	= (		Oscl::Mot8xx::Smc::SMCE::RX::ValueMask_Clear
					|	Oscl::Mot8xx::Smc::SMCE::TX::ValueMask_Clear
					|	Oscl::Mot8xx::Smc::SMCE::BSY::ValueMask_Clear
					|	Oscl::Mot8xx::Smc::SMCE::BRK::ValueMask_Clear
					|	Oscl::Mot8xx::Smc::SMCE::BRKE::ValueMask_Clear
					);
	// 28. SMCM=0x001A
	OsclCpuInOrderMemoryAccessBarrier();
	_reg.smcm	=	(		SMCM::TX::ValueMask_Enable
						|	SMCM::RX::ValueMask_Enable
						);
	// 29. CIMR=0x40000000,CICR=init
	// 30. GSMR_H=0x00000000
	OsclCpuInOrderMemoryAccessBarrier();

	// FIXME: parameterize the following
	const unsigned	dataBits	= 8;
	const unsigned	stopBits	= 1;
	const unsigned	startBits	= 1;
	const unsigned	parityBits	= 0;
	const unsigned	bitsPerChar	= (dataBits+stopBits+startBits+parityBits);

	_reg.smcmr	= (		((bitsPerChar-1)<<SMCMR::CLEN::Lsb)
					|	SMCMR::SL::ValueMask_OneStopBit
					|	SMCMR::PEN::ValueMask_NoParity
					|	SMCMR::PM::ValueMask_OddParity
					|	SMCMR::SM::ValueMask_UART
					|	SMCMR::DM::ValueMask_Normal
//					|	SMCMR::DM::ValueMask_LocalLoop
					|	SMCMR::TEN::ValueMask_TxDisable
					|	SMCMR::REN::ValueMask_RxDisable
					);

	// 32. DSR=0xD555
	OsclCpuInOrderMemoryAccessBarrier();
	// 33. PSMR=0x0A0A
	OsclCpuInOrderMemoryAccessBarrier();
	// 34. PortC RTS/TENA
	// 35. GSMR_L=0x1088003C (ENT | ENR)
	OsclCpuInOrderMemoryAccessBarrier();
	_reg.smcmr.changeBits(	(		SMCMR::TEN::FieldMask
								|	SMCMR::REN::FieldMask
								),
							(		SMCMR::TEN::ValueMask_TxEnable
								|	SMCMR::REN::ValueMask_RxEnable
								)
							);
	}

Oscl::Interrupt::StatusHandlerApi&	Driver::getISR() noexcept{
	return _interrupt;
	}

Oscl::Interrupt::IsrDsrApi&	Driver::getIsrDsr() noexcept{
	return _interrupt;
	}

Oscl::Mt::Runnable&	Driver::getTxDriverRunnable() noexcept{
	return _txDriver;
	}

Oscl::Stream::Output::Req::Api::SAP&	Driver::getTxDriverSAP() noexcept{
	return _txDriver.getSAP();
	}

Oscl::Stream::Output::Api&	Driver::getTxSyncApi() noexcept{
	return _txDriver.getSyncApi();
	}

Oscl::Mt::Runnable&	Driver::getRxDriverRunnable() noexcept{
	return _rxDriver;
	}

Oscl::Stream::Input::Req::Api::SAP&	Driver::getRxDriverSAP() noexcept{
	return _rxDriver.getSAP();
	}

Oscl::Stream::Input::Api&	Driver::getRxSyncApi() noexcept{
	return _rxDriver.getSyncApi();
	}

bool	Driver::hasError(unsigned descIndex) noexcept{
	using namespace
		Oscl::Motorola::Quicc::Sdma::RxBD::Status::SmcUART;
	if(!(_rxDescs[descIndex]._cs.equal(	(			BR::FieldMask
												|	FR::FieldMask
												|	PR::FieldMask
												|	OV::FieldMask
												),
											0
											)
					)
				){
		accumulateErrors(_rxDescs[descIndex]._cs);
		}
	return false;
	}

void	Driver::accumulateErrors(unsigned errors) noexcept{
	// FIXME: This should callback to an error counter
	// interface for each error type.
	}

void	Driver::syncOpen() noexcept{
	initialize();
	_rxDriver.syncOpen();
	_txDriver.syncOpen();
	}

void	Driver::syncClose() noexcept{
	_rxDriver.syncClose();
	_txDriver.syncClose();
	}

bool	Driver::sdmaIsStopped(unsigned descStatus) noexcept{
	// FIXME: This should be implemented
	// to check the descStatus
	return false;
	}

void	Driver::restart() noexcept{
	// FIXME: Should not ever be called unless
	// sdmaIsStopped() returns true.
	for(;;);
	}
