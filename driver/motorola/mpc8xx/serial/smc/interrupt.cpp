/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "interrupt.h"
#include "oscl/bits/bitfield.h"
#include "oscl/mt/mutex/iss.h"

using namespace Oscl::Mot8xx::Serial::SMC;

Interrupt::Interrupt(	volatile Oscl::Mot8xx::Smc::SMCE::Reg&	smce,
						Oscl::Mt::Sema::SignalApi&				txSema,
						Oscl::Mt::Sema::SignalApi&				rxSema
						) noexcept:
		_smce(smce),
		_txSema(txSema),
		_rxSema(rxSema)
		{
	}

bool	Interrupt::interrupt() noexcept{
	if(isr()){
		dsr();
		return true;
		}
	return false;
	}

bool	Interrupt::isr() noexcept{
	using namespace Oscl::Mot8xx::Smc::SMCE;
	Oscl::Mot8xx::Smc::SMCE::Reg	smce;
	smce	= _smce;
	_smceCopy	|= smce;
	_smce		= smce;
	
	return smce != 0;
	}

void	Interrupt::dsr() noexcept{
	using namespace Oscl::Mot8xx::Smc::SMCE;
	Oscl::Mot8xx::Smc::SMCE::Reg	smce;
		{
		Oscl::Mt::IntScopeSync	iss;
		smce		= _smceCopy;
		_smceCopy	= 0;
		}
	if(		(smce & (TX::FieldMask))
		!= (TX::ValueMask_NotPending)
		){
		_txSema.suSignal();
		}

	if(		(smce & (RX::FieldMask))
		!= (RX::ValueMask_NotPending)
		){
		_rxSema.suSignal();
		}
	}

