/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_mot_mpc860_serial_smc_driverh_
#define _oscl_drv_mot_mpc860_serial_smc_driverh_

#include "oscl/mt/itc/srv/ocsyncapi.h"
#include "oscl/mt/itc/mbox/server.h"
#include "oscl/hw/motorola/mpc8xx/pram/smc.h"
#include "oscl/hw/motorola/mpc8xx/smc.h"
#include "oscl/driver/motorola/mpc8xx/cp/cr/smc/api.h"
#include "oscl/driver/motorola/quicc/sdma/tx/driver.h"
#include "oscl/driver/motorola/quicc/sdma/rx/driver.h"
#include "oscl/driver/motorola/quicc/sdma/serial/tx/driver.h"
#include "oscl/driver/motorola/quicc/sdma/serial/rx/driver.h"
#include "oscl/serial/itc/rx/driver.h"
#include "oscl/serial/itc/tx/driver.h"
#include "interrupt.h"

/** */
namespace Oscl {
/** */
namespace Mot8xx {
/** */
namespace Serial {
/** */
namespace SMC {

/** */
class Driver :	public Motorola::Quicc::Sdma::Serial::Rx::DescErrorObserver,
				private	Oscl::Motorola::Quicc::Sdma::Tx::ErrorHandler,
				public Oscl::Mt::Itc::Srv::OpenCloseSyncApi
				{
	private:
		/** */
		Oscl::Mot8xx::Pram::SMC&						_pram;
		/** */
		Oscl::Mot8xx::Smc::Map&							_reg;
		/** */
		Oscl::Mot8xx::CP::CR::SMC::Api&					_cpcr;
		/** */
		const uint16_t								_rxBufferSize;
		/** */
		Oscl::Motorola::Quicc::Sdma::Tx::BufferDesc*	const _txDescs;
		/** */
		const unsigned									_nTxDescs;
		/** */
		Oscl::Motorola::Quicc::Sdma::Rx::BufferDesc*	const _rxDescs;
		/** */
		const unsigned									_nRxDescs;
		/** */
		Oscl::Motorola::Quicc::Sdma::Tx::Driver			_sdmaTxDriver;
		/** */
		Oscl::Motorola::Quicc::Sdma::Rx::Driver			_sdmaRxDriver;
		/** */
		Oscl::Motorola::Quicc::Sdma::Serial::Tx::Driver	_sdmaTxSerialDriver;
		/** */
		Oscl::Motorola::Quicc::Sdma::Serial::Rx::Driver	_sdmaRxSerialDriver;
		/** */
		Oscl::Serial::Itc::TX::Driver					_txDriver;
		/** */
		Oscl::Serial::Itc::RX::Driver					_rxDriver;
		/** */
		Interrupt										_interrupt;
		/** */
		const char*								const	_dpramBase;

	public:
		/** */
		Driver(	Oscl::Mot8xx::Pram::SMC&					pram,
				Oscl::Mot8xx::Smc::Map&						reg,
				Oscl::Mot8xx::CP::CR::SMC::Api&				cpcr,
				Oscl::Motorola::Quicc::Sdma::Tx::BufferDesc	txDescs[],
				Oscl::Done::Api*							txDescCompletes[],
				unsigned									nTxDescs,
				Oscl::Motorola::Quicc::Sdma::Rx::BufferDesc	rxDescs[],
				unsigned									nRxDescs,
				const char*							const	dpramBase,
				Oscl::Serial::Itc::RX::BufferMem			bufferMem[],
				unsigned									nBuffers,
				Oscl::Serial::Itc::TX::DoneMem				doneMem[],
				unsigned									nDoneMem
				) noexcept;
		/** */
		virtual ~Driver(){}
		/** */
		Oscl::Interrupt::StatusHandlerApi&		getISR() noexcept;
		/** */
		Oscl::Interrupt::IsrDsrApi&				getIsrDsr() noexcept;
		/** */
		Oscl::Mt::Runnable&						getTxDriverRunnable() noexcept;
		/** */
		Oscl::Stream::Output::Req::Api::SAP&	getTxDriverSAP() noexcept;
		/** */
		Oscl::Stream::Output::Api&				getTxSyncApi() noexcept;
		/** */
		Oscl::Mt::Runnable&						getRxDriverRunnable() noexcept;
		/** */
		Oscl::Stream::Input::Req::Api::SAP&		getRxDriverSAP() noexcept;
		/** */
		Oscl::Stream::Input::Api&				getRxSyncApi() noexcept;

	private:	// DescErrorObserver
		/** */
		bool	hasError(unsigned descIndex) noexcept;
		/** */
		void	accumulateErrors(unsigned errors) noexcept;
		/** */
		void	initialize() noexcept;

	public:	// Oscl::Mt::Itc::Srv::OpenCloseSyncApi
		/** */
		void	syncOpen() noexcept;
		/** */
		void	syncClose() noexcept;
	private: // Oscl::Motorola::Quicc::Sdma::Tx::ErrorHandler
		/** */
		bool	sdmaIsStopped(unsigned descStatus) noexcept;
		/** */
		void	restart() noexcept;
	};

}
}
}
}

#endif
