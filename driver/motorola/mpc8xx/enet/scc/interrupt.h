/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_mot_mpc860_enet_scc_interrupth_
#define _oscl_drv_mot_mpc860_enet_scc_interrupth_
#include "oscl/mt/sema/sigapi.h"
#include "oscl/interrupt/shandler.h"
#include "oscl/hw/motorola/mpc8xx/sccreg.h"

/** */
namespace Oscl {
/** */
namespace Mot8xx {
/** */
namespace Enet {
/** */
namespace SCC {

/** */
class Interrupt : public Oscl::Interrupt::StatusHandlerApi {
	private:
		/** */
		volatile Oscl::Mot8xx::Scc::SCCE::Reg&	_scce;
		/** */
		Oscl::Mt::Sema::SignalApi&				_txSema;
		/** */
		Oscl::Mt::Sema::SignalApi&				_rxSema;

	public:
		/** */
		Interrupt(	volatile Oscl::Mot8xx::Scc::SCCE::Reg&	scce,
					Oscl::Mt::Sema::SignalApi&				txSema,
					Oscl::Mt::Sema::SignalApi&				rxSema
					) noexcept;

	private:
		/** */
		bool	interrupt() noexcept;
	};

}
}
}
}


#endif
