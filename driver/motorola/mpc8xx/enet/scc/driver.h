/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_mot_mpc860_enet_scc_driverh_
#define _oscl_drv_mot_mpc860_enet_scc_driverh_

#include "oscl/mt/itc/mbox/server.h"
#include "oscl/hw/motorola/mpc8xx/pram/enet.h"
#include "oscl/hw/motorola/mpc8xx/scc.h"
#include "oscl/driver/motorola/mpc8xx/cp/cr/scc/api.h"
#include "oscl/driver/motorola/quicc/sdma/tx/driver.h"
#include "oscl/driver/motorola/quicc/sdma/frame/tx/driver.h"
#include "oscl/driver/motorola/quicc/sdma/frame/rx/driver.h"
#include "interrupt.h"
#include "oscl/frame/pdu/rx/driver/driver.h"
#include "oscl/frame/pdu/tx/driver/driver.h"
#include "oscl/protocol/ieee/mac.h"
#include "oscl/frame/pdu/rx/mac.h"

/** */
namespace Oscl {
/** */
namespace Mot8xx {
/** */
namespace Enet {
/** */
namespace SCC {

/** */
class Driver :	public Motorola::Quicc::Sdma::Frame::Rx::DescErrorObserver,
				private Oscl::Motorola::Quicc::Sdma::Tx::ErrorHandler,
				public Mt::Itc::Srv::OpenCloseSyncApi,
				public Oscl::Frame::Pdu::Rx::MediaAccessApi
				{
	private:
		/** */
		Oscl::Mot8xx::Pram::SCC::ENET&					_pram;
		/** */
		Oscl::Mot8xx::Scc::Map&							_reg;
		/** */
		Oscl::Mot8xx::CP::CR::SCC::Api&					_cpcr;
		/** */
		const uint16_t								_rxBufferSize;
		/** */
		const uint16_t								_maximumFrameLength;
		/** */
		Oscl::Motorola::Quicc::Sdma::Tx::BufferDesc*	const _txDescs;
		/** */
		const unsigned									_nTxDescs;
		/** */
		Oscl::Motorola::Quicc::Sdma::Rx::BufferDesc*	const _rxDescs;
		/** */
		const unsigned									_nRxDescs;
		/** */
		Oscl::Motorola::Quicc::Sdma::Tx::Driver			_sdmaTxDriver;
		/** */
		Oscl::Motorola::Quicc::Sdma::Frame::Tx::Driver	_sdmaTxFrameDriver;
		/** */
		Oscl::Motorola::Quicc::Sdma::Frame::Rx::Driver	_sdmaRxFrameDriver;
		/** */
		Oscl::Frame::Pdu::Rx::Driver					_rxDriver;
		/** */
		Oscl::Frame::Pdu::Tx::Driver					_txDriver;
		/** */
		Interrupt										_interrupt;
		/** */
		const char*								const	_dpramBase;
		/** */
		const Oscl::Protocol::IEEE::MacAddress&			_stationAddress;

		/** */
		struct {
			/** */
			unsigned long	nRetries;
			/** */
			unsigned long	carrierSenseLost;
			/** */
			unsigned long	nUnderrun;
			/** */
			unsigned long	nRetryLimitExceeded;
			/** */
			unsigned long	nLateCollision;
			/** */
			unsigned long	nHeartBeatsMissed;
			/** */
			unsigned long	sdmaRestarts;
			} _stats;

	public:
		/** */
		Driver(	Oscl::Mot8xx::Pram::SCC::ENET&				pram,
				Oscl::Mot8xx::Scc::Map&						reg,
				Oscl::Mot8xx::CP::CR::SCC::Api&				cpcr,
				uint16_t									rxBufferSize,
				uint16_t									maximumFrameLength,
				Oscl::Motorola::Quicc::Sdma::Tx::BufferDesc	txDescs[],
				Oscl::Done::Api*							txDescCompletes[],
				unsigned									nTxDescs,
				Oscl::Motorola::Quicc::Sdma::Rx::BufferDesc	rxDescs[],
				unsigned									nRxDescs,
				const char*							const	dpramBase,
				const Oscl::Protocol::IEEE::MacAddress&		stationAddress
				) noexcept;
		/** */
		virtual ~Driver(){}
		/** */
		Oscl::Interrupt::StatusHandlerApi&	getISR() noexcept;
		/** */
		Oscl::Mt::Runnable&					getTxDriverRunnable() noexcept;
		/** */
		Oscl::Frame::Pdu::Tx::Req::Api::SAP&	getTxDriverSAP() noexcept;
		/** */
		Oscl::Mt::Runnable&					getRxDriverRunnable() noexcept;
		/** */
		Oscl::Frame::Pdu::Rx::DriverApi&	getRxDriver() noexcept;

	private:	// DescErrorObserver
		/** */
		bool	hasError(unsigned descIndex) noexcept;
		/** */
		void	initialize() noexcept;

	public:	// Oscl::Mt::Itc::Srv::OpenCloseSyncApi
		/** */
		void	syncOpen() noexcept;
		/** */
		void	syncClose() noexcept;

	public:	// Oscl::Frame::Pdu::Rx::MediaAccessApi
		/** */
		bool	addressMatch(Oscl::Pdu::Pdu* frame) noexcept;
		/** */
		unsigned long	getProtocolType(Oscl::Pdu::Pdu* frame) noexcept;
		/** */
		void	strip(Oscl::Pdu::Composite* frame) noexcept;

	private: // Oscl::Motorola::Quicc::Sdma::Tx::ErrorHandler
		/** */
		bool	sdmaIsStopped(unsigned descStatus) noexcept;
		/** */
		void	restart() noexcept;
	};

}
}
}
}

#endif
