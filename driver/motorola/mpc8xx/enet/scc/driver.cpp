/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "driver.h"
#include "oscl/hw/motorola/mpc8xx/pramreg.h"
#include "oscl/error/fatal.h"
#include "oscl/hw/motorola/quicc/sdma/bdreg.h"
#include "oscl/cpu/sync.h"
#include "oscl/protocol/ieee/ethernet/header.h"
#include "oscl/endian/type.h"

// FIXME
#include "oscl/driver/motorola/mpc8xx/cp/cr/api.h"

using namespace Oscl::Mot8xx::Enet::SCC;
using namespace Oscl::Motorola::Quicc::Sdma;

Driver::Driver(	Oscl::Mot8xx::Pram::SCC::ENET&				pram,
				Oscl::Mot8xx::Scc::Map&						reg,
				Oscl::Mot8xx::CP::CR::SCC::Api&				cpcr,
				uint16_t									rxBufferSize,
				uint16_t									maximumFrameLength,
				Oscl::Motorola::Quicc::Sdma::Tx::BufferDesc	txDescs[],
				Oscl::Done::Api*							txDescCompletes[],
				unsigned									nTxDescs,
				Oscl::Motorola::Quicc::Sdma::Rx::BufferDesc	rxDescs[],
				unsigned									nRxDescs,
				const char*							const	dpramBase,
				const Oscl::Protocol::IEEE::MacAddress&		stationAddress
				) noexcept:
		_pram(pram),
		_reg(reg),
		_cpcr(cpcr),
		_rxBufferSize(rxBufferSize),
		_maximumFrameLength(maximumFrameLength),
		_txDescs(txDescs),
		_nTxDescs(nTxDescs),
		_rxDescs(rxDescs),
		_nRxDescs(nRxDescs),
		_sdmaTxDriver(	*this,
						txDescs,
						txDescCompletes,
						nTxDescs,
							TxBD::Status::I::FieldMask
						|	TxBD::Status::L::FieldMask
						|	TxBD::Status::TC::FieldMask
						|	TxBD::Status::PAD::FieldMask,
							TxBD::Status::I::ValueMask_Enable
						|	TxBD::Status::L::ValueMask_LastDesc
						|	TxBD::Status::TC::ValueMask_TxCRC
						|	TxBD::Status::PAD::ValueMask_PadFrame
						),
		_sdmaTxFrameDriver(_sdmaTxDriver),
		_sdmaRxFrameDriver(rxDescs,nRxDescs,*this),
		_rxDriver(_sdmaRxFrameDriver,*this,32,rxBufferSize,4),
		_txDriver(_sdmaTxFrameDriver,nTxDescs),
		_interrupt(reg.scce,_txDriver,_rxDriver),
		_dpramBase(dpramBase),
		_stationAddress(stationAddress)
		{
	_stats.nRetries				= 0;
	_stats.carrierSenseLost		= 0;
	_stats.nUnderrun			= 0;
	_stats.nRetryLimitExceeded	= 0;
	_stats.nLateCollision		= 0;
	_stats.nHeartBeatsMissed	= 0;
	_stats.sdmaRestarts			= 0;

	if(rxBufferSize & 0x003){
		Oscl::ErrorFatal::logAndExit(
			"Oscl::Mot8xx::Enet::SCC::Driver:"
			"Buffer size must be multiple of 4"
			);
		}
	}

void Driver::initialize() noexcept{
	using namespace Oscl::Mot8xx::Scc;
	// FIXME:
	Oscl::Mot8xx::CP::CR::Api&
	cpcr	= *(Oscl::Mot8xx::CP::CR::Api*)0xFF0009C0;
	// Configure according to docs:
	// 1. PortA TXD,RXD
	// 2. PortC CTS/CLSN and CD/RENA
	// 3. Don't enable RTS/TENA yet.
	// 4. PortA CLK1,CLK2
	// 5. SICR RxCS=CLK1,TxCS=CLK2
	// 6. SICR SC=NMSI
	// 7. SDCR=0x0001
	// 8. RBASE and TBASE
	_pram.header.rbptr	= (uint16_t)0;
	_pram.header.tbptr	= (uint16_t)0;
	_pram.header.rbase	= (uint16_t)0;
	_pram.header.tbase	= (uint16_t)0;
	OsclCpuInOrderMemoryAccessBarrier();
	cpcr.reset();
	_pram.header.rbase	= (uint16_t)
		((unsigned long)_rxDescs-(unsigned long)_dpramBase);
	_pram.header.tbase	= (uint16_t)
		((unsigned long)_txDescs-(unsigned long)_dpramBase);
	// 9. CPCR = InitRXandRx
	OsclCpuInOrderMemoryAccessBarrier();
	_cpcr.initRxAndTxParams();
	// 10. RFCR=0x10 ,TFCR = 0x10
	_pram.header.rfcr.changeBits(~0,Scc::PRAM::FCR::BO::ValueMask_BigEndian);
	_pram.header.tfcr.changeBits(~0,Scc::PRAM::FCR::BO::ValueMask_BigEndian);
	// 11. MRBLR=1520
	_pram.header.mrblr	= _rxBufferSize;
	// 12. C_PRES=0xFFFFFFFF (CCITT-CRC32)
	_pram.c_pres	= (uint32_t)Scc::PRAM::ENET::C_PRES::CrcCcitt32;
	// 13. C_MASK=0xDEBB20E3 (CCITT-CRC32)
	_pram.c_mask	= (uint32_t)Scc::PRAM::ENET::C_MASK::CrcCcitt32;
	// 14. CRCEC=0,ALEC=0,DISFC=0
	_pram.crcec	= 0;
	_pram.alec	= 0;
	_pram.disfc	= 0;
	// 15. PAD=0x8888
	_pram.pads	= 0x8888;
	// 16. RET_LIM=0x000F
	_pram.ret_lim	= Scc::PRAM::ENET::RET_LIM::Typical;
	// 17. MFLR=0x05EE
//	_pram.mflr	= Scc::PRAM::ENET::MFLR::Typical;
	_pram.mflr	= _maximumFrameLength;
	// 18. MINFLR=64
	_pram.minflr	= Scc::PRAM::ENET::MINFLR::Typical;
	// 19. MAXD1=MAXD2=1520
	_pram.maxd1	= Scc::PRAM::ENET::MAXD1::Typical;
	_pram.maxd2	= Scc::PRAM::ENET::MAXD2::Typical;
	// 20. GADDR1:GADDR4=0
	_pram.gaddr1	= 
	_pram.gaddr2	= 
	_pram.gaddr3	= 
	_pram.gaddr4	= 0;	// FIXME: Should be configurable
	// 21. PADDR1_H=0x380,PADDR1_M=0x12E0,PADDR1_L=0x5634
//	_pram.paddr1_h	= 0x5634;	// FIXME
//	_pram.paddr1_m	= 0x12E0;	// FIXME
//	_pram.paddr1_l	= 0x0300;	// FIXME
	uint16_t		w;
	w	= _stationAddress.sixthOctet();
	w	<<=8;
	w	|= _stationAddress.fifthOctet();
	_pram.paddr1_h	= w;
	w	= _stationAddress.fourthOctet();
	w	<<=8;
	w	|= _stationAddress.thirdOctet();
	_pram.paddr1_m	= w;
	w	= _stationAddress.secondOctet();
	w	<<=8;
	w	|= _stationAddress.firstOctet();
	_pram.paddr1_l	= w;
	// 22. P_PER=0
	_pram.p_per	= 0;
	// 23. IADDR1:IADDR4=0
	_pram.iaddr1	=
	_pram.iaddr2	=
	_pram.iaddr3	=
	_pram.iaddr4	= 0;
	// 24. TADDR_H=TADDR_M=TADDR_L=0
	_pram.taddr_h	=
	_pram.taddr_m	=
	_pram.taddr_l	= 0;
	// 25. RXBD init
	_sdmaRxFrameDriver.initialize();
	// 26. TXBD init
	_sdmaTxFrameDriver.initialize();
//	_txDescs[0]._cs.clearBits(~0);
	// 27. SCCE=0xFFFF
	OsclCpuInOrderMemoryAccessBarrier();
	_reg.scce	= Oscl::Mot8xx::Scc::SCCE::ENET::FieldMask;
	// 28. SCCM=0x001A
	OsclCpuInOrderMemoryAccessBarrier();
	_reg.sccm.setBits(		SCCM::ENET::TXB::ValueMask_Enable
						|	SCCM::ENET::TXE::ValueMask_Enable
						|	SCCM::ENET::RXF::ValueMask_Enable
						);
	// 29. CIMR=0x40000000,CICR=init
	// 30. GSMR_H=0x00000000
	OsclCpuInOrderMemoryAccessBarrier();
	_reg.gsmrh.changeBits(
		(		GSMR_H::IRP::FieldMask
			|	GSMR_H::GDE::FieldMask
			|	GSMR_H::TCRC::FieldMask
			|	GSMR_H::REVD::FieldMask
			|	GSMR_H::TRX::FieldMask
			|	GSMR_H::TTX::FieldMask
			|	GSMR_H::CDP::FieldMask
			|	GSMR_H::CTSP::FieldMask
			|	GSMR_H::CDS::FieldMask
			|	GSMR_H::CTSS::FieldMask
			|	GSMR_H::TFL::FieldMask
			|	GSMR_H::RFW::FieldMask
			|	GSMR_H::TXSY::FieldMask
			|	GSMR_H::SYNL::FieldMask
			|	GSMR_H::RTSM::FieldMask
			|	GSMR_H::RSYN::FieldMask
			),
		(		GSMR_H::IRP::ValueMask_ActiveHigh
			|	GSMR_H::GDE::ValueMask_NoGlitchDetect
			|	GSMR_H::TCRC::ValueMask_NonTransparent
			|	GSMR_H::REVD::ValueMask_Normal
			|	GSMR_H::TRX::ValueMask_Normal
			|	GSMR_H::TTX::ValueMask_Normal
			|	GSMR_H::CDP::ValueMask_Normal
			|	GSMR_H::CTSP::ValueMask_Normal
			|	GSMR_H::CDS::ValueMask_Asynchronous
			|	GSMR_H::CTSS::ValueMask_Asynchronous
			|	GSMR_H::TFL::ValueMask_TxFifo32Bytes
			|	GSMR_H::RFW::ValueMask_WideFifo
			|	GSMR_H::TXSY::ValueMask_TxRxNotSynchronized
			|	GSMR_H::SYNL::ValueMask_SyncExternal
			|	GSMR_H::RTSM::ValueMask_IdleBetweenFrames
			|	GSMR_H::RSYN::ValueMask_Normal
			)
		);
	// 31. GSMR_L=
	OsclCpuInOrderMemoryAccessBarrier();
	_reg.gsmrl.changeBits(
		(		GSMR_L::SIR::FieldMask
			|	GSMR_L::EDGE::FieldMask
			|	GSMR_L::TCI::FieldMask
			|	GSMR_L::TSNC::FieldMask
			|	GSMR_L::RINV::FieldMask
			|	GSMR_L::TINV::FieldMask
			|	GSMR_L::TPL::FieldMask
			|	GSMR_L::TPP::FieldMask
			|	GSMR_L::TEND::FieldMask
			|	GSMR_L::TDCR::FieldMask
			|	GSMR_L::RDCR::FieldMask
			|	GSMR_L::RENC::FieldMask
			|	GSMR_L::TENC::FieldMask
			|	GSMR_L::DIAG::FieldMask
			|	GSMR_L::ENR::FieldMask
			|	GSMR_L::ENT::FieldMask
			|	GSMR_L::MODE::FieldMask
			),
		(		GSMR_L::SIR::ValueMask_Normal	// 0x1088_000C
			|	GSMR_L::EDGE::ValueMask_BothEdges
			|	GSMR_L::TCI::ValueMask_Invert
			|	GSMR_L::TSNC::ValueMask_Infinite
			|	GSMR_L::RINV::ValueMask_Normal
			|	GSMR_L::TINV::ValueMask_Normal
			|	GSMR_L::TPL::ValueMask_Preamble48Bits
			|	GSMR_L::TPP::ValueMask_Repetitive10
			|	GSMR_L::TEND::ValueMask_DataOnly
			|	GSMR_L::TDCR::ValueMask_Clock1X
			|	GSMR_L::RDCR::ValueMask_Clock1X
			|	GSMR_L::RENC::ValueMask_NRZ
			|	GSMR_L::TENC::ValueMask_NRZ
			|	GSMR_L::DIAG::ValueMask_Normal
			|	GSMR_L::ENR::ValueMask_Disable
			|	GSMR_L::ENT::ValueMask_Disable
			|	GSMR_L::MODE::ValueMask_ENET
			)
		);
	// 32. DSR=0xD555
	OsclCpuInOrderMemoryAccessBarrier();
	_reg.dsr.changeBits(	(		DSR::SYN2::ENET::FieldMask
								|	DSR::SYN1::ENET::FieldMask
								),
							(		DSR::SYN2::ENET::ValueMask_Normal
								|	DSR::SYN1::ENET::ValueMask_Normal
								)
							);
	// 33. PSMR=0x0A0A
	OsclCpuInOrderMemoryAccessBarrier();
	_reg.psmr.changeBits(	(		PSMR::ENET::HBC::FieldMask
								|	PSMR::ENET::FC::FieldMask
								|	PSMR::ENET::RSH::FieldMask
								|	PSMR::ENET::IAM::FieldMask
								|	PSMR::ENET::CRC::FieldMask
								|	PSMR::ENET::PRO::FieldMask
								|	PSMR::ENET::BRO::FieldMask
								|	PSMR::ENET::SBT::FieldMask
								|	PSMR::ENET::LPB::FieldMask
								|	PSMR::ENET::SIP::FieldMask
								|	PSMR::ENET::LCW::FieldMask
								|	PSMR::ENET::NIB::FieldMask
								|	PSMR::ENET::FDE::FieldMask
								),
							(		PSMR::ENET::HBC::ValueMask_Disable
								|	PSMR::ENET::FC::ValueMask_Normal
								|	PSMR::ENET::RSH::ValueMask_DiscardShortFrames
								|	PSMR::ENET::IAM::ValueMask_PADDR1
								|	PSMR::ENET::CRC::ValueMask_CcittCrc32
//								|	PSMR::ENET::PRO::ValueMask_Promiscuous
								|	PSMR::ENET::PRO::ValueMask_Normal
								|	PSMR::ENET::BRO::ValueMask_ReceiveBroadcasts
								|	PSMR::ENET::SBT::ValueMask_Normal
//								|	PSMR::ENET::LPB::ValueMask_Loopback
								|	PSMR::ENET::LPB::ValueMask_Normal
								|	PSMR::ENET::SIP::ValueMask_Normal
								|	PSMR::ENET::LCW::ValueMask_Late64Byte
								|	PSMR::ENET::NIB::ValueMask_Typical22Bits
//								|	PSMR::ENET::FDE::ValueMask_Enable
								|	PSMR::ENET::FDE::ValueMask_Disable
								)
							);
	// 34. PortC RTS/TENA
	// 35. GSMR_L=0x1088003C (ENT | ENR)
	OsclCpuInOrderMemoryAccessBarrier();
	_reg.gsmrl.changeBits(		GSMR_L::ENR::FieldMask
							|	GSMR_L::ENT::FieldMask,
								GSMR_L::ENR::ValueMask_Enable
							|	GSMR_L::ENT::ValueMask_Enable
							);
	}

Oscl::Interrupt::StatusHandlerApi&	Driver::getISR() noexcept{
	return _interrupt;
	}

Oscl::Mt::Runnable&	Driver::getTxDriverRunnable() noexcept{
	return _txDriver;
	}

Oscl::Frame::Pdu::Tx::Req::Api::SAP&	Driver::getTxDriverSAP() noexcept{
	return _txDriver.getTxSAP();
	}

Oscl::Mt::Runnable&	Driver::getRxDriverRunnable() noexcept{
	return _rxDriver;
	}

Oscl::Frame::Pdu::Rx::DriverApi&	Driver::getRxDriver() noexcept{
	return _rxDriver;
	}

bool	Driver::hasError(unsigned descIndex) noexcept{
	using namespace
		Oscl::Motorola::Quicc::Sdma::RxBD::Status::Eof::Sof::Ethernet;
	bool	error;
	error	= !(_rxDescs[descIndex]._cs.equal(	(		LG::FieldMask
													|	NO::FieldMask
													|	SH::FieldMask
													|	CR::FieldMask
													|	OV::FieldMask
													|	CD::FieldMask
													),
												0
												)
					);
	return error;
	}

void	Driver::syncOpen() noexcept{
	initialize();
	_rxDriver.syncOpen();
	_txDriver.syncOpen();
	}

void	Driver::syncClose() noexcept{
	_rxDriver.syncClose();
	_txDriver.syncClose();
	}

bool	Driver::addressMatch(Oscl::Pdu::Pdu* frame) noexcept{
	// FIXME
	// This function should compare against all alternate addresses
	// assigned to this interface, not just the station and broadcast.
	Oscl::Protocol::IEEE::MacAddress		destination;
	Oscl::Protocol::IEEE::MacAddressInit	broadcast(	0xFF,
														0xFF,
														0xFF,
														0xFF,
														0xFF,
														0xFF
														);
	frame->read(	(uint8_t*)&destination,
					Oscl::Protocol::IEEE::MacAddress::nOctets,
					0
					);
	if(destination == _stationAddress) return true;
	if(destination == broadcast) return true;
	return false;
	}

unsigned long	Driver::getProtocolType(Oscl::Pdu::Pdu* frame) noexcept{
	Oscl::Endian::Big::U16	protocolType;
	Oscl::Protocol::IEEE::Ethernet::Header*	header	= 0;
	frame->read(	(uint8_t*)&protocolType,
					sizeof(protocolType),
					(unsigned)&header->type
					);
	return protocolType;
	}

void	Driver::strip(Oscl::Pdu::Composite* frame) noexcept{
	using namespace Oscl::Protocol::IEEE;
	const unsigned	headerSize	=		(2*MacAddress::nOctets)
									+	sizeof(uint16_t)
									;
	const unsigned	crcTrailerSize	=	sizeof(uint32_t);
	frame->stripHeader(headerSize);
	frame->stripTrailer(crcTrailerSize);
	}

bool	Driver::sdmaIsStopped(unsigned descStatus) noexcept{
	const unsigned	errorStatusMask	=
		(		TxBD::Status::Ethernet::UN::FieldMask
			|	TxBD::Status::Ethernet::RL::FieldMask
			|	TxBD::Status::Ethernet::LC::FieldMask
			|	TxBD::Status::Ethernet::RC::FieldMask
			|	TxBD::Status::Ethernet::CSL::FieldMask
			|	TxBD::Status::Ethernet::HB::FieldMask
			);
	if (!(descStatus & errorStatusMask)){
		return false;
		}

	const unsigned	stopStatusMask	=
		(		TxBD::Status::Ethernet::UN::FieldMask
			|	TxBD::Status::Ethernet::RL::FieldMask
			|	TxBD::Status::Ethernet::LC::FieldMask
			);
	bool	stopStatus	= descStatus & stopStatusMask;

	if(descStatus & TxBD::Status::Ethernet::RC::FieldMask){
		unsigned	nRetries	=
			(		(descStatus & TxBD::Status::Ethernet::RC::FieldMask)
				>>	TxBD::Status::Ethernet::RC::Lsb
				);
		_stats.nRetries	+= nRetries;
		}
	if(descStatus & TxBD::Status::Ethernet::CSL::FieldMask){
		++_stats.carrierSenseLost;
		}

	if(descStatus & TxBD::Status::Ethernet::UN::FieldMask){
		++_stats.nUnderrun;
		}
	if(descStatus & TxBD::Status::Ethernet::RL::FieldMask){
		++_stats.nRetryLimitExceeded;
		}
	if(descStatus & TxBD::Status::Ethernet::LC::FieldMask){
		++_stats.nLateCollision;
		}
	if(descStatus & TxBD::Status::Ethernet::HB::FieldMask){
		++_stats.nHeartBeatsMissed;
		}
	return stopStatus;
	}

void	Driver::restart() noexcept{
	++_stats.sdmaRestarts;
	_cpcr.restartTx();
	}
