/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _drv_motorola_mpc8xx_cache_sprh_
#define _drv_motorola_mpc8xx_cache_sprh_
#include "oscl/hw/motorola/mpc8xx/cache/iccst.h"
#include "oscl/hw/motorola/mpc8xx/cache/dccst.h"

/** */
namespace Oscl {
/** */
namespace Mot8xx {
/** */
namespace Cache {

/** */
inline void writeIC_CST(IcCst::Reg reg) noexcept{
    asm volatile("mtspr 560,%0" : : "r" (reg));
    }

/** */
inline IcCst::Reg readIC_CST() noexcept{
	IcCst::Reg	reg;
    asm volatile("mfspr %0,560" : "=r" (reg));
	return reg;
    }

/** */
inline void writeDC_CST(DcCst::Reg reg) noexcept{
    asm volatile("mtspr 568,%0" : : "r" (reg));
    }

/** */
inline DcCst::Reg readDC_CST() noexcept{
	DcCst::Reg	reg;
    asm volatile("mfspr %0,568" : "=r" (reg));
	return reg;
    }

}
}
}

#endif

