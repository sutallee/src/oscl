/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _drv_motorola_mpc8xx_cache_iccsth_
#define _drv_motorola_mpc8xx_cache_iccsth_
#include "spr.h"
#include "oscl/hw/motorola/mpc8xx/cache/iccst.h"

/** */
namespace Oscl {
/** */
namespace Mot8xx {
/** */
namespace Cache {

/** */
class InstructionCacheControl {
	private:
		/** */
		IcCst::Reg		_regCopy;
	private:
		// Prevent heap allocation
		void*	operator new (size_t) noexcept;

	public:
		/** */
		InstructionCacheControl() noexcept{
			_regCopy	= readIC_CST();
			}
		/** */
		inline void	updateFromSpr() noexcept{
			_regCopy	= readIC_CST();
			}
		/** */
		inline bool	cacheEnabled() noexcept{
			return (_regCopy & IcCst::IEN::FieldMask)
						== IcCst::IEN::ValueMask_CacheEnabled;
			}
		/** */
		inline bool	loadAndLockBusError() noexcept{
			return (_regCopy & IcCst::CCER1::FieldMask)
						== IcCst::CCER1::ValueMask_BusError;
			}
		/** */
		inline bool	loadAndLockWayError() noexcept{
			return (_regCopy & IcCst::CCER2::FieldMask)
						== IcCst::CCER2::ValueMask_NoWayAvailable;
			}
		/** */
		inline void	enableCache() noexcept{
			writeIC_CST(IcCst::CMD::ValueMask_CacheEnable);
			}
		/** */
		inline void	disableCache() noexcept{
			writeIC_CST(IcCst::CMD::ValueMask_CacheDisable);
			}
		/** */
		inline void	loadAndLockCacheBlock() noexcept{
			writeIC_CST(IcCst::CMD::ValueMask_LoadAndLockCacheBlock);
			}
		/** */
		inline void	unlockCacheBlock() noexcept{
			writeIC_CST(IcCst::CMD::ValueMask_UnlockCacheBlock);
			}
		/** */
		inline void	unlockAll() noexcept{
			writeIC_CST(IcCst::CMD::ValueMask_UnlockAll);
			}
		/** */
		inline void	invalidateAll() noexcept{
			writeIC_CST(IcCst::CMD::ValueMask_InvalidateAll);
			}
	};

}
}
}

#endif

