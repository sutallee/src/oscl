/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _drv_motorola_mpc8xx_cache_dccsth_
#define _drv_motorola_mpc8xx_cache_dccsth_
#include "spr.h"
#include "oscl/hw/motorola/mpc8xx/cache/dccst.h"

/** */
namespace Oscl {
/** */
namespace Mot8xx {
/** */
namespace Cache {

/** */
class DataCacheControl {
	private:
		/** */
		DcCst::Reg		_regCopy;

	private:
		// Prevent heap allocation
		void*	operator new (size_t) noexcept;

	public:
		/** */
		DataCacheControl() noexcept{
			_regCopy	= readDC_CST();
			}
		/** */
		inline void	updateFromSpr() noexcept{
			_regCopy	= readDC_CST();
			}
		/** */
		inline bool	cacheEnabled() noexcept{
			return (_regCopy & DcCst::DEN::FieldMask)
						== DcCst::DEN::ValueMask_CacheEnabled;
			}
		/** */
		inline bool	loadAndLockBusError() noexcept{
			return (_regCopy & DcCst::CCER1::FieldMask)
						== DcCst::CCER1::ValueMask_BusError;
			}
		/** */
		inline bool	loadAndLockWayError() noexcept{
			return (_regCopy & DcCst::CCER2::FieldMask)
						== DcCst::CCER2::ValueMask_BusErrorOrNoWayAvailable;
			}
		/** */
		inline void	enableCache() noexcept{
			writeDC_CST(DcCst::CMD::ValueMask_CacheEnable);
			}
		/** */
		inline void	disableCache() noexcept{
			writeDC_CST(DcCst::CMD::ValueMask_CacheDisable);
			}
		/** */
		inline void	loadAndLockCacheBlock() noexcept{
			writeDC_CST(DcCst::CMD::ValueMask_LoadAndLockCacheBlock);
			}
		/** */
		inline void	unlockCacheBlock() noexcept{
			writeDC_CST(DcCst::CMD::ValueMask_UnlockCacheBlock);
			}
		/** */
		inline void	unlockAll() noexcept{
			writeDC_CST(DcCst::CMD::ValueMask_UnlockAll);
			}
		/** */
		inline void	invalidateAll() noexcept{
			writeDC_CST(DcCst::CMD::ValueMask_InvalidateAll);
			}
		/** */
		inline void	forceWriteThrough() noexcept{
			writeDC_CST(DcCst::CMD::ValueMask_SetForcedWriteThrough);
			}
		/** */
		inline void	writeThroughAccordingToMmu() noexcept{
			writeDC_CST(DcCst::CMD::ValueMask_ClearForcedWriteThrough);
			}
		/** */
		inline void	enableTrueLittleEndian() noexcept{
			writeDC_CST(DcCst::CMD::ValueMask_SetTrueLittleEndianSwap);
			}
		/** */
		inline void	disableTrueLittleEndian() noexcept{
			writeDC_CST(DcCst::CMD::ValueMask_ClearLittleEndianSwap);
			}
		/** */
		inline void	flushCacheBlock() noexcept{
			writeDC_CST(DcCst::CMD::ValueMask_FlushCacheBlock);
			}
	};

}
}
}

#endif

