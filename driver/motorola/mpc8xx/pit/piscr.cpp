/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "piscr.h"
#include "oscl/driver/powerpc/asm.h"

using namespace Oscl::Mot8xx::PIT;

Piscr::Piscr(	volatile Oscl::Mot8xx::SiTimer::PISCR::Reg&	piscr,
				volatile Oscl::Mot8xx::SitKey::PISCRK::Reg&	key,
				Oscl::Mot8xx::Siu::InterruptPriorityApi&	priority,
				bool										freezeStopsTimer
				) noexcept:
		_piscr(piscr),
		_key(key)
		{
	using namespace Oscl::Mot8xx::SiTimer::PISCR;
	Reg		freezeMask	= 	freezeStopsTimer
								?	(Reg)PITF::ValueMask_StopOnFreeze
								:	(Reg)PITF::ValueMask_IgnoreFreeze
								;
	key					= Oscl::Mot8xx::SitKey::PISCRK::Key;
	Ppc::eieio();
	_piscr	=		(priority.mask() << PIRQ::Lsb)
				|	PS::ValueMask_Clear
				|	PIE::ValueMask_Disable
				|	freezeMask
				|	PTE::ValueMask_Disable
				;
	Ppc::eieio();
	key		= 0;
	Ppc::eieio();
	}

bool	Piscr::interruptPending() noexcept{
	using namespace Oscl::Mot8xx::SiTimer::PISCR;
	bool	result	= _piscr & PS::ValueMask_Pending;
	Ppc::eieio();
	return result;
	}

void	Piscr::clearInterrupt() noexcept{
	using namespace Oscl::Mot8xx::SiTimer::PISCR;
	Reg		mask;
	mask	= _key;
	mask	= _piscr;
	mask	|=	(PS::ValueMask_Clear);
	Ppc::eieio();
	_key	= Oscl::Mot8xx::SitKey::PISCRK::Key;
	Ppc::eieio();
	_piscr	= mask;
	Ppc::eieio();
	_key	= 0;
	Ppc::eieio();
	}

void	Piscr::enableInterrupt() noexcept{
	using namespace Oscl::Mot8xx::SiTimer::PISCR;
	Reg		mask;
	mask	= _piscr;
	mask	&=	~(PIE::FieldMask | PS::FieldMask);
	mask	|=	(PIE::ValueMask_Enable | PS::ValueMask_Clear);
	Ppc::eieio();
	_key	= Oscl::Mot8xx::SitKey::PISCRK::Key;
	Ppc::eieio();
	_piscr	= mask;
	Ppc::eieio();
	_key	= 0;
	Ppc::eieio();
	}

void	Piscr::disableInterrupt() noexcept{
	using namespace Oscl::Mot8xx::SiTimer::PISCR;
	Reg		mask;
	mask	= _piscr;
	mask	&=	~(PIE::FieldMask | PS::FieldMask);
	mask	|=	(PIE::ValueMask_Disable | PS::ValueMask_Clear);
	_key	= Oscl::Mot8xx::SitKey::PISCRK::Key;
	Ppc::eieio();
	_piscr	= mask;
	Ppc::eieio();
	_key	= 0;
	Ppc::eieio();
	}

void	Piscr::enableTimer() noexcept{
	using namespace Oscl::Mot8xx::SiTimer::PISCR;
	Reg		mask;
	mask	= _piscr;
	mask	&=	~(PTE::FieldMask | PS::FieldMask);
	mask	|=	(PTE::ValueMask_Enable | PS::ValueMask_Clear);
	_key	= Oscl::Mot8xx::SitKey::PISCRK::Key;
	Ppc::eieio();
	_piscr	= mask;
	Ppc::eieio();
	_key	= 0;
	Ppc::eieio();
	}

void	Piscr::disableTimer() noexcept{
	using namespace Oscl::Mot8xx::SiTimer::PISCR;
	Reg		mask;
	mask	= _piscr;
	mask	&=	~(PTE::FieldMask | PS::FieldMask);
	mask	|=	(PTE::ValueMask_Disable | PS::ValueMask_Clear);
	_key	= Oscl::Mot8xx::SitKey::PISCRK::Key;
	Ppc::eieio();
	_piscr	= mask;
	Ppc::eieio();
	_key	= 0;
	Ppc::eieio();
	}

void	Piscr::enableFreeze() noexcept{
	using namespace Oscl::Mot8xx::SiTimer::PISCR;
	Reg		mask;
	mask	= _piscr;
	mask	&=	~(PITF::FieldMask | PS::FieldMask);
	mask	|=	(PITF::ValueMask_StopOnFreeze | PS::ValueMask_Clear);
	_key	= Oscl::Mot8xx::SitKey::PISCRK::Key;
	Ppc::eieio();
	_piscr	= mask;
	Ppc::eieio();
	_key	= 0;
	Ppc::eieio();
	}

void	Piscr::disableFreeze() noexcept{
	using namespace Oscl::Mot8xx::SiTimer::PISCR;
	Reg		mask;
	mask	= _piscr;
	mask	&=	~(PITF::FieldMask | PS::FieldMask);
	mask	|=	(PITF::ValueMask_IgnoreFreeze | PS::ValueMask_Clear);
	_key	= Oscl::Mot8xx::SitKey::PISCRK::Key;
	Ppc::eieio();
	_piscr	= mask;
	Ppc::eieio();
	_key	= 0;
	Ppc::eieio();
	}

