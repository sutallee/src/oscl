/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_mot_mpc8xx_pit_driverh_
#define _oscl_drv_mot_mpc8xx_pit_driverh_
#include "api.h"
#include "oscl/hw/motorola/mpc8xx/sitimerreg.h"
#include "oscl/hw/motorola/mpc8xx/sitkeyreg.h"
#include "oscl/driver/motorola/mpc8xx/siu/priority.h"
#include "oscl/interrupt/shandler.h"
#include "oscl/queue/queue.h"
#include "oscl/queue/queueitem.h"
#include "piscr.h"

/** */
namespace Oscl {
/** */
namespace Mot8xx {
/** */
namespace PIT {

/** */
class Driver : public Api {
	private:
		/** */
		Piscr					_piscr;

		/** */
		Oscl::Queue<Observer>	_observers;

	public:
		/** */
		Driver(	Oscl::Mot8xx::Siu::InterruptPriorityApi&	priority,
				volatile Oscl::Mot8xx::SitKey::PISCRK::Reg&	key,
				volatile Oscl::Mot8xx::SiTimer::PISCR::Reg&	piscr,
				volatile Oscl::Mot8xx::SiTimer::PITC::Reg&	pitc,
				volatile Oscl::Mot8xx::SiTimer::PITR::Reg&	pitr,
				Oscl::Mot8xx::SiTimer::PITC::Reg			period
				) noexcept;

		/** */
		void	start() noexcept;

		/** */
		void	stop() noexcept;

		/** */
		void	attach(Observer& observer) noexcept;

		/** */
		void	detach(Observer& observer) noexcept;

	private:
		/** */
		bool	interrupt() noexcept;
	};

}
}
}

#endif
