/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "driver.h"

using namespace Oscl::Mot8xx::PIT;

Driver::Driver(	Oscl::Mot8xx::Siu::InterruptPriorityApi&	priority,
				volatile Oscl::Mot8xx::SitKey::PISCRK::Reg&	key,
				volatile Oscl::Mot8xx::SiTimer::PISCR::Reg&	piscr,
				volatile Oscl::Mot8xx::SiTimer::PITC::Reg&	pitc,
				volatile Oscl::Mot8xx::SiTimer::PITR::Reg&	pitr,
				Oscl::Mot8xx::SiTimer::PITC::Reg			period
				) noexcept:
		_piscr(piscr,key,priority,true)
		{
	pitc	= period;
	}

void	Driver::start() noexcept{
	_piscr.disableTimer();
	_piscr.disableInterrupt();
	_piscr.clearInterrupt();
	_piscr.enableInterrupt();
	_piscr.enableTimer();
	}

void	Driver::stop() noexcept{
	_piscr.disableInterrupt();
	_piscr.disableTimer();
	_piscr.clearInterrupt();
	}

void	Driver::attach(Observer& observer) noexcept{
	_observers.put(&observer);
	}

void	Driver::detach(Observer& observer) noexcept{
	_observers.remove(&observer);
	}

bool	Driver::interrupt() noexcept{
	using namespace Oscl::Mot8xx::SiTimer;
	if(!_piscr.interruptPending()){
		return false;
		}
	_piscr.clearInterrupt();
	Observer*	observer;
	for(	observer=_observers.first();
			observer;
			observer=_observers.next(observer)
			){
		observer->tick();
		}
	return true;
	}

