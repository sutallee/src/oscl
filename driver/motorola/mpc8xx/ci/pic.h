/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_mot_mpc8xx_ci_pich_
#define _oscl_drv_mot_mpc8xx_ci_pich_
#include "picapi.h"
#include "irqapi.h"
#include "oscl/hw/motorola/mpc8xx/cpmicreg.h"

/** */
namespace Oscl {
/** */
namespace Mot8xx {
/** */
namespace CI {

/** */
class PIC : public PicApi{
	private:
		/** */
		volatile Oscl::Mot8xx::Cpmic::CIVR::Reg&	_civr;
		/** */
		enum{maxVectors=0x20};
		/** */
		IrqApi*										_handler[maxVectors];
	public:
		/** */
		PIC(	volatile Oscl::Mot8xx::Cpmic::CIVR::Reg&	civr,
				IrqApi&										vect00,
				IrqApi&										vect01,
				IrqApi&										vect02,
				IrqApi&										vect03,
				IrqApi&										vect04,
				IrqApi&										vect05,
				IrqApi&										vect06,
				IrqApi&										vect07,
				IrqApi&										vect08,
				IrqApi&										vect09,
				IrqApi&										vect0A,
				IrqApi&										vect0B,
				IrqApi&										vect0C,
				IrqApi&										vect0D,
				IrqApi&										vect0E,
				IrqApi&										vect0F,
				IrqApi&										vect10,
				IrqApi&										vect11,
				IrqApi&										vect12,
				IrqApi&										vect13,
				IrqApi&										vect14,
				IrqApi&										vect15,
				IrqApi&										vect16,
				IrqApi&										vect17,
				IrqApi&										vect18,
				IrqApi&										vect19,
				IrqApi&										vect1A,
				IrqApi&										vect1B,
				IrqApi&										vect1C,
				IrqApi&										vect1D,
				IrqApi&										vect1E,
				IrqApi&										vect1F
				) noexcept;

	public:	// PicApi
		/** */
		void	reserve(	unsigned vector,
							Oscl::Interrupt::StatusHandlerApi&	handler
							) noexcept;
		/** */
		void	release(unsigned vector) noexcept;

		/** */
		IrqApi&	getIrqApi(unsigned vector) noexcept;

		/** */
		bool	interrupt() noexcept;
	};

}
}
}

#endif
