/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_mot_mpc8xx_ci_sr_bitinth_
#define _oscl_drv_mot_mpc8xx_ci_sr_bitinth_
#include "interrupt.h"
#include "bit.h"

/** */
namespace Oscl {
/** */
namespace Mot8xx {
/** */
namespace CI {
/** */
namespace SR {

/** */
template <unsigned BitNum>
class BitInterrupt : public Oscl::Mot8xx::CI::SR::Interrupt {
	private:
		/** */
		Oscl::Mot8xx::CI::SR::Bit<BitNum>	_cisrBit;

	public:
		/** */
		BitInterrupt(	Oscl::Interrupt::StatusHandlerApi&			isr,
						volatile Oscl::Mot8xx::Cpmic::CISR::Reg&	reg
						) noexcept:
			Oscl::Mot8xx::CI::SR::Interrupt(isr,_cisrBit),
			_cisrBit(reg){}
		/** */
		BitInterrupt(	volatile Oscl::Mot8xx::Cpmic::CISR::Reg&	reg
						) noexcept:
			Oscl::Mot8xx::CI::SR::Interrupt(_cisrBit),
			_cisrBit(reg){}
	};

}
}
}
}

#endif
