/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "pic.h"
#include "oscl/error/fatal.h"

using namespace Oscl::Mot8xx::CI;

PIC::PIC(	volatile Oscl::Mot8xx::Cpmic::CIVR::Reg&	civr,
			IrqApi&										vect00,
			IrqApi&										vect01,
			IrqApi&										vect02,
			IrqApi&										vect03,
			IrqApi&										vect04,
			IrqApi&										vect05,
			IrqApi&										vect06,
			IrqApi&										vect07,
			IrqApi&										vect08,
			IrqApi&										vect09,
			IrqApi&										vect0A,
			IrqApi&										vect0B,
			IrqApi&										vect0C,
			IrqApi&										vect0D,
			IrqApi&										vect0E,
			IrqApi&										vect0F,
			IrqApi&										vect10,
			IrqApi&										vect11,
			IrqApi&										vect12,
			IrqApi&										vect13,
			IrqApi&										vect14,
			IrqApi&										vect15,
			IrqApi&										vect16,
			IrqApi&										vect17,
			IrqApi&										vect18,
			IrqApi&										vect19,
			IrqApi&										vect1A,
			IrqApi&										vect1B,
			IrqApi&										vect1C,
			IrqApi&										vect1D,
			IrqApi&										vect1E,
			IrqApi&										vect1F
			) noexcept:
		_civr(civr)
		{
	_handler[0x00]=&vect00;
	_handler[0x01]=&vect01;
	_handler[0x02]=&vect02;
	_handler[0x03]=&vect03;
	_handler[0x04]=&vect04;
	_handler[0x05]=&vect05;
	_handler[0x06]=&vect06;
	_handler[0x07]=&vect07;
	_handler[0x08]=&vect08;
	_handler[0x09]=&vect09;
	_handler[0x0A]=&vect0A;
	_handler[0x0B]=&vect0B;
	_handler[0x0C]=&vect0C;
	_handler[0x0D]=&vect0D;
	_handler[0x0E]=&vect0E;
	_handler[0x0F]=&vect0F;
	_handler[0x10]=&vect10;
	_handler[0x11]=&vect11;
	_handler[0x12]=&vect12;
	_handler[0x13]=&vect13;
	_handler[0x14]=&vect14;
	_handler[0x15]=&vect15;
	_handler[0x16]=&vect16;
	_handler[0x17]=&vect17;
	_handler[0x18]=&vect18;
	_handler[0x19]=&vect19;
	_handler[0x1A]=&vect1A;
	_handler[0x1B]=&vect1B;
	_handler[0x1C]=&vect1C;
	_handler[0x1D]=&vect1D;
	_handler[0x1E]=&vect1E;
	_handler[0x1F]=&vect1F;
	}

void	PIC::reserve(	unsigned vector,
						Oscl::Interrupt::StatusHandlerApi& handler
						) noexcept{
	if(vector > (maxVectors-1)){
		Oscl::ErrorFatal::
		logAndExit("PIC: attempt to reserve illegal vector.\n");
		}
	_handler[vector]->reserve(handler);
	}

void	PIC::release(unsigned vector) noexcept{
	if(vector > (maxVectors-1)){
		Oscl::ErrorFatal::
		logAndExit("PIC: attempt to release illegal vector.\n");
		}
	_handler[vector]->release();
	}

IrqApi&	PIC::getIrqApi(unsigned vector) noexcept{
	if(vector > (maxVectors-1)){
		Oscl::ErrorFatal::
		logAndExit("PIC: attempt to get illegal vector.\n");
		}
	return *_handler[vector];
	}

bool	PIC::interrupt() noexcept{
	using namespace Oscl::Mot8xx::Cpmic::CIVR;
	_civr	= IACK::ValueMask_ACK;
	return _handler[(_civr & VN::FieldMask)>>VN::Lsb]->interrupt();
	}

