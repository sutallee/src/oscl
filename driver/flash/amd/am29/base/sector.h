/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_flash_amd_am29_base_sectorh_
#define _oscl_drv_flash_amd_am29_base_sectorh_
#include <stdint.h>
#include "oscl/cpu/sync.h"

/** */
namespace Oscl {
/** */
namespace Drv {
/** */
namespace Flash {
/** */
namespace AMD {
/** */
namespace AM29 {
/** */
namespace Base {

/** */
class Pause {
	public:
		/** */
		virtual ~Pause() {}
		/** */
		virtual void	pause() noexcept=0;
	};

/** */
class Sector {
	private:
		/** */
		unsigned char* const	_base;
		/** */
		const unsigned short	_offset;
		/** */
		const unsigned short	_width;
		/** */
		unsigned char			_statusA;
		/** */
		unsigned char			_statusB;

	public:
		/** Base is the address of the sector, not accounting
			for the offset.
			Offset is the "lane-number" on a bus
			that is n-bytes wide.
			Width is the width of the bus.
		 */
		Sector(	void*			base,
				unsigned short	offset,
				unsigned short	width
				) noexcept;

		/** */
		inline void	unlock() noexcept{
			const unsigned	offset0x555	= (0x0555*_width)+_offset;
			const unsigned	offset0x2AA	= (0x02AA*_width)+_offset;
			_base[offset0x555]	= 0xAA;
			OsclCpuInOrderMemoryAccessBarrier();
			_base[offset0x2AA]	= 0x55;
			OsclCpuInOrderMemoryAccessBarrier();
			}

		/** */
		inline void	setupByteProgram() noexcept{
			const unsigned	offset0x555	= (0x0555*_width)+_offset;
			_base[offset0x555]	= 0xA0;
			OsclCpuInOrderMemoryAccessBarrier();
			}

		/** */
		inline void	setupErase() noexcept{
			const unsigned	offset0x555	= (0x0555*_width)+_offset;
			const unsigned	offset0x2AA	= (0x02AA*_width)+_offset;
			_base[offset0x555]	= 0x80;
			OsclCpuInOrderMemoryAccessBarrier();
			_base[offset0x555]	= 0xAA;
			OsclCpuInOrderMemoryAccessBarrier();
			_base[offset0x2AA]	= 0x55;
			OsclCpuInOrderMemoryAccessBarrier();
			}

		/** */
		inline void	updateStatus() noexcept{
			volatile unsigned char*	status	= &_base[_offset];
			_statusA	= *status;
			OsclCpuInOrderMemoryAccessBarrier();
			_statusB	= *status;
			OsclCpuInOrderMemoryAccessBarrier();
			}

		/** */
		inline bool	toggling() noexcept{
			return ((_statusA ^ _statusB) & 0x40);
			}

		/** */
		inline bool	timingLimitExceeded() noexcept{
			return (toggling() && (_statusB & 0x20));
			}

		/** */
		inline void	program(	unsigned		paOffset,
								unsigned char	pd
								) noexcept{
			unlock();
			setupByteProgram();
			_base[(paOffset*_width)+_offset]	= pd;
			OsclCpuInOrderMemoryAccessBarrier();
			}

		/** */
		inline void	erase() noexcept{
			unlock();
			setupErase();
			_base[_offset]	= 0x30;
			OsclCpuInOrderMemoryAccessBarrier();
			}

		/** Returns true if the programming fails.
			Busy waits until program completes.
		 */
		bool	programAndWait(	unsigned		paOffset,
								unsigned char	pd
								) noexcept;

		/** Returns true if the programming fails.
			Invokes pause each time throught the status
			wait loop.
		 */
		bool	programAndWait(	Pause&			pause,
								unsigned		paOffset,
								unsigned char	pd
								) noexcept;

		/** Returns true if the erase fails.
			Busy waits until program completes.
		 */
		bool	eraseAndWait() noexcept;

		/** Returns true if the erase fails.
			Invokes pause each time throught the status
			wait loop.
		 */
		bool	eraseAndWait(Pause& pause) noexcept;

	};

}
}
}
}
}
}

#endif
