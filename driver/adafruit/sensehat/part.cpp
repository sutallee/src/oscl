/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/
#include "part.h"
#include "oscl/hw/st/lsm9ds1/i2c.h"
#include "oscl/hw/st/lps25h/i2c.h"

using namespace Oscl::Driver::Adafruit::SenseHat;

static constexpr bool	sdo_ag	= false;
static constexpr bool	sdo_m	= false;
static constexpr bool	sa0		= false;

Part::Part(
	Oscl::Mt::Itc::PostMsgApi&				papi,
	Oscl::Mt::Itc::PostMsgApi&				lsm9ds1Papi,
	Oscl::Mt::Itc::PostMsgApi&				lps25hPapi,
	Oscl::I2C::Master::SevenBit::Api&		i2cSevenBitApi,
	Oscl::Event::Observer::Req::Api::SAP&	lsm9ds1AccelerometerGyroPeriodicSAP,
	Oscl::Event::Observer::Req::Api::SAP&	lsm9ds1MagnetometerPeriodicSAP,
	Oscl::Event::Observer::Req::Api::SAP&	lps25hPeriodicSAP
	) noexcept:
		_agI2CDevice(
			i2cSevenBitApi,
			Oscl::HW::ST::LSM9DS1::I2C::accelerometerGyroAddress<sdo_ag>
			),
		_agI2CAdapter(_agI2CDevice),
		_magI2CDevice(
			i2cSevenBitApi,
			Oscl::HW::ST::LSM9DS1::I2C::magnetometerAddress<sdo_m>
			),
		_magI2CAdapter(_magI2CDevice),
		_lms9DS1Driver(
			lsm9ds1Papi,
			_agI2CAdapter,
			_magI2CAdapter,
			lsm9ds1AccelerometerGyroPeriodicSAP,
			lsm9ds1MagnetometerPeriodicSAP
			),
		_pressureI2CDevice(
			i2cSevenBitApi,
			Oscl::HW::ST::LPS25H::I2C::sensorAddress<sa0>
			),
		_pressureI2CAdapter(_pressureI2CDevice),
		_lps25hDriver(
			lps25hPapi,
			_pressureI2CAdapter,
			lps25hPeriodicSAP
			)
		{
	}

Oscl::XYZ::Observer::Req::Api::SAP&	Part::getAccelerometerSAP() noexcept{
	return _lms9DS1Driver.getAccelSAP();
	}

Oscl::XYZ::Observer::Req::Api::SAP&	Part::getGyroSAP() noexcept{
	return _lms9DS1Driver.getGyroSAP();
	}

Oscl::Double::Observer::Req::Api::SAP&	Part::getGyroTemperatureSAP() noexcept{
	return _lms9DS1Driver.getTemperatureSAP();
	}

Oscl::XYZ::Observer::Req::Api::SAP&	Part::getMagnetometerSAP() noexcept{
	return _lms9DS1Driver.getMagnetometerSAP();
	}

Oscl::Double::Observer::Req::Api::SAP&	Part::getPressureSAP() noexcept{
	return _lps25hDriver.getPressureSAP();
	}

void	Part::start() noexcept{
	_lms9DS1Driver.getOpenSyncApi().syncOpen();

	_lps25hDriver.getOpenSyncApi().syncOpen();
	}

