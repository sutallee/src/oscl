/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_driver_adafruit_sensehat_parth_
#define _oscl_driver_adafruit_sensehat_parth_

#include "oscl/i2c/device/sevenbit/adapter.h"
#include "oscl/driver/st/lsm9ds1/ag/i2c/adapter.h"
#include "oscl/driver/st/lsm9ds1/mag/i2c/adapter.h"
#include "oscl/driver/st/lsm9ds1/service.h"
#include "oscl/driver/st/lps25h/service.h"
#include "oscl/driver/st/lps25h/i2c/adapter.h"

/** */
namespace Oscl {
/** */
namespace Driver {
/** */
namespace Adafruit {
/** */
namespace SenseHat {

/** This part is intended to contain a driver subsystem
	that supports "all" of the Adafruit Sense Hat devices.

	It will probably never support the 8x8 LED matrix,
	since that is automatically supported as a frame-buffer
	device (/dev/fb1) under Linux.

	Similarly, it will probably never support the joystick
	as that is automatically supported as a Linux input
	device (/dev/input/event0).
 */
class Part {
	private:
		/** */
		Oscl::I2C::Device::SevenBit::Adapter			_agI2CDevice;

		/** */
		Oscl::Driver::ST::LSM9DS1::AG::I2C::Adapter		_agI2CAdapter;

	private:
		/** */
		Oscl::I2C::Device::SevenBit::Adapter			_magI2CDevice;

		/** */
		Oscl::Driver::ST::LSM9DS1::MAG::I2C::Adapter	_magI2CAdapter;

	private:
		/** */
		Oscl::Driver::ST::LSM9DS1::Service				_lms9DS1Driver;

	private:
		/** */
		Oscl::I2C::Device::SevenBit::Adapter			_pressureI2CDevice;

		/** */
		Oscl::Driver::ST::LPS25H::I2C::Adapter			_pressureI2CAdapter;

	private:
		/** */
		Oscl::Driver::ST::LPS25H::Service				_lps25hDriver;

	public:
		/** For flexibility, the constructor supports multiple
			PostMsgApi for each driver, but they *may* all refer
			to the same thread.
			Likewise, each major driver can have its own periodic SAP
			that determines the driver's polling rate. These may all
			refer to the same periodic SAP. This driver sub-system
			was tested with a single 100ms periodic SAP.
		 */
		Part(
			Oscl::Mt::Itc::PostMsgApi&				papi,
			Oscl::Mt::Itc::PostMsgApi&				lsm9ds1Papi,
			Oscl::Mt::Itc::PostMsgApi&				lps25hPapi,
			Oscl::I2C::Master::SevenBit::Api&		i2cSevenBitApi,
			Oscl::Event::Observer::Req::Api::SAP&	lsm9ds1AccelerometerGyroPeriodicSAP,
			Oscl::Event::Observer::Req::Api::SAP&	lsm9ds1MagnetometerPeriodicSAP,
			Oscl::Event::Observer::Req::Api::SAP&	lps25hPeriodicSAP
			) noexcept;

		/** The 3-axis accelerometer data in Gs.
		 */
		Oscl::XYZ::Observer::Req::Api::SAP&	getAccelerometerSAP() noexcept;

		/** The 3-axis gyroscope data in degrees-per-second.
		 */
		Oscl::XYZ::Observer::Req::Api::SAP&	getGyroSAP() noexcept;

		/** The Gyro temperature sensor in Celsius.
		 */
		Oscl::Double::Observer::Req::Api::SAP&	getGyroTemperatureSAP() noexcept;

		/** The 3-axis Magnetometer sensor in gauss.
		 */
		Oscl::XYZ::Observer::Req::Api::SAP&	getMagnetometerSAP() noexcept;

		/** The LPS25H pressure sensor in hPa (hundred Pascals).
		 */
		Oscl::Double::Observer::Req::Api::SAP&	getPressureSAP() noexcept;

		/** */
		void	start() noexcept;
	};

}
}
}
}

#endif
