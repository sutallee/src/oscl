/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "creator.h"
#include "oscl/interrupt/null.h"
#include "oscl/hw/est/mdp860p/pci/map.h"

volatile int xDummy=0;

Oscl::Interrupt::HandlerApi* ExternalInterruptHandler;

extern Oscl::Krux::Sema::BroadcastBinary	UKClockSema;

using namespace Oscl::Est::Mdp860PPro;

void	SystemTimer::tick() noexcept{
	UKClockSema.suSignal();
	}

static Oscl::Interrupt::NullHandler				nullHandler;

Creator::Creator(	const Oscl::Protocol::
					IEEE::MacAddress&		scc1EnetStationAddress,
					Oscl::Mt::
					Mutex::Simple::Api&		mutex,
					unsigned				rxBufferSize,
					unsigned				maximumFrameLength,
					unsigned				bridgeOidType,
					unsigned				slotOidType,
					unsigned				functionOidType,
					unsigned				bridgeID
					) noexcept:
		_bitPortB(Mpc860PQUICC.portb.pbdat,0),
		_mutexedBitPortB(_bitPortB,mutex),
		_ledControlPort(	Mdp8xxProDiscrete.
							eightBitPort.
							discrete.
							control.ledControl,
							0
							),
		_mutexedLedControlPort(_ledControlPort,mutex),
		_mdp860P(Mpc860PQUICC,QspanPciBridge,mutex),
		_scc1Enet(	_mdp860P.getMC68160(),
					_mdp860P.getMpc860P().getSICR(),
					_mdp860P.getMpc860P().getPortA(),
					_mdp860P.getMpc860P().getPortB(),
					_mdp860P.getMpc860P().getPortC(),
					_mdp860P.getMpc860P().getDPRAM(),
					_mdp860P.getMpc860P().getScc1CpcrApi(),
					Mpc860PQUICC.scc1,
					Mpc860PQUICC.pram1.enet.scc1,
					scc1EnetStationAddress,
					rxBufferSize,
					maximumFrameLength
					),
		_smc1Serial(	_mdp860P.getMpc860P().getSIMODE(),
						_mdp860P.getMpc860P().getPortB(),
						_mdp860P.getMpc860P().getDPRAM(),
						_mdp860P.getMpc860P().getSmc1CpcrApi(),
						Mpc860PQUICC.smc1,
						Mpc860PQUICC.pram3.other.smc1
					),
		_smc2Serial(	_mdp860P.getMpc860P().getSIMODE(),
						_mdp860P.getMpc860P().getPortB(),
						_mdp860P.getMpc860P().getDPRAM(),
						_mdp860P.getMpc860P().getSmc2CpcrApi(),
						Mpc860PQUICC.smc2,
						Mpc860PQUICC.pram4.other.smcpip.smc2
					),
		_cpmPicHandler(_mdp860P.getMpc860P().getPicApi().getISR()),
		_pitHandler(_mdp860P.getMpc860P().getPitApi()),
		_pciFuncAdvServer(),
		_pciFuncServer(),
		_pciFuncAdv(_pciFuncAdvServer),
		_pciEnumService(	_mdp860P.getPciConfigApi(),
							(unsigned long)Mdp8xxPCI,
							Mdp8xxPciSize,
							_pciFuncServer,
							_pciFuncAdvServer,
							_pciFuncAdv,
							_pciFuncAdv,
							_mdp860P.getPciIntSourceApi(),	// FIXME:
															// Interrupt routing
															// is dependent on
															// the kernel. Thus
															// this should be
															// passed in
															// throught the
															// constructor.
							bridgeOidType,
							slotOidType,
							functionOidType,
							bridgeID
							),
		_delayServer(100)	// FIXME: need to correlate with PIT init.
		{
	// Set QSPAN-IMSEL line Low
	_mdp860P.getMpc860P().getPortA().getPA12().reserveOutput().low();
	Oscl::Interrupt::HandlerApi&	picIsr = _mdp860P.
												getMpc860P().
												getSiuPicApi();
	ExternalInterruptHandler	= &picIsr;
	_mdp860P.getMpc860P().getPitApi().attach(_delayServer);
	}

void	Creator::initialize() noexcept{
	using namespace Oscl::Mot8xx::Cpmic;

	_mdp860P.getMpc860P().getPicApi().getSCC1().reserve(_scc1Enet.getISR());
	_mdp860P.getMpc860P().getPicApi().getSMC1().reserve(_smc1Serial.getISR());
	_mdp860P.getMpc860P().getPicApi().getSMC2().reserve(_smc2Serial.getISR());

	Oscl::Mot8xx::Siu::PicApi&	pic	= _mdp860P.getMpc860P().getSiuPicApi();

    pic.getInternalLevel0SourceApi().attach(_pitHandler);
    pic.getInternalLevel4SourceApi().attach(_cpmPicHandler);
	pic.getIrqDriverApi1().levelMode();
	pic.getIrqDriverApi1().disableLowPowerWakeUp();
	pic.getIrqDriverApi1().enable();
	_mdp860P.getMpc860P().getPitApi().attach(_pitSysTimer);
	_mdp860P.getMpc860P().getPitApi().start();
	Mpc860PQUICC.cpmic.cicr.changeBits(~0,
											CICR::SCdP::ValueMask_SCC1
										|	CICR::SCcP::ValueMask_SCC1
										|	CICR::SCbP::ValueMask_SCC1
										|	CICR::SCaP::ValueMask_SCC1
										|	CICR::IRL::ValueMask_Typical
										|	CICR::HP::ValueMask_PC15
										|	CICR::IEN::ValueMask_Enable
										|	CICR::SPS::ValueMask_Grouped
										);
	Mpc860PQUICC.cpmic.cimr.changeBits(	CIMR::SCC1::FieldMask,
										CIMR::SCC1::ValueMask_Enable
										);
	Mpc860PQUICC.cpmic.cimr.changeBits(	CIMR::SMC1::FieldMask,
										CIMR::SMC1::ValueMask_Enable
										);
	Mpc860PQUICC.cpmic.cimr.changeBits(	CIMR::SMC2::FieldMask,
										CIMR::SMC2::ValueMask_Enable
										);
	_mdp860P.getMC68160().reset();
	_scc1Enet.syncOpen();
	///////// BRG1/SMC1
	_mdp860P.getMpc860P().getBRGC1().getRST().selectReset();
	for(unsigned i=0;i<200;++i){ xDummy	= i; }
	_mdp860P.getMpc860P().getBRGC1().getEXTC().selectBRGCLK();
	for(unsigned i=0;i<200;++i){ xDummy	= i; }
	_mdp860P.getMpc860P().getBRGC1().getATB().selectNormal();
	for(unsigned i=0;i<200;++i){ xDummy	= i; }
	_mdp860P.getMpc860P().getBRGC1().getDIV().selectDivideBy1();
	for(unsigned i=0;i<200;++i){ xDummy	= i; }
	// FIXME: 9600
	_mdp860P.getMpc860P().getBRGC1().getCD().setDivisor((50000000)/(16*9600));
	for(unsigned i=0;i<200;++i){ xDummy	= i; }
	_mdp860P.getMpc860P().getBRGC1().getRST().selectEnable();
	for(unsigned i=0;i<200;++i){ xDummy	= i; }
	_mdp860P.getMpc860P().getBRGC1().getEN().selectInputClockEnable();
	for(unsigned i=0;i<200;++i){ xDummy	= i; }
	_smc1Serial.syncOpen();
	///////// BRG2/SMC2
	_mdp860P.getMpc860P().getBRGC2().getRST().selectReset();
	for(unsigned i=0;i<200;++i){ xDummy	= i; }
	_mdp860P.getMpc860P().getBRGC2().getEXTC().selectBRGCLK();
	for(unsigned i=0;i<200;++i){ xDummy	= i; }
	_mdp860P.getMpc860P().getBRGC2().getATB().selectNormal();
	for(unsigned i=0;i<200;++i){ xDummy	= i; }
	_mdp860P.getMpc860P().getBRGC2().getDIV().selectDivideBy1();
	for(unsigned i=0;i<200;++i){ xDummy	= i; }
	// FIXME: 9600
	_mdp860P.getMpc860P().getBRGC2().getCD().setDivisor((50000000)/(16*9600));
	for(unsigned i=0;i<200;++i){ xDummy	= i; }
	_mdp860P.getMpc860P().getBRGC2().getRST().selectEnable();
	for(unsigned i=0;i<200;++i){ xDummy	= i; }
	_mdp860P.getMpc860P().getBRGC2().getEN().selectInputClockEnable();
	for(unsigned i=0;i<200;++i){ xDummy	= i; }
	_smc2Serial.syncOpen();
	}

Oscl::Interrupt::Shared::SourceApi&	Creator::getPciIrqSource() noexcept{
	return _mdp860P.getPciIntSourceApi();
	}

Oscl::Pci::Config::Api&	Creator::getPciConfigApi() noexcept{
	return _mdp860P.getPciConfigApi();
	}

Oscl::Est::Mdp860PPro::Api&		Creator::getMdp860P() noexcept{
	return _mdp860P;
	}

Oscl::Bits::
FieldApi<	Oscl::Est::Mdp8xxPro::
			LedControl::Reg
			>&						Creator::getLedControl() noexcept{
	return _mutexedLedControlPort;
	}

Oscl::Mot8xx::Siu::PicApi&			Creator::getSiuPIC() noexcept{
	return _mdp860P.getMpc860P().getSiuPicApi();
	}

Oscl::Mt::Runnable&					Creator::getScc1TxEnetRunnable() noexcept{
	return _scc1Enet.getTxDriverRunnable();
	}

Oscl::Frame::Pdu::Tx::Req::Api::SAP&	Creator::getScc1TxDriverSAP() noexcept{
	return _scc1Enet.getTxDriverSAP();
	}

Oscl::Mt::Runnable&					Creator::getScc1RxEnetRunnable() noexcept{
	return _scc1Enet.getRxDriverRunnable();
	}

Oscl::Frame::Pdu::Rx::
DriverApi&				Creator::getScc1RxDriver() noexcept{
	return _scc1Enet.getRxDriver();
	}

Oscl::Mt::Runnable&	Creator::getPciFuncAdvRunnable() noexcept{
	return _pciFuncAdvServer;
	}

Oscl::Mt::Runnable&	Creator::getPciFuncRunnable() noexcept{
	return _pciFuncServer;
	}

Oscl::Mt::Runnable&	Creator::getPciEnumRunnable() noexcept{
	return _pciEnumService;
	}

Oscl::Mt::Itc::Dyn::Adv::Cli::
SyncApi<	Oscl::PciSrv::
			Func::Server
			>&						Creator::getPciFuncAdvFind() noexcept{
	return _pciFuncAdv;
	}

Oscl::Mt::Itc::Dyn::Adv::Cli::
Req::Api<	Oscl::PciSrv::
			Func::Server
			>::SAP&					Creator::getPciFuncAdvSAP() noexcept{
	return _pciFuncAdv.getCliSAP();
	}

Oscl::Mt::Itc::Delay::Req::Api::SAP&	Creator::getDelayServiceSAP() noexcept{
	return _delayServer.getSAP();
	}

Oscl::Mt::Runnable&	Creator::getDelayServiceRunnable() noexcept{
	return _delayServer;
	}

Oscl::Mt::Runnable&		Creator::getSmc1TxRunnable() noexcept{
	return _smc1Serial.getTxDriverRunnable();
	}

Oscl::Stream::Output::Req::Api::SAP&	Creator::getSmc1SerialOutSAP() noexcept{
	return _smc1Serial.getTxDriverSAP();
	}

Oscl::Stream::Output::Api&			Creator::getSmc1OutApi() noexcept{
	return _smc1Serial.getTxSyncApi();
	}

Oscl::Mt::Runnable&					Creator::getSmc1RxRunnable() noexcept{
	return _smc1Serial.getRxDriverRunnable();
	}

Oscl::Stream::Input::Req::Api::SAP&		Creator::getSmc1SerialInSAP() noexcept{
	return _smc1Serial.getRxDriverSAP();
	}

Oscl::Stream::Input::Api&			Creator::getSmc1InApi() noexcept{
	return _smc1Serial.getRxSyncApi();
	}
///////
Oscl::Mt::Runnable&		Creator::getSmc2TxRunnable() noexcept{
	return _smc2Serial.getTxDriverRunnable();
	}

Oscl::Stream::Output::Req::Api::SAP&	Creator::getSmc2SerialOutSAP() noexcept{
	return _smc2Serial.getTxDriverSAP();
	}

Oscl::Stream::Output::Api&			Creator::getSmc2OutApi() noexcept{
	return _smc2Serial.getTxSyncApi();
	}

Oscl::Mt::Runnable&					Creator::getSmc2RxRunnable() noexcept{
	return _smc2Serial.getRxDriverRunnable();
	}

Oscl::Stream::Input::Req::Api::SAP&		Creator::getSmc2SerialInSAP() noexcept{
	return _smc2Serial.getRxDriverSAP();
	}

Oscl::Stream::Input::Api&			Creator::getSmc2InApi() noexcept{
	return _smc2Serial.getRxSyncApi();
	}

