/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_est_mdp860p_pci_ecos_bsp_basic_creatorh_
#define _oscl_drv_est_mdp860p_pci_ecos_bsp_basic_creatorh_
#include "oscl/memory/transapi.h"
#include "oscl/hw/pci/config.h"
#include "oscl/hw/est/mdp8xx/discrete.h"
#include "oscl/bits/port.h"
#include "oscl/bits/mt/mutexedbp.h"
#include "oscl/bits/mt/mutexedfield.h"
#include "oscl/driver/est/mdp860p/device/api.h"
#include "oscl/driver/motorola/mpc8xx/cp/cr/scc/scc1.h"
#include "oscl/driver/est/mdp860/smc1/serial.h"
#include "oscl/driver/est/mdp860/smc2/serial.h"
#include "oscl/driver/motorola/mpc8xx/ci/pic.h"
#include "oscl/driver/motorola/mpc8xx/ci/sr/bitint.h"
#include "pkgconf/kernel.h"
#include "cyg/kernel/intr.hxx"
#include "oscl/protocol/ieee/mac.h"

#include "oscl/mt/itc/dyn/adv/service.h"
#include "oscl/pcisrv/enum/server.h"

#include "oscl/driver/motorola/mpc8xx/pit/delay/server.h"
#include "oscl/interrupt/ecos/adaptor.h"

/** */
namespace Oscl {
/** */
namespace Est {
/** */
namespace Mdp860PPro {

/** */
class SystemTimer :	public Oscl::Mot8xx::PIT::Observer {
	private:
		/** */
		Cyg_Interrupt	_interrupt;

	private:	// Oscl::Mot8xx::PIT::Observer
		/** */
		void	tick() noexcept;

	public:
		/** */
		SystemTimer() noexcept;
	};

/** */
class Creator {
	private:
		/** */
		Oscl::Est::Mdp860PPro::Api&							_mdp860PPro;
		/** */
		Oscl::Bits::Port<Oscl::Mot8xx::Port::PBDAT::Reg>	_bitPortB;
		/** */
		Oscl::Bits::MutexedPort<Oscl::Mot8xx::Port::PBDAT::Reg>	
			_mutexedBitPortB;
		/** */
		Oscl::Bits::Port<Oscl::Est::Mdp8xxPro::LedControl::Reg>
			_ledControlPort;
		/** */
		Oscl::Bits::MutexedPort<Oscl::Est::Mdp8xxPro::LedControl::Reg>
			_mutexedLedControlPort;
		/** */
		Oscl::Mot8xx::CP::CR::SCC::SCC1			_scc1CPCR;
		/** */
		Oscl::Est::Mdp860::SMC1::Serial			_smc1Serial;
		/** */
		Oscl::Est::Mdp860::SMC2::Serial			_smc2Serial;
		/** */
		Oscl::Interrupt::eCos::Adaptor			_smc1IsrDsrAdaptor;
		/** */
		Oscl::Interrupt::eCos::Adaptor			_smc2IsrDsrAdaptor;
		/** */
		Oscl::Memory::Bus::NullTranslation		_memTranslator;
		/** */
		SystemTimer								_pitSysTimer;
        /** */
		Oscl::Interrupt::Shared::Handler		_cpmPicHandler;
        /** */
		Oscl::Interrupt::Shared::Handler		_pitHandler;
		/** */
		Oscl::Mt::Itc::Server					_pciFuncAdvServer;
		/** */
		Oscl::Mt::Itc::Server					_pciFuncServer;
		/** */
		Oscl::Mt::Itc::Dyn::Adv::
		Service<Oscl::PciSrv::Func::Server>		_pciFuncAdv;
		/** */
		Oscl::PciSrv::Enum::Server				_pciEnumService;
		/** */
		Oscl::Mot8xx::PIT::Delay::Server		_delayServer;

	public:
		/** */
		Creator(	Oscl::Est::Mdp860PPro::Api&		mdp860PPro,
					Oscl::Mt::Mutex::Simple::Api&	mutex
					) noexcept;
		/** This operation is invoked before the creator threads are started.
		 */
		void	initialize() noexcept;
		/** This operation is invoked after the creator threads are started.
		 */
		void	start() noexcept;
		/** */
		Oscl::Interrupt::Shared::SourceApi&	getPciIrqSource() noexcept;
		/** */
		Oscl::Pci::Config::Api&				getPciConfigApi() noexcept;
		/** */
		Oscl::Est::Mdp860PPro::Api&			getMdp860P() noexcept;
		/** */
		Oscl::Bits::
		FieldApi<	Oscl::Est::Mdp8xxPro::
					LedControl::Reg
					>&						getLedControl() noexcept;
		/** */
		Oscl::Mot8xx::Siu::PicApi&			getSiuPIC() noexcept;
		/** */
		Oscl::Mt::Runnable&					getPciDriverRunnable() noexcept;
		/** */
		Oscl::Mt::Runnable&					getPciFuncAdvRunnable() noexcept;
		/** */
		Oscl::Mt::Runnable&					getPciFuncRunnable() noexcept;
		/** */
		Oscl::Mt::Runnable&					getPciEnumRunnable() noexcept;
		/** */
		Oscl::Mt::Itc::Dyn::Adv::Cli::
		SyncApi<	Oscl::PciSrv::
					Func::Server
					>&						getPciFuncAdvFind() noexcept;
		/** */
		Oscl::Mt::Itc::Dyn::Adv::Cli::
		Req::Api<	Oscl::PciSrv::
					Func::Server
					>::SAP&					getPciFuncAdvSAP() noexcept;
		/** */
		Oscl::Mt::Itc::
		Delay::Req::Api::SAP&				getDelayServiceSAP() noexcept;
		/** */
		Oscl::Mt::Runnable&					getDelayServiceRunnable() noexcept;

		/** */
		Oscl::Mt::Runnable&					getSmc1TxRunnable() noexcept;
		/** */
		Oscl::Stream::Output::Req::Api::SAP&	getSmc1SerialOutSAP() noexcept;
		/** */
		Oscl::Stream::Output::Api&			getSmc1OutApi() noexcept;

		/** */
		Oscl::Mt::Runnable&					getSmc1RxRunnable() noexcept;
		/** */
		Oscl::Stream::Input::Req::Api::SAP&		getSmc1SerialInSAP() noexcept;
		/** */
		Oscl::Stream::Input::Api&			getSmc1InApi() noexcept;

		/** */
		Oscl::Mt::Runnable&					getSmc2TxRunnable() noexcept;
		/** */
		Oscl::Stream::Output::Req::Api::SAP&	getSmc2SerialOutSAP() noexcept;
		/** */
		Oscl::Stream::Output::Api&			getSmc2OutApi() noexcept;

		/** */
		Oscl::Mt::Runnable&					getSmc2RxRunnable() noexcept;
		/** */
		Oscl::Stream::Input::Req::Api::SAP&		getSmc2SerialInSAP() noexcept;
		/** */
		Oscl::Stream::Input::Api&			getSmc2InApi() noexcept;
	};

}
}
}

#endif
