/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_est_mdp860p_pci_ecos_bsp_basic_glue_ecosh_
#define _oscl_drv_est_mdp860p_pci_ecos_bsp_basic_glue_ecosh_
#include "cyg/kernel/kapi.h"
#include "oscl/driver/est/mdp860p/device/device.h"

/** */
extern Oscl::Est::Mdp860PPro::Device	theDevice;

extern "C" {

/** */
extern void	externalPic(void);
/** */
extern bool	scc1_eth_init(void);
/** */
extern int	fetch_scc1_enet_addr(unsigned char * buffer);
/** */
extern void mpc8xx_scc1_phy_init(void);
/** */
extern void mpc8xx_scc1_route_clocks(void);
/** */
extern void mpc8xx_pic_scc1_interrupt_attach(	cyg_addrword_t  data,
												cyg_ISR_t*      isr,
												cyg_DSR_t*      dsr
												);
}
#endif

