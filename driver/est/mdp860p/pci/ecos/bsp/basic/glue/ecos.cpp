/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <string.h>
#include <new>
#include "ecos.h"
#include "oscl/memory/block.h"
#include "oscl/interrupt/ecos/adaptor.h"
#include "oscl/interrupt/ecos/isrdsr.h"
#include "oscl/hw/est/mdp860p/pci/map.h"
#include "oscl/hw/motorola/mpc8xx/sitkeyreg.h"
#include "oscl/driver/powerpc/asm.h"
#include "oscl/driver/est/mdp8xx/pci/bsp/basic/pci.h"

// NOTE:
//	Network drivers are initialized at "NET_INIT", which is 
//  after "CYG_INIT_LIBC".
//

// Must initialize before "theDevice"
Oscl::Interrupt::eCos::
Adaptor	Scc1InterruptHandler
							CYGBLD_ATTRIB_INIT_BEFORE((CYG_INIT_INTERRUPTS-1));

// Must initialize before the SystemTimer is installed, and before
// the network or any other interrupts happen.
Oscl::Est::Mdp860PPro::Device	theDevice(	Mpc860PQUICC,
											QspanPciBridge
											)
								CYGBLD_ATTRIB_INIT_BEFORE(CYG_INIT_INTERRUPTS);

Oscl::Memory::
AlignedBlock<sizeof (Oscl::Interrupt::eCos::IsrDsr)>	eCosInterruptMem;

void mpc8xx_scc1_phy_init() {
	theDevice.getMC68160().reset();
	theDevice.getMC68160().selectTwistedPair();
	}

void mpc8xx_scc1_route_clocks(){
    Oscl::Mot860::SI::CR::SCC1Api&
	sicrSCC1    = theDevice.getMpc860P().getSICR().reserveSCC1();
    sicrSCC1.getGR().selectTxGrantAlwaysAsserted();
    sicrSCC1.getSC().selectNMSI();
    sicrSCC1.getTCS().selectCLK1();
    sicrSCC1.getRCS().selectCLK2();
	}

void mpc8xx_pic_scc1_interrupt_attach(	cyg_addrword_t  data,
										cyg_ISR_t*      isr,
										cyg_DSR_t*      dsr
										){
	Oscl::Interrupt::eCos::IsrDsr*
	isrdsr	= new(&eCosInterruptMem)
				Oscl::Interrupt::eCos::IsrDsr(data,isr,dsr);
	Scc1InterruptHandler.attach(*isrdsr);
	}

static void setupSIUMCR() noexcept{
	// FIXME: This routine demonstrates the
	// configuration needed to access the QSPAN
	// PCI bridge. It should be moved to a
	// platform specific location.
	// Chip selects must also be configured properly.
	// In particular the QSPAN configuration space
	// chip select must be set to enable BIH and
	// use external TA.
	// Known good value: 0x00610900
	using namespace Oscl::Mot8xx::Siu::SIUMCR;
	Mpc8xxQUICC.siu.siumcr	=
			EARB::ValueMask_InternalArbitration
		|	EARP::ValueMask_HighestPriority
		|	DSHW::ValueMask_DisableInternalDataShowCycles
		|	DBGC::ValueMask_VFLS
		|	DBPC::ValueMask_Mode00
		|	FRC::ValueMask_FRZ
		|	DLK::ValueMask_Locked
		|	OPAR::ValueMask_EvenParity
		|	PNCS::ValueMask_DisableParity
		|	DPC::ValueMask_IRQ
		|	MPRE::ValueMask_IRQ
		|	MLRC::ValueMask_KrRetry	// Required for QSPAN
		|	AEME::ValueMask_ExternalMasterDisabled
		|	SEME::ValueMask_ExternalMasterEnabled	// Required for QSPAN
		|	BSC::ValueMask_Dedicated
		|	GB5E::ValueMask_BDIP
		|	B2DD::ValueMask_CS2Only
		|	B3DD::ValueMask_CS3Only
		;
	}

static void setupTBSCR() noexcept{
	using namespace Oscl::Mot8xx;
	// The next three lines can be used to enable and
	// initialize the PPC decrementer. Currently,
	// I am using the PIT for its rate monotonic characteristics,
	// and thus have the decrementer and timebase disabled for now.
	// It seems to me that the decrementer would be useful for
	// time sharing multi-tasking, where rate monotonicity is
	// not required. I've disabled it to prevent the currently
	// unused interrupts it causes, since this is the only means
	// I can find of disabling the decrementer interrupts.
	Mpc8xxQUICC.sitkey.tbk		= SitKey::TBK::Key;
	Mpc8xxQUICC.sitimer.tbscr	= SiTimer::TBSCR::TBE::ValueMask_Disable;
	Ppc::mtdec(1000);
	}

static void setupSCCR() noexcept{
	using namespace Oscl::Mot8xx;
	Mpc8xxQUICC.clkrstkey.sccrk	= ClkRstKey::SCCRK::Key;
	Mpc8xxQUICC.clkrst.sccr		=
			ClkRst::SCCR::COM::ValueMask_FullStrength
		|	ClkRst::SCCR::TBS::ValueMask_OscClkDiv
		|	ClkRst::SCCR::RTDIV::ValueMask_DivideBy4
		|	ClkRst::SCCR::RTSEL::ValueMask_OSCM
		|	ClkRst::SCCR::CRQEN::ValueMask_HighFrequency	// Change
		|	ClkRst::SCCR::PRQEN::ValueMask_HighFrequency	// Change
		|	ClkRst::SCCR::EBDF::ValueMask_DivideBy1
		|	ClkRst::SCCR::DFSYNC::ValueMask_DivideBy1
		|	ClkRst::SCCR::DFBRG::ValueMask_DivideBy1
		|	ClkRst::SCCR::DFNL::ValueMask_DivideBy2
		|	ClkRst::SCCR::DFNH::ValueMask_DivideBy1
		;
	}

extern "C" void externalInit(){
	// This routine is executed before static constructors.
	setupSCCR();
	setupSIUMCR();
	setupTBSCR();
	initializePciBridge();
	}

void Oscl::Tundra::Qspan::Driver::catchBusError() noexcept{
//	Oscl::UKernThread::getCurrentKernelThread().catchBusError();
	// FIXME:
	// I assume that the QSPAN being used is one of the new
	// models that does NOT issue a bus error if a non existent
	// PCI device is polled during enumeration.
	return;
	}

bool Oscl::Tundra::Qspan::Driver::caughtBusError() noexcept{
//	return Oscl::UKernThread::getCurrentKernelThread().caughtBusError();
	// FIXME:
	return false;
	}

void	externalPic(){
	extern Oscl::Interrupt::HandlerApi* ExternalInterruptHandler;
	ExternalInterruptHandler->interrupt();
	}

static unsigned char etherAddr[] = { 0x00, 0x03, 0xE0, 0x12, 0x34, 0x56}; 
int fetch_scc1_enet_addr(unsigned char * buffer){
	const unsigned nBytes	= 8;
	memcpy(buffer,etherAddr,nBytes);
	return nBytes;
	}

