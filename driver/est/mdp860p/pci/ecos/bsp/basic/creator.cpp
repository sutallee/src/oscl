/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "creator.h"
#include "oscl/interrupt/null.h"
#include "oscl/hw/est/mdp860p/pci/map.h"
#include "oscl/interrupt/ecos/adaptor.h"
#include "cyg/kernel/kapi.h"

extern "C" void cyg_kernel_tick_dsr(cyg_ucount32 count);

extern Oscl::Interrupt::eCos::Adaptor	Scc1InterruptHandler;

Oscl::Interrupt::HandlerApi*	ExternalInterruptHandler;

using namespace Oscl::Est::Mdp860PPro;

static void	c_dsr(	cyg_vector_t    vector,
					cyg_ucount32    count,
					cyg_addrword_t  data
					){
	cyg_kernel_tick_dsr(1);
    }

SystemTimer::SystemTimer() noexcept:
	_interrupt(	(CYG_ADDRWORD)this,
				c_dsr
				)
		{
	}

static unsigned long	nDsrPost;

void	SystemTimer::tick() noexcept{
	++nDsrPost;
	_interrupt.post_dsr();
	}

class MyNullHandler : public Oscl::Interrupt::StatusHandlerApi {
	public:
		bool	interrupt() noexcept{
			return false;
			}
	};

static Oscl::Interrupt::NullHandler				nullHandler;

Creator::Creator(	Oscl::Est::Mdp860PPro::Api&		mdp860PPro,
					Oscl::Mt::Mutex::Simple::Api&	mutex
					) noexcept:
		_mdp860PPro(mdp860PPro),
		_bitPortB(Mpc860PQUICC.portb.pbdat,0),
		_mutexedBitPortB(_bitPortB,mutex),
		_ledControlPort(	Mdp8xxProDiscrete.
							eightBitPort.
							discrete.
							control.ledControl,
							0
							),
		_mutexedLedControlPort(_ledControlPort,mutex),
		_scc1CPCR(Mpc860PQUICC.cpm.cpcr),
		_smc1Serial(	mdp860PPro.getMpc860P().getSIMODE(),
						mdp860PPro.getMpc860P().getPortB(),
						mdp860PPro.getMpc860P().getDPRAM(),
						mdp860PPro.getMpc860P().getSmc1CpcrApi(),
						Mpc860PQUICC.smc1,
						Mpc860PQUICC.pram3.other.smc1
					),
		_smc2Serial(	mdp860PPro.getMpc860P().getSIMODE(),
						mdp860PPro.getMpc860P().getPortB(),
						mdp860PPro.getMpc860P().getDPRAM(),
						mdp860PPro.getMpc860P().getSmc2CpcrApi(),
						Mpc860PQUICC.smc2,
						Mpc860PQUICC.pram4.other.smcpip.smc2
					),
		_smc1IsrDsrAdaptor(_smc1Serial.getIsrDsr()),
		_smc2IsrDsrAdaptor(_smc2Serial.getIsrDsr()),
		_cpmPicHandler(mdp860PPro.getMpc860P().getPicApi().getISR()),
		_pitHandler(mdp860PPro.getMpc860P().getPitApi()),
		_pciFuncAdvServer(),
		_pciFuncServer(),
		_pciFuncAdv(_pciFuncAdvServer),
		_pciEnumService(	mdp860PPro.getPciConfigApi(),
							(unsigned long)Mdp8xxPCI,
							Mdp8xxPciSize,
							_pciFuncServer,
							_pciFuncAdvServer,
							_pciFuncAdv,
							_pciFuncAdv,
							mdp860PPro.getPciIntSourceApi()	// FIXME: This
							// should be a "Decorated" version that applies
							// the eCos ISR/DSR differences. Perhaps what
							// needs to be supplied here is an interface to
							// be used for attaching IsrDsrApi interrupts.
							// This means I must change the OHCI HCD to
							// always use the IsrDsrApi and rely on the
							// environment to couple to the StatusHandlerApi.
							),
		_delayServer(100)	// FIXME: need to correlate with PIT init.
		{
	// Set QSPAN-IMSEL line Low
	mdp860PPro.getMpc860P().getPortA().getPA12().reserveOutput().low();
	Oscl::Interrupt::HandlerApi&	picIsr = mdp860PPro.
												getMpc860P().
												getSiuPicApi();
	ExternalInterruptHandler	= &picIsr;
	mdp860PPro.getMpc860P().getPitApi().attach(_delayServer);
	}

void	Creator::initialize() noexcept{
	using namespace Oscl::Mot8xx::Cpmic;

	Oscl::Mot860::CI::PIC::Api&	cipic	= _mdp860PPro.getMpc860P().getPicApi();

	cipic.getSCC1().reserve(Scc1InterruptHandler);
	// FIXME: SMC1 seems to be devoured by eCos in some strange way,
	// and a conflict seems to arise if we use it. Thus, until
	// this is resolved, I've stopped using SMC1. If/when this is
	// resolved, BRG1 will need to be initialized here.
//	cipic.getSMC1().reserve(_smc1IsrDsrAdaptor);
	cipic.getSMC2().reserve(_smc2IsrDsrAdaptor);

	Oscl::Mot8xx::Siu::PicApi&	pic	= _mdp860PPro.getMpc860P().getSiuPicApi();

	pic.getInternalLevel0SourceApi().attach(_pitHandler);
	pic.getInternalLevel4SourceApi().attach(_cpmPicHandler);
	pic.getIrqDriverApi1().levelMode();
	pic.getIrqDriverApi1().disableLowPowerWakeUp();
	pic.getIrqDriverApi1().enable();
	_mdp860PPro.getMpc860P().getPitApi().attach(_pitSysTimer);
	_mdp860PPro.getMpc860P().getPitApi().start();
	Mpc860PQUICC.cpmic.cicr.changeBits(~0,
											CICR::SCdP::ValueMask_SCC1
										|	CICR::SCcP::ValueMask_SCC1
										|	CICR::SCbP::ValueMask_SCC1
										|	CICR::SCaP::ValueMask_SCC1
										|	CICR::IRL::ValueMask_Typical
										|	CICR::HP::ValueMask_PC15
										|	CICR::IEN::ValueMask_Enable
										|	CICR::SPS::ValueMask_Grouped
										);
	Mpc860PQUICC.cpmic.cimr.changeBits(	CIMR::SCC1::FieldMask,
										CIMR::SCC1::ValueMask_Enable
										);
	// FIXME: SMC1 seems to be devoured by eCos in some strange way,
	// and a conflict seems to arise if we use it. Thus, until
	// this is resolved, I've stopped using SMC1. If/when this is
	// resolved, BRG1 will need to be initialized here.
	///////// BRG2/SMC2
	_mdp860PPro.getMpc860P().getBRGC2().getRST().selectReset();
	_mdp860PPro.getMpc860P().getBRGC2().getEXTC().selectBRGCLK();
	_mdp860PPro.getMpc860P().getBRGC2().getATB().selectNormal();
	_mdp860PPro.getMpc860P().getBRGC2().getDIV().selectDivideBy1();
	// FIXME: 9600
	_mdp860PPro.getMpc860P().getBRGC2().getCD().setDivisor((50000000)/(16*9600));
	_mdp860PPro.getMpc860P().getBRGC2().getRST().selectEnable();
	_mdp860PPro.getMpc860P().getBRGC2().getEN().selectInputClockEnable();
	}

void	Creator::start() noexcept{
	// FIXME: SMC1 seems to be devoured by eCos in some strange way,
	// and a conflict seems to arise if we use it. Thus, until
	// this is resolved, I've stopped using SMC1.
//	_smc1Serial.syncOpen();
	_smc2Serial.syncOpen();
	}

Oscl::Interrupt::Shared::SourceApi&	Creator::getPciIrqSource() noexcept{
	return _mdp860PPro.getPciIntSourceApi();
	}

Oscl::Pci::Config::Api&	Creator::getPciConfigApi() noexcept{
	return _mdp860PPro.getPciConfigApi();
	}

Oscl::Est::Mdp860PPro::Api&		Creator::getMdp860P() noexcept{
	return _mdp860PPro;
	}

Oscl::Bits::FieldApi<Oscl::Est::Mdp8xxPro::LedControl::Reg>&
			Creator::getLedControl() noexcept{
	return _mutexedLedControlPort;
	}

Oscl::Mot8xx::Siu::PicApi&			Creator::getSiuPIC() noexcept{
	return _mdp860PPro.getMpc860P().getSiuPicApi();
	}

Oscl::Mt::Runnable&	Creator::getPciFuncAdvRunnable() noexcept{
	return _pciFuncAdvServer;
	}

Oscl::Mt::Runnable&	Creator::getPciFuncRunnable() noexcept{
	return _pciFuncServer;
	}

Oscl::Mt::Runnable&	Creator::getPciEnumRunnable() noexcept{
	return _pciEnumService;
	}


Oscl::Mt::Itc::Dyn::Adv::Cli::
SyncApi<	Oscl::PciSrv::
			Func::Server
			>&						Creator::getPciFuncAdvFind() noexcept{
	return _pciFuncAdv;
	}

Oscl::Mt::Itc::Dyn::Adv::Cli::
Req::Api<	Oscl::PciSrv::
			Func::Server
			>::SAP&					Creator::getPciFuncAdvSAP() noexcept{
	return _pciFuncAdv.getCliSAP();
	}

Oscl::Mt::Itc::Delay::Req::Api::SAP&	Creator::getDelayServiceSAP() noexcept{
	return _delayServer.getSAP();
	}

Oscl::Mt::Runnable&	Creator::getDelayServiceRunnable() noexcept{
	return _delayServer;
	}

Oscl::Mt::Runnable&		Creator::getSmc1TxRunnable() noexcept{
	return _smc1Serial.getTxDriverRunnable();
	}

Oscl::Stream::Output::Req::Api::SAP&	Creator::getSmc1SerialOutSAP() noexcept{
	return _smc1Serial.getTxDriverSAP();
	}

Oscl::Stream::Output::Api&			Creator::getSmc1OutApi() noexcept{
	return _smc1Serial.getTxSyncApi();
	}

Oscl::Mt::Runnable&					Creator::getSmc1RxRunnable() noexcept{
	return _smc1Serial.getRxDriverRunnable();
	}

Oscl::Stream::Input::Req::Api::SAP&		Creator::getSmc1SerialInSAP() noexcept{
	return _smc1Serial.getRxDriverSAP();
	}

Oscl::Stream::Input::Api&			Creator::getSmc1InApi() noexcept{
	return _smc1Serial.getRxSyncApi();
	}
///////
Oscl::Mt::Runnable&		Creator::getSmc2TxRunnable() noexcept{
	return _smc2Serial.getTxDriverRunnable();
	}

Oscl::Stream::Output::Req::Api::SAP&	Creator::getSmc2SerialOutSAP() noexcept{
	return _smc2Serial.getTxDriverSAP();
	}

Oscl::Stream::Output::Api&			Creator::getSmc2OutApi() noexcept{
	return _smc2Serial.getTxSyncApi();
	}

Oscl::Mt::Runnable&					Creator::getSmc2RxRunnable() noexcept{
	return _smc2Serial.getRxDriverRunnable();
	}

Oscl::Stream::Input::Req::Api::SAP&		Creator::getSmc2SerialInSAP() noexcept{
	return _smc2Serial.getRxDriverSAP();
	}

Oscl::Stream::Input::Api&			Creator::getSmc2InApi() noexcept{
	return _smc2Serial.getRxSyncApi();
	}

