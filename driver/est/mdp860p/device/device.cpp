/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "device.h"

using namespace Oscl::Est::Mdp860PPro;

Device::Device(	Mot8xx::Mpc860P::Map&			mpc860p,
				Oscl::Tundra::Qspan::RegMap&	qspan,
				Oscl::Mt::Mutex::Simple::Api&	mutex
				) noexcept:
		_qspanControlDriver(qspan.INT_CTL),
		_pciIntSource(_qspanControlDriver),
		_qspanDispatcher(qspan.INT_STAT),
		_mpc860p(mpc860p,mutex),
		_scc1EnetPhy(	_mpc860p.getPortA(),
						_mpc860p.getPortB(),
						_mpc860p.getPortC()
						),
		_mdp8xx(_scc1EnetPhy),
		_qSpanDriver(qspan,0),
		_qspanPciAdaptor(_qSpanDriver)
		{
	_mpc860p.getSiuPicApi().reserveIrq1(_qspanDispatcher);
    getQspanIrqApi().reservePciInt(_pciIntSource);
	}

Oscl::Mot860P::Api&	Device::getMpc860P() noexcept{
	return _mpc860p;
	}

Oscl::Motorola::MC68160::Api&	Device::getMC68160() noexcept{
	return _mdp8xx.getMC68160();
	}

Oscl::Tundra::Qspan::Irq::Api&	Device::getQspanIrqApi() noexcept{
	return _qspanDispatcher;
	}

Oscl::Interrupt::Shared::SourceApi&	Device::getPciIntSourceApi() noexcept{
	return _pciIntSource.getSourceApi();
	}

Oscl::Pci::Config::Api&			Device::getPciConfigApi() noexcept{
	return _qspanPciAdaptor;
	}

