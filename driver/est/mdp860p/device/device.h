/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_est_mdp860p_device_deviceh_
#define _oscl_drv_est_mdp860p_device_deviceh_
#include "oscl/driver/motorola/mpc860p/device/device.h"
#include "oscl/driver/est/mdp860/scc1/phy.h"
#include "oscl/driver/est/mdp860p/device/api.h"
#include "oscl/driver/est/mdp8xx/device/driver.h"
#include "oscl/driver/tundra/qspan/interrupt/dispatcher.h"
#include "oscl/driver/tundra/qspan/interrupt/stat.h"
#include "oscl/driver/tundra/qspan/interrupt/ctrl.h"
#include "oscl/driver/tundra/qspan/interrupt/dir.h"
#include "oscl/driver/tundra/qspan/adaptor.h"
#include "oscl/driver/tundra/qspan/qspan.h"
#include "oscl/hw/tundra/qspan/qspan.h"
#include "oscl/driver/tundra/qspan/interrupt/source.h"
#include "api.h"

/** */
namespace Oscl {
/** */
namespace Est {
/** */
namespace Mdp860PPro {

/** This class implements the low level devices available on
	the MDP860PPro development board. It contains only primitive
	device drivers that make no assumptions about the existence
	of a kernel. No policy or configuration options are to be
	assumed at this level either.
 */
class Device : public Api {
	private:
		/** */
		Oscl::Tundra::Qspan::Irq::ControlDriver	_qspanControlDriver;
		/** */
		Oscl::Tundra::Qspan::Irq::PciIntSource	_pciIntSource;
		/** */
		Oscl::Tundra::Qspan::Irq::Dispatcher	_qspanDispatcher;
		/** */
		Oscl::Mot860P::Device					_mpc860p;
		/** */
		Oscl::Est::Mdp860::SCC1::Phy			_scc1EnetPhy;
		/** */
		Oscl::Est::Mdp8xxPro::Driver			_mdp8xx;
		/** */
		Oscl::Tundra::Qspan::Driver 			_qSpanDriver;
		/** */
		Oscl::Tundra::Qspan::PciAdaptor			_qspanPciAdaptor;

	public:
		/** */
		Device(	Mot8xx::Mpc860P::Map&			mpc860p,
				Oscl::Tundra::Qspan::RegMap&	qspan,
				Oscl::Mt::Mutex::Simple::Api&	mutex
				) noexcept;

	public:	// Api
		/** */
		Mot860P::Api&						getMpc860P() noexcept;
		/** */
		Oscl::Motorola::MC68160::Api&		getMC68160() noexcept;
		/** */
		Oscl::Tundra::Qspan::Irq::Api&		getQspanIrqApi() noexcept;
		/** */
		Oscl::Interrupt::Shared::SourceApi&	getPciIntSourceApi() noexcept;
		/** */
		Oscl::Pci::Config::Api&				getPciConfigApi() noexcept;
	};

}
}
}


#endif
