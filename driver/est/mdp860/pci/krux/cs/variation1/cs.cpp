/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "oscl/driver/est/mdp860/pci/krux/cs.h"
#include "oscl/driver/est/mdp860/krux/cs.h"
#include "oscl/hw/est/mdp860/pci/map.h"

using namespace Oscl::Est::Mdp860Pro::PCI;

void Oscl::Est::Mdp860Pro::PCI::ConfigChipSelects() noexcept{
	Oscl::Est::Mdp860Pro::ConfigChipSelects();
	// CS6 PCI aperature
	Mpc8xxQUICC.memc.cs[6]._or	=
			Oscl::Mot8xx::Memc::OR::AM::ValueMask_Size1G
		|	(0<<Oscl::Mot8xx::Memc::OR::ATM::Lsb)
		|	Oscl::Mot8xx::Memc::OR::GPCM::CSNT::ValueMask_Normal
		|	Oscl::Mot8xx::Memc::OR::GPCM::ACS::ValueMask_Normal
		|	Oscl::Mot8xx::Memc::OR::BIH::ValueMask_AllowBurst
		|	(0<<Oscl::Mot8xx::Memc::OR::SCY::Lsb)
		|	Oscl::Mot8xx::Memc::OR::SETA::ValueMask_ExternalTA
		|	Oscl::Mot8xx::Memc::OR::TRLX::ValueMask_Normal
		|	Oscl::Mot8xx::Memc::OR::EHTR::ValueMask_Normal
		;
	Mpc8xxQUICC.memc.cs[6]._br	=
			(		((Oscl::Mot8xx::Memc::BR::Reg)Mdp8xxPCI)
				&	Oscl::Mot8xx::Memc::BR::BA::FieldMask
				)
		|	(0<<Oscl::Mot8xx::Memc::BR::AT::Lsb)
		|	Oscl::Mot8xx::Memc::BR::PS::ValueMask_PortSize32bit
		|	Oscl::Mot8xx::Memc::BR::PARE::ValueMask_Disabled
		|	Oscl::Mot8xx::Memc::BR::WP::ValueMask_ReadWrite
		|	Oscl::Mot8xx::Memc::BR::MS::ValueMask_GPCM
		|	Oscl::Mot8xx::Memc::BR::V::ValueMask_Valid
		;
	// CS7 QSPAN
	Mpc8xxQUICC.memc.cs[7]._or	=
			Oscl::Mot8xx::Memc::OR::AM::ValueMask_Size32K
		|	(0<<Oscl::Mot8xx::Memc::OR::ATM::Lsb)
		|	Oscl::Mot8xx::Memc::OR::GPCM::CSNT::ValueMask_Normal
		|	Oscl::Mot8xx::Memc::OR::GPCM::ACS::ValueMask_Normal
		|	Oscl::Mot8xx::Memc::OR::BIH::ValueMask_AllowBurst
		|	(0<<Oscl::Mot8xx::Memc::OR::SCY::Lsb)
		|	Oscl::Mot8xx::Memc::OR::SETA::ValueMask_ExternalTA
		|	Oscl::Mot8xx::Memc::OR::TRLX::ValueMask_Normal
		|	Oscl::Mot8xx::Memc::OR::EHTR::ValueMask_Normal
		;
	Mpc8xxQUICC.memc.cs[7]._br	=
			(		((Oscl::Mot8xx::Memc::BR::Reg)&QspanPciBridge)
				&	Oscl::Mot8xx::Memc::BR::BA::FieldMask
				)
		|	(0<<Oscl::Mot8xx::Memc::BR::AT::Lsb)
		|	Oscl::Mot8xx::Memc::BR::PS::ValueMask_PortSize32bit
		|	Oscl::Mot8xx::Memc::BR::PARE::ValueMask_Disabled
		|	Oscl::Mot8xx::Memc::BR::WP::ValueMask_ReadWrite
		|	Oscl::Mot8xx::Memc::BR::MS::ValueMask_GPCM
		|	Oscl::Mot8xx::Memc::BR::V::ValueMask_Valid
		;
	}

