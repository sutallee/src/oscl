/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_est_mdp860_serial_smc_creatorh_
#define _oscl_drv_est_mdp860_serial_smc_creatorh_
#include "oscl/driver/motorola/mpc8xx/serial/smc/driver.h"
#include "oscl/extalloc/driver.h"


/** */
namespace Oscl{
/** */
namespace Est {
/** */
namespace Mdp860 {
/** */
namespace Serial {
/** */
namespace Smc {

/** */
class Creator : public Mt::Itc::Srv::OpenCloseSyncApi {
	private:
		/** */
		enum {nRxDescriptors=4};
		/** */
		enum {nTxDescriptors=4};
		/** */
		enum {nRxBuffers=8};
		/** */
		enum {nDoneMem=8};
		/** */
		Oscl::Serial::Itc::RX::BufferMem	_bufferMem[nRxBuffers];
		/** */
		Oscl::Serial::Itc::TX::DoneMem		_doneMem[nDoneMem];
		/** */
		Oscl::ExtAlloc::Record				_rxDescriptors;
		/** */
		Oscl::ExtAlloc::Record				_txDescriptors;
		/** */
		Oscl::Done::Api*					_completes[nTxDescriptors];
		/** */
		Oscl::ExtAlloc::Api&				_dpram;
		/** */
		Oscl::Mot8xx::Serial::SMC::Driver	_smcSerialDriver;
	public:
		/** */
		Creator(	Oscl::ExtAlloc::Api&				dpram,
					Oscl::Mot8xx::CP::CR::SMC::Api&		cpcr,
					Mot8xx::Smc::Map&					registers,
					Oscl::Mot8xx::Pram::SMC&			pram
					) noexcept;
		/** */
		virtual ~Creator(){}
		/** */
		Oscl::Interrupt::StatusHandlerApi&	getISR() noexcept;
		/** */
		Oscl::Interrupt::IsrDsrApi&			getIsrDsr() noexcept;
		/** */
		Oscl::Mt::Runnable&	getTxDriverRunnable() noexcept;
		/** */
		Oscl::Stream::Output::Req::Api::SAP&	getTxDriverSAP() noexcept;
		/** */
		Oscl::Stream::Output::Api&			getTxSyncApi() noexcept;
		/** */
		Oscl::Mt::Runnable&	getRxDriverRunnable() noexcept;
		/** */
		Oscl::Stream::Input::Req::Api::SAP&	getRxDriverSAP() noexcept;
		/** */
		Oscl::Stream::Input::Api&		getRxSyncApi() noexcept;

	private:
		/** */
		Oscl::Motorola::Quicc::Sdma::Rx::BufferDesc*
			allocRxDescriptors() noexcept;
		/** */
		Oscl::Motorola::Quicc::Sdma::Tx::BufferDesc*
			allocTxDescriptors() noexcept;
	public:
		/** */
		void	syncOpen() noexcept;
		/** */
		void	syncClose() noexcept;
	};

}
}
}
}
}
#endif
