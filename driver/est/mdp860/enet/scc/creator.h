/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_est_mdp860_enet_scc_creatorh_
#define _oscl_drv_est_mdp860_enet_scc_creatorh_
#include "oscl/driver/motorola/mpc8xx/enet/scc/driver.h"
#include "oscl/extalloc/driver.h"


/** */
namespace Oscl{
/** */
namespace Est {
/** */
namespace Mdp860 {
/** */
namespace Enet {
/** */
namespace Scc {

/** */
class Creator : public Mt::Itc::Srv::OpenCloseSyncApi {
	private:
		/** */
		enum {nRxDescriptors=16};
		/** */
		enum {nTxDescriptors=16};
		/** */
		Oscl::ExtAlloc::Record			_rxDescriptors;
		/** */
		Oscl::ExtAlloc::Record			_txDescriptors;
		/** */
		Oscl::Done::Api*				_completes[nTxDescriptors];
		/** */
		Oscl::ExtAlloc::Api&			_dpram;
		/** */
		Oscl::Mot8xx::Enet::SCC::Driver	_sccEnetDriver;
	public:
		/** */
		Creator(	Oscl::ExtAlloc::Api&					dpram,
					Oscl::Mot8xx::CP::CR::SCC::Api&			cpcr,
					Mot8xx::Scc::Map&						registers,
					Oscl::Mot8xx::Pram::SCC::ENET&			pram,
					const Oscl::Protocol::IEEE::MacAddress&	stationAddress,
					unsigned								rxBufferSize,
					unsigned								maximumFrameLength
					) noexcept;
		/** */
		virtual ~Creator(){}
		/** */
		Oscl::Interrupt::StatusHandlerApi&	getISR() noexcept;
		/** */
		Oscl::Mt::Runnable&	getTxDriverRunnable() noexcept;
		/** */
		Oscl::Frame::Pdu::Tx::Req::Api::SAP&	getTxDriverSAP() noexcept;
		/** */
		Oscl::Mt::Runnable&	getRxDriverRunnable() noexcept;
		/** */
		Oscl::Frame::Pdu::Rx::DriverApi&	getRxDriver() noexcept;
	private:
		/** */
		Oscl::Motorola::Quicc::Sdma::Rx::BufferDesc*
			allocRxDescriptors() noexcept;
		/** */
		Oscl::Motorola::Quicc::Sdma::Tx::BufferDesc*
			allocTxDescriptors() noexcept;
	public:
		/** */
		void	syncOpen() noexcept;
		/** */
		void	syncClose() noexcept;
	};

}
}
}
}
}
#endif
