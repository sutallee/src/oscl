/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "creator.h"
#include "oscl/error/fatal.h"

using namespace Oscl::Est::Mdp860::Enet::Scc;

Creator::Creator(	Oscl::ExtAlloc::Api&					dpram,
					Oscl::Mot8xx::CP::CR::SCC::Api&			cpcr,
					Mot8xx::Scc::Map&						registers,
					Oscl::Mot8xx::Pram::SCC::ENET&			pram,
					const Oscl::Protocol::IEEE::MacAddress&	stationAddress,
					unsigned								rxBufferSize,
					unsigned								maximumFrameLength
					) noexcept:
		_dpram(dpram),
		_sccEnetDriver(	pram,
						registers,
						cpcr,
						rxBufferSize,
						maximumFrameLength,
						allocTxDescriptors(),
						_completes,
						nTxDescriptors,
						allocRxDescriptors(),
						nRxDescriptors,
						(char *)dpram.baseUnit(),
						stationAddress
						)
		{
	}

Oscl::Interrupt::StatusHandlerApi&	Creator::getISR() noexcept{
	return _sccEnetDriver.getISR();
	}

Oscl::Mt::Runnable&	Creator::getTxDriverRunnable() noexcept{
	return _sccEnetDriver.getTxDriverRunnable();
	}

Oscl::Frame::Pdu::Tx::Req::Api::SAP&	Creator::getTxDriverSAP() noexcept{
	return _sccEnetDriver.getTxDriverSAP();
	}

Oscl::Mt::Runnable&	Creator::getRxDriverRunnable() noexcept{
	return _sccEnetDriver.getRxDriverRunnable();
	}

Oscl::Frame::Pdu::Rx::DriverApi&	Creator::getRxDriver() noexcept{
	return _sccEnetDriver.getRxDriver();
	}

Oscl::Motorola::Quicc::Sdma::Rx::BufferDesc*
	Creator::allocRxDescriptors() noexcept{
	using namespace Oscl::Motorola::Quicc::Sdma::Rx;
	if(!_dpram.alloc(	_rxDescriptors,
						nRxDescriptors*
						sizeof(BufferDesc)
						)){
		Oscl::ErrorFatal::logAndExit("Can't reserve rx buffer space");
		return 0;
		}
	
	return (BufferDesc*)_rxDescriptors.getFirstUnit();
	}

Oscl::Motorola::Quicc::Sdma::Tx::BufferDesc*
	Creator::allocTxDescriptors() noexcept{
	using namespace Oscl::Motorola::Quicc::Sdma::Tx;
	if(!_dpram.alloc(	_txDescriptors,
						nTxDescriptors*
						sizeof(BufferDesc)
						)){
		Oscl::ErrorFatal::logAndExit("Can't reserve tx buffer space");
		return 0;
		}
	return (BufferDesc*)_txDescriptors.getFirstUnit();
	}

void	Creator::syncOpen() noexcept{
	_sccEnetDriver.syncOpen();
	}

void	Creator::syncClose() noexcept{
	_sccEnetDriver.syncClose();
	}

