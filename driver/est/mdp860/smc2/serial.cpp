/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "serial.h"
#include "oscl/hw/est/mdp860/map.h"

using namespace Oscl::Est::Mdp860::SMC2;

Serial::Serial(	Oscl::Mot860::SI::MR::DriverApi&			simode,
				Oscl::Mot860::PortB::PortApi&				portb,
				Oscl::ExtAlloc::Api&						dpram,
				Oscl::Mot8xx::CP::CR::SMC::Api&				cpcr,
				Mot8xx::Smc::Map&							registers,
				Oscl::Mot8xx::Pram::SMC&					pram
				) noexcept:
		_smcSerialCreator(	dpram,
							cpcr,
							registers,
							pram
							)
		{
	Oscl::Mot860::SI::MR::SMC2Api&	simodeSMC2	= simode.reserveSMC2();
	simodeSMC2.getSMC().selectNMSI();
	simodeSMC2.getCS().selectBRG2();	// FIXME: Not here?
	portb.getPB21().reserveSMTXD2();
	portb.getPB20().reserveSMRXD2();
	}

Oscl::Mt::Runnable&	Serial::getTxDriverRunnable() noexcept{
	return _smcSerialCreator.getTxDriverRunnable();
	}

Oscl::Stream::Output::Req::Api::SAP&	Serial::getTxDriverSAP() noexcept{
	return _smcSerialCreator.getTxDriverSAP();
	}

Oscl::Stream::Output::Api&	Serial::getTxSyncApi() noexcept{
	return _smcSerialCreator.getTxSyncApi();
	}

Oscl::Stream::Input::Req::Api::SAP&	Serial::getRxDriverSAP() noexcept{
	return _smcSerialCreator.getRxDriverSAP();
	}

Oscl::Stream::Input::Api&	Serial::getRxSyncApi() noexcept{
	return _smcSerialCreator.getRxSyncApi();
	}

Oscl::Mt::Runnable&	Serial::getRxDriverRunnable() noexcept{
	return _smcSerialCreator.getRxDriverRunnable();
	}

Oscl::Interrupt::StatusHandlerApi&	Serial::getISR() noexcept{
	return _smcSerialCreator.getISR();
	}

Oscl::Interrupt::IsrDsrApi&	Serial::getIsrDsr() noexcept{
	return _smcSerialCreator.getIsrDsr();
	}

void	Serial::syncOpen() noexcept{
	_smcSerialCreator.syncOpen();
	}

void	Serial::syncClose() noexcept{
	_smcSerialCreator.syncClose();
	}

