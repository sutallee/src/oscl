/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "phy.h"

using namespace Oscl::Est::Mdp860::SCC1;

Phy::Phy(	Oscl::Mot860::PortA::PortApi&	porta,
			Oscl::Mot860::PortB::PortApi&	portb,
			Oscl::Mot860::PortC::PortApi&	portc
			) noexcept:
		_mc68160RxPin(porta.getPA15().reserveRXD1()),
		_mc68160RxClkPin(porta.getPA6().reserveCLK2()),
		_mc68160RenaPin(portc.getPC10().reserveCD1()),
		_mc68160ClsnPin(portc.getPC11().reserveCTS1()),
		_mc68160TxPin(porta.getPA14().reserveTXD1()),
		_mc68160TenaPin(portb.getPB19().reserveRTS1()),
		_mc68160TxClkPin(porta.getPA7().reserveCLK1()),
		_mc68160TpenPin(portc.getPC9().reserveInOut()),
		_mc68160AportPin(portc.getPC8().reserveInOut()),
		_mc68160TpacpePin(portc.getPC7().reserveInOut()),
		_mc68160TpsqelPin(portc.getPC6().reserveInOut()),
		_mc68160TpfuldlPin(porta.getPA8().reserveInOut()),
		_mc68160LoopPin(portc.getPC4().reserveInOut())
		{
	}

Oscl::Bits::InputApi&	Phy::getMc68160RxPin() noexcept{
	return _mc68160RxPin;
	}

Oscl::Bits::InputApi&	Phy::getMc68160RxClkPin() noexcept{
	return _mc68160RxClkPin;
	}

Oscl::Bits::InputApi&	Phy::getMc68160RenaPin() noexcept{
	return _mc68160RenaPin;
	}

Oscl::Bits::InputApi&	Phy::getMc68160ClsnPin() noexcept{
	return _mc68160ClsnPin;
	}

Oscl::Bits::InputApi&	Phy::getMc68160TxPin() noexcept{
	return _mc68160TxPin;
	}

Oscl::Bits::InputApi&	Phy::getMc68160TenaPin() noexcept{
	return _mc68160TenaPin;
	}

Oscl::Bits::InputApi&	Phy::getMc68160TxClkPin() noexcept{
	return _mc68160TxClkPin;
	}

Oscl::Bits::InOutApi&	Phy::getMc68160TpenPin() noexcept{
	return _mc68160TpenPin;
	}

Oscl::Bits::InOutApi&	Phy::getMc68160AportPin() noexcept{
	return _mc68160AportPin;
	}

Oscl::Bits::InOutApi&	Phy::getMc68160TpacpePin() noexcept{
	return _mc68160TpacpePin;
	}

Oscl::Bits::InOutApi&	Phy::getMc68160TpsqelPin() noexcept{
	return _mc68160TpsqelPin;
	}

Oscl::Bits::InOutApi&	Phy::getMc68160TpfuldlPin() noexcept{
	return _mc68160TpfuldlPin;
	}

Oscl::Bits::InOutApi&	Phy::getMc68160LoopPin() noexcept{
	return _mc68160LoopPin;
	}

