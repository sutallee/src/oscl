/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_est_mdp860_scc1_phyh_
#define _oscl_drv_est_mdp860_scc1_phyh_
#include "oscl/driver/motorola/mpc860/porta/portapi.h"
#include "oscl/driver/motorola/mpc860/portb/portapi.h"
#include "oscl/driver/motorola/mpc860/portc/portapi.h"
#include "oscl/driver/est/mdp8xx/enet/scc/phyapi.h"

/** */
namespace Oscl {
/** */
namespace Est {
/** */
namespace Mdp860 {
/** */
namespace SCC1 {

/** */
class Phy : public Oscl::Est::Mdp8xxPro::Enet::Scc::PhyApi {
	private:
		/** */
		Bits::InputApi&		_mc68160RxPin;
		/** */
		Bits::InputApi&		_mc68160RxClkPin;
		/** */
		Bits::InputApi&		_mc68160RenaPin;
		/** */
		Bits::InputApi&		_mc68160ClsnPin;
		/** */
		Bits::InputApi&		_mc68160TxPin;
		/** */
		Bits::InputApi&		_mc68160TenaPin;
		/** */
		Bits::InputApi&		_mc68160TxClkPin;
		/** */
		Bits::InOutApi&		_mc68160TpenPin;
		/** */
		Bits::InOutApi&		_mc68160AportPin;
		/** */
		Bits::InOutApi&		_mc68160TpacpePin;
		/** */
		Bits::InOutApi&		_mc68160TpsqelPin;
		/** */
		Bits::InOutApi&		_mc68160TpfuldlPin;
		/** */
		Bits::InOutApi&		_mc68160LoopPin;

	public:
		/** */
		Phy(	Mot860::PortA::PortApi&	porta,
				Mot860::PortB::PortApi&	portb,
				Mot860::PortC::PortApi&	portc
				) noexcept;
	public:
		/** */
		Bits::InputApi&	getMc68160RxPin() noexcept;
		/** */
		Bits::InputApi&	getMc68160RxClkPin() noexcept;
		/** */
		Bits::InputApi&	getMc68160RenaPin() noexcept;
		/** */
		Bits::InputApi&	getMc68160ClsnPin() noexcept;
		/** */
		Bits::InputApi&	getMc68160TxPin() noexcept;
		/** */
		Bits::InputApi&	getMc68160TenaPin() noexcept;
		/** */
		Bits::InputApi&	getMc68160TxClkPin() noexcept;
		/** */
		Bits::InOutApi&	getMc68160TpenPin() noexcept;
		/** */
		Bits::InOutApi&	getMc68160AportPin() noexcept;
		/** */
		Bits::InOutApi&	getMc68160TpacpePin() noexcept;
		/** */
		Bits::InOutApi&	getMc68160TpsqelPin() noexcept;
		/** */
		Bits::InOutApi&	getMc68160TpfuldlPin() noexcept;
		/** */
		Bits::InOutApi&	getMc68160LoopPin() noexcept;
	};

}
}
}
}


#endif
