/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "enet.h"
#include "oscl/hw/est/mdp860/map.h"

using namespace Oscl::Est::Mdp860::SCC1;

Enet::Enet(	Oscl::Motorola::MC68160::Api&				phy,
			Oscl::Mot860::SI::CR::DriverApi&			sicr,
			Oscl::Mot860::PortA::PortApi&				porta,
			Oscl::Mot860::PortB::PortApi&				portb,
			Oscl::Mot860::PortC::PortApi&				portc,
			Oscl::ExtAlloc::Api&						dpram,
			Oscl::Mot8xx::CP::CR::SCC::Api&				cpcr,
			Mot8xx::Scc::Map&							registers,
			Oscl::Mot8xx::Pram::SCC::ENET&				pram,
			const Oscl::Protocol::IEEE::MacAddress&		stationAddress,
			unsigned									rxBufferSize,
			unsigned									maximumFrameLength
			) noexcept:
		_sccEnetCreator(	dpram,
							cpcr,
							registers,
							pram,
							stationAddress,
							rxBufferSize,
							maximumFrameLength
							)
		{
	Oscl::Mot860::SI::CR::SCC1Api&	sicrSCC1	= sicr.reserveSCC1();
	sicrSCC1.getGR().selectTxGrantAlwaysAsserted();
	sicrSCC1.getSC().selectNMSI();
	sicrSCC1.getTCS().selectCLK1();
	sicrSCC1.getRCS().selectCLK2();
	}

Oscl::Mt::Runnable&	Enet::getTxDriverRunnable() noexcept{
	return _sccEnetCreator.getTxDriverRunnable();
	}

Oscl::Frame::Pdu::Tx::Req::Api::SAP&	Enet::getTxDriverSAP() noexcept{
	return _sccEnetCreator.getTxDriverSAP();
	}

Oscl::Frame::Pdu::Rx::DriverApi&	Enet::getRxDriver() noexcept{
	return _sccEnetCreator.getRxDriver();
	}

Oscl::Mt::Runnable&	Enet::getRxDriverRunnable() noexcept{
	return _sccEnetCreator.getRxDriverRunnable();
	}

Oscl::Interrupt::StatusHandlerApi&	Enet::getISR() noexcept{
	return _sccEnetCreator.getISR();
	}

void	Enet::syncOpen() noexcept{
	_sccEnetCreator.syncOpen();
	}

void	Enet::syncClose() noexcept{
	_sccEnetCreator.syncClose();
	}

