/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "oscl/driver/est/mdp8xx/krux/cs.h"
#include "oscl/hw/est/mdp8xx/map.h"
#include "oscl/driver/motorola/mpc8xx/spr.h"

using namespace Oscl::Est::Mdp8xxPro;

static Oscl::Est::Mdp8xxPro::FlashSimmPresent::Reg	flashSimmPresentStatus;

static unsigned long	dramSize() noexcept{
	using namespace Oscl::Est::Mdp8xxPro::DramPciPresent;
	Reg	dramPciPresentStatus	=
		Mdp8xxProDiscrete.eightBitPort.discrete.status.dramAndPciPresent;
	// This calculation is probably bogus!
	unsigned long	size	=
			(		(dramPciPresentStatus &	DRM_PD1::FieldMask)
				==	DRM_PD1::ValueMask_Present
				)
		+	(		(dramPciPresentStatus &	DRM_PD2::FieldMask)
				==	DRM_PD2::ValueMask_Present
				)
		+	(		(dramPciPresentStatus &	DRM_PD3::FieldMask)
				==	DRM_PD3::ValueMask_Present
				)
		+	(		(dramPciPresentStatus &	DRM_PD4::FieldMask)
				==	DRM_PD4::ValueMask_Present
				)
		;
	return size*4*1024*1024;
	}

static void setupDiscretesAndGetPresentStatus() noexcept{
	// Setup Discretes to 16-bit port size temporarily
	Mpc8xxQUICC.memc.cs[5]._or	=
			Oscl::Mot8xx::Memc::OR::AM::ValueMask_Size32K
		|	(0<<Oscl::Mot8xx::Memc::OR::ATM::Lsb)
		|	Oscl::Mot8xx::Memc::OR::GPCM::CSNT::ValueMask_Normal
		|	Oscl::Mot8xx::Memc::OR::GPCM::ACS::ValueMask_Normal
		|	Oscl::Mot8xx::Memc::OR::BIH::ValueMask_AllowBurst
		|	(4<<Oscl::Mot8xx::Memc::OR::SCY::Lsb)
		|	Oscl::Mot8xx::Memc::OR::SETA::ValueMask_InternalTA
		|	Oscl::Mot8xx::Memc::OR::TRLX::ValueMask_Normal
		|	Oscl::Mot8xx::Memc::OR::EHTR::ValueMask_Normal
		;
	Mpc8xxQUICC.memc.cs[5]._br	=
			(		((Oscl::Mot8xx::Memc::BR::Reg)&Mdp8xxProDiscrete)
				&	Oscl::Mot8xx::Memc::BR::BA::FieldMask
				)
		|	(0<<Oscl::Mot8xx::Memc::BR::AT::Lsb)
		|	Oscl::Mot8xx::Memc::BR::PS::ValueMask_PortSize16bit
		|	Oscl::Mot8xx::Memc::BR::PARE::ValueMask_Disabled
		|	Oscl::Mot8xx::Memc::BR::WP::ValueMask_ReadWrite
		|	Oscl::Mot8xx::Memc::BR::MS::ValueMask_GPCM
		|	Oscl::Mot8xx::Memc::BR::V::ValueMask_Valid
		;
	flashSimmPresentStatus	=
		Mdp8xxProDiscrete.sixteenBitPort.discrete.status.flashPresent;
	// Change back to 8-bit port size
	Mpc8xxQUICC.memc.cs[5]._br	=
			(		((Oscl::Mot8xx::Memc::BR::Reg)&Mdp8xxProDiscrete)
				&	Oscl::Mot8xx::Memc::BR::BA::FieldMask
				)
		|	(0<<Oscl::Mot8xx::Memc::BR::AT::Lsb)
		|	Oscl::Mot8xx::Memc::BR::PS::ValueMask_PortSize8bit
		|	Oscl::Mot8xx::Memc::BR::PARE::ValueMask_Disabled
		|	Oscl::Mot8xx::Memc::BR::WP::ValueMask_ReadWrite
		|	Oscl::Mot8xx::Memc::BR::MS::ValueMask_GPCM
		|	Oscl::Mot8xx::Memc::BR::V::ValueMask_Valid
		;
	}

void Oscl::Est::Mdp8xxPro::ConfigChipSelects() noexcept{
	Oscl::Mot8xx::writeIMMR((uint32_t)&Mpc8xxQUICC);
	// CS5 Discrete
	setupDiscretesAndGetPresentStatus();
	// CS0 Flash
	Mpc8xxQUICC.memc.cs[0]._or	=
			Oscl::Mot8xx::Memc::OR::AM::ValueMask_Size4M
		|	(0<<Oscl::Mot8xx::Memc::OR::ATM::Lsb)
		|	Oscl::Mot8xx::Memc::OR::GPCM::CSNT::ValueMask_Normal
		|	Oscl::Mot8xx::Memc::OR::GPCM::ACS::ValueMask_Normal	// 1/2?
		|	Oscl::Mot8xx::Memc::OR::BIH::ValueMask_InhibitBurst
		|	(6<<Oscl::Mot8xx::Memc::OR::SCY::Lsb)
		|	Oscl::Mot8xx::Memc::OR::SETA::ValueMask_InternalTA
		|	Oscl::Mot8xx::Memc::OR::TRLX::ValueMask_Normal
		|	Oscl::Mot8xx::Memc::OR::EHTR::ValueMask_Normal
		;
	Mpc8xxQUICC.memc.cs[0]._br	=
			(		((Oscl::Mot8xx::Memc::BR::Reg)Mdp8xxProFlash)
				&	Oscl::Mot8xx::Memc::BR::BA::FieldMask
				)
		|	(0<<Oscl::Mot8xx::Memc::BR::AT::Lsb)
		|	Oscl::Mot8xx::Memc::BR::PS::ValueMask_PortSize32bit
		|	Oscl::Mot8xx::Memc::BR::PARE::ValueMask_Disabled
		|	Oscl::Mot8xx::Memc::BR::WP::ValueMask_ReadWrite
		|	Oscl::Mot8xx::Memc::BR::MS::ValueMask_GPCM
		|	Oscl::Mot8xx::Memc::BR::V::ValueMask_Valid
		;
	// CS1 FlashSIMM High Bank
	Mpc8xxQUICC.memc.cs[1]._br	=
			Oscl::Mot8xx::Memc::BR::V::ValueMask_Invalid
		;
	// CS2 DRAM Low Bank
	//		Need to load UPMB first
	dramSize();
	//		Don't mess with DRAM
	// CS3 DRAM High Bank
	//		Need to load UPMB first
	//		Don't mess with DRAM
	// CS4 SRAM
	//	Need to load UPMA first
#if 0	// Check against EST config before enabling

	Mpc8xxQUICC.memc.cs[4]._or	=
			Oscl::Mot8xx::Memc::OR::AM::ValueMask_Size2M
		|	(0<<Oscl::Mot8xx::Memc::OR::ATM::Lsb)
		|	Oscl::Mot8xx::Memc::OR::UPM::SAM::ValueMask_Normal
		|	Oscl::Mot8xx::Memc::OR::UPM::GPL5::ValueMask_GPL_B5	// ??
		|	Oscl::Mot8xx::Memc::OR::UPM::GPLS::ValueMask_Low	// ??
		|	Oscl::Mot8xx::Memc::OR::BIH::ValueMask_AllowBurst
		|	(0<<Oscl::Mot8xx::Memc::OR::SCY::Lsb)				// ??
		|	Oscl::Mot8xx::Memc::OR::SETA::ValueMask_InternalTA
		|	Oscl::Mot8xx::Memc::OR::TRLX::ValueMask_Normal
		|	Oscl::Mot8xx::Memc::OR::EHTR::ValueMask_Normal
		;
	Mpc8xxQUICC.memc.cs[4]._br	=
			(Mdp8xxProSRAM & Oscl::Mot8xx::Memc::BR::BA::FieldMask)
		|	(0<<Oscl::Mot8xx::Memc::BR::AT::Lsb)
		|	Oscl::Mot8xx::Memc::BR::PS::ValueMask_PortSize32bit
		|	Oscl::Mot8xx::Memc::BR::PARE::ValueMask_Disabled
		|	Oscl::Mot8xx::Memc::BR::WP::ValueMask_ReadWrite
		|	Oscl::Mot8xx::Memc::BR::MS::ValueMask_UPMA
		|	Oscl::Mot8xx::Memc::BR::V::ValueMask_Valid
		;
#endif
	}

