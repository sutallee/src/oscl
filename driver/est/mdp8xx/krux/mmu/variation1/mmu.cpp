/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "oscl/driver/est/mdp8xx/krux/mmu.h"
#include "oscl/krux/platform/ppc/mmu.h"
#include "oscl/hw/est/mdp8xx/cs.h"
#include "oscl/hw/est/mdp8xx/map.h"

extern char		_dataStart;
extern char		SystemRamSize;

using namespace Oscl::Est::Mdp8xxPro;

static unsigned long	dramSize() noexcept{
	using namespace Oscl::Est::Mdp8xxPro::DramPciPresent;
	Reg	dramPciPresentStatus	=
		Mdp8xxProDiscrete.eightBitPort.discrete.status.dramAndPciPresent;
	// This calculation is probably bogus!
	unsigned long	size	=
			(		(dramPciPresentStatus &	DRM_PD1::FieldMask)
				==	DRM_PD1::ValueMask_Present
				)
		+	(		(dramPciPresentStatus &	DRM_PD2::FieldMask)
				==	DRM_PD2::ValueMask_Present
				)
		+	(		(dramPciPresentStatus &	DRM_PD3::FieldMask)
				==	DRM_PD3::ValueMask_Present
				)
		+	(		(dramPciPresentStatus &	DRM_PD4::FieldMask)
				==	DRM_PD4::ValueMask_Present
				)
		;
	return size*4*1024*1024;
	}

/** This routine is called to initialize and map the fixed
	memory and I/O resources using the MMU. It must be called
	after MPC8xx chip selects have been initialized.
 */
void Oscl::Est::Mdp8xxPro::MmuMapFixed() noexcept{
	// IMMR Oscl::Mot8xx::writeIMMR((uint32_t)&Mpc8xxQUICC);
	// CS5 Discrete (		((Oscl::Mot8xx::Memc::BR::Reg)&Mdp8xxProDiscrete)
	// CS0 Flash (		((Oscl::Mot8xx::Memc::BR::Reg)Mdp8xxProFlash)
	// CS1 FlashSIMM High Bank Oscl::Mot8xx::Memc::BR::V::ValueMask_Invalid
	// CS2 DRAM Low Bank dramSize();
	// CS3 DRAM High Bank
	// CS4 SRAM (Mdp8xxProSRAM & Oscl::Mot8xx::Memc::BR::BA::FieldMask)

	// Map kernel text
	MapReadOnlyRamRange(0,(unsigned long)&_dataStart);

	// Map remainder of physical RAM for kernel visibility
	dramSize();
	MapWriteableRamRange(&_dataStart,(unsigned long)&SystemRamSize);

	MapIoRange(&Mpc8xxQUICC,sizeof(Mpc8xxQUICC));
	MapIoRange(&Mdp8xxProDiscrete,sizeof(Mdp8xxProDiscrete));
	MapIoRange(Mdp8xxProFlash,Mdp8xxProFlashSize);
	MapIoRange(Mdp8xxProSRAM,Mdp8xxProSramSize);
	}
