/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_est_mdp8xx_krux_csh_
#define _oscl_drv_est_mdp8xx_krux_csh_

/** */
namespace Oscl {
/** */
namespace Est {
/** */
namespace Mdp8xxPro {

/** This routine is called at startup to initialize
	the MPC8xx chip selects for the generic parts of
	the MDP8xxPro mother-board. This includes determining
	the presence and size of Flash and RAM resources.
	This routine is responsible for (re)mapping the
	following "chip-selects":
	IMMR
	Flash       CS0
	FlashSIMM   CS1
	DRAM Low    CS2 ?
	DRAM High   CS3
	SRAM        CS4
	Discrete    CS5
 */
void ConfigChipSelects() noexcept;

}
}
}

#endif
