/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "pci.h"
#include "oscl/driver/pci/function.h"
#include "oscl/hw/est/mdp860p/pci/map.h"
#include "oscl/extalloc/driver.h"
#include "oscl/driver/pci/stdfunc.h"
#include "oscl/krux/platform/ppc/mmu.h"
#include "oscl/cpu/sync.h"

/////////////////////////////////////////////////////////////////////

static void	initializePciBridgeConfig() {
	using namespace Oscl::Tundra::Qspan;
	unsigned long	qBusBaseAdderess	= 0x30000000;
	unsigned long	pciBusBaseAddress	= 0x40000000;
	// For MDP860, PA12 controls IMSEL (image select).
	// PA12.low() is currently done in
	// "oscl/driver/est/mdp860p/pci/krux/bsp/basic/creator.cpp".
	// IMSEL needs to be set low for QBUS Slave Image Zero.
	// When MPC8xx accesses the PCI bus via CSPCI chip select.
	QspanPciBridge.
	MISC_CTL		= 		MiscControlStatus::SW_RST::ValueMask_Negate
						|	MiscControlStatus::S_BG::ValueMask_Synchronous
						|	MiscControlStatus::S_BB::ValueMask_Synchronous
						|	MiscControlStatus::QB_BOC::ValueMask_LittleEndian
						|	MiscControlStatus::MA_BE_D::ValueMask_Pci2_1
						|	(0<<MiscControlStatus::PRCNT::Lsb)
						|	MiscControlStatus::MSTSLV::
							ValueMask_PowerQuiccMasterQuiccPowerQuiccSlave
						;
	// QBus Slave Image Zero
	QspanPciBridge.
	QBSI0_AT	=		(		((unsigned long)Mdp8xxPCI)
							&	QBusSlaveImage0AddressTranslation::TA::FieldMask
								)
					|	QBusSlaveImage0AddressTranslation::BS::ValueMask_Size1G
					|	QBusSlaveImage0AddressTranslation::EN::ValueMask_Enable
					;
	QspanPciBridge.
	QBSI0_CTL	=		QBusSlaveImage0Control::PWEN::ValueMask_Disable
					|	QBusSlaveImage0Control::PAS::ValueMask_Memory
					;

	// PCI Target Image Zero
	// When PCI bus master tries to access MDP8xx memory.
	QspanPciBridge.
	PBTI0_ADD	= 		(		pciBusBaseAddress
							&	BusTargetImage0Address::BA::FieldMask
							)
					|	(	(		qBusBaseAdderess
								>> BusTargetImage0Address::BA::Lsb
								)
							&	BusTargetImage0Address::TA::FieldMask
							)
					;
	QspanPciBridge.
	PBTI1_ADD	= 		(		0xF0000000
							&	BusTargetImage0Address::BA::FieldMask
							)
					|	(	(		0xD0000000
								>> BusTargetImage0Address::BA::Lsb
								)
							&	BusTargetImage0Address::TA::FieldMask
							)
					;
	QspanPciBridge.
	PBTI0_CTL	=		BusTargetImage0Control::EN::ValueMask_Enable
					|	BusTargetImage0Control::BS::ValueMask_Size512K
					|	BusTargetImage0Control::PREN::ValueMask_Disable
					|	BusTargetImage0Control::BRSTWREN::ValueMask_Enable
					|	BusTargetImage0Control::INVEND::ValueMask_InvertQB_BOC
					|	BusTargetImage0Control::TC::ValueMask_Default
					|	BusTargetImage0Control::DSIZE::ValueMask_Size32Bit
					|	BusTargetImage0Control::PWEN::ValueMask_Disable
					|	BusTargetImage0Control::PAS::ValueMask_MemorySpace
					;
	QspanPciBridge.
	PBTI1_CTL	=		BusTargetImage0Control::EN::ValueMask_Enable
					|	BusTargetImage0Control::BS::ValueMask_Size64K
					|	BusTargetImage0Control::PREN::ValueMask_Disable
					|	BusTargetImage0Control::BRSTWREN::ValueMask_Enable
					|	BusTargetImage0Control::INVEND::ValueMask_UseQB_BOC
					|	BusTargetImage0Control::TC::ValueMask_Default
					|	BusTargetImage0Control::DSIZE::ValueMask_Size32Bit
					|	BusTargetImage0Control::PWEN::ValueMask_Disable
					|	BusTargetImage0Control::PAS::ValueMask_MemorySpace
					;
	// Configure PCI Config Space PCI address
	QspanPciBridge.PCI_BSM	= 0xFFFF0000;	// Not at 0xFFFFF000 to prevent
											// confusion at address 0xFFFFFFFF
	OsclCpuInOrderMemoryAccessBarrier();
	// Enable target
	QspanPciBridge.
	PCI_CS	|= 	(		ConfigSpaceControlStatus::MS::ValueMask_Enable
					| 	ConfigSpaceControlStatus::BM::ValueMask_Enable
					)
					;
	OsclCpuInOrderMemoryAccessBarrier();
	// Enable error logging.
	QspanPciBridge.
	QB_ERRCS	=	0x80000000;
	}

static void	initializePciBridgeInterrupts() {
	// Since the MDP8xxPro does not have a "South Bridge" PIC
	// there is no need to issue a PCI IACK transaction
	// using a QspanPciBridge.IACK_GEN access. All PCI
	// interrupts must be chained and then all sources
	// polled until the cause of the interrupt is located.
	// This polling probably starts by checking the
	// QspanPciBridge.INT_STAT register, which indicates
	// whether the source is a QSPAN source or a PCI bus
	// source. The INT_IS of the QspanPciBridge.INT_STAT
	// indicates that the PCI bus INT# (INTA# on MDP8xxPro)
	// is interrupting.
	// Since the MDP8xxPro only supports INTA#, all devices
	// on the QSPAN PCI bus must have their interrupts
	// applied to INTA# via the pciConfig.InterruptPin
	// register. However, pciConfig.InterruptPin is a read
	// only register. Therefore, devices that map to INTB#,
	// INTC#, or INTD# cannot be used with the MDP8xxPro.
	// However, the OCHI USB chip does use INTA#. Thus,
	// it would appear that there is no need configuration
	// necessary for the usbConfig.InterruptPin.
	}

namespace Oscl {

void	initializePciBridge() {
	initializePciBridgeConfig();
	initializePciBridgeInterrupts();
	}

}
