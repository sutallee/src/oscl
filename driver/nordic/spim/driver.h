/*
   Copyright (C) 2021 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_driver_nordic_spim_driverh_
#define _oscl_driver_nordic_spim_driverh_

#include "oscl/mt/itc/mbox/server.h"
#include "oscl/hw/nordic/spim/map.h"
#include "oscl/mt/itc/srv/openapi.h"
#include "oscl/mt/itc/srv/open.h"
#include "oscl/mt/itc/mbox/server.h"
#include "oscl/spi/master/itc/reqapi.h"
#include "oscl/spi/master/itc/sync.h"

/** */
namespace Oscl {
/** */
namespace Nordic {
/** */
namespace SPIM {

/**
 */
class Driver :	
	public Oscl::Mt::Itc::Server,
	public Oscl::Mt::Itc::Srv::OpenSync,
	private Oscl::Mt::Itc::Srv::Open::Req::Api,
	private Oscl::SPI::Master::Req::Api
	{
	private:
		/** */
		Oscl::Nordic::SPIM::Map&		_reg;

	private:
		/** */
		Oscl::SPI::Master::Sync			_sync;

	private:
        /** */
		Oscl::Queue<
			Oscl::SPI::Master::
			Req::Api::TransferReq
			>						_pending;
        
        /** */
		Oscl::SPI::Master::
		Req::Api::TransferReq*		_currentTransferMsg;

	private:
		/**	Cached value of the current
			FREQUENCY register contents.
		 */
		Oscl::Nordic::
		SPIM::FREQUENCY::Reg		_frequencyRegValue;

		/**	Cached value of the current
			CONFIG register contents.
			ORDER,CPOL, and CPHA
		 */
		Oscl::Nordic::
		SPIM::CONFIG::Reg			_configRegValue;

		/** */
		const uint8_t				_mosiPort;
		/** */
		const uint8_t				_mosiPin;
		/** */
		const uint8_t				_misoPort;
		/** */
		const uint8_t				_misoPin;
		/** */
		const uint8_t				_sckPort;
		/** */
		const uint8_t				_sckPin;
		/** */
		const uint8_t				_csnPort;
		/** */
		const uint8_t				_csnPin;

		/** */
		const uint8_t				_dcxPort;
		/** */
		const uint8_t				_dcxPin;

	public:
		/** */
		Driver(
			Oscl::Nordic::SPIM::Map&	reg,
			void						(osInitHook)(void*),
			void*						osInitHookArg,
			Oscl::Nordic::
			SPIM::FREQUENCY::Reg		frequencyRegValue,
			Oscl::Nordic::
			SPIM::CONFIG::Reg			configRegValue,
			uint8_t						mosiPort,
			uint8_t						mosiPin,
			uint8_t						misoPort,
			uint8_t						misoPin,
			uint8_t						sckPort,
			uint8_t						sckPin,
			uint8_t						csnPort,
			uint8_t						csnPin
			) noexcept;

		/** */
		virtual ~Driver(){}

		/** */
		Oscl::Mt::Runnable&		getDriverRunnable() noexcept;

		/** RETURN: Reference to the SignalApi to
			be used to notify the driver when a
			transmit interrupt has happened.
		 */
		Oscl::Mt::Sema::SignalApi&	getSignalApi() noexcept;

		/** */
		Oscl::SPI::Master::Api&		getSyncApi() noexcept;

		/** */
		Oscl::SPI::Master::Req::Api::SAP&	getSAP() noexcept;

	private: // Oscl::Mt::Itc::Server
		/** */
		void	mboxSignaled() noexcept;

	private:
		/** */
		void	start() noexcept;

		/** */
		void	initSckPin() noexcept;

		/** */
		void	initMosiPin() noexcept;

		/** */
		void	initMisoPin() noexcept;

		/** */
		void	initCsnPin() noexcept;

		/** */
		void	initDcxPin() noexcept;

		/** */
		void	initSckGpio() noexcept;

		/** */
		void	initMosiGpio() noexcept;

		/** */
		void	initMisoGpio() noexcept;

		/** */
		void	initCsnGpio() noexcept;

		/** */
		void	initDcxGpio() noexcept;

	private:
		/** */
		void	disableAllInterrupts() noexcept;

		/** */
		void	processNextReq() noexcept;

		/** */
		void	updateConfig(
					Oscl::Nordic::
					SPIM::FREQUENCY::Reg	frequencyRegValue,
					Oscl::Nordic::
					SPIM::CONFIG::Reg		configRegValue
					) noexcept;

	private: // Oscl::Mt::Itc::Srv::Open::Req::Api
		/** */
		void    request(Oscl::Mt::Itc::Srv::Open::Req::Api::OpenReq& msg) noexcept;

	private: // Oscl::SPI::Master::Req::Api
		/** */
		void	request(Oscl::SPI::Master::Req::Api::TransferReq& msg) noexcept;

		/** */
		void	request(Oscl::SPI::Master::Req::Api::CancelTransferReq& msg) noexcept;

	};

}
}
}

#endif
