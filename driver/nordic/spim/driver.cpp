/*
   Copyright (C) 2020 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "oscl/hw/nordic/gpio/map.h"
#include "oscl/hw/nordic/spim/reg.h"
#include "driver.h"
#include "oscl/cpu/sync.h"
#include "oscl/error/fatal.h"

using namespace Oscl::Nordic::SPIM;

//#define DEBUG_TRACE

#ifdef DEBUG_TRACE
#include "oscl/error/info.h"
#endif

Driver::Driver(
	Oscl::Nordic::SPIM::Map&	reg,
	void						(osInitHook)(void*),
	void*						osInitHookArg,
	Oscl::Nordic::
	SPIM::FREQUENCY::Reg		frequencyRegValue,
	Oscl::Nordic::
	SPIM::CONFIG::Reg			configRegValue,
	uint8_t						mosiPort,
	uint8_t						mosiPin,
	uint8_t						misoPort,
	uint8_t						misoPin,
	uint8_t						sckPort,
	uint8_t						sckPin,
	uint8_t						csnPort,
	uint8_t						csnPin
	) noexcept:
		OpenSync(
			*this,
			*this
			),
		_reg(reg),
		_sync(
			*this,
			*this
			),
		_currentTransferMsg(0),
		_frequencyRegValue(frequencyRegValue),
		_configRegValue(configRegValue),
		_mosiPort(mosiPort),
		_mosiPin(mosiPin),
		_misoPort(misoPort),
		_misoPin(misoPin),
		_sckPort(sckPort),
		_sckPin(sckPin),
		_csnPort(csnPort),
		_csnPin(csnPin),
		_dcxPort(~0),
		_dcxPin(8)
		{
	disableAllInterrupts();

	if(osInitHook){
		osInitHook(osInitHookArg);
		}
	}

void	Driver::disableAllInterrupts() noexcept {

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif

	using namespace Oscl::Nordic::SPIM;
	_reg.intenclr	=
			INTENCLR::STOPPED::ValueMask_Clear
		|	INTENCLR::ENDRX::ValueMask_Clear
		|	INTENCLR::END::ValueMask_Clear
		|	INTENCLR::ENDTX::ValueMask_Clear
		|	INTENCLR::STARTED::ValueMask_Clear
		;

	OsclCpuInOrderMemoryAccessBarrier();
	}

Oscl::Mt::Runnable&	Driver::getDriverRunnable() noexcept{
	return *this;
	}

Oscl::Mt::Sema::SignalApi&	Driver::getSignalApi() noexcept{
	return *this;
	}

Oscl::SPI::Master::Api&		Driver::getSyncApi() noexcept{
	return _sync;
	}

Oscl::SPI::Master::Req::Api::SAP&	Driver::getSAP() noexcept{
	return _sync.getSAP();
	}

void	Driver::initDcxGpio() noexcept{

	using namespace Oscl::Nordic::GPIO;
	using namespace Oscl::Nordic::SPIM;

	if(_dcxPort == 0xFF){
		return;
		}

	Oscl::Nordic::GPIO::Map*
	port	= Oscl::Nordic::GPIO::getInstance(_dcxPort);

	if(!port){
		Oscl::ErrorFatal::logAndExit(
			"%s: Unknown GPIO port.\n",
			OSCL_PRETTY_FUNCTION
			);
		return;
		}

	port->outset	= 1 << _dcxPin;

	OsclCpuInOrderMemoryAccessBarrier();

	port->pin_cnf[_dcxPin]	=
			PIN_CNF::DIR::ValueMask_Output
		|	PIN_CNF::INPUT::ValueMask_Disconnect
		|	PIN_CNF::PULL::ValueMask_NoPull
		|	PIN_CNF::DRIVE::ValueMask_H0H1
		|	PIN_CNF::SENSE::ValueMask_Disabled
		;

	OsclCpuInOrderMemoryAccessBarrier();
	}
void	Driver::initCsnGpio() noexcept{

	using namespace Oscl::Nordic::GPIO;
	using namespace Oscl::Nordic::SPIM;

	if(_csnPort == 0xFF){
		return;
		}

	Oscl::Nordic::GPIO::Map*
	port	= Oscl::Nordic::GPIO::getInstance(_csnPort);

	if(!port){
		Oscl::ErrorFatal::logAndExit(
			"%s: Unknown GPIO port.\n",
			OSCL_PRETTY_FUNCTION
			);
		return;
		}

	if((_configRegValue & CONFIG::CPOL::FieldMask) == CONFIG::CPOL::ValueMask_ActiveHigh){
		port->outclr	= 1 << _csnPin;
		}
	else {
		port->outset	= 1 << _csnPin;
		}

	OsclCpuInOrderMemoryAccessBarrier();

	port->pin_cnf[_csnPin]	=
			PIN_CNF::DIR::ValueMask_Output
		|	PIN_CNF::INPUT::ValueMask_Disconnect
		|	PIN_CNF::PULL::ValueMask_NoPull
		|	PIN_CNF::DRIVE::ValueMask_H0H1
		|	PIN_CNF::SENSE::ValueMask_Disabled
		;

	OsclCpuInOrderMemoryAccessBarrier();
	}

void	Driver::initSckGpio() noexcept{

	using namespace Oscl::Nordic::GPIO;
	using namespace Oscl::Nordic::SPIM;

	if(_sckPort == 0xFF){
		// NOT optional!
		Oscl::ErrorFatal::logAndExit(
			"%s: SCK required!\n",
			OSCL_PRETTY_FUNCTION
			);
		return;
		}

	Oscl::Nordic::GPIO::Map*
	port	= Oscl::Nordic::GPIO::getInstance(_sckPort);

	if(!port){
		Oscl::ErrorFatal::logAndExit(
			"%s: Unknown GPIO port.\n",
			OSCL_PRETTY_FUNCTION
			);
		return;
		}

	if((_configRegValue & CONFIG::CPOL::FieldMask) == CONFIG::CPOL::ValueMask_ActiveHigh){
		port->outclr	= 1 << _sckPin;
		}
	else {
		port->outset	= 1 << _sckPin;
		}

	OsclCpuInOrderMemoryAccessBarrier();

	port->pin_cnf[_sckPin]	=
			PIN_CNF::DIR::ValueMask_Output
		|	PIN_CNF::INPUT::ValueMask_Connect
		|	PIN_CNF::PULL::ValueMask_NoPull
		|	PIN_CNF::DRIVE::ValueMask_H0H1
		|	PIN_CNF::SENSE::ValueMask_Disabled
		;

	OsclCpuInOrderMemoryAccessBarrier();
	}

void	Driver::initMosiGpio() noexcept{

	using namespace Oscl::Nordic::GPIO;
	using namespace Oscl::Nordic::SPIM;

	if(_mosiPort == 0xFF){
		return;
		}

	Oscl::Nordic::GPIO::Map*
	port	= Oscl::Nordic::GPIO::getInstance(_mosiPort);

	if(!port){
		Oscl::ErrorFatal::logAndExit(
			"%s: Unknown GPIO port.\n",
			OSCL_PRETTY_FUNCTION
			);
		return;
		}

	port->outclr	= 1 << _mosiPin;

	OsclCpuInOrderMemoryAccessBarrier();

	port->pin_cnf[_mosiPin]	=
			PIN_CNF::DIR::ValueMask_Output
		|	PIN_CNF::INPUT::ValueMask_Disconnect
		|	PIN_CNF::PULL::ValueMask_NoPull
		|	PIN_CNF::DRIVE::ValueMask_H0H1
		|	PIN_CNF::SENSE::ValueMask_Disabled
		;

	OsclCpuInOrderMemoryAccessBarrier();
	}

void	Driver::initMisoGpio() noexcept{

	using namespace Oscl::Nordic::GPIO;
	using namespace Oscl::Nordic::SPIM;

	if(_misoPort == 0xFF){
		return;
		}

	Oscl::Nordic::GPIO::Map*
	port	= Oscl::Nordic::GPIO::getInstance(_misoPort);

	if(!port){
		Oscl::ErrorFatal::logAndExit(
			"%s: Unknown GPIO port.\n",
			OSCL_PRETTY_FUNCTION
			);
		return;
		}

	port->pin_cnf[_misoPin]	=
			PIN_CNF::DIR::ValueMask_Input
		|	PIN_CNF::INPUT::ValueMask_Connect
		|	PIN_CNF::PULL::ValueMask_Pullup
		|	PIN_CNF::DRIVE::ValueMask_H0H1
		|	PIN_CNF::SENSE::ValueMask_Disabled
		;

	OsclCpuInOrderMemoryAccessBarrier();
	}

void	Driver::initDcxPin() noexcept{

	using namespace Oscl::Nordic::SPIM;

	if(_dcxPort == 0xFF){
		return;
		}

	_reg.pseldcx	=
			(_dcxPort << PSEL_MOSI::PORT::Lsb)
		|	(_dcxPin << PSEL_MOSI::PIN::Lsb)
		|	PSEL_MOSI::CONNECT::ValueMask_Connect
		;

	OsclCpuInOrderMemoryAccessBarrier();
	}
void	Driver::initCsnPin() noexcept{

	using namespace Oscl::Nordic::SPIM;

	if(_csnPort == 0xFF){
		return;
		}

	_reg.pselcsn	=
			(_csnPort << PSEL_MOSI::PORT::Lsb)
		|	(_csnPin << PSEL_MOSI::PIN::Lsb)
		|	PSEL_MOSI::CONNECT::ValueMask_Connect
		;

	OsclCpuInOrderMemoryAccessBarrier();

	_reg.csnpol	= CSNPOL::CSNPOL::ValueMask_LOW;	// FIXME: variable active state

	_reg.iftimingcsndur	= 1;	// FIXME: variable duration

	OsclCpuInOrderMemoryAccessBarrier();
	}

void	Driver::initSckPin() noexcept{

	using namespace Oscl::Nordic::SPIM;

	if(_sckPort == 0xFF){
		// NOT optional!
		Oscl::ErrorFatal::logAndExit(
			"%s: SCK required!\n",
			OSCL_PRETTY_FUNCTION
			);
		return;
		}

	_reg.pselsck	=
			(_sckPort << PSEL_MOSI::PORT::Lsb)
		|	(_sckPin << PSEL_MOSI::PIN::Lsb)
		|	PSEL_MOSI::CONNECT::ValueMask_Connect
		;

	OsclCpuInOrderMemoryAccessBarrier();
	}

void	Driver::initMosiPin() noexcept{

	using namespace Oscl::Nordic::SPIM;

	if(_mosiPort == 0xFF){
		return;
		}

	_reg.pselmosi	=
			(_mosiPort << PSEL_MOSI::PORT::Lsb)
		|	(_mosiPin << PSEL_MOSI::PIN::Lsb)
		|	PSEL_MOSI::CONNECT::ValueMask_Connect
		;

	OsclCpuInOrderMemoryAccessBarrier();
	}

void	Driver::initMisoPin() noexcept{

	using namespace Oscl::Nordic::SPIM;

	if(_misoPort == 0xFF){
		return;
		}

	_reg.pselmiso	=
			(_misoPort << PSEL_MISO::PORT::Lsb)
		|	(_misoPin << PSEL_MISO::PIN::Lsb)
		|	PSEL_MISO::CONNECT::ValueMask_Connect
		;

	OsclCpuInOrderMemoryAccessBarrier();
	}

void    Driver::start() noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif

	using namespace Oscl::Nordic::SPIM;

	OsclCpuInOrderMemoryAccessBarrier();

	/*
		According to the diagram:
		Figure 157: SPIM — SPI master with EasyDMA

		The GPIO pins for the following registers
		are required by the SPIM as "inputs":
		- SCK
		- CSN
		- DCX

		Therefore, it is unacceptable to *not* assign/configure
		GPIO pins to these functions.

		In particular, the feedback from the CSN and SCK signals
		may be *required* to enable data output on MOSI.

		Also, based on:
		Table 102: GPIO configuration

		It may be that the GPIO output value, must be set
		"Same as CONFIG.CPOL".

		WARNING! Since the GPIO pin configuration must be done
		while the SPIM is *disabled*, this may affect the
		per-transaction CONFIG setup (frequency,order,cpol,cpha)
		to be modified such that the GPIO output (OUTSET,OUTCLR)
		is modified according to CONFIG.CPOL.

		/home/mike/root/src/mnm/btrelay/adafruit/feather/btrelayinit.cpp
	 */

	OsclCpuInOrderMemoryAccessBarrier();

	initSckGpio();
	initMosiGpio();
	initMisoGpio();
	initCsnGpio();
	initDcxGpio();

	initCsnPin();
	initDcxPin();

	initSckPin();
	initMosiPin();
	initMisoPin();

	_reg.frequency	= _frequencyRegValue;

	OsclCpuInOrderMemoryAccessBarrier();

	_reg.config		= _configRegValue;

	OsclCpuInOrderMemoryAccessBarrier();

	/*	6.1.3 Peripheral registers
		Most peripherals feature an ENABLE register.
		Unless otherwise specified in the relevant chapter, the
		peripheral registers (in particular the PSEL registers)
		must be configured before enabling the peripheral.

		6.25.3 Pin configuration

		The SCK, CSN, DCX, MOSI, and MISO signals associated with
		the SPIM are mapped to physical pins according to the
		configuration specified in the PSEL.n registers.
		The contents of registers PSEL.SCK on page 416,
		PSEL.CSN on page 417, PSELDCX on page 420,
		PSEL.MOSI on page 416 and PSEL.MISO on page 416
		are only used when the SPIM is enabled and retained
		only as long as the device is in System ON mode.
		The PSEL.n registers can only be configured when
		the SPIM is disabled.  Enabling/disabling is done
		using register ENABLE on page 415.

		To ensure correct behavior, the pins used by the SPIM
		must be configured in the GPIO peripheral as
		described in GPIO configuration on page 409 before
		the SPIM is enabled.

		Only one peripheral can be assigned to drive a
		particular GPIO pin at a time. Failing to do so
		may result in unpredictable behavior.
	 */

	_reg.enable	= ENABLE::ENABLE::ValueMask_Enabled;

	OsclCpuInOrderMemoryAccessBarrier();
	}

void    Driver::request(OpenReq& msg) noexcept{
	start();
	msg.returnToSender();
	}

void	Driver::mboxSignaled() noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif

	using namespace Oscl::Nordic::SPIM;

	/*	At this point all interrupts have been
		disabled by the ISR.
	 */

	if(_reg.end & EVENTS_END::END::ValueMask_Generated){
		_reg.end	= EVENTS_END::END::ValueMask_NotGenerated;

		if(_currentTransferMsg){
			_currentTransferMsg->getPayload()._failed	= false;
			_currentTransferMsg->returnToSender();
			_currentTransferMsg	= 0;
			}
		}

	else if(_reg.stallstat & EVENTS_END::END::ValueMask_Generated){
		_reg.end	= EVENTS_END::END::ValueMask_NotGenerated;

		if(_currentTransferMsg){
			_currentTransferMsg->getPayload()._failed	= false;
			_currentTransferMsg->returnToSender();
			_currentTransferMsg	= 0;
			}
		}

	processNextReq();
	}

/**	RETURN: true for failure to support
	required values.
 */
static bool	processConfig(
				unsigned long			frequencyInHz,
				bool					lsbFirst,
				bool					cpol,
				bool					cpha,
				Oscl::Nordic::
				SPIM::FREQUENCY::Reg&	frequencyRegValue,
				Oscl::Nordic::
				SPIM::CONFIG::Reg&		configRegValue
				) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif

	using namespace Oscl::Nordic::SPIM;

	FREQUENCY::Reg	freq	= 0;

	switch(frequencyInHz){
		case 125000:
			freq	= FREQUENCY::K125;
			break;
		case 250000:
			freq	= FREQUENCY::K250;
			break;
		case 500000:
			freq	= FREQUENCY::K500;
			break;
		case 1000000:
			freq	= FREQUENCY::M1;
			break;
		case 2000000:
			freq	= FREQUENCY::M2;
			break;
		case 4000000:
			freq	= FREQUENCY::M4;
			break;
		case 8000000:
			freq	= FREQUENCY::M8;
			break;
		case 16000000:
			freq	= FREQUENCY::M16;
			break;
		case 32000000:
			freq	= FREQUENCY::M32;
			break;
		default:
			return true;
		}

	frequencyRegValue	= freq;

	configRegValue =
			lsbFirst?CONFIG::ORDER::ValueMask_LsbFirst:CONFIG::ORDER::ValueMask_MsbFirst
		|	cpol?CONFIG::CPOL::ValueMask_ActiveLow:CONFIG::CPOL::ValueMask_ActiveHigh
		|	cpha?CONFIG::CPHA::ValueMask_Trailing:CONFIG::CPHA::ValueMask_Leading
		;

	return false;
	}

void	Driver::processNextReq() noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif

	using namespace Oscl::Nordic::SPIM;

	if(_currentTransferMsg){
		return;
		}

	while((_currentTransferMsg = _pending.get())){

		/* FIXME: IMPLEMENT
			1. Setup SPI configuration
				a. FREQUENCY
				b. CONFIG.CPOL
				c. CONFIG.CPHA
				d. CONFIG.ORDER
			2. Select device
				msg.getPayload()._deviceSelectApi.select();
			3. Start transfer
		 */

		Oscl::SPI::Master::Req::Api::TransferPayload&
		payload	= _currentTransferMsg->getPayload();

		FREQUENCY::Reg	frequencyRegValue;

		CONFIG::Reg		configRegValue;

		if(payload._frequency){
			bool
			failed	= processConfig(
						payload._frequency,
						payload._lsbFirst,
						payload._cpol,
						payload._cpha,
						frequencyRegValue,
						configRegValue
						);

			if(failed){

				#ifdef DEBUG_TRACE
				Oscl::Error::Info::log(
					"%s: processConfig() failed.\n",
					OSCL_PRETTY_FUNCTION
					);
				#endif

				payload._failed	= true;
				_currentTransferMsg->returnToSender();
				continue;
				}

			updateConfig(
				frequencyRegValue,
				configRegValue
				);
			}

		/** At this point, we are ready to start
			the new transaction.
		 */
		payload._deviceSelectApi.select();

		_reg.stallstat	=
				STALLSTAT::TX::ValueMask_NoStall
			|	STALLSTAT::RX::ValueMask_NoStall
			;

		/*
			The reinterpret_cast operators are used to quiet
			the compiler on x86_64 machines which are used to
			simply see that the file will compile.
			On the target ARM machine (Nordic nRF52), these
			casts are not necessary.
		 */
		_reg.txdptr		= reinterpret_cast<uintptr_t>(payload._txDataBuffer);
		_reg.rxdptr		= reinterpret_cast<uintptr_t>(payload._rxDataBuffer);

		_reg.txdmaxcnt	= payload._nDataBytesToTransfer;
		_reg.rxdmaxcnt	= payload._nDataBytesToTransfer;

		disableAllInterrupts();

		OsclCpuInOrderMemoryAccessBarrier();

		_reg.intenset	= INTENSET::END::ValueMask_Set;

		OsclCpuInOrderMemoryAccessBarrier();

		_reg.start	= TASKS_START::START::ValueMask_Trigger;

		/*
			The END event will be generated after both
			the ENDRX and ENDTX events have been generated.
		 */

		#ifdef DEBUG_TRACE
		Oscl::Error::Info::log(
			"%s: started.\n",
			OSCL_PRETTY_FUNCTION
			);
		#endif

		return;
		}

	}

void	Driver::updateConfig(
			Oscl::Nordic::
			SPIM::FREQUENCY::Reg	frequencyRegValue,
			Oscl::Nordic::
			SPIM::CONFIG::Reg		configRegValue
			) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif

	if(frequencyRegValue == _frequencyRegValue) {
		if(configRegValue == _configRegValue) {
			// No change in config.
			return;
			}
		}

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s: Update\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif

	/*
		At this point, the config has changed,
		so update the cache and the registers.
	 */

	_frequencyRegValue	= frequencyRegValue;
	_configRegValue		= configRegValue;

	_reg.config		= _configRegValue;

	OsclCpuInOrderMemoryAccessBarrier();

	_reg.frequency	= _frequencyRegValue;

	OsclCpuInOrderMemoryAccessBarrier();
	}

void	Driver::request(Oscl::SPI::Master::Req::Api::TransferReq& msg) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif

	_pending.put(&msg);

	processNextReq();
	}

void	Driver::request(Oscl::SPI::Master::Req::Api::CancelTransferReq& msg) noexcept{
	// FIXME:
	Oscl::ErrorFatal::logAndExit(
		"%s: NOT IMPLEMENTED.\n",
		OSCL_PRETTY_FUNCTION
		);
	}

