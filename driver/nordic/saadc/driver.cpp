/*
   Copyright (C) 2020 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "oscl/hw/nordic/saadc/reg.h"
#include "driver.h"
#include "oscl/error/fatal.h"
#include "oscl/error/info.h"
#include "oscl/cpu/sync.h"

using namespace Oscl::Nordic::SAADC;

//#define DEBUG_TRACE

Channel::Channel(
	Oscl::Nordic::SAADC::PSELP::Reg		pselp,
	Oscl::Nordic::SAADC::PSELN::Reg		pseln,
	Oscl::Nordic::SAADC::CONFIG::Reg	config,
	Oscl::Nordic::SAADC::LIMIT::Reg		limit
	) noexcept:
		_pselp(pselp),
		_pseln(pseln),
		_config(config),
		_limit(limit)
		{
	}


Driver::Driver(
	Oscl::Nordic::SAADC::Map&	reg,
	void						(osInitHook)(void*),
	void*						osInitHookArg
	) noexcept:
		OpenSync(
			*this,
			*this
			),
		_reg(reg),
		_sampleComposer(
			*this,
			&Driver::sampleRequest
			),
		_sampleSync(
			*this,
			_sampleComposer
			),
		_calibrateComposer(
			*this,
			&Driver::calibrateRequest
			),
		_calibrateSync(
			*this,
			_calibrateComposer
			),
		_nActiveChannels(0),
		_starting(true),
		_calibrating(false),
		_sampling(false),
		_samplePending(true),
		_calibrationPending(true),
		_stopping(false),
		_stopped(true)
		{
	disableAllInterrupts();

	if(osInitHook){
		osInitHook(osInitHookArg);
		}

	for(unsigned i=0;i<maxChannels;++i){
		_channels[i]	= 0;
		}
	}

void	Driver::set(Channel* handler, unsigned channel) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif


	if(channel >= maxChannels){
		Oscl::ErrorFatal::logAndExit(
			"%s: invalid channel.\n",
			OSCL_PRETTY_FUNCTION
			);
		}

	if(_channels[channel]){
		Oscl::ErrorFatal::logAndExit(
			"%s: channel [%u] already in use.\n",
			OSCL_PRETTY_FUNCTION,
			channel
			);
		}

	_channels[channel]	= handler;
	}

void	Driver::disableAllInterrupts() noexcept {

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif

	using namespace Oscl::Nordic::SAADC;
	_reg.intenclr	=
			INTENCLR::STARTED::ValueMask_Clear
		|	INTENCLR::END::ValueMask_Clear
		|	INTENCLR::DONE::ValueMask_Clear
		|	INTENCLR::RESULTDONE::ValueMask_Clear
		|	INTENCLR::CALIBRATEDONE::ValueMask_Clear
		|	INTENCLR::STOPPED::ValueMask_Clear
		|	INTENCLR::CH0LIMITH::ValueMask_Clear
		|	INTENCLR::CH1LIMITH::ValueMask_Clear
		|	INTENCLR::CH2LIMITH::ValueMask_Clear
		|	INTENCLR::CH3LIMITH::ValueMask_Clear
		|	INTENCLR::CH4LIMITH::ValueMask_Clear
		|	INTENCLR::CH5LIMITH::ValueMask_Clear
		|	INTENCLR::CH6LIMITH::ValueMask_Clear
		|	INTENCLR::CH7LIMITH::ValueMask_Clear
		|	INTENCLR::CH0LIMITL::ValueMask_Clear
		|	INTENCLR::CH1LIMITL::ValueMask_Clear
		|	INTENCLR::CH2LIMITL::ValueMask_Clear
		|	INTENCLR::CH3LIMITL::ValueMask_Clear
		|	INTENCLR::CH4LIMITL::ValueMask_Clear
		|	INTENCLR::CH5LIMITL::ValueMask_Clear
		|	INTENCLR::CH6LIMITL::ValueMask_Clear
		|	INTENCLR::CH7LIMITL::ValueMask_Clear
		;

	OsclCpuInOrderMemoryAccessBarrier();
	}

void	Driver::enablePeripheral() noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif

	using namespace Oscl::Nordic::SAADC;

	_reg.enable	=
		(ENABLE::ENABLE::Value_Enabled << ENABLE::ENABLE::Lsb)
		;
	}

void	Driver::setupDMA() noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif

	using namespace Oscl::Nordic::SAADC;

	_reg.resultptr		= (Oscl::Nordic::SAADC::RESULTPTR::Reg)_samples;

	_reg.resultmaxcnt	= _nActiveChannels;

	for(unsigned i=0;i<maxChannels;++i){
		_samples[i]	= 0xFFFF;
		}

	}

void	Driver::startPeripheral() noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif

	using namespace Oscl::Nordic::SAADC;

	setupDMA();

	OsclCpuInOrderMemoryAccessBarrier();

	_reg.start	=
		(TASKS_START::START::Value_Trigger << TASKS_START::START::Lsb)
		;

	_starting	= true;
	_stopped	= false;

	OsclCpuInOrderMemoryAccessBarrier();

	_reg.intenset	= INTENSET::STARTED::ValueMask_Set;

	}

void	Driver::processStarted() noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif

	_reg.started	= EVENTS_STARTED::STARTED::ValueMask_NotGenerated;

	if(!_starting){
		return;
		}

	_starting	= false;

	OsclCpuInOrderMemoryAccessBarrier();

	if(_calibrationPending){
		startCalibration();
		}
	else if(_samplePending){
		startSample();
		}
	else {
		startStop();
		}
	}

void	Driver::startSample() noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif

	using namespace Oscl::Nordic::SAADC;

//	setupDMA();	Now done in the startPeripheral()

	_reg.sample	=
		(TASKS_SAMPLE::SAMPLE::Value_Trigger << TASKS_SAMPLE::SAMPLE::Lsb)
		;

	_sampling		= true;
	_samplePending	= false;

	OsclCpuInOrderMemoryAccessBarrier();

	_reg.intenset	= INTENSET::END::ValueMask_Set;
	}

void	Driver::processEnd() noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif

	using namespace Oscl::Nordic::SAADC;

	_reg.end	= EVENTS_END::END::ValueMask_NotGenerated;

	OsclCpuInOrderMemoryAccessBarrier();

	if(_stopping || _calibrating || _starting){
		return;
		}

	if(_sampling){

		_sampling	= false;

		unsigned	sampleOffset	= 0;

		for(unsigned i=0;i<maxChannels;++i){
			if(!_channels[i]){
				continue;
				}
			#ifdef DEBUG_TRACE
			Oscl::Error::Info::log(
				"%s: channel: %u, _samples[%u]: 0x%4.4X\n",
				OSCL_PRETTY_FUNCTION,
				i,
				sampleOffset,
				_samples[sampleOffset]
				);
			Oscl::Error::Info::hexDump(
				_samples,
				sizeof(_samples)
				);
			#endif

			_channels[i]->update(_samples[sampleOffset]);
			++sampleOffset;
			}
		}

	if(_stopped){
		return;
		}

	if(!_calibrating){
		startStop();
		}
	}

void	Driver::startCalibration() noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif

	using namespace Oscl::Nordic::SAADC;

	_reg.calibrateOffset	=
		TASKS_CALIBRATEOFFSET::CALIBRATE::Value_Trigger << TASKS_CALIBRATEOFFSET::CALIBRATE::Lsb
		;

	_calibrating		= true;
	_calibrationPending	= false;

	OsclCpuInOrderMemoryAccessBarrier();

	_reg.intenset	= INTENSET::CALIBRATEDONE::ValueMask_Set;

	OsclCpuInOrderMemoryAccessBarrier();
	}

void	Driver::processCalibration() noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif

	_reg.calibrateDone	= EVENTS_CALIBRATEDONE::CALIBRATEDONE::ValueMask_NotGenerated;

	OsclCpuInOrderMemoryAccessBarrier();

	_calibrating	= false;

	startStop();
	}

void	Driver::startStop() noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif

	using namespace Oscl::Nordic::SAADC;

	_reg.stop	=
		(TASKS_STOP::STOP::Value_Trigger << TASKS_STOP::STOP::Lsb)
		;

	_stopping		= true;

	OsclCpuInOrderMemoryAccessBarrier();

	_reg.intenset	= INTENSET::STOPPED::ValueMask_Set;
	}

void	Driver::processStopped() noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif

	_reg.stopped	= EVENTS_STOPPED::STOPPED::ValueMask_NotGenerated;

	OsclCpuInOrderMemoryAccessBarrier();

	_stopping	= false;
	_stopped	= true;

	if(_calibrationPending){
		startPeripheral();
		}
	else if(_samplePending){
		startPeripheral();
		}
	}

Oscl::Mt::Runnable&	Driver::getDriverRunnable() noexcept{
	return *this;
	}

Oscl::Mt::Sema::SignalApi&	Driver::getSignalApi() noexcept{
	return *this;
	}

Oscl::Event::Control::Api&	Driver::getSampleApi() noexcept{
	return _sampleSync;
	}

Oscl::Event::Control::Api&	Driver::getCalibrateApi() noexcept{
	return _calibrateSync;
	}

void    Driver::configureChannels() noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif

	_nActiveChannels	= 0;

	for(unsigned i=0;i<maxChannels;++i){
		if(!_channels[i]){
			continue;
			}
		Channel&	channel	= *_channels[i];
		_reg.ch[i].pselp	= channel._pselp;
		_reg.ch[i].pseln	= channel._pseln;
		_reg.ch[i].config	= channel._config;
		_reg.ch[i].limit	= channel._limit;
		++_nActiveChannels;

		#ifdef DEBUG_TRACE
		Oscl::Error::Info::log(
			"%s: channel [%u]\n"
			" pselp: 0x%8.8X\n"
			" pseln: 0x%8.8X\n"
			" config: 0x%8.8X\n"
			" limit: 0x%8.8X\n"
			" _nActiveChannels: %u\n"
			"",
			OSCL_PRETTY_FUNCTION,
			i,
			_reg.ch[i].pselp,
			_reg.ch[i].pseln,
			_reg.ch[i].config,
			_reg.ch[i].limit,
			_nActiveChannels
			);
		#endif

		}

	OsclCpuInOrderMemoryAccessBarrier();
	}

void    Driver::start() noexcept{
	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif

	// FIXME: This is where I start-up the SAADC
	// based on individual channel configurations.

	enablePeripheral();

	configureChannels();

	using namespace Oscl::Nordic::SAADC;

	_reg.resolution	=
		RESOLUTION::VAL::Value_Val14bit	<< RESOLUTION::VAL::Lsb
		;

	_reg.oversample	=
		OVERSAMPLE::OVERSAMPLE::Value_Bypass << OVERSAMPLE::OVERSAMPLE::Lsb
		;

	_reg.samplerate	=
			SAMPLERATE::CC::Value_Default	<< SAMPLERATE::CC::Lsb
		|	SAMPLERATE::MODE::Value_Task	<< SAMPLERATE::MODE::Lsb
		;


	startPeripheral();
	}

void    Driver::request(OpenReq& msg) noexcept{
	start();
	msg.returnToSender();
	}

void	Driver::mboxSignaled() noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif

	using namespace Oscl::Nordic::SAADC;

	if(_reg.end & EVENTS_END::END::ValueMask_Generated){
		processEnd();
		}

	if(_reg.calibrateDone & EVENTS_CALIBRATEDONE::CALIBRATEDONE::ValueMask_Generated){
		processCalibration();
		}

	if(_reg.started & EVENTS_STARTED::STARTED::ValueMask_Generated){
		processStarted();
		}

	if(_reg.stopped & EVENTS_STOPPED::STOPPED::ValueMask_Generated){
		processStopped();
		}

	}

bool	Driver::busy() noexcept{
	return (_sampling || _starting || _calibrating || _stopping);
	}

void	Driver::sampleRequest(Oscl::Event::Control::Req::Api::TriggerReq& msg) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif

	_samplePending	= true;

	if(!busy()){
		startPeripheral();
		}

	msg.returnToSender();
	}

void	Driver::calibrateRequest(Oscl::Event::Control::Req::Api::TriggerReq& msg) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif

	_calibrationPending	= true;

	if(!busy()){
		startPeripheral();
		}

	msg.returnToSender();
	}
