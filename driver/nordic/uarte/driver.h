/*
   Copyright (C) 2020 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_driver_nordic_uarte_driverh_
#define _oscl_driver_nordic_uarte_driverh_

#include "oscl/hw/nordic/uarte/map.h"
#include "oscl/mt/itc/srv/ocsyncapi.h"
#include "oscl/mt/itc/mbox/server.h"
#include "oscl/serial/itc/tx/driver.h"
#include "oscl/serial/driver/tx/api.h"

/** */
namespace Oscl {
/** */
namespace Nordic {
/** */
namespace UARTE {

/** */
class Driver :	
	public Oscl::Mt::Itc::Srv::OpenCloseSyncApi,
	private Oscl::Serial::Driver::TX::Api
	{
	private:
		/** */
		Oscl::Nordic::UARTE::Map&			_reg;

		/** */
		const uint8_t						_txPort;

		/** */
		const uint8_t						_txPin;

		/** */
		const uint8_t						_rxPort;

		/** */
		const uint8_t						_rxPin;

	private:
		/** */
		Oscl::Serial::Itc::TX::Driver		_txDriver;

	private:
		/** */
		Oscl::Done::Api*					_txComplete;

	public:
		/** */
		Driver(
			Oscl::Nordic::UARTE::Map&		reg,
			Oscl::Serial::Itc::TX::DoneMem	doneMem[],
			unsigned						nDoneMem,
			uint8_t							txPort,
			uint8_t							txPin,
			uint8_t							rxPort,
			uint8_t							rxPin,
			void							(osInitHook)(void*)	= 0,
			void*							osInitHookArg	= 0,
			bool							enableHardwareFlowControl	=  false
			) noexcept;

		/** */
		virtual ~Driver(){}

		/** */
		Oscl::Mt::Runnable&						getTxDriverRunnable() noexcept;

		/** */
		Oscl::Stream::Output::Req::Api::SAP&	getTxDriverSAP() noexcept;

		/** */
		Oscl::Stream::Output::Api&				getTxSyncApi() noexcept;

		/** RETURN: Reference to the SignalApi to
			be used to notify the driver when a
			transmit interrupt has happened.
		 */
		Oscl::Mt::Sema::SignalApi&				getTxSignalApi() noexcept;

	public:	// Oscl::Mt::Itc::Srv::OpenCloseSyncApi
		/** */
		void	syncOpen() noexcept;

		/** */
		void	syncClose() noexcept;

//	private:
	public:
		/** */
		void	initialize() noexcept;

	private: // Oscl::Serial::Driver::TX::Api
		/** This operation is used to queue a buffer for transmission.
			When the buffer has been transmitted successfully, the
			callback is invoked. The operation returns true if the
			buffer is queued for transmission, or false if the
			transmitter does not currently have the resources available
			to queue the buffer. In the latter case, the client should
			try to queue the buffer again after another buffer has
			completed transmission.
		 */
		bool	sendBuffer(
					Oscl::Done::Api&	complete,
					const void*			buffer,
					unsigned			length
					) noexcept;

		/** This operation is invoked by the driver as a result
			of a transmitter interrupt. The implementation shall
			take whatever actions are required, including
			queing buffers or characters for transmission, and
			invoking the "done" operation for buffers with which
			it is finished. Buffers shall always be returned in
			the order in which they are given to the driver using
			the sendBuffer() operation.
		 */
		void	processTransmitter() noexcept;
	};

}
}
}

#endif
