/*
   Copyright (C) 2020 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "oscl/hw/nordic/uarte/reg.h"
#include "driver.h"
#include "oscl/error/fatal.h"
#include "oscl/cpu/sync.h"

using namespace Oscl::Nordic::UARTE;

Driver::Driver(
	Oscl::Nordic::UARTE::Map&		reg,
	Oscl::Serial::Itc::TX::DoneMem	doneMem[],
	unsigned						nDoneMem,
	uint8_t							txPort,
	uint8_t							txPin,
	uint8_t							rxPort,
	uint8_t							rxPin,
	void							(osInitHook)(void*),
	void*							osInitHookArg,
	bool							enableHardwareFlowControl
	) noexcept:
		_reg(reg),
		_txPort(txPort),
		_txPin(txPin),
		_rxPort(rxPort),
		_rxPin(rxPin),
		_txDriver(
			*this,
			doneMem,
			nDoneMem
			),
		_txComplete(0)
		{

	if(osInitHook){
		osInitHook(osInitHookArg);
		}

	}

unsigned long	loopCounter;

void Driver::initialize() noexcept{

	using namespace Oscl::Nordic::UARTE;
	_reg.intenclr	=
			INTENCLR::CTS::ValueMask_Clear
		|	INTENCLR::NCTS::ValueMask_Clear
		|	INTENCLR::RXDRDY::ValueMask_Clear
		|	INTENCLR::ENDRX::ValueMask_Clear
		|	INTENCLR::TXDRDY::ValueMask_Clear
		|	INTENCLR::ENDTX::ValueMask_Clear
		|	INTENCLR::ERROR::ValueMask_Clear
		|	INTENCLR::RXTO::ValueMask_Clear
		|	INTENCLR::RXSTARTED::ValueMask_Clear
		|	INTENCLR::TXSTARTED::ValueMask_Clear
		|	INTENCLR::TXSTOPPED::ValueMask_Clear
		;

	_reg.config	=
			CONFIG::HWFC::ValueMask_Disabled
		|	CONFIG::PARITY::ValueMask_Excluded
		|	CONFIG::STOP::ValueMask_One
		;

	OsclCpuInOrderMemoryAccessBarrier();
	_reg.baudrate	=
		BAUDRATE::BAUDRATE::ValueMask_Baud115200
		;

	#undef PSELTXD
	#undef PSELRXD
	#undef TXDPTR

	OsclCpuInOrderMemoryAccessBarrier();
	/*
		To secure correct signal levels on the pins by the UARTE
		when the system is in OFF mode, the pins must be configured
		in the GPIO peripheral as described in GPIO configuration
		before enabling peripheral on page 516.

		UARTE signal	UARTE pin					Direction	Output value
		RXD				As specified in PSEL.RXD	Input		Not applicable
		CTS				As specified in PSEL.CTS	Input		Not applicable
		RTS				As specified in PSEL.RTS	Output		1
		TXD				As specified in PSEL.TXD	Output		1
	 */
	_reg.pseltxd	=
			(_txPort << PSELTXD::PORT::Lsb)
		|	(_txPin << PSELTXD::PIN::Lsb)
		|	PSELTXD::CONNECT::ValueMask_Connected
		;
	_reg.pselrxd	=
			(_rxPort << PSELRXD::PORT::Lsb)
		|	(_rxPin << PSELRXD::PIN::Lsb)
		|	PSELRXD::CONNECT::ValueMask_Connected
		;
	OsclCpuInOrderMemoryAccessBarrier();
	_reg.enable	= ENABLE::ENABLE::ValueMask_Enabled;
	OsclCpuInOrderMemoryAccessBarrier();
	}

Oscl::Mt::Runnable&	Driver::getTxDriverRunnable() noexcept{
	return _txDriver;
	}

Oscl::Stream::Output::Req::Api::SAP&	Driver::getTxDriverSAP() noexcept{
	return _txDriver.getSAP();
	}

Oscl::Stream::Output::Api&	Driver::getTxSyncApi() noexcept{
	return _txDriver.getSyncApi();
	}

Oscl::Mt::Sema::SignalApi&	Driver::getTxSignalApi() noexcept{
	return _txDriver;
	}

void	Driver::syncOpen() noexcept{
	initialize();
	_txDriver.syncOpen();
	}

void	Driver::syncClose() noexcept{
	_txDriver.syncClose();
	}

bool	Driver::sendBuffer(
			Oscl::Done::Api&	complete,
			const void*			buffer,
			unsigned			length
			) noexcept{
	/** This operation is used to queue a buffer for transmission.
		When the buffer has been transmitted successfully, the
		callback is invoked. The operation returns true if the
		buffer is queued for transmission, or false if the
		transmitter does not currently have the resources available
		to queue the buffer. In the latter case, the client should
		try to queue the buffer again after another buffer has
		completed transmission.
	 */

	/*
		As much as possible, we want to allow the UARTE
		Easy DMA to work autonomously.

		Therefore, we will maintain a set of buffers
		(at least 2) within this driver.

		When one of these buffers is available, then we will:

		1.	copy the content of buffer parameter into that buffer.
		2.	Queue that buffer for transmission when the UARTE
			transmitter is ready.

		How do we deal with the local buffer being too small?

		-	We can stash the buffer and length parameters locally and
			send segments as the local buffers are freed unil
			we hae sent the full contents. At that point,
			we can invoe the complete callback.
		-	In the mean-time, we can just reurn false.

		Otherwise, we will return false.
	 */

	if(_txComplete){
		return false;
		}

	_txComplete	= &complete;

	_reg.txdptr	= (Oscl::Nordic::UARTE::TXDPTR::Reg)buffer;
	_reg.txdmaxcnt	= length;

	OsclCpuInOrderMemoryAccessBarrier();

	_reg.starttx	= TASKS_STARTTX::STARTTX::ValueMask_Trigger;

	OsclCpuInOrderMemoryAccessBarrier();

	using namespace Oscl::Nordic::UARTE;
	_reg.intenset	=
		INTENSET::ENDTX::ValueMask_Set
		;

	return true;
	}

void	Driver::processTransmitter() noexcept{
	/** This operation is invoked by the driver as a result
		of a transmitter interrupt. The implementation shall
		take whatever actions are required, including
		queing buffers or characters for transmission, and
		invoking the "done" operation for buffers with which
		it is finished. Buffers shall always be returned in
		the order in which they are given to the driver using
		the sendBuffer() operation.
	 */

	_reg.endtx	= 0;

	Oscl::Done::Api*
	done	= _txComplete;

	_txComplete	= 0;

	done->done();
	}

