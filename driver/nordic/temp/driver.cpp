/*
   Copyright (C) 2020 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "oscl/hw/nordic/temp/reg.h"
#include "driver.h"
#include "oscl/cpu/sync.h"

using namespace Oscl::Nordic::TEMP;

//#define DEBUG_TRACE

#ifdef DEBUG_TRACE
#include "oscl/error/info.h"
#endif

Driver::Driver(
	Oscl::Nordic::TEMP::Map&	reg,
	void						(osInitHook)(void*),
	void*						osInitHookArg
	) noexcept:
		OpenSync(
			*this,
			*this
			),
		_reg(reg),
		_sampleComposer(
			*this,
			&Driver::sampleRequest
			),
		_sampleSync(
			*this,
			_sampleComposer
			),
		_output(
			*this
			),
		_sampling(false)
		{
	disableAllInterrupts();

	if(osInitHook){
		osInitHook(osInitHookArg);
		}
	}

Oscl::Double::Observer::Req::Api::SAP&	Driver::getTemperatureInCelsiusSAP() noexcept{
	return _output.getSAP();
	}

void	Driver::disableAllInterrupts() noexcept {

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif

	using namespace Oscl::Nordic::TEMP;
	_reg.intenclr	=
			INTENCLR::DATARDY::ValueMask_Clear
		;

	OsclCpuInOrderMemoryAccessBarrier();
	}

void	Driver::startSample() noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif

	using namespace Oscl::Nordic::TEMP;

	_reg.start	=
		(TASKS_START::START::Value_Trigger << TASKS_START::START::Lsb)
		;

	_sampling		= true;

	OsclCpuInOrderMemoryAccessBarrier();

	_reg.intenset	= INTENSET::DATARDY::ValueMask_Set;
	}

void	Driver::processDatardy() noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif

	using namespace Oscl::Nordic::TEMP;

	_reg.datardy	= EVENTS_DATARDY::DATARDY::ValueMask_NotGenerated;

	OsclCpuInOrderMemoryAccessBarrier();

	if(_sampling){

		_sampling	= false;

		int32_t
		temperatureInCelsiusDiv4	= _reg.temp;

		double
		temperatureInCelsius	= temperatureInCelsiusDiv4;
		temperatureInCelsius	/= 4.0;

		#ifdef DEBUG_TRACE
		Oscl::Error::Info::log(
			"%s: TEMP: 0x%8.8X, %u, %f C\n",
			OSCL_PRETTY_FUNCTION,
			temperatureInCelsiusDiv4,
			temperatureInCelsiusDiv4,
			temperatureInCelsius
			);
		#endif
		_output.update(temperatureInCelsius);
		}
	}

Oscl::Mt::Runnable&	Driver::getDriverRunnable() noexcept{
	return *this;
	}

Oscl::Mt::Sema::SignalApi&	Driver::getSignalApi() noexcept{
	return *this;
	}

Oscl::Event::Control::Api&	Driver::getSampleApi() noexcept{
	return _sampleSync;
	}

void    Driver::start() noexcept{
	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif
	}

void    Driver::request(OpenReq& msg) noexcept{
	start();
	msg.returnToSender();
	}

void	Driver::mboxSignaled() noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif

	using namespace Oscl::Nordic::TEMP;

	if(_reg.datardy & EVENTS_DATARDY::DATARDY::ValueMask_Generated){
		processDatardy();
		}
	}

bool	Driver::busy() noexcept{
	return (_sampling);
	}

void	Driver::sampleRequest(Oscl::Event::Control::Req::Api::TriggerReq& msg) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif

	if(!busy()){
		startSample();
		}

	msg.returnToSender();
	}

