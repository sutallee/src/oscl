/*
   Copyright (C) 2020 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_driver_nordic_temp_driverh_
#define _oscl_driver_nordic_temp_driverh_

#include "oscl/mt/itc/mbox/server.h"
#include "oscl/hw/nordic/temp/map.h"
#include "oscl/mt/itc/srv/openapi.h"
#include "oscl/mt/itc/srv/open.h"
#include "oscl/mt/itc/mbox/server.h"
#include "oscl/event/control/reqcomp.h"
#include "oscl/event/control/sync.h"
#include "oscl/double/output/part.h"

/** */
namespace Oscl {
/** */
namespace Nordic {
/** */
namespace TEMP {

/** */
class Driver :	
	public Oscl::Mt::Itc::Server,
	public Oscl::Mt::Itc::Srv::OpenSync,
	private Oscl::Mt::Itc::Srv::Open::Req::Api
	{
	private:
		/** */
		Oscl::Nordic::TEMP::Map&		_reg;

	private:
		/** */
		Oscl::Event::Control::
		Req::Composer<Driver>		_sampleComposer;

		/** */
		Oscl::Event::Control::Sync	_sampleSync;

	private:
		/** */
		Oscl::Double::Output::Part	_output;

	private:
		/** */
		bool						_sampling;

	public:
		/** */
		Driver(
			Oscl::Nordic::TEMP::Map&	reg,
			void						(osInitHook)(void*),
			void*						osInitHookArg
			) noexcept;

		/** */
		virtual ~Driver(){}

		/** */
		Oscl::Double::Observer::Req::Api::SAP&	getTemperatureInCelsiusSAP() noexcept;

		/** */
		Oscl::Mt::Runnable&						getDriverRunnable() noexcept;

		/** RETURN: Reference to the SignalApi to
			be used to notify the driver when a
			transmit interrupt has happened.
		 */
		Oscl::Mt::Sema::SignalApi&				getSignalApi() noexcept;

		/** RETURN: Reference to the Api
			used to start a sampling sequence.
		 */
		Oscl::Event::Control::Api&				getSampleApi() noexcept;

	private: // Oscl::Mt::Itc::Server
		/** */
		void	mboxSignaled() noexcept;

	private:
		/** */
		void	start() noexcept;

	private:
		/** */
		void	startSample() noexcept;

		/** */
		void	disableAllInterrupts() noexcept;

		/** */
		void	processDatardy() noexcept;

		/** */
		bool	busy() noexcept;

	private:	// Oscl::Mt::Itc::Srv::Open::Req::Api
		/** */
		void    request(Oscl::Mt::Itc::Srv::Open::Req::Api::OpenReq& msg) noexcept;

	private: // Oscl::Event::Control::Req::Composer _sampleComposer
		/**	This request triggers a sample/start cycle.
		 */
		void	sampleRequest(Oscl::Event::Control::Req::Api::TriggerReq& msg) noexcept;

	};

}
}
}

#endif
