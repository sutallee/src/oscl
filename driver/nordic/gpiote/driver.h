/*
   Copyright (C) 2020 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_driver_nordic_gpiote_driverh_
#define _oscl_driver_nordic_gpiote_driverh_

#include "oscl/mt/itc/mbox/server.h"
#include "oscl/hw/nordic/gpiote/map.h"
#include "oscl/mt/itc/srv/openapi.h"
#include "oscl/mt/itc/srv/open.h"
#include "oscl/mt/itc/mbox/server.h"
#include "oscl/event/control/reqcomp.h"
#include "oscl/event/control/sync.h"

/** */
namespace Oscl {
/** */
namespace Nordic {
/** */
namespace GPIOTE {

/** */
class Channel {
	friend class Driver;
	public:
		/** */
		const Oscl::Nordic::GPIOTE::CONFIG::Reg	_config;

	public:
		/** */
		Channel(
			Oscl::Nordic::GPIOTE::CONFIG::Reg	config
			) noexcept;

	public:
		/** One-time start of this channel.
		 */
		virtual	void	start() noexcept=0;

		/** The event for this channel was fired.
		 */
		virtual	void	update() noexcept=0;

		/** Conditionally (re)enable the interrupt
			for this channel.
		 */
		virtual	void	enable() noexcept=0;
	};

/** */
class Driver :	
	public Oscl::Mt::Itc::Server,
	public Oscl::Mt::Itc::Srv::OpenSync,
	private Oscl::Mt::Itc::Srv::Open::Req::Api
	{
	private:
		/** */
		Oscl::Nordic::GPIOTE::Map&		_reg;

	private:
		/** */
		static constexpr unsigned		maxChannels = 8;

		/** */
		Channel*						_channels[maxChannels];

	public:
		/** */
		Driver(
			Oscl::Nordic::GPIOTE::Map&	reg,
			void						(osInitHook)(void*),
			void*						osInitHookArg
			) noexcept;

		/** */
		virtual ~Driver(){}

		/** This operation must only be invoked
			before the driver is opened.
		 */
		void	set(Channel* handler, unsigned channel) noexcept;

		/** */
		Oscl::Mt::Runnable&						getDriverRunnable() noexcept;

		/** RETURN: Reference to the SignalApi to
			be used to notify the driver when a
			transmit interrupt has happened.
		 */
		Oscl::Mt::Sema::SignalApi&				getSignalApi() noexcept;

	private: // Oscl::Mt::Itc::Server
		/** */
		void	mboxSignaled() noexcept;

	private:
		/** */
		void	start() noexcept;

	private:
		/** */
		void	enablePeripheral() noexcept;

		/** */
		void	startPeripheral() noexcept;

		/** */
		void	setupDMA() noexcept;

		/** */
		void	startSample() noexcept;

		/** */
		void	disableAllInterrupts() noexcept;

		/** */
		void	configureChannels() noexcept;

		/** */
		void	startChannels() noexcept;

		/** */
		void	startCalibration() noexcept;

		/** */
		void	startStop() noexcept;

		/** */
		void	processStarted() noexcept;

		/** */
		void	processCalibration() noexcept;

		/** */
		void	processEnd() noexcept;

		/** */
		void	processStopped() noexcept;

		/** */
		bool	busy() noexcept;

	private:	// Oscl::Mt::Itc::Srv::Open::Req::Api
		/** */
		void    request(Oscl::Mt::Itc::Srv::Open::Req::Api::OpenReq& msg) noexcept;
	};

}
}
}

#endif
