/*
   Copyright (C) 2020 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "oscl/error/info.h"
#include "oscl/hw/nordic/gpio/map.h"
#include "oscl/hw/nordic/nrf52840/map.h"
#include "part.h"

using namespace Oscl::Nordic::GPIOTE::Button;

//#define DEBUG_TRACE

Part::Part(
	Oscl::Mt::Itc::PostMsgApi&				papi,
	Oscl::Mt::Itc::Delay::Req::Api::SAP&	delaySAP,
	Oscl::Nordic::GPIOTE::CONFIG::Reg		config,
	uint8_t									channel
	) noexcept:
		Channel(
			config
			),
		_papi(papi),
		_delaySAP(delaySAP),
		_channel(channel),
		_debounceTimerRunning(false),
		_output(
			papi,
			Oscl::Button::Var::getRelease()
			)
		{

	unsigned
	portNum	= (config & Oscl::Nordic::GPIOTE::CONFIG::PORT::FieldMask);
	portNum	>>=  Oscl::Nordic::GPIOTE::CONFIG::PORT::Lsb;

	unsigned
	pin	= (config & Oscl::Nordic::GPIOTE::CONFIG::PSEL::FieldMask);
	pin	>>=  Oscl::Nordic::GPIOTE::CONFIG::PSEL::Lsb;

	using namespace Oscl::Nordic::GPIO;

	Oscl::Nordic::GPIO::Map&
	port	= (portNum == 0)?
				Nrf52840GPIOP0:
				Nrf52840GPIOP1
				;

	port.pin_cnf[pin]	=
			PIN_CNF::DIR::ValueMask_Input
		|	PIN_CNF::INPUT::ValueMask_Connect
		|	PIN_CNF::PULL::ValueMask_Pullup
		|	PIN_CNF::DRIVE::ValueMask_S0S1
		|	PIN_CNF::SENSE::ValueMask_Disabled
		;
	}

Oscl::Button::Observer::Req::Api::SAP&	Part::getButtonSAP() noexcept{
	return _output.getSAP();
	}

void	Part::timerExpired() noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif

	Nrf52840GPIOTE.intenset		=	Oscl::Nordic::GPIOTE::INTENSET::Set << _channel;
	}

void	Part::start() noexcept{
	// Enable the interrupt for this channel
	Nrf52840GPIOTE.events_in[_channel]	= Oscl::Nordic::GPIOTE::EVENTS_IN::IN::ValueMask_NotGenerated;
	Nrf52840GPIOTE.intenset		= 		Oscl::Nordic::GPIOTE::INTENSET::Set << _channel;
	}

void	Part::update() noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif

	Nrf52840GPIOTE.intenclr		= 		Oscl::Nordic::GPIOTE::INTENSET::Set << _channel;

	unsigned
	portNum	= (_config & Oscl::Nordic::GPIOTE::CONFIG::PORT::FieldMask);
	portNum	>>=  Oscl::Nordic::GPIOTE::CONFIG::PORT::Lsb;

	unsigned
	pin	= (_config & Oscl::Nordic::GPIOTE::CONFIG::PSEL::FieldMask);
	pin	>>=  Oscl::Nordic::GPIOTE::CONFIG::PSEL::Lsb;

	using namespace Oscl::Nordic::GPIO;

	Oscl::Nordic::GPIO::Map&
	port	= (portNum == 0)?
				Nrf52840GPIOP0:
				Nrf52840GPIOP1
				;

	unsigned
	up	= port.in;

	up	>>=	pin;
	up	&=	0x01;

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"\tButton: %s\n",
		(up)?"up":"down"
		);
	#endif

	if(up){
		_output.update(Oscl::Button::Var::getRelease());
		}
	else {
		_output.update(Oscl::Button::Var::getPress());
		}

	startTimer();
	}

void	Part::enable() noexcept{
	if(!_debounceTimerRunning){
		Nrf52840GPIOTE.intenset		= 		Oscl::Nordic::GPIOTE::INTENSET::Set << _channel;
		}
	}

void	Part::startTimer() noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif

	if(_debounceTimerRunning){
		// This should never happen
		#ifdef DEBUG_TRACE
		Oscl::Error::Info::log(
			"%s: _debounceTimerRunning == true\n",
			OSCL_PRETTY_FUNCTION
			);
		#endif
		return;
		}

	_debounceTimerRunning	= true;
	Oscl::Mt::Itc::Delay::Resp::Api::DelayResp&
	resp	= _delayMem.build(
				_delaySAP,
				*this,
				_papi,
				500	// delay in milliseconds
				);

	_delaySAP.post(resp.getSrvMsg());
	}

void	Part::response(Oscl::Mt::Itc::Delay::Resp::Api::DelayResp& msg) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif

	_debounceTimerRunning	= false;
	timerExpired();
	}

void	Part::response(Oscl::Mt::Itc::Delay::Resp::Api::CancelResp& msg) noexcept{
	// Nothing to do here since we never
	// cancel the timer.
	}

