/*
   Copyright (C) 2020 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_driver_nordic_gpiote_button_parth_
#define _oscl_driver_nordic_gpiote_button_parth_

#include "oscl/driver/nordic/gpiote/driver.h"
#include "oscl/mt/itc/delay/respmem.h"
#include "oscl/button/output/part.h"

/** */
namespace Oscl {
/** */
namespace Nordic {
/** */
namespace GPIOTE {
/** */
namespace Button {

/** */
class Part:
	public Oscl::Nordic::GPIOTE::Channel,
	private Oscl::Mt::Itc::Delay::Resp::Api
	{
	private:
		/** */
		Oscl::Mt::Itc::PostMsgApi&				_papi;

		/** */
		Oscl::Mt::Itc::Delay::Req::Api::SAP&	_delaySAP;

		/** */
		const uint8_t							_channel;

	private:
		/** */
		bool									_debounceTimerRunning;

		/** */
		Oscl::Mt::Itc::Delay::Resp::DelayMem	_delayMem;

	private:
		/** */
		Oscl::Button::Output::Part				_output;

	public:
		/** */
		Part(
			Oscl::Mt::Itc::PostMsgApi&				papi,
			Oscl::Mt::Itc::Delay::Req::Api::SAP&	delaySAP,
			Oscl::Nordic::GPIOTE::CONFIG::Reg		config,
			uint8_t									channel
			) noexcept;

		/** */
		Oscl::Button::Observer::Req::Api::SAP&	getButtonSAP() noexcept;

		/** */
		void	timerExpired() noexcept;

	private:
		/** */
		void	start() noexcept;

		/** */
		void	update() noexcept;

		/** */
		void	enable() noexcept;

		/** */
		void	startTimer() noexcept;

	private: // Oscl::Mt::Itc::Delay::Resp::Api
		/**	This message requests the host to attach a the specified
			symbiont.
		 */
		void	response(Oscl::Mt::Itc::Delay::Resp::Api::DelayResp& msg) noexcept;

		/**	This message requests the host to attach a the specified
			symbiont.
		 */
		void	response(Oscl::Mt::Itc::Delay::Resp::Api::CancelResp& msg) noexcept;
	};

}
}
}
}

#endif
