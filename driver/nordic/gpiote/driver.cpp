/*
   Copyright (C) 2020 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "oscl/hw/nordic/gpiote/reg.h"
#include "driver.h"
#include "oscl/error/fatal.h"
#include "oscl/error/info.h"
#include "oscl/cpu/sync.h"

using namespace Oscl::Nordic::GPIOTE;

//#define DEBUG_TRACE

Channel::Channel(
	Oscl::Nordic::GPIOTE::CONFIG::Reg	config
	) noexcept:
		_config(config)
		{
	}


Driver::Driver(
	Oscl::Nordic::GPIOTE::Map&	reg,
	void						(osInitHook)(void*),
	void*						osInitHookArg
	) noexcept:
		OpenSync(
			*this,
			*this
			),
		_reg(reg)
		{
	disableAllInterrupts();

	if(osInitHook){
		osInitHook(osInitHookArg);
		}

	for(unsigned i=0;i<maxChannels;++i){
		_channels[i]	= 0;
		}
	}

void	Driver::set(Channel* handler, unsigned channel) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif


	if(channel >= maxChannels){
		Oscl::ErrorFatal::logAndExit(
			"%s: invalid channel.\n",
			OSCL_PRETTY_FUNCTION
			);
		}

	if(_channels[channel]){
		Oscl::ErrorFatal::logAndExit(
			"%s: channel [%u] already in use.\n",
			OSCL_PRETTY_FUNCTION,
			channel
			);
		}

	_channels[channel]	= handler;
	}

void	Driver::disableAllInterrupts() noexcept {

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif

	_reg.intenclr	= ~0;

	OsclCpuInOrderMemoryAccessBarrier();
	}

Oscl::Mt::Runnable&	Driver::getDriverRunnable() noexcept{
	return *this;
	}

Oscl::Mt::Sema::SignalApi&	Driver::getSignalApi() noexcept{
	return *this;
	}

void    Driver::configureChannels() noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif

	for(unsigned i=0;i<maxChannels;++i){
		if(!_channels[i]){
			continue;
			}
		Channel&	channel	= *_channels[i];
		_reg.config[i]	= channel._config;

		#ifdef DEBUG_TRACE
		Oscl::Error::Info::log(
			"%s: channel [%u]\n"
			" config: reg: %p, 0x%8.8X,0x%8.8X\n"
			"",
			OSCL_PRETTY_FUNCTION,
			i,
			&_reg.config[i],
			_reg.config[i],
			channel._config
			);
		#endif
		}

	OsclCpuInOrderMemoryAccessBarrier();
	}

void    Driver::startChannels() noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif

	for(unsigned i=0;i<maxChannels;++i){
		if(!_channels[i]){
			continue;
			}
		_channels[i]->start();
		}
	}

void    Driver::start() noexcept{
	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif

	configureChannels();
	startChannels();
	}

void    Driver::request(OpenReq& msg) noexcept{
	start();
	msg.returnToSender();
	}

void	Driver::mboxSignaled() noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif

	using namespace Oscl::Nordic::GPIOTE;

	for(unsigned i=0;i<maxChannels;++i){
		if(_channels[i]){
			if(_reg.events_in[i] & (EVENTS_IN::IN::Value_Generated << EVENTS_IN::IN::Lsb)){
				_reg.events_in[i]	= (EVENTS_IN::IN::Value_NotGenerated << EVENTS_IN::IN::Lsb);
				_channels[i]->update();
				}
			else {
				_channels[i]->enable();
				}
			}
		}
	}

