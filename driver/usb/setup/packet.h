/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_usb_setup_packeth_
#define _oscl_drv_usb_setup_packeth_
#include "oscl/hw/usb/setupreg.h"
#include "oscl/bits/bitfield.h"
#include "oscl/endian/type.h"
#include <stdint.h>
#include "oscl/cache/line.h"

/** */
namespace Oscl {
/** */
namespace Usb {
/** */
namespace Setup {

using namespace Oscl::Usb::Hw::Setup;

/** This data type represents the payload
	of a setup packet.
 */
OsclCacheAlignMacroPre()
class Packet {
	private:
		/** */
		Oscl::BitField<bmRequestType::Reg>		_bmRequestType;
		/** */
		bRequest::Reg							_bRequest;
		/** */
		Oscl::Endian::Little::E<wValue::Reg>	_wValue;
		/** */
		Oscl::Endian::Little::E<wIndex::Reg>	_wIndex;
		/** */
		Oscl::Endian::Little::E<wLength::Reg>	_wLength;

	public:
		Packet(	bmRequestType::Reg	bmRequestType,
				bRequest::Reg		bRequest,
				wValue::Reg			wValue,
				wIndex::Reg			wIndex,
				wLength::Reg		wLength
				) noexcept;

		/** */
		unsigned	getDataLength() const noexcept;

	}
OsclCacheAlignMacroPost();

}
}
}

#endif
