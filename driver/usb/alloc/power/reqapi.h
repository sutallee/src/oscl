/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_usb_alloc_power_reqapih_
#define _oscl_drv_usb_alloc_power_reqapih_
#include "oscl/mt/itc/mbox/sap.h"
#include "oscl/mt/itc/mbox/srvreq.h"

/** */
namespace Oscl {
/** */
namespace Usb {
/** */
namespace Alloc {
/** */
namespace Power {
/** */
namespace Req {

/** */
class Api {
	public:
		/** */
		class ReservePayload {
			public:
				/** The amount of current needed by the client.
				 */
				const unsigned	_requiredCurrentInMilliamps;
				/** Set to true by the server if the specified
					amount of current is unavailable. Otherwise,
					the specified current is reserved and this
					field is set to false.
				 */
				bool		_failed;
			public:
				/** */
				ReservePayload(unsigned requiredCurrentInMilliamps) noexcept;
			};

		/** */
		typedef Oscl::Mt::Itc::SrvRequest<Api,ReservePayload>	ReserveReq;

		/** */
		class FreePayload {
			public:
				/** The amount of current being freed by the client.
					This should be the same amount previously reserved
					by the client.
				 */
				const unsigned	_freeCurrentInMilliamps;
			public:
				/** */
				FreePayload(unsigned freeCurrentInMilliamps) noexcept;
			};

		/** */
		typedef Oscl::Mt::Itc::SrvRequest<Api,FreePayload>	FreeReq;

		/** */
		class ChangePayload {
			public:
				/** When the amount of available current is greater
					than or equal to the amount specified by
					this member variable, this notification
					request is returned to the client.
				 */
				const unsigned	_requiredCurrentInMilliamps;
			public:
				/** */
				ChangePayload(unsigned requiredCurrentInMilliamps) noexcept;
			};

		/** */
		typedef Oscl::Mt::Itc::SrvRequest<Api,ChangePayload>	ChangeReq;

		/** */
		class CancelPayload {
			public:
				/** Reference to change notification request to
					be canceled.
				 */
				ChangeReq&		_reqToCancel;
			public:
				/** */
				CancelPayload(ChangeReq& reqToCancel) noexcept;
			};

		/** */
		typedef Oscl::Mt::Itc::SrvRequest<Api,CancelPayload>	CancelReq;

		/** */
		typedef Oscl::Mt::Itc::ConcreteSAP<Api>	ConcreteSAP;
		/** */
		typedef Oscl::Mt::Itc::SAP<Api>			SAP;

	public:
		/** Shut-up GCC. */
		virtual ~Api() {}
		/** */
		virtual void	request(ReserveReq& msg) noexcept=0;
		/** */
		virtual void	request(FreeReq& msg) noexcept=0;
		/** */
		virtual void	request(ChangeReq& msg) noexcept=0;
		/** */
		virtual void	request(CancelReq& msg) noexcept=0;

	};

}
}
}
}
}

#endif
