/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_usb_alloc_addr_respapih_
#define _oscl_drv_usb_alloc_addr_respapih_
#include "reqapi.h"
#include "oscl/mt/itc/mbox/clievt.h"
#include "oscl/mt/itc/mbox/clirsp.h"

/** */
namespace Oscl {
/** */
namespace Usb {
/** */
namespace Alloc {
/** */
namespace Addr {
/** */
namespace Resp {

/** */
class Api {
	public:	// Requests
		/** */
		typedef Oscl::Mt::Itc::
				CliEvent<	Req::Api,
							Api,
							Req::Api::LockAddressZeroPayload
							>	LockAddressZeroResp;
		/** */
		typedef Oscl::Mt::Itc::
				CliEvent<	Req::Api,
							Api,
							Req::Api::UnlockAddressZeroPayload
							>	UnlockAddressZeroResp;
		/** */
		typedef Oscl::Mt::Itc::
				CliResponse<	Req::Api,
								Api,
								Req::Api::CancelAddrZeroLockPayload
								>	CancelAddrZeroLockResp;
		/** */
		typedef Oscl::Mt::Itc::
				CliResponse<	Req::Api,
								Api,
								Req::Api::AllocateNewAddressPayload
								>	AllocateNewAddressResp;
		/** */
		typedef Oscl::Mt::Itc::
				CliResponse<	Req::Api,
								Api,
								Req::Api::FreeAddressPayload
								>	FreeAddressResp;
		/** */
		typedef Oscl::Mt::Itc::
				CliEvent<	Req::Api,
							Api,
							Req::Api::UsbAddrResChangePayload
							>	UsbAddrResChangeResp;
		/** */
		typedef Oscl::Mt::Itc::
				CliResponse<	Req::Api,
								Api,
								Req::Api::CancelUsbAddrResChangePayload
								>	CancelUsbAddrResChangeResp;
		/** */
		typedef union MsgPtr {
			/** */
			LockAddressZeroResp*	lockAddrZero;
			/** */
			UnlockAddressZeroResp*	unlockAddrZero;
			/** */
			CancelAddrZeroLockResp*	cancelLockAddrZero;
			/** */
			AllocateNewAddressResp*	allocUsbAddr;
			/** */
			FreeAddressResp*		freeUsbAddr;
			} MsgPtr;
	public:
		/** Shut-up GCC. */
		virtual ~Api() {}
		/** */
		virtual void	response(LockAddressZeroResp& msg) noexcept=0;
		/** */
		virtual void	response(UnlockAddressZeroResp& msg) noexcept=0;
		/** */
		virtual void	response(CancelAddrZeroLockResp& msg) noexcept=0;
		/** */
		virtual void	response(AllocateNewAddressResp& msg) noexcept=0;
		/** */
		virtual void	response(FreeAddressResp& msg) noexcept=0;
		/** */
		virtual void	response(UsbAddrResChangeResp& msg) noexcept=0;
		/** */
		virtual void	response(CancelUsbAddrResChangeResp& msg) noexcept=0;
	};

}
}
}
}
}

#endif
