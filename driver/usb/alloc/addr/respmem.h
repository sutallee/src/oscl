/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_usb_alloc_addr_respmemh_
#define _oscl_drv_usb_alloc_addr_respmemh_
#include "respapi.h"
#include "oscl/memory/block.h"

/** */
namespace Oscl {
/** */
namespace Usb {
/** */
namespace Alloc {
/** */
namespace Addr {
/** */
namespace Resp {

using namespace Oscl::Usb::Alloc::Addr;

/** */
typedef Oscl::Memory::AlignedBlock<	sizeof(	Resp::Api::LockAddressZeroResp)
									>	LockAddressZeroMem;

/** */
typedef Oscl::Memory::AlignedBlock<	sizeof(	Resp::Api::LockAddressZeroResp)
									>	UnlockAddressZeroMem;

/** */
typedef struct AllocateNewAddressMem {
	/** */
	Oscl::Memory::
	AlignedBlock<sizeof(Resp::Api::AllocateNewAddressResp)>		resp;
	/** */
	Oscl::Memory::
	AlignedBlock<sizeof(Req::Api::AllocateNewAddressPayload)>	payload;
	} AllocateNewAddressMem;

/** */
typedef struct FreeAddressMem {
	/** */
	Oscl::Memory::
	AlignedBlock<sizeof(Resp::Api::FreeAddressResp)>	resp;
	/** */
	Oscl::Memory::
	AlignedBlock<sizeof(Req::Api::FreeAddressPayload)>	payload;
	} FreeAddressMem;

/** */
typedef struct CancelAddrZeroLockMem {
	/** */
	Oscl::Memory::
	AlignedBlock<sizeof(Resp::Api::CancelAddrZeroLockResp)>	resp;
	/** */
	Oscl::Memory::
	AlignedBlock<sizeof(Req::Api::CancelAddrZeroLockPayload)>	payload;
	} CancelAddrZeroLockMem;

/** */
typedef Oscl::Memory::AlignedBlock<	sizeof(	Resp::Api::UsbAddrResChangeResp)
									>	UsbAddrResChangeMem;

/** */
typedef struct CancelUsbAddrResChangeMem {
	/** */
	Oscl::Memory::
	AlignedBlock<sizeof(Resp::Api::CancelUsbAddrResChangeResp)>	resp;
	/** */
	Oscl::Memory::
	AlignedBlock<sizeof(Req::Api::CancelUsbAddrResChangePayload)>	payload;
	} CancelUsbAddrResChangeMem;

/** */
typedef union Mem {
	/** */
	void*					__qitemlink;
	/** */
	LockAddressZeroMem		_lockAddressZero;
	/** */
	UnlockAddressZeroMem	_unlockAddressZero;
	/** */
	AllocateNewAddressMem	_allocNewAddress;
	/** */
	FreeAddressMem			_freeAddress;
	} Mem;

/** */
typedef union CancelMem {
	/** */
	void*						__qitemlink;
	/** */
	CancelAddrZeroLockMem		_cancelLockAddrZero;
	/** */
	CancelUsbAddrResChangeMem	_cancelResChange;
	} CancelMem;

}
}
}
}
}

#endif
