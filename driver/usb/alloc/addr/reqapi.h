/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_usb_alloc_addr_reqapih_
#define _oscl_drv_usb_alloc_addr_reqapih_
#include "oscl/mt/itc/mbox/srvevt.h"
#include "oscl/mt/itc/mbox/srvreq.h"
#include "oscl/mt/itc/mbox/sap.h"
#include "oscl/driver/usb/address.h"

/** */
namespace Oscl {
/** */
namespace Usb {
/** */
namespace Alloc {
/** */
namespace Addr {
/** */
namespace Req {

/** */
class Api {
	public:	// Payloads
		/** */
		class LockAddressZeroPayload {
			public:
				/** This attribute is set to true by the
					server if the lock request was canceled
					by the client before the lock was aquired.
				 */
				bool	_canceled;
			};
		/** */
		typedef Oscl::Mt::Itc::
				SrvEvent<	Api,
							LockAddressZeroPayload
							>	LockAddressZeroReq;
		/** */
		class UnlockAddressZeroPayload {};
		/** */
		typedef Oscl::Mt::Itc::
				SrvEvent<	Api,
							UnlockAddressZeroPayload
							>	UnlockAddressZeroReq;
		/** */
		class CancelAddrZeroLockPayload {
			public:
				/** */
				LockAddressZeroReq&	_toCancel;
			public:
				/** */
				CancelAddrZeroLockPayload(LockAddressZeroReq& toCancel) noexcept:
					_toCancel(toCancel){}
			};
		/** */
		typedef Oscl::Mt::Itc::
				SrvRequest<	Api,
							CancelAddrZeroLockPayload
							>	CancelAddrZeroLockReq;
		/** */
		class AllocateNewAddressPayload {
			private:
				/** */
				Oscl::Usb::Address	_address;
				/** */
				const bool			_lowSpeed;
				/** */
				bool				_result;
			public:
				/** */
				AllocateNewAddressPayload(bool lowSpeed) noexcept:
					_address(0),
					_lowSpeed(lowSpeed)
					{}
				/** */
				bool	lowSpeedDevice() const noexcept{
					return _lowSpeed;
					}
				/** This operation only invoked by the HCD server
					before the containing request is returned to
					the client. This is called to set the result
					when the address allocation operation was
					successful.
				 */
				void	setSuccess(Oscl::Usb::Address address) noexcept{
					_address	= address;
					_result		= true;
					}
				/** This operation only invoked by the HCD server
					before the containing request is returned to
					the client. This is called to set the result
					when the address allocation was not successful
					for what ever reason.
				 */
				void	setFailure() noexcept{
					_result	= false;
					}
				/** This operation only invoked by the client.
					When it returns true, the hub may use the
					getAddress() operation to retrieve the newly
					allocated USB address. The hub will then
					use this address in subsequent operations
					to configure the default control pipe. It
					is expected that the address will be freed
					if the port becomes disconnected at a later
					time. FIXME: by who?
				 */
				bool	succeeded() const noexcept{
					return _result;
					}
				/** This operation only invoked by the client
					when the request was successful as indicated
					by the succeeded() operation to retrieve
					the newly allocated USB address.
				 */
				Oscl::Usb::Address	getAddress() const noexcept{
					return _address;
					}
			};
		/** */
		typedef Oscl::Mt::Itc::
				SrvRequest<	Api,
							AllocateNewAddressPayload
							>	AllocateNewAddressReq;
		/** */
		class FreeAddressPayload {
			private:
				/** */
				Oscl::Usb::Address	_address;
			public:
				/** */
				FreeAddressPayload(Oscl::Usb::Address address) noexcept:
					_address(address)
					{}
				/** This operation only invoked by the server
					when the request is received, to obtain
					the address that is being freed.
				 */
				Oscl::Usb::Address	getAddress() const noexcept{
					return _address;
					}
			};
		/** */
		typedef Oscl::Mt::Itc::
				SrvRequest<	Api,
							FreeAddressPayload
							>	FreeAddressReq;
		/** */
		class UsbAddrResChangePayload {};
		/** */
		typedef Oscl::Mt::Itc::
				SrvEvent<	Api,
							UsbAddrResChangePayload
							>	UsbAddrResChangeReq;
		/** */
		class CancelUsbAddrResChangePayload {
			public:
				/** */
				UsbAddrResChangeReq&	_toCancel;
			public:
				/** */
				CancelUsbAddrResChangePayload(	UsbAddrResChangeReq&	toCancel
												) noexcept:_toCancel(toCancel){}
				
			};
		/** */
		typedef Oscl::Mt::Itc::
				SrvRequest<	Api,
							CancelUsbAddrResChangePayload
							>	CancelUsbAddrResChangeReq;
	public:	// SAPs
		/** */
		typedef Oscl::Mt::Itc::SAP<Api>			SAP;
		/** */
		typedef Oscl::Mt::Itc::ConcreteSAP<Api>	ConcreteSAP;

	public:
		/** Shut-up GCC. */
		virtual ~Api() {}
		/** */
		virtual void	request(LockAddressZeroReq& msg) noexcept=0;
		/** */
		virtual void	request(UnlockAddressZeroReq& msg) noexcept=0;
		/** */
		virtual void	request(CancelAddrZeroLockReq& msg) noexcept=0;
		/** */
		virtual void	request(AllocateNewAddressReq& msg) noexcept=0;
		/** */
		virtual void	request(FreeAddressReq& msg) noexcept=0;
		/** */
		virtual void	request(UsbAddrResChangeReq& msg) noexcept=0;
		/** */
		virtual void	request(CancelUsbAddrResChangeReq& msg) noexcept=0;
	};

}
}
}
}
}


#endif
