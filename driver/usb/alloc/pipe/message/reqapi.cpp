/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "reqapi.h"

using namespace Oscl::Usb::Alloc::Pipe::Message;

Req::Api::NewPayload::
NewPayload(	Oscl::Usb::Address		address,
			Oscl::Usb::EndpointID	endpoint,
			unsigned char			maxPacketSize,
			bool					isLowSpeedDevice
			) noexcept:
		_address(address),
		_endpoint(endpoint),
		_maxPacketSize(maxPacketSize),
		_lowSpeedDevice(isLowSpeedDevice),
		_newPipe(0)
		{
	}

Oscl::Usb::Pipe::Message::PipeApi*
Req::Api::NewPayload::getPipeApi() noexcept{
	return _newPipe;
	}

void	Req::Api::NewPayload::
setPipeApi(Oscl::Usb::Pipe::Message::PipeApi* newPipe) noexcept{
	_newPipe	= newPipe;
	}

Oscl::Usb::Address
Req::Api::NewPayload::getDeviceAddress() const noexcept{
	return _address;
	}

Oscl::Usb::EndpointID
Req::Api::NewPayload::getEndpointID() const noexcept{
	return _endpoint;
	}

unsigned char	Req::Api::NewPayload::getMaxPacketSize() const noexcept{
	return _maxPacketSize;
	}

bool			Req::Api::NewPayload::isLowSpeedDevice() const noexcept{
	return _lowSpeedDevice;
	}

Req::Api::FreePayload::
FreePayload(Oscl::Usb::Pipe::Message::PipeApi& freePipe) noexcept:
		_freePipe(freePipe)
		{
	}

Oscl::Usb::Pipe::Message::PipeApi&
Req::Api::FreePayload::getPipeApi() noexcept{
	return _freePipe;
	}
