/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_usb_alloc_pipe_msg_reqapih_
#define _oscl_drv_usb_alloc_pipe_msg_reqapih_
#include "oscl/mt/itc/mbox/srvreq.h"
#include "oscl/mt/itc/mbox/srvevt.h"
#include "oscl/driver/usb/address.h"
#include "oscl/driver/usb/endpoint.h"
#include "oscl/driver/usb/pipe/message/pipeapi.h"

/** */
namespace Oscl {
/** */
namespace Usb {
/** */
namespace Alloc {
/** */
namespace Pipe {
/** */
namespace Message {
/** */
namespace Req {

using namespace Oscl::Mt::Itc;

/** */
class Api {
	public:
		/** */
		class NewPayload {
			private:
				/** */
				const Oscl::Usb::Address			_address;
				/** */
				const Oscl::Usb::EndpointID			_endpoint;
				/** */
				unsigned char						_maxPacketSize;
				/** */
				bool								_lowSpeedDevice;
				/** */
				Oscl::Usb::Pipe::Message::PipeApi*	_newPipe;

			public:
				/** */
				NewPayload(	Oscl::Usb::Address		address,
							Oscl::Usb::EndpointID	endpoint,
							unsigned char			maxPacketSize,
							bool					isLowSpeedDevice
							) noexcept;

			public: // Client operations
				/** Returns null pointer if allocation fails. */
				Oscl::Usb::Pipe::Message::PipeApi*	getPipeApi() noexcept;

			public: // Server operations
				/** Called by server only. */
				void	setPipeApi(	Oscl::Usb::Pipe::Message::
									PipeApi*						newPipe
									) noexcept;

				/** */
				Oscl::Usb::Address	getDeviceAddress() const noexcept;

				/** */
				Oscl::Usb::EndpointID	getEndpointID() const noexcept;

				/** */
				unsigned char	getMaxPacketSize() const noexcept;

				/** */
				bool			isLowSpeedDevice() const noexcept;
			};

		/** */
		typedef SrvRequest<Api,NewPayload>	NewReq;

		/** */
		class FreePayload {
			private:
				/** */
				Oscl::Usb::Pipe::Message::PipeApi&	_freePipe;
			public:
				/** */
				FreePayload(	Oscl::Usb::Pipe::Message::
								PipeApi&						pipe
								) noexcept;

				/** */
				Oscl::Usb::Pipe::Message::PipeApi&	getPipeApi() noexcept;
			};
		/** */
		typedef SrvRequest<Api,FreePayload>	FreeReq;

		/** */
		class NotifyChangePayload {};
		/** Message is returned when HCD resources for a Message Pipe
			are available.
		 */
		typedef SrvEvent<	Api,
							NotifyChangePayload
							> NotifyChangeReq;
		/** */
		class CancelChangePayload {
			public:
				/** */
				NotifyChangeReq&	_toCancel;
			public:
				/** */
				CancelChangePayload(	NotifyChangeReq&	toCancel
										) noexcept:_toCancel(toCancel){}
			};
		/** */
		typedef SrvRequest<	Api,
							CancelChangePayload
							>	CancelChangeReq;

	public:
		/** */
		typedef Oscl::Mt::Itc::SAP<Api>			SAP;
		/** */
		typedef Oscl::Mt::Itc::ConcreteSAP<Api>	ConcreteSAP;

	public:
		/** Shut-up GCC. */
		virtual ~Api() {}
		/** */
		virtual void	request(NewReq& msg) noexcept=0;
		/** */
		virtual void	request(FreeReq& msg) noexcept=0;
		/** */
		virtual void	request(NotifyChangeReq& msg) noexcept=0;
		/** */
		virtual void	request(CancelChangeReq& msg) noexcept=0;
	};

}
}
}
}
}
}

#endif
