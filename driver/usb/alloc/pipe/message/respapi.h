/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_usb_alloc_pipe_msg_respapih_
#define _oscl_drv_usb_alloc_pipe_msg_respapih_
#include "oscl/mt/itc/mbox/clirsp.h"
#include "oscl/mt/itc/mbox/clievt.h"
#include "reqapi.h"

/** */
namespace Oscl {
/** */
namespace Usb {
/** */
namespace Alloc {
/** */
namespace Pipe {
/** */
namespace Message {
/** */
namespace Resp {

using namespace Oscl::Mt::Itc;

/** */
class Api {
	public:
		/** */
		typedef CliResponse<	Req::Api,
								Resp::Api,
								Req::Api::NewPayload
								>	NewResp;
		/** */
		typedef CliResponse<	Req::Api,
								Resp::Api,
								Req::Api::FreePayload
								>	FreeResp;
		/** */
		typedef CliEvent<		Req::Api,
								Resp::Api,
								Req::Api::NotifyChangePayload
								>	NotifyChangeResp;
		/** */
		typedef CliResponse<	Req::Api,
								Resp::Api,
								Req::Api::CancelChangePayload
								>	CancelChangeResp;

		/** */
		typedef union MsgPtr {
			NewResp*			msgPipeAlloc;
			FreeResp*			msgPipeFree;
			NotifyChangeResp*	msgPipeNotifyChange;
			CancelChangeResp*	msgPipeCancelChange;
			} MsgPtr;

	public:
		/** Shut-up GCC. */
		virtual ~Api() {}
		/** */
		virtual void	response(NewResp& msg) noexcept=0;
		/** */
		virtual void	response(FreeResp& msg) noexcept=0;
		/** */
		virtual void	response(NotifyChangeResp& msg) noexcept=0;
		/** */
		virtual void	response(CancelChangeResp& msg) noexcept=0;
	};

}
}
}
}
}
}

#endif
