/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_usb_alloc_pipe_bulk_out_respmemh_
#define _oscl_drv_usb_alloc_pipe_bulk_out_respmemh_
#include "respapi.h"
#include "oscl/memory/block.h"

/** */
namespace Oscl {
/** */
namespace Usb {
/** */
namespace Alloc {
/** */
namespace Pipe {
/** */
namespace Bulk {
/** */
namespace OUT {
/** */
namespace Resp {

using namespace Oscl::Usb::Alloc::Pipe::Bulk::OUT;

/** */
typedef struct NewMem {
	/** */
	Oscl::Memory::
	AlignedBlock<sizeof(Resp::Api::NewResp)>		resp;
	/** */
	Oscl::Memory::
	AlignedBlock<sizeof(Req::Api::NewPayload)>		payload;
	} NewMem;

/** */
typedef struct FreeMem {
	/** */
	Oscl::Memory::
	AlignedBlock<sizeof(Resp::Api::FreeResp)>		resp;
	/** */
	Oscl::Memory::
	AlignedBlock<sizeof(Req::Api::FreePayload)>		payload;
	} FreeMem;

typedef	Oscl::Memory::
		AlignedBlock<sizeof(Resp::Api::NotifyChangeResp)>	NotifyChangeMem;

typedef struct CancelChangeMem {
	/** */
	Oscl::Memory::
	AlignedBlock<sizeof(Resp::Api::CancelChangeResp)>		resp;
	/** */
	Oscl::Memory::
	AlignedBlock<sizeof(Req::Api::CancelChangePayload)>		payload;
	} CancelChangeMem;

/** */
typedef union Mem {
	/** */
	void*				__qitemlink;
	/** */
	NewMem				_newMem;
	/** */
	FreeMem				_freeMem;
	} Mem;

/** */
typedef union CancelMem {
	/** */
	void*				__qitemlink;
	/** */
	CancelChangeMem		_cancelChange;
	} CancelMem;

}
}
}
}
}
}
}

#endif
