/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_usb_alloc_bw_interrupt_reqapih_
#define _oscl_drv_usb_alloc_bw_interrupt_reqapih_
#include "bwrec.h"
#include "oscl/mt/itc/mbox/sap.h"
#include "oscl/mt/itc/mbox/srvreq.h"

/** */
namespace Oscl {
/** */
namespace Usb {
/** */
namespace Alloc {
/** */
namespace BW {
/** */
namespace Interrupt {
/** */
namespace Req {

/** */
class Api {
	public:
		/** */
		class ReservePayload {
			public:
				/** */
				const unsigned	_maxPacketSize;
				/** */
				const unsigned	_pollPeriodInMilliseconds;
				/** True if the bandwidth is for an IN endpoint
					and false if it is an OUT endpoint.
				 */
				const bool		_in;
				/** This pointer/identifier is returned/set by the
					server and has a non-zero value if the
					allocation succeeded or zero if the allocation
					failed.
				 */
				BwRec*			_bwRec;
				/** A valid value is returned by the allocator in
					this variable when the bandwidth allocation fails.
					The client may then use this value to register
					for a subsequent bandwidth free notification
					request. The allocation server increments its
					"free state" each time it reclaims bandwidth
					after a client issues a free request.
				 */
				unsigned long	_currentFreeState;
			public:
				/** */
				ReservePayload(	unsigned	maxPacketSize,
								unsigned	pollPeriodInMilliseconds,
								bool		in
								) noexcept;
			};

		/** */
		typedef Oscl::Mt::Itc::SrvRequest<Api,ReservePayload>	ReserveReq;

		/** */
		class FreePayload {
			public:
				/** Reference to bandwidth record obtained with
					a previous bandwidth Reserve request.
				 */
				BwRec&		_bwRecToFree;
				/** A valid value is returned by the allocator in
					this variable after the server sucessfully frees
					the bandwidth identified by the _bwRecToFree field.
					The client may then use this
					value to register for a subsequent bandwidth
					free notification request, which will be returned
					when the server's "free state" does not match
					the "free state" contained in the notification
					request. The allocation server increments its
					copy of the "free state" and returns pending
					notification request each time it reclaims
					bandwidth after any client issues a free request.
				 */
				unsigned long	_currentFreeState;
			public:
				/** */
				FreePayload(BwRec& bwRecToFree) noexcept;
			};

		/** */
		typedef Oscl::Mt::Itc::SrvRequest<Api,FreePayload>	FreeReq;

		/** */
		typedef Oscl::Mt::Itc::ConcreteSAP<Api>				ConcreteSAP;

		/** */
		typedef Oscl::Mt::Itc::SAP<Api>						SAP;

	public:
		/** Shut-up GCC. */
		virtual ~Api() {}
		/** */
		virtual void	request(ReserveReq& msg) noexcept=0;
		/** */
		virtual void	request(FreeReq& msg) noexcept=0;

	};

}
}
}
}
}
}

#endif
