/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_usb_alloc_bw_reqapih_
#define _oscl_drv_usb_alloc_bw_reqapih_
#include "oscl/mt/itc/mbox/sap.h"
#include "oscl/mt/itc/mbox/srvreq.h"

/** */
namespace Oscl {
/** */
namespace Usb {
/** */
namespace Alloc {
/** */
namespace BW {
/** */
namespace Req {

/** */
class Api {
	public:
		/** */
		class ChangePayload {
			public:
				/** When the "free state" of the server does NOT
					match this value, the server copies its
					"free state" value to this member and returns
					this notification request to its sender.
					The allocation server increments its
					copy of the "free state" and returns pending
					notification request each time it reclaims
					bandwidth after any client issues a request
					that frees reserved bandwidth.
				 */
				unsigned long	_currentFreeState;
			public:
				/** */
				ChangePayload(unsigned long	currentFreeState) noexcept;
			};

		/** */
		typedef Oscl::Mt::Itc::SrvRequest<Api,ChangePayload>	ChangeReq;

		/** */
		class CancelPayload {
			public:
				/** Reference to change notification request to
					be canceled.
				 */
				ChangeReq&		_reqToCancel;
			public:
				/** */
				CancelPayload(ChangeReq& reqToCancel) noexcept;
			};

		/** */
		typedef Oscl::Mt::Itc::SrvRequest<Api,CancelPayload>	CancelReq;

		/** */
		typedef Oscl::Mt::Itc::ConcreteSAP<Api>					ConcreteSAP;

		/** */
		typedef Oscl::Mt::Itc::SAP<Api>							SAP;

	public:
		/** Shut-up GCC. */
		virtual ~Api() {}
		/** */
		virtual void	request(ChangeReq& msg) noexcept=0;
		/** */
		virtual void	request(CancelReq& msg) noexcept=0;

	};

}
}
}
}
}

#endif
