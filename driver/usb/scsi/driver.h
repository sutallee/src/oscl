/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_usb_hid_pl2303_driverh_
#define _oscl_drv_usb_hid_pl2303_driverh_
#include "oscl/memory/block.h"
#include "oscl/queue/queue.h"
#include "oscl/mt/itc/srv/close.h"
#include "oscl/driver/usb/pipe/message/pipeapi.h"
#include "oscl/driver/usb/pipe/bulk/in/respmem.h"
#include "oscl/driver/usb/pipe/bulk/out/respmem.h"
#include "oscl/driver/usb/pipe/message/respmem.h"
#include "oscl/stream/input/itc/reqapi.h"
#include "oscl/stream/output/itc/reqapi.h"
#include "oscl/driver/usb/setup/mem.h"
#include "oscl/boolstate/dist/dist.h"

/** */
namespace Oscl {
/** */
namespace Usb {
/** */
namespace SCSI {
/** */
namespace Host {

/** */
class Driver :	public Oscl::Mt::Itc::Srv::CloseSync,
				private Oscl::Mt::Itc::Srv::Close::Req::Api,
				private Oscl::Usb::Pipe::Bulk::OUT::Resp::Api,
				private Oscl::Stream::Input::Req::Api,
				private Oscl::Stream::Output::Req::Api,
				private Oscl::SCSI::Req::Api
				{
	public:
		/** */
		struct SetupMem {
			/** */
			Oscl::Usb::Setup::Mem	controlSetup;
			};
		/** */
		struct ControlPacketMem {
			Oscl::Memory::AlignedBlock<16>	controlBytes;
			};
		/** */
		union ContextPacketMem {
			ControlPacketMem	control;
			};
		/** */
		struct ChangePacketMem {
			Oscl::Memory::AlignedBlock<32>	change;
			};
		/** */
		enum{maxBulkRxPacketBytes=2*0x40};
		/** */
		struct BulkRxPacketMem {
			Oscl::Memory::AlignedBlock<maxBulkRxPacketBytes>	mem;
			};
		/** */
		enum{maxBulkTxPacketBytes=1024};
		/** */
		union BulkTxPacketMem {
			void*		__qitemlink;
			Oscl::Memory::AlignedBlock<maxBulkTxPacketBytes>	mem;
			};
		/** */
		enum{nRxBuffers=2};
		/** */
		struct PacketMem {
			/** */
			ControlPacketMem	control;
			/** */
			ChangePacketMem		change;
			/** */
			BulkRxPacketMem			rx[nRxBuffers];
			/** */
			BulkTxPacketMem			tx;
			/** */
			ContextPacketMem	context;
			};
		/** */
		union CancelMem {
			/** */
			Oscl::Usb::Pipe::Bulk::
			IN::Resp::CancelMem					bulkInPipe;
			Oscl::Usb::Pipe::Bulk::
			OUT::Resp::CancelMem				bulkOutPipe;
			Oscl::Usb::Pipe::Message::
			Resp::CancelMem						messagePipe;
			};
		/** */
		class RxTrans :	public Oscl::QueueItem,
						private Oscl::Usb::Pipe::Bulk::IN::Resp::Api,
						public Oscl::Buffer::Base
						{
			private:
				/** */
				Driver&						_context;
				/** */
				Oscl::Usb::Pipe::
				Bulk::IN::Resp::ReadMem		_respMem;
				/** */
				Oscl::Usb::Pipe::Bulk::
				IN::Req::Api::SAP&			_bulkInPipe;
				/** */
				Oscl::Mt::Itc::PostMsgApi&	_myPapi;
				/** */
				Oscl::Mt::Itc::SrvMsg*		_req;
				/** */
				Oscl::Usb::Pipe::Bulk::
				IN::Resp::CancelMem*		_cancelMem;
				/** */
				bool						_closing;

			public:
				void*						_packetMem;
				/** */
				const unsigned				_maxPacketSize;
			public:
				/** */
				RxTrans(	Driver&						context,
							Oscl::Usb::Pipe::Bulk::
							IN::Req::Api::SAP&			bulkInPipe,
							Oscl::Mt::Itc::PostMsgApi&	myPapi,
							void*						packetMem,
							unsigned					maxPacketSize
							) noexcept;

				/** */
				void	start() noexcept;
				/** */
				void	cancel(	Oscl::Usb::Pipe::Bulk::
								IN::Resp::CancelMem&		mem
								) noexcept;

			private:	// Oscl::Usb::Pipe::Bulk::IN::Resp::Api
				/** */
				void	response(	Oscl::Usb::Pipe::Bulk::
									IN::Resp::Api::ReadResp&		msg
									) noexcept;
				/** */
				void	response(	Oscl::Usb::Pipe::Bulk::
									IN::Resp::Api::CancelResp&		msg
									) noexcept;
			public: // Oscl::Buffer::Base
				/** */
				unsigned	bufferSize() const noexcept;
				/** */
				const void*	getBuffer() const noexcept;

			private: // Oscl::Buffer::Base
				void*		buffer() noexcept;
			};

		/** */
		friend class RxTrans;
		/** */
		union RxTransMem{
			/** */
			void*	__qitemlink;
			/** */
			Oscl::Memory::AlignedBlock< sizeof(RxTrans) >	trans;
			};

		/** */
		struct RxBuffer {
			void*	__qitemlink;
			};

	private:
		/** */
		Oscl::Usb::Pipe::
		Bulk::IN::Resp::ReadMem				_bulkInRespMem;
		/** */
		enum{nRxTrans=2};
		/** */
		RxTransMem							_rxTransMem[nRxTrans];
		/** */
		Oscl::Queue<RxBuffer>				_freeRxBuffers;
		/** */
		Oscl::Queue<RxTransMem>				_freeRxTransMem;
		/** List of all outstanding RxTrans */
		Oscl::Queue<RxTrans>				_pendingRxTrans;
		/** List of all RxTrans which still un-consumed buffer content */
		Oscl::Queue<RxTrans>				_finishedRxTrans;
		/** */
		Oscl::Queue<	Oscl::Usb::Pipe::
						Bulk::OUT::Resp::
						Api::WriteResp
						>					_pendingWriteReqs;
		/** */
		Oscl::Queue<	Oscl::Stream::Input::
						Req::Api::ReadReq
						>					_pendingStreamInReqs;
		/** */
		unsigned							_outstandingRxTransactions;
		/** */
		Oscl::Usb::Pipe::
		Bulk::OUT::Resp::WriteMem			_bulkOutRespMem;
		/** */
		CancelMem							_cancelMem;
		/** */
		Oscl::Usb::Pipe::
		Message::PipeApi&					_controlPipe;
		/** */
		Oscl::Usb::Pipe::Bulk::
		IN::Req::Api::SAP&					_bulkInPipe;
		/** */
		Oscl::Usb::Pipe::Bulk::
		OUT::Req::Api::SAP&					_bulkOutPipe;
		/** */
		SetupMem&							_setupMem;
		/** */
		PacketMem&							_packetMem;
		/** */
		Oscl::Mt::Itc::PostMsgApi&			_myPapi;
		/** */
//		Oscl::SCSI::Host::Sync				_scsiHostSync;
		/** */
		unsigned char						_maxBulkInPacketSize;
		/** */
		unsigned char						_maxBulkOutPacketSize;
		/** */
		Oscl::Mt::Itc::Srv::Open::
		Req::Api::OpenReq*					_openReq;
		/** */
		Oscl::Mt::Itc::Srv::Close::
		Req::Api::CloseReq*					_closeReq;
		/** */
		bool								_open;
		/** */
		bool								_closing;
		/** */
		uint32_t							_baudRate;
		/** */
		StopBits							_nStopBits;
		/** */
		Parity								_parity;
		/** */
		BitsPerChar							_bitsPerChar;
		/** */
		bool								_assertRTS;
		/** */
		bool								_assertDTR;
		/** */
		bool								_enableHardwareFlowControl;
		/** */
		struct Transmitter {
			Transmitter():
				currentReq(0),
				currentOffset(0),
				currentRemaining(0),
				currentLength(0),
				busy(false)
				{};
			/** */
			Oscl::Queue<	Oscl::Stream::
							Output::Req::Api::
							WriteReq
							>					pendingReq;
			/** */
			Oscl::Stream::Output::
			Req::Api::WriteReq*					currentReq;
			/** */
			Oscl::Usb::Pipe::Bulk::
			OUT::Resp::Api::WriteResp*			pendingBulkWrite;
			/** */
			unsigned							currentOffset;
			/** */
			unsigned							currentRemaining;
			/** */
			unsigned							currentLength;
			/** */
			bool								busy;
			} _write;

	public:
		/** */
		Driver(	Oscl::Usb::Pipe::
				Message::PipeApi&				controlPipe,
				Oscl::Usb::Pipe::Bulk::
				IN::Req::Api::SAP&				bulkInPipe,
				Oscl::Usb::Pipe::Bulk::
				OUT::Req::Api::SAP&				bulkOutPipe,
				Oscl::Mt::Itc::PostMsgApi&		myPapi,
				SetupMem&						setupMem,
				PacketMem&						packetMem,
				unsigned char					maxBulkInPacketSize,
				unsigned char					maxBulkOutPacketSize
				) noexcept;

		/** */
		Oscl::SCSI::Host::Api&	getScsiHostApi() noexcept;

	private:
		/** */
		void	cancelPendingBulkReads() noexcept;

		/** */
		void	cancelPendingBulkWrites() noexcept;

		/** */
		void	txCurrentBulk() noexcept;

		/** */
		void	txNextBulk() noexcept;

		/** */
		void	sendAllRxTrans() noexcept;

		/** */
		void	done(RxTrans& trans) noexcept;

		/** */
		void	done(	Oscl::Usb::Pipe::Bulk::
						IN::Resp::CancelMem&		mem
						) noexcept;

		/** */
		void	failed(RxTrans& trans) noexcept;

		/** */
		void	free(RxTrans& trans) noexcept;

		/** */
		RxTrans&	build(	RxTransMem&		mem,
							void*			packetMem
							) noexcept;

		/** */
		void		restart(RxTrans& trans) noexcept;

		/** This operation copies data from pending
			bulk receive transactions to pending stream read
			requests.
		 */
		void	processRxStream() noexcept;

	private:	// Oscl::Mt::Itc::Srv::Close::Req::Api
		/** */
		void	request(	Oscl::Mt::Itc::Srv::
							Open::Req::Api::OpenReq&	msg
							) noexcept;
		/** */
		void	request(	Oscl::Mt::Itc::Srv::
							Close::Req::Api::CloseReq&	msg
							) noexcept;

	private:	// Oscl::Stream::Input::Req::Api
		/** */
		void	request(	Oscl::Stream::Input::
							Req::Api::ReadReq&		msg
							) noexcept;
		/** */
		void	request(	Oscl::Stream::Input::
							Req::Api::CancelReq&	msg
							) noexcept;

	private:	// Oscl::Stream::Output::Req::Api
		/** */
		void	request(	Oscl::Stream::Output::
							Req::Api::WriteReq&		msg
							) noexcept;
		/** */
		void	request(	Oscl::Stream::Output::
							Req::Api::FlushReq&		msg
							) noexcept;

	private:	// Oscl::UART::Req::Api
		/** */
		void	request(Oscl::UART::Req::Api::SetBaudRateReq& msg) noexcept;
		/** */
		void	request(Oscl::UART::Req::Api::NoParityReq& msg) noexcept;
		/** */
		void	request(Oscl::UART::Req::Api::EvenParityReq& msg) noexcept;
		/** */
		void	request(Oscl::UART::Req::Api::OddParityReq& msg) noexcept;
		/** */
		void	request(Oscl::UART::Req::Api::MarkParityReq& msg) noexcept;
		/** */
		void	request(Oscl::UART::Req::Api::SpaceParityReq& msg) noexcept;
		/** */
		void	request(Oscl::UART::Req::Api::SetStartBitsReq& msg) noexcept;
		/** */
		void	request(Oscl::UART::Req::Api::SetStopBitsReq& msg) noexcept;
		/** */
		void	request(Oscl::UART::Req::Api::RtsCtsFlowControlReq& msg) noexcept;
		/** */
		void	request(Oscl::UART::Req::Api::ManualFlowControlReq& msg) noexcept;
		/** */
		void	request(Oscl::UART::Req::Api::DisableFlowControlReq& msg) noexcept;
		/** */
		void	request(Oscl::UART::Req::Api::AssertRtsReq& msg) noexcept;
		/** */
		void	request(Oscl::UART::Req::Api::NegateRtsReq& msg) noexcept;
		/** */
		void	request(Oscl::UART::Req::Api::AssertCtsReq& msg) noexcept;
		/** */
		void	request(Oscl::UART::Req::Api::NegateCtsReq& msg) noexcept;
		/** */
		void	request(Oscl::UART::Req::Api::DoThisReq& msg) noexcept;

	private:	// Oscl::UART::DTE::Req::Api
		/** */
		void	request(Oscl::UART::DTE::Req::Api::AssertDtrReq& msg) noexcept;
		/** */
		void	request(Oscl::UART::DTE::Req::Api::NegateDtrReq& msg) noexcept;

	private:	// Oscl::Usb::Pipe::Bulk::OUT::Resp::Api
		/** */
		void	response(	Oscl::Usb::Pipe::Bulk::
							OUT::Resp::Api::WriteResp&		msg
							) noexcept;
		/** */
		void	response(	Oscl::Usb::Pipe::Bulk::
							OUT::Resp::Api::CancelResp&		msg
							) noexcept;
	};

}
}
}
}


#endif
