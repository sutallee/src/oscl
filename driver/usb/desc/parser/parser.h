/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_usb_desc_parser_parserh_
#define _oscl_drv_usb_desc_parser_parserh_
#include "oscl/driver/usb/desc/config.h"
#include "oscl/driver/usb/desc/interface.h"
#include "oscl/driver/usb/desc/endpoint.h"

/** */
namespace Oscl {
/** */
namespace Usb {
/** */
namespace Desc {
/** */
namespace Parser {

/** */
class VisitorApi {
	public:
		/** Make GCC happy */
		virtual ~VisitorApi() {}
		/** */
		virtual void	visit(const Oscl::Usb::Desc::Config& desc) noexcept=0;
		/** */
		virtual void	visit(const Oscl::Usb::Desc::Interface& desc) noexcept=0;
		/** */
		virtual void	visit(const Oscl::Usb::Desc::Endpoint& desc) noexcept=0;
		/** */
		virtual void	visit(const void* unknown,unsigned length) noexcept=0;
	};

/** */
class Part {
	private:
		/** */
		const Oscl::Usb::Desc::Config&		_configDesc;
		/** */
		const unsigned						_descBufferSize;
		/** */
		const Oscl::Usb::Desc::Descriptor*	_nextDesc;
		/** */
	public:
		/** The "descBufferSize" is the number of bytes in the
		 * 	buffer that begins with "configDesc" and is followed
		 * 	by the remaining interface and endpoint descriptors.
		 * 	This is the number of bytes actually read from the USB device.
		 */
		Part(	const Oscl::Usb::Desc::Config&	configDesc,
				unsigned						descBufferSize
				) noexcept;
		/** */
		void	first() noexcept;
		/** */
		bool	more() const noexcept;
		/** */
		void	next() noexcept;
		/** */
		void	current(VisitorApi& visitor) const noexcept;
	};

}
}
}
}

#endif
