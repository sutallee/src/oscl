/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "oscl/hw/usb/desc/descreg.h"
#include "parser.h"

using namespace Oscl::Usb::Desc::Parser;

Part::Part(	const Oscl::Usb::Desc::Config&	configDesc,
			unsigned						descBufferSize
			) noexcept:
		_configDesc(configDesc),
		_descBufferSize(descBufferSize),
		_nextDesc((const Oscl::Usb::Desc::Descriptor*)&_configDesc)
		{
	}

void	Part::first() noexcept{
	_nextDesc	= (const Oscl::Usb::Desc::Descriptor*)&_configDesc;
	}

bool	Part::more() const noexcept{
	unsigned long	start	= (unsigned long)&_configDesc;
	unsigned long	current	= (unsigned long)_nextDesc;
	unsigned long	offset	= current-start;
	return	(		(offset < _descBufferSize)
				&&	(offset < (unsigned long)_configDesc._wTotalLength)
				&&	(offset+_nextDesc->_bLength <= (unsigned long)_configDesc._wTotalLength)
				);
	}

void	Part::next() noexcept{
	unsigned long	addr	= (unsigned long)_nextDesc;
	addr	+= _nextDesc->_bLength;
	_nextDesc	= (const Oscl::Usb::Desc::Descriptor*)addr;
	}

void	Part::current(VisitorApi& visitor) const noexcept{
	using namespace Oscl::Usb::Hw::Desc;
	switch(_nextDesc->_bDescriptorType){
		case bDescriptorType::CONFIGURATION:
			visitor.visit(*(const Oscl::Usb::Desc::Config*)_nextDesc);
			break;
		case bDescriptorType::INTERFACE:
			visitor.visit(*(const Oscl::Usb::Desc::Interface*)_nextDesc);
			break;
		case bDescriptorType::ENDPOINT:
			visitor.visit(*(const Oscl::Usb::Desc::Endpoint*)_nextDesc);
			break;
		case bDescriptorType::DEVICE:
		case bDescriptorType::STRING:
		default:
			visitor.visit(&_nextDesc,_nextDesc->_bLength);
			break;
		}
	}

