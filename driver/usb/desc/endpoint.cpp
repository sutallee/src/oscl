/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "endpoint.h"

//using namespace Oscl::Usb::Desc;

Oscl::Usb::Desc::
Endpoint::Endpoint(	uint8_t		bEndpointAddress,
					uint8_t		bmAttributes,
					uint16_t		wMaxPacketSize,
					uint8_t		bInterval
					) noexcept:
		Descriptor(sizeof(Endpoint),bDescriptorType::ENDPOINT)
		{
	_bEndpointAddress	= bEndpointAddress;
	_bmAttributes		= bmAttributes;
	_wMaxPacketSize		= wMaxPacketSize;
	_bInterval			= bInterval;
	}

unsigned char
Oscl::Usb::Desc::Endpoint::getEndpointID() const noexcept{
	using namespace Oscl::Usb::Hw::Desc::Endpoint::bEndpointAddress;
	return _bEndpointAddress.extract(	EndpointNumber::FieldMask,
										EndpointNumber::Lsb
										);
	}

bool
Oscl::Usb::Desc::Endpoint::directionIsIN() const noexcept{
	using namespace Oscl::Usb::Hw::Desc::Endpoint::bEndpointAddress;
	return _bEndpointAddress.equal(	Direction::FieldMask,
									Direction::ValueMask_IN
									);
	}

