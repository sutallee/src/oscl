/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_usb_desc_hubh_
#define _oscl_drv_usb_desc_hubh_
#include "oscl/endian/type.h"
#include "oscl/hw/usb/desc/devicereg.h"
#include "oscl/hw/usb/desc/hubreg.h"
#include "desc.h"

/** */
namespace Oscl {
/** */
namespace Usb {
/** */
namespace Desc {
/** */
namespace Hub {

// This using statement does not extend
// beyond the scope of the enclosing namespace.
// I verified this experimentally.
using namespace Oscl::Usb::Hw::Desc::Hub;

/** This structure describes the USB Hub Class
	Descriptor format.
 */
struct Header : public Descriptor {
	public:
		/** This constructor allows the descriptor to be
			type cast onto an existing buffer without initializing
			the members.
		 */
		inline Header() noexcept:Descriptor(){};
		inline Header(	uint8_t	bNbrPorts,
						uint16_t	wHubCharacteristics,
						uint8_t	bPwrOn2PwrGood,
						uint8_t	bHubContrCurrent,
						unsigned	descLength
						) noexcept:
			Descriptor(descLength,Oscl::Usb::Hw::Desc::bDescriptorType::HUB)
			{
			_bNbrPorts				= bNbrPorts;
			_wHubCharacteristics	= wHubCharacteristics;
			_bPwrOn2PwrGood			= bPwrOn2PwrGood;
			_bHubContrCurrent		= bHubContrCurrent;
			};
		/** Number of ports supported by this hub */
		bNbrPorts::Reg							_bNbrPorts;
		/** */
		wHubCharacteristics::Reg				_wHubCharacteristics;
		/** Units of 2ms */
		bPwrOn2PwrGood::Reg						_bPwrOn2PwrGood;
		/** Units of mA */
		bHubContrCurrent::Reg					_bHubContrCurrent;
		// Variable length fields follow.
		//		DeviceRemovable		bDeviceRemovable[_bNbrPorts];
		//		PortPwrCtrlMask		bPortPwrCtrlMask[_bNbrPorts];
	};

/** This structure describes the USB Hub Class
	Descriptor format.
 */
struct DeviceRemovable {
	public:
		/** This constructor allows the descriptor to be
			type cast onto an existing buffer without initializing
			the members.
		 */
		inline DeviceRemovable() noexcept{}
		inline DeviceRemovable(uint8_t bDeviceRemovable) noexcept
			{
			_bDeviceRemovable	= bDeviceRemovable;
			};
		/** */
		bDeviceRemovable::Reg		_bDeviceRemovable;
	};

/** */
struct PortPwrCtrlMask {
	public:
		/** This constructor allows the descriptor to be
			type cast onto an existing buffer without initializing
			the members.
		 */
		inline PortPwrCtrlMask() noexcept{}
		/** */
		inline PortPwrCtrlMask(uint8_t bPortPwrCtrlMask) noexcept
			{
			_bPortPwrCtrlMask	= bPortPwrCtrlMask;
			};
		/** */
		bPortPwrCtrlMask::Reg		_bPortPwrCtrlMask;
	};

}
}
}
}

#endif
