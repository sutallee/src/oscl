/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_usb_desc_interfaceh_
#define _oscl_drv_usb_desc_interfaceh_
#include "oscl/hw/usb/desc/ifreg.h"
#include "desc.h"

/** */
namespace Oscl {
/** */
namespace Usb {
/** */
namespace Desc {

// This using statement does not extend
// beyond the scope of the enclosing namespace.
// I verified this experimentally.
using namespace Oscl::Usb::Hw::Desc::Interface;

/** This structure describes the USB Interface
	Descriptor format.
 */
struct Interface : public Descriptor {
	public:
		/** This constructor allows the descriptor to be
			type cast onto an existing buffer without initializing
			the members.
		 */
		inline Interface() noexcept:Descriptor(){}
		/** */
		inline Interface(	uint8_t		bInterfaceNumber,
							uint8_t		bAlternateSetting,
							uint8_t		bNumEndpoints,
							uint8_t		bInterfaceClass,
							uint8_t		bInterfaceSubClass,
							uint8_t		bInterfaceProtocol,
							uint8_t		iInterface
							) noexcept:
				Descriptor(sizeof(Interface),bDescriptorType::INTERFACE),
				_bInterfaceNumber(bInterfaceNumber),
				_bAlternateSetting(bAlternateSetting),
				_bNumEndpoints(bNumEndpoints),
				_bInterfaceClass(bInterfaceClass),
				_bInterfaceSubClass(bInterfaceSubClass),
				_bInterfaceProtocol(bInterfaceProtocol),
				_iInterface(iInterface)
				{};
		/** 
		 */
		bInterfaceNumber::Reg	_bInterfaceNumber;
		/** 
		 */
		bAlternateSetting::Reg	_bAlternateSetting;
		/** 
		 */
		bNumEndpoints::Reg		_bNumEndpoints;
		/** 
		 */
		bInterfaceClass::Reg	_bInterfaceClass;
		/** 
		 */
		bInterfaceSubClass::Reg	_bInterfaceSubClass;
		/** 
		 */
		bInterfaceProtocol::Reg	_bInterfaceProtocol;
		/** 
		 */
		iInterface::Reg			_iInterface;
	};


}
}
}

#endif
