/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "type.h"
#include "parser.h"

namespace Oscl {
namespace Usb {
namespace Desc {
namespace Type {

class Device : public Api {
	public:
		void	query(VisitorApi& visitor) noexcept{
			visitor.device();
			}
	};

class Configuration : public Api {
	public:
		void	query(VisitorApi& visitor) noexcept{
			visitor.configuration();
			}
	};

class Interface : public Api {
	public:
		void	query(VisitorApi& visitor) noexcept{
			visitor.interface();
			}
	};

class Endpoint : public Api {
	public:
		void	query(VisitorApi& visitor) noexcept{
			visitor.endpoint();
			}
	};

class String : public Api {
	public:
		void	query(VisitorApi& visitor) noexcept{
			visitor.string();
			}
	};

class Unknown : public Api {
	public:
		void	query(VisitorApi& visitor) noexcept{
			visitor.unknown();
			}
	};

static const Device			device;
static const Configuration	configuration;
static const Interface		interface;
static const Endpoint		endpoint;
static const String			string;
static const Unknown		unknown;

}
}
}
}

using namespace Oscl::Usb::Desc::Type;
using namespace Oscl;

const Api&	parse(uint8_t type) noexcept{
	using namespace Oscl::Usb::Hw::Desc;
	switch(type){
		case bDescriptorType::DEVICE:
			return device;
		case bDescriptorType::CONFIGURATION:
			return configuration;
		case bDescriptorType::STRING:
			return string;
		case bDescriptorType::INTERFACE:
			return interface;
		case bDescriptorType::ENDPOINT:
			return endpoint;
		default:
			return unknown;
		}
	}

