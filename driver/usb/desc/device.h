/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_usb_desc_deviceh_
#define _oscl_drv_usb_desc_deviceh_
#include "oscl/endian/type.h"
#include "oscl/hw/usb/desc/devicereg.h"
#include "desc.h"

/** */
namespace Oscl {
/** */
namespace Usb {
/** */
namespace Desc {

// This using statement does not extend
// beyond the scope of the enclosing namespace.
// I verified this experimentally.
using namespace Oscl::Usb::Hw::Desc::Device;

/** This structure describes the USB Device
	Descriptor format.
 */
struct Device : public Descriptor {
	public:
		/** This constructor allows the descriptor to be
			type cast onto an existing buffer without initializing
			the members.
		 */
		inline Device() noexcept:Descriptor(){}

		/** */
		inline Device(	uint16_t		bcdUSB,
						uint8_t		bDeviceClass,
						uint8_t		bDeviceSubClass,
						uint8_t		bDeviceProtocol,
						uint8_t		bMaxPacketSize,
						uint16_t		idVendor,
						uint16_t		idProduct,
						uint16_t		bcdDevice,
						uint8_t		iManufacturer,
						uint8_t		iProduct,
						uint8_t		iSerialNumber,
						uint8_t		bNumConfigurations
						) noexcept:
				Descriptor(sizeof(Device),bDescriptorType::DEVICE)
				{
			_bcdUSB	= bcdUSB;
			_bDeviceClass	= bDeviceClass;
			_bDeviceSubClass	= bDeviceSubClass;
			_bDeviceProtocol	= bDeviceProtocol;
			_bMaxPacketSize	= bMaxPacketSize;
			_idVendor	= idVendor;
			_idProduct	= idProduct;
			_bcdDevice	= bcdDevice;
			_iManufacturer	= iManufacturer;
			_iProduct	= iProduct;
			_iSerialNumber	= iSerialNumber;
			_bNumConfigurations	= bNumConfigurations;
			};
		/** 
		 */
		Oscl::Endian::Little::E<bcdUSB::Reg>	_bcdUSB;
		/** 
		 */
		bDeviceClass::Reg						_bDeviceClass;
		/** 
		 */
		bDeviceSubClass::Reg					_bDeviceSubClass;
		/** 
		 */
		bDeviceProtocol::Reg					_bDeviceProtocol;
		/** 
		 */
		bMaxPacketSize::Reg						_bMaxPacketSize;
		/** 
		 */
		Oscl::Endian::Little::E<idVendor::Reg>	_idVendor;
		/** 
		 */
		Oscl::Endian::Little::E<idProduct::Reg>	_idProduct;
		/** 
		 */
		Oscl::Endian::Little::E<bcdDevice::Reg>	_bcdDevice;
		/** 
		 */
		iManufacturer::Reg						_iManufacturer;
		/** 
		 */
		iProduct::Reg							_iProduct;
		/** 
		 */
		iSerialNumber::Reg						_iSerialNumber;
		/** 
		 */
		bNumConfigurations::Reg					_bNumConfigurations;
	};


}
}
}

#endif
