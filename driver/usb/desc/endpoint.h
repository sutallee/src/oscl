/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_usb_desc_endpointh_
#define _oscl_drv_usb_desc_endpointh_
#include "oscl/bits/bitfield.h"
#include "oscl/endian/type.h"
#include "oscl/hw/usb/desc/endpreg.h"
#include "desc.h"

/** */
namespace Oscl {
/** */
namespace Usb {
/** */
namespace Desc {

// This using statement does not extend
// beyond the scope of the enclosing namespace.
// I verified this experimentally.
using namespace Oscl::Usb::Hw::Desc::Endpoint;

/** This structure describes the USB Endpoint
	Descriptor format.
	XXX: Oh No! This thing is an odd number of
		bytes long! However, USB 1.1 section 9.5
		states: "If the descriptor returns with
		a value in its length field that is *greater*
		than defined by this specification, the extra
		bytes are ignored by the host, but the next
		descriptor is located using the length returned
		rather than the length expected. Note that there
		is *no* USB requriement to align descriptors, and
		thus USB Word (2 byte) elements may not be correctly
		aligned for processor access :-(
 */
struct Endpoint : public Descriptor {
	public:
		/** This constructor allows the descriptor to be
			type cast onto an existing buffer without initializing
			the members.
		 */
		inline Endpoint() noexcept:Descriptor(){}
		/** */
		Endpoint(	uint8_t		bEndpointAddress,
					uint8_t		bmAttributes,
					uint16_t		wMaxPacketSize,
					uint8_t		bInterval
					) noexcept;
		/** 
		 */
		Oscl::BitField<bEndpointAddress::Reg>			_bEndpointAddress;
		/** 
		 */
		Oscl::BitField<	Hw::Desc::Endpoint::
						bmAttributes::Reg
						>								_bmAttributes;
		/** 
		 */
		Oscl::Endian::Little::E<wMaxPacketSize::Reg>	_wMaxPacketSize;
		/** */
		bInterval::Reg									_bInterval;
		/** */
		unsigned char	getEndpointID() const noexcept;
		/** */
		bool	directionIsIN() const noexcept;
	};


}
}
}

#endif
