/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_usb_desc_configh_
#define _oscl_drv_usb_desc_configh_
#include "oscl/endian/type.h"
#include "oscl/hw/usb/desc/configreg.h"
#include "desc.h"

/** */
namespace Oscl {
/** */
namespace Usb {
/** */
namespace Desc {

// This using statement does not extend
// beyond the scope of the enclosing namespace.
// I verified this experimentally.
using namespace Oscl::Usb::Hw::Desc::Config;

/** This structure describes the USB Configuration
	Descriptor format.
 */
struct Config : public Descriptor {
	public:
		/** This constructor allows the descriptor to be
			type cast onto an existing buffer without initializing
			the members.
		 */
		inline Config() noexcept:Descriptor(){};
		inline Config(	uint16_t	wTotalLength,
						uint8_t	bNumInterfaces,
						uint8_t	bConfigurationValue,
						uint8_t	iConfiguration,
						uint8_t	bmAttributes,
						uint8_t	bMaxPower
						) noexcept:
			Descriptor(sizeof(Config),bDescriptorType::CONFIGURATION)
			{
			_wTotalLength	= wTotalLength;
			_bNumInterfaces	= bNumInterfaces;
			_bConfigurationValue	= bConfigurationValue;
			_iConfiguration			= iConfiguration;
			_bmAttributes			= bmAttributes;
			_bMaxPower				= bMaxPower;
			};
		/** This contains the total length of all the descriptors
			associated with this configuration. Including configuration,
			interface, endpoint and class or vendor specific descriptors.
		 */
		Oscl::Endian::Little::E<wTotalLength::Reg>	_wTotalLength;
		/** This contains the number of interfaces supported by this
			configuration.
		 */
		bNumInterfaces::Reg							_bNumInterfaces;
		/** This contains a unique identifier for this configuration
			within the scope of the device.
		 */
		bConfigurationValue::Reg					_bConfigurationValue;
		/** This contains the string descriptor index of a string
			that describes this configuration.
		 */
		iConfiguration::Reg							_iConfiguration;
		/** This contains a bit map value that describes various
			standard features that are available for this configuration.
		 */
		bmAttributes::Reg							_bmAttributes;
		/** Maximum power consumption of the device from the USB bus
			in this configuration. Expressed in 2mA units.
		 */
		bMaxPower::Reg								_bMaxPower;
	public:
		/** */
		Descriptor*	findDescriptor(	bDescriptorType::Reg	type,
									unsigned				index,
									unsigned				maxLength=~0
									) const noexcept;
	};


}
}
}

#endif
