/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_usb_desc_serial_pl2303h_
#define _oscl_drv_usb_desc_serial_pl2303h_
#include "oscl/endian/type.h"
#include "oscl/hw/usb/desc/devicereg.h"
#include "oscl/hw/usb/desc/serial/pl2303reg.h"
#include "oscl/driver/usb/desc/desc.h"

/** */
namespace Oscl {
/** */
namespace Usb {
/** */
namespace Desc {
/** */
namespace Serial {
/** */
namespace PL2303 {

// This using statement does not extend
// beyond the scope of the enclosing namespace.
// I verified this experimentally.
using namespace Oscl::Usb::Hw::Desc::Serial::PL2303;

/** This structure describes the USB PL2303 Class
	Descriptor format.
 */
struct Header : public Descriptor {
	public:
		/** This constructor allows the descriptor to be
			type cast onto an existing buffer without initializing
			the members.
		 */
		inline Header() noexcept:Descriptor(){};
		inline Header(	uint8_t	bNbrPorts,
						unsigned	descLength
						) noexcept:
			Descriptor(	descLength,
						0	// FIXME: ??
//						Oscl::Usb::Hw::Desc::bDescriptorType::PL2303
						)
			{
			_bNbrPorts				= bNbrPorts;
			};
		/** Number of ports supported by this pl2303 */
		bNbrPorts::Reg							_bNbrPorts;
	};

}
}
}
}
}

#endif
