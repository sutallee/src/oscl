/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_usb_desc_stringh_
#define _oscl_drv_usb_desc_stringh_
#include "oscl/hw/usb/desc/word.h"
#include "desc.h"

/** */
namespace Oscl {
/** */
namespace Usb {
/** */
namespace Desc {

// This using statement does not extend
// beyond the scope of the enclosing namespace.
// I verified this experimentally.
//using namespace Oscl::Usb::Hw::Desc::String;

/** This structure describes the header of the USB String
	Descriptor format.
	The actual UNICODE string must immediate follow this
	structure and be composed of the number of 16-bit words
	specified in the constructor.
 */
struct StringHdr : public Descriptor {
	public:
		/** This constructor allows the descriptor to be
			type cast onto an existing buffer without initializing
			the members.
		 */
		inline StringHdr() noexcept:Descriptor(){}

		/** This constructor is used to create a descriptor.
			The nWordsInString argument must be less than or
			equal to 126.
		 */
		inline StringHdr(uint8_t	nWordsInString) noexcept:
				Descriptor(	sizeof(StringHdr)+(2*nWordsInString),
							bDescriptorType::STRING
							)
				{}

		/** */
		inline uint8_t	nWordsInString() const noexcept{
			return (_bLength-sizeof(StringHdr))/2;
			}

		/** */
		inline Word*	unicodeString() const noexcept{
			return (Word*)(((unsigned char*)this)+sizeof(StringHdr));
			}
	};

/** */
template <unsigned char nWords>
struct String : public StringHdr {
	public:
		/** */
		Oscl::Usb::Hw::Desc::Word	_uString[nWords];
		/** */
		String(const uint16_t	uString[]) noexcept:
				StringHdr(nWords)
				{
			for(unsigned i=0;i<nWords;++i){
				_uString[i]	= uString[i];
				}
			}
		/** */
		String(const char	uString[]) noexcept:
				StringHdr(nWords)
				{
			for(unsigned i=0;i<nWords;++i){
				_uString[i]	= uString[i];
				}
			}
	};

}
}
}

#endif
