/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_usb_desc_desch_
#define _oscl_drv_usb_desc_desch_
#include "type.h"
#include "oscl/hw/usb/desc/descreg.h"

/** */
namespace Oscl {
/** */
namespace Usb {
/** */
namespace Desc {

// This using statement does not extend
// beyond the scope of the enclosing namespace.
// I verified this experimentally.
using namespace Oscl::Usb::Hw::Desc;

/** */
struct Descriptor {
	public:
		/** */
		bLength::Reg			_bLength;
		/** */
		bDescriptorType::Reg	_bDescriptorType;
		
	public:
		/** */
		inline Descriptor() noexcept{}
		/** */
		inline Descriptor(	uint8_t	bLength,
							uint8_t	bDescriptorType
							) noexcept:
			_bLength(bLength),
			_bDescriptorType(bDescriptorType)
			{}
		/** Returns the type of the descriptor
		 */
		const Type::Api&	type() const noexcept;
	};

}
}
}


#endif
