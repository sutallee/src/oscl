/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "config.h"

Oscl::Usb::Desc::Descriptor*
Oscl::Usb::Desc::Config::findDescriptor(	bDescriptorType::Reg	type,
											unsigned				index,
											unsigned				maxLength
											) const noexcept{
	unsigned	lastByteIndex	= (maxLength > (unsigned)_wTotalLength )
									?_wTotalLength
									:maxLength
									;
	Descriptor*	desc;
	unsigned char*	p	= (unsigned char*)this;
	unsigned char*	end	= &p[lastByteIndex];
	unsigned		i	= 0;
	for(	unsigned char* p	= (unsigned char*)this;
			p < end;
			p	+= desc->_bLength
			){
		desc	= (Descriptor*)p;
		if(desc->_bDescriptorType == type){
			if(i == index){
				if((p+desc->_bLength) <= end){
					return desc;
					}
				return 0;
				}
			else {
				++i;
				}
			}
		}
	return 0;
	}
