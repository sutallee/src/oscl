/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_usb_ohci_hub_root_porth_
#define _oscl_drv_usb_ohci_hub_root_porth_
#include "oscl/hw/usb/ohci/reg.h"
#include "oscl/driver/usb/ohci/reg/rhportstat.h"
#include "oscl/queue/queueitem.h"
#include "oscl/queue/queue.h"
#include "oscl/memory/block.h"
#include "oscl/driver/usb/hub/port/reqapi.h"

/** */
namespace Oscl {
/** */
namespace Usb {
/** */
namespace Ohci {
/** */
namespace Hub {
/** */
namespace Root {

/** */
class Port :	public Oscl::Usb::Hub::Port::Req::Api,
				public Oscl::QueueItem
				{
	private:
		/** */
		Oscl::Usb::Ohci::Register::RootHubPortStatus		_portStatus;
		/** */
		Oscl::Usb::Hub::Port::Req::Api::ConcreteSAP			_hubPortSAP;
		/** */
		Oscl::Queue<NotifyPortChangeReq>					_notificationList;
		/** */
		Oscl::BitField<	Oscl::Usb::Hw::Hub::Port::
						wPortChange::Reg
						>									_portChangeStatus;

	public:
		/** */
		Port(	volatile Oscl::Usb::Ohci::
				RhPortStatus::Reg&				reg,
				Oscl::Mt::Itc::PostMsgApi&		myPapi
				) noexcept;

		/** */
		virtual ~Port(){}

		/** */
		void	initialize() noexcept;

		/** */
		void	statusChange() noexcept;

		/** */
		Oscl::Usb::Hub::Port::Req::Api::SAP&	getHubPortSAP() noexcept;

	private:	// Oscl::Usb::Hub::Port::Req::Api
		/** */
		void	request(SetResetReq& msg) noexcept;
		/** */
		void	request(SetSuspendReq& msg) noexcept;
		/** */
		void	request(SetPowerReq& msg) noexcept;
		/** */
		void	request(ClearEnableReq& msg) noexcept;
		/** */
		void	request(ClearSuspendReq& msg) noexcept;
		/** */
		void	request(ClearPowerReq& msg) noexcept;
		/** */
		void	request(ClearConnectChangeReq& msg) noexcept;
		/** */
		void	request(ClearResetChangeReq& msg) noexcept;
		/** */
		void	request(ClearEnableChangeReq& msg) noexcept;
		/** */
		void	request(ClearSuspendChangeReq& msg) noexcept;
		/** */
		void	request(ClearOverCurrentChangeReq& msg) noexcept;
		/** */
		void	request(CancelReq& msg) noexcept;
		/** */
		void	request(NotifyPortChangeReq& msg) noexcept;
		/** */
		void	request(CancelNotifyPortChangeReq& msg) noexcept;
		/** */
		void	request(GetPortStatusReq& msg) noexcept;
	};

}
}
}
}
}


#endif
