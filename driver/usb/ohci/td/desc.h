/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_usb_ohci_td_desch_
#define _oscl_drv_usb_ohci_td_desch_
#include "oscl/hw/usb/ohci/mem.h"
#include "oscl/bus/api.h"
#include "transapi.h"

/** */
namespace Oscl {
/** */
namespace Usb {
/** */
namespace Ohci {
/** */
namespace TD {

/** This class initializes and provides operations for manipulating
	the various fields of the OHCI USB transfer descriptor structure.
	Instances of this class and sub-classes must be aligned to a
	16-byte boundary per USB-OHCI.
 */
class Descriptor {
	protected:
		/** */
		TD::Map				_map;
		/** */
		TransferApi&		_tapi;
		/** */
		Oscl::Bus::Api&		_dmaBus;
	public:
		/** */
		enum CC {
			NoError				= Dword0::CC::Value_NoError,
			CRC					= Dword0::CC::Value_CRC,
			BitStuffing			= Dword0::CC::Value_BitStuffing,
			DataToggleMismatch	= Dword0::CC::Value_DataToggleMismatch,
			Stall				= Dword0::CC::Value_Stall,
			DeviceNotResponding	= Dword0::CC::Value_DeviceNotResponding,
			PidCheckFailure		= Dword0::CC::Value_PidCheckFailure,
			UnexpectedPID		= Dword0::CC::Value_UnexpectedPID,
			DataOverrun			= Dword0::CC::Value_DataOverrun,
			DataUnderrun		= Dword0::CC::Value_DataUnderrun,
			BufferOverrun		= Dword0::CC::Value_BufferOverrun,
			BufferUnderrun		= Dword0::CC::Value_BufferUnderrun,
			NotAccessed0		= Dword0::CC::Value_NotAccessed0,
			NotAccessed1		= Dword0::CC::Value_NotAccessed1
			};
	public:
		/** This field may be used by the owner of
			this TD to keep track of outstanding
			transfers.
		 */
		void*				__qitemlink;

	public:
		/** This is used by the done head mechanism.
		 */
		Descriptor*			_doneStackLink;

	public:
		/** */
		Descriptor(	TransferApi&		tapi,
					Oscl::Bus::Api&		dmaBus
					) noexcept;
		/** */
		uint32_t		getNextTD() noexcept;
		/** */
		Descriptor*		nextTD() noexcept;
		/** */
		void			setNextTD(Descriptor* td) noexcept;
		/** */
		TransferApi&	getTransferApi() noexcept;
		/** */
		void			invalidate() noexcept;
		/** */
		void			flush() noexcept;
		/** */
		CC				result() noexcept;
	};

}
}
}
}

#endif
