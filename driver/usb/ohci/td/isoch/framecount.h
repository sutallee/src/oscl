/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_usb_ohci_framecounth_
#define _oscl_drv_usb_ohci_framecounth_
#include "oscl/hw/usb/ohci/memory.h"

/** */
namespace Oscl {
/** */
namespace Usb {
/** */
namespace Ohci {
/** */
namespace TD {
/** */
namespace Isochronous {

/** The purpose of this abstract class is to provide a type safe
	means of specifying the interrupt delay field for USB 
	transfer descriptors.
 */
class FrameCount {
	private:
		/** This member contains the DI field of a transfer descriptor
			in the least significant bits of the value. This is *not*
			a "ValueMask", and must be shifted into the proper position
			before being placed in the OHCI transfer descriptor.
		 */
		unsigned char	_frameCountValue;
	protected:
		/** */
		FrameCount(unsigned char frameCountValue) noexcept:
			_frameCountValue(frameCountValue)
			{
			using namespace Oscl::Usb::Ohci::TD::Isochronous::Dword0;
			if(frameCountValue > FC::Value_Maximum){
				while(true);
				}
			}
	public:
		/** */
		TD::Dword0::Reg	getValueMask() const noexcept{
			using namespace Oscl::Usb::Ohci::TD::Isochronous::Dword0;
			return (((TD::Dword0::Reg)_frameCountValue) << FC::Lsb);
			}
		/** */
		FrameCount(const FrameCount& frameCount) noexcept:
			_frameCountValue(frameCount._frameCountValue)
			{}
	};

/** */
class OneFrame : public FrameCount {
	public:
		/** */
		OneFrame() noexcept:
			FrameCount(TD::Isochronous::Dword0::FC::Value_One)
			{}
		/** */
		OneFrame(const OneFrame& other) noexcept:
			FrameCount(other)
			{}
	};

/** */
class TwoFrame : public FrameCount {
	public:
		/** */
		TwoFrame() noexcept:
			FrameCount(TD::Isochronous::Dword0::FC::Value_Two)
			{}
	};

/** */
class ThreeFrame : public FrameCount {
	public:
		/** */
		ThreeFrame() noexcept:
			FrameCount(TD::Isochronous::Dword0::FC::Value_Three)
			{}
	};

/** */
class FourFrame : public FrameCount {
	public:
		/** */
		FourFrame() noexcept:
			FrameCount(TD::Isochronous::Dword0::FC::Value_Four)
			{}
	};

/** */
class FiveFrame : public FrameCount {
	public:
		/** */
		FiveFrame() noexcept:
			FrameCount(TD::Isochronous::Dword0::FC::Value_Five)
			{}
	};

/** */
class SixFrame : public FrameCount {
	public:
		/** */
		SixFrame() noexcept:
			FrameCount(TD::Isochronous::Dword0::FC::Value_Six)
			{}
	};

/** */
class SevenFrame : public FrameCount {
	public:
		/** */
		SevenFrame() noexcept:
			FrameCount(TD::Isochronous::Dword0::FC::Value_Seven)
			{}
	};

/** */
class EightFrame : public FrameCount {
	public:
		/** */
		EightFrame() noexcept:
			FrameCount(TD::Isochronous::Dword0::FC::Value_Eight)
			{}
	};

}
}
}
}
}

#endif
