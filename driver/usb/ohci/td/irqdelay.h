/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_usb_ohci_td_irqdelayh_
#define _oscl_drv_usb_ohci_td_irqdelayh_
#include "oscl/hw/usb/ohci/memory.h"

/** */
namespace Oscl {
/** */
namespace Usb {
/** */
namespace Ohci {
/** */
namespace TD {

/** The purpose of this abstract class is to provide a type safe
	means of specifying the interrupt delay field for USB 
	transfer descriptors.
 */
class IrqDelay {
	private:
		/** This member contains the DI field of a transfer descriptor
			in the least significant bits of the value. This is *not*
			a "ValueMask", and must be shifted into the proper position
			before being placed in the OHCI transfer descriptor.
		 */
		unsigned char	_irqDelayValue;
	public:
		/** */
		IrqDelay(unsigned char irqDelayValue) noexcept:
			_irqDelayValue(irqDelayValue)
			{
			using namespace Oscl::Usb::Ohci::TD::Dword0;
			if(irqDelayValue > DI::Value_Maximum){
				while(true);
				}
			}
	public:
		/** */
		TD::Dword0::Reg	getValueMask() const noexcept{
			using namespace Oscl::Usb::Ohci::TD::Dword0;
			return (((TD::Dword0::Reg)_irqDelayValue) << DI::Lsb);
			}
		/** */
		IrqDelay(const IrqDelay& irqDelay) noexcept:
			_irqDelayValue(irqDelay._irqDelayValue)
			{}
	};

/** */
class OneFrameDelay : public IrqDelay {
	public:
		/** */
		OneFrameDelay() noexcept:
			IrqDelay(TD::Dword0::DI::Value_OneFrame)
			{}
		/** */
		OneFrameDelay(const OneFrameDelay& other) noexcept:
			IrqDelay(other)
			{}
	};

/** */
class TwoFrameDelay : public IrqDelay {
	public:
		/** */
		TwoFrameDelay() noexcept:
			IrqDelay(TD::Dword0::DI::Value_TwoFrame)
			{}
	};

/** */
class ThreeFrameDelay : public IrqDelay {
	public:
		/** */
		ThreeFrameDelay() noexcept:
			IrqDelay(TD::Dword0::DI::Value_ThreeFrame)
			{}
	};

/** */
class FourFrameDelay : public IrqDelay {
	public:
		/** */
		FourFrameDelay() noexcept:
			IrqDelay(TD::Dword0::DI::Value_FourFrame)
			{}
	};

/** */
class FiveFrameDelay : public IrqDelay {
	public:
		/** */
		FiveFrameDelay() noexcept:
			IrqDelay(TD::Dword0::DI::Value_FiveFrame)
			{}
	};

/** */
class SixFrameDelay : public IrqDelay {
	public:
		/** */
		SixFrameDelay() noexcept:
			IrqDelay(TD::Dword0::DI::Value_SixFrame)
			{}
	};

/** */
class SevenFrameDelay : public IrqDelay {
	public:
		/** */
		SevenFrameDelay() noexcept:
			IrqDelay(TD::Dword0::DI::Value_SevenFrame)
			{}
	};

/** */
class NoInterrupt : public IrqDelay {
	public:
		/** */
		NoInterrupt() noexcept:
			IrqDelay(TD::Dword0::DI::Value_NoInterrupt)
			{}
	};
}
}
}
}

#endif
