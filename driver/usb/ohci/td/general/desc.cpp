/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "desc.h"
#include "oscl/error/fatal.h"
#include "oscl/kernel/mmu.h"			// FIXME: This should not be here.
										// An OHCI specific call should
										// be used to translate virtual
										// user addresses to physical
										// OCHI DMA addresses. The 
										// OS environment implementation
										// could then invoke the Plat
										// version. Also, should the
										// buffer be cache flushed when
										// loaded?

using namespace Oscl::Usb::Ohci::TD::General;

Descriptor::Descriptor(	TransferApi&		tapi,
						Oscl::Bus::Api&		dmaBus,
						bool				shortPacketOK,
						IrqDelay			irqDelay,
						void*				buffer,
						unsigned			size
						) noexcept:
		TD::Descriptor(tapi,dmaBus)
		{
	_map.general.dword0	|=
						(shortPacketOK << Dword0::R::Lsb)
					|	irqDelay.getValueMask()
					;
	unsigned char*	p	= (unsigned char*)buffer;
	void*	cpuAddress;

	cpuAddress	= OsclKernelVirtualToPhysical(p);
	// FIXME: I'm assuming the translation succeeds.
	if(size == 0){
		_map.general.dword1	= 0;
		_map.general.dword3	= 0;
		return;
		}

	uintptr_t	dword1;
	if(!_dmaBus.cpuToBus(cpuAddress,dword1)){
		Oscl::ErrorFatal::logAndExit(
			"Oscl::Usb::Ohci::TD::General::Descriptor: cpuToBus failure\n");
		}

	_map.general.dword1	= dword1;

	cpuAddress	= OsclKernelVirtualToPhysical(p+(size-1));
	// FIXME: I'm assuming the translation succeeds.
	uintptr_t	dword3;
	if(!_dmaBus.cpuToBus(cpuAddress,dword3)){
		Oscl::ErrorFatal::logAndExit(
			"Oscl::Usb::Ohci::TD::General::Descriptor: cpuToBus failure\n");
		}
	_map.general.dword3	= dword3;
	}

Oscl::Usb::Ohci::TD::TransferApi&	Descriptor::getTransferApi() noexcept{
	return _tapi;
	}

void*	Descriptor::getCurrentBufferPointer() const noexcept{
	Oscl::Usb::Ohci::TD::Dword1::Reg	dword1;
	dword1	= _map.general.dword1;
	if(!dword1){
		return 0;
		}
	void*	cpuAddress;
	if(!_dmaBus.busToCpu(dword1,cpuAddress)){
		Oscl::ErrorFatal::logAndExit(
			"Oscl::Usb::Ohci::TD::General::Descriptor:"
			" busToCpu failure\n"
			);
		}
	return cpuAddress;
	}

Setup::Setup(	TransferApi&		tapi,
				Oscl::Bus::Api&		dmaBus,
				IrqDelay			irqDelay,
				void*				buffer
				) noexcept:
		Descriptor(tapi,dmaBus,false,irqDelay,buffer,8)
		{
	_map.general.dword0	|=
						Dword0::DP::ValueMask_SETUP
					|	Dword0::T::Source::ValueMask_LSB
					|	Dword0::T::Toggle::ValueMask_DATA0
					;
	}

In::In(	TransferApi&	tapi,
		Oscl::Bus::Api&	dmaBus,
		bool			shortPacketOK,
		IrqDelay		irqDelay,
		void*			buffer,
		unsigned		bufferSize,
		bool			dataToggleSourceTD,
		bool			dataToggleValue
		) noexcept:
		Descriptor(tapi,dmaBus,shortPacketOK,irqDelay,buffer,bufferSize)
		{
	Dword0::Reg	dataToggleSourceMask;
	Dword0::Reg	dataToggleValueMask;
	dataToggleSourceMask	= (dataToggleSourceTD)
									? (Dword0::Reg)Dword0::T::Source::ValueMask_LSB
									: (Dword0::Reg)Dword0::T::Source::ValueMask_ToggleCary
									;
	dataToggleValueMask	= (dataToggleValue)
									? (Dword0::Reg)Dword0::T::Toggle::ValueMask_DATA1
									: (Dword0::Reg)Dword0::T::Toggle::ValueMask_DATA0
									;

	_map.general.dword0	|=
						Dword0::DP::ValueMask_IN
					|	dataToggleSourceMask
					|	dataToggleValueMask
					;
	}

Out::Out(	TransferApi&	tapi,
			Oscl::Bus::Api&	dmaBus,
			IrqDelay		irqDelay,
			void*			buffer,
			unsigned		nTxBytes
			) noexcept:
		Descriptor(tapi,dmaBus,false,irqDelay,buffer,nTxBytes)
		{
	_map.general.dword0	|= Dword0::DP::ValueMask_OUT;
	}

StatusIn::StatusIn(	TransferApi&		tapi,
					Oscl::Bus::Api&		dmaBus
					) noexcept:
		Descriptor(	tapi,
					dmaBus,
					true,
					TD::IrqDelay(TD::Dword0::DI::Value_OneFrame),
					0,
					0
					)
		{
	_map.general.dword0	|=
						Dword0::DP::ValueMask_IN
					|	Dword0::T::Toggle::ValueMask_DATA1
					|	Dword0::T::Source::ValueMask_LSB
					;
	}

StatusOut::StatusOut(	TransferApi&		tapi,
						Oscl::Bus::Api&		dmaBus
						) noexcept:
		Descriptor(	tapi,
					dmaBus,
					true,
					TD::IrqDelay(TD::Dword0::DI::Value_OneFrame),
					0,
					0
					)
		{
	_map.general.dword0	|=
						Dword0::DP::ValueMask_OUT
					|	Dword0::T::Toggle::ValueMask_DATA1
					|	Dword0::T::Source::ValueMask_LSB
					;
	}

