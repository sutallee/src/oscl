/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_usb_ohci_td_general_desch_
#define _oscl_drv_usb_ohci_td_general_desch_
#include "oscl/driver/usb/ohci/td/desc.h"
#include "oscl/hw/usb/ohci/mem.h"
#include "oscl/driver/usb/address.h"
#include "oscl/driver/usb/endpoint.h"
#include "oscl/driver/usb/ohci/td/irqdelay.h"
#include "oscl/bus/api.h"

/** */
namespace Oscl {
/** */
namespace Usb {
/** */
namespace Ohci {
/** */
namespace TD {
/** */
namespace General {

/** This class initializes and provides operations for manipulating
	the various fields of the OHCI USB transfer descriptor structure.
	Instances of this class and sub-classes must be aligned to a
	16-byte boundary per USB-OHCI.
 */
class Descriptor : public Oscl::Usb::Ohci::TD::Descriptor {
	public:
		/** */
		Descriptor(	TransferApi&		tapi,
					Oscl::Bus::Api&		dmaBus,
					bool				shortPacketOK,
					IrqDelay			irqDelay,
					void*				buffer,
					unsigned			size
					) noexcept;
		/** */
		TransferApi&	getTransferApi() noexcept;
		/** */
		void*			getCurrentBufferPointer() const noexcept;
	};

/** */
class Setup : public Descriptor {
	public:
		/** */
		Setup(	TransferApi&		tapi,
				Oscl::Bus::Api&		dmaBus,
				IrqDelay			irqDelay,
				void*				buffer
				) noexcept;
	};

/** */
class In : public Descriptor {
	public:
		/** */
		In(	TransferApi&		tapi,
			Oscl::Bus::Api&		dmaBus,
			bool				shortPacketOK,
			IrqDelay			irqDelay,
			void*				buffer,
			unsigned			bufferSize,
			bool				dataToggleSourceTD=0,
			bool				dataToggleValue=0
			) noexcept;
	};

/** */
class Out : public Descriptor {
	public:
		/** */
		Out(	TransferApi&		tapi,
				Oscl::Bus::Api&		dmaBus,
				IrqDelay			irqDelay,
				void*				buffer,
				unsigned			nTxBytes
				) noexcept;
	};

/** */
class StatusIn : public Descriptor {
	public:
		/** */
		StatusIn(	TransferApi&		tapi,
					Oscl::Bus::Api&		dmaBus
					) noexcept;
	};

/** */
class StatusOut : public Descriptor {
	public:
		/** */
		StatusOut(	TransferApi&		tapi,
					Oscl::Bus::Api&		dmaBus
					) noexcept;
	};

}
}
}
}
}


#endif
