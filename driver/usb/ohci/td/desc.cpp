/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "desc.h"
#include "oscl/cache/operations.h"		// FIXME: This should not be here.
										// An OHCI specific call should
										// be used to translate virtual
										// user addresses to physical
										// OCHI DMA addresses. The 
										// OS environment implementation
										// could then invoke the Plat
										// version. Also, should the
										// buffer be cache flushed when
										// loaded?

using namespace Oscl::Usb::Ohci::TD;
using namespace Oscl;

Descriptor::Descriptor(	TransferApi&		tapi,
						Oscl::Bus::Api&		dmaBus
						) noexcept:
		_tapi(tapi),
		_dmaBus(dmaBus)
		{
	// FIXME: Using general here for generic: change memory.reg
	// should be TD::Dword0::CC::ValueMask_NotAccessed1
	_map.general.dword0	= TD::Dword0::CC::ValueMask_NotAccessed1;
	}

uint32_t	Descriptor::getNextTD() noexcept{
	return _map.base.dword2;
	}

Descriptor*		Descriptor::nextTD() noexcept{
	void*		desc=0;
	_dmaBus.busToCpu(_map.base.dword2,desc);
	return (Descriptor*)desc;
	}

void			Descriptor::setNextTD(Descriptor* td) noexcept{
	uintptr_t	dword2;
	_dmaBus.cpuToBus(td,dword2);
	_map.base.dword2	= dword2;
	}

Oscl::Usb::Ohci::TD::TransferApi&	Descriptor::getTransferApi() noexcept{
	return _tapi;
	}

void	Descriptor::invalidate() noexcept{
	OsclCacheDataInvalidateRange(&_map,sizeof(_map));
	}

void	Descriptor::flush() noexcept{
	OsclCacheDataFlushRange(&_map,sizeof(_map));
	}

Oscl::Usb::Ohci::TD::Descriptor::CC
Descriptor::result() noexcept{
	unsigned	cc;
	cc	= (_map.base.dword0 & TD::Dword0::CC::FieldMask)>>TD::Dword0::CC::Lsb;
	return (Oscl::Usb::Ohci::TD::Descriptor::CC)cc;
	}

