/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "group.h"
#include "srnode.h"
#include "bwalloc.h"

using namespace Oscl::Usb::Ohci::BW;

Group::Group() noexcept:
		_firstSrNode(0)
		{
	}

bool	Group::bwAvailable(unsigned nBitTimes) const noexcept{
	// 12Mbps bit rate
	// 1K frames per second
	// 12Mbps/1Kfps = 12K = 12,000 raw bits/frame
	// 12K - sync - pid - ((frame number - crc5)*7)/6
	//	= 12K - 8 -8 - ((11+5)*7)/6
	//	= 12K - 16 - 19
	//	= 11,965 bits available per frame
	const unsigned	maxBitsInFrame			= 11965;
	unsigned	nBitsInUse	= _firstSrNode->bitsInUse();
	unsigned	availableBits	= maxBitsInFrame - nBitsInUse;
	if(nBitTimes > availableBits){
		return false;
		}
	return true;
	}

bool	Group::alloc(BwAlloc& bw) noexcept{
	return _firstSrNode->alloc(bw);
	}

void	Group::free(BwAlloc& bw) noexcept{
	return _firstSrNode->free(bw);
	}

void	Group::push(SrNode& node) noexcept{
	node._nextSrNode	= _firstSrNode;
	_firstSrNode		= &node;
	}

