/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "sfsr.h"

using namespace Oscl::Usb::Ohci::BW;

SfSr::SfSr(	Allocator&	context,
			SfNodePtr& sfNodeHead) noexcept:
		_sfNode(*this),
		_srNode(context,*this,sfNodeHead)
		{
	}

unsigned	SfSr::bitsInUse() const noexcept{
	unsigned	nBits	= 0;
	BwNode*	next;
	for(	next	= _list.first();
			next;
			next	= _list.next(next)
			){
		nBits	+= next->bitsInUse();
		}
	return nBits;
	}

void		SfSr::add(	EdPtr&	prev,
						EdPtr&	next,
						BwNode&	node
						) noexcept{
	BwNode*	p	= lastBwNode();
	if(p){
		prev	= &p->_bwAlloc._ed.getRefToNextPtr();
		}
	_list.put(&node);
	/*  This next trick uses the facilities of the endpoint
		to convert to a bus address.
	 */
	Oscl::Usb::Ohci::ED::Descriptor&	ed	= node._bwAlloc._ed;
	ed.setNextED(&ed);
	*prev	= node._bwAlloc._ed.getRefToNextPtr();
	if(next){
		node._bwAlloc._ed.getRefToNextPtr()	= *next;
		}
	else {
		node._bwAlloc._ed.getRefToNextPtr()	= 0;
		}
	}

BwNode&		SfSr::free(	EdPtr&	prev,
						BwAlloc&	node
						) noexcept{
	BwNode*	next;
	BwNode*	pNode	= 0;
	for(	next	= _list.first();
			next;
			next	= _list.next(next)
			){
		if(&next->_bwAlloc == &node){
			_list.remove(next);
			if(pNode){
				pNode->_bwAlloc._ed.getRefToNextPtr()
					= next->_bwAlloc._ed.getRefToNextPtr();
				}
			else{
				*prev	= next->_bwAlloc._ed.getRefToNextPtr();
				}
			return *next;
			}
		pNode	= next;
		}
	// FIXME: This should never happen
	while(true);
	return *(BwNode*)0;
	}

BwNode*		SfSr::lastBwNode() const noexcept{
	return _list.last();
	}

BwNode*		SfSr::firstBwNode() const noexcept{
	return _list.first();
	}

