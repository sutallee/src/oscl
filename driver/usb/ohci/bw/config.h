/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_usb_ohci_bw_configh_
#define _oscl_drv_usb_ohci_bw_configh_
#include <new>
#include "sfsr.h"
#include "edptr.h"
#include "sfnodeptr.h"
#include "interval.h"
#include "allocator.h"

/** */
namespace Oscl {
/** */
namespace Usb {
/** */
namespace Ohci {
/** */
namespace BW {

/** */
typedef Mem<sizeof(SfNodePtr)>	SfNodePtrMem;
/** */
typedef Mem<sizeof(Interval)>	IntervalMem;
/** */
typedef Mem<sizeof(SfSr)>		SfSrMem;
/** */
typedef Mem<sizeof(Group)>		GroupMem;
/** */
typedef Mem<sizeof(BwAlloc)>	BwAllocMem;

/** */
template <unsigned nLevels>
class Config : public Allocator {
	private:
		enum{theN	= (nLevels-1)};
		/** */
		enum{nFrames	= (1<<theN)};
		/** */
		enum{nSfSr	= (nLevels * (1<<theN))};
		/** */
		enum{nGroups	= (1<<nLevels)-1};
		/** max 1-byte nFrames period frames
			- 122 bits for 1-byte interrupt IN EP
			- 11,965 bits in a 1ms frame
			- 11,965/122 = 98
			- nFrames = 32
			- total BwAllocs = 32*98	= 3,136 records
			- total Bytes ~= 16 * 3,136 = 50,176 bytes = 49K bytes
		 */
		enum{nBwAlloc	= (98 * nFrames)};
		/** FIXME: I've assumed worst case nBwAlloc 1:1 with nodes */
		enum{nBwNode	= nBwAlloc};
		/** */
		SfNodePtrMem			_frameMem[1<<nLevels];
		/** */
		IntervalMem				_intervalMem[nLevels];
		/** */
		SfSrMem					_sfsrMem[nSfSr];
		/** */
		GroupMem				_groupMem[nGroups];
		/** */
		BwAllocMem				_bwAllocMem[nBwAlloc];
		/** */
		BwNodeMem				_bwNodeMem[nBwNode];
		/** */
		Oscl::Queue<BwAllocMem>	_freeBwAllocMem;
		/** */
		Oscl::Queue<BwNodeMem>	_freeBwNode;
		/** */
		Oscl::Queue<BwAlloc>	_activeBwAlloc;
		/** */
		SfNodePtr*				_frame;
		/** */
		Interval*				_interval;
		/** */
		Group*					_availableGroup;
		/** */
		unsigned				_nBitTimes;

	public:
		/** */
		Config(EdPtr edPtrs) noexcept;

		/** */
		bool		bwAvailable(	unsigned	pollPeriod,
									unsigned	nBitTimes
									) noexcept;

		/** */
		BwNodeMem*	allocBwNodeMem() noexcept;

		/** */
		void		free(BwNodeMem* mem) noexcept;

		/** */
		Oscl::Usb::Alloc::
		BW::Interrupt::BwRec*	alloc(	Oscl::Usb::Ohci::ED::
										Descriptor&				ed
										) noexcept;

		/** */
		void			free(	Oscl::Usb::Alloc::
								BW::Interrupt::BwRec& bwRec
								) noexcept;

		/** */
		BwAlloc*	find(	Oscl::Usb::Alloc::
							BW::Interrupt::BwRec&	bwRec
							) noexcept;
	private:
		/** */
		void		free(BwAlloc* bwAlloc) noexcept;

		/** */
		BwAllocMem*	allocBwAllocMem() noexcept;
		/** */
		void		free(BwAllocMem* mem) noexcept;

	};

template <unsigned nLevels>
Config<nLevels>::Config(EdPtr edPtrs) noexcept:
		_availableGroup(0)
		{
	
	_frame	= (SfNodePtr*)_frameMem;
	for(unsigned i=0;i<(1<<nLevels);++i){
		new(&_frame[i]) SfNodePtr(&edPtrs[i]);
		}

	_interval	= (Interval*)_intervalMem;
	for(unsigned i=0;i<nLevels;++i){
		new(&_interval[i]) Interval(1<<i);
		}

	Oscl::Queue<SfSrMem>	sfsrList;
	for(unsigned i=0;i<nSfSr;++i){
		sfsrList.put(&_sfsrMem[i]);
		}

	Oscl::Queue<GroupMem>	groupList;
	for(unsigned i=0;i<nGroups;++i){
		groupList.put(&_groupMem[i]);
		}

	for(unsigned i=0;i<nBwAlloc;++i){
		_freeBwAllocMem.put(&_bwAllocMem[i]);
		}

	for(unsigned i=0;i<nBwNode;++i){
		_freeBwNode.put(&_bwNodeMem[i]);
		}

	for(unsigned intervalN=0;intervalN<nLevels;++intervalN){
		const unsigned	frameIncr	= (1<<intervalN);
		const unsigned	nFrameIncr	= (1<<(theN-intervalN));

		const unsigned	nGroupsInInterval		= (1<<intervalN);
		for(unsigned groupN=0;groupN<nGroupsInInterval;++groupN){
			GroupMem*	groupMem	= groupList.get();
			if(!groupMem){
				// Logic ERROR
				while(true);
				}
			Group*	group	= new(groupMem) Group();
			_interval[intervalN].add(*group);
			unsigned groupOffset	= (nGroupsInInterval-groupN-1);

			for(unsigned sfsrN=0;sfsrN<nFrameIncr;++sfsrN){
				unsigned	sfsrIndex	= ((nFrameIncr-1)-sfsrN);
				unsigned	frameN	= (sfsrIndex*frameIncr)+groupOffset;

				// Interval=0;Group=0;
				// (((32-1)-0) * 1)+0	= 31
				// (((32-1)-1) * 1)+0	= 29
				// (((32-1)-31) * 1)+0	= 0
				// Interval=1;Group=0;
				// (((16-1)-0) * 2)+0	= 30
				// (((16-1)-1) * 2)+0	= 28
				// (((16-1)-15) * 2)+0	= 0
				// Interval=1;Group=1;
				// (((16-1)-0) * 2)+1	= 31
				// (((16-1)-1) * 2)+1	= 29
				// (((16-1)-15) * 2)+1	= 1
				// Interval=2;Group=0;
				// (((8-1)-0) * 4)+0	= 28
				// (((8-1)-1) * 4)+0	= 24
				// (((8-1)-7) * 4)+0	= 0
				// Interval=2;Group=1;
				// (((8-1)-0) * 4)+1	= 29
				// (((8-1)-1) * 4)+1	= 25
				// (((8-1)-7) * 4)+1	= 1

				SfSrMem*	sfsrMem	= sfsrList.get();
				if(!sfsrMem){
					// Logic ERROR
					while(true);
					}
				SfSr*	sfsr	= new(sfsrMem) SfSr(*this,_frame[frameN]);
				group->push(sfsr->_srNode);
				_frame[frameN].push(sfsr->_sfNode);
				}
			}
		}
	if(sfsrList.first()){
		// Logic ERROR
		while(true);
		}
	if(groupList.first()){
		// Logic ERROR
		while(true);
		}
	}

template <unsigned nLevels>
bool	Config<nLevels>::bwAvailable(	unsigned	pollPeriod,
										unsigned	nBitTimes
										) noexcept{
	if(_availableGroup){
		// FIXME: Use ErrorFatal , Logic Error!
		while(true);
		}
	pollPeriod	&= ((1<<(nLevels-1))-1); // Max poll rate
	for(unsigned i=0;i<nLevels;++i){
		unsigned	mask	= ~((1<<i)-1);
		unsigned	roundPeriod	= mask & pollPeriod;
		if(roundPeriod < _interval[i].getPollPeriod()){
			_availableGroup	= _interval[i].bwAvailable(nBitTimes);
			_nBitTimes		= nBitTimes;
			return _availableGroup;
			}
		}
	return false;
	}

template <unsigned nLevels>
BwNodeMem*	Config<nLevels>::allocBwNodeMem() noexcept{
	return _freeBwNode.get();
	}

template <unsigned nLevels>
void		Config<nLevels>::free(BwNodeMem* mem) noexcept{
	_freeBwNode.put(mem);
	}

template <unsigned nLevels>
Oscl::Usb::Alloc::BW::Interrupt::BwRec*
Config<nLevels>::alloc(	Oscl::Usb::Ohci::ED::
						Descriptor&				ed
						) noexcept{
	if(!_availableGroup){
		return 0;
		}
	BwAllocMem*	bwAllocMem	= allocBwAllocMem();
	if(!bwAllocMem){
		_availableGroup	= 0;
		return 0;
		}
	BwAlloc*	bwAlloc	= new(bwAllocMem) BwAlloc(	*_availableGroup,
													ed,
													_nBitTimes
													);

	if(!_availableGroup->alloc(*bwAlloc)){
		bwAlloc->~BwAlloc();
		free((BwAllocMem*)bwAlloc);
		bwAlloc	= 0;
		}
	else{
		_activeBwAlloc.put(bwAlloc);
		}
	_availableGroup	= 0;

	return bwAlloc;
	}

template <unsigned nLevels>
void	Config<nLevels>::free(	Oscl::Usb::Alloc::
								BW::Interrupt::BwRec& bwRec
								) noexcept{
	// FIXME: this should:
	//	- find BwAlloc
	//	- extract Group
	//	- execute free(BwAlloc*) on group
	BwAlloc*	bwAlloc	= find(bwRec);
	if(!bwAlloc){
		// FIXME: Logic Error should never happen
		while(true);
		}
	Group&	group	= bwAlloc->_group;
	group.free(*bwAlloc);
	}

template <unsigned nLevels>
void		Config<nLevels>::free(BwAlloc* bwAlloc) noexcept{
	_activeBwAlloc.remove(bwAlloc);
	bwAlloc->~BwAlloc();
	free((BwAllocMem*)bwAlloc);
	}

template <unsigned nLevels>
BwAlloc*	Config<nLevels>::find(	Oscl::Usb::Alloc::
									BW::Interrupt::BwRec&	bwRec
									) noexcept{
	BwAlloc*	next;
	for(	next	= _activeBwAlloc.first();
			next;
			next	= _activeBwAlloc.next(next)
			){
		Oscl::Usb::Alloc::BW::Interrupt::BwRec&	rec	= *next;
		if(&bwRec == &rec){
			return next;
			}
		}
	return 0;
	}

template <unsigned nLevels>
BwAllocMem*	Config<nLevels>::allocBwAllocMem() noexcept{
	return _freeBwAllocMem.get();
	}

template <unsigned nLevels>
void		Config<nLevels>::free(BwAllocMem* mem) noexcept{
	_freeBwAllocMem.put(mem);
	}

}
}
}
}

#endif
