/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <new>
#include "allocator.h"
#include "srnode.h"
#include "sfnodeptr.h"

using namespace Oscl::Usb::Ohci::BW;

SrNode::SrNode(	Allocator&	context,
				SfSr&		sfsr,
				SfNodePtr&	sfNodeHead
				) noexcept:
		_context(context),
		_sfsr(sfsr),
		_sfNodeHead(sfNodeHead)
		{
	}

unsigned	SrNode::bitsInUse() const noexcept{
	unsigned	sfBits	= _sfNodeHead.bitsInUse();
	if(_nextSrNode){
		unsigned srBits	= _nextSrNode->bitsInUse();
		if(srBits < sfBits){
			sfBits	= srBits;
			}
		}
	return sfBits;
	}

bool		SrNode::alloc(BwAlloc& bw) noexcept{
	BwNodeMem*	mem	= _context.allocBwNodeMem();
	if(!mem){
		return false;
		}
	if(_nextSrNode){
		if(!_nextSrNode->alloc(bw)){
			_context.free(mem);
			return false;
			}
		}
	BwNode*		node	= new(mem)BwNode(bw);
	_sfNodeHead.addNode(_sfsr,*node);
	return true;
	}

void		SrNode::free(BwAlloc& bw) noexcept{
	if(_nextSrNode){
		_nextSrNode->free(bw);
		}
	_sfNodeHead.freeNode(_sfsr,bw);
	}
