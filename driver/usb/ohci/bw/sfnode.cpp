/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "sfnode.h"
#include "sfsr.h"

using namespace Oscl::Usb::Ohci::BW;

SfNode::SfNode(SfSr& sfsr) noexcept:
		_sfsr(sfsr)
		{
	}

unsigned	SfNode::bitsInUse() noexcept{
	unsigned nBits	= _sfsr.bitsInUse();
	if(_nextSfNode){
		nBits	+= _nextSfNode->bitsInUse();
		}
	return nBits;
	}

void		SfNode::addNode(	EdPtr&	prev,
								SfSr&	sfsr,
								BwNode&	node
								) noexcept{
	if(&sfsr == &_sfsr){
		EdPtr	next	= 0;
		if(_nextSfNode){
			BwNode*	nextNode;
			nextNode	= _nextSfNode->nextBwNode();
			if(nextNode){
				next	= &nextNode->_bwAlloc._ed.getRefToNextPtr();
				}
			}
		_sfsr.add(	prev,
					next,
					node
					);
		return;
		}
	if(_nextSfNode){
		BwNode*	last	= _sfsr.lastBwNode();
		EdPtr	p	= 0;
		if(last){
			p	= &last->_bwAlloc._ed.getRefToNextPtr();
			}
		else{
			p	= prev;
			}
		_nextSfNode->addNode(p,sfsr,node);
		}
	else {
		// FIXME: Logic Error
		// This means the allocation structure was
		// incorrectly constructed.
		while(true);
		}
	}

BwNode&		SfNode::freeNode(	EdPtr&		prev,
								SfSr&		sfsr,
								BwAlloc&	bw
								) noexcept{
	if(&sfsr == &_sfsr){
		return _sfsr.free(	prev,
							bw
							);
		}
	if(_nextSfNode){
		BwNode*	last	= _sfsr.lastBwNode();
		EdPtr	p	= 0;
		if(last){
			p	= &last->_bwAlloc._ed.getRefToNextPtr();
			}
		else{
			p	= prev;
			}
		return _nextSfNode->freeNode(p,sfsr,bw);
		}
	else {
		// FIXME: Logic Error
		// This means the allocation structure was
		// incorrectly constructed.
		while(true);
		}
	return *(BwNode*)0;
	}

BwNode*		SfNode::nextBwNode() noexcept{
	BwNode*	node	= _sfsr.firstBwNode();
	if(!node){
		if(_nextSfNode){
			return _nextSfNode->nextBwNode();
			}
		}
	return node;
	}

