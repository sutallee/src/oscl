/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_usb_ohci_hcd_irp_td_tdh_
#define _oscl_drv_usb_ohci_hcd_irp_td_tdh_
#include "oscl/queue/queueitem.h"
#include "oscl/driver/usb/ohci/td/general/desc.h"
#include "oscl/driver/usb/ohci/hcd/irp/api.h"
#include "oscl/driver/usb/setup.h"

/** */
namespace Oscl {
/** */
namespace Usb {
/** */
namespace Ohci {
/** */
namespace HCD {
/** */
namespace IRP {
/** */
namespace TD {

/** */
class Setup :	public Oscl::Usb::Ohci::TD::TransferApi,
				public Oscl::QueueItem
				{
	public:
		/** */
		Setup(	Oscl::Usb::Ohci::HCD::IRP::Api&	irp,
				Oscl::Bus::Api&					dmaBus,
				Oscl::Usb::Ohci::IrqDelay&		irqDelay,
				Oscl::Usb::Setup&				setup
				) noexcept;
	private: // Oscl::Usb::Ohci::TD::TransferApi
		/** */
		void	done() noexcept;
	};

}
}
}
}
}
}

#endif
