/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <new>
#include "oscl/cpu/sync.h"
#include "oscl/cache/operations.h"
#include "driver.h"
#include "oscl/driver/usb/pipe/status.h"
#include "oscl/driver/usb/pipe/info.h"

using namespace Oscl::Usb::Ohci::HCD::Pipe::Message;

static const Oscl::Usb::Pipe::Status::Result*
parseResult(Oscl::Usb::Ohci::TD::Descriptor::CC result) noexcept{
	using namespace Oscl::Usb::Ohci::TD;
	switch(result){
		case Descriptor::NoError:
			return 0;
		case Descriptor::CRC:
			return &Oscl::Usb::Pipe::Status::getCRC();
		case Descriptor::BitStuffing:
			return &Oscl::Usb::Pipe::Status::getBitStuffing();
		case Descriptor::DataToggleMismatch:
			return &Oscl::Usb::Pipe::Status::getDataToggleMismatch();
		case Descriptor::Stall:
			return &Oscl::Usb::Pipe::Status::getStall();
		case Descriptor::DeviceNotResponding:
			return &Oscl::Usb::Pipe::Status::getDeviceNotResponding();
		case Descriptor::PidCheckFailure:
			return &Oscl::Usb::Pipe::Status::getPidCheckFailure();
		case Descriptor::UnexpectedPID:
			return &Oscl::Usb::Pipe::Status::getUnexpectedPID();
		case Descriptor::DataOverrun:
			return &Oscl::Usb::Pipe::Status::getDataOverrun();
		case Descriptor::DataUnderrun:
			return &Oscl::Usb::Pipe::Status::getDataUnderrun();
		case Descriptor::BufferOverrun:
			return &Oscl::Usb::Pipe::Status::getBufferOverrun();
		case Descriptor::BufferUnderrun:
			return &Oscl::Usb::Pipe::Status::getBufferUnderrun();
		default:
			for(;;);
		};
	}
Driver::Driver(	Oscl::Mt::Itc::PostMsgApi&		myPapi,
				Oscl::Usb::Ohci::ED::Message&	ed,
				Oscl::Usb::Ohci::HCD::HcdApi&	hcdContext
				) noexcept:
		_ocSync(*this,myPapi),
		_sap(*this,myPapi),
		_ed(ed),
		_hcdContext(hcdContext),
		_skipCount(0),
		_maxPacketSize(ed.getMaxPacketSize()),
		_closeReq(0),
		_nFreeTransMem(0),
		_opened(true)
		{
	for(unsigned i=0;i<maxTransfers;++i){
		free(&_transMem[i]);
		}
	}

Oscl::Usb::Pipe::Message::Req::Api::SAP&	Driver::getSAP() noexcept{
	return _sap;
	}

Oscl::Mt::Itc::Srv::
Close::Req::Api::SAP&	Driver::getCloseSAP() noexcept{
	return _ocSync.getSAP();
	}

Oscl::Mt::Itc::Srv::
OpenCloseSyncApi&		Driver::getOpenCloseSyncApi() noexcept{
	return _ocSync;
	}

Oscl::Usb::Ohci::ED::Message&			Driver::getED() noexcept{
	return _ed;
	}

unsigned	Driver::getMaxPacketSize() const noexcept{
	return _maxPacketSize;
	}

void		Driver::cancelAllPendingTransactions() noexcept{
	TransApi*	next;
	while((next=_pendingTransactions.get())){
		next->cancel();
		}
	}

void	Driver::free(NoDataTrans* xfer) noexcept{
	remove(xfer);
	xfer->~NoDataTrans();
	free((TransMem*)xfer);
	if(_deferredQ.first()){
		Oscl::Mt::Itc::Msg*	msg	= _deferredQ.get();
		msg->process();
		}
	}

void	Driver::free(ReadDataTrans* xfer) noexcept{
	remove(xfer);
	xfer->~ReadDataTrans();
	free((TransMem*)xfer);
	if(_deferredQ.first()){
		Oscl::Mt::Itc::Msg*	msg	= _deferredQ.get();
		msg->process();
		}
	}

void	Driver::free(WriteDataTrans* xfer) noexcept{
	remove(xfer);
	xfer->~WriteDataTrans();
	free((TransMem*)xfer);
	if(_deferredQ.first()){
		Oscl::Mt::Itc::Msg*	msg	= _deferredQ.get();
		msg->process();
		}
	}

void	Driver::free(TransMem* mem) noexcept{
	++_nFreeTransMem;
	_freeTransMem.put((TransMem*)mem);
	if(!_opened){
		if(_nFreeTransMem == maxTransfers){
			if(_closeReq){
				_closeReq->returnToSender();
				_closeReq	= 0;
				}
			}
		}
	}

Driver::TransMem*	Driver::allocTransMem() noexcept{
	TransMem*	tm	= _freeTransMem.get();
	if(tm){
		--_nFreeTransMem;
		}
	return tm;
	}

Driver::TransApi*	Driver::remove(TransApi* trans) noexcept{
	TransApi*
	removed	= _pendingTransactions.remove(trans);
	return removed;
	}

void	Driver::pending(TransApi* trans) noexcept{
	_pendingTransactions.put(trans);
	}

void	Driver::skip() noexcept{
	if(!_skipCount){
		_ed.skip();
		}
	++_skipCount;
	}

void	Driver::dontSkip() noexcept{
	if(_skipCount){
		if(_ed.tdListIsEmpty()){
			if(_ed.isHalted()){
				_ed.clearHalted();
				OsclCpuInOrderMemoryAccessBarrier();
				_hcdContext.controlListFilled();
				}
			}
		else {
			if(_ed.isHalted()){
				cancelAllPendingTransactions();
				}
			}
		_ed.dontSkip();
		--_skipCount;
		}
	else {
		// FIXME:
		while(true);
		}
	}

void	Driver::request(CancelReq& msg) noexcept{
	Oscl::Mt::Itc::SrvMsg&	irp	= msg.getPayload()._irpToCancel;
	if(_deferredQ.remove(&irp)){
		irp.returnToSender();
		msg.returnToSender();
		return;
		}
	TransApi*	next;
	for(	next	= _pendingTransactions.first();
			next;
			next	= _pendingTransactions.next(next)
			){
		if(next->irpMatch(msg.getPayload()._irpToCancel)){
			remove(next);
			next->cancel(msg);
			return;
			}
		}
	msg.returnToSender();
	}

void	Driver::request(NoDataReq& msg) noexcept{
	if(!_opened){
		msg.getPayload()._failed	=
			&Oscl::Usb::Pipe::Status::getPipeClosed();
		msg.returnToSender();
		return;
		}
	TransMem*	mem	= allocTransMem();
	if(!mem){
		_deferredQ.put(&msg);
		return;
		}
	NoDataTrans*	t	= new(mem) NoDataTrans(*this,_hcdContext,_ed,msg);
	pending(t);
	}

void	Driver::request(ReadReq& msg) noexcept{
	if(!_opened){
		msg.getPayload()._failed	=
			&Oscl::Usb::Pipe::Status::getPipeClosed();
		msg.returnToSender();
		return;
		}
	TransMem*	mem	= allocTransMem();
	if(!mem){
		_deferredQ.put(&msg);
		return;
		}
	ReadDataTrans*	t	= new(mem) ReadDataTrans(*this,_hcdContext,_ed,msg);
	pending(t);
	}

void	Driver::request(WriteReq& msg) noexcept{
	if(!_opened){
		msg.getPayload()._failed	=
			&Oscl::Usb::Pipe::Status::getPipeClosed();
		msg.returnToSender();
		return;
		}
	TransMem*	mem	= allocTransMem();
	if(!mem){
		_deferredQ.put(&msg);
		return;
		}
	WriteDataTrans*	t	= new(mem) WriteDataTrans(*this,_hcdContext,_ed,msg);
	pending(t);
	}

void	Driver::request(OpenReq& msg) noexcept{
	// FIXME: Not sure we'll be using this.
	while(true);
	}

void	Driver::request(CloseReq& msg) noexcept{
	// FIXME: Need to cancel all pending IRPs
	_opened		= false;
	if(_nFreeTransMem == maxTransfers){
		msg.returnToSender();
		return;
		}
	_closeReq	= &msg;
	cancelAllPendingTransactions();
	}

//////// Driver::NoDataTrans ////////

Driver::NoDataTrans::NoDataTrans(	Driver&					context,
									Oscl::Usb::Ohci::HCD::
									HcdApi&					hcdContext,
									Oscl::Usb::Ohci::ED::
									Message&				ed,
									NoDataReq&				irp
									) noexcept:
		_otherTransfer(*this,&Driver::NoDataTrans::otherTransferComplete),
		_lastTransfer(*this,&Driver::NoDataTrans::lastTransferComplete),
		_context(context),
		_hcdContext(hcdContext),
		_ed(ed),
		_irp(irp),
		_cancelReq(0),
		_nTDs(0),
		_canceling(false),
		_retryCancel(false),
		_result(0)
		{
	tdAlloc();
	}

bool	Driver::NoDataTrans::irpMatch(Oscl::Mt::Itc::SrvMsg& irp) noexcept{
	Oscl::Mt::Itc::SrvMsg&	msg	= _irp;
	return (&irp == &msg);
	}

void	Driver::NoDataTrans::cancel(CancelReq& cancelReq) noexcept{
	// 1. disable endpoint.
	// 1.1 Set Transaction to cancelling state.
	// 2. register SOF delayed task
	// 3. first SOF delayed task process() moves to "armed" state.
	// 4. second SOF delayed task process() removes all outstanding
	//    TDs from ED lists.
	// 5. retire IRP
	if(_cancelReq){
		//FIXME:
		while(true);
		}
	_cancelReq	= &cancelReq;
	_result	= &Oscl::Usb::Pipe::Status::getCanceled();
	cancel();
	}

void	Driver::NoDataTrans::cancel() noexcept{
	if(_canceling) return;
	_canceling	= true;
	_context.skip();
	_sofFrameNumber	= _hcdContext.getCurrentFrameNumber();
	_hcdContext.queueSofTask(cancelSofTask());
	}

void Driver::NoDataTrans::tdAlloc() noexcept{
	while(_nTDs<nRequiredTDs){
		TransactionDescMem*	tdMem	= _hcdContext.allocTdMem();
		if(!tdMem){
			_hcdContext.queueTdMemTask(tdMemTask());
			return;
			}
		_tdMem.put(tdMem);
		++_nTDs;
		}
	executeIRP();
	}

void	Driver::NoDataTrans::cancelSof() noexcept{
	if(_sofFrameNumber == _hcdContext.getCurrentFrameNumber()){
		_hcdContext.queueSofTask(cancelSofTask());
		return;
		}
	// This is where we re-claim outstanding TDs from
	// the ED.
	using namespace Oscl::Usb::Ohci;
	TD::Descriptor*	next;
	while((next=_pendingTdList.get())){
		if(!_ed.remove(next)){
			// FIXME: Use ErrorFatal
			// Where is that descriptor?
			// This TD *must* be in the Done List
			// Thus, push the TD back into the
			// _pendingTdList.
			// However, it appears that this should
			// not happen if cancel() does not
			// invoke cancelSof() if halt is set
			// without delaying until after SOF.
			if(_retryCancel){
				while(true);
				}
			_retryCancel	= true;
			_pendingTdList.push(next);
			_hcdContext.queueSofTask(cancelSofTask());
			}
		next->getTransferApi().done(*next);
		}
	_context.dontSkip();
	if(_result){
		Oscl::Usb::Pipe::Status::Info	info;
		_result->query(info);
		}
	_irp.getPayload()._failed	= _result;
	_irp.returnToSender();
	if(_cancelReq){
		_cancelReq->returnToSender();
		}
	_context.free(this);
	}

void	Driver::NoDataTrans::executeIRP() noexcept{
	void*							mem;
	TD::General::Setup*				setupTD;
	const Oscl::Usb::Setup::Packet*	setupPkt;
	TD::General::StatusIn*			statusTD;
	TD::Descriptor*					termTD;

	//
	setupPkt	= _irp.getPayload().getSetupBuffer();
	OsclCacheDataFlushRange((void*)setupPkt,sizeof(Oscl::Usb::Setup::Packet));

	//
	mem		= _ed.getTdQueueTailPointer();
	setupTD	= new(mem)	TD::General::Setup(	_otherTransfer,
											_hcdContext.getDmaBusApi(),
											TD::IrqDelay(	TD::Dword0::DI::
															Value_NoInterrupt
															),
											(void*)setupPkt
											);
	_pendingTdList.put(setupTD);
	//
	mem	= _tdMem.get();
	statusTD	= new(mem)
		TD::General::StatusIn(	_lastTransfer,
								_hcdContext.getDmaBusApi()
								);
	_pendingTdList.put(statusTD);
	//
	mem	= _tdMem.get();
	termTD	= new(mem) TD::Descriptor(	_otherTransfer,
										_hcdContext.getDmaBusApi()
										);

	//
	_ed.put(statusTD);
	_ed.put(termTD);
	OsclCpuInOrderMemoryAccessBarrier();
	_hcdContext.controlListFilled();
	}

void	Driver::NoDataTrans::
lastTransferComplete(TD::Descriptor& desc) noexcept{
	using namespace Oscl::Usb::Ohci::TD;
	_pendingTdList.remove(&desc);
	if(!_canceling){
		_irp.getPayload()._failed	= parseResult(desc.result());
		_irp.returnToSender();
		_context.free(this);
		}
	_hcdContext.free(desc);
	}

void	Driver::NoDataTrans::
otherTransferComplete(TD::Descriptor& desc) noexcept{
	using namespace Oscl::Usb::Ohci::TD;
	_pendingTdList.remove(&desc);
	if(!_canceling){
		if(desc.result() != Descriptor::NoError){
			_result	= parseResult(desc.result());
			cancel();
			}
		}
	_hcdContext.free(desc);
	}

Oscl::Usb::Ohci::HCD::DeferredTaskApi&
Driver::NoDataTrans::tdMemTask() noexcept{
	using namespace Oscl::Usb::Ohci::HCD;
	DeferredTask<NoDataTrans>*	dt;
	dt	= new(&_dtMem)
				DeferredTask<NoDataTrans> (	*this,
											&Driver::NoDataTrans::tdAlloc
											);
	return *dt;
	}

Oscl::Usb::Ohci::HCD::DeferredTaskApi&
Driver::NoDataTrans::cancelSofTask() noexcept{
	using namespace Oscl::Usb::Ohci::HCD;
	DeferredTask<NoDataTrans>*	dt;
	dt	= new(&_dtMem)
				DeferredTask<NoDataTrans> (	*this,
											&Driver::NoDataTrans::
											cancelSof
											);
	return *dt;
	}

//////// Driver::ReadDataTrans ////////

Driver::ReadDataTrans::ReadDataTrans(	Driver&					context,
										Oscl::Usb::Ohci::HCD::
										HcdApi&					hcdContext,
										Oscl::Usb::Ohci::ED::
										Message&				ed,
										ReadReq&				irp
									) noexcept:
		_otherTransfer(*this,&Driver::ReadDataTrans::otherTransferComplete),
		_lastTransfer(*this,&Driver::ReadDataTrans::lastTransferComplete),
		_context(context),
		_hcdContext(hcdContext),
		_ed(ed),
		_irp(irp),
		_cancelReq(0),
		_nTDs(0),
		_canceling(false),
		_retryCancel(false),
		_result(0)
		{
	unsigned		nDataTDs;
	const unsigned	nSetupTDs = 1;
	const unsigned	nStatusTDs = 1;
	const unsigned	maxPacketSize = context.getMaxPacketSize();
	_dataLength		=	irp.
						getPayload().
						getSetupBuffer()->
						getDataLength()
						;
	nDataTDs	= (_dataLength+(maxPacketSize-1))/maxPacketSize;
	_nRequiredTDs	= nDataTDs+nSetupTDs+nStatusTDs;
	tdAlloc();
	}

bool	Driver::ReadDataTrans::irpMatch(Oscl::Mt::Itc::SrvMsg& irp) noexcept{
	Oscl::Mt::Itc::SrvMsg&	msg	= _irp;
	return (&irp == &msg);
	}

void	Driver::ReadDataTrans::cancel(CancelReq& cancelReq) noexcept{
	if(_cancelReq){
		//FIXME:
		while(true);
		}
	_cancelReq	= &cancelReq;
	_result	= &Oscl::Usb::Pipe::Status::getCanceled();
	cancel();
	}

void	Driver::ReadDataTrans::cancel() noexcept{
	if(_canceling) return;
	_canceling	= true;
	_context.skip();
	_sofFrameNumber	= _hcdContext.getCurrentFrameNumber();
	_hcdContext.queueSofTask(cancelSofTask());
	}

void Driver::ReadDataTrans::tdAlloc() noexcept{
	while(_nTDs<_nRequiredTDs){
		TransactionDescMem*	tdMem	= _hcdContext.allocTdMem();
		if(!tdMem){
			_hcdContext.queueTdMemTask(tdMemTask());
			return;
			}
		_tdMem.put(tdMem);
		++_nTDs;
		}
	executeIRP();
	}

void	Driver::ReadDataTrans::cancelSof() noexcept{
	if(_sofFrameNumber == _hcdContext.getCurrentFrameNumber()){
		_hcdContext.queueSofTask(cancelSofTask());
		return;
		}
	// This is where we re-claim outstanding TDs from
	// the ED.
	using namespace Oscl::Usb::Ohci;
	TD::Descriptor*	next;
	while((next=_pendingTdList.get())){
		if(!_ed.remove(next)){
			if(_retryCancel){
				// FIXME: Use ErrorFatal
				// Where is that descriptor?
				while(true);
				}
			// FIXME: This is a hack!
			// Try one more time
			_retryCancel	= true;
			_pendingTdList.push(next);
			_hcdContext.queueSofTask(cancelSofTask());
			}
		next->getTransferApi().done(*next);
		}
	_context.dontSkip();
	if(_result){
		Oscl::Usb::Pipe::Status::Info	info;
		_result->query(info);
		}
	_irp.getPayload()._failed	= _result;
	_irp.returnToSender();
	if(_cancelReq){
		_cancelReq->returnToSender();
		}
	_context.free(this);
	}

void	Driver::ReadDataTrans::executeIRP() noexcept{
	void*				mem;
	TD::General::Setup*				setupTD;
	const Oscl::Usb::Setup::Packet*	setupPkt;
	TD::General::In*				dataTD;
	TD::General::StatusOut*			statusTD;
	TD::Descriptor*					termTD;

	//
	setupPkt	= _irp.getPayload().getSetupBuffer();
	OsclCacheDataFlushRange((void *)setupPkt,sizeof(Oscl::Usb::Setup::Packet));

	//
	mem		= _ed.getTdQueueTailPointer();
	setupTD	= new(mem)	TD::General::Setup(	_otherTransfer,
											_hcdContext.getDmaBusApi(),
											TD::IrqDelay(	TD::Dword0::DI::
															Value_SevenFrame
															),
											(void*)setupPkt
											);
	_pendingTdList.put(setupTD);
	//
	mem	= _tdMem.get();
	termTD	= new(mem) TD::Descriptor(	_otherTransfer,
										_hcdContext.getDmaBusApi()
										);
	--_nTDs;

	// !! At this point I need to ensure that
	// the first IN TD has a T field of binary '11'
	// that forces the data PID to DATA1.
	//
	const unsigned	maxPacketSize	= _context.getMaxPacketSize();
	unsigned		dataLength		= _dataLength;
	unsigned char*	buffer;
	bool			data12	= true;
	buffer	= (unsigned char*)_irp.getPayload().getDataBuffer();
	for(;_nTDs>2;--_nTDs,buffer += maxPacketSize){
		mem	= _tdMem.get();
		dataTD	= new(mem)
			TD::General::In(	_otherTransfer,
								_hcdContext.getDmaBusApi(),
								true,
								TD::IrqDelay(TD::Dword0::DI::Value_NoInterrupt),
								buffer,
								maxPacketSize,
								true,	// Source of DATA1/DATA0 is TD
								data12	// DATA1/DATA2
								);
		data12	= !data12;
		_pendingTdList.put(dataTD);
		_ed.put(dataTD);
		}

	// Last buffer
	mem	= _tdMem.get();
	dataLength	= dataLength%maxPacketSize;
	if(!dataLength){
		dataLength	= maxPacketSize;
		}
	dataTD	= new(mem)
		TD::General::In(	_otherTransfer,
							_hcdContext.getDmaBusApi(),
							true,
							TD::IrqDelay(TD::Dword0::DI::Value_NoInterrupt),
							buffer,
							dataLength,
							true,
							data12
							);
	_pendingTdList.put(dataTD);
	_ed.put(dataTD);

	mem	= _tdMem.get();
	statusTD	= new(mem)
		TD::General::StatusOut(	_lastTransfer,
								_hcdContext.getDmaBusApi()
								);
	_pendingTdList.put(statusTD);
	_ed.put(statusTD);
	_ed.put(termTD);
	OsclCpuInOrderMemoryAccessBarrier();
	_hcdContext.controlListFilled();
	}

void	Driver::ReadDataTrans::
lastTransferComplete(TD::Descriptor& desc) noexcept{
	using namespace Oscl::Usb::Ohci::TD;
	_pendingTdList.remove(&desc);
	if(!_canceling){
		Descriptor::CC	result	= desc.result();
		if(result != Descriptor::NoError){
			if(result == Descriptor::Stall){
				if(_ed.isHalted()){
					_ed.clearHalted();
					OsclCpuInOrderMemoryAccessBarrier();
					_hcdContext.controlListFilled();
					}
				_result	= 0;	// The special case
				}
			else {
				_result	= parseResult(result);
				}
			}
		if(_result){
			Oscl::Usb::Pipe::Status::Info	info;
			_result->query(info);
			}
		_irp.getPayload()._failed	= _result;
		_irp.returnToSender();
		_context.free(this);
		}
	_hcdContext.free(desc);
	}

void	Driver::ReadDataTrans::
otherTransferComplete(TD::Descriptor& desc) noexcept{
	using namespace Oscl::Usb::Ohci::TD;
	_pendingTdList.remove(&desc);
	if(!_canceling){
		Descriptor::CC	result	= desc.result();
		if(result != Descriptor::NoError){
			_result	= parseResult(result);	// NEW
			cancel();
			}
		}
	_hcdContext.free(desc);
	}

Oscl::Usb::Ohci::HCD::DeferredTaskApi&
Driver::ReadDataTrans::tdMemTask() noexcept{
	using namespace Oscl::Usb::Ohci::HCD;
	DeferredTask<ReadDataTrans>*	dt;
	dt	= new(&_dtMem)
				DeferredTask<ReadDataTrans> (	*this,
												&Driver::ReadDataTrans::
												tdAlloc
												);
	return *dt;
	}

Oscl::Usb::Ohci::HCD::DeferredTaskApi&
Driver::ReadDataTrans::cancelSofTask() noexcept{
	using namespace Oscl::Usb::Ohci::HCD;
	DeferredTask<ReadDataTrans>*	dt;
	dt	= new(&_dtMem)
				DeferredTask<ReadDataTrans> (	*this,
												&Driver::ReadDataTrans::
												cancelSof
												);
	return *dt;
	}

//////// Driver::WriteDataTrans ////////

Driver::WriteDataTrans::WriteDataTrans(	Driver&					context,
										Oscl::Usb::Ohci::HCD::
										HcdApi&					hcdContext,
										Oscl::Usb::Ohci::ED::
										Message&				ed,
										WriteReq&				irp
									) noexcept:
		_otherTransfer(*this,&Driver::WriteDataTrans::otherTransferComplete),
		_lastTransfer(*this,&Driver::WriteDataTrans::lastTransferComplete),
		_context(context),
		_hcdContext(hcdContext),
		_ed(ed),
		_irp(irp),
		_cancelReq(0),
		_nTDs(0),
		_canceling(false),
		_retryCancel(false),
		_result(0)
		{
	unsigned		nDataTDs;
	const unsigned	nSetupTDs = 1;
	const unsigned	nStatusTDs = 1;
	const unsigned	maxPacketSize = context.getMaxPacketSize();
	_dataLength		=	irp.
						getPayload().
						getSetupBuffer()->
						getDataLength()
						;
	nDataTDs	= (_dataLength+(maxPacketSize-1))/maxPacketSize;
	_nRequiredTDs	= nDataTDs+nSetupTDs+nStatusTDs;
	tdAlloc();
	}

bool	Driver::WriteDataTrans::irpMatch(Oscl::Mt::Itc::SrvMsg& irp) noexcept{
	Oscl::Mt::Itc::SrvMsg&	msg	= _irp;
	return (&irp == &msg);
	}

void	Driver::WriteDataTrans::cancel(CancelReq& cancelReq) noexcept{
	if(_cancelReq){
		//FIXME:
		while(true);
		}
	_cancelReq	= &cancelReq;
	_result	= &Oscl::Usb::Pipe::Status::getCanceled();
	cancel();
	}

void	Driver::WriteDataTrans::cancel() noexcept{
	if(_canceling) return;
	_canceling	= true;
	_context.skip();
	_sofFrameNumber	= _hcdContext.getCurrentFrameNumber();
	_hcdContext.queueSofTask(cancelSofTask());
	}

void Driver::WriteDataTrans::tdAlloc() noexcept{
	while(_nTDs<_nRequiredTDs){
		TransactionDescMem*	tdMem	= _hcdContext.allocTdMem();
		if(!tdMem){
			_hcdContext.queueTdMemTask(tdMemTask());
			return;
			}
		_tdMem.put(tdMem);
		++_nTDs;
		}
	executeIRP();
	}

void	Driver::WriteDataTrans::cancelSof() noexcept{
	if(_sofFrameNumber == _hcdContext.getCurrentFrameNumber()){
		_hcdContext.queueSofTask(cancelSofTask());
		return;
		}
	// This is where we re-claim outstanding TDs from
	// the ED.
	using namespace Oscl::Usb::Ohci;
	TD::Descriptor*	next;
	while((next=_pendingTdList.get())){
		if(!_ed.remove(next)){
			if(_retryCancel){
				// FIXME: Use ErrorFatal
				// Where is that descriptor?
				while(true);
				}
			// FIXME: This is a hack!
			// Try one more time
			_retryCancel	= true;
			_pendingTdList.push(next);
			_hcdContext.queueSofTask(cancelSofTask());
			}
		next->getTransferApi().done(*next);
		}
	_context.dontSkip();
	if(_result){
		Oscl::Usb::Pipe::Status::Info	info;
		_result->query(info);
		}
	_irp.getPayload()._failed	= _result;
	_irp.returnToSender();
	if(_cancelReq){
		_cancelReq->returnToSender();
		}
	_context.free(this);
	}

void	Driver::WriteDataTrans::executeIRP() noexcept{
	void*				mem;
	TD::General::Setup*				setupTD;
	const Oscl::Usb::Setup::Packet*	setupPkt;
	TD::General::Out*				dataTD;
	TD::General::StatusIn*			statusTD;
	TD::Descriptor*					termTD;

	//
	setupPkt	= _irp.getPayload().getSetupBuffer();
	OsclCacheDataFlushRange((void *)setupPkt,sizeof(Oscl::Usb::Setup::Packet));

	//
	mem		= _ed.getTdQueueTailPointer();
	setupTD	= new(mem)	TD::General::Setup(	_otherTransfer,
											_hcdContext.getDmaBusApi(),
											TD::IrqDelay(	TD::Dword0::DI::
															Value_NoInterrupt
															),
											(void*)setupPkt
											);
	_pendingTdList.put(setupTD);
	//
	mem	= _tdMem.get();
	termTD	= new(mem) TD::Descriptor(	_otherTransfer,
										_hcdContext.getDmaBusApi()
										);
	--_nTDs;

	//
	const unsigned	maxPacketSize	= _context.getMaxPacketSize();
	unsigned		dataLength		= _dataLength;
	unsigned char*	buffer;
	buffer	= (unsigned char*)_irp.getPayload().getWriteDataBuffer();
	OsclCacheDataFlushRange((void *)buffer,_dataLength);
	for(;_nTDs>2;--_nTDs,buffer += maxPacketSize){
		mem	= _tdMem.get();
		dataTD	= new(mem)
			TD::General::Out(	_otherTransfer,
								_hcdContext.getDmaBusApi(),
								TD::IrqDelay(TD::Dword0::DI::Value_NoInterrupt),
								buffer,
								maxPacketSize
								);
		_pendingTdList.put(dataTD);
		_ed.put(dataTD);
		}

	// Last buffer
	mem	= _tdMem.get();
	dataLength	= dataLength%maxPacketSize;
	if(!dataLength){
		dataLength	= maxPacketSize;
		}
	dataTD	= new(mem)
		TD::General::Out(	_otherTransfer,
							_hcdContext.getDmaBusApi(),
							TD::IrqDelay(TD::Dword0::DI::Value_NoInterrupt),
							buffer,
							dataLength
							);
	_pendingTdList.put(dataTD);
	_ed.put(dataTD);

	mem	= _tdMem.get();
	statusTD	= new(mem)
		TD::General::StatusIn(	_lastTransfer,
								_hcdContext.getDmaBusApi()
								);
	_pendingTdList.put(statusTD);
	_ed.put(statusTD);
	_ed.put(termTD);
	OsclCpuInOrderMemoryAccessBarrier();
	_hcdContext.controlListFilled();
	}

void	Driver::WriteDataTrans::
lastTransferComplete(TD::Descriptor& desc) noexcept{
	using namespace Oscl::Usb::Ohci::TD;
	_pendingTdList.remove(&desc);
	if(!_canceling){
		Descriptor::CC	result	= desc.result();
		if(result != Descriptor::NoError){
			if(result == Descriptor::Stall){
				// Special case
				if(_ed.isHalted()){
					_ed.clearHalted();
					OsclCpuInOrderMemoryAccessBarrier();
					_hcdContext.controlListFilled();
					}
				_result	= 0;
				}
			else {
				_result	= parseResult(result);
				}
			}
		if(_result){
			Oscl::Usb::Pipe::Status::Info	info;
			_result->query(info);
			}
		_irp.getPayload()._failed	= _result;
		_irp.returnToSender();
		_context.free(this);
		}
	_hcdContext.free(desc);
	}

void	Driver::WriteDataTrans::
otherTransferComplete(TD::Descriptor& desc) noexcept{
	using namespace Oscl::Usb::Ohci::TD;
	_pendingTdList.remove(&desc);
	if(!_canceling){
		Descriptor::CC	result	= desc.result();
		if(result != Descriptor::NoError){
			_result	= parseResult(result);
			cancel();
			}
		}
	_hcdContext.free(desc);
	}

Oscl::Usb::Ohci::HCD::DeferredTaskApi&
Driver::WriteDataTrans::tdMemTask() noexcept{
	using namespace Oscl::Usb::Ohci::HCD;
	DeferredTask<WriteDataTrans>*	dt;
	dt	= new(&_dtMem)
				DeferredTask<WriteDataTrans> (	*this,
												&Driver::WriteDataTrans::
												tdAlloc
												);
	return *dt;
	}

Oscl::Usb::Ohci::HCD::DeferredTaskApi&
Driver::WriteDataTrans::cancelSofTask() noexcept{
	using namespace Oscl::Usb::Ohci::HCD;
	DeferredTask<WriteDataTrans>*	dt;
	dt	= new(&_dtMem)
				DeferredTask<WriteDataTrans> (	*this,
												&Driver::WriteDataTrans::
												cancelSof
												);
	return *dt;
	}

