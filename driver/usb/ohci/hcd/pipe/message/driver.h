/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_usb_ohci_hcd_pipe_message_driverh_
#define _oscl_drv_usb_ohci_hcd_pipe_message_driverh_
#include "oscl/driver/usb/pipe/message/reqapi.h"
#include "oscl/driver/usb/ohci/ed/message.h"
#include "oscl/driver/usb/ohci/hcd/server/hcdapi.h"
#include "oscl/queue/queueitem.h"
#include "oscl/mt/itc/srv/close.h"

/** */
namespace Oscl {
/** */
namespace Usb {
/** */
namespace Ohci {
/** */
namespace HCD {
/** */
namespace Pipe {
/** */
namespace Message {

class Driver :
	public Oscl::Usb::Pipe::Message::Req::Api,
	public Oscl::Mt::Itc::Srv::Close::Req::Api,
	public Oscl::QueueItem
	{
	private:
		/** */
		class TransApi : public QueueItem {
			public:
				/** Shut-up GCC. */
				virtual ~TransApi() {}
				/** */
				virtual bool	irpMatch(Oscl::Mt::Itc::SrvMsg& irp) noexcept=0;
				/** */
				virtual void	cancel(CancelReq& cancelReq) noexcept=0;
				/** */
				virtual void	cancel() noexcept=0;
			};

		/** */
		class NoDataTrans : public TransApi {
			private:
				/** This memory is used to perform exactly one
					deferred task at a time. The transaction
					must track the state of this memory to
					prevent problems.
				 */
				Oscl::Memory::AlignedBlock
					<sizeof(	Oscl::Usb::Ohci::HCD::
								DeferredTask<NoDataTrans>
								)
						>									_dtMem;
				/** */
				Oscl::Usb::Ohci::TD::Transfer<NoDataTrans>	_otherTransfer;
				/** */
				Oscl::Usb::Ohci::TD::Transfer<NoDataTrans>	_lastTransfer;
				/** */
				Driver&										_context;
				/** */
				Oscl::Usb::Ohci::HCD::HcdApi&				_hcdContext;
				/** */
				Oscl::Usb::Ohci::ED::Message&				_ed;
				/** */
				NoDataReq&									_irp;
				/** */
				CancelReq*									_cancelReq;
				/** */
				Oscl::Queue<TransactionDescMem>				_tdMem;
				/** */
				Oscl::Queue<	Oscl::Usb::Ohci::
								TD::Descriptor
								>							_pendingTdList;
				/** */
				unsigned									_nTDs;
				/** */
				bool										_canceling;
				/** */
				unsigned									_sofFrameNumber;
				/** */
				bool										_retryCancel;
				/** */
				enum{nRequiredTDs=1+1};
				/** */
				const Oscl::Usb::Pipe::Status::Result*	_result;

			public:
				/** */
				NoDataTrans(	Driver&							context,
								Oscl::Usb::Ohci::HCD::HcdApi&	hcdContext,
								Oscl::Usb::Ohci::ED::Message&	ed,
								NoDataReq&						irp
								) noexcept;

			private: // TransApi
				/** */
				bool	irpMatch(Oscl::Mt::Itc::SrvMsg& irp) noexcept;
				/** */
				void	cancel(CancelReq& cancelReq) noexcept;
				/** */
				void	cancel() noexcept;

			private:
				/** */
				friend class Oscl::Usb::Ohci::TD::Transfer<NoDataTrans>;
				/** */
				void	tdAlloc() noexcept;
				/** */
				void	cancelSof() noexcept;
				/** */
				void	executeIRP() noexcept;
				/** */
				void	lastTransferComplete(TD::Descriptor& desc) noexcept;
				/** */
				void	otherTransferComplete(TD::Descriptor& desc) noexcept;
				/** */
				Oscl::Usb::Ohci::HCD::DeferredTaskApi&	tdMemTask() noexcept;
				/** */
				Oscl::Usb::Ohci::HCD::
				DeferredTaskApi&		cancelSofTask() noexcept;
			};

		/** */
		class ReadDataTrans : public TransApi {
			private:
				/** This memory is used to perform exactly one
					deferred task at a time. The transaction
					must track the state of this memory to
					prevent problems.
				 */
				Oscl::Memory::AlignedBlock
					<sizeof(	Oscl::Usb::Ohci::HCD::
								DeferredTask<ReadDataTrans>
								)
						>									_dtMem;
				/** */
				Oscl::Usb::Ohci::TD::
				Transfer<ReadDataTrans>						_otherTransfer;
				/** */
				Oscl::Usb::Ohci::TD::
				Transfer<ReadDataTrans>						_lastTransfer;
				/** */
				Driver&										_context;
				/** */
				Oscl::Usb::Ohci::HCD::HcdApi&				_hcdContext;
				/** */
				Oscl::Usb::Ohci::ED::Message&				_ed;
				/** */
				ReadReq&									_irp;
				/** */
				CancelReq*									_cancelReq;
				/** */
				Oscl::Queue<TransactionDescMem>				_tdMem;
				/** */
				Oscl::Queue<	Oscl::Usb::Ohci::
								TD::Descriptor
								>							_pendingTdList;
				/** */
				unsigned									_nTDs;
				/** */
				unsigned									_nRequiredTDs;
				/** */
				unsigned									_dataLength;
				/** */
				bool										_canceling;
				/** */
				unsigned									_sofFrameNumber;
				/** */
				bool										_retryCancel;
				/** */
				const Oscl::Usb::Pipe::Status::Result*	_result;

			public:
				/** */
				ReadDataTrans(	Driver&							context,
								Oscl::Usb::Ohci::HCD::HcdApi&	hcdContext,
								Oscl::Usb::Ohci::ED::Message&	ed,
								ReadReq&						irp
								) noexcept;

			private: // TransApi
				/** */
				bool	irpMatch(Oscl::Mt::Itc::SrvMsg& irp) noexcept;
				/** */
				void	cancel(CancelReq& msg) noexcept;
				/** */
				void	cancel() noexcept;

			private:
				/** */
				friend class Oscl::Usb::Ohci::TD::Transfer<ReadDataTrans>;
				/** */
				void	tdAlloc() noexcept;
				/** */
				void	cancelSof() noexcept;
				/** */
				void	executeIRP() noexcept;
				/** */
				void	lastTransferComplete(TD::Descriptor& desc) noexcept;
				/** */
				void	otherTransferComplete(TD::Descriptor& desc) noexcept;
				/** */
				Oscl::Usb::Ohci::HCD::DeferredTaskApi&	tdMemTask() noexcept;
				/** */
				Oscl::Usb::Ohci::HCD::
				DeferredTaskApi&		cancelSofTask() noexcept;
			};

		/** */
		class WriteDataTrans : public TransApi {
			private:
				/** This memory is used to perform exactly one
					deferred task at a time. The transaction
					must track the state of this memory to
					prevent problems.
				 */
				Oscl::Memory::AlignedBlock
					<sizeof(	Oscl::Usb::Ohci::HCD::
								DeferredTask<WriteDataTrans>
								)
						>									_dtMem;
				/** */
				Oscl::Usb::Ohci::TD::
				Transfer<WriteDataTrans>					_otherTransfer;
				/** */
				Oscl::Usb::Ohci::TD::
				Transfer<WriteDataTrans>					_lastTransfer;
				/** */
				Driver&										_context;
				/** */
				Oscl::Usb::Ohci::HCD::HcdApi&				_hcdContext;
				/** */
				Oscl::Usb::Ohci::ED::Message&				_ed;
				/** */
				WriteReq&									_irp;
				/** */
				CancelReq*									_cancelReq;
				/** */
				Oscl::Queue<TransactionDescMem>				_tdMem;
				/** */
				Oscl::Queue<	Oscl::Usb::Ohci::
								TD::Descriptor
								>							_pendingTdList;
				/** */
				unsigned									_nTDs;
				/** */
				unsigned									_nRequiredTDs;
				/** */
				unsigned									_dataLength;
				/** */
				bool										_canceling;
				/** */
				unsigned									_sofFrameNumber;
				/** */
				bool										_retryCancel;
				/** */
				const Oscl::Usb::Pipe::Status::Result*	_result;

			public:
				/** */
				WriteDataTrans(	Driver&							context,
								Oscl::Usb::Ohci::HCD::HcdApi&	hcdContext,
								Oscl::Usb::Ohci::ED::Message&	ed,
								WriteReq&						irp
								) noexcept;

			private: // TransApi
				/** */
				bool	irpMatch(Oscl::Mt::Itc::SrvMsg& irp) noexcept;
				/** */
				void	cancel(CancelReq& msg) noexcept;
				/** */
				void	cancel() noexcept;

			private:
				/** */
				friend class Oscl::Usb::Ohci::TD::Transfer<WriteDataTrans>;
				/** */
				void	tdAlloc() noexcept;
				/** */
				void	cancelSof() noexcept;
				/** */
				void	executeIRP() noexcept;
				/** */
				void	lastTransferComplete(TD::Descriptor& desc) noexcept;
				/** */
				void	otherTransferComplete(TD::Descriptor& desc) noexcept;
				/** */
				Oscl::Usb::Ohci::HCD::DeferredTaskApi&	tdMemTask() noexcept;
				/** */
				Oscl::Usb::Ohci::HCD::
				DeferredTaskApi&		cancelSofTask() noexcept;
			};
		/** */
		typedef union TransMem {
			/** */
            void*	__qitemlink;
			/** */
			Oscl::Memory::AlignedBlock<sizeof(ReadDataTrans)>	_read;
			/** */
			Oscl::Memory::AlignedBlock<sizeof(WriteDataTrans)>	_write;
			/** */
			Oscl::Memory::AlignedBlock<sizeof(NoDataTrans)>		_none;
			} TransMem;

	private:
		/** */
		Oscl::Mt::Itc::Srv::CloseSync				_ocSync;

		/** */
		enum{maxTransfers = 2};

		/** */
		TransMem									_transMem[maxTransfers];

		/** */
		Oscl::Queue<TransMem>						_freeTransMem;

		/** */
		Oscl::Queue<TransApi>						_pendingTransactions;

		/** */
		Oscl::Usb::Pipe::
		Message::Req::Api::ConcreteSAP				_sap;

		/** */
		Oscl::Usb::Ohci::ED::Message&				_ed;

		/** */
		Oscl::Queue<Oscl::Mt::Itc::SrvMsg>			_deferredQ;
		/** */
		Oscl::Usb::Ohci::HCD::HcdApi&			_hcdContext;
		/** */
		unsigned									_skipCount;
		/** */
		const unsigned								_maxPacketSize;
		/** */
		Oscl::Mt::Itc::Srv::
		Close::Req::Api::CloseReq*					_closeReq;
		/** */
		unsigned									_nFreeTransMem;
		/** */
		bool										_opened;

	public:
		/** */
		Driver(	Oscl::Mt::Itc::PostMsgApi&			myPapi,
				Oscl::Usb::Ohci::ED::Message&		ed,
				Oscl::Usb::Ohci::HCD::HcdApi&		hcdContext
				) noexcept;

		/** */
		virtual ~Driver() {}

		/** */
		Oscl::Usb::Pipe::Message::
		Req::Api::SAP&					getSAP() noexcept;

		/** */
		Oscl::Mt::Itc::Srv::
		Close::Req::Api::SAP&			getCloseSAP() noexcept;

		/** */
		Oscl::Mt::Itc::Srv::
		OpenCloseSyncApi&				getOpenCloseSyncApi() noexcept;

		/** */
		Oscl::Usb::Ohci::ED::Message&	getED() noexcept;

		/** */
		unsigned	getMaxPacketSize() const noexcept;

		/** */
		void		cancelAllPendingTransactions() noexcept;

	private: // Transfer
		/** */
		friend class NoDataTrans;
		/** */
		friend class ReadDataTrans;
		/** */
		friend class WriteDataTrans;
		/** */
		void	free(NoDataTrans* xfer) noexcept;
		/** */
		void	free(ReadDataTrans* xfer) noexcept;
		/** */
		void	free(WriteDataTrans* xfer) noexcept;
		/** */
		void	free(TransMem* mem) noexcept;
		/** */
		TransMem*	allocTransMem() noexcept;
		/** */
		TransApi*	remove(TransApi* trans) noexcept;
		/** */
		void	pending(TransApi* trans) noexcept;
		/** */
		void	skip() noexcept;
		/** */
		void	dontSkip() noexcept;

	private:	// Oscl::Usb::Pipe::Message::Req::Api
		/** */
		void	request(CancelReq& msg) noexcept;
		/** */
		void	request(NoDataReq& msg) noexcept;
		/** */
		void	request(ReadReq& msg) noexcept;
		/** */
		void	request(WriteReq& msg) noexcept;

	private:	// Oscl::Mt::Itc::Srv::Close::Req::Api
		/** */
		void	request(OpenReq& msg) noexcept;
		/** */
		void	request(CloseReq& msg) noexcept;
	};

}
}
}
}
}
}

#endif
