/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "msgpipe.h"

using namespace Oscl::Usb::Ohci::HCD;

MessagePipe::MessagePipe(	Oscl::Mt::Itc::PostMsgApi&			myPapi,
							Oscl::Usb::Ohci::ED::Message&		ed,
							Oscl::Usb::Ohci::HCD::HcdApi&		hcdContext
							) noexcept:
		_driver(myPapi,ed,hcdContext),
		_sync(_driver.getSAP())
		{
	}

Oscl::Mt::Itc::Srv::
Close::Req::Api::SAP&		MessagePipe::getCloseSAP() noexcept{
	return _driver.getCloseSAP();
	}

Oscl::Mt::Itc::Srv::
OpenCloseSyncApi&			MessagePipe::getOpenCloseSyncApi() noexcept{
	return _driver.getOpenCloseSyncApi();
	}

Oscl::Usb::Pipe::Message::Req::Api::SAP&	MessagePipe::getSAP() noexcept{
	return _driver.getSAP();
	}

Oscl::Usb::Pipe::Message::SyncApi&		MessagePipe::getSyncApi() noexcept{
	return _sync;
	}

Oscl::Usb::Ohci::ED::Message&			MessagePipe::getED() noexcept{
	return _driver.getED();
	}

/////////// MessagePipeRec /////////

MessagePipeRec::MessagePipeRec(	Oscl::Mt::Itc::PostMsgApi&			myPapi,
								Oscl::Usb::Ohci::ED::Message&		ed,
								Oscl::Usb::Ohci::HCD::HcdApi&		hcdContext
								) noexcept:
		_pipe(myPapi,ed,hcdContext)
		{
	}

