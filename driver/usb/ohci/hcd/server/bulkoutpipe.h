/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_usb_ohci_hcd_server_bulkoutpipeh_
#define _oscl_drv_usb_ohci_hcd_server_bulkoutpipeh_
#include "oscl/driver/usb/pipe/bulk/out/pipeapi.h"
#include "oscl/driver/usb/ohci/hcd/pipe/bulk/out/driver.h"

/** */
namespace Oscl {
/** */
namespace Usb {
/** */
namespace Ohci {
/** */
namespace HCD {

/** */
class BulkOutPipe : public Oscl::Usb::Pipe::Bulk::OUT::PipeApi {
	private:
		/** */
		Oscl::Usb::Ohci::HCD::Pipe::Bulk::OUT::Driver	_driver;
	public:
		/** */
		BulkOutPipe(	Oscl::Mt::Itc::PostMsgApi&			myPapi,
						Oscl::Usb::Ohci::ED::GeneralOut&		ed,
						Oscl::Usb::Ohci::HCD::HcdApi&		hcdContext
						) noexcept;
		/** */
		Oscl::Usb::Ohci::ED::GeneralOut&			getED() noexcept;

	public:	// Oscl::Usb::Pipe::Bulk::OUT::PipeApi
        /** */
		Oscl::Mt::Itc::Srv::
		Close::Req::Api::SAP&			getCloseSAP() noexcept;
        /** */
		Oscl::Mt::Itc::Srv::
		OpenCloseSyncApi&				getOpenCloseSyncApi() noexcept;
		/** */
		Oscl::Usb::Pipe::Bulk::OUT::
		Req::Api::SAP&					getSAP() noexcept;
	};

/** */
class BulkOutPipeRec : public Oscl::QueueItem {
	public:
		/** */
		BulkOutPipe		_pipe;
	public:
		/** */
		BulkOutPipeRec(	Oscl::Mt::Itc::PostMsgApi&			myPapi,
							Oscl::Usb::Ohci::ED::GeneralOut&		ed,
							Oscl::Usb::Ohci::HCD::HcdApi&		hcdContext
							) noexcept;
	};

}
}
}
}

#endif
