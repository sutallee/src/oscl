/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_usb_ohci_hcd_server_epapih_
#define _oscl_drv_usb_ohci_hcd_server_epapih_
#include "oscl/queue/queue.h"
#include "defer.h"
#include "tdmem.h"
#include "oscl/bus/api.h"

/** */
namespace Oscl {
/** */
namespace Usb {
/** */
namespace Ohci {
/** */
namespace HCD {

/** This interface describes the operations availabe to
	endpoint services to the core HCD. The operations
	provided by this interface are serialized with all
	other HCD operations.
 */
class HcdApi {
	public:
		/** Shut-up GCC. */
		virtual ~HcdApi() {}
		/** */
		virtual TransactionDescMem*	allocTdMem() noexcept=0;
		/** */
		virtual void		free(TransactionDescMem*) noexcept=0;
		/** */
		virtual void		free(Oscl::Usb::Ohci::TD::Descriptor& d) noexcept=0;
		/** */
		virtual void		queueSofTask(DeferredTaskApi& dt) noexcept=0;
		/** */
		virtual void		cancelSofTask(DeferredTaskApi& dt) noexcept=0;
		/** */
		virtual void		queueTdMemTask(DeferredTaskApi& dt) noexcept=0;
		/** */
		virtual void		cancelTdMemTask(DeferredTaskApi& dt) noexcept=0;
		/** */
		virtual void		controlListFilled() noexcept=0;
		/** */
		virtual void		bulkListFilled() noexcept=0;
		/** */
		virtual unsigned	getCurrentFrameNumber() noexcept=0;
		/** */
		virtual Oscl::Bus::Api&	getDmaBusApi() noexcept=0;
	};

}
}
}
}


#endif
