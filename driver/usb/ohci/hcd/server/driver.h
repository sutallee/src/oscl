/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_usb_ohci_hcd_server_driverh_
#define _oscl_drv_usb_ohci_hcd_server_driverh_
#include "oscl/driver/usb/ohci/hcd/rhreqapi.h"
#include "oscl/interrupt/shandler.h"
#include "oscl/interrupt/isrdsr.h"
#include "oscl/mt/itc/mbox/srvapi.h"
#include "oscl/mt/itc/srv/close.h"
#include "oscl/mt/itc/srv/closeapi.h"
#include "oscl/hw/usb/ohci/reg.h"
#include "oscl/hw/usb/ohci/hcca.h"
#include "oscl/driver/usb/ohci/reg/control.h"
#include "oscl/driver/usb/ohci/reg/cmdstat.h"
#include "oscl/driver/usb/ohci/reg/irqstat.h"
#include "oscl/driver/usb/ohci/reg/irqenable.h"
#include "oscl/driver/usb/ohci/reg/irqdisable.h"
#include "oscl/queue/queue.h"
#include "oscl/bus/api.h"
#include "oscl/driver/usb/ohci/td/general/desc.h"
#include "oscl/driver/usb/ohci/td/isoch/desc.h"
#include "oscl/driver/usb/alloc/addr/reqapi.h"
#include "oscl/driver/usb/ohci/ed/message.h"
#include "oscl/memory/block.h"
#include "oscl/driver/usb/ohci/ed/in.h"
#include "oscl/driver/usb/ohci/ed/out.h"
#include "oscl/driver/usb/ohci/ed/isoin.h"
#include "oscl/driver/usb/ohci/ed/isoout.h"
#include "oscl/driver/usb/ohci/ed/message.h"
#include "oscl/driver/usb/alloc/pipe/message/reqapi.h"
#include "oscl/driver/usb/alloc/pipe/interrupt/in/reqapi.h"
#include "oscl/driver/usb/alloc/pipe/bulk/in/reqapi.h"
#include "oscl/driver/usb/alloc/pipe/bulk/out/reqapi.h"
#include "oscl/driver/usb/alloc/bw/reqapi.h"
#include "oscl/driver/usb/alloc/bw/interrupt/reqapi.h"
#include "oscl/driver/usb/hcd/enum/monitor/var.h"
#include "oscl/driver/usb/hcd/enum/dcp/var.h"
#include "oscl/driver/usb/ohci/bw/config.h"
#include "tdmem.h"
#include "edmem.h"
#include "hcdapi.h"
#include "defer.h"
#include "msgpipe.h"
#include "iinpipe.h"
#include "bulkinpipe.h"
#include "bulkoutpipe.h"

/** */
namespace Oscl {
/** */
namespace Usb {
/** */
namespace Ohci {
/** */
namespace HCD {

/** */
class Driver :	public Oscl::Interrupt::StatusHandlerApi,
				public Oscl::Interrupt::IsrDsrApi,
				public Oscl::Mt::Itc::Srv::CloseSync,
				public Oscl::Mt::Itc::Srv::Close::Req::Api,
				public Oscl::Usb::Alloc::Addr::Req::Api,
				public Oscl::Usb::Alloc::Pipe::Message::Req::Api,
				public Oscl::Usb::Alloc::Pipe::Interrupt::IN::Req::Api,
				public Oscl::Usb::Alloc::Pipe::Bulk::IN::Req::Api,
				public Oscl::Usb::Alloc::Pipe::Bulk::OUT::Req::Api,
				public Oscl::Usb::Alloc::BW::Req::Api,
				public Oscl::Usb::Alloc::BW::Interrupt::Req::Api,
				public Oscl::Mt::Itc::ServerApi::SignaledApi,
				public RootHub::Req::Api,
				public HcdApi
				{
	private:
		/** */
		class ControlListAttachDefTask {
			private:
				/** */
				Oscl::Memory::AlignedBlock
					<sizeof(DeferredTask<ControlListAttachDefTask>)>	_dtMem;
				/** */
				Driver&							_context;
				/** */
				Oscl::Usb::Alloc::Pipe::
				Message::Req::Api::NewReq&		_msg;
				/** */
				MessagePipeRec&					_pipe;
				/** */
				unsigned						_currentFrameNumber;
			public:
				/** */
				ControlListAttachDefTask(	Driver&				context,
											Oscl::Usb::Alloc::
											Pipe::Message::
											Req::Api::NewReq&	msg,
											MessagePipeRec&		pipe
											) noexcept;
				/** */
				void	queueSofTask() noexcept;
				/** */
				void	sofDone() noexcept;
			};
		/** */
		class ControlListDetachDefTask {
			private:
				/** */
				Oscl::Memory::AlignedBlock
					<sizeof(DeferredTask<ControlListDetachDefTask>)>	_dtMem;
				/** */
				Driver&							_context;
				/** */
				Oscl::Usb::Alloc::Pipe::
				Message::Req::Api::FreeReq&		_msg;
				/** */
				unsigned						_currentFrameNumber;
			public:
				/** */
				ControlListDetachDefTask(	Driver&				context,
											Oscl::Usb::Alloc::
											Pipe::Message::
											Req::Api::FreeReq&	msg
											) noexcept;
				/** */
				void	queueSofTask() noexcept;
				/** */
				void	sofDone() noexcept;
			};

		/** */
		class BulkInListAttachDefTask {
			private:
				/** */
				Oscl::Memory::AlignedBlock
					<sizeof(DeferredTask<BulkInListAttachDefTask>)>	_dtMem;
				/** */
				Driver&							_context;
				/** */
				Oscl::Usb::Alloc::Pipe::
				Bulk::IN::Req::Api::NewReq&		_msg;
				/** */
				BulkInPipeRec&					_pipe;
				/** */
				unsigned						_currentFrameNumber;
			public:
				/** */
				BulkInListAttachDefTask(	Driver&				context,
											Oscl::Usb::Alloc::
											Pipe::Bulk::IN::
											Req::Api::NewReq&	msg,
											BulkInPipeRec&		pipe
											) noexcept;
				/** */
				void	queueSofTask() noexcept;
				/** */
				void	sofDone() noexcept;
			};
		/** */
		class BulkInListDetachDefTask {
			private:
				/** */
				Oscl::Memory::AlignedBlock
					<sizeof(DeferredTask<BulkInListDetachDefTask>)>	_dtMem;
				/** */
				Driver&							_context;
				/** */
				Oscl::Usb::Alloc::Pipe::
				Bulk::IN::Req::Api::FreeReq&	_msg;
				/** */
				unsigned						_currentFrameNumber;
			public:
				/** */
				BulkInListDetachDefTask(	Driver&				context,
											Oscl::Usb::Alloc::
											Pipe::Bulk::IN::
											Req::Api::FreeReq&	msg
											) noexcept;
				/** */
				void	queueSofTask() noexcept;
				/** */
				void	sofDone() noexcept;
			};

		/** */
		class BulkOutListAttachDefTask {
			private:
				/** */
				Oscl::Memory::AlignedBlock
					<sizeof(DeferredTask<BulkOutListAttachDefTask>)>	_dtMem;
				/** */
				Driver&							_context;
				/** */
				Oscl::Usb::Alloc::Pipe::
				Bulk::OUT::Req::Api::NewReq&	_msg;
				/** */
				BulkOutPipeRec&					_pipe;
				/** */
				unsigned						_currentFrameNumber;
			public:
				/** */
				BulkOutListAttachDefTask(	Driver&				context,
											Oscl::Usb::Alloc::
											Pipe::Bulk::OUT::
											Req::Api::NewReq&	msg,
											BulkOutPipeRec&		pipe
											) noexcept;
				/** */
				void	queueSofTask() noexcept;
				/** */
				void	sofDone() noexcept;
			};
		/** */
		class BulkOutListDetachDefTask {
			private:
				/** */
				Oscl::Memory::AlignedBlock
					<sizeof(DeferredTask<BulkOutListDetachDefTask>)>	_dtMem;
				/** */
				Driver&							_context;
				/** */
				Oscl::Usb::Alloc::Pipe::
				Bulk::OUT::Req::Api::FreeReq&	_msg;
				/** */
				unsigned						_currentFrameNumber;
			public:
				/** */
				BulkOutListDetachDefTask(	Driver&				context,
											Oscl::Usb::Alloc::
											Pipe::Bulk::OUT::
											Req::Api::FreeReq&	msg
											) noexcept;
				/** */
				void	queueSofTask() noexcept;
				/** */
				void	sofDone() noexcept;
			};

		/** */
		class InterruptListAttachDefTask {
			private:
				/** */
				Oscl::Memory::AlignedBlock
					<sizeof(DeferredTask<InterruptListAttachDefTask>)>	_dtMem;
				/** */
				Driver&							_context;
				/** */
				Oscl::Usb::Alloc::BW::
				Interrupt::Req::Api::
				ReserveReq&						_msg;
				/** */
				Oscl::Usb::Ohci::
				BW::Allocator&					_allocator;
				/** */
				unsigned						_currentFrameNumber;
			public:
				/** */
				InterruptListAttachDefTask(	Driver&				context,
											Oscl::Usb::Alloc::
											BW::Interrupt::
											Req::Api::
											ReserveReq&			msg,
											Oscl::Usb::Ohci::
											BW::Allocator&		allocator
											) noexcept;
				/** */
				void	queueSofTask() noexcept;
				/** */
				void	sofDone() noexcept;
			};
		/** */
		class InterruptListDetachDefTask {
			private:
				/** */
				Oscl::Memory::AlignedBlock
					<sizeof(DeferredTask<InterruptListDetachDefTask>)>	_dtMem;
				/** */
				Driver&							_context;
				/** */
				Oscl::Usb::Alloc::
				BW::Interrupt::
				Req::Api::FreeReq&				_msg;
				/** */
				Oscl::Usb::Ohci::
				BW::Allocator&					_allocator;
				/** */
				unsigned						_currentFrameNumber;
			public:
				/** */
				InterruptListDetachDefTask(	Driver&				context,
											Oscl::Usb::Alloc::
											BW::Interrupt::
											Req::Api::
											FreeReq&			msg,
											Oscl::Usb::Ohci::
											BW::Allocator&		allocator
											) noexcept;
				/** */
				void	queueSofTask() noexcept;
				/** */
				void	sofDone() noexcept;
			};

		/** */
		union DeferredTaskMem {
			/** */
			void*	__qitemlink;
			/** */
			Oscl::Memory::AlignedBlock
				<sizeof(ControlListAttachDefTask)>	_cladt;
			/** */
			Oscl::Memory::AlignedBlock
				<sizeof(ControlListDetachDefTask)>	_clddt;
			/** */
			Oscl::Memory::AlignedBlock
				<sizeof(InterruptListAttachDefTask)>	_iladt;
			/** */
			Oscl::Memory::AlignedBlock
				<sizeof(InterruptListDetachDefTask)>	_ilddt;
			};
		/** */
		typedef Oscl::Usb::Ohci::TD::Transfer<Driver>	Transfer;

	public:
		/** */
		union PipeMem {
			/** */
			void*	__qitemlink;
			/** */
			Oscl::Memory::AlignedBlock<sizeof(MessagePipeRec)>		_message;
			/** */
			Oscl::Memory::AlignedBlock<sizeof(InterruptInPipeRec)>	_iin;
			/** */
			Oscl::Memory::AlignedBlock<sizeof(BulkInPipeRec)>		_bulkin;
			/** */
			Oscl::Memory::AlignedBlock<sizeof(BulkOutPipeRec)>		_bulkout;
			};

	private:
		/** */
		enum{maxUsbDeviceAddresses=128};
	private:
		/** */
		Oscl::Usb::HCD::Enum::Monitor::Var			_enumMonitorVar;
		/** */
		Oscl::Memory::
		AlignedBlock<	sizeof(	Oscl::Usb::
								HCD::Enum::
								DefCntrlPipe::Var
								)
						>							_enumDcpVarMem;
		/** */
		Oscl::Usb::Ohci::BW::Config<6>				_bwAlloc;
		/** */
		Oscl::Usb::HCD::Enum::DefCntrlPipe::Var*	_enumDcpVar;
		/** */
		bool						_allocatedUsbAddr[maxUsbDeviceAddresses];
		/** */
		Oscl::Queue<PipeMem>			_freePipeMem;
		/** */
		Oscl::Queue<	Oscl::Usb::Alloc::
						Addr::Req::Api::
						UsbAddrResChangeReq
						>					_usbAddrChangeList;
		/** */
		Oscl::Queue<DeferredTaskMem>		_freeDtMem;
		/** */
		Oscl::Queue<Oscl::Mt::Itc::SrvMsg>	_pendingDeferredTaskReqList;
		/** */
		enum{nDeferredTasks=2};
		/** */
		DeferredTaskMem						_dtMem[nDeferredTasks];
		/** */
		Oscl::Usb::Ohci::Map&				_reg;
		/** */
		Register::Control					_control;
		/** */
		Register::CmdStatus					_cmdStatus;
		/** */
		Register::InterruptStatus			_irqStatus;
		/** */
		Register::InterruptEnable			_irqEnable;
		/** */
		Register::InterruptDisable			_irqDisable;
		/** */
		Oscl::Usb::Ohci::Hcca&				_hcca;
		/** */
		Oscl::Bus::Api&						_dmaBus;
		/** */
		Oscl::Mt::Itc::ServerApi&			_server;
		/** */
		bool								_open;
		/** */
		RootHub::Req::Api::OpenReq*			_rhOpenReq;
		/** */
		RootHub::Req::Api::CloseReq*		_rhCloseReq;
		/** */
		RootHub::Req::Api::ChangeReq*		_rhChangeReq;
		/** */
		RootHub::Req::Api::ConcreteSAP		_rhSAP;
		/** */
		Oscl::Usb::Alloc::Addr::
		Req::Api::ConcreteSAP				_hubPortSAP;
		/** */
		Oscl::Usb::Alloc::Pipe::
		Message::Req::Api::ConcreteSAP		_msgPipeAllocSAP;
		/** */
		Oscl::Usb::Alloc::Pipe::
		Interrupt::IN::Req::Api::
		ConcreteSAP							_intInPipeAllocSAP;
		/** */
		Oscl::Usb::Alloc::Pipe::
		Bulk::IN::Req::Api::
		ConcreteSAP							_bulkInPipeAllocSAP;
		/** */
		Oscl::Usb::Alloc::Pipe::
		Bulk::OUT::Req::Api::
		ConcreteSAP							_bulkOutPipeAllocSAP;
		/** */
		Oscl::Queue<	Oscl::Usb::Alloc::
						Addr::
						Req::Api::
						LockAddressZeroReq
						>					_addressZeroLockPendingQ;
		/** */
		bool								_addressZeroLocked;

		/** Always contains high and low speed EP descriptors for
			default address.
		 */
		Oscl::Usb::Ohci::ED::Message*		_controlEpListHead;
		/** */
		Oscl::Usb::Ohci::ED::Descriptor*	_bulkEpListHead;
		/** */
		MessagePipeRec*						_lsDefaultPipe;
		/** */
		MessagePipeRec*						_hsDefaultPipe;
		/** */
		Oscl::Queue<TransactionDescMem>		_freeTDList;
		/** */
		Oscl::Queue<EndpointDescMem>		_freeEDList;
		/** */
		Oscl::Queue<DeferredTaskApi>		_sofDeferredTaskList;
		/** */
		Oscl::Queue<DeferredTaskApi>		_tdMemDeferredTaskList;
		/** */
		Transfer							_unexpectedTransfer;
		/** */
		Oscl::Queue<MessagePipeRec>			_activeMessageEpList;
		/** */
		Oscl::Queue<InterruptInPipeRec>		_activeInterruptInPipeList;
		/** */
		Oscl::Queue<BulkInPipeRec>			_activeBulkInPipeList;
		/** */
		Oscl::Queue<BulkOutPipeRec>			_activeBulkOutPipeList;
		/** */
		Oscl::Queue<	Oscl::Usb::Alloc::
						Pipe::Message::Req::
						Api::NotifyChangeReq
						>					_msgPipeResChangeList;
		/** */
		Oscl::Queue<	Oscl::Usb::Alloc::
						Pipe::Interrupt::IN::
						Req::Api::
						NotifyChangeReq
						>					_intInPipeResChangeList;
		/** */
		Oscl::Queue<	Oscl::Usb::Alloc::
						Pipe::Bulk::IN::
						Req::Api::
						NotifyChangeReq
						>					_bulkInPipeResChangeList;
		/** */
		Oscl::Queue<	Oscl::Usb::Alloc::
						Pipe::Bulk::OUT::
						Req::Api::
						NotifyChangeReq
						>					_bulkOutPipeResChangeList;
		/** */
		unsigned long						_controlListDisableCount;
		/** */
		unsigned long						_periodicListDisableCount;
		/** */
		unsigned long						_bulkListDisableCount;
		/** _pipeMem[maxEndpoints] */
		PipeMem*							_pipeMem;

	public:
		/** */
		Driver(	Oscl::Usb::Ohci::Map&			reg,
				Oscl::Usb::Ohci::Hcca&			hcca,
				TransactionDescMem*				tdArray,
				unsigned						tdArraySize,
				EndpointDescMem*				edArray,
				unsigned						edArraySize,
				Oscl::Bus::Api&					dmaBus,
				Oscl::Mt::Itc::ServerApi&		server,
				PipeMem*						pipeMem,
				unsigned						maxEndpoints
				) noexcept;

		/** */
		~Driver();
		/** */
		RootHub::Req::Api::SAP&			getRootHubSAP() noexcept;
		/** */
		Oscl::Usb::Pipe::Message::
		Req::Api::SAP&					getLsEpSAP() noexcept;
		/** */
		Oscl::Usb::Pipe::Message::
		Req::Api::SAP&					getHsEpSAP() noexcept;
		/** */
		Oscl::Usb::Pipe::Message::
		SyncApi&						getLsSyncApi() noexcept;
		/** */
		Oscl::Usb::Pipe::Message::
		SyncApi&						getHsSyncApi() noexcept;
		/** */
		Oscl::Usb::Alloc::Addr::
		Req::Api::SAP&					getHubPortSAP() noexcept;
		/** */
		Oscl::Usb::Alloc::Pipe::
		Message::Req::Api::SAP&			getMsgPipeAllocSAP() noexcept;
		/** */
		Oscl::Usb::Alloc::Pipe::
		Interrupt::IN::Req::Api::SAP&	getInterruptInPipeAllocSAP() noexcept;
		/** */
		Oscl::Usb::Alloc::Pipe::
		Bulk::IN::Req::Api::SAP&		getBulkInPipeAllocSAP() noexcept;
		/** */
		Oscl::Usb::Alloc::Pipe::
		Bulk::OUT::Req::Api::SAP&		getBulkOutPipeAllocSAP() noexcept;

		/** */
		Oscl::Usb::HCD::Enum::
		DefCntrlPipe::Api&				getEnumDefCntrlPipeApi() noexcept;

		/** */
		Oscl::Usb::HCD::Enum::
		Monitor::Api&					getEnumMonitorApi() noexcept;
	private:
		/** Return address zero if allocation fails */
		Oscl::Usb::Address	allocateAddress() noexcept;
		/** */
		void	freeAddress(Oscl::Usb::Address address) noexcept;
		/** */
		bool	usbAddressAvailable() noexcept;

	private:
		friend class ControlListAttachDefTask;
		friend class ControlListDetachDefTask;
		friend class InterruptListAttachDefTask;
		friend class InterruptListDetachDefTask;
		friend class BulkInListAttachDefTask;
		friend class BulkInListDetachDefTask;
		friend class BulkOutListAttachDefTask;
		friend class BulkOutListDetachDefTask;
		/** */
		void	insertToControlList(Oscl::Usb::Ohci::ED::Message& ed) noexcept;
		/** */
		void	removeFromControlList(Oscl::Usb::Ohci::ED::Message& ed) noexcept;
		/** */
		void	insertToBulkList(Oscl::Usb::Ohci::ED::Descriptor& ed) noexcept;
		/** */
		void	removeFromBulkList(Oscl::Usb::Ohci::ED::Descriptor& ed) noexcept;
		/** */
		DeferredTaskMem*	allocDeferredTaskMem() noexcept;
		/** */
		void				free(DeferredTaskMem* mem) noexcept;
		/** */
		void				release(ControlListAttachDefTask& dt) noexcept;
		/** */
		void				release(ControlListDetachDefTask& dt) noexcept;
		/** */
		void				release(InterruptListAttachDefTask& dt) noexcept;
		/** */
		void				release(InterruptListDetachDefTask& dt) noexcept;
		/** */
		void				release(BulkInListAttachDefTask& dt) noexcept;
		/** */
		void				release(BulkInListDetachDefTask& dt) noexcept;
		/** */
		void				release(BulkOutListAttachDefTask& dt) noexcept;
		/** */
		void				release(BulkOutListDetachDefTask& dt) noexcept;
		/** */
		MessagePipeRec*		removeActiveMessageEp(	Oscl::Usb::Pipe::
													Message::PipeApi&	pipe
													) noexcept;
		/** */
		InterruptInPipeRec*
							removeActiveInterruptInPipe(	Oscl::Usb::Pipe::
															Interrupt::IN::
															PipeApi&		pipe
															) noexcept;
		/** */
		BulkInPipeRec*
							removeActiveBulkInPipe(	Oscl::Usb::Pipe::
													Bulk::IN::
													PipeApi&		pipe
													) noexcept;
		/** */
		BulkOutPipeRec*
							removeActiveBulkOutPipe(	Oscl::Usb::Pipe::
														Bulk::OUT::
														PipeApi&		pipe
														) noexcept;
		/** */
		void				disableControlList() noexcept;
		/** */
		void				enableControlList() noexcept;
		/** */
		void				disablePeriodicList() noexcept;
		/** */
		void				enablePeriodicList() noexcept;
		/** */
		void				disableBulkList() noexcept;
		/** */
		void				enableBulkList() noexcept;

	private:
		/** */
		void	scheduleOverrun() noexcept;
		/** */
		void	writebackDoneHead(uint32_t doneHead) noexcept;
		/** */
		void	startOfFrame() noexcept;
		/** */
		void	resumeDetected() noexcept;
		/** */
		void	unrecoverableError() noexcept;
		/** */
		void	frameNumberOverflow() noexcept;
		/** */
		void	rootHubStatusChange() noexcept;
		/** */
		void	ownershipChange() noexcept;

	private:
		/** */
		MessagePipeRec*
				allocMessagePipe(	Address		usbAddress,
									EndpointID	endpointID,
									unsigned	maxPacketSize,
									bool		lowSpeedDevice
									) noexcept;
		/** */
		InterruptInPipeRec*
				allocInterruptInPipe(	ED::Descriptor&	ed,
										Address			usbAddress,
										EndpointID		endpointID,
										unsigned		maxPacketSize,
										bool			lowSpeedDevice
										) noexcept;
		/** */
		BulkInPipeRec*
				allocBulkInPipe(	Address			usbAddress,
									EndpointID		endpointID,
									unsigned		maxPacketSize,
									bool			lowSpeedDevice
									) noexcept;
		/** */
		BulkOutPipeRec*
				allocBulkOutPipe(	Address			usbAddress,
									EndpointID		endpointID,
									unsigned		maxPacketSize,
									bool			lowSpeedDevice
									) noexcept;
		/** */
		void	free(MessagePipeRec& pipe) noexcept;
		/** */
		void	free(InterruptInPipeRec& pipe) noexcept;
		/** */
		void	free(BulkInPipeRec& pipe) noexcept;
		/** */
		void	free(BulkOutPipeRec& pipe) noexcept;
		/** */
		void	notifyPipeResourceAvailable() noexcept;

	public: // Oscl::Mt::Itc::Srv::Close::Req::Api
		/** */
		void	request(	Oscl::Mt::Itc::Srv::
							Open::Req::Api::OpenReq&	msg
							) noexcept;
		/** */
		void	request(	Oscl::Mt::Itc::Srv::
							Close::Req::Api::CloseReq&	msg
							) noexcept;

	public: // Oscl::Usb::Alloc::Addr::Req::Api
		/** */
		void	request(	Oscl::Usb::Alloc::Addr::
							Req::Api::LockAddressZeroReq&		msg
							) noexcept;
		/** */
		void	request(	Oscl::Usb::Alloc::Addr::
							Req::Api::UnlockAddressZeroReq&	msg
							) noexcept;
		/** */
		void	request(	Oscl::Usb::Alloc::Addr::
							Req::Api::CancelAddrZeroLockReq&	msg
							) noexcept;
		/** */
		void	request(	Oscl::Usb::Alloc::Addr::
							Req::Api::AllocateNewAddressReq&	msg
							) noexcept;
		/** */
		void	request(	Oscl::Usb::Alloc::Addr::
							Req::Api::FreeAddressReq&			msg
							) noexcept;
		/** */
		void	request(	Oscl::Usb::Alloc::Addr::
							Req::Api::UsbAddrResChangeReq&		msg
							) noexcept;
		/** */
		void	request(	Oscl::Usb::Alloc::Addr::
							Req::Api::CancelUsbAddrResChangeReq&	msg
							) noexcept;

	public:	//Oscl::Usb::Alloc::Pipe::Message::Req::Api
		/** */
		void	request(	Oscl::Usb::Alloc::Pipe::
							Message::Req::Api::NewReq&	msg
							) noexcept;
		/** */
		void	request(	Oscl::Usb::Alloc::Pipe::
							Message::Req::Api::FreeReq&	msg
							) noexcept;
		/** */
		void	request(	Oscl::Usb::Alloc::Pipe::
							Message::Req::Api::NotifyChangeReq&	msg
							) noexcept;
		/** */
		void	request(	Oscl::Usb::Alloc::Pipe::
							Message::Req::Api::CancelChangeReq&	msg
							) noexcept;

	public:	//Oscl::Usb::Alloc::Pipe::Interrupt::IN::Req::Api
		/** */
		void	request(	Oscl::Usb::Alloc::Pipe::
							Interrupt::IN::Req::Api::NewReq&	msg
							) noexcept;
		/** */
		void	request(	Oscl::Usb::Alloc::Pipe::
							Interrupt::IN::Req::Api::FreeReq&	msg
							) noexcept;
		/** */
		void	request(	Oscl::Usb::Alloc::Pipe::
							Interrupt::IN::Req::Api::NotifyChangeReq&	msg
							) noexcept;
		/** */
		void	request(	Oscl::Usb::Alloc::Pipe::
							Interrupt::IN::Req::Api::CancelChangeReq&	msg
							) noexcept;

	public:	//Oscl::Usb::Alloc::Pipe::Bulk::IN::Req::Api
		/** */
		void	request(	Oscl::Usb::Alloc::Pipe::
							Bulk::IN::Req::Api::NewReq&	msg
							) noexcept;
		/** */
		void	request(	Oscl::Usb::Alloc::Pipe::
							Bulk::IN::Req::Api::FreeReq&	msg
							) noexcept;
		/** */
		void	request(	Oscl::Usb::Alloc::Pipe::
							Bulk::IN::Req::Api::NotifyChangeReq&	msg
							) noexcept;
		/** */
		void	request(	Oscl::Usb::Alloc::Pipe::
							Bulk::IN::Req::Api::CancelChangeReq&	msg
							) noexcept;

	public:	//Oscl::Usb::Alloc::Pipe::Bulk::OUT::Req::Api
		/** */
		void	request(	Oscl::Usb::Alloc::Pipe::
							Bulk::OUT::Req::Api::NewReq&	msg
							) noexcept;
		/** */
		void	request(	Oscl::Usb::Alloc::Pipe::
							Bulk::OUT::Req::Api::FreeReq&	msg
							) noexcept;
		/** */
		void	request(	Oscl::Usb::Alloc::Pipe::
							Bulk::OUT::Req::Api::NotifyChangeReq&	msg
							) noexcept;
		/** */
		void	request(	Oscl::Usb::Alloc::Pipe::
							Bulk::OUT::Req::Api::CancelChangeReq&	msg
							) noexcept;

	private:	// Oscl::Interrupt::StatusHandlerApi
		/** */
		bool	interrupt() noexcept;

	private:	// Oscl::Interrupt::IsrDsrApi
		/** */
		bool	isr() noexcept;
		/** */
		void	dsr() noexcept;

	private:	// Oscl::Mt::Itc::Server
		/** */
		void	initialize() noexcept;

	private:	// RootHub::Req::Api
		/** */
		void	request(RootHub::Req::Api::OpenReq& msg) noexcept;
		/** */
		void	request(RootHub::Req::Api::CloseReq& msg) noexcept;
		/** */
		void	request(RootHub::Req::Api::ChangeReq& msg) noexcept;
		/** */
		void	request(RootHub::Req::Api::CancelAllReq& msg) noexcept;

	private:	// Oscl::Usb::Alloc::BW::Interrupt::Req::Api
		/** */
		void	request(	Alloc::BW::Interrupt::
							Req::Api::ReserveReq&	msg
							) noexcept;
		/** */
		void	request(	Alloc::BW::Interrupt::
							Req::Api::FreeReq&		msg
							) noexcept;

	private:	// Oscl::Usb::Alloc::BW::Req::Api
		/** */
		void	request(	Alloc::BW::
							Req::Api::ChangeReq&	msg
							) noexcept;
		/** */
		void	request(	Alloc::BW::
							Req::Api::CancelReq&	msg
							) noexcept;

	private:	// HcdApi
		/** */
		TransactionDescMem*	allocTdMem() noexcept;
		/** */
		void		free(TransactionDescMem* m) noexcept;
		/** */
		void		free(Oscl::Usb::Ohci::TD::Descriptor& d) noexcept;
		/** */
		void		queueSofTask(DeferredTaskApi& dt) noexcept;
		/** */
		void		cancelSofTask(DeferredTaskApi& dt) noexcept;
		/** */
		void		queueTdMemTask(DeferredTaskApi& dt) noexcept;
		/** */
		void		cancelTdMemTask(DeferredTaskApi& dt) noexcept;
		/** */
		void		controlListFilled() noexcept;
		/** */
		void		bulkListFilled() noexcept;
		/** */
		unsigned    getCurrentFrameNumber() noexcept;
		/** */
		Oscl::Bus::Api&	getDmaBusApi() noexcept;

	private:
		/** */
		Oscl::Usb::Ohci::
		TD::TransferApi&	getUnexpectedTransferVector() noexcept;
		/** */
		EndpointDescMem*	allocEdMem() noexcept;
		/** */
		void				free(EndpointDescMem* m) noexcept;
	private:
		/** */
		void	unexpectedDone(Oscl::Usb::Ohci::TD::Descriptor& desc) noexcept;
	private:	// ServerApi::SignaledApi
		/** */
		void	signaled() noexcept;
	};

}
}
}
}


#endif
