/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_usb_ohci_ed_desch_
#define _oscl_drv_usb_ohci_ed_desch_
#include "oscl/hw/usb/ohci/mem.h"
#include "oscl/driver/usb/address.h"
#include "oscl/driver/usb/endpoint.h"
#include "oscl/bus/api.h"

/** */
namespace Oscl {
/** */
namespace Usb {
/** */
namespace Ohci {

/** */
namespace TD {
	/** */
	class Descriptor;
	}

/** */
namespace ED {

/** This class initializes and provides operations for manipulating
	the various fields of the OHCI USB endpoint structure. Instances
	of this class and sub-classes must be aligned to a 16-byte boundary
	per USB-OHCI.
 */
class Descriptor {
	protected:
		/** */
		ED::Map			_map;
		/** */
		Oscl::Bus::Api&		_dmaBus;

	public:
		/** */
		Descriptor(	Oscl::Bus::Api&	dmaBus,
					Address			address,
					EndpointID		endpoint,
					unsigned		maxPacketSize,
					bool			lowSpeed,
					bool			isochronous
					) noexcept;
		/** */
		void	skip() noexcept;
		/** */
		void	dontSkip() noexcept;
		/** */
		bool	isHalted() noexcept;
		/** */
		bool	tdListIsEmpty() noexcept;
		/** */
		void	clearHalted() noexcept;
		/** */
		void	setAddress(Address address) noexcept;
		/** */
		void	insert(Descriptor& desc) noexcept;
		/** */
		Descriptor*	getNextEndpointDescriptor() noexcept;
		/** */
		void		setNextED(Descriptor* ed) noexcept;
		/** */
		TD::Descriptor*	getTdQueueTailPointer() noexcept;
		/** */
		TD::Descriptor*	getTdQueueHeadPointer() noexcept;
		/** */
		void	put(TD::Descriptor* td) noexcept;
		/** */
		TD::Descriptor*	remove(TD::Descriptor* td) noexcept;
		/** */
		unsigned	getMaxPacketSize() noexcept;
		/** */
		Oscl::Endian::Little::U32&	getRefToNextPtr() noexcept;

	private:
		/** */
		void	setTdQueueHeadPointer(TD::Descriptor* td) noexcept;
		/** */
		void	setTdQueueTailPointer(TD::Descriptor* td) noexcept;
	};

}
}
}
}

#endif
