/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "desc.h"
#include "oscl/driver/usb/ohci/td/desc.h"
#include "oscl/cpu/sync.h"
#include "oscl/error/fatal.h"

using namespace Oscl::Usb::Ohci::ED;

Descriptor::Descriptor(	Oscl::Bus::Api&	dmaBus,
						Address			address,
						EndpointID		endpoint,
						unsigned		maxPacketSize,
						bool			lowSpeed,
						bool			isochronous
						) noexcept:
		_dmaBus(dmaBus)
		{
	using namespace Oscl::Usb::Ohci::ED;
	
	Dword0::Reg	dword0;
	Dword0::Reg addr		= address;
	Dword0::Reg endp		= endpoint;
	dword0		=		(addr << Dword0::FA::Lsb)
					|	(endp << Dword0::EN::Lsb)
					|	(lowSpeed?
								(Dword0::Reg)Dword0::S::ValueMask_LowSpeed :
								(Dword0::Reg)Dword0::S::ValueMask_FullSpeed
								)
					|	(isochronous?
								(Dword0::Reg)Dword0::F::ValueMask_Isochronous :
								(Dword0::Reg)Dword0::F::ValueMask_General
								)
					|	(maxPacketSize<<Dword0::MPS::Lsb)
					;
	_map.dword0	=		dword0;
	_map.dword1	= 0;
	_map.dword2	= 0;
	_map.dword3	= 0;
	}

void	Descriptor::skip() noexcept{
	using namespace Oscl::Usb::Ohci::ED;
	_map.dword0.changeBits(	Dword0::K::FieldMask,
							Dword0::K::ValueMask_SkipThisTD
							);
	}

void	Descriptor::dontSkip() noexcept{
	using namespace Oscl::Usb::Ohci::ED;
	_map.dword0.changeBits(	Dword0::K::FieldMask,
							Dword0::K::ValueMask_Normal
							);
	}

bool	Descriptor::isHalted() noexcept{
	using namespace Oscl::Usb::Ohci::ED;
	return _map.dword2.equal(	Dword2::H::FieldMask,
								Dword2::H::ValueMask_Halted
								);
	}

bool	Descriptor::tdListIsEmpty() noexcept{
	using namespace Oscl::Usb::Ohci::ED;
	return _map.dword2.equal(	Dword2::HeadP::FieldMask,
								_map.dword1
								);
	}

void	Descriptor::clearHalted() noexcept{
	using namespace Oscl::Usb::Ohci::ED;
	_map.dword2.changeBits(	Dword2::H::FieldMask,
							Dword2::H::ValueMask_Normal
							);
	}

void	Descriptor::setAddress(Address address) noexcept{
	using namespace Oscl::Usb::Ohci::ED;
	_map.dword0.changeBits(	Dword0::FA::FieldMask,
							(address<<Dword0::FA::Lsb)
							);
	}

void	Descriptor::insert(Descriptor& desc) noexcept{
	using namespace Oscl::Usb::Ohci::ED;
	Dword3::Reg		save	= _map.dword3;
	uintptr_t	p;
	_dmaBus.cpuToBus(&desc,p);
	_map.dword3			= (Dword3::Reg)p;
	desc._map.dword3	= save;
	}

Oscl::Usb::Ohci::
TD::Descriptor*	Descriptor::getTdQueueTailPointer() noexcept{
	unsigned long	d	= _map.dword1;
	if(!d) return 0;
	void*	p;
	_dmaBus.busToCpu(d,p);
	return (TD::Descriptor*)p;
	}

Oscl::Usb::Ohci::
TD::Descriptor*	Descriptor::getTdQueueHeadPointer() noexcept{
	using namespace Oscl::Usb::Ohci::ED;
	unsigned long	d	= _map.dword2&Dword2::HeadP::FieldMask;
	if(!d) return 0;
	void*	p;
	_dmaBus.busToCpu(d,p);
	return (TD::Descriptor*)p;
	}

void	Descriptor::setTdQueueHeadPointer(TD::Descriptor* td) noexcept{
	uintptr_t	p;
	_dmaBus.cpuToBus(td,p);
	_map.dword2.changeBits(Dword2::HeadP::FieldMask,p);
	}

void	Descriptor::setTdQueueTailPointer(TD::Descriptor* td) noexcept{
	uintptr_t	p;
	_dmaBus.cpuToBus(td,p);
	_map.dword1	= (uint32_t)p;
	}

void	Descriptor::put(Oscl::Usb::Ohci::TD::Descriptor* td) noexcept{
	Oscl::Usb::Ohci::TD::Descriptor*	head	= getTdQueueHeadPointer();
	Oscl::Usb::Ohci::TD::Descriptor*	tail	= getTdQueueTailPointer();
	uintptr_t	p;
	_dmaBus.cpuToBus(td,p);
	if(!head || !tail){
		_map.dword2.changeBits(Dword2::HeadP::FieldMask,p);
		_map.dword1	= (uint32_t)p;
		return;
		}
	tail->setNextTD(td);
	OsclCpuInOrderMemoryAccessBarrier();
	_map.dword1	= (uint32_t)p;
	}

Oscl::Usb::Ohci::TD::Descriptor*
Descriptor::remove(Oscl::Usb::Ohci::TD::Descriptor* td) noexcept{
	using namespace Oscl::Usb::Ohci::TD;
	// This implementation takes advantage of the fact
	// that there is always at least one TD enqueued.
	// Thus, the client must never use this routine
	// to remove the "empty" TD at the end of the list.
	TD::Descriptor*	nxt;
	TD::Descriptor*	prv;
	for(	nxt=getTdQueueHeadPointer(),prv=0;
			nxt;
			prv=nxt,nxt=nxt->nextTD()
			){
		if(nxt == td){
			TD::Descriptor*	d;
			d	= nxt->nextTD();
			if(!d){
				// Last TD on list.
				Oscl::ErrorFatal::logAndExit(	
					"Client must not remove last TD.\n"
					);
				}
			if(prv){
				// Not first TD on list.
				prv->setNextTD(d);
				}
			else{
				// First TD on list.
				setTdQueueHeadPointer(d);
				}
			return td;
			}
		}
	return 0;
	}

Descriptor*	Descriptor::getNextEndpointDescriptor() noexcept{
	void*	p;
	Oscl::Usb::Ohci::ED::Dword3::Reg	w	= _map.dword3;
	if(w){
		_dmaBus.busToCpu(w,p);
		}
	else {
		p	= 0;
		}
	return (Descriptor*)p;
	}

void	Descriptor::setNextED(Descriptor* ed) noexcept{
	using namespace Oscl::Usb::Ohci::ED;
	if(ed){
		uintptr_t	p;
		_dmaBus.cpuToBus(ed,p);
		_map.dword3	= (Dword3::Reg)p;
		}
	else {
		_map.dword3	= (Dword3::Reg)0;
		}
	}

unsigned	Descriptor::getMaxPacketSize() noexcept{
	using namespace Oscl::Usb::Ohci::ED;
	
	return (_map.dword0 & Dword0::MPS::FieldMask)>>Dword0::MPS::Lsb;
	}

Oscl::Endian::Little::U32&	Descriptor::getRefToNextPtr() noexcept{
	return _map.dword3;
	}
