/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "out.h"

using namespace Oscl::Usb::Ohci::ED;

GeneralOut::GeneralOut(	Oscl::Bus::Api&	dmaBus,
						Address			address,
						EndpointID		endpoint,
						unsigned		maxPacketSize,
						bool			lowSpeed
						) noexcept:
		Descriptor(dmaBus,address,endpoint,maxPacketSize,lowSpeed,false)
		{
	using namespace Oscl::Usb::Ohci::ED;
	_map.dword0.changeBits(	Dword0::D::FieldMask,
							Dword0::D::ValueMask_OUT
							);
	}

