/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_usb_ohci_reg_rhstatush_
#define _oscl_drv_usb_ohci_reg_rhstatush_
#include "oscl/hw/usb/ohci/reg.h"
#include "oscl/bits/bitfield.h"

/** */
namespace Oscl {
/** */
namespace Usb {
/** */
namespace Ohci {
/** */
namespace Register {

/** */
class RootHubStatus {
	private:
		/** */
		volatile Oscl::Usb::Ohci::RhStatus::Reg&		_reg;

		/** */
		Oscl::BitField<Oscl::Usb::Ohci::RhStatus::Reg>	_status;

	public:
		/** */
		inline RootHubStatus(	volatile Oscl::Usb::Ohci::
								RhStatus::Reg&		reg
								) noexcept:
				_reg(reg)
				{
			}

		/** */
		inline void	update() noexcept{
			_status	= _reg;
			}

		/** */
		inline bool	localPowerError() noexcept{
			using namespace Oscl::Usb::Ohci::RhStatus;
			return _status.equal(	LPS::FieldMask,
									LPS::ValueMask_LocalPowerError
									);
			}
		/** */
		inline void	clearGlobalPower() noexcept{
			using namespace Oscl::Usb::Ohci::RhStatus;
			_reg	= LPS::ValueMask_ClearGlobalPower;
			}
		/** */
		inline bool	overCurrent() noexcept{
			using namespace Oscl::Usb::Ohci::RhStatus;
			return _status.equal(	OCI::FieldMask,
									OCI::ValueMask_OverCurrentCondition
									);
			}
		/** */
		inline bool	connectStatusChangeIsRemoteWakeupEvent() noexcept{
			using namespace Oscl::Usb::Ohci::RhStatus;
			return _status.equal(
				DRWE::FieldMask,
				DRWE::ValueMask_ConnectStatusChangeIsARemoteWakeupEvent
				);
			}
		/** */
		inline void	setRemoteWakeupEnable() noexcept{
			using namespace Oscl::Usb::Ohci::RhStatus;
			_reg =	DRWE::ValueMask_SetRemoteWakeupEnable;
			}
		/** */
		inline bool	localPowerStatusChange() noexcept{
			using namespace Oscl::Usb::Ohci::RhStatus;
			return _status.equal(	LPSC::FieldMask,
									LPSC::ValueMask_LocalPowerStatusChanged
									);
			}
		/** */
		inline void	setGlobalPower() noexcept{
			using namespace Oscl::Usb::Ohci::RhStatus;
			_reg =	LPSC::ValueMask_SetGlobalPower;
			}
		/** */
		inline bool	overCurrentChange() noexcept{
			using namespace Oscl::Usb::Ohci::RhStatus;
			return _status.equal(	OCIC::FieldMask,
									OCIC::ValueMask_OciChanged
									);
			}
		/** */
		inline void	clearOverCurrentChange() noexcept{
			using namespace Oscl::Usb::Ohci::RhStatus;
			_reg =	OCIC::ValueMask_Clear;
			}
		/** */
		inline void	clearRemoteWakeupEnable() noexcept{
			using namespace Oscl::Usb::Ohci::RhStatus;
			_reg =	CRWE::ValueMask_ClearRemoteWakeupEnable;
			}
	};

}
}
}
}
#endif
