/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "control.h"

using namespace Oscl::Usb::Ohci::Register;

Control::Control(volatile Oscl::Usb::Ohci::Control::Reg& reg) noexcept:
		_reg(*(Oscl::BitField<Oscl::Usb::Ohci::Control::Reg>*)&reg)
		{
	}

Control::BulkServiceRatio	Control::controlBulkServiceRatio() noexcept{
	using namespace Oscl::Usb::Ohci::Control;
	return (Control::BulkServiceRatio)
		((_reg.read() & CBSR::FieldMask)>>CBSR::Lsb);
	}

void		Control::controlBulkServiceOneToOne() noexcept{
	using namespace Oscl::Usb::Ohci::Control;
	_reg.changeBits(	CBSR::FieldMask,
						CBSR::ValueMask_OneToOne
						);
	}

void		Control::controlBulkServiceTwoToOne() noexcept{
	using namespace Oscl::Usb::Ohci::Control;
	_reg.changeBits(	CBSR::FieldMask,
						CBSR::ValueMask_TwoToOne
						);
	}

void		Control::controlBulkServiceThreeToOne() noexcept{
	using namespace Oscl::Usb::Ohci::Control;
	_reg.changeBits(	CBSR::FieldMask,
						CBSR::ValueMask_ThreeToOne
						);
	}

void		Control::controlBulkServiceFourToOne() noexcept{
	using namespace Oscl::Usb::Ohci::Control;
	_reg.changeBits(	CBSR::FieldMask,
						CBSR::ValueMask_FourToOne
						);
	}

bool		Control::periodicListEnabled() noexcept{
	using namespace Oscl::Usb::Ohci::Control;
	return _reg.equal(	PLE::FieldMask,
						PLE::ValueMask_EnablePeriodicListProcessing
						);
	}

void		Control::enablePeriodicList() noexcept{
	using namespace Oscl::Usb::Ohci::Control;
	_reg.changeBits(	PLE::FieldMask,
						PLE::ValueMask_EnablePeriodicListProcessing
						);
	}

void		Control::disablePeriodicList() noexcept{
	using namespace Oscl::Usb::Ohci::Control;
	_reg.changeBits(	PLE::FieldMask,
						PLE::ValueMask_DisablePeriodicListProcessing
						);
	}

bool		Control::isochronousEnabled() noexcept{
	using namespace Oscl::Usb::Ohci::Control;
	return _reg.equal(	IE::FieldMask,
						IE::ValueMask_EnableIsochronousListProcessing
						);
	}

void		Control::enableIsochronous() noexcept{
	using namespace Oscl::Usb::Ohci::Control;
	_reg.changeBits(	IE::FieldMask,
						IE::ValueMask_EnableIsochronousListProcessing
						);
	}

void		Control::disableIsochronous() noexcept{
	using namespace Oscl::Usb::Ohci::Control;
	_reg.changeBits(	IE::FieldMask,
						IE::ValueMask_DisableIsochronousListProcessing
						);
	}

bool		Control::controlListEnabled() noexcept{
	using namespace Oscl::Usb::Ohci::Control;
	return _reg.equal(	CLE::FieldMask,
						CLE::ValueMask_EnableControlListProcessing
						);
	}

void		Control::enableControlList() noexcept{
	using namespace Oscl::Usb::Ohci::Control;
	_reg.changeBits(	CLE::FieldMask,
						CLE::ValueMask_EnableControlListProcessing
						);
	}

void		Control::disableControlList() noexcept{
	using namespace Oscl::Usb::Ohci::Control;
	_reg.changeBits(	CLE::FieldMask,
						CLE::ValueMask_DisableControlListProcessing
						);
	}

bool		Control::bulkListEnabled() noexcept{
	using namespace Oscl::Usb::Ohci::Control;
	return _reg.equal(	BLE::FieldMask,
						BLE::ValueMask_EnableBulkListProcessing
						);
	}

void		Control::enableBulkList() noexcept{
	using namespace Oscl::Usb::Ohci::Control;
	_reg.changeBits(	BLE::FieldMask,
						BLE::ValueMask_EnableBulkListProcessing
						);
	}

void		Control::disableBulkList() noexcept{
	using namespace Oscl::Usb::Ohci::Control;
	_reg.changeBits(	BLE::FieldMask,
						BLE::ValueMask_DisableBulkListProcessing
						);
	}

Control::UsbState	Control::state() noexcept{
	using namespace Oscl::Usb::Ohci::Control;
	return (Control::UsbState)
		((_reg.read() & HCFS::FieldMask)>>HCFS::Lsb);
	}

void		Control::usbReset() noexcept{
	using namespace Oscl::Usb::Ohci::Control;
	_reg.changeBits(	HCFS::FieldMask,
						HCFS::ValueMask_Reset
						);
	}

void		Control::usbResume() noexcept{
	using namespace Oscl::Usb::Ohci::Control;
	_reg.changeBits(	HCFS::FieldMask,
						HCFS::ValueMask_Resume
						);
	}

void		Control::usbOperational() noexcept{
	using namespace Oscl::Usb::Ohci::Control;
	_reg.changeBits(	HCFS::FieldMask,
						HCFS::ValueMask_Operational
						);
	}

void		Control::usbSuspend() noexcept{
	using namespace Oscl::Usb::Ohci::Control;
	_reg.changeBits(	HCFS::FieldMask,
						HCFS::ValueMask_Suspend
						);
	}

bool		Control::interruptsRoutedToSystemManagement() noexcept{
	using namespace Oscl::Usb::Ohci::Control;
	return _reg.equal(	IR::FieldMask,
						IR::ValueMask_RouteIrqToSystemManagement
						);
	}

void		Control::routeInterruptsNormal() noexcept{
	using namespace Oscl::Usb::Ohci::Control;
	_reg.changeBits(	IR::FieldMask,
						IR::ValueMask_RouteIrqToHost
						);
	}

void		Control::routeInterruptsToSystemManagement() noexcept{
	using namespace Oscl::Usb::Ohci::Control;
	_reg.changeBits(	IR::FieldMask,
						IR::ValueMask_RouteIrqToSystemManagement
						);
	}

bool		Control::remoteWakupConntected() noexcept{
	using namespace Oscl::Usb::Ohci::Control;
	return _reg.equal(	RWC::FieldMask,
						RWC::ValueMask_ConnectRemoteWakeup
						);
	}

void		Control::indicateRemoteWakeupConnected() noexcept{
	using namespace Oscl::Usb::Ohci::Control;
	_reg.changeBits(	RWC::FieldMask,
						RWC::ValueMask_ConnectRemoteWakeup
						);
	}

void		Control::indicateRemoteWakeupNotConnected() noexcept{
	using namespace Oscl::Usb::Ohci::Control;
	_reg.changeBits(	RWC::FieldMask,
						RWC::ValueMask_DisconnectRemoteWakeup
						);
	}

bool		Control::remoteWakupEnabled() noexcept{
	using namespace Oscl::Usb::Ohci::Control;
	return _reg.equal(	RWE::FieldMask,
						RWE::ValueMask_EnableRemoteWakeup
						);
	}

void		Control::enableRemoteWakeup() noexcept{
	using namespace Oscl::Usb::Ohci::Control;
	_reg.changeBits(	RWE::FieldMask,
						RWE::ValueMask_EnableRemoteWakeup
						);
	}

void		Control::disableRemoteWakeup() noexcept{
	using namespace Oscl::Usb::Ohci::Control;
	_reg.changeBits(	RWE::FieldMask,
						RWE::ValueMask_DisableRemoteWakeup
						);
	}

