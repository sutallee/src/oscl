/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_usb_ohci_reg_controlh_
#define _oscl_drv_usb_ohci_reg_controlh_
#include "oscl/hw/usb/ohci/reg.h"
#include "oscl/bits/bitfield.h"

/** */
namespace Oscl {
/** */
namespace Usb {
/** */
namespace Ohci {
/** */
namespace Register {

/** */
class Control {
	public:
		typedef enum {
			Reset = Oscl::Usb::Ohci::Control::HCFS::Value_Reset,
			Resume = Oscl::Usb::Ohci::Control::HCFS::Value_Resume,
			Operational = Oscl::Usb::Ohci::Control::HCFS::Value_Operational,
			Suspend = Oscl::Usb::Ohci::Control::HCFS::Value_Suspend
			} UsbState;
		typedef enum {
			OneToOne = Oscl::Usb::Ohci::Control::CBSR::Value_OneToOne,
			TwoToOne = Oscl::Usb::Ohci::Control::CBSR::Value_TwoToOne,
			ThreeToOne = Oscl::Usb::Ohci::Control::CBSR::Value_ThreeToOne,
			FourToOne = Oscl::Usb::Ohci::Control::CBSR::Value_ThreeToOne
			} BulkServiceRatio;
	private:
		/** */
		Oscl::BitField<Oscl::Usb::Ohci::Control::Reg>&	_reg;

	public:
		/** */
		Control(volatile Oscl::Usb::Ohci::Control::Reg& reg) noexcept;
	public:	// CBSR
		/** */
		BulkServiceRatio	controlBulkServiceRatio() noexcept;
		/** */
		void		controlBulkServiceOneToOne() noexcept;
		/** */
		void		controlBulkServiceTwoToOne() noexcept;
		/** */
		void		controlBulkServiceThreeToOne() noexcept;
		/** */
		void		controlBulkServiceFourToOne() noexcept;

	public:	// PLE
		/** */
		bool		periodicListEnabled() noexcept;
		/** */
		void		enablePeriodicList() noexcept;
		/** */
		void		disablePeriodicList() noexcept;

	public:	// IE
		/** */
		bool		isochronousEnabled() noexcept;
		/** */
		void		enableIsochronous() noexcept;
		/** */
		void		disableIsochronous() noexcept;

	public:	// CLE
		/** */
		bool		controlListEnabled() noexcept;
		/** */
		void		enableControlList() noexcept;
		/** */
		void		disableControlList() noexcept;

	public:	// BLE
		/** */
		bool		bulkListEnabled() noexcept;
		/** */
		void		enableBulkList() noexcept;
		/** */
		void		disableBulkList() noexcept;

	public:	// HCFS
		/** */
		UsbState	state() noexcept;
		/** */
		void		usbReset() noexcept;
		/** */
		void		usbResume() noexcept;
		/** */
		void		usbOperational() noexcept;
		/** */
		void		usbSuspend() noexcept;

	public:	// IR
		/** */
		bool		interruptsRoutedToSystemManagement() noexcept;
		/** */
		void		routeInterruptsNormal() noexcept;
		/** */
		void		routeInterruptsToSystemManagement() noexcept;

	public:	// RWC
		/** */
		bool		remoteWakupConntected() noexcept;
		/** */
		void		indicateRemoteWakeupConnected() noexcept;
		/** */
		void		indicateRemoteWakeupNotConnected() noexcept;

	public:	// RWE
		/** */
		bool		remoteWakupEnabled() noexcept;
		/** */
		void		enableRemoteWakeup() noexcept;
		/** */
		void		disableRemoteWakeup() noexcept;
	};

}
}
}
}
#endif
