/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_usb_ohci_reg_rhportstath_
#define _oscl_drv_usb_ohci_reg_rhportstath_
#include "oscl/hw/usb/ohci/reg.h"
#include "oscl/bits/bitfield.h"

/** */
namespace Oscl {
/** */
namespace Usb {
/** */
namespace Ohci {
/** */
namespace Register {

/** */
class RootHubPortStatus {
	private:
		/** */
		volatile Oscl::Usb::Ohci::RhPortStatus::Reg&	_reg;

		/** */
		Oscl::BitField<Oscl::Usb::Ohci::RhPortStatus::Reg>	_status;

	public:
		/** */
		inline RootHubPortStatus(	volatile Oscl::Usb::Ohci::
									RhPortStatus::Reg&		reg
									) noexcept:
				_reg(reg)
				{
			}

		/** */
		inline void	update() noexcept{
			_status	= _reg;
			}

		/** */
		inline bool	deviceConnected() noexcept{
			using namespace Oscl::Usb::Ohci::RhPortStatus;
			return _status.equal(	CCS::FieldMask,
									CCS::ValueMask_DeviceConnected
									);
			}
		/** */
		inline void	clearPortEnable() noexcept{
			using namespace Oscl::Usb::Ohci::RhPortStatus;
			_reg	= CCS::ValueMask_ClearPortEnable;
			}
		/** */
		inline bool	portEnabled() noexcept{
			using namespace Oscl::Usb::Ohci::RhPortStatus;
			return _status.equal(	PES::FieldMask,
									PES::ValueMask_PortEnabled
									);
			}
		/** */
		inline void	setPortEnable() noexcept{
			using namespace Oscl::Usb::Ohci::RhPortStatus;
			_reg	= PES::ValueMask_SetPortEnable;
			}
		/** */
		inline bool	portSuspended() noexcept{
			using namespace Oscl::Usb::Ohci::RhPortStatus;
			return _status.equal(	PSS::FieldMask,
									PSS::ValueMask_PortSuspended
									);
			}
		/** */
		inline void	setPortSuspend() noexcept{
			using namespace Oscl::Usb::Ohci::RhPortStatus;
			_reg =	PSS::ValueMask_SetPortSuspend;
			}
		/** */
		inline bool	overcurrent() noexcept{
			using namespace Oscl::Usb::Ohci::RhPortStatus;
			return _status.equal(	POCI::FieldMask,
									POCI::ValueMask_Overcurrent
									);
			}
		/** */
		inline void	clearSuspendStatus() noexcept{
			using namespace Oscl::Usb::Ohci::RhPortStatus;
			_reg =	POCI::ValueMask_ClearSuspendStatus;
			}
		/** */
		inline bool	portResetSignalIsActive() noexcept{
			using namespace Oscl::Usb::Ohci::RhPortStatus;
			return _status.equal(	PRS::FieldMask,
									PRS::ValueMask_PortResetActive
									);
			}
		/** */
		inline void	setPortReset() noexcept{
			using namespace Oscl::Usb::Ohci::RhPortStatus;
			_reg =	PRS::ValueMask_SetPortReset;
			}
		/** */
		inline bool	portPowerIsOn() noexcept{
			using namespace Oscl::Usb::Ohci::RhPortStatus;
			return _status.equal(	PPS::FieldMask,
									PPS::ValueMask_PortPowerIsOn
									);
			}
		/** */
		inline void	setPortPower() noexcept{
			using namespace Oscl::Usb::Ohci::RhPortStatus;
			_reg =	PPS::ValueMask_SetPortPower;
			}
		/** */
		inline bool	lowSpeedDeviceAttached() noexcept{
			using namespace Oscl::Usb::Ohci::RhPortStatus;
			return _status.equal(	LSDA::FieldMask,
									LSDA::ValueMask_LowSpeedDeviceAttached
									);
			}
		/** */
		inline void	clearPortPower() noexcept{
			using namespace Oscl::Usb::Ohci::RhPortStatus;
			_reg =	LSDA::ValueMask_ClearPortPower;
			}
		/** */
		inline bool	currentConnectStatusChange() noexcept{
			using namespace Oscl::Usb::Ohci::RhPortStatus;
			return _status.equal(	CSC::FieldMask,
									CSC::ValueMask_Changed
									);
			}
		/** */
		inline void	clearCurrentConnectStatusChange() noexcept{
			using namespace Oscl::Usb::Ohci::RhPortStatus;
			_reg =	CSC::ValueMask_Clear;
			}
		/** */
		inline bool	portEnableStatusChange() noexcept{
			using namespace Oscl::Usb::Ohci::RhPortStatus;
			return _status.equal(	PESC::FieldMask,
									PESC::ValueMask_Changed
									);
			}
		/** */
		inline void	clearPortEnableStatusChange() noexcept{
			using namespace Oscl::Usb::Ohci::RhPortStatus;
			_reg =	PESC::ValueMask_Clear;
			}
		/** */
		inline bool	portSuspendStatusChange() noexcept{
			using namespace Oscl::Usb::Ohci::RhPortStatus;
			return _status.equal(	PSSC::FieldMask,
									PSSC::ValueMask_ResumeIsComplete
									);
			}
		/** */
		inline void	clearPortSuspendStatusChange() noexcept{
			using namespace Oscl::Usb::Ohci::RhPortStatus;
			_reg =	PSSC::ValueMask_Clear;
			}
		/** */
		inline bool	portOverCurrentIndicatorChange() noexcept{
			using namespace Oscl::Usb::Ohci::RhPortStatus;
			return _status.equal(	OCIC::FieldMask,
									OCIC::ValueMask_Changed
									);
			}
		/** */
		inline void	clearPortOverCurrentIndicatorChange() noexcept{
			using namespace Oscl::Usb::Ohci::RhPortStatus;
			_reg =	OCIC::ValueMask_Clear;
			}
		/** */
		inline bool	portResetStatusChange() noexcept{
			using namespace Oscl::Usb::Ohci::RhPortStatus;
			return _status.equal(	PRSC::FieldMask,
									PRSC::ValueMask_ResetComplete
									);
			}
		/** */
		inline void	clearPortResetStatusChange() noexcept{
			using namespace Oscl::Usb::Ohci::RhPortStatus;
			_reg =	PRSC::ValueMask_Clear;
			}
	};

}
}
}
}
#endif
