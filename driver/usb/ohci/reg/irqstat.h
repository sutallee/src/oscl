/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_usb_ohci_reg_irqstath_
#define _oscl_drv_usb_ohci_reg_irqstath_
#include "oscl/hw/usb/ohci/reg.h"
#include "oscl/bits/bitfield.h"

/** */
namespace Oscl {
/** */
namespace Usb {
/** */
namespace Ohci {
/** */
namespace Register {

/** */
class InterruptStatus {
	private:
		/** */
		volatile Oscl::Usb::Ohci::Control::Reg&			_reg;

		/** */
		Oscl::BitField<Oscl::Usb::Ohci::Control::Reg>	_status;

	public:
		/** */
		inline InterruptStatus(	volatile Oscl::Usb::Ohci::
								InterruptStatus::Reg&		reg
								) noexcept:
				_reg(reg)
				{
			}

		/** */
		inline void	update() noexcept{
			_status	= _reg;
			}

		inline bool	anyPending() noexcept{
			return _reg;
			}

		/** */
		inline bool	scheduleOverrun() noexcept{
			using namespace Oscl::Usb::Ohci::InterruptStatus;
			return _status.equal(	SO::FieldMask,
									SO::ValueMask_Pending
									);
			}

		/** */
		inline void	ackScheduleOverrun() noexcept{
			using namespace Oscl::Usb::Ohci::InterruptStatus;
			_reg	= SO::ValueMask_Pending;
			}

		/** */
		inline bool	writebackDoneHead() noexcept{
			using namespace Oscl::Usb::Ohci::InterruptStatus;
			return _status.equal(	WDH::FieldMask,
									WDH::ValueMask_Pending
									);
			}

		/** */
		inline void	ackWritebackDoneHead() noexcept{
			using namespace Oscl::Usb::Ohci::InterruptStatus;
			_reg	= WDH::ValueMask_Pending;
			}

		/** */
		inline bool	startOfFrame() noexcept{
			using namespace Oscl::Usb::Ohci::InterruptStatus;
			return _status.equal(	SF::FieldMask,
									SF::ValueMask_Pending
									);
			}

		/** */
		inline void	ackStartOfFrame() noexcept{
			using namespace Oscl::Usb::Ohci::InterruptStatus;
			_reg	= SF::ValueMask_Pending;
			}

		/** */
		inline bool	resumeDetected() noexcept{
			using namespace Oscl::Usb::Ohci::InterruptStatus;
			return _status.equal(	RD::FieldMask,
									RD::ValueMask_Pending
									);
			}

		/** */
		inline void	ackResumeDetected() noexcept{
			using namespace Oscl::Usb::Ohci::InterruptStatus;
			_reg	= RD::ValueMask_Pending;
			}

		/** */
		inline bool	unrecoverableError() noexcept{
			using namespace Oscl::Usb::Ohci::InterruptStatus;
			return _status.equal(	UE::FieldMask,
									UE::ValueMask_Pending
									);
			}

		/** */
		inline void	ackUnrecoverableError() noexcept{
			using namespace Oscl::Usb::Ohci::InterruptStatus;
			_reg	= UE::ValueMask_Pending;
			}

		/** */
		inline bool	frameNumberOverflow() noexcept{
			using namespace Oscl::Usb::Ohci::InterruptStatus;
			return _status.equal(	FNO::FieldMask,
									FNO::ValueMask_Pending
									);
			}

		/** */
		inline void	ackFrameNumberOverflow() noexcept{
			using namespace Oscl::Usb::Ohci::InterruptStatus;
			_reg	= FNO::ValueMask_Pending;
			}

		/** */
		inline bool	rootHubStatusChange() noexcept{
			using namespace Oscl::Usb::Ohci::InterruptStatus;
			return _status.equal(	RHSC::FieldMask,
									RHSC::ValueMask_Pending
									);
			}

		/** */
		inline void	ackRootHubStatusChange() noexcept{
			using namespace Oscl::Usb::Ohci::InterruptStatus;
			_reg	= RHSC::ValueMask_Pending;
			}

		/** */
		inline bool	ownershipChange() noexcept{
			using namespace Oscl::Usb::Ohci::InterruptStatus;
			return _status.equal(	OC::FieldMask,
									OC::ValueMask_Pending
									);
			}

		/** */
		inline void	ackOwnershipChange() noexcept{
			using namespace Oscl::Usb::Ohci::InterruptStatus;
			_reg	= OC::ValueMask_Pending;
			}

	};

}
}
}
}
#endif
