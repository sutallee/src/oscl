/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "cmdstat.h"

using namespace Oscl::Usb::Ohci::Register;

CmdStatus::CmdStatus(volatile Oscl::Usb::Ohci::CommandStatus::Reg& reg) noexcept:
		_reg(reg)
		{
	}

void		CmdStatus::hostControllerReset() noexcept{
	using namespace Oscl::Usb::Ohci::CommandStatus;
	_reg	= HCR::ValueMask_ResetCommand;
	}

void		CmdStatus::controlListFilled() noexcept{
	using namespace Oscl::Usb::Ohci::CommandStatus;
	_reg	= CLF::ValueMask_ProcessControlList;
	}

void		CmdStatus::bulkListFilled() noexcept{
	using namespace Oscl::Usb::Ohci::CommandStatus;
	_reg	= BLF::ValueMask_ProcessBulkList;
	}

void		CmdStatus::ownershipChangeRequest() noexcept{
	using namespace Oscl::Usb::Ohci::CommandStatus;
	_reg	= OCR::ValueMask_RequestOwnershipChange;
	}

unsigned		CmdStatus::schedulingOverrunCount() noexcept{
	using namespace Oscl::Usb::Ohci::CommandStatus;
	return (_reg & SOC::FieldMask) >> SOC::Lsb;
	}

