/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_usb_ohci_reg_rhdescah_
#define _oscl_drv_usb_ohci_reg_rhdescah_
#include "oscl/hw/usb/ohci/reg.h"
#include "oscl/bits/bitfield.h"

/** */
namespace Oscl {
/** */
namespace Usb {
/** */
namespace Ohci {
/** */
namespace Register {

/** */
class RootHubDescA {
	private:
		/** */
		volatile Oscl::Usb::Ohci::RhDescriptorA::Reg&	_reg;

		/** */
		Oscl::BitField<Oscl::Usb::Ohci::RhDescriptorA::Reg>	_status;

		/** */
		typedef enum {
			Ganged			= 0,
			Individual		= 1,
			AlwaysPowered	= 2
			} PowerSwitchMode;
		/** */
		typedef enum {
			Collective	= 0,
			PerPort		= 1,
			None		= 2
			} OvercurrentMode;

	public:
		/** */
		inline RootHubDescA(	volatile Oscl::Usb::Ohci::
								RhDescriptorA::Reg&		reg
									) noexcept:
				_reg(reg)
				{
			}

		/** */
		inline unsigned	numberOfDownstreamPorts() noexcept{
			using namespace Oscl::Usb::Ohci::RhDescriptorA;
			return (_reg & NDP::FieldMask)>>NDP::Lsb;
			}
		
		/** */
		inline PowerSwitchMode	powerSwitchMode() noexcept{
			using namespace Oscl::Usb::Ohci::RhDescriptorA;
			if(		(_reg & NPS::FieldMask)
					==	NPS::ValueMask_PortsAreAlwaysPowered
				) return AlwaysPowered;
			if(		(_reg & PSM::FieldMask)
				==	PSM::ValueMask_PortsAreIndividuallyPowered
				){
				return Individual;
				}
			return Ganged;
			}
		inline bool	compoundDevice() noexcept{
			using namespace Oscl::Usb::Ohci::RhDescriptorA;
			return		(_reg & DT::FieldMask)
					==	DT::ValueMask_CompoundDevice;
			}
		/** */
		inline void	powerSwitchModeAlwaysPowered() noexcept{
			using namespace Oscl::Usb::Ohci::RhDescriptorA;
			_reg	= 
						(_reg & ~(NPS::FieldMask | PSM::FieldMask))
					|	(		NPS::ValueMask_PortsAreAlwaysPowered
							|	PSM::ValueMask_AllPortsArePoweredAtTheSameTime
							)
				;
						
			}
		/** */
		inline void	powerSwitchModeIndividual() noexcept{
			using namespace Oscl::Usb::Ohci::RhDescriptorA;
			_reg	= 
						(_reg & ~(NPS::FieldMask | PSM::FieldMask))
					|	(		NPS::ValueMask_PortsArePowerSwitched
							|	PSM::ValueMask_AllPortsArePoweredAtTheSameTime
							)
				;
						
			}
		/** */
		inline void	powerSwitchModeGanged() noexcept{
			using namespace Oscl::Usb::Ohci::RhDescriptorA;
			_reg	= 
						(_reg & ~(NPS::FieldMask | PSM::FieldMask))
					|	(		NPS::ValueMask_PortsArePowerSwitched
							|	PSM::ValueMask_PortsAreIndividuallyPowered
							)
				;
						
			}
		/** */
		inline OvercurrentMode	overcurrentMode() noexcept{
			using namespace Oscl::Usb::Ohci::RhDescriptorA;
			if(		(_reg & NOCP::FieldMask)
				==	NOCP::ValueMask_NoOvercurrentProtection
				) return None;
			if(		(_reg & OCPM::FieldMask)
				==	OCPM::ValueMask_IndividualStatusReporting
				) return PerPort;
			return Collective;
			}
		/** */
		inline unsigned	powerOnToGoodTimeIn2msUnits() noexcept{
			using namespace Oscl::Usb::Ohci::RhDescriptorA;
			return (_reg & POTPGT::FieldMask)>>POTPGT::Lsb;
			}
	};

}
}
}
}
#endif
