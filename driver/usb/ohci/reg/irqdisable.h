/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_usb_ohci_reg_irqdisableh_
#define _oscl_drv_usb_ohci_reg_irqdisableh_
#include "oscl/hw/usb/ohci/reg.h"
#include "oscl/bits/bitfield.h"

/** */
namespace Oscl {
/** */
namespace Usb {
/** */
namespace Ohci {
/** */
namespace Register {

/** */
class InterruptDisable {
	private:
		/** */
		volatile Oscl::Usb::Ohci::InterruptDisable::Reg&	_reg;

	public:
		/** */
		inline InterruptDisable(	volatile Oscl::Usb::Ohci::
									InterruptDisable::Reg&		reg
								) noexcept:
				_reg(reg)
				{
			}

		/** */
		inline void	scheduleOverrun() noexcept{
			using namespace Oscl::Usb::Ohci::InterruptDisable;
			_reg	= SO::ValueMask_Disable;
			}
		/** */
		inline void	writebackDone() noexcept{
			using namespace Oscl::Usb::Ohci::InterruptDisable;
			_reg	= WDH::ValueMask_Disable;
			}
		/** */
		inline void	startOfFrame() noexcept{
			using namespace Oscl::Usb::Ohci::InterruptDisable;
			_reg	= SF::ValueMask_Disable;
			}
		/** */
		inline void	resumeDetected() noexcept{
			using namespace Oscl::Usb::Ohci::InterruptDisable;
			_reg	= RD::ValueMask_Disable;
			}
		/** */
		inline void	unrecoverableError() noexcept{
			using namespace Oscl::Usb::Ohci::InterruptDisable;
			_reg	= UE::ValueMask_Disable;
			}
		/** */
		inline void	frameNumberOverflow() noexcept{
			using namespace Oscl::Usb::Ohci::InterruptDisable;
			_reg	= FNO::ValueMask_Disable;
			}
		/** */
		inline void	rootHubStatusChange() noexcept{
			using namespace Oscl::Usb::Ohci::InterruptDisable;
			_reg	= RHSC::ValueMask_Disable;
			}
		/** */
		inline void	ownershipChange() noexcept{
			using namespace Oscl::Usb::Ohci::InterruptDisable;
			_reg	= OC::ValueMask_Disable;
			}
		/** */
		inline void	master() noexcept{
			using namespace Oscl::Usb::Ohci::InterruptDisable;
			_reg	= MIE::ValueMask_Disable;
			}
	};

}
}
}
}
#endif
