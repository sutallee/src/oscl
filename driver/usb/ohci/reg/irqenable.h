/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_usb_ohci_reg_irqenableh_
#define _oscl_drv_usb_ohci_reg_irqenableh_
#include "oscl/hw/usb/ohci/reg.h"
#include "oscl/bits/bitfield.h"

/** */
namespace Oscl {
/** */
namespace Usb {
/** */
namespace Ohci {
/** */
namespace Register {

/** */
class InterruptEnable {
	private:
		/** */
		volatile Oscl::Usb::Ohci::InterruptEnable::Reg&		_reg;

	public:
		/** */
		inline InterruptEnable(	volatile Oscl::Usb::Ohci::
								InterruptEnable::Reg&		reg
								) noexcept:
				_reg(reg)
				{
			}

		/** */
		inline void	scheduleOverrun() noexcept{
			using namespace Oscl::Usb::Ohci::InterruptEnable;
			_reg	= SO::ValueMask_Enable;
			}
		/** */
		inline void	writebackDone() noexcept{
			using namespace Oscl::Usb::Ohci::InterruptEnable;
			_reg	= WDH::ValueMask_Enable;
			}
		/** */
		inline void	startOfFrame() noexcept{
			using namespace Oscl::Usb::Ohci::InterruptEnable;
			_reg	= SF::ValueMask_Enable;
			}
		/** */
		inline void	resumeDetected() noexcept{
			using namespace Oscl::Usb::Ohci::InterruptEnable;
			_reg	= RD::ValueMask_Enable;
			}
		/** */
		inline void	unrecoverableError() noexcept{
			using namespace Oscl::Usb::Ohci::InterruptEnable;
			_reg	= UE::ValueMask_Enable;
			}
		/** */
		inline void	frameNumberOverflow() noexcept{
			using namespace Oscl::Usb::Ohci::InterruptEnable;
			_reg	= FNO::ValueMask_Enable;
			}
		/** */
		inline void	rootHubStatusChange() noexcept{
			using namespace Oscl::Usb::Ohci::InterruptEnable;
			_reg	= RHSC::ValueMask_Enable;
			}
		/** */
		inline void	ownershipChange() noexcept{
			using namespace Oscl::Usb::Ohci::InterruptEnable;
			_reg	= OC::ValueMask_Enable;
			}
		/** */
		inline void	master() noexcept{
			using namespace Oscl::Usb::Ohci::InterruptEnable;
			_reg	= MIE::ValueMask_Enable;
			}
	};

}
}
}
}
#endif
