/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_usb_hcd_apih_
#define _oscl_drv_usb_hcd_apih_
#include "oscl/driver/usb/alloc/pipe/message/reqapi.h"
#include "oscl/driver/usb/alloc/pipe/message/syncapi.h"
#include "oscl/driver/usb/alloc/pipe/interrupt/in/reqapi.h"
#include "oscl/driver/usb/alloc/pipe/interrupt/in/syncapi.h"
#include "oscl/driver/usb/alloc/pipe/bulk/in/reqapi.h"
#include "oscl/driver/usb/alloc/pipe/bulk/in/syncapi.h"
#include "oscl/driver/usb/alloc/pipe/bulk/out/reqapi.h"
#include "oscl/driver/usb/alloc/pipe/bulk/out/syncapi.h"
#include "oscl/driver/usb/alloc/bw/reqapi.h"
#include "oscl/driver/usb/alloc/bw/interrupt/reqapi.h"
#include "oscl/driver/usb/alloc/bw/interrupt/syncapi.h"

#include "oscl/driver/usb/alloc/addr/reqapi.h"
#include "oscl/driver/usb/pipe/message/reqapi.h"
#include "oscl/driver/usb/pipe/message/syncapi.h"

/** */
namespace Oscl {
/** */
namespace Usb {
/** */
namespace HCD {

/** This interface represents access to a USB 1.1 Host Controller.
	The driver could be for a standard OHCI or UHCI host controller,
	or some other type of host controller.
 */
class Api {
	public:
		// Device Monitor Required
		/** This operation returns a reference to the interface used
			to dynamically allocate Message (e.g. Control) pipes
			asynchronously.
		 */
		virtual Oscl::Usb::Alloc::
		Pipe::Message::
		Req::Api::SAP&				getMsgPipeAllocSAP() noexcept=0;

		/** This operation returns a reference to the interface used
			to dynamically allocate Message (e.g. Control) pipes
			synchronously.
		 */
		virtual Oscl::Usb::Alloc::
		Pipe::Message::
		SyncApi&					getMsgPipeAllocSyncApi() noexcept=0;

		/** This operation returns a reference to the interface used
			to dynamically allocate Interrupt pipes asynchronously.
		 */
		virtual Oscl::Usb::Alloc::
		Pipe::Interrupt::IN::
		Req::Api::SAP&				getInterruptInPipeAllocSAP() noexcept=0;

		/** This operation returns a reference to the interface used
			to dynamically allocate Interrupt pipes synchronously.
		 */
		virtual Oscl::Usb::Alloc::
		Pipe::Interrupt::IN::
		SyncApi&					getInterruptInPipeAllocSyncApi() noexcept=0;

		/** This operation returns a reference to the interface used
			to dynamically allocate Bulk-IN pipes asynchronously.
		 */
		virtual Oscl::Usb::Alloc::
		Pipe::Bulk::IN::
		Req::Api::SAP&				getBulkInPipeAllocSAP() noexcept=0;

		/** This operation returns a reference to the interface used
			to dynamically allocate Bulk-IN pipes synchronously.
		 */
		virtual Oscl::Usb::Alloc::
		Pipe::Bulk::IN::
		SyncApi&					getBulkInPipeAllocSyncApi() noexcept=0;

		/** This operation returns a reference to the interface used
			to dynamically allocate Bulk-OUT pipes asynchronously.
		 */
		virtual Oscl::Usb::Alloc::
		Pipe::Bulk::OUT::
		Req::Api::SAP&				getBulkOutPipeAllocSAP() noexcept=0;

		/** This operation returns a reference to the interface used
			to dynamically allocate Bulk-OUT pipes synchronously.
		 */
		virtual Oscl::Usb::Alloc::
		Pipe::Bulk::OUT::
		SyncApi&					getBulkOutPipeAllocSyncApi() noexcept=0;

		/** This operation returns a reference to the interface used
			to monitor changes in the amount of USB bus bandwidth
			allocation. These notifications can be used by clients
			to wait until required bandwidth becomes available.
		 */
		virtual Oscl::Usb::Alloc::
		BW::Req::Api::SAP&			getBwAllocSAP() noexcept=0;

		/** This operation returns a reference to the interface used
			to dynamically allocate USB bus bandwidth for interrupt
			pipes asyncronously.
		 */
		virtual Oscl::Usb::Alloc::
		BW::Interrupt::
		Req::Api::SAP&				getBwInterruptAllocSAP() noexcept=0;

		/** This operation returns a reference to the interface used
			to dynamically allocate USB bus bandwidth for interrupt
			pipes syncronously.
		 */
		virtual Oscl::Usb::Alloc::
		BW::Interrupt::
		SyncApi&					getBwInterruptAllocSyncApi() noexcept=0;

		// Hub Port required : USB Address Zero API

		/** This operation returns a reference to a SAP used
			to arbitrate access to the USB default address (zero)
			and to dynamically allocate bus addresses.
		 */
		virtual Oscl::Usb::Alloc::
		Addr::Req::Api::SAP&		getHubPortSAP() noexcept=0;

		/** This operation returns a reference to an interface used
			transfer control packets across a Low Speed default
			(address zero) control pipe synchronously. This is typically
			used during device enumeration.
		 */
		virtual Oscl::Usb::Pipe::
		Message::SyncApi&			getLsSyncApi() noexcept=0;

		/** This operation returns a reference to an interface used
			transfer control packets across a Full Speed default
			(address zero) control pipe synchronously. This is typically
			used during device enumeration.
		 */
		virtual Oscl::Usb::Pipe::
		Message::SyncApi&			getHsSyncApi() noexcept=0;

		/** This operation returns a reference to an interface used
			transfer control packets across a Low Speed default
			(address zero) control pipe asynchronously. This is typically
			used during device enumeration.
		 */
		virtual Oscl::Usb::Pipe::
		Message::Req::Api::SAP&		getLsEpSAP() noexcept=0;

		/** This operation returns a reference to an interface used
			transfer control packets across a Full Speed default
			(address zero) control pipe asynchronously. This is typically
			used during device enumeration.
		 */
		virtual Oscl::Usb::Pipe::
		Message::Req::Api::SAP&		getHsEpSAP() noexcept=0;
	};

}
}
}

#endif
