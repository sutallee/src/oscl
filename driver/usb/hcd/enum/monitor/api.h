/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_usb_hcd_enum_monitor_apih_
#define _oscl_drv_usb_hcd_enum_monitor_apih_
#include "oscl/driver/usb/alloc/pipe/message/reqapi.h"
#include "oscl/driver/usb/alloc/pipe/message/syncapi.h"
#include "oscl/driver/usb/alloc/pipe/interrupt/in/reqapi.h"
#include "oscl/driver/usb/alloc/pipe/interrupt/in/syncapi.h"
#include "oscl/driver/usb/alloc/pipe/bulk/in/reqapi.h"
#include "oscl/driver/usb/alloc/pipe/bulk/in/syncapi.h"
#include "oscl/driver/usb/alloc/pipe/bulk/out/reqapi.h"
#include "oscl/driver/usb/alloc/pipe/bulk/out/syncapi.h"
#include "oscl/driver/usb/alloc/bw/reqapi.h"
#include "oscl/driver/usb/alloc/bw/interrupt/reqapi.h"
#include "oscl/driver/usb/alloc/bw/interrupt/syncapi.h"

/** */
namespace Oscl {
/** */
namespace Usb {
/** */
namespace HCD {
/** */
namespace Enum {
/** */
namespace Monitor {

/** HCD interfaces required by a device enumeration monitor. */
class Api {
	public:
		/** */
		virtual ~Api(){}
		/** */
		virtual Oscl::Usb::Alloc::
		Pipe::Message::
		Req::Api::SAP&				getMsgPipeAllocSAP() noexcept=0;
		/** */
		virtual Oscl::Usb::Alloc::
		Pipe::Message::
		SyncApi&					getMsgPipeAllocSyncApi() noexcept=0;
		/** */
		virtual Oscl::Usb::Alloc::
		Pipe::Interrupt::IN::
		Req::Api::SAP&				getInterruptInPipeAllocSAP() noexcept=0;
		/** */
		virtual Oscl::Usb::Alloc::
		Pipe::Interrupt::IN::
		SyncApi&					getInterruptInPipeAllocSyncApi() noexcept=0;
		/** */
		virtual Oscl::Usb::Alloc::
		Pipe::Bulk::IN::
		Req::Api::SAP&				getBulkInPipeAllocSAP() noexcept=0;
		/** */
		virtual Oscl::Usb::Alloc::
		Pipe::Bulk::IN::
		SyncApi&					getBulkInPipeAllocSyncApi() noexcept=0;
		/** */
		virtual Oscl::Usb::Alloc::
		Pipe::Bulk::OUT::
		Req::Api::SAP&				getBulkOutPipeAllocSAP() noexcept=0;
		/** */
		virtual Oscl::Usb::Alloc::
		Pipe::Bulk::OUT::
		SyncApi&					getBulkOutPipeAllocSyncApi() noexcept=0;
		/** */
		virtual Oscl::Usb::Alloc::
		BW::Req::Api::SAP&			getBwAllocSAP() noexcept=0;
		/** */
		virtual Oscl::Usb::Alloc::
		BW::Interrupt::
		Req::Api::SAP&				getBwInterruptAllocSAP() noexcept=0;
		/** */
		virtual Oscl::Usb::Alloc::
		BW::Interrupt::
		SyncApi&					getBwInterruptAllocSyncApi() noexcept=0;
	};

}
}
}
}
}

#endif
