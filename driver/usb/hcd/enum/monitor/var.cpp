/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "var.h"

using namespace Oscl::Usb::HCD::Enum::Monitor;

Var::Var(	Oscl::Mt::Itc::PostMsgApi&	myPapi,
				Oscl::Usb::Alloc::
				Pipe::Message::
				Req::Api&					msgPipeAllocReqApi,
				Oscl::Usb::Alloc::
				Pipe::Interrupt::IN::
				Req::Api&					interruptInPipeAllocReqApi,
				Oscl::Usb::Alloc::
				Pipe::Bulk::IN::
				Req::Api&					bulkInPipeAllocReqApi,
				Oscl::Usb::Alloc::
				Pipe::Bulk::OUT::
				Req::Api&					bulkOutPipeAllocReqApi,
				Oscl::Usb::Alloc::
				BW::Req::Api&				bwAllocReqApi,
				Oscl::Usb::Alloc::
				BW::Interrupt::
				Req::Api&					bwInterruptAllocReqApi
				) noexcept:
		_msgPipeAllocSAP(msgPipeAllocReqApi,myPapi),
		_msgPipeAllocSync(_msgPipeAllocSAP),
		_interruptInPipeAllocSAP(interruptInPipeAllocReqApi,myPapi),
		_interruptInPipeAllocSync(_interruptInPipeAllocSAP),
		_bulkInPipeAllocSAP(bulkInPipeAllocReqApi,myPapi),
		_bulkInPipeAllocSync(_bulkInPipeAllocSAP),
		_bulkOutPipeAllocSAP(bulkOutPipeAllocReqApi,myPapi),
		_bulkOutPipeAllocSync(_bulkOutPipeAllocSAP),
		_bwAllocSAP(bwAllocReqApi,myPapi),
		_bwInterruptAllocSAP(bwInterruptAllocReqApi,myPapi),
		_bwInterruptAllocSync(_bwInterruptAllocSAP)
		{
	}

Oscl::Usb::Alloc::
Pipe::Message::
Req::Api::SAP&				Var::getMsgPipeAllocSAP() noexcept{
	return _msgPipeAllocSAP;
	}

Oscl::Usb::Alloc::
Pipe::Message::
SyncApi&					Var::getMsgPipeAllocSyncApi() noexcept{
	return _msgPipeAllocSync;
	}

Oscl::Usb::Alloc::
Pipe::Interrupt::IN::
Req::Api::SAP&				Var::getInterruptInPipeAllocSAP() noexcept{
	return _interruptInPipeAllocSAP;
	}

Oscl::Usb::Alloc::
Pipe::Interrupt::IN::
SyncApi&					Var::getInterruptInPipeAllocSyncApi() noexcept{
	return _interruptInPipeAllocSync;
	}

Oscl::Usb::Alloc::
Pipe::Bulk::IN::
Req::Api::SAP&				Var::getBulkInPipeAllocSAP() noexcept{
	return _bulkInPipeAllocSAP;
	}

Oscl::Usb::Alloc::
Pipe::Bulk::IN::
SyncApi&					Var::getBulkInPipeAllocSyncApi() noexcept{
	return _bulkInPipeAllocSync;
	}

Oscl::Usb::Alloc::
Pipe::Bulk::OUT::
Req::Api::SAP&				Var::getBulkOutPipeAllocSAP() noexcept{
	return _bulkOutPipeAllocSAP;
	}

Oscl::Usb::Alloc::
Pipe::Bulk::OUT::
SyncApi&					Var::getBulkOutPipeAllocSyncApi() noexcept{
	return _bulkOutPipeAllocSync;
	}

Oscl::Usb::Alloc::
BW::Req::Api::SAP&			Var::getBwAllocSAP() noexcept{
	return _bwAllocSAP;
	}

Oscl::Usb::Alloc::
BW::Interrupt::
Req::Api::SAP&				Var::getBwInterruptAllocSAP() noexcept{
	return _bwInterruptAllocSAP;
	}

Oscl::Usb::Alloc::
BW::Interrupt::
SyncApi&					Var::getBwInterruptAllocSyncApi() noexcept{
	return _bwInterruptAllocSync;
	}

