/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_usb_hcd_enum_monitor_varh_
#define _oscl_drv_usb_hcd_enum_monitor_varh_
#include "api.h"
#include "oscl/driver/usb/alloc/pipe/message/sync.h"
#include "oscl/driver/usb/alloc/pipe/interrupt/in/sync.h"
#include "oscl/driver/usb/alloc/pipe/bulk/in/sync.h"
#include "oscl/driver/usb/alloc/pipe/bulk/out/sync.h"
#include "oscl/driver/usb/alloc/bw/interrupt/sync.h"

/** */
namespace Oscl {
/** */
namespace Usb {
/** */
namespace HCD {
/** */
namespace Enum {
/** */
namespace Monitor {

/** HCD interfaces required by a device enumeration monitor. */
class Var : public Api {
	public:
		Oscl::Usb::Alloc::
		Pipe::Message::
		Req::Api::ConcreteSAP		_msgPipeAllocSAP;
		/** */
		Oscl::Usb::Alloc::
		Pipe::Message::
		Sync						_msgPipeAllocSync;
		/** */
		Oscl::Usb::Alloc::
		Pipe::Interrupt::IN::
		Req::Api::ConcreteSAP		_interruptInPipeAllocSAP;
		/** */
		Oscl::Usb::Alloc::
		Pipe::Interrupt::IN::
		Sync						_interruptInPipeAllocSync;
		/** */
		Oscl::Usb::Alloc::
		Pipe::Bulk::IN::
		Req::Api::ConcreteSAP		_bulkInPipeAllocSAP;
		/** */
		Oscl::Usb::Alloc::
		Pipe::Bulk::IN::
		Sync						_bulkInPipeAllocSync;
		/** */
		Oscl::Usb::Alloc::
		Pipe::Bulk::OUT::
		Req::Api::ConcreteSAP		_bulkOutPipeAllocSAP;
		/** */
		Oscl::Usb::Alloc::
		Pipe::Bulk::OUT::
		Sync						_bulkOutPipeAllocSync;
		/** */
		Oscl::Usb::Alloc::
		BW::Req::Api::ConcreteSAP	_bwAllocSAP;
		/** */
		Oscl::Usb::Alloc::
		BW::Interrupt::
		Req::Api::ConcreteSAP		_bwInterruptAllocSAP;
		/** */
		Oscl::Usb::Alloc::
		BW::Interrupt::
		Sync						_bwInterruptAllocSync;
	public:
		Var(	Oscl::Mt::Itc::PostMsgApi&	myPapi,
				Oscl::Usb::Alloc::
				Pipe::Message::
				Req::Api&					msgPipeAllocReqApi,
				Oscl::Usb::Alloc::
				Pipe::Interrupt::IN::
				Req::Api&					interruptInPipeAllocReqApi,
				Oscl::Usb::Alloc::
				Pipe::Bulk::IN::
				Req::Api&					bulkInPipeAllocReqApi,
				Oscl::Usb::Alloc::
				Pipe::Bulk::OUT::
				Req::Api&					bulkOutPipeAllocReqApi,
				Oscl::Usb::Alloc::
				BW::Req::Api&				bwAllocReqApi,
				Oscl::Usb::Alloc::
				BW::Interrupt::
				Req::Api&					bwInterruptAllocReqApi
				) noexcept;
	public:	// Api
		/** */
		Oscl::Usb::Alloc::
		Pipe::Message::
		Req::Api::SAP&				getMsgPipeAllocSAP() noexcept;
		/** */
		Oscl::Usb::Alloc::
		Pipe::Message::
		SyncApi&					getMsgPipeAllocSyncApi() noexcept;
		/** */
		Oscl::Usb::Alloc::
		Pipe::Interrupt::IN::
		Req::Api::SAP&				getInterruptInPipeAllocSAP() noexcept;
		/** */
		Oscl::Usb::Alloc::
		Pipe::Interrupt::IN::
		SyncApi&					getInterruptInPipeAllocSyncApi() noexcept;
		/** */
		Oscl::Usb::Alloc::
		Pipe::Bulk::IN::
		Req::Api::SAP&				getBulkInPipeAllocSAP() noexcept;
		/** */
		Oscl::Usb::Alloc::
		Pipe::Bulk::IN::
		SyncApi&					getBulkInPipeAllocSyncApi() noexcept;
		/** */
		Oscl::Usb::Alloc::
		Pipe::Bulk::OUT::
		Req::Api::SAP&				getBulkOutPipeAllocSAP() noexcept;
		/** */
		Oscl::Usb::Alloc::
		Pipe::Bulk::OUT::
		SyncApi&					getBulkOutPipeAllocSyncApi() noexcept;
		/** */
		Oscl::Usb::Alloc::
		BW::Req::Api::SAP&			getBwAllocSAP() noexcept;
		/** */
		Oscl::Usb::Alloc::
		BW::Interrupt::
		Req::Api::SAP&				getBwInterruptAllocSAP() noexcept;
		/** */
		Oscl::Usb::Alloc::
		BW::Interrupt::
		SyncApi&					getBwInterruptAllocSyncApi() noexcept;
	};

}
}
}
}
}

#endif
