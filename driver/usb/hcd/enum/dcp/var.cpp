/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "var.h"

using namespace Oscl::Usb::HCD::Enum::DefCntrlPipe;

Var::Var(	Oscl::Mt::Itc::PostMsgApi&	myPapi,
			Oscl::Usb::Alloc::
			Pipe::Message::
			Req::Api&					msgPipeAllocReqApi,
			Oscl::Usb::Alloc::
			Addr::Req::Api&				hubPortReqApi,
			Oscl::Usb::Pipe::
			Message::PipeApi&			lsPipeApi,
			Oscl::Usb::Pipe::
			Message::PipeApi&			hsPipeApi
			) noexcept:
		_msgPipeAllocSAP(msgPipeAllocReqApi,myPapi),
		_msgPipeAllocSync(_msgPipeAllocSAP),
		_hubPortSAP(hubPortReqApi,myPapi),
		_lsPipeApi(lsPipeApi),
		_hsPipeApi(hsPipeApi)
		{
	}

Oscl::Usb::Alloc::
Pipe::Message::
Req::Api::SAP&				Var::getMsgPipeAllocSAP() noexcept{
	return _msgPipeAllocSAP;
	}

Oscl::Usb::Alloc::
Pipe::Message::
SyncApi&					Var::getMsgPipeAllocSyncApi() noexcept{
	return _msgPipeAllocSync;
	}

Oscl::Usb::Alloc::
Addr::Req::Api::SAP&		Var::getHubPortSAP() noexcept{
	return _hubPortSAP;
	}

Oscl::Usb::Pipe::
Message::PipeApi&			Var::getLsPipeApi() noexcept{
	return _lsPipeApi;
	}

Oscl::Usb::Pipe::
Message::PipeApi&			Var::getHsPipeApi() noexcept{
	return _hsPipeApi;
	}

