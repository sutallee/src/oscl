/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_usb_hid_boot_mouse_driverh_
#define _oscl_drv_usb_hid_boot_mouse_driverh_
#include "oscl/memory/block.h"
#include "oscl/queue/queue.h"
#include "oscl/mt/itc/srv/close.h"
#include "oscl/driver/usb/pipe/interrupt/in/respmem.h"
#include "oscl/driver/usb/pipe/message/respmem.h"
#include "oscl/bits/fieldapi.h"
#include "oscl/hw/est/mdp8xx/cs.h"

/** */
namespace Oscl {
/** */
namespace Usb {
/** */
namespace HID {
/** */
namespace Boot {
/** */
namespace Mouse {

/** */
class Driver :	public Oscl::Mt::Itc::Srv::CloseSync,
				private Oscl::Mt::Itc::Srv::Close::Req::Api,
				private Oscl::Usb::Pipe::Interrupt::IN::Resp::Api
				{
	public:
		union ContextSetupMem {
			};
		/** */
		struct SetupMem {
			/** */
			ContextSetupMem		context;
			};
		/** */
		union ContextPacketMem {
			unsigned char	mousePayload[16];	// FIXME: use symbol for size
			};
		/** */
		struct PacketMem {
			/** */
			ContextPacketMem				context;
			};
		/** */
		union CancelMem {
			/** */
			Oscl::Usb::Pipe::Interrupt::
			IN::Resp::CancelMem					interruptInPipe;
			Oscl::Usb::Pipe::Message::
			Resp::CancelMem						messagePipe;
			};
	private:
		/** */
		Oscl::Usb::Pipe::
		Interrupt::IN::Resp::ReadMem		_respMem;
		/** */
		CancelMem							_cancelMem;
		/** */
		Oscl::Usb::Pipe::Interrupt::
		IN::Req::Api::SAP&					_changePipe;
		/** */
		SetupMem&							_setupMem;
		/** */
		PacketMem&							_packetMem;
		/** */
		Oscl::Mt::Itc::PostMsgApi&			_myPapi;
		/** */
		Oscl::Bits::
		FieldApi<	Oscl::Est::Mdp8xxPro::
					LedControl::Reg
					>&    					_led;
		/** */
		unsigned char						_maxPacketSize;
		/** */
		Oscl::Mt::Itc::Srv::Open::
		Req::Api::OpenReq*					_openReq;
		/** */
		Oscl::Mt::Itc::Srv::Close::
		Req::Api::CloseReq*					_closeReq;
		/** */
		Oscl::Usb::Pipe::Interrupt::IN::
		Resp::Api::ReadResp*				_resp;
		/** */
		bool								_open;
		/** */
		bool								_closing;

	public:
		/** */
		Driver(	Oscl::Usb::Pipe::Interrupt::
				IN::Req::Api::SAP&					changePipe,
				Oscl::Mt::Itc::PostMsgApi&			myPapi,
				SetupMem&							setupMem,
				PacketMem&							packetMem,
				Oscl::Bits::
				FieldApi<	Oscl::Est::Mdp8xxPro::
							LedControl::Reg
							>&    					led,
				unsigned char						maxPacketSize
				) noexcept;

	private:
		/** */
		void	sendChangeIRP() noexcept;

		/** */
		void	cancelChangeIRP() noexcept;

		/** */
		void	processChange() noexcept;

	private:	// Oscl::Mt::Itc::Srv::Close::Req::Api
		/** */
		void	request(	Oscl::Mt::Itc::Srv::
							Open::Req::Api::OpenReq&	msg
							) noexcept;
		/** */
		void	request(	Oscl::Mt::Itc::Srv::
							Close::Req::Api::CloseReq&	msg
							) noexcept;
	private:	// Oscl::Usb::Pipe::Interrupt::IN::Resp::Api
		/** */
		void	response(	Oscl::Usb::Pipe::Interrupt::
							IN::Resp::Api::ReadResp&		msg
							) noexcept;
		/** */
		void	response(	Oscl::Usb::Pipe::Interrupt::
							IN::Resp::Api::CancelResp&		msg
							) noexcept;
	};

}
}
}
}
}


#endif
