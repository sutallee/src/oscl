/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <new>
#include "driver.h"
#include "oscl/mt/thread.h"
#include "oscl/error/fatal.h"

using namespace Oscl::Usb::HID::Boot::Mouse;

Driver::Driver(	Oscl::Usb::Pipe::Interrupt::
				IN::Req::Api::SAP&					changePipe,
				Oscl::Mt::Itc::PostMsgApi&			myPapi,
				SetupMem&							setupMem,
				PacketMem&							packetMem,
				Oscl::Bits::
				FieldApi<	Oscl::Est::Mdp8xxPro::
							LedControl::Reg
							>&    					led,
				unsigned char						maxPacketSize
				) noexcept:
		Oscl::Mt::Itc::Srv::CloseSync(*this,myPapi),
		_changePipe(changePipe),
		_setupMem(setupMem),
		_packetMem(packetMem),
		_myPapi(myPapi),
		_led(led),
		_maxPacketSize(maxPacketSize),
		_openReq(0),
		_closeReq(0),
		_open(false),
		_closing(false)
		{
	}

void	Driver::sendChangeIRP() noexcept{
	Oscl::Usb::Pipe::Interrupt::IN::Req::Api::ReadPayload*
	payload	= new(&_respMem.payload)
				Oscl::Usb::Pipe::Interrupt::IN::
				Req::Api::ReadPayload(	&_packetMem,
										_maxPacketSize
										);

	_resp	= new(&_respMem.resp)
				Oscl::Usb::Pipe::Interrupt::IN::
				Resp::Api::ReadResp(	_changePipe.getReqApi(),
										*this,
										_myPapi,
										*payload
										);
	_changePipe.post(_resp->getSrvMsg());
	}

void	Driver::cancelChangeIRP() noexcept{
	Oscl::Usb::Pipe::Interrupt::IN::Req::Api::CancelPayload*
	payload	= new(&_cancelMem.interruptInPipe.payload)
				Oscl::Usb::Pipe::Interrupt::IN::
				Req::Api::CancelPayload(_resp->getSrvMsg());

	Oscl::Usb::Pipe::Interrupt::IN::Resp::Api::CancelResp*
	resp	= new(&_cancelMem.interruptInPipe.resp)
				Oscl::Usb::Pipe::Interrupt::IN::
				Resp::Api::CancelResp(	_changePipe.getReqApi(),
										*this,
										_myPapi,
										*payload
										);

	_changePipe.post(resp->getSrvMsg());
	}

void	Driver::processChange() noexcept{
	unsigned char*	p	= (unsigned char*)&_packetMem;
	unsigned char	button	= (p[0] & 0x07)<<2;
	unsigned char	x	= p[1]<<5;
	_led.changeBits(0xFC,(button|x));
	}

void	Driver::request(	Oscl::Mt::Itc::Srv::
							Open::Req::Api::OpenReq&	msg
							) noexcept{
	if(_open){
		// FIXME: protocol error
		while(true);
		}
	_openReq	= &msg;
	_open		= true;
	sendChangeIRP();
	msg.returnToSender();
	}

void	Driver::request(	Oscl::Mt::Itc::Srv::
							Close::Req::Api::CloseReq&	msg
							) noexcept{
	_closeReq	= &msg;
	_closing	= true;
	cancelChangeIRP();	// async
	}

void	Driver::response(	Oscl::Usb::Pipe::Interrupt::IN::
							Resp::Api::CancelResp&		msg
							) noexcept{
	_closeReq->returnToSender();
	}

void	Driver::response(	Oscl::Usb::Pipe::Interrupt::IN::
							Resp::Api::ReadResp&		msg
							) noexcept{
	if(_closing) return;
	if(msg.getPayload()._failed){
		// presumably a device removal
		// just wait for the close.
		return;
		}
	processChange();
	sendChangeIRP();
	}

