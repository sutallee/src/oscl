/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <new>
#include <string.h>
#include <stdio.h>
#include "driver.h"
#include "oscl/mt/thread.h"
#include "oscl/error/fatal.h"
#include "oscl/error/info.h"
#include "oscl/hw/usb/serial/pl2303/interrupt.h"
#include "oscl/cache/line.h"

using namespace Oscl::Usb::Serial::PL2303;

Driver::Driver(	Oscl::Usb::Pipe::Message::
				PipeApi&							controlPipe,
				Oscl::Usb::Pipe::Interrupt::
				IN::Req::Api::SAP&					changePipe,
				Oscl::Usb::Pipe::Bulk::
				IN::Req::Api::SAP&					bulkInPipe,
				Oscl::Usb::Pipe::Bulk::
				OUT::Req::Api::SAP&					bulkOutPipe,
				Oscl::Mt::Itc::PostMsgApi&			myPapi,
				SetupMem&							setupMem,
				PacketMem&							packetMem,
				unsigned char						maxInterruptPacketSize,
				unsigned char						maxBulkInPacketSize,
				unsigned char						maxBulkOutPacketSize
				) noexcept:
		Oscl::Mt::Itc::Srv::CloseSync(*this,myPapi),
		_outstandingRxTransactions(0),
		_controlPipe(controlPipe),
		_changePipe(changePipe),
		_bulkInPipe(bulkInPipe),
		_bulkOutPipe(bulkOutPipe),
		_setupMem(setupMem),
		_packetMem(packetMem),
		_myPapi(myPapi),
		_ctsDist(false),
		_overflowDist(false),
		_dcdDist(false),
		_dsrDist(false),
		_ringDist(false),
		_uartSync(	*this,
					myPapi,
					*this,
					myPapi,
					*this,
					myPapi,
					_dcdDist,
					myPapi,
					_dsrDist,
					myPapi,
					*this,
					myPapi
					),
		_maxInterruptPacketSize(maxInterruptPacketSize),
		_maxBulkInPacketSize(maxBulkInPacketSize),
		_maxBulkOutPacketSize(maxBulkOutPacketSize),
		_openReq(0),
		_closeReq(0),
		_open(false),
		_closing(false),
		_baudRate(9600),
		_nStopBits(stopBits1_0),
		_parity(parityNone),
		_bitsPerChar(bitsPerChar8),
		_assertRTS(true),
		_assertDTR(true),
		_enableHardwareFlowControl(true)
		{
	unsigned
	bulkInPacketSize	= ((	sizeof(BulkRxPacketMem)
							+	(OsclCacheLineSizeInBytes-1))/
								OsclCacheLineSizeInBytes
								)
							* OsclCacheLineSizeInBytes
							;
	if(maxBulkInPacketSize > bulkInPacketSize){
		Oscl::ErrorFatal::logAndExit("buffer too small.");
		}
	const unsigned	maxPackets	= sizeof(_packetMem.rx)/bulkInPacketSize;
	unsigned char*	p	= (unsigned char*)_packetMem.rx;
	for(unsigned i=0;i<maxPackets;++i,p+=bulkInPacketSize){
		RxBuffer*	packet = (RxBuffer*)p;
		_freeRxBuffers.put(packet);
		}

	for(unsigned i=0;i<nRxTrans;++i){
		_freeRxTransMem.put(&_rxTransMem[i]);
		}
	}

//	| 7 | 6 | 5 | 4 | 3 | 2 | 1 | 0 |
//  +---+---+---+---+---+---+---+---+
//  |Dir|  Type |  Recipient        |
//  +---+---+---+---+---+---+---+---+
// SetLine
//	bmRequestType: 0x21	= | 0 | 0 1 | 0 0 0 0 1 |
//		Host-to-Device
//		Class
//		Interface
//	bRequest	0x20
//	buffer[0:3]	: baud rate
//	buffer[4]	: number of stop bits
//					0	= 1 stop bit
//					1	= 1.5 stop bit
//					2	= 2 stop bit
//	buffer[5]	: parity
//					0	= none
//					1	= odd
//					2	= even
//					3	= mark
//					4	= space
//	buffer[6]	: character size
//					5	= 5 bits
//					6	= 6 bits
//					7	= 7 bits
//					8	= 8 bits
//
// SetControl
//	bmRequestType: 0x21	= | 0 | 0 1 | 0 0 0 0 1 |
//		Host-to-Device
//		Class
//		Interface
//	bRequest	0x22
//	wValue	bit[0]	= DTR
//			bit[1]	= RTS
//
// BreakRequest
//	bmRequestType: 0x21	= | 0 | 0 1 | 0 0 0 0 1 |
//		Host-to-Device
//		Class
//		Interface
//	bRequest	0x23
//	wValue	ON	= 0xFFFF
//			OFF	= 0x0000
//
// GetLine
//	bmRequestType: 0xA1	= | 1 | 0 1 | 0 0 0 0 1 |
//		Device-to-Host
//		Class
//		Interface
//	bRequest	0x21
//	buffer[0:3]	: baud rate
//	buffer[4]	: number of stop bits
//					0	= 1 stop bit
//					1	= 1.5 stop bit
//					2	= 2 stop bit
//	buffer[5]	: parity
//					0	= none
//					1	= odd
//					2	= even
//					3	= mark
//					4	= space
//	buffer[6]	: character size
//					5	= 5 bits
//					6	= 6 bits
//					7	= 7 bits
//					8	= 8 bits
//
// VendorWrite
//	bmRequestType: 0x40	= | 0 | 1 0 | 0 0 0 0 0 |
//		Host-to-Device
//		Vendor
//		Device
//	bRequest	0x01
//
//	wValue		0x0000
//	wIndex		0x0001
//
//	wValue		0x0000
//	wIndex		0x0041	// CRTSCTS (hardware RTS/CTS flow control enable?)
//	-- during open --
//	wValue		0x0404
//	wIndex		0x0000
//
//	wValue		0x0404
//	wIndex		0x0001
//
//	wValue		0x0000
//	wIndex		0x0001
//
//	wValue		0x0001
//	wIndex		0x00C0
//
//	wValue		0x0002
//	wIndex		0x0004
//
// VendorRead
//	bmRequestType: 0xC0	= | 1 | 1 0 | 0 0 0 0 0 |
//		Device-to-Host
//		Vendor
//		Device
//	bRequest	0x01
//	-- during open --
//	wValue		0x8484
//	wIndex		0x0000
//
//	wValue		0x8383
//	wIndex		0x0000
//
//

Oscl::UART::DTE::Api&	Driver::getUartDteApi() noexcept{
	return _uartSync;
	}

bool	Driver::setBaudRate(unsigned long bps) noexcept{
	_baudRate	= bps;
	return updateCommParameters();
	}

bool	Driver::breakON() noexcept{
	return sendBreak(true);
	}

bool	Driver::breakOFF() noexcept{
	return sendBreak(false);
	}

void	Driver::sendChangeIRP() noexcept{
	Oscl::Usb::Pipe::Interrupt::IN::Req::Api::ReadPayload*
	payload	= new(&_interruptRespMem.payload)
				Oscl::Usb::Pipe::Interrupt::IN::
				Req::Api::ReadPayload(	&_packetMem.change,
										_maxInterruptPacketSize
										);

	_resp	= new(&_interruptRespMem.resp)
				Oscl::Usb::Pipe::Interrupt::IN::
				Resp::Api::ReadResp(	_changePipe.getReqApi(),
										*this,
										_myPapi,
										*payload
										);
	_changePipe.post(_resp->getSrvMsg());
	}

void	Driver::cancelChangeIRP() noexcept{
	Oscl::Usb::Pipe::Interrupt::IN::Req::Api::CancelPayload*
	payload	= new(&_cancelMem.interruptInPipe.payload)
				Oscl::Usb::Pipe::Interrupt::IN::
				Req::Api::CancelPayload(_resp->getSrvMsg());

	Oscl::Usb::Pipe::Interrupt::IN::Resp::Api::CancelResp*
	resp	= new(&_cancelMem.interruptInPipe.resp)
				Oscl::Usb::Pipe::Interrupt::IN::
				Resp::Api::CancelResp(	_changePipe.getReqApi(),
										*this,
										_myPapi,
										*payload
										);

	_changePipe.post(resp->getSrvMsg());
	}

void	Driver::cancelPendingBulkReads() noexcept{
	RxTrans*	next;
	while((next=_finishedRxTrans.get())){
		free(*next);
		}
	Oscl::Stream::Input::Req::Api::ReadReq* readReq;
	while((readReq=_pendingStreamInReqs.get())){
		readReq->returnToSender();
		}
	next	= _pendingRxTrans.get();
	if(next){
		next->cancel(_cancelMem.bulkInPipe);
		return;
		}
	cancelPendingBulkWrites();
	}

void	Driver::cancelPendingBulkWrites() noexcept{
	if(_write.currentReq){
		_write.currentReq->_payload._nWritten	= 0;
		_write.currentReq->returnToSender();
		_write.currentReq	= 0;
		}
	if(!_write.pendingBulkWrite){
		// All done.
		_open		= false;
		_closing	= false;
		_closeReq->returnToSender();
		return;
		}
	Oscl::Usb::Pipe::Bulk::OUT::Req::Api::CancelPayload*
	payload	= new(&_cancelMem.bulkOutPipe.payload)
				Oscl::Usb::Pipe::Bulk::OUT::
				Req::Api::CancelPayload(_write.pendingBulkWrite->getSrvMsg());

	Oscl::Usb::Pipe::Bulk::OUT::Resp::Api::CancelResp*
	resp	= new(&_cancelMem.bulkOutPipe.resp)
				Oscl::Usb::Pipe::Bulk::OUT::
				Resp::Api::CancelResp(	_bulkOutPipe.getReqApi(),
										*this,
										_myPapi,
										*payload
										);
	_write.pendingBulkWrite	= 0;
	_bulkOutPipe.post(resp->getSrvMsg());
	}

void	Driver::processChange() noexcept{
	using namespace Oscl::Usb::Hw::Serial::PL2303::Interrupt;
	enum{maxBufferSize=64};
	char	buffer[maxBufferSize+1];
	const unsigned char*	p	= (const unsigned char*)&_packetMem.change;
	Oscl::Error::Info::log("PL2303 Interrupt IN:\n\r");
	for(unsigned i=0;i<_maxInterruptPacketSize;++i){
		sprintf(buffer," %u:0x%2.2X",i,(unsigned)p[i]);
		Oscl::Error::Info::log(buffer);
		};
	Oscl::Error::Info::log("\n\r");
	Change::Reg	change	= p[Offset::change];
	if(		(change & Change::CTS::FieldMask)
		==	Change::CTS::ValueMask_Asserted
		){
		_ctsDist.assert();
		Oscl::Error::Info::log("CTS+ ");
		}
	else{
		_ctsDist.negate();
		Oscl::Error::Info::log("CTS- ");
		}
	if(		(change & Change::BufferOverflow::FieldMask)
		==	Change::BufferOverflow::ValueMask_Asserted
		){
		_overflowDist.assert();
		Oscl::Error::Info::log("OV+ ");
		}
	else{
		_overflowDist.negate();
		Oscl::Error::Info::log("OV- ");
		}
	if(		(change & Change::DSR::FieldMask)
		==	Change::DSR::ValueMask_Asserted
		){
		_dsrDist.assert();
		Oscl::Error::Info::log("DSR+ ");
		}
	else{
		_dsrDist.negate();
		Oscl::Error::Info::log("DSR- ");
		}
	if(		(change & Change::CD::FieldMask)
		==	Change::CD::ValueMask_Asserted
		){
		_dcdDist.assert();
		Oscl::Error::Info::log("DCD+ ");
		}
	else{
		_dcdDist.negate();
		Oscl::Error::Info::log("DCD- ");
		}
	if(		(change & Change::RI::FieldMask)
		==	Change::RI::ValueMask_Asserted
		){
		_ringDist.assert();
		Oscl::Error::Info::log("RI+ ");
		}
	else{
		_ringDist.negate();
		Oscl::Error::Info::log("RI- ");
		}
	Oscl::Error::Info::log("\n\r");
	}

bool	Driver::vendorRead(	unsigned	wValue,
							unsigned	wIndex
							) noexcept{
	bool	failed;
	using namespace Oscl::Usb::Hw::Setup;
	const unsigned char
	bmRequestType	=		bmRequestType::DIR::ValueMask_DeviceToHost
						|	bmRequestType::Type::ValueMask_Vendor
						|	bmRequestType::Recipient::ValueMask_Device
						;
	const unsigned char
	bRequest	= 1;	// Meaning?
	Oscl::Usb::Setup::Packet*
	setupPacket	= new(&_setupMem)
					Oscl::Usb::Setup::Packet(	bmRequestType,
												bRequest,
												wValue,
												wIndex,
												1
												);
	unsigned char*	rawMem	= (unsigned char*)&_packetMem.control;
	failed	= _controlPipe.getSyncApi().read(*setupPacket,rawMem);
	return !failed;
	}

bool	Driver::vendorWrite(	unsigned	wValue,
								unsigned	wIndex
								) noexcept{
	bool	failed;
	using namespace Oscl::Usb::Hw::Setup;
	const unsigned char
	bmRequestType	=		bmRequestType::DIR::ValueMask_HostToDevice
						|	bmRequestType::Type::ValueMask_Vendor
						|	bmRequestType::Recipient::ValueMask_Device
						;
	const unsigned char
	bRequest	= 1;	// Meaning?
	Oscl::Usb::Setup::Packet*
	setupPacket	= new(&_setupMem)
					Oscl::Usb::Setup::Packet(	bmRequestType,
												bRequest,
												wValue,
												wIndex,
												0
												);
	failed	= _controlPipe.getSyncApi().control(*setupPacket);
	return !failed;
	}

bool	Driver::updateCommParameters() noexcept{
	if(!vendorWrite(0x0000,0x0001)){
		return false;
		}
	bool	failed;
	unsigned char*	buff	= (unsigned char*)&_packetMem.control;

	buff[3]	= _baudRate >>  0;
	buff[2]	= _baudRate >>  8;
	buff[1]	= _baudRate >> 16;
	buff[0]	= _baudRate >> 24;
	buff[4]	= _nStopBits;
	buff[5]	= _parity;
	buff[6]	= _bitsPerChar;

	using namespace Oscl::Usb::Hw::Setup;
	const unsigned char
	bmRequestType	=		bmRequestType::DIR::ValueMask_HostToDevice
						|	bmRequestType::Type::ValueMask_Class
						|	bmRequestType::Recipient::ValueMask_Interface
						;
	const unsigned char	bRequest	= 0x20;	// Meaning?
	Oscl::Usb::Setup::Packet*
	setupPacket	= new(&_setupMem)
					Oscl::Usb::Setup::Packet(	bmRequestType,
												bRequest,
												0,	// wValue
												0,	// wIndex
												7
												);
	failed	= _controlPipe.getSyncApi().write(*setupPacket,buff);
	return !failed;
	}

bool	Driver::updateRtsDtr() noexcept{
	bool	failed;
	const unsigned dtrMask	= (_assertDTR)?0x01:0x00;
	const unsigned rtsMask	= (_assertDTR)?0x02:0x00;
	using namespace Oscl::Usb::Hw::Setup;
	const unsigned char
	bmRequestType	=		bmRequestType::DIR::ValueMask_HostToDevice
						|	bmRequestType::Type::ValueMask_Class
						|	bmRequestType::Recipient::ValueMask_Interface
						;
	const unsigned char	bRequest	= 0x20;	// Meaning? SET LINE?
	Oscl::Usb::Setup::Packet*
	setupPacket	= new(&_setupMem)
					Oscl::Usb::Setup::Packet(	bmRequestType,
												bRequest,
												(dtrMask | rtsMask),// wValue
												0,	// wIndex
												0	// wLength
												);
	failed	= _controlPipe.getSyncApi().control(*setupPacket);
	return !failed;
	}

bool	Driver::updateHardwareFlowControl() noexcept{
	// FIXME: I'm guessing here.
	unsigned	wIndex	= (_enableHardwareFlowControl)?0x0041:0x0001;
	return vendorWrite(0x0000,wIndex);
	}

bool	Driver::sendBreak(bool on) noexcept{
	bool	failed;
	const unsigned wValue	= (on)?0xFFFF:0x0000;
	using namespace Oscl::Usb::Hw::Setup;
	const unsigned char
	bmRequestType	=		bmRequestType::DIR::ValueMask_HostToDevice
						|	bmRequestType::Type::ValueMask_Class
						|	bmRequestType::Recipient::ValueMask_Interface
						;
	const unsigned char	bRequest	= 0x22;	// Meaning?
	Oscl::Usb::Setup::Packet*
	setupPacket	= new(&_setupMem)
					Oscl::Usb::Setup::Packet(	bmRequestType,
												bRequest,
												wValue,
												0,	// wIndex
												0	// wLength
												);
	failed	= _controlPipe.getSyncApi().control(*setupPacket);
	return !failed;
	}

void	Driver::txCurrentBulk() noexcept{
	Oscl::Usb::Pipe::Bulk::
	OUT::Req::Api::WritePayload*
	payload	= new(&_bulkOutRespMem.payload)
				Oscl::Usb::Pipe::Bulk::
				OUT::Req::Api::WritePayload(	&_packetMem.tx,
												_write.currentLength
												);
	Oscl::Usb::Pipe::Bulk::
	OUT::Resp::Api::WriteResp*
	resp	= new (&_bulkOutRespMem.resp)
				Oscl::Usb::Pipe::Bulk::
				OUT::Resp::Api::WriteResp(	_bulkOutPipe.getReqApi(),
											*this,
											_myPapi,
											*payload
											);
	_bulkOutPipe.post(resp->getSrvMsg());
	_write.pendingBulkWrite	= resp;
	_write.busy	= true;
	}

void	Driver::txNextBulk() noexcept{
	if(_write.busy) return;
	while(true){
		if(!_write.currentReq){
			_write.currentReq	= _write.pendingReq.get();
			if(!_write.currentReq) return;
			_write.currentOffset	= 0;
			_write.currentRemaining	= _write.currentReq->
										_payload._buffer.length();
			}
		if(!_write.currentRemaining){
			_write.currentReq->_payload._nWritten	= _write.currentOffset;
			_write.currentReq->returnToSender();
			_write.currentReq	= 0;
			continue;
			}
		unsigned len	=
		_write.currentReq->_payload._buffer.copyOut(	&_packetMem.tx,
														maxBulkTxPacketBytes,
														_write.currentOffset
														);
		_write.currentRemaining	-= len;
		_write.currentOffset	+= len;
		_write.currentLength	= len;
		txCurrentBulk();
		return;
		}
	}

void	Driver::sendAllRxTrans() noexcept{
	while(_freeRxBuffers.first() && _freeRxTransMem.first()){
		RxTrans&
		trans	= build(	*_freeRxTransMem.get(),
							_freeRxBuffers.get()
							);
		trans.start();
		_pendingRxTrans.put(&trans);
		}
	}

void		Driver::done(RxTrans& trans) noexcept{
	// This operation should either copy the
	// buffer context to a waiting
	_pendingRxTrans.remove(&trans);
	if(_closing || !_open){
		free(trans);
		return;
		}

	if(_finishedRxTrans.last()){
		Oscl::Buffer::Base&	source	= trans;
		Oscl::Buffer::Base&	dest	= *_finishedRxTrans.last();
		unsigned
		n	= dest.append(source);
		source.stripHeader(n);
		if(source.length()){
			_finishedRxTrans.put(&trans);
			return;
			}
		restart(trans);
		return;
		}
	_finishedRxTrans.put(&trans);
	processRxStream();
	}

void	Driver::done(	Oscl::Usb::Pipe::Bulk::
						IN::Resp::CancelMem&		mem
						) noexcept{
	// FIXME:
	// This is a part of the closing sequence.
	// If there are more pending transactions,
	// then the next one should be canceled.
	// Otherwise, all pendint RxTrans have
	// been canceled at this point and the RxTrans
	// closing is done.
	cancelPendingBulkReads();
	}

void	Driver::failed(RxTrans& trans) noexcept{
	// FIXME: Indicate error condition, start close?
	_pendingRxTrans.remove(&trans);
	free(trans);
	}

void		Driver::free(RxTrans& trans) noexcept{
	void*		packetMem		= trans._packetMem;
	trans.~RxTrans();
	_freeRxTransMem.put((RxTransMem*)&trans);
	_freeRxBuffers.put((RxBuffer*)packetMem);
	}

Driver::RxTrans&	Driver::build(	RxTransMem&	mem,
									void*		packetMem
									) noexcept{
	Driver::RxTrans*
	trans	= new(&mem) RxTrans(	*this,
									_bulkInPipe,
									_myPapi,
									packetMem,
//									_maxBulkInPacketSize
									sizeof(BulkRxPacketMem)
									);
	return *trans;
	}

void		Driver::restart(RxTrans& trans) noexcept{
	void*		packetMem		= trans._packetMem;
	trans.~RxTrans();
	build(	*(RxTransMem*)&trans,
			packetMem
			).start();
	_pendingRxTrans.put(&trans);
	}

void	Driver::processRxStream() noexcept{
	while(_pendingStreamInReqs.first() && _finishedRxTrans.first()){
		Oscl::Stream::Input::Req::Api::ReadReq*
		streamReq	= _pendingStreamInReqs.get();
		RxTrans*	rxTrans;
		while((rxTrans=_finishedRxTrans.get())){
			Oscl::Buffer::Base&	dest	= streamReq->_payload._buffer;
			Oscl::Buffer::Base&	source	= *rxTrans;
			unsigned
			n	= dest.append(source);
			source.stripHeader(n);
			if(source.length()){
				_finishedRxTrans.push(rxTrans);
				break;
				}
			restart(*rxTrans);
			if(!dest.remaining()){
				break;
				}
			}
		streamReq->returnToSender();
		}
	}

void	Driver::request(	Oscl::Mt::Itc::Srv::
							Open::Req::Api::OpenReq&	msg
							) noexcept{
	if(_open){
		// FIXME: protocol error
		while(true);
		}
	// Series of mysterious vendor
	// specific setup requests
	if(!vendorRead(0x8484,0x0000)){
		for(;;);	// FIXME
		}
	if(!vendorWrite(0x0404,0x0000)){
		for(;;);	// FIXME
		}
	if(!vendorRead(0x8484,0x0000)){
		for(;;);	// FIXME
		}
	if(!vendorRead(0x8383,0x0000)){
		for(;;);	// FIXME
		}
	if(!vendorRead(0x8484,0x0000)){
		for(;;);	// FIXME
		}
	if(!vendorWrite(0x0404,0x0001)){
		for(;;);	// FIXME
		}
	if(!vendorRead(0x8484,0x0000)){
		for(;;);	// FIXME
		}
	if(!vendorRead(0x8383,0x0000)){
		for(;;);	// FIXME
		}
	if(!vendorWrite(0x0000,0x0001)){
		for(;;);	// FIXME
		}
	if(!vendorWrite(0x0001,0x00C0)){
		for(;;);	// FIXME
		}
	if(!vendorWrite(0x0002,0x0004)){
		for(;;);	// FIXME
		}
	// Setup default comm parameters
	if(!updateCommParameters()){
		for(;;);	// FIXME
		}
	// Setup default comm parameters
	if(!updateRtsDtr()){
		for(;;);	// FIXME
		}
	if(!updateHardwareFlowControl()){
		for(;;);	// FIXME
		}
	sendChangeIRP();
	_openReq	= &msg;
	_open		= true;
	msg.returnToSender();
	sendAllRxTrans();
	}

void	Driver::request(	Oscl::Mt::Itc::Srv::
							Close::Req::Api::CloseReq&	msg
							) noexcept{
	_closeReq	= &msg;
	_closing	= true;
	cancelChangeIRP();	// async
	}

void	Driver::response(	Oscl::Usb::Pipe::Interrupt::IN::
							Resp::Api::CancelResp&		msg
							) noexcept{
	cancelPendingBulkReads();
	}

void	Driver::response(	Oscl::Usb::Pipe::Interrupt::IN::
							Resp::Api::ReadResp&		msg
							) noexcept{
	if(_closing || !_open) return;
	if(msg.getPayload()._failed){
		Oscl::Error::Info::log("USB PL2303 serial interrupt IN failed!\n\r");
		// presumably a device removal
		// just wait for the close.
		return;
		}
	processChange();
	sendChangeIRP();
	}

void	Driver::response(	Oscl::Usb::Pipe::Bulk::
							OUT::Resp::Api::WriteResp&		msg
							) noexcept{
	_write.busy	= false;
	_write.pendingBulkWrite	= 0;
	if(_closing || !_open){
		return;
		}
	txNextBulk();
	}

void	Driver::response(	Oscl::Usb::Pipe::Bulk::
							OUT::Resp::Api::CancelResp&		msg
							) noexcept{
	cancelPendingBulkWrites();
	}

void	Driver::request(	Oscl::Stream::Input::
							Req::Api::ReadReq&		msg
							) noexcept{
	if(_closing || !_open){
		msg.returnToSender();
		return;
		}
	_pendingStreamInReqs.put(&msg);
	processRxStream();
	}

void	Driver::request(	Oscl::Stream::Input::
							Req::Api::CancelReq&		msg
							) noexcept{
	Oscl::Stream::Input::Req::Api::ReadReq*
	readToCancel	= _pendingStreamInReqs.remove(&msg._payload._readToCancel);
	if(readToCancel){
		readToCancel->returnToSender();
		}
	msg.returnToSender();
	}

void	Driver::request(	Oscl::Stream::Output::
							Req::Api::WriteReq&		msg
							) noexcept{
	if(_closing || !_open){
		msg.returnToSender();
		return;
		}
	_write.pendingReq.put(&msg);
	txNextBulk();
	}

void	Driver::request(	Oscl::Stream::Output::
							Req::Api::FlushReq&		msg
							) noexcept{
	if(_closing || !_open){
		msg.returnToSender();
		return;
		}
	// FIXME
	for(;;);
	}

void	Driver::request(	Oscl::UART::Req::
							Api::SetBaudRateReq&	msg
							) noexcept{
	if(_closing || !_open){
		msg._payload._success	= false;
		msg.returnToSender();
		return;
		}
	msg._payload._success	= setBaudRate(msg._payload._bitsPerSecond);
	msg.returnToSender();
	}

void	Driver::request(	Oscl::UART::Req::
							Api::NoParityReq&	msg
							) noexcept{
	if(_closing || !_open){
		msg._payload._success	= false;
		msg.returnToSender();
		return;
		}
	_parity	= parityNone;
	msg._payload._success	= updateCommParameters();
	msg.returnToSender();
	}

void	Driver::request(	Oscl::UART::Req::
							Api::EvenParityReq&	msg
							) noexcept{
	if(_closing || !_open){
		msg._payload._success	= false;
		msg.returnToSender();
		return;
		}
	_parity	= parityEven;
	msg._payload._success	= updateCommParameters();
	msg.returnToSender();
	}

void	Driver::request(	Oscl::UART::Req::
							Api::OddParityReq&	msg
							) noexcept{
	if(_closing || !_open){
		msg._payload._success	= false;
		msg.returnToSender();
		return;
		}
	_parity	= parityOdd;
	msg._payload._success	= updateCommParameters();
	msg.returnToSender();
	}

void	Driver::request(	Oscl::UART::Req::
							Api::MarkParityReq&	msg
							) noexcept{
	if(_closing || !_open){
		msg._payload._success	= false;
		msg.returnToSender();
		return;
		}
	_parity	= parityMark;
	msg._payload._success	= updateCommParameters();
	msg.returnToSender();
	}

void	Driver::request(	Oscl::UART::Req::
							Api::SpaceParityReq&	msg
							) noexcept{
	if(_closing || !_open){
		msg._payload._success	= false;
		msg.returnToSender();
		return;
		}
	_parity	= paritySpace;
	msg._payload._success	= updateCommParameters();
	msg.returnToSender();
	}

void	Driver::request(	Oscl::UART::Req::
							Api::SetStartBitsReq&	msg
							) noexcept{
	if(_closing || !_open){
		msg._payload._success	= false;
		msg.returnToSender();
		return;
		}
	if(msg._payload._nHalfBits != 2){
		msg._payload._success	= false;
		}
	else {
		msg._payload._success	= true;
		}
	msg.returnToSender();
	}

void	Driver::request(	Oscl::UART::Req::
							Api::SetStopBitsReq&	msg
							) noexcept{
	if(_closing || !_open){
		msg._payload._success	= false;
		msg.returnToSender();
		return;
		}
	switch(msg._payload._nHalfBits){
		case 2:
			_nStopBits	= stopBits1_0;
			msg._payload._success	= updateCommParameters();
			break;
		case 3:
			_nStopBits	= stopBits1_5;
			msg._payload._success	= updateCommParameters();
			break;
		case 4:
			_nStopBits	= stopBits2_0;
			msg._payload._success	= updateCommParameters();
			break;
		default:
			msg._payload._success	= false;
			}
	msg.returnToSender();
	}

void	Driver::request(	Oscl::UART::Req::
							Api::RtsCtsFlowControlReq&	msg
							) noexcept{
	if(_closing || !_open){
		msg._payload._success	= false;
		msg.returnToSender();
		return;
		}
	_enableHardwareFlowControl	= true;
	msg._payload._success	= updateCommParameters();
	msg.returnToSender();
	}

void	Driver::request(	Oscl::UART::Req::
							Api::ManualFlowControlReq&	msg
							) noexcept{
	if(_closing || !_open){
		msg._payload._success	= false;
		msg.returnToSender();
		return;
		}
	// FIXME: This should be possible
	msg._payload._success	= false;
	msg.returnToSender();
	}

void	Driver::request(	Oscl::UART::Req::
							Api::DisableFlowControlReq&	msg
							) noexcept{
	if(_closing || !_open){
		msg._payload._success	= false;
		msg.returnToSender();
		return;
		}
	// FIXME: This should be possible
	msg._payload._success	= false;
	msg.returnToSender();
	}

void	Driver::request(	Oscl::UART::Req::
							Api::AssertRtsReq&	msg
							) noexcept{
	if(_closing || !_open){
		msg._payload._success	= false;
		msg.returnToSender();
		return;
		}
	// FIXME: This should be possible
	msg._payload._success	= false;
	msg.returnToSender();
	}

void	Driver::request(	Oscl::UART::Req::
							Api::NegateRtsReq&	msg
							) noexcept{
	if(_closing || !_open){
		msg._payload._success	= false;
		msg.returnToSender();
		return;
		}
	// FIXME: This should be possible
	msg._payload._success	= false;
	msg.returnToSender();
	}

void	Driver::request(	Oscl::UART::Req::
							Api::AssertCtsReq&	msg
							) noexcept{
	if(_closing || !_open){
		msg._payload._success	= false;
		msg.returnToSender();
		return;
		}
	// FIXME: This should be possible
	msg._payload._success	= false;
	msg.returnToSender();
	}

void	Driver::request(	Oscl::UART::Req::
							Api::NegateCtsReq&	msg
							) noexcept{
	if(_closing || !_open){
		msg._payload._success	= false;
		msg.returnToSender();
		return;
		}
	// FIXME: This should be possible
	msg._payload._success	= false;
	msg.returnToSender();
	}

void	Driver::request(	Oscl::UART::Req::
							Api::DoThisReq&		msg
							) noexcept{
	if(_closing || !_open){
		msg._payload._success	= false;
		msg.returnToSender();
		return;
		}
	Oscl::Error::Info::log(msg._payload._cmd);
	Oscl::Error::Info::log("\n\r");
	msg._payload._success	= false;
	msg.returnToSender();
	}

void	Driver::request(Oscl::UART::DTE::Req::Api::AssertDtrReq& msg) noexcept{
	if(_closing || !_open){
		msg._payload._success	= false;
		msg.returnToSender();
		return;
		}
	// FIXME: This should be possible
	msg._payload._success	= false;
	msg.returnToSender();
	}

void	Driver::request(Oscl::UART::DTE::Req::Api::NegateDtrReq& msg) noexcept{
	if(_closing || !_open){
		msg._payload._success	= false;
		msg.returnToSender();
		return;
		}
	// FIXME: This should be possible
	msg._payload._success	= false;
	msg.returnToSender();
	}

/////// RxTrans
Driver::RxTrans::RxTrans(	Driver&						context,
							Oscl::Usb::Pipe::Bulk::
							IN::Req::Api::SAP&			bulkInPipe,
							Oscl::Mt::Itc::PostMsgApi&	myPapi,
							void*						packetMem,
							unsigned					maxPacketSize
							) noexcept:
		_context(context),
		_bulkInPipe(bulkInPipe),
		_myPapi(myPapi),
		_req(0),
		_cancelMem(0),
		_closing(false),
		_packetMem(packetMem),
		_maxPacketSize(maxPacketSize)
		{
	}

void	Driver::RxTrans::start() noexcept{
	Oscl::Usb::Pipe::Bulk::
	IN::Req::Api::ReadPayload*
	payload	= new(&_respMem.payload)
				Oscl::Usb::Pipe::Bulk::
				IN::Req::Api::ReadPayload(	_packetMem,
											_maxPacketSize
											);
	Oscl::Usb::Pipe::Bulk::
	IN::Resp::Api::ReadResp*
	resp	= new (&_respMem.resp)
				Oscl::Usb::Pipe::Bulk::
				IN::Resp::Api::ReadResp(	_bulkInPipe.getReqApi(),
											*this,
											_myPapi,
											*payload
											);
	_req	= &resp->getSrvMsg();
	_bulkInPipe.post(*_req);
	}

void	Driver::RxTrans::cancel(	Oscl::Usb::Pipe::Bulk::
									IN::Resp::CancelMem&		mem
									) noexcept{
	_cancelMem	= &mem;
	_closing	= true;
	Oscl::Usb::Pipe::Bulk::
	IN::Req::Api::CancelPayload*
	payload	= new(&mem.payload)
				Oscl::Usb::Pipe::Bulk::
				IN::Req::Api::CancelPayload(*_req);

	Oscl::Usb::Pipe::Bulk::
	IN::Resp::Api::CancelResp*
	resp	= new (&mem.resp)
				Oscl::Usb::Pipe::Bulk::
				IN::Resp::Api::CancelResp(	_bulkInPipe.getReqApi(),
											*this,
											_myPapi,
											*payload
											);
	_bulkInPipe.post(resp->getSrvMsg());
	}

void	Driver::RxTrans::response(	Oscl::Usb::Pipe::Bulk::
									IN::Resp::Api::ReadResp&		msg
									) noexcept{
	if(_closing) return;
	if(msg._payload._failed){
		_context.failed(*this);
		return;
		}
	advance(msg._payload._nRead);
	_context.done(*this);
	}

void	Driver::RxTrans::response(	Oscl::Usb::Pipe::Bulk::
									IN::Resp::Api::CancelResp&		msg
									) noexcept{
	_context.done(*_cancelMem);
	}

unsigned	Driver::RxTrans::bufferSize() const noexcept{
	return _maxPacketSize;
	}

const void*	Driver::RxTrans::getBuffer() const noexcept{
	return _packetMem;
	}

void*		Driver::RxTrans::buffer() noexcept{
	return _packetMem;
	}


