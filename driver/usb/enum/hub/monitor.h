/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_usb_enum_hub_monitorh_
#define _oscl_drv_usb_enum_hub_monitorh_
#include "match.h"
#include "oscl/driver/usb/device/dyndev.h"
#include "oscl/mt/itc/dyn/monitor/unit/monitor/monitor.h"
#include "oscl/driver/usb/hub/creator/part.h"

/** */
namespace Oscl {
/** */
namespace Usb {
/** */
namespace Enum {
/** */
namespace Hub {

/** This concrete class is a single USB HUB device monitor.
	It waits for a new USB hub to be created, claims the
	device, and then creates a new HUB driver for that device.
	When the device is removed from the USB hub device is removed
	from the USB bus, the resources for the HUB driver are reclaimed
	and the monitor waits for a new hub to be created.
	This class is responsible for creating exactly one USB HUB
	driver.

Patterned after:
/src/swenv/src/oscl/pcisrv/class/sbc/usb/ohci/monitor.h
/src/swenv/src/oscl/driver/usb/hub/port/driver/driver.h

 */
class Monitor :
		public Oscl::Mt::Itc::Dyn::Unit::Monitor<DynDevice>,
		private Oscl::Done::Api
		{
	private:
		/** */
		Oscl::Usb::Hub::Creator::Part			_hubCreator;
		/** */
		Oscl::Mt::Itc::Srv::
		Close::Resp::CloseMem					_closeMem;
		/** */
		Match									_match;
		
	public:
		/** */
		Monitor(	Oscl::Mt::Itc::Dyn::Adv::Cli::
					SyncApi<DynDevice>&						advFind,
					Oscl::Mt::Itc::Dyn::Adv::Cli::
					Req::Api<	DynDevice
								>::SAP&						advSAP,
					Oscl::Mt::Itc::Dyn::Adv::
					Creator::SyncApi<DynDevice>&			advCapi,
					Oscl::Mt::Itc::Dyn::Adv::
					Core<DynDevice>::ReleaseApi&			advRapi,
					Oscl::Mt::Itc::Delay::Req::Api::SAP&	delayServiceSAP,
					Oscl::Mt::Itc::PostMsgApi&				myPapi,
					Oscl::Mt::Itc::PostMsgApi&				hubPapi,
					Oscl::Mt::Itc::PostMsgApi&				dynSrvPapi,
					Oscl::Usb::Setup::Mem&					setupMem,
					Oscl::Usb::Hub::Creator::Part::
					PacketMem&								packetMem,
					Oscl::Usb::Hub::Adaptor::
					Driver::SetupMem&						adaptorSetupMem,
					Oscl::Usb::Hub::Adaptor::
					Driver::PacketMem&						adaptorPacketMem,
					Oscl::Usb::Hub::Creator::Part::
					PortDriverMem*							portDriverMem,
					Oscl::Usb::Hub::Creator::Part::
					PortAdaptorMem*							portAdaptorMem,
					Oscl::Usb::Hub::Port::Adaptor::
					Port::SetupMem*							portSetupMem,
					Oscl::Usb::Hub::Port::Adaptor::
					Port::PacketMem*						portPacketMem,
					Oscl::Usb::Hub::Port::
					Driver::SetupPktMem*					portDriverSetupMem,
					Oscl::Usb::Hub::Port::
					Driver::PacketMem*						portDriverPacketMem,
					unsigned								maxPorts,
					unsigned								oidType,
					const Oscl::ObjectID::RO::Api*			location=0
					) noexcept;
	private:
		/** */
		void	activate() noexcept;
		/** */
		void	deactivate() noexcept;
	private:	// Oscl::Done::Api
		/** */
		void	done() noexcept;
	};

}
}
}
}

#endif

