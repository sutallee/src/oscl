/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_usb_enum_hub_configh_
#define _oscl_drv_usb_enum_hub_configh_
#include <new>
#include "monitor.h"
#include "oscl/error/fatal.h"
#include "oscl/kernel/mmu.h"
#include "oscl/extalloc/api.h"

/** */
namespace Oscl {
/** */
namespace Usb {
/** */
namespace Enum {
/** */
namespace Hub {

/**	The purpose of this template class is to provide a configurable
	means of static allocation for USB Hub Monitor.
 */
template<
			unsigned maxPorts
			>
class Config {
	private:
		/** Memory for the actual monitor. The monitor is
			initialized (a la "new") in the body of the
			constructor such that "complex" memory calculations
			can be performed and dynamic memory allocated.
		 */
		Oscl::Memory::
		AlignedBlock<sizeof(Monitor)>			_monMem;

		/** */
		Oscl::Usb::Hub::Creator::
		Part::PortDriverMem					_portDriverMem[maxPorts];

		/** */
		Oscl::Usb::Hub::Creator::
		Part::PortAdaptorMem					_portAdaptorMem[maxPorts];

		/** */
		Oscl::ExtAlloc::Record					_dmaMemRec;
		/** This pointer is initialized during construction,
			and points to _monMem after the "new" operation
			is complete.
		 */
		Monitor*								_theMonitor;
		
	public:
		/** */
		Config(		Oscl::Mt::Itc::Dyn::Adv::Cli::
					SyncApi<DynDevice>&						advFind,
					Oscl::Mt::Itc::Dyn::Adv::Cli::
					Req::Api<DynDevice>::SAP&				advSAP,
					Oscl::Mt::Itc::Dyn::Adv::
					Creator::SyncApi<DynDevice>&			advCapi,
					Oscl::Mt::Itc::Dyn::Adv::
					Core<DynDevice>::ReleaseApi&			advRapi,
					Oscl::Mt::Itc::Delay::Req::Api::SAP&	delayServiceSAP,
					Oscl::Mt::Itc::PostMsgApi&				myPapi,
					Oscl::Mt::Itc::PostMsgApi&				hubPapi,
					Oscl::Mt::Itc::PostMsgApi&				dynSrvPapi,
					Oscl::ExtAlloc::Api&					usbMemAllocator,
					unsigned								oidType,
					const Oscl::ObjectID::RO::Api*			location=0
					) noexcept;
		/** */
		virtual ~Config(){}

		/** */
		Oscl::Mt::Itc::Srv::OpenCloseSyncApi&	getOpenCloseSyncApi() noexcept{
			return *_theMonitor;
			}
	};

/**
 */
template<	unsigned maxPorts
			>
Config<	maxPorts
		>::Config(	Oscl::Mt::Itc::Dyn::Adv::Cli::
					SyncApi<DynDevice>&						advFind,
					Oscl::Mt::Itc::Dyn::Adv::Cli::
					Req::Api<DynDevice>::SAP&				advSAP,
					Oscl::Mt::Itc::Dyn::Adv::
					Creator::SyncApi<DynDevice>&			advCapi,
					Oscl::Mt::Itc::Dyn::Adv::
					Core<DynDevice>::ReleaseApi&			advRapi,
					Oscl::Mt::Itc::Delay::Req::Api::SAP&	delayServiceSAP,
					Oscl::Mt::Itc::PostMsgApi&				myPapi,
					Oscl::Mt::Itc::PostMsgApi&				hubPapi,
					Oscl::Mt::Itc::PostMsgApi&				dynSrvPapi,
					Oscl::ExtAlloc::Api&					usbMemAllocator,
					unsigned								oidType,
					const Oscl::ObjectID::RO::Api*			location
					) noexcept
		{
	const unsigned long	pageSize			= OsclKernelGetPageSize();
	const unsigned long	almostPageSize		= pageSize-1;

	const unsigned long	monitorSetupPktMemSize		=
		(sizeof(Oscl::Usb::Setup::Mem));

	const unsigned long	monitorPacketMemSize		=
		(sizeof(Oscl::Usb::Hub::Creator::Part::PacketMem));

	const unsigned long	hubAdaptorSetupPktMemSize		=
		(sizeof(Oscl::Usb::Hub::Adaptor::Driver::SetupMem));

	const unsigned long	hubAdaptorPacketMemSize		=
		(sizeof(Oscl::Usb::Hub::Adaptor::Driver::PacketMem));

	const unsigned long	portAdaptorSetupPktMemSize		=
		(maxPorts*sizeof(Oscl::Usb::Hub::Port::Adaptor::Port::SetupMem));

	const unsigned long	portAdaptorPacketMemSize		=
		(maxPorts*sizeof(Oscl::Usb::Hub::Port::Adaptor::Port::PacketMem));

	const unsigned long	portDriverSetupMemSize		=
		(maxPorts*sizeof(Oscl::Usb::Hub::Port::Driver::SetupPktMem));

	const unsigned long	portDriverPacketMemSize		=
		(maxPorts*sizeof(Oscl::Usb::Hub::Port::Driver::PacketMem));


	const unsigned long	dmaMemSize		=
		(		monitorSetupPktMemSize
			+	monitorPacketMemSize
			+	hubAdaptorSetupPktMemSize
			+	hubAdaptorPacketMemSize
			+	portAdaptorSetupPktMemSize
			+	portAdaptorPacketMemSize
			+	portDriverSetupMemSize
			+	portDriverPacketMemSize
			);

	const unsigned long	nPages		=	(dmaMemSize+almostPageSize)/pageSize;
	const unsigned long	allocSize	=	pageSize*nPages;

	if(!usbMemAllocator.alloc(_dmaMemRec,allocSize,~almostPageSize)){
		Oscl::ErrorFatal::logAndExit(
			"Oscl::Usb::Enum::Hub::Config: cant allocate packet buffers."
			);
		}

	Oscl::Usb::Setup::Mem*
	monitorSetupMem	= (Oscl::Usb::Setup::Mem*)_dmaMemRec.getFirstUnit();

	Oscl::Usb::Hub::Creator::Part::PacketMem*
	monitorPacketMem = (Oscl::Usb::Hub::Creator::Part::PacketMem*)
		(((unsigned long)monitorSetupMem) + monitorSetupPktMemSize);

	Oscl::Usb::Hub::Adaptor::Driver::SetupMem*
	hubAdaptorSetupPktMem	= (Oscl::Usb::Hub::Adaptor::Driver::SetupMem*)
		(((unsigned long)monitorPacketMem) + monitorPacketMemSize);
		
	Oscl::Usb::Hub::Adaptor::Driver::PacketMem*
	hubAdaptorPacketMem	= (Oscl::Usb::Hub::Adaptor::Driver::PacketMem*)
		(((unsigned long)hubAdaptorSetupPktMem) + hubAdaptorSetupPktMemSize);
		
	Oscl::Usb::Hub::Port::Adaptor::Port::SetupMem*
	portAdaptorSetupPktMem	= (Oscl::Usb::Hub::Port::Adaptor::Port::SetupMem*)
		(((unsigned long)hubAdaptorPacketMem) + hubAdaptorPacketMemSize);

	Oscl::Usb::Hub::Port::Adaptor::Port::PacketMem*
	portAdaptorPacketMem	= (Oscl::Usb::Hub::Port::Adaptor::Port::PacketMem*)
		(((unsigned long)portAdaptorSetupPktMem) + portAdaptorSetupPktMemSize);

	Oscl::Usb::Hub::Port::Driver::SetupPktMem*
	portDriverSetupMem	= (Oscl::Usb::Hub::Port::Driver::SetupPktMem*)
		(((unsigned long)portAdaptorPacketMem) + portAdaptorPacketMemSize);

	Oscl::Usb::Hub::Port::Driver::PacketMem*
	portDriverPacketMem	= (Oscl::Usb::Hub::Port::Driver::PacketMem*)
		(((unsigned long)portDriverSetupMem) + portDriverSetupMemSize);

	_theMonitor	= new(&_monMem)	Monitor(	advFind,
											advSAP,
											advCapi,
											advRapi,
											delayServiceSAP,
											myPapi,
											hubPapi,
											dynSrvPapi,
											*monitorSetupMem,
											*monitorPacketMem,
											*hubAdaptorSetupPktMem,
											*hubAdaptorPacketMem,
											_portDriverMem,
											_portAdaptorMem,
											portAdaptorSetupPktMem,
											portAdaptorPacketMem,
											portDriverSetupMem,
											portDriverPacketMem,
											maxPorts,
											oidType,
											location
											);
	}

}
}
}
}

#endif
