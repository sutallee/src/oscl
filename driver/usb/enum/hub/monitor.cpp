/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "monitor.h"

using namespace Oscl::Usb::Enum::Hub;

Monitor::Monitor(	Oscl::Mt::Itc::Dyn::Adv::Cli::
					SyncApi<DynDevice>&						advFind,
					Oscl::Mt::Itc::Dyn::Adv::Cli::
					Req::Api<	DynDevice
								>::SAP&						advSAP,
					Oscl::Mt::Itc::Dyn::Adv::
					Creator::SyncApi<DynDevice>&			advCapi,
					Oscl::Mt::Itc::Dyn::Adv::
					Core<DynDevice>::ReleaseApi&			advRapi,
					Oscl::Mt::Itc::Delay::Req::Api::SAP&	delayServiceSAP,
					Oscl::Mt::Itc::PostMsgApi&				myPapi,
					Oscl::Mt::Itc::PostMsgApi&				hubPapi,
					Oscl::Mt::Itc::PostMsgApi&				dynSrvPapi,
					Oscl::Usb::Setup::Mem&					setupMem,
					Oscl::Usb::Hub::Creator::Part::
					PacketMem&								packetMem,
					Oscl::Usb::Hub::Adaptor::
					Driver::SetupMem&						adaptorSetupMem,
					Oscl::Usb::Hub::Adaptor::
					Driver::PacketMem&						adaptorPacketMem,
					Oscl::Usb::Hub::Creator::Part::
					PortDriverMem*							portDriverMem,
					Oscl::Usb::Hub::Creator::Part::
					PortAdaptorMem*							portAdaptorMem,
					Oscl::Usb::Hub::Port::Adaptor::
					Port::SetupMem*							portSetupMem,
					Oscl::Usb::Hub::Port::Adaptor::
					Port::PacketMem*						portPacketMem,
					Oscl::Usb::Hub::Port::
					Driver::SetupPktMem*					portDriverSetupMem,
					Oscl::Usb::Hub::Port::
					Driver::PacketMem*						portDriverPacketMem,
					unsigned								maxPorts,
					unsigned								oidType,
					const Oscl::ObjectID::RO::Api*			location
					) noexcept:
		Oscl::Mt::Itc::Dyn::Unit::Monitor<DynDevice>(	advSAP,
														advFind,
														myPapi,
														_match
														),
		_hubCreator(	advSAP,
						advCapi,
						advRapi,
						delayServiceSAP,
						myPapi,
						hubPapi,
						dynSrvPapi,
						setupMem,
						packetMem,
						adaptorSetupMem,
						adaptorPacketMem,
						portDriverMem,
						portAdaptorMem,
						portSetupMem,
						portPacketMem,
						portDriverSetupMem,
						portDriverPacketMem,
						maxPorts,
						oidType
						),
		_match(location)
		{
	}

void	Monitor::activate() noexcept{
	_hubCreator.start(_handle);
	}

void	Monitor::deactivate() noexcept{
	_hubCreator.stop(*this);
	}

void	Monitor::done() noexcept{
	deactivateDone();
	}

