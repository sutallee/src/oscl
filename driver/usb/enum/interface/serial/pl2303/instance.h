/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_usb_enum_interface_serial_pl2303_instanceh_
#define _oscl_drv_usb_enum_interface_serial_pl2303_instanceh_
#include "oscl/driver/usb/enum/interface/api.h"
#include "oscl/mt/itc/dyn/monitor/unit/monitor/monitor.h"
#include "oscl/driver/usb/setup/mem.h"
#include "oscl/driver/usb/desc/config.h"
#include "oscl/driver/usb/desc/interface.h"
#include "oscl/driver/usb/desc/endpoint.h"
#include "oscl/mt/itc/srv/crespmem.h"
#include "oscl/oid/roapi.h"

#include "oscl/driver/usb/serial/pl2303/driver.h"
#include "oscl/driver/usb/desc/serial/pl2303.h"
#include "oscl/uart/dyn/dte/device.h"
#include "oscl/mt/itc/dyn/adv/creatorsync.h"

/** */
namespace Oscl {
/** */
namespace Usb {
/** */
namespace Enum {
/** */
namespace Interface {
/** */
namespace Serial {
/** */
namespace PL2303 {

/** This concrete class creates an instance of a USB PL2303
	serial device. 
 */
class Instance :	public	Oscl::Usb::Enum::Interface::Api,
					private	Oscl::Mt::Itc::Srv::Close::Resp::Api
					{
	public:
		/** */
		struct RawMem {
			/** PL2303 Desc Mem */
			/** Standard Desc Mem */
			unsigned char	mem[		sizeof(Oscl::Usb::Desc::Interface)
									+	(3*sizeof(Oscl::Usb::Desc::Endpoint))
									];
			};
		/** */
		typedef Oscl::Memory::
				AlignedBlock<sizeof(RawMem)>		DescMem;
		/** */
		union PacketMem {
			/** */
			void*								__qitemlink;
			/** */
			DescMem								_descMem;
			};

	private:	// Interface specific memory for created parts
		/** */
		Oscl::Memory::
		AlignedBlock<	sizeof(	Oscl::Usb::Serial::
								PL2303::Driver
								)
							>					_driverMem;
		/** */
		Oscl::Memory::
		AlignedBlock<	sizeof(	Oscl::UART::
								Dyn::DTE::Device
								)
							>					_deviceMem;

	private:	// Interface specific constructor specified variables
		/** */
		Oscl::Usb::Setup::Mem&					_setupMem;
		/** */
		PacketMem&								_packetMem;
		/** */
		Oscl::Usb::Serial::PL2303::
		Driver::SetupMem*						_driverSetupMem;
		/** */
		Oscl::Usb::Serial::PL2303::
		Driver::PacketMem*						_driverPacketMem;
		/** */
		const unsigned							_oidType;
		/** */
		Oscl::Mt::Itc::PostMsgApi&				_driverPapi;
		/** */
		Oscl::Mt::Itc::Dyn::Adv::Creator::
		SyncApi<	Oscl::UART::
					Dyn::DTE::Device
					>&							_uartDteAdvCapi;
		/** */
		Oscl::Mt::Itc::PostMsgApi&      		_uartDteDynDevPapi;
		/** */
		Oscl::Mt::Itc::Dyn::Adv::
		Core<	Oscl::UART::
				Dyn::DTE::Device
				>::ReleaseApi&					_uartDteDynAdvRapi;
		/** */
		Oscl::Mt::Itc::PostMsgApi&				_uartDteDynAdvPapi;

	private:	// Interface specific "created" variables
		/** */
		Oscl::Usb::Alloc::
		BW::Interrupt::BwRec*					_bwRec;
		/** */
		Oscl::Usb::Pipe::
		Interrupt::IN::PipeApi*					_statusChangePipe;
		/** */
		Oscl::Usb::Pipe::
		Bulk::IN::PipeApi*						_bulkInPipe;
		/** */
		Oscl::Usb::Pipe::
		Bulk::OUT::PipeApi*						_bulkOutPipe;
		/** */
		bool									_driverOpen;
		/** */
		Oscl::Usb::Serial::PL2303::Driver*		_driver;
		/** */
		Oscl::UART::Dyn::DTE::Device*			_device;
		/** */
		unsigned char							_maxChangePacketSize;
		/** */
		unsigned char							_maxBulkInPacketSize;
		/** */
		unsigned char							_maxBulkOutPacketSize;
		/** */
		unsigned								_reservedPower;

	private:	// Generic "Instance" passed in through start()/stop()
		/** */
		Oscl::Mt::Itc::PostMsgApi*				_myPapi;
		/** */
		Oscl::Usb::HCD::Enum::Monitor::Api*		_hcdMonApi;
		/** */
		Oscl::Usb::Pipe::Message::PipeApi*		_ctrlPipe;
		/** */
		Oscl::Usb::Address						_usbAddress;
		/** */
		bool									_lowSpeedDevice;
		/** */
		Oscl::Mt::Itc::Srv::
		Close::Resp::CloseMem*					_closeMem;
		/** */
		StopDoneApi*							_sdapi;
		
	public:
		/** */
		Instance(	Oscl::Mt::Itc::PostMsgApi&				driverPapi,
					Oscl::Mt::Itc::Dyn::Adv::Creator::
					SyncApi<	Oscl::UART::
								Dyn::DTE::Device
								>&							uartDteAdvCapi,
					Oscl::Mt::Itc::PostMsgApi&      		uartDteDynDevPapi,
					Oscl::Mt::Itc::Dyn::Adv::
					Core<	Oscl::UART::
							Dyn::DTE::Device
							>::ReleaseApi&					uartDteDynAdvRapi,
					Oscl::Mt::Itc::PostMsgApi&				uartDteDynAdvPapi,
					Oscl::Usb::Setup::Mem&					setupMem,
					PacketMem&								packetMem,
					Oscl::Usb::Serial::PL2303::
					Driver::SetupMem*						driverSetupMem,
					Oscl::Usb::Serial::PL2303::
					Driver::PacketMem*						driverPacketMem,
					unsigned								oidType,
					const Oscl::ObjectID::RO::Api*			location
					) noexcept;
	private:
		/** */
		void	releaseResources() noexcept;

	private: // helpers
		/** */
		Oscl::Usb::Desc::Interface*
			getInterface(	Oscl::Usb::Desc::
							Config*			config,
							unsigned char	interfaceNumber
							) noexcept;
		/** */
		bool	getEndpoints(	Oscl::Usb::Desc::Config*	config,
								Oscl::Usb::Desc::Endpoint**	change,
								Oscl::Usb::Desc::Endpoint**	bulkIn,
								Oscl::Usb::Desc::Endpoint**	bulkOut
								) noexcept;
		/** */
		bool	allocateBandwidth(Oscl::Usb::Desc::Endpoint& desc) noexcept;
		/** */
		bool	allocateInterruptInPipe(	Oscl::Usb::Desc::
											Endpoint&			desc
											) noexcept;
		/** */
		bool	allocateBulkInPipe(	Oscl::Usb::Desc::
									Endpoint&			desc
									) noexcept;
		/** */
		bool	allocateBulkOutPipe(	Oscl::Usb::Desc::
										Endpoint&			desc
										) noexcept;
		/** */
		bool	setPL2303Configuration(Oscl::Usb::Desc::Config& desc) noexcept;
		/** */
		bool	readEndpointStatus(unsigned endpointID) noexcept;
		/** */
		void	createDriver(const Oscl::ObjectID::RO::Api& location) noexcept;
		/** */
		void	closeDriver() noexcept;
		/** */
		void	closeDevice() noexcept;

	private:	// Oscl::Mt::Itc::Srv::Close::Resp::Api
		/** */
		void	response(	Oscl::Mt::Itc::Srv::Close::
							Resp::Api::CloseResp&		msg
							) noexcept;
		/** */
		void	response(	Oscl::Mt::Itc::Srv::Open::
							Resp::Api::OpenResp&		msg
							) noexcept;

	private: // Oscl::Usb::Enum::Interface::Api
		/** */
		void	start(	Oscl::Mt::Itc::PostMsgApi&		myPapi,
						Oscl::Usb::Pipe::
						Message::PipeApi&				epzPipe,
						Oscl::Usb::HCD::Enum::
						Monitor::Api&					hcdMonApi,
						Oscl::Usb::Desc::Config&		configDesc,
						Oscl::Usb::Address				usbAddress,
						bool							lowSpeedDevice,
						unsigned char					interfaceNumber,
						const Oscl::ObjectID::RO::Api*	location
						) noexcept;
		/** */
		void	stop(	StopDoneApi&				sdapi,
						Oscl::Mt::Itc::Srv::
						Close::Resp::CloseMem&		closeMem
						) noexcept;
		/** */
		bool	match(	Oscl::Usb::Pipe::
						Message::SyncApi&			epzSyncApi,
						Oscl::Usb::Desc::Config&	configDesc,
						uint16_t					deviceIdVendor,
						uint16_t					deviceIdProduct,
						uint16_t					deviceBcdDevice,
						uint8_t					deviceManufacturer,
						uint8_t					deviceProduct,
						uint8_t					deviceSerialNumber,
						unsigned char				interfaceNumber
						) noexcept;
	};

}
}
}
}
}
}

#endif

