/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "instance.h"
#include "oscl/hw/usb/desc/ifreg.h"
#include "oscl/driver/usb/setup/setconfig.h"
#include "oscl/driver/usb/setup/getepstatus.h"
#include "oscl/error/info.h"
#include "oscl/error/fatal.h"
#include "oscl/oid/fixed.h"

using namespace Oscl::Usb::Enum::Interface::Serial::PL2303;

Instance::Instance(	Oscl::Mt::Itc::PostMsgApi&				driverPapi,
					Oscl::Mt::Itc::Dyn::Adv::Creator::
					SyncApi<	Oscl::UART::
								Dyn::DTE::Device
								>&							uartDteAdvCapi,
					Oscl::Mt::Itc::PostMsgApi&      		uartDteDynDevPapi,
					Oscl::Mt::Itc::Dyn::Adv::
					Core<	Oscl::UART::
							Dyn::DTE::Device
							>::ReleaseApi&					uartDteDynAdvRapi,
					Oscl::Mt::Itc::PostMsgApi&				uartDteDynAdvPapi,
					Oscl::Usb::Setup::Mem&					setupMem,
					PacketMem&								packetMem,
					Oscl::Usb::Serial::PL2303::
					Driver::SetupMem*						driverSetupMem,
					Oscl::Usb::Serial::PL2303::
					Driver::PacketMem*						driverPacketMem,
					unsigned								oidType,
					const Oscl::ObjectID::RO::Api*			location
					) noexcept:
		_setupMem(setupMem),
		_packetMem(packetMem),
		_driverSetupMem(driverSetupMem),
		_driverPacketMem(driverPacketMem),
		_oidType(oidType),
		_driverPapi(driverPapi),
		_uartDteAdvCapi(uartDteAdvCapi),
		_uartDteDynDevPapi(uartDteDynDevPapi),
		_uartDteDynAdvRapi(uartDteDynAdvRapi),
		_uartDteDynAdvPapi(uartDteDynAdvPapi),
		_bwRec(0),
		_statusChangePipe(0),
		_bulkInPipe(0),
		_bulkOutPipe(0),
		_driverOpen(false),
		_driver(0),
		_device(0),
		_maxChangePacketSize(0),
		_maxBulkInPacketSize(0),
		_maxBulkOutPacketSize(0),
		_reservedPower(0),
		_myPapi(0),
		_hcdMonApi(0),
		_ctrlPipe(0),
		_usbAddress(0),
		_lowSpeedDevice(false),
		_closeMem(0),
		_sdapi(0)
		{
	}

void	Instance::releaseResources() noexcept{
	if(_bulkInPipe){
		_hcdMonApi->
		getBulkInPipeAllocSyncApi().
		free(*_bulkInPipe);
		}
	if(_bulkOutPipe){
		_hcdMonApi->
		getBulkOutPipeAllocSyncApi().
		free(*_bulkOutPipe);
		}
	if(_statusChangePipe){
		_hcdMonApi->
		getInterruptInPipeAllocSyncApi().
		free(*_statusChangePipe);
		}
	if(_bwRec){
		_hcdMonApi->
		getBwInterruptAllocSyncApi().
		free(*_bwRec);
		}
	_sdapi->stopDone(*this);
	}

Oscl::Usb::Desc::Interface*
Instance::getInterface(	Oscl::Usb::Desc::
						Config*			config,
						unsigned char	interfaceNumber
						) noexcept{
	unsigned char	nInterfaces	= config->_bNumInterfaces;
	using namespace Oscl::Usb::Hw::Desc;
	for(unsigned char i=0;i<nInterfaces;++i){
		Oscl::Usb::Desc::Descriptor*
		desc	= config->findDescriptor(	bDescriptorType::INTERFACE,
											i
											);
		if(!desc){
			// FIXME: _bNumInterfaces lied?
			while(true);
			}
		Oscl::Usb::Desc::Interface*
		interface	= (Oscl::Usb::Desc::Interface*)desc;
		if(interface->_bInterfaceNumber == interfaceNumber){
			return interface;
			}
		}

	return 0;
	}

bool
Instance::getEndpoints(	Oscl::Usb::Desc::Config*	config,
						Oscl::Usb::Desc::Endpoint**	change,
						Oscl::Usb::Desc::Endpoint**	bulkIn,
						Oscl::Usb::Desc::Endpoint**	bulkOut
						) noexcept{
	enum{nEndpointsDescs=3};
	using namespace Oscl::Usb::Hw::Desc;

	// First find all three descriptors
	*change		= 0;
	*bulkIn		= 0;
	*bulkOut	= 0;
	for(unsigned i=0;i<nEndpointsDescs;++i){
		Oscl::Usb::Desc::Endpoint*
		ep	=	(Oscl::Usb::Desc::Endpoint*)
				config->findDescriptor(	bDescriptorType::ENDPOINT,
										i,
										sizeof(DescMem)
										);
		if(!ep){
			// No Endpoint Descriptor found
			Oscl::Error::Info::log(
				"Oscl::Usb::Enum::Interface::Serial::PL2303:\n"
				"not enough endpoint descriptors.\n"
				);
			return false;
			}
		if(ep->_bmAttributes.equal(	Endpoint::bmAttributes::
									TransferType::FieldMask,
									Endpoint::bmAttributes::
									TransferType::ValueMask_Interrupt
									)
				){
			*change	= ep;
			}
		else if(ep->_bmAttributes.equal(	Endpoint::bmAttributes::
											TransferType::FieldMask,
											Endpoint::bmAttributes::
											TransferType::ValueMask_Bulk
											)
				){
			if(ep->_bEndpointAddress.equal(	Endpoint::bEndpointAddress::
											Direction::FieldMask,
											Endpoint::bEndpointAddress::
											Direction::ValueMask_OUT
											)
					){
				*bulkOut	= ep;
				}
			else {
				*bulkIn		= ep;
				}
			}
		else {
			// No Endpoint Descriptor found
			Oscl::Error::Info::log(	"Oscl::Usb::Enum::Interface::Serial::PL2303:\n"
									"unexpected endpoint descriptor type.\n"
									);
			return false;
			}
		}

	if(*change && *bulkIn && *bulkOut){
		return true;
		}
	// No Endpoint Descriptor found
	Oscl::Error::Info::log(	"Oscl::Usb::Enum::Interface::Serial::PL2303:\n"
							"cant find all expected descriptors.\n"
							);
	return false;
	}

void	Instance::closeDriver() noexcept{
	Oscl::Mt::Itc::Srv::Close::Req::Api::ClosePayload*
	payload	= new(&_closeMem->payload)
				Oscl::Mt::Itc::Srv::Close::
				Req::Api::ClosePayload();
	Oscl::Mt::Itc::Srv::Close::Resp::Api::CloseResp*
	resp	= new(&_closeMem->resp)
				Oscl::Mt::Itc::Srv::Close::
				Resp::Api::CloseResp(	_driver->getSAP().getReqApi(),
										*this,
										*_myPapi,
										*payload
										);
	_driver->getSAP().post(resp->getSrvMsg());
	}

void	Instance::closeDevice() noexcept{
	Oscl::Mt::Itc::Srv::Close::Req::Api::ClosePayload*
	payload	= new(&_closeMem->payload)
				Oscl::Mt::Itc::Srv::Close::
				Req::Api::ClosePayload();
	Oscl::Mt::Itc::Srv::Close::Resp::Api::CloseResp*
	resp	= new(&_closeMem->resp)
				Oscl::Mt::Itc::Srv::Close::
				Resp::Api::CloseResp(	_device->getSAP().getReqApi(),
										*this,
										*_myPapi,
										*payload
										);
	_device->getSAP().post(resp->getSrvMsg());
	}

void	Instance::start(	Oscl::Mt::Itc::PostMsgApi&		myPapi,
							Oscl::Usb::Pipe::
							Message::PipeApi&				epzPipe,
							Oscl::Usb::HCD::Enum::
							Monitor::Api&					hcdMonApi,
							Oscl::Usb::Desc::Config&		configDesc,
							Oscl::Usb::Address				usbAddress,
							bool							lowSpeedDevice,
							unsigned char					interfaceNumber,
							const Oscl::ObjectID::RO::Api*	location
							) noexcept{
	_myPapi			= &myPapi;
	_hcdMonApi		= &hcdMonApi;
	_ctrlPipe		= &epzPipe;
	_usbAddress		= usbAddress;
	_lowSpeedDevice	= lowSpeedDevice;

	Oscl::Usb::Desc::Interface*
	interfaceDesc	= getInterface(&configDesc,interfaceNumber);
	if(!interfaceDesc) return;

	if(interfaceDesc->_bNumEndpoints != 3){
		Oscl::Error::Info::log(	"Oscl::Usb::Enum::Interface::Serial::PL2303:"
								"expected 3 endpoints.\n"
								);
		return;
		}

	Oscl::Usb::Desc::Endpoint*	changeED;
	Oscl::Usb::Desc::Endpoint*	bulkInED;
	Oscl::Usb::Desc::Endpoint*	bulkOutED;
	if(!getEndpoints(	&configDesc,
						&changeED,
						&bulkInED,
						&bulkOutED
						)
			){
		return;
		}

	unsigned char
	endpointID	=	changeED->getEndpointID();
	if(endpointID == 0){
		Oscl::Error::Info::log(	"Oscl::Usb::Enum::Interface::Serial::PL2303:"
								"Invalid change endpoint ID.\n"
								);
		return;
		}

	endpointID	=	bulkInED->getEndpointID();
	if(endpointID == 0){
		Oscl::Error::Info::log(	"Oscl::Usb::Enum::Interface::Serial::PL2303:"
								"Invalid bulk IN endpoint ID.\n"
								);
		return;
		}

	endpointID	=	bulkOutED->getEndpointID();
	if(endpointID == 0){
		Oscl::Error::Info::log(	"Oscl::Usb::Enum::Interface::Serial::PL2303:"
								"Invalid bulk OUT endpoint ID.\n"
								);
		return;
		}

	_maxChangePacketSize	= changeED->_wMaxPacketSize;
	_maxBulkInPacketSize	= bulkInED->_wMaxPacketSize;
	_maxBulkOutPacketSize	= bulkOutED->_wMaxPacketSize;

	if(!allocateBandwidth(*changeED)) return;

	if(!allocateInterruptInPipe(*changeED)) return;

	if(!allocateBulkInPipe(*bulkInED)) return;

	if(!allocateBulkOutPipe(*bulkOutED)) return;

	if(!setPL2303Configuration(configDesc)) return;

	if(!readEndpointStatus(changeED->getEndpointID())) return;
	if(!readEndpointStatus(bulkInED->getEndpointID())) return;
	if(!readEndpointStatus(bulkOutED->getEndpointID())) return;

	Oscl::ObjectID::Fixed<32>	loc;
	if(location){
		Oscl::ObjectID::Api&	l	= loc;
		l	= *location;
		}
	loc	+= _oidType;
	loc	+= interfaceNumber;
	createDriver(loc);
	}

void	Instance::stop(	StopDoneApi&				sdapi,
						Oscl::Mt::Itc::Srv::
						Close::Resp::CloseMem&		closeMem
						) noexcept{
	_closeMem	= &closeMem;
	_sdapi		= &sdapi;

	// Asynchronously issue CloseReq to
	// PL2303. When finished,
	// release power,bw, and pipe resources
	// to HCD (e.g. releaseResources()) and finally
	// invoke deactivateDone() on self
	// (Dyn::Unit::Monitor<Oscl::Usb::DynDevice>)
	// NOTE: To implement the CloseReq,
	// this class must either implement the
	// Close::Resp::Api or create a new type
	// of transaction that does.
	closeDriver();
	}

bool	Instance::match(	Oscl::Usb::Pipe::
							Message::SyncApi&			epzSyncApi,
							Oscl::Usb::Desc::Config&	configDesc,
							uint16_t					deviceIdVendor,
							uint16_t					deviceIdProduct,
							uint16_t					deviceBcdDevice,
							uint8_t					deviceManufacturer,
							uint8_t					deviceProduct,
							uint8_t					deviceSerialNumber,
							unsigned char				interfaceNumber
							) noexcept{
	// FIXME: implement
	// 1. get interface descriptor by interface number.
	// 2. if class subclass and protocol match return true
	// 3. else return false
	// PL2303 match
	using namespace Oscl::Usb::Hw::Desc::Interface;
	Oscl::Usb::Desc::Interface*
	interface	= getInterface(&configDesc,interfaceNumber);
	if(!interface){
		// FIXME: Logic error
		// match should not have been given a bogus interface number.
		while(true);
		}
	if(deviceIdVendor != 0x0557){
		return false;
		}
	if(deviceIdProduct != 0x2008){
		return false;
		}
	if(interface->_bInterfaceClass != bInterfaceClass::VendorSpecific){
		return false;
		}
//	if(interface->_bInterfaceSubClass != bInterfaceClass::Future){
//		return false;
//		}
//	if(interface->_bInterfaceProtocol != bInterfaceProtocol::StandardProtocol){
//		return false;
//		}
	return true;
	}

bool	Instance::allocateBandwidth(Oscl::Usb::Desc::Endpoint& desc) noexcept{
	unsigned long	bwFreeState;
	_bwRec	=	_hcdMonApi->
				getBwInterruptAllocSyncApi().
				reserve(	desc._wMaxPacketSize,
							desc._bInterval,
							desc.directionIsIN(),
							bwFreeState
							);
	if(!_bwRec){
		Oscl::Error::Info::log(	"Oscl::Usb::Enum::Interface::Serial::PL2303:\n"
								"Not enough bandwidth available.\n"
								);
		return false;
		}
	return true;
	}

bool	Instance::allocateInterruptInPipe(Oscl::Usb::Desc::Endpoint& desc) noexcept{
	using namespace Oscl::Usb::Hw::Desc::Endpoint::bEndpointAddress;

	unsigned char	endpointID	= desc.getEndpointID();
	_statusChangePipe	=
	_hcdMonApi->
	getInterruptInPipeAllocSyncApi().
	alloc(	*_bwRec,
			_usbAddress,
			endpointID,
			desc._wMaxPacketSize,
			_lowSpeedDevice
			);
	if(!_statusChangePipe){
		Oscl::Error::Info::log(	"Oscl::Usb::Enum::Interface::Serial::PL2303:\n"
								"Cant allocate interrupt IN pipe.\n"
								);
		return false;
		}
	return true;
	}

bool	Instance::allocateBulkInPipe(Oscl::Usb::Desc::Endpoint& desc) noexcept{
	using namespace Oscl::Usb::Hw::Desc::Endpoint::bEndpointAddress;

	unsigned char	endpointID	= desc.getEndpointID();
	_bulkInPipe	=
	_hcdMonApi->
	getBulkInPipeAllocSyncApi().
	alloc(	_usbAddress,
			endpointID,
			desc._wMaxPacketSize,
			_lowSpeedDevice
			);
	if(!_bulkInPipe){
		Oscl::Error::Info::log(	"Oscl::Usb::Enum::Interface::Serial::PL2303:\n"
								"Cant allocate bulk IN pipe.\n"
								);
		return false;
		}
	return true;
	}

bool	Instance::allocateBulkOutPipe(Oscl::Usb::Desc::Endpoint& desc) noexcept{
	using namespace Oscl::Usb::Hw::Desc::Endpoint::bEndpointAddress;

	unsigned char	endpointID	= desc.getEndpointID();
	_bulkOutPipe	=
	_hcdMonApi->
	getBulkOutPipeAllocSyncApi().
	alloc(	_usbAddress,
			endpointID,
			desc._wMaxPacketSize,
			_lowSpeedDevice
			);
	if(!_bulkOutPipe){
		Oscl::Error::Info::log(	"Oscl::Usb::Enum::Interface::Serial::PL2303:\n"
								"Cant allocate bulk OUT pipe.\n"
								);
		return false;
		}
	return true;
	// FIXME:
	for(;;);
	return true;
	}

bool	Instance::setPL2303Configuration(Oscl::Usb::Desc::Config& desc) noexcept{
	Oscl::Usb::Pipe::Message::SyncApi&
	syncPipe	= _ctrlPipe->getSyncApi();
	bool	failed;

	Oscl::Usb::Setup::SetConfiguration*
	setConfigSetup	= new(&_setupMem)
						Oscl::Usb::Setup::
						SetConfiguration(desc._bConfigurationValue);

	failed	= syncPipe.control(*setConfigSetup);
	if(failed){
		Oscl::Error::Info::log(	"Oscl::Usb::Enum::Interface::Serial::PL2303:\n"
								"Error trying to set device configuration.\n"
								);
		return false;
		}
	return true;
	}

bool	Instance::readEndpointStatus(unsigned endpointID) noexcept{
	Oscl::Usb::Pipe::Message::SyncApi&
	syncPipe	= _ctrlPipe->getSyncApi();
	bool	failed;

	unsigned char*	rawMem	= (unsigned char*)&_packetMem._descMem;

	Oscl::Usb::Setup::GetEndpointStatus*
	getEpStatusSetup	= new(&_setupMem)
							Oscl::Usb::Setup::
							GetEndpointStatus(endpointID);

	failed	= syncPipe.read(*getEpStatusSetup,rawMem);
	if(failed){
		Oscl::Error::Info::log(	"Oscl::Usb::Enum::Interface::Serial::PL2303:\n"
								"Error reading endpoint status.\n"
								);
		return false;
		}
	return true;
	}

void	Instance::createDriver(const Oscl::ObjectID::RO::Api& location) noexcept{
	_driver	= new (&_driverMem)
				Oscl::Usb::Serial::PL2303::
				Driver(	*_ctrlPipe,
						_statusChangePipe->getSAP(),
						_bulkInPipe->getSAP(),
						_bulkOutPipe->getSAP(),
						_driverPapi,
						*_driverSetupMem,
						*_driverPacketMem,
						_maxChangePacketSize,
						_maxBulkInPacketSize,
						_maxBulkOutPacketSize
						);
	_driver->syncOpen();
	_driverOpen	= true;
	_device	=	new(&_deviceMem)
					Oscl::UART::Dyn::DTE::Device(	_uartDteDynDevPapi,
													_uartDteDynAdvRapi,
													_uartDteDynAdvPapi,
													_driver->getUartDteApi(),
													&location
													);
	_device->syncOpen();
	_uartDteAdvCapi.add(*_device);
	}

void	Instance::response(	Oscl::Mt::Itc::Srv::Close::
							Resp::Api::CloseResp&		msg
							) noexcept{
	if(_driverOpen){
		// This is the close for the driver
		// Mark it as done; deactivate the dynamic
		// service; then begin asynchronously
		// closing the device in preparation for
		// the deleting.
		_driverOpen	= false;
		_device->deactivate();
		closeDevice();
		return;
		}

	_device->~Device();
	_device	= 0;

	_driver->~Driver();
	_driver	= 0;

	releaseResources();
	}

void	Instance::response(	Oscl::Mt::Itc::Srv::Open::
							Resp::Api::OpenResp&		msg
							) noexcept{
	// This should never happen since I never issue an async open.
	// FIXME:
	while(true);
	}

