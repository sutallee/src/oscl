/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <stdio.h>
#include <string.h>
#include "instance.h"
#include "oscl/hw/usb/desc/ifreg.h"
#include "oscl/driver/usb/setup/setconfig.h"
#include "oscl/driver/usb/setup/getepstatus.h"
#include "oscl/error/info.h"
#include "oscl/error/fatal.h"
#include "oscl/oid/fixed.h"
#include "oscl/block/rw/dyn/service.h"
#include "oscl/scsi/cmd/scsi.h"
#include "oscl/tt/handler.h"
#include "oscl/tt/result.h"

using namespace Oscl::Usb::Enum::Interface::Storage::SCSI;

Instance::Instance(	Oscl::Mt::Itc::PostMsgApi&				driverPapi,
					Oscl::Mt::Itc::PostMsgApi&				lunDriverPapi,
					Oscl::Mt::Itc::Dyn::Adv::Creator::
					SyncApi<	Oscl::Block::RW::
								Dyn::Service
								>&							blockDevAdvCapi,
					Oscl::Mt::Itc::PostMsgApi&      		blockDevDynDevPapi,
					Oscl::Mt::Itc::Dyn::Adv::
					Core<	Oscl::Block::RW::
							Dyn::Service
							>::ReleaseApi&					blockDevDynAdvRapi,
					Oscl::Mt::Itc::PostMsgApi&				blockDevDynAdvPapi,
					Oscl::Usb::Setup::Mem&					setupMem,
					PacketMem&								packetMem,
					Oscl::Usb::TT::BO::
					Driver::SetupMem*						driverSetupMem,
					Oscl::Usb::TT::BO::
					Driver::PacketMem*						driverPacketMem,
					Oscl::TT::LUN::
					Driver::PacketMem*						lunDriverPacketMem,
					unsigned								oidType,
					const Oscl::ObjectID::RO::Api*			location
					) noexcept:
		_setupMem(setupMem),
		_packetMem(packetMem),
		_driverSetupMem(driverSetupMem),
		_driverPacketMem(driverPacketMem),
		_lunDriverPacketMem(lunDriverPacketMem),
		_oidType(oidType),
		_driverPapi(driverPapi),
		_lunDriverPapi(lunDriverPapi),
		_blockDevAdvCapi(blockDevAdvCapi),
		_blockDevDynDevPapi(blockDevDynDevPapi),
		_blockDevDynAdvRapi(blockDevDynAdvRapi),
		_blockDevDynAdvPapi(blockDevDynAdvPapi),
		_bulkInPipe(0),
		_bulkOutPipe(0),
		_driverOpen(false),
		_driver(0),
		_lunDriver(0),
		_device(0),
		_maxBulkInPacketSize(0),
		_maxBulkOutPacketSize(0),
		_reservedPower(0),
		_myPapi(0),
		_hcdMonApi(0),
		_ctrlPipe(0),
		_usbAddress(0),
		_lowSpeedDevice(false),
		_closeMem(0),
		_sdapi(0)
		{
	}

void	Instance::releaseResources() noexcept{
	if(_bulkInPipe){
		_hcdMonApi->
		getBulkInPipeAllocSyncApi().
		free(*_bulkInPipe);
		}
	if(_bulkOutPipe){
		_hcdMonApi->
		getBulkOutPipeAllocSyncApi().
		free(*_bulkOutPipe);
		}
	_sdapi->stopDone(*this);
	}

Oscl::Usb::Desc::Interface*
Instance::getInterface(	Oscl::Usb::Desc::
						Config*			config,
						unsigned char	interfaceNumber
						) noexcept{
	unsigned char	nInterfaces	= config->_bNumInterfaces;
	using namespace Oscl::Usb::Hw::Desc;
	for(unsigned char i=0;i<nInterfaces;++i){
		Oscl::Usb::Desc::Descriptor*
		desc	= config->findDescriptor(	bDescriptorType::INTERFACE,
											i
											);
		if(!desc){
			// FIXME: _bNumInterfaces lied?
			while(true);
			}
		Oscl::Usb::Desc::Interface*
		interface	= (Oscl::Usb::Desc::Interface*)desc;
		if(interface->_bInterfaceNumber == interfaceNumber){
			return interface;
			}
		}

	return 0;
	}

bool
Instance::getEndpoints(	Oscl::Usb::Desc::Config*	config,
						Oscl::Usb::Desc::Endpoint**	bulkIn,
						Oscl::Usb::Desc::Endpoint**	bulkOut
						) noexcept{
	enum{nEndpointsDescs=2};
	using namespace Oscl::Usb::Hw::Desc;

	// First both descriptors
	*bulkIn		= 0;
	*bulkOut	= 0;
	for(unsigned i=0;i<nEndpointsDescs;++i){
		Oscl::Usb::Desc::Endpoint*
		ep	=	(Oscl::Usb::Desc::Endpoint*)
				config->findDescriptor(	bDescriptorType::ENDPOINT,
										i,
										sizeof(DescMem)
										);
		if(!ep){
			// No Endpoint Descriptor found
			Oscl::Error::Info::log(
				"Oscl::Usb::Enum::Interface::Storage::SCSI:\n"
				"not enough endpoint descriptors.\n"
				);
			return false;
			}
		else if(ep->_bmAttributes.equal(	Endpoint::bmAttributes::
											TransferType::FieldMask,
											Endpoint::bmAttributes::
											TransferType::ValueMask_Bulk
											)
				){
			if(ep->_bEndpointAddress.equal(	Endpoint::bEndpointAddress::
											Direction::FieldMask,
											Endpoint::bEndpointAddress::
											Direction::ValueMask_OUT
											)
					){
				*bulkOut	= ep;
				}
			else {
				*bulkIn		= ep;
				}
			}
		else {
			// No Endpoint Descriptor found
			Oscl::Error::Info::log(	"Oscl::Usb::Enum::Interface::Storage::SCSI:\n"
									"unexpected endpoint descriptor type.\n"
									);
			return false;
			}
		}

	if(*bulkIn && *bulkOut){
		return true;
		}
	// No Endpoint Descriptor found
	Oscl::Error::Info::log(	"Oscl::Usb::Enum::Interface::Storage::SCSI:\n"
							"cant find all expected descriptors.\n"
							);
	return false;
	}

void	Instance::close(Oscl::Mt::Itc::Srv::Close::Req::Api::SAP& sap) noexcept{
	Oscl::Mt::Itc::Srv::Close::Req::Api::ClosePayload*
	payload	= new(&_closeMem->payload)
				Oscl::Mt::Itc::Srv::Close::
				Req::Api::ClosePayload();
	Oscl::Mt::Itc::Srv::Close::Resp::Api::CloseResp*
	resp	= new(&_closeMem->resp)
				Oscl::Mt::Itc::Srv::Close::
				Resp::Api::CloseResp(	sap.getReqApi(),
										*this,
										*_myPapi,
										*payload
										);
	sap.post(resp->getSrvMsg());
	}

void	Instance::start(	Oscl::Mt::Itc::PostMsgApi&		myPapi,
							Oscl::Usb::Pipe::
							Message::PipeApi&				epzPipe,
							Oscl::Usb::HCD::Enum::
							Monitor::Api&					hcdMonApi,
							Oscl::Usb::Desc::Config&		configDesc,
							Oscl::Usb::Address				usbAddress,
							bool							lowSpeedDevice,
							unsigned char					interfaceNumber,
							const Oscl::ObjectID::RO::Api*	location
							) noexcept{
	_myPapi				= &myPapi;
	_hcdMonApi			= &hcdMonApi;
	_ctrlPipe			= &epzPipe;
	_usbAddress			= usbAddress;
	_lowSpeedDevice		= lowSpeedDevice;

	Oscl::Usb::Desc::Interface*
	interfaceDesc	= getInterface(&configDesc,interfaceNumber);
	if(!interfaceDesc) return;

	Oscl::Usb::Desc::Endpoint*	bulkInED;
	Oscl::Usb::Desc::Endpoint*	bulkOutED;
	if(!getEndpoints(	&configDesc,
						&bulkInED,
						&bulkOutED
						)
			){
		return;
		}

	_maxBulkInPacketSize	= bulkInED->_wMaxPacketSize;
	_maxBulkOutPacketSize	= bulkOutED->_wMaxPacketSize;

	if(!allocateBulkInPipe(*bulkInED)) return;

	if(!allocateBulkOutPipe(*bulkOutED)) return;

	if(!setSCSIConfiguration(configDesc)) return;

	Oscl::ObjectID::Fixed<32>	loc;
	if(location){
		Oscl::ObjectID::Api&	l	= loc;
		l	= *location;
		}
	loc	+= _oidType;
	loc	+= interfaceNumber;
	createDriver(	loc,
					interfaceNumber,
					bulkInED->getEndpointID(),
					bulkOutED->getEndpointID()
					);
	}

void	Instance::stop(	StopDoneApi&				sdapi,
						Oscl::Mt::Itc::Srv::
						Close::Resp::CloseMem&		closeMem
						) noexcept{
	_closeMem	= &closeMem;
	_sdapi		= &sdapi;

	// Asynchronously issue CloseReq to
	// SCSI. When finished,
	// release power,and pipe resources
	// to HCD (e.g. releaseResources()) and finally
	// invoke deactivateDone() on self
	// (Dyn::Unit::Monitor<Oscl::Usb::DynDevice>)
	// NOTE: To implement the CloseReq,
	// this class must either implement the
	// Close::Resp::Api or create a new type
	// of transaction that does.
	_device->deactivate();
	close(_device->getSAP());
	}

bool	Instance::match(	Oscl::Usb::Pipe::
							Message::SyncApi&			epzSyncApi,
							Oscl::Usb::Desc::Config&	configDesc,
							uint16_t					deviceIdVendor,
							uint16_t					deviceIdProduct,
							uint16_t					deviceBcdDevice,
							uint8_t					deviceManufacturer,
							uint8_t					deviceProduct,
							uint8_t					deviceSerialNumber,
							unsigned char				interfaceNumber
							) noexcept{
	// FIXME: implement
	// 1. get interface descriptor by interface number.
	// 2. if class subclass and protocol match return true
	// 3. else return false
	// SCSI match
	using namespace Oscl::Usb::Hw::Desc::Interface;
	Oscl::Usb::Desc::Interface*
	interface	= getInterface(&configDesc,interfaceNumber);
	if(!interface){
		// FIXME: Logic error
		// match should not have been given a bogus interface number.
		while(true);
		}
//	if(deviceIdVendor != 0x0557){
//		return false;
//		}
//	if(deviceIdProduct != 0x2008){
//		return false;
//		}
	if(interface->_bInterfaceClass != bInterfaceClass::MassStorage){
		return false;
		}
	if(interface->_bInterfaceSubClass != bInterfaceSubClass::MSSCSI){
		return false;
		}
	if(interface->_bInterfaceProtocol != bInterfaceProtocol::BulkOnlyTransport){
		return false;
		}
	return true;
	}

bool	Instance::allocateBulkInPipe(Oscl::Usb::Desc::Endpoint& desc) noexcept{
	using namespace Oscl::Usb::Hw::Desc::Endpoint::bEndpointAddress;

	unsigned char	endpointID	= desc.getEndpointID();
	_bulkInPipe	=
	_hcdMonApi->
	getBulkInPipeAllocSyncApi().
	alloc(	_usbAddress,
			endpointID,
			desc._wMaxPacketSize,
			_lowSpeedDevice
			);
	if(!_bulkInPipe){
		Oscl::Error::Info::log(	"Oscl::Usb::Enum::Interface::Storage::SCSI:\n"
								"Cant allocate bulk IN pipe.\n"
								);
		return false;
		}
	return true;
	}

bool	Instance::allocateBulkOutPipe(Oscl::Usb::Desc::Endpoint& desc) noexcept{
	using namespace Oscl::Usb::Hw::Desc::Endpoint::bEndpointAddress;

	unsigned char	endpointID	= desc.getEndpointID();
	_bulkOutPipe	=
	_hcdMonApi->
	getBulkOutPipeAllocSyncApi().
	alloc(	_usbAddress,
			endpointID,
			desc._wMaxPacketSize,
			_lowSpeedDevice
			);
	if(!_bulkOutPipe){
		Oscl::Error::Info::log(	"Oscl::Usb::Enum::Interface::Storage::SCSI:\n"
								"Cant allocate bulk OUT pipe.\n"
								);
		return false;
		}
	return true;
	}

bool	Instance::setSCSIConfiguration(Oscl::Usb::Desc::Config& desc) noexcept{
	Oscl::Usb::Pipe::Message::SyncApi&
	syncPipe	= _ctrlPipe->getSyncApi();
	bool	failed;

	Oscl::Usb::Setup::SetConfiguration*
	setConfigSetup	= new(&_setupMem)
						Oscl::Usb::Setup::
						SetConfiguration(desc._bConfigurationValue);

	failed	= syncPipe.control(*setConfigSetup);
	if(failed){
		Oscl::Error::Info::log(	"Oscl::Usb::Enum::Interface::Storage::SCSI:\n"
								"Error trying to set device configuration.\n"
								);
		return false;
		}
	return true;
	}

void	Instance::createDriver(	const Oscl::ObjectID::RO::Api&	location,
								unsigned char					interfaceNumber,
								unsigned char					bulkInEndpointID,
								unsigned char					bulkOutEndpointID
								) noexcept{
	_driver	= new (&_driverMem)
				Oscl::Usb::TT::BO::
				Driver(	*_ctrlPipe,
						_bulkInPipe->getSAP(),
						_bulkOutPipe->getSAP(),
						_driverPapi,
						*_driverSetupMem,
						*_driverPacketMem,
						_maxBulkInPacketSize,
						_maxBulkOutPacketSize,
						interfaceNumber,
						bulkInEndpointID,
						bulkOutEndpointID
						);
	_driver->syncOpen();
	_driverOpen	= true;

	Oscl::TT::Api&	ttApi	= _driver->getTTApi();

	if(ttApi.reset()){
		Oscl::Error::Info::log(	"USB Mass Storage:"
								" Reset failure!\n"
								);
		return;
		}
	unsigned char	maxLUN;
	if(ttApi.getMaxLUN(maxLUN)){
		Oscl::Error::Info::log(	"USB Mass Storage:"
								" GetMaxLun failure!\n"
								);
		return;
		}


	if(maxLUN > 0){
		char	buff[128];
		sprintf(buff,"USB Mass Storage driver only supports one LUN: nLUNs= %u\n",((unsigned)maxLUN)+1);
		Oscl::Error::Info::log(buff);
		}

	if(waitTestUnitReady(3)){
		Oscl::Error::Info::log(	"USB Mass Storage:"
								" device still not ready!\n"
								);
		return;
		}
	if(inquiry()){
		Oscl::Error::Info::log(	"USB Mass Storage:"
								" INQUIRY failed!\n"
								);
		}
	Oscl::SCSI::CMD::Inquiry::Response*
	inquiryResp	= (Oscl::SCSI::CMD::Inquiry::Response*)_lunDriverPacketMem;
	static const char	jungsoftStr[] = "JUNGSOFT\n";
	if(	!strncmp(	(const char*)inquiryResp->_vendorIdentification,
					jungsoftStr,
					sizeof(jungsoftStr)-2
					)
			){
		jungsoftSeq(location);
		}
	else{
		genericSeq();
		}
	//
	// This is where I should issue the following:
	// 1. _driver->getTTApi().reset()
	// 2. _driver->getTTApi().getMaxLun()
	// 3. _driver->getTTApi().read(SCSI TEST-UNIT-READY) until ready
	// 4. _driver->getTTApi().read(SCSI INQUIRY)
	// 5. Examine INQUIRY:
	//		- if inquiryResp->_vendorIdentification == "JUNGSOFT"
	// 6. _driver->getTTApi().read(SCSI TEST-UNIT-READY) until ready
	// 7. _driver->getTTApi().read(SCSI START-STOP-UNIT) until ready
	}

void	Instance::response(	Oscl::Mt::Itc::Srv::Close::
							Resp::Api::CloseResp&		msg
							) noexcept{
	if(_device){
		_device->~Service();
		_device	= 0;
		if(_lunDriver){
			// Lun driver *should* have been closed by the dynamic service
			_lunDriver->~Driver();
			_lunDriver	= 0;
			}
		close(_driver->getSAP());
		return;
		}

	if(_driver){
		_driver->~Driver();
		_driver	= 0;
		}
	if(_driverOpen){
		// This is the close for the driver
		// Mark it as done; deactivate the dynamic
		// service; then begin asynchronously
		// closing the device in preparation for
		// the deleting.
		_driverOpen	= false;
		}

	releaseResources();
	}

void	Instance::response(	Oscl::Mt::Itc::Srv::Open::
							Resp::Api::OpenResp&		msg
							) noexcept{
	// This should never happen since I never issue an async open.
	// FIXME:
	while(true);
	}

class FailHandler : public Oscl::TT::Status::Handler {
	public:
		bool	_failed;
	public:
		FailHandler() noexcept:_failed(false){}
	private:
		void	failed() noexcept{
			// CHECK-CONDITION
			_failed	= false;
			};
		void	phaseError() noexcept{
			_failed	= true;
			}
		void	transportError() noexcept{
			_failed	= true;
			}
	};

bool	Instance::waitTestUnitReady(unsigned nRetries) noexcept{
	for(unsigned i=0;i<nRetries;++i){
		Oscl::SCSI::CMD::TestUnitReady::Standard	test;
		const Oscl::TT::Status::Result*
		result = _driver->getTTApi().command(	0,
												&test,
												6
												);
		if(!result) return false;
		FailHandler	fh;
		result->query(fh);
		if(fh._failed){
			Oscl::Error::Info::log(	"Fatal USB Mass Storage"
									" TEST-UNIT-READY failure: "
									);
			return true;
			}
		}
	return true;
	}

bool	Instance::inquiry() noexcept{
	Oscl::SCSI::CMD::Inquiry::Standard	inquiry(Oscl::SCSI::CMD::Inquiry::Response::size);
	unsigned long	residue;
	const Oscl::TT::Status::Result*
	result = _driver->getTTApi().read(	0,
										&inquiry,
										sizeof(inquiry),
										_lunDriverPacketMem,
										Oscl::SCSI::CMD::Inquiry::Response::size,
										residue
										);
	return result;
	}

bool	Instance::syncStart() noexcept{
	Oscl::SCSI::CMD::StartStopUnit::StartSync	start;
	const Oscl::TT::Status::Result*
	result = _driver->getTTApi().command(	0,
											&start,
											6
											);
	return result;
	}

bool	Instance::readCapacity() noexcept{
	unsigned long	residue;
	Oscl::SCSI::CMD::ReadCapacity::RBC	readCapacity;
	const Oscl::TT::Status::Result*
	result = _driver->getTTApi().read(	0,
										&readCapacity,
										10,
										_lunDriverPacketMem,
										Oscl::SCSI::CMD::ReadCapacity::Response::size,
										residue
										);
	return result;
	}

static const char	deviceNotReady[]	= "USB Mass Storage: device not ready\n";

void	Instance::jungsoftSeq(const Oscl::ObjectID::RO::Api& location) noexcept{
	if(waitTestUnitReady(3)){
		Oscl::Error::Info::log(deviceNotReady);
		return;
		}
	if(syncStart()){
		Oscl::Error::Info::log("USB Mass Storage: start failed\n");
		return;
		}
	if(waitTestUnitReady(3)){
		Oscl::Error::Info::log(deviceNotReady);
		return;
		}
	if(readCapacity()){
		Oscl::Error::Info::log("USB Mass Storage: read capacity failed 2\n");
		return;
		}
	Oscl::SCSI::CMD::ReadCapacity::Response*
	readCapacityResp	= (Oscl::SCSI::CMD::ReadCapacity::Response*)_lunDriverPacketMem;

	if(readCapacityResp->blockLengthInBytes() != 512){
		Oscl::Error::Info::log("USB Mass Storage: block length not 512 bytes\n");
		return;
		}

	if(waitTestUnitReady(3)){
		Oscl::Error::Info::log(deviceNotReady);
		return;
		}

#if 0
	// READ MBR
	// 1. Check Magic/sanity
	uint32_t	firstPartitionLBA;
	if(printMBR(firstPartitionLBA)){
		printRequestSense();
		}
	else {
		printPartitionHeader(firstPartitionLBA);
		}
#endif

	_lunDriver	=	new(&_lunDriverMem)
					Oscl::TT::LUN::Driver(	_lunDriverPapi,
											_driver->getTTSAP(),
											_driver->getTTApi(),
											*_lunDriverPacketMem
											);
//	_lunDriver->syncOpen();
	_device	=	new(&_deviceMem)
					Oscl::Block::RW::
					Dyn::Service(	_blockDevDynDevPapi,
									_blockDevDynAdvRapi,
									_blockDevDynAdvPapi,
									*_lunDriver,
									*_lunDriver,
									*_lunDriver,
									_lunDriver->getSAP(),
									Oscl::Block::RW::Dyn::Service::raw,
									readCapacityResp->logicalBlockAddress(),
									&location
									);
	_device->syncOpen();
	_blockDevAdvCapi.add(*_device);
	}

void	Instance::genericSeq() noexcept{
#if 0
	while(printTestUnitReady()){
		printRequestSense();
		}
	if(printLoadMedium()){
		printRequestSense();
		}
	for(unsigned i=0; (i<3) && printTestUnitReady();++i){
		printRequestSense();
		}
	if(printStartSync()){
		printRequestSense();
		}
	for(unsigned i=0; (i<3) && printTestUnitReady();++i){
		printRequestSense();
		}
//	if(printGoActive()){
//		printRequestSense();
//		}
//	while(printTestUnitReady()){
//		printRequestSense();
//		}
	if(printReadCapacity()){
		printRequestSense();
		}
	for(unsigned i=0; (i<3) && printTestUnitReady();++i){
		printRequestSense();
		}
//	if(printModeSense10(0)){
//		printRequestSense();
//		}
//	for(unsigned long i=0;i<0x3d3f;++i){
	for(unsigned long i=0;i<3;++i){
		if(printRead10Block(i)){
			printRequestSense();
			}
		if(printTestUnitReady()){
			printRequestSense();
			}
		}
	if(printModeSense6(0x80)){
		printRequestSense();
		}
	if(printRead6Block(1)){
		printRequestSense();
		}
#endif
	}

