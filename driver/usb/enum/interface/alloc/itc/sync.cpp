/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "sync.h"
#include "oscl/mt/itc/mbox/syncrh.h"

using namespace Oscl::Usb::Enum::Interface::Alloc::ITC;

Sync::Sync(	Oscl::Usb::Enum::Interface::Alloc::ITC::Req::Api&	api,
			Oscl::Mt::Itc::PostMsgApi&							myPapi
			) noexcept :
		_sap(api,myPapi)
		{
	}

Oscl::Usb::Enum::Interface::Alloc::ITC::Req::Api::SAP&	Sync::getSAP() noexcept{
	return _sap;
	}

Oscl::Usb::Enum::Interface::Api*
Sync::alloc(	Oscl::Usb::Pipe::
				Message::SyncApi&			epzSyncApi,
				Oscl::Usb::Desc::Config&	configDesc,
				uint16_t					deviceIdVendor,
				uint16_t					deviceIdProduct,
				uint16_t					deviceBcdDevice,
				uint8_t					deviceManufacturer,
				uint8_t					deviceProduct,
				uint8_t					deviceSerialNumber,
				unsigned char				interfaceNumber
				) noexcept{
	Oscl::Mt::Itc::SyncReturnHandler	srh;
	Req::Api::AllocPayload				payload(	epzSyncApi,
													configDesc,
													deviceIdVendor,
													deviceIdProduct,
													deviceBcdDevice,
													deviceManufacturer,
													deviceProduct,
													deviceSerialNumber,
													interfaceNumber
													);
	Req::Api::AllocReq					req(	_sap.getReqApi(),
												payload,
												srh
												);
	_sap.postSync(req);
	return payload._interface;
	}

void	Sync::free(	Oscl::Usb::Enum::Interface::Api& interface) noexcept{
	Oscl::Mt::Itc::SyncReturnHandler	srh;
	Req::Api::FreePayload				payload(interface);
	Req::Api::FreeReq					req(	_sap.getReqApi(),
												payload,
												srh
												);
	_sap.postSync(req);
	}

