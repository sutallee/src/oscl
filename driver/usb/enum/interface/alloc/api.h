/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_usb_enum_interface_alloc_apih_
#define _oscl_drv_usb_enum_interface_alloc_apih_
#include "oscl/driver/usb/enum/interface/api.h"

/** */
namespace Oscl {
/** */
namespace Usb {
/** */
namespace Enum {
/** */
namespace Interface {
/** */
namespace Alloc {

/** */
class Api {
	public:
		/** */
		virtual ~Api(){}

		/** */
		virtual Oscl::Usb::Enum::Interface::Api*
					alloc(	Oscl::Usb::Pipe::
							Message::SyncApi&			epzSyncApi,
							Oscl::Usb::Desc::Config&	configDesc,
							uint16_t					deviceIdVendor,
							uint16_t					deviceIdProduct,
							uint16_t					deviceBcdDevice,
							uint8_t					deviceManufacturer,
							uint8_t					deviceProduct,
							uint8_t					deviceSerialNumber,
							unsigned char				interfaceNumber
							) noexcept=0;

		/** */
		virtual void	free(	Oscl::Usb::Enum::
								Interface::Api&	interface
								) noexcept=0;
	};

}
}
}
}
}

#endif
