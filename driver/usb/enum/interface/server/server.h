/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_usb_enum_interface_server_serverh_
#define _oscl_drv_usb_enum_interface_server_serverh_
#include "oscl/mt/itc/srv/close.h"
#include "oscl/mt/itc/srv/closeapi.h"
#include "oscl/driver/usb/enum/interface/alloc/itc/reqapi.h"
#include "oscl/queue/queue.h"
#include "oscl/driver/usb/enum/interface/alloc/itc/sync.h"

/** */
namespace Oscl {
/** */
namespace Usb {
/** */
namespace Enum {
/** */
namespace Interface {

/** */
class Server :	private Oscl::Usb::Enum::Interface::Alloc::ITC::Req::Api,
				private Oscl::Mt::Itc::Srv::Close::Req::Api,
				public Oscl::Mt::Itc::Srv::CloseSync
				{
	private:
		/** */
		Oscl::Usb::Enum::Interface::Alloc::ITC::Sync	_sync;
		/** */
		Oscl::Queue<Oscl::Usb::Enum::Interface::Api>	_freeInterfaces;
		/** */
		Oscl::Queue<Oscl::Mt::Itc::SrvMsg>				_pending;
		/** */
		bool			_open;

	public:
		/** */
		Server(Oscl::Mt::Itc::PostMsgApi&	myPapi) noexcept;

		/** */
		Oscl::Usb::Enum::Interface::Alloc::Api&	getSyncApi() noexcept;

		/** This operation should only be called while the server
			is in the closed state. This is typically done during
			construction before the openSync() operation is invoked
			on the server.
		 */
		void	add(Interface::Api& interface) noexcept;

	private:	// Oscl::Usb::Enum::Interface::Alloc::ITC::Req::Api
		/** */
		void	request(AllocReq& msg) noexcept;
		/** */
		void	request(FreeReq& msg) noexcept;

	private:	// Oscl::Mt::Itc::Srv::Close::Req::Api
		/** */
		void	request(OpenReq& msg) noexcept;
		/** */
		void	request(CloseReq& msg) noexcept;
	};

}
}
}
}

#endif
