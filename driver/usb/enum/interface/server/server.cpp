/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "server.h"

using namespace Oscl::Usb::Enum::Interface;

Server::Server(Oscl::Mt::Itc::PostMsgApi&	myPapi) noexcept:
		Oscl::Mt::Itc::Srv::CloseSync(*this,myPapi),
		_sync(*this,myPapi),
		_open(false)
		{
	}

Oscl::Usb::Enum::Interface::Alloc::Api&	Server::getSyncApi() noexcept{
	return _sync;
	}

void	Server::add(Interface::Api& interface) noexcept{
	_freeInterfaces.put(&interface);
	}

void	Server::request(AllocReq& msg) noexcept{
	if(!_open){
		_pending.put(&msg);
		return;
		}
	AllocPayload&	payload	= msg.getPayload();
	Oscl::Usb::Desc::Config&
	configDesc	= msg.getPayload()._configDesc;
	unsigned char
	interfaceNumber	= msg.getPayload()._interfaceNumber;
	Oscl::Usb::Pipe::
	Message::SyncApi&	epzSyncApi	= msg.getPayload()._epzSyncApi;


	Interface::Api*	interface;
	for(	interface = _freeInterfaces.first();
			interface;
			interface	= _freeInterfaces.next(interface)
			){
		if(interface->match(	epzSyncApi,
								configDesc,
								payload._deviceIdVendor,
								payload._deviceIdProduct,
								payload._deviceBcdDevice,
								payload._deviceManufacturer,
								payload._deviceProduct,
								payload._deviceSerialNumber,
								interfaceNumber
								)
			){
			msg.getPayload()._interface	= interface;
			_freeInterfaces.remove(interface);
			msg.returnToSender();
			return;
			}
		}
	msg.getPayload()._interface	= 0;
	msg.returnToSender();
	}

void	Server::request(FreeReq& msg) noexcept{
	if(!_open){
		_pending.put(&msg);
		return;
		}
	_freeInterfaces.put(&msg.getPayload()._interfaceToFree);
	msg.returnToSender();
	}

void	Server::request(OpenReq& msg) noexcept{
	if(_open){
		// FIXME: Protocol Error
		while(true);
		}
	_open	= true;
	Oscl::Queue<Oscl::Mt::Itc::SrvMsg>	list	= _pending;
	Oscl::Mt::Itc::SrvMsg*	next;
	while((next=list.get())){
		next->process();
		}
	msg.returnToSender();
	}

void	Server::request(CloseReq& msg) noexcept{
	if(!_open){
		// FIXME: Protocol Error
		while(true);
		}
	_open	= false;
	msg.returnToSender();
	}

