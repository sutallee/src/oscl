/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_usb_enum_interface_apih_
#define _oscl_drv_usb_enum_interface_apih_
#include "oscl/driver/usb/desc/config.h"
#include "oscl/driver/usb/pipe/message/pipeapi.h"
#include "oscl/mt/itc/srv/crespmem.h"
#include "oscl/driver/usb/alloc/power/syncapi.h"
#include "oscl/driver/usb/hcd/enum/monitor/api.h"
#include "oscl/oid/roapi.h"

/** */
namespace Oscl {
/** */
namespace Usb {
/** */
namespace Enum {
/** */
namespace Interface {

/** */
class StopDoneApi;

/** This abstract entity represents a USB Interface. The inherited
	QueueItem is used by the interface server to keep track of the
	interface while it is not allocated to a client. When a client
	has allocated the interface it may use the QueueItem field for
	its purpose until it returns the interface to the control of
	the interface server via a free message.
	The concrete implementation of this class must contain all of
	the resources necesssary to instantiate a driver for the
	interface. The driver is instantiated and opened during the
	start() operation.  The driver is closed and destructed during
	the stop() operation.
 */
class Api : public Oscl::QueueItem {
	public:
		/** */
		virtual ~Api(){}

		/** This operation is responsible for preparing the interface
			driver for operation. This might include allocating USB
			and other resources (e.g. pipes, and bandwidth) and then
			making the new "device" driver availble to clients.
			If any of these allocation or configuration operations
			fail for any reason, then the implementation should log
			an appropriate error message with the system, release
			any other resources already allocated and simply return.
			The stop operation will be called when the interface
			is removed from the system.
		 */
		virtual void	start(	Oscl::Mt::Itc::PostMsgApi&		myPapi,
								Oscl::Usb::Pipe::
								Message::PipeApi&				epzPipe,
								Oscl::Usb::HCD::Enum::
								Monitor::Api&					hcdMonApi,
								Oscl::Usb::Desc::Config&		configDesc,
								Oscl::Usb::Address				usbAddress,
								bool							lowSpeedDevice,
								unsigned char					interfaceNumber,
								const Oscl::ObjectID::RO::Api*	location
								) noexcept=0;

		/** This operation is responsible for removing the dynamic
			interface from the system, closing any drivers created,
			and finally releasing all resources allocated by the
			start operation.
		 */
		virtual void	stop(	StopDoneApi&				sdapi,
								Oscl::Mt::Itc::Srv::
								Close::Resp::CloseMem&		closeMem
								) noexcept=0;

		/** This operation is responsible for examining the USB
			device, and interface information to determine if
			this is is the type of interface expected by the
			implementation. The implementation shall return
			true if it is prepared to take responsiblity for
			this interface.
		 */
		virtual bool	match(	Oscl::Usb::Pipe::
								Message::SyncApi&			epzSyncApi,
								Oscl::Usb::Desc::Config&	configDesc,
								uint16_t					deviceIdVendor,
								uint16_t					deviceIdProduct,
								uint16_t					deviceBcdDevice,
								uint8_t					deviceManufacturer,
								uint8_t					deviceProduct,
								uint8_t					deviceSerialNumber,
								unsigned char				interfaceNumber
								) noexcept=0;
	};

/** */
class StopDoneApi {
	public:
		/** */
		virtual ~StopDoneApi(){}
		/** */
		virtual void	stopDone(Api& ) noexcept=0;
	};

}
}
}
}

#endif
