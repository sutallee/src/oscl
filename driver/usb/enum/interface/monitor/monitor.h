/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_usb_enum_interface_monitorh_
#define _oscl_drv_usb_enum_interface_monitorh_
#include "match.h"
#include "oscl/driver/usb/device/dyndev.h"
#include "oscl/mt/itc/dyn/monitor/unit/monitor/monitor.h"
#include "oscl/driver/usb/alloc/power/respapi.h"
#include "oscl/driver/usb/setup/mem.h"
#include "oscl/driver/usb/desc/config.h"
#include "oscl/driver/usb/desc/interface.h"
#include "oscl/driver/usb/desc/endpoint.h"
#include "oscl/mt/itc/srv/crespmem.h"
#include "oscl/driver/usb/desc/hid.h"
#include "oscl/driver/usb/enum/interface/alloc/api.h"

/** */
namespace Oscl {
/** */
namespace Usb {
/** */
namespace Enum {
/** */
namespace Interface {

/** This concrete class listens for the creation of a USB device
	that is described by its interfaces.  It waits for a new USB device
	to be created, claims the device, and allocates a driver to
	each interface.
	When the device is removed, the drivers are stopped, the
	resources for the driver are reclaimed, and the monitor waits
	for a new device to be created.
	This class is responsible for creating drivers for exactly
	one USB device.
 */
class Monitor :
		public Oscl::Mt::Itc::Dyn::Unit::Monitor<DynDevice>,
		private Oscl::Usb::Enum::Interface::StopDoneApi
		{
	public:
		/** */
		struct RawMem {
			/** */
			unsigned char	mem[		sizeof(Oscl::Usb:: Desc::Config)
									+	128	// FIXME: maximum conifg descriptor
									];
			};
		/** */
		typedef Oscl::Memory::
				AlignedBlock<sizeof(RawMem)>		DescMem;
		/** */
		union PacketMem {
			/** */
			void*								__qitemlink;
			/** */
			DescMem								_descMem;
			};

	private:
		/** */
		Oscl::Mt::Itc::Srv::
		Close::Resp::CloseMem					_closeMem;
		/** */
		Match									_match;
		/** */
		Oscl::Usb::Enum::Interface::Alloc::Api&	_ifServerApi;
		/** */
		Oscl::Mt::Itc::PostMsgApi&				_myPapi;
		/** */
		Oscl::Usb::Setup::Mem&					_setupMem;
		/** */
		PacketMem&								_packetMem;
		/** */
		Oscl::Queue<	Oscl::Usb::Enum::
						Interface::Api
						>						_interfaceList;
		/** */
		unsigned								_reservedPower;
		/** */
		const unsigned							_oidType;
		
	public:
		/** */
		Monitor(	Oscl::Mt::Itc::Dyn::Adv::Cli::
					SyncApi<DynDevice>&						advFind,
					Oscl::Mt::Itc::Dyn::Adv::Cli::
					Req::Api<	DynDevice
								>::SAP&						advSAP,
					Oscl::Usb::Enum::Interface::Alloc::Api&	ifServerApi,
					Oscl::Mt::Itc::PostMsgApi&				myPapi,
					Oscl::Usb::Setup::Mem&					setupMem,
					PacketMem&								packetMem,
					unsigned								oidType,
					const Oscl::ObjectID::RO::Api*			location=0
					) noexcept;
	private:
		/** */
		void	closeNextInterface() noexcept;
		/** */
		void	releaseResources() noexcept;

	private:
		/** */
		void	activate() noexcept;
		/** */
		void	deactivate() noexcept;

	private: // helpers
		/** */
		Oscl::Usb::Desc::Config*	readConfigDesc(unsigned length) noexcept;
		/** */
		bool	allocatePower() noexcept;
		/** */
		void	releasePower() noexcept;
		/** */
		bool	setConfiguration(Oscl::Usb::Desc::Config& desc) noexcept;

	private: // Oscl::Usb::Enum::Interface::StopeDoneApi
		/** */
		void	stopDone(Interface::Api& interface) noexcept;
	};

}
}
}
}

#endif

