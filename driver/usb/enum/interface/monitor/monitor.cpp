/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "monitor.h"
#include "oscl/driver/usb/setup/getconfigdesc.h"
#include "oscl/driver/usb/setup/setconfig.h"
#include "oscl/error/info.h"
#include "oscl/error/fatal.h"
#include "oscl/oid/fixed.h"
#include "oscl/driver/usb/pipe/status.h"

using namespace Oscl::Usb::Enum::Interface;

Monitor::Monitor(	Oscl::Mt::Itc::Dyn::Adv::Cli::
					SyncApi<DynDevice>&						advFind,
					Oscl::Mt::Itc::Dyn::Adv::Cli::
					Req::Api<	DynDevice
								>::SAP&						advSAP,
					Oscl::Usb::Enum::Interface::Alloc::Api&	ifServerApi,
					Oscl::Mt::Itc::PostMsgApi&				myPapi,
					Oscl::Usb::Setup::Mem&					setupMem,
					PacketMem&								packetMem,
					unsigned								oidType,
					const Oscl::ObjectID::RO::Api*			location
					) noexcept:
		Oscl::Mt::Itc::Dyn::Unit::Monitor<DynDevice>(	advSAP,
														advFind,
														myPapi,
														_match
														),
		_match(location),
		_ifServerApi(ifServerApi),
		_myPapi(myPapi),
		_setupMem(setupMem),
		_packetMem(packetMem),
		_reservedPower(0),
		_oidType(oidType)
		{
	}

void	Monitor::activate() noexcept{
	Oscl::Usb::Desc::Config*
	configDesc	= readConfigDesc(9);
	if(!configDesc){
		// Message is issued by readConfigDesc()
		return;
		}

	unsigned	l	= configDesc->_wTotalLength;

	if(l > sizeof(DescMem)){
		// If this happens, we don't have enough packet memory
		// to read all config-interface-endpoint descriptors.
		Oscl::ErrorFatal::logAndExit(	"Oscl::Usb::Enum::Interface::Monitor:"
										"configuration error:"
										"insufficient packet memory "
										"for device.\n"
										);
		}

	configDesc	= readConfigDesc(l);
	if(!configDesc){
		// Message is issued by readConfigDesc()
		return;
		}

	_reservedPower	= (configDesc->_bMaxPower * 50);

	if(!allocatePower()){
		// not enough power available
		return;
		}

	if(!setConfiguration(*configDesc)){
		// Control pipe request failed
		releasePower();
		return;
		}

	DynDevice&	dyndev	= _handle->getDynSrv();

	unsigned char	maxInterfaces	= configDesc->_bNumInterfaces;
	for(unsigned i=0;i<maxInterfaces;++i){
		Oscl::Usb::Enum::Interface::Api*
		interface	= _ifServerApi.alloc(	dyndev._ctrlPipe.getSyncApi(),
											*configDesc,
											dyndev._idVendor,
											dyndev._idProduct,
											dyndev._bcdDevice,
											dyndev._iManufacturer,
											dyndev._iProduct,
											dyndev._iSerialNumber,
											i
											);
		if(interface){
			Oscl::ObjectID::Fixed<32>	location;
			static_cast<Oscl::ObjectID::Api&>(location)	=	dyndev._location;
			location	+=	_oidType;
			location	+=	i;
			interface->start(	_myPapi,
								dyndev._ctrlPipe,
								dyndev._hcdEnumMonitorApi,
								*configDesc,
								dyndev._usbAddress,
								dyndev._lowSpeedDevice,
								i,
								&location
								);
			_interfaceList.put(interface);
			}
		}
	}

void	Monitor::closeNextInterface() noexcept{
	Oscl::Usb::Enum::Interface::Api*	interface	= _interfaceList.get();
	if(interface){
		interface->stop(*this,_closeMem);
		return;
		}
	releaseResources();	// assumed synchronous
	deactivateDone();
	}

void	Monitor::releaseResources() noexcept{
	releasePower();
	}

void	Monitor::deactivate() noexcept{
	closeNextInterface();
	}

Oscl::Usb::Desc::Config*
Monitor::readConfigDesc(unsigned length) noexcept{
	DynDevice&	dyndev	= _handle->getDynSrv();
	Oscl::Usb::Pipe::Message::SyncApi&
	syncPipe	= dyndev._ctrlPipe.getSyncApi();
	const Oscl::Usb::Pipe::Status::Result*	failed;

	unsigned char*		rawMem	= (unsigned char*)&_packetMem._descMem;

	Oscl::Usb::Setup::GetConfigDesc*
	getConfigSetup	= new(&_setupMem)
						Oscl::Usb::Setup::
						GetConfigDesc(	0, // desc index
										0, // lang ID
										length
										);

	failed	= syncPipe.read(*getConfigSetup,rawMem);
	if(failed){
		Oscl::Error::Info::log(	"Oscl::Usb::Enum::Interface::Monitor:"
								"control pipe read failed.\n"
								);
		return 0;
		}

	Oscl::Usb::Desc::Descriptor*
	desc	= (Oscl::Usb::Desc::Descriptor*)rawMem;

	using namespace Oscl::Usb::Hw::Desc;

	if(desc->_bDescriptorType != bDescriptorType::CONFIGURATION){
		releaseResources();
		Oscl::Error::Info::log(	"Oscl::Usb::Enum::Interface::Monitor:"
								"device returned wrong descriptor type.\n"
								);
		return 0;
		}

	Oscl::Usb::Desc::Config*
	configDesc	= (Oscl::Usb::Desc::Config*)desc;

	return configDesc;
	}

bool	Monitor::allocatePower() noexcept{
	DynDevice&	dyndev	= _handle->getDynSrv();
	Oscl::Usb::Alloc::Power::SyncApi&
	power	= dyndev.getPowerSyncApi();

	bool	failed	= power.reserve(_reservedPower);
	if(failed){
		Oscl::Error::Info::log(	"Oscl::Usb::Enum::Interface::Monitor:"
								"insufficient power available for device.\n"
								);
		return false;
		}
	return true;
	}

void	Monitor::releasePower() noexcept{
	if(!_reservedPower) return;
	DynDevice&	dyndev	= _handle->getDynSrv();
	Oscl::Usb::Alloc::Power::SyncApi&
	power	= dyndev.getPowerSyncApi();
	power.free(_reservedPower);
	_reservedPower	= 0;
	}

bool	Monitor::setConfiguration(Oscl::Usb::Desc::Config& desc) noexcept{
	DynDevice&	dyndev	= _handle->getDynSrv();
	Oscl::Usb::Pipe::Message::SyncApi&
	syncPipe	= dyndev._ctrlPipe.getSyncApi();
	bool	failed;

	Oscl::Usb::Setup::SetConfiguration*
	setConfigSetup	= new(&_setupMem)
						Oscl::Usb::Setup::
						SetConfiguration(desc._bConfigurationValue);

	failed	= syncPipe.control(*setConfigSetup);
	if(failed){
		Oscl::Error::Info::log(	"Oscl::Usb::Enum::Interface::Monitor:"
								"control pipe request failed.\n"
								);
		return false;
		}
	return true;
	}

void	Monitor::stopDone(Interface::Api& interface) noexcept{
	_ifServerApi.free(interface);
	closeNextInterface();
	}

