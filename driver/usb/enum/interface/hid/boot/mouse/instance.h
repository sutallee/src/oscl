/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_usb_enum_interface_hid_boot_mouse_instanceh_
#define _oscl_drv_usb_enum_interface_hid_boot_mouse_instanceh_
#include "oscl/driver/usb/enum/interface/api.h"
#include "oscl/mt/itc/dyn/monitor/unit/monitor/monitor.h"
#include "oscl/driver/usb/setup/mem.h"
#include "oscl/driver/usb/desc/config.h"
#include "oscl/driver/usb/desc/interface.h"
#include "oscl/driver/usb/desc/endpoint.h"
#include "oscl/driver/usb/hid/boot/mouse/driver.h"
#include "oscl/mt/itc/srv/crespmem.h"
#include "oscl/driver/usb/desc/hid.h"
#include "oscl/oid/roapi.h"

/** */
namespace Oscl {
/** */
namespace Usb {
/** */
namespace Enum {
/** */
namespace Interface {
/** */
namespace HID {
/** */
namespace Boot {
/** */
namespace Mouse {

/** This concrete class creates an instance of a USB HID Boot
	Mouse device. 
 */
class Instance :	public Oscl::Usb::Enum::Interface::Api,
					private Oscl::Mt::Itc::Srv::Close::Resp::Api
					{
		/** */
		typedef	Oscl::Memory::
				AlignedBlock<	sizeof(	Oscl::Usb::HID::Boot::
										Mouse::Driver
										)
								>			MouseDriverMem;
	private:
		/** */
		MouseDriverMem							_driverMem;
		/** */
		Oscl::Usb::HID::Boot::Mouse::
		Driver::SetupMem&						_mouseSetupMem;
		/** */
		Oscl::Usb::HID::Boot::Mouse::
		Driver::PacketMem&						_mousePacketMem;
		/** */
		Oscl::Mt::Itc::PostMsgApi&				_mousePapi;
		/** */
		Oscl::Usb::Alloc::
		BW::Interrupt::BwRec*					_bwRec;
		/** */
		Oscl::Usb::Pipe::
		Interrupt::IN::PipeApi*					_mousePipe;
		/** */
		Oscl::Usb::HID::Boot::Mouse::Driver*	_mouseDriver;
		/** */
		Oscl::Bits::
		FieldApi<	Oscl::Est::Mdp8xxPro::
					LedControl::Reg
					>&    						_led;
		/** */
		const unsigned							_oidType;
		/** */
		Oscl::Mt::Itc::PostMsgApi*				_myPapi;
		/** */
		Oscl::Usb::HCD::Enum::Monitor::Api*		_hcdMonApi;
		/** */
		Oscl::Usb::Address						_usbAddress;
		/** */
		bool									_lowSpeedDevice;
		/** */
		StopDoneApi*							_sdapi;
		
	public:
		/** */
		Instance(	Oscl::Mt::Itc::PostMsgApi&				mousePapi,
					Oscl::Usb::HID::Boot::Mouse::
					Driver::SetupMem&						mouseSetupMem,
					Oscl::Usb::HID::Boot::Mouse::
					Driver::PacketMem&						mousePacketMem,
					Oscl::Bits::
					FieldApi<	Oscl::Est::Mdp8xxPro::
								LedControl::Reg
								>&    						led,
					unsigned								oidType,
					const Oscl::ObjectID::RO::Api*			location=0
					) noexcept;
	private:
		/** */
		void	releaseResources() noexcept;

	private: // helpers
		/** */
		Oscl::Usb::Desc::Interface*
			getInterface(	Oscl::Usb::Desc::
							Config*			config,
							unsigned char	interfaceNumber
							) noexcept;
		/** */
		Oscl::Usb::Desc::Endpoint*
			getEndpoint(	Oscl::Usb::Desc::
							Config*				config,
							Oscl::Usb::Desc::
							Interface*			interface
							) noexcept;
		/** */
		void	allocBandwidth(Oscl::Usb::Desc::Endpoint& desc) noexcept;
		/** */
		void	allocateInterruptInPipe(	Oscl::Usb::Desc::
											Endpoint&			desc
											) noexcept;

		/** */
		void	createMouseDriver(	const Oscl::ObjectID::RO::Api&	location,
									unsigned char					maxPacketSize
									) noexcept;

	private:	// Oscl::Mt::Itc::Srv::Close::Resp::Api
		/** */
		void	response(	Oscl::Mt::Itc::Srv::Close::
							Resp::Api::CloseResp&		msg
							) noexcept;
		/** */
		void	response(	Oscl::Mt::Itc::Srv::Open::
							Resp::Api::OpenResp&		msg
							) noexcept;

	private: // Oscl::Usb::Enum::Interface::Api
		/** */
		void	start(	Oscl::Mt::Itc::PostMsgApi&		myPapi,
						Oscl::Usb::Pipe::
						Message::PipeApi&				epzPipe,
						Oscl::Usb::HCD::Enum::
						Monitor::Api&					hcdMonApi,
						Oscl::Usb::Desc::Config&		configDesc,
						Oscl::Usb::Address				usbAddress,
						bool							lowSpeedDevice,
						unsigned char					interfaceNumber,
						const Oscl::ObjectID::RO::Api*	location
						) noexcept;
		/** */
		void	stop(	StopDoneApi&				sdapi,
						Oscl::Mt::Itc::Srv::
						Close::Resp::CloseMem&		closeMem
						) noexcept;
		/** */
		bool	match(	Oscl::Usb::Pipe::
						Message::SyncApi&			epzSyncApi,
						Oscl::Usb::Desc::Config&	configDesc,
						uint16_t					deviceIdVendor,
						uint16_t					deviceIdProduct,
						uint16_t					deviceBcdDevice,
						uint8_t					deviceManufacturer,
						uint8_t					deviceProduct,
						uint8_t					deviceSerialNumber,
						unsigned char				interfaceNumber
						) noexcept;
	};

}
}
}
}
}
}
}

#endif

