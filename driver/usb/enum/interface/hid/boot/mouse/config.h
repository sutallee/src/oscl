/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_usb_enum_interface_hid_boot_mouse_configh_
#define _oscl_drv_usb_enum_interface_hid_boot_mouse_configh_
#include <new>
#include "instance.h"
#include "oscl/error/fatal.h"
#include "oscl/kernel/mmu.h"
#include "oscl/extalloc/api.h"

/** */
namespace Oscl {
/** */
namespace Usb {
/** */
namespace Enum {
/** */
namespace Interface {
/** */
namespace HID {
/** */
namespace Boot {
/** */
namespace Mouse {

/**	The purpose of this template class is to provide a configurable
	means of static allocation for a USB HID Boot Mouse.
 */
template< unsigned maxPorts >
class Config {
	private:
		/** Memory for the actual monitor. The monitor is
			initialized (a la "new") in the body of the
			constructor such that "complex" memory calculations
			can be performed and dynamic memory allocated.
		 */
		Oscl::Memory::
		AlignedBlock<sizeof(Instance)>			_monMem;

		/** */
		Oscl::ExtAlloc::Record					_dmaMemRec;
		/** This pointer is initialized during construction,
			and points to _monMem after the "new" operation
			is complete.
		 */
		Instance*								_theMonitor;
		
	public:
		/** */
		Config(	Oscl::Mt::Itc::PostMsgApi&				mousePapi,
				Oscl::ExtAlloc::Api&					usbMemAllocator,
				Oscl::Bits::
				FieldApi<	Oscl::Est::Mdp8xxPro::
							LedControl::Reg
							>&    						led,
				unsigned								oidType,
				const Oscl::ObjectID::RO::Api*			location=0
				) noexcept;
		/** */
		virtual ~Config(){}

		/** */
		Oscl::Mt::Itc::Srv::OpenCloseSyncApi&	getOpenCloseSyncApi() noexcept{
			Oscl::Mt::Itc::Srv::OpenCloseSyncApi*
			p = _theMonitor;
			return *p;
			}

		/** */
		Instance&		getInstance() noexcept{
			return *_theMonitor;
			}

	};

/**
 */
template<unsigned maxPorts>
Config<	maxPorts
		>::Config(	Oscl::Mt::Itc::PostMsgApi&				mousePapi,
					Oscl::ExtAlloc::Api&					usbMemAllocator,
					Oscl::Bits::
					FieldApi<	Oscl::Est::Mdp8xxPro::
								LedControl::Reg
								>&    						led,
					unsigned								oidType,
					const Oscl::ObjectID::RO::Api*			location
					) noexcept
		{
	const unsigned long	pageSize			= OsclKernelGetPageSize();
	const unsigned long	almostPageSize		= pageSize-1;

	const unsigned long	mouseSetupMemSize		=
		(sizeof(Oscl::Usb::HID::Boot::Mouse::Driver::SetupMem));

	const unsigned long	mousePacketMemSize		=
		(sizeof(Oscl::Usb::HID::Boot::Mouse::Driver::PacketMem));

	const unsigned long	dmaMemSize		=
		(		mouseSetupMemSize
			+	mousePacketMemSize
			);

	const unsigned long	nPages		=	(dmaMemSize+almostPageSize)/pageSize;
	const unsigned long	allocSize	=	pageSize*nPages;

	if(!usbMemAllocator.alloc(_dmaMemRec,allocSize,~almostPageSize)){
		Oscl::ErrorFatal::logAndExit(
			"Oscl::Usb::Enum::HID::Mouse::Config: cant allocate packet buffers."
			);
		}

	Oscl::Usb::HID::Boot::Mouse::Driver::SetupMem*
	mouseSetupMem	= (Oscl::Usb::HID::Boot::Mouse::Driver::SetupMem*)
						_dmaMemRec.getFirstUnit();

	Oscl::Usb::HID::Boot::Mouse::Driver::PacketMem*
	mousePacketMem	= (Oscl::Usb::HID::Boot::Mouse::Driver::PacketMem*)
		(((unsigned long)mouseSetupMem) + mouseSetupMemSize);
		
	_theMonitor	= new(&_monMem)	Instance(	mousePapi,
											*mouseSetupMem,
											*mousePacketMem,
											led,
											oidType,
											location
											);
	}

}
}
}
}
}
}
}

#endif
