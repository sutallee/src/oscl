/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "instance.h"
#include "oscl/oid/fixed.h"

using namespace Oscl::Usb::Enum::Interface::HID::Boot::Mouse;

Instance::Instance(	Oscl::Mt::Itc::PostMsgApi&				mousePapi,
					Oscl::Usb::HID::Boot::Mouse::
					Driver::SetupMem&						mouseSetupMem,
					Oscl::Usb::HID::Boot::Mouse::
					Driver::PacketMem&						mousePacketMem,
					Oscl::Bits::
					FieldApi<	Oscl::Est::Mdp8xxPro::
								LedControl::Reg
								>&    						led,
					unsigned								oidType,
					const Oscl::ObjectID::RO::Api*			location
					) noexcept:
		_mouseSetupMem(mouseSetupMem),
		_mousePacketMem(mousePacketMem),
		_mousePapi(mousePapi),
		_bwRec(0),
		_mousePipe(0),
		_mouseDriver(0),
		_led(led),
		_oidType(oidType),
		_myPapi(0),
		_usbAddress(0)
		{
	}

void	Instance::releaseResources() noexcept{
	if(_mousePipe){
		_hcdMonApi->
		getInterruptInPipeAllocSyncApi().
		free(*_mousePipe);
		}
	if(_bwRec){
		_hcdMonApi->
		getBwInterruptAllocSyncApi().
		free(*_bwRec);
		}
	_sdapi->stopDone(*this);
	}

Oscl::Usb::Desc::Interface*
Instance::getInterface(	Oscl::Usb::Desc::
						Config*			config,
						unsigned char	interfaceNumber
						) noexcept{
	unsigned char	nInterfaces	= config->_bNumInterfaces;
	using namespace Oscl::Usb::Hw::Desc;
	for(unsigned char i=0;i<nInterfaces;++i){
		Oscl::Usb::Desc::Descriptor*
		desc	= config->findDescriptor(	bDescriptorType::INTERFACE,
											i
											);
		if(!desc){
			// FIXME: _bNumInterfaces lied?
			while(true);
			}
		Oscl::Usb::Desc::Interface*
		interface	= (Oscl::Usb::Desc::Interface*)desc;
		if(interface->_bInterfaceNumber == interfaceNumber){
			return interface;
			}
		}

	return 0;
	}

Oscl::Usb::Desc::Endpoint*
Instance::getEndpoint(	Oscl::Usb::Desc::
						Config*				config,
						Oscl::Usb::Desc::
						Interface*			interface
						) noexcept{
	// Here we make the assumption that the descriptor
	// is formatted according to the USB spec. That is
	// to say that the endpoints for an interface follow
	// the interface descriptor.
	using namespace Oscl::Usb::Hw::Desc;
	enum{maxEndpoints=15};
	for(unsigned i	= 0; i< maxEndpoints;++i){
		Oscl::Usb::Desc::Descriptor*
		desc	= config->findDescriptor(	bDescriptorType::ENDPOINT,
											i
											);
		if(!desc){
			// FIXME:
			// No Endpoint Descriptor found
			while(true);
			}
		if(((unsigned char*)desc) > ((unsigned char*)interface)){
			// First endpoint descriptor after the interface desc.
			// Boot Mouse only has one interrupt IN endpoint.
			return (Oscl::Usb::Desc::Endpoint*)desc;
			}
		}

	return 0;
	}

void
Instance::allocateInterruptInPipe(Oscl::Usb::Desc::Endpoint&	desc) noexcept{
	using namespace Oscl::Usb::Hw::Desc::Endpoint::bEndpointAddress;

	unsigned char	endpointID	= desc.getEndpointID();

	_mousePipe	=
	_hcdMonApi->getInterruptInPipeAllocSyncApi().
	alloc(	*_bwRec,
			_usbAddress,
			endpointID,
			desc._wMaxPacketSize,
			_lowSpeedDevice
			);

	if(!_mousePipe){
		// FIXME
		while(true);
		}
	}

void
	Instance::createMouseDriver(	const Oscl::ObjectID::RO::Api&	location,
									unsigned char					maxPacketSize
									) noexcept{
	_mouseDriver	= new(&_driverMem)
					Oscl::Usb::HID::Boot::
					Mouse::Driver(	_mousePipe->getSAP(),
									_mousePapi,
									_mouseSetupMem,
									_mousePacketMem,
									_led,
									maxPacketSize
									);
	_mouseDriver->syncOpen();
	}

void	Instance::allocBandwidth(	Oscl::Usb::Desc::
									Endpoint&			endpointDesc
									) noexcept{
	unsigned long	bwFreeState;
	_bwRec	=	_hcdMonApi->
				getBwInterruptAllocSyncApi().
				reserve(	endpointDesc._wMaxPacketSize,
							endpointDesc._bInterval,
							endpointDesc.directionIsIN(),
							bwFreeState
							);
	if(!_bwRec){
		// FIXME
		while(true);
		}
	}

void	Instance::start(	Oscl::Mt::Itc::PostMsgApi&		myPapi,
							Oscl::Usb::Pipe::
							Message::PipeApi&				epzPipe,
							Oscl::Usb::HCD::Enum::
							Monitor::Api&					hcdMonApi,
							Oscl::Usb::Desc::Config&		configDesc,
							Oscl::Usb::Address				usbAddress,
							bool							lowSpeedDevice,
							unsigned char					interfaceNumber,
							const Oscl::ObjectID::RO::Api*	location
							) noexcept{
	_myPapi			= &myPapi;
	_hcdMonApi		= &hcdMonApi;
	_usbAddress		= usbAddress;
	_lowSpeedDevice	= lowSpeedDevice;

	Oscl::Usb::Desc::Interface*
	interface	= getInterface(&configDesc,interfaceNumber);

	if(!interface){
		// FIXME
		while(true);
		}

	Oscl::Usb::Desc::Endpoint*
	endpointDesc	= getEndpoint(&configDesc,interface);

	unsigned char	endpointID	=	endpointDesc->getEndpointID();
	if(endpointID == 0){
		// FIXME
		while(true);
		}

	allocBandwidth(*endpointDesc);

	allocateInterruptInPipe(*endpointDesc);

	// Instantiate drivers
	Oscl::ObjectID::Fixed<32>	loc;
	if(location){
		Oscl::ObjectID::Api&	l	= loc;
		l	= *location;
		}
	loc	+= _oidType;
	loc	+= interfaceNumber;
	createMouseDriver(loc,endpointDesc->_wMaxPacketSize);
	}

void	Instance::stop(	StopDoneApi&				sdapi,
						Oscl::Mt::Itc::Srv::
						Close::Resp::CloseMem&		closeMem
						) noexcept{
	if(!_mouseDriver){
		sdapi.stopDone(*this);
		return;
		}
	_sdapi	= &sdapi;
	Oscl::Mt::Itc::Srv::Close::Req::Api::ClosePayload*
	payload	= new(&closeMem.payload)
				Oscl::Mt::Itc::Srv::Close::
				Req::Api::ClosePayload();
	Oscl::Mt::Itc::Srv::Close::Resp::Api::CloseResp*
	resp	= new(&closeMem.resp)
				Oscl::Mt::Itc::Srv::Close::
				Resp::Api::CloseResp(	_mouseDriver->getSAP().getReqApi(),
										*this,
										*_myPapi,
										*payload
										);
	_mouseDriver->getSAP().post(resp->getSrvMsg());
	}

bool	Instance::match(	Oscl::Usb::Pipe::
							Message::SyncApi&			epzSyncApi,
							Oscl::Usb::Desc::Config&	configDesc,
							uint16_t					deviceIdVendor,
							uint16_t					deviceIdProduct,
							uint16_t					deviceBcdDevice,
							uint8_t					deviceManufacturer,
							uint8_t					deviceProduct,
							uint8_t					deviceSerialNumber,
							unsigned char				interfaceNumber
							) noexcept{
	// FIXME: implement
	// 1. get interface descriptor by interface number.
	// 2. if class subclass and protocol match return true
	// 3. else return false
	using namespace Oscl::Usb::Hw::Desc::Device;
	Oscl::Usb::Desc::Interface*
	interface	= getInterface(&configDesc,interfaceNumber);
	if(!interface){
		// FIXME: Logic error
		// match should not have been given a bogus interface number.
		while(true);
		}
	if(interface->_bInterfaceClass != bDeviceClass::HID){
		return false;
		}
	if(interface->_bInterfaceSubClass != bDeviceSubClass::HidBoot){
		return false;
		}
	if(interface->_bInterfaceProtocol != bDeviceProtocol::HidMouse){
		return false;
		}
	return true;
	}

void	Instance::response(	Oscl::Mt::Itc::Srv::Close::
							Resp::Api::CloseResp&		msg
							) noexcept{
	releaseResources();
	}

void	Instance::response(	Oscl::Mt::Itc::Srv::Open::
							Resp::Api::OpenResp&		msg
							) noexcept{
	// This should never happen since I never issue an async close.
	// FIXME:
	while(true);
	}

