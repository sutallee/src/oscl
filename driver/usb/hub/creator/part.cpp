/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <new>
#include "part.h"
#include "oscl/driver/usb/setup/getconfigdesc.h"
#include "oscl/driver/usb/setup/setconfig.h"
#include "oscl/driver/usb/setup/hub/gethubdesc.h"
#include "oscl/driver/usb/setup/hub/gethubstatus.h"
#include "oscl/driver/usb/setup/getepstatus.h"
#include "oscl/driver/usb/desc/hub.h"
#include "oscl/mt/thread.h"
#include "oscl/error/info.h"
#include "oscl/error/fatal.h"
#include "oscl/driver/usb/pipe/status.h"

using namespace Oscl::Usb::Hub::Creator;

Part::Part(
					Oscl::Mt::Itc::Dyn::Adv::Cli::
					Req::Api<	Oscl::Usb::DynDevice
								>::SAP&						advSAP,
					Oscl::Mt::Itc::Dyn::Adv::
					Creator::SyncApi<Oscl::Usb::DynDevice>&	advCapi,
					Oscl::Mt::Itc::Dyn::Adv::
					Core<Oscl::Usb::DynDevice>::ReleaseApi&	advRapi,
					Oscl::Mt::Itc::Delay::Req::Api::SAP&	delayServiceSAP,
					Oscl::Mt::Itc::PostMsgApi&				myPapi,
					Oscl::Mt::Itc::PostMsgApi&				hubPapi,
					Oscl::Mt::Itc::PostMsgApi&				dynSrvPapi,
					Oscl::Usb::Setup::Mem&					setupMem,
					PacketMem&								packetMem,
					Oscl::Usb::Hub::Adaptor::
					Driver::SetupMem&						adaptorSetupMem,
					Oscl::Usb::Hub::Adaptor::
					Driver::PacketMem&						adaptorPacketMem,
					PortDriverMem*							portDriverMem,
					PortAdaptorMem*							portAdaptorMem,
					Oscl::Usb::Hub::Port::Adaptor::
					Port::SetupMem*							portSetupMem,
					Oscl::Usb::Hub::Port::Adaptor::
					Port::PacketMem*						portPacketMem,
					Oscl::Usb::Hub::Port::
					Driver::SetupPktMem*					portDriverSetupMem,
					Oscl::Usb::Hub::Port::
					Driver::PacketMem*						portDriverPacketMem,
					unsigned								maxPorts,
					unsigned								oidType
					) noexcept:
		_advSAP(advSAP),
		_advCapi(advCapi),
		_advRapi(advRapi),
		_delayServiceSAP(delayServiceSAP),
		_myPapi(myPapi),
		_setupMem(setupMem),
		_packetMem(packetMem),
		_adaptorSetupMem(adaptorSetupMem),
		_adaptorPacketMem(adaptorPacketMem),
		_portDriverMem(portDriverMem),
		_portAdaptorMem(portAdaptorMem),
		_portSetupMem(portSetupMem),
		_portPacketMem(portPacketMem),
		_portDriverSetupMem(portDriverSetupMem),
		_portDriverPacketMem(portDriverPacketMem),
		_maxPorts(maxPorts),
		_oidType(oidType),
		_hubPapi(hubPapi),
		_dynSrvPapi(dynSrvPapi),
		_reservedPower(0),
		_bwRec(0),
		_hubStatusChangePipe(0),
		_currentHubPort(0),
		_adaptor(0),
		_stopDone(0)
		{
	}

bool	Part::start(	Oscl::Mt::Itc::
						Dyn::Handle<Oscl::Usb::DynDevice>	handle
						) noexcept{
	if(_handle != 0){
		for(;;);	// FIXME: programming protocol error
		}
	if(handle == 0){
		return true;
		}
	_handle	= handle;

	// Provision the circuit-pack.
	return activate();
	}

void	Part::stop(Oscl::Done::Api& stopDone) noexcept{
	if(_stopDone){
		for(;;);	// FIXME: programming protocol error
		}
	_stopDone	= &stopDone;
	deactivate();
	}

void	Part::addPort(bool nonRemovable,bool pwrControlMask) noexcept{
	}

bool	Part::activate() noexcept{
	Oscl::Usb::Desc::Config*
	configDesc	= readConfigDesc();
	if(!configDesc) return true;

	unsigned	l	= configDesc->_wTotalLength;

	if(l > sizeof(DescMem)){
		// If this happens, we don't have enough packet memory
		// to read all config-interface-endpoint descriptors.
		Oscl::Error::Info::log(	"Oscl::Equip::CP::USB::Shelf:"
								"Device descriptor too long.\n"
								);
		return true;
		}

	_reservedPower	= (configDesc->_bMaxPower * 50);

	Oscl::Usb::Desc::Interface*
	interfaceDesc	= getInterface(configDesc);
	if(!interfaceDesc) return true;

	if(interfaceDesc->_bNumEndpoints > 1){
		Oscl::Error::Info::log(	"Oscl::Equip::CP::USB::Shelf:"
								"Too many hub endpoints.\n"
								);
		return true;
		}

	Oscl::Usb::Desc::Endpoint*
	endpointDesc	= getEndpoint(configDesc);
	if(!endpointDesc) return true;

	unsigned char	endpointID	=	endpointDesc->getEndpointID();
	if(endpointID == 0){
		Oscl::Error::Info::log(	"Oscl::Equip::CP::USB::Shelf:"
								"Invalid endpoint ID.\n"
								);
		return true;
		}

	if(!allocatePower()) return true;

	if(!allocateBandwidth(*endpointDesc)) return true;

	if(!allocateInterruptInPipe(*endpointDesc)) return true;

	if(!setHubConfiguration(*configDesc)) return true;

	Oscl::Usb::Desc::Hub::Header*
	hubHeader	= readHubDescHeader();
	if(!hubHeader) return true;

	uint8_t	nPorts		= hubHeader->_bNbrPorts;

	if(nPorts > _maxPorts){
		// Static Configuration Error
		Oscl::Error::Info::log(	"Oscl::Equip::CP::USB::Shelf:\n"
								"Static configuration error.\n"
								"Not enough resources for hub.\n"
								"Too many ports.\n"
								);
		return true;
		}

	processHubPortDescriptors(hubHeader);

	if(!readEndpointStatus(endpointID)) return true;

	// Instantiate drivers
	createAdaptor(nPorts);

	createHubPorts(nPorts);

	return false;
	}

void	Part::releaseResources() noexcept{
	Oscl::Usb::DynDevice&	dyndev	= _handle->getDynSrv();
	if(_hubStatusChangePipe){
		dyndev.
		_hcdEnumMonitorApi.
		getInterruptInPipeAllocSyncApi().
		free(*_hubStatusChangePipe);
		}
	if(_bwRec){
		dyndev.
		_hcdEnumMonitorApi.
		getBwInterruptAllocSyncApi().
		free(*_bwRec);
		}
	Oscl::Usb::Alloc::Power::SyncApi&
	power	= dyndev.getPowerSyncApi();
	power.free(_reservedPower);
	_reservedPower	= 0;
	_handle	= 0;
	_stopDone->done();
	_stopDone	= 0;
	}

void	Part::closeCurrentHubPort() noexcept{
	Oscl::Mt::Itc::Srv::Close::Req::Api::ClosePayload*
	payload	= new(&_closeMem.payload)
				Oscl::Mt::Itc::Srv::Close::
				Req::Api::ClosePayload();
	Oscl::Mt::Itc::Srv::Close::Resp::Api::CloseResp*
	resp	= new(&_closeMem.resp)
				Oscl::Mt::Itc::Srv::Close::
				Resp::Api::CloseResp(	_currentHubPort->getSAP().getReqApi(),
										*this,
										_myPapi,
										*payload
										);
	_currentHubPort->getSAP().post(resp->getSrvMsg());
	}

void	Part::closeHubAdaptor() noexcept{
	Oscl::Mt::Itc::Srv::Close::Req::Api::ClosePayload*
	payload	= new(&_closeMem.payload)
				Oscl::Mt::Itc::Srv::Close::
				Req::Api::ClosePayload();
	Oscl::Mt::Itc::Srv::Close::Resp::Api::CloseResp*
	resp	= new(&_closeMem.resp)
				Oscl::Mt::Itc::Srv::Close::
				Resp::Api::CloseResp(	_adaptor->getSAP().getReqApi(),
										*this,
										_myPapi,
										*payload
										);
	_adaptor->getSAP().post(resp->getSrvMsg());
	}

void	Part::deactivate() noexcept{
	// Asynchronously issue CloseReq to
	// Hub and Ports. When finished,
	// release power,bw, and pipe resources
	// to HCD (e.g. releaseResources()) and finally
	// invoke deactivateDone() on self
	// (Dyn::Unit::Monitor<DynDevice>)
	// NOTE: To implement the CloseReq,
	// this class must either implement the
	// Close::Resp::Api or create a new type
	// of transaction that does.
	if(!_adaptor){
		releaseResources();
		return;
		}
	_currentHubPort	= _hubPorts.get();
	if(_currentHubPort){
		closeCurrentHubPort();
		return;
		}
	closeHubAdaptor();
	}

Oscl::Usb::Desc::Config*	Part::readConfigDesc() noexcept{
	Oscl::Usb::DynDevice&	dyndev	= _handle->getDynSrv();

	Oscl::Usb::Pipe::Message::SyncApi&
	syncPipe	= dyndev._ctrlPipe.getSyncApi();
	const Oscl::Usb::Pipe::Status::Result*	failed;

	unsigned char*		rawMem	= (unsigned char*)&_packetMem._descMem;

	Oscl::Usb::Setup::GetConfigDesc*
	getConfigSetup	= new(&_setupMem)
						Oscl::Usb::Setup::
						GetConfigDesc(	0, // desc index
										0, // lang ID
										9
										);

	failed	= syncPipe.read(*getConfigSetup,rawMem);
	if(failed){
		Oscl::Error::Info::log(	"Oscl::Equip::CP::USB::Shelf:\n"
								"Device config read error.\n"
								);
		if(failed != &Oscl::Usb::Pipe::Status::getStall()){
			return 0;
			}
		}


	Oscl::Usb::Desc::Config*
	cdesc	= (Oscl::Usb::Desc::Config*)rawMem;

	getConfigSetup	= new(&_setupMem)
						Oscl::Usb::Setup::
						GetConfigDesc(	0, // desc index
										0, // lang ID
//										sizeof(DescMem)
										cdesc->_wTotalLength
										);

	failed	= syncPipe.read(*getConfigSetup,rawMem);
	if(failed){
		Oscl::Error::Info::log(	"Oscl::Equip::CP::USB::Shelf:\n"
								"Device config read error.\n"
								);
		if(failed != &Oscl::Usb::Pipe::Status::getStall()){
			return 0;
			}
		}

	Oscl::Usb::Desc::Descriptor*
	desc	= (Oscl::Usb::Desc::Descriptor*)rawMem;

	using namespace Oscl::Usb::Hw::Desc;

	if(desc->_bDescriptorType != bDescriptorType::CONFIGURATION){
		Oscl::Error::Info::log(	"Oscl::Equip::CP::USB::Shelf:\n"
								"Invalid descriptor type.\n"
								);
		return 0;
		}

	Oscl::Usb::Desc::Config*
	configDesc	= (Oscl::Usb::Desc::Config*)desc;

	return configDesc;
	}

Oscl::Usb::Desc::Interface*
Part::getInterface(	Oscl::Usb::Desc::
						Config*			config
						) noexcept{
	using namespace Oscl::Usb::Hw::Desc;
	Oscl::Usb::Desc::Descriptor*
	desc	= config->findDescriptor(	bDescriptorType::INTERFACE,
										0,
										sizeof(DescMem)
										);
	if(!desc){
		Oscl::Error::Info::log(	"Oscl::Equip::CP::USB::Shelf:\n"
								"Device has no HID descriptor.\n"
								);
		return 0;
		}
	return (Oscl::Usb::Desc::Interface*)desc;
	}

Oscl::Usb::Desc::Endpoint*
Part::getEndpoint(	Oscl::Usb::Desc::
						Config*			config
						) noexcept{
	using namespace Oscl::Usb::Hw::Desc;
	Oscl::Usb::Desc::Descriptor*
	desc	= config->findDescriptor(	bDescriptorType::ENDPOINT,
										0,
										sizeof(DescMem)
										);
	if(!desc){
		// No Endpoint Descriptor found
		Oscl::Error::Info::log(	"Oscl::Equip::CP::USB::Shelf:\n"
								"Device has no endpoint descriptor.\n"
								);
		return 0;
		}
	return (Oscl::Usb::Desc::Endpoint*)desc;
	}

bool	Part::allocatePower() noexcept{
	Oscl::Usb::DynDevice&	dyndev	= _handle->getDynSrv();
	Oscl::Usb::Alloc::Power::SyncApi&
	power	= dyndev.getPowerSyncApi();

	bool	failed	= power.reserve(_reservedPower);
	if(failed){
		Oscl::Error::Info::log(	"Oscl::Equip::CP::USB::Shelf:\n"
								"Insufficient power for device.\n"
								);
		return false;
		}
	return true;
	}

bool	Part::allocateBandwidth(Oscl::Usb::Desc::Endpoint& desc) noexcept{
	Oscl::Usb::DynDevice&	dyndev	= _handle->getDynSrv();
	unsigned long	bwFreeState;
	_bwRec	=	dyndev.
				_hcdEnumMonitorApi.
				getBwInterruptAllocSyncApi().
				reserve(	desc._wMaxPacketSize,
							desc._bInterval,
							desc.directionIsIN(),
							bwFreeState
							);
	if(!_bwRec){
		Oscl::Error::Info::log(	"Oscl::Equip::CP::USB::Shelf:\n"
								"Not enough bandwidth available.\n"
								);
		return false;
		}
	return true;
	}

bool	Part::allocateInterruptInPipe(Oscl::Usb::Desc::Endpoint& desc) noexcept{
	Oscl::Usb::DynDevice&	dyndev	= _handle->getDynSrv();
	using namespace Oscl::Usb::Hw::Desc::Endpoint::bEndpointAddress;

	unsigned char	endpointID	= desc.getEndpointID();
	_hubStatusChangePipe	=
	dyndev.
	_hcdEnumMonitorApi.
	getInterruptInPipeAllocSyncApi().
	alloc(	*_bwRec,
			dyndev._usbAddress,
			endpointID,
			desc._wMaxPacketSize,
			dyndev._lowSpeedDevice
			);
	if(!_hubStatusChangePipe){
		Oscl::Error::Info::log(	"Oscl::Equip::CP::USB::Shelf:\n"
								"Cant allocate interrupt IN pipe.\n"
								);
		return false;
		}
	return true;
	}

bool	Part::setHubConfiguration(Oscl::Usb::Desc::Config& desc) noexcept{
	Oscl::Usb::DynDevice&	dyndev	= _handle->getDynSrv();
	Oscl::Usb::Pipe::Message::SyncApi&
	syncPipe	= dyndev._ctrlPipe.getSyncApi();
	bool	failed;

	Oscl::Usb::Setup::SetConfiguration*
	setConfigSetup	= new(&_setupMem)
						Oscl::Usb::Setup::
						SetConfiguration(desc._bConfigurationValue);

	failed	= syncPipe.control(*setConfigSetup);
	if(failed){
		Oscl::Error::Info::log(	"Oscl::Equip::CP::USB::Shelf:\n"
								"Error trying to set device configuration.\n"
								);
		return false;
		}
	return true;
	}

Oscl::Usb::Desc::Hub::Header*	Part::readHubDescHeader() noexcept{
	Oscl::Usb::DynDevice&	dyndev	= _handle->getDynSrv();
	Oscl::Usb::Pipe::Message::SyncApi&
	syncPipe	= dyndev._ctrlPipe.getSyncApi();
	bool	failed;

	unsigned char*
	rawMem	= (unsigned char*)&_packetMem._descMem;

	Oscl::Usb::Setup::Hub::GetHubDesc*
	getHubDescSetup	= new(&_setupMem)
						Oscl::Usb::Setup::Hub::
						GetHubDesc(	0, // lang ID
//									sizeof(DescMem)
									5
									);

	failed	= syncPipe.read(*getHubDescSetup,rawMem);
	if(failed){
		Oscl::Error::Info::log(	"Oscl::Equip::CP::USB::Shelf:\n"
								"Device config read error.\n"
								);
		return 0;
		}

	Oscl::Usb::Desc::Descriptor*
	desc	= (Oscl::Usb::Desc::Descriptor*)rawMem;

	unsigned	length	= (desc->_bLength > sizeof(DescMem))?
					sizeof(DescMem):
					desc->_bLength;

	getHubDescSetup	= new(&_setupMem)
						Oscl::Usb::Setup::Hub::
						GetHubDesc(	0, // lang ID
//									sizeof(DescMem)
									length
									);

	failed	= syncPipe.read(*getHubDescSetup,rawMem);
	if(failed){
		Oscl::Error::Info::log(	"Oscl::Equip::CP::USB::Shelf:\n"
								"Device config read error.\n"
								);
		return 0;
		}

	using namespace Oscl::Usb::Hw::Desc;

	if(desc->_bDescriptorType != bDescriptorType::HUB){
		Oscl::Error::Info::log(	"Oscl::Equip::CP::USB::Shelf:\n"
								"Invalid hub descriptor type.\n"
								);
		return 0;
		}

	return (Oscl::Usb::Desc::Hub::Header*)desc;
	}

void	Part::processHubPortDescriptors(Oscl::Usb::Desc::Hub::Header* desc) noexcept{
	unsigned char*	rawMem	= (unsigned char*)desc;

	uint8_t	nPorts		= desc->_bNbrPorts;

	unsigned	nPortBytes	= (nPorts+7)/8;

	Oscl::Usb::Desc::Hub::DeviceRemovable*
	dr	= (Oscl::Usb::Desc::Hub::DeviceRemovable*)&rawMem[7];

	Oscl::Usb::Desc::Hub::PortPwrCtrlMask*
	ppcm	= (Oscl::Usb::Desc::Hub::PortPwrCtrlMask*)&rawMem[7+nPortBytes];

	unsigned	port=0;
	for(unsigned i=0;i<nPortBytes;++i){
		for(unsigned j=0;(j<8)&&(port<nPorts);++j,++port){
			addPort(	(dr[i]._bDeviceRemovable & (1<<((port+1)%8))),
						(ppcm[i]._bPortPwrCtrlMask & (1<<((port+1)%8)))
						);
			}
		}

	}

bool	Part::readEndpointStatus(unsigned endpointID) noexcept{
	Oscl::Usb::DynDevice&	dyndev	= _handle->getDynSrv();
	Oscl::Usb::Pipe::Message::SyncApi&
	syncPipe	= dyndev._ctrlPipe.getSyncApi();
	bool	failed;

	unsigned char*	rawMem	= (unsigned char*)&_packetMem._descMem;

	Oscl::Usb::Setup::GetEndpointStatus*
	getEpStatusSetup	= new(&_setupMem)
							Oscl::Usb::Setup::
							GetEndpointStatus(endpointID);

	failed	= syncPipe.read(*getEpStatusSetup,rawMem);
	if(failed){
		Oscl::Error::Info::log(	"Oscl::Equip::CP::USB::Shelf:\n"
								"Error reading endpoint status.\n"
								);
		return false;
		}
	return true;
	}

bool	Part::readHubStatus() noexcept{
	Oscl::Usb::DynDevice&	dyndev	= _handle->getDynSrv();
	Oscl::Usb::Pipe::Message::SyncApi&
	syncPipe	= dyndev._ctrlPipe.getSyncApi();
	bool	failed;

	unsigned char*	rawMem	= (unsigned char*)&_packetMem._descMem;

	Oscl::Usb::Setup::Hub::GetHubStatus*
	getHubStatusSetup	= new(&_setupMem)
							Oscl::Usb::Setup::Hub::
							GetHubStatus();

	failed	= syncPipe.read(*getHubStatusSetup,rawMem);
	if(failed){
		Oscl::Error::Info::log(	"Oscl::Equip::CP::USB::Shelf:\n"
								"Error reading hub status.\n"
								);
		return false;
		}
	return true;
	}

void	Part::createAdaptor(unsigned char nPorts) noexcept{
	Oscl::Usb::DynDevice&	dyndev	= _handle->getDynSrv();
	_adaptor	= new(&_hubAdaptorMem)
					Oscl::Usb::Hub::
					Adaptor::Driver(	_hubStatusChangePipe->getSAP(),
										dyndev._ctrlPipe.getSAP(),
										_hubPapi,
										_adaptorSetupMem,
										_adaptorPacketMem,
										_portSetupMem,
										_portPacketMem,
										_portAdaptorMem,
										_maxPorts,
										nPorts
										);
	_adaptor->syncOpen();
	}

void	Part::createHubPorts(unsigned char nPorts) noexcept{
	Oscl::Usb::DynDevice&	dyndev	= _handle->getDynSrv();
	for(unsigned i=0;i<nPorts;++i){
		Oscl::ObjectID::Fixed<32>	location;
		static_cast<Oscl::ObjectID::Api&>(location)	= dyndev._location;
		location	+= _oidType;
		location	+= i;
		Oscl::Usb::Hub::Port::Driver*
		driver	= new(&_portDriverMem[i])
					Oscl::Usb::Hub::
					Port::Driver(	dyndev._hcdEnumDcpApi.getHubPortSAP(),
									*_adaptor->getPortSAP(i),
									dyndev._hcdEnumDcpApi.getMsgPipeAllocSAP(),
									_advCapi,
									_advRapi,
									_advSAP,
									dyndev._powerSAP,
									dyndev._powerSyncApi,
									dyndev._hcdEnumMonitorApi,
									dyndev._hcdEnumDcpApi,
									_dynSrvPapi,
									dyndev._hcdEnumDcpApi.
									getHsPipeApi().getSAP(),
									dyndev._hcdEnumDcpApi.
									getLsPipeApi().getSAP(),
									_delayServiceSAP,
									_hubPapi,
									_portDriverPacketMem[i],
									_portDriverSetupMem[i],
									&location
									);
		driver->syncOpen();
		_hubPorts.put(driver);
		}

	}

void	Part::response(	Oscl::Mt::Itc::Srv::Close::
							Resp::Api::CloseResp&		msg
							) noexcept{
	if(_currentHubPort){
		_currentHubPort->~Driver();
		_currentHubPort	= _hubPorts.get();
		if(_currentHubPort){
			closeCurrentHubPort();
			}
		else{
			closeHubAdaptor();
			}
		return;
		}
	_adaptor	= 0;
	// Hub ports and adaptor are closed.
	// Now release resources and finish deactivation.
	releaseResources();
	}

void	Part::response(	Oscl::Mt::Itc::Srv::Open::
							Resp::Api::OpenResp&		msg
							) noexcept{
	// Since I do opens synchronously, this
	// operation should never get called.
	for(;;);
	}

