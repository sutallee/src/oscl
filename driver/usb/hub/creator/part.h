/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_usb_hub_creator_parth_
#define _oscl_drv_usb_hub_creator_parth_
#include "oscl/driver/usb/device/dyndev.h"
#include "oscl/mt/itc/dyn/adv/cli.h"
#include "oscl/driver/usb/alloc/power/respapi.h"
#include "oscl/driver/usb/setup/mem.h"
#include "oscl/driver/usb/desc/config.h"
#include "oscl/driver/usb/desc/interface.h"
#include "oscl/driver/usb/desc/endpoint.h"
#include "oscl/driver/usb/hub/adaptor/driver.h"
#include "oscl/driver/usb/hub/port/driver/driver.h"
#include "oscl/driver/usb/desc/hub.h"
#include "oscl/mt/itc/delay/reqapi.h"
#include "oscl/mt/itc/srv/crespapi.h"
#include "oscl/done/api.h"

/** */
namespace Oscl {
/** */
namespace Usb {
/** */
namespace Hub {
/** */
namespace Creator {

/** This concrete class is a single USB HUB device creator.
	When the start() operation is invoked, the hub driver
	is instantiated and begins operation. When the stop()
	operation is invoked, the hub driver is shut-down
	asynchronously, and when it has been release, the
	stop-done callback is invoked to inform the context
	that the creator part may be detructed.
 */
class Part :
		public Oscl::Mt::Itc::Srv::Close::Resp::Api
		{
	public:
		/** */
		struct RawMem {
			/** Hub Desc Mem */
			/** Standard Desc Mem */
			unsigned char	mem[		sizeof(Oscl::Usb:: Desc::Config)
									+	sizeof(Oscl::Usb::Desc::Interface)
									+	sizeof(Oscl::Usb::Desc::Endpoint)
									];
			};
		/** */
		typedef Oscl::Memory::
				AlignedBlock<sizeof(RawMem)>		DescMem;
		/** */
		union PacketMem {
			/** */
			void*								__qitemlink;
			/** */
			DescMem								_descMem;
			};

		/** */
		typedef	Oscl::Memory::
			AlignedBlock<	sizeof(	Oscl::Usb::Hub::
									Adaptor::Driver
									)
							>			HubAdaptorMem;

		/** */
		typedef	Oscl::Memory::
			AlignedBlock<	sizeof(	Oscl::Usb::Hub::Port::
									Adaptor::Port
									)
							>			PortAdaptorMem;

		/** */
		typedef	Oscl::Memory::
			AlignedBlock<	sizeof(	Oscl::Usb::Hub::
									Port::Driver
									)
							>			PortDriverMem;

	private:
		/** */
		Oscl::Mt::Itc::Dyn::
		Handle<Oscl::Usb::DynDevice>			_handle;

		/** */
		Oscl::Mt::Itc::Srv::
		Close::Resp::CloseMem					_closeMem;
		/** */
		HubAdaptorMem							_hubAdaptorMem;
		/** */
		Oscl::Mt::Itc::Dyn::Adv::Cli::
		Req::Api<Oscl::Usb::DynDevice>::SAP&	_advSAP;
		/** */
		Oscl::Mt::Itc::Dyn::Adv::
		Creator::SyncApi<Oscl::Usb::DynDevice>&	_advCapi;
		/** */
		Oscl::Mt::Itc::Dyn::Adv::
		Core<Oscl::Usb::DynDevice>::ReleaseApi&	_advRapi;
		/** */
		Oscl::Mt::Itc::Delay::Req::Api::SAP&	_delayServiceSAP;
		/** */
		Oscl::Mt::Itc::PostMsgApi&				_myPapi;
		/** */
		Oscl::Usb::Setup::Mem&					_setupMem;
		/** */
		PacketMem&								_packetMem;
		/** */
		Oscl::Usb::Hub::Adaptor::
		Driver::SetupMem&						_adaptorSetupMem;
		/** */
		Oscl::Usb::Hub::Adaptor::
		Driver::PacketMem&						_adaptorPacketMem;
		/** */
		PortDriverMem*							_portDriverMem;
		/** */
		PortAdaptorMem*							_portAdaptorMem;
		/** */
		Oscl::Usb::Hub::Port::Adaptor::
		Port::SetupMem*							_portSetupMem;
		/** */
		Oscl::Usb::Hub::Port::Adaptor::
		Port::PacketMem*						_portPacketMem;
		/** */
		Oscl::Usb::Hub::Port::
		Driver::SetupPktMem*					_portDriverSetupMem;
		/** */
		Oscl::Usb::Hub::Port::
		Driver::PacketMem*						_portDriverPacketMem;
		/** */
		const unsigned							_maxPorts;
		/** */
		const unsigned							_oidType;
		/** */
		Oscl::Mt::Itc::PostMsgApi&				_hubPapi;
		/** */
		Oscl::Mt::Itc::PostMsgApi&				_dynSrvPapi;
		/** */
		unsigned								_reservedPower;
		/** */
		Oscl::Usb::Alloc::
		BW::Interrupt::BwRec*					_bwRec;
		/** */
		Oscl::Usb::Pipe::
		Interrupt::IN::PipeApi*					_hubStatusChangePipe;
		/** */
		Oscl::Queue<	Oscl::Usb::
						Hub::Port::Driver
						>						_hubPorts;
		/** */
		Oscl::Usb::Hub::Port::Driver*			_currentHubPort;
		/** */
		Oscl::Usb::Hub::Adaptor::Driver*		_adaptor;
		/** */
		Oscl::Done::Api*						_stopDone;
		
	public:
		/** */
		virtual ~Part(){}
		/** */
		Part(
					Oscl::Mt::Itc::Dyn::Adv::Cli::
					Req::Api<	Oscl::Usb::DynDevice
								>::SAP&						advSAP,
					Oscl::Mt::Itc::Dyn::Adv::
					Creator::SyncApi<Oscl::Usb::DynDevice>&	advCapi,
					Oscl::Mt::Itc::Dyn::Adv::
					Core<Oscl::Usb::DynDevice>::ReleaseApi&	advRapi,
					Oscl::Mt::Itc::Delay::Req::Api::SAP&	delayServiceSAP,
					Oscl::Mt::Itc::PostMsgApi&				myPapi,
					Oscl::Mt::Itc::PostMsgApi&				hubPapi,
					Oscl::Mt::Itc::PostMsgApi&				dynSrvPapi,
					Oscl::Usb::Setup::Mem&					setupMem,
					Oscl::Usb::Hub::Creator::Part::
					PacketMem&								packetMem,
					Oscl::Usb::Hub::Adaptor::
					Driver::SetupMem&						adaptorSetupMem,
					Oscl::Usb::Hub::Adaptor::
					Driver::PacketMem&						adaptorPacketMem,
					PortDriverMem*							portDriverMem,
					PortAdaptorMem*							portAdaptorMem,
					Oscl::Usb::Hub::Port::Adaptor::
					Port::SetupMem*							portSetupMem,
					Oscl::Usb::Hub::Port::Adaptor::
					Port::PacketMem*						portPacketMem,
					Oscl::Usb::Hub::Port::
					Driver::SetupPktMem*					portDriverSetupMem,
					Oscl::Usb::Hub::Port::
					Driver::PacketMem*						portDriverPacketMem,
					unsigned								maxPorts,
					unsigned								oidType
					) noexcept;

		/** */
		bool	start(	Oscl::Mt::Itc::
						Dyn::Handle<Oscl::Usb::DynDevice>	handle
						) noexcept;
		/** */
		void	stop(Oscl::Done::Api& done) noexcept;

	private:
		/** */
		void	addPort(bool nonRemovable,bool pwrControlMask) noexcept;
		/** */
		void	releaseResources() noexcept;
		/** */
		void	closeCurrentHubPort() noexcept;
		/** */
		void	closeHubAdaptor() noexcept;
	private:
		/** Returns true if activation/provisioning fails. */
		bool	activate() noexcept;
		/** */
		void	deactivate() noexcept;
	private: // helpers
		/** */
		Oscl::Usb::Desc::Config*	readConfigDesc() noexcept;
		/** */
		Oscl::Usb::Desc::Interface*	getInterface(	Oscl::Usb::Desc::
													Config*			config
													) noexcept;
		/** */
		Oscl::Usb::Desc::Endpoint*	getEndpoint(	Oscl::Usb::Desc::
													Config*			config
													) noexcept;

		/** */
		bool	allocatePower() noexcept;
		/** */
		bool	allocateBandwidth(Oscl::Usb::Desc::Endpoint& desc) noexcept;
		/** */
		bool	allocateInterruptInPipe(	Oscl::Usb::Desc::
											Endpoint&			desc
											) noexcept;
		/** */
		bool	setHubConfiguration(Oscl::Usb::Desc::Config& desc) noexcept;
		/** */
		Oscl::Usb::Desc::Hub::Header*	readHubDescHeader() noexcept;
		/** */
		void	processHubPortDescriptors(	Oscl::Usb::Desc::
											Hub::Header*		desc
											) noexcept;
		/** */
		bool	readEndpointStatus(unsigned endpointID) noexcept;
		/** */
		bool	readHubStatus() noexcept;
		/** */
		void	createAdaptor(unsigned char nPorts) noexcept;
		/** */
		void	createHubPorts(unsigned char nPorts) noexcept;

	private:	// Oscl::Mt::Itc::Srv::Close::Resp::Api
		/** */
		void	response(	Oscl::Mt::Itc::Srv::Close::
							Resp::Api::CloseResp&		msg
							) noexcept;
		/** */
		void	response(	Oscl::Mt::Itc::Srv::Open::
							Resp::Api::OpenResp&		msg
							) noexcept;
	};

}
}
}
}

#endif

