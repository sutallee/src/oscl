/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_usb_hub_port_adaptor_porth_
#define _oscl_drv_usb_hub_port_adaptor_porth_
#include "oscl/queue/queueitem.h"
#include "oscl/queue/queue.h"
#include "oscl/memory/block.h"
#include "oscl/driver/usb/hub/port/reqapi.h"
#include "oscl/driver/usb/setup/hub/clrportcconn.h"
#include "oscl/driver/usb/setup/hub/clrportcenable.h"
#include "oscl/driver/usb/setup/hub/clrportcoc.h"
#include "oscl/driver/usb/setup/hub/clrportconn.h"
#include "oscl/driver/usb/setup/hub/clrportcreset.h"
#include "oscl/driver/usb/setup/hub/clrportcsuspend.h"
#include "oscl/driver/usb/setup/hub/clrportenable.h"
#include "oscl/driver/usb/setup/hub/clrportoc.h"
#include "oscl/driver/usb/setup/hub/clrportpower.h"
#include "oscl/driver/usb/setup/hub/clrportreset.h"
#include "oscl/driver/usb/setup/hub/clrportsuspend.h"
#include "oscl/driver/usb/setup/hub/getportstatus.h"
#include "oscl/driver/usb/setup/hub/setportconn.h"
#include "oscl/driver/usb/setup/hub/setportenable.h"
#include "oscl/driver/usb/setup/hub/setportoc.h"
#include "oscl/driver/usb/setup/hub/setportpower.h"
#include "oscl/driver/usb/setup/hub/setportreset.h"
#include "oscl/driver/usb/setup/hub/setportsuspend.h"
#include "oscl/hw/usb/hub/port/packet.h"
#include "trans.h"
#include "oscl/driver/usb/hub/adaptor/portapi.h"

/** */
namespace Oscl {
/** */
namespace Usb {
/** */
namespace Hub {
/** */
namespace Port {
/** */
namespace Adaptor {

/** */
class Port :	public Oscl::Usb::Hub::Port::Req::Api,
				public Oscl::QueueItem
				{
	private:
		/** */
		typedef Oscl::Memory::AlignedBlock<	sizeof(GetStatusTrans)
											>	GetStatusTransMem;
		/** */
		union TransMem {
			/** */
			void*				__qitemlink;
			/** */
			GetStatusTransMem	getStatus;
			/** */
			Oscl::Memory::
			AlignedBlock<	sizeof(ClearConnChangeTrans)
							>	clrConnChange;
			/** */
			Oscl::Memory::
			AlignedBlock<	sizeof(ClearEnableChangeTrans)
							>	clrEnableChange;
			/** */
			Oscl::Memory::
			AlignedBlock<	sizeof(ClearOverCurrentChangeTrans)
							>	clrOverCurrentChange;
			/** */
			Oscl::Memory::
			AlignedBlock<	sizeof(ClearResetChangeTrans)
							>	clrResetChange;
			/** */
			Oscl::Memory::
			AlignedBlock<	sizeof(ClearSuspendChangeTrans)
							>	clrSuspendChange;
			/** */
			Oscl::Memory::
			AlignedBlock<	sizeof(ClearEnableTrans)
							>	clrEnable;
			/** */
			Oscl::Memory::
			AlignedBlock<	sizeof(ClearSuspendTrans)
							>	clrSuspend;
			/** */
			Oscl::Memory::
			AlignedBlock<	sizeof(ClearPowerTrans)
							>	clrPower;
			/** */
			Oscl::Memory::
			AlignedBlock<	sizeof(SetResetTrans)
							>	setReset;
			/** */
			Oscl::Memory::
			AlignedBlock<	sizeof(SetSuspendTrans)
							>	setSuspend;
			/** */
			Oscl::Memory::
			AlignedBlock<	sizeof(SetPowerTrans)
							>	setPower;
			};
		/** */
		union TransSetupMem {
			/** */
			GetStatusTrans::SetupMem				getStatus;
			/** */
			ClearConnChangeTrans::SetupMem			clrConnChange;
			/** */
			ClearEnableChangeTrans::SetupMem		clrEnableChange;
			/** */
			ClearOverCurrentChangeTrans::SetupMem	clrOverCurrentChange;
			/** */
			ClearResetChangeTrans::SetupMem			clrResetChange;
			/** */
			ClearSuspendChangeTrans::SetupMem		clrSuspendChange;
			/** */
			ClearEnableTrans::SetupMem				clrEnable;
			/** */
			ClearSuspendTrans::SetupMem				clrSuspend;
			/** */
			ClearPowerTrans::SetupMem				clrPower;
			/** */
			SetResetTrans::SetupMem					setReset;
			/** */
			SetSuspendTrans::SetupMem				setSuspend;
			/** */
			SetPowerTrans::SetupMem					setPower;
			};
		/** */
		union ContextSetupMem {
			/** */
			GetPortStatusTrans::SetupMem			getPortStatus;
			};

	public:
		/** */
		struct SetupMem {
			/** */
			TransSetupMem				trans;
			/** */
			ContextSetupMem				context;
			};
		/** */
		union TransPacketMem {
			/** */
			GetStatusTrans::PacketMem	getStatus;
			};
		/** */
		union ContextPacketMem {
			/** */
			GetPortStatusTrans::PacketMem	getPortStatus;
			};

	public:
		/** */
		struct PacketMem {
			/** */
			TransPacketMem				trans;
			/** */
			ContextPacketMem			context;
			};

		/** */
		union CloseMem {
			/** */
			Oscl::Usb::Pipe::Message::Resp::CancelMem	irp;
			};

	private:
		/** */
		GetStatusTransMem								_statusMem;
		/** */
		TransMem										_transMem;
		/** */
		Oscl::Usb::Pipe::Message::Resp::CancelMem		_cancelMem;
		/** */
		Oscl::Usb::Hub::Adaptor::PortApi&				_context;
		/** */
		Oscl::Usb::Pipe::Message::Req::Api::SAP&		_pipeSAP;
		/** */
		SetupMem&										_setupMem;
		/** */
		PacketMem&										_packetMem;
		/** */
		Oscl::Mt::Itc::PostMsgApi&						_myPapi;
		/** */
		Oscl::Usb::Hub::Port::Req::Api::ConcreteSAP		_hubPortSAP;
		/** */
		Oscl::Queue<NotifyPortChangeReq>				_notificationList;
		/** */
		Oscl::BitField<	Oscl::Usb::Hw::Hub::Port::
						wPortChange::Reg
						>								_portChangeStatus;
		/** */
		const unsigned									_portNum;
		/** */
		CancelReq*										_cancelReq;
		/** */
		TransApi*										_contextTrans;
		/** */
		TransApi*										_currentTrans;
		/** */
		CloseMem*										_closeMem;
		/** */
		bool											_contextNotifyEnabled;
		/** */
		bool											_open;
		/** */
		bool											_closing;

	public:
		/** */
		Port(	Oscl::Usb::Hub::
				Adaptor::PortApi&				context,
				Oscl::Usb::Pipe::Message::
				Req::Api::SAP&					pipeSAP,
				SetupMem&						setupMem,
				PacketMem&						packetMem,
				Oscl::Mt::Itc::PostMsgApi&		myPapi,
				unsigned						portNum
				) noexcept;

		/** */
		virtual ~Port(){}

		/** */
		void	open() noexcept;

		/** */
		void	close(CloseMem& mem) noexcept;

		/** */
		void	cancelNext() noexcept;

		/** */
		void	statusChange() noexcept;

		/** */
		Oscl::Usb::Hub::Port::Req::Api::SAP&	getHubPortSAP() noexcept;

	private:
		/** */
		void	clearPortPower() noexcept;

		/** */
		void		sendGetPortStatusIRP() noexcept;

		/** */
		void		notifyChanged() noexcept;

		/** */
		void		changeUpdate(	Oscl::Usb::Hw::Hub::Port::
									wPortChange::Reg			change
									) noexcept;

		/** */
		unsigned	getPortNum() noexcept;

	private:
		friend class GetPortStatusTrans;
		/** */
		void	canceled(GetPortStatusTrans& trans) noexcept;
		/** */
		void	failed(GetPortStatusTrans& trans) noexcept;
		/** */
		void	done(	GetPortStatusTrans&					trans,
						Oscl::Usb::Hw::Hub::Port::Packet&	packet
						) noexcept;

	private:
		friend class GetStatusTrans;
		/** */
		void	canceled(GetStatusTrans& trans) noexcept;
		/** */
		void	failed(GetStatusTrans& trans) noexcept;
		/** */
		void	done(	GetStatusTrans&						trans,
						Oscl::Usb::Hw::Hub::Port::Packet&	packet
						) noexcept;

	private:
		friend class ClearConnChangeTrans;
		/** */
		void	canceled(ClearConnChangeTrans& trans) noexcept;
		/** */
		void	failed(ClearConnChangeTrans& trans) noexcept;
		/** */
		void	done(ClearConnChangeTrans& trans) noexcept;

	private:
		friend class ClearEnableChangeTrans;
		/** */
		void	canceled(ClearEnableChangeTrans& trans) noexcept;
		/** */
		void	failed(ClearEnableChangeTrans& trans) noexcept;
		/** */
		void	done(ClearEnableChangeTrans& trans) noexcept;

	private:
		friend class ClearOverCurrentChangeTrans;
		/** */
		void	canceled(ClearOverCurrentChangeTrans& trans) noexcept;
		/** */
		void	failed(ClearOverCurrentChangeTrans& trans) noexcept;
		/** */
		void	done(ClearOverCurrentChangeTrans& trans) noexcept;

	private:
		friend class ClearResetChangeTrans;
		/** */
		void	canceled(ClearResetChangeTrans& trans) noexcept;
		/** */
		void	failed(ClearResetChangeTrans& trans) noexcept;
		/** */
		void	done(ClearResetChangeTrans& trans) noexcept;

	private:
		friend class ClearSuspendChangeTrans;
		/** */
		void	canceled(ClearSuspendChangeTrans& trans) noexcept;
		/** */
		void	failed(ClearSuspendChangeTrans& trans) noexcept;
		/** */
		void	done(ClearSuspendChangeTrans& trans) noexcept;

	private:
		friend class ClearEnableTrans;
		/** */
		void	canceled(ClearEnableTrans& trans) noexcept;
		/** */
		void	failed(ClearEnableTrans& trans) noexcept;
		/** */
		void	done(ClearEnableTrans& trans) noexcept;

	private:
		friend class ClearSuspendTrans;
		/** */
		void	canceled(ClearSuspendTrans& trans) noexcept;
		/** */
		void	failed(ClearSuspendTrans& trans) noexcept;
		/** */
		void	done(ClearSuspendTrans& trans) noexcept;

	private:
		friend class ClearPowerTrans;
		/** */
		void	canceled(ClearPowerTrans& trans) noexcept;
		/** */
		void	failed(ClearPowerTrans& trans) noexcept;
		/** */
		void	done(ClearPowerTrans& trans) noexcept;

	private:
		friend class SetResetTrans;
		/** */
		void	canceled(SetResetTrans& trans) noexcept;
		/** */
		void	failed(SetResetTrans& trans) noexcept;
		/** */
		void	done(SetResetTrans& trans) noexcept;

	private:
		friend class SetSuspendTrans;
		/** */
		void	canceled(SetSuspendTrans& trans) noexcept;
		/** */
		void	failed(SetSuspendTrans& trans) noexcept;
		/** */
		void	done(SetSuspendTrans& trans) noexcept;

	private:
		friend class SetPowerTrans;
		/** */
		void	canceled(SetPowerTrans& trans) noexcept;
		/** */
		void	failed(SetPowerTrans& trans) noexcept;
		/** */
		void	done(SetPowerTrans& trans) noexcept;

	private:	// Oscl::Usb::Hub::Port::Req::Api
		/** */
		void	request(SetResetReq& msg) noexcept;
		/** */
		void	request(SetSuspendReq& msg) noexcept;
		/** */
		void	request(SetPowerReq& msg) noexcept;
		/** */
		void	request(ClearEnableReq& msg) noexcept;
		/** */
		void	request(ClearSuspendReq& msg) noexcept;
		/** */
		void	request(ClearPowerReq& msg) noexcept;
		/** */
		void	request(ClearConnectChangeReq& msg) noexcept;
		/** */
		void	request(ClearResetChangeReq& msg) noexcept;
		/** */
		void	request(ClearEnableChangeReq& msg) noexcept;
		/** */
		void	request(ClearSuspendChangeReq& msg) noexcept;
		/** */
		void	request(ClearOverCurrentChangeReq& msg) noexcept;
		/** */
		void	request(CancelReq& msg) noexcept;
		/** */
		void	request(NotifyPortChangeReq& msg) noexcept;
		/** */
		void	request(CancelNotifyPortChangeReq& msg) noexcept;
		/** */
		void	request(GetPortStatusReq& msg) noexcept;
	};

}
}
}
}
}


#endif

