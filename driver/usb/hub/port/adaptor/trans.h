/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_usb_hub_port_adaptor_transh_
#define _oscl_drv_usb_hub_port_adaptor_transh_
#include "oscl/queue/queueitem.h"
#include "oscl/queue/queue.h"
#include "oscl/memory/block.h"
#include "oscl/driver/usb/hub/port/reqapi.h"
#include "oscl/driver/usb/setup/hub/clrportcconn.h"
#include "oscl/driver/usb/setup/hub/clrportcenable.h"
#include "oscl/driver/usb/setup/hub/clrportcoc.h"
#include "oscl/driver/usb/setup/hub/clrportconn.h"
#include "oscl/driver/usb/setup/hub/clrportcreset.h"
#include "oscl/driver/usb/setup/hub/clrportcsuspend.h"
#include "oscl/driver/usb/setup/hub/clrportenable.h"
#include "oscl/driver/usb/setup/hub/clrportoc.h"
#include "oscl/driver/usb/setup/hub/clrportpower.h"
#include "oscl/driver/usb/setup/hub/clrportreset.h"
#include "oscl/driver/usb/setup/hub/clrportsuspend.h"
#include "oscl/driver/usb/setup/hub/getportstatus.h"
#include "oscl/driver/usb/setup/hub/setportconn.h"
#include "oscl/driver/usb/setup/hub/setportenable.h"
#include "oscl/driver/usb/setup/hub/setportoc.h"
#include "oscl/driver/usb/setup/hub/setportpower.h"
#include "oscl/driver/usb/setup/hub/setportreset.h"
#include "oscl/driver/usb/setup/hub/setportsuspend.h"
#include "oscl/hw/usb/hub/port/packet.h"
#include "oscl/driver/usb/pipe/message/respmem.h"

/** */
namespace Oscl {
/** */
namespace Usb {
/** */
namespace Hub {
/** */
namespace Port {
/** */
namespace Adaptor {

/** */
class Port;

/** */
class TransApi :	public Oscl::QueueItem {
	public:
		/** */
		virtual ~TransApi(){};

		virtual void	cancel(	Oscl::Usb::Pipe::
								Message::Resp::CancelMem&	mem
								) noexcept=0;
	};

/** */
class GetPortStatusTrans :	private Oscl::Usb::Pipe::Message::Resp::Api,
							public TransApi
							{
	public:
		/** */
		union SetupMem {
			/** */
			Oscl::Memory::
			AlignedBlock<	sizeof(	Oscl::Usb::Setup::
									Hub::GetPortStatus
									)
							>	getPortStatus;
			};

		/** */
		union PacketMem {
			/** */
			Oscl::Memory::
			AlignedBlock<	sizeof(	Oscl::Usb::Hw::
									Hub::Port::Packet
									)
							>	portStatus;
			};
	private:
		/** */
		Oscl::Usb::Pipe::Message::Resp::ReadMem				_respMem;
		/** */
		Port&												_context;
		/** */
		Oscl::Usb::Pipe::Message::Req::Api::SAP&			_pipeSAP;
		/** */
		SetupMem&											_setupMem;
		/** */
		PacketMem&											_packetMem;
		/** */
		Oscl::Mt::Itc::PostMsgApi&							_myPapi;
		/** */
		Oscl::Usb::Pipe::Message::Resp::Api::ReadResp*		_resp;
		/** */
		bool												_canceling;
		/** */
		bool												_failed;

	public:
		/** */
		GetPortStatusTrans(	Port&							context,
							Oscl::Usb::Pipe::Message::
							Req::Api::SAP&					pipeSAP,
							SetupMem&						setupMem,
							PacketMem&						packetMem,
							Oscl::Mt::Itc::PostMsgApi&		myPapi
							) noexcept;

	private:	// TransApi
		/** */
		void	cancel(Oscl::Usb::Pipe::Message::Resp::CancelMem& mem) noexcept;

	private:	// Oscl::Usb::Pipe::Message::Resp::Api
		/** */
		void	response(	Oscl::Usb::Pipe::Message::
							Resp::Api::CancelResp&		msg
							) noexcept;
		/** */
		void	response(	Oscl::Usb::Pipe::Message::
							Resp::Api::NoDataResp&		msg
							) noexcept;
		/** */
		void	response(	Oscl::Usb::Pipe::Message::
							Resp::Api::ReadResp&		msg
							) noexcept;
		/** */
		void	response(	Oscl::Usb::Pipe::Message::
							Resp::Api::WriteResp&		msg
							) noexcept;
	};

/** */
class GetStatusTrans :	private Oscl::Usb::Pipe::Message::Resp::Api,
						public TransApi
						{
	public:
		/** */
		union SetupMem {
			/** */
			Oscl::Memory::
			AlignedBlock<	sizeof(	Oscl::Usb::Setup::
									Hub::GetPortStatus
									)
							>	getPortStatus;
			};

		/** */
		union PacketMem {
			/** */
			Oscl::Memory::
			AlignedBlock<	sizeof(	Oscl::Usb::Hw::
									Hub::Port::Packet
									)
							>	portStatus;
			};
	private:
		/** */
		Oscl::Usb::Pipe::Message::Resp::ReadMem				_respMem;
		/** */
		Port&												_context;
		/** */
		Oscl::Usb::Pipe::Message::Req::Api::SAP&			_pipeSAP;
		/** */
		Oscl::Usb::Hub::Port::Req::Api::GetPortStatusReq&	_req;
		/** */
		SetupMem&											_setupMem;
		/** */
		PacketMem&											_packetMem;
		/** */
		Oscl::Mt::Itc::PostMsgApi&							_myPapi;
		/** */
		Oscl::Usb::Pipe::Message::Resp::Api::ReadResp*		_resp;
		/** */
		bool												_canceling;
		/** */
		bool												_failed;

	public:
		/** */
		GetStatusTrans(	Port&							context,
						Oscl::Usb::Pipe::Message::
						Req::Api::SAP&					pipeSAP,
						Oscl::Usb::Hub::Port::
						Req::Api::GetPortStatusReq&		req,
						SetupMem&						setupMem,
						PacketMem&						packetMem,
						Oscl::Mt::Itc::PostMsgApi&		myPapi
						) noexcept;

	private:	// TransApi
		/** */
		void	cancel(Oscl::Usb::Pipe::Message::Resp::CancelMem& mem) noexcept;

	private:	// Oscl::Usb::Pipe::Message::Resp::Api
		/** */
		void	response(	Oscl::Usb::Pipe::Message::
							Resp::Api::CancelResp&		msg
							) noexcept;
		/** */
		void	response(	Oscl::Usb::Pipe::Message::
							Resp::Api::NoDataResp&		msg
							) noexcept;
		/** */
		void	response(	Oscl::Usb::Pipe::Message::
							Resp::Api::ReadResp&		msg
							) noexcept;
		/** */
		void	response(	Oscl::Usb::Pipe::Message::
							Resp::Api::WriteResp&		msg
							) noexcept;
	};

/** */
class ClearConnChangeTrans :	private Oscl::Usb::Pipe::Message::Resp::Api,
								public TransApi
								{
	public:
		/** */
		union SetupMem {
			/** */
			Oscl::Memory::
			AlignedBlock<	sizeof(	Oscl::Usb::Setup::
									Hub::ClearPortConnectChange
									)
							>	clearConnectChange;
			};
	private:
		/** */
		Oscl::Usb::Pipe::Message::Resp::NoDataMem			_respMem;
		/** */
		Port&												_context;
		/** */
		Oscl::Usb::Pipe::Message::Req::Api::SAP&			_pipeSAP;
		/** */
		Oscl::Usb::Hub::Port::
		Req::Api::ClearConnectChangeReq&					_req;
		/** */
		SetupMem&											_setupMem;
		/** */
		Oscl::Mt::Itc::PostMsgApi&							_myPapi;
		/** */
		Oscl::Usb::Pipe::Message::Resp::Api::NoDataResp*	_resp;
		/** */
		bool												_canceling;
		/** */
		bool												_failed;

	public:
		/** */
		ClearConnChangeTrans(	Port&								context,
								Oscl::Usb::Pipe::Message::
								Req::Api::SAP&						pipeSAP,
								Oscl::Usb::Hub::Port::
								Req::Api::ClearConnectChangeReq&	req,
								SetupMem&							setupMem,
								Oscl::Mt::Itc::PostMsgApi&			myPapi
								) noexcept;

	private:	// TransApi
		/** */
		void	cancel(Oscl::Usb::Pipe::Message::Resp::CancelMem& mem) noexcept;

	private:	// Oscl::Usb::Pipe::Message::Resp::Api
		/** */
		void	response(	Oscl::Usb::Pipe::Message::
							Resp::Api::CancelResp&		msg
							) noexcept;
		/** */
		void	response(	Oscl::Usb::Pipe::Message::
							Resp::Api::NoDataResp&		msg
							) noexcept;
		/** */
		void	response(	Oscl::Usb::Pipe::Message::
							Resp::Api::ReadResp&		msg
							) noexcept;
		/** */
		void	response(	Oscl::Usb::Pipe::Message::
							Resp::Api::WriteResp&		msg
							) noexcept;
	};

/** */
class ClearEnableChangeTrans :	private Oscl::Usb::Pipe::Message::Resp::Api,
								public TransApi
								{
	public:
		/** */
		union SetupMem {
			/** */
			Oscl::Memory::
			AlignedBlock<	sizeof(	Oscl::Usb::Setup::
									Hub::ClearPortEnableChange
									)
							>	clearEnableChange;
			};
	private:
		/** */
		Oscl::Usb::Pipe::Message::Resp::NoDataMem			_respMem;
		/** */
		Port&												_context;
		/** */
		Oscl::Usb::Pipe::Message::Req::Api::SAP&			_pipeSAP;
		/** */
		Oscl::Usb::Hub::Port::
		Req::Api::ClearEnableChangeReq&						_req;
		/** */
		SetupMem&											_setupMem;
		/** */
		Oscl::Mt::Itc::PostMsgApi&							_myPapi;
		/** */
		Oscl::Usb::Pipe::Message::Resp::Api::NoDataResp*	_resp;
		/** */
		bool												_canceling;
		/** */
		bool												_failed;

	public:
		/** */
		ClearEnableChangeTrans(	Port&								context,
								Oscl::Usb::Pipe::Message::
								Req::Api::SAP&						pipeSAP,
								Oscl::Usb::Hub::Port::
								Req::Api::ClearEnableChangeReq&		req,
								SetupMem&							setupMem,
								Oscl::Mt::Itc::PostMsgApi&			myPapi
								) noexcept;

	private:	// TransApi
		/** */
		void	cancel(Oscl::Usb::Pipe::Message::Resp::CancelMem& mem) noexcept;

	private:	// Oscl::Usb::Pipe::Message::Resp::Api
		/** */
		void	response(	Oscl::Usb::Pipe::Message::
							Resp::Api::CancelResp&		msg
							) noexcept;
		/** */
		void	response(	Oscl::Usb::Pipe::Message::
							Resp::Api::NoDataResp&		msg
							) noexcept;
		/** */
		void	response(	Oscl::Usb::Pipe::Message::
							Resp::Api::ReadResp&		msg
							) noexcept;
		/** */
		void	response(	Oscl::Usb::Pipe::Message::
							Resp::Api::WriteResp&		msg
							) noexcept;
	};

/** */
class ClearOverCurrentChangeTrans :	private Oscl::Usb::Pipe::Message::Resp::Api,
									public TransApi
									{
	public:
		/** */
		union SetupMem {
			/** */
			Oscl::Memory::
			AlignedBlock<	sizeof(	Oscl::Usb::Setup::
									Hub::ClearPortOverCurrentChange
									)
							>	clearOverCurrentChange;
			};
	private:
		/** */
		Oscl::Usb::Pipe::Message::Resp::NoDataMem			_respMem;
		/** */
		Port&												_context;
		/** */
		Oscl::Usb::Pipe::Message::Req::Api::SAP&			_pipeSAP;
		/** */
		Oscl::Usb::Hub::Port::
		Req::Api::ClearOverCurrentChangeReq&				_req;
		/** */
		SetupMem&											_setupMem;
		/** */
		Oscl::Mt::Itc::PostMsgApi&							_myPapi;
		/** */
		Oscl::Usb::Pipe::Message::Resp::Api::NoDataResp*	_resp;
		/** */
		bool												_canceling;
		/** */
		bool												_failed;

	public:
		/** */
		ClearOverCurrentChangeTrans(	Port&						context,
										Oscl::Usb::Pipe::Message::
										Req::Api::SAP&				pipeSAP,
										Oscl::Usb::Hub::Port::
										Req::Api::
										ClearOverCurrentChangeReq&	req,
										SetupMem&					setupMem,
										Oscl::Mt::Itc::PostMsgApi&	myPapi
										) noexcept;

	private:	// TransApi
		/** */
		void	cancel(Oscl::Usb::Pipe::Message::Resp::CancelMem& mem) noexcept;

	private:	// Oscl::Usb::Pipe::Message::Resp::Api
		/** */
		void	response(	Oscl::Usb::Pipe::Message::
							Resp::Api::CancelResp&		msg
							) noexcept;
		/** */
		void	response(	Oscl::Usb::Pipe::Message::
							Resp::Api::NoDataResp&		msg
							) noexcept;
		/** */
		void	response(	Oscl::Usb::Pipe::Message::
							Resp::Api::ReadResp&		msg
							) noexcept;
		/** */
		void	response(	Oscl::Usb::Pipe::Message::
							Resp::Api::WriteResp&		msg
							) noexcept;
	};

/** */
class ClearResetChangeTrans :	private Oscl::Usb::Pipe::Message::Resp::Api,
									public TransApi
									{
	public:
		/** */
		union SetupMem {
			/** */
			Oscl::Memory::
			AlignedBlock<	sizeof(	Oscl::Usb::Setup::
									Hub::ClearPortResetChange
									)
							>	clearResetChange;
			};
	private:
		/** */
		Oscl::Usb::Pipe::Message::Resp::NoDataMem			_respMem;
		/** */
		Port&												_context;
		/** */
		Oscl::Usb::Pipe::Message::Req::Api::SAP&			_pipeSAP;
		/** */
		Oscl::Usb::Hub::Port::
		Req::Api::ClearResetChangeReq&						_req;
		/** */
		SetupMem&											_setupMem;
		/** */
		Oscl::Mt::Itc::PostMsgApi&							_myPapi;
		/** */
		Oscl::Usb::Pipe::Message::Resp::Api::NoDataResp*	_resp;
		/** */
		bool												_canceling;
		/** */
		bool												_failed;

	public:
		/** */
		ClearResetChangeTrans(	Port&						context,
								Oscl::Usb::Pipe::Message::
								Req::Api::SAP&				pipeSAP,
								Oscl::Usb::Hub::Port::
								Req::Api::
								ClearResetChangeReq&		req,
								SetupMem&					setupMem,
								Oscl::Mt::Itc::PostMsgApi&	myPapi
								) noexcept;

	private:	// TransApi
		/** */
		void	cancel(Oscl::Usb::Pipe::Message::Resp::CancelMem& mem) noexcept;

	private:	// Oscl::Usb::Pipe::Message::Resp::Api
		/** */
		void	response(	Oscl::Usb::Pipe::Message::
							Resp::Api::CancelResp&		msg
							) noexcept;
		/** */
		void	response(	Oscl::Usb::Pipe::Message::
							Resp::Api::NoDataResp&		msg
							) noexcept;
		/** */
		void	response(	Oscl::Usb::Pipe::Message::
							Resp::Api::ReadResp&		msg
							) noexcept;
		/** */
		void	response(	Oscl::Usb::Pipe::Message::
							Resp::Api::WriteResp&		msg
							) noexcept;
	};

/** */
class ClearSuspendChangeTrans :	private Oscl::Usb::Pipe::Message::Resp::Api,
									public TransApi
									{
	public:
		/** */
		union SetupMem {
			/** */
			Oscl::Memory::
			AlignedBlock<	sizeof(	Oscl::Usb::Setup::
									Hub::ClearPortSuspendChange
									)
							>	clearSuspendChange;
			};
	private:
		/** */
		Oscl::Usb::Pipe::Message::Resp::NoDataMem			_respMem;
		/** */
		Port&												_context;
		/** */
		Oscl::Usb::Pipe::Message::Req::Api::SAP&			_pipeSAP;
		/** */
		Oscl::Usb::Hub::Port::
		Req::Api::ClearSuspendChangeReq&					_req;
		/** */
		SetupMem&											_setupMem;
		/** */
		Oscl::Mt::Itc::PostMsgApi&							_myPapi;
		/** */
		Oscl::Usb::Pipe::Message::Resp::Api::NoDataResp*	_resp;
		/** */
		bool												_canceling;
		/** */
		bool												_failed;

	public:
		/** */
		ClearSuspendChangeTrans(	Port&						context,
									Oscl::Usb::Pipe::Message::
									Req::Api::SAP&				pipeSAP,
									Oscl::Usb::Hub::Port::
									Req::Api::
									ClearSuspendChangeReq&		req,
									SetupMem&					setupMem,
									Oscl::Mt::Itc::PostMsgApi&	myPapi
									) noexcept;

	private:	// TransApi
		/** */
		void	cancel(Oscl::Usb::Pipe::Message::Resp::CancelMem& mem) noexcept;

	private:	// Oscl::Usb::Pipe::Message::Resp::Api
		/** */
		void	response(	Oscl::Usb::Pipe::Message::
							Resp::Api::CancelResp&		msg
							) noexcept;
		/** */
		void	response(	Oscl::Usb::Pipe::Message::
							Resp::Api::NoDataResp&		msg
							) noexcept;
		/** */
		void	response(	Oscl::Usb::Pipe::Message::
							Resp::Api::ReadResp&		msg
							) noexcept;
		/** */
		void	response(	Oscl::Usb::Pipe::Message::
							Resp::Api::WriteResp&		msg
							) noexcept;
	};

/** */
class ClearEnableTrans :	private Oscl::Usb::Pipe::Message::Resp::Api,
							public TransApi
							{
	public:
		/** */
		union SetupMem {
			/** */
			Oscl::Memory::
			AlignedBlock<	sizeof(	Oscl::Usb::Setup::
									Hub::ClearPortEnable
									)
							>	clearEnable;
			};
	private:
		/** */
		Oscl::Usb::Pipe::Message::Resp::NoDataMem			_respMem;
		/** */
		Port&												_context;
		/** */
		Oscl::Usb::Pipe::Message::Req::Api::SAP&			_pipeSAP;
		/** */
		Oscl::Usb::Hub::Port::
		Req::Api::ClearEnableReq&							_req;
		/** */
		SetupMem&											_setupMem;
		/** */
		Oscl::Mt::Itc::PostMsgApi&							_myPapi;
		/** */
		Oscl::Usb::Pipe::Message::Resp::Api::NoDataResp*	_resp;
		/** */
		bool												_canceling;
		/** */
		bool												_failed;

	public:
		/** */
		ClearEnableTrans(	Port&						context,
							Oscl::Usb::Pipe::Message::
							Req::Api::SAP&				pipeSAP,
							Oscl::Usb::Hub::Port::
							Req::Api::
							ClearEnableReq&				req,
							SetupMem&					setupMem,
							Oscl::Mt::Itc::PostMsgApi&	myPapi
							) noexcept;

	private:	// TransApi
		/** */
		void	cancel(Oscl::Usb::Pipe::Message::Resp::CancelMem& mem) noexcept;

	private:	// Oscl::Usb::Pipe::Message::Resp::Api
		/** */
		void	response(	Oscl::Usb::Pipe::Message::
							Resp::Api::CancelResp&		msg
							) noexcept;
		/** */
		void	response(	Oscl::Usb::Pipe::Message::
							Resp::Api::NoDataResp&		msg
							) noexcept;
		/** */
		void	response(	Oscl::Usb::Pipe::Message::
							Resp::Api::ReadResp&		msg
							) noexcept;
		/** */
		void	response(	Oscl::Usb::Pipe::Message::
							Resp::Api::WriteResp&		msg
							) noexcept;
	};

/** */
class ClearSuspendTrans :	private Oscl::Usb::Pipe::Message::Resp::Api,
							public TransApi
							{
	public:
		/** */
		union SetupMem {
			/** */
			Oscl::Memory::
			AlignedBlock<	sizeof(	Oscl::Usb::Setup::
									Hub::ClearPortSuspend
									)
							>	clearSuspend;
			};
	private:
		/** */
		Oscl::Usb::Pipe::Message::Resp::NoDataMem			_respMem;
		/** */
		Port&												_context;
		/** */
		Oscl::Usb::Pipe::Message::Req::Api::SAP&			_pipeSAP;
		/** */
		Oscl::Usb::Hub::Port::
		Req::Api::ClearSuspendReq&							_req;
		/** */
		SetupMem&											_setupMem;
		/** */
		Oscl::Mt::Itc::PostMsgApi&							_myPapi;
		/** */
		Oscl::Usb::Pipe::Message::Resp::Api::NoDataResp*	_resp;
		/** */
		bool												_canceling;
		/** */
		bool												_failed;

	public:
		/** */
		ClearSuspendTrans(	Port&						context,
							Oscl::Usb::Pipe::Message::
							Req::Api::SAP&				pipeSAP,
							Oscl::Usb::Hub::Port::
							Req::Api::
							ClearSuspendReq&			req,
							SetupMem&					setupMem,
							Oscl::Mt::Itc::PostMsgApi&	myPapi
							) noexcept;

	private:	// TransApi
		/** */
		void	cancel(Oscl::Usb::Pipe::Message::Resp::CancelMem& mem) noexcept;

	private:	// Oscl::Usb::Pipe::Message::Resp::Api
		/** */
		void	response(	Oscl::Usb::Pipe::Message::
							Resp::Api::CancelResp&		msg
							) noexcept;
		/** */
		void	response(	Oscl::Usb::Pipe::Message::
							Resp::Api::NoDataResp&		msg
							) noexcept;
		/** */
		void	response(	Oscl::Usb::Pipe::Message::
							Resp::Api::ReadResp&		msg
							) noexcept;
		/** */
		void	response(	Oscl::Usb::Pipe::Message::
							Resp::Api::WriteResp&		msg
							) noexcept;
	};

/** */
class ClearPowerTrans :	private Oscl::Usb::Pipe::Message::Resp::Api,
							public TransApi
							{
	public:
		/** */
		union SetupMem {
			/** */
			Oscl::Memory::
			AlignedBlock<	sizeof(	Oscl::Usb::Setup::
									Hub::ClearPortPower
									)
							>	clearPower;
			};
	private:
		/** */
		Oscl::Usb::Pipe::Message::Resp::NoDataMem			_respMem;
		/** */
		Port&												_context;
		/** */
		Oscl::Usb::Pipe::Message::Req::Api::SAP&			_pipeSAP;
		/** */
		Oscl::Usb::Hub::Port::
		Req::Api::ClearPowerReq&							_req;
		/** */
		SetupMem&											_setupMem;
		/** */
		Oscl::Mt::Itc::PostMsgApi&							_myPapi;
		/** */
		Oscl::Usb::Pipe::Message::Resp::Api::NoDataResp*	_resp;
		/** */
		bool												_canceling;
		/** */
		bool												_failed;

	public:
		/** */
		ClearPowerTrans(	Port&						context,
							Oscl::Usb::Pipe::Message::
							Req::Api::SAP&				pipeSAP,
							Oscl::Usb::Hub::Port::
							Req::Api::
							ClearPowerReq&				req,
							SetupMem&					setupMem,
							Oscl::Mt::Itc::PostMsgApi&	myPapi
							) noexcept;

	private:	// TransApi
		/** */
		void	cancel(Oscl::Usb::Pipe::Message::Resp::CancelMem& mem) noexcept;

	private:	// Oscl::Usb::Pipe::Message::Resp::Api
		/** */
		void	response(	Oscl::Usb::Pipe::Message::
							Resp::Api::CancelResp&		msg
							) noexcept;
		/** */
		void	response(	Oscl::Usb::Pipe::Message::
							Resp::Api::NoDataResp&		msg
							) noexcept;
		/** */
		void	response(	Oscl::Usb::Pipe::Message::
							Resp::Api::ReadResp&		msg
							) noexcept;
		/** */
		void	response(	Oscl::Usb::Pipe::Message::
							Resp::Api::WriteResp&		msg
							) noexcept;
	};

/** */
class SetResetTrans :	private Oscl::Usb::Pipe::Message::Resp::Api,
							public TransApi
							{
	public:
		/** */
		union SetupMem {
			/** */
			Oscl::Memory::
			AlignedBlock<	sizeof(	Oscl::Usb::Setup::
									Hub::ClearPortPower
									)
							>	setReset;
			};
	private:
		/** */
		Oscl::Usb::Pipe::Message::Resp::NoDataMem			_respMem;
		/** */
		Port&												_context;
		/** */
		Oscl::Usb::Pipe::Message::Req::Api::SAP&			_pipeSAP;
		/** */
		Oscl::Usb::Hub::Port::
		Req::Api::SetResetReq&								_req;
		/** */
		SetupMem&											_setupMem;
		/** */
		Oscl::Mt::Itc::PostMsgApi&							_myPapi;
		/** */
		Oscl::Usb::Pipe::Message::Resp::Api::NoDataResp*	_resp;
		/** */
		bool												_canceling;
		/** */
		bool												_failed;

	public:
		/** */
		SetResetTrans(	Port&						context,
						Oscl::Usb::Pipe::Message::
						Req::Api::SAP&				pipeSAP,
						Oscl::Usb::Hub::Port::
						Req::Api::
						SetResetReq&				req,
						SetupMem&					setupMem,
						Oscl::Mt::Itc::PostMsgApi&	myPapi
						) noexcept;

	private:	// TransApi
		/** */
		void	cancel(Oscl::Usb::Pipe::Message::Resp::CancelMem& mem) noexcept;

	private:	// Oscl::Usb::Pipe::Message::Resp::Api
		/** */
		void	response(	Oscl::Usb::Pipe::Message::
							Resp::Api::CancelResp&		msg
							) noexcept;
		/** */
		void	response(	Oscl::Usb::Pipe::Message::
							Resp::Api::NoDataResp&		msg
							) noexcept;
		/** */
		void	response(	Oscl::Usb::Pipe::Message::
							Resp::Api::ReadResp&		msg
							) noexcept;
		/** */
		void	response(	Oscl::Usb::Pipe::Message::
							Resp::Api::WriteResp&		msg
							) noexcept;
	};

/** */
class SetSuspendTrans :	private Oscl::Usb::Pipe::Message::Resp::Api,
							public TransApi
							{
	public:
		/** */
		union SetupMem {
			/** */
			Oscl::Memory::
			AlignedBlock<	sizeof(	Oscl::Usb::Setup::
									Hub::ClearPortPower
									)
							>	setSuspend;
			};
	private:
		/** */
		Oscl::Usb::Pipe::Message::Resp::NoDataMem			_respMem;
		/** */
		Port&												_context;
		/** */
		Oscl::Usb::Pipe::Message::Req::Api::SAP&			_pipeSAP;
		/** */
		Oscl::Usb::Hub::Port::
		Req::Api::SetSuspendReq&							_req;
		/** */
		SetupMem&											_setupMem;
		/** */
		Oscl::Mt::Itc::PostMsgApi&							_myPapi;
		/** */
		Oscl::Usb::Pipe::Message::Resp::Api::NoDataResp*	_resp;
		/** */
		bool												_canceling;
		/** */
		bool												_failed;

	public:
		/** */
		SetSuspendTrans(	Port&						context,
							Oscl::Usb::Pipe::Message::
							Req::Api::SAP&				pipeSAP,
							Oscl::Usb::Hub::Port::
							Req::Api::
							SetSuspendReq&				req,
							SetupMem&					setupMem,
							Oscl::Mt::Itc::PostMsgApi&	myPapi
							) noexcept;

	private:	// TransApi
		/** */
		void	cancel(Oscl::Usb::Pipe::Message::Resp::CancelMem& mem) noexcept;

	private:	// Oscl::Usb::Pipe::Message::Resp::Api
		/** */
		void	response(	Oscl::Usb::Pipe::Message::
							Resp::Api::CancelResp&		msg
							) noexcept;
		/** */
		void	response(	Oscl::Usb::Pipe::Message::
							Resp::Api::NoDataResp&		msg
							) noexcept;
		/** */
		void	response(	Oscl::Usb::Pipe::Message::
							Resp::Api::ReadResp&		msg
							) noexcept;
		/** */
		void	response(	Oscl::Usb::Pipe::Message::
							Resp::Api::WriteResp&		msg
							) noexcept;
	};

/** */
class SetPowerTrans :	private Oscl::Usb::Pipe::Message::Resp::Api,
							public TransApi
							{
	public:
		/** */
		union SetupMem {
			/** */
			Oscl::Memory::
			AlignedBlock<	sizeof(	Oscl::Usb::Setup::
									Hub::ClearPortPower
									)
							>	setPower;
			};
	private:
		/** */
		Oscl::Usb::Pipe::Message::Resp::NoDataMem			_respMem;
		/** */
		Port&												_context;
		/** */
		Oscl::Usb::Pipe::Message::Req::Api::SAP&			_pipeSAP;
		/** */
		Oscl::Usb::Hub::Port::
		Req::Api::SetPowerReq&								_req;
		/** */
		SetupMem&											_setupMem;
		/** */
		Oscl::Mt::Itc::PostMsgApi&							_myPapi;
		/** */
		Oscl::Usb::Pipe::Message::Resp::Api::NoDataResp*	_resp;
		/** */
		bool												_canceling;
		/** */
		bool												_failed;

	public:
		/** */
		SetPowerTrans(	Port&						context,
						Oscl::Usb::Pipe::Message::
						Req::Api::SAP&				pipeSAP,
						Oscl::Usb::Hub::Port::
						Req::Api::
						SetPowerReq&				req,
						SetupMem&					setupMem,
						Oscl::Mt::Itc::PostMsgApi&	myPapi
						) noexcept;

	private:	// TransApi
		/** */
		void	cancel(Oscl::Usb::Pipe::Message::Resp::CancelMem& mem) noexcept;

	private:	// Oscl::Usb::Pipe::Message::Resp::Api
		/** */
		void	response(	Oscl::Usb::Pipe::Message::
							Resp::Api::CancelResp&		msg
							) noexcept;
		/** */
		void	response(	Oscl::Usb::Pipe::Message::
							Resp::Api::NoDataResp&		msg
							) noexcept;
		/** */
		void	response(	Oscl::Usb::Pipe::Message::
							Resp::Api::ReadResp&		msg
							) noexcept;
		/** */
		void	response(	Oscl::Usb::Pipe::Message::
							Resp::Api::WriteResp&		msg
							) noexcept;
	};

}
}
}
}
}


#endif
