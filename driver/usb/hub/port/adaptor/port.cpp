/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <new>
#include "port.h"
#include "oscl/mt/thread.h"
#include "oscl/mt/itc/mbox/syncrh.h"

using namespace Oscl::Usb::Hub::Port::Adaptor;

Port::Port(	Oscl::Usb::Hub::
			Adaptor::PortApi&				context,
			Oscl::Usb::Pipe::Message::
			Req::Api::SAP&					pipeSAP,
			SetupMem&						setupMem,
			PacketMem&						packetMem,
			Oscl::Mt::Itc::PostMsgApi&		myPapi,
			unsigned						portNum
			) noexcept:
		_context(context),
		_pipeSAP(pipeSAP),
		_setupMem(setupMem),
		_packetMem(packetMem),
		_myPapi(myPapi),
		_hubPortSAP(*this,myPapi),
		_portNum(portNum),
		_cancelReq(0),
		_contextTrans(0),
		_currentTrans(0),
		_contextNotifyEnabled(true),
		_open(false),
		_closing(false)
		{
	_portChangeStatus.clearBits(~0);
	clearPortPower();
	}

void	Port::clearPortPower() noexcept{
	Oscl::Usb::Setup::Hub::ClearPortPower*
	setup	= new(&_setupMem)
				Oscl::Usb::Setup::Hub::
				ClearPortPower(_portNum);

	Oscl::Usb::Pipe::
	Message::Req::Api::NoDataPayload	payload(*setup);
	Oscl::Mt::Itc::SyncReturnHandler	srh;
	Oscl::Usb::Pipe::
	Message::Req::Api::NoDataReq		req(	_pipeSAP.getReqApi(),
												payload,
												srh
												);
	_pipeSAP.postSync(req);
	}

void	Port::open() noexcept{
	_open	= true;
	// nothing to do?
	}

void	Port::close(CloseMem& mem) noexcept{
	// * Change to closing state.
	// * Cancel any pending notifications?
	//	 No, these should have already been canceled
	//   by the client.
	// * Cancel any pending IRPs.
	//	 Client IRPs should have already been canceled.
	_closeMem	= &mem;
	_open		= false;
	_closing	= true;
	cancelNext();
	}

void	Port::cancelNext() noexcept{
	if(_currentTrans){
		// FIXME:	Logic Error
		// This should never happen since the client
		// should have been closed before this adaptor,
		// and that close should have canceled all
		// outstanding requests before completing.
		while(true);
		_currentTrans->cancel(_closeMem->irp);
		}
	if(_contextTrans){
		_contextTrans->cancel(_closeMem->irp);
		return;
		}
	_closing	= false;
	_context.closeComplete();
	}

Oscl::Usb::Hub::Port::Req::Api::SAP&	Port::getHubPortSAP() noexcept{
	return _hubPortSAP;
	}

void		Port::sendGetPortStatusIRP() noexcept{
	if(_currentTrans){
		// FIXME: Logic Error
		while(true);
		}
	_contextTrans	= new(&_statusMem)
						GetPortStatusTrans(	*this,
											_pipeSAP,
											_setupMem.context.getPortStatus,
											_packetMem.context.getPortStatus,
											_myPapi
											);
	}

unsigned	Port::getPortNum() noexcept{
	return _portNum;
	}

void	Port::notifyChanged() noexcept{
	NotifyPortChangeReq*				next;
	while((next=_notificationList.get())){
		next->_payload._lastChange	= _portChangeStatus;
		next->returnToSender();
		}
	}

void	Port::changeUpdate(	Oscl::Usb::Hw::Hub::Port::
							wPortChange::Reg			change
							) noexcept{
	_portChangeStatus	= change;
	if(_portChangeStatus.anySet(~0)){
		notifyChanged();
		}
	else{
		if(!_contextNotifyEnabled){
			_contextNotifyEnabled	= true;
			_context.enableChangeNotification();
			}
		}
	}

void	Port::statusChange() noexcept{
	_context.disableChangeNotification();
	_contextNotifyEnabled	= false;
	sendGetPortStatusIRP();
	}

void	Port::canceled(GetPortStatusTrans& trans) noexcept{
	trans.~GetPortStatusTrans();
	_contextTrans	= 0;
	}

void	Port::failed(GetPortStatusTrans& trans) noexcept{
	trans.~GetPortStatusTrans();
	_contextTrans	= 0;
	}

void	Port::done(	GetPortStatusTrans&					trans,
					Oscl::Usb::Hw::Hub::Port::Packet&	packet
					) noexcept{
	trans.~GetPortStatusTrans();
	_contextTrans	= 0;
	using namespace Oscl::Usb::Hw::Hub::Port::wPortChange;
	_portChangeStatus	= packet.wPortChange;
	changeUpdate(packet.wPortChange);
	}

void	Port::canceled(GetStatusTrans& trans) noexcept{
	trans.~GetStatusTrans();
	_currentTrans	= 0;
	if(_cancelReq){
		_cancelReq->returnToSender();
		_cancelReq		= 0;
		}
	if(_closing){
		cancelNext();
		}
	}

void	Port::failed(GetStatusTrans& trans) noexcept{
	trans.~GetStatusTrans();
	_contextTrans	= 0;
	}

void	Port::done(	GetStatusTrans&						trans,
					Oscl::Usb::Hw::Hub::Port::Packet&	packet
					) noexcept{
	trans.~GetStatusTrans();
	_currentTrans	= 0;
	using namespace Oscl::Usb::Hw::Hub::Port::wPortChange;
	changeUpdate(packet.wPortChange);
	}

void	Port::canceled(ClearConnChangeTrans& trans) noexcept{
	trans.~ClearConnChangeTrans();
	_currentTrans	= 0;
	if(_cancelReq){
		_cancelReq->returnToSender();
		_cancelReq		= 0;
		}
	if(_closing){
		cancelNext();
		}
	}

void	Port::failed(ClearConnChangeTrans& trans) noexcept{
	trans.~ClearConnChangeTrans();
	_currentTrans	= 0;
	}

void	Port::done(ClearConnChangeTrans& trans) noexcept{
	trans.~ClearConnChangeTrans();
	_currentTrans	= 0;
	using namespace Oscl::Usb::Hw::Hub::Port::wPortChange;
	_portChangeStatus.clearBits(Connection::ValueMask_Changed);
	sendGetPortStatusIRP();
	}

void	Port::canceled(ClearEnableChangeTrans& trans) noexcept{
	trans.~ClearEnableChangeTrans();
	_currentTrans	= 0;
	if(_cancelReq){
		_cancelReq->returnToSender();
		_cancelReq		= 0;
		}
	if(_closing){
		cancelNext();
		}
	}

void	Port::failed(ClearEnableChangeTrans& trans) noexcept{
	trans.~ClearEnableChangeTrans();
	_currentTrans	= 0;
	}
void	Port::done(ClearEnableChangeTrans& trans) noexcept{
	trans.~ClearEnableChangeTrans();
	_currentTrans	= 0;
	using namespace Oscl::Usb::Hw::Hub::Port::wPortChange;
	_portChangeStatus.clearBits(Enable::ValueMask_Disabled);
	sendGetPortStatusIRP();
	}

void	Port::canceled(ClearOverCurrentChangeTrans& trans) noexcept{
	trans.~ClearOverCurrentChangeTrans();
	_currentTrans	= 0;
	if(_cancelReq){
		_cancelReq->returnToSender();
		_cancelReq		= 0;
		}
	if(_closing){
		cancelNext();
		}
	}

void	Port::failed(ClearOverCurrentChangeTrans& trans) noexcept{
	trans.~ClearOverCurrentChangeTrans();
	_currentTrans	= 0;
	}

void	Port::done(ClearOverCurrentChangeTrans& trans) noexcept{
	trans.~ClearOverCurrentChangeTrans();
	_currentTrans	= 0;
	using namespace Oscl::Usb::Hw::Hub::Port::wPortChange;
	_portChangeStatus.clearBits(OverCurrent::ValueMask_Changed);
	sendGetPortStatusIRP();
	}

void	Port::canceled(ClearResetChangeTrans& trans) noexcept{
	trans.~ClearResetChangeTrans();
	_currentTrans	= 0;
	if(_cancelReq){
		_cancelReq->returnToSender();
		_cancelReq		= 0;
		}
	if(_closing){
		cancelNext();
		}
	}

void	Port::failed(ClearResetChangeTrans& trans) noexcept{
	trans.~ClearResetChangeTrans();
	_currentTrans	= 0;
	}

void	Port::done(ClearResetChangeTrans& trans) noexcept{
	trans.~ClearResetChangeTrans();
	_currentTrans	= 0;
	using namespace Oscl::Usb::Hw::Hub::Port::wPortChange;
	_portChangeStatus.clearBits(Reset::ValueMask_ResetDone);
	sendGetPortStatusIRP();
	}

void	Port::canceled(ClearSuspendChangeTrans& trans) noexcept{
	trans.~ClearSuspendChangeTrans();
	_currentTrans	= 0;
	if(_cancelReq){
		_cancelReq->returnToSender();
		_cancelReq		= 0;
		}
	if(_closing){
		cancelNext();
		}
	}

void	Port::failed(ClearSuspendChangeTrans& trans) noexcept{
	trans.~ClearSuspendChangeTrans();
	_currentTrans	= 0;
	// Wait to be canceled.
	}

void	Port::done(ClearSuspendChangeTrans& trans) noexcept{
	trans.~ClearSuspendChangeTrans();
	_currentTrans	= 0;
	using namespace Oscl::Usb::Hw::Hub::Port::wPortChange;
	_portChangeStatus.clearBits(Suspend::ValueMask_ResumeDone);
	sendGetPortStatusIRP();
	}

void	Port::canceled(ClearEnableTrans& trans) noexcept{
	trans.~ClearEnableTrans();
	_currentTrans	= 0;
	if(_cancelReq){
		_cancelReq->returnToSender();
		_cancelReq		= 0;
		}
	if(_closing){
		cancelNext();
		}
	}

void	Port::failed(ClearEnableTrans& trans) noexcept{
	trans.~ClearEnableTrans();
	_currentTrans	= 0;
	}

void	Port::done(ClearEnableTrans& trans) noexcept{
	trans.~ClearEnableTrans();
	_currentTrans	= 0;
	}

void	Port::canceled(ClearSuspendTrans& trans) noexcept{
	trans.~ClearSuspendTrans();
	_currentTrans	= 0;
	if(_cancelReq){
		_cancelReq->returnToSender();
		_cancelReq		= 0;
		}
	if(_closing){
		cancelNext();
		}
	}

void	Port::failed(ClearSuspendTrans& trans) noexcept{
	trans.~ClearSuspendTrans();
	_currentTrans	= 0;
	}

void	Port::done(ClearSuspendTrans& trans) noexcept{
	trans.~ClearSuspendTrans();
	_currentTrans	= 0;
	}

void	Port::canceled(ClearPowerTrans& trans) noexcept{
	trans.~ClearPowerTrans();
	_currentTrans	= 0;
	if(_cancelReq){
		_cancelReq->returnToSender();
		_cancelReq		= 0;
		}
	if(_closing){
		cancelNext();
		}
	}

void	Port::failed(ClearPowerTrans& trans) noexcept{
	trans.~ClearPowerTrans();
	_currentTrans	= 0;
	}

void	Port::done(ClearPowerTrans& trans) noexcept{
	trans.~ClearPowerTrans();
	_currentTrans	= 0;
	}

void	Port::canceled(SetResetTrans& trans) noexcept{
	trans.~SetResetTrans();
	_currentTrans	= 0;
	if(_cancelReq){
		_cancelReq->returnToSender();
		_cancelReq		= 0;
		}
	if(_closing){
		cancelNext();
		}
	}

void	Port::failed(SetResetTrans& trans) noexcept{
	trans.~SetResetTrans();
	_currentTrans	= 0;
	}

void	Port::done(SetResetTrans& trans) noexcept{
	trans.~SetResetTrans();
	_currentTrans	= 0;
	}

void	Port::canceled(SetSuspendTrans& trans) noexcept{
	trans.~SetSuspendTrans();
	_currentTrans	= 0;
	if(_cancelReq){
		_cancelReq->returnToSender();
		_cancelReq		= 0;
		}
	if(_closing){
		cancelNext();
		}
	}

void	Port::failed(SetSuspendTrans& trans) noexcept{
	trans.~SetSuspendTrans();
	_currentTrans	= 0;
	}

void	Port::done(SetSuspendTrans& trans) noexcept{
	trans.~SetSuspendTrans();
	_currentTrans	= 0;
	}

void	Port::canceled(SetPowerTrans& trans) noexcept{
	trans.~SetPowerTrans();
	_currentTrans	= 0;
	if(_cancelReq){
		_cancelReq->returnToSender();
		_cancelReq		= 0;
		}
	if(_closing){
		cancelNext();
		}
	}

void	Port::failed(SetPowerTrans& trans) noexcept{
	trans.~SetPowerTrans();
	_currentTrans	= 0;
	}

void	Port::done(SetPowerTrans& trans) noexcept{
	trans.~SetPowerTrans();
	_currentTrans	= 0;
	}

void	Port::request(SetResetReq& msg) noexcept{
	if(_currentTrans){
		// FIXME: Logic Error
		while(true);
		}
	_currentTrans	= new(&_transMem.getStatus)
						SetResetTrans(	*this,
										_pipeSAP,
										msg,
										_setupMem.trans.setReset,
										_myPapi
										);
	}

void	Port::request(SetSuspendReq& msg) noexcept{
	if(_currentTrans){
		// FIXME: Logic Error
		while(true);
		}
	_currentTrans	= new(&_transMem.setSuspend)
						SetSuspendTrans(	*this,
											_pipeSAP,
											msg,
											_setupMem.trans.setSuspend,
											_myPapi
											);
	}

void	Port::request(SetPowerReq& msg) noexcept{
	if(_currentTrans){
		// FIXME: Logic Error
		while(true);
		}
	_currentTrans	= new(&_transMem.setPower)
						SetPowerTrans(	*this,
										_pipeSAP,
										msg,
										_setupMem.trans.setPower,
										_myPapi
										);
	}

void	Port::request(ClearEnableReq& msg) noexcept{
	if(_currentTrans){
		// FIXME: Logic Error
		while(true);
		}
	_currentTrans	= new(&_transMem.clrEnable)
						ClearEnableTrans(	*this,
											_pipeSAP,
											msg,
											_setupMem.trans.clrEnable,
											_myPapi
											);
	}

void	Port::request(ClearSuspendReq& msg) noexcept{
	if(_currentTrans){
		// FIXME: Logic Error
		while(true);
		}
	_currentTrans	= new(&_transMem.clrSuspend)
						ClearSuspendTrans(	*this,
										_pipeSAP,
										msg,
										_setupMem.trans.clrSuspend,
										_myPapi
										);
	}

void	Port::request(ClearPowerReq& msg) noexcept{
	if(_currentTrans){
		// FIXME: Logic Error
		while(true);
		}
	_currentTrans	= new(&_transMem.clrPower)
						ClearPowerTrans(	*this,
											_pipeSAP,
											msg,
											_setupMem.trans.clrPower,
											_myPapi
											);
	}

void	Port::request(ClearConnectChangeReq& msg) noexcept{
	if(_currentTrans){
		// FIXME: Logic Error
		while(true);
		}
	_currentTrans	= new(&_transMem.clrConnChange)
						ClearConnChangeTrans(	*this,
												_pipeSAP,
												msg,
												_setupMem.trans.clrConnChange,
												_myPapi
												);
	}

void	Port::request(ClearResetChangeReq& msg) noexcept{
	if(_currentTrans){
		// FIXME: Logic Error
		while(true);
		}
	_currentTrans	= new(&_transMem.clrResetChange)
						ClearResetChangeTrans(	*this,
												_pipeSAP,
												msg,
												_setupMem.trans.clrResetChange,
												_myPapi
												);
	}

void	Port::request(ClearEnableChangeReq& msg) noexcept{
	if(_currentTrans){
		// FIXME: Logic Error
		while(true);
		}
	_currentTrans	= new(&_transMem.clrEnableChange)
						ClearEnableChangeTrans(	*this,
												_pipeSAP,
												msg,
												_setupMem.trans.clrEnableChange,
												_myPapi
												);
	}

void	Port::request(ClearSuspendChangeReq& msg) noexcept{
	if(_currentTrans){
		// FIXME: Logic Error
		while(true);
		}
	_currentTrans	= new(&_transMem.clrSuspendChange)
						ClearSuspendChangeTrans(	*this,
													_pipeSAP,
													msg,
													_setupMem.
													trans.clrSuspendChange,
													_myPapi
													);
	}

void	Port::request(ClearOverCurrentChangeReq& msg) noexcept{
	if(_currentTrans){
		// FIXME: Logic Error
		while(true);
		}
	_currentTrans	= new(&_transMem.clrOverCurrentChange)
						ClearOverCurrentChangeTrans(	*this,
														_pipeSAP,
														msg,
														_setupMem.trans.
														clrOverCurrentChange,
														_myPapi
														);
	}

void	Port::request(CancelReq& msg) noexcept{
	if(_cancelReq){
		// FIXME: Logic Error
		while(true);
		}
	if(_currentTrans){
		_cancelReq	= &msg;
		_currentTrans->cancel(_cancelMem);
		return;
		}
	msg.returnToSender();
	}

void	Port::request(NotifyPortChangeReq& msg) noexcept{
	_notificationList.put(&msg);
	if(_portChangeStatus.anySet(~0)){
		notifyChanged();
		}
	}

void	Port::request(CancelNotifyPortChangeReq& msg) noexcept{
	if(_notificationList.remove(&msg._payload._reqToCancel)){
		msg._payload._reqToCancel.returnToSender();
		}
	msg.returnToSender();
	}

void	Port::request(GetPortStatusReq& msg) noexcept{
	if(_currentTrans){
		// FIXME: Logic Error
		while(true);
		}
	_currentTrans	= new(&_transMem.getStatus)
						GetStatusTrans(	*this,
										_pipeSAP,
										msg,
										_setupMem.trans.getStatus,
										_packetMem.trans.getStatus,
										_myPapi
										);
	}

