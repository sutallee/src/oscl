/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <new>
#include "trans.h"
#include "port.h"

using namespace Oscl::Usb::Hub::Port::Adaptor;

SetResetTrans::
SetResetTrans(	Port&								context,
				Oscl::Usb::Pipe::Message::
				Req::Api::SAP&						pipeSAP,
				Oscl::Usb::Hub::Port::
				Req::Api::
				SetResetReq&						req,
				SetupMem&							setupMem,
				Oscl::Mt::Itc::PostMsgApi&			myPapi
				) noexcept:
		_context(context),
		_pipeSAP(pipeSAP),
		_req(req),
		_setupMem(setupMem),
		_myPapi(myPapi),
		_resp(0),
		_canceling(false),
		_failed(false)
		{
	Oscl::Usb::Setup::Hub::SetPortReset*
	setup	= new(&setupMem)
				Oscl::Usb::Setup::Hub::
				SetPortReset(context.getPortNum());

	Oscl::Usb::Pipe::Message::Req::Api::NoDataPayload*
	payload	= new(&_respMem.payload)
				Oscl::Usb::Pipe::Message::
				Req::Api::NoDataPayload(*setup);

	_resp	= new(&_respMem.resp)
				Oscl::Usb::Pipe::Message::
				Resp::Api::NoDataResp(	pipeSAP.getReqApi(),
										*this,
										myPapi,
										*payload
										);
	pipeSAP.post(_resp->getSrvMsg());
	}

void	SetResetTrans::cancel(	Oscl::Usb::Pipe::
								Message::Resp::CancelMem&	mem
								) noexcept{
	_canceling	= true;
	if(_failed){
		_req.returnToSender();
		_context.canceled(*this);
		return;
		}
	Oscl::Usb::Pipe::Message::Req::Api::CancelPayload*
	payload	= new(&mem.payload)
				Oscl::Usb::Pipe::Message::
				Req::Api::CancelPayload(_resp->getSrvMsg());

	Oscl::Usb::Pipe::Message::Resp::Api::CancelResp*
	resp	= new(&mem.resp)
				Oscl::Usb::Pipe::Message::
				Resp::Api::CancelResp(	_pipeSAP.getReqApi(),
										*this,
										_myPapi,
										*payload
										);

	_pipeSAP.post(resp->getSrvMsg());
	}

void	SetResetTrans::response(	Oscl::Usb::Pipe::Message::
									Resp::Api::CancelResp&		msg
									) noexcept{
	_req.returnToSender();
	_context.canceled(*this);
	}

void	SetResetTrans::response(	Oscl::Usb::Pipe::Message::
									Resp::Api::NoDataResp&		msg
									) noexcept{
	if(_canceling) return;
	if(msg.getPayload()._failed){
		_req.getPayload()._failed	= true;
		_req.returnToSender();
		_failed	= true;
		_context.failed(*this);
		return;
		}
	_req.returnToSender();
	_context.done(*this);
	}

void	SetResetTrans::response(	Oscl::Usb::Pipe::Message::
									Resp::Api::ReadResp&		msg
									) noexcept{
	// FIXME: Logic Error
	while(true);
	}

void	SetResetTrans::response(	Oscl::Usb::Pipe::Message::
									Resp::Api::WriteResp&		msg
									) noexcept{
	// FIXME: Logic Error
	while(true);
	}


