/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_usb_hub_port_reg_statush_
#define _oscl_drv_usb_hub_port_reg_statush_
#include "oscl/hw/usb/hub/port/status.h"
#include "oscl/bits/bitfield.h"

/** */
namespace Oscl {
/** */
namespace Usb {
/** */
namespace Hub {
/** */
namespace Port {
/** */
namespace Reg {

/** */
class Status {
	private:
		/** */
		Oscl::BitField<Oscl::Usb::Hw::Hub::Port::wPortStatus::Reg>	_status;

	public:
		/** */
		operator Oscl::Usb::Hw::Hub::Port::wPortStatus::Reg() const noexcept{
			return _status;
			}

		/** */
		void operator =(	Oscl::Usb::Hw::Hub::Port::
							wPortStatus::Reg	status
							) noexcept{
			_status	= status;
			}

		/** */
		inline bool	deviceConnected() noexcept{
			using namespace Oscl::Usb::Hw::Hub::Port::wPortStatus;
			return _status.equal(	Connect::FieldMask,
									Connect::ValueMask_DevicePresent
									);
			}

		/** */
		inline bool	portEnabled() noexcept{
			using namespace Oscl::Usb::Hw::Hub::Port::wPortStatus;
			return _status.equal(	Enable::FieldMask,
									Enable::ValueMask_Enabled
									);
			}

		/** */
		inline bool	portSuspended() noexcept{
			using namespace Oscl::Usb::Hw::Hub::Port::wPortStatus;
			return _status.equal(	Suspend::FieldMask,
									Suspend::ValueMask_Suspended
									);
			}

		/** */
		inline bool	overcurrent() noexcept{
			using namespace Oscl::Usb::Hw::Hub::Port::wPortStatus;
			return _status.equal(	OverCurrent::FieldMask,
									OverCurrent::ValueMask_Asserted
									);
			}

		/** */
		inline bool	portResetSignalIsActive() noexcept{
			using namespace Oscl::Usb::Hw::Hub::Port::wPortStatus;
			return _status.equal(	Reset::FieldMask,
									Reset::ValueMask_Asserted
									);
			}

		/** */
		inline bool	portPowerIsOn() noexcept{
			using namespace Oscl::Usb::Hw::Hub::Port::wPortStatus;
			return _status.equal(	Power::FieldMask,
									Power::ValueMask_On
									);
			}

		/** */
		inline bool	lowSpeedDeviceAttached() noexcept{
			using namespace Oscl::Usb::Hw::Hub::Port::wPortStatus;
			return _status.equal(	DeviceSpeed::FieldMask,
									DeviceSpeed::ValueMask_LowSpeed
									);
			}
	};

}
}
}
}
}
#endif
