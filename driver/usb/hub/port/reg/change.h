/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_usb_hub_port_reg_changeh_
#define _oscl_drv_usb_hub_port_reg_changeh_
#include "oscl/hw/usb/hub/port/status.h"
#include "oscl/bits/bitfield.h"

/** */
namespace Oscl {
/** */
namespace Usb {
/** */
namespace Hub {
/** */
namespace Port {
/** */
namespace Reg {

/** */
class Change {
	private:
		/** */
		Oscl::BitField<Oscl::Usb::Hw::Hub::Port::wPortChange::Reg>	_status;

	public:
		/** */
		operator Oscl::Usb::Hw::Hub::Port::wPortChange::Reg() const noexcept{
			return _status;
			}

		/** */
		void operator =(	Oscl::Usb::Hw::Hub::Port::
							wPortChange::Reg status
							) noexcept{
			_status	= status;
			}

		/** */
		inline bool	currentConnectStatusChange() noexcept{
			using namespace Oscl::Usb::Hw::Hub::Port::wPortChange;
			return _status.equal(	Connection::FieldMask,
									Connection::ValueMask_Changed
									);
			}

		/** */
		inline bool	portEnableStatusChange() noexcept{
			using namespace Oscl::Usb::Hw::Hub::Port::wPortChange;
			return _status.equal(	Enable::FieldMask,
									Enable::ValueMask_Disabled
									);
			}

		/** */
		inline bool	portSuspendStatusChange() noexcept{
			using namespace Oscl::Usb::Hw::Hub::Port::wPortChange;
			return _status.equal(	Suspend::FieldMask,
									Suspend::ValueMask_ResumeDone
									);
			}

		/** */
		inline bool	portOverCurrentIndicatorChange() noexcept{
			using namespace Oscl::Usb::Hw::Hub::Port::wPortChange;
			return _status.equal(	OverCurrent::FieldMask,
									OverCurrent::ValueMask_Changed
									);
			}

		/** */
		inline bool	portResetStatusChange() noexcept{
			using namespace Oscl::Usb::Hw::Hub::Port::wPortChange;
			return _status.equal(	Reset::FieldMask,
									Reset::ValueMask_ResetDone
									);
			}
	};

}
}
}
}
}

#endif
