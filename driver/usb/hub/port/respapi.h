/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_usb_hub_port_respapih_
#define _oscl_usb_hub_port_respapih_
#include "reqapi.h"
#include "oscl/mt/itc/mbox/clirsp.h"

/** */
namespace Oscl {
/** */
namespace Usb {
/** */
namespace Hub {
/** */
namespace Port {
/** */
namespace Resp {

/** */
class Api {
	public:
		/** */
		typedef Oscl::Mt::Itc::
				CliResponse<	Req::Api,
								Api,
								Req::Api::SetResetPayload
							>	SetResetResp;
		/** */
		typedef Oscl::Mt::Itc::
				CliResponse<	Req::Api,
								Api,
								Req::Api::SetSuspendPayload
								>	SetSuspendResp;
		/** */
		typedef Oscl::Mt::Itc::
				CliResponse<	Req::Api,
								Api,
								Req::Api::SetPowerPayload
								>	SetPowerResp;
		/** */
		typedef Oscl::Mt::Itc::
				CliResponse<	Req::Api,
								Api,
								Req::Api::ClearEnablePayload
								>	ClearEnableResp;
		/** */
		typedef Oscl::Mt::Itc::
				CliResponse<	Req::Api,
								Api,
								Req::Api::ClearSuspendPayload
								>	ClearSuspendResp;
		/** */
		typedef Oscl::Mt::Itc::
				CliResponse<	Req::Api,
								Api,
								Req::Api::ClearPowerPayload
								>	ClearPowerResp;
		/** */
		typedef Oscl::Mt::Itc::
				CliResponse<	Req::Api,
								Api,
								Req::Api::ClearConnectChangePayload
								>	ClearConnectChangeResp;
		/** */
		typedef Oscl::Mt::Itc::
				CliResponse<	Req::Api,
								Api,
								Req::Api::ClearResetChangePayload
								>	ClearResetChangeResp;
		/** */
		typedef Oscl::Mt::Itc::
				CliResponse<	Req::Api,
								Api,
								Req::Api::ClearEnableChangePayload
								>	ClearEnableChangeResp;
		/** */
		typedef Oscl::Mt::Itc::
				CliResponse<	Req::Api,
								Api,
								Req::Api::ClearSuspendChangePayload
								>	ClearSuspendChangeResp;
		/** */
		typedef Oscl::Mt::Itc::
				CliResponse<	Req::Api,
								Api,
								Req::Api::ClearOverCurrentChangePayload
								>	ClearOverCurrentChangeResp;
		/** */
		typedef Oscl::Mt::Itc::
				CliResponse<	Req::Api,
								Api,
								Req::Api::CancelPayload
								>	CancelResp;
		/** */
		typedef Oscl::Mt::Itc::
				CliResponse<	Req::Api,
								Api,
								Req::Api::NotifyPortChangePayload
								>	NotifyPortChangeResp;
		/** */
		typedef Oscl::Mt::Itc::
				CliResponse<	Req::Api,
								Api,
								Req::Api::CancelNotifyPortChangePayload
								>	CancelNotifyPortChangeResp;
		/** */
		typedef Oscl::Mt::Itc::
				CliResponse<	Req::Api,
								Api,
								Req::Api::GetPortStatusPayload
								>	GetPortStatusResp;

		/** */
		typedef union MsgPtr {
			/** */
			SetResetResp*					setReset;
			/** */
			SetSuspendResp*					setSuspend;
			/** */
			SetPowerResp*					setPower;
			/** */
			ClearEnableResp*				clrEnable;
			/** */
			ClearSuspendResp*				clrSuspended;
			/** */
			ClearPowerResp*					clrPower;
			/** */
			ClearConnectChangeResp*			clrConnectChange;
			/** */
			ClearResetChangeResp*			clrResetChange;
			/** */
			ClearEnableChangeResp*			clrEnableChange;
			/** */
			ClearSuspendChangeResp*			clrSuspendChange;
			/** */
			ClearOverCurrentChangeResp*		clrOverCurrentChange;
			/** */
			CancelResp*						cancel;
			/** */
			NotifyPortChangeResp*			notifyPortChange;
			/** */
			CancelNotifyPortChangeResp*		cancelNotifyPortChange;
			/** */
			GetPortStatusResp*				getStatus;
			} MsgPtr;

	public:
		/** Shut-up GCC. */
		virtual ~Api() {}
		/** */
		virtual void	response(SetResetResp& msg) noexcept=0;
		/** */
		virtual void	response(SetSuspendResp& msg) noexcept=0;
		/** */
		virtual void	response(SetPowerResp& msg) noexcept=0;
		/** */
		virtual void	response(ClearEnableResp& msg) noexcept=0;
		/** */
		virtual void	response(ClearSuspendResp& msg) noexcept=0;
		/** */
		virtual void	response(ClearPowerResp& msg) noexcept=0;

		/** */
		virtual void	response(ClearConnectChangeResp& msg) noexcept=0;
		/** */
		virtual void	response(ClearResetChangeResp& msg) noexcept=0;
		/** */
		virtual void	response(ClearEnableChangeResp& msg) noexcept=0;
		/** */
		virtual void	response(ClearSuspendChangeResp& msg) noexcept=0;
		/** */
		virtual void	response(ClearOverCurrentChangeResp& msg) noexcept=0;

		/** */
		virtual void	response(CancelResp& msg) noexcept=0;

		/** */
		virtual void	response(NotifyPortChangeResp& msg) noexcept=0;
		/** */
		virtual void	response(CancelNotifyPortChangeResp& msg) noexcept=0;

		/** */
		virtual void	response(GetPortStatusResp& msg) noexcept=0;
	};

}
}
}
}
}

#endif
