/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_usb_hub_port_driverh_
#define _oscl_drv_usb_hub_port_driverh_
#include "oscl/mt/itc/mbox/sap.h"
#include "oscl/queue/queueitem.h"
#include "oscl/queue/queue.h"
#include "oscl/driver/usb/alloc/addr/respmem.h"
#include "oscl/memory/block.h"
#include "oscl/driver/usb/alloc/pipe/message/respmem.h"
#include "oscl/driver/usb/setup/getdesc.h"
#include "oscl/driver/usb/setup/setaddr.h"
#include "oscl/driver/usb/desc/device.h"
#include "oscl/driver/usb/pipe/message/respmem.h"
#include "oscl/driver/usb/hub/port/respmem.h"
#include "oscl/driver/usb/hub/port/fsm/fsm.h"
#include "oscl/mt/itc/srv/close.h"
#include "oscl/driver/usb/device/dyndev.h"
#include "oscl/mt/itc/dyn/adv/creatorsync.h"
#include "oscl/mt/itc/dyn/core/mem.h"
#include "oscl/mt/itc/srv/crespmem.h"
#include "oscl/driver/usb/alloc/power/reqapi.h"
#include "oscl/driver/usb/alloc/power/syncapi.h"
#include "oscl/mt/itc/delay/respmem.h"
#include "oscl/oid/fixed.h"

/** */
namespace Oscl {
/** */
namespace Usb {
/** */
namespace Hub {
/** */
namespace Port {

/** */
class Driver :	
				public Oscl::Mt::Itc::Srv::CloseSync,
				public Oscl::Mt::Itc::Srv::Close::Req::Api,
				public Oscl::Usb::Alloc::Addr::Resp::Api,
				public Oscl::Usb::Alloc::Pipe::Message::Resp::Api,
				private Oscl::Usb::Pipe::Message::Resp::Api,
				public Oscl::Usb::Hub::Port::Resp::Api,
				private Oscl::Mt::Itc::Dyn::Creator::Resp::Api<DynDevice>,
				private Oscl::Mt::Itc::Srv::Close::Resp::Api,
				private Oscl::Mt::Itc::Delay::Resp::Api,
				public Oscl::Usb::Hub::Port::StateVar::ContextApi,
				public Oscl::QueueItem
				{
	private:
		/** */
		typedef union RespMsg {
			/** */
			Oscl::Mt::Itc::SrvMsg*					srvmsg;
			/** */
			Oscl::Usb::Alloc::Addr::
			Resp::Api::MsgPtr						hcdHubPort;
			/** */
			Oscl::Usb::Alloc::Pipe::
			Message::Resp::Api::MsgPtr				hcdAlloc;
			/** */
			Oscl::Usb::Pipe::Message::
			Resp::Api::MsgPtr						msgPipeIrp;
			/** */
			Oscl::Usb::Hub::Port::
			Resp::Api::MsgPtr						hubPort;
			} RespMsg;

		/** */
		typedef union RespMem {
			/** */
			void*									__qitemlink;
			/** */
			Oscl::Usb::Alloc::Addr::Resp::Mem		hcdHubPort;
			/** */
			Oscl::Usb::Alloc::Pipe::
			Message::Resp::Mem						hcdAlloc;
			/** */
			Oscl::Usb::Pipe::Message::Resp::Mem		msgPipeIrp;
			/** */
			Oscl::Usb::Hub::Port::Resp::ReqMem		hubPort;
			/** */
			Oscl::Mt::Itc::Dyn::Creator::
			Resp::DeactivateMem<DynDevice>			deactivate;
			/** */
			Oscl::Mt::Itc::Srv::Close::
			Resp::Mem								close;
			} RespMem;
		/** */
		typedef union CancelMem {
			/** */
			void*									__qitemlink;
			/** */
			Oscl::Usb::Hub::Port::Resp::CancelMem	hubPort;
			/** */
			Oscl::Usb::Pipe::Message::
			Resp::CancelMem							msgPipeIrp;
			} CancelMem;
	public:
		/** */
		typedef union PacketMem {
			Oscl::Memory::AlignedBlock
				<sizeof(Oscl::Usb::Desc::Device)>	_stdDevDesc;
			} PacketMem;
		/** */
		union SetupPktMem {
			/** */
			Oscl::Memory::
			AlignedBlock<	sizeof(	Oscl::Usb::Setup::
									SetAddress
									)
							>								_setAddr;
			/** */
			Oscl::Memory::
			AlignedBlock<	sizeof(	Oscl::Usb::Setup::
									GetDescriptor
									)
							>								_getDesc;
			};
	private:
		/** */
		typedef Oscl::Memory::
				AlignedBlock<sizeof(DynDevice)>			DynDeviceMem;
	private:
		/** */
		Oscl::Usb::Alloc::Addr::Req::Api::SAP&			_hcdPortSAP;
		/** */
		Oscl::Usb::Hub::Port::Req::Api::SAP&			_hubPortSAP;
		/** */
		Oscl::Usb::Alloc::Pipe::
		Message::Req::Api::SAP&							_msgPipeAllocSAP;
		/** */
		Oscl::Usb::Alloc::Power::
		Req::Api::SAP&									_powerSAP;
		/** */
		Oscl::Usb::Alloc::Power::
		SyncApi&										_powerSyncApi;
		/** */
		Oscl::Usb::HCD::Enum::Monitor::Api&				_hcdEnumMonitorApi;
		/** */
		Oscl::Usb::HCD::Enum::
		DefCntrlPipe::Api&								_hcdEnumDcpApi;
		/** */
		DynDevice*										_dynDevice;
		/** */
		Oscl::Mt::Itc::Dyn::Adv::
		Creator::SyncApi<DynDevice>&					_advCapi;
		/** */
		Oscl::Mt::Itc::Dyn::Adv::
		Core<DynDevice>::ReleaseApi&					_advRapi;
		/** */
		Oscl::Mt::Itc::PostMsgApi&						_advPapi;
		/** */
		Oscl::Mt::Itc::PostMsgApi&						_dynSrvPapi;
		/** */
		Oscl::Mt::Itc::PostMsgApi&						_myPapi;
		/** */
		RespMem											_respMem;
		/** */
		Oscl::Usb::Hub::
		Port::Resp::NotifyPortChangeMem					_portChangeMem;
		/** */
		Oscl::Usb::Hub::Port::Resp::CancelMem			_hubPortCancelMem;
		/** */
		Oscl::Usb::Pipe::Message::Resp::CancelMem		_irpCancelMem;
		/** */
		Oscl::Usb::Alloc::Pipe::
		Message::Resp::CancelMem						_epAllocCancelMem;
		/** */
		DynDeviceMem									_dynDevMem;
		/** */
		Oscl::Usb::Alloc::Pipe::Message::
		Resp::NotifyChangeMem							_pipeResChangeMem;
		/** */
		Oscl::Usb::Alloc::Addr::Resp::CancelMem			_hcdHubPortRespMem;
		/** */
		Oscl::Usb::Alloc::Addr::
		Resp::UsbAddrResChangeMem						_usbAddrChangeMem;
		/** */
		Oscl::Mt::Itc::Delay::Resp::DelayMem			_delayMem;
		/** */
		Oscl::Mt::Itc::Delay::Resp::CancelMem			_delayCancelMem;
		/** */
		Oscl::Usb::Alloc::Addr::
		Resp::Api::UsbAddrResChangeResp*				_usbAddrChangeResp;
		/** */
		Oscl::Usb::Alloc::Addr::
		Resp::Api::LockAddressZeroResp*					_lockAddrZeroResp;
		/** */
		Oscl::Mt::Itc::Delay::Resp::Api::DelayResp*		_delayResp;
		/** */
		Oscl::Usb::Pipe::Message::Req::Api::SAP&		_hsDefaultPipeSAP;
		/** */
		Oscl::Usb::Pipe::Message::Req::Api::SAP&		_lsDefaultPipeSAP;
		/** */
		Oscl::Mt::Itc::Delay::Req::Api::SAP&			_delayServiceSAP;
		/** */
		Oscl::ObjectID::Fixed<32>						_location;
		/** */
		PacketMem&										_packetMem;
		/** */
		SetupPktMem&									_setupMem;
		/** */
		Oscl::Usb::Hub::Port::StateVar					_state;
		/** */
		RespMsg											_lastRespMsg;
		/** */
		Oscl::Usb::Alloc::Pipe::Message::
		Resp::Api::NotifyChangeResp*					_pipeResChangeResp;
		/** */
		RespMsg											_currentReq;
		/** */
		Oscl::Usb::Hub::Port::
		Req::Api::NotifyPortChangeReq*					_currentChangeReq;
		/** */
		Oscl::Usb::Hub::Port::Reg::Status				_portStatus;
		/** */
		Oscl::Usb::Hub::Port::Reg::Change				_portChange;
		/** */
		Oscl::Usb::Address								_usbAddress;
		/** */
		unsigned char									_maxPacketSize;
		/** */
		bool											_lowSpeedDevice;
		/** */
		bool											_usbAddrAllocated;
		/** */
		bool											_lockAddrZeroCanceled;
		/** */
		Oscl::Mt::Itc::Srv::Open::Req::Api::OpenReq*	_openReq;
		/** */
		Oscl::Mt::Itc::Srv::Close::Req::Api::CloseReq*	_closeReq;
		/** */
		Oscl::Usb::Pipe::Message::PipeApi*				_devicePipe;
		/** */
		Oscl::Usb::Desc::Device*						_devDesc;
		/** */
		Oscl::Usb::Pipe::Message::Req::Api::SAP*		_lastReqSAP;

	public:
		/** */
		Driver(	Oscl::Usb::Alloc::Addr::
				Req::Api::SAP&						hcdPortSAP,
				Oscl::Usb::Hub::Port::
				Req::Api::SAP&						hubPortSAP,
				Oscl::Usb::Alloc::Pipe::
				Message::Req::Api::SAP&				msgPipeAllocSAP,
				Oscl::Mt::Itc::Dyn::Adv::
				Creator::SyncApi<DynDevice>&		advCapi,
				Oscl::Mt::Itc::Dyn::Adv::
				Core<DynDevice>::ReleaseApi&		advRapi,
				Oscl::Mt::Itc::PostMsgApi&			advPapi,
				Oscl::Usb::Alloc::Power::
				Req::Api::SAP&						powerSAP,
				Oscl::Usb::Alloc::Power::
				SyncApi&							powerSyncApi,
				Oscl::Usb::HCD::Enum::
				Monitor::Api&						hcdEnumMonitorApi,
				Oscl::Usb::HCD::Enum::
				DefCntrlPipe::Api&					hcdEnumDcpApi,
				Oscl::Mt::Itc::PostMsgApi&			dynSrvPapi,
				Oscl::Usb::Pipe::Message::
				Req::Api::SAP&						hsDefaultPipeSAP,
				Oscl::Usb::Pipe::Message::
				Req::Api::SAP&						lsDefaultPipeSAP,
				Oscl::Mt::Itc::
				Delay::Req::Api::SAP&				delayServiceSAP,
				Oscl::Mt::Itc::PostMsgApi&			myPapi,
				PacketMem&							packetMem,
				SetupPktMem&						setupPktMem,
				const Oscl::ObjectID::RO::Api*		location=0
				) noexcept;

		/** */
		void	initialize() noexcept;

		/** */
		Oscl::Mt::Itc::Srv::Close::Req::Api::SAP&	getCloseSAP() noexcept;

		/** */
		void	requestReadDesc(	Oscl::Usb::Pipe::
									Message::Req::Api::SAP&	sap,
									unsigned				length
									) noexcept;
	private:	// Oscl::Mt::Itc::Srv::Open::Req::Api
		/** */
		void	request(	Oscl::Mt::Itc::Srv::Open::
							Req::Api::OpenReq&			msg
							) noexcept;
		/** */
		void	request(	Oscl::Mt::Itc::Srv::Close::
							Req::Api::CloseReq&			msg
							) noexcept;

	private:	// Oscl::Usb::Alloc::Addr::Resp::Api
		/** */
		void	response(LockAddressZeroResp& msg) noexcept;
		/** */
		void	response(UnlockAddressZeroResp& msg) noexcept;
		/** */
		void	response(AllocateNewAddressResp& msg) noexcept;
		/** */
		void	response(FreeAddressResp& msg) noexcept;
		/** */
		void	response(CancelAddrZeroLockResp& msg) noexcept;
		/** */
		void	response(UsbAddrResChangeResp& msg) noexcept;
		/** */
		void	response(CancelUsbAddrResChangeResp& msg) noexcept;

	private:	// Oscl::Usb::Alloc::Pipe::Message::Resp::Api
		/** */
		void	response(NewResp& msg) noexcept;
		/** */
		void	response(FreeResp& msg) noexcept;
		/** */
		void	response(NotifyChangeResp& msg) noexcept;
		/** */
		void	response(CancelChangeResp& msg) noexcept;

	private: // Oscl::Usb::Pipe::Message::Resp::Api
		/** */
		void	response(	Oscl::Usb::Pipe::Message::
							Resp::Api::CancelResp&		msg
							) noexcept;
		/** */
		void	response(	Oscl::Usb::Pipe::Message::
							Resp::Api::NoDataResp&		msg
							) noexcept;
		/** */
		void	response(	Oscl::Usb::Pipe::Message::
							Resp::Api::ReadResp&		msg
							) noexcept;
		/** */
		void	response(	Oscl::Usb::Pipe::Message::
							Resp::Api::WriteResp&		msg
							) noexcept;
	private:	// Oscl::Usb::Hub::Port::Resp::Api
		/** */
		void	response(SetResetResp& msg) noexcept;
		/** */
		void	response(SetSuspendResp& msg) noexcept;
		/** */
		void	response(SetPowerResp& msg) noexcept;
		/** */
		void	response(ClearEnableResp& msg) noexcept;
		/** */
		void	response(ClearSuspendResp& msg) noexcept;
		/** */
		void	response(ClearPowerResp& msg) noexcept;
		/** */
		void	response(ClearConnectChangeResp& msg) noexcept;
		/** */
		void	response(ClearResetChangeResp& msg) noexcept;
		/** */
		void	response(ClearEnableChangeResp& msg) noexcept;
		/** */
		void	response(ClearSuspendChangeResp& msg) noexcept;
		/** */
		void	response(ClearOverCurrentChangeResp& msg) noexcept;
		/** */
		void	response(	Oscl::Usb::Hub::Port::
							Resp::Api::CancelResp&	msg
							) noexcept;
		/** */
		void	response(NotifyPortChangeResp& msg) noexcept;
		/** */
		void	response(CancelNotifyPortChangeResp& msg) noexcept;
		/** */
		void	response(GetPortStatusResp& msg) noexcept;

	private:	// Oscl::Mt::Itc::Dyn::Creator::Resp::Api
		/** */
		void	response(	Oscl::Mt::Itc::Dyn::Creator::
							Resp::Api<DynDevice>::DeactivateResp&	msg
							) noexcept;

	private:	// Oscl::Mt::Itc::Srv::Close::Resp::Api
		/** */
		void	response(	Oscl::Mt::Itc::Srv::Close::
							Resp::Api::CloseResp&		msg
							) noexcept;
		/** */
		void	response(	Oscl::Mt::Itc::Srv::Open::
							Resp::Api::OpenResp&		msg
							) noexcept;

	private:	// Oscl::Mt::Itc::Delay::Resp::Api
		/** */
		void	response(	Oscl::Mt::Itc::Delay::
							Resp::Api::DelayResp&	msg
							) noexcept;
		/** */
		void	response(	Oscl::Mt::Itc::Delay::
							Resp::Api::CancelResp&	msg
							) noexcept;

	private:	// StateVar::ContextApi
		/** */
		void	requestSetReset() noexcept;
		/** */
		void	requestSetSuspend() noexcept;
		/** */
		void	requestSetPower() noexcept;
		/** */
		void	requestClrEnable() noexcept;
		/** */
		void	requestClrSuspend() noexcept;
		/** */
		void	requestClrPower() noexcept;
		/** */
		void	requestClrConnectChange() noexcept;
		/** */
		void	requestClrResetChange() noexcept;
		/** */
		void	requestClrEnableChange() noexcept;
		/** */
		void	requestClrSuspendChange() noexcept;
		/** */
		void	requestClrOverCurrentChange() noexcept;
		/** */
		void	requestCancelReq() noexcept;
		/** */
		void	requestNotifyPortChange() noexcept;
		/** */
		void	requestCancelNotifyPortChange() noexcept;
		/** */
		void	requestGetPortStatus() noexcept;
		/** */
		void	requestMsgPipeIrpCancel() noexcept;
		/** */
		void	requestMsgPipeNoDataIrp() noexcept;
		/** */
		void	requestMsgPipeReadPartIrp() noexcept;
		/** */
		void	requestMsgPipeReadFullIrp() noexcept;
		/** */
		void	requestMsgPipeWriteIrp() noexcept;
		/** */
		void	requestHcdMsgPipeAlloc() noexcept;
		/** */
		void	requestHcdMsgPipeFree() noexcept;
		/** */
		void	requestNotifyMsgPipeResourceChange() noexcept;
		/** */
		void	requestCancelNotifyMsgPipeResChange() noexcept;
		/** */
		void	requestHcdLockAddressZero() noexcept;
		/** */
		void	requestHcdUnlockAddressZero() noexcept;
		/** */
		void	requestHcdCancelLockAddrZero() noexcept;
		/** */
		void	requestHcdAllocUsbAddr() noexcept;
		/** */
		void	requestHcdFreeUsbAddr() noexcept;
		/** */
		void	requestNotifyUsbAddrResourceChange() noexcept;
		/** */
		void	requestCancelNotifyUsbAddrResChange() noexcept;
		/** */
		void	prepareToCloseDynSrv() noexcept;
		/** */
		void	closeDynSrv() noexcept;
		/** */
		void	advertiseNewPipe() noexcept;
		/** */
		void	parseChange() noexcept;
		/** */
		void	startRetryTimer() noexcept;
		/** */
		void	startPowerTimer() noexcept;
		/** */
		void	startResetTimer() noexcept;
		/** */
		void	cancelTimer() noexcept;
		/** */
		bool	deviceConnected() noexcept;
		/** */
		bool	overCurrent() noexcept;
		/** */
		bool	usbAddrAllocated() noexcept;
		/** */
		bool	lockAddressZeroCanceled() noexcept;
		/** */
		bool	pipeAllocated() noexcept;
		/** */
		bool	irpReadError() noexcept;
		/** */
		bool	irpNoDataError() noexcept;
		/** */
		void	returnOpenReq() noexcept;
		/** */
		void	returnCloseReq() noexcept;
	private:
		/** */
		void	startTimer(unsigned long milliseconds) noexcept;
	};

}
}
}
}

#endif
