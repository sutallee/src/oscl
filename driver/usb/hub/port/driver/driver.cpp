/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <new>
#include "driver.h"
#include "oscl/mt/thread.h"
#include "oscl/driver/usb/pipe/status.h"

using namespace Oscl::Usb::Hub::Port;

Driver::Driver(	Oscl::Usb::Alloc::Addr::
				Req::Api::SAP&						hcdPortSAP,
				Oscl::Usb::Hub::Port::
				Req::Api::SAP&						hubPortSAP,
				Oscl::Usb::Alloc::Pipe::
				Message::Req::Api::SAP&				msgPipeAllocSAP,
				Oscl::Mt::Itc::Dyn::Adv::
				Creator::SyncApi<DynDevice>&		advCapi,
				Oscl::Mt::Itc::Dyn::Adv::
				Core<DynDevice>::ReleaseApi&		advRapi,
				Oscl::Mt::Itc::PostMsgApi&			advPapi,
				Oscl::Usb::Alloc::Power::
				Req::Api::SAP&						powerSAP,
				Oscl::Usb::Alloc::Power::
				SyncApi&							powerSyncApi,
				Oscl::Usb::HCD::Enum::
				Monitor::Api&						hcdEnumMonitorApi,
				Oscl::Usb::HCD::Enum::
				DefCntrlPipe::Api&					hcdEnumDcpApi,
				Oscl::Mt::Itc::PostMsgApi&			dynSrvPapi,
				Oscl::Usb::Pipe::Message::
				Req::Api::SAP&						hsDefaultPipeSAP,
				Oscl::Usb::Pipe::Message::
				Req::Api::SAP&						lsDefaultPipeSAP,
				Oscl::Mt::Itc::
				Delay::Req::Api::SAP&				delayServiceSAP,
				Oscl::Mt::Itc::PostMsgApi&			myPapi,
				PacketMem&							packetMem,
				SetupPktMem&						setupPktMem,
				const Oscl::ObjectID::RO::Api*		loc
				) noexcept:
		Oscl::Mt::Itc::Srv::CloseSync(*this,myPapi),
		_hcdPortSAP(hcdPortSAP),
		_hubPortSAP(hubPortSAP),
		_msgPipeAllocSAP(msgPipeAllocSAP),
		_powerSAP(powerSAP),
		_powerSyncApi(powerSyncApi),
		_hcdEnumMonitorApi(hcdEnumMonitorApi),
		_hcdEnumDcpApi(hcdEnumDcpApi),
		_dynDevice(0),
		_advCapi(advCapi),
		_advRapi(advRapi),
		_advPapi(advPapi),
		_dynSrvPapi(dynSrvPapi),
		_myPapi(myPapi),
		_hsDefaultPipeSAP(hsDefaultPipeSAP),
		_lsDefaultPipeSAP(lsDefaultPipeSAP),
		_delayServiceSAP(delayServiceSAP),
		_packetMem(packetMem),
		_setupMem(setupPktMem),
		_state(*this),
		_usbAddress(0)
		{
	if(loc){
		static_cast<Oscl::ObjectID::Api&>(_location)	= *loc;
		}
	}

void	Driver::initialize() noexcept{
	// FIXME: delete this operation? where called?
	while(true);
	}

Oscl::Mt::Itc::Srv::Close::Req::Api::SAP&
Driver::getCloseSAP() noexcept{
	return this->getSAP();
	}

void	Driver::request(	Oscl::Mt::Itc::Srv::Open::
							Req::Api::OpenReq&			msg
							) noexcept{
	_openReq	= &msg;
	_state.open();
	}

void	Driver::request(	Oscl::Mt::Itc::Srv::Close::
							Req::Api::CloseReq&			msg
							) noexcept{
	_closeReq	= &msg;
	_state.close();
	}

void	Driver::response(	Oscl::Usb::Alloc::Addr::
							Resp::Api::LockAddressZeroResp&	msg
							) noexcept{
	_lastRespMsg.hcdHubPort.lockAddrZero	= &msg;
	_lockAddrZeroCanceled	= msg.getSrvMsg()._payload._canceled;
	_state.hcdLockAddressZeroDone();
	}

void	Driver::response(	Oscl::Usb::Alloc::Addr::
							Resp::Api::UnlockAddressZeroResp&	msg
							) noexcept{
	_lastRespMsg.hcdHubPort.unlockAddrZero	= &msg;
	_state.hcdUnlockAddressZeroDone();
	}

void	Driver::response(	Oscl::Usb::Alloc::Addr::
							Resp::Api::AllocateNewAddressResp&	msg
							) noexcept{
	_lastRespMsg.hcdHubPort.allocUsbAddr	= &msg;
	_usbAddrAllocated		= msg.getPayload().succeeded();
	_usbAddress				= msg.getPayload().getAddress();
	_state.hcdAllocUsbAddrDone();
	}

void	Driver::response(	Oscl::Usb::Alloc::Addr::
							Resp::Api::FreeAddressResp&	msg
							) noexcept{
	_lastRespMsg.hcdHubPort.freeUsbAddr	= &msg;
	_state.hcdFreeUsbAddrDone();
	}

void	Driver::response(	Oscl::Usb::Alloc::Addr::
							Resp::Api::CancelAddrZeroLockResp&	msg
							) noexcept{
	_state.hcdCancelLockAddrZeroDone();
	}

void	Driver::response(	Oscl::Usb::Alloc::Addr::
							Resp::Api::UsbAddrResChangeResp&	msg
							) noexcept{
	_state.notifyUsbAddrResChangeDone();
	}

void	Driver::response(	Oscl::Usb::Alloc::Addr::
							Resp::Api::CancelUsbAddrResChangeResp&	msg
							) noexcept{
	_state.cancelNotifyUsbAddrResChangeDone();
	}

void	Driver::response(NewResp& msg) noexcept{
	_lastRespMsg.hcdAlloc.msgPipeAlloc	= &msg;
	_devicePipe	= msg.getPayload().getPipeApi();
	_state.hcdMsgPipeAllocDone();
	}

void	Driver::response(FreeResp& msg) noexcept{
	_lastRespMsg.hcdAlloc.msgPipeFree	= &msg;
	_state.hcdMsgPipeFreeDone();
	}

void	Driver::response(NotifyChangeResp& msg) noexcept{
	_state.notifyMsgPipeResourceChangeDone();
	}

void	Driver::response(CancelChangeResp& msg) noexcept{
	_state.cancelNotifyMsgPipeResourceChangeDone();
	}

void	Driver::response(	Oscl::Usb::Pipe::Message::
							Resp::Api::CancelResp&		msg
							) noexcept{
	_lastRespMsg.msgPipeIrp.cancel	= &msg;
	_state.msgPipeIrpCancelDone();
	}

void	Driver::response(	Oscl::Usb::Pipe::Message::
							Resp::Api::NoDataResp&		msg
							) noexcept{
	_lastRespMsg.msgPipeIrp.noData	= &msg;
	_state.msgPipeNoDataIrpDone();
	}

void	Driver::response(	Oscl::Usb::Pipe::Message::
							Resp::Api::ReadResp&			msg
							) noexcept{
	_lastRespMsg.msgPipeIrp.read	= &msg;
	_devDesc	= (Oscl::Usb::Desc::Device*)msg.getPayload().getDataBuffer();
	_maxPacketSize	= _devDesc->_bMaxPacketSize;
	_state.msgPipeReadIrpDone();
	}

void	Driver::response(	Oscl::Usb::Pipe::Message::
							Resp::Api::WriteResp&			msg
							) noexcept{
	_lastRespMsg.msgPipeIrp.write	= &msg;
	_state.msgPipeWriteIrpDone();
	}

////// Oscl::Usb::Hub::Port::Resp::Api //////

void	Driver::response(SetResetResp& msg) noexcept{
	_lastRespMsg.hubPort.setReset	= &msg;
	if(msg.getPayload()._failed){
		_state.requestFailed();
		}
	else{
		_state.setResetDone();
		}
	}

void	Driver::response(SetSuspendResp& msg) noexcept{
	_lastRespMsg.hubPort.setSuspend	= &msg;
	if(msg.getPayload()._failed){
		_state.requestFailed();
		}
	else{
		_state.setSuspendDone();
		}
	}

void	Driver::response(SetPowerResp& msg) noexcept{
	_lastRespMsg.hubPort.setPower	= &msg;
	if(msg.getPayload()._failed){
		_state.requestFailed();
		}
	else{
		_state.setPowerDone();
		}
	}

void	Driver::response(ClearEnableResp& msg) noexcept{
	_lastRespMsg.hubPort.clrEnable	= &msg;
	if(msg.getPayload()._failed){
		_state.requestFailed();
		}
	else{
		_state.clrEnableDone();
		}
	}

void	Driver::response(ClearSuspendResp& msg) noexcept{
	_lastRespMsg.hubPort.clrSuspended	= &msg;
	if(msg.getPayload()._failed){
		_state.requestFailed();
		}
	else{
		_state.clrSuspendDone();
		}
	}

void	Driver::response(ClearPowerResp& msg) noexcept{
	_lastRespMsg.hubPort.clrPower	= &msg;
	if(msg.getPayload()._failed){
		_state.requestFailed();
		}
	else{
		_state.clrPowerDone();
		}
	}

void	Driver::response(ClearConnectChangeResp& msg) noexcept{
	_lastRespMsg.hubPort.clrConnectChange	= &msg;
	if(msg.getPayload()._failed){
		_state.requestFailed();
		}
	else{
		_state.clrConnectChangeDone();
		}
	}

void	Driver::response(ClearResetChangeResp& msg) noexcept{
	_lastRespMsg.hubPort.clrResetChange	= &msg;
	if(msg.getPayload()._failed){
		_state.requestFailed();
		}
	else{
		_state.clrResetChangeDone();
		}
	}

void	Driver::response(ClearEnableChangeResp& msg) noexcept{
	_lastRespMsg.hubPort.clrEnableChange	= &msg;
	if(msg.getPayload()._failed){
		_state.requestFailed();
		}
	else{
		_state.clrEnableChangeDone();
		}
	}

void	Driver::response(ClearSuspendChangeResp& msg) noexcept{
	_lastRespMsg.hubPort.clrSuspendChange	= &msg;
	if(msg.getPayload()._failed){
		_state.requestFailed();
		}
	else{
		_state.clrSuspendChangeDone();
		}
	}

void	Driver::response(ClearOverCurrentChangeResp& msg) noexcept{
	_lastRespMsg.hubPort.clrOverCurrentChange	= &msg;
	if(msg.getPayload()._failed){
		_state.requestFailed();
		}
	else{
		_state.clrOverCurrentChangeDone();
		}
	}

void	Driver::response(	Oscl::Usb::Hub::Port::
							Resp::Api::CancelResp&	msg
							) noexcept{
	_lastRespMsg.hubPort.cancel	= &msg;
	_state.cancelReqDone();
	}

void	Driver::response(NotifyPortChangeResp& msg) noexcept{
	_lastRespMsg.hubPort.notifyPortChange	= &msg;
	if(msg.getPayload()._failed){
		_state.requestFailed();
		}
	else{
		_portChange	= msg.getSrvMsg()._payload._lastChange;
		_state.notifyPortChangeDone();
		}
	}

void	Driver::response(CancelNotifyPortChangeResp& msg) noexcept{
	_lastRespMsg.hubPort.cancelNotifyPortChange	= &msg;
	_state.cancelNotifyPortChangeDone();
	}

void	Driver::response(GetPortStatusResp& msg) noexcept{
	_lastRespMsg.hubPort.getStatus	= &msg;
	if(msg.getPayload()._failed){
		_state.requestFailed();
		}
	else{
		_portStatus	= msg.getSrvMsg()._payload._status;
		_state.getPortStatusDone();
		}
	}

void	Driver::response(	Oscl::Mt::Itc::Dyn::Creator::
							Resp::Api<DynDevice>::DeactivateResp&	msg
							) noexcept{
	_state.prepareToCloseDynSrvDone();
	}

void	Driver::response(	Oscl::Mt::Itc::Srv::Close::
							Resp::Api::CloseResp&		msg
							) noexcept{
	_state.closeDynSrvDone();
	}

void	Driver::response(	Oscl::Mt::Itc::Srv::Open::
							Resp::Api::OpenResp&		msg
							) noexcept{
	// FIXME: ?? open synchronously, not used ??
	//_state.openDynSrvDone();
	while(true);
	}

void	Driver::response(	Oscl::Mt::Itc::Delay::
							Resp::Api::DelayResp&	msg
							) noexcept{
	_state.timerExpired();
	}

void	Driver::response(	Oscl::Mt::Itc::Delay::
							Resp::Api::CancelResp&	msg
							) noexcept{
	_state.timerCanceled();
	}

void	Driver::requestSetReset() noexcept{
	Oscl::Usb::Hub::Port::
	Resp::Api::SetResetResp&
	resp	= _respMem.hubPort.setReset.create(	_hubPortSAP.getReqApi(),
												*this,
												_myPapi
												);
	_currentReq.srvmsg	= &resp.getSrvMsg();
	_hubPortSAP.post(resp.getSrvMsg());
	}

void	Driver::requestSetSuspend() noexcept{
	Oscl::Usb::Hub::Port::
	Resp::Api::SetSuspendResp&
	resp	= _respMem.hubPort.setSuspend.create(	_hubPortSAP.getReqApi(),
													*this,
													_myPapi
													);
	_currentReq.srvmsg	= &resp.getSrvMsg();
	_hubPortSAP.post(resp.getSrvMsg());
	}

void	Driver::requestSetPower() noexcept{
	Oscl::Usb::Hub::Port::
	Resp::Api::SetPowerResp&
	resp	= _respMem.hubPort.setPower.create(	_hubPortSAP.getReqApi(),
												*this,
												_myPapi
												);
	_currentReq.srvmsg	= &resp.getSrvMsg();
	_hubPortSAP.post(resp.getSrvMsg());
	}

void	Driver::requestClrEnable() noexcept{
	Oscl::Usb::Hub::Port::
	Resp::Api::ClearEnableResp&
	resp	= _respMem.hubPort.clrEnable.create(	_hubPortSAP.getReqApi(),
													*this,
													_myPapi
													);
	_currentReq.srvmsg	= &resp.getSrvMsg();
	_hubPortSAP.post(resp.getSrvMsg());
	}

void	Driver::requestClrSuspend() noexcept{
	Oscl::Usb::Hub::Port::
	Resp::Api::ClearSuspendResp&
	resp	= _respMem.hubPort.clrSuspend.create(	_hubPortSAP.getReqApi(),
													*this,
													_myPapi
													);
	_currentReq.srvmsg	= &resp.getSrvMsg();
	_hubPortSAP.post(resp.getSrvMsg());
	}

void	Driver::requestClrPower() noexcept{
	Oscl::Usb::Hub::Port::
	Resp::Api::ClearPowerResp&
	resp	= _respMem.hubPort.clrPower.create(	_hubPortSAP.getReqApi(),
												*this,
												_myPapi
												);
	_currentReq.srvmsg	= &resp.getSrvMsg();
	_hubPortSAP.post(resp.getSrvMsg());
	}

void	Driver::requestClrConnectChange() noexcept{
	Oscl::Usb::Hub::Port::
	Resp::Api::ClearConnectChangeResp&
	resp	= _respMem.hubPort.clrConnectChange.create(	_hubPortSAP.getReqApi(),
														*this,
														_myPapi
														);
	_currentReq.srvmsg	= &resp.getSrvMsg();
	_hubPortSAP.post(resp.getSrvMsg());
	}

void	Driver::requestClrResetChange() noexcept{
	Oscl::Usb::Hub::Port::
	Resp::Api::ClearResetChangeResp&
	resp	= _respMem.hubPort.clrResetChange.create(	_hubPortSAP.getReqApi(),
														*this,
														_myPapi
														);
	_currentReq.srvmsg	= &resp.getSrvMsg();
	_hubPortSAP.post(resp.getSrvMsg());
	}

void	Driver::requestClrEnableChange() noexcept{
	Oscl::Usb::Hub::Port::
	Resp::Api::ClearEnableChangeResp&
	resp	= _respMem.hubPort.clrEnableChange.create(	_hubPortSAP.getReqApi(),
														*this,
														_myPapi
														);
	_currentReq.srvmsg	= &resp.getSrvMsg();
	_hubPortSAP.post(resp.getSrvMsg());
	}

void	Driver::requestClrSuspendChange() noexcept{
	Oscl::Usb::Hub::Port::
	Resp::Api::ClearSuspendChangeResp&
	resp	= _respMem.hubPort.clrSuspendChange.create(	_hubPortSAP.getReqApi(),
														*this,
														_myPapi
														);
	_currentReq.srvmsg	= &resp.getSrvMsg();
	_hubPortSAP.post(resp.getSrvMsg());
	}

void	Driver::requestClrOverCurrentChange() noexcept{
	Oscl::Usb::Hub::Port::
	Resp::Api::ClearOverCurrentChangeResp&
	resp	= _respMem.hubPort.
				clrOverCurrentChange.create(	_hubPortSAP.getReqApi(),
												*this,
												_myPapi
												);
	_currentReq.srvmsg	= &resp.getSrvMsg();
	_hubPortSAP.post(resp.getSrvMsg());
	}

void	Driver::requestCancelReq() noexcept{
	Resp::Api::CancelResp&
	resp	= _hubPortCancelMem.cancelReq.create(	_hubPortSAP.getReqApi(),
													*this,
													_myPapi,
													*_currentReq.srvmsg
													);
	_hubPortSAP.post(resp.getSrvMsg());
	}

void	Driver::requestNotifyPortChange() noexcept{
	Oscl::Usb::Hub::Port::
	Resp::Api::NotifyPortChangeResp&
	resp	= _portChangeMem.create(	_hubPortSAP.getReqApi(),
										*this,
										_myPapi
										);
	_currentChangeReq	= &resp.getSrvMsg();
	_hubPortSAP.post(resp.getSrvMsg());
	}

void	Driver::requestCancelNotifyPortChange() noexcept{
	Oscl::Usb::Hub::Port::
	Resp::Api::CancelNotifyPortChangeResp&
	resp	= _hubPortCancelMem.
				cancelNotifyPortChange.create(	_hubPortSAP.getReqApi(),
												*this,
												_myPapi,
												*_currentChangeReq
												);
	_hubPortSAP.post(resp.getSrvMsg());
	}

void	Driver::requestGetPortStatus() noexcept{
	Oscl::Usb::Hub::Port::
	Resp::Api::GetPortStatusResp&
	resp	= _respMem.hubPort.getStatus.create(	_hubPortSAP.getReqApi(),
													*this,
													_myPapi
													);
	_currentReq.srvmsg	= &resp.getSrvMsg();
	_hubPortSAP.post(resp.getSrvMsg());
	}

void	Driver::requestMsgPipeIrpCancel() noexcept{
	Oscl::Usb::Pipe::Message::Req::Api::SAP*	sap;
	sap	= _lastReqSAP;
	Oscl::Usb::Pipe::Message::
	Req::Api::CancelPayload*
	payload	= new(&_irpCancelMem.payload)
				Oscl::Usb::Pipe::Message::
				Req::Api::CancelPayload(*_currentReq.srvmsg);
	Oscl::Usb::Pipe::Message::
	Resp::Api::CancelResp*
	resp	= new(&_irpCancelMem.resp)
				Oscl::Usb::Pipe::Message::
				Resp::Api::CancelResp(	sap->getReqApi(),
										*this,
										_myPapi,
										*payload
										);
	sap->post(resp->getSrvMsg());
	}

void	Driver::requestMsgPipeNoDataIrp() noexcept{
	_lowSpeedDevice =
			_portStatus.lowSpeedDeviceAttached();
	using namespace Oscl::Usb::Hw::Setup;
	Oscl::Usb::Setup::Packet*
	setupPkt =
		new (&_setupMem)
			Oscl::Usb::Setup::
			SetAddress(_usbAddress);

	Oscl::Usb::Pipe::Message::
	Req::Api::NoDataPayload*	payload	=
		new (&_respMem.msgPipeIrp._noDataIrp.payload)
				Oscl::Usb::Pipe::Message::
				Req::Api::NoDataPayload(*setupPkt);

	Oscl::Usb::Pipe::Message::Req::Api::SAP*	sap;
	if(_lowSpeedDevice){
		sap	= &_lsDefaultPipeSAP;
		}
	else{
		sap	= &_hsDefaultPipeSAP;
		}
	_lastReqSAP	= sap;

	Oscl::Usb::Pipe::Message::
	Resp::Api::NoDataResp*	msg	=
		new (&_respMem.msgPipeIrp._noDataIrp.resp)
			Oscl::Usb::Pipe::Message::
			Resp::Api::NoDataResp(
				sap->getReqApi(),
				*this,
				_myPapi,
				*payload
				);
	_currentReq.srvmsg	= &msg->getSrvMsg();
	sap->post(msg->getSrvMsg());
	}

void	Driver::requestMsgPipeReadPartIrp() noexcept{
	Oscl::Usb::Pipe::Message::Req::Api::SAP*	sap;
	if(_portStatus.lowSpeedDeviceAttached()){
		sap	= &_lsDefaultPipeSAP;
		}
	else{
		sap	= &_hsDefaultPipeSAP;
		}
	requestReadDesc(*sap,8);	// 8 is minimum USB maximumPacketSize
	}

void	Driver::requestMsgPipeReadFullIrp() noexcept{
	requestReadDesc(	_devicePipe->getSAP(),
						Oscl::Usb::Hw::Desc::Device::Length
						);
	}

void	Driver::requestReadDesc(	Oscl::Usb::Pipe::
									Message::Req::Api::SAP&	sap,
									unsigned				length
									) noexcept{
	using namespace Oscl::Usb::Hw::Setup;
	_lastReqSAP	= &sap;
	Oscl::Usb::Setup::Packet*
	setupPkt =
		new (&_setupMem)
			Oscl::Usb::Setup::
			GetDescriptor(	wValue::Desc::Type::Value_DEVICE,
							0x00,	// descriptorIndex
							0,		// languageID
							length
							);
	using namespace Oscl::Usb::Pipe::Message;
	Oscl::Usb::Pipe::Message::
	Req::Api::ReadPayload*	payload	=
		new (&_respMem.msgPipeIrp._readIrp.payload)
				Oscl::Usb::Pipe::Message::
				Req::Api::ReadPayload(	*setupPkt,
										&_packetMem
										);
	Oscl::Usb::Pipe::Message::
	Resp::Api::ReadResp*	msg	=
		new (&_respMem.msgPipeIrp._readIrp.resp)
			Oscl::Usb::Pipe::Message::
			Resp::Api::ReadResp(
				sap.getReqApi(),
				*this,
				_myPapi,
				*payload
				);
	_currentReq.srvmsg	= &msg->getSrvMsg();
	sap.post(msg->getSrvMsg());
	}

void	Driver::requestMsgPipeWriteIrp() noexcept{
	// Not used.
	while(true);
	}

void	Driver::requestHcdMsgPipeAlloc() noexcept{
	Oscl::Usb::Alloc::Pipe::Message::
	Req::Api::NewPayload*
	payload	= new (&_respMem.hcdAlloc._newMessageEpMem.payload)
				Oscl::Usb::Alloc::Pipe::Message::
				Req::Api::NewPayload(	_usbAddress,	// Address
												0,	// EndpointID (default)
												_maxPacketSize,
												_lowSpeedDevice
												);
	Oscl::Usb::Alloc::Pipe::Message::
	Resp::Api::NewResp*
	resp	= new (&_respMem.hcdAlloc._newMessageEpMem.resp)
				Oscl::Usb::Alloc::Pipe::Message::
				Resp::Api::NewResp(	_msgPipeAllocSAP.getReqApi(),
									*this,
									_myPapi,
									*payload	// payload
									);
	_msgPipeAllocSAP.post(resp->getSrvMsg());
	}

void	Driver::requestHcdMsgPipeFree() noexcept{
	Oscl::Usb::Alloc::Pipe::Message::
	Req::Api::FreePayload*
	payload	= new (&_respMem.hcdAlloc._freeMessageEpMem.payload)
				Oscl::Usb::Alloc::Pipe::Message::
				Req::Api::FreePayload(	*_devicePipe);

	Oscl::Usb::Alloc::Pipe::Message::
	Resp::Api::FreeResp*
	resp	= new (&_respMem.hcdAlloc._freeMessageEpMem.resp)
				Oscl::Usb::Alloc::Pipe::Message::
				Resp::Api::FreeResp(	_msgPipeAllocSAP.getReqApi(),
										*this,
										_myPapi,
										*payload	// payload
										);
	_msgPipeAllocSAP.post(resp->getSrvMsg());
	}

void	Driver::requestNotifyMsgPipeResourceChange() noexcept{
	Oscl::Usb::Alloc::Pipe::Message::
	Resp::Api::NotifyChangeResp*
	resp	= new (&_pipeResChangeMem)
				Oscl::Usb::Alloc::Pipe::Message::
				Resp::Api::NotifyChangeResp(	_msgPipeAllocSAP.getReqApi(),
												*this,
												_myPapi
												);
	_pipeResChangeResp	= resp;
	_msgPipeAllocSAP.post(resp->getSrvMsg());
	}

void	Driver::requestCancelNotifyMsgPipeResChange() noexcept{
	Oscl::Usb::Alloc::Pipe::Message::
	Req::Api::CancelChangePayload*
	payload	= new (&_epAllocCancelMem._cancelChange.payload)
				Oscl::Usb::Alloc::Pipe::Message::
				Req::Api::CancelChangePayload(_pipeResChangeResp->getSrvMsg());

	Oscl::Usb::Alloc::Pipe::Message::
	Resp::Api::CancelChangeResp*
	resp	= new (&_epAllocCancelMem._cancelChange.resp)
				Oscl::Usb::Alloc::Pipe::Message::
				Resp::Api::CancelChangeResp(	_msgPipeAllocSAP.getReqApi(),
												*this,
												_myPapi,
												*payload	// payload
												);
	_msgPipeAllocSAP.post(resp->getSrvMsg());
	}

void	Driver::requestNotifyUsbAddrResourceChange() noexcept{
	Oscl::Usb::Alloc::Addr::
	Resp::Api::UsbAddrResChangeResp*	msg	=
		new (&_usbAddrChangeMem)
			Oscl::Usb::Alloc::Addr::
			Resp::Api::UsbAddrResChangeResp(
				_hcdPortSAP.getReqApi(),
				*this,
				_myPapi
				);
	_usbAddrChangeResp	= msg;
	_hcdPortSAP.post(msg->getSrvMsg());
	}

void	Driver::requestCancelNotifyUsbAddrResChange() noexcept{
	Oscl::Usb::Alloc::Addr::
	Req::Api::CancelUsbAddrResChangePayload*	payload	=
		new (&_hcdHubPortRespMem._cancelResChange.payload)
			Oscl::Usb::Alloc::Addr::Req::Api::
			CancelUsbAddrResChangePayload(_usbAddrChangeResp->getSrvMsg());

	Oscl::Usb::Alloc::Addr::
	Resp::Api::CancelUsbAddrResChangeResp*	msg	=
		new (&_hcdHubPortRespMem._cancelResChange.resp)
			Oscl::Usb::Alloc::Addr::
			Resp::Api::CancelUsbAddrResChangeResp(
				_hcdPortSAP.getReqApi(),
				*this,
				_myPapi,
				*payload
				);
	_hcdPortSAP.post(msg->getSrvMsg());
	}

void	Driver::prepareToCloseDynSrv() noexcept{
	Oscl::Mt::Itc::Dyn::Creator::
	Req::Api<DynDevice>::DeactivatePayload*
	payload	= new(&_respMem.deactivate.payload)
				Oscl::Mt::Itc::Dyn::Creator::
				Req::Api<DynDevice>::DeactivatePayload();

	Oscl::Mt::Itc::Dyn::Creator::
	Resp::Api<DynDevice>::DeactivateResp*
	resp	= new(&_respMem.deactivate.resp)
				Oscl::Mt::Itc::Dyn::Creator::
				Resp::Api<DynDevice>::
				DeactivateResp(	_dynDevice->getCreatorSAP().getReqApi(),
								*this,
								_myPapi,
								*payload
								);
	_dynDevice->getCreatorSAP().post(resp->getSrvMsg());
	}

void	Driver::closeDynSrv() noexcept{
	Oscl::Mt::Itc::Srv::Close::
	Req::Api::ClosePayload*
	payload	= new(&_respMem.close.close.payload)
				Oscl::Mt::Itc::Srv::Close::
				Req::Api::ClosePayload();
	Oscl::Mt::Itc::Srv::Close::
	Resp::Api::CloseResp*
	resp	= new(&_respMem.close.close.resp)
				Oscl::Mt::Itc::Srv::Close::
				Resp::Api::CloseResp(	_dynDevice->getCreatorSAP().getReqApi(),
										*this,
										_myPapi,
										*payload
										);
	_dynDevice->getCreatorSAP().post(resp->getSrvMsg());
	}

void	Driver::advertiseNewPipe() noexcept{
	_dynDevice	=	new(&_dynDevMem)
						DynDevice(	_dynSrvPapi,
									_advRapi,
									_advPapi,
									*_devicePipe,
									_usbAddress,
									_portStatus.lowSpeedDeviceAttached(),
									_powerSAP,
									_powerSyncApi,
									_hcdEnumMonitorApi,
									_hcdEnumDcpApi,
									*_devDesc,
									&_location
									);
	_dynDevice->getCreatorSAP().getReqApi().syncOpen();
	_advCapi.add(*_dynDevice);
	}

void	Driver::requestHcdLockAddressZero() noexcept{
	Oscl::Usb::Alloc::Addr::
	Resp::Api::LockAddressZeroResp*	msg	=
		new (&_respMem.hcdHubPort._lockAddressZero)
			Oscl::Usb::Alloc::Addr::
			Resp::Api::LockAddressZeroResp(
				_hcdPortSAP.getReqApi(),
				*this,
				_myPapi
				);
	_lockAddrZeroResp	= msg;
	_hcdPortSAP.post(msg->getSrvMsg());
	}

void	Driver::requestHcdUnlockAddressZero() noexcept{
	Oscl::Usb::Alloc::Addr::
	Resp::Api::UnlockAddressZeroResp*	msg	=
		new (&_respMem.hcdHubPort._unlockAddressZero)
			Oscl::Usb::Alloc::Addr::
			Resp::Api::UnlockAddressZeroResp(
				_hcdPortSAP.getReqApi(),
				*this,
				_myPapi
				);
	_hcdPortSAP.post(msg->getSrvMsg());
	}

void	Driver::requestHcdCancelLockAddrZero() noexcept{
	Oscl::Usb::Alloc::Addr::
	Req::Api::CancelAddrZeroLockPayload*	payload	=
		new (&_hcdHubPortRespMem._cancelLockAddrZero.payload)
			Oscl::Usb::Alloc::Addr::Req::Api::
			CancelAddrZeroLockPayload(_lockAddrZeroResp->getSrvMsg());

	Oscl::Usb::Alloc::Addr::
	Resp::Api::CancelAddrZeroLockResp*	msg	=
		new (&_hcdHubPortRespMem._cancelLockAddrZero.resp)
			Oscl::Usb::Alloc::Addr::
			Resp::Api::CancelAddrZeroLockResp(
				_hcdPortSAP.getReqApi(),
				*this,
				_myPapi,
				*payload
				);
	_hcdPortSAP.post(msg->getSrvMsg());
	}

void	Driver::requestHcdAllocUsbAddr() noexcept{
	Oscl::Usb::Alloc::Addr::
	Req::Api::AllocateNewAddressPayload*	payload	=
		new (&_respMem.hcdHubPort._allocNewAddress.payload)
			Oscl::Usb::Alloc::Addr::Req::Api::
			AllocateNewAddressPayload(false);

	Oscl::Usb::Alloc::Addr::
	Resp::Api::AllocateNewAddressResp*	msg	=
		new (&_respMem.hcdHubPort._allocNewAddress.resp)
			Oscl::Usb::Alloc::Addr::
			Resp::Api::AllocateNewAddressResp(
				_hcdPortSAP.getReqApi(),
				*this,
				_myPapi,
				*payload
				);
	_hcdPortSAP.post(msg->getSrvMsg());
	}

void	Driver::requestHcdFreeUsbAddr() noexcept{
	Oscl::Usb::Alloc::Addr::
	Req::Api::FreeAddressPayload*	payload	=
		new (&_respMem.hcdHubPort._allocNewAddress.payload)
			Oscl::Usb::Alloc::Addr::Req::Api::
			FreeAddressPayload(_usbAddress);

	Oscl::Usb::Alloc::Addr::
	Resp::Api::FreeAddressResp*	msg	=
		new (&_respMem.hcdHubPort._allocNewAddress.resp)
			Oscl::Usb::Alloc::Addr::
			Resp::Api::FreeAddressResp(
				_hcdPortSAP.getReqApi(),
				*this,
				_myPapi,
				*payload
				);
	_hcdPortSAP.post(msg->getSrvMsg());
	}

void	Driver::parseChange() noexcept{
	if(_portChange.portEnableStatusChange()){
		_state.portDisabled();
		return;
		}
	if(_portChange.currentConnectStatusChange()){
		_state.connectChanged();
		return;
		}
	if(_portChange.portSuspendStatusChange()){
		_state.portResumed();
		return;
		}
	if(_portChange.portOverCurrentIndicatorChange()){
		_state.portOverCurrentChange();
		return;
		}
	if(_portChange.portResetStatusChange()){
		_state.portResetDone();
		return;
		}
	return;
	}

void	Driver::startRetryTimer() noexcept{
	startTimer(1000);	// FIXME: 1s hard coded
	}

void	Driver::startPowerTimer() noexcept{
	startTimer(700);	// FIXME: 700ms hard coded
	}

void	Driver::startResetTimer() noexcept{
	startTimer(1000);	// FIXME: 1000ms hard coded
	}

void	Driver::startTimer(unsigned long milliseconds) noexcept{
	Oscl::Mt::Itc::Delay::Req::Api::DelayPayload*
	payload		= new(&_delayMem.payload)
					Oscl::Mt::Itc::Delay::
					Req::Api::DelayPayload(milliseconds);
	_delayResp	= new(&_delayMem.resp)
					Oscl::Mt::Itc::Delay::
					Resp::Api::DelayResp(	_delayServiceSAP.getReqApi(),
											*this,
											_myPapi,
											*payload
											);
	_delayServiceSAP.post(_delayResp->getSrvMsg());
	}

void	Driver::cancelTimer() noexcept{
	Oscl::Mt::Itc::Delay::Req::Api::CancelPayload*
	payload		= new(&_delayCancelMem.payload)
					Oscl::Mt::Itc::Delay::
					Req::Api::CancelPayload(_delayResp->getSrvMsg());
	Oscl::Mt::Itc::Delay::
	Resp::Api::CancelResp*
	cancelResp	= new(&_delayCancelMem.resp)
					Oscl::Mt::Itc::Delay::
					Resp::Api::CancelResp(	_delayServiceSAP.getReqApi(),
											*this,
											_myPapi,
											*payload
											);
	_delayServiceSAP.post(cancelResp->getSrvMsg());
	}

bool	Driver::deviceConnected() noexcept{
	return _portStatus.deviceConnected();
	}

bool	Driver::overCurrent() noexcept{
	return _portStatus.overcurrent();
	}

bool	Driver::usbAddrAllocated() noexcept{
	return _usbAddrAllocated;
	}

bool	Driver::lockAddressZeroCanceled() noexcept{
	return _lockAddrZeroCanceled;
	}

bool	Driver::pipeAllocated() noexcept{
	return _devicePipe;
	}

bool	Driver::irpReadError() noexcept{
	const Oscl::Usb::Pipe::Status::Result*
	result	= _lastRespMsg.msgPipeIrp.read->getPayload()._failed;
	return result;
	}

bool	Driver::irpNoDataError() noexcept{
	return _lastRespMsg.msgPipeIrp.noData->getPayload()._failed;
	}

void	Driver::returnOpenReq() noexcept{
	_openReq->returnToSender();
	}

void	Driver::returnCloseReq() noexcept{
	_closeReq->returnToSender();
	}
