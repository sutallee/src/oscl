/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <new>
#include "respmem.h"

using namespace Oscl::Usb::Hub::Port::Resp;
using namespace Oscl::Usb::Hub::Port;

Resp::Api::SetResetResp&
SetResetMem::create(	Req::Api&	srv,
						Resp::Api&	cli,
						Oscl::Mt::Itc::PostMsgApi&	papi
						) noexcept{
	Req::Api::SetResetPayload*
	payload	= new(&_payload)
				Req::Api::SetResetPayload();
	Resp::Api::SetResetResp*
	resp	= new(&_resp)
				Resp::Api::SetResetResp(srv,cli,papi,*payload);
	return *resp;
	}

Resp::Api::SetSuspendResp&
SetSuspendMem::create(	Req::Api&	srv,
						Resp::Api&	cli,
						Oscl::Mt::Itc::PostMsgApi&	papi
						) noexcept{
	Req::Api::SetSuspendPayload*
	payload	= new(&_payload)
				Req::Api::SetSuspendPayload();
	Resp::Api::SetSuspendResp*
	resp	= new(&_resp)
				Resp::Api::SetSuspendResp(srv,cli,papi,*payload);
	return *resp;
	}

Resp::Api::SetPowerResp&
SetPowerMem::create(	Req::Api&	srv,
						Resp::Api&	cli,
						Oscl::Mt::Itc::PostMsgApi&	papi
						) noexcept{
	Req::Api::SetPowerPayload*
	payload	= new(&_payload)
				Req::Api::SetPowerPayload();
	Resp::Api::SetPowerResp*
	resp	= new(&_resp)
				Resp::Api::SetPowerResp(srv,cli,papi,*payload);
	return *resp;
	}

Resp::Api::ClearEnableResp&
ClearEnableMem::create(	Req::Api&	srv,
						Resp::Api&	cli,
						Oscl::Mt::Itc::PostMsgApi&	papi
						) noexcept{
	Req::Api::ClearEnablePayload*
	payload	= new(&_payload)
				Req::Api::ClearEnablePayload();
	Resp::Api::ClearEnableResp*
	resp	= new(&_resp)
				Resp::Api::ClearEnableResp(srv,cli,papi,*payload);
	return *resp;
	}

Resp::Api::ClearSuspendResp&
ClearSuspendMem::create(	Req::Api&	srv,
							Resp::Api&	cli,
							Oscl::Mt::Itc::PostMsgApi&	papi
							) noexcept{
	Req::Api::ClearSuspendPayload*
	payload	= new(&_payload)
				Req::Api::ClearSuspendPayload();
	Resp::Api::ClearSuspendResp*
	resp	= new(&_resp)
				Resp::Api::ClearSuspendResp(srv,cli,papi,*payload);
	return *resp;
	}

Resp::Api::ClearPowerResp&
ClearPowerMem::create(	Req::Api&	srv,
						Resp::Api&	cli,
						Oscl::Mt::Itc::PostMsgApi&	papi
						) noexcept{
	Req::Api::ClearPowerPayload*
	payload	= new(&_payload)
				Req::Api::ClearPowerPayload();
	Resp::Api::ClearPowerResp*
	resp	= new(&_resp)
				Resp::Api::ClearPowerResp(srv,cli,papi,*payload);
	return *resp;
	}

Resp::Api::ClearConnectChangeResp&
ClearConnectChangeMem::create(	Req::Api&	srv,
								Resp::Api&	cli,
								Oscl::Mt::Itc::PostMsgApi&	papi
								) noexcept{
	Req::Api::ClearConnectChangePayload*
	payload	= new(&_payload)
				Req::Api::ClearConnectChangePayload();
	Resp::Api::ClearConnectChangeResp*
	resp	= new(&_resp)
				Resp::Api::ClearConnectChangeResp(srv,cli,papi,*payload);
	return *resp;
	}

Resp::Api::ClearResetChangeResp&
ClearResetChangeMem::create(	Req::Api&	srv,
								Resp::Api&	cli,
								Oscl::Mt::Itc::PostMsgApi&	papi
								) noexcept{
	Req::Api::ClearResetChangePayload*
	payload	= new(&_payload)
				Req::Api::ClearResetChangePayload();
	Resp::Api::ClearResetChangeResp*
	resp	= new(&_resp)
				Resp::Api::ClearResetChangeResp(srv,cli,papi,*payload);
	return *resp;
	}

Resp::Api::ClearEnableChangeResp&
ClearEnableChangeMem::create(	Req::Api&	srv,
								Resp::Api&	cli,
								Oscl::Mt::Itc::PostMsgApi&	papi
								) noexcept{
	Req::Api::ClearEnableChangePayload*
	payload	= new(&_payload)
				Req::Api::ClearEnableChangePayload();
	Resp::Api::ClearEnableChangeResp*
	resp	= new(&_resp)
				Resp::Api::ClearEnableChangeResp(srv,cli,papi,*payload);
	return *resp;
	}

Resp::Api::ClearSuspendChangeResp&
ClearSuspendChangeMem::create(	Req::Api&	srv,
								Resp::Api&	cli,
								Oscl::Mt::Itc::PostMsgApi&	papi
								) noexcept{
	Req::Api::ClearSuspendChangePayload*
	payload	= new(&_payload)
				Req::Api::ClearSuspendChangePayload();
	Resp::Api::ClearSuspendChangeResp*
	resp	= new(&_resp)
				Resp::Api::ClearSuspendChangeResp(srv,cli,papi,*payload);
	return *resp;
	}

Resp::Api::ClearOverCurrentChangeResp&
ClearOverCurrentChangeMem::create(	Req::Api&	srv,
									Resp::Api&	cli,
									Oscl::Mt::Itc::PostMsgApi&	papi
									) noexcept{
	Req::Api::ClearOverCurrentChangePayload*
	payload	= new(&_payload)
				Req::Api::ClearOverCurrentChangePayload();
	Resp::Api::ClearOverCurrentChangeResp*
	resp	= new(&_resp)
				Resp::Api::ClearOverCurrentChangeResp(srv,cli,papi,*payload);
	return *resp;
	}

Resp::Api::CancelResp&
CancelReqMem::create(	Req::Api&					srv,
						Resp::Api&					cli,
						Oscl::Mt::Itc::PostMsgApi&	papi,
						Oscl::Mt::Itc::SrvMsg&		reqToCancel
						) noexcept{
	Req::Api::CancelPayload*
	payload	= new(&_payload)
				Req::Api::CancelPayload(reqToCancel);
	Resp::Api::CancelResp*
	resp	= new(&_resp)
				Resp::Api::CancelResp(srv,cli,papi,*payload);
	return *resp;
	}

Resp::Api::NotifyPortChangeResp&
NotifyPortChangeMem::create(	Req::Api&	srv,
								Resp::Api&	cli,
								Oscl::Mt::Itc::PostMsgApi&	papi
								) noexcept{
	Req::Api::NotifyPortChangePayload*
	payload	= new(&_payload)
				Req::Api::NotifyPortChangePayload();
	Resp::Api::NotifyPortChangeResp*
	resp	= new(&_resp)
				Resp::Api::NotifyPortChangeResp(srv,cli,papi,*payload);
	return *resp;
	}

Resp::Api::CancelNotifyPortChangeResp&
CancelNotifyPortChangeMem::create(	Req::Api&						srv,
									Resp::Api&						cli,
									Oscl::Mt::Itc::PostMsgApi&		papi,
									Req::Api::NotifyPortChangeReq&	reqToCancel
									) noexcept{
	Req::Api::CancelNotifyPortChangePayload*
	payload	= new(&_payload)
				Req::Api::CancelNotifyPortChangePayload(reqToCancel);
	Resp::Api::CancelNotifyPortChangeResp*
	resp	= new(&_resp)
				Resp::Api::CancelNotifyPortChangeResp(srv,cli,papi,*payload);
	return *resp;
	}

Resp::Api::GetPortStatusResp&
GetPortStatusMem::create(	Req::Api&	srv,
							Resp::Api&	cli,
							Oscl::Mt::Itc::PostMsgApi&	papi
							) noexcept{
	Req::Api::GetPortStatusPayload*
	payload	= new(&_payload)
				Req::Api::GetPortStatusPayload();
	Resp::Api::GetPortStatusResp*
	resp	= new(&_resp)
				Resp::Api::GetPortStatusResp(srv,cli,papi,*payload);
	return *resp;
	}

