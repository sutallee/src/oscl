/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_usb_hub_port_respmemh_
#define _oscl_usb_hub_port_respmemh_
#include "respapi.h"
#include "oscl/memory/block.h"

/** */
namespace Oscl {
/** */
namespace Usb {
/** */
namespace Hub {
/** */
namespace Port {
/** */
namespace Resp {

using namespace Oscl::Usb::Hub::Port;

/** */
struct SetResetMem {
	private:
		/** */
		Oscl::Memory::
		AlignedBlock<sizeof(Req::Api::SetResetPayload)>	_payload;
		/** */
		Oscl::Memory::
		AlignedBlock<sizeof(Resp::Api::SetResetResp)>	_resp;
	public:
		/** */
		Resp::Api::SetResetResp&	create(	Req::Api&					srv,
											Resp::Api&					cli,
											Oscl::Mt::Itc::PostMsgApi&	papi
											) noexcept;
	};

/** */
struct SetSuspendMem {
	private:
		/** */
		Oscl::Memory::
		AlignedBlock<sizeof(Req::Api::SetSuspendPayload)>	_payload;
		/** */
		Oscl::Memory::
		AlignedBlock<sizeof(Resp::Api::SetSuspendResp)>		_resp;
	public:
		/** */
		Resp::Api::SetSuspendResp&	create(	Req::Api&					srv,
											Resp::Api&					cli,
											Oscl::Mt::Itc::PostMsgApi&	papi
											) noexcept;
	};

/** */
struct SetPowerMem {
	private:
		/** */
		Oscl::Memory::
		AlignedBlock<sizeof(Req::Api::SetPowerPayload)>	_payload;
		/** */
		Oscl::Memory::
		AlignedBlock<sizeof(Resp::Api::SetPowerResp)>	_resp;
	public:
		/** */
		Resp::Api::SetPowerResp&	create(	Req::Api&					srv,
											Resp::Api&					cli,
											Oscl::Mt::Itc::PostMsgApi&	papi
											) noexcept;
	};

/** */
struct ClearEnableMem {
	private:
		/** */
		Oscl::Memory::
		AlignedBlock<sizeof(Req::Api::ClearEnablePayload)>	_payload;
		/** */
		Oscl::Memory::
		AlignedBlock<sizeof(Resp::Api::ClearEnableResp)>	_resp;
	public:
		/** */
		Resp::Api::ClearEnableResp&	create(	Req::Api&					srv,
											Resp::Api&					cli,
											Oscl::Mt::Itc::PostMsgApi&	papi
											) noexcept;
	};

/** */
struct ClearSuspendMem {
	private:
		/** */
		Oscl::Memory::
		AlignedBlock<sizeof(Req::Api::ClearSuspendPayload)>	_payload;
		/** */
		Oscl::Memory::
		AlignedBlock<sizeof(Resp::Api::ClearSuspendResp)>	_resp;
	public:
		/** */
		Resp::Api::ClearSuspendResp&	create(	Req::Api&					srv,
												Resp::Api&					cli,
												Oscl::Mt::Itc::PostMsgApi&	papi
												) noexcept;
	};

/** */
struct ClearPowerMem {
	private:
		/** */
		Oscl::Memory::
		AlignedBlock<sizeof(Req::Api::ClearPowerPayload)>	_payload;
		/** */
		Oscl::Memory::
		AlignedBlock<sizeof(Resp::Api::ClearPowerResp)>		_resp;
	public:
		/** */
		Resp::Api::ClearPowerResp&	create(	Req::Api&					srv,
											Resp::Api&					cli,
											Oscl::Mt::Itc::PostMsgApi&	papi
											) noexcept;
	};

/** */
struct ClearConnectChangeMem {
	public:
		/** */
		Oscl::Memory::
		AlignedBlock<sizeof(Req::Api::ClearConnectChangePayload)>	_payload;
		/** */
		Oscl::Memory::
		AlignedBlock<sizeof(Resp::Api::ClearConnectChangeResp)>		_resp;
	public:
		/** */
		Resp::Api::ClearConnectChangeResp&
			create(	Req::Api&					srv,
					Resp::Api&					cli,
					Oscl::Mt::Itc::PostMsgApi&	papi
					) noexcept;
	};

/** */
struct ClearResetChangeMem {
		/** */
		Oscl::Memory::
		AlignedBlock<sizeof(Req::Api::ClearResetChangePayload)>	_payload;
		/** */
		Oscl::Memory::
		AlignedBlock<sizeof(Resp::Api::ClearResetChangeResp)>	_resp;
	public:
		/** */
		Resp::Api::ClearResetChangeResp&
			create(	Req::Api&					srv,
					Resp::Api&					cli,
					Oscl::Mt::Itc::PostMsgApi&	papi
					) noexcept;
	};

/** */
struct ClearEnableChangeMem {
	private:
		/** */
		Oscl::Memory::
		AlignedBlock<sizeof(Req::Api::ClearEnableChangePayload)>	_payload;
		/** */
		Oscl::Memory::
		AlignedBlock<sizeof(Resp::Api::ClearEnableChangeResp)>		_resp;
	public:
		/** */
		Resp::Api::ClearEnableChangeResp&
			create(	Req::Api&	srv,
					Resp::Api&	cli,
					Oscl::Mt::Itc::PostMsgApi&	papi
					) noexcept;
	};

/** */
struct ClearSuspendChangeMem {
	private:
		/** */
		Oscl::Memory::
		AlignedBlock<sizeof(Req::Api::ClearSuspendChangePayload)>	_payload;
		/** */
		Oscl::Memory::
		AlignedBlock<sizeof(Resp::Api::ClearSuspendChangeResp)>		_resp;
	public:
		/** */
		Resp::Api::ClearSuspendChangeResp&
			create(	Req::Api&					srv,
					Resp::Api&					cli,
					Oscl::Mt::Itc::PostMsgApi&	papi
					) noexcept;
	};

/** */
struct ClearOverCurrentChangeMem {
	public:
		/** */
		Oscl::Memory::
		AlignedBlock<sizeof(Req::Api::ClearOverCurrentChangePayload)>	_payload;
		/** */
		Oscl::Memory::
		AlignedBlock<sizeof(Resp::Api::ClearOverCurrentChangeResp)>		_resp;
	public:
		/** */
		Resp::Api::ClearOverCurrentChangeResp&
			create(	Req::Api&					srv,
					Resp::Api&					cli,
					Oscl::Mt::Itc::PostMsgApi&	papi
					) noexcept;
	};

/** */
struct CancelReqMem {
	private:
		/** */
		Oscl::Memory::
		AlignedBlock<sizeof(Resp::Api::CancelResp)>		_resp;
		/** */
		Oscl::Memory::
		AlignedBlock<sizeof(Req::Api::CancelPayload)>	_payload;
	public:
		/** */
		Resp::Api::CancelResp&	create(	Req::Api&					srv,
										Resp::Api&					cli,
										Oscl::Mt::Itc::PostMsgApi&	papi,
										Oscl::Mt::Itc::SrvMsg&		reqToCancel
										) noexcept;
	};

/** */
struct NotifyPortChangeMem {
	private:
		/** */
		Oscl::Memory::
		AlignedBlock<sizeof(Req::Api::NotifyPortChangePayload)>	_payload;
		/** */
		Oscl::Memory::
		AlignedBlock<sizeof(Resp::Api::NotifyPortChangeResp)>	_resp;
	public:
		/** */
		Resp::Api::NotifyPortChangeResp&
			create(	Req::Api&					srv,
					Resp::Api&					cli,
					Oscl::Mt::Itc::PostMsgApi&	papi
					) noexcept;
	};

struct CancelNotifyPortChangeMem {
	private:
		/** */
		Oscl::Memory::
		AlignedBlock<sizeof(Resp::Api::CancelNotifyPortChangeResp)>		_resp;
		/** */
		Oscl::Memory::
		AlignedBlock<sizeof(Req::Api::CancelNotifyPortChangePayload)>	_payload;
	public:
		/** */
		Resp::Api::CancelNotifyPortChangeResp&
			create(	Req::Api&						srv,
					Resp::Api&						cli,
					Oscl::Mt::Itc::PostMsgApi&		papi,
					Req::Api::NotifyPortChangeReq&	reqToCancel
					) noexcept;
	};

/** */
struct GetPortStatusMem {
	private:
		/** */
		Oscl::Memory::
		AlignedBlock<sizeof(Req::Api::GetPortStatusPayload)>	_payload;
		/** */
		Oscl::Memory::
		AlignedBlock<sizeof(Resp::Api::GetPortStatusResp)>		_resp;
	public:
		/** */
		Resp::Api::GetPortStatusResp&
			create(	Req::Api&					srv,
					Resp::Api&					cli,
					Oscl::Mt::Itc::PostMsgApi&	papi
					) noexcept;
	};

/** */
typedef union ReqMem {
	/** */
	void*						__qitemlink;
	/** */
	SetResetMem					setReset;
	/** */
	SetSuspendMem				setSuspend;
	/** */
	SetPowerMem					setPower;
	/** */
	ClearEnableMem				clrEnable;
	/** */
	ClearSuspendMem				clrSuspend;
	/** */
	ClearPowerMem				clrPower;
	/** */
	ClearConnectChangeMem		clrConnectChange;
	/** */
	ClearResetChangeMem			clrResetChange;
	/** */
	ClearEnableChangeMem		clrEnableChange;
	/** */
	ClearSuspendChangeMem		clrSuspendChange;
	/** */
	ClearOverCurrentChangeMem	clrOverCurrentChange;
	/** */
	NotifyPortChangeMem			notifyPortChange;
	/** */
	GetPortStatusMem			getStatus;
	} ReqMem;

/** */
typedef union CancelMem {
	/** */
	void*						__qitemlink;
	/** */
	CancelReqMem				cancelReq;
	/** */
	CancelNotifyPortChangeMem	cancelNotifyPortChange;
	} CancelMem;

/** */
typedef union Mem {
	/** */
	void*						__qitemlink;
	/** */
	ReqMem						_reqMem;
	/** */
	CancelMem					_cancelMem;
	} Mem;

}
}
}
}
}

#endif
