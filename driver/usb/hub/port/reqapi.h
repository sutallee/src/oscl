/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_usb_hub_port_reqapih_
#define _oscl_usb_hub_port_reqapih_
#include "oscl/mt/itc/mbox/sap.h"
#include "oscl/mt/itc/mbox/srvreq.h"
#include "oscl/hw/usb/hub/port/status.h"
#include "oscl/bits/bitfield.h"
#include "oscl/driver/usb/hub/port/reg/status.h"
#include "oscl/driver/usb/hub/port/reg/change.h"

/** */
namespace Oscl {
/** */
namespace Usb {
/** */
namespace Hub {
/** */
namespace Port {
/** */
namespace Req {

/** */
class Api {
	public:
		/** */
		class SetResetPayload {
			public:
				/** */
				bool	_failed;
			public:
				/** */
				SetResetPayload() noexcept:_failed(false){}
			};
		/** */
		typedef Oscl::Mt::Itc::
				SrvRequest<Api,SetResetPayload>	SetResetReq;
		/** */
		class SetSuspendPayload {
			public:
				/** */
				bool	_failed;
			public:
				/** */
				SetSuspendPayload() noexcept:_failed(false){}
			};
		/** */
		typedef Oscl::Mt::Itc::
				SrvRequest<Api,SetSuspendPayload>	SetSuspendReq;
		/** */
		class SetPowerPayload {
			public:
				/** */
				bool	_failed;
			public:
				/** */
				SetPowerPayload() noexcept:_failed(false){}
			};
		/** */
		typedef Oscl::Mt::Itc::
				SrvRequest<Api,SetPowerPayload>	SetPowerReq;
		/** */
		class ClearEnablePayload {
			public:
				/** */
				bool	_failed;
			public:
				/** */
				ClearEnablePayload() noexcept:_failed(false){}
			};
		/** */
		typedef Oscl::Mt::Itc::
				SrvRequest<	Api,
							ClearEnablePayload
							>	ClearEnableReq;
		/** */
		class ClearSuspendPayload {
			public:
				/** */
				bool	_failed;
			public:
				/** */
				ClearSuspendPayload() noexcept:_failed(false){}
			};
		/** */
		typedef Oscl::Mt::Itc::
				SrvRequest<	Api,
							ClearSuspendPayload
							>	ClearSuspendReq;
		/** */
		class ClearPowerPayload {
			public:
				/** */
				bool	_failed;
			public:
				/** */
				ClearPowerPayload() noexcept:_failed(false){}
			};
		/** */
		typedef Oscl::Mt::Itc::
				SrvRequest<	Api,
							ClearPowerPayload
							>	ClearPowerReq;
		/** */
		class ClearConnectChangePayload {
			public:
				/** */
				bool	_failed;
			public:
				/** */
				ClearConnectChangePayload() noexcept:_failed(false){}
			};
		/** */
		typedef Oscl::Mt::Itc::
				SrvRequest<	Api,
							ClearConnectChangePayload
							>	ClearConnectChangeReq;
		/** */
		class ClearResetChangePayload {
			public:
				/** */
				bool	_failed;
			public:
				/** */
				ClearResetChangePayload() noexcept:_failed(false){}
			};
		/** */
		typedef Oscl::Mt::Itc::
				SrvRequest<	Api,
							ClearResetChangePayload
							>	ClearResetChangeReq;
		/** */
		class ClearEnableChangePayload {
			public:
				/** */
				bool	_failed;
			public:
				/** */
				ClearEnableChangePayload() noexcept:_failed(false){}
			};
		/** */
		typedef Oscl::Mt::Itc::
				SrvRequest<	Api,
							ClearEnableChangePayload
							>	ClearEnableChangeReq;
		/** */
		class ClearSuspendChangePayload {
			public:
				/** */
				bool	_failed;
			public:
				/** */
				ClearSuspendChangePayload() noexcept:_failed(false){}
			};
		/** */
		typedef Oscl::Mt::Itc::
				SrvRequest<	Api,
							ClearSuspendChangePayload
							>	ClearSuspendChangeReq;
		/** */
		class ClearOverCurrentChangePayload {
			public:
				/** */
				bool	_failed;
			public:
				/** */
				ClearOverCurrentChangePayload() noexcept:_failed(false){}
			};
		/** */
		typedef Oscl::Mt::Itc::
				SrvRequest<	Api,
							ClearOverCurrentChangePayload
							>	ClearOverCurrentChangeReq;
		/** */
		class CancelPayload {
			public:
				/** */
				Oscl::Mt::Itc::SrvMsg&	_reqToCancel;
			public:
				/** */
				CancelPayload(Oscl::Mt::Itc::SrvMsg& reqToCancel) noexcept:
					_reqToCancel(reqToCancel){}
			};
		/** */
		typedef Oscl::Mt::Itc::
				SrvRequest<Api,CancelPayload>	CancelReq;
		/** */
		class NotifyPortChangePayload {
			public:
				/** */
				Oscl::Usb::Hub::Port::Reg::Change	_lastChange;
				/** */
				bool	_failed;
			public:
				NotifyPortChangePayload() noexcept: _failed(false){
					_lastChange = 0;
					}
			};
		/** */
		typedef Oscl::Mt::Itc::
				SrvRequest<	Api,
							NotifyPortChangePayload
							>	NotifyPortChangeReq;
		/** */
		class CancelNotifyPortChangePayload {
			public:
				/** */
				NotifyPortChangeReq&	_reqToCancel;
			public:
				/** */
				CancelNotifyPortChangePayload(NotifyPortChangeReq& reqToCancel) noexcept:
					_reqToCancel(reqToCancel){}
			};
		/** */
		typedef Oscl::Mt::Itc::
				SrvRequest<	Api,
							CancelNotifyPortChangePayload
							>	CancelNotifyPortChangeReq;
		/** */
		class GetPortStatusPayload {
			public:
				/** */
				Oscl::Usb::Hub::Port::Reg::Status	_status;
				/** */
				bool								_failed;
			public:
				GetPortStatusPayload() noexcept:_failed(false){}
			};
		/** */
		typedef Oscl::Mt::Itc::
				SrvRequest<Api,GetPortStatusPayload>	GetPortStatusReq;

	public:
		/** */
		typedef Oscl::Mt::Itc::SAP<Api>			SAP;
		/** */
		typedef Oscl::Mt::Itc::ConcreteSAP<Api>	ConcreteSAP;

	public:
		/** Shut-up GCC. */
		virtual ~Api() {}
		/** */
		virtual void	request(SetResetReq& msg) noexcept=0;
		/** */
		virtual void	request(SetSuspendReq& msg) noexcept=0;
		/** */
		virtual void	request(SetPowerReq& msg) noexcept=0;
		/** */
		virtual void	request(ClearEnableReq& msg) noexcept=0;
		/** */
		virtual void	request(ClearSuspendReq& msg) noexcept=0;
		/** */
		virtual void	request(ClearPowerReq& msg) noexcept=0;

		/** */
		virtual void	request(ClearConnectChangeReq& msg) noexcept=0;
		/** */
		virtual void	request(ClearResetChangeReq& msg) noexcept=0;
		/** */
		virtual void	request(ClearEnableChangeReq& msg) noexcept=0;
		/** */
		virtual void	request(ClearSuspendChangeReq& msg) noexcept=0;
		/** */
		virtual void	request(ClearOverCurrentChangeReq& msg) noexcept=0;

		/** */
		virtual void	request(CancelReq& msg) noexcept=0;

		/** */
		virtual void	request(NotifyPortChangeReq& msg) noexcept=0;
		/** */
		virtual void	request(CancelNotifyPortChangeReq& msg) noexcept=0;

		/** */
		virtual void	request(GetPortStatusReq& msg) noexcept=0;
	};

}
}
}
}
}

#endif
