/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_usb_hub_port_fsm_fsmh_
#define _oscl_drv_usb_hub_port_fsm_fsmh_
#include "oscl/queue/queue.h"
#include "oscl/queue/queueitem.h"

/** */
namespace Oscl {
/** */
namespace Usb {
/** */
namespace Hub {
/** */
namespace Port {

/** */
class StateVar {
	public:
		/** */
		class ContextApi {
			public:	// request actions
				/** Shut-up GCC. */
				virtual ~ContextApi() {}
				/** */
				virtual void	requestSetReset() noexcept=0;
				/** */
				virtual void	requestSetSuspend() noexcept=0;
				/** */
				virtual void	requestSetPower() noexcept=0;
				/** */
				virtual void	requestClrEnable() noexcept=0;
				/** */
				virtual void	requestClrSuspend() noexcept=0;
				/** */
				virtual void	requestClrPower() noexcept=0;
				/** */
				virtual void	requestClrConnectChange() noexcept=0;
				/** */
				virtual void	requestClrResetChange() noexcept=0;
				/** */
				virtual void	requestClrEnableChange() noexcept=0;
				/** */
				virtual void	requestClrSuspendChange() noexcept=0;
				/** */
				virtual void	requestClrOverCurrentChange() noexcept=0;
				/** */
				virtual void	requestCancelReq() noexcept=0;
				/** */
				virtual void	requestNotifyPortChange() noexcept=0;
				/** */
				virtual void	requestCancelNotifyPortChange() noexcept=0;
				/** */
				virtual void	requestGetPortStatus() noexcept=0;
				/** */
				virtual void	requestMsgPipeIrpCancel() noexcept=0;
				/** */
				virtual void	requestMsgPipeNoDataIrp() noexcept=0;
				/** */
				virtual void	requestMsgPipeReadPartIrp() noexcept=0;
				/** */
				virtual void	requestMsgPipeReadFullIrp() noexcept=0;
				/** */
				virtual void	requestMsgPipeWriteIrp() noexcept=0;
				/** */
				virtual void	requestHcdMsgPipeAlloc() noexcept=0;
				/** */
				virtual void	requestHcdMsgPipeFree() noexcept=0;
				/** */
				virtual void	requestNotifyMsgPipeResourceChange() noexcept=0;
				/** */
				virtual void	requestCancelNotifyMsgPipeResChange() noexcept=0;
				/** */
				virtual void	requestHcdLockAddressZero() noexcept=0;
				/** */
				virtual void	requestHcdUnlockAddressZero() noexcept=0;
				/** */
				virtual void	requestHcdCancelLockAddrZero() noexcept=0;
				/** was: requestHcdAllocNewAddr */
				virtual void	requestHcdAllocUsbAddr() noexcept=0;
				/** was: requestHcdFreeAddr */
				virtual void	requestHcdFreeUsbAddr() noexcept=0;
				/** */
				virtual void	requestNotifyUsbAddrResourceChange() noexcept=0;
				/** */
				virtual void	requestCancelNotifyUsbAddrResChange() noexcept=0;
				/** */
				virtual void	prepareToCloseDynSrv() noexcept=0;
				/** */
				virtual void	closeDynSrv() noexcept=0;
				/** */
				virtual void	advertiseNewPipe() noexcept=0;
				/** */
				virtual void	parseChange() noexcept=0;
				/** */
				virtual void	startRetryTimer() noexcept=0;
				/** */
				virtual void	startPowerTimer() noexcept=0;
				/** */
				virtual void	startResetTimer() noexcept=0;
				/** */
				virtual void	cancelTimer() noexcept=0;
				/** */
				virtual bool	deviceConnected() noexcept=0;
				/** */
				virtual bool	overCurrent() noexcept=0;
				/**	returns true if requestHcdAllocUsbAddr successfully
					obtained a new USB address.
				 */
				virtual bool	usbAddrAllocated() noexcept=0;
				/** returns true if the just completed
					requestHcdLockAddressZero indicates that the
					lock request was cancelled before the lock
					was obtained.
				 */
				virtual bool	lockAddressZeroCanceled() noexcept=0;
				/**	returns true if requestHcdMsgPipeAlloc successfully
					allocated a new pipe.
				 */
				virtual bool	pipeAllocated() noexcept=0;
				/** returns true if last Read IRP failed. */
				virtual bool	irpReadError() noexcept=0;
				/** */
				/** returns true if last NoData IRP failed. */
				virtual bool	irpNoDataError() noexcept=0;
			public:
				/** */
				virtual void	returnOpenReq() noexcept=0;
				/** */
				virtual void	returnCloseReq() noexcept=0;
			};
	private:
		/** */
		class State {
			public:
				/** Shut-up GCC. */
				virtual ~State() {}

			public:	// Admin Events
				/** */
				virtual void	open(	ContextApi&	context,
										StateVar&	sv
										) const noexcept;
				/** */
				virtual void	close(	ContextApi&	context,
										StateVar&	sv
										) const noexcept;
			public:	// Hub port events
				/** */
				virtual void	setResetDone(	ContextApi&	context,
												StateVar&	sv
												) const noexcept;
				/** */
				virtual void	setSuspendDone(	ContextApi&	context,
												StateVar&	sv
												) const noexcept;
				/** */
				virtual void	setPowerDone(	ContextApi&	context,
												StateVar&	sv
												) const noexcept;
				/** */
				virtual void	clrEnableDone(	ContextApi&	context,
												StateVar&	sv
												) const noexcept;
				/** */
				virtual void	clrSuspendDone(	ContextApi&	context,
												StateVar&	sv
												) const noexcept;
				/** */
				virtual void	clrPowerDone(	ContextApi&	context,
												StateVar&	sv
												) const noexcept;
				/** */
				virtual void	clrConnectChangeDone(	ContextApi&	context,
														StateVar&	sv
														) const noexcept;
				/** */
				virtual void	clrResetChangeDone(	ContextApi&	context,
													StateVar&	sv
													) const noexcept;
				/** */
				virtual void	clrEnableChangeDone(	ContextApi&	context,
														StateVar&	sv
														) const noexcept;
				/** */
				virtual void	clrSuspendChangeDone(	ContextApi&	context,
														StateVar&	sv
														) const noexcept;
				/** */
				virtual void	clrOverCurrentChangeDone(	ContextApi&	context,
															StateVar&	sv
															) const noexcept;
				/** */
				virtual void	cancelReqDone(	ContextApi&	context,
												StateVar&	sv
												) const noexcept;
				/** */
				virtual void	notifyPortChangeDone(	ContextApi&	context,
														StateVar&	sv
														) const noexcept;
				/** */
				virtual void	cancelNotifyPortChangeDone(	ContextApi&	context,
															StateVar&	sv
															) const noexcept;
				/** */
				virtual void	getPortStatusDone(	ContextApi&	context,
													StateVar&	sv
													) const noexcept;
				/** */
				virtual void	requestFailed(	ContextApi&	context,
												StateVar&	sv
												) const noexcept;

			public:	// Message Pipe Events
				/** */
				virtual void	msgPipeIrpCancelDone(	ContextApi&	context,
														StateVar&	sv
														) const noexcept;
				/** */
				virtual void	msgPipeNoDataIrpDone(	ContextApi&	context,
														StateVar&	sv
														) const noexcept;
				/** */
				virtual void	msgPipeReadIrpDone(	ContextApi&	context,
													StateVar&	sv
													) const noexcept;
				/** */
				virtual void	msgPipeWriteIrpDone(	ContextApi&	context,
														StateVar&	sv
														) const noexcept;
			public:	// HCD Message Pipe Allocation Events
				/** */
				virtual void	hcdMsgPipeAllocDone(	ContextApi&	context,
														StateVar&	sv
														) const noexcept;
				/** */
				virtual void	hcdMsgPipeFreeDone(	ContextApi&	context,
													StateVar&	sv
													) const noexcept;
				/** */
				virtual void	notifyMsgPipeResourceChangeDone(
															ContextApi&	context,
															StateVar&	sv
															) const noexcept;
				/** */
				virtual void	cancelNotifyMsgPipeResourceChangeDone(
															ContextApi&	context,
															StateVar&	sv
															) const noexcept;
			public:	// HCD Hub Control
				/** */
				virtual void	hcdLockAddressZeroDone(	ContextApi&	context,
														StateVar&	sv
														) const noexcept;
				/** */
				virtual void	hcdUnlockAddressZeroDone(	ContextApi&	context,
															StateVar&	sv
															) const noexcept;
				/** */
				virtual void	hcdCancelLockAddrZeroDone(	ContextApi&	context,
															StateVar&	sv
															) const noexcept;
				/** was : hcdAllocNewAddrDone */
				virtual void	hcdAllocUsbAddrDone(	ContextApi&	context,
														StateVar&	sv
														) const noexcept;
				/** was : hcdFreeAddrDone */
				virtual void	hcdFreeUsbAddrDone(	ContextApi&	context,
													StateVar&	sv
													) const noexcept;
				/** */
				virtual void	prepareToCloseDynSrvDone(	ContextApi&	context,
															StateVar&	sv
															) const noexcept;
				/** */
				virtual void	closeDynSrvDone(	ContextApi&	context,
													StateVar&	sv
													) const noexcept;
				/** */
				virtual void	notifyUsbAddrResChangeDone(
															ContextApi&	context,
															StateVar&	sv
															) const noexcept;
				/** */
				virtual void	cancelNotifyUsbAddrResChangeDone(
															ContextApi&	context,
															StateVar&	sv
															) const noexcept;
			public:
				/** */
				virtual void	timerExpired(	ContextApi&	context,
												StateVar&	sv
												) const noexcept;
				/** */
				virtual void	timerCanceled(	ContextApi&	context,
												StateVar&	sv
												) const noexcept;
			public:
				/** */
				virtual void	connectChanged(	ContextApi&	context,
												StateVar&	sv
												) const noexcept;
				/** */
				virtual void	portDisabled(	ContextApi&	context,
												StateVar&	sv
												) const noexcept;
				/** */
				virtual void	portResumed(	ContextApi&	context,
												StateVar&	sv
												) const noexcept;
				/** */
				virtual void	portOverCurrentChange(	ContextApi&	context,
														StateVar&	sv
														) const noexcept;
				/** */
				virtual void	portResetDone(	ContextApi&	context,
												StateVar&	sv
												) const noexcept;
			public:
				/** */
				virtual const char*	stateName() const noexcept=0;
			};

	public:
		/** */
		class Closed : public State {
			private:
				/** */
				const char*	stateName() const noexcept
					{ return "Closed";}
			private:
				/** */
				void	open(	ContextApi&	context,
								StateVar&	sv
								) const noexcept;
			};
		/** */
		class Closing : public State {
			private:
				/** */
				const char*	stateName() const noexcept
					{ return "Closing";}
			private:
				/** */
				void	close(	ContextApi&	context,
								StateVar&	sv
								) const noexcept;
			};
		/** */
		class ClosingPccPc : public State {
			private:
				/** */
				const char*	stateName() const noexcept
					{ return "ClosingPccPc";}
			private:
				/** */
				void	setResetDone(	ContextApi&	context,
										StateVar&	sv
										) const noexcept;
				/** */
				void	setSuspendDone(	ContextApi&	context,
										StateVar&	sv
										) const noexcept;
				/** */
				void	setPowerDone(	ContextApi&	context,
										StateVar&	sv
										) const noexcept;
				/** */
				void	clrEnableDone(	ContextApi&	context,
										StateVar&	sv
										) const noexcept;
				/** */
				void	clrSuspendDone(	ContextApi&	context,
										StateVar&	sv
										) const noexcept;
				/** */
				void	clrPowerDone(	ContextApi&	context,
										StateVar&	sv
										) const noexcept;
				/** */
				void	clrConnectChangeDone(	ContextApi&	context,
												StateVar&	sv
												) const noexcept;
				/** */
				void	clrResetChangeDone(	ContextApi&	context,
											StateVar&	sv
											) const noexcept;
				/** */
				void	clrEnableChangeDone(	ContextApi&	context,
												StateVar&	sv
												) const noexcept;
				/** */
				void	clrSuspendChangeDone(	ContextApi&	context,
												StateVar&	sv
												) const noexcept;
				/** */
				void	clrOverCurrentChangeDone(	ContextApi&	context,
													StateVar&	sv
													) const noexcept;
				/** */
				void	cancelReqDone(	ContextApi&	context,
										StateVar&	sv
										) const noexcept;
				/** */
				void	getPortStatusDone(	ContextApi&	context,
											StateVar&	sv
											) const noexcept;
				/** */
				void	requestFailed(	ContextApi&	context,
										StateVar&	sv
										) const noexcept;
				/** */
				void	timerExpired(	ContextApi&	context,
										StateVar&	sv
										) const noexcept;
				/** */
				void	timerCanceled(	ContextApi&	context,
										StateVar&	sv
										) const noexcept;
			};
		/** */
		class ClosingPncPn : public State {
			private:
				/** */
				const char*	stateName() const noexcept
					{ return "ClosingPncPn";}
			private:
				/** */
				void	notifyPortChangeDone(	ContextApi&	context,
												StateVar&	sv
												) const noexcept;
				/** */
				void	cancelNotifyPortChangeDone(	ContextApi&	context,
													StateVar&	sv
													) const noexcept;
			};
		/** */
		class ClosingAllocUsbAddrHc : public State {
			private:
				/** */
				const char*	stateName() const noexcept
					{ return "ClosingAllocUsbAddrHc";}
			private:
				/** */
				void	hcdAllocUsbAddrDone(	ContextApi&	context,
												StateVar&	sv
												) const noexcept;
			};
		/** */
		class ClosingFreeUsbAddrHc : public State {
			private:
				/** */
				const char*	stateName() const noexcept
					{ return "ClosingFreeUsbAddrHc";}
			private:
				/** */
				void	hcdFreeUsbAddrDone(	ContextApi&	context,
											StateVar&	sv
											) const noexcept;
			};
		/** */
		class ClosingHczcHczUa : public State {
			private:
				/** */
				const char*	stateName() const noexcept
					{ return "ClosingHczcHczUa";}
			private:
				/** */
				void	hcdLockAddressZeroDone(	ContextApi&	context,
												StateVar&	sv
												) const noexcept;
			};
		/** */
		class ClosingHczcUa : public State {
			private:
				/** */
				const char*	stateName() const noexcept
					{ return "ClosingHczcUa";}
			private:
				/** */
				void	hcdCancelLockAddrZeroDone(	ContextApi&	context,
													StateVar&	sv
													) const noexcept;
			};
		/** */
		class ClosingHczcAzUa : public State {
			private:
				/** */
				const char*	stateName() const noexcept
					{ return "ClosingHczcAzUa";}
			private:
				/** */
				void	hcdCancelLockAddrZeroDone(	ContextApi&	context,
													StateVar&	sv
													) const noexcept;
			};
		/** */
		class ClosingUnlockAddrZeroHcUa : public State {
			private:
				/** */
				const char*	stateName() const noexcept
					{ return "ClosingUnlockAddrZeroHcUa";}
			private:
				/** */
				void	hcdUnlockAddressZeroDone(	ContextApi&	context,
													StateVar&	sv
													) const noexcept;
			};
		/** */
		class ClosingSetResetPccPcAzUa : public State {
			private:
				/** */
				const char*	stateName() const noexcept
					{ return "ClosingSetResetPccPcAzUa";}
			private:
				/** */
				void	setResetDone(	ContextApi&	context,
										StateVar&	sv
										) const noexcept;
				/** */
				void	cancelReqDone(	ContextApi&	context,
										StateVar&	sv
										) const noexcept;
				/** */
				void	requestFailed(	ContextApi&	context,
										StateVar&	sv
										) const noexcept;
			};
		/** */
		class ClosingTimerCancelPncPnAzUa : public State {
			private:
				/** */
				const char*	stateName() const noexcept
					{ return "ClosingTimerCancelPncPnAzUa";}
			private:
				/** */
				void	notifyPortChangeDone(	ContextApi&	context,
												StateVar&	sv
												) const noexcept;
				/** */
				void	cancelNotifyPortChangeDone(	ContextApi&	context,
													StateVar&	sv
													) const noexcept;
				/** */
				void	timerExpired(	ContextApi&	context,
										StateVar&	sv
										) const noexcept;
				/** */
				void	timerCanceled(	ContextApi&	context,
										StateVar&	sv
										) const noexcept;
			};
		/** */
		class ClosingTimerCancelPncPnPaUa : public State {
			private:
				/** */
				const char*	stateName() const noexcept
					{ return "ClosingTimerCancelPncPnPaUa";}
			private:
				/** */
				void	notifyPortChangeDone(	ContextApi&	context,
												StateVar&	sv
												) const noexcept;
				/** */
				void	cancelNotifyPortChangeDone(	ContextApi&	context,
													StateVar&	sv
													) const noexcept;
				/** */
				void	timerExpired(	ContextApi&	context,
										StateVar&	sv
										) const noexcept;
				/** */
				void	timerCanceled(	ContextApi&	context,
										StateVar&	sv
										) const noexcept;
			};
		/** */
		class ClosingTimerCancelPaUa : public State {
			private:
				/** */
				const char*	stateName() const noexcept
					{ return "ClosingTimerCancelPncPnPaUa";}
			private:
				/** */
				void	timerExpired(	ContextApi&	context,
										StateVar&	sv
										) const noexcept;
				/** */
				void	timerCanceled(	ContextApi&	context,
										StateVar&	sv
										) const noexcept;
			};
		/** */
		class ClosingTimerCancelAzUa : public State {
			private:
				/** */
				const char*	stateName() const noexcept
					{ return "ClosingTimerCancelAzUa";}
			private:
				/** */
				void	timerExpired(	ContextApi&	context,
										StateVar&	sv
										) const noexcept;
				/** */
				void	timerCanceled(	ContextApi&	context,
										StateVar&	sv
										) const noexcept;
			};
		/** */
		class ClosingPncPnAzUa : public State {
			private:
				/** */
				const char*	stateName() const noexcept
					{ return "ClosingPncPnAzUa";}
			private:
				/** */
				void	notifyPortChangeDone(	ContextApi&	context,
												StateVar&	sv
												) const noexcept;
				/** */
				void	cancelNotifyPortChangeDone(	ContextApi&	context,
													StateVar&	sv
													) const noexcept;
			};
		/** */
		class ClosingClrResetChangePccPcAzUa : public State {
			private:
				/** */
				const char*	stateName() const noexcept
					{ return "ClosingClrResetChangePccPcAzUa";}
			private:
				/** */
				void	clrResetChangeDone(	ContextApi&	context,
											StateVar&	sv
											) const noexcept;
				/** */
				void	cancelReqDone(	ContextApi&	context,
										StateVar&	sv
										) const noexcept;
				/** */
				void	requestFailed(	ContextApi&	context,
										StateVar&	sv
										) const noexcept;
			};
		/** */
		class ClosingResetWaitTwcTwAzUa : public State {
			private:
				/** */
				const char*	stateName() const noexcept
					{ return "ClosingResetWaitTwcTwAzUa";}
			private:
				/** */
				void	timerExpired(	ContextApi&	context,
										StateVar&	sv
										) const noexcept;
				/** */
				void	timerCanceled(	ContextApi&	context,
										StateVar&	sv
										) const noexcept;
			};
		/** */
		class ClosingUncUnPncPn : public State {
			private:
				/** */
				const char*	stateName() const noexcept
					{ return "ClosingUncUnPncPn";}
			private:
				/** */
				void	notifyPortChangeDone(	ContextApi&	context,
												StateVar&	sv
												) const noexcept;
				/** */
				void	cancelNotifyPortChangeDone(	ContextApi&	context,
													StateVar&	sv
													) const noexcept;
				/** */
				void	notifyUsbAddrResChangeDone(	ContextApi&	context,
													StateVar&	sv
													) const noexcept;
				/** */
				void	cancelNotifyUsbAddrResChangeDone(	ContextApi&	context,
															StateVar&	sv
															) const noexcept;
			};
		/** */
		class ClosingUncUn : public State {
			private:
				/** */
				const char*	stateName() const noexcept
					{ return "ClosingUncUn";}
			private:
				/** */
				void	notifyUsbAddrResChangeDone(	ContextApi&	context,
													StateVar&	sv
													) const noexcept;
				/** */
				void	cancelNotifyUsbAddrResChangeDone(	ContextApi&	context,
															StateVar&	sv
															) const noexcept;
			};
		/** */
		class ClosingReadDescIrpPncPnAzUa : public State {
			private:
				/** */
				const char*	stateName() const noexcept
					{ return "ClosingReadDescIrpPncPnAzUa";}
			private:
				/** */
				void	notifyPortChangeDone(	ContextApi&	context,
												StateVar&	sv
												) const noexcept;
				/** */
				void	cancelNotifyPortChangeDone(	ContextApi&	context,
													StateVar&	sv
													) const noexcept;
				/** */
				void	msgPipeReadIrpDone(	ContextApi&	context,
											StateVar&	sv
											) const noexcept;
			};
		/** */
		class ClosingReadDescIrpcIrpAzUa : public State {
			private:
				/** */
				const char*	stateName() const noexcept
					{ return "ClosingReadDescIrpcIrpAzUa";}
			private:
				/** */
				void	msgPipeIrpCancelDone(	ContextApi&	context,
												StateVar&	sv
												) const noexcept;
				/** */
				void	msgPipeReadIrpDone(	ContextApi&	context,
											StateVar&	sv
											) const noexcept;
			};
		/** */
		class ClosingSetAddrIrpPncPnAzUa : public State {
			private:
				/** */
				const char*	stateName() const noexcept
					{ return "ClosingSetAddrIrpPncPnAzUa";}
			private:
				/** */
				void	notifyPortChangeDone(	ContextApi&	context,
												StateVar&	sv
												) const noexcept;
				/** */
				void	cancelNotifyPortChangeDone(	ContextApi&	context,
													StateVar&	sv
													) const noexcept;
				/** */
				void	msgPipeNoDataIrpDone(	ContextApi&	context,
												StateVar&	sv
												) const noexcept;
			};
		/** */
		class ClosingSetAddrIrpcIrpAzUa : public State {
			private:
				/** */
				const char*	stateName() const noexcept
					{ return "ClosingSetAddrIrpcIrpAzUa";}
			private:
				/** */
				void	msgPipeIrpCancelDone(	ContextApi&	context,
												StateVar&	sv
												) const noexcept;
				/** */
				void	msgPipeNoDataIrpDone(	ContextApi&	context,
												StateVar&	sv
												) const noexcept;
			};
		/** */
		class ClosingUazPncPnUa : public State {
			private:
				/** */
				const char*	stateName() const noexcept
					{ return "ClosingUazPncPnUa";}
			private:
				/** */
				void	notifyPortChangeDone(	ContextApi&	context,
												StateVar&	sv
												) const noexcept;
				/** */
				void	cancelNotifyPortChangeDone(	ContextApi&	context,
													StateVar&	sv
													) const noexcept;
				/** */
				void	hcdUnlockAddressZeroDone(	ContextApi&	context,
													StateVar&	sv
													) const noexcept;
			};
		/** */
		class ClosingUazUa : public State {
			private:
				/** */
				const char*	stateName() const noexcept
					{ return "ClosingUazUa";}
			private:
				/** */
				void	hcdUnlockAddressZeroDone(	ContextApi&	context,
													StateVar&	sv
													) const noexcept;
			};
		/** */
		class ClosingPncPnUa : public State {
			private:
				/** */
				const char*	stateName() const noexcept
					{ return "ClosingPncPnUa";}
			private:
				/** */
				void	notifyPortChangeDone(	ContextApi&	context,
												StateVar&	sv
												) const noexcept;
				/** */
				void	cancelNotifyPortChangeDone(	ContextApi&	context,
													StateVar&	sv
													) const noexcept;
			};
		/** */
		class ClosingPipeAllocHcPncPnUa : public State {
			private:
				/** */
				const char*	stateName() const noexcept
					{ return "ClosingPipeAllocHcPncPnUa";}
			private:
				/** */
				void	notifyPortChangeDone(	ContextApi&	context,
												StateVar&	sv
												) const noexcept;
				/** */
				void	cancelNotifyPortChangeDone(	ContextApi&	context,
													StateVar&	sv
													) const noexcept;
				/** */
				void	hcdMsgPipeAllocDone(	ContextApi&	context,
														StateVar&	sv
														) const noexcept;
			};
		/** */
		class ClosingPipeAllocHcUa : public State {
			private:
				/** */
				const char*	stateName() const noexcept
					{ return "ClosingPipeAllocHcUa";}
			private:
				/** */
				void	hcdMsgPipeAllocDone(	ContextApi&	context,
														StateVar&	sv
														) const noexcept;
			};
		/** */
		class ClosingFreePipeHcUa : public State {
			private:
				/** */
				const char*	stateName() const noexcept
					{ return "ClosingFreePipeHcUa";}
			private:
				/** */
				void	hcdMsgPipeFreeDone(	ContextApi&	context,
											StateVar&	sv
											) const noexcept;
			};
		/** */
		class ClosingFreePipeHcPncPnUa : public State {
			private:
				/** */
				const char*	stateName() const noexcept
					{ return "ClosingFreePipeHcPncPnUa";}
			private:
				/** */
				void	notifyPortChangeDone(	ContextApi&	context,
												StateVar&	sv
												) const noexcept;
				/** */
				void	cancelNotifyPortChangeDone(	ContextApi&	context,
													StateVar&	sv
													) const noexcept;
				/** */
				void	hcdMsgPipeFreeDone(	ContextApi&	context,
											StateVar&	sv
											) const noexcept;
			};
		/** */
		class ClosingAdvPncPnPaUa : public State {
			private:
				/** */
				const char*	stateName() const noexcept
					{ return "ClosingAdvPncPnPaUa";}
			private:
				/** */
				void	notifyPortChangeDone(	ContextApi&	context,
												StateVar&	sv
												) const noexcept;
				/** */
				void	cancelNotifyPortChangeDone(	ContextApi&	context,
													StateVar&	sv
													) const noexcept;
			};
		/** */
		class ClosingPrepareHcPaUa : public State {
			private:
				/** */
				const char*	stateName() const noexcept
					{ return "ClosingPrepareHcPaUa";}
			private:
				/** */
				void	prepareToCloseDynSrvDone(	ContextApi&	context,
													StateVar&	sv
													) const noexcept;
			};
		/** */
		class ClosingDynSrvHcPaUa : public State {
			private:
				/** */
				const char*	stateName() const noexcept
					{ return "ClosingDynSrvHcPaUa";}
			private:
				/** */
				void	closeDynSrvDone(	ContextApi&	context,
											StateVar&	sv
											) const noexcept;
			};
		/** */
		class ClosingRncRnUa : public State {
			private:
				/** */
				const char*	stateName() const noexcept
					{ return "ClosingRncRnUa";}
			private:
				/** */
				void	notifyMsgPipeResourceChangeDone(	ContextApi&	context,
															StateVar&	sv
															) const noexcept;
				/** */
				void	cancelNotifyMsgPipeResourceChangeDone(
														ContextApi&	context,
														StateVar&	sv
														) const noexcept;
			};
		/** */
		class ClosingRncRnPncPnUa : public State {
			private:
				/** */
				const char*	stateName() const noexcept
					{ return "ClosingRncRnPncPnUa";}
			private:
				/** */
				void	notifyPortChangeDone(	ContextApi&	context,
												StateVar&	sv
												) const noexcept;
				/** */
				void	cancelNotifyPortChangeDone(	ContextApi&	context,
													StateVar&	sv
													) const noexcept;
				/** */
				void	notifyMsgPipeResourceChangeDone(	ContextApi&	context,
															StateVar&	sv
															) const noexcept;
				/** */
				void	cancelNotifyMsgPipeResourceChangeDone(
														ContextApi&	context,
														StateVar&	sv
														) const noexcept;
			};
		/** */
		class ClosingReadFullIrpPncPnPaUa : public State {
			private:
				/** */
				const char*	stateName() const noexcept
					{ return "ClosingReadFullIrpPncPnPaUa";}
			private:
				/** */
				void	notifyPortChangeDone(	ContextApi&	context,
												StateVar&	sv
												) const noexcept;
				/** */
				void	cancelNotifyPortChangeDone(	ContextApi&	context,
													StateVar&	sv
													) const noexcept;
				/** */
				void	msgPipeReadIrpDone(	ContextApi&	context,
											StateVar&	sv
											) const noexcept;
			};
		/** */
		class ClosingReadFullIrpcIrpPaUa : public State {
			private:
				/** */
				const char*	stateName() const noexcept
					{ return "ClosingReadFullIrpcIrpPaUa";}
			private:
				/** */
				void	msgPipeIrpCancelDone(	ContextApi&	context,
												StateVar&	sv
												) const noexcept;
				/** */
				void	msgPipeReadIrpDone(	ContextApi&	context,
											StateVar&	sv
											) const noexcept;
			};
		/** */
		class ClosingPncPnPaUa : public State {
			private:
				/** */
				const char*	stateName() const noexcept
					{ return "ClosingPncPnPaUa";}
			private:
				/** */
				void	notifyPortChangeDone(	ContextApi&	context,
												StateVar&	sv
												) const noexcept;
				/** */
				void	cancelNotifyPortChangeDone(	ContextApi&	context,
													StateVar&	sv
													) const noexcept;
			};
		/** */
		class PowerWait : public State {
			private:
				/** */
				const char*	stateName() const noexcept
					{ return "PowerWait";}
			private:
				/** */
				void	close(	ContextApi&	context,
								StateVar&	sv
								) const noexcept;
				/** */
				void	timerExpired(	ContextApi&	context,
										StateVar&	sv
										) const noexcept;
			};
		/** */
		class DisabledPc : public State {
			private:
				/** */
				const char*	stateName() const noexcept
					{ return "DisabledPc";}
			private:
				/** */
				void	close(	ContextApi&	context,
								StateVar&	sv
								) const noexcept;
				/** */
				void	setPowerDone(	ContextApi&	context,
										StateVar&	sv
										) const noexcept;
				/** */
				void	requestFailed(	ContextApi&	context,
										StateVar&	sv
										) const noexcept;
			};
		/** */
		class DisabledPn : public State {
			private:
				/** */
				const char*	stateName() const noexcept
					{ return "DisabledPn";}
			private:
				/** */
				void	close(	ContextApi&	context,
								StateVar&	sv
								) const noexcept;
				/** */
				void	notifyPortChangeDone(	ContextApi&	context,
												StateVar&	sv
												) const noexcept;
				/** */
				void	connectChanged(	ContextApi&	context,
										StateVar&	sv
										) const noexcept;
				/** */
				void	portDisabled(	ContextApi&	context,
										StateVar&	sv
										) const noexcept;
				/** */
				void	portResetDone(	ContextApi&	context,
										StateVar&	sv
										) const noexcept;
				/** */
				void	portOverCurrentChange(	ContextApi&	context,
												StateVar&	sv
												) const noexcept;
			};
		/** */
		class DisabledClrEnableChangePc : public State {
			private:
				/** */
				const char*	stateName() const noexcept
					{ return "DisabledClrEnableChangePc";}
			private:
				/** */
				void	close(	ContextApi&	context,
								StateVar&	sv
								) const noexcept;
				/** */
				void	clrEnableChangeDone(	ContextApi&	context,
												StateVar&	sv
												) const noexcept;
				/** */
				void	requestFailed(	ContextApi&	context,
										StateVar&	sv
										) const noexcept;
			};
		/** */
		class DisabledClrResetChangePc : public State {
			private:
				/** */
				const char*	stateName() const noexcept
					{ return "DisabledClrResetChangePc";}
			private:
				/** */
				void	close(	ContextApi&	context,
								StateVar&	sv
								) const noexcept;
				/** */
				void	clrResetChangeDone(	ContextApi&	context,
											StateVar&	sv
											) const noexcept;
				/** */
				void	requestFailed(	ContextApi&	context,
										StateVar&	sv
										) const noexcept;
			};
		/** */
		class DisabledClrConnChangePc : public State {
			private:
				/** */
				const char*	stateName() const noexcept
					{ return "DisabledClrConnChangePc";}
			private:
				/** */
				void	close(	ContextApi&	context,
								StateVar&	sv
								) const noexcept;
				/** */
				void	clrConnectChangeDone(	ContextApi&	context,
												StateVar&	sv
												) const noexcept;
				/** */
				void	requestFailed(	ContextApi&	context,
										StateVar&	sv
										) const noexcept;
			};
		/** */
		class DisabledGetStatusPc : public State {
			private:
				/** */
				const char*	stateName() const noexcept
					{ return "DisabledGetStatusPc";}
			private:
				/** */
				void	close(	ContextApi&	context,
								StateVar&	sv
								) const noexcept;
				/** */
				void	notifyPortChangeDone(	ContextApi&	context,
												StateVar&	sv
												) const noexcept;
				/** */
				void	getPortStatusDone(	ContextApi&	context,
											StateVar&	sv
											) const noexcept;
				/** */
				void	requestFailed(	ContextApi&	context,
										StateVar&	sv
										) const noexcept;
			};
		/** */
		class ConnAllocUsbAddrHc : public State {
			private:
				/** */
				const char*	stateName() const noexcept
					{ return "ConnAllocUsbAddrHc";}
			private:
				/** */
				void	close(	ContextApi&	context,
								StateVar&	sv
								) const noexcept;
				/** */
				void	hcdAllocUsbAddrDone(	ContextApi&	context,
												StateVar&	sv
												) const noexcept;
			};
		/** */
		class ConnLockAddrZeroUa : public State {
			private:
				/** */
				const char*	stateName() const noexcept
					{ return "ConnLockAddrZeroUa";}
			private:
				/** */
				void	close(	ContextApi&	context,
								StateVar&	sv
								) const noexcept;
				/** */
				void	hcdLockAddressZeroDone(	ContextApi&	context,
												StateVar&	sv
												) const noexcept;
			};
		/** */
		class ConnSetResetPcAzUa : public State {
			private:
				/** */
				const char*	stateName() const noexcept
					{ return "ConnSetResetPcAzUa";}
			private:
				/** */
				void	close(	ContextApi&	context,
								StateVar&	sv
								) const noexcept;
				/** */
				void	setResetDone(	ContextApi&	context,
										StateVar&	sv
										) const noexcept;
				/** */
				void	requestFailed(	ContextApi&	context,
										StateVar&	sv
										) const noexcept;
			};
		/** */
		class ConnWaitResetPnAzUa : public State {
			private:
				/** */
				const char*	stateName() const noexcept
					{ return "ConnWaitResetPnAzUa";}
			private:
				/** */
				void	close(	ContextApi&	context,
								StateVar&	sv
								) const noexcept;
				/** */
				void	notifyPortChangeDone(	ContextApi&	context,
												StateVar&	sv
												) const noexcept;
				/** */
				void	connectChanged(	ContextApi&	context,
										StateVar&	sv
										) const noexcept;
				/** */
				void	portDisabled(	ContextApi&	context,
										StateVar&	sv
										) const noexcept;
				/** */
				void	portResetDone(	ContextApi&	context,
										StateVar&	sv
										) const noexcept;
			};
		/** */
		class ConnClrResetChangePcAzUa : public State {
			private:
				/** */
				const char*	stateName() const noexcept
					{ return "ConnClrResetChangePcAzUa";}
			private:
				/** */
				void	close(	ContextApi&	context,
								StateVar&	sv
								) const noexcept;
				/** */
				void	clrResetChangeDone(	ContextApi&	context,
											StateVar&	sv
											) const noexcept;
				/** */
				void	requestFailed(	ContextApi&	context,
										StateVar&	sv
										) const noexcept;
			};
		/** */
		class ConnResetWaitTwAzUa : public State {
			private:
				/** */
				const char*	stateName() const noexcept
					{ return "ConnResetWaitTwAzUa";}
			private:
				/** */
				void	close(	ContextApi&	context,
								StateVar&	sv
								) const noexcept;
				/** */
				void	timerExpired(	ContextApi&	context,
										StateVar&	sv
										) const noexcept;
			};
		/** */
		class ConnWaitUsbAddrUnPn : public State {
			private:
				/** */
				const char*	stateName() const noexcept
					{ return "ConnWaitUsbAddrUnPn";}
			private:
				/** */
				void	close(	ContextApi&	context,
								StateVar&	sv
								) const noexcept;
				/** */
				void	notifyPortChangeDone(	ContextApi&	context,
												StateVar&	sv
												) const noexcept;
				/** */
				void	notifyUsbAddrResChangeDone(	ContextApi&	context,
													StateVar&	sv
													) const noexcept;
				/** */
				void	connectChanged(	ContextApi&	context,
										StateVar&	sv
										) const noexcept;
				/** */
				void	portDisabled(	ContextApi&	context,
										StateVar&	sv
										) const noexcept;
			};
		/** */
		class ConnReadDescIrpPnAzUa : public State {
			private:
				/** */
				const char*	stateName() const noexcept
					{ return "ConnReadDescIrpPnAzUa";}
			private:
				/** */
				void	close(	ContextApi&	context,
								StateVar&	sv
								) const noexcept;
				/** */
				void	notifyPortChangeDone(	ContextApi&	context,
												StateVar&	sv
												) const noexcept;
				/** */
				void	msgPipeReadIrpDone(	ContextApi&	context,
											StateVar&	sv
											) const noexcept;
				/** */
				void	connectChanged(	ContextApi&	context,
										StateVar&	sv
										) const noexcept;
				/** */
				void	portDisabled(	ContextApi&	context,
										StateVar&	sv
										) const noexcept;
				/** */
				void	portResumed(	ContextApi&	context,
										StateVar&	sv
										) const noexcept;
			};
		/** */
		class ConnReadDescErrorPnAzUa : public State {
			private:
				/** */
				const char*	stateName() const noexcept
					{ return "ConnReadDescErrorPnAzUa";}
			private:
				/** */
				void	close(	ContextApi&	context,
								StateVar&	sv
								) const noexcept;
				/** */
				void	notifyPortChangeDone(	ContextApi&	context,
												StateVar&	sv
												) const noexcept;
				/** */
				void	connectChanged(	ContextApi&	context,
										StateVar&	sv
										) const noexcept;
				/** */
				void	portDisabled(	ContextApi&	context,
										StateVar&	sv
										) const noexcept;
				/** */
				void	portResumed(	ContextApi&	context,
										StateVar&	sv
										) const noexcept;
				/** */
				void	timerExpired(	ContextApi&	context,
										StateVar&	sv
										) const noexcept;
			};
		/** */
		class ConnSetAddrIrpPnAzUa : public State {
			private:
				/** */
				const char*	stateName() const noexcept
					{ return "ConnSetAddrIrpPnAzUa";}
			private:
				/** */
				void	close(	ContextApi&	context,
								StateVar&	sv
								) const noexcept;
				/** */
				void	notifyPortChangeDone(	ContextApi&	context,
												StateVar&	sv
												) const noexcept;
				/** */
				void	msgPipeNoDataIrpDone(	ContextApi&	context,
												StateVar&	sv
												) const noexcept;
				/** */
				void	connectChanged(	ContextApi&	context,
										StateVar&	sv
										) const noexcept;
				/** */
				void	portDisabled(	ContextApi&	context,
										StateVar&	sv
										) const noexcept;
			};
		/** */
		class ConnSetAddrErrorPnAzUa : public State {
			private:
				/** */
				const char*	stateName() const noexcept
					{ return "ConnSetAddrErrorPnAzUa";}
			private:
				/** */
				void	close(	ContextApi&	context,
								StateVar&	sv
								) const noexcept;
				/** */
				void	notifyPortChangeDone(	ContextApi&	context,
												StateVar&	sv
												) const noexcept;
				/** */
				void	connectChanged(	ContextApi&	context,
										StateVar&	sv
										) const noexcept;
				/** */
				void	portDisabled(	ContextApi&	context,
										StateVar&	sv
										) const noexcept;
				/** */
				void	timerExpired(	ContextApi&	context,
										StateVar&	sv
										) const noexcept;
			};
		/** */
		class ConnUnlockAddrZeroHcPnUa : public State {
			private:
				/** */
				const char*	stateName() const noexcept
					{ return "ConnUnlockAddrZeroHcPnUa";}
			private:
				/** */
				void	close(	ContextApi&	context,
								StateVar&	sv
								) const noexcept;
				/** */
				void	notifyPortChangeDone(	ContextApi&	context,
												StateVar&	sv
												) const noexcept;
				/** */
				void	hcdUnlockAddressZeroDone(	ContextApi&	context,
													StateVar&	sv
													) const noexcept;
				/** */
				void	connectChanged(	ContextApi&	context,
										StateVar&	sv
										) const noexcept;
				/** */
				void	portDisabled(	ContextApi&	context,
										StateVar&	sv
										) const noexcept;
			};
		/** */
		class ConnPipeAllocHcPnUa : public State {
			private:
				/** */
				const char*	stateName() const noexcept
					{ return "ConnPipeAllocHcPnUa";}
			private:
				/** */
				void	close(	ContextApi&	context,
								StateVar&	sv
								) const noexcept;
				/** */
				void	notifyPortChangeDone(	ContextApi&	context,
												StateVar&	sv
												) const noexcept;
				/** */
				void	hcdMsgPipeAllocDone(	ContextApi&	context,
												StateVar&	sv
												) const noexcept;
				/** */
				void	connectChanged(	ContextApi&	context,
										StateVar&	sv
										) const noexcept;
				/** */
				void	portDisabled(	ContextApi&	context,
										StateVar&	sv
										) const noexcept;
			};
		/** */
		class ConnReadFullIrpPnPaUa : public State {
			private:
				/** */
				const char*	stateName() const noexcept
					{ return "ConnReadFullIrpPnPaUa";}
			private:
				/** */
				void	close(	ContextApi&	context,
								StateVar&	sv
								) const noexcept;
				/** */
				void	notifyPortChangeDone(	ContextApi&	context,
												StateVar&	sv
												) const noexcept;
				/** */
				void	msgPipeReadIrpDone(	ContextApi&	context,
											StateVar&	sv
											) const noexcept;
				/** */
				void	connectChanged(	ContextApi&	context,
										StateVar&	sv
										) const noexcept;
				/** */
				void	portDisabled(	ContextApi&	context,
										StateVar&	sv
										) const noexcept;
				/** */
				void	portResumed(	ContextApi&	context,
										StateVar&	sv
										) const noexcept;
			};
		/** */
		class ConnReadFullErrorPnPaUa : public State {
			private:
				/** */
				const char*	stateName() const noexcept
					{ return "ConnReadFullErrorPnPaUa";}
			private:
				/** */
				void	close(	ContextApi&	context,
								StateVar&	sv
								) const noexcept;
				/** */
				void	notifyPortChangeDone(	ContextApi&	context,
												StateVar&	sv
												) const noexcept;
				/** */
				void	connectChanged(	ContextApi&	context,
										StateVar&	sv
										) const noexcept;
				/** */
				void	portDisabled(	ContextApi&	context,
										StateVar&	sv
										) const noexcept;
				/** */
				void	portResumed(	ContextApi&	context,
										StateVar&	sv
										) const noexcept;
				/** */
				void	timerExpired(	ContextApi&	context,
										StateVar&	sv
										) const noexcept;
			};
		/** */
		class ReadyPnPaUa : public State {
			private:
				/** */
				const char*	stateName() const noexcept
					{ return "ReadyPnPaUa";}
			private:
				/** */
				void	close(	ContextApi&	context,
								StateVar&	sv
								) const noexcept;
				/** */
				void	notifyPortChangeDone(	ContextApi&	context,
												StateVar&	sv
												) const noexcept;
				/** */
				void	connectChanged(	ContextApi&	context,
										StateVar&	sv
										) const noexcept;
				/** */
				void	portDisabled(	ContextApi&	context,
										StateVar&	sv
										) const noexcept;
				/** */
				void	portResumed(	ContextApi&	context,
										StateVar&	sv
										) const noexcept;
			};
		/** */
		class RezRnPnUa : public State {
			private:
				/** */
				const char*	stateName() const noexcept
					{ return "RezRnPnUa";}
			private:
				/** */
				void	close(	ContextApi&	context,
								StateVar&	sv
								) const noexcept;
				/** */
				void	notifyPortChangeDone(	ContextApi&	context,
												StateVar&	sv
												) const noexcept;
				/** */
				void	notifyMsgPipeResourceChangeDone(	ContextApi&	context,
															StateVar&	sv
															) const noexcept;
				/** */
				void	connectChanged(	ContextApi&	context,
										StateVar&	sv
										) const noexcept;
				/** */
				void	portDisabled(	ContextApi&	context,
										StateVar&	sv
										) const noexcept;
			};
		/** */
		class DiscUncUn : public State {
			private:
				/** */
				const char*	stateName() const noexcept
					{ return "DiscUncUn";}
			private:
				/** */
				void	close(	ContextApi&	context,
								StateVar&	sv
								) const noexcept;
				/** */
				void	notifyUsbAddrResChangeDone(	ContextApi&	context,
													StateVar&	sv
													) const noexcept;
				/** */
				void	cancelNotifyUsbAddrResChangeDone(	ContextApi&	context,
															StateVar&	sv
															) const noexcept;
			};
		/** */
		class DiscUnlockAddrZeroHcUa : public State {
			private:
				/** */
				const char*	stateName() const noexcept
					{ return "DiscUnlockAddrZeroHcUa";}
			private:
				/** */
				void	close(	ContextApi&	context,
								StateVar&	sv
								) const noexcept;
				/** */
				void	hcdUnlockAddressZeroDone(	ContextApi&	context,
													StateVar&	sv
													) const noexcept;
			};
		/** */
		class DiscFreeUsbAddrHc : public State {
			private:
				/** */
				const char*	stateName() const noexcept
					{ return "DiscFreeUsbAddrHc";}
			private:
				/** */
				void	close(	ContextApi&	context,
								StateVar&	sv
								) const noexcept;
				/** */
				void	hcdFreeUsbAddrDone(	ContextApi&	context,
											StateVar&	sv
											) const noexcept;
			};
		/** */
		class DiscTimerCancelAzUa : public State {
			private:
				/** */
				const char*	stateName() const noexcept
					{ return "DiscTimerCancelAzUa";}
			private:
				/** */
				void	close(	ContextApi&	context,
								StateVar&	sv
								) const noexcept;
				/** */
				void	timerExpired(	ContextApi&	context,
										StateVar&	sv
										) const noexcept;
				/** */
				void	timerCanceled(	ContextApi&	context,
										StateVar&	sv
										) const noexcept;
			};
		/** */
		class DiscTimerCancelPaUa : public State {
			private:
				/** */
				const char*	stateName() const noexcept
					{ return "DiscTimerCancelPaUa";}
			private:
				/** */
				void	close(	ContextApi&	context,
								StateVar&	sv
								) const noexcept;
				/** */
				void	timerExpired(	ContextApi&	context,
										StateVar&	sv
										) const noexcept;
				/** */
				void	timerCanceled(	ContextApi&	context,
										StateVar&	sv
										) const noexcept;
			};
		/** */
		class DiscReadDescIrpcIrpAzUa : public State {
			private:
				/** */
				const char*	stateName() const noexcept
					{ return "DiscReadDescIrpcIrpAzUa";}
			private:
				/** */
				void	close(	ContextApi&	context,
								StateVar&	sv
								) const noexcept;
				/** */
				void	msgPipeIrpCancelDone(	ContextApi&	context,
												StateVar&	sv
												) const noexcept;
				/** */
				void	msgPipeReadIrpDone(	ContextApi&	context,
											StateVar&	sv
											) const noexcept;
			};
		/** */
		class DiscSetAddrIrpcIrpAzUa : public State {
			private:
				/** */
				const char*	stateName() const noexcept
					{ return "DiscSetAddrIrpcIrpAzUa";}
			private:
				/** */
				void	close(	ContextApi&	context,
								StateVar&	sv
								) const noexcept;
				/** */
				void	msgPipeIrpCancelDone(	ContextApi&	context,
												StateVar&	sv
												) const noexcept;
				/** */
				void	msgPipeNoDataIrpDone(	ContextApi&	context,
												StateVar&	sv
												) const noexcept;
			};
		/** */
		class DiscPrepareHcPaUa : public State {
			private:
				/** */
				const char*	stateName() const noexcept
					{ return "DiscPrepareHcPaUa";}
			private:
				/** */
				void	close(	ContextApi&	context,
								StateVar&	sv
								) const noexcept;
				/** */
				void	prepareToCloseDynSrvDone(	ContextApi&	context,
													StateVar&	sv
													) const noexcept;
			};
		/** */
		class DiscDynSrvHcPaUa : public State {
			private:
				/** */
				const char*	stateName() const noexcept
					{ return "DiscDynSrvHcPaUa";}
			private:
				/** */
				void	close(	ContextApi&	context,
								StateVar&	sv
								) const noexcept;
				/** */
				void	closeDynSrvDone(	ContextApi&	context,
											StateVar&	sv
											) const noexcept;
			};
		class DiscFreePipeHcUa : public State {
			private:
				/** */
				const char*	stateName() const noexcept
					{ return "DiscFreePipeHcUa";}
			private:
				/** */
				void	close(	ContextApi&	context,
								StateVar&	sv
								) const noexcept;
				/** */
				void	hcdMsgPipeFreeDone(	ContextApi&	context,
											StateVar&	sv
											) const noexcept;
			};
		/** */
		class DiscPipeAllocHcUa : public State {
			private:
				/** */
				const char*	stateName() const noexcept
					{ return "DiscPipeAllocHcUa";}
			private:
				/** */
				void	close(	ContextApi&	context,
								StateVar&	sv
								) const noexcept;
				/** */
				void	hcdMsgPipeAllocDone(	ContextApi&	context,
												StateVar&	sv
												) const noexcept;
			};
		/** */
		class DiscRncRnUa : public State {
			private:
				/** */
				const char*	stateName() const noexcept
					{ return "DiscRncRnUa";}
			private:
				/** */
				void	close(	ContextApi&	context,
								StateVar&	sv
								) const noexcept;
				/** */
				void	notifyMsgPipeResourceChangeDone(	ContextApi&	context,
															StateVar&	sv
															) const noexcept;
				/** */
				void	cancelNotifyMsgPipeResourceChangeDone(
														ContextApi&	context,
														StateVar&	sv
														) const noexcept;
			};
		/** */
		class DiscReadFullIrpcIrpPaUa : public State {
			private:
				/** */
				const char*	stateName() const noexcept
					{ return "DiscReadFullIrpcIrpPaUa";}
			private:
				/** */
				void	close(	ContextApi&	context,
								StateVar&	sv
								) const noexcept;
				/** */
				void	msgPipeIrpCancelDone(	ContextApi&	context,
												StateVar&	sv
												) const noexcept;
				/** */
				void	msgPipeReadIrpDone(	ContextApi&	context,
											StateVar&	sv
											) const noexcept;
			};

	private:
		/** */
		struct TraceRec : public Oscl::QueueItem {
			public:
				/** */
				const State*	_state;
			};

	private:
		/** */
		ContextApi&		_context;
		/** */
		const State*	_state;

	private:
		/** */
		enum{nTraceRecs=20};
		/** */
		TraceRec				_traceRec[nTraceRecs];
		/** */
		Oscl::Queue<TraceRec>	_freeTraceRecs;
		/** */
		Oscl::Queue<TraceRec>	_trace;

	public:	// Constructors
		/** */
		StateVar(ContextApi& context) noexcept;

	public:	// events
		/** */
		void	open() noexcept;
		/** */
		void	close() noexcept;
		/** */
		void	setResetDone() noexcept;
		/** */
		void	setSuspendDone() noexcept;
		/** */
		void	setPowerDone() noexcept;
		/** */
		void	clrEnableDone() noexcept;
		/** */
		void	clrSuspendDone() noexcept;
		/** */
		void	clrPowerDone() noexcept;
		/** */
		void	clrConnectChangeDone() noexcept;
		/** */
		void	clrResetChangeDone() noexcept;
		/** */
		void	clrEnableChangeDone() noexcept;
		/** */
		void	clrSuspendChangeDone() noexcept;
		/** */
		void	clrOverCurrentChangeDone() noexcept;
		/** */
		void	cancelReqDone() noexcept;
		/** */
		void	notifyPortChangeDone() noexcept;
		/** */
		void	cancelNotifyPortChangeDone() noexcept;
		/** */
		void	getPortStatusDone() noexcept;
		/** */
		void	requestFailed() noexcept;
		/** */
		void	msgPipeIrpCancelDone() noexcept;
		/** */
		void	msgPipeNoDataIrpDone() noexcept;
		/** */
		void	msgPipeReadIrpDone() noexcept;
		/** */
		void	msgPipeWriteIrpDone() noexcept;
		/** */
		void	hcdMsgPipeAllocDone() noexcept;
		/** */
		void	hcdMsgPipeFreeDone() noexcept;
		/** */
		void	notifyMsgPipeResourceChangeDone() noexcept;
		/** */
		void	cancelNotifyMsgPipeResourceChangeDone() noexcept;
		/** */
		void	hcdLockAddressZeroDone() noexcept;
		/** */
		void	hcdUnlockAddressZeroDone() noexcept;
		/** */
		void	hcdCancelLockAddrZeroDone() noexcept;
		/** */
		void	hcdAllocUsbAddrDone() noexcept;
		/** */
		void	hcdFreeUsbAddrDone() noexcept;
		/** */
		void	prepareToCloseDynSrvDone() noexcept;
		/** */
		void	closeDynSrvDone() noexcept;
		/** */
		void	notifyUsbAddrResChangeDone() noexcept;
		/** */
		void	cancelNotifyUsbAddrResChangeDone() noexcept;
		/** */
		void	timerExpired() noexcept;
		/** */
		void	timerCanceled() noexcept;
		/** */
		void	connectChanged() noexcept;
		/** */
		void	portDisabled() noexcept;
		/** */
		void	portResumed() noexcept;
		/** */
		void	portOverCurrentChange() noexcept;
		/** */
		void	portResetDone() noexcept;

	private: // global actions and state change operations
		/** */
		friend class State;
		friend class Closed;
		friend class Closing;
		friend class ClosingPccPc;
		friend class ClosingPncPn;
		friend class ClosingAllocUsbAddrHc;
		friend class ClosingFreeUsbAddrHc;
		friend class ClosingHczcHczUa;
		friend class ClosingHczcUa;
		friend class ClosingHczcAzUa;
		friend class ClosingUnlockAddrZeroHcUa;
		friend class ClosingSetResetPccPcAzUa;
		friend class ClosingTimerCancelPncPnAzUa;
		friend class ClosingTimerCancelPncPnPaUa;
		friend class ClosingTimerCancelPaUa;
		friend class ClosingTimerCancelAzUa;
		friend class ClosingPncPnAzUa;
		friend class ClosingPncPnPaUa;
		friend class ClosingClrResetChangePccPcAzUa;
		friend class ClosingResetWaitTwcTwAzUa;
		friend class ClosingUncUnPncPn;
		friend class ClosingUncUn;
		friend class ClosingReadDescIrpPncPnAzUa;
		friend class ClosingReadFullIrpPncPnPaUa;
		friend class ClosingReadDescIrpcIrpAzUa;
		friend class ClosingReadFullIrpcIrpPaUa;
		friend class ClosingSetAddrIrpPncPnAzUa;
		friend class ClosingSetAddrIrpcIrpAzUa;
		friend class ClosingUazPncPnUa;
		friend class ClosingUazUa;
		friend class ClosingPncPnUa;
		friend class ClosingPipeAllocHcPncPnUa;
		friend class ClosingPipeAllocHcUa;
		friend class ClosingFreePipeHcUa;
		friend class ClosingFreePipeHcPncPnUa;
		friend class ClosingAdvPncPnPaUa;
		friend class ClosingPrepareHcPaUa;
		friend class ClosingDynSrvHcPaUa;
		friend class ClosingRncRnUa;
		friend class ClosingRncRnPncPnUa;
		friend class PowerWait;
		friend class DisabledPc;
		friend class DisabledPn;
		friend class DisabledClrConnChangePc;
		friend class DisabledGetStatusPc;
		friend class DisabledClrEnableChangePc;
		friend class DisabledClrResetChangePc;
		friend class ConnAllocUsbAddrHc;
		friend class ConnLockAddrZeroUa;
		friend class ConnSetResetPcAzUa;
		friend class ConnWaitResetPnAzUa;
		friend class ConnClrResetChangePcAzUa;
		friend class ConnResetWaitTwAzUa;
		friend class ConnWaitUsbAddrUnPn;
		friend class ConnReadDescIrpPnAzUa;
		friend class ConnReadDescErrorPnAzUa;
		friend class ConnSetAddrIrpPnAzUa;
		friend class ConnSetAddrErrorPnAzUa;
		friend class ConnUnlockAddrZeroHcPnUa;
		friend class ConnPipeAllocHcPnUa;
		friend class ConnReadFullIrpPnPaUa;
		friend class ConnReadFullErrorPnPaUa;
		friend class ReadyPnPaUa;
		friend class RezRnPnUa;
		friend class DiscUncUn;
		friend class DiscUnlockAddrZeroHcUa;
		friend class DiscFreeUsbAddrHc;
		friend class DiscTimerCancelAzUa;
		friend class DiscTimerCancelPaUa;
		friend class DiscReadDescIrpcIrpAzUa;
		friend class DiscSetAddrIrpcIrpAzUa;
		friend class DiscPrepareHcPaUa;
		friend class DiscDynSrvHcPaUa;
		friend class DiscFreePipeHcUa;
		friend class DiscPipeAllocHcUa;
		friend class DiscRncRnUa;
		friend class DiscReadFullIrpcIrpPaUa;
		/** */
		void	protocolError(const char* state) noexcept;
		/** */
		void	changeToClosed() noexcept;
		/** */
		void	changeToClosing() noexcept;
		/** */
		void	changeToClosingPccPc() noexcept;
		/** */
		void	changeToClosingPncPn() noexcept;
		/** */
		void	changeToClosingAllocUsbAddrHc() noexcept;
		/** */
		void	changeToClosingFreeUsbAddrHc() noexcept;
		/** */
		void	changeToClosingHczcHczUa() noexcept;
		/** */
		void	changeToClosingHczcUa() noexcept;
		/** */
		void	changeToClosingHczcAzUa() noexcept;
		/** */
		void	changeToClosingUnlockAddrZeroHcUa() noexcept;
		/** */
		void	changeToClosingSetResetPccPcAzUa() noexcept;
		/** */
		void	changeToClosingPncPnAzUa() noexcept;
		/** */
		void	changeToClosingTimerCancelPncPnAzUa() noexcept;
		/** */
		void	changeToClosingTimerCancelPncPnPaUa() noexcept;
		/** */
		void	changeToClosingTimerCancelPaUa() noexcept;
		/** */
		void	changeToClosingTimerCancelAzUa() noexcept;
		/** */
		void	changeToClosingClrResetChangePccPcAzUa() noexcept;
		/** */
		void	changeToClosingResetWaitTwcTwAzUa() noexcept;
		/** */
		void	changeToClosingUncUnPncPn() noexcept;
		/** */
		void	changeToClosingUncUn() noexcept;
		/** */
		void	changeToClosingReadDescIrpPncPnAzUa() noexcept;
		/** */
		void	changeToClosingReadDescIrpcIrpAzUa() noexcept;
		/** */
		void	changeToClosingSetAddrIrpPncPnAzUa() noexcept;
		/** */
		void	changeToClosingSetAddrIrpcIrpAzUa() noexcept;
		/** */
		void	changeToClosingUazPncPnUa() noexcept;
		/** */
		void	changeToClosingUazUa() noexcept;
		/** */
		void	changeToClosingPncPnUa() noexcept;
		/** */
		void	changeToClosingPipeAllocHcPncPnUa() noexcept;
		/** */
		void	changeToClosingPipeAllocHcUa() noexcept;
		/** */
		void	changeToClosingFreePipeHcUa() noexcept;
		/** */
		void	changeToClosingFreePipeHcPncPnUa() noexcept;
		/** */
		void	changeToClosingAdvPncPnPaUa() noexcept;
		/** */
		void	changeToClosingPrepareHcPaUa() noexcept;
		/** */
		void	changeToClosingDynSrvHcPaUa() noexcept;
		/** */
		void	changeToClosingRncRnUa() noexcept;
		/** */
		void	changeToClosingRncRnPncPnUa() noexcept;
		/** */
		void	changeToClosingReadFullIrpPncPnPaUa() noexcept;
		/** */
		void	changeToClosingReadFullIrpcIrpPaUa() noexcept;
		/** */
		void	changeToClosingPncPnPaUa() noexcept;
		/** */
		void	changeToPowerWait() noexcept;
		/** */
		void	changeToDisabledPc() noexcept;
		/** */
		void	changeToDisabledPn() noexcept;
		/** */
		void	changeToDisabledClrConnChangePc() noexcept;
		/** */
		void	changeToDisabledGetStatusPc() noexcept;
		/** */
		void	changeToDisabledClrEnableChangePc() noexcept;
		/** */
		void	changeToDisabledClrResetChangePc() noexcept;
		/** */
		void	changeToConnAllocUsbAddrHc() noexcept;
		/** */
		void	changeToConnLockAddrZeroUa() noexcept;
		/** */
		void	changeToConnSetResetPcAzUa() noexcept;
		/** */
		void	changeToConnWaitResetPnAzUa() noexcept;
		/** */
		void	changeToConnClrResetChangePcAzUa() noexcept;
		/** */
		void	changeToConnResetWaitTwAzUa() noexcept;
		/** */
		void	changeToConnWaitUsbAddrUnPn() noexcept;
		/** */
		void	changeToConnReadDescIrpPnAzUa() noexcept;
		/** */
		void	changeToConnReadDescErrorPnAzUa() noexcept;
		/** */
		void	changeToConnSetAddrIrpPnAzUa() noexcept;
		/** */
		void	changeToConnSetAddrErrorPnAzUa() noexcept;
		/** */
		void	changeToConnUnlockAddrZeroHcPnUa() noexcept;
		/** */
		void	changeToConnPipeAllocHcPnUa() noexcept;
		/** */
		void	changeToConnReadFullIrpPnPaUa() noexcept;
		/** */
		void	changeToConnReadFullErrorPnPaUa() noexcept;
		/** */
		void	changeToReadyPnPaUa() noexcept;
		/** */
		void	changeToRezRnPnUa() noexcept;
		/** */
		void	changeToDiscUncUn() noexcept;
		/** */
		void	changeToDiscUnlockAddrZeroHcUa() noexcept;
		/** */
		void	changeToDiscFreeUsbAddrHc() noexcept;
		/** */
		void	changeToDiscTimerCancelAzUa() noexcept;
		/** */
		void	changeToDiscTimerCancelPaUa() noexcept;
		/** */
		void	changeToDiscReadDescIrpcIrpAzUa() noexcept;
		/** */
		void	changeToDiscSetAddrIrpcIrpAzUa() noexcept;
		/** */
		void	changeToDiscPrepareHcPaUa() noexcept;
		/** */
		void	changeToDiscDynSrvHcPaUa() noexcept;
		/** */
		void	changeToDiscFreePipeHcUa() noexcept;
		/** */
		void	changeToDiscPipeAllocHcUa() noexcept;
		/** */
		void	changeToDiscRncRnUa() noexcept;
		/** */
		void	changeToDiscReadFullIrpcIrpPaUa() noexcept;

	private:
		/** */
		void	trace(const State& state) noexcept;
	};

}
}
}
}

#endif
