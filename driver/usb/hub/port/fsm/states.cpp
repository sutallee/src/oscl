/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "fsm.h"
#include "oscl/mt/thread.h"
using namespace Oscl::Usb::Hub::Port;

void	StateVar::State::
open(	ContextApi&	context,
		StateVar&	sv
		) const noexcept{
	sv.protocolError(stateName());
	}

void	StateVar::State::
close(	ContextApi&	context,
		StateVar&	sv
		) const noexcept{
	sv.protocolError(stateName());
	}

void	StateVar::State::
setResetDone(	ContextApi&	context,
				StateVar&	sv
				) const noexcept{
	sv.protocolError(stateName());
	}

void	StateVar::State::
setSuspendDone(	ContextApi&	context,
				StateVar&	sv
				) const noexcept{
	sv.protocolError(stateName());
	}

void	StateVar::State::
setPowerDone(	ContextApi&	context,
				StateVar&	sv
				) const noexcept{
	sv.protocolError(stateName());
	}

void	StateVar::State::
clrEnableDone(	ContextApi&	context,
				StateVar&	sv
				) const noexcept{
	sv.protocolError(stateName());
	}

void	StateVar::State::
clrSuspendDone(	ContextApi&	context,
				StateVar&	sv
				) const noexcept{
	sv.protocolError(stateName());
	}

void	StateVar::State::
clrPowerDone(	ContextApi&	context,
				StateVar&	sv
				) const noexcept{
	sv.protocolError(stateName());
	}

void	StateVar::State::
clrConnectChangeDone(	ContextApi&	context,
						StateVar&	sv
						) const noexcept{
	sv.protocolError(stateName());
	}

void	StateVar::State::
clrResetChangeDone(	ContextApi&	context,
					StateVar&	sv
					) const noexcept{
	sv.protocolError(stateName());
	}

void	StateVar::State::
clrEnableChangeDone(	ContextApi&	context,
						StateVar&	sv
						) const noexcept{
	sv.protocolError(stateName());
	}

void	StateVar::State::
clrSuspendChangeDone(	ContextApi&	context,
						StateVar&	sv
						) const noexcept{
	sv.protocolError(stateName());
	}

void	StateVar::State::
clrOverCurrentChangeDone(	ContextApi&	context,
							StateVar&	sv
							) const noexcept{
	sv.protocolError(stateName());
	}

void	StateVar::State::
cancelReqDone(	ContextApi&	context,
				StateVar&	sv
				) const noexcept{
	sv.protocolError(stateName());
	}

void	StateVar::State::
notifyPortChangeDone(	ContextApi&	context,
						StateVar&	sv
						) const noexcept{
	sv.protocolError(stateName());
	}

void	StateVar::State::
cancelNotifyPortChangeDone(	ContextApi&	context,
							StateVar&	sv
							) const noexcept{
	sv.protocolError(stateName());
	}

void	StateVar::State::
getPortStatusDone(	ContextApi&	context,
					StateVar&	sv
					) const noexcept{
	sv.protocolError(stateName());
	}

void	StateVar::State::
requestFailed(	ContextApi&	context,
				StateVar&	sv
				) const noexcept{
	sv.protocolError(stateName());
	}

void	StateVar::State::
msgPipeIrpCancelDone(	ContextApi&	context,
						StateVar&	sv
						) const noexcept{
	sv.protocolError(stateName());
	}

void	StateVar::State::
msgPipeNoDataIrpDone(	ContextApi&	context,
						StateVar&	sv
						) const noexcept{
	sv.protocolError(stateName());
	}

void	StateVar::State::
msgPipeReadIrpDone(	ContextApi&	context,
					StateVar&	sv
					) const noexcept{
	sv.protocolError(stateName());
	}

void	StateVar::State::
msgPipeWriteIrpDone(	ContextApi&	context,
						StateVar&	sv
						) const noexcept{
	sv.protocolError(stateName());
	}

void	StateVar::State::
notifyMsgPipeResourceChangeDone(	ContextApi&	context,
									StateVar&	sv
									) const noexcept{
	sv.protocolError(stateName());
	}

void	StateVar::State::
cancelNotifyMsgPipeResourceChangeDone(	ContextApi&	context,
										StateVar&	sv
										) const noexcept{
	sv.protocolError(stateName());
	}

void	StateVar::State::
hcdMsgPipeAllocDone(	ContextApi&	context,
						StateVar&	sv
						) const noexcept{
	sv.protocolError(stateName());
	}

void	StateVar::State::
hcdMsgPipeFreeDone(	ContextApi&	context,
					StateVar&	sv
					) const noexcept{
	sv.protocolError(stateName());
	}

void	StateVar::State::
hcdLockAddressZeroDone(	ContextApi&	context,
						StateVar&	sv
						) const noexcept{
	sv.protocolError(stateName());
	}

void	StateVar::State::
hcdUnlockAddressZeroDone(	ContextApi&	context,
							StateVar&	sv
							) const noexcept{
	sv.protocolError(stateName());
	}

void	StateVar::State::
hcdCancelLockAddrZeroDone(	ContextApi&	context,
							StateVar&	sv
							) const noexcept{
	sv.protocolError(stateName());
	}

void	StateVar::State::
hcdAllocUsbAddrDone(	ContextApi&	context,
						StateVar&	sv
						) const noexcept{
	sv.protocolError(stateName());
	}

void	StateVar::State::
hcdFreeUsbAddrDone(	ContextApi&	context,
					StateVar&	sv
					) const noexcept{
	sv.protocolError(stateName());
	}

void	StateVar::State::
prepareToCloseDynSrvDone(	ContextApi&	context,
							StateVar&	sv
							) const noexcept{
	sv.protocolError(stateName());
	}

void	StateVar::State::
closeDynSrvDone(	ContextApi&	context,
					StateVar&	sv
					) const noexcept{
	sv.protocolError(stateName());
	}

void	StateVar::State::
notifyUsbAddrResChangeDone(	ContextApi&	context,
							StateVar&	sv
							) const noexcept{
	sv.protocolError(stateName());
	}

void	StateVar::State::
cancelNotifyUsbAddrResChangeDone(	ContextApi&	context,
									StateVar&	sv
									) const noexcept{
	sv.protocolError(stateName());
	}

void	StateVar::State::
connectChanged(	ContextApi&	context,
				StateVar&	sv
				) const noexcept{
	sv.protocolError(stateName());
	}

void	StateVar::State::
timerExpired(	ContextApi&	context,
				StateVar&	sv
				) const noexcept{
	sv.protocolError(stateName());
	}

void	StateVar::State::
timerCanceled(	ContextApi&	context,
				StateVar&	sv
				) const noexcept{
	sv.protocolError(stateName());
	}

void	StateVar::State::
portDisabled(	ContextApi&	context,
				StateVar&	sv
				) const noexcept{
	sv.protocolError(stateName());
	}

void	StateVar::State::
portResumed(	ContextApi&	context,
				StateVar&	sv
				) const noexcept{
	sv.protocolError(stateName());
	}

void	StateVar::State::
portOverCurrentChange(	ContextApi&	context,
						StateVar&	sv
						) const noexcept{
	sv.protocolError(stateName());
	}

void	StateVar::State::portResetDone(	ContextApi&	context,
										StateVar&	sv
										) const noexcept{
	sv.protocolError(stateName());
	}

void	StateVar::Closed::open(	ContextApi&	context,
								StateVar&	sv
								) const noexcept{
	context.startPowerTimer();
	context.returnOpenReq();
	sv.changeToPowerWait();
	}

void	StateVar::Closing::
close(	ContextApi&	context,
		StateVar&	sv
		) const noexcept{
	context.returnCloseReq();
	sv.changeToClosed();
	}

void	StateVar::ClosingPccPc::
setResetDone(	ContextApi&	context,
				StateVar&	sv
				) const noexcept{
	// ignore
	}

void	StateVar::ClosingPccPc::
timerExpired(	ContextApi&	context,
				StateVar&	sv
				) const noexcept{
	// ignore
	}

void	StateVar::ClosingPccPc::
setSuspendDone(	ContextApi&	context,
				StateVar&	sv
				) const noexcept{
	// ignore
	}

void	StateVar::ClosingPccPc::
setPowerDone(	ContextApi&	context,
				StateVar&	sv
				) const noexcept{
	// ignore
	}

void	StateVar::ClosingPccPc::
clrEnableDone(	ContextApi&	context,
				StateVar&	sv
				) const noexcept{
	// ignore
	}

void	StateVar::ClosingPccPc::
clrSuspendDone(	ContextApi&	context,
				StateVar&	sv
				) const noexcept{
	// ignore
	}

void	StateVar::ClosingPccPc::
clrPowerDone(	ContextApi&	context,
				StateVar&	sv
				) const noexcept{
	// ignore
	}

void	StateVar::ClosingPccPc::
clrConnectChangeDone(	ContextApi&	context,
						StateVar&	sv
						) const noexcept{
	// ignore
	}

void	StateVar::ClosingPccPc::
clrResetChangeDone(	ContextApi&	context,
					StateVar&	sv
					) const noexcept{
	// ignore
	}

void	StateVar::ClosingPccPc::
clrEnableChangeDone(	ContextApi&	context,
						StateVar&	sv
						) const noexcept{
	// ignore
	}

void	StateVar::ClosingPccPc::
clrSuspendChangeDone(	ContextApi&	context,
						StateVar&	sv
						) const noexcept{
	// ignore
	}

void	StateVar::ClosingPccPc::clrOverCurrentChangeDone(	ContextApi&	context,
															StateVar&	sv
															) const noexcept{
	// ignore
	}

void	StateVar::ClosingPccPc::
cancelReqDone(	ContextApi&	context,
				StateVar&	sv
				) const noexcept{
	context.returnCloseReq();
	sv.changeToClosed();
	}

void	StateVar::ClosingPccPc::
timerCanceled(	ContextApi&	context,
				StateVar&	sv
				) const noexcept{
	context.returnCloseReq();
	sv.changeToClosed();
	}

void	StateVar::ClosingPccPc::
getPortStatusDone(	ContextApi&	context,
					StateVar&	sv
					) const noexcept{
	// ignore
	}

void	StateVar::ClosingPccPc::
requestFailed(	ContextApi&	context,
				StateVar&	sv
				) const noexcept{
	sv.changeToClosing();
	}

void	StateVar::ClosingPncPn::
notifyPortChangeDone(	ContextApi&	context,
						StateVar&	sv
						) const noexcept{
	// ignore
	}

void	StateVar::ClosingPncPn::
cancelNotifyPortChangeDone(	ContextApi&	context,
							StateVar&	sv
							) const noexcept{
	context.returnCloseReq();
	sv.changeToClosed();
	}

void	StateVar::ClosingAllocUsbAddrHc::
hcdAllocUsbAddrDone(	ContextApi&	context,
						StateVar&	sv
						) const noexcept{
	if(context.usbAddrAllocated()){
		context.requestHcdFreeUsbAddr();
		sv.changeToClosingFreeUsbAddrHc();
		}
	else{
		context.returnCloseReq();
		sv.changeToClosed();
		}
	}

void	StateVar::ClosingFreeUsbAddrHc::
hcdFreeUsbAddrDone(	ContextApi&	context,
					StateVar&	sv
					) const noexcept{
	context.returnCloseReq();
	sv.changeToClosed();
	}

void	StateVar::ClosingHczcHczUa::
hcdLockAddressZeroDone(	ContextApi&	context,
						StateVar&	sv
						) const noexcept{
	if(context.lockAddressZeroCanceled()){
		sv.changeToClosingHczcUa();
		}
	else{
		sv.changeToClosingHczcAzUa();
		}
	}

void	StateVar::ClosingHczcUa::
hcdCancelLockAddrZeroDone(	ContextApi&	context,
							StateVar&	sv
							) const noexcept{
	context.requestHcdFreeUsbAddr();
	sv.changeToClosingFreeUsbAddrHc();
	}

void	StateVar::ClosingHczcAzUa::
hcdCancelLockAddrZeroDone(	ContextApi&	context,
							StateVar&	sv
							) const noexcept{
	context.requestHcdUnlockAddressZero();
	sv.changeToClosingUnlockAddrZeroHcUa();
	}

void	StateVar::ClosingUnlockAddrZeroHcUa::
hcdUnlockAddressZeroDone(	ContextApi&	context,
							StateVar&	sv
							) const noexcept{
	context.requestHcdFreeUsbAddr();
	sv.changeToClosingFreeUsbAddrHc();
	}

void	StateVar::ClosingSetResetPccPcAzUa::
setResetDone(	ContextApi&	context,
				StateVar&	sv
				) const noexcept{
	// ignore
	}

void	StateVar::ClosingSetResetPccPcAzUa::
cancelReqDone(	ContextApi&	context,
				StateVar&	sv
				) const noexcept{
	context.requestHcdUnlockAddressZero();
	sv.changeToClosingUnlockAddrZeroHcUa();
	}

void	StateVar::ClosingSetResetPccPcAzUa::
requestFailed(	ContextApi&	context,
				StateVar&	sv
				) const noexcept{
	sv.changeToClosing();
	}

void	StateVar::ClosingPncPnAzUa::
notifyPortChangeDone(	ContextApi&	context,
						StateVar&	sv
						) const noexcept{
	// ignore
	}

void	StateVar::ClosingPncPnAzUa::
cancelNotifyPortChangeDone(	ContextApi&	context,
							StateVar&	sv
							) const noexcept{
	context.requestHcdUnlockAddressZero();
	sv.changeToClosingUnlockAddrZeroHcUa();
	}

void	StateVar::ClosingTimerCancelPncPnAzUa::
notifyPortChangeDone(	ContextApi&	context,
						StateVar&	sv
						) const noexcept{
	// ignore
	}

void	StateVar::ClosingTimerCancelPncPnAzUa::
cancelNotifyPortChangeDone(	ContextApi&	context,
							StateVar&	sv
							) const noexcept{
	sv.changeToClosingTimerCancelAzUa();
	}

void	StateVar::ClosingTimerCancelPncPnAzUa::
timerExpired(	ContextApi&	context,
				StateVar&	sv
				) const noexcept{
	// ignore
	}

void	StateVar::ClosingTimerCancelPncPnAzUa::
timerCanceled(	ContextApi&	context,
				StateVar&	sv
				) const noexcept{
	sv.changeToClosingPncPnAzUa();
	}

void	StateVar::ClosingTimerCancelPncPnPaUa::
notifyPortChangeDone(	ContextApi&	context,
						StateVar&	sv
						) const noexcept{
	// ignore
	}

void	StateVar::ClosingTimerCancelPncPnPaUa::
cancelNotifyPortChangeDone(	ContextApi&	context,
							StateVar&	sv
							) const noexcept{
	sv.changeToClosingTimerCancelPaUa();
	}

void	StateVar::ClosingTimerCancelPncPnPaUa::
timerExpired(	ContextApi&	context,
				StateVar&	sv
				) const noexcept{
	// ignore
	}

void	StateVar::ClosingTimerCancelPncPnPaUa::
timerCanceled(	ContextApi&	context,
				StateVar&	sv
				) const noexcept{
	sv.changeToClosingPncPnPaUa();
	}

void	StateVar::ClosingTimerCancelPaUa::
timerExpired(	ContextApi&	context,
				StateVar&	sv
				) const noexcept{
	// ignore
	}

void	StateVar::ClosingTimerCancelPaUa::
timerCanceled(	ContextApi&	context,
				StateVar&	sv
				) const noexcept{
	context.requestHcdMsgPipeFree();
	sv.changeToClosingFreePipeHcUa();
	}

void	StateVar::ClosingTimerCancelAzUa::
timerExpired(	ContextApi&	context,
				StateVar&	sv
				) const noexcept{
	// ignore
	}

void	StateVar::ClosingTimerCancelAzUa::
timerCanceled(	ContextApi&	context,
					StateVar&	sv
					) const noexcept{
	context.requestHcdUnlockAddressZero();
	sv.changeToClosingUnlockAddrZeroHcUa();
	}

void	StateVar::ClosingClrResetChangePccPcAzUa::
clrResetChangeDone(	ContextApi&	context,
					StateVar&	sv
					) const noexcept{
	// ignore
	}

void	StateVar::ClosingClrResetChangePccPcAzUa::
cancelReqDone(	ContextApi&	context,
				StateVar&	sv
				) const noexcept{
	context.requestHcdUnlockAddressZero();
	sv.changeToClosingUnlockAddrZeroHcUa();
	}

void	StateVar::ClosingClrResetChangePccPcAzUa::
requestFailed(	ContextApi&	context,
				StateVar&	sv
				) const noexcept{
	sv.changeToClosing();
	}

//
void	StateVar::ClosingResetWaitTwcTwAzUa::
timerExpired(	ContextApi&	context,
				StateVar&	sv
				) const noexcept{
	// ignore
	}

void	StateVar::ClosingResetWaitTwcTwAzUa::
timerCanceled(	ContextApi&	context,
				StateVar&	sv
				) const noexcept{
	context.requestHcdUnlockAddressZero();
	sv.changeToClosingUnlockAddrZeroHcUa();
	}
//
void	StateVar::ClosingUncUnPncPn::
notifyPortChangeDone(	ContextApi&	context,
						StateVar&	sv
						) const noexcept{
	// ignore
	}

void	StateVar::ClosingUncUnPncPn::
cancelNotifyPortChangeDone(	ContextApi&	context,
							StateVar&	sv
							) const noexcept{
	sv.changeToClosingUncUn();
	}

void	StateVar::ClosingUncUnPncPn::
notifyUsbAddrResChangeDone(	ContextApi&	context,
							StateVar&	sv
							) const noexcept{
	// ignore
	}

void	StateVar::ClosingUncUnPncPn::
cancelNotifyUsbAddrResChangeDone(	ContextApi&	context,
									StateVar&	sv
									) const noexcept{
	sv.changeToClosingPncPn();
	}

void	StateVar::ClosingUncUn::
notifyUsbAddrResChangeDone(	ContextApi&	context,
							StateVar&	sv
							) const noexcept{
	// ignore
	}

void	StateVar::ClosingUncUn::
cancelNotifyUsbAddrResChangeDone(	ContextApi&	context,
									StateVar&	sv
									) const noexcept{
	context.returnCloseReq();
	sv.changeToClosed();
	}

void	StateVar::ClosingReadDescIrpPncPnAzUa::
notifyPortChangeDone(	ContextApi&	context,
						StateVar&	sv
						) const noexcept{
	// ignore
	}

void	StateVar::ClosingReadDescIrpPncPnAzUa::
cancelNotifyPortChangeDone(	ContextApi&	context,
							StateVar&	sv
							) const noexcept{
	context.requestMsgPipeIrpCancel();
	sv.changeToClosingReadDescIrpcIrpAzUa();
	}

void	StateVar::ClosingReadDescIrpPncPnAzUa::
msgPipeReadIrpDone(	ContextApi&	context,
					StateVar&	sv
					) const noexcept{
	sv.changeToClosingPncPnAzUa();
	}

void	StateVar::ClosingReadDescIrpcIrpAzUa::
msgPipeIrpCancelDone(	ContextApi&	context,
						StateVar&	sv
						) const noexcept{
	context.requestHcdUnlockAddressZero();
	sv.changeToClosingUnlockAddrZeroHcUa();
	}

void	StateVar::ClosingReadDescIrpcIrpAzUa::
msgPipeReadIrpDone(	ContextApi&	context,
					StateVar&	sv
					) const noexcept{
	// ignore
	}

void	StateVar::ClosingSetAddrIrpPncPnAzUa::
notifyPortChangeDone(	ContextApi&	context,
						StateVar&	sv
						) const noexcept{
	// ignore
	}

void	StateVar::ClosingSetAddrIrpPncPnAzUa::
cancelNotifyPortChangeDone(	ContextApi&	context,
							StateVar&	sv
							) const noexcept{
	context.requestMsgPipeIrpCancel();
	sv.changeToClosingSetAddrIrpcIrpAzUa();
	}

void	StateVar::ClosingSetAddrIrpPncPnAzUa::
msgPipeNoDataIrpDone(	ContextApi&	context,
						StateVar&	sv
						) const noexcept{
	sv.changeToClosingPncPnAzUa();
	}

void	StateVar::ClosingSetAddrIrpcIrpAzUa::
msgPipeIrpCancelDone(	ContextApi&	context,
						StateVar&	sv
						) const noexcept{
	context.requestHcdUnlockAddressZero();
	sv.changeToClosingUnlockAddrZeroHcUa();
	}

void	StateVar::ClosingSetAddrIrpcIrpAzUa::
msgPipeNoDataIrpDone(	ContextApi&	context,
						StateVar&	sv
						) const noexcept{
	// ignore
	}

void	StateVar::ClosingUazPncPnUa::
notifyPortChangeDone(	ContextApi&	context,
						StateVar&	sv
						) const noexcept{
	// ignore
	}

void	StateVar::ClosingUazPncPnUa::
cancelNotifyPortChangeDone(	ContextApi&	context,
							StateVar&	sv
							) const noexcept{
	sv.changeToClosingUazUa();
	}

void	StateVar::ClosingUazPncPnUa::
hcdUnlockAddressZeroDone(	ContextApi&	context,
							StateVar&	sv
							) const noexcept{
	sv.changeToClosingPncPnUa();
	}

void	StateVar::ClosingUazUa::
hcdUnlockAddressZeroDone(	ContextApi&	context,
							StateVar&	sv
							) const noexcept{
	context.requestHcdFreeUsbAddr();
	sv.changeToClosingFreeUsbAddrHc();
	}

void	StateVar::ClosingPncPnUa::
notifyPortChangeDone(	ContextApi&	context,
						StateVar&	sv
						) const noexcept{
	// ignore
	}

void	StateVar::ClosingPncPnUa::
cancelNotifyPortChangeDone(	ContextApi&	context,
							StateVar&	sv
							) const noexcept{
	context.requestHcdFreeUsbAddr();
	sv.changeToClosingFreeUsbAddrHc();
	}

void	StateVar::ClosingPipeAllocHcPncPnUa::
notifyPortChangeDone(	ContextApi&	context,
						StateVar&	sv
						) const noexcept{
	// ignore
	}

void	StateVar::ClosingPipeAllocHcPncPnUa::
cancelNotifyPortChangeDone(	ContextApi&	context,
							StateVar&	sv
							) const noexcept{
	sv.changeToClosingPipeAllocHcUa();
	}

void	StateVar::ClosingPipeAllocHcPncPnUa::
hcdMsgPipeAllocDone(	ContextApi&	context,
						StateVar&	sv
						) const noexcept{
	if(context.pipeAllocated()){
		context.requestHcdMsgPipeFree();
		sv.changeToClosingFreePipeHcPncPnUa();
		}
	else{
		sv.changeToClosingPncPnUa();
		}
	}

void	StateVar::ClosingPipeAllocHcUa::
hcdMsgPipeAllocDone(	ContextApi&	context,
						StateVar&	sv
						) const noexcept{
	if(context.pipeAllocated()){
		context.requestHcdMsgPipeFree();
		sv.changeToClosingFreePipeHcUa();
		}
	else{
		context.requestHcdFreeUsbAddr();
		sv.changeToClosingFreeUsbAddrHc();
		}
	}

void	StateVar::ClosingFreePipeHcUa::
hcdMsgPipeFreeDone(	ContextApi&	context,
					StateVar&	sv
					) const noexcept{
	context.requestHcdFreeUsbAddr();
	sv.changeToClosingFreeUsbAddrHc();
	}

void	StateVar::ClosingFreePipeHcPncPnUa::
notifyPortChangeDone(	ContextApi&	context,
						StateVar&	sv
						) const noexcept{
	// ignore
	}

void	StateVar::ClosingFreePipeHcPncPnUa::
cancelNotifyPortChangeDone(	ContextApi&	context,
							StateVar&	sv
							) const noexcept{
	sv.changeToClosingFreePipeHcUa();
	}

void	StateVar::ClosingFreePipeHcPncPnUa::
hcdMsgPipeFreeDone(	ContextApi&	context,
					StateVar&	sv
					) const noexcept{
	sv.changeToClosingPncPnUa();
	}

void	StateVar::ClosingAdvPncPnPaUa::
notifyPortChangeDone(	ContextApi&	context,
						StateVar&	sv
						) const noexcept{
	// ignore
	}

void	StateVar::ClosingAdvPncPnPaUa::
cancelNotifyPortChangeDone(	ContextApi&	context,
							StateVar&	sv
							) const noexcept{
	context.prepareToCloseDynSrv();
	sv.changeToClosingPrepareHcPaUa();
	}

void	StateVar::ClosingPrepareHcPaUa::
prepareToCloseDynSrvDone(	ContextApi&	context,
							StateVar&	sv
							) const noexcept{
	context.closeDynSrv();
	sv.changeToClosingDynSrvHcPaUa();
	}

void	StateVar::ClosingDynSrvHcPaUa::
closeDynSrvDone(	ContextApi&	context,
					StateVar&	sv
					) const noexcept{
	context.requestHcdMsgPipeFree();
	sv.changeToClosingFreePipeHcUa();
	}

void	StateVar::ClosingRncRnUa::
notifyMsgPipeResourceChangeDone(	ContextApi&	context,
									StateVar&	sv
									) const noexcept{
	// ignore
	}

void	StateVar::ClosingRncRnUa::
cancelNotifyMsgPipeResourceChangeDone(	ContextApi&	context,
										StateVar&	sv
										) const noexcept{
	context.requestHcdFreeUsbAddr();
	sv.changeToClosingFreeUsbAddrHc();
	}

void	StateVar::ClosingRncRnPncPnUa::
notifyPortChangeDone(	ContextApi&	context,
						StateVar&	sv
						) const noexcept{
	// ignore
	}

void	StateVar::ClosingRncRnPncPnUa::
cancelNotifyPortChangeDone(	ContextApi&	context,
							StateVar&	sv
							) const noexcept{
	sv.changeToClosingRncRnUa();
	}

void	StateVar::ClosingRncRnPncPnUa::
notifyMsgPipeResourceChangeDone(	ContextApi&	context,
									StateVar&	sv
									) const noexcept{
	// ignore
	}

void	StateVar::ClosingRncRnPncPnUa::
cancelNotifyMsgPipeResourceChangeDone(	ContextApi&	context,
										StateVar&	sv
										) const noexcept{
	sv.changeToClosingPncPnUa();
	}

void	StateVar::ClosingReadFullIrpPncPnPaUa::
notifyPortChangeDone(	ContextApi&	context,
						StateVar&	sv
						) const noexcept{
	// ignore
	}

void	StateVar::ClosingReadFullIrpPncPnPaUa::
cancelNotifyPortChangeDone(	ContextApi&	context,
							StateVar&	sv
							) const noexcept{
	context.requestMsgPipeIrpCancel();
	sv.changeToClosingReadFullIrpcIrpPaUa();
	}

void	StateVar::ClosingReadFullIrpPncPnPaUa::
msgPipeReadIrpDone(	ContextApi&	context,
					StateVar&	sv
					) const noexcept{
	sv.changeToClosingPncPnPaUa();
	}

void	StateVar::ClosingReadFullIrpcIrpPaUa::
msgPipeIrpCancelDone(	ContextApi&	context,
						StateVar&	sv
						) const noexcept{
	context.requestHcdMsgPipeFree();
	sv.changeToClosingFreePipeHcUa();
	}

void	StateVar::ClosingReadFullIrpcIrpPaUa::
msgPipeReadIrpDone(	ContextApi&	context,
					StateVar&	sv
					) const noexcept{
	// ignore
	}

void	StateVar::ClosingPncPnPaUa::
notifyPortChangeDone(	ContextApi&	context,
						StateVar&	sv
						) const noexcept{
	// ignore
	}

void	StateVar::ClosingPncPnPaUa::
cancelNotifyPortChangeDone(	ContextApi&	context,
							StateVar&	sv
							) const noexcept{
	context.requestHcdMsgPipeFree();
	sv.changeToClosingFreePipeHcUa();
	}

//
void	StateVar::PowerWait::
close(	ContextApi&	context,
		StateVar&	sv
		) const noexcept{
	context.cancelTimer();
	sv.changeToClosingPccPc();
	}

void	StateVar::PowerWait::
timerExpired(	ContextApi&	context,
				StateVar&	sv
				) const noexcept{
	context.requestSetPower();
	sv.changeToDisabledPc();
	}

//
void	StateVar::DisabledPc::
close(	ContextApi&	context,
		StateVar&	sv
		) const noexcept{
	context.requestCancelReq();
	sv.changeToClosingPccPc();
	}

void	StateVar::DisabledPc::
setPowerDone(	ContextApi&	context,
				StateVar&	sv
				) const noexcept{
//	Oscl::Mt::Thread::sleep(600);
	context.requestNotifyPortChange();
	sv.changeToDisabledPn();
	}

void	StateVar::DisabledPc::
requestFailed(	ContextApi&	context,
				StateVar&	sv
				) const noexcept{
	sv.changeToClosing();
	}

void	StateVar::DisabledPn::
close(	ContextApi&	context,
		StateVar&	sv
		) const noexcept{
	context.requestCancelNotifyPortChange();
	sv.changeToClosingPncPn();
	}

void	StateVar::DisabledPn::
notifyPortChangeDone(	ContextApi&	context,
						StateVar&	sv
						) const noexcept{
	context.parseChange();
	}

void	StateVar::DisabledPn::
connectChanged(	ContextApi&	context,
				StateVar&	sv
				) const noexcept{
	context.requestClrConnectChange();
	sv.changeToDisabledClrConnChangePc();
	}

void	StateVar::DisabledPn::
portDisabled(	ContextApi&	context,
				StateVar&	sv
				) const noexcept{
	context.requestClrEnableChange();
	sv.changeToDisabledClrEnableChangePc();
	}

void	StateVar::DisabledPn::
portResetDone(	ContextApi&	context,
				StateVar&	sv
				) const noexcept{
	context.requestClrResetChange();
	sv.changeToDisabledClrResetChangePc();
	}

void	StateVar::DisabledPn::
portOverCurrentChange(	ContextApi&	context,
						StateVar&	sv
						) const noexcept{
	while(true);	// FIXME: not implemented yet
	}

void	StateVar::DisabledClrEnableChangePc::
close(	ContextApi&	context,
		StateVar&	sv
		) const noexcept{
	context.requestCancelReq();
	sv.changeToClosingPccPc();
	}

void	StateVar::DisabledClrEnableChangePc::
clrEnableChangeDone(	ContextApi&	context,
						StateVar&	sv
						) const noexcept{
	context.requestNotifyPortChange();
	sv.changeToDisabledPn();
	}

void	StateVar::DisabledClrEnableChangePc::
requestFailed(	ContextApi&	context,
				StateVar&	sv
				) const noexcept{
	sv.changeToClosing();
	}

void	StateVar::DisabledClrResetChangePc::
close(	ContextApi&	context,
		StateVar&	sv
		) const noexcept{
	context.requestCancelReq();
	sv.changeToClosingPccPc();
	}

void	StateVar::DisabledClrResetChangePc::
clrResetChangeDone(	ContextApi&	context,
					StateVar&	sv
					) const noexcept{
	context.requestNotifyPortChange();
	sv.changeToDisabledPn();
	}

void	StateVar::DisabledClrResetChangePc::
requestFailed(	ContextApi&	context,
				StateVar&	sv
				) const noexcept{
	sv.changeToClosing();
	}

void	StateVar::DisabledClrConnChangePc::
close(	ContextApi&	context,
		StateVar&	sv
		) const noexcept{
	context.requestCancelReq();
	sv.changeToClosingPccPc();
	}

void	StateVar::DisabledClrConnChangePc::
clrConnectChangeDone(	ContextApi&	context,
						StateVar&	sv
						) const noexcept{
	context.requestGetPortStatus();
	sv.changeToDisabledGetStatusPc();
	}

void	StateVar::DisabledClrConnChangePc::
requestFailed(	ContextApi&	context,
				StateVar&	sv
				) const noexcept{
	sv.changeToClosing();
	}

void	StateVar::DisabledGetStatusPc::
close(	ContextApi&	context,
		StateVar&	sv
		) const noexcept{
	context.requestCancelReq();
	sv.changeToClosingPccPc();
	}

void	StateVar::DisabledGetStatusPc::
notifyPortChangeDone(	ContextApi&	context,
						StateVar&	sv
						) const noexcept{
	context.parseChange();
	}

void	StateVar::DisabledGetStatusPc::
getPortStatusDone(	ContextApi&	context,
					StateVar&	sv
					) const noexcept{
	if(context.deviceConnected()){
		context.requestHcdAllocUsbAddr();
		sv.changeToConnAllocUsbAddrHc();
		}
	else{
		context.requestNotifyPortChange();
		sv.changeToDisabledPn();
		}
	}

void	StateVar::DisabledGetStatusPc::
requestFailed(	ContextApi&	context,
				StateVar&	sv
				) const noexcept{
	sv.changeToClosing();
	}

void	StateVar::ConnAllocUsbAddrHc::
close(	ContextApi&	context,
		StateVar&	sv
		) const noexcept{
	sv.changeToClosingAllocUsbAddrHc();
	}

void	StateVar::ConnAllocUsbAddrHc::
hcdAllocUsbAddrDone(	ContextApi&	context,
						StateVar&	sv
						) const noexcept{
	if(context.usbAddrAllocated()){
		context.requestHcdLockAddressZero();
		sv.changeToConnLockAddrZeroUa();
		}
	else{
		context.requestNotifyUsbAddrResourceChange();
		context.requestNotifyPortChange();
		sv.changeToConnWaitUsbAddrUnPn();
		}
	}

void	StateVar::ConnLockAddrZeroUa::
close(	ContextApi&	context,
		StateVar&	sv
		) const noexcept{
	context.requestHcdCancelLockAddrZero();
	sv.changeToClosingHczcHczUa();
	}

void	StateVar::ConnLockAddrZeroUa::
hcdLockAddressZeroDone(	ContextApi&	context,
						StateVar&	sv
						) const noexcept{
	context.requestSetReset();
	sv.changeToConnSetResetPcAzUa();
	}

void	StateVar::ConnSetResetPcAzUa::
close(	ContextApi&	context,
		StateVar&	sv
		) const noexcept{
	context.requestCancelReq();
	sv.changeToClosingSetResetPccPcAzUa();
	}

void	StateVar::ConnSetResetPcAzUa::
setResetDone(	ContextApi&	context,
				StateVar&	sv
				) const noexcept{
	context.requestNotifyPortChange();
	sv.changeToConnWaitResetPnAzUa();
	}

void	StateVar::ConnSetResetPcAzUa::
requestFailed(	ContextApi&	context,
				StateVar&	sv
				) const noexcept{
	sv.changeToClosing();
	}

void	StateVar::ConnWaitResetPnAzUa::
close(	ContextApi&	context,
		StateVar&	sv
		) const noexcept{
	context.requestCancelNotifyPortChange();
	sv.changeToClosingPncPnAzUa();
	}

void	StateVar::ConnWaitResetPnAzUa::
notifyPortChangeDone(	ContextApi&	context,
						StateVar&	sv
						) const noexcept{
	context.parseChange();
	}

void	StateVar::ConnWaitResetPnAzUa::
connectChanged(	ContextApi&	context,
				StateVar&	sv
				) const noexcept{
	context.requestHcdUnlockAddressZero();
	sv.changeToDiscUnlockAddrZeroHcUa();
	}

void	StateVar::ConnWaitResetPnAzUa::
portDisabled(	ContextApi&	context,
				StateVar&	sv
				) const noexcept{
	context.requestHcdUnlockAddressZero();
	sv.changeToDiscUnlockAddrZeroHcUa();
	}

void	StateVar::ConnWaitResetPnAzUa::
portResetDone(	ContextApi&	context,
				StateVar&	sv
				) const noexcept{
	context.requestClrResetChange();
	sv.changeToConnClrResetChangePcAzUa();
	}

void	StateVar::ConnClrResetChangePcAzUa::
close(	ContextApi&	context,
		StateVar&	sv
		) const noexcept{
	context.requestCancelReq();
	sv.changeToClosingClrResetChangePccPcAzUa();
	}

void	StateVar::ConnClrResetChangePcAzUa::
clrResetChangeDone(	ContextApi&	context,
					StateVar&	sv
					) const noexcept{
	context.startResetTimer();
	sv.changeToConnResetWaitTwAzUa();
	}

void	StateVar::ConnClrResetChangePcAzUa::
requestFailed(	ContextApi&	context,
				StateVar&	sv
				) const noexcept{
	sv.changeToClosing();
	}

//
void	StateVar::ConnResetWaitTwAzUa::
close(	ContextApi&	context,
		StateVar&	sv
		) const noexcept{
	context.cancelTimer();
	sv.changeToClosingResetWaitTwcTwAzUa();
	}

void	StateVar::ConnResetWaitTwAzUa::
timerExpired(	ContextApi&	context,
				StateVar&	sv
				) const noexcept{
	context.requestNotifyPortChange();
	context.requestMsgPipeReadPartIrp();
	sv.changeToConnReadDescIrpPnAzUa();
	}
//
void	StateVar::ConnWaitUsbAddrUnPn::
close(	ContextApi&	context,
		StateVar&	sv
		) const noexcept{
	context.requestCancelNotifyPortChange();
	context.requestCancelNotifyUsbAddrResChange();
	}

void	StateVar::ConnWaitUsbAddrUnPn::
notifyPortChangeDone(	ContextApi&	context,
						StateVar&	sv
						) const noexcept{
	context.parseChange();
	}

void	StateVar::ConnWaitUsbAddrUnPn::
notifyUsbAddrResChangeDone(	ContextApi&	context,
							StateVar&	sv
							) const noexcept{
	context.requestHcdAllocUsbAddr();
	sv.changeToConnAllocUsbAddrHc();
	}

void	StateVar::ConnWaitUsbAddrUnPn::
connectChanged(	ContextApi&	context,
				StateVar&	sv
				) const noexcept{
	context.requestCancelNotifyUsbAddrResChange();
	sv.changeToDiscUncUn();
	}

void	StateVar::ConnWaitUsbAddrUnPn::
portDisabled(	ContextApi&	context,
				StateVar&	sv
				) const noexcept{
	context.requestCancelNotifyUsbAddrResChange();
	sv.changeToDiscUncUn();
	}

void	StateVar::ConnReadDescIrpPnAzUa::
close(	ContextApi&	context,
		StateVar&	sv
		) const noexcept{
	context.requestCancelNotifyPortChange();
	sv.changeToClosingReadDescIrpPncPnAzUa();
	}

void	StateVar::ConnReadDescIrpPnAzUa::
notifyPortChangeDone(	ContextApi&	context,
						StateVar&	sv
						) const noexcept{
	context.parseChange();
	}

void	StateVar::ConnReadDescIrpPnAzUa::
msgPipeReadIrpDone(	ContextApi&	context,
					StateVar&	sv
					) const noexcept{
	if(context.irpReadError()){
		context.startRetryTimer();
		sv.changeToConnReadDescErrorPnAzUa();
		}
	else{
		context.requestMsgPipeNoDataIrp();
		sv.changeToConnSetAddrIrpPnAzUa();
		}
	}

void	StateVar::ConnReadDescIrpPnAzUa::
connectChanged(	ContextApi&	context,
				StateVar&	sv
				) const noexcept{
	context.requestMsgPipeIrpCancel();
	sv.changeToDiscReadDescIrpcIrpAzUa();
	}

void	StateVar::ConnReadDescIrpPnAzUa::
portDisabled(	ContextApi&	context,
				StateVar&	sv
				) const noexcept{
	context.requestMsgPipeIrpCancel();
	sv.changeToDiscReadDescIrpcIrpAzUa();
	}

void	StateVar::ConnReadDescIrpPnAzUa::
portResumed(	ContextApi&	context,
				StateVar&	sv
				) const noexcept{
	while(true); // FIXME: not implemented yet.
	}

void	StateVar::ConnReadDescErrorPnAzUa::
close(	ContextApi&	context,
		StateVar&	sv
		) const noexcept{
	context.cancelTimer();
	context.requestCancelNotifyPortChange();
	sv.changeToClosingTimerCancelPncPnAzUa();
	}

void	StateVar::ConnReadDescErrorPnAzUa::
notifyPortChangeDone(	ContextApi&	context,
						StateVar&	sv
						) const noexcept{
	context.parseChange();
	}

void	StateVar::ConnReadDescErrorPnAzUa::
connectChanged(	ContextApi&	context,
				StateVar&	sv
				) const noexcept{
	context.cancelTimer();
	sv.changeToDiscTimerCancelAzUa();
	}

void	StateVar::ConnReadDescErrorPnAzUa::
portDisabled(	ContextApi&	context,
				StateVar&	sv
				) const noexcept{
	context.cancelTimer();
	sv.changeToDiscTimerCancelAzUa();
	}

void	StateVar::ConnReadDescErrorPnAzUa::
portResumed(	ContextApi&	context,
				StateVar&	sv
				) const noexcept{
	while(true); // FIXME: not implemented yet.
	}

void	StateVar::ConnReadDescErrorPnAzUa::
timerExpired(	ContextApi&	context,
				StateVar&	sv
				) const noexcept{
	context.requestMsgPipeReadPartIrp();
	sv.changeToConnReadDescIrpPnAzUa();
	}

void	StateVar::ConnSetAddrIrpPnAzUa::
close(	ContextApi&	context,
		StateVar&	sv
		) const noexcept{
	context.requestCancelNotifyPortChange();
	sv.changeToClosingSetAddrIrpPncPnAzUa();
	}

void	StateVar::ConnSetAddrIrpPnAzUa::
notifyPortChangeDone(	ContextApi&	context,
						StateVar&	sv
						) const noexcept{
	context.parseChange();
	}

void	StateVar::ConnSetAddrIrpPnAzUa::
msgPipeNoDataIrpDone(	ContextApi&	context,
						StateVar&	sv
						) const noexcept{
	if(context.irpNoDataError()){
		context.startRetryTimer();
		sv.changeToConnSetAddrErrorPnAzUa();
		}
	else{
		context.requestHcdUnlockAddressZero();
		sv.changeToConnUnlockAddrZeroHcPnUa();
		}
	}

void	StateVar::ConnSetAddrIrpPnAzUa::
connectChanged(	ContextApi&	context,
				StateVar&	sv
				) const noexcept{
	context.requestMsgPipeIrpCancel();
	sv.changeToDiscSetAddrIrpcIrpAzUa();
	}

void	StateVar::ConnSetAddrIrpPnAzUa::
portDisabled(	ContextApi&	context,
				StateVar&	sv
				) const noexcept{
	context.requestMsgPipeIrpCancel();
	sv.changeToDiscSetAddrIrpcIrpAzUa();
	}

void	StateVar::ConnSetAddrErrorPnAzUa::
close(	ContextApi&	context,
		StateVar&	sv
		) const noexcept{
	context.cancelTimer();
	context.requestCancelNotifyPortChange();
	sv.changeToClosingTimerCancelPncPnAzUa();
	}

void	StateVar::ConnSetAddrErrorPnAzUa::
notifyPortChangeDone(	ContextApi&	context,
						StateVar&	sv
						) const noexcept{
	context.parseChange();
	}

void	StateVar::ConnSetAddrErrorPnAzUa::
connectChanged(	ContextApi&	context,
				StateVar&	sv
				) const noexcept{
	context.cancelTimer();
	sv.changeToDiscTimerCancelAzUa();
	}

void	StateVar::ConnSetAddrErrorPnAzUa::
portDisabled(	ContextApi&	context,
				StateVar&	sv
				) const noexcept{
	context.cancelTimer();
	sv.changeToDiscTimerCancelAzUa();
	}

void	StateVar::ConnSetAddrErrorPnAzUa::
timerExpired(	ContextApi&	context,
				StateVar&	sv
				) const noexcept{
	context.requestMsgPipeNoDataIrp();
	sv.changeToConnSetAddrIrpPnAzUa();
	}


void	StateVar::ConnUnlockAddrZeroHcPnUa::
close(	ContextApi&	context,
		StateVar&	sv
		) const noexcept{
	context.requestCancelNotifyPortChange();
	sv.changeToClosingUazPncPnUa();
	}

void	StateVar::ConnUnlockAddrZeroHcPnUa::
notifyPortChangeDone(	ContextApi&	context,
						StateVar&	sv
						) const noexcept{
	context.parseChange();
	}

void	StateVar::ConnUnlockAddrZeroHcPnUa::
hcdUnlockAddressZeroDone(	ContextApi&	context,
							StateVar&	sv
							) const noexcept{
	context.requestHcdMsgPipeAlloc();
	sv.changeToConnPipeAllocHcPnUa();
	}

void	StateVar::ConnUnlockAddrZeroHcPnUa::
connectChanged(	ContextApi&	context,
				StateVar&	sv
				) const noexcept{
	sv.changeToDiscUnlockAddrZeroHcUa();
	}

void	StateVar::ConnUnlockAddrZeroHcPnUa::
portDisabled(	ContextApi&	context,
				StateVar&	sv
				) const noexcept{
	sv.changeToDiscUnlockAddrZeroHcUa();
	}

void	StateVar::ConnPipeAllocHcPnUa::
close(	ContextApi&	context,
		StateVar&	sv
		) const noexcept{
	context.requestCancelNotifyPortChange();
	sv.changeToClosingPipeAllocHcPncPnUa();
	}

void	StateVar::ConnPipeAllocHcPnUa::
notifyPortChangeDone(	ContextApi&	context,
						StateVar&	sv
						) const noexcept{
	context.parseChange();
	}

void	StateVar::ConnPipeAllocHcPnUa::
hcdMsgPipeAllocDone(	ContextApi&	context,
						StateVar&	sv
						) const noexcept{
	if(context.pipeAllocated()){
		context.requestMsgPipeReadFullIrp();
		sv.changeToConnReadFullIrpPnPaUa();
		}
	else{
		context.requestNotifyMsgPipeResourceChange();
		sv.changeToRezRnPnUa();
		}
	}

void	StateVar::ConnPipeAllocHcPnUa::
connectChanged(	ContextApi&	context,
				StateVar&	sv
				) const noexcept{
	sv.changeToDiscPipeAllocHcUa();
	}

void	StateVar::ConnPipeAllocHcPnUa::
portDisabled(	ContextApi&	context,
				StateVar&	sv
				) const noexcept{
	sv.changeToDiscPipeAllocHcUa();
	}

void	StateVar::ConnReadFullIrpPnPaUa::
close(	ContextApi&	context,
		StateVar&	sv
		) const noexcept{
	context.requestCancelNotifyPortChange();
	sv.changeToClosingReadFullIrpPncPnPaUa();
	}

void	StateVar::ConnReadFullIrpPnPaUa::
notifyPortChangeDone(	ContextApi&	context,
						StateVar&	sv
						) const noexcept{
	context.parseChange();
	}

void	StateVar::ConnReadFullIrpPnPaUa::
msgPipeReadIrpDone(	ContextApi&	context,
					StateVar&	sv
					) const noexcept{
	if(context.irpReadError()){
		context.startRetryTimer();
		sv.changeToConnReadFullErrorPnPaUa();
		}
	else{
		context.advertiseNewPipe();
		sv.changeToReadyPnPaUa();
		}
	}

void	StateVar::ConnReadFullIrpPnPaUa::
connectChanged(	ContextApi&	context,
				StateVar&	sv
				) const noexcept{
	context.requestMsgPipeIrpCancel();
	sv.changeToDiscReadFullIrpcIrpPaUa();
	}

void	StateVar::ConnReadFullIrpPnPaUa::
portDisabled(	ContextApi&	context,
				StateVar&	sv
				) const noexcept{
	context.requestMsgPipeIrpCancel();
	sv.changeToDiscReadFullIrpcIrpPaUa();
	}

void	StateVar::ConnReadFullIrpPnPaUa::
portResumed(	ContextApi&	context,
				StateVar&	sv
				) const noexcept{
	while(true); // FIXME: not implemented yet.
	}

void	StateVar::ConnReadFullErrorPnPaUa::
close(	ContextApi&	context,
		StateVar&	sv
		) const noexcept{
	context.cancelTimer();
	context.requestCancelNotifyPortChange();
	sv.changeToClosingTimerCancelPncPnPaUa();
	}

void	StateVar::ConnReadFullErrorPnPaUa::
notifyPortChangeDone(	ContextApi&	context,
						StateVar&	sv
						) const noexcept{
	context.parseChange();
	}

void	StateVar::ConnReadFullErrorPnPaUa::
connectChanged(	ContextApi&	context,
				StateVar&	sv
				) const noexcept{
	context.cancelTimer();
	sv.changeToDiscTimerCancelPaUa();
	}

void	StateVar::ConnReadFullErrorPnPaUa::
portDisabled(	ContextApi&	context,
				StateVar&	sv
				) const noexcept{
	context.cancelTimer();
	sv.changeToDiscTimerCancelPaUa();
	}

void	StateVar::ConnReadFullErrorPnPaUa::
portResumed(	ContextApi&	context,
				StateVar&	sv
				) const noexcept{
	while(true); // FIXME: not implemented yet.
	}

void	StateVar::ConnReadFullErrorPnPaUa::
timerExpired(	ContextApi&	context,
				StateVar&	sv
				) const noexcept{
	context.requestMsgPipeReadFullIrp();
	sv.changeToConnReadFullIrpPnPaUa();
	}

void	StateVar::ReadyPnPaUa::
close(	ContextApi&	context,
		StateVar&	sv
		) const noexcept{
	context.requestCancelNotifyPortChange();
	sv.changeToClosingAdvPncPnPaUa();
	}

void	StateVar::ReadyPnPaUa::
notifyPortChangeDone(	ContextApi&	context,
						StateVar&	sv
						) const noexcept{
	context.parseChange();
	}

void	StateVar::ReadyPnPaUa::
connectChanged(	ContextApi&	context,
				StateVar&	sv
				) const noexcept{
	context.prepareToCloseDynSrv();
	sv.changeToDiscPrepareHcPaUa();
	}

void	StateVar::ReadyPnPaUa::
portDisabled(	ContextApi&	context,
				StateVar&	sv
				) const noexcept{
	context.prepareToCloseDynSrv();
	sv.changeToDiscPrepareHcPaUa();
	}

void	StateVar::ReadyPnPaUa::
portResumed(	ContextApi&	context,
				StateVar&	sv
				) const noexcept{
	while(true); // FIXME: not implemented yet
	}

void	StateVar::RezRnPnUa::
close(	ContextApi&	context,
		StateVar&	sv
		) const noexcept{
	context.requestCancelNotifyPortChange();
	context.requestCancelNotifyMsgPipeResChange();
	sv.changeToClosingRncRnPncPnUa();
	}

void	StateVar::RezRnPnUa::
notifyPortChangeDone(	ContextApi&	context,
						StateVar&	sv
						) const noexcept{
	context.parseChange();
	}

void	StateVar::RezRnPnUa::
notifyMsgPipeResourceChangeDone(	ContextApi&	context,
									StateVar&	sv
									) const noexcept{
	context.requestHcdMsgPipeAlloc();
	sv.changeToConnPipeAllocHcPnUa();
	}

void	StateVar::RezRnPnUa::
connectChanged(	ContextApi&	context,
				StateVar&	sv
				) const noexcept{
	context.requestCancelNotifyMsgPipeResChange();
	sv.changeToDiscRncRnUa();
	}

void	StateVar::RezRnPnUa::
portDisabled(	ContextApi&	context,
				StateVar&	sv
				) const noexcept{
	context.requestCancelNotifyMsgPipeResChange();
	sv.changeToDiscRncRnUa();
	}

void	StateVar::DiscUncUn::
close(	ContextApi&	context,
		StateVar&	sv
		) const noexcept{
	sv.changeToClosingUncUn();
	}

void	StateVar::DiscUncUn::
notifyUsbAddrResChangeDone(	ContextApi&	context,
							StateVar&	sv
							) const noexcept{
	// ignore
	}

void	StateVar::DiscUncUn::
cancelNotifyUsbAddrResChangeDone(	ContextApi&	context,
									StateVar&	sv
									) const noexcept{
	context.requestNotifyPortChange();
	sv.changeToDisabledPn();
	}

void	StateVar::DiscUnlockAddrZeroHcUa::
close(	ContextApi&	context,
		StateVar&	sv
		) const noexcept{
	sv.changeToClosingUnlockAddrZeroHcUa();
	}

void	StateVar::DiscUnlockAddrZeroHcUa::
hcdUnlockAddressZeroDone(	ContextApi&	context,
							StateVar&	sv
							) const noexcept{
	context.requestHcdFreeUsbAddr();
	sv.changeToDiscFreeUsbAddrHc();
	}

void	StateVar::DiscFreeUsbAddrHc::
close(	ContextApi&	context,
		StateVar&	sv
		) const noexcept{
	sv.changeToClosingFreeUsbAddrHc();
	}

void	StateVar::DiscFreeUsbAddrHc::
hcdFreeUsbAddrDone(	ContextApi&	context,
					StateVar&	sv
					) const noexcept{
	context.requestNotifyPortChange();
	sv.changeToDisabledPn();
	}

void	StateVar::DiscTimerCancelAzUa::
close(	ContextApi&	context,
		StateVar&	sv
		) const noexcept{
	sv.changeToClosingTimerCancelAzUa();
	}

void	StateVar::DiscTimerCancelAzUa::
timerExpired(	ContextApi&	context,
				StateVar&	sv
				) const noexcept{
	// ignore
	}

void	StateVar::DiscTimerCancelAzUa::
timerCanceled(	ContextApi&	context,
				StateVar&	sv
				) const noexcept{
	context.requestHcdUnlockAddressZero();
	sv.changeToDiscUnlockAddrZeroHcUa();
	}

void	StateVar::DiscTimerCancelPaUa::
close(	ContextApi&	context,
		StateVar&	sv
		) const noexcept{
	sv.changeToClosingTimerCancelPaUa();
	}

void	StateVar::DiscTimerCancelPaUa::
timerExpired(	ContextApi&	context,
				StateVar&	sv
				) const noexcept{
	// ignore
	}

void	StateVar::DiscTimerCancelPaUa::
timerCanceled(	ContextApi&	context,
				StateVar&	sv
				) const noexcept{
	context.requestHcdMsgPipeFree();
	sv.changeToDiscFreePipeHcUa();
	}

void	StateVar::DiscReadDescIrpcIrpAzUa::
close(	ContextApi&	context,
		StateVar&	sv
		) const noexcept{
	sv.changeToClosingReadDescIrpcIrpAzUa();
	}

void	StateVar::DiscReadDescIrpcIrpAzUa::
msgPipeIrpCancelDone(	ContextApi&	context,
						StateVar&	sv
						) const noexcept{
	context.requestHcdUnlockAddressZero();
	sv.changeToDiscUnlockAddrZeroHcUa();
	}

void	StateVar::DiscReadDescIrpcIrpAzUa::
msgPipeReadIrpDone(	ContextApi&	context,
					StateVar&	sv
					) const noexcept{
	// ignore
	}

void	StateVar::DiscSetAddrIrpcIrpAzUa::
close(	ContextApi&	context,
		StateVar&	sv
		) const noexcept{
	sv.changeToClosingSetAddrIrpcIrpAzUa();
	}

void	StateVar::DiscSetAddrIrpcIrpAzUa::
msgPipeIrpCancelDone(	ContextApi&	context,
						StateVar&	sv
						) const noexcept{
	context.requestMsgPipeIrpCancel();
	sv.changeToDiscUnlockAddrZeroHcUa();
	}

void	StateVar::DiscSetAddrIrpcIrpAzUa::
msgPipeNoDataIrpDone(	ContextApi&	context,
						StateVar&	sv
						) const noexcept{
	// ignore
	}

void	StateVar::DiscPrepareHcPaUa::
close(	ContextApi&	context,
		StateVar&	sv
		) const noexcept{
	sv.changeToClosingPrepareHcPaUa();
	}

void	StateVar::DiscPrepareHcPaUa::
prepareToCloseDynSrvDone(	ContextApi&	context,
							StateVar&	sv
							) const noexcept{
	context.closeDynSrv();
	sv.changeToDiscDynSrvHcPaUa();
	}

void	StateVar::DiscDynSrvHcPaUa::
close(	ContextApi&	context,
		StateVar&	sv
		) const noexcept{
	sv.changeToClosingDynSrvHcPaUa();
	}

void	StateVar::DiscDynSrvHcPaUa::
closeDynSrvDone(	ContextApi&	context,
					StateVar&	sv
					) const noexcept{
	context.requestHcdMsgPipeFree();
	sv.changeToDiscFreePipeHcUa();
	}

void	StateVar::DiscFreePipeHcUa::
close(	ContextApi&	context,
		StateVar&	sv
		) const noexcept{
	sv.changeToClosingFreePipeHcUa();
	}

void	StateVar::DiscFreePipeHcUa::
hcdMsgPipeFreeDone(	ContextApi&	context,
					StateVar&	sv
					) const noexcept{
	context.requestHcdFreeUsbAddr();
	sv.changeToDiscFreeUsbAddrHc();
	}

void	StateVar::DiscPipeAllocHcUa::
close(	ContextApi&	context,
		StateVar&	sv
		) const noexcept{
	sv.changeToClosingPipeAllocHcUa();
	}

void	StateVar::DiscPipeAllocHcUa::
hcdMsgPipeAllocDone(	ContextApi&	context,
						StateVar&	sv
						) const noexcept{
	if(context.pipeAllocated()){
		context.requestHcdMsgPipeFree();
		sv.changeToDiscFreePipeHcUa();
		}
	else{
		context.requestHcdFreeUsbAddr();
		sv.changeToDiscFreeUsbAddrHc();
		}
	}

void	StateVar::DiscRncRnUa::
close(	ContextApi&	context,
		StateVar&	sv
		) const noexcept{
	sv.changeToClosingRncRnUa();
	}

void	StateVar::DiscRncRnUa::
notifyMsgPipeResourceChangeDone(	ContextApi&	context,
									StateVar&	sv
									) const noexcept{
	// ignore
	}

void	StateVar::DiscRncRnUa::
cancelNotifyMsgPipeResourceChangeDone(	ContextApi&	context,
										StateVar&	sv
										) const noexcept{
	context.requestHcdFreeUsbAddr();
	sv.changeToDiscFreeUsbAddrHc();
	}

void	StateVar::DiscReadFullIrpcIrpPaUa::
close(	ContextApi&	context,
		StateVar&	sv
		) const noexcept{
	sv.changeToClosingReadFullIrpcIrpPaUa();
	}

void	StateVar::DiscReadFullIrpcIrpPaUa::
msgPipeIrpCancelDone(	ContextApi&	context,
						StateVar&	sv
						) const noexcept{
	context.requestHcdMsgPipeFree();
	sv.changeToDiscFreePipeHcUa();
	}

void	StateVar::DiscReadFullIrpcIrpPaUa::
msgPipeReadIrpDone(	ContextApi&	context,
					StateVar&	sv
					) const noexcept{
	// ignore
	}

