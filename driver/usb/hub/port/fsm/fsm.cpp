/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "fsm.h"
#include "oscl/error/fatal.h"

using namespace Oscl::Usb::Hub::Port;

static const StateVar::Closed						closed;
static const StateVar::Closing						closing;
static const StateVar::ClosingPccPc					closingPccPc;
static const StateVar::ClosingPncPn					closingPncPn;
static const StateVar::ClosingAllocUsbAddrHc		closingAllocUsbAddrHc;
static const StateVar::ClosingFreeUsbAddrHc			closingFreeUsbAddrHc;
static const StateVar::ClosingHczcHczUa				closingHczcHczUa;
static const StateVar::ClosingHczcUa				closingHczcUa;
static const StateVar::ClosingHczcAzUa				closingHczcAzUa;
static const StateVar::ClosingUnlockAddrZeroHcUa	closingUnlockAddrZeroHcUa;
static const StateVar::ClosingSetResetPccPcAzUa		closingSetResetPccPcAzUa;
static const StateVar::ClosingTimerCancelPncPnAzUa	closingTimerCancelPncPnAzUa;
static const StateVar::ClosingTimerCancelPncPnPaUa	closingTimerCancelPncPnPaUa;
static const StateVar::ClosingTimerCancelPaUa		closingTimerCancelPaUa;
static const StateVar::ClosingTimerCancelAzUa		closingTimerCancelAzUa;
static const StateVar::ClosingPncPnAzUa				closingPncPnAzUa;
static const StateVar::ClosingClrResetChangePccPcAzUa
				closingClrResetChangePccPcAzUa;
static const StateVar::ClosingResetWaitTwcTwAzUa	closingResetWaitTwcTwAzUa;
static const StateVar::ClosingUncUnPncPn			closingUncUnPncPn;
static const StateVar::ClosingUncUn					closingUncUn;
static const StateVar::ClosingReadDescIrpPncPnAzUa	closingReadDescIrpPncPnAzUa;
static const StateVar::ClosingReadDescIrpcIrpAzUa	closingReadDescIrpcIrpAzUa;
static const StateVar::ClosingSetAddrIrpPncPnAzUa	closingSetAddrIrpPncPnAzUa;
static const StateVar::ClosingSetAddrIrpcIrpAzUa	closingSetAddrIrpcIrpAzUa;
static const StateVar::ClosingUazPncPnUa			closingUazPncPnUa;
static const StateVar::ClosingUazUa					closingUazUa;
static const StateVar::ClosingPncPnUa				closingPncPnUa;
static const StateVar::ClosingPipeAllocHcPncPnUa	closingPipeAllocHcPncPnUa;
static const StateVar::ClosingPipeAllocHcUa			closingPipeAllocHcUa;
static const StateVar::ClosingFreePipeHcUa			closingFreePipeHcUa;
static const StateVar::ClosingFreePipeHcPncPnUa		closingFreePipeHcPncPnUa;
static const StateVar::ClosingAdvPncPnPaUa			closingAdvPncPnPaUa;
static const StateVar::ClosingPrepareHcPaUa			closingPrepareHcPaUa;
static const StateVar::ClosingDynSrvHcPaUa			closingDynSrvHcPaUa;
static const StateVar::ClosingRncRnUa				closingRncRnUa;
static const StateVar::ClosingRncRnPncPnUa			closingRncRnPncPnUa;
static const StateVar::DisabledPc					disabledPc;
static const StateVar::PowerWait					powerWait;
static const StateVar::DisabledPn					disabledPn;
static const StateVar::DisabledClrConnChangePc		disabledClrConnChangePc;
static const StateVar::DisabledGetStatusPc			disabledGetStatusPc;
static const StateVar::DisabledClrEnableChangePc	disabledClrEnableChangePc;
static const StateVar::DisabledClrResetChangePc		disabledClrResetChangePc;
static const StateVar::ConnAllocUsbAddrHc			connAllocUsbAddrHc;
static const StateVar::ConnLockAddrZeroUa			connLockAddrZeroUa;
static const StateVar::ConnSetResetPcAzUa			connSetResetPcAzUa;
static const StateVar::ConnWaitResetPnAzUa			connWaitResetPnAzUa;
static const StateVar::ConnClrResetChangePcAzUa		connClrResetChangePcAzUa;
static const StateVar::ConnResetWaitTwAzUa			connResetWaitTwAzUa;
static const StateVar::ConnWaitUsbAddrUnPn			connWaitUsbAddrUnPn;
static const StateVar::ConnReadDescIrpPnAzUa		connReadDescIrpPnAzUa;
static const StateVar::ConnReadDescErrorPnAzUa		connReadDescErrorPnAzUa;
static const StateVar::ConnSetAddrIrpPnAzUa			connSetAddrIrpPnAzUa;
static const StateVar::ConnSetAddrErrorPnAzUa		connSetAddrErrorPnAzUa;
static const StateVar::ConnUnlockAddrZeroHcPnUa		connUnlockAddrZeroHcPnUa;
static const StateVar::ConnPipeAllocHcPnUa			connPipeAllocHcPnUa;
static const StateVar::ReadyPnPaUa					readyPnPaUa;
static const StateVar::RezRnPnUa					rezRnPnUa;
static const StateVar::DiscUncUn					discUncUn;
static const StateVar::DiscUnlockAddrZeroHcUa		discUnlockAddrZeroHcUa;
static const StateVar::DiscFreeUsbAddrHc			discFreeUsbAddrHc;
static const StateVar::DiscTimerCancelAzUa			discTimerCancelAzUa;
static const StateVar::DiscTimerCancelPaUa			discTimerCancelPaUa;
static const StateVar::DiscReadDescIrpcIrpAzUa		discReadDescIrpcIrpAzUa;
static const StateVar::DiscSetAddrIrpcIrpAzUa		discSetAddrIrpcIrpAzUa;
static const StateVar::DiscPrepareHcPaUa			discPrepareHcPaUa;
static const StateVar::DiscDynSrvHcPaUa				discDynSrvHcPaUa;
static const StateVar::DiscFreePipeHcUa				discFreePipeHcUa;
static const StateVar::DiscPipeAllocHcUa			discPipeAllocHcUa;
static const StateVar::DiscRncRnUa					discRncRnUa;
static const StateVar::ClosingReadFullIrpPncPnPaUa	closingReadFullIrpPncPnPaUa;
static const StateVar::ClosingReadFullIrpcIrpPaUa	closingReadFullIrpcIrpPaUa;
static const StateVar::ClosingPncPnPaUa				closingPncPnPaUa;
static const StateVar::ConnReadFullIrpPnPaUa		connReadFullIrpPnPaUa;
static const StateVar::ConnReadFullErrorPnPaUa		connReadFullErrorPnPaUa;
static const StateVar::DiscReadFullIrpcIrpPaUa		discReadFullIrpcIrpPaUa;

// StateVar

StateVar::StateVar(ContextApi& context) noexcept:
		_context(context),
		_state(&closed)
		{
	for(unsigned i=0;i<nTraceRecs;++i){
		_freeTraceRecs.put(&_traceRec[i]);
		}
	}

void	StateVar::open() noexcept{
	_state->open(_context,*this);
	}

void	StateVar::close() noexcept{
	_state->close(_context,*this);
	}

void	StateVar::setResetDone() noexcept{
	_state->setResetDone(_context,*this);
	}

void	StateVar::setSuspendDone() noexcept{
	_state->setSuspendDone(_context,*this);
	}

void	StateVar::setPowerDone() noexcept{
	_state->setPowerDone(_context,*this);
	}

void	StateVar::clrEnableDone() noexcept{
	_state->clrEnableDone(_context,*this);
	}

void	StateVar::clrSuspendDone() noexcept{
	_state->clrSuspendDone(_context,*this);
	}

void	StateVar::clrPowerDone() noexcept{
	_state->clrPowerDone(_context,*this);
	}

void	StateVar::clrConnectChangeDone() noexcept{
	_state->clrConnectChangeDone(_context,*this);
	}

void	StateVar::clrResetChangeDone() noexcept{
	_state->clrResetChangeDone(_context,*this);
	}

void	StateVar::clrEnableChangeDone() noexcept{
	_state->clrEnableChangeDone(_context,*this);
	}

void	StateVar::clrSuspendChangeDone() noexcept{
	_state->clrSuspendChangeDone(_context,*this);
	}

void	StateVar::clrOverCurrentChangeDone() noexcept{
	_state->clrOverCurrentChangeDone(_context,*this);
	}

void	StateVar::cancelReqDone() noexcept{
	_state->cancelReqDone(_context,*this);
	}

void	StateVar::notifyPortChangeDone() noexcept{
	_state->notifyPortChangeDone(_context,*this);
	}

void	StateVar::cancelNotifyPortChangeDone() noexcept{
	_state->cancelNotifyPortChangeDone(_context,*this);
	}

void	StateVar::getPortStatusDone() noexcept{
	_state->getPortStatusDone(_context,*this);
	}

void	StateVar::requestFailed() noexcept{
	_state->requestFailed(_context,*this);
	}

void	StateVar::msgPipeIrpCancelDone() noexcept{
	_state->msgPipeIrpCancelDone(_context,*this);
	}

void	StateVar::msgPipeNoDataIrpDone() noexcept{
	_state->msgPipeNoDataIrpDone(_context,*this);
	}

void	StateVar::msgPipeReadIrpDone() noexcept{
	_state->msgPipeReadIrpDone(_context,*this);
	}

void	StateVar::msgPipeWriteIrpDone() noexcept{
	_state->msgPipeWriteIrpDone(_context,*this);
	}

void	StateVar::hcdMsgPipeAllocDone() noexcept{
	_state->hcdMsgPipeAllocDone(_context,*this);
	}

void	StateVar::hcdMsgPipeFreeDone() noexcept{
	_state->hcdMsgPipeFreeDone(_context,*this);
	}

void	StateVar::notifyMsgPipeResourceChangeDone() noexcept{
	_state->notifyMsgPipeResourceChangeDone(_context,*this);
	}

void	StateVar::cancelNotifyMsgPipeResourceChangeDone() noexcept{
	_state->cancelNotifyMsgPipeResourceChangeDone(_context,*this);
	}

void	StateVar::hcdLockAddressZeroDone() noexcept{
	_state->hcdLockAddressZeroDone(_context,*this);
	}

void	StateVar::hcdUnlockAddressZeroDone() noexcept{
	_state->hcdUnlockAddressZeroDone(_context,*this);
	}

void	StateVar::hcdCancelLockAddrZeroDone() noexcept{
	_state->hcdCancelLockAddrZeroDone(_context,*this);
	}

void	StateVar::hcdAllocUsbAddrDone() noexcept{
	_state->hcdAllocUsbAddrDone(_context,*this);
	}

void	StateVar::hcdFreeUsbAddrDone() noexcept{
	_state->hcdFreeUsbAddrDone(_context,*this);
	}

void	StateVar::prepareToCloseDynSrvDone() noexcept{
	_state->prepareToCloseDynSrvDone(_context,*this);
	}

void	StateVar::closeDynSrvDone() noexcept{
	_state->closeDynSrvDone(_context,*this);
	}

void	StateVar::notifyUsbAddrResChangeDone() noexcept{
	_state->notifyUsbAddrResChangeDone(_context,*this);
	}

void	StateVar::cancelNotifyUsbAddrResChangeDone() noexcept{
	_state->cancelNotifyUsbAddrResChangeDone(_context,*this);
	}

void	StateVar::timerExpired() noexcept{
	_state->timerExpired(_context,*this);
	}

void	StateVar::timerCanceled() noexcept{
	_state->timerCanceled(_context,*this);
	}

void	StateVar::connectChanged() noexcept{
	_state->connectChanged(_context,*this);
	}

void	StateVar::portDisabled() noexcept{
	_state->portDisabled(_context,*this);
	}

void	StateVar::portResumed() noexcept{
	_state->portResumed(_context,*this);
	}

void	StateVar::portOverCurrentChange() noexcept{
	_state->portOverCurrentChange(_context,*this);
	}

void	StateVar::portResetDone() noexcept{
	_state->portResetDone(_context,*this);
	}

// global actions
void	StateVar::protocolError(const char* state) noexcept{
	ErrorFatal::logAndExit(state);
	}

// state change operations
void	StateVar::changeToClosed() noexcept{
	trace(closed);
	_state	= &closed;
	}

void	StateVar::changeToClosing() noexcept{
	trace(closing);
	_state	= &closing;
	}

void	StateVar::changeToClosingPccPc() noexcept{
	trace(closingPccPc);
	_state	= &closingPccPc;
	}

void	StateVar::changeToClosingPncPn() noexcept{
	trace(closingPncPn);
	_state	= &closingPncPn;
	}

void	StateVar::changeToClosingAllocUsbAddrHc() noexcept{
	trace(closingAllocUsbAddrHc);
	_state	= &closingAllocUsbAddrHc;
	}

void	StateVar::changeToClosingFreeUsbAddrHc() noexcept{
	trace(closingFreeUsbAddrHc);
	_state	= &closingFreeUsbAddrHc;
	}

void	StateVar::changeToClosingHczcHczUa() noexcept{
	trace(closingHczcHczUa);
	_state	= &closingHczcHczUa;
	}

void	StateVar::changeToClosingHczcUa() noexcept{
	trace(closingHczcUa);
	_state	= &closingHczcUa;
	}

void	StateVar::changeToClosingHczcAzUa() noexcept{
	trace(closingHczcAzUa);
	_state	= &closingHczcAzUa;
	}

void	StateVar::changeToClosingUnlockAddrZeroHcUa() noexcept{
	trace(closingUnlockAddrZeroHcUa);
	_state	= &closingUnlockAddrZeroHcUa;
	}

void	StateVar::changeToClosingSetResetPccPcAzUa() noexcept{
	trace(closingSetResetPccPcAzUa);
	_state	= &closingSetResetPccPcAzUa;
	}

void	StateVar::changeToClosingTimerCancelPncPnAzUa() noexcept{
	trace(closingTimerCancelPncPnAzUa);
	_state	= &closingTimerCancelPncPnAzUa;
	}

void	StateVar::changeToClosingTimerCancelPncPnPaUa() noexcept{
	trace(closingTimerCancelPncPnPaUa);
	_state	= &closingTimerCancelPncPnPaUa;
	}

void	StateVar::changeToClosingTimerCancelPaUa() noexcept{
	trace(closingTimerCancelPaUa);
	_state	= &closingTimerCancelPaUa;
	}

void	StateVar::changeToClosingTimerCancelAzUa() noexcept{
	trace(closingTimerCancelAzUa);
	_state	= &closingTimerCancelAzUa;
	}

void	StateVar::changeToClosingPncPnAzUa() noexcept{
	trace(closingPncPnAzUa);
	_state	= &closingPncPnAzUa;
	}

void	StateVar::changeToClosingClrResetChangePccPcAzUa() noexcept{
	trace(closingClrResetChangePccPcAzUa);
	_state	= &closingClrResetChangePccPcAzUa;
	}

void	StateVar::changeToClosingResetWaitTwcTwAzUa() noexcept{
	trace(closingResetWaitTwcTwAzUa);
	_state	= &closingResetWaitTwcTwAzUa;
	}

void	StateVar::changeToClosingUncUnPncPn() noexcept{
	trace(closingUncUnPncPn);
	_state	= &closingUncUnPncPn;
	}

void	StateVar::changeToClosingUncUn() noexcept{
	trace(closingUncUn);
	_state	= &closingUncUn;
	}

void	StateVar::changeToClosingReadDescIrpPncPnAzUa() noexcept{
	trace(closingReadDescIrpPncPnAzUa);
	_state	= &closingReadDescIrpPncPnAzUa;
	}

void	StateVar::changeToClosingReadDescIrpcIrpAzUa() noexcept{
	trace(closingReadDescIrpcIrpAzUa);
	_state	= &closingReadDescIrpcIrpAzUa;
	}

void	StateVar::changeToClosingSetAddrIrpPncPnAzUa() noexcept{
	trace(closingSetAddrIrpPncPnAzUa);
	_state	= &closingSetAddrIrpPncPnAzUa;
	}

void	StateVar::changeToClosingSetAddrIrpcIrpAzUa() noexcept{
	trace(closingSetAddrIrpcIrpAzUa);
	_state	= &closingSetAddrIrpcIrpAzUa;
	}

void	StateVar::changeToClosingUazPncPnUa() noexcept{
	trace(closingUazPncPnUa);
	_state	= &closingUazPncPnUa;
	}

void	StateVar::changeToClosingUazUa() noexcept{
	trace(closingUazUa);
	_state	= &closingUazUa;
	}

void	StateVar::changeToClosingPncPnUa() noexcept{
	trace(closingPncPnUa);
	_state	= &closingPncPnUa;
	}

void	StateVar::changeToClosingPipeAllocHcPncPnUa() noexcept{
	trace(closingPipeAllocHcPncPnUa);
	_state	= &closingPipeAllocHcPncPnUa;
	}

void	StateVar::changeToClosingPipeAllocHcUa() noexcept{
	trace(closingPipeAllocHcUa);
	_state	= &closingPipeAllocHcUa;
	}

void	StateVar::changeToClosingFreePipeHcUa() noexcept{
	trace(closingFreePipeHcUa);
	_state	= &closingFreePipeHcUa;
	}

void	StateVar::changeToClosingFreePipeHcPncPnUa() noexcept{
	trace(closingFreePipeHcPncPnUa);
	_state	= &closingFreePipeHcPncPnUa;
	}

void	StateVar::changeToClosingAdvPncPnPaUa() noexcept{
	trace(closingAdvPncPnPaUa);
	_state	= &closingAdvPncPnPaUa;
	}

void	StateVar::changeToClosingPrepareHcPaUa() noexcept{
	trace(closingPrepareHcPaUa);
	_state	= &closingPrepareHcPaUa;
	}

void	StateVar::changeToClosingDynSrvHcPaUa() noexcept{
	trace(closingDynSrvHcPaUa);
	_state	= &closingDynSrvHcPaUa;
	}

void	StateVar::changeToClosingRncRnUa() noexcept{
	trace(closingRncRnUa);
	_state	= &closingRncRnUa;
	}

void	StateVar::changeToClosingRncRnPncPnUa() noexcept{
	trace(closingRncRnPncPnUa);
	_state	= &closingRncRnPncPnUa;
	}

void	StateVar::changeToPowerWait() noexcept{
	trace(powerWait);
	_state	= &powerWait;
	}

void	StateVar::changeToDisabledPc() noexcept{
	trace(disabledPc);
	_state	= &disabledPc;
	}

void	StateVar::changeToDisabledPn() noexcept{
	trace(disabledPn);
	_state	= &disabledPn;
	}

void	StateVar::changeToDisabledClrConnChangePc() noexcept{
	trace(disabledClrConnChangePc);
	_state	= &disabledClrConnChangePc;
	}

void	StateVar::changeToDisabledGetStatusPc() noexcept{
	trace(disabledGetStatusPc);
	_state	= &disabledGetStatusPc;
	}

void	StateVar::changeToDisabledClrEnableChangePc() noexcept{
	trace(disabledClrEnableChangePc);
	_state	= &disabledClrEnableChangePc;
	}

void	StateVar::changeToDisabledClrResetChangePc() noexcept{
	trace(disabledClrResetChangePc);
	_state	= &disabledClrResetChangePc;
	}

void	StateVar::changeToConnAllocUsbAddrHc() noexcept{
	trace(connAllocUsbAddrHc);
	_state	= &connAllocUsbAddrHc;
	}

void	StateVar::changeToConnLockAddrZeroUa() noexcept{
	trace(connLockAddrZeroUa);
	_state	= &connLockAddrZeroUa;
	}

void	StateVar::changeToConnSetResetPcAzUa() noexcept{
	trace(connSetResetPcAzUa);
	_state	= &connSetResetPcAzUa;
	}

void	StateVar::changeToConnWaitResetPnAzUa() noexcept{
	trace(connWaitResetPnAzUa);
	_state	= &connWaitResetPnAzUa;
	}

void	StateVar::changeToConnClrResetChangePcAzUa() noexcept{
	trace(connClrResetChangePcAzUa);
	_state	= &connClrResetChangePcAzUa;
	}

void	StateVar::changeToConnResetWaitTwAzUa() noexcept{
	trace(connResetWaitTwAzUa);
	_state	= &connResetWaitTwAzUa;
	}

void	StateVar::changeToConnWaitUsbAddrUnPn() noexcept{
	trace(connWaitUsbAddrUnPn);
	_state	= &connWaitUsbAddrUnPn;
	}

void	StateVar::changeToConnReadDescIrpPnAzUa() noexcept{
	trace(connReadDescIrpPnAzUa);
	_state	= &connReadDescIrpPnAzUa;
	}

void	StateVar::changeToConnReadDescErrorPnAzUa() noexcept{
	trace(connReadDescErrorPnAzUa);
	_state	= &connReadDescErrorPnAzUa;
	}

void	StateVar::changeToConnSetAddrIrpPnAzUa() noexcept{
	trace(connSetAddrIrpPnAzUa);
	_state	= &connSetAddrIrpPnAzUa;
	}

void	StateVar::changeToConnSetAddrErrorPnAzUa() noexcept{
	trace(connSetAddrErrorPnAzUa);
	_state	= &connSetAddrErrorPnAzUa;
	}

void	StateVar::changeToConnUnlockAddrZeroHcPnUa() noexcept{
	trace(connUnlockAddrZeroHcPnUa);
	_state	= &connUnlockAddrZeroHcPnUa;
	}

void	StateVar::changeToConnPipeAllocHcPnUa() noexcept{
	trace(connPipeAllocHcPnUa);
	_state	= &connPipeAllocHcPnUa;
	}

void	StateVar::changeToReadyPnPaUa() noexcept{
	trace(readyPnPaUa);
	_state	= &readyPnPaUa;
	}

void	StateVar::changeToRezRnPnUa() noexcept{
	trace(rezRnPnUa);
	_state	= &rezRnPnUa;
	}

void	StateVar::changeToDiscUncUn() noexcept{
	trace(discUncUn);
	_state	= &discUncUn;
	}

void	StateVar::changeToDiscUnlockAddrZeroHcUa() noexcept{
	trace(discUnlockAddrZeroHcUa);
	_state	= &discUnlockAddrZeroHcUa;
	}

void	StateVar::changeToDiscFreeUsbAddrHc() noexcept{
	trace(discFreeUsbAddrHc);
	_state	= &discFreeUsbAddrHc;
	}

void	StateVar::changeToDiscTimerCancelAzUa() noexcept{
	trace(discTimerCancelAzUa);
	_state	= &discTimerCancelAzUa;
	}

void	StateVar::changeToDiscTimerCancelPaUa() noexcept{
	trace(discTimerCancelPaUa);
	_state	= &discTimerCancelPaUa;
	}

void	StateVar::changeToDiscReadDescIrpcIrpAzUa() noexcept{
	trace(discReadDescIrpcIrpAzUa);
	_state	= &discReadDescIrpcIrpAzUa;
	}

void	StateVar::changeToDiscSetAddrIrpcIrpAzUa() noexcept{
	trace(discSetAddrIrpcIrpAzUa);
	_state	= &discSetAddrIrpcIrpAzUa;
	}

void	StateVar::changeToDiscPrepareHcPaUa() noexcept{
	trace(discPrepareHcPaUa);
	_state	= &discPrepareHcPaUa;
	}

void	StateVar::changeToDiscDynSrvHcPaUa() noexcept{
	trace(discDynSrvHcPaUa);
	_state	= &discDynSrvHcPaUa;
	}

void	StateVar::changeToDiscFreePipeHcUa() noexcept{
	trace(discFreePipeHcUa);
	_state	= &discFreePipeHcUa;
	}

void	StateVar::changeToDiscPipeAllocHcUa() noexcept{
	trace(discPipeAllocHcUa);
	_state	= &discPipeAllocHcUa;
	}

void	StateVar::changeToDiscRncRnUa() noexcept{
	trace(discRncRnUa);
	_state	= &discRncRnUa;
	}

void	StateVar::changeToClosingReadFullIrpPncPnPaUa() noexcept{
	trace(closingReadFullIrpPncPnPaUa);
	_state	= &closingReadFullIrpPncPnPaUa;
	}

void	StateVar::changeToClosingReadFullIrpcIrpPaUa() noexcept{
	trace(closingReadFullIrpcIrpPaUa);
	_state	= &closingReadFullIrpcIrpPaUa;
	}

void	StateVar::changeToClosingPncPnPaUa() noexcept{
	trace(closingPncPnPaUa);
	_state	= &closingPncPnPaUa;
	}

void	StateVar::changeToConnReadFullIrpPnPaUa() noexcept{
	trace(connReadFullIrpPnPaUa);
	_state	= &connReadFullIrpPnPaUa;
	}

void	StateVar::changeToConnReadFullErrorPnPaUa() noexcept{
	trace(connReadFullErrorPnPaUa);
	_state	= &connReadFullErrorPnPaUa;
	}

void	StateVar::changeToDiscReadFullIrpcIrpPaUa() noexcept{
	trace(discReadFullIrpcIrpPaUa);
	_state	= &discReadFullIrpcIrpPaUa;
	}

unsigned long	_nTraces;

void	StateVar::trace(const State& state) noexcept{
	TraceRec*	rec;
	++_nTraces;
	rec	= _freeTraceRecs.get();
	if(!rec){
		rec	= _trace.get();
		}
	rec->_state	= &state;
	_trace.put(rec);
	}

