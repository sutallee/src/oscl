/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <new>
#include "driver.h"
#include "oscl/mt/thread.h"
#include "oscl/error/fatal.h"

using namespace Oscl::Usb::Hub::Adaptor;

Driver::Driver(	Oscl::Usb::Pipe::Interrupt::
				IN::Req::Api::SAP&							changePipe,
				Oscl::Usb::Pipe::Message::
				Req::Api::SAP&								controlPipe,
				Oscl::Mt::Itc::PostMsgApi&					myPapi,
				SetupMem&									setupMem,
				PacketMem&									packetMem,
				Oscl::Usb::Hub::Port::Adaptor::
				Port::SetupMem*								portSetupMem,
				Oscl::Usb::Hub::Port::Adaptor::
				Port::PacketMem*							portPacketMem,
				PortMem*									portMem,
				unsigned									maxPorts,
				unsigned									nPorts
				) noexcept:
		Oscl::Mt::Itc::Srv::CloseSync(*this,myPapi),
		_changePipe(changePipe),
		_controlPipe(controlPipe),
		_setupMem(setupMem),
		_packetMem(packetMem),
		_portMem(portMem),
		_maxPorts(maxPorts),
		_nPorts(nPorts),
		_nChangeBytes((maxPorts+7+1)/8),
		_myPapi(myPapi),
		_openReq(0),
		_closeReq(0),
		_open(false),
		_closing(false),
		_powerSAP(*this,myPapi),
		_powerSync(_powerSAP),
		_changeDisabled(0),
		_currentClose(0),
		_pendingChange(0),
		_pendingCancel(0)
		{
	for(unsigned i=0;i<nTransactions;++i){
		free(&_transMem[i]);
		}
	if(nPorts > maxPorts){
		Oscl::ErrorFatal::logAndExit
		("Usb::Ohci::Hub::Root::Driver::initialize: nPorts > maxPorts\n");
		}
	for(unsigned i=0;i<nPorts;++i){
		Oscl::Usb::Hub::Port::Adaptor::Port*
		port	= new(&_portMem[i])
					Oscl::Usb::Hub::Port::
					Adaptor::Port(	*this,
									controlPipe,
									portSetupMem[i],
									portPacketMem[i],
									_myPapi,
									i+1
									);
		_ports.put(port);
		}
	}

void	Driver::initialize() noexcept{
	Oscl::Usb::Hub::Port::Adaptor::Port*	next;
	for(	next	= _ports.first();
			next;
			next	= _ports.next(next)
			){
		next->open();
		}
	sendSetGlobalPowerIRP();
	// after sendSetGlobalPowerIRP() completes
	// sendChangeIRP();
	}

Oscl::Usb::Hub::Port::
Req::Api::SAP*	Driver::getPortSAP(unsigned portNum) noexcept{
	Oscl::Usb::Hub::Port::Adaptor::Port*	next	= _ports.first();
	for(unsigned i=0;(next && (i<_maxPorts));++i,next=_ports.next(next)){
		if(i==portNum){
			return &next->getHubPortSAP();
			}
		}
	return 0;
	}

Oscl::Usb::Alloc::Power::Req::Api::SAP&	Driver::getPowerSAP() noexcept{
	return _powerSAP;
	}

Oscl::Usb::Alloc::Power::SyncApi&	Driver::getPowerSyncApi() noexcept{
	return _powerSync;
	}

void	Driver::statusChange() noexcept{
	using namespace Oscl::Usb::Hw::Hub;

	if(_hubChange.equal(	wChange::LocalPowerSource::FieldMask,
							wChange::LocalPowerSource::ValueMask_Changed
							)
			){
		if(_hubStatus.equal(	wStatus::LocalPowerSource::FieldMask,
								wStatus::LocalPowerSource::ValueMask_Good
								)
				){
			localPowerErrorNegated();
			}
		else{
			localPowerErrorAsserted();
			}
		}
	else if(_hubChange.equal(	wChange::OverCurrent::FieldMask,
								wChange::OverCurrent::ValueMask_Changed
								)
			){
		if(_hubStatus.equal(	wStatus::OverCurrent::FieldMask,
								wStatus::OverCurrent::ValueMask_Active
								)
				){
			overcurrentIndicatorAsserted();
			}
		else{
			overcurrentIndicatorNegated();
			}
		}
	else {
		enableChangeNotification();
		}
	}

void	Driver::hubStatusChange() noexcept{
	sendGetStatusIRP();
	disableChangeNotification();
	}

void	Driver::sendChangeIRP() noexcept{
	_pendingChange	= new(&_changeMem)
						GetChangeTrans(	*this,
										_changePipe,
										_packetMem.
										context.getChange,
										_myPapi,
										_nChangeBytes
										);
	}

void	Driver::sendSetGlobalPowerIRP() noexcept{
	Driver::TransMem*	mem	= allocTransMem();
	if(!mem){
		// FIXME: Logic Error
		while(true);
		}
	TransApi*	t	= new(mem) SetGlobalPowerTrans(	*this,
													_controlPipe,
													_setupMem.trans.
													setGlobalPower,
													_myPapi
													);
	_pendingTrans.put(t);
	}

void	Driver::sendGetStatusIRP() noexcept{
	Driver::TransMem*	mem	= allocTransMem();
	if(!mem){
		// FIXME: Logic Error
		while(true);
		}
	TransApi*	t	= new(mem) GetStatusTrans(	*this,
												_controlPipe,
												_setupMem.trans.
												getStatus,
												_packetMem.trans.
												getStatus,
												_myPapi
												);
	_pendingTrans.put(t);
	}

Driver::TransMem*	Driver::allocTransMem() noexcept{
	return _freeTrans.get();
	}

void	Driver::retire(TransApi& trans) noexcept{
	_pendingTrans.remove(&trans);
	if(_closing){
		if(&trans != _pendingCancel){
			return;
			}
		_pendingCancel		= _pendingTrans.first();
		if(!_pendingCancel){
			_open		= false;
			_closing	= false;
			if(!_closeReq){
				// FIXME:Logic Error
				while(true);
				}
			_closeReq->returnToSender();
			}
		else {
			_pendingCancel->cancel(_cancelMem.messagePipe);
			}
		}
	}

void		Driver::free(TransMem* mem) noexcept{
	_freeTrans.put(mem);
	}

void	Driver::canceled(SetGlobalPowerTrans& trans) noexcept{
	retire(trans);
	trans.~SetGlobalPowerTrans();
	free((TransMem*)&trans);
	}

void	Driver::failed(SetGlobalPowerTrans& trans) noexcept{
	_pendingChange	= 0;
	trans.~SetGlobalPowerTrans();
	free((TransMem*)&trans);
	// FIXME: Hopefully, this is done due to the hub being
	// disconnected. At least, thats my assumption.
	}

void	Driver::done(SetGlobalPowerTrans& trans) noexcept{
	retire(trans);
	if(!_open || _closing) return;
	trans.~SetGlobalPowerTrans();
	free((TransMem*)&trans);
	sendChangeIRP();
	}

void	Driver::canceled(GetStatusTrans& trans) noexcept{
	retire(trans);
	trans.~GetStatusTrans();
	free((TransMem*)&trans);
	}

void	Driver::failed(GetStatusTrans& trans) noexcept{
	_pendingChange	= 0;
	trans.~GetStatusTrans();
	free((TransMem*)&trans);
	// FIXME: Hopefully, this is done due to the hub being
	// disconnected. At least, thats my assumption.
	}

void	Driver::done(	GetStatusTrans&				trans,
						Oscl::Usb::Hw::Hub::Packet&	packet
						) noexcept{
	retire(trans);
	if(!_open || _closing) return;
	trans.~GetStatusTrans();
	free((TransMem*)&trans);
	_hubChange	= packet.wChange;
	_hubStatus	= packet.wStatus;
	statusChange();
	}

void	Driver::canceled(GetChangeTrans& trans) noexcept{
	_pendingChange	= 0;
	trans.~GetChangeTrans();
	closeNextPort();
	}

void	Driver::failed(GetChangeTrans& trans) noexcept{
	_pendingChange	= 0;
	trans.~GetChangeTrans();
	// FIXME: Hopefully, this is done due to the hub being
	// disconnected. At least, thats my assumption.
	}

void	Driver::done(	GetChangeTrans&				trans,
						unsigned char*				packet
						) noexcept{
	if(!_open || _closing) return;
	_pendingChange	= 0;
	trans.~GetChangeTrans();
	unsigned char*	c	= packet;
	if(c[0] & (1<<0)){
		hubStatusChange();
		}
	Oscl::Usb::Hub::Port::Adaptor::Port*	port	= _ports.first();
	for(	unsigned j=1;
			port && (j<7);
			++j,port	= _ports.next(port)
			){
		if(c[0] & (1<<j)){
			port->statusChange();
			}
		}
	for(unsigned i=1;port && (i<_nChangeBytes);++i){
		for(	unsigned j=0;
				port&&(j<8);
				++j,port	= _ports.next(port)
				){
			if(c[i] & (1<<j)){
				port->statusChange();
				}
			}
		}
	}

void	Driver::localPowerErrorAsserted() noexcept{
	// FIXME:
	while(true);
	}

void	Driver::localPowerErrorNegated() noexcept{
	// FIXME:
	while(true);
	}

void	Driver::overcurrentIndicatorAsserted() noexcept{
	// FIXME:
	while(true);
	}

void	Driver::overcurrentIndicatorNegated() noexcept{
	// FIXME:
	while(true);
	}

void	Driver::enableChangeNotification() noexcept{
	if(_changeDisabled){
		--_changeDisabled;
		}
	if(!_changeDisabled){
		sendChangeIRP();
		}
	}

void	Driver::disableChangeNotification() noexcept{
	++_changeDisabled;
	}

void		Driver::closeNextPort() noexcept{
	if(_currentClose){
		_currentClose	= _ports.next(_currentClose);
		}
	else {
		_currentClose	= _ports.get();
		}
	if(_currentClose){
		_currentClose->close(_portCloseMem);
		}
	else {
		_closeReq->returnToSender();
		}
	}

void	Driver::closeComplete() noexcept{
	// a port has completed an asynchronous close.
	closeNextPort();
	}

void	Driver::request(	Oscl::Mt::Itc::Srv::
							Open::Req::Api::OpenReq&	msg
							) noexcept{
	_openReq	= &msg;
	_open		= true;
	initialize();
	msg.returnToSender();	// FIXME: ?
	}

void	Driver::request(	Oscl::Mt::Itc::Srv::
							Close::Req::Api::CloseReq&	msg
							) noexcept{
	_closeReq	= &msg;
	_closing	= true;
	if(_pendingChange){
		_pendingChange->cancel(_cancelMem.interruptInPipe);
		return;
		}
	closeNextPort();
	}

void	Driver::request(	Alloc::Power::
							Req::Api::ReserveReq&	msg
							) noexcept{
	// FIXME: do real power allocation
	msg.getPayload()._failed	= false;
	msg.returnToSender();
	}

void	Driver::request(	Alloc::Power::
							Req::Api::FreeReq&		msg
							) noexcept{
	// FIXME: do real power free
	msg.returnToSender();
	}

void	Driver::request(	Alloc::Power::
							Req::Api::ChangeReq&	msg
							) noexcept{
	// FIXME:
	while(true);
	}

void	Driver::request(	Alloc::Power::
							Req::Api::CancelReq&	msg
							) noexcept{
	// FIXME:
	while(true);
	}

