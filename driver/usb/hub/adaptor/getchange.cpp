/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <new>
#include "trans.h"
#include "driver.h"

using namespace Oscl::Usb::Hub::Adaptor;

GetChangeTrans::GetChangeTrans(	Driver&							context,
								Oscl::Usb::Pipe::Interrupt::
								IN::Req::Api::SAP&				pipeSAP,
								PacketMem&						packetMem,
								Oscl::Mt::Itc::PostMsgApi&		myPapi,
								unsigned						nChangeBytes
								) noexcept:
		_context(context),
		_pipeSAP(pipeSAP),
		_packetMem(packetMem),
		_myPapi(myPapi),
		_resp(0),
		_canceling(false)
		{
	Oscl::Usb::Pipe::Interrupt::IN::Req::Api::ReadPayload*
	payload	= new(&_respMem.payload)
				Oscl::Usb::Pipe::Interrupt::IN::
				Req::Api::ReadPayload(	&packetMem,
										nChangeBytes
										);

	_resp	= new(&_respMem.resp)
				Oscl::Usb::Pipe::Interrupt::IN::
				Resp::Api::ReadResp(	pipeSAP.getReqApi(),
										*this,
										myPapi,
										*payload
										);
	pipeSAP.post(_resp->getSrvMsg());
	}

void	GetChangeTrans::cancel(	Oscl::Usb::Pipe::
								Interrupt::IN::Resp::CancelMem&	mem
								) noexcept{
	_canceling	= false;
	Oscl::Usb::Pipe::Interrupt::IN::Req::Api::CancelPayload*
	payload	= new(&mem.payload)
				Oscl::Usb::Pipe::Interrupt::IN::
				Req::Api::CancelPayload(_resp->getSrvMsg());

	Oscl::Usb::Pipe::Interrupt::IN::Resp::Api::CancelResp*
	resp	= new(&mem.resp)
				Oscl::Usb::Pipe::Interrupt::IN::
				Resp::Api::CancelResp(	_pipeSAP.getReqApi(),
										*this,
										_myPapi,
										*payload
										);

	_pipeSAP.post(resp->getSrvMsg());
	}

void	GetChangeTrans::response(	Oscl::Usb::Pipe::Interrupt::IN::
									Resp::Api::CancelResp&		msg
									) noexcept{
	_context.canceled(*this);
	}

void	GetChangeTrans::response(	Oscl::Usb::Pipe::Interrupt::IN::
									Resp::Api::ReadResp&		msg
									) noexcept{
	if(_canceling) return;
	if(msg.getPayload()._failed){
		_context.failed(*this);
		return;
		}
	unsigned char*	packet	= (unsigned char*)&_packetMem;
	_context.done(*this,packet);
	}

