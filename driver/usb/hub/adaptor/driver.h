/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_usb_hub_adaptor_driverh_
#define _oscl_drv_usb_hub_adaptor_driverh_
#include "oscl/memory/block.h"
#include "oscl/queue/queue.h"
#include "oscl/driver/usb/hub/port/adaptor/port.h"
#include "oscl/mt/itc/srv/close.h"
#include "oscl/driver/usb/alloc/power/reqapi.h"
#include "oscl/driver/usb/alloc/power/sync.h"
#include "trans.h"

/** */
namespace Oscl {
/** */
namespace Usb {
/** */
namespace Hub {
/** */
namespace Adaptor {

/** */
class Driver :	public Oscl::Mt::Itc::Srv::CloseSync,
				public Oscl::Mt::Itc::Srv::Close::Req::Api,
				public Oscl::Usb::Alloc::Power::Req::Api,
				public PortApi
				{
	private:
		/** */
		typedef Oscl::Memory::
				AlignedBlock<	sizeof(	Oscl::Usb::Hub::
										Port::Adaptor::Port
										)
								>		PortMem;
		/** */
		typedef Oscl::Memory::
				AlignedBlock<	sizeof(	Oscl::Usb::Hw::
										Hub::Packet
										)
								>		HubStatusMem;
		/** */
		union TransSetupMem {
			/** */
			GetStatusTrans::SetupMem			getStatus;
			/** */
			SetGlobalPowerTrans::SetupMem		setGlobalPower;
			/** */
			ClearGlobalPowerTrans::SetupMem		clrGlobalPower;
			/** */
			ClearHubOverCurrentTrans::SetupMem	clrHubOverCurrent;
			};
		/** */
		union ContextSetupMem {
			};
	public:
		/** */
		struct SetupMem {
			/** */
			TransSetupMem		trans;
			/** */
			ContextSetupMem		context;
			};
		union TransPacketMem {
			/** */
			GetStatusTrans::PacketMem			getStatus;
			};
		/** */
		union ContextPacketMem {
			/** */
			enum{maxChangeBytes=((127+7)/8)};
			/** */
			GetChangeTrans::PacketMem		getChange;
			};
		/** */
		struct PacketMem {
			/** */
			TransPacketMem					trans;
			/** */
			ContextPacketMem				context;
			};
		/** */
		union TransMem {
			/** */
			void*			__qitemlink;
			/** */
			Oscl::Memory::
			AlignedBlock<	sizeof(GetStatusTrans)
							>		getStatus;
			/** */
			Oscl::Memory::
			AlignedBlock<	sizeof(SetGlobalPowerTrans)
							>		setGlobalPower;
			/** */
			Oscl::Memory::
			AlignedBlock<	sizeof(ClearGlobalPowerTrans)
							>		clearGlobalPower;
			/** */
			Oscl::Memory::
			AlignedBlock<	sizeof(ClearHubOverCurrentTrans)
							>		clearHubOverCurrent;
			};
		/** */
		union ChangeTransMem {
			/** */
			Oscl::Memory::
			AlignedBlock<	sizeof(GetChangeTrans)
							>		getChange;
			};
		/** */
		union CancelMem {
			/** */
			Oscl::Usb::Pipe::Interrupt::
			IN::Resp::CancelMem					interruptInPipe;
			Oscl::Usb::Pipe::Message::
			Resp::CancelMem						messagePipe;
			};
	private:
		/** */
		enum{nTransactions=1};
		/** */
		TransMem							_transMem[nTransactions];
		/** */
		Oscl::Usb::Hub::Port::
		Adaptor::Port::CloseMem				_portCloseMem;
		/** */
		ChangeTransMem						_changeMem;
		/** */
		CancelMem							_cancelMem;
		/** */
		Oscl::Usb::Pipe::Interrupt::
		IN::Req::Api::SAP&					_changePipe;
		/** */
		Oscl::Usb::Pipe::Message::
		Req::Api::SAP&						_controlPipe;
		/** */
		SetupMem&							_setupMem;
		/** */
		PacketMem&							_packetMem;
		/** */
		PortMem*							_portMem;
		/** */
		const unsigned						_maxPorts;
		/** */
		const unsigned						_nPorts;
		/** */
		const unsigned						_nChangeBytes;
		/** */
		Oscl::Mt::Itc::PostMsgApi&			_myPapi;
		/** */
		Oscl::BitField<	Oscl::Usb::Hw::
						Hub::wChange::Reg
						>					_hubChange;
		/** */
		Oscl::BitField<	Oscl::Usb::Hw::
						Hub::wChange::Reg
						>					_hubStatus;
		/** */
		Oscl::Queue<	Oscl::Usb::Hub::
						Port::Adaptor::Port
						>					_ports;
		/** */
		Oscl::Queue<TransMem>				_freeTrans;
		/** */
		Oscl::Queue<TransApi>				_pendingTrans;
		/** */
		Oscl::Mt::Itc::Srv::Open::
		Req::Api::OpenReq*					_openReq;
		/** */
		Oscl::Mt::Itc::Srv::Close::
		Req::Api::CloseReq*					_closeReq;
		/** */
		bool								_open;
		/** */
		bool								_closing;
		/** */
		Oscl::Usb::Alloc::Power::
		Req::Api::ConcreteSAP				_powerSAP;
		/** */
		Oscl::Usb::Alloc::Power::Sync		_powerSync;
		/** */
		unsigned							_changeDisabled;
		/** */
		Oscl::Usb::Hub::Port::
		Adaptor::Port*						_currentClose;
		/** */
		GetChangeTrans*						_pendingChange;
		/** */
		TransApi*							_pendingCancel;

	public:
		/** */
		Driver(	Oscl::Usb::Pipe::Interrupt::
				IN::Req::Api::SAP&				changePipe,
				Oscl::Usb::Pipe::Message::
				Req::Api::SAP&					controlPipe,
				Oscl::Mt::Itc::PostMsgApi&		myPapi,
				SetupMem&						setupMem,
				PacketMem&						packetMem,
				Oscl::Usb::Hub::Port::Adaptor::
				Port::SetupMem*					portSetupMem,
				Oscl::Usb::Hub::Port::Adaptor::
				Port::PacketMem*				portPacketMem,
				PortMem*						portMem,
				unsigned						maxPorts,
				unsigned						nPorts
				) noexcept;

		/** */
		Oscl::Usb::Hub::Port::
		Req::Api::SAP*			getPortSAP(unsigned portNum) noexcept;

		/** */
		Oscl::Usb::Alloc::
		Power::Req::Api::SAP&	getPowerSAP() noexcept;

		/** */
		Oscl::Usb::Alloc::
		Power::SyncApi&			getPowerSyncApi() noexcept;

	private:
		/** */
		void	initialize() noexcept;

		/** */
		void	statusChange() noexcept;

		/** */
		void	hubStatusChange() noexcept;

		/** */
		void	sendChangeIRP() noexcept;

		/** */
		void	sendSetGlobalPowerIRP() noexcept;

		/** */
		void	sendGetStatusIRP() noexcept;

		/** */
		void		retire(TransApi& trans) noexcept;

		/** */
		TransMem*	allocTransMem() noexcept;

		/** */
		void		free(TransMem* mem) noexcept;

		/** */
		void		closeNextPort() noexcept;
	private:
		friend class SetGlobalPowerTrans;
		/** */
		void	canceled(SetGlobalPowerTrans& trans) noexcept;
		/** */
		void	failed(SetGlobalPowerTrans& trans) noexcept;
		/** */
		void	done(SetGlobalPowerTrans& trans) noexcept;

	private:
		friend class GetStatusTrans;
		/** */
		void	canceled(GetStatusTrans& trans) noexcept;
		/** */
		void	failed(GetStatusTrans& trans) noexcept;
		/** */
		void	done(	GetStatusTrans&				trans,
						Oscl::Usb::Hw::Hub::Packet&	packet
						) noexcept;

	private:
		friend class GetChangeTrans;
		/** */
		void	canceled(GetChangeTrans& trans) noexcept;
		/** */
		void	failed(GetChangeTrans& trans) noexcept;
		/** */
		void	done(	GetChangeTrans&				trans,
						unsigned char*				packet
						) noexcept;

	private:
		/** */
		void	localPowerErrorAsserted() noexcept;
		/** */
		void	localPowerErrorNegated() noexcept;
		/** */
		void	overcurrentIndicatorAsserted() noexcept;
		/** */
		void	overcurrentIndicatorNegated() noexcept;

	public:	// Port Api
		/** */
		void	enableChangeNotification() noexcept;
		/** */
		void	disableChangeNotification() noexcept;
		/** */
		void	closeComplete() noexcept;
		
	private:	// Oscl::Mt::Itc::Srv::Close::Req::Api
		/** */
		void	request(	Oscl::Mt::Itc::Srv::
							Open::Req::Api::OpenReq&	msg
							) noexcept;
		/** */
		void	request(	Oscl::Mt::Itc::Srv::
							Close::Req::Api::CloseReq&	msg
							) noexcept;

	private:	// Oscl::Usb::Alloc::Power::Req::Api
		/** */
		void	request(	Alloc::Power::
							Req::Api::ReserveReq&	msg
							) noexcept;
		/** */
		void	request(	Alloc::Power::
							Req::Api::FreeReq&		msg
							) noexcept;
		/** */
		void	request(	Alloc::Power::
							Req::Api::ChangeReq&	msg
							) noexcept;
		/** */
		void	request(	Alloc::Power::
							Req::Api::CancelReq&	msg
							) noexcept;

	};

}
}
}
}


#endif
