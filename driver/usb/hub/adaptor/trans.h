/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_usb_hub_adaptor_transh_
#define _oscl_drv_usb_hub_adaptor_transh_
#include "oscl/queue/queueitem.h"
#include "oscl/queue/queue.h"
#include "oscl/memory/block.h"
#include "oscl/driver/usb/hub/port/reqapi.h"
#include "oscl/hw/usb/hub/port/packet.h"
#include "oscl/driver/usb/pipe/message/respmem.h"
#include "oscl/driver/usb/setup/hub/gethubstatus.h"
#include "oscl/driver/usb/setup/hub/setglobalpower.h"
#include "oscl/driver/usb/setup/hub/clrglobalpower.h"
#include "oscl/driver/usb/setup/hub/clrhuboc.h"
#include "oscl/hw/usb/hub/packet.h"
#include "oscl/driver/usb/pipe/interrupt/in/respmem.h"

/** */
namespace Oscl {
/** */
namespace Usb {
/** */
namespace Hub {
/** */
namespace Adaptor {

/** */
class Driver;

/** */
class TransApi :	public Oscl::QueueItem {
	public:
		/** */
		virtual ~TransApi(){};

		virtual void	cancel(	Oscl::Usb::Pipe::
								Message::Resp::CancelMem&	mem
								) noexcept=0;
	};

/** */
class GetStatusTrans :	private Oscl::Usb::Pipe::Message::Resp::Api,
						public TransApi
						{
	public:
		/** */
		union SetupMem {
			/** */
			Oscl::Memory::
			AlignedBlock<	sizeof(	Oscl::Usb::Setup::
									Hub::GetHubStatus
									)
							>	hubStatus;
			};

		/** */
		union PacketMem {
			/** */
			Oscl::Memory::
			AlignedBlock<	sizeof(	Oscl::Usb::Hw::
									Hub::Packet
									)
							>	hubStatus;
			};
	private:
		/** */
		Oscl::Usb::Pipe::Message::Resp::ReadMem				_respMem;
		/** */
		Driver&												_context;
		/** */
		Oscl::Usb::Pipe::Message::Req::Api::SAP&			_pipeSAP;
		/** */
		SetupMem&											_setupMem;
		/** */
		PacketMem&											_packetMem;
		/** */
		Oscl::Mt::Itc::PostMsgApi&							_myPapi;
		/** */
		Oscl::Usb::Pipe::Message::Resp::Api::ReadResp*		_resp;
		/** */
		bool												_canceling;

	public:
		/** */
		GetStatusTrans(	Driver&							context,
						Oscl::Usb::Pipe::Message::
						Req::Api::SAP&					pipeSAP,
						SetupMem&						setupMem,
						PacketMem&						packetMem,
						Oscl::Mt::Itc::PostMsgApi&		myPapi
						) noexcept;

	private:	// TransApi
		/** */
		void	cancel(Oscl::Usb::Pipe::Message::Resp::CancelMem& mem) noexcept;

	private:	// Oscl::Usb::Pipe::Message::Resp::Api
		/** */
		void	response(	Oscl::Usb::Pipe::Message::
							Resp::Api::CancelResp&		msg
							) noexcept;
		/** */
		void	response(	Oscl::Usb::Pipe::Message::
							Resp::Api::NoDataResp&		msg
							) noexcept;
		/** */
		void	response(	Oscl::Usb::Pipe::Message::
							Resp::Api::ReadResp&		msg
							) noexcept;
		/** */
		void	response(	Oscl::Usb::Pipe::Message::
							Resp::Api::WriteResp&		msg
							) noexcept;
	};

/** */
class SetGlobalPowerTrans :	private Oscl::Usb::Pipe::Message::Resp::Api,
						public TransApi
						{
	public:
		/** */
		union SetupMem {
			/** */
			Oscl::Memory::
			AlignedBlock<	sizeof(	Oscl::Usb::Setup::
									Hub::SetGlobalPower
									)
							>	setGlobalPower;
			};

	private:
		/** */
		Oscl::Usb::Pipe::Message::Resp::NoDataMem			_respMem;
		/** */
		Driver&												_context;
		/** */
		Oscl::Usb::Pipe::Message::Req::Api::SAP&			_pipeSAP;
		/** */
		SetupMem&											_setupMem;
		/** */
		Oscl::Mt::Itc::PostMsgApi&							_myPapi;
		/** */
		Oscl::Usb::Pipe::Message::Resp::Api::NoDataResp*	_resp;
		/** */
		bool												_canceling;

	public:
		/** */
		SetGlobalPowerTrans(	Driver&							context,
								Oscl::Usb::Pipe::Message::
								Req::Api::SAP&					pipeSAP,
								SetupMem&						setupMem,
								Oscl::Mt::Itc::PostMsgApi&		myPapi
								) noexcept;

	private:	// TransApi
		/** */
		void	cancel(Oscl::Usb::Pipe::Message::Resp::CancelMem& mem) noexcept;

	private:	// Oscl::Usb::Pipe::Message::Resp::Api
		/** */
		void	response(	Oscl::Usb::Pipe::Message::
							Resp::Api::CancelResp&		msg
							) noexcept;
		/** */
		void	response(	Oscl::Usb::Pipe::Message::
							Resp::Api::NoDataResp&		msg
							) noexcept;
		/** */
		void	response(	Oscl::Usb::Pipe::Message::
							Resp::Api::ReadResp&		msg
							) noexcept;
		/** */
		void	response(	Oscl::Usb::Pipe::Message::
							Resp::Api::WriteResp&		msg
							) noexcept;
	};

/** */
class ClearGlobalPowerTrans :	private Oscl::Usb::Pipe::Message::Resp::Api,
						public TransApi
						{
	public:
		/** */
		union SetupMem {
			/** */
			Oscl::Memory::
			AlignedBlock<	sizeof(	Oscl::Usb::Setup::
									Hub::ClearGlobalPower
									)
							>	setGlobalPower;
			};

	private:
		/** */
		Oscl::Usb::Pipe::Message::Resp::NoDataMem			_respMem;
		/** */
		Driver&												_context;
		/** */
		Oscl::Usb::Pipe::Message::Req::Api::SAP&			_pipeSAP;
		/** */
		SetupMem&											_setupMem;
		/** */
		Oscl::Mt::Itc::PostMsgApi&							_myPapi;
		/** */
		Oscl::Usb::Pipe::Message::Resp::Api::NoDataResp*	_resp;
		/** */
		bool												_canceling;

	public:
		/** */
		ClearGlobalPowerTrans(	Driver&							context,
								Oscl::Usb::Pipe::Message::
								Req::Api::SAP&					pipeSAP,
								SetupMem&						setupMem,
								Oscl::Mt::Itc::PostMsgApi&		myPapi
								) noexcept;

	private:	// TransApi
		/** */
		void	cancel(Oscl::Usb::Pipe::Message::Resp::CancelMem& mem) noexcept;

	private:	// Oscl::Usb::Pipe::Message::Resp::Api
		/** */
		void	response(	Oscl::Usb::Pipe::Message::
							Resp::Api::CancelResp&		msg
							) noexcept;
		/** */
		void	response(	Oscl::Usb::Pipe::Message::
							Resp::Api::NoDataResp&		msg
							) noexcept;
		/** */
		void	response(	Oscl::Usb::Pipe::Message::
							Resp::Api::ReadResp&		msg
							) noexcept;
		/** */
		void	response(	Oscl::Usb::Pipe::Message::
							Resp::Api::WriteResp&		msg
							) noexcept;
	};

/** */
class ClearHubOverCurrentTrans :	private Oscl::Usb::Pipe::Message::Resp::Api,
									public TransApi
									{
	public:
		/** */
		union SetupMem {
			/** */
			Oscl::Memory::
			AlignedBlock<	sizeof(	Oscl::Usb::Setup::
									Hub::ClearHubOverCurrent
									)
							>	setHubOverCurrent;
			};

	private:
		/** */
		Oscl::Usb::Pipe::Message::Resp::NoDataMem			_respMem;
		/** */
		Driver&												_context;
		/** */
		Oscl::Usb::Pipe::Message::Req::Api::SAP&			_pipeSAP;
		/** */
		SetupMem&											_setupMem;
		/** */
		Oscl::Mt::Itc::PostMsgApi&							_myPapi;
		/** */
		Oscl::Usb::Pipe::Message::Resp::Api::NoDataResp*	_resp;
		/** */
		bool												_canceling;

	public:
		/** */
		ClearHubOverCurrentTrans(	Driver&							context,
									Oscl::Usb::Pipe::Message::
									Req::Api::SAP&					pipeSAP,
									SetupMem&						setupMem,
									Oscl::Mt::Itc::PostMsgApi&		myPapi
									) noexcept;

	private:	// TransApi
		/** */
		void	cancel(Oscl::Usb::Pipe::Message::Resp::CancelMem& mem) noexcept;

	private:	// Oscl::Usb::Pipe::Message::Resp::Api
		/** */
		void	response(	Oscl::Usb::Pipe::Message::
							Resp::Api::CancelResp&		msg
							) noexcept;
		/** */
		void	response(	Oscl::Usb::Pipe::Message::
							Resp::Api::NoDataResp&		msg
							) noexcept;
		/** */
		void	response(	Oscl::Usb::Pipe::Message::
							Resp::Api::ReadResp&		msg
							) noexcept;
		/** */
		void	response(	Oscl::Usb::Pipe::Message::
							Resp::Api::WriteResp&		msg
							) noexcept;
	};

/** */
class GetChangeTrans :	private Oscl::Usb::Pipe::Interrupt::IN::Resp::Api {
	public:
		/** */
		union PacketMem {
			/** */
			enum{maxChangeBytes=((127+7)/8)};
			/** */
			Oscl::Memory::
			AlignedBlock<maxChangeBytes>	hubChangeMask;
			};

	private:
		/** */
		Oscl::Usb::Pipe::Interrupt::IN::Resp::ReadMem		_respMem;
		/** */
		Driver&												_context;
		/** */
		Oscl::Usb::Pipe::Interrupt::IN::Req::Api::SAP&		_pipeSAP;
		/** */
		PacketMem&											_packetMem;
		/** */
		Oscl::Mt::Itc::PostMsgApi&							_myPapi;
		/** */
		Oscl::Usb::Pipe::Interrupt::IN::
		Resp::Api::ReadResp*								_resp;
		/** */
		bool												_canceling;

	public:
		/** */
		GetChangeTrans(	Driver&							context,
						Oscl::Usb::Pipe::Interrupt::
						IN::Req::Api::SAP&				pipeSAP,
						PacketMem&						packetMem,
						Oscl::Mt::Itc::PostMsgApi&		myPapi,
						unsigned						nChangeBytes
						) noexcept;

		/** */
		void	cancel(	Oscl::Usb::Pipe::Interrupt::
						IN::Resp::CancelMem&			mem
						) noexcept;

	private:	// Oscl::Usb::Pipe::Interrupt::IN::Resp::Api
		/** */
		void	response(	Oscl::Usb::Pipe::Interrupt::
							IN::Resp::Api::ReadResp&		msg
							) noexcept;
		/** */
		void	response(	Oscl::Usb::Pipe::Interrupt::
							IN::Resp::Api::CancelResp&		msg
							) noexcept;
	};

}
}
}
}

#endif
