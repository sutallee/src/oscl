/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_usb_device_locapih_
#define _oscl_drv_usb_device_locapih_
#include "oscl/oid/roapi.h"

/** */
namespace Oscl {
/** */
namespace Usb {
/** */
namespace Device {

/** */
class LocationApi {
	public:
		/** The path OID is an absolute identifier that is a series of
			alternating device addresses and hub port slot. The series
			is always an even number in length. The series always
			begins with a port identifier and ends with a USB device
			address. The root hub is always implied.
		 */
		virtual const Oscl::ObjectID::RO::Api&	path() const noexcept=0;
	};

}
}
}

#endif
