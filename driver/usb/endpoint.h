/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_usb_endpointh_
#define _oscl_drv_usb_endpointh_

/** */
namespace Oscl {
/** */
namespace Usb {

/** The purpose of this class is to provide a type safe
	means of specifying a USB device endpoint identifier,
	and to provide a run-time check on the range of valid
	endpoint numbers. The range check is primitive and results
	in an infinite loop, which will allow for detection of
	logic errors during driver development.
 */
class EndpointID {
	private:
		/** */
		unsigned char	_endpoint;
	public:
		/** */
		EndpointID(unsigned char endpoint) noexcept:
			_endpoint(endpoint)
			{
			if(endpoint > 15){
				while(true);
				}
			}
		/** */
		EndpointID(const EndpointID& endpoint) noexcept:
			_endpoint(endpoint)
			{
			if(_endpoint > 15){
				while(true);
				}
			}
		/** */
		operator unsigned char() const noexcept{
			return _endpoint;
			}
	};

}
}

#endif
