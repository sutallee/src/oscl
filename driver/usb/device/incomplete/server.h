/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_usb_ohci_usb_device_serverh_
#define _oscl_drv_usb_ohci_usb_device_serverh_
#include "drvapi.h"
#include "oscl/pcisrv/enum/location.h"
#include "oscl/pcisrv/enum/funcapi.h"
#include "oscl/mt/itc/dyn/adv/core.h"

/** */
namespace Oscl {
/** */
namespace Usb {
/** */
namespace Device {

class InfoApi {
	public:
		/** */
		virtual uint16_t	getUsbSpecRelease() const noexcept=0;
		/** */
		virtual uint8_t	getClassCode() const noexcept=0;
		/** */
		virtual uint8_t	getSubClassCode() const noexcept=0;
		/** */
		virtual uint8_t	getProtocolCode() const noexcept=0;
		/** */
		virtual uint8_t	getMaximumPacketSize() const noexcept=0;
		/** */
		virtual uint16_t	getVendorID() const noexcept=0;
		/** */
		virtual uint16_t	getProductID() const noexcept=0;
		/** */
		virtual uint16_t	getDeviceReleaseNumber() const noexcept=0;
		/** */
		virtual uint8_t	getProductDescStrIndex() const noexcept=0;
		/** */
		virtual uint8_t	getSerialNumberStrIndex() const noexcept=0;
		/** */
		virtual uint8_t	getNumberOfConfigurations() const noexcept=0;

	public:
		/** */
		virtual const Oscl::Usb::Device::
				LocationApi&	getLocation() const noexcept=0;
	};

/** */
class Server :	public DriverApi,
				public Oscl::Mt::Itc::Dyn::Adv::Core<Server>
				{
	private:
		/** */
		InfoApi&						_funcInfo;
		/** */
		bool							_claimed;

	public:
		/** */
		Server(	InfoApi&								funcInfo,
				Oscl::Mt::Itc::PostMsgApi&				dynSrvPostApi,
				Oscl::Mt::Itc::PostMsgApi&				advPostApi,
				Oscl::Mt::Itc::Dyn::Adv::Core<Server>::
					ReleaseApi&							advertiser
				) noexcept;

		/** */
		bool	isClaimed() const noexcept;

	public:	// DriverApi
		/** */
		uint16_t	getUsbSpecRelease() const noexcept;
		/** */
		uint8_t	getClassCode() const noexcept;
		/** */
		uint8_t	getSubClassCode() const noexcept;
		/** */
		uint8_t	getProtocolCode() const noexcept;
		/** */
		uint8_t	getMaximumPacketSize() const noexcept;
		/** */
		uint16_t	getVendorID() const noexcept;
		/** */
		uint16_t	getProductID() const noexcept;
		/** */
		uint16_t	getDeviceReleaseNumber() const noexcept;
		/** */
		uint8_t	getProductDescStrIndex() const noexcept;
		/** */
		uint8_t	getSerialNumberStrIndex() const noexcept;
		/** */
		uint8_t	getNumberOfConfigurations() const noexcept;

		/** */
		const Oscl::Usb::Device::LocationApi&	getLocation() const noexcept;

	public: // Oscl::Mt::Itc::Dyn::Adv::Core
		/** Executes within the dynamic service thread. Connect
			to any lower layer services synchronously.
		 */
		void	openDynamicService() noexcept;
		/** Executes within the dynamic service thread. Disconnect
			from any lower layer services synchronously.
		 */
		void	closeDynamicService() noexcept;
		/** */
		Server&	getDynSrv() noexcept;
	};

}
}
}

#endif
