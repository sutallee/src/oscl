/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_usb_device_drvapih_
#define _oscl_drv_usb_device_drvapih_
#include "oscl/hw/usb/desc/devicereg.h"
// oscl/pcisrv/locapi.h
// oscl/driver/usb/locapi.h
//#include "oscl/usb/usb/locapi.h"
#include "oscl/driver/usb/locapi.h"
#include <stdint.h>

/** */
namespace Oscl {
/** */
namespace Usb {
/** */
namespace Device {

/** */
class DriverApi {
	public:
		/** */
		virtual uint16_t	getUsbSpecRelease() const noexcept=0;
		/** */
		virtual uint8_t	getClassCode() const noexcept=0;
		/** */
		virtual uint8_t	getSubClassCode() const noexcept=0;
		/** */
		virtual uint8_t	getProtocolCode() const noexcept=0;
		/** */
		virtual uint8_t	getMaximumPacketSize() const noexcept=0;
		/** */
		virtual uint16_t	getVendorID() const noexcept=0;
		/** */
		virtual uint16_t	getProductID() const noexcept=0;
		/** */
		virtual uint16_t	getDeviceReleaseNumber() const noexcept=0;
		/** */
		virtual uint8_t	getProductDescStrIndex() const noexcept=0;
		/** */
		virtual uint8_t	getSerialNumberStrIndex() const noexcept=0;
		/** */
		virtual uint8_t	getNumberOfConfigurations() const noexcept=0;

	public:
		/** */
		virtual const Oscl::Usb::Device::
				LocationApi&	getLocation() const noexcept=0;
	};

}
}
}

#endif
