/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "server.h"

using namespace Oscl::Usb::Device;

Server::Server(	InfoApi&						funcInfo,
				Oscl::Mt::Itc::PostMsgApi&		dynSrvPostApi,
				Oscl::Mt::Itc::PostMsgApi&		advPostApi,
				Oscl::Mt::Itc::Dyn::Adv::
				Core<Server>::ReleaseApi&		advertiser
				) noexcept:
		Oscl::Mt::Itc::Dyn::Adv::
		Core<Server>(dynSrvPostApi,advertiser,advPostApi),
		_funcInfo(funcInfo),
		_claimed(false)
		{
	}

bool	Server::isClaimed() const noexcept{
	return _claimed;
	}

uint16_t	Server::getUsbSpecRelease() const noexcept{
	return _funcInfo.getUsbSpecRelease();
	}

uint8_t	Server::getSubClassCode() const noexcept{
	return _funcInfo.getSubClassCode();
	}

uint8_t	Server::getProtocolCode() const noexcept{
	return _funcInfo.getProtocolCode();
	}

uint8_t	Server::getMaximumPacketSize() const noexcept{
	return _funcInfo.getMaximumPacketSize();
	}

uint16_t	Server::getVendorID() const noexcept{
	return _funcInfo.getVendorID();
	}

uint16_t	Server::getProductID() const noexcept{
	return _funcInfo.getProductID();
	}

uint16_t	Server::getDeviceReleaseNumber() const noexcept{
	return _funcInfo.getDeviceReleaseNumber();
	}

uint8_t	Server::getProductDescStrIndex() const noexcept{
	return _funcInfo.getProductDescStrIndex();
	}

uint8_t	Server::getSerialNumberStrIndex() const noexcept{
	return _funcInfo.getSerialNumberStrIndex();
	}

uint8_t	Server::getNumberOfConfigurations() const noexcept{
	return _funcInfo.getNumberOfConfigurations();
	}

const Oscl::Usb::Device::LocationApi&	Server::getLocation() const noexcept{
	return _funcInfo.getLocation();
	}

void	Server::openDynamicService() noexcept{
	// Nothing to attach yet
	}

void	Server::closeDynamicService() noexcept{
	// Nothing to detach yet
	}

Server&	Server::getDynSrv() noexcept{
	return *this;
	}

