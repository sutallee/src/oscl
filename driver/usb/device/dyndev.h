/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_usb_device_dyndevh_
#define _oscl_drv_usb_device_dyndevh_
#include "oscl/mt/itc/dyn/adv/core.h"
#include "oscl/driver/usb/pipe/message/pipeapi.h"
#include "oscl/driver/usb/alloc/power/reqapi.h"
#include "oscl/driver/usb/alloc/power/syncapi.h"
#include "oscl/oid/fixed.h"
#include "oscl/driver/usb/desc/device.h"
#include "oscl/driver/usb/hcd/enum/monitor/api.h"
#include "oscl/driver/usb/hcd/enum/dcp/api.h"
#include "oscl/mt/itc/mbox/srvevt.h"

/** */
namespace Oscl {
/** */
namespace Usb {

/** */
class DynDevice :	public Oscl::Mt::Itc::Dyn::Adv::Core<DynDevice> {
	public:
		/** */
		using Oscl::Mt::Itc::Dyn::Adv::Core<DynDevice>::request;

	private:
		/** */
		class LockPayload {};
		/** */
		class UnlockPayload {};
		/** */
		typedef Oscl::Mt::Itc::SrvEvent<DynDevice,LockPayload>		LockReq;
		/** */
		typedef Oscl::Mt::Itc::SrvEvent<DynDevice,UnlockPayload>	UnlockReq;

	public:
		/** */
		Oscl::Mt::Itc::PostMsgApi&			_myPapi;
		/** */
		Oscl::Usb::Alloc::Power::
		Req::Api::SAP&						_powerSAP;
		/** */
		Oscl::Usb::Alloc::Power::
		SyncApi&							_powerSyncApi;
		/** */
		Oscl::Usb::HCD::Enum::
		Monitor::Api&						_hcdEnumMonitorApi;
		/** */
		Oscl::Usb::HCD::Enum::
		DefCntrlPipe::Api&					_hcdEnumDcpApi;
		/** */
		Oscl::Usb::Pipe::Message::PipeApi&	_ctrlPipe;
		/** */
		Oscl::Usb::Address					_usbAddress;
		/** */
		bool								_lowSpeedDevice;
		/** */
		enum{ObjectIDSize=32};	// FIXME
		/** */
		Oscl::ObjectID::Fixed<ObjectIDSize>	_location;
	public:	// Device descriptor data
		/** */
		const uint16_t					_bcdUSB;
		/** */
		const uint8_t						_bDeviceClass;
		/** */
		const uint8_t						_bDeviceSubClass;
		/** */
		const uint8_t						_bDeviceProtocol;
		/** */
		const uint8_t						_bMaxPacketSize;
		/** */
		const uint16_t					_idVendor;
		/** */
		const uint16_t					_idProduct;
		/** */
		const uint16_t					_bcdDevice;
		/** */
		const uint8_t						_iManufacturer;
		/** */
		const uint8_t						_iProduct;
		/** */
		const uint8_t						_iSerialNumber;
		/** */
		const uint8_t						_bNumConfigurations;
		/** */
		Oscl::Queue<Oscl::Mt::Itc::SrvMsg>	_pendingLocks;
		/** */
		bool								_locked;
		/** */
		bool								_reserved;

	public:
		/** */
		DynDevice(	Oscl::Mt::Itc::PostMsgApi&			myPapi,
					Oscl::Mt::Itc::Dyn::Adv::
					Core<DynDevice>::ReleaseApi&		advRapi,
					Oscl::Mt::Itc::PostMsgApi&			advPapi,
					Oscl::Usb::Pipe::Message::PipeApi&	ctrlPipe,
					Oscl::Usb::Address					myUsbAddress,
					bool								lowSpeedDevice,
					Oscl::Usb::Alloc::Power::
					Req::Api::SAP&						powerSAP,
					Oscl::Usb::Alloc::Power::
					SyncApi&							powerSyncApi,
					Oscl::Usb::HCD::Enum::
					Monitor::Api&						hcdEnumMonitorApi,
					Oscl::Usb::HCD::Enum::
					DefCntrlPipe::Api&					hcdEnumDcpApi,
					const Oscl::Usb::Desc::Device&		devDesc,
					const Oscl::ObjectID::RO::Api*		location=0
					) noexcept;
		/** */
		Oscl::Usb::Alloc::Power::
		Req::Api::SAP&				getPowerSAP() noexcept;
		/** */
		Oscl::Usb::Alloc::Power::
		SyncApi&					getPowerSyncApi() noexcept;

		/** */
		void						lock() noexcept;
		/** */
		void						unlock() noexcept;
		/** */
		bool						isReserved() noexcept;
		/** */
		void						reserve() noexcept;

	private: // Oscl::Mt::Itc::Dyn::Adv::Core
		/** */
		void	openDynamicService() noexcept;
		/** */
		void	closeDynamicService() noexcept;

	public:
		/** */
		DynDevice&	getDynSrv() noexcept;

	private:
		/** */
		friend class Oscl::Mt::Itc::SrvEvent<DynDevice,LockPayload>;
		/** */
		friend class Oscl::Mt::Itc::SrvEvent<DynDevice,UnlockPayload>;
		/** */
		void	request(LockReq& msg) noexcept;
		/** */
		void	request(UnlockReq& msg) noexcept;
	};

}
}

#endif
