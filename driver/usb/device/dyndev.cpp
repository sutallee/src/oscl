/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "dyndev.h"
#include "oscl/mt/itc/mbox/syncrh.h"
#include "oscl/error/fatal.h"

using namespace Oscl::Usb;

DynDevice::DynDevice(	Oscl::Mt::Itc::PostMsgApi&			myPapi,
						Oscl::Mt::Itc::Dyn::Adv::
						Core<DynDevice>::ReleaseApi&		advRapi,
						Oscl::Mt::Itc::PostMsgApi&			advPapi,
						Oscl::Usb::Pipe::Message::PipeApi&	ctrlPipe,
						Oscl::Usb::Address					myUsbAddress,
						bool								lowSpeedDevice,
						Oscl::Usb::Alloc::Power::
						Req::Api::SAP&						powerSAP,
						Oscl::Usb::Alloc::Power::
						SyncApi&							powerSyncApi,
						Oscl::Usb::HCD::Enum::
						Monitor::Api&						hcdEnumMonitorApi,
						Oscl::Usb::HCD::Enum::
						DefCntrlPipe::Api&					hcdEnumDcpApi,
						const Oscl::Usb::Desc::Device&		devDesc,
						const Oscl::ObjectID::RO::Api*		location
						) noexcept:
		Oscl::Mt::Itc::Dyn::Adv::Core<DynDevice>(myPapi,advRapi,advPapi),
		_myPapi(myPapi),
		_powerSAP(powerSAP),
		_powerSyncApi(powerSyncApi),
		_hcdEnumMonitorApi(hcdEnumMonitorApi),
		_hcdEnumDcpApi(hcdEnumDcpApi),
		_ctrlPipe(ctrlPipe),
		_usbAddress(myUsbAddress),
		_lowSpeedDevice(lowSpeedDevice),
		_bcdUSB(devDesc._bcdUSB),
		_bDeviceClass(devDesc._bDeviceClass),
		_bDeviceSubClass(devDesc._bDeviceSubClass),
		_bDeviceProtocol(devDesc._bDeviceProtocol),
		_bMaxPacketSize(devDesc._bMaxPacketSize),
		_idVendor(devDesc._idVendor),
		_idProduct(devDesc._idProduct),
		_bcdDevice(devDesc._bcdDevice),
		_iManufacturer(devDesc._iManufacturer),
		_iProduct(devDesc._iProduct),
		_iSerialNumber(devDesc._iSerialNumber),
		_bNumConfigurations(devDesc._bNumConfigurations),
		_locked(false),
		_reserved(false)
		{
	if(location){
		static_cast<Oscl::ObjectID::Api&>(_location)	= *location;
		}
	}

Oscl::Usb::Alloc::Power::
Req::Api::SAP&				DynDevice::getPowerSAP() noexcept{
	return _powerSAP;
	}

Oscl::Usb::Alloc::Power::
SyncApi&					DynDevice::getPowerSyncApi() noexcept{
	return _powerSyncApi;
	}

void	DynDevice::lock() noexcept{
	Oscl::Mt::Itc::SyncReturnHandler	srh;
	LockReq								req(*this,srh);
	_myPapi.postSync(req);
	}

void	DynDevice::unlock() noexcept{
	Oscl::Mt::Itc::SyncReturnHandler	srh;
	UnlockReq							req(*this,srh);
	_myPapi.postSync(req);
	}

bool	DynDevice::isReserved() noexcept{
	return _reserved;
	}

void	DynDevice::reserve() noexcept{
	if(_reserved){
		Oscl::ErrorFatal::
			logAndExit(	"Oscl::Usb::DynDevice::reserve()"
						"Programmer error:"
						"device already reserved."
						);
		}
	_reserved	= true;
	}


void	DynDevice::openDynamicService() noexcept{
	// nothing to register
	}

void	DynDevice::closeDynamicService() noexcept{
	// Since the USB will timeout transactions
	// if a device does not respond with an ACK
	// or a NAK after several retries, synchronous
	// control pipe clients will be notified of
	// device removal automatically before this
	// operation is invoked.
	_ctrlPipe.getOpenCloseSyncApi().syncClose();
	}

DynDevice&	DynDevice::getDynSrv() noexcept{
	return *this;
	}

void	DynDevice::request(LockReq& msg) noexcept{
	if(_locked){
		_pendingLocks.put(&msg);
		return;
		}
	_locked	= true;
	msg.returnToSender();
	}

void	DynDevice::request(UnlockReq& msg) noexcept{
	_locked	= false;
	msg.returnToSender();
	Oscl::Mt::Itc::SrvMsg*	req	= _pendingLocks.get();
	if(req){
		req->process();
		}
	}

