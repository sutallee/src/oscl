/*
   Copyright (C) 2007 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <new>
#include "driver.h"
#include "oscl/mt/thread.h"
#include "oscl/error/fatal.h"

using namespace Oscl::Usb::UsbHostSlave::Hub::Root;

Driver::Driver(	Oscl::Usb::UsbHostSlave::RootHub&					reg,
				Oscl::Usb::UsbHostSlave::HCD::
				RootHub::Req::Api::SAP&						hcdRhSAP,
				Oscl::Mt::Itc::PostMsgApi&					myPapi,
				PortMem*									portMem,
				unsigned									maxPorts
				) noexcept:
		Oscl::Mt::Itc::Srv::CloseSync(*this,myPapi),
		_changeResp(	hcdRhSAP.getReqApi(),
						*this,
						myPapi
						),
		_cancelAllResp(	hcdRhSAP.getReqApi(),
						*this,
						myPapi
						),
		_reg(reg),
		_rhStatus(reg.rhStatus),
		_rhDescA(reg.rhDescriptorA),
		_portMem(portMem),
		_maxPorts(maxPorts),
		_hcdRhSAP(hcdRhSAP),
		_openReq(0),
		_closeReq(0),
		_myPapi(myPapi),
		_open(false),
		_closing(false),
		_powerSAP(*this,myPapi),
		_powerSync(_powerSAP)
		{
	}

void	Driver::initialize() noexcept{
//	_rhDescA.powerSwitchModeAlwaysPowered();	// FIXME
	/* FIXME: should this threading dependency be removed? */
	Oscl::Mt::Thread::sleep(_rhDescA.powerOnToGoodTimeIn2msUnits()*2);
	unsigned	nPorts	= _rhDescA.numberOfDownstreamPorts();
	if(nPorts > _maxPorts){
		Oscl::ErrorFatal::logAndExit
		("Usb::UsbHostSlave::Hub::Root::Driver::initialize: nPorts > _maxPorts\n");
		}
	for(unsigned i=0;i<nPorts;++i){
		Port*	port	= new(&_portMem[i]) Port(	_reg.rhPortStatus[i],
													_myPapi
													);
		_ports.put(port);
		port->initialize();
		}
	_rhStatus.setGlobalPower();	// This is where the hub starts working.
	_hcdRhSAP.post(_changeResp.getSrvMsg());
	}

Oscl::Usb::Hub::Port::
Req::Api::SAP*	Driver::getPortSAP(unsigned portNum) noexcept{
	Port*	next	= _ports.first();
	for(unsigned i=0;(next && (i<_maxPorts));++i,next=_ports.next(next)){
		if(i==portNum){
			return &next->getHubPortSAP();
			}
		}
	return 0;
	}

Oscl::Usb::Alloc::Power::Req::Api::SAP&	Driver::getPowerSAP() noexcept{
	return _powerSAP;
	}

Oscl::Usb::Alloc::Power::SyncApi&	Driver::getPowerSyncApi() noexcept{
	return _powerSync;
	}

void	Driver::statusChange() noexcept{
	_rhStatus.updateCacheFromReg();
	for(Port* port=_ports.first();port;port=_ports.next(port)){
		port->statusChange();
		}
	_hcdRhSAP.post(_changeResp.getSrvMsg());
	}

void	Driver::localPowerErrorAsserted() noexcept{
	}

void	Driver::localPowerErrorNegated() noexcept{
	}

void	Driver::overcurrentIndicatorAsserted() noexcept{
	}

void	Driver::overcurrentIndicatorNegated() noexcept{
	}

void	Driver::request(	Oscl::Mt::Itc::Srv::
							Open::Req::Api::OpenReq&	msg
							) noexcept{
	if(_openReq) while(true);
	_openReq	= &msg;
	HCD::RootHub::Resp::Api::OpenResp*	resp =
		new (&_openCloseMem)
			HCD::RootHub::Resp::Api::OpenResp(	_hcdRhSAP.getReqApi(),
											*this,
											_myPapi
											);
	_hcdRhSAP.post(resp->getSrvMsg());
	}

void	Driver::request(	Oscl::Mt::Itc::Srv::
							Close::Req::Api::CloseReq&	msg
							) noexcept{
	_closing	= true;
	_closeReq	= &msg;
	_hcdRhSAP.post(_cancelAllResp.getSrvMsg());
	}

void	Driver::request(	Alloc::Power::
							Req::Api::ReserveReq&	msg
							) noexcept{
	// FIXME: do real power allocation
	msg.getPayload()._failed	= false;
	msg.returnToSender();
	}

void	Driver::request(	Alloc::Power::
							Req::Api::FreeReq&		msg
							) noexcept{
	// FIXME: do real power free
	msg.returnToSender();
	}

void	Driver::request(	Alloc::Power::
							Req::Api::ChangeReq&	msg
							) noexcept{
	// FIXME:
	while(true);
	}

void	Driver::request(	Alloc::Power::
							Req::Api::CancelReq&	msg
							) noexcept{
	// FIXME:
	while(true);
	}

void	Driver::response(HCD::RootHub::Resp::Api::OpenResp& msg) noexcept{
	if(_closing) return;
	_open	= true;
	initialize();
	_openReq->returnToSender();
	// This close request is actually a close notification
	// request. It is returned by the HCD when the HCD
	// is closed, or the HCD receives a CancelAll request.
	HCD::RootHub::Resp::Api::CloseResp*	resp =
		new (&_openCloseMem)
			HCD::RootHub::Resp::Api::CloseResp(	_hcdRhSAP.getReqApi(),
											*this,
											_myPapi
											);
	_hcdRhSAP.post(resp->getSrvMsg());
	}

void	Driver::response(HCD::RootHub::Resp::Api::CloseResp& msg) noexcept{
	_open	= false;
	if(_closing) return;
	}

void	Driver::response(HCD::RootHub::Resp::Api::ChangeResp& msg) noexcept{
	if(_closing) return;
	statusChange();
	}

void	Driver::response(HCD::RootHub::Resp::Api::CancelAllResp& msg) noexcept{
	_closing	= false;
	_closeReq->returnToSender();
	}

