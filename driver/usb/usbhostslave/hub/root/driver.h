/*
   Copyright (C) 2007 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_usb_usbhostslave_hub_root_driverh_
#define _oscl_drv_usb_usbhostslave_hub_root_driverh_
#include "oscl/hw/usb/usbhostslave/reg.h"
#include "oscl/driver/usb/ohci/reg/rhstatus.h"
#include "oscl/driver/usb/ohci/reg/rhdesca.h"
#include "oscl/memory/block.h"
#include "oscl/queue/queue.h"
#include "port.h"
#include "oscl/driver/usb/usbhostslave/hcd/rhrespapi.h"
#include "oscl/mt/itc/srv/close.h"
#include "oscl/driver/usb/alloc/power/reqapi.h"
#include "oscl/driver/usb/alloc/power/sync.h"

/** */
namespace Oscl {
/** */
namespace Usb {
/** */
namespace UsbHostSlave {
/** */
namespace Hub {
/** */
namespace Root {

/** */
class Driver :	public Oscl::Mt::Itc::Srv::CloseSync,
				public Oscl::Mt::Itc::Srv::Close::Req::Api,
				public Oscl::Usb::Alloc::Power::Req::Api,
				public HCD::RootHub::Resp::Api
				{
	private:
		/** */
		typedef union OpenCloseMem {
			/** */
			Oscl::Memory::AlignedBlock
				<sizeof(HCD::RootHub::Resp::Api::OpenResp)>		open;
			/** */
			Oscl::Memory::AlignedBlock
				<sizeof(HCD::RootHub::Resp::Api::CloseResp)>	close;
			} OpenCloseMem;

	public:
		/** */
		typedef Oscl::Memory::AlignedBlock<sizeof(Port)>		PortMem;

	private:
		/** */
		OpenCloseMem									_openCloseMem;
		/** */
		HCD::RootHub::Resp::Api::ChangeResp				_changeResp;
		/** */
		HCD::RootHub::Resp::Api::CancelAllResp			_cancelAllResp;
		/** */
		Oscl::Usb::UsbHostSlave::RootHub&						_reg;
		/** */
		Oscl::Usb::UsbHostSlave::Register::Host::ConnectState	_rhStatus;
		/** */
		Oscl::Usb::UsbHostSlave::Register::RootHubDescA			_rhDescA;
		/** */
		Oscl::Memory::AlignedBlock<sizeof(Port)>*		_portMem;
		/** */
		unsigned										_maxPorts;
		/** */
		Oscl::Queue<Port>								_ports;
		/** */
		Oscl::Usb::UsbHostSlave::HCD::RootHub::Req::Api::SAP&	_hcdRhSAP;
		/** */
		Oscl::Mt::Itc::Srv::Open::Req::Api::OpenReq*	_openReq;
		/** */
		Oscl::Mt::Itc::Srv::Close::Req::Api::CloseReq*	_closeReq;
		/** */
		Oscl::Mt::Itc::PostMsgApi&						_myPapi;
		/** */
		bool											_open;
		/** */
		bool											_closing;
		/** */
		Oscl::Usb::Alloc::Power::Req::Api::ConcreteSAP	_powerSAP;
		/** */
		Oscl::Usb::Alloc::Power::Sync					_powerSync;

	public:
		/** */
		Driver(	Oscl::Usb::UsbHostSlave::RootHub&					reg,
				Oscl::Usb::UsbHostSlave::HCD::
				RootHub::Req::Api::SAP&						hcdRhSAP,
				Oscl::Mt::Itc::PostMsgApi&					myPapi,
				PortMem*									portMem,
				unsigned									maxPorts
				) noexcept;

		/** */
		Oscl::Usb::Hub::Port::
		Req::Api::SAP*		getPortSAP(unsigned portNum) noexcept;

		/** */
		Oscl::Usb::Alloc::Power::Req::Api::SAP&	getPowerSAP() noexcept;
		/** */
		Oscl::Usb::Alloc::Power::SyncApi&	getPowerSyncApi() noexcept;

	private:
		/** */
		void	initialize() noexcept;

		/** */
		void	statusChange() noexcept;

	private:
		/** */
		void	localPowerErrorAsserted() noexcept;
		/** */
		void	localPowerErrorNegated() noexcept;
		/** */
		void	overcurrentIndicatorAsserted() noexcept;
		/** */
		void	overcurrentIndicatorNegated() noexcept;

	private:	// Oscl::Mt::Itc::Srv::Close::Req::Api
		/** */
		void	request(	Oscl::Mt::Itc::Srv::
							Open::Req::Api::OpenReq&	msg
							) noexcept;
		/** */
		void	request(	Oscl::Mt::Itc::Srv::
							Close::Req::Api::CloseReq&	msg
							) noexcept;

	private:	// Oscl::Usb::Alloc::Power::Req::Api
		/** */
		void	request(	Alloc::Power::
							Req::Api::ReserveReq&	msg
							) noexcept;
		/** */
		void	request(	Alloc::Power::
							Req::Api::FreeReq&		msg
							) noexcept;
		/** */
		void	request(	Alloc::Power::
							Req::Api::ChangeReq&	msg
							) noexcept;
		/** */
		void	request(	Alloc::Power::
							Req::Api::CancelReq&	msg
							) noexcept;

	private:	// HCD::RootHub::Resp::Api
		/** */
		void	response(HCD::RootHub::Resp::Api::OpenResp& msg) noexcept;
		/** */
		void	response(HCD::RootHub::Resp::Api::CloseResp& msg) noexcept;
		/** */
		void	response(HCD::RootHub::Resp::Api::ChangeResp& msg) noexcept;
		/** */
		void	response(HCD::RootHub::Resp::Api::CancelAllResp& msg) noexcept;
	};

}
}
}
}
}


#endif
