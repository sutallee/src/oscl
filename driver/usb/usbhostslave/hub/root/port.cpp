/*
   Copyright (C) 2007 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <new>
#include "port.h"
#include "oscl/mt/thread.h"

using namespace Oscl::Usb::UsbHostSlave::Hub::Root;

Port::Port(	volatile Oscl::Hw::Usb::UsbHostSlave::
			HOST_RX_CONNECT_STATE::Reg&					portStatusReg,
			volatile Oscl::Hw::Usb::UsbHostSlave::
			HOST_TX_LINE_CONTROL::Reg&					lineControlReg,
			Oscl::Mt::Itc::PostMsgApi&					myPapi
			) noexcept:
		_portStatus(portStatusReg),
		_hubPortSAP(*this,myPapi),
		_lineControl(lineControlReg)
		{
	_portChangeStatus.clearBits(~0);

	// USB HOST SLAVE constants
	_portReset.setDminusLow()
	_portReset.setDplusLow()
	_portReset.enableDirectControl()

	_fullSpeed.disableDirectControl()
	_fullSpeed.selectFullSpeedLinePolarity();
	_fullSpeed.selectFullSpeedLineRate();

	_lowSpeed.disableDirectControl()
	_lowSpeed.selectLowSpeedLinePolarity();
	_lowSpeed.selectLowSpeedLineRate();
	}

void	Port::initialize() noexcept{
	// called by Root Hub Driver
	}

Oscl::Usb::Hub::Port::Req::Api::SAP&	Port::getHubPortSAP() noexcept{
	return _hubPortSAP;
	}

void	Port::statusChange() noexcept{
	_portStatus.updateCacheFromReg();

	using namespace Oscl::Usb::Hw::Hub::Port::wPortChange;
	Reg		value	=0;
	if(_portStatus.currentConnectStatusChange()){
		_portStatus.clearCurrentConnectStatusChange();
		value	|= Connection::ValueMask_Changed;
		}
	if(_portStatus.portEnableStatusChange()){
		_portStatus.clearPortEnableStatusChange();
		value	|= Enable::ValueMask_Disabled;
		}
	if(_portStatus.portSuspendStatusChange()){
		_portStatus.clearPortSuspendStatusChange();
		value	|= Suspend::ValueMask_ResumeDone;
		}
	if(_portStatus.portOverCurrentIndicatorChange()){
		_portStatus.clearPortOverCurrentIndicatorChange();
		value	|= OverCurrent::ValueMask_Changed;
		}
	if(_portStatus.portResetStatusChange()){
		_portStatus.clearPortResetStatusChange();
		value	|= Reset::ValueMask_ResetDone;
		}
	_portChangeStatus.setBits(value);
	if(_portChangeStatus.anySet(~0)){
		NotifyPortChangeReq*				next;
		while((next=_notificationList.get())){
			next->_payload._lastChange	= _portChangeStatus;
			next->returnToSender();
			}
		}
	}

Oscl::Usb::UsbHostSlave::Register::Host::TX::LineControlCache
void	Port::request(SetResetReq& msg) noexcept{
	_portStatus.setPortReset();
// For the USB HOST SLAVE HC, we need to do the reset timing
// ourselves.
// At this point, we should:
//	o _lineControl.updateRegFromCache(_portReset);
//	o wait 50ms (asynchronously)
//	o _lineControl.updateRegFromCache(_fullSpeed);
//	o _portChangeStatus.setBits(Reset::ValueMask_ResetDone);
//	o notify all NotifyPortChangeReq

	msg.returnToSender();
	}

void	Port::request(SetSuspendReq& msg) noexcept{
	_portStatus.setPortSuspend();
	msg.returnToSender();
	}

void	Port::request(SetPowerReq& msg) noexcept{
	_portStatus.setPortPower();
	msg.returnToSender();
	}

void	Port::request(ClearEnableReq& msg) noexcept{
	_portStatus.clearPortEnable();
	msg.returnToSender();
	}

void	Port::request(ClearSuspendReq& msg) noexcept{
	_portStatus.clearSuspendStatus();
	msg.returnToSender();
	}

void	Port::request(ClearPowerReq& msg) noexcept{
	_portStatus.clearPortPower();
	msg.returnToSender();
	}

void	Port::request(ClearConnectChangeReq& msg) noexcept{
	using namespace Oscl::Usb::Hw::Hub::Port::wPortChange;
	_portChangeStatus.clearBits(Connection::ValueMask_Changed);
	msg.returnToSender();
	}

void	Port::request(ClearResetChangeReq& msg) noexcept{
	using namespace Oscl::Usb::Hw::Hub::Port::wPortChange;
	_portChangeStatus.clearBits(Reset::ValueMask_ResetDone);
	msg.returnToSender();
	}

void	Port::request(ClearEnableChangeReq& msg) noexcept{
	using namespace Oscl::Usb::Hw::Hub::Port::wPortChange;
	_portChangeStatus.clearBits(Enable::ValueMask_Disabled);
	msg.returnToSender();
	}

void	Port::request(ClearSuspendChangeReq& msg) noexcept{
	using namespace Oscl::Usb::Hw::Hub::Port::wPortChange;
	_portChangeStatus.clearBits(Suspend::ValueMask_ResumeDone);
	msg.returnToSender();
	}

void	Port::request(ClearOverCurrentChangeReq& msg) noexcept{
	using namespace Oscl::Usb::Hw::Hub::Port::wPortChange;
	_portChangeStatus.clearBits(OverCurrent::ValueMask_Changed);
	msg.returnToSender();
	}

void	Port::request(CancelReq& msg) noexcept{
	// Since all of the "normal" requests since to this
	// UsbHostSlave implementation are performed synchronously
	// by manipulating the registers. This requst is
	// not used for canceling the notification requests.
	// Therefore, we simply need to return this
	// request to the sender immediately.
	msg.returnToSender();
	}

void	Port::request(NotifyPortChangeReq& msg) noexcept{
	_notificationList.put(&msg);
	statusChange();
	}

void	Port::request(CancelNotifyPortChangeReq& msg) noexcept{
	if(_notificationList.remove(&msg._payload._reqToCancel)){
		msg._payload._reqToCancel.returnToSender();
		}
	msg.returnToSender();
	}

void	Port::request(GetPortStatusReq& msg) noexcept{
	// Convert UsbHostSlave port status to USB port status
	using namespace Oscl::Usb::Hw::Hub::Port::wPortStatus;
	Oscl::Usb::Hw::Hub::Port::wPortStatus::
	Reg		stat	= msg._payload._status;
	Oscl::BitField<Oscl::Usb::Hw::Hub::Port::wPortStatus::Reg>	status;
	status	= 0;
	status.changeBits(~0,stat);
	_portStatus.update();
	Reg		mask	= (		Connect::FieldMask
						|	Enable::FieldMask
						|	Suspend::FieldMask
						|	OverCurrent::FieldMask
						|	Reset::FieldMask
						|	DeviceSpeed::FieldMask
						);
	Reg		value	=	(		(_portStatus.deviceConnected()
									?(Reg)Connect::ValueMask_DevicePresent
									:(Reg)Connect::ValueMask_DeviceAbsent
									)
							|	(_portStatus.portEnabled()
									?(Reg)Enable::ValueMask_Enabled
									:(Reg)Enable::ValueMask_Disabled
									)
							|	(_portStatus.portSuspended()
									?(Reg)Suspend::ValueMask_Suspended
									:(Reg)Suspend::ValueMask_NotSuspended
									)
							|	(_portStatus.overcurrent()
									?(Reg)OverCurrent::ValueMask_Asserted
									:(Reg)OverCurrent::ValueMask_Negated
									)
							|	(_portStatus.lowSpeedDeviceAttached()
									?(Reg)DeviceSpeed::ValueMask_LowSpeed
									:(Reg)DeviceSpeed::ValueMask_FullSpeed
									)
							);
	status.changeBits(mask,value);
	msg._payload._status	= status;
	msg.returnToSender();
	}

