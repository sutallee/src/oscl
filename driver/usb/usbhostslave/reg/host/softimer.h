/*
   Copyright (C) 2007 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_usb_usbhostslave_reg_host_softimerh_
#define _oscl_drv_usb_usbhostslave_reg_host_softimerh_
#include "oscl/hw/usb/usbhostslave/reg.h"
#include "oscl/bits/bitfield.h"

/** */
namespace Oscl {
/** */
namespace Usb {
/** */
namespace UsbHostSlave {
/** */
namespace Register {
/** */
namespace Host {

/** This driver class provides an interface to manipulate the
	HOST_SOF_TIMER_MSB register of the UsbHostSlave device.
	From Specification:
	This register contains the MSB of the SOF timer used
	for SOF transmissiion. The timer is incremented at 48MHz,
	thus there are 48K ticks in a 1ms frame.
	
	This register can be used to calculate the number of ticks
	remaining in a frame (Tremaining).

	Tremaining = 0xbb - HOST_SOF_TIMER_MSB
 */
class SofTimer {
	private:
		/** */
		volatile Oscl::Hw::Usb::UsbHostSlave::HOST_SOF_TIMER_MSB::Reg&	_reg;

	public:
		/** */
		inline SofTimer(	volatile Oscl::Hw::Usb::UsbHostSlave::
							HOST_SOF_TIMER_MSB::Reg&				reg
							) noexcept:
				_reg(reg)
				{
			}

	public:
		/** */
		inline uint8_t	rawSofTimerMsb() noexcept{
			return _reg;
			}

		/** FIXME: Can the value of the register exceed 0xBB? If so,
			there may be wrapping to an unsigned 8 bit "negative"
			value?
		 */
		inline uint8_t	ticksRemainingInFrame() noexcept{
			return 0xBB-_reg;
			}
	};

}
}
}
}
}

#endif
