/*
   Copyright (C) 2007 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_usb_usbhostslave_reg_host_interruptstatush_
#define _oscl_drv_usb_usbhostslave_reg_host_interruptstatush_
#include "oscl/hw/usb/usbhostslave/reg.h"
#include "oscl/bits/bitfield.h"

/** */
namespace Oscl {
/** */
namespace Usb {
/** */
namespace UsbHostSlave {
/** */
namespace Register {
/** */
namespace Host {

/** This driver class provides an interface to manipulate the
	HOST_INTERRUPT_STATUS register of the UsbHostSlave device.
	This driver maintains a cache copy of the register contents and the
	actual register is only read when updateCacheFromReg() is invoked.
	All other read operations read the cached value.
	Clear operations only affect the register and not the cached
	value.
 */
class InterruptStatus {
	private:
		/** */
		volatile Oscl::Hw::Usb::UsbHostSlave::HOST_INTERRUPT_STATUS::Reg&		_reg;

		/** */
		Oscl::BitField<	Oscl::Hw::Usb::
						UsbHostSlave::HOST_INTERRUPT_STATUS::Reg
						>											_cache;

	public:
		/** */
		inline InterruptStatus(	volatile Oscl::Hw::Usb::UsbHostSlave::
								HOST_INTERRUPT_STATUS::Reg&					reg
								) noexcept:
				_reg(reg)
				{
			}

		/** */
		inline void	updateCacheFromReg() noexcept{
			_cache	= _reg;
			}

		/** This operation clears/acknowledges any and all pending
			interrupt indications whether or not they are coherent
			with the cached copy.
		 */
		inline void	unconditionallyClearAllPendingInterrupts() noexcept{
			// Write all ones
			_reg	= (Oscl::Hw::Usb::UsbHostSlave::HOST_INTERRUPT_STATUS::Reg)~0U;
			}

		/** This operation clears/acknowledges all of the interrupts
			that are indicated as pending in the _cache.
		 */
		inline void	clearAllInterruptsPendingInCache() noexcept{
			_reg	= _cache;
			}

	public: // TRANS_DONE field
		/** */
		inline bool	transactionComplete() noexcept{
			using namespace Oscl::Hw::Usb::UsbHostSlave::HOST_INTERRUPT_STATUS;
			return _cache.equal(	TRANS_DONE::FieldMask,
									TRANS_DONE::ValueMask_TransactionComplete
									);
			}

		/** */
		inline void	clearTransactionComplete() noexcept{
			using namespace Oscl::Hw::Usb::UsbHostSlave::HOST_INTERRUPT_STATUS;
			_reg	= TRANS_DONE::ValueMask_Clear;
			}

	public: // RESUME_INT field
		/** */
		inline bool	resumeStateDetected() noexcept{
			using namespace Oscl::Hw::Usb::UsbHostSlave::HOST_INTERRUPT_STATUS;
			return _cache.equal(	RESUME_INT::FieldMask,
									RESUME_INT::ValueMask_ResumeStateDetected
									);
			}

		/** */
		inline void	clearResumeStateDetected() noexcept{
			using namespace Oscl::Hw::Usb::UsbHostSlave::HOST_INTERRUPT_STATUS;
			_reg	= RESUME_INT::ValueMask_Clear;
			}

	public: // CONNECTION_EVENT field
		/** */
		inline bool	connectStateChanged() noexcept{
			using namespace Oscl::Hw::Usb::UsbHostSlave::HOST_INTERRUPT_STATUS;
			return _cache.equal(	CONNECTION_EVENT::FieldMask,
									CONNECTION_EVENT::ValueMask_ConnectStateChanged
									);
			}

		/** */
		inline void	clearConnectStateChanged() noexcept{
			using namespace Oscl::Hw::Usb::UsbHostSlave::HOST_INTERRUPT_STATUS;
			_reg	= CONNECTION_EVENT::ValueMask_Clear;
			}

	public: // SOF_SENT field
		/** */
		inline bool	sofSent() noexcept{
			using namespace Oscl::Hw::Usb::UsbHostSlave::HOST_INTERRUPT_STATUS;
			return _cache.equal(	SOF_SENT::FieldMask,
									SOF_SENT::ValueMask_SofSent
									);
			}

		/** */
		inline void	clearSofSent() noexcept{
			using namespace Oscl::Hw::Usb::UsbHostSlave::HOST_INTERRUPT_STATUS;
			_reg	= SOF_SENT::ValueMask_Clear;
			}
	};

}
}
}
}
}
#endif
