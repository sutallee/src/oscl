/*
   Copyright (C) 2007 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_usb_usbhostslave_reg_host_rx_statush_
#define _oscl_drv_usb_usbhostslave_reg_host_rx_statush_
#include "oscl/hw/usb/usbhostslave/reg.h"
#include "oscl/bits/bitfield.h"

/** */
namespace Oscl {
/** */
namespace Usb {
/** */
namespace UsbHostSlave {
/** */
namespace Register {
/** */
namespace Host {
/** */
namespace RX {

/** This driver class provides an interface to manipulate the HOST_RX_STATUS
	register of the UsbHostSlave device. This driver maintains a cache copy
	of the register contents and the actual register is only read when
	updateCacheFromReg() is invoked.
	All other operations read the cached value.
 */
class Status {
	private:
		/** */
		volatile Oscl::Hw::Usb::UsbHostSlave::HOST_RX_STATUS::Reg&		_reg;

		/** */
		Oscl::BitField<	Oscl::Hw::Usb::
						UsbHostSlave::HOST_RX_STATUS::Reg
						>												_cache;

	public:
		/** */
		inline Status(	volatile Oscl::Hw::Usb::UsbHostSlave::
						HOST_RX_STATUS::Reg&					reg
						) noexcept:
				_reg(reg)
				{
			}

		/** */
		inline void	updateCacheFromReg() noexcept{
			_cache	= _reg;
			}

	public: // CRC_ERROR field
		/** */
		inline bool	crcErrorDetectedInLastTransaction() noexcept{
			using namespace Oscl::Hw::Usb::UsbHostSlave::HOST_RX_STATUS;
			return _cache.equal(	CRC_ERROR::FieldMask,
									CRC_ERROR::ValueMask_Error
									);
			}

	public: // BIT_STUFF_ERROR field
		/** */
		inline bool	bitStuffErrorDetectedInLastTransaction() noexcept{
			using namespace Oscl::Hw::Usb::UsbHostSlave::HOST_RX_STATUS;
			return _cache.equal(	BIT_STUFF_ERROR::FieldMask,
									BIT_STUFF_ERROR::ValueMask_Error
									);
			}

	public: // RX_OVERFLOW field
		/** */
		inline bool	rxOverflowDetectedInLastTransaction() noexcept{
			using namespace Oscl::Hw::Usb::UsbHostSlave::HOST_RX_STATUS;
			return _cache.equal(	RX_OVERFLOW::FieldMask,
									RX_OVERFLOW::ValueMask_Error
									);
			}

	public: // RX_TIME_OUT field
		/** */
		inline bool	rxTimeoutAfterLastTransaction() noexcept{
			using namespace Oscl::Hw::Usb::UsbHostSlave::HOST_RX_STATUS;
			return _cache.equal(	RX_TIME_OUT::FieldMask,
									RX_TIME_OUT::ValueMask_Timeout
									);
			}

	public: // NAK_RXED field
		/** */
		inline bool	nakReceivedForLastTransaction() noexcept{
			using namespace Oscl::Hw::Usb::UsbHostSlave::HOST_RX_STATUS;
			return _cache.equal(	NAK_RXED::FieldMask,
									NAK_RXED::ValueMask_ReceivedNAK
									);
			}

	public: // STALL_RXED field
		/** */
		inline bool	stallReceivedForLastTransaction() noexcept{
			using namespace Oscl::Hw::Usb::UsbHostSlave::HOST_RX_STATUS;
			return _cache.equal(	STALL_RXED::FieldMask,
									STALL_RXED::ValueMask_ReceivedSTALL
									);
			}

	public: // ACK_RXED field
		/** */
		inline bool	ackReceivedForLastTransaction() noexcept{
			using namespace Oscl::Hw::Usb::UsbHostSlave::HOST_RX_STATUS;
			return _cache.equal(	ACK_RXED::FieldMask,
									ACK_RXED::ValueMask_ReceivedACK
									);
			}

	public: // DATA_SEQUENCE_BIT field
		/** Returns true if the last data sequence bit was DATA1
			or false if the last data sequence bit was DATA0. This
			is only valid if the last transaction was an IN type.
		 */
		inline bool	receivedDATA1() noexcept{
			using namespace Oscl::Hw::Usb::UsbHostSlave::HOST_RX_STATUS;
			return _cache.equal(	DATA_SEQUENCE_BIT::FieldMask,
									DATA_SEQUENCE_BIT::ValueMask_DATA1
									);
			}
	};

}
}
}
}
}
}
#endif
