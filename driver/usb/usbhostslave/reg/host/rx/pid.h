/*
   Copyright (C) 2007 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_usb_usbhostslave_reg_host_rx_pidh_
#define _oscl_drv_usb_usbhostslave_reg_host_rx_pidh_
#include "oscl/hw/usb/usbhostslave/reg.h"
#include "oscl/bits/bitfield.h"

/** */
namespace Oscl {
/** */
namespace Usb {
/** */
namespace UsbHostSlave {
/** */
namespace Register {
/** */
namespace Host {
/** */
namespace RX {

/** */
class PidVisitor {
	public:
		/** */
		virtual ~PidVisitor() {}
		/** */
		virtual void	outPID() noexcept=0;
		/** */
		virtual void	inPID() noexcept=0;
		/** */
		virtual void	sofPID() noexcept=0;
		/** */
		virtual void	setupPID() noexcept=0;
		/** */
		virtual void	data0PID() noexcept=0;
		/** */
		virtual void	data1PID() noexcept=0;
		/** */
		virtual void	ackPID() noexcept=0;
		/** */
		virtual void	nakPID() noexcept=0;
		/** */
		virtual void	stallPID() noexcept=0;
		/** */
		virtual void	prePID() noexcept=0;
		/** */
		virtual void	unknownPID() noexcept=0;
	};

/** */
class PidGroupVisitor {
	public:
		/** */
		virtual ~PidGroupVisitor() {}
		/** */
		virtual void	tokenPID() noexcept=0;
		/** */
		virtual void	dataPID() noexcept=0;
		/** */
		virtual void	handshakePID() noexcept=0;
		/** */
		virtual void	specialPID() noexcept=0;
	};

/** This driver class provides an interface to manipulate the HOST_RX_PID
	register of the UsbHostSlave device. This driver maintains a cache copy
	of the register contents and the actual register is only read when
	updateCacheFromReg() is invoked.
	All other operations read the cached value.
 */
class PID {
	private:
		/** */
		volatile Oscl::Hw::Usb::UsbHostSlave::HOST_RX_PID::Reg&		_reg;

		/** */
		Oscl::BitField<	Oscl::Hw::Usb::
						UsbHostSlave::HOST_RX_PID::Reg
						>											_cache;

	public:
		/** */
		inline PID(	volatile Oscl::Hw::Usb::UsbHostSlave::
						HOST_RX_PID::Reg&					reg
						) noexcept:
				_reg(reg)
				{
			}

		/** */
		inline void	updateCacheFromReg() noexcept{
			_cache	= _reg;
			}

	public: // RECEIVE_PID field
		/** */
		inline void	visit(PidVisitor& visitor) noexcept{
			using namespace Oscl::Hw::Usb::UsbHostSlave::HOST_RX_PID;
			switch(_cache & RECEIVE_PID::FieldMask){
				case RECEIVE_PID::ValueMask_OUT:
					visitor.outPID();
					break;
				case RECEIVE_PID::ValueMask_IN:
					visitor.inPID();
					break;
				case RECEIVE_PID::ValueMask_SOF:
					visitor.sofPID();
					break;
				case RECEIVE_PID::ValueMask_SETUP:
					visitor.setupPID();
					break;
				case RECEIVE_PID::ValueMask_DATA0:
					visitor.data0PID();
					break;
				case RECEIVE_PID::ValueMask_DATA1:
					visitor.data1PID();
					break;
				case RECEIVE_PID::ValueMask_ACK:
					visitor.ackPID();
					break;
				case RECEIVE_PID::ValueMask_NAK:
					visitor.nakPID();
					break;
				case RECEIVE_PID::ValueMask_STALL:
					visitor.stallPID();
					break;
				case RECEIVE_PID::ValueMask_PRE:
					visitor.prePID();
					break;
				default:
					visitor.unknownPID();
				};
			}

	public: // RECEIVE_PID::CodingGroup field
		/** */
		inline void	visit(PidGroupVisitor& visitor) noexcept{
			using namespace Oscl::Hw::Usb::UsbHostSlave::HOST_RX_PID;
			switch(_cache & RECEIVE_PID::CodingGroup::FieldMask){
				case RECEIVE_PID::CodingGroup::ValueMask_Token:
					visitor.tokenPID();
					break;
				case RECEIVE_PID::CodingGroup::ValueMask_Data:
					visitor.dataPID();
					break;
				case RECEIVE_PID::CodingGroup::ValueMask_Handshake:
					visitor.handshakePID();
					break;
				case RECEIVE_PID::CodingGroup::ValueMask_Special:
					visitor.specialPID();
					break;
				};
			}
	};

}
}
}
}
}
}
#endif
