/*
   Copyright (C) 2007 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_usb_usbhostslave_reg_host_rx_fifodatacounth_
#define _oscl_drv_usb_usbhostslave_reg_host_rx_fifodatacounth_
#include "oscl/hw/usb/usbhostslave/reg.h"
#include "oscl/bits/bitfield.h"

/** */
namespace Oscl {
/** */
namespace Usb {
/** */
namespace UsbHostSlave {
/** */
namespace Register {
/** */
namespace Host {
/** */
namespace RX {

/** This driver class provides an interface to manipulate the
	HOST_RX_FIFO_DATA_COUNT_LSB and HOST_RX_FIFO_DATA_COUNT_MSB
	register of the UsbHostSlave device.
 */
class FifoDataCount {
	private:
		/** */
		volatile	Oscl::Hw::Usb::
					UsbHostSlave::HOST_RX_FIFO_COUNT_LSB::Reg&	_lsbReg;

		/** */
		volatile	Oscl::Hw::Usb::
					UsbHostSlave::HOST_RX_FIFO_COUNT_MSB::Reg&	_msbReg;

	public:
		/** */
		inline FifoDataCount(	volatile Oscl::Hw::Usb::UsbHostSlave::
								HOST_RX_FIFO_COUNT_LSB::Reg&		lsbReg,
								volatile Oscl::Hw::Usb::UsbHostSlave::
								HOST_RX_FIFO_COUNT_MSB::Reg&		msbReg
								) noexcept:
				_lsbReg(lsbReg),
				_msbReg(msbReg)
				{
			}

	public:
		/** */
		inline uint16_t	count() noexcept{
			uint16_t	value;
			value	= _msbReg;
			value	<<=	8;
			value	|=	_lsbReg;
			return value;
			}

		/** */
		inline uint8_t	countLSB() noexcept{
			return _lsbReg;
			}

		/** */
		inline uint8_t	countMSB() noexcept{
			return _msbReg;
			}
	};

}
}
}
}
}
}
#endif
