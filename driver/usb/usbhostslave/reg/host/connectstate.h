/*
   Copyright (C) 2007 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_usb_usbhostslave_reg_host_connectstatush_
#define _oscl_drv_usb_usbhostslave_reg_host_connectstatush_
#include "oscl/hw/usb/usbhostslave/reg.h"
#include "oscl/bits/bitfield.h"

/** */
namespace Oscl {
/** */
namespace Usb {
/** */
namespace UsbHostSlave {
/** */
namespace Register {
/** */
namespace Host {

/** This interface is a visitor pattern callback
	for obtaining the current connect state of the
	port.
 */
class ConnectStateVisitor {
	public:
		/** */
		virtual ~ConnectStateVisitor() {}
		/** */
		virtual void	disconnect() noexcept=0;
		/** */
		virtual void	lowSpeedConnect() noexcept=0;
		/** */
		virtual void	fullSpeedConnect() noexcept=0;
		/** */
		virtual void	invalidConnectState() noexcept=0;
	};

/** This driver class provides an interface to manipulate the
	HOST_RX_CONNECT_STATE register of the UsbHostSlave device.
	This driver maintains a cache copy of the register contents and the
	actual register is only read when updateCacheFromReg() is invoked.
	All other operations read the cached value.
 */
class ConnectState {
	private:
		/** */
		volatile	Oscl::Hw::Usb::
					UsbHostSlave::HOST_RX_CONNECT_STATE::Reg&	_reg;

		/** */
		Oscl::BitField<	Oscl::Hw::Usb::
						UsbHostSlave::HOST_RX_CONNECT_STATE::Reg
						>											_cache;

	public:
		/** */
		inline ConnectState(	volatile Oscl::Hw::Usb::UsbHostSlave::
								HOST_RX_CONNECT_STATE::Reg&					reg
								) noexcept:
				_reg(reg)
				{
			}

		/** */
		inline void	updateCacheFromReg() noexcept{
			_cache	= _reg;
			}

	public: // RX_LINE_STATE field
		/** */
		inline void	visit(ConnectStateVisitor& visitor) noexcept{
			using namespace Oscl::Hw::Usb::UsbHostSlave::HOST_RX_CONNECT_STATE;
			switch(_cache & RX_LINE_STATE::FieldMask){
				case RX_LINE_STATE::ValueMask_Disconnect:
					visitor.disconnect();
					break;
				case RX_LINE_STATE::ValueMask_LowSpeedConnect:
					visitor.lowSpeedConnect();
					break;
				case RX_LINE_STATE::ValueMask_FullSpeedConnect:
					visitor.fullSpeedConnect();
					break;
				default:
					visitor.invalidConnectState();
				};
			}
	};

}
}
}
}
}
#endif
