/*
   Copyright (C) 2007 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_usb_usbhostslave_reg_host_interruptmaskh_
#define _oscl_drv_usb_usbhostslave_reg_host_interruptmaskh_
#include "oscl/hw/usb/usbhostslave/reg.h"

/** */
namespace Oscl {
/** */
namespace Usb {
/** */
namespace UsbHostSlave {
/** */
namespace Register {
/** */
namespace Host {

/** This driver class provides an interface to manipulate the
	HOST_INTERRUPT_MASK register of the UsbHostSlave device.
 */
class InterruptMask {
	private:
		/** */
		volatile	Oscl::Hw::Usb::
					UsbHostSlave::HOST_INTERRUPT_MASK::Reg&		_reg;

	public:
		/** */
		inline InterruptMask(	volatile Oscl::Hw::Usb::UsbHostSlave::
								HOST_INTERRUPT_MASK::Reg&				reg
							) noexcept:
				_reg(reg)
				{
			}

		/** */
		inline void	disableAllInterrupts() noexcept{
			_reg	= 0;
			}

	public: // TRANS_DONE field
		/** */
		inline void	enableTransactionCompleteInterrupt() noexcept{
			using namespace Oscl::Hw::Usb::UsbHostSlave::HOST_INTERRUPT_MASK;
			Reg		value;
			value	=	_reg;
			value	&=	~TRANS_DONE::FieldMask;
			value	|=	TRANS_DONE::ValueMask_Enable;
			_reg	= value;
			}

		/** */
		inline void	disableTransactionCompleteInterrupt() noexcept{
			using namespace Oscl::Hw::Usb::UsbHostSlave::HOST_INTERRUPT_MASK;
			Reg		value;
			value	=	_reg;
			value	&=	~TRANS_DONE::FieldMask;
			value	|=	TRANS_DONE::ValueMask_Disable;
			_reg	= value;
			}

		/** */
		inline bool	transactionCompleteInterruptEnabled() noexcept{
			using namespace Oscl::Hw::Usb::UsbHostSlave::HOST_INTERRUPT_MASK;
			return (_reg & TRANS_DONE::FieldMask) == TRANS_DONE::ValueMask_Enable;
			}

	public: // RESUME_INT field
		/** */
		inline void	enableResumeStateInterrupt() noexcept{
			using namespace Oscl::Hw::Usb::UsbHostSlave::HOST_INTERRUPT_MASK;
			Reg		value;
			value	=	_reg;
			value	&=	~RESUME_INT::FieldMask;
			value	|=	RESUME_INT::ValueMask_Enable;
			_reg	= value;
			}

		/** */
		inline void	disableResumeStateInterrupt() noexcept{
			using namespace Oscl::Hw::Usb::UsbHostSlave::HOST_INTERRUPT_MASK;
			Reg		value;
			value	=	_reg;
			value	&=	~RESUME_INT::FieldMask;
			value	|=	RESUME_INT::ValueMask_Disable;
			_reg	= value;
			}

		/** */
		inline bool	resumeStateInterruptEnabled() noexcept{
			using namespace Oscl::Hw::Usb::UsbHostSlave::HOST_INTERRUPT_MASK;
			return (_reg & RESUME_INT::FieldMask) == RESUME_INT::ValueMask_Enable;
			}

	public: // CONNECTION_EVENT field
		/** */
		inline void	enableConnectStateChangeInterrupt() noexcept{
			using namespace Oscl::Hw::Usb::UsbHostSlave::HOST_INTERRUPT_MASK;
			Reg		value;
			value	=	_reg;
			value	&=	~CONNECTION_EVENT::FieldMask;
			value	|=	CONNECTION_EVENT::ValueMask_Enable;
			_reg	= value;
			}

		/** */
		inline void	disableConnectStateChangeInterrupt() noexcept{
			using namespace Oscl::Hw::Usb::UsbHostSlave::HOST_INTERRUPT_MASK;
			Reg		value;
			value	=	_reg;
			value	&=	~CONNECTION_EVENT::FieldMask;
			value	|=	CONNECTION_EVENT::ValueMask_Disable;
			_reg	= value;
			}

		/** */
		inline bool	connectStateChangeInterruptEnabled() noexcept{
			using namespace Oscl::Hw::Usb::UsbHostSlave::HOST_INTERRUPT_MASK;
			return (_reg & CONNECTION_EVENT::FieldMask) == CONNECTION_EVENT::ValueMask_Enable;
			}

	public: // SOF_SENT field
		/** */
		inline void	enableSofSentInterrupt() noexcept{
			using namespace Oscl::Hw::Usb::UsbHostSlave::HOST_INTERRUPT_MASK;
			Reg		value;
			value	=	_reg;
			value	&=	~SOF_SENT::FieldMask;
			value	|=	SOF_SENT::ValueMask_Enable;
			_reg	= value;
			}

		/** */
		inline void	disableSofSentInterrupt() noexcept{
			using namespace Oscl::Hw::Usb::UsbHostSlave::HOST_INTERRUPT_MASK;
			Reg		value;
			value	=	_reg;
			value	&=	~SOF_SENT::FieldMask;
			value	|=	SOF_SENT::ValueMask_Disable;
			_reg	= value;
			}

		/** */
		inline bool	sofSentInterruptEnabled() noexcept{
			using namespace Oscl::Hw::Usb::UsbHostSlave::HOST_INTERRUPT_MASK;
			return (_reg & SOF_SENT::FieldMask) == SOF_SENT::ValueMask_Enable;
			}

	};

}
}
}
}
}
#endif
