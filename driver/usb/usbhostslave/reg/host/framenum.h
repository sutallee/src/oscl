/*
   Copyright (C) 2007 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_usb_usbhostslave_reg_host_framenumh_
#define _oscl_drv_usb_usbhostslave_reg_host_framenumh_
#include "oscl/hw/usb/usbhostslave/reg.h"
#include "oscl/bits/bitfield.h"

/** */
namespace Oscl {
/** */
namespace Usb {
/** */
namespace UsbHostSlave {
/** */
namespace Register {
/** */
namespace Host {

/** This driver class provides an interface to manipulate the
	HOST_FRAME_NUM_MSP and HOST_FRAME_NUM_LSP registers of the
	UsbHostSlave device.

	The HOST_FRAME_NUM_MSP and HOST_FRAME_NUM_LSP registers
	together contain the frame number used for SOF transmission.
 */
class FrameNumber {
	private:
		/** */
		volatile Oscl::Hw::Usb::UsbHostSlave::HOST_FRAME_NUM_MSP::Reg&	_mspReg;
		/** */
		volatile Oscl::Hw::Usb::UsbHostSlave::HOST_FRAME_NUM_MSP::Reg&	_lspReg;

	public:
		/** */
		inline FrameNumber(	volatile Oscl::Hw::Usb::UsbHostSlave::
							HOST_FRAME_NUM_MSP::Reg&				msp,
							volatile Oscl::Hw::Usb::UsbHostSlave::
							HOST_FRAME_NUM_LSP::Reg&				lsp
							) noexcept:
				_mspReg(msp),
				_lspReg(lsp)
				{
			}

	public:
		/** I assume the upper bits of the MSP are always zero.
		 */
		inline uint8_t	rawFrameNumberMSP() noexcept{
			return _mspReg;
			}

		/** */
		inline uint8_t	rawFrameNumberLSP() noexcept{
			return _lspReg;
			}

		/** FIXME: I *assume* the upper bits of the MSP register
			are always zero.
		 */
		inline uint16_t	frameNumber() noexcept{
			uint16_t	value;
			uint8_t		msp	= _mspReg;
			uint8_t		lsp;
			do {
				lsp	= _lspReg;
				uint8_t	msp2	= _mspReg;
				if(msp == msp2){
					break;
					}
				msp	= msp2;
				} while(true);
			value	= msp;
			value	<<= 8;
			value	|= lsp;
			return value;
			}
	};

}
}
}
}
}

#endif
