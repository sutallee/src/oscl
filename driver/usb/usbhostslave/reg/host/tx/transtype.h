/*
   Copyright (C) 2007 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_usb_usbhostslave_reg_host_tx_transtypeh_
#define _oscl_drv_usb_usbhostslave_reg_host_tx_transtypeh_
#include "oscl/hw/usb/usbhostslave/reg.h"
#include "oscl/bits/bitfield.h"

/** */
namespace Oscl {
/** */
namespace Usb {
/** */
namespace UsbHostSlave {
/** */
namespace Register {
/** */
namespace Host {
/** */
namespace TX {

/** This driver class provides an interface to manipulate the
	HOST_TX_TRANS_TYPE register of the UsbHostSlave device.
 */
class TransType {
	private:
		/** */
		volatile Oscl::Hw::Usb::UsbHostSlave::HOST_TX_TRANS_TYPE::Reg&	_reg;

	public:
		/** */
		inline TransType(	volatile Oscl::Hw::Usb::UsbHostSlave::
							HOST_TX_TRANS_TYPE::Reg&				reg
							) noexcept:
				_reg(reg)
				{
			}

	public: // TRANSACTION_TYPE field
		/** */
		inline void	selectTransactionTypeSETUP() noexcept{
			using namespace Oscl::Hw::Usb::UsbHostSlave::HOST_TX_TRANS_TYPE;
			_reg	= TRANSACTION_TYPE::ValueMask_SETUP_TRANS;
			}

		/** */
		inline void	selectInTransactionTypeIN() noexcept{
			using namespace Oscl::Hw::Usb::UsbHostSlave::HOST_TX_TRANS_TYPE;
			_reg	= TRANSACTION_TYPE::ValueMask_IN_TRANS;
			}

		/** */
		inline void	selectInTransactionTypeOUTDATA0() noexcept{
			using namespace Oscl::Hw::Usb::UsbHostSlave::HOST_TX_TRANS_TYPE;
			_reg	= TRANSACTION_TYPE::ValueMask_OUTDATA0_TRANS;
			}

		/** */
		inline void	selectInTransactionTypeOUTDATA1() noexcept{
			using namespace Oscl::Hw::Usb::UsbHostSlave::HOST_TX_TRANS_TYPE;
			_reg	= TRANSACTION_TYPE::ValueMask_OUTDATA1_TRANS;
			}
	};

}
}
}
}
}
}
#endif
