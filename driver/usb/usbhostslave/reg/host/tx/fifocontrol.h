/*
   Copyright (C) 2007 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_usb_usbhostslave_reg_host_tx_fifocontrolh_
#define _oscl_drv_usb_usbhostslave_reg_host_tx_fifocontrolh_
#include "oscl/hw/usb/usbhostslave/reg.h"
#include "oscl/bits/bitfield.h"

/** */
namespace Oscl {
/** */
namespace Usb {
/** */
namespace UsbHostSlave {
/** */
namespace Register {
/** */
namespace Host {
/** */
namespace TX {

/** This driver class provides an interface to manipulate the
	HOST_TX_FIFO_CONTROL register of the UsbHostSlave device.
 */
class FifoControl {
	private:
		/** */
		volatile Oscl::Hw::Usb::
		UsbHostSlave::HOST_TX_FIFO_CONTROL::Reg&	_reg;

	public:
		/** */
		inline FifoControl(	volatile Oscl::Hw::Usb::UsbHostSlave::
							HOST_TX_FIFO_CONTROL::Reg&				reg
							) noexcept:
				_reg(reg)
				{
			}

	public: // FIFO_FORCE_EMPTY field
		/** */
		inline void	forceFifoToEmpty() noexcept{
			using namespace Oscl::Hw::Usb::UsbHostSlave::HOST_TX_FIFO_CONTROL;
			_reg	= FIFO_FORCE_EMPTY::ValueMask_DeleteAllData;
			}
	};

}
}
}
}
}
}
#endif
