/*
   Copyright (C) 2007 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_usb_usbhostslave_reg_host_tx_linecontrolh_
#define _oscl_drv_usb_usbhostslave_reg_host_tx_linecontrolh_
#include "oscl/hw/usb/usbhostslave/reg.h"
#include "oscl/bits/bitfield.h"

/** */
namespace Oscl {
/** */
namespace Usb {
/** */
namespace UsbHostSlave {
/** */
namespace Register {
/** */
namespace Host {
/** */
namespace TX {

/** This class provides an interface to manipulate the fields of a
	HOST_TX_LINE_CONTROL register cache value. This cached configuration
	can then subsequently be written to the actual HOST_TX_LINE_CONTROL
	register as required.
 */
class LineControlCache {
	private:
		friend class LineControl;
		/** */
		Oscl::BitField<	Oscl::Hw::Usb::
						UsbHostSlave::HOST_TX_LINE_CONTROL::Reg
						>												_cache;

	public:
		/** */
		inline LineControlCache() noexcept
				{
			_cache	= 0;
			}

	public: // TX_LINE_STATE field
		/** */
		inline void	setDminusHigh() noexcept{
			using namespace Oscl::Hw::Usb::UsbHostSlave::HOST_TX_LINE_CONTROL;
			_cache.changeBits(	TX_LINE_STATE::DMINUS::FieldMask,
								TX_LINE_STATE::DMINUS::ValueMask_High
								);
			}

		/** */
		inline void	setDminusLow() noexcept{
			using namespace Oscl::Hw::Usb::UsbHostSlave::HOST_TX_LINE_CONTROL;
			_cache.changeBits(	TX_LINE_STATE::DMINUS::FieldMask,
								TX_LINE_STATE::DMINUS::ValueMask_Low
								);
			}

		/** */
		inline void	setDplusHigh() noexcept{
			using namespace Oscl::Hw::Usb::UsbHostSlave::HOST_TX_LINE_CONTROL;
			_cache.changeBits(	TX_LINE_STATE::DPLUS::FieldMask,
								TX_LINE_STATE::DPLUS::ValueMask_High
								);
			}

		/** */
		inline void	setDplusLow() noexcept{
			using namespace Oscl::Hw::Usb::UsbHostSlave::HOST_TX_LINE_CONTROL;
			_cache.changeBits(	TX_LINE_STATE::DPLUS::FieldMask,
								TX_LINE_STATE::DPLUS::ValueMask_Low
								);
			}


	public: // DIRECT_CONTROL field
		/** Enables software to directly control the state of
			the USB D+ and D- signals via the TX_LINE_STATE
			routines above.
		 */
		inline void	enableDirectControl() noexcept{
			using namespace Oscl::Hw::Usb::UsbHostSlave::HOST_TX_LINE_CONTROL;
			_cache.changeBits(	DIRECT_CONTROL::FieldMask,
								DIRECT_CONTROL::ValueMask_EnableDirectControl
								);
			}

		/** D+ and D- controls are automatic.
		 */
		inline void	disableDirectControl() noexcept{
			using namespace Oscl::Hw::Usb::UsbHostSlave::HOST_TX_LINE_CONTROL;
			_cache.changeBits(	DIRECT_CONTROL::FieldMask,
								DIRECT_CONTROL::ValueMask_Normal
								);
			}

		/** */
		inline bool	directControlEnabled() noexcept{
			using namespace Oscl::Hw::Usb::UsbHostSlave::HOST_TX_LINE_CONTROL;
			return _cache.equal(	DIRECT_CONTROL::FieldMask,
									DIRECT_CONTROL::ValueMask_EnableDirectControl
									);
			}
	public: // FULL_SPEED_LINE_POLARITY field
		/** */
		inline void	selectFullSpeedLinePolarity() noexcept{
			using namespace Oscl::Hw::Usb::UsbHostSlave::HOST_TX_LINE_CONTROL;
			_cache.changeBits(	FULL_SPEED_LINE_POLARITY::FieldMask,
								FULL_SPEED_LINE_POLARITY::ValueMask_FullSpeedLinePolarity
								);
			}

		/** */
		inline void	selectLowSpeedLinePolarity() noexcept{
			using namespace Oscl::Hw::Usb::UsbHostSlave::HOST_TX_LINE_CONTROL;
			_cache.changeBits(	FULL_SPEED_LINE_POLARITY::FieldMask,
								FULL_SPEED_LINE_POLARITY::ValueMask_LowSpeedLinePolarity
								);
			}

	public: // FULL_SPEED_LINE_RATE field
		/** */
		inline void	selectFullSpeedLineRate() noexcept{
			using namespace Oscl::Hw::Usb::UsbHostSlave::HOST_TX_LINE_CONTROL;
			_cache.changeBits(	FULL_SPEED_LINE_RATE::FieldMask,
								FULL_SPEED_LINE_RATE::ValueMask_FullSpeedLineRate
								);
			}

		/** */
		inline void	selectLowSpeedLineRate() noexcept{
			using namespace Oscl::Hw::Usb::UsbHostSlave::HOST_TX_LINE_CONTROL;
			_cache.changeBits(	FULL_SPEED_LINE_RATE::FieldMask,
								FULL_SPEED_LINE_RATE::ValueMask_LowSpeedLineRate
								);
			}

	};

/** This driver class provides an interface to manipulate the HOST_TX_LINE_CONTROL
	register of the UsbHostSlave device. This driver maintains a cache copy
	of the register contents and the actual register is only read or written
	by the use of the updateRegFromCache() and updateCacheFromReg() operations.
	All other operations manipulate the cached value.
 */
class LineControl {
	private:
		/** */
		volatile Oscl::Hw::Usb::
				UsbHostSlave::HOST_TX_LINE_CONTROL::Reg&			_reg;

		/** */
		Oscl::BitField<	Oscl::Hw::Usb::
						UsbHostSlave::HOST_TX_LINE_CONTROL::Reg
						>											_cache;

	public:
		/** */
		inline LineControl(	volatile Oscl::Hw::Usb::UsbHostSlave::
							HOST_TX_LINE_CONTROL::Reg&				reg
							) noexcept:
				_reg(reg)
				{
			}

		/** */
		inline void	updateRegFromCache(const LineControlCache& cache) noexcept{
			_reg	= _cache.read();
			}

	};

}
}
}
}
}
}
#endif
