/*
   Copyright (C) 2007 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_usb_usbhostslave_reg_host_tx_controlh_
#define _oscl_drv_usb_usbhostslave_reg_host_tx_controlh_
#include "oscl/hw/usb/usbhostslave/reg.h"
#include "oscl/bits/bitfield.h"

/** */
namespace Oscl {
/** */
namespace Usb {
/** */
namespace UsbHostSlave {
/** */
namespace Register {
/** */
namespace Host {
/** */
namespace TX {

/** This driver class provides an interface to manipulate the HOST_TX_CONTROL
	register of the UsbHostSlave device. This driver maintains a cache copy
	of the register contents and the actual register is only read or written
	by the use of the updateRegFromCache() and updateCacheFromReg() operations.
	All other operations manipulate the cached value.
 */
class Control {
	private:
		/** */
		volatile Oscl::Hw::Usb::UsbHostSlave::HOST_TX_CONTROL::Reg&		_reg;

		/** */
		Oscl::BitField<	Oscl::Hw::Usb::
						UsbHostSlave::HOST_TX_CONTROL::Reg
						>												_cache;

	public:
		/** */
		inline Control(	volatile Oscl::Hw::Usb::UsbHostSlave::
						HOST_TX_CONTROL::Reg&					reg
						) noexcept:
				_reg(reg)
				{
			updateCacheFromReg();
			}

		/** */
		inline void	updateRegFromCache() noexcept{
			_reg	= _cache;
			}

		/** */
		inline void	updateCacheFromReg() noexcept{
			_cache	= _reg;
			}

	public: // TRANS_REQ field
		/** */
		inline void	enableTransactionRequest() noexcept{
			using namespace Oscl::Hw::Usb::UsbHostSlave::HOST_TX_CONTROL;
			_cache.changeBits(	TRANS_REQ::FieldMask,
								TRANS_REQ::ValueMask_Enable
								);
			}

		/** */
		inline void	disableTransactionRequest() noexcept{
			using namespace Oscl::Hw::Usb::UsbHostSlave::HOST_TX_CONTROL;
			_cache.changeBits(	TRANS_REQ::FieldMask,
								TRANS_REQ::ValueMask_Disable
								);
			}

		/** */
		inline bool	transactionRequestEnabled() noexcept{
			using namespace Oscl::Hw::Usb::UsbHostSlave::HOST_TX_CONTROL;
			return _cache.equal(	TRANS_REQ::FieldMask,
									TRANS_REQ::ValueMask_Enabled
									);
			}

	public: // SOF_SYNC field
		/** */
		inline void	enableSofSync() noexcept{
			using namespace Oscl::Hw::Usb::UsbHostSlave::HOST_TX_CONTROL;
			_cache.changeBits(	SOF_SYNC::FieldMask,
								SOF_SYNC::ValueMask_SyncTransWithSOF
								);
			}

		/** */
		inline void	disableSofSync() noexcept{
			using namespace Oscl::Hw::Usb::UsbHostSlave::HOST_TX_CONTROL;
			_cache.changeBits(	SOF_SYNC::FieldMask,
								SOF_SYNC::ValueMask_DontSyncTransWithSOF
								);
			}

		/** */
		inline bool	sofSyncEnabled() noexcept{
			using namespace Oscl::Hw::Usb::UsbHostSlave::HOST_TX_CONTROL;
			return _cache.equal(	SOF_SYNC::FieldMask,
									SOF_SYNC::ValueMask_SyncTransWithSOF
									);
			}
	public: // PREAMBLE_ENABLE field
		/** */
		inline void	enableLowSpeedPreamble() noexcept{
			using namespace Oscl::Hw::Usb::UsbHostSlave::HOST_TX_CONTROL;
			_cache.changeBits(	PREAMBLE_ENABLE::FieldMask,
								PREAMBLE_ENABLE::ValueMask_EnablePreamble
								);
			}

		/** */
		inline void	disableLowSpeedPreamble() noexcept{
			using namespace Oscl::Hw::Usb::UsbHostSlave::HOST_TX_CONTROL;
			_cache.changeBits(	PREAMBLE_ENABLE::FieldMask,
								PREAMBLE_ENABLE::ValueMask_DisablePreamble
								);
			}

		/** */
		inline bool	lowSpeedPreambleEnabled() noexcept{
			using namespace Oscl::Hw::Usb::UsbHostSlave::HOST_TX_CONTROL;
			return _cache.equal(	PREAMBLE_ENABLE::FieldMask,
									PREAMBLE_ENABLE::ValueMask_EnablePreamble
									);
			}

	public: // ISO_ENABLE field
		/** */
		inline void	enableIsochronousMode() noexcept{
			using namespace Oscl::Hw::Usb::UsbHostSlave::HOST_TX_CONTROL;
			_cache.changeBits(	ISO_ENABLE::FieldMask,
								ISO_ENABLE::ValueMask_IsochronousEnable
								);
			}

		/** */
		inline void	disableIsochronousMode() noexcept{
			using namespace Oscl::Hw::Usb::UsbHostSlave::HOST_TX_CONTROL;
			_cache.changeBits(	ISO_ENABLE::FieldMask,
								ISO_ENABLE::ValueMask_IsochronousDisable
								);
			}

		/** */
		inline bool	isochronousModeEnabled() noexcept{
			using namespace Oscl::Hw::Usb::UsbHostSlave::HOST_TX_CONTROL;
			return _cache.equal(	ISO_ENABLE::FieldMask,
									ISO_ENABLE::ValueMask_IsochronousEnable
									);
			}
	};

}
}
}
}
}
}
#endif
