/*
   Copyright (C) 2007 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <new>
#include "oscl/cpu/sync.h"
#include "driver.h"
#include "oscl/driver/usb/pipe/status.h"
#include "oscl/driver/usb/pipe/info.h"
#include "oscl/error/info.h"

using namespace Oscl::Usb::UsbHostSlave::HCD::Pipe::Bulk::IN;

static const Oscl::Usb::Pipe::Status::Result*
parseResult(Oscl::Usb::UsbHostSlave::TD::Descriptor::CC result) noexcept{
	using namespace Oscl::Usb::UsbHostSlave::TD;
	switch(result){
		case Descriptor::NoError:
			return 0;
		case Descriptor::CRC:
			return &Oscl::Usb::Pipe::Status::getCRC();
		case Descriptor::BitStuffing:
			return &Oscl::Usb::Pipe::Status::getBitStuffing();
		case Descriptor::DataToggleMismatch:
			return &Oscl::Usb::Pipe::Status::getDataToggleMismatch();
		case Descriptor::Stall:
			return &Oscl::Usb::Pipe::Status::getStall();
		case Descriptor::DeviceNotResponding:
			return &Oscl::Usb::Pipe::Status::getDeviceNotResponding();
		case Descriptor::PidCheckFailure:
			return &Oscl::Usb::Pipe::Status::getPidCheckFailure();
		case Descriptor::UnexpectedPID:
			return &Oscl::Usb::Pipe::Status::getUnexpectedPID();
		case Descriptor::DataOverrun:
			return &Oscl::Usb::Pipe::Status::getDataOverrun();
		case Descriptor::DataUnderrun:
			return &Oscl::Usb::Pipe::Status::getDataUnderrun();
		case Descriptor::BufferOverrun:
			return &Oscl::Usb::Pipe::Status::getBufferOverrun();
		case Descriptor::BufferUnderrun:
			return &Oscl::Usb::Pipe::Status::getBufferUnderrun();
		default:
			for(;;);
		};
	}

Driver::Driver(	Oscl::Mt::Itc::PostMsgApi&		myPapi,
				Oscl::Usb::UsbHostSlave::ED::GeneralIn&	ed,
				Oscl::Usb::UsbHostSlave::HCD::HcdApi&	hcdContext
				) noexcept:
		_ocSync(*this,myPapi),
		_sap(*this,myPapi),
		_ed(ed),
		_hcdContext(hcdContext),
		_skipCount(0),
		_maxPacketSize(ed.getMaxPacketSize()),
		_closeReq(0),
		_nFreeTransMem(0),
		_firstTransfer(true),
		_opened(true)
		{
	for(unsigned i=0;i<maxTransfers;++i){
		free(&_transMem[i]);
		}
	}

Oscl::Usb::Pipe::Bulk::IN::Req::Api::SAP&	Driver::getSAP() noexcept{
	return _sap;
	}

Oscl::Mt::Itc::Srv::
Close::Req::Api::SAP&	Driver::getCloseSAP() noexcept{
	return _ocSync.getSAP();
	}

Oscl::Mt::Itc::Srv::
OpenCloseSyncApi&		Driver::getOpenCloseSyncApi() noexcept{
	return _ocSync;
	}

Oscl::Usb::UsbHostSlave::ED::GeneralIn&			Driver::getED() noexcept{
	return _ed;
	}

unsigned	Driver::getMaxPacketSize() const noexcept{
	return _maxPacketSize;
	}

void		Driver::cancelAllPendingTransactions() noexcept{
	TransApi*	next;
	while((next=_pendingTransactions.get())){
		next->cancel();
		}
	}

void	Driver::free(ReadDataTrans* xfer) noexcept{
	remove(xfer);
	xfer->~ReadDataTrans();
	free((TransMem*)xfer);
	if(_deferredQ.first()){
		Oscl::Mt::Itc::Msg*	msg	= _deferredQ.get();
		msg->process();
		}
	}

void	Driver::free(TransMem* mem) noexcept{
	++_nFreeTransMem;
	_freeTransMem.put((TransMem*)mem);
	if(!_opened){
		if(_nFreeTransMem == maxTransfers){
			if(_closeReq){
				_closeReq->returnToSender();
				_closeReq	= 0;
				}
			}
		}
	}

Driver::TransMem*	Driver::allocTransMem() noexcept{
	TransMem*	tm	= _freeTransMem.get();
	if(tm){
		--_nFreeTransMem;
		}
	return tm;
	}

Driver::TransApi*	Driver::remove(TransApi* trans) noexcept{
	TransApi*
	removed	= _pendingTransactions.remove(trans);
	return removed;
	}

void	Driver::pending(TransApi* trans) noexcept{
	_pendingTransactions.put(trans);
	}

void	Driver::skip() noexcept{
	if(!_skipCount){
		_ed.skip();
		}
	++_skipCount;
	}

void	Driver::dontSkip() noexcept{
	if(_skipCount){
		if(_ed.tdListIsEmpty()){
			if(_ed.isHalted()){
				_ed.clearHalted();
				}
			}
		_ed.dontSkip();
		--_skipCount;
		}
	else {
		// FIXME:
		while(true);
		}
	}

void	Driver::clearHalted() noexcept{
	_ed.clearHalted();
	}

void	Driver::request(CancelReq& msg) noexcept{
	Oscl::Mt::Itc::SrvMsg&	irp	= msg.getPayload()._irpToCancel;
	if(_deferredQ.remove(&irp)){
		irp.returnToSender();
		msg.returnToSender();
		return;
		}
	TransApi*	next;
	for(	next	= _pendingTransactions.first();
			next;
			next	= _pendingTransactions.next(next)
			){
		if(next->irpMatch(msg.getPayload()._irpToCancel)){
			remove(next);
			next->cancel(msg);
			return;
			}
		}
	msg.returnToSender();
	}

void	Driver::request(ReadReq& msg) noexcept{
	if(!_opened){
		msg.getPayload()._failed	=
			&Oscl::Usb::Pipe::Status::getPipeClosed();
		msg.returnToSender();
		return;
		}
	TransMem*	mem	= allocTransMem();
	if(!mem){
		_deferredQ.put(&msg);
		return;
		}
	ReadDataTrans*
	t	= new(mem)
			ReadDataTrans(	*this,
			_hcdContext,
			_ed,
			msg,
			_firstTransfer
			);
	_firstTransfer	= false;	// FIXME: not until after a transfer completes.
	pending(t);
	}

void	Driver::request(OpenReq& msg) noexcept{
	// FIXME: Not sure we'll be using this.
	while(true);
	}

void	Driver::request(CloseReq& msg) noexcept{
	// FIXME: Need to cancel all pending IRPs
	_opened		= false;
	if(_nFreeTransMem == maxTransfers){
		msg.returnToSender();
		return;
		}
	_closeReq	= &msg;
	cancelAllPendingTransactions();
	}

//////// Driver::ReadDataTrans ////////

Driver::ReadDataTrans::ReadDataTrans(	Driver&					context,
										Oscl::Usb::UsbHostSlave::HCD::
										HcdApi&					hcdContext,
										Oscl::Usb::UsbHostSlave::ED::
										GeneralIn&				ed,
										ReadReq&				irp,
										bool					firstOnPipe
									) noexcept:
		_otherTransfer(*this,&Driver::ReadDataTrans::otherTransferComplete),
		_lastTransfer(*this,&Driver::ReadDataTrans::lastTransferComplete),
		_context(context),
		_hcdContext(hcdContext),
		_ed(ed),
		_irp(irp),
		_cancelReq(0),
		_nTDs(0),
		_canceling(false),
		_firstOnPipe(firstOnPipe),
		_result(0)
		{
	unsigned		nDataTDs;
	const unsigned	maxPacketSize = context.getMaxPacketSize();
	_dataLength		=	irp.
						getPayload().
						getDataLength()
						;
	nDataTDs	= (_dataLength+(maxPacketSize-1))/maxPacketSize;
	_nRequiredTDs	= nDataTDs;
	tdAlloc();
	}

bool	Driver::ReadDataTrans::irpMatch(Oscl::Mt::Itc::SrvMsg& irp) noexcept{
	Oscl::Mt::Itc::SrvMsg&	msg	= _irp;
	return (&irp == &msg);
	}

void	Driver::ReadDataTrans::cancel(CancelReq& cancelReq) noexcept{
	if(_cancelReq){
		//FIXME:
		while(true);
		}
	_cancelReq	= &cancelReq;
	_result = &Oscl::Usb::Pipe::Status::getCanceled();
	cancel();
	}

void	Driver::ReadDataTrans::cancel() noexcept{
	if(_canceling) return;
	_canceling	= true;
	// This is where we re-claim outstanding TDs from
	// the ED.
	using namespace Oscl::Usb::UsbHostSlave;
	TD::Descriptor::Link*	next;
	while((next=_pendingTdList.get())){
		if(!_ed.remove(&next->_container)){
			// FIXME: Use ErrorFatal
			// Where is that descriptor?
			for(;;);
			}
		next->_container.getTransferApi().done(next->_container);
		}
	_irp._payload._failed	= _result;
	_irp.returnToSender();
	if(_cancelReq){
		_cancelReq->returnToSender();
		}
	_context.free(this);
	}

void Driver::ReadDataTrans::tdAlloc() noexcept{
	while(_nTDs<_nRequiredTDs){
		TransactionDescMem*	tdMem	= _hcdContext.allocTdMem();
		if(!tdMem){
			_hcdContext.queueTdMemTask(tdMemTask());
			return;
			}
		_tdMem.put(tdMem);
		++_nTDs;
		}
	executeIRP();
	}

void	Driver::ReadDataTrans::reclaimIrpTDs() noexcept{
	// This operation is only called when the
	// enpoint is halted by the Host Controller.

	// This is where we re-claim outstanding TDs from
	// the ED.
	using namespace Oscl::Usb::UsbHostSlave;
	TD::Descriptor::Link*	next;
	while((next=_pendingTdList.get())){
		if(!_ed.remove(&next->_container)){
			// FIXME: Use ErrorFatal
			// Where is that descriptor?
			// Consider:
			// * _context.skip();
			// * _hcdContext.getCurrentFrameNumber()
			// * _hcdContext.queueSofTask(cancelSofTask());
			// * Transaction completes
			// *
			while(true);
			}
		_hcdContext.free(next->_container);
		}
	_context.clearHalted();
	}

void	Driver::ReadDataTrans::executeIRP() noexcept{
	void*				mem;
	TD::General::In*	dataTD;

	const unsigned	maxPacketSize	= _context.getMaxPacketSize();
	unsigned		dataLength		= _dataLength;
	unsigned char*	buffer;
	buffer	= (unsigned char*)_irp.getPayload().getDataBuffer();

	unsigned		firstDataLength	=
		(dataLength < maxPacketSize)?dataLength:maxPacketSize;

	Oscl::Usb::UsbHostSlave::TD::TransferApi*	tapi;

	if(_nTDs == 1){
		tapi	= &_lastTransfer;
		}
	else {
		tapi	= &_otherTransfer;
		}

	//
	mem	= _tdMem.get();
	--_nTDs;
	dataTD	= new(mem)	TD::General::In(	*tapi,
											false,	// MUST not round
											buffer,
											firstDataLength,
											_firstOnPipe
											);
	_firstOnPipe	= false;
	buffer	+= maxPacketSize;
	_pendingTdList.put(&dataTD->_ownerLink);
	_ed.put(dataTD);

	//
	if(_nTDs){
		//
		for(;_nTDs>1;--_nTDs,buffer += maxPacketSize){
			mem	= _tdMem.get();
			dataTD	= new(mem)
				TD::General::In(	_otherTransfer,
									false,	// MUST not round
									buffer,
									maxPacketSize
									);
			_pendingTdList.put(&dataTD->_ownerLink);
			_ed.put(dataTD);
			}
	
		// Last buffer
		mem	= _tdMem.get();
		dataLength	= dataLength%maxPacketSize;
		if(!dataLength){
			dataLength	= maxPacketSize;
			}
		dataTD	= new(mem)
			TD::General::In(	_lastTransfer,
								false,	// MUST not round
								buffer,
								dataLength
								);
		_pendingTdList.put(&dataTD->_ownerLink);
		_ed.put(dataTD);
		}
	}

void	Driver::ReadDataTrans::
lastTransferComplete(TD::Descriptor& desc) noexcept{
	using namespace Oscl::Usb::UsbHostSlave::TD;
	_pendingTdList.remove(&desc._ownerLink);
	if(!_canceling){
		Descriptor::CC	result	= desc.result();
		if(result != Descriptor::NoError){
			Oscl::Error::Info::log("IN lastTransferComplete ");
			Oscl::Usb::Pipe::Status::Info	info;
			_result	= parseResult(result);
			_result->query(info);
#if 0
			if(result == Descriptor::DataUnderrun) {
				_context.clearHalted();
				}
			else if(result != Descriptor::Stall){
				_irp.getPayload()._failed	= true;
				}
#endif
			if(result == Descriptor::DataUnderrun) {
				_context.clearHalted();
				_result	= 0;
				}
			}
		Oscl::Usb::UsbHostSlave::TD::General::Descriptor*
		genDesc	= (Oscl::Usb::UsbHostSlave::TD::General::Descriptor*)&desc;
		unsigned long
		bufferEnd	= (unsigned long)genDesc->getCurrentBufferPointer();
		if(bufferEnd){
			unsigned long
			bufferStart	= (unsigned long)_irp._payload.getDataBuffer();
			_irp._payload._nRead	= bufferEnd-bufferStart;
			}
		else {
			_irp._payload._nRead	= _irp._payload.getDataLength();
			}
		_irp.getPayload()._failed	= _result;
		_irp.returnToSender();
		_context.free(this);
		}
	_context.clearHalted();
	_hcdContext.free(desc);
	}

void	Driver::ReadDataTrans::
otherTransferComplete(TD::Descriptor& desc) noexcept{
	using namespace Oscl::Usb::UsbHostSlave::TD;
	_pendingTdList.remove(&desc._ownerLink);
	if(!_canceling){
		Descriptor::CC	result	= desc.result();
		if(result != Descriptor::NoError){
			Oscl::Error::Info::log("IN otherTransferComplete ");
			Oscl::Usb::Pipe::Status::Info	info;
			_result	= parseResult(result);
			_result->query(info);
			if(result == Descriptor::DataUnderrun) {
				// The transfer has ended on a "short-packet".
				// This means that this IRP must be retired.
				// In my current implementation, that means that
				// all of the Transfer Descriptors associated
				// with this transaction must be recovered and
				// freed from the associated Endpoint Descriptor.
				// The problem with this is that it is quite
				// conceivable that the TD following this TD
				// has already been filled with the beginning
				// bytes of the next transfer! To recover that
				// TD would mean loosing the data from the
				// subsequent frame.
				// Solution?:
				//  NEVER set the bufferRounding bit in the TD.
				//  This action would cause the Endpoint Descriptor
				//  to enter the HALT state as soon as the 
				//  "short-packet" was received. When this happens
				//  The result field of the TD will be set to
				//  DataUnderrun and the CurrentBufferPointer
				//  will point to the byte after the last written
				//  to the receive buffer.
				//
				// Since I ALWAYS clear the bufferRounding
				// bit in the TD, the HALT bit of the ED
				// is set, allowing me to recover the TD's
				// associated with this IRP, after which
				// I will clear the HALT condition.
				// Assumptions:
				//  o The UsbHostSlave simply skips the TD while
				//    HALT is true, and does not issue
				//    IN requests on the Endpoint until
				//    HALT is set to false.

				// Reclaims all IRP TDs and clears HALT,
				// allowing the next IRP to proceed.
				reclaimIrpTDs();

				Oscl::Usb::UsbHostSlave::TD::General::Descriptor*
				genDesc	= (Oscl::Usb::UsbHostSlave::TD::General::Descriptor*)&desc;
				unsigned long
				bufferEnd	= (unsigned long)genDesc->getCurrentBufferPointer();
				if(!bufferEnd){
					// The "DataUnderrun" result implies that
					// the CurrentBufferPointer is set to the
					// byte after the last byte written to the
					// receive buffer. Thus, if it is ZERO,
					// something is wrong ... likely my understanding.
					for(;;);
					}
				unsigned long
				bufferStart	= (unsigned long)_irp._payload.getDataBuffer();
				_irp._payload._nRead	= bufferEnd-bufferStart;

				_result	= 0;
				_irp.getPayload()._failed	= _result;
				_irp.returnToSender();
				_context.free(this);
				}
			else {
				// At this point, the ED is halted,
				//	thus, I can simply remove the pending
				//  TDs and then clear the halted bit.
				cancel();
				}
			}
		}
	_hcdContext.free(desc);
	}

Oscl::Usb::UsbHostSlave::HCD::DeferredTaskApi&
Driver::ReadDataTrans::tdMemTask() noexcept{
	using namespace Oscl::Usb::UsbHostSlave::HCD;
	DeferredTask<ReadDataTrans>*	dt;
	dt	= new(&_dtMem)
				DeferredTask<ReadDataTrans> (	*this,
												&Driver::ReadDataTrans::
												tdAlloc
												);
	return *dt;
	}

