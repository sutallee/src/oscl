/*
   Copyright (C) 2007 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <new>
#include "oscl/cpu/sync.h"
#include "driver.h"

using namespace Oscl::Usb::UsbHostSlave::HCD::Pipe::Interrupt::IN;

Driver::Driver(	Oscl::Mt::Itc::PostMsgApi&		myPapi,
				Oscl::Usb::UsbHostSlave::ED::GeneralIn&	ed,
				Oscl::Usb::UsbHostSlave::HCD::HcdApi&	hcdContext
				) noexcept:
		_ocSync(*this,myPapi),
		_sap(*this,myPapi),
		_ed(ed),
		_hcdContext(hcdContext),
		_maxPacketSize(ed.getMaxPacketSize()),
		_closeReq(0),
		_nFreeTransMem(0),
		_firstTransfer(true),
		_opened(true)
		{
	for(unsigned i=0;i<maxTransfers;++i){
		free(&_transMem[i]);
		}
	}

Oscl::Usb::Pipe::Interrupt::IN::Req::Api::SAP&	Driver::getSAP() noexcept{
	return _sap;
	}

Oscl::Mt::Itc::Srv::
Close::Req::Api::SAP&	Driver::getCloseSAP() noexcept{
	return _ocSync.getSAP();
	}

Oscl::Mt::Itc::Srv::
OpenCloseSyncApi&		Driver::getOpenCloseSyncApi() noexcept{
	return _ocSync;
	}

Oscl::Usb::UsbHostSlave::ED::GeneralIn&			Driver::getED() noexcept{
	return _ed;
	}

unsigned	Driver::getMaxPacketSize() const noexcept{
	return _maxPacketSize;
	}

void		Driver::cancelAllPendingTransactions() noexcept{
	TransApi*	next;
	while((next=_pendingTransactions.get())){
		next->cancel();
		}
	}

void	Driver::free(ReadDataTrans* xfer) noexcept{
	remove(xfer);
	xfer->~ReadDataTrans();
	free((TransMem*)xfer);
	if(_deferredQ.first()){
		Oscl::Mt::Itc::Msg*	msg	= _deferredQ.get();
		msg->process();
		}
	}

void	Driver::free(TransMem* mem) noexcept{
	++_nFreeTransMem;
	_freeTransMem.put((TransMem*)mem);
	if(!_opened){
		if(_nFreeTransMem == maxTransfers){
			if(_closeReq){
				_closeReq->returnToSender();
				_closeReq	= 0;
				}
			}
		}
	}

Driver::TransMem*	Driver::allocTransMem() noexcept{
	TransMem*	tm	= _freeTransMem.get();
	if(tm){
		--_nFreeTransMem;
		}
	return tm;
	}

Driver::TransApi*	Driver::remove(TransApi* trans) noexcept{
	TransApi*
	removed	= _pendingTransactions.remove(trans);
	return removed;
	}

void	Driver::pending(TransApi* trans) noexcept{
	_pendingTransactions.put(trans);
	}

void	Driver::request(CancelReq& msg) noexcept{
	Oscl::Mt::Itc::SrvMsg&	irp	= msg.getPayload()._irpToCancel;
	if(_deferredQ.remove(&irp)){
		irp.returnToSender();
		msg.returnToSender();
		return;
		}
	TransApi*	next;
	for(	next	= _pendingTransactions.first();
			next;
			next	= _pendingTransactions.next(next)
			){
		if(next->irpMatch(msg.getPayload()._irpToCancel)){
			remove(next);
			next->cancel(msg);
			return;
			}
		}
	msg.returnToSender();
	}

void	Driver::request(ReadReq& msg) noexcept{
	if(!_opened){
		msg.getPayload()._failed	= true;
		msg.returnToSender();
		return;
		}
	TransMem*	mem	= allocTransMem();
	if(!mem){
		_deferredQ.put(&msg);
		return;
		}
	ReadDataTrans*
	t	= new(mem)
			ReadDataTrans(	*this,
			_hcdContext,
			_ed,
			msg,
			_firstTransfer
			);
	_firstTransfer	= false;	// FIXME: not until after a transfer completes.
	pending(t);
	}

void	Driver::request(OpenReq& msg) noexcept{
	// FIXME: Not sure we'll be using this.
	while(true);
	}

void	Driver::request(CloseReq& msg) noexcept{
	// FIXME: Need to cancel all pending IRPs
	_opened		= false;
	if(_nFreeTransMem == maxTransfers){
		msg.returnToSender();
		return;
		}
	_closeReq	= &msg;
	cancelAllPendingTransactions();
	}

//////// Driver::ReadDataTrans ////////

Driver::ReadDataTrans::ReadDataTrans(	Driver&					context,
										Oscl::Usb::UsbHostSlave::HCD::
										HcdApi&					hcdContext,
										Oscl::Usb::UsbHostSlave::ED::
										GeneralIn&				ed,
										ReadReq&				irp,
										bool					firstOnPipe
									) noexcept:
		_otherTransfer(*this,&Driver::ReadDataTrans::otherTransferComplete),
		_lastTransfer(*this,&Driver::ReadDataTrans::lastTransferComplete),
		_context(context),
		_hcdContext(hcdContext),
		_ed(ed),
		_irp(irp),
		_cancelReq(0),
		_nTDs(0),
		_canceling(false),
		_firstOnPipe(firstOnPipe)
		{
	unsigned		nDataTDs;
	const unsigned	maxPacketSize = context.getMaxPacketSize();
	_dataLength		=	irp.
						getPayload().
						getDataLength()
						;
	nDataTDs	= (_dataLength+(maxPacketSize-1))/maxPacketSize;
	_nRequiredTDs	= nDataTDs;
	tdAlloc();
	}

bool	Driver::ReadDataTrans::irpMatch(Oscl::Mt::Itc::SrvMsg& irp) noexcept{
	Oscl::Mt::Itc::SrvMsg&	msg	= _irp;
	return (&irp == &msg);
	}

void	Driver::ReadDataTrans::cancel(CancelReq& cancelReq) noexcept{
	if(_cancelReq){
		//FIXME:
		while(true);
		}
	_cancelReq	= &cancelReq;
	cancel();
	}

void	Driver::ReadDataTrans::cancel() noexcept{
	if(_canceling) return;
	_canceling	= true;
	// This is where we re-claim outstanding TDs from
	// the ED.
	using namespace Oscl::Usb::UsbHostSlave;
	TD::Descriptor::Link*	next;
	while((next=_pendingTdList.get())){
		if(!_ed.remove(&next->_container)){
			// FIXME: Use ErrorFatal
			// Where is that descriptor?
			for(;;);
			}
		next->_container.getTransferApi().done(next->_container);
		}
	_irp.returnToSender();
	if(_cancelReq){
		_irp.getPayload()._failed	= true;
		_cancelReq->returnToSender();
		}
	_context.free(this);
	}

void Driver::ReadDataTrans::tdAlloc() noexcept{
	while(_nTDs<_nRequiredTDs){
		TransactionDescMem*	tdMem	= _hcdContext.allocTdMem();
		if(!tdMem){
			_hcdContext.queueTdMemTask(tdMemTask());
			return;
			}
		_tdMem.put(tdMem);
		++_nTDs;
		}
	executeIRP();
	}

void	Driver::ReadDataTrans::executeIRP() noexcept{
	void*				mem;
	TD::General::In*	dataTD;

	const unsigned	maxPacketSize	= _context.getMaxPacketSize();
	unsigned		dataLength		= _dataLength;
	unsigned char*	buffer;
	buffer	= (unsigned char*)_irp.getPayload().getDataBuffer();

	unsigned		firstDataLength	=
		(dataLength < maxPacketSize)?dataLength:maxPacketSize;

	Oscl::Usb::UsbHostSlave::TD::TransferApi*	tapi;
	if(_nTDs == 1){
		tapi	= &_lastTransfer;
		}
	else {
		tapi	= &_otherTransfer;
		}

	//
	mem	= _tdMem.get();
	--_nTDs;
	dataTD	= new(mem)	TD::General::In(	*tapi,
											true,
											buffer,
											firstDataLength,
											_firstOnPipe
											);
	_firstOnPipe	= false;
	buffer	+= maxPacketSize;
	_pendingTdList.put(&dataTD->_ownerLink);
	_ed.put(dataTD);

	//
	if(_nTDs){
		//
		for(;_nTDs>2;--_nTDs,buffer += maxPacketSize){
			mem	= _tdMem.get();
			dataTD	= new(mem)
				TD::General::In(	_otherTransfer,
									true,
									buffer,
									maxPacketSize
									);
			_pendingTdList.put(&dataTD->_ownerLink);
			_ed.put(dataTD);
			}
	
		// Last buffer
		mem	= _tdMem.get();
		dataLength	= dataLength%maxPacketSize;
		if(!dataLength){
			dataLength	= maxPacketSize;
			}
		dataTD	= new(mem)
			TD::General::In(	_lastTransfer,
								true,
								buffer,
								dataLength
								);
		_pendingTdList.put(&dataTD->_ownerLink);
		_ed.put(dataTD);
		}
	}

void	Driver::ReadDataTrans::
lastTransferComplete(TD::Descriptor& desc) noexcept{
	using namespace Oscl::Usb::UsbHostSlave::TD;
	_pendingTdList.remove(&desc._ownerLink);
	if(!_canceling){
		Descriptor::CC	result	= desc.result();
		if(result != Descriptor::NoError){
			if(result != Descriptor::Stall){
				_irp.getPayload()._failed	= true;
				}
			}
		_irp.returnToSender();
		_context.free(this);
		}
	_hcdContext.free(desc);
	}

void	Driver::ReadDataTrans::
otherTransferComplete(TD::Descriptor& desc) noexcept{
	using namespace Oscl::Usb::UsbHostSlave::TD;
	_pendingTdList.remove(&desc._ownerLink);
	if(!_canceling){
		Descriptor::CC	result	= desc.result();
		if(result != Descriptor::NoError){
			if(result != Descriptor::Stall){
				_irp.getPayload()._failed	= true;
				cancel();
				}
			else {
				// At this point, the ED is halted,
				//	thus, I can simply remove the pending
				//  TDs and then clear the halted bit.
				cancel();
				}
			}
		}
	_hcdContext.free(desc);
	}

Oscl::Usb::UsbHostSlave::HCD::DeferredTaskApi&
Driver::ReadDataTrans::tdMemTask() noexcept{
	using namespace Oscl::Usb::UsbHostSlave::HCD;
	DeferredTask<ReadDataTrans>*	dt;
	dt	= new(&_dtMem)
				DeferredTask<ReadDataTrans> (	*this,
												&Driver::ReadDataTrans::
												tdAlloc
												);
	return *dt;
	}

