/*
   Copyright (C) 2007 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_usb_usbhostslave_hcd_server_deferh_
#define _oscl_drv_usb_usbhostslave_hcd_server_deferh_
#include "oscl/queue/queueitem.h"

/** */
namespace Oscl {
/** */
namespace Usb {
/** */
namespace UsbHostSlave {
/** */
namespace HCD {

/** The purpose of this interface is to allow HCD components
	to register for a notification at the end of done head
	processing or in a different queue for SOF processing.
 */
class DeferredTaskApi : public Oscl::QueueItem {
	public:
		/** Shut-up GCC. */
		virtual ~DeferredTaskApi() {}

		/** True
		 */
		virtual void	process() noexcept=0;
	};

/** */
template <class Context>
class DeferredTask : public DeferredTaskApi {
	private:
		/** */
		typedef void	(Context::* Func)();
		/** */
		Context&		_context;
		/** */
		Func			_func;
	public:
		/** */
		DeferredTask(	Context&	context,
						Func		func
						) noexcept:
					_context(context),
					_func(func)
					{}
	private:
		/** */
		void	process() noexcept{
			(_context.*_func)();
			}
	};

}
}
}
}


#endif
