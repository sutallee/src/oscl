/*
   Copyright (C) 2007 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <new>
#include "oscl/bits/bitfield.h"
#include "driver.h"
#include "oscl/cpu/sync.h"
#include "oscl/cache/operations.h"
#include "oscl/error/fatal.h"

extern void UsbPciConfigDump() noexcept;

using namespace Oscl::Usb::UsbHostSlave::HCD;

Driver::Driver(
				Oscl::Hw::Usb::UsbHostSlave::HOST_SLAVE_CONTROL::Reg&		host_slave_control,
				Oscl::Hw::Usb::UsbHostSlave::HOST_SLAVE_VERSION::Reg&		host_slave_version,
				Oscl::Hw::Usb::UsbHostSlave::HOST_TX_CONTROL::Reg&			host_tx_control,
				Oscl::Hw::Usb::UsbHostSlave::HOST_TX_TRANS_TYPE::Reg&		host_tx_trans_type,
				Oscl::Hw::Usb::UsbHostSlave::HOST_TX_LINE_CONTROL::Reg&		host_tx_line_control,
				Oscl::Hw::Usb::UsbHostSlave::HOST_TX_SOF_ENABLE::Reg&		host_tx_sof_enable,
				Oscl::Hw::Usb::UsbHostSlave::HOST_TX_ADDR::Reg&				host_tx_addr,
				Oscl::Hw::Usb::UsbHostSlave::HOST_TX_ENDP::Reg&				host_tx_endp,
				Oscl::Hw::Usb::UsbHostSlave::HOST_FRAME_NUM_MSP::Reg&		host_frame_num_msp,
				Oscl::Hw::Usb::UsbHostSlave::HOST_FRAME_NUM_LSP::Reg&		host_frame_num_lsp,
				Oscl::Hw::Usb::UsbHostSlave::HOST_INTERRUPT_STATUS::Reg&	host_interrupt_status,
				Oscl::Hw::Usb::UsbHostSlave::HOST_INTERRUPT_MASK::Reg&		host_interrupt_mask,
				Oscl::Hw::Usb::UsbHostSlave::HOST_RX_STATUS::Reg&			host_rx_status,
				Oscl::Hw::Usb::UsbHostSlave::HOST_RX_PID::Reg&				host_rx_pid,
				Oscl::Hw::Usb::UsbHostSlave::HOST_RX_ADDR::Reg&				host_rx_addr,
				Oscl::Hw::Usb::UsbHostSlave::HOST_RX_ENDP::Reg&				host_rx_endp,
				Oscl::Hw::Usb::UsbHostSlave::HOST_RX_CONNECT_STATE::Reg&	host_rx_connect_state,
				Oscl::Hw::Usb::UsbHostSlave::HOST_SOF_TIMER_MSB::Reg&		host_sof_timer_msb,
				Oscl::Hw::Usb::UsbHostSlave::HOST_RX_FIFO_DATA::Reg&		host_rx_fifo_data,
				Oscl::Hw::Usb::UsbHostSlave::HOST_RX_FIFO_COUNT_MSB::Reg&	host_rx_fifo_count_msb,
				Oscl::Hw::Usb::UsbHostSlave::HOST_RX_FIFO_COUNT_LSB::Reg&	host_rx_fifo_count_lsb,
				Oscl::Hw::Usb::UsbHostSlave::HOST_RX_FIFO_CONTROL::Reg&		host_rx_fifo_count_control,
				Oscl::Hw::Usb::UsbHostSlave::HOST_TX_FIFO_DATA::Reg&		host_tx_fifo_data,
				Oscl::Hw::Usb::UsbHostSlave::HOST_TX_FIFO_CONTROL::Reg&		host_tx_fifo_control,
				TransactionDescMem*				tdArray,
				unsigned						tdArraySize,
				EndpointDescMem*				edArray,
				unsigned						edArraySize,
				Oscl::Mt::Itc::ServerApi&		server,
				PipeMem*						pipeMem,
				unsigned						maxEndpoints
				) noexcept:
		Oscl::Mt::Itc::Srv::CloseSync(*this,server.getPostMsgApi()),
		_enumMonitorVar(	server.getPostMsgApi(),
							*this,
							*this,
							*this,
							*this,
							*this,
							*this
							),
		_bwAlloc(_hccaInterruptTable),
		_host_tx_control(host_tx_control),
		_host_tx_trans_type(host_tx_trans_type),
		_host_tx_line_control(host_tx_line_control),
		_host_tx_sof_enable(host_tx_sof_enable),
		_host_tx_addr(host_tx_addr),
		_host_tx_endp(host_tx_endp),
		_host_frame_num(host_frame_num_msp,host_frame_num_lsp),
		_host_interrupt_status(host_interrupt_status),
		_host_interrupt_mask(host_interrupt_mask),
		_host_rx_status(host_rx_status),
		_host_rx_pid(host_rx_pid),
		_host_rx_addr(host_rx_addr),
		_host_rx_endp(host_rx_endp),
		_host_rx_connect_state(host_rx_connect_state),
		_host_sof_timer(host_sof_timer_msb),
		_host_rx_fifo_data(host_rx_fifo_data),
		_host_rx_fifo_count(host_rx_fifo_count_lsb,host_rx_fifo_count_msb),
		_host_tx_fifo_data(host_tx_fifo_data),
		_host_tx_fifo_control(host_tx_fifo_control),
		_server(server),
		_open(false),
		_rhOpenReq(0),
		_rhCloseReq(0),
		_rhChangeReq(0),
		_rhSAP(*this,server.getPostMsgApi()),
		_hubPortSAP(*this,server.getPostMsgApi()),
		_msgPipeAllocSAP(*this,server.getPostMsgApi()),
		_intInPipeAllocSAP(*this,server.getPostMsgApi()),
		_bulkInPipeAllocSAP(*this,server.getPostMsgApi()),
		_bulkOutPipeAllocSAP(*this,server.getPostMsgApi()),
		_addressZeroLocked(false),
		_lsDefaultPipe(),
		_hsDefaultPipe(),
		_unexpectedTransfer(*this,&HCD::Driver::unexpectedDone),
		_controlListDisableCount(0),
		_periodicListDisableCount(0),
		_bulkListDisableCount(0),
		_pipeMem(pipeMem)
		{
	for(unsigned i=0;i<maxUsbDeviceAddresses;++i){
		_allocatedUsbAddr[i]	= false;
		}
	for(unsigned i=0;i<tdArraySize;++i){
		_freeTDList.put(&tdArray[i]);
		}
	for(unsigned i=0;i<edArraySize;++i){
		_freeEDList.put(&edArray[i]);
		}
	for(unsigned i=0;i<maxEndpoints;++i){
		_freePipeMem.put(&_pipeMem[i]);
		}
//	for(unsigned i=0;i<nDeferredTasks;++i){
//		_freeDtMem.put(&_dtMem[i]);
//		}
//	unsigned long	hccax;
//	_dmaBus.cpuToBus(&_hcca,hccax);
//	_reg.memoryPointer.hcca	= hccax;
	_lsDefaultPipe	= allocMessagePipe(	Address(0),
										EndpointID(0),
										8,
										true
										);
	_hsDefaultPipe	= allocMessagePipe(	Address(0),
										EndpointID(0),
										8,
										false
										);

	// Create dummy ED for bulk list.
//	EndpointDescMem*	edMem	= allocEdMem();
//	if(!edMem){
//		// This should NEVER happen
//		for(;;);
//		}
//	_bulkEpListHead = new (edMem)
//			Oscl::Usb::UsbHostSlave::ED::Descriptor(
//												0,0,0,false,false
//												);
//	_bulkEpListHead->skip();

	// Create control pipe list
	_controlEpListHead.put(&_lsDefaultPipe->_pipe.getED());
	_controlEpListHead.put(&_hsDefaultPipe->_pipe.getED());
	_enumDcpVar	= new(&_enumDcpVarMem)
					Oscl::Usb::HCD::Enum::
					DefCntrlPipe::Var(	server.getPostMsgApi(),
										*this,
										*this,
										_lsDefaultPipe->_pipe,
										_hsDefaultPipe->_pipe
										);
	server.setSignaledApi(this);
	}
Driver::~Driver(){
	_server.setSignaledApi(0);
	}

RootHub::Req::Api::SAP&		Driver::getRootHubSAP() noexcept{
	return _rhSAP;
	}

Oscl::Usb::Pipe::Message::Req::Api::SAP&	Driver::getLsEpSAP() noexcept{
	return _lsDefaultPipe->_pipe.getSAP();
	}

Oscl::Usb::Pipe::Message::Req::Api::SAP&	Driver::getHsEpSAP() noexcept{
	return _hsDefaultPipe->_pipe.getSAP();
	}

Oscl::Usb::Pipe::Message::SyncApi&		Driver::getLsSyncApi() noexcept{
	return _lsDefaultPipe->_pipe.getSyncApi();
	}

Oscl::Usb::Pipe::Message::SyncApi&		Driver::getHsSyncApi() noexcept{
	return _hsDefaultPipe->_pipe.getSyncApi();
	}

Oscl::Usb::Alloc::Addr::Req::Api::SAP&	Driver::getHubPortSAP() noexcept{
	return _hubPortSAP;
	}

Oscl::Usb::Alloc::Pipe::
Message::Req::Api::SAP&		Driver::getMsgPipeAllocSAP() noexcept{
	return _msgPipeAllocSAP;
	}

Oscl::Usb::Alloc::Pipe::
Interrupt::IN::Req::Api::SAP&	Driver::getInterruptInPipeAllocSAP() noexcept{
	return _intInPipeAllocSAP;
	}

Oscl::Usb::Alloc::Pipe::
Bulk::IN::Req::Api::SAP&	Driver::getBulkInPipeAllocSAP() noexcept{
	return _bulkInPipeAllocSAP;
	}

Oscl::Usb::Alloc::Pipe::
Bulk::OUT::Req::Api::SAP&	Driver::getBulkOutPipeAllocSAP() noexcept{
	return _bulkOutPipeAllocSAP;
	}

Oscl::Usb::HCD::Enum::
DefCntrlPipe::Api&				Driver::getEnumDefCntrlPipeApi() noexcept{
	return *_enumDcpVar;
	}

Oscl::Usb::HCD::Enum::
Monitor::Api&					Driver::getEnumMonitorApi() noexcept{
	return _enumMonitorVar;
	}

// keep
bool	Driver::usbAddressAvailable() noexcept{
	for(unsigned i=1;i<maxUsbDeviceAddresses;++i){
		if(!_allocatedUsbAddr[i]){
			return true;
			}
		}
	return false;
	}

void	Driver::insertToControlList(Oscl::Usb::UsbHostSlave::ED::Message& ed) noexcept{
	_controlEpListHead.put(&ed);
	}

void	Driver::removeFromControlList(Oscl::Usb::UsbHostSlave::ED::Message& ed) noexcept{
	_controlEpListHead.remove(&ed);
	}

void	Driver::insertToBulkList(Oscl::Usb::UsbHostSlave::ED::Descriptor& ed) noexcept{
	_bulkEpListHead.put(&ed);
	}

void	Driver::removeFromBulkList(Oscl::Usb::UsbHostSlave::ED::Descriptor& ed) noexcept{
	_bulkEpListHead.remove(&ed);
#if 0
	// Takes advantage of the fact that the list is never empty.
	Oscl::Usb::UsbHostSlave::ED::Descriptor*	next;
	for(	next=_bulkEpListHead.first();
			next;
			next	= next->getNextEndpointDescriptor()
			){
		if(next->getNextEndpointDescriptor() == &ed){
			next->setNextED(ed.getNextEndpointDescriptor());
			return;
			}
		}
#endif
	}

// This operation should stay for OpenCores
//Driver::DeferredTaskMem*	Driver::allocDeferredTaskMem() noexcept{
//	return _freeDtMem.get();
//	}

//void	Driver::free(DeferredTaskMem* mem) noexcept{
//	_freeDtMem.put(mem);
//	Oscl::Mt::Itc::SrvMsg*	msg	= _pendingDeferredTaskReqList.get();
//	if(msg){
//		msg->process();
//		}
//	}

MessagePipeRec*	Driver::removeActiveMessageEp(	Oscl::Usb::Pipe::
												Message::PipeApi&	pipe
												) noexcept{
	MessagePipeRec*	next;
	for(	next=_activeMessageEpList.first();
			next;
			next=_activeMessageEpList.next(next)
			){
		Oscl::Usb::Pipe::Message::PipeApi*	api	= &next->_pipe;
		if(api == &pipe){
			_activeMessageEpList.remove(next);
			return next;
			}
		}
	return 0;
	}

InterruptInPipeRec*
Driver::removeActiveInterruptInPipe(	Oscl::Usb::Pipe::
										Interrupt::IN::
										PipeApi&			pipe
										) noexcept{
	InterruptInPipeRec*	next;
	for(	next=_activeInterruptInPipeList.first();
			next;
			next=_activeInterruptInPipeList.next(next)
			){
		Oscl::Usb::Pipe::Interrupt::IN::PipeApi*	api	= &next->_pipe;
		if(api == &pipe){
			_activeInterruptInPipeList.remove(next);
			return next;
			}
		}
	return 0;
	}

BulkInPipeRec*
Driver::removeActiveBulkInPipe(	Oscl::Usb::Pipe::
								Bulk::IN::
								PipeApi&			pipe
								) noexcept{
	BulkInPipeRec*	next;
	for(	next=_activeBulkInPipeList.first();
			next;
			next=_activeBulkInPipeList.next(next)
			){
		Oscl::Usb::Pipe::Bulk::IN::PipeApi*	api	= &next->_pipe;
		if(api == &pipe){
			_activeBulkInPipeList.remove(next);
			return next;
			}
		}
	return 0;
	}

BulkOutPipeRec*
Driver::removeActiveBulkOutPipe(	Oscl::Usb::Pipe::
									Bulk::OUT::
									PipeApi&			pipe
									) noexcept{
	BulkOutPipeRec*	next;
	for(	next=_activeBulkOutPipeList.first();
			next;
			next=_activeBulkOutPipeList.next(next)
			){
		Oscl::Usb::Pipe::Bulk::OUT::PipeApi*	api	= &next->_pipe;
		if(api == &pipe){
			_activeBulkOutPipeList.remove(next);
			return next;
			}
		}
	return 0;
	}

#if 0 // OHCI stuff
void	Driver::disableControlList() noexcept{
	if(!_controlListDisableCount){
		_control.disableControlList();
		}
	++_controlListDisableCount;
	}

void	Driver::enableControlList() noexcept{
	if(_controlListDisableCount){
		--_controlListDisableCount;
		}
	if(!_controlListDisableCount){
		_control.enableControlList();
		}
	}

void	Driver::disablePeriodicList() noexcept{
	if(!_periodicListDisableCount){
		_control.disablePeriodicList();
		}
	++_periodicListDisableCount;
	}

void	Driver::enablePeriodicList() noexcept{
	if(_periodicListDisableCount){
		--_periodicListDisableCount;
		}
	if(!_periodicListDisableCount){
		_control.enablePeriodicList();
		}
	}

void	Driver::disableBulkList() noexcept{
	if(!_bulkListDisableCount){
		_control.disableBulkList();
		}
	++_bulkListDisableCount;
	}

void	Driver::enableBulkList() noexcept{
	if(_bulkListDisableCount){
		--_bulkListDisableCount;
		}
	if(!_bulkListDisableCount){
		_control.enableBulkList();
		}
	}
#endif

Oscl::Usb::Address Driver::allocateAddress() noexcept{
	for(unsigned i=1;i<maxUsbDeviceAddresses;++i){
		if(!_allocatedUsbAddr[i]){
			_allocatedUsbAddr[i]	= true;
			return i;
			}
		}
	return 0;
	}

void	Driver::freeAddress(Oscl::Usb::Address address) noexcept{
	if(!_allocatedUsbAddr[address]){
		Oscl::ErrorFatal::logAndExit("Free of unallocted USB address!\n");
		}
	Oscl::Usb::Alloc::Addr::
	Req::Api::UsbAddrResChangeReq*	next;
	while((next=_usbAddrChangeList.get())){
		next->returnToSender();
		}
	_allocatedUsbAddr[address]	= false;
	}

void	Driver::scheduleOverrun() noexcept{
	}

#if 0 // OHCI
void	Driver::writebackDoneHead(uint32_t busTD) noexcept{
	TD::Descriptor*				doneStack=0;
	TD::Descriptor*				desc;
	void*						vdesc;

	while(busTD){
		if(!_dmaBus.busToCpu(busTD,vdesc)){
			Oscl::ErrorFatal::logAndExit("no translation\n");
			}
		// FIXME: Need desc=PlatPhysicalToVirtual(desc) here.
		desc	= (TD::Descriptor*)vdesc;
		busTD	= desc->getNextTD();
		desc->_doneStackLink	= doneStack;
		doneStack				= desc;
		}

	while((desc=doneStack)){
		doneStack	= desc->_doneStackLink;
		desc->getTransferApi().done(*desc);
		}
	Queue<DeferredTaskApi>	deferredTasks	= _tdMemDeferredTaskList;
	DeferredTaskApi*	dt;
	while((dt=deferredTasks.get())){
		dt->process();
		}
	}

void	Driver::startOfFrame() noexcept{
	Queue<DeferredTaskApi>	deferredTasks	= _sofDeferredTaskList;
	DeferredTaskApi*	dt;
	while((dt=deferredTasks.get())){
		dt->process();
		}
	if(!_sofDeferredTaskList.first()){
		// List is empty
		_irqDisable.startOfFrame();
		}
	}
#endif

void	Driver::resumeDetected() noexcept{
	}

void	Driver::unrecoverableError() noexcept{
	}

void	Driver::frameNumberOverflow() noexcept{
	}

#if 0 // OHCI implementation
void	Driver::rootHubStatusChange() noexcept{
	_irqDisable.rootHubStatusChange();
	if(_rhChangeReq){
		_rhChangeReq->returnToSender();
		_rhChangeReq	= 0;
		}
	}

void	Driver::ownershipChange() noexcept{
	}
#endif

void	Driver::transactionComplete() noexcept{
//	_host_interrupt_mask.disableTransactionCompleteInterrupt();
//	_host_interrupt_mask.enableTransactionCompleteInterrupt();
	}

void	Driver::resumeStateDetected() noexcept{
//	_host_interrupt_mask.disableResumeStateInterrupt();
//	_host_interrupt_mask.enableResumeStateInterrupt();
	}

void	Driver::connectStateChanged() noexcept{
	_host_interrupt_mask.disableConnectStateChangeInterrupt();
//	_host_interrupt_mask.enableConnectStateChangeInterrupt();
	_irqDisable.rootHubStatusChange();
	_host_interrupt_mask.
	if(_rhChangeReq){
		_rhChangeReq->returnToSender();
		_rhChangeReq	= 0;
		}
	}

void	Driver::sofSent() noexcept{
//	_host_interrupt_mask.disableSofSentInterrupt();
//	_host_interrupt_mask.enableSofSentInterrupt();
	}


void	Driver::request(	Oscl::Mt::Itc::Srv::
							Open::Req::Api::OpenReq&	msg
							) noexcept{
	_open	= true;

	// Setup UsbHostSlave Hardware
	// o enable interrupts
	// o initialize control list
	// o initialize bulk list
	// o initialize bulk list
	// o initialize frame counter (frame interval)
	// FIXME: Need to delay before going operational.

	if(_rhOpenReq){
		_rhOpenReq->returnToSender();
		_rhOpenReq	= 0;
		}
	msg.returnToSender();
	}

void	Driver::request(	Oscl::Mt::Itc::Srv::
							Close::Req::Api::CloseReq& msg
							) noexcept{
	if(_rhCloseReq){
		_rhCloseReq->returnToSender();
		_rhCloseReq		= 0;
		}

	// Shut-down hardware
	// o disable interrupts

	_open	= false;
	msg.returnToSender();
	}

// This may be used for OC intact.
void	Driver::request(	Oscl::Usb::Alloc::Pipe::
							Message::Req::Api::NewReq&	msg
							) noexcept{
#if 0 // OHCI
	DeferredTaskMem*	dtMem	= allocDeferredTaskMem();
	if(!dtMem){
		_pendingDeferredTaskReqList.put(&msg);
		return;
		}
	MessagePipeRec*	ep;
	ep	= allocMessagePipe(	msg.getPayload().getDeviceAddress(),
							msg.getPayload().getEndpointID(),
							msg.getPayload().getMaxPacketSize(),
							msg.getPayload().isLowSpeedDevice()
							);
	if(!ep){
		msg.getPayload().setPipeApi(&ep->_pipe);	// BUG!
		msg.returnToSender();
		free(dtMem);
		return;
		}
	new(dtMem) ControlListAttachDefTask(	*this,
											msg,
											*ep
											);
	_activeMessageEpList.put(ep);
#else
	// Simplified. No need for deferred tasks since
	// all lists (periodic,control,bulk) are serviced
	// from the same thread.
	MessagePipeRec*	ep;
	ep	= allocMessagePipe(	msg.getPayload().getDeviceAddress(),
							msg.getPayload().getEndpointID(),
							msg.getPayload().getMaxPacketSize(),
							msg.getPayload().isLowSpeedDevice()
							);
	if(!ep){
		msg.returnToSender();
		return;
		}

	msg.getPayload().setPipeApi(&ep->_pipe);
	msg.returnToSender();

	insertToControlList(ep->_pipe.getED());	// OHCI ED ?
	_activeMessageEpList.put(ep);
#endif
	}

void	Driver::request(	Oscl::Usb::Alloc::Pipe::
							Message::Req::Api::FreeReq&	msg
							) noexcept{
#if 0 // OHCI
	DeferredTaskMem*	dtMem	= allocDeferredTaskMem();
	if(!dtMem){
		_pendingDeferredTaskReqList.put(&msg);
		return;
		}
	new(dtMem) ControlListDetachDefTask(	*this,
											msg
											);
#else
	// Simplified. No need for deferred tasks since
	// all lists (periodic,control,bulk) are serviced
	// from the same thread.
	Oscl::Usb::Pipe::Message::PipeApi*
	pipeApi	= &msg.getPayload().getPipeApi();

	MessagePipeRec*
	pipe	= removeActiveMessageEp(*pipeApi);

	if(pipe){
		removeFromControlList(pipe->_pipe.getED());
		free(*pipe);
		}
	msg.returnToSender();
#endif
	}

void	Driver::request(	Oscl::Usb::Alloc::Pipe::
							Message::Req::Api::NotifyChangeReq&	msg
							) noexcept{
	// if allocMessagePipe() resources are available:
	if(		_freeTDList.first()
		&& _freeEDList.first()
		&& _freePipeMem.first()
		){
		msg.returnToSender();
		return;
		}
	_msgPipeResChangeList.put(&msg);
	}

void	Driver::request(	Oscl::Usb::Alloc::Pipe::
							Message::Req::Api::CancelChangeReq&	msg
							) noexcept{
	_msgPipeResChangeList.remove(&msg.getPayload()._toCancel);
	msg.getPayload()._toCancel.returnToSender();
	msg.returnToSender();
	}

void	Driver::request(	Oscl::Usb::Alloc::Pipe::
							Interrupt::IN::Req::Api::NewReq&	msg
							) noexcept{
	Oscl::Usb::Alloc::Pipe::
	Interrupt::IN::Req::Api::NewPayload&
	payload	= msg.getPayload();
	Oscl::Usb::UsbHostSlave::BW::BwAlloc*
	bwAlloc	= _bwAlloc.find(payload.getBwRec());
	if(!bwAlloc){
		// FIXME Logic Error This should never happen
		while(true);
		}
	InterruptInPipeRec*
	pipe	=	allocInterruptInPipe(	bwAlloc->_ed,
										payload.getDeviceAddress(),
										payload.getEndpointID(),
										payload.getMaxPacketSize(),
										payload.isLowSpeedDevice()
										);
	if(pipe){
		payload.setPipeApi(&pipe->_pipe);
		_activeInterruptInPipeList.put(pipe);
		}
	else{
		payload.setPipeApi(0);
		}
	msg.returnToSender();
	}

void	Driver::request(	Oscl::Usb::Alloc::Pipe::
							Interrupt::IN::Req::Api::FreeReq&	msg
							) noexcept{
	Oscl::Usb::Pipe::Interrupt::IN::PipeApi*
	pipeApi	= &msg.getPayload().getPipeApi();
	InterruptInPipeRec*
	pipe	= removeActiveInterruptInPipe(*pipeApi);
	if(!pipe){
		// FIXME: Logic error should never happen
		while(true);
		}
	free(*pipe);
	msg.returnToSender();
	}

void	Driver::request(	Oscl::Usb::Alloc::Pipe::
							Interrupt::IN::Req::Api::NotifyChangeReq&	msg
							) noexcept{
	// FIXME
	while(true);
	}

void	Driver::request(	Oscl::Usb::Alloc::Pipe::
							Interrupt::IN::Req::Api::CancelChangeReq&	msg
							) noexcept{
	// FIXME
	while(true);
	}

void	Driver::request(	Oscl::Usb::Alloc::Pipe::
							Bulk::IN::Req::Api::NewReq&	msg
							) noexcept{
#if 0 // OHCI
	DeferredTaskMem*	dtMem	= allocDeferredTaskMem();
	if(!dtMem){
		_pendingDeferredTaskReqList.put(&msg);
		return;
		}
	Oscl::Usb::Alloc::Pipe::
	Bulk::IN::Req::Api::NewPayload&
	payload	= msg.getPayload();
	BulkInPipeRec*
	pipe	=	allocBulkInPipe(	payload.getDeviceAddress(),
									payload.getEndpointID(),
									payload.getMaxPacketSize(),
									payload.isLowSpeedDevice()
									);
	if(!pipe){
		msg.getPayload().setPipeApi(&pipe->_pipe);
		msg.returnToSender();
		free(dtMem);
		return;
		}
	new(dtMem) BulkInListAttachDefTask(	*this,
										msg,
										*pipe
										);
	_activeBulkInPipeList.put(pipe);
#else
	// Simplified. No need for deferred tasks since
	// all lists (periodic,control,bulk) are serviced
	// from the same thread.
	Oscl::Usb::Alloc::Pipe::Bulk::IN::Req::Api::NewPayload&
	payload	= msg.getPayload();

	BulkInPipeRec*
	pipe	=	allocBulkInPipe(	payload.getDeviceAddress(),
									payload.getEndpointID(),
									payload.getMaxPacketSize(),
									payload.isLowSpeedDevice()
									);
	if(!pipe){
		msg.getPayload().setPipeApi(&pipe->_pipe);
		msg.returnToSender();
		return;
		}
	msg.getPayload().setPipeApi(&pipe->_pipe);
	msg.returnToSender();
	insertToBulkList(pipe->_pipe.getED());
#endif
#if 0
	if(pipe){
		payload.setPipeApi(&pipe->_pipe);
		_activeBulkInPipeList.put(pipe);
		}
	else{
		payload.setPipeApi(0);
		}
	msg.returnToSender();
#endif
	}

void	Driver::request(	Oscl::Usb::Alloc::Pipe::
							Bulk::IN::Req::Api::FreeReq&	msg
							) noexcept{
	Oscl::Usb::Pipe::Bulk::IN::PipeApi*
	pipeApi	= &msg.getPayload().getPipeApi();
	BulkInPipeRec*
	pipe	= removeActiveBulkInPipe(*pipeApi);
	if(!pipe){
		// FIXME: Logic error should never happen
		while(true);
		}
	free(*pipe);
	msg.returnToSender();
	}

void	Driver::request(	Oscl::Usb::Alloc::Pipe::
							Bulk::IN::Req::Api::NotifyChangeReq&	msg
							) noexcept{
	// FIXME
	while(true);
	}

void	Driver::request(	Oscl::Usb::Alloc::Pipe::
							Bulk::IN::Req::Api::CancelChangeReq&	msg
							) noexcept{
	// FIXME
	while(true);
	}

void	Driver::request(	Oscl::Usb::Alloc::Pipe::
							Bulk::OUT::Req::Api::NewReq&	msg
							) noexcept{
#if 0 // OHCI
	DeferredTaskMem*	dtMem	= allocDeferredTaskMem();
	if(!dtMem){
		_pendingDeferredTaskReqList.put(&msg);
		return;
		}
	Oscl::Usb::Alloc::Pipe::
	Bulk::OUT::Req::Api::NewPayload&
	payload	= msg.getPayload();
	BulkOutPipeRec*
	pipe	=	allocBulkOutPipe(	payload.getDeviceAddress(),
									payload.getEndpointID(),
									payload.getMaxPacketSize(),
									payload.isLowSpeedDevice()
									);
	if(!pipe){
		msg.getPayload().setPipeApi(&pipe->_pipe);
		msg.returnToSender();
		free(dtMem);
		return;
		}
	new(dtMem) BulkOutListAttachDefTask(	*this,
											msg,
											*pipe
											);
	_activeBulkOutPipeList.put(pipe);
#else
	// Simplified. No need for deferred tasks since
	// all lists (periodic,control,bulk) are serviced
	// from the same thread.
	Oscl::Usb::Alloc::Pipe::Bulk::OUT::Req::Api::NewPayload&
	payload	= msg.getPayload();

	BulkOutPipeRec*
	pipe	=	allocBulkOutPipe(	payload.getDeviceAddress(),
									payload.getEndpointID(),
									payload.getMaxPacketSize(),
									payload.isLowSpeedDevice()
									);
	if(!pipe){
		msg.getPayload().setPipeApi(&pipe->_pipe);
		msg.returnToSender();
		return;
		}

	msg.getPayload().setPipeApi(&pipe->_pipe);
	msg.returnToSender();
	insertToBulkList(pipe->_pipe.getED());
#endif
#if 0
	if(pipe){
		payload.setPipeApi(&pipe->_pipe);
		_activeBulkOutPipeList.put(pipe);
		}
	else{
		payload.setPipeApi(0);
		}
	msg.returnToSender();
#endif
	}

void	Driver::request(	Oscl::Usb::Alloc::Pipe::
							Bulk::OUT::Req::Api::FreeReq&	msg
							) noexcept{
	Oscl::Usb::Pipe::Bulk::OUT::PipeApi*
	pipeApi	= &msg.getPayload().getPipeApi();
	BulkOutPipeRec*
	pipe	= removeActiveBulkOutPipe(*pipeApi);
	if(!pipe){
		// FIXME: Logic error should never happen
		while(true);
		}
	free(*pipe);
	msg.returnToSender();
	}

void	Driver::request(	Oscl::Usb::Alloc::Pipe::
							Bulk::OUT::Req::Api::NotifyChangeReq&	msg
							) noexcept{
	// FIXME
	while(true);
	}

void	Driver::request(	Oscl::Usb::Alloc::Pipe::
							Bulk::OUT::Req::Api::CancelChangeReq&	msg
							) noexcept{
	// FIXME
	while(true);
	}

bool	Driver::interrupt() noexcept{
	if(isr()){
		dsr();
		return true;
		}
	return false;
	}

bool	Driver::isr() noexcept{
#if 0 // OHCI implementation
	uint32_t	doneHead	= _hcca.doneHead;
	if(doneHead && (doneHead & 0x00000001)){
		_irqDisable.master();
		return true;
		}
	else if (_irqStatus.anyPending()){
		_irqDisable.master();
		return true;
		}
#endif
	return false;
	}

void	Driver::dsr() noexcept{
	if(_open) _server.getSignalApi().suSignal();
	}

void	Driver::initialize() noexcept{
	}

void	Driver::signaled() noexcept{
#if 0 // OHCI implementation
	uint32_t	doneHead	= _hcca.doneHead;
	if(doneHead && !(doneHead & 0x00000001)){	// FIXME: constant
		// In this case, there is no need to read
		// the UsbHostSlave registers (delayed PCI read).
		_hcca.doneHead	= 0;
		OsclCpuInOrderMemoryAccessBarrier();
		_irqStatus.ackWritebackDoneHead();
		writebackDoneHead(doneHead);
		_irqEnable.master();
		return;
		}
	_irqStatus.update();
	if(_irqStatus.scheduleOverrun()){
		_irqStatus.ackScheduleOverrun();
		scheduleOverrun();
		}

	if(_irqStatus.writebackDoneHead()){
		OsclCacheDataInvalidateRange(	&_hcca.doneHead,
										sizeof(_hcca.doneHead)
										);
		uint32_t	doneHead	= _hcca.doneHead;
		_hcca.doneHead			= 0;
		OsclCpuInOrderMemoryAccessBarrier();
		doneHead				&= ~0x00000001;	// FIXME: constant
		_irqStatus.ackWritebackDoneHead();
		writebackDoneHead(doneHead);
		}
	if(_irqStatus.startOfFrame()){
		_irqStatus.ackStartOfFrame();
		startOfFrame();
		}
	if(_irqStatus.resumeDetected()){
		_irqStatus.ackResumeDetected();
		resumeDetected();
		}
	if(_irqStatus.unrecoverableError()){
		_irqStatus.ackUnrecoverableError();
		unrecoverableError();
		}
	if(_irqStatus.frameNumberOverflow()){
		_irqStatus.ackFrameNumberOverflow();
		frameNumberOverflow();
		}
	if(_irqStatus.rootHubStatusChange()){
		_irqStatus.ackRootHubStatusChange();
		rootHubStatusChange();
		}
	if(_irqStatus.ownershipChange()){
		_irqStatus.ackOwnershipChange();
		ownershipChange();
		}

	_irqEnable.master();
#else
	_host_interrupt_status.updateCacheFromReg();
	if(_host_interrupt_status.transactionComplete()){
		_host_interrupt_status.clearTransactionComplete();
		}
	if(_host_interrupt_status.resumeStateDetected()){
		_host_interrupt_status.clearResumeStateDetected();
		}
	if(_host_interrupt_status.connectStateChanged()){
		_host_interrupt_status.clearConnectStateChanged();
		}
	if(_host_interrupt_status.sofSent()){
		_host_interrupt_status.clearSofSent();
		}
#endif
	}

// keep
void	Driver::request(RootHub::Req::Api::OpenReq& msg) noexcept{
	if(_open){
		msg.returnToSender();
		}
	else{
		_rhOpenReq	= &msg;
		}
	}

// keep
void	Driver::request(RootHub::Req::Api::CloseReq& msg) noexcept{
	if(!_open){
		msg.returnToSender();
		}
	else{
		_rhCloseReq	= &msg;
		}
	}

// keep
void	Driver::request(RootHub::Req::Api::ChangeReq& msg) noexcept{
	if(!_open) {
		msg.returnToSender();
		return;
		}
	_rhChangeReq	= &msg;
#if 0 // OHCI implementation
	_irqEnable.rootHubStatusChange();
#endif
	}

// keep
void	Driver::request(RootHub::Req::Api::CancelAllReq& msg) noexcept{
	if(_rhOpenReq) _rhOpenReq->returnToSender();
	if(_rhCloseReq) _rhCloseReq->returnToSender();
	if(_rhChangeReq) _rhChangeReq->returnToSender();
	_rhChangeReq	= 0;
	_rhCloseReq		= 0;
	msg.returnToSender();
	}

// keep
void	Driver::request(	Oscl::Usb::Alloc::Addr::
							Req::Api::LockAddressZeroReq&		msg
							) noexcept{
	if(_addressZeroLocked){
		_addressZeroLockPendingQ.put(&msg);
		}
	else{
		_addressZeroLocked	= true;
		msg._payload._canceled	= false;
		msg.returnToSender();
		}
	}

// keep
void	Driver::request(	Oscl::Usb::Alloc::Addr::
							Req::Api::UnlockAddressZeroReq&	msg
							) noexcept{
	using namespace Oscl::Usb::Alloc::Addr;
	_addressZeroLocked	= false;
	msg.returnToSender();
	Req::Api::LockAddressZeroReq*	req	= _addressZeroLockPendingQ.get();
	if(req){
		req->_payload._canceled	= false;
		req->returnToSender();
		}
	}

// keep
void	Driver::request(	Oscl::Usb::Alloc::Addr::
							Req::Api::CancelAddrZeroLockReq&	msg
							) noexcept{
	
	Oscl::Usb::Alloc::Addr::
	Req::Api::LockAddressZeroReq*
	canceled	= _addressZeroLockPendingQ.remove(&msg.getPayload()._toCancel);
	if(canceled){
		canceled->_payload._canceled	= true;
		canceled->returnToSender();
		}
	msg.returnToSender();
	}

// keep
void	Driver::request(	Oscl::Usb::Alloc::Addr::
							Req::Api::AllocateNewAddressReq&	msg
							) noexcept{
	Oscl::Usb::Address	usbAddr	= allocateAddress();
	if(usbAddr == 0){
		msg.getPayload().setFailure();
		msg.returnToSender();
		return;
		}
	msg.getPayload().setSuccess(usbAddr);
	msg.returnToSender();
	}

// keep
void	Driver::request(	Oscl::Usb::Alloc::Addr::
							Req::Api::FreeAddressReq&		msg
							) noexcept{
	freeAddress(msg.getPayload().getAddress());
	msg.returnToSender();
	}

// keep
void	Driver::request(	Oscl::Usb::Alloc::Addr::
							Req::Api::UsbAddrResChangeReq&		msg
							) noexcept{
	if(usbAddressAvailable()){
		msg.returnToSender();
		}
	else{
		_usbAddrChangeList.put(&msg);
		}
	}

// keep
void	Driver::request(	Oscl::Usb::Alloc::Addr::
							Req::Api::CancelUsbAddrResChangeReq&	msg
							) noexcept{
	Oscl::Usb::Alloc::Addr::
	Req::Api::UsbAddrResChangeReq*	next;
	next	= _usbAddrChangeList.remove(&msg.getPayload()._toCancel);
	if(next){
		next->returnToSender();
		}
	msg.returnToSender();
	}

//static Oscl::Usb::Alloc::BW::Interrupt::BwRec	bwRec;

/*
	Interrupt IN:
		SYNC		8
		IN PID		8
		Addr		7
		Endpoint	4
		CRC5		5
		EOP			4

		IPD			8

		SYNC		8
		Data0/1 PID	8
		payload		(maxPacketSize*8)
		CRC16		16
		EOP			4

		IPD			8

		SYNC		8
		Ack PID		8
		EOP			3
		--------------
 */

void	Driver::request(	Alloc::BW::Interrupt::
							Req::Api::ReserveReq&	msg
							) noexcept{
#if 0 // OHCI implementation
	DeferredTaskMem*	dtMem	= allocDeferredTaskMem();
	if(!dtMem){
		_pendingDeferredTaskReqList.put(&msg);
		return;
		}
	const unsigned  exemptOverheadBits      = 75;
	const unsigned  nonExemptOverheadBits   = 32;

	ReservePayload&	payload	= msg.getPayload();
	unsigned	nBitTimes;
	nBitTimes	= payload._maxPacketSize * 8;
	nBitTimes	+= nonExemptOverheadBits;
	nBitTimes	*= 7;
	nBitTimes	+= 5;	// round up
	nBitTimes	/= 6;
	nBitTimes	+=	exemptOverheadBits;
	if(!_bwAlloc.bwAvailable(payload._pollPeriodInMilliseconds,nBitTimes)){
		msg.getPayload()._bwRec	= 0;
		msg.returnToSender();
		return;
		}
	new(dtMem) InterruptListAttachDefTask(	*this,
											msg,
											_bwAlloc
											);
#else
	// Simplified. No need for deferred tasks since
	// all lists (periodic,control,bulk) are serviced
	// from the same thread.

	const unsigned  exemptOverheadBits      = 75;
	const unsigned  nonExemptOverheadBits   = 32;

	ReservePayload&	payload	= msg.getPayload();
	unsigned	nBitTimes;
	nBitTimes	= payload._maxPacketSize * 8;
	nBitTimes	+= nonExemptOverheadBits;
	nBitTimes	*= 7;
	nBitTimes	+= 5;	// round up
	nBitTimes	/= 6;
	nBitTimes	+=	exemptOverheadBits;
	if(!_bwAlloc.bwAvailable(payload._pollPeriodInMilliseconds,nBitTimes)){
		msg.getPayload()._bwRec	= 0;
		msg.returnToSender();
		return;
		}

	// First get all the memory.
	EndpointDescMem*	edMem	= allocEdMem();
	TransactionDescMem*	tdMem	= allocTdMem();
	if(!edMem || !tdMem ){
		if(edMem) free(edMem);
		if(tdMem) free(tdMem);
		msg.getPayload()._bwRec	= 0;
		}
	else{
		// Next build the endpoint.
		Oscl::Usb::UsbHostSlave::ED::GeneralIn*	ed;
		TD::Descriptor*					td;
		ed	= new(edMem)ED::GeneralIn(
										0,		// usbAddress,
										0,		// endpointID,
										0,		// maxPacketSize,
										false	// lowSpeedDevice
										);
		td	= new(tdMem)
					TD::Descriptor(getUnexpectedTransferVector());
		ed->skip();

		// Put termination TD on ED list.
		ed->put(td);

		Oscl::Usb::Alloc::BW::Interrupt::BwRec*
		bwRec	= _bwAlloc.alloc(*ed);
		if(!bwRec){
			td->~Descriptor();
			free(tdMem);
			ed->~GeneralIn();
			free(edMem);
			}
		msg.getPayload()._bwRec	= bwRec;
		}
	msg.returnToSender();
#endif
	}

void	Driver::request(	Alloc::BW::Interrupt::
							Req::Api::FreeReq&		msg
							) noexcept{
#if 0 // OHCI implementation
	DeferredTaskMem*	dtMem	= allocDeferredTaskMem();
	if(!dtMem){
		_pendingDeferredTaskReqList.put(&msg);
		return;
		}
	new(dtMem) InterruptListDetachDefTask(	*this,
											msg,
											_bwAlloc
											);
#else
	// Simplified. No need for deferred tasks since
	// all lists (periodic,control,bulk) are serviced
	// from the same thread.
	_bwAlloc.free(msg.getPayload()._bwRecToFree);
	msg.returnToSender();
#endif
	}

void	Driver::request(	Alloc::BW::
							Req::Api::ChangeReq&	msg
							) noexcept{
	// FIXME:
	while(true);
	}

void	Driver::request(	Alloc::BW::
							Req::Api::CancelReq&	msg
							) noexcept{
	// FIXME:
	while(true);
	}

void	Driver::unexpectedDone(Oscl::Usb::UsbHostSlave::TD::Descriptor& desc) noexcept{
	Oscl::ErrorFatal::
	logAndExit("Terminating descriptor shouldn't complete\n");
	}

TransactionDescMem*	Driver::allocTdMem() noexcept{
	return _freeTDList.get();
	}

EndpointDescMem*	Driver::allocEdMem() noexcept{
	return _freeEDList.get();
	}

void				Driver::free(TransactionDescMem* m) noexcept{
	_freeTDList.put(m);
	}

void	Driver::free(EndpointDescMem* m) noexcept{
	return _freeEDList.put(m);
	}

void				Driver::free(Oscl::Usb::UsbHostSlave::TD::Descriptor& d) noexcept{
	d.~Descriptor();
	_freeTDList.put((TransactionDescMem*)&d);
	}

#if 0 // OHCI
void		Driver::queueSofTask(DeferredTaskApi& dt) noexcept{
	// List is not empty
	_sofDeferredTaskList.put(&dt);
	_irqEnable.startOfFrame();
	}

void		Driver::cancelSofTask(DeferredTaskApi& dt) noexcept{
	if(_sofDeferredTaskList.remove(&dt)){
		if(!_sofDeferredTaskList.first()){
			_irqDisable.startOfFrame();
			}
		}
	}

void		Driver::queueTdMemTask(DeferredTaskApi& dt) noexcept{
	// List is not empty
	_tdMemDeferredTaskList.put(&dt);
	}

void		Driver::cancelTdMemTask(DeferredTaskApi& dt) noexcept{
	_tdMemDeferredTaskList.remove(&dt);
	}

void		Driver::controlListFilled() noexcept{
	_cmdStatus.controlListFilled();
	}

void		Driver::bulkListFilled() noexcept{
	_cmdStatus.bulkListFilled();
	}
#endif

#if 0	// OHCI
// This operation is used by deferred SOF tasks.
unsigned    Driver::getCurrentFrameNumber() noexcept{

#if 0 // OHCI implementation
	return _hcca.frameNumber;
#else
	#warning "Need to do OC implementation"
#endif
	}
#endif

//Oscl::Bus::Api&	Driver::getDmaBusApi() noexcept{
//	return _dmaBus;
//	}

Oscl::Usb::UsbHostSlave::
TD::TransferApi&	Driver::getUnexpectedTransferVector() noexcept{
	return _unexpectedTransfer;
	}

MessagePipeRec*
Driver::allocMessagePipe(	Address		usbAddress,
							EndpointID	endpointID,
							unsigned	maxPacketSize,
							bool		lowSpeedDevice
							) noexcept{
	// First get all the memory.
	EndpointDescMem*	edMem	= _freeEDList.get();
	TransactionDescMem*	tdMem	= allocTdMem();
	PipeMem*			pipeMem	= _freePipeMem.get();
	if(!edMem || !tdMem || !pipeMem){
		if(edMem) _freeEDList.put(edMem);
		if(tdMem) free(tdMem);
		if(pipeMem) _freePipeMem.put(pipeMem);
		return 0;
		}

	// Next build the endpoint.
	Oscl::Usb::UsbHostSlave::ED::Message*	ed;
	TD::Descriptor*					td;
	MessagePipeRec*					ep;
	ed	= new(edMem)ED::Message(	usbAddress,
									endpointID,
									maxPacketSize,
									lowSpeedDevice
									);
	td	= new(tdMem)
				TD::Descriptor(	_unexpectedTransfer);

	// Put termination TD on ED list.
	ed->put(td);

	ep	= new(pipeMem) MessagePipeRec(_server.getPostMsgApi(),*ed,*this);

	return ep;
	}

InterruptInPipeRec*
Driver::allocInterruptInPipe(	ED::Descriptor&	ed,
								Address			usbAddress,
								EndpointID		endpointID,
								unsigned		maxPacketSize,
								bool			lowSpeedDevice
								) noexcept{
	// First get all the memory.
	PipeMem*			pipeMem	= _freePipeMem.get();
	if(!pipeMem){
		return 0;
		}

	// Save the value of the next endpoint pointer
	// while rebuilding the endpoint descriptor.
	ED::Descriptor*	nextED	= ed.getNextEndpointDescriptor();

	// Next re-build the endpoint.
	ED::GeneralIn*
	ined	= new(&ed)ED::GeneralIn(	usbAddress,
										endpointID,
										maxPacketSize,
										lowSpeedDevice
										);
	InterruptInPipeRec*
	pipe	= new(pipeMem)
				InterruptInPipeRec(	_server.getPostMsgApi(),
									*ined,
									*this
									);
	// Restore the pointer to the next endpoint descriptor
	ined->setNextED(nextED);

	return pipe;
	}

BulkInPipeRec*
Driver::allocBulkInPipe(	Address			usbAddress,
							EndpointID		endpointID,
							unsigned		maxPacketSize,
							bool			lowSpeedDevice
							) noexcept{
	// First get all the memory.
	EndpointDescMem*	edMem	= _freeEDList.get();
	TransactionDescMem*	tdMem	= allocTdMem();
	PipeMem*			pipeMem	= _freePipeMem.get();
	if(!edMem || !tdMem || !pipeMem){
		if(edMem) _freeEDList.put(edMem);
		if(tdMem) free(tdMem);
		if(pipeMem) _freePipeMem.put(pipeMem);
		return 0;
		}

	// Next build the endpoint.
	Oscl::Usb::UsbHostSlave::ED::GeneralIn*	ed;
	TD::Descriptor*					td;
	ed	= new(edMem)ED::GeneralIn(	usbAddress,
									endpointID,
									maxPacketSize,
									lowSpeedDevice
									);
	BulkInPipeRec*
	pipe	= new(pipeMem)
				BulkInPipeRec(	_server.getPostMsgApi(),
								*ed,
								*this
								);

	td	= new(tdMem)
				TD::Descriptor(	_unexpectedTransfer);

	// Put termination TD on ED list.
	ed->put(td);

	// Since this operation is invoked during
	// construction (before the bulk list is initialized)
	// we let the caller link the endpoint onto the bulk
	// endpoint list.

	return pipe;
	}

BulkOutPipeRec*
Driver::allocBulkOutPipe(	Address			usbAddress,
							EndpointID		endpointID,
							unsigned		maxPacketSize,
							bool			lowSpeedDevice
							) noexcept{
	// First get all the memory.
	EndpointDescMem*	edMem	= _freeEDList.get();
	TransactionDescMem*	tdMem	= allocTdMem();
	PipeMem*			pipeMem	= _freePipeMem.get();
	if(!edMem || !tdMem || !pipeMem){
		if(edMem) _freeEDList.put(edMem);
		if(tdMem) free(tdMem);
		if(pipeMem) _freePipeMem.put(pipeMem);
		return 0;
		}

	// Next build the endpoint.
	TD::Descriptor*					td;
	ED::GeneralOut*
	ed	= new(edMem)ED::GeneralOut(	usbAddress,
									endpointID,
									maxPacketSize,
									lowSpeedDevice
									);
	BulkOutPipeRec*
	pipe	= new(pipeMem)
				BulkOutPipeRec(	_server.getPostMsgApi(),
								*ed,
								*this
								);

	td	= new(tdMem)
				TD::Descriptor(	_unexpectedTransfer);

	// Put termination TD on ED list.
	ed->put(td);

	// Since this operation is invoked during
	// construction (before the bulk list is initialized)
	// we let the caller link the endpoint onto the bulk
	// endpoint list.

	return pipe;
	}

#if 0
InterruptOutPipeRec*
Driver::allocInterruptOutPipe(	ED::Descriptor&	ed,
								Address			usbAddress,
								EndpointID		endpointID,
								unsigned		maxPacketSize,
								bool			lowSpeedDevice,
								bool			in
								) noexcept{
	// FIXME
	while(true);
	// First get all the memory.
	PipeMem*			pipeMem	= _freePipeMem.get();
	if(!pipeMem){
		return 0;
		}

	// Get termination TD
	TD::Descriptor*	td		= ed.getTdQueueTailPointer();
	ED::Descriptor*	nextED	= ed.getNextEndpointDescriptor();

	new(&ed)ED::GeneralOut(	_dmaBus,
							usbAddress,
							endpointID,
							maxPacketSize,
							lowSpeedDevice
							);
	InterruptOutPipeRec*
	ep	= 0;
//	ep	= new(pipeMem)
//			InterruptOutPipeRec(	_server.getPostMsgApi(),
//									*ed,
//									*this
//									);

	// Put termination TD on ED list.
	ed.put(td);
	ed.setNextED(nextED);

	// Link new ED to control list?

	return ep;
	}
#endif

void	Driver::free(MessagePipeRec& pipe) noexcept{
	Oscl::Usb::UsbHostSlave::ED::Message&	ed	= pipe._pipe.getED();

	_freeEDList.put((EndpointDescMem*)&ed);

	pipe.~MessagePipeRec();
	_freePipeMem.put((PipeMem*)&pipe);

	notifyPipeResourceAvailable();
	}

void	Driver::free(InterruptInPipeRec& pipe) noexcept{
	Oscl::Usb::UsbHostSlave::ED::GeneralIn&	ed	= pipe._pipe.getED();

	_freeEDList.put((EndpointDescMem*)&ed);

	pipe.~InterruptInPipeRec();
	_freePipeMem.put((PipeMem*)&pipe);

	notifyPipeResourceAvailable();
	}

void	Driver::free(BulkInPipeRec& pipe) noexcept{
	Oscl::Usb::UsbHostSlave::ED::GeneralIn&	ed	= pipe._pipe.getED();

	_freeEDList.put((EndpointDescMem*)&ed);

	pipe.~BulkInPipeRec();
	_freePipeMem.put((PipeMem*)&pipe);

	notifyPipeResourceAvailable();
	}

void	Driver::free(BulkOutPipeRec& pipe) noexcept{
	Oscl::Usb::UsbHostSlave::ED::GeneralOut&	ed	= pipe._pipe.getED();

	_freeEDList.put((EndpointDescMem*)&ed);

	pipe.~BulkOutPipeRec();
	_freePipeMem.put((PipeMem*)&pipe);

	notifyPipeResourceAvailable();
	}

void	Driver::notifyPipeResourceAvailable() noexcept{
	Oscl::Usb::Alloc::Pipe::
	Interrupt::IN::Req::Api::NotifyChangeReq*	iin;
	while((iin=_intInPipeResChangeList.get())){
		iin->returnToSender();
		}

	Oscl::Usb::Alloc::Pipe::Message::Req::Api::NotifyChangeReq*	msg;
	while((msg=_msgPipeResChangeList.get())){
		msg->returnToSender();
		}

	Oscl::Usb::Alloc::Pipe::
	Bulk::IN::Req::Api::NotifyChangeReq*	bulkin;
	while((bulkin=_bulkInPipeResChangeList.get())){
		bulkin->returnToSender();
		}

	Oscl::Usb::Alloc::Pipe::
	Bulk::OUT::Req::Api::NotifyChangeReq*	bulkout;
	while((bulkout=_bulkOutPipeResChangeList.get())){
		bulkout->returnToSender();
		}
	}

#if 0 // OHCI
//================= ControlListAttachDefTask ========

Driver::ControlListAttachDefTask::
ControlListAttachDefTask(	Driver&				context,
							Oscl::Usb::Alloc::
							Pipe::Message::
							Req::Api::NewReq&	msg,
							MessagePipeRec&		pipe
							) noexcept:
		_context(context),
		_msg(msg),
		_pipe(pipe)
		{
	_context.disableControlList();
	_currentFrameNumber	= _context.getCurrentFrameNumber();
	queueSofTask();
	}

void	Driver::ControlListAttachDefTask::queueSofTask() noexcept{
	DeferredTask<ControlListAttachDefTask>*
	dt	= new(&_dtMem)
			DeferredTask<	ControlListAttachDefTask
							>(	*this,
								&Driver::ControlListAttachDefTask::sofDone
								);
	_context.queueSofTask(*dt);
	}

void	Driver::ControlListAttachDefTask::sofDone() noexcept{
	if(_currentFrameNumber == _context.getCurrentFrameNumber()){
		queueSofTask();
		return;
		}
	_msg.getPayload().setPipeApi(&_pipe._pipe);
	_msg.returnToSender();
	_context.insertToControlList(_pipe._pipe.getED());
	_context.enableControlList();
	_context.release(*this);
	}

//================= ControlListDetachDefTask ========

Driver::ControlListDetachDefTask::
ControlListDetachDefTask(	Driver&				context,
							Oscl::Usb::Alloc::
							Pipe::Message::
							Req::Api::FreeReq&	msg
							) noexcept:
		_context(context),
		_msg(msg)
		{
	_context.disableControlList();
	_currentFrameNumber	= _context.getCurrentFrameNumber();
	queueSofTask();
	}

void	Driver::ControlListDetachDefTask::queueSofTask() noexcept{
	DeferredTask<ControlListDetachDefTask>*
	dt	= new(&_dtMem)
			DeferredTask<	ControlListDetachDefTask
							>(	*this,
								&Driver::ControlListDetachDefTask::sofDone
								);
	_context.queueSofTask(*dt);
	}

void	Driver::ControlListDetachDefTask::sofDone() noexcept{
	if(_currentFrameNumber == _context.getCurrentFrameNumber()){
		queueSofTask();
		return;
		}
	Oscl::Usb::Pipe::Message::PipeApi*
	pipeApi	= &_msg.getPayload().getPipeApi();
	MessagePipeRec*
	pipe	= _context.removeActiveMessageEp(*pipeApi);
	if(pipe){
		_context.removeFromControlList(pipe->_pipe.getED());
		_context.free(*pipe);
		}
	_msg.returnToSender();
	_context.enableControlList();
	_context.release(*this);
	}

//================= InterruptListAttachDefTask ========

Driver::InterruptListAttachDefTask::
InterruptListAttachDefTask(	Driver&				context,
							Oscl::Usb::Alloc::
							BW::Interrupt::
							Req::Api::
							ReserveReq&			msg,
							Oscl::Usb::UsbHostSlave::
							BW::Allocator&		allocator
							) noexcept:
		_context(context),
		_msg(msg),
		_allocator(allocator)
		{
	_context.disablePeriodicList();
	_currentFrameNumber	= _context.getCurrentFrameNumber();
	queueSofTask();
	}

void	Driver::InterruptListAttachDefTask::queueSofTask() noexcept{
	DeferredTask<InterruptListAttachDefTask>*
	dt	= new(&_dtMem)
			DeferredTask<	InterruptListAttachDefTask
							>(	*this,
								&Driver::InterruptListAttachDefTask::sofDone
								);
	_context.queueSofTask(*dt);
	}

void	Driver::InterruptListAttachDefTask::sofDone() noexcept{
	if(_currentFrameNumber == _context.getCurrentFrameNumber()){
		queueSofTask();
		return;
		}
	// First get all the memory.
	EndpointDescMem*	edMem	= _context.allocEdMem();
	TransactionDescMem*	tdMem	= _context.allocTdMem();
	if(!edMem || !tdMem ){
		if(edMem) _context.free(edMem);
		if(tdMem) _context.free(tdMem);
		_msg.getPayload()._bwRec	= 0;
		}
	else{
		// Next build the endpoint.
		Oscl::Usb::UsbHostSlave::ED::GeneralIn*	ed;
		TD::Descriptor*					td;
		ed	= new(edMem)ED::GeneralIn(	_context.getDmaBusApi(),
										0,		// usbAddress,
										0,		// endpointID,
										0,		// maxPacketSize,
										false	// lowSpeedDevice
										);
		td	= new(tdMem)
					TD::Descriptor(	_context.getUnexpectedTransferVector(),
									_context.getDmaBusApi()
									);
		ed->skip();

		// Put termination TD on ED list.
		ed->put(td);

		Oscl::Usb::Alloc::BW::Interrupt::BwRec*
		bwRec	= _allocator.alloc(*ed);
		if(!bwRec){
			td->~Descriptor();
			_context.free(tdMem);
			ed->~GeneralIn();
			_context.free(edMem);
			}
		_msg.getPayload()._bwRec	= bwRec;
		}
	_msg.returnToSender();
	_context.enablePeriodicList();
	_context.release(*this);
	}

//================= InterruptListDetachDefTask ========

Driver::InterruptListDetachDefTask::
InterruptListDetachDefTask(	Driver&				context,
							Oscl::Usb::Alloc::
							BW::Interrupt::
							Req::Api::
							FreeReq&			msg,
							Oscl::Usb::UsbHostSlave::
							BW::Allocator&		allocator
							) noexcept:
		_context(context),
		_msg(msg),
		_allocator(allocator)
		{
	_context.disablePeriodicList();
	_currentFrameNumber	= _context.getCurrentFrameNumber();
	queueSofTask();
	}

void	Driver::InterruptListDetachDefTask::queueSofTask() noexcept{
	DeferredTask<InterruptListDetachDefTask>*
	dt	= new(&_dtMem)
			DeferredTask<	InterruptListDetachDefTask
							>(	*this,
								&Driver::InterruptListDetachDefTask::sofDone
								);
	_context.queueSofTask(*dt);
	}

void	Driver::InterruptListDetachDefTask::sofDone() noexcept{
	if(_currentFrameNumber == _context.getCurrentFrameNumber()){
		queueSofTask();
		return;
		}
	_allocator.free(_msg.getPayload()._bwRecToFree);
	_msg.returnToSender();
	_context.enablePeriodicList();
	_context.release(*this);
	}

//================= BulkInListAttachDefTask ========

Driver::BulkInListAttachDefTask::
BulkInListAttachDefTask(	Driver&				context,
							Oscl::Usb::Alloc::
							Pipe::Bulk::IN::
							Req::Api::NewReq&	msg,
							BulkInPipeRec&		pipe
							) noexcept:
		_context(context),
		_msg(msg),
		_pipe(pipe)
		{
	_context.disableBulkList();
	_currentFrameNumber	= _context.getCurrentFrameNumber();
	queueSofTask();
	}

void	Driver::BulkInListAttachDefTask::queueSofTask() noexcept{
	DeferredTask<BulkInListAttachDefTask>*
	dt	= new(&_dtMem)
			DeferredTask<	BulkInListAttachDefTask
							>(	*this,
								&Driver::BulkInListAttachDefTask::sofDone
								);
	_context.queueSofTask(*dt);
	}

void	Driver::BulkInListAttachDefTask::sofDone() noexcept{
	if(_currentFrameNumber == _context.getCurrentFrameNumber()){
		queueSofTask();
		return;
		}
	_msg.getPayload().setPipeApi(&_pipe._pipe);
	_msg.returnToSender();
	_context.insertToBulkList(_pipe._pipe.getED());
	_context.enableBulkList();
	_context.release(*this);
	}

//================= BulkInListDetachDefTask ========

Driver::BulkInListDetachDefTask::
BulkInListDetachDefTask(	Driver&				context,
							Oscl::Usb::Alloc::
							Pipe::Bulk::IN::
							Req::Api::FreeReq&	msg
							) noexcept:
		_context(context),
		_msg(msg)
		{
	_context.disableBulkList();
	_currentFrameNumber	= _context.getCurrentFrameNumber();
	queueSofTask();
	}

void	Driver::BulkInListDetachDefTask::queueSofTask() noexcept{
	DeferredTask<BulkInListDetachDefTask>*
	dt	= new(&_dtMem)
			DeferredTask<	BulkInListDetachDefTask
							>(	*this,
								&Driver::BulkInListDetachDefTask::sofDone
								);
	_context.queueSofTask(*dt);
	}

void	Driver::BulkInListDetachDefTask::sofDone() noexcept{
	if(_currentFrameNumber == _context.getCurrentFrameNumber()){
		queueSofTask();
		return;
		}
	Oscl::Usb::Pipe::Bulk::IN::PipeApi*
	pipeApi	= &_msg.getPayload().getPipeApi();
	BulkInPipeRec*
	pipe	= _context.removeActiveBulkInPipe(*pipeApi);
	if(pipe){
		_context.removeFromBulkList(pipe->_pipe.getED());
		_context.free(*pipe);
		}
	_msg.returnToSender();
	_context.enableBulkList();
	_context.release(*this);
	}

//================= BulkOutListAttachDefTask ========

Driver::BulkOutListAttachDefTask::
BulkOutListAttachDefTask(	Driver&				context,
							Oscl::Usb::Alloc::
							Pipe::Bulk::OUT::
							Req::Api::NewReq&	msg,
							BulkOutPipeRec&		pipe
							) noexcept:
		_context(context),
		_msg(msg),
		_pipe(pipe)
		{
	_context.disableBulkList();
	_currentFrameNumber	= _context.getCurrentFrameNumber();
	queueSofTask();
	}

void	Driver::BulkOutListAttachDefTask::queueSofTask() noexcept{
	DeferredTask<BulkOutListAttachDefTask>*
	dt	= new(&_dtMem)
			DeferredTask<	BulkOutListAttachDefTask
							>(	*this,
								&Driver::BulkOutListAttachDefTask::sofDone
								);
	_context.queueSofTask(*dt);
	}

void	Driver::BulkOutListAttachDefTask::sofDone() noexcept{
	if(_currentFrameNumber == _context.getCurrentFrameNumber()){
		queueSofTask();
		return;
		}
	_msg.getPayload().setPipeApi(&_pipe._pipe);
	_msg.returnToSender();
	_context.insertToBulkList(_pipe._pipe.getED());
	_context.enableBulkList();
	_context.release(*this);
	}

//================= BulkOutListDetachDefTask ========

Driver::BulkOutListDetachDefTask::
BulkOutListDetachDefTask(	Driver&				context,
							Oscl::Usb::Alloc::
							Pipe::Bulk::OUT::
							Req::Api::FreeReq&	msg
							) noexcept:
		_context(context),
		_msg(msg)
		{
	_context.disableBulkList();
	_currentFrameNumber	= _context.getCurrentFrameNumber();
	queueSofTask();
	}

void	Driver::BulkOutListDetachDefTask::queueSofTask() noexcept{
	DeferredTask<BulkOutListDetachDefTask>*
	dt	= new(&_dtMem)
			DeferredTask<	BulkOutListDetachDefTask
							>(	*this,
								&Driver::BulkOutListDetachDefTask::sofDone
								);
	_context.queueSofTask(*dt);
	}

void	Driver::BulkOutListDetachDefTask::sofDone() noexcept{
	if(_currentFrameNumber == _context.getCurrentFrameNumber()){
		queueSofTask();
		return;
		}
	Oscl::Usb::Pipe::Bulk::OUT::PipeApi*
	pipeApi	= &_msg.getPayload().getPipeApi();
	BulkOutPipeRec*
	pipe	= _context.removeActiveBulkOutPipe(*pipeApi);
	if(pipe){
		_context.removeFromBulkList(pipe->_pipe.getED());
		_context.free(*pipe);
		}
	_msg.returnToSender();
	_context.enableBulkList();
	_context.release(*this);
	}
#endif
