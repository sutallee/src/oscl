/*
   Copyright (C) 2007 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_usb_usbhostslave_hcd_server_edmemh_
#define _oscl_drv_usb_usbhostslave_hcd_server_edmemh_
#include "oscl/driver/usb/usbhostslave/ed/in.h"
#include "oscl/driver/usb/usbhostslave/ed/out.h"
#include "oscl/driver/usb/usbhostslave/ed/isoin.h"
#include "oscl/driver/usb/usbhostslave/ed/isoout.h"
#include "oscl/driver/usb/usbhostslave/ed/message.h"
#include "oscl/memory/block.h"

/** */
namespace Oscl {
/** */
namespace Usb {
/** */
namespace UsbHostSlave {
/** */
namespace HCD {

/** */
typedef union EndpointDescMem {
	void*		__qitemlink;
	Oscl::Memory::AlignedBlock
		<sizeof(Oscl::Usb::UsbHostSlave::ED::GeneralIn)>		_in;
	Oscl::Memory::AlignedBlock
		<sizeof(Oscl::Usb::UsbHostSlave::ED::IsochronousIn)>	_isoin;
	Oscl::Memory::AlignedBlock
		<sizeof(Oscl::Usb::UsbHostSlave::ED::IsochronousOut)>	_isoout;
	Oscl::Memory::AlignedBlock
		<sizeof(Oscl::Usb::UsbHostSlave::ED::Message)>			_message;
	Oscl::Memory::AlignedBlock
		<sizeof(Oscl::Usb::UsbHostSlave::ED::GeneralOut)>		_out;
	} EndpointDescMem;

}
}
}
}


#endif
