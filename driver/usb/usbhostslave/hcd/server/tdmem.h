/*
   Copyright (C) 2007 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_usb_usbhostslave_hcd_server_tdmemh_
#define _oscl_drv_usb_usbhostslave_hcd_server_tdmemh_
#include "oscl/driver/usb/usbhostslave/td/general/desc.h"
#include "oscl/driver/usb/usbhostslave/td/isoch/desc.h"
#include "oscl/memory/block.h"

/** */
namespace Oscl {
/** */
namespace Usb {
/** */
namespace UsbHostSlave {
/** */
namespace HCD {

/** */
typedef struct TransactionDescMem {
	/** This union defines the memory needs for the various
		transaction descriptor types.
	 */
	union {
		/** */
		Oscl::Memory::AlignedBlock
			<sizeof(Oscl::Usb::UsbHostSlave::TD::General::Setup)>			_setup;
		/** */
		Oscl::Memory::AlignedBlock
			<sizeof(Oscl::Usb::UsbHostSlave::TD::General::In)>				_in;
		/** */
		Oscl::Memory::AlignedBlock
			<sizeof(Oscl::Usb::UsbHostSlave::TD::General::Out)>				_out;
		/** */
		Oscl::Memory::AlignedBlock
			<sizeof(Oscl::Usb::UsbHostSlave::TD::Isochronous::Descriptor)>	_isoch;
		}mem ;
	/** It is important that this link be at the end of
		this data structure. It may be used by the current
		owner to keep track of its outstanding transfer
		descriptors.
	 */
	void*		__qitemlink;
	}TransactionDescMem;

}
}
}
}


#endif
