/*
   Copyright (C) 2007 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_usb_usbhostslave_hcd_server_iinpipeh_
#define _oscl_drv_usb_usbhostslave_hcd_server_iinpipeh_
#include "oscl/driver/usb/pipe/interrupt/in/pipeapi.h"
#include "oscl/driver/usb/usbhostslave/hcd/pipe/interrupt/in/driver.h"

/** */
namespace Oscl {
/** */
namespace Usb {
/** */
namespace UsbHostSlave {
/** */
namespace HCD {

/** */
class InterruptInPipe : public Oscl::Usb::Pipe::Interrupt::IN::PipeApi {
	private:
		/** */
		Oscl::Usb::UsbHostSlave::HCD::Pipe::Interrupt::IN::Driver	_driver;
	public:
		/** */
		InterruptInPipe(	Oscl::Mt::Itc::PostMsgApi&			myPapi,
							Oscl::Usb::UsbHostSlave::ED::GeneralIn&		ed,
							Oscl::Usb::UsbHostSlave::HCD::HcdApi&		hcdContext
							) noexcept;
		/** */
		Oscl::Usb::UsbHostSlave::ED::GeneralIn&			getED() noexcept;

	public:	// Oscl::Usb::Pipe::Interrupt::IN::PipeApi
        /** */
		Oscl::Mt::Itc::Srv::
		Close::Req::Api::SAP&			getCloseSAP() noexcept;
        /** */
		Oscl::Mt::Itc::Srv::
		OpenCloseSyncApi&				getOpenCloseSyncApi() noexcept;
		/** */
		Oscl::Usb::Pipe::Interrupt::IN::
		Req::Api::SAP&					getSAP() noexcept;
	};

/** */
class InterruptInPipeRec : public Oscl::QueueItem {
	public:
		/** */
		InterruptInPipe		_pipe;
	public:
		/** */
		InterruptInPipeRec(	Oscl::Mt::Itc::PostMsgApi&			myPapi,
							Oscl::Usb::UsbHostSlave::ED::GeneralIn&		ed,
							Oscl::Usb::UsbHostSlave::HCD::HcdApi&		hcdContext
							) noexcept;
	};

}
}
}
}

#endif
