/*
   Copyright (C) 2007 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_usb_usbhostslave_device_rhreqapih_
#define _oscl_drv_usb_usbhostslave_device_rhreqapih_
#include "oscl/mt/itc/mbox/srvevt.h"
#include "oscl/mt/itc/mbox/sap.h"

/** */
namespace Oscl {
/** */
namespace Usb {
/** */
namespace UsbHostSlave {
/** */
namespace HCD {
/** */
namespace RootHub {
/** */
namespace Req {

/** */
class Api {
	public:	// Payloads
		/** */
		class OpenPayload {};
		/** */
		class ClosePayload {};
		/** */
		class StatusChangePayload {};
		/** */
		class CancelAllPayload {};
	public:	// Requests
		/** */
		typedef Oscl::Mt::Itc::
				SrvEvent<Api,OpenPayload>			OpenReq;
		/** */
		typedef Oscl::Mt::Itc::
				SrvEvent<Api,ClosePayload>		CloseReq;
		/** */
		typedef Oscl::Mt::Itc::
				SrvEvent<Api,StatusChangePayload>	ChangeReq;
		/** */
		typedef Oscl::Mt::Itc::
				SrvEvent<Api,CancelAllPayload>	CancelAllReq;
	public:	// Requests
		/** */
		typedef Oscl::Mt::Itc::SAP<Api>			SAP;
		/** */
		typedef Oscl::Mt::Itc::ConcreteSAP<Api>	ConcreteSAP;

	public:
		/** Shut-up GCC. */
		virtual ~Api() {}
		/** */
		virtual void	request(OpenReq& msg) noexcept=0;
		/** */
		virtual void	request(CloseReq& msg) noexcept=0;
		/** */
		virtual void	request(ChangeReq& msg) noexcept=0;
		/** */
		virtual void	request(CancelAllReq& msg) noexcept=0;
	};

}
}
}
}
}
}


#endif
