/*
   Copyright (C) 2007 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "desc.h"
#include "oscl/driver/usb/usbhostslave/td/desc.h"
#include "oscl/cpu/sync.h"
#include "oscl/error/fatal.h"

using namespace Oscl::Usb::UsbHostSlave::ED;

Descriptor::Descriptor(	Address			address,
						EndpointID		endpoint,
						unsigned		maxPacketSize,
						bool			lowSpeed,
						bool			isochronous
						) noexcept:
		__qitemlink(0),
		_address(address),
		_endpoint(endpoint),
		_maxPacketSize(maxPacketSize),
		_lowSpeed(lowSpeed),
		_isochronous(isochronous),
		_skip(false),
		_isHalted(false)
		{
	}

void	Descriptor::skip() noexcept{
	_skip	= true;
	}

void	Descriptor::dontSkip() noexcept{
	_skip	= false;
	}

void	Descriptor::halt() noexcept{
	_isHalted	= true;
	}

bool	Descriptor::isHalted() noexcept{
	return _isHalted;
	}

bool	Descriptor::tdListIsEmpty() noexcept{
	return _tdList.first()?false:true;
	}

void	Descriptor::clearHalted() noexcept{
	_isHalted	= false;
	}

void	Descriptor::setAddress(Address address) noexcept{
	_address	= address;
	}

void	Descriptor::put(Oscl::Usb::UsbHostSlave::TD::Descriptor* td) noexcept{
	_tdList.put(&td->_tdLink);
	}

Oscl::Usb::UsbHostSlave::TD::Descriptor*
Descriptor::remove(Oscl::Usb::UsbHostSlave::TD::Descriptor* td) noexcept{
	Oscl::Usb::UsbHostSlave::TD::Descriptor::Link*
	p = _tdList.remove(&td->_tdLink);
	if(p){
		return &p->_container;
		}
	else {
		return 0;
		}
	}

unsigned	Descriptor::getMaxPacketSize() noexcept{
	return _maxPacketSize;
	}

Descriptor*	Descriptor::getNextEndpointDescriptor() noexcept{
	return (Descriptor*)__qitemlink;
	}

void	Descriptor::setNextED(Descriptor* ed) noexcept{
	__qitemlink	= ed;
	}

void	Descriptor::insert(Descriptor& desc) noexcept{
	using namespace Oscl::Usb::UsbHostSlave::ED;
	void*		save	= __qitemlink;
	__qitemlink			= &desc;
	desc.__qitemlink	= save;
	}

void*&		Descriptor::getRefToNextPtr() noexcept{
	return __qitemlink;
	}

