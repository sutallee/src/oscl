/*
   Copyright (C) 2007 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_usb_usbhostslave_ed_desch_
#define _oscl_drv_usb_usbhostslave_ed_desch_
#include "oscl/driver/usb/address.h"
#include "oscl/driver/usb/endpoint.h"
#include "oscl/driver/usb/usbhostslave/td/desc.h"
#include "oscl/queue/queue.h"

/** */
namespace Oscl {
/** */
namespace Usb {
/** */
namespace UsbHostSlave {

/** */
namespace ED {

/** This class initializes and provides operations for manipulating
	the various fields of the UsbHostSlave USB endpoint structure.
 */
class Descriptor {
	public:
		enum Direction {
			GetDirFromTD	= 0,
			OUT,
			IN,
			GetDirFromTDAlt
			};
	public:
		/** This link is used to maintain a list of ED::Descriptors.
		 */
		void*			__qitemlink;

		/** This is a list of pending ED::Descriptors
		 */
		Oscl::Queue<Oscl::Usb::UsbHostSlave::TD::Descriptor::Link>	_tdList;

	protected:
		/** The USB device address associated with this endpoint descriptor */
		Address			_address;

		/** The USB endpoint addresss associated with this endpoint descriptor */
		EndpointID		_endpoint;

		/** The maximum packet size associated with this endpoint descriptor */
		unsigned		_maxPacketSize;

		/** This field is true for a Low Speed descriptor and false for Full Speed.*/
		bool			_lowSpeed;

		/** This field is true for an isochronous descriptor and false for general.*/
		bool			_isochronous;

		/** This field is true if this TD should be skipped.*/
		bool			_skip;

		/** This field is true if this TD should be skipped.*/
		bool			_isHalted;

		/** */
		enum Direction	_direction;
	public:
		/** */
		Descriptor(	Address			address,
					EndpointID		endpoint,
					unsigned		maxPacketSize,
					bool			lowSpeed,
					bool			isochronous
					) noexcept;
		/** */
		void	skip() noexcept;
		/** */
		void	dontSkip() noexcept;
		/** */
		void	halt() noexcept;
		/** */
		bool	isHalted() noexcept;
		/** */
		bool	tdListIsEmpty() noexcept;
		/** */
		void	clearHalted() noexcept;
		/** */
		void	setAddress(Address address) noexcept;
		/** */
		void	insert(Descriptor& desc) noexcept;
		/** */
		Descriptor*	getNextEndpointDescriptor() noexcept;
		/** */
		void		setNextED(Descriptor* ed) noexcept;
		/** */
		void	put(TD::Descriptor* td) noexcept;
		/** */
		TD::Descriptor*	remove(TD::Descriptor* td) noexcept;
		/** */
		unsigned	getMaxPacketSize() noexcept;
		/** */
		void*&		getRefToNextPtr() noexcept;

	};

}
}
}
}

#endif
