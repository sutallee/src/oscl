/*
   Copyright (C) 2007 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_usb_usbhostslave_td_desch_
#define _oscl_drv_usb_usbhostslave_td_desch_
#include "oscl/bus/api.h"
#include "transapi.h"
#include <stdint.h>

/** */
namespace Oscl {
/** */
namespace Usb {
/** */
namespace UsbHostSlave {
/** */
namespace TD {

/** This class initializes and provides operations for manipulating
	the various fields of the UsbHostSlave USB transfer descriptor structure.
 */
class Descriptor {
	public:
		/** */
		struct Link {
			/** */
			void*				__qitemlink;
			/** */
			Descriptor&			_container;
			/** */
			Link(Descriptor& container) noexcept:
					_container(container)
					{ }
			
			};
	public:
		/** */
		enum CC {
			NoError	= 0,
			CRC,
			BitStuffing,
			DataToggleMismatch,
			Stall,
			DeviceNotResponding,
			PidCheckFailure,
			UnexpectedPID,
			DataOverrun,
			DataUnderrun,
			BufferOverrun,
			BufferUnderrun,
			NotAccessed0,
			NotAccessed1
			};

		/** */
		enum XferType {
			SETUP	= 0,
			IN,
			OUT
			};

		/** */
		enum TSource {
			TOGGLE	= 0,
			LSB
			};

	protected:
		enum CC				_cc;
		/** */
		bool				_shortPacketOK;

		/** Pointer to _buffer used for data transfer */
		void*				_buffer;

		/** Number of valid octets in _buffer */
		unsigned			_size;

		/** */
		TransferApi&		_tapi;

		/** */
		enum XferType		_type;

		/** */
		enum TSource		_tSource;

		/** */
		bool				_data01;

		/** Only for isochronous frames */
		unsigned char		_frameCountValue;

		/** Only for isochronous frames */
		uint16_t			_startFrame;

	public:
		/** This field may be used by the owner of
			this TD to keep track of outstanding
			transfers.
		 */
		Link				_ownerLink;

		/** This link is used to maintain a list of TD::Descriptors.
		 */
		Link				_tdLink;

	public:
		/** This is used by the done head mechanism.
		 */
//		Descriptor*			_doneStackLink;

	public:
		/** */
		Descriptor(	TransferApi&		tapi
					) noexcept;
		/** */
		Descriptor*		nextTD() noexcept;
		/** */
		void			setNextTD(Descriptor* td) noexcept;
		/** */
		TransferApi&	getTransferApi() noexcept;
		/** */
		CC				result() noexcept;
	};

}
}
}
}

#endif
