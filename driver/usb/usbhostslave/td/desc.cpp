/*
   Copyright (C) 2007 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "desc.h"

using namespace Oscl::Usb::UsbHostSlave::TD;
using namespace Oscl;

Descriptor::Descriptor(	TransferApi&		tapi
						) noexcept:
		_cc(NotAccessed1),
		_tapi(tapi),
		_ownerLink(*this),
		_tdLink(*this)
		{
	}

Descriptor*		Descriptor::nextTD() noexcept{
	return (Descriptor*)_tdLink.__qitemlink;
	}

void			Descriptor::setNextTD(Descriptor* td) noexcept{
	_tdLink.__qitemlink	= td;
	}

Oscl::Usb::UsbHostSlave::TD::TransferApi&	Descriptor::getTransferApi() noexcept{
	return _tapi;
	}

Oscl::Usb::UsbHostSlave::TD::Descriptor::CC
Descriptor::result() noexcept{
	return _cc;
	}

