/*
   Copyright (C) 2007 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "desc.h"
#include "oscl/error/fatal.h"
#include "oscl/kernel/mmu.h"			// FIXME: This should not be here.
										// A UsbHostSlave specific call should
										// be used to translate virtual
										// user addresses to physical
										// addresses. The OS environment
										// implementation
										// could then invoke the Plat
										// version. Also, should the
										// buffer be cache flushed when
										// loaded?


using namespace Oscl::Usb::UsbHostSlave::TD::General;

Descriptor::Descriptor(	TransferApi&		tapi,
						bool				shortPacketOK,
						void*				buffer,
						unsigned			size
						) noexcept:
		TD::Descriptor(tapi)
		{
	_shortPacketOK	= shortPacketOK;

	_buffer	= buffer;

	_size	= size;
	}

Oscl::Usb::UsbHostSlave::TD::TransferApi&	Descriptor::getTransferApi() noexcept{
	return _tapi;
	}

void*	Descriptor::getCurrentBufferPointer() const noexcept{
	return _buffer;
	}

Setup::Setup(	TransferApi&		tapi,
				void*				buffer
				) noexcept:
		Descriptor(tapi,false,buffer,8)
		{
	_type		= SETUP;
	_tSource	= LSB;
	_data01		= 0;
	}

In::In(	TransferApi&	tapi,
		bool			shortPacketOK,
		void*			buffer,
		unsigned		bufferSize,
		bool			dataToggleSourceTD,
		bool			dataToggleValue
		) noexcept:
		Descriptor(tapi,shortPacketOK,buffer,bufferSize)
		{
	_tSource	= (dataToggleSourceTD)
									? LSB
									: TOGGLE
									;
	_data01	= (dataToggleValue)
									? true
									: false
									;

	_type	= IN;
	}

Out::Out(	TransferApi&	tapi,
			void*			buffer,
			unsigned		nTxBytes
			) noexcept:
		Descriptor(tapi,false,buffer,nTxBytes)
		{
	_type	= OUT;
	}

StatusIn::StatusIn(	TransferApi&		tapi
					) noexcept:
		Descriptor(	tapi,
					true,
					0,
					0
					)
		{
	
	_type		= IN;
	_tSource	= LSB;
	_data01		= true;
	}

StatusOut::StatusOut(	TransferApi&		tapi
						) noexcept:
		Descriptor(	tapi,
					true,
					0,
					0
					)
		{
	_type		= OUT;
	_tSource	= LSB;
	_data01		= true;
	}

