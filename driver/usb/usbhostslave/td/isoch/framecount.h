/*
   Copyright (C) 2007 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_usb_usbhostslave_framecounth_
#define _oscl_drv_usb_usbhostslave_framecounth_
//#include "oscl/hw/usb/usbhostslave/memory.h"

/** */
namespace Oscl {
/** */
namespace Usb {
/** */
namespace UsbHostSlave {
/** */
namespace TD {
/** */
namespace Isochronous {

/** The purpose of this abstract class is to provide a type safe
	means of specifying the interrupt delay field for USB 
	transfer descriptors.
 */
class FrameCount {
	private:
		/** This member contains the DI field of a transfer descriptor
			in the least significant bits of the value. This is *not*
			a "ValueMask", and must be shifted into the proper position
			before being placed in the UsbHostSlave transfer descriptor.
		 */
		unsigned char	_frameCountValue;
	protected:
		/** */
		FrameCount(unsigned char frameCountValue) noexcept:
			_frameCountValue(frameCountValue)
			{
			if(frameCountValue > 7){
				while(true);
				}
			}
	public:
		/** */
		FrameCount(const FrameCount& frameCount) noexcept:
			_frameCountValue(frameCount._frameCountValue)
			{}
		unsigned char	getValue() const noexcept{
			return _frameCountValue;
			};
	};

/** */
class OneFrame : public FrameCount {
	public:
		/** */
		OneFrame() noexcept:
			FrameCount(0)
			{}
		/** */
		OneFrame(const OneFrame& other) noexcept:
			FrameCount(other)
			{}
	};

/** */
class TwoFrame : public FrameCount {
	public:
		/** */
		TwoFrame() noexcept:
			FrameCount(1)
			{}
	};

/** */
class ThreeFrame : public FrameCount {
	public:
		/** */
		ThreeFrame() noexcept:
			FrameCount(2)
			{}
	};

/** */
class FourFrame : public FrameCount {
	public:
		/** */
		FourFrame() noexcept:
			FrameCount(3)
			{}
	};

/** */
class FiveFrame : public FrameCount {
	public:
		/** */
		FiveFrame() noexcept:
			FrameCount(4)
			{}
	};

/** */
class SixFrame : public FrameCount {
	public:
		/** */
		SixFrame() noexcept:
			FrameCount(5)
			{}
	};

/** */
class SevenFrame : public FrameCount {
	public:
		/** */
		SevenFrame() noexcept:
			FrameCount(6)
			{}
	};

/** */
class EightFrame : public FrameCount {
	public:
		/** */
		EightFrame() noexcept:
			FrameCount(7)
			{}
	};

}
}
}
}
}

#endif
