/*
   Copyright (C) 2007 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "desc.h"

using namespace Oscl::Usb::UsbHostSlave::TD::Isochronous;

Descriptor::
Descriptor(	TransferApi&		tapi,
			FrameCount			frameCount,
			uint16_t			startFrame,
			void*				buffer,
			unsigned			size,
			unsigned			offset0,
			unsigned			offset1,
			unsigned			offset2,
			unsigned			offset3,
			unsigned			offset4,
			unsigned			offset5,
			unsigned			offset6,
			unsigned			offset7
			) noexcept:
		TD::Descriptor(tapi)
		{
//	unsigned char*	p	= (unsigned char*)buffer;
	_frameCountValue	= frameCount.getValue();
	_startFrame			= startFrame;
	}

