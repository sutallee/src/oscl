/*
   Copyright (C) 2007 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_usb_usbhostslave_bw_allocatorh_
#define _oscl_drv_usb_usbhostslave_bw_allocatorh_
#include "bwnode.h"

/** */
namespace Oscl {
/** */
namespace Usb {
/** */
namespace UsbHostSlave {
/** */
namespace BW {

/** */
template <unsigned size>
union Mem {
	/** */
	void*			__qitemlink;
	/** */
	unsigned long	l;
	/** */
	unsigned char	obj[size];
	};

/** */
typedef Mem<sizeof(BwNode)>		BwNodeMem;

/** */
class Allocator {
	public:
		/** Shut-up GCC. */
		virtual ~Allocator() {}

		/** */
		virtual bool		bwAvailable(	unsigned	pollRate,
											unsigned	nBitTimes
											) noexcept=0;
		/** */
		virtual BwNodeMem*	allocBwNodeMem() noexcept=0;
		/** */
		virtual void		free(BwNodeMem* mem) noexcept=0;

		/** */
		virtual Oscl::Usb::Alloc::
		BW::Interrupt::BwRec*	alloc(	Oscl::Usb::UsbHostSlave::ED::
										Descriptor&				ed
										) noexcept=0;
		/** */
		virtual void			free(	Oscl::Usb::Alloc::
										BW::Interrupt::BwRec& bwRec
										) noexcept=0;
		/** */
		virtual void	free(BwAlloc* mem) noexcept=0;

		/** */
		virtual BwAlloc*	find(	Oscl::Usb::Alloc::
									BW::Interrupt::BwRec&	bwRec
									) noexcept=0;
	};

}
}
}
}

#endif
