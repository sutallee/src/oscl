/*
   Copyright (C) 2007 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_usb_usbhostslave_bw_sfnodeh_
#define _oscl_drv_usb_usbhostslave_bw_sfnodeh_
#include "edptr.h"

/** */
namespace Oscl {
/** */
namespace Usb {
/** */
namespace UsbHostSlave {
/** */
namespace BW {

/** */
class SfSr;
/** */
class BwNode;
/** */
class BwAlloc;

class SfNode {
	public:
		/** */
		SfNode*	_nextSfNode;
		/** */
		SfSr&	_sfsr;
	public:
		/** */
		SfNode(SfSr& sfsr) noexcept;
		/** */
		unsigned	bitsInUse() noexcept;
		/** */
		void		addNode(	EdPtr&	prev,
								SfSr&	sfsr,
								BwNode&	node
								) noexcept;

		/** */
		BwNode&		freeNode(	EdPtr&		prev,
								SfSr&		sfsr,
								BwAlloc&	bw
								) noexcept;

		/** */
		BwNode*		nextBwNode() noexcept;
	};

}
}
}
}

#endif
