/*
   Copyright (C) 2007 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_usb_usbhostslave_bw_bwalloch_
#define _oscl_drv_usb_usbhostslave_bw_bwalloch_
#include "oscl/driver/usb/usbhostslave/ed/desc.h"
#include "oscl/driver/usb/alloc/bw/interrupt/bwrec.h"
#include "oscl/endian/type.h"
#include "oscl/queue/queueitem.h"

/** */
namespace Oscl {
/** */
namespace Usb {
/** */
namespace UsbHostSlave {
/** */
namespace BW {

/** */
class Group;

/** */
struct BwAlloc :	public Oscl::Usb::Alloc::BW::Interrupt::BwRec,
					public Oscl::QueueItem
					{
	public:
		/** */
		Group&						_group;
		/** */
		Oscl::Usb::UsbHostSlave::
		ED::Descriptor&				_ed;
		/** */
		const unsigned				_nBitTimes;
	public:
		/** */
		BwAlloc(	Group&					group,
					Oscl::Usb::UsbHostSlave::ED::
					Descriptor&				ed,
					unsigned				nBitTimes
					) noexcept;
	};

}
}
}
}

#endif
