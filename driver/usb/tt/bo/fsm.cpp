/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "fsm.h"
#include "oscl/error/fatal.h"

using namespace Oscl::Usb::TT::BO;

static const StateVar::Closed						closed;
static const StateVar::Idle							idle;
static const StateVar::Fail							fail;
static const StateVar::Resetting					resetting;
static const StateVar::GettingMaxLun				gettingMaxLun;
static const StateVar::ReadCB						readCB;
static const StateVar::ReadData						readData;
static const StateVar::ReadStatus					readStatus;
static const StateVar::ReadClear					readClear;
static const StateVar::ReadStatus2					readStatus2;
static const StateVar::CommandCB					commandCB;
static const StateVar::CommandStatus				commandStatus;
static const StateVar::CommandClear					commandClear;
static const StateVar::CommandStatus2				commandStatus2;
static const StateVar::WriteCB						writeCB;
static const StateVar::WriteData					writeData;
static const StateVar::WriteStatus					writeStatus;
static const StateVar::WriteClear					writeClear;
static const StateVar::WriteStatus2					writeStatus2;
static const StateVar::Canceling					canceling;

// StateVar

StateVar::StateVar(ContextApi& context) noexcept:
		_context(context),
		_state(&closed)
		{
	for(unsigned i=0;i<nTraceRecs;++i){
		_freeTraceRecs.put(&_traceRec[i]);
		}
	}

void	StateVar::open() noexcept{
	_state->open(_context,*this);
	}

void	StateVar::close() noexcept{
	_state->close(_context,*this);
	}

void	StateVar::cancelResp() noexcept{
	_state->cancelResp(_context,*this);
	}

void	StateVar::controlNoDataResp() noexcept{
	_state->controlNoDataResp(_context,*this);
	}

void	StateVar::controlReadResp() noexcept{
	_state->controlReadResp(_context,*this);
	}

void	StateVar::controlWriteResp() noexcept{
	_state->controlWriteResp(_context,*this);
	}

void	StateVar::writeResp() noexcept{
	_state->writeResp(_context,*this);
	}

void	StateVar::readResp() noexcept{
	_state->readResp(_context,*this);
	}

void	StateVar::commandReq() noexcept{
	_state->commandReq(_context,*this);
	}

void	StateVar::writeReq() noexcept{
	_state->writeReq(_context,*this);
	}

void	StateVar::readReq() noexcept{
	_state->readReq(_context,*this);
	}

void	StateVar::resetReq() noexcept{
	_state->resetReq(_context,*this);
	}

void	StateVar::getMaxLunReq() noexcept{
	_state->getMaxLunReq(_context,*this);
	}

void	StateVar::cancelReq() noexcept{
	_state->cancelReq(_context,*this);
	}

// global actions
void	StateVar::protocolError(const char* state) noexcept{
	ErrorFatal::logAndExit(state);
	}

// state change operations
void	StateVar::changeToClosed() noexcept{
	trace(closed);
	_state	= &closed;
	}

void	StateVar::changeToIdle() noexcept{
	trace(idle);
	_state	= &idle;
	executeDeferredReq();
	}

void	StateVar::changeToFail() noexcept{
	trace(fail);
	_state	= &fail;
	flushDeferQueue();
	}

void	StateVar::changeToResetting() noexcept{
	trace(resetting);
	_state	= &resetting;
	}

void	StateVar::changeToGettingMaxLun() noexcept{
	trace(gettingMaxLun);
	_state	= &gettingMaxLun;
	}

void	StateVar::changeToReadCB() noexcept{
	trace(readCB);
	_state	= &readCB;
	}

void	StateVar::changeToReadData() noexcept{
	trace(readData);
	_state	= &readData;
	}

void	StateVar::changeToReadStatus() noexcept{
	trace(readStatus);
	_state	= &readStatus;
	}

void	StateVar::changeToReadClear() noexcept{
	trace(readClear);
	_state	= &readClear;
	}

void	StateVar::changeToReadStatus2() noexcept{
	trace(readStatus2);
	_state	= &readStatus2;
	}

void	StateVar::changeToWriteCB() noexcept{
	trace(writeCB);
	_state	= &writeCB;
	}

void	StateVar::changeToCommandCB() noexcept{
	trace(commandCB);
	_state	= &commandCB;
	}

void	StateVar::changeToWriteData() noexcept{
	trace(writeData);
	_state	= &writeData;
	}

void	StateVar::changeToWriteStatus() noexcept{
	trace(writeStatus);
	_state	= &writeStatus;
	}

void	StateVar::changeToWriteClear() noexcept{
	trace(writeClear);
	_state	= &writeClear;
	}

void	StateVar::changeToWriteStatus2() noexcept{
	trace(writeStatus2);
	_state	= &writeStatus2;
	}

void	StateVar::changeToCommandStatus() noexcept{
	trace(commandStatus);
	_state	= &commandStatus;
	}

void	StateVar::changeToCommandClear() noexcept{
	trace(commandClear);
	_state	= &commandClear;
	}

void	StateVar::changeToCommandStatus2() noexcept{
	trace(commandStatus2);
	_state	= &commandStatus2;
	}

void	StateVar::changeToCanceling() noexcept{
	trace(canceling);
	_state	= &canceling;
	}

static unsigned long	_nTraces;

void	StateVar::trace(const State& state) noexcept{
	TraceRec*	rec;
	++_nTraces;
	rec	= _freeTraceRecs.get();
	if(!rec){
		rec	= _trace.get();
		}
	rec->_state	= &state;
	_trace.put(rec);
	}

void	StateVar::executeDeferredReq() noexcept{
	_context.executeDeferredReq();
	}

void	StateVar::flushDeferQueue() noexcept{
	_context.flushDeferQueue();
	}
