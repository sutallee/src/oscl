/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "fsm.h"
#include "oscl/mt/thread.h"
using namespace Oscl::Usb::TT::BO;

void	StateVar::State::
open(	ContextApi&	context,
		StateVar&	sv
		) const noexcept{
	sv.protocolError(stateName());
	}

void	StateVar::State::
close(	ContextApi&	context,
		StateVar&	sv
		) const noexcept{
	sv.protocolError(stateName());
	}

void	StateVar::State::
cancelResp(	ContextApi&	context,
			StateVar&	sv
			) const noexcept{
	sv.protocolError(stateName());
	}

void	StateVar::State::
controlNoDataResp(	ContextApi&	context,
					StateVar&	sv
					) const noexcept{
	sv.protocolError(stateName());
	}

void	StateVar::State::
controlReadResp(	ContextApi&	context,
					StateVar&	sv
					) const noexcept{
	sv.protocolError(stateName());
	}

void	StateVar::State::
controlWriteResp(	ContextApi&	context,
					StateVar&	sv
					) const noexcept{
	sv.protocolError(stateName());
	}

void	StateVar::State::
writeResp(	ContextApi&	context,
			StateVar&	sv
			) const noexcept{
	sv.protocolError(stateName());
	}

void	StateVar::State::
readResp(	ContextApi&	context,
			StateVar&	sv
			) const noexcept{
	sv.protocolError(stateName());
	}

void	StateVar::State::
writeReq(	ContextApi&	context,
			StateVar&	sv
			) const noexcept{
	sv.protocolError(stateName());
	}

void	StateVar::State::
commandReq(	ContextApi&	context,
			StateVar&	sv
			) const noexcept{
	sv.protocolError(stateName());
	}

void	StateVar::State::
readReq(	ContextApi&	context,
			StateVar&	sv
			) const noexcept{
	sv.protocolError(stateName());
	}

void	StateVar::State::
resetReq(	ContextApi&	context,
			StateVar&	sv
			) const noexcept{
	sv.protocolError(stateName());
	}

void	StateVar::State::
getMaxLunReq(	ContextApi&	context,
				StateVar&	sv
				) const noexcept{
	sv.protocolError(stateName());
	}

void	StateVar::State::
cancelReq(	ContextApi&	context,
			StateVar&	sv
			) const noexcept{
	sv.protocolError(stateName());
	}

///////////////////////////////////

void	StateVar::Closed::
open(	ContextApi&	context,
		StateVar&	sv
		) const noexcept{
	context.returnOpenReq();
	sv.changeToIdle();
	}

///////////////////////////////////

void	StateVar::DeferReq::
writeReq(	ContextApi&	context,
			StateVar&	sv
			) const noexcept{
	context.deferReq();
	}

void	StateVar::DeferReq::
commandReq(	ContextApi&	context,
			StateVar&	sv
			) const noexcept{
	context.deferReq();
	}

void	StateVar::DeferReq::
readReq(	ContextApi&	context,
			StateVar&	sv
			) const noexcept{
	context.deferReq();
	}

void	StateVar::DeferReq::
resetReq(	ContextApi&	context,
			StateVar&	sv
			) const noexcept{
	context.deferReq();
	}

void	StateVar::DeferReq::
getMaxLunReq(	ContextApi&	context,
				StateVar&	sv
				) const noexcept{
	context.deferReq();
	}


///////////////////////////////////
void	StateVar::Idle::
close(	ContextApi&	context,
		StateVar&	sv
		) const noexcept{
	context.returnCloseReq();
	sv.changeToClosed();
	}

void	StateVar::Idle::
writeReq(	ContextApi&	context,
			StateVar&	sv
			) const noexcept{
	context.startWriteCB();
	sv.changeToWriteCB();
	}

void	StateVar::Idle::
commandReq(	ContextApi&	context,
			StateVar&	sv
			) const noexcept{
	context.startCommandCB();
	sv.changeToCommandCB();
	}

void	StateVar::Idle::
readReq(	ContextApi&	context,
			StateVar&	sv
			) const noexcept{
	context.startReadCB();
	sv.changeToReadCB();
	}

void	StateVar::Idle::
resetReq(	ContextApi&	context,
			StateVar&	sv
			) const noexcept{
	context.startReset();
	sv.changeToResetting();
	}

void	StateVar::Idle::
getMaxLunReq(	ContextApi&	context,
				StateVar&	sv
				) const noexcept{
	context.startGetMaxLun();
	sv.changeToGettingMaxLun();
	}

void	StateVar::Idle::
cancelReq(	ContextApi&	context,
			StateVar&	sv
			) const noexcept{
	context.returnCancelReq();
	}

///////////////////////////////////
void	StateVar::Fail::
close(	ContextApi&	context,
		StateVar&	sv
		) const noexcept{
	context.returnCloseReq();
	sv.changeToClosed();
	}

void	StateVar::Fail::
writeReq(	ContextApi&	context,
			StateVar&	sv
			) const noexcept{
	context.failWriteReq();
	context.returnWriteReq();
	}

void	StateVar::Fail::
commandReq(	ContextApi&	context,
			StateVar&	sv
			) const noexcept{
	context.failCommandReq();
	context.returnCommandReq();
	}

void	StateVar::Fail::
readReq(	ContextApi&	context,
			StateVar&	sv
			) const noexcept{
	context.failReadReq();
	context.returnReadReq();
	}

void	StateVar::Fail::
resetReq(	ContextApi&	context,
			StateVar&	sv
			) const noexcept{
	context.failResetReq();
	context.returnResetReq();
	}

void	StateVar::Fail::
getMaxLunReq(	ContextApi&	context,
				StateVar&	sv
				) const noexcept{
	context.failGetMaxLunReq();
	context.returnGetMaxLunReq();
	}

///////////////////////////////////
void	StateVar::Resetting::
controlNoDataResp(	ContextApi&	context,
					StateVar&	sv
					) const noexcept{
	context.finishResetReq();
	context.returnResetReq();
	sv.changeToIdle();
	context.executeDeferredReq();
	}

void	StateVar::Resetting::
cancelReq(	ContextApi&	context,
			StateVar&	sv
			) const noexcept{
	context.cancelResetReq();
	sv.changeToCanceling();
	}

///////////////////////////////////

void	StateVar::GettingMaxLun::
controlReadResp(	ContextApi&	context,
					StateVar&	sv
					) const noexcept{
	context.finishGetMaxLunReq();
	context.returnGetMaxLunReq();
	sv.changeToIdle();
	context.executeDeferredReq();
	}

void	StateVar::GettingMaxLun::
cancelReq(	ContextApi&	context,
			StateVar&	sv
			) const noexcept{
	context.cancelGetMaxLunReq();
	sv.changeToCanceling();
	}

///////////////////////////////////

void	StateVar::WriteCB::
writeResp(	ContextApi&	context,
			StateVar&	sv
			) const noexcept{
	if(context.writeFailed()){
		context.failWriteReq();
		context.returnWriteReq();
		sv.changeToFail();
		return;
		}
	context.startWriteData();
	sv.changeToWriteData();
	}

void	StateVar::WriteCB::
cancelReq(	ContextApi&	context,
			StateVar&	sv
			) const noexcept{
	context.cancelWriteReq();
	sv.changeToCanceling();
	}

///////////////////////////////////

void	StateVar::CommandCB::
writeResp(	ContextApi&	context,
			StateVar&	sv
			) const noexcept{
	if(context.writeFailed()){
		context.failWriteReq();
		context.returnWriteReq();
		sv.changeToFail();
		return;
		}
	context.startStatus();
	sv.changeToCommandStatus();
	}

void	StateVar::CommandCB::
cancelReq(	ContextApi&	context,
			StateVar&	sv
			) const noexcept{
	context.cancelCommandReq();
	sv.changeToCanceling();
	}


///////////////////////////////////

void	StateVar::WriteData::
writeResp(	ContextApi&	context,
			StateVar&	sv
			) const noexcept{
	if(context.writeFailed()){
		context.failWriteReq();
		context.returnWriteReq();
		sv.changeToFail();
		return;
		}
	context.startStatus();
	sv.changeToWriteStatus();
	}

void	StateVar::WriteData::
cancelReq(	ContextApi&	context,
			StateVar&	sv
			) const noexcept{
	context.cancelWriteReq();
	sv.changeToCanceling();
	}

///////////////////////////////////

void	StateVar::WriteStatus::
readResp(	ContextApi&	context,
			StateVar&	sv
			) const noexcept{
	bool	fail	= context.statusFailed();
	if(fail){
		if(context.statusRetryFailureType()){
			context.startBulkInClearHalt();
			sv.changeToWriteClear();
			return;
			}
		context.failWriteReq();
		context.returnWriteReq();
		sv.changeToFail();
		return;
		}
	if(!context.cswIsValid()){
		context.failWriteReq();
		context.returnWriteReq();
		sv.changeToFail();
		return;
		}
	context.finishWriteReq();
	context.returnWriteReq();
	sv.changeToIdle();
	context.executeDeferredReq();
	}

void	StateVar::WriteStatus::
cancelReq(	ContextApi&	context,
			StateVar&	sv
			) const noexcept{
	context.cancelWriteReq();
	sv.changeToCanceling();
	}

///////////////////////////////////

void	StateVar::WriteClear::
controlNoDataResp(	ContextApi&	context,
					StateVar&	sv
					) const noexcept{
	context.startStatus();
	sv.changeToWriteStatus2();
	}

void	StateVar::WriteClear::
cancelReq(	ContextApi&	context,
			StateVar&	sv
			) const noexcept{
	context.cancelWriteReq();
	sv.changeToCanceling();
	}

///////////////////////////////////

void	StateVar::WriteStatus2::
readResp(	ContextApi&	context,
			StateVar&	sv
			) const noexcept{
	if(context.statusFailed() || !context.cswIsValid()){
		context.failWriteReq();
		context.returnWriteReq();
		sv.changeToFail();
		return;
		}
	context.finishWriteReq();
	context.returnWriteReq();
	sv.changeToIdle();
	context.executeDeferredReq();
	}

void	StateVar::WriteStatus2::
cancelReq(	ContextApi&	context,
			StateVar&	sv
			) const noexcept{
	context.cancelWriteReq();
	sv.changeToCanceling();
	}

///////////////////////////////////

void	StateVar::CommandStatus::
readResp(	ContextApi&	context,
			StateVar&	sv
			) const noexcept{
	if(context.statusFailed()){
		if(context.statusRetryFailureType()){
			context.startBulkInClearHalt();
			sv.changeToCommandClear();
			return;
			}
		context.startStatus();
		sv.changeToCommandStatus2();
		return;
		}
	if(!context.cswIsValid()){
		context.failCommandReq();
		context.returnCommandReq();
		sv.changeToFail();
		return;
		}
	context.finishCommandReq();
	context.returnCommandReq();
	sv.changeToIdle();
	context.executeDeferredReq();
	}

void	StateVar::CommandStatus::
cancelReq(	ContextApi&	context,
			StateVar&	sv
			) const noexcept{
	context.cancelCommandReq();
	sv.changeToCanceling();
	}

///////////////////////////////////

void	StateVar::CommandClear::
controlNoDataResp(	ContextApi&	context,
					StateVar&	sv
					) const noexcept{
	context.startStatus();
	sv.changeToCommandStatus2();
	}

void	StateVar::CommandClear::
cancelReq(	ContextApi&	context,
			StateVar&	sv
			) const noexcept{
	context.cancelCommandReq();
	sv.changeToCanceling();
	}

///////////////////////////////////

void	StateVar::CommandStatus2::
readResp(	ContextApi&	context,
			StateVar&	sv
			) const noexcept{
	if(context.statusFailed() || !context.cswIsValid()){
		context.failCommandReq();
		context.returnCommandReq();
		sv.changeToFail();
		return;
		}
	context.finishCommandReq();
	context.returnCommandReq();
	sv.changeToIdle();
	context.executeDeferredReq();
	}

void	StateVar::CommandStatus2::
cancelReq(	ContextApi&	context,
			StateVar&	sv
			) const noexcept{
	context.cancelCommandReq();
	sv.changeToCanceling();
	}

///////////////////////////////////

void	StateVar::ReadCB::
writeResp(	ContextApi&	context,
			StateVar&	sv
			) const noexcept{
	if(context.writeFailed()){
		context.failReadReq();
		context.returnReadReq();
		sv.changeToFail();
		return;
		}
	context.startReadData();
	sv.changeToReadData();
	}

void	StateVar::ReadCB::
cancelReq(	ContextApi&	context,
			StateVar&	sv
			) const noexcept{
	context.cancelWriteReq();
	sv.changeToCanceling();
	}

///////////////////////////////////

void	StateVar::ReadData::
readResp(	ContextApi&	context,
			StateVar&	sv
			) const noexcept{
	if(context.readFailed()){
		context.failReadReq();
		context.returnReadReq();
		sv.changeToFail();
		return;
		}
	context.startStatus();
	sv.changeToReadStatus();
	}

void	StateVar::ReadData::
cancelReq(	ContextApi&	context,
			StateVar&	sv
			) const noexcept{
	context.cancelReadReq();
	sv.changeToCanceling();
	}

///////////////////////////////////

void	StateVar::ReadStatus::
readResp(	ContextApi&	context,
			StateVar&	sv
			) const noexcept{
	bool	fail	= context.statusFailed();
	if(fail){
		if(context.statusRetryFailureType()){
			context.startBulkInClearHalt();
			sv.changeToReadClear();
			return;
			}
		}
	if(fail || !context.cswIsValid()){
		context.failReadReq();
		context.returnReadReq();
		sv.changeToFail();
		return;
		}
	context.finishReadReq();
	context.returnReadReq();
	sv.changeToIdle();
	context.executeDeferredReq();
	}

void	StateVar::ReadStatus::
cancelReq(	ContextApi&	context,
			StateVar&	sv
			) const noexcept{
	context.cancelReadReq();
	sv.changeToCanceling();
	}

///////////////////////////////////

void	StateVar::ReadClear::
controlNoDataResp(	ContextApi&	context,
					StateVar&	sv
					) const noexcept{
	context.startStatus();
	sv.changeToReadStatus2();
	}

void	StateVar::ReadClear::
cancelReq(	ContextApi&	context,
			StateVar&	sv
			) const noexcept{
	context.cancelClearFeatureReq();
	sv.changeToCanceling();
	}

///////////////////////////////////

void	StateVar::ReadStatus2::
readResp(	ContextApi&	context,
			StateVar&	sv
			) const noexcept{
	if(context.statusFailed() || !context.cswIsValid()){
		context.failReadReq();
		context.returnReadReq();
		sv.changeToFail();
		return;
		}
	context.finishReadReq();
	context.returnReadReq();
	sv.changeToIdle();
	context.executeDeferredReq();
	}

void	StateVar::ReadStatus2::
cancelReq(	ContextApi&	context,
			StateVar&	sv
			) const noexcept{
	context.cancelReadReq();
	sv.changeToCanceling();
	}

///////////////////////////////////

void	StateVar::Canceling::
controlNoDataResp(	ContextApi&	context,
					StateVar&	sv
					) const noexcept{
	context.returnCanceledReq();
	}

void	StateVar::Canceling::
controlReadResp(	ContextApi&	context,
					StateVar&	sv
					) const noexcept{
	context.returnCanceledReq();
	}

void	StateVar::Canceling::
controlWriteResp(	ContextApi&	context,
					StateVar&	sv
					) const noexcept{
	context.returnCanceledReq();
	}

void	StateVar::Canceling::
writeResp(	ContextApi&	context,
			StateVar&	sv
			) const noexcept{
	context.returnCanceledReq();
	}

void	StateVar::Canceling::
readResp(	ContextApi&	context,
			StateVar&	sv
			) const noexcept{
	context.returnCanceledReq();
	}

void	StateVar::Canceling::
cancelResp(	ContextApi&	context,
			StateVar&	sv
			) const noexcept{
	context.flushDeferQueue();
	context.returnCancelReq();
	sv.changeToIdle();
	}
