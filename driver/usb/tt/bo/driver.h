/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_usb_tt_bo_driverh_
#define _oscl_drv_usb_tt_bo_driverh_
#include "oscl/memory/block.h"
#include "oscl/queue/queue.h"
#include "oscl/mt/itc/srv/close.h"
#include "oscl/driver/usb/pipe/message/pipeapi.h"
#include "oscl/driver/usb/pipe/bulk/in/respmem.h"
#include "oscl/driver/usb/pipe/bulk/out/respmem.h"
#include "oscl/driver/usb/pipe/message/respmem.h"
#include "oscl/driver/usb/setup/mem.h"
#include "oscl/tt/itc/sync.h"
#include "fsm.h"
#include "oscl/driver/usb/pipe/handler.h"

/** */
namespace Oscl {
/** */
namespace Usb {
/** */
namespace TT {
/** */
namespace BO {

/** This implementation of the Oscl::TT::ITC::Req::Api serializes
	all requests and adapts them as specified for USB Transparent
	Transport using the Bulk Only method.
 */
class Driver :	public Oscl::Mt::Itc::Srv::CloseSync,
				private Oscl::Mt::Itc::Srv::Close::Req::Api,
				private Oscl::Usb::Pipe::Bulk::OUT::Resp::Api,
				private Oscl::Usb::Pipe::Bulk::IN::Resp::Api,
				private Oscl::Usb::Pipe::Message::Resp::Api,
				private Oscl::TT::ITC::Req::Api,
				private Oscl::Usb::TT::BO::StateVar::ContextApi
				{
	private:
		/** */
		class GetMaxLunResult : public Oscl::Usb::Pipe::Status::Handler {
			public:
				/** */
				bool	_failed;
			public:
				/** */
				GetMaxLunResult() noexcept;
			private:
				/** */
				void	pipeClosed() noexcept;
				/** */
				void	canceled() noexcept;
				/** */
				void	crc() noexcept;
				/** */
				void	bitStuffing() noexcept;
				/** */
				void	dataToggleMismatch() noexcept;
				/** */
				void	stall() noexcept;
				/** */
				void	deviceNotResponding() noexcept;
				/** */
				void	pidCheckFailure() noexcept;
				/** */
				void	unexpectedPID() noexcept;
				/** */
				void	dataOverrun() noexcept;
				/** */
				void	dataUnderrun() noexcept;
				/** */
				void	bufferOverrun() noexcept;
				/** */
				void	bufferUnderrun() noexcept;
			};
		/** */
		class BulkInStatusResult : public Oscl::Usb::Pipe::Status::Handler {
			public:
				/** */
				bool	_failed;
			public:
				/** */
				BulkInStatusResult() noexcept;
			private:
				/** */
				void	pipeClosed() noexcept;
				/** */
				void	canceled() noexcept;
				/** */
				void	crc() noexcept;
				/** */
				void	bitStuffing() noexcept;
				/** */
				void	dataToggleMismatch() noexcept;
				/** */
				void	stall() noexcept;
				/** */
				void	deviceNotResponding() noexcept;
				/** */
				void	pidCheckFailure() noexcept;
				/** */
				void	unexpectedPID() noexcept;
				/** */
				void	dataOverrun() noexcept;
				/** */
				void	dataUnderrun() noexcept;
				/** */
				void	bufferOverrun() noexcept;
				/** */
				void	bufferUnderrun() noexcept;
			};
		/** */
		class BulkInReadResult : public Oscl::Usb::Pipe::Status::Handler {
			public:
				/** */
				bool	_failed;
			public:
				/** */
				BulkInReadResult() noexcept;
			private:
				/** */
				void	pipeClosed() noexcept;
				/** */
				void	canceled() noexcept;
				/** */
				void	crc() noexcept;
				/** */
				void	bitStuffing() noexcept;
				/** */
				void	dataToggleMismatch() noexcept;
				/** */
				void	stall() noexcept;
				/** */
				void	deviceNotResponding() noexcept;
				/** */
				void	pidCheckFailure() noexcept;
				/** */
				void	unexpectedPID() noexcept;
				/** */
				void	dataOverrun() noexcept;
				/** */
				void	dataUnderrun() noexcept;
				/** */
				void	bufferOverrun() noexcept;
				/** */
				void	bufferUnderrun() noexcept;
			};
		/** Returns _retry == true if the result is one
			that should be retried according to the USB
			Mass Storage spec.
		 */
		class BulkInStatusRetry : public Oscl::Usb::Pipe::Status::Handler {
			public:
				/** */
				bool	_retry;
			public:
				/** */
				BulkInStatusRetry() noexcept;
			private:
				/** */
				void	pipeClosed() noexcept;
				/** */
				void	canceled() noexcept;
				/** */
				void	crc() noexcept;
				/** */
				void	bitStuffing() noexcept;
				/** */
				void	dataToggleMismatch() noexcept;
				/** */
				void	stall() noexcept;
				/** */
				void	deviceNotResponding() noexcept;
				/** */
				void	pidCheckFailure() noexcept;
				/** */
				void	unexpectedPID() noexcept;
				/** */
				void	dataOverrun() noexcept;
				/** */
				void	dataUnderrun() noexcept;
				/** */
				void	bufferOverrun() noexcept;
				/** */
				void	bufferUnderrun() noexcept;
			};
	public:
		/** */
		struct CBW {
			/** */
			unsigned char	dCBWSignature[4];
			/** */
			unsigned char	dCBWTag[4];
			/** */
			unsigned char	dCBWDataTransferLength[4];
			/** */
			unsigned char	bCBWFlags;
			/** */
			unsigned char	bCBWLUN;
			/** */
			unsigned char	bCBWCBLength;
			/** */
			unsigned char	CBWCB[16];
			};
		/** */
		struct CSW {
			/** */
			unsigned char	dCSWSignature[4];
			/** */
			unsigned char	dCSWTag[4];
			/** */
			unsigned char	dCSWDataResidue[4];
			/** */
			unsigned char	dCSWStatus;
			};
		/** */
		struct SetupMem {
			/** */
			Oscl::Usb::Setup::Mem	controlSetup;
			};
		/** */
		struct ControlPacketMem {
			/** */
			Oscl::Memory::AlignedBlock<16>	controlBytes;
			};
		/** */
		union ContextPacketMem {
			/** */
			ControlPacketMem	control;
			};
		/** */
		enum{maxBulkRxPacketBytes=2*0x40};
		/** */
		struct BulkRxPacketMem {
			/** */
			Oscl::Memory::AlignedBlock<maxBulkRxPacketBytes>	mem;
			};
		/** */
		enum{maxBulkTxPacketBytes=1024};
		/** */
		union BulkTxPacketMem {
			/** */
			void*		__qitemlink;
			/** */
			Oscl::Memory::AlignedBlock<maxBulkTxPacketBytes>	mem;
			};
		/** */
		enum{nRxBuffers=2};
		/** */
		union PacketMem {
			/** */
			ControlPacketMem				control;
			/** */
			Oscl::Memory::AlignedBlock<31>	cbw;
			/** */
			Oscl::Memory::AlignedBlock<13>	csw;
			/** */
			Oscl::Memory::AlignedBlock<1>	getMaxLun;
			};
		/** */
		union CancelMem {
			/** */
			Oscl::Usb::Pipe::Bulk::
			IN::Resp::CancelMem					bulkInPipe;
			/** */
			Oscl::Usb::Pipe::Bulk::
			OUT::Resp::CancelMem				bulkOutPipe;
			/** */
			Oscl::Usb::Pipe::Message::
			Resp::CancelMem						messagePipe;
			};

	private:
		/** Message resources for bulk I/O. A union is
			used assuming that this implementation only
			has a single bulk transaction in progress
			at a time. Thats the plan since this is to
			be a simple implementation.
		 */
		union {
			/** */
			Oscl::Usb::Pipe::
			Bulk::IN::Resp::ReadMem				bulkIn;
			/** */
			Oscl::Usb::Pipe::
			Bulk::OUT::Resp::WriteMem			bulkOut;
			/** */
			Oscl::Usb::Pipe::
			Message::Resp::NoDataMem			controlNoData;
			/** */
			Oscl::Usb::Pipe::
			Message::Resp::ReadMem				controlRead;
			/** */
			Oscl::Usb::Pipe::
			Message::Resp::WriteMem				controlWrite;
			} _pipeMem;

		/** TT requests that are queued/serialized
			for execution while the pending bulk
			transaction is executing.
		 */
		Oscl::Queue<Oscl::Mt::Itc::SrvMsg>	_pendingReqs;

		/** Memory used to cancel USB bulk and control
			requests.
		 */
		CancelMem							_cancelMem;

		/** */
		Oscl::Usb::Pipe::
		Message::PipeApi&					_controlPipe;

		/** */
		const uint8_t						_bInterfaceNumber;

		/** */
		const uint8_t						_bulkInEndpointID;

		/** */
		const uint8_t						_bulkOutEndpointID;

		/** */
		Oscl::Usb::Pipe::Bulk::
		IN::Req::Api::SAP&					_bulkInPipe;

		/** */
		Oscl::Usb::Pipe::Bulk::
		OUT::Req::Api::SAP&					_bulkOutPipe;

		/** */
		SetupMem&							_setupMem;

		/** */
		PacketMem&							_packetMem;

		/** */
		Oscl::Mt::Itc::PostMsgApi&			_myPapi;

		/** */
		Oscl::TT::ITC::Sync					_sync;

		/** */
		unsigned char						_maxBulkInPacketSize;

		/** */
		unsigned char						_maxBulkOutPacketSize;

		/** */
		Oscl::Mt::Itc::Srv::Open::
		Req::Api::OpenReq*					_openReq;

		/** */
		Oscl::Mt::Itc::Srv::Close::
		Req::Api::CloseReq*					_closeReq;

		/** */
		Oscl::Usb::Pipe::Bulk::
		OUT::Resp::Api::WriteResp*			_currentWrite;

		/** */
		Oscl::Usb::Pipe::Bulk::
		IN::Resp::Api::ReadResp*			_currentRead;

		/** */
		Oscl::TT::ITC::Req::Api::CancelReq*	_currentCancelReq;

		/** */
		Oscl::Usb::TT::BO::StateVar			_state;

		/** */
		Oscl::TT::ITC::
		Req::Api::CancelReq*				_cancelReq;

		/** */
		union Requests {
			/** */
			Oscl::Mt::Itc::SrvMsg*	generic;
			/** */
			Oscl::TT::ITC::Req::Api::ResetReq*				reset;
			/** */
			Oscl::TT::ITC::Req::Api::GetMaxLunReq*			getMaxLun;
			/** */
			Oscl::TT::ITC::Req::Api::ReadReq*				read;
			/** */
			Oscl::TT::ITC::Req::Api::WriteReq*				write;
			/** */
			Oscl::TT::ITC::Req::Api::CommandReq*			command;
			};

		/** The "just received" request */
		Requests							_incomming;

		/** The currently executing request */
		Requests							_current;

		/** */
		union {
			/** */
			Oscl::Mt::Itc::SrvMsg*							generic;
			/** */
			Oscl::Usb::Pipe::Bulk::
			OUT::Resp::Api::WriteResp*						bulkWrite;
			/** */
			Oscl::Usb::Pipe::Bulk::
			OUT::Resp::Api::CancelResp*						bulkOutCancel;
			/** */
			Oscl::Usb::Pipe::Bulk::
			IN::Resp::Api::ReadResp*						bulkRead;
			/** */
			Oscl::Usb::Pipe::Bulk::
			IN::Resp::Api::CancelResp*						bulkInCancel;
			/** */
			Oscl::Usb::Pipe::Message::
			Resp::Api::CancelResp*							messageCancel;
			/** */
			Oscl::Usb::Pipe::Message::
			Resp::Api::NoDataResp*							messageNoData;
			/** */
			Oscl::Usb::Pipe::Message::
			Resp::Api::ReadResp*							messageRead;
			/** */
			Oscl::Usb::Pipe::Message::
			Resp::Api::WriteResp*							messageWrite;
			} _currentPipe;

		/** */
		union{
			uint8_t	array[4];
			uint32_t	word;
			} _currentTag;

		/** */
		const Oscl::Usb::Pipe::Status::Result*		_result;

	public:
		/** */
		Driver(	Oscl::Usb::Pipe::
				Message::PipeApi&				controlPipe,
				Oscl::Usb::Pipe::Bulk::
				IN::Req::Api::SAP&				bulkInPipe,
				Oscl::Usb::Pipe::Bulk::
				OUT::Req::Api::SAP&				bulkOutPipe,
				Oscl::Mt::Itc::PostMsgApi&		myPapi,
				SetupMem&						setupMem,
				PacketMem&						packetMem,
				unsigned char					maxBulkInPacketSize,
				unsigned char					maxBulkOutPacketSize,
				uint8_t						bInterfaceNumber,
				uint8_t						bulkInEndpointID,
				uint8_t						bulkOutEndpointID
				) noexcept;

		/** */
		Oscl::TT::Api&	getTTApi() noexcept;

		/** */
		Oscl::TT::ITC::Req::Api::SAP&	getTTSAP() noexcept;

	private:	// Oscl::Mt::Itc::Srv::Close::Req::Api
		/** */
		void	request(	Oscl::Mt::Itc::Srv::
							Open::Req::Api::OpenReq&	msg
							) noexcept;
		/** */
		void	request(	Oscl::Mt::Itc::Srv::
							Close::Req::Api::CloseReq&	msg
							) noexcept;

	public:	// Oscl::TT::ITC::Req::Api
		/** */
		void	request(ResetReq& msg) noexcept;
		/** */
		void	request(GetMaxLunReq& msg) noexcept;
		/** */
		void	request(CancelReq& msg) noexcept;
		/** */
		void	request(ReadReq& msg) noexcept;
		/** */
		void	request(WriteReq& msg) noexcept;
		/** */
		void	request(CommandReq& msg) noexcept;

	private:	// Oscl::Usb::Pipe::Bulk::OUT::Resp::Api
		/** */
		void	response(	Oscl::Usb::Pipe::Bulk::
							OUT::Resp::Api::WriteResp&		msg
							) noexcept;
		/** */
		void	response(	Oscl::Usb::Pipe::Bulk::
							OUT::Resp::Api::CancelResp&		msg
							) noexcept;

	private:	// Oscl::Usb::Pipe::Bulk::IN::Resp::Api
		/** */
		void	response(	Oscl::Usb::Pipe::Bulk::
							IN::Resp::Api::ReadResp&		msg
							) noexcept;
		/** */
		void	response(	Oscl::Usb::Pipe::Bulk::
							IN::Resp::Api::CancelResp&		msg
							) noexcept;

	public:	// Oscl::Usb::Pipe::Message::Resp::Api
		/** */
		void	response(	Oscl::Usb::Pipe::Message::
							Resp::Api::CancelResp&			msg
							) noexcept;
		/** */
		void	response(	Oscl::Usb::Pipe::Message::
							Resp::Api::NoDataResp&			msg
							) noexcept;
		/** */
		void	response(	Oscl::Usb::Pipe::Message::
							Resp::Api::ReadResp&			msg
							) noexcept;
		/** */
		void	response(	Oscl::Usb::Pipe::Message::
							Resp::Api::WriteResp&			msg
							) noexcept;

	private:	// Oscl::Usb::TT::BO::StateVar::ContextApi
		/** */
		void	returnOpenReq() noexcept;
		/** */
		void	returnCloseReq() noexcept;
		/** */
		void	executeDeferredReq() noexcept;
		/** */
		void	deferReq() noexcept;
		/** */
		void	flushDeferQueue() noexcept;
		/** */
		void	startReset() noexcept;
		/** */
		void	startGetMaxLun() noexcept;
		/** */
		void	startBulkInClearHalt() noexcept;
		/** */
		void	startCommandCB() noexcept;
		/** */
		void	startWriteCB() noexcept;
		/** */
		void	startWriteData() noexcept;
		/** */
		void	startReadCB() noexcept;
		/** */
		void	startReadData() noexcept;
		/** */
		void	startStatus() noexcept;
		/** */
		void	failReadReq() noexcept;
		/** */
		void	failWriteReq() noexcept;
		/** */
		void	failCommandReq() noexcept;
		/** */
		void	failResetReq() noexcept;
		/** */
		void	failGetMaxLunReq() noexcept;
		/** */
		void	finishWriteReq() noexcept;
		/** */
		void	finishCommandReq() noexcept;
		/** */
		void	finishReadReq() noexcept;
		/** */
		void	finishResetReq() noexcept;
		/** */
		void	finishGetMaxLunReq() noexcept;
		/** */
		void	returnResetReq() noexcept;
		/** */
		void	returnGetMaxLunReq() noexcept;
		/** */
		void	returnReadReq() noexcept;
		/** */
		void	returnWriteReq() noexcept;
		/** */
		void	returnCommandReq() noexcept;
		/** */
		void	returnCancelReq() noexcept;
		/** */
		void	returnCanceledReq() noexcept;
		/** */
		void	cancelClearFeatureReq() noexcept;
		/** */
		void	cancelResetReq() noexcept;
		/** */
		void	cancelGetMaxLunReq() noexcept;
		/** */
		void	cancelReadReq() noexcept;
		/** */
		void	cancelWriteReq() noexcept;
		/** */
		void	cancelCommandReq() noexcept;
		/** */
		bool	deferQueueEmpty() noexcept;
		/** */
		bool	writeFailed() noexcept;
		/** */
		bool	statusFailed() noexcept;
		/** */
		bool	readFailed() noexcept;
		/** */
		bool	cswIsValid() noexcept;
		/** */
		bool	statusRetryFailureType() noexcept;
	private:
		/** */
		const Oscl::TT::Status::Result*
		parseCswResult() noexcept;
		/** */
		const Oscl::TT::Status::Result*
		parseCSW(unsigned long& residue) noexcept;
		/** */
		void	cancelMessage(Oscl::Mt::Itc::SrvMsg& msg) noexcept;
		/** */
		void	cancelBulkIn(Oscl::Mt::Itc::SrvMsg& msg) noexcept;
		/** */
		void	cancelBulkOut(Oscl::Mt::Itc::SrvMsg& msg) noexcept;
	};

}
}
}
}


#endif
