/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_usb_tt_bo_fsmh_
#define _oscl_drv_usb_tt_bo_fsmh_
#include "oscl/queue/queue.h"
#include "oscl/queue/queueitem.h"

/** */
namespace Oscl {
/** */
namespace Usb {
/** */
namespace TT {
/** */
namespace BO {

/** */
class StateVar {
	public:
		/** */
		class ContextApi {
			public:	// request actions
				/** Make GCC happy */
				virtual ~ContextApi() {}
				/** */
				virtual void	returnOpenReq() noexcept=0;
				/** */
				virtual void	returnCloseReq() noexcept=0;
				/** */
				virtual void	executeDeferredReq() noexcept=0;
				/** */
				virtual void	deferReq() noexcept=0;
				/** */
				virtual void	flushDeferQueue() noexcept=0;
				/** */
				virtual void	startReset() noexcept=0;
				/** */
				virtual void	startGetMaxLun() noexcept=0;
				/** */
				virtual void	startBulkInClearHalt()=0;
				/** */
				virtual void	startCommandCB() noexcept=0;
				/** */
				virtual void	startWriteCB() noexcept=0;
				/** */
				virtual void	startWriteData() noexcept=0;
				/** */
				virtual void	startReadCB() noexcept=0;
				/** */
				virtual void	startReadData() noexcept=0;
				/** */
				virtual void	startStatus() noexcept=0;
				/** */
				virtual void	failReadReq() noexcept=0;
				/** */
				virtual void	failWriteReq() noexcept=0;
				/** */
				virtual void	failCommandReq() noexcept=0;
				/** */
				virtual void	failResetReq() noexcept=0;
				/** */
				virtual void	failGetMaxLunReq() noexcept=0;
				/** */
				virtual void	finishWriteReq() noexcept=0;
				/** */
				virtual void	finishCommandReq() noexcept=0;
				/** */
				virtual void	finishReadReq() noexcept=0;
				/** */
				virtual void	finishResetReq() noexcept=0;
				/** */
				virtual void	finishGetMaxLunReq() noexcept=0;
				/** */
				virtual void	returnResetReq() noexcept=0;
				/** */
				virtual void	returnGetMaxLunReq() noexcept=0;
				/** */
				virtual void	returnReadReq() noexcept=0;
				/** */
				virtual void	returnWriteReq() noexcept=0;
				/** */
				virtual void	returnCommandReq() noexcept=0;
				/** */
				virtual void	returnCancelReq() noexcept=0;
				/** */
				virtual void	returnCanceledReq() noexcept=0;
				/** */
				virtual void	cancelClearFeatureReq() noexcept=0;
				/** */
				virtual void	cancelResetReq() noexcept=0;
				/** */
				virtual void	cancelGetMaxLunReq() noexcept=0;
				/** */
				virtual void	cancelReadReq() noexcept=0;
				/** */
				virtual void	cancelWriteReq() noexcept=0;
				/** */
				virtual void	cancelCommandReq() noexcept=0;
				/** */
				virtual bool	deferQueueEmpty() noexcept=0;
				/** */
				virtual bool	writeFailed() noexcept=0;
				/** */
				virtual bool	readFailed() noexcept=0;
				/** */
				virtual bool	statusFailed() noexcept=0;
				/** */
				virtual bool	cswIsValid() noexcept=0;
				/** */
				virtual bool	statusRetryFailureType() noexcept=0;
			};
	private:
		/** */
		class State {
			public:	// Admin Events
				/** Make GCC happy */
				virtual ~State() {}

			public:	// Admin Events
				/** */
				virtual void	open(	ContextApi&	context,
										StateVar&	sv
										) const noexcept;
				/** */
				virtual void	close(	ContextApi&	context,
										StateVar&	sv
										) const noexcept;
				/** */
				virtual void	cancelResp(	ContextApi&	context,
											StateVar&	sv
											) const noexcept;
				/** */
				virtual void	controlNoDataResp(	ContextApi&	context,
													StateVar&	sv
													) const noexcept;
				/** */
				virtual void	controlReadResp(	ContextApi&	context,
													StateVar&	sv
													) const noexcept;
				/** */
				virtual void	controlWriteResp(	ContextApi&	context,
													StateVar&	sv
													) const noexcept;
				/** */
				virtual void	writeResp(	ContextApi&	context,
											StateVar&	sv
											) const noexcept;
				/** */
				virtual void	readResp(	ContextApi&	context,
											StateVar&	sv
											) const noexcept;
				/** */
				virtual void	readReq(	ContextApi&	context,
											StateVar&	sv
											) const noexcept;
				/** */
				virtual void	writeReq(	ContextApi&	context,
											StateVar&	sv
											) const noexcept;
				/** */
				virtual void	commandReq(	ContextApi&	context,
											StateVar&	sv
											) const noexcept;
				/** */
				virtual void	resetReq(	ContextApi&	context,
											StateVar&	sv
											) const noexcept;
				/** */
				virtual void	getMaxLunReq(	ContextApi&	context,
												StateVar&	sv
												) const noexcept;
				/** */
				virtual void	cancelReq(	ContextApi&	context,
											StateVar&	sv
											) const noexcept;
			public:
				/** */
				virtual const char*	stateName() const noexcept=0;
			};

	public:
		/** */
		class Closed : public State {
			private:
				/** */
				const char*	stateName() const noexcept
					{ return "Closed";}
			private:
				/** */
				void	open(	ContextApi&	context,
								StateVar&	sv
								) const noexcept;
			};
		/** */
		class Idle : public State {
			private:
				/** */
				const char*	stateName() const noexcept
					{ return "Resetting";}
			public:
				/** */
				void	close(	ContextApi&	context,
								StateVar&	sv
								) const noexcept;
				/** */
				void	readReq(	ContextApi&	context,
									StateVar&	sv
									) const noexcept;
				/** */
				void	writeReq(	ContextApi&	context,
									StateVar&	sv
									) const noexcept;
				/** */
				void	commandReq(	ContextApi&	context,
									StateVar&	sv
									) const noexcept;
				/** */
				void	resetReq(	ContextApi&	context,
									StateVar&	sv
									) const noexcept;
				/** */
				void	getMaxLunReq(	ContextApi&	context,
										StateVar&	sv
										) const noexcept;
				/** */
				void	cancelReq(	ContextApi&	context,
									StateVar&	sv
									) const noexcept;
			};
		/** */
		class Fail : public State {
			private:
				/** */
				const char*	stateName() const noexcept
					{ return "Resetting";}
			public:
				/** */
				void	close(	ContextApi&	context,
								StateVar&	sv
								) const noexcept;
				/** */
				void	readReq(	ContextApi&	context,
									StateVar&	sv
									) const noexcept;
				/** */
				void	writeReq(	ContextApi&	context,
									StateVar&	sv
									) const noexcept;
				/** */
				void	commandReq(	ContextApi&	context,
									StateVar&	sv
									) const noexcept;
				/** */
				void	resetReq(	ContextApi&	context,
									StateVar&	sv
									) const noexcept;
				/** */
				void	getMaxLunReq(	ContextApi&	context,
										StateVar&	sv
										) const noexcept;
			};
		/** */
		class DeferReq : public State {
			public:
				/** */
				void	readReq(	ContextApi&	context,
									StateVar&	sv
									) const noexcept;
				/** */
				void	writeReq(	ContextApi&	context,
									StateVar&	sv
									) const noexcept;
				/** */
				void	commandReq(	ContextApi&	context,
									StateVar&	sv
									) const noexcept;
				/** */
				void	resetReq(	ContextApi&	context,
									StateVar&	sv
									) const noexcept;
				/** */
				void	getMaxLunReq(	ContextApi&	context,
										StateVar&	sv
										) const noexcept;
			};
		/** */
		class Resetting : public DeferReq {
			private:
				/** */
				const char*	stateName() const noexcept
					{ return "Resetting";}
			public:
				/** */
				void	controlNoDataResp(	ContextApi&	context,
											StateVar&	sv
											) const noexcept;
				/** */
				void	cancelReq(	ContextApi&	context,
									StateVar&	sv
									) const noexcept;
			};
		/** */
		class GettingMaxLun : public DeferReq {
			private:
				/** */
				const char*	stateName() const noexcept
					{ return "GettingMaxLun";}
			public:
				/** */
				void	controlReadResp(	ContextApi&	context,
											StateVar&	sv
											) const noexcept;
				/** */
				void	cancelReq(	ContextApi&	context,
									StateVar&	sv
									) const noexcept;
			};
		/** */
		class CommandCB : public DeferReq {
			private:
				/** */
				const char*	stateName() const noexcept
					{ return "CommandCB";}
			public:
				/** */
				void	writeResp(	ContextApi&	context,
									StateVar&	sv
									) const noexcept;
				/** */
				void	cancelReq(	ContextApi&	context,
									StateVar&	sv
									) const noexcept;
			};
		/** */
		class CommandStatus : public DeferReq {
			private:
				/** */
				const char*	stateName() const noexcept
					{ return "CommandStatus";}
			public:
				/** */
				void	readResp(	ContextApi&	context,
									StateVar&	sv
									) const noexcept;
				/** */
				void	cancelReq(	ContextApi&	context,
									StateVar&	sv
									) const noexcept;
			};
		/** */
		class CommandClear : public DeferReq {
			private:
				/** */
				const char*	stateName() const noexcept
					{ return "CommandClear";}
			public:
				/** */
				void	controlNoDataResp(	ContextApi&	context,
											StateVar&	sv
											) const noexcept;
				/** */
				void	cancelReq(	ContextApi&	context,
									StateVar&	sv
									) const noexcept;
			};
		/** */
		class CommandStatus2 : public DeferReq {
			private:
				/** */
				const char*	stateName() const noexcept
					{ return "CommandStatus2";}
			public:
				/** */
				void	readResp(	ContextApi&	context,
									StateVar&	sv
									) const noexcept;
				/** */
				void	cancelReq(	ContextApi&	context,
									StateVar&	sv
									) const noexcept;
			};
		/** */
		class WriteCB : public DeferReq {
			private:
				/** */
				const char*	stateName() const noexcept
					{ return "WriteCB";}
			public:
				/** */
				void	writeResp(	ContextApi&	context,
									StateVar&	sv
									) const noexcept;
				/** */
				void	cancelReq(	ContextApi&	context,
									StateVar&	sv
									) const noexcept;
			};
		/** */
		class WriteData : public DeferReq {
			private:
				/** */
				const char*	stateName() const noexcept
					{ return "WriteData";}
			public:
				/** */
				void	writeResp(	ContextApi&	context,
									StateVar&	sv
									) const noexcept;
				/** */
				void	cancelReq(	ContextApi&	context,
									StateVar&	sv
									) const noexcept;
			};
		/** */
		class WriteStatus : public DeferReq {
			private:
				/** */
				const char*	stateName() const noexcept
					{ return "WriteStatus";}
			public:
				/** */
				void	readResp(	ContextApi&	context,
									StateVar&	sv
									) const noexcept;
				/** */
				void	cancelReq(	ContextApi&	context,
									StateVar&	sv
									) const noexcept;
			};
		/** */
		class WriteClear : public DeferReq {
			private:
				/** */
				const char*	stateName() const noexcept
					{ return "WriteClear";}
			public:
				/** */
				void	controlNoDataResp(	ContextApi&	context,
											StateVar&	sv
											) const noexcept;
				/** */
				void	cancelReq(	ContextApi&	context,
									StateVar&	sv
									) const noexcept;
			};
		/** */
		class WriteStatus2 : public DeferReq {
			private:
				/** */
				const char*	stateName() const noexcept
					{ return "WriteStatus2";}
			public:
				/** */
				void	readResp(	ContextApi&	context,
									StateVar&	sv
									) const noexcept;
				/** */
				void	cancelReq(	ContextApi&	context,
									StateVar&	sv
									) const noexcept;
			};
		/** */
		class ReadCB : public DeferReq {
			private:
				/** */
				const char*	stateName() const noexcept
					{ return "ReadCB";}
			public:
				/** */
				void	writeResp(	ContextApi&	context,
									StateVar&	sv
									) const noexcept;
				/** */
				void	cancelReq(	ContextApi&	context,
									StateVar&	sv
									) const noexcept;
			};
		/** */
		class ReadData : public DeferReq {
			private:
				/** */
				const char*	stateName() const noexcept
					{ return "ReadData";}
			public:
				/** */
				void	readResp(	ContextApi&	context,
									StateVar&	sv
									) const noexcept;
				/** */
				void	cancelReq(	ContextApi&	context,
									StateVar&	sv
									) const noexcept;
			};
		/** */
		class ReadStatus : public DeferReq {
			private:
				/** */
				const char*	stateName() const noexcept
					{ return "ReadStatus";}
			public:
				/** */
				void	readResp(	ContextApi&	context,
									StateVar&	sv
									) const noexcept;
				/** */
				void	cancelReq(	ContextApi&	context,
									StateVar&	sv
									) const noexcept;
			};
		/** */
		class ReadClear : public DeferReq {
			private:
				/** */
				const char*	stateName() const noexcept
					{ return "ReadClear";}
			public:
				/** */
				void	controlNoDataResp(	ContextApi&	context,
											StateVar&	sv
											) const noexcept;
				/** */
				void	cancelReq(	ContextApi&	context,
									StateVar&	sv
									) const noexcept;
			};
		/** */
		class ReadStatus2 : public DeferReq {
			private:
				/** */
				const char*	stateName() const noexcept
					{ return "ReadStatus2";}
			public:
				/** */
				void	readResp(	ContextApi&	context,
									StateVar&	sv
									) const noexcept;
				/** */
				void	cancelReq(	ContextApi&	context,
									StateVar&	sv
									) const noexcept;
			};
		/** */
		class Canceling : public State {
			private:
				/** */
				const char*	stateName() const noexcept
					{ return "Canceling";}
			public:
				/** */
				void	cancelResp(	ContextApi&	context,
									StateVar&	sv
									) const noexcept;
				/** */
				void	controlNoDataResp(	ContextApi&	context,
											StateVar&	sv
											) const noexcept;
				/** */
				void	controlReadResp(	ContextApi&	context,
											StateVar&	sv
											) const noexcept;
				/** */
				void	controlWriteResp(	ContextApi&	context,
											StateVar&	sv
											) const noexcept;
				/** */
				void	writeResp(	ContextApi&	context,
									StateVar&	sv
									) const noexcept;
				/** */
				void	readResp(	ContextApi&	context,
									StateVar&	sv
									) const noexcept;
			};

	private:
		/** */
		struct TraceRec : public Oscl::QueueItem {
			public:
				/** */
				const State*	_state;
			};

	private:
		/** */
		ContextApi&		_context;
		/** */
		const State*	_state;

	private:
		/** */
		enum{nTraceRecs=20};
		/** */
		TraceRec				_traceRec[nTraceRecs];
		/** */
		Oscl::Queue<TraceRec>	_freeTraceRecs;
		/** */
		Oscl::Queue<TraceRec>	_trace;

	public:	// Constructors
		/** */
		StateVar(ContextApi& context) noexcept;

	public:	// events
		/** */
		void	open() noexcept;
		/** */
		void	close() noexcept;
		/** */
		void	cancelResp() noexcept;
		/** */
		void	controlNoDataResp() noexcept;
		/** */
		void	controlReadResp() noexcept;
		/** */
		void	controlWriteResp() noexcept;
		/** */
		void	writeResp() noexcept;
		/** */
		void	readResp() noexcept;
		/** */
		void	writeReq() noexcept;
		/** */
		void	commandReq() noexcept;
		/** */
		void	readReq() noexcept;
		/** */
		void	resetReq() noexcept;
		/** */
		void	getMaxLunReq() noexcept;
		/** */
		void	cancelReq() noexcept;

	private: // global actions and state change operations
		/** */
		friend class State;
		friend class Closed;
		friend class Resetting;
		friend class GettingMaxLun;
		friend class Idle;
		friend class ReadCB;
		friend class ReadData;
		friend class ReadStatus;
		friend class ReadClear;
		friend class ReadStatus2;
		friend class WriteCB;
		friend class CommandCB;
		friend class CommandStatus;
		friend class CommandClear;
		friend class CommandStatus2;
		friend class WriteData;
		friend class WriteStatus;
		friend class WriteClear;
		friend class WriteStatus2;
		friend class Canceling;
		/** */
		void	protocolError(const char* state) noexcept;
		/** */
		void	changeToClosed() noexcept;
		/** */
		void	changeToResetting() noexcept;
		/** */
		void	changeToGettingMaxLun() noexcept;
		/** */
		void	changeToFail() noexcept;
		/** */
		void	changeToIdle() noexcept;
		/** */
		void	changeToReadCB() noexcept;
		/** */
		void	changeToReadData() noexcept;
		/** */
		void	changeToReadStatus() noexcept;
		/** */
		void	changeToReadClear() noexcept;
		/** */
		void	changeToReadStatus2() noexcept;
		/** */
		void	changeToCommandCB() noexcept;
		/** */
		void	changeToWriteCB() noexcept;
		/** */
		void	changeToWriteData() noexcept;
		/** */
		void	changeToWriteStatus() noexcept;
		/** */
		void	changeToWriteClear() noexcept;
		/** */
		void	changeToWriteStatus2() noexcept;
		/** */
		void	changeToCommandStatus() noexcept;
		/** */
		void	changeToCommandClear() noexcept;
		/** */
		void	changeToCommandStatus2() noexcept;
		/** */
		void	changeToCanceling() noexcept;

	private:
		/** */
		void	trace(const State& state) noexcept;
		/** */
		void	executeDeferredReq() noexcept;
		/** */
		void	flushDeferQueue() noexcept;
	};

}
}
}
}

#endif
