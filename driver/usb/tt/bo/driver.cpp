/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <new>
#include <string.h>
#include <stdio.h>
#include "driver.h"
#include "oscl/mt/thread.h"
#include "oscl/error/fatal.h"
#include "oscl/error/info.h"
#include "oscl/tt/itc/sync.h"
#include "oscl/tt/status.h"
#include "oscl/driver/usb/pipe/result.h"
#include "oscl/driver/usb/setup/clrepfeature.h"

#if 0
static void dumpPacket(void* packet) {
	unsigned char*	p	= (unsigned char*)packet;
	for(unsigned i=0;i<16;++i){
		if(!(i%8)){
			Oscl::Error::Info::log("\n");
			}
		char	buffer[16];
		sprintf(buffer," %2.2X",(unsigned)p[i]);
		Oscl::Error::Info::log(buffer);
		}
	Oscl::Error::Info::log("\n");
	}
#endif

using namespace Oscl::Usb::TT::BO;

Driver::Driver(	Oscl::Usb::Pipe::Message::
				PipeApi&							controlPipe,
				Oscl::Usb::Pipe::Bulk::
				IN::Req::Api::SAP&					bulkInPipe,
				Oscl::Usb::Pipe::Bulk::
				OUT::Req::Api::SAP&					bulkOutPipe,
				Oscl::Mt::Itc::PostMsgApi&			myPapi,
				SetupMem&							setupMem,
				PacketMem&							packetMem,
				unsigned char						maxBulkInPacketSize,
				unsigned char						maxBulkOutPacketSize,
				uint8_t							bInterfaceNumber,
				uint8_t							bulkInEndpointID,
				uint8_t							bulkOutEndpointID
				) noexcept:
		Oscl::Mt::Itc::Srv::CloseSync(*this,myPapi),
		_controlPipe(controlPipe),
		_bInterfaceNumber(bInterfaceNumber),
		_bulkInEndpointID(bulkInEndpointID),
		_bulkOutEndpointID(bulkOutEndpointID),
		_bulkInPipe(bulkInPipe),
		_bulkOutPipe(bulkOutPipe),
		_setupMem(setupMem),
		_packetMem(packetMem),
		_myPapi(myPapi),
		_sync(	*this,
				myPapi
				),
		_maxBulkInPacketSize(maxBulkInPacketSize),
		_maxBulkOutPacketSize(maxBulkOutPacketSize),
		_openReq(0),
		_closeReq(0),
		_state(*this)
		{
	}

Oscl::TT::Api&	Driver::getTTApi() noexcept{
	return _sync;
	}

Oscl::TT::ITC::Req::Api::SAP&	Driver::getTTSAP() noexcept{
	return _sync.getSAP();
	}


void	Driver::request(	Oscl::Mt::Itc::Srv::
							Open::Req::Api::OpenReq&	msg
							) noexcept{
	_openReq	= &msg;
	_state.open();
	}

void	Driver::request(	Oscl::Mt::Itc::Srv::
							Close::Req::Api::CloseReq&	msg
							) noexcept{
	_closeReq	= &msg;
	_state.close();
	}

void	Driver::request(ResetReq& msg) noexcept{
	_incomming.reset	= &msg;
	_state.resetReq();
	}

void	Driver::request(GetMaxLunReq& msg) noexcept{
	_incomming.getMaxLun	= &msg;
	_state.getMaxLunReq();
	}

void	Driver::request(CancelReq& msg) noexcept{
	_cancelReq	= &msg;
	if(!_pendingReqs.remove(&msg._payload._msgToCancel)){
		if(_current.generic == &msg._payload._msgToCancel){
			_state.cancelReq();
			return;
			}
		}
	msg.returnToSender();
	}

void	Driver::request(ReadReq& msg) noexcept{
	_incomming.read	= &msg;
	_state.readReq();
	}

void	Driver::request(WriteReq& msg) noexcept{
	_incomming.write	= &msg;
	_state.writeReq();
	}

void	Driver::request(CommandReq& msg) noexcept{
	_incomming.command	= &msg;
	_state.commandReq();
	}

void	Driver::response(	Oscl::Usb::Pipe::Bulk::
							OUT::Resp::Api::WriteResp&		msg
							) noexcept{
	if((_result=msg._payload._failed)){
		Oscl::Error::Info::log("USB Mass Storage Bulk Write failed\n");
		}
	_state.writeResp();
	}

void	Driver::response(	Oscl::Usb::Pipe::Bulk::
							OUT::Resp::Api::CancelResp&		msg
							) noexcept{
	_state.cancelResp();
	}

void	Driver::response(	Oscl::Usb::Pipe::Bulk::
							IN::Resp::Api::ReadResp&		msg
							) noexcept{
	if((_result=msg._payload._failed)){
		Oscl::Error::Info::log("USB Mass Storage Bulk Read failed\n");
		}
	_state.readResp();
	}

void	Driver::response(	Oscl::Usb::Pipe::Bulk::
							IN::Resp::Api::CancelResp&		msg
							) noexcept{
	_state.cancelResp();
	}

void	Driver::response(	Oscl::Usb::Pipe::Message::
							Resp::Api::CancelResp&		msg
							) noexcept{
	_state.cancelResp();
	}

void	Driver::response(	Oscl::Usb::Pipe::Message::
							Resp::Api::NoDataResp&		msg
							) noexcept{
	_state.controlNoDataResp();
	}

void	Driver::response(	Oscl::Usb::Pipe::Message::
							Resp::Api::ReadResp&		msg
							) noexcept{
	_state.controlReadResp();
	}

void	Driver::response(	Oscl::Usb::Pipe::Message::
							Resp::Api::WriteResp&		msg
							) noexcept{
	_state.controlWriteResp();
	}

/////////////////////////////////
void	Driver::returnOpenReq() noexcept{
	_openReq->returnToSender();
	}

void	Driver::returnCloseReq() noexcept{
	_closeReq->returnToSender();
	}

void	Driver::executeDeferredReq() noexcept{
	Oscl::Mt::Itc::SrvMsg*
	req = _pendingReqs.get();
	if(req){
		req->process();
		}
	}

void	Driver::deferReq() noexcept{
	_pendingReqs.put(_current.generic);
	}

void	Driver::flushDeferQueue() noexcept{
	Oscl::Mt::Itc::SrvMsg* req;
	while((req=_pendingReqs.get())){
		req->process();
		}
	}

void	Driver::startReset() noexcept{
	using namespace Oscl::Usb::Hw::Setup;
	_current.reset	= _incomming.reset;
	Oscl::Usb::Setup::Packet*
	packet	= new(&_setupMem.controlSetup)
				Oscl::Usb::Setup::Packet(	0x21,
											0xFF,
											0,
											_bInterfaceNumber,
											0
											);
	Oscl::Usb::Pipe::Message::
	Req::Api::NoDataPayload*
	payload	= new(&_pipeMem.controlNoData.payload)
				Oscl::Usb::Pipe::Message::
				Req::Api::NoDataPayload(*packet);
	Oscl::Usb::Pipe::Message::
	Resp::Api::NoDataResp*
	resp	= new (&_pipeMem.controlNoData.resp)
				Oscl::Usb::Pipe::Message::
				Resp::Api::NoDataResp(	_controlPipe.getSAP().getReqApi(),
										*this,
										_myPapi,
										*payload
										);
	_currentPipe.messageNoData	= resp;
	_controlPipe.getSAP().post(resp->getSrvMsg());
	}

void	Driver::startBulkInClearHalt() noexcept{
	using namespace Oscl::Usb::Hw::Setup;
	_current.reset	= _incomming.reset;
	Oscl::Usb::Setup::ClearEndpointFeature*
	packet	= new(&_setupMem.controlSetup)
				Oscl::Usb::Setup::ClearEndpointFeature(	0,	// Endpoint HALT
														_bulkInEndpointID
														);
	Oscl::Usb::Pipe::Message::
	Req::Api::NoDataPayload*
	payload	= new(&_pipeMem.controlNoData.payload)
				Oscl::Usb::Pipe::Message::
				Req::Api::NoDataPayload(*packet);
	Oscl::Usb::Pipe::Message::
	Resp::Api::NoDataResp*
	resp	= new (&_pipeMem.controlNoData.resp)
				Oscl::Usb::Pipe::Message::
				Resp::Api::NoDataResp(	_controlPipe.getSAP().getReqApi(),
										*this,
										_myPapi,
										*payload
										);
	_currentPipe.messageNoData	= resp;
	_controlPipe.getSAP().post(resp->getSrvMsg());
	}

void	Driver::startGetMaxLun() noexcept{
	using namespace Oscl::Usb::Hw::Setup;
	_current.getMaxLun	= _incomming.getMaxLun;
	Oscl::Usb::Setup::Packet*
	packet	= new(&_setupMem.controlSetup)
				Oscl::Usb::Setup::Packet(	0xA1,
											0xFE,
											0,
											_bInterfaceNumber,
											1
											);
	_packetMem.getMaxLun._charMem[0]	= 0xFF;
	_packetMem.getMaxLun._charMem[1]	= 0xFF;
	_packetMem.getMaxLun._charMem[2]	= 0xFF;
	_packetMem.getMaxLun._charMem[3]	= 0xFF;
//	dumpPacket(_packetMem.getMaxLun._charMem);
	Oscl::Usb::Pipe::Message::
	Req::Api::ReadPayload*
	payload	= new(&_pipeMem.controlRead.payload)
				Oscl::Usb::Pipe::Message::
				Req::Api::ReadPayload(	*packet,
										_packetMem.getMaxLun._charMem
										);
	Oscl::Usb::Pipe::Message::
	Resp::Api::ReadResp*
	resp	= new (&_pipeMem.controlRead.resp)
				Oscl::Usb::Pipe::Message::
				Resp::Api::ReadResp(	_controlPipe.getSAP().getReqApi(),
										*this,
										_myPapi,
										*payload
										);
	_currentPipe.messageRead	= resp;
	_controlPipe.getSAP().post(resp->getSrvMsg());
	}

void	Driver::startCommandCB() noexcept{
	_current.command	= _incomming.command;
	++_currentTag.word;
	CBW*	cbw	= (CBW*)&_packetMem.cbw;
	cbw->dCBWSignature[0]			= 0x55;
	cbw->dCBWSignature[1]			= 0x53;
	cbw->dCBWSignature[2]			= 0x42;
	cbw->dCBWSignature[3]			= 0x43;
	cbw->dCBWTag[0]					= _currentTag.array[0];
	cbw->dCBWTag[1]					= _currentTag.array[1];
	cbw->dCBWTag[2]					= _currentTag.array[2];
	cbw->dCBWTag[3]					= _currentTag.array[3];
	cbw->dCBWDataTransferLength[0]	= 0;
	cbw->dCBWDataTransferLength[1]	= 0;
	cbw->dCBWDataTransferLength[2]	= 0;
	cbw->dCBWDataTransferLength[3]	= 0;
	cbw->bCBWFlags					= 0x00;	// don't care
	cbw->bCBWLUN					= _current.command->_payload._lun;
	cbw->bCBWCBLength				= _current.command->_payload._cbLen;

	memcpy(	cbw->CBWCB,
			_current.command->_payload._cb,
			_current.command->_payload._cbLen
			);

	Oscl::Usb::Pipe::Bulk::
	OUT::Req::Api::WritePayload*
	payload	= new(&_pipeMem.bulkOut.payload)
				Oscl::Usb::Pipe::Bulk::
				OUT::Req::Api::
				WritePayload(	cbw,
								31
								);
	Oscl::Usb::Pipe::Bulk::
	OUT::Resp::Api::WriteResp*
	resp	= new (&_pipeMem.bulkOut.resp)
				Oscl::Usb::Pipe::Bulk::
				OUT::Resp::Api::WriteResp(	_bulkOutPipe.getReqApi(),
											*this,
											_myPapi,
											*payload
											);
	_currentPipe.bulkWrite	= resp;
	_bulkOutPipe.post(resp->getSrvMsg());
	}

void	Driver::startWriteCB() noexcept{
	_current.write	= _incomming.write;
	++_currentTag.word;
	CBW*	cbw	= (CBW*)&_packetMem.cbw;
	cbw->dCBWSignature[0]			= 0x55;
	cbw->dCBWSignature[1]			= 0x53;
	cbw->dCBWSignature[2]			= 0x42;
	cbw->dCBWSignature[3]			= 0x43;
	cbw->dCBWTag[0]					= _currentTag.array[0];
	cbw->dCBWTag[1]					= _currentTag.array[1];
	cbw->dCBWTag[2]					= _currentTag.array[2];
	cbw->dCBWTag[3]					= _currentTag.array[3];
	uint32_t		length			= _current.write->_payload._bufferLen;
	cbw->dCBWDataTransferLength[0]	= (unsigned char)(length >> 0);
	cbw->dCBWDataTransferLength[1]	= (unsigned char)(length >> 8);
	cbw->dCBWDataTransferLength[2]	= (unsigned char)(length >> 16);
	cbw->dCBWDataTransferLength[3]	= (unsigned char)(length >> 24);
	cbw->bCBWFlags					= 0x00;	// Out/Write
	cbw->bCBWLUN					= _current.write->_payload._lun;
	cbw->bCBWCBLength				= _current.write->_payload._cbLen;

	memcpy(	cbw->CBWCB,
			_current.write->_payload._cb,
			_current.write->_payload._cbLen
			);

	Oscl::Usb::Pipe::Bulk::
	OUT::Req::Api::WritePayload*
	payload	= new(&_pipeMem.bulkOut.payload)
				Oscl::Usb::Pipe::Bulk::
				OUT::Req::Api::
				WritePayload(	cbw,
								31
								);
	Oscl::Usb::Pipe::Bulk::
	OUT::Resp::Api::WriteResp*
	resp	= new (&_pipeMem.bulkOut.resp)
				Oscl::Usb::Pipe::Bulk::
				OUT::Resp::Api::WriteResp(	_bulkOutPipe.getReqApi(),
											*this,
											_myPapi,
											*payload
											);
	_currentPipe.bulkWrite	= resp;
	_bulkOutPipe.post(resp->getSrvMsg());
	}

void	Driver::startWriteData() noexcept{
	Oscl::Usb::Pipe::Bulk::
	OUT::Req::Api::WritePayload*
	payload	= new(&_pipeMem.bulkOut.payload)
				Oscl::Usb::Pipe::Bulk::
				OUT::Req::Api::
				WritePayload(	_current.write->_payload._buffer,
								_current.write->_payload._bufferLen
								);
	Oscl::Usb::Pipe::Bulk::
	OUT::Resp::Api::WriteResp*
	resp	= new (&_pipeMem.bulkOut.resp)
				Oscl::Usb::Pipe::Bulk::
				OUT::Resp::Api::WriteResp(	_bulkOutPipe.getReqApi(),
											*this,
											_myPapi,
											*payload
											);
	_currentPipe.bulkWrite	= resp;
	_bulkOutPipe.post(resp->getSrvMsg());
	}

void	Driver::startReadCB() noexcept{
	_current.read	= _incomming.read;
	++_currentTag.word;
	CBW*	cbw	= (CBW*)(&_packetMem.cbw);
	cbw->dCBWSignature[0]			= 0x55;	// 'U'
	cbw->dCBWSignature[1]			= 0x53;	// 'S'
	cbw->dCBWSignature[2]			= 0x42;	// 'B'
	cbw->dCBWSignature[3]			= 0x43;	// 'C' command
	cbw->dCBWTag[0]					= _currentTag.array[0];
	cbw->dCBWTag[1]					= _currentTag.array[1];
	cbw->dCBWTag[2]					= _currentTag.array[2];
	cbw->dCBWTag[3]					= _currentTag.array[3];
	uint32_t		length			= _current.read->_payload._bufferLen;
	cbw->dCBWDataTransferLength[0]	= (unsigned char)(length >> 0);
	cbw->dCBWDataTransferLength[1]	= (unsigned char)(length >> 8);
	cbw->dCBWDataTransferLength[2]	= (unsigned char)(length >> 16);
	cbw->dCBWDataTransferLength[3]	= (unsigned char)(length >> 24);
	cbw->bCBWFlags					= 0x80;	// IN/Read
	cbw->bCBWLUN					= _current.read->_payload._lun;
	cbw->bCBWCBLength				= _current.read->_payload._cbLen;

	memcpy(	cbw->CBWCB,
			_current.read->_payload._cb,
			_current.read->_payload._cbLen
			);

	Oscl::Usb::Pipe::Bulk::
	OUT::Req::Api::WritePayload*
	payload	= new(&_pipeMem.bulkOut.payload)
				Oscl::Usb::Pipe::Bulk::
				OUT::Req::Api::WritePayload(	cbw,
												31
												);
	Oscl::Usb::Pipe::Bulk::
	OUT::Resp::Api::WriteResp*
	resp	= new (&_pipeMem.bulkOut.resp)
				Oscl::Usb::Pipe::Bulk::
				OUT::Resp::Api::WriteResp(	_bulkOutPipe.getReqApi(),
											*this,
											_myPapi,
											*payload
											);
	_currentPipe.bulkWrite	= resp;
	_bulkOutPipe.post(resp->getSrvMsg());
	}

void	Driver::startReadData() noexcept{
	Oscl::Usb::Pipe::Bulk::
	IN::Req::Api::ReadPayload*
	payload	= new(&_pipeMem.bulkIn.payload)
				Oscl::Usb::Pipe::Bulk::
				IN::Req::Api::ReadPayload(	_current.read->_payload._buffer,
											_current.read->_payload._bufferLen
											);
	Oscl::Usb::Pipe::Bulk::
	IN::Resp::Api::ReadResp*
	resp	= new (&_pipeMem.bulkIn.resp)
				Oscl::Usb::Pipe::Bulk::
				IN::Resp::Api::ReadResp(	_bulkInPipe.getReqApi(),
											*this,
											_myPapi,
											*payload
											);
	_currentPipe.bulkRead	= resp;
	_bulkInPipe.post(resp->getSrvMsg());
	}

void	Driver::startStatus() noexcept{
	Oscl::Usb::Pipe::Bulk::
	IN::Req::Api::ReadPayload*
	payload	= new(&_pipeMem.bulkIn.payload)
				Oscl::Usb::Pipe::Bulk::
				IN::Req::Api::ReadPayload(	&_packetMem.csw,
											13
											);
	Oscl::Usb::Pipe::Bulk::
	IN::Resp::Api::ReadResp*
	resp	= new (&_pipeMem.bulkIn.resp)
				Oscl::Usb::Pipe::Bulk::
				IN::Resp::Api::ReadResp(	_bulkInPipe.getReqApi(),
											*this,
											_myPapi,
											*payload
											);
	_currentPipe.bulkRead	= resp;
	_bulkInPipe.post(resp->getSrvMsg());
	}

const Oscl::TT::Status::Result*
Driver::parseCswResult() noexcept{
	CSW*	csw	= (CSW*)&_packetMem.csw;
	// Check dCSWStatus
	switch(csw->dCSWStatus){
		case 0:	// Good
			return 0;
		case 1:	// Failed
			Oscl::Error::Info::log("Oscl::Usb::TT::BO::Driver : CSW Failed\n");
			return &Oscl::TT::Status::getFailed();
		case 2:	// Phase Error
			Oscl::Error::Info::log("Oscl::Usb::TT::BO::Driver : CSW PhaseError\n");
			return &Oscl::TT::Status::getPhaseError();
		default:	// !unexpected
			Oscl::Error::Info::log("Oscl::Usb::TT::BO::Driver : CSW Unexpected Error\n");
			return &Oscl::TT::Status::getFailed();
		};
	}

const Oscl::TT::Status::Result*
Driver::parseCSW( unsigned long& residue) noexcept{
	// Assume that "cswIsValid() is true)
	CSW*	csw	= (CSW*)&_packetMem.csw;

	// Get residue
	uint32_t	res	= csw->dCSWDataResidue[3];
	res	<<= 8;
	res	|=	csw->dCSWDataResidue[2];
	res	<<= 8;
	res	|=	csw->dCSWDataResidue[1];
	res	<<= 8;
	res	|=	csw->dCSWDataResidue[0];

	residue	= res;
	
	return parseCswResult();
	}

void	Driver::finishWriteReq() noexcept{
	// Assume that "cswIsValid() is true)
	// Test status READ
	if(_result){
		BulkInStatusResult	result;
		_result->query(result);
		if(result._failed){
			Oscl::Error::Info::log("Oscl::Usb::TT::BO::Driver : finishWriteReq Failed\n");
			_current.write->_payload._result	= &Oscl::TT::Status::getTransportError();
			return;
			}
		}
	_current.write->_payload._result	=
	parseCSW(_current.write->_payload._residue);
	}

void	Driver::finishCommandReq() noexcept{
	// Assume that "cswIsValid() is true)
	// Test status READ
	if(_result){
		BulkInStatusResult	result;
		_result->query(result);
		if(result._failed){
			Oscl::Error::Info::log("Oscl::Usb::TT::BO::Driver : finishCommandReq Failed\n");
			_current.command->_payload._result	= &Oscl::TT::Status::getTransportError();
			return;
			}
		}
	_current.command->_payload._result	= parseCswResult();
	}

void	Driver::failReadReq() noexcept{
	_current.read->_payload._result	= &Oscl::TT::Status::getTransportError();
	}

void	Driver::failWriteReq() noexcept{
	_current.write->_payload._result	= &Oscl::TT::Status::getTransportError();
	}

void	Driver::failCommandReq() noexcept{
	_current.command->_payload._result	= &Oscl::TT::Status::getTransportError();
	}

void	Driver::failResetReq() noexcept{
	_current.reset->_payload._failed	= true;
	}

void	Driver::failGetMaxLunReq() noexcept{
	_current.getMaxLun->_payload._failed	= true;
	}

void	Driver::finishReadReq() noexcept{
	// Test status READ
	if(_result){
		BulkInStatusResult	result;
		_result->query(result);
		if(result._failed){
			Oscl::Error::Info::log("Oscl::Usb::TT::BO::Driver : finishReadReq Failed\n");
			_current.read->_payload._result	= &Oscl::TT::Status::getTransportError();
			return;
			}
		}
	_current.read->_payload._result	=
	parseCSW(_current.read->_payload._residue);
	}

void	Driver::finishResetReq() noexcept{
	_current.reset->_payload._failed =
		_currentPipe.messageNoData->_payload._failed;
	Oscl::Mt::Thread::sleep(500);	// Give the device 500ms
	}

void	Driver::finishGetMaxLunReq() noexcept{
	const Oscl::Usb::Pipe::Status::Result*
	result = _currentPipe.messageRead->_payload._failed;
	unsigned char	maxLun = _packetMem.getMaxLun._charMem[0];
	if(result){
		GetMaxLunResult					decide;
		result->query(decide);
		if(decide._failed){
			Oscl::Error::Info::log(	"Fatal USB Mass Storage"
									" GetMaxLun failure!\n"
									);
			return;
			}
		maxLun	= 0;
		result	= 0;
		}

	_current.getMaxLun->_payload._failed = result;
	_current.getMaxLun->_payload._maxLun = maxLun;
//	dumpPacket(_packetMem.getMaxLun._charMem);
	}

void	Driver::returnResetReq() noexcept{
	_current.reset->returnToSender();
	_current.generic	= 0;
	}

void	Driver::returnGetMaxLunReq() noexcept{
	_current.getMaxLun->returnToSender();
	_current.generic	= 0;
	}

void	Driver::returnReadReq() noexcept{
	_current.read->returnToSender();
	_current.generic	= 0;
	}

void	Driver::returnWriteReq() noexcept{
	_current.write->returnToSender();
	_current.generic	= 0;
	}

void	Driver::returnCommandReq() noexcept{
	_current.command->returnToSender();
	_current.generic	= 0;
	}

void	Driver::returnCancelReq() noexcept{
	_cancelReq->returnToSender();
	_current.generic	= 0;
	}

void	Driver::returnCanceledReq() noexcept{
	_current.generic->returnToSender();
	_current.generic	= 0;
	}

void	Driver::cancelMessage(Oscl::Mt::Itc::SrvMsg& msg) noexcept{
	Oscl::Usb::Pipe::Message::
	Req::Api::CancelPayload*
	payload	= new(&_cancelMem.messagePipe.payload)
				Oscl::Usb::Pipe::Message::Req::Api::
				CancelPayload(msg);
	Oscl::Usb::Pipe::Message::
	Resp::Api::CancelResp*
	resp	= new (&_cancelMem.messagePipe.resp)
				Oscl::Usb::Pipe::Message::
				Resp::Api::CancelResp(	_controlPipe.getSAP().getReqApi(),
										*this,
										_myPapi,
										*payload
										);
	_controlPipe.getSAP().post(resp->getSrvMsg());
	}

void	Driver::cancelBulkOut(Oscl::Mt::Itc::SrvMsg& msg) noexcept{
	Oscl::Usb::Pipe::Bulk::OUT::
	Req::Api::CancelPayload*
	payload	= new(&_cancelMem.bulkOutPipe.payload)
				Oscl::Usb::Pipe::Bulk::OUT::Req::Api::
				CancelPayload(msg);
	Oscl::Usb::Pipe::Bulk::OUT::
	Resp::Api::CancelResp*
	resp	= new (&_cancelMem.bulkOutPipe.resp)
				Oscl::Usb::Pipe::Bulk::OUT::
				Resp::Api::CancelResp(	_bulkOutPipe.getReqApi(),
										*this,
										_myPapi,
										*payload
										);
	_bulkOutPipe.post(resp->getSrvMsg());
	}

void	Driver::cancelBulkIn(Oscl::Mt::Itc::SrvMsg& msg) noexcept{
	Oscl::Usb::Pipe::Bulk::IN::
	Req::Api::CancelPayload*
	payload	= new(&_cancelMem.bulkInPipe.payload)
				Oscl::Usb::Pipe::Bulk::IN::Req::Api::
				CancelPayload(msg);
	Oscl::Usb::Pipe::Bulk::IN::
	Resp::Api::CancelResp*
	resp	= new (&_cancelMem.bulkInPipe.resp)
				Oscl::Usb::Pipe::Bulk::IN::
				Resp::Api::CancelResp(	_bulkInPipe.getReqApi(),
										*this,
										_myPapi,
										*payload
										);
	_bulkInPipe.post(resp->getSrvMsg());
	}

void	Driver::cancelClearFeatureReq() noexcept{
	cancelMessage(_currentPipe.messageNoData->getSrvMsg());
	}

void	Driver::cancelResetReq() noexcept{
	_current.generic	= _current.reset;
	cancelMessage(_currentPipe.messageNoData->getSrvMsg());
	}

void	Driver::cancelGetMaxLunReq() noexcept{
	_current.generic	= _current.getMaxLun;
	cancelMessage(_currentPipe.messageRead->getSrvMsg());
	}

void	Driver::cancelReadReq() noexcept{
	_current.generic	= _current.read;
	cancelBulkIn(_currentPipe.bulkRead->getSrvMsg());
	}

void	Driver::cancelWriteReq() noexcept{
	_current.generic	= _current.write;
	cancelBulkOut(_currentPipe.bulkWrite->getSrvMsg());
	}

void	Driver::cancelCommandReq() noexcept{
	_current.generic	= _current.command;
	cancelBulkOut(_currentPipe.bulkWrite->getSrvMsg());
	}

bool	Driver::deferQueueEmpty() noexcept{
	return !_pendingReqs.first();
	}

bool	Driver::statusRetryFailureType() noexcept{
	if(_result){
		BulkInStatusRetry	result;
		_result->query(result);
		if(!result._retry){
			Oscl::Error::Info::log("Oscl::Usb::TT::BO::Driver : Status Failed\n");
			}
		return result._retry;
		}
	return false;
	}

bool	Driver::statusFailed() noexcept{
	if(_result){
		BulkInStatusResult	result;
		_result->query(result);
		if(result._failed){
			Oscl::Error::Info::log("Oscl::Usb::TT::BO::Driver : Status Failed\n");
			}
		return result._failed;
		}
	return false;
	}

bool	Driver::readFailed() noexcept{
	if(_result){
		BulkInReadResult	result;
		_result->query(result);
		if(result._failed){
			Oscl::Error::Info::log("Oscl::Usb::TT::BO::Driver : Read Failed\n");
			}
		return result._failed;
		}
	return false;
	}

bool	Driver::writeFailed() noexcept{
	if(_result){
		Oscl::Error::Info::log("Oscl::Usb::TT::BO::Driver : Write Failed\n");
		}
	return _result;
	}

bool	Driver::cswIsValid() noexcept{
	// Check CSW size
	if(_currentPipe.bulkRead->_payload._nRead != 13){
		Oscl::Error::Info::log("Oscl::Usb::TT::BO::Driver : CSW size failed\n");
		return false;
		}
	uint8_t*	csw	= (uint8_t*)&_packetMem.csw;
	// Check dCSWSignature
	if(	!	(	(csw[0] == 0x55)	// 'U'
			&&	(csw[1] == 0x53)	// 'S'
			&&	(csw[2] == 0x42)	// 'B'
			&&	(csw[3] == 0x53)	// 'S' status
			)
		){
		// Bad signature
		Oscl::Error::Info::log("Oscl::Usb::TT::BO::Driver : CSW signature failed\n");
		return false;
		}

	// Check dCSWTag
	if(	!	(	(csw[4] == _currentTag.array[0])
			&&	(csw[5] == _currentTag.array[1])
			&&	(csw[6] == _currentTag.array[2])
			&&	(csw[7] == _currentTag.array[3])
			)
		){
		Oscl::Error::Info::log("Oscl::Usb::TT::BO::Driver : CSW tag failed\n");
		return false;
		}
	return true;
	}


///////////////////////////

Driver::GetMaxLunResult::GetMaxLunResult() noexcept
		{}

void	Driver::GetMaxLunResult::pipeClosed() noexcept{
	_failed	= true;
	}

void	Driver::GetMaxLunResult::canceled() noexcept{
	_failed	= true;
	}

void	Driver::GetMaxLunResult::crc() noexcept{
	_failed	= true;
	}

void	Driver::GetMaxLunResult::bitStuffing() noexcept{
	_failed	= true;
	}

void	Driver::GetMaxLunResult::dataToggleMismatch() noexcept{
	_failed	= true;
	}

void	Driver::GetMaxLunResult::stall() noexcept{
	_failed	= false;
	}

void	Driver::GetMaxLunResult::deviceNotResponding() noexcept{
	_failed	= true;
	}

void	Driver::GetMaxLunResult::pidCheckFailure() noexcept{
	_failed	= true;
	}

void	Driver::GetMaxLunResult::unexpectedPID() noexcept{
	_failed	= true;
	}

void	Driver::GetMaxLunResult::dataOverrun() noexcept{
	_failed	= true;
	}

void	Driver::GetMaxLunResult::dataUnderrun() noexcept{
	_failed	= true;
	}

void	Driver::GetMaxLunResult::bufferOverrun() noexcept{
	_failed	= true;
	}

void	Driver::GetMaxLunResult::bufferUnderrun() noexcept{
	_failed	= true;
	}

///////////////////////////

Driver::BulkInStatusResult::BulkInStatusResult() noexcept
		{}

void	Driver::BulkInStatusResult::pipeClosed() noexcept{
	_failed	= true;
	}

void	Driver::BulkInStatusResult::canceled() noexcept{
	_failed	= true;
	}

void	Driver::BulkInStatusResult::crc() noexcept{
	_failed	= true;
	}

void	Driver::BulkInStatusResult::bitStuffing() noexcept{
	_failed	= true;
	}

void	Driver::BulkInStatusResult::dataToggleMismatch() noexcept{
	_failed	= true;
	}

void	Driver::BulkInStatusResult::stall() noexcept{
	_failed	= false;
	}

void	Driver::BulkInStatusResult::deviceNotResponding() noexcept{
	_failed	= true;
	}

void	Driver::BulkInStatusResult::pidCheckFailure() noexcept{
	_failed	= true;
	}

void	Driver::BulkInStatusResult::unexpectedPID() noexcept{
	_failed	= true;
	}

void	Driver::BulkInStatusResult::dataOverrun() noexcept{
	_failed	= true;
	}

void	Driver::BulkInStatusResult::dataUnderrun() noexcept{
	_failed	= false;
	}

void	Driver::BulkInStatusResult::bufferOverrun() noexcept{
	_failed	= true;
	}

void	Driver::BulkInStatusResult::bufferUnderrun() noexcept{
	_failed	= true;
	}

///////////////////////////

Driver::BulkInReadResult::BulkInReadResult() noexcept
		{}

void	Driver::BulkInReadResult::pipeClosed() noexcept{
	_failed	= true;
	}

void	Driver::BulkInReadResult::canceled() noexcept{
	_failed	= true;
	}

void	Driver::BulkInReadResult::crc() noexcept{
	_failed	= true;
	}

void	Driver::BulkInReadResult::bitStuffing() noexcept{
	_failed	= true;
	}

void	Driver::BulkInReadResult::dataToggleMismatch() noexcept{
	_failed	= true;
	}

void	Driver::BulkInReadResult::stall() noexcept{
	_failed	= true;
	}

void	Driver::BulkInReadResult::deviceNotResponding() noexcept{
	_failed	= true;
	}

void	Driver::BulkInReadResult::pidCheckFailure() noexcept{
	_failed	= true;
	}

void	Driver::BulkInReadResult::unexpectedPID() noexcept{
	_failed	= true;
	}

void	Driver::BulkInReadResult::dataOverrun() noexcept{
	_failed	= true;
	}

void	Driver::BulkInReadResult::dataUnderrun() noexcept{
	_failed	= false;
	}

void	Driver::BulkInReadResult::bufferOverrun() noexcept{
	_failed	= true;
	}

void	Driver::BulkInReadResult::bufferUnderrun() noexcept{
	_failed	= true;
	}

///////////////////////////

Driver::BulkInStatusRetry::BulkInStatusRetry() noexcept
		{}

void	Driver::BulkInStatusRetry::pipeClosed() noexcept{
	_retry	= false;
	}

void	Driver::BulkInStatusRetry::canceled() noexcept{
	_retry	= false;
	}

void	Driver::BulkInStatusRetry::crc() noexcept{
	_retry	= true;
	}

void	Driver::BulkInStatusRetry::bitStuffing() noexcept{
	_retry	= true;
	}

void	Driver::BulkInStatusRetry::dataToggleMismatch() noexcept{
	_retry	= true;
	}

void	Driver::BulkInStatusRetry::stall() noexcept{
	_retry	= true;
	}

void	Driver::BulkInStatusRetry::deviceNotResponding() noexcept{
	_retry	= true;
	}

void	Driver::BulkInStatusRetry::pidCheckFailure() noexcept{
	_retry	= true;
	}

void	Driver::BulkInStatusRetry::unexpectedPID() noexcept{
	_retry	= true;
	}

void	Driver::BulkInStatusRetry::dataOverrun() noexcept{
	_retry	= true;
	}

void	Driver::BulkInStatusRetry::dataUnderrun() noexcept{
	_retry	= true;
	}

void	Driver::BulkInStatusRetry::bufferOverrun() noexcept{
	_retry	= true;
	}

void	Driver::BulkInStatusRetry::bufferUnderrun() noexcept{
	_retry	= true;
	}

