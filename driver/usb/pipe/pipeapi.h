/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_usb_pipe_pipeapih_
#define _oscl_drv_usb_pipe_pipeapih_
#include "oscl/mt/itc/srv/closeapi.h"
#include "oscl/mt/itc/srv/ocsyncapi.h"
#include "oscl/queue/queueitem.h"

/** */
namespace Oscl {
/** */
namespace Usb {
/** */
namespace Pipe {

/** */
class PipeApi : public Oscl::QueueItem {
	public:
		/** */
		virtual ~PipeApi(){}
		/** */
		virtual Oscl::Mt::Itc::Srv::
				Close::Req::Api::SAP&	getCloseSAP() noexcept=0;
		/** */
		virtual Oscl::Mt::Itc::Srv::
				OpenCloseSyncApi&		getOpenCloseSyncApi() noexcept=0;
	};

}
}
}

#endif
