/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_driver_usb_pipe_statush_
#define _oscl_driver_usb_pipe_statush_
#include "result.h"

/** */
namespace Oscl {
/** */
namespace Usb {
/** */
namespace Pipe {
/** */
namespace Status {

/** */
const Result&	getPipeClosed() noexcept;
/** */
const Result&	getCanceled() noexcept;
/** */
const Result&	getCRC() noexcept;
/** */
const Result&	getBitStuffing() noexcept;
/** */
const Result&	getDataToggleMismatch() noexcept;
/** */
const Result&	getStall() noexcept;
/** */
const Result&	getDeviceNotResponding() noexcept;
/** */
const Result&	getPidCheckFailure() noexcept;
/** */
const Result&	getUnexpectedPID() noexcept;
/** */
const Result&	getDataOverrun() noexcept;
/** */
const Result&	getDataUnderrun() noexcept;
/** */
const Result&	getBufferOverrun() noexcept;
/** */
const Result&	getBufferUnderrun() noexcept;

}
}
}
}

#endif
