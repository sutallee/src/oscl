/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "reqapi.h"

using namespace Oscl::Usb::Pipe::Message;

Req::Api::CancelPayload::
CancelPayload(Oscl::Mt::Itc::SrvMsg& irpToCancel) noexcept:
		_irpToCancel(irpToCancel){
	}

Req::Api::NoDataPayload::
NoDataPayload(const Oscl::Usb::Setup::Packet& setupPkt) noexcept:
		_setupPkt(setupPkt),
		_failed(0)
		{
	}

const Oscl::Usb::Setup::Packet*
Req::Api::NoDataPayload::getSetupBuffer() const noexcept{
	return &_setupPkt;
	}

Req::Api::ReadPayload::ReadPayload(	const Oscl::Usb::Setup::Packet&	setupPkt,
									void*							buffer
									) noexcept:
		_setupPkt(setupPkt),
		_buffer(buffer),
		_failed(0)
		{
	}

const Oscl::Usb::Setup::Packet*
Req::Api::ReadPayload::getSetupBuffer() const noexcept{
	return &_setupPkt;
	}

void*	Req::Api::ReadPayload::getDataBuffer() noexcept{
	return _buffer;
	}
	
Req::Api::WritePayload::
WritePayload(	const Oscl::Usb::Setup::Packet&	setupPkt,
				const void*				writeData
				) noexcept:
		_setupPkt(setupPkt),
		_writeData(writeData),
		_failed(0)
		{
	}

const Oscl::Usb::Setup::Packet*
Req::Api::WritePayload::getSetupBuffer() const noexcept{
	return &_setupPkt;
	}

const void*	Req::Api::WritePayload::getWriteDataBuffer() const noexcept{
	return _writeData;
	}

