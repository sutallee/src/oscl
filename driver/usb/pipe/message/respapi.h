/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_usb_pipe_message_respapih_
#define _oscl_drv_usb_pipe_message_respapih_
#include "oscl/mt/itc/mbox/clirsp.h"
#include "reqapi.h"

/** */
namespace Oscl {
/** */
namespace Usb {
/** */
namespace Pipe {
/** */
namespace Message {
/** */
namespace Resp {

/** */
class Api {
	public:
		/** */
		typedef Oscl::Mt::Itc::CliResponse<	Req::Api,
											Api,
											Req::Api::CancelPayload
											>	CancelResp;
		/** */
		typedef Oscl::Mt::Itc::CliResponse<	Req::Api,
											Api,
											Req::Api::NoDataPayload
											>	NoDataResp;
		/** */
		typedef Oscl::Mt::Itc::CliResponse<	Req::Api,
											Api,
											Req::Api::ReadPayload
											>	ReadResp;
		/** */
		typedef Oscl::Mt::Itc::CliResponse<	Req::Api,
											Api,
											Req::Api::WritePayload
											>	WriteResp;

		/** */
		typedef union MsgPtr {
			/** */
			CancelResp*		cancel;
			/** */
			NoDataResp*		noData;
			/** */
			ReadResp*		read;
			/** */
			WriteResp*		write;
			} MsgPtr;

	public:
		/** Shut-up GCC. */
		virtual ~Api() {}
		/** */
		virtual void	response(CancelResp& msg) noexcept=0;
		/** */
		virtual void	response(NoDataResp& msg) noexcept=0;
		/** */
		virtual void	response(ReadResp& msg) noexcept=0;
		/** */
		virtual void	response(WriteResp& msg) noexcept=0;
	};

}
}
}
}
}

#endif
