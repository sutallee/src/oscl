/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_driver_usb_pipe_handlerh_
#define _oscl_driver_usb_pipe_handlerh_

/** */
namespace Oscl {
/** */
namespace Usb {
/** */
namespace Pipe {
/** */
namespace Status {


/** */
class Handler {
	public:
		/** Make GCC happy */
		virtual ~Handler() {}
		/** */
		virtual void	pipeClosed() noexcept=0;
		/** */
		virtual void	canceled() noexcept=0;
		/** */
		virtual void	crc() noexcept=0;
		/** */
		virtual void	bitStuffing() noexcept=0;
		/** */
		virtual void	dataToggleMismatch() noexcept=0;
		/** */
		virtual void	stall() noexcept=0;
		/** */
		virtual void	deviceNotResponding() noexcept=0;
		/** */
		virtual void	pidCheckFailure() noexcept=0;
		/** */
		virtual void	unexpectedPID() noexcept=0;
		/** */
		virtual void	dataOverrun() noexcept=0;
		/** */
		virtual void	dataUnderrun() noexcept=0;
		/** */
		virtual void	bufferOverrun() noexcept=0;
		/** */
		virtual void	bufferUnderrun() noexcept=0;
	};


}
}
}
}

#endif
