/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "info.h"
#include "oscl/error/info.h"

using namespace Oscl::Usb::Pipe::Status;

void	Info::pipeClosed() noexcept{
	Oscl::Error::Info::log("USB IRP: pipe closed\n");
	}

void	Info::canceled() noexcept{
	Oscl::Error::Info::log("USB IRP: canceled\n");
	}

void	Info::crc() noexcept{
	Oscl::Error::Info::log("USB IRP: CRC error\n");
	}

void	Info::bitStuffing() noexcept{
	Oscl::Error::Info::log("USB IRP: bit stuffing error\n");
	}

void	Info::dataToggleMismatch() noexcept{
	Oscl::Error::Info::log("USB IRP: data toggle mismatch\n");
	}

void	Info::stall() noexcept{
	Oscl::Error::Info::log("USB IRP: stall\n");
	}

void	Info::deviceNotResponding() noexcept{
	Oscl::Error::Info::log("USB IRP: device not responding\n");
	}

void	Info::pidCheckFailure() noexcept{
	Oscl::Error::Info::log("USB IRP: pid check failure\n");
	}

void	Info::unexpectedPID() noexcept{
	Oscl::Error::Info::log("USB IRP: unexpected PID\n");
	}

void	Info::dataOverrun() noexcept{
	Oscl::Error::Info::log("USB IRP: data overrun\n");
	}

void	Info::dataUnderrun() noexcept{
	Oscl::Error::Info::log("USB IRP: data underrun\n");
	}

void	Info::bufferOverrun() noexcept{
	Oscl::Error::Info::log("USB IRP: buffer overrun\n");
	}

void	Info::bufferUnderrun() noexcept{
	Oscl::Error::Info::log("USB IRP: buffer underrun\n");
	}

