/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "status.h"
#include "handler.h"

namespace Oscl {
namespace Usb {
namespace Pipe {
namespace Status {

class PipeClosed : public Result {
	public:
		void	query(Handler& handler) const noexcept;
	};

class Canceled : public Result {
	public:
		void	query(Handler& handler) const noexcept;
	};

class CRC : public Result {
	public:
		void	query(Handler& handler) const noexcept;
	};

class BitStuffing : public Result {
	public:
		void	query(Handler& handler) const noexcept;
	};

class DataToggleMismatch : public Result {
	public:
		void	query(Handler& handler) const noexcept;
	};

class Stall : public Result {
	public:
		void	query(Handler& handler) const noexcept;
	};

class DeviceNotResponding : public Result {
	public:
		void	query(Handler& handler) const noexcept;
	};

class PidCheckFailure : public Result {
	public:
		void	query(Handler& handler) const noexcept;
	};

class UnexpectedPID : public Result {
	public:
		void	query(Handler& handler) const noexcept;
	};

class DataOverrun : public Result {
	public:
		void	query(Handler& handler) const noexcept;
	};

class DataUnderrun : public Result {
	public:
		void	query(Handler& handler) const noexcept;
	};

class BufferOverrun : public Result {
	public:
		void	query(Handler& handler) const noexcept;
	};

class BufferUnderrun : public Result {
	public:
		void	query(Handler& handler) const noexcept;
	};

void	Canceled::query(Handler& handler) const noexcept{
	handler.canceled();
	}

void	PipeClosed::query(Handler& handler) const noexcept{
	handler.pipeClosed();
	}

void	CRC::query(Handler& handler) const noexcept{
	handler.crc();
	}

void	BitStuffing::query(Handler& handler) const noexcept{
	handler.bitStuffing();
	}

void	DataToggleMismatch::query(Handler& handler) const noexcept{
	handler.dataToggleMismatch();
	}

void	Stall::query(Handler& handler) const noexcept{
	handler.stall();
	}

void	DeviceNotResponding::query(Handler& handler) const noexcept{
	handler.deviceNotResponding();
	}

void	PidCheckFailure::query(Handler& handler) const noexcept{
	handler.pidCheckFailure();
	}

void	UnexpectedPID::query(Handler& handler) const noexcept{
	handler.unexpectedPID();
	}

void	DataOverrun::query(Handler& handler) const noexcept{
	handler.dataOverrun();
	}

void	DataUnderrun::query(Handler& handler) const noexcept{
	handler.dataUnderrun();
	}

void	BufferOverrun::query(Handler& handler) const noexcept{
	handler.bufferOverrun();
	}

void	BufferUnderrun::query(Handler& handler) const noexcept{
	handler.bufferUnderrun();
	}

const Result&	getCanceled() noexcept{
	static const Canceled		canceled;
	return canceled;
	}

const Result&	getPipeClosed() noexcept{
	static const PipeClosed		pipeClosed;
	return pipeClosed;
	}

const Result&	getCRC() noexcept{
	static const CRC		crc;
	return crc;
	}

const Result&	getBitStuffing() noexcept{
	static const BitStuffing	bitStuffing;
	return bitStuffing;
	}

const Result&	getDataToggleMismatch() noexcept{
	static const DataToggleMismatch	dataToggleMismatch;
	return dataToggleMismatch;
	}

const Result&	getStall() noexcept{
	static const Stall	stall;
	return stall;
	}

const Result&	getDeviceNotResponding() noexcept{
	static const DeviceNotResponding	deviceNotResponding;
	return deviceNotResponding;
	}

const Result&	getPidCheckFailure() noexcept{
	static const PidCheckFailure	pidCheckFailure;
	return pidCheckFailure;
	}

const Result&	getUnexpectedPID() noexcept{
	static const UnexpectedPID	unexpectedPID;
	return unexpectedPID;
	}

const Result&	getDataOverrun() noexcept{
	static const DataOverrun	dataOverrun;
	return dataOverrun;
	}

const Result&	getDataUnderrun() noexcept{
	static const DataUnderrun	dataUnderrun;
	return dataUnderrun;
	}

const Result&	getBufferOverrun() noexcept{
	static const BufferOverrun	bufferOverrun;
	return bufferOverrun;
	}

const Result&	getBufferUnderrun() noexcept{
	static const BufferUnderrun	bufferUnderrun;
	return bufferUnderrun;
	}

}
}
}
}

