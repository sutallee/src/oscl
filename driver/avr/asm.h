/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_avr_asmh_
#define _oscl_drv_avr_asmh_

/** */
namespace Oscl {

/** */
namespace Avr {

// OEA user
inline bool getGiFlag() noexcept{
	bool	flag;
	asm volatile(	" brid	1f\n"
					" clr	%0\n"
					" brid	2f\n"
					"1: ldi %0,1\n"
					"2: \n"
					: "=r" (flag)
					);
	return flag;
	}

inline void	cli() noexcept{
	asm volatile(" cli\n");
	}

inline void	sei() noexcept{
	asm volatile(" sei\n");
	}

//inline void mtdec(unsigned long value) noexcept{
//	asm volatile("mtdec %0" : : "r" (value));
//	}

// OEA user
//inline unsigned long mfdec() noexcept{
//	unsigned long	reg;
//	asm volatile("mfdec %0" : "=r" (reg));
//	return reg;
//	}

//inline void	eieio() noexcept{
//	asm volatile ("eieio" : : : "memory");	// memory?
//	}

//inline void	tlbie(void* address) noexcept{
//	asm volatile ("tlbie %0" : : "r" (address));
//	}

//inline void	rfi() noexcept{
//	asm volatile ("rfi" : : );
//	}

// UISA	user
// Sets reservation for the "address" within the processor specific
// granularity (typically cache line). Some implementations only allow
// a single oustanding reservation per processor.
//inline unsigned long	lwarx(volatile unsigned long *address) noexcept{
//	unsigned long	result;
//	asm volatile (	"lwarx %0,0,%1\n"
//					: "=r" (result)
//					: "r" (address)
//					);
//	return result;
//	}

// UISA	user
// This operation returns true if the conditional store succeeds.
// The store will succeed if no other processor/procedure has
// stored to the cache line (granularity) associated with "address".
//inline bool	stwcx(volatile unsigned long* address,unsigned long value) noexcept{
//	unsigned long	result;
//	asm volatile ("stwcx. %2,0,%1; beq 0f; li %0,0; b 1f;0:; li %0,1;1:"
//					: "=r" (result)
//					: "r" (address), "r" (value)
//					: "cc"
//					);
//	return result;
//	}

// UISA	user
//inline unsigned long	mulhwu(	unsigned long	multiplicand,
//								unsigned long	multiplier
//								) noexcept{
//	unsigned long	result;
//	asm volatile (	"mulhwu %0,%1,%2\n"
//					: "=r" (result)
//					: "r" (multiplicand), "r" (multiplier)
//					);
//	return result;
//	}

// UISA	user
//inline unsigned long	mullw(	unsigned long	multiplicand,
//								unsigned long	multiplier
//								) noexcept{
//	unsigned long	result;
//	asm volatile (	"mullw %0,%1,%2\n"
//					: "=r" (result)
//					: "r" (multiplicand), "r" (multiplier)
//					);
//	return result;
//	}

}
}

#endif
