/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "qspan.h"

using namespace Oscl::Tundra::Qspan;

Driver::Driver(	Oscl::Tundra::Qspan::RegMap&	device,
				unsigned char					busNumber
				) noexcept:
		_device(device),
		_busNumber(busNumber)
		{
	}

bool	Driver::readConfigSpaceDW(	uint32_t&		data,
									unsigned char	bus,
									unsigned char	device,
									unsigned char	function,
									unsigned char	dwNum
									) noexcept{
	_device.MISC_CTL		|= MiscControlStatus::MA_BE_D::ValueMask_Pci2_1;
	_device.CON_ADD	=		(bus<<ConfigAddress::BUS_NUM::Lsb)
						|	(device<<ConfigAddress::DEV_NUM::Lsb)
						|	(function<<ConfigAddress::FUNC_NUM::Lsb)
						|	(dwNum<<ConfigAddress::REG_NUM::Lsb)
						|	((_busNumber!=bus)<<ConfigAddress::TYPE::Lsb)
						;
	catchBusError();
	data	= _device.CON_DATA;
	clearReceivedMasterAbort();
	_device.MISC_CTL		&= ~MiscControlStatus::MA_BE_D::FieldMask;
	return !caughtBusError();
	}

bool	Driver::writeConfigSpaceDW(	uint32_t		data,
									unsigned char	bus,
									unsigned char	device,
									unsigned char	function,
									unsigned char	dwNum
									) noexcept{
	_device.MISC_CTL		|= MiscControlStatus::MA_BE_D::ValueMask_Pci2_1;
	_device.CON_ADD	=		(bus<<ConfigAddress::BUS_NUM::Lsb)
						|	(device<<ConfigAddress::DEV_NUM::Lsb)
						|	(function<<ConfigAddress::FUNC_NUM::Lsb)
						|	(dwNum<<ConfigAddress::REG_NUM::Lsb)
						|	((_busNumber!=bus)<<ConfigAddress::TYPE::Lsb)
						;
	catchBusError();
	_device.CON_DATA	= data;
	clearReceivedMasterAbort();
	_device.MISC_CTL		&= ~MiscControlStatus::MA_BE_D::FieldMask;
	return !caughtBusError();
	}

void	Driver::clearReceivedMasterAbort() noexcept{
	ConfigSpaceControlStatus::Reg	pci_cs;
	pci_cs	= _device.PCI_CS;
	pci_cs	&=	~(		ConfigSpaceControlStatus::D_PE::FieldMask
					|	ConfigSpaceControlStatus::S_SERR::FieldMask
					|	ConfigSpaceControlStatus::R_MA::FieldMask
					|	ConfigSpaceControlStatus::R_TA::FieldMask
					|	ConfigSpaceControlStatus::S_TA::FieldMask
					|	ConfigSpaceControlStatus::DP_D::FieldMask
					);
	pci_cs	|= ConfigSpaceControlStatus::R_MA::ValueMask_Clear;
	_device.PCI_CS	= pci_cs;
	}

void	Driver::clearReceivedTargetAbort() noexcept{
	ConfigSpaceControlStatus::Reg	pci_cs;
	pci_cs	= _device.PCI_CS;
	pci_cs	&=	~(		ConfigSpaceControlStatus::D_PE::FieldMask
					|	ConfigSpaceControlStatus::S_SERR::FieldMask
					|	ConfigSpaceControlStatus::R_MA::FieldMask
					|	ConfigSpaceControlStatus::R_TA::FieldMask
					|	ConfigSpaceControlStatus::S_TA::FieldMask
					|	ConfigSpaceControlStatus::DP_D::FieldMask
					);
	pci_cs	|= ConfigSpaceControlStatus::R_TA::ValueMask_Clear;
	_device.PCI_CS	= pci_cs;
	}

