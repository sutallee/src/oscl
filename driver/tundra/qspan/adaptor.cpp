/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "adaptor.h"

using namespace Oscl::Tundra::Qspan;
using namespace Oscl;

PciAdaptor::PciAdaptor(Driver& qSpanDriver) noexcept:
		_qSpanDriver(qSpanDriver)
		{
	}

uint32_t	PciAdaptor::readDword(	unsigned char	bus,
									unsigned char	device,
									unsigned char	function,
									unsigned		dword
									) noexcept{
	uint32_t	result;
	using namespace Oscl::Tundra::Qspan;
	if(device > ConfigAddress::DEV_NUM::Value_LastDev){
		// QSPAN only supports 16 instead of the PCI specified
		// 32 devices. Thus, we return all ones as if the
		// transaction were completed but the device were
		// not present.
		return ~0;
		}
	_qSpanDriver.readConfigSpaceDW(	result,
									bus,
									device,
									function,
									dword
									);
	return result;
	}

void		PciAdaptor::writeDword(	unsigned char	bus,
									unsigned char	device,
									unsigned char	function,
									unsigned		dword,
									uint32_t		data
									) noexcept{
	_qSpanDriver.writeConfigSpaceDW(	data,
										bus,
										device,
										function,
										dword
										);
	}

