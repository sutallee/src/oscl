/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_driver_tundra_qspan_qspanh_
#define _oscl_driver_tundra_qspan_qspanh_
#include "oscl/hw/tundra/qspan/qspan.h"

/** */
namespace Oscl {
/** */
namespace Tundra {
/** */
namespace Qspan {

/** */
class Driver {
	private:
		/** */
		Oscl::Tundra::Qspan::RegMap&	_device;
		/** */
		const unsigned char				_busNumber;
	public:
		/** */
		Driver(	Oscl::Tundra::Qspan::RegMap&	device,
				unsigned char					busNumber
				) noexcept;
	public:
		/** */
		bool 	readConfigSpaceDW(	uint32_t&		data,
									unsigned char	bus,
									unsigned char	device,
									unsigned char	function,
									unsigned char	reg
									) noexcept;
		/** */
		bool 	writeConfigSpaceDW(	uint32_t		data,
									unsigned char	bus,
									unsigned char	device,
									unsigned char	function,
									unsigned char	reg
									) noexcept;
		/** */
		bool	readVendorID(	uint16_t&		vendorID,
								unsigned char	bus,
								unsigned char	device,
								unsigned char	function
								) noexcept;
		/** */
		bool	readDeviceID(	uint16_t&		deviceID,
								unsigned char	bus,
								unsigned char	device,
								unsigned char	function
								) noexcept;
		/** */
		bool	readStatusRegister(	uint16_t&		statusRegister,
									unsigned char	bus,
									unsigned char	device,
									unsigned char	function
									) noexcept;
		/** */
		void	clearReceivedMasterAbort() noexcept;
		/** */
		void	clearReceivedTargetAbort() noexcept;

	public:
		/** */
		static void	catchBusError() noexcept;
		/** */
		static bool	caughtBusError() noexcept;
	};

}
}
}

#endif
