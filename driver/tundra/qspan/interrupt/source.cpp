/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "source.h"

Oscl::Tundra::Qspan::Irq::PciIntSource::
PciIntSource(ControlDriver& ctrlDrv) noexcept:
		_source(*this),
		_ctrlDrv(ctrlDrv)
		{
	}

Oscl::Interrupt::Shared::SourceApi&
Oscl::Tundra::Qspan::Irq::PciIntSource::getSourceApi() noexcept{
	return _source;
	}

void	Oscl::Tundra::Qspan::Irq::PciIntSource::listIsNowEmpty() noexcept{
	_ctrlDrv.disablePciInterrupt();
	}

void	Oscl::Tundra::Qspan::Irq::PciIntSource::listIsNowNotEmpty() noexcept{
	_ctrlDrv.enablePciInterrupt();
	}

void	Oscl::Tundra::Qspan::Irq::PciIntSource::interrupt() noexcept{
	if(_source.interrupt()) return;
	while(true); // Should not spurrious interrupts
	}

