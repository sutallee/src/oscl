/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_driver_tundra_qspan_interrupt_dispatcherh_
#define _oscl_driver_tundra_qspan_interrupt_dispatcherh_
#include "stat.h"
#include "oscl/interrupt/handler.h"
#include "api.h"

/** */
namespace Oscl {
/** */
namespace Tundra {
/** */
namespace Qspan {
/** */
namespace Irq {

/** */
class Dispatcher : public Oscl::Interrupt::HandlerApi, public Api {
	private:
		/** */
		StatusDriver					_status;
		/** */
		Oscl::Interrupt::HandlerApi*	_pciErrorLog;
		/** */
		Oscl::Interrupt::HandlerApi*	_qBusErrorLog;
		/** */
		Oscl::Interrupt::HandlerApi*	_dataParityDetected;
		/** */
		Oscl::Interrupt::HandlerApi*	_idmaQBusError;
		/** */
		Oscl::Interrupt::HandlerApi*	_idmaPciError;
		/** */
		Oscl::Interrupt::HandlerApi*	_idmaReset;
		/** */
		Oscl::Interrupt::HandlerApi*	_idmaDone;
		/** */
		Oscl::Interrupt::HandlerApi*	_pciInt;
		/** */
		Oscl::Interrupt::HandlerApi*	_pciBusPerr;
		/** */
		Oscl::Interrupt::HandlerApi*	_pciBusSerr;
		/** */
		Oscl::Interrupt::HandlerApi*	_qBusInt;
		/** */
		Oscl::Interrupt::HandlerApi*	_software3;
		/** */
		Oscl::Interrupt::HandlerApi*	_software2;
		/** */
		Oscl::Interrupt::HandlerApi*	_software1;
		/** */
		Oscl::Interrupt::HandlerApi*	_software0;
	public:
		/** */
		Dispatcher(	volatile Oscl::Tundra::Qspan::
					InterruptStatus::Reg&			status,
					Oscl::Interrupt::HandlerApi&	pciErrorLog,
					Oscl::Interrupt::HandlerApi&	qBusErrorLog,
					Oscl::Interrupt::HandlerApi&	dataParityDetected,
					Oscl::Interrupt::HandlerApi&	idmaQBusError,
					Oscl::Interrupt::HandlerApi&	idmaPciError,
					Oscl::Interrupt::HandlerApi&	idmaReset,
					Oscl::Interrupt::HandlerApi&	idmaDone,
					Oscl::Interrupt::HandlerApi&	pciInt,
					Oscl::Interrupt::HandlerApi&	pciBusPerr,
					Oscl::Interrupt::HandlerApi&	pciBusSerr,
					Oscl::Interrupt::HandlerApi&	qBusInt,
					Oscl::Interrupt::HandlerApi&	software3,
					Oscl::Interrupt::HandlerApi&	software2,
					Oscl::Interrupt::HandlerApi&	software1,
					Oscl::Interrupt::HandlerApi&	software0
					) noexcept;
		/** */
		Dispatcher(	volatile Oscl::Tundra::Qspan::
					InterruptStatus::Reg&			status
					) noexcept;
	public:
		void	interrupt() noexcept;

	private: // Api
		/** */
		void	reservePciBusErrorLog(	Oscl::Interrupt::
										HandlerApi&		handler
										) noexcept;
		/** */
		void	releasePciBusErrorLog() noexcept;
		/** */
		void	reserveQBusErrorLog(	Oscl::Interrupt::
										HandlerApi&		handler
										) noexcept;
		/** */
		void	releaseQBusErrorLog() noexcept;
		/** */
		void	reserveDataParity(	Oscl::Interrupt::
									HandlerApi&		handler
									) noexcept;
		/** */
		void	releaseDataParity() noexcept;
		/** */
		void	reserveIdmaQBusError(	Oscl::Interrupt::
										HandlerApi&		handler
										) noexcept;
		/** */
		void	releaseIdmaQBusError() noexcept;
		/** */
		void	reserveIdmaPciError(	Oscl::Interrupt::
										HandlerApi&		handler
										) noexcept;
		/** */
		void	releaseIdmaPciError() noexcept;
		/** */
		void	reserveIdmaReset(	Oscl::Interrupt::
									HandlerApi&		handler
									) noexcept;
		/** */
		void	releaseIdmaReset() noexcept;
		/** */
		void	reserveIdmaDone(	Oscl::Interrupt::
									HandlerApi&		handler
									) noexcept;
		/** */
		void	releaseIdmaDone() noexcept;
		/** */
		void	reservePciInt(	Oscl::Interrupt::
								HandlerApi&		handler
								) noexcept;
		/** */
		void	releasePciInt() noexcept;
		/** */
		void	reservePciBusPerr(	Oscl::Interrupt::
									HandlerApi&		handler
									) noexcept;
		/** */
		void	releasePciBusPerr() noexcept;
		/** */
		void	reservePciBusSerr(	Oscl::Interrupt::
									HandlerApi&		handler
									) noexcept;
		/** */
		void	releasePciBusSerr() noexcept;
		/** */
		void	reserveQBusInt(	Oscl::Interrupt::
								HandlerApi&		handler
								) noexcept;
		/** */
		void	releaseQBusInt() noexcept;
		/** */
		void	reserveSoftware3(	Oscl::Interrupt::
									HandlerApi&		handler
									) noexcept;
		/** */
		void	releaseSoftware3() noexcept;
		/** */
		void	reserveSoftware2(	Oscl::Interrupt::
									HandlerApi&		handler
									) noexcept;
		/** */
		void	releaseSoftware2() noexcept;
		/** */
		void	reserveSoftware1(	Oscl::Interrupt::
									HandlerApi&		handler
									) noexcept;
		/** */
		void	releaseSoftware1() noexcept;
		/** */
		void	reserveSoftware0(	Oscl::Interrupt::
									HandlerApi&		handler
									) noexcept;
		/** */
		void	releaseSoftware0() noexcept;
	};

}
}
}
}

#endif
