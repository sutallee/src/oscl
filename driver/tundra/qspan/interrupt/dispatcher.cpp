/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <new>
#include "dispatcher.h"
#include "oscl/memory/block.h"
#include "oscl/error/fatal.h"

/** */
namespace Oscl {
/** */
namespace Tundra {
/** */
namespace Qspan {
/** */
namespace Irq {
/** */
class NullHandler : public Oscl::Interrupt::HandlerApi {
	private:
		/** */
		NullHandler() noexcept;
	private:	// Oscl::Interrupt::HandlerApi
		/** */
		void	interrupt() noexcept;
	public:
		/** Singleton operation */
		static Oscl::Interrupt::HandlerApi*	handler;
		/** Singleton operation */
		static Oscl::Interrupt::HandlerApi&	getNull() noexcept;
	};
}
}
}
}

using namespace Oscl::Tundra::Qspan::Irq;

Oscl::Interrupt::HandlerApi*	NullHandler::handler;

static Oscl::Memory::
AlignedBlock<sizeof(NullHandler)>	nullHandlerMem;

Oscl::Interrupt::HandlerApi&	NullHandler::getNull() noexcept{
	if(!NullHandler::handler){
		NullHandler::handler	= new(&nullHandlerMem)
									NullHandler();
		}
	return *handler;
	}

NullHandler::NullHandler() noexcept{
	}

void NullHandler::interrupt() noexcept{
	Oscl::ErrorFatal::logAndExit("Interrupt: unreserved vector used.\n");
	}

Dispatcher::Dispatcher(
		volatile Oscl::Tundra::Qspan::
		InterruptStatus::Reg&			status,
		Oscl::Interrupt::HandlerApi&	pciErrorLog,
		Oscl::Interrupt::HandlerApi&	qBusErrorLog,
		Oscl::Interrupt::HandlerApi&	dataParityDetected,
		Oscl::Interrupt::HandlerApi&	idmaQBusError,
		Oscl::Interrupt::HandlerApi&	idmaPciError,
		Oscl::Interrupt::HandlerApi&	idmaReset,
		Oscl::Interrupt::HandlerApi&	idmaDone,
		Oscl::Interrupt::HandlerApi&	pciInt,
		Oscl::Interrupt::HandlerApi&	pciBusPerr,
		Oscl::Interrupt::HandlerApi&	pciBusSerr,
		Oscl::Interrupt::HandlerApi&	qBusInt,
		Oscl::Interrupt::HandlerApi&	software3,
		Oscl::Interrupt::HandlerApi&	software2,
		Oscl::Interrupt::HandlerApi&	software1,
		Oscl::Interrupt::HandlerApi&	software0
		) noexcept:
		_status(status),
		_pciErrorLog(&pciErrorLog),
		_qBusErrorLog(&qBusErrorLog),
		_dataParityDetected(&dataParityDetected),
		_idmaQBusError(&idmaQBusError),
		_idmaPciError(&idmaPciError),
		_idmaReset(&idmaReset),
		_idmaDone(&idmaDone),
		_pciInt(&pciInt),
		_pciBusPerr(&pciBusPerr),
		_pciBusSerr(&pciBusSerr),
		_qBusInt(&qBusInt),
		_software3(&software3),
		_software2(&software2),
		_software1(&software1),
		_software0(&software0)
		{
	}

Dispatcher::Dispatcher( volatile Oscl::Tundra::Qspan::
						InterruptStatus::Reg&			status
						) noexcept:
		_status(status),
		_pciErrorLog(&NullHandler::getNull()),
		_qBusErrorLog(&NullHandler::getNull()),
		_dataParityDetected(&NullHandler::getNull()),
		_idmaQBusError(&NullHandler::getNull()),
		_idmaPciError(&NullHandler::getNull()),
		_idmaReset(&NullHandler::getNull()),
		_idmaDone(&NullHandler::getNull()),
		_pciInt(&NullHandler::getNull()),
		_pciBusPerr(&NullHandler::getNull()),
		_pciBusSerr(&NullHandler::getNull()),
		_qBusInt(&NullHandler::getNull()),
		_software3(&NullHandler::getNull()),
		_software2(&NullHandler::getNull()),
		_software1(&NullHandler::getNull()),
		_software0(&NullHandler::getNull())
		{
	}

void	Dispatcher::interrupt() noexcept{
	_status.update();
	if(!_status.anyPending()) return;
	if(_status.pciBusErrorLogInterruptPending()){
		_pciErrorLog->interrupt();
		_status.clearPciBusErrorLogInterrupt();
		}
	if(_status.qBusErrorLogInterruptPending()){
		_qBusErrorLog->interrupt();
		_status.clearQBusErrorLogInterrupt();
		}
	if(_status.dataParityDetectedInterruptPending()){
		_dataParityDetected->interrupt();
		_status.clearDataParityDetectedInterrupt();
		}
	if(_status.idmaQBusErrorInterruptPending()){
		_idmaQBusError->interrupt();
		_status.clearIdmaQBusErrorLogInterrupt();
		}
	if(_status.idmaPciErrorInterruptPending()){
		_idmaPciError->interrupt();
		_status.clearIdmaPciErrorInterrupt();
		}
	if(_status.idmaResetInterruptPending()){
		_idmaReset->interrupt();
		_status.clearIdmaResetInterrupt();
		}
	if(_status.idmaDoneInterruptPending()){
		_idmaDone->interrupt();
		_status.clearIdmaDoneInterrupt();
		}
	if(_status.pciInterruptPending()){
		_pciInt->interrupt();
		_status.clearPciInterrupt();
		}
	if(_status.pciBusSerrInterruptPending()){
		_pciBusSerr->interrupt();
		_status.clearPciBusSerrInterrupt();
		}
	if(_status.qBusInterruptPending()){
		_qBusInt->interrupt();
		_status.clearQBusInterrupt();
		}
	if(_status.software3InterruptPending()){
		_software3->interrupt();
		_status.clearSoftware3Interrupt();
		}
	if(_status.software2InterruptPending()){
		_software2->interrupt();
		_status.clearSoftware2Interrupt();
		}
	if(_status.software1InterruptPending()){
		_software1->interrupt();
		_status.clearSoftware1Interrupt();
		}
	if(_status.software0InterruptPending()){
		_software0->interrupt();
		_status.clearSoftware0Interrupt();
		}
	}

void	Dispatcher::reservePciBusErrorLog(	Oscl::Interrupt::
											HandlerApi&		handler
											) noexcept{
	if(_pciErrorLog != &NullHandler::getNull()){
		Oscl::ErrorFatal::logAndExit("Interrupt: vector already reserved.\n");
		}
	_pciErrorLog	= &handler;
	}

void	Dispatcher::releasePciBusErrorLog() noexcept{
	_pciErrorLog	= &NullHandler::getNull();
	}

void	Dispatcher::reserveQBusErrorLog(	Oscl::Interrupt::
											HandlerApi&		handler
											) noexcept{
	if(_qBusErrorLog != &NullHandler::getNull()){
		Oscl::ErrorFatal::logAndExit("Interrupt: vector already reserved.\n");
		}
	_qBusErrorLog	= &handler;
	}

void	Dispatcher::releaseQBusErrorLog() noexcept{
	_qBusErrorLog	= &NullHandler::getNull();
	}

void	Dispatcher::reserveDataParity(	Oscl::Interrupt::
										HandlerApi&		handler
										) noexcept{
	if(_dataParityDetected != &NullHandler::getNull()){
		Oscl::ErrorFatal::logAndExit("Interrupt: vector already reserved.\n");
		}
	_dataParityDetected	= &handler;
	}

void	Dispatcher::releaseDataParity() noexcept{
	_dataParityDetected	= &NullHandler::getNull();
	}

void	Dispatcher::reserveIdmaQBusError(	Oscl::Interrupt::
											HandlerApi&		handler
											) noexcept{
	if(_idmaQBusError != &NullHandler::getNull()){
		Oscl::ErrorFatal::logAndExit("Interrupt: vector already reserved.\n");
		}
	_idmaQBusError	= &handler;
	}

void	Dispatcher::releaseIdmaQBusError() noexcept{
	_idmaQBusError	= &NullHandler::getNull();
	}

void	Dispatcher::reserveIdmaPciError(	Oscl::Interrupt::
											HandlerApi&		handler
											) noexcept{
	if(_idmaPciError != &NullHandler::getNull()){
		Oscl::ErrorFatal::logAndExit("Interrupt: vector already reserved.\n");
		}
	_idmaPciError	= &handler;
	}

void	Dispatcher::releaseIdmaPciError() noexcept{
	_idmaPciError	= &NullHandler::getNull();
	}

void	Dispatcher::reserveIdmaReset(	Oscl::Interrupt::
										HandlerApi&		handler
										) noexcept{
	if(_idmaReset != &NullHandler::getNull()){
		Oscl::ErrorFatal::logAndExit("Interrupt: vector already reserved.\n");
		}
	_idmaReset	= &handler;
	}

void	Dispatcher::releaseIdmaReset() noexcept{
	_idmaReset	= &NullHandler::getNull();
	}

void	Dispatcher::reserveIdmaDone(	Oscl::Interrupt::
										HandlerApi&		handler
										) noexcept{
	if(_idmaDone != &NullHandler::getNull()){
		Oscl::ErrorFatal::logAndExit("Interrupt: vector already reserved.\n");
		}
	_idmaDone	= &handler;
	}

void	Dispatcher::releaseIdmaDone() noexcept{
	_idmaDone	= &NullHandler::getNull();
	}

void	Dispatcher::reservePciInt(	Oscl::Interrupt::
									HandlerApi&		handler
									) noexcept{
	if(_pciInt != &NullHandler::getNull()){
		Oscl::ErrorFatal::logAndExit("Interrupt: vector already reserved.\n");
		}
	_pciInt	= &handler;
	}

void	Dispatcher::releasePciInt() noexcept{
	_pciInt	= &NullHandler::getNull();
	}

void	Dispatcher::reservePciBusPerr(	Oscl::Interrupt::
										HandlerApi&		handler
										) noexcept{
	if(_pciBusPerr != &NullHandler::getNull()){
		Oscl::ErrorFatal::logAndExit("Interrupt: vector already reserved.\n");
		}
	_pciBusPerr	= &handler;
	}

void	Dispatcher::releasePciBusPerr() noexcept{
	_pciBusPerr	= &NullHandler::getNull();
	}

void	Dispatcher::reservePciBusSerr(	Oscl::Interrupt::
										HandlerApi&		handler
										) noexcept{
	if(_pciBusSerr != &NullHandler::getNull()){
		Oscl::ErrorFatal::logAndExit("Interrupt: vector already reserved.\n");
		}
	_pciBusSerr	= &handler;
	}

void	Dispatcher::releasePciBusSerr() noexcept{
	_pciBusSerr	= &NullHandler::getNull();
	}

void	Dispatcher::reserveQBusInt(	Oscl::Interrupt::
									HandlerApi&		handler
									) noexcept{
	if(_qBusInt != &NullHandler::getNull()){
		Oscl::ErrorFatal::logAndExit("Interrupt: vector already reserved.\n");
		}
	_qBusInt	= &handler;
	}

void	Dispatcher::releaseQBusInt() noexcept{
	_qBusInt	= &NullHandler::getNull();
	}

void	Dispatcher::reserveSoftware3(	Oscl::Interrupt::
										HandlerApi&		handler
										) noexcept{
	if(_software3 != &NullHandler::getNull()){
		Oscl::ErrorFatal::logAndExit("Interrupt: vector already reserved.\n");
		}
	_software3	= &handler;
	}

void	Dispatcher::releaseSoftware3() noexcept{
	_software3	= &NullHandler::getNull();
	}

void	Dispatcher::reserveSoftware2(	Oscl::Interrupt::
										HandlerApi&		handler
										) noexcept{
	if(_software2 != &NullHandler::getNull()){
		Oscl::ErrorFatal::logAndExit("Interrupt: vector already reserved.\n");
		}
	_software2	= &handler;
	}

void	Dispatcher::releaseSoftware2() noexcept{
	_software2	= &NullHandler::getNull();
	}

void	Dispatcher::reserveSoftware1(	Oscl::Interrupt::
										HandlerApi&		handler
										) noexcept{
	if(_software1 != &NullHandler::getNull()){
		Oscl::ErrorFatal::logAndExit("Interrupt: vector already reserved.\n");
		}
	_software1	= &handler;
	}

void	Dispatcher::releaseSoftware1() noexcept{
	_software1	= &NullHandler::getNull();
	}

void	Dispatcher::reserveSoftware0(	Oscl::Interrupt::
										HandlerApi&		handler
										) noexcept{
	if(_software0 != &NullHandler::getNull()){
		Oscl::ErrorFatal::logAndExit("Interrupt: vector already reserved.\n");
		}
	_software0	= &handler;
	}

void	Dispatcher::releaseSoftware0() noexcept{
	_software0	= &NullHandler::getNull();
	}

