/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_driver_tundra_qspan_interrupt_ctrlh_
#define _oscl_driver_tundra_qspan_interrupt_ctrlh_
#include "oscl/hw/tundra/qspan/fields.h"

/** */
namespace Oscl {
/** */
namespace Tundra {
/** */
namespace Qspan {
/** */
namespace Irq {

/** */
class ControlDriver {
	private:
		/** */
		volatile Oscl::Tundra::Qspan::InterruptControl::Reg&	_control;
	public:
		/** */
		ControlDriver(
			volatile Oscl::Tundra::Qspan::InterruptControl::Reg& control
			) noexcept:_control(control){}
	public:
		/** */
		inline void	enablePciBusErrorLogInterrupt() noexcept{
			_control |= Oscl::Tundra::Qspan::InterruptControl::PEL::ValueMask_Enable;
			}
		/** */
		inline void	disablePciBusErrorLogInterrupt() noexcept{
			_control &= ~Oscl::Tundra::Qspan::InterruptControl::PEL::ValueMask_Enable;
			}
		/** */
		inline void	enableQBusErrorLogInterrupt() noexcept{
			_control |= Oscl::Tundra::Qspan::InterruptControl::QEL::ValueMask_Enable;
			}
		/** */
		inline void	disableQBusErrorLogInterrupt() noexcept{
			_control &= ~Oscl::Tundra::Qspan::InterruptControl::QEL::ValueMask_Enable;
			}
		/** */
		inline void	enableDataParityDetectedInterrupt() noexcept{
			_control |= Oscl::Tundra::Qspan::InterruptControl::DPD::ValueMask_Enable;
			}
		/** */
		inline void	disableDataParityDetectedInterrupt() noexcept{
			_control &= ~Oscl::Tundra::Qspan::InterruptControl::DPD::ValueMask_Enable;
			}
		/** */
		inline void	enableIdmaQBusErrorInterrupt() noexcept{
			_control |= Oscl::Tundra::Qspan::InterruptControl::IQE::ValueMask_Enable;
			}
		/** */
		inline void	disableIdmaQBusErrorInterrupt() noexcept{
			_control &= ~Oscl::Tundra::Qspan::InterruptControl::IQE::ValueMask_Enable;
			}
		/** */
		inline void	enableIdmaPciErrorInterrupt() noexcept{
			_control |= Oscl::Tundra::Qspan::InterruptControl::IPE::ValueMask_Enable;
			}
		/** */
		inline void	disableIdmaPciErrorInterrupt() noexcept{
			_control &= ~Oscl::Tundra::Qspan::InterruptControl::IPE::ValueMask_Enable;
			}
		/** */
		inline void	enableIdmaResetInterrupt() noexcept{
			_control |= Oscl::Tundra::Qspan::InterruptControl::IRST::ValueMask_Enable;
			}
		/** */
		inline void	disableIdmaResetInterrupt() noexcept{
			_control &= ~Oscl::Tundra::Qspan::InterruptControl::IRST::ValueMask_Enable;
			}
		/** */
		inline void	enableIdmaDoneInterrupt() noexcept{
			_control |= Oscl::Tundra::Qspan::InterruptControl::DONE::ValueMask_Enable;
			}
		/** */
		inline void	disableIdmaDoneInterrupt() noexcept{
			_control &= ~Oscl::Tundra::Qspan::InterruptControl::DONE::ValueMask_Enable;
			}
		/** */
		inline void	enablePciInterrupt() noexcept{
			_control |= Oscl::Tundra::Qspan::InterruptControl::INT::ValueMask_Enable;
			}
		/** */
		inline void	disablePciInterrupt() noexcept{
			_control &= ~Oscl::Tundra::Qspan::InterruptControl::INT::ValueMask_Enable;
			}
		/** */
		inline void	enablePciPerrInterrupt() noexcept{
			_control |= Oscl::Tundra::Qspan::InterruptControl::PERR::ValueMask_Enable;
			}
		/** */
		inline void	disablePciPerrInterrupt() noexcept{
			_control &= ~Oscl::Tundra::Qspan::InterruptControl::PERR::ValueMask_Enable;
			}
		/** */
		inline void	enablePciSerrInterrupt() noexcept{
			_control |= Oscl::Tundra::Qspan::InterruptControl::SERR::ValueMask_Enable;
			}
		/** */
		inline void	disablePciSerrInterrupt() noexcept{
			_control &= ~Oscl::Tundra::Qspan::InterruptControl::SERR::ValueMask_Enable;
			}
		/** */
		inline void	enableQBusInterrupt() noexcept{
			_control |= Oscl::Tundra::Qspan::InterruptControl::QINT::ValueMask_Enable;
			}
		/** */
		inline void	disableQBusInterrupt() noexcept{
			_control &= ~Oscl::Tundra::Qspan::InterruptControl::QINT::ValueMask_Enable;
			}
		/** */
		inline void	enableSoftware1Interrupt() noexcept{
			_control |= Oscl::Tundra::Qspan::InterruptControl::SI1::ValueMask_Enable;
			}
		/** */
		inline void	disableSoftware1Interrupt() noexcept{
			_control &= ~Oscl::Tundra::Qspan::InterruptControl::SI1::ValueMask_Enable;
			}
		/** */
		inline void	enableSoftware0Interrupt() noexcept{
			_control |= Oscl::Tundra::Qspan::InterruptControl::SI0::ValueMask_Enable;
			}
		/** */
		inline void	disableSoftware0Interrupt() noexcept{
			_control &= ~Oscl::Tundra::Qspan::InterruptControl::SI0::ValueMask_Enable;
			}
	};

}
}
}
}

#endif
