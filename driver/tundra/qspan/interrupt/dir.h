/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_driver_tundra_qspan_interrupt_dirh_
#define _oscl_driver_tundra_qspan_interrupt_dirh_
#include "oscl/hw/tundra/qspan/fields.h"

/** */
namespace Oscl {
/** */
namespace Tundra {
/** */
namespace Qspan {
/** */
namespace Irq {

/** */
class DirectionDriver {
	private:
		/** */
		volatile InterruptDirectionControl::Reg&	_control;
	public:
		/** */
		DirectionDriver(
			volatile InterruptDirectionControl::Reg& control
			) noexcept:_control(control){}
	public:
		/** */
		inline void	pciBusErrorLogInterruptToPciBus() noexcept{
			_control =	(		_control
							&	~InterruptDirectionControl::PEL::FieldMask
							)
						| InterruptDirectionControl::PEL::ValueMask_ToPciBus;
			}
		/** */
		inline void	mapPciBusErrorLogInterruptToQBus() noexcept{
			_control =	(		_control
							&	~InterruptDirectionControl::PEL::FieldMask
							)
						| InterruptDirectionControl::PEL::ValueMask_ToQBus;
			}
		/** */
		inline void	mapQBusErrorLogInterruptToPciBus() noexcept{
			_control =	(		_control
							&	~InterruptDirectionControl::QEL::FieldMask
							)
						| InterruptDirectionControl::QEL::ValueMask_ToPciBus;
			}
		/** */
		inline void	mapQBusErrorLogInterruptToQBus() noexcept{
			_control =	(		_control
							&	~InterruptDirectionControl::QEL::FieldMask
							)
						| InterruptDirectionControl::QEL::ValueMask_ToQBus;
			}
		/** */
		inline void	mapDataParityDetectedInterruptToPciBus() noexcept{
			_control =	(		_control
							&	~InterruptDirectionControl::DPD::FieldMask
							)
						| InterruptDirectionControl::DPD::ValueMask_ToPciBus;
			}
		/** */
		inline void	mapDataParityDetectedInterruptToQBus() noexcept{
			_control =	(		_control
							&	~InterruptDirectionControl::DPD::FieldMask
							)
						| InterruptDirectionControl::DPD::ValueMask_ToQBus;
			}
		/** */
		inline void	mapIdmaQBusErrorInterruptToPciBus() noexcept{
			_control =	(		_control
							&	~InterruptDirectionControl::IQE::FieldMask
							)
						| InterruptDirectionControl::IQE::ValueMask_ToPciBus;
			}
		/** */
		inline void	mapIdmaQBusErrorInterruptToQBus() noexcept{
			_control =	(		_control
							&	~InterruptDirectionControl::IQE::FieldMask
							)
						| InterruptDirectionControl::IQE::ValueMask_ToQBus;
			}
		/** */
		inline void	mapIdmaPciErrorInterruptToPciBus() noexcept{
			_control =	(		_control
							&	~InterruptDirectionControl::IPE::FieldMask
							)
						| InterruptDirectionControl::IPE::ValueMask_ToPciBus;
			}
		/** */
		inline void	mapIdmaPciErrorInterruptToQBus() noexcept{
			_control =	(		_control
							&	~InterruptDirectionControl::IPE::FieldMask
							)
						| InterruptDirectionControl::IPE::ValueMask_ToQBus;
			}
		/** */
		inline void	mapIdmaResetInterruptToPciBus() noexcept{
			_control =	(		_control
							&	~InterruptDirectionControl::IRST::FieldMask
							)
						| InterruptDirectionControl::IRST::ValueMask_ToPciBus;
			}
		/** */
		inline void	mapIdmaResetInterruptToQBus() noexcept{
			_control =	(		_control
							&	~InterruptDirectionControl::IRST::FieldMask
							)
						| InterruptDirectionControl::IRST::ValueMask_ToQBus;
			}
		/** */
		inline void	mapIdmaDoneInterruptToPciBus() noexcept{
			_control =	(		_control
							&	~InterruptDirectionControl::DONE::FieldMask
							)
						| InterruptDirectionControl::DONE::ValueMask_ToPciBus;
			}
		/** */
		inline void	mapIdmaDoneInterruptToQBus() noexcept{
			_control =	(		_control
							&	~InterruptDirectionControl::DONE::FieldMask
							)
						| InterruptDirectionControl::DONE::ValueMask_ToQBus;
			}
		/** */
		inline void	mapSoftware3InterruptToPciBus() noexcept{
			_control =	(		_control
							&	~InterruptDirectionControl::SI3::FieldMask
							)
						| InterruptDirectionControl::SI3::ValueMask_ToPciBus;
			}
		/** */
		inline void	mapSoftware3InterruptToQBus() noexcept{
			_control =	(		_control
							&	~InterruptDirectionControl::SI3::FieldMask
							)
						| InterruptDirectionControl::SI3::ValueMask_ToQBus;
			}
		/** */
		inline void	mapSoftware2InterruptToPciBus() noexcept{
			_control =	(		_control
							&	~InterruptDirectionControl::SI2::FieldMask
							)
						| InterruptDirectionControl::SI2::ValueMask_ToPciBus;
			}
		/** */
		inline void	mapSoftware2InterruptToQBus() noexcept{
			_control =	(		_control
							&	~InterruptDirectionControl::SI2::FieldMask
							)
						| InterruptDirectionControl::SI2::ValueMask_ToQBus;
			}
		/** */
		inline void	mapSoftware1InterruptToPciBus() noexcept{
			_control =	(		_control
							&	~InterruptDirectionControl::SI1::FieldMask
							)
						| InterruptDirectionControl::SI1::ValueMask_ToPciBus;
			}
		/** */
		inline void	mapSoftware1InterruptToQBus() noexcept{
			_control =	(		_control
							&	~InterruptDirectionControl::SI1::FieldMask
							)
						| InterruptDirectionControl::SI1::ValueMask_ToQBus;
			}
		/** */
		inline void	mapSoftware0InterruptToPciBus() noexcept{
			_control =	(		_control
							&	~InterruptDirectionControl::SI0::FieldMask
							)
						| InterruptDirectionControl::SI0::ValueMask_ToPciBus;
			}
		/** */
		inline void	mapSoftware0InterruptToQBus() noexcept{
			_control =	(		_control
							&	~InterruptDirectionControl::SI0::FieldMask
							)
						| InterruptDirectionControl::SI0::ValueMask_ToQBus;
			}
	};

}
}
}
}

#endif
