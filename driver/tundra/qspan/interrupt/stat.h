/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_driver_tundra_qspan_interrupt_stath_
#define _oscl_driver_tundra_qspan_interrupt_stath_
#include "oscl/hw/tundra/qspan/fields.h"

/** */
namespace Oscl {
/** */
namespace Tundra {
/** */
namespace Qspan {
/** */
namespace Irq {

/** */
class StatusDriver {
	private:
		/** */
		Oscl::Tundra::Qspan::InterruptStatus::Reg			_status;
		/** */
		volatile Oscl::Tundra::Qspan::InterruptStatus::Reg&	_reg;

	public:
		/** */
		StatusDriver(volatile Oscl::Tundra::Qspan::InterruptStatus::Reg& status
			) noexcept:_reg(status){
			update();
			}
	public:
		/** */
		inline bool	pciBusErrorLogInterruptPending() noexcept{
			return (_status & Oscl::Tundra::Qspan::InterruptStatus::PEL::FieldMask) == Oscl::Tundra::Qspan::InterruptStatus::PEL::ValueMask_Pending;
			}
		/** */
		inline void	clearPciBusErrorLogInterrupt() noexcept{
			_reg	= Oscl::Tundra::Qspan::InterruptStatus::PEL::ValueMask_Clear;
			}
		/** */
		inline bool	qBusErrorLogInterruptPending() noexcept{
			return (_status & Oscl::Tundra::Qspan::InterruptStatus::QEL::FieldMask) == Oscl::Tundra::Qspan::InterruptStatus::QEL::ValueMask_Pending;
			}
		/** */
		inline void	clearQBusErrorLogInterrupt() noexcept{
			_reg	= Oscl::Tundra::Qspan::InterruptStatus::QEL::ValueMask_Clear;
			}
		/** */
		inline bool	dataParityDetectedInterruptPending() noexcept{
			return (_status & Oscl::Tundra::Qspan::InterruptStatus::DPD::FieldMask) == Oscl::Tundra::Qspan::InterruptStatus::DPD::ValueMask_Pending;
			}
		/** */
		inline void	clearDataParityDetectedInterrupt() noexcept{
			_reg	= Oscl::Tundra::Qspan::InterruptStatus::DPD::ValueMask_Clear;
			}
		/** */
		inline bool	idmaQBusErrorInterruptPending() noexcept{
			return (_status & Oscl::Tundra::Qspan::InterruptStatus::IQE::FieldMask) == Oscl::Tundra::Qspan::InterruptStatus::IQE::ValueMask_Pending;
			}
		/** */
		inline void	clearIdmaQBusErrorLogInterrupt() noexcept{
			_reg	= Oscl::Tundra::Qspan::InterruptStatus::IQE::ValueMask_Clear;
			}
		/** */
		inline bool	idmaPciErrorInterruptPending() noexcept{
			return (_status & Oscl::Tundra::Qspan::InterruptStatus::IPE::FieldMask) == Oscl::Tundra::Qspan::InterruptStatus::IPE::ValueMask_Pending;
			}
		/** */
		inline void	clearIdmaPciErrorInterrupt() noexcept{
			_reg	= Oscl::Tundra::Qspan::InterruptStatus::IPE::ValueMask_Clear;
			}
		/** */
		inline bool	idmaResetInterruptPending() noexcept{
			return (_status & Oscl::Tundra::Qspan::InterruptStatus::IRST::FieldMask) == Oscl::Tundra::Qspan::InterruptStatus::IRST::ValueMask_Pending;
			}
		/** */
		inline void	clearIdmaResetInterrupt() noexcept{
			_reg	= Oscl::Tundra::Qspan::InterruptStatus::IRST::ValueMask_Clear;
			}
		/** */
		inline bool	idmaDoneInterruptPending() noexcept{
			return (_status & Oscl::Tundra::Qspan::InterruptStatus::DONE::FieldMask) == Oscl::Tundra::Qspan::InterruptStatus::DONE::ValueMask_Pending;
			}
		/** */
		inline void	clearIdmaDoneInterrupt() noexcept{
			_reg	= Oscl::Tundra::Qspan::InterruptStatus::DONE::ValueMask_Clear;
			}
		/** */
		inline bool	pciInterruptPending() noexcept{
			return (_status & Oscl::Tundra::Qspan::InterruptStatus::INT::FieldMask) == Oscl::Tundra::Qspan::InterruptStatus::INT::ValueMask_Pending;
			}
		/** */
		inline void	clearPciInterrupt() noexcept{
			_reg	= Oscl::Tundra::Qspan::InterruptStatus::INT::ValueMask_Clear;
			}
		/** */
		inline bool	pciBusPerrInterruptPending() noexcept{
			return (_status & Oscl::Tundra::Qspan::InterruptStatus::PERR::FieldMask) == Oscl::Tundra::Qspan::InterruptStatus::PERR::ValueMask_Pending;
			}
		/** */
		inline void	clearPciBusPerrInterrupt() noexcept{
			_reg	= Oscl::Tundra::Qspan::InterruptStatus::PERR::ValueMask_Clear;
			}
		/** */
		inline bool	pciBusSerrInterruptPending() noexcept{
			return (_status & Oscl::Tundra::Qspan::InterruptStatus::SERR::FieldMask) == Oscl::Tundra::Qspan::InterruptStatus::SERR::ValueMask_Pending;
			}
		/** */
		inline void	clearPciBusSerrInterrupt() noexcept{
			_reg	= Oscl::Tundra::Qspan::InterruptStatus::SERR::ValueMask_Clear;
			}
		/** */
		inline bool	qBusInterruptPending() noexcept{
			return (_status & Oscl::Tundra::Qspan::InterruptStatus::QINT::FieldMask) == Oscl::Tundra::Qspan::InterruptStatus::QINT::ValueMask_Pending;
			}
		/** */
		inline void	clearQBusInterrupt() noexcept{
			_reg	= Oscl::Tundra::Qspan::InterruptStatus::QINT::ValueMask_Clear;
			}
		/** */
		inline bool	software3InterruptPending() noexcept{
			return (_status & Oscl::Tundra::Qspan::InterruptStatus::SI3::FieldMask) == Oscl::Tundra::Qspan::InterruptStatus::SI3::ValueMask_Pending;
			}
		/** */
		inline void	clearSoftware3Interrupt() noexcept{
			_reg	= Oscl::Tundra::Qspan::InterruptStatus::SI3::ValueMask_Clear;
			}
		/** */
		inline bool	software2InterruptPending() noexcept{
			return (_status & Oscl::Tundra::Qspan::InterruptStatus::SI2::FieldMask) == Oscl::Tundra::Qspan::InterruptStatus::SI2::ValueMask_Pending;
			}
		/** */
		inline void	clearSoftware2Interrupt() noexcept{
			_reg	= Oscl::Tundra::Qspan::InterruptStatus::SI2::ValueMask_Clear;
			}
		/** */
		inline bool	software1InterruptPending() noexcept{
			return (_status & Oscl::Tundra::Qspan::InterruptStatus::SI1::FieldMask) == Oscl::Tundra::Qspan::InterruptStatus::SI1::ValueMask_Pending;
			}
		/** */
		inline void	clearSoftware1Interrupt() noexcept{
			_reg	= Oscl::Tundra::Qspan::InterruptStatus::SI1::ValueMask_Clear;
			}
		/** */
		inline bool	software0InterruptPending() noexcept{
			return (_status & Oscl::Tundra::Qspan::InterruptStatus::SI0::FieldMask) == Oscl::Tundra::Qspan::InterruptStatus::SI0::ValueMask_Pending;
			}
		/** */
		inline void	clearSoftware0Interrupt() noexcept{
			_reg	= Oscl::Tundra::Qspan::InterruptStatus::SI0::ValueMask_Clear;
			}
		inline void	update() noexcept{
			_status	= _reg;
			}
		inline bool anyPending() noexcept{
			return _status & (
					InterruptStatus::PEL::ValueMask_Pending
				|	InterruptStatus::QEL::ValueMask_Pending
				|	InterruptStatus::DPD::ValueMask_Pending
				|	InterruptStatus::IQE::ValueMask_Pending
				|	InterruptStatus::IPE::ValueMask_Pending
				|	InterruptStatus::IRST::ValueMask_Pending
				|	InterruptStatus::DONE::ValueMask_Pending
				|	InterruptStatus::INT::ValueMask_Pending
				|	InterruptStatus::PERR::ValueMask_Pending
				|	InterruptStatus::SERR::ValueMask_Pending
				|	InterruptStatus::QINT::ValueMask_Pending
				|	InterruptStatus::SI3::ValueMask_Pending
				|	InterruptStatus::SI2::ValueMask_Pending
				|	InterruptStatus::SI1::ValueMask_Pending
				|	InterruptStatus::SI0::ValueMask_Pending
				);
			}
	};

}
}
}
}

#endif
