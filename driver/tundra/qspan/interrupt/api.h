/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_driver_tundra_qspan_interrupt_apih_
#define _oscl_driver_tundra_qspan_interrupt_apih_
#include "stat.h"
#include "oscl/interrupt/handler.h"

/** */
namespace Oscl {
/** */
namespace Tundra {
/** */
namespace Qspan {
/** */
namespace Irq {

/** */
class Api {
	public:
		/** */
		virtual ~Api() {}
		/** */
		virtual void	reservePciBusErrorLog(	Oscl::Interrupt::
												HandlerApi&		handler
												) noexcept=0;
		/** */
		virtual void	releasePciBusErrorLog() noexcept=0;
		/** */
		virtual void	reserveQBusErrorLog(	Oscl::Interrupt::
												HandlerApi&		handler
												) noexcept=0;
		/** */
		virtual void	releaseQBusErrorLog() noexcept=0;
		/** */
		virtual void	reserveDataParity(	Oscl::Interrupt::
											HandlerApi&		handler
											) noexcept=0;
		/** */
		virtual void	releaseDataParity() noexcept=0;
		/** */
		virtual void	reserveIdmaQBusError(	Oscl::Interrupt::
												HandlerApi&		handler
												) noexcept=0;
		/** */
		virtual void	releaseIdmaQBusError() noexcept=0;
		/** */
		virtual void	reserveIdmaPciError(	Oscl::Interrupt::
												HandlerApi&		handler
												) noexcept=0;
		/** */
		virtual void	releaseIdmaPciError() noexcept=0;
		/** */
		virtual void	reserveIdmaReset(	Oscl::Interrupt::
											HandlerApi&		handler
											) noexcept=0;
		/** */
		virtual void	releaseIdmaReset() noexcept=0;
		/** */
		virtual void	reserveIdmaDone(	Oscl::Interrupt::
											HandlerApi&		handler
											) noexcept=0;
		/** */
		virtual void	releaseIdmaDone() noexcept=0;
		/** */
		virtual void	reservePciInt(	Oscl::Interrupt::
										HandlerApi&		handler
										) noexcept=0;
		/** */
		virtual void	releasePciInt() noexcept=0;
		/** */
		virtual void	reservePciBusPerr(	Oscl::Interrupt::
										HandlerApi&		handler
										) noexcept=0;
		/** */
		virtual void	releasePciBusPerr() noexcept=0;
		/** */
		virtual void	reservePciBusSerr(	Oscl::Interrupt::
											HandlerApi&		handler
											) noexcept=0;
		/** */
		virtual void	releasePciBusSerr() noexcept=0;
		/** */
		virtual void	reserveQBusInt(	Oscl::Interrupt::
										HandlerApi&		handler
										) noexcept=0;
		/** */
		virtual void	releaseQBusInt() noexcept=0;
		/** */
		virtual void	reserveSoftware3(	Oscl::Interrupt::
											HandlerApi&		handler
											) noexcept=0;
		/** */
		virtual void	releaseSoftware3() noexcept=0;
		/** */
		virtual void	reserveSoftware2(	Oscl::Interrupt::
											HandlerApi&		handler
											) noexcept=0;
		/** */
		virtual void	releaseSoftware2() noexcept=0;
		/** */
		virtual void	reserveSoftware1(	Oscl::Interrupt::
											HandlerApi&		handler
											) noexcept=0;
		/** */
		virtual void	releaseSoftware1() noexcept=0;
		/** */
		virtual void	reserveSoftware0(	Oscl::Interrupt::
											HandlerApi&		handler
											) noexcept=0;
		/** */
		virtual void	releaseSoftware0() noexcept=0;
	};

}
}
}
}

#endif
