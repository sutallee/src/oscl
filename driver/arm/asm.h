/*
   Copyright (C) 2006 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_arm_asmh_
#define _oscl_drv_arm_asmh_

/** */
namespace Oscl {

/** */
namespace ARM {

inline void msrSpsr(unsigned long value) noexcept{
    asm volatile("msr cpsr_cxsf,%0" : : "r" (value));
    }

inline unsigned long mrsSpsr() noexcept{
	unsigned long	reg;
    asm volatile("mrs %0,CPSR" : "=r" (reg));
	return reg;
    }

// Sets reservation for the "address" within the processor specific
// granularity (typically cache line). Some implementations only allow
// a single oustanding reservation per processor.
inline unsigned long	ldrex(volatile unsigned long *address) noexcept{
	unsigned long	result;
	asm volatile (	"ldrex %[result],[%[address]]\n"
					: /* Output */
					[result]"=r" (result)
					: /* Input */
					[address]"r" (address)
					);
	return result;
	}

// This operation returns zero (false) if the conditional store succeeds.
// The store will succeed if no other processor/procedure has
// stored to the cache line (granularity) associated with "address".
inline bool	strex(volatile unsigned long* address,unsigned long value) noexcept{
	int	result;

	asm volatile (
		"   strex   %[result], %[value], [%[address]]\n"
		: /* Output */
		[result]"=&r" (result)
		: /* Input */
		[address]"r" (address),
		[value]"r" (value)
		);

	return result;
	}

}
}

#endif
