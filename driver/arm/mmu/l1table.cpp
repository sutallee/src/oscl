/*
   Copyright (C) 2006 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "l1table.h"

using namespace Oscl::ARM::Mmu;

Level1Descriptor::Level1Descriptor() noexcept:
		_desc(Oscl::HW::ARM::MMU::Level1Desc::All::Format::ValueMask_Fault)
		{
	}

void			Level1Descriptor::setDescType(Level1Descriptor::DescType type) noexcept{
	Oscl::HW::ARM::MMU::Level1Desc::All::Reg	value;
	switch(type){
		case Coarse:
			value	= Oscl::HW::ARM::MMU::Level1Desc::All::Format::ValueMask_Coarse;
			break;
		case Section:
			value	= Oscl::HW::ARM::MMU::Level1Desc::All::Format::ValueMask_Section;
			break;
		case Fine:
			value	= Oscl::HW::ARM::MMU::Level1Desc::All::Format::ValueMask_Fine;
			break;
		case Fault:
		default:
			value	= Oscl::HW::ARM::MMU::Level1Desc::All::Format::ValueMask_Fault;
			break;
		}
	_desc	&= ~Oscl::HW::ARM::MMU::Level1Desc::All::Format::FieldMask;
	_desc	|= value;
	}

Level1Descriptor::DescType		Level1Descriptor::getDescType() const noexcept{
	Oscl::HW::ARM::MMU::Level1Desc::All::Reg	value	= _desc;
	value	&= ~Oscl::HW::ARM::MMU::Level1Desc::All::Format::FieldMask;
	switch(value){
		case Oscl::HW::ARM::MMU::Level1Desc::All::Format::ValueMask_Coarse:
			return Coarse;
		case Oscl::HW::ARM::MMU::Level1Desc::All::Format::ValueMask_Section:
			return Section;
		case Oscl::HW::ARM::MMU::Level1Desc::All::Format::ValueMask_Fine:
			return Fine;
		case Oscl::HW::ARM::MMU::Level1Desc::All::Format::ValueMask_Fault:
		default:
			return Fault;
		}
	}

Level1CoarseDescriptor*		Level1Descriptor::getCoarseDescriptor() const noexcept{
	if((_desc & Oscl::HW::ARM::MMU::Level1Desc::All::Format::FieldMask)==Oscl::HW::ARM::MMU::Level1Desc::All::Format::ValueMask_Coarse){
		return (Level1CoarseDescriptor*)this;
		}
	return 0;
	}

Level1SectionDescriptor*	Level1Descriptor::getSectionDescriptor() const noexcept{
	if((_desc & Oscl::HW::ARM::MMU::Level1Desc::All::Format::FieldMask)==Oscl::HW::ARM::MMU::Level1Desc::All::Format::ValueMask_Section){
		return (Level1SectionDescriptor*)this;
		}
	return 0;
	}

Level1FineDescriptor*		Level1Descriptor::getFineDescriptor() const noexcept{
	if((_desc & Oscl::HW::ARM::MMU::Level1Desc::All::Format::FieldMask)==Oscl::HW::ARM::MMU::Level1Desc::All::Format::ValueMask_Fine){
		return (Level1FineDescriptor*)this;
		}
	return 0;
	}

void	Level1CoarseDescriptor::setLevel2Table(CoarseLevel2Table& level2Table) noexcept{
	_desc	&= ~(Oscl::HW::ARM::MMU::Level1Desc::Coarse::PageTableBaseAddress::FieldMask);
	_desc	|= (((unsigned long)&level2Table) & Oscl::HW::ARM::MMU::Level1Desc::Coarse::PageTableBaseAddress::FieldMask);
	}

CoarseLevel2Table*	Level1CoarseDescriptor::getLevel2Table() const noexcept{
	return (CoarseLevel2Table*)(_desc & Oscl::HW::ARM::MMU::Level1Desc::Coarse::PageTableBaseAddress::FieldMask);
	}

void	Level1CoarseDescriptor::setDomain(unsigned char domain) noexcept{
	_desc	&= ~(Oscl::HW::ARM::MMU::Level1Desc::Coarse::Domain::FieldMask);
	_desc	|= ((((unsigned long)domain)<<Oscl::HW::ARM::MMU::Level1Desc::Coarse::Domain::Lsb) & Oscl::HW::ARM::MMU::Level1Desc::Coarse::Domain::FieldMask);
	}

unsigned char	Level1CoarseDescriptor::getDomain() const noexcept{
	return (unsigned char)((_desc & Oscl::HW::ARM::MMU::Level1Desc::Coarse::Domain::FieldMask) >> Oscl::HW::ARM::MMU::Level1Desc::Coarse::Domain::Lsb);
	}

void	Level1CoarseDescriptor::setIMP(unsigned char imp) noexcept{
	_desc	&= ~(Oscl::HW::ARM::MMU::Level1Desc::Coarse::IMP::FieldMask);
	_desc	|= ((((unsigned long)imp)<<Oscl::HW::ARM::MMU::Level1Desc::Coarse::IMP::Lsb) & Oscl::HW::ARM::MMU::Level1Desc::Coarse::IMP::FieldMask);
	}

unsigned char	Level1CoarseDescriptor::getIMP() const noexcept{
	return (unsigned char)((_desc & Oscl::HW::ARM::MMU::Level1Desc::Coarse::Domain::FieldMask) >> Oscl::HW::ARM::MMU::Level1Desc::Coarse::Domain::Lsb);
	}

void	Level1SectionDescriptor::setLevel2Table(Level2Table& level2Table) noexcept{
	_desc	&= ~(Oscl::HW::ARM::MMU::Level1Desc::Section::BaseAddress::FieldMask);
	_desc	|= (((unsigned long)&level2Table) & Oscl::HW::ARM::MMU::Level1Desc::Section::BaseAddress::FieldMask);
	}

Level2Table*	Level1SectionDescriptor::getLevel2Table() const noexcept{
	return (Level2Table*)(_desc & Oscl::HW::ARM::MMU::Level1Desc::Section::BaseAddress::FieldMask);
	}

void	Level1SectionDescriptor::setAccessPermissions(unsigned char domain) noexcept{
	_desc	&= ~(Oscl::HW::ARM::MMU::Level1Desc::Section::Domain::FieldMask);
	_desc	|= ((((unsigned long)domain)<<Oscl::HW::ARM::MMU::Level1Desc::Section::AP::Lsb) & Oscl::HW::ARM::MMU::Level1Desc::Section::AP::FieldMask);
	}

unsigned char	Level1SectionDescriptor::getAccessPermissions() const noexcept{
	return (unsigned char)((_desc & Oscl::HW::ARM::MMU::Level1Desc::Section::AP::FieldMask) >> Oscl::HW::ARM::MMU::Level1Desc::Section::AP::Lsb);
	}

void	Level1SectionDescriptor::setDomain(unsigned char domain) noexcept{
	_desc	&= ~(Oscl::HW::ARM::MMU::Level1Desc::Section::Domain::FieldMask);
	_desc	|= ((((unsigned long)domain)<<Oscl::HW::ARM::MMU::Level1Desc::Section::Domain::Lsb) & Oscl::HW::ARM::MMU::Level1Desc::Section::Domain::FieldMask);
	}

unsigned char	Level1SectionDescriptor::getDomain() const noexcept{
	return (unsigned char)((_desc & Oscl::HW::ARM::MMU::Level1Desc::Section::Domain::FieldMask) >> Oscl::HW::ARM::MMU::Level1Desc::Section::Domain::Lsb);
	}

void	Level1SectionDescriptor::setIMP(unsigned char imp) noexcept{
	_desc	&= ~(Oscl::HW::ARM::MMU::Level1Desc::Section::IMP::FieldMask);
	_desc	|= ((((unsigned long)imp)<<Oscl::HW::ARM::MMU::Level1Desc::Section::IMP::Lsb) & Oscl::HW::ARM::MMU::Level1Desc::Section::IMP::FieldMask);
	}

unsigned char	Level1SectionDescriptor::getIMP() const noexcept{
	return (unsigned char)((_desc & Oscl::HW::ARM::MMU::Level1Desc::Section::Domain::FieldMask) >> Oscl::HW::ARM::MMU::Level1Desc::Section::Domain::Lsb);
	}

void	Level1SectionDescriptor::setBufferable() noexcept{
	_desc	&= ~(Oscl::HW::ARM::MMU::Level1Desc::Section::B::FieldMask);
	_desc	|= Oscl::HW::ARM::MMU::Level1Desc::Section::B::ValueMask_Buffered;
	}

void	Level1SectionDescriptor::setNotBufferable() noexcept{
	_desc	&= ~(Oscl::HW::ARM::MMU::Level1Desc::Section::B::FieldMask);
	_desc	|= Oscl::HW::ARM::MMU::Level1Desc::Section::B::ValueMask_Unbuffered;
	}

bool	Level1SectionDescriptor::isBufferable() const noexcept{
	return (_desc & Oscl::HW::ARM::MMU::Level1Desc::Section::B::FieldMask) == Oscl::HW::ARM::MMU::Level1Desc::Section::B::ValueMask_Buffered;
	}

void	Level1SectionDescriptor::setCachable() noexcept{
	_desc	&= ~(Oscl::HW::ARM::MMU::Level1Desc::Section::C::FieldMask);
	_desc	|= Oscl::HW::ARM::MMU::Level1Desc::Section::C::ValueMask_Cached;
	}

void	Level1SectionDescriptor::setNotCachable() noexcept{
	_desc	&= ~(Oscl::HW::ARM::MMU::Level1Desc::Section::C::FieldMask);
	_desc	|= Oscl::HW::ARM::MMU::Level1Desc::Section::C::ValueMask_Uncached;
	}

bool	Level1SectionDescriptor::isCachable() const noexcept{
	return (_desc & Oscl::HW::ARM::MMU::Level1Desc::Section::C::FieldMask) == Oscl::HW::ARM::MMU::Level1Desc::Section::C::ValueMask_Cached;
	}

void	Level1FineDescriptor::setLevel2Table(FineLevel2Table& level2Table) noexcept{
	_desc	&= ~(Oscl::HW::ARM::MMU::Level1Desc::Fine::PageTableBaseAddress::FieldMask);
	_desc	|= (((unsigned long)&level2Table) & Oscl::HW::ARM::MMU::Level1Desc::Fine::PageTableBaseAddress::FieldMask);
	}

FineLevel2Table*	Level1FineDescriptor::getLevel2Table() const noexcept{
	return (FineLevel2Table*)(_desc & Oscl::HW::ARM::MMU::Level1Desc::Fine::PageTableBaseAddress::FieldMask);
	}

void	Level1FineDescriptor::setDomain(unsigned char domain) noexcept{
	_desc	&= ~(Oscl::HW::ARM::MMU::Level1Desc::Fine::Domain::FieldMask);
	_desc	|= ((((unsigned long)domain)<<Oscl::HW::ARM::MMU::Level1Desc::Fine::Domain::Lsb) & Oscl::HW::ARM::MMU::Level1Desc::Fine::Domain::FieldMask);
	}

unsigned char	Level1FineDescriptor::getDomain() const noexcept{
	return (unsigned char)((_desc & Oscl::HW::ARM::MMU::Level1Desc::Fine::Domain::FieldMask) >> Oscl::HW::ARM::MMU::Level1Desc::Fine::Domain::Lsb);
	}

void	Level1FineDescriptor::setIMP(unsigned char imp) noexcept{
	_desc	&= ~(Oscl::HW::ARM::MMU::Level1Desc::Fine::IMP::FieldMask);
	_desc	|= ((((unsigned long)imp)<<Oscl::HW::ARM::MMU::Level1Desc::Fine::IMP::Lsb) & Oscl::HW::ARM::MMU::Level1Desc::Fine::IMP::FieldMask);
	}

unsigned char	Level1FineDescriptor::getIMP() const noexcept{
	return (unsigned char)((_desc & Oscl::HW::ARM::MMU::Level1Desc::Fine::Domain::FieldMask) >> Oscl::HW::ARM::MMU::Level1Desc::Fine::Domain::Lsb);
	}


Level1Table::Level1Table() noexcept {
	}

Level1Descriptor* Level1Table::getDesc(unsigned entry) noexcept{
	if(entry > (nEntriesInLevel1Table-1)){
		return 0;
		}
	return &_table[entry];
	}

Level1Descriptor* Level1Table::getDesc(void* virtualAddr) noexcept{
	return getDesc(	(unsigned)(
					(	((unsigned long)virtualAddr)
							& Oscl::HW::ARM::MMU::VirtualAddress::FirstLevelTableIndex::FieldMask)
							>> Oscl::HW::ARM::MMU::VirtualAddress::FirstLevelTableIndex::Lsb
						)
					);
	}

