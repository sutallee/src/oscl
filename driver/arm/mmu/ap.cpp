#include "ap.h"

using namespace Oscl::ARM::Mmu::AP;

DeterminedBySR						Oscl::ARM::Mmu::AP::determinedBySR;
PrivilegedReadWriteUserNoAccess		Oscl::ARM::Mmu::AP::privilegedReadWriteUserNoAccess;
PrivilegedReadWriteUserReadOnly		Oscl::ARM::Mmu::AP::privilegedReadWriteUserReadOnly;
PrivilegedReadWriteUserReadWrite	Oscl::ARM::Mmu::AP::privilegedReadWriteUserReadWrite;

void	DeterminedBySR::query(Handler& handler) const noexcept{
	handler.determinedBySR();
	}

void	PrivilegedReadWriteUserNoAccess::query(Handler& handler) const noexcept{
	handler.privRwUserNoAccess();
	}

void	PrivilegedReadWriteUserReadOnly::query(Handler& handler) const noexcept{
	handler.privRwUserReadOnly();
	}

void	PrivilegedReadWriteUserReadWrite::query(Handler& handler) const noexcept{
	handler.privRwUserReadWrite();
	}
