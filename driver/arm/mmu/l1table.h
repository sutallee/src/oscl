/*
   Copyright (C) 2006 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_arm_mmu_l1tableh_
#define _oscl_drv_arm_mmu_l1tableh_
#include "oscl/hw/arm/mmu/level1.h"
#include "oscl/hw/arm/mmu/level2.h"
#include "l2table.h"

/** */
namespace Oscl {
/** */
namespace ARM {
/** */
namespace Mmu {

class Level1CoarseDescriptor;
class Level1SectionDescriptor;
class Level1FineDescriptor;
class Level2Table;

/** */
class Level1Descriptor {
	public:
		/** Used to set/get the appropriate type of descriptor format */
		typedef enum {	Fault	= Oscl::HW::ARM::MMU::Level1Desc::All::Format::Value_Fault,
						Coarse	= Oscl::HW::ARM::MMU::Level1Desc::All::Format::Value_Coarse,
						Section	= Oscl::HW::ARM::MMU::Level1Desc::All::Format::Value_Section,
						Fine	= Oscl::HW::ARM::MMU::Level1Desc::All::Format::Value_Fine
						} DescType;
	private:
		/** */
		Oscl::HW::ARM::MMU::Level1Desc::All::Reg		_desc;
	public:
		/** Initializes to Fault descriptor */
		Level1Descriptor() noexcept;
		/** */
		void			setDescType(DescType type) noexcept;
		/** */
		DescType		getDescType() const noexcept;
		/** Returns pointer to coarse descriptor if the descriptor type
			is Coarse, otherwise a null pointer is returned.
		 */
		Level1CoarseDescriptor*		getCoarseDescriptor() const noexcept;
		/** Returns pointer to section descriptor if the descriptor type
			is Section, otherwise a null pointer is returned.
		 */
		Level1SectionDescriptor*	getSectionDescriptor() const noexcept;
		/** Returns pointer to Fine descriptor if the descriptor type
			is Fine, otherwise a null pointer is returned.
		 */
		Level1FineDescriptor*		getFineDescriptor() const noexcept;
	};

/** */
class Level1CoarseDescriptor {
	private:
		/** */
		Oscl::HW::ARM::MMU::Level1Desc::Coarse::Reg		_desc;
	public:
		/** */
		void				setLevel2Table(CoarseLevel2Table& level2Table) noexcept;
		/** */
		CoarseLevel2Table*	getLevel2Table() const noexcept;
		/** */
		void				setDomain(unsigned char domain) noexcept;
		/** */
		unsigned char		getDomain() const noexcept;
		/** */
		void				setIMP(unsigned char imp) noexcept;
		/** */
		unsigned char		getIMP() const noexcept;
	};

/** */
class Level1SectionDescriptor {
	private:
		/** */
		Oscl::HW::ARM::MMU::Level1Desc::Section::Reg		_desc;
	public:
		/** */
		void			setLevel2Table(Level2Table& level2Table) noexcept;
		/** */
		Level2Table*	getLevel2Table() const noexcept;
		/** */
		void			setAccessPermissions(unsigned char ap) noexcept;
		/** */
		unsigned char	getAccessPermissions() const noexcept;
		/** */
		void			setDomain(unsigned char domain) noexcept;
		/** */
		unsigned char	getDomain() const noexcept;
		/** */
		void			setIMP(unsigned char imp) noexcept;
		/** */
		unsigned char	getIMP() const noexcept;
		/** */
		void			setBufferable() noexcept;
		/** */
		void			setNotBufferable() noexcept;
		/** */
		bool			isBufferable() const noexcept;
		/** */
		void			setCachable() noexcept;
		/** */
		void			setNotCachable() noexcept;
		/** */
		bool			isCachable() const noexcept;
	};

/** */
class Level1FineDescriptor {
	private:
		/** */
		Oscl::HW::ARM::MMU::Level1Desc::Fine::Reg		_desc;
	public:
		/** */
		void				setLevel2Table(FineLevel2Table& level2Table) noexcept;
		/** */
		FineLevel2Table*	getLevel2Table() const noexcept;
		/** */
		void				setDomain(unsigned char domain) noexcept;
		/** */
		unsigned char		getDomain() const noexcept;
		/** */
		void				setIMP(unsigned char imp) noexcept;
		/** */
		unsigned char		getIMP() const noexcept;
	};
/** */
class Level1Table {
	public:
		enum{nEntriesInLevel1Table = 4096};
	private:
		/** */
		Level1Descriptor	_table[nEntriesInLevel1Table];
	public:
		/** */
		Level1Table() noexcept;
		/** */
		Level1Descriptor* getDesc(unsigned entry) noexcept;
		/** */
		Level1Descriptor* getDesc(void* virtualAddress) noexcept;
	};

}
}
}

#endif
