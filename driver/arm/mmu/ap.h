/*
   Copyright (C) 2006 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_arm_mmu_aph_
#define _oscl_drv_arm_mmu_aph_

/** */
namespace Oscl {
/** */
namespace ARM {
/** */
namespace Mmu {
/** */
namespace AP {

/** */
class Handler {
	public:
		/** */
		virtual ~Handler() {}
		/** */
		virtual void	determinedBySR() noexcept=0;
		/** */
		virtual void	privRwUserNoAccess() noexcept=0;
		/** */
		virtual void	privRwUserReadOnly() noexcept=0;
		/** */
		virtual void	privRwUserReadWrite() noexcept=0;
	};

/** */
class Api {
	public:
		/** */
		virtual ~Api() {}
		/** */
		virtual void	query(Handler& handler) const noexcept=0;
	};

class DeterminedBySR : public Api {
	public:
		/** */
		void	query(Handler& handler) const noexcept;
	};

class PrivilegedReadWriteUserNoAccess : public Api {
	public:
		/** */
		void	query(Handler& handler) const noexcept;
	};

class PrivilegedReadWriteUserReadOnly : public Api {
	public:
		/** */
		void	query(Handler& handler) const noexcept;
	};

class PrivilegedReadWriteUserReadWrite : public Api {
	public:
		/** */
		void	query(Handler& handler) const noexcept;
	};

extern DeterminedBySR					determinedBySR;
extern PrivilegedReadWriteUserNoAccess	privilegedReadWriteUserNoAccess;
extern PrivilegedReadWriteUserReadOnly	privilegedReadWriteUserReadOnly;
extern PrivilegedReadWriteUserReadWrite	privilegedReadWriteUserReadWrite;

}
}
}
}

#endif
