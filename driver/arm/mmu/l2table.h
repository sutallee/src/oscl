/*
   Copyright (C) 2006 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_arm_mmu_l2tableh_
#define _oscl_drv_arm_mmu_l2tableh_
#include "oscl/hw/arm/mmu/level2.h"
#include "ap.h"

/** */
namespace Oscl {
/** */
namespace ARM {
/** */
namespace Mmu {

class Level2LargeDescriptor;
class Level2SmallDescriptor;
class Level2TinyDescriptor;

/** */
class Level2Descriptor {
	public:
		/** Used to set/get the appropriate type of descriptor format */
		typedef enum {	Fault	= Oscl::HW::ARM::MMU::Level2Desc::All::Format::Value_Fault,
						Large	= Oscl::HW::ARM::MMU::Level2Desc::All::Format::Value_LargePage,
						Small	= Oscl::HW::ARM::MMU::Level2Desc::All::Format::Value_SmallPage,
						Tiny	= Oscl::HW::ARM::MMU::Level2Desc::All::Format::Value_TinyPage
						} DescType;
	private:
		/** */
		Oscl::HW::ARM::MMU::Level2Desc::All::Reg		_desc;

	public:
		/** */
		Level2Descriptor() noexcept;

		/** */
		void			setDescType(DescType type) noexcept;

		/** */
		DescType		getDescType() const noexcept;

		/** Returns pointer to large descriptor if the descriptor type
			is Large , otherwise a null pointer is returned.
		 */
		Level2LargeDescriptor*		getLargeDescriptor() const noexcept;

		/** Returns pointer to small descriptor if the descriptor type
			is Small, otherwise a null pointer is returned.
		 */
		Level2SmallDescriptor*		getSmallDescriptor() const noexcept;

		/** Returns pointer to tiny descriptor if the descriptor type
			is Tiny, otherwise a null pointer is returned.
		 */
		Level2TinyDescriptor*		getTinyDescriptor() const noexcept;
	};

/** */
class Level2LargeDescriptor {
	private:
		/** */
		Oscl::HW::ARM::MMU::Level2Desc::LargePage::Reg		_desc;
	public:
		/** Must be 64KB aligned */
		void							setPageBase(void* page) noexcept;
		/** */
		void*							getPageBase() const noexcept;
		/** */
		void							setAP0(const Oscl::ARM::Mmu::AP::Api& ap) noexcept;
		/** */
		const Oscl::ARM::Mmu::AP::Api&	getAP0() const noexcept;
		/** */
		void							setAP1(const Oscl::ARM::Mmu::AP::Api& ap) noexcept;
		/** */
		const Oscl::ARM::Mmu::AP::Api&	getAP1() const noexcept;
		/** */
		void							setAP2(const Oscl::ARM::Mmu::AP::Api& ap) noexcept;
		/** */
		const Oscl::ARM::Mmu::AP::Api&	getAP2() const noexcept;
		/** */
		void							setAP3(const Oscl::ARM::Mmu::AP::Api& ap) noexcept;
		/** */
		const Oscl::ARM::Mmu::AP::Api&	getAP3() const noexcept;
		/** */
		void							setBufferable() noexcept;
		/** */
		void							setNotBufferable() noexcept;
		/** */
		bool							isBufferable() const noexcept;
		/** */
		void							setCachable() noexcept;
		/** */
		void							setNotCachable() noexcept;
		/** */
		bool							isCachable() const noexcept;
	};

/** */
class Level2SmallDescriptor {
	private:
		/** */
		Oscl::HW::ARM::MMU::Level2Desc::SmallPage::Reg		_desc;
	public:
		/** Must be 4KB aligned */
		void							setPageBase(void* page) noexcept;
		/** */
		void*							getPageBase() const noexcept;
		/** */
		void							setAP0(const Oscl::ARM::Mmu::AP::Api& ap) noexcept;
		/** */
		const Oscl::ARM::Mmu::AP::Api&	getAP0() const noexcept;
		/** */
		void							setAP1(const Oscl::ARM::Mmu::AP::Api& ap) noexcept;
		/** */
		const Oscl::ARM::Mmu::AP::Api&	getAP1() const noexcept;
		/** */
		void							setAP2(const Oscl::ARM::Mmu::AP::Api& ap) noexcept;
		/** */
		const Oscl::ARM::Mmu::AP::Api&	getAP2() const noexcept;
		/** */
		void							setAP3(const Oscl::ARM::Mmu::AP::Api& ap) noexcept;
		/** */
		const Oscl::ARM::Mmu::AP::Api&	getAP3() const noexcept;
		/** */
		void							setBufferable() noexcept;
		/** */
		void							setNotBufferable() noexcept;
		/** */
		bool							isBufferable() const noexcept;
		/** */
		void							setCachable() noexcept;
		/** */
		void							setNotCachable() noexcept;
		/** */
		bool							isCachable() const noexcept;
	};

/** */
class Level2TinyDescriptor {
	private:
		/** */
		Oscl::HW::ARM::MMU::Level2Desc::TinyPage::Reg		_desc;
	public:
		/** Must be 1KB aligned */
		void							setPageBase(void* page) noexcept;
		/** */
		void*							getPageBase() const noexcept;
		/** */
		void							setAP(const Oscl::ARM::Mmu::AP::Api& ap) noexcept;
		/** */
		const Oscl::ARM::Mmu::AP::Api&	getAP() const noexcept;
		/** */
		void							setBufferable() noexcept;
		/** */
		void							setNotBufferable() noexcept;
		/** */
		bool							isBufferable() const noexcept;
		/** */
		void							setCachable() noexcept;
		/** */
		void							setNotCachable() noexcept;
		/** */
		bool							isCachable() const noexcept;
	};

#if 0
/** */
class Level2Table {
	private:
		/** */
		Level2Descriptor	_table[nEntriesInLevel2Table];
	public:
		/** */
		Level2Table() noexcept;
		/** */
		Level2Descriptor* getDesc(unsigned entry) noexcept;
		/** */
		Level2Descriptor* getDesc(void* virtAddr) noexcept;
	};
#endif

/** */
class CoarseLevel2Table {
	public:
		/** */
		enum { nEntriesInCoarseLevel2Table = 256};
	private:
		/** */
		Level2Descriptor	_table[nEntriesInCoarseLevel2Table];
	public:
		/** */
		CoarseLevel2Table() noexcept;
		/** */
		Level2Descriptor* getDesc(unsigned entry) noexcept;
		/** */
		Level2Descriptor* getDesc(void* virtualAddr) noexcept;
	};

/** */
class FineLevel2Table {
	public:
		/** */
		enum { nEntriesInFineLevel2Table = 1024};
	private:
		/** */
		Level2Descriptor	_table[nEntriesInFineLevel2Table];
	public:
		/** */
		FineLevel2Table() noexcept;
		/** */
		Level2Descriptor* getDesc(unsigned entry) noexcept;
		/** */
		Level2Descriptor* getDesc(void* virtualAddr) noexcept;
	};
}
}
}

#endif
