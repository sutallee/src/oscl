/*
   Copyright (C) 2006 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "l2table.h"
#include "oscl/hw/arm/mmu/level1.h"

using namespace Oscl::ARM::Mmu;

Level2Descriptor::Level2Descriptor() noexcept:
		// The 0xFF000000 is just an initialization marker.
		_desc(0xFF000000 | Oscl::HW::ARM::MMU::Level2Desc::All::Format::ValueMask_Fault)
		{
	}

void	Level2Descriptor::setDescType(DescType type) noexcept{
	Oscl::HW::ARM::MMU::Level2Desc::All::Reg	value;
	switch(type){
		case Large:
			value	= Oscl::HW::ARM::MMU::Level2Desc::All::Format::ValueMask_LargePage;
			break;
		case Small:
			value	= Oscl::HW::ARM::MMU::Level2Desc::All::Format::ValueMask_SmallPage;
			break;
		case Tiny:
			value	= Oscl::HW::ARM::MMU::Level2Desc::All::Format::ValueMask_TinyPage;
			break;
		case Fault:
		default:
			value	= Oscl::HW::ARM::MMU::Level2Desc::All::Format::ValueMask_Fault;
			break;
		}
	_desc	&= ~Oscl::HW::ARM::MMU::Level2Desc::All::Format::FieldMask;
	_desc	|= value;
	}

Level2Descriptor::DescType	Level2Descriptor::getDescType() const noexcept{
	Oscl::HW::ARM::MMU::Level2Desc::All::Reg	value	= _desc;
	value	&= ~Oscl::HW::ARM::MMU::Level2Desc::All::Format::FieldMask;
	switch(value){
		case Oscl::HW::ARM::MMU::Level2Desc::All::Format::ValueMask_LargePage:
			return Large;
		case Oscl::HW::ARM::MMU::Level2Desc::All::Format::ValueMask_SmallPage:
			return Small;
		case Oscl::HW::ARM::MMU::Level2Desc::All::Format::ValueMask_TinyPage:
			return Tiny;
		case Oscl::HW::ARM::MMU::Level2Desc::All::Format::ValueMask_Fault:
		default:
			return Fault;
		}
	}

Level2LargeDescriptor*	Level2Descriptor::getLargeDescriptor() const noexcept{
	if((_desc & Oscl::HW::ARM::MMU::Level2Desc::All::Format::FieldMask)==Oscl::HW::ARM::MMU::Level2Desc::All::Format::ValueMask_LargePage){
		return (Level2LargeDescriptor*)this;
		}
	return 0;
	}

Level2SmallDescriptor*	Level2Descriptor::getSmallDescriptor() const noexcept{
	if((_desc & Oscl::HW::ARM::MMU::Level2Desc::All::Format::FieldMask)==Oscl::HW::ARM::MMU::Level2Desc::All::Format::ValueMask_SmallPage){
		return (Level2SmallDescriptor*)this;
		}
	return 0;
	}

Level2TinyDescriptor*	Level2Descriptor::getTinyDescriptor() const noexcept{
	if((_desc & Oscl::HW::ARM::MMU::Level2Desc::All::Format::FieldMask)==Oscl::HW::ARM::MMU::Level2Desc::All::Format::ValueMask_TinyPage){
		return (Level2TinyDescriptor*)this;
		}
	return 0;
	}

void	Level2LargeDescriptor::setPageBase(void* page) noexcept{
	_desc	&= ~(Oscl::HW::ARM::MMU::Level2Desc::LargePage::PageBaseAddress::FieldMask);
	_desc	|= (((unsigned long)page) & Oscl::HW::ARM::MMU::Level2Desc::LargePage::PageBaseAddress::FieldMask);
	}

void*	Level2LargeDescriptor::getPageBase() const noexcept{
	return (void*)(_desc & Oscl::HW::ARM::MMU::Level2Desc::LargePage::PageBaseAddress::FieldMask);
	}

class LargeAP0 : public Oscl::ARM::Mmu::AP::Handler {
	private:
		/** */
		Oscl::HW::ARM::MMU::Level2Desc::LargePage::Reg&		_desc;
	public:
		/** */
		LargeAP0(Oscl::HW::ARM::MMU::Level2Desc::LargePage::Reg& desc) noexcept;
	private:
		/** */
		void	determinedBySR() noexcept;
		/** */
		void	privRwUserNoAccess() noexcept;
		/** */
		void	privRwUserReadOnly() noexcept;
		/** */
		void	privRwUserReadWrite() noexcept;
	};

LargeAP0::LargeAP0(Oscl::HW::ARM::MMU::Level2Desc::LargePage::Reg& desc) noexcept:
		_desc(desc)
		{
	}

void	LargeAP0::determinedBySR() noexcept{
	_desc	&= ~(Oscl::HW::ARM::MMU::Level2Desc::LargePage::AP0::FieldMask);
	_desc	|= Oscl::HW::ARM::MMU::Level2Desc::LargePage::AP0::ValueMask_PrivNoAccessUserNoAccessS0R0;
	}

void	LargeAP0::privRwUserNoAccess() noexcept{
	_desc	&= ~(Oscl::HW::ARM::MMU::Level2Desc::LargePage::AP0::FieldMask);
	_desc	|= Oscl::HW::ARM::MMU::Level2Desc::LargePage::AP0::ValueMask_PrivReadWriteUserNoAccess;
	}

void	LargeAP0::privRwUserReadOnly() noexcept{
	_desc	&= ~(Oscl::HW::ARM::MMU::Level2Desc::LargePage::AP0::FieldMask);
	_desc	|= Oscl::HW::ARM::MMU::Level2Desc::LargePage::AP0::ValueMask_PrivReadWriteUserReadOnly;
	}

void	LargeAP0::privRwUserReadWrite() noexcept{
	_desc	&= ~(Oscl::HW::ARM::MMU::Level2Desc::LargePage::AP0::FieldMask);
	_desc	|= Oscl::HW::ARM::MMU::Level2Desc::LargePage::AP0::ValueMask_PrivReadWriteUserReadWrite;
	}

void	Level2LargeDescriptor::setAP0(const Oscl::ARM::Mmu::AP::Api& ap) noexcept{
	LargeAP0	handler(_desc);
	ap.query(handler);
	}

const Oscl::ARM::Mmu::AP::Api&	Level2LargeDescriptor::getAP0() const noexcept{
	Oscl::HW::ARM::MMU::Level2Desc::LargePage::Reg	value	= _desc;
	value	&= ~(Oscl::HW::ARM::MMU::Level2Desc::LargePage::AP0::FieldMask);
	switch(value){
		case Oscl::HW::ARM::MMU::Level2Desc::LargePage::AP0::ValueMask_PrivNoAccessUserNoAccessS0R0:
			return Oscl::ARM::Mmu::AP::determinedBySR;
		case Oscl::HW::ARM::MMU::Level2Desc::LargePage::AP0::ValueMask_PrivReadWriteUserNoAccess:
			return Oscl::ARM::Mmu::AP::privilegedReadWriteUserNoAccess;
		case Oscl::HW::ARM::MMU::Level2Desc::LargePage::AP0::ValueMask_PrivReadWriteUserReadOnly:
			return Oscl::ARM::Mmu::AP::privilegedReadWriteUserReadOnly;
		case Oscl::HW::ARM::MMU::Level2Desc::LargePage::AP0::ValueMask_PrivReadWriteUserReadWrite:
			return Oscl::ARM::Mmu::AP::privilegedReadWriteUserReadWrite;
		default:
			return Oscl::ARM::Mmu::AP::determinedBySR;
		}
	}

class LargeAP1 : public Oscl::ARM::Mmu::AP::Handler {
	private:
		/** */
		Oscl::HW::ARM::MMU::Level2Desc::LargePage::Reg&		_desc;
	public:
		/** */
		LargeAP1(Oscl::HW::ARM::MMU::Level2Desc::LargePage::Reg& desc) noexcept;
	private:
		/** */
		void	determinedBySR() noexcept;
		/** */
		void	privRwUserNoAccess() noexcept;
		/** */
		void	privRwUserReadOnly() noexcept;
		/** */
		void	privRwUserReadWrite() noexcept;
	};

LargeAP1::LargeAP1(Oscl::HW::ARM::MMU::Level2Desc::LargePage::Reg& desc) noexcept:
		_desc(desc)
		{
	}

void	LargeAP1::determinedBySR() noexcept{
	_desc	&= ~(Oscl::HW::ARM::MMU::Level2Desc::LargePage::AP1::FieldMask);
	_desc	|= Oscl::HW::ARM::MMU::Level2Desc::LargePage::AP1::ValueMask_PrivNoAccessUserNoAccessS0R0;
	}

void	LargeAP1::privRwUserNoAccess() noexcept{
	_desc	&= ~(Oscl::HW::ARM::MMU::Level2Desc::LargePage::AP1::FieldMask);
	_desc	|= Oscl::HW::ARM::MMU::Level2Desc::LargePage::AP1::ValueMask_PrivReadWriteUserNoAccess;
	}

void	LargeAP1::privRwUserReadOnly() noexcept{
	_desc	&= ~(Oscl::HW::ARM::MMU::Level2Desc::LargePage::AP1::FieldMask);
	_desc	|= Oscl::HW::ARM::MMU::Level2Desc::LargePage::AP1::ValueMask_PrivReadWriteUserReadOnly;
	}

void	LargeAP1::privRwUserReadWrite() noexcept{
	_desc	&= ~(Oscl::HW::ARM::MMU::Level2Desc::LargePage::AP1::FieldMask);
	_desc	|= Oscl::HW::ARM::MMU::Level2Desc::LargePage::AP1::ValueMask_PrivReadWriteUserReadWrite;
	}

void	Level2LargeDescriptor::setAP1(const Oscl::ARM::Mmu::AP::Api& ap) noexcept{
	LargeAP1	handler(_desc);
	ap.query(handler);
	}

const Oscl::ARM::Mmu::AP::Api&	Level2LargeDescriptor::getAP1() const noexcept{
	Oscl::HW::ARM::MMU::Level2Desc::LargePage::Reg	value	= _desc;
	value	&= ~(Oscl::HW::ARM::MMU::Level2Desc::LargePage::AP1::FieldMask);
	switch(value){
		case Oscl::HW::ARM::MMU::Level2Desc::LargePage::AP1::ValueMask_PrivNoAccessUserNoAccessS0R0:
			return Oscl::ARM::Mmu::AP::determinedBySR;
		case Oscl::HW::ARM::MMU::Level2Desc::LargePage::AP1::ValueMask_PrivReadWriteUserNoAccess:
			return Oscl::ARM::Mmu::AP::privilegedReadWriteUserNoAccess;
		case Oscl::HW::ARM::MMU::Level2Desc::LargePage::AP1::ValueMask_PrivReadWriteUserReadOnly:
			return Oscl::ARM::Mmu::AP::privilegedReadWriteUserReadOnly;
		case Oscl::HW::ARM::MMU::Level2Desc::LargePage::AP1::ValueMask_PrivReadWriteUserReadWrite:
			return Oscl::ARM::Mmu::AP::privilegedReadWriteUserReadWrite;
		default:
			return Oscl::ARM::Mmu::AP::determinedBySR;
		}
	}

class LargeAP2 : public Oscl::ARM::Mmu::AP::Handler {
	private:
		/** */
		Oscl::HW::ARM::MMU::Level2Desc::LargePage::Reg&		_desc;
	public:
		/** */
		LargeAP2(Oscl::HW::ARM::MMU::Level2Desc::LargePage::Reg& desc) noexcept;
	private:
		/** */
		void	determinedBySR() noexcept;
		/** */
		void	privRwUserNoAccess() noexcept;
		/** */
		void	privRwUserReadOnly() noexcept;
		/** */
		void	privRwUserReadWrite() noexcept;
	};

LargeAP2::LargeAP2(Oscl::HW::ARM::MMU::Level2Desc::LargePage::Reg& desc) noexcept:
		_desc(desc)
		{
	}

void	LargeAP2::determinedBySR() noexcept{
	_desc	&= ~(Oscl::HW::ARM::MMU::Level2Desc::LargePage::AP2::FieldMask);
	_desc	|= Oscl::HW::ARM::MMU::Level2Desc::LargePage::AP2::ValueMask_PrivNoAccessUserNoAccessS0R0;
	}

void	LargeAP2::privRwUserNoAccess() noexcept{
	_desc	&= ~(Oscl::HW::ARM::MMU::Level2Desc::LargePage::AP2::FieldMask);
	_desc	|= Oscl::HW::ARM::MMU::Level2Desc::LargePage::AP2::ValueMask_PrivReadWriteUserNoAccess;
	}

void	LargeAP2::privRwUserReadOnly() noexcept{
	_desc	&= ~(Oscl::HW::ARM::MMU::Level2Desc::LargePage::AP2::FieldMask);
	_desc	|= Oscl::HW::ARM::MMU::Level2Desc::LargePage::AP2::ValueMask_PrivReadWriteUserReadOnly;
	}

void	LargeAP2::privRwUserReadWrite() noexcept{
	_desc	&= ~(Oscl::HW::ARM::MMU::Level2Desc::LargePage::AP2::FieldMask);
	_desc	|= Oscl::HW::ARM::MMU::Level2Desc::LargePage::AP2::ValueMask_PrivReadWriteUserReadWrite;
	}

void	Level2LargeDescriptor::setAP2(const Oscl::ARM::Mmu::AP::Api& ap) noexcept{
	LargeAP2	handler(_desc);
	ap.query(handler);
	}

const Oscl::ARM::Mmu::AP::Api&	Level2LargeDescriptor::getAP2() const noexcept{
	Oscl::HW::ARM::MMU::Level2Desc::LargePage::Reg	value	= _desc;
	value	&= ~(Oscl::HW::ARM::MMU::Level2Desc::LargePage::AP2::FieldMask);
	switch(value){
		case Oscl::HW::ARM::MMU::Level2Desc::LargePage::AP2::ValueMask_PrivNoAccessUserNoAccessS0R0:
			return Oscl::ARM::Mmu::AP::determinedBySR;
		case Oscl::HW::ARM::MMU::Level2Desc::LargePage::AP2::ValueMask_PrivReadWriteUserNoAccess:
			return Oscl::ARM::Mmu::AP::privilegedReadWriteUserNoAccess;
		case Oscl::HW::ARM::MMU::Level2Desc::LargePage::AP2::ValueMask_PrivReadWriteUserReadOnly:
			return Oscl::ARM::Mmu::AP::privilegedReadWriteUserReadOnly;
		case Oscl::HW::ARM::MMU::Level2Desc::LargePage::AP2::ValueMask_PrivReadWriteUserReadWrite:
			return Oscl::ARM::Mmu::AP::privilegedReadWriteUserReadWrite;
		default:
			return Oscl::ARM::Mmu::AP::determinedBySR;
		}
	}

class LargeAP3 : public Oscl::ARM::Mmu::AP::Handler {
	private:
		/** */
		Oscl::HW::ARM::MMU::Level2Desc::LargePage::Reg&		_desc;
	public:
		/** */
		LargeAP3(Oscl::HW::ARM::MMU::Level2Desc::LargePage::Reg& desc) noexcept;
	private:
		/** */
		void	determinedBySR() noexcept;
		/** */
		void	privRwUserNoAccess() noexcept;
		/** */
		void	privRwUserReadOnly() noexcept;
		/** */
		void	privRwUserReadWrite() noexcept;
	};

LargeAP3::LargeAP3(Oscl::HW::ARM::MMU::Level2Desc::LargePage::Reg& desc) noexcept:
		_desc(desc)
		{
	}

void	LargeAP3::determinedBySR() noexcept{
	_desc	&= ~(Oscl::HW::ARM::MMU::Level2Desc::LargePage::AP3::FieldMask);
	_desc	|= Oscl::HW::ARM::MMU::Level2Desc::LargePage::AP3::ValueMask_PrivNoAccessUserNoAccessS0R0;
	}

void	LargeAP3::privRwUserNoAccess() noexcept{
	_desc	&= ~(Oscl::HW::ARM::MMU::Level2Desc::LargePage::AP3::FieldMask);
	_desc	|= Oscl::HW::ARM::MMU::Level2Desc::LargePage::AP3::ValueMask_PrivReadWriteUserNoAccess;
	}

void	LargeAP3::privRwUserReadOnly() noexcept{
	_desc	&= ~(Oscl::HW::ARM::MMU::Level2Desc::LargePage::AP3::FieldMask);
	_desc	|= Oscl::HW::ARM::MMU::Level2Desc::LargePage::AP3::ValueMask_PrivReadWriteUserReadOnly;
	}

void	LargeAP3::privRwUserReadWrite() noexcept{
	_desc	&= ~(Oscl::HW::ARM::MMU::Level2Desc::LargePage::AP3::FieldMask);
	_desc	|= Oscl::HW::ARM::MMU::Level2Desc::LargePage::AP3::ValueMask_PrivReadWriteUserReadWrite;
	}

void	Level2LargeDescriptor::setAP3(const Oscl::ARM::Mmu::AP::Api& ap) noexcept{
	LargeAP3	handler(_desc);
	ap.query(handler);
	}

const Oscl::ARM::Mmu::AP::Api&	Level2LargeDescriptor::getAP3() const noexcept{
	Oscl::HW::ARM::MMU::Level2Desc::LargePage::Reg	value	= _desc;
	value	&= ~(Oscl::HW::ARM::MMU::Level2Desc::LargePage::AP3::FieldMask);
	switch(value){
		case Oscl::HW::ARM::MMU::Level2Desc::LargePage::AP3::ValueMask_PrivNoAccessUserNoAccessS0R0:
			return Oscl::ARM::Mmu::AP::determinedBySR;
		case Oscl::HW::ARM::MMU::Level2Desc::LargePage::AP3::ValueMask_PrivReadWriteUserNoAccess:
			return Oscl::ARM::Mmu::AP::privilegedReadWriteUserNoAccess;
		case Oscl::HW::ARM::MMU::Level2Desc::LargePage::AP3::ValueMask_PrivReadWriteUserReadOnly:
			return Oscl::ARM::Mmu::AP::privilegedReadWriteUserReadOnly;
		case Oscl::HW::ARM::MMU::Level2Desc::LargePage::AP3::ValueMask_PrivReadWriteUserReadWrite:
			return Oscl::ARM::Mmu::AP::privilegedReadWriteUserReadWrite;
		default:
			return Oscl::ARM::Mmu::AP::determinedBySR;
		}
	}

void	Level2LargeDescriptor::setBufferable() noexcept{
	_desc	&= ~(Oscl::HW::ARM::MMU::Level2Desc::LargePage::B::FieldMask);
	_desc	|= Oscl::HW::ARM::MMU::Level2Desc::LargePage::B::ValueMask_Buffered;
	}

void	Level2LargeDescriptor::setNotBufferable() noexcept{
	_desc	&= ~(Oscl::HW::ARM::MMU::Level2Desc::LargePage::B::FieldMask);
	_desc	|= Oscl::HW::ARM::MMU::Level2Desc::LargePage::B::ValueMask_Unbuffered;
	}

bool	Level2LargeDescriptor::isBufferable() const noexcept{
	return (_desc & Oscl::HW::ARM::MMU::Level2Desc::LargePage::B::FieldMask) == Oscl::HW::ARM::MMU::Level2Desc::LargePage::B::ValueMask_Buffered;
	}

void	Level2LargeDescriptor::setCachable() noexcept{
	_desc	&= ~(Oscl::HW::ARM::MMU::Level2Desc::LargePage::C::FieldMask);
	_desc	|= Oscl::HW::ARM::MMU::Level2Desc::LargePage::C::ValueMask_Cached;
	}

void	Level2LargeDescriptor::setNotCachable() noexcept{
	_desc	&= ~(Oscl::HW::ARM::MMU::Level2Desc::LargePage::C::FieldMask);
	_desc	|= Oscl::HW::ARM::MMU::Level2Desc::LargePage::C::ValueMask_Uncached;
	}

bool	Level2LargeDescriptor::isCachable() const noexcept{
	return (_desc & Oscl::HW::ARM::MMU::Level2Desc::LargePage::C::FieldMask) == Oscl::HW::ARM::MMU::Level2Desc::LargePage::C::ValueMask_Cached;
	}

void	Level2SmallDescriptor::setPageBase(void* page) noexcept{
	_desc	&= ~(Oscl::HW::ARM::MMU::Level2Desc::SmallPage::PageBaseAddress::FieldMask);
	_desc	|= (((unsigned long)page) & Oscl::HW::ARM::MMU::Level2Desc::SmallPage::PageBaseAddress::FieldMask);
	}

void*	Level2SmallDescriptor::getPageBase() const noexcept{
	return (void*)(_desc & Oscl::HW::ARM::MMU::Level2Desc::SmallPage::PageBaseAddress::FieldMask);
	}

class SmallAP0 : public Oscl::ARM::Mmu::AP::Handler {
	private:
		/** */
		Oscl::HW::ARM::MMU::Level2Desc::SmallPage::Reg&		_desc;
	public:
		/** */
		SmallAP0(Oscl::HW::ARM::MMU::Level2Desc::SmallPage::Reg& desc) noexcept;
	private:
		/** */
		void	determinedBySR() noexcept;
		/** */
		void	privRwUserNoAccess() noexcept;
		/** */
		void	privRwUserReadOnly() noexcept;
		/** */
		void	privRwUserReadWrite() noexcept;
	};

SmallAP0::SmallAP0(Oscl::HW::ARM::MMU::Level2Desc::SmallPage::Reg& desc) noexcept:
		_desc(desc)
		{
	}

void	SmallAP0::determinedBySR() noexcept{
	_desc	&= ~(Oscl::HW::ARM::MMU::Level2Desc::SmallPage::AP0::FieldMask);
	_desc	|= Oscl::HW::ARM::MMU::Level2Desc::SmallPage::AP0::ValueMask_PrivNoAccessUserNoAccessS0R0;
	}

void	SmallAP0::privRwUserNoAccess() noexcept{
	_desc	&= ~(Oscl::HW::ARM::MMU::Level2Desc::SmallPage::AP0::FieldMask);
	_desc	|= Oscl::HW::ARM::MMU::Level2Desc::SmallPage::AP0::ValueMask_PrivReadWriteUserNoAccess;
	}

void	SmallAP0::privRwUserReadOnly() noexcept{
	_desc	&= ~(Oscl::HW::ARM::MMU::Level2Desc::SmallPage::AP0::FieldMask);
	_desc	|= Oscl::HW::ARM::MMU::Level2Desc::SmallPage::AP0::ValueMask_PrivReadWriteUserReadOnly;
	}

void	SmallAP0::privRwUserReadWrite() noexcept{
	_desc	&= ~(Oscl::HW::ARM::MMU::Level2Desc::SmallPage::AP0::FieldMask);
	_desc	|= Oscl::HW::ARM::MMU::Level2Desc::SmallPage::AP0::ValueMask_PrivReadWriteUserReadWrite;
	}

void	Level2SmallDescriptor::setAP0(const Oscl::ARM::Mmu::AP::Api& ap) noexcept{
	SmallAP0	handler(_desc);
	ap.query(handler);
	}

const Oscl::ARM::Mmu::AP::Api&	Level2SmallDescriptor::getAP0() const noexcept{
	Oscl::HW::ARM::MMU::Level2Desc::SmallPage::Reg	value	= _desc;
	value	&= ~(Oscl::HW::ARM::MMU::Level2Desc::SmallPage::AP0::FieldMask);
	switch(value){
		case Oscl::HW::ARM::MMU::Level2Desc::SmallPage::AP0::ValueMask_PrivNoAccessUserNoAccessS0R0:
			return Oscl::ARM::Mmu::AP::determinedBySR;
		case Oscl::HW::ARM::MMU::Level2Desc::SmallPage::AP0::ValueMask_PrivReadWriteUserNoAccess:
			return Oscl::ARM::Mmu::AP::privilegedReadWriteUserNoAccess;
		case Oscl::HW::ARM::MMU::Level2Desc::SmallPage::AP0::ValueMask_PrivReadWriteUserReadOnly:
			return Oscl::ARM::Mmu::AP::privilegedReadWriteUserReadOnly;
		case Oscl::HW::ARM::MMU::Level2Desc::SmallPage::AP0::ValueMask_PrivReadWriteUserReadWrite:
			return Oscl::ARM::Mmu::AP::privilegedReadWriteUserReadWrite;
		default:
			return Oscl::ARM::Mmu::AP::determinedBySR;
		}
	}

class SmallAP1 : public Oscl::ARM::Mmu::AP::Handler {
	private:
		/** */
		Oscl::HW::ARM::MMU::Level2Desc::SmallPage::Reg&		_desc;
	public:
		/** */
		SmallAP1(Oscl::HW::ARM::MMU::Level2Desc::SmallPage::Reg& desc) noexcept;
	private:
		/** */
		void	determinedBySR() noexcept;
		/** */
		void	privRwUserNoAccess() noexcept;
		/** */
		void	privRwUserReadOnly() noexcept;
		/** */
		void	privRwUserReadWrite() noexcept;
	};

SmallAP1::SmallAP1(Oscl::HW::ARM::MMU::Level2Desc::SmallPage::Reg& desc) noexcept:
		_desc(desc)
		{
	}

void	SmallAP1::determinedBySR() noexcept{
	_desc	&= ~(Oscl::HW::ARM::MMU::Level2Desc::SmallPage::AP1::FieldMask);
	_desc	|= Oscl::HW::ARM::MMU::Level2Desc::SmallPage::AP1::ValueMask_PrivNoAccessUserNoAccessS0R0;
	}

void	SmallAP1::privRwUserNoAccess() noexcept{
	_desc	&= ~(Oscl::HW::ARM::MMU::Level2Desc::SmallPage::AP1::FieldMask);
	_desc	|= Oscl::HW::ARM::MMU::Level2Desc::SmallPage::AP1::ValueMask_PrivReadWriteUserNoAccess;
	}

void	SmallAP1::privRwUserReadOnly() noexcept{
	_desc	&= ~(Oscl::HW::ARM::MMU::Level2Desc::SmallPage::AP1::FieldMask);
	_desc	|= Oscl::HW::ARM::MMU::Level2Desc::SmallPage::AP1::ValueMask_PrivReadWriteUserReadOnly;
	}

void	SmallAP1::privRwUserReadWrite() noexcept{
	_desc	&= ~(Oscl::HW::ARM::MMU::Level2Desc::SmallPage::AP1::FieldMask);
	_desc	|= Oscl::HW::ARM::MMU::Level2Desc::SmallPage::AP1::ValueMask_PrivReadWriteUserReadWrite;
	}

void	Level2SmallDescriptor::setAP1(const Oscl::ARM::Mmu::AP::Api& ap) noexcept{
	SmallAP1	handler(_desc);
	ap.query(handler);
	}

const Oscl::ARM::Mmu::AP::Api&	Level2SmallDescriptor::getAP1() const noexcept{
	Oscl::HW::ARM::MMU::Level2Desc::SmallPage::Reg	value	= _desc;
	value	&= ~(Oscl::HW::ARM::MMU::Level2Desc::SmallPage::AP1::FieldMask);
	switch(value){
		case Oscl::HW::ARM::MMU::Level2Desc::SmallPage::AP1::ValueMask_PrivNoAccessUserNoAccessS0R0:
			return Oscl::ARM::Mmu::AP::determinedBySR;
		case Oscl::HW::ARM::MMU::Level2Desc::SmallPage::AP1::ValueMask_PrivReadWriteUserNoAccess:
			return Oscl::ARM::Mmu::AP::privilegedReadWriteUserNoAccess;
		case Oscl::HW::ARM::MMU::Level2Desc::SmallPage::AP1::ValueMask_PrivReadWriteUserReadOnly:
			return Oscl::ARM::Mmu::AP::privilegedReadWriteUserReadOnly;
		case Oscl::HW::ARM::MMU::Level2Desc::SmallPage::AP1::ValueMask_PrivReadWriteUserReadWrite:
			return Oscl::ARM::Mmu::AP::privilegedReadWriteUserReadWrite;
		default:
			return Oscl::ARM::Mmu::AP::determinedBySR;
		}
	}

class SmallAP2 : public Oscl::ARM::Mmu::AP::Handler {
	private:
		/** */
		Oscl::HW::ARM::MMU::Level2Desc::SmallPage::Reg&		_desc;
	public:
		/** */
		SmallAP2(Oscl::HW::ARM::MMU::Level2Desc::SmallPage::Reg& desc) noexcept;
	private:
		/** */
		void	determinedBySR() noexcept;
		/** */
		void	privRwUserNoAccess() noexcept;
		/** */
		void	privRwUserReadOnly() noexcept;
		/** */
		void	privRwUserReadWrite() noexcept;
	};

SmallAP2::SmallAP2(Oscl::HW::ARM::MMU::Level2Desc::SmallPage::Reg& desc) noexcept:
		_desc(desc)
		{
	}

void	SmallAP2::determinedBySR() noexcept{
	_desc	&= ~(Oscl::HW::ARM::MMU::Level2Desc::SmallPage::AP2::FieldMask);
	_desc	|= Oscl::HW::ARM::MMU::Level2Desc::SmallPage::AP2::ValueMask_PrivNoAccessUserNoAccessS0R0;
	}

void	SmallAP2::privRwUserNoAccess() noexcept{
	_desc	&= ~(Oscl::HW::ARM::MMU::Level2Desc::SmallPage::AP2::FieldMask);
	_desc	|= Oscl::HW::ARM::MMU::Level2Desc::SmallPage::AP2::ValueMask_PrivReadWriteUserNoAccess;
	}

void	SmallAP2::privRwUserReadOnly() noexcept{
	_desc	&= ~(Oscl::HW::ARM::MMU::Level2Desc::SmallPage::AP2::FieldMask);
	_desc	|= Oscl::HW::ARM::MMU::Level2Desc::SmallPage::AP2::ValueMask_PrivReadWriteUserReadOnly;
	}

void	SmallAP2::privRwUserReadWrite() noexcept{
	_desc	&= ~(Oscl::HW::ARM::MMU::Level2Desc::SmallPage::AP2::FieldMask);
	_desc	|= Oscl::HW::ARM::MMU::Level2Desc::SmallPage::AP2::ValueMask_PrivReadWriteUserReadWrite;
	}

void	Level2SmallDescriptor::setAP2(const Oscl::ARM::Mmu::AP::Api& ap) noexcept{
	SmallAP2	handler(_desc);
	ap.query(handler);
	}

const Oscl::ARM::Mmu::AP::Api&	Level2SmallDescriptor::getAP2() const noexcept{
	Oscl::HW::ARM::MMU::Level2Desc::SmallPage::Reg	value	= _desc;
	value	&= ~(Oscl::HW::ARM::MMU::Level2Desc::SmallPage::AP2::FieldMask);
	switch(value){
		case Oscl::HW::ARM::MMU::Level2Desc::SmallPage::AP2::ValueMask_PrivNoAccessUserNoAccessS0R0:
			return Oscl::ARM::Mmu::AP::determinedBySR;
		case Oscl::HW::ARM::MMU::Level2Desc::SmallPage::AP2::ValueMask_PrivReadWriteUserNoAccess:
			return Oscl::ARM::Mmu::AP::privilegedReadWriteUserNoAccess;
		case Oscl::HW::ARM::MMU::Level2Desc::SmallPage::AP2::ValueMask_PrivReadWriteUserReadOnly:
			return Oscl::ARM::Mmu::AP::privilegedReadWriteUserReadOnly;
		case Oscl::HW::ARM::MMU::Level2Desc::SmallPage::AP2::ValueMask_PrivReadWriteUserReadWrite:
			return Oscl::ARM::Mmu::AP::privilegedReadWriteUserReadWrite;
		default:
			return Oscl::ARM::Mmu::AP::determinedBySR;
		}
	}

class SmallAP3 : public Oscl::ARM::Mmu::AP::Handler {
	private:
		/** */
		Oscl::HW::ARM::MMU::Level2Desc::SmallPage::Reg&		_desc;
	public:
		/** */
		SmallAP3(Oscl::HW::ARM::MMU::Level2Desc::SmallPage::Reg& desc) noexcept;
	private:
		/** */
		void	determinedBySR() noexcept;
		/** */
		void	privRwUserNoAccess() noexcept;
		/** */
		void	privRwUserReadOnly() noexcept;
		/** */
		void	privRwUserReadWrite() noexcept;
	};

SmallAP3::SmallAP3(Oscl::HW::ARM::MMU::Level2Desc::SmallPage::Reg& desc) noexcept:
		_desc(desc)
		{
	}

void	SmallAP3::determinedBySR() noexcept{
	_desc	&= ~(Oscl::HW::ARM::MMU::Level2Desc::SmallPage::AP3::FieldMask);
	_desc	|= Oscl::HW::ARM::MMU::Level2Desc::SmallPage::AP3::ValueMask_PrivNoAccessUserNoAccessS0R0;
	}

void	SmallAP3::privRwUserNoAccess() noexcept{
	_desc	&= ~(Oscl::HW::ARM::MMU::Level2Desc::SmallPage::AP3::FieldMask);
	_desc	|= Oscl::HW::ARM::MMU::Level2Desc::SmallPage::AP3::ValueMask_PrivReadWriteUserNoAccess;
	}

void	SmallAP3::privRwUserReadOnly() noexcept{
	_desc	&= ~(Oscl::HW::ARM::MMU::Level2Desc::SmallPage::AP3::FieldMask);
	_desc	|= Oscl::HW::ARM::MMU::Level2Desc::SmallPage::AP3::ValueMask_PrivReadWriteUserReadOnly;
	}

void	SmallAP3::privRwUserReadWrite() noexcept{
	_desc	&= ~(Oscl::HW::ARM::MMU::Level2Desc::SmallPage::AP3::FieldMask);
	_desc	|= Oscl::HW::ARM::MMU::Level2Desc::SmallPage::AP3::ValueMask_PrivReadWriteUserReadWrite;
	}

void	Level2SmallDescriptor::setAP3(const Oscl::ARM::Mmu::AP::Api& ap) noexcept{
	SmallAP3	handler(_desc);
	ap.query(handler);
	}

const Oscl::ARM::Mmu::AP::Api&	Level2SmallDescriptor::getAP3() const noexcept{
	Oscl::HW::ARM::MMU::Level2Desc::SmallPage::Reg	value	= _desc;
	value	&= ~(Oscl::HW::ARM::MMU::Level2Desc::SmallPage::AP3::FieldMask);
	switch(value){
		case Oscl::HW::ARM::MMU::Level2Desc::SmallPage::AP3::ValueMask_PrivNoAccessUserNoAccessS0R0:
			return Oscl::ARM::Mmu::AP::determinedBySR;
		case Oscl::HW::ARM::MMU::Level2Desc::SmallPage::AP3::ValueMask_PrivReadWriteUserNoAccess:
			return Oscl::ARM::Mmu::AP::privilegedReadWriteUserNoAccess;
		case Oscl::HW::ARM::MMU::Level2Desc::SmallPage::AP3::ValueMask_PrivReadWriteUserReadOnly:
			return Oscl::ARM::Mmu::AP::privilegedReadWriteUserReadOnly;
		case Oscl::HW::ARM::MMU::Level2Desc::SmallPage::AP3::ValueMask_PrivReadWriteUserReadWrite:
			return Oscl::ARM::Mmu::AP::privilegedReadWriteUserReadWrite;
		default:
			return Oscl::ARM::Mmu::AP::determinedBySR;
		}
	}

void	Level2SmallDescriptor::setBufferable() noexcept{
	_desc	&= ~(Oscl::HW::ARM::MMU::Level2Desc::SmallPage::B::FieldMask);
	_desc	|= Oscl::HW::ARM::MMU::Level2Desc::SmallPage::B::ValueMask_Buffered;
	}

void	Level2SmallDescriptor::setNotBufferable() noexcept{
	_desc	&= ~(Oscl::HW::ARM::MMU::Level2Desc::SmallPage::B::FieldMask);
	_desc	|= Oscl::HW::ARM::MMU::Level2Desc::SmallPage::B::ValueMask_Unbuffered;
	}

bool	Level2SmallDescriptor::isBufferable() const noexcept{
	return (_desc & Oscl::HW::ARM::MMU::Level2Desc::SmallPage::B::FieldMask) == Oscl::HW::ARM::MMU::Level2Desc::SmallPage::B::ValueMask_Buffered;
	}

void	Level2SmallDescriptor::setCachable() noexcept{
	_desc	&= ~(Oscl::HW::ARM::MMU::Level2Desc::SmallPage::C::FieldMask);
	_desc	|= Oscl::HW::ARM::MMU::Level2Desc::SmallPage::C::ValueMask_Cached;
	}

void	Level2SmallDescriptor::setNotCachable() noexcept{
	_desc	&= ~(Oscl::HW::ARM::MMU::Level2Desc::SmallPage::C::FieldMask);
	_desc	|= Oscl::HW::ARM::MMU::Level2Desc::SmallPage::C::ValueMask_Uncached;
	}

bool	Level2SmallDescriptor::isCachable() const noexcept{
	return (_desc & Oscl::HW::ARM::MMU::Level2Desc::SmallPage::C::FieldMask) == Oscl::HW::ARM::MMU::Level2Desc::SmallPage::C::ValueMask_Cached;
	}

void	Level2TinyDescriptor::setPageBase(void* page) noexcept{
	_desc	&= ~(Oscl::HW::ARM::MMU::Level2Desc::TinyPage::PageBaseAddress::FieldMask);
	_desc	|= (((unsigned long)page) & Oscl::HW::ARM::MMU::Level2Desc::TinyPage::PageBaseAddress::FieldMask);
	}

void*	Level2TinyDescriptor::getPageBase() const noexcept{
	return (void*)(_desc & Oscl::HW::ARM::MMU::Level2Desc::TinyPage::PageBaseAddress::FieldMask);
	}

class TinyAP : public Oscl::ARM::Mmu::AP::Handler {
	private:
		/** */
		Oscl::HW::ARM::MMU::Level2Desc::TinyPage::Reg&		_desc;
	public:
		/** */
		TinyAP(Oscl::HW::ARM::MMU::Level2Desc::TinyPage::Reg& desc) noexcept;
	private:
		/** */
		void	determinedBySR() noexcept;
		/** */
		void	privRwUserNoAccess() noexcept;
		/** */
		void	privRwUserReadOnly() noexcept;
		/** */
		void	privRwUserReadWrite() noexcept;
	};

TinyAP::TinyAP(Oscl::HW::ARM::MMU::Level2Desc::TinyPage::Reg& desc) noexcept:
		_desc(desc)
		{
	}

void	TinyAP::determinedBySR() noexcept{
	_desc	&= ~(Oscl::HW::ARM::MMU::Level2Desc::TinyPage::AP::FieldMask);
	_desc	|= Oscl::HW::ARM::MMU::Level2Desc::TinyPage::AP::ValueMask_PrivNoAccessUserNoAccessS0R0;
	}

void	TinyAP::privRwUserNoAccess() noexcept{
	_desc	&= ~(Oscl::HW::ARM::MMU::Level2Desc::TinyPage::AP::FieldMask);
	_desc	|= Oscl::HW::ARM::MMU::Level2Desc::TinyPage::AP::ValueMask_PrivReadWriteUserNoAccess;
	}

void	TinyAP::privRwUserReadOnly() noexcept{
	_desc	&= ~(Oscl::HW::ARM::MMU::Level2Desc::TinyPage::AP::FieldMask);
	_desc	|= Oscl::HW::ARM::MMU::Level2Desc::TinyPage::AP::ValueMask_PrivReadWriteUserReadOnly;
	}

void	TinyAP::privRwUserReadWrite() noexcept{
	_desc	&= ~(Oscl::HW::ARM::MMU::Level2Desc::TinyPage::AP::FieldMask);
	_desc	|= Oscl::HW::ARM::MMU::Level2Desc::TinyPage::AP::ValueMask_PrivReadWriteUserReadWrite;
	}

void	Level2TinyDescriptor::setAP(const Oscl::ARM::Mmu::AP::Api& ap) noexcept{
	TinyAP	handler(_desc);
	ap.query(handler);
	}

const Oscl::ARM::Mmu::AP::Api&	Level2TinyDescriptor::getAP() const noexcept{
	Oscl::HW::ARM::MMU::Level2Desc::TinyPage::Reg	value	= _desc;
	value	&= ~(Oscl::HW::ARM::MMU::Level2Desc::TinyPage::AP::FieldMask);
	switch(value){
		case Oscl::HW::ARM::MMU::Level2Desc::TinyPage::AP::ValueMask_PrivNoAccessUserNoAccessS0R0:
			return Oscl::ARM::Mmu::AP::determinedBySR;
		case Oscl::HW::ARM::MMU::Level2Desc::TinyPage::AP::ValueMask_PrivReadWriteUserNoAccess:
			return Oscl::ARM::Mmu::AP::privilegedReadWriteUserNoAccess;
		case Oscl::HW::ARM::MMU::Level2Desc::TinyPage::AP::ValueMask_PrivReadWriteUserReadOnly:
			return Oscl::ARM::Mmu::AP::privilegedReadWriteUserReadOnly;
		case Oscl::HW::ARM::MMU::Level2Desc::TinyPage::AP::ValueMask_PrivReadWriteUserReadWrite:
			return Oscl::ARM::Mmu::AP::privilegedReadWriteUserReadWrite;
		default:
			return Oscl::ARM::Mmu::AP::determinedBySR;
		}
	}

void	Level2TinyDescriptor::setBufferable() noexcept{
	_desc	&= ~(Oscl::HW::ARM::MMU::Level2Desc::TinyPage::B::FieldMask);
	_desc	|= Oscl::HW::ARM::MMU::Level2Desc::TinyPage::B::ValueMask_Buffered;
	}

void	Level2TinyDescriptor::setNotBufferable() noexcept{
	_desc	&= ~(Oscl::HW::ARM::MMU::Level2Desc::TinyPage::B::FieldMask);
	_desc	|= Oscl::HW::ARM::MMU::Level2Desc::TinyPage::B::ValueMask_Unbuffered;
	}

bool	Level2TinyDescriptor::isBufferable() const noexcept{
	return (_desc & Oscl::HW::ARM::MMU::Level2Desc::TinyPage::B::FieldMask) == Oscl::HW::ARM::MMU::Level2Desc::TinyPage::B::ValueMask_Buffered;
	}

void	Level2TinyDescriptor::setCachable() noexcept{
	_desc	&= ~(Oscl::HW::ARM::MMU::Level2Desc::TinyPage::C::FieldMask);
	_desc	|= Oscl::HW::ARM::MMU::Level2Desc::TinyPage::C::ValueMask_Cached;
	}

void	Level2TinyDescriptor::setNotCachable() noexcept{
	_desc	&= ~(Oscl::HW::ARM::MMU::Level2Desc::TinyPage::C::FieldMask);
	_desc	|= Oscl::HW::ARM::MMU::Level2Desc::TinyPage::C::ValueMask_Uncached;
	}

bool	Level2TinyDescriptor::isCachable() const noexcept{
	return (_desc & Oscl::HW::ARM::MMU::Level2Desc::TinyPage::C::FieldMask) == Oscl::HW::ARM::MMU::Level2Desc::TinyPage::C::ValueMask_Cached;
	}

CoarseLevel2Table::CoarseLevel2Table() noexcept { }

Level2Descriptor* CoarseLevel2Table::getDesc(unsigned entry) noexcept{
	if(entry > (nEntriesInCoarseLevel2Table-1)){
		return 0;
		}
	return &_table[entry];
	}

Level2Descriptor* CoarseLevel2Table::getDesc(void* virtualAddr) noexcept{
	return getDesc(	(unsigned)(
					(	((unsigned long)virtualAddr)
							& Oscl::HW::ARM::MMU::VirtualAddress::CoarseTableIndex::FieldMask)
							>> Oscl::HW::ARM::MMU::VirtualAddress::CoarseTableIndex::Lsb
						)
					);
	}

FineLevel2Table::FineLevel2Table() noexcept{ }

Level2Descriptor* FineLevel2Table::getDesc(unsigned entry) noexcept{
	if(entry > (nEntriesInFineLevel2Table-1)){
		return 0;
		}
	return &_table[entry];
	}

Level2Descriptor* FineLevel2Table::getDesc(void* virtualAddr) noexcept{
	return getDesc(	(unsigned)(
					(	((unsigned long)virtualAddr)
							& Oscl::HW::ARM::MMU::VirtualAddress::FineTableIndex::FieldMask)
							>> Oscl::HW::ARM::MMU::VirtualAddress::FineTableIndex::Lsb
						)
					);
	}

