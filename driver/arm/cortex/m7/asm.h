/*
   Copyright (C) 2024 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/


#ifndef _oscl_drv_arm_cortex_m7_asmh_
#define _oscl_drv_arm_cortex_m7_asmh_
#include "oscl/hw/arm/cortexm7/map.h"

/** */
namespace Oscl {

/** */
namespace Arm {

/** */
namespace Cortex {

/** */
namespace M7 {

inline void	waitForInterrupt() noexcept{
	asm volatile(" wfi");
	}

inline void invalidateEntireInstructionCache() noexcept{
	CortexM7CACHE.iciallu	= 0;
    }

inline void	invalidateInstructionCacheLineByVirtualAddress(unsigned long virtualAddress) noexcept{
	CortexM7CACHE.icimvau	= virtualAddress;
	}

inline void	invalidateDataCacheLineByVirtualAddress(unsigned long virtualAddress) noexcept{
	CortexM7CACHE.dcimvac	= virtualAddress;
	}

inline void	cleanDataCacheLineByVirtualAddress(unsigned long virtualAddress) noexcept{
	CortexM7CACHE.dccmvau	= virtualAddress;
	}

}
}
}
}

#endif
