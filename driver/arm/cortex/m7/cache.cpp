/*
   Copyright (C) 2024 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "cache.h"
#include "oscl/hw/arm/cortexm7/map.h"
#include "oscl/hw/arm/cortexm7/map.h"
#include "oscl/driver/cortexm/asm.h"

using namespace Oscl::Arm::Cortex;

/* CSSIDR: 0xf00fe019 (STM32H7B3)
	1111 0000 0000 1111 1110 0000 0001 1001
	1 1 1 1 000000001111111 0000000011 001
	WT = 1 Write-Through support available
	WB = 1 Write-Back support available
	RA = 1 Read allocation support available
	WA = 1 Write allocation support available
	NumSets = 000 0000 0111 1111 = 0x007F = 127 + 1 = 128
	Associativity = 00 0000 0011 = 0x003 = 3 + 1 = 4
	LineSize = 001 = 1 = 8 words = 32 bytes per line
 */
static inline void csslerParameters(
	unsigned&	nSets,
	unsigned&	nWays,
	unsigned&	lineSize,
	unsigned&	lineShift
	) noexcept {
	auto
	ccsidr	= CortexM7FEATURE.ccsidr;

	auto ls	= ccsidr;
	ls	&= Oscl::Arm::CortexM7::pFEATURE::rCCSIDR::fLineSize::FieldMask;
	ls	>>= Oscl::Arm::CortexM7::pFEATURE::rCCSIDR::fLineSize::Lsb;
	// The LineSize field is encoded as 2 less than log(2) of the number of words in the cache line.
	ls	+= 2;
	lineSize	= (1U << ls) * 4;	// 4 bytes per word
	lineShift	= ls + 2;	// Convert shift from words to bytes

	auto numSets	= ccsidr;
	numSets	&= Oscl::Arm::CortexM7::pFEATURE::rCCSIDR::fNumSets::FieldMask;
	numSets	>>= Oscl::Arm::CortexM7::pFEATURE::rCCSIDR::fNumSets::Lsb;
	++numSets;
	nSets	= numSets;

	auto associativity	= ccsidr;
	associativity	&= Oscl::Arm::CortexM7::pFEATURE::rCCSIDR::fAssociativity::FieldMask;
	associativity	>>= Oscl::Arm::CortexM7::pFEATURE::rCCSIDR::fAssociativity::Lsb;
	++associativity;
	nWays	= associativity;

	}

void M7::cleanEntireDataCache() noexcept {
	CortexM7FEATURE.csselr	= Oscl::Arm::CortexM7::pFEATURE::rCSSELR::fInD::ValueMask_dataCache;

	Oscl::CortexM::dsb();

	/* CSSIDR: 0xf00fe019
		1111 0000 0000 1111 1110 0000 0001 1001
		1 1 1 1 000000001111111 0000000011 001
		WT = 1 Write-Through support available
		WB = 1 Write-Back support available
		RA = 1 Read allocation support available
		WA = 1 Write allocation support available
		NumSets = 000 0000 0111 1111 = 0x007F = 127 + 1 = 128
		Associativity = 00 0000 0011 = 0x003 = 3 + 1 = 4
		LineSize = 001 = 1 = 8 words = 32 bytes per line
	 */

	unsigned	nSets;
	unsigned	nWays;
	unsigned	lineSize;
	unsigned	lineShift;

	csslerParameters(
		nSets,
		nWays,
		lineSize,
		lineShift
		);

	for( unsigned s=0; s < nSets; ++s ){
		for( unsigned w=0; w < nWays; ++w ){
			CortexM7CACHE.dccsw	= (
					(s << Oscl::Arm::CortexM7::pCACHE::rDCCSW::fSet::Lsb)
				|	(w << Oscl::Arm::CortexM7::pCACHE::rDCCSW::fWay::Lsb)
				);
			}
		}

	Oscl::CortexM::dsb();
	Oscl::CortexM::isb();
	}

void M7::cleanAndInvalidateEntireDataCache() noexcept {
	CortexM7FEATURE.csselr	= Oscl::Arm::CortexM7::pFEATURE::rCSSELR::fInD::ValueMask_dataCache;

	Oscl::CortexM::dsb();

	/* CSSIDR: 0xf00fe019
		1111 0000 0000 1111 1110 0000 0001 1001
		1 1 1 1 000000001111111 0000000011 001
		WT = 1 Write-Through support available
		WB = 1 Write-Back support available
		RA = 1 Read allocation support available
		WA = 1 Write allocation support available
		NumSets = 000 0000 0111 1111 = 0x007F = 127 + 1 = 128
		Associativity = 00 0000 0011 = 0x003 = 3 + 1 = 4
		LineSize = 001 = 1 = 8 words = 32 bytes per line
	 */

	unsigned	nSets;
	unsigned	nWays;
	unsigned	lineSize;
	unsigned	lineShift;

	csslerParameters(
		nSets,
		nWays,
		lineSize,
		lineShift
		);

	for( unsigned s=0; s < nSets; ++s ){
		for( unsigned w=0; w < nWays; ++w ){
			CortexM7CACHE.dccisw	= (
					(s << Oscl::Arm::CortexM7::pCACHE::rDCCISW::fSet::Lsb)
				|	(w << Oscl::Arm::CortexM7::pCACHE::rDCCISW::fWay::Lsb)
				);
			}
		}

	Oscl::CortexM::dsb();
	Oscl::CortexM::isb();
	}

void M7::invalidateEntireDataCache() noexcept {

	CortexM7FEATURE.csselr	= Oscl::Arm::CortexM7::pFEATURE::rCSSELR::fInD::ValueMask_dataCache;

	Oscl::CortexM::dsb();

	/* CSSIDR: 0xf00fe019
		1111 0000 0000 1111 1110 0000 0001 1001
		1 1 1 1 000000001111111 0000000011 001
		WT = 1 Write-Through support available
		WB = 1 Write-Back support available
		RA = 1 Read allocation support available
		WA = 1 Write allocation support available
		NumSets = 000 0000 0111 1111 = 0x007F = 127 + 1 = 128
		Associativity = 00 0000 0011 = 0x003 = 3 + 1 = 4
		LineSize = 001 = 1 = 8 words = 32 bytes per line
	 */

	unsigned	nSets;
	unsigned	nWays;
	unsigned	lineSize;
	unsigned	lineShift;
	csslerParameters(
		nSets,
		nWays,
		lineSize,
		lineShift
		);

	for( unsigned s=0; s < nSets; ++s ){
		for( unsigned w=0; w < nWays; ++w ){
			CortexM7CACHE.dcisw	= (
					(s << Oscl::Arm::CortexM7::pCACHE::rDCISW::fSet::Lsb)
				|	(w << Oscl::Arm::CortexM7::pCACHE::rDCISW::fWay::Lsb)
				);
			}
		}

	Oscl::CortexM::dsb();
	Oscl::CortexM::isb();
	}

void M7::invalidateEntireInstructionCache() noexcept {
	CortexM7CACHE.iciallu	= 0;
	Oscl::CortexM::dsb();
	Oscl::CortexM::isb();
	}

void M7::enableDataCache() noexcept {
	invalidateEntireDataCache();

	auto ccr	= CortexM7SCB.ccr;
	ccr	&= ~Oscl::Arm::CortexM7::pSCB::rCCR::fDC::FieldMask;
	ccr	|= Oscl::Arm::CortexM7::pSCB::rCCR::fDC::ValueMask_enable;
	CortexM7SCB.ccr	= ccr;

	Oscl::CortexM::dsb();
	Oscl::CortexM::isb();
	}

void M7::disableDataCache() noexcept {
	auto ccr	= CortexM7SCB.ccr;
	ccr	&= ~Oscl::Arm::CortexM7::pSCB::rCCR::fDC::FieldMask;
	ccr	|= Oscl::Arm::CortexM7::pSCB::rCCR::fDC::ValueMask_disable;
	CortexM7SCB.ccr	= ccr;
	}

void M7::enableInstructionCache() noexcept {

	invalidateEntireInstructionCache();

	auto ccr	= CortexM7SCB.ccr;
	ccr	&= ~Oscl::Arm::CortexM7::pSCB::rCCR::fIC::FieldMask;
	ccr	|= Oscl::Arm::CortexM7::pSCB::rCCR::fIC::ValueMask_enable;
	CortexM7SCB.ccr	= ccr;
	}

void M7::disableInstructionCache() noexcept {
	auto ccr	= CortexM7SCB.ccr;
	ccr	&= ~Oscl::Arm::CortexM7::pSCB::rCCR::fIC::FieldMask;
	ccr	|= Oscl::Arm::CortexM7::pSCB::rCCR::fIC::ValueMask_disable;
	CortexM7SCB.ccr	= ccr;
	}

static inline unsigned long	nCacheLines( unsigned long length, unsigned& cacheLineSize ) noexcept {

	unsigned	nSets;
	unsigned	nWays;
	unsigned	cacheLineSizeShift;

	csslerParameters(
		nSets,
		nWays,
		cacheLineSize,
		cacheLineSizeShift
		);

	// Round up to cacheLineSize
	unsigned long
	size	= length + (cacheLineSize-1);
	// Divide by cacheLineSize to get number of cache lines
	size	>>= cacheLineSizeShift;

	return size;
	}

static void	dataCacheRange(
				volatile uint32_t&	mvareg,
				const void* address,
				unsigned long length
				) noexcept {
	unsigned cacheLineSize;

	unsigned long
	nLines	= nCacheLines( length, cacheLineSize );

	uint32_t
	addr	= reinterpret_cast<uint32_t>(address);

	Oscl::CortexM::dsb();

	for( unsigned long i=0; i < nLines; ++i, addr += cacheLineSize ) {
		mvareg	= addr;
		}

	Oscl::CortexM::dsb();
	Oscl::CortexM::isb();
	}

void	M7::cleanDataCacheAddressRange(const void* address, unsigned long length) noexcept {

	dataCacheRange(
		CortexM7CACHE.dccmvau,
		address,
		length
		);
	}

void	M7::invalidateDataCacheAddressRange(const void* address, unsigned long length) noexcept {

	dataCacheRange(
		CortexM7CACHE.dcimvac,
		address,
		length
		);
	}

