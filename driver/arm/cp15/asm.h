/*
   Copyright (C) 2005 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_arm_cp15__asmh_
#define _oscl_drv_arm_cp15__asmh_
#include "oscl/hw/arm/cp15/control.h"

/** */
namespace Oscl {

/** */
namespace Arm {

/** */
namespace CP15 {

inline void	cpWait() noexcept{
	asm volatile(	" mrc p15,0,%0,c2,c0,0;"	// arbitrary read of a CP15 register
					" mov	%0,%0;"
					" sub	pc,pc,#4;"
					);
	}

inline void	waitForInterrupt() noexcept{
	asm volatile(" mcr p15, 0, %0, c7, c0, 0");
	}

inline void invalidateEntireInstructionCache() noexcept{
	asm volatile(" mcr p15, 0, %r0, c7, c5, 0");
    }

inline void	invalidateInstructionCacheLineByVirtualAddress(unsigned long virtualAddress) noexcept{
	asm volatile(" mcr p15, 0, %0, c7, c5, 1" : : "r" (virtualAddress));
	}

inline void	invalidateInstructionCacheLineBySetIndex(unsigned long setIndex) noexcept{
	asm volatile("mcr p15, 0, %0, c7, c5, 2" : : "r" (setIndex));
	}

inline void flushPrefetchBuffer() noexcept{
	asm volatile("mcr p15, 0, %0, c7, c5, 4");
    }

inline void flushEntireBranchTargetCache() noexcept{
	asm volatile("mcr p15, 0, r0, c7, c5, 6");
    }

inline void flushBranchTargetCacheEntry(unsigned long imp) noexcept{
	asm volatile("mcr p15, 0, r0, c7, c5, 7" : : "r" (imp) );
    }

inline void invalidateEntireDataCache() noexcept{
	asm volatile("mcr p15, 0, %0, c7, c6, 0");
    }

inline void	invalidateDataCacheLineByVirtualAddress(unsigned long virtualAddress) noexcept{
	asm volatile("mcr p15, 0, %0, c7, c6, 1" : : "r" (virtualAddress));
	}

inline void	invalidateDataCacheLineBySetIndex(unsigned long setIndex) noexcept{
	asm volatile("mcr p15, 0, %0, c7, c6, 2" : : "r" (setIndex));
	}

inline void invalidateEntireInstructionAndDataCache() noexcept{
	asm volatile("mcr p15, 0, %0, c7, c7, 0");
    }

inline void invalidateEntireUnifiedCache() noexcept{
	invalidateEntireInstructionAndDataCache();
    }

inline void	invalidateUnifiedCacheLineByVirtualAddress(unsigned long virtualAddress) noexcept{
	asm volatile("mcr p15, 0, %0, c7, c7, 1" : : "r" (virtualAddress));
	}

inline void	invalidateUnifiedCacheLineBySetIndex(unsigned long setIndex) noexcept{
	asm volatile("mcr p15, 0, %0, c7, c7, 2" : : "r" (setIndex));
	}

inline void	cleanDataCacheLineByVirtualAddress(unsigned long virtualAddress) noexcept{
	asm volatile("mcr p15, 0, %0, c7, c10, 1" : : "r" (virtualAddress));
	}

inline void	cleanDataCacheLineBySetIndex(unsigned long setIndex) noexcept{
	asm volatile("mcr p15, 0, %0, c7, c10, 2" : : "r" (setIndex));
	}

inline void drainWriteBuffer() noexcept{
	asm volatile("mcr p15, 0, r0, c7, c10, 4");
    }

inline void	cleanUnifiedCacheLineByVirtualAddress(unsigned long virtualAddress) noexcept{
	asm volatile("mcr p15, 0, %0, c7, c11, 1" : : "r" (virtualAddress));
	}

inline void	cleanUnifiedCacheLineBySetIndex(unsigned long setIndex) noexcept{
	asm volatile("mcr p15, 0, %0, c7, c11, 2" : : "r" (setIndex));
	}

inline void	prefetchInstructionCacheLineByVirtualAddress(unsigned long virtualAddress) noexcept{
	asm volatile("mcr p15, 0, %0, c7, c13, 1" : : "r" (virtualAddress));
	}

inline void	cleanAndInvalidateCacheLineByVirtualAddress(unsigned long virtualAddress) noexcept{
	asm volatile("mcr p15, 0, %0, c7, c14, 1" : : "r" (virtualAddress));
	}

inline void	cleanAndInvalidateCacheLineBySetIndex(unsigned long setIndex) noexcept{
	asm volatile("mcr p15, 0, %0, c7, c14, 2" : : "r" (setIndex));
	}

inline void	cleanAndInvalidateUnifiedCacheLineByVirtualAddress(unsigned long virtualAddress) noexcept{
	asm volatile("mcr p15, 0, %0, c7, c15, 1" : : "r" (virtualAddress));
	}

inline void	cleanAndInvalidateUnifiedCacheLineBySetIndex(unsigned long setIndex) noexcept{
	asm volatile("mcr p15, 0, %0, c7, c15, 2" : : "r" (setIndex));
	}




inline void flushInstructionAndDataCaches() noexcept{
	asm volatile("mcr p15, 0, r0, c7, c7, 0");
    }

inline void disableInstructionAndDataCaches() noexcept{
	asm volatile (
		"mrc p15, 0, r0, c1, c0, 0;"
		"bic r0, r0, #0x0d;"
		"bic r0, r0, #0x1000;"
		"mcr p15, 0, r0, c1, c0, 0;"
		: : : "r0"
		);
	}

inline Oscl::HW::ARM::CP15::Control::Reg	readControlRegister() noexcept{
	unsigned long	reg;
    asm volatile("mrc p15, 0, %0, c1, c0, 0" : "=r" (reg));
	return reg;
	}

inline void	writeControlRegister(Oscl::HW::ARM::CP15::Control::Reg value) noexcept{
	asm volatile("mcr p15, 0, %0, c1, c0, 0" : : "r" (value));
	}

inline void	flushInstructioAndDataTlbs() noexcept{
	asm volatile("mcr p15, 0, %0, c8, c7, 0");
	}

inline void	invalidateInstructionCache() noexcept{
	asm volatile("mcr p15, 0, %0, c7, c5, 0");
	}


// MMU CP15

inline unsigned long readTranslationBase() noexcept{
	unsigned long	result;
	asm volatile("mrc p15, 0, %0, c2, c0, 0;" : "=r" (result) : );
	return result;
	}

inline void writeTranslationBase(unsigned long address) noexcept{
	asm volatile("mcr p15, 0, %0, c2, c0, 0" : : "r" (address) :);
	}

inline unsigned long readDomainAccessControl() noexcept{
	unsigned long	result;
	asm volatile("mrc p15, 0, %0, c3, c0, 0" : "=r" (result) :);
	return result;
	}

inline void writeDomainAccessControl(unsigned long address) noexcept{
	asm volatile("mcr p15, 0, %0, c3, c0, 0" : : "r" (address) :);
	}

inline unsigned long readFaultStatus() noexcept{
	unsigned long	result;
	asm volatile("mrc p15, 0, %0, c5, c0, 0" : "=r" (result) :);
	return result;
	}

inline void writeFaultStatus(unsigned long address) noexcept{
	asm volatile("mcr p15, 0, %0, c5, c0, 0" : : "r" (address) :);
	}

inline unsigned long readFaultAddress() noexcept{
	unsigned long	result;
	asm volatile("mrc p15, 0, %0, c6, c0, 0" : "=r" (result) :);
	return result;
	}

inline void writeFaultAddress(unsigned long address) noexcept{
	asm volatile("mcr p15, 0, %0, c6, c0, 0" : : "r" (address) :);
	}

inline void invalidateBothInstructionAndDataTLBs() noexcept{
	asm volatile("mcr p15, 0, %0, c8, c7, 0");
	}

inline void invalidateEntireUnifiedTLBs() noexcept{
	invalidateBothInstructionAndDataTLBs();
	}

inline void invalidateUnifiedSingleTlbEntry(unsigned long virtualAddress) noexcept{
	asm volatile("mcr p15, 0, %0, c8, c7, 1" : : "r" (virtualAddress));
	}

inline void invalidadteEntireInstructionTLB() noexcept{
	asm volatile("mcr p15, 0, %0, c8, c5, 0");
	}

inline void invalidateSingleInstructionTlbEntry(unsigned long virtualAddress) noexcept{
	asm volatile("mcr p15, 0, %0, c8, c5, 1" : : "r" (virtualAddress));
	}

inline void invalidateEntireDataTLB() noexcept{
	asm volatile("mcr p15, 0, %0, c8, c6, 0");
	}

inline void invalidateSingleDataTlbEntry(unsigned long virtualAddress) noexcept{
	asm volatile("mcr p15, 0, %0, c8, c6, 1" : : "r" (virtualAddress));
	}

inline unsigned long readDataTlbLockdown() noexcept{
	unsigned long	result;
	asm volatile("mrc p15, 0, %0, c10, c0, 0" : "=r" (result) :);
	return result;
	}

inline void writeDataTlbLockdown(unsigned long value) noexcept{
	asm volatile("mcr p15, 0, %0, c10, c0, 0" : : "r" (value) :);
	}

inline unsigned long readInstructionTlbLockdown() noexcept{
	unsigned long	result;
	asm volatile("mrc p15, 0, %0, c10, c0, 1" : "=r" (result) :);
	return result;
	}

inline void writeInstructionTlbLockdown(unsigned long value) noexcept{
	asm volatile("mcr p15, 0, %0, c10, c0, 1" : : "r" (value) :);
	}

inline unsigned long readUnifiedTlbLockdown() noexcept{
	return readDataTlbLockdown();
	}

inline void writeUnifiedTlbLockdown(unsigned long value) noexcept{
	writeDataTlbLockdown(value);
	}

}
}
}

#endif
