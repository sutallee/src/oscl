#ifndef _oscl_driver_nokia_lcd3310_spi_driverh_
#define _oscl_driver_nokia_lcd3310_spi_driverh_
#include "oscl/driver/nokia/lcd3310/api.h"
#include "oscl/spi/master/api.h"

/** */
namespace Oscl {
/** */
namespace Nokia {
/** */
namespace LCD3310 {
/** */
namespace SPI {

/** */
class DataControlApi {
	public:
		/** */
		virtual ~DataControlApi(){}
		/** */
		virtual void	selectData() noexcept=0;
		/** */
		virtual void	selectControl() noexcept=0;
	};

/** */
class Driver : public Oscl::Nokia::LCD3310::Api {
	private:
		/** */
		Oscl::SPI::Master::Api&			_spiApi;
		/** */
		Oscl::SPI::Master::Select::Api&	_lcdSpiSelectApi;
		/** */
		DataControlApi&					_dcApi;
	public:
		/** */
		Driver(	Oscl::SPI::Master::Api&			spiApi,
				Oscl::SPI::Master::Select::Api&	lcdSpiSelectApi,
				DataControlApi&					dcApi
				) noexcept;
		/** */
		void	functionSet(bool pwrDown,bool vertical,bool extended) noexcept;
		/** */
		void	writeData(const uint8_t* data,unsigned len) noexcept;
		/** */
		void	displayBlank() noexcept;
		/** */
		void	displayNormalMode() noexcept;
		/** */
		void	displayAllSegmentsOn() noexcept;
		/** */
		void	displayInverseVideo() noexcept;
		/** Range 0 -> 5 inclusive */
		void	setY(unsigned char address) noexcept;
		/** Range 0 -> 83 inclusive */
		void	setX(unsigned char address) noexcept;
		/** */
		void	setTemperatureCoefficient(unsigned char tc) noexcept;
		/** */
		void	setBiasSystem(unsigned char bs) noexcept;
		/** */
		void	setVop(unsigned char vop) noexcept;
	};

}
}
}
}

#endif
