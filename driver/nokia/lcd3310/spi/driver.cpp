#include "oscl/hw/nokia/lcd3310/reg.h"
#include "driver.h"

using namespace Oscl::Nokia::LCD3310::SPI;

Driver::Driver(	Oscl::SPI::Master::Api&			spiApi,
				Oscl::SPI::Master::Select::Api&	lcdSpiSelectApi,
				DataControlApi&					dcApi
				) noexcept:
		_spiApi(spiApi),
		_lcdSpiSelectApi(lcdSpiSelectApi),
		_dcApi(dcApi)
		{
	}

void	Driver::functionSet(bool pwrDown,bool vertical,bool extended) noexcept{
	using namespace Oscl::HW::Nokia::LCD3310::FunctionSet;
	uint8_t	buffer;
	uint8_t	pd	=	pwrDown?
						(uint8_t)PD::ValueMask_PowerDown :
						(uint8_t)PD::ValueMask_Active
						;
	uint8_t	v	=	vertical?
						(uint8_t)V::ValueMask_VerticalAddr :
						(uint8_t)V::ValueMask_HorizontalAddr
						;
	uint8_t	h	=	extended?
						(uint8_t)H::ValueMask_Extended :
						(uint8_t)H::ValueMask_Basic
						;

	buffer	=		(uint8_t)OpCode::ValueMask_Always
				|	pd
				|	v
				|	h
				;

	_dcApi.selectControl();
	_spiApi.transfer(	_lcdSpiSelectApi,
						&buffer,
						&buffer,
						1
						);
	}

void	Driver::writeData(const uint8_t* data,unsigned len) noexcept{
	_dcApi.selectData();

	// We need to allocate a receive buffer for
	// the SPI transfer, even though we discard
	// its contents. For speed and efficiency,
	// we'd like to transfer/ the data in the largest
	// chunks possible. However, we'd also like to
	// minimize our stack usage for memory constrained
	// systems. 
	enum{rxBufferSize=4};
	uint8_t	rxBuffer[rxBufferSize];
	unsigned	nTransfer	= rxBufferSize;
	for(	unsigned remaining=len;
			remaining;
			remaining-=nTransfer, data+=nTransfer
			){
		if(remaining < rxBufferSize){
			nTransfer	= remaining;
			}
		_spiApi.transfer(	_lcdSpiSelectApi,
							&data,
							&rxBuffer,
							nTransfer
							);
		}
	}

void	Driver::displayBlank() noexcept{
	using namespace Oscl::HW::Nokia::LCD3310;
	uint8_t	buffer	= (uint8_t)Display::Blank;
	_dcApi.selectControl();
	_spiApi.transfer(	_lcdSpiSelectApi,
						&buffer,
						&buffer,
						1
						);
	}

void	Driver::displayNormalMode() noexcept{
	using namespace Oscl::HW::Nokia::LCD3310;
	uint8_t	buffer	= (uint8_t)Display::NormalMode;
	_dcApi.selectControl();
	_spiApi.transfer(	_lcdSpiSelectApi,
						&buffer,
						&buffer,
						1
						);
	}

void	Driver::displayAllSegmentsOn() noexcept{
	using namespace Oscl::HW::Nokia::LCD3310;
	uint8_t	buffer	= (uint8_t)Display::AllSegmentsOn;
	_dcApi.selectControl();
	_spiApi.transfer(	_lcdSpiSelectApi,
						&buffer,
						&buffer,
						1
						);
	}

void	Driver::displayInverseVideo() noexcept{
	using namespace Oscl::HW::Nokia::LCD3310;
	uint8_t	buffer	= (uint8_t)Display::InverseVideo;
	_dcApi.selectControl();
	_spiApi.transfer(	_lcdSpiSelectApi,
						&buffer,
						&buffer,
						1
						);
	}

void	Driver::setY(unsigned char address) noexcept{
	using namespace Oscl::HW::Nokia::LCD3310::SetY;
	uint8_t	buffer;

	buffer	=		(uint8_t)OpCode::ValueMask_Always
				|	(uint8_t)((address<<Address::Lsb) & Address::FieldMask)
				;

	_dcApi.selectControl();
	_spiApi.transfer(	_lcdSpiSelectApi,
						&buffer,
						&buffer,
						1
						);
	}

void	Driver::setX(unsigned char address) noexcept{
	using namespace Oscl::HW::Nokia::LCD3310::SetX;
	uint8_t	buffer;

	buffer	=		(uint8_t)OpCode::ValueMask_Always
				|	(uint8_t)((address<<Address::Lsb) & Address::FieldMask)
				;

	_dcApi.selectControl();
	_spiApi.transfer(	_lcdSpiSelectApi,
						&buffer,
						&buffer,
						1
						);
	}

void	Driver::setTemperatureCoefficient(unsigned char tc) noexcept{
	using namespace Oscl::HW::Nokia::LCD3310::Temperature;
	uint8_t	buffer;

	buffer	=		(uint8_t)OpCode::ValueMask_Always
				|	(uint8_t)((tc<<TC::Lsb) & TC::FieldMask)
				;

	_dcApi.selectControl();
	_spiApi.transfer(	_lcdSpiSelectApi,
						&buffer,
						&buffer,
						1
						);
	}

void	Driver::setBiasSystem(unsigned char bs) noexcept{
	using namespace Oscl::HW::Nokia::LCD3310::BiasSystem;
	uint8_t	buffer;

	buffer	=		(uint8_t)OpCode::ValueMask_Always
				|	(uint8_t)((bs<<BS::Lsb) & BS::FieldMask)
				;

	_dcApi.selectControl();
	_spiApi.transfer(	_lcdSpiSelectApi,
						&buffer,
						&buffer,
						1
						);
	}

void	Driver::setVop(unsigned char vop) noexcept{
	using namespace Oscl::HW::Nokia::LCD3310::SetVop;
	uint8_t	buffer;

	buffer	=		(uint8_t)OpCode::ValueMask_Always
				|	(uint8_t)((vop<<Vop::Lsb) & Vop::FieldMask)
				;

	_dcApi.selectControl();
	_spiApi.transfer(	_lcdSpiSelectApi,
						&buffer,
						&buffer,
						1
						);
	}

