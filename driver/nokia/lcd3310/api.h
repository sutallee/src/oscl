#ifndef _oscl_driver_nokia_lcd3310_apih_
#define _oscl_driver_nokia_lcd3310_apih_
#include <stdint.h>

/** */
namespace Oscl {
/** */
namespace Nokia {
/** */
namespace LCD3310 {

/** */
class Api {
	public:
		/** */
		virtual ~Api() {}
		/** */
		virtual void	functionSet(bool pwrDown,bool vertical,bool extended) noexcept=0;
		/** */
		virtual void	writeData(const uint8_t* data,unsigned len) noexcept=0;
		/** */
		virtual void	displayBlank() noexcept=0;
		/** */
		virtual void	displayNormalMode() noexcept=0;
		/** */
		virtual void	displayAllSegmentsOn() noexcept=0;
		/** */
		virtual void	displayInverseVideo() noexcept=0;
		/** Range 0 -> 5 inclusive */
		virtual void	setY(unsigned char address) noexcept=0;
		/** Range 0 -> 83 inclusive */
		virtual void	setX(unsigned char address) noexcept=0;
		/** */
		virtual void	setTemperatureCoefficient(unsigned char tc) noexcept=0;
		/** */
		virtual void	setBiasSystem(unsigned char bs) noexcept=0;
		/** */
		virtual void	setVop(unsigned char vop) noexcept=0;
	};

}
}
}

#endif
