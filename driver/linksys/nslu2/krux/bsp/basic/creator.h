/*
   Copyright (C) 2006 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_linksys_nslu2_krux_bsp_basic_creatorh_
#define _oscl_drv_linksys_nslu2_krux_bsp_basic_creatorh_
#include "oscl/memory/transapi.h"
#include "oscl/hw/pci/config.h"
#include "oscl/krux/sema/bbsema.h"
#include "oscl/bits/port.h"
#include "oscl/bits/mt/mutexedbp.h"
#include "oscl/bits/mt/mutexedfield.h"
#include "oscl/driver/linksys/nslu2/device/device.h"
#include "oscl/mt/itc/dyn/adv/service.h"
#include "oscl/pcisrv/enum/server.h"
#include "oscl/mt/mutex/simple.h"
#include "oscl/protocol/ieee/mac.h"
#include "oscl/interrupt/sadaptor.h"
#include "oscl/interrupt/shared/handler.h"
#include "oscl/driver/intel/ixp4xx/ost/delay/server.h"

/** */
namespace Oscl {
/** */
namespace Linksys {
/** */
namespace NSLU2 {

/** */
class SystemTimer : public Oscl::Intel::IXP4xx::OST::Observer {
	public:
		/** */
		void	tick() noexcept;
	};

/** This class implements the high level drivers that are common
	to a basic configuration of the MDP860PPro development board
	under OOOS kernel. This level of the BSP includes policy
	as well as ITC services.
 */
class Creator {
	private:
		/** */
		Oscl::Linksys::NSLU2::Device			_nslu2;

		/** */
		Oscl::Interrupt::StatusAdaptor			_statusIrqHandlerAdaptor;

		/** */
		Oscl::Memory::Bus::NullTranslation		_memTranslator;

		/** */
		SystemTimer								_ostSysTimer;

		/** */
		Oscl::Intel::IXP4xx::OST::Delay::Server	_delayServer;

	public:
		/** */
		Creator(	const Oscl::Protocol::
					IEEE::MacAddress&		scc1EnetStationAddress,
					Oscl::Mt::
					Mutex::Simple::Api&		mutex
					) noexcept;
		/** */
		void	initialize() noexcept;

		/** */
		Oscl::Linksys::NSLU2::Api&			getNSLU2() noexcept;

		/** */
		Oscl::Mt::Itc::
		Delay::Req::Api::SAP&				getDelayServiceSAP() noexcept;
		/** */
		Oscl::Mt::Runnable&					getDelayServiceRunnable() noexcept;

	};

}
}
}

#endif
