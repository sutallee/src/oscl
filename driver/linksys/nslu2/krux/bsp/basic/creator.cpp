/*
   Copyright (C) 2006 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "creator.h"
#include "oscl/interrupt/null.h"
#include "oscl/hw/linksys/nslu2/map.h"

Oscl::Interrupt::HandlerApi* ExternalInterruptHandler;

using namespace Oscl::Linksys::NSLU2;

void	SystemTimer::tick() noexcept{
	extern Oscl::Krux::Sema::BroadcastBinary	UKClockSema;
	UKClockSema.suSignal();
	}

static Oscl::Interrupt::NullHandler				nullHandler;

Creator::Creator(	const Oscl::Protocol::
					IEEE::MacAddress&		scc1EnetStationAddress,
					Oscl::Mt::
					Mutex::Simple::Api&		mutex
					) noexcept:
		_nslu2(IntelIxp4xxMap,mutex),
		_statusIrqHandlerAdaptor(_nslu2.getIXP420().getPicApi().getISR()),
		_delayServer(100)	// FIXME: need to correlate with PIT init.
		{
	ExternalInterruptHandler	= &_statusIrqHandlerAdaptor;
	_nslu2.getIXP420().getTimer0Api().attach(_delayServer);
	}

void	Creator::initialize() noexcept{
	Oscl::Intel::IXP4xx::INTR::Api&	pic	= _nslu2.getIXP420().getPicApi();
    pic.getTimers0().reserve(_nslu2.getIXP420().getTimer0Api());
	_nslu2.getIXP420().getTimer0Api().attach(_ostSysTimer);
	_nslu2.getIXP420().getInterruptDriverApi().getTimer0().routeToIRQ();
	_nslu2.getIXP420().getInterruptDriverApi().getTimer0().enable();
	_nslu2.getIXP420().getTimer0Api().start();
	}

Oscl::Linksys::NSLU2::Api&		Creator::getNSLU2() noexcept{
	return _nslu2;
	}

Oscl::Mt::Itc::Delay::Req::Api::SAP&	Creator::getDelayServiceSAP() noexcept{
	return _delayServer.getSAP();
	}

Oscl::Mt::Runnable&	Creator::getDelayServiceRunnable() noexcept{
	return _delayServer;
	}

