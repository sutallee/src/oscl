/*
   Copyright (C) 2006 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_linksys_nslu2_device_apih_
#define _oscl_drv_linksys_nslu2_device_apih_
#include "oscl/driver/intel/ixp420/device/api.h"
#include "oscl/driver/pci/config.h"

#include "oscl/interrupt/shared/api.h"

/** */
namespace Oscl {
/** */
namespace Linksys {
/** */
namespace NSLU2 {

/** This is the interface to the NSLU2 hardware drivers.
	All drivers available through this interface are independent
	of operating syste/environment.
 */
class Api {
	public:
		/** */
		virtual ~Api(){}
		/** */
		virtual Oscl::Intel::IXP420::Api&		getIXP420() noexcept=0;
	};

}
}
}


#endif
