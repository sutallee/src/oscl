/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_driver_pci_functionh_
#define _oscl_driver_pci_functionh_
#include "funcapi.h"
#include "config.h"
#include "oscl/hw/pci/config.h"

/** */
namespace Oscl {
/** */
namespace Pci {
/** */
namespace Config {
/** */
namespace Function {

/** */
class Generic : public Api{
	private:
		/** */
		Oscl::Pci::Config::Api&			_config;
		/** */
		unsigned char					_bus;
		/** */
		unsigned char					_device;
		/** */
		unsigned char					_function;
		/** */
		ID::Reg							_id;
		/** */
		CmdStatus::Reg					_cmdStatus;
		/** */
		ClassRev::Reg					_classRev;
		/** */
		BistTypeLatCache::Reg			_bistTypeLatCache;
	public:
		/** */
		Generic(	Oscl::Pci::Config::Api&	config,
					unsigned char			bus,
					unsigned char			device,
					unsigned char			function
					) noexcept;
	public:
		/** */
		bool	exists() const noexcept;
	public:
		/** Initiates transaction to read all PCI configuration
			registers for this function. Returns true if the
			function exits in PCI space and configuration was
			successfully read into cache.
		 */
		bool			update() noexcept;
	public:	// ID::Reg
		/** */
		uint16_t		deviceID() const noexcept;
		/** */
		uint16_t		vendorID() const noexcept;
	public:	// CmdStatus::CmdReg
		/** */
		bool			ioDecodersEnabled() const noexcept;
		/** */
		void			enableIoDecoders() noexcept;
		/** */
		void			disableIoDecoders() noexcept;
		/** */
		bool			memoryDecodersEnabled() const noexcept;
		/** */
		void			enableMemoryDecoders() noexcept;
		/** */
		void			disableMemoryDecoders() noexcept;
		/** */
		bool			busMasteringEnabled() const noexcept;
		/** */
		void			enableBusMastering() noexcept;
		/** */
		void			disableBusMastering() noexcept;
		/** */
		bool			monitoringSpecialCycles() const noexcept;
		/** */
		void			respondToSpecialCycles() noexcept;
		/** */
		void			ignoreSpecialCycles() noexcept;
		/** */
		bool			memoryWriteAndInvalidateEnabled() const noexcept;
		/** */
		void			enableMemoryWriteAndInvalidate() noexcept;
		/** */
		void			disableMemoryWriteAndInvalidate() noexcept;
		/** */
		bool			snoopingVgaPalette() const noexcept;
		/** */
		void			enableVgaPaletteSnoop() noexcept;
		/** */
		void			disableVgaPaletteSnoop() noexcept;
		/** */
		bool			parityErrorEnabled() const noexcept;
		/** */
		void			enableParityError() noexcept;
		/** */
		void			disableParityError() noexcept;
		/** */
		bool			steppingEnabled() const noexcept;
		/** */
		void			enableStepping() noexcept;
		/** */
		void			disableStepping() noexcept;
		/** */
		bool			systemErrorEnabled() const noexcept;
		/** */
		void			enableSystemError() noexcept;
		/** */
		void			disableSystemError() noexcept;
		/** */
		bool			fastBackToBackEnabled() const noexcept;
		/** */
		void			enableFastBackToBack() noexcept;
		/** */
		void			disableFastBackToBack() noexcept;
	public:	// CmdStatus::StatusReg
		/** */
		void			updateStatus() noexcept;
		/** */
		bool			hasCapabilitiesList() const noexcept;
		/** */
		bool			capableOf66MHz() const noexcept;
		/** */
		bool			udfSupported() const noexcept;
		/** */
		bool			fastBackToBackCapable() const noexcept;
		/** */
		bool			masterDataParityError() const noexcept;
		/** */
		void			clearMasterDataParityError() noexcept;
		/** */
		DevSelTiming	deviceSelectTiming() const noexcept;
		/** */
		bool			signaledTargetAbort() const noexcept;
		/** */
		void			clearSignaledTargetAbort() noexcept;
		/** */
		bool			receivedTargetAbort() const noexcept;
		/** */
		void			clearReceivedTargetAbort() noexcept;
		/** */
		bool			receivedMasterAbort() const noexcept;
		/** */
		void			clearReceivedMasterAbort() noexcept;
		/** */
		bool			signaledSystemError() const noexcept;
		/** */
		void			clearSignaledSystemError() noexcept;
		/** */
		bool			detectedParityError() const noexcept;
		/** */
		void			clearDetectedParityError() noexcept;
	public:	// ClassRev::Reg
		/** */
		unsigned char	baseClass() const noexcept;
		/** */
		unsigned char	subClass() const noexcept;
		/** */
		unsigned char	programmingInterface() const noexcept;
		/** */
		unsigned char	revisionID() const noexcept;
	public:	// BistTypeLatCache::Reg
		/** */
		bool			singleFunction() const noexcept;
		/** */
		HeaderType		headerType() const noexcept;
		/** */
		unsigned char	cacheLineSize() const noexcept;
		/** */
		void			setCacheLineSize(unsigned char dwords)noexcept;
		/** */
		unsigned char	latencyTimer() const noexcept;
		/** */
		void			setLatencyTimer(unsigned char pciClks)noexcept;
		/** */
		bool			bistCapable() const noexcept;
		/** */
		void			startBuiltInSelfTest() noexcept;
		/** */
		unsigned char	bistCompletionCode() const noexcept;
	private:
		/** */
		void	readID() noexcept;
		/** */
		void	readCmdStatus() noexcept;
		/** */
		void	readClassRev() noexcept;
		/** */
		void	readBistTypeLatCache() noexcept;
	private:
		/** */
		void	flushCmdStatus() noexcept;
		/** */
		void	flushBistTypeLatCache() noexcept;
		/** */
		void	clearStatus(CmdStatus::Reg bitsToClear) noexcept;
	};

}
}
}
}

#endif
