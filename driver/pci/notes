The unit of "removability" in a PCI system is the "Card/Device".
This means that individual functions are not dynamically
replaceable. Thus, what is "advertised" are "Cards/Devices".

Pure geographic location is unknown in a hierarchical PCI
bus. A given application of PCI may pre-define/ assume a
certain topology and infer geographic location based
on this assumed knowledge using bus-device tuples,
system imposed constraints and either a knowledge of the
PCI bus ID assignment ordering, or a custom assignment policy
(which is the same thing).

Using the dynamic service framework, the creator would be a
policy of the PCI bus enumeration service, which would invoke
add(device) on an advertiser, and deactivate() on the device
upon discovering the device removed.

Device Tree:
	Composite Pattern
		Composite-Bus-Dynamic
		Composite-Device-Dynamic
		Leaf-Function-Static
	Visitor



///// Older Notes
Imagine a register class that uses decorators and
has an interface like this:

class RegApi<Type> {
	public:
		typedef bool	AccessFailed;
	protected:
		virtual AccessFailed	read(	unsigned long	offset,
										Type&			output
										) noexcept=0;
		virtual AccessFailed	rmw(	unsigned long	offset,
										Type			mask,
										Type			value,
										Type&			written
										) noexcept=0;
	};

template <Type>
class Reg<Type> {
	private:
		Type	_cache;
	public:
		typedef bool	AccessFailed;
	public:
		Reg(AccessFailed& result) noexcept;
		Reg() noexcept;	// No auto read.
		AccessFailed	refreshCache() noexcept;
		AccessFailed	rmw(Type mask,Type value);
		AccessFailed	flushCache();
		Type	operator =()
	}

class AccessFailureCatchStack : public Oscl::Queue<AccessFailureCatcher> {
	public:
		static	void					push(	AccessFailureCatcher&	catcher
												) noexcept;
		static	AccessFailureCatcher*	pop(	AccessFailureCatcher&	catcher
												) noexcept;
	};

class AccessFailureCatcher : public Oscl::QueueItem {
	private:
		bool	_failed;
	public:
		AccessFailureCatcher() noexcept:
				_failed(false)
				{
			}
		void	failed() noexcept{ _failed=true;}
		bool	accessFailed() noexcept{return _failed;}
	};

class ScopeCatch :: private AccessFailureCatcher {
	public:
		ScopeCatch() noexcept{
			AccessFailureCatchStack::push(*this);
			}
		~ScopeCatch() {
			AccessFailureCatchStack::pop();
			}
		bool	accessFailed()
	};

namespace Oscl {
namespace Pci {
namespace Driver {
namespace Config {
namespace CmdStatus {

class  CmdStatusReg {
	private:
		Oscl::Pci::Config::CmdStatus::Reg			_cache;
		volatile Oscl::Pci::Config::CmdStatus::Reg&	_reg;
	public:
		CmdStatusReg(volatile Oscl::Pci::Config::CmdStatus::Reg& reg) noexcept;
	public: // Operations that access the register
		AccessFailed	refreshCache() noexcept{
			ScopeCatch	catch;
			_cache	= _reg;
			return catch.accessResult();
			}
		AccessFailed	write(Oscl::Pci::Config::CmdStatus::Reg value) noexcept;
		AccessFailed	rmw(	Oscl::Pci::Config::CmdStatus::Reg	mask
								Oscl::Pci::Config::CmdStatus::Reg	value
								) noexcept;
	public: // Operations that access the cached value
		bool	detectedParityError() noexcept;
		bool	signaledSystemError() noexcept;
		bool	signaledSystemError() noexcept;
	};
