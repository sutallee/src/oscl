/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_driver_pci_funcapih_
#define _oscl_driver_pci_funcapih_
#include "config.h"
#include "oscl/hw/pci/config.h"

/** */
namespace Oscl {
/** */
namespace Pci {
/** */
namespace Config {
/** */
namespace Function {

/** */
enum DevSelTiming {
	/** */
	Fast=0,
	/** */
	Medium=1,
	/** */
	Slow=2
	};

/** */
enum HeaderType {
	/** */
	Standard=0,
	/** */
	PciToPciBridge=1,
	/** */
	PciToCardBusBridge=2
	};

/** */
class Api {
	public:
		/** Make GCC happy */
		virtual ~Api() {}
	public:
		/** */
		bool	exists() noexcept;
	public:
		/** Initiates transaction to read all PCI configuration
			registers for this function. Returns true if the
			function exits in PCI space and configuration was
			successfully read into cache.
		 */
		virtual bool			update() noexcept=0;
	public:	// ID::Reg
		/** */
		virtual uint16_t		deviceID() const noexcept=0;
		/** */
		virtual uint16_t		vendorID() const noexcept=0;
	public:	// CmdStatus::CmdReg
		/** */
		virtual bool			busMasteringEnabled() const noexcept=0;
		/** */
		virtual void			enableBusMastering() noexcept=0;
		/** */
		virtual void			disableBusMastering() noexcept=0;
		/** */
		virtual bool			monitoringSpecialCycles() const noexcept=0;
		/** */
		virtual void			respondToSpecialCycles() noexcept=0;
		/** */
		virtual void			ignoreSpecialCycles() noexcept=0;
		/** */
		virtual bool			memoryWriteAndInvalidateEnabled() const noexcept=0;
		/** */
		virtual void			enableMemoryWriteAndInvalidate() noexcept=0;
		/** */
		virtual void			disableMemoryWriteAndInvalidate() noexcept=0;
		/** */
		virtual bool			snoopingVgaPalette() const noexcept=0;
		/** */
		virtual void			enableVgaPaletteSnoop() noexcept=0;
		/** */
		virtual void			disableVgaPaletteSnoop() noexcept=0;
		/** */
		virtual bool			parityErrorEnabled() const noexcept=0;
		/** */
		virtual void			enableParityError() noexcept=0;
		/** */
		virtual void			disableParityError() noexcept=0;
		/** */
		virtual bool			steppingEnabled() const noexcept=0;
		/** */
		virtual void			enableStepping() noexcept=0;
		/** */
		virtual void			disableStepping() noexcept=0;
		/** */
		virtual bool			systemErrorEnabled() const noexcept=0;
		/** */
		virtual void			enableSystemError() noexcept=0;
		/** */
		virtual void			disableSystemError() noexcept=0;
		/** */
		virtual bool			fastBackToBackEnabled() const noexcept=0;
		/** */
		virtual void			enableFastBackToBack() noexcept=0;
		/** */
		virtual void			disableFastBackToBack() noexcept=0;
	public:	// CmdStatus::StatusReg
		/** */
		virtual void			updateStatus() noexcept=0;
		/** */
		virtual bool			hasCapabilitiesList() const noexcept=0;
		/** */
		virtual bool			capableOf66MHz() const noexcept=0;
		/** */
		virtual bool			udfSupported() const noexcept=0;
		/** */
		virtual bool			fastBackToBackCapable() const noexcept=0;
		/** */
		virtual bool			masterDataParityError() const noexcept=0;
		/** */
		virtual void			clearMasterDataParityError() noexcept=0;
		/** */
		virtual DevSelTiming	deviceSelectTiming() const noexcept=0;
		/** */
		virtual bool			signaledTargetAbort() const noexcept=0;
		/** */
		virtual void			clearSignaledTargetAbort() noexcept=0;
		/** */
		virtual bool			receivedTargetAbort() const noexcept=0;
		/** */
		virtual void			clearReceivedTargetAbort() noexcept=0;
		/** */
		virtual bool			receivedMasterAbort() const noexcept=0;
		/** */
		virtual void			clearReceivedMasterAbort() noexcept=0;
		/** */
		virtual bool			signaledSystemError() const noexcept=0;
		/** */
		virtual void			clearSignaledSystemError() noexcept=0;
		/** */
		virtual bool			detectedParityError() const noexcept=0;
		/** */
		virtual void			clearDetectedParityError() noexcept=0;
	public:	// ClassRev::Reg
		/** */
		virtual unsigned char	baseClass() const noexcept=0;
		/** */
		virtual unsigned char	subClass() const noexcept=0;
		/** */
		virtual unsigned char	programmingInterface() const noexcept=0;
		/** */
		virtual unsigned char	revisionID() const noexcept=0;
	public:	// BistTypeLatCache::Reg
		/** */
		virtual bool			singleFunction() const noexcept=0;
		/** */
		virtual HeaderType		headerType() const noexcept=0;
		/** */
		virtual unsigned char	cacheLineSize() const noexcept=0;
		/** */
		virtual void			setCacheLineSize(unsigned char dwords)noexcept=0;
		/** */
		virtual unsigned char	latencyTimer() const noexcept=0;
		/** */
		virtual void			setLatencyTimer(unsigned char pciClks)noexcept=0;
		/** */
		virtual bool			bistCapable() const noexcept=0;
		/** */
		virtual void			startBuiltInSelfTest() noexcept=0;
		/** */
		virtual unsigned char	bistCompletionCode() const noexcept=0;
	};
}
}
}
}

#endif
