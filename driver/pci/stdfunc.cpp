/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "stdfunc.h"
#include "oscl/hw/pci/config.h"

using namespace Oscl::Pci::Config;
using namespace Oscl;

Function::HeaderTypeZero::HeaderTypeZero(	Oscl::Pci::Config::Api&	config,
						unsigned char			bus,
						unsigned char			device,
						unsigned char			function
						) noexcept:
		_config(config),
		_bus(bus),
		_device(device),
		_function(function)
		{
	}

bool			Function::HeaderTypeZero::update() noexcept{
	using namespace Oscl::Pci::Config::HeaderTypeZero;
	readID();
	if(!exists()) return false;
	readCmdStatus();
	readClassRev();
	readBistTypeLatCache();
	switch(headerType()){
		case Standard:
			// Only deal with these for now
			break;
		case PciToPciBridge:
		case PciToCardBusBridge:
		default:
			return true;
		};
	for(unsigned reg=0;reg<MaxBaseAddressRegisters;++reg){
		readBaseAddress(reg);
		_baseAddress[reg]	= ~(uint32_t)0;
		flushBaseAddress(reg);
		readBaseAddress(reg);
		}
	readSubsystem();
	readExpansionRomBaseAddr();
	readMaxLatMinGntIntPinIntLine();
	return true;
	}

bool	Function::HeaderTypeZero::exists() const noexcept{
	return vendorID() != ID::Vendor::Value_NoFunctionPresent;
	}

uint16_t		Function::HeaderTypeZero::deviceID() const noexcept{
	return (_id & ID::Device::FieldMask)>>ID::Device::Lsb;
	}

uint16_t		Function::HeaderTypeZero::vendorID() const noexcept{
	return (_id & ID::Vendor::FieldMask)>>ID::Vendor::Lsb;
	}

bool			Function::HeaderTypeZero::ioDecodersEnabled() const noexcept{
	using namespace Oscl::Pci::Config::HeaderTypeZero::CmdStatus;
	return (_cmdStatus & CmdReg::IoSpace::FieldMask)
			==	CmdReg::IoSpace::ValueMask_Enable
			;
	}

void			Function::HeaderTypeZero::enableIoDecoders() noexcept{
	using namespace Oscl::Pci::Config::HeaderTypeZero::CmdStatus;
	_cmdStatus	&= ~CmdReg::IoSpace::FieldMask;
	_cmdStatus	|= CmdReg::IoSpace::ValueMask_Enable;
	flushCmdStatus();
	}

void			Function::HeaderTypeZero::disableIoDecoders() noexcept{
	using namespace Oscl::Pci::Config::HeaderTypeZero::CmdStatus;
	_cmdStatus	&= ~CmdReg::IoSpace::FieldMask;
	_cmdStatus	|= CmdReg::IoSpace::ValueMask_Disable;
	flushCmdStatus();
	}
bool			Function::HeaderTypeZero::memoryDecodersEnabled() const noexcept{
	using namespace Oscl::Pci::Config::HeaderTypeZero::CmdStatus;
	return (_cmdStatus & CmdReg::MemorySpace::FieldMask)
			==	CmdReg::MemorySpace::ValueMask_Enable
			;
	}
void			Function::HeaderTypeZero::enableMemoryDecoders() noexcept{
	using namespace Oscl::Pci::Config::HeaderTypeZero::CmdStatus;
	_cmdStatus	&= ~CmdReg::MemorySpace::FieldMask;
	_cmdStatus	|= CmdReg::MemorySpace::ValueMask_Enable;
	flushCmdStatus();
	}
void			Function::HeaderTypeZero::disableMemoryDecoders() noexcept{
	using namespace Oscl::Pci::Config::HeaderTypeZero::CmdStatus;
	_cmdStatus	&= ~CmdReg::MemorySpace::FieldMask;
	_cmdStatus	|= CmdReg::MemorySpace::ValueMask_Disable;
	flushCmdStatus();
	}
bool			Function::HeaderTypeZero::busMasteringEnabled() const noexcept{
	return (_cmdStatus & CmdStatus::CmdReg::BusMaster::FieldMask)
			==	CmdStatus::CmdReg::BusMaster::ValueMask_Enable
			;
	}
void			Function::HeaderTypeZero::enableBusMastering() noexcept{
	_cmdStatus	&= ~CmdStatus::CmdReg::BusMaster::FieldMask;
	_cmdStatus	|= CmdStatus::CmdReg::BusMaster::ValueMask_Enable;
	flushCmdStatus();
	}
void			Function::HeaderTypeZero::disableBusMastering() noexcept{
	_cmdStatus	&= ~CmdStatus::CmdReg::BusMaster::FieldMask;
	_cmdStatus	|= CmdStatus::CmdReg::BusMaster::ValueMask_Disable;
	flushCmdStatus();
	}
bool			Function::HeaderTypeZero::monitoringSpecialCycles() const noexcept{
	return (_cmdStatus & CmdStatus::CmdReg::SpecialCycles::FieldMask)
			==	CmdStatus::CmdReg::SpecialCycles::ValueMask_Enable
			;
	}
void			Function::HeaderTypeZero::respondToSpecialCycles() noexcept{
	_cmdStatus	&= ~CmdStatus::CmdReg::SpecialCycles::FieldMask;
	_cmdStatus	|= CmdStatus::CmdReg::SpecialCycles::ValueMask_Enable;
	flushCmdStatus();
	}
void			Function::HeaderTypeZero::ignoreSpecialCycles() noexcept{
	_cmdStatus	&= ~CmdStatus::CmdReg::SpecialCycles::FieldMask;
	_cmdStatus	|= CmdStatus::CmdReg::SpecialCycles::ValueMask_Disable;
	flushCmdStatus();
	}
bool			Function::HeaderTypeZero::memoryWriteAndInvalidateEnabled() const noexcept{
	return (_cmdStatus & CmdStatus::CmdReg::MemoryWriteAndInvalidate::FieldMask)
			==	CmdStatus::CmdReg::MemoryWriteAndInvalidate::ValueMask_Enable
			;
	}
void			Function::HeaderTypeZero::enableMemoryWriteAndInvalidate() noexcept{
	_cmdStatus	&= ~CmdStatus::CmdReg::MemoryWriteAndInvalidate::FieldMask;
	_cmdStatus	|= CmdStatus::CmdReg::MemoryWriteAndInvalidate::
					ValueMask_Enable;
	flushCmdStatus();
	}
void			Function::HeaderTypeZero::disableMemoryWriteAndInvalidate() noexcept{
	_cmdStatus	&= ~CmdStatus::CmdReg::MemoryWriteAndInvalidate::FieldMask;
	_cmdStatus	|= CmdStatus::CmdReg::MemoryWriteAndInvalidate::
					ValueMask_Disable;
	flushCmdStatus();
	}
bool			Function::HeaderTypeZero::snoopingVgaPalette() const noexcept{
	return (_cmdStatus & CmdStatus::CmdReg::VgaPaletteSnoop::FieldMask)
			==	CmdStatus::CmdReg::VgaPaletteSnoop::ValueMask_Enable
			;
	}
void			Function::HeaderTypeZero::enableVgaPaletteSnoop() noexcept{
	_cmdStatus	&= ~CmdStatus::CmdReg::VgaPaletteSnoop::FieldMask;
	_cmdStatus	|= CmdStatus::CmdReg::VgaPaletteSnoop::ValueMask_Enable;
	flushCmdStatus();
	}
void			Function::HeaderTypeZero::disableVgaPaletteSnoop() noexcept{
	_cmdStatus	&= ~CmdStatus::CmdReg::VgaPaletteSnoop::FieldMask;
	_cmdStatus	|= CmdStatus::CmdReg::VgaPaletteSnoop::ValueMask_Disable;
	flushCmdStatus();
	}
bool			Function::HeaderTypeZero::parityErrorEnabled() const noexcept{
	return (_cmdStatus & CmdStatus::CmdReg::ParityErrorResponse::FieldMask)
			==	CmdStatus::CmdReg::ParityErrorResponse::ValueMask_Enable
			;
	}
void			Function::HeaderTypeZero::enableParityError() noexcept{
	_cmdStatus	&= ~CmdStatus::CmdReg::ParityErrorResponse::FieldMask;
	_cmdStatus	|= CmdStatus::CmdReg::ParityErrorResponse::ValueMask_Enable;
	flushCmdStatus();
	}
void			Function::HeaderTypeZero::disableParityError() noexcept{
	_cmdStatus	&= ~CmdStatus::CmdReg::ParityErrorResponse::FieldMask;
	_cmdStatus	|= CmdStatus::CmdReg::ParityErrorResponse::ValueMask_Disable;
	flushCmdStatus();
	}
bool			Function::HeaderTypeZero::steppingEnabled() const noexcept{
	return (_cmdStatus & CmdStatus::CmdReg::SteppingControl::FieldMask)
			==	CmdStatus::CmdReg::SteppingControl::ValueMask_Enable
			;
	}
void			Function::HeaderTypeZero::enableStepping() noexcept{
	_cmdStatus	&= ~CmdStatus::CmdReg::SteppingControl::FieldMask;
	_cmdStatus	|= CmdStatus::CmdReg::SteppingControl::ValueMask_Enable;
	flushCmdStatus();
	}
void			Function::HeaderTypeZero::disableStepping() noexcept{
	_cmdStatus	&= ~CmdStatus::CmdReg::SteppingControl::FieldMask;
	_cmdStatus	|= CmdStatus::CmdReg::SteppingControl::ValueMask_Disable;
	flushCmdStatus();
	}
bool			Function::HeaderTypeZero::systemErrorEnabled() const noexcept{
	return (_cmdStatus & CmdStatus::CmdReg::SystemError::FieldMask)
			==	CmdStatus::CmdReg::SystemError::ValueMask_Enable
			;
	}
void			Function::HeaderTypeZero::enableSystemError() noexcept{
	_cmdStatus	&= ~CmdStatus::CmdReg::SystemError::FieldMask;
	_cmdStatus	|= CmdStatus::CmdReg::SystemError::ValueMask_Enable;
	flushCmdStatus();
	}
void			Function::HeaderTypeZero::disableSystemError() noexcept{
	_cmdStatus	&= ~CmdStatus::CmdReg::SystemError::FieldMask;
	_cmdStatus	|= CmdStatus::CmdReg::SystemError::ValueMask_Disable;
	flushCmdStatus();
	}
bool			Function::HeaderTypeZero::fastBackToBackEnabled() const noexcept{
	return (_cmdStatus & CmdStatus::CmdReg::FastBackToBack::FieldMask)
			==	CmdStatus::CmdReg::FastBackToBack::ValueMask_Enable
			;
	}
void			Function::HeaderTypeZero::enableFastBackToBack() noexcept{
	_cmdStatus	&= ~CmdStatus::CmdReg::FastBackToBack::FieldMask;
	_cmdStatus	|= CmdStatus::CmdReg::FastBackToBack::ValueMask_Enable;
	flushCmdStatus();
	}
void			Function::HeaderTypeZero::disableFastBackToBack() noexcept{
	_cmdStatus	&= ~CmdStatus::CmdReg::FastBackToBack::FieldMask;
	_cmdStatus	|= CmdStatus::CmdReg::FastBackToBack::ValueMask_Disable;
	flushCmdStatus();
	}

bool			Function::HeaderTypeZero::hasCapabilitiesList() const noexcept{
	return (_cmdStatus & CmdStatus::StatusReg::CapabilitiesList::FieldMask)
			==	CmdStatus::StatusReg::CapabilitiesList::ValueMask_Implemented
			;
	}

bool			Function::HeaderTypeZero::capableOf66MHz() const noexcept{
	return (_cmdStatus & CmdStatus::StatusReg::CapableOf66MHz::FieldMask)
			==	CmdStatus::StatusReg::CapableOf66MHz::ValueMask_Capable
			;
	}

bool			Function::HeaderTypeZero::udfSupported() const noexcept{
	return (_cmdStatus & CmdStatus::StatusReg::UdfSupport::FieldMask)
			==	CmdStatus::StatusReg::UdfSupport::ValueMask_Supported
			;
	}
bool			Function::HeaderTypeZero::fastBackToBackCapable() const noexcept{
	return (_cmdStatus & CmdStatus::StatusReg::FastBackToBackCapable::FieldMask)
			==	CmdStatus::StatusReg::FastBackToBackCapable::ValueMask_Capable
			;
	}
bool			Function::HeaderTypeZero::masterDataParityError() const noexcept{
	return (_cmdStatus & CmdStatus::StatusReg::MasterDataParityError::FieldMask)
			==	CmdStatus::StatusReg::MasterDataParityError::ValueMask_Error
			;
	}

void			Function::HeaderTypeZero::clearMasterDataParityError() noexcept{
	clearStatus(CmdStatus::StatusReg::MasterDataParityError::ValueMask_Clear);
	}

Oscl::Pci::Config::Function::DevSelTiming
Function::HeaderTypeZero::deviceSelectTiming() const noexcept{
	return  (Oscl::Pci::Config::Function::DevSelTiming)(
			(_cmdStatus & CmdStatus::StatusReg::DeviceSelectTiming::FieldMask)
			>>	CmdStatus::StatusReg::DeviceSelectTiming::Lsb
			);
	}
bool			Function::HeaderTypeZero::signaledTargetAbort() const noexcept{
	return (_cmdStatus & CmdStatus::StatusReg::SignaledTargetAbort::FieldMask)
			==	CmdStatus::StatusReg::SignaledTargetAbort::ValueMask_Abort
			;
	}
void			Function::HeaderTypeZero::clearSignaledTargetAbort() noexcept{
	clearStatus(CmdStatus::StatusReg::SignaledTargetAbort::ValueMask_Clear);
	}
bool			Function::HeaderTypeZero::receivedTargetAbort() const noexcept{
	return (_cmdStatus & CmdStatus::StatusReg::ReceivedTargetAbort::FieldMask)
			==	CmdStatus::StatusReg::ReceivedTargetAbort::ValueMask_Abort
			;
	}
void			Function::HeaderTypeZero::clearReceivedTargetAbort() noexcept{
	clearStatus(CmdStatus::StatusReg::ReceivedTargetAbort::ValueMask_Clear);
	}
bool			Function::HeaderTypeZero::receivedMasterAbort() const noexcept{
	return (_cmdStatus & CmdStatus::StatusReg::ReceivedMasterAbort::FieldMask)
			==	CmdStatus::StatusReg::ReceivedMasterAbort::ValueMask_Abort
			;
	}
void			Function::HeaderTypeZero::clearReceivedMasterAbort() noexcept{
	clearStatus(CmdStatus::StatusReg::ReceivedMasterAbort::ValueMask_Clear);
	}
bool			Function::HeaderTypeZero::signaledSystemError() const noexcept{
	return (_cmdStatus & CmdStatus::StatusReg::SignaledSystemError::FieldMask)
			==	CmdStatus::StatusReg::SignaledSystemError::ValueMask_Error
			;
	}
void			Function::HeaderTypeZero::clearSignaledSystemError() noexcept{
	clearStatus(CmdStatus::StatusReg::SignaledSystemError::ValueMask_Clear);
	}
bool			Function::HeaderTypeZero::detectedParityError() const noexcept{
	return (_cmdStatus & CmdStatus::StatusReg::DetectedParityError::FieldMask)
			==	CmdStatus::StatusReg::DetectedParityError::ValueMask_Error
			;
	}
void			Function::HeaderTypeZero::clearDetectedParityError() noexcept{
	clearStatus(CmdStatus::StatusReg::DetectedParityError::ValueMask_Clear);
	}
unsigned char	Function::HeaderTypeZero::baseClass() const noexcept{
	return (_classRev & ClassRev::ClassCode::FieldMask)
			>>	ClassRev::ClassCode::Lsb
			;
	}
unsigned char	Function::HeaderTypeZero::subClass() const noexcept{
	return (_classRev & ClassRev::SubClassCode::FieldMask)
			>>	ClassRev::SubClassCode::Lsb
			;
	}
unsigned char	Function::HeaderTypeZero::programmingInterface() const noexcept{
	return (_classRev & ClassRev::ProgIf::FieldMask)
			>>	ClassRev::ProgIf::Lsb
			;
	}
unsigned char	Function::HeaderTypeZero::revisionID() const noexcept{
	return (_classRev & ClassRev::Revision::FieldMask)
			>>	ClassRev::Revision::Lsb
			;
	}
bool			Function::HeaderTypeZero::singleFunction() const noexcept{
	return (_bistTypeLatCache & BistTypeLatCache::MultiFunction::FieldMask)
			==	BistTypeLatCache::MultiFunction::ValueMask_SingleFunctionDevice
			;
	}
Oscl::Pci::Config::Function::HeaderType
Function::HeaderTypeZero::headerType() const noexcept{
	return  (Oscl::Pci::Config::Function::HeaderType)(
			(_bistTypeLatCache & BistTypeLatCache::HeaderFormat::FieldMask)
			>>	BistTypeLatCache::HeaderFormat::Lsb
			);
	}
unsigned char	Function::HeaderTypeZero::cacheLineSize() const noexcept{
	return (_bistTypeLatCache & BistTypeLatCache::CacheLineSize::FieldMask)
			>>	BistTypeLatCache::CacheLineSize::Lsb
			;
	}
void			Function::HeaderTypeZero::setCacheLineSize(unsigned char dwords)noexcept{
	_bistTypeLatCache	&= ~BistTypeLatCache::CacheLineSize::FieldMask;
	_bistTypeLatCache	|= (	(dwords<<BistTypeLatCache::CacheLineSize::Lsb)
							&	BistTypeLatCache::CacheLineSize::FieldMask
							);
	flushBistTypeLatCache();
	}
unsigned char	Function::HeaderTypeZero::latencyTimer() const noexcept{
	return (_bistTypeLatCache & BistTypeLatCache::LatencyTimer::FieldMask)
			>>	BistTypeLatCache::LatencyTimer::Lsb
			;
	}
void			Function::HeaderTypeZero::setLatencyTimer(unsigned char pciClks)noexcept{
	_bistTypeLatCache	&= ~BistTypeLatCache::LatencyTimer::FieldMask;
	_bistTypeLatCache	|=		(pciClks << BistTypeLatCache::LatencyTimer::Lsb)
							&	BistTypeLatCache::LatencyTimer::FieldMask;
	flushBistTypeLatCache();
	readBistTypeLatCache();
	}
bool			Function::HeaderTypeZero::bistCapable() const noexcept{
	return (_bistTypeLatCache & BistTypeLatCache::BistCapable::FieldMask)
			==	BistTypeLatCache::BistCapable::ValueMask_Capable
			;
	}

void			Function::HeaderTypeZero::startBuiltInSelfTest() noexcept{
	BistTypeLatCache::Reg	mask	= _bistTypeLatCache;
	mask	&= (	BistTypeLatCache::LatencyTimer::FieldMask
				|	BistTypeLatCache::CacheLineSize::FieldMask
				);
	mask	|= BistTypeLatCache::BistStart::ValueMask_Start;
	_config.writeDword(	_bus,
						_device,
						_function,
						DwRegOffset::Bist,
						mask
						);
	}

unsigned char	Function::HeaderTypeZero::bistCompletionCode() const noexcept{
	return (_bistTypeLatCache & BistTypeLatCache::CompletionCode::FieldMask)
			>>	BistTypeLatCache::CompletionCode::Lsb
			;
	}

bool	Function::HeaderTypeZero::isImplemented(unsigned char bar) const noexcept{
	return _baseAddress[bar];
	}

bool	Function::HeaderTypeZero::isIoAddress(unsigned char bar) const noexcept{
	return _baseAddress[bar] & BaseAddress::SpaceType::FieldMask;
	}

bool	Function::HeaderTypeZero::prefetchable(unsigned char bar) const noexcept{
	return _baseAddress[bar] & BaseAddress::Prefetchable::FieldMask;
	}

Oscl::Pci::Config::Function::DecoderConstraint
Function::HeaderTypeZero::barConstraint(unsigned char bar) const noexcept{
	return (Oscl::Pci::Config::Function::DecoderConstraint)(
			(_baseAddress[bar] & BaseAddress::DecoderType::FieldMask)
			>>BaseAddress::DecoderType::Lsb
			);
	}

uint32_t	Function::HeaderTypeZero::blockSizeMask(unsigned char bar) noexcept{
	_baseAddress[bar]	= ~(uint32_t)0;
	flushBaseAddress(bar);
	readBaseAddress(bar);
	return _baseAddress[bar] & BaseAddress::Address::FieldMask;
	}

void				Function::HeaderTypeZero::setAddress(	unsigned char	bar,
												uint32_t		address
												) noexcept{
	_baseAddress[bar]	&= ~BaseAddress::Address::FieldMask;
	if(address != (address & BaseAddress::Address::FieldMask)){
		while(true);
		}
	_baseAddress[bar]	|= address;
	flushBaseAddress(bar);
	}


void	Function::HeaderTypeZero::readID() noexcept{
	_id	= _config.readDword(	_bus,
								_device,
								_function,
								DwRegOffset::ID
								);
	}

void	Function::HeaderTypeZero::readCmdStatus() noexcept{
	_cmdStatus	= _config.readDword(	_bus,
										_device,
										_function,
										DwRegOffset::CmdStatus
										);
	}

void	Function::HeaderTypeZero::readClassRev() noexcept{
	_classRev	= _config.readDword(	_bus,
										_device,
										_function,
										DwRegOffset::ClassRev
										);
	}

void	Function::HeaderTypeZero::readBistTypeLatCache() noexcept{
	_bistTypeLatCache	= _config.readDword(	_bus,
												_device,
												_function,
												DwRegOffset::Bist
												);
	}

void	Function::HeaderTypeZero::readBaseAddress(unsigned baReg) noexcept{
	using namespace Oscl::Pci::Config::HeaderTypeZero::DwRegOffset;
	_baseAddress[baReg]	= _config.readDword(	_bus,
												_device,
												_function,
												BaseAddr0+baReg
												);
	}

void	Function::HeaderTypeZero::readSubsystem() noexcept{
	using namespace Oscl::Pci::Config::HeaderTypeZero::DwRegOffset;
	_subsystem	= _config.readDword(	_bus,
										_device,
										_function,
										SubsystemID
										);
	}

void	Function::HeaderTypeZero::readExpansionRomBaseAddr() noexcept{
	using namespace Oscl::Pci::Config::HeaderTypeZero::DwRegOffset;
	_expansionRomBaseAddr	= _config.readDword(	_bus,
													_device,
													_function,
													ExpRomBaseAddr
													);
	}

void	Function::HeaderTypeZero::readMaxLatMinGntIntPinIntLine() noexcept{
	using namespace Oscl::Pci::Config::HeaderTypeZero::DwRegOffset;
	_maxLatMinGntIntPinIntLine	= _config.readDword(	_bus,
														_device,
														_function,
														MaxLat
														);
	}

void	Function::HeaderTypeZero::flushCmdStatus() noexcept{
	_config.writeDword(	_bus,
						_device,
						_function,
						DwRegOffset::CmdStatus,
						_cmdStatus & CmdStatus::CmdReg::FieldMask
						);
	}

void	Function::HeaderTypeZero::flushBistTypeLatCache() noexcept{
	_config.writeDword(	_bus,
						_device,
						_function,
						DwRegOffset::Bist,
						_bistTypeLatCache & (
								BistTypeLatCache::LatencyTimer::FieldMask
							|	BistTypeLatCache::CacheLineSize::FieldMask
							)
						);
	}

void	Function::HeaderTypeZero::flushBaseAddress(unsigned baReg) noexcept{
	using namespace Oscl::Pci::Config::HeaderTypeZero::DwRegOffset;
	_config.writeDword(	_bus,
						_device,
						_function,
						BaseAddr0+baReg,
						_baseAddress[baReg]
						);
	}

void	Function::HeaderTypeZero::flushSubsystem() noexcept{
	using namespace Oscl::Pci::Config::HeaderTypeZero::DwRegOffset;
	_config.writeDword(	_bus,
						_device,
						_function,
						SubsystemID,
						_subsystem
						);
	}

void	Function::HeaderTypeZero::flushExpansionRomBaseAddr() noexcept{
	using namespace Oscl::Pci::Config::HeaderTypeZero::DwRegOffset;
	_config.writeDword(	_bus,
						_device,
						_function,
						ExpRomBaseAddr,
						_expansionRomBaseAddr
						);
	}

void	Function::HeaderTypeZero::flushMaxLatMinGntIntPinIntLine() noexcept{
	using namespace Oscl::Pci::Config::HeaderTypeZero::DwRegOffset;
	_config.writeDword(	_bus,
						_device,
						_function,
						MaxLat,
						_maxLatMinGntIntPinIntLine &
							(	Config::HeaderTypeZero::
									MaxLatMinGntIntPinIntLine::InterruptLine::
									FieldMask
								)
						);
	}

void	Function::HeaderTypeZero::clearStatus(CmdStatus::Reg bitsToClear) noexcept{
	CmdStatus::Reg	masked	= _cmdStatus;
	masked	&= ~CmdStatus::StatusReg::FieldMask;
	bitsToClear	&= CmdStatus::StatusReg::FieldMask;
	masked	|= bitsToClear;
	_config.writeDword(	_bus,
						_device,
						_function,
						DwRegOffset::CmdStatus,
						masked
						);
	}

void			Function::HeaderTypeZero::updateStatus() noexcept{
	readCmdStatus();
	}

