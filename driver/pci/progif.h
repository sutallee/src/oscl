/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_pci_progifh_
#define _oscl_drv_pci_progifh_

/** */
namespace Oscl {
/** */
namespace Pci {
/** */
namespace Config {

/** */
namespace MassStorageController {
	namespace SCSI {
		/** */
		class VisitorApi {
			public:
				/** */
				virtual void	generic() noexcept=0;
			};
		}
	namespace IDE {
		/** */
		class VisitorApi {
			public:
				/** */
				virtual void	generic(	bool	primaryOpMode,
											bool	primaryProgInd,
											bool	secondaryOpMode,
											bool	secondaryProgInd,
											bool	masterDevice
											) noexcept=0;
			};
		}
	namespace Floppy {
		/** */
		class VisitorApi {
			public:
				/** */
				virtual void	generic() noexcept=0;
			};
		}
	namespace IPI {
		/** */
		class VisitorApi {
			public:
				/** */
				virtual void	generic() noexcept=0;
			};
		}
	namespace RAID {
		/** */
		class VisitorApi {
			public:
				/** */
				virtual void	generic() noexcept=0;
			};
		}
	}

/** */
namespace NetworkController {
	namespace Ethernet {
		/** */
		class VisitorApi {
			public:
				/** */
				virtual void	generic() noexcept=0;
			};
		}
	namespace TokenRing {
		/** */
		class VisitorApi {
			public:
				/** */
				virtual void	generic() noexcept=0;
			};
		}
	namespace FDDI {
		/** */
		class VisitorApi {
			public:
				/** */
				virtual void	generic() noexcept=0;
			};
		}
	namespace ATM {
		/** */
		class VisitorApi {
			public:
				/** */
				virtual void	generic() noexcept=0;
			};
		}
	namespace ISDN {
		/** */
		class VisitorApi {
			public:
				/** */
				virtual void	generic() noexcept=0;
			};
		}
	}

/** */
namespace DisplayController {
	namespace VGA {
		/** */
		class VisitorApi {
			public:
				/** */
				virtual void	generic() noexcept=0;
				/** */
				virtual void	compatible8215() noexcept=0;
			};
		}
	namespace XGA {
		/** */
		class VisitorApi {
			public:
				/** */
				virtual void	generic() noexcept=0;
			};
		}
	namespace ThreeD {
		/** */
		class VisitorApi {
			public:
				/** */
				virtual void	generic() noexcept=0;
			};
		}
	}

/** */
namespace MultimediaDevice {
	namespace Video {
		/** */
		class VisitorApi {
			public:
				/** */
				virtual void	generic() noexcept=0;
			};
		}
	namespace Audio {
		/** */
		class VisitorApi {
			public:
				/** */
				virtual void	generic() noexcept=0;
			};
		}
	namespace ComputerTelephony {
		/** */
		class VisitorApi {
			public:
				/** */
				virtual void	generic() noexcept=0;
			};
		}
	}

/** */
namespace MemoryController {
	namespace RAM {
		/** */
		class VisitorApi {
			public:
				/** */
				virtual void	generic() noexcept=0;
			};
		}
	namespace Flash {
		/** */
		class VisitorApi {
			public:
				/** */
				virtual void	generic() noexcept=0;
			};
		}
	}

/** */
namespace BridgeDevice {
	/** */
	namespace HostPci {
		/** */
		class VisitorApi {
			public:
				/** */
				virtual void	generic() noexcept=0;
			};
		}
	/** */
	namespace PciIsa {
		/** */
		class VisitorApi {
			public:
				/** */
				virtual void	generic() noexcept=0;
			};
		}
	/** */
	namespace PciEisa {
		/** */
		class VisitorApi {
			public:
				/** */
				virtual void	generic() noexcept=0;
			};
		}
	/** */
	namespace PciMicroChannel {
		/** */
		class VisitorApi {
			public:
				/** */
				virtual void	generic() noexcept=0;
			};
		}
	/** */
	namespace PciPci {
		/** */
		class VisitorApi {
			public:
				/** */
				virtual void	generic() noexcept=0;
				/** */
				virtual void	pciSubtractive() noexcept=0;
			};
		}
	/** */
	namespace PciPcmcia {
		/** */
		class VisitorApi {
			public:
				/** */
				virtual void	generic() noexcept=0;
			};
		}
	/** */
	namespace PciNuBus {
		/** */
		class VisitorApi {
			public:
				/** */
				virtual void	generic() noexcept=0;
			};
		}
	/** */
	namespace PciCardBus {
		/** */
		class VisitorApi {
			public:
				/** */
				virtual void	generic() noexcept=0;
			};
		}
	/** */
	namespace PciRaceway {
		/** */
		class VisitorApi {
			public:
				/** */
				virtual void	generic() noexcept=0;
			};
		}
	}

/** */
namespace SimpleCommController {
	/** */
	namespace Serial {
		/** */
		class VisitorApi {
			public:
				/** */
				virtual void	genericXt() noexcept=0;
				/** */
				virtual void	compatible16450() noexcept=0;
				/** */
				virtual void	compatible16550() noexcept=0;
				/** */
				virtual void	compatible16650() noexcept=0;
				/** */
				virtual void	compatible16750() noexcept=0;
				/** */
				virtual void	compatible16850() noexcept=0;
				/** */
				virtual void	compatible16950() noexcept=0;
			};
		}
	/** */
	namespace Parallel {
		/** */
		class VisitorApi {
			public:
				/** */
				virtual void	generic() noexcept=0;
				/** */
				virtual void	biDirectional() noexcept=0;
				/** */
				virtual void	ecp1_XCompliant() noexcept=0;
				/** */
				virtual void	ieee1284Controller() noexcept=0;
				/** */
				virtual void	ieee1284Target() noexcept=0;
			};
		}
	/** */
	namespace MultiportSerial {
		/** */
		class VisitorApi {
			public:
				/** */
				virtual void	generic() noexcept=0;
			};
		}
	/** */
	namespace Modem {
		/** */
		class VisitorApi {
			public:
				/** */
				virtual void	generic() noexcept=0;
				/** */
				virtual void	hayes16450() noexcept=0;
				/** */
				virtual void	hayes16550() noexcept=0;
				/** */
				virtual void	hayes16650() noexcept=0;
				/** */
				virtual void	hayes16750() noexcept=0;
			};
		}
	}

/** */
namespace BaseSystemPeripheral {
	/** */
	namespace PIC {
		/** */
		class VisitorApi {
			public:
				/** */
				virtual void	generic8259() noexcept=0;
				/** */
				virtual void	isa() noexcept=0;
				/** */
				virtual void	eisa() noexcept=0;
				/** */
				virtual void	ioA() noexcept=0;
				/** */
				virtual void	ioxA() noexcept=0;
			};
		}
	/** */
	namespace DMA {
		/** */
		class VisitorApi {
			public:
				/** */
				virtual void	generic8237() noexcept=0;
				/** */
				virtual void	isa() noexcept=0;
				/** */
				virtual void	eisa() noexcept=0;
			};
		}
	}

/** */
namespace InputDevice {
	/** */
	namespace Keyboard {
		/** */
		class VisitorApi {
			public:
				/** */
				virtual void	generic() noexcept=0;
			};
		}
	/** */
	namespace Digitizer {
		/** */
		class VisitorApi {
			public:
				/** */
				virtual void	generic() noexcept=0;
			};
		}
	/** */
	namespace Mouse {
		/** */
		class VisitorApi {
			public:
				/** */
				virtual void	generic() noexcept=0;
			};
		}
	/** */
	namespace Scanner {
		/** */
		class VisitorApi {
			public:
				/** */
				virtual void	generic() noexcept=0;
			};
		}
	/** */
	namespace Gameport {
		/** */
		class VisitorApi {
			public:
				/** */
				virtual void	generic() noexcept=0;
				/** */
				virtual void	legacy() noexcept=0;
			};
		}
	}

/** */
namespace DockingStation {
	/** */
	namespace Generic {
		/** */
		class VisitorApi {
			public:
				/** */
				virtual void	generic() noexcept=0;
			};
		}
	}

/** */
namespace Processor {
	/** */
	namespace i386 {
		/** */
		class VisitorApi {
			public:
				/** */
				virtual void	generic() noexcept=0;
			};
		}
	/** */
	namespace i486 {
		/** */
		class VisitorApi {
			public:
				/** */
				virtual void	generic() noexcept=0;
			};
		}
	/** */
	namespace Pentium {
		/** */
		class VisitorApi {
			public:
				/** */
				virtual void	generic() noexcept=0;
			};
		}
	/** */
	namespace Alpha {
		/** */
		class VisitorApi {
			public:
				/** */
				virtual void	generic() noexcept=0;
			};
		}
	/** */
	namespace PowerPC {
		/** */
		class VisitorApi {
			public:
				/** */
				virtual void	generic() noexcept=0;
			};
		}
	/** */
	namespace MIPS {
		/** */
		class VisitorApi {
			public:
				/** */
				virtual void	generic() noexcept=0;
			};
		}
	/** */
	namespace CoPocessor {
		/** */
		class VisitorApi {
			public:
				/** */
				virtual void	generic() noexcept=0;
			};
		}
	}

/** */
namespace SerialBusController {
	/** */
	namespace Firewire {
		/** */
		class VisitorApi {
			public:
				/** */
				virtual void	generic() noexcept=0;
				/** */
				virtual void	ohci() noexcept=0;
			};
		}
	/** */
	namespace AccessBus {
		/** */
		class VisitorApi {
			public:
				/** */
				virtual void	generic() noexcept=0;
			};
		}
	/** */
	namespace SSA {
		/** */
		class VisitorApi {
			public:
				/** */
				virtual void	generic() noexcept=0;
			};
		}
	/** */
	namespace USB {
		/** */
		class VisitorApi {
			public:
				/** */
				virtual void	uhci() noexcept=0;
				/** */
				virtual void	ohci() noexcept=0;
				/** */
				virtual void	Unspecified() noexcept=0;
				/** */
				virtual void	Device() noexcept=0;
			};
		}
	/** */
	namespace FibreChannel {
		/** */
		class VisitorApi {
			public:
				/** */
				virtual void	generic() noexcept=0;
			};
		}
	/** */
	namespace SMBus {
		/** */
		class VisitorApi {
			public:
				/** */
				virtual void	generic() noexcept=0;
			};
		}
	}

/** */
namespace WirelessController {
	/** */
	namespace iRDA {
		/** */
		class VisitorApi {
			public:
				/** */
				virtual void	generic() noexcept=0;
			};
		}
	/** */
	namespace ConsumerIR {
		/** */
		class VisitorApi {
			public:
				/** */
				virtual void	generic() noexcept=0;
			};
		}
	/** */
	namespace RF {
		/** */
		class VisitorApi {
			public:
				/** */
				virtual void	generic() noexcept=0;
			};
		}
	}

/** */
namespace IntelligentIoController {
	/** */
	namespace MessageFifo {
		/** */
		class VisitorApi {
			public:
				/** */
				virtual void	generic() noexcept=0;
			};
		}
	}

/** */
namespace SatelliteCommControler {
	/** */
	namespace TV {
		/** */
		class VisitorApi {
			public:
				/** */
				virtual void	generic() noexcept=0;
			};
		}
	/** */
	namespace Audio {
		/** */
		class VisitorApi {
			public:
				/** */
				virtual void	generic() noexcept=0;
			};
		}
	/** */
	namespace Voice {
		/** */
		class VisitorApi {
			public:
				/** */
				virtual void	generic() noexcept=0;
			};
		}
	/** */
	namespace Data {
		/** */
		class VisitorApi {
			public:
				/** */
				virtual void	generic() noexcept=0;
			};
		}
	}

/** */
namespace EncryptionController {
	/** */
	namespace NetworkComputing {
		/** */
		class VisitorApi {
			public:
				/** */
				virtual void	generic() noexcept=0;
			};
		}
	/** */
	namespace Entertainment {
		/** */
		class VisitorApi {
			public:
				/** */
				virtual void	generic() noexcept=0;
			};
		}
	}

/** */
namespace DataAcquisitionDspController {
	/** */
	namespace DPIO {
		/** */
		class VisitorApi {
			public:
				/** */
				virtual void	generic() noexcept=0;
			};
		}
	}

}
}
}

#endif
