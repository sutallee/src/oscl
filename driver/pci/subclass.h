/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_pci_subclassh_
#define _oscl_drv_pci_subclassh_

/** */
namespace Oscl {
/** */
namespace Pci {
/** */
namespace Config {

/** */
namespace MassStorageController {
	/** */
	class VisitorApi {
		public:
			/** */
			virtual void	scsi() noexcept=0;
			/** */
			virtual void	ide() noexcept=0;
			/** */
			virtual void	floppy() noexcept=0;
			/** */
			virtual void	ipi() noexcept=0;
			/** */
			virtual void	raid() noexcept=0;
		};
	}

/** */
namespace NetworkController {
	/** */
	class VisitorApi {
		public:
			/** */
			virtual void	ethernet() noexcept=0;
			/** */
			virtual void	tokenRing() noexcept=0;
			/** */
			virtual void	fddi() noexcept=0;
			/** */
			virtual void	atm() noexcept=0;
			/** */
			virtual void	isdn() noexcept=0;
		};
	}

/** */
namespace DisplayController {
	/** */
	class VisitorApi {
		public:
			/** */
			virtual void	vga() noexcept=0;
			/** */
			virtual void	xga() noexcept=0;
			/** */
			virtual void	threeD() noexcept=0;
		};
	}

/** */
namespace MultimediaDevice {
	/** */
	class VisitorApi {
		public:
			/** */
			virtual void	video() noexcept=0;
			/** */
			virtual void	audio() noexcept=0;
			/** */
			virtual void	computerTelephony() noexcept=0;
		};
	}

/** */
namespace MemoryController {
	/** */
	class VisitorApi {
		public:
			/** */
			virtual void	ram() noexcept=0;
			/** */
			virtual void	flash() noexcept=0;
		};
	}

/** */
namespace BridgeDevice {
	/** */
	class VisitorApi {
		public:
			/** */
			virtual void	hostPci() noexcept=0;
			/** */
			virtual void	pciIsa() noexcept=0;
			/** */
			virtual void	pciEisa() noexcept=0;
			/** */
			virtual void	pciMicroChannel() noexcept=0;
			/** */
			virtual void	pciPci() noexcept=0;
			/** */
			virtual void	pciPcmcia() noexcept=0;
			/** */
			virtual void	pciNuBus() noexcept=0;
			/** */
			virtual void	pciCardBus() noexcept=0;
			/** */
			virtual void	pciRaceway() noexcept=0;
		};
	}

/** */
namespace SimpleCommController {
	/** */
	class VisitorApi {
		public:
			/** */
			virtual void	serial() noexcept=0;
			/** */
			virtual void	parallel() noexcept=0;
			/** */
			virtual void	multiportSerial() noexcept=0;
			/** */
			virtual void	modem() noexcept=0;
		};
	}

/** */
namespace BaseSystemPeripheral {
	/** */
	class VisitorApi {
		public:
			/** */
			virtual void	pic() noexcept=0;
			/** */
			virtual void	dma() noexcept=0;
			/** */
			virtual void	timer() noexcept=0;
			/** */
			virtual void	timer() noexcept=0;
			/** */
			virtual void	rtc() noexcept=0;
			/** */
			virtual void	pciHotPlug() noexcept=0;
		};
	}

/** */
namespace InputDevice {
	/** */
	class VisitorApi {
		public:
			/** */
			virtual void	keyboard() noexcept=0;
			/** */
			virtual void	digitizer() noexcept=0;
			/** */
			virtual void	mouse() noexcept=0;
			/** */
			virtual void	scanner() noexcept=0;
			/** */
			virtual void	gameport() noexcept=0;
		};
	}

/** */
namespace DockingStation {
	/** */
	class VisitorApi {
		public:
			/** */
			virtual void	generic() noexcept=0;
		};
	}

/** */
namespace Processor {
	/** */
	class VisitorApi {
		public:
			/** */
			virtual void	i386() noexcept=0;
			/** */
			virtual void	i486() noexcept=0;
			/** */
			virtual void	pentium() noexcept=0;
			/** */
			virtual void	alpha() noexcept=0;
			/** */
			virtual void	powerPC() noexcept=0;
			/** */
			virtual void	mips() noexcept=0;
			/** */
			virtual void	coProcessor() noexcept=0;
		};
	}

/** */
namespace SerialBusController {
	/** */
	class VisitorApi {
		public:
			/** */
			virtual void	firewire() noexcept=0;
			/** */
			virtual void	accessBus() noexcept=0;
			/** */
			virtual void	ssa() noexcept=0;
			/** */
			virtual void	usb() noexcept=0;
			/** */
			virtual void	fibreChannel() noexcept=0;
			/** */
			virtual void	smbus() noexcept=0;
		};
	}

/** */
namespace WirelessController {
	/** */
	class VisitorApi {
		public:
			/** */
			virtual void	irda() noexcept=0;
			/** */
			virtual void	consumerIR() noexcept=0;
			/** */
			virtual void	rf() noexcept=0;
		};
	}

/** */
namespace IntelligentIoController {
	/** */
	class VisitorApi {
		public:
			/** */
			virtual void	unknown() noexcept=0;
		};
	}

/** */
namespace SatelliteCommControler {
	/** */
	class VisitorApi {
		public:
			/** */
			virtual void	tv() noexcept=0;
			/** */
			virtual void	audio() noexcept=0;
			/** */
			virtual void	voice() noexcept=0;
			/** */
			virtual void	data() noexcept=0;
		};
	}

/** */
namespace EncryptionController {
	/** */
	class VisitorApi {
		public:
			/** */
			virtual void	networkComputing() noexcept=0;
			/** */
			virtual void	entertainment() noexcept=0;
		};
	}

/** */
namespace DataAcquisitionDspController {
	/** */
	class VisitorApi {
		public:
			/** */
			virtual void	dpio() noexcept=0;
		};
	}

/** */
namespace DoesNotFitDefinedClassCode {
	/** */
	class VisitorApi {
		public:
			/** */
			virtual void	unknown() noexcept=0;
		};
	}
}
}
}

#endif
