/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "function.h"
#include "oscl/hw/pci/config.h"

using namespace Oscl::Pci::Config;
using namespace Oscl;

Function::Generic::Generic(	Oscl::Pci::Config::Api&	config,
							unsigned char			bus,
							unsigned char			device,
							unsigned char			function
							) noexcept:
		_config(config),
		_bus(bus),
		_device(device),
		_function(function)
		{
	}

bool			Function::Generic::update() noexcept{
	readID();
	if(!exists()) return false;
	readCmdStatus();
	readClassRev();
	readBistTypeLatCache();
	return true;
	}

bool	Function::Generic::exists() const noexcept{
	return vendorID() != ID::Vendor::Value_NoFunctionPresent;
	}

uint16_t		Function::Generic::deviceID() const noexcept{
	return (_id & ID::Device::FieldMask)>>ID::Device::Lsb;
	}

uint16_t		Function::Generic::vendorID() const noexcept{
	return (_id & ID::Vendor::FieldMask)>>ID::Vendor::Lsb;
	}

bool			Function::Generic::busMasteringEnabled() const noexcept{
	return (_cmdStatus & CmdStatus::CmdReg::BusMaster::FieldMask)
			==	CmdStatus::CmdReg::BusMaster::ValueMask_Enable
			;
	}
void			Function::Generic::enableBusMastering() noexcept{
	_cmdStatus	&= ~CmdStatus::CmdReg::BusMaster::FieldMask;
	_cmdStatus	|= CmdStatus::CmdReg::BusMaster::ValueMask_Enable;
	flushCmdStatus();
	}
void			Function::Generic::disableBusMastering() noexcept{
	_cmdStatus	&= ~CmdStatus::CmdReg::BusMaster::FieldMask;
	_cmdStatus	|= CmdStatus::CmdReg::BusMaster::ValueMask_Disable;
	flushCmdStatus();
	}
bool			Function::Generic::monitoringSpecialCycles() const noexcept{
	return (_cmdStatus & CmdStatus::CmdReg::SpecialCycles::FieldMask)
			==	CmdStatus::CmdReg::SpecialCycles::ValueMask_Enable
			;
	}
void			Function::Generic::respondToSpecialCycles() noexcept{
	_cmdStatus	&= ~CmdStatus::CmdReg::SpecialCycles::FieldMask;
	_cmdStatus	|= CmdStatus::CmdReg::SpecialCycles::ValueMask_Enable;
	flushCmdStatus();
	}
void			Function::Generic::ignoreSpecialCycles() noexcept{
	_cmdStatus	&= ~CmdStatus::CmdReg::SpecialCycles::FieldMask;
	_cmdStatus	|= CmdStatus::CmdReg::SpecialCycles::ValueMask_Disable;
	flushCmdStatus();
	}
bool			Function::Generic::memoryWriteAndInvalidateEnabled() const noexcept{
	return (_cmdStatus & CmdStatus::CmdReg::MemoryWriteAndInvalidate::FieldMask)
			==	CmdStatus::CmdReg::MemoryWriteAndInvalidate::ValueMask_Enable
			;
	}
void			Function::Generic::enableMemoryWriteAndInvalidate() noexcept{
	_cmdStatus	&= ~CmdStatus::CmdReg::MemoryWriteAndInvalidate::FieldMask;
	_cmdStatus	|= CmdStatus::CmdReg::MemoryWriteAndInvalidate::
					ValueMask_Enable;
	flushCmdStatus();
	}
void			Function::Generic::disableMemoryWriteAndInvalidate() noexcept{
	_cmdStatus	&= ~CmdStatus::CmdReg::MemoryWriteAndInvalidate::FieldMask;
	_cmdStatus	|= CmdStatus::CmdReg::MemoryWriteAndInvalidate::
					ValueMask_Disable;
	flushCmdStatus();
	}
bool			Function::Generic::snoopingVgaPalette() const noexcept{
	return (_cmdStatus & CmdStatus::CmdReg::VgaPaletteSnoop::FieldMask)
			==	CmdStatus::CmdReg::VgaPaletteSnoop::ValueMask_Enable
			;
	}
void			Function::Generic::enableVgaPaletteSnoop() noexcept{
	_cmdStatus	&= ~CmdStatus::CmdReg::VgaPaletteSnoop::FieldMask;
	_cmdStatus	|= CmdStatus::CmdReg::VgaPaletteSnoop::ValueMask_Enable;
	flushCmdStatus();
	}
void			Function::Generic::disableVgaPaletteSnoop() noexcept{
	_cmdStatus	&= ~CmdStatus::CmdReg::VgaPaletteSnoop::FieldMask;
	_cmdStatus	|= CmdStatus::CmdReg::VgaPaletteSnoop::ValueMask_Disable;
	flushCmdStatus();
	}
bool			Function::Generic::parityErrorEnabled() const noexcept{
	return (_cmdStatus & CmdStatus::CmdReg::ParityErrorResponse::FieldMask)
			==	CmdStatus::CmdReg::ParityErrorResponse::ValueMask_Enable
			;
	}
void			Function::Generic::enableParityError() noexcept{
	_cmdStatus	&= ~CmdStatus::CmdReg::ParityErrorResponse::FieldMask;
	_cmdStatus	|= CmdStatus::CmdReg::ParityErrorResponse::ValueMask_Enable;
	flushCmdStatus();
	}
void			Function::Generic::disableParityError() noexcept{
	_cmdStatus	&= ~CmdStatus::CmdReg::ParityErrorResponse::FieldMask;
	_cmdStatus	|= CmdStatus::CmdReg::ParityErrorResponse::ValueMask_Disable;
	flushCmdStatus();
	}
bool			Function::Generic::steppingEnabled() const noexcept{
	return (_cmdStatus & CmdStatus::CmdReg::SteppingControl::FieldMask)
			==	CmdStatus::CmdReg::SteppingControl::ValueMask_Enable
			;
	}
void			Function::Generic::enableStepping() noexcept{
	_cmdStatus	&= ~CmdStatus::CmdReg::SteppingControl::FieldMask;
	_cmdStatus	|= CmdStatus::CmdReg::SteppingControl::ValueMask_Enable;
	flushCmdStatus();
	}
void			Function::Generic::disableStepping() noexcept{
	_cmdStatus	&= ~CmdStatus::CmdReg::SteppingControl::FieldMask;
	_cmdStatus	|= CmdStatus::CmdReg::SteppingControl::ValueMask_Disable;
	flushCmdStatus();
	}
bool			Function::Generic::systemErrorEnabled() const noexcept{
	return (_cmdStatus & CmdStatus::CmdReg::SystemError::FieldMask)
			==	CmdStatus::CmdReg::SystemError::ValueMask_Enable
			;
	}
void			Function::Generic::enableSystemError() noexcept{
	_cmdStatus	&= ~CmdStatus::CmdReg::SystemError::FieldMask;
	_cmdStatus	|= CmdStatus::CmdReg::SystemError::ValueMask_Enable;
	flushCmdStatus();
	}
void			Function::Generic::disableSystemError() noexcept{
	_cmdStatus	&= ~CmdStatus::CmdReg::SystemError::FieldMask;
	_cmdStatus	|= CmdStatus::CmdReg::SystemError::ValueMask_Disable;
	flushCmdStatus();
	}
bool			Function::Generic::fastBackToBackEnabled() const noexcept{
	return (_cmdStatus & CmdStatus::CmdReg::FastBackToBack::FieldMask)
			==	CmdStatus::CmdReg::FastBackToBack::ValueMask_Enable
			;
	}
void			Function::Generic::enableFastBackToBack() noexcept{
	_cmdStatus	&= ~CmdStatus::CmdReg::FastBackToBack::FieldMask;
	_cmdStatus	|= CmdStatus::CmdReg::FastBackToBack::ValueMask_Enable;
	flushCmdStatus();
	}
void			Function::Generic::disableFastBackToBack() noexcept{
	_cmdStatus	&= ~CmdStatus::CmdReg::FastBackToBack::FieldMask;
	_cmdStatus	|= CmdStatus::CmdReg::FastBackToBack::ValueMask_Disable;
	flushCmdStatus();
	}

void			Function::Generic::updateStatus() noexcept{
	readCmdStatus();
	}

bool			Function::Generic::hasCapabilitiesList() const noexcept{
	return (_cmdStatus & CmdStatus::StatusReg::CapabilitiesList::FieldMask)
			==	CmdStatus::StatusReg::CapabilitiesList::ValueMask_Implemented
			;
	}

bool			Function::Generic::capableOf66MHz() const noexcept{
	return (_cmdStatus & CmdStatus::StatusReg::CapableOf66MHz::FieldMask)
			==	CmdStatus::StatusReg::CapableOf66MHz::ValueMask_Capable
			;
	}

bool			Function::Generic::udfSupported() const noexcept{
	return (_cmdStatus & CmdStatus::StatusReg::UdfSupport::FieldMask)
			==	CmdStatus::StatusReg::UdfSupport::ValueMask_Supported
			;
	}
bool			Function::Generic::fastBackToBackCapable() const noexcept{
	return (_cmdStatus & CmdStatus::StatusReg::FastBackToBackCapable::FieldMask)
			==	CmdStatus::StatusReg::FastBackToBackCapable::ValueMask_Capable
			;
	}
bool			Function::Generic::masterDataParityError() const noexcept{
	return (_cmdStatus & CmdStatus::StatusReg::MasterDataParityError::FieldMask)
			==	CmdStatus::StatusReg::MasterDataParityError::ValueMask_Error
			;
	}

void			Function::Generic::clearMasterDataParityError() noexcept{
	clearStatus(CmdStatus::StatusReg::MasterDataParityError::ValueMask_Clear);
	}

Oscl::Pci::Config::Function::DevSelTiming
Function::Generic::deviceSelectTiming() const noexcept{
	return  (Oscl::Pci::Config::Function::DevSelTiming)(
			(_cmdStatus & CmdStatus::StatusReg::DeviceSelectTiming::FieldMask)
			>>	CmdStatus::StatusReg::DeviceSelectTiming::Lsb
			);
	}
bool			Function::Generic::signaledTargetAbort() const noexcept{
	return (_cmdStatus & CmdStatus::StatusReg::SignaledTargetAbort::FieldMask)
			==	CmdStatus::StatusReg::SignaledTargetAbort::ValueMask_Abort
			;
	}
void			Function::Generic::clearSignaledTargetAbort() noexcept{
	clearStatus(CmdStatus::StatusReg::SignaledTargetAbort::ValueMask_Clear);
	}
bool			Function::Generic::receivedTargetAbort() const noexcept{
	return (_cmdStatus & CmdStatus::StatusReg::ReceivedTargetAbort::FieldMask)
			==	CmdStatus::StatusReg::ReceivedTargetAbort::ValueMask_Abort
			;
	}
void			Function::Generic::clearReceivedTargetAbort() noexcept{
	clearStatus(CmdStatus::StatusReg::ReceivedTargetAbort::ValueMask_Clear);
	}
bool			Function::Generic::receivedMasterAbort() const noexcept{
	return (_cmdStatus & CmdStatus::StatusReg::ReceivedMasterAbort::FieldMask)
			==	CmdStatus::StatusReg::ReceivedMasterAbort::ValueMask_Abort
			;
	}
void			Function::Generic::clearReceivedMasterAbort() noexcept{
	clearStatus(CmdStatus::StatusReg::ReceivedMasterAbort::ValueMask_Clear);
	}
bool			Function::Generic::signaledSystemError() const noexcept{
	return (_cmdStatus & CmdStatus::StatusReg::SignaledSystemError::FieldMask)
			==	CmdStatus::StatusReg::SignaledSystemError::ValueMask_Error
			;
	}
void			Function::Generic::clearSignaledSystemError() noexcept{
	clearStatus(CmdStatus::StatusReg::SignaledSystemError::ValueMask_Clear);
	}
bool			Function::Generic::detectedParityError() const noexcept{
	return (_cmdStatus & CmdStatus::StatusReg::DetectedParityError::FieldMask)
			==	CmdStatus::StatusReg::DetectedParityError::ValueMask_Error
			;
	}
void			Function::Generic::clearDetectedParityError() noexcept{
	clearStatus(CmdStatus::StatusReg::DetectedParityError::ValueMask_Clear);
	}
unsigned char	Function::Generic::baseClass() const noexcept{
	return (_classRev & ClassRev::ClassCode::FieldMask)
			>>	ClassRev::ClassCode::Lsb
			;
	}
unsigned char	Function::Generic::subClass() const noexcept{
	return (_classRev & ClassRev::SubClassCode::FieldMask)
			>>	ClassRev::SubClassCode::Lsb
			;
	}
unsigned char	Function::Generic::programmingInterface() const noexcept{
	return (_classRev & ClassRev::ProgIf::FieldMask)
			>>	ClassRev::ProgIf::Lsb
			;
	}
unsigned char	Function::Generic::revisionID() const noexcept{
	return (_classRev & ClassRev::Revision::FieldMask)
			>>	ClassRev::Revision::Lsb
			;
	}
bool			Function::Generic::singleFunction() const noexcept{
	return (_bistTypeLatCache & BistTypeLatCache::MultiFunction::FieldMask)
			==	BistTypeLatCache::MultiFunction::ValueMask_SingleFunctionDevice
			;
	}
Oscl::Pci::Config::Function::HeaderType
Function::Generic::headerType() const noexcept{
	return  (Oscl::Pci::Config::Function::HeaderType)(
			(_bistTypeLatCache & BistTypeLatCache::HeaderFormat::FieldMask)
			>>	BistTypeLatCache::HeaderFormat::Lsb
			);
	}
unsigned char	Function::Generic::cacheLineSize() const noexcept{
	return (_bistTypeLatCache & BistTypeLatCache::CacheLineSize::FieldMask)
			>>	BistTypeLatCache::CacheLineSize::Lsb
			;
	}
void			Function::Generic::setCacheLineSize(unsigned char dwords)noexcept{
	_bistTypeLatCache	&= ~BistTypeLatCache::CacheLineSize::FieldMask;
	_bistTypeLatCache	|= (	(dwords<<BistTypeLatCache::CacheLineSize::Lsb)
							&	BistTypeLatCache::CacheLineSize::FieldMask
							);
	flushBistTypeLatCache();
	}
unsigned char	Function::Generic::latencyTimer() const noexcept{
	return (_bistTypeLatCache & BistTypeLatCache::LatencyTimer::FieldMask)
			>>	BistTypeLatCache::LatencyTimer::Lsb
			;
	}
void			Function::Generic::setLatencyTimer(unsigned char pciClks)noexcept{
	_bistTypeLatCache	&= ~BistTypeLatCache::LatencyTimer::FieldMask;
	_bistTypeLatCache	|=		(pciClks << BistTypeLatCache::LatencyTimer::Lsb)
							&	BistTypeLatCache::LatencyTimer::FieldMask;
	flushBistTypeLatCache();
	readBistTypeLatCache();
	}
bool			Function::Generic::bistCapable() const noexcept{
	return (_bistTypeLatCache & BistTypeLatCache::BistCapable::FieldMask)
			==	BistTypeLatCache::BistCapable::ValueMask_Capable
			;
	}

void			Function::Generic::startBuiltInSelfTest() noexcept{
	BistTypeLatCache::Reg	mask	= _bistTypeLatCache;
	mask	&= (	BistTypeLatCache::LatencyTimer::FieldMask
				|	BistTypeLatCache::CacheLineSize::FieldMask
				);
	mask	|= BistTypeLatCache::BistStart::ValueMask_Start;
	_config.writeDword(	_bus,
						_device,
						_function,
						DwRegOffset::Bist,
						mask
						);
	}

unsigned char	Function::Generic::bistCompletionCode() const noexcept{
	return (_bistTypeLatCache & BistTypeLatCache::CompletionCode::FieldMask)
			>>	BistTypeLatCache::CompletionCode::Lsb
			;
	}

void	Function::Generic::readID() noexcept{
	_id	= _config.readDword(	_bus,
								_device,
								_function,
								DwRegOffset::ID
								);
	}

void	Function::Generic::readCmdStatus() noexcept{
	_cmdStatus	= _config.readDword(	_bus,
										_device,
										_function,
										DwRegOffset::CmdStatus
										);
	}

void	Function::Generic::readClassRev() noexcept{
	_classRev	= _config.readDword(	_bus,
										_device,
										_function,
										DwRegOffset::ClassRev
										);
	}

void	Function::Generic::readBistTypeLatCache() noexcept{
	_bistTypeLatCache	= _config.readDword(	_bus,
												_device,
												_function,
												DwRegOffset::Bist
												);
	}

void	Function::Generic::flushCmdStatus() noexcept{
	_config.writeDword(	_bus,
						_device,
						_function,
						DwRegOffset::CmdStatus,
						_cmdStatus & CmdStatus::CmdReg::FieldMask
						);
	}

void	Function::Generic::flushBistTypeLatCache() noexcept{
	_config.writeDword(	_bus,
						_device,
						_function,
						DwRegOffset::Bist,
						_bistTypeLatCache & (
								BistTypeLatCache::LatencyTimer::FieldMask
							|	BistTypeLatCache::CacheLineSize::FieldMask
							)
						);
	}

void	Function::Generic::clearStatus(CmdStatus::Reg bitsToClear) noexcept{
	CmdStatus::Reg	masked	= _cmdStatus;
	masked	&= CmdStatus::StatusReg::FieldMask;
	bitsToClear	&= ~CmdStatus::StatusReg::FieldMask;
	masked	|= bitsToClear;
	_config.writeDword(	_bus,
						_device,
						_function,
						DwRegOffset::CmdStatus,
						masked
						);
	}
