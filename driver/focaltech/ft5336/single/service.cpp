/*
   Copyright (C) 2024 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "service.h"
#include "oscl/error/info.h"

using namespace Oscl::Driver::Focaltech::Ft5336::Single;

Service::Service(
	Oscl::Mt::Itc::PostMsgApi&				papi,
	Oscl::Revision::Observer::
	Req::Api::SAP&							touchChangeSAP,
	Oscl::I2C::Master::SevenBit::
	Req::Api::SAP&							i2cSAP,
	Oscl::Touch::Single::Control::Api&		outputApi,
	uint8_t									i2cAddress
	) noexcept:
		_closeSync(
			*this,
			papi
			),
		_part(
			papi,
			touchChangeSAP,
			i2cSAP,
			outputApi,
			i2cAddress
			)
		{
	}

Oscl::Mt::Itc::Srv::OpenSyncApi&	Service::getOpenSyncApi() noexcept{
	return _closeSync;
	}

Oscl::Mt::Itc::Srv::CloseSyncApi&	Service::getCloseSyncApi() noexcept{
	return _closeSync;
	}

Oscl::Mt::Itc::Srv::Open::Req::Api::SAP&    Service::getOpenSAP() noexcept{
	return _closeSync.getOpenSAP();
	}

Oscl::Mt::Itc::Srv::Close::Req::Api::SAP&   Service::getCloseSAP() noexcept{
	return _closeSync.getSAP();
	}

void	Service::request(Oscl::Mt::Itc::Srv::Open::Req::Api::OpenReq& msg) noexcept{
	_part.start();
	msg.returnToSender();
	}

void	Service::request(Oscl::Mt::Itc::Srv::Close::Req::Api::CloseReq& msg) noexcept{
	_part.stop();
	msg.returnToSender();
	}

