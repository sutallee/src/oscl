/*
   Copyright (C) 2024 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_driver_focaltech_ft5336_single_parth_
#define _oscl_driver_focaltech_ft5336_single_parth_

#include "oscl/i2c/master/sevenbit/itc/respmem.h"
#include "oscl/mt/itc/postmsgapi.h"
#include "oscl/touch/single/control/api.h"
#include "oscl/revision/observer/respcomp.h"

/** */
namespace Oscl {

/** */
namespace Driver {

/** */
namespace Focaltech {

/** */
namespace Ft5336 {

/**	This driver only implements the single-touch
	functionality of the FT5336.
 */
namespace Single {

/**	Asynchronous FT5336 I2C touch driver.
 */
class Part : public Oscl::I2C::Master::SevenBit::Resp::Api {
	private:
		/** */
		Oscl::Mt::Itc::PostMsgApi&						_papi;

		/** */
		Oscl::Revision::Observer::Req::Api::SAP&		_touchChangeSAP;

		/** */
		Oscl::I2C::Master::SevenBit::Req::Api::SAP&		_i2cSAP;

		/** */
		Oscl::Touch::Single::Control::Api&				_outputApi;

		/** */
		const uint8_t									_i2cAddress;

	private:
		/** */
		Oscl::Revision::Observer::Resp::Composer<Part>		_touchChangeRespComposer;

		/** */
		Oscl::Revision::Observer::Req::Api::ChangePayload	_touchChangePayload;

		/** */
		Oscl::Revision::Observer::Resp::Api::ChangeResp		_touchChangeResp;

	private:
		/** */
		union {
			/** */
			Oscl::I2C::Master::SevenBit::Resp::WriteRespMem	writeRespMem;
			/** */
			Oscl::I2C::Master::SevenBit::Resp::ReadRespMem	readRespMem;
			} _i2c;

		/** */
		uint8_t												_i2cBuffer[4];

	public:
		/** */
		Part(
			Oscl::Mt::Itc::PostMsgApi&				papi,
			Oscl::Revision::Observer::
			Req::Api::SAP&							touchChangeSAP,
			Oscl::I2C::Master::SevenBit::
			Req::Api::SAP&							i2cSAP,
			Oscl::Touch::Single::Control::Api&		outputApi,
			uint8_t									i2cAddress
			) noexcept;

		/** */
		void	start() noexcept;

		/**	The current implementation does not implement
			the stopping of this driver.
		 */
		void	stop() noexcept;

	private: // Oscl::Revision::Observer::Resp::Composer _touchChangeRespComposer
		/**	*/
		void	touchChangeResponse(Oscl::Revision::Observer::Resp::Api::ChangeResp& msg) noexcept;

	private: // Oscl::I2C::Master::SevenBit::Resp::Api
        /** */
		void    response(Oscl::I2C::Master::SevenBit::Resp::Api::ReadResp& msg) noexcept;

        /** */
        void    response(Oscl::I2C::Master::SevenBit::Resp::Api::CancelReadResp& msg) noexcept;

        /** */
        void    response(Oscl::I2C::Master::SevenBit::Resp::Api::WriteResp& msg) noexcept;

        /** */
        void    response(Oscl::I2C::Master::SevenBit::Resp::Api::CancelWriteResp& msg) noexcept;

	};

}
}
}
}
}

#endif
