/*
   Copyright (C) 2024 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <new>
#include "part.h"
#include "oscl/decoder/linear/be/base.h"
#include "oscl/error/fatal.h"

using namespace Oscl::Driver::Focaltech::Ft5336::Single;

Part::Part(
	Oscl::Mt::Itc::PostMsgApi&				papi,
	Oscl::Revision::Observer::
	Req::Api::SAP&							touchChangeSAP,
	Oscl::I2C::Master::SevenBit::
	Req::Api::SAP&							i2cSAP,
	Oscl::Touch::Single::Control::Api&		outputApi,
	uint8_t									i2cAddress
	) noexcept:
		_papi( papi ),
		_touchChangeSAP( touchChangeSAP ),
		_i2cSAP( i2cSAP ),
		_outputApi( outputApi ),
		_i2cAddress( i2cAddress ),
		_touchChangeRespComposer(
			*this,
			&Part::touchChangeResponse,
			0
			),
		_touchChangePayload(
			0
			),
		_touchChangeResp(
			_touchChangeSAP.getReqApi(),
			_touchChangeRespComposer,
			papi,
			_touchChangePayload
			)
		{
	}

void	Part::start() noexcept {
	_touchChangeSAP.post( _touchChangeResp.getSrvMsg() );
	}

void	Part::stop() noexcept {
	Oscl::ErrorFatal::logAndExit(
		"%s: NOT IMPLEMENTED!\n",
		__PRETTY_FUNCTION__
		);
	}

void	Part::touchChangeResponse( Oscl::Revision::Observer::Resp::Api::ChangeResp& msg ) noexcept {

	static constexpr unsigned char	positionRegisterAddress = 0x03;

	static const uint8_t			positionAddress[] = {positionRegisterAddress};

	Oscl::I2C::Master::SevenBit::Req::Api::WritePayload&
	payload = *new( &_i2c.writeRespMem.payload )
				Oscl::I2C::Master::SevenBit::Req::Api::WritePayload(
					_i2cAddress,
					positionAddress,
					sizeof( positionAddress )
					);

	Oscl::I2C::Master::SevenBit::Resp::Api::WriteResp&
	resp = *new(&_i2c.writeRespMem.resp)
				Oscl::I2C::Master::SevenBit::Resp::Api::WriteResp(
					_i2cSAP.getReqApi(),
					*this,
					_papi,
					payload
					);

	_i2cSAP.post( resp.getSrvMsg() );
	}

void    Part::response(Oscl::I2C::Master::SevenBit::Resp::Api::WriteResp& msg) noexcept {

	Oscl::I2C::Master::SevenBit::Req::Api::ReadPayload&
	payload = *new( &_i2c.readRespMem.payload )
				Oscl::I2C::Master::SevenBit::Req::Api::ReadPayload(
					_i2cAddress,
					_i2cBuffer,
					sizeof( _i2cBuffer )
					);

	Oscl::I2C::Master::SevenBit::Resp::Api::ReadResp&
	resp = *new(&_i2c.readRespMem.resp)
				Oscl::I2C::Master::SevenBit::Resp::Api::ReadResp(
					_i2cSAP.getReqApi(),
					*this,
					_papi,
					payload
					);

	_i2cSAP.post( resp.getSrvMsg() );
	}

void    Part::response(Oscl::I2C::Master::SevenBit::Resp::Api::CancelWriteResp& msg) noexcept {
	// Not used
	}

void    Part::response(Oscl::I2C::Master::SevenBit::Resp::Api::ReadResp& msg) noexcept {
	Oscl::Decoder::Linear::BE::Base
	decoder(
		_i2cBuffer,
		sizeof( _i2cBuffer )
		);

	Oscl::Decoder::Linear::Api&
	decoderApi	= decoder;

	uint16_t	x;
	decoderApi.decode( x );

	uint16_t	y;
	decoderApi.decode( y );

	_outputApi.update(
		(_i2cBuffer[0] & 0x80)?true:false,	// pressed
		(x & 0x0FFF),	// x
		(y & 0xFFF)		// y
		);

	_touchChangeSAP.post( _touchChangeResp.getSrvMsg() );
	}

void    Part::response(Oscl::I2C::Master::SevenBit::Resp::Api::CancelReadResp& msg) noexcept {
	// Not used
	}

