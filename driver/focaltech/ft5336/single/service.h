/*
   Copyright (C) 2024 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_driver_focaltech_ft5336_single_serviceh_
#define _oscl_driver_focaltech_ft5336_single_serviceh_

#include "part.h"
#include "oscl/mt/itc/srv/close.h"

/** */
namespace Oscl {

/** */
namespace Driver {

/** */
namespace Focaltech {

/** */
namespace Ft5336 {

/** */
namespace Single {

/**	This class contains the driver Part and implements the
	ITC open/close interface allowing the driver to be
	run asynchronously.
 */
class Service : private Oscl::Mt::Itc::Srv::Close::Req::Api {
	private:
		/**	The ITC open/close synchronous interface
			essentials.
		 */
		Oscl::Mt::Itc::Srv::CloseSync		_closeSync;

		/**	The fundamental driver.
		 */
		Oscl::Driver::Focaltech::Ft5336::Single::Part		_part;

	public:
		/**	This constructor requires references to the
			externally provided ITC interfaces used by
			the driver.
			The papi is the shared mailbox that is used
			by this service and is provided by the Thread.
			The touchChangeSAP is the ITC interface used
			to detect changes in the FT5336 which triggers
			this driver into action. This is typically a
			result of a change in state of the FT5336 interrupt.
			The i2cSAP is the ITC interface used to communicate
			asynchronously with the FT5336 over the I2C bus.
			The outputApi is used to communicate changes in the
			touch controller to the upper layers through an
			ITC distributor.
			The i2cAddress is the seven-bit address of the
			FT5336 on the I2C bus.
		 */
		Service(
			Oscl::Mt::Itc::PostMsgApi&				papi,
			Oscl::Revision::Observer::
			Req::Api::SAP&							touchChangeSAP,
			Oscl::I2C::Master::SevenBit::
			Req::Api::SAP&							i2cSAP,
			Oscl::Touch::Single::Control::Api&		outputApi,
			uint8_t									i2cAddress
			) noexcept;

		/** The synchronous interface used to start the operation
			of this driver.
		 */
		Oscl::Mt::Itc::Srv::OpenSyncApi&	getOpenSyncApi() noexcept;

		/** The synchronous interface used to sopt the operation
			of this driver.
			Closing is NOT IMPLEMENTED at this time.
		 */
		Oscl::Mt::Itc::Srv::CloseSyncApi&	getCloseSyncApi() noexcept;

		/** The asynchronous interface used to start the operation
			of this driver.
		 */
        Oscl::Mt::Itc::Srv::Open::Req::Api::SAP&    getOpenSAP() noexcept;

		/** The asynchronous interface used to stop the operation
			of this driver.
			Closing is NOT IMPLEMENTED at this time.
		 */
        Oscl::Mt::Itc::Srv::Close::Req::Api::SAP&   getCloseSAP() noexcept;

	private: // Oscl::Mt::Itc::Srv::Open::Req::Api
		/** */
		void	request(Oscl::Mt::Itc::Srv::Open::Req::Api::OpenReq& msg) noexcept;

		/** */
		void	request(Oscl::Mt::Itc::Srv::Close::Req::Api::CloseReq& msg) noexcept;

	};

}
}
}
}
}

#endif
