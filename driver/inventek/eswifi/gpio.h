/*
   Copyright (C) 2024 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/


#ifndef _oscl_driver_inventek_eswifi_gpioh_
#define _oscl_driver_inventek_eswifi_gpioh_

/** */
namespace Oscl {
/** */
namespace Driver {
/** */
namespace Inventek {
/** */
namespace esWiFi {
/** */
namespace Gpio {

/** An interface to be implemented by the
	esWiFi driver context to implement the
	GPIO related functions.
 */
class Api {
	public:
		/** Return true if the es-WiFi data-ready
			signal is LOW.
		 */
		virtual	bool	dataReadyIsLOW() noexcept = 0;

		/** Assert the es-WiFi SPI chip-select.
		 */
		virtual	void	assertChipSelect() noexcept = 0;

		/** Negate the es-WiFi SPI chip-select.
		 */
		virtual	void	negateChipSelect() noexcept = 0;

		/** Assert the es-WiFi reset.
		 */
		virtual	void	assertReset() noexcept = 0;

		/** Negate the es-WiFi reset.
		 */
		virtual	void	negateReset() noexcept = 0;

		/** Assert the es-WiFi wakeup signal.
		 */
		virtual	void	assertWakeup() noexcept = 0;

		/** Negate the es-WiFi wakeup signal.
		 */
		virtual	void	negateWakeup() noexcept = 0;

		/** Select the es-WiFi BOOT0 pin such that it will
			boot normally into the WiFi code.
		 */
		virtual	void	selectBootMainFlashMemory() noexcept = 0;

		/** Select the es-WiFi BOOT0 pin such that it will
			boot into the boot-loader.
		 */
		virtual	void	selectBootSystemMemory() noexcept = 0;
	};

/** This composer makes it possible for an implementation
	to use composition to implement the GPIO context
	API rather than inheritance.
 */
template < class Context >
class Composer : public Api {
	private:
		/** */
		Context&	_context;

		/** */
		bool	( Context::*_dataReadyIsLOW )();

		/** */
		void	( Context::*_assertChipSelect )();

		/** */
		void	( Context::*_negateChipSelect )();

		/** */
		void	( Context::*_assertReset )();

		/** */
		void	( Context::*_negateReset )();

		/** */
		void	( Context::*_assertWakeup )();

		/** */
		void	( Context::*_negateWakeup )();

		/** */
		void	( Context::*_selectBootMainFlashMemory )();

		/** */
		void	( Context::*_selectBootSystemMemory )();

	public:
		/** */
		Composer(
			Context&	context,
			bool		(Context::*dataReadyIsLOW)(),
			void		(Context::*assertChipSelect)(),
			void		(Context::*negateChipSelect)(),
			void		(Context::*assertReset)(),
			void		(Context::*negateReset)(),
			void		(Context::*assertWakeup)(),
			void		(Context::*negateWakeup)(),
			void		(Context::*selectBootMainFlashMemory)(),
			void		(Context::*selectBootSystemMemory)()
			) noexcept;

	private:
		/** */
		bool	dataReadyIsLOW() noexcept override;

		/** */
		void	assertChipSelect() noexcept override;

		/** */
		void	negateChipSelect() noexcept override;

		/** */
		void	assertReset() noexcept override;

		/** */
		void	negateReset() noexcept override;

		/** */
		void	assertWakeup() noexcept override;

		/** */
		void	negateWakeup() noexcept override;

		/** */
		void	selectBootMainFlashMemory() noexcept override;

		/** */
		void	selectBootSystemMemory() noexcept override;
	};

template < class Context >
Composer< Context >::Composer(
		Context&	context,
		bool		(Context::*dataReadyIsLOW)(),
		void		(Context::*assertChipSelect)(),
		void		(Context::*negateChipSelect)(),
		void		(Context::*assertReset)(),
		void		(Context::*negateReset)(),
		void		(Context::*assertWakeup)(),
		void		(Context::*negateWakeup)(),
		void		(Context::*selectBootMainFlashMemory)(),
		void		(Context::*selectBootSystemMemory)()
		) noexcept:
	_context( context ),
	_dataReadyIsLOW( dataReadyIsLOW ),
	_assertChipSelect( assertChipSelect ),
	_negateChipSelect( negateChipSelect ),
	_assertReset( assertReset ),
	_negateReset( negateReset ),
	_assertWakeup( assertWakeup ),
	_negateWakeup( negateWakeup ),
	_selectBootMainFlashMemory( selectBootMainFlashMemory ),
	_selectBootSystemMemory( selectBootSystemMemory )
	{
    }

template < class Context >
bool	Composer< Context >::dataReadyIsLOW() noexcept {
	return (_context.*_dataReadyIsLOW)();
    }

template < class Context >
void	Composer< Context >::assertChipSelect() noexcept {
	(_context.*_assertChipSelect)();
    }

template < class Context >
void	Composer< Context >::negateChipSelect() noexcept {
	(_context.*_negateChipSelect)();
    }

template < class Context >
void	Composer< Context >::assertReset() noexcept {
	(_context.*_assertReset)();
    }

template < class Context >
void	Composer< Context >::negateReset() noexcept {
	(_context.*_negateReset)();
    }

template < class Context >
void	Composer< Context >::assertWakeup() noexcept {
	(_context.*_assertWakeup)();
    }

template < class Context >
void	Composer< Context >::negateWakeup() noexcept {
	(_context.*_negateWakeup)();
    }

template < class Context >
void	Composer< Context >::selectBootMainFlashMemory() noexcept {
	(_context.*_selectBootMainFlashMemory)();
    }

template < class Context >
void	Composer< Context >::selectBootSystemMemory() noexcept {
	(_context.*_selectBootSystemMemory)();
    }

}
}
}
}
}
#endif
