/*
   Copyright (C) 2024 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_driver_inventek_eswifi_symbiont_kernel_apih_
#define _oscl_driver_inventek_eswifi_symbiont_kernel_apih_

#include <functional>
#include <stddef.h>
#include "oscl/id/alloc/api.h"
#include "reqapi.h"

/** */
namespace Oscl {

/** */
namespace Driver {

/** */
namespace Inventek {

/** */
namespace esWiFi {

/** */
namespace Symbiont {

/** */
namespace Kernel {

/** This interface specifies all of the 
	host operations that may be used by 
	the symbiont.
 */
class Api {
	public:
		/** */
		virtual Oscl::Driver::Inventek::esWiFi::Symbiont::Kernel::Req::Api::SAP&	getSAP() noexcept = 0;

		/** */
		virtual Oscl::Id::Alloc::Api&	getSocketIdAllocator() noexcept = 0;
	};

}
}
}
}
}
}

#endif
