/*
   Copyright (C) 2024 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_driver_inventek_eswifi_symbiont_kernel_itc_reqapih_
#define _oscl_driver_inventek_eswifi_symbiont_kernel_itc_reqapih_

#include "oscl/mt/itc/mbox/srvreq.h"
#include "oscl/mt/itc/postmsgapi.h"
#include "oscl/mt/itc/mbox/sap.h"
#include "oscl/buffer/base.h"
#include "oscl/id/alloc/node.h"
#include <stdint.h>

/** */
namespace Oscl {

/** */
namespace Driver {

/** */
namespace Inventek {

/** */
namespace esWiFi {

/** */
namespace Symbiont {

/** */
namespace Kernel {

/** */
namespace Req {

/**	This class describes an ITC server interface. The
	interface includes the definition of messages and their
	associated payloads, as well as an interface that is to be
	implemented by a server that supports this ITC interface.
 */
/**	This interface is used to attach and detach a symbiont
	with its host.
 */
class Api {
	public:
		/** This class contains the data that is transfered
			between the client and server within the WriteReq
			ITC message.
		 */
		class WritePayload {
			public:
				/**	The socket ID to apply.
				 */
				const Oscl::Id::Alloc::Node&	_socketID;

				/**	
				 */
				const Oscl::Buffer::Base&	_buffer;

				/**	
				 */
				unsigned long	_nWritten;

			public:
				/** The WritePayload constructor. */
				WritePayload(

					const Oscl::Id::Alloc::Node&	socketID,

					const Oscl::Buffer::Base&	buffer
					) noexcept;

			};

		/**	
		 */
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::Driver::Inventek::esWiFi::Symbiont::Kernel::Req::Api,
					WritePayload
					>		WriteReq;

	public:
		/** This class contains the data that is transfered
			between the client and server within the ReadReq
			ITC message.
		 */
		class ReadPayload {
			public:
				/**	The socket ID to apply.
				 */
				const Oscl::Id::Alloc::Node&	_socketID;

				/**	
				 */
				Oscl::Buffer::Base&	_buffer;

				/**	
				 */
				bool	_failed;

			public:
				/** The ReadPayload constructor. */
				ReadPayload(

					const Oscl::Id::Alloc::Node&	socketID,

					Oscl::Buffer::Base&	buffer
					) noexcept;

			};

		/**	
		 */
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::Driver::Inventek::esWiFi::Symbiont::Kernel::Req::Api,
					ReadPayload
					>		ReadReq;

	public:
		/** This class contains the data that is transfered
			between the client and server within the DisconnectReq
			ITC message.
		 */
		class DisconnectPayload {
			public:
				/**	The socket ID to apply.
				 */
				const Oscl::Id::Alloc::Node&	_socketID;

				/**	
				 */
				bool	_failed;

			public:
				/** The DisconnectPayload constructor. */
				DisconnectPayload(

					const Oscl::Id::Alloc::Node&	socketID
					) noexcept;

			};

		/**	
		 */
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::Driver::Inventek::esWiFi::Symbiont::Kernel::Req::Api,
					DisconnectPayload
					>		DisconnectReq;

	public:
		/** This class contains the data that is transfered
			between the client and server within the ConnectReq
			ITC message.
		 */
		class ConnectPayload {
			public:
				/**	The socket ID to apply.
				 */
				const Oscl::Id::Alloc::Node&	_socketID;

				/**	
				 */
				const char*	_hostname;

				/**	
				 */
				uint16_t	_port;

				/**	
				 */
				unsigned int	_transportProtocol;

				/**	
				 */
				bool	_failed;

			public:
				/** The ConnectPayload constructor. */
				ConnectPayload(

					const Oscl::Id::Alloc::Node&	socketID,

					const char*	hostname,

					uint16_t	port,

					unsigned int	transportProtocol
					) noexcept;

			};

		/**	
		 */
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::Driver::Inventek::esWiFi::Symbiont::Kernel::Req::Api,
					ConnectPayload
					>		ConnectReq;

	public:
		/** This type defines a SAP (Service Access Point) for this
			ITC server interface. A SAP holds server address information
			that is required by clients to send messages to a server.
		 */
		typedef Oscl::Mt::Itc::SAP<	Oscl::Driver::Inventek::esWiFi::Symbiont::Kernel::
							Req::Api
							>			SAP;

	public:
		/** This type defines a SAP (Service Access Point) for this
			ITC server interface. A SAP holds server address information
			that is required by clients to send messages to a server.
		 */
		typedef Oscl::Mt::Itc::ConcreteSAP<	Oscl::Driver::Inventek::esWiFi::Symbiont::Kernel::
							Req::Api
							>			ConcreteSAP;

	public:
		/** Make the compiler happy. */
		virtual ~Api(){}

		/** This operation is invoked within the server thread
			whenever a WriteReq is received.
		 */
		virtual void	request(	Oscl::Driver::Inventek::esWiFi::Symbiont::Kernel::
									Req::Api::WriteReq& msg
									) noexcept=0;

		/** This operation is invoked within the server thread
			whenever a ReadReq is received.
		 */
		virtual void	request(	Oscl::Driver::Inventek::esWiFi::Symbiont::Kernel::
									Req::Api::ReadReq& msg
									) noexcept=0;

		/** This operation is invoked within the server thread
			whenever a DisconnectReq is received.
		 */
		virtual void	request(	Oscl::Driver::Inventek::esWiFi::Symbiont::Kernel::
									Req::Api::DisconnectReq& msg
									) noexcept=0;

		/** This operation is invoked within the server thread
			whenever a ConnectReq is received.
		 */
		virtual void	request(	Oscl::Driver::Inventek::esWiFi::Symbiont::Kernel::
									Req::Api::ConnectReq& msg
									) noexcept=0;

	};

}
}
}
}
}
}
}
#endif
