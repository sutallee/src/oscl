/*
   Copyright (C) 2024 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <new>
#include "respmem.h"

using namespace Oscl::Driver::Inventek::esWiFi::Symbiont::Kernel;

Resp::Api::WriteResp&
	Resp::WriteMem::build(	Req::Api::SAP&			sap,
							Resp::Api&				respApi,
							Oscl::Mt::Itc::PostMsgApi&	clientPapi,
							const Oscl::Id::Alloc::Node&	socketID,
							const Oscl::Buffer::Base&	buffer
							) noexcept{
	Req::Api::WritePayload*
		p	=	new(&payload)
				Req::Api::WritePayload(
							socketID,
							buffer
							);
	Resp::Api::WriteResp*
		r	=	new(&resp)
			Resp::Api::WriteResp(	sap.getReqApi(),
									respApi,
									clientPapi,
									*p
									);

	return *r;
	}

Resp::Api::ReadResp&
	Resp::ReadMem::build(	Req::Api::SAP&			sap,
							Resp::Api&				respApi,
							Oscl::Mt::Itc::PostMsgApi&	clientPapi,
							const Oscl::Id::Alloc::Node&	socketID,
							Oscl::Buffer::Base&	buffer
							) noexcept{
	Req::Api::ReadPayload*
		p	=	new(&payload)
				Req::Api::ReadPayload(
							socketID,
							buffer
							);
	Resp::Api::ReadResp*
		r	=	new(&resp)
			Resp::Api::ReadResp(	sap.getReqApi(),
									respApi,
									clientPapi,
									*p
									);

	return *r;
	}

Resp::Api::DisconnectResp&
	Resp::DisconnectMem::build(	Req::Api::SAP&			sap,
							Resp::Api&				respApi,
							Oscl::Mt::Itc::PostMsgApi&	clientPapi,
							const Oscl::Id::Alloc::Node&	socketID
							) noexcept{
	Req::Api::DisconnectPayload*
		p	=	new(&payload)
				Req::Api::DisconnectPayload(
							socketID
							);
	Resp::Api::DisconnectResp*
		r	=	new(&resp)
			Resp::Api::DisconnectResp(	sap.getReqApi(),
									respApi,
									clientPapi,
									*p
									);

	return *r;
	}

Resp::Api::ConnectResp&
	Resp::ConnectMem::build(	Req::Api::SAP&			sap,
							Resp::Api&				respApi,
							Oscl::Mt::Itc::PostMsgApi&	clientPapi,
							const Oscl::Id::Alloc::Node&	socketID,
							const char*	hostname,
							uint16_t	port,
							unsigned int	transportProtocol
							) noexcept{
	Req::Api::ConnectPayload*
		p	=	new(&payload)
				Req::Api::ConnectPayload(
							socketID,
							hostname,
							port,
							transportProtocol
							);
	Resp::Api::ConnectResp*
		r	=	new(&resp)
			Resp::Api::ConnectResp(	sap.getReqApi(),
									respApi,
									clientPapi,
									*p
									);

	return *r;
	}

