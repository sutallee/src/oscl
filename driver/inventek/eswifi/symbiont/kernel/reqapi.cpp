/*
   Copyright (C) 2024 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "reqapi.h"

using namespace Oscl::Driver::Inventek::esWiFi::Symbiont::Kernel;

Oscl::Driver::Inventek::esWiFi::Symbiont::Kernel::Req::Api::WritePayload::
WritePayload(
		const Oscl::Id::Alloc::Node&	socketID,
		const Oscl::Buffer::Base&	buffer
		) noexcept:
		_socketID(socketID),
		_buffer(buffer)
		{}

Oscl::Driver::Inventek::esWiFi::Symbiont::Kernel::Req::Api::ReadPayload::
ReadPayload(
		const Oscl::Id::Alloc::Node&	socketID,
		Oscl::Buffer::Base&	buffer
		) noexcept:
		_socketID(socketID),
		_buffer(buffer)
		{}

Oscl::Driver::Inventek::esWiFi::Symbiont::Kernel::Req::Api::DisconnectPayload::
DisconnectPayload(
		const Oscl::Id::Alloc::Node&	socketID
		) noexcept:
		_socketID(socketID)
		{}

Oscl::Driver::Inventek::esWiFi::Symbiont::Kernel::Req::Api::ConnectPayload::
ConnectPayload(
		const Oscl::Id::Alloc::Node&	socketID,
		const char*	hostname,
		uint16_t	port,
		unsigned int	transportProtocol
		) noexcept:
		_socketID(socketID),
		_hostname(hostname),
		_port(port),
		_transportProtocol(transportProtocol)
		{}

