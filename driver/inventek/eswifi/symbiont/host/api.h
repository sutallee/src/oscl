/*
   Copyright (C) 2024 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_driver_inventek_eswifi_symbiont_host_apih_
#define _oscl_driver_inventek_eswifi_symbiont_host_apih_

#include "oscl/driver/inventek/eswifi/symbiont/api.h"

/** */
namespace Oscl {

/** */
namespace Driver {

/** */
namespace Inventek {

/** */
namespace esWiFi {

/** */
namespace Symbiont {

/** */
namespace Host {

/**	This class defines the operations that can be performed
	procedurally by the client.
 */
class Api {
	public:
		/** Make the compiler happy.
		 */
		virtual ~Api(){};

		/**	This operation attaches the symbiont to the esWiFi host.
		 */
		virtual void	attach(	
							Oscl::Driver::Inventek::esWiFi::Symbiont::Api&	symbiont
							) noexcept=0;

		/**	This operation detaches the symbiont from the esWiFi
			host.
		 */
		virtual void	detach(	
							Oscl::Driver::Inventek::esWiFi::Symbiont::Api&	symbiont
							) noexcept=0;

	};

}
}
}
}
}
}
#endif
