/*
   Copyright (C) 2024 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <new>
#include "respmem.h"

using namespace Oscl::Driver::Inventek::esWiFi::Symbiont::Host;

Resp::Api::AttachResp&
	Resp::AttachMem::build(	Req::Api::SAP&			sap,
							Resp::Api&				respApi,
							Oscl::Mt::Itc::PostMsgApi&	clientPapi,
							Oscl::Driver::Inventek::esWiFi::Symbiont::Api&	symbiont
							) noexcept{
	Req::Api::AttachPayload*
		p	=	new(&payload)
				Req::Api::AttachPayload(
							symbiont
							);
	Resp::Api::AttachResp*
		r	=	new(&resp)
			Resp::Api::AttachResp(	sap.getReqApi(),
									respApi,
									clientPapi,
									*p
									);

	return *r;
	}

Resp::Api::DetachResp&
	Resp::DetachMem::build(	Req::Api::SAP&			sap,
							Resp::Api&				respApi,
							Oscl::Mt::Itc::PostMsgApi&	clientPapi,
							Oscl::Driver::Inventek::esWiFi::Symbiont::Api&	symbiont
							) noexcept{
	Req::Api::DetachPayload*
		p	=	new(&payload)
				Req::Api::DetachPayload(
							symbiont
							);
	Resp::Api::DetachResp*
		r	=	new(&resp)
			Resp::Api::DetachResp(	sap.getReqApi(),
									respApi,
									clientPapi,
									*p
									);

	return *r;
	}

