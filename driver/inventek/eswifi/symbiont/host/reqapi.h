/*
   Copyright (C) 2024 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_driver_inventek_eswifi_symbiont_host_itc_reqapih_
#define _oscl_driver_inventek_eswifi_symbiont_host_itc_reqapih_

#include "oscl/mt/itc/mbox/srvreq.h"
#include "oscl/mt/itc/postmsgapi.h"
#include "oscl/mt/itc/mbox/sap.h"
#include "oscl/driver/inventek/eswifi/symbiont/api.h"

/** */
namespace Oscl {

/** */
namespace Driver {

/** */
namespace Inventek {

/** */
namespace esWiFi {

/** */
namespace Symbiont {

/** */
namespace Host {

/** */
namespace Req {

/**	This class describes an ITC server interface. The
	interface includes the definition of messages and their
	associated payloads, as well as an interface that is to be
	implemented by a server that supports this ITC interface.
 */
/**	This is the host interface for attaching and detaching a
	symbiont to/from the host. It is to be implemented by the
	host and used by the context. This interface is used to
	attach and detach a symbiont with its host.
 */
class Api {
	public:
		/** This class contains the data that is transfered
			between the client and server within the AttachReq
			ITC message.
		 */
		class AttachPayload {
			public:
				/**	A reference to the symbiont to be attached.
				 */
				Oscl::Driver::Inventek::esWiFi::Symbiont::Api&	_symbiont;

			public:
				/** The AttachPayload constructor. */
				AttachPayload(

					Oscl::Driver::Inventek::esWiFi::Symbiont::Api&	symbiont
					) noexcept;

			};

		/**	This message attaches a symbiont to the esWiFi host.
		 */
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::Driver::Inventek::esWiFi::Symbiont::Host::Req::Api,
					AttachPayload
					>		AttachReq;

	public:
		/** This class contains the data that is transfered
			between the client and server within the DetachReq
			ITC message.
		 */
		class DetachPayload {
			public:
				/**	A reference to the symbiont to be detached.
				 */
				Oscl::Driver::Inventek::esWiFi::Symbiont::Api&	_symbiont;

			public:
				/** The DetachPayload constructor. */
				DetachPayload(

					Oscl::Driver::Inventek::esWiFi::Symbiont::Api&	symbiont
					) noexcept;

			};

		/**	This message detaches a symbiont from the esWiFi host.
		 */
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::Driver::Inventek::esWiFi::Symbiont::Host::Req::Api,
					DetachPayload
					>		DetachReq;

	public:
		/** This type defines a SAP (Service Access Point) for this
			ITC server interface. A SAP holds server address information
			that is required by clients to send messages to a server.
		 */
		typedef Oscl::Mt::Itc::SAP<	Oscl::Driver::Inventek::esWiFi::Symbiont::Host::
							Req::Api
							>			SAP;

	public:
		/** This type defines a SAP (Service Access Point) for this
			ITC server interface. A SAP holds server address information
			that is required by clients to send messages to a server.
		 */
		typedef Oscl::Mt::Itc::ConcreteSAP<	Oscl::Driver::Inventek::esWiFi::Symbiont::Host::
							Req::Api
							>			ConcreteSAP;

	public:
		/** Make the compiler happy. */
		virtual ~Api(){}

		/** This operation is invoked within the server thread
			whenever a AttachReq is received.
		 */
		virtual void	request(	Oscl::Driver::Inventek::esWiFi::Symbiont::Host::
									Req::Api::AttachReq& msg
									) noexcept=0;

		/** This operation is invoked within the server thread
			whenever a DetachReq is received.
		 */
		virtual void	request(	Oscl::Driver::Inventek::esWiFi::Symbiont::Host::
									Req::Api::DetachReq& msg
									) noexcept=0;

	};

}
}
}
}
}
}
}
#endif
