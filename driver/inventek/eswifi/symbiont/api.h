/*
   Copyright (C) 2024 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_driver_inventek_eswifi_symbiont_apih_
#define _oscl_driver_inventek_eswifi_symbiont_apih_

#include <stdint.h>
#include "oscl/queue/queueitem.h"
#include "oscl/driver/inventek/eswifi/symbiont/kernel/api.h"

/** */
namespace Oscl {
/** */
namespace Driver {
/** */
namespace Inventek {
/** */
namespace esWiFi {
/** */
namespace Symbiont {

/** This interface is used to register an Element with
	the lower layer Mesh Network sub-system.
	The QueueItem link is reserved for use by the
	lower layer Mesh Network host sub-system.
 */
class Api : public Oscl::QueueItem {
	public:
		/** This operation is invoked once by the host
			when the symbiont is attached.
		 */
		virtual void	start( Oscl::Driver::Inventek::esWiFi::Symbiont::Kernel::Api& api ) noexcept = 0;

		/** This operation is invoked once by the host
			when the symbiont is detached. The implementation
			must halt operations and cease using the kernelApi.
		 */
		virtual void	stop() noexcept = 0;

		/** This operation is invoked by the host to give
			time to the symbiont to perform operations.
		 */
		virtual void	poll() noexcept = 0;
	};

}
}
}
}
}

#endif
