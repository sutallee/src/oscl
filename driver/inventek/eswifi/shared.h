/*
   Copyright (C) 2024 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_driver_inventek_eswifi_sharedh_
#define _oscl_driver_inventek_eswifi_sharedh_

#include "part.h"
#include "oscl/driver/shared/symbiont.h"

/** */
namespace Oscl {
/** */
namespace Driver {
/** */
namespace Inventek {
/** */
namespace esWiFi {

/** Implements the es-WiFi driver as a shared
	driver symbiont, allowing it to share a thread
	with other symbionts.
 */
class Shared : public Oscl::Driver::Shared::Symbiont::Api {
	private:
		/** */
		Oscl::Driver::Inventek::esWiFi::Part		_part;

	public:
		/** */
		Shared(
			Oscl::Mt::Itc::PostMsgApi&					papi,
			Oscl::SPI::Master16::Api&					spiApi,
			Gpio::Api&									gpioApi,
			Oscl::BoolType::Control::Api&				wifiUpControlApi,
			Oscl::Mt::Itc::Delay::Req::Api::SAP&		delaySAP
#if 0
			,
			Oscl::Revision::Observer::Req::Api::SAP&	readySAP
#endif
			) noexcept;

		/** */
		Oscl::WiFi::Control::Api&	getWifiControlSyncApi() noexcept;

		/** */
		Oscl::WiFi::Control::Req::Api::SAP&	getWifiControlSAP() noexcept;

		/** */
		Oscl::Driver::Inventek::esWiFi::Symbiont::Host::Api&	getHostSyncApi() noexcept;

		/** Returns the interface used to inform the system
			that the esWiFi ready signal has changed state
			from LOW to HIGH.
		 */
		Oscl::Revision::Control::Api&	getReadyControlApi() noexcept;

	private: // Oscl::Driver::Shared::Symbiont::Api
		/** */
		void	initialize() noexcept override;

		/** */
		void	process() noexcept override;
	};

}
}
}
}

#endif

