/*
   Copyright (C) 2024 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "symbiont.h"
#include <string.h>

using namespace Oscl::Driver::Inventek::esWiFi::Socket;

Symbiont::Symbiont(
		Oscl::Mt::Itc::PostMsgApi&	papi
		) noexcept:
	Base( papi ),
	_currentReadReq( nullptr ),
	_kernelApi( nullptr ),
	_socketID( nullptr ),
	_currentTransaction( nullptr ),
	_connected( false )
	{
	}

void	Symbiont::start( Oscl::Driver::Inventek::esWiFi::Symbiont::Kernel::Api& kernelApi ) noexcept {
	_kernelApi	= &kernelApi;
	}

void	Symbiont::stop( ) noexcept {
	_kernelApi	= nullptr;
	}

void	Symbiont::processNextReq() noexcept {

	Oscl::Mt::Itc::SrvMsg* req;
	while( ( req = _pendingReqQ.get() ) ) {
		req->process();
		if( _currentTransaction ) {
			break;
			}
		}

	}

void	Symbiont::transactionComplete() noexcept {
	_currentTransaction->~TransactionBase();
	_currentTransaction	= nullptr;
	processNextReq();
	}

void	Symbiont::request(
			Oscl::Socket::Ip::Connect::
			Req::Api::ConnectReq& msg
			) noexcept {

	if( _currentTransaction ) {
		_pendingReqQ.put( &msg );
		return;
		}

	auto&
	payload	= msg.getPayload();

	if( !_kernelApi || !_socketID || _connected ) {
		payload._failed	= true;
		msg.returnToSender();
		return;
		}

	_currentTransaction	=
		new ( &_mem.connect ) ConnectTrans(
			*this,
			msg
			);

	_currentTransaction->start();
	}

void	Symbiont::request(
			Oscl::Socket::Ip::Connect::
			Req::Api::CloseReq& msg
			) noexcept {

	if( _currentTransaction ) {
		_pendingReqQ.put( &msg );
		return;
		}

	auto&
	payload	= msg.getPayload();

	if( !_kernelApi ) {
		payload._failed	= true;
		msg.returnToSender();
		return;
		}

	if( !_socketID ) {
		payload._failed	= true;
		msg.returnToSender();
		return;
		}

	_currentTransaction	=
		new ( &_mem.disconnect ) DisconnectTrans(
			*this,
			msg
			);

	_currentTransaction->start();
	}

void	Symbiont::request(
			Oscl::Socket::Ip::Connect::
			Req::Api::OpenReq& msg
			) noexcept {

	if( _currentTransaction ) {
		_pendingReqQ.put( &msg );
		return;
		}

	auto&
	payload	= msg.getPayload();

	if( !_kernelApi ) {
		payload._failed	= true;
		msg.returnToSender();
		return;
		}

	if( _socketID ) {
		payload._failed	= true;
		msg.returnToSender();
		return;
		}

	_socketID	= _kernelApi->getSocketIdAllocator().alloc();

	payload._failed	= _socketID?false:true;

	msg.returnToSender();
	}

void	Symbiont::poll() noexcept {
	pollRead();
	}

void	Symbiont::pollRead() noexcept {

	if( _currentTransaction ) {
		return;
		}

	if( !_currentReadReq ) {
		_currentReadReq = _pendingReadQ.get();
		if( !_currentReadReq ) {
			return;
			}
		}	

	_currentTransaction	=
		new ( &_mem.read ) ReadTrans(
			*this,
			*_currentReadReq
			);

	_currentTransaction->start();
	}

void	Symbiont::request( Oscl::Stream::Input::Req::Api::ReadReq& msg ) noexcept {

	if( _currentTransaction ) {
		_pendingReqQ.put( &msg );
		return;
		}

	if( _currentReadReq ) {
		_pendingReadQ.put( &msg );
		return;
		}

	_currentReadReq	= &msg;

	pollRead();
	}

void	Symbiont::request( Oscl::Stream::Input::Req::Api::CancelReq& msg ) noexcept {

	if( _currentTransaction ) {
		_pendingReqQ.put( &msg );
		return;
		}

	auto&
	readToCancel	= msg.getPayload()._readToCancel;

	if( _currentReadReq == &readToCancel ) {
		msg.returnToSender();
		return;
		}

	_pendingReadQ.remove( &readToCancel );

	msg.returnToSender();
	}

void	Symbiont::request( Oscl::Stream::Output::Req::Api::WriteReq& msg ) noexcept {

	if( _currentTransaction ) {
		_pendingReqQ.put( &msg );
		return;
		}

	_currentTransaction	=
		new ( &_mem.write ) WriteTrans(
			*this,
			msg
			);

	_currentTransaction->start();
	}

void	Symbiont::request( Oscl::Stream::Output::Req::Api::FlushReq& msg ) noexcept {

	if( _currentTransaction ) {
		_pendingReqQ.put( &msg );
		return;
		}

	/*	Writes are synchronous. Nothing to flush.
	 */
	msg.returnToSender();
	}

