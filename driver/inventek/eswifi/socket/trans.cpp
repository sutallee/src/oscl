/*
   Copyright (C) 2024 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "trans.h"
#include "symbiont.h"

using namespace Oscl::Driver::Inventek::esWiFi::Socket;

TransactionBase::TransactionBase(
		Symbiont&	context
		) noexcept:
	_context( context )
	{
	}

TransactionBase::~TransactionBase() noexcept {
	}

ConnectTrans::ConnectTrans(
		Symbiont&					context,
		Oscl::Socket::Ip::Connect::
		Req::Api::ConnectReq&		msg
		) noexcept:
	TransactionBase( context ),
	_msg( msg )
	{
	}

void	ConnectTrans::start() noexcept {

	auto&
	payload	= _msg.getPayload();

	auto&
	resp	= _mem.build(
		_context._kernelApi->getSAP(),
		*this,
		_context._papi,
		*_context._socketID,
		payload._hostname,
		payload._port,
		payload._useSSL?3:0
		);

	_context._kernelApi->getSAP().post( resp.getSrvMsg() );
	}

void	ConnectTrans::response( Oscl::Driver::Inventek::esWiFi::Symbiont::Kernel::Resp::Api::WriteResp& msg ) noexcept {
	// not used
	}

void	ConnectTrans::response( Oscl::Driver::Inventek::esWiFi::Symbiont::Kernel::Resp::Api::ReadResp& msg ) noexcept {
	// not used
	}

void	ConnectTrans::response( Oscl::Driver::Inventek::esWiFi::Symbiont::Kernel::Resp::Api::DisconnectResp& msg ) noexcept {
	// not used
	}

void	ConnectTrans::response( Oscl::Driver::Inventek::esWiFi::Symbiont::Kernel::Resp::Api::ConnectResp& msg ) noexcept {

	_msg.getPayload()._failed	= msg.getPayload()._failed;

	_msg.returnToSender();

	_context.transactionComplete();
	}

DisconnectTrans::DisconnectTrans(
		Symbiont&					context,
		Oscl::Socket::Ip::Connect::
		Req::Api::CloseReq&			msg
		) noexcept:
	TransactionBase( context ),
	_msg( msg )
	{
	}

void	DisconnectTrans::start() noexcept {

	auto&
	resp	= _mem.build(
		_context._kernelApi->getSAP(),
		*this,
		_context._papi,
		*_context._socketID
		);

	_context._kernelApi->getSAP().post( resp.getSrvMsg() );
	}

void	DisconnectTrans::response( Oscl::Driver::Inventek::esWiFi::Symbiont::Kernel::Resp::Api::WriteResp& msg ) noexcept {
	// not used
	}

void	DisconnectTrans::response( Oscl::Driver::Inventek::esWiFi::Symbiont::Kernel::Resp::Api::ReadResp& msg ) noexcept {
	// not used
	}

void	DisconnectTrans::response( Oscl::Driver::Inventek::esWiFi::Symbiont::Kernel::Resp::Api::DisconnectResp& msg ) noexcept {

	_msg.getPayload()._failed	= msg.getPayload()._failed;

	_msg.returnToSender();

	_context._kernelApi->getSocketIdAllocator().free( _context._socketID );
	_context._socketID	= nullptr;

	_context.transactionComplete();
	}

void	DisconnectTrans::response( Oscl::Driver::Inventek::esWiFi::Symbiont::Kernel::Resp::Api::ConnectResp& msg ) noexcept {
	// not used
	}

WriteTrans::WriteTrans(
		Symbiont&				context,
		Oscl::Stream::Output::
		Req::Api::WriteReq&		msg
		) noexcept:
	TransactionBase( context ),
	_msg( msg )
	{
	}

void	WriteTrans::start() noexcept {

	auto&
	resp	= _mem.build(
		_context._kernelApi->getSAP(),
		*this,
		_context._papi,
		*_context._socketID,
		_msg.getPayload()._buffer
		);

	_context._kernelApi->getSAP().post( resp.getSrvMsg() );
	}

void	WriteTrans::response( Oscl::Driver::Inventek::esWiFi::Symbiont::Kernel::Resp::Api::WriteResp& msg ) noexcept {
	_msg.getPayload()._nWritten	= msg.getPayload()._nWritten;

	_msg.returnToSender();

	_context.transactionComplete();
	}

void	WriteTrans::response( Oscl::Driver::Inventek::esWiFi::Symbiont::Kernel::Resp::Api::ReadResp& msg ) noexcept {
	// not used
	}

void	WriteTrans::response( Oscl::Driver::Inventek::esWiFi::Symbiont::Kernel::Resp::Api::DisconnectResp& msg ) noexcept {
	// not used
	}

void	WriteTrans::response( Oscl::Driver::Inventek::esWiFi::Symbiont::Kernel::Resp::Api::ConnectResp& msg ) noexcept {
	// not used
	}

ReadTrans::ReadTrans(
		Symbiont&				context,
		Oscl::Stream::Input::
		Req::Api::ReadReq&		msg
		) noexcept:
	TransactionBase( context ),
	_msg( msg )
	{
	}

void	ReadTrans::start() noexcept {

	auto&
	resp	= _mem.build(
		_context._kernelApi->getSAP(),
		*this,
		_context._papi,
		*_context._socketID,
		_msg.getPayload()._buffer
		);

	_context._kernelApi->getSAP().post( resp.getSrvMsg() );
	}

void	ReadTrans::response( Oscl::Driver::Inventek::esWiFi::Symbiont::Kernel::Resp::Api::WriteResp& msg ) noexcept {
	// not used
	}

void	ReadTrans::response( Oscl::Driver::Inventek::esWiFi::Symbiont::Kernel::Resp::Api::ReadResp& msg ) noexcept {

	auto&
	payload	= msg.getPayload();

	_context._currentReadReq	= nullptr;

	if( payload._failed ) {
		_msg.returnToSender();
		}
	else {
		if( !payload._buffer.length() ) {
			_context._pendingReadQ.put( &_msg );
			}
		else {
			_msg.returnToSender();
			}
		}

	_context.transactionComplete();
	}

void	ReadTrans::response( Oscl::Driver::Inventek::esWiFi::Symbiont::Kernel::Resp::Api::DisconnectResp& msg ) noexcept {
	// not used
	}

void	ReadTrans::response( Oscl::Driver::Inventek::esWiFi::Symbiont::Kernel::Resp::Api::ConnectResp& msg ) noexcept {
	// not used
	}

