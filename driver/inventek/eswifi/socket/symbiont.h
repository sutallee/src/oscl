/*
   Copyright (C) 2024 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_driver_inventek_eswifi_socket_symbionth_
#define _oscl_driver_inventek_eswifi_socket_symbionth_

#include "oscl/queue/queue.h"
#include "oscl/driver/inventek/eswifi/symbiont/kernel/api.h"
#include "oscl/driver/inventek/eswifi/symbiont/kernel/respmem.h"
#include "oscl/driver/inventek/eswifi/symbiont/api.h"
#include "oscl/socket/ip/stream/base.h"
#include "trans.h"

/** */
namespace Oscl {
/** */
namespace Driver {
/** */
namespace Inventek {
/** */
namespace esWiFi {
/** */
namespace Socket {

/** */
class Symbiont :
		public Oscl::Driver::Inventek::esWiFi::Symbiont::Api,
		public Oscl::Socket::Ip::Stream::Base
		{
	private:
		/** */
		Oscl::Queue< Oscl::Mt::Itc::SrvMsg >	_pendingReqQ;

		/** */
		Oscl::Queue< Oscl::Stream::Input::Req::Api::ReadReq >	_pendingReadQ;

		/** */
		Oscl::Stream::Input::Req::Api::ReadReq*	_currentReadReq;

	private:
		/** */
		Oscl::Driver::Inventek::esWiFi::Symbiont::Kernel::Api*	_kernelApi;

		/** */
		Oscl::Id::Alloc::Node*	_socketID;

	private:

		friend ConnectTrans;
		friend WriteTrans;
		friend ReadTrans;
		friend DisconnectTrans;

		/** */
		union {
			/** */
			Oscl::Memory::AlignedBlock<
				sizeof( ConnectTrans )
				>						connect;
			/** */
			Oscl::Memory::AlignedBlock<
				sizeof( DisconnectTrans )
				>						disconnect;
			/** */
			Oscl::Memory::AlignedBlock<
				sizeof( WriteTrans )
				>						write;
			/** */
			Oscl::Memory::AlignedBlock<
				sizeof( ReadTrans )
				>						read;
			}	_mem;

		/** */
		TransactionBase*	_currentTransaction;

	private:
		/** */
		bool	_connected;

	public:
		/** */
		Symbiont(
			Oscl::Mt::Itc::PostMsgApi&	papi
			) noexcept;

	private: // Oscl::Driver::Inventek::esWiFi::Symbiont::Api
		/** This operation is invoked once by the host
			when the symbiont is attached.
		 */
		void	start( Oscl::Driver::Inventek::esWiFi::Symbiont::Kernel::Api& api ) noexcept;

		/** This operation is invoked once by the host
			when the symbiont is detached. The implementation
			must halt operations and cease using the kernelApi.
		 */
		void	stop() noexcept;

		/** This operation is invoked by the host to give
			time to the symbiont to perform operations.
		 */
		void	poll() noexcept;

	private:
		/** */
		void	pollRead() noexcept;

	private: // Oscl::Socket::Ip::Connect::Req::Api,
		/** This operation is invoked within the server thread
			whenever a ConnectReq is received.
		 */
		void	request(	Oscl::Socket::Ip::Connect::
							Req::Api::ConnectReq& msg
							) noexcept override;

		/** This operation is invoked within the server thread
			whenever a CloseReq is received.
		 */
		void	request(	Oscl::Socket::Ip::Connect::
							Req::Api::CloseReq& msg
							) noexcept override;

		/** This operation is invoked within the server thread
			whenever a OpenReq is received.
		 */
		void	request(	Oscl::Socket::Ip::Connect::
							Req::Api::OpenReq& msg
							) noexcept override;

	private:
		/** */
		void	processNextReq() noexcept;

		/** */
		void	transactionComplete() noexcept;

	private: // Oscl::Stream::Input::Req::Api,
		/** */
		void	request( Oscl::Stream::Input::Req::Api::ReadReq& msg ) noexcept override;

		/** */
		void	request( Oscl::Stream::Input::Req::Api::CancelReq& msg ) noexcept override;

	private: // Oscl::Stream::Output::Req::Api
		/** */
		void	request( Oscl::Stream::Output::Req::Api::WriteReq& msg ) noexcept override;

		/** */
		void	request( Oscl::Stream::Output::Req::Api::FlushReq& msg ) noexcept override;
	};

}
}
}
}
}

#endif
