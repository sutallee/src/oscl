/*
   Copyright (C) 2024 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_driver_inventek_eswifi_socket_transh_
#define _oscl_driver_inventek_eswifi_socket_transh_

#include "oscl/driver/inventek/eswifi/symbiont/kernel/api.h"
#include "oscl/driver/inventek/eswifi/symbiont/kernel/respmem.h"
#include "oscl/driver/inventek/eswifi/symbiont/api.h"
#include "oscl/socket/ip/stream/base.h"

/** */
namespace Oscl {
/** */
namespace Driver {
/** */
namespace Inventek {
/** */
namespace esWiFi {
/** */
namespace Socket {

/** */
class Symbiont;

/** */
class TransactionBase {
	protected:
		/** */
		Symbiont&	_context;

	public:
		/** */
		TransactionBase(
			Symbiont&	context
			) noexcept;

		/** */
		virtual ~TransactionBase() noexcept;

		/** */
		virtual void	start() noexcept = 0;
	};

/** */
class ConnectTrans:
		public TransactionBase,
		private Oscl::Driver::Inventek::esWiFi::Symbiont::Kernel::Resp::Api
		{
	private:
		/** */
		Oscl::Socket::Ip::Connect::Req::Api::ConnectReq&	_msg;

		/** */
		esWiFi::Symbiont::Kernel::Resp::ConnectMem			_mem;

	public:
		/** */
		ConnectTrans(
			Symbiont&					context,
			Oscl::Socket::Ip::Connect::
			Req::Api::ConnectReq&		msg
			) noexcept;

		/** */
		void	start() noexcept override;

	private: // Oscl::Driver::Inventek::esWiFi::Symbiont::Kernel::Resp::Api
		/**	*/
		void	response( Oscl::Driver::Inventek::esWiFi::Symbiont::Kernel::Resp::Api::WriteResp& msg ) noexcept override;

		/**	*/
		void	response( Oscl::Driver::Inventek::esWiFi::Symbiont::Kernel::Resp::Api::ReadResp& msg ) noexcept override;

		/**	*/
		void	response( Oscl::Driver::Inventek::esWiFi::Symbiont::Kernel::Resp::Api::DisconnectResp& msg ) noexcept override;

		/**	*/
		void	response( Oscl::Driver::Inventek::esWiFi::Symbiont::Kernel::Resp::Api::ConnectResp& msg ) noexcept override;
	};

/** */
class DisconnectTrans:
		public TransactionBase,
		private Oscl::Driver::Inventek::esWiFi::Symbiont::Kernel::Resp::Api
		{
	private:
		/** */
		Oscl::Socket::Ip::Connect::Req::Api::CloseReq&	_msg;

		/** */
		esWiFi::Symbiont::Kernel::Resp::DisconnectMem	_mem;

	public:
		/** */
		DisconnectTrans(
			Symbiont&					context,
			Oscl::Socket::Ip::Connect::
			Req::Api::CloseReq&			msg
			) noexcept;

		/** */
		void	start() noexcept override;

	private: // Oscl::Driver::Inventek::esWiFi::Symbiont::Kernel::Resp::Api
		/**	*/
		void	response( Oscl::Driver::Inventek::esWiFi::Symbiont::Kernel::Resp::Api::WriteResp& msg ) noexcept override;

		/**	*/
		void	response( Oscl::Driver::Inventek::esWiFi::Symbiont::Kernel::Resp::Api::ReadResp& msg ) noexcept override;

		/**	*/
		void	response( Oscl::Driver::Inventek::esWiFi::Symbiont::Kernel::Resp::Api::DisconnectResp& msg ) noexcept override;

		/**	*/
		void	response( Oscl::Driver::Inventek::esWiFi::Symbiont::Kernel::Resp::Api::ConnectResp& msg ) noexcept override;
	};

/** */
class WriteTrans:
		public TransactionBase,
		private Oscl::Driver::Inventek::esWiFi::Symbiont::Kernel::Resp::Api
		{
	private:
		/** */
		Oscl::Stream::Output::Req::Api::WriteReq&	_msg;

		/** */
		esWiFi::Symbiont::Kernel::Resp::WriteMem	_mem;

	public:
		/** */
		WriteTrans(
			Symbiont&						context,
			Oscl::Stream::Output::
			Req::Api::WriteReq&				msg
			) noexcept;

		/** */
		void	start() noexcept override;

	private: // Oscl::Driver::Inventek::esWiFi::Symbiont::Kernel::Resp::Api
		/**	*/
		void	response( Oscl::Driver::Inventek::esWiFi::Symbiont::Kernel::Resp::Api::WriteResp& msg ) noexcept override;

		/**	*/
		void	response( Oscl::Driver::Inventek::esWiFi::Symbiont::Kernel::Resp::Api::ReadResp& msg ) noexcept override;

		/**	*/
		void	response( Oscl::Driver::Inventek::esWiFi::Symbiont::Kernel::Resp::Api::DisconnectResp& msg ) noexcept override;

		/**	*/
		void	response( Oscl::Driver::Inventek::esWiFi::Symbiont::Kernel::Resp::Api::ConnectResp& msg ) noexcept override;
	};

/** */
class ReadTrans:
		public TransactionBase,
		private Oscl::Driver::Inventek::esWiFi::Symbiont::Kernel::Resp::Api
		{
	private:
		/** */
		Oscl::Stream::Input::Req::Api::ReadReq&		_msg;

		/** */
		esWiFi::Symbiont::Kernel::Resp::ReadMem		_mem;

	public:
		/** */
		ReadTrans(
			Symbiont&				context,
			Oscl::Stream::Input::
			Req::Api::ReadReq&		msg
			) noexcept;

		/** */
		void	start() noexcept override;

	private: // Oscl::Driver::Inventek::esWiFi::Symbiont::Kernel::Resp::Api
		/**	*/
		void	response( Oscl::Driver::Inventek::esWiFi::Symbiont::Kernel::Resp::Api::WriteResp& msg ) noexcept override;

		/**	*/
		void	response( Oscl::Driver::Inventek::esWiFi::Symbiont::Kernel::Resp::Api::ReadResp& msg ) noexcept override;

		/**	*/
		void	response( Oscl::Driver::Inventek::esWiFi::Symbiont::Kernel::Resp::Api::DisconnectResp& msg ) noexcept override;

		/**	*/
		void	response( Oscl::Driver::Inventek::esWiFi::Symbiont::Kernel::Resp::Api::ConnectResp& msg ) noexcept override;
	};

}
}
}
}
}

#endif
