/*
   Copyright (C) 2024 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_driver_inventek_eswifi_parth_
#define _oscl_driver_inventek_eswifi_parth_

#include <sys/types.h>
#include <functional>
#include "gpio.h"
#include "oscl/revision/observer/respcomp.h"
#include "oscl/booltype/control/api.h"
#include "oscl/spi/master/select/composer.h"
#include "oscl/spi/master16/api.h"
#include "oscl/spi/master16/respcomp.h"
#include "oscl/spi/master16/respmem.h"
#include "oscl/wifi/control/reqcomp.h"
#include "oscl/wifi/control/sync.h"
#include "oscl/driver/inventek/eswifi/symbiont/kernel/api.h"
#include "oscl/driver/inventek/eswifi/symbiont/kernel/reqapi.h"
#include "oscl/driver/inventek/eswifi/symbiont/host/sync.h"
#include "oscl/id/alloc/basic/config.h"
#include "oscl/mt/itc/delay/respcomp.h"
#include "oscl/revision/control/api.h"

/** */
namespace Oscl {
/** */
namespace Driver {
/** */
namespace Inventek {
/** */
namespace esWiFi {

/** Implements the base logic of the es-WiFi driver.
 */
class Part :
		private Oscl::Driver::Inventek::esWiFi::Symbiont::Kernel::Api,
		private Oscl::Driver::Inventek::esWiFi::Symbiont::Kernel::Req::Api,
		private Oscl::Driver::Inventek::esWiFi::Symbiont::Host::Req::Api,
		private Oscl::Mt::Itc::Delay::Resp::Api,
		public Oscl::Revision::Control::Api
		{
	private:
		/** */
		Oscl::Mt::Itc::PostMsgApi&					_papi;

		/** */
		Oscl::SPI::Master16::Api&					_spiApi;

		/** */
		Gpio::Api&									_gpioApi;

		/** */
		Oscl::BoolType::Control::Api&				_wifiUpControlApi;

		/** */
		Oscl::Mt::Itc::Delay::Req::Api::SAP&		_delaySAP;

	private:
		/** */
		Oscl::Driver::Inventek::esWiFi::Symbiont::
		Kernel::Req::Api::ConcreteSAP				_kernelSAP;

	private:
		/** */
		Oscl::Mt::Itc::Delay::Req::Api::DelayPayload	_delayPayload;

		/** */
		Oscl::Mt::Itc::Delay::Resp::Api::DelayResp		_delayResp;

	private:
		/** */
		Oscl::Driver::Inventek::esWiFi::Symbiont::Host::Sync	_hostSync;

		/** */
		Oscl::Queue<
			Oscl::Driver::Inventek::esWiFi::Symbiont::Api
			>	_symbionts;

		/** */
		static constexpr unsigned maxSockets = 4;

		/** */
		Oscl::Id::Alloc::Basic::Config< maxSockets >	_socketIdAllocator;

	private:
		/** */
		Oscl::SPI::Master::Select::Composer< Part >			_spiSelectComposer;

		/** */
		bool	_started;

	private:
		/** */
		Oscl::WiFi::Control::Req::Composer< Part >			_wifiReqComposer;

		/** */
		Oscl::WiFi::Control::Sync							_wifiControlSync;

	private:
		/** */
		static constexpr unsigned	maxRxBufferSize	= 256;

		/** */
		uint16_t	_rxDataBuffer[ maxRxBufferSize ];

		/** */
		Oscl::Queue< Oscl::Mt::Itc::SrvMsg >	_pendingReqQ;

		/** */
		Oscl::Mt::Itc::SrvMsg*		_currentReq;

		/** */
		static constexpr size_t	maxCmdSize = 256;

		/** */
		static constexpr size_t	maxCmdWords = (maxCmdSize + 1) / sizeof( uint16_t);

		/** */
		uint16_t	_cmdBuffer[ maxCmdWords ];

		/** */
		static constexpr size_t	maxRawRxCharBufferSize = 1400;

		/** */
		uint8_t		_rawRxBuffer[ maxRawRxCharBufferSize ];

		/** */
		unsigned	_rawRxBufferOffset;

		/** */
		bool		_rxOverflowPrinted;

	private:
		/** true when scanning is active between
			start and stop scan.
		 */
		bool	_scanning;

		/** true when the last station has been found
			suring a scan.
		 */
		bool	_scanComplete;

		/** */
		bool	_timerActive;

		/** */
		bool	_connected;

		/** */
		bool	_ignoreReady;

		/** */
		volatile bool	_dataReadyChanged;

	public:
		/** */
		Part(
			Oscl::Mt::Itc::PostMsgApi&					papi,
			Oscl::SPI::Master16::Api&					spiApi,
			Gpio::Api&									gpioApi,
			Oscl::BoolType::Control::Api&				wifiUpControlApi,
			Oscl::Mt::Itc::Delay::Req::Api::SAP&		delaySAP
			) noexcept;

		/** */
		void	start() noexcept;

		/** */
		Oscl::WiFi::Control::Api&	getWifiControlSyncApi() noexcept;

		/** */
		Oscl::WiFi::Control::Req::Api::SAP&	getWifiControlSAP() noexcept;

		/** */
		Oscl::Driver::Inventek::esWiFi::Symbiont::Host::Api&	getHostSyncApi() noexcept;

	private:
		/** */
		void	processNextReq() noexcept;

	private: // Oscl::Revision::Control::Api
		/** This operation executes in another thread.
			It is invoked when a rising edge is detected
			on the esWiFi data-ready signal.
		 */
		void    update() noexcept override;

	private:
		/** */
		void	assertChipSelect() noexcept;

		/** */
		void	negateChipSelect() noexcept;

		/** */
		void	startTimer() noexcept;

	private:
		/** */
		void	resetRxBuffer() noexcept;

		/** */
		void	parseRx( uint16_t rx ) noexcept;

		/**	Copy rx data to buffer.
			RETURN: nullptr for success,
			otherwise error string.
		 */
		const char*	copyOutRxData(
						std::function<void ( uint8_t* buffer, size_t len )> callback
						) noexcept;
		/** */
		const char*	copyOutAsyncRxData(
						std::function<void ( uint8_t* buffer, size_t len )> callback
						) noexcept;

		/** */
		const char*	sendOneDataPhase(
						std::function<void ( uint8_t* buffer, size_t len )> callback
						) noexcept;

		/** */
		const char*	sendOneDataPhaseMessageRead(
						std::function<void ( uint8_t* buffer, size_t len )> callback
						) noexcept;

		/** */
		const char*	sendDataPhaseMessageRead(
						std::function<void ( uint8_t* buffer, size_t len )> callback
						) noexcept;

	private:
		/*	Concatenates the verb and argument into
			the _cmdBuffer and performs appropriate
			fill padding.

			The verb must contain the trailing '=' if required.

			RETURN: -1 on error, or the number of
			words in the _cmdBuffer on success.
		 */
		ssize_t	buildCmd(
					const char*	verb,
					const char*	arguments
					) noexcept;

	private:
		/** RETURN: nullptr for success,
			otherwise error string.
		 */
		const char*	startScanWiFiNetworks() noexcept;

		/** RETURN: nullptr for success,
			otherwise error string.
		 */
		const char*	setWiFiSSID( const char* ssid ) noexcept;

		/** RETURN: nullptr for success,
			otherwise error string.
		 */
		const char*	setWiFiPassphrase( const char* passPhrase ) noexcept;

		/** RETURN: nullptr for success,
			otherwise error string.
		 */
		const char*	setWiFiSecurityTypeWPA2_AES() noexcept;

		/** RETURN: nullptr for success,
			otherwise error string.
		 */
		const char*	startWiFiJoin() noexcept;

		/** RETURN: nullptr for success,
			otherwise error string.
		 */
		const char*	startWiFiDisconnect() noexcept;

		/** RETURN: nullptr for success,
			otherwise error string.
		 */
		const char*	setTxDataPacketSize( const char* packetSize ) noexcept;

		/** */
		const char*	joinWiFiNetworkWPA2_AES(
						const char*	ssid,
						const char*	passPhrase
						) noexcept;

		/** */
		const char*	setSocketID( const Oscl::Id::Alloc::Node& socketID ) noexcept;

	private: // Oscl::Driver::Inventek::esWiFi::Symbiont::Kernel::Api
		/** */
		Oscl::Driver::Inventek::esWiFi::Symbiont::Kernel::Req::Api::SAP&	getSAP() noexcept override;

		/** */
		Oscl::Id::Alloc::Api&	getSocketIdAllocator() noexcept override;

	private:
		/** RETURN: nullptr for success,
			otherwise error string.
		 */
		const char*	setSocket( const char* socketID ) noexcept;

		/** RETURN: nullptr for success,
			otherwise error string.
		 */
		const char*	setRemoteHost( const char* ipAddress ) noexcept;

		/** RETURN: nullptr for success,
			otherwise error string.
		 */
		const char*	setRemotePort( const char* portNumber ) noexcept;

		/** RETURN: nullptr for success,
			otherwise error string.
		 */
		const char*	setRemotePort( unsigned portNumber ) noexcept;

		/** RETURN: nullptr for success,
			otherwise error string.
		 */
		const char*	setTxTimeout( const char* timeoutInMs ) noexcept;

		/** RETURN: nullptr for success,
			otherwise error string.
		 */
		const char*	setRxTimeout( const char* timeoutInMs ) noexcept;

		/** RETURN: nullptr for success,
			otherwise error string.
		 */
		const char*	setTransportProtocol( const char* protocol ) noexcept;

		/** RETURN: nullptr for success,
			otherwise error string.
		 */
		const char*	setSslVerificationLevel( const char* level ) noexcept;

		/** RETURN: nullptr for success,
			otherwise error string.
		 */
		const char*	startTcpClient( ) noexcept;

		/** RETURN: nullptr for success,
			otherwise error string.
		 */
		const char*	stopTcpClient( ) noexcept;

		/** RETURN: nullptr for success,
			otherwise error string.
		 */
		const char*	setRxDataPacketSize( const char* packetSize ) noexcept;

		/** RETURN: nullptr for success,
			otherwise error string.
		 */
		const char*	rxData(
						std::function<void ( const uint8_t* buffer, size_t len )> callback
 						) noexcept;

		/** RETURN: nullptr for success,
			otherwise error string.
		 */
		const char*	sendDataWithPacketSize(
						const uint8_t*	data,
						size_t			dataSize,
						size_t&			sentSize
						) noexcept;

		/** */
		const char*	dnsLookup(
						const char*	hostname,
						std::function<void ( uint8_t* buffer, size_t len )> callback
						) noexcept;

	private:
		/** */
		bool	sendSyncCommandPhase(
					const uint16_t*	txData,
					unsigned		n,
					bool			partial	= false
					) noexcept;

		/** */
		bool	transferCommandFragment(
					const uint16_t&	tx,
					uint16_t&		rx
					) noexcept;

		/** */
		bool	sendSyncDataPhase() noexcept;

		/** */
		void	sendDataPhase() noexcept;

	private:
		/** */
		void	assertSpiSelect() noexcept;

	private: // Oscl::WiFi::Control::Req::Composer _wifiReqComposer
		/** */
		void	getNextStationReq( Oscl::WiFi::Control::Req::Api::GetNextStationReq& msg ) noexcept;

		/** */
		void	startScanReq( Oscl::WiFi::Control::Req::Api::StartScanReq& msg ) noexcept;

		/** */
		void	stopScanReq( Oscl::WiFi::Control::Req::Api::StopScanReq& msg ) noexcept;

		/** */
		void	joinNetworkWPA2_AESReq( Oscl::WiFi::Control::Req::Api::JoinNetworkWPA2_AESReq& msg ) noexcept;

		/** */
		void	disconnectNetworkReq( Oscl::WiFi::Control::Req::Api::DisconnectNetworkReq& msg ) noexcept;

	private: // Oscl::Driver::Inventek::esWiFi::Symbiont::Host::Req::Api

		/** This operation is invoked within the server thread
			whenever a AttachReq is received.
		 */
		void	request(	Oscl::Driver::Inventek::esWiFi::Symbiont::Host::
							Req::Api::AttachReq& msg
							) noexcept override;

		/** This operation is invoked within the server thread
			whenever a DetachReq is received.
		 */
		void	request(	Oscl::Driver::Inventek::esWiFi::Symbiont::Host::
							Req::Api::DetachReq& msg
							) noexcept override;

	private: // Oscl::Mt::Itc::Delay::Resp::Api
		/** */
		void	response( Oscl::Mt::Itc::Delay::Resp::Api::DelayResp& msg) noexcept override;

		/** */
		void	response( Oscl::Mt::Itc::Delay::Resp::Api::CancelResp& msg) noexcept override;

	private: // Oscl::Revision::Observer::Resp::Composer
		/** */
		void	readyChangeResp( Oscl::Revision::Observer::Resp::Api::ChangeResp& msg ) noexcept;

	private: // Oscl::Driver::Inventek::esWiFi::Symbiont::Kernel::Req::Api
		/** This operation is invoked within the server thread
			whenever a WriteReq is received.
		 */
		void	request(	Oscl::Driver::Inventek::esWiFi::Symbiont::Kernel::
							Req::Api::WriteReq& msg
							) noexcept override;

		/** This operation is invoked within the server thread
			whenever a ReadReq is received.
		 */
		void	request(	Oscl::Driver::Inventek::esWiFi::Symbiont::Kernel::
							Req::Api::ReadReq& msg
							) noexcept override;

		/** This operation is invoked within the server thread
			whenever a DisconnectReq is received.
		 */
		void	request(	Oscl::Driver::Inventek::esWiFi::Symbiont::Kernel::
							Req::Api::DisconnectReq& msg
							) noexcept override;

		/** This operation is invoked within the server thread
			whenever a ConnectReq is received.
		 */
		void	request(	Oscl::Driver::Inventek::esWiFi::Symbiont::Kernel::
							Req::Api::ConnectReq& msg
							) noexcept override;
	};

}
}
}
}

#endif

