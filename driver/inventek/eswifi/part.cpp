/*
   Copyright (C) 2024 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <stdio.h>
#include <string.h>
#include <stddef.h>
#include <ctype.h>
#include "part.h"
#include "oscl/mt/thread.h"
#include "oscl/error/info.h"

using namespace Oscl::Driver::Inventek::esWiFi;

Part::Part(
		Oscl::Mt::Itc::PostMsgApi&					papi,
		Oscl::SPI::Master16::Api&					spiApi,
		Gpio::Api&									gpioApi,
		Oscl::BoolType::Control::Api&				wifiUpControlApi,
		Oscl::Mt::Itc::Delay::Req::Api::SAP&		delaySAP
		) noexcept:
	_papi( papi ),
	_spiApi( spiApi ),
	_gpioApi( gpioApi ),
	_wifiUpControlApi( wifiUpControlApi ),
	_delaySAP( delaySAP ),
	_kernelSAP(
		*this,
		papi
		),
	_delayPayload( 100 ),
	_delayResp(
		delaySAP.getReqApi(),
		*this,
		papi,
		_delayPayload
		),
	_hostSync(
		papi,
		*this
		),
	_spiSelectComposer(
		*this,
		&Part::assertSpiSelect
		),
	_started( false ),
	_wifiReqComposer(
		*this,
		&Part::disconnectNetworkReq,
		&Part::joinNetworkWPA2_AESReq,
		&Part::getNextStationReq,
		&Part::stopScanReq,
		&Part::startScanReq
		),
	_wifiControlSync(
		papi,
		_wifiReqComposer
		),
	_currentReq( nullptr ),
	_rawRxBufferOffset( 0 ),
	_rxOverflowPrinted( false ),
	_scanning( false ),
	_scanComplete( true ),
	_timerActive( false ),
	_connected( false ),
	_ignoreReady( false ),
	_dataReadyChanged( false )
	{
	}

Oscl::WiFi::Control::Api&	Part::getWifiControlSyncApi() noexcept {
	return _wifiControlSync;
	}

Oscl::WiFi::Control::Req::Api::SAP&	Part::getWifiControlSAP() noexcept {
	return _wifiControlSync.getSAP();
	}

Oscl::Driver::Inventek::esWiFi::Symbiont::Host::Api&	Part::getHostSyncApi() noexcept {
	return _hostSync;
	}

void	Part::startTimer() noexcept {

	if( _timerActive ) {
		return;
		}

	_timerActive	= true;

	_delayPayload._timer._milliseconds	= 100;
	_delaySAP.post( _delayResp.getSrvMsg() );
	}

void	Part::start() noexcept {
	_gpioApi.negateWakeup();
	_gpioApi.assertReset();
	_gpioApi.assertWakeup();

	Oscl::Mt::Thread::sleep(100);

	_gpioApi.selectBootMainFlashMemory();
	negateChipSelect();

	_gpioApi.negateReset();

	// Wait for the initial rising edge of
	// data-ready after reset is negated.

	while( _dataReadyChanged == false );
	while( _gpioApi.dataReadyIsLOW() );
	_dataReadyChanged	= false;

	sendDataPhase();

	_started	= true;

	processNextReq();

	startTimer();
	}

void	Part::assertChipSelect() noexcept {
	_gpioApi.assertChipSelect();
	}

void	Part::negateChipSelect() noexcept {
	_gpioApi.negateChipSelect();
	}

void	Part::resetRxBuffer() noexcept {
	_rawRxBufferOffset	= 0;
	_rxOverflowPrinted	= false;
	}

static constexpr char okTrailer[] = { "\r\nOK\r\n> " };
static constexpr size_t okTrailerLen = sizeof( okTrailer ) - 1;

static constexpr char okTrailer2[] = { "\r\n\r\nOK\r\n> " };
static constexpr size_t okTrailer2Len = sizeof( okTrailer2 ) - 1;

const char*	Part::copyOutRxData(
		std::function<void ( uint8_t* buffer, size_t len )> callback
		) noexcept {

	unsigned		offset	= 0;
	unsigned		len		= _rawRxBufferOffset;
	uint8_t*		p		= _rawRxBuffer;

	while( len && ( *p == 0x15 ) ) {
		++p;
		--len;
		}

	if( p[0] != '\r' || p[1] != '\n' ) {
		printf("%s: %s\n",__PRETTY_FUNCTION__, "bad header");
		Oscl::Error::Info::hexDump(_rawRxBuffer,8,0);
		return "bad header";
		}

	p		+= 2;
	offset	+= 2;
	len		-= 2;

	while( len && ( p[ len-1 ] == 0x15 ) ) {
		len--;
		}

	if( len < okTrailerLen ) {
		return "data too small";
		}

	len	-= okTrailerLen;

	if( !memcmp( &p[ len ] , okTrailer, okTrailerLen ) ) {
		}
	else if( !memcmp( &p[ len-2 ] , okTrailer2, okTrailer2Len ) ) {
		}
	else {
		return "command failed";
		}

	p[len]	= '\0';

	callback( p, len );

	return nullptr;
	}

static bool isLeadingChar( uint8_t c ) {
	if( c == 0x15 ) {
		return true;
		}
	if( c == '\n' ) {
		return true;
		}
	if( c == '\r' ) {
		return true;
		}

	return false;
	}

const char*	Part::copyOutAsyncRxData(
		std::function<void ( uint8_t* buffer, size_t len )> callback
		) noexcept {

	unsigned		offset	= 0;
	unsigned		len		= _rawRxBufferOffset;
	uint8_t*		p		= _rawRxBuffer;

	while( len && isLeadingChar( *p ) ) {
		p++;
		len--;
		}

	static constexpr char	somHeader[] = { "[SOMA]" };
	static constexpr size_t somHeaderLen = sizeof( somHeader ) - 1;

	static constexpr char	somHeader2[] = { "[SOMA]" };
	static constexpr size_t somHeader2Len = sizeof( somHeader2 ) - 1;

	static constexpr char okHeader[] = { "OK\r\n> " };
	static constexpr size_t okHeaderLen = sizeof( okHeader ) - 1;

	static constexpr char	eomHeader[] = { "[SOMA][EOMA]" };
	static constexpr size_t eomHeaderLen = sizeof( eomHeader ) - 1;

	static constexpr char	eomTrailer[] = { "[SOMA]\r" };
	static constexpr size_t eomTrailerLen = sizeof( eomTrailer ) - 1;

	if( !memcmp( p, eomHeader, eomHeaderLen ) ) {
		p		+= eomHeaderLen;
		offset	+= eomHeaderLen;
		len		-= eomHeaderLen;
		return eomHeader;
		}
	else if( !memcmp( p, somHeader2, somHeader2Len ) ) {
		p		+= somHeader2Len;
		offset	+= somHeader2Len;
		len		-= somHeader2Len;
		}
	else if( !memcmp( p, somHeader, somHeaderLen ) ) {
		p		+= somHeaderLen;
		offset	+= somHeaderLen;
		len		-= somHeaderLen;
		}
	else if( !memcmp( p, okHeader, okHeaderLen ) ) {
		return "OK";
		}
	else {
		return "bad SOMA header";
		}

	while( len && ( p[ len-1 ] == 0x15 ) ) {
		len--;
		}

	if( len < eomTrailerLen ) {
		return "missing EOMA trailer";
		}

	len	-= eomTrailerLen;

	p[len] = '\0';

	callback( p, len );

	return nullptr;
	}

void	Part::parseRx( uint16_t rx ) noexcept {

	const uint8_t*	p = (const uint8_t*)&rx;

	for( unsigned i=0; i < 2; ++i ) {

		if( _rawRxBufferOffset >= ( sizeof(_rawRxBuffer)-1 ) ) {
			if( _rxOverflowPrinted ) {
				return;
				}
			_rxOverflowPrinted	= true;
			printf(
				"%s: overflow\n",
				__PRETTY_FUNCTION__
				);
			return;
			}

		_rawRxBuffer[ _rawRxBufferOffset ] = p[i];
		++_rawRxBufferOffset;
		}
	}

bool	Part::sendSyncDataPhase() noexcept {

	constexpr size_t	nOctets = 2;

	static const union {
		uint8_t		octets[ nOctets ];
		uint16_t	words[ nOctets/sizeof(uint16_t) ];
		} txBuffer = { .octets = {'\n','\n'} };

	static uint16_t	rxDataBuffer[ sizeof( txBuffer )/sizeof(uint16_t) ];

	constexpr size_t
	nRxDataBufferElements	= sizeof( rxDataBuffer )/sizeof(rxDataBuffer[0]);

	memset(
		rxDataBuffer,
		0x55,
		nRxDataBufferElements
		);

	bool
	failed	= _spiApi.transfer(
				_spiSelectComposer,
				txBuffer.words,
				rxDataBuffer,
				nRxDataBufferElements,
				2000000,	// frequency
				false,	// lsbFirst
				false,	// cpol
				false	// cpha
				);

	parseRx( rxDataBuffer[0] );

	return failed;
	}

const char*	Part::sendOneDataPhase(
			std::function<void ( uint8_t* buffer, size_t len )> callback
			) noexcept {

	sendDataPhase();

	const char*
	errMsg	= copyOutRxData(
				callback
				);

	return errMsg;
	}

void	Part::sendDataPhase() noexcept {

	resetRxBuffer();

	assertChipSelect();

	do {
		sendSyncDataPhase();
		}
	while( !_gpioApi.dataReadyIsLOW() );

	/*	Data-Ready will remain LOW until the chip-select
		is negated.
	 */

	negateChipSelect();

	/*	Data-Ready will transition to HIGH sometime
		after this point.
	 */

	while( _dataReadyChanged == false );
	while( _gpioApi.dataReadyIsLOW() );
	_dataReadyChanged	= false;

	}

bool	Part::sendSyncCommandPhase(
			const uint16_t*	txData,
			unsigned		n,
			bool			partial
			) noexcept {

	resetRxBuffer();

	bool	failed	= false;

	assertChipSelect();

	for( unsigned i=0; i < n; ++i ) {
		uint16_t	rxData;

		failed	= transferCommandFragment( txData[i], rxData );

		if( failed ) {
			break;
			}
		}

	if( partial ) {
		return false;
		}

	/*	Data-Ready remains HIGH at this point
		until the chip-select is negated, which
		tells the esWiFi that the command has been
		sent.
	 */
	negateChipSelect();

	/*	After the chip-select is negated, the esWiFi
		will drive data-ready LOW and subsequently
		raise it to HIGH when it is ready to begin
		the data-phase.
	 */

	while( _dataReadyChanged == false );
	while( _gpioApi.dataReadyIsLOW() );

	_dataReadyChanged	= false;


	return failed;
	}

bool	Part::transferCommandFragment(
			const uint16_t&	tx,
			uint16_t&		rx
			) noexcept {
	bool
	failed	= _spiApi.transfer(
				_spiSelectComposer,
				&tx,
				&rx,
				1,
				2000000,	// frequency
				false,	// lsbFirst
				false,	// cpol
				false	// cpha
				);

	parseRx( rx );

	return failed;
	}


void	Part::assertSpiSelect() noexcept {
	_gpioApi.assertChipSelect();
	}

void	Part::processNextReq() noexcept {

	if( !_started ) {
		return;
		}

	Oscl::Mt::Itc::SrvMsg* req;
	while( ( req = _pendingReqQ.get() ) ) {
		req->process();
		}
	}

void    Part::update() noexcept {
	/** This operation executes in another thread.
		It is invoked when a rising edge is detected
		on the esWiFi data-ready signal.
	 */
	_dataReadyChanged	= true;
	}

/*	Concatenates the verb and argument into
	the _cmdBuffer and performs appropriate
	fill padding.

	The verb must contain the trailing '=' if required.

	RETURN: -1 on error, or the number of
	words in the _cmdBuffer on success.
 */
ssize_t	Part::buildCmd(
			const char*	verb,
			const char*	arguments
			) noexcept {

	constexpr size_t	maxVerbAndArgLen = sizeof( _cmdBuffer ) -1;

	size_t
	verblen	= strnlen(
				verb,
				sizeof( _cmdBuffer ) + 1
				);

	if( verblen >= sizeof( _cmdBuffer ) ) {
		/* verb is too long */
		return -1;
		}

	size_t
	arglen	= strnlen(
				arguments,	// const char s[.maxlen],
				sizeof( _cmdBuffer ) + 1
				);

	if( arglen >= sizeof( _cmdBuffer ) ) {
		/* argument is too long */
		return -1;
		}

	size_t
	cmdverblen	= ( verblen + arglen );

	if( cmdverblen > maxVerbAndArgLen ) {
		return -1;
		}

	char*	p = (char*)_cmdBuffer;

	memcpy(
		p,
		verb,
		verblen
		);

	p	= &p[ verblen ];

	memcpy(
		p,
		arguments,
		arglen
		);

	p	= &p[ arglen ];

	if( cmdverblen  % 2 ) {
		/* That's odd */
		p[0] = '\r';
		}
	else {
		/* That's even */
		p[0] = '\r';
		p[1] = '\n';
		}

	return (cmdverblen + 1 + 1) / 2;
	}

const char*	Part::setWiFiSSID( const char* ssid ) noexcept {

	resetRxBuffer();

	ssize_t
	nWords	= buildCmd(
				"C1=",		// const char*	verb,
				ssid	// const char*	arguments
				);

	if( nWords < 0 ) {
		return "SSID command too long";
		}

	sendSyncCommandPhase(
			_cmdBuffer,	//	const uint16*	txDataBuffer,
			nWords		// unsigned	nDataWordsToTransfer
			);

	const char*
	errMsg	= sendOneDataPhase(
				[&] ( uint8_t* buffer, size_t len ) {
					// Ignore
					}
				);

	return errMsg;
	}

const char*	Part::setWiFiPassphrase( const char* passPhrase ) noexcept {

	resetRxBuffer();

	ssize_t
	nWords	= buildCmd(
				"C2=",		// const char*	verb,
				passPhrase	// const char*	arguments
				);

	if( nWords < 0 ) {
		return "Passphrase command too long!";
		}

	sendSyncCommandPhase(
			_cmdBuffer,	//	const uint16*	txDataBuffer,
			nWords		// unsigned	nDataWordsToTransfer
			);

	const char*
	errMsg	= sendOneDataPhase(
				[&] ( uint8_t* buffer, size_t len ) {
					// Ignore
					}
				);

	return errMsg;
	}

const char*	Part::setWiFiSecurityTypeWPA2_AES() noexcept {

	ssize_t
	nWords	= buildCmd(
				"C3=",		// const char*	verb,
				"3"			// const char*	arguments
				);

	if( nWords < 0 ) {
		return "SecurityType command too long!";
		}

	sendSyncCommandPhase(
			_cmdBuffer,	//	const uint16*	txDataBuffer,
			nWords		// unsigned	nDataWordsToTransfer
			);

	const char*
	errMsg	= sendOneDataPhase(
				[&] ( uint8_t* buffer, size_t len ) {
					// Ignore
					}
				);

	return errMsg;
	}

const char*	Part::startWiFiJoin() noexcept {

	ssize_t
	nWords	= buildCmd(
				"C0",		// const char*	verb,
				""			// const char*	arguments
				);

	if( nWords < 0 ) {
		return "Join command too long!";
		}

	sendSyncCommandPhase(
			_cmdBuffer,	//	const uint16*	txDataBuffer,
			nWords		// unsigned	nDataWordsToTransfer
			);

	const char*
	errMsg	= sendOneDataPhase(
				[&] ( uint8_t* buffer, size_t len ) {
					// Ignore
					}
				);

	if( !errMsg ) {
		_connected	= true;
		_wifiUpControlApi.update( _connected );
		}

	return errMsg;
	}

const char*	Part::startWiFiDisconnect() noexcept {

	_connected	= false;
	_wifiUpControlApi.update( _connected );

	ssize_t
	nWords	= buildCmd(
				"CD",		// const char*	verb,
				""			// const char*	arguments
				);

	if( nWords < 0 ) {
		return "Join command too long!";
		}

	sendSyncCommandPhase(
			_cmdBuffer,	//	const uint16*	txDataBuffer,
			nWords		// unsigned	nDataWordsToTransfer
			);

	const char*
	errMsg	= sendOneDataPhase(
				[&] ( uint8_t* buffer, size_t len ) {
					// Ignore
					}
				);

	return errMsg;
	}

const char*	Part::setSocket( const char* socketID ) noexcept {

	ssize_t
	nWords	= buildCmd(
				"P0=",		// const char*	verb,
				socketID	// const char*	arguments
				);

	if( nWords < 0 ) {
		return "command too long!";
		}

	sendSyncCommandPhase(
			_cmdBuffer,	//	const uint16*	txDataBuffer,
			nWords		// unsigned	nDataWordsToTransfer
			);

	const char*
	errMsg	= sendOneDataPhase(
				[&] ( uint8_t* buffer, size_t len ) {
					// Ignore
					}
				);

	return errMsg;
	}

const char*	Part::setRemoteHost( const char* ipAddress ) noexcept {

	ssize_t
	nWords	= buildCmd(
				"P3=",		// const char*	verb,
				ipAddress	// const char*	arguments
				);

	if( nWords < 0 ) {
		return "command too long!";
		}

	sendSyncCommandPhase(
			_cmdBuffer,	//	const uint16*	txDataBuffer,
			nWords		// unsigned	nDataWordsToTransfer
			);

	const char*
	errMsg	= sendOneDataPhase(
				[&] ( uint8_t* buffer, size_t len ) {
					// Ignore
					}
				);

	return errMsg;
	}

const char*	Part::setRemotePort( const char* portNumber ) noexcept {

	ssize_t
	nWords	= buildCmd(
				"P4=",		// const char*	verb,
				portNumber	// const char*	arguments
				);

	if( nWords < 0 ) {
		return "command too long!";
		}

	sendSyncCommandPhase(
			_cmdBuffer,	//	const uint16*	txDataBuffer,
			nWords		// unsigned	nDataWordsToTransfer
			);

	const char*
	errMsg	= sendOneDataPhase(
				[&] ( uint8_t* buffer, size_t len ) {
					// Ignore
					}
				);

	return errMsg;
	}

const char*	Part::setRemotePort( unsigned portNumber ) noexcept {
	char	s[32];

	int
	n	= snprintf(
			s,
			sizeof( s ),
			"%u",
			portNumber
			);

	if( n < 0 ) {
		return "snprintf error";
		}

	unsigned
	writeLen	= n;

	if( writeLen  >= sizeof( s ) ) {
		return "too long";
		}

	return setRemotePort( s );
	}

const char*	Part::setTxDataPacketSize( const char* packetSize ) noexcept {
	// FIXME: Implement

	return nullptr;
	}

const char*	Part::setTxTimeout( const char* timeoutInMs ) noexcept {

	ssize_t
	nWords	= buildCmd(
				"S2=",		// const char*	verb,
				timeoutInMs	// const char*	arguments
				);

	if( nWords < 0 ) {
		return "command too long!";
		}

	sendSyncCommandPhase(
			_cmdBuffer,	//	const uint16*	txDataBuffer,
			nWords		// unsigned	nDataWordsToTransfer
			);

	const char*
	errMsg	= sendOneDataPhase(
				[&] ( uint8_t* buffer, size_t len ) {
					// Ignore
					}
				);

	return errMsg;
	}

const char*	Part::setRxTimeout( const char* timeoutInMs ) noexcept {

	ssize_t
	nWords	= buildCmd(
				"R2=",		// const char*	verb,
				timeoutInMs	// const char*	arguments
				);

	if( nWords < 0 ) {
		return "command too long!";
		}

	sendSyncCommandPhase(
			_cmdBuffer,	//	const uint16*	txDataBuffer,
			nWords		// unsigned	nDataWordsToTransfer
			);

	const char*
	errMsg	= sendOneDataPhase(
				[&] ( uint8_t* buffer, size_t len ) {
					// Ignore
					}
				);

	return errMsg;
	}

const char*	Part::setTransportProtocol( const char* protocol ) noexcept {

	ssize_t
	nWords	= buildCmd(
				"P1=",		// const char*	verb,
				protocol	// const char*	arguments
				);

	if( nWords < 0 ) {
		return "command too long!";
		}

	sendSyncCommandPhase(
			_cmdBuffer,	//	const uint16*	txDataBuffer,
			nWords		// unsigned	nDataWordsToTransfer
			);

	const char*
	errMsg	= sendOneDataPhase(
				[&] ( uint8_t* buffer, size_t len ) {
					// Ignore
					}
				);

	return errMsg;
	}

const char*	Part::setSslVerificationLevel( const char* level ) noexcept {

	ssize_t
	nWords	= buildCmd(
				"P9=",		// const char*	verb,
				level	// const char*	arguments
				);

	if( nWords < 0 ) {
		return "command too long!";
		}

	sendSyncCommandPhase(
			_cmdBuffer,	//	const uint16*	txDataBuffer,
			nWords		// unsigned	nDataWordsToTransfer
			);

	const char*
	errMsg	= sendOneDataPhase(
				[&] ( uint8_t* buffer, size_t len ) {
					// Ignore
					}
				);

	return errMsg;
	}

const char*	Part::startTcpClient( ) noexcept {

	ssize_t
	nWords	= buildCmd(
				"P6=",	// const char*	verb,
				"1"		// const char*	arguments
				);

	if( nWords < 0 ) {
		return "command too long!";
		}

	sendSyncCommandPhase(
			_cmdBuffer,	//	const uint16*	txDataBuffer,
			nWords		// unsigned	nDataWordsToTransfer
			);

	/*	On success, the startTcpClient() operation actually returns a
		data-phase string of the form:
		"[TCP  RC] Connecting to 192.168.254.79"
	 */
	const char*
	errMsg	= sendOneDataPhase(
				[&] ( uint8_t* buffer, size_t len ) {
					// Ignore
					}
				);

	return errMsg;
	}

const char*	Part::stopTcpClient( ) noexcept {

	ssize_t
	nWords	= buildCmd(
				"P6=",	// const char*	verb,
				"0"		// const char*	arguments
				);

	if( nWords < 0 ) {
		return "command too long!";
		}

	sendSyncCommandPhase(
			_cmdBuffer,	//	const uint16*	txDataBuffer,
			nWords		// unsigned	nDataWordsToTransfer
			);

	const char*
	errMsg	= sendOneDataPhase(
				[&] ( uint8_t* buffer, size_t len ) {
					// Ignore
					}
				);

	return errMsg;
	}

const char*	Part::sendDataWithPacketSize(
				const uint8_t*	data,
				size_t			dataSize,
				size_t&			sentSize
				) noexcept {

	char
	packetSizeBuffer[10];

	unsigned
	dsize	= dataSize;

	size_t
	n	= snprintf(
			packetSizeBuffer,
			sizeof(packetSizeBuffer),
			"%4.4u",
			dsize
			);

	if( n >= sizeof( packetSizeBuffer ) ) {
		return "packet size buffer overflow!";
		}

	ssize_t
	nWords	= buildCmd(
				"S3=",				// const char*	verb,
				packetSizeBuffer	// const char*	arguments
				);

	if( nWords < 0 ) {
		return "command too long!";
		}

	sendSyncCommandPhase(
			_cmdBuffer,	//	const uint16*	txDataBuffer,
			nWords,		// unsigned	nDataWordsToTransfer
			true		// partial
			);

	size_t
	remaining	= dataSize;

	while( remaining ) {
		size_t
		size = ( remaining < sizeof( _cmdBuffer ) ) ?
				remaining:
				sizeof( _cmdBuffer )
				;
		memcpy(
			_cmdBuffer,
			data,
			size
			);
		data	= &data[size];

		remaining	-= size;

		if( size < sizeof( _cmdBuffer ) ) {
			if( size & 2 ) {
				// That's odd
				_cmdBuffer[ size ] = '\n';
				}
			}

		nWords	= (size + 1 + 1) / 2;

		sendSyncCommandPhase(
			_cmdBuffer,	//	const uint16*	txDataBuffer,
			nWords,		// unsigned	nDataWordsToTransfer
			remaining?true:false		// partial
			);
		}

	const char*
	errMsg	= sendOneDataPhase(
				[&] ( uint8_t* buffer, size_t len ) {
					sentSize	= strtoul( (const char*) buffer, nullptr, 0 );
					}
				);

	if( errMsg ) {
		return errMsg;
		}

	return errMsg;
	}

const char*	Part::setRxDataPacketSize( const char* packetSize ) noexcept {

	ssize_t
	nWords	= buildCmd(
				"R1=",		// const char*	verb,
				packetSize	// const char*	arguments
				);

	if( nWords < 0 ) {
		return "command too long!";
		}

	sendSyncCommandPhase(
			_cmdBuffer,	//	const uint16*	txDataBuffer,
			nWords		// unsigned	nDataWordsToTransfer
			);

	const char*
	errMsg	= sendOneDataPhase(
				[&] ( uint8_t* buffer, size_t len ) {
					}
				);

	return errMsg;
	}

const char*	Part::dnsLookup(
				const char*	hostname,
				std::function<void ( uint8_t* buffer, size_t len )> callback
				) noexcept {

	/*
		Note: The IP address will automatically be stored as the remote host IP address.
		(Note, reference the P3 command.)
	 */

	ssize_t
	nWords	= buildCmd(
				"D0=",		// const char*	verb,
				hostname	// const char*	arguments
				);

	if( nWords < 0 ) {
		return "command too long!";
		}

	sendSyncCommandPhase(
			_cmdBuffer,	//	const uint16*	txDataBuffer,
			nWords		// unsigned	nDataWordsToTransfer
			);

	const char*
	errMsg	= sendOneDataPhase(
				[&] ( uint8_t* buffer, size_t len ) {
					callback( buffer, len );
					}
				);

	return errMsg;
	}

const char*	Part::rxData(
				std::function<void ( const uint8_t* buffer, size_t len )> callback
				) noexcept {

	ssize_t
	nWords	= buildCmd(
				"R0",		// const char*	verb,
				""			// const char*	arguments
				);

	if( nWords < 0 ) {
		return "command too long!";
		}

	sendSyncCommandPhase(
			_cmdBuffer,	//	const uint16*	txDataBuffer,
			nWords		// unsigned	nDataWordsToTransfer
			);

	const char*
	errMsg	= sendOneDataPhase(
				[&] ( uint8_t* buffer, size_t len ) {
					callback( buffer, len );
					}
				);

	return errMsg;
	}

const char*	Part::sendOneDataPhaseMessageRead(
			std::function<void ( uint8_t* buffer, size_t len )> callback
			) noexcept {

	sendDataPhase();

	const char*
	errMsg	= copyOutAsyncRxData(
				callback
				);

	if( errMsg ) {
		if( strcmp( errMsg, "OK") == 0 ) {
			return nullptr;
			}
		return errMsg;
		}

	ssize_t
	nWords	= buildCmd(
				"MR",	// const char*	verb,
				""		// const char*	arguments
				);

	if( nWords < 0 ) {
		return "MR command too long";
		}

	sendSyncCommandPhase(
			_cmdBuffer,	//	const uint16*	txDataBuffer,
			nWords		// unsigned	nDataWordsToTransfer
			);

	return "OK";
	}

const char*	Part::sendDataPhaseMessageRead(
			std::function<void ( uint8_t* buffer, size_t len )> callback
			) noexcept {
	do {

		const char*
		errMsg	= sendOneDataPhaseMessageRead( callback );

		if( !errMsg ) {
			return nullptr;
			}

		} while( true );
	}

Oscl::Driver::Inventek::esWiFi::Symbiont::Kernel::Req::Api::SAP&	Part::getSAP() noexcept {
	return _kernelSAP;
	}

Oscl::Id::Alloc::Api&	Part::getSocketIdAllocator() noexcept {
	return _socketIdAllocator;
	}

const char*	Part::startScanWiFiNetworks() noexcept {

	resetRxBuffer();

	ssize_t
	nWords	= buildCmd(
				"F0=2",	// const char*	verb,
				""		// const char*	arguments
				);

	if( nWords < 0 ) {
		return "find networks command too long";
		}

	sendSyncCommandPhase(
			_cmdBuffer,	//	const uint16*	txDataBuffer,
			nWords		// unsigned	nDataWordsToTransfer
			);

	return nullptr;
	}

const char*	Part::joinWiFiNetworkWPA2_AES(
			const char*	ssid,
			const char*	passPhrase
			) noexcept {

	const char*
	errMsg	= setWiFiSSID( ssid );

	if( errMsg ) {
		printf(
			"%s: SSID: \"%s\", FAILED: %s\n",
			__PRETTY_FUNCTION__,
			ssid,
			errMsg
			);
		return errMsg;
		}

	errMsg	= setWiFiPassphrase( passPhrase );

	if( errMsg ) {
		printf(
			"%s: PassPhrase: \"%s\", FAILED: %s\n",
			__PRETTY_FUNCTION__,
			passPhrase,
			errMsg
			);
		return errMsg;
		}

	errMsg	= setWiFiSecurityTypeWPA2_AES();

	if( errMsg ) {
		printf(
			"%s: WPA2_AES, FAILED: %s\n",
			__PRETTY_FUNCTION__,
			errMsg
			);
		return errMsg;
		}

	errMsg	= startWiFiJoin();

	if( errMsg ) {
		printf(
			"%s: Join, FAILED: %s\n",
			__PRETTY_FUNCTION__,
			errMsg
			);
		return errMsg;
		}

	return errMsg;
	}

void	Part::startScanReq( Oscl::WiFi::Control::Req::Api::StartScanReq& msg ) noexcept{

	if( !_started ) {
		_pendingReqQ.put( &msg );
		return;
		}

	_scanning		= true;
	_scanComplete	= false;

	auto&
	payload	= msg.getPayload();

	const char*
	errMsg	= startScanWiFiNetworks();

	payload._failed	= (errMsg)?true:false;

	msg.returnToSender();
	}

void	Part::stopScanReq( Oscl::WiFi::Control::Req::Api::StopScanReq& msg ) noexcept{

	if( !_started ) {
		_pendingReqQ.put( &msg );
		return;
		}

	auto&
	payload	= msg.getPayload();

	if( !_scanning ) {
		payload._failed	= true;
		msg.returnToSender();
		return;
		}

	_scanning		= false;

	if( _scanComplete ) {
		payload._failed	= false;
		msg.returnToSender();
		return;
		}

	auto
	errMsg	= sendDataPhaseMessageRead(
		[&] ( uint8_t* buffer, size_t len ) {
			// ignore everything
			return;
			}
		);

	payload._failed	= (errMsg)?true:false;

	msg.returnToSender();
	}

volatile unsigned long	bssidThing;

/* RETURN: true for failure */
static bool	parseBSSID(
			uint8_t*	bssid,
			char*	string
			) noexcept {
	char *saveptr;

	char*
	t	= strtok_r( string, ":", &saveptr );

	unsigned	i	= 0;

	for( i=0; t && (i < 6) ; ++i ) {
		bssidThing	= strtoul( t, nullptr, 16);
		bssid[i]	= bssidThing;
		t	= strtok_r( nullptr, ":", &saveptr );
		}

	return ( ( i < 6 ) || t )?true:false;
	}

static void	parseNetwork(
			Oscl::WiFi::Control::Station&	station,
			uint8_t*						buffer,
			size_t							len
			) noexcept {

	char *saveptr;
	char*	p	= (char*)buffer;

	char*
	t	= strtok_r( p, ",", &saveptr );

	if( !t ) {
		return;
		}

	// SSID, BSSID, RSSI, Data Rate, Network Type, Security, Radio Band, Channel.
	for(unsigned i=0; ;++i) {
		t	= strtok_r( nullptr, ",", &saveptr );
		if(!t) {
			break;
			}
		switch( i ) {
			case 0: {
				// ssid
				size_t
				n	= strlen( t );
				if( n > 1 ) {
					n = 1;
					}
				strncpy( station.ssid, &t[n], sizeof( station.ssid ) - 1);
				station.ssid[ sizeof( station.ssid ) -1 ] = '\0';
				n	= strlen( station.ssid );
				if( n > 0 ) {
					--n;
					if( station.ssid[n] == '\"' ) {
						station.ssid[n] = '\0';
						}
					}
				}
				break;
			case 1:
				// bssid
				parseBSSID( station.macAddress, t );
				break;
			case 2:
				// rssi
				station.rssi	= atoi( t );
				break;
			case 3:
				// data rate
				/*
					FIXME: What are the units? MHz? BitsPerSecond? Bytes-per-second?
					Empirically:
						- a 5GHz radio band is 600.0 units.
						- a 2.4GHz radio band is 72.2 or 216.7 units.
					The WiFi beacon has a MaxBitRate field with units Mbps.
				 */
				station.maxDataRate	= atof( t );
				break;
			case 4:
				// network type
				if( !strcmp( "Infrastructure", t ) ) {
					station.networkType	= Oscl::WiFi::Control::Station::NetworkType::Infrastructure;
					}
				else {
					station.networkType	= Oscl::WiFi::Control::Station::NetworkType::unknown;
					}
				break;
			case 5:
				// security
				if( !strcmp( "WPA2 AES", t ) ) {
					station.securityType	= Oscl::WiFi::Control::Station::SecurityType::wpa2AES;
					}
				else if( !strcmp( "Open", t ) ) {
					station.securityType	= Oscl::WiFi::Control::Station::SecurityType::open;
					}
				else if( !strcmp( "WPA", t ) ) {
					station.securityType	= Oscl::WiFi::Control::Station::SecurityType::wpa;
					}
        		// wpa2Mixed string?
				else {
					station.securityType	= Oscl::WiFi::Control::Station::SecurityType::unknown;
					}
				break;
			case 6:
				// radio band
				if( !strcmp( "2.4GHz", t ) ) {
					station.radioBand	= Oscl::WiFi::Control::Station::RadioBand::_2_4GHz;
					}
				else if( !strcmp( "5GHz", t ) ) {
					station.radioBand	= Oscl::WiFi::Control::Station::RadioBand::_5GHz;
					}
				else {
					station.radioBand	= Oscl::WiFi::Control::Station::RadioBand::unknown;
					}
				break;
			case 7:
				// channel
				station.channel	= strtoul( t, nullptr, 0 );
				break;
			default:
				break;
			}
		}
	}

void	Part::getNextStationReq( Oscl::WiFi::Control::Req::Api::GetNextStationReq& msg ) noexcept{

	if( !_started ) {
		_pendingReqQ.put( &msg );
		return;
		}

	auto&
	payload	= msg.getPayload();

	if( !_scanning || _scanComplete ) {
		payload._failed	= true;
		msg.returnToSender();
		return;
		}

	bool	foundStation	= false;

	const char*
	errMsg	= sendOneDataPhaseMessageRead(
		[&] ( uint8_t* buffer, size_t len ) {
			parseNetwork( payload._station, buffer, len );
			foundStation	= true;
			}
		);

	if( !foundStation ) {
		_scanComplete	= true;
		}
	else if( errMsg ) {
		if( strcmp( errMsg, "OK" ) ) {
			_scanComplete	= true;
			}
		}

	payload._failed	= foundStation?false:true;

	msg.returnToSender();
	}

void	Part::disconnectNetworkReq( Oscl::WiFi::Control::Req::Api::DisconnectNetworkReq& msg ) noexcept{

	if( !_started ) {
		_pendingReqQ.put( &msg );
		return;
		}

	auto&
	payload	= msg.getPayload();

	const char*
	errMsg	= startWiFiDisconnect();

	if( errMsg ) {
		printf(
			"%s: Disconnect, FAILED: %s\n",
			__PRETTY_FUNCTION__,
			errMsg
			);
		}

	payload._failed	= (errMsg)?true:false;

	msg.returnToSender();
	}

void	Part::joinNetworkWPA2_AESReq( Oscl::WiFi::Control::Req::Api::JoinNetworkWPA2_AESReq& msg ) noexcept{

	if( !_started ) {
		_pendingReqQ.put( &msg );
		return;
		}

	auto&
	payload	= msg.getPayload();

	const char*
	errMsg	= joinWiFiNetworkWPA2_AES(
				payload._ssid,
				payload._passPhrase
				);

	payload._failed	= (errMsg)?true:false;

	msg.returnToSender();
	}

void	Part::request(	Oscl::Driver::Inventek::esWiFi::Symbiont::Host::
							Req::Api::AttachReq& msg
							) noexcept {
	auto&
	payload	= msg.getPayload();

	_symbionts.put( &payload._symbiont );

	payload._symbiont.start( *this );

	msg.returnToSender();
	}

void	Part::request(	Oscl::Driver::Inventek::esWiFi::Symbiont::Host::
						Req::Api::DetachReq& msg
						) noexcept {

	auto&
	payload	= msg.getPayload();

	if( _symbionts.remove( &payload._symbiont ) ) {
		payload._symbiont.stop();
		}

	msg.returnToSender();
	}

void	Part::response( Oscl::Mt::Itc::Delay::Resp::Api::DelayResp& msg) noexcept {

	_timerActive	= false;

	for(
		Oscl::Driver::Inventek::esWiFi::Symbiont::Api* symbiont = _symbionts.first();
		symbiont;
		symbiont = _symbionts.next( symbiont )
		) {
			if( !_scanning ) {
				symbiont->poll();
				}
		}

	startTimer();
	}

void	Part::response( Oscl::Mt::Itc::Delay::Resp::Api::CancelResp& msg) noexcept {
	for(;;);	// Not implemented
	}

const char*	Part::setSocketID( const Oscl::Id::Alloc::Node& socketID ) noexcept {

	char	s[32];

	auto
	n	= snprintf(
			s,
			sizeof( s ),
			"%u",
			socketID._id
			);

	if( n < 0 ) {
		return "snprintf failed";
		}

	unsigned
	writeLen	= n;

	if( writeLen  >= sizeof( s ) ) {
		return "string too large";
		}

	ssize_t
	nWords	= buildCmd(
				"P0=",	// const char*	verb,
				s		// const char*	arguments
				);

	if( nWords < 0 ) {
		return "command too long!";
		}

	sendSyncCommandPhase(
		_cmdBuffer,	//	const uint16*	txDataBuffer,
		nWords		// unsigned	nDataWordsToTransfer
		);

	const char*
	errMsg	= sendOneDataPhase(
				[&] ( uint8_t* buffer, size_t len ) {
					// Ignore
					}
				);

	return errMsg;
	}

void	Part::request(	Oscl::Driver::Inventek::esWiFi::Symbiont::Kernel::
						Req::Api::WriteReq& msg
						) noexcept {

	auto&
	payload	= msg.getPayload();

	if( !_started || !_connected ) {
		payload._nWritten	= 0;
		msg.returnToSender();
		return;
		}

	payload._nWritten	= 0;

	const char*
	errMsg	= setSocketID( payload._socketID );

	if( errMsg ) {
		msg.returnToSender();
		return;
		}

	char	s[32];

	auto
	n	= snprintf(
			s,
			sizeof( s ),
			"%u",
			payload._buffer.length()
			);

	if( n < 0 ) {
		msg.returnToSender();
		return;
		}

	unsigned
	writeLen	= n;

	if( writeLen  >= sizeof( s ) ) {
		msg.returnToSender();
		return;
		}

	size_t	sentSize	= 0;

	errMsg	= sendDataWithPacketSize(
				(const uint8_t*)payload._buffer.getBuffer(),
				payload._buffer.length(),
				sentSize
				);

	if( errMsg ) {
		printf(
			"%s: send data: FAILED: %s\n",
			__PRETTY_FUNCTION__,
			errMsg
			);
		}

	payload._nWritten	+= sentSize;

	msg.returnToSender();
	}

void	Part::request(	Oscl::Driver::Inventek::esWiFi::Symbiont::Kernel::
						Req::Api::ReadReq& msg
						) noexcept {

	auto&
	payload	= msg.getPayload();

	if( !_started || !_connected ) {
		payload._failed	= true;
		msg.returnToSender();
		return;
		}

	if( _scanning ) {
		payload._failed	= false;
		msg.returnToSender();
		return;
		}

	const char*
	errMsg	= setSocketID( payload._socketID );

	if( errMsg ) {
		payload._failed	= true;
		msg.returnToSender();
		return;
		}

	char	s[32];

	auto
	n	= snprintf(
			s,
			sizeof( s ),
			"%u",
			payload._buffer.bufferSize()
			);

	if( n < 0 ) {
		payload._failed	= true;
		msg.returnToSender();
		return;
		}

	unsigned
	writeLen	= n;

	if( writeLen  >= sizeof( s ) ) {
		payload._failed	= true;
		msg.returnToSender();
		return;
		}

	errMsg	= setRxDataPacketSize( s );

	if( errMsg ) {
		payload._failed	= true;
		msg.returnToSender();
		return;
		}

	errMsg	= setRxTimeout( "10" );

	if( errMsg ) {
		payload._failed	= true;
		msg.returnToSender();
		return;
		}

	size_t	nRead	= 0;

	errMsg	= rxData(
		[&] ( const uint8_t* buffer, size_t len ) {
			nRead	= len;
			if( !len ) {
				return;
				}
			payload._buffer.copyIn(
				buffer,
				len
				);
			}
		);

	if( errMsg ) {
		payload._failed	= true;
		msg.returnToSender();
		return;
		}

	if( !nRead ) {
		// Not a failure
		payload._failed	= false;
		msg.returnToSender();
		return;
		}

	payload._failed	= false;

	msg.returnToSender();
	}

void	Part::request(	Oscl::Driver::Inventek::esWiFi::Symbiont::Kernel::
						Req::Api::DisconnectReq& msg
						) noexcept {

	auto&
	payload	= msg.getPayload();

	if( !_started || !_connected ) {
		payload._failed	= true;
		msg.returnToSender();
		return;
		}

	const char*
	errMsg	= setSocketID( payload._socketID );

	if( errMsg ) {
		payload._failed	= true;
		msg.returnToSender();
		return;
		}

	errMsg	= stopTcpClient( );

	payload._failed	= ( errMsg )?true:false;

	msg.returnToSender();
	}

void	Part::request(	Oscl::Driver::Inventek::esWiFi::Symbiont::Kernel::
						Req::Api::ConnectReq& msg
						) noexcept {

	auto&
	payload	= msg.getPayload();

	if( !_started || _scanning ) {
		payload._failed	= true;
		msg.returnToSender();
		return;
		}

	const char*
	errMsg	= setSocketID( payload._socketID );

	if( errMsg ) {
		payload._failed	= true;
		msg.returnToSender();
		return;
		}

	char	ipAddress[33];
	static constexpr size_t	ipAddressSize = sizeof( ipAddress );

	errMsg	= dnsLookup(
				payload._hostname,
				[&] ( uint8_t* buffer, size_t len ) {
					strncpy(
						ipAddress,
						(const char*)buffer,
						ipAddressSize - 1
						);

					ipAddress[ ipAddressSize -1 ]	= '\0';
					}
				);

	if( errMsg ) {
		payload._failed	= true;
		msg.returnToSender();
		return;
		}

	errMsg	= setRemotePort( payload._port );

	if( errMsg ) {
		payload._failed	= true;
		msg.returnToSender();
		return;
		}

	if( payload._transportProtocol == 3 ) {
		// TCP-SSL
		errMsg	= setTransportProtocol( "3" );
		if( !errMsg ) {
			errMsg	= setSslVerificationLevel( "0" );
			}
		}
	else {
		// TCP
		errMsg	= setTransportProtocol( "0" );
		}

	if( errMsg ) {
		payload._failed	= true;
		msg.returnToSender();
		return;
		}

	/*	The setRemoteHost is unnecessary when dnsLookup is used.
		D0 Note: The IP address will automatically be stored as
		the remote host IP address.
		(Note, reference the P3 command.)
	 */

	errMsg	= stopTcpClient( );

	if( errMsg ) {
		// printf("stopTcpClient: failed \"%s\"\n", errMsg);
		}

	// printf("startTcpClient: starting\n");

	errMsg	= startTcpClient( );

	if( errMsg ) {
		// printf("startTcpClient: failed \"%s\"\n", errMsg);
		payload._failed	= true;
		msg.returnToSender();
		return;
		}

	// printf("startTcpClient: \"%s\"\n", errMsg);

	payload._failed	= ( errMsg )?true:false;

	msg.returnToSender();
	}

