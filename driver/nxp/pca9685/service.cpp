/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/


#include "service.h"
#include "oscl/error/info.h"

using namespace Oscl::Driver::NXP::PCA9685;

Service::Service(
	Oscl::Mt::Itc::PostMsgApi&		papi,
	Oscl::Driver::NXP::
	PCA9685::Register::Api&			api,
	uint8_t							preScaler
	) noexcept:
		_closeSync(
			*this,
			papi
			),
		_part(
			papi,
			api,
			preScaler
			)
		{
	}

Oscl::Mt::Itc::Srv::OpenSyncApi&	Service::getOpenSyncApi() noexcept{
	return _closeSync;
	}

Oscl::Mt::Itc::Srv::CloseSyncApi&	Service::getCloseSyncApi() noexcept{
	return _closeSync;
	}

Oscl::Mt::Itc::Srv::Open::Req::Api::SAP&    Service::getOpenSAP() noexcept{
	return _closeSync.getOpenSAP();
	}

Oscl::Mt::Itc::Srv::Close::Req::Api::SAP&   Service::getCloseSAP() noexcept{
	return _closeSync.getSAP();
	}

void	Service::request(Oscl::Mt::Itc::Srv::Open::Req::Api::OpenReq& msg) noexcept{
	_part.start();
	msg.returnToSender();
	}

void	Service::request(Oscl::Mt::Itc::Srv::Close::Req::Api::CloseReq& msg) noexcept{
	// Not implemented yet.
	Oscl::Error::Info::log(
		"%s: NOT IMPLEMENTED!\n",
		__PRETTY_FUNCTION__
		);
	for(;;);
	}

Oscl::PWM::Discrete::Api&	Service::getChannel0SyncApi() noexcept{
	return _part.getChannel0SyncApi();
	}

Oscl::PWM::Discrete::Req::Api::SAP&	Service::getChannel0SAP() noexcept{
	return _part.getChannel0SAP();
	}

Oscl::PWM::Discrete::Api&	Service::getChannel1SyncApi() noexcept{
	return _part.getChannel1SyncApi();
	}

Oscl::PWM::Discrete::Req::Api::SAP&	Service::getChannel1SAP() noexcept{
	return _part.getChannel1SAP();
	}

Oscl::PWM::Discrete::Api&	Service::getChannel2SyncApi() noexcept{
	return _part.getChannel2SyncApi();
	}

Oscl::PWM::Discrete::Req::Api::SAP&	Service::getChannel2SAP() noexcept{
	return _part.getChannel2SAP();
	}

Oscl::PWM::Discrete::Api&	Service::getChannel3SyncApi() noexcept{
	return _part.getChannel3SyncApi();
	}

Oscl::PWM::Discrete::Req::Api::SAP&	Service::getChannel3SAP() noexcept{
	return _part.getChannel3SAP();
	}

Oscl::PWM::Discrete::Api&	Service::getChannel4SyncApi() noexcept{
	return _part.getChannel4SyncApi();
	}

Oscl::PWM::Discrete::Req::Api::SAP&	Service::getChannel4SAP() noexcept{
	return _part.getChannel4SAP();
	}

Oscl::PWM::Discrete::Api&	Service::getChannel5SyncApi() noexcept{
	return _part.getChannel5SyncApi();
	}

Oscl::PWM::Discrete::Req::Api::SAP&	Service::getChannel5SAP() noexcept{
	return _part.getChannel5SAP();
	}

Oscl::PWM::Discrete::Api&	Service::getChannel6SyncApi() noexcept{
	return _part.getChannel6SyncApi();
	}

Oscl::PWM::Discrete::Req::Api::SAP&	Service::getChannel6SAP() noexcept{
	return _part.getChannel6SAP();
	}

Oscl::PWM::Discrete::Api&	Service::getChannel7SyncApi() noexcept{
	return _part.getChannel7SyncApi();
	}

Oscl::PWM::Discrete::Req::Api::SAP&	Service::getChannel7SAP() noexcept{
	return _part.getChannel7SAP();
	}

Oscl::PWM::Discrete::Api&	Service::getChannel8SyncApi() noexcept{
	return _part.getChannel8SyncApi();
	}

Oscl::PWM::Discrete::Req::Api::SAP&	Service::getChannel8SAP() noexcept{
	return _part.getChannel8SAP();
	}

Oscl::PWM::Discrete::Api&	Service::getChannel9SyncApi() noexcept{
	return _part.getChannel9SyncApi();
	}

Oscl::PWM::Discrete::Req::Api::SAP&	Service::getChannel9SAP() noexcept{
	return _part.getChannel9SAP();
	}

Oscl::PWM::Discrete::Api&	Service::getChannel10SyncApi() noexcept{
	return _part.getChannel10SyncApi();
	}

Oscl::PWM::Discrete::Req::Api::SAP&	Service::getChannel10SAP() noexcept{
	return _part.getChannel10SAP();
	}

Oscl::PWM::Discrete::Api&	Service::getChannel11SyncApi() noexcept{
	return _part.getChannel11SyncApi();
	}

Oscl::PWM::Discrete::Req::Api::SAP&	Service::getChannel11SAP() noexcept{
	return _part.getChannel11SAP();
	}

Oscl::PWM::Discrete::Api&	Service::getChannel12SyncApi() noexcept{
	return _part.getChannel12SyncApi();
	}

Oscl::PWM::Discrete::Req::Api::SAP&	Service::getChannel12SAP() noexcept{
	return _part.getChannel12SAP();
	}

Oscl::PWM::Discrete::Api&	Service::getChannel13SyncApi() noexcept{
	return _part.getChannel13SyncApi();
	}

Oscl::PWM::Discrete::Req::Api::SAP&	Service::getChannel13SAP() noexcept{
	return _part.getChannel13SAP();
	}

Oscl::PWM::Discrete::Api&	Service::getChannel14SyncApi() noexcept{
	return _part.getChannel14SyncApi();
	}

Oscl::PWM::Discrete::Req::Api::SAP&	Service::getChannel14SAP() noexcept{
	return _part.getChannel14SAP();
	}

Oscl::PWM::Discrete::Api&	Service::getChannel15SyncApi() noexcept{
	return _part.getChannel15SyncApi();
	}

Oscl::PWM::Discrete::Req::Api::SAP&	Service::getChannel15SAP() noexcept{
	return _part.getChannel15SAP();
	}

