/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/


#ifndef _oscl_driver_nxp_pca9685_parth_
#define _oscl_driver_nxp_pca9685_parth_

#include "oscl/driver/nxp/pca9685/register/api.h"
#include "oscl/mt/itc/postmsgapi.h"
#include "oscl/pwm/discrete/sync.h"
#include "oscl/pwm/discrete/reqcomp.h"

/** */
namespace Oscl {

/** */
namespace Driver {

/** */
namespace NXP {

/** */
namespace PCA9685 {

/** */
class Part {
	private:
		/** */
		Oscl::Mt::Itc::PostMsgApi&						_papi;

		/** */
		Oscl::Driver::NXP::PCA9685::Register::Api&		_api;

		/** Valid range: 3:255
		 */
		const uint8_t									_preScaler;

	private:
		/** */
		Oscl::PWM::Discrete::Req::Composer<Part>		_channel0ReqComp;

		/** */
		Oscl::PWM::Discrete::Sync						_channel0Sync;

	private:
		/** */
		Oscl::PWM::Discrete::Req::Composer<Part>		_channel1ReqComp;

		/** */
		Oscl::PWM::Discrete::Sync						_channel1Sync;

	private:
		/** */
		Oscl::PWM::Discrete::Req::Composer<Part>		_channel2ReqComp;

		/** */
		Oscl::PWM::Discrete::Sync						_channel2Sync;

	private:
		/** */
		Oscl::PWM::Discrete::Req::Composer<Part>		_channel3ReqComp;

		/** */
		Oscl::PWM::Discrete::Sync						_channel3Sync;

	private:
		/** */
		Oscl::PWM::Discrete::Req::Composer<Part>		_channel4ReqComp;

		/** */
		Oscl::PWM::Discrete::Sync						_channel4Sync;

	private:
		/** */
		Oscl::PWM::Discrete::Req::Composer<Part>		_channel5ReqComp;

		/** */
		Oscl::PWM::Discrete::Sync						_channel5Sync;

	private:
		/** */
		Oscl::PWM::Discrete::Req::Composer<Part>		_channel6ReqComp;

		/** */
		Oscl::PWM::Discrete::Sync						_channel6Sync;

	private:
		/** */
		Oscl::PWM::Discrete::Req::Composer<Part>		_channel7ReqComp;

		/** */
		Oscl::PWM::Discrete::Sync						_channel7Sync;

	private:
		/** */
		Oscl::PWM::Discrete::Req::Composer<Part>		_channel8ReqComp;

		/** */
		Oscl::PWM::Discrete::Sync						_channel8Sync;

	private:
		/** */
		Oscl::PWM::Discrete::Req::Composer<Part>		_channel9ReqComp;

		/** */
		Oscl::PWM::Discrete::Sync						_channel9Sync;

	private:
		/** */
		Oscl::PWM::Discrete::Req::Composer<Part>		_channel10ReqComp;

		/** */
		Oscl::PWM::Discrete::Sync						_channel10Sync;

	private:
		/** */
		Oscl::PWM::Discrete::Req::Composer<Part>		_channel11ReqComp;

		/** */
		Oscl::PWM::Discrete::Sync						_channel11Sync;

	private:
		/** */
		Oscl::PWM::Discrete::Req::Composer<Part>		_channel12ReqComp;

		/** */
		Oscl::PWM::Discrete::Sync						_channel12Sync;

	private:
		/** */
		Oscl::PWM::Discrete::Req::Composer<Part>		_channel13ReqComp;

		/** */
		Oscl::PWM::Discrete::Sync						_channel13Sync;

	private:
		/** */
		Oscl::PWM::Discrete::Req::Composer<Part>		_channel14ReqComp;

		/** */
		Oscl::PWM::Discrete::Sync						_channel14Sync;

	private:
		/** */
		Oscl::PWM::Discrete::Req::Composer<Part>		_channel15ReqComp;

		/** */
		Oscl::PWM::Discrete::Sync						_channel15Sync;

	public:
		/** */
		Part(
			Oscl::Mt::Itc::PostMsgApi&	papi,
			Oscl::Driver::NXP::
			PCA9685::Register::Api&		agApi,
			uint8_t						preScaler
			) noexcept;

		/** */
		void	start() noexcept;

		/** */
		Oscl::PWM::Discrete::Api&	getChannel0SyncApi() noexcept;

		/** */
		Oscl::PWM::Discrete::Req::Api::SAP&	getChannel0SAP() noexcept;

		/** */
		Oscl::PWM::Discrete::Api&	getChannel1SyncApi() noexcept;

		/** */
		Oscl::PWM::Discrete::Req::Api::SAP&	getChannel1SAP() noexcept;

		/** */
		Oscl::PWM::Discrete::Api&	getChannel2SyncApi() noexcept;

		/** */
		Oscl::PWM::Discrete::Req::Api::SAP&	getChannel2SAP() noexcept;

		/** */
		Oscl::PWM::Discrete::Api&	getChannel3SyncApi() noexcept;

		/** */
		Oscl::PWM::Discrete::Req::Api::SAP&	getChannel3SAP() noexcept;

		/** */
		Oscl::PWM::Discrete::Api&	getChannel4SyncApi() noexcept;

		/** */
		Oscl::PWM::Discrete::Req::Api::SAP&	getChannel4SAP() noexcept;

		/** */
		Oscl::PWM::Discrete::Api&	getChannel5SyncApi() noexcept;

		/** */
		Oscl::PWM::Discrete::Req::Api::SAP&	getChannel5SAP() noexcept;

		/** */
		Oscl::PWM::Discrete::Api&	getChannel6SyncApi() noexcept;

		/** */
		Oscl::PWM::Discrete::Req::Api::SAP&	getChannel6SAP() noexcept;

		/** */
		Oscl::PWM::Discrete::Api&	getChannel7SyncApi() noexcept;

		/** */
		Oscl::PWM::Discrete::Req::Api::SAP&	getChannel7SAP() noexcept;

		/** */
		Oscl::PWM::Discrete::Api&	getChannel8SyncApi() noexcept;

		/** */
		Oscl::PWM::Discrete::Req::Api::SAP&	getChannel8SAP() noexcept;

		/** */
		Oscl::PWM::Discrete::Api&	getChannel9SyncApi() noexcept;

		/** */
		Oscl::PWM::Discrete::Req::Api::SAP&	getChannel9SAP() noexcept;

		/** */
		Oscl::PWM::Discrete::Api&	getChannel10SyncApi() noexcept;

		/** */
		Oscl::PWM::Discrete::Req::Api::SAP&	getChannel10SAP() noexcept;

		/** */
		Oscl::PWM::Discrete::Api&	getChannel11SyncApi() noexcept;

		/** */
		Oscl::PWM::Discrete::Req::Api::SAP&	getChannel11SAP() noexcept;

		/** */
		Oscl::PWM::Discrete::Api&	getChannel12SyncApi() noexcept;

		/** */
		Oscl::PWM::Discrete::Req::Api::SAP&	getChannel12SAP() noexcept;

		/** */
		Oscl::PWM::Discrete::Api&	getChannel13SyncApi() noexcept;

		/** */
		Oscl::PWM::Discrete::Req::Api::SAP&	getChannel13SAP() noexcept;

		/** */
		Oscl::PWM::Discrete::Api&	getChannel14SyncApi() noexcept;

		/** */
		Oscl::PWM::Discrete::Req::Api::SAP&	getChannel14SAP() noexcept;

		/** */
		Oscl::PWM::Discrete::Api&	getChannel15SyncApi() noexcept;

		/** */
		Oscl::PWM::Discrete::Req::Api::SAP&	getChannel15SAP() noexcept;

	private:
		/** RETURN: true on failure. */
		bool	configureMODE1() noexcept;

		/** RETURN: true on failure. */
		bool	configureMODE2() noexcept;

		/** RETURN: true on failure. */
		bool	configurePRE_SCALE() noexcept;

		/** RETURN: true on failure. */
		bool	configurePWM(unsigned channel) noexcept;

		/** */
		void	channelUpdate(
					uint8_t	channel,
					Oscl::PWM::Discrete::Req::Api::SetPulseWidthPayload& payload
					) noexcept;

	private: // Oscl::PWM::Discrete::Req::Api _channel0ReqComp
		/** */
		void	channel0Request(Oscl::PWM::Discrete::Req::Api::SetPulseWidthReq& msg) noexcept;

	private: // Oscl::PWM::Discrete::Req::Api _channel1ReqComp
		/** */
		void	channel1Request(Oscl::PWM::Discrete::Req::Api::SetPulseWidthReq& msg) noexcept;

	private: // Oscl::PWM::Discrete::Req::Api _channel2ReqComp
		/** */
		void	channel2Request(Oscl::PWM::Discrete::Req::Api::SetPulseWidthReq& msg) noexcept;

	private: // Oscl::PWM::Discrete::Req::Api _channel3ReqComp
		/** */
		void	channel3Request(Oscl::PWM::Discrete::Req::Api::SetPulseWidthReq& msg) noexcept;

	private: // Oscl::PWM::Discrete::Req::Api _channel4ReqComp
		/** */
		void	channel4Request(Oscl::PWM::Discrete::Req::Api::SetPulseWidthReq& msg) noexcept;

	private: // Oscl::PWM::Discrete::Req::Api _channel5ReqComp
		/** */
		void	channel5Request(Oscl::PWM::Discrete::Req::Api::SetPulseWidthReq& msg) noexcept;

	private: // Oscl::PWM::Discrete::Req::Api _channel6ReqComp
		/** */
		void	channel6Request(Oscl::PWM::Discrete::Req::Api::SetPulseWidthReq& msg) noexcept;

	private: // Oscl::PWM::Discrete::Req::Api _channel7ReqComp
		/** */
		void	channel7Request(Oscl::PWM::Discrete::Req::Api::SetPulseWidthReq& msg) noexcept;

	private: // Oscl::PWM::Discrete::Req::Api _channel8ReqComp
		/** */
		void	channel8Request(Oscl::PWM::Discrete::Req::Api::SetPulseWidthReq& msg) noexcept;

	private: // Oscl::PWM::Discrete::Req::Api _channel9ReqComp
		/** */
		void	channel9Request(Oscl::PWM::Discrete::Req::Api::SetPulseWidthReq& msg) noexcept;

	private: // Oscl::PWM::Discrete::Req::Api _channel10ReqComp
		/** */
		void	channel10Request(Oscl::PWM::Discrete::Req::Api::SetPulseWidthReq& msg) noexcept;

	private: // Oscl::PWM::Discrete::Req::Api _channel11ReqComp
		/** */
		void	channel11Request(Oscl::PWM::Discrete::Req::Api::SetPulseWidthReq& msg) noexcept;

	private: // Oscl::PWM::Discrete::Req::Api _channel12ReqComp
		/** */
		void	channel12Request(Oscl::PWM::Discrete::Req::Api::SetPulseWidthReq& msg) noexcept;

	private: // Oscl::PWM::Discrete::Req::Api _channel13ReqComp
		/** */
		void	channel13Request(Oscl::PWM::Discrete::Req::Api::SetPulseWidthReq& msg) noexcept;

	private: // Oscl::PWM::Discrete::Req::Api _channel14ReqComp
		/** */
		void	channel14Request(Oscl::PWM::Discrete::Req::Api::SetPulseWidthReq& msg) noexcept;

	private: // Oscl::PWM::Discrete::Req::Api _channel15ReqComp
		/** */
		void	channel15Request(Oscl::PWM::Discrete::Req::Api::SetPulseWidthReq& msg) noexcept;

	};

}
}
}
}

#endif
