/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <string.h>
#include "part.h"
#include "oscl/error/info.h"
#include "oscl/hw/nxp/pca9685/reg.h"
#include "oscl/mt/thread.h"

using namespace Oscl::Driver::NXP::PCA9685;

Part::Part(
	Oscl::Mt::Itc::PostMsgApi&		papi,
	Oscl::Driver::NXP::
	PCA9685::Register::Api&			api,
	uint8_t							preScaler
	) noexcept:
		_papi(papi),
		_api(api),
		_preScaler(preScaler),
		_channel0ReqComp(
			*this,
			&Part::channel0Request
			),
		_channel0Sync(
			papi,
			_channel0ReqComp
			),
		_channel1ReqComp(
			*this,
			&Part::channel1Request
			),
		_channel1Sync(
			papi,
			_channel1ReqComp
			),
		_channel2ReqComp(
			*this,
			&Part::channel2Request
			),
		_channel2Sync(
			papi,
			_channel2ReqComp
			),
		_channel3ReqComp(
			*this,
			&Part::channel3Request
			),
		_channel3Sync(
			papi,
			_channel3ReqComp
			),
		_channel4ReqComp(
			*this,
			&Part::channel4Request
			),
		_channel4Sync(
			papi,
			_channel4ReqComp
			),
		_channel5ReqComp(
			*this,
			&Part::channel5Request
			),
		_channel5Sync(
			papi,
			_channel5ReqComp
			),
		_channel6ReqComp(
			*this,
			&Part::channel6Request
			),
		_channel6Sync(
			papi,
			_channel6ReqComp
			),
		_channel7ReqComp(
			*this,
			&Part::channel7Request
			),
		_channel7Sync(
			papi,
			_channel7ReqComp
			),
		_channel8ReqComp(
			*this,
			&Part::channel8Request
			),
		_channel8Sync(
			papi,
			_channel8ReqComp
			),
		_channel9ReqComp(
			*this,
			&Part::channel9Request
			),
		_channel9Sync(
			papi,
			_channel9ReqComp
			),
		_channel10ReqComp(
			*this,
			&Part::channel10Request
			),
		_channel10Sync(
			papi,
			_channel10ReqComp
			),
		_channel11ReqComp(
			*this,
			&Part::channel11Request
			),
		_channel11Sync(
			papi,
			_channel11ReqComp
			),
		_channel12ReqComp(
			*this,
			&Part::channel12Request
			),
		_channel12Sync(
			papi,
			_channel12ReqComp
			),
		_channel13ReqComp(
			*this,
			&Part::channel13Request
			),
		_channel13Sync(
			papi,
			_channel13ReqComp
			),
		_channel14ReqComp(
			*this,
			&Part::channel14Request
			),
		_channel14Sync(
			papi,
			_channel14ReqComp
			),
		_channel15ReqComp(
			*this,
			&Part::channel15Request
			),
		_channel15Sync(
			papi,
			_channel15ReqComp
			)
		{
	}

void	Part::start() noexcept{
	configurePRE_SCALE();
	configureMODE1();
	configureMODE2();
	for(unsigned channel=0;channel<16;++channel){
		configurePWM(channel);
		}
	}

Oscl::PWM::Discrete::Api&	Part::getChannel0SyncApi() noexcept{
	return _channel0Sync;
	}

Oscl::PWM::Discrete::Req::Api::SAP&	Part::getChannel0SAP() noexcept{
	return _channel0Sync.getSAP();
	}

Oscl::PWM::Discrete::Api&	Part::getChannel1SyncApi() noexcept{
	return _channel1Sync;
	}

Oscl::PWM::Discrete::Req::Api::SAP&	Part::getChannel1SAP() noexcept{
	return _channel1Sync.getSAP();
	}

Oscl::PWM::Discrete::Api&	Part::getChannel2SyncApi() noexcept{
	return _channel2Sync;
	}

Oscl::PWM::Discrete::Req::Api::SAP&	Part::getChannel2SAP() noexcept{
	return _channel2Sync.getSAP();
	}

Oscl::PWM::Discrete::Api&	Part::getChannel3SyncApi() noexcept{
	return _channel3Sync;
	}

Oscl::PWM::Discrete::Req::Api::SAP&	Part::getChannel3SAP() noexcept{
	return _channel3Sync.getSAP();
	}

Oscl::PWM::Discrete::Api&	Part::getChannel4SyncApi() noexcept{
	return _channel4Sync;
	}

Oscl::PWM::Discrete::Req::Api::SAP&	Part::getChannel4SAP() noexcept{
	return _channel4Sync.getSAP();
	}

Oscl::PWM::Discrete::Api&	Part::getChannel5SyncApi() noexcept{
	return _channel5Sync;
	}

Oscl::PWM::Discrete::Req::Api::SAP&	Part::getChannel5SAP() noexcept{
	return _channel5Sync.getSAP();
	}

Oscl::PWM::Discrete::Api&	Part::getChannel6SyncApi() noexcept{
	return _channel6Sync;
	}

Oscl::PWM::Discrete::Req::Api::SAP&	Part::getChannel6SAP() noexcept{
	return _channel6Sync.getSAP();
	}

Oscl::PWM::Discrete::Api&	Part::getChannel7SyncApi() noexcept{
	return _channel7Sync;
	}

Oscl::PWM::Discrete::Req::Api::SAP&	Part::getChannel7SAP() noexcept{
	return _channel7Sync.getSAP();
	}

Oscl::PWM::Discrete::Api&	Part::getChannel8SyncApi() noexcept{
	return _channel8Sync;
	}

Oscl::PWM::Discrete::Req::Api::SAP&	Part::getChannel8SAP() noexcept{
	return _channel8Sync.getSAP();
	}

Oscl::PWM::Discrete::Api&	Part::getChannel9SyncApi() noexcept{
	return _channel9Sync;
	}

Oscl::PWM::Discrete::Req::Api::SAP&	Part::getChannel9SAP() noexcept{
	return _channel9Sync.getSAP();
	}

Oscl::PWM::Discrete::Api&	Part::getChannel10SyncApi() noexcept{
	return _channel10Sync;
	}

Oscl::PWM::Discrete::Req::Api::SAP&	Part::getChannel10SAP() noexcept{
	return _channel10Sync.getSAP();
	}

Oscl::PWM::Discrete::Api&	Part::getChannel11SyncApi() noexcept{
	return _channel11Sync;
	}

Oscl::PWM::Discrete::Req::Api::SAP&	Part::getChannel11SAP() noexcept{
	return _channel11Sync.getSAP();
	}

Oscl::PWM::Discrete::Api&	Part::getChannel12SyncApi() noexcept{
	return _channel12Sync;
	}

Oscl::PWM::Discrete::Req::Api::SAP&	Part::getChannel12SAP() noexcept{
	return _channel12Sync.getSAP();
	}

Oscl::PWM::Discrete::Api&	Part::getChannel13SyncApi() noexcept{
	return _channel13Sync;
	}

Oscl::PWM::Discrete::Req::Api::SAP&	Part::getChannel13SAP() noexcept{
	return _channel13Sync.getSAP();
	}

Oscl::PWM::Discrete::Api&	Part::getChannel14SyncApi() noexcept{
	return _channel14Sync;
	}

Oscl::PWM::Discrete::Req::Api::SAP&	Part::getChannel14SAP() noexcept{
	return _channel14Sync.getSAP();
	}

Oscl::PWM::Discrete::Api&	Part::getChannel15SyncApi() noexcept{
	return _channel15Sync;
	}

Oscl::PWM::Discrete::Req::Api::SAP&	Part::getChannel15SAP() noexcept{
	return _channel15Sync.getSAP();
	}

bool	Part::configureMODE1() noexcept{
	uint8_t
	mode1	=
			Oscl::HW::NXP::PCA9685::Mode1::RESTART::ValueMask_Default
		|	Oscl::HW::NXP::PCA9685::Mode1::EXTCLK::ValueMask_UseInternalClock
		|	Oscl::HW::NXP::PCA9685::Mode1::AI::ValueMask_AutoIncrementEnable
		|	Oscl::HW::NXP::PCA9685::Mode1::SLEEP::ValueMask_Normal
		|	Oscl::HW::NXP::PCA9685::Mode1::SUB1::ValueMask_IgnoreSubAddress
		|	Oscl::HW::NXP::PCA9685::Mode1::SUB2::ValueMask_IgnoreSubAddress
		|	Oscl::HW::NXP::PCA9685::Mode1::SUB3::ValueMask_IgnoreSubAddress
		|	Oscl::HW::NXP::PCA9685::Mode1::ALLCALL::ValueMask_IgnoreAllCallAddress
		;
	_api.setMODE1(mode1);

	// Must wait at least 500us when comming out of SLEEP.
	Oscl::Mt::Thread::sleep(10);
	return false;
	}

bool	Part::configureMODE2() noexcept{
	uint8_t
	mode2	=
			Oscl::HW::NXP::PCA9685::Mode2::INVRT::ValueMask_Default
		|	Oscl::HW::NXP::PCA9685::Mode2::OCH::ValueMask_Default
		|	Oscl::HW::NXP::PCA9685::Mode2::OUTDRV::ValueMask_TotemPole
		|	Oscl::HW::NXP::PCA9685::Mode2::OUTNE::ValueMask_LedLow
		;
	_api.setMODE2(mode2);
	return false;
	}

bool	Part::configurePRE_SCALE() noexcept{
	_api.setPRE_SCALE(_preScaler);
	return false;
	}

bool	Part::configurePWM(unsigned channel) noexcept{
	_api.setLED_ON(
			channel,	// channel
			0			// value
			);

	_api.setLED_OFF(
			channel,	// channel
			0			// value
			);

	return false;
	}

void	Part::channelUpdate(
			uint8_t	channel,
			Oscl::PWM::Discrete::Req::Api::SetPulseWidthPayload& payload
			) noexcept {
	unsigned	value	= payload._count;

	if(value > 4095){
		value	= 4095;
		Oscl::Error::Info::log(
			"%s: channel %u count (%u) out of range, clamped to 4095\n",
			__PRETTY_FUNCTION__,
			channel,
			value
			);
		}

	_api.setLED_OFF(
			channel,
			value
			);
	}

void	Part::channel0Request(Oscl::PWM::Discrete::Req::Api::SetPulseWidthReq& msg) noexcept{

	channelUpdate(0,msg.getPayload());

	msg.returnToSender();
	}

void	Part::channel1Request(Oscl::PWM::Discrete::Req::Api::SetPulseWidthReq& msg) noexcept{

	channelUpdate(1,msg.getPayload());

	msg.returnToSender();
	}

void	Part::channel2Request(Oscl::PWM::Discrete::Req::Api::SetPulseWidthReq& msg) noexcept{

	channelUpdate(2,msg.getPayload());

	msg.returnToSender();
	}

void	Part::channel3Request(Oscl::PWM::Discrete::Req::Api::SetPulseWidthReq& msg) noexcept{

	channelUpdate(3,msg.getPayload());

	msg.returnToSender();
	}

void	Part::channel4Request(Oscl::PWM::Discrete::Req::Api::SetPulseWidthReq& msg) noexcept{

	channelUpdate(4,msg.getPayload());

	msg.returnToSender();
	}

void	Part::channel5Request(Oscl::PWM::Discrete::Req::Api::SetPulseWidthReq& msg) noexcept{

	channelUpdate(5,msg.getPayload());

	msg.returnToSender();
	}

void	Part::channel6Request(Oscl::PWM::Discrete::Req::Api::SetPulseWidthReq& msg) noexcept{

	channelUpdate(6,msg.getPayload());

	msg.returnToSender();
	}

void	Part::channel7Request(Oscl::PWM::Discrete::Req::Api::SetPulseWidthReq& msg) noexcept{

	channelUpdate(7,msg.getPayload());

	msg.returnToSender();
	}

void	Part::channel8Request(Oscl::PWM::Discrete::Req::Api::SetPulseWidthReq& msg) noexcept{

	channelUpdate(8,msg.getPayload());

	msg.returnToSender();
	}

void	Part::channel9Request(Oscl::PWM::Discrete::Req::Api::SetPulseWidthReq& msg) noexcept{

	channelUpdate(9,msg.getPayload());

	msg.returnToSender();
	}

void	Part::channel10Request(Oscl::PWM::Discrete::Req::Api::SetPulseWidthReq& msg) noexcept{

	channelUpdate(10,msg.getPayload());

	msg.returnToSender();
	}

void	Part::channel11Request(Oscl::PWM::Discrete::Req::Api::SetPulseWidthReq& msg) noexcept{

	channelUpdate(11,msg.getPayload());

	msg.returnToSender();
	}

void	Part::channel12Request(Oscl::PWM::Discrete::Req::Api::SetPulseWidthReq& msg) noexcept{

	channelUpdate(12,msg.getPayload());

	msg.returnToSender();
	}

void	Part::channel13Request(Oscl::PWM::Discrete::Req::Api::SetPulseWidthReq& msg) noexcept{

	channelUpdate(13,msg.getPayload());

	msg.returnToSender();
	}

void	Part::channel14Request(Oscl::PWM::Discrete::Req::Api::SetPulseWidthReq& msg) noexcept{

	channelUpdate(14,msg.getPayload());

	msg.returnToSender();
	}

void	Part::channel15Request(Oscl::PWM::Discrete::Req::Api::SetPulseWidthReq& msg) noexcept{

	channelUpdate(15,msg.getPayload());

	msg.returnToSender();
	}

