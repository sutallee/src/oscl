/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_driver_nxp_pca9685_register_apih_
#define _oscl_driver_nxp_pca9685_register_apih_

#include <stdint.h>

/** */
namespace Oscl {

/** */
namespace Driver {

/** */
namespace NXP {

/** */
namespace PCA9685 {

/** */
namespace Register {

/**	This class defines the operations that can be performed
	procedurally by the client.
 */
class Api {
	public:
		/** Make the compiler happy.
		 */
		virtual ~Api(){};

		/**	
		 */
		virtual void	setPRE_SCALE(	
							uint8_t	value
							) noexcept=0;

		/**	
		 */
		virtual uint8_t	getPRE_SCALE(	
							) noexcept=0;

		/**	
		 */
		virtual void	setALL_LED_ON(	
							uint16_t	value
							) noexcept=0;

		/**	
		 */
		virtual uint16_t	getALL_LED_ON(	
							) noexcept=0;

		/**	
		 */
		virtual void	setLED_OFF(	
							uint8_t	channel,
							uint16_t	value
							) noexcept=0;

		/**	
		 */
		virtual uint16_t	getLED_OFF(	
							uint8_t	channel
							) noexcept=0;

		/**	
		 */
		virtual void	setLED_ON(	
							uint8_t	channel,
							uint16_t	value
							) noexcept=0;

		/**	
		 */
		virtual uint16_t	getLED_ON(	
							uint8_t	channel
							) noexcept=0;

		/**	
		 */
		virtual void	setALLCALLADDR(	
							uint8_t	value
							) noexcept=0;

		/**	
		 */
		virtual uint8_t	getALLCALLADDR(	
							) noexcept=0;

		/**	
		 */
		virtual void	setSUBADR3(	
							uint8_t	value
							) noexcept=0;

		/**	
		 */
		virtual uint8_t	getSUBADR3(	
							) noexcept=0;

		/**	
		 */
		virtual void	setSUBADR2(	
							uint8_t	value
							) noexcept=0;

		/**	
		 */
		virtual uint8_t	getSUBADR2(	
							) noexcept=0;

		/**	
		 */
		virtual void	setSUBADR1(	
							uint8_t	value
							) noexcept=0;

		/**	
		 */
		virtual uint8_t	getSUBADR1(	
							) noexcept=0;

		/**	
		 */
		virtual void	setMODE2(	
							uint8_t	value
							) noexcept=0;

		/**	
		 */
		virtual uint8_t	getMODE2(	
							) noexcept=0;

		/**	
		 */
		virtual void	setMODE1(	
							uint8_t	value
							) noexcept=0;

		/**	
		 */
		virtual uint8_t	getMODE1(	
							) noexcept=0;

	};

}
}
}
}
}
#endif
