/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

sysinclude "stdint.h";

/** */
namespace Oscl {
/** */
namespace Driver {
/** */
namespace NXP {
/** */
namespace PCA9685 {
/** */
namespace Register {

/** This interface defines methods to access the
	PCA9685 registers indpendent of the type of bus.
 */
itc ITC {

	/**  Read the MODE1 register.
	 */
	message	GetMODE1 {
		fields{
			/** */
			output	uint8_t	value;
			}

		/** */
		sync	uint8_t getMODE1 value;
		}

	/**  Write the MODE1 register
	 */
	message	SetMODE1 {
		fields{
			/** */
			input	uint8_t	value;
			}

		/** */
		sync	void setMODE1;
		}

	/**  Read the MODE2 register.
	 */
	message	GetMODE2 {
		fields{
			/** */
			output	uint8_t	value;
			}

		/** */
		sync	uint8_t getMODE2 value;
		}

	/**  Write the MODE2 register
	 */
	message	SetMODE2 {
		fields{
			/** */
			input	uint8_t	value;
			}

		/** */
		sync	void setMODE2;
		}

	/**  Read the SUBADR1 register.
	 */
	message	GetSUBADR1 {
		fields{
			/** */
			output	uint8_t	value;
			}

		/** */
		sync	uint8_t getSUBADR1 value;
		}

	/**  Write the SUBADR1 register
	 */
	message	SetSUBADR1 {
		fields{
			/** */
			input	uint8_t	value;
			}

		/** */
		sync	void setSUBADR1;
		}

	/**  Read the SUBADR2 register.
	 */
	message	GetSUBADR2 {
		fields{
			/** */
			output	uint8_t	value;
			}

		/** */
		sync	uint8_t getSUBADR2 value;
		}

	/**  Write the SUBADR2 register
	 */
	message	SetSUBADR2 {
		fields{
			/** */
			input	uint8_t	value;
			}

		/** */
		sync	void setSUBADR2;
		}

	/**  Read the SUBADR3 register.
	 */
	message	GetSUBADR3 {
		fields{
			/** */
			output	uint8_t	value;
			}

		/** */
		sync	uint8_t getSUBADR3 value;
		}

	/**  Write the SUBADR3 register
	 */
	message	SetSUBADR3 {
		fields{
			/** */
			input	uint8_t	value;
			}

		/** */
		sync	void setSUBADR3;
		}

	/**  Read the ALLCALLADDR register.
	 */
	message	GetALLCALLADDR {
		fields{
			/** */
			output	uint8_t	value;
			}

		/** */
		sync	uint8_t getALLCALLADDR value;
		}

	/**  Write the ALLCALLADDR register
	 */
	message	SetALLCALLADDR {
		fields{
			/** */
			input	uint8_t	value;
			}

		/** */
		sync	void setALLCALLADDR;
		}

	/**  Read the LED_ON register.
	 */
	message	GetLED_ON {
		fields{
			/** Valid range 0:15 */
			input	uint8_t	channel;

			/** */
			output	uint16_t	value;
			}

		/** */
		sync	uint16_t getLED_ON value;
		}

	/**  Write the LED_ON register
	 */
	message	SetLED_ON {
		fields{
			/** Valid range 0:15 */
			input	uint8_t	channel;

			/** Valid range 0:4095
			 */
			input	uint16_t	value;
			}

		/** */
		sync	void setLED_ON;
		}

	/**  Read the LED_OFF register.
	 */
	message	GetLED_OFF {
		fields{
			/** Valid range 0:15 */
			input	uint8_t	channel;

			/** */
			output	uint16_t	value;
			}

		/** */
		sync	uint16_t getLED_OFF value;
		}

	/**  Write the LED_OFF register
	 */
	message	SetLED_OFF {
		fields{
			/** Valid range 0:15 */
			input	uint8_t	channel;

			/** Valid range 0:4095
			 */
			input	uint16_t	value;
			}

		/** */
		sync	void setLED_OFF;
		}

	/**  Read the ALL_LED_ON register.
	 */
	message	GetALL_LED_ON {
		fields{
			/** */
			output	uint16_t	value;
			}

		/** */
		sync	uint16_t getALL_LED_ON value;
		}

	/**  Write the ALL_LED_ON register
	 */
	message	SetALL_LED_ON {
		fields{
			/** Valid range 0:4095
			 */
			input	uint16_t	value;
			}

		/** */
		sync	void setALL_LED_ON;
		}

	/**  Read the PRE_SCALE register.
	 */
	message	GetPRE_SCALE {
		fields{
			/** */
			output	uint8_t	value;
			}

		/** */
		sync	uint8_t getPRE_SCALE value;
		}

	/**  Write the PRE_SCALE register
	 */
	message	SetPRE_SCALE {
		fields{
			/** */
			input	uint8_t	value;
			}

		/** */
		sync	void setPRE_SCALE;
		}
	}
}
}
}
}
}
