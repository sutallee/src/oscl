/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "reqapi.h"

using namespace Oscl::Driver::NXP::PCA9685::Register;

Oscl::Driver::NXP::PCA9685::Register::Req::Api::SetPRE_SCALEPayload::
SetPRE_SCALEPayload(
		uint8_t	value
		) noexcept:
		_value(value)
		{}

Oscl::Driver::NXP::PCA9685::Register::Req::Api::GetPRE_SCALEPayload::
GetPRE_SCALEPayload(
		) noexcept
		{}

Oscl::Driver::NXP::PCA9685::Register::Req::Api::SetALL_LED_ONPayload::
SetALL_LED_ONPayload(
		uint16_t	value
		) noexcept:
		_value(value)
		{}

Oscl::Driver::NXP::PCA9685::Register::Req::Api::GetALL_LED_ONPayload::
GetALL_LED_ONPayload(
		) noexcept
		{}

Oscl::Driver::NXP::PCA9685::Register::Req::Api::SetLED_OFFPayload::
SetLED_OFFPayload(
		uint8_t	channel,
		uint16_t	value
		) noexcept:
		_channel(channel),
		_value(value)
		{}

Oscl::Driver::NXP::PCA9685::Register::Req::Api::GetLED_OFFPayload::
GetLED_OFFPayload(
		uint8_t	channel
		) noexcept:
		_channel(channel)
		{}

Oscl::Driver::NXP::PCA9685::Register::Req::Api::SetLED_ONPayload::
SetLED_ONPayload(
		uint8_t	channel,
		uint16_t	value
		) noexcept:
		_channel(channel),
		_value(value)
		{}

Oscl::Driver::NXP::PCA9685::Register::Req::Api::GetLED_ONPayload::
GetLED_ONPayload(
		uint8_t	channel
		) noexcept:
		_channel(channel)
		{}

Oscl::Driver::NXP::PCA9685::Register::Req::Api::SetALLCALLADDRPayload::
SetALLCALLADDRPayload(
		uint8_t	value
		) noexcept:
		_value(value)
		{}

Oscl::Driver::NXP::PCA9685::Register::Req::Api::GetALLCALLADDRPayload::
GetALLCALLADDRPayload(
		) noexcept
		{}

Oscl::Driver::NXP::PCA9685::Register::Req::Api::SetSUBADR3Payload::
SetSUBADR3Payload(
		uint8_t	value
		) noexcept:
		_value(value)
		{}

Oscl::Driver::NXP::PCA9685::Register::Req::Api::GetSUBADR3Payload::
GetSUBADR3Payload(
		) noexcept
		{}

Oscl::Driver::NXP::PCA9685::Register::Req::Api::SetSUBADR2Payload::
SetSUBADR2Payload(
		uint8_t	value
		) noexcept:
		_value(value)
		{}

Oscl::Driver::NXP::PCA9685::Register::Req::Api::GetSUBADR2Payload::
GetSUBADR2Payload(
		) noexcept
		{}

Oscl::Driver::NXP::PCA9685::Register::Req::Api::SetSUBADR1Payload::
SetSUBADR1Payload(
		uint8_t	value
		) noexcept:
		_value(value)
		{}

Oscl::Driver::NXP::PCA9685::Register::Req::Api::GetSUBADR1Payload::
GetSUBADR1Payload(
		) noexcept
		{}

Oscl::Driver::NXP::PCA9685::Register::Req::Api::SetMODE2Payload::
SetMODE2Payload(
		uint8_t	value
		) noexcept:
		_value(value)
		{}

Oscl::Driver::NXP::PCA9685::Register::Req::Api::GetMODE2Payload::
GetMODE2Payload(
		) noexcept
		{}

Oscl::Driver::NXP::PCA9685::Register::Req::Api::SetMODE1Payload::
SetMODE1Payload(
		uint8_t	value
		) noexcept:
		_value(value)
		{}

Oscl::Driver::NXP::PCA9685::Register::Req::Api::GetMODE1Payload::
GetMODE1Payload(
		) noexcept
		{}

