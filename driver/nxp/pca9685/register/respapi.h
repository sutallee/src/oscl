/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_driver_nxp_pca9685_register_itc_respapih_
#define _oscl_driver_nxp_pca9685_register_itc_respapih_

#include "reqapi.h"
#include "oscl/mt/itc/mbox/clirsp.h"

/** */
namespace Oscl {

/** */
namespace Driver {

/** */
namespace NXP {

/** */
namespace PCA9685 {

/** */
namespace Register {

/** */
namespace Resp {

/**	This class describes an ITC client interface. The
	interface includes the definition of client response messages
	based on their corresponding server request messages and
	associated payloads, as well as an interface that is to be
	implemented by a client that uses the corresponding server
	asynchronously.
 */
/**	This interface defines methods to access the PCA9685
	registers indpendent of the type of bus.
 */
class Api {
	public:
		/**	This describes a client response message that corresponds
			to the SetPRE_SCALEReq message described in the "reqapi.h"
			header file. This SetPRE_SCALEResp response message actually
			contains a SetPRE_SCALEReq request message.
		 */
		typedef Oscl::Mt::Itc::CliResponse<
					Oscl::Driver::NXP::PCA9685::Register::Req::Api,
					Api,
					Oscl::Driver::NXP::PCA9685::Register::
					Req::Api::SetPRE_SCALEPayload
					>		SetPRE_SCALEResp;

	public:
		/**	This describes a client response message that corresponds
			to the GetPRE_SCALEReq message described in the "reqapi.h"
			header file. This GetPRE_SCALEResp response message actually
			contains a GetPRE_SCALEReq request message.
		 */
		typedef Oscl::Mt::Itc::CliResponse<
					Oscl::Driver::NXP::PCA9685::Register::Req::Api,
					Api,
					Oscl::Driver::NXP::PCA9685::Register::
					Req::Api::GetPRE_SCALEPayload
					>		GetPRE_SCALEResp;

	public:
		/**	This describes a client response message that corresponds
			to the SetALL_LED_ONReq message described in the "reqapi.h"
			header file. This SetALL_LED_ONResp response message actually
			contains a SetALL_LED_ONReq request message.
		 */
		typedef Oscl::Mt::Itc::CliResponse<
					Oscl::Driver::NXP::PCA9685::Register::Req::Api,
					Api,
					Oscl::Driver::NXP::PCA9685::Register::
					Req::Api::SetALL_LED_ONPayload
					>		SetALL_LED_ONResp;

	public:
		/**	This describes a client response message that corresponds
			to the GetALL_LED_ONReq message described in the "reqapi.h"
			header file. This GetALL_LED_ONResp response message actually
			contains a GetALL_LED_ONReq request message.
		 */
		typedef Oscl::Mt::Itc::CliResponse<
					Oscl::Driver::NXP::PCA9685::Register::Req::Api,
					Api,
					Oscl::Driver::NXP::PCA9685::Register::
					Req::Api::GetALL_LED_ONPayload
					>		GetALL_LED_ONResp;

	public:
		/**	This describes a client response message that corresponds
			to the SetLED_OFFReq message described in the "reqapi.h"
			header file. This SetLED_OFFResp response message actually
			contains a SetLED_OFFReq request message.
		 */
		typedef Oscl::Mt::Itc::CliResponse<
					Oscl::Driver::NXP::PCA9685::Register::Req::Api,
					Api,
					Oscl::Driver::NXP::PCA9685::Register::
					Req::Api::SetLED_OFFPayload
					>		SetLED_OFFResp;

	public:
		/**	This describes a client response message that corresponds
			to the GetLED_OFFReq message described in the "reqapi.h"
			header file. This GetLED_OFFResp response message actually
			contains a GetLED_OFFReq request message.
		 */
		typedef Oscl::Mt::Itc::CliResponse<
					Oscl::Driver::NXP::PCA9685::Register::Req::Api,
					Api,
					Oscl::Driver::NXP::PCA9685::Register::
					Req::Api::GetLED_OFFPayload
					>		GetLED_OFFResp;

	public:
		/**	This describes a client response message that corresponds
			to the SetLED_ONReq message described in the "reqapi.h"
			header file. This SetLED_ONResp response message actually
			contains a SetLED_ONReq request message.
		 */
		typedef Oscl::Mt::Itc::CliResponse<
					Oscl::Driver::NXP::PCA9685::Register::Req::Api,
					Api,
					Oscl::Driver::NXP::PCA9685::Register::
					Req::Api::SetLED_ONPayload
					>		SetLED_ONResp;

	public:
		/**	This describes a client response message that corresponds
			to the GetLED_ONReq message described in the "reqapi.h"
			header file. This GetLED_ONResp response message actually
			contains a GetLED_ONReq request message.
		 */
		typedef Oscl::Mt::Itc::CliResponse<
					Oscl::Driver::NXP::PCA9685::Register::Req::Api,
					Api,
					Oscl::Driver::NXP::PCA9685::Register::
					Req::Api::GetLED_ONPayload
					>		GetLED_ONResp;

	public:
		/**	This describes a client response message that corresponds
			to the SetALLCALLADDRReq message described in the "reqapi.h"
			header file. This SetALLCALLADDRResp response message actually
			contains a SetALLCALLADDRReq request message.
		 */
		typedef Oscl::Mt::Itc::CliResponse<
					Oscl::Driver::NXP::PCA9685::Register::Req::Api,
					Api,
					Oscl::Driver::NXP::PCA9685::Register::
					Req::Api::SetALLCALLADDRPayload
					>		SetALLCALLADDRResp;

	public:
		/**	This describes a client response message that corresponds
			to the GetALLCALLADDRReq message described in the "reqapi.h"
			header file. This GetALLCALLADDRResp response message actually
			contains a GetALLCALLADDRReq request message.
		 */
		typedef Oscl::Mt::Itc::CliResponse<
					Oscl::Driver::NXP::PCA9685::Register::Req::Api,
					Api,
					Oscl::Driver::NXP::PCA9685::Register::
					Req::Api::GetALLCALLADDRPayload
					>		GetALLCALLADDRResp;

	public:
		/**	This describes a client response message that corresponds
			to the SetSUBADR3Req message described in the "reqapi.h"
			header file. This SetSUBADR3Resp response message actually
			contains a SetSUBADR3Req request message.
		 */
		typedef Oscl::Mt::Itc::CliResponse<
					Oscl::Driver::NXP::PCA9685::Register::Req::Api,
					Api,
					Oscl::Driver::NXP::PCA9685::Register::
					Req::Api::SetSUBADR3Payload
					>		SetSUBADR3Resp;

	public:
		/**	This describes a client response message that corresponds
			to the GetSUBADR3Req message described in the "reqapi.h"
			header file. This GetSUBADR3Resp response message actually
			contains a GetSUBADR3Req request message.
		 */
		typedef Oscl::Mt::Itc::CliResponse<
					Oscl::Driver::NXP::PCA9685::Register::Req::Api,
					Api,
					Oscl::Driver::NXP::PCA9685::Register::
					Req::Api::GetSUBADR3Payload
					>		GetSUBADR3Resp;

	public:
		/**	This describes a client response message that corresponds
			to the SetSUBADR2Req message described in the "reqapi.h"
			header file. This SetSUBADR2Resp response message actually
			contains a SetSUBADR2Req request message.
		 */
		typedef Oscl::Mt::Itc::CliResponse<
					Oscl::Driver::NXP::PCA9685::Register::Req::Api,
					Api,
					Oscl::Driver::NXP::PCA9685::Register::
					Req::Api::SetSUBADR2Payload
					>		SetSUBADR2Resp;

	public:
		/**	This describes a client response message that corresponds
			to the GetSUBADR2Req message described in the "reqapi.h"
			header file. This GetSUBADR2Resp response message actually
			contains a GetSUBADR2Req request message.
		 */
		typedef Oscl::Mt::Itc::CliResponse<
					Oscl::Driver::NXP::PCA9685::Register::Req::Api,
					Api,
					Oscl::Driver::NXP::PCA9685::Register::
					Req::Api::GetSUBADR2Payload
					>		GetSUBADR2Resp;

	public:
		/**	This describes a client response message that corresponds
			to the SetSUBADR1Req message described in the "reqapi.h"
			header file. This SetSUBADR1Resp response message actually
			contains a SetSUBADR1Req request message.
		 */
		typedef Oscl::Mt::Itc::CliResponse<
					Oscl::Driver::NXP::PCA9685::Register::Req::Api,
					Api,
					Oscl::Driver::NXP::PCA9685::Register::
					Req::Api::SetSUBADR1Payload
					>		SetSUBADR1Resp;

	public:
		/**	This describes a client response message that corresponds
			to the GetSUBADR1Req message described in the "reqapi.h"
			header file. This GetSUBADR1Resp response message actually
			contains a GetSUBADR1Req request message.
		 */
		typedef Oscl::Mt::Itc::CliResponse<
					Oscl::Driver::NXP::PCA9685::Register::Req::Api,
					Api,
					Oscl::Driver::NXP::PCA9685::Register::
					Req::Api::GetSUBADR1Payload
					>		GetSUBADR1Resp;

	public:
		/**	This describes a client response message that corresponds
			to the SetMODE2Req message described in the "reqapi.h"
			header file. This SetMODE2Resp response message actually
			contains a SetMODE2Req request message.
		 */
		typedef Oscl::Mt::Itc::CliResponse<
					Oscl::Driver::NXP::PCA9685::Register::Req::Api,
					Api,
					Oscl::Driver::NXP::PCA9685::Register::
					Req::Api::SetMODE2Payload
					>		SetMODE2Resp;

	public:
		/**	This describes a client response message that corresponds
			to the GetMODE2Req message described in the "reqapi.h"
			header file. This GetMODE2Resp response message actually
			contains a GetMODE2Req request message.
		 */
		typedef Oscl::Mt::Itc::CliResponse<
					Oscl::Driver::NXP::PCA9685::Register::Req::Api,
					Api,
					Oscl::Driver::NXP::PCA9685::Register::
					Req::Api::GetMODE2Payload
					>		GetMODE2Resp;

	public:
		/**	This describes a client response message that corresponds
			to the SetMODE1Req message described in the "reqapi.h"
			header file. This SetMODE1Resp response message actually
			contains a SetMODE1Req request message.
		 */
		typedef Oscl::Mt::Itc::CliResponse<
					Oscl::Driver::NXP::PCA9685::Register::Req::Api,
					Api,
					Oscl::Driver::NXP::PCA9685::Register::
					Req::Api::SetMODE1Payload
					>		SetMODE1Resp;

	public:
		/**	This describes a client response message that corresponds
			to the GetMODE1Req message described in the "reqapi.h"
			header file. This GetMODE1Resp response message actually
			contains a GetMODE1Req request message.
		 */
		typedef Oscl::Mt::Itc::CliResponse<
					Oscl::Driver::NXP::PCA9685::Register::Req::Api,
					Api,
					Oscl::Driver::NXP::PCA9685::Register::
					Req::Api::GetMODE1Payload
					>		GetMODE1Resp;

	public:
		/** Make the compiler happy. */
		virtual ~Api(){}

		/** This operation is invoked within the client thread
			whenever a SetPRE_SCALEResp message is received.
		 */
		virtual void	response(	Oscl::Driver::NXP::PCA9685::Register::
									Resp::Api::SetPRE_SCALEResp& msg
									) noexcept=0;

		/** This operation is invoked within the client thread
			whenever a GetPRE_SCALEResp message is received.
		 */
		virtual void	response(	Oscl::Driver::NXP::PCA9685::Register::
									Resp::Api::GetPRE_SCALEResp& msg
									) noexcept=0;

		/** This operation is invoked within the client thread
			whenever a SetALL_LED_ONResp message is received.
		 */
		virtual void	response(	Oscl::Driver::NXP::PCA9685::Register::
									Resp::Api::SetALL_LED_ONResp& msg
									) noexcept=0;

		/** This operation is invoked within the client thread
			whenever a GetALL_LED_ONResp message is received.
		 */
		virtual void	response(	Oscl::Driver::NXP::PCA9685::Register::
									Resp::Api::GetALL_LED_ONResp& msg
									) noexcept=0;

		/** This operation is invoked within the client thread
			whenever a SetLED_OFFResp message is received.
		 */
		virtual void	response(	Oscl::Driver::NXP::PCA9685::Register::
									Resp::Api::SetLED_OFFResp& msg
									) noexcept=0;

		/** This operation is invoked within the client thread
			whenever a GetLED_OFFResp message is received.
		 */
		virtual void	response(	Oscl::Driver::NXP::PCA9685::Register::
									Resp::Api::GetLED_OFFResp& msg
									) noexcept=0;

		/** This operation is invoked within the client thread
			whenever a SetLED_ONResp message is received.
		 */
		virtual void	response(	Oscl::Driver::NXP::PCA9685::Register::
									Resp::Api::SetLED_ONResp& msg
									) noexcept=0;

		/** This operation is invoked within the client thread
			whenever a GetLED_ONResp message is received.
		 */
		virtual void	response(	Oscl::Driver::NXP::PCA9685::Register::
									Resp::Api::GetLED_ONResp& msg
									) noexcept=0;

		/** This operation is invoked within the client thread
			whenever a SetALLCALLADDRResp message is received.
		 */
		virtual void	response(	Oscl::Driver::NXP::PCA9685::Register::
									Resp::Api::SetALLCALLADDRResp& msg
									) noexcept=0;

		/** This operation is invoked within the client thread
			whenever a GetALLCALLADDRResp message is received.
		 */
		virtual void	response(	Oscl::Driver::NXP::PCA9685::Register::
									Resp::Api::GetALLCALLADDRResp& msg
									) noexcept=0;

		/** This operation is invoked within the client thread
			whenever a SetSUBADR3Resp message is received.
		 */
		virtual void	response(	Oscl::Driver::NXP::PCA9685::Register::
									Resp::Api::SetSUBADR3Resp& msg
									) noexcept=0;

		/** This operation is invoked within the client thread
			whenever a GetSUBADR3Resp message is received.
		 */
		virtual void	response(	Oscl::Driver::NXP::PCA9685::Register::
									Resp::Api::GetSUBADR3Resp& msg
									) noexcept=0;

		/** This operation is invoked within the client thread
			whenever a SetSUBADR2Resp message is received.
		 */
		virtual void	response(	Oscl::Driver::NXP::PCA9685::Register::
									Resp::Api::SetSUBADR2Resp& msg
									) noexcept=0;

		/** This operation is invoked within the client thread
			whenever a GetSUBADR2Resp message is received.
		 */
		virtual void	response(	Oscl::Driver::NXP::PCA9685::Register::
									Resp::Api::GetSUBADR2Resp& msg
									) noexcept=0;

		/** This operation is invoked within the client thread
			whenever a SetSUBADR1Resp message is received.
		 */
		virtual void	response(	Oscl::Driver::NXP::PCA9685::Register::
									Resp::Api::SetSUBADR1Resp& msg
									) noexcept=0;

		/** This operation is invoked within the client thread
			whenever a GetSUBADR1Resp message is received.
		 */
		virtual void	response(	Oscl::Driver::NXP::PCA9685::Register::
									Resp::Api::GetSUBADR1Resp& msg
									) noexcept=0;

		/** This operation is invoked within the client thread
			whenever a SetMODE2Resp message is received.
		 */
		virtual void	response(	Oscl::Driver::NXP::PCA9685::Register::
									Resp::Api::SetMODE2Resp& msg
									) noexcept=0;

		/** This operation is invoked within the client thread
			whenever a GetMODE2Resp message is received.
		 */
		virtual void	response(	Oscl::Driver::NXP::PCA9685::Register::
									Resp::Api::GetMODE2Resp& msg
									) noexcept=0;

		/** This operation is invoked within the client thread
			whenever a SetMODE1Resp message is received.
		 */
		virtual void	response(	Oscl::Driver::NXP::PCA9685::Register::
									Resp::Api::SetMODE1Resp& msg
									) noexcept=0;

		/** This operation is invoked within the client thread
			whenever a GetMODE1Resp message is received.
		 */
		virtual void	response(	Oscl::Driver::NXP::PCA9685::Register::
									Resp::Api::GetMODE1Resp& msg
									) noexcept=0;

	};

}
}
}
}
}
}
#endif
