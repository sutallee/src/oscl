/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_driver_nxp_pca9685_register_itc_reqapih_
#define _oscl_driver_nxp_pca9685_register_itc_reqapih_

#include "oscl/mt/itc/mbox/srvreq.h"
#include "oscl/mt/itc/postmsgapi.h"
#include "oscl/mt/itc/mbox/sap.h"
#include <stdint.h>

/** */
namespace Oscl {

/** */
namespace Driver {

/** */
namespace NXP {

/** */
namespace PCA9685 {

/** */
namespace Register {

/** */
namespace Req {

/**	This class describes an ITC server interface. The
	interface includes the definition of messages and their
	associated payloads, as well as an interface that is to be
	implemented by a server that supports this ITC interface.
 */
/**	This interface defines methods to access the PCA9685
	registers indpendent of the type of bus.
 */
class Api {
	public:
		/** This class contains the data that is transfered
			between the client and server within the SetPRE_SCALEReq
			ITC message.
		 */
		class SetPRE_SCALEPayload {
			public:
				/**	
				 */
				uint8_t	_value;

			public:
				/** The SetPRE_SCALEPayload constructor. */
				SetPRE_SCALEPayload(

					uint8_t	value
					) noexcept;

			};

		/**	Write the PRE_SCALE register
		 */
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::Driver::NXP::PCA9685::Register::Req::Api,
					SetPRE_SCALEPayload
					>		SetPRE_SCALEReq;

	public:
		/** This class contains the data that is transfered
			between the client and server within the GetPRE_SCALEReq
			ITC message.
		 */
		class GetPRE_SCALEPayload {
			public:
				/**	
				 */
				uint8_t	_value;

			public:
				/** The GetPRE_SCALEPayload constructor. */
				GetPRE_SCALEPayload(
					) noexcept;

			};

		/**	Read the PRE_SCALE register.
		 */
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::Driver::NXP::PCA9685::Register::Req::Api,
					GetPRE_SCALEPayload
					>		GetPRE_SCALEReq;

	public:
		/** This class contains the data that is transfered
			between the client and server within the SetALL_LED_ONReq
			ITC message.
		 */
		class SetALL_LED_ONPayload {
			public:
				/**	Valid range 0:4095
				 */
				uint16_t	_value;

			public:
				/** The SetALL_LED_ONPayload constructor. */
				SetALL_LED_ONPayload(

					uint16_t	value
					) noexcept;

			};

		/**	Write the ALL_LED_ON register
		 */
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::Driver::NXP::PCA9685::Register::Req::Api,
					SetALL_LED_ONPayload
					>		SetALL_LED_ONReq;

	public:
		/** This class contains the data that is transfered
			between the client and server within the GetALL_LED_ONReq
			ITC message.
		 */
		class GetALL_LED_ONPayload {
			public:
				/**	
				 */
				uint16_t	_value;

			public:
				/** The GetALL_LED_ONPayload constructor. */
				GetALL_LED_ONPayload(
					) noexcept;

			};

		/**	Read the ALL_LED_ON register.
		 */
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::Driver::NXP::PCA9685::Register::Req::Api,
					GetALL_LED_ONPayload
					>		GetALL_LED_ONReq;

	public:
		/** This class contains the data that is transfered
			between the client and server within the SetLED_OFFReq
			ITC message.
		 */
		class SetLED_OFFPayload {
			public:
				/**	Valid range 0:15
				 */
				uint8_t	_channel;

				/**	Valid range 0:4095
				 */
				uint16_t	_value;

			public:
				/** The SetLED_OFFPayload constructor. */
				SetLED_OFFPayload(

					uint8_t	channel,

					uint16_t	value
					) noexcept;

			};

		/**	Write the LED_OFF register
		 */
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::Driver::NXP::PCA9685::Register::Req::Api,
					SetLED_OFFPayload
					>		SetLED_OFFReq;

	public:
		/** This class contains the data that is transfered
			between the client and server within the GetLED_OFFReq
			ITC message.
		 */
		class GetLED_OFFPayload {
			public:
				/**	Valid range 0:15
				 */
				uint8_t	_channel;

				/**	
				 */
				uint16_t	_value;

			public:
				/** The GetLED_OFFPayload constructor. */
				GetLED_OFFPayload(

					uint8_t	channel
					) noexcept;

			};

		/**	Read the LED_OFF register.
		 */
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::Driver::NXP::PCA9685::Register::Req::Api,
					GetLED_OFFPayload
					>		GetLED_OFFReq;

	public:
		/** This class contains the data that is transfered
			between the client and server within the SetLED_ONReq
			ITC message.
		 */
		class SetLED_ONPayload {
			public:
				/**	Valid range 0:15
				 */
				uint8_t	_channel;

				/**	Valid range 0:4095
				 */
				uint16_t	_value;

			public:
				/** The SetLED_ONPayload constructor. */
				SetLED_ONPayload(

					uint8_t	channel,

					uint16_t	value
					) noexcept;

			};

		/**	Write the LED_ON register
		 */
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::Driver::NXP::PCA9685::Register::Req::Api,
					SetLED_ONPayload
					>		SetLED_ONReq;

	public:
		/** This class contains the data that is transfered
			between the client and server within the GetLED_ONReq
			ITC message.
		 */
		class GetLED_ONPayload {
			public:
				/**	Valid range 0:15
				 */
				uint8_t	_channel;

				/**	
				 */
				uint16_t	_value;

			public:
				/** The GetLED_ONPayload constructor. */
				GetLED_ONPayload(

					uint8_t	channel
					) noexcept;

			};

		/**	Read the LED_ON register.
		 */
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::Driver::NXP::PCA9685::Register::Req::Api,
					GetLED_ONPayload
					>		GetLED_ONReq;

	public:
		/** This class contains the data that is transfered
			between the client and server within the SetALLCALLADDRReq
			ITC message.
		 */
		class SetALLCALLADDRPayload {
			public:
				/**	
				 */
				uint8_t	_value;

			public:
				/** The SetALLCALLADDRPayload constructor. */
				SetALLCALLADDRPayload(

					uint8_t	value
					) noexcept;

			};

		/**	Write the ALLCALLADDR register
		 */
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::Driver::NXP::PCA9685::Register::Req::Api,
					SetALLCALLADDRPayload
					>		SetALLCALLADDRReq;

	public:
		/** This class contains the data that is transfered
			between the client and server within the GetALLCALLADDRReq
			ITC message.
		 */
		class GetALLCALLADDRPayload {
			public:
				/**	
				 */
				uint8_t	_value;

			public:
				/** The GetALLCALLADDRPayload constructor. */
				GetALLCALLADDRPayload(
					) noexcept;

			};

		/**	Read the ALLCALLADDR register.
		 */
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::Driver::NXP::PCA9685::Register::Req::Api,
					GetALLCALLADDRPayload
					>		GetALLCALLADDRReq;

	public:
		/** This class contains the data that is transfered
			between the client and server within the SetSUBADR3Req
			ITC message.
		 */
		class SetSUBADR3Payload {
			public:
				/**	
				 */
				uint8_t	_value;

			public:
				/** The SetSUBADR3Payload constructor. */
				SetSUBADR3Payload(

					uint8_t	value
					) noexcept;

			};

		/**	Write the SUBADR3 register
		 */
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::Driver::NXP::PCA9685::Register::Req::Api,
					SetSUBADR3Payload
					>		SetSUBADR3Req;

	public:
		/** This class contains the data that is transfered
			between the client and server within the GetSUBADR3Req
			ITC message.
		 */
		class GetSUBADR3Payload {
			public:
				/**	
				 */
				uint8_t	_value;

			public:
				/** The GetSUBADR3Payload constructor. */
				GetSUBADR3Payload(
					) noexcept;

			};

		/**	Read the SUBADR3 register.
		 */
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::Driver::NXP::PCA9685::Register::Req::Api,
					GetSUBADR3Payload
					>		GetSUBADR3Req;

	public:
		/** This class contains the data that is transfered
			between the client and server within the SetSUBADR2Req
			ITC message.
		 */
		class SetSUBADR2Payload {
			public:
				/**	
				 */
				uint8_t	_value;

			public:
				/** The SetSUBADR2Payload constructor. */
				SetSUBADR2Payload(

					uint8_t	value
					) noexcept;

			};

		/**	Write the SUBADR2 register
		 */
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::Driver::NXP::PCA9685::Register::Req::Api,
					SetSUBADR2Payload
					>		SetSUBADR2Req;

	public:
		/** This class contains the data that is transfered
			between the client and server within the GetSUBADR2Req
			ITC message.
		 */
		class GetSUBADR2Payload {
			public:
				/**	
				 */
				uint8_t	_value;

			public:
				/** The GetSUBADR2Payload constructor. */
				GetSUBADR2Payload(
					) noexcept;

			};

		/**	Read the SUBADR2 register.
		 */
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::Driver::NXP::PCA9685::Register::Req::Api,
					GetSUBADR2Payload
					>		GetSUBADR2Req;

	public:
		/** This class contains the data that is transfered
			between the client and server within the SetSUBADR1Req
			ITC message.
		 */
		class SetSUBADR1Payload {
			public:
				/**	
				 */
				uint8_t	_value;

			public:
				/** The SetSUBADR1Payload constructor. */
				SetSUBADR1Payload(

					uint8_t	value
					) noexcept;

			};

		/**	Write the SUBADR1 register
		 */
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::Driver::NXP::PCA9685::Register::Req::Api,
					SetSUBADR1Payload
					>		SetSUBADR1Req;

	public:
		/** This class contains the data that is transfered
			between the client and server within the GetSUBADR1Req
			ITC message.
		 */
		class GetSUBADR1Payload {
			public:
				/**	
				 */
				uint8_t	_value;

			public:
				/** The GetSUBADR1Payload constructor. */
				GetSUBADR1Payload(
					) noexcept;

			};

		/**	Read the SUBADR1 register.
		 */
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::Driver::NXP::PCA9685::Register::Req::Api,
					GetSUBADR1Payload
					>		GetSUBADR1Req;

	public:
		/** This class contains the data that is transfered
			between the client and server within the SetMODE2Req
			ITC message.
		 */
		class SetMODE2Payload {
			public:
				/**	
				 */
				uint8_t	_value;

			public:
				/** The SetMODE2Payload constructor. */
				SetMODE2Payload(

					uint8_t	value
					) noexcept;

			};

		/**	Write the MODE2 register
		 */
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::Driver::NXP::PCA9685::Register::Req::Api,
					SetMODE2Payload
					>		SetMODE2Req;

	public:
		/** This class contains the data that is transfered
			between the client and server within the GetMODE2Req
			ITC message.
		 */
		class GetMODE2Payload {
			public:
				/**	
				 */
				uint8_t	_value;

			public:
				/** The GetMODE2Payload constructor. */
				GetMODE2Payload(
					) noexcept;

			};

		/**	Read the MODE2 register.
		 */
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::Driver::NXP::PCA9685::Register::Req::Api,
					GetMODE2Payload
					>		GetMODE2Req;

	public:
		/** This class contains the data that is transfered
			between the client and server within the SetMODE1Req
			ITC message.
		 */
		class SetMODE1Payload {
			public:
				/**	
				 */
				uint8_t	_value;

			public:
				/** The SetMODE1Payload constructor. */
				SetMODE1Payload(

					uint8_t	value
					) noexcept;

			};

		/**	Write the MODE1 register
		 */
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::Driver::NXP::PCA9685::Register::Req::Api,
					SetMODE1Payload
					>		SetMODE1Req;

	public:
		/** This class contains the data that is transfered
			between the client and server within the GetMODE1Req
			ITC message.
		 */
		class GetMODE1Payload {
			public:
				/**	
				 */
				uint8_t	_value;

			public:
				/** The GetMODE1Payload constructor. */
				GetMODE1Payload(
					) noexcept;

			};

		/**	Read the MODE1 register.
		 */
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::Driver::NXP::PCA9685::Register::Req::Api,
					GetMODE1Payload
					>		GetMODE1Req;

	public:
		/** This type defines a SAP (Service Access Point) for this
			ITC server interface. A SAP holds server address information
			that is required by clients to send messages to a server.
		 */
		typedef Oscl::Mt::Itc::SAP<	Oscl::Driver::NXP::PCA9685::Register::
							Req::Api
							>			SAP;

	public:
		/** This type defines a SAP (Service Access Point) for this
			ITC server interface. A SAP holds server address information
			that is required by clients to send messages to a server.
		 */
		typedef Oscl::Mt::Itc::ConcreteSAP<	Oscl::Driver::NXP::PCA9685::Register::
							Req::Api
							>			ConcreteSAP;

	public:
		/** Make the compiler happy. */
		virtual ~Api(){}

		/** This operation is invoked within the server thread
			whenever a SetPRE_SCALEReq is received.
		 */
		virtual void	request(	Oscl::Driver::NXP::PCA9685::Register::
									Req::Api::SetPRE_SCALEReq& msg
									) noexcept=0;

		/** This operation is invoked within the server thread
			whenever a GetPRE_SCALEReq is received.
		 */
		virtual void	request(	Oscl::Driver::NXP::PCA9685::Register::
									Req::Api::GetPRE_SCALEReq& msg
									) noexcept=0;

		/** This operation is invoked within the server thread
			whenever a SetALL_LED_ONReq is received.
		 */
		virtual void	request(	Oscl::Driver::NXP::PCA9685::Register::
									Req::Api::SetALL_LED_ONReq& msg
									) noexcept=0;

		/** This operation is invoked within the server thread
			whenever a GetALL_LED_ONReq is received.
		 */
		virtual void	request(	Oscl::Driver::NXP::PCA9685::Register::
									Req::Api::GetALL_LED_ONReq& msg
									) noexcept=0;

		/** This operation is invoked within the server thread
			whenever a SetLED_OFFReq is received.
		 */
		virtual void	request(	Oscl::Driver::NXP::PCA9685::Register::
									Req::Api::SetLED_OFFReq& msg
									) noexcept=0;

		/** This operation is invoked within the server thread
			whenever a GetLED_OFFReq is received.
		 */
		virtual void	request(	Oscl::Driver::NXP::PCA9685::Register::
									Req::Api::GetLED_OFFReq& msg
									) noexcept=0;

		/** This operation is invoked within the server thread
			whenever a SetLED_ONReq is received.
		 */
		virtual void	request(	Oscl::Driver::NXP::PCA9685::Register::
									Req::Api::SetLED_ONReq& msg
									) noexcept=0;

		/** This operation is invoked within the server thread
			whenever a GetLED_ONReq is received.
		 */
		virtual void	request(	Oscl::Driver::NXP::PCA9685::Register::
									Req::Api::GetLED_ONReq& msg
									) noexcept=0;

		/** This operation is invoked within the server thread
			whenever a SetALLCALLADDRReq is received.
		 */
		virtual void	request(	Oscl::Driver::NXP::PCA9685::Register::
									Req::Api::SetALLCALLADDRReq& msg
									) noexcept=0;

		/** This operation is invoked within the server thread
			whenever a GetALLCALLADDRReq is received.
		 */
		virtual void	request(	Oscl::Driver::NXP::PCA9685::Register::
									Req::Api::GetALLCALLADDRReq& msg
									) noexcept=0;

		/** This operation is invoked within the server thread
			whenever a SetSUBADR3Req is received.
		 */
		virtual void	request(	Oscl::Driver::NXP::PCA9685::Register::
									Req::Api::SetSUBADR3Req& msg
									) noexcept=0;

		/** This operation is invoked within the server thread
			whenever a GetSUBADR3Req is received.
		 */
		virtual void	request(	Oscl::Driver::NXP::PCA9685::Register::
									Req::Api::GetSUBADR3Req& msg
									) noexcept=0;

		/** This operation is invoked within the server thread
			whenever a SetSUBADR2Req is received.
		 */
		virtual void	request(	Oscl::Driver::NXP::PCA9685::Register::
									Req::Api::SetSUBADR2Req& msg
									) noexcept=0;

		/** This operation is invoked within the server thread
			whenever a GetSUBADR2Req is received.
		 */
		virtual void	request(	Oscl::Driver::NXP::PCA9685::Register::
									Req::Api::GetSUBADR2Req& msg
									) noexcept=0;

		/** This operation is invoked within the server thread
			whenever a SetSUBADR1Req is received.
		 */
		virtual void	request(	Oscl::Driver::NXP::PCA9685::Register::
									Req::Api::SetSUBADR1Req& msg
									) noexcept=0;

		/** This operation is invoked within the server thread
			whenever a GetSUBADR1Req is received.
		 */
		virtual void	request(	Oscl::Driver::NXP::PCA9685::Register::
									Req::Api::GetSUBADR1Req& msg
									) noexcept=0;

		/** This operation is invoked within the server thread
			whenever a SetMODE2Req is received.
		 */
		virtual void	request(	Oscl::Driver::NXP::PCA9685::Register::
									Req::Api::SetMODE2Req& msg
									) noexcept=0;

		/** This operation is invoked within the server thread
			whenever a GetMODE2Req is received.
		 */
		virtual void	request(	Oscl::Driver::NXP::PCA9685::Register::
									Req::Api::GetMODE2Req& msg
									) noexcept=0;

		/** This operation is invoked within the server thread
			whenever a SetMODE1Req is received.
		 */
		virtual void	request(	Oscl::Driver::NXP::PCA9685::Register::
									Req::Api::SetMODE1Req& msg
									) noexcept=0;

		/** This operation is invoked within the server thread
			whenever a GetMODE1Req is received.
		 */
		virtual void	request(	Oscl::Driver::NXP::PCA9685::Register::
									Req::Api::GetMODE1Req& msg
									) noexcept=0;

	};

}
}
}
}
}
}
#endif
