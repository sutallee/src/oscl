/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_driver_nxp_pca9685_register_composerh_
#define _oscl_driver_nxp_pca9685_register_composerh_

#include "api.h"

/** */
namespace Oscl {

/** */
namespace Driver {

/** */
namespace NXP {

/** */
namespace PCA9685 {

/** */
namespace Register {

/**	This template class implements a kind of GOF decorator
	pattern allows the context to employ composition instead
	of inheritance. Frequently, a context may need to implement
	more than one interface that may result in method name
	clashes. This template solves the problem by implementing
	the interface and then invoking member function pointers
	in the context instead. One instance of this object is
	created in the context for each interface of this type used
	in the context. Each instance is constructed such that it
	refers to different member functions within the context.
	The interface of this object is passed to any object that
	requires the interface. When the object invokes any of the
	operations of the API, the corresponding member function
	of the context will subsequently be invoked.
 */
template <class Context>
class Composer : public Api {
	private:
		/** A reference to the Context.
		 */
		Context&	_context;

	private:
		/**	
		 */
		void	(Context::*_setPRE_SCALE)(	
							uint8_t	value
							);

		/**	
		 */
		uint8_t	(Context::*_getPRE_SCALE)(	
							);

		/**	
		 */
		void	(Context::*_setALL_LED_ON)(	
							uint16_t	value
							);

		/**	
		 */
		uint16_t	(Context::*_getALL_LED_ON)(	
							);

		/**	
		 */
		void	(Context::*_setLED_OFF)(	
							uint8_t	channel,
							uint16_t	value
							);

		/**	
		 */
		uint16_t	(Context::*_getLED_OFF)(	
							uint8_t	channel
							);

		/**	
		 */
		void	(Context::*_setLED_ON)(	
							uint8_t	channel,
							uint16_t	value
							);

		/**	
		 */
		uint16_t	(Context::*_getLED_ON)(	
							uint8_t	channel
							);

		/**	
		 */
		void	(Context::*_setALLCALLADDR)(	
							uint8_t	value
							);

		/**	
		 */
		uint8_t	(Context::*_getALLCALLADDR)(	
							);

		/**	
		 */
		void	(Context::*_setSUBADR3)(	
							uint8_t	value
							);

		/**	
		 */
		uint8_t	(Context::*_getSUBADR3)(	
							);

		/**	
		 */
		void	(Context::*_setSUBADR2)(	
							uint8_t	value
							);

		/**	
		 */
		uint8_t	(Context::*_getSUBADR2)(	
							);

		/**	
		 */
		void	(Context::*_setSUBADR1)(	
							uint8_t	value
							);

		/**	
		 */
		uint8_t	(Context::*_getSUBADR1)(	
							);

		/**	
		 */
		void	(Context::*_setMODE2)(	
							uint8_t	value
							);

		/**	
		 */
		uint8_t	(Context::*_getMODE2)(	
							);

		/**	
		 */
		void	(Context::*_setMODE1)(	
							uint8_t	value
							);

		/**	
		 */
		uint8_t	(Context::*_getMODE1)(	
							);

	public:
		/** */
		Composer(
			Context&		context,
			void	(Context::*setPRE_SCALE)(	
								uint8_t	value
								),
			uint8_t	(Context::*getPRE_SCALE)(	
								),
			void	(Context::*setALL_LED_ON)(	
								uint16_t	value
								),
			uint16_t	(Context::*getALL_LED_ON)(	
								),
			void	(Context::*setLED_OFF)(	
								uint8_t	channel,
								uint16_t	value
								),
			uint16_t	(Context::*getLED_OFF)(	
								uint8_t	channel
								),
			void	(Context::*setLED_ON)(	
								uint8_t	channel,
								uint16_t	value
								),
			uint16_t	(Context::*getLED_ON)(	
								uint8_t	channel
								),
			void	(Context::*setALLCALLADDR)(	
								uint8_t	value
								),
			uint8_t	(Context::*getALLCALLADDR)(	
								),
			void	(Context::*setSUBADR3)(	
								uint8_t	value
								),
			uint8_t	(Context::*getSUBADR3)(	
								),
			void	(Context::*setSUBADR2)(	
								uint8_t	value
								),
			uint8_t	(Context::*getSUBADR2)(	
								),
			void	(Context::*setSUBADR1)(	
								uint8_t	value
								),
			uint8_t	(Context::*getSUBADR1)(	
								),
			void	(Context::*setMODE2)(	
								uint8_t	value
								),
			uint8_t	(Context::*getMODE2)(	
								),
			void	(Context::*setMODE1)(	
								uint8_t	value
								),
			uint8_t	(Context::*getMODE1)(	
								)
			) noexcept;

	private:
		/**	
		 */
		void	setPRE_SCALE(	
							uint8_t	value
							) noexcept;

		/**	
		 */
		uint8_t	getPRE_SCALE(	
							) noexcept;

		/**	
		 */
		void	setALL_LED_ON(	
							uint16_t	value
							) noexcept;

		/**	
		 */
		uint16_t	getALL_LED_ON(	
							) noexcept;

		/**	
		 */
		void	setLED_OFF(	
							uint8_t	channel,
							uint16_t	value
							) noexcept;

		/**	
		 */
		uint16_t	getLED_OFF(	
							uint8_t	channel
							) noexcept;

		/**	
		 */
		void	setLED_ON(	
							uint8_t	channel,
							uint16_t	value
							) noexcept;

		/**	
		 */
		uint16_t	getLED_ON(	
							uint8_t	channel
							) noexcept;

		/**	
		 */
		void	setALLCALLADDR(	
							uint8_t	value
							) noexcept;

		/**	
		 */
		uint8_t	getALLCALLADDR(	
							) noexcept;

		/**	
		 */
		void	setSUBADR3(	
							uint8_t	value
							) noexcept;

		/**	
		 */
		uint8_t	getSUBADR3(	
							) noexcept;

		/**	
		 */
		void	setSUBADR2(	
							uint8_t	value
							) noexcept;

		/**	
		 */
		uint8_t	getSUBADR2(	
							) noexcept;

		/**	
		 */
		void	setSUBADR1(	
							uint8_t	value
							) noexcept;

		/**	
		 */
		uint8_t	getSUBADR1(	
							) noexcept;

		/**	
		 */
		void	setMODE2(	
							uint8_t	value
							) noexcept;

		/**	
		 */
		uint8_t	getMODE2(	
							) noexcept;

		/**	
		 */
		void	setMODE1(	
							uint8_t	value
							) noexcept;

		/**	
		 */
		uint8_t	getMODE1(	
							) noexcept;

	};

template <class Context>
Composer<Context>::Composer(
			Context&		context,
			void	(Context::*setPRE_SCALE)(	
								uint8_t	value
								),
			uint8_t	(Context::*getPRE_SCALE)(	
								),
			void	(Context::*setALL_LED_ON)(	
								uint16_t	value
								),
			uint16_t	(Context::*getALL_LED_ON)(	
								),
			void	(Context::*setLED_OFF)(	
								uint8_t	channel,
								uint16_t	value
								),
			uint16_t	(Context::*getLED_OFF)(	
								uint8_t	channel
								),
			void	(Context::*setLED_ON)(	
								uint8_t	channel,
								uint16_t	value
								),
			uint16_t	(Context::*getLED_ON)(	
								uint8_t	channel
								),
			void	(Context::*setALLCALLADDR)(	
								uint8_t	value
								),
			uint8_t	(Context::*getALLCALLADDR)(	
								),
			void	(Context::*setSUBADR3)(	
								uint8_t	value
								),
			uint8_t	(Context::*getSUBADR3)(	
								),
			void	(Context::*setSUBADR2)(	
								uint8_t	value
								),
			uint8_t	(Context::*getSUBADR2)(	
								),
			void	(Context::*setSUBADR1)(	
								uint8_t	value
								),
			uint8_t	(Context::*getSUBADR1)(	
								),
			void	(Context::*setMODE2)(	
								uint8_t	value
								),
			uint8_t	(Context::*getMODE2)(	
								),
			void	(Context::*setMODE1)(	
								uint8_t	value
								),
			uint8_t	(Context::*getMODE1)(	
								)
			) noexcept:
		_context(context),
		_setPRE_SCALE(setPRE_SCALE),
		_getPRE_SCALE(getPRE_SCALE),
		_setALL_LED_ON(setALL_LED_ON),
		_getALL_LED_ON(getALL_LED_ON),
		_setLED_OFF(setLED_OFF),
		_getLED_OFF(getLED_OFF),
		_setLED_ON(setLED_ON),
		_getLED_ON(getLED_ON),
		_setALLCALLADDR(setALLCALLADDR),
		_getALLCALLADDR(getALLCALLADDR),
		_setSUBADR3(setSUBADR3),
		_getSUBADR3(getSUBADR3),
		_setSUBADR2(setSUBADR2),
		_getSUBADR2(getSUBADR2),
		_setSUBADR1(setSUBADR1),
		_getSUBADR1(getSUBADR1),
		_setMODE2(setMODE2),
		_getMODE2(getMODE2),
		_setMODE1(setMODE1),
		_getMODE1(getMODE1)
		{
	}

template <class Context>
void	Composer<Context>::setPRE_SCALE(	
							uint8_t	value
							) noexcept{

	return (_context.*_setPRE_SCALE)(
				value
				);
	}

template <class Context>
uint8_t	Composer<Context>::getPRE_SCALE(	
							) noexcept{

	return (_context.*_getPRE_SCALE)(
				);
	}

template <class Context>
void	Composer<Context>::setALL_LED_ON(	
							uint16_t	value
							) noexcept{

	return (_context.*_setALL_LED_ON)(
				value
				);
	}

template <class Context>
uint16_t	Composer<Context>::getALL_LED_ON(	
							) noexcept{

	return (_context.*_getALL_LED_ON)(
				);
	}

template <class Context>
void	Composer<Context>::setLED_OFF(	
							uint8_t	channel,
							uint16_t	value
							) noexcept{

	return (_context.*_setLED_OFF)(
				channel,
				value
				);
	}

template <class Context>
uint16_t	Composer<Context>::getLED_OFF(	
							uint8_t	channel
							) noexcept{

	return (_context.*_getLED_OFF)(
				channel
				);
	}

template <class Context>
void	Composer<Context>::setLED_ON(	
							uint8_t	channel,
							uint16_t	value
							) noexcept{

	return (_context.*_setLED_ON)(
				channel,
				value
				);
	}

template <class Context>
uint16_t	Composer<Context>::getLED_ON(	
							uint8_t	channel
							) noexcept{

	return (_context.*_getLED_ON)(
				channel
				);
	}

template <class Context>
void	Composer<Context>::setALLCALLADDR(	
							uint8_t	value
							) noexcept{

	return (_context.*_setALLCALLADDR)(
				value
				);
	}

template <class Context>
uint8_t	Composer<Context>::getALLCALLADDR(	
							) noexcept{

	return (_context.*_getALLCALLADDR)(
				);
	}

template <class Context>
void	Composer<Context>::setSUBADR3(	
							uint8_t	value
							) noexcept{

	return (_context.*_setSUBADR3)(
				value
				);
	}

template <class Context>
uint8_t	Composer<Context>::getSUBADR3(	
							) noexcept{

	return (_context.*_getSUBADR3)(
				);
	}

template <class Context>
void	Composer<Context>::setSUBADR2(	
							uint8_t	value
							) noexcept{

	return (_context.*_setSUBADR2)(
				value
				);
	}

template <class Context>
uint8_t	Composer<Context>::getSUBADR2(	
							) noexcept{

	return (_context.*_getSUBADR2)(
				);
	}

template <class Context>
void	Composer<Context>::setSUBADR1(	
							uint8_t	value
							) noexcept{

	return (_context.*_setSUBADR1)(
				value
				);
	}

template <class Context>
uint8_t	Composer<Context>::getSUBADR1(	
							) noexcept{

	return (_context.*_getSUBADR1)(
				);
	}

template <class Context>
void	Composer<Context>::setMODE2(	
							uint8_t	value
							) noexcept{

	return (_context.*_setMODE2)(
				value
				);
	}

template <class Context>
uint8_t	Composer<Context>::getMODE2(	
							) noexcept{

	return (_context.*_getMODE2)(
				);
	}

template <class Context>
void	Composer<Context>::setMODE1(	
							uint8_t	value
							) noexcept{

	return (_context.*_setMODE1)(
				value
				);
	}

template <class Context>
uint8_t	Composer<Context>::getMODE1(	
							) noexcept{

	return (_context.*_getMODE1)(
				);
	}

}
}
}
}
}
#endif
