/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_driver_nxp_pca9685_register_itc_respmemh_
#define _oscl_driver_nxp_pca9685_register_itc_respmemh_

#include "respapi.h"
#include "oscl/memory/block.h"

/**	These record types in this file describe blocks of memory
	that are large enough and appropriately aligned such that
	placement new operations creating instances of their
	corresponding objects can safely be performed. These records
	are useful to most clients that use the server's ITC
	interface asynchronously.
 */

/** */
namespace Oscl {

/** */
namespace Driver {

/** */
namespace NXP {

/** */
namespace PCA9685 {

/** */
namespace Register {

/** */
namespace Resp {

/**	This record describes a block of memory that is
	large enough (and properly aligned) to hold a
	SetPRE_SCALEResp and its corresponding Req::Api::SetPRE_SCALEPayload.
	Clients which use the corresponding server asynchronously
	frequently need to reservere/manage memory for for these
	ITC interactions and instances of this record are useful
	for that purpose.
 */
struct SetPRE_SCALEMem {
	/**	This is memory enough for the response message.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Resp::Api::SetPRE_SCALEResp)>	resp;

	/**	This is memory enough for the message payload.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Req::Api::SetPRE_SCALEPayload)>	payload;

	/**	This operation creates a response message using this
		 block of memory.
	 */
	Oscl::Driver::NXP::PCA9685::Register::Resp::Api::SetPRE_SCALEResp&
		build(	Oscl::Driver::NXP::PCA9685::Register::Req::Api::SAP&	sap,
				Oscl::Driver::NXP::PCA9685::Register::Resp::Api&		respApi,
				Oscl::Mt::Itc::PostMsgApi&		clientPapi,
				uint8_t	value
				) noexcept;

	};

/**	This record describes a block of memory that is
	large enough (and properly aligned) to hold a
	GetPRE_SCALEResp and its corresponding Req::Api::GetPRE_SCALEPayload.
	Clients which use the corresponding server asynchronously
	frequently need to reservere/manage memory for for these
	ITC interactions and instances of this record are useful
	for that purpose.
 */
struct GetPRE_SCALEMem {
	/**	This is memory enough for the response message.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Resp::Api::GetPRE_SCALEResp)>	resp;

	/**	This is memory enough for the message payload.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Req::Api::GetPRE_SCALEPayload)>	payload;

	/**	This operation creates a response message using this
		 block of memory.
	 */
	Oscl::Driver::NXP::PCA9685::Register::Resp::Api::GetPRE_SCALEResp&
		build(	Oscl::Driver::NXP::PCA9685::Register::Req::Api::SAP&	sap,
				Oscl::Driver::NXP::PCA9685::Register::Resp::Api&		respApi,
				Oscl::Mt::Itc::PostMsgApi&		clientPapi
				) noexcept;

	};

/**	This record describes a block of memory that is
	large enough (and properly aligned) to hold a
	SetALL_LED_ONResp and its corresponding Req::Api::SetALL_LED_ONPayload.
	Clients which use the corresponding server asynchronously
	frequently need to reservere/manage memory for for these
	ITC interactions and instances of this record are useful
	for that purpose.
 */
struct SetALL_LED_ONMem {
	/**	This is memory enough for the response message.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Resp::Api::SetALL_LED_ONResp)>	resp;

	/**	This is memory enough for the message payload.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Req::Api::SetALL_LED_ONPayload)>	payload;

	/**	This operation creates a response message using this
		 block of memory.
	 */
	Oscl::Driver::NXP::PCA9685::Register::Resp::Api::SetALL_LED_ONResp&
		build(	Oscl::Driver::NXP::PCA9685::Register::Req::Api::SAP&	sap,
				Oscl::Driver::NXP::PCA9685::Register::Resp::Api&		respApi,
				Oscl::Mt::Itc::PostMsgApi&		clientPapi,
				uint16_t	value
				) noexcept;

	};

/**	This record describes a block of memory that is
	large enough (and properly aligned) to hold a
	GetALL_LED_ONResp and its corresponding Req::Api::GetALL_LED_ONPayload.
	Clients which use the corresponding server asynchronously
	frequently need to reservere/manage memory for for these
	ITC interactions and instances of this record are useful
	for that purpose.
 */
struct GetALL_LED_ONMem {
	/**	This is memory enough for the response message.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Resp::Api::GetALL_LED_ONResp)>	resp;

	/**	This is memory enough for the message payload.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Req::Api::GetALL_LED_ONPayload)>	payload;

	/**	This operation creates a response message using this
		 block of memory.
	 */
	Oscl::Driver::NXP::PCA9685::Register::Resp::Api::GetALL_LED_ONResp&
		build(	Oscl::Driver::NXP::PCA9685::Register::Req::Api::SAP&	sap,
				Oscl::Driver::NXP::PCA9685::Register::Resp::Api&		respApi,
				Oscl::Mt::Itc::PostMsgApi&		clientPapi
				) noexcept;

	};

/**	This record describes a block of memory that is
	large enough (and properly aligned) to hold a
	SetLED_OFFResp and its corresponding Req::Api::SetLED_OFFPayload.
	Clients which use the corresponding server asynchronously
	frequently need to reservere/manage memory for for these
	ITC interactions and instances of this record are useful
	for that purpose.
 */
struct SetLED_OFFMem {
	/**	This is memory enough for the response message.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Resp::Api::SetLED_OFFResp)>	resp;

	/**	This is memory enough for the message payload.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Req::Api::SetLED_OFFPayload)>	payload;

	/**	This operation creates a response message using this
		 block of memory.
	 */
	Oscl::Driver::NXP::PCA9685::Register::Resp::Api::SetLED_OFFResp&
		build(	Oscl::Driver::NXP::PCA9685::Register::Req::Api::SAP&	sap,
				Oscl::Driver::NXP::PCA9685::Register::Resp::Api&		respApi,
				Oscl::Mt::Itc::PostMsgApi&		clientPapi,
				uint8_t	channel,
				uint16_t	value
				) noexcept;

	};

/**	This record describes a block of memory that is
	large enough (and properly aligned) to hold a
	GetLED_OFFResp and its corresponding Req::Api::GetLED_OFFPayload.
	Clients which use the corresponding server asynchronously
	frequently need to reservere/manage memory for for these
	ITC interactions and instances of this record are useful
	for that purpose.
 */
struct GetLED_OFFMem {
	/**	This is memory enough for the response message.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Resp::Api::GetLED_OFFResp)>	resp;

	/**	This is memory enough for the message payload.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Req::Api::GetLED_OFFPayload)>	payload;

	/**	This operation creates a response message using this
		 block of memory.
	 */
	Oscl::Driver::NXP::PCA9685::Register::Resp::Api::GetLED_OFFResp&
		build(	Oscl::Driver::NXP::PCA9685::Register::Req::Api::SAP&	sap,
				Oscl::Driver::NXP::PCA9685::Register::Resp::Api&		respApi,
				Oscl::Mt::Itc::PostMsgApi&		clientPapi,
				uint8_t	channel
				) noexcept;

	};

/**	This record describes a block of memory that is
	large enough (and properly aligned) to hold a
	SetLED_ONResp and its corresponding Req::Api::SetLED_ONPayload.
	Clients which use the corresponding server asynchronously
	frequently need to reservere/manage memory for for these
	ITC interactions and instances of this record are useful
	for that purpose.
 */
struct SetLED_ONMem {
	/**	This is memory enough for the response message.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Resp::Api::SetLED_ONResp)>	resp;

	/**	This is memory enough for the message payload.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Req::Api::SetLED_ONPayload)>	payload;

	/**	This operation creates a response message using this
		 block of memory.
	 */
	Oscl::Driver::NXP::PCA9685::Register::Resp::Api::SetLED_ONResp&
		build(	Oscl::Driver::NXP::PCA9685::Register::Req::Api::SAP&	sap,
				Oscl::Driver::NXP::PCA9685::Register::Resp::Api&		respApi,
				Oscl::Mt::Itc::PostMsgApi&		clientPapi,
				uint8_t	channel,
				uint16_t	value
				) noexcept;

	};

/**	This record describes a block of memory that is
	large enough (and properly aligned) to hold a
	GetLED_ONResp and its corresponding Req::Api::GetLED_ONPayload.
	Clients which use the corresponding server asynchronously
	frequently need to reservere/manage memory for for these
	ITC interactions and instances of this record are useful
	for that purpose.
 */
struct GetLED_ONMem {
	/**	This is memory enough for the response message.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Resp::Api::GetLED_ONResp)>	resp;

	/**	This is memory enough for the message payload.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Req::Api::GetLED_ONPayload)>	payload;

	/**	This operation creates a response message using this
		 block of memory.
	 */
	Oscl::Driver::NXP::PCA9685::Register::Resp::Api::GetLED_ONResp&
		build(	Oscl::Driver::NXP::PCA9685::Register::Req::Api::SAP&	sap,
				Oscl::Driver::NXP::PCA9685::Register::Resp::Api&		respApi,
				Oscl::Mt::Itc::PostMsgApi&		clientPapi,
				uint8_t	channel
				) noexcept;

	};

/**	This record describes a block of memory that is
	large enough (and properly aligned) to hold a
	SetALLCALLADDRResp and its corresponding Req::Api::SetALLCALLADDRPayload.
	Clients which use the corresponding server asynchronously
	frequently need to reservere/manage memory for for these
	ITC interactions and instances of this record are useful
	for that purpose.
 */
struct SetALLCALLADDRMem {
	/**	This is memory enough for the response message.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Resp::Api::SetALLCALLADDRResp)>	resp;

	/**	This is memory enough for the message payload.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Req::Api::SetALLCALLADDRPayload)>	payload;

	/**	This operation creates a response message using this
		 block of memory.
	 */
	Oscl::Driver::NXP::PCA9685::Register::Resp::Api::SetALLCALLADDRResp&
		build(	Oscl::Driver::NXP::PCA9685::Register::Req::Api::SAP&	sap,
				Oscl::Driver::NXP::PCA9685::Register::Resp::Api&		respApi,
				Oscl::Mt::Itc::PostMsgApi&		clientPapi,
				uint8_t	value
				) noexcept;

	};

/**	This record describes a block of memory that is
	large enough (and properly aligned) to hold a
	GetALLCALLADDRResp and its corresponding Req::Api::GetALLCALLADDRPayload.
	Clients which use the corresponding server asynchronously
	frequently need to reservere/manage memory for for these
	ITC interactions and instances of this record are useful
	for that purpose.
 */
struct GetALLCALLADDRMem {
	/**	This is memory enough for the response message.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Resp::Api::GetALLCALLADDRResp)>	resp;

	/**	This is memory enough for the message payload.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Req::Api::GetALLCALLADDRPayload)>	payload;

	/**	This operation creates a response message using this
		 block of memory.
	 */
	Oscl::Driver::NXP::PCA9685::Register::Resp::Api::GetALLCALLADDRResp&
		build(	Oscl::Driver::NXP::PCA9685::Register::Req::Api::SAP&	sap,
				Oscl::Driver::NXP::PCA9685::Register::Resp::Api&		respApi,
				Oscl::Mt::Itc::PostMsgApi&		clientPapi
				) noexcept;

	};

/**	This record describes a block of memory that is
	large enough (and properly aligned) to hold a
	SetSUBADR3Resp and its corresponding Req::Api::SetSUBADR3Payload.
	Clients which use the corresponding server asynchronously
	frequently need to reservere/manage memory for for these
	ITC interactions and instances of this record are useful
	for that purpose.
 */
struct SetSUBADR3Mem {
	/**	This is memory enough for the response message.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Resp::Api::SetSUBADR3Resp)>	resp;

	/**	This is memory enough for the message payload.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Req::Api::SetSUBADR3Payload)>	payload;

	/**	This operation creates a response message using this
		 block of memory.
	 */
	Oscl::Driver::NXP::PCA9685::Register::Resp::Api::SetSUBADR3Resp&
		build(	Oscl::Driver::NXP::PCA9685::Register::Req::Api::SAP&	sap,
				Oscl::Driver::NXP::PCA9685::Register::Resp::Api&		respApi,
				Oscl::Mt::Itc::PostMsgApi&		clientPapi,
				uint8_t	value
				) noexcept;

	};

/**	This record describes a block of memory that is
	large enough (and properly aligned) to hold a
	GetSUBADR3Resp and its corresponding Req::Api::GetSUBADR3Payload.
	Clients which use the corresponding server asynchronously
	frequently need to reservere/manage memory for for these
	ITC interactions and instances of this record are useful
	for that purpose.
 */
struct GetSUBADR3Mem {
	/**	This is memory enough for the response message.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Resp::Api::GetSUBADR3Resp)>	resp;

	/**	This is memory enough for the message payload.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Req::Api::GetSUBADR3Payload)>	payload;

	/**	This operation creates a response message using this
		 block of memory.
	 */
	Oscl::Driver::NXP::PCA9685::Register::Resp::Api::GetSUBADR3Resp&
		build(	Oscl::Driver::NXP::PCA9685::Register::Req::Api::SAP&	sap,
				Oscl::Driver::NXP::PCA9685::Register::Resp::Api&		respApi,
				Oscl::Mt::Itc::PostMsgApi&		clientPapi
				) noexcept;

	};

/**	This record describes a block of memory that is
	large enough (and properly aligned) to hold a
	SetSUBADR2Resp and its corresponding Req::Api::SetSUBADR2Payload.
	Clients which use the corresponding server asynchronously
	frequently need to reservere/manage memory for for these
	ITC interactions and instances of this record are useful
	for that purpose.
 */
struct SetSUBADR2Mem {
	/**	This is memory enough for the response message.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Resp::Api::SetSUBADR2Resp)>	resp;

	/**	This is memory enough for the message payload.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Req::Api::SetSUBADR2Payload)>	payload;

	/**	This operation creates a response message using this
		 block of memory.
	 */
	Oscl::Driver::NXP::PCA9685::Register::Resp::Api::SetSUBADR2Resp&
		build(	Oscl::Driver::NXP::PCA9685::Register::Req::Api::SAP&	sap,
				Oscl::Driver::NXP::PCA9685::Register::Resp::Api&		respApi,
				Oscl::Mt::Itc::PostMsgApi&		clientPapi,
				uint8_t	value
				) noexcept;

	};

/**	This record describes a block of memory that is
	large enough (and properly aligned) to hold a
	GetSUBADR2Resp and its corresponding Req::Api::GetSUBADR2Payload.
	Clients which use the corresponding server asynchronously
	frequently need to reservere/manage memory for for these
	ITC interactions and instances of this record are useful
	for that purpose.
 */
struct GetSUBADR2Mem {
	/**	This is memory enough for the response message.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Resp::Api::GetSUBADR2Resp)>	resp;

	/**	This is memory enough for the message payload.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Req::Api::GetSUBADR2Payload)>	payload;

	/**	This operation creates a response message using this
		 block of memory.
	 */
	Oscl::Driver::NXP::PCA9685::Register::Resp::Api::GetSUBADR2Resp&
		build(	Oscl::Driver::NXP::PCA9685::Register::Req::Api::SAP&	sap,
				Oscl::Driver::NXP::PCA9685::Register::Resp::Api&		respApi,
				Oscl::Mt::Itc::PostMsgApi&		clientPapi
				) noexcept;

	};

/**	This record describes a block of memory that is
	large enough (and properly aligned) to hold a
	SetSUBADR1Resp and its corresponding Req::Api::SetSUBADR1Payload.
	Clients which use the corresponding server asynchronously
	frequently need to reservere/manage memory for for these
	ITC interactions and instances of this record are useful
	for that purpose.
 */
struct SetSUBADR1Mem {
	/**	This is memory enough for the response message.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Resp::Api::SetSUBADR1Resp)>	resp;

	/**	This is memory enough for the message payload.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Req::Api::SetSUBADR1Payload)>	payload;

	/**	This operation creates a response message using this
		 block of memory.
	 */
	Oscl::Driver::NXP::PCA9685::Register::Resp::Api::SetSUBADR1Resp&
		build(	Oscl::Driver::NXP::PCA9685::Register::Req::Api::SAP&	sap,
				Oscl::Driver::NXP::PCA9685::Register::Resp::Api&		respApi,
				Oscl::Mt::Itc::PostMsgApi&		clientPapi,
				uint8_t	value
				) noexcept;

	};

/**	This record describes a block of memory that is
	large enough (and properly aligned) to hold a
	GetSUBADR1Resp and its corresponding Req::Api::GetSUBADR1Payload.
	Clients which use the corresponding server asynchronously
	frequently need to reservere/manage memory for for these
	ITC interactions and instances of this record are useful
	for that purpose.
 */
struct GetSUBADR1Mem {
	/**	This is memory enough for the response message.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Resp::Api::GetSUBADR1Resp)>	resp;

	/**	This is memory enough for the message payload.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Req::Api::GetSUBADR1Payload)>	payload;

	/**	This operation creates a response message using this
		 block of memory.
	 */
	Oscl::Driver::NXP::PCA9685::Register::Resp::Api::GetSUBADR1Resp&
		build(	Oscl::Driver::NXP::PCA9685::Register::Req::Api::SAP&	sap,
				Oscl::Driver::NXP::PCA9685::Register::Resp::Api&		respApi,
				Oscl::Mt::Itc::PostMsgApi&		clientPapi
				) noexcept;

	};

/**	This record describes a block of memory that is
	large enough (and properly aligned) to hold a
	SetMODE2Resp and its corresponding Req::Api::SetMODE2Payload.
	Clients which use the corresponding server asynchronously
	frequently need to reservere/manage memory for for these
	ITC interactions and instances of this record are useful
	for that purpose.
 */
struct SetMODE2Mem {
	/**	This is memory enough for the response message.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Resp::Api::SetMODE2Resp)>	resp;

	/**	This is memory enough for the message payload.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Req::Api::SetMODE2Payload)>	payload;

	/**	This operation creates a response message using this
		 block of memory.
	 */
	Oscl::Driver::NXP::PCA9685::Register::Resp::Api::SetMODE2Resp&
		build(	Oscl::Driver::NXP::PCA9685::Register::Req::Api::SAP&	sap,
				Oscl::Driver::NXP::PCA9685::Register::Resp::Api&		respApi,
				Oscl::Mt::Itc::PostMsgApi&		clientPapi,
				uint8_t	value
				) noexcept;

	};

/**	This record describes a block of memory that is
	large enough (and properly aligned) to hold a
	GetMODE2Resp and its corresponding Req::Api::GetMODE2Payload.
	Clients which use the corresponding server asynchronously
	frequently need to reservere/manage memory for for these
	ITC interactions and instances of this record are useful
	for that purpose.
 */
struct GetMODE2Mem {
	/**	This is memory enough for the response message.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Resp::Api::GetMODE2Resp)>	resp;

	/**	This is memory enough for the message payload.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Req::Api::GetMODE2Payload)>	payload;

	/**	This operation creates a response message using this
		 block of memory.
	 */
	Oscl::Driver::NXP::PCA9685::Register::Resp::Api::GetMODE2Resp&
		build(	Oscl::Driver::NXP::PCA9685::Register::Req::Api::SAP&	sap,
				Oscl::Driver::NXP::PCA9685::Register::Resp::Api&		respApi,
				Oscl::Mt::Itc::PostMsgApi&		clientPapi
				) noexcept;

	};

/**	This record describes a block of memory that is
	large enough (and properly aligned) to hold a
	SetMODE1Resp and its corresponding Req::Api::SetMODE1Payload.
	Clients which use the corresponding server asynchronously
	frequently need to reservere/manage memory for for these
	ITC interactions and instances of this record are useful
	for that purpose.
 */
struct SetMODE1Mem {
	/**	This is memory enough for the response message.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Resp::Api::SetMODE1Resp)>	resp;

	/**	This is memory enough for the message payload.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Req::Api::SetMODE1Payload)>	payload;

	/**	This operation creates a response message using this
		 block of memory.
	 */
	Oscl::Driver::NXP::PCA9685::Register::Resp::Api::SetMODE1Resp&
		build(	Oscl::Driver::NXP::PCA9685::Register::Req::Api::SAP&	sap,
				Oscl::Driver::NXP::PCA9685::Register::Resp::Api&		respApi,
				Oscl::Mt::Itc::PostMsgApi&		clientPapi,
				uint8_t	value
				) noexcept;

	};

/**	This record describes a block of memory that is
	large enough (and properly aligned) to hold a
	GetMODE1Resp and its corresponding Req::Api::GetMODE1Payload.
	Clients which use the corresponding server asynchronously
	frequently need to reservere/manage memory for for these
	ITC interactions and instances of this record are useful
	for that purpose.
 */
struct GetMODE1Mem {
	/**	This is memory enough for the response message.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Resp::Api::GetMODE1Resp)>	resp;

	/**	This is memory enough for the message payload.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Req::Api::GetMODE1Payload)>	payload;

	/**	This operation creates a response message using this
		 block of memory.
	 */
	Oscl::Driver::NXP::PCA9685::Register::Resp::Api::GetMODE1Resp&
		build(	Oscl::Driver::NXP::PCA9685::Register::Req::Api::SAP&	sap,
				Oscl::Driver::NXP::PCA9685::Register::Resp::Api&		respApi,
				Oscl::Mt::Itc::PostMsgApi&		clientPapi
				) noexcept;

	};

}
}
}
}
}
}
#endif
