/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_driver_nxp_pca9685_register_reqcomph_
#define _oscl_driver_nxp_pca9685_register_reqcomph_

#include "reqapi.h"

/** */
namespace Oscl {

/** */
namespace Driver {

/** */
namespace NXP {

/** */
namespace PCA9685 {

/** */
namespace Register {

/** */
namespace Req {

/**	This template class implements a kind of GOF decorator
	pattern allows the context to employ composition instead
	of inheritance. Frequently, a context may need to implement
	more than one interface that may result in method name
	clashes. This template solves the problem by implementing
	the interface and then invoking member function pointers
	in the context instead. One instance of this object is
	created in the context for each interface of this type used
	in the context. Each instance is constructed such that it
	refers to different member functions within the context.
	The interface of this object is passed to any object that
	requires the interface. When the object invokes any of the
	operations of the API, the corresponding member function
	of the context will subsequently be invoked.
 */
/**	This interface defines methods to access the PCA9685
	registers indpendent of the type of bus.
 */
template <class Context>
class Composer : public Api {
	private:
		/** A reference to the Context.
		 */
		Context&	_context;

	private:
		/**	Write the PRE_SCALE register
		 */
		void	(Context::*_SetPRE_SCALE)(Oscl::Driver::NXP::PCA9685::Register::Req::Api::SetPRE_SCALEReq& msg);

		/**	Read the PRE_SCALE register.
		 */
		void	(Context::*_GetPRE_SCALE)(Oscl::Driver::NXP::PCA9685::Register::Req::Api::GetPRE_SCALEReq& msg);

		/**	Write the ALL_LED_ON register
		 */
		void	(Context::*_SetALL_LED_ON)(Oscl::Driver::NXP::PCA9685::Register::Req::Api::SetALL_LED_ONReq& msg);

		/**	Read the ALL_LED_ON register.
		 */
		void	(Context::*_GetALL_LED_ON)(Oscl::Driver::NXP::PCA9685::Register::Req::Api::GetALL_LED_ONReq& msg);

		/**	Write the LED_OFF register
		 */
		void	(Context::*_SetLED_OFF)(Oscl::Driver::NXP::PCA9685::Register::Req::Api::SetLED_OFFReq& msg);

		/**	Read the LED_OFF register.
		 */
		void	(Context::*_GetLED_OFF)(Oscl::Driver::NXP::PCA9685::Register::Req::Api::GetLED_OFFReq& msg);

		/**	Write the LED_ON register
		 */
		void	(Context::*_SetLED_ON)(Oscl::Driver::NXP::PCA9685::Register::Req::Api::SetLED_ONReq& msg);

		/**	Read the LED_ON register.
		 */
		void	(Context::*_GetLED_ON)(Oscl::Driver::NXP::PCA9685::Register::Req::Api::GetLED_ONReq& msg);

		/**	Write the ALLCALLADDR register
		 */
		void	(Context::*_SetALLCALLADDR)(Oscl::Driver::NXP::PCA9685::Register::Req::Api::SetALLCALLADDRReq& msg);

		/**	Read the ALLCALLADDR register.
		 */
		void	(Context::*_GetALLCALLADDR)(Oscl::Driver::NXP::PCA9685::Register::Req::Api::GetALLCALLADDRReq& msg);

		/**	Write the SUBADR3 register
		 */
		void	(Context::*_SetSUBADR3)(Oscl::Driver::NXP::PCA9685::Register::Req::Api::SetSUBADR3Req& msg);

		/**	Read the SUBADR3 register.
		 */
		void	(Context::*_GetSUBADR3)(Oscl::Driver::NXP::PCA9685::Register::Req::Api::GetSUBADR3Req& msg);

		/**	Write the SUBADR2 register
		 */
		void	(Context::*_SetSUBADR2)(Oscl::Driver::NXP::PCA9685::Register::Req::Api::SetSUBADR2Req& msg);

		/**	Read the SUBADR2 register.
		 */
		void	(Context::*_GetSUBADR2)(Oscl::Driver::NXP::PCA9685::Register::Req::Api::GetSUBADR2Req& msg);

		/**	Write the SUBADR1 register
		 */
		void	(Context::*_SetSUBADR1)(Oscl::Driver::NXP::PCA9685::Register::Req::Api::SetSUBADR1Req& msg);

		/**	Read the SUBADR1 register.
		 */
		void	(Context::*_GetSUBADR1)(Oscl::Driver::NXP::PCA9685::Register::Req::Api::GetSUBADR1Req& msg);

		/**	Write the MODE2 register
		 */
		void	(Context::*_SetMODE2)(Oscl::Driver::NXP::PCA9685::Register::Req::Api::SetMODE2Req& msg);

		/**	Read the MODE2 register.
		 */
		void	(Context::*_GetMODE2)(Oscl::Driver::NXP::PCA9685::Register::Req::Api::GetMODE2Req& msg);

		/**	Write the MODE1 register
		 */
		void	(Context::*_SetMODE1)(Oscl::Driver::NXP::PCA9685::Register::Req::Api::SetMODE1Req& msg);

		/**	Read the MODE1 register.
		 */
		void	(Context::*_GetMODE1)(Oscl::Driver::NXP::PCA9685::Register::Req::Api::GetMODE1Req& msg);

	public:
		/** */
		Composer(
			Context&		context,
			void	(Context::*SetPRE_SCALE)(Oscl::Driver::NXP::PCA9685::Register::Req::Api::SetPRE_SCALEReq& msg),
			void	(Context::*GetPRE_SCALE)(Oscl::Driver::NXP::PCA9685::Register::Req::Api::GetPRE_SCALEReq& msg),
			void	(Context::*SetALL_LED_ON)(Oscl::Driver::NXP::PCA9685::Register::Req::Api::SetALL_LED_ONReq& msg),
			void	(Context::*GetALL_LED_ON)(Oscl::Driver::NXP::PCA9685::Register::Req::Api::GetALL_LED_ONReq& msg),
			void	(Context::*SetLED_OFF)(Oscl::Driver::NXP::PCA9685::Register::Req::Api::SetLED_OFFReq& msg),
			void	(Context::*GetLED_OFF)(Oscl::Driver::NXP::PCA9685::Register::Req::Api::GetLED_OFFReq& msg),
			void	(Context::*SetLED_ON)(Oscl::Driver::NXP::PCA9685::Register::Req::Api::SetLED_ONReq& msg),
			void	(Context::*GetLED_ON)(Oscl::Driver::NXP::PCA9685::Register::Req::Api::GetLED_ONReq& msg),
			void	(Context::*SetALLCALLADDR)(Oscl::Driver::NXP::PCA9685::Register::Req::Api::SetALLCALLADDRReq& msg),
			void	(Context::*GetALLCALLADDR)(Oscl::Driver::NXP::PCA9685::Register::Req::Api::GetALLCALLADDRReq& msg),
			void	(Context::*SetSUBADR3)(Oscl::Driver::NXP::PCA9685::Register::Req::Api::SetSUBADR3Req& msg),
			void	(Context::*GetSUBADR3)(Oscl::Driver::NXP::PCA9685::Register::Req::Api::GetSUBADR3Req& msg),
			void	(Context::*SetSUBADR2)(Oscl::Driver::NXP::PCA9685::Register::Req::Api::SetSUBADR2Req& msg),
			void	(Context::*GetSUBADR2)(Oscl::Driver::NXP::PCA9685::Register::Req::Api::GetSUBADR2Req& msg),
			void	(Context::*SetSUBADR1)(Oscl::Driver::NXP::PCA9685::Register::Req::Api::SetSUBADR1Req& msg),
			void	(Context::*GetSUBADR1)(Oscl::Driver::NXP::PCA9685::Register::Req::Api::GetSUBADR1Req& msg),
			void	(Context::*SetMODE2)(Oscl::Driver::NXP::PCA9685::Register::Req::Api::SetMODE2Req& msg),
			void	(Context::*GetMODE2)(Oscl::Driver::NXP::PCA9685::Register::Req::Api::GetMODE2Req& msg),
			void	(Context::*SetMODE1)(Oscl::Driver::NXP::PCA9685::Register::Req::Api::SetMODE1Req& msg),
			void	(Context::*GetMODE1)(Oscl::Driver::NXP::PCA9685::Register::Req::Api::GetMODE1Req& msg)
			) noexcept;

	private:
		/**	Write the PRE_SCALE register
		 */
		void	request(Oscl::Driver::NXP::PCA9685::Register::Req::Api::SetPRE_SCALEReq& msg) noexcept;

		/**	Read the PRE_SCALE register.
		 */
		void	request(Oscl::Driver::NXP::PCA9685::Register::Req::Api::GetPRE_SCALEReq& msg) noexcept;

		/**	Write the ALL_LED_ON register
		 */
		void	request(Oscl::Driver::NXP::PCA9685::Register::Req::Api::SetALL_LED_ONReq& msg) noexcept;

		/**	Read the ALL_LED_ON register.
		 */
		void	request(Oscl::Driver::NXP::PCA9685::Register::Req::Api::GetALL_LED_ONReq& msg) noexcept;

		/**	Write the LED_OFF register
		 */
		void	request(Oscl::Driver::NXP::PCA9685::Register::Req::Api::SetLED_OFFReq& msg) noexcept;

		/**	Read the LED_OFF register.
		 */
		void	request(Oscl::Driver::NXP::PCA9685::Register::Req::Api::GetLED_OFFReq& msg) noexcept;

		/**	Write the LED_ON register
		 */
		void	request(Oscl::Driver::NXP::PCA9685::Register::Req::Api::SetLED_ONReq& msg) noexcept;

		/**	Read the LED_ON register.
		 */
		void	request(Oscl::Driver::NXP::PCA9685::Register::Req::Api::GetLED_ONReq& msg) noexcept;

		/**	Write the ALLCALLADDR register
		 */
		void	request(Oscl::Driver::NXP::PCA9685::Register::Req::Api::SetALLCALLADDRReq& msg) noexcept;

		/**	Read the ALLCALLADDR register.
		 */
		void	request(Oscl::Driver::NXP::PCA9685::Register::Req::Api::GetALLCALLADDRReq& msg) noexcept;

		/**	Write the SUBADR3 register
		 */
		void	request(Oscl::Driver::NXP::PCA9685::Register::Req::Api::SetSUBADR3Req& msg) noexcept;

		/**	Read the SUBADR3 register.
		 */
		void	request(Oscl::Driver::NXP::PCA9685::Register::Req::Api::GetSUBADR3Req& msg) noexcept;

		/**	Write the SUBADR2 register
		 */
		void	request(Oscl::Driver::NXP::PCA9685::Register::Req::Api::SetSUBADR2Req& msg) noexcept;

		/**	Read the SUBADR2 register.
		 */
		void	request(Oscl::Driver::NXP::PCA9685::Register::Req::Api::GetSUBADR2Req& msg) noexcept;

		/**	Write the SUBADR1 register
		 */
		void	request(Oscl::Driver::NXP::PCA9685::Register::Req::Api::SetSUBADR1Req& msg) noexcept;

		/**	Read the SUBADR1 register.
		 */
		void	request(Oscl::Driver::NXP::PCA9685::Register::Req::Api::GetSUBADR1Req& msg) noexcept;

		/**	Write the MODE2 register
		 */
		void	request(Oscl::Driver::NXP::PCA9685::Register::Req::Api::SetMODE2Req& msg) noexcept;

		/**	Read the MODE2 register.
		 */
		void	request(Oscl::Driver::NXP::PCA9685::Register::Req::Api::GetMODE2Req& msg) noexcept;

		/**	Write the MODE1 register
		 */
		void	request(Oscl::Driver::NXP::PCA9685::Register::Req::Api::SetMODE1Req& msg) noexcept;

		/**	Read the MODE1 register.
		 */
		void	request(Oscl::Driver::NXP::PCA9685::Register::Req::Api::GetMODE1Req& msg) noexcept;

	};

template <class Context>
Composer<Context>::Composer(
			Context&		context,
			void	(Context::*SetPRE_SCALE)(Oscl::Driver::NXP::PCA9685::Register::Req::Api::SetPRE_SCALEReq& msg),
			void	(Context::*GetPRE_SCALE)(Oscl::Driver::NXP::PCA9685::Register::Req::Api::GetPRE_SCALEReq& msg),
			void	(Context::*SetALL_LED_ON)(Oscl::Driver::NXP::PCA9685::Register::Req::Api::SetALL_LED_ONReq& msg),
			void	(Context::*GetALL_LED_ON)(Oscl::Driver::NXP::PCA9685::Register::Req::Api::GetALL_LED_ONReq& msg),
			void	(Context::*SetLED_OFF)(Oscl::Driver::NXP::PCA9685::Register::Req::Api::SetLED_OFFReq& msg),
			void	(Context::*GetLED_OFF)(Oscl::Driver::NXP::PCA9685::Register::Req::Api::GetLED_OFFReq& msg),
			void	(Context::*SetLED_ON)(Oscl::Driver::NXP::PCA9685::Register::Req::Api::SetLED_ONReq& msg),
			void	(Context::*GetLED_ON)(Oscl::Driver::NXP::PCA9685::Register::Req::Api::GetLED_ONReq& msg),
			void	(Context::*SetALLCALLADDR)(Oscl::Driver::NXP::PCA9685::Register::Req::Api::SetALLCALLADDRReq& msg),
			void	(Context::*GetALLCALLADDR)(Oscl::Driver::NXP::PCA9685::Register::Req::Api::GetALLCALLADDRReq& msg),
			void	(Context::*SetSUBADR3)(Oscl::Driver::NXP::PCA9685::Register::Req::Api::SetSUBADR3Req& msg),
			void	(Context::*GetSUBADR3)(Oscl::Driver::NXP::PCA9685::Register::Req::Api::GetSUBADR3Req& msg),
			void	(Context::*SetSUBADR2)(Oscl::Driver::NXP::PCA9685::Register::Req::Api::SetSUBADR2Req& msg),
			void	(Context::*GetSUBADR2)(Oscl::Driver::NXP::PCA9685::Register::Req::Api::GetSUBADR2Req& msg),
			void	(Context::*SetSUBADR1)(Oscl::Driver::NXP::PCA9685::Register::Req::Api::SetSUBADR1Req& msg),
			void	(Context::*GetSUBADR1)(Oscl::Driver::NXP::PCA9685::Register::Req::Api::GetSUBADR1Req& msg),
			void	(Context::*SetMODE2)(Oscl::Driver::NXP::PCA9685::Register::Req::Api::SetMODE2Req& msg),
			void	(Context::*GetMODE2)(Oscl::Driver::NXP::PCA9685::Register::Req::Api::GetMODE2Req& msg),
			void	(Context::*SetMODE1)(Oscl::Driver::NXP::PCA9685::Register::Req::Api::SetMODE1Req& msg),
			void	(Context::*GetMODE1)(Oscl::Driver::NXP::PCA9685::Register::Req::Api::GetMODE1Req& msg)
			) noexcept:
		_context(context),
		_SetPRE_SCALE(SetPRE_SCALE),
		_GetPRE_SCALE(GetPRE_SCALE),
		_SetALL_LED_ON(SetALL_LED_ON),
		_GetALL_LED_ON(GetALL_LED_ON),
		_SetLED_OFF(SetLED_OFF),
		_GetLED_OFF(GetLED_OFF),
		_SetLED_ON(SetLED_ON),
		_GetLED_ON(GetLED_ON),
		_SetALLCALLADDR(SetALLCALLADDR),
		_GetALLCALLADDR(GetALLCALLADDR),
		_SetSUBADR3(SetSUBADR3),
		_GetSUBADR3(GetSUBADR3),
		_SetSUBADR2(SetSUBADR2),
		_GetSUBADR2(GetSUBADR2),
		_SetSUBADR1(SetSUBADR1),
		_GetSUBADR1(GetSUBADR1),
		_SetMODE2(SetMODE2),
		_GetMODE2(GetMODE2),
		_SetMODE1(SetMODE1),
		_GetMODE1(GetMODE1)
		{
	}

template <class Context>
void	Composer<Context>::request(Oscl::Driver::NXP::PCA9685::Register::Req::Api::SetPRE_SCALEReq& msg) noexcept{
	(_context.*_SetPRE_SCALE)(msg);
	}

template <class Context>
void	Composer<Context>::request(Oscl::Driver::NXP::PCA9685::Register::Req::Api::GetPRE_SCALEReq& msg) noexcept{
	(_context.*_GetPRE_SCALE)(msg);
	}

template <class Context>
void	Composer<Context>::request(Oscl::Driver::NXP::PCA9685::Register::Req::Api::SetALL_LED_ONReq& msg) noexcept{
	(_context.*_SetALL_LED_ON)(msg);
	}

template <class Context>
void	Composer<Context>::request(Oscl::Driver::NXP::PCA9685::Register::Req::Api::GetALL_LED_ONReq& msg) noexcept{
	(_context.*_GetALL_LED_ON)(msg);
	}

template <class Context>
void	Composer<Context>::request(Oscl::Driver::NXP::PCA9685::Register::Req::Api::SetLED_OFFReq& msg) noexcept{
	(_context.*_SetLED_OFF)(msg);
	}

template <class Context>
void	Composer<Context>::request(Oscl::Driver::NXP::PCA9685::Register::Req::Api::GetLED_OFFReq& msg) noexcept{
	(_context.*_GetLED_OFF)(msg);
	}

template <class Context>
void	Composer<Context>::request(Oscl::Driver::NXP::PCA9685::Register::Req::Api::SetLED_ONReq& msg) noexcept{
	(_context.*_SetLED_ON)(msg);
	}

template <class Context>
void	Composer<Context>::request(Oscl::Driver::NXP::PCA9685::Register::Req::Api::GetLED_ONReq& msg) noexcept{
	(_context.*_GetLED_ON)(msg);
	}

template <class Context>
void	Composer<Context>::request(Oscl::Driver::NXP::PCA9685::Register::Req::Api::SetALLCALLADDRReq& msg) noexcept{
	(_context.*_SetALLCALLADDR)(msg);
	}

template <class Context>
void	Composer<Context>::request(Oscl::Driver::NXP::PCA9685::Register::Req::Api::GetALLCALLADDRReq& msg) noexcept{
	(_context.*_GetALLCALLADDR)(msg);
	}

template <class Context>
void	Composer<Context>::request(Oscl::Driver::NXP::PCA9685::Register::Req::Api::SetSUBADR3Req& msg) noexcept{
	(_context.*_SetSUBADR3)(msg);
	}

template <class Context>
void	Composer<Context>::request(Oscl::Driver::NXP::PCA9685::Register::Req::Api::GetSUBADR3Req& msg) noexcept{
	(_context.*_GetSUBADR3)(msg);
	}

template <class Context>
void	Composer<Context>::request(Oscl::Driver::NXP::PCA9685::Register::Req::Api::SetSUBADR2Req& msg) noexcept{
	(_context.*_SetSUBADR2)(msg);
	}

template <class Context>
void	Composer<Context>::request(Oscl::Driver::NXP::PCA9685::Register::Req::Api::GetSUBADR2Req& msg) noexcept{
	(_context.*_GetSUBADR2)(msg);
	}

template <class Context>
void	Composer<Context>::request(Oscl::Driver::NXP::PCA9685::Register::Req::Api::SetSUBADR1Req& msg) noexcept{
	(_context.*_SetSUBADR1)(msg);
	}

template <class Context>
void	Composer<Context>::request(Oscl::Driver::NXP::PCA9685::Register::Req::Api::GetSUBADR1Req& msg) noexcept{
	(_context.*_GetSUBADR1)(msg);
	}

template <class Context>
void	Composer<Context>::request(Oscl::Driver::NXP::PCA9685::Register::Req::Api::SetMODE2Req& msg) noexcept{
	(_context.*_SetMODE2)(msg);
	}

template <class Context>
void	Composer<Context>::request(Oscl::Driver::NXP::PCA9685::Register::Req::Api::GetMODE2Req& msg) noexcept{
	(_context.*_GetMODE2)(msg);
	}

template <class Context>
void	Composer<Context>::request(Oscl::Driver::NXP::PCA9685::Register::Req::Api::SetMODE1Req& msg) noexcept{
	(_context.*_SetMODE1)(msg);
	}

template <class Context>
void	Composer<Context>::request(Oscl::Driver::NXP::PCA9685::Register::Req::Api::GetMODE1Req& msg) noexcept{
	(_context.*_GetMODE1)(msg);
	}

}
}
}
}
}
}
#endif
