/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <new>
#include "respmem.h"

using namespace Oscl::Driver::NXP::PCA9685::Register;

Resp::Api::SetPRE_SCALEResp&
	Resp::SetPRE_SCALEMem::build(	Req::Api::SAP&			sap,
							Resp::Api&				respApi,
							Oscl::Mt::Itc::PostMsgApi&	clientPapi,
							uint8_t	value
							) noexcept{
	Req::Api::SetPRE_SCALEPayload*
		p	=	new(&payload)
				Req::Api::SetPRE_SCALEPayload(
							value
							);
	Resp::Api::SetPRE_SCALEResp*
		r	=	new(&resp)
			Resp::Api::SetPRE_SCALEResp(	sap.getReqApi(),
									respApi,
									clientPapi,
									*p
									);

	return *r;
	}

Resp::Api::GetPRE_SCALEResp&
	Resp::GetPRE_SCALEMem::build(	Req::Api::SAP&			sap,
							Resp::Api&				respApi,
							Oscl::Mt::Itc::PostMsgApi&	clientPapi
							) noexcept{
	Req::Api::GetPRE_SCALEPayload*
		p	=	new(&payload)
				Req::Api::GetPRE_SCALEPayload(
);
	Resp::Api::GetPRE_SCALEResp*
		r	=	new(&resp)
			Resp::Api::GetPRE_SCALEResp(	sap.getReqApi(),
									respApi,
									clientPapi,
									*p
									);

	return *r;
	}

Resp::Api::SetALL_LED_ONResp&
	Resp::SetALL_LED_ONMem::build(	Req::Api::SAP&			sap,
							Resp::Api&				respApi,
							Oscl::Mt::Itc::PostMsgApi&	clientPapi,
							uint16_t	value
							) noexcept{
	Req::Api::SetALL_LED_ONPayload*
		p	=	new(&payload)
				Req::Api::SetALL_LED_ONPayload(
							value
							);
	Resp::Api::SetALL_LED_ONResp*
		r	=	new(&resp)
			Resp::Api::SetALL_LED_ONResp(	sap.getReqApi(),
									respApi,
									clientPapi,
									*p
									);

	return *r;
	}

Resp::Api::GetALL_LED_ONResp&
	Resp::GetALL_LED_ONMem::build(	Req::Api::SAP&			sap,
							Resp::Api&				respApi,
							Oscl::Mt::Itc::PostMsgApi&	clientPapi
							) noexcept{
	Req::Api::GetALL_LED_ONPayload*
		p	=	new(&payload)
				Req::Api::GetALL_LED_ONPayload(
);
	Resp::Api::GetALL_LED_ONResp*
		r	=	new(&resp)
			Resp::Api::GetALL_LED_ONResp(	sap.getReqApi(),
									respApi,
									clientPapi,
									*p
									);

	return *r;
	}

Resp::Api::SetLED_OFFResp&
	Resp::SetLED_OFFMem::build(	Req::Api::SAP&			sap,
							Resp::Api&				respApi,
							Oscl::Mt::Itc::PostMsgApi&	clientPapi,
							uint8_t	channel,
							uint16_t	value
							) noexcept{
	Req::Api::SetLED_OFFPayload*
		p	=	new(&payload)
				Req::Api::SetLED_OFFPayload(
							channel,
							value
							);
	Resp::Api::SetLED_OFFResp*
		r	=	new(&resp)
			Resp::Api::SetLED_OFFResp(	sap.getReqApi(),
									respApi,
									clientPapi,
									*p
									);

	return *r;
	}

Resp::Api::GetLED_OFFResp&
	Resp::GetLED_OFFMem::build(	Req::Api::SAP&			sap,
							Resp::Api&				respApi,
							Oscl::Mt::Itc::PostMsgApi&	clientPapi,
							uint8_t	channel
							) noexcept{
	Req::Api::GetLED_OFFPayload*
		p	=	new(&payload)
				Req::Api::GetLED_OFFPayload(
							channel
							);
	Resp::Api::GetLED_OFFResp*
		r	=	new(&resp)
			Resp::Api::GetLED_OFFResp(	sap.getReqApi(),
									respApi,
									clientPapi,
									*p
									);

	return *r;
	}

Resp::Api::SetLED_ONResp&
	Resp::SetLED_ONMem::build(	Req::Api::SAP&			sap,
							Resp::Api&				respApi,
							Oscl::Mt::Itc::PostMsgApi&	clientPapi,
							uint8_t	channel,
							uint16_t	value
							) noexcept{
	Req::Api::SetLED_ONPayload*
		p	=	new(&payload)
				Req::Api::SetLED_ONPayload(
							channel,
							value
							);
	Resp::Api::SetLED_ONResp*
		r	=	new(&resp)
			Resp::Api::SetLED_ONResp(	sap.getReqApi(),
									respApi,
									clientPapi,
									*p
									);

	return *r;
	}

Resp::Api::GetLED_ONResp&
	Resp::GetLED_ONMem::build(	Req::Api::SAP&			sap,
							Resp::Api&				respApi,
							Oscl::Mt::Itc::PostMsgApi&	clientPapi,
							uint8_t	channel
							) noexcept{
	Req::Api::GetLED_ONPayload*
		p	=	new(&payload)
				Req::Api::GetLED_ONPayload(
							channel
							);
	Resp::Api::GetLED_ONResp*
		r	=	new(&resp)
			Resp::Api::GetLED_ONResp(	sap.getReqApi(),
									respApi,
									clientPapi,
									*p
									);

	return *r;
	}

Resp::Api::SetALLCALLADDRResp&
	Resp::SetALLCALLADDRMem::build(	Req::Api::SAP&			sap,
							Resp::Api&				respApi,
							Oscl::Mt::Itc::PostMsgApi&	clientPapi,
							uint8_t	value
							) noexcept{
	Req::Api::SetALLCALLADDRPayload*
		p	=	new(&payload)
				Req::Api::SetALLCALLADDRPayload(
							value
							);
	Resp::Api::SetALLCALLADDRResp*
		r	=	new(&resp)
			Resp::Api::SetALLCALLADDRResp(	sap.getReqApi(),
									respApi,
									clientPapi,
									*p
									);

	return *r;
	}

Resp::Api::GetALLCALLADDRResp&
	Resp::GetALLCALLADDRMem::build(	Req::Api::SAP&			sap,
							Resp::Api&				respApi,
							Oscl::Mt::Itc::PostMsgApi&	clientPapi
							) noexcept{
	Req::Api::GetALLCALLADDRPayload*
		p	=	new(&payload)
				Req::Api::GetALLCALLADDRPayload(
);
	Resp::Api::GetALLCALLADDRResp*
		r	=	new(&resp)
			Resp::Api::GetALLCALLADDRResp(	sap.getReqApi(),
									respApi,
									clientPapi,
									*p
									);

	return *r;
	}

Resp::Api::SetSUBADR3Resp&
	Resp::SetSUBADR3Mem::build(	Req::Api::SAP&			sap,
							Resp::Api&				respApi,
							Oscl::Mt::Itc::PostMsgApi&	clientPapi,
							uint8_t	value
							) noexcept{
	Req::Api::SetSUBADR3Payload*
		p	=	new(&payload)
				Req::Api::SetSUBADR3Payload(
							value
							);
	Resp::Api::SetSUBADR3Resp*
		r	=	new(&resp)
			Resp::Api::SetSUBADR3Resp(	sap.getReqApi(),
									respApi,
									clientPapi,
									*p
									);

	return *r;
	}

Resp::Api::GetSUBADR3Resp&
	Resp::GetSUBADR3Mem::build(	Req::Api::SAP&			sap,
							Resp::Api&				respApi,
							Oscl::Mt::Itc::PostMsgApi&	clientPapi
							) noexcept{
	Req::Api::GetSUBADR3Payload*
		p	=	new(&payload)
				Req::Api::GetSUBADR3Payload(
);
	Resp::Api::GetSUBADR3Resp*
		r	=	new(&resp)
			Resp::Api::GetSUBADR3Resp(	sap.getReqApi(),
									respApi,
									clientPapi,
									*p
									);

	return *r;
	}

Resp::Api::SetSUBADR2Resp&
	Resp::SetSUBADR2Mem::build(	Req::Api::SAP&			sap,
							Resp::Api&				respApi,
							Oscl::Mt::Itc::PostMsgApi&	clientPapi,
							uint8_t	value
							) noexcept{
	Req::Api::SetSUBADR2Payload*
		p	=	new(&payload)
				Req::Api::SetSUBADR2Payload(
							value
							);
	Resp::Api::SetSUBADR2Resp*
		r	=	new(&resp)
			Resp::Api::SetSUBADR2Resp(	sap.getReqApi(),
									respApi,
									clientPapi,
									*p
									);

	return *r;
	}

Resp::Api::GetSUBADR2Resp&
	Resp::GetSUBADR2Mem::build(	Req::Api::SAP&			sap,
							Resp::Api&				respApi,
							Oscl::Mt::Itc::PostMsgApi&	clientPapi
							) noexcept{
	Req::Api::GetSUBADR2Payload*
		p	=	new(&payload)
				Req::Api::GetSUBADR2Payload(
);
	Resp::Api::GetSUBADR2Resp*
		r	=	new(&resp)
			Resp::Api::GetSUBADR2Resp(	sap.getReqApi(),
									respApi,
									clientPapi,
									*p
									);

	return *r;
	}

Resp::Api::SetSUBADR1Resp&
	Resp::SetSUBADR1Mem::build(	Req::Api::SAP&			sap,
							Resp::Api&				respApi,
							Oscl::Mt::Itc::PostMsgApi&	clientPapi,
							uint8_t	value
							) noexcept{
	Req::Api::SetSUBADR1Payload*
		p	=	new(&payload)
				Req::Api::SetSUBADR1Payload(
							value
							);
	Resp::Api::SetSUBADR1Resp*
		r	=	new(&resp)
			Resp::Api::SetSUBADR1Resp(	sap.getReqApi(),
									respApi,
									clientPapi,
									*p
									);

	return *r;
	}

Resp::Api::GetSUBADR1Resp&
	Resp::GetSUBADR1Mem::build(	Req::Api::SAP&			sap,
							Resp::Api&				respApi,
							Oscl::Mt::Itc::PostMsgApi&	clientPapi
							) noexcept{
	Req::Api::GetSUBADR1Payload*
		p	=	new(&payload)
				Req::Api::GetSUBADR1Payload(
);
	Resp::Api::GetSUBADR1Resp*
		r	=	new(&resp)
			Resp::Api::GetSUBADR1Resp(	sap.getReqApi(),
									respApi,
									clientPapi,
									*p
									);

	return *r;
	}

Resp::Api::SetMODE2Resp&
	Resp::SetMODE2Mem::build(	Req::Api::SAP&			sap,
							Resp::Api&				respApi,
							Oscl::Mt::Itc::PostMsgApi&	clientPapi,
							uint8_t	value
							) noexcept{
	Req::Api::SetMODE2Payload*
		p	=	new(&payload)
				Req::Api::SetMODE2Payload(
							value
							);
	Resp::Api::SetMODE2Resp*
		r	=	new(&resp)
			Resp::Api::SetMODE2Resp(	sap.getReqApi(),
									respApi,
									clientPapi,
									*p
									);

	return *r;
	}

Resp::Api::GetMODE2Resp&
	Resp::GetMODE2Mem::build(	Req::Api::SAP&			sap,
							Resp::Api&				respApi,
							Oscl::Mt::Itc::PostMsgApi&	clientPapi
							) noexcept{
	Req::Api::GetMODE2Payload*
		p	=	new(&payload)
				Req::Api::GetMODE2Payload(
);
	Resp::Api::GetMODE2Resp*
		r	=	new(&resp)
			Resp::Api::GetMODE2Resp(	sap.getReqApi(),
									respApi,
									clientPapi,
									*p
									);

	return *r;
	}

Resp::Api::SetMODE1Resp&
	Resp::SetMODE1Mem::build(	Req::Api::SAP&			sap,
							Resp::Api&				respApi,
							Oscl::Mt::Itc::PostMsgApi&	clientPapi,
							uint8_t	value
							) noexcept{
	Req::Api::SetMODE1Payload*
		p	=	new(&payload)
				Req::Api::SetMODE1Payload(
							value
							);
	Resp::Api::SetMODE1Resp*
		r	=	new(&resp)
			Resp::Api::SetMODE1Resp(	sap.getReqApi(),
									respApi,
									clientPapi,
									*p
									);

	return *r;
	}

Resp::Api::GetMODE1Resp&
	Resp::GetMODE1Mem::build(	Req::Api::SAP&			sap,
							Resp::Api&				respApi,
							Oscl::Mt::Itc::PostMsgApi&	clientPapi
							) noexcept{
	Req::Api::GetMODE1Payload*
		p	=	new(&payload)
				Req::Api::GetMODE1Payload(
);
	Resp::Api::GetMODE1Resp*
		r	=	new(&resp)
			Resp::Api::GetMODE1Resp(	sap.getReqApi(),
									respApi,
									clientPapi,
									*p
									);

	return *r;
	}

