/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "sync.h"
#include "oscl/mt/itc/mbox/syncrh.h"

using namespace Oscl::Driver::NXP::PCA9685::Register;

Sync::Sync(	Oscl::Mt::Itc::PostMsgApi&	myPapi,
			Req::Api&				reqApi
			) noexcept:
		_sap(reqApi,myPapi)
		{
	}

Req::Api::SAP&	Sync::getSAP() noexcept{
	return _sap;
	}

void	Sync::setPRE_SCALE(	
						uint8_t	value
						) noexcept{

	Oscl::Mt::Itc::SyncReturnHandler		srh;

	Req::Api::SetPRE_SCALEPayload		payload(
									value
									);

	Req::Api::SetPRE_SCALEReq	req(	_sap.getReqApi(),
									payload,
									srh
									);

	_sap.postSync(req);
	}

uint8_t	Sync::getPRE_SCALE(	
						) noexcept{

	Oscl::Mt::Itc::SyncReturnHandler		srh;

	Req::Api::GetPRE_SCALEPayload		payload;

	Req::Api::GetPRE_SCALEReq	req(	_sap.getReqApi(),
									payload,
									srh
									);

	_sap.postSync(req);

	return payload._value;
	}

void	Sync::setALL_LED_ON(	
						uint16_t	value
						) noexcept{

	Oscl::Mt::Itc::SyncReturnHandler		srh;

	Req::Api::SetALL_LED_ONPayload		payload(
									value
									);

	Req::Api::SetALL_LED_ONReq	req(	_sap.getReqApi(),
									payload,
									srh
									);

	_sap.postSync(req);
	}

uint16_t	Sync::getALL_LED_ON(	
						) noexcept{

	Oscl::Mt::Itc::SyncReturnHandler		srh;

	Req::Api::GetALL_LED_ONPayload		payload;

	Req::Api::GetALL_LED_ONReq	req(	_sap.getReqApi(),
									payload,
									srh
									);

	_sap.postSync(req);

	return payload._value;
	}

void	Sync::setLED_OFF(	
						uint8_t	channel,

						uint16_t	value
						) noexcept{

	Oscl::Mt::Itc::SyncReturnHandler		srh;

	Req::Api::SetLED_OFFPayload		payload(
									channel,

									value
									);

	Req::Api::SetLED_OFFReq	req(	_sap.getReqApi(),
									payload,
									srh
									);

	_sap.postSync(req);
	}

uint16_t	Sync::getLED_OFF(	
						uint8_t	channel
						) noexcept{

	Oscl::Mt::Itc::SyncReturnHandler		srh;

	Req::Api::GetLED_OFFPayload		payload(
									channel
									);

	Req::Api::GetLED_OFFReq	req(	_sap.getReqApi(),
									payload,
									srh
									);

	_sap.postSync(req);

	return payload._value;
	}

void	Sync::setLED_ON(	
						uint8_t	channel,

						uint16_t	value
						) noexcept{

	Oscl::Mt::Itc::SyncReturnHandler		srh;

	Req::Api::SetLED_ONPayload		payload(
									channel,

									value
									);

	Req::Api::SetLED_ONReq	req(	_sap.getReqApi(),
									payload,
									srh
									);

	_sap.postSync(req);
	}

uint16_t	Sync::getLED_ON(	
						uint8_t	channel
						) noexcept{

	Oscl::Mt::Itc::SyncReturnHandler		srh;

	Req::Api::GetLED_ONPayload		payload(
									channel
									);

	Req::Api::GetLED_ONReq	req(	_sap.getReqApi(),
									payload,
									srh
									);

	_sap.postSync(req);

	return payload._value;
	}

void	Sync::setALLCALLADDR(	
						uint8_t	value
						) noexcept{

	Oscl::Mt::Itc::SyncReturnHandler		srh;

	Req::Api::SetALLCALLADDRPayload		payload(
									value
									);

	Req::Api::SetALLCALLADDRReq	req(	_sap.getReqApi(),
									payload,
									srh
									);

	_sap.postSync(req);
	}

uint8_t	Sync::getALLCALLADDR(	
						) noexcept{

	Oscl::Mt::Itc::SyncReturnHandler		srh;

	Req::Api::GetALLCALLADDRPayload		payload;

	Req::Api::GetALLCALLADDRReq	req(	_sap.getReqApi(),
									payload,
									srh
									);

	_sap.postSync(req);

	return payload._value;
	}

void	Sync::setSUBADR3(	
						uint8_t	value
						) noexcept{

	Oscl::Mt::Itc::SyncReturnHandler		srh;

	Req::Api::SetSUBADR3Payload		payload(
									value
									);

	Req::Api::SetSUBADR3Req	req(	_sap.getReqApi(),
									payload,
									srh
									);

	_sap.postSync(req);
	}

uint8_t	Sync::getSUBADR3(	
						) noexcept{

	Oscl::Mt::Itc::SyncReturnHandler		srh;

	Req::Api::GetSUBADR3Payload		payload;

	Req::Api::GetSUBADR3Req	req(	_sap.getReqApi(),
									payload,
									srh
									);

	_sap.postSync(req);

	return payload._value;
	}

void	Sync::setSUBADR2(	
						uint8_t	value
						) noexcept{

	Oscl::Mt::Itc::SyncReturnHandler		srh;

	Req::Api::SetSUBADR2Payload		payload(
									value
									);

	Req::Api::SetSUBADR2Req	req(	_sap.getReqApi(),
									payload,
									srh
									);

	_sap.postSync(req);
	}

uint8_t	Sync::getSUBADR2(	
						) noexcept{

	Oscl::Mt::Itc::SyncReturnHandler		srh;

	Req::Api::GetSUBADR2Payload		payload;

	Req::Api::GetSUBADR2Req	req(	_sap.getReqApi(),
									payload,
									srh
									);

	_sap.postSync(req);

	return payload._value;
	}

void	Sync::setSUBADR1(	
						uint8_t	value
						) noexcept{

	Oscl::Mt::Itc::SyncReturnHandler		srh;

	Req::Api::SetSUBADR1Payload		payload(
									value
									);

	Req::Api::SetSUBADR1Req	req(	_sap.getReqApi(),
									payload,
									srh
									);

	_sap.postSync(req);
	}

uint8_t	Sync::getSUBADR1(	
						) noexcept{

	Oscl::Mt::Itc::SyncReturnHandler		srh;

	Req::Api::GetSUBADR1Payload		payload;

	Req::Api::GetSUBADR1Req	req(	_sap.getReqApi(),
									payload,
									srh
									);

	_sap.postSync(req);

	return payload._value;
	}

void	Sync::setMODE2(	
						uint8_t	value
						) noexcept{

	Oscl::Mt::Itc::SyncReturnHandler		srh;

	Req::Api::SetMODE2Payload		payload(
									value
									);

	Req::Api::SetMODE2Req	req(	_sap.getReqApi(),
									payload,
									srh
									);

	_sap.postSync(req);
	}

uint8_t	Sync::getMODE2(	
						) noexcept{

	Oscl::Mt::Itc::SyncReturnHandler		srh;

	Req::Api::GetMODE2Payload		payload;

	Req::Api::GetMODE2Req	req(	_sap.getReqApi(),
									payload,
									srh
									);

	_sap.postSync(req);

	return payload._value;
	}

void	Sync::setMODE1(	
						uint8_t	value
						) noexcept{

	Oscl::Mt::Itc::SyncReturnHandler		srh;

	Req::Api::SetMODE1Payload		payload(
									value
									);

	Req::Api::SetMODE1Req	req(	_sap.getReqApi(),
									payload,
									srh
									);

	_sap.postSync(req);
	}

uint8_t	Sync::getMODE1(	
						) noexcept{

	Oscl::Mt::Itc::SyncReturnHandler		srh;

	Req::Api::GetMODE1Payload		payload;

	Req::Api::GetMODE1Req	req(	_sap.getReqApi(),
									payload,
									srh
									);

	_sap.postSync(req);

	return payload._value;
	}

