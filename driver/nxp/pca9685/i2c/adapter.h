/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/


#ifndef _oscl_driver_nxp_pca9685_i2c_adapterh_
#define _oscl_driver_nxp_pca9685_i2c_adapterh_

#include "oscl/driver/nxp/pca9685/register/api.h"
#include "oscl/i2c/device/api.h"

/** */
namespace Oscl {

/** */
namespace Driver {

/** */
namespace NXP {

/** */
namespace PCA9685 {

/** */
namespace I2C {

/** */
class Adapter :
	public Oscl::Driver::NXP::PCA9685::Register::Api
	{
	private:
		/** */
		Oscl::I2C::Device::Api&	_api;

	public:
		/** */
		Adapter(
			Oscl::I2C::Device::Api&	api
			) noexcept;

	private:
		/** */
		void	selectThenWriteReg8(uint8_t regNum,uint8_t value) noexcept;

		/** */
		void	selectThenWriteReg16(uint8_t regNum,uint16_t value) noexcept;

		/** */
		uint8_t	selectThenReadReg8(uint8_t regNum) noexcept;

		/** */
		uint16_t	selectThenReadReg16(uint8_t regNum) noexcept;

	private:
		/**	*/
		void	setTestMode(uint8_t value) noexcept;

		/**	*/
		uint8_t	getTestMode() noexcept;

		/**	*/
		void	setPRE_SCALE(uint8_t value) noexcept;

		/**	*/
		uint8_t	getPRE_SCALE() noexcept;

		/**	*/
		void	setALL_LED_ON(uint16_t value) noexcept;

		/**	*/
		uint16_t	getALL_LED_ON() noexcept;

		/**	*/
		void	setLED_OFF(	
					uint8_t		channel,
					uint16_t	value
					) noexcept;

		/**	*/
		uint16_t	getLED_OFF(uint8_t channel) noexcept;

		/**	*/
		void	setLED_ON(
					uint8_t	channel,
					uint16_t	value
					) noexcept;

		/**	*/
		uint16_t	getLED_ON(uint8_t channel) noexcept;

		/**	*/
		void	setALLCALLADDR(uint8_t	value) noexcept;

		/**	*/
		uint8_t	getALLCALLADDR() noexcept;

		/**	*/
		void	setSUBADR3(uint8_t value) noexcept;

		/**	*/
		uint8_t	getSUBADR3() noexcept;

		/**	*/
		void	setSUBADR2(uint8_t value) noexcept;

		/**	*/
		uint8_t	getSUBADR2() noexcept;

		/**	*/
		void	setSUBADR1(uint8_t value) noexcept;

		/**	*/
		uint8_t	getSUBADR1() noexcept;

		/**	*/
		void	setMODE2(uint8_t value) noexcept;

		/**	*/
		uint8_t	getMODE2() noexcept;

		/**	*/
		void	setMODE1(uint8_t value) noexcept;

		/**	*/
		uint8_t	getMODE1() noexcept;
	};

}
}
}
}
}

#endif
