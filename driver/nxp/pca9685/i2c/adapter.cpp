/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/


#include "adapter.h"
#include "oscl/hw/nxp/pca9685/reg.h"

using namespace Oscl::Driver::NXP::PCA9685::I2C;

Adapter::Adapter(
			Oscl::I2C::Device::Api&	api
			) noexcept:
			_api(api)
		{
	}

void	Adapter::selectThenWriteReg8(uint8_t regNum,uint8_t value) noexcept{
	uint8_t	buff[2];
	buff[0]	= regNum;
	buff[1]	= value;

	_api.write(&buff,sizeof(buff));
	}

void	Adapter::selectThenWriteReg16(uint8_t regNum,uint16_t value) noexcept{
	uint8_t	buff[3];
	buff[0]	= regNum;
	buff[1]	= value;
	buff[2]	= value >> 8;

	_api.write(&buff,sizeof(buff));
	}

uint8_t	Adapter::selectThenReadReg8(uint8_t regNum) noexcept{

	_api.write(&regNum,1);

	uint8_t	value;
	_api.read(&value,1);

	return value;
	}

uint16_t	Adapter::selectThenReadReg16(uint8_t regNum) noexcept{

	_api.write(&regNum,1);

	uint8_t	buffer[2];
	_api.read(buffer,sizeof(buffer));

	uint16_t
	value	= buffer[1];
	value	<<= 8;
	value	|= buffer[0];

	return value;
	}

void	Adapter::setTestMode(uint8_t value) noexcept{
	selectThenWriteReg8(
		Oscl::HW::NXP::PCA9685::TestMode,
		value
		);
	}

uint8_t	Adapter::getTestMode() noexcept{
	return selectThenReadReg8(Oscl::HW::NXP::PCA9685::TestMode);
	}

void	Adapter::setPRE_SCALE(uint8_t value) noexcept{
	selectThenWriteReg8(
		Oscl::HW::NXP::PCA9685::PRE_SCALE,
		value
		);
	}

uint8_t	Adapter::getPRE_SCALE() noexcept{
	return selectThenReadReg8(Oscl::HW::NXP::PCA9685::PRE_SCALE);
	}

void	Adapter::setALL_LED_ON(uint16_t value) noexcept{
	selectThenWriteReg16(
		Oscl::HW::NXP::PCA9685::ALL_LED_ON_L,
		value
		);
	}

uint16_t	Adapter::getALL_LED_ON() noexcept{
	return selectThenReadReg16(Oscl::HW::NXP::PCA9685::ALL_LED_ON_L);
	}

void	Adapter::setLED_OFF(	
					uint8_t		channel,
					uint16_t	value
					) noexcept{
	uint8_t
	regNum	=
			Oscl::HW::NXP::PCA9685::LED0
		+	(channel * 4)
		+	Oscl::HW::NXP::PCA9685::LED_OFF_L
		;

	selectThenWriteReg16(
		regNum,
		value
		);
	}

uint16_t	Adapter::getLED_OFF(uint8_t channel) noexcept{
	uint8_t
	regNum	=
			Oscl::HW::NXP::PCA9685::LED0
		+	(channel * 4)
		+	Oscl::HW::NXP::PCA9685::LED_OFF_L
		;

	return selectThenReadReg16(regNum);
	}

void	Adapter::setLED_ON(
					uint8_t	channel,
					uint16_t	value
					) noexcept{
	uint8_t
	regNum	=
			Oscl::HW::NXP::PCA9685::LED0
		+	(channel * 4)
		+	Oscl::HW::NXP::PCA9685::LED_ON_L
		;

	selectThenWriteReg16(
		regNum,
		value
		);
	}

uint16_t	Adapter::getLED_ON(uint8_t channel) noexcept{
	uint8_t
	regNum	=
			Oscl::HW::NXP::PCA9685::LED0
		+	(channel * 4)
		+	Oscl::HW::NXP::PCA9685::LED_ON_L
		;

	return selectThenReadReg16(regNum);
	}

void	Adapter::setALLCALLADDR(uint8_t	value) noexcept{
	selectThenWriteReg8(
		Oscl::HW::NXP::PCA9685::ALLCALLADDR,
		value
		);
	}

uint8_t	Adapter::getALLCALLADDR() noexcept{
	return selectThenReadReg8(Oscl::HW::NXP::PCA9685::ALLCALLADDR);
	}

void	Adapter::setSUBADR3(uint8_t value) noexcept{
	selectThenWriteReg8(
		Oscl::HW::NXP::PCA9685::SUBADR3,
		value
		);
	}

uint8_t	Adapter::getSUBADR3() noexcept{
	return selectThenReadReg8(Oscl::HW::NXP::PCA9685::SUBADR3);
	}

void	Adapter::setSUBADR2(uint8_t value) noexcept{
	selectThenWriteReg8(
		Oscl::HW::NXP::PCA9685::SUBADR2,
		value
		);
	}

uint8_t	Adapter::getSUBADR2() noexcept{
	return selectThenReadReg8(Oscl::HW::NXP::PCA9685::SUBADR2);
	}

void	Adapter::setSUBADR1(uint8_t value) noexcept{
	selectThenWriteReg8(
		Oscl::HW::NXP::PCA9685::SUBADR1,
		value
		);
	}

uint8_t	Adapter::getSUBADR1() noexcept{
	return selectThenReadReg8(Oscl::HW::NXP::PCA9685::SUBADR1);
	}

void	Adapter::setMODE2(uint8_t value) noexcept{
	selectThenWriteReg8(
		Oscl::HW::NXP::PCA9685::MODE2,
		value
		);
	}

uint8_t	Adapter::getMODE2() noexcept{
	return selectThenReadReg8(Oscl::HW::NXP::PCA9685::MODE2);
	}

void	Adapter::setMODE1(uint8_t value) noexcept{
	selectThenWriteReg8(
		Oscl::HW::NXP::PCA9685::MODE1,
		value
		);
	}

uint8_t	Adapter::getMODE1() noexcept{
	return selectThenReadReg8(Oscl::HW::NXP::PCA9685::MODE1);
	}

