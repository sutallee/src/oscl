/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/


#ifndef _oscl_driver_nxp_pca9685_serviceh_
#define _oscl_driver_nxp_pca9685_serviceh_

#include "part.h"
#include "oscl/mt/itc/srv/close.h"

/** */
namespace Oscl {

/** */
namespace Driver {

/** */
namespace NXP {

/** */
namespace PCA9685 {

/** */
class Service : private Oscl::Mt::Itc::Srv::Close::Req::Api {
	private:
		/** */
		Oscl::Mt::Itc::Srv::CloseSync		_closeSync;

		/** */
		Oscl::Driver::NXP::PCA9685::Part		_part;

	public:
		/** */
		Service(
			Oscl::Mt::Itc::PostMsgApi&	papi,
			Oscl::Driver::NXP::
			PCA9685::Register::Api&		api,
			uint8_t						preScaler
			) noexcept;

		/** */
		Oscl::Mt::Itc::Srv::OpenSyncApi&	getOpenSyncApi() noexcept;

		/** */
		Oscl::Mt::Itc::Srv::CloseSyncApi&	getCloseSyncApi() noexcept;

		/** */
        Oscl::Mt::Itc::Srv::Open::Req::Api::SAP&    getOpenSAP() noexcept;

		/** */
        Oscl::Mt::Itc::Srv::Close::Req::Api::SAP&   getCloseSAP() noexcept;

		/** */
		Oscl::PWM::Discrete::Api&	getChannel0SyncApi() noexcept;

		/** */
		Oscl::PWM::Discrete::Req::Api::SAP&	getChannel0SAP() noexcept;

		/** */
		Oscl::PWM::Discrete::Api&	getChannel1SyncApi() noexcept;

		/** */
		Oscl::PWM::Discrete::Req::Api::SAP&	getChannel1SAP() noexcept;

		/** */
		Oscl::PWM::Discrete::Api&	getChannel2SyncApi() noexcept;

		/** */
		Oscl::PWM::Discrete::Req::Api::SAP&	getChannel2SAP() noexcept;

		/** */
		Oscl::PWM::Discrete::Api&	getChannel3SyncApi() noexcept;

		/** */
		Oscl::PWM::Discrete::Req::Api::SAP&	getChannel3SAP() noexcept;

		/** */
		Oscl::PWM::Discrete::Api&	getChannel4SyncApi() noexcept;

		/** */
		Oscl::PWM::Discrete::Req::Api::SAP&	getChannel4SAP() noexcept;

		/** */
		Oscl::PWM::Discrete::Api&	getChannel5SyncApi() noexcept;

		/** */
		Oscl::PWM::Discrete::Req::Api::SAP&	getChannel5SAP() noexcept;

		/** */
		Oscl::PWM::Discrete::Api&	getChannel6SyncApi() noexcept;

		/** */
		Oscl::PWM::Discrete::Req::Api::SAP&	getChannel6SAP() noexcept;

		/** */
		Oscl::PWM::Discrete::Api&	getChannel7SyncApi() noexcept;

		/** */
		Oscl::PWM::Discrete::Req::Api::SAP&	getChannel7SAP() noexcept;

		/** */
		Oscl::PWM::Discrete::Api&	getChannel8SyncApi() noexcept;

		/** */
		Oscl::PWM::Discrete::Req::Api::SAP&	getChannel8SAP() noexcept;

		/** */
		Oscl::PWM::Discrete::Api&	getChannel9SyncApi() noexcept;

		/** */
		Oscl::PWM::Discrete::Req::Api::SAP&	getChannel9SAP() noexcept;

		/** */
		Oscl::PWM::Discrete::Api&	getChannel10SyncApi() noexcept;

		/** */
		Oscl::PWM::Discrete::Req::Api::SAP&	getChannel10SAP() noexcept;

		/** */
		Oscl::PWM::Discrete::Api&	getChannel11SyncApi() noexcept;

		/** */
		Oscl::PWM::Discrete::Req::Api::SAP&	getChannel11SAP() noexcept;

		/** */
		Oscl::PWM::Discrete::Api&	getChannel12SyncApi() noexcept;

		/** */
		Oscl::PWM::Discrete::Req::Api::SAP&	getChannel12SAP() noexcept;

		/** */
		Oscl::PWM::Discrete::Api&	getChannel13SyncApi() noexcept;

		/** */
		Oscl::PWM::Discrete::Req::Api::SAP&	getChannel13SAP() noexcept;

		/** */
		Oscl::PWM::Discrete::Api&	getChannel14SyncApi() noexcept;

		/** */
		Oscl::PWM::Discrete::Req::Api::SAP&	getChannel14SAP() noexcept;

		/** */
		Oscl::PWM::Discrete::Api&	getChannel15SyncApi() noexcept;

		/** */
		Oscl::PWM::Discrete::Req::Api::SAP&	getChannel15SAP() noexcept;

		/** */
		void	start() noexcept;

	private: // Oscl::Mt::Itc::Srv::Open::Req::Api
		/** */
		void	request(Oscl::Mt::Itc::Srv::Open::Req::Api::OpenReq& msg) noexcept;

		/** */
		void	request(Oscl::Mt::Itc::Srv::Close::Req::Api::CloseReq& msg) noexcept;

	};

}
}
}
}

#endif
