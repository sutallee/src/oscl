/*
   Copyright (C) 2006 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_driver_hitachi_hd44780_apih_
#define _oscl_driver_hitachi_hd44780_apih_

/** */
namespace Oscl {
/** */
namespace Hitachi {
/** */
namespace HD44780 {
/** */
namespace Register {

/** */
class Api {
	public:
		/** */
		virtual ~Api() {}

		/** */
		virtual void	clearDisplay() noexcept=0;
		/** */
		virtual void	homeCursor() noexcept=0;
		/** */
		virtual void	setEntryMode(	bool	increment,
										bool	shift
										) noexcept=0;
		/** */
		virtual void	display(	bool	displayOn,
									bool	cursorOn,
									bool	blinkOn
									) noexcept=0;
		/** */
		virtual void	shift(	bool	shiftOn,
								bool	shiftRight
								) noexcept=0;
		/** */
		virtual void	functionSet(	bool	eightBit,
										bool	dualLine,
										bool	dots5x10
										) noexcept=0;
		/** */
		virtual void	cgRamAddressSet(unsigned char address) noexcept=0;

		/** */
		virtual void	ddRamAddressSet(unsigned char address) noexcept=0;

		/** */
		virtual bool	isBusy() const noexcept=0;

		/** */
		virtual unsigned char	addressCounter() const noexcept=0;

		/** */
		virtual unsigned char	readData() const noexcept=0;

		/** */
		virtual void	writeData(unsigned char data) noexcept=0;
	};

}
}
}
}

#endif
