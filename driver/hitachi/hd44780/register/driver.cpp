/*
   Copyright (C) 2006 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "driver.h"
#include "oscl/hw/hitachi/hd44780/hd44780reg.h"
#include "oscl/cpu/sync.h"

using namespace Oscl::Hitachi::HD44780::Register;

Driver::Driver(	volatile unsigned char&	commandStatusReg,
				volatile unsigned char&	dataReg
				) noexcept:
		_commandStatusReg(commandStatusReg),
		_dataReg(dataReg)
		{
	}

void	Driver::clearDisplay() noexcept{
	_commandStatusReg	= Oscl::HW::Hitachi::HD44780::Command::DisplayClear;
	OsclCpuInOrderMemoryAccessBarrier();
	}

void	Driver::homeCursor() noexcept{
	_commandStatusReg	= Oscl::HW::Hitachi::HD44780::Command::CursorHome;
	OsclCpuInOrderMemoryAccessBarrier();
	}

void	Driver::setEntryMode(	bool	increment,
								bool	shift
								) noexcept{
	Oscl::HW::Hitachi::HD44780::Command::Reg
	idMask	= (increment)?
				Oscl::HW::Hitachi::HD44780::Command::EntryMode::ID::ValueMask_Increment:
				Oscl::HW::Hitachi::HD44780::Command::EntryMode::ID::ValueMask_Increment
				;
	Oscl::HW::Hitachi::HD44780::Command::Reg
	shiftMask	= (Oscl::HW::Hitachi::HD44780::Command::Reg)
				(shift)?
				(Oscl::HW::Hitachi::HD44780::Command::Reg)Oscl::HW::Hitachi::HD44780::Command::EntryMode::S::ValueMask_ShiftOn:
				(Oscl::HW::Hitachi::HD44780::Command::Reg)Oscl::HW::Hitachi::HD44780::Command::EntryMode::S::ValueMask_ShiftOff
				;
	_commandStatusReg	= 		Oscl::HW::Hitachi::HD44780::Command::EntryMode::Opcode::ValueMask_Opcode
							|	idMask
							|	shiftMask
							;
	OsclCpuInOrderMemoryAccessBarrier();
	}

void	Driver::display(	bool	displayOn,
							bool	cursorOn,
							bool	blinkOn
							) noexcept{
	Oscl::HW::Hitachi::HD44780::Command::Reg
	displayMask	=
				(Oscl::HW::Hitachi::HD44780::Command::Reg)
				(displayOn)?
				(Oscl::HW::Hitachi::HD44780::Command::Reg)Oscl::HW::Hitachi::HD44780::Command::DisplayOnOff::D::ValueMask_DisplayOn:
				(Oscl::HW::Hitachi::HD44780::Command::Reg)Oscl::HW::Hitachi::HD44780::Command::DisplayOnOff::D::ValueMask_DisplayOff
				;
	Oscl::HW::Hitachi::HD44780::Command::Reg
	cursorMask	=
				(Oscl::HW::Hitachi::HD44780::Command::Reg)
				(cursorOn)?
				(Oscl::HW::Hitachi::HD44780::Command::Reg) Oscl::HW::Hitachi::HD44780::Command::DisplayOnOff::C::ValueMask_CursorOn:
				(Oscl::HW::Hitachi::HD44780::Command::Reg) Oscl::HW::Hitachi::HD44780::Command::DisplayOnOff::C::ValueMask_CursorOff
				;
	Oscl::HW::Hitachi::HD44780::Command::Reg
	blinkMask	= (blinkOn)?
				(Oscl::HW::Hitachi::HD44780::Command::Reg) Oscl::HW::Hitachi::HD44780::Command::DisplayOnOff::B::Value_CursorBlinkOn:
				(Oscl::HW::Hitachi::HD44780::Command::Reg) Oscl::HW::Hitachi::HD44780::Command::DisplayOnOff::B::Value_CursorBlinkOff
				;
	_commandStatusReg	= 		Oscl::HW::Hitachi::HD44780::Command::DisplayOnOff::Opcode::ValueMask_Opcode
							|	displayMask
							|	cursorMask
							|	blinkMask
							;
	OsclCpuInOrderMemoryAccessBarrier();
	}

void	Driver::shift(	bool	shiftOn,
						bool	shiftRight
						) noexcept{
	Oscl::HW::Hitachi::HD44780::Command::Reg
	shiftOnMask	=
				(Oscl::HW::Hitachi::HD44780::Command::Reg)
				(shiftOn)?
				(Oscl::HW::Hitachi::HD44780::Command::Reg) Oscl::HW::Hitachi::HD44780::Command::CursorShift::SC::ValueMask_ShiftDisplay:
				(Oscl::HW::Hitachi::HD44780::Command::Reg) Oscl::HW::Hitachi::HD44780::Command::CursorShift::SC::ValueMask_DontShiftDisplay
				;
	Oscl::HW::Hitachi::HD44780::Command::Reg
	shiftRightMask	=
				(Oscl::HW::Hitachi::HD44780::Command::Reg)
				(shiftRight)?
				(Oscl::HW::Hitachi::HD44780::Command::Reg) Oscl::HW::Hitachi::HD44780::Command::CursorShift::RL::ValueMask_ShiftRight:
				(Oscl::HW::Hitachi::HD44780::Command::Reg) Oscl::HW::Hitachi::HD44780::Command::CursorShift::RL::ValueMask_ShiftLeft
				;
	_commandStatusReg	= 		Oscl::HW::Hitachi::HD44780::Command::CursorShift::Opcode::ValueMask_Opcode
							|	shiftOnMask
							|	shiftRightMask
							;
	OsclCpuInOrderMemoryAccessBarrier();
	}

void	Driver::functionSet(	bool	eightBit,
								bool	dualLine,
								bool	dots5x10
								) noexcept{
	Oscl::HW::Hitachi::HD44780::Command::Reg
	sizeMask	=
				(Oscl::HW::Hitachi::HD44780::Command::Reg)
				(eightBit)?
				(Oscl::HW::Hitachi::HD44780::Command::Reg) Oscl::HW::Hitachi::HD44780::Command::FunctionSet::DL::ValueMask_EightBit:
				(Oscl::HW::Hitachi::HD44780::Command::Reg) Oscl::HW::Hitachi::HD44780::Command::FunctionSet::DL::ValueMask_FourBit
				;
	Oscl::HW::Hitachi::HD44780::Command::Reg
	lineMask	=
				(Oscl::HW::Hitachi::HD44780::Command::Reg)
				(dualLine)?
				(Oscl::HW::Hitachi::HD44780::Command::Reg) Oscl::HW::Hitachi::HD44780::Command::FunctionSet::N::ValueMask_DualLine:
				(Oscl::HW::Hitachi::HD44780::Command::Reg) Oscl::HW::Hitachi::HD44780::Command::FunctionSet::N::ValueMask_SingleLine
				;
	Oscl::HW::Hitachi::HD44780::Command::Reg
	dotMask	=
				(Oscl::HW::Hitachi::HD44780::Command::Reg)
				(dots5x10)?
				(Oscl::HW::Hitachi::HD44780::Command::Reg) Oscl::HW::Hitachi::HD44780::Command::FunctionSet::F::ValueMask_Dots5x10:
				(Oscl::HW::Hitachi::HD44780::Command::Reg) Oscl::HW::Hitachi::HD44780::Command::FunctionSet::F::ValueMask_Dots5x8
				;
	_commandStatusReg	= 		Oscl::HW::Hitachi::HD44780::Command::FunctionSet::Opcode::ValueMask_Opcode
							|	sizeMask
							|	lineMask
							|	dotMask
							;
	OsclCpuInOrderMemoryAccessBarrier();
	}

void	Driver::cgRamAddressSet(unsigned char address) noexcept{
	Oscl::HW::Hitachi::HD44780::Command::Reg
	addrMask	=		(address << Oscl::HW::Hitachi::HD44780::Command::CgRamAddressSet::ACG::Lsb)
					&	Oscl::HW::Hitachi::HD44780::Command::CgRamAddressSet::ACG::FieldMask
					;
	_commandStatusReg	= 		Oscl::HW::Hitachi::HD44780::Command::CgRamAddressSet::Opcode::ValueMask_Opcode
							|	addrMask
							;
	OsclCpuInOrderMemoryAccessBarrier();
	}

void	Driver::ddRamAddressSet(unsigned char address) noexcept{
	Oscl::HW::Hitachi::HD44780::Command::Reg
	addrMask	=		(address << Oscl::HW::Hitachi::HD44780::Command::DdRamAddressSet::ADD::Lsb)
					&	Oscl::HW::Hitachi::HD44780::Command::DdRamAddressSet::ADD::FieldMask
					;
	_commandStatusReg	= 	(Oscl::HW::Hitachi::HD44780::Command::Reg)(
								Oscl::HW::Hitachi::HD44780::Command::DdRamAddressSet::Opcode::ValueMask_Opcode
							|	addrMask
							);
	OsclCpuInOrderMemoryAccessBarrier();
	}

bool	Driver::isBusy() const noexcept{
	return 		(_commandStatusReg & Oscl::HW::Hitachi::HD44780::Status::BusyFlag::FieldMask)
			==	Oscl::HW::Hitachi::HD44780::Status::BusyFlag::ValueMask_Busy
			;
	}

unsigned char	Driver::addressCounter() const noexcept{
	return 		(_commandStatusReg & Oscl::HW::Hitachi::HD44780::Status::AddressCounter::FieldMask)
			>>	Oscl::HW::Hitachi::HD44780::Status::AddressCounter::Lsb
			;
	}

unsigned char	Driver::readData() const noexcept{
	return _dataReg;
	}

void	Driver::writeData(unsigned char data) noexcept{
	_dataReg	= data;
	OsclCpuInOrderMemoryAccessBarrier();
	}

