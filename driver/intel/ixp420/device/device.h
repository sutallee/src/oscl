/*
   Copyright (C) 2006 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_intel_ixp420_device_deviceh_
#define _oscl_drv_intel_ixp420_device_deviceh_

#include "api.h"
#include "oscl/driver/intel/ixp4xx/ost/driver.h"
#include "oscl/driver/intel/ixp4xx/intr/drv.h"
#include "oscl/extalloc/driver.h"
#include "oscl/extalloc/mt/mutex.h"
#include "oscl/mt/mutex/simple.h"
#include "oscl/hw/intel/ixp4xx/imm.h"
#include "oscl/driver/intel/ixp4xx/intr/pic.h"
#include "oscl/driver/intel/ixp4xx/intr/interrupt.h"
#include "oscl/driver/intel/ixp4xx/intr/map.h"
#include "oscl/driver/intel/ixp4xx/gpio/drv.h"

/** */
namespace Oscl {
/** */
namespace Intel {
/** */
namespace IXP420 {

/** This class implements the low level drivers for an Intel
	IXP420 processor and attending peripherals. The drivers
	contained in this layer make no use of kernel services.
 */
class Device : public Api {
	private:
		/** */
		Oscl::Intel::IXP4xx::GPIO::Driver		_gpioDriver;
		/** */
		Oscl::Intel::IXP4xx::INTR::Interrupt	_irqWAN_HSS_NPE;
		/** */
		Oscl::Intel::IXP4xx::INTR::Interrupt	_irqNPEA;
		/** */
		Oscl::Intel::IXP4xx::INTR::Interrupt	_irqNPEB;
		/** */
		Oscl::Intel::IXP4xx::INTR::Interrupt	_irqQueueManager1_32;
		/** */
		Oscl::Intel::IXP4xx::INTR::Interrupt	_irqQueueManager33_64;
		/** */
		Oscl::Intel::IXP4xx::INTR::Interrupt	_irqTimers0;
		/** */
		Oscl::Intel::IXP4xx::INTR::Interrupt	_irqGPIO0;
		/** */
		Oscl::Intel::IXP4xx::INTR::Interrupt	_irqGPIO1;
		/** */
		Oscl::Intel::IXP4xx::INTR::Interrupt	_irqPCI;
		/** */
		Oscl::Intel::IXP4xx::INTR::Interrupt	_irqPciDmaChannel1;
		/** */
		Oscl::Intel::IXP4xx::INTR::Interrupt	_irqPciDmaChannel2;
		/** */
		Oscl::Intel::IXP4xx::INTR::Interrupt	_irqTimers1;
		/** */
		Oscl::Intel::IXP4xx::INTR::Interrupt	_irqUSB;
		/** */
		Oscl::Intel::IXP4xx::INTR::Interrupt	_irqConsoleUART;
		/** */
		Oscl::Intel::IXP4xx::INTR::Interrupt	_irqTimestampTimer;
		/** */
		Oscl::Intel::IXP4xx::INTR::Interrupt	_irqHighSpeedUART;
		/** */
		Oscl::Intel::IXP4xx::INTR::Interrupt	_irqWatchdogTimer;
		/** */
		Oscl::Intel::IXP4xx::INTR::Interrupt	_irqPmuCounter;
		/** */
		Oscl::Intel::IXP4xx::INTR::Interrupt	_irqXscalePmuCounter;
		/** */
		Oscl::Intel::IXP4xx::INTR::Interrupt	_irqGPIO2;
		/** */
		Oscl::Intel::IXP4xx::INTR::Interrupt	_irqGPIO3;
		/** */
		Oscl::Intel::IXP4xx::INTR::Interrupt	_irqGPIO4;
		/** */
		Oscl::Intel::IXP4xx::INTR::Interrupt	_irqGPIO5;
		/** */
		Oscl::Intel::IXP4xx::INTR::Interrupt	_irqGPIO6;
		/** */
		Oscl::Intel::IXP4xx::INTR::Interrupt	_irqGPIO7;
		/** */
		Oscl::Intel::IXP4xx::INTR::Interrupt	_irqGPIO8;
		/** */
		Oscl::Intel::IXP4xx::INTR::Interrupt	_irqGPIO9;
		/** */
		Oscl::Intel::IXP4xx::INTR::Interrupt	_irqGPIO10;
		/** */
		Oscl::Intel::IXP4xx::INTR::Interrupt	_irqGPIO11;
		/** */
		Oscl::Intel::IXP4xx::INTR::Interrupt	_irqGPIO12;
		/** */
		Oscl::Intel::IXP4xx::INTR::Interrupt	_irqSwInterrupt0;
		/** */
		Oscl::Intel::IXP4xx::INTR::Interrupt	_irqSwInterrupt1;
		/** */
		Oscl::Intel::IXP4xx::INTR::PIC			_irqPic;
		/** */
		Oscl::Intel::IXP4xx::INTR::Map			_irqPicMap;
		/** */
		Oscl::Intel::IXP4xx::INTR::Driver		_intrDriver;
		/** */
		Oscl::Intel::IXP4xx::OST::Driver		_ostDriver;

	public:
		/** */
		Device(	Oscl::Intel::IXP4xx::Map&		registers,
				Oscl::Mt::Mutex::Simple::Api&	mutex
				) noexcept;

	public:	// IXP420::Api
		/** */
		Oscl::Intel::IXP4xx::GPIO::Api&	getGpioApi() noexcept;

		/** */
		Oscl::Intel::IXP4xx::INTR::DrvApi&	getInterruptDriverApi() noexcept;

		/** */
		Oscl::Intel::IXP4xx::OST::Api&		getTimer0Api() noexcept;

		/** */
		Oscl::Intel::IXP4xx::INTR::Api&		getPicApi() noexcept;
	};

}
}
}


#endif
