/*
   Copyright (C) 2006 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "device.h"

using namespace Oscl::Intel::IXP420;
using namespace Oscl::Intel::IXP4xx;
using namespace Oscl::Intel::IXP4xx;

Device::Device(	Oscl::Intel::IXP4xx::Map&		registers,
				Oscl::Mt::Mutex::Simple::Api&	mutex
				) noexcept:
		_gpioDriver(registers.gpioController.gpio),
		_irqWAN_HSS_NPE(),
		_irqNPEA(),
		_irqNPEB(),
		_irqQueueManager1_32(),
		_irqQueueManager33_64(),
		_irqTimers0(),
		_irqGPIO0(),
		_irqGPIO1(),
		_irqPCI(),
		_irqPciDmaChannel1(),
		_irqPciDmaChannel2(),
		_irqTimers1(),
		_irqUSB(),
		_irqConsoleUART(),
		_irqTimestampTimer(),
		_irqHighSpeedUART(),
		_irqWatchdogTimer(),
		_irqPmuCounter(),
		_irqXscalePmuCounter(),
		_irqGPIO2(),
		_irqGPIO3(),
		_irqGPIO4(),
		_irqGPIO5(),
		_irqGPIO6(),
		_irqGPIO7(),
		_irqGPIO8(),
		_irqGPIO9(),
		_irqGPIO10(),
		_irqGPIO11(),
		_irqGPIO12(),
		_irqSwInterrupt0(),
		_irqSwInterrupt1(),
		_irqPic(	registers.interruptController.intr.intr_irq_enc_st,
					_irqWAN_HSS_NPE,
					_irqNPEA,
					_irqNPEB,
					_irqQueueManager1_32,
					_irqQueueManager33_64,
					_irqTimers0,
					_irqGPIO0,
					_irqGPIO1,
					_irqPCI,
					_irqPciDmaChannel1,
					_irqPciDmaChannel2,
					_irqTimers1,
					_irqUSB,
					_irqConsoleUART,
					_irqTimestampTimer,
					_irqHighSpeedUART,
					_irqWatchdogTimer,
					_irqPmuCounter,
					_irqXscalePmuCounter,
					_irqGPIO2,
					_irqGPIO3,
					_irqGPIO4,
					_irqGPIO5,
					_irqGPIO6,
					_irqGPIO7,
					_irqGPIO8,
					_irqGPIO9,
					_irqGPIO10,
					_irqGPIO11,
					_irqGPIO12,
					_irqSwInterrupt0,
					_irqSwInterrupt1
					),
		_irqPicMap(_irqPic),
		_intrDriver(registers.interruptController.intr),
		_ostDriver(	registers.timers.ost.ost_tim0,
					registers.timers.ost.ost_tim0_rl,
					registers.timers.ost.ost_status,
					Oscl::IXP4XX::OST::OST_STATUS::Timer0Interrupt::FieldMask,
					(66666666/100)+1	// FIXME: This should be determined
										// by the system. The value is written
										// to the PITC register is clocked
										// at 66666666Hz.
										// The value here attempts to achieve
										// a period of at least 10ms
										// (a frequency of 100Hz).
					)
		{
	}

Oscl::Intel::IXP4xx::GPIO::Api&	Device::getGpioApi() noexcept{
	return _gpioDriver;
	}

Oscl::Intel::IXP4xx::INTR::DrvApi&		Device::getInterruptDriverApi() noexcept{
	return _intrDriver;
	}

Oscl::Intel::IXP4xx::OST::Api&		Device::getTimer0Api() noexcept{
	return _ostDriver;
	}

Oscl::Intel::IXP4xx::INTR::Api&		Device::getPicApi() noexcept{
	return _irqPicMap;
	}

