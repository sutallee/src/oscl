/*
   Copyright (C) 2006 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_intel_ixp4xx_gpio_bith_
#define _oscl_drv_intel_ixp4xx_gpio_bith_
#include "oscl/hw/intel/ixp4xx/gpio.h"
#include "bitapi.h"

/** */
namespace Oscl {
/** */
namespace Intel {
/** */
namespace IXP4xx {
/** */
namespace GPIO {

/** This interface represents the operations that can
	be performed on GPIO bits 0 through 7.
 */
template < unsigned bitNumber >
class Bit0_7 : public BitApi {
	private:
		/** */
		Oscl::IXP4XX::GPIO::GpioRegisters&	_registers;

	public:
		/** Make GCC happy */
		Bit0_7(Oscl::IXP4XX::GPIO::GpioRegisters& registers) noexcept:
			_registers(registers)
			{}

	public: // Oscl::Bits::InOutApi
		/** */
		void	high() noexcept{
			Oscl::IXP4XX::GPIO::GPOUTR::Reg	value	= _registers.gpoutr;
			value	&= ~(Oscl::IXP4XX::GPIO::GPOUTR::Mask << bitNumber);
			value	|= (Oscl::IXP4XX::GPIO::GPOUTR::High << bitNumber);
			_registers.gpoutr	= value;
			}
		/** */
		void	low() noexcept{
			Oscl::IXP4XX::GPIO::GPOUTR::Reg	value	= _registers.gpoutr;
			value	&= ~(Oscl::IXP4XX::GPIO::GPOUTR::Mask << bitNumber);
			value	|= (Oscl::IXP4XX::GPIO::GPOUTR::Low << bitNumber);
			_registers.gpoutr	= value;
			}
		/** */
		bool	state() noexcept{
			return (_registers.gpinr & (Oscl::IXP4XX::GPIO::GPOUTR::Mask<<bitNumber)) == (Oscl::IXP4XX::GPIO::GPOUTR::High << bitNumber);
			}

	public: // GPIOER interface
		/** This operation is invoked to allow the output
			to float effectively making the GPIO bit an input.
		 */
		void	triStateOutput() noexcept{
			Oscl::IXP4XX::GPIO::GPOER::Reg	value	= _registers.gpoer;
			value	&= ~(Oscl::IXP4XX::GPIO::GPOER::Mask << bitNumber);
			value	|= (Oscl::IXP4XX::GPIO::GPOER::TriState << bitNumber);
			_registers.gpoer	= value;
			}
		/** This operation drives the output with a one
			or zeror depending upon the reset value or
			the last called of the high() or low() operations.
		 */
		void	driveOutput() noexcept{
			Oscl::IXP4XX::GPIO::GPOER::Reg	value	= _registers.gpoer;
			value	&= ~(Oscl::IXP4XX::GPIO::GPOER::Mask << bitNumber);
			value	|= (Oscl::IXP4XX::GPIO::GPOER::Driven << bitNumber);
			_registers.gpoer	= value;
			}

	public: // GPISR interface
		/** */
		bool	interruptPending() noexcept{
			return (_registers.gpisr & (Oscl::IXP4XX::GPIO::GPISR::Mask<<bitNumber)) == (Oscl::IXP4XX::GPIO::GPISR::Pending << bitNumber);
			}
		/** */
		void	clearInterrupt() noexcept{
			_registers.gpisr	= (Oscl::IXP4XX::GPIO::GPISR::Clear << bitNumber);
			}
		/** */
		void	interruptActiveHigh() noexcept{
			Oscl::IXP4XX::GPIO::GPIT1R::Reg	value	= _registers.gpit1r;
			value	&= ~(Oscl::IXP4XX::GPIO::GPIT1R::Mask << bitNumber*4);
			value	|= (Oscl::IXP4XX::GPIO::GPIT1R::ActiveHigh << bitNumber*4);
			_registers.gpit1r	= value;
			}
		/** */
		void	interruptActiveLow() noexcept{
			Oscl::IXP4XX::GPIO::GPIT1R::Reg	value	= _registers.gpit1r;
			value	&= ~(Oscl::IXP4XX::GPIO::GPIT1R::Mask << bitNumber*4);
			value	|= (Oscl::IXP4XX::GPIO::GPIT1R::ActiveLow << bitNumber*4);
			_registers.gpit1r	= value;
			}
		/** */
		void	interruptRisingEdge() noexcept{
			Oscl::IXP4XX::GPIO::GPIT1R::Reg	value	= _registers.gpit1r;
			value	&= ~(Oscl::IXP4XX::GPIO::GPIT1R::Mask << bitNumber*4);
			value	|= (Oscl::IXP4XX::GPIO::GPIT1R::RisingEdge << bitNumber*4);
			_registers.gpit1r	= value;
			}
		/** */
		void	interruptFallingEdge() noexcept{
			Oscl::IXP4XX::GPIO::GPIT1R::Reg	value	= _registers.gpit1r;
			value	&= ~(Oscl::IXP4XX::GPIO::GPIT1R::Mask << bitNumber*4);
			value	|= (Oscl::IXP4XX::GPIO::GPIT1R::FallingEdge << bitNumber*4);
			_registers.gpit1r	= value;
			}
		/** */
		void	interruptBothEdges() noexcept{
			Oscl::IXP4XX::GPIO::GPIT1R::Reg	value	= _registers.gpit1r;
			value	&= ~(Oscl::IXP4XX::GPIO::GPIT1R::Mask << bitNumber*4);
			value	|= (Oscl::IXP4XX::GPIO::GPIT1R::Transitional << bitNumber*4);
			_registers.gpit1r	= value;
			}
	};

/** This interface represents the operations that can
	be performed on GPIO bits 8 through 15.
 */
template < unsigned bitNumber >
class Bit8_15 : public BitApi {
	private:
		/** */
		Oscl::IXP4XX::GPIO::GpioRegisters&	_registers;

	public:
		/** Make GCC happy */
		Bit8_15(Oscl::IXP4XX::GPIO::GpioRegisters& registers) noexcept:
			_registers(registers)
			{}

	public: // Oscl::Bits::InOutApi
		/** */
		void	high() noexcept{
			Oscl::IXP4XX::GPIO::GPOUTR::Reg	value	= _registers.gpoutr;
			value	&= ~(Oscl::IXP4XX::GPIO::GPOUTR::Mask << bitNumber);
			value	|= (Oscl::IXP4XX::GPIO::GPOUTR::High << bitNumber);
			_registers.gpoutr	= value;
			}
		/** */
		void	low() noexcept{
			Oscl::IXP4XX::GPIO::GPOUTR::Reg	value	= _registers.gpoutr;
			value	&= ~(Oscl::IXP4XX::GPIO::GPOUTR::Mask << bitNumber);
			value	|= (Oscl::IXP4XX::GPIO::GPOUTR::Low << bitNumber);
			_registers.gpoutr	= value;
			}
		/** */
		bool	state() noexcept{
			return (_registers.gpinr & (Oscl::IXP4XX::GPIO::GPOUTR::Mask<<bitNumber)) == (Oscl::IXP4XX::GPIO::GPOUTR::High << bitNumber);
			}

	public: // GPIOER interface
		/** This operation is invoked to allow the output
			to float effectively making the GPIO bit an input.
		 */
		void	triStateOutput() noexcept{
			Oscl::IXP4XX::GPIO::GPOER::Reg	value	= _registers.gpoer;
			value	&= ~(Oscl::IXP4XX::GPIO::GPOER::Mask << bitNumber);
			value	|= (Oscl::IXP4XX::GPIO::GPOER::TriState << bitNumber);
			_registers.gpoer	= value;
			}
		/** This operation drives the output with a one
			or zeror depending upon the reset value or
			the last called of the high() or low() operations.
		 */
		void	driveOutput() noexcept{
			Oscl::IXP4XX::GPIO::GPOER::Reg	value	= _registers.gpoer;
			value	&= ~(Oscl::IXP4XX::GPIO::GPOER::Mask << bitNumber);
			value	|= (Oscl::IXP4XX::GPIO::GPOER::Driven << bitNumber);
			_registers.gpoer	= value;
			}

	public: // GPISR interface
		/** */
		bool	interruptPending() noexcept{
			return (_registers.gpisr & (Oscl::IXP4XX::GPIO::GPISR::Mask<<bitNumber)) == (Oscl::IXP4XX::GPIO::GPISR::Pending << bitNumber);
			}
		/** */
		void	clearInterrupt() noexcept{
			_registers.gpisr	= (Oscl::IXP4XX::GPIO::GPISR::Clear << bitNumber);
			}
		/** */
		void	interruptActiveHigh() noexcept{
			Oscl::IXP4XX::GPIO::GPIT2R::Reg	value	= _registers.gpit2r;
			value	&= ~(Oscl::IXP4XX::GPIO::GPIT2R::Mask << (bitNumber-8)*4);
			value	|= (Oscl::IXP4XX::GPIO::GPIT2R::ActiveHigh << (bitNumber-8)*4);
			_registers.gpit2r	= value;
			}
		/** */
		void	interruptActiveLow() noexcept{
			Oscl::IXP4XX::GPIO::GPIT2R::Reg	value	= _registers.gpit2r;
			value	&= ~(Oscl::IXP4XX::GPIO::GPIT2R::Mask << (bitNumber-8)*4);
			value	|= (Oscl::IXP4XX::GPIO::GPIT2R::ActiveLow << (bitNumber-8)*4);
			_registers.gpit2r	= value;
			}
		/** */
		void	interruptRisingEdge() noexcept{
			Oscl::IXP4XX::GPIO::GPIT2R::Reg	value	= _registers.gpit2r;
			value	&= ~(Oscl::IXP4XX::GPIO::GPIT2R::Mask << (bitNumber-8)*4);
			value	|= (Oscl::IXP4XX::GPIO::GPIT2R::RisingEdge << (bitNumber-8)*4);
			_registers.gpit2r	= value;
			}
		/** */
		void	interruptFallingEdge() noexcept{
			Oscl::IXP4XX::GPIO::GPIT2R::Reg	value	= _registers.gpit2r;
			value	&= ~(Oscl::IXP4XX::GPIO::GPIT2R::Mask << (bitNumber-8)*4);
			value	|= (Oscl::IXP4XX::GPIO::GPIT2R::FallingEdge << (bitNumber-8)*4);
			_registers.gpit2r	= value;
			}
		/** */
		void	interruptBothEdges() noexcept{
			Oscl::IXP4XX::GPIO::GPIT2R::Reg	value	= _registers.gpit2r;
			value	&= ~(Oscl::IXP4XX::GPIO::GPIT2R::Mask << (bitNumber-8)*4);
			value	|= (Oscl::IXP4XX::GPIO::GPIT2R::Transitional << (bitNumber-8)*4);
			_registers.gpit2r	= value;
			}
	};

}
}
}
}

#endif
