/*
   Copyright (C) 2006 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_intel_ixp4xx_gpio_drvh_
#define _oscl_drv_intel_ixp4xx_gpio_drvh_
#include "api.h"
#include "bit.h"

/** */
namespace Oscl {
/** */
namespace Intel {
/** */
namespace IXP4xx {
/** */
namespace GPIO {

/** This interface represents the GPIO functionality.
 */
class Driver : public Api {
	private:
		Bit0_7<0>	_gpio0;
		Bit0_7<1>	_gpio1;
		Bit0_7<2>	_gpio2;
		Bit0_7<3>	_gpio3;
		Bit0_7<4>	_gpio4;
		Bit0_7<5>	_gpio5;
		Bit0_7<6>	_gpio6;
		Bit0_7<7>	_gpio7;
		Bit8_15<8>	_gpio8;
		Bit8_15<9>	_gpio9;
		Bit8_15<10>	_gpio10;
		Bit8_15<11>	_gpio11;
		Bit8_15<12>	_gpio12;
		Bit8_15<13>	_gpio13;
		Bit8_15<14>	_gpio14;
		Bit8_15<15>	_gpio15;

	public:
		/** Make GCC happy */
		Driver(Oscl::IXP4XX::GPIO::GpioRegisters& registers) noexcept;

	public:
		/** */
		BitApi&	getGPIO0() noexcept;
		/** */
		BitApi&	getGPIO1() noexcept;
		/** */
		BitApi&	getGPIO2() noexcept;
		/** */
		BitApi&	getGPIO3() noexcept;
		/** */
		BitApi&	getGPIO4() noexcept;
		/** */
		BitApi&	getGPIO5() noexcept;
		/** */
		BitApi&	getGPIO6() noexcept;
		/** */
		BitApi&	getGPIO7() noexcept;
		/** */
		BitApi&	getGPIO8() noexcept;
		/** */
		BitApi&	getGPIO9() noexcept;
		/** */
		BitApi&	getGPIO10() noexcept;
		/** */
		BitApi&	getGPIO11() noexcept;
		/** */
		BitApi&	getGPIO12() noexcept;
		/** */
		BitApi&	getGPIO13() noexcept;
		/** */
		BitApi&	getGPIO14() noexcept;
		/** */
		BitApi&	getGPIO15() noexcept;
	};

}
}
}
}

#endif
