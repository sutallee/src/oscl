/*
   Copyright (C) 2006 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_intel_ixp4xx_gpio_bitapih_
#define _oscl_drv_intel_ixp4xx_gpio_bitapih_
#include "oscl/bits/ioapi.h"

/** */
namespace Oscl {
/** */
namespace Intel {
/** */
namespace IXP4xx {
/** */
namespace GPIO {

/** This interface represents the operations that can
	be performed on all GPIO bits.
 */
class BitApi : Oscl::Bits::InOutApi {
	public:
		/** Make GCC happy */
		virtual ~BitApi() {}

	public: // Oscl::Bits::InOutApi
		/** */
		virtual void	high() noexcept=0;
		/** */
		virtual void	low() noexcept=0;
		/** */
		virtual bool	state() noexcept=0;

	public: // GPIOER interface
		/** This operation is invoked to allow the output
			to float effectively making the GPIO bit an input.
		 */
		virtual void	triStateOutput() noexcept=0;
		/** This operation drives the output with a one
			or zeror depending upon the reset value or
			the last called of the high() or low() operations.
		 */
		virtual void	driveOutput() noexcept=0;

	public: // GPISR interface
		/** */
		virtual bool	interruptPending() noexcept=0;
		/** */
		virtual void	clearInterrupt() noexcept=0;
		/** */
		virtual void	interruptActiveHigh() noexcept=0;
		/** */
		virtual void	interruptActiveLow() noexcept=0;
		/** */
		virtual void	interruptRisingEdge() noexcept=0;
		/** */
		virtual void	interruptFallingEdge() noexcept=0;
		/** */
		virtual void	interruptBothEdges() noexcept=0;
	};

}
}
}
}

#endif
