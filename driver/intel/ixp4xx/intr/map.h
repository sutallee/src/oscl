/*
   Copyright (C) 2006 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_intel_ixp4xx_intr_maph_
#define _oscl_drv_intel_ixp4xx_intr_maph_
#include "api.h"
#include "oscl/driver/intel/ixp4xx/intr/picapi.h"

/** */
namespace Oscl {
/** */
namespace Intel {
/** */
namespace IXP4xx {
/** */
namespace INTR {

/** */
class Map : public Api {
	private:
		/** */
		Oscl::Intel::IXP4xx::INTR::PicApi&	_pic;

	public:
		/** */
		Map(Oscl::Intel::IXP4xx::INTR::PicApi& pic) noexcept;

	private:	// INTR::PicApi
		/** */
		Oscl::Interrupt::StatusHandlerApi&	getISR() noexcept;
		/** */
		Oscl::Intel::IXP4xx::INTR::IrqApi&	getWAN_HSS_NPE() noexcept;
		/** */
		Oscl::Intel::IXP4xx::INTR::IrqApi&	getNPEA() noexcept;
		/** */
		Oscl::Intel::IXP4xx::INTR::IrqApi&	getNPEB() noexcept;
		/** */
		Oscl::Intel::IXP4xx::INTR::IrqApi&	getQueueManager1_32() noexcept;
		/** */
		Oscl::Intel::IXP4xx::INTR::IrqApi&	getQueueManager33_64() noexcept;
		/** */
		Oscl::Intel::IXP4xx::INTR::IrqApi&	getTimers0() noexcept;
		/** */
		Oscl::Intel::IXP4xx::INTR::IrqApi&	getGPIO0() noexcept;
		/** */
		Oscl::Intel::IXP4xx::INTR::IrqApi&	getGPIO1() noexcept;
		/** */
		Oscl::Intel::IXP4xx::INTR::IrqApi&	getPCI() noexcept;
		/** */
		Oscl::Intel::IXP4xx::INTR::IrqApi&	getPciDmaChannel1() noexcept;
		/** */
		Oscl::Intel::IXP4xx::INTR::IrqApi&	getPciDmaChannel2() noexcept;
		/** */
		Oscl::Intel::IXP4xx::INTR::IrqApi&	getTimers1() noexcept;
		/** */
		Oscl::Intel::IXP4xx::INTR::IrqApi&	getUSB() noexcept;
		/** */
		Oscl::Intel::IXP4xx::INTR::IrqApi&	getConsoleUART() noexcept;
		/** */
		Oscl::Intel::IXP4xx::INTR::IrqApi&	getTimestampTimer() noexcept;
		/** */
		Oscl::Intel::IXP4xx::INTR::IrqApi&	getHighSpeedUART() noexcept;
		/** */
		Oscl::Intel::IXP4xx::INTR::IrqApi&	getWatchdogTimer() noexcept;
		/** */
		Oscl::Intel::IXP4xx::INTR::IrqApi&	getPmuCounter() noexcept;
		/** */
		Oscl::Intel::IXP4xx::INTR::IrqApi&	getXscalePmuCounter() noexcept;
		/** */
		Oscl::Intel::IXP4xx::INTR::IrqApi&	getGPIO2() noexcept;
		/** */
		Oscl::Intel::IXP4xx::INTR::IrqApi&	getGPIO3() noexcept;
		/** */
		Oscl::Intel::IXP4xx::INTR::IrqApi&	getGPIO4() noexcept;
		/** */
		Oscl::Intel::IXP4xx::INTR::IrqApi&	getGPIO5() noexcept;
		/** */
		Oscl::Intel::IXP4xx::INTR::IrqApi&	getGPIO6() noexcept;
		/** */
		Oscl::Intel::IXP4xx::INTR::IrqApi&	getGPIO7() noexcept;
		/** */
		Oscl::Intel::IXP4xx::INTR::IrqApi&	getGPIO8() noexcept;
		/** */
		Oscl::Intel::IXP4xx::INTR::IrqApi&	getGPIO9() noexcept;
		/** */
		Oscl::Intel::IXP4xx::INTR::IrqApi&	getGPIO10() noexcept;
		/** */
		Oscl::Intel::IXP4xx::INTR::IrqApi&	getGPIO11() noexcept;
		/** */
		Oscl::Intel::IXP4xx::INTR::IrqApi&	getGPIO12() noexcept;
		/** */
		Oscl::Intel::IXP4xx::INTR::IrqApi&	getSwInterrupt0() noexcept;
		/** */
		Oscl::Intel::IXP4xx::INTR::IrqApi&	getSwInterrupt1() noexcept;
	};

}
}
}
}

#endif
