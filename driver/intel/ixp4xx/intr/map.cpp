/*
   Copyright (C) 2006 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "map.h"

using namespace Oscl::Intel::IXP4xx::INTR;

Map::Map(Oscl::Intel::IXP4xx::INTR::PicApi& pic) noexcept:
		_pic(pic)
		{
	}

Oscl::Interrupt::StatusHandlerApi&	Map::getISR() noexcept{
	return _pic;
	}

Oscl::Intel::IXP4xx::INTR::IrqApi&	Map::getWAN_HSS_NPE() noexcept{
	return _pic.getIrqApi(0);
	}

Oscl::Intel::IXP4xx::INTR::IrqApi&	Map::getNPEA() noexcept{
	return _pic.getIrqApi(1);
	}

Oscl::Intel::IXP4xx::INTR::IrqApi&	Map::getNPEB() noexcept{
	return _pic.getIrqApi(2);
	}

Oscl::Intel::IXP4xx::INTR::IrqApi&	Map::getQueueManager1_32() noexcept{
	return _pic.getIrqApi(3);
	}

Oscl::Intel::IXP4xx::INTR::IrqApi&	Map::getQueueManager33_64() noexcept{
	return _pic.getIrqApi(4);
	}

Oscl::Intel::IXP4xx::INTR::IrqApi&	Map::getTimers0() noexcept{
	return _pic.getIrqApi(5);
	}

Oscl::Intel::IXP4xx::INTR::IrqApi&	Map::getGPIO0() noexcept{
	return _pic.getIrqApi(6);
	}

Oscl::Intel::IXP4xx::INTR::IrqApi&	Map::getGPIO1() noexcept{
	return _pic.getIrqApi(7);
	}

Oscl::Intel::IXP4xx::INTR::IrqApi&	Map::getPCI() noexcept{
	return _pic.getIrqApi(8);
	}

Oscl::Intel::IXP4xx::INTR::IrqApi&	Map::getPciDmaChannel1() noexcept{
	return _pic.getIrqApi(9);
	}

Oscl::Intel::IXP4xx::INTR::IrqApi&	Map::getPciDmaChannel2() noexcept{
	return _pic.getIrqApi(10);
	}

Oscl::Intel::IXP4xx::INTR::IrqApi&	Map::getTimers1() noexcept{
	return _pic.getIrqApi(11);
	}

Oscl::Intel::IXP4xx::INTR::IrqApi&	Map::getUSB() noexcept{
	return _pic.getIrqApi(12);
	}

Oscl::Intel::IXP4xx::INTR::IrqApi&	Map::getConsoleUART() noexcept{
	return _pic.getIrqApi(13);
	}

Oscl::Intel::IXP4xx::INTR::IrqApi&	Map::getTimestampTimer() noexcept{
	return _pic.getIrqApi(14);
	}

Oscl::Intel::IXP4xx::INTR::IrqApi&	Map::getHighSpeedUART() noexcept{
	return _pic.getIrqApi(15);
	}

Oscl::Intel::IXP4xx::INTR::IrqApi&	Map::getWatchdogTimer() noexcept{
	return _pic.getIrqApi(16);
	}

Oscl::Intel::IXP4xx::INTR::IrqApi&	Map::getPmuCounter() noexcept{
	return _pic.getIrqApi(17);
	}

Oscl::Intel::IXP4xx::INTR::IrqApi&	Map::getXscalePmuCounter() noexcept{
	return _pic.getIrqApi(18);
	}

Oscl::Intel::IXP4xx::INTR::IrqApi&	Map::getGPIO2() noexcept{
	return _pic.getIrqApi(19);
	}

Oscl::Intel::IXP4xx::INTR::IrqApi&	Map::getGPIO3() noexcept{
	return _pic.getIrqApi(20);
	}

Oscl::Intel::IXP4xx::INTR::IrqApi&	Map::getGPIO4() noexcept{
	return _pic.getIrqApi(21);
	}

Oscl::Intel::IXP4xx::INTR::IrqApi&	Map::getGPIO5() noexcept{
	return _pic.getIrqApi(22);
	}

Oscl::Intel::IXP4xx::INTR::IrqApi&	Map::getGPIO6() noexcept{
	return _pic.getIrqApi(23);
	}

Oscl::Intel::IXP4xx::INTR::IrqApi&	Map::getGPIO7() noexcept{
	return _pic.getIrqApi(24);
	}

Oscl::Intel::IXP4xx::INTR::IrqApi&	Map::getGPIO8() noexcept{
	return _pic.getIrqApi(25);
	}

Oscl::Intel::IXP4xx::INTR::IrqApi&	Map::getGPIO9() noexcept{
	return _pic.getIrqApi(26);
	}

Oscl::Intel::IXP4xx::INTR::IrqApi&	Map::getGPIO10() noexcept{
	return _pic.getIrqApi(27);
	}

Oscl::Intel::IXP4xx::INTR::IrqApi&	Map::getGPIO11() noexcept{
	return _pic.getIrqApi(28);
	}

Oscl::Intel::IXP4xx::INTR::IrqApi&	Map::getGPIO12() noexcept{
	return _pic.getIrqApi(29);
	}

Oscl::Intel::IXP4xx::INTR::IrqApi&	Map::getSwInterrupt0() noexcept{
	return _pic.getIrqApi(30);
	}

Oscl::Intel::IXP4xx::INTR::IrqApi&	Map::getSwInterrupt1() noexcept{
	return _pic.getIrqApi(31);
	}

