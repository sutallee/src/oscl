/*
   Copyright (C) 2006 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_intel_ixp4xx_intr_irqdrvh_
#define _oscl_drv_intel_ixp4xx_intr_irqdrvh_
#include "irqdrvapi.h"
#include "oscl/hw/intel/ixp4xx/intr.h"

#include "oscl/error/info.h"

/** */
namespace Oscl {
/** */
namespace Intel {
/** */
namespace IXP4xx {
/** */
namespace INTR {

/** */
template <unsigned IrqNumber>
class IrqDriver : public IrqDrvApi {
	private:
		/** */
		Oscl::IXP4XX::INTR::InterruptController&		_intr;

	public:
		/** */
		IrqDriver(Oscl::IXP4XX::INTR::InterruptController& intr) noexcept:
				_intr(intr)
				{
			disable();
			}

		/** */
		void	enable() noexcept{
			Oscl::IXP4XX::INTR::INTR_EN::Reg	value	= _intr.intr_en;
			value	&= ~(		Oscl::IXP4XX::INTR::INTR_EN::Mask
							<<	IrqNumber
							);
			value	|= (		Oscl::IXP4XX::INTR::INTR_EN::Enable
							<<	IrqNumber
							);
			_intr.intr_en	= value;
			}

		/** */
		void	disable() noexcept{
			Oscl::IXP4XX::INTR::INTR_EN::Reg	value	= _intr.intr_en;
			value	&= ~(		Oscl::IXP4XX::INTR::INTR_EN::Mask
							<<	IrqNumber
							);
			value	|= (		Oscl::IXP4XX::INTR::INTR_EN::Disable
							<<	IrqNumber
							);
			_intr.intr_en	= value;
			}

		/** */
		void	routeToIRQ() noexcept{
			Oscl::IXP4XX::INTR::INTR_SEL::Reg	value	= _intr.intr_sel;
			value	&= ~(		Oscl::IXP4XX::INTR::INTR_SEL::Mask
							<<	IrqNumber
							);
			value	|= (		Oscl::IXP4XX::INTR::INTR_SEL::SelectIRQ
							<<	IrqNumber
							);
			_intr.intr_sel	= value;
			}

		/** */
		void	routeToFIQ() noexcept{
			Oscl::IXP4XX::INTR::INTR_SEL::Reg	value	= _intr.intr_sel;
			value	&= ~(		Oscl::IXP4XX::INTR::INTR_SEL::Mask
							<<	IrqNumber
							);
			value	|= (		Oscl::IXP4XX::INTR::INTR_SEL::SelectFIQ
							<<	IrqNumber
							);
			_intr.intr_sel	= value;
			}
		/** */
		bool	pending() noexcept{
			return (		_intr.intr_st
						&	(((Oscl::IXP4XX::INTR::INTR_ST::Reg)Oscl::IXP4XX::INTR::INTR_ST::Mask) << IrqNumber)
						)
					==	(((Oscl::IXP4XX::INTR::INTR_ST::Reg)Oscl::IXP4XX::INTR::INTR_ST::Pending) << IrqNumber);
			}
		/** */
		bool	irqPending() noexcept{
			return (		_intr.intr_st
						& (((Oscl::IXP4XX::INTR::INTR_IRQ_ST::Reg)Oscl::IXP4XX::INTR::INTR_IRQ_ST::Mask) << IrqNumber)
						)
					== (((Oscl::IXP4XX::INTR::INTR_IRQ_ST::Reg)Oscl::IXP4XX::INTR::INTR_IRQ_ST::Pending) << IrqNumber);
			}
		/** */
		bool	fiqPending() noexcept{
			return (		_intr.intr_st
						&	(((Oscl::IXP4XX::INTR::INTR_FIQ_ST::Reg)Oscl::IXP4XX::INTR::INTR_FIQ_ST::Mask) << IrqNumber)
						)
					== (((Oscl::IXP4XX::INTR::INTR_FIQ_ST::Reg)Oscl::IXP4XX::INTR::INTR_FIQ_ST::Pending) << IrqNumber);
			}
	};

}
}
}
}

#endif
