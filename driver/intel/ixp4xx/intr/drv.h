/*
   Copyright (C) 2006 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_intel_ixp4xx_intr_drvh_
#define _oscl_drv_intel_ixp4xx_intr_drvh_
#include "drvapi.h"
#include "irqdrv.h"

/** */
namespace Oscl {
/** */
namespace Intel {
/** */
namespace IXP4xx {
/** */
namespace INTR {

/** */
class Driver : public DrvApi {
	private:
		/** */
		IrqDriver<0>	_wan_hss_npe;
		/** */
		IrqDriver<1>	_npea;
		/** */
		IrqDriver<2>	_npeb;
		/** */
		IrqDriver<3>	_queueManager1_32;
		/** */
		IrqDriver<4>	_queueManager33_64;
		/** */
		IrqDriver<5>	_timer0;
		/** */
		IrqDriver<6>	_gpio0;
		/** */
		IrqDriver<7>	_gpio1;
		/** */
		IrqDriver<8>	_pci;
		/** */
		IrqDriver<9>	_pciDmaChannel1;
		/** */
		IrqDriver<10>	_pciDmaChannel2;
		/** */
		IrqDriver<11>	_timer1;
		/** */
		IrqDriver<12>	_usb;
		/** */
		IrqDriver<13>	_consoleUART;
		/** */
		IrqDriver<14>	_timestampTimer;
		/** */
		IrqDriver<15>	_highSpeedUART;
		/** */
		IrqDriver<16>	_watchdogTimer;
		/** */
		IrqDriver<17>	_pmu;
		/** */
		IrqDriver<18>	_xscalePmu;
		/** */
		IrqDriver<19>	_gpio2;
		/** */
		IrqDriver<20>	_gpio3;
		/** */
		IrqDriver<21>	_gpio4;
		/** */
		IrqDriver<22>	_gpio5;
		/** */
		IrqDriver<23>	_gpio6;
		/** */
		IrqDriver<24>	_gpio7;
		/** */
		IrqDriver<25>	_gpio8;
		/** */
		IrqDriver<26>	_gpio9;
		/** */
		IrqDriver<27>	_gpio10;
		/** */
		IrqDriver<28>	_gpio11;
		/** */
		IrqDriver<29>	_gpio12;
		/** */
		IrqDriver<30>	_swInterrupt0;
		/** */
		IrqDriver<31>	_swInterrupt1;

	public:
		/** */
		Driver(Oscl::IXP4XX::INTR::InterruptController& intr) noexcept;

	private:	// INTR::DrvApi
		/** */
		Oscl::Intel::IXP4xx::INTR::IrqDrvApi&	getWAN_HSS_NPE() noexcept;
		/** */
		Oscl::Intel::IXP4xx::INTR::IrqDrvApi&	getNPEA() noexcept;
		/** */
		Oscl::Intel::IXP4xx::INTR::IrqDrvApi&	getNPEB() noexcept;
		/** */
		Oscl::Intel::IXP4xx::INTR::IrqDrvApi&	getQueueManager1_32() noexcept;
		/** */
		Oscl::Intel::IXP4xx::INTR::IrqDrvApi&	getQueueManager33_64() noexcept;
		/** */
		Oscl::Intel::IXP4xx::INTR::IrqDrvApi&	getTimer0() noexcept;
		/** */
		Oscl::Intel::IXP4xx::INTR::IrqDrvApi&	getGPIO0() noexcept;
		/** */
		Oscl::Intel::IXP4xx::INTR::IrqDrvApi&	getGPIO1() noexcept;
		/** */
		Oscl::Intel::IXP4xx::INTR::IrqDrvApi&	getPCI() noexcept;
		/** */
		Oscl::Intel::IXP4xx::INTR::IrqDrvApi&	getPciDmaChannel1() noexcept;
		/** */
		Oscl::Intel::IXP4xx::INTR::IrqDrvApi&	getPciDmaChannel2() noexcept;
		/** */
		Oscl::Intel::IXP4xx::INTR::IrqDrvApi&	getTimer1() noexcept;
		/** */
		Oscl::Intel::IXP4xx::INTR::IrqDrvApi&	getUSB() noexcept;
		/** */
		Oscl::Intel::IXP4xx::INTR::IrqDrvApi&	getConsoleUART() noexcept;
		/** */
		Oscl::Intel::IXP4xx::INTR::IrqDrvApi&	getTimestampTimer() noexcept;
		/** */
		Oscl::Intel::IXP4xx::INTR::IrqDrvApi&	getHighSpeedUART() noexcept;
		/** */
		Oscl::Intel::IXP4xx::INTR::IrqDrvApi&	getWatchdogTimer() noexcept;
		/** */
		Oscl::Intel::IXP4xx::INTR::IrqDrvApi&	getPmuCounter() noexcept;
		/** */
		Oscl::Intel::IXP4xx::INTR::IrqDrvApi&	getXscalePmuCounter() noexcept;
		/** */
		Oscl::Intel::IXP4xx::INTR::IrqDrvApi&	getGPIO2() noexcept;
		/** */
		Oscl::Intel::IXP4xx::INTR::IrqDrvApi&	getGPIO3() noexcept;
		/** */
		Oscl::Intel::IXP4xx::INTR::IrqDrvApi&	getGPIO4() noexcept;
		/** */
		Oscl::Intel::IXP4xx::INTR::IrqDrvApi&	getGPIO5() noexcept;
		/** */
		Oscl::Intel::IXP4xx::INTR::IrqDrvApi&	getGPIO6() noexcept;
		/** */
		Oscl::Intel::IXP4xx::INTR::IrqDrvApi&	getGPIO7() noexcept;
		/** */
		Oscl::Intel::IXP4xx::INTR::IrqDrvApi&	getGPIO8() noexcept;
		/** */
		Oscl::Intel::IXP4xx::INTR::IrqDrvApi&	getGPIO9() noexcept;
		/** */
		Oscl::Intel::IXP4xx::INTR::IrqDrvApi&	getGPIO10() noexcept;
		/** */
		Oscl::Intel::IXP4xx::INTR::IrqDrvApi&	getGPIO11() noexcept;
		/** */
		Oscl::Intel::IXP4xx::INTR::IrqDrvApi&	getGPIO12() noexcept;
		/** */
		Oscl::Intel::IXP4xx::INTR::IrqDrvApi&	getSwInterrupt0() noexcept;
		/** */
		Oscl::Intel::IXP4xx::INTR::IrqDrvApi&	getSwInterrupt1() noexcept;
	};

}
}
}
}

#endif
