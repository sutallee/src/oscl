/*
   Copyright (C) 2006 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_intel_ixp4xx_intr_drvapih_
#define _oscl_drv_intel_ixp4xx_intr_drvapih_
#include "irqdrvapi.h"

/** */
namespace Oscl {
/** */
namespace Intel {
/** */
namespace IXP4xx {
/** */
namespace INTR {

/** */
class DrvApi {
	public:
		/** */
		virtual ~DrvApi() {}

	public:
		/** */
		virtual Oscl::Intel::IXP4xx::INTR::IrqDrvApi&	getWAN_HSS_NPE() noexcept=0;
		/** */
		virtual Oscl::Intel::IXP4xx::INTR::IrqDrvApi&	getNPEA() noexcept=0;
		/** */
		virtual Oscl::Intel::IXP4xx::INTR::IrqDrvApi&	getNPEB() noexcept=0;
		/** */
		virtual Oscl::Intel::IXP4xx::INTR::IrqDrvApi&	getQueueManager1_32() noexcept=0;
		/** */
		virtual Oscl::Intel::IXP4xx::INTR::IrqDrvApi&	getQueueManager33_64() noexcept=0;
		/** */
		virtual Oscl::Intel::IXP4xx::INTR::IrqDrvApi&	getTimer0() noexcept=0;
		/** */
		virtual Oscl::Intel::IXP4xx::INTR::IrqDrvApi&	getGPIO0() noexcept=0;
		/** */
		virtual Oscl::Intel::IXP4xx::INTR::IrqDrvApi&	getGPIO1() noexcept=0;
		/** */
		virtual Oscl::Intel::IXP4xx::INTR::IrqDrvApi&	getPCI() noexcept=0;
		/** */
		virtual Oscl::Intel::IXP4xx::INTR::IrqDrvApi&	getPciDmaChannel1() noexcept=0;
		/** */
		virtual Oscl::Intel::IXP4xx::INTR::IrqDrvApi&	getPciDmaChannel2() noexcept=0;
		/** */
		virtual Oscl::Intel::IXP4xx::INTR::IrqDrvApi&	getTimer1() noexcept=0;
		/** */
		virtual Oscl::Intel::IXP4xx::INTR::IrqDrvApi&	getUSB() noexcept=0;
		/** */
		virtual Oscl::Intel::IXP4xx::INTR::IrqDrvApi&	getConsoleUART() noexcept=0;
		/** */
		virtual Oscl::Intel::IXP4xx::INTR::IrqDrvApi&	getTimestampTimer() noexcept=0;
		/** */
		virtual Oscl::Intel::IXP4xx::INTR::IrqDrvApi&	getHighSpeedUART() noexcept=0;
		/** */
		virtual Oscl::Intel::IXP4xx::INTR::IrqDrvApi&	getWatchdogTimer() noexcept=0;
		/** */
		virtual Oscl::Intel::IXP4xx::INTR::IrqDrvApi&	getPmuCounter() noexcept=0;
		/** */
		virtual Oscl::Intel::IXP4xx::INTR::IrqDrvApi&	getXscalePmuCounter() noexcept=0;
		/** */
		virtual Oscl::Intel::IXP4xx::INTR::IrqDrvApi&	getGPIO2() noexcept=0;
		/** */
		virtual Oscl::Intel::IXP4xx::INTR::IrqDrvApi&	getGPIO3() noexcept=0;
		/** */
		virtual Oscl::Intel::IXP4xx::INTR::IrqDrvApi&	getGPIO4() noexcept=0;
		/** */
		virtual Oscl::Intel::IXP4xx::INTR::IrqDrvApi&	getGPIO5() noexcept=0;
		/** */
		virtual Oscl::Intel::IXP4xx::INTR::IrqDrvApi&	getGPIO6() noexcept=0;
		/** */
		virtual Oscl::Intel::IXP4xx::INTR::IrqDrvApi&	getGPIO7() noexcept=0;
		/** */
		virtual Oscl::Intel::IXP4xx::INTR::IrqDrvApi&	getGPIO8() noexcept=0;
		/** */
		virtual Oscl::Intel::IXP4xx::INTR::IrqDrvApi&	getGPIO9() noexcept=0;
		/** */
		virtual Oscl::Intel::IXP4xx::INTR::IrqDrvApi&	getGPIO10() noexcept=0;
		/** */
		virtual Oscl::Intel::IXP4xx::INTR::IrqDrvApi&	getGPIO11() noexcept=0;
		/** */
		virtual Oscl::Intel::IXP4xx::INTR::IrqDrvApi&	getGPIO12() noexcept=0;
		/** */
		virtual Oscl::Intel::IXP4xx::INTR::IrqDrvApi&	getSwInterrupt0() noexcept=0;
		/** */
		virtual Oscl::Intel::IXP4xx::INTR::IrqDrvApi&	getSwInterrupt1() noexcept=0;
	};

}
}
}
}

#endif
