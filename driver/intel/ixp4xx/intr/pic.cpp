/*
   Copyright (C) 2006 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "pic.h"
#include "oscl/error/fatal.h"

#include "oscl/error/info.h"

using namespace Oscl::Intel::IXP4xx::INTR;

PIC::PIC(	volatile Oscl::IXP4XX::INTR::INTR_ENC_ST::Reg&		intr_enc_st,
			IrqApi&												vect00,
			IrqApi&												vect01,
			IrqApi&												vect02,
			IrqApi&												vect03,
			IrqApi&												vect04,
			IrqApi&												vect05,
			IrqApi&												vect06,
			IrqApi&												vect07,
			IrqApi&												vect08,
			IrqApi&												vect09,
			IrqApi&												vect0A,
			IrqApi&												vect0B,
			IrqApi&												vect0C,
			IrqApi&												vect0D,
			IrqApi&												vect0E,
			IrqApi&												vect0F,
			IrqApi&												vect10,
			IrqApi&												vect11,
			IrqApi&												vect12,
			IrqApi&												vect13,
			IrqApi&												vect14,
			IrqApi&												vect15,
			IrqApi&												vect16,
			IrqApi&												vect17,
			IrqApi&												vect18,
			IrqApi&												vect19,
			IrqApi&												vect1A,
			IrqApi&												vect1B,
			IrqApi&												vect1C,
			IrqApi&												vect1D,
			IrqApi&												vect1E,
			IrqApi&												vect1F
			) noexcept:
		_intr_enc_st(intr_enc_st)
		{
	_handler[0x00]=&_noInterrupt;
	_handler[0x01]=&vect00;
	_handler[0x02]=&vect01;
	_handler[0x03]=&vect02;
	_handler[0x04]=&vect03;
	_handler[0x05]=&vect04;
	_handler[0x06]=&vect05;
	_handler[0x07]=&vect06;
	_handler[0x08]=&vect07;
	_handler[0x09]=&vect08;
	_handler[0x0A]=&vect09;
	_handler[0x0B]=&vect0A;
	_handler[0x0C]=&vect0B;
	_handler[0x0D]=&vect0C;
	_handler[0x0E]=&vect0D;
	_handler[0x0F]=&vect0E;
	_handler[0x10]=&vect0F;
	_handler[0x11]=&vect10;
	_handler[0x12]=&vect11;
	_handler[0x13]=&vect12;
	_handler[0x14]=&vect13;
	_handler[0x15]=&vect14;
	_handler[0x16]=&vect15;
	_handler[0x17]=&vect16;
	_handler[0x18]=&vect17;
	_handler[0x19]=&vect18;
	_handler[0x1A]=&vect19;
	_handler[0x1B]=&vect1A;
	_handler[0x1C]=&vect1B;
	_handler[0x1D]=&vect1C;
	_handler[0x1E]=&vect1D;
	_handler[0x1F]=&vect1E;
	_handler[0x20]=&vect1F;
	}

void	PIC::reserve(	unsigned vector,
						Oscl::Interrupt::StatusHandlerApi& handler
						) noexcept{
	if(vector > (maxVectors-1)){
		Oscl::ErrorFatal::
		logAndExit("PIC: attempt to reserve illegal vector.\n");
		}
	_handler[vector+1]->reserve(handler);
	}

void	PIC::release(unsigned vector) noexcept{
	if(vector > (maxVectors-1)){
		Oscl::ErrorFatal::
		logAndExit("PIC: attempt to release illegal vector.\n");
		}
	_handler[vector+1]->release();
	}

IrqApi&	PIC::getIrqApi(unsigned vector) noexcept{
	if(vector > (maxVectors-1)){
		Oscl::ErrorFatal::
		logAndExit("PIC: attempt to get illegal vector.\n");
		}
	return *_handler[vector+1];
	}

bool	PIC::interrupt() noexcept{
	using namespace Oscl::IXP4XX::INTR::INTR_ENC_ST;
	return _handler[(_intr_enc_st & ENC_ST::FieldMask)>>ENC_ST::Lsb]->interrupt();
	}

