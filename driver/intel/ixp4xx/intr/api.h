/*
   Copyright (C) 2006 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_intel_ixp4xx_intr_apih_
#define _oscl_drv_intel_ixp4xx_intr_apih_
#include "oscl/interrupt/shandler.h"
#include "irqapi.h"

/** */
namespace Oscl {
/** */
namespace Intel {
/** */
namespace IXP4xx {
/** */
namespace INTR {

/** */
class Api {
	public:
		/** */
		virtual ~Api() {}
		/** */
		virtual Oscl::Interrupt::StatusHandlerApi&	getISR() noexcept=0;
		/** */
		virtual Oscl::Intel::IXP4xx::INTR::IrqApi&	getWAN_HSS_NPE() noexcept=0;
		/** */
		virtual Oscl::Intel::IXP4xx::INTR::IrqApi&	getNPEA() noexcept=0;
		/** */
		virtual Oscl::Intel::IXP4xx::INTR::IrqApi&	getNPEB() noexcept=0;
		/** */
		virtual Oscl::Intel::IXP4xx::INTR::IrqApi&	getQueueManager1_32() noexcept=0;
		/** */
		virtual Oscl::Intel::IXP4xx::INTR::IrqApi&	getQueueManager33_64() noexcept=0;
		/** */
		virtual Oscl::Intel::IXP4xx::INTR::IrqApi&	getTimers0() noexcept=0;
		/** */
		virtual Oscl::Intel::IXP4xx::INTR::IrqApi&	getGPIO0() noexcept=0;
		/** */
		virtual Oscl::Intel::IXP4xx::INTR::IrqApi&	getGPIO1() noexcept=0;
		/** */
		virtual Oscl::Intel::IXP4xx::INTR::IrqApi&	getPCI() noexcept=0;
		/** */
		virtual Oscl::Intel::IXP4xx::INTR::IrqApi&	getPciDmaChannel1() noexcept=0;
		/** */
		virtual Oscl::Intel::IXP4xx::INTR::IrqApi&	getPciDmaChannel2() noexcept=0;
		/** */
		virtual Oscl::Intel::IXP4xx::INTR::IrqApi&	getTimers1() noexcept=0;
		/** */
		virtual Oscl::Intel::IXP4xx::INTR::IrqApi&	getUSB() noexcept=0;
		/** */
		virtual Oscl::Intel::IXP4xx::INTR::IrqApi&	getConsoleUART() noexcept=0;
		/** */
		virtual Oscl::Intel::IXP4xx::INTR::IrqApi&	getTimestampTimer() noexcept=0;
		/** */
		virtual Oscl::Intel::IXP4xx::INTR::IrqApi&	getHighSpeedUART() noexcept=0;
		/** */
		virtual Oscl::Intel::IXP4xx::INTR::IrqApi&	getWatchdogTimer() noexcept=0;
		/** */
		virtual Oscl::Intel::IXP4xx::INTR::IrqApi&	getPmuCounter() noexcept=0;
		/** */
		virtual Oscl::Intel::IXP4xx::INTR::IrqApi&	getXscalePmuCounter() noexcept=0;
		/** */
		virtual Oscl::Intel::IXP4xx::INTR::IrqApi&	getGPIO2() noexcept=0;
		/** */
		virtual Oscl::Intel::IXP4xx::INTR::IrqApi&	getGPIO3() noexcept=0;
		/** */
		virtual Oscl::Intel::IXP4xx::INTR::IrqApi&	getGPIO4() noexcept=0;
		/** */
		virtual Oscl::Intel::IXP4xx::INTR::IrqApi&	getGPIO5() noexcept=0;
		/** */
		virtual Oscl::Intel::IXP4xx::INTR::IrqApi&	getGPIO6() noexcept=0;
		/** */
		virtual Oscl::Intel::IXP4xx::INTR::IrqApi&	getGPIO7() noexcept=0;
		/** */
		virtual Oscl::Intel::IXP4xx::INTR::IrqApi&	getGPIO8() noexcept=0;
		/** */
		virtual Oscl::Intel::IXP4xx::INTR::IrqApi&	getGPIO9() noexcept=0;
		/** */
		virtual Oscl::Intel::IXP4xx::INTR::IrqApi&	getGPIO10() noexcept=0;
		/** */
		virtual Oscl::Intel::IXP4xx::INTR::IrqApi&	getGPIO11() noexcept=0;
		/** */
		virtual Oscl::Intel::IXP4xx::INTR::IrqApi&	getGPIO12() noexcept=0;
		/** */
		virtual Oscl::Intel::IXP4xx::INTR::IrqApi&	getSwInterrupt0() noexcept=0;
		/** */
		virtual Oscl::Intel::IXP4xx::INTR::IrqApi&	getSwInterrupt1() noexcept=0;
	};

}
}
}
}

#endif
