/*
   Copyright (C) 2006 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "drv.h"

#include "oscl/error/info.h"

using namespace Oscl::Intel::IXP4xx::INTR;

Driver::Driver(Oscl::IXP4XX::INTR::InterruptController& intr) noexcept:
		_wan_hss_npe(intr),
		_npea(intr),
		_npeb(intr),
		_queueManager1_32(intr),
		_queueManager33_64(intr),
		_timer0(intr),
		_gpio0(intr),
		_gpio1(intr),
		_pci(intr),
		_pciDmaChannel1(intr),
		_pciDmaChannel2(intr),
		_timer1(intr),
		_usb(intr),
		_consoleUART(intr),
		_timestampTimer(intr),
		_highSpeedUART(intr),
		_watchdogTimer(intr),
		_pmu(intr),
		_xscalePmu(intr),
		_gpio2(intr),
		_gpio3(intr),
		_gpio4(intr),
		_gpio5(intr),
		_gpio6(intr),
		_gpio7(intr),
		_gpio8(intr),
		_gpio9(intr),
		_gpio10(intr),
		_gpio11(intr),
		_gpio12(intr),
		_swInterrupt0(intr),
		_swInterrupt1(intr)
		{
	}

Oscl::Intel::IXP4xx::INTR::IrqDrvApi&	Driver::getWAN_HSS_NPE() noexcept{
	return _wan_hss_npe;
	}

Oscl::Intel::IXP4xx::INTR::IrqDrvApi&	Driver::getNPEA() noexcept{
	return _npea;
	}

Oscl::Intel::IXP4xx::INTR::IrqDrvApi&	Driver::getNPEB() noexcept{
	return _npeb;
	}

Oscl::Intel::IXP4xx::INTR::IrqDrvApi&	Driver::getQueueManager1_32() noexcept{
	return _queueManager1_32;
	}

Oscl::Intel::IXP4xx::INTR::IrqDrvApi&	Driver::getQueueManager33_64() noexcept{
	return _queueManager33_64;
	}

Oscl::Intel::IXP4xx::INTR::IrqDrvApi&	Driver::getTimer0() noexcept{
	return _timer0;
	}

Oscl::Intel::IXP4xx::INTR::IrqDrvApi&	Driver::getGPIO0() noexcept{
	return _gpio0;
	}

Oscl::Intel::IXP4xx::INTR::IrqDrvApi&	Driver::getGPIO1() noexcept{
	return _gpio1;
	}

Oscl::Intel::IXP4xx::INTR::IrqDrvApi&	Driver::getPCI() noexcept{
	return _pci;
	}

Oscl::Intel::IXP4xx::INTR::IrqDrvApi&	Driver::getPciDmaChannel1() noexcept{
	return _pciDmaChannel1;
	}

Oscl::Intel::IXP4xx::INTR::IrqDrvApi&	Driver::getPciDmaChannel2() noexcept{
	return _pciDmaChannel2;
	}

Oscl::Intel::IXP4xx::INTR::IrqDrvApi&	Driver::getTimer1() noexcept{
	return _timer1;
	}

Oscl::Intel::IXP4xx::INTR::IrqDrvApi&	Driver::getUSB() noexcept{
	return _usb;
	}

Oscl::Intel::IXP4xx::INTR::IrqDrvApi&	Driver::getConsoleUART() noexcept{
	return _consoleUART;
	}

Oscl::Intel::IXP4xx::INTR::IrqDrvApi&	Driver::getTimestampTimer() noexcept{
	return _timestampTimer;
	}

Oscl::Intel::IXP4xx::INTR::IrqDrvApi&	Driver::getHighSpeedUART() noexcept{
	return _highSpeedUART;
	}

Oscl::Intel::IXP4xx::INTR::IrqDrvApi&	Driver::getWatchdogTimer() noexcept{
	return _watchdogTimer;
	}

Oscl::Intel::IXP4xx::INTR::IrqDrvApi&	Driver::getPmuCounter() noexcept{
	return _pmu;
	}

Oscl::Intel::IXP4xx::INTR::IrqDrvApi&	Driver::getXscalePmuCounter() noexcept{
	return _xscalePmu;
	}

Oscl::Intel::IXP4xx::INTR::IrqDrvApi&	Driver::getGPIO2() noexcept{
	return _gpio2;
	}

Oscl::Intel::IXP4xx::INTR::IrqDrvApi&	Driver::getGPIO3() noexcept{
	return _gpio3;
	}

Oscl::Intel::IXP4xx::INTR::IrqDrvApi&	Driver::getGPIO4() noexcept{
	return _gpio4;
	}

Oscl::Intel::IXP4xx::INTR::IrqDrvApi&	Driver::getGPIO5() noexcept{
	return _gpio5;
	}

Oscl::Intel::IXP4xx::INTR::IrqDrvApi&	Driver::getGPIO6() noexcept{
	return _gpio6;
	}

Oscl::Intel::IXP4xx::INTR::IrqDrvApi&	Driver::getGPIO7() noexcept{
	return _gpio7;
	}

Oscl::Intel::IXP4xx::INTR::IrqDrvApi&	Driver::getGPIO8() noexcept{
	return _gpio8;
	}

Oscl::Intel::IXP4xx::INTR::IrqDrvApi&	Driver::getGPIO9() noexcept{
	return _gpio9;
	}

Oscl::Intel::IXP4xx::INTR::IrqDrvApi&	Driver::getGPIO10() noexcept{
	return _gpio10;
	}

Oscl::Intel::IXP4xx::INTR::IrqDrvApi&	Driver::getGPIO11() noexcept{
	return _gpio11;
	}

Oscl::Intel::IXP4xx::INTR::IrqDrvApi&	Driver::getGPIO12() noexcept{
	return _gpio12;
	}

Oscl::Intel::IXP4xx::INTR::IrqDrvApi&	Driver::getSwInterrupt0() noexcept{
	return _swInterrupt0;
	}

Oscl::Intel::IXP4xx::INTR::IrqDrvApi&	Driver::getSwInterrupt1() noexcept{
	return _swInterrupt1;
	}

