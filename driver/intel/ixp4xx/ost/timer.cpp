/*
   Copyright (C) 2006 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "timer.h"
#include "oscl/error/info.h"

using namespace Oscl::Intel::IXP4xx::OST;

Timer::Timer(	volatile Oscl::IXP4XX::OST::OST_TIM::Reg&			ost_tim,
				volatile Oscl::IXP4XX::OST::OST_TIM_RL::Reg&		ost_tim_rl,
				volatile Oscl::IXP4XX::OST::OST_STATUS::Reg&		ost_status,
				Oscl::IXP4XX::OST::OST_STATUS::Reg					timerInterruptMask
				) noexcept:
		_ost_tim(ost_tim),
		_ost_tim_rl(ost_tim_rl),
		_ost_status(ost_status),
		_timerInterruptMask(timerInterruptMask)
		{
	// Initialize mode and disable timer
	ost_tim_rl	=	Oscl::IXP4XX::OST::OST_TIM_RL::Enable::ValueMask_Disable;
	// Clear pending interrupts
	ost_status	= timerInterruptMask;
	}

bool	Timer::interruptPending() noexcept{
	bool	result	= _ost_status & _timerInterruptMask;
	return result;
	}

void	Timer::clearInterrupt() noexcept{
	_ost_status	= _timerInterruptMask;
	}

void	Timer::startOneShotMode(uint32_t nTicks) noexcept{
	clearInterrupt();
	_ost_tim_rl	=		(nTicks & Oscl::IXP4XX::OST::OST_TIM_RL::ReloadValue::FieldMask)
					|	Oscl::IXP4XX::OST::OST_TIM_RL::Mode::ValueMask_OneShot
					|	Oscl::IXP4XX::OST::OST_TIM_RL::Enable::ValueMask_Enable
					;
	}

void	Timer::restartTimer() noexcept{
	uint32_t	value	= _ost_tim_rl;
	value	&= ~Oscl::IXP4XX::OST::OST_TIM_RL::Enable::FieldMask;
	value	|= Oscl::IXP4XX::OST::OST_TIM_RL::Enable::ValueMask_Enable;
	_ost_tim_rl	= value;
	}

void	Timer::stopTimer() noexcept{
	uint32_t	value	= _ost_tim_rl;
	value	&= ~Oscl::IXP4XX::OST::OST_TIM_RL::Enable::FieldMask;
	value	|= Oscl::IXP4XX::OST::OST_TIM_RL::Enable::ValueMask_Disable;
	_ost_tim_rl	= value;
	}

void	Timer::startReloadAndContinueMode(uint32_t periodInTicks) noexcept{
	clearInterrupt();
	_ost_tim_rl	=		(periodInTicks & Oscl::IXP4XX::OST::OST_TIM_RL::ReloadValue::FieldMask)
					|	Oscl::IXP4XX::OST::OST_TIM_RL::Mode::ValueMask_ReloadAndContinue
					|	Oscl::IXP4XX::OST::OST_TIM_RL::Enable::ValueMask_Enable
					;
	}

Oscl::IXP4XX::OST::OST_TIM::Reg	Timer::getCurrentCounterValue() noexcept{
	return _ost_tim;
	}

void	Timer::setCurrentCounterValue(Oscl::IXP4XX::OST::OST_TIM::Reg value) noexcept{
	_ost_tim	= value;
	}

