/*
   Copyright (C) 2006 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "driver.h"

#include "oscl/error/info.h"

using namespace Oscl::Intel::IXP4xx::OST;

Driver::Driver(	volatile Oscl::IXP4XX::OST::OST_TIM::Reg&			ost_tim,
				volatile Oscl::IXP4XX::OST::OST_TIM_RL::Reg&		ost_tim_rl,
				volatile Oscl::IXP4XX::OST::OST_STATUS::Reg&		ost_status,
				Oscl::IXP4XX::OST::OST_STATUS::Reg					timerInterruptMask,
				Oscl::IXP4XX::OST::OST_TIM_RL::Reg					period
				) noexcept:
		_timer(	ost_tim,
				ost_tim_rl,
				ost_status,
				timerInterruptMask
				),
		_period(period)
		{
	}

void	Driver::start() noexcept{
	_timer.clearInterrupt();
	_timer.startReloadAndContinueMode(_period);
	}

void	Driver::stop() noexcept{
	_timer.stopTimer();
	}

void	Driver::attach(Observer& observer) noexcept{
	_observers.put(&observer);
	}

void	Driver::detach(Observer& observer) noexcept{
	_observers.remove(&observer);
	}

bool	Driver::interrupt() noexcept{
//	Oscl::Error::Info::log("OST::Driver::interrupt");
	if(!_timer.interruptPending()){
		return false;
		}
	_timer.clearInterrupt();
	Observer*	observer;
	for(	observer=_observers.first();
			observer;
			observer=_observers.next(observer)
			){
//		Oscl::Error::Info::log("OST::Driver::interrupt observer->tick()");
		observer->tick();
		}
	return true;
	}

