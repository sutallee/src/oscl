/*
   Copyright (C) 2006 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_intel_ixp4xx_ost_timerh_
#define _oscl_drv_intel_ixp4xx_ost_timerh_
#include "oscl/hw/intel/ixp4xx/ost.h"

/** */
namespace Oscl {
/** */
namespace Intel {
/** */
namespace IXP4xx {
/** */
namespace OST {

/** */
class Timer {
	private:
		/** */
		volatile Oscl::IXP4XX::OST::OST_TIM::Reg&			_ost_tim;
		/** */
		volatile Oscl::IXP4XX::OST::OST_TIM_RL::Reg&		_ost_tim_rl;
		/** */
		volatile Oscl::IXP4XX::OST::OST_STATUS::Reg&		_ost_status;
		/** */
		Oscl::IXP4XX::OST::OST_STATUS::Reg					_timerInterruptMask;

	public:
		/** */
		Timer(	volatile Oscl::IXP4XX::OST::OST_TIM::Reg&			ost_tim,
				volatile Oscl::IXP4XX::OST::OST_TIM_RL::Reg&		ost_tim_rl,
				volatile Oscl::IXP4XX::OST::OST_STATUS::Reg&		ost_status,
				Oscl::IXP4XX::OST::OST_STATUS::Reg					timerInterruptMask
				) noexcept;
		/** */
		bool	interruptPending() noexcept;
		/** */
		void	clearInterrupt() noexcept;
		/** */
		void	startOneShotMode(uint32_t nTicks) noexcept;
		/** */
		void	restartTimer() noexcept;
		/** */
		void	stopTimer() noexcept;
		/** */
		void	startReloadAndContinueMode(uint32_t periodInTicks) noexcept;
		/** */
		Oscl::IXP4XX::OST::OST_TIM::Reg	getCurrentCounterValue() noexcept;
		/** */
		void	setCurrentCounterValue(Oscl::IXP4XX::OST::OST_TIM::Reg value) noexcept;
	};

}
}
}
}

#endif
