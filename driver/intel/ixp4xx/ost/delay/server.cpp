/*
   Copyright (C) 2006 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "server.h"

#include "oscl/error/info.h"

using namespace Oscl::Intel::IXP4xx::OST::Delay;

Server::Server(unsigned long ostTickFreqInHz) noexcept:
		_delayService(),
		_sap(_delayService,*this),
		_ticksPerMillisecond((ostTickFreqInHz+(1000-1))/1000),
		_millisecondsPerTock(1000/ostTickFreqInHz),
		_tickCount(_ticksPerMillisecond)
		{
	if(_millisecondsPerTock > ((unsigned)~0)){
		// _tickCount not big enough to hold that many ticks
		while(true);
		}
	_millisecondsPerTock	= (_millisecondsPerTock)?_millisecondsPerTock:1;
	_ticksPerMillisecond	= (_ticksPerMillisecond)?_ticksPerMillisecond:1;
	}

Oscl::Mt::Itc::Delay::Req::Api::SAP&	Server::getSAP(){
	return _sap;
	}

void	Server::mboxSignaled() noexcept{
	// tock
	Oscl::Error::Info::log("Delay::Server::mboxSignaled()");
	_delayService.tick(_millisecondsPerTock);
	}

void	Server::tick() noexcept{
//	Oscl::Error::Info::log("Delay::Server::tick()");
	--_tickCount;
	if(_tickCount) return;
	// no need to wake up the delay service if
	// there are no delay requests pending.
	if(_delayService.delayPending()){
		Oscl::Error::Info::log("Delay::Server::tick() tock");
		suSignal();	// tock
		}
	_tickCount	= _ticksPerMillisecond;
	}

