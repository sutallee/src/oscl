/*
   Copyright (C) 2006 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_drv_intel_ixp4xx_ost_apih_
#define _oscl_drv_intel_ixp4xx_ost_apih_
#include "oscl/interrupt/shandler.h"
#include "oscl/queue/queueitem.h"

/** */
namespace Oscl {
/** */
namespace Intel {
/** */
namespace IXP4xx {
/** */
namespace OST {

/** */
class Observer : public Oscl::QueueItem {
	public:
		/** */
		virtual ~Observer(){}

		/** This operation is called every OST
			period.
		 */
		virtual void	tick() noexcept=0;
	};

/** */
class Api : public Oscl::Interrupt::StatusHandlerApi {
	public:
		/** */
		virtual void	start() noexcept=0;

		/** */
		virtual void	stop() noexcept=0;

		/** */
		virtual void	attach(Observer& observer) noexcept=0;

		/** */
		virtual void	detach(Observer& observer) noexcept=0;

		/** */
		virtual bool	interrupt() noexcept=0;
	};

}
}
}
}

#endif
