/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_string_output_parth_
#define _oscl_string_output_parth_

#include "oscl/string/observer/reqapi.h"
#include "oscl/queue/queue.h"
#include "oscl/string/control/api.h"
#include "oscl/strings/base.h"

/** */
namespace Oscl {

/** */
namespace String {

/** */
namespace Output {

/** */
class Part :
		public Oscl::String::Observer::Req::Api,
		public Oscl::String::Control::Api
		{
	private:
		/** */
		Oscl::String::Observer::Req::Api::ConcreteSAP	_sap;

		/** */
		Oscl::Queue<Oscl::String::Observer::Req::Api::ChangeReq>	_noteQ;

		/** This variable contains the current state of the
			monitor point.
		 */
		Oscl::Strings::Base											_state;

	public:
		/** */
		Part(
			Oscl::Mt::Itc::PostMsgApi&	papi,
			char*						buffer,
			unsigned long				maxBufferSize,
			const char*					initialState
			) noexcept;

		/** */
		Oscl::String::Observer::Req::Api::SAP&	getSAP() noexcept;

	private:
		/** */
		void	notify(const char* state) noexcept;

		/** */
		void	update(Oscl::String::Observer::Req::Api::ChangeReq& msg, Oscl::Strings::Api& api) noexcept;

	public: // Oscl::String::Control::Api
		/** */
		void	update(const char* state) noexcept;

	public:	// Oscl::String::Observer::Req::Api
		/** */
		void request(Oscl::String::Observer::Req::Api::ChangeReq& msg) noexcept;

		/** */
		void request(Oscl::String::Observer::Req::Api::CancelChangeReq& msg) noexcept;
	};

}
}
}

#endif
