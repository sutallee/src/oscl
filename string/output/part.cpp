#include "part.h"
#include "oscl/strings/base.h"

using namespace Oscl::String::Output;

Part::Part(
	Oscl::Mt::Itc::PostMsgApi&	papi,
	char*						buffer,
	unsigned long				maxBufferSize,
	const char*					initialState
	) noexcept:
		_sap(
			*this,
			papi
			),
		_state(
			buffer,
			maxBufferSize,
			initialState
			)
		{
	}

Oscl::String::Observer::Req::Api::SAP&	Part::getSAP() noexcept{
	return _sap;
	}

void	Part::notify(const char* state) noexcept{
	Oscl::Strings::Api&
	api	= _state;

	api	= state;

	Oscl::String::Observer::Req::Api::ChangeReq* note;

	while((note=_noteQ.get())){
		update(*note,_state);
		note->returnToSender();
		}
	}

void	Part::update(
			Oscl::String::Observer::Req::Api::ChangeReq&	msg,
			Oscl::Strings::Api&								api
			) noexcept{
	Oscl::Strings::Base	output(
		msg.getPayload()._state,
		msg.getPayload()._maxStringLength
		);

	Oscl::Strings::Api&
	stringApi	= output;

	stringApi	= api;
	}

void Part::request(Oscl::String::Observer::Req::Api::ChangeReq& msg) noexcept{
	if(_state == msg.getPayload()._state){
		_noteQ.put(&msg);
		}
	else{
		update(msg,_state);
		msg.returnToSender();
		}
	}

void Part::request(Oscl::String::Observer::Req::Api::CancelChangeReq& msg) noexcept{
	for(
		Oscl::String::Observer::Req::Api::ChangeReq* next=_noteQ.first();
		next;
		next=_noteQ.next(next)
		){
		if(next == &msg.getPayload()._requestToCancel){
			_noteQ.remove(next);
			next->returnToSender();
			break;
			}
		}
	msg.returnToSender();
	}

void	Part::update(const char* state) noexcept{
	if(_state != state){
		notify(state);
		}
	}

