/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_string_observer_itc_respmemcfgh_
#define _oscl_string_observer_itc_respmemcfgh_

#include "respmem.h"

/** */
namespace Oscl {

/** */
namespace String {

/** */
namespace Observer {

/** */
namespace Resp {

/**	This record describes a block of memory that is
	large enough (and properly aligned) to hold a
	ChangeResp and its corresponding Req::Api::ChangePayload.
	Clients which use the corresponding server asynchronously
	frequently need to reservere/manage memory for for these
	ITC interactions and instances of this record are useful
	for that purpose.
 */
template <unsigned long strSize>
struct ChangeMemCfg {
	/**	This is memory enough for the response message.
	 */
	Oscl::String::Observer::Resp::ChangeMem	_mem;

	/** */
	char	_buffer[strSize+1];

	/**	This operation creates a response message using this
		 block of memory.
	 */
	Oscl::String::Observer::Resp::Api::ChangeResp&
		build(	Oscl::String::Observer::Req::Api::SAP&	sap,
				Oscl::String::Observer::Resp::Api&		respApi,
				Oscl::Mt::Itc::PostMsgApi&		clientPapi
				) noexcept;
	};

template <unsigned long strSize>
Oscl::String::Observer::Resp::Api::ChangeResp&
ChangeMemCfg<strSize>::build(
	Oscl::String::Observer::Req::Api::SAP&	sap,
	Oscl::String::Observer::Resp::Api&		respApi,
	Oscl::Mt::Itc::PostMsgApi&				clientPapi
	) noexcept{
	return _mem.build(
			sap,
			respApi,
			clientPapi,
			_buffer,
			strSize
			);
	}

}
}
}
}

#endif
