/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_endian_bigh_
#define _oscl_endian_bigh_
#include <stdint.h>
/* This file should only be included by Big Endian machines.
	This file should only included indirectly by the
	"compiler/endian.h" file.
 */

namespace Oscl {
namespace Endian {

inline uint16_t	fromLittleEndian(uint16_t le) noexcept{
	return (le<<8) | ((le>>8) & 0x00FF);
	}

inline uint16_t	fromBigEndian(uint16_t be) noexcept{
	return be;
	}

inline uint32_t	fromLittleEndian(uint32_t le) noexcept{
	uint32_t	result	= ((le&0xFF)<<24);
	result	=  (le<<24)	& 0xFF000000;
	result	|= (le<<8)	& 0x00FF0000;
	result	|= (le>>8)	& 0x0000FF00;
	result	|= (le>>24)	& 0x000000FF;
	return result;
	}

inline uint32_t	fromBigEndian(uint32_t be) noexcept{
	return be;
	}

inline uint16_t	toLittleEndian(uint16_t native) noexcept{
	return (native<<8) | ((native>>8) & 0x00FF);
	}

inline uint16_t	toBigEndian(uint16_t native) noexcept{
	return native;
	}

inline uint32_t	toLittleEndian(uint32_t native) noexcept{
	uint32_t	result	= ((native&0xFF)<<24);
	result	=  (native<<24)	& 0xFF000000;
	result	|= (native<<8)	& 0x00FF0000;
	result	|= (native>>8)	& 0x0000FF00;
	result	|= (native>>24)	& 0x000000FF;
	return result;
	}

inline uint32_t	toBigEndian(uint32_t native) noexcept{
	return native;
	}

inline int16_t	fromLittleEndian(int16_t le) noexcept{
	return (le<<8) | ((le>>8) & 0x00FF);
	}

inline int16_t	fromBigEndian(int16_t be) noexcept{
	return be;
	}

inline int32_t	fromLittleEndian(int32_t le) noexcept{
	int32_t	result	= ((le&0xFF)<<24);
	result	=  (le<<24)	& 0xFF000000;
	result	|= (le<<8)	& 0x00FF0000;
	result	|= (le>>8)	& 0x0000FF00;
	result	|= (le>>24)	& 0x000000FF;
	return result;
	}

inline int32_t	fromBigEndian(int32_t be) noexcept{
	return be;
	}

inline int16_t	toLittleEndian(int16_t native) noexcept{
	return (native<<8) | ((native>>8) & 0x00FF);
	}

inline int16_t	toBigEndian(int16_t native) noexcept{
	return native;
	}

inline int32_t	toLittleEndian(int32_t native) noexcept{
	int32_t	result	= ((native&0xFF)<<24);
	result	=  (native<<24)	& 0xFF000000;
	result	|= (native<<8)	& 0x00FF0000;
	result	|= (native>>8)	& 0x0000FF00;
	result	|= (native>>24)	& 0x000000FF;
	return result;
	}

inline int32_t	toBigEndian(int32_t native) noexcept{
	return native;
	}

}
}

#endif
