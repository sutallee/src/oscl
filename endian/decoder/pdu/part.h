/*
   Copyright (C) 2020 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_endian_decoder_pdu_parth_
#define _oscl_endian_decoder_pdu_parth_

#include "oscl/endian/decoder/api.h"
#include "le.h"
#include "be.h"
#include "state.h"

/** */
namespace Oscl {
/** */
namespace Endian {
/** */
namespace Decoder {
/** */
namespace PDU {

/** */
class Part: public Oscl::Endian::Decoder::Api {
	private:
		/** */
		Oscl::Endian::Decoder::PDU::State	_state;

		/** */
		Oscl::Endian::Decoder::PDU::BigEndian	_bigEndian;

		/** */
		Oscl::Endian::Decoder::PDU::LittleEndian	_littleEndian;

	public:
		/** */
		Part(
			Oscl::Pdu::Pdu*	pdu
			) noexcept;

		/** Get the big-endian decoder.
		 */
		Oscl::Decoder::Api&	be() noexcept;

		/** Get the little-endian decoder.
		 */
		Oscl::Decoder::Api&	le() noexcept;
	};
}
}
}
}

#endif
