/*
   Copyright (C) 2020 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_endian_decoder_pdu_leh_
#define _oscl_endian_decoder_pdu_leh_

#include "oscl/decoder/api.h"
#include "state.h"

/** */
namespace Oscl {
/** */
namespace Endian {
/** */
namespace Decoder {
/** */
namespace PDU {

/** */
class LittleEndian: public Oscl::Decoder::Api {
	private:
		/** */
		Oscl::Endian::Decoder::PDU::State&	_state;

	public:
		/** */
		LittleEndian(
			Oscl::Endian::Decoder::PDU::State&	state
			) noexcept;

		/** */
		unsigned	remaining() const noexcept;

		/** */
		void	push(
					Oscl::Decoder::State&	state,
					unsigned				remaining
					) noexcept;

		/** */
		unsigned	pop() noexcept;

		/** */
		void	reset() noexcept;

		/** */
		bool	underflow() const noexcept;

		/** */
		void	skip(unsigned n) noexcept;

		/** */
		void	forceAlign(unsigned octets) noexcept;

		/** */
		void	decode(uint8_t& v) noexcept;

		/** */
		void	decode(int8_t& v) noexcept;

		/** */
		void	decode(uint16_t& v) noexcept;

		/** */
		void	decode(int16_t& v) noexcept;

		/** */
		void	decode(uint32_t& v) noexcept;

		/** */
		void	decode24Bit(uint32_t& v) noexcept;

		/** */
		void	decode(int32_t& v) noexcept;

		/** */
		void	decode(uint64_t& v) noexcept;

		/** */
		void	decode(int64_t& v) noexcept;

		/** */
		void	decode(double& v) noexcept;

		/** */
		void	copyOut(void* buffer, unsigned length) noexcept;

		/** */
		bool	align(unsigned octets) noexcept;
	};
}
}
}
}

#endif
