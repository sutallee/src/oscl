/*
   Copyright (C) 2020 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "be.h"

using namespace Oscl::Endian::Decoder::PDU;

BigEndian::BigEndian(
	Oscl::Endian::Decoder::PDU::State&	state
	) noexcept:
		_state(state)
		{
	}

unsigned	BigEndian::remaining() const noexcept{
	return _state.remaining();
	}

void	BigEndian::push(
			Oscl::Decoder::State&	state,
			unsigned				remaining
			) noexcept{

	_state.push(
		state,
		remaining
		);
	}

unsigned	BigEndian::pop() noexcept{
	return _state.pop();
	}

bool	BigEndian::underflow() const noexcept{
	return _state.underflow();
	}

void	BigEndian::skip(unsigned n) noexcept{
	_state.skip(n);
	}

void	BigEndian::forceAlign(unsigned octets) noexcept{
	_state.forceAlign(octets);
	}

void	BigEndian::decode(uint8_t& v) noexcept{

	if(_state._offset+1 > _state._bufferLength){
		_state._underflow	= true;
		return;
		}

	unsigned
	n	= _state._pdu->read(
			&v,
			1,
			_state._offset
			);

	if(n == 1){
		_state._underflow	= false;
		++_state._offset;
		}
	else {
		_state._underflow	= true;
		}
	}

void	BigEndian::decode(int8_t& v) noexcept{
	decode((uint8_t&)v);
	}

void	BigEndian::decode(uint16_t& v) noexcept{

	if(_state.align(2)){
		return;
		}

	if(_state._offset+2 > _state._bufferLength){
		_state._underflow	= true;
		return;
		}

	uint8_t	buf[2];

	unsigned
	n	= _state._pdu->read(
			buf,
			2,
			_state._offset
			);

	if(n < 2){
		_state._underflow	= true;
		return;
		}

	_state._underflow	= false;

	v	=	(buf[0] & 0x00FF);
	v	<<= 8;
	v	|=	(buf[1] & 0x00FF);

	_state._offset += 2;
	}

void	BigEndian::decode(int16_t& v) noexcept{
	decode((uint16_t&)v);
	}

void	BigEndian::decode(uint32_t& v) noexcept{

	if(_state.align(4)){
		return;
		}

	if(_state._offset+4 > _state._bufferLength){
		_state._underflow	= true;
		return;
		}

	uint8_t	buf[4];

	unsigned
	n	= _state._pdu->read(
			buf,
			4,
			_state._offset
			);

	if(n < 4){
		_state._underflow	= true;
		return;
		}

	_state._underflow	= false;

	v	=	(buf[0] & 0x00FF);
	v	<<= 8;
	v	|=	(buf[1] & 0x00FF);
	v	<<= 8;
	v	|=	(buf[2] & 0x00FF);
	v	<<= 8;
	v	|=	(buf[3] & 0x00FF);

	_state._offset += 4;
	}

void	BigEndian::decode(int32_t& v) noexcept{
	decode((uint32_t&)v);
	}

void	BigEndian::decode24Bit(uint32_t& v) noexcept{

	if(_state._offset+3 > _state._bufferLength){
		_state._underflow	= true;
		return;
		}

	uint8_t	buf[3];

	unsigned
	n	= _state._pdu->read(
			buf,
			3,
			_state._offset
			);

	if(n < 3){
		_state._underflow	= true;
		return;
		}

	_state._underflow	= false;

	v	=	(buf[0] & 0x00FF);
	v	<<= 8;
	v	|=	(buf[1] & 0x00FF);
	v	<<= 8;
	v	|=	(buf[2] & 0x00FF);

	_state._offset += 3;
	}

void	BigEndian::decode(uint64_t& v) noexcept{

	if(_state.align(8)){
		return;
		}

	if(_state._offset+8 > _state._bufferLength){
		_state._underflow	= true;
		return;
		}

	uint8_t	buf[8];

	unsigned
	n	= _state._pdu->read(
			buf,
			8,
			_state._offset
			);

	if(n < 8){
		_state._underflow	= true;
		return;
		}

	_state._underflow	= false;

	v	=	(buf[0] & 0x00FF);
	v	<<= 8;
	v	|=	(buf[1] & 0x00FF);
	v	<<= 8;
	v	|=	(buf[2] & 0x00FF);
	v	<<= 8;
	v	|=	(buf[3] & 0x00FF);
	v	<<= 8;
	v	|=	(buf[4] & 0x00FF);
	v	<<= 8;
	v	|=	(buf[5] & 0x00FF);
	v	<<= 8;
	v	|=	(buf[6] & 0x00FF);
	v	<<= 8;
	v	|=	(buf[7] & 0x00FF);

	_state._offset += 8;
	}

void	BigEndian::decode(int64_t& v) noexcept{
	decode((uint64_t&)v);
	}

void	BigEndian::decode(double& v) noexcept{

	union{
		double		d;
		uint64_t	u;
		}	magicUnion;

	decode(magicUnion.u);

	if(_state._underflow){
		return;
		}

	v	= magicUnion.d;
	}

bool	BigEndian::align(unsigned bytes) noexcept{
	return _state.align(bytes);
	}

void	BigEndian::copyOut(void* buffer, unsigned length) noexcept{
	_state.copyOut(buffer,length);
	}

