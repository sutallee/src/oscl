/*
   Copyright (C) 2020 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "state.h"
#include "oscl/error/info.h"

using namespace Oscl::Endian::Decoder::Linear;

State::State(
	const void*	buffer,
	unsigned	bufferLength
	) noexcept:
		_buffer((const uint8_t*)buffer),
		_bufferLength(bufferLength),
		_offset(0),
		_underflow(false)
		{
	}

unsigned	State::remaining() const noexcept{
	return _bufferLength-_offset;
	}

void	State::push(
			Oscl::Decoder::State&	state,
			unsigned				remaining
			) noexcept{

	_stack.push(&state);

	state._offset			= _offset;
	state._length	= _bufferLength;

	_bufferLength			= _offset+remaining;
	}

unsigned	State::pop() noexcept{
	Oscl::Decoder::State*
	state	= _stack.pop();

	if(!state){
		Oscl::Error::Info::log(
			"%s: pop without push.\n",
			OSCL_PRETTY_FUNCTION
			);
		return 0;
		}

	unsigned	len		= _offset-state->_offset;

	_offset			= state->_offset;
	_bufferLength	= state->_length;
	_underflow		= (_offset > _bufferLength)?true:false;

	return len;
	}

bool	State::align(unsigned bytes) noexcept{
	return false;
	}

bool	State::underflow() const noexcept{
	return _underflow;
	}

void	State::skip(unsigned n) noexcept{

	if((_offset + n) > _bufferLength){
		_underflow	= true;
		return;
		}

	_offset	+= n;
	}

void	State::forceAlign(unsigned octets) noexcept{

	unsigned 	offset	= _offset;

	offset	+= octets-1;

	offset	/= octets;

	offset	*= octets;

	if(offset > _bufferLength){
		_underflow	=  true;
		return;
		}

	_offset	= offset;
	}

void	State::copyOut(void* buffer, unsigned length) noexcept{

	if(_underflow){
		return;
		}

	unsigned	remain	= remaining();

	unsigned	len	= (length > remain)?remain:length;

	uint8_t*	p	= (uint8_t*)buffer;

	for(unsigned i=0;i<len;++i){
		p[i]	= _buffer[_offset+i];
		}

	_offset	+= len;

	if(len < length){
		_underflow	= true;
		}
	}

const char*	State::decodeString() noexcept{

	unsigned	i;

	for(i=_offset; i<_bufferLength; ++i){
		if(!_buffer[i]){
			break;
			}
		}

	if(i < _bufferLength){
		const char*	p = (const char*)&_buffer[_offset];
		++i;
		_offset	= i;
		return p; 
		}

	return 0;
	}

