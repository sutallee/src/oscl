/*
   Copyright (C) 2020 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "be.h"

using namespace Oscl::Endian::Decoder::Linear;

BigEndian::BigEndian(
	Oscl::Endian::Decoder::Linear::State&	state
	) noexcept:
		_state(state)
		{
	}

unsigned	BigEndian::remaining() const noexcept{
	return _state.remaining();
	}

void	BigEndian::push(
			Oscl::Decoder::State&	state,
			unsigned				remaining
			) noexcept{

	_state.push(
		state,
		remaining
		);
	}

unsigned	BigEndian::pop() noexcept{
	return _state.pop();
	}

bool	BigEndian::underflow() const noexcept{
	return _state.underflow();
	}

void	BigEndian::skip(unsigned n) noexcept{
	_state.skip(n);
	}

void	BigEndian::forceAlign(unsigned octets) noexcept{
	_state.forceAlign(octets);
	}

void	BigEndian::decode(uint8_t& v) noexcept{
	if(_state._offset+1 > _state._bufferLength){
		_state._underflow	= true;
		return;
		}
	_state._underflow	= false;
	v	= _state._buffer[_state._offset];
	++_state._offset;
	}

void	BigEndian::decode(int8_t& v) noexcept{
	decode((uint8_t&)v);
	}

void	BigEndian::decode(uint16_t& v) noexcept{

	if(_state.align(2)){
		return;
		}

	if(_state._offset+2 > _state._bufferLength){
		_state._underflow	= true;
		return;
		}

	_state._underflow	= false;

	v	=	(_state._buffer[_state._offset+0] & 0x00FF);
	v	<<= 8;
	v	|=	(_state._buffer[_state._offset+1] & 0x00FF);

	_state._offset += 2;
	}

void	BigEndian::decode(int16_t& v) noexcept{
	decode((uint16_t&)v);
	}

void	BigEndian::decode(uint32_t& v) noexcept{

	if(_state.align(4)){
		return;
		}

	if(_state._offset+4 > _state._bufferLength){
		_state._underflow	= true;
		return;
		}

	_state._underflow	= false;

	v	=	(_state._buffer[_state._offset+0] & 0x00FF);
	v	<<= 8;
	v	|=	(_state._buffer[_state._offset+1] & 0x00FF);
	v	<<= 8;
	v	|=	(_state._buffer[_state._offset+2] & 0x00FF);
	v	<<= 8;
	v	|=	(_state._buffer[_state._offset+3] & 0x00FF);

	_state._offset += 4;
	}

void	BigEndian::decode(int32_t& v) noexcept{
	decode((uint32_t&)v);
	}

void	BigEndian::decode24Bit(uint32_t& v) noexcept{
	if(_state._offset+3 > _state._bufferLength){
		_state._underflow	= true;
		return;
		}

	_state._underflow	= false;

	v	=	(_state._buffer[_state._offset+0] & 0x00FF);
	v	<<= 8;
	v	|=	(_state._buffer[_state._offset+1] & 0x00FF);
	v	<<= 8;
	v	|=	(_state._buffer[_state._offset+2] & 0x00FF);

	_state._offset += 3;
	}

void	BigEndian::decode(uint64_t& v) noexcept{
	if(_state.align(8)){
		return;
		}

	if(_state._offset+8 > _state._bufferLength){
		_state._underflow	= true;
		return;
		}

	_state._underflow	= false;

	v	=	(_state._buffer[_state._offset+0] & 0x00FF);
	v	<<= 8;
	v	|=	(_state._buffer[_state._offset+1] & 0x00FF);
	v	<<= 8;
	v	|=	(_state._buffer[_state._offset+2] & 0x00FF);
	v	<<= 8;
	v	|=	(_state._buffer[_state._offset+3] & 0x00FF);
	v	<<= 8;
	v	|=	(_state._buffer[_state._offset+4] & 0x00FF);
	v	<<= 8;
	v	|=	(_state._buffer[_state._offset+5] & 0x00FF);
	v	<<= 8;
	v	|=	(_state._buffer[_state._offset+6] & 0x00FF);
	v	<<= 8;
	v	|=	(_state._buffer[_state._offset+7] & 0x00FF);

	_state._offset += 8;
	}

void	BigEndian::decode(int64_t& v) noexcept{
	decode((uint64_t&)v);
	}

void	BigEndian::decode(double& v) noexcept{
	union{
		double		d;
		uint64_t	u;
		}	magicUnion;

	decode(magicUnion.u);

	if(_state._underflow){
		return;
		}

	v	= magicUnion.d;
	}

bool	BigEndian::align(unsigned bytes) noexcept{
	return _state.align(bytes);
	}

const char*	BigEndian::decodeString() noexcept{
	return _state.decodeString();
	}

const void*	BigEndian::decodeSequence(uint32_t& length) noexcept{

	uint32_t	len;
	unsigned	offset	= _state._offset;

	decode(len);

	if(underflow()){
		return 0;
		}

	if(_state._offset + len > _state._bufferLength){
		_state._underflow	= true;
		_state._offset		= offset;
		return 0;
		}

	const void*	p	= &_state._buffer[_state._offset];

	_state._offset	+= len;

	length	= len;

	return p;
	}

const void*	BigEndian::decodeSequence(uint16_t& length) noexcept{

	uint16_t	len;
	unsigned	offset	= _state._offset;

	decode(len);

	if(underflow()){
		return 0;
		}

	if(_state._offset + len > _state._bufferLength){
		_state._underflow	= true;
		_state._offset		= offset;
		return 0;
		}

	const void*	p	= &_state._buffer[_state._offset];

	_state._offset	+= len;

	length	= len;

	return p;
	}

const void*	BigEndian::decodeSequence(uint8_t& length) noexcept{

	uint8_t		len;
	unsigned	offset	= _state._offset;

	decode(len);

	if(underflow()){
		return 0;
		}

	if(_state._offset + len > _state._bufferLength){
		_state._underflow	= true;
		_state._offset		= offset;
		return 0;
		}

	const void*	p	= &_state._buffer[_state._offset];

	_state._offset	+= len;

	length	= len;

	return p;
	}

void	BigEndian::copyOut(void* buffer, unsigned length) noexcept{
	_state.copyOut(buffer,length);
	}

