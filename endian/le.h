/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_endian_leh_
#define _oscl_endian_leh_
#include <stdint.h>

/** */
namespace Oscl {
/** */
namespace Endian {

/** This template represents a little endian type
	as an array of bytes. This prevents problems
	that are caused by alignment issues since the
	type is byte aligned.
 */
template <class iType>
class LE {
	private:
		/** Value stored in little-endian format.
		 */
		uint8_t	_leValue[sizeof(iType)];
	public:
		/** */
		inline iType	value() const noexcept{
			const uint8_t*	p	= &_leValue[sizeof(iType)-1];
			iType	v	= *p;
			--p;
			for(unsigned i=1;i<sizeof(iType);++i,--p){
				v	<<=	8;
				v	|=	*p;
				}
			return v;
			}
		/** */
		inline void	value(iType v) noexcept{
			_leValue[0]	= (uint8_t)v;
			for(unsigned i=1;i<sizeof(iType);++i){
				v	>>=	8;
				_leValue[i]	= (uint8_t)v;
				}
			}
		/** */
		inline operator iType() const noexcept{
			return value();
			}
		/** */
		inline LE	operator=(iType native) noexcept{
			value(native);
			return *this;
			}
		/** */
		inline LE	operator&=(iType native) noexcept{
			iType	x	= value();
			x	&= native;
			value(x);
			return *this;
			}
		/** */
		inline LE	operator|=(iType native) noexcept{
			iType	x	= value();
			x	|= native;
			value(x);
			return *this;
			}
		/** */
		inline LE	operator^=(iType native) noexcept{
			iType	x	= value();
			x	^= native;
			value(x);
			return *this;
			}
		/** */
		inline LE	operator+=(iType native) noexcept{
			iType	x	= value();
			x	+= native;
			value(x);
			return *this;
			}
		/** */
		inline LE	operator*=(iType native) noexcept{
			iType	x	= value();
			x	*= native;
			value(x);
			return *this;
			}
		/** */
		inline LE	operator/=(iType native) noexcept{
			iType	x	= value();
			x	/= native;
			value(x);
			return *this;
			}
		/** */
		inline LE	operator%=(iType native) noexcept{
			iType	x	= value();
			x	%= native;
			value(x);
			return *this;
			}
		/** */
		inline LE	operator-=(iType native) noexcept{
			iType	x	= value();
			x	-= native;
			value(x);
			return *this;
			}
		/** */
		inline LE	operator<<=(iType native) noexcept{
			iType	x	= value();
			x	<<= native;
			value(x);
			return *this;
			}
		/** */
		inline LE	operator>>=(iType native) noexcept{
			iType	x	= value();
			x	>>= native;
			value(x);
			return *this;
			}

		//=========== BitField Operations ==================
		/** Change the bits, specified as a mask of affectedBits,
			to the values specified in the bitValues mask.
		 */
		inline LE	changeBits(	iType	affectedBits,
								iType	bitValues
								) noexcept{
			iType	x	= value();
			x	&= ~affectedBits;
			x	|= (bitValues & affectedBits);
			value(x);
			return *this;
			}

		/** Clear the bits specified as a mask of affectedBits.
		 */
		inline LE	clearBits(iType affectedBits) noexcept{
			iType	x	= value();
			x	&= ~affectedBits;
			value(x);
			return *this;
			}

		/** Set the bits specified as a mask of affectedBits.
		 */
		inline LE	setBits(iType affectedBits) noexcept{
			iType	x	= value();
			x	|= affectedBits;
			_leValue	= value(x);
			return *this;
			}

		/** Set the bits specified as a mask of affectedBits
			if all of the affected bits are zero, otherwise
			the bits are unchanged. Returns true if the
			affected bits were changed to ones by this invocation 
			of the operation, and false if any of the bits were
			set to one prior to its invocation. This operation
			is NOT atomic.
		 */
		bool	testAndSet(iType affectedBits) noexcept{
			if(equal(affectedBits,0)){
				iType	x	= value();
				x	|= affectedBits;
				_leValue	= value(x);
				return true;
				}
			return false;
			}

		/** Compare the interestingBits of the field with the
			values of the corresponding bits of the bitValues mask.
		 */
		inline bool	equal(	iType	interestingBits,
							iType	bitValues
							) const noexcept{
			iType	x	= value();
			return (x & interestingBits) == (bitValues & interestingBits);
			}

		/** Explicitly read the whole value of the bit field.
			Operation is not necessarily const if this field
			represents a register which has side effects when read.
		 */
		inline iType	read() const noexcept{
			iType	x	= value();
			return x;
			}

	};

/** */
typedef LE<uint16_t>	LEU16;
/** */
typedef LE<uint32_t>	LEU32;
/** */
typedef LE<int16_t>	LES16;
/** */
typedef LE<int32_t>	LES32;

}
}

#endif

