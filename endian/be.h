/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_endian_beh_
#define _oscl_endian_beh_
#include <stdint.h>

/** */
namespace Oscl {
/** */
namespace Endian {

/** This template represents a big endian type
	as an array of bytes. This prevents problems
	that are caused by alignment issues since the
	type is byte aligned.
 */
template <class iType>
class BE {
	public:
		/** Value stored in big-endian format.
		 */
		uint8_t	_beValue[sizeof(iType)];
	public:
		/** */
		inline iType	value() const noexcept{
			iType	v	= _beValue[0];
			for(unsigned i=1;i<sizeof(iType);++i){
				v	<<=	8;
				v	|=	_beValue[i];
				}
			return v;
			}
		/** */
		inline void	value(iType v) noexcept{
			uint8_t*	p	= &_beValue[sizeof(iType)-1];
			*p	= (uint8_t)v;
			--p;
			for(unsigned i=1;i<sizeof(iType);++i,--p){
				v	>>=	8;
				*p	= (uint8_t)v;
				}
			}
		/** */
		inline operator iType() const noexcept{
			return value();
			}
		/** */
		inline BE	operator=(iType native) noexcept{
			value(native);
			return *this;
			}
		/** */
		inline BE	operator&=(iType native) noexcept{
			iType	x	= value();
			x	&= native;
			return *this;
			}
		/** */
		inline BE	operator|=(iType native) noexcept{
			iType	x	= value();
			x	|= native;
			value(x);
			return *this;
			}
		/** */
		inline BE	operator^=(iType native) noexcept{
			iType	x	= value();
			x	^= native;
			value(x);
			return *this;
			}
		/** */
		inline BE	operator+=(iType native) noexcept{
			iType	x	= value();
			x	+= native;
			value(x);
			return *this;
			}
		/** */
		inline BE	operator*=(iType native) noexcept{
			iType	x	= value();
			x	*= native;
			value(x);
			return *this;
			}
		/** */
		inline BE	operator/=(iType native) noexcept{
			iType	x	= value();
			x	/= native;
			value(x);
			return *this;
			}
		/** */
		inline BE	operator%=(iType native) noexcept{
			iType	x	= value();
			x	%= native;
			value(x);
			return *this;
			}
		/** */
		inline BE	operator-=(iType native) noexcept{
			iType	x	= value();
			x	-= native;
			value(x);
			return *this;
			}
		/** */
		inline BE	operator<<=(iType native) noexcept{
			iType	x	= value();
			x	<<= native;
			value(x);
			return *this;
			}
		/** */
		inline BE	operator>>=(iType native) noexcept{
			iType	x	= value();
			x	>>= native;
			value(x);
			return *this;
			}

		//=========== BitField Operations ==================
		/** Change the bits, specified as a mask of affectedBits,
			to the values specified in the bitValues mask.
		 */
		inline BE	changeBits(	iType	affectedBits,
								iType	bitValues
								) noexcept{
			iType	x	= value();
			x	&= ~affectedBits;
			x	|= (bitValues & affectedBits);
			value(x);
			return *this;
			}

		/** Clear the bits specified as a mask of affectedBits.
		 */
		inline BE	clearBits(iType affectedBits) noexcept{
			iType	x	= value();
			x	&= ~affectedBits;
			value(x);
			return *this;
			}

		/** Set the bits specified as a mask of affectedBits.
		 */
		inline BE	setBits(iType affectedBits) noexcept{
			iType	x	= value();
			x	|= affectedBits;
			value(x);
			return *this;
			}

		/** Set the bits specified as a mask of affectedBits
			if all of the affected bits are zero, otherwise
			the bits are unchanged. Returns true if the
			affected bits were changed to ones by this invocation 
			of the operation, and false if any of the bits were
			set to one prior to its invocation. This operation
			is NOT atomic.
		 */
		bool	testAndSet(iType affectedBits) noexcept{
			if(equal(affectedBits,0)){
				iType	x	= value();
				x	|= affectedBits;
				value(x);
				return true;
				}
			return false;
			}

		/** Compare the interestingBits of the field with the
			values of the corresponding bits of the bitValues mask.
		 */
		inline bool	equal(	iType	interestingBits,
							iType	bitValues
							) const noexcept{
			iType	x	= value();
			return (x & interestingBits) == (bitValues & interestingBits);
			}

		/** Explicitly read the whole value of the bit field.
			Operation is not necessarily const if this field
			represents a register which has side effects when read.
		 */
		inline iType	read() const noexcept{
			iType	x	= value();
			return x;
			}

	};

/** */
typedef BE<uint16_t>	BEU16;
/** */
typedef BE<uint32_t>	BEU32;
/** */
typedef BE<int16_t>	BES16;
/** */
typedef BE<int32_t>	BES32;

}
}

#endif

