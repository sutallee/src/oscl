/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_endian_typeh_
#define _oscl_endian_typeh_
#include "compiler/endian.h"

/** */
namespace Oscl {
/** */
namespace Endian {
/** */
namespace Big {

/** */
template <typename iType>
struct E {
	private:
		/** */
		iType	_beValue;
	public:
		/** */
		inline const iType&	raw() const noexcept{
			return _beValue;
			}
		/** */
		inline operator iType() const noexcept{
			return Oscl::Endian::fromBigEndian(_beValue);
			}
		/** */
		inline E	operator=(iType native) noexcept{
			_beValue	= Oscl::Endian::toBigEndian(native);
			return *this;
			}
		/** */
		inline E	operator&=(iType native) noexcept{
			iType	x	= Oscl::Endian::fromBigEndian(_beValue);
			x	&= native;
			_beValue	= Oscl::Endian::toBigEndian(x);
			return *this;
			}
		/** */
		inline E	operator|=(iType native) noexcept{
			iType	x	= Oscl::Endian::fromBigEndian(_beValue);
			x	|= native;
			_beValue	= Oscl::Endian::toBigEndian(x);
			return *this;
			}
		/** */
		inline E	operator^=(iType native) noexcept{
			iType	x	= Oscl::Endian::fromBigEndian(_beValue);
			x	^= native;
			_beValue	= Oscl::Endian::toBigEndian(x);
			return *this;
			}
		/** */
		inline E	operator+=(iType native) noexcept{
			iType	x	= Oscl::Endian::fromBigEndian(_beValue);
			x	+= native;
			_beValue	= Oscl::Endian::toBigEndian(x);
			return *this;
			}
		/** */
		inline E	operator*=(iType native) noexcept{
			iType	x	= Oscl::Endian::fromBigEndian(_beValue);
			x	*= native;
			_beValue	= Oscl::Endian::toBigEndian(x);
			return *this;
			}
		/** */
		inline E	operator/=(iType native) noexcept{
			iType	x	= Oscl::Endian::fromBigEndian(_beValue);
			x	/= native;
			_beValue	= Oscl::Endian::toBigEndian(x);
			return *this;
			}
		/** */
		inline E	operator%=(iType native) noexcept{
			iType	x	= Oscl::Endian::fromBigEndian(_beValue);
			x	%= native;
			_beValue	= Oscl::Endian::toBigEndian(x);
			return *this;
			}
		/** */
		inline E	operator-=(iType native) noexcept{
			iType	x	= Oscl::Endian::fromBigEndian(_beValue);
			x	-= native;
			_beValue	= Oscl::Endian::toBigEndian(x);
			return *this;
			}
		/** */
		inline E	operator<<=(iType native) noexcept{
			iType	x	= Oscl::Endian::fromBigEndian(_beValue);
			x	<<= native;
			_beValue	= Oscl::Endian::toBigEndian(x);
			return *this;
			}
		/** */
		inline E	operator>>=(iType native) noexcept{
			iType	x	= Oscl::Endian::fromBigEndian(_beValue);
			x	>>= native;
			_beValue	= Oscl::Endian::toBigEndian(x);
			return *this;
			}

		//=========== BitField Operations ==================
		/** Change the bits, specified as a mask of affectedBits,
			to the values specified in the bitValues mask.
		 */
		inline E	changeBits(	iType	affectedBits,
								iType	bitValues
								) noexcept{
			iType	x	= Oscl::Endian::fromBigEndian(_beValue);
			x	&= ~affectedBits;
			x	|= (bitValues & affectedBits);
			_beValue	= Oscl::Endian::toBigEndian(x);
			return *this;
			}

		/** Clear the bits specified as a mask of affectedBits.
		 */
		inline E	clearBits(iType affectedBits) noexcept{
			iType	x	= Oscl::Endian::fromBigEndian(_beValue);
			x	&= ~affectedBits;
			_beValue	= Oscl::Endian::toBigEndian(x);
			return *this;
			}

		/** Set the bits specified as a mask of affectedBits.
		 */
		inline E	setBits(iType affectedBits) noexcept{
			iType	x	= Oscl::Endian::fromBigEndian(_beValue);
			x	|= affectedBits;
			_beValue	= Oscl::Endian::toBigEndian(x);
			return *this;
			}

		/** Set the bits specified as a mask of affectedBits
			if all of the affected bits are zero, otherwise
			the bits are unchanged. Returns true if the
			affected bits were changed to ones by this invocation 
			of the operation, and false if any of the bits were
			set to one prior to its invocation. This operation
			is NOT atomic.
		 */
		bool	testAndSet(iType affectedBits) noexcept{
			if(equal(affectedBits,0)){
				iType	x	= Oscl::Endian::fromBigEndian(_beValue);
				x	|= affectedBits;
				_beValue	= Oscl::Endian::toBigEndian(x);
				return true;
				}
			return false;
			}

		/** Compare the interestingBits of the field with the
			values of the corresponding bits of the bitValues mask.
		 */
		inline bool	equal(	iType	interestingBits,
							iType	bitValues
							) const noexcept{
			iType	x	= Oscl::Endian::fromBigEndian(_beValue);
			return (x & interestingBits) == (bitValues & interestingBits);
			}

		/** Explicitly read the whole value of the bit field.
			Operation is not necessarily const if this field
			represents a register which has side effects when read.
		 */
		inline iType	read() const noexcept{
			iType	x	= Oscl::Endian::fromBigEndian(_beValue);
			return x;
			}

	};

/** */
typedef E<uint16_t>	U16;
/** */
typedef E<uint32_t>	U32;
/** */
typedef E<int16_t>		S16;
/** */
typedef E<int32_t>		S32;

}

/** */
namespace Little {

/** */
template <class iType>
class E {
	private:
		/** */
		iType	_leValue;
	public:
		/** */
		inline const iType&	raw() const noexcept{
			return _leValue;
			}
		/** */
		inline operator iType() const noexcept{
			return Oscl::Endian::fromLittleEndian(_leValue);
			}
		/** */
		inline E	operator=(iType native) noexcept{
			_leValue	= Oscl::Endian::toLittleEndian(native);
			return *this;
			}
		/** */
		inline E	operator&=(iType native) noexcept{
			iType	x	= Oscl::Endian::fromLittleEndian(_leValue);
			x	&= native;
			_leValue	= Oscl::Endian::toLittleEndian(x);
			return *this;
			}
		/** */
		inline E	operator|=(iType native) noexcept{
			iType	x	= Oscl::Endian::fromLittleEndian(_leValue);
			x	|= native;
			_leValue	= Oscl::Endian::toLittleEndian(x);
			return *this;
			}
		/** */
		inline E	operator^=(iType native) noexcept{
			iType	x	= Oscl::Endian::fromLittleEndian(_leValue);
			x	^= native;
			_leValue	= Oscl::Endian::toLittleEndian(x);
			return *this;
			}
		/** */
		inline E	operator+=(iType native) noexcept{
			iType	x	= Oscl::Endian::fromLittleEndian(_leValue);
			x	+= native;
			_leValue	= Oscl::Endian::toLittleEndian(x);
			return *this;
			}
		/** */
		inline E	operator*=(iType native) noexcept{
			iType	x	= Oscl::Endian::fromLittleEndian(_leValue);
			x	*= native;
			_leValue	= Oscl::Endian::toLittleEndian(x);
			return *this;
			}
		/** */
		inline E	operator/=(iType native) noexcept{
			iType	x	= Oscl::Endian::fromLittleEndian(_leValue);
			x	/= native;
			_leValue	= Oscl::Endian::toLittleEndian(x);
			return *this;
			}
		/** */
		inline E	operator%=(iType native) noexcept{
			iType	x	= Oscl::Endian::fromLittleEndian(_leValue);
			x	%= native;
			_leValue	= Oscl::Endian::toLittleEndian(x);
			return *this;
			}
		/** */
		inline E	operator-=(iType native) noexcept{
			iType	x	= Oscl::Endian::fromLittleEndian(_leValue);
			x	-= native;
			_leValue	= Oscl::Endian::toLittleEndian(x);
			return *this;
			}
		/** */
		inline E	operator<<=(iType native) noexcept{
			iType	x	= Oscl::Endian::fromLittleEndian(_leValue);
			x	<<= native;
			_leValue	= Oscl::Endian::toLittleEndian(x);
			return *this;
			}
		/** */
		inline E	operator>>=(iType native) noexcept{
			iType	x	= Oscl::Endian::fromLittleEndian(_leValue);
			x	>>= native;
			_leValue	= Oscl::Endian::toLittleEndian(x);
			return *this;
			}

		//=========== BitField Operations ==================
		/** Change the bits, specified as a mask of affectedBits,
			to the values specified in the bitValues mask.
		 */
		inline E	changeBits(	iType	affectedBits,
								iType	bitValues
								) noexcept{
			iType	x	= Oscl::Endian::fromLittleEndian(_leValue);
			x	&= ~affectedBits;
			x	|= (bitValues & affectedBits);
			_leValue	= Oscl::Endian::toLittleEndian(x);
			return *this;
			}

		/** Clear the bits specified as a mask of affectedBits.
		 */
		inline E	clearBits(iType affectedBits) noexcept{
			iType	x	= Oscl::Endian::fromLittleEndian(_leValue);
			x	&= ~affectedBits;
			_leValue	= Oscl::Endian::toLittleEndian(x);
			return *this;
			}

		/** Set the bits specified as a mask of affectedBits.
		 */
		inline E	setBits(iType affectedBits) noexcept{
			iType	x	= Oscl::Endian::fromLittleEndian(_leValue);
			x	|= affectedBits;
			_leValue	= Oscl::Endian::toLittleEndian(x);
			return *this;
			}

		/** Set the bits specified as a mask of affectedBits
			if all of the affected bits are zero, otherwise
			the bits are unchanged. Returns true if the
			affected bits were changed to ones by this invocation 
			of the operation, and false if any of the bits were
			set to one prior to its invocation. This operation
			is NOT atomic.
		 */
		bool	testAndSet(iType affectedBits) noexcept{
			if(equal(affectedBits,0)){
				iType	x	= Oscl::Endian::fromLittleEndian(_leValue);
				x	|= affectedBits;
				_leValue	= Oscl::Endian::toLittleEndian(x);
				return true;
				}
			return false;
			}

		/** Compare the interestingBits of the field with the
			values of the corresponding bits of the bitValues mask.
		 */
		inline bool	equal(	iType	interestingBits,
							iType	bitValues
							) const noexcept{
			iType	x	= Oscl::Endian::fromLittleEndian(_leValue);
			return (x & interestingBits) == (bitValues & interestingBits);
			}

		/** Explicitly read the whole value of the bit field.
			Operation is not necessarily const if this field
			represents a register which has side effects when read.
		 */
		inline iType	read() const noexcept{
			iType	x	= Oscl::Endian::fromLittleEndian(_leValue);
			return x;
			}

	};

/** */
typedef E<uint16_t>	U16;
/** */
typedef E<uint32_t>	U32;
/** */
typedef E<int16_t>		S16;
/** */
typedef E<int32_t>		S32;

}
}
}

#endif

