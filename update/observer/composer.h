/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_update_observer_composerh_
#define _oscl_update_observer_composerh_

#include "oscl/update/observer/item.h"

/** */
namespace Oscl {
/** */
namespace Update {
/** */
namespace Observer {

/** */
template <class Context, class Arg>
class Composer : public Oscl::Update::Observer::Item<Arg> {
	public:
		/** */
		typedef void	(Context::* UpdateOp)(Arg& arg);

	private:
		/** */
		Context&		_context;
		/** */
		UpdateOp		_updateOperation;

	public:
		/** */
		Composer(	Context&	context,
					UpdateOp	updateOperation
					) noexcept;
	private:
		/** This operation is called to notify the implementation
			that the task has been completed.
		 */
		void	update(Arg& arg) noexcept;
	};

template <class Context,class Arg>
Composer<Context,Arg>::Composer(	Context&	context,
								UpdateOp	updateOperation
								) noexcept:
		_context(context),
		_updateOperation(updateOperation)
		{
	}

template <class Context,class Arg>
void Composer<Context,Arg>::update(Arg& arg) noexcept{
	(_context.*_updateOperation)(arg);
	}

}
}
}

#endif
