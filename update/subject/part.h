/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_update_subject_parth_
#define _oscl_update_subject_parth_

#include "api.h"
#include "oscl/update/observer/item.h"
#include "oscl/queue/queue.h"

/** */
namespace Oscl {
/** */
namespace Update {
/** */
namespace Subject {

/** */
template <class Arg>
class Part :
	public Oscl::Update::Api<Arg>,
	public Oscl::Update::Subject::Api<Arg>
	{
	public:
		/** */
		Oscl::Queue<
			Oscl::Update::Observer::Item<Arg>
			>	_observers;

	public:
		/** */
		Part() noexcept;

	public: // Oscl::Update::Api<Arg>
		/** This operation is called to notify the implementation
			that the task has been completed.
		 */
		void	update(Arg& arg) noexcept;

	public: // Oscl::Update::Subject::Api<Arg>
		/** */
		void	attach(Oscl::Update::Observer::Item<Arg>& item) noexcept;

		/** */
		void	detach(Oscl::Update::Observer::Item<Arg>& item) noexcept;
	};

template <class Arg>
Part<Arg>::Part() noexcept
		{
	}

template <class Arg>
void Part<Arg>::update(Arg& state) noexcept{
	for(
		Oscl::Update::Observer::Item<Arg>*
		item	= _observers.first();
		item;
		item	= _observers.next(item)
		){
		item->update(state);
		}
	}

template <class Arg>
void Part<Arg>::attach(Oscl::Update::Observer::Item<Arg>& item) noexcept{
	_observers.put(&item);
	}

template <class Arg>
void Part<Arg>::detach(Oscl::Update::Observer::Item<Arg>& item) noexcept{
	_observers.remove(&item);
	}

}
}
}

#endif
