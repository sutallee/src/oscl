/*
   Copyright (C) 2021 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_neopixel_spi_parth_
#define _oscl_neopixel_spi_parth_

#include "oscl/rgb/frame/sync.h"
#include "oscl/rgb/frame/reqcomp.h"
#include "oscl/spi/master/itc/respmem.h"
#include "oscl/queue/queue.h"
#include "oscl/mt/itc/delay/respmem.h"
#include "oscl/spi/master/select/null.h"

/** */
namespace Oscl {
/** */
namespace Neopixel {
/** */
namespace SPI {

/** This part implements a NEOPIXEL driver
	as an ARGB32 interface that uses a
	SPI "bus" as the mechanism to drive the
	NEOPIXEL RGB LED.
	This implementation executes asynchronously
	and may, therefore, may execute in the
	same thread as the SPI driver.
 */
class Part :
	private Oscl::SPI::Master::Resp::Api,
	private Oscl::Mt::Itc::Delay::Resp::Api
	{
	private:
		/** */
		Oscl::Mt::Itc::PostMsgApi&				_papi;

		/** */
		Oscl::SPI::Master::Req::Api::SAP&		_spiSAP;

		/** */
		Oscl::Mt::Itc::Delay::Req::Api::SAP&	_delaySAP;

	private:
		/** */
		Oscl::RGB::Frame::Req::Composer<Part>	_argbReqComposer;

		/** */
		Oscl::RGB::Frame::Sync					_argbSync;

	private:
		/** */
		Oscl::Mt::Itc::Delay::Resp::DelayMem	_delayMem;

	private:

		/** With 8-bit encoding per-pixel, each bit
			requires 8 bits and therefore 24 bytes
			are required for each RGB pixel.
		 */
		static constexpr unsigned	nBytesPerPixel = 24;

		/** This is the number of pixels that can
			be encoded into a single buffer.
		 */
		static constexpr unsigned	nNeopixelsPerBuffer = 1;

		/** */
		static constexpr unsigned	bufferSize	= nNeopixelsPerBuffer*nBytesPerPixel;

		/** */
		struct RespBuffer {
			/** The response *must* be the
				first item in this structure,
				since a returned TransferResp
				is simply to cast to a RespMem
				and placed in the _freeMem list.
			 */
			Oscl::SPI::Master::
			Resp::TransferRespMem		_resp;

			Oscl::Memory::
    		AlignedBlock<bufferSize>	_buffer;
			};

		/** */
		union RespMem {
			/** */
			void*		__qitemlink;

			/** */
			RespBuffer	_rb;
			};

		/** */
		static constexpr unsigned	nRespMem	= 2;

		/** */
		RespMem						_mem[nRespMem];

		/** */
		Oscl::Queue<RespMem>		_freeMem;

	private:
		/** */
		Oscl::Queue<
			Oscl::RGB::Frame::
			Req::Api::UpdateReq
			>									_pending;

		/** A NEOPIXEL has no chip select and,
			therefore, there must not be any
			other devices on the SPI bus.
			As a result, there is no need for
			a chip-select so we use a chip select
			implementation that does nothing.
		 */
		Oscl::SPI::Master::Select::Null			_nullSpiSelect;

	private:
		/** */
		Oscl::RGB::Frame::Req::Api::UpdateReq*	_currentReq;

		/** */
		const uint32_t*							_pixels;

		/** */
		unsigned								_pixelsRemaining;

		/** */
		unsigned								_nTransfersOutstanding;

		/** */
		bool									_timerRunning;

	public:
		/** */
		Part(
			Oscl::Mt::Itc::PostMsgApi&				papi,
			Oscl::SPI::Master::Req::Api::SAP&		spiSAP,
			Oscl::Mt::Itc::Delay::Req::Api::SAP&	delaySAP
			) noexcept;

		/** */
		Oscl::RGB::Frame::Api&	getArgb32Api() noexcept;

		/** */
		Oscl::RGB::Frame::Req::Api::SAP&	getArgb32SAP() noexcept;

	private: // Oscl::RGB::Frame::Req::Composer _argbReqComposer
		/**	This message requests a frame update with the specified
			pixels. Pixels are ARGB32 encoded.
			This implementations ignores the Alpha field.
		 */
		void	argbRequest(Oscl::RGB::Frame::Req::Api::UpdateReq& msg) noexcept;

	private: // Oscl::SPI::Master::Resp::Api
		/** */
		void	response(Oscl::SPI::Master::Resp::Api::TransferResp& msg) noexcept;

		/** */
		void	response(Oscl::SPI::Master::Resp::Api::CancelTransferResp& msg) noexcept;

	private: // Oscl::Mt::Itc::Delay::Resp::Api
		/**	This message requests the host to attach a the specified
			symbiont.
		 */
		void	response(Oscl::Mt::Itc::Delay::Resp::Api::DelayResp& msg) noexcept;

		/**	This message requests the host to attach a the specified
			symbiont.
		 */
		void	response(Oscl::Mt::Itc::Delay::Resp::Api::CancelResp& msg) noexcept;

	private:
		/** */
		void	processNextReq() noexcept;

		/** */
		void	processNextResp() noexcept;

		/** */
		void	sendNextTransfer() noexcept;

		/** */
		void	startTimer() noexcept;
	};

}
}
}

#endif
