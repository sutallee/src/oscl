/*
   Copyright (C) 2021 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "part.h"
#include "oscl/spi/master/select/null.h"
#include "oscl/encoder/be/base.h"           

/* WARNING: Enabling DEBUG_TRACE affects the
	maximum frame rate.
 */
//#define DEBUG_TRACE

/* WARNING: Enabling DEBUG_TRACE_TIMING affects the
	NEOPIXEL timing and may cause a premature
	reset code to be recognized, especially
	when bufferSize is small.
 */
#ifdef DEBUG_TRACE
//#define DEBUG_TRACE_TIMING
#endif

#ifdef DEBUG_TRACE
#include "oscl/error/info.h"
#endif

using namespace Oscl::Neopixel::SPI;

Part::Part(
	Oscl::Mt::Itc::PostMsgApi&				papi,
	Oscl::SPI::Master::Req::Api::SAP&		spiSAP,
	Oscl::Mt::Itc::Delay::Req::Api::SAP&	delaySAP
	) noexcept:
		_papi(papi),
		_spiSAP(spiSAP),
		_delaySAP(delaySAP),
		_argbReqComposer(
			*this,
			&Part::argbRequest
			),
		_argbSync(
			papi,
			_argbReqComposer
			),
		_currentReq(0),
		_pixels(0),
		_pixelsRemaining(0),
		_nTransfersOutstanding(0),
		_timerRunning(false)
		{
	for(
		unsigned	i=0;
		i < nRespMem;
		++i
		){
		_freeMem.put(&_mem[i]);
		}
	}

Oscl::RGB::Frame::Api&	Part::getArgb32Api() noexcept{
	return _argbSync;
	}

Oscl::RGB::Frame::Req::Api::SAP&	Part::getArgb32SAP() noexcept{
	return _argbSync.getSAP();
	}

void	Part::argbRequest(Oscl::RGB::Frame::Req::Api::UpdateReq& msg) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif

	_pending.put(&msg);

	processNextReq();
	}

void	Part::response(Oscl::SPI::Master::Resp::Api::TransferResp& msg) noexcept{

	#ifdef DEBUG_TRACE_TIMING
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif

	--_nTransfersOutstanding;

	RespMem*
	mem	= (RespMem*)&msg;

	_freeMem.put(mem);

	processNextResp();
	}

void	Part::response(Oscl::SPI::Master::Resp::Api::CancelTransferResp& msg) noexcept{
	}

void	Part::startTimer() noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif

	if(_timerRunning){
		// This should never happen
		#ifdef DEBUG_TRACE
		Oscl::Error::Info::log(
			"%s: _timerRunning == true\n",
			OSCL_PRETTY_FUNCTION
			);
		#endif
		return;
		}

	_timerRunning	= true;

	Oscl::Mt::Itc::Delay::Resp::Api::DelayResp&
	resp	= _delayMem.build(
				_delaySAP,
				*this,
				_papi,
				1	// Only 50us is required for NEOPIXEL to reset.
				);

	_delaySAP.post(resp.getSrvMsg());
	}

void	Part::response(Oscl::Mt::Itc::Delay::Resp::Api::DelayResp& msg) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif

	_timerRunning	= false;

	processNextReq();
	}

void	Part::response(Oscl::Mt::Itc::Delay::Resp::Api::CancelResp& msg) noexcept{
	// Nothing to do here since we never
	// cancel the timer.
	}

static void	encodeNeoPixelColor(
				Oscl::Encoder::Api&	encoder,
				uint8_t	color
				) noexcept{

	constexpr uint8_t	T0	= 0x80;
	constexpr uint8_t	T1	= 0xE0;

	for(
		unsigned	i = 0;
		i < 8;
		++i
		){
		uint8_t
		value	= ((color << i) & 0x80)?T1:T0;
		encoder.encode(value);
		}
	}

static void	encodeNeoPixelRGB(
				Oscl::Encoder::Api&	encoder,
				uint8_t	red,
				uint8_t	green,
				uint8_t	blue
				) noexcept{

	#ifdef DEBUG_TRACE_TIMING
	Oscl::Error::Info::log(
		"%s: r: %u, g: %u, b: %u\n",
		OSCL_PRETTY_FUNCTION,
		red,
		green,
		blue
		);
	#endif

	encodeNeoPixelColor(
		encoder,
		green
		);

	encodeNeoPixelColor(
		encoder,
		red
		);

	encodeNeoPixelColor(
		encoder,
		blue
		);
	}

void	Part::processNextReq() noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif

	if(_timerRunning){
		return;
		}

	if(_currentReq){
		return;
		}

	_currentReq	= _pending.get();

	if(!_currentReq){
		return;
		}

	Oscl::RGB::Frame::Req::Api::UpdatePayload&
	payload	= _currentReq->getPayload();

	_pixels				= payload._pixels;
	_pixelsRemaining	= payload._nPixels;

	sendNextTransfer();
	}

void	Part::processNextResp() noexcept{

	#ifdef DEBUG_TRACE_TIMING
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif

	if(!_currentReq){
		startTimer();
		return;
		}

	if(_pixelsRemaining){
		sendNextTransfer();
		return;
		}

	if(_nTransfersOutstanding){
		return;
		}

	_currentReq->returnToSender();
	_currentReq	= 0;

	startTimer();
	}

void	Part::sendNextTransfer() noexcept{

	#ifdef DEBUG_TRACE_TIMING
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif

	RespMem*	mem;

	while((mem = _freeMem.get())){

		Oscl::Encoder::BE::Base
		beEncoder(
			&mem->_rb._buffer,
			sizeof(mem->_rb._buffer)
			);

		constexpr unsigned	pixelsPerBuffer = bufferSize/nBytesPerPixel;

		for(
			unsigned	i = 0;
			(i < pixelsPerBuffer) && _pixelsRemaining;
			++i,--_pixelsRemaining,++_pixels
			){
			uint32_t	pixel	= *_pixels;
			encodeNeoPixelRGB(
				beEncoder,
				(pixel >> 16)	& 0x0FF,	// red
				(pixel >> 8)	& 0x0FF,	// green
				(pixel >> 0)	& 0x0FF		// blue
				);
			}

		/*	We don't care about the data received,
			so we dump it all into the same buffer.
		 */
		static uint8_t	rxDataBuffer[bufferSize];

		Oscl::SPI::Master::Resp::Api::TransferResp&
		resp	= mem->_rb._resp.build(
					_spiSAP,	// Oscl::SPI::Master::Req::Api::SAP&   sap,
	            	*this,		// Oscl::SPI::Master::Resp::Api&       respApi,
	            	_papi,		// Oscl::Mt::Itc::PostMsgApi&          clientPapi,
					_nullSpiSelect,
	            	&mem->_rb._buffer,	// const void*                         txDataBuffer,
	            	rxDataBuffer,	// void*                               rxDataBuffer,
	            	beEncoder.length(),	// unsigned                            nDataBytesToTransfer,
					4000000,	// frequency
					0,	// lsbFirst
					0,	// cpol
					0	// cpha
					);


		_spiSAP.post(resp.getSrvMsg());

		++_nTransfersOutstanding;

		if(!_pixelsRemaining){
			return;
			}
		}
	}

