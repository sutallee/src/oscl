/*
   Copyright (C) 2005 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_delay_nanoh_
#define _oscl_delay_nanoh_

namespace Oscl {
namespace Delay {
namespace Nano {

/*	This is an interface for a nano-second delay service.
	This interface does NOT ensure nano-second accuracy.
	The only guarantee is that the wait operations will not
	return for AT-LEAST the specified amount number of
	nano-seconds.

	Implementations *may* block allowing for other threads
	to run OR may busy-wait. The effect is the same from
	the perspective of the client.

	Implementations of busy waits may be well advised to
	"kick the dog" in systems with watch-dogs.
 */
class Api {
	public:
		virtual ~Api() {}

		/** Returns after AT-LEAST the number of specified nano-seconds */
		virtual void	wait(unsigned long nanoseconds) noexcept=0;
	};

}
}
}

#endif
