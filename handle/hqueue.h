/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_handle_hqueueh_
#define _oscl_handle_hqueueh_
#include "handle.h"

/** */
namespace Oscl {

/** This template class implements a singly _linked list
	which maintains a FIFO ordering.
 */
template <class hQType>
	class HQueue{
		private:

			/** Points to the first item in the list.
			 */
			Handle<hQType>	_head;

			/** Points to the last item in the list.
			 */
			Handle<hQType>	_tail;

		public:

			/** Public constructor initializes _head and _tail pointers.
			 */
			HQueue() noexcept;

			/** Remove next element from queue and return a pointer to
				it as a result. hQType must be a subclass of HQueueItem<hQType>.
			 */
			Handle<hQType>	get(void) noexcept;

			/** Thread into the _linked list at the end of the FIFO queue.
				hQType is a subclass of HQueueItem<hQType>.
			 */
			void	put(Handle<hQType> item) noexcept;

			/** Thread into the _linked list at the beginning of the FIFO queue.
				hQType is a subclass of HQueueItem<hQType>.
			 */
			void	push(Handle<hQType> item) noexcept;

			/** Return a pointer to the first hQType item in the queue.
				The returned item remains in the queue.
				hQType is a subclass of HQueueItem<hQType>.
			 */
			Handle<hQType>	first(void) const noexcept;

			/** Return a pointer to the last hQType item in the queue.
				The returned item remains in the queue.
				hQType is a subclass of HQueueItem<hQType>.
			 */
			Handle<hQType>	last(void) noexcept;

			/** Return a pointer to the hQType item after the hQType item "prev".
				Both items remain in the queue.
				hQType is a subclass of HQueueItem<hQType>.
			 */
			Handle<hQType>	next(Handle<hQType> prev) const noexcept;

			/** Remove specified hQType element from the queue.
				hQType must be a subclass of HQueueItem<hQType>.
			 */
			void	remove(Handle<hQType> item) noexcept;

#if 0
			/** Insert the "item" hQType into the queue behind the
				"after" hQType element.
				hQType must be a subclass of HQueueItem<hQType>.
			 */
			void	insertAfter(hQType* after,hQType* item) noexcept;

			/** Insert the "item" hQType into the queue a_head of the
				"before" hQType element.
				hQType must be a subclass of HQueueItem<hQType>.
			 */
			void	insertBefore(hQType* before,hQType* item) noexcept;

#endif

		};

/** This template class is used by the HQueue class
	to implement a singly _linked list.
 */

template <class hQType>
	class HQueueItem {
		public:
			/// The _link field.
			class Handle<hQType>	_link;

		public:
			HQueueItem():_link((hQType*)0){
				}
		};

template <class hQType>
	HQueue<hQType>::HQueue(void) noexcept:_head((hQType*)0),_tail((hQType*)0){
		_head=_tail=0;
		}

template <class hQType>
	inline Handle<hQType> HQueue<hQType>::get(void) noexcept{
		Handle<hQType>			next=0;
		if( (next = _head) ){
			_head	= next->_link;
			if(!_head) _tail = 0;
			}
		return(next);
		}

template <class hQType>
	inline void	HQueue<hQType>::put(Handle<hQType> item) noexcept{
		if(_head){
			_tail->_link	= item;
			}
		else{
			_head	= item;
			}
		_tail		= item;
		item->_link	= 0;
		}

template <class hQType>
	inline void	HQueue<hQType>::push(Handle<hQType> item) noexcept{
		if(_head){
			item->_link	= _head;
			_head		= item;
			}
		else{
			_head	= _tail	= item;
			item->_link	= 0;
			}
		}

template <class hQType>
	inline Handle<hQType> HQueue<hQType>::first(void) const noexcept{
		return(_head);
		}

template <class hQType>
	inline Handle<hQType> HQueue<hQType>::last(void) noexcept{
		return(_tail);
		}

template <class hQType>
	inline Handle<hQType> HQueue<hQType>::next(Handle<hQType> prev) const noexcept{
		return(prev->_link);
		}

template <class hQType>
	inline void	HQueue<hQType>::remove(Handle<hQType> item) noexcept{
		Handle<hQType>	prv=0;
		for(Handle<hQType> nxt=first();nxt;prv=nxt,nxt=next(nxt)){
			if(nxt.operator->() == item.operator->()){
				if(prv){
					if(!(prv->_link=nxt->_link)){
						_tail	= prv;
						}
					}
				else{
					if(!(_head=nxt->_link)){
						_tail	= 0;
						}
					}
				break;
				}
			}
		}

#if 0
template <class hQType>
	inline void	HQueue<hQType>::insertAfter(hQType* prev,hQType* item) noexcept{
		item->_link	= prev->_link;
		prev->_link	= item;
		}

template <class hQType>
	inline void	HQueue<hQType>::insertBefore(hQType* before,hQType* item) noexcept{
		hQType*			nxt;
		hQType*			prv;
		for(nxt=first(),prv=0;nxt;prv=nxt,nxt=next(nxt)){
			if(nxt == before){
				item->_link	= nxt;
				if(prv){
					prv->_link	=item;
					}
				else{
					_head		= item;
					}
				break;
				}
			}
		}

#endif

};

#endif
