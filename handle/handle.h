/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_handle_handleh_
#define _oscl_handle_handleh_
#include "refcount.h"

/** */
namespace Oscl {

/** This template class is a smart-pointer implementation that uses
	reference counting to free a resource <class handType> when all references
	to that resource have been exhausted.
	This handle class assumes that the referenced class handType is a subclass
	of the RefCount class.
 */

template <class handType> class Handle {
	private:
		/** This is a pointer to the referenced object.
		 */
		handType*			_representation;

	public:
		/** This constructor is invoked without an argument.
		 */
		Handle() noexcept:_representation(0){}

		/** This constructor is envoked when a new Handle
			is created.
		 */
		Handle(handType* representation) noexcept;

		/** This copy constructor is envoked when a new Handle
			is created as a copy of an existing handle,.
		 */
		Handle(const Handle& original) noexcept;

		/** The destructor de-references the referenced object.
		 */
		~Handle();

		/** The assignment operator causes the handle to refer to
			a different object.
		 */
		Handle&	operator=(const Handle& newContent) noexcept;

		/** The assignment operator causes the handle to refer to
			a different object.
		 */
		Handle&	operator=(handType* newContent) noexcept;

		/** Conversion to bool for testing whether the handle
			reference is null.
		 */
		inline operator bool() const { return (bool)_representation; }

		/** The pointer de-reference operation allows users to
			treat Handle objects as pointers to the referenced
			object.
		 */
		inline handType*	operator->() const noexcept {
			return _representation;
			}

		/** */
		inline operator handType*() const noexcept{return _representation;}

		/** */
		inline operator handType&() const noexcept{return *_representation;}

	private:
		/** Private prevents pointers to the handle.
		 */
		void	operator&();	// Prevent pointers to the handle
	};

template <class handType>
Handle<handType>::Handle(handType* representation) noexcept:
		_representation(representation)
		{
	if(_representation)
		_representation->reference();
	}

template <class handType>
Handle<handType>::Handle(const Handle<handType>& original) noexcept:
		_representation(original._representation)
		{
	if(_representation)
		_representation->reference();
	}

template <class handType>
Handle<handType>::~Handle() {
	if(_representation)
		_representation->deReference();
	}

template <class handType>
Handle<handType>&
	Handle<handType>::operator=(const Handle<handType>& newContent) noexcept {
	if(_representation == newContent._representation) return *this;
	if(_representation)
		_representation->deReference();
	_representation	= newContent._representation;
	if(_representation)
		_representation->reference();
	return *this;
	}

template <class handType>
Handle<handType>&
	Handle<handType>::operator=(handType* newContent) noexcept {
	if(_representation == newContent) return *this;
	if(_representation)
		_representation->deReference();
	_representation	= newContent;
	if(_representation)
		_representation->reference();
	return *this;
	}
};

#endif
