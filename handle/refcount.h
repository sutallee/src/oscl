/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_handle_refcounth_
#define _oscl_handle_refcounth_
#include "oscl/atomic/types.h"

/** */
namespace Oscl {

/** This class implements a reference count for Handle classes. The intent
	is for the referenced class to inherit from this class. Upon being
	referenced, the reference() function should be called by the Handle
	class. When the handle is destructed, the handle should call the
	deReference member function, which decrements the reference count.
	If the reference count is decremented to zero during deReference()
	the reference count (and its subclass) are deleted. Low level
	operating system functions are used to ensure mutual exclusion
	during the reference() and deReference() operations.
 */

class RefCount {
	private:
		/** The actual reference count is maintained by this variable.
		 */
		mutable Oscl::Atomic::Integer	_count;

	public:
		/** The constructor initializes the reference count to zero.
		 */
		RefCount() noexcept;

		/** A virtual destructor ensures that the proper delete operator
			is called for the subclass.
		 */
		virtual ~RefCount(){}

		/** This operation must be called when a new reference to this object
			is created. Mutual exclusion is ensured during the execution of this
			operation.
		 */
		void	reference() const noexcept ;

		/** This operation must be called when a reference to this object
			is destroyed. This object will be deleted when the last reference
			is destroyed. Mutual exclusion is ensured during the execution of this
			operation.
		 */
		unsigned	deReference() const noexcept ;

	private:

		/** This private operation ensures that the count is decremented
			with appropriate mutual exclusion ensured. The value of the
			reference count after it has been decremented is returned.
		 */
		unsigned decrementCount() const noexcept ;
	};

};

#endif
