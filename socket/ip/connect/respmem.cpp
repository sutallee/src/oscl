/*
   Copyright (C) 2025 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <new>
#include "respmem.h"

using namespace Oscl::Socket::Ip::Connect;

Resp::Api::ConnectResp&
	Resp::ConnectMem::build(	Req::Api::SAP&			sap,
							Resp::Api&				respApi,
							Oscl::Mt::Itc::PostMsgApi&	clientPapi,
							const char*	hostname,
							uint16_t	port,
							bool	useSSL
							) noexcept{
	Req::Api::ConnectPayload*
		p	=	new(&payload)
				Req::Api::ConnectPayload(
							hostname,
							port,
							useSSL
							);
	Resp::Api::ConnectResp*
		r	=	new(&resp)
			Resp::Api::ConnectResp(	sap.getReqApi(),
									respApi,
									clientPapi,
									*p
									);

	return *r;
	}

Resp::Api::CloseResp&
	Resp::CloseMem::build(	Req::Api::SAP&			sap,
							Resp::Api&				respApi,
							Oscl::Mt::Itc::PostMsgApi&	clientPapi
							) noexcept{
	Req::Api::ClosePayload*
		p	=	new(&payload)
				Req::Api::ClosePayload(
);
	Resp::Api::CloseResp*
		r	=	new(&resp)
			Resp::Api::CloseResp(	sap.getReqApi(),
									respApi,
									clientPapi,
									*p
									);

	return *r;
	}

Resp::Api::OpenResp&
	Resp::OpenMem::build(	Req::Api::SAP&			sap,
							Resp::Api&				respApi,
							Oscl::Mt::Itc::PostMsgApi&	clientPapi
							) noexcept{
	Req::Api::OpenPayload*
		p	=	new(&payload)
				Req::Api::OpenPayload(
);
	Resp::Api::OpenResp*
		r	=	new(&resp)
			Resp::Api::OpenResp(	sap.getReqApi(),
									respApi,
									clientPapi,
									*p
									);

	return *r;
	}

