/*
   Copyright (C) 2025 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_socket_ip_connect_composerh_
#define _oscl_socket_ip_connect_composerh_

#include "api.h"

/** */
namespace Oscl {

/** */
namespace Socket {

/** */
namespace Ip {

/** */
namespace Connect {

/**	This template class implements a kind of GOF decorator
	pattern allows the context to employ composition instead
	of inheritance. Frequently, a context may need to implement
	more than one interface that may result in method name
	clashes. This template solves the problem by implementing
	the interface and then invoking member function pointers
	in the context instead. One instance of this object is
	created in the context for each interface of this type used
	in the context. Each instance is constructed such that it
	refers to different member functions within the context.
	The interface of this object is passed to any object that
	requires the interface. When the object invokes any of the
	operations of the API, the corresponding member function
	of the context will subsequently be invoked.
 */
template <class Context>
class Composer : public Api {
	private:
		/** A reference to the Context.
		 */
		Context&	_context;

	private:
		/**	
		 */
		bool	(Context::*_connect)(	
							const char*	hostname,
							uint16_t	port,
							bool	useSSL
							);

		/**	
		 */
		bool	(Context::*_close)(	
							);

		/**	Allocate a socket ID and initialize operation.
		 */
		bool	(Context::*_open)(	
							);

	public:
		/** */
		Composer(
			Context&		context,
			bool	(Context::*connect)(	
								const char*	hostname,
								uint16_t	port,
								bool	useSSL
								),
			bool	(Context::*close)(	
								),
			bool	(Context::*open)(	
								)
			) noexcept;

	private:
		/**	
		 */
		bool	connect(	
							const char*	hostname,
							uint16_t	port,
							bool	useSSL
							) noexcept;

		/**	
		 */
		bool	close(	
							) noexcept;

		/**	Allocate a socket ID and initialize operation.
		 */
		bool	open(	
							) noexcept;

	};

template <class Context>
Composer<Context>::Composer(
			Context&		context,
			bool	(Context::*connect)(	
								const char*	hostname,
								uint16_t	port,
								bool	useSSL
								),
			bool	(Context::*close)(	
								),
			bool	(Context::*open)(	
								)
			) noexcept:
		_context(context),
		_connect(connect),
		_close(close),
		_open(open)
		{
	}

template <class Context>
bool	Composer<Context>::connect(	
							const char*	hostname,
							uint16_t	port,
							bool	useSSL
							) noexcept{

	return (_context.*_connect)(
				hostname,
				port,
				useSSL
				);
	}

template <class Context>
bool	Composer<Context>::close(	
							) noexcept{

	return (_context.*_close)(
				);
	}

template <class Context>
bool	Composer<Context>::open(	
							) noexcept{

	return (_context.*_open)(
				);
	}

}
}
}
}
#endif
