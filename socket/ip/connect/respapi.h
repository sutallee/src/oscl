/*
   Copyright (C) 2025 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_socket_ip_connect_itc_respapih_
#define _oscl_socket_ip_connect_itc_respapih_

#include "reqapi.h"
#include "oscl/mt/itc/mbox/clirsp.h"

/** */
namespace Oscl {

/** */
namespace Socket {

/** */
namespace Ip {

/** */
namespace Connect {

/** */
namespace Resp {

/**	This class describes an ITC client interface. The
	interface includes the definition of client response messages
	based on their corresponding server request messages and
	associated payloads, as well as an interface that is to be
	implemented by a client that uses the corresponding server
	asynchronously.
 */
/**	
 */
class Api {
	public:
		/**	This describes a client response message that corresponds
			to the ConnectReq message described in the "reqapi.h"
			header file. This ConnectResp response message actually
			contains a ConnectReq request message.
		 */
		typedef Oscl::Mt::Itc::CliResponse<
					Oscl::Socket::Ip::Connect::Req::Api,
					Api,
					Oscl::Socket::Ip::Connect::
					Req::Api::ConnectPayload
					>		ConnectResp;

	public:
		/**	This describes a client response message that corresponds
			to the CloseReq message described in the "reqapi.h"
			header file. This CloseResp response message actually
			contains a CloseReq request message.
		 */
		typedef Oscl::Mt::Itc::CliResponse<
					Oscl::Socket::Ip::Connect::Req::Api,
					Api,
					Oscl::Socket::Ip::Connect::
					Req::Api::ClosePayload
					>		CloseResp;

	public:
		/**	This describes a client response message that corresponds
			to the OpenReq message described in the "reqapi.h"
			header file. This OpenResp response message actually
			contains a OpenReq request message.
		 */
		typedef Oscl::Mt::Itc::CliResponse<
					Oscl::Socket::Ip::Connect::Req::Api,
					Api,
					Oscl::Socket::Ip::Connect::
					Req::Api::OpenPayload
					>		OpenResp;

	public:
		/** Make the compiler happy. */
		virtual ~Api(){}

		/** This operation is invoked within the client thread
			whenever a ConnectResp message is received.
		 */
		virtual void	response(	Oscl::Socket::Ip::Connect::
									Resp::Api::ConnectResp& msg
									) noexcept=0;

		/** This operation is invoked within the client thread
			whenever a CloseResp message is received.
		 */
		virtual void	response(	Oscl::Socket::Ip::Connect::
									Resp::Api::CloseResp& msg
									) noexcept=0;

		/** This operation is invoked within the client thread
			whenever a OpenResp message is received.
		 */
		virtual void	response(	Oscl::Socket::Ip::Connect::
									Resp::Api::OpenResp& msg
									) noexcept=0;

	};

}
}
}
}
}
#endif
