/*
   Copyright (C) 2024 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "base.h"

using namespace Oscl::Socket::Ip::Stream;

Base::Base(
		Oscl::Mt::Itc::PostMsgApi&	papi
		) noexcept:
	_papi( papi ),
		// /home/mike/root/src/oscl/stream/input/itc/sync.h
	_connectSync(
		papi,
		*this
		),
	_inputSync(
		papi,
		*this
		),
	_outputSync(
		papi,
		*this
		)
	{}

Oscl::Socket::Ip::Connect::Api&	Base::getConnectSyncApi() noexcept {
	return _connectSync;
	}

Oscl::Socket::Ip::Connect::Req::Api::SAP&	Base::getConnectSAP() noexcept {
	return _connectSync.getSAP();
	}

Oscl::Stream::Input::Api&	Base::getInputSyncApi() noexcept {
	return _inputSync;
	}

Oscl::Stream::Input::Req::Api::SAP&	Base::getInputSAP() noexcept {
	return _inputSync.getSAP();
	}

Oscl::Stream::Output::Api&	Base::getOutputSyncApi() noexcept {
	return _outputSync;
	}

Oscl::Stream::Output::Req::Api::SAP&	Base::getOutputSAP() noexcept {
	return _outputSync.getSAP();
	}

#if 0
		virtual void	request( Oscl::Stream::Input::Req::Api::ReadReq& msg ) noexcept override;
		virtual void	request( Oscl::Stream::Input::Req::Api::CancelReq& msg ) noexcept override;
		virtual void	request( Oscl::Stream::Output::Req::Api::WriteReq& msg ) noexcept override;
		virtual void	request( Oscl::Stream::Output::Req::Api::FlushReq& msg ) noexcept override;
#endif

