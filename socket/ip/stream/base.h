/*
   Copyright (C) 2024 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_socket_ip_baseh_
#define _oscl_socket_ip_baseh_

#include "api.h"

#include "oscl/socket/ip/connect/sync.h"
#include "oscl/stream/input/itc/sync.h"
#include "oscl/stream/output/itc/sync.h"

/** */
namespace Oscl {

/** */
namespace Socket {

/** */
namespace Ip {

/** */
namespace Stream {

/**	This is the abstract interface for a streaming IP socket.
 */
class Base:
		public Api,
		protected Oscl::Socket::Ip::Connect::Req::Api,
		protected Oscl::Stream::Input::Req::Api,
		protected Oscl::Stream::Output::Req::Api
		{
	protected:
		/** */
		Oscl::Mt::Itc::PostMsgApi&		_papi;

		/** */
		Oscl::Socket::Ip::Connect::Sync	_connectSync;

		/** */
		Oscl::Stream::Input::Sync		_inputSync;

		/** */
		Oscl::Stream::Output::Sync		_outputSync;

	public:
		/** */
		Base(
			Oscl::Mt::Itc::PostMsgApi&	papi
			) noexcept;

	public: // Oscl::Socket::Ip::Connect
		/** */
		Oscl::Socket::Ip::Connect::Api&	getConnectSyncApi() noexcept override;

		/** */
		Oscl::Socket::Ip::Connect::Req::Api::SAP&	getConnectSAP() noexcept override;

	public: // Oscl::Stream::Input
		/** */
		Oscl::Stream::Input::Api&	getInputSyncApi() noexcept override;

		/** */
		Oscl::Stream::Input::Req::Api::SAP&	getInputSAP() noexcept override;

	public: // Oscl::Stream::Output
		/** */
		Oscl::Stream::Output::Api&	getOutputSyncApi() noexcept override;

		/** */
		Oscl::Stream::Output::Req::Api::SAP&	getOutputSAP() noexcept override;

	private: // Oscl::Socket::Ip::Connect::Req::Api,
		/** This operation is invoked within the server thread
			whenever a ConnectReq is received.
		 */
		virtual void	request(	Oscl::Socket::Ip::Connect::
									Req::Api::ConnectReq& msg
									) noexcept override = 0;

		/** This operation is invoked within the server thread
			whenever a CloseReq is received.
		 */
		virtual void	request(	Oscl::Socket::Ip::Connect::
									Req::Api::CloseReq& msg
									) noexcept override = 0;

		/** This operation is invoked within the server thread
			whenever a OpenReq is received.
		 */
		virtual void	request(	Oscl::Socket::Ip::Connect::
									Req::Api::OpenReq& msg
									) noexcept override = 0;

	private: // Oscl::Stream::Input::Req::Api,
		/** */
		virtual void	request( Oscl::Stream::Input::Req::Api::ReadReq& msg ) noexcept override = 0;

		/** */
		virtual void	request( Oscl::Stream::Input::Req::Api::CancelReq& msg ) noexcept override = 0;

	private: // Oscl::Stream::Output::Req::Api
		// /home/mike/root/src/oscl/stream/output/itc/reqapi.h
		/** */
		virtual void	request( Oscl::Stream::Output::Req::Api::WriteReq& msg ) noexcept override = 0;

		/** */
		virtual void	request( Oscl::Stream::Output::Req::Api::FlushReq& msg ) noexcept override = 0;
	};

}
}
}
}
#endif
