/*
   Copyright (C) 2024 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_socket_ip_apih_
#define _oscl_socket_ip_apih_

#include "oscl/socket/ip/connect/api.h"
#include "oscl/socket/ip/connect/reqapi.h"
#include "oscl/stream/input/api.h"
#include "oscl/stream/input/itc/reqapi.h"
#include "oscl/stream/output/api.h"
#include "oscl/stream/output/itc/reqapi.h"

/** */
namespace Oscl {

/** */
namespace Socket {

/** */
namespace Ip {

/** */
namespace Stream {

/**	This is the abstract interface for a streaming IP socket.
 */
class Api {
	public:
		/** Make the compiler happy.
		 */
		virtual ~Api(){};

	public: // Oscl::Socket::Ip::Connect
		/** */
		virtual Oscl::Socket::Ip::Connect::Api&	getConnectSyncApi() noexcept = 0;

		/** */
		virtual Oscl::Socket::Ip::Connect::Req::Api::SAP&	getConnectSAP() noexcept = 0;

	public: // Oscl::Stream::Input
		// /home/mike/root/src/oscl/stream/input/api.h
		/** */
		virtual Oscl::Stream::Input::Api&	getInputSyncApi() noexcept = 0;

		/** */
		virtual Oscl::Stream::Input::Req::Api::SAP&	getInputSAP() noexcept = 0;

	public: // Oscl::Stream::Output
		// /home/mike/root/src/oscl/stream/output/api.h
		/** */
		virtual Oscl::Stream::Output::Api&	getOutputSyncApi() noexcept = 0;

		/** */
		virtual Oscl::Stream::Output::Req::Api::SAP&	getOutputSAP() noexcept = 0;
	};

}
}
}
}
#endif
