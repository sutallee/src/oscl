/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "result.h"

const char*	Oscl::SSL::Result::reason(int result) noexcept{
	const char*	reason	= "unknown";

	switch(result){
		case SSL_ERROR_NONE:
			reason	= "SSL_ERROR_NONE";
			break;
		case SSL_ERROR_ZERO_RETURN:
			reason	= "SSL_ERROR_ZERO_RETURN";
			break;
		case SSL_ERROR_WANT_READ:
			reason	= "SSL_ERROR_WANT_READ";
			break;
		case SSL_ERROR_WANT_WRITE:
			reason	= "SSL_ERROR_WANT_WRITE";
			break;
		case SSL_ERROR_WANT_CONNECT:
			reason	= "SSL_ERROR_WANT_CONNECT";
			break;
		case SSL_ERROR_WANT_ACCEPT:
			reason	= "SSL_ERROR_WANT_ACCEPT";
			break;
		case SSL_ERROR_WANT_X509_LOOKUP:
			reason	= "SSL_ERROR_WANT_X509_LOOKUP";
			break;
		case SSL_ERROR_SYSCALL:
			reason	= "SSL_ERROR_SYSCALL";
			break;
		case SSL_ERROR_SSL:
			reason	= "SSL_ERROR_SSL";
			break;
		default:
			break;
		}

	return reason;
	}

const char*	Oscl::SSL::Result::reason(
				::SSL*	ssl,
				int		result
				) noexcept{
	int
	err	= SSL_get_error(ssl,result);

	return reason(err);
	}

