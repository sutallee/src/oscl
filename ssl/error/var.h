/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_ssl_error_varh_
#define _oscl_ssl_error_varh_

/** */
namespace Oscl {

/** */
namespace SSL {

/** */
namespace Error {

/**	This Var class implements an EMORF. The Flyweight Pattern
	is used in the implementation of the state subclasses
	allowing a single set of concrete state classes to be shared
	between any number of instances of this EMORF.
 */
/**	An EMORF variable (Var) is an object-oriented enumeration
	(enum) type. An enumeration variable may have a value that
	is one of a fixed number of discrete states/values.
 */
class Var {
	public:
		/**	This interface a kind of GOF visitor
			that allows the state of the variable
			to be determined/queried.
		 */
		class Query {
			public:
				/** Make the compiler happy. */
				virtual ~Query() {}
				/**	This operation is invoked by the variable
					when the variable is in the SSL_ERROR_NONE state.
				 */
				virtual void	sSL_ERROR_NONE() noexcept=0;

				/**	This operation is invoked by the variable
					when the variable is in the SSL_ERROR_SSL state.
				 */
				virtual void	sSL_ERROR_SSL() noexcept=0;

				/**	This operation is invoked by the variable
					when the variable is in the SSL_ERROR_WANT_READ state.
				 */
				virtual void	sSL_ERROR_WANT_READ() noexcept=0;

				/**	This operation is invoked by the variable
					when the variable is in the SSL_ERROR_WANT_WRITE state.
				 */
				virtual void	sSL_ERROR_WANT_WRITE() noexcept=0;

				/**	This operation is invoked by the variable
					when the variable is in the SSL_ERROR_WANT_X509_LOOKUP state.
				 */
				virtual void	sSL_ERROR_WANT_X509_LOOKUP() noexcept=0;

				/**	This operation is invoked by the variable
					when the variable is in the SSL_ERROR_SYSCALL state.
				 */
				virtual void	sSL_ERROR_SYSCALL() noexcept=0;

				/**	This operation is invoked by the variable
					when the variable is in the SSL_ERROR_ZERO_RETURN state.
				 */
				virtual void	sSL_ERROR_ZERO_RETURN() noexcept=0;

				/**	This operation is invoked by the variable
					when the variable is in the SSL_ERROR_WANT_CONNECT state.
				 */
				virtual void	sSL_ERROR_WANT_CONNECT() noexcept=0;

				/**	This operation is invoked by the variable
					when the variable is in the SSL_ERROR_WANT_ACCEPT state.
				 */
				virtual void	sSL_ERROR_WANT_ACCEPT() noexcept=0;

			};
	public:
		/**	This interface defines the operations that are
			supported by all of the states that the variable
			can assume. Sub-classes of this variable are to
			have no state (member variables) of their own.
			This allows a GOF fly-weight pattern to be used
			such that many instances of the Var can share the
			same concrete state instances.
		 */
		class State {
			public:
				/** Make the compiler happy. */
				virtual ~State() {}

				/**	Compare this state with another state for equality.
				 */
				virtual bool	operator ==(const State& other) const noexcept=0;

				/**	Compare this state with another state for inequality.
				 */
				virtual bool	operator !=(const State& other) const noexcept=0;

				/** Allow the concrete state to be queried to
					determine the actual state.
				 */
				virtual void	accept(Query& q) const noexcept=0;

			};
	private:
		/**	This member determines the current state of the EMORF.
		 */
		const State*	_state;

	public:
		/**	The constructor requires a reference to its context.
		 */
		Var(const State& initial=getSSL_ERROR_NONE()) noexcept;

		/** Be happy compiler! */
		virtual ~Var() {}

	public:
		/**	This operation is invoked to determine the current
			state of the variable via visitation.
		 */
		void	accept(Query& q) noexcept;

		/**	Compare the state of this Var with another for equality.
		 */
		bool	operator == (const Var& other) const noexcept;

		/**	Compare the state of this Var with another for inequality.
		 */
		bool	operator != (const Var& other) const noexcept;

		/**	Assign the state of another Var to this var.
		 */
		void	operator=(const Var& newState) noexcept;

		/**	Assign the specified state to this Var.
		 */
		void	operator=(const State& newState) noexcept;

		/**	Returns a reference to the current state.
		 */
		const State&	getCurrentState() const noexcept;

	public:
		/** This operation is invoked to change the EMORF
			to the SSL_ERROR_NONE state.
		 */
		void sSL_ERROR_NONE() noexcept;

		/** This operation is invoked to change the EMORF
			to the SSL_ERROR_SSL state.
		 */
		void sSL_ERROR_SSL() noexcept;

		/** This operation is invoked to change the EMORF
			to the SSL_ERROR_WANT_READ state.
		 */
		void sSL_ERROR_WANT_READ() noexcept;

		/** This operation is invoked to change the EMORF
			to the SSL_ERROR_WANT_WRITE state.
		 */
		void sSL_ERROR_WANT_WRITE() noexcept;

		/** This operation is invoked to change the EMORF
			to the SSL_ERROR_WANT_X509_LOOKUP state.
		 */
		void sSL_ERROR_WANT_X509_LOOKUP() noexcept;

		/** This operation is invoked to change the EMORF
			to the SSL_ERROR_SYSCALL state.
		 */
		void sSL_ERROR_SYSCALL() noexcept;

		/** This operation is invoked to change the EMORF
			to the SSL_ERROR_ZERO_RETURN state.
		 */
		void sSL_ERROR_ZERO_RETURN() noexcept;

		/** This operation is invoked to change the EMORF
			to the SSL_ERROR_WANT_CONNECT state.
		 */
		void sSL_ERROR_WANT_CONNECT() noexcept;

		/** This operation is invoked to change the EMORF
			to the SSL_ERROR_WANT_ACCEPT state.
		 */
		void sSL_ERROR_WANT_ACCEPT() noexcept;

	public:
		/** This operation returns an EMORF state pointer
			matching the numeric state, or zero if there is
			no matching state.
		 */
		static const State* get(long num) noexcept;

		/** This operation returns an EMORF state reference
			to the SSL_ERROR_NONE state.
		 */
		static const State& getSSL_ERROR_NONE() noexcept;

		/** This operation returns an EMORF state reference
			to the SSL_ERROR_SSL state.
		 */
		static const State& getSSL_ERROR_SSL() noexcept;

		/** This operation returns an EMORF state reference
			to the SSL_ERROR_WANT_READ state.
		 */
		static const State& getSSL_ERROR_WANT_READ() noexcept;

		/** This operation returns an EMORF state reference
			to the SSL_ERROR_WANT_WRITE state.
		 */
		static const State& getSSL_ERROR_WANT_WRITE() noexcept;

		/** This operation returns an EMORF state reference
			to the SSL_ERROR_WANT_X509_LOOKUP state.
		 */
		static const State& getSSL_ERROR_WANT_X509_LOOKUP() noexcept;

		/** This operation returns an EMORF state reference
			to the SSL_ERROR_SYSCALL state.
		 */
		static const State& getSSL_ERROR_SYSCALL() noexcept;

		/** This operation returns an EMORF state reference
			to the SSL_ERROR_ZERO_RETURN state.
		 */
		static const State& getSSL_ERROR_ZERO_RETURN() noexcept;

		/** This operation returns an EMORF state reference
			to the SSL_ERROR_WANT_CONNECT state.
		 */
		static const State& getSSL_ERROR_WANT_CONNECT() noexcept;

		/** This operation returns an EMORF state reference
			to the SSL_ERROR_WANT_ACCEPT state.
		 */
		static const State& getSSL_ERROR_WANT_ACCEPT() noexcept;

	};
}
}
}
#endif
