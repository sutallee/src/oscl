
		The operation did not complete;
		the same TLS/SSL I/O function should be called again later.
		If, by then, the underlying BIO allows writing data,
		then some TLS/SSL protocol progress will take place,
		i.e. at least part of an TLS/SSL record will be read or written.
		Note that the retry may again lead to a SSL_ERROR_WANT_READ or
		SSL_ERROR_WANT_WRITE condition.
		There is no fixed upper limit for the number of iterations that
		may be necessary until progress becomes visible at application
		protocol level.

		For socket BIOs (e.g. when SSL_set_fd() was used),
		select() or poll() on the underlying socket can be used to find
		out when the TLS/SSL I/O function should be retried.

		Caveat: Any TLS/SSL I/O function can lead to either of
		SSL_ERROR_WANT_READ and SSL_ERROR_WANT_WRITE.
		In particular, SSL_read() or SSL_peek() may want to write data
		and SSL_write() may want to read data.  This is mainly
		because TLS/SSL handshakes may occur at any time during the
		protocol (initiated by either the client
