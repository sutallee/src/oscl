/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_ssl_error_composerh_
#define _oscl_ssl_error_composerh_

#include "var.h"

/** */
namespace Oscl {

/** */
namespace SSL {

/** */
namespace Error {

/**	This template class implements a kind of GOF decorator
	pattern allows the context to employ composition instead
	of inheritance. Frequently, a context may need to implement
	more than one interface that may result in method name
	clashes. This template solves the problem by implementing
	the interface and then invoking member function pointers
	in the context instead. One instance of this object is
	created in the context for each interface of this type used
	in the context. Each instance is constructed such that it
	refers to different member functions within the context.
	The interface of this object is passed to any object that
	requires the interface. When the object invokes any of the
	operations of the API, the corresponding member function
	of the context will subsequently be invoked.
 */
/**	An EMORF variable (Var) is an object-oriented enumeration
	(enum) type. An enumeration variable may have a value that
	is one of a fixed number of discrete states/values.
 */
template <class Context>
class Composer : public Var::Query {
	private:
		/** A reference to the Context.
		 */
		Context&	_context;

	private:
		/**	sSL_ERROR_NONE
		 */
		void	(Context::*_sSL_ERROR_NONE)();

		/**	sSL_ERROR_SSL
		 */
		void	(Context::*_sSL_ERROR_SSL)();

		/**	sSL_ERROR_WANT_READ
		 */
		void	(Context::*_sSL_ERROR_WANT_READ)();

		/**	sSL_ERROR_WANT_WRITE
		 */
		void	(Context::*_sSL_ERROR_WANT_WRITE)();

		/**	sSL_ERROR_WANT_X509_LOOKUP
		 */
		void	(Context::*_sSL_ERROR_WANT_X509_LOOKUP)();

		/**	sSL_ERROR_SYSCALL
		 */
		void	(Context::*_sSL_ERROR_SYSCALL)();

		/**	sSL_ERROR_ZERO_RETURN
		 */
		void	(Context::*_sSL_ERROR_ZERO_RETURN)();

		/**	sSL_ERROR_WANT_CONNECT
		 */
		void	(Context::*_sSL_ERROR_WANT_CONNECT)();

		/**	sSL_ERROR_WANT_ACCEPT
		 */
		void	(Context::*_sSL_ERROR_WANT_ACCEPT)();

	public:
		/** */
		Composer(
			Context&		context,
			void	(Context::*sSL_ERROR_NONE)(),
			void	(Context::*sSL_ERROR_SSL)(),
			void	(Context::*sSL_ERROR_WANT_READ)(),
			void	(Context::*sSL_ERROR_WANT_WRITE)(),
			void	(Context::*sSL_ERROR_WANT_X509_LOOKUP)(),
			void	(Context::*sSL_ERROR_SYSCALL)(),
			void	(Context::*sSL_ERROR_ZERO_RETURN)(),
			void	(Context::*sSL_ERROR_WANT_CONNECT)(),
			void	(Context::*sSL_ERROR_WANT_ACCEPT)()
			) noexcept;

	private:
		/**	The TLS/SSL I/O operation completed. This result code is
			returned if and only if ret > 0.
		 */
		void	sSL_ERROR_NONE() noexcept;

		/**	A failure in the SSL library occurred, usually a protocol
			error. The OpenSSL error queue contains more information
			on the error.
		 */
		void	sSL_ERROR_SSL() noexcept;

		/**	The operation did not complete; the same TLS/SSL I/O
			function should be called again later. If, by then, the
			underlying BIO has data available for reading then some
			TLS/SSL protocol progress will take place, i.e. at least
			part of an TLS/SSL record will be read or written. Note that
			the retry may again lead to a SSL_ERROR_WANT_READ or
			SSL_ERROR_WANT_WRITE condition. There is no fixed upper limit
			for the number of iterations that may be necessary until
			progress becomes visible at application protocol level. For
			socket BIOs (e.g. when SSL_set_fd() was used), select() or
			poll() on the underlying socket can be used to find out when
			the TLS/SSL I/O function should be retried. For socket BIOs
			(e.g. when SSL_set_fd() was used), select() or poll() on
			the underlying socket can be used to find out when the
			TLS/SSL I/O function should be retried. Caveat: Any TLS/SSL
			I/O function can lead to either of SSL_ERROR_WANT_READ and
			SSL_ERROR_WANT_WRITE. In particular, SSL_read() or SSL_peek()
			may want to write data and SSL_write() may want to read data.
			This is mainly because TLS/SSL handshakes may occur at any
			time during the protocol (initiated by either the client
			or the server); SSL_read(), SSL_peek(), and SSL_write() will
			handle any pending handshakes.
		 */
		void	sSL_ERROR_WANT_READ() noexcept;

		/**	The operation did not complete; the same TLS/SSL I/O
			function should be called again later. If, by then, the
			underlying BIO allows writing data, then some TLS/SSL
			protocol progress will take place, i.e. at least part of
			an TLS/SSL record will be read or written. Note that the
			retry may again lead to a SSL_ERROR_WANT_READ or
			SSL_ERROR_WANT_WRITE condition. There is no fixed upper limit
			for the number of iterations that may be necessary until
			progress becomes visible at application protocol level. For
			socket BIOs (e.g. when SSL_set_fd() was used), select() or
			poll() on the underlying socket can be used to find out when
			the TLS/SSL I/O function should be retried. Caveat: Any
			TLS/SSL I/O function can lead to either of
			SSL_ERROR_WANT_READ and SSL_ERROR_WANT_WRITE. In particular,
			SSL_read() or SSL_peek() may want to write data and
			SSL_write() may want to read data. This is mainly because
			TLS/SSL handshakes may occur at any time during the protocol
			(initiated by either the client or the server); SSL_read(),
			SSL_peek(), and SSL_write() will handle any pending
			handshakes.
		 */
		void	sSL_ERROR_WANT_WRITE() noexcept;

		/**	The operation did not complete because an application
			callback set by SSL_CTX_set_client_cert_cb() has asked to
			be called again. The TLS/SSL I/O function should be called
			again later. Details depend on the application.
		 */
		void	sSL_ERROR_WANT_X509_LOOKUP() noexcept;

		/**	Some non-recoverable I/O error occurred. The OpenSSL
			error queue may contain more information on the error. For
			socket I/O on Unix systems, consult errno for details.
		 */
		void	sSL_ERROR_SYSCALL() noexcept;

		/**	The TLS/SSL connection has been closed. If the protocol
			version is SSL 3.0 or higher, this result code is returned
			only if a closure alert has occurred in the protocol, i.e.
			if the connection has been closed cleanly. Note that in this
			case SSL_ERROR_ZERO_RETURN does not necessarily indicate
			that the underlying transport has been closed.
		 */
		void	sSL_ERROR_ZERO_RETURN() noexcept;

		/**	The operation did not complete; the same TLS/SSL I/O
			function should be called again later. The underlying BIO
			was not connected yet to the peer and the call would block
			in connect(). The SSL function should be called again when
			the connection is established. These messages can only appear
			with a BIO_s_connect() BIO. In order to find out, when the
			connection has been successfully established, on many
			platforms select() or poll() for writing on the socket file
			descriptor can be used.
		 */
		void	sSL_ERROR_WANT_CONNECT() noexcept;

		/**	The operation did not complete; the same TLS/SSL I/O
			function should be called again later. The underlying BIO
			was not connected yet to the peer and the call would block
			in accept(). The SSL function should be called again when
			the connection is established. These messages can only appear
			with a BIO_s_accept() BIO. In order to find out, when the
			connection has been successfully established, on many
			platforms select() or poll() for writing on the socket file
			descriptor can be used.
		 */
		void	sSL_ERROR_WANT_ACCEPT() noexcept;

	};

template <class Context>
Composer<Context>::Composer(
			Context&		context,
			void	(Context::*sSL_ERROR_NONE)(),
			void	(Context::*sSL_ERROR_SSL)(),
			void	(Context::*sSL_ERROR_WANT_READ)(),
			void	(Context::*sSL_ERROR_WANT_WRITE)(),
			void	(Context::*sSL_ERROR_WANT_X509_LOOKUP)(),
			void	(Context::*sSL_ERROR_SYSCALL)(),
			void	(Context::*sSL_ERROR_ZERO_RETURN)(),
			void	(Context::*sSL_ERROR_WANT_CONNECT)(),
			void	(Context::*sSL_ERROR_WANT_ACCEPT)()
			) noexcept:
		_context(context),
		_sSL_ERROR_NONE(sSL_ERROR_NONE),
		_sSL_ERROR_SSL(sSL_ERROR_SSL),
		_sSL_ERROR_WANT_READ(sSL_ERROR_WANT_READ),
		_sSL_ERROR_WANT_WRITE(sSL_ERROR_WANT_WRITE),
		_sSL_ERROR_WANT_X509_LOOKUP(sSL_ERROR_WANT_X509_LOOKUP),
		_sSL_ERROR_SYSCALL(sSL_ERROR_SYSCALL),
		_sSL_ERROR_ZERO_RETURN(sSL_ERROR_ZERO_RETURN),
		_sSL_ERROR_WANT_CONNECT(sSL_ERROR_WANT_CONNECT),
		_sSL_ERROR_WANT_ACCEPT(sSL_ERROR_WANT_ACCEPT)
		{
	}

template <class Context>
void	Composer<Context>::sSL_ERROR_NONE() noexcept{
	(_context.*_sSL_ERROR_NONE)();
	}

template <class Context>
void	Composer<Context>::sSL_ERROR_SSL() noexcept{
	(_context.*_sSL_ERROR_SSL)();
	}

template <class Context>
void	Composer<Context>::sSL_ERROR_WANT_READ() noexcept{
	(_context.*_sSL_ERROR_WANT_READ)();
	}

template <class Context>
void	Composer<Context>::sSL_ERROR_WANT_WRITE() noexcept{
	(_context.*_sSL_ERROR_WANT_WRITE)();
	}

template <class Context>
void	Composer<Context>::sSL_ERROR_WANT_X509_LOOKUP() noexcept{
	(_context.*_sSL_ERROR_WANT_X509_LOOKUP)();
	}

template <class Context>
void	Composer<Context>::sSL_ERROR_SYSCALL() noexcept{
	(_context.*_sSL_ERROR_SYSCALL)();
	}

template <class Context>
void	Composer<Context>::sSL_ERROR_ZERO_RETURN() noexcept{
	(_context.*_sSL_ERROR_ZERO_RETURN)();
	}

template <class Context>
void	Composer<Context>::sSL_ERROR_WANT_CONNECT() noexcept{
	(_context.*_sSL_ERROR_WANT_CONNECT)();
	}

template <class Context>
void	Composer<Context>::sSL_ERROR_WANT_ACCEPT() noexcept{
	(_context.*_sSL_ERROR_WANT_ACCEPT)();
	}

}
}
}
#endif
