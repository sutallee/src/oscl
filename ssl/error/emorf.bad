// A comment about this

/** */
namespace Oscl {
/** */
namespace SSL {
/** */
namespace Error {

/** An EMORF variable (Var) is an object-oriented
	enumeration (enum) type.
	An enumeration variable may have a value that
	is one of a fixed number of discrete states/values.
 */
emorf exampleEmorf {
	/** The TLS/SSL I/O operation completed.
		This result code is returned if and only if ret > 0.
	 */
	state SSL_ERROR_NONE {
		0
		}

	/** A failure in the SSL library occurred, usually a protocol error.
		The OpenSSL error queue contains more information on the error.
	 */
	state SSL_ERROR_SSL {
		1
		}

	/** The operation did not complete;
		the same TLS/SSL I/O function should be called again later.
		If, by then, the underlying BIO has data available for reading
		then some TLS/SSL protocol progress will take place,
		i.e. at least part of an TLS/SSL record will be read or written.
		Note that the retry may again lead to a SSL_ERROR_WANT_READ or
		SSL_ERROR_WANT_WRITE condition.
		There is no fixed upper limit for the number of iterations that
		may be necessary until progress becomes visible at application
		protocol level.

		For socket BIOs (e.g. when SSL_set_fd() was used),
		select() or poll() on the underlying socket can be used to find
		out when the TLS/SSL I/O function should be retried.

		For socket BIOs (e.g. when SSL_set_fd() was used),
		select() or poll() on the underlying socket can be used to find
		out when the TLS/SSL I/O function should be retried.
		Caveat: Any TLS/SSL I/O function can lead to either of
		SSL_ERROR_WANT_READ and SSL_ERROR_WANT_WRITE.
		In particular, SSL_read() or SSL_peek() may want to write data
		and SSL_write() may want to read data.  This is mainly
		because TLS/SSL handshakes may occur at any time during the
		protocol (initiated by either the client or the server);
		SSL_read(), SSL_peek(), and SSL_write() will handle any
		pending handshakes.
	 */
	state SSL_ERROR_WANT_READ {
		2
		}

	/** The operation did not complete;
		the same TLS/SSL I/O function should be called again later.
		If, by then, the underlying BIO allows writing data,
		then some TLS/SSL protocol progress will take place,
		i.e. at least part of an TLS/SSL record will be read or written.
		Note that the retry may again lead to a SSL_ERROR_WANT_READ or
		SSL_ERROR_WANT_WRITE condition.
		There is no fixed upper limit for the number of iterations that
		may be necessary until progress becomes visible at application
		protocol level.

		For socket BIOs (e.g. when SSL_set_fd() was used),
		select() or poll() on the underlying socket can be used to find
		out when the TLS/SSL I/O function should be retried.

		Caveat: Any TLS/SSL I/O function can lead to either of
		SSL_ERROR_WANT_READ and SSL_ERROR_WANT_WRITE.
		In particular, SSL_read() or SSL_peek() may want to write data
		and SSL_write() may want to read data.  This is mainly
		because TLS/SSL handshakes may occur at any time during the
		protocol (initiated by either the client or the server);
		SSL_read(), SSL_peek(), and SSL_write() will handle any
		pending handshakes.
	 */
	state SSL_ERROR_WANT_WRITE {
		3
		}

	/** The operation did not complete because an application callback
		set by SSL_CTX_set_client_cert_cb() has asked to be called again.
		The TLS/SSL I/O function should be called again later.
		Details depend on the application.
	 */
	state SSL_ERROR_WANT_X509_LOOKUP {
		4
		}

	/** Some non-recoverable I/O error occurred.
		The OpenSSL error queue may contain more information on the error.
		For socket I/O on Unix systems, consult errno for details.
	 */
	state SSL_ERROR_SYSCALL {
		5
		}

	/** The TLS/SSL connection has been closed.
		If the protocol version is SSL 3.0 or higher, this result code is
		returned only if a closure alert has occurred in the protocol,
		i.e. if the connection has been closed cleanly.
		Note that in this case SSL_ERROR_ZERO_RETURN does not necessarily
		indicate that the underlying transport has been closed.
	 */
	state SSL_ERROR_ZERO_RETURN {
		6
		}

	/** The operation did not complete;
		the same TLS/SSL I/O function should be called again later.
		The underlying BIO was not connected yet to the peer and the
		call would block in connect().
		The SSL function should be called again when the connection
		is established. These messages can only appear with a BIO_s_connect() BIO.
		In order to find out, when the connection has been successfully
		established, on many platforms select() or poll() for writing
		on the socket file descriptor can be used.
	 */
	state SSL_ERROR_WANT_CONNECT {
		7
		}

	/** The operation did not complete;
		the same TLS/SSL I/O function should be called again later.
		The underlying BIO was not connected yet to the peer and the
		call would block in accept().
		The SSL function should be called again when the connection
		is established. These messages can only appear with a 
		BIO_s_accept() BIO.
		In order to find out, when the connection has been successfully
		established, on many platforms select() or poll() for writing
		on the socket file descriptor can be used.
	 */
	state SSL_ERROR_WANT_ACCEPT {
		8
		}
	}
}
}
}
