/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "var.h"
/** */
namespace Oscl {

/** */
namespace SSL {

/** */
namespace Error {

class VarIsSSL_ERROR_NONE : public Var::Query {
	public:
		bool	_match;
	public:
		inline void sSL_ERROR_NONE() noexcept{
			_match	= true;
			}
		inline void sSL_ERROR_SSL() noexcept{
			_match	= false;
			}
		inline void sSL_ERROR_WANT_READ() noexcept{
			_match	= false;
			}
		inline void sSL_ERROR_WANT_WRITE() noexcept{
			_match	= false;
			}
		inline void sSL_ERROR_WANT_X509_LOOKUP() noexcept{
			_match	= false;
			}
		inline void sSL_ERROR_SYSCALL() noexcept{
			_match	= false;
			}
		inline void sSL_ERROR_ZERO_RETURN() noexcept{
			_match	= false;
			}
		inline void sSL_ERROR_WANT_CONNECT() noexcept{
			_match	= false;
			}
		inline void sSL_ERROR_WANT_ACCEPT() noexcept{
			_match	= false;
			}
	};

class VarIsSSL_ERROR_SSL : public Var::Query {
	public:
		bool	_match;
	public:
		inline void sSL_ERROR_NONE() noexcept{
			_match	= false;
			}
		inline void sSL_ERROR_SSL() noexcept{
			_match	= true;
			}
		inline void sSL_ERROR_WANT_READ() noexcept{
			_match	= false;
			}
		inline void sSL_ERROR_WANT_WRITE() noexcept{
			_match	= false;
			}
		inline void sSL_ERROR_WANT_X509_LOOKUP() noexcept{
			_match	= false;
			}
		inline void sSL_ERROR_SYSCALL() noexcept{
			_match	= false;
			}
		inline void sSL_ERROR_ZERO_RETURN() noexcept{
			_match	= false;
			}
		inline void sSL_ERROR_WANT_CONNECT() noexcept{
			_match	= false;
			}
		inline void sSL_ERROR_WANT_ACCEPT() noexcept{
			_match	= false;
			}
	};

class VarIsSSL_ERROR_WANT_READ : public Var::Query {
	public:
		bool	_match;
	public:
		inline void sSL_ERROR_NONE() noexcept{
			_match	= false;
			}
		inline void sSL_ERROR_SSL() noexcept{
			_match	= false;
			}
		inline void sSL_ERROR_WANT_READ() noexcept{
			_match	= true;
			}
		inline void sSL_ERROR_WANT_WRITE() noexcept{
			_match	= false;
			}
		inline void sSL_ERROR_WANT_X509_LOOKUP() noexcept{
			_match	= false;
			}
		inline void sSL_ERROR_SYSCALL() noexcept{
			_match	= false;
			}
		inline void sSL_ERROR_ZERO_RETURN() noexcept{
			_match	= false;
			}
		inline void sSL_ERROR_WANT_CONNECT() noexcept{
			_match	= false;
			}
		inline void sSL_ERROR_WANT_ACCEPT() noexcept{
			_match	= false;
			}
	};

class VarIsSSL_ERROR_WANT_WRITE : public Var::Query {
	public:
		bool	_match;
	public:
		inline void sSL_ERROR_NONE() noexcept{
			_match	= false;
			}
		inline void sSL_ERROR_SSL() noexcept{
			_match	= false;
			}
		inline void sSL_ERROR_WANT_READ() noexcept{
			_match	= false;
			}
		inline void sSL_ERROR_WANT_WRITE() noexcept{
			_match	= true;
			}
		inline void sSL_ERROR_WANT_X509_LOOKUP() noexcept{
			_match	= false;
			}
		inline void sSL_ERROR_SYSCALL() noexcept{
			_match	= false;
			}
		inline void sSL_ERROR_ZERO_RETURN() noexcept{
			_match	= false;
			}
		inline void sSL_ERROR_WANT_CONNECT() noexcept{
			_match	= false;
			}
		inline void sSL_ERROR_WANT_ACCEPT() noexcept{
			_match	= false;
			}
	};

class VarIsSSL_ERROR_WANT_X509_LOOKUP : public Var::Query {
	public:
		bool	_match;
	public:
		inline void sSL_ERROR_NONE() noexcept{
			_match	= false;
			}
		inline void sSL_ERROR_SSL() noexcept{
			_match	= false;
			}
		inline void sSL_ERROR_WANT_READ() noexcept{
			_match	= false;
			}
		inline void sSL_ERROR_WANT_WRITE() noexcept{
			_match	= false;
			}
		inline void sSL_ERROR_WANT_X509_LOOKUP() noexcept{
			_match	= true;
			}
		inline void sSL_ERROR_SYSCALL() noexcept{
			_match	= false;
			}
		inline void sSL_ERROR_ZERO_RETURN() noexcept{
			_match	= false;
			}
		inline void sSL_ERROR_WANT_CONNECT() noexcept{
			_match	= false;
			}
		inline void sSL_ERROR_WANT_ACCEPT() noexcept{
			_match	= false;
			}
	};

class VarIsSSL_ERROR_SYSCALL : public Var::Query {
	public:
		bool	_match;
	public:
		inline void sSL_ERROR_NONE() noexcept{
			_match	= false;
			}
		inline void sSL_ERROR_SSL() noexcept{
			_match	= false;
			}
		inline void sSL_ERROR_WANT_READ() noexcept{
			_match	= false;
			}
		inline void sSL_ERROR_WANT_WRITE() noexcept{
			_match	= false;
			}
		inline void sSL_ERROR_WANT_X509_LOOKUP() noexcept{
			_match	= false;
			}
		inline void sSL_ERROR_SYSCALL() noexcept{
			_match	= true;
			}
		inline void sSL_ERROR_ZERO_RETURN() noexcept{
			_match	= false;
			}
		inline void sSL_ERROR_WANT_CONNECT() noexcept{
			_match	= false;
			}
		inline void sSL_ERROR_WANT_ACCEPT() noexcept{
			_match	= false;
			}
	};

class VarIsSSL_ERROR_ZERO_RETURN : public Var::Query {
	public:
		bool	_match;
	public:
		inline void sSL_ERROR_NONE() noexcept{
			_match	= false;
			}
		inline void sSL_ERROR_SSL() noexcept{
			_match	= false;
			}
		inline void sSL_ERROR_WANT_READ() noexcept{
			_match	= false;
			}
		inline void sSL_ERROR_WANT_WRITE() noexcept{
			_match	= false;
			}
		inline void sSL_ERROR_WANT_X509_LOOKUP() noexcept{
			_match	= false;
			}
		inline void sSL_ERROR_SYSCALL() noexcept{
			_match	= false;
			}
		inline void sSL_ERROR_ZERO_RETURN() noexcept{
			_match	= true;
			}
		inline void sSL_ERROR_WANT_CONNECT() noexcept{
			_match	= false;
			}
		inline void sSL_ERROR_WANT_ACCEPT() noexcept{
			_match	= false;
			}
	};

class VarIsSSL_ERROR_WANT_CONNECT : public Var::Query {
	public:
		bool	_match;
	public:
		inline void sSL_ERROR_NONE() noexcept{
			_match	= false;
			}
		inline void sSL_ERROR_SSL() noexcept{
			_match	= false;
			}
		inline void sSL_ERROR_WANT_READ() noexcept{
			_match	= false;
			}
		inline void sSL_ERROR_WANT_WRITE() noexcept{
			_match	= false;
			}
		inline void sSL_ERROR_WANT_X509_LOOKUP() noexcept{
			_match	= false;
			}
		inline void sSL_ERROR_SYSCALL() noexcept{
			_match	= false;
			}
		inline void sSL_ERROR_ZERO_RETURN() noexcept{
			_match	= false;
			}
		inline void sSL_ERROR_WANT_CONNECT() noexcept{
			_match	= true;
			}
		inline void sSL_ERROR_WANT_ACCEPT() noexcept{
			_match	= false;
			}
	};

class VarIsSSL_ERROR_WANT_ACCEPT : public Var::Query {
	public:
		bool	_match;
	public:
		inline void sSL_ERROR_NONE() noexcept{
			_match	= false;
			}
		inline void sSL_ERROR_SSL() noexcept{
			_match	= false;
			}
		inline void sSL_ERROR_WANT_READ() noexcept{
			_match	= false;
			}
		inline void sSL_ERROR_WANT_WRITE() noexcept{
			_match	= false;
			}
		inline void sSL_ERROR_WANT_X509_LOOKUP() noexcept{
			_match	= false;
			}
		inline void sSL_ERROR_SYSCALL() noexcept{
			_match	= false;
			}
		inline void sSL_ERROR_ZERO_RETURN() noexcept{
			_match	= false;
			}
		inline void sSL_ERROR_WANT_CONNECT() noexcept{
			_match	= false;
			}
		inline void sSL_ERROR_WANT_ACCEPT() noexcept{
			_match	= true;
			}
	};

class GetState : public Var::Query {
	public:
		const Var::State*	_state;
	public:
		inline void sSL_ERROR_NONE() noexcept{
			_state	= &Var::getSSL_ERROR_NONE();
			}
		inline void sSL_ERROR_SSL() noexcept{
			_state	= &Var::getSSL_ERROR_SSL();
			}
		inline void sSL_ERROR_WANT_READ() noexcept{
			_state	= &Var::getSSL_ERROR_WANT_READ();
			}
		inline void sSL_ERROR_WANT_WRITE() noexcept{
			_state	= &Var::getSSL_ERROR_WANT_WRITE();
			}
		inline void sSL_ERROR_WANT_X509_LOOKUP() noexcept{
			_state	= &Var::getSSL_ERROR_WANT_X509_LOOKUP();
			}
		inline void sSL_ERROR_SYSCALL() noexcept{
			_state	= &Var::getSSL_ERROR_SYSCALL();
			}
		inline void sSL_ERROR_ZERO_RETURN() noexcept{
			_state	= &Var::getSSL_ERROR_ZERO_RETURN();
			}
		inline void sSL_ERROR_WANT_CONNECT() noexcept{
			_state	= &Var::getSSL_ERROR_WANT_CONNECT();
			}
		inline void sSL_ERROR_WANT_ACCEPT() noexcept{
			_state	= &Var::getSSL_ERROR_WANT_ACCEPT();
			}
	};
class VarSSL_ERROR_NONE : public Var::State {
	public:
		VarSSL_ERROR_NONE(){}
		bool	operator == (const Var::State& other) const noexcept;
		bool	operator != (const Var::State& other) const noexcept;
		void	accept(Var::Query& q) const noexcept;
	};

class VarSSL_ERROR_SSL : public Var::State {
	public:
		VarSSL_ERROR_SSL(){}
		bool	operator == (const Var::State& other) const noexcept;
		bool	operator != (const Var::State& other) const noexcept;
		void	accept(Var::Query& q) const noexcept;
	};

class VarSSL_ERROR_WANT_READ : public Var::State {
	public:
		VarSSL_ERROR_WANT_READ(){}
		bool	operator == (const Var::State& other) const noexcept;
		bool	operator != (const Var::State& other) const noexcept;
		void	accept(Var::Query& q) const noexcept;
	};

class VarSSL_ERROR_WANT_WRITE : public Var::State {
	public:
		VarSSL_ERROR_WANT_WRITE(){}
		bool	operator == (const Var::State& other) const noexcept;
		bool	operator != (const Var::State& other) const noexcept;
		void	accept(Var::Query& q) const noexcept;
	};

class VarSSL_ERROR_WANT_X509_LOOKUP : public Var::State {
	public:
		VarSSL_ERROR_WANT_X509_LOOKUP(){}
		bool	operator == (const Var::State& other) const noexcept;
		bool	operator != (const Var::State& other) const noexcept;
		void	accept(Var::Query& q) const noexcept;
	};

class VarSSL_ERROR_SYSCALL : public Var::State {
	public:
		VarSSL_ERROR_SYSCALL(){}
		bool	operator == (const Var::State& other) const noexcept;
		bool	operator != (const Var::State& other) const noexcept;
		void	accept(Var::Query& q) const noexcept;
	};

class VarSSL_ERROR_ZERO_RETURN : public Var::State {
	public:
		VarSSL_ERROR_ZERO_RETURN(){}
		bool	operator == (const Var::State& other) const noexcept;
		bool	operator != (const Var::State& other) const noexcept;
		void	accept(Var::Query& q) const noexcept;
	};

class VarSSL_ERROR_WANT_CONNECT : public Var::State {
	public:
		VarSSL_ERROR_WANT_CONNECT(){}
		bool	operator == (const Var::State& other) const noexcept;
		bool	operator != (const Var::State& other) const noexcept;
		void	accept(Var::Query& q) const noexcept;
	};

class VarSSL_ERROR_WANT_ACCEPT : public Var::State {
	public:
		VarSSL_ERROR_WANT_ACCEPT(){}
		bool	operator == (const Var::State& other) const noexcept;
		bool	operator != (const Var::State& other) const noexcept;
		void	accept(Var::Query& q) const noexcept;
	};

}
}
}
using namespace Oscl::SSL::Error;
static const VarSSL_ERROR_NONE	_sSL_ERROR_NONE;
static const VarSSL_ERROR_SSL	_sSL_ERROR_SSL;
static const VarSSL_ERROR_WANT_READ	_sSL_ERROR_WANT_READ;
static const VarSSL_ERROR_WANT_WRITE	_sSL_ERROR_WANT_WRITE;
static const VarSSL_ERROR_WANT_X509_LOOKUP	_sSL_ERROR_WANT_X509_LOOKUP;
static const VarSSL_ERROR_SYSCALL	_sSL_ERROR_SYSCALL;
static const VarSSL_ERROR_ZERO_RETURN	_sSL_ERROR_ZERO_RETURN;
static const VarSSL_ERROR_WANT_CONNECT	_sSL_ERROR_WANT_CONNECT;
static const VarSSL_ERROR_WANT_ACCEPT	_sSL_ERROR_WANT_ACCEPT;
bool VarSSL_ERROR_NONE::operator == (const Var::State& other) const noexcept{
	VarIsSSL_ERROR_NONE	query;
	other.accept(query);
	return query._match;
	}

bool VarSSL_ERROR_NONE::operator != (const Var::State& other) const noexcept{
	VarIsSSL_ERROR_NONE	query;
	other.accept(query);
	return !query._match;
	}

void VarSSL_ERROR_NONE::accept(Var::Query& q) const noexcept{
	q.sSL_ERROR_NONE();
	}

bool VarSSL_ERROR_SSL::operator == (const Var::State& other) const noexcept{
	VarIsSSL_ERROR_SSL	query;
	other.accept(query);
	return query._match;
	}

bool VarSSL_ERROR_SSL::operator != (const Var::State& other) const noexcept{
	VarIsSSL_ERROR_SSL	query;
	other.accept(query);
	return !query._match;
	}

void VarSSL_ERROR_SSL::accept(Var::Query& q) const noexcept{
	q.sSL_ERROR_SSL();
	}

bool VarSSL_ERROR_WANT_READ::operator == (const Var::State& other) const noexcept{
	VarIsSSL_ERROR_WANT_READ	query;
	other.accept(query);
	return query._match;
	}

bool VarSSL_ERROR_WANT_READ::operator != (const Var::State& other) const noexcept{
	VarIsSSL_ERROR_WANT_READ	query;
	other.accept(query);
	return !query._match;
	}

void VarSSL_ERROR_WANT_READ::accept(Var::Query& q) const noexcept{
	q.sSL_ERROR_WANT_READ();
	}

bool VarSSL_ERROR_WANT_WRITE::operator == (const Var::State& other) const noexcept{
	VarIsSSL_ERROR_WANT_WRITE	query;
	other.accept(query);
	return query._match;
	}

bool VarSSL_ERROR_WANT_WRITE::operator != (const Var::State& other) const noexcept{
	VarIsSSL_ERROR_WANT_WRITE	query;
	other.accept(query);
	return !query._match;
	}

void VarSSL_ERROR_WANT_WRITE::accept(Var::Query& q) const noexcept{
	q.sSL_ERROR_WANT_WRITE();
	}

bool VarSSL_ERROR_WANT_X509_LOOKUP::operator == (const Var::State& other) const noexcept{
	VarIsSSL_ERROR_WANT_X509_LOOKUP	query;
	other.accept(query);
	return query._match;
	}

bool VarSSL_ERROR_WANT_X509_LOOKUP::operator != (const Var::State& other) const noexcept{
	VarIsSSL_ERROR_WANT_X509_LOOKUP	query;
	other.accept(query);
	return !query._match;
	}

void VarSSL_ERROR_WANT_X509_LOOKUP::accept(Var::Query& q) const noexcept{
	q.sSL_ERROR_WANT_X509_LOOKUP();
	}

bool VarSSL_ERROR_SYSCALL::operator == (const Var::State& other) const noexcept{
	VarIsSSL_ERROR_SYSCALL	query;
	other.accept(query);
	return query._match;
	}

bool VarSSL_ERROR_SYSCALL::operator != (const Var::State& other) const noexcept{
	VarIsSSL_ERROR_SYSCALL	query;
	other.accept(query);
	return !query._match;
	}

void VarSSL_ERROR_SYSCALL::accept(Var::Query& q) const noexcept{
	q.sSL_ERROR_SYSCALL();
	}

bool VarSSL_ERROR_ZERO_RETURN::operator == (const Var::State& other) const noexcept{
	VarIsSSL_ERROR_ZERO_RETURN	query;
	other.accept(query);
	return query._match;
	}

bool VarSSL_ERROR_ZERO_RETURN::operator != (const Var::State& other) const noexcept{
	VarIsSSL_ERROR_ZERO_RETURN	query;
	other.accept(query);
	return !query._match;
	}

void VarSSL_ERROR_ZERO_RETURN::accept(Var::Query& q) const noexcept{
	q.sSL_ERROR_ZERO_RETURN();
	}

bool VarSSL_ERROR_WANT_CONNECT::operator == (const Var::State& other) const noexcept{
	VarIsSSL_ERROR_WANT_CONNECT	query;
	other.accept(query);
	return query._match;
	}

bool VarSSL_ERROR_WANT_CONNECT::operator != (const Var::State& other) const noexcept{
	VarIsSSL_ERROR_WANT_CONNECT	query;
	other.accept(query);
	return !query._match;
	}

void VarSSL_ERROR_WANT_CONNECT::accept(Var::Query& q) const noexcept{
	q.sSL_ERROR_WANT_CONNECT();
	}

bool VarSSL_ERROR_WANT_ACCEPT::operator == (const Var::State& other) const noexcept{
	VarIsSSL_ERROR_WANT_ACCEPT	query;
	other.accept(query);
	return query._match;
	}

bool VarSSL_ERROR_WANT_ACCEPT::operator != (const Var::State& other) const noexcept{
	VarIsSSL_ERROR_WANT_ACCEPT	query;
	other.accept(query);
	return !query._match;
	}

void VarSSL_ERROR_WANT_ACCEPT::accept(Var::Query& q) const noexcept{
	q.sSL_ERROR_WANT_ACCEPT();
	}


Var::Var(const State& initial) noexcept:
		_state(&initial)
		{
	}

void Var::accept(Query& q) noexcept{
	_state->accept(q);
	}

bool Var::operator == (const Var& other) const noexcept{
	return *other._state == *_state;
	}

bool Var::operator != (const Var& other) const noexcept{
	return *other._state != *_state;
	}

void Var::operator = (const Var& newState) noexcept{
	*this	= *newState._state;
	}

void Var::operator = (const State& newState) noexcept{
	GetState	getState;
	newState.accept(getState);
	_state	= getState._state;
	}

const Var::State& Var::getCurrentState() const noexcept{
	return *_state;
	}

void Var::sSL_ERROR_NONE() noexcept{
	_state	= &_sSL_ERROR_NONE;
	}

void Var::sSL_ERROR_SSL() noexcept{
	_state	= &_sSL_ERROR_SSL;
	}

void Var::sSL_ERROR_WANT_READ() noexcept{
	_state	= &_sSL_ERROR_WANT_READ;
	}

void Var::sSL_ERROR_WANT_WRITE() noexcept{
	_state	= &_sSL_ERROR_WANT_WRITE;
	}

void Var::sSL_ERROR_WANT_X509_LOOKUP() noexcept{
	_state	= &_sSL_ERROR_WANT_X509_LOOKUP;
	}

void Var::sSL_ERROR_SYSCALL() noexcept{
	_state	= &_sSL_ERROR_SYSCALL;
	}

void Var::sSL_ERROR_ZERO_RETURN() noexcept{
	_state	= &_sSL_ERROR_ZERO_RETURN;
	}

void Var::sSL_ERROR_WANT_CONNECT() noexcept{
	_state	= &_sSL_ERROR_WANT_CONNECT;
	}

void Var::sSL_ERROR_WANT_ACCEPT() noexcept{
	_state	= &_sSL_ERROR_WANT_ACCEPT;
	}

const Var::State*	Var::get(long num) noexcept{
	switch(num){
		case 0:
			return &_sSL_ERROR_NONE;
		case 1:
			return &_sSL_ERROR_SSL;
		case 2:
			return &_sSL_ERROR_WANT_READ;
		case 3:
			return &_sSL_ERROR_WANT_WRITE;
		case 4:
			return &_sSL_ERROR_WANT_X509_LOOKUP;
		case 5:
			return &_sSL_ERROR_SYSCALL;
		case 6:
			return &_sSL_ERROR_ZERO_RETURN;
		case 7:
			return &_sSL_ERROR_WANT_CONNECT;
		case 8:
			return &_sSL_ERROR_WANT_ACCEPT;
		default:
			return 0;
		};
	}

const Var::State&	Var::getSSL_ERROR_NONE() noexcept{
	return _sSL_ERROR_NONE;
	}

const Var::State&	Var::getSSL_ERROR_SSL() noexcept{
	return _sSL_ERROR_SSL;
	}

const Var::State&	Var::getSSL_ERROR_WANT_READ() noexcept{
	return _sSL_ERROR_WANT_READ;
	}

const Var::State&	Var::getSSL_ERROR_WANT_WRITE() noexcept{
	return _sSL_ERROR_WANT_WRITE;
	}

const Var::State&	Var::getSSL_ERROR_WANT_X509_LOOKUP() noexcept{
	return _sSL_ERROR_WANT_X509_LOOKUP;
	}

const Var::State&	Var::getSSL_ERROR_SYSCALL() noexcept{
	return _sSL_ERROR_SYSCALL;
	}

const Var::State&	Var::getSSL_ERROR_ZERO_RETURN() noexcept{
	return _sSL_ERROR_ZERO_RETURN;
	}

const Var::State&	Var::getSSL_ERROR_WANT_CONNECT() noexcept{
	return _sSL_ERROR_WANT_CONNECT;
	}

const Var::State&	Var::getSSL_ERROR_WANT_ACCEPT() noexcept{
	return _sSL_ERROR_WANT_ACCEPT;
	}

