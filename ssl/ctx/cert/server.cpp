/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <openssl/err.h>
#include "server.h"
#include "oscl/error/info.h"
//#include "scl/posix/common/debug/print/hexdump.h"
//#include "scl/ascii/hex/convert.h"
//#include "oscl/entropy/random.h"

using namespace Oscl::SSL::CTX::Cert;

Server::Server(
	const char*			certPemFile,
	const char*			keyPemFile,
	const SSL_METHOD*	method
	) noexcept:
		_ctx(0)
		{

	_ctx	= SSL_CTX_new(method);

	if(!_ctx){
		Oscl::Error::Info::log(
			"%s: SSL_CTX_new failed\n",
			__PRETTY_FUNCTION__
			);
		ERR_print_errors_fp(stdout);
		exit(1);
		}

	ERR_print_errors_fp(stdout);

	SSL_CTX_set_app_data(_ctx,this);

	SSL_CTX_set_ecdh_auto(_ctx, 1);

	int
	result	= SSL_CTX_use_certificate_file(_ctx,certPemFile,SSL_FILETYPE_PEM);
	if(result <= 0){
		ERR_print_errors_fp(stderr);
		exit(1);
		}

	result	= SSL_CTX_use_PrivateKey_file(_ctx, keyPemFile, SSL_FILETYPE_PEM);
	if(result <= 0) {
        ERR_print_errors_fp(stderr);
		exit(1);
		}
	}

SSL_CTX*	Server::getSSL_CTX() noexcept{
	return _ctx;
	}

