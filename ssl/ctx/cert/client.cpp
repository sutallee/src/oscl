/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <openssl/err.h>
#include <string.h>
#include "client.h"
#include "oscl/error/info.h"
#include "oscl/entropy/rand.h"

static bool	debug	= false;

using namespace Oscl::SSL::CTX::Cert;

static Client*	getClientFromSSL(::SSL* ssl) noexcept{

	if(!ssl){
		Oscl::Error::Info::log(
			"%s: getClientFromSSL() ssl pointer is NULL.",
			__PRETTY_FUNCTION__
			);
		return 0;
		}

	SSL_CTX*
	ctx	= SSL_get_SSL_CTX(ssl);

	Client*
	client	= (Client*)SSL_CTX_get_app_data(ctx);

	if(!client){
		Oscl::Error::Info::log(
			"%s: getClientFromSSL() pointer is NULL.",
			__PRETTY_FUNCTION__
			);
		return 0;
		}

	return client;
	}

static int generateCookieCallback(
			::SSL*			ssl,
			unsigned char*	cookie,
			unsigned*		cookie_len
			){
	Client*
	client	= getClientFromSSL(ssl);

	if(!client){
		return 0;
		}

	return client->generateCookie(
			ssl,
			cookie,
			*cookie_len
			);
	}

static int verifyCookieCallback(::SSL* ssl, const unsigned char* cookie, unsigned cookie_len){
	Client*
	client	= getClientFromSSL(ssl);

	if(!client){
		return 0;
		}

	return client->verifyCookie(
		ssl,
		cookie,
		cookie_len
		);

	}

static void	dumpCert(X509_NAME* subject,X509_NAME* issuer){
	char	xbuff[1024];

	char*
	b	= X509_NAME_oneline(subject,xbuff,sizeof(xbuff));
	xbuff[sizeof(xbuff)-1]	= '\0';

	if(b){
		Oscl::Error::Info::log(
			"%s: Subject: %s\n",
			__PRETTY_FUNCTION__,
			b
			);
		}

	b	= X509_NAME_oneline(issuer,xbuff,sizeof(xbuff));
	xbuff[sizeof(xbuff)-1]	= '\0';

	if(b){
		Oscl::Error::Info::log(
			"%s: Issuer: %s\n",
			__PRETTY_FUNCTION__,
			b
			);
		}

	}

static int verify_callback(int preverify_ok, X509_STORE_CTX *ctx) {
	if(preverify_ok==1){
		return 1;
		}

	long err=X509_STORE_CTX_get_error(ctx);

	if(err==X509_V_ERR_CERT_HAS_EXPIRED){
		// The otherwise trusted certificate has expired.
		X509*	cert	= X509_STORE_CTX_get_current_cert(ctx);
		/*	What we *really* want to do here is to only allow
			specific expired certificates... not *all*.

			Since we do not allow self-signed certs other than
			those in /usr/ssl and this error always comes after
			self-signed certs are checked, we will never get here
			unless we trust the cert.

			We want to compare the issuer and subject X509_NAMEs
			of the current cert. If they are the same, then we
			allow the expired certificate.
		 */
		X509_NAME*	subject	= X509_get_subject_name(cert);
		X509_NAME*	issuer	= X509_get_issuer_name(cert);
		if(debug){
			dumpCert(subject,issuer);
			}

		// Test for self-signed certificate.
		if(X509_NAME_cmp(subject,issuer)){
			// If we get here, then this is *not* a self-signed certificate.
			// In this case we reject the connection since it is expired.
			Oscl::Error::Info::log(
				"Issuer != Subject: \"%s\"! Connection rejected.\n",
				X509_verify_cert_error_string(err)
				);
			return 0;
			}

		// Trusted self-signed certificate
		Oscl::Error::Info::log(
			"%s: Warning %s!!\n",
			__PRETTY_FUNCTION__,
			X509_verify_cert_error_string(err)
			);
		return 1;
		}
	else if(err==X509_V_ERR_DEPTH_ZERO_SELF_SIGNED_CERT){
		// The passed certificate is self signed and the same 
		// certificate cannot be found in the list of trusted certificates. 

		X509*	cert	= X509_STORE_CTX_get_current_cert(ctx);
		/*	What we *really* want to do here is to only allow
			specific expired certificates... not *all*.

			Since we do not allow self-signed certs other than
			those in /usr/ssl and this error always comes after
			self-signed certs are checked, we will never get here
			unless we trust the cert.

			We want to compare the issuer and subject X509_NAMEs
			of the current cert. If they are the same, then we
			allow the expired certificate.
		 */
		X509_NAME*	subject	= X509_get_subject_name(cert);
		X509_NAME*	issuer	= X509_get_issuer_name(cert);
		if(debug){
			dumpCert(subject,issuer);
			}

		Oscl::Error::Info::log(
			"Certificate validation failed: untrusted \"%s\"\n",
			X509_verify_cert_error_string(err)
			);

		return 0;
		}
	else{
		Oscl::Error::Info::log(
			"Certificate chain validation failed: %s\n",
			X509_verify_cert_error_string(err)
			);
		return 0;
		}
	}

Client::Client(
	const SSL_METHOD*	method,
	const char*			trustStoreDir,
	const char*			trustStoreFile
	) noexcept:
		_ctx(0)
		{

	for(unsigned i=0;i<maxCookies;++i){
		_freeCookies.put(&_cookieMem[i]);
		}

	_ctx	= SSL_CTX_new(method);

	if(!_ctx){
		Oscl::Error::Info::log(
			"%s: SSL_CTX_new failed\n",
			__PRETTY_FUNCTION__
			);
		ERR_print_errors_fp(stdout);
		exit(1);
		}

	SSL_CTX_set_app_data(_ctx,this);

	SSL_CTX_set_verify(_ctx, SSL_VERIFY_PEER, verify_callback);

	int
	result	= SSL_CTX_load_verify_locations(_ctx,trustStoreFile, trustStoreDir);

	if( result != 1) {
		Oscl::Error::Info::log(
			"%s: SSL_CTX_load_verify_locations() failed.\n",
			__PRETTY_FUNCTION__
			);
		ERR_print_errors_fp(stdout);
		exit(1);
		return;
		}

	SSL_CTX_set_cookie_generate_cb(_ctx,generateCookieCallback);

	SSL_CTX_set_cookie_verify_cb(_ctx,verifyCookieCallback);
	}

SSL_CTX*	Client::getSSL_CTX() noexcept{
	return _ctx;
	}

Oscl::SSL::CTX::Cookie*	Client::findCookie(::SSL* ssl) noexcept{
	Cookie*	next;
	for(
		next	= _cookieList.first();
		next;
		next	= _cookieList.next(next)
		){
		if(next->ssl == ssl){
			return next;
			}
		}
	return 0;
	}

unsigned	Client::generateCookie(
				::SSL*			ssl,
				unsigned char*	cookieBuffer,
				unsigned		maxCookieSize
				) noexcept{

	Cookie*	c	= findCookie(ssl);

	if(!c){
		c	= _freeCookies.get();
		c->ssl	= ssl;
		_cookieList.put(c);
		}

	if(!c){
		// No memory for cookies!
		return 0;
		}

	static unsigned	len	= sizeof(c->cookie);

	if(len > maxCookieSize){
		len	= maxCookieSize;
		}

	Oscl::Entropy::Random::fetch(c->cookie,len);

	memcpy(cookieBuffer,c->cookie,len);

	return len;
	}

bool	Client::verifyCookie(
			::SSL*					ssl,
			const unsigned char*	cookie,
			unsigned				cookieLength
			) noexcept{

	Cookie*
	c	= findCookie(ssl);

	if(!c){
		return false;
		}

	_cookieList.remove(c);

	_freeCookies.put(c);

	size_t
	size	= sizeof(c->cookie);

	if(cookieLength > size){
		return false;
		}

	if(!memcmp(c->cookie,cookie,cookieLength)){
		return true;
		}

	return false;
	}

