/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <openssl/err.h>
#include <string.h>
#include "client.h"
#include "oscl/error/info.h"
#include "oscl/entropy/rand.h"
#include "oscl/strings/hex.h"

using namespace Oscl::SSL::CTX::PSK;

static Client*	getClientFromSSL(::SSL* ssl) noexcept{

	if(!ssl){
		Oscl::Error::Info::log(
			"%s: ssl pointer is NULL.",
			__PRETTY_FUNCTION__
			);
		return 0;
		}

	SSL_CTX*
	ctx	= SSL_get_SSL_CTX(ssl);

	Client*
	client	= (Client*)SSL_CTX_get_app_data(ctx);

	if(!client){
		Oscl::Error::Info::log(
			"%s: client pointer is NULL.",
			__PRETTY_FUNCTION__
			);
		return 0;
		}

	return client;
	}

static int generateCookieCallback(
			::SSL*			ssl,
			unsigned char*	cookie,
			unsigned*		cookie_len
			){
	Client*
	client	= getClientFromSSL(ssl);

	if(!client){
		return 0;
		}

	return client->generateCookie(
			ssl,
			cookie,
			*cookie_len
			);
	}

static int verifyCookieCallback(::SSL* ssl, const unsigned char* cookie, unsigned cookie_len){
	Client*
	client	= getClientFromSSL(ssl);

	if(!client){
		return 0;
		}

	return client->verifyCookie(
		ssl,
		cookie,
		cookie_len
		);

	}

static unsigned int pskClientCallback(
	::SSL*			ssl,
	const char*		hint,
	char*			identity,
    unsigned		max_identity_len,
	unsigned char*	psk,
	unsigned		max_psk_len
	) {
	Client*
	client	= getClientFromSSL(ssl);

	if(!client){
		return 0;
		}

	return client->copyPSK(
		hint,
		psk,
		max_psk_len,
		identity,
		max_identity_len
		);
	}

Client::Client(
	const SSL_METHOD*	method,
	const char*			psk,
	const char*			identity
	) noexcept:
		_ctx(0),
		_identity(identity)
		{

	for(unsigned i=0;i<maxCookies;++i){
		_freeCookies.put(&_cookieMem[i]);
		}

	_ctx	= SSL_CTX_new(method);

	if(!_ctx){
		perror("SSL_CTX_new failed");
		ERR_print_errors_fp(stdout);
		exit(1);
		}

	ERR_print_errors_fp(stdout);

	SSL_CTX_set_app_data(_ctx,this);

	SSL_CTX_set_cookie_generate_cb(_ctx,generateCookieCallback);
	ERR_print_errors_fp(stdout);

	SSL_CTX_set_cookie_verify_cb(_ctx,verifyCookieCallback);
	ERR_print_errors_fp(stdout);

	if (!SSL_CTX_set_cipher_list(_ctx, "PSK")) {
		Oscl::Error::Info::log(
			"%s: Unable to select ciphers\n",
			__PRETTY_FUNCTION__
			);
		}

	SSL_CTX_set_psk_client_callback(_ctx, pskClientCallback);


	_pskLength	= Oscl::Strings::hexAsciiStringToBinary(
		psk,
		_psk,
		sizeof(_psk)
		);
	}

SSL_CTX*	Client::getSSL_CTX() noexcept{
	return _ctx;
	}

Oscl::SSL::CTX::Cookie*	Client::findCookie(::SSL* ssl) noexcept{
	Cookie*	next;
	for(
		next	= _cookieList.first();
		next;
		next	= _cookieList.next(next)
		){
		if(next->ssl == ssl){
			return next;
			}
		}
	return 0;
	}

unsigned	Client::generateCookie(
				::SSL*			ssl,
				unsigned char*	cookieBuffer,
				unsigned		maxCookieSize
				) noexcept{

	Cookie*	c	= findCookie(ssl);

	if(!c){
		c	= _freeCookies.get();
		c->ssl	= ssl;
		_cookieList.put(c);
		}

	if(!c){
		// No memory for cookies!
		return 0;
		}

	static unsigned	len	= sizeof(c->cookie);

	if(len > maxCookieSize){
		len	= maxCookieSize;
		}

	Oscl::Entropy::Random::fetch(c->cookie,len);

	memcpy(cookieBuffer,c->cookie,len);

	return len;
	}

bool	Client::verifyCookie(
			::SSL*					ssl,
			const unsigned char*	cookie,
			unsigned				cookieLength
			) noexcept{

	Cookie*
	c	= findCookie(ssl);

	if(!c){
		return false;
		}

	_cookieList.remove(c);

	_freeCookies.put(c);

	size_t
	size	= sizeof(c->cookie);

	if(cookieLength > size){
		return false;
		}

	if(!memcmp(c->cookie,cookie,cookieLength)){
		return true;
		}

	return false;
	}

unsigned	Client::copyPSK(
				const char*		hint,
				unsigned char*	psk,
				unsigned		maxPskBufferLength,
				char*			identity,
			    unsigned		maxIdentityLength
				) noexcept{
	if(_pskLength > maxPskBufferLength){
		Oscl::Error::Info::log(
			"%s, PSK too long for SSL.\n",
			__PRETTY_FUNCTION__
			);
		return 0;
		}

	memcpy(psk,_psk,_pskLength);

	size_t
	identityLen	= strlen(_identity);

	if(identityLen > maxIdentityLength){
		Oscl::Error::Info::log(
			"%s: Identity string is too long for SSL.\n",
			__PRETTY_FUNCTION__
			);
		exit(1);
		}

	strcpy(identity,_identity);

	return _pskLength;
	}

