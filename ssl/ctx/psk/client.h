/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _scl_ssl_ctx_psk_clienth_
#define _scl_ssl_ctx_psk_clienth_

#include <openssl/ssl.h>
#include "oscl/queue/queue.h"
#include "oscl/ssl/ctx/cookie.h"

/** */
namespace Oscl {

/** */
namespace SSL {

/** */
namespace CTX {

/** */
namespace PSK {

/** */
class Client {
	private:
		/** */
		SSL_CTX*	_ctx;

	private:
		/** */
		enum{maxCookies=16};

		/** */
		Cookie				_cookieMem[maxCookies];

		/** */
		Oscl::Queue<Cookie>	_freeCookies;

		/** */
		Oscl::Queue<Cookie>	_cookieList;

		/** */
		unsigned char		_psk[64];

		/** */
		unsigned			_pskLength;

		/** */
		const char*			_identity;

	public:
		/** */
		Client(
			const SSL_METHOD*	method		= DTLS_client_method(),
			const char*			psk			= "123456789012345678901234567890AA",
			const char*			identity	= "Client_identity"
			) noexcept;

		/** */
		SSL_CTX*	getSSL_CTX() noexcept;

	private:
		/** */
		Cookie*	findCookie(::SSL* ssl) noexcept;

	public:
		/** Returns length of cookie or ZERO
			if unable to create a cookie.
		 */
		unsigned	generateCookie(
						::SSL*			ssl,
						unsigned char*	cookieBuffer,
						unsigned		maxCcookieSize
						) noexcept;

		/** Returns true if we find a matching cookie.
		 */
		bool	verifyCookie(
					::SSL*					ssl,
					const unsigned char*	cookie,
					unsigned				cookieLength
					) noexcept;

		/** Returns length of the PSK or ZERO on failure.
		 */
		unsigned	copyPSK(
						const char*		hint,
						unsigned char*	psk,
						unsigned		maxPskBufferLength,
						char*			identity,
					    unsigned		maxIdentityLength
						) noexcept;
	};


}
}
}
}

#endif

