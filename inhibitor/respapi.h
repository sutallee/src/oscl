/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_inhibitor_itc_respapih_
#define _oscl_inhibitor_itc_respapih_

#include "reqapi.h"
#include "oscl/mt/itc/mbox/clirsp.h"

/** */
namespace Oscl {

/** */
namespace Inhibitor {

/** */
namespace Resp {

/**	This class describes an ITC client interface. The
	interface includes the definition of client response messages
	based on their corresponding server request messages and
	associated payloads, as well as an interface that is to be
	implemented by a client that uses the corresponding server
	asynchronously.
 */
/**	An Inhibitor is a mechanism used by one or more clients
	to control a process such that it is prevented from
	proceeding is allowed to proceed by all clients. The Allow
	and Prevent operations must be balanced by a client such
	that an equal number of each operations result in the server
	being in an uninhibited (normal) state. The server typically
	maintains a counter that is incremented by a Prevent
	operation and decremented by an Allow operation. The server
	process is inhibited whenever the counter is non-zero.
 */
class Api {
	public:
		/**	This describes a client response message that corresponds
			to the PreventReq message described in the "reqapi.h"
			header file. This PreventResp response message actually
			contains a PreventReq request message.
		 */
		typedef Oscl::Mt::Itc::CliResponse<
					Oscl::Inhibitor::Req::Api,
					Api,
					Oscl::Inhibitor::
					Req::Api::PreventPayload
					>		PreventResp;

	public:
		/**	This describes a client response message that corresponds
			to the AllowReq message described in the "reqapi.h"
			header file. This AllowResp response message actually
			contains a AllowReq request message.
		 */
		typedef Oscl::Mt::Itc::CliResponse<
					Oscl::Inhibitor::Req::Api,
					Api,
					Oscl::Inhibitor::
					Req::Api::AllowPayload
					>		AllowResp;

	public:
		/** Make the compiler happy. */
		virtual ~Api(){}

		/** This operation is invoked within the client thread
			whenever a PreventResp message is received.
		 */
		virtual void	response(	Oscl::Inhibitor::
									Resp::Api::PreventResp& msg
									) noexcept=0;

		/** This operation is invoked within the client thread
			whenever a AllowResp message is received.
		 */
		virtual void	response(	Oscl::Inhibitor::
									Resp::Api::AllowResp& msg
									) noexcept=0;

	};

}
}
}
#endif
