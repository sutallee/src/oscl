/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_inhibitor_itc_reqapih_
#define _oscl_inhibitor_itc_reqapih_

#include "oscl/mt/itc/mbox/srvreq.h"
#include "oscl/mt/itc/postmsgapi.h"
#include "oscl/mt/itc/mbox/sap.h"

/** */
namespace Oscl {

/** */
namespace Inhibitor {

/** */
namespace Req {

/**	This class describes an ITC server interface. The
	interface includes the definition of messages and their
	associated payloads, as well as an interface that is to be
	implemented by a server that supports this ITC interface.
 */
/**	An Inhibitor is a mechanism used by one or more clients
	to control a process such that it is prevented from
	proceeding is allowed to proceed by all clients. The Allow
	and Prevent operations must be balanced by a client such
	that an equal number of each operations result in the server
	being in an uninhibited (normal) state. The server typically
	maintains a counter that is incremented by a Prevent
	operation and decremented by an Allow operation. The server
	process is inhibited whenever the counter is non-zero.
 */
class Api {
	public:
		/** This class contains the data that is transfered
			between the client and server within the PreventReq
			ITC message.
		 */
		class PreventPayload {
			public:
			public:
				/** The PreventPayload constructor. */
				PreventPayload(
					) noexcept;

			};

		/**	This message indicates to the server that the process is
			not allowed to proceed. Each invocation of this message be
			matched by an Allow invocation.
		 */
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::Inhibitor::Req::Api,
					PreventPayload
					>		PreventReq;

	public:
		/** This class contains the data that is transfered
			between the client and server within the AllowReq
			ITC message.
		 */
		class AllowPayload {
			public:
			public:
				/** The AllowPayload constructor. */
				AllowPayload(
					) noexcept;

			};

		/**	This message indicates to the server that the process is
			allowed to proceed by this client.
		 */
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::Inhibitor::Req::Api,
					AllowPayload
					>		AllowReq;

	public:
		/** This type defines a SAP (Service Access Point) for this
			ITC server interface. A SAP holds server address information
			that is required by clients to send messages to a server.
		 */
		typedef Oscl::Mt::Itc::SAP<	Oscl::Inhibitor::
							Req::Api
							>			SAP;

	public:
		/** This type defines a SAP (Service Access Point) for this
			ITC server interface. A SAP holds server address information
			that is required by clients to send messages to a server.
		 */
		typedef Oscl::Mt::Itc::ConcreteSAP<	Oscl::Inhibitor::
							Req::Api
							>			ConcreteSAP;

	public:
		/** Make the compiler happy. */
		virtual ~Api(){}

		/** This operation is invoked within the server thread
			whenever a PreventReq is received.
		 */
		virtual void	request(	Oscl::Inhibitor::
									Req::Api::PreventReq& msg
									) noexcept=0;

		/** This operation is invoked within the server thread
			whenever a AllowReq is received.
		 */
		virtual void	request(	Oscl::Inhibitor::
									Req::Api::AllowReq& msg
									) noexcept=0;

	};

}
}
}
#endif
