/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_inhibitor_respcomph_
#define _oscl_inhibitor_respcomph_

#include "respapi.h"

/** */
namespace Oscl {

/** */
namespace Inhibitor {

/** */
namespace Resp {

/**	This template class implements a kind of GOF decorator
	pattern allows the context to employ composition instead
	of inheritance. Frequently, a context may need to implement
	more than one interface that may result in method name
	clashes. This template solves the problem by implementing
	the interface and then invoking member function pointers
	in the context instead. One instance of this object is
	created in the context for each interface of this type used
	in the context. Each instance is constructed such that it
	refers to different member functions within the context.
	The interface of this object is passed to any object that
	requires the interface. When the object invokes any of the
	operations of the API, the corresponding member function
	of the context will subsequently be invoked.
 */
/**	An Inhibitor is a mechanism used by one or more clients
	to control a process such that it is prevented from
	proceeding is allowed to proceed by all clients. The Allow
	and Prevent operations must be balanced by a client such
	that an equal number of each operations result in the server
	being in an uninhibited (normal) state. The server typically
	maintains a counter that is incremented by a Prevent
	operation and decremented by an Allow operation. The server
	process is inhibited whenever the counter is non-zero.
 */
template <class Context>
class Composer : public Api {
	private:
		/** A reference to the Context.
		 */
		Context&	_context;

	private:
		/**	This message indicates to the server that the process is
			not allowed to proceed. Each invocation of this message be
			matched by an Allow invocation.
		 */
		void	(Context::*_Prevent)(Oscl::Inhibitor::Resp::Api::PreventResp& msg);

		/**	This message indicates to the server that the process is
			allowed to proceed by this client.
		 */
		void	(Context::*_Allow)(Oscl::Inhibitor::Resp::Api::AllowResp& msg);

	public:
		/** */
		Composer(
			Context&		context,
			void	(Context::*Prevent)(Oscl::Inhibitor::Resp::Api::PreventResp& msg),
			void	(Context::*Allow)(Oscl::Inhibitor::Resp::Api::AllowResp& msg)
			) noexcept;

	private:
		/**	This message indicates to the server that the process is
			not allowed to proceed. Each invocation of this message be
			matched by an Allow invocation.
		 */
		void	response(Oscl::Inhibitor::Resp::Api::PreventResp& msg) noexcept;

		/**	This message indicates to the server that the process is
			allowed to proceed by this client.
		 */
		void	response(Oscl::Inhibitor::Resp::Api::AllowResp& msg) noexcept;

	};

template <class Context>
Composer<Context>::Composer(
			Context&		context,
			void	(Context::*Prevent)(Oscl::Inhibitor::Resp::Api::PreventResp& msg),
			void	(Context::*Allow)(Oscl::Inhibitor::Resp::Api::AllowResp& msg)
			) noexcept:
		_context(context),
		_Prevent(Prevent),
		_Allow(Allow)
		{
	}

template <class Context>
void	Composer<Context>::response(Oscl::Inhibitor::Resp::Api::PreventResp& msg) noexcept{
	(_context.*_Prevent)(msg);
	}

template <class Context>
void	Composer<Context>::response(Oscl::Inhibitor::Resp::Api::AllowResp& msg) noexcept{
	(_context.*_Allow)(msg);
	}

}
}
}
#endif
