#ifndef _oscl_rfc_rfc2396_rfc2396h_
#define _oscl_rfc_rfc2396_rfc2396h_

namespace Oscl {
namespace RFC {
namespace RFC2396 {

/** Returns true if the specified character is "lowalpha". */
bool	isLowAlpha(char c) noexcept;

/** Returns true if the specified character is "upalpha". */
bool	isUpAlpha(char c) noexcept;

/** Returns true if the specified character is "alpha". */
bool	isAlpha(char c) noexcept;

/** Returns true if the specified character is "digit". */
bool	isDigit(char c) noexcept;

/** Returns true if the specified character is "alphanum". */
bool	isAlphaNum(char c) noexcept;

/** Returns true if the specified character is "reserved". */
bool	isReservedCharacter(char c) noexcept;

/** Returns true if the specified character is "mark". */
bool	isMarkCharacter(char c) noexcept;

/** Returns true if the specified character is "Unreserved". */
bool	isUnreservedCharacter(char c) noexcept;

/** Returns true if the specified character is "control". */
bool	isControl(char c) noexcept;

/** Returns true if the specified character is "space". */
bool	isSpace(char c) noexcept;

/** Returns true if the specified character is "delims". */
bool	isDelimiter(char c) noexcept;

/** Returns true if the specified character is "unwise". */
bool	isUnwise(char c) noexcept;

/** Returns true if the specified character is an excluded URI character. */
bool	isExcluded(char c) noexcept;
}
}
}


#endif
