#include "rfc2396.h"

/** Returns true if the specified character is "lowalpha".

      lowalpha = "a" | "b" | "c" | "d" | "e" | "f" | "g" | "h" | "i" |
                 "j" | "k" | "l" | "m" | "n" | "o" | "p" | "q" | "r" |
                 "s" | "t" | "u" | "v" | "w" | "x" | "y" | "z"
 */
bool	Oscl::RFC::RFC2396::isLowAlpha(char c) noexcept{
	if(c < 'a') return false;
	if(c > 'z') return false;
	return true;
	}

/** Returns true if the specified character is "upalpha".

      upalpha  = "A" | "B" | "C" | "D" | "E" | "F" | "G" | "H" | "I" |
                 "J" | "K" | "L" | "M" | "N" | "O" | "P" | "Q" | "R" |
                 "S" | "T" | "U" | "V" | "W" | "X" | "Y" | "Z"
 */
bool	Oscl::RFC::RFC2396::isUpAlpha(char c) noexcept{
	if(c < 'A') return false;
	if(c > 'Z') return false;
	return true;
	}

/** Returns true if the specified character is "alpha".

      alpha = lowalpha | upalpha
 */
bool	Oscl::RFC::RFC2396::isAlpha(char c) noexcept{
	if(isLowAlpha(c)) return true;
	if(isUpAlpha(c)) return true;
	return false;
	}

/** Returns true if the specified character is "digit".
      digit    = "0" | "1" | "2" | "3" | "4" | "5" | "6" | "7" |
                 "8" | "9"

 */
bool	Oscl::RFC::RFC2396::isDigit(char c) noexcept{
	if(c < '0') return false;
	if(c > '9') return false;
	return true;
	}

/** Returns true if the specified character is "alphanum".

      alphanum = alpha | digit
 */
bool	Oscl::RFC::RFC2396::isAlphaNum(char c) noexcept{
	if(isAlpha(c)) return true;
	if(isDigit(c)) return true;
	return false;
	}

/** Returns true if the specified character is "reserved".

      reserved    = ";" | "/" | "?" | ":" | "@" | "&" | "=" | "+" |
                    "$" | ","
 */
bool	Oscl::RFC::RFC2396::isReservedCharacter(char c) noexcept{
	if(c == ';') return true;
	if(c == '/') return true;
	if(c == '?') return true;
	if(c == ':') return true;
	if(c == '@') return true;
	if(c == '&') return true;
	if(c == '=') return true;
	if(c == '+') return true;
	if(c == '$') return true;
	if(c == ',') return true;
	return false;
	}

/** Returns true if the specified character is "mark".

      mark        = "-" | "_" | "." | "!" | "~" | "*" | "'" | "(" | ")"
*/
bool	Oscl::RFC::RFC2396::isMarkCharacter(char c) noexcept{
	if(c == '-') return true;
	if(c == '_') return true;
	if(c == '.') return true;
	if(c == '!') return true;
	if(c == '~') return true;
	if(c == '*') return true;
	if(c == '\'') return true;
	if(c == '(') return true;
	if(c == '|') return true;
	if(c == ')') return true;
	return false;
	}

/** Returns true if the specified character is "Unreserved". */
bool	Oscl::RFC::RFC2396::isUnreservedCharacter(char c) noexcept{
	if(isAlphaNum(c)) return true;
	if(isMarkCharacter(c)) return true;
	return false;
	}

/** Returns true if the specified character is "control". */
bool	Oscl::RFC::RFC2396::isControl(char c) noexcept{
	if(c < 0x20){
		return true;
		}
	if(c == 0x7F){
		return true;
		}
	return false;
	}

/** Returns true if the specified character is "space". */
bool	Oscl::RFC::RFC2396::isSpace(char c) noexcept{
	if(c == 0x20){
		return true;
		}
	return false;
	}

/** Returns true if the specified character is "delims". */
bool	Oscl::RFC::RFC2396::isDelimiter(char c) noexcept{
	if(c == '<') return true;
	if(c == '>') return true;
	if(c == '#') return true;
	if(c == '%') return true;
	if(c == '"') return true;
	return false;
	}

/** Returns true if the specified character is "unwise". */
bool	Oscl::RFC::RFC2396::isUnwise(char c) noexcept{
	if(c == '{') return true;
	if(c == '}') return true;
	if(c == '|') return true;
	if(c == '\\') return true;
	if(c == '^') return true;
	if(c == '[') return true;
	if(c == ']') return true;
	if(c == '`') return true;
	return false;
	}

/** Returns true if the specified character is an excluded URI character. */
bool	Oscl::RFC::RFC2396::isExcluded(char c) noexcept{
	if(isControl(c)) return true;
	if(isSpace(c)) return true;
	if(isDelimiter(c)) return true;
	if(isUnwise(c)) return true;
	return false;
	}
