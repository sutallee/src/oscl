#include <stdio.h>
#include "stream.h"
#include "oscl/rfc/rfc2396/rfc2396.h"

Oscl::RFC::RFC2396::Encoder::Stream::Stream(	Oscl::Stream::Output::Api&	streamApi,
												const char*					buffer,
												unsigned					length
												) noexcept:
		_streamApi(streamApi),
		_buffer(buffer),
		_remaining(length),
		_state(*this)
		{
	}

bool	Oscl::RFC::RFC2396::Encoder::Stream::encode(){
	while(_remaining){
		if(_state.process(*_buffer)){
			return true;
			}
		}
	return false;
	}

bool	Oscl::RFC::RFC2396::Encoder::Stream::appendEscape() noexcept{
	if(_streamApi.write("%",1) < 1){
		return true;
		}
	return false;
	}

bool	Oscl::RFC::RFC2396::Encoder::Stream::appendFirstHex(char c) noexcept{
	char	buffer[16];
	sprintf(buffer,"%1.1X",(unsigned)((c>>4)&0x0F));
	if(_streamApi.write(buffer,1) < 1){
		return true;
		}
	return false;
	}

bool	Oscl::RFC::RFC2396::Encoder::Stream::appendSecondHex(char c) noexcept{
	char	buffer[16];
	sprintf(buffer,"%1.1X",(unsigned)(c&0x0F));
	if(_streamApi.write(buffer,1) < 1){
		return true;
		}
	return false;
	}

bool	Oscl::RFC::RFC2396::Encoder::Stream::append(char c) noexcept{
	if(_streamApi.write(&c,1) < 1){
		return true;
		}
	return false;
	}

void	Oscl::RFC::RFC2396::Encoder::Stream::nextCharacter() noexcept{
	--_remaining;
	++_buffer;
	}

