#ifndef _oscl_rfc_rfc2396_encoder_stream_streamh_
#define _oscl_rfc_rfc2396_encoder_stream_streamh_
#include "oscl/rfc/rfc2396/encoder/fsm/fsm.h"
#include "oscl/stream/output/api.h"

/** */
namespace Oscl {
/** */
namespace RFC {
/** */
namespace RFC2396 {
/** */
namespace Encoder {

/** */
class Stream : public FSM::StateVar::ContextApi {
	private:
		/** */
		Oscl::Stream::Output::Api&	_streamApi;
		/** */
		const char*					_buffer;
		/** */
		unsigned					_remaining;
		/** */
		FSM::StateVar				_state;

	public:
		/** */
		Stream(		Oscl::Stream::Output::Api&	streamApi,
					const char*					buffer,
					unsigned					length
					) noexcept;

		/** */
		bool	encode();

	private:
		/** Returns true if failed */
		bool	appendEscape() noexcept;
		/** Returns true if failed */
		bool	appendFirstHex(char c) noexcept;
		/** Returns true if failed */
		bool	appendSecondHex(char c) noexcept;
		/** Returns true if failed */
		bool	append(char c) noexcept;
		/** */
		void	nextCharacter() noexcept;
	};
}
}
}
}


#endif
