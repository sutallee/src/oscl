#ifndef _oscl_rfc_rfc2396_encoder_fsm_fsmh_
#define _oscl_rfc_rfc2396_encoderh_

/** */
namespace Oscl {
/** */
namespace RFC {
/** */
namespace RFC2396 {
/** */
namespace Encoder {
/** */
namespace FSM {

/** */
class StateVar {
	public:
		/** */
		class ContextApi {
			public:
				/** Returns true if failed */
				virtual bool	appendEscape() noexcept=0;
				/** Returns true if failed */
				virtual bool	appendFirstHex(char c) noexcept=0;
				/** Returns true if failed */
				virtual bool	appendSecondHex(char c) noexcept=0;
				/** Returns true if failed */
				virtual bool	append(char c) noexcept=0;
				/** Returns true if failed */
				virtual void	nextCharacter() noexcept=0;
			};

	private:
		/** */
		class State {
			public:
				/** Shut-up warnings. */
				virtual ~State(){}

				/** */
				virtual bool	process(	ContextApi& context,
											StateVar&	sv,
											char c
											) const noexcept=0;
			};

	public:
		/** */
		class Idle : public State {
			private:
				/** */
				bool	process(	ContextApi& context,
									StateVar&	sv,
									char c
									) const noexcept;
			};

		/** */
		class EscapeSent : public State {
			private:
				/** */
				bool	process(	ContextApi& context,
									StateVar&	sv,
									char c
									) const noexcept;
			};

		/** */
		class FirstHexSent : public State {
			private:
				/** */
				bool	process(	ContextApi& context,
									StateVar&	sv,
									char c
									) const noexcept;
			};
	private:
		/** */
		ContextApi&		_context;
		/** */
		const State*	_state;

	public:
		/** */
		StateVar(ContextApi& context) noexcept;

	public:
		/** */
		bool	process(char c) noexcept;

	private:
		friend class State;
		friend class Idle;
		friend class EscapeSent;
		friend class FirstHexSent;

		void	changeToIdle() noexcept;
		void	changeToEscapeSent() noexcept;
		void	changeToFirstHexSent() noexcept;
	};

}
}
}
}
}


#endif
