#include "fsm.h"
#include "oscl/rfc/rfc2396/rfc2396.h"

bool	Oscl::RFC::RFC2396::Encoder::FSM::StateVar::Idle::process(	ContextApi& context,
																	StateVar&	sv,
																	char c
																	) const noexcept{
	if(Oscl::RFC::RFC2396::isReservedCharacter(c)){
		if(context.appendEscape()){
			return true;
			}
		sv.changeToEscapeSent();
		return false;
		}
	context.append(c);
	context.nextCharacter();
	return false;
	}

bool	Oscl::RFC::RFC2396::Encoder::FSM::StateVar::EscapeSent::process(	ContextApi& context,
																			StateVar&	sv,
																			char c
																			) const noexcept{
	if(context.appendFirstHex(c)){
		return true;
		}
	sv.changeToFirstHexSent();
	return false;
	}

bool	Oscl::RFC::RFC2396::Encoder::FSM::StateVar::FirstHexSent::process(	ContextApi& context,
																			StateVar&	sv,
																			char c
																			) const noexcept{
	if(context.appendSecondHex(c)){
		return true;
		}
	sv.changeToIdle();
	context.nextCharacter();
	return false;
	}

static const Oscl::RFC::RFC2396::Encoder::FSM::StateVar::Idle			_idle;
static const Oscl::RFC::RFC2396::Encoder::FSM::StateVar::EscapeSent		_escapeSent;
static const Oscl::RFC::RFC2396::Encoder::FSM::StateVar::FirstHexSent	_firstHexSent;

Oscl::RFC::RFC2396::Encoder::FSM::StateVar::StateVar(ContextApi& context) noexcept:
		_context(context),
		_state(&_idle)
		{
	}

bool	Oscl::RFC::RFC2396::Encoder::FSM::StateVar::process(char c) noexcept{
	return _state->process(_context,*this,c);
	}

void	Oscl::RFC::RFC2396::Encoder::FSM::StateVar::changeToIdle() noexcept{
	_state	= &_idle;
	}

void	Oscl::RFC::RFC2396::Encoder::FSM::StateVar::changeToEscapeSent() noexcept{
	_state	= &_escapeSent;
	}

void	Oscl::RFC::RFC2396::Encoder::FSM::StateVar::changeToFirstHexSent() noexcept{
	_state	= &_firstHexSent;
	}

