#ifndef _oscl_rfc_rfc2822_rfc2822h_
#define _oscl_rfc_rfc2822_rfc2822h_

namespace Oscl {
namespace RFC {
namespace RFC2822 {

/** Returns true if 32 < character < 127 */
bool	isPrintable(char c) noexcept;

/** Returns if character is valid for a header name. */
bool	isValidHeaderName(char c) noexcept;

/** */
bool	isValidHeaderBody(char c) noexcept;

}
}
}

#endif
