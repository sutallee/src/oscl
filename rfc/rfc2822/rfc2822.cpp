#include "rfc2822.h"

/** */
bool	Oscl::RFC::RFC2822::isPrintable(char c) noexcept{
	if(c < 33) return false;
	if(c > 126) return false;
	return true;
	}

/** */
bool	Oscl::RFC::RFC2822::isValidHeaderName(char c) noexcept{
	if(!isPrintable(c)) return false;
	if(c == ':') return false;
	return true;
	}

