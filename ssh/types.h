/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_ssh_typesh_
#define _oscl_ssh_typesh_
#include "oscl/stream/input/exact.h"
#include "oscl/buffer/var.h"
#include "oscl/memory/block.h"
#include <stdint.h>

/** */
namespace Oscl {
/** */
namespace SSH {

enum{maxUncompressedPayloadSize=32768};
enum{maxPacketSize=35000};
enum{maxPacketHeaderSize=1+4};
enum{maxMacSize=maxPacketSize-maxPacketHeaderSize-maxUncompressedPayloadSize};

enum{	compressionTypeNone	= 0,
		compressionTypeZip	= 1
		};

enum{	encryptionTypeNone		= 0,
		encryptionTypeIdeaCBC	= 1,
		encryptionType3desCBC	= 2,
		encryptionTypeDesCBC	= 3,
		encryptionTypeARCFOUR	= 4
		};

enum{	macTypeNone	= 0,		// length = 0
		macTypeHmacMD5	= 1,	// length = 16
		macTypeHmacSHA	= 2,	// length = 20
		macTypeMacMD5	= 3		// length = 16, data+key
		};

enum{	keyExchangeTypeRsaSHA	= 0,
		keyExchangeTypeDhSHA	= 1
		};

/** Returns the number of bytes used to encode
	the "value" as a vlint32.
 */
inline unsigned	vlint32Size(unsigned long value) {
	if(value < 0x20){
		return 1;
		}
	else if(value < 0x2000){
		return 2;
		}
	else if(value < 0x200000){
		return 3;
		}
	return 5;
	}

/** */
class DecryptApi {
	public:
		/** Returns true if the decryption is successful */
		virtual bool	decryptInPlace(	void*			buffer,
										unsigned long	length
										) noexcept=0;
	};

/** */
class EncryptApi {
	public:
		/** Returns true if the encryption is successful */
		virtual bool	encryptInPlace(	void*			buffer,
										unsigned long	length
										) noexcept=0;
	};

/** */
class CompressApi {
	public:
		/** Returns true if the compression is successful */
		virtual bool	compressInPlace(	void*			buffer,
											unsigned long	length
											) noexcept=0;
	};

/** */
class DecompressApi {
	public:
		/** Returns true if the decompression is successful */
		virtual bool	decompressInPlace(	void*			buffer,
											unsigned long	length
											) noexcept=0;
	};

/** */
class Input : public Oscl::Stream::Input::Api {
	private:
		/** */
		Oscl::Stream::Input::Exact	_input;
		/** */
		uint32_t					_stringRemaining;
		/** */
		uint32_t					_sequenceNumber;

	public:
		/** */
		DecryptApi*				_decryptApi;
		/** */
		DecompressApi*			_decompressApi;

	public:
		/** */
		Input(Oscl::Stream::Input::Api& input) noexcept;

		/** Returns true if successful. False is only
			returned if the stream read fails. The
			"output" parameter is only modified if
			successful.
		 */
		bool 	vlint32(uint32_t&	output) noexcept;

		/** Returns true if successful. False is only
			returned if the stream read fails. The
			"output" parameter is only modified if
			successful.
		 */
		bool 	uint16(uint16_t&	output) noexcept;

		/** Returns true if successful. False is only
			returned if the stream read fails. The
			"output" parameter is only modified if
			successful.
		 */
		bool 	uint32(uint32_t&	output) noexcept;

		/** Returns true if successful. False is only
			returned if the stream read fails. The
			"output" parameter is only modified if
			successful.
		 */
		bool 	boolean(bool&	output) noexcept;

		/** Returns true if successful. False is only
			returned if the stream read fails. The
			"length",and "more" parameter are only
			modified if successful. The "dest" parameter
			may be modified if the stream read fails.
		 */
		bool 	string(	uint32_t&	length,
						bool&		more,
						void*		dest,
						unsigned	maxLen
						) noexcept;

	public: // Oscl::Stream::Input::Api
		/** */
		unsigned long	read(	void*			dest,
								unsigned long	maxSize
								) noexcept;

		/** */
		unsigned long	read(Oscl::Buffer::Base& dest) noexcept;
	};

/** */
class PacketEncoder {
	public:
		/** */
		Oscl::Buffer::Var							_vBuff;
		/** */
		Oscl::Memory::
		AlignedBlock<sizeof(Oscl::Buffer::Var)>		_nameLenMem;
		/** */
		Oscl::Buffer::Var*							_nameLen;
		/** */
		unsigned									_lengthBeforeNameList;
		/** */
		const unsigned								_cipherBlockSizePow2;

	public:
		/** */
		PacketEncoder(	void*		buffer,
						unsigned	size,
						unsigned	cipherBlockSizePow2=3
						) noexcept;

		/** Pushes the vlint32 value onto the head of the buffer
			rather than the tail of the buffer.
		 */
		bool	preEncVlint32(uint32_t value) noexcept;

		/** */
		bool	vlint32(uint32_t value) noexcept;

		/** */
		bool	uint16(uint16_t value) noexcept;


////	New/Correct stuff
		/** */
		bool	preEncUint32(uint32_t value) noexcept;
		/** */
		bool	boolean(bool value) noexcept;

		/** */
		bool	uint32(uint32_t value) noexcept;

		/** */
		bool	uint64(uint16_t value) noexcept;

		/** */
		bool	string(	const void*	source,
						unsigned	length
						) noexcept;

		/** Number bytes are assumed network byte order */
		bool	mpint(	const unsigned char*	number,
						unsigned				length
						) noexcept;

		/** Number bytes are assumed network byte order */
		bool	mpuint(	const unsigned char*	number,
						unsigned				length
						) noexcept;

		/** */
		bool	openNameList() noexcept;

		/** */
		bool	addName(	const void*	source,
							unsigned	length
							) noexcept;

		/** Null terminated */
		bool	addName(const char*	name) noexcept;

		/** */
		bool	closeNameList() noexcept;

		/** */
		bool	padPayload() noexcept;

		/** */
		bool	final() noexcept;

		/** */
		bool	append(	const void*	source,
						unsigned	length
						) noexcept;
	};

}
}

#endif
