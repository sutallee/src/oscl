/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <new>
#include <string.h>
#include "types.h"
#include "oscl/endian/type.h"

using namespace Oscl::SSH;
using namespace Oscl;

Input::Input(Oscl::Stream::Input::Api& input) noexcept:
		_input(input),
		_stringRemaining(0),
		_sequenceNumber(0),
		_decryptApi(0),
		_decompressApi(0)
		{
	}

bool 	Input::vlint32(uint32_t&	output) noexcept{
	uint32_t		value;
	unsigned char	firstByte;
	unsigned	len	= _input.read(&firstByte,1);
	if(len != 1){
		return false;
		}
	unsigned	nFollowing;
	switch((firstByte & 0xC0)>>6){
		case 0:
			output		= (firstByte & 0x3F);
			return true;
		case 1:
			value		= (firstByte & 0x3F);
			nFollowing	= 1;
			break;
		case 2:
			value		= (firstByte & 0x3F);
			nFollowing	= 2;
			break;
		case 3:
		default:
			value		= 0;
			nFollowing	= 4;
			break;
		}
	
	unsigned char	buffer[4];
	len	= _input.read(buffer,nFollowing);
	if(len != nFollowing){
		return false;
		}

	for(unsigned i=0;i<nFollowing;++i){
		value	<<= 8;
		value	|= buffer[i];
		}

	return true;
	}
bool 	Input::uint16(uint16_t&	output) noexcept{
	unsigned char	buffer[2];
	unsigned len	= _input.read(buffer,2);
	if(len != 2){
		return false;
		}

	uint16_t		value	= 0;
	for(unsigned i=0;i<2;++i){
		value	<<= 8;
		value	|= buffer[i];
		}
	output	= value;
	return true;
	}

bool 	Input::uint32(uint32_t&	output) noexcept{
	unsigned char	buffer[4];
	unsigned len	= _input.read(buffer,4);
	if(len != 4){
		return false;
		}

	uint32_t	value = 0;
	for(unsigned i=0;i<4;++i){
		value	<<= 8;
		value	|= buffer[i];
		}
	output	= value;
	return true;
	}
bool 	Input::boolean(bool&	output) noexcept{
	unsigned char	value;
	unsigned	len	= _input.read(&value,1);
	if(len != 1){
		return false;
		}
	output	= value;
	return true;
	}
bool 	Input::string(	uint32_t&	length,
				bool&		more,
				void*		dest,
				unsigned	maxLen
				) noexcept{
	if(!_stringRemaining){
		if(!uint32(_stringRemaining)){
			return false;
			}
		}
	unsigned long	n;
	if(maxLen < _stringRemaining){
		n	= maxLen;
		}
	else {
		n	= _stringRemaining;
		}
	if(!n){
		length	= 0;
		more	= false;
		return true;
		}
	
	unsigned	len	= _input.read(dest,n);
	if(len != n){
		return false;
		}
	_stringRemaining	-= len;
	more				= _stringRemaining;
	length				= len;
	return true;
	}

unsigned long	Input::read(	void*			dest,
						unsigned long	maxSize
						) noexcept{
	return _input.read(dest,maxSize);
	}
unsigned long	Input::read(Oscl::Buffer::Base& dest) noexcept{
	return _input.read(dest);
	}

//////// PacketEncoder

PacketEncoder::PacketEncoder(	void*		buffer,
								unsigned	size,
								unsigned	cipherBlockSizePow2
								) noexcept:
		_vBuff(buffer,size),
		_nameLen(0),
		_cipherBlockSizePow2((cipherBlockSizePow2<3)?3:cipherBlockSizePow2)
		{
	_vBuff.reserveHeader(maxPacketHeaderSize);
	}

static unsigned	encodeVlint32(unsigned char	buffer[5],uint32_t value){
	unsigned		len;
	if(value < 0x20){
		buffer[0]	= (unsigned char)value;
		len			= 1;
		}
	else if(value < 0x2000){
		buffer[1]	= (unsigned char)value;
		value		>>=	8;
		value		|= 0x40;
		buffer[0]	= (unsigned char)value;
		len			= 2;
		}
	else if(value < 0x200000){
		buffer[2]	= (unsigned char)value;
		value		>>=	8;
		buffer[1]	= (unsigned char)value;
		value		>>=	8;
		value		|= 0x80;
		buffer[0]	= (unsigned char)value;
		len			= 3;
		}
	else {
		buffer[4]	= (unsigned char)value;
		value		>>=	8;
		buffer[3]	= (unsigned char)value;
		value		>>=	8;
		buffer[2]	= (unsigned char)value;
		value		>>=	8;
		buffer[1]	= (unsigned char)value;
		buffer[0]	= 0xC0;
		len			= 5;
		}
	return len;
	}

bool	PacketEncoder::preEncVlint32(uint32_t value) noexcept{
	unsigned char	buffer[5];
	unsigned		len	= encodeVlint32(buffer,value);
	return (_vBuff.prepend(buffer,len)==len);
	}

bool	PacketEncoder::vlint32(uint32_t value) noexcept{
	unsigned char	buffer[5];
	unsigned		len	= encodeVlint32(buffer,value);
	return (_vBuff.append(buffer,len)==len);
	}

bool	PacketEncoder::uint16(uint16_t value) noexcept{
	Oscl::Endian::Big::U16	enc;
	enc	= value;
	return (_vBuff.append(&enc,2) == 2);
	}

// New/Correct stuff
bool	PacketEncoder::preEncUint32(uint32_t value) noexcept{
	Oscl::Endian::Big::U32	enc;
	enc	= value;
	return (_vBuff.prepend(&enc,4)==4);
	}

bool	PacketEncoder::boolean(bool value) noexcept{
	unsigned char	c	= value;
	return (_vBuff.append(&c,1) == 1);
	}

bool	PacketEncoder::uint32(uint32_t value) noexcept{
	Oscl::Endian::Big::U32	enc;
	enc	= value;
	return (_vBuff.append(&enc,4) == 4);
	}

bool	PacketEncoder::uint64(uint16_t value) noexcept{
	unsigned char	buffer[8];
	buffer[7]	= (unsigned char)value;
	value		>>= 8;
	buffer[6]	= (unsigned char)value;
	value		>>= 8;
	buffer[5]	= (unsigned char)value;
	value		>>= 8;
	buffer[4]	= (unsigned char)value;
	value		>>= 8;
	buffer[3]	= (unsigned char)value;
	value		>>= 8;
	buffer[2]	= (unsigned char)value;
	value		>>= 8;
	buffer[1]	= (unsigned char)value;
	value		>>= 8;
	buffer[0]	= (unsigned char)value;
	return (_vBuff.append(&buffer,8) == 8);
	}

bool	PacketEncoder::string(	const void*	source,
								unsigned	length
								) noexcept{
	if(!uint32(length)){
		return false;
		}
	return append(source,length);
	}

bool	PacketEncoder::mpint(	const unsigned char*	number,
								unsigned				length
								) noexcept{
	if(!length){
		return uint32(0);
		}
	bool	zero	= true;
	for(unsigned i=0;i<length;++i){
		if(number[i]){
			zero	= false;
			break;
			}
		}
	if(zero){
		return uint32(0);
		}
	if(number[0] & 0x80){
		// We're negative
		// Find the first byte without all ones.
		unsigned	i=0;
		for(;i<length;++i){
			if(number[i] != 0xFF){
				break;
				}
			}
		if(i<length){
			if(number[i] & 0x80){
				uint32(length-i);
				}
			else {
				uint32((length-i)+1);
				const unsigned char	c	= 0xFF;
				append(&c,1);
				}
			for(;i<length;++i){
				if(!append(&number[i],1)){	
					return false;
					}
				}
			return true;
			}
		else {
			// -1 exception
			// Must have at least one byte
			uint32(1);
			return append(&number[0],1);
			}
		}
	// We're positive
	// Find the first byte without all zeros
	unsigned	i=0;
	for(;i<length;++i){
		if(number[i] != 0x00){
			break;
			}
		}
	if(i<length){
		if(number[i] & 0x80){
			uint32((length-i)+1);
			const unsigned char	c	= 0x00;
			append(&c,1);
			}
		else {
			uint32(length-i);
			}
		for(;i<length;++i){
			if(!append(&number[i],1)){	
				return false;
				}
			}
		return true;
		}
	else {
		// 0 exception
		return uint32(0);
		}
	}

bool	PacketEncoder::mpuint(	const unsigned char*	number,
								unsigned				length
								) noexcept{
	if(!length){
		return uint32(0);
		}
	bool	zero	= true;
	for(unsigned i=0;i<length;++i){
		if(number[i]){
			zero	= false;
			break;
			}
		}
	if(zero){
		return uint32(0);
		}
	if(number[0] & 0x80){
		// Since the MSB is set, we must
		// insert a zero byte.
		uint32(length+1);
		const unsigned char	c	= 0x00;
		append(&c,1);
		return append(number,length);
		}

	uint32(length);
	return append(number,length);
	}

bool	PacketEncoder::openNameList() noexcept{
	if(_nameLen){
		// FIXME: This is actually a protocol error
		// as only one open name list is allowed
		// at a time.
		return false;
		}
	unsigned char*	data	= (unsigned char*)_vBuff.startOfData();
	_nameLen	= new(&_nameLenMem)
					Oscl::Buffer::Var(	&data[_vBuff.length()],
										4
										);
	if(!_vBuff.advance(4)){
		_nameLen->~Var();
		_nameLen	= 0;
		return false;
		}
	_lengthBeforeNameList	= _vBuff.length();
	return true;
	}

bool	PacketEncoder::addName(	const void*	source,
								unsigned	length
								) noexcept{
	// Notice: I'm not checking for valid values!
	return append(source,length);
	}

bool	PacketEncoder::addName(const char*	name) noexcept{
	return addName(name,strlen(name));
	}

bool	PacketEncoder::closeNameList() noexcept{
	if(!_nameLen){
		return false;
		}
	uint32_t		length	= _vBuff.length() - _lengthBeforeNameList;
	unsigned char	buffer[4];
	buffer[3]		= (unsigned char)length;
	length			>>=	8;
	buffer[2]		= (unsigned char)length;
	length			>>=	8;
	buffer[1]		= (unsigned char)length;
	length			>>=	8;
	buffer[0]		= (unsigned char)length;
	_nameLen->append(buffer,4);
	_nameLen->~Var();
	_nameLen		= 0;
	return true;
	}

bool	PacketEncoder::padPayload() noexcept{
	unsigned	payloadLength	= _vBuff.length();
	unsigned	length			= payloadLength;
	const unsigned	roundUp	= (1<<_cipherBlockSizePow2)-1;
	length		+= 4+1+4+roundUp;
							//		payload length field size
							//	+	padding length field size
							//	+	minimum padding
							//	+	preparation for floor()
	length		&= ~roundUp;	// round/floor()
	unsigned char
	paddingLength	= (unsigned char)(length-(payloadLength+4+1));
	unsigned char	padding[12] = {1,2,3,4,5,6,7,8,9,10,11,12};	// FIXME: randomize these bytes
	if(!_vBuff.append(padding,paddingLength)){
		return false;
		}
	return _vBuff.prepend(&paddingLength,1);
	}

bool	PacketEncoder::final() noexcept{
	return preEncUint32(_vBuff.length());
	}

bool	PacketEncoder::append(	const void*	source,
								unsigned	length
								) noexcept{
	return (_vBuff.append(source,length) == length);
	}

