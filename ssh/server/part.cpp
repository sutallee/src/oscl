/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <new>
#include <string.h>
#include <stdio.h>
#include "part.h"
#include "oscl/mt/itc/mbox/syncrh.h"
#include "oscl/error/info.h"
#include "oscl/bignum/value.h"
#include "oscl/checksum/sha1/calculator.h"
#include "oscl/timer/counter.h"
#include "oscl/entropy/random.h"

using namespace Oscl::SSH::Server;

static const char	sshVersion[]	= {"SSH-1.5-1.2.27\n"};

static const unsigned char	numA[]	= {
	0xAA,0xBB,0xCC,0xFF,0xDD,0xFF,0xFF,0xEE,
	0x79,0x0F,0x8A,0xA2,0x21,0x68,0xC2,0x34,
	0xC4,0xC6,0xA2,0x84,0x80,0xDC,0x1C,0xD1,
	0x25,0x02,0x4E,0x08,0x8A,0x67,0xCC,0x74,
	0x02,0x0B,0xCE,0xA6,0x3B,0x13,0x9B,0x22,
	0x51,0x4A,0x08,0x73,0x8E,0x34,0x04,0xD2,
	0xE6,0x95,0x19,0xB3,0xCD,0x3A,0x43,0x1B,
	0x30,0x2B,0x9A,0x6D,0x22,0x5F,0x14,0x37,
	0x4F,0xE1,0x35,0x9A,0x6D,0x51,0xC2,0x45,
	0xE4,0x85,0xB5,0x76,0x62,0x5E,0x7E,0xC6,
	0xF4,0x1C,0x42,0xE9,0xF6,0x37,0xED,0x61,
	0x0B,0xFF,0x5C,0xB6,0xF4,0x06,0xB7,0xED,
	0xEE,0x38,0x6B,0xFB,0x5A,0x89,0x9F,0xA5,
	0xAE,0x2F,0x24,0x11,0x7C,0x4B,0x1F,0x96,
	0x47,0x28,0x66,0x51,0xEC,0xE6,0x53,0x81,
	0xF8,0xFF,0xF0,0xAF,0xFF,0x0F,0xFF,0x3F
	};

static const unsigned char	numB[]	= {
	0xAF,0xBB,0xCC,0xF5,0xDD,0xFF,0xFF,0xEE,
	0x79,0xEF,0x8A,0xA2,0x21,0x68,0xC2,0x34,
	0xC4,0xC6,0xA2,0x8B,0x80,0x6C,0x1C,0x71,
	0x25,0x02,0x4E,0x08,0x8A,0x67,0xCC,0x74,
	0x02,0x0B,0xCE,0xA6,0x3B,0x13,0x9B,0x21,
	0x51,0xDA,0x08,0x7C,0x8E,0x34,0x04,0xD2,
	0xE6,0x05,0x19,0xB3,0xCD,0x3A,0x43,0x1B,
	0x30,0x2B,0x9A,0x6D,0x22,0x5F,0x14,0x37,
	0x4F,0xE1,0x35,0x9A,0x6D,0x51,0xC2,0x45,
	0xE4,0xD5,0xB5,0x7E,0x6F,0x5E,0x7E,0x86,
	0xF4,0xCC,0x42,0xE9,0x16,0x97,0xED,0x61,
	0x0B,0xFF,0x34,0xB6,0x24,0x06,0xB7,0x2D,
	0xEE,0x78,0x6B,0xF8,0x5A,0x89,0x9F,0xA5,
	0xAE,0x2F,0x24,0x11,0x7C,0x4B,0x1F,0x96,
	0x47,0x28,0xB6,0x51,0xEC,0xE6,0x53,0x81,
	0xF8,0x6F,0xF0,0xAA,0x5F,0x0F,0x34,0x9F
	};

static const unsigned char	diffieHellmanGenerator	= 2;
static const unsigned char	diffieHellmanPrime[]	= {
	0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,
	0xC9,0x0F,0xDA,0xA2,0x21,0x68,0xC2,0x34,
	0xC4,0xC6,0x62,0x8B,0x80,0xDC,0x1C,0xD1,
	0x29,0x02,0x4E,0x08,0x8A,0x67,0xCC,0x74,
	0x02,0x0B,0xBE,0xA6,0x3B,0x13,0x9B,0x22,
	0x51,0x4A,0x08,0x79,0x8E,0x34,0x04,0xDD,
	0xEF,0x95,0x19,0xB3,0xCD,0x3A,0x43,0x1B,
	0x30,0x2B,0x0A,0x6D,0xF2,0x5F,0x14,0x37,
	0x4F,0xE1,0x35,0x6D,0x6D,0x51,0xC2,0x45,
	0xE4,0x85,0xB5,0x76,0x62,0x5E,0x7E,0xC6,
	0xF4,0x4C,0x42,0xE9,0xA6,0x37,0xED,0x6B,
	0x0B,0xFF,0x5C,0xB6,0xF4,0x06,0xB7,0xED,
	0xEE,0x38,0x6B,0xFB,0x5A,0x89,0x9F,0xA5,
	0xAE,0x9F,0x24,0x11,0x7C,0x4B,0x1F,0xE6,
	0x49,0x28,0x66,0x51,0xEC,0xE6,0x53,0x81,
	0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF
	};

static const Oscl::BigNum::Value<Part::bigSize>
	diffieHellmanPrimeConst(	diffieHellmanPrime,
								sizeof(diffieHellmanPrime)
								);

const Oscl::BigNum::Value<Part::bigSize>	numAConst(numA,sizeof(numA));
const Oscl::BigNum::Value<Part::bigSize>	numBConst(numB,sizeof(numB));

Part::Part(	Oscl::Mt::Itc::PostMsgApi&				myPapi,
			Oscl::Stream::Input::Req::Api::SAP&		inputSAP,
			Oscl::Stream::Output::Req::Api::SAP&	outputSAP,
			RxTransMem								rxTransMem[],
			unsigned								nRxTransMem
			) noexcept:
		_myPapi(myPapi),
		_sync(*this,myPapi),
		_inputSAP(inputSAP),
		_outputSAP(outputSAP),
		_pendingRxTransactions(0),
		_closeReq(0),
		_open(false),
		_closing(false)
		{
	for(unsigned i=0;i<nRxTransMem;++i){
		_freeRxTransMem.put(&rxTransMem[i]);
		}
	}

void	Part::open() noexcept{
	_sync.syncOpen();
	}

void	Part::sendRxReq(RxTransMem& mem) noexcept{
	Oscl::Stream::Input::
	Req::Api::ReadPayload*
	payload	= new (&mem._rxMem._readMem.payload)
		Oscl::Stream::Input::
		Req::Api::ReadPayload(	&mem._rxMem._buffer,
								sizeof(mem._rxMem._buffer)
								);
	Oscl::Stream::Input::
	Resp::Api::ReadResp*
	resp	= new (&mem._rxMem._readMem.resp)
				Oscl::Stream::Input::
				Resp::Api::ReadResp(	_inputSAP.getReqApi(),
										*this,
										_myPapi,
										*payload
										);
	_inputSAP.post(resp->getSrvMsg());
	}

void	Part::ready(Frame& frame) noexcept{
	}

RxTransMem*		Part::allocRxTransMem() noexcept{
	RxTransMem*	mem	= _freeRxTransMem.get();
	if(mem){
		++_pendingRxTransactions;
		}
	return mem;
	}

void			Part::free(RxTransMem& mem) noexcept{
	_freeRxTransMem.put(&mem);
	--_pendingRxTransactions;
	}

bool	Part::sendSync(	const void*	message,
						unsigned	length
						) noexcept{
	Oscl::Mt::Itc::
	SyncReturnHandler		srh;
	const Oscl::Buffer::Var	var(	(unsigned char*)message,
									length,
									length
									);
	Oscl::Stream::Output::
	Req::Api::WritePayload	payload(var);
	Oscl::Stream::Output::
	Req::Api::WriteReq		req(	_outputSAP.getReqApi(),
									payload,
									srh
									);
	_outputSAP.postSync(req);
	if(payload._nWritten != length){
		return false;
		}
	return true;
	}

bool	Part::sendSshVersion() noexcept{
	return sendSync(sshVersion,strlen(sshVersion));
	}

bool	Part::sendKeyDHInit() noexcept{
	Oscl::Error::Info::log("SSH Generating d-h e:\n\r");
	unsigned char						randomDigest[Oscl::Checksum::SHA1::HashSize];
	Oscl::Checksum::SHA1::Calculator	sha1Calc;

// SSH sez:
// Randomize our secret value (x or y) and public value (e or f).
// We'll use only 192 bits of entropy for faster exponentiation.
// ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
// For discussion, see:
// P. C. van Oorschot and M. J. Wiener, "On Diffie-Hellman Key Agreement
// with Short Exponents", proc. Eurocrypt 96
// ^^^^^^^^^^^^^^^

// NOTE: Here, I'm generating an sha1 digest using 32-bits of randomness
// resulting in a 160 bit (20 byte) value.
// The d-h prime number is 1024 bits (128 bytes).

	unsigned long	random	= OsclEntropyGetRandom();
	sha1Calc.input(&random,sizeof(random));
	sha1Calc.result(randomDigest);
	_clientX	= Oscl::BigNum::Value<Part::bigSize>(	randomDigest,
												Oscl::Checksum::SHA1::HashSize
												);
	--_clientX;
	_clientX	>>= 1;
	_clientE	= 2;	// Diffie-Hellman generator constant
	unsigned long	start	= OsclTimerGetMillisecondCounter();
	_clientE.powXmodY(_clientX,diffieHellmanPrimeConst);
	unsigned long	end		= OsclTimerGetMillisecondCounter();
	char			timeBuff[32];
	unsigned long	duration	= end-start;
	sprintf(timeBuff,"%lu ms",duration);
	Oscl::Error::Info::log("SSH d-h e time: ");
	Oscl::Error::Info::log(timeBuff);
	Oscl::Error::Info::log("\n\r");

	Oscl::SSH::PacketEncoder	enc(	&_payloadMem,
										sizeof(_payloadMem)
										);
	const unsigned char	sshMsgKEXDH_INIT	= 30;
	enc.append(&sshMsgKEXDH_INIT,1);
	enc.mpuint((const unsigned char*)&_clientE,128);

	enc.padPayload();
	enc.final();

	return sendSync(enc._vBuff.startOfData(),enc._vBuff.length());
	}

#if 0
struct {
	unsigned long	start;
	unsigned long	end;
	}dhTime;

static void dhTimeTest() {
	Oscl::BigNum::Value<Part::bigSize>	x;
	x	= numBConst;
	Oscl::BigNum::Value<Part::bigSize>	e;
	e	= numAConst;
	dhTime.start	= OsclTimerGetMillisecondCounter();
	e.powXmodY(x,diffieHellmanPrimeConst);
	dhTime.end	= OsclTimerGetMillisecondCounter();
	char	timeBuff[32];
	unsigned long	duration	= dhTime.end-dhTime.start;
	sprintf(timeBuff,"%lu ms",duration);
	Oscl::Error::Info::log("D-H done: ");
	Oscl::Error::Info::log(timeBuff);
	Oscl::Error::Info::log("\n\r");
	}
#endif

bool	Part::sendKeyExchange() noexcept{
	unsigned char		randomDigest[Oscl::Checksum::SHA1::HashSize];
	Oscl::Checksum::SHA1::Calculator	sha1Calc;
	unsigned long	random	= OsclEntropyGetRandom();
	sha1Calc.input(&random,sizeof(random));
	sha1Calc.result(randomDigest);

	Oscl::SSH::PacketEncoder	enc(	&_payloadMem,
										sizeof(_payloadMem)
										);
	const unsigned char	sshMsgKexinit	= 20;
	enc.append(&sshMsgKexinit,1);
	// Cookie
	enc.append(randomDigest,16);	// First 16 bytes of SHA1 Hash
	// KEX Algorithms
	enc.openNameList();
	enc.addName("diffie-hellman-group1-sha1");
	enc.closeNameList();
	// Server Host Key Algorithms
	enc.openNameList();
	enc.addName("ssh-dss");
	enc.closeNameList();
	// Encryption Algorithms Client to Server
	enc.openNameList();
	enc.addName("none");
	enc.closeNameList();
	// Encryption Algorithms Server to Client
	enc.openNameList();
	enc.addName("none");
	enc.closeNameList();
	// MAC algorithms client to server
	enc.openNameList();
	enc.addName("none");
	enc.closeNameList();
	// MAC algorithms server to client
	enc.openNameList();
	enc.addName("none");
	enc.closeNameList();
	// Compression algorithms client to server
	enc.openNameList();
	enc.addName("none");
	enc.closeNameList();
	// Compression algorithms server to client
	enc.openNameList();
	enc.addName("none");
	enc.closeNameList();
	// Languages client to server
	enc.openNameList();
	enc.closeNameList();
	// Languages server to client
	enc.openNameList();
	enc.closeNameList();
	// First KEX packet follows
	enc.boolean(true);
	// Reserved
	enc.uint32(0);

	enc.padPayload();
	enc.final();

	return sendSync(enc._vBuff.startOfData(),enc._vBuff.length());
	}

void	Part::request(	Oscl::Mt::Itc::Srv::
						Open::Req::Api::OpenReq&	msg
						) noexcept{
	RxTransMem*	mem;
	while((mem=allocRxTransMem())){
		sendRxReq(*mem);
		}

	if(sendSshVersion()){
		Oscl::Error::Info::log("ssh opened\n\r");
		}
	else{
		Oscl::Error::Info::log("ssh version not sent\n\r");
		}
	if(sendKeyExchange()){
		Oscl::Error::Info::log("ssh kex sent\n\r");
		}
	if(sendKeyDHInit()){
		Oscl::Error::Info::log("ssh kex d-h sent\n\r");
		}
	else{
		Oscl::Error::Info::log("ssh kex not sent\n\r");
		}
	_open	= true;
	msg.returnToSender();
	}

void	Part::request(	Oscl::Mt::Itc::Srv::
						Close::Req::Api::CloseReq&	msg
						) noexcept{
	}

void	Part::response(	Oscl::Stream::Input::
						Resp::Api::ReadResp&	msg
						) noexcept{
	if(!msg._payload._buffer.length()){
		msg.~ReadResp();
		free(*(RxTransMem*)&msg);
		return;
		}
	// FIXME: This is temporary
	msg.~ReadResp();
	sendRxReq(*(RxTransMem*)&msg);
	}

void	Part::response(	Oscl::Stream::Output::
						Resp::Api::WriteResp&	msg
						) noexcept{
	}

void	Part::response(	Oscl::Stream::Output::
						Resp::Api::FlushResp&	msg
						) noexcept{
	}

