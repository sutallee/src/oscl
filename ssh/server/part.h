/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_ssh_server_parth_
#define _oscl_ssh_server_parth_
#include "oscl/stream/input/itc/respmem.h"
#include "oscl/stream/output/itc/respmem.h"
#include "oscl/mt/itc/srv/close.h"
#include "oscl/mt/itc/srv/closeapi.h"
#include "oscl/mt/runnable.h"
#include "oscl/ssh/types.h"
#include "oscl/bignum/value.h"
#include "frame.h"

/** */
namespace Oscl {
/** */
namespace SSH {
/** */
namespace Server {

/** */
struct RxTransRec {
	/** */
	Oscl::Stream::Input::Resp::ReadRespMem		_readMem;
	/** */
	enum{rxBufferSize=256};
	/** */
	Oscl::Memory::AlignedBlock< rxBufferSize >	_buffer;
	};

/** */
union RxTransMem {
	void*		__qitemlink;
	RxTransRec	_rxMem;
	};

/** */
class Part :	private Oscl::Mt::Itc::Srv::Close::Req::Api,
				private Oscl::Stream::Input::Resp::Api,
				private Oscl::Stream::Output::Resp::Api
			{
	public:
		/** */
		using Oscl::Stream::Input::Resp::Api::response;

	public:
		/** */
		enum{bigSize=128/sizeof(Oscl::BigNum::FragType)};
	private:
		/** */
		Oscl::BigNum::Value<bigSize>				_clientX;
		/** */
		Oscl::BigNum::Value<bigSize>				_clientE;
		/** */
		Oscl::BigNum::Value<bigSize>				_clientK;
		/** */
		Oscl::BigNum::Value<bigSize>				_clientH;
	private:
		/** */
		Oscl::Mt::Itc::PostMsgApi&					_myPapi;
		/** */
		Oscl::Mt::Itc::Srv::CloseSync				_sync;
		/** */
		Oscl::Stream::Input::Req::Api::SAP&			_inputSAP;
		/** */
		Oscl::Stream::Output::Req::Api::SAP&		_outputSAP;
		/** */
		Oscl::Memory::
		AlignedBlock<maxUncompressedPayloadSize>	_payloadMem;
		/** */
		Oscl::Queue<RxTransMem>							_freeRxTransMem;
		/** */
		unsigned										_pendingRxTransactions;
		/** */
		Oscl::Mt::Itc::Srv::Close::Req::Api::CloseReq*	_closeReq;
		/** */
		bool											_open;
		/** */
		bool											_closing;

	public:
		/** */
		Part(	Oscl::Mt::Itc::PostMsgApi&				myPapi,
				Oscl::Stream::Input::Req::Api::SAP&		inputSAP,
				Oscl::Stream::Output::Req::Api::SAP&	outputSAP,
				RxTransMem								rxTransMem[],
				unsigned								nRxTransMem
				) noexcept;

		/** */
		void	open() noexcept;

		/** */
		void	sendRxReq(RxTransMem& mem) noexcept;

		/** This operation is invoked when the new frame
			has been received. At this point its contents
			have been decrypted and the payload is ready
			to be parsed/fowarded.
		 */
		void	ready(Frame& frame) noexcept;

		/** */
		RxTransMem*		allocRxTransMem() noexcept;

		/** */
		void			free(RxTransMem& mem) noexcept;

		/** Returns true if successfull, false if stream closed. */
		bool	sendSync(	const void*	message,
							unsigned	length
							) noexcept;

		/** Returns true if successfull, false if stream closed. */
		bool	sendSshVersion() noexcept;

		/** Returns true if successfull, false if stream closed. */
		bool	sendKeyDHInit() noexcept;

		/** Returns true if successfull, false if stream closed. */
		bool	sendKeyExchange() noexcept;

	private: // Oscl::Mt::Itc::Srv::Close::Req::Api
		/** */
		void	request(	Oscl::Mt::Itc::Srv::
							Open::Req::Api::OpenReq&	msg
							) noexcept;
		/** */
		void	request(	Oscl::Mt::Itc::Srv::
							Close::Req::Api::CloseReq&	msg
							) noexcept;

	private: // Oscl::Stream::Input::Resp::Api
		/** */
		void	response(	Oscl::Stream::Input::
							Resp::Api::ReadResp&	msg
							) noexcept;
	private: //Oscl::Stream::Output::Resp::Api
		/** */
		void	response(	Oscl::Stream::Output::
							Resp::Api::WriteResp&	msg
							) noexcept;
		/** */
		void	response(	Oscl::Stream::Output::
							Resp::Api::FlushResp&	msg
							) noexcept;
	};

}
}
}

#endif
