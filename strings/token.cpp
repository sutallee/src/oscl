/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

//#include <string.h>
#include "strip.h"
#include "token.h"

/** */
namespace Oscl {
/** */
namespace Strings {
/** */
namespace Token {

/** This is identical in function to the <string.h> function
	of the same name. However, I've run across "standard"
	libraries that do not implement the function. Thus, for
	increased portability I implement it here.
 */
static char *strpbrk(const char *s, const char *accept){
	for(const char* p=s;*p;++p){
		for(const char* a = accept;*a;++a){
			if(*p == *a){
				return (char*)p;
				}
			}
		}
	return 0;
	}

char*	next(	char**		string,
				const char*	separators
				) noexcept{
	char*	s	= *string;	
	if(!s){
		return 0;
		}
	char*	p	= strpbrk(s,separators);
	if(p){
		*p	= '\0';
		++p;
		}

	char*	token	= s;
	*string			= p;

	return token;
	}

}
}
}

