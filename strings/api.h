/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_strings_apih_
#define _oscl_strings_apih_

#include <stdarg.h>

/** */
namespace Oscl {

/** */
namespace Strings {

/** */
class Api {
	protected:
		/** */
		bool	_truncated;
	public:
		/** */
		Api() noexcept;
		/** */
		virtual ~Api(){}
		/** */
		virtual Api&	operator += (char c) noexcept;
		/** */
		virtual Api&	operator += (signed long num) noexcept;
		/** */
		virtual Api&	operator += (signed num) noexcept;
		/** */
		virtual Api&	operator += (unsigned long num) noexcept;
		/** */
		virtual Api&	operator += (unsigned num) noexcept;
		/** */
		virtual Api&	operator += (const Api& s) noexcept;
		/** */
		virtual Api&	operator += (const char* s) noexcept;
		/** */
		virtual Api&	operator = (const char* s) noexcept;
		/** */
		virtual Api&	operator = (const Api& s) noexcept;
		/** */
		virtual bool	operator == (const char* s) const noexcept;
		/** */
		virtual bool	operator == (const Api& s) const noexcept;
		/** */
		virtual bool	operator != (const char* s) const noexcept;
		/** */
		virtual bool	operator != (const Api& s) const noexcept;
		/** */
		virtual bool	operator < (const Api& s) const noexcept;
		/** */
		virtual void	fillFromBuffer(const void* buffer,unsigned long nChars) noexcept;
		/** */
		virtual operator const char* () const noexcept;
		/** */
		virtual unsigned long length() const noexcept;
		/** */
		virtual void	convertToUpperCase() noexcept;
		/** */
		virtual void	convertToLowerCase() noexcept;
		/** */
		virtual void	charToUpperCase(unsigned offset) noexcept;
		/** */
		virtual void	charToLowerCase(unsigned offset) noexcept;

		/** Returns true if the string was truncated during a
			modification operation. The flag is cleared by the next
			successful non-truncating operation.
		 */
		inline virtual bool truncated() const noexcept{ return _truncated;}

		/** Remove the last n characters from the string.
			If n is greater than len, then the string is emptied.
		 */
		virtual void	truncateLast(unsigned n) noexcept;

	public:
		/** */
		virtual const char*	getString() const noexcept=0;

		/** This returns the maximum number of characters
			that the internal buffer can hold excluding
			a null-termination.
		 */
		virtual unsigned long maxLength() const noexcept=0;

	protected:
		/** */
		virtual char*	getBuffer() noexcept=0;

	public:
		/** */
		void	append(
						const char*	format,
						...
						) noexcept __attribute__ ((format (printf, 2, 3)));

		void	vappend(
					const char*	format,
					va_list		ap
					) noexcept;

		/** */
		void	vassign(
					const char*	format,
					va_list		ap
					) noexcept;
		/** */
		void	assign(
						const char*	format,
						...
						) noexcept __attribute__ ((format (printf, 2, 3)));
	};

}
}

#endif
