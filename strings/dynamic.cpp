/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "dynamic.h"

using namespace Oscl::Strings;

static void	myFree(void* p) noexcept{
	free(p);
	}

static void*	myMalloc(size_t size) noexcept{
	void*	p	= malloc(size);
	return p;
	}

/*inline*/ void	Dynamic::alloc(unsigned long stringLength){
	unsigned long length;
	length		= stringLength+1+_blockSize;
	_maxLength	= (length/_blockSize);
	_maxLength	*= _blockSize;
	_string		= (char*)myMalloc(_maxLength);
	if(!_string){
		exit(1);
		}
	_maxLength	-= 1;
	}

/*inline*/ void	Dynamic::alloc(const char* string){
	if(!string){
		printf("%s: !string\n",__PRETTY_FUNCTION__);
		string	= "";
		}
	size_t
	length		= strlen(string);
	alloc(length);
	if(length > _maxLength){
		printf("%s:(length:%zu) > (_maxLength:%zu)\n",__PRETTY_FUNCTION__,length,_maxLength);
		exit(1);
		}
	strcpy(_string,string);
	_length	= length;
	}

Dynamic::Dynamic(const char* string,unsigned long blockSize) noexcept:
		_string(0),
		_blockSize(blockSize)
		{
	if(!string){
		string="";
		}
	alloc(string);
	}

Dynamic::Dynamic(const Dynamic& s) noexcept{
	Api::operator=(s);
	if(truncated()){
		if(_string){
			myFree(_string);
			_string	= 0;
			}
		alloc(s);
		}
	}

Dynamic::~Dynamic() noexcept{
	if(_string){
		myFree(_string);
		_string	= 0;
		}
	}

Dynamic&	Dynamic::operator = (const Dynamic& s) noexcept{
	const Api&	sapi	= s;
	Api::operator=(sapi);
	if(truncated()){
		if(_string){
			myFree(_string);
			_string	= 0;
			}
		alloc(s);
		}
	return *this;
	}

Api&	Dynamic::operator += (char c) noexcept{
	Api::operator+=(c);

	if(truncated()){
		char*	s	= _string;
		alloc(_maxLength+_blockSize);
		Api::operator=(s);
		Api::operator+=(c);
		myFree(s);
		}

	++_length;

	return *this;
	}

Api&	Dynamic::operator += (signed long num) noexcept{
	Api::operator+=(num);
	if(truncated()){
		char*	s	= _string;
		alloc(_maxLength+_blockSize);
		Api::operator=(s);
		Api::operator+=(num);
		myFree(s);
		}
	_length	= strlen(_string);
	return *this;
	}

Api&	Dynamic::operator += (signed num) noexcept{
	Api::operator+=(num);
	if(truncated()){
		char*	s	= _string;
		alloc(_maxLength+_blockSize);
		Api::operator=(s);
		Api::operator+=(num);
		myFree(s);
		}
	_length	= strlen(_string);
	return *this;
	}

Api&	Dynamic::operator += (unsigned long num) noexcept{
	Api::operator+=(num);
	if(truncated()){
		char*	s	= _string;
		alloc(_maxLength+_blockSize);
		Api::operator=(s);
		Api::operator+=(num);
		myFree(s);
		}
	_length	= strlen(_string);
	return *this;
	}

Api&	Dynamic::operator += (unsigned num) noexcept{
	Api::operator+=(num);
	if(truncated()){
		char*	s	= _string;
		alloc(_maxLength+_blockSize);
		Api::operator=(s);
		Api::operator+=(num);
		myFree(s);
		}
	_length	= strlen(_string);
	return *this;
	}

Api&	Dynamic::operator += (const Api& scat) noexcept{
	Api::operator+=(scat);
	if(truncated()){
		char*	s	= _string;
		alloc(_maxLength+_blockSize+scat.length());
		Api::operator=(s);
		Api::operator+=(scat);
		myFree(s);
		}
	_length	= strlen(_string);
	return *this;
	}

Api&	Dynamic::operator += (const char* scat) noexcept{
	Api::operator+=(scat);
	if(truncated()){
		size_t	len	= strlen(scat);
		char*	s	= _string;
		alloc(_maxLength+_blockSize+len);
		Api::operator=(s);
		Api::operator+=(scat);
		myFree(s);
		}
	_length	= strlen(_string);
	return *this;
	}

Api&	Dynamic::operator = (const char* s) noexcept{
	Api::operator=(s);
	if(truncated()){
		myFree(_string);
		alloc(s);
		}
	else {
		_length	= strlen(s);
		}
	return *this;
	}

Api&	Dynamic::operator = (const Api& s) noexcept{
	Api::operator=(s);
	if(truncated()){
		myFree(_string);
		alloc(s);
		}
	else {
		_length	= strlen(s);
		}
	return *this;
	}

void	Dynamic::fillFromBuffer(const void* buffer,unsigned long nChars) noexcept{
	if(nChars > _maxLength){
		myFree(_string);
		_string	= (char*)myMalloc(nChars);
		strncpy(_string,(const char*)buffer,nChars);
		_string[nChars]	= '\0';
		_length	= strlen(_string);
		}
	else {
		Api::fillFromBuffer( buffer, nChars );
		_length	= strlen(_string);
		}
	}

unsigned long Dynamic::length() const noexcept{
	return _length;
	}

void	Dynamic::truncateLast(unsigned n) noexcept{
	Api::truncateLast(n);

	_length	= Api::length();
	}

const char*	Dynamic::getString() const noexcept{
	return _string;
	}

unsigned long Dynamic::maxLength() const noexcept{
	return _maxLength;
	}

char*	Dynamic::getBuffer() noexcept{
	return _string;
	}

