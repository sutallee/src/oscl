/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_strings_dynamich_
#define _oscl_strings_dynamich_
#include "api.h"
#include <stddef.h>
//#include <stdint.h>

/** */
namespace Oscl {

/** */
namespace Strings {

/** */
class Dynamic : public Strings::Api {
	public:
		/** */
		using Strings::Api::operator=;
	private:
		/** */
		char*			_string;

		/** */
		size_t	_maxLength;

		/** */
		size_t	_blockSize;

		/** */
		size_t	_length;

	public:
		/** */
		Dynamic(const char* string = 0,unsigned long blockSize=128) noexcept;
		/** */
		Dynamic(const Dynamic& d) noexcept;
		/** */
		~Dynamic() noexcept;
		/** */
		Dynamic&	operator = (const Dynamic& s) noexcept;

	public: // Strings::Api overrides
		/** */
		Strings::Api&	operator += (char c) noexcept override;
		/** */
		Strings::Api&	operator += (signed long num) noexcept override;
		/** */
		Strings::Api&	operator += (signed num) noexcept override;
		/** */
		Strings::Api&	operator += (unsigned long num) noexcept override;
		/** */
		Strings::Api&	operator += (unsigned num) noexcept override;
		/** */
		Strings::Api&	operator += (const Strings::Api& s) noexcept override;
		/** */
		Strings::Api&	operator += (const char* s) noexcept override;
		/** */
		Strings::Api&	operator = (const char* s) noexcept override;
		/** */
		Strings::Api&	operator = (const Strings::Api& s) noexcept override;
		/** */
		void	fillFromBuffer(const void* buffer,unsigned long nChars) noexcept override;
		/** */
		unsigned long length() const noexcept override;

		/** Remove the last n characters from the string.
			If n is greater than len, then the string is emptied.
		 */
		void	truncateLast(unsigned n) noexcept override;

	public: // Strings::Api
		/** */
		const char*	getString() const noexcept override;
		/** */
		unsigned long maxLength() const noexcept override;

	protected:	// Strings::Api
		/** */
		char*	getBuffer() noexcept override;

	private:
		/** */
		void	alloc(unsigned long stringLength);
		/** */
		void	alloc(const char* s);
	};

}
}

#endif
