/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "api.h"
#include <string.h>
#include <stdio.h>
#include <stdarg.h>
#include <ctype.h>
#ifndef __GNUC__
#ifdef __ENEA_OSE__
#include <outfmt.h>
#else
#error	"need definition of snprintf()"
#endif
#endif

using namespace Oscl::Strings;

/**
	To support both dynamic and static strings, I will require dynamic
	strings to polymorphically override the appropriate members and
	then use the base operators subsequent to buffer allocation and
	expansion. Thus, this base class will implement the static
	functionality which is common to both static and dynmic string
	manipulation.
	We can assume and require that every modification operation
	results in a null terminated string.
 */

void	Api::vappend(
				const char*	format,
				va_list		ap
				) noexcept {
	char*			p			= getBuffer();
	unsigned long	maxLen		= maxLength();
	unsigned long	curLen		= length();
	unsigned long	remaining	= maxLen - curLen;

	p[maxLen]	= '\0';	// This is cleared here to enable fast truncation check.

	unsigned
	n	= vsnprintf(
			&p[curLen],
			remaining+1,
			format,
			ap
			);

	p[maxLen] = '\0';

	_truncated	= (n >= (remaining+1))?true:false;
	}

inline void	Api::append(
				const char*	format,
				...
				) noexcept {
    va_list ap;

    va_start(ap,format);

	vappend(
		format,
		ap
		);

	va_end(ap);
	}

void	Api::vassign(
				const char*	format,
				va_list		ap
				) noexcept {
	char*			p			= getBuffer();
	unsigned long	maxLen		= maxLength();

	p[maxLen]	= '\0';	// This is cleared here to enable fast truncation check.

	unsigned
	n	= vsnprintf(
			p,
			maxLen+1,
			format,
			ap
			);

	p[maxLen] = '\0';

	_truncated	= (n >= (maxLen+1))?true:false;
	}

inline void	Api::assign(
				const char*	format,
				...
				) noexcept {
    va_list ap;

    va_start(ap,format);

	vassign(
		format,
		ap
		);

	va_end(ap);
	}

Api::Api() noexcept:
		_truncated(false)
		{
	}

Api&	Api::operator += (char c) noexcept{
	char	b[2];
	b[0]	= c;
	b[1]	= '\0';
	append(b);
	return *this;
	}

Api&	Api::operator += (signed long num) noexcept{
	// big enough for 128-bit signed long , sign, and null
	static const size_t	decimalBuffSize	= 39+1+1;
	char			b[decimalBuffSize];
	sprintf(b,"%ld",num);
	append(b);
	return *this;
	}

Api&	Api::operator += (signed num) noexcept{
	// big enough for 128-bit signed long , sign, and null
	static const size_t	decimalBuffSize	= 39+1+1;
	char			b[decimalBuffSize];
	snprintf(b,decimalBuffSize,"%d",num);
	append(b);
	return *this;
	}

Api&	Api::operator += (unsigned long num) noexcept{
	// big enough for 128-bit unsigned long and null
	static const size_t	decimalBuffSize	= 39+1;
	char			b[decimalBuffSize];
	snprintf(b,decimalBuffSize,"%lu",num);
	append(b);
	return *this;
	}

Api&	Api::operator += (unsigned num) noexcept{
	// big enough for 128-bit unsigned and null
	static const size_t	decimalBuffSize	= 39+1;
	char			b[decimalBuffSize];
	snprintf(b,decimalBuffSize,"%u",num);
	append(b);
	return *this;
	}

Api&	Api::operator += (const Api& s) noexcept{
	append(s.getString());
	return *this;
	}

Api&	Api::operator += (const char* s) noexcept{
	append(s);
	return *this;
	}

Api&	Api::operator = (const char* s) noexcept{
	assign("%s",s);
	return *this;
	}

Api&	Api::operator = (const Api& s) noexcept{
	assign("%s",s.getString());
	return *this;
	}

bool	Api::operator == (const char* s) const noexcept{
	return (strcmp(s,getString())==0);
	}

bool	Api::operator == (const Api& s) const noexcept{
	return (strcmp(s.getString(),getString())==0);
	}

bool	Api::operator != (const char* s) const noexcept{
	return (strcmp(s,getString())!=0);
	}

bool	Api::operator != (const Api& s) const noexcept{
	return (strcmp(s.getString(),getString())!=0);
	}

/** WARNING: This WAS backwards!
	Old code may be broken by the fix.
 */
bool	Api::operator < (const Api& s) const noexcept{
	return (strcmp(getString(),s.getString())<0)?true:false;
	}

void	Api::fillFromBuffer(const void* buffer,unsigned long nChars) noexcept{
	unsigned long	maxLen	= maxLength();
	unsigned long	n;
	if(nChars > maxLen){
		n			= maxLen;
		_truncated	= true;
		}
	else{
		n			= nChars;
		_truncated	= false;
		}
	char*			p		= getBuffer();
	memcpy(p,(const char*)buffer,n);
	p[n]	= '\0';	// This is cleared here to enable fast truncation check.
	}

Api::operator const char* () const noexcept{
	return getString();
	}

unsigned long Api::length() const noexcept{
	return strlen(getString());
	}

void	Api::convertToUpperCase() noexcept{
	char*			p			= getBuffer();
	for(unsigned i=0;p[i];++i){
		p[i]	= toupper(p[i]);
		}
	}

void	Api::convertToLowerCase() noexcept{
	char*			p			= getBuffer();
	for(unsigned i=0;p[i];++i){
		p[i]	= tolower(p[i]);
		}
	}

void	Api::charToUpperCase(unsigned offset) noexcept{
	if(offset > length()){
		return;
		}

	char*	p	= getBuffer();

	p[offset]	= toupper(p[offset]);
	}

void	Api::charToLowerCase(unsigned offset) noexcept{
	if(offset > length()){
		return;
		}

	char*	p	= getBuffer();

	p[offset]	= tolower(p[offset]);
	}

void	Api::truncateLast(unsigned n) noexcept{
	char*	p	= getBuffer();
	unsigned	len	= length();
	if(len < n){
		p[0]	= '\0';
		return;
		}
	p[len-n]	= '\0';
	}

