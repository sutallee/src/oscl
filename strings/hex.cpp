/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <ctype.h>
#include <stdio.h>
#include "oscl/strings/strip.h"
#include "hex.h"

char	Oscl::Strings::nibbleToLowerCaseHexAscii(
			unsigned char	nibble
			) noexcept{
	nibble	&= 0x0F;

	return (nibble < 10)?
				nibble+'0':
				nibble-10+'a'
				;
	}

void	Oscl::Strings::octetToLowerCaseHexAscii(
			unsigned char octet,
			char&			msn,
			char&			lsn
			) noexcept{
	octet	&= 0xFF;
	msn	= nibbleToLowerCaseHexAscii((octet>>4)&0x0F);
	lsn	= nibbleToLowerCaseHexAscii(octet&0x0F);
	}

bool	Oscl::Strings::asciiNibbleToBinary(
			char			nibble,
			unsigned char&	octet
			) noexcept{

	if(!isxdigit(nibble)){
		return true;
		}

	unsigned char	v	= (unsigned char)toupper(nibble);

	octet	= (v >= 'A')?
				v - 'A' + 10:
				v - '0'
				;

	return false;
	}

bool	Oscl::Strings::hexAsciiToBinary(
			unsigned char&	octet,
			char			msn,
			char			lsn
			) noexcept{

	if(!isxdigit(msn) || !isxdigit(lsn)){
		return true;
		}

	unsigned char	vmsn=0;

	if(asciiNibbleToBinary(msn,vmsn)){
		return true;
		}

	unsigned char	vlsn=0;

	if(asciiNibbleToBinary(lsn,vlsn)){
		return true;
		}

	octet	=	vmsn;
	octet	<<=	4;
	octet	|=	vlsn;

	return false;
	}

unsigned	Oscl::Strings::hexAsciiStringToBinary(
			const char*		hexAscii,
			void*			buffer,
			unsigned		bufferLen
			) noexcept{
	Oscl::Strings::stripSpace(hexAscii);

	const char*		p	= hexAscii;
	unsigned char*	b	= (unsigned char*)buffer;

	unsigned		i;

	for(i=0;(i<bufferLen)&&p[0]&&p[1];++i,p+=2){
		if(hexAsciiToBinary(b[i],p[0],p[1])){
			break;
			}
		}

	return i;
	}

char*	Oscl::Strings::binaryToHexAscii(
			const void*	binaryData,
			unsigned	binaryDataLength,
			char*		stringBuffer,
			unsigned	stringBufferLength
			) noexcept{

	int	len = stringBufferLength;

	const unsigned char*	d	= (const unsigned char*)binaryData;

	char*	p	= stringBuffer;
	for(
		unsigned	i = 0;
		(i < binaryDataLength) && (len > 0);
		++i
		){
		int
		n	= snprintf(
				p,
				len,
				"%2.2x",
				d[i]
				);
		p	+= n;
		len	-= n;
		}
	stringBuffer[stringBufferLength-1]	= '\0';

	return stringBuffer;
	}

