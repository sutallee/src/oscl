/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_strings_tokenh_
#define _oscl_strings_tokenh_

/** */
namespace Oscl {
/** */
namespace Strings {
/** */
namespace Token {

/** This function returns a pointer to the beginning
	of the next token in the string or zero if there
	are no more tokens in the string (i.e. that the
	original "string" argument points to a zero/NULL
	pointer.
	In the process, the original string is modified
	to insert a string terminator (i.e. null charcter).
	The original string pointer is modified to be advanced
	to the first character beyond the token, or to the
	string terminator if the string is empty.
	If *string == returnedTokenPointer, then there was
	a seperator match.
 */
char*	next(	char**		string,
				const char*	separators
				) noexcept;

}
}
}

#endif
