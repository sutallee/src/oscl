/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "base.h"

using namespace Oscl::Strings;

Base::Base(
	char*			buffer,
	unsigned long	bufferSize
	) noexcept :
		_string(buffer),
		_maxLength(bufferSize-1)
		{
	if(buffer && bufferSize){
		_string[0]='\0';
		}
	}

Base::Base(
	char*			buffer,
	unsigned long	bufferSize,
	const char*		format,
	...
	) noexcept:
		_string(buffer),
		_maxLength(bufferSize-1)
		{
	va_list ap;

	va_start(ap,format);

	Oscl::Strings::Api::vassign(
		format,
		ap
		);

	va_end(ap);
	}

Base::Base(
	char*				buffer,
	unsigned long		bufferSize,
	const Strings::Api& s
	) noexcept:
		_string(buffer),
		_maxLength(bufferSize-1)
		{
	Strings::Api::operator=(s);
	}

const char*	Base::getString() const noexcept{
	return _string;
	}

unsigned long Base::maxLength() const noexcept{
	return _maxLength;
	}

char*	Base::getBuffer() noexcept{
	return _string;
	}
