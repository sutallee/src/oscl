/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "tokens.h"
#include "strip.h"
#include <stdio.h>
#include <stdlib.h>
#include "dynamic.h"

using namespace Oscl::Strings;

Tokens::Tokens(const char* string) noexcept{
	Dynamic	duplicate(string);
	const char* dup	= duplicate;
	if(!dup){
		perror("Tokens::Tokens");
		exit(-1);
		}
	const char*	prev	= dup;
	char*		next;
	for(;*prev;prev=next){
		prev	= stripSpace(prev);
		if(!*prev) break;
		next	= const_cast<char*>(stripNotSpace(prev));
		if(*next){
			*next	= '\0';
			++next;
			}
		Node*	node	= new Node(prev);
		if(node){
			put(node);
			}
		else{
			perror("Tokens::Tokens");
			exit(-1);
			}
		}
	}

