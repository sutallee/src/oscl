/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <ctype.h>
#include "strip.h"

namespace Oscl {
namespace Strings {

const char *stripSpace(const char *s){
	while(*s && isspace(*s)) s++;
	return s;
	}

char *stripSpaceWritable(char *s){
	while(*s && isspace(*s)) s++;
	return s;
	}

const char *stripNotSpace(const char *s){
	while(*s && !(isspace(*s))) s++;
	return s;
	}

char *stripNotSpaceWritable(char *s){
	while(*s && !(isspace(*s))) s++;
	return s;
	}

const char *stripSeparator(const char *s, char separator ) {
	while(*s && ( *s == separator ) ) s++;
	return s;
	}

const char *stripNotSeparator(const char *s, char separator ) {
	while(*s && ( *s != separator ) ) s++;
	return s;
	}

}
}
