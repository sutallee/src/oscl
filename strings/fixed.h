/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_strings_fixedh_
#define _oscl_strings_fixedh_
#include "api.h"

/** */
namespace Oscl {
/** */
namespace Strings {

/** */
template <unsigned long strSize>
class Fixed : public Strings::Api {
	public:
		/** */
		using Strings::Api::operator=;

	private:
		/** */
		char	_string[strSize+1];

	public:
		/** */
		Fixed(){
			_string[0]='\0';
			}

		/** */
		Fixed(
			const char* format,
			...
			){
		    va_list ap;

		    va_start(ap,format);

			Oscl::Strings::Api::vassign(
				format,
				ap
				);

			va_end(ap);
			}

		Fixed(
			const char*	format,
			va_list		ap
			){
			Oscl::Strings::Api::vassign(
				format,
				ap
				);
			}

		/** */
		Fixed(const Strings::Api& s){
			_string[0]='\0';
			Strings::Api::operator=(s);
			}

	public:
		/** */
		const char*	getString() const noexcept override {return _string;}

		/** */
		unsigned long maxLength() const noexcept override {return strSize;}

	protected:
		/** */
		char*	getBuffer() noexcept{return _string;}
	};

}
}

#endif
