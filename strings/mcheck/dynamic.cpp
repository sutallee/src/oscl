/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
//#include <mcheck.h>
#include "oscl/strings/dynamic.h"

using namespace Oscl::Strings;

class Trace {
	private:
		const void*	_p;
		const char*	_f;
		const char*	_s;

	public:
		Trace(void* p,const char* f,const char* s) noexcept:
			_p(p),
			_f(f),
			_s(s)
			{
			printf("%p:%s IN \"%s\"\n",_p,_f,_s);
			fflush(stdout);
//			mcheck_check_all();
			}

		~Trace(){
//			mcheck_check_all();
			printf("%p:%s OUT \"%s\"\n",_p,_f,_s);
			fflush(stdout);
			}
	};

//#define MTRACE()	Trace(this,__PRETTY_FUNCTION__,_string)
#define MTRACE()

static void	myFree(void* p) noexcept{
//	printf("myFree(%p) IN\n",p);
//	fflush(stdout);
	free(p);
//	printf("myFree(%p) OUT\n",p);
	}

static void*	myMalloc(size_t size) noexcept{
	void*	p	= malloc(size);
//	printf("myMalloc(%d:%x): %p\n",size,size,p);
//	fflush(stdout);
	return p;
	}

/*inline*/ void	Dynamic::alloc(unsigned long stringLength){
	MTRACE();
//	printf("%s:stringLength:%lu\n",__PRETTY_FUNCTION__,stringLength);
	unsigned long length;
	length		= stringLength+1+_blockSize;
//	printf("%s:length:%lu\n",__PRETTY_FUNCTION__,length);
	_maxLength	= (length/_blockSize);
//	printf("%s:_maxLength:%u\n",__PRETTY_FUNCTION__,_maxLength);
	_maxLength	*= _blockSize;
//	printf("%s:_maxLength:%u\n",__PRETTY_FUNCTION__,_maxLength);
	_string		= (char*)myMalloc(_maxLength);
	if(!_string){
		exit(1);
		}
//	memset(_string,0xAA,_maxLength);
	_maxLength	-= 1;
//	printf("%s:_maxLength:%u\n",__PRETTY_FUNCTION__,_maxLength);
	}

/*inline*/ void	Dynamic::alloc(const char* string){
	MTRACE();
	if(!string){
		printf("%s: !string\n",__PRETTY_FUNCTION__);
//		mcheck_check_all();
		string	= "";
		}
	size_t
	length		= strlen(string);
//		printf("%s: before alloc(_length:%lu)\n",__PRETTY_FUNCTION__,length);
//		printf("%s: before alloc(_string:%p,length:%u,_maxLength:%u)\n",__PRETTY_FUNCTION__,_string,length,_maxLength);
//		mcheck_check_all();
	alloc(length);
//		printf("%s: after alloc(length:%lu,_maxLength:%lu,t:%lu)\n",__PRETTY_FUNCTION__,length,_maxLength,(unsigned long)t);
//		printf("%s: after alloc(_length:%u,_maxLength:%u)\n",__PRETTY_FUNCTION__,_length,_maxLength);
//		printf("%s: before strncpy(%p,\"%s\",%u)\n",__PRETTY_FUNCTION__,_string,string,_maxLength);
//		mcheck_check_all();
	if(length > _maxLength){
		printf("%s:(length:%zu) > (_maxLength:%zu)\n",__PRETTY_FUNCTION__,length,_maxLength);
		exit(1);
		}
	strcpy(_string,string);
//	_string[length]	= '\0';
//		printf("%s: after strcpy(%p,\"%s\",%u)\n",__PRETTY_FUNCTION__,_string,string,_maxLength);
//		mcheck_check_all();
	}

Dynamic::Dynamic(const char* string,unsigned long blockSize) noexcept:
		_string(0),
		_blockSize(blockSize)
		{
	MTRACE();
	if(!string){
		string="";
		}
	alloc(string);
	}

Dynamic::Dynamic(const Dynamic& s) noexcept{
	MTRACE();
	Api::operator=(s);
	if(truncated()){
		if(_string){
			myFree(_string);
			_string	= 0;
			}
		alloc(s);
		}
	}

Dynamic::~Dynamic() noexcept{
	MTRACE();
	if(_string){
		myFree(_string);
		_string	= 0;
		}
	}

Dynamic&	Dynamic::operator = (const Dynamic& s) noexcept{
	MTRACE();
	const Api&	sapi	= s;
	Api::operator=(sapi);
	if(truncated()){
		if(_string){
			myFree(_string);
			_string	= 0;
			}
		alloc(s);
		}
	return *this;
	}

Api&	Dynamic::operator += (signed long num) noexcept{
	MTRACE();
	Api::operator+=(num);
	if(truncated()){
		char*	s	= _string;
		alloc(_maxLength+_blockSize);
		Api::operator=(s);
		Api::operator+=(num);
		myFree(s);
		}
	_length	= strlen(_string);
	return *this;
	}

Api&	Dynamic::operator += (signed num) noexcept{
	MTRACE();
	Api::operator+=(num);
	if(truncated()){
		char*	s	= _string;
		alloc(_maxLength+_blockSize);
		Api::operator=(s);
		Api::operator+=(num);
		myFree(s);
		}
	_length	= strlen(_string);
	return *this;
	}

Api&	Dynamic::operator += (unsigned long num) noexcept{
	MTRACE();
	Api::operator+=(num);
	if(truncated()){
		char*	s	= _string;
		alloc(_maxLength+_blockSize);
		Api::operator=(s);
		Api::operator+=(num);
		myFree(s);
		}
	_length	= strlen(_string);
	return *this;
	}

Api&	Dynamic::operator += (unsigned num) noexcept{
	MTRACE();
	Api::operator+=(num);
	if(truncated()){
		char*	s	= _string;
		alloc(_maxLength+_blockSize);
		Api::operator=(s);
		Api::operator+=(num);
		myFree(s);
		}
	_length	= strlen(_string);
	return *this;
	}

Api&	Dynamic::operator += (const Api& scat) noexcept{
	MTRACE();
	Api::operator+=(scat);
	if(truncated()){
		char*	s	= _string;
		alloc(_maxLength+_blockSize+scat.length());
		Api::operator=(s);
		Api::operator+=(scat);
		myFree(s);
		}
	_length	= strlen(_string);
	return *this;
	}

Api&	Dynamic::operator += (const char* scat) noexcept{
	MTRACE();
	Api::operator+=(scat);
	if(truncated()){
		size_t	len	= strlen(scat);
		char*	s	= _string;
		alloc(_maxLength+_blockSize+len);
		Api::operator=(s);
		Api::operator+=(scat);
		myFree(s);
		}
	_length	= strlen(_string);
	return *this;
	}

Api&	Dynamic::operator = (const char* s) noexcept{
	MTRACE();
	Api::operator=(s);
	if(truncated()){
		myFree(_string);
		alloc(s);
		}
	return *this;
	}

Api&	Dynamic::operator = (const Api& s) noexcept{
	MTRACE();
	Api::operator=(s);
	if(truncated()){
		myFree(_string);
		alloc(s);
		}
	return *this;
	}

void	Dynamic::fillFromBuffer(const void* buffer,unsigned long nChars) noexcept{
	MTRACE();
	if(nChars > _maxLength){
		myFree(_string);
		_string	= (char*)myMalloc(nChars);
		strncpy(_string,(const char*)buffer,nChars);
		_string[nChars]	= '\0';
		_length	= strlen(_string);
		}
	}

unsigned long Dynamic::length() const noexcept{
	return _length;
	}

const char*	Dynamic::getString() const noexcept{
	return _string;
	}

unsigned long Dynamic::maxLength() const noexcept{
	return _maxLength;
	}

char*	Dynamic::getBuffer() noexcept{
	return _string;
	}

