/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_aes128_cipherh_
#define _oscl_aes128_cipherh_

#include <stdint.h>

/** */
namespace Oscl {
/** */
namespace AES128 {

/** */
class Cipher {

	private:
		/** */
		constexpr static unsigned	blockSizeInBits	= 128;

		/** */
		constexpr static unsigned	Nb	= blockSizeInBits/32;

		/** */
		constexpr static unsigned	keyLengthInBits	= 128;

		/** */
		constexpr static unsigned	Nk	= keyLengthInBits/32;

		/** */
		constexpr static unsigned	Nr	= Nk + 6;

		/** */
		constexpr static unsigned	expandedKeySize	= 4 * Nb * (Nr + 1);

	public:
		/** */
		uint8_t	_state[4][4];

		/** */
		uint8_t	_expandedKey[expandedKeySize];

	public:
		/**
			Constructor
			@param [in] key		16-octet key.
		 */
		Cipher(const void* key) noexcept;

		/**
			Reset using a new key.
			@param [in] key		16-octet key.
		 */
		void	reset(const void* key) noexcept;

		/** Set the cipher state to all zeros.
			This can be used for example in CBC
			mode to efficiently load the initialization
			vector.
		 */
		void	zero() noexcept;

		/**
			Run cipher on block with the current state.
			This can be used if the state is already loaded.
			An example is for CBC-MAC.
		 */
		void	cipher() noexcept;

		/**
			Run cipher on block
			@param [in] input	16-octet input block.
		 */
		void	cipher(
			const void*	input
			) noexcept;

		/**
			Run inverse cipher on block
			@param [in] input	16-octet input block.
		 */
		void	invCipher(
			const void*	input
			)	noexcept;

		/**
			Perform an XOR with the current cipher
			state and the 16-octet block referenced by io.
			@param [in,out] io	Reference to 16-octet io block.
		 */
		void	xorBlock(void* io) noexcept;

		/**
			Perform an XOR with the current cipher
			state and the 16-octet block referenced by io.
			@param [in] in		Reference to 16-octet input block.
			@param [out] out	Reference to 16-octet output block.
			@param [in] len		Number of octets to copy to output block.
		 */
		void	xorBlock(
					const void*	in,
					void*		out,
					unsigned	len	= 16
					) noexcept;

		/**
			Copy the 16-octet output block to the dest buffer.
			@param [out] dest	Reference to dest buffer.
			@param [in] output	Number of octets to copy. Limited to 16.
			@param [in] offset	state offset to start copy
		 */
		void	copyOut(
					void*		dest,
					unsigned	len = 16,
					unsigned	offset = 0
					) const noexcept;

		/**
			Copy the 16-octet source block to the state.
			This can be used in certain modes that need to
			set the initial state of cipher to some value.
			@param [in] in	Reference to input block.
		 */
		void	copyIn(const void* in) noexcept;

		/**
			Compare the block to the current state of the cipher.
			This is often use when the cipher is being used to
			accumulate a MAC, and avoids a copy-out operation.
			RETURN: true if they match.
			@param [in] block	Reference to input block.
		 */
		bool	matchState(const void*	block) const noexcept;

		/** This is a helper function for certain
			clients that need to perform an exclusive-OR
			on two buffers.
			@param [in] buff1	Reference to first buffer of length octets
			@param [in] buff2	Reference to second buffer of length octets
			@param [out] output	Reference to output buffer of length octets
			@param [in] length	Length of the xor operation on the 3 bufers.
		 */
		static void	xorBuffers(
						const void*	buff1,
						const void*	buff2,
						void*		output,
						unsigned	length	= 16
						) noexcept;

	private:
		/** */
		void	expand(const uint8_t* key) noexcept;

		/** */
		void	init(const uint8_t *src) noexcept;

		/** */
		void	addRoundKey(unsigned round) noexcept;

		/** */
		void	subBytes() noexcept;

		/** */
		void	shiftRows() noexcept;

		/** */
		void	mixColumns() noexcept;

		/** */
		void	invShiftRows() noexcept;

		/** */
		void	invSubBytes() noexcept;

		/** */
		void	invMix(unsigned n) noexcept;

		/** */
		void	invMixColumns() noexcept;

	};

}
}

#endif
