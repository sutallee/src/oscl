/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <string.h>
#include "ccm.h"
#include "cbcmac.h"
#include "ctr.h"
#include "cipher.h"
#include "oscl/encoder/be/base.h"

//#define DEBUG_TRACE

#ifdef DEBUG_TRACE
#include "oscl/error/info.h"
#endif

static constexpr unsigned	blockSize = 16;
static constexpr unsigned	L = 2;	// Octets in size field.
static constexpr unsigned	nonceLength = 15-L;

static constexpr uint8_t    zeros[blockSize] = {
	0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0
	};

static void	calculateCbcMAC(
				const void*				key,
				const void*				nonce,
				const void*				authData,
				unsigned				authDataLen,
				const void*				payload,
				unsigned				payloadLength,
				int						micLength,
				unsigned char*			outbuf
				) noexcept {

	unsigned char BUF[blockSize];

	Oscl::AES128::CbcMAC	mac(key);

	Oscl::Encoder::BE::Base	beEncoder(BUF,blockSize);
	Oscl::Encoder::Api&		encoder	= beEncoder;

	uint8_t
	flags	= ((authDataLen > 0 ? 1 : 0)) << 6;
	flags	|= ((micLength - 2) / 2) << 3;
	flags	|= (L - 1) << 0;

	encoder.encode(flags);

	encoder.copyIn(
		nonce,
		nonceLength
		);

	uint16_t
	len	= payloadLength;

	encoder.encode(len);

	mac.accumulate(
		BUF,
		encoder.length()
		);

	if(authDataLen > 0) {
		uint8_t*	adat	= (uint8_t*)authData;
		uint16_t
		aLen	= authDataLen;

		beEncoder.reset();
		encoder.encode(aLen);

		unsigned
		blockRemaining	= 16-encoder.length();

		unsigned
		len	= (authDataLen > blockRemaining)?blockRemaining:authDataLen;

		mac.accumulate(
			BUF,
			encoder.length()
			);

		mac.accumulate(
			adat,
			len
			);

		if(len < blockRemaining){
			mac.accumulate(
				zeros,
				blockRemaining - len
				);
			}

		authDataLen	-= len;

		mac.accumulate(
			&adat[len],
			authDataLen
			);

		mac.finalize();
		}

	mac.accumulate(
		payload,
		payloadLength
		);

	mac.finalize();

	mac.copyOut(
		outbuf,
		micLength
		);
	}

static inline void	buildHeader(
						uint8_t*	block,
						const void*	nonce,
						int			micLength,
						unsigned	authDataLen,
						unsigned	cipherTextLength
						) noexcept{

	Oscl::Encoder::BE::Base	beEncoder(block,blockSize);
	Oscl::Encoder::Api&		encoder	= beEncoder;

	uint8_t
	flags	= ((authDataLen > 0 ? 1 : 0)) << 6;
	flags	|= ((micLength - 2) / 2) << 3;
	flags	|= (L - 1) << 0;

	encoder.encode(flags);

	encoder.copyIn(
		nonce,
		nonceLength
		);

	uint16_t
	len	= cipherTextLength;

	encoder.encode(len);
	}

static void	verifyCbcMAC(
				const void*				key,
				const void*				nonce,
				const void*				authData,
				unsigned				authDataLen,
				const void*				cipher,
				unsigned				cipherTextLength,
				int						micLength,
				unsigned char*			outbuf
				) noexcept {
	const uint8_t*	adat	= (const uint8_t*)authData;

	unsigned char BUF[blockSize];

	buildHeader(
		BUF,
		nonce,
		micLength,
		authDataLen,
		cipherTextLength
		);

	Oscl::AES128::CbcMAC	mac(key);

	mac.accumulate(
		BUF,
		16
		);

	if(authDataLen > 0) {
		Oscl::Encoder::BE::Base	beEncoder(BUF,blockSize);
		Oscl::Encoder::Api&		encoder	= beEncoder;

		uint16_t
		aLen	= authDataLen;

		beEncoder.reset();
		encoder.encode(aLen);

		unsigned
		blockRemaining	= 16-encoder.length();

		unsigned
		len	= (authDataLen > blockRemaining)?blockRemaining:authDataLen;

		mac.accumulate(
			BUF,
			encoder.length()
			);

		mac.accumulate(
			adat,
			len
			);

		authDataLen	-= len;

		mac.accumulate(
			&adat[len],
			authDataLen
			);

		if(authDataLen && (authDataLen < blockSize)){
			mac.accumulate(
				zeros,
				blockSize - authDataLen
				);
			}

		mac.finalize();
		}

	if(cipherTextLength > 0) {
		const uint8_t*	ciph	= (const uint8_t*)cipher;

		unsigned
		remaining = cipherTextLength;

		uint8_t	nnonce[16];
		nnonce[0]	= 1;	// flags
		memcpy(&nnonce[1],nonce,nonceLength);

		for(
			unsigned counter=1,
			size	= 0,
			offset	= 0
			;
			remaining
			;
			++counter,
			offset		+=blockSize,
			remaining	-= size
			) {

			size	= (remaining < blockSize)?remaining:blockSize;

			Oscl::AES128::ctrEncrypt(
				key,
				nnonce,
				nonceLength+1,
				counter,
				&ciph[offset],
				BUF,
				size
				);

			mac.accumulate(
				BUF,
				size
				);
			}
		}

	mac.finalize();

	mac.copyOut(
		outbuf,
		micLength
		);
	}

/*
	Expected output:
	// [authData<authDataLen>][enc(1,payload<payloadLength>)][enc(0,mic<micLength>)]
	[enc(1,payload<payloadLength>)][enc(0,mic<micLength>)]

	I changed the expected output to get rid of the pre-pended authData
	since the client already knows the authData. I do NOT know if this
	will affect the validation suite.

 */
unsigned	Oscl::AES128::Ccm::encrypt(
				const void*	key,
				const void*	nonce,
				const void*	authData,
				unsigned	authDataLen,
				const void*	payload,
				unsigned	payloadLength,
				unsigned	micLength,
				void*		outbuf
				) noexcept {

	uint8_t*	out	= (uint8_t*)outbuf;

	unsigned char mic[16];

//	memcpy(&out[0], authData, authDataLen);

	calculateCbcMAC(
		key,
		nonce,
		authData,
		authDataLen,
		payload,
		payloadLength,
		micLength,
		mic
		);

	uint8_t	nnonce[16];
	nnonce[0]	= 1;	// flags
	memcpy(&nnonce[1],nonce,nonceLength);

	Oscl::AES128::ctrEncrypt(
		key,
		nnonce,
		nonceLength+1,
		1,	// initialCount
		payload,
		out,
		payloadLength
		);

	Oscl::AES128::ctrEncrypt(
		key,
		nnonce,
		nonceLength+1,
		0,	// initialCount
		mic,
		&out[payloadLength],
		micLength
		);

//	return (authDataLen + payloadLength + micLength);
	return (payloadLength + micLength);
	}

/*
	Expected input:
    [enc(1,payload<payloadLength>)][enc(0,mic<micLength>)]

	NOTE! [authData<authDataLen>] is not present!

	[cipherTextAndMic<cipherTextAndMicLen>][enc(0,mic<micLength>]
	[authData<authDataLen>][enc(1,payload<payloadLength>)][enc(0,mic<micLength>)]
	|----------- cipherTextAndMicLen ----------------------------------------|
	                                               |------ mic --------|
 */
unsigned	Oscl::AES128::Ccm::decrypt(
				const void*	key,
				const void*	nonce,
				const void*	authData,
				unsigned	authDataLen,
				const void*	cipherTextAndMic,
				unsigned	cipherTextAndMicLen,
				unsigned	micLength,
				void*		outbuf
				) noexcept{

	const uint8_t*	cmic	= (const uint8_t*)cipherTextAndMic;

	unsigned char mic[16];
	unsigned char calculatedMic[16];

	uint8_t	nnonce[16];
	nnonce[0]	= 1;
	memcpy(&nnonce[1],nonce,nonceLength);

	if(cipherTextAndMicLen < micLength) {
		#ifdef DEBUG_TRACE
		Oscl::Error::Info::log(
			"%s: cipherTextAndMicLen(%u) < micLength(%u))\n",
			__PRETTY_FUNCTION__,
			cipherTextAndMicLen,
			micLength
			);
		#endif
		return 0;
		}

	unsigned
	micOffset	= cipherTextAndMicLen - micLength;

	Oscl::AES128::ctrEncrypt(
		key,
		nnonce,
		nonceLength+1,
		0,	// initialCount
		&cmic[micOffset],
		mic,
		micLength
		);

	unsigned	payloadLen	= micOffset;

	Oscl::AES128::ctrEncrypt(
		key,
		nnonce,
		nonceLength+1,
		1,	// initialCount
		cmic,
		outbuf,
		payloadLen
		);

	calculateCbcMAC(
		key,
		nonce,
		authData,
		authDataLen,
		outbuf,
		payloadLen,
		micLength,
		calculatedMic
		);

	for(unsigned i = 0; i < micLength; i++) {
		if(mic[i] != calculatedMic[i]) {
			#ifdef DEBUG_TRACE
			Oscl::Error::Info::log(
				"%s: (mic[i(%u)](0x%2.2X) != calculatedMic[i(%u)](0x%2.2X))\n",
				__PRETTY_FUNCTION__,
				i,
				mic[i],
				i,
				calculatedMic[i]
				);
			#endif
			return 0;
			}
		}

	return payloadLen;
	}

// RETURN: true for failure
bool	Oscl::AES128::Ccm::verify(
			const void*	key,
			const void*	nonce,
			const void*	authData,
			unsigned	authDataLen,
			const void*	cipherTextAndMic,
			unsigned	cipherTextAndMicLen,
			unsigned	micLength
			) noexcept {
	const uint8_t*	cmic	= (const uint8_t*)cipherTextAndMic;

	unsigned char mic[16];
	unsigned char calculatedMic[16]; /* recomputed MIC */

	uint8_t	nnonce[16];
	nnonce[0]	= 1;	// flags
	memcpy(&nnonce[1],nonce,nonceLength);

	Oscl::AES128::ctrEncrypt(
		key,
		nnonce,
		nonceLength+1,
		0,	// initialCount
		&cmic[cipherTextAndMicLen - micLength],
		mic,
		micLength
		);

	verifyCbcMAC(
		key,
		nonce,
		authData,
		authDataLen,
		cipherTextAndMic,
		cipherTextAndMicLen - micLength,
		micLength,
		calculatedMic
		);

	for(unsigned i = 0; i < micLength; i++) {
		if(mic[i] != calculatedMic[i]) {
			return true;
			}
		}

	return false;
	}
