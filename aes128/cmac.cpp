/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <string.h>
#include "cmac.h"

using namespace Oscl::AES128;

static void	shiftBlockLeftByOne(
				uint8_t*	io
				) noexcept{

	bool	prevMSB	= false;

	for(unsigned i=0;i<16;++i){
		uint8_t	octet	= io[15-i];

		io[15-i]	<<= 1;

		if(prevMSB){
			io[15-i]	|= 0x01;
			}

		prevMSB	= (octet & 0x80)?true:false;
		}
	}

static void	xorBlockWithConstRb(
				uint8_t*	io
				) noexcept{
	static const uint8_t	const_Rb[16]	= {
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x87
		};

	for(unsigned i=0;i<16;++i){
		io[i]	^= const_Rb[i];
		}
	}

void	Oscl::AES128::genSubKeys(
				const void*	k,
				void*		key1,
				void*		key2
				) noexcept{
	const uint8_t*	key	= (const uint8_t*)k;
	uint8_t*		k1	= (uint8_t*)key1;
	uint8_t*		k2	= (uint8_t*)key2;

	Oscl::AES128::Cipher	cipher(key);
	cipher.zero();

	cipher.cipher();
	cipher.copyOut(k1);

	bool	msbOfL	= (k1[0] & 0x80)?true:false;

	shiftBlockLeftByOne(k1);

	if(msbOfL){
		xorBlockWithConstRb(k1);
		}

	memcpy(k2,k1,16);

	shiftBlockLeftByOne(k2);

	bool	msbOfK1	= (k1[0] & 0x80)?true:false;

	if(msbOfK1){
		xorBlockWithConstRb(k2);
		}
	}

CMAC::CMAC(
	const void*	key
	) noexcept:
		_cipher(
			key
			),
		_bufferOffset(0),
		_bufferPending(false)
		{
	_cipher.zero();
	memset(_buffer,0,sizeof(_buffer));

	genSubKeys(
		key,
		_k1,
		_k2
		);
	}

void	CMAC::accumulate(
			const void*	input,
			unsigned	length
			) noexcept{

	if(_bufferPending){
		_cipher.xorBlock(_buffer);
		_cipher.cipher(_buffer);
		_bufferPending	= false;
		}

	if(!length){
		return;
		}

	const uint8_t*	p	= (const uint8_t*)input;

	if(_bufferOffset){
		unsigned	remaining	= 16-_bufferOffset;

		if(remaining <= length){
			memcpy(&_buffer[_bufferOffset],p,remaining);

			_bufferPending	= true;

			p				+= remaining;
			_bufferOffset	= 0;
			length			-= remaining;
			}
		else {
			memcpy(&_buffer[_bufferOffset],p,length);
			_bufferOffset	+= length;
			_bufferPending	= false;
			return;
			}
		}

	if(!length){
		return;
		}

	unsigned	nBlocks	= length/16;
	unsigned	remaining	= length%16;

	unsigned	block;

	if(remaining){
		}
	else {
		--nBlocks;
		}

	for(block = 0; block < nBlocks; ++block){
		_cipher.xorBlock(p,_buffer);
		_cipher.cipher(_buffer);
		p	+=	16;
		}

	if(remaining){
		memcpy(_buffer,p,remaining);
		_bufferOffset	= remaining;
		_bufferPending	= false;
		}
	else {
		memcpy(_buffer,p,16);
		_bufferPending	= true;
		}
	}

void	CMAC::finalize() noexcept{
	if(_bufferOffset || !_bufferPending){
		unsigned	remaining	= 16-_bufferOffset;

		for(unsigned i=0;i<remaining;++i){
			if(!i){
				_buffer[_bufferOffset+i]	= 0x80;
				}
			else {
				_buffer[_bufferOffset+i]	= 0x00;
				}
			}
		for(unsigned i=0;i<16;++i){
			_buffer[i]	^= _k2[i];
			}
		_cipher.xorBlock(_buffer);
		_cipher.cipher(_buffer);
		return;
		}

	for(unsigned i=0;i<16;++i){
		_buffer[i]	^= _k1[i];
		}
	_cipher.xorBlock(_buffer);
	_cipher.cipher(_buffer);

	_bufferOffset	= 0;
	}

void	CMAC::copyOut(
			void*		output,
			unsigned	len,
			unsigned	offset
			) const noexcept{
	_cipher.copyOut(
		output,
		len,
		offset
		);
	}

bool	CMAC::match(
			const void*	mac,
			unsigned	len
			) const noexcept{
	uint8_t	temp[16];
	_cipher.copyOut(temp,len);
	return !memcmp(mac,temp,len);
	}

bool	CMAC::match(const CMAC& other) const noexcept{
	uint8_t	temp1[16];
	uint8_t	temp2[16];
	_cipher.copyOut(temp1);
	other._cipher.copyOut(temp2);
	return !memcmp(temp1,temp2,16);
	}

