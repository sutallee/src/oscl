/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_aes128_cmach_
#define _oscl_aes128_cmach_

#include "cipher.h"

/** */
namespace Oscl {
/** */
namespace AES128 {

/** */
class CMAC {
	public:
		/** */
		Cipher		_cipher;

		/** */
		uint8_t		_buffer[16];

		/** */
		unsigned	_bufferOffset;

		/** */
		uint8_t		_k1[16];

		/** */
		uint8_t		_k2[16];

		/** */
		bool		_bufferPending;

	public:
		/** */
		CMAC(
			const void*	key
			) noexcept;

		/** */
		void	accumulate(const void* input,unsigned length) noexcept;

		/** */
		void	finalize() noexcept;

		/** */
		void	copyOut(
					void*		output,
					unsigned	n	= 16,
					unsigned	offset	= 0
					) const noexcept;

		/** RETURN: true if the mac's match */
		bool	match(
					const void* mac,
					unsigned	len	= 16
					) const noexcept;

		/** RETURN: true if the mac's match */
		bool	match(const CMAC& other) const noexcept;
	};

void	genSubKeys(
			const void*	key,
			void*		k1,
			void*		k2
			) noexcept;
}
}

#endif
