/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <stdint.h>
#include <string.h>
#include "cbc.h"
#include "cipher.h"

using namespace Oscl::AES128;

void Oscl::AES128::cbcEncrypt(
		const void*		iv,
		const void*		key,
		void*			buffer,
		unsigned		nBlocks
		) noexcept {

	uint8_t*			io	= (uint8_t*)buffer;

	Oscl::AES128::Cipher	cipher(key);

	cipher.copyIn(iv);

	for(unsigned i=0;i<nBlocks;++i){

		unsigned	offset = i<<4;

		cipher.xorBlock(&io[offset]);

		cipher.cipher(
			&io[offset]
			);

		cipher.copyOut(&io[offset]);
		}
	}

void Oscl::AES128::cbcDecrypt(
		const void*		iv,
		const void*		key,
		void*			buffer,
		unsigned		nBlocks
		) noexcept {

	uint8_t*			io	= (uint8_t*)buffer;

	Oscl::AES128::Cipher	cipher(key);

	uint8_t	prevBlock[16];

	memcpy(prevBlock,iv,16);

	uint8_t	block[16];

	for(unsigned i=0;i<nBlocks;++i){
		unsigned	offset = i<<4;

		memcpy(block,&io[offset],16);

		cipher.invCipher(block);

		cipher.xorBlock(
			prevBlock,
			&io[offset]
			);

		memcpy(prevBlock,block,16);
		}
	}

