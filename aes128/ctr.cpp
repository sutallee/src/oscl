/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <stdint.h>
#include "ctr.h"
#include "cipher.h"
#include "oscl/encoder/be/base.h"

using namespace Oscl::AES128;

void Oscl::AES128::ctrEncrypt(
		const void*		key,
		const void*		nonce,
		unsigned		nonceLength,
		unsigned		initialCount,
		const void*		input,
		void*			output,
		unsigned		len
		) noexcept {

	const uint8_t*		in	= (uint8_t*)input;
	uint8_t*			out	= (uint8_t*)output;

	uint8_t	block[16];

	Oscl::Encoder::BE::Base	beEncoder(block,16);
	Oscl::Encoder::Api&		encoder	= beEncoder;

	encoder.copyIn(
		nonce,
		nonceLength
		);

	// The save record will allow us
	// to reuse the block for encoding
	// the incremented counter on each
	// block iteration.
	Oscl::Encoder::State	state;
	encoder.push(state);

	const unsigned	remainder	= len%16;

	const unsigned	n		= len/16;

	Oscl::AES128::Cipher	cipher(key);

	uint16_t	count	= initialCount;

	for(unsigned i=0;i<n;++i){
		unsigned	offset = i<<4;

		encoder.encode(count);

		cipher.cipher(
			block
			);

		cipher.xorBlock(
			&in[offset],
			&out[offset]
			);

		encoder.pop();
		encoder.push(state);
		++count;
		}

	if(remainder){
		unsigned	offset = n<<4;

		encoder.encode(count);

		cipher.cipher(
			block
			);

		cipher.xorBlock(
			&in[offset],
			&out[offset],
			remainder
			);
		}
	}

