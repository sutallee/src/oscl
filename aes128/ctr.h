/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_aes128_ctrh_
#define _oscl_aes128_ctrh_

/** */
namespace Oscl {
/** */
namespace AES128 {

/**
	The Counter Mode encryption algorithm
	encrypts the specified buffer in-place.
	@param [in] key	16-octet (128-bit) key array.
	@param [in] nonce	<nonceLength> octet nonce array.
	@param [in] nonceLength	<nonce> length in octets.
	@param [in] initialCount	Initial counter value that is incremented each block.
	@param [in] input	Data array to be encrypted <len> octets.
	@param [out] output	Destination for encrypted <len> octets.
	@param [in] len	Size of <input> and <output> arrays in octets.
 */
void	ctrEncrypt(
			const void*		key,
			const void*		nonce,
			unsigned		nonceLength,
			unsigned		initialCount,
			const void*		input,
			void*			output,
			unsigned		len
			) noexcept;

}
}

#endif
