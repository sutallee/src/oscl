/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <string.h>
#include "cipher.h"

using namespace Oscl::AES128;

Cipher::Cipher(const void* key) noexcept{
	reset(key);
	}

void	Cipher::reset(const void* k) noexcept{
	const uint8_t*	key	= (const uint8_t*)k;
	expand(key);
	}

void	Cipher::zero() noexcept{
	for(unsigned col = 0;col < 4;++col) {
		for(unsigned row = 0;row < 4;++row) {
			_state[row][col]	= 0;
			}
		}
	}

void Cipher::cipher() noexcept {
	addRoundKey(0);

	for(unsigned i = 1;i < Nr;++i) {
		subBytes();
		shiftRows();
		mixColumns();
		addRoundKey(i);
		}

	subBytes();
	shiftRows();
	addRoundKey(Nr);
	}

void Cipher::cipher(
		const void*	in
		) noexcept {

	const uint8_t*	input	= (const uint8_t*)in;

	init(input);
	cipher();
	}

void Cipher::invCipher(
		const void*	in
		) noexcept {

	const uint8_t*	input	= (const uint8_t*)in;

	init(input);

	addRoundKey(Nr);

	for(int i = (Nr-1);i > 0;--i) {
		invShiftRows();
		invSubBytes();
		addRoundKey(i);
		invMixColumns();
		}

	invShiftRows();
	invSubBytes();
	addRoundKey(0);
	}

void	Cipher::xorBlock(
			void*	io
			) noexcept{
	uint8_t*	p	= (uint8_t*)io;

	for(unsigned col = 0; col < 4; col++) {
		for(unsigned row = 0; row < 4; row++, ++p) {
			*p ^= _state[row][col];
			}
		}
	}

void	Cipher::xorBlock(
			const void*	input,
			void*		output,
			unsigned	len
			) noexcept{
	const uint8_t*	in	= (const uint8_t*)input;
	uint8_t*		out	= (uint8_t*)output;

	for(unsigned col = 0; col < 4; col++) {
		for(unsigned row = 0; len && (row < 4); row++,++in,++out,--len) {
			*out = *in ^_state[row][col];
			}
		}
	}

bool	Cipher::matchState(const void*	block) const noexcept{
	const uint8_t*	p	= (const uint8_t*)block;

	uint8_t	col;
	uint8_t	row;

	for(col = 0;col < 4; col++) {
		for(row = 0;row < 4; row++,++p){
			if(*p != _state[row][col]){
				return false;
				}
			}
		}

	return true;
	}

static constexpr uint8_t	sbox[]	= {
	0x63,0x7C,0x77,0x7B,0xF2,0x6B,0x6F,0xC5,0x30,0x01,0x67,0x2B,0xFE,0xD7,0xAB,0x76,
	0xCA,0x82,0xC9,0x7D,0xFA,0x59,0x47,0xF0,0xAD,0xD4,0xA2,0xAF,0x9C,0xA4,0x72,0xC0,
	0xB7,0xFD,0x93,0x26,0x36,0x3F,0xF7,0xCC,0x34,0xA5,0xE5,0xF1,0x71,0xD8,0x31,0x15,
	0x04,0xC7,0x23,0xC3,0x18,0x96,0x05,0x9A,0x07,0x12,0x80,0xE2,0xEB,0x27,0xB2,0x75,
	0x09,0x83,0x2C,0x1A,0x1B,0x6E,0x5A,0xA0,0x52,0x3B,0xD6,0xB3,0x29,0xE3,0x2F,0x84,
	0x53,0xD1,0x00,0xED,0x20,0xFC,0xB1,0x5B,0x6A,0xCB,0xBE,0x39,0x4A,0x4C,0x58,0xCF,
	0xD0,0xEF,0xAA,0xFB,0x43,0x4D,0x33,0x85,0x45,0xF9,0x02,0x7F,0x50,0x3C,0x9F,0xA8,
	0x51,0xA3,0x40,0x8F,0x92,0x9D,0x38,0xF5,0xBC,0xB6,0xDA,0x21,0x10,0xFF,0xF3,0xD2,
	0xCD,0x0C,0x13,0xEC,0x5F,0x97,0x44,0x17,0xC4,0xA7,0x7E,0x3D,0x64,0x5D,0x19,0x73,
	0x60,0x81,0x4F,0xDC,0x22,0x2A,0x90,0x88,0x46,0xEE,0xB8,0x14,0xDE,0x5E,0x0B,0xDB,
	0xE0,0x32,0x3A,0x0A,0x49,0x06,0x24,0x5C,0xC2,0xD3,0xAC,0x62,0x91,0x95,0xE4,0x79,
	0xE7,0xC8,0x37,0x6D,0x8D,0xD5,0x4E,0xA9,0x6C,0x56,0xF4,0xEA,0x65,0x7A,0xAE,0x08,
	0xBA,0x78,0x25,0x2E,0x1C,0xA6,0xB4,0xC6,0xE8,0xDD,0x74,0x1F,0x4B,0xBD,0x8B,0x8A,
	0x70,0x3E,0xB5,0x66,0x48,0x03,0xF6,0x0E,0x61,0x35,0x57,0xB9,0x86,0xC1,0x1D,0x9E,
	0xE1,0xF8,0x98,0x11,0x69,0xD9,0x8E,0x94,0x9B,0x1E,0x87,0xE9,0xCE,0x55,0x28,0xDF,
	0x8C,0xA1,0x89,0x0D,0xBF,0xE6,0x42,0x68,0x41,0x99,0x2D,0x0F,0xB0,0x54,0xBB,0x16
	};

void Cipher::expand(const uint8_t* key) noexcept {

	struct Word {
		uint8_t	b[4];
		};

	Word w[Nb*(Nr+1)];

	for(unsigned i = 0;i < Nk;++i) {
	    w[i].b[0]	= key[(i<<2)];
	    w[i].b[1]	= key[(i<<2)+1];
	    w[i].b[2]	= key[(i<<2)+2];
	    w[i].b[3]	= key[(i<<2)+3];
		}

	for(unsigned i = Nk;i < (Nb * (Nr+1));++i) {
		Word
	    word	= w[i-1];

	    if ((i % Nk) == 0) {
			uint8_t		temp;
	        temp		= word.b[0];
	        word.b[0]	= word.b[1];
	        word.b[1]	= word.b[2];
	        word.b[2]	= word.b[3];
	        word.b[3]	= temp;

	        word.b[0]	= sbox[word.b[0]];
	        word.b[1]	= sbox[word.b[1]];
	        word.b[2]	= sbox[word.b[2]];
	        word.b[3]	= sbox[word.b[3]];

			static constexpr uint8_t
			Rcon[]	= {
				0x01,0x02,0x04,0x08,0x10,0x20,0x40,0x80,0x1B,0x36
				};

	        word.b[0]	^= Rcon[(uint8_t)i/Nk - 1];
	    	}
	    else if ( (Nk > 6) && ((i % Nk) == 4) ) {
	        word.b[0]	= sbox[word.b[0]];
	        word.b[1]	= sbox[word.b[1]];
	        word.b[2]	= sbox[word.b[2]];
	        word.b[3]	= sbox[word.b[3]];
	    	}

	    w[i].b[0]	= w[i-Nk].b[0] ^ word.b[0];
	    w[i].b[1]	= w[i-Nk].b[1] ^ word.b[1];
	    w[i].b[2]	= w[i-Nk].b[2] ^ word.b[2];
	    w[i].b[3]	= w[i-Nk].b[3] ^ word.b[3];
		}

	for(unsigned i = 0;i < (Nb*(Nr + 1));++i) {
		unsigned	k	= i<<2;
	    _expandedKey[k+0]	= w[i].b[0];
	    _expandedKey[k+1]	= w[i].b[1];
	    _expandedKey[k+2]	= w[i].b[2];
	    _expandedKey[k+3]	= w[i].b[3];
		}
	}

void Cipher::init(const uint8_t* src) noexcept {
	for(unsigned col = 0;col < 4;++col) {
		for(unsigned row = 0;row < 4;++row,++src) {
			_state[row][col]	= *src;
			}
		}
	}

void	Cipher::addRoundKey(unsigned round) noexcept {

	for(unsigned i=0;i<4;++i){
		for(unsigned j=0;j<4;++j){
			_state[j][i] ^= _expandedKey[(round*Nb*4)+(i*Nb)+j];
			}
		}
	}

void Cipher::subBytes() noexcept {
	_state[0][0]	= sbox[_state[0][0]];
	_state[0][1]	= sbox[_state[0][1]];
	_state[0][2]	= sbox[_state[0][2]];
	_state[0][3]	= sbox[_state[0][3]];
	_state[1][0]	= sbox[_state[1][0]];
	_state[1][1]	= sbox[_state[1][1]];
	_state[1][2]	= sbox[_state[1][2]];
	_state[1][3]	= sbox[_state[1][3]];
	_state[2][0]	= sbox[_state[2][0]];
	_state[2][1]	= sbox[_state[2][1]];
	_state[2][2]	= sbox[_state[2][2]];
	_state[2][3]	= sbox[_state[2][3]];
	_state[3][0]	= sbox[_state[3][0]];
	_state[3][1]	= sbox[_state[3][1]];
	_state[3][2]	= sbox[_state[3][2]];
	_state[3][3]	= sbox[_state[3][3]];
	}

void	Cipher::shiftRows() noexcept {
	uint8_t	temp;

	temp			= _state[1][0];
	_state[1][0]	= _state[1][1];
	_state[1][1]	= _state[1][2];
	_state[1][2]	= _state[1][3];
	_state[1][3]	= temp;
	temp			= _state[2][0];
	_state[2][0]	= _state[2][2];
	_state[2][2]	= temp;
	temp			= _state[2][1];
	_state[2][1]	= _state[2][3];
	_state[2][3]	= temp;
	temp			= _state[3][3];
	_state[3][3]	= _state[3][2];
	_state[3][2]	= _state[3][1];
	_state[3][1]	= _state[3][0];
	_state[3][0]	= temp;
	}

static inline uint8_t	xtime(uint8_t v) {
  return
			(v <<1 )
		^	( ((v >> 7) & 1) * 0x1b )
		;
	}

void Cipher::mixColumns() noexcept {
	uint8_t temp[4];

	temp[1]	=
			_state[0][0]
		^	_state[1][0];

	temp[3]	=
			_state[2][0]
		^	_state[3][0];

	temp[0]	=
			temp[1]
		^	temp[3];

	temp[2]	=
			_state[2][0]
		^	_state[1][0];

	temp[1]	= xtime(temp[1]);
	temp[2]	= xtime(temp[2]);
	temp[3]	= xtime(temp[3]);

	_state[0][0]	=
			temp[0]
		^	temp[1]
		^	_state[0][0];

	_state[1][0]	=
			temp[0]
		^	temp[2]
		^	_state[1][0];

	_state[2][0]	=
			temp[0]
		^	temp[3]
		^	_state[2][0];

	_state[3][0]	=
			_state[0][0]
		^	_state[1][0]
		^	_state[2][0]
		^	temp[0];

	temp[1]	=
			_state[0][1]
		^	_state[1][1];

	temp[3]	=
			_state[2][1]
		^	_state[3][1];

	temp[0]	=
			temp[1]
		^	temp[3];

	temp[2]	=
			_state[2][1]
		^	_state[1][1];

	temp[1]	= xtime(temp[1]);
	temp[2]	= xtime(temp[2]);
	temp[3]	= xtime(temp[3]);

	_state[0][1]	=
			temp[0]
		^	temp[1]
		^	_state[0][1];

	_state[1][1]	=
			temp[0]
		^	temp[2]
		^	_state[1][1];

	_state[2][1]	=
			temp[0]
		^	temp[3]
		^	_state[2][1];

	_state[3][1]	=
			_state[0][1]
		^	_state[1][1]
		^	_state[2][1]
		^	temp[0];

	temp[1]	=
			_state[0][2]
		^	_state[1][2];

	temp[3]	=
			_state[2][2]
		^	_state[3][2];

	temp[0]	=
			temp[1]
		^	temp[3];

	temp[2]	=
			_state[2][2]
		^	_state[1][2];

	temp[1]	= xtime(temp[1]);
	temp[2]	= xtime(temp[2]);
	temp[3]	= xtime(temp[3]);

	_state[0][2]	=
			temp[0]
		^	temp[1]
		^	_state[0][2];

	_state[1][2]	=
			temp[0]
		^	temp[2]
		^	_state[1][2];

	_state[2][2]	=
			temp[0]
		^	temp[3]
		^	_state[2][2];

	_state[3][2]	=
			_state[0][2]
		^	_state[1][2]
		^	_state[2][2]
		^	temp[0];

	temp[1]	=
			_state[0][3]
		^	_state[1][3];

	temp[3]	=
			_state[2][3]
		^	_state[3][3];

	temp[0]	=
			temp[1]
		^	temp[3];

	temp[2]	=
			_state[2][3]
		^	_state[1][3];

	temp[1]	= xtime(temp[1]);
	temp[2]	= xtime(temp[2]);
	temp[3]	= xtime(temp[3]);

	_state[0][3]	=
			temp[0]
		^	temp[1]
		^	_state[0][3];

	_state[1][3]	=
			temp[0]
		^	temp[2]
		^	_state[1][3];

	_state[2][3]	=
			temp[0]
		^	temp[3]
		^	_state[2][3];

	_state[3][3]	=
			_state[0][3]
		^	_state[1][3]
		^	_state[2][3]
		^	temp[0];
	}

void	Cipher::invShiftRows() noexcept {
	uint8_t	temp;

	temp			= _state[1][3];
	_state[1][3]	= _state[1][2];
	_state[1][2]	= _state[1][1];
	_state[1][1]	= _state[1][0];
	_state[1][0]	= temp;

	temp			= _state[2][2];
	_state[2][2]	= _state[2][0];
	_state[2][0]	= temp;

	temp			= _state[2][3];
	_state[2][3]	= _state[2][1];
	_state[2][1]	= temp;

	temp			= _state[3][0];
	_state[3][0]	= _state[3][1];
	_state[3][1]	= _state[3][2];
	_state[3][2]	= _state[3][3];
	_state[3][3]	= temp;
	}

void	Cipher::invSubBytes() noexcept {
	static constexpr uint8_t	invSbox[]	= {
		0x52,0x09,0x6A,0xD5,0x30,0x36,0xA5,0x38,0xBF,0x40,0xA3,0x9E,0x81,0xF3,0xD7,0xFB,
		0x7C,0xE3,0x39,0x82,0x9B,0x2F,0xFF,0x87,0x34,0x8E,0x43,0x44,0xC4,0xDE,0xE9,0xCB,
		0x54,0x7B,0x94,0x32,0xA6,0xC2,0x23,0x3D,0xEE,0x4C,0x95,0x0B,0x42,0xFA,0xC3,0x4E,
		0x08,0x2E,0xA1,0x66,0x28,0xD9,0x24,0xB2,0x76,0x5B,0xA2,0x49,0x6D,0x8B,0xD1,0x25,
		0x72,0xF8,0xF6,0x64,0x86,0x68,0x98,0x16,0xD4,0xA4,0x5C,0xCC,0x5D,0x65,0xB6,0x92,
		0x6C,0x70,0x48,0x50,0xFD,0xED,0xB9,0xDA,0x5E,0x15,0x46,0x57,0xA7,0x8D,0x9D,0x84,
		0x90,0xD8,0xAB,0x00,0x8C,0xBC,0xD3,0x0A,0xF7,0xE4,0x58,0x05,0xB8,0xB3,0x45,0x06,
		0xD0,0x2C,0x1E,0x8F,0xCA,0x3F,0x0F,0x02,0xC1,0xAF,0xBD,0x03,0x01,0x13,0x8A,0x6B,
		0x3A,0x91,0x11,0x41,0x4F,0x67,0xDC,0xEA,0x97,0xF2,0xCF,0xCE,0xF0,0xB4,0xE6,0x73,
		0x96,0xAC,0x74,0x22,0xE7,0xAD,0x35,0x85,0xE2,0xF9,0x37,0xE8,0x1C,0x75,0xDF,0x6E,
		0x47,0xF1,0x1A,0x71,0x1D,0x29,0xC5,0x89,0x6F,0xB7,0x62,0x0E,0xAA,0x18,0xBE,0x1B,
		0xFC,0x56,0x3E,0x4B,0xC6,0xD2,0x79,0x20,0x9A,0xDB,0xC0,0xFE,0x78,0xCD,0x5A,0xF4,
		0x1F,0xDD,0xA8,0x33,0x88,0x07,0xC7,0x31,0xB1,0x12,0x10,0x59,0x27,0x80,0xEC,0x5F,
		0x60,0x51,0x7F,0xA9,0x19,0xB5,0x4A,0x0D,0x2D,0xE5,0x7A,0x9F,0x93,0xC9,0x9C,0xEF,
		0xA0,0xE0,0x3B,0x4D,0xAE,0x2A,0xF5,0xB0,0xC8,0xEB,0xBB,0x3C,0x83,0x53,0x99,0x61,
		0x17,0x2B,0x04,0x7E,0xBA,0x77,0xD6,0x26,0xE1,0x69,0x14,0x63,0x55,0x21,0x0C,0x7D
		};

	_state[0][0]	= invSbox[_state[0][0]];
	_state[0][1]	= invSbox[_state[0][1]];
	_state[0][2]	= invSbox[_state[0][2]];
	_state[0][3]	= invSbox[_state[0][3]];
	_state[1][0]	= invSbox[_state[1][0]];
	_state[1][1]	= invSbox[_state[1][1]];
	_state[1][2]	= invSbox[_state[1][2]];
	_state[1][3]	= invSbox[_state[1][3]];
	_state[2][0]	= invSbox[_state[2][0]];
	_state[2][1]	= invSbox[_state[2][1]];
	_state[2][2]	= invSbox[_state[2][2]];
	_state[2][3]	= invSbox[_state[2][3]];
	_state[3][0]	= invSbox[_state[3][0]];
	_state[3][1]	= invSbox[_state[3][1]];
	_state[3][2]	= invSbox[_state[3][2]];
	_state[3][3]	= invSbox[_state[3][3]];
	}

static inline uint8_t mult(uint8_t x, uint8_t y) {
  return
			((y & 1) * x)
		^	((y>>1 & 1) * xtime(x))
		^	((y>>2 & 1) * xtime(xtime(x)))
		^	((y>>3 & 1) * xtime(xtime(xtime(x))))
//		^	((y>>4 & 1) * xtime(xtime(xtime(xtime(x)))))
		;
	}

void	Cipher::invMix(unsigned n) noexcept {
	uint8_t	temp[4];

	temp[0]	=
			mult(_state[0][n],0x0E)
		^	mult(_state[1][n],0x0B)
		^	mult(_state[2][n],0x0D)
		^	mult(_state[3][n],0x09);

	temp[1]	=
			mult(_state[0][n],0x09)
		^	mult(_state[1][n],0x0E)
		^	mult(_state[2][n],0x0B)
		^	mult(_state[3][n],0x0D);

	temp[2]	=
			mult(_state[0][n],0x0D)
		^	mult(_state[1][n],0x09)
		^	mult(_state[2][n],0x0E)
		^	mult(_state[3][n],0x0B);

	temp[3]	=
			mult(_state[0][n],0x0B)
		^	mult(_state[1][n],0xD)
		^	mult(_state[2][n],0x09)
		^	mult(_state[3][n],0x0E);

	_state[0][n]	= temp[0];
	_state[1][n]	= temp[1];
	_state[2][n]	= temp[2];
	_state[3][n]	= temp[3];
	}

void	Cipher::invMixColumns() noexcept {
	invMix(0);
	invMix(1);
	invMix(2);
	invMix(3);
	}

void Cipher::copyOut(
		void* out,
		unsigned len,
		unsigned offset
		) const noexcept {

	len	+= offset;

	uint8_t*	p	= (uint8_t*)out;

	for(unsigned col = 0; col < 4;++col) {
		for(unsigned row = 0; len && (row < 4);++row,--len) {
			if(!offset){
				*p	= _state[row][col];
				++p;
				}
			else {
				--offset;
				}
			}
		}
	}

void Cipher::copyIn(const void* in) noexcept {
	const uint8_t*	p	= (const uint8_t*)in;

	for(unsigned col = 0; col < 4;++col) {
		for(unsigned row = 0; row < 4;++row,++p) {
			_state[row][col]	= *p;
			}
		}
	}

void	Oscl::AES128::Cipher::xorBuffers(
			const void*	buff1,
			const void*	buff2,
			void*		output,
			unsigned	length
			) noexcept{
	const uint8_t*	b1	= (const uint8_t*)buff1;
	const uint8_t*	b2	= (const uint8_t*)buff2;
	uint8_t*		out	= (uint8_t*)output;

	for(unsigned i=0;i<length;++i){
		out[i]	= b1[i] ^ b2[i];
		}
	}

