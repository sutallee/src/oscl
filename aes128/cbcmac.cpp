/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <string.h>
#include "cbcmac.h"

using namespace Oscl::AES128;

CbcMAC::CbcMAC(
	const void*	key
	) noexcept:
		_cipher(
			key
			),
		_bufferOffset(0)
		{
	_cipher.zero();
	memset(_buffer,0,sizeof(_buffer));
	}

void	CbcMAC::accumulate(
			const void*	input,
			unsigned	length
			) noexcept{

	if(!length){
		return;
		}

	const uint8_t*	p	= (const uint8_t*)input;

	if(_bufferOffset){
		unsigned	remaining	= 16-_bufferOffset;

		if(remaining <= length){
			memcpy(&_buffer[_bufferOffset],p,remaining);

			_cipher.xorBlock(_buffer);
			_cipher.cipher(_buffer);

			p				+= remaining;
			_bufferOffset	= 0;
			length			-= remaining;
			}
		else {
			memcpy(&_buffer[_bufferOffset],p,length);
			_bufferOffset	+= length;
			return;
			}
		}

	if(!length){
		return;
		}

	unsigned	nBlocks	= length/16;
	unsigned	remaining	= length%16;

	unsigned	block;
	for(block = 0; block < nBlocks; ++block){
		_cipher.xorBlock(p,_buffer);
		_cipher.cipher(_buffer);
		p	+=	16;
		}

	if(remaining){
		memcpy(_buffer,p,remaining);
		_bufferOffset	= remaining;
		}
	}

void	CbcMAC::finalize() noexcept{
	if(!_bufferOffset){
		return;
		}

	memset(&_buffer[_bufferOffset],0,16-_bufferOffset);

	_cipher.xorBlock(_buffer);
	_cipher.cipher(_buffer);

	_bufferOffset	= 0;
	}

void	CbcMAC::copyOut(
			void*		output,
			unsigned	len
			) const noexcept{
	_cipher.copyOut(output,len);
	}

bool	CbcMAC::match(
			const void*	mac,
			unsigned	len
			) const noexcept{
	uint8_t	temp[16];
	_cipher.copyOut(temp);
	return !memcmp(mac,temp,len);
	}

bool	CbcMAC::match(const CbcMAC& other) const noexcept{
	uint8_t	temp1[16];
	uint8_t	temp2[16];
	_cipher.copyOut(temp1);
	other._cipher.copyOut(temp2);
	return !memcmp(temp1,temp2,16);
	}

