/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "sv.h"

using namespace Oscl::Session::Creator::FSM;

void	StateVar::State::
open(	ContextApi&	context,
		StateVar&	sv
		) const noexcept{
	sv.protocolError(stateName());
	}

void	StateVar::State::
close(	ContextApi&	context,
		StateVar&	sv
		) const noexcept{
	sv.protocolError(stateName());
	}

void	StateVar::State::
listenReqDone(	ContextApi&	context,
				StateVar&	sv
				) const noexcept{
	sv.protocolError(stateName());
	}

void	StateVar::State::
sessionCloseNotificationDone(	ContextApi&	context,
								StateVar&	sv
								) const noexcept{
	sv.protocolError(stateName());
	}

void	StateVar::State::
listenCancelDone(	ContextApi&	context,
					StateVar&	sv
					) const noexcept{
	sv.protocolError(stateName());
	}


///////////////////////////////////

void	StateVar::Closed::
open(	ContextApi&	context,
		StateVar&	sv
		) const noexcept{
	context.sendAsyncListenReq();
	context.returnOpenReq();
	sv.changeToListening();
	}

///////////////////////////////////

void	StateVar::Listening::
close(	ContextApi&	context,
		StateVar&	sv
		) const noexcept{
	context.sendAsyncListenCancelReq();
	sv.changeToWaitListenCancel();
	}

void	StateVar::Listening::
listenReqDone(	ContextApi&	context,
				StateVar&	sv
				) const noexcept{
	context.syncCreateSession();
	context.syncOpenSession();
	context.sendAsyncSessionCloseNotificationReq();
	sv.changeToActive();
	}

///////////////////////////////////

void	StateVar::Active::
close(	ContextApi&	context,
		StateVar&	sv
		) const noexcept{
	context.syncCloseConnection();
	sv.changeToClosing();
	}

void	StateVar::Active::
sessionCloseNotificationDone(	ContextApi&	context,
								StateVar&	sv
								) const noexcept{
	context.syncCloseConnection();
	context.syncCloseSession();
	context.syncDestroySession();
	context.sendAsyncListenReq();
	sv.changeToListening();
	}

///////////////////////////////////

void	StateVar::WaitListenCancel::
listenReqDone(	ContextApi&	context,
				StateVar&	sv
				) const noexcept{
	// Do nothing
	}

void	StateVar::WaitListenCancel::
listenCancelDone(	ContextApi&	context,
					StateVar&	sv
					) const noexcept{
	context.syncCloseConnection();
	context.returnCloseReq();
	sv.changeToClosed();
	}

///////////////////////////////////

void	StateVar::Closing::
sessionCloseNotificationDone(	ContextApi&	context,
								StateVar&	sv
								) const noexcept{
	context.syncCloseSession();
	context.syncDestroySession();
	context.returnCloseReq();
	sv.changeToClosed();
	}

