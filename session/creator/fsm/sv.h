/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_session_creator_fsm_svh_
#define _oscl_session_creator_fsm_svh_

/** */
namespace Oscl {
/** */
namespace Session {
/** */
namespace Creator {
/** */
namespace FSM {

/** */
class StateVar {
	public:
		/** */
		class ContextApi {
			public:	// request actions
				/** Shut-up GCC.
				 */
				virtual ~ContextApi() {}

				/** */
				virtual	void	sendAsyncListenReq() noexcept=0;
				/** */
				virtual	void	sendAsyncListenCancelReq() noexcept=0;
				/** */
				virtual	void	syncCreateSession() noexcept=0;
				/** */
				virtual	void	syncOpenSession() noexcept=0;
				/** */
				virtual	void	syncCloseConnection() noexcept=0;
				/** */
				virtual	void	syncCloseSession() noexcept=0;
				/** */
				virtual	void	syncDestroySession() noexcept=0;
				/** */
				virtual	void	sendAsyncSessionCloseNotificationReq() noexcept=0;
				/** */
				virtual	void	returnOpenReq() noexcept=0;
				/** */
				virtual	void	returnCloseReq() noexcept=0;
			};
	public:
		/** */
		class State {
			public:	// Admin Events
				/** Shut-up GCC */
				virtual ~State() {}

				/** */
				virtual void	open(	ContextApi&	context,
										StateVar&	sv
										) const noexcept;
				/** */
				virtual void	close(	ContextApi&	context,
										StateVar&	sv
										) const noexcept;
				/** */
				virtual void	listenReqDone(	ContextApi&	context,
												StateVar&	sv
												) const noexcept;
				/** */
				virtual void	sessionCloseNotificationDone(
											ContextApi&	context,
											StateVar&	sv
											) const noexcept;
				/** */
				virtual void	listenCancelDone(	ContextApi&	context,
													StateVar&	sv
													) const noexcept;
			public:
				/** */
				virtual const char*	stateName() const noexcept=0;
			};

	public:
		/** */
		class Closed : public State {
			private:
				/** */
				const char*	stateName() const noexcept
					{ return "Closed";}
			private:
				/** */
				void	open(	ContextApi&	context,
								StateVar&	sv
								) const noexcept;
			};
		/** */
		class Listening : public State {
			private:
				/** */
				const char*	stateName() const noexcept
					{ return "Listening";}
			public:
				/** */
				void	close(	ContextApi&	context,
								StateVar&	sv
								) const noexcept;
				/** */
				void	listenReqDone(	ContextApi&	context,
										StateVar&	sv
										) const noexcept;
			};
		/** */
		class Active : public State {
			private:
				/** */
				const char*	stateName() const noexcept
					{ return "Active";}
			public:
				/** */
				void	close(	ContextApi&	context,
								StateVar&	sv
								) const noexcept;
				/** */
				void	sessionCloseNotificationDone(	ContextApi&	context,
														StateVar&	sv
														) const noexcept;
			};
		/** */
		class WaitListenCancel : public State {
			private:
				/** */
				const char*	stateName() const noexcept
					{ return "WaitListenCancel";}
			public:
				/** */
				void	listenReqDone(	ContextApi&	context,
										StateVar&	sv
										) const noexcept;
				/** */
				void	listenCancelDone(	ContextApi&	context,
											StateVar&	sv
											) const noexcept;
			};
		/** */
		class Closing : public State {
			private:
				/** */
				const char*	stateName() const noexcept
					{ return "Closing";}
			public:
				/** */
				void	sessionCloseNotificationDone(	ContextApi&	context,
														StateVar&	sv
														) const noexcept;
			};
	private:
		/** */
		ContextApi&		_context;
		/** */
		const State*	_state;

	public:
		/** */
		StateVar(ContextApi& context) noexcept;

	public:	// events
		/** */
		void	open() noexcept;
		/** */
		void	close() noexcept;
		/** */
		void	listenReqDone() noexcept;
		/** */
		void	sessionCloseNotificationDone() noexcept;
		/** */
		void	listenCancelDone() noexcept;

	private: // global actions and state change operations
		/** */
		friend class Closed;
		friend class Listening;
		friend class Active;
		friend class WaitListenCancel;
		friend class Closing;
		/** */
		void	protocolError(const char* state) noexcept;
		/** */
		void	changeToClosed() noexcept;
		/** */
		void	changeToListening() noexcept;
		/** */
		void	changeToActive() noexcept;
		/** */
		void	changeToWaitListenCancel() noexcept;
		/** */
		void	changeToClosing() noexcept;
	};

}
}
}
}

#endif
