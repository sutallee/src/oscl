/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "sv.h"
#include "oscl/error/fatal.h"

using namespace Oscl::Session::Creator::FSM;

static const StateVar::Closed						closed;
static const StateVar::Listening					listening;
static const StateVar::Active						active;
static const StateVar::WaitListenCancel				waitListenCancel;
static const StateVar::Closing						closing;

// StateVar

StateVar::StateVar(ContextApi& context) noexcept:
		_context(context),
		_state(&closed)
		{
	}

void	StateVar::open() noexcept{
	_state->open(_context,*this);
	}

void	StateVar::close() noexcept{
	_state->close(_context,*this);
	}

void	StateVar::listenReqDone() noexcept{
	_state->listenReqDone(_context,*this);
	}

void	StateVar::sessionCloseNotificationDone() noexcept{
	_state->sessionCloseNotificationDone(_context,*this);
	}

void	StateVar::listenCancelDone() noexcept{
	_state->listenCancelDone(_context,*this);
	}

// global actions
void	StateVar::protocolError(const char* state) noexcept{
	ErrorFatal::logAndExit(state);
	}

// state change operations
void	StateVar::changeToClosed() noexcept{
	_state	= &closed;
	}

void	StateVar::changeToListening() noexcept{
	_state	= &listening;
	}

void	StateVar::changeToActive() noexcept{
	_state	= &active;
	}

void	StateVar::changeToWaitListenCancel() noexcept{
	_state	= &waitListenCancel;
	}

void	StateVar::changeToClosing() noexcept{
	_state	= &closing;
	}

