/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "revlabel.h"
#include <stdlib.h>

using namespace Oscl;

RcsRevLabel::RcsRevLabel(const char* label,const char* revision) noexcept:
		Tree::Node<RcsRevLabel,ObjectID::Api>::Node(*this),
		_label(label),
		_rev()
		{
	ObjectID::Api&	rev	= _rev;
	rev	= revision;
	}

RcsRevLabel::~RcsRevLabel(){
	}

const char*			RcsRevLabel::getLabel() noexcept{
	return _label;
	}

const ObjectID::RO::Api&	RcsRevLabel::getKey() const noexcept{
	return _rev;
	}

const Oscl::ObjectID::OidType*   RcsRevLabel::getRevisionArray() noexcept{
	return _rev.getObjectID();
	}

unsigned			RcsRevLabel::getRevisionArraySize() noexcept{
	return _rev.length();
	}

bool	RcsRevLabel::operator < (const RcsRevLabel& cmp) const noexcept{
	return (_rev < cmp._rev);
	}

bool	RcsRevLabel::operator == (const RcsRevLabel& cmp) const noexcept{
	return (_rev == cmp._rev);
	}

