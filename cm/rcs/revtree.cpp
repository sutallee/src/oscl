/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "revtree.h"
#include "oscl/stream/input/line.h"
#include <string.h>
#include "oscl/strings/strip.h"
#include <stdio.h>
#include <stdlib.h>

using namespace Oscl;

RcsRevTree::RcsRevTree(Stream::Input::Api& input,const char* headLabel) noexcept:
		Tree::Root<RcsRevLabel,ObjectID::Api>()
		{
	Stream::Input::Line	lineInput(input);
	char						buffer[2048];
	/* Find start of symbolic names */
	bool	found=false;
	while(lineInput.read(buffer,2048)){
		const char*	line	= buffer;
		if(!strncmp("head",line,strlen("head"))){
			char*			semicolon;
			line	= Oscl::Strings::stripNotSpace(line);
			line	= Oscl::Strings::stripSpace(line);
			if((semicolon=index((char*)line,';'))){
				*semicolon	= '\0';
				}
			RcsRevLabel*	snode	= new RcsRevLabel(headLabel,line);
			insert(snode);
			found	= true;
			break;
			}
		}
	if(!found) exit(1);
	found	= false;
	while(lineInput.read(buffer,2048)){
		char*	line	= buffer;
		if(!strncmp("symbols",line,strlen("symbols"))){
			if(index(line,';')){
				return;
				}
			found	= true;
			break;
			}
		}
	if(!found) exit(1);
	for(bool lastSymbol=false;!lastSymbol && lineInput.read(buffer,2048);){
		char*		line	= buffer;
		char*		semicolon;
		char*		colon;
		char*		end;
		const char*	label;
		char*		rev=0;
		label	= Oscl::Strings::stripSpace(line);
		end		= const_cast<char*>(Oscl::Strings::stripNotSpace(line));
		end[0]	= '\0';
		if((semicolon=index((char*)label,';'))){
			lastSymbol		= true;
			semicolon[0]	= '\0';
			}
		if((colon=index((char*)label,':'))){
			colon[0]	= '\0';
			rev	= &colon[1];
			}
		else{
			fprintf(stderr,"Missing colon ':' in revision label!\n");
			exit(1);
			}
		RcsRevLabel*	snode	= new RcsRevLabel(label,rev);
		insert(snode);
		}
	}

RcsRevTree::~RcsRevTree() noexcept{
	}

