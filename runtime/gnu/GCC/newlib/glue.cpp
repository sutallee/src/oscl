/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <sys/types.h>
#include <sys/stat.h>
#include <_ansi.h>
#include <errno.h>

extern "C" {

int read (int fd, char *buf,size_t nbytes) {
	while(1);
	}

int write (int fd, char *buf, size_t nbytes) {
	while(1);
	}

int open (const char *buf, int flags, int mode) {
	while(1);
	}

int close (int fd) {
	while(1);
	}

int isatty (int fd) {
	while(1);
	}

off_t lseek (int fd,  off_t offset, int whence) {
	while(1);
	}

int fstat (int fd, struct stat *buf) {
	while(1);
	}

void _exit(int val) {
	for(;;);
	}

int kill(int pid, int val) {
	_exit(val);
	}

int getpid() {
	return 1;
	}

/** Start New stuff 2.8.1 **/

void hardware_init_hook(){
	}

void software_init_hook(){
	}

}
