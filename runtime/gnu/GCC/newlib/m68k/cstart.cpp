/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "oscl/hw/motorola/68k/vectors.h"

extern "C" {

int main(int argc,char *argv[]);

void busErrorHandler(void){
	while(1);
	}

void cStart(void)
{

	// Symbols defined by linker magic.
	extern char ___text_data_start;
	extern char ___data_start;
	extern char ___data_end;
	extern char __bss_start;
	extern char __bss_end;

	// Copy initialized data to RAM
	char	*dst	= &___data_start;
	char	*src	= &___text_data_start;

	long	size = (char *)&___data_end-(char *)&___data_start;
	long	i;

	for(i=0;i<size;i++){
		*dst++	= *src++;
		}


	// Zero the BSS
	size	= (char *)&__bss_end-(char *)&__bss_start;
	for(i=0;i<size;i++) *dst++ = 0;

	// Set the default bus error handler.
	M68xxxExceptionVectors*	vtable = 0;
	vtable->byName.busError	= busErrorHandler;

	// Run static constructors/initialization
	// This depends upon linker magic.
	//  o executes static constructors
	//  o executes other ".init" section initializers
	extern void ___init();
	___init();

	// Run main()
	main(0,(char **)0);
}

}
