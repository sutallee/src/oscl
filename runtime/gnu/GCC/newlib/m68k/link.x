/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

/*
 * This script is a generic C++ linker script for embedded targets
 * which require that the initialized data segment be loaded into
 * the text segment for PROMS.
 */

STARTUP(linker/crt0.o)
OUTPUT_FORMAT("coff-m68k")
SEARCH_DIR(/tools/gnu/m68k-coff/lib);

SECTIONS
{
  .text : {
    *(.text)
    *(.gcc_exc)
    *(.eh_fram)
	*(.gnu.linkonce*)
  } > rom

  .cdtors : {
	. = ALIGN(0x4);
     __CTOR_LIST__ = .;
     __CTOR_LIST__ = .;
     LONG((__CTOR_END__ - __CTOR_LIST__) / 4 - 2)
     *(.ctors)
     LONG(0)
     __CTOR_END__ = .;
     __DTOR_LIST__ = .;
     LONG((__DTOR_END__ - __DTOR_LIST__) / 4 - 2)
     *(.dtors)
     LONG(0)
     __DTOR_END__ = .;

/** Start New stuff **/
    *(.rodata)
    *(.gcc_except_table) 

    __INIT_SECTION__ = . ;
    LONG (0x4e560000)	/* linkw %fp,#0 */
    *(.init)
    SHORT (0x4e5e)	/* unlk %fp */
    SHORT (0x4e75)	/* rts */

    __FINI_SECTION__ = . ;
    LONG (0x4e560000)	/* linkw %fp,#0 */
    *(.fini)
    SHORT (0x4e5e)	/* unlk %fp */
    SHORT (0x4e75)	/* rts */
/** End New stuff **/

     etext  =  .;
	} > rom

  .data : AT (ADDR(.cdtors) + SIZEOF(.cdtors)){
	_data	= .;
    *(.data)
     edata  =  .;
  } > ram

  .bss : {
     __bss_start = .;
    *(.bss)
    *(COMMON)
      end = ALIGN(0x8);
      _end = ALIGN(0x8);
     __bss_end = .;
  } > ram

  .stab  0 (NOLOAD) : 
  {

    [ .stab ]
  }

  .stabstr  0 (NOLOAD) :
  {
    [ .stabstr ]
  }
}
