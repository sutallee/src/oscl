/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "encoder.h"

using namespace Oscl::HDLC::LLC::CMD::PPP::TX;

Encoder::Encoder(Oscl::PDU::TX::Api& framer) noexcept:
		_framer(framer)
		{
	}

unsigned	Encoder::open(unsigned length) noexcept{
	static const unsigned char	cmdField[cmdFieldLength] = {0x03};
	const unsigned	totalLen	= cmdFieldLength+length;
	unsigned		mtu;
	mtu	= _framer.open(totalLen);
	if(!mtu) return 0;
	if(mtu != totalLen){
		return (mtu < cmdFieldLength)?0:mtu-cmdFieldLength;
		}

	if(cmdFieldLength != _framer.append(cmdField,cmdFieldLength)){
		return 0;
		}

	return length;
	}

unsigned	Encoder::append(const void* pdu,unsigned length) noexcept{
	return _framer.append(pdu,length);
	}

void		Encoder::close() noexcept{
	_framer.close();
	}

