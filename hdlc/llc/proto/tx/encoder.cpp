/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "encoder.h"

using namespace Oscl::HDLC::LLC::Proto::TX;

Encoder::Encoder(	Oscl::PDU::TX::Api&	transmitter,
					unsigned			protocol
					) noexcept:
		_transmitter(transmitter)
		{
	_protocol[0]	= (unsigned char)(protocol>>8);
	_protocol[1]	= (unsigned char)protocol;
	}

unsigned	Encoder::open(unsigned length) noexcept{
	unsigned	len;
	const unsigned	totalLen=length+protocolHeaderLength;
	len	= _transmitter.open(totalLen);
	if(!len){
		return 0;
		}

	if(len < totalLen){
		if(len < protocolHeaderLength){
			// This should NEVER happen
			return 0;
			}
		return len-protocolHeaderLength;
		}

	len	= _transmitter.append(_protocol,protocolHeaderLength);

	if(len != protocolHeaderLength){
		return 0;
		}

	return len;
	}

unsigned	Encoder::append(const void* pdu,unsigned length) noexcept{
	return _transmitter.append(pdu,length);
	}

void		Encoder::close() noexcept{
	_transmitter.close();
	}

