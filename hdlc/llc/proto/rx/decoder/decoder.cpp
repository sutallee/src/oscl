/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "decoder.h"

using namespace Oscl::HDLC::LLC::Proto::RX;

Decoder::Decoder() noexcept
		{
	}

void	Decoder::transfer(Oscl::Pdu::Pdu* pdu) noexcept{
	unsigned		protocol;
	unsigned char	buffer[protocolFieldLength];

	pdu->read(
		buffer,
		protocolFieldLength,	// max
		0	// offset
		);

	// This is big-endian
	protocol	=	buffer[0];
	protocol	<<=	8;
	protocol	|=	buffer[1];

	Oscl::HDLC::LLC::Proto::RX::Entry*	entry;

	for(	entry	= _protocols.first();
			entry;
			entry	= _protocols.next(entry)
			){
		if(entry->match(protocol)){
			pdu->stripHeader(protocolFieldLength);
			entry->transfer(pdu);
			return;
			}
		}
	}

void	Decoder::attach(	Oscl::HDLC::LLC::
							Proto::RX::Entry&		entry
							) noexcept{
	_protocols.put(&entry);
	}

void	Decoder::detach(	Oscl::HDLC::LLC::
							Proto::RX::Entry&		entry
							) noexcept{
	_protocols.remove(&entry);
	}

