/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_hdlc_llc_proto_rx_decoratorh_
#define _oscl_hdlc_llc_proto_rx_decoratorh_
#include "entry.h"

/** */
namespace Oscl {
/** */
namespace HDLC {
/** */
namespace LLC {
/** */
namespace Proto {
/** */
namespace RX {

/** This decorator style class is used to allow a HDLC LLC
	protocol receiver layer to route/forward raw frames to a handler
	for a specific protocol type.
 */
class Decorator : public Entry {
	private:
		/** */
		Oscl::Pdu::FWD::Api&	_receiver;

		/** */
		unsigned				_protocol;

	public:
		/** */
		Decorator(	Oscl::Pdu::FWD::Api&	receiver,
					unsigned				protocol
					) noexcept;

	public:
		/** This operation is invoked by the lower layer receiver.
			The upper layer client implementation compares the
			ID to its own and returns true if the two match, or
			false otherwise.
		 */
		bool	match(unsigned	protocol) noexcept;

	public:	// Oscl::Pdu::FWD::Api
		/** This operation is to be used to transfer responsibility
			for a PDU between layer entities. The implementation
			*must* take responsibility for the PDU. The implementation
			*must* assign the PDU to a Handle if it is to keep the
			PDU beyond the lifetime of this function call, as the
			caller will release its references to the PDU after
			the transfer invocation, and thus the PDU memory resources
			will be released if its reference count goes to zero.
		 */
		void	transfer(Oscl::Pdu::Pdu* pdu) noexcept;
	};

}
}
}
}
}

#endif
