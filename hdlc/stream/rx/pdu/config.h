/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_hdlc_stream_rx_configh_
#define _oscl_hdlc_stream_rx_configh_
#include "part.h"

/** */
namespace Oscl {
/** */
namespace HDLC {
/** */
namespace Stream {
/** */
namespace RX {
/** */
namespace PDU {

/** */
template <	nComposites,		// 4
			nFragments,			// 4
			pduBufferSize,		// 128
			nStreamReads,		// 4
			streamReadBuffSize	// 32
			>
class Config {
	private:
		/** */
		union StreamBuffMem {
			/** */
			Oscl::Memory::AlignedBlock<bufferSize>	mem;
			};
	private:
		/** */
		ReadTransMem		readMem[nStreamReads];
		/** */
		StreamBuffMem		streamBufferMem[nStreamReads];

	private:
		/** */
		union PduBuffMem {
			/** */
			Oscl::Memory::AlignedBlock< pduBufferSize >	mem;
			};
	private:
		/** */
		CompositeMem	compositeMem[nComposites];
		/** */
		FragmentMem		fragmentMem[nFragments];
		/** */
		FixedMem		fixedMem[nFragments];
		/** */
		PduBuffMem		pduBufferMem[nFragments];

	public:
		/** */
		Part			_part;

	public:
		/** */
		Config(	Oscl::Mt::Itc::PostMsgApi&			myPapi,
				Oscl::Stream::Input::Req::Api::SAP&	streamSAP,
				Oscl::Pdu::FWD::Api&				fwdApi
				) noexcept:
			_part(	myPapi,
					streamSAP,
					fwdApi,
					readMem,
					nStreamReads,
					streamReadBuffSize,
					streamBufferMem,
					compositeMem,
					fragmentMem,
					fixedMem,
					pduBufferMem,
					nComposites,
					nFragments,
					pduBufferSize
					){}
	};

}
}
}
}
}

#endif
