/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <new>
#include "part.h"

using namespace Oscl::HDLC::Stream::RX::PDU;

Part::Part(	Oscl::Mt::Itc::PostMsgApi&			myPapi,
			Oscl::Stream::Input::Req::Api::SAP&	streamSAP,
			Oscl::Pdu::FWD::Api&				fwdApi,
			Oscl::Stream::Framer::
			RX::Srv::ReadTransMem				readMem[],
			void*								streamBufferMem,
			unsigned							nReads,
			unsigned							streamBufferSize,
			Oscl::Frame::Stream::
			RX::PDU::CompositeMem				compositeMem[],
			unsigned							nCompositeMem,
			Oscl::Frame::Stream::
			RX::PDU::FragmentMem				fragmentMem[],
			Oscl::Frame::Stream::
			RX::PDU::FixedMem					fixedMem[],
			unsigned							nFragments,
			void*								pduBufferMem,
			unsigned							pduBufferSize
			) noexcept:
		_myPapi(myPapi),
		_closeSAP(*this,myPapi),
		_pduFramer(	myPapi,
					fwdApi,
					compositeMem,
					nCompositeMem,
					fragmentMem,
					fixedMem,
					nFragments,
					pduBufferMem,
					pduBufferSize
					),
		_fcs(),
		_hdlcFramer(	_pduFramer,
						_fcs,
						*this,	// _errorApi
						0xFFFFFFFF
						),
		_streamFramer(	myPapi,
						_hdlcFramer,
						streamSAP,
						readMem,
						streamBufferMem,
						nReads,
						streamBufferSize
						),
		_nextStep(0),
		_openReq(0),
		_closeReq(0)
		{
	}

Oscl::Mt::Itc::Srv::Close::Req::Api::SAP&	Part::getCloseSAP() noexcept{
	return _closeSAP;
	}

void	Part::open(	Oscl::Mt::Itc::Srv::
					Close::Req::Api::SAP&	sap
					) noexcept{
	Oscl::Mt::Itc::Srv::Open::Req::Api::OpenPayload*
	payload	= new (&_openCloseMem.open.payload)
				Oscl::Mt::Itc::Srv::
				Open::Req::Api::OpenPayload();

	Oscl::Mt::Itc::Srv::Open::Resp::Api::OpenResp*
	resp	= new (&_openCloseMem.open.resp)
				Oscl::Mt::Itc::Srv::
				Open::Resp::Api::OpenResp(	sap.getReqApi(),
											*this,
											_myPapi,
											*payload
											);
	sap.post(resp->getSrvMsg());
	}

void	Part::openStreamFramer() noexcept{
	open(_streamFramer.getCloseSAP());
	_nextStep	= &Oscl::HDLC::Stream::RX::PDU::Part::openPduFramer;
	}

void	Part::openPduFramer() noexcept{
	open(_pduFramer.getCloseSAP());
	_nextStep	= &Oscl::HDLC::Stream::RX::PDU::Part::openComplete;
	}

void	Part::openComplete() noexcept{
	_openReq->returnToSender();
	}

void	Part::close(	Oscl::Mt::Itc::Srv::
						Close::Req::Api::SAP&	sap
						) noexcept{
	Oscl::Mt::Itc::Srv::Close::Req::Api::ClosePayload*
	payload	= new (&_openCloseMem.close.payload)
				Oscl::Mt::Itc::Srv::
				Close::Req::Api::ClosePayload();

	Oscl::Mt::Itc::Srv::Close::Resp::Api::CloseResp*
	resp	= new (&_openCloseMem.close.resp)
				Oscl::Mt::Itc::Srv::
				Close::Resp::Api::CloseResp(	sap.getReqApi(),
												*this,
												_myPapi,
												*payload
												);
	sap.post(resp->getSrvMsg());
	}

void	Part::closeStreamFramer() noexcept{
	close(_streamFramer.getCloseSAP());
	_nextStep	= &Oscl::HDLC::Stream::RX::PDU::Part::closePduFramer;
	}

void	Part::closePduFramer() noexcept{
	close(_pduFramer.getCloseSAP());
	_nextStep	= &Oscl::HDLC::Stream::RX::PDU::Part::closeComplete;
	}

void	Part::closeComplete() noexcept{
	_closeReq->returnToSender();
	}

// FIXME: This is temporary
void	Part::transfer(Oscl::Pdu::Pdu* frame) noexcept{
	}

void	Part::badCRC() noexcept{
	}

void	Part::discardDueToResourceShortage() noexcept{
	}

void	Part::invalidFrameStructure() noexcept{
	}

void	Part::request(	Oscl::Mt::Itc::Srv::
						Open::Req::Api::OpenReq&	msg
						) noexcept{
	_openReq	= &msg;
	openPduFramer();
	}

void	Part::request(	Oscl::Mt::Itc::Srv::
						Close::Req::Api::CloseReq&	msg
						) noexcept{
	_closeReq	= &msg;
	closeStreamFramer();
	}

void	Part::response(	Oscl::Mt::Itc::Srv::
						Open::Resp::Api::OpenResp&	msg
						) noexcept{
	(this->*_nextStep)();
	}

void	Part::response(	Oscl::Mt::Itc::Srv::
						Close::Resp::Api::CloseResp&	msg
						) noexcept{
	(this->*_nextStep)();
	}

