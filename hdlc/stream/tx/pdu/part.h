/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_hdlc_stream_rx_parth_
#define _oscl_hdlc_stream_rx_parth_
#include "oscl/stream/framer/rx/srv/part.h"
#include "oscl/frame/stream/rx/pdu/part.h"
#include "oscl/mt/itc/srv/closeapi.h"
#include "oscl/mt/itc/srv/crespmem.h"
#include "oscl/stream/hdlc/fcs/fcs16.h"
#include "oscl/stream/hdlc/rx/framer.h"

/** */
namespace Oscl {
/** */
namespace HDLC {
/** */
namespace Stream {
/** */
namespace RX {
/** */
namespace PDU {

/** */
class Part :	private Oscl::Mt::Itc::Srv::Close::Req::Api,
				private	Oscl::Mt::Itc::Srv::Close::Resp::Api,
				private Oscl::Pdu::FWD::Api,	// temporary
				private Oscl::Stream::HDLC::RX::ErrorApi	// temporary
			{
	private:
		/** */
		Oscl::Mt::Itc::Srv::
		Close::Resp::Mem					_openCloseMem;
		/** */
		Oscl::Mt::Itc::PostMsgApi&			_myPapi;
		/** */
		Oscl::Mt::Itc::Srv::
		Close::Req::Api::ConcreteSAP		_closeSAP;
		/** */
		Oscl::Frame::Stream::
		RX::PDU::Part							_pduFramer;

		/** */
		Oscl::Stream::HDLC::FCS16::Accumulator	_fcs;
		/** */
		Oscl::Stream::HDLC::RX::Framer			_hdlcFramer;
		/** */
		Oscl::Stream::Framer::
		RX::Srv::Part							_streamFramer;
		/** Function pointer to next sub-server to asynchronously close
		 */
		void			(Part::*	_nextStep)();

		/** */
		Oscl::Mt::Itc::Srv::
		Open::Req::Api::OpenReq*		_openReq;

		/** */
		Oscl::Mt::Itc::Srv::
		Close::Req::Api::CloseReq*		_closeReq;
		
	public:
		/** */
		Part(	Oscl::Mt::Itc::PostMsgApi&			myPapi,
				Oscl::Stream::Input::Req::Api::SAP&	streamSAP,
				Oscl::Pdu::FWD::Api&				fwdApi,
				Oscl::Stream::Framer::
				RX::Srv::ReadTransMem				readMem[],
				void*								streamBufferMem,
				unsigned							nReads,
				unsigned							streamBufferSize,
				Oscl::Frame::Stream::
				RX::PDU::CompositeMem				compositeMem[],
				unsigned							nCompositeMem,
				Oscl::Frame::Stream::
				RX::PDU::FragmentMem				fragmentMem[],
				Oscl::Frame::Stream::
				RX::PDU::FixedMem					fixedMem[],
				unsigned							nFragments,
				void*								pduBufferMem,
				unsigned							pduBufferSize
				) noexcept;

		Oscl::Mt::Itc::Srv::Close::Req::Api::SAP&	getCloseSAP() noexcept;

	private:
		/** */
		void	open(	Oscl::Mt::Itc::Srv::
						Close::Req::Api::SAP&	sap
						) noexcept;
		/** */
		void	openStreamFramer() noexcept;
		/** */
		void	openPduFramer() noexcept;
		/** */
		void	openComplete() noexcept;

		/** */
		void	close(	Oscl::Mt::Itc::Srv::
						Close::Req::Api::SAP&	sap
						) noexcept;
		/** */
		void	closeStreamFramer() noexcept;
		/** */
		void	closePduFramer() noexcept;
		/** */
		void	closeComplete() noexcept;

	private:	// Oscl::Pdu::FWD::Api
		/** */
		void	transfer(Oscl::Pdu::Pdu* frame) noexcept;

	private:	// Oscl::Stream::HDLC::RX::ErrorApi	// temporary
		/** */
		void	badCRC() noexcept;
		/** */
		void	discardDueToResourceShortage() noexcept;
		/** */
		void	invalidFrameStructure() noexcept;

	private:	// Oscl::Mt::Itc::Srv::Close::Req::Api
		/** */
		void	request(	Oscl::Mt::Itc::Srv::
							Open::Req::Api::OpenReq&	msg
							) noexcept;
		/** */
		void	request(	Oscl::Mt::Itc::Srv::
							Close::Req::Api::CloseReq&	msg
							) noexcept;

	private:	// Oscl::Mt::Itc::Srv::Close::Resp::Api
		/** */
		void	response(	Oscl::Mt::Itc::Srv::
							Open::Resp::Api::OpenResp&	msg
							) noexcept;
		/** */
		void	response(	Oscl::Mt::Itc::Srv::
							Close::Resp::Api::CloseResp&	msg
							) noexcept;
	};

}
}
}
}
}

#endif
