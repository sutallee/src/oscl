/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "encoder.h"
#include "oscl/error/fatal.h"
#include "oscl/protocol/iso/std3309.h"

using namespace Oscl::HDLC::MAC::TX;

Encoder::Encoder(	Oscl::PDU::TX::Api&	framer,
					unsigned			address
					) noexcept:
		_framer(framer)
		{
	using namespace Oscl::Protocol::ISO::Std3309;

	// Find first non-zero octet
	unsigned	nOctets=0;
	for(signed i=(sizeof(address)-1);i>0;--i){
		const unsigned char	octet	= (unsigned char)(address>>(i*8));
		if(octet){
			nOctets	= i+1;
			break;
			}
		}

	if(!nOctets) {
		// Allow for address zero
		nOctets	= 1;
		}

	if(nOctets == 1){
		unsigned char	octet	= (unsigned char)address;
		// Special case for all stations (broadcast)
		// and no station (link test) address.
		if(		(octet == Oscl::Protocol::ISO::Std3309::addressAllStations)
			||	(octet == Oscl::Protocol::ISO::Std3309::addressNoStation)
			){
			_address[0]		= octet;
			_addressLength	= 1;
			return;
			}
		}

	if((address & addressE_FieldMask) == addressE_ValueMask_Extended){
		// Extended bit set in LSB is illegal
		Oscl::ErrorFatal::logAndExit(	"Oscl::HDLC::MAC::TX::Encoder:"
										"Extend bit set in LSB of address"
										);
		}

	if(nOctets > maxAddressLength){
		// Huston .. we have a problem
		Oscl::ErrorFatal::logAndExit(	"Oscl::HDLC::MAC::TX::Encoder:"
										"Address too long"
										);
		}

	for(signed i=nOctets-1;i>0;--i){
		unsigned char	octet	= (unsigned char)(address>>(i*8));
		_address[i]		= octet;
		if((octet & addressE_FieldMask)!=addressE_ValueMask_Extended){
			// Last octet must not have extended bit set
			if(i==0){	// If last octet (LSB)
				// OK
				_addressLength	= i+1;
				return;
				}
			else{
				// Evil address
				Oscl::ErrorFatal::logAndExit(	"Oscl::HDLC::MAC::TX::Encoder:"
												"Extend bit must be set in"
												"all octets except LSB"
												);
				}
			}
		address	>>= 8;
		}

	Oscl::ErrorFatal::logAndExit(	"Oscl::HDLC::MAC::TX::Encoder:"
									"Invalid HDLC address"
									);
	}

unsigned	Encoder::open(unsigned length) noexcept{
	const unsigned	totalLen	= _addressLength+length;
	unsigned		mtu;
	mtu	= _framer.open(totalLen);
	if(!mtu) return 0;
	if(mtu != totalLen){
		return (mtu < _addressLength)?0:mtu-_addressLength;
		}

	if(_addressLength != _framer.append(_address,_addressLength)){
		return 0;
		}

	return length;
	}

unsigned	Encoder::append(const void* pdu,unsigned length) noexcept{
	return _framer.append(pdu,length);
	}

void		Encoder::close() noexcept{
	_framer.close();
	}

