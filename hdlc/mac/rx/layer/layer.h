/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_hdlc_mac_rx_layer_layerh_
#define _oscl_hdlc_mac_rx_layer_layerh_
#include "oscl/hdlc/mac/rx/parser/parser.h"
#include "oscl/hdlc/mac/rx/parser/mux.h"
#include "oscl/hdlc/mac/rx/parser/sniffer.h"

/** */
namespace Oscl {
/** */
namespace HDLC {
/** */
namespace MAC {
/** */
namespace RX {

/** */
class Layer : private Oscl::HDLC::MAC::RX::MuxApi {
	private:
		/** */
		Sniffer		_snifferMux;
		/** */
		Mux			_addressMux;
		/** */
		Parser		_parser;

	public:
		/** */
		Layer() noexcept;

		/** */
		Oscl::Pdu::FWD::Api&				getFrameReceiver() noexcept;
		/** */
		Oscl::HDLC::MAC::RX::Mux::ListApi&	getSnifferMux() noexcept;
		/** */
		Oscl::HDLC::MAC::RX::Mux::ListApi&	getAddressMux() noexcept;

	private:	// MuxApi
		/** */
		Oscl::Pdu::Pdu*	forward(	Oscl::Pdu::Pdu*	frame,
									unsigned		macAddress
									) noexcept;
	};

}
}
}
}

#endif
