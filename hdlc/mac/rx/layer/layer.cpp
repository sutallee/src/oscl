/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "oscl/error/fatal.h"
#include "layer.h"

using namespace Oscl::HDLC::MAC::RX;

Layer::Layer() noexcept:
		_snifferMux(),
		_addressMux(),
		_parser(*this)
		{
	}

Oscl::Pdu::FWD::Api&				Layer::getFrameReceiver() noexcept{
	return _parser;
	}

Oscl::HDLC::MAC::RX::Mux::ListApi&	Layer::getSnifferMux() noexcept{
	return _snifferMux;
	}

Oscl::HDLC::MAC::RX::Mux::ListApi&	Layer::getAddressMux() noexcept{
	return _addressMux;
	}

Oscl::Pdu::Pdu*	Layer::forward(	Oscl::Pdu::Pdu*	frame,
								unsigned		macAddress
								) noexcept{
	if(!_snifferMux.forward(frame,macAddress)){
		// This should never happen for a sniffer.
		Oscl::ErrorFatal::logAndExit(	"Oscl::HDLC::MAC::RX::Layer:"
										"Invalid sniffer behavior"
										);
		}

	if(_addressMux.forward(frame,macAddress)){
		// Nobody wanted the frame for this address
		return frame;
		}
	return 0;
	}

