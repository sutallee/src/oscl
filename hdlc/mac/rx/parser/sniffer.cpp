/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "oscl/error/info.h"
#include "sniffer.h"

using namespace Oscl::HDLC::MAC::RX;

Sniffer::Sniffer() noexcept
		{
	}

void	Sniffer::attach(Oscl::HDLC::MAC::RX::MUX::Entry& entry) noexcept{
	_entries.put(&entry);
	}

void	Sniffer::detach(Oscl::HDLC::MAC::RX::MUX::Entry& entry) noexcept{
	_entries.remove(&entry);
	}

Oscl::Pdu::Pdu*	Sniffer::forward(	Oscl::Pdu::Pdu*	frame,
									unsigned		macAddress
									) noexcept{
	Oscl::HDLC::MAC::RX::MUX::Entry*	entry;
	for(	entry	= _entries.first();
			entry;
			entry	= _entries.next(entry)
			){
		if(entry->match(macAddress)){
			entry->transfer(frame);
			}
		}
	return frame;
	}

