/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "oscl/protocol/iso/std3309.h"
#include "parser.h"

using namespace Oscl::HDLC::MAC::RX;

Parser::Parser(MuxApi& mux) noexcept:
		_mux(mux)
		{
	}

void	Parser::transfer(Oscl::Pdu::Pdu* frame) noexcept{
	using namespace Oscl::Protocol::ISO::Std3309;
	unsigned		address;
	unsigned char	octet;
	unsigned		headerLength;
	if(!frame->read(&octet,1,0)){
		// PDU too short
		return;
		}

	address	= octet;

	if(octet == addressAllStations){
		headerLength	= 1;
		}
	else if((octet & addressE_FieldMask)==addressE_ValueMask_Extended){
		// Extension bit is set
		address	<<= 8;
		if(!frame->read(&octet,1,1)){
			// PDU too short
			return;
			}
		address			|= octet;
		headerLength	= 2;
		}
	else{
		headerLength	= 1;
		}

	frame->stripHeader(headerLength);

	_mux.forward(frame,address);
	return;
	}

