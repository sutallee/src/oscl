/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_hdlc_mac_rx_mux_entryh_
#define _oscl_hdlc_mac_rx_mux_entryh_
#include "oscl/pdu/fwd/api.h"
#include "oscl/queue/queueitem.h"

/** */
namespace Oscl {
/** */
namespace HDLC {
/** */
namespace MAC {
/** */
namespace RX {
/** */
namespace MUX {

/** This decorator style class is used to allow a HDLC MAC
	receiver layer to route/forward raw frames to a handler
	for a specific MAC address. The QueueItem provides
	a link to allow the multiplexor to keep track of the
	Entrys instance without memory managment.
	The interface is used as follows:
		The receiver walks its list of Entry records,
		successively invoking the match() function of each.
		If the match() operation returns false, the
		iteration continues with the next Entry record.
		If the match() function returns true, the PDU
		is forwarded to the upper layer by invoking the
		forward operation after the MAC address is stripped
		from the PDU. If the forward operation returns
		null/zero, then the receiver will end the iteration
		will stop and no further Entry records will be given
		the chance to examine the PDU. If the forward operation
		returns a non-zero pointer, then it is assumed that
		the receiver made a copy of the PDU (sniffer) and
		the iteration will continue finding a match() with the
		next Entry record.
		If there are no other Entry records to match(), then
		the free() operation of the frame is invoked
		and the procedure is done.
 */
class Entry : public Oscl::Pdu::FWD::Api, public Oscl::QueueItem {
	private:
		/** */
		Oscl::Pdu::FWD::Api&	_receiver;

		/** */
		unsigned				_address;

	public:
		/** */
		Entry(	Oscl::Pdu::FWD::Api&	receiver,
				unsigned				address
				) noexcept;

	public:
		/** This operation is invoked by the lower layer receiver.
			The upper layer client implementation compares the
			ID to its own and returns true if the two match, or
			false otherwise.
		 */
		bool	match(unsigned	address) noexcept;

	public:	// Oscl::Pdu::FWD::Api
		/** This operation is only invoked if the match() operation
			returns true. The frame that is passed as an argument
			has had its MAC address stripped. The _receiver's forward
			operation (decorated) is invoked. If the receiver
			returns zero/NULL the caller will invoke the free()
			operation of the PDU. Otherwise, the receivr is responsible
			for invoking the free() operation of the PDU when it
			has completed processing the PDU.
		 */
		void	transfer(Oscl::Pdu::Pdu* frame) noexcept;
	};

}
}
}
}
}

#endif
