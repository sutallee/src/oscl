/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_pdu_rx_mux_listapih_
#define _oscl_pdu_rx_mux_listapih_
#include "entry.h"
#include "oscl/queue/queueitem.h"

/** */
namespace Oscl {
/** */
namespace HDLC {
/** */
namespace MAC {
/** */
namespace RX {
/** */
namespace MUX {

/** This abstract class is used to allow a lower layer to route
	PDUs to one of many upper layer entities based on an ID
	carried in the PDU. Examples of the ID include MAC addresses,
	protocol types, destination port numbers and other addressing
	information used to route a packet. The QueueItem provides
	a link to allow the multiplexor to keep track of the
	ListApi instance without memory managment.
	The interface is used as follows:
		The caller walks its list of ListApi entries,
		successively invoking the match() function of each.
		If the match() function returns zero/null, the
		iteration is complete and the procedure is done.
		If the match() function returns non-zero, the
		procedure continues to the next list list.
		If there are no other entries to match(), then
		the free() operation of the frame is invoked
		and the procedure is done.
 */
class ListApi {
	public:
		/** */
		virtual ~ListApi(){}

		/** */
		virtual void	attach(	Oscl::HDLC::MAC::
								RX::MUX::Entry&		entry
								) noexcept=0;
		/** */
		virtual void	detach(	Oscl::HDLC::MAC::
								RX::MUX::Entry&		entry
								) noexcept=0;
	};

}
}
}
}
}

#endif
