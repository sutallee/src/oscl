/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_kernel_interrupth_
#define _oscl_kernel_interrupth_

#include "oscl/kernel/platform/interrupt.h"

extern "C"{
	/** This operation atomically aquires the SMP global system
		lock and disables interrupts. The lock is released by
		passing the returned Oscl::Kernel::IrqMaskState value to
		a corresponding OsclKernelGlobalMpLockRestore() invocation.
	 */
	void	OsclKernelDisableInterrupts(Oscl::Kernel::IrqMaskState& save) noexcept;

	/** This operation releases the global lock aquired through
		a previous invocation of OsclKernelGlobalMpLockAquire().
	 */
	void	OsclKernelRestoreInterruptState(const Oscl::Kernel::IrqMaskState& savedLevel) noexcept;

	}

#endif
