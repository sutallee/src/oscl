/*
   Copyright (C) 2024 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_lvgl_gui_symbiont_apih_
#define _oscl_lvgl_gui_symbiont_apih_

#include <stdint.h>
#include "oscl/queue/queueitem.h"
#include "oscl/lvgl/gui/symbiont/kernel/api.h"

/** */
namespace Oscl {

/** */
namespace Lvgl {

/** */
namespace Gui {

/** */
namespace Symbiont {

/** This interface is used to register a GUI symbiont with
	the GUI sub-system.
	The QueueItem link is reserved for use by the
	GUI sub-system.
 */
class Api : public Oscl::QueueItem {
	public:
		/** This operation is invoked once by the host
			when the symbiont is attached.
		 */
		virtual void	start( Oscl::Lvgl::Gui::Symbiont::Kernel::Api& api ) noexcept = 0;

		/** This operation is invoked once by the host
			when the symbiont is detached. The implementation
			must halt operations and cease using the kernelApi.
		 */
		virtual void	stop() noexcept = 0;

		/** This operation is invoked periodically by the host
			to give time to the symbiont to perform operations
			when the GUI is not blanked (active).
		 */
		virtual void	poll() noexcept = 0;
	};

}
}
}
}

#endif
