/*
   Copyright (C) 2024 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "server.h"
#include "oscl/decoder/linear/be/base.h"
#include <stdio.h>
#include "lvgl/lvgl.h"
#include "oscl/lvgl/tft/init.h"
#include "oscl/lvgl/touch/simple/touch.h"
#include "mnm/second_lv_gl/ui/ui.h"
#include "cgui.h"
#include "littlefs.h"
#include "oscl/littlefs/file/scope.h"

using namespace Oscl::Lvgl::Gui;

static constexpr uint32_t	guiUpdatePeriodInMs = 30;
static constexpr uint32_t	timeTillBlankInMs = 10000;
static constexpr uint32_t	ticksTillBlank = timeTillBlankInMs/guiUpdatePeriodInMs;

Server::Server(
	Oscl::Touch::Single::Observer::Req::Api::SAP&	touchChangeSAP,
	Oscl::Revision::Observer::Req::Api::SAP&		wakeupChangeSAP,
	Oscl::Mt::Itc::Delay::Req::Api::SAP&			delaySAP,
	Oscl::PWM::Percent::Api&						backlight
	) noexcept:
	_touchChangeSAP( touchChangeSAP ),
	_wakeupChangeSAP( wakeupChangeSAP ),
	_delaySAP( delaySAP ),
	_backlight( backlight ),
	_openSync(
		*this,
		*this
		),
	_hostSync(
		*this,
		*this
		),
	_touchChangeComposer(
		*this,
		&Server::touchChangeResp,
		0
		),
	_touchChangePayload(
		false,
		0,
		0
		),
	_touchChangeResp(
		touchChangeSAP.getReqApi(),
		_touchChangeComposer,
		*this,
		_touchChangePayload
		),
	_wakeupChangeComposer(
		*this,
		&Server::wakeupChangeResp,
		0
		),
	_wakeupChangePayload(
		0
		),
	_wakeupChangeResp(
		wakeupChangeSAP.getReqApi(),
		_wakeupChangeComposer,
		*this,
		_wakeupChangePayload
		),
	_guiTickRespComposer(
		*this,
		&Server::guiTimerDelayResp,
		0
		),
	_guiTickDelayPayload(
		guiUpdatePeriodInMs	// milliseconds
		),
	_guiTickDelayResp(
		delaySAP.getReqApi(),	// ReqApi
		_guiTickRespComposer,	// RespApi
		*this,				// PostMsgApi
		_guiTickDelayPayload	// Payload
		),
	_idleCounter( 0 ),
	_timerActive( false ),
	_backlightMax( 100 )
	{
	}

void Server::initialize() noexcept{
	}

void	Server::mboxSignaled() noexcept{
	}

void	Server::request( Oscl::Mt::Itc::Srv::Open::Req::Api::OpenReq& msg ) noexcept {

	_touchChangeSAP.post( _touchChangeResp.getSrvMsg() );

	_wakeupChangeSAP.post( _wakeupChangeResp.getSrvMsg() );

	lv_init();
	Oscl::Lvgl::Tft::initialize();
	Oscl::Lvgl::Touch::Simple::initialize();
	ui_init();

	for(
		auto
		symbiont	= _symbionts.first();
		symbiont;
		symbiont	= _symbionts.next( symbiont )
		) {
		symbiont->start( *this );
		}

	startTimer();

	msg.returnToSender();
	}

Oscl::Mt::Runnable&	Server::getRunnable() noexcept{
	return *this;
	}

Oscl::Mt::Itc::Srv::OpenSyncApi&	Server::getOpenSyncApi() noexcept {
	return _openSync;
	}

Oscl::Lvgl::Gui::Symbiont::Host::Api&	Server::getHostSyncApi() noexcept {
	return _hostSync;
	}

void	Server::setBacklightMax( uint8_t percentage ) noexcept {

	_backlightMax	= percentage;

	_backlight.set( percentage );
	}

void	Server::startTimer() noexcept {

	if( _timerActive ) {
		// Don't start the timer if its running
		return;
		}

	_timerActive	= true;
	_guiTickDelayPayload._timer._milliseconds	= guiUpdatePeriodInMs;
	_delaySAP.post( _guiTickDelayResp.getSrvMsg() );
	}

void	Server::touchChangeResp( Oscl::Touch::Single::Observer::Resp::Api::ChangeResp& msg ) noexcept {

	Oscl::Touch::Single::Observer::Req::Api::ChangePayload&
	payload	= msg.getPayload();

	Oscl::Lvgl::Touch::Simple::update(
		payload._pressed?1:0,
		payload._x,
		payload._y
		);

	startTimer();

	_backlight.set( _backlightMax );

	_idleCounter	= 0;

	_touchChangeSAP.post( _touchChangeResp.getSrvMsg() );
	}

void	Server::wakeupChangeResp( Oscl::Revision::Observer::Resp::Api::ChangeResp& msg ) noexcept {
	lv_state_t
	state	= lv_obj_get_state( ui_Switch1 );

	printf("%s: state: 0x%8.8X\n",__PRETTY_FUNCTION__,state);

	if( state & LV_STATE_CHECKED ) {
		lv_obj_clear_state( ui_Switch1, LV_STATE_CHECKED );
		}
	else {
		lv_obj_add_state( ui_Switch1, LV_STATE_CHECKED );
		}

	uint16_t
	nOptions	= lv_roller_get_option_cnt( ui_Roller1 );

	uint16_t
	selected	= lv_roller_get_selected ( ui_Roller1 );

	++selected;
	selected	%= nOptions;

	lv_roller_set_selected ( ui_Roller1 , selected, LV_ANIM_OFF );

	_wakeupChangeSAP.post( _wakeupChangeResp.getSrvMsg() );
	}

void	Server::guiTimerDelayResp( Oscl::Mt::Itc::Delay::Resp::Api::DelayResp& msg ) noexcept {

	_timerActive	= false;

	++_idleCounter;

	if( _idleCounter < ticksTillBlank ) {
		/*	Go ahead and restart the timer first to reduce jitter.
		 */
		if( _idleCounter > (ticksTillBlank-_backlightMax) ) {
			// Start dimming the screen
			_backlight.set( _backlightMax - (_idleCounter - (ticksTillBlank-_backlightMax)) );
			}
		startTimer();
		}
	else {
		_backlight.set( 0 );
		}

	for(
		auto
		symbiont	= _symbionts.first();
		symbiont;
		symbiont	= _symbionts.next( symbiont )
		) {
		symbiont->poll();
		}

	lv_tick_inc( guiUpdatePeriodInMs );
	lv_task_handler();
	}

void	Server::request(	Oscl::Lvgl::Gui::Symbiont::Host::
							Req::Api::AttachReq& msg
							) noexcept {

	_symbionts.put( &msg.getPayload()._symbiont );

	msg.returnToSender();
	}

void	Server::request(	Oscl::Lvgl::Gui::Symbiont::Host::
							Req::Api::DetachReq& msg
							) noexcept {

	_symbionts.remove( &msg.getPayload()._symbiont );

	msg.returnToSender();
	}

