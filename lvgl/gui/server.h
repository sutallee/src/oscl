/*
   Copyright (C) 2024 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_lvgl_gui_serverh_
#define _oscl_lvgl_gui_serverh_

#include "oscl/mt/itc/mbox/server.h"
#include "oscl/touch/single/observer/respcomp.h"
#include "oscl/revision/observer/respcomp.h"
#include "oscl/mt/itc/delay/respcomp.h"
#include "oscl/pwm/percent/api.h"
#include "oscl/mt/itc/srv/open.h"
#include "oscl/lvgl/gui/symbiont/kernel/api.h"
#include "oscl/lvgl/gui/symbiont/host/sync.h"
#include "oscl/lvgl/gui/symbiont/host/reqapi.h"

/** */
namespace Oscl {

/** */
namespace Lvgl {

/** */
namespace Gui {

/** This server runs with the LVGL GUI.
 */
class Server :
	public Oscl::Mt::Itc::Server,
	private Oscl::Mt::Itc::Srv::Open::Req::Api,
	private Oscl::Lvgl::Gui::Symbiont::Kernel::Api,
	private Oscl::Lvgl::Gui::Symbiont::Host::Req::Api
	{
	private:
		/** */
		Oscl::Touch::Single::Observer::Req::Api::SAP&	_touchChangeSAP;

		/** */
		Oscl::Revision::Observer::Req::Api::SAP&		_wakeupChangeSAP;

		/** */
		Oscl::Mt::Itc::Delay::Req::Api::SAP&			_delaySAP;

		/** */
		Oscl::PWM::Percent::Api&						_backlight;

	private:
		/** */
		Oscl::Mt::Itc::Srv::OpenSync					_openSync;

		/** */
		Oscl::Lvgl::Gui::Symbiont::Host::Sync 			_hostSync;

		/** */
		Oscl::Queue< Oscl::Lvgl::Gui::Symbiont::Api >	_symbionts;

	private:
		/** */
		Oscl::Touch::Single::Observer::Resp::Composer<Server>	_touchChangeComposer;

		/** */
		Oscl::Touch::Single::Observer::Req::Api::ChangePayload	_touchChangePayload;

		/** */
		Oscl::Touch::Single::Observer::Resp::Api::ChangeResp	_touchChangeResp;

	private:
		/** */
		Oscl::Revision::Observer::Resp::Composer<Server>	_wakeupChangeComposer;

		/** */
		Oscl::Revision::Observer::Req::Api::ChangePayload	_wakeupChangePayload;

		/** */
		Oscl::Revision::Observer::Resp::Api::ChangeResp		_wakeupChangeResp;

	private:
		/** */
		Oscl::Mt::Itc::Delay::Resp::Composer<Server>		_guiTickRespComposer;

		/** */
		Oscl::Mt::Itc::Delay::Req::Api::DelayPayload		_guiTickDelayPayload;

		/** */
		Oscl::Mt::Itc::Delay::Resp::Api::DelayResp			_guiTickDelayResp;

	private:
		/** */
		unsigned											_idleCounter;

		/** */
		bool												_timerActive;

		/** */
		uint8_t												_backlightMax;

	public:
		/** */
		Server(
			Oscl::Touch::Single::Observer::Req::Api::SAP&	touchChangeSAP,
			Oscl::Revision::Observer::Req::Api::SAP&		wakeupChangeSAP,
			Oscl::Mt::Itc::Delay::Req::Api::SAP&			delaySAP,
			Oscl::PWM::Percent::Api&						backlight
			) noexcept;

		/** */
		virtual ~Server(){}

		/** */
		Oscl::Mt::Runnable&						getRunnable() noexcept;

		/** */
		Oscl::Mt::Itc::Srv::OpenSyncApi&	getOpenSyncApi() noexcept;

		/** */
		Oscl::Lvgl::Gui::Symbiont::Host::Api&	getHostSyncApi() noexcept;

	private:	// Server
		/** */
		void	initialize() noexcept;

		/** */
		void	mboxSignaled() noexcept;

	private: // Oscl::Mt::Itc::Srv::Open::Req::Api
		/** */
		void	request( Oscl::Mt::Itc::Srv::Open::Req::Api::OpenReq& msg ) noexcept override;

	private:	// Server
		/** */
		void	startTimer() noexcept;

	public: // Oscl::Lvgl::Gui::Symbiont::Kernel::Api
		/** */
		void	setBacklightMax( uint8_t percentage ) noexcept override;

	private: // Oscl::Touch::Single::Observer::Resp::Composer _touchChangeComposer
		/**	This message is used to observe changes in the touch state.
		 */
		void	touchChangeResp( Oscl::Touch::Single::Observer::Resp::Api::ChangeResp& msg) noexcept;

	private: // Oscl::Revision::Observer::Resp::Composer _wakeupChangeComposer
		/**	This message is used to observe changes in the wakeup button state.
		 */
		void	wakeupChangeResp( Oscl::Revision::Observer::Resp::Api::ChangeResp& msg) noexcept;

	private: // Oscl::Mt::Itc::Delay::Resp::Composer
		/** */
		void	guiTimerDelayResp( Oscl::Mt::Itc::Delay::Resp::Api::DelayResp& msg ) noexcept;

	private: // Oscl::Lvgl::Gui::Symbiont::Host::Req::Api
		/** This operation is invoked within the server thread
			whenever a AttachReq is received.
		 */
		void	request(	Oscl::Lvgl::Gui::Symbiont::Host::
							Req::Api::AttachReq& msg
							) noexcept override;

		/** This operation is invoked within the server thread
			whenever a DetachReq is received.
		 */
		void	request(	Oscl::Lvgl::Gui::Symbiont::Host::
							Req::Api::DetachReq& msg
							) noexcept override;

	};

}
}
}

#endif
