/*
   Copyright (C) 2024 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "lvgl/src/hal/lv_hal.h"
#include "touch.h"

// Current touch screen state
static struct {
	uint32_t	nTouch;
	uint32_t	x;
	uint32_t	y;
	} touchState;

void Oscl::Lvgl::Touch::Simple::update(
		uint32_t	nTouch,
		uint32_t	x,
		uint32_t	y
		) {
	touchState.nTouch	= nTouch;

	// NOTE! x & y are swapped
	touchState.x	= y;
	touchState.y	= x;
	}

static void	read(
				lv_indev_drv_t*		indev,
				lv_indev_data_t*	data
				) {

	data->state		= touchState.nTouch?
						LV_INDEV_STATE_PRESSED:
						LV_INDEV_STATE_RELEASED
						;
	data->point.x	= touchState.x;
	data->point.y	= touchState.y;
	}

static lv_indev_drv_t inputDevice;

void Oscl::Lvgl::Touch::Simple::initialize() {

	lv_indev_drv_init( &inputDevice );

	inputDevice.read_cb	= read;
	inputDevice.type	= LV_INDEV_TYPE_POINTER;


	lv_indev_drv_register( &inputDevice );
	}

