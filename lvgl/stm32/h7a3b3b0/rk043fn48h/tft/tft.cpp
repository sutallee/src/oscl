/*
   Copyright (C) 2024 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "lv_conf.h"
#include "lvgl/lvgl.h"
#include <string.h>
#include <stdlib.h>

#include "oscl/lvgl/tft/init.h"
#include <stdint.h>
#include "lvgl/src/misc/lv_color.h"
#include "lvgl/src/misc/lv_area.h"
#include "oscl/hw/st/stm32/h7a3b3b0/map.h"
#include "oscl/driver/arm/cortex/m7/cache.h"
#include "oscl/hw/rocktech/rk043fn48h.h"

using namespace Oscl::Rocktech::RK043FN48H;

static lv_disp_drv_t disp_drv;

typedef uint16_t uintpixel_t;

static lv_disp_t *our_disp = NULL;

static constexpr void*	layer0FrameBuffer = STM32H7A3B3B0_AXISRAM1_BASE;

/* Flush the content of the internal buffer the specific area on the display
 * You can use DMA or any hardware acceleration to do this operation in the background but
 * 'lv_flush_ready()' has to be called when finished
 * This function is required only when LV_VDB_SIZE != 0 in lv_conf.h
 */
static void ex_disp_flush(
				lv_disp_drv_t*		drv,
				const lv_area_t*	area,
				lv_color_t*			color_p
				) {
	/*	In the lvgl/src/core/lv_refr.c , it is clear that this
		can be called with an area that is smaller than the
		entire screen. (see call_flush_cb() )
	 */
	/* FIXME: This could be done by DMA2D, no?
		If so, we would need to clean the cache
		first for each line.
	 */
	uint16_t*
	fb		= (uint16_t *) layer0FrameBuffer;

	uint16_t
	stride	= disp_drv.hor_res;

	fb	+= area->y1 * stride;
	fb	+= area->x1;

	lv_coord_t w	= lv_area_get_width(area);

	for(int32_t y = area->y1; y <= area->y2; y++) {

		lv_memcpy(
			fb,
			color_p,
			w * 2
			);

		Oscl::Arm::Cortex::M7::cleanDataCacheAddressRange(
			fb,		// const void* address
			w * 2	// unsigned long length
			);

		fb		+= stride;
		color_p	+= w;
		}

	lv_disp_flush_ready(&disp_drv);
	}

/*	This is invoked when the lv_disp_clean_dcache() is invoked,
	which is NOWHERE.

	HOWEVER, lvgl/src/draw/stm32_dma2d/lv_gpu_stm32_dma2d.c, invokes
	the clean_dcache_cb callback directly.
	We are not CURRENTLY using the DMA2D.
 */
static void ex_disp_clean_dcache(lv_disp_drv_t *drv) {
	Oscl::Arm::Cortex::M7::cleanAndInvalidateEntireDataCache();
	}

void Oscl::Lvgl::Tft::initialize() {
	/* There is only one display on STM32 */
	if(our_disp != NULL)
		abort();

	// BSP initialization is static and
	// done elsewhere. (Before we get here)

	static lv_disp_draw_buf_t display;

	static lv_color_t buf[WidthInPixels * 68];
	lv_disp_draw_buf_init(
		&display,
		buf,
		NULL,
		WidthInPixels * 68
		);

	/*-----------------------------------
	* Register the display in LittlevGL
	*----------------------------------*/
	/*	This initialization performs some "special" initialization of
		disp_drv structure for draw_ctx_init, draw_ctx_deinit, and draw_ctx_size
		when e.g., LV_USE_GPU_STM32_DMA2D is defined.
		This includes setup for things like DMA2D operations.
		See: lvgl/src/draw/stm32_dma2d/lv_gpu_stm32_dma2d.c
	 */
	lv_disp_drv_init(&disp_drv);

	/*Set up the functions to access to your display*/

	/*	Set the resolution of the display.
		FIXME: usa a platform-specific call to get the resolution.
	 */
	disp_drv.hor_res = WidthInPixels;
	disp_drv.ver_res = HeightInPixels;

	/*Used to copy the buffer's content to the display*/

	/*	Write the internal buffer (draw_buf) to the display.
		'lv_disp_flush_ready()' has to be called when finished
	 */
	disp_drv.flush_cb = ex_disp_flush;

	/* Called when lvgl needs any CPU cache that affects rendering to be cleaned*/
	disp_drv.clean_dcache_cb	= ex_disp_clean_dcache;
	disp_drv.full_refresh		= false;

	/*Set a display buffer*/
	disp_drv.draw_buf = &display;

	/*Finally register the driver*/
	our_disp = lv_disp_drv_register(&disp_drv);
	}
