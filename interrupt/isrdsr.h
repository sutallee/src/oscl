/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_interrupt_isrdsrh_
#define _oscl_interrupt_isrdsrh_

/** */
namespace Oscl {
/** */
namespace Interrupt {

/** This interface separates the two essential parts
	of an interrupt handler that executes on a Real-Time
	kernel. Under some kernels (e.g. eCos) the Interrupt Service
	Routine runs with interrupts disabled and no kernel
	system calls are allowed at this stage. The Deferred Service
	Routine is designed to be run with interrupts enabled
	and is where waiting threads can be signaled. The idea
	is that interrupt latency is reduced if the ISR portion
	is extremely short.
 */
class IsrDsrApi {
	public:
		/** Shut-up GCC. */
		virtual ~IsrDsrApi() {}

		/** This operation is executed with interrupts disabled.
			No kernel operations should be invoked during its
			execution. Generally, if an interrupt is pending for
			the device, the interrupt should be masked and acknowledged.
			This interrupt service routine handler returns true if the
			handler detected and serviced a pending interrupt.
		 */
		virtual bool	isr() noexcept=0;
		/** This operation is executed after the isr() has completed
			and returned true. This operation is where kernel calls
			(e.g. signaling a semaphore) are performed to notify
			driver tasks of the interupt. Blocking kernel calls
			must not be issued in the dsr().
		 */
		virtual void	dsr() noexcept=0;
	};

}
}

#endif
