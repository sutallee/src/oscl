/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "oscl/mt/mutex/iss.h"
#include "source.h"

using namespace Oscl::Interrupt;

Oscl::Interrupt::Shared::Source::
Source(ListStateMonitorApi& listStateMonitor) noexcept:
		_listStateMonitor(listStateMonitor)
		{
	}

void	Oscl::Interrupt::Shared::Source::attach(Handler& handler) noexcept{
   Oscl::Mt::IntScopeSync  iss;
	bool	listEmpty	= !_handlerList.first();
	_handlerList.put(&handler);
	if(listEmpty){
		_listStateMonitor.listIsNowNotEmpty();
		}
	}

void	Oscl::Interrupt::Shared::Source::detach(Handler& handler) noexcept{
   Oscl::Mt::IntScopeSync  iss;
	_handlerList.remove(&handler);
	if(!_handlerList.first()){
		_listStateMonitor.listIsNowEmpty();
		}
	}

bool	Oscl::Interrupt::Shared::Source::interrupt() noexcept{
	Shared::Handler*	next;
	bool				handled = false;
	for(next=_handlerList.first();next;next=_handlerList.next(next)){
		handled	|= next->interrupt();
		}
	return handled;
	}

