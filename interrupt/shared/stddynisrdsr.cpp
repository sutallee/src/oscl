/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <new>
#include "stddynisrdsr.h"

using namespace Oscl::Interrupt::Shared;

StandardDynIsrDsr::StandardDynIsrDsr() noexcept:
		_adaptor(0),
		_handler(0),
		_source(0)
		{
	}

void	StandardDynIsrDsr::attach(	Oscl::Interrupt::IsrDsrApi&			isrdsr,
									Oscl::Interrupt::Shared::SourceApi&	source
									) noexcept{
	if(_source){
		// FIXME: already in use
		while(true);
		}
	_source		= &source;
	_adaptor	= new(&_adaptorMem)
					Oscl::Interrupt::IsrDsrInterrupt(isrdsr);
	_handler	= new(&_handlerMem)
					Oscl::Interrupt::Shared::Handler(*_adaptor);
	source.attach(*_handler);
	}

void	StandardDynIsrDsr::detach() noexcept{
	if(!_source){
		// FIXME: nothing attached
		while(true);
		}
	_source->detach(*_handler);
	_handler->~Handler();
	_adaptor->~IsrDsrInterrupt();
	_handler	= 0;
	_adaptor	= 0;
	_source		= 0;
	}

