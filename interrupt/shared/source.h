/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_interrupt_shared_sourceh_
#define _oscl_interrupt_shared_sourceh_
#include "oscl/queue/queue.h"
#include "oscl/interrupt/shandler.h"
#include "handler.h"
#include "api.h"

/** */
namespace Oscl{
/** */
namespace Interrupt {
/** */
namespace Shared {

/** */
class ListStateMonitorApi {
	public:
		/** Shut-up GCC. */
		virtual ~ListStateMonitorApi() {}
		/** */
		virtual void	listIsNowEmpty() noexcept=0;
		/** */
		virtual void	listIsNowNotEmpty() noexcept=0;
	};

/** */
class Source : public SourceApi, public Oscl::Interrupt::StatusHandlerApi {
	private:
		/** */
		Oscl::Queue<Handler>	_handlerList;
		/** */
		ListStateMonitorApi&	_listStateMonitor;

	private:
		explicit Source(const Oscl::Interrupt::Shared::Source&);

	public:
		/** */
		Source(ListStateMonitorApi& listStateMonitor) noexcept;
		/** */
		void	attach(Handler& handler) noexcept;
		/** */
		void	detach(Handler& handler) noexcept;
		/** */
		bool	interrupt() noexcept;
		/** */
		bool	listIsNotEmpty() noexcept;
	};

};
};
};

#endif
