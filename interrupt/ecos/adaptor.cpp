/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "adaptor.h"
#include "cyg/kernel/kapi.h"

using namespace Oscl::Interrupt::eCos;

static	void	c_dsr(	cyg_vector_t	vector,
						cyg_ucount32	count,
						cyg_addrword_t	data
						){
	((Adaptor*)data)->theDsr();
	}

Adaptor::Adaptor(Oscl::Interrupt::IsrDsrApi& isrdsr) noexcept:
		_interrupt(	(CYG_ADDRWORD)this,
					c_dsr
					),
		_isrdsr(&isrdsr)
		{
	}

Adaptor::Adaptor() noexcept:
		_interrupt(	(CYG_ADDRWORD)this,
					c_dsr
					),
		_isrdsr(this)
		{
	}

bool	Adaptor::interrupt() noexcept{
	if(_isrdsr->isr()){
		_interrupt.post_dsr();
		return true;
		}
	return false;
	}

void	Adaptor::theDsr() noexcept{
	_isrdsr->dsr();
	}

void	Adaptor::attach(Oscl::Interrupt::IsrDsrApi& isrdsr) noexcept{
	_isrdsr	= &isrdsr;
	}

void	Adaptor::detach() noexcept{
	_isrdsr	= this;
	}

bool	Adaptor::isr() noexcept{
	// FIXME:
	// This means that the interrupt happend
	// without attaching the device!
	while(true);
	}

void	Adaptor::dsr() noexcept{
	// FIXME:
	// This means that the interrupt happend
	// without attaching the device!
	while(true);
	}

