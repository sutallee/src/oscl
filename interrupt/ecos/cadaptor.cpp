/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "cadaptor.h"

using namespace Oscl::Interrupt::eCos;

CAdaptor::CAdaptor(	cyg_addrword_t	thisData,
					cyg_ISR_t*		isr,
					cyg_DSR_t*		dsr
					) noexcept:
		_interrupt(	(CYG_ADDRWORD)this,
					dsr
					),
		_isr(isr),
		_thisData(thisData)
		{
	}

bool	CAdaptor::interrupt() noexcept{
	if((_isr)(0,_thisData)){
		_interrupt.post_dsr();
		return true;
		}
	return false;
	}
