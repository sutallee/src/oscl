/*
   Copyright (C) 2020 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_change_operationh_
#define _oscl_change_operationh_
#include "api.h"

/** */
namespace Oscl {
/** */
namespace Change {

/** */
class Operation : public Api {
	public:
		/** */
		typedef void	(Context::* ChangedOp)();

	private:
		/** */
		Context&		_context;
		/** */
		ChangedOp		_changedOperation;

	public:
		/** */
		Operation(	Context&	context,
					ChangedOp	changedOperation
					) noexcept;
	private:
		/** This operation is called to notify the implementation
			that the task has been completed.
		 */
		void	changed() noexcept;
	};

template <class Context>
Operation<Context>::Operation(	Context&	context,
								ChangedOp	changedOperation
								) noexcept:
		_context(context),
		_changedOperation(changedOperation)
		{
	}

template <class Context>
void Operation<Context>::changed() noexcept{
	(_context.*_changedOperation)();
	}

}
}

#endif
