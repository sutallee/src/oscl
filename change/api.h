/*
   Copyright (C) 2020 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_change_apih_
#define _oscl_change_apih_

/** */
namespace Oscl {
/** */
namespace Change {

/** This interface may be used as a means for a lower
	layer to notify an upper layer that it has asynchronously
	changed and that the context needs to take action.
	The upper layer entity implements this interface,
	and the lower layer entity invokes its operation
	when the subject has changed.

	NOTE: It is expected that the implementation
	will use some other means to query the subject
	and determine what actually changed.
 */
class Api {
	public:
		/** Shut-up GCC.
		 */
		virtual ~Api() {}

		/** This operation is called to notify the implementation
			that the state of the subject has changed.
		 */
		virtual void	changed() noexcept=0;
	};

}
}

#endif
