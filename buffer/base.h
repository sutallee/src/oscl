/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_buffer_baseh_
#define _oscl_buffer_baseh_
#include "api.h"

/** */
namespace Oscl {
/** */
namespace Buffer {

/** This abstract class represents the standard interface for all
	buffers.
 */
class Base : public ReadWrite {
	protected:
		/** */
		unsigned	_offset;
		/** */
		unsigned	_length;

	public:
		/** */
		Base() noexcept;

		/** Virtual destructors are just a good idea.
		 */
		virtual ~Base();

		/** This operation adjusts the buffer such that "n"
			bytes are reserved at the beginning of the buffer.
			This operation is only valid while the buffer is
			empty. If "n" is greater than the maximum buffer
			length then "n" is reduced to the remaining()
			space in the buffer and the new value of "n"
			is returned. If the buffer is NOT empty, zero
			is returned. Otherwise, "n" is returned.
		 */
		unsigned	reserveHeader(unsigned n) noexcept;
		/** */
		unsigned	stripHeader(unsigned n) noexcept;
		/** */
		unsigned	stripTrailer(unsigned n) noexcept;
		/** */
		unsigned	advance(unsigned n) noexcept;
		/** */
		const unsigned char*	startOfData() const noexcept;

	public:
		/** This interface returns the maximum length of the buffer.
		 */
		virtual unsigned	bufferSize() const noexcept=0;

		/** This interface returns a read-only pointer to the actual
			buffer. It may return zero if no buffer is currently
			allocated.
		 */
		virtual const void*		getBuffer() const noexcept=0;

	public:
		/** This operation sets the number of valid bytes to zero.
		 */
		void	reset() noexcept;

		/** This operation returns the number of bytes remaining
			in the buffer excluding the number of valid bytes.
		 */
		unsigned	remaining() const noexcept;

	public:
		/** */
		unsigned	prepend(	const void*	source,
								unsigned	length
								) noexcept;

		/** */
		unsigned	prepend(	const ReadOnly&	source,
								unsigned		length
								) noexcept;

		/** */
		unsigned	prepend(const ReadOnly& source) noexcept;

	public: // ReadOnly
		/** This operation returns the amount of valid data contained
			in the buffer. The assumption is that the buffer is filled
			contiguously from its beginning.
		 */
		unsigned	length() const noexcept;

		/** This operation adjusts the buffer such that its
			contents are no longer accessible. Subsequent
			append and copyIn operations reuse the empty buffer
			space.
			The behaviour of this operation may vary significantly
			with implementation. A simple contiguous buffer
			implementation may simply reset offset and length
			indicies. A scatter/gather buffer chain may release
			all buffers.
		 */
		void	empty() noexcept;

	public: //------------ Copy OUT Operations ---------- //
		/** This operation copies up to "length" valid bytes from this buffer
			into the destination buffer. The number of bytes actually copied
			to the destination buffer is returned as a result.
		 */
		unsigned	copyOut(	void*		destination,
								unsigned	length
								) const noexcept;

		/** */
		/** This operation copies up to "length" valid bytes from this buffer
			at the specified "sourceOffset" from the beginning of the buffer
			into the destination buffer. The number of bytes actually copied
			to the destination buffer is returned as a result.
		 */
		unsigned	copyOut(	void*		destination,
								unsigned	length,
								unsigned	sourceOffset
								) const noexcept;

		/** This operation copies the valid bytes in this buffer into the
			destination buffer. The actual number copied is returned as a
			result. The actual number copied to the destination may be less
			than the number of valid bytes in this buffer if the destination
			buffer is not large enought to hold all of the valid bytes.
		 */
		unsigned	copyOut(ReadWrite& destination) const noexcept;

		/** This operation returns the byte at the specified offset.
			If the offset is out of buffer range, the return value
			is undefined. It is the responsiblity of the caller to
			use the length() operation to find out if the offset is
			valid.
		 */
		unsigned char	copyOut(unsigned offset) const noexcept;

		/** Returns true if the "data" byte has the same value as the
			byte in the buffer at the specified "offset".
		 */
		bool	isEqual(	unsigned char	data,
							unsigned		offset
							) const noexcept;

		/** Returns true if the "length" bytes of source are exactly equal
			to the "length" bytes of this buffer starting from the "offset"
			of this buffer.
		 */
		bool	areEqual(	const void*		source,
							unsigned		length,
							unsigned		offset
							) const noexcept;

		bool	areEqual(	const ReadOnly&		source,
							unsigned			offset
							) const noexcept;
	public:	// ReadWrite
		/** This operation copies count bytes of data from the source into
			the buffer starting at the beginning of the buffer. The
			number of bytes actually copied to the buffer is returned
			as a result. The number of bytes actually copied may be
			less than the requested count bytes if this buffer is not large
			enough to hold all the source data.
			This operation replaces any existing data in the buffer.

			The behaviour of this operation may vary significantly
			with implementation. A simple contiguous buffer
			implementation may simply reset an offset index;
			memcpy() the source into the buffer; and record the
			new length value. A scatter/gather buffer chain may
			allocate and/or release buffers.
		 */
		unsigned	copyIn(	const void*		source,
							unsigned	count
							) noexcept;

		/** This operation copies the valid bytes in the source buffer into
			this buffer. The number of bytes actually copied to the buffer
			is returned as a result. The number of bytes actually copied may be
			less than the number of valid bytes in the source buffer if this
			buffer is not large enough to hold all of the source data.
			This operation replaces any existing data in the buffer.

			The behaviour of this operation may vary significantly
			with implementation. A simple contiguous buffer
			implementation may simply reset an offset index;
			memcpy() the source into the buffer; and record the
			new length value. A scatter/gather buffer chain may
			allocate and/or release buffers.
		 */
		unsigned	copyIn(const ReadOnly& source) noexcept;

	public: //------------ Append Operations ---------- //

		/** This operation appends "length" bytes of data from the "source" into
			the buffer. The number of bytes actually copied to the buffer
			is returned as a result. The number of bytes actually copied may be
			less than the requested "length" bytes if this buffer is not large
			enough to hold all the source data.
		 */
		unsigned	append(	const void*		source,
							unsigned	length
							) noexcept;

		/** This operation appends "length" bytes of data from the "source" into
			the buffer. The number of bytes actually copied to the buffer
			is returned as a result. The number of bytes actually copied may be
			less than the requested "length" bytes if this buffer is not large
			enough to hold all the source data or the source does not contain
			the "length" bytes of data.
		 */
		unsigned	append(	const ReadOnly&	source,
							unsigned		length
							) noexcept;

		/** This operation copies all bytes of data from the "source" into
			the buffer. The number of bytes actually copied to the buffer
			is returned as a result. The number of bytes actually copied may be
			less than the number of bytes in the "source" if the buffer is not
			large enough to hold all the source data.
		 */
		unsigned	append(const ReadOnly& source) noexcept;

#if 0
	public:	// ReadWrite
		/** This operation copies count bytes of data from the source into
			the buffer starting at the beginning of the buffer. The
			number of bytes actually copied to the buffer is returned
			as a result. The number of bytes actually copied may be
			less than the requested count bytes if this buffer is not large
			enough to hold all the source data.
		 */
		unsigned	copyIn(const void* source,unsigned count) noexcept;

		/** This operation copies the valid bytes in the source buffer into
			this buffer. The number of bytes actually copied to the buffer
			is returned as a result. The number of bytes actually copied may be
			less than the number of valid bytes in the source buffer if this
			buffer is not large enough to hold all of the source data.
		 */
		unsigned	copyIn(const Base& source) noexcept;

		/** This operation appends count bytes of data from the source into
			the buffer. The number of bytes actually copied to the buffer
			is returned as a result. The number of bytes actually copied may be
			less than the requested count bytes if this buffer is not large
			enough to hold all the source data.
		 */
		unsigned	append(const void* source,unsigned count) noexcept;
#endif


		/** This operation copies the null terminated string source into
			this buffer. The actual number copied is limited to the size
			of this buffer.
		 */
		void operator =(const char* source) noexcept;

		/** This operation copies the character to the beginning of
			this buffer.
		 */
		void operator =(char c) noexcept;

		/** This operation copies the character to the beginning of
			this buffer.
		 */
		void operator =(unsigned char c) noexcept;

		/** This operation copies the valid bytes from the source buffer into
			this buffer. The actual number copied is limited to the size
			of this buffer.
		 */
		void operator =(const Base& source) noexcept;

		/** This operation appends the valid bytes from the source buffer
			to the valid bytes in the destination buffer. The actual number
			copied is limited by the number of valid bytes already in this
			buffer and the size of this buffer.
		 */
		void operator +=(const Base& source) noexcept;

		/** This operation appends the char to the valid bytes in
			the destination buffer. The actual number copied is limited by
			the number of valid bytes already in this buffer.
		 */
		void operator +=(char c) noexcept;

		/** This operation appends the unsigned char to the valid bytes in
			the destination buffer. The actual number copied is limited by
			the number of valid bytes already in this buffer.
		 */
		void operator +=(unsigned char c) noexcept;

		/** This operation returns true if the valid bytes in this buffer
			have the same value as the valid bytes in the other buffer.
		 */
		bool operator ==(const Base& other) const noexcept;

		/** This operation returns true if any of the valid bytes in this
			buffer are different from the valid bytes in the other buffer.
		 */
		bool operator !=(const Base& other) const noexcept;

	protected:
		/** Returns a pointer to the buffer memory. May return
			zero if no buffer is currently allocated. This operation
			must be implemented by sub-classes and is used to implement
			the standard buffer operations.
		 */
		virtual void*		buffer() noexcept=0;
	};

}
}

#endif
