/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "base.h"
#include <string.h>

using namespace Oscl::Buffer;

Base::Base() noexcept:
		_offset(0),
		_length(0)
		{
	}

Base::~Base(){
	}

unsigned	Base::reserveHeader(unsigned n) noexcept{
	if(!_length){
		unsigned	remaining	= this->remaining();
		if(n > remaining){
			n	= remaining;
			}
		_offset	+= n;
		return n;
		}
	return 0;
	}

unsigned	Base::stripHeader(unsigned n) noexcept{
	if(n > _length){
		n	= _length;
		}
	_offset	+= n;
	_length	-= n;
	return n;
	}

unsigned	Base::stripTrailer(unsigned n) noexcept{
	if(n > _length){
		n	= _length;
		}
	_length	-= n;
	return n;
	}

unsigned	Base::advance(unsigned n) noexcept{
	unsigned	rem = remaining();
	if(n > rem){
		n	= rem;
		}
	_length	+= n;
	return n;
	}

const unsigned char*	Base::startOfData() const noexcept{
	const unsigned char*	s	= (const unsigned char*)getBuffer();
	return &s[_offset];
	}

void	Base::reset() noexcept{
	_length	= 0;
	_offset	= 0;
	}

void	Base::empty() noexcept{
	_length	= 0;
	_offset	= 0;
	}

unsigned	Base::length() const noexcept{
	return _length;
	}

// ReadOnly
unsigned	Base::copyOut(	void*		destination,
							unsigned	length
							) const noexcept{
	const unsigned char*	s	= (const unsigned char*)getBuffer();
	if(!s) return 0;
	if(_length < length){
		length	= _length;
		}
	memcpy(destination,&s[_offset],length);
	return length;
	}

unsigned	Base::copyOut(	void*			destination,
							unsigned	length,
							unsigned	sourceOffset
							) const noexcept{
	const unsigned char*	s	= (const unsigned char*)getBuffer();
	if(!s) return 0;
	if(sourceOffset >= _length) return 0;
	const unsigned	remaining	= _length-sourceOffset;
	if(remaining < length){
		length	= remaining;
		}
	memcpy(destination,&s[_offset],length);
	return length;
	}

unsigned	Base::copyOut(ReadWrite& destination) const noexcept{
	destination.empty();
	const void*		s	= getBuffer();
	if(!s) return 0;
	return destination.append(s,_length);
	}

unsigned char	Base::copyOut(unsigned offset) const noexcept{
	const unsigned char*	s	= (const unsigned char*)getBuffer();
	if(!s) return 0;
	if(offset >= _length) return 0;
	return s[offset+_offset];
	}

bool	Base::isEqual(	unsigned char	data,
						unsigned		offset
						) const noexcept{
	const unsigned char*	s	= (const unsigned char*)getBuffer();
	if(offset >= _length) return false;
	return (s[offset+_offset] == data);
	}

bool	Base::areEqual(	const void*		source,
						unsigned		length,
						unsigned		offset
						) const noexcept{
	if(offset >= _length) return false;
	const unsigned	remaining	= _length-offset;
	if(remaining < length) return false;
	const unsigned char*		s	= (const unsigned char*)getBuffer();
	if(!s) return false;
	return !memcmp(source,&s[offset+_offset],length);
	}

bool	Base::areEqual(	const ReadOnly&		source,
						unsigned			offset
						) const noexcept{
	if(offset >= _length) return false;
	const unsigned	remaining	= _length-offset;
	const unsigned	length		= source.length();
	if(remaining < length){
		return false;
		}
	const unsigned char*		s	= (const unsigned char*)getBuffer();
	if(!s) return false;
	return source.areEqual(&s[offset+_offset],0,length);
	}

unsigned	Base::copyIn(	const void*		source,
							unsigned		length
							) noexcept{
	void*		s	= buffer();
	if(!s) return 0;
	const unsigned		size	= bufferSize();
	if(length > size){
		length	= size;
		}
	memcpy(s,source,length);
	_offset	= 0;	// FIXME:?
	_length	= length;
	return length;
	}

unsigned	Base::copyIn(const ReadOnly& source) noexcept{
	void*		s	= buffer();
	if(!s) return 0;
	_length	= source.copyOut(s,source.length());
	_offset	= 0;
	return _length;
	}

unsigned	Base::prepend(	const void*		source,
							unsigned		length
							) noexcept{
	const unsigned char*	src	= (const unsigned char*)source;
	unsigned char*			s	= (unsigned char*)buffer();
	if(!s) return 0;
	const unsigned		remaining	= _offset;
	unsigned			offset		= 0;
	if(length > remaining){
		length	= remaining;
		offset	= length-remaining;
		}
	_offset	-= length;
	memcpy(&s[_offset],&src[offset],length);
	_length	+= length;
	return length;
	}

unsigned	Base::append(	const void*		source,
							unsigned		length
							) noexcept{
	unsigned char*		s	= (unsigned char*)buffer();
	if(!s) return 0;
	const unsigned		remaining	= this->remaining();
	if(length > remaining){
		length	= remaining;
		}
	memcpy(&s[_offset+_length],source,length);
	_length	+= length;
	return length;
	}

unsigned	Base::prepend(	const ReadOnly&	source,
							unsigned		length
							) noexcept{
	unsigned char*		s	= (unsigned char*)buffer();
	if(!s) return 0;
	const unsigned		remaining	= _offset;
	unsigned			offset		= 0;
	if(length > remaining){
		length	= remaining;
		offset	= length-remaining;
		}
	_offset	-= length;
	length	= source.copyOut(	&s[_offset+_length],
								length,
								offset
								);
	_length	+= length;
	return length;
	}

unsigned	Base::append(	const ReadOnly&	source,
							unsigned		length
							) noexcept{
	unsigned char*		s	= (unsigned char*)buffer();
	if(!s) return 0;
	const unsigned		remaining	= this->remaining();
	if(length > remaining){
		length	= remaining;
		}
	length	= source.copyOut(	&s[_offset+_length],
								length,
								0
								);
	_length	+= length;
	return length;
	}

unsigned	Base::prepend(const ReadOnly& source) noexcept{
	unsigned char*		s	= (unsigned char*)buffer();
	if(!s) return 0;
	const unsigned		remaining	= _offset;
	unsigned			offset		= 0;
	unsigned			length		= source.length();
	if(length > remaining){
		length		= remaining;
		offset		= length-remaining;
		}
	_offset	-=	length;
	_length	-=	length;
	length	= source.copyOut(	&s[_offset+_length],
								length,
								offset
								);
	return length;
	}

unsigned	Base::append(const ReadOnly& source) noexcept{
	unsigned char*		s	= (unsigned char*)buffer();
	if(!s) return 0;
	const unsigned		remaining	= this->remaining();
	unsigned	length	= source.copyOut(	&s[_offset+_length],
											remaining,
											0
											);
	_length	+=	length;
	return length;
	}

unsigned	Base::remaining() const noexcept{
	return bufferSize()-(_offset+_length);
	}

void Base::operator =(const char* source) noexcept{
	_offset	= 0;
	_length	= 0;
	unsigned	count	= strlen(source);
	unsigned	size	= bufferSize();
	unsigned	n		= (size<count)?size:count;
	append(source,n);
	}

void Base::operator =(char c) noexcept{
	char*			d		= (char*)buffer();
	if(!d) return;
	*d	= c;
	_offset	= 0;
	_length	= 1;
	}

void Base::operator =(unsigned char c) noexcept{
	unsigned char*			d		= (unsigned char*)buffer();
	if(!d) return;
	*d	= c;
	_offset	= 0;
	_length	= 1;
	}

void Base::operator =(const Base& source) noexcept{
	_offset	= 0;
	_length	= 0;
	append(source);
	}

void Base::operator +=(const Base& source) noexcept{
	append(source);
	}

void Base::operator +=(char c) noexcept{
	append(&c,1);
	}

void Base::operator +=(unsigned char c) noexcept{
	append(&c,1);
	}

bool Base::operator ==(const Base& other) const noexcept{
	if(_length != other._length) return false;
	const unsigned char*	thisBuff	= startOfData();
	const unsigned char*	thatBuff	= other.startOfData();
	return !memcmp(thisBuff,thatBuff,_length);
	}

bool Base::operator !=(const Base& other) const noexcept{
	if(_length != other._length) return true;
	const unsigned char*	thisBuff	= startOfData();
	const unsigned char*	thatBuff	= other.startOfData();
	return memcmp(thisBuff,thatBuff,_length);
	}

