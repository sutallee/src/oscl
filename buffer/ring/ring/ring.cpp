/*
   Copyright (C) 2005 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "ring.h"

using namespace Oscl::Buffer::Ring;

TxDesc::TxDesc(
    SharedApi&    shared,
    uint32_t    waterMark
    ) noexcept:
    _allocationOffset(0),
    _waterMark(waterMark),
    _nCommits(0),
    _shared(shared)
    {

    while(!_shared.isReady());

    uint32_t    maxFifoDepth    = _shared.maxFifoDepth();

    if(waterMark > maxFifoDepth){
        _waterMark    = maxFifoDepth;
        }
    }

bool    TxDesc::alloc(uint32_t& index) noexcept{

    uint32_t
    nElements    = _shared.nElements();

    uint32_t
    writeIndex    = _shared.writeIndex() + _allocationOffset;

    writeIndex    %=    nElements;


    uint32_t
    nextIndex    = (writeIndex + 1) % nElements;

    if( nextIndex == _shared.readIndex() ) {
        return false;
        }
    else {
        index    = writeIndex;
        ++_allocationOffset;
        return true;
        }
    }

bool    TxDesc::isFull() const noexcept{
    uint32_t
    nElements    = _shared.nElements();

    uint32_t
    index    = _shared.writeIndex() + _allocationOffset;

    index    %=    nElements;

    uint32_t
    nextIndex    =    (index + 1) % nElements;

    return (nextIndex == _shared.readIndex());
    }

void    TxDesc::commit() noexcept{
    uint32_t    nCommits;

    if(_waterMark) {
        ++_nCommits;
        if(_waterMark < _nCommits) {
            return;
            }
        nCommits    = _nCommits;
        _nCommits    = 0;
        _waterMark    = 0;
        }
    else {
        nCommits    = 1;
        }

    uint32_t
    index    = _shared.writeIndex();

    uint32_t
    nextIndex    = (index + nCommits) % _shared.nElements();

    _shared.writeIndex(nextIndex);

    _allocationOffset    -= nCommits;
    }

RxDesc::RxDesc(    SharedApi&    shared
                ) noexcept:
        _allocationOffset(0),
        _shared(shared)
        {
    }

bool    RxDesc::alloc(uint32_t& index) noexcept{
    uint32_t
    readIndex    = _shared.readIndex() + _allocationOffset;

    uint32_t
    nElements    = _shared.nElements();

    readIndex        %= nElements;

    uint32_t
    writeIndex    = _shared.writeIndex();

    if( readIndex == writeIndex ) {
        return false;
        }
    else {
        ++_allocationOffset;
        index    = readIndex;
        return true;
        }
    }

void    RxDesc::resetAllocationOffset() noexcept{
    _allocationOffset    = 0;
    }

void    RxDesc::commit() noexcept{
    uint32_t
    nElements    = _shared.nElements();

    uint32_t
    index        =    _shared.readIndex() + 1;
    index        %=    nElements;

    _shared.readIndex(index);

    --_allocationOffset;

    }

void    RxDesc::commitAll() noexcept{
    uint32_t
    nElements    = _shared.nElements();

    uint32_t
    index        =    _shared.readIndex() + _allocationOffset;
    index        %=    nElements;

    _shared.readIndex(index);

    _allocationOffset    = 0;
    }

