/*
   Copyright (C) 2005 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "lering.h"
#include "oscl/endian/type.h"
#include "oscl/cpu/sync.h"

using namespace Oscl::Buffer::Ring;

LittleEndianDesc::LittleEndianDesc(	Header&		header,
									Unsigned32 nElements
									) noexcept:
		_header(header)
		{
		header._nElements	= Oscl::Endian::toLittleEndian(nElements);
		header._readIndex	= 0;
		header._writeIndex	= 0;

		OsclCpuInOrderMemoryAccessBarrier();

		header._initialized	= Oscl::Endian::toLittleEndian((Unsigned32)(0xAA5500FFL));
	}

LittleEndianDesc::LittleEndianDesc(Header& header) noexcept:
		_header(header)
		{
	// Initialization is left to the other constructor.
	// Nothing is initialized in this constructor.
	}

bool	LittleEndianDesc::isReady() const noexcept{
	return (0xAA5500FF == Oscl::Endian::fromLittleEndian(_header._initialized));
	}

Oscl::Unsigned32	LittleEndianDesc::maxFifoDepth() const noexcept{
	return Oscl::Endian::fromLittleEndian(_header._nElements) - 1;
	}

Oscl::Unsigned32	LittleEndianDesc::nElements() const noexcept{
	return Oscl::Endian::fromLittleEndian(_header._nElements);
	}

Oscl::Unsigned32	LittleEndianDesc::writeIndex() const noexcept{
	return Oscl::Endian::fromLittleEndian(_header._writeIndex);
	}

void	LittleEndianDesc::writeIndex(Unsigned32 value) noexcept{
	_header._writeIndex	= Oscl::Endian::toLittleEndian(value);
	}

Oscl::Unsigned32	LittleEndianDesc::readIndex() const noexcept{
	return Oscl::Endian::fromLittleEndian(_header._readIndex);
	}

void	LittleEndianDesc::readIndex(Unsigned32 value) noexcept{
	_header._readIndex	= Oscl::Endian::toLittleEndian(value);
	}

