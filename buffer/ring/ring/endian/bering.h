/*
   Copyright (C) 2005 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_buffer_ring_beringh_
#define _oscl_buffer_ring_beringh_

#include "oscl/buffer/ring/ring.h"

/** */
namespace Oscl {
/** */
namespace Buffer {
/** */
namespace Ring {

/**
    This class represents the part of the ring buffer that is shared
    between the transmitting and receiving ends of the ring buffer.
    For multi-processor or shared memory applications that communicate
    through shared memory, this object *must* live in the shared memory.
 */
class BigEndianDesc : public SharedApi {
    private:
        /** This is a referenced to the header in shared memory.
         */
        Header&                _header;

    public:
        /** This constructor is invoked by the end of the ring
            buffer that is responsible for initializing the
            shared descriptor. The entity not responsible for
            initializing this object should use the other
            descriptor, which performs no initialization.
         */
        BigEndianDesc(
            Header&     header,
            uint32_t    nElements
            ) noexcept;

        /** This constructor is to be invoked by the end of
            the ring that is NOT responsible for initializing
            the shared descriptor. The entity that *is* responsible
            must use the other constructor.
         */
        BigEndianDesc( Header& header ) noexcept;

        /** Returns true if the ring has been initialized by the
            responsible party.
         */
        bool    isReady() const noexcept;

        /** Returns the maximum number of elements that
            may be placed in the ring.
         */
        uint32_t    maxFifoDepth() const noexcept;

        /** Returns the maximum number of elements
         */
        uint32_t    nElements() const noexcept;

        /** Returns the current write index.
         */
        uint32_t    writeIndex() const noexcept;

        /** Update the write index.
         */
        void    writeIndex(uint32_t value) noexcept;

        /** Returns the current read index.
         */
        uint32_t    readIndex() const noexcept;

        /** Update the read index.
         */
        void    readIndex(uint32_t value) noexcept;

    };

}
}
}

#endif
