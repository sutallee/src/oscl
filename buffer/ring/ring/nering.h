/*
   Copyright (C) 2005 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_buffer_ring_neringh_
#define _oscl_buffer_ring_neringh_

#include "ring.h"

/** */
namespace Oscl {
/** */
namespace Buffer {
/** */
namespace Ring {

/**
    This class represents the part of the ring buffer that is shared
    between the transmitting and receiving ends of the ring buffer.
    For multi-processor or shared memory applications that communicate
    through shared memory, this object *must* live in the shared memory.

    NOTE: This NoEndian variant MUST NOT be used between CPUs with
    different byte-ordering.
 */
class NoEndianDesc : public SharedApi {
    private:
        /** This is a referenced to the header in shared memory.
         */
        Header&                _header;

    public:
        /** This constructor is invoked by the end of the ring
            buffer that is responsible for initializing the
            shared descriptor. The entity not responsible for
            initializing this object should use the other
            descriptor, which performs no initialization.
         */
        NoEndianDesc(
            Header&     header,
            uint32_t    nElements
            ) noexcept;

        /** This constructor is to be invoked by the end of
            the ring that is NOT responsible for initializing
            the shared descriptor. The entity that *is* responsible
            must use the other constructor.
         */
        NoEndianDesc( Header&    header ) noexcept;

        /** Returns true if the ring has been initialized by the
            responsible party.
         */
        bool    isReady() const noexcept;

        /** Returns the maximum number of elements that
            may be placed in the ring.
         */
        uint32_t    maxFifoDepth() const noexcept;

        /** Returns the maximum number of elements
         */
        uint32_t    nElements() const noexcept;

        /** Returns the current write index.
         */
        uint32_t    writeIndex() const noexcept;

        /** Update the write index.
         */
        void    writeIndex(uint32_t value) noexcept;

        /** Returns the current read index.
         */
        uint32_t    readIndex() const noexcept;

        /** Update the read index.
         */
        void    readIndex(uint32_t value) noexcept;
    };

template <class ITEM, unsigned nItems>
class NeRing {
    private:
        ITEM            _items[nItems];
        Header          _header;
        NoEndianDesc    _shared;
        TxDesc          _txdesc;
        RxDesc          _rxdesc;

    public:
        NeRing() noexcept:
            _shared(_header,nItems),
            _txdesc(_shared),
            _rxdesc(_shared)
            {
            }

    public:
        /** Removes the first item in the Buffer. The contents of the
            removed item will be copied into the 'dst' argument. The method
            return true if the operation was successful; else false is
            returned, i.e. the Ring buffer is/was empty.
        */
        bool remove( ITEM& dst ) noexcept{
            uint32_t    index;
            if( !_rxdesc.alloc(index) ){
                return false;
                }
            dst = _items[index];
            _rxdesc.commit();
            return true;
            }


        /** The contents of 'item' will be copied into the Ring Buffer as the
            'last' item in the  buffer. Return true if the operation was
            successful; else false is returned, i.e. the Buffer was full prior to
            the attempted add().
         */
        bool add( const ITEM& item ) noexcept{
            uint32_t    index;
            if( !_txdesc.alloc(index) ){
                return false;
                }
            _items[index]   = item;
            _txdesc.commit();
            return true;
            }

        /** Empties the Ring Buffer.  All references to the item(s) in the
            buffer are lost.
        */
        void clearTheBuffer() noexcept{
            _header._readIndex    = 0;
            _header._writeIndex   = 0;
            _header._initialized = (uint32_t)(0xAA5500FFL);
            }
        /** This method returns true if the Ring Buffer is empty
         */
        bool isEmpty( void ) const noexcept {
            return _header._readIndex == _header._writeIndex;
            }

        /** This method returns true if the Ring Buffer is full
         */
        bool isFull( void ) const noexcept{
            return _txdesc.isFull();
            }

    };

}
}
}

#endif
