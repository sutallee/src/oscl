/*
   Copyright (C) 2005 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_buffer_ring_ringh_
#define _oscl_buffer_ring_ringh_

#include <stdint.h>

/** */
namespace Oscl {
/** */
namespace Buffer {
/** */
namespace Ring {

/** This data structure represents the format of the data
	in the memory shared by the ring transmitter and the
	ring receiver (which may be the same in a single memory
	space system.)
	For multi-processor or shared memory applications that communicate
	through shared memory, this object *must* live in the shared memory.
 */
struct Header {
	/** The variable indicates the number of elements in the
		ring buffer. The definition of the element itself is
		outside of the scope of this class.
	 */
	uint32_t			_nElements;

	/** This variable is updated only by the ring buffer
		receiver when an element is removed from the ring buffer.
	 */
	volatile uint32_t	_readIndex;

	/** This variable is updated only by the ring buffer
		transmitter when an element is addded to the ring
		buffer.
	 */
	volatile uint32_t	_writeIndex;

	/** This boolean variable is zero if this header has been
		initialized. It is important that only one of the transmit
		or receive end of the ring descriptor perform this
		initialization and the other end wait until this flag
		is non-zero.
	 */
	volatile uint32_t	_initialized;
	};

/**
	This class represents interface to the part of the ring buffer
	that is shared between the transmitting and receiving ends of the
	ring buffer. The reason for this interface is to support
	shared memory in which the communication protocol can be declared
	as BIG or LITTLE endian.
 */
class SharedApi {
	public:
		/** Obligatory virtual destructor.
		 */
		virtual ~SharedApi(){}

		/** Returns true if the ring has been initialized by the
			responsible party.
		 */
		virtual bool	isReady() const noexcept=0;

		/** Returns the maximum number of elements that
			may be placed in the ring.
		 */
		virtual uint32_t	maxFifoDepth() const noexcept=0;

		/** Returns the maximum number of elements
		 */
		virtual uint32_t	nElements() const noexcept=0;

		/** Returns the current write index.
		 */
		virtual uint32_t	writeIndex() const noexcept=0;

		/** Update the write index.
		 */
		virtual void	writeIndex(uint32_t value) noexcept=0;

		/** Returns the current read index.
		 */
		virtual uint32_t	readIndex() const noexcept=0;

		/** Update the read index.
		 */
		virtual void	readIndex(uint32_t value) noexcept=0;

	};

/**
	This class represents the part of the ring buffer that is shared
	between the transmitting and receiving ends of the ring buffer.
	For multi-processor or shared memory applications that communicate
	through shared memory, this object *must* live in the shared memory.
 */
class BigEndianDesc : public SharedApi {
	private:
		/** This is a referenced to the header in shared memory.
		 */
		Header&				_header;

	public:
		/** This constructor is invoked by the end of the ring
			buffer that is responsible for initializing the
			shared descriptor. The entity not responsible for
			initializing this object should use the other
			descriptor, which performs no initialization.
		 */
		BigEndianDesc(	Header&		header,
						uint32_t	nElements
						) noexcept;

		/** This constructor is to be invoked by the end of
			the ring that is NOT responsible for initializing
			the shared descriptor. The entity that *is* responsible
			must use the other constructor.
		 */
		BigEndianDesc( Header&	header ) noexcept;

		/** Returns true if the ring has been initialized by the
			responsible party.
		 */
		bool	isReady() const noexcept;

		/** Returns the maximum number of elements that
			may be placed in the ring.
		 */
		uint32_t	maxFifoDepth() const noexcept;

		/** Returns the maximum number of elements
		 */
		uint32_t	nElements() const noexcept;

		/** Returns the current write index.
		 */
		uint32_t	writeIndex() const noexcept;

		/** Update the write index.
		 */
		void	writeIndex(uint32_t value) noexcept;

		/** Returns the current read index.
		 */
		uint32_t	readIndex() const noexcept;

		/** Update the read index.
		 */
		void	readIndex(uint32_t value) noexcept;

	};

/**
	This class represents the part of the ring buffer that is shared
	between the transmitting and receiving ends of the ring buffer.
	For multi-processor or shared memory applications that communicate
	through shared memory, this object *must* live in the shared memory.
 */
class LittleEndianDesc : public SharedApi {
	private:
		/** This is a referenced to the header in shared memory.
		 */
		Header&				_header;

	public:
		/** This constructor is invoked by the end of the ring
			buffer that is responsible for initializing the
			shared descriptor. The entity not responsible for
			initializing this object should use the other
			descriptor, which performs no initialization.
		 */
		LittleEndianDesc(	Header&		header,
							uint32_t	nElements
							) noexcept;

		/** This constructor is to be invoked by the end of
			the ring that is NOT responsible for initializing
			the shared descriptor. The entity that *is* responsible
			must use the other constructor.
		 */
		LittleEndianDesc( Header&	header ) noexcept;

		/** Returns true if the ring has been initialized by the
			responsible party.
		 */
		bool	isReady() const noexcept;

		/** Returns the maximum number of elements that
			may be placed in the ring.
		 */
		uint32_t	maxFifoDepth() const noexcept;

		/** Returns the maximum number of elements
		 */
		uint32_t	nElements() const noexcept;

		/** Returns the current write index.
		 */
		uint32_t	writeIndex() const noexcept;

		/** Update the write index.
		 */
		void	writeIndex(uint32_t value) noexcept;

		/** Returns the current read index.
		 */
		uint32_t	readIndex() const noexcept;

		/** Update the read index.
		 */
		void	readIndex(uint32_t value) noexcept;

	};

/** This class is used by the entity that writes into the ring.
	In this implementation, the process of writing the ring buffer
	is divided into two stages. In the first stage, the client
	allocates a slot in the ring buffer. After allocating the slot,
	the client then fills the buffer at its convenience.
	In the second stage, after the buffer associated with the slot
	has been filled, the client then invokes a commit operation,
	which modifies the shared ring buffer announcing the availability
	of data to the receiving end.

	This object is accessed only by the transmitter, and may
	live in memory that is NOT shared with the receiver.
 */
class TxDesc {
	private:
		/** A variable used by the implementation to manage
			slot allocation.
		 */
		uint32_t	_allocationOffset;

		/** This variable is used to allow a certain number of
			transmit commits to accumulate before actually
			making them visible to the receiver.
		 */
		uint32_t	_waterMark;

		/** This variable is used in the water-marking process
			at startup to track the number of commits made by
			the client.
		 */
		uint32_t	_nCommits;

		/** This is a refrence to the ring descriptor that must
			live in the memory shared by the transmitter and
			receiver processes.
		 */
		SharedApi&	_shared;

	public:
		/** The constructor requires a reference to the
			memory object shared with the receiver. It also
			can be invoked with a water mark value that
			determines the number of transmit buffers/slots
			that must be committed at startup before the
			shared descriptor is updated and they
			are actually made visible to the receiver.
		 */
		TxDesc(	SharedApi&	shared,
				uint32_t	waterMark = 0
				) noexcept;

		/** This operation attempts to allocate a writeable
			slot in the ring buffer. The index of the allocated
			slot is written to the variable referenced by the
			"index" argument if a slot is available.
			Returns true if a slot is available and false
			if there are no available slots.
		 */
		bool	alloc(uint32_t& index) noexcept;

		/** This operation returns true if the ring buffer
			is full and false otherwise.
		 */
		bool	isFull() const noexcept;

		/** This operation is used to update the shared ring
			descriptor to indicate that a previously allocated
			slot (using the alloc() opration above) has been
			filled with data and is ready to be recognized by
			the receiving end of the ring.

			Note: Slots are assumed to be committed in the
			same order in which they are allocated.
		 */
		void	commit() noexcept;
	};

/** This class is used by the entity that reads the ring.
	In this implementation, the process of reading the ring buffer
	is divided into two stages. In the first stage, the client
	allocates a filled slot in the ring buffer. After a slot is
	allocated, the client then processes the data in the buffer
	at its convenience.

	Once the client finishes using the buffer associated with the
	allocated slot, the client then invokes a commit operation,
	which modifies the shared ring buffer announcing the free
	slot to transmitting end.

	This object is accessed only by the transmitter, and may
	live in memory that is NOT shared with the transmitter.
 */
class RxDesc {
	private:
		/** A variable used by the implementation to manage
			slot allocation.
		 */
		uint32_t	_allocationOffset;

		/** This is a refrence to the ring descriptor that must
			live in the memory shared by the transmitter and
			receiver processes.
		 */
		SharedApi&	_shared;

	public:
		/** The constructor requires a reference to the
			memory object shared with the receiver.
		 */
		RxDesc(	SharedApi&	shared
				) noexcept;

		/** This operation attempts to allocate a readable
			slot in the ring buffer. The index of the allocated
			slot is written to the variable referenced by the
			"index" argument if a slot is available.
			Returns true if a slot is available and false
			if there are no available slots.
		 */
		bool	alloc(uint32_t& index) noexcept;

		/** This operation resets the uncommitted allocation
			index. This allows readers to re-issue the
			alloc() operation which then returns the
			index of the first un-committed slot.
		 */
		void	resetAllocationOffset() noexcept;

		/** This operation is used to update the shared ring
			descriptor to indicate that a previously allocated
			slot (using the alloc() opration above) has been
			released and is ready to be recognized as such by
			the transmitting end of the ring.

			Note: Slots are assumed to be committed in the
			same order in which they are allocated.
		 */
		void	commit() noexcept;

		/** This operation is used to update the shared ring
			descriptor to indicate that all previously allocated
			slots (using the alloc() opration above) have been
			released and are ready to be recognized as such by
			the transmitting end of the ring.
		 */
		void	commitAll() noexcept;
	};

}
}
}

#endif
