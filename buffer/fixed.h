/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_buffer_fixedh_
#define _oscl_buffer_fixedh_
#include "base.h"

/** */
namespace Oscl {
/** */
namespace Buffer {

template <unsigned buffSize>
class Fixed : public Base {
	private:
		/** */
		unsigned char				_buffer[buffSize];

	public:
		/** */
		Fixed() noexcept;

	public:
		/** This returns the maximum length of the buffer.
		 */
		unsigned	bufferSize() const noexcept;

		/** This returns a read-only pointer to the actual
			buffer. It may return zero if no buffer is currently
			allocated.
		 */
		const void*		getBuffer() const noexcept;

	private:
		/** Returns a pointer to the buffer memory. May return
			zero if no buffer is currently allocated. This operation
			must be implemented by sub-classes and is used to implement
			the standard buffer operations.
		 */
		void*		buffer() noexcept;
	};

template <unsigned buffSize>
Fixed<buffSize>::Fixed() noexcept
		{
	}

template <unsigned buffSize>
unsigned	Fixed<buffSize>::bufferSize() const noexcept{
	return buffSize;
	}

template <unsigned buffSize>
const void*		Fixed<buffSize>::getBuffer() const noexcept{
	return _buffer;
	}

template <unsigned buffSize>
void*			Fixed<buffSize>::buffer() noexcept{
	return _buffer;
	}

}
}

#endif
