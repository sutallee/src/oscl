/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_buffer_apih_
#define _oscl_buffer_apih_

/*	The interfaces described in this file are intended to implementable
	for various types of buffers. In particular, it is important that
	these interfaces be implementable for buffers which may not be
	contiguous (e.g. scatter/gather buffers).
 */

/** */
namespace Oscl {
/** */
namespace Buffer {

class ReadWrite;

class ReadOnly {
	public:
		/** Virtual destructors are just a good idea.
		 */
		virtual ~ReadOnly(){}

	public:
		/** This operation returns the amount of valid data contained
			in the buffer.
		 */
		virtual unsigned	length() const noexcept=0;

	public: //------------ Truncation Operations ---------- //
		/** This operation adjusts the buffer such that its
			contents are no longer accessible. Subsequent
			append and copyIn operations reuse the empty buffer
			space.
			The behaviour of this operation may vary significantly
			with implementation. A simple contiguous buffer
			implementation may simply reset offset and length
			indicies. A scatter/gather buffer chain may release
			all buffers.
		 */
		virtual void	empty() noexcept=0;

	public: //------------ Copy OUT Operations ---------- //
		/** This operation copies up to "length" valid bytes from this buffer
			into the destination buffer. The number of bytes actually copied
			to the destination buffer is returned as a result.
		 */
		virtual unsigned	copyOut(	void*		destination,
										unsigned	length
										) const noexcept=0;

		/** */
		virtual unsigned	copyOut(ReadWrite& destination) const noexcept=0;

		/** This operation copies up to "length" valid bytes from this buffer
			at the specified "sourceOffset" from the beginning of the buffer
			into the destination buffer. The number of bytes actually copied
			to the destination buffer is returned as a result.
		 */
		virtual unsigned	copyOut(	void*		destination,
										unsigned	length,
										unsigned	sourceOffset
										) const noexcept=0;

		/** This operation copies the valid bytes in this buffer into the
			destination buffer. The actual number copied is returned as a
			result. The actual number copied to the destination may be less
			than the number of valid bytes in this buffer if the destination
			buffer is not large enought to hold all of the valid bytes.
		 */
		/** This operation returns the byte at the specified offset.
			If the offset is out of buffer range, the return value
			is undefined. It is the responsiblity of the caller to
			use the length() operation to find out if the offset is
			valid.
		 */
		virtual unsigned char	copyOut(unsigned offset) const noexcept=0;

		/** Returns true if the "data" byte has the same value as the
			byte in the buffer at the specified "offset".
		 */
		virtual bool	isEqual(	unsigned char	data,
									unsigned		offset
									) const noexcept=0;

		/** Returns true if the "length" bytes of source are exactly equal
			to the "length" bytes of this buffer starting from the
			"sourceOffset" of this buffer.
		 */
		virtual bool	areEqual(	const void*		source,
									unsigned		length,
									unsigned		sourceOffset
									) const noexcept=0;

		/** Returns true if all of the bytes of "source" exactly matches
			the bytes of this buffer starting at the "sourceOffset" of
			this buffer.
		 */
		virtual bool	areEqual(	const ReadOnly&		source,
									unsigned			sourceOffset
									) const noexcept=0;
	};

/** This abstract class represents the standard interface for all
	writable buffers.

	A ReadOnly buffer is one which the user/client may only truncate
	or read/copy.

	A ReadWrite buffer is one to which the user/client may not only
	truncate and read/copy, but may also append or replace the content.

	Notice that a "const" reference to a ReadOnly buffer is more
	restrictive, since the user/client may not truncate the buffer.

	Also, notice that a "const" reference to a ReadWrite buffer has
	the same restrictions as a "const" reference to a ReadOnly buffer.
 */
class ReadWrite : public ReadOnly {
	public:
		/** */
		virtual ~ReadWrite(){}

	public: //------------ Copy IN Operations ---------- //

		/** This operation copies count bytes of data from the source into
			the buffer starting at the beginning of the buffer. The
			number of bytes actually copied to the buffer is returned
			as a result. The number of bytes actually copied may be
			less than the requested count bytes if this buffer is not large
			enough to hold all the source data.
			This operation replaces any existing data in the buffer.

			The behaviour of this operation may vary significantly
			with implementation. A simple contiguous buffer
			implementation may simply reset an offset index;
			memcpy() the source into the buffer; and record the
			new length value. A scatter/gather buffer chain may
			allocate and/or release buffers.
		 */
		virtual unsigned	copyIn(	const void*	source,
									unsigned	count
									) noexcept=0;

// This operation is not implemented since this operation:
//
//		destination.copyIn(source,25);
//
// may be achieved by:
//
//		source.copyOut(destination,25);
//
// A copyOut operation is "easily" implemented for a scatter/gather
// implementation by invoking the 
//
//	SG::copyOut(ReadWrite& destination,unsinged long length) const noexcept{
//		unsigned	total=0;
//		destination.empty();
//		if(!remaining) return;
//		foreach fragment in fragment list {
//			unsigned	fragLength	= fragment.length();
//			if(fragLength > length){
//				fragLength	= length;
//				}
//			length	-= fragLength;
//			copied	=
//				distination.append(fragment.getBuffer(),fragLength);
//			total	+= copied;
//			if(copied < fragLength){
//				return total;
//				}
//			if(!length){
//				return total;
//				}
//			}
//		}
//	
//
//
//		virtual unsigned	copyIn(	const ReadOnly&		source,
//										unsigned		length
//										) noexcept=0;

		/** This operation copies the valid bytes in the source buffer into
			this buffer. The number of bytes actually copied to the buffer
			is returned as a result. The number of bytes actually copied may be
			less than the number of valid bytes in the source buffer if this
			buffer is not large enough to hold all of the source data.
			This operation replaces any existing data in the buffer.

			The behaviour of this operation may vary significantly
			with implementation. A simple contiguous buffer
			implementation may simply reset an offset index;
			memcpy() the source into the buffer; and record the
			new length value. A scatter/gather buffer chain may
			allocate and/or release buffers.
		 */
		virtual unsigned	copyIn(const ReadOnly& source) noexcept=0;

		public: //------------ Append Operations ---------- //

		/** This operation appends "length" bytes of data from the "source" into
			the buffer. The number of bytes actually copied to the buffer
			is returned as a result. The number of bytes actually copied may be
			less than the requested "length" bytes if this buffer is not large
			enough to hold all the source data.
		 */
		virtual unsigned	append(	const void*	source,
									unsigned	length
									) noexcept=0;

		/** This operation appends "length" bytes of data from the "source" into
			the buffer. The number of bytes actually copied to the buffer
			is returned as a result. The number of bytes actually copied may be
			less than the requested "length" bytes if this buffer is not large
			enough to hold all the source data or the source does not contain
			the "length" bytes of data.
		 */
		virtual unsigned	append(	const ReadOnly&	source,
									unsigned		length
									) noexcept=0;

		/** This operation copies all bytes of data from the "source" into
			the buffer. The number of bytes actually copied to the buffer
			is returned as a result. The number of bytes actually copied may be
			less than the number of bytes in the "source" if the buffer is not
			large enough to hold all the source data.
		 */
		virtual unsigned	append(const ReadOnly& source) noexcept=0;

	};

}
}

#endif
