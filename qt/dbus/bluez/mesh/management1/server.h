/*
   Copyright (C) 2020 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_qt_dbus_bluez_mesh_management1_management1h_
#define _oscl_qt_dbus_bluez_mesh_management1_management1h_

#include <QObject>
#include <QDBusObjectPath>
#include <QByteArray>
#include <QVariantMap>
#include <QDBusContext>
#include "contextapi.h"

/** */
namespace Oscl {
/** */
namespace Qt {
/** */
namespace DBus {
/** */
namespace BlueZ {
/** */
namespace Mesh {
/** */
namespace Management1 {

/** */
class Server : public QObject, protected QDBusContext {
	Q_OBJECT
	Q_CLASSINFO("D-Bus Interface", "org.bluez.mesh.Management1")

	private:
		/** */
		ContextApi&	_contextApi;

	public:
		/** */
		Server(
			ContextApi&	contextApi,
			QObject*	parent	= 0
			);

		/** */
		virtual ~Server();

	public slots:
		/** */
		void	UnprovisionedScan(
					const QVariantMap&	options
					);

		/** */
		void	UnprovisionedScanCancel();

		/** */
		void	AddNode(
					const QByteArray&	uuid,
					const QVariantMap&	options
					);

		/** */
		void	CreateSubnet(
					ushort	net_index
					);

		/** */
		void	ImportSubnet(
					ushort	net_index,
					const QByteArray&	net_key
					);

		/** */
		void	UpdateSubnet(
					ushort	net_index
					);

		/** */
		void	DeleteSubnet(
					ushort	net_index
					);

		/** */
		void	SetKeyPhase(
					ushort	net_index,
					uchar	phase
					);

		/** */
		void	CreateAppKey(
					ushort	net_index,
					ushort	app_index
					);

		/** */
		void	ImportAppKey(
					ushort	net_index,
					ushort	app_index,
					const QByteArray&	app_key
					);

		/** */
		void	UpdateAppKey(
					ushort	app_index
					);

		/** */
		void	DeleteAppKey(
					ushort	app_index
					);

		/** */
		void	ImportRemoteNode(
					ushort	primary,
					uchar	count,
					const QByteArray&	device_key
					);

		/** */
		void	DeleteRemoteNode(
					ushort	primary,
					uchar	count
					);
	};

}
}
}
}
}
}

#endif
