/*
   Copyright (C) 2020 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_qt_dbus_bluez_mesh_management1_contextapih_
#define _oscl_qt_dbus_bluez_mesh_management1_contextapih_

#include <functional>

/** */
namespace Oscl {
/** */
namespace Qt {
/** */
namespace DBus {
/** */
namespace BlueZ {
/** */
namespace Mesh {
/** */
namespace Management1 {

/** This interface is designed to satisfy the
	data needs of the Management1 class without
	reference to the D-Bus or Qt in general.
 */
class ContextApi {
	public:
		/** seconds set to zero means ulimited seconds. */
		virtual void	unprovisionedScanReceived(
							uint16_t			seconds
							) noexcept=0;

		/** */
		virtual void	unprovisionedScanCancelReceived() noexcept=0;

		/** */
		virtual void	addNodeReceived(
							const uint8_t		uuid[16]
							) noexcept=0;

		/** */
		virtual void	createSubnetReceived(
							uint16_t	net_index
							) noexcept=0;

		/** */
		virtual void	importSubnetReceived(
							uint16_t		net_index,
							const uint8_t	net_key[16]
							) noexcept=0;

		/** */
		virtual void	updateSubnetReceived(
							uint16_t	net_index
							) noexcept=0;

		/** */
		virtual void	deleteSubnetReceived(
							uint16_t	net_index
							) noexcept=0;

		/** */
		virtual void	setKeyPhaseReceived(
							uint16_t	net_index,
							uint8_t		phase
							) noexcept=0;

		/** */
		virtual void	createAppKeyReceived(
							uint16_t	net_index,
							uint16_t	app_index
							) noexcept=0;

		/** */
		virtual void	importAppKeyReceived(
							uint16_t		net_index,
							uint16_t		app_index,
							const uint8_t	app_key[16]
							) noexcept=0;

		/** */
		virtual void	updateAppKeyReceived(
							uint16_t	app_index
							) noexcept=0;

		/** */
		virtual void	deleteAppKeyReceived(
							uint16_t	app_index
							) noexcept=0;

		/** */
		virtual void	importRemoteNodeReceived(
							uint16_t		primary,
							uint8_t			count,
							const uint8_t	device_key[16]
							) noexcept=0;

		/** */
		virtual void	deleteRemoteNodeReceived(
							uint16_t	primary,
							uint8_t		count
							) noexcept=0;
	};

}
}
}
}
}
}

#endif
