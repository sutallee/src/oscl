/*
   Copyright (C) 2020 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "server.h"

using namespace Oscl::Qt::DBus::BlueZ::Mesh::Management1;

Server::Server(
	ContextApi&	contextApi,
	QObject*	parent
	) :
		QObject(parent),
		_contextApi(contextApi)
		{
	}

Server::~Server() {
	}

void	Server::UnprovisionedScan(
			const QVariantMap&	options
			){
	uint16_t	seconds	= 0;

	QVariantMap::const_iterator	i	= options.cbegin();
	for(;i != options.end();++i){
		if(i.key() == "Seconds"){
			seconds	= i.value().value<ushort>();
			}
		}

	_contextApi.unprovisionedScanReceived(
		seconds
		);
	}

void	Server::UnprovisionedScanCancel(){
	_contextApi.unprovisionedScanCancelReceived();
	}

void	Server::AddNode(
			const QByteArray&	uuid,
			const QVariantMap&	options
			){

	// There are, currently, no options defined. :-/

	if(uuid.count() != 16){
		sendErrorReply(
			"org.bluez.mesh.Error.InvalidArguments",
			"uuid must be exactly 16 octets."
			);
		return;
		}

	_contextApi.addNodeReceived(
		(const uint8_t*)uuid.constData()
		);
	}

void	Server::CreateSubnet(
			ushort	net_index
			){
	_contextApi.createSubnetReceived(
		net_index
		);
	}

void	Server::ImportSubnet(
			ushort	net_index,
			const QByteArray&	net_key
			){
	if(net_key.count() != 16){
		sendErrorReply(
			"org.bluez.mesh.Error.InvalidArguments",
			"net_key must be exactly 16 octets."
			);
		return;
		}

	_contextApi.importSubnetReceived(
		net_index,
		(const uint8_t*)net_key.constData()
		);
	}

void	Server::UpdateSubnet(
			ushort	net_index
			){
	_contextApi.updateSubnetReceived(
		net_index
		);
	}

void	Server::DeleteSubnet(
			ushort	net_index
			){
	_contextApi.deleteSubnetReceived(
		net_index
		);
	}

void	Server::SetKeyPhase(
			ushort	net_index,
			uchar	phase
			){
	_contextApi.setKeyPhaseReceived(
		net_index,
		phase
		);
	}

void	Server::CreateAppKey(
			ushort	net_index,
			ushort	app_index
			){
	_contextApi.createAppKeyReceived(
		net_index,
		app_index
		);
	}

void	Server::ImportAppKey(
			ushort	net_index,
			ushort	app_index,
			const QByteArray&	app_key
			){

	if(app_key.count() != 16){
		sendErrorReply(
			"org.bluez.mesh.Error.InvalidArguments",
			"app_key must be exactly 16 octets."
			);
		return;
		}

	_contextApi.importAppKeyReceived(
		net_index,
		app_index,
		(const uint8_t*)app_key.constData()
		);
	}

void	Server::UpdateAppKey(
			ushort	app_index
			){
	_contextApi.updateAppKeyReceived(
		app_index
		);
	}

void	Server::DeleteAppKey(
			ushort	app_index
			){
	_contextApi.deleteAppKeyReceived(
		app_index
		);
	}

void	Server::ImportRemoteNode(
			ushort	primary,
			uchar	count,
			const QByteArray&	device_key
			){

	if(device_key.count() != 16){
		sendErrorReply(
			"org.bluez.mesh.Error.InvalidArguments",
			"device_key must be exactly 16 octets."
			);
		return;
		}

	_contextApi.importRemoteNodeReceived(
		primary,
		count,
		(const uint8_t*)device_key.constData()
		);
	}

void	Server::DeleteRemoteNode(
			ushort	primary,
			uchar	count
			){
	_contextApi.deleteRemoteNodeReceived(
		primary,
		count
		);
	}

