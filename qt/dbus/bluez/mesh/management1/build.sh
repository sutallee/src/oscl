#!/usr/bin/bash

echo "Creating C++ Adaptor for Management1"
qdbusxml2cpp -l Oscl::Qt::DBus::BlueZ::Mesh::Management1::Server -i server.h -a adaptor dbus.xml

echo "Creating C++ Proxy for Management1"
qdbusxml2cpp -l Oscl::Qt::DBus::BlueZ::Mesh::Management1::Server -i server.h -p proxy dbus.xml

