/*
   Copyright (C) 2020 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/


#include "server.h"

using namespace Oscl::Qt::DBus::BlueZ::Mesh::Node1;

Server::Server(
	ContextApi&	contextApi,
	QObject*	parent
	) :
		QObject(parent),
		_contextApi(contextApi)
		{
	}

Server::~Server() {
	}

void	Server::Send(
			const QDBusObjectPath&	element_path,
			ushort					destination,
			ushort					key_index,
			const QVariantMap&		options,
			const QByteArray&		data
			){
	bool	forceSegmented	= false;

	QVariantMap::const_iterator	i	= options.constFind("ForceSegmented");
	for(;i != options.end() && i.key() == "ForceSegmented";++i){
		forceSegmented	= i.value().toBool();
		}

	_contextApi.sendReceived(
		element_path.path().toLatin1().constData(),
		destination,
		key_index,
		forceSegmented,
		(unsigned)data.count(),
		(const uint8_t*)data.constData()
		);
	}

void	Server::DevKeySend(
			const QDBusObjectPath&	element_path,
			ushort					destination,
			bool					remote,
			ushort					net_index,
			const QVariantMap&		options,
			const QByteArray&		data
			){
	bool	forceSegmented	= false;

	QVariantMap::const_iterator	i	= options.constFind("ForceSegmented");
	for(;i != options.end() && i.key() == "ForceSegmented";++i){
		forceSegmented	= i.value().toBool();
		}

	_contextApi.devKeySendReceived(
		element_path.path().toLatin1().constData(),
		destination,
		remote,
		net_index,
		forceSegmented,
		(unsigned)data.count(),
		(const uint8_t*)data.constData()
		);
	}

void	Server::AddNetKey(
			const QDBusObjectPath&	element_path,
			ushort					destination,
			ushort					subnet_index,
			ushort					net_index,
			bool					update
			){
	_contextApi.addNetKeyReceived(
		element_path.path().toLatin1().constData(),
		destination,
		subnet_index,
		net_index,
		update
		);
	}

void	Server::AddAppKey(
			const QDBusObjectPath&	element_path,
			ushort					destination,
			ushort					app_index,
			ushort					net_index,
			bool					update
			){
	_contextApi.addAppKeyReceived(
		element_path.path().toLatin1().constData(),
		destination,
		app_index,
		net_index,
		update
		);
	}

void	Server::Publish(
			const QDBusObjectPath&	element_path,
			ushort					model,
			const QVariantMap&		options,
			const QByteArray&		data
			){
	bool	forceSegmented	= false;

	bool	vendorValid		= false;
	uint16_t	companyID	= 0;

	QVariantMap::const_iterator	i	= options.cbegin();
	for(;i != options.end();++i){
		if(i.key() == "ForceSegmented"){
			forceSegmented	= i.value().toBool();
			}
		if(i.key() == "Vendor"){
			vendorValid	= true;
			printf("%s: FIXME Vendor: typeName: %s\n",__PRETTY_FUNCTION__,i.value().typeName());
			companyID	= i.value().value<ushort>();
			}
		}

	_contextApi.publishReceived(
		element_path.path().toLatin1().constData(),
		model,
		forceSegmented,
		vendorValid,
		companyID,
		(unsigned)data.count(),
		(const uint8_t*)data.constData()
		);
	}

QVariantMap	Server::Features() const{
	/*
	dict Features [read-only]

		The dictionary that contains information about feature support.
		The following keys are defined:

		boolean Friend

			Indicates the ability to establish a friendship with a
			Low Power node

		boolean LowPower

			Indicates support for operating in Low Power node mode

		boolean Proxy

			Indicates support for GATT proxy

		boolean Relay
			Indicates support for relaying messages

	If a key is absent from the dictionary, the feature is not supported.
	Otherwise, true means that the feature is enabled and false means that
	the feature is disabled.

	 */

	printf("%s\n",__PRETTY_FUNCTION__);fflush(stdout);

	QVariantMap	features;

	if(_contextApi.friendFeatureSupported()){
		bool	enabled	= _contextApi.friendFeatureEnabled();
		features.insert("Friend",QVariant::fromValue(enabled));
		}

	if(_contextApi.lowPowerFeatureSupported()){
		bool	enabled	= _contextApi.lowPowerFeatureEnabled();
		features.insert("LowPower",QVariant::fromValue(enabled));
		}

	if(_contextApi.proxyFeatureSupported()){
		bool	enabled	= _contextApi.proxyFeatureEnabled();
		features.insert("Proxy",QVariant::fromValue(enabled));
		}

	if(_contextApi.relayFeatureSupported()){
		bool	enabled	= _contextApi.relayFeatureEnabled();
		features.insert("Relay",QVariant::fromValue(enabled));
		}

	return features;
	}

bool	Server::Beacon() const{
	return _contextApi.beacon();
	}

bool	Server::IvUpdate() const{
	return _contextApi.ivUpdate();
	}

uint32_t	Server::IvIndex() const{
	return _contextApi.ivIndex();
	}

uint32_t	Server::SecondsSinceLastHeard() const{
	return _contextApi.secondsSinceLastHeard();
	}

Oscl::Qt::DBus::BlueZ::Mesh::Node1::UshortArray	Server::Addresses() const{
	Oscl::Qt::DBus::BlueZ::Mesh::Node1::UshortArray	array;

	_contextApi.iterateAddresses(
		[&] (uint16_t address) {
			array._array.append(address);
			return false;
			}
		);

	return array;
	}

uint32_t	Server::SequenceNumber() const{
	return _contextApi.sequenceNumber();
	}


