/*
   Copyright (C) 2020 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_qt_dbus_bluez_mesh_node1_node1h_
#define _oscl_qt_dbus_bluez_mesh_node1_node1h_

#include <QObject>
#include <QDBusObjectPath>
#include <QByteArray>
#include <QVariantMap>
#include <QDBusContext>
#include "contextapi.h"
#include "ushortarray.h"

/** */
namespace Oscl {
/** */
namespace Qt {
/** */
namespace DBus {
/** */
namespace BlueZ {
/** */
namespace Mesh {
/** */
namespace Node1 {

/** */
class Server : public QObject, protected QDBusContext {
	Q_OBJECT
	Q_CLASSINFO("D-Bus Interface", "org.bluez.mesh.Node1")
	Q_PROPERTY( QVariantMap Features READ Features)
	Q_PROPERTY( bool Beacon READ Beacon)
	Q_PROPERTY( bool IvUpdate READ IvUpdate)
	Q_PROPERTY( uint IvIndex READ IvIndex)
	Q_PROPERTY( uint SecondsSinceLastHeard READ SecondsSinceLastHeard)
	Q_PROPERTY( Oscl::Qt::DBus::BlueZ::Mesh::Node1::UshortArray Addresses READ Addresses)
	Q_PROPERTY( uint SequenceNumber READ SequenceNumber)

	private:
		/** */
		ContextApi&	_contextApi;

	public:
		/** */
		Server(
			ContextApi&	contextApi,
			QObject*	parent	= 0
			);

		/** */
		virtual ~Server();

	public:
		/** */
		QVariantMap	Features() const;

		/** */
		bool	Beacon() const;

		/** */
		bool	IvUpdate() const;

		/** */
		uint	IvIndex() const;

		/** */
		uint	SecondsSinceLastHeard() const;

		/** */
		Oscl::Qt::DBus::BlueZ::Mesh::Node1::UshortArray	Addresses() const;

		/** */
		uint	SequenceNumber() const;

	public slots:
		/** */
		void	Send(
					const QDBusObjectPath&	element_path,
					ushort					destination,
					ushort					key_index,
					const QVariantMap&		options,
					const QByteArray&		data
					);

		/** */
		void	DevKeySend(
					const QDBusObjectPath&	element_path,
					ushort					destination,
					bool					remote,
					ushort					net_index,
					const QVariantMap&		options,
					const QByteArray&		data
					);

		/** */
		void	AddNetKey(
					const QDBusObjectPath&	element_path,
					ushort					destination,
					ushort					subnet_index,
					ushort					net_index,
					bool					update
					);

		/** */
		void	AddAppKey(
					const QDBusObjectPath&	element_path,
					ushort					destination,
					ushort					app_index,
					ushort					net_index,
					bool					update
					);

		/** */
		void	Publish(
					const QDBusObjectPath&	element_path,
					ushort					model,
					const QVariantMap&		options,
					const QByteArray&		data
					);

	};

}
}
}
}
}
}

#endif
