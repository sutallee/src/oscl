#!/usr/bin/bash
#Mesh Node Hierarchy
#===================
#Service		org.bluez.mesh
#Interface	org.bluez.mesh.Node1
#Object path	/org/bluez/mesh/node<uuid>
#		where <uuid> is the Device UUID passed to Join(),
#		CreateNetwork() or Import()
#
#Methods:
#	void Send(object element_path, uint16 destination, uint16 key_index,
#						dict options, array{byte} data)
#
#		This method is used to send a message originated by a local
#		model.
#
#		The element_path parameter is the object path of an element from
#		a collection of the application elements (see Mesh Application
#		Hierarchy section).
#
#		The destination parameter contains the destination address. This
#		destination must be a uint16 to a unicast address, or a well
#		known group address.
#
#		The key_index parameter determines which application key to use
#		for encrypting the message. The key_index must be valid for that
#		element, i.e., the application key must be bound to a model on
#		this element. Otherwise, org.bluez.mesh.Error.NotAuthorized will
#		be returned.
#
#		The options parameter is a dictionary with the following keys
#		defined:
#
#			bool ForceSegmented
#				Specifies whether to force sending of a short
#				message as one-segment payload. If not present,
#				the default setting is "false".
#
#		The data parameter is an outgoing message to be encypted by the
#		bluetooth-meshd daemon and sent on.
#
#		Possible errors:
#			org.bluez.mesh.Error.NotAuthorized
#			org.bluez.mesh.Error.InvalidArguments
#			org.bluez.mesh.Error.NotFound
#
#	void DevKeySend(object element_path, uint16 destination, boolean remote,
#			uint16 net_index, dict options, array{byte} data)
#
#		This method is used to send a message originated by a local
#		model encoded with the device key of the remote node.
#
#		The element_path parameter is the object path of an element from
#		a collection of the application elements (see Mesh Application
#		Hierarchy section).
#
#		The destination parameter contains the destination address. This
#		destination must be a uint16 to a unicast address, or a well
#		known group address.
#
#		The remote parameter, if true, looks up the device key by the
#		destination address in the key database to encrypt the message.
#		If remote is true, but requested key does not exist, a NotFound
#		error will be returned. If set to false, the local node's
#		device key is used.
#
#		The net_index parameter is the subnet index of the network on
#		which the message is to be sent.
#
#		The options parameter is a dictionary with the following keys
#		defined:
#
#			bool ForceSegmented
#				Specifies whether to force sending of a short
#				message as one-segment payload. If not present,
#				the default setting is "false".
#
#		The data parameter is an outgoing message to be encypted by the
#		meshd daemon and sent on.
#
#		Possible errors:
#			org.bluez.mesh.Error.InvalidArguments
#			org.bluez.mesh.Error.NotFound
#
#	void AddNetKey(object element_path, uint16 destination,
#			uint16 subnet_index, uint16 net_index, boolean update)
#
#		This method is used to send add or update network key originated
#		by the local configuration client to a remote configuration
#		server.
#
#		The element_path parameter is the object path of an element from
#		a collection of the application elements (see Mesh Application
#		Hierarchy section).
#
#		The destination parameter contains the destination address. This
#		destination must be a uint16 to a nodes primary unicast address.
#
#		The subnet_index parameter refers to the subnet index of the
#		network that is being added or updated. This key must exist in
#		the local key database.
#
#		The net_index parameter is the subnet index of the network on
#		which the message is to be sent.
#
#		The update parameter indicates if this is an addition or an
#		update. If true, the subnet key must be in the phase 1 state of
#		the key update procedure.
#
#		Possible errors:
#			org.bluez.mesh.Error.InvalidArguments
#			org.bluez.mesh.Error.NotFound
#
#	void AddAppKey(object element_path, uint16 destination,
#			uint16 app_index, uint16 net_index, boolean update)
#
#		This method is used to send add or update network key originated
#		by the local configuration client to a remote configuration
#		server.
#
#		The element_path parameter is the object path of an element from
#		a collection of the application elements (see Mesh Application
#		Hierarchy section).
#
#		The destination parameter contains the destination address. This
#		destination must be a uint16 to a nodes primary unicast address.
#
#		The app_index parameter refers to the application key which is
#		being added or updated. This key must exist in the local key
#		database.
#
#		The net_index parameter is the subnet index of the network on
#		which the message is to be sent.
#
#		The update parameter indicates if this is an addition or an
#		update. If true, the subnet key must be in the phase 1 state of
#		the key update procedure.
#
#		Possible errors:
#			org.bluez.mesh.Error.InvalidArguments
#			org.bluez.mesh.Error.NotFound
#
#	void Publish(object element_path, uint16 model, dict options,
#							array{byte} data)
#
#		This method is used to send a publication originated by a local
#		model. If the model does not exist, or it has no publication
#		record, the method returns org.bluez.mesh.Error.DoesNotExist
#		error.
#
#		The element_path parameter is the object path of an element from
#		a collection of the application elements (see Mesh Application
#		Hierarchy section).
#
#		The model parameter contains a model ID, as defined by the
#		Bluetooth SIG. If the options dictionary contains a "Vendor"
#		key, then this ID is defined by the specified vendor.
#
#		The options parameter is a dictionary with the following keys
#		defined:
#
#			bool ForceSegmented
#				Specifies whether to force sending of a short
#				message as one-segment payload. If not present,
#				the default setting is "false".
#
#			uint16 Vendor
#				A 16-bit Company ID as defined by the
#				Bluetooth SIG. This key should only exist when
#				publishing on a Vendor defined model.
#
#		The data parameter is an outgoing message to be encypted by the
#		meshd daemon and sent on.
#
#		Since only one Publish record may exist per element-model, the
#		destination and key_index are obtained from the Publication
#		record cached by the daemon.
#
#		Possible errors:
#			org.bluez.mesh.Error.DoesNotExist
#			org.bluez.mesh.Error.InvalidArguments
#
#
#Properties:
#	dict Features [read-only]
#
#		The dictionary that contains information about feature support.
#		The following keys are defined:
#
#		boolean Friend
#
#			Indicates the ability to establish a friendship with a
#			Low Power node
#
#		boolean LowPower
#
#			Indicates support for operating in Low Power node mode
#
#		boolean Proxy
#
#			Indicates support for GATT proxy
#
#		boolean Relay
#			Indicates support for relaying messages
#
#	If a key is absent from the dictionary, the feature is not supported.
#	Otherwise, true means that the feature is enabled and false means that
#	the feature is disabled.
#
#	boolean Beacon [read-only]
#
#		This property indicates whether the periodic beaconing is
#		enabled (true) or disabled (false).
#
#	boolean IvUpdate [read-only]
#
#		When true, indicates that the network is in the middle of IV
#		Index Update procedure. This information is only useful for
#		provisioning.
#
#	uint32 IvIndex [read-only]
#
#		This property may be read at any time to determine the IV_Index
#		that the current network is on. This information is only useful
#		for provisioning.
#
#	uint32 SecondsSinceLastHeard [read-only]
#
#		This property may be read at any time to determine the number of
#		seconds since mesh network layer traffic was last detected on
#		this node's network.
#
#	array{uint16} Addresses [read-only]
#
#		This property contains unicast addresses of node's elements.
#
#	uint32 SequenceNumber [read-only]
#
#		This property may be read at any time to determine the
#		sequence number.

echo "Creating C++ Adaptor for Node1"
qdbusxml2cpp -l Oscl::Qt::DBus::BlueZ::Mesh::Node1::Server -i ushortarray.h -i server.h -a adaptor dbus.xml
echo "Creating C++ Proxy for Node1"
qdbusxml2cpp -l Oscl::Qt::DBus::BlueZ::Mesh::Node1::Server -i ushortarray.h -i server.h -p proxy dbus.xml

