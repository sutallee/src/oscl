/*
   Copyright (C) 2020 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_qt_dbus_bluez_mesh_node1_ushortarrayh_
#define _oscl_qt_dbus_bluez_mesh_node1_ushortarrayh_

#include <QtDBus>
#include <QList>

/** */
namespace Oscl {
/** */
namespace Qt {
/** */
namespace DBus {
/** */
namespace BlueZ {
/** */
namespace Mesh {
/** */
namespace Node1 {

/** */
class UshortArray {
	public:
		/** */
		QList<ushort>	_array;

	public:
		/** */
		UshortArray();

		/** */
		UshortArray(const UshortArray &other);

		/** */
		UshortArray& operator=(const UshortArray &other);

		/** */
		~UshortArray();

		//register UshortArray with the Qt type system
		static void registerMetaType();
	};

}
}
}
}
}
}

QDBusArgument&	operator<<(
					QDBusArgument&			argument,
					const Oscl::Qt::DBus::BlueZ::Mesh::Node1::UshortArray&	models
					);

const QDBusArgument&	operator>>(
							const QDBusArgument&	argument,
							Oscl::Qt::DBus::BlueZ::Mesh::Node1::UshortArray&			models
							);

Q_DECLARE_METATYPE(Oscl::Qt::DBus::BlueZ::Mesh::Node1::UshortArray)

#endif

