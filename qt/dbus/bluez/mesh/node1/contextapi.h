/*
   Copyright (C) 2020 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_qt_dbus_bluez_mesh_node1_contextapih_
#define _oscl_qt_dbus_bluez_mesh_node1_contextapih_

#include <functional>

/** */
namespace Oscl {
/** */
namespace Qt {
/** */
namespace DBus {
/** */
namespace BlueZ {
/** */
namespace Mesh {
/** */
namespace Node1 {

/** This interface is designed to satisfy the
	data needs of the Node1 class without
	reference to the D-Bus or Qt in general.
 */
class ContextApi {
	public:
		/**
			org.bluez.mesh.Error.NotAuthorized
			org.bluez.mesh.Error.InvalidArguments
			org.bluez.mesh.Error.NotFound,
		 */
		virtual void	sendReceived(
							const char*	element_path,
							uint16_t	destination,
							uint16_t	key_index,
							bool		forceSegmented,
							unsigned	nData,
							const void*	data
							) noexcept=0;

		/**
			org.bluez.mesh.Error.InvalidArguments
			org.bluez.mesh.Error.NotFound,
		 */
		virtual void	devKeySendReceived(
							const char*	element_path,
							uint16_t	destination,
							bool		remote,
							uint16_t	net_index,
							bool		forceSegmented,
							unsigned	nData,
							const void*	data
							) noexcept=0;

		/**
			org.bluez.mesh.Error.InvalidArguments
			org.bluez.mesh.Error.NotFound,
		 */
		virtual void	addNetKeyReceived(
							const char*	element_path,
							uint16_t	destination,
							uint16_t	subnet_index,
							uint16_t	net_index,
							bool		update
							) noexcept=0;

		/**
			org.bluez.mesh.Error.InvalidArguments
			org.bluez.mesh.Error.NotFound,
		 */
		virtual void	addAppKeyReceived(
							const char*	element_path,
							uint16_t	destination,
							uint16_t	app_index,
							uint16_t	net_index,
							bool		update
							) noexcept=0;

		/**
			org.bluez.mesh.Error.DoesNotExist
			org.bluez.mesh.Error.InvalidArguments
		 */
		virtual void	publishReceived(
							const char*	element_path,
							uint16_t	model,
							bool		forceSegmented,
							bool		vendorValid,
							uint16_t	companyID,
							unsigned	nData,
							const void*	data
							) noexcept=0;

		/** */
		virtual bool	friendFeatureSupported() const noexcept=0;

		/** */
		virtual bool	friendFeatureEnabled() const noexcept=0;

		/** */
		virtual bool	lowPowerFeatureSupported() const noexcept=0;

		/** */
		virtual bool	lowPowerFeatureEnabled() const noexcept=0;

		/** */
		virtual bool	proxyFeatureSupported() const noexcept=0;

		/** */
		virtual bool	proxyFeatureEnabled() const noexcept=0;

		/** */
		virtual bool	relayFeatureSupported() const noexcept=0;

		/** */
		virtual bool	relayFeatureEnabled() const noexcept=0;

		/** */
		virtual bool	beacon() const noexcept=0;

		/** */
		virtual bool	ivUpdate() const noexcept=0;

		/** */
		virtual uint32_t	ivIndex() const noexcept=0;

		/** */
		virtual uint32_t	sequenceNumber() const noexcept=0;

		/** */
		virtual uint32_t	secondsSinceLastHeard() const noexcept=0;

		/** */
		virtual void	iterateAddresses(std::function<bool (uint16_t address)> callback) const = 0;
	};

}
}
}
}
}
}

#endif
