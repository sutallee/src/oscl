#!/usr/bin/bash

# A mesh node element implements the following interface:
# Object path: <app_defined_element_path>
# org.bluez.mesh.Element1
#	void MessageReceived(
#			uint16 source,
#			uint16 key_index,
#			variant destination,
#			array{byte} data
#			)
#	void DevKeyMessageReceived(
#			uint16 source,
#			boolean remote,
#			uint16 net_index,
#			array{byte} data
#			)
#	void UpdateModelConfiguration(
#			uint16 model_id,
#			dict config
#			)
#	uint8 Index [read-only]
#	array{(uint16 id, dict caps)} Models [read-only]
#	array{(uint16 vendor, uint16 id, dict options)} VendorModels [read-only]
#	uint16 Location [read-only, optional]

echo "Creating C++ Adaptor for Element1"
qdbusxml2cpp -i configurationtype.h -i sigmodelstype.h -i vendormodelstype.h -a adaptor dbus.xml
echo "Creating C++ Proxy for Element1"
qdbusxml2cpp -i configurationtype.h -i sigmodelstype.h -i vendormodelstype.h -p proxy dbus.xml

