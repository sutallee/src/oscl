/*
   Copyright (C) 2020 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "vendormodeltype.h"
#define DEBUG_TRACE

using namespace Oscl::Qt::DBus::BlueZ::Mesh::Element1;

VendorModelType::VendorModelType(){
	}

VendorModelType::VendorModelType(const VendorModelType &other) {
	_companyID	= other._companyID;
	_vendorModelID	= other._vendorModelID;
	_options	= other._options;
	}

VendorModelType&	VendorModelType::operator = (const VendorModelType &other) {
	return *this;
	}

VendorModelType::~VendorModelType() {
	}

void	VendorModelType::registerMetaType() {

	qRegisterMetaType<Oscl::Qt::DBus::BlueZ::Mesh::Element1::VendorModelType>("Oscl::Qt::DBus::BlueZ::Mesh::Element1::VendorModelType");

	qDBusRegisterMetaType<Oscl::Qt::DBus::BlueZ::Mesh::Element1::VendorModelType>();
	}

QDBusArgument&	operator << (
					QDBusArgument&				argument,
					const Oscl::Qt::DBus::BlueZ::Mesh::Element1::VendorModelType&	model
					) {

	/*
		(qqa{sv}) - NOTE: The curly braces indicate a dictionary entry {key,value}

		The Vendor Model Structure looks like this:

			vendor - a 16-bit Bluetooth-assigned Company ID as
			defined by Bluetooth SIG.

			id - a 16-bit vendor-assigned Model Identifier

			options - a dictionary that may contain additional model
			info. The following keys are defined:

				boolean Publish - indicates whether the model
					supports publication mechanism

				boolean Subscribe - indicates whether the model
					supports subscription mechanism
	*/

	argument.beginStructure(); {
		argument << model._companyID;
		argument << model._vendorModelID;

		argument << model._options;
		argument.endStructure();
		}

	return argument;
	}

const QDBusArgument&	operator >> (
							const QDBusArgument&	argument,
							Oscl::Qt::DBus::BlueZ::Mesh::Element1::VendorModelType&		model
							) {
	/*
		(qqa{sv}) - NOTE: The curly braces indicate a dictionary entry {key,value}

		The Vendor Model Structure looks like this:

			vendor - a 16-bit Bluetooth-assigned Company ID as
			defined by Bluetooth SIG.

			id - a 16-bit vendor-assigned Model Identifier

			options - a dictionary that may contain additional model
			info. The following keys are defined:

				boolean Publish - indicates whether the model
					supports publication mechanism

				boolean Subscribe - indicates whether the model
					supports subscription mechanism
	*/

	argument.beginStructure(); {
		argument >> model._companyID;
		argument >> model._vendorModelID;
		argument >> model._options;
		argument.endStructure();
		}

	return argument;
	}
