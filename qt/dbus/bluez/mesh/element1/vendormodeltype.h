/*
   Copyright (C) 2020 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_qt_dbus_bluez_mesh_element1_vendormodeltypeh_
#define _oscl_qt_dbus_bluez_mesh_element1_vendormodeltypeh_

#include <QtDBus>
#include <QVariantMap>

/** */
namespace Oscl {
/** */
namespace Qt {
/** */
namespace DBus {
/** */
namespace BlueZ {
/** */
namespace Mesh {
/** */
namespace Element1 {

/** (qqa{sv}) - NOTE: The curly braces indicate a dictionary entry {key,value}

	array{(uint16 vendor, uint16 id, dict options)} VendorModels [read-only]

	The Vendor Model Structure looks like this:

		vendor - a 16-bit Bluetooth-assigned Company ID as
		defined by Bluetooth SIG.

		id - a 16-bit vendor-assigned Model Identifier

		options - a dictionary that may contain additional model
		info. The following keys are defined:

			boolean Publish - indicates whether the model
				supports publication mechanism

			boolean Subscribe - indicates whether the model
				supports subscription mechanism

 */
class VendorModelType {
	public:
		/** */
		ushort		_companyID = 0x0000;

		/** */
		ushort		_vendorModelID = 0x0000;

		/** */
		QVariantMap	_options;
		
	public:
		/** */
		VendorModelType();

		/** */
		VendorModelType(const VendorModelType &other);

		/** */
		VendorModelType& operator=(const VendorModelType &other);

		/** */
		~VendorModelType();

		//register VendorModelType with the Qt type system
		static void registerMetaType();
	};

}
}
}
}
}
}

QDBusArgument&		operator<<(
						QDBusArgument&												argument,
						const Oscl::Qt::DBus::BlueZ::Mesh::Element1::VendorModelType&	model
						);

const QDBusArgument&	operator>>(
						const QDBusArgument&									argument,
						Oscl::Qt::DBus::BlueZ::Mesh::Element1::VendorModelType&	model
						);

Q_DECLARE_METATYPE(Oscl::Qt::DBus::BlueZ::Mesh::Element1::VendorModelType)

#endif

