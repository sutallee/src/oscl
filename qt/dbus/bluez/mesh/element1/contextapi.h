/*
   Copyright (C) 2020 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_qt_dbus_bluez_mesh_element1_contextapih_
#define _oscl_qt_dbus_bluez_mesh_element1_contextapih_

#include <functional>
#include <QtGlobal>

/** */
namespace Oscl {
/** */
namespace Qt {
/** */
namespace DBus {
/** */
namespace BlueZ {
/** */
namespace Mesh {
/** */
namespace Element1 {

/** */
struct SigModel {
	public:
		/** */
		const ushort		_sigModelID;

		/** */
		const bool			_publicationSupported;

		/** */
		const bool			_subscriptionSupported;

	public:
		/** */
		SigModel(
			ushort		sigModelID,
			bool		publicationSupported,
			bool		subscriptionSupported
			):
			_sigModelID(sigModelID),
			_publicationSupported(publicationSupported),
			_subscriptionSupported(subscriptionSupported)
			{}
	};

/** */
struct VirtualLabel {
	public:
		/** */
		uint8_t		_hash[16];

	public:
		/** */
		VirtualLabel(){}

		/** */
		void	set(const uint8_t hash[16]){
					memcpy(_hash,hash,16);
					}
	};

/** */
struct VendorModel {
	public:
		/** */
		const ushort		_companyID;

		/** */
		const ushort		_vendorModelID;

		/** */
		const bool			_publicationSupported;

		/** */
		const bool			_subscriptionSupported;

	public:
		/** */
		VendorModel(
			ushort		companyID,
			ushort		vendorModelID,
			bool		publicationSupported,
			bool		subscriptionSupported
			):
			_companyID(companyID),
			_vendorModelID(vendorModelID),
			_publicationSupported(publicationSupported),
			_subscriptionSupported(subscriptionSupported)
			{}
	};

/** This interface is designed to satisfy the
	data needs of the Element1 class without
	reference to the D-Bus or Qt in general.
 */
class ContextApi {
	public:
		/** Returns the Element Index for this Element
		 */
		virtual uint8_t	getIndex() const noexcept=0;

		/** Used to iterate through all of the SIG Models
			contained within the Element.
			The callback can return true to halt the
			iteration early.
		 */
		virtual void	iterate(std::function<bool (const SigModel& model)> callback) const = 0;

		/** Used to iterate through all of the Vendor Models
			contained within the Element.
			The callback can return true to halt the
			iteration early.
		 */
		virtual void	iterate(std::function<bool (const VendorModel& model)> callback) const = 0;

		/** */
		virtual ushort	getLocation() const noexcept=0;

		/** */
		virtual void	updateModelConfiguration(
							uint16_t		modelID,
							uint16_t		companyID,
							bool			isVendorModel,
							uint32_t		publicationPeriod,
							const uint16_t*	appKeys,
							unsigned		nAppKeys,
							const uint16_t*	subscriptions,
							unsigned		nSubscriptions,
							const VirtualLabel*	virtualSubscriptions,
							unsigned			nVirtualSubscriptions
							) noexcept=0;

		/** */
		virtual	void	devKeyMessageReceived(
							uint16_t			source,
							bool				remote,
							uint16_t			net_index,
							const uint8_t*		message,
							unsigned			messageLength
							) noexcept=0;

		/** */
		virtual void	messageReceived(
							uint16_t			source,
							uint16_t			key_index,
							uint16_t			address,
							const uint8_t*		message,
							unsigned			messageLength
							) noexcept=0;

		/** */
		virtual void	messageReceived(
							uint16_t			source,
							uint16_t			key_index,
							const VirtualLabel&	destination,
							const uint8_t*		message,
							unsigned			messageLength
							) noexcept=0;

	};

}
}
}
}
}
}

#endif
