/*
   Copyright (C) 2020 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "server.h"
#include <stdio.h>
#include <QVariant>

using namespace Oscl::Qt::DBus::BlueZ::Mesh::Element1;

Server::Server(
	ContextApi&	contextApi,
	QObject*	parent
	) :
		QObject(parent),
		_contextApi(contextApi)
		{
	}

Server::~Server() {
	}

void	Server::DevKeyMessageReceived(
			ushort				source,
			bool				remote,
			ushort				net_index,
			const QByteArray&	data
			){

	const uint8_t*  p   = (const uint8_t*)data.constData();
	const int   	count   = data.count();

	_contextApi.devKeyMessageReceived(
		source,
		remote,
		net_index,
		p,
		count
		);

	fflush(stdout);
	}

void	Server::MessageReceived(
			ushort				source,
			ushort				key_index,
			const QDBusVariant&	destination,
			const QByteArray&	data
			){

	const uint8_t*  p   = (const uint8_t*)data.constData();
	const int   	count   = data.count();

	QVariant	var	= destination.variant();

	int	type	= var.type();

	switch(type){
		case QMetaType::UShort:
			_contextApi.messageReceived(
				source,
				key_index,
				var.value<ushort>(),
				p,
				count
				);
			break;
		case QMetaType::QByteArray:
			{
			QByteArray	ba	= var.value<QByteArray>();
			VirtualLabel	label;
			if(ba.count() != 16){
				printf(
					"%s: unexpected virtual label length: %d\n",
					__PRETTY_FUNCTION__,
					ba.count()
					);
				sendErrorReply(
					QDBusError::InvalidArgs,
					"Invalid destination label length."
					);
				return;
				}
			label.set((const uint8_t*)ba.constData());
			_contextApi.messageReceived(
				source,
				key_index,
				label,
				p,
				count
				);
			}
			break;
		default:
			printf(
				"%s: unknown type: %d, typeName: %s\n",
				__PRETTY_FUNCTION__,
				type,
				var.typeName()
				);

			sendErrorReply(
				QDBusError::InvalidArgs,
				"Invalid destination variant type."
				);

		}

	fflush(stdout);
	}

void	Server::UpdateModelConfiguration(
			ushort				model_id,
			Oscl::Qt::DBus::BlueZ::Mesh::Element1::ConfigurationType	config
			){

	constexpr int	maxAppKeys	= 16;
	uint16_t	appKeys[maxAppKeys];
	int	nAppKeys;
	for(
			nAppKeys=0;
			nAppKeys < config._bindings.count() && nAppKeys < maxAppKeys;
			++nAppKeys
			){
		appKeys[nAppKeys]	= config._bindings.at(nAppKeys);
		}

	constexpr int	maxSubscriptions	= 16;
	uint16_t	subscriptions[maxSubscriptions];
	int			nSubscriptions	= 0;

	for(
			nSubscriptions=0;
			nSubscriptions < config._subscriptions.count() && nSubscriptions < maxSubscriptions;
			++nSubscriptions
			){
		subscriptions[nSubscriptions]	= config._subscriptions.at(nSubscriptions);
		}

	constexpr int	maxVirtualSubscriptions	= 16;
	VirtualLabel	virtualSubscriptions[maxVirtualSubscriptions];
	int				nVirtualSubscriptions	= 0;

	for(
			nVirtualSubscriptions=0;
			nVirtualSubscriptions < config._virtualSubscriptions.count() && nVirtualSubscriptions < maxVirtualSubscriptions;
			++nVirtualSubscriptions
			){
		const uint8_t*	p	= (const uint8_t*)config._virtualSubscriptions.at(nVirtualSubscriptions).constData();
		const int	count	= config._virtualSubscriptions.at(nVirtualSubscriptions).count();
		if(count != 16){ // size of hash
			printf("%s: [%d] count(%d) != 16\n",__PRETTY_FUNCTION__,nVirtualSubscriptions,count);fflush(stdout);
			sendErrorReply(
				QDBusError::InvalidArgs,
				"Invalid subscription label length."
				);
			return;
			}
		virtualSubscriptions[nVirtualSubscriptions].set(p);
		}

	_contextApi.updateModelConfiguration(
					model_id,
					config._companyID,
					config._isVendorModel,
					config._publicationPeriod,
					appKeys,	// const uint16_t*	appKeys,
					nAppKeys,	// unsigned		nAppKeys,
					subscriptions,	// const uint16_t*	subscriptions,
					nSubscriptions,
					virtualSubscriptions,
					nVirtualSubscriptions
					);

	fflush(stdout);
	}

uint8_t			Server::Index() const{
	return _contextApi.getIndex(); 
	}

Oscl::Qt::DBus::BlueZ::Mesh::Element1::SigModelsType	Server::Models() const{

	Oscl::Qt::DBus::BlueZ::Mesh::Element1::SigModelsType	models;

	_contextApi.iterate(
		[&] (const SigModel& model) {

			Oscl::Qt::DBus::BlueZ::Mesh::Element1::SigModelType	sigmodel;

			sigmodel._sigModelID	= model._sigModelID;

			QVariant	publicationVariant(model._publicationSupported);
			sigmodel._caps.insert("Publish",publicationVariant);

			QVariant	subscriptionVariant(model._subscriptionSupported);
			sigmodel._caps.insert("Subscribe",subscriptionVariant);

			models._modelList.append(sigmodel);

			return false;
			}
		);

	return models;
	}

Oscl::Qt::DBus::BlueZ::Mesh::Element1::VendorModelsType	Server::VendorModels() const{

	Oscl::Qt::DBus::BlueZ::Mesh::Element1::VendorModelsType	models;

	_contextApi.iterate(
		[&] (const VendorModel& model) {

			Oscl::Qt::DBus::BlueZ::Mesh::Element1::VendorModelType vendormodel;

			vendormodel._companyID		= model._companyID;
			vendormodel._vendorModelID	= model._vendorModelID;

			QVariant	publicationVariant(model._publicationSupported);
			vendormodel._options.insert("Publish",publicationVariant);

			QVariant	subscriptionVariant(model._subscriptionSupported);
			vendormodel._options.insert("Subscribe",subscriptionVariant);

			models._modelList.append(vendormodel);

			return false;
			}
		);

	return models;
	}

ushort		Server::Location() const{
	return _contextApi.getLocation();
	}

