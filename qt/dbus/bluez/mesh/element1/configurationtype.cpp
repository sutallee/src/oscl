/*
   Copyright (C) 2020 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "configurationtype.h"

using namespace Oscl::Qt::DBus::BlueZ::Mesh::Element1;

ConfigurationType::ConfigurationType():
		_publicationPeriod(0),
		_companyID(0),
		_isVendorModel(false)
		{
	}

ConfigurationType::ConfigurationType(const ConfigurationType& other)
		{
	_bindings				= other._bindings;
	_publicationPeriod		= other._publicationPeriod;
	_companyID				= other._companyID;
	_isVendorModel			= other._isVendorModel;
	_subscriptions			= other._subscriptions;
	_virtualSubscriptions	= other._virtualSubscriptions;
	}

ConfigurationType&	ConfigurationType::operator = (const ConfigurationType& other) {
	_bindings				= other._bindings;
	_publicationPeriod		= other._publicationPeriod;
	_companyID				= other._companyID;
	_isVendorModel			= other._isVendorModel;
	_subscriptions			= other._subscriptions;
	_virtualSubscriptions	= other._virtualSubscriptions;
	return *this;
	}

ConfigurationType::~ConfigurationType() {
	}

void	ConfigurationType::registerMetaType() {

	qRegisterMetaType<Oscl::Qt::DBus::BlueZ::Mesh::Element1::ConfigurationType>("Oscl::Qt::DBus::BlueZ::Mesh::Element1::ConfigurationType");

	qDBusRegisterMetaType<Oscl::Qt::DBus::BlueZ::Mesh::Element1::ConfigurationType>();
	}

QDBusArgument&	operator << (
					QDBusArgument&			argument,
					const Oscl::Qt::DBus::BlueZ::Mesh::Element1::ConfigurationType&	config
					) {
	// "a{sv}" - NOTE: The curly braces indicate a dictionary entry {key,value}
	/*
		The config parameter is a dictionary with the following keys
		defined:

		array{uint16} Bindings

			Indices of application keys bound to the model

		uint32 PublicationPeriod

			Model publication period in milliseconds

		uint16 Vendor

			A 16-bit Bluetooth-assigned Company Identifier of the
			vendor as defined by Bluetooth SIG

		array{variant} Subscriptions

			Addresses the model is subscribed to.

			Each address is provided either as uint16 for group
			addresses, or as array{byte} for virtual labels.
	*/

	/*	ConfigurationType Member variables
		QList<ushort>			_bindings;
		uint32_t				_publicationPeriod;
		QList<ushort>			_subscriptions;
		QList<QByteArray>		_virtualSubscriptions;
	 */

	QVariantMap		map;

	map.insert(
		"PublicationPeriod",
		QVariant::fromValue(config._publicationPeriod)
		);

	map.insert(
		"Bindings",
		QVariant::fromValue(config._bindings)
		);

	if(config._isVendorModel){
		map.insert(
			"Vendor",
			QVariant::fromValue(config._companyID)
			);
		}

	QList<QVariant>	vList;

	int
	count	= config._subscriptions.count();
	for(int i=0;i<count;++i){
		vList.append(QVariant::fromValue(config._subscriptions.at(i)));
		}

	count	= config._virtualSubscriptions.count();
	for(int i=0;i<count;++i){
		vList.append(QVariant::fromValue(config._virtualSubscriptions.at(i)));
		}

	map.insert(
		"Subscriptions",
		QVariant::fromValue(vList)
		);

	argument << map;

	return argument;
	}

const QDBusArgument&	operator >> (
							const QDBusArgument&	argument,
							Oscl::Qt::DBus::BlueZ::Mesh::Element1::ConfigurationType&			config
							) {
	// "a{sv}" - NOTE: The curly braces indicate a dictionary entry {key,value}

	/*
		The config parameter is a dictionary with the following keys
		defined:

		array{uint16} Bindings

			Indices of application keys bound to the model

		uint32 PublicationPeriod

			Model publication period in milliseconds

		uint16 Vendor

			A 16-bit Bluetooth-assigned Company Identifier of the
			vendor as defined by Bluetooth SIG

		array{variant} Subscriptions

			Addresses the model is subscribed to.

			Each address is provided either as uint16 for group
			addresses, or as array{byte} for virtual labels.
	*/

	/*	ConfigurationType Member variables
		QList<ushort>			_bindings;
		uint32_t				_publicationPeriod;
		QList<ushort>			_subscriptions;
		QList<QByteArray>		_virtualSubscriptions;
	 */

	QMap<QString,QVariant>	map;
	argument >> map;

	QMap<QString,QVariant>::iterator i;
	for(
			i = map.begin();
			i != map.end();
			++i
			){
		if(i.key() == "Bindings"){
			QDBusArgument	arg	= i.value().value<QDBusArgument>();
			arg	>> config._bindings;
			}
		else if(i.key() == "PublicationPeriod"){
			config._publicationPeriod	= i.value().value<uint32_t>();
			}
		else if(i.key() == "Vendor"){
			config._companyID		= i.value().value<uint16_t>();
			config._isVendorModel	= true;
			}
		else if(i.key() == "Subscriptions"){
			QDBusArgument	arg	= i.value().value<QDBusArgument>();

			QList<QVariant>	subscriptions;
			arg	>> subscriptions;

			QList<QVariant>::iterator j;

			for(int j=0;j<subscriptions.size();++j){
				int	type	= subscriptions.at(j).type();
				switch(type){
					case QMetaType::UShort:
						config._subscriptions.append(subscriptions.at(j).value<ushort>());
						break;
					case QMetaType::QByteArray:
						config._virtualSubscriptions.append(subscriptions.at(j).value<QByteArray>());
						break;
					default:
						printf("Unsupported type: %d\n",type);
					}
				}

			}
		}

	return argument;
	}

