/*
   Copyright (C) 2020 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "sigmodelstype.h"

using namespace Oscl::Qt::DBus::BlueZ::Mesh::Element1;

//#define DEBUG_TRACE

SigModelsType::SigModelsType() {
	}

SigModelsType::SigModelsType(const SigModelsType &other) {
	_modelList	= other._modelList;

	fflush(stdout);
	}

SigModelsType&	SigModelsType::operator = (const SigModelsType &other) {
	_modelList	= other._modelList;

	return *this;
	}

SigModelsType::~SigModelsType() {
	}

void	SigModelsType::registerMetaType() {

	qRegisterMetaType<Oscl::Qt::DBus::BlueZ::Mesh::Element1::SigModelsType>("Oscl::Qt::DBus::BlueZ::Mesh::Element1::SigModelsType");

	qDBusRegisterMetaType<Oscl::Qt::DBus::BlueZ::Mesh::Element1::SigModelsType>();

	}

QDBusArgument&	operator << (
					QDBusArgument&			argument,
					const Oscl::Qt::DBus::BlueZ::Mesh::Element1::SigModelsType&	models
					) {

	/* a(qa{sv}) - NOTE: The curly braces indicate a dictionary entry {key,value}

	The "a(qa{sv})" signature can be broken down into an
	array of a custom structure SigModelType that contains
	a 'q' field for the sig-model-ID and a QVariantMap with
	the signature "a{sv}".

	The new SigModelType will have a custom marshalling/unmarshalling
	function "<<".

	stuct SigModelType {
		ushort		_sigModelID;
		QVariantMap	_caps;	// a.k.a. options
		};

	QDBusArgument&	operator << (QDBusArgument& argument, const SigModelType& model);
	QDBusArgument&	operator >> (const QDBusArgument& argument, SigModelType& model);

	array{(uint16 id, dict caps)} Models [read-only]

		An array of SIG Models:

			id - SIG Model Identifier

			options - a dictionary that may contain additional model
			info. The following keys are defined:

				boolean Publish - indicates whether the model
					supports publication mechanism. If not
					present, publication is enabled.

				boolean Subscribe - indicates whether the model
					supports subscription mechanism. If not
					present, subscriptons are enabled.

		The array may be empty.
	*/

	argument << models._modelList;

	return argument;
	}

const QDBusArgument&	operator >> (
							const QDBusArgument&	argument,
							Oscl::Qt::DBus::BlueZ::Mesh::Element1::SigModelsType&			models
							) {
	/* a(qa{sv}) - NOTE: The curly braces indicate a dictionary entry {key,value}
	array{(uint16 id, dict caps)} Models [read-only]

		An array of SIG Models:

			id - SIG Model Identifier

			options - a dictionary that may contain additional model
			info. The following keys are defined:

				boolean Publish - indicates whether the model
					supports publication mechanism. If not
					present, publication is enabled.

				boolean Subscribe - indicates whether the model
					supports subscription mechanism. If not
					present, subscriptons are enabled.

		The array may be empty.
	*/

	argument.beginArray(); {
		while ( !argument.atEnd() ) {
			Oscl::Qt::DBus::BlueZ::Mesh::Element1::SigModelType	model;
			argument >> model;
			models._modelList.append(model);
			}
		argument.endArray();
		}

	return argument;
	}
