/*
   Copyright (C) 2020 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_qt_dbus_bluez_mesh_element1_configurationtypeh_
#define _oscl_qt_dbus_bluez_mesh_element1_configurationtypeh_

#include <QtDBus>
#include <QMap>

/** */
namespace Oscl {
/** */
namespace Qt {
/** */
namespace DBus {
/** */
namespace BlueZ {
/** */
namespace Mesh {
/** */
namespace Element1 {

/**	"a{sv}" - NOTE: The curly braces indicate a dictionary entry {key,value}
	The config parameter is a dictionary with the following keys
	defined:

	array{uint16} Bindings

		Indices of application keys bound to the model

	uint32 PublicationPeriod

		Model publication period in milliseconds

	uint16 Vendor

		A 16-bit Bluetooth-assigned Company Identifier of the
		vendor as defined by Bluetooth SIG

	array{variant} Subscriptions

		Addresses the model is subscribed to.

		Each address is provided either as uint16 for group
		addresses, or as array{byte} for virtual labels.
 */
class ConfigurationType {
	public:
		/** */
		QList<ushort>			_bindings;

		/** */
		uint32_t				_publicationPeriod;

		/** */
		uint16_t				_companyID;

		/** */
		bool					_isVendorModel;

		/** */
		QList<ushort>			_subscriptions;

		/** */
		QList<QByteArray>		_virtualSubscriptions;

	public:
		/** */
		ConfigurationType();

		/** */
		ConfigurationType(const ConfigurationType& other);

		/** */
		ConfigurationType& operator=(const ConfigurationType& other);

		/** */
		~ConfigurationType();

		//register ConfigurationType with the Qt type system
		static void registerMetaType();
	};

}
}
}
}
}
}

QDBusArgument&	operator<<(
					QDBusArgument&			argument,
					const Oscl::Qt::DBus::BlueZ::Mesh::Element1::ConfigurationType&	config
					);

const QDBusArgument&	operator>>(
							const QDBusArgument&	argument,
							Oscl::Qt::DBus::BlueZ::Mesh::Element1::ConfigurationType&			config
							);

Q_DECLARE_METATYPE(Oscl::Qt::DBus::BlueZ::Mesh::Element1::ConfigurationType)

#endif

