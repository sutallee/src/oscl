/*
   Copyright (C) 2020 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_qt_dbus_bluez_mesh_element1_element1h_
#define _oscl_qt_dbus_bluez_mesh_element1_element1h_

#include <QObject>
#include <QDBusVariant>

#include "oscl/qt/dbus/bluez/mesh/element1/configurationtype.h"
#include "oscl/qt/dbus/bluez/mesh/element1/sigmodelstype.h"
#include "oscl/qt/dbus/bluez/mesh/element1/vendormodelstype.h"
#include "contextapi.h"

/*
Mesh Element Hierarchy
======================
Service		unique name
Interface	org.bluez.mesh.Element1
Object path	<app_defined_element_path>

Methods:
	void MessageReceived(uint16 source, uint16 key_index,
					variant destination, array{byte} data)

		This method is called by bluetooth-meshd daemon when a message
		arrives addressed to the application.

		The source parameter is unicast address of the remote
		node-element that sent the message.

		The key_index parameter indicates which application key has been
		used to decode the incoming message. The same key_index should
		be used by the application when sending a response to this
		message (in case a response is expected).

		The destination parameter contains the destination address of
		received message. Underlying variant types are:

		uint16

			Destination is an unicast address, or a well known
			group address

		array{byte}

			Destination is a virtual address label

		The data parameter is the incoming message.

	void DevKeyMessageReceived(uint16 source, boolean remote,
					uint16 net_index, array{byte} data)

		This method is called by meshd daemon when a message arrives
		addressed to the application, which was sent with the remote
		node's device key.

		The source parameter is unicast address of the remote
		node-element that sent the message.

		The remote parameter if true indicates that the device key
		used to decrypt the message was from the sender. False
		indicates that the local nodes device key was used, and the
		message has permissions to modify local states.

		The net_index parameter indicates what subnet the message was
		received on, and if a response is required, the same subnet
		must be used to send the response.

		The data parameter is the incoming message.

	void UpdateModelConfiguration(uint16 model_id, dict config)

		This method is called by bluetooth-meshd daemon when a model's
		configuration is updated.

		The model_id parameter contains BT SIG Model Identifier or, if
		Vendor key is present in config dictionary, a 16-bit
		vendor-assigned Model Identifier.

		The config parameter is a dictionary with the following keys
		defined:

		array{uint16} Bindings

			Indices of application keys bound to the model

		uint32 PublicationPeriod

			Model publication period in milliseconds

		uint16 Vendor

			A 16-bit Bluetooth-assigned Company Identifier of the
			vendor as defined by Bluetooth SIG

		array{variant} Subscriptions

			Addresses the model is subscribed to.

			Each address is provided either as uint16 for group
			addresses, or as array{byte} for virtual labels.

Properties:
	uint8 Index [read-only]

		Element index. It is required that the application follows
		sequential numbering scheme for the elements, starting with 0.

	array{(uint16 id, dict caps)} Models [read-only]

		An array of SIG Models:

			id - SIG Model Identifier

			options - a dictionary that may contain additional model
			info. The following keys are defined:

				boolean Publish - indicates whether the model
					supports publication mechanism. If not
					present, publication is enabled.

				boolean Subscribe - indicates whether the model
					supports subscription mechanism. If not
					present, subscriptons are enabled.

		The array may be empty.


	array{(uint16 vendor, uint16 id, dict options)} VendorModels [read-only]

		An array of Vendor Models:

			vendor - a 16-bit Bluetooth-assigned Company ID as
			defined by Bluetooth SIG.

			id - a 16-bit vendor-assigned Model Identifier

			options - a dictionary that may contain additional model
			info. The following keys are defined:

				boolean Publish - indicates whether the model
					supports publication mechanism

				boolean Subscribe - indicates whether the model
					supports subscription mechanism

		The array may be empty.

	uint16 Location [read-only, optional]

		Location descriptor as defined in the GATT Bluetooth Namespace
		Descriptors section of the Bluetooth SIG Assigned Numbers

 */

/** */
namespace Oscl {
/** */
namespace Qt {
/** */
namespace DBus {
/** */
namespace BlueZ {
/** */
namespace Mesh {
/** */
namespace Element1 {

/** */
class Server : public QObject , protected QDBusContext {
	Q_OBJECT
	Q_CLASSINFO("D-Bus Interface", "org.bluez.mesh.Element1")
	Q_PROPERTY( uchar Index READ Index)
	Q_PROPERTY( Oscl::Qt::DBus::BlueZ::Mesh::Element1::SigModelsType Models READ Models)
	Q_PROPERTY( Oscl::Qt::DBus::BlueZ::Mesh::Element1::VendorModelsType VendorModels READ VendorModels)
	Q_PROPERTY( ushort Location READ Location)

	private:
		/** */
		ContextApi&	_contextApi;

	public:
		Server(
			ContextApi&	contextApi,
			QObject*	parent = 0
			);
		virtual ~Server();

		uint8_t				Index() const;
		Oscl::Qt::DBus::BlueZ::Mesh::Element1::SigModelsType		Models() const;
		Oscl::Qt::DBus::BlueZ::Mesh::Element1::VendorModelsType	VendorModels() const;
		ushort				Location() const;

	public slots:
		void	DevKeyMessageReceived(
					ushort				source,
					bool				remote,
					ushort				net_index,
					const QByteArray&	data
					);

		void	MessageReceived(
					ushort				source,
					ushort				key_index,
					const QDBusVariant&	destination,
					const QByteArray&	data
					);

		void	UpdateModelConfiguration(
					ushort				model_id,
					Oscl::Qt::DBus::BlueZ::Mesh::Element1::ConfigurationType	config
					);
	};

}
}
}
}
}
}

#endif
