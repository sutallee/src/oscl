/*
   Copyright (C) 2020 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "configurationtype.h"
//#define DEBUG_TRACE

using namespace Oscl::Qt::DBus::BlueZ::Mesh::Network1;

ConfigurationType::ConfigurationType(){
	}

ConfigurationType::ConfigurationType(const ConfigurationType &other) {
	_elementIndex	= other._elementIndex;
	_modelsList		= other._modelsList;
	}

ConfigurationType&	ConfigurationType::operator = (const ConfigurationType &other) {
	_elementIndex	= other._elementIndex;
	_modelsList		= other._modelsList;
	return *this;
	}

ConfigurationType::~ConfigurationType() {
	}

void	ConfigurationType::registerMetaType() {

	qRegisterMetaType<Oscl::Qt::DBus::BlueZ::Mesh::Network1::ConfigurationType>("Oscl::Qt::DBus::BlueZ::Mesh::Network1::ConfigurationType");

	qDBusRegisterMetaType<Oscl::Qt::DBus::BlueZ::Mesh::Network1::ConfigurationType>();

	ConfigModelsType::registerMetaType();
	}

QDBusArgument&	operator << (
					QDBusArgument&				argument,
					const Oscl::Qt::DBus::BlueZ::Mesh::Network1::ConfigurationType&	config
					) {

	/* "(ya(qa{sv}))"
		object node, array{byte, array{(uint16, dict)}} configuration
				Attach(object app_root, uint64 token)

		The Configuration Structures:

		byte

			Element index, identifies the element to which this
			configuration entry pertains.

		array{struct}	array{"ConfigModelsType"}

			Models array where each entry is a structure with the
			following members:

			uint16

				Either a SIG Model Identifier or, if Vendor key
				is present in model configuration dictionary, a
				16-bit vendor-assigned Model Identifier

			dict

				A dictionary that contains model configuration
				with the following keys defined:

				array{uint16} Bindings

					Indices of application keys bound to the
					model

				uint32 PublicationPeriod

					Model publication period in milliseconds

				uint16 Vendor

					A 16-bit Company ID as defined by the
					Bluetooth SIG

				array{variant} Subscriptions

					Addresses the model is subscribed to.

					Each address is provided either as
					uint16 for group addresses, or
					as array{byte} for virtual labels.
	*/

	/* "(ya(qa{sv}))" */
	argument.beginStructure(); {

		argument << config._elementIndex;

		argument << config._modelsList;

		argument.endStructure();
		}

	return argument;
	}

const QDBusArgument&	operator >> (
							const QDBusArgument&	argument,
							Oscl::Qt::DBus::BlueZ::Mesh::Network1::ConfigurationType&	config
							) {

	argument.beginStructure(); {

		argument >> config._elementIndex;

		argument.beginArray(); {
			while ( !argument.atEnd() ) {
				Oscl::Qt::DBus::BlueZ::Mesh::Network1::ConfigModelsType	models;
				argument >> models;
				config._modelsList.append(models);
				}
			argument.endArray();
			}

		argument.endStructure();
		}

	return argument;
	}

