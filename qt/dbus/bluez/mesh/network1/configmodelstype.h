/*
   Copyright (C) 2020 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_qt_dbus_bluez_mesh_network1_configmodelstypeh_
#define _oscl_qt_dbus_bluez_mesh_network1_configmodelstypeh_

#include <QtDBus>
#include <QList>
//#include "configmodeltype.h"

/** */
namespace Oscl {
/** */
namespace Qt {
/** */
namespace DBus {
/** */
namespace BlueZ {
/** */
namespace Mesh {
/** */
namespace Network1 {

/**	
	{struct} "(qa{sv})"

			Models array where each entry is a structure with the
			following members:

			uint16	'q'

				Either a SIG Model Identifier or, if Vendor key
				is present in model configuration dictionary, a
				16-bit vendor-assigned Model Identifier

			dict	"a{sv}"

				A dictionary that contains model configuration
				with the following keys defined:

				array{uint16} Bindings

					Indices of application keys bound to the
					model

				uint32 PublicationPeriod

					Model publication period in milliseconds

				uint16 Vendor

					A 16-bit Company ID as defined by the
					Bluetooth SIG

				array{variant} Subscriptions

					Addresses the model is subscribed to.

					Each address is provided either as
					uint16 for group addresses, or
					as array{byte} for virtual labels.
 */
class ConfigModelsType {
	public:
		/** */
		ushort		_modelID	= 0x0000;

		/** a.k.a. options */
		QVariantMap	_dict;

	public:
		/** */
		ConfigModelsType();

		/** */
		ConfigModelsType(const ConfigModelsType &other);

		/** */
		ConfigModelsType& operator=(const ConfigModelsType &other);

		/** */
		~ConfigModelsType();

		//register ConfigModelsType with the Qt type system
		static void registerMetaType();
	};

}
}
}
}
}
}

QDBusArgument&	operator<<(
							QDBusArgument&			argument,
							const Oscl::Qt::DBus::BlueZ::Mesh::Network1::ConfigModelsType&	models
							);

const QDBusArgument&	operator>>(
							const QDBusArgument&	argument,
							Oscl::Qt::DBus::BlueZ::Mesh::Network1::ConfigModelsType&			models
							);

Q_DECLARE_METATYPE(Oscl::Qt::DBus::BlueZ::Mesh::Network1::ConfigModelsType)

#endif

