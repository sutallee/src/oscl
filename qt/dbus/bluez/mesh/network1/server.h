/*
   Copyright (C) 2020 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/


#ifndef _oscl_qt_dbus_bluez_mesh_network1_serverh_
#define _oscl_qt_dbus_bluez_mesh_network1_serverh_

#include <QObject>
#include <QDBusObjectPath>
#include <QByteArray>
#include <QVariantMap>
#include "configurationstype.h"
#include "contextapi.h"

/** */
namespace Oscl {
/** */
namespace Qt {
/** */
namespace DBus {
/** */
namespace BlueZ {
/** */
namespace Mesh {
/** */
namespace Network1 {

/** */
class Server : public QObject, protected QDBusContext {
	Q_OBJECT
	Q_CLASSINFO("D-Bus Interface", "org.bluez.mesh.Network1")

	private:
		/** */
		ContextApi&	_contextApi;

	public:
		/** */
		Server(
			ContextApi&	contextApi,
			QObject*	parent	= 0
			);

		/** */
		virtual ~Server();

	public slots:
		/** */
		QDBusObjectPath Attach(const QDBusObjectPath &app_root, qulonglong token, Oscl::Qt::DBus::BlueZ::Mesh::Network1::ConfigurationsType &configuration);

		/** */
		void Cancel();

		/** */
		void CreateNetwork(const QDBusObjectPath &app_root, const QByteArray &uuid);

		/** */
		void Import(const QDBusObjectPath &app_root, const QByteArray &uuid, const QByteArray &dev_key, const QByteArray &net_key, ushort net_index, const QVariantMap &flags, uint iv_index, ushort unicast);

		/** */
		void Join(const QDBusObjectPath &app_root, const QByteArray &uuid);

		/** */
		void Leave(qulonglong token);
	};

}
}
}
}
}
}

#endif
