/*
   Copyright (C) 2020 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_qt_dbus_bluez_mesh_network1_sigmodeltypeh_
#define _oscl_qt_dbus_bluez_mesh_network1_sigmodeltypeh_

#include <QtDBus>
#include <QVariantMap>
#include "configmodelstype.h"

/** */
namespace Oscl {
/** */
namespace Qt {
/** */
namespace DBus {
/** */
namespace BlueZ {
/** */
namespace Mesh {
/** */
namespace Network1 {

/**
	This class is the D-Bus representation of a user
	defined Qt Meta Type structure that is responsible
	for marshalling/un-marshalling this portion of
	a D-Bus message for the Models property.

	"(ya(qa{sv}))"
		object node, array{byte, array{(uint16, dict)}} configuration
				Attach(object app_root, uint64 token)

		The Configuration Structure:

		byte	'y'

			Element index, identifies the element to which this
			configuration entry pertains.

		array{struct} "a(qa{sv})"

			Models array where each entry is a structure with the
			following members:

			uint16	'q'

				Either a SIG Model Identifier or, if Vendor key
				is present in model configuration dictionary, a
				16-bit vendor-assigned Model Identifier

			dict	"a{sv}"

				A dictionary that contains model configuration
				with the following keys defined:

				array{uint16} Bindings

					Indices of application keys bound to the
					model

				uint32 PublicationPeriod

					Model publication period in milliseconds

				uint16 Vendor

					A 16-bit Company ID as defined by the
					Bluetooth SIG

				array{variant} Subscriptions

					Addresses the model is subscribed to.

					Each address is provided either as
					uint16 for group addresses, or
					as array{byte} for virtual labels.
	*/
class ConfigurationType {
	public:
		/**
			e.g. Configuration Server Model (0x0000)
		 */
		uint8_t		_elementIndex = 0x00;

		/** a.k.a. options */
		QList<Oscl::Qt::DBus::BlueZ::Mesh::Network1::ConfigModelsType>	_modelsList;
		
	public:
		/** */
		ConfigurationType();

		/** */
		ConfigurationType(const ConfigurationType &other);

		/** */
		ConfigurationType& operator=(const ConfigurationType &other);

		/** */
		~ConfigurationType();

		//register ConfigurationType with the Qt type system
		static void registerMetaType();
	};

}
}
}
}
}
}

QDBusArgument&		operator<<(
						QDBusArgument&												argument,
						const Oscl::Qt::DBus::BlueZ::Mesh::Network1::ConfigurationType&	model
						);

const QDBusArgument&	operator>>(
						const QDBusArgument&									argument,
						Oscl::Qt::DBus::BlueZ::Mesh::Network1::ConfigurationType&	model
						);

Q_DECLARE_METATYPE(Oscl::Qt::DBus::BlueZ::Mesh::Network1::ConfigurationType)


#endif

