/*
   Copyright (C) 2020 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "configurationstype.h"

using namespace Oscl::Qt::DBus::BlueZ::Mesh::Network1;

//#define DEBUG_TRACE

ConfigurationsType::ConfigurationsType() {
	#ifdef DEBUG_TRACE
	printf("%s\n",__PRETTY_FUNCTION__);
	fflush(stdout);
	#endif
	}

ConfigurationsType::ConfigurationsType(const ConfigurationsType &other) {
	_configurationList	= other._configurationList;

	#ifdef DEBUG_TRACE
	printf(
		"%s: [%p]._configurationList.count: %d, other[%p]._configurationList.count: %d\n",
		__PRETTY_FUNCTION__,
		this,
		_configurationList.count(),
		&other,
		other._configurationList.count()
		);
	#endif

	fflush(stdout);
	}

ConfigurationsType&	ConfigurationsType::operator = (const ConfigurationsType &other) {
	_configurationList	= other._configurationList;

	#ifdef DEBUG_TRACE
	printf(
		"%s: [%p]._configurationList.count: %d, other[%p]._configurationList.count: %d\n",
		__PRETTY_FUNCTION__,
		this,
		_configurationList.count(),
		&other,
		other._configurationList.count()
		);
	fflush(stdout);
	#endif

	return *this;
	}

ConfigurationsType::~ConfigurationsType() {
	#ifdef DEBUG_TRACE
	printf("%s\n",__PRETTY_FUNCTION__);
	fflush(stdout);
	#endif
	}

void	ConfigurationsType::registerMetaType() {

	qRegisterMetaType<Oscl::Qt::DBus::BlueZ::Mesh::Network1::ConfigurationsType>("Oscl::Qt::DBus::BlueZ::Mesh::Network1::ConfigurationsType");

	qDBusRegisterMetaType<Oscl::Qt::DBus::BlueZ::Mesh::Network1::ConfigurationsType>();

	ConfigurationType::registerMetaType();
	}

QDBusArgument&	operator << (
					QDBusArgument&			argument,
					const Oscl::Qt::DBus::BlueZ::Mesh::Network1::ConfigurationsType&	configs
					) {

	/* "a(ya(qa{sv}))"
		object node, array{byte, array{(uint16, dict)}} configuration
				Attach(object app_root, uint64 token)

		An array of Configuration Structures:

		array{struct}

			Models array where each entry is a structure with the
			following members:

			uint16

				Either a SIG Model Identifier or, if Vendor key
				is present in model configuration dictionary, a
				16-bit vendor-assigned Model Identifier

			dict

				A dictionary that contains model configuration
				with the following keys defined:

				array{uint16} Bindings

					Indices of application keys bound to the
					model

				uint32 PublicationPeriod

					Model publication period in milliseconds

				uint16 Vendor

					A 16-bit Company ID as defined by the
					Bluetooth SIG

				array{variant} Subscriptions

					Addresses the model is subscribed to.

					Each address is provided either as
					uint16 for group addresses, or
					as array{byte} for virtual labels.
	*/

	argument << configs._configurationList;

	return argument;
	}

const QDBusArgument&	operator >> (
							const QDBusArgument&	argument,
							Oscl::Qt::DBus::BlueZ::Mesh::Network1::ConfigurationsType&			models
							) {
	/* "a(ya(qa{sv}))"
		object node, array{byte, array{(uint16, dict)}} configuration

		An array of Configuration Structures:

		array{struct}

			Models array where each entry is a structure with the
			following members:

			uint16

				Either a SIG Model Identifier or, if Vendor key
				is present in model configuration dictionary, a
				16-bit vendor-assigned Model Identifier

			dict

				A dictionary that contains model configuration
				with the following keys defined:

				array{uint16} Bindings

					Indices of application keys bound to the
					model

				uint32 PublicationPeriod

					Model publication period in milliseconds

				uint16 Vendor

					A 16-bit Company ID as defined by the
					Bluetooth SIG

				array{variant} Subscriptions

					Addresses the model is subscribed to.

					Each address is provided either as
					uint16 for group addresses, or
					as array{byte} for virtual labels.
	*/

	argument.beginArray(); {
		while ( !argument.atEnd() ) {
			Oscl::Qt::DBus::BlueZ::Mesh::Network1::ConfigurationType	model;
			argument >> model;
			models._configurationList.append(model);
			}
		argument.endArray();
		}

	return argument;
	}
