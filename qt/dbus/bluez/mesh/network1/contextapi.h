/*
   Copyright (C) 2020 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_qt_dbus_bluez_mesh_network1_contextapih_
#define _oscl_qt_dbus_bluez_mesh_network1_contextapih_

#include <functional>

/** */
namespace Oscl {
/** */
namespace Qt {
/** */
namespace DBus {
/** */
namespace BlueZ {
/** */
namespace Mesh {
/** */
namespace Network1 {

/** */
class LabelAddress {
	public:
		/** */
		uint8_t	_label[16];

	public:
		/** */
		LabelAddress(
			const uint8_t	label[16]
			) noexcept{
				memcpy(_label,label,16);
				}
	};

/** */
class ModelApi {
	// "(qa{sv})"
	public:
		/** */
		virtual uint16_t	modelID() const noexcept=0;

		/** */
		virtual bool	isVendorModel() const noexcept=0;

		/** Only valid if isVendorModel() is true.
		 */
		virtual uint16_t	companyID() const noexcept=0;

		/** */
		virtual uint32_t	publicationPeriod() const noexcept=0;

		/** */
		virtual void	iterate(std::function<bool (uint16_t subscription)> callback) const = 0;

		/** */
		virtual void	iterate(std::function<bool (const LabelAddress& virtualSubscription)> callback) const = 0;

		/** */
		virtual void	iterateBindings(std::function<bool (uint16_t bindings)> callback) const = 0;
	};

/** */
class ElementApi {
	// "(ya(qa{sv})"
	public:
		/** */
		virtual uint8_t	elementIndex() const noexcept = 0;

		/** */
		virtual void	iterate(std::function<bool (const ModelApi& element)> callback) const = 0;
	};

/** */
class NodeApi {
	// "oa(ya(qa{sv}))"
	public:
		/** */
		virtual const char*	nodeObjectPath() const noexcept=0;

		/** */
		virtual void	iterate(std::function<bool (const ElementApi& element)> callback) const = 0;
	};

/** */
enum AttachResult {InvalidArguments,NotFound,AlreadyExists,Busy,Failed};

/** This interface is designed to satisfy the
	data needs of the Network1 class without
	reference to the D-Bus or Qt in general.
 */
class ContextApi {
	public:
		/** Return 0 if the specified node is not found.
			PossibleErrors:
			org.bluez.mesh.Error.InvalidArguments
			org.bluez.mesh.Error.NotFound,
			org.bluez.mesh.Error.AlreadyExists,
			org.bluez.mesh.Error.Busy,
			org.bluez.mesh.Error.Failed
		 */
		virtual	const NodeApi*	attachReceived(
									const char*			app_root,
									uint64_t			token,
									AttachResult&		result
									) noexcept=0;

		/** */
		virtual	void	importReceived(
							const char*			app_root,
							const uint8_t		deviceUUID[16],
							const uint8_t		deviceKey[16],
							const uint8_t		networkKey[16],
							uint16_t			networkKeyIndex,
							bool				ivUpdate,
							bool				keyRefresh,
							uint32_t			ivIndex,
							uint16_t			unicastAddress
							) noexcept=0;

		/** */
		virtual void	cancelReceived() noexcept=0;

		/** */
		virtual void	createNetworkReceived(
							const char*			app_root,
							const uint8_t		deviceUUID[16]
							) noexcept=0;

		/** */
		virtual void	joinReceived(
							const char*			app_root,
							const uint8_t		deviceUUID[16]
							) noexcept=0;

		/** */
		virtual void	leaveReceived(uint64_t token) noexcept=0;

	};

}
}
}
}
}
}

#endif
