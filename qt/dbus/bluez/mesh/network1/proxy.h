/*
 * This file was generated by qdbusxml2cpp version 0.7
 * Command line was: qdbusxml2cpp -l Oscl::Qt::DBus::BlueZ::Mesh::Network1::Server -i server.h -i configurationstype.h -p proxy dbus.xml
 *
 * qdbusxml2cpp is Copyright (C) 2015 The Qt Company Ltd.
 *
 * This is an auto-generated file.
 * Do not edit! All changes made to it will be lost.
 */

#ifndef PROXY_H_1600719512
#define PROXY_H_1600719512

#include <QtCore/QObject>
#include <QtCore/QByteArray>
#include <QtCore/QList>
#include <QtCore/QMap>
#include <QtCore/QString>
#include <QtCore/QStringList>
#include <QtCore/QVariant>
#include <QtDBus/QtDBus>
#include "server.h"
#include "configurationstype.h"

/*
 * Proxy class for interface org.bluez.mesh.Network1
 */
class OrgBluezMeshNetwork1Interface: public QDBusAbstractInterface
{
    Q_OBJECT
public:
    static inline const char *staticInterfaceName()
    { return "org.bluez.mesh.Network1"; }

public:
    OrgBluezMeshNetwork1Interface(const QString &service, const QString &path, const QDBusConnection &connection, QObject *parent = 0);

    ~OrgBluezMeshNetwork1Interface();

public Q_SLOTS: // METHODS
    inline QDBusPendingReply<QDBusObjectPath, Oscl::Qt::DBus::BlueZ::Mesh::Network1::ConfigurationsType> Attach(const QDBusObjectPath &app_root, qulonglong token)
    {
        QList<QVariant> argumentList;
        argumentList << QVariant::fromValue(app_root) << QVariant::fromValue(token);
        return asyncCallWithArgumentList(QLatin1String("Attach"), argumentList);
    }
    inline QDBusReply<QDBusObjectPath> Attach(const QDBusObjectPath &app_root, qulonglong token, Oscl::Qt::DBus::BlueZ::Mesh::Network1::ConfigurationsType &configuration)
    {
        QList<QVariant> argumentList;
        argumentList << QVariant::fromValue(app_root) << QVariant::fromValue(token);
        QDBusMessage reply = callWithArgumentList(QDBus::Block, QLatin1String("Attach"), argumentList);
        if (reply.type() == QDBusMessage::ReplyMessage && reply.arguments().count() == 2) {
            configuration = qdbus_cast<Oscl::Qt::DBus::BlueZ::Mesh::Network1::ConfigurationsType>(reply.arguments().at(1));
        }
        return reply;
    }

    inline QDBusPendingReply<> Cancel()
    {
        QList<QVariant> argumentList;
        return asyncCallWithArgumentList(QLatin1String("Cancel"), argumentList);
    }

    inline QDBusPendingReply<> CreateNetwork(const QDBusObjectPath &app_root, const QByteArray &uuid)
    {
        QList<QVariant> argumentList;
        argumentList << QVariant::fromValue(app_root) << QVariant::fromValue(uuid);
        return asyncCallWithArgumentList(QLatin1String("CreateNetwork"), argumentList);
    }

    inline QDBusPendingReply<> Import(const QDBusObjectPath &app_root, const QByteArray &uuid, const QByteArray &dev_key, const QByteArray &net_key, ushort net_index, const QVariantMap &flags, uint iv_index, ushort unicast)
    {
        QList<QVariant> argumentList;
        argumentList << QVariant::fromValue(app_root) << QVariant::fromValue(uuid) << QVariant::fromValue(dev_key) << QVariant::fromValue(net_key) << QVariant::fromValue(net_index) << QVariant::fromValue(flags) << QVariant::fromValue(iv_index) << QVariant::fromValue(unicast);
        return asyncCallWithArgumentList(QLatin1String("Import"), argumentList);
    }

    inline QDBusPendingReply<> Join(const QDBusObjectPath &app_root, const QByteArray &uuid)
    {
        QList<QVariant> argumentList;
        argumentList << QVariant::fromValue(app_root) << QVariant::fromValue(uuid);
        return asyncCallWithArgumentList(QLatin1String("Join"), argumentList);
    }

    inline QDBusPendingReply<> Leave(qulonglong token)
    {
        QList<QVariant> argumentList;
        argumentList << QVariant::fromValue(token);
        return asyncCallWithArgumentList(QLatin1String("Leave"), argumentList);
    }

Q_SIGNALS: // SIGNALS
};

namespace org {
  namespace bluez {
    namespace mesh {
      typedef ::OrgBluezMeshNetwork1Interface Network1;
    }
  }
}
#endif
