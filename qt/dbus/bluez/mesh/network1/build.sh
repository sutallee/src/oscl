#!/usr/bin/bash
#Mesh Network Hierarchy
#======================
#Service		org.bluez.mesh
#Interface	org.bluez.mesh.Network1
#Object path	/org/bluez/mesh
#
#Methods:
#	void Join(object app_root, array{byte}[16] uuid)
#
#		This is the first method that an application has to call to
#		become a provisioned node on a mesh network. The call will
#		initiate broadcasting of Unprovisioned Device Beacon.
#
#		The app_root parameter is a D-Bus object root path of
#		the application that implements org.bluez.mesh.Application1
#		interface. The application represents a node where child mesh
#		elements have their own objects that implement
#		org.bluez.mesh.Element1 interface. The application hierarchy
#		also contains a provision agent object that implements
#		org.bluez.mesh.ProvisionAgent1 interface. The standard
#		DBus.ObjectManager interface must be available on the
#		app_root path.
#
#		The uuid parameter is a 16-byte array that contains Device UUID.
#		This UUID must be unique (at least from the daemon perspective),
#		therefore attempting to call this function using already
#		registered UUID results in an error.
#
#		When provisioning finishes, the daemon will call either
#		JoinComplete or JoinFailed method on object implementing
#		org.bluez.mesh.Application1 interface.
#
#		PossibleErrors:
#			org.bluez.mesh.Error.InvalidArguments
#			org.bluez.mesh.Error.AlreadyExists,
#
#	void Cancel(void)
#
#		Cancels an outstanding provisioning request initiated by Join()
#		method.
#
#	object node, array{byte, array{(uint16, dict)}} configuration
#			Attach(object app_root, uint64 token)
#
#		This is the first method that an application must call to get
#		access to mesh node functionalities.
#
#		The app_root parameter is a D-Bus object root path of
#		the application that implements org.bluez.mesh.Application1
#		interface. The application represents a node where child mesh
#		elements have their own objects that implement
#		org.bluez.mesh.Element1 interface. The standard
#		DBus.ObjectManager interface must be available on the
#		app_root path.
#
#		The token parameter is a 64-bit number that has been assigned to
#		the application when it first got provisioned/joined mesh
#		network, i.e. upon receiving JoinComplete() method. The daemon
#		uses the token to verify whether the application is authorized
#		to assume the mesh node identity.
#
#		In case of success, the method call returns mesh node object
#		(see Mesh Node Hierarchy section) and current configuration
#		settings. The return value of configuration parameter is an
#		array, where each entry is a structure that contains element
#		configuration. The element configuration structure is organized
#		as follows:
#
#		byte
#
#			Element index, identifies the element to which this
#			configuration entry pertains.
#
#		array{struct}
#
#			Models array where each entry is a structure with the
#			following members:
#
#			uint16
#
#				Either a SIG Model Identifier or, if Vendor key
#				is present in model configuration dictionary, a
#				16-bit vendor-assigned Model Identifier
#
#			dict
#
#				A dictionary that contains model configuration
#				with the following keys defined:
#
#				array{uint16} Bindings
#
#					Indices of application keys bound to the
#					model
#
#				uint32 PublicationPeriod
#
#					Model publication period in milliseconds
#
#				uint16 Vendor
#
#					A 16-bit Company ID as defined by the
#					Bluetooth SIG
#
#				array{variant} Subscriptions
#
#					Addresses the model is subscribed to.
#
#					Each address is provided either as
#					uint16 for group addresses, or
#					as array{byte} for virtual labels.
#
#		PossibleErrors:
#			org.bluez.mesh.Error.InvalidArguments
#			org.bluez.mesh.Error.NotFound,
#			org.bluez.mesh.Error.AlreadyExists,
#			org.bluez.mesh.Error.Busy,
#			org.bluez.mesh.Error.Failed
#
#	void Leave(uint64 token)
#
#		This removes the configuration information about the mesh node
#		identified by the 64-bit token parameter. The token parameter
#		has been obtained as a result of successful Join() method call.
#
#		PossibleErrors:
#			org.bluez.mesh.Error.InvalidArguments
#			org.bluez.mesh.Error.NotFound
#			org.bluez.mesh.Error.Busy
#
#	void CreateNetwork(object app_root, array{byte}[16] uuid)
#
#		This is the first method that an application calls to become
#		a Provisioner node, and a Configuration Client on a newly
#		created Mesh Network.
#
#		The app_root parameter is a D-Bus object root path of the
#		application that implements org.bluez.mesh.Application1
#		interface, and a org.bluez.mesh.Provisioner1 interface. The
#		application represents a node where child mesh elements have
#		their own objects that implement org.bluez.mesh.Element1
#		interface. The application hierarchy also contains a provision
#		agent object that implements org.bluez.mesh.ProvisionAgent1
#		interface. The standard DBus.ObjectManager interface must be
#		available on the app_root path.
#
#		The uuid parameter is a 16-byte array that contains Device UUID.
#		This UUID must be unique (at least from the daemon perspective),
#		therefore attempting to call this function using already
#		registered UUID results in an error.
#
#		The other information the bluetooth-meshd daemon will preserve
#		about the initial node, is to give it the initial primary
#		unicast address (0x0001), and create and assign a net_key as the
#		primary network net_index (0x000).
#
#		Upon successful processing of Create() method, the daemon
#		will call JoinComplete method on object implementing
#		org.bluez.mesh.Application1.
#
#		PossibleErrors:
#			org.bluez.mesh.Error.InvalidArguments
#			org.bluez.mesh.Error.AlreadyExists,
#
#	void Import(object app_root, array{byte}[16] uuid,
#				array{byte}[16] dev_key,
#				array{byte}[16] net_key, uint16 net_index,
#				dict flags, uint32 iv_index, uint16 unicast)
#
#		This method creates a local mesh node based on node
#		configuration that has been generated outside bluetooth-meshd.
#
#		The app_root parameter is a D-Bus object root path of the
#		application that implements org.bluez.mesh.Application1
#		interface.
#
#		The uuid parameter is a 16-byte array that contains Device UUID.
#		This UUID must be unique (at least from the daemon perspective),
#		therefore attempting to call this function using already
#		registered UUID results in an error.
#
#		The dev_key parameter is the 16-byte value of the dev key of
#		the imported mesh node.
#
#		Remaining parameters correspond to provisioning data:
#
#		The net_key and net_index parameters describe the network (or a
#		subnet, if net_index is not 0) the imported mesh node belongs
#		to.
#
#		The flags parameter is a dictionary containing provisioning
#		flags. Supported values are:
#
#			boolean IvUpdate
#
#				When true, indicates that the network is in the
#				middle of IV Index Update procedure.
#
#			boolean KeyRefresh
#
#				When true, indicates that the specified net key
#				is in the middle of a key refresh procedure.
#
#		The iv_index parameter is the current IV Index value used by
#		the network. This value is known by the provisioner.
#
#		The unicast parameter is the primary unicast address of the
#		imported node.
#
#		Upon successful processing of Import() method, the daemon will
#		call JoinComplete method on object implementing
#		org.bluez.mesh.Application1 interface.
#
#		PossibleErrors:
#			org.bluez.mesh.Error.InvalidArguments,
#			org.bluez.mesh.Error.AlreadyExists,
#			org.bluez.mesh.Error.NotSupported,
#			org.bluez.mesh.Error.Failed


echo "Creating C++ Adaptor for Network1"
qdbusxml2cpp -l Oscl::Qt::DBus::BlueZ::Mesh::Network1::Server -i server.h -i configurationstype.h -a adaptor dbus.xml
echo "Creating C++ Proxy for Network1"
qdbusxml2cpp -l Oscl::Qt::DBus::BlueZ::Mesh::Network1::Server -i server.h -i configurationstype.h -p proxy dbus.xml

