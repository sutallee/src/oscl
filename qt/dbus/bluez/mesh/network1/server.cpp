/*
   Copyright (C) 2020 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/


#include "server.h"
#include "oscl/bluetooth/mesh/sigmodelid.h"

using namespace Oscl::Qt::DBus::BlueZ::Mesh::Network1;

Server::Server(
	ContextApi&	contextApi,
	QObject*	parent
	) :
		QObject(parent),
		_contextApi(contextApi)
		{
	}

Server::~Server() {
	}

QDBusObjectPath Server::Attach(
	const QDBusObjectPath &app_root,
	qulonglong token,
	Oscl::Qt::DBus::BlueZ::Mesh::Network1::ConfigurationsType &configuration
	) {
	// In the bluez mesh daemon, app_path/app_root is associated with
	// the node.
	// "oa(ya(qa{sv}))"

	/* The procedure for calling the context is:
		1)	Call the context to obtain a reference
			to the node matching the token.
		2)	Use the node reference to build the
			return information.
	 */

	Oscl::Qt::DBus::BlueZ::Mesh::Network1::AttachResult	result;

	const Oscl::Qt::DBus::BlueZ::Mesh::Network1::NodeApi*
	node	= _contextApi.attachReceived(
					app_root.path().toLatin1().constData(),
					token,
					result
					);

	QDBusObjectPath	bogusPath;

	if(!node){
		switch(result) {
			// InvalidArguments,NotFound,AlreadyExists,Busy,Failed
			case Oscl::Qt::DBus::BlueZ::Mesh::Network1::AttachResult::InvalidArguments:
				sendErrorReply(
					"org.bluez.mesh.Error.InvalidArgs",
					"Invalid arguments"
					);
				return bogusPath;
			case Oscl::Qt::DBus::BlueZ::Mesh::Network1::AttachResult::NotFound:
				sendErrorReply(
					"org.bluez.mesh.Error.NotFound",
					"Object not found"
					);
				return bogusPath;
			case Oscl::Qt::DBus::BlueZ::Mesh::Network1::AttachResult::AlreadyExists:
				sendErrorReply(
					"org.bluez.mesh.Error.AlreadyExists",
					"Already exists"
					);
				return bogusPath;
			case Oscl::Qt::DBus::BlueZ::Mesh::Network1::AttachResult::Busy:
				sendErrorReply(
					"org.bluez.mesh.Error.Busy",
					"Busy"
					);
				return bogusPath;
			case Oscl::Qt::DBus::BlueZ::Mesh::Network1::AttachResult::Failed:
				sendErrorReply(
					"org.bluez.mesh.Error.Failed",
					"Operation failed"
					);
				return bogusPath;
			default:
				sendErrorReply(
					"org.bluez.mesh.Error.Failed",
					"Unknown failure type"
					);
				return bogusPath;
			}
		}

	QDBusObjectPath	objectPath(node->nodeObjectPath());

	node->iterate(
		[&] (const ElementApi& element) {

			Oscl::Qt::DBus::BlueZ::Mesh::Network1::ConfigurationType configurationType;

			configurationType._elementIndex	= element.elementIndex();

			element.iterate(
				[&] (const ModelApi& model) {

					Oscl::Qt::DBus::BlueZ::Mesh::Network1::ConfigModelsType	modelType;

					modelType._modelID	= model.modelID();

					modelType._dict.insert("PublicationPeriod",QVariant::fromValue(model.publicationPeriod()));

					QList<ushort>	bindingList;

					model.iterateBindings(
						[&] (uint16_t binding) {
							bindingList.append(binding);
							return false;
							}
						);

					modelType._dict.insert("Bindings",QVariant::fromValue(bindingList));

					QList<QVariant>	subscriptionList;

					model.iterate(
						[&] (uint16_t subscription) {
							subscriptionList.append(QVariant::fromValue(subscription));
							return false;
							}
						);

					model.iterate(
						[&] (const LabelAddress& virtualSubscription) {
							QByteArray	ba(
											(const char*)virtualSubscription._label,
											16
											);
							subscriptionList.append(QVariant::fromValue(ba));
							return false;
							}
						);

					modelType._dict.insert("Subscriptions",QVariant::fromValue(subscriptionList));

					configurationType._modelsList.append(modelType);

					return false;
					}
				);

			configuration._configurationList.append(configurationType);

			return false;
			}
		);

	return objectPath;
	}

void Server::Cancel(){
	_contextApi.cancelReceived();
	}

void Server::CreateNetwork(const QDBusObjectPath &app_root, const QByteArray &uuid){
	// oay

	_contextApi.createNetworkReceived(
		app_root.path().toLatin1().constData(),
		(const uint8_t*)uuid.constData()
		);
	}

void Server::Import(
		const QDBusObjectPath&	app_root,
		const QByteArray&		uuid,
		const QByteArray&		dev_key,
		const QByteArray&		net_key,
		ushort					net_index,
		const QVariantMap&		flags,
		uint					iv_index,
		ushort					unicast
		){

		bool	ivUpdate	= false;
		bool	keyRefresh	= false;

		QVariant
		ivUpdateVar		= flags.value("IvUpdate");
		if(ivUpdateVar.canConvert<bool>()){
			ivUpdate	= ivUpdateVar.toBool();
			}

		QVariant
		keyRefreshVar	= flags.value("KeyRefresh");
		if(keyRefreshVar.canConvert<bool>()){
			keyRefresh	= keyRefreshVar.toBool();
			}

		_contextApi.importReceived(
				app_root.path().toLatin1(),
				(const uint8_t*)uuid.constData(),
				(const uint8_t*)dev_key.constData(),
				(const uint8_t*)net_key.constData(),
				net_index,
				ivUpdate,
				keyRefresh,
				iv_index,
				unicast
				);
	}

void Server::Join(const QDBusObjectPath &app_root, const QByteArray &uuid){
	// "oay"

	_contextApi.joinReceived(
		app_root.path().toLatin1().constData(),
		(const uint8_t*)uuid.constData()
		);
	}

void Server::Leave(qulonglong token){

	_contextApi.leaveReceived(token);
	}

