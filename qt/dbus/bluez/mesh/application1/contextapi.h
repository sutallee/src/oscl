/*
   Copyright (C) 2020 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_qt_dbus_bluez_mesh_application1_contextapih_
#define _oscl_qt_dbus_bluez_mesh_application1_contextapih_

#include <functional>

/** */
namespace Oscl {
/** */
namespace Qt {
/** */
namespace DBus {
/** */
namespace BlueZ {
/** */
namespace Mesh {
/** */
namespace Application1 {

/** This interface is designed to satisfy the
	data needs of the Application1 class without
	reference to the D-Bus or Qt in general.
 */
class ContextApi {
	public:
		/** */
		virtual void	joinCompleteReceived(
							uint64_t	token
							) noexcept=0;

		/** */
		virtual void	joinFailedReceived(
							const char*	reason
							) noexcept=0;

		/** */
		virtual uint16_t	getCompanyID() const noexcept=0;

		/** */
		virtual uint16_t	getProductID() const noexcept=0;

		/** */
		virtual uint16_t	getVersionID() const noexcept=0;

		/** */
		virtual uint16_t	getCRPL() const noexcept=0;

	};

}
}
}
}
}
}

#endif
