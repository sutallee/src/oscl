#!/usr/bin/bash

#echo "Creating C++ Adaptor for Application1"
#qdbusxml2cpp -l Oscl::Qt::DBus::BlueZ::Mesh::Application1::Server -i server.h -a adaptor dbus.xml

echo "Creating C++ Proxy for Application1"
#qdbusxml2cpp -l Oscl::Qt::DBus::BlueZ::Mesh::Application1::Server -i server.h -p proxy dbus.xml
qdbusxml2cpp -p proxy dbus.xml

