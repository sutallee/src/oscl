/*
   Copyright (C) 2020 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_qt_dbus_bluez_mesh_attention1_contextapih_
#define _oscl_qt_dbus_bluez_mesh_attention1_contextapih_

#include <functional>

/** */
namespace Oscl {
/** */
namespace Qt {
/** */
namespace DBus {
/** */
namespace BlueZ {
/** */
namespace Mesh {
/** */
namespace Attention1 {

/** This interface is designed to satisfy the
	data needs of the Attention1 class without
	reference to the D-Bus or Qt in general.
 */
class ContextApi {
	public:
		/** */
		virtual void	setTimerReceived(
							uint8_t		element_index,
							uint16_t	timer
							) noexcept=0;

		/** */
		virtual uint16_t	getTimerReceived(
								uint16_t	element
								) noexcept=0;

	};

/** */
template <class Context>
class ContextApiComposer: public ContextApi {
	private:
		/** */
		Context&	_context;

		/** */
		void	(Context::*const _setTimerReceived)(
					uint8_t		element_index,
					uint16_t	timer
					);

		/** */
		uint16_t	(Context::*const _getTimerReceived)(
						uint16_t	element
						);

	public:
		/** */
		ContextApiComposer(
			Context&	context,
			void	(Context::*const setTimerReceived)(
						uint8_t		element_index,
						uint16_t	timer
						),
			uint16_t	(Context::*const getTimerReceived)(
							uint16_t	element
							)
			) noexcept:
				_context(context),
				_setTimerReceived(setTimerReceived),
				_getTimerReceived(getTimerReceived)
				{
			}
		
	private:
		/** */
		void	setTimerReceived(
					uint8_t		element_index,
					uint16_t	timer
					) noexcept{
						(_context.*_setTimerReceived)(
							element_index,
							timer
							);
						}

		/** Returns net_index */
		uint16_t	getTimerReceived(
						uint16_t	element
						) noexcept{
							return (_context.*_getTimerReceived)(
								element
								);
							}
	};
}
}
}
}
}
}

#endif
