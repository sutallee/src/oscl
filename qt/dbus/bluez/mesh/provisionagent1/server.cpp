/*
   Copyright (C) 2020 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "server.h"
#include <QDBusMessage>
#include <QDBusConnection>
#include <QDBusContext>

using namespace Oscl::Qt::DBus::BlueZ::Mesh::ProvisionAgent1;

Server::Server(
	ContextApi&	contextApi,
	QObject*	parent
	) :
		QObject(parent),
		_contextApi(contextApi)
		{
	}

Server::~Server() {
	}

QStringList	Server::Capabilities() const{
	unsigned	nCapabilities;

	const char* const*
	capabilities	= _contextApi.getCapabilities(nCapabilities);

	QStringList	list;

	for(unsigned i=0;i<nCapabilities;++i){
		list.append(capabilities[i]);
		}

	return list;
	}

QStringList	Server::OutOfBandInfo() const{
	unsigned	nOutOfBandInfo;

	const char* const *
	outOfBandInfo	= _contextApi.getOutOfBandInfo(nOutOfBandInfo);

	QStringList	list;

	for(unsigned i=0;i<nOutOfBandInfo;++i){
		list.append(outOfBandInfo[i]);
		}

	return list;
	}

QString	Server::URI() const{
	QString	uri	= _contextApi.getURI();
	return uri;
	}

QByteArray Server::PrivateKey(){
	QByteArray
	key(
		(const char*)_contextApi.privateKeyReceived(),
		32
		);
	return key;
	}

QByteArray Server::PublicKey(){
	QByteArray
	key(
		(const char*)_contextApi.publicKeyReceived(),
		64
		);
	return key;
	}

void Server::DisplayString(QString value){
	_contextApi.displayStringReceived(value.toLatin1());
	}

void Server::DisplayNumeric(
				QString	type,
				uint	number
				){
	_contextApi.displayNumericReceived(
		type.toLatin1(),
		number
		);
	}

class PromptNumericReply : public Oscl::Qt::DBus::BlueZ::Mesh::ProvisionAgent1::PromptNumericReplyApi {
	private:
		/** */
		QDBusMessage		_message;
		/** */
		QDBusConnection		_connection;

	public:
		/** */
		PromptNumericReply(
			QDBusContext&	context
			) noexcept:
				_message(context.message()),
				_connection(context.connection())
				{ }

		/** */
		~PromptNumericReply() noexcept{
			printf("%s\n",__PRETTY_FUNCTION__);
			}

	private:
		/** */
		void	sendError(
					const char*	description
					) noexcept{
					QDBusMessage	reply = _message.createErrorReply(
												"org.bluez.mesh.Error.Failed",
												description
												);
					_connection.send(reply);
					}

		/** */
		void	sendReply(uint32_t number) noexcept{
					QDBusMessage	reply = _message.createReply(QVariant::fromValue(number));
					_connection.send(reply);
					delete this;
					}
	};

uint Server::PromptNumeric(
				QString				type
				){
	PromptNumericReply*
	delayedReply	= new PromptNumericReply(*this);

	uint	bogusReturnValue	= 0;

	if(!delayedReply){
		sendErrorReply(
			QDBusError::NoMemory,
			"Can't allocate PromptNumericReply"
			);
		return bogusReturnValue;
		}

	setDelayedReply(true);

	_contextApi.promptNumericReceived(
				type.toLatin1(),
				*delayedReply
				);

	return bogusReturnValue;
	}

class PromptStaticReply : public Oscl::Qt::DBus::BlueZ::Mesh::ProvisionAgent1::PromptStaticReplyApi {
	private:
		/** */
		QDBusMessage		_message;
		/** */
		QDBusConnection		_connection;

	public:
		/** */
		PromptStaticReply(
			QDBusContext&	context
			) noexcept:
				_message(context.message()),
				_connection(context.connection())
				{ }

		/** */
		~PromptStaticReply() noexcept{ }

	private:
		/** */
		void	sendError(
					const char*	description
					) noexcept{
					QDBusMessage	reply = _message.createErrorReply(
												"org.bluez.mesh.Error.Failed",
												description
												);
					_connection.send(reply);
					delete this;
					}

		/** */
		void	sendReply(const uint8_t* value, unsigned length) noexcept{
					QByteArray	v(
									(const char*)value,
									length
									);
					QDBusMessage	reply = _message.createReply(QVariant::fromValue(v));
					_connection.send(reply);
					delete this;
					}
	};

QByteArray Server::PromptStatic(
				QString	type
				){
#if 0
	QByteArray
	value(
		(const char*)_contextApi.promptStaticReceived(type.toLatin1()),
		16
		);
	return value;
#endif
	PromptStaticReply*
	delayedReply	= new PromptStaticReply(*this);

	QByteArray	bogusReturnValue;

	if(!delayedReply){
		sendErrorReply(
			QDBusError::NoMemory,
			"Can't allocate PromptStaticReply"
			);
		return bogusReturnValue;
		}

	setDelayedReply(true);

	_contextApi.promptStaticReceived(
				type.toLatin1(),
				*delayedReply
				);

	return bogusReturnValue;
	}

void	Server::Cancel(){
	_contextApi.cancelReceived();
	}


