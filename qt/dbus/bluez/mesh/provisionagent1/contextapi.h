/*
   Copyright (C) 2020 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_qt_dbus_bluez_mesh_provisionagent1_contextapih_
#define _oscl_qt_dbus_bluez_mesh_provisionagent1_contextapih_

#include <functional>

/** */
namespace Oscl {
/** */
namespace Qt {
/** */
namespace DBus {
/** */
namespace BlueZ {
/** */
namespace Mesh {
/** */
namespace ProvisionAgent1 {

/** */
class PromptNumericReplyApi {
	public:
		/** */
		virtual ~PromptNumericReplyApi(){}

		/** */
		virtual void	sendError(
							const char*	description
							) noexcept=0;

		/** */
		virtual void	sendReply(uint32_t number) noexcept=0;
	};

/** */
class PromptStaticReplyApi {
	public:
		/** */
		virtual ~PromptStaticReplyApi(){}

		/** */
		virtual void	sendError(
							const char*	description
							) noexcept=0;

		/** */
		virtual void	sendReply(
							const uint8_t*	value,
							unsigned		length
							) noexcept=0;
	};

/** This interface is designed to satisfy the
	data needs of the ProvisionAgent1 class without
	reference to the D-Bus or Qt in general.
 */
class ContextApi {
	public:
		/** The returned key must be 32-octets */
		virtual const uint8_t*	privateKeyReceived() noexcept=0;

		/** The returned key must be 64-octets */
		virtual const uint8_t*	publicKeyReceived() noexcept=0;

		/** */
		virtual void	displayStringReceived(
							const char*	value
							) noexcept=0;

		/** */
		virtual void	displayNumericReceived(
							const char*	type,
							uint32_t	number
							) noexcept=0;

		/** */
		virtual void	promptNumericReceived(
							const char*				type,
							PromptNumericReplyApi&	replyApi
							) noexcept=0;

		/** The returned value must be 16-octets. */
		virtual void	promptStaticReceived(
							const char*				type,
							PromptStaticReplyApi&	replyApi
							) noexcept=0;

		/** */
		virtual void	cancelReceived() noexcept=0;

		/** */
		virtual const char* const *	getCapabilities(unsigned& nCapabilities) const noexcept=0;

		/** */
		virtual const char* const *	getOutOfBandInfo(unsigned& nOutOfBandInfo) const noexcept=0;

		/** */
		virtual const char*	getURI() const noexcept=0;

	};

}
}
}
}
}
}

#endif
