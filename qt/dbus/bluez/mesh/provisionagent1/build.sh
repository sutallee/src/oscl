#!/usr/bin/bash

echo "Creating C++ Adaptor for ProvisionAgent1"
qdbusxml2cpp -l Oscl::Qt::DBus::BlueZ::Mesh::ProvisionAgent1::Server -i server.h -a adaptor dbus.xml

echo "Creating C++ Proxy for ProvisionAgent1"
qdbusxml2cpp -l Oscl::Qt::DBus::BlueZ::Mesh::ProvisionAgent1::Server -i server.h -p proxy dbus.xml

