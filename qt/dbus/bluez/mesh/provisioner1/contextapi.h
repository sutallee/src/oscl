/*
   Copyright (C) 2020 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_qt_dbus_bluez_mesh_provisioner1_contextapih_
#define _oscl_qt_dbus_bluez_mesh_provisioner1_contextapih_

#include <functional>

/** */
namespace Oscl {
/** */
namespace Qt {
/** */
namespace DBus {
/** */
namespace BlueZ {
/** */
namespace Mesh {
/** */
namespace Provisioner1 {

/** This interface is designed to satisfy the
	data needs of the Provisioner1 class without
	reference to the D-Bus or Qt in general.
 */
class ContextApi {
	public:
		/** */
		virtual void	scanResultReceived(
							int16_t				rssi,
							unsigned			nData,
							const void*			data
							) noexcept=0;

		/** Returns net_index */
		virtual uint16_t	requestProvDataReceived(
							uint8_t		count,
							uint16_t&	unicast
							) noexcept=0;

		/** */
		virtual void	addNodeCompleteReceived(
							const uint8_t		uuid[16],
							uint16_t			unicast,
							uint8_t				count
							) noexcept=0;

		/** */
		virtual void	addNodeFailedReceived(
							const uint8_t		uuid[16],
							const char*			reason
							) noexcept=0;
		
	};

/** */
template <class Context>
class ContextApiComposer: public ContextApi {
	private:
		/** */
		Context&	_context;

		/** */
		void	(Context::*const _scanResultReceived)(
					int16_t				rssi,
					unsigned			nData,
					const void*			data
					);

		/** */
		uint16_t	(Context::*const _requestProvDataReceived)(
						uint8_t		count,
						uint16_t&	unicast
						);

		/** */
		void	(Context::*const _addNodeCompleteReceived)(
					const uint8_t		uuid[16],
					uint16_t			unicast,
					uint8_t				count
					);

		/** */
		void	(Context::*const _addNodeFailedReceived)(
					const uint8_t		uuid[16],
					const char*			reason
					);

	public:
		/** */
		ContextApiComposer(
			Context&	context,
			void		(Context::*scanResultReceived)(
							int16_t				rssi,
							unsigned			nData,
							const void*			data
							),
			uint16_t	(Context::*requestProvDataReceived)(
							uint8_t		count,
							uint16_t&	unicast
							),
			void		(Context::*addNodeCompleteReceived)(
							const uint8_t		uuid[16],
							uint16_t			unicast,
							uint8_t				count
							),
			void		(Context::*addNodeFailedReceived)(
							const uint8_t		uuid[16],
							const char*			reason
							)
			) noexcept:
				_context(context),
				_scanResultReceived(scanResultReceived),
				_requestProvDataReceived(requestProvDataReceived),
				_addNodeCompleteReceived(addNodeCompleteReceived),
				_addNodeFailedReceived(addNodeFailedReceived)
				{
			}
		
	private:
		/** */
		void	scanResultReceived(
					int16_t				rssi,
					unsigned			nData,
					const void*			data
					) noexcept{
						(_context.*_scanResultReceived)(
							rssi,
							nData,
							data
							);
						}

		/** Returns net_index */
		uint16_t	requestProvDataReceived(
						uint8_t		count,
						uint16_t&	unicast
						) noexcept{
							return (_context.*_requestProvDataReceived)(
								count,
								unicast
								);
							}

		/** */
		void	addNodeCompleteReceived(
					const uint8_t		uuid[16],
					uint16_t			unicast,
					uint8_t				count
					) noexcept{
						(_context.*_addNodeCompleteReceived)(
							uuid,
							unicast,
							count
							);
						}

		/** */
		void	addNodeFailedReceived(
					const uint8_t		uuid[16],
					const char*			reason
					) noexcept{
						(_context.*_addNodeFailedReceived)(
							uuid,
							reason
							);
						}
		
	};

}
}
}
}
}
}

#endif
