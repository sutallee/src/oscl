/*
   Copyright (C) 2020 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_qt_dbus_bluez_freedesktop_objectmanager_clienth_
#define _oscl_qt_dbus_bluez_freedesktop_objectmanager_clienth_

#include <QObject>
#include <QDBusObjectPath>
#include <QByteArray>
#include <QVariantMap>
#include <QDBusContext>
//#include "clientcontextapi.h"
#include "types.h"
#include "proxy.h"

/** */
namespace Oscl {
/** */
namespace Qt {
/** */
namespace DBus {
/** */
namespace FreeDesktop {
/** */
namespace ObjectManager {

/** */
class Client : public QObject, protected QDBusContext {
	Q_OBJECT
//	Q_CLASSINFO("D-Bus Interface", "org.freedesktop.dbus.ObjectManager")

	public:
		/** This interface is designed to satisfy the
			data needs of the Network1 class without
			reference to the D-Bus.
		 */
		class ContextApi {
			public:
				/** */
				virtual void	managedObjectsUpdate(
						const Oscl::Qt::DBus::FreeDesktop::ObjectManager::ManagedObjectList&	objects
           		     ) noexcept=0;

				/** */
				virtual void	interfacesRemoved(
									const QDBusObjectPath &object_path,
									const QStringList&		interfaces
									) noexcept=0;

				virtual void	interfacesAdded(
									const QDBusObjectPath &object_path,
									const QMap<QString, QVariantMap>&	interfaces
									) noexcept=0;
			};
	private:
		/** */
		Client::ContextApi&	_contextApi;

		/** */
		org::freedesktop::DBus::ObjectManager*	_manager;

	public:
		/** */
		Client(
			Client::ContextApi&	contextApi,
			QObject*	parent	= 0
			);

		/** */
		virtual ~Client();

		/** */
		void	start() noexcept;

		/** */
		void	getManagedObjects() noexcept;

	private:
		/** */
		void	getManagedObjectsFinished(QDBusPendingCallWatcher* watcher);

	private slots:
		/** */
		void	InterfacesRemoved(
					const QDBusObjectPath&	object_path,
					const QStringList&		interfaces
					);

		void	InterfacesAdded(
					const QDBusObjectPath &object_path,
					const Oscl::Qt::DBus::FreeDesktop::ObjectManager::InterfaceList&	interfaces
					);
	};

}
}
}
}
}

#endif
