/*
   Copyright (C) 2020 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "client.h"
#include <QtDBus>
#include <stdio.h>

using namespace Oscl::Qt::DBus::FreeDesktop::ObjectManager;

Client::Client(
	ContextApi&	contextApi,
	QObject*	parent
	) :
		QObject(parent),
		_contextApi(contextApi),
		_manager(0)
		{
	}

Client::~Client() {
	}

void	Client::start() noexcept{

    qRegisterMetaType<Oscl::Qt::DBus::FreeDesktop::ObjectManager::InterfaceList>(
		"Oscl::Qt::DBus::FreeDesktop::ObjectManager::InterfaceList"
		);

    qRegisterMetaType<Oscl::Qt::DBus::FreeDesktop::ObjectManager::ManagedObjectList>(
		"Oscl::Qt::DBus::FreeDesktop::ObjectManager::ManagedObjectList"
		);

    qDBusRegisterMetaType<Oscl::Qt::DBus::FreeDesktop::ObjectManager::InterfaceList>();
    qDBusRegisterMetaType<Oscl::Qt::DBus::FreeDesktop::ObjectManager::ManagedObjectList>();

	_manager	= new org::freedesktop::DBus::ObjectManager(
					QStringLiteral("org.bluez.mesh"),	// const QString &service,
					QStringLiteral("/"),				// const QString &path,
					QDBusConnection::systemBus(),		// const QDBusConnection&	connection,
					this
					);

	connect(
		_manager,
		&OrgFreedesktopDBusObjectManagerInterface::InterfacesAdded,
		this,
		&Client::InterfacesAdded
		);

	connect(
		_manager,
		&OrgFreedesktopDBusObjectManagerInterface::InterfacesRemoved,
		this,
		&Client::InterfacesRemoved
		);
	}

void	Client::getManagedObjects() noexcept{
	QDBusPendingCall
	async	=	_manager->GetManagedObjects();

	QDBusPendingCallWatcher*
	watcher	= new QDBusPendingCallWatcher(
				async,
				this
				);

	QObject::connect(
		watcher,
		&QDBusPendingCallWatcher::finished,
		this,
		&Client::getManagedObjectsFinished
		);
	}

void	Client::getManagedObjectsFinished(QDBusPendingCallWatcher* watcher){
	QDBusPendingReply<Oscl::Qt::DBus::FreeDesktop::ObjectManager::ManagedObjectList>
    reply = *watcher;

    if(reply.isError()){
        printf(
            "%s: \"%s\", \"%s\"\n",
            __PRETTY_FUNCTION__,
            reply.error().name().toLatin1().data(),
            reply.error().message().toLatin1().data()
            );
        return;
        }

	_contextApi.managedObjectsUpdate(
        reply.argumentAt<0>()
		);

    watcher->deleteLater();
	}

void	Client::InterfacesRemoved(
			const QDBusObjectPath&	object_path,
			const QStringList&		interfaces
			){

	_contextApi.interfacesRemoved(
		object_path,
		interfaces
		);
	}

void	Client::InterfacesAdded(
			const QDBusObjectPath &object_path,
			const Oscl::Qt::DBus::FreeDesktop::ObjectManager::InterfaceList&	interfaces
			){

	_contextApi.interfacesAdded(
		object_path,
		interfaces
		);
	}

