/*
   Copyright (C) 2020 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_qt_posix_mbox_parth_
#define _oscl_qt_posix_mbox_parth_

#include "oscl/mt/itc/mbox/srvapi.h"
#include "oscl/mt/itc/mbox/base.h"
#include <QObject>
#include <QSocketNotifier>

/** */
namespace Oscl {
/** */
namespace Qt {
/** */
namespace Posix {
/** */
namespace Mbox {

/** */
class Part :
	public QObject,
	private Oscl::Mt::Sema::SignalApi
	{
	private:
		Q_OBJECT

	private:
		/** */
		Oscl::Mt::Itc::Base			_mboxBase;

		/** */
		QSocketNotifier*			_notifier;

		/** This pipe is used as a signalling
			mechanism to wake-up the mailbox host
			thread such that it can service its
			mailbox.
			_sigPipe[0] is for reading
			_sigPipe[1] is for writing
		 */
		int						_sigPipe[2];

	public:
		/** */
		Part() noexcept;

		/** */
		virtual ~Part();

		/** */
		void	start() noexcept;

		/** */
		Oscl::Mt::Itc::PostMsgApi&	getPostMsgApi() noexcept;

	private slots:
		/** */
		void	activated(int socket);

	public: // Oscl::Mt::Sema::SignalApi
		/** */
		void signal(void) noexcept;

		/** */
		void suSignal(void) noexcept;

	};

}
}
}
}

#endif
