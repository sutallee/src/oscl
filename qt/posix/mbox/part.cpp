/*
   Copyright (C) 2020 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/
#include "part.h"
#include "oscl/mt/scopesync.h"
#include "oscl/mt/thread.h"
#include "oscl/error/info.h"
#include <stdio.h>
#include <errno.h>
#include <unistd.h>
#include <fcntl.h>
#include <QSocketNotifier>

using namespace Oscl::Qt::Posix::Mbox;

Part::Part() noexcept:
	_mboxBase(*this),
	_notifier(0)
	{

	int err = pipe(_sigPipe);
    if(err){
        perror("Oscl::Qt::Posix::Mbox::Part: Cannot create signal pipe\n");
        exit(err);
        }

	err	= fcntl(_sigPipe[1],F_SETFL,O_NONBLOCK);
	if(err){
        perror("Oscl::Qt::Posix::Mbox::Part: Cannot set O_NONBLOCK.\n");
		}

	}

Part::~Part(){

	if(_sigPipe[0] > -1){
		close(_sigPipe[0]);
		}

	if(_sigPipe[1] > -1){
		close(_sigPipe[1]);
		}

	_sigPipe[0]	= -1;
	_sigPipe[1]	= -1;

	}

void	Part::start() noexcept{
	/*	The QSocketNotifier constructor automatically registers
		itself with Qt's main event loop (QCoreApplication::exec())
		in the current QThread.
	 */
	_notifier	= new QSocketNotifier(
						_sigPipe[0],
						QSocketNotifier::Read
						);
	if(!_notifier){
        perror("Oscl::Qt::Posix::Mbox::Part: Cannot allocate QSocketNotifier.\n");
		}

	connect(
		_notifier,
		&QSocketNotifier::activated,
		this,
		&Part::activated
		);

	_notifier->setEnabled(true);
	}

void	Part::signal() noexcept{

	/*	This value is unimportant but could
		be useful in debugging.
	 */
	char	value	= 0xaa;

	for(unsigned i;;++i){

		ssize_t	result	= write(_sigPipe[1],&value,1);

		if(result < 1){

			if(result < 0){
				if(errno == EINTR) {
					/*	We were interrupted by
						a signal so we try a gain.
					 */
					continue;
					}

				if( (errno == EAGAIN) || (errno == EWOULDBLOCK) ){
					/*	The pipe is full so the FIFO has at least
						one value in it, which is sufficient for
						to signal the host thread.
					 */
					break;
					}

				/*	At this point, we have an error that
					is likely fatal, but may be handled
					at a higher layer.
				 */
				Oscl::Error::Info::log(
					"%s: [%p]: _sigPipe[1]:%d write failed: errno: %d\n",
					OSCL_PRETTY_FUNCTION,
					this,
					_sigPipe[1],
					errno
					);
				break;
				}

			/*	Zero data was written to the FIFO. This is
				unexpected since we tried to write one byte.
				According to the documentation,
				"the results are not specified".
				To avoid some kind of endless loop,
				we will *assume* the write succeeded;
				log the result; and behave as-if
				everything is fine.
			 */
			Oscl::Error::Info::log(
				"%s: [%p]: _sigPipe[1]:%d unexpected count of zero.\n",
				OSCL_PRETTY_FUNCTION,
				this,
				_sigPipe[1]
				);

			break;
			}
		else {
			/*	This is the normal path. The byte was
				written, so we're done.
			 */
			break;
			}
		}
	}

void	Part::suSignal() noexcept{

	/*	This value is unimportant but could
		be useful in debugging.
	 */
	char	value	= 0x55;

	ssize_t
	result	= write(_sigPipe[1],&value,1);

	if(result < 0){
		// Nothing to do.
		// This is expected and the mailbox
		// will recover eventually when
		// the pipe empties.
		}
	}

void	Part::activated(int socket){
	// Someone has invoked signal()

	char	buffer[1];

	ssize_t
	result	= read(_sigPipe[0],buffer,1);

	if(result < 0){
		// We'll ignore this since it is
		// expected?
		}

	_mboxBase.processMessages();
	}

Oscl::Mt::Itc::PostMsgApi&	Part::getPostMsgApi() noexcept{
	return _mboxBase;
	}

