/*
   Copyright (C) 2023 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <iostream>
#include <QBrush>
#include <QLineF>
#include <QBuffer>
#include <QtCore>
#include "sizer.h"
#include "handles.h"

using namespace Oscl::Qt::Graphics::Item;

static qreal	distance(
	const QPointF& a,
	const QPointF& b
	) {
	qreal	x1	= a.x();
	qreal	y1	= a.y();
	qreal	x2	= b.x();
	qreal	y2	= b.y();

	qreal	xd	= x1 - x2;
	qreal	yd	= y1 - y2;

	xd	*= xd;
	yd	*= yd;

	return	sqrt( xd + yd );
	}

static bool	intersection(
	const QLineF& centerline,
	const QPolygonF& polygon, QPointF& intersectPoint
	) {
	unsigned	bounded	= 0;
	qreal	minDx	= distance( centerline.p1(),centerline.p2() );

	QPointF p1 = polygon.first();

	for ( int i = 1; i < polygon.count(); ++i ) {
		QPointF	p2			= polygon.at( i );
		QLineF	polyLine	= QLineF( p1, p2 );

		QPointF xy;

		QLineF::IntersectionType
		intersectionType = polyLine.intersects( centerline, &xy );

		if ( intersectionType == QLineF::BoundedIntersection ) {
			if( bounded ) {
				qreal	newDx	= distance( centerline.p1(),xy );
				if( newDx < minDx ) {
					minDx	= newDx;
					intersectPoint	= xy;
					}
				}
			else {
				intersectPoint	= xy;
				minDx	= distance( centerline.p1(),xy );
				}
			++bounded;
			}
		p1 = p2;
		}


	if( !bounded ) {
		qWarning()
			<< __PRETTY_FUNCTION__
			<< "unbounded"
			;
		}

	return bounded;
	}

TopLeftHandle::TopLeftHandle( Sizer* parent ):
	Handle( parent ),
	_sizer( parent )
	{}

void	TopLeftHandle::reposition() {

	QPainterPath	path		= _sizer->shape();	// in local coordinates
	QPolygonF		polygon		= path.toFillPolygon();	// in local coordinates
	QRectF			bound		= _sizer->boundingRect();

	bound.marginsAdded( QMarginsF( 1, 1, 1, 1 ) );

	QLineF 	centerLine( bound.topLeft(), bound.bottomRight() );
	QPointF	xy;

	if( intersection( centerLine, polygon,xy ) ) {
		setPos( xy );
		}
	}

void	TopLeftHandle::positionHasChanged( const QPointF delta ) {
	_sizer->offsetTopLeft( delta );
	}

void	TopLeftHandle::mousePressed() {
	_sizer->mousePressed();
	}

void	TopLeftHandle::mouseReleased() {
	_sizer->mouseReleased();
	}

QPointF TopLeftHandle::constrain( const QPointF& proposed ) const {

	QPointF			position = pos();
	const QRectF&	bounds	= _sizer->boundingRect();

	position.setY( proposed.y() );

	position.setX( proposed.x() );

	if( position.y() > bounds.bottom() ) {
		position.setY( bounds.bottom() );
		}

	if( position.x() > bounds.right() ) {
		position.setX( bounds.right() );
		}

	return position;
	}

TopHandle::TopHandle( Sizer* parent ):
	Handle( parent ),
	_sizer( parent )
	{}

void	TopHandle::reposition() {

	QPainterPath	path		= _sizer->shape();	// in local coordinates
	QPolygonF		polygon		= path.toFillPolygon();	// in local coordinates
	QRectF			bound		= _sizer->boundingRect();

	QPointF			start( bound.left() + bound.width() / 2, bound.top() );
	QPointF			end( bound.left() + bound.width() /2 , bound.bottom() );
	QLineF 			centerLine( start, end );

	QPointF			xy;

	if( intersection( centerLine, polygon,xy ) ) {
		setPos( xy );
		}
	}

void	TopHandle::positionHasChanged( const QPointF delta ) {
	_sizer->offsetTop( delta.y() );
	}

void	TopHandle::mousePressed() {
	_sizer->mousePressed();
	}

void	TopHandle::mouseReleased() {
	_sizer->mouseReleased();
	}

QPointF TopHandle::constrain( const QPointF& proposed ) const {

	QPointF			position = pos();
	const QRectF&	bounds	= _sizer->boundingRect();

	position.setY( proposed.y() );

	if( position.y() > bounds.bottom() ) {
		position.setY( bounds.bottom() );
		}

	return position;
	}

TopRightHandle::TopRightHandle( Sizer* parent ):
	Handle( parent ),
	_sizer( parent )
	{}

void	TopRightHandle::reposition() {

	QPainterPath	path		= _sizer->shape();	// in local coordinates
	QPolygonF		polygon		= path.toFillPolygon();	// in local coordinates
	QRectF			bound		= _sizer->boundingRect();

	bound.marginsAdded( QMarginsF( 1, 1, 1, 1) );

	QLineF 			centerLine( bound.topRight(), bound.bottomLeft() );

	QPointF			xy;

	if( intersection( centerLine, polygon, xy ) ) {
		setPos( xy );
		}
	}

void	TopRightHandle::positionHasChanged( const QPointF delta ) {
	_sizer->offsetTopRight( delta );
	}

void	TopRightHandle::mousePressed() {
	_sizer->mousePressed();
	}

void	TopRightHandle::mouseReleased() {
	_sizer->mouseReleased();
	}

QPointF TopRightHandle::constrain( const QPointF& proposed ) const {

	QPointF			position = pos();
	const QRectF&	bounds	= _sizer->boundingRect();

	position.setY( proposed.y() );
	position.setX( proposed.x() );

	if( position.y() > bounds.bottom() ) {
		position.setY( bounds.bottom() );
		}

	if( position.x() < bounds.left() ) {
		position.setX( bounds.left() );
		}

	return position;
	}

RightHandle::RightHandle( Sizer* parent ):
	Handle( parent ),
	_sizer( parent )
	{}

void	RightHandle::reposition() {

	QPainterPath	path		= _sizer->shape();	// in local coordinates
	QPolygonF		polygon		= path.toFillPolygon();	// in local coordinates
	QRectF			bound		= _sizer->boundingRect();

	bound.marginsAdded( QMarginsF( 1, 1, 1, 1) );

	QPointF	start( bound.right(), bound.top() + bound.height() / 2 );
	QPointF	end( bound.left(), bound.top() + bound.height() /2 );
	QLineF 	centerLine( start, end );

	QPointF			xy;

	if( intersection( centerLine, polygon, xy ) ) {
		setPos( xy );
		}
	}

void	RightHandle::positionHasChanged( const QPointF delta ) {
	_sizer->offsetRight( delta.x() );
	}

void	RightHandle::mousePressed() {
	_sizer->mousePressed();
	}

void	RightHandle::mouseReleased() {
	_sizer->mouseReleased();
	}

QPointF RightHandle::constrain( const QPointF& proposed ) const {

	QPointF			position = pos();
	const QRectF&	bounds	= _sizer->boundingRect();

	position.setX( proposed.x() );

	if( position.x() < bounds.left() ) {
		position.setX( bounds.left() );
		}

	return position;
	}

BottomRightHandle::BottomRightHandle( Sizer* parent ):
	Handle( parent ),
	_sizer( parent )
	{}

void	BottomRightHandle::reposition() {

	QPainterPath	path		= _sizer->shape();	// in local coordinates
	QPolygonF		polygon		= path.toFillPolygon();	// in local coordinates
	QRectF			bound		= _sizer->boundingRect();

	bound.marginsAdded( QMarginsF( 1, 1, 1, 1) );

	QLineF 	centerLine( bound.bottomRight(), bound.topLeft() );

	QPointF	xy;

	if( intersection( centerLine, polygon, xy ) ) {
		setPos( xy );
		}
	}

void	BottomRightHandle::positionHasChanged( const QPointF delta ) {
	_sizer->offsetBottomRight( delta );
	}

void	BottomRightHandle::mousePressed() {
	_sizer->mousePressed();
	}

void	BottomRightHandle::mouseReleased() {
	_sizer->mouseReleased();
	}

QPointF BottomRightHandle::constrain( const QPointF& proposed ) const {

	QPointF			position = pos();
	const QRectF&	bounds	= _sizer->boundingRect();

	position.setY( proposed.y() );
	position.setX( proposed.x() );

	if( position.y() < bounds.top() ) {
		position.setY( bounds.top() );
		}

	if( position.x() < bounds.left() ) {
		position.setX( bounds.left() );
		}

	return position;
	}

BottomHandle::BottomHandle( Sizer* parent ):
	Handle( parent ),
	_sizer( parent )
	{}

void	BottomHandle::reposition() {

	QPainterPath	path		= _sizer->shape();	// in local coordinates
	QPolygonF		polygon		= path.toFillPolygon();	// in local coordinates
	QRectF			bound		= _sizer->boundingRect();

	bound.marginsAdded( QMarginsF( 1, 1, 1, 1 ) );

	QPointF	start( bound.left() + bound.width() / 2, bound.bottom() );
	QPointF	end( bound.left() + bound.width() / 2, bound.top() );
	QLineF 	centerLine( start, end );

	QPointF			xy;

	if( intersection( centerLine, polygon, xy ) ) {
		setPos( xy );
		}
	}

void	BottomHandle::positionHasChanged( const QPointF delta ) {
	_sizer->offsetBottom( delta.y() );
	}

void	BottomHandle::mousePressed() {
	_sizer->mousePressed();
	}

void	BottomHandle::mouseReleased() {
	_sizer->mouseReleased();
	}

QPointF BottomHandle::constrain( const QPointF& proposed ) const {

	QPointF			position = pos();
	const QRectF&	bounds	= _sizer->boundingRect();

	position.setY( proposed.y() );

	if( position.y() < bounds.top() ) {
		position.setY( bounds.top() );
		}

	return position;
	}

BottomLeftHandle::BottomLeftHandle( Sizer* parent ):
	Handle( parent ),
	_sizer( parent )
	{}

void	BottomLeftHandle::reposition() {

	QPainterPath	path		= _sizer->shape();	// in local coordinates
	QPolygonF		polygon		= path.toFillPolygon();	// in local coordinates
	QRectF			bound		= _sizer->boundingRect();

	bound.marginsAdded( QMarginsF( 1, 1, 1, 1) );

	QLineF	centerLine( bound.bottomLeft(), bound.topRight() );

	QPointF	xy;

	if( intersection( centerLine, polygon, xy ) ) {
		setPos( xy );
		}
	}

void	BottomLeftHandle::positionHasChanged( const QPointF delta ) {
	_sizer->offsetBottomLeft( delta );
	}

void	BottomLeftHandle::mousePressed() {
	_sizer->mousePressed();
	}

void	BottomLeftHandle::mouseReleased() {
	_sizer->mouseReleased();
	}

QPointF BottomLeftHandle::constrain( const QPointF& proposed ) const {

	QPointF			position = pos();
	const QRectF&	bounds	= _sizer->boundingRect();

	position.setY( proposed.y() );
	position.setX( proposed.x() );

	if( position.y() < bounds.top() ) {
		position.setY( bounds.top() );
		}

	if( position.x() > bounds.right() ) {
		position.setX( bounds.right() );
		}

	return position;
	}

LeftHandle::LeftHandle( Sizer* parent ):
	Handle( parent ),
	_sizer( parent )
	{}

void	LeftHandle::reposition() {

	QPainterPath	path		= _sizer->shape();	// in local coordinates
	QPolygonF		polygon		= path.toFillPolygon();	// in local coordinates
	QRectF			bound		= _sizer->boundingRect();

	bound.marginsAdded( QMarginsF( 1, 1, 1, 1 ) );

	QPointF	start( bound.left(), bound.top() + bound.height() / 2 );
	QPointF	end( bound.right(), bound.top() + bound.height() / 2 );
	QLineF 	centerLine( start, end );

	QPointF	xy;

	if( intersection( centerLine, polygon, xy ) ) {
		setPos( xy );
		}
	}

void	LeftHandle::positionHasChanged( const QPointF delta ) {
	_sizer->offsetLeft( delta.x() );
	}

void	LeftHandle::mousePressed() {
	_sizer->mousePressed();
	}

void	LeftHandle::mouseReleased() {
	_sizer->mouseReleased();
	}

QPointF LeftHandle::constrain( const QPointF& proposed ) const {

	QPointF			position = pos();
	const QRectF&	bounds	= _sizer->boundingRect();

	position.setX( proposed.x() );

	if( position.x() > bounds.right() ) {
		position.setX( bounds.right() );
		}

	return position;
	}

