/*
   Copyright (C) 2023 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_qt_graphics_item_sizerh_
#define _oscl_qt_graphics_item_sizerh_

#include <QGraphicsItem>
#include <QGraphicsEllipseItem>

#include "handle.h"
#include "resizeapi.h"

/** */
namespace Oscl {
/** */
namespace Qt {
/** */
namespace Graphics {
/** */
namespace Item {

/**	This concrete class is a QGraphicsItem
	that overlays a set of graphical sizing
	handles on the parent QGraphicsItem.
	When the user uses the mouse to move
	a handle, the parent is resized.
 */
class Sizer : public QGraphicsItem {

	/*	We permit full access to all
		of the Sizer handles.
	 */
	friend class TopHandle;
	friend class TopLeftHandle;
	friend class TopRightHandle;
	friend class BottomHandle;
	friend class BottomLeftHandle;
	friend class BottomRightHandle;
	friend class RightHandle;
	friend class LeftHandle;

	private:
		/**	Contains all of the handles.
		 */
		QList<Handle*>		_handles;

		/**	A reference to the interface
			used to notify the parent that
			it needs to be resized.
		 */
		ResizeApi&			_resizer;

	private:
		/** */
		QRectF				_bounds;

	public:
		/** */
		Sizer(
			ResizeApi&		resize,
			QGraphicsItem*	parent = 0
			);

		/** */
		virtual ~Sizer();

		/** */
		QPainterPath shape() const{
			return mapFromItem(parentItem(),parentItem()->shape());
			}

		/**	This operation may be invoked by the
			parent/context to make the handles
			visible or invisible. Initially, the
			handles are visible.
		 */
		void setVisibility(bool visible);

		/**	This operation may be invoked by the
			parent/context to notify the Sizer
			that the parent has changed size by
			a means other than the sizing handles.
			E.g., when the parent has been restored
			to a previous state as part of an undo
			operation.
		 */
		void	parentResized();

	private:
		/**	Though the Sizer itself is not visible,
			it still has a bounding rectangle that
			represents the locations of its handles.
		 */
		QRectF boundingRect() const override;

		/**	The Sizer itself does not actually
			have any graphical representation.
			But this operation must be implemented
			for the inherited QGraphicsItem.
		 */
		void	paint(
					QPainter* painter,
					const QStyleOptionGraphicsItem* option,
					QWidget* widget
					) override;

	private: // Operations required by the handles.
		/** Invoked to change the position of the
			top side of the Sizer's boundingRect.
			The parent/context is notified and all of
			the handles are relocated accordingly.
		 */
		void	offsetTop(qreal deltaY);

		/** Invoked to change the position of the
			right side of the Sizer's boundingRect.
			The parent/context is notified and all of
			the handles are relocated accordingly.
		 */
		void	offsetRight(qreal deltaX);

		/** Invoked to change the position of the
			bottom side of the Sizer's boundingRect.
			The parent/context is notified and all of
			the handles are relocated accordingly.
		 */
		void	offsetBottom(qreal deltaY);

		/** Invoked to change the position of the
			left side of the Sizer's boundingRect.
			The parent/context is notified and all of
			the handles are relocated accordingly.
		 */
		void	offsetLeft(qreal deltaX);

		/** Invoked to change the position of the
			top-right corner of the Sizer's boundingRect.
			The parent/context is notified and all of
			the handles are relocated accordingly.
		 */
		void	offsetTopRight(const QPointF& delta);

		/** Invoked to change the position of the
			bottom-right corner of the Sizer's boundingRect.
			The parent/context is notified and all of
			the handles are relocated accordingly.
		 */
		void	offsetBottomRight(const QPointF& delta);

		/** Invoked to change the position of the
			top-left corner of the Sizer's boundingRect.
			The parent/context is notified and all of
			the handles are relocated accordingly.
		 */
		void	offsetTopLeft(const QPointF& delta);

		/** Invoked to change the position of the
			bottom-left corner of the Sizer's boundingRect.
			The parent/context is notified and all of
			the handles are relocated accordingly.
		 */
		void	offsetBottomLeft(const QPointF& delta);

		/**	Invoked by handles when they receive
			a mouse-click. This notifies the
			parent/context that resizing is
			about to begin.
		 */
		void	mousePressed();

		/** */
		/**	Invoked by handles when they receive
			a mouse-released. This notifies the
			parent/context that resizing is complete.
		 */
		void	mouseReleased();

	private:	// Sizer mplementation helpers
		/** */
		void	updateHandles();

		/** */
		void	resize();
	};

}
}
}
}

#endif
