/*
   Copyright (C) 2023 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_qt_graphics_item_handlesh_
#define _oscl_qt_graphics_item_handlesh_

#include <QGraphicsItem>
#include <QGraphicsEllipseItem>
#include "handle.h"

/** */
namespace Oscl {
/** */
namespace Qt {
/** */
namespace Graphics {
/** */
namespace Item {

/** */
class TopLeftHandle : public Handle {
	private:
		/** */
		Sizer*		_sizer;

	public:
		/** */
		TopLeftHandle(Sizer* sizer);

	private:
		/** */
		void	reposition();

		/** */
		void	positionHasChanged(const QPointF delta);

		/** */
		QPointF constrain(const QPointF& proposed) const ;

		/** */
		void	mousePressed();

		/** */
		void	mouseReleased();
	};

/** */
class TopHandle : public Handle {
	private:
		/** */
		Sizer*		_sizer;

	public:
		/** */
		TopHandle(Sizer* sizer);

	private:
		/** */
		void	reposition();

		/** */
		void	positionHasChanged(const QPointF delta);

		/** */
		QPointF constrain(const QPointF& proposed) const ;

		/** */
		void	mousePressed();

		/** */
		void	mouseReleased();
	};

/** */
class TopRightHandle : public Handle {
	private:
		/** */
		Sizer*		_sizer;

	public:
		/** */
		TopRightHandle(Sizer* sizer);

	private:
		/** */
		void	reposition();

		/** */
		void	positionHasChanged(const QPointF delta);

		/** */
		QPointF constrain(const QPointF& proposed) const ;

		/** */
		void	mousePressed();

		/** */
		void	mouseReleased();
	};

/** */
class RightHandle : public Handle {
	private:
		/** */
		Sizer*		_sizer;

	public:
		/** */
		RightHandle(Sizer* sizer);

	private:
		/** */
		void	reposition();

		/** */
		void	positionHasChanged(const QPointF delta);

		/** */
		QPointF constrain(const QPointF& proposed) const ;

		/** */
		void	mousePressed();

		/** */
		void	mouseReleased();
	};

/** */
class BottomRightHandle : public Handle {
	private:
		/** */
		Sizer*		_sizer;

	public:
		/** */
		BottomRightHandle(Sizer* sizer);

	private:
		/** */
		void	reposition();

		/** */
		void	positionHasChanged(const QPointF delta);

		/** */
		QPointF constrain(const QPointF& proposed) const ;

		/** */
		void	mousePressed();

		/** */
		void	mouseReleased();
	};

/** */
class BottomHandle : public Handle {
	private:
		/** */
		Sizer*		_sizer;

	public:
		/** */
		BottomHandle(Sizer* sizer);

	private:
		/** */
		void	reposition();

		/** */
		void	positionHasChanged(const QPointF delta);

		/** */
		QPointF constrain(const QPointF& proposed) const ;

		/** */
		void	mousePressed();

		/** */
		void	mouseReleased();
	};

/** */
class BottomLeftHandle : public Handle {
	private:
		/** */
		Sizer*		_sizer;

	public:
		/** */
		BottomLeftHandle(Sizer* sizer);

	private:
		/** */
		void	reposition();

		/** */
		void	positionHasChanged(const QPointF delta);

		/** */
		QPointF constrain(const QPointF& proposed) const ;

		/** */
		void	mousePressed();

		/** */
		void	mouseReleased();
	};

/** */
class LeftHandle : public Handle {
	private:
		/** */
		Sizer*		_sizer;

	public:
		/** */
		LeftHandle(Sizer* sizer);

	private:
		/** */
		void	reposition();

		/** */
		void	positionHasChanged(const QPointF delta);

		/** */
		QPointF	constrain(const QPointF& proposed) const ;

		/** */
		void	mousePressed();

		/** */
		void	mouseReleased();
	};

}
}
}
}

#endif
