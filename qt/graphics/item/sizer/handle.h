/*
   Copyright (C) 2023 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_qt_graphics_item_handleh_
#define _oscl_qt_graphics_item_handleh_

#include <QGraphicsItem>
#include <QGraphicsEllipseItem>

/** */
namespace Oscl {
/** */
namespace Qt {
/** */
namespace Graphics {
/** */
namespace Item {

/** This class represents a Handle that is used
	to change the location of a point within
	the parent.
 */
class Handle : public QGraphicsRectItem {
	protected:
		/** This is used to track changes in the
			position of the handle between
			ItemPositionChange events.
		 */
		QPointF		_delta;

	public:
		/** */
		Handle(
			QGraphicsItem*	parent,
			qreal			size	= 6
			);

	public:
		/** This operation is invoked by the parent to
			reposition the handle after the context
			is moved. Ultimately, this should invoke
			setPos(x,y).
		 */
		virtual void	reposition()=0;

	protected:
		/** This operation is invoked to notify
			the parent that handle has moved
			by some amount delta.
		 */
		virtual void	positionHasChanged(const QPointF delta)=0;

	private:
		/** This operation is invoked to allow
			the parent to constrain the handle's
			movement to the proposed position
			(in local coordinates).
		 */
		virtual QPointF constrain(const QPointF& proposed) const =0;

		/** This operation is invoked to notify
			the parent that the mouse has been
			pressed on the handle. This happens
			before the handle is moved by the
			mouse.
		 */
		virtual void	mousePressed()=0;

		/** This operation is invoked to notify
			the parent that the mouse has been
			released on the handle. This happens
			after the handle is moved by the
			mouse.
		 */
		virtual void	mouseReleased()=0;

	private:
		/** */
		QVariant	itemChange(
						GraphicsItemChange	change,
						const QVariant&		value
						) override;

		/** */
		void	mousePressEvent( QGraphicsSceneMouseEvent* mouseEvent ) override;

		/** */
		void	mouseReleaseEvent( QGraphicsSceneMouseEvent* mouseEvent ) override;
	};

/**	This Composer is used by a Context to track the
	movement of a Handle.
 */
template <class Context> class HandleComposer : public Handle {
	private:
		/** */
		Context&	_context;
		/** */
		void		(Context::*_reposition)();
		/** */
		void		(Context::*_updateParent)(const QPointF& delta);
		/** */
		QPointF		(Context::*_constrain)(const QPointF& proposed) const;
		/** */
		void		(Context::*_mouseReleased)();
		/** */
		void		(Context::*_mousePressed)();

	public:
		/** */
		HandleComposer(
			QGraphicsItem*	parent,
			qreal			size,
			Context&		context,
			void			(Context::*reposition)(),
			void			(Context::*positionHasChanged)(const QPointF& delta),
			QPointF			(Context::*constrain)(const QPointF& proposed) const,
			void			(Context::*mouseReleased)(),
			void			(Context::*mousePressed)()
			):
			Handle(
				parent,
				size
				),
			_context(context),
			_reposition(reposition),
			_updateParent(positionHasChanged),
			_constrain(constrain),
			_mouseReleased(mouseReleased),
			_mousePressed(mousePressed)
			{}

		/** This operation is invoked by the context to
			reposition the handle after the context
			is moved. Ultimately, this should invoke
			setPos(x,y).
		 */
		void	reposition(){
			(_context.*_reposition)();
			}

	private:
		/** This operation is invoked by the handle,
			after the handle has moved in response
			to ItemPositionHasChanged, to update the
			position of the parent.
		 */
		void	positionHasChanged(const QPointF delta){
			(_context.*_updateParent)(delta);
			}

		/** This operation is invoked by the handle
			in response to a ItemPositionChange event.
			This gives the context the oportunity to
			constrain/restrict the movement of the handle.
		 */
		QPointF constrain(const QPointF& proposed) const{
			return (_context.*_constrain)(proposed);
			}

		/** This operation is invoked by the handle
			when the mouse is released on the handle.
		 */
		void	mouseReleased(){
			(_context.*_mouseReleased)();
			}

		/** This operation is invoked by the handle
			when the mouse is pressed on the handle.
		 */
		void	mousePressed(){
			(_context.*_mousePressed)();
			}
	};

}
}
}
}

#endif
