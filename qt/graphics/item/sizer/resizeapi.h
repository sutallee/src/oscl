/*
   Copyright (C) 2023 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_qt_graphics_item_resizeapih_
#define _oscl_qt_graphics_item_resizeapih_

#include <QRectF>

/** */
namespace Oscl {
/** */
namespace Qt {
/** */
namespace Graphics {
/** */
namespace Item {

/**	This interface is used to notify the
	sizer's parent/context as its size is
	changed.

	The sequence of a resizing follows:

	1. When the user clicks on one of the sizing
	handles, the mousePressed() operation is
	invoked. This allows the parent/context to
	prepare for being resized.

	2. As one of the sizing handles is moved, the
	resize() operation is invoked such that the
	context/parent can update its graphical
	representation to match its newly calculated
	bounds. This operation may be invoked zero or
	more times between mousePressed() and
	mouseReleased()

	3. Finally, mouseReleased() is invoked when the
	user is finished moving a handle. This allows
	the parent/context to take any additional
	actions after the resizing is complete.

	The mousePressed() and mouseReleased() operations
	allow the parent/context to bracket the time
	when resizing is in progress.
 */
class ResizeApi {
	public:
		/**	This operation is invoked to notify
			the parent/context of it's new size.
			The bounds are in the parent's local
			coordinates.
		 */
		virtual void	resize(const QRectF& bounds) = 0;

		/**	This operation is invoked to notify the
			parent/context at the start of a resizing
			operation. That is to say that one of the
			handles is about to be dragged.
		 */
		virtual void	mousePressed() = 0;

		/**	This operation is invoked to notify the
			parent/context at the end of a resizing
			operation. This is invoked after all resizing
			is complete.
		 */
		virtual void	mouseReleased() = 0;
	};

/**	This Composer enables a parent/context to use
	composition instead of inheritance to implement
	the ResizeApi.
 */
template <class Context> class ResizeComposer : public ResizeApi {
	private:
		/** */
		Context&	_context;

		/** */
		void		(Context::*_resize)(const QRectF& bounds);

		/** */
		void		(Context::*_mousePressed)();

		/** */
		void		(Context::*_mouseReleased)();

	public:
		/** */
		ResizeComposer(
			Context&	context,
			void		(Context::*resize)(const QRectF& bounds),
			void		(Context::*mousePressed)() = 0,
			void		(Context::*mouseReleased)() = 0
			):
			_context(context),
			_resize(resize),
			_mousePressed( mousePressed ),
			_mouseReleased( mouseReleased )
			{}

	private:
		/** */
		void	resize(const QRectF& bounds){
			(_context.*_resize)(bounds);
			}

		/** */
		void	mousePressed(){
			if( _mousePressed ) {
				(_context.*_mousePressed)();
				}
			}

		/** */
		void	mouseReleased(){
			if( _mouseReleased ) {
				(_context.*_mouseReleased)();
				}
			}
	};

}
}
}
}

#endif
