/*
   Copyright (C) 2023 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <iostream>
#include <QBrush>
#include <QLineF>
#include <QBuffer>
#include <QtCore>
#include "sizer.h"
#include "handles.h"

using namespace Oscl::Qt::Graphics::Item;

Sizer::Sizer(
	ResizeApi&		resize,
	QGraphicsItem*	parent
	):
	QGraphicsItem( parent ),
	_resizer( resize )
	{

	if( parentItem() ) {
		_bounds = parentItem()->boundingRect();
		}

	_handles << new TopHandle( this );
	_handles << new BottomHandle( this );
	_handles << new RightHandle( this );
	_handles << new LeftHandle( this );

	_handles << new TopLeftHandle( this );
	_handles << new TopRightHandle( this );

	_handles << new BottomLeftHandle( this );
	_handles << new BottomRightHandle( this );

	updateHandles();
	}

Sizer::~Sizer() {
	}

QRectF Sizer::boundingRect() const {
	return _bounds;
	}

void Sizer::paint(
		QPainter*						painter,
		const QStyleOptionGraphicsItem*	option,
		QWidget*						widget
		) {
	// Nothing to paint
	}

void Sizer::offsetTopLeft( const QPointF& delta ) {

	_bounds.setTop( _bounds.top() + delta.y() );

	_bounds.setLeft( _bounds.left() + delta.x() );

	resize();
	}

void Sizer::offsetTop( qreal deltaY ) {
	_bounds.setTop( _bounds.top() + deltaY );
	resize();
	}

void Sizer::offsetTopRight( const QPointF& delta ) {
	_bounds.setTop( _bounds.top() + delta.y() );
	_bounds.setRight( _bounds.right() + delta.x() );
	resize();
	}

void Sizer::offsetRight( qreal deltaX ) {
	_bounds.setRight( _bounds.right() + deltaX );
	resize();
	}

void Sizer::offsetBottomRight( const QPointF& delta ) {
	_bounds.setBottom( _bounds.bottom() + delta.y() );
	_bounds.setRight( _bounds.right() + delta.x() );
	resize();
	}

void Sizer::offsetBottom( qreal deltaY ) {
	_bounds.setBottom( _bounds.bottom() + deltaY );
	resize();
	}

void Sizer::offsetBottomLeft( const QPointF& delta ) {
	_bounds.setBottom( _bounds.bottom() + delta.y() );
	_bounds.setLeft( _bounds.left() + delta.x() );
	resize();
	}

void Sizer::offsetLeft( qreal deltaX ) {
	_bounds.setLeft( _bounds.left() + deltaX );
	resize();
	}

void	Sizer::parentResized() {

	if( parentItem() ) {
		_bounds = parentItem()->boundingRect();
		}

	updateHandles();
	}

void Sizer::updateHandles() {

	foreach( Handle* item, _handles ) {
		item->setFlag( ItemSendsGeometryChanges, false );
		item->reposition();
		item->setFlag( ItemSendsGeometryChanges, true );
		}
	}

void Sizer::resize() {

	_resizer.resize( _bounds );
	updateHandles();
	}

void	Sizer::mousePressed() {
	_resizer.mousePressed();
	}

void	Sizer::mouseReleased() {
	_resizer.mouseReleased();
	}

void Sizer::setVisibility( bool visible ) {
	foreach ( Handle* item, _handles ) {
		item->setVisible( visible );
		}
	}

