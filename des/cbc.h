/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_des_cbch_
#define _oscl_des_cbch_

/** */
namespace Oscl {
/** */
namespace DES {
/** */
namespace CBC {

enum{keySize=8};

/** This function prototype matches either the
	DES "encrypt" or "decrypt" operation.
 */
typedef unsigned (*Crypt)(	const unsigned char	key[keySize],
							const void*			input,
							unsigned			inputLength,
							void*				output,
							unsigned			maxOutputLength
							);

/** This operation performs DES encryption on the "input"
	and puts the result in the "output". Note that the size
	of the output *may* be larger than the input such that
	the output is padded to an even multiple of 8 bytes.
	The size of the output (including padding) is returned.
 */
unsigned	encrypt(	const unsigned char	key[keySize],
						const void*			input,
						unsigned			inputLength,
						void*				output,
						unsigned			maxOutputLength
						) noexcept;

/** This operation performs DES decryption on the "input"
	and puts the result in the "output". Note that the
	output will be exactly the same size as the input,
	and that the output will contain any end padding 
	included when the data was first encrypted. The "inputLength"
	*MUST* be an even multiple of 8 bytes. The size of
	the output is returned.
 */
unsigned	decrypt(	const unsigned char	key[keySize],
						const void*			input,
						unsigned			inputLength,
						void*				output,
						unsigned			maxOutputLength
						) noexcept;

}
}
}

#endif
