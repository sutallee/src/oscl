/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "type.h"

/*	Implementation format

	
    ******* Format of CD **************************
	The 56 bit quantity CD is the concatenation
	of 28 bits of C as bits 1:28 and D as bits
	29:56.

    ******* Format of C ***************************
    |<------------------------ 32 bits ---------->|
                |<---------28 bits -------------->|
     31 30 29 28 27 26 25 24            3  2  1  0  word bit number
    +--+--+--+--+--+--+--+--+---------+--+--+--+--+
    |xx|xx|xx|xx|57|49|41|33| - - - - |60|52|44|36| Key bit number
    +--+--+--+--+--+--+--+--+---------+--+--+--+--+
                  1  2  3  4           25 26 27 28  CD bit number

    ******* Format of D ***************************
    |<------------------------ 32 bits ---------->|
                |<---------28 bits -------------->|
     31 30 29 28 27 26 25 24            3  2  1  0  word bit number
    +--+--+--+--+--+--+--+--+---------+--+--+--+--+
    |xx|xx|xx|xx|63|55|47|39| - - - - |28|20|12| 4| Key bit number
    +--+--+--+--+--+--+--+--+---------+--+--+--+--+
                 29 30 31 32           53 54 55 56  CD bit number

    ******* Format of 64-bit Key ******************
       |<-----8 bits---------->|
         7  6  5  4  3  2  1  0
    -  +--+--+--+--+--+--+--+--+
    ^  | 1| 2| 3| 4| 5| 6| 7| 8| [0]
    |  +--+--+--+--+--+--+--+--+
    |  | 9|10|11|12|13|14|15|16| [1]
    |  +--+--+--+--+--+--+--+--+
    |  |17|18|19|20|21|22|23|24| [2]
    8  +--+--+--+--+--+--+--+--+
 bytes |25|26|27|28|29|30|31|32| [3]
    |  +--+--+--+--+--+--+--+--+
    |  |33|34|35|36|37|38|39|40| [4]
    |  +--+--+--+--+--+--+--+--+
    |  |41|42|43|44|45|46|47|48| [5]
    |  +--+--+--+--+--+--+--+--+
    |  |49|50|51|52|53|54|55|56| [6]
    |  +--+--+--+--+--+--+--+--+
    V  |57|58|59|60|61|62|64|64| [7]
    -  +--+--+--+--+--+--+--+--+
    
    ******* Format of 64-bit Block ****************
     Examples are the output of IP and IP(-1)
     Example LR.
       |<-----8 bits---------->|
         7  6  5  4  3  2  1  0
    -  +--+--+--+--+--+--+--+--+
    ^  | 1| 2| 3| 4| 5| 6| 7| 8| [0]
    |  +--+--+--+--+--+--+--+--+
    |  | 9|10|11|12|13|14|15|16| [1]
    |  +--+--+--+--+--+--+--+--+
    |  |17|18|19|20|21|22|23|24| [2]
    8  +--+--+--+--+--+--+--+--+
 bytes |25|26|27|28|29|30|31|32| [3]
    |  +--+--+--+--+--+--+--+--+
    |  |33|34|35|36|37|38|39|40| [4]
    |  +--+--+--+--+--+--+--+--+
    |  |41|42|43|44|45|46|47|48| [5]
    |  +--+--+--+--+--+--+--+--+
    |  |49|50|51|52|53|54|55|56| [6]
    |  +--+--+--+--+--+--+--+--+
    V  |57|58|59|60|61|62|64|64| [7]
    -  +--+--+--+--+--+--+--+--+
    
    ******* Format of 64-bit LR  ****************
       |<-----8 bits---------->|
         7  6  5  4  3  2  1  0
    -  +--+--+--+--+--+--+--+--+------
    ^  | 1| 2| 3| 4| 5| 6| 7| 8| [0] ^
    |  +--+--+--+--+--+--+--+--+     |
    |  | 9|10|11|12|13|14|15|16| [1] |
    |  +--+--+--+--+--+--+--+--+     L
    |  |17|18|19|20|21|22|23|24| [2] |
    8  +--+--+--+--+--+--+--+--+     |
 bytes |25|26|27|28|29|30|31|32| [3] V
    |  +--+--+--+--+--+--+--+--+------
    |  |33|34|35|36|37|38|39|40| [4] ^
    |  +--+--+--+--+--+--+--+--+     |
    |  |41|42|43|44|45|46|47|48| [5] |
    |  +--+--+--+--+--+--+--+--+     R
    |  |49|50|51|52|53|54|55|56| [6] |
    |  +--+--+--+--+--+--+--+--+     |
    V  |57|58|59|60|61|62|64|64| [7] V
    -  +--+--+--+--+--+--+--+--+------

    ******* Format of 32-bit L ****************
       |<-----8 bits---------->|
         7  6  5  4  3  2  1  0
    -  +--+--+--+--+--+--+--+--+
    ^  | 1| 2| 3| 4| 5| 6| 7| 8| [0]
    |  +--+--+--+--+--+--+--+--+
    4  | 9|10|11|12|13|14|15|16| [1]
 bytes +--+--+--+--+--+--+--+--+
    |  |17|18|19|20|21|22|23|24| [2]
    |  +--+--+--+--+--+--+--+--+
    V  |25|26|27|28|29|30|31|32| [3]
    -  +--+--+--+--+--+--+--+--+
    
    ******* Format of 32-bit R ****************
       |<-----8 bits---------->|
         7  6  5  4  3  2  1  0
    -  +--+--+--+--+--+--+--+--+
    ^  | 1| 2| 3| 4| 5| 6| 7| 8| [0]
    |  +--+--+--+--+--+--+--+--+
    4  | 9|10|11|12|13|14|15|16| [1]
 bytes +--+--+--+--+--+--+--+--+
    |  |17|18|19|20|21|22|23|24| [2]
    |  +--+--+--+--+--+--+--+--+
    V  |25|26|27|28|29|30|31|32| [3]
    -  +--+--+--+--+--+--+--+--+
    
    ******* Format of 8-bit input to S() ****************
       |<-----8 bits---------->|
       |     |<-- 6 bits ----->|
       | 7  6| 5  4  3  2  1  0
       +--+--+--+--+--+--+--+--+
       | x| x| 1| 2| 3| 4| 5| 6|
       +--+--+--+--+--+--+--+--+

    ******* Format of 48-bit **************
	o Output of E() conversion 
		- E() converts a 32-bit R to a 48-bit format
		  used in f(R,K)
	o Output of PC2() conversion (Kn)
		- PC2() converts a 56 bit (2x28-bit words)
		  to a 48-bit K format f(R,K)

	These two output formats are XOR'd
	together during f(R,K)

       |<-----8 bits---------->|
             |<---- 6 bits --->|
         7  6| 5  4  3  2  1  0
    -  +--+--+--+--+--+--+--+--+
    ^  | x| x| 1| 2| 3| 4| 5| 6| [0]
    |  +--+--+--+--+--+--+--+--+
    |  | x| x| 7| 8| 9|10|11|12| [1]
    |  +--+--+--+--+--+--+--+--+
    |  | x| x|13|14|15|16|17|18| [2]
    8  +--+--+--+--+--+--+--+--+
 bytes | x| x|19|20|21|22|23|24| [3]
    |  +--+--+--+--+--+--+--+--+
    |  | x| x|25|26|27|28|29|30| [4]
    |  +--+--+--+--+--+--+--+--+
    |  | x| x|31|32|33|34|35|36| [5]
    |  +--+--+--+--+--+--+--+--+
    |  | x| x|37|38|39|40|41|42| [6]
    |  +--+--+--+--+--+--+--+--+
    V  | x| x|43|44|45|46|47|48| [7]
    -  +--+--+--+--+--+--+--+--+

    ******* Format of 48-bit PC2 ******************
       |<-----8 bits---------->|
         7  6  5  4  3  2  1  0
    -  +--+--+--+--+--+--+--+--+
    ^  | 1| 2| 3| 4| 5| 6| 7| 8| [0]
    |  +--+--+--+--+--+--+--+--+
    |  | 9|10|11|12|13|14|15|16| [1]
    |  +--+--+--+--+--+--+--+--+
    |  |17|18|19|20|21|22|23|24| [2]
    8  +--+--+--+--+--+--+--+--+
 bytes |25|26|27|28|29|30|31|32| [3]
    |  +--+--+--+--+--+--+--+--+
    |  |33|34|35|36|37|38|39|40| [4]
    |  +--+--+--+--+--+--+--+--+
    |  |41|42|43|44|45|46|47|48| [5]
    |  +--+--+--+--+--+--+--+--+
    |  |49|50|51|52|53|54|55|56| [6]
    |  +--+--+--+--+--+--+--+--+
    V  |57|58|59|60|61|62|64|64| [7]
    -  +--+--+--+--+--+--+--+--+
 */

static const unsigned char	stable[8][64]	= {
	{
	14, 0, 4,15,13, 7, 1, 4, 2,14,15, 2,11,13, 8, 1,
	 3,10,10, 6, 6,12,12,11, 5, 9, 9, 5, 0, 3, 7, 8,
	 4,15, 1,12,14, 8, 8, 2,13, 4, 6, 9, 2, 1,11, 7,
	15, 5,12,11, 9, 3, 7,14, 3,10,10, 0, 5, 6, 0,13
	},
	{
	15, 3, 1,13, 8, 4,14, 7, 6,15,11, 2, 3, 8, 4,14,
	 9,12, 7, 0, 2, 1,13,10,12, 6, 0, 9, 5,11,10, 5,
	 0,13,14, 8, 7,10,11, 1,10, 3, 4,15,13, 4, 1, 2,
	 5,11, 8, 6,12, 7, 6,12, 9, 0, 3, 5, 2,14,15, 9
	},
	{
	10,13, 0, 7, 9, 0,14, 9, 6, 3, 3, 4,15, 6, 5,10,
	 1, 2,13, 8,12, 5, 7,14,11,12, 4,11, 2,15, 8, 1,
	13, 1, 6,10, 4,13, 9, 0, 8, 6,15, 9, 3, 8, 0, 7,
	11, 4, 1,15, 2,14,12, 3, 5,11,10, 5,14, 2, 7,12
	},
	{
	 7,13,13, 8,14,11, 3, 5, 0, 6, 6,15, 9, 0,10, 3,
	 1, 4, 2, 7, 8, 2, 5,12,11, 1,12,10, 4,14,15, 9,
	10, 3, 6,15, 9, 0, 0, 6,12,10,11, 1, 7,13,13, 8,
	15, 9, 1, 4, 3, 5,14,11, 5,12, 2, 7, 8, 2, 4,14
	},
	{
	 2,14,12,11, 4, 2, 1,12, 7, 4,10, 7,11,13, 6, 1,
	 8, 5, 5, 0, 3,15,15,10,13, 3, 0, 9,14, 8, 9, 6,
	 4,11, 2, 8, 1,12,11, 7,10, 1,13,14, 7, 2, 8,13,
	15, 6, 9,15,12, 0, 5, 9, 6,10, 3, 4, 0, 5,14, 3
	},
	{
	12,10, 1,15,10, 4,15, 2, 9, 7, 2,12, 6, 9, 8, 5,
	 0, 6,13, 1, 3,13, 4,14,14, 0, 7,11, 5, 3,11, 8,
	 9, 4,14, 3,15, 2, 5,12, 2, 9, 8, 5,12,15, 3,10,
	 7,11, 0,14, 4, 1,10, 7, 1, 6,13, 0,11, 8, 6,13
	},
	{
	 4,13,11, 0, 2,11,14, 7,15, 4, 0, 9, 8, 1,13,10,
	 3,14,12, 3, 9, 5, 7,12, 5, 2,10,15, 6, 8, 1, 6,
	 1, 6, 4,11,11,13,13, 8,12, 1, 3, 4, 7,10,14, 7,
	10, 9,15, 5, 6, 0, 8,15, 0,14, 5, 2, 9, 3, 2,12
	},
	{
	13, 1, 2,15, 8,13, 4, 8, 6,10,15, 3,11, 7, 1, 4,
	10,12, 9, 5, 3, 6,14,11, 5, 0, 0,14,12, 9, 7, 2,
	 7, 2,11, 1, 4,14, 1, 7, 9, 4,12,10,14, 8, 2,13,
	 0,15, 6,12,10, 9,13, 0,15, 3, 3, 5, 5, 6, 8,11
	}
};

template <unsigned sn>
inline unsigned char S(unsigned char input){
	return stable[sn-1][input&0x3F];
}

///// Simple Conversions

// bit		= <63..0>
// result	= <7..0>
static inline unsigned keyByteFromBit(unsigned bit) noexcept{
	return 7 - (bit/8);
	}

// keyBit	= <1..64>
// result	= <7..0>
static inline unsigned keyByteFromKeyBit(unsigned keyBit) noexcept{
	return (keyBit-1)/8;
	}

// keyBit	= <1..64>
// bit		= <63..0>
static inline unsigned bitFromKeyBit(unsigned keyBit) noexcept{
	return 64 - keyBit;
	}

// cdBit	= <1..56>
// result	= <55..0>
static inline unsigned bitFromCdBit(unsigned cdBit) noexcept{
	return 56-cdBit;
	}

// bit		= <55..0>
// result	= <1..56>
static inline unsigned cdBitBitFromBit(unsigned bit) noexcept{
	return (55-bit)+1;
	}

// Convert cdBit number to "little-bitendian" word bit number.
// cdBit	= <28..1>
// result	= <27..0>
static inline unsigned	wordBitFromCdBit(unsigned cdBit) noexcept{
	return ((28-cdBit) % 28);
	}

static inline unsigned long	rotateLeft28(unsigned long l,unsigned n) noexcept{
	l	<<= n;
	unsigned long	carry	= l;
	carry	>>= 28;	
	l	|=	carry;
	l	&= 0x0FFFFFFF;
	return l;
	}

static inline void	rotateLeft(	unsigned long&	c,
								unsigned long&	d,
								unsigned		n
								) noexcept{
	c	= rotateLeft28(c,n);
	d	= rotateLeft28(d,n);
	}

///// End Simple Conversions

static inline unsigned char	pc2Bit(	unsigned long	cd,
									unsigned		cdBit,
									unsigned char	keyBit
									){
	const unsigned		wordBit	= wordBitFromCdBit(cdBit);
	const unsigned		bit		= bitFromKeyBit(keyBit)%8;
	cd	>>= wordBit;
	cd	<<= bit;
	return cd & (1<<bit);
	}

// kbit			= <1..64>
// cdOutputBit	= <1..28>
static inline unsigned long	keyBit(	const unsigned char	key[8],
									unsigned char		kbit,
									unsigned char		cdOutputBit
									){
	const unsigned		bit			= bitFromKeyBit(kbit);
	const unsigned		outputBit	= wordBitFromCdBit(cdOutputBit);
	const unsigned		keyByte		= keyByteFromBit(bit);
	const unsigned		keyBit		= (bit%8);
	unsigned	long	mask		= (key[keyByte] & (1<<keyBit));
	mask	>>= keyBit;
	mask	<<= outputBit;
	return mask;
	}

static void	PC1(	const unsigned char key[8],
					unsigned long&		c,
					unsigned long&		d
					){
	unsigned long	C;
	unsigned long	D;

	C		=	keyBit(key,57, 1);
	C		|=	keyBit(key,49, 2);
	C		|=	keyBit(key,41, 3);
	C		|=	keyBit(key,33, 4);
	C		|=	keyBit(key,25, 5);
	C		|=	keyBit(key,17, 6);
	C		|=	keyBit(key, 9, 7);
	C		|=	keyBit(key, 1, 8);
	C		|=	keyBit(key,58, 9);
	C		|=	keyBit(key,50,10);
	C		|=	keyBit(key,42,11);
	C		|=	keyBit(key,34,12);
	C		|=	keyBit(key,26,13);
	C		|=	keyBit(key,18,14);
	C		|=	keyBit(key,10,15);
	C		|=	keyBit(key, 2,16);
	C		|=	keyBit(key,59,17);
	C		|=	keyBit(key,51,18);
	C		|=	keyBit(key,43,19);
	C		|=	keyBit(key,35,20);
	C		|=	keyBit(key,27,21);
	C		|=	keyBit(key,19,22);
	C		|=	keyBit(key,11,23);
	C		|=	keyBit(key, 3,24);
	C		|=	keyBit(key,60,25);
	C		|=	keyBit(key,52,26);
	C		|=	keyBit(key,44,27);
	C		|=	keyBit(key,36,28);

	D		=	keyBit(key,63, 1);
	D		|=	keyBit(key,55, 2);
	D		|=	keyBit(key,47, 3);
	D		|=	keyBit(key,39, 4);
	D		|=	keyBit(key,31, 5);
	D		|=	keyBit(key,23, 6);
	D		|=	keyBit(key,15, 7);
	D		|=	keyBit(key, 7, 8);
	D		|=	keyBit(key,62, 9);
	D		|=	keyBit(key,54,10);
	D		|=	keyBit(key,46,11);
	D		|=	keyBit(key,38,12);
	D		|=	keyBit(key,30,13);
	D		|=	keyBit(key,22,14);
	D		|=	keyBit(key,14,15);
	D		|=	keyBit(key, 6,16);
	D		|=	keyBit(key,61,17);
	D		|=	keyBit(key,53,18);
	D		|=	keyBit(key,45,19);
	D		|=	keyBit(key,37,20);
	D		|=	keyBit(key,29,21);
	D		|=	keyBit(key,21,22);
	D		|=	keyBit(key,13,23);
	D		|=	keyBit(key, 5,24);
	D		|=	keyBit(key,28,25);
	D		|=	keyBit(key,20,26);
	D		|=	keyBit(key,12,27);
	D		|=	keyBit(key, 4,28);

	d	= D;
	c	= C;
	}

static void	PC2(	unsigned char		key[8],
					unsigned long		c,
					unsigned long		d
					){
	key[0]	= (unsigned char)pc2Bit(c,14, 1);
	key[0]	|= (unsigned char)pc2Bit(c,17, 2);
	key[0]	|= (unsigned char)pc2Bit(c,11, 3);
	key[0]	|= (unsigned char)pc2Bit(c,24, 4);
	key[0]	|= (unsigned char)pc2Bit(c, 1, 5);
	key[0]	|= (unsigned char)pc2Bit(c, 5, 6);

	key[1]	= (unsigned char)pc2Bit(c, 3, 7);
	key[1]	|= (unsigned char)pc2Bit(c,28, 8);
	key[1]	|= (unsigned char)pc2Bit(c,15, 9);
	key[1]	|= (unsigned char)pc2Bit(c, 6,10);
	key[1]	|= (unsigned char)pc2Bit(c,21,10);
	key[1]	|= (unsigned char)pc2Bit(c,10,10);

	key[2]	= (unsigned char)pc2Bit(c,23,10);
	key[2]	|= (unsigned char)pc2Bit(c,19,10);
	key[2]	|= (unsigned char)pc2Bit(c,12,10);
	key[2]	|= (unsigned char)pc2Bit(c, 4,10);
	key[2]	|= (unsigned char)pc2Bit(c,26,10);
	key[2]	|= (unsigned char)pc2Bit(c, 8,10);

	key[3]	= (unsigned char)pc2Bit(c,16,10);
	key[3]	|= (unsigned char)pc2Bit(c, 7,20);
	key[3]	|= (unsigned char)pc2Bit(c,27,20);
	key[3]	|= (unsigned char)pc2Bit(c,20,20);
	key[3]	|= (unsigned char)pc2Bit(c,13,20);
	key[3]	|= (unsigned char)pc2Bit(c, 2,20);

	key[4]	= (unsigned char)pc2Bit(d,41,20);
	key[4]	|= (unsigned char)pc2Bit(d,52,20);
	key[4]	|= (unsigned char)pc2Bit(d,31,20);
	key[4]	|= (unsigned char)pc2Bit(d,37,20);
	key[4]	|= (unsigned char)pc2Bit(d,47,20);
	key[4]	|= (unsigned char)pc2Bit(d,55,30);

	key[5]	= (unsigned char)pc2Bit(d,30,30);
	key[5]	|= (unsigned char)pc2Bit(d,40,30);
	key[5]	|= (unsigned char)pc2Bit(d,51,30);
	key[5]	|= (unsigned char)pc2Bit(d,45,30);
	key[5]	|= (unsigned char)pc2Bit(d,33,30);
	key[5]	|= (unsigned char)pc2Bit(d,48,30);

	key[6]	= (unsigned char)pc2Bit(d,44,30);
	key[6]	|= (unsigned char)pc2Bit(d,49,30);
	key[6]	|= (unsigned char)pc2Bit(d,39,30);
	key[6]	|= (unsigned char)pc2Bit(d,56,40);
	key[6]	|= (unsigned char)pc2Bit(d,34,40);
	key[6]	|= (unsigned char)pc2Bit(d,53,40);

	key[7]	= (unsigned char)pc2Bit(d,46,40);
	key[7]	|= (unsigned char)pc2Bit(d,42,40);
	key[7]	|= (unsigned char)pc2Bit(d,50,40);
	key[7]	|= (unsigned char)pc2Bit(d,36,40);
	key[7]	|= (unsigned char)pc2Bit(d,29,40);
	key[7]	|= (unsigned char)pc2Bit(d,32,40);
	}

using namespace Oscl::DES;

Type32::Type32(const unsigned char value[4]) noexcept
		{
	for(unsigned i=0;i<4;++i){
		_value[i]	= value[i];
		}
	}

Type32::Type32() noexcept
		{
	}

Type32&	Type32::operator ^= (const Type32& other) noexcept{
	for(unsigned i=0;i<4;++i){
		_value[i]	^= other._value[i];
		}
	return *this;
	}

#if 0
void	Type32::p(	unsigned char	s1,
					unsigned char	s2,
					unsigned char	s3,
					unsigned char	s4,
					unsigned char	s5,
					unsigned char	s6,
					unsigned char	s7,
					unsigned char	s8
					) noexcept{
	// FIXME: This could/should be inlined.
	_value[0]	= (s1<<4) | s2;
	_value[1]	= (s3<<4) | s3;
	_value[2]	= (s5<<4) | s6;
	_value[3]	= (s7<<4) | s8;
	}
#endif

static inline void	E(	unsigned char		output[8],
						const unsigned char	input[4]
						) noexcept{
	unsigned long	in;
	in	=	input[0];
	in	<<=	8;
	in	=	input[1];
	in	<<=	8;
	in	=	input[2];
	in	<<=	8;
	in	=	input[3];
	in	= (in << 1) | ((in >> 31) & 0x01);
	output[7]	= (in & 0x3F);
	in	>>=	4;
	output[6]	= (in & 0x3F);
	in	>>=	4;
	output[5]	= (in & 0x3F);
	in	>>=	4;
	output[4]	= (in & 0x3F);
	in	>>=	4;
	output[3]	= (in & 0x3F);
	in	>>=	4;
	output[2]	= (in & 0x3F);
	in	>>=	4;
	output[1]	= (in & 0x3F);
	in	>>=	4;
	output[0]	= (in & 0x3F);
	in	>>=	4;
	}

void	Type32::e(unsigned char	output[8]) const noexcept{
	E(output,_value);
	}

void	Type32::read(unsigned char output[4]) const noexcept{
	for(unsigned i=0;i<4;++i){
		output[i]	= _value[i];
		}
	}

Type48::Type48() noexcept
		{
	}

Type48::Type48(const Type32& r) noexcept{
	// Convert from 32 to 48-bit
	r.e(_value);
	}

void	Type48::pc2(	unsigned long	c,
						unsigned long	d
						) noexcept{
	PC2(_value,c,d);
	}

void	Type48::f(	Type32&			output,
					const Type48&	keyn
					) noexcept{
	for(unsigned i=0;i<8;++i){
		_value[i]	^= keyn._value[i];
		}
	output.p(	S<1>(_value[0]),
				S<2>(_value[1]),
				S<3>(_value[2]),
				S<4>(_value[3]),
				S<5>(_value[4]),
				S<6>(_value[5]),
				S<7>(_value[6]),
				S<8>(_value[7])
				);
	}

static void	kn(	Type48&			output,
				unsigned long&	c,
				unsigned long&	d,
				unsigned		n
				) noexcept{
	switch(n){
		case 1:
			rotateLeft(c,d,1);
			break;
		case 2:
			rotateLeft(c,d,1);
			break;
		case 3:
			rotateLeft(c,d,2);
			break;
		case 4:
			rotateLeft(c,d,2);
			break;
		case 5:
			rotateLeft(c,d,2);
			break;
		case 6:
			rotateLeft(c,d,2);
			break;
		case 7:
			rotateLeft(c,d,2);
			break;
		case 8:
			rotateLeft(c,d,2);
			break;
		case 9:
			rotateLeft(c,d,1);
			break;
		case 10:
			rotateLeft(c,d,2);
			break;
		case 11:
			rotateLeft(c,d,2);
			break;
		case 12:
			rotateLeft(c,d,2);
			break;
		case 13:
			rotateLeft(c,d,2);
			break;
		case 14:
			rotateLeft(c,d,2);
			break;
		case 15:
			rotateLeft(c,d,2);
			break;
		case 16:
			rotateLeft(c,d,1);
			break;
		default:
			for(;;);
		}
	output.pc2(c,d);
	}

KeySched::KeySched(const unsigned char key[8]) noexcept
		{
	unsigned long	c;
	unsigned long	d;
	PC1(key,c,d);
	kn(_key1,c,d,1);
	kn(_key2,c,d,2);
	kn(_key3,c,d,3);
	kn(_key4,c,d,4);
	kn(_key5,c,d,5);
	kn(_key6,c,d,6);
	kn(_key7,c,d,7);
	kn(_key8,c,d,8);
	kn(_key9,c,d,9);
	kn(_key10,c,d,10);
	kn(_key11,c,d,11);
	kn(_key12,c,d,12);
	kn(_key13,c,d,13);
	kn(_key14,c,d,14);
	kn(_key15,c,d,16);
	kn(_key16,c,d,16);
	}

Type64::Type64(unsigned char block[8]) noexcept:
		_value(block)
		{
	}

static void	ipBit(	const unsigned char	input[8],
							unsigned char		output[8],
							unsigned			inputBit,
							unsigned			outputBit
							) noexcept{
	unsigned	inByte	= (inputBit-1)/8;
	unsigned	inBit	= 7-((inputBit-1)%8);
	unsigned	outByte	= (outputBit-1)/8;
	unsigned	outBit	= 7-((outputBit-1)%8);
	output[outByte]	|= ((input[inByte] >> inBit) << outBit) & (1<<outBit);
	}

void	Type64::ip(Type64& output) const noexcept{
	for(unsigned i=0;i<8;++i){
		output._value[i]	= 0;
		}
	ipBit(_value,output._value,58, 1);
	ipBit(_value,output._value,50, 2);
	ipBit(_value,output._value,42, 3);
	ipBit(_value,output._value,34, 4);
	ipBit(_value,output._value,26, 5);
	ipBit(_value,output._value,18, 6);
	ipBit(_value,output._value,10, 7);
	ipBit(_value,output._value, 2, 8);

	ipBit(_value,output._value,60, 9);
	ipBit(_value,output._value,52,10);
	ipBit(_value,output._value,44,11);
	ipBit(_value,output._value,36,12);
	ipBit(_value,output._value,28,13);
	ipBit(_value,output._value,20,14);
	ipBit(_value,output._value,12,15);
	ipBit(_value,output._value, 4,16);

	ipBit(_value,output._value,62,17);
	ipBit(_value,output._value,54,18);
	ipBit(_value,output._value,46,19);
	ipBit(_value,output._value,38,20);
	ipBit(_value,output._value,30,21);
	ipBit(_value,output._value,22,22);
	ipBit(_value,output._value,14,23);
	ipBit(_value,output._value, 6,24);

	ipBit(_value,output._value,64,25);
	ipBit(_value,output._value,56,26);
	ipBit(_value,output._value,48,27);
	ipBit(_value,output._value,40,28);
	ipBit(_value,output._value,32,29);
	ipBit(_value,output._value,24,30);
	ipBit(_value,output._value,16,31);
	ipBit(_value,output._value, 8,32);

	ipBit(_value,output._value,57,33);
	ipBit(_value,output._value,49,34);
	ipBit(_value,output._value,41,35);
	ipBit(_value,output._value,33,36);
	ipBit(_value,output._value,25,37);
	ipBit(_value,output._value,17,38);
	ipBit(_value,output._value, 9,39);
	ipBit(_value,output._value, 1,40);

	ipBit(_value,output._value,59,41);
	ipBit(_value,output._value,51,42);
	ipBit(_value,output._value,43,43);
	ipBit(_value,output._value,35,44);
	ipBit(_value,output._value,27,45);
	ipBit(_value,output._value,19,46);
	ipBit(_value,output._value,11,47);
	ipBit(_value,output._value, 3,48);

	ipBit(_value,output._value,61,49);
	ipBit(_value,output._value,53,50);
	ipBit(_value,output._value,45,51);
	ipBit(_value,output._value,37,52);
	ipBit(_value,output._value,29,53);
	ipBit(_value,output._value,21,54);
	ipBit(_value,output._value,13,55);
	ipBit(_value,output._value, 5,56);

	ipBit(_value,output._value,63,57);
	ipBit(_value,output._value,55,58);
	ipBit(_value,output._value,47,59);
	ipBit(_value,output._value,39,60);
	ipBit(_value,output._value,31,61);
	ipBit(_value,output._value,23,62);
	ipBit(_value,output._value,15,63);
	ipBit(_value,output._value, 7,64);
	}

void	Type64::iip(const Type64& input) noexcept{
	for(unsigned i=0;i<8;++i){
		_value[i]	= 0;
		}
	ipBit(input._value,_value,40, 1);
	ipBit(input._value,_value, 8, 2);
	ipBit(input._value,_value,48, 3);
	ipBit(input._value,_value,16, 4);
	ipBit(input._value,_value,56, 5);
	ipBit(input._value,_value,24, 6);
	ipBit(input._value,_value,64, 7);
	ipBit(input._value,_value,32, 8);

	ipBit(input._value,_value,39, 9);
	ipBit(input._value,_value, 7,10);
	ipBit(input._value,_value,47,11);
	ipBit(input._value,_value,15,12);
	ipBit(input._value,_value,55,13);
	ipBit(input._value,_value,23,14);
	ipBit(input._value,_value,63,15);
	ipBit(input._value,_value,31,16);

	ipBit(input._value,_value,38,17);
	ipBit(input._value,_value, 6,18);
	ipBit(input._value,_value,46,19);
	ipBit(input._value,_value,14,20);
	ipBit(input._value,_value,54,21);
	ipBit(input._value,_value,22,22);
	ipBit(input._value,_value,62,23);
	ipBit(input._value,_value,30,24);

	ipBit(input._value,_value,37,25);
	ipBit(input._value,_value, 5,26);
	ipBit(input._value,_value,45,27);
	ipBit(input._value,_value,13,28);
	ipBit(input._value,_value,53,29);
	ipBit(input._value,_value,21,30);
	ipBit(input._value,_value,61,31);
	ipBit(input._value,_value,29,32);

	ipBit(input._value,_value,36,33);
	ipBit(input._value,_value, 4,34);
	ipBit(input._value,_value,44,35);
	ipBit(input._value,_value,12,36);
	ipBit(input._value,_value,52,37);
	ipBit(input._value,_value,20,38);
	ipBit(input._value,_value,60,39);
	ipBit(input._value,_value,28,40);

	ipBit(input._value,_value,35,41);
	ipBit(input._value,_value, 3,42);
	ipBit(input._value,_value,43,43);
	ipBit(input._value,_value,11,44);
	ipBit(input._value,_value,51,45);
	ipBit(input._value,_value,19,46);
	ipBit(input._value,_value,59,47);
	ipBit(input._value,_value,27,48);

	ipBit(input._value,_value,34,49);
	ipBit(input._value,_value, 2,50);
	ipBit(input._value,_value,42,51);
	ipBit(input._value,_value,10,52);
	ipBit(input._value,_value,50,53);
	ipBit(input._value,_value,18,54);
	ipBit(input._value,_value,58,55);
	ipBit(input._value,_value,26,56);

	ipBit(input._value,_value,33,57);
	ipBit(input._value,_value, 1,58);
	ipBit(input._value,_value,41,59);
	ipBit(input._value,_value, 9,60);
	ipBit(input._value,_value,49,61);
	ipBit(input._value,_value,17,62);
	ipBit(input._value,_value,57,63);
	ipBit(input._value,_value,25,64);
	}

void	Type64::cipher(const KeySched& ks) noexcept{
	unsigned char	xblock[8];
	Type64			x(xblock);

	ip(x);

	Type32		l0(&x._value[0]);
	Type32		r0(&x._value[4]);

	Type48		fo1(r0);
	Type32		r1;
	fo1.f(r1,ks.kn1());
	r1			^= l0;
	Type32&		l1	= r0;


	Type48		fo2(r1);
	Type32		r2;
	fo2.f(r2,ks.kn2());
	r2			^= l1;
	Type32&		l2	= r1;

	Type48		fo3(r2);
	Type32		r3;
	fo3.f(r3,ks.kn3());
	r3			^= l2;
	Type32&		l3	= r2;

	Type48		fo4(r3);
	Type32		r4;
	fo4.f(r4,ks.kn4());
	r4			^= l3;
	Type32&		l4	= r3;

	Type48		fo5(r4);
	Type32		r5;
	fo5.f(r5,ks.kn5());
	r5			^= l4;
	Type32&		l5	= r4;

	Type48		fo6(r5);
	Type32		r6;
	fo6.f(r6,ks.kn6());
	r6			^= l5;
	Type32&		l6	= r5;

	Type48		fo7(r6);
	Type32		r7;
	fo7.f(r7,ks.kn7());
	r7			^= l6;
	Type32&		l7	= r6;

	Type48		fo8(r7);
	Type32		r8;
	fo8.f(r8,ks.kn8());
	r8			^= l7;
	Type32&		l8	= r7;

	Type48		fo9(r8);
	Type32		r9;
	fo9.f(r9,ks.kn9());
	r9			^= l8;
	Type32&		l9	= r8;

	Type48		fo10(r9);
	Type32		r10;
	fo10.f(r10,ks.kn10());
	r10			^= l9;
	Type32&		l10	= r9;

	Type48		fo11(r10);
	Type32		r11;
	fo11.f(r11,ks.kn11());
	r11			^= l10;
	Type32&		l11	= r10;

	Type48		fo12(r11);
	Type32		r12;
	fo12.f(r12,ks.kn12());
	r12			^= l11;
	Type32&		l12	= r11;

	Type48		fo13(r12);
	Type32		r13;
	fo13.f(r13,ks.kn13());
	r13			^= l12;
	Type32&		l13	= r12;

	Type48		fo14(r13);
	Type32		r14;
	fo14.f(r14,ks.kn14());
	r14			^= l13;
	Type32&		l14	= r13;

	Type48		fo15(r14);
	Type32		r15;
	fo15.f(r15,ks.kn15());
	r15			^= l14;
	Type32&		l15	= r14;

	Type48		fo16(r15);
	Type32		r16;
	fo16.f(r16,ks.kn16());
	r16			^= l15;
	Type32&		l16	= r15;

	r16.read(&x._value[0]);
	l16.read(&x._value[4]);

	iip(x);
	}

void	Type64::decipher(const KeySched& ks) noexcept{
	unsigned char	xblock[8];
	Type64			x(xblock);

	ip(x);

	Type32		l0(&x._value[0]);
	Type32		r0(&x._value[4]);

	Type48		fo1(r0);
	Type32		r1;
	fo1.f(r1,ks.kn16());
	r1			^= l0;
	Type32&		l1	= r0;

	Type48		fo2(r1);
	Type32		r2;
	fo2.f(r2,ks.kn15());
	r2			^= l1;
	Type32&		l2	= r1;

	Type48		fo3(r2);
	Type32		r3;
	fo3.f(r3,ks.kn14());
	r3			^= l2;
	Type32&		l3	= r2;

	Type48		fo4(r3);
	Type32		r4;
	fo4.f(r4,ks.kn13());
	r4			^= l3;
	Type32&		l4	= r3;

	Type48		fo5(r4);
	Type32		r5;
	fo5.f(r5,ks.kn12());
	r5			^= l4;
	Type32&		l5	= r4;

	Type48		fo6(r5);
	Type32		r6;
	fo6.f(r6,ks.kn11());
	r6			^= l5;
	Type32&		l6	= r5;

	Type48		fo7(r6);
	Type32		r7;
	fo7.f(r7,ks.kn10());
	r7			^= l6;
	Type32&		l7	= r6;

	Type48		fo8(r7);
	Type32		r8;
	fo8.f(r8,ks.kn9());
	r8			^= l7;
	Type32&		l8	= r7;

	Type48		fo9(r8);
	Type32		r9;
	fo9.f(r9,ks.kn8());
	r9			^= l8;
	Type32&		l9	= r8;

	Type48		fo10(r9);
	Type32		r10;
	fo10.f(r10,ks.kn7());
	r10			^= l9;
	Type32&		l10	= r9;

	Type48		fo11(r10);
	Type32		r11;
	fo11.f(r11,ks.kn6());
	r11			^= l10;
	Type32&		l11	= r10;

	Type48		fo12(r11);
	Type32		r12;
	fo12.f(r12,ks.kn5());
	r12			^= l11;
	Type32&		l12	= r11;

	Type48		fo13(r12);
	Type32		r13;
	fo13.f(r13,ks.kn4());
	r13			^= l12;
	Type32&		l13	= r12;

	Type48		fo14(r13);
	Type32		r14;
	fo14.f(r14,ks.kn3());
	r14			^= l13;
	Type32&		l14	= r13;

	Type48		fo15(r14);
	Type32		r15;
	fo15.f(r15,ks.kn2());
	r15			^= l14;
	Type32&		l15	= r14;

	Type48		fo16(r15);
	Type32		r16;
	fo16.f(r16,ks.kn1());
	r16			^= l15;
	Type32&		l16	= r15;

	r16.read(&x._value[0]);
	l16.read(&x._value[4]);

	iip(x);
	}
