/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_des_typeh_
#define _oscl_des_typeh_

/** */
namespace Oscl {
/** */
namespace DES {

class Type32 {
	private:
		/** */
		unsigned char	_value[4];
	public:
		/** */
		Type32(const unsigned char value[4]) noexcept;

		/** */
		Type32() noexcept;

		/** XOR */
		Type32&	operator ^= (const Type32& other) noexcept;

		/** Set the value of this object
			using the eight 4-bit values
			s<1..8>.
		 */
		inline void	p(	unsigned char	s1,
						unsigned char	s2,
						unsigned char	s3,
						unsigned char	s4,
						unsigned char	s5,
						unsigned char	s6,
						unsigned char	s7,
						unsigned char	s8
						) noexcept{
			_value[0]	= (s1<<4) | s2;
			_value[1]	= (s3<<4) | s3;
			_value[2]	= (s5<<4) | s6;
			_value[3]	= (s7<<4) | s8;
			}

		/** Executes E() transform and places the
			result in "output".
		 */
		void	e(unsigned char	output[8]) const noexcept;

		/** */
		void	read(unsigned char	output[4]) const noexcept;
	};

/** */
class Type48 {
	private:
		/** 48 bit value */
		unsigned char	_value[8];
	public:
		/** */
		Type48() noexcept;

		/** performs E(). converts Type32 to Type48
		 */
		Type48(const Type32& r) noexcept;

		/** performs PC2 assignment.
			converts cd to Type48.
		 */
		void	pc2(	unsigned long	c,
						unsigned long	d
						) noexcept;

		/** output = f(R,K), where R is *this
		 */
		void	f(	Type32&			output,
					const Type48&	keyn
					) noexcept;

	};

/** Sequences key generation */
class KeySched {
	private:
		/** */
		Type48		_key1;
		/** */
		Type48		_key2;
		/** */
		Type48		_key3;
		/** */
		Type48		_key4;
		/** */
		Type48		_key5;
		/** */
		Type48		_key6;
		/** */
		Type48		_key7;
		/** */
		Type48		_key8;
		/** */
		Type48		_key9;
		/** */
		Type48		_key10;
		/** */
		Type48		_key11;
		/** */
		Type48		_key12;
		/** */
		Type48		_key13;
		/** */
		Type48		_key14;
		/** */
		Type48		_key15;
		/** */
		Type48		_key16;

	public:
		/** */
		KeySched(const unsigned char key[8]) noexcept;
		/** */
		inline const Type48&	kn1() const noexcept{return _key1;}
		/** */
		inline const Type48&	kn2() const noexcept{return _key2;}
		/** */
		inline const Type48&	kn3() const noexcept{return _key3;}
		/** */
		inline const Type48&	kn4() const noexcept{return _key4;}
		/** */
		inline const Type48&	kn5() const noexcept{return _key5;}
		/** */
		inline const Type48&	kn6() const noexcept{return _key6;}
		/** */
		inline const Type48&	kn7() const noexcept{return _key7;}
		/** */
		inline const Type48&	kn8() const noexcept{return _key8;}
		/** */
		inline const Type48&	kn9() const noexcept{return _key9;}
		/** */
		inline const Type48&	kn10() const noexcept{return _key10;}
		/** */
		inline const Type48&	kn11() const noexcept{return _key11;}
		/** */
		inline const Type48&	kn12() const noexcept{return _key12;}
		/** */
		inline const Type48&	kn13() const noexcept{return _key13;}
		/** */
		inline const Type48&	kn14() const noexcept{return _key14;}
		/** */
		inline const Type48&	kn15() const noexcept{return _key15;}
		/** */
		inline const Type48&	kn16() const noexcept{return _key16;}
	};

/** */
class Type64 {
	private:
		/** 64 bit (8 byte) value*/
		unsigned char*	_value;

	public:
		/** */
		Type64(unsigned char block[8]) noexcept;

		/** */
		void	cipher(const KeySched& ks) noexcept;
		/** */
		void	decipher(const KeySched& ks) noexcept;

	private:
		/** IP
		 */
		void	ip(Type64& output) const noexcept;

		/** Inverse IP
		 */
		void	iip(const Type64& input) noexcept;
	};

}
}

#endif
