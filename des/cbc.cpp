/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "cbc.h"
#include "type.h"
#include <string.h>

using namespace Oscl::DES::CBC;

unsigned	Oscl::DES::CBC::encrypt(	const unsigned char	key[keySize],
										const void*			input,
										unsigned			inputLength,
										void*				output,
										unsigned			maxOutputLength
										) noexcept{
	unsigned				nBlocks	= (inputLength+7)/8;
	unsigned				length	= nBlocks*8;
	unsigned char*			out		= (unsigned char*)output;
	maxOutputLength	/= 8;
	maxOutputLength	*= 8;

	if(length > maxOutputLength){
		length	= maxOutputLength;
		}

	memcpy(out,input,inputLength);

	KeySched	ks(key);

	for(unsigned i=0;i<nBlocks;++i){
		Type64	x(&out[i*8]);
		x.cipher(ks);
		}

	return nBlocks*8;
	}

unsigned	Oscl::DES::CBC::decrypt(	const unsigned char	key[keySize],
										const void*			input,
										unsigned			inputLength,
										void*				output,
										unsigned			maxOutputLength
										) noexcept{
	unsigned				nBlocks	= (inputLength+7)/8;
	unsigned				length	= nBlocks*8;
	unsigned char*			out		= (unsigned char*)output;
	maxOutputLength	%= 8;
	maxOutputLength	*= 8;

	if(length > maxOutputLength){
		length	= maxOutputLength;
		}

	memcpy(out,input,inputLength);

	KeySched	ks(key);

	for(unsigned i=0;i<nBlocks;++i){
		Type64	x(&out[i*8]);
		x.decipher(ks);
		}

	return nBlocks*8;
	}
