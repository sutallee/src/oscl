/*
   Copyright (C) 2021 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "part.h"
#include "oscl/mt/itc/mbox/syncrh.h"

using namespace Oscl::Switch::Spst::Dist;

Part::Part(
	Oscl::Mt::Itc::PostMsgApi&	papi,
	bool						initialState
	) noexcept:
		_output(
			papi,
			initialState
			)
		{
	}

Oscl::Switch::Spst::Observer::Req::Api::SAP&	Part::getOutputSAP() noexcept{
	return _output.getSAP();
	}

Oscl::Switch::Spst::Control::Api&	Part::getLocalControlApi() noexcept{
	return _output;
	}

void Part::request(Control::Req::Api::OnReq& msg) noexcept{
	_output.on();
	msg.returnToSender();
	}

void Part::request(Control::Req::Api::OffReq& msg) noexcept{
	_output.off();
	msg.returnToSender();
	}

void	Part::on() noexcept{

	Oscl::Switch::Spst::
	Control::Req::Api::OnPayload	payload;

	Oscl::Mt::Itc::SyncReturnHandler	srh;

	Oscl::Switch::Spst::
	Control::Req::Api::OnReq
	req(
		*this,
		payload,
		srh
		);

	_output.getSAP().postSync(req);
	}

void	Part::off() noexcept{

	Oscl::Switch::Spst::
	Control::Req::Api::OffPayload	payload;

	Oscl::Mt::Itc::SyncReturnHandler	srh;

	Oscl::Switch::Spst::
	Control::Req::Api::OffReq
	req(
		*this,
		payload,
		srh
		);

	_output.getSAP().postSync(req);
	}

