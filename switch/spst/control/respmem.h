/*
   Copyright (C) 2021 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_switch_spst_control_itc_respmemh_
#define _oscl_switch_spst_control_itc_respmemh_

#include "respapi.h"
#include "oscl/memory/block.h"

/**	These record types in this file describe blocks of memory
	that are large enough and appropriately aligned such that
	placement new operations creating instances of their
	corresponding objects can safely be performed. These records
	are useful to most clients that use the server's ITC
	interface asynchronously.
 */

/** */
namespace Oscl {

/** */
namespace Switch {

/** */
namespace Spst {

/** */
namespace Control {

/** */
namespace Resp {

/**	This record describes a block of memory that is
	large enough (and properly aligned) to hold a
	OnResp and its corresponding Req::Api::OnPayload.
	Clients which use the corresponding server asynchronously
	frequently need to reservere/manage memory for for these
	ITC interactions and instances of this record are useful
	for that purpose.
 */
struct OnMem {
	/**	This is memory enough for the response message.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Resp::Api::OnResp)>	resp;

	/**	This is memory enough for the message payload.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Req::Api::OnPayload)>	payload;

	/**	This operation creates a response message using this
		 block of memory.
	 */
	Oscl::Switch::Spst::Control::Resp::Api::OnResp&
		build(	Oscl::Switch::Spst::Control::Req::Api::SAP&	sap,
				Oscl::Switch::Spst::Control::Resp::Api&		respApi,
				Oscl::Mt::Itc::PostMsgApi&		clientPapi
				) noexcept;

	};

/**	This record describes a block of memory that is
	large enough (and properly aligned) to hold a
	OffResp and its corresponding Req::Api::OffPayload.
	Clients which use the corresponding server asynchronously
	frequently need to reservere/manage memory for for these
	ITC interactions and instances of this record are useful
	for that purpose.
 */
struct OffMem {
	/**	This is memory enough for the response message.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Resp::Api::OffResp)>	resp;

	/**	This is memory enough for the message payload.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Req::Api::OffPayload)>	payload;

	/**	This operation creates a response message using this
		 block of memory.
	 */
	Oscl::Switch::Spst::Control::Resp::Api::OffResp&
		build(	Oscl::Switch::Spst::Control::Req::Api::SAP&	sap,
				Oscl::Switch::Spst::Control::Resp::Api&		respApi,
				Oscl::Mt::Itc::PostMsgApi&		clientPapi
				) noexcept;

	};

}
}
}
}
}
#endif
