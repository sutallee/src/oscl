/*
   Copyright (C) 2021 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_switch_spst_output_parth_
#define _oscl_switch_spst_output_parth_

#include "oscl/switch/spst/observer/reqapi.h"
#include "oscl/queue/queue.h"
#include "oscl/switch/spst/control/api.h"

/** */
namespace Oscl {

/** */
namespace Switch {

/** */
namespace Spst {

/** */
namespace Output {

/** */
class Part :
		public Oscl::Switch::Spst::Observer::Req::Api,
		public Oscl::Switch::Spst::Control::Api
		{
	private:
		/** */
		Oscl::Switch::Spst::Observer::Req::Api::ConcreteSAP	_sap;

		/** */
		Oscl::Queue<Oscl::Switch::Spst::Observer::Req::Api::ChangeReq>	_noteQ;

		/** This variable contains the current state of the
			monitor point.
		 */
		bool							_on;

	public:
		/** */
		Part(
			Oscl::Mt::Itc::PostMsgApi&	papi,
			bool						initialState
			) noexcept;

		/** */
		Oscl::Switch::Spst::Observer::Req::Api::SAP&	getSAP() noexcept;

	private:
		/** */
		void	notify(bool on) noexcept;

	public: // Oscl::Switch::Spst::Control::Api
		/** */
		void	on() noexcept;

		/** */
		void	off() noexcept;

	public:	// Oscl::Switch::Spst::Observer::Req::Api
		/** */
		void request(Oscl::Switch::Spst::Observer::Req::Api::ChangeReq& msg) noexcept;

		/** */
		void request(Oscl::Switch::Spst::Observer::Req::Api::CancelChangeReq& msg) noexcept;
	};

}
}
}
}

#endif
