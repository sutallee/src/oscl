/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "var.h"

namespace Oscl {
namespace TriState {

class VarIsAsserted : public TriState::Var::Query {
	public:
		bool	_isAsserted;
	public:
		inline void asserted() noexcept{
			_isAsserted	= true;
			}
		inline void negated() noexcept{
			_isAsserted	= false;
			}
		inline void inhibited() noexcept{
			_isAsserted	= false;
			}
	};

class VarIsNegated : public TriState::Var::Query {
	public:
		bool	_isNegated;
	public:
		inline void asserted() noexcept{
			_isNegated	= false;
			}
		inline void negated() noexcept{
			_isNegated	= true;
			}
		inline void inhibited() noexcept{
			_isNegated	= false;
			}
	};

class IsInhibited : public TriState::Var::Query {
	public:
		bool	_isInhibited;
	public:
		inline void asserted() noexcept{
			_isInhibited	= false;
			}
		inline void negated() noexcept{
			_isInhibited	= false;
			}
		inline void inhibited() noexcept{
			_isInhibited	= true;
			}
	};

class VarAsserted : public TriState::Var::State {
	public:
		bool operator ==(const TriState::Var::State& other) const noexcept;
		bool operator !=(const TriState::Var::State& other) const noexcept;
		void accept(TriState::Var::Query& q) const noexcept;
	};

class VarNegated : public TriState::Var::State {
	public:
		bool operator ==(const TriState::Var::State& other) const noexcept;
		bool operator !=(const TriState::Var::State& other) const noexcept;
		void accept(TriState::Var::Query& q) const noexcept;
	};

class VarInhibited : public TriState::Var::State {
	public:
		bool operator ==(const TriState::Var::State& other) const noexcept;
		bool operator !=(const TriState::Var::State& other) const noexcept;
		void accept(TriState::Var::Query& q) const noexcept;
	};

};
};

using namespace Oscl;

static const TriState::VarAsserted	_asserted;
static const TriState::VarNegated		_negated;
static const TriState::VarInhibited	_inhibited;

bool TriState::VarAsserted::operator ==(const TriState::Var::State& other) const noexcept{
	TriState::VarIsAsserted	assertedQuery;
	other.accept(assertedQuery);
	return assertedQuery._isAsserted;
	}

bool TriState::VarAsserted::operator !=(const TriState::Var::State& other) const noexcept{
	TriState::VarIsAsserted	assertedQuery;
	other.accept(assertedQuery);
	return !assertedQuery._isAsserted;
	}

void TriState::VarAsserted::accept(TriState::Var::Query& q) const noexcept{
	q.asserted();
	}

bool TriState::VarNegated::operator ==(const TriState::Var::State& other) const noexcept{
	TriState::VarIsNegated	negatedQuery;
	other.accept(negatedQuery);
	return negatedQuery._isNegated;
	}

bool TriState::VarNegated::operator !=(const TriState::Var::State& other) const noexcept{
	TriState::VarIsNegated	negatedQuery;
	other.accept(negatedQuery);
	return !negatedQuery._isNegated;
	}

void TriState::VarNegated::accept(TriState::Var::Query& q) const noexcept{
	q.negated();
	}

bool TriState::VarInhibited::operator ==(const TriState::Var::State& other) const noexcept{
	IsInhibited	inhbitedQuery;
	other.accept(inhbitedQuery);
	return inhbitedQuery._isInhibited;
	}

bool TriState::VarInhibited::operator !=(const TriState::Var::State& other) const noexcept{
	IsInhibited	inhbitedQuery;
	other.accept(inhbitedQuery);
	return !inhbitedQuery._isInhibited;
	}

void TriState::VarInhibited::accept(TriState::Var::Query& q) const noexcept{
	q.inhibited();
	}

void	TriState::Var::accept(Query& q) const noexcept{
	_state->accept(q);
	}

class GetState : public TriState::Var::Query {
	public:
		const TriState::Var::State*	_state;
	public:
		inline void asserted() noexcept{
			_state	= &TriState::Var::getAsserted();
			}
		inline void negated() noexcept{
			_state	= &TriState::Var::getNegated();
			}
		inline void inhibited() noexcept{
			_state	= &TriState::Var::getInhibited();
			}
	};

TriState::Var::Var(const State& initial) noexcept:
		_state(&initial)
		{
	}

bool	TriState::Var::operator == (const TriState::Var& other) noexcept{
	return *other._state == *_state;
	}

bool	TriState::Var::operator != (const TriState::Var& other) noexcept{
	return *other._state != *_state;
	}

void	TriState::Var::operator=(const TriState::Var& newState) noexcept{
	*this	= *newState._state;
	}

void	TriState::Var::operator=(const State& newState) noexcept{
	GetState	getState;
	newState.accept(getState);
	_state	= getState._state;
	}

const TriState::Var::State&	TriState::Var::getCurrentState() const noexcept{
	return *_state;
	}

void	TriState::Var::assert() noexcept{
	_state	= &_asserted;
	}

void	TriState::Var::negate() noexcept{
	_state	= &_negated;
	}

void	TriState::Var::inhibit() noexcept{
	_state	= &_inhibited;
	}

const TriState::Var::State&	TriState::Var::getAsserted() noexcept{
	return _asserted;
	}

const TriState::Var::State&	TriState::Var::getNegated() noexcept{
	return _negated;
	}

const TriState::Var::State&	TriState::Var::getInhibited() noexcept{
	return _inhibited;
	}

