/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_tristate_varh_
#define _oscl_tristate_varh_

/** */
namespace Oscl {
/** */
namespace TriState {

/** */
class Var {
	public:
		/** */
		class Query {
			public:
				/** */
				virtual ~Query(){}
				/** */
				virtual void asserted() noexcept=0;
				/** */
				virtual void negated() noexcept=0;
				/** */
				virtual void inhibited() noexcept=0;
			};
		/** */
		 class State {
			public:
				/** */
				virtual ~State(){}
				/** */
				virtual bool operator ==(const State& other) const noexcept=0;
				/** */
				virtual bool operator !=(const State& other) const noexcept=0;
				/** */
				virtual void accept(Query& q) const noexcept=0;
			};
	public:
		/** */
		const State*		_state;
	public:
		/** */
		Var(const State& initial=getNegated()) noexcept;
		/** */
		void			accept(Query& q) const noexcept;
		/** */
		bool			operator == (const TriState::Var& other) noexcept;
		/** */
		bool			operator != (const TriState::Var& other) noexcept;
	public:
		/** */
		void			assert() noexcept;
		/** */
		void			negate() noexcept;
		/** */
		void			inhibit() noexcept;
		/** */
		void			operator=(const TriState::Var& newState) noexcept;
	public:
		/** */
		void			operator=(const State& newState) noexcept;
		/** */
		const State&	getCurrentState() const noexcept;
	public:
		/** */
		static const State&	getAsserted() noexcept;
		/** */
		static const State&	getNegated() noexcept;
		/** */
		static const State&	getInhibited() noexcept;
	};

};
};

#endif

