/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_tristate_itc_outputh_
#define _oscl_tristate_itc_outputh_
#include "oscl/tristate/var.h"
#include "reqapi.h"

/** */
namespace Oscl {
/** */
namespace TriState {

/** */
class Output :	public TriState::Var::Query,
					public TriState::Req::Api
					{
	private:
		/** */
		Queue<TriState::Req::Api::ChangeRequest>	_noteQ;
		/** */
		TriState::Var								_state;
	public:
		/** */
		Output() noexcept;
	public:
		/** */
		void	accept(TriState::Var::Query& query) noexcept;
		/** */
		void	notify(const TriState::Var::State& newState) noexcept;
	public:
		/** */
		void	assert() noexcept;
		/** */
		void	negate() noexcept;
		/** */
		void	inhibit() noexcept;
	public:	// TriState::Req::Api
		/** */
		void request(TriState::Req::Api::ChangeRequest& msg) noexcept;
		/** */
		void request(TriState::Req::Api::CancelRequest& msg) noexcept;
	private: // TriState::Var::Query
		/** */
		void	asserted() noexcept;
		/** */
		void	negated() noexcept;
		/** */
		void	inhibited() noexcept;
	};

};
};

#endif
