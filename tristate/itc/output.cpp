/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "output.h"

using namespace Oscl;

TriState::Output::Output() noexcept
		{
	}

void	TriState::Output::accept(TriState::Var::Query& query) noexcept{
	_state.accept(query);
	}

void	TriState::Output::notify(const TriState::Var::State& newState) noexcept{
	TriState::Req::Api::ChangeRequest* note;
	while((note=_noteQ.get())){
		note->getPayload()	= newState;
		note->returnToSender();
		}
	}

void TriState::Output::request(TriState::Req::Api::ChangeRequest& msg) noexcept{
	if(_state == msg.getPayload()){
		_noteQ.put(&msg);
		}
	else{
		TriState::Var&	payloadState	= msg.getPayload();
		payloadState	= _state;
		msg.returnToSender();
		}
	}

void TriState::Output::request(TriState::Req::Api::CancelRequest& msg) noexcept{
	for(	TriState::Req::Api::ChangeRequest* next=_noteQ.first();
			next;
			next=_noteQ.next(next)
			){
		if(next == &msg.getPayload()._reqToCancel){
			_noteQ.remove(next);
			next->returnToSender();
			break;
			}
		}
	msg.returnToSender();
	}

void	TriState::Output::asserted() noexcept{
	assert();
	}

void	TriState::Output::negated() noexcept{
	negate();
	}

void	TriState::Output::inhibited() noexcept{
	inhibit();
	}

void	TriState::Output::assert() noexcept{
	const TriState::Var::State&	assertedState	= TriState::Var::getAsserted();
	if(_state != assertedState){
		_state.assert();
		notify(assertedState);
		}
	}

void	TriState::Output::negate() noexcept{
	const TriState::Var::State&	negatedState	= TriState::Var::getNegated();
	if(_state != negatedState){
		_state.negate();
		notify(negatedState);
		}
	}

void	TriState::Output::inhibit() noexcept{
	const TriState::Var::State&	inhibitedState	= TriState::Var::getInhibited();
	if(_state != inhibitedState){
		_state.inhibit();
		notify(inhibitedState);
		}
	}

