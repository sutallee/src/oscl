/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <new>
#include "base.h"

using namespace Oscl;
using namespace Oscl::TriState;

MonitorBase::MonitorBase(	Req::Api::SAP&			monitorPointSAP,
							Mt::Itc::PostMsgApi&	myPapi,
							Var::Query&				query
							) noexcept:
		_monitorPointSAP(monitorPointSAP),
		_myPapi(myPapi),
		_payload(),
		_response(	monitorPointSAP.getReqApi(),
					*this,
					myPapi,
					_payload
					),
		_state(*this),
		_query(query),
		_cancelMem(0)
		{
	}

void	MonitorBase::activate() noexcept{
	_payload.negate();
	_state.activate();
	}

void	MonitorBase::activate(	const Oscl::TriState::Var::
								State&		assumedInitialState
								) noexcept{
	_payload	= assumedInitialState;
	_state.activate();
	}

void	MonitorBase::deactivate(CancelMemory& mem) noexcept{
	_cancelMem	= &mem;
	_state.deactivate();
	}

void	MonitorBase::response(Resp::Api::NotificationResp& msg) noexcept{
	_state.notified();
	}

void	MonitorBase::response(Resp::Api::CancelResp& msg) noexcept{
	_state.canceled();
	}

void	MonitorBase::initNotificationReq() noexcept{
	}

void	MonitorBase::requestChangeNotification() noexcept{
	_monitorPointSAP.post(_response.getSrvMsg());
	}

void	MonitorBase::processChangeNotification() noexcept{
	_payload.accept(_query);
	}

void	MonitorBase::cancelChangeNotification() noexcept{
	Req::Api::CancelPayload*	payload	= new (&_cancelMem->payload)
		Req::Api::CancelPayload(_response.getSrvMsg());
	Resp::Api::CancelResp*	response	= new (&_cancelMem->response)
		Resp::Api::CancelResp(	_monitorPointSAP.getReqApi(),
								*this,
								_myPapi,
								*payload
								);
	_monitorPointSAP.post(response->getSrvMsg());
	}

void	MonitorBase::completeDeactivation() noexcept{
	deactivated();
	}


