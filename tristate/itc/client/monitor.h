/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_tristate_itc_client_monitorh_
#define _oscl_tristate_itc_client_monitorh_
#include "base.h"

/** */
namespace Oscl {
/** */
namespace TriState {

/** */
template <class Context>
class Monitor :	public Oscl::TriState::MonitorBase,
				public Oscl::TriState::Var::Query
				{
	public:
		/** */
		typedef void (Context::* ActionFunc)();
	private:
		/** */
		Context&		_context;
		/** */
		ActionFunc		_asserted;
		/** */
		ActionFunc		_negated;
		/** */
		ActionFunc		_inhibited;
		/** */
		ActionFunc		_deactivated;
	public:
		/** */
		Monitor(	Oscl::TriState::Req::Api::SAP&	mpSAP,
					Oscl::Mt::Itc::PostMsgApi&		myPapi,
					Context&						context,
					ActionFunc						asserted,
					ActionFunc						negated,
					ActionFunc						inhibited
					) noexcept;
		/** */
		void	activate() noexcept;
		/** */
		void	activate(	const Oscl::TriState::Var::
							State&		assumedInitialState
							) noexcept;
		/** */
		void	deactivate(CancelMemory& mem,ActionFunc deactivated) noexcept;
	private:
		/** */
		void	asserted() noexcept;
		/** */
		void	negated() noexcept;
		/** */
		void	inhibited() noexcept;
	private:
		/** */
		void	deactivated() noexcept;
	};

};
};

template <class Context>
Oscl::TriState::Monitor<Context>::
Monitor(	Oscl::TriState::Req::Api::SAP&					mpSAP,
			Oscl::Mt::Itc::PostMsgApi&						myPapi,
			Context&										context,
			Oscl::TriState::Monitor<Context>::ActionFunc	asserted,
			Oscl::TriState::Monitor<Context>::ActionFunc	negated,
			Oscl::TriState::Monitor<Context>::ActionFunc	inhibited
			) noexcept:
		Oscl::TriState::MonitorBase(	mpSAP,
										myPapi,
										*this
										),
		_context(context),
		_asserted(asserted),
		_negated(negated),
		_inhibited(inhibited),
		_deactivated(0)
		{
	}

template <class Context>
void	Oscl::TriState::Monitor<Context>::activate() noexcept{
	Oscl::TriState::MonitorBase::activate();
	}

template <class Context>
void	Oscl::TriState::Monitor<Context>::
		activate(	const Oscl::TriState::Var::
					State&		assumedInitialState
					) noexcept{
	Oscl::TriState::MonitorBase::activate(assumedInitialState);
	}

template <class Context>
void	Oscl::TriState::Monitor<Context>::
		deactivate(	Oscl::TriState::MonitorBase::CancelMemory&		mem,
					Oscl::TriState::Monitor<Context>::ActionFunc	deactivated
					) noexcept{
	_deactivated	= deactivated;
	Oscl::TriState::MonitorBase::deactivate(mem);
	}

template <class Context>
void	Oscl::TriState::Monitor<Context>::asserted() noexcept{
	(_context.*_asserted)();
	}

template <class Context>
void	Oscl::TriState::Monitor<Context>::negated() noexcept{
	(_context.*_negated)();
	}

template <class Context>
void	Oscl::TriState::Monitor<Context>::inhibited() noexcept{
	(_context.*_inhibited)();
	}

template <class Context>
void	Oscl::TriState::Monitor<Context>::deactivated() noexcept{
	(_context.*_deactivated)();
	}

#endif
