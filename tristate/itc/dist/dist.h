/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_tristate_itc_disth_
#define _oscl_tristate_itc_disth_
#include "reqapi.h"
#include "oscl/tristate/controlapi.h"
#include "oscl/tristate/itc/output.h"
#include "oscl/mt/itc/postmsgapi.h"

/** */
namespace Oscl {
/** */
namespace TriState {

/** */
class Dist :	public TriState::DistReq::Api,
				public TriState::ControlApi
				{
	private:
		/** */
		TriState::Output			_output;
		/** */
		TriState::Req::Api::ConcreteSAP	_sap;
	public:
		/** */
		Dist(Mt::Itc::PostMsgApi& papi) noexcept;
		/** */
		TriState::Req::Api::SAP&	getTriStateReqApiSAP() noexcept;
	public:	// TriState::DistReq::Api
		/** */
		void request(TriState::DistReq::Api::AssertReq& msg) noexcept;
		/** */
		void request(TriState::DistReq::Api::NegateReq& msg) noexcept;
		/** */
		void request(TriState::DistReq::Api::InhibitReq& msg) noexcept;
	public:	// TriState::ControlApi
		/** */
		void	assert() noexcept;
		/** */
		void	negate() noexcept;
		/** */
		void	inhibit() noexcept;
	};

};
};

#endif
