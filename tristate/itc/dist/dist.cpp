/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "dist.h"
#include "oscl/mt/itc/mbox/syncrh.h"

using namespace Oscl;

TriState::Dist::Dist(Mt::Itc::PostMsgApi& papi) noexcept:
		_output(),
		_sap(_output,papi)
		{
	}

TriState::Req::Api::SAP&	TriState::Dist::getTriStateReqApiSAP() noexcept{
	return _sap;
	}

void TriState::Dist::request(TriState::DistReq::Api::AssertReq& msg) noexcept{
	_output.assert();
	msg.returnToSender();
	}

void TriState::Dist::request(TriState::DistReq::Api::NegateReq& msg) noexcept{
	_output.negate();
	msg.returnToSender();
	}

void TriState::Dist::request(TriState::DistReq::Api::InhibitReq& msg) noexcept{
	_output.inhibit();
	msg.returnToSender();
	}

void	TriState::Dist::assert() noexcept{
	TriState::DistReq::Api::AssertPayload	payload;
	Mt::Itc::SyncReturnHandler			srh;
	TriState::DistReq::Api::AssertReq		req(*this,payload,srh);
	_sap.postSync(req);
	}

void	TriState::Dist::negate() noexcept{
	TriState::DistReq::Api::NegatePayload	payload;
	Mt::Itc::SyncReturnHandler			srh;
	TriState::DistReq::Api::NegateReq		req(*this,payload,srh);
	_sap.postSync(req);
	}

void	TriState::Dist::inhibit() noexcept{
	TriState::DistReq::Api::InhibitPayload	payload;
	Mt::Itc::SyncReturnHandler			srh;
	TriState::DistReq::Api::InhibitReq		req(*this,payload,srh);
	_sap.postSync(req);
	}

