/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_tristate_subjecth_
#define _oscl_tristate_subjecth_
#include "oscl/queue/queue.h"
#include "subjectapi.h"

/** */
namespace Oscl {
/** */
namespace TriState {

/** */
class Subject : public TriState::SubjectApi {
	private:
		/** */
		Queue<TriState::Observer>	_observers;
		/** */
		TriState::Var				_state;
	public:
		/** */
		virtual ~Subject(){}
		/** */
		void	notify() noexcept;
	public:	// TriState::SubjectApi
		/** */
		void	attach(TriState::Observer& o,const TriState::Var::State& assumed) noexcept;
		/** */
		void	detach(TriState::Observer& o) noexcept;
	public:
		/** */
		void	assert() noexcept;
		/** */
		void	negate() noexcept;
		/** */
		void	inhibit() noexcept;
	};

};
};

#endif
