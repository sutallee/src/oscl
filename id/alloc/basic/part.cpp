/*
   Copyright (C) 2024 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <new>
#include "part.h"

using namespace Oscl::Id::Alloc::Basic;

Part::Part(
	Oscl::Id::Alloc::NodeMem*	nodeMemory,
	unsigned					nNodes
	) noexcept {
	for( unsigned i=0; i < nNodes; ++i ) {
		Oscl::Id::Alloc::Node*
		node	= new ( &nodeMemory[i] ) Node(i);
		_freeNodes.put( node );
		}
	}

Oscl::Id::Alloc::Node*	Part::alloc() noexcept {
	return _freeNodes.get();
	}

void Part::free( Oscl::Id::Alloc::Node* node)	noexcept {
	_freeNodes.remove( node );
	return _freeNodes.put( node );
	}


