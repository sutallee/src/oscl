/*
   Copyright (C) 2021 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_spi_device_parth_
#define _oscl_spi_device_parth_

#include "oscl/spi/device/api.h"
#include "oscl/spi/master/api.h"

/** */
namespace Oscl {
/** */
namespace SPI {
/** */
namespace Device {

/** This class is a decorator that implements the
	SPI device API and encapsulates the device specific
	implementation needed to perform a synchronous
	transaction with that SPI device.
 */
class Part: public Oscl::SPI::Device::Api {
	private:
		/** */
		Oscl::SPI::Master::Api&			_spiMasterApi;
		/** */
		Oscl::SPI::Master::Select::Api&	_deviceSelectApi;
		/** */
		const unsigned long				_frequency;
		/** */
		const bool						_cpol;
		/** */
		const bool						_cpha;

	public:
		/** */
		Part(
			Oscl::SPI::Master::Api&			spiMasterApi,
			Oscl::SPI::Master::Select::Api&	deviceSelectApi,
			unsigned long					frequency=0,
			bool							cpol=false,
			bool							cpha=false
			) noexcept;

		/** This operation transfers the number of octets specified
			by the nDataBytesToTransfer argument from the buffer
			specified by the txDataBuffer argument to the device
			while simultaneously transfering the same number of
			octets from the device to the buffer specified by
			rxDataBuffer.

			Returns false when successful, or true if any
			error is encountered.
		 */
		bool	transfer(
					const void*	txDataBuffer,
					void*		rxDataBuffer,
					unsigned	nDataBytesToTransfer
					) noexcept;
	};

}
}
}

#endif
