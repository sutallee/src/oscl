/*
   Copyright (C) 2021 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "part.h"

using namespace Oscl::SPI::Device;

Part::Part(
	Oscl::SPI::Master::Api&			spiMasterApi,
	Oscl::SPI::Master::Select::Api&	deviceSelectApi,
	unsigned long					frequency,
	bool							cpol,
	bool							cpha
	) noexcept:
		_spiMasterApi(spiMasterApi),
		_deviceSelectApi(deviceSelectApi),
		_frequency(frequency),
		_cpol(cpol),
		_cpha(cpha)
		{
	}

bool	Part::transfer(
			const void*	txDataBuffer,
			void*		rxDataBuffer,
			unsigned	nDataBytesToTransfer
			) noexcept{

	return _spiMasterApi.transfer(
		_deviceSelectApi,
		txDataBuffer,
		rxDataBuffer,
		nDataBytesToTransfer,
		_frequency,
		_cpol,
		_cpha
		);
	}
