/*
   Copyright (C) 2006 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_spi_master_apih_
#define _oscl_spi_master_apih_
#include "oscl/spi/master/select/api.h"

/** */
namespace Oscl {
/** */
namespace SPI {
/** */
namespace Master {

/** */
class Api {
	public:
		/** */
		virtual ~Api(){}

		/** This operation transfers the number of octets specified
			by the nDataBytesToTransfer argument from the buffer
			specified by the txDataBuffer argument to the device
			while simultaneously transfering the same number of
			octets from the device to the buffer specified by
			rxDataBuffer.

			Note: If frequency is 0 then the frequency, cpol,
			and cpha parameters are assumed to be the system
			selected values for buses that only have a single
			type of SPI slave device.

			Returns false when successful, or true if any
			error is encountered.
		 */
		virtual bool	transfer(
							Oscl::SPI::Master::Select::Api&	deviceSelectApi,
							const void*						txDataBuffer,
							void*							rxDataBuffer,
							unsigned						nDataBytesToTransfer,
							unsigned long					frequency=0,
							bool							lsbFirst=false,
							bool							cpol=false,
							bool							cpha=false
							) noexcept=0;
	};

}
}
}

#endif
