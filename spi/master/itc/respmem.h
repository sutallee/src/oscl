/*
   Copyright (C) 2006 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_spi_master_itc_respmemh_
#define _oscl_spi_master_itc_respmemh_
#include "oscl/memory/block.h"
#include "respapi.h"

/** */
namespace Oscl {
/** */
namespace SPI {
/** */
namespace Master {
/** */
namespace Resp {

struct TransferRespMem {
	/** */
	Oscl::Memory::
	AlignedBlock<sizeof(Resp::Api::TransferResp)>		resp;

	/** */
	Oscl::Memory::
	AlignedBlock<sizeof(Req::Api::TransferPayload)>		payload;

	/**	This operation creates a response message using this
		 block of memory.
	 */
	Oscl::SPI::Master::Resp::Api::TransferResp&
		build(
			Oscl::SPI::Master::Req::Api::SAP&	sap,
			Oscl::SPI::Master::Resp::Api&		respApi,
			Oscl::Mt::Itc::PostMsgApi&			clientPapi,
			Oscl::SPI::Master::Select::Api&		deviceSelectApi,
			const void*							txDataBuffer,
			void*								rxDataBuffer,
			unsigned							nDataBytesToTransfer,
			unsigned long						frequency = 0,
			bool								lsbFirst = 0,
			bool								cpol = 0,
			bool								cpha = 0
			) noexcept;

	};

struct CancelTransferRespMem {
	/** */
	Oscl::Memory::
	AlignedBlock<sizeof(Resp::Api::CancelTransferResp)>		resp;

	/** */
	Oscl::Memory::
	AlignedBlock<sizeof(Req::Api::CancelTransferPayload)>	payload;

	/**	This operation creates a response message using this
		 block of memory.
	 */
	Oscl::SPI::Master::Resp::Api::CancelTransferResp&
		build(
			Oscl::SPI::Master::Req::Api::SAP&	sap,
			Oscl::SPI::Master::Resp::Api&		respApi,
			Oscl::Mt::Itc::PostMsgApi&			clientPapi,
			Req::Api::TransferReq&				requestToCancel
			) noexcept;

	};

union Mem {
	/** */
	void*				__qitemlink;
	/** */
	TransferRespMem		transfer;
	};

union CancelMem {
	/** */
	void*					__qitemlink;
	/** */
	CancelTransferRespMem	cancelTransfer;
	};

}
}
}
}

#endif
