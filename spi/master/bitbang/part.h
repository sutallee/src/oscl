/*
   Copyright (C) 2024 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_spi_master_bitbang_parth_
#define _oscl_spi_master_bitbang_parth_

#include <stdint.h>
#include "api.h"
#include "oscl/spi/master/itc/reqapi.h"
#include "oscl/spi/master/itc/sync.h"

/** */
namespace Oscl {
/** */
namespace SPI {
/** */
namespace Master {
/** */
namespace BitBang {

/** This implements the primary logic of a
	bit-banged SPI master.
 */
class Part : private Oscl::SPI::Master::Req::Api {
	private:
		/** */
		Oscl::SPI::Master::BitBang::Api&	_api;

		/** */
		Oscl::SPI::Master::Sync				_sync;

	public:
		/** */
		Part(
			Oscl::Mt::Itc::PostMsgApi&			papi,
			Oscl::SPI::Master::BitBang::Api&	api
			) noexcept;

		/** */
		virtual ~Part(){}

		/** */
		Oscl::SPI::Master::Req::Api::SAP&	getSAP() noexcept;

		/** */
		Oscl::SPI::Master::Api&	getSyncApi() noexcept;

		/** */
		void	start() noexcept;

	private:
		/** */
		uint8_t	transfer(
					uint8_t		txOctet,
					unsigned	periodInUs
				 	) noexcept;

	private: // Oscl::SPI::Master::Req::Api
		/** */
		void	request( Oscl::SPI::Master::Req::Api::TransferReq& msg ) noexcept override;

		/** */
		void	request( Oscl::SPI::Master::Req::Api::CancelTransferReq& msg ) noexcept override;
	};

}
}
}
}

#endif
