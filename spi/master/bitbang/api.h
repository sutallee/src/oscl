/*
   Copyright (C) 2024 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_spi_master_bitbang_apih_
#define _oscl_spi_master_bitbang_apih_

#include <stdint.h>

/** */
namespace Oscl {
/** */
namespace SPI {
/** */
namespace Master {
/** */
namespace BitBang {

/** This interface is to be implemented by the
	context to perform GPIO I/O and other external
	functions.
 */
class Api {
	public:
		/** Configure and initialize GPIO and other
			state as required.
		 */
		virtual void	initialize() noexcept=0;

		/** Set the external state of the SPI clock to HIGH.
		 */
		virtual void	assertSCK() noexcept=0;

		/** Set the external state of the SPI clock to LOW.
		 */
		virtual void	negateSCK() noexcept=0;

		/** Set the external state of the SPI MOSI pin HIGH.
		 */
		virtual void	assertMOSI() noexcept=0;

		/** Set the external state of the SPI MOSI pin LOW.
		 */
		virtual void	negateMOSI() noexcept=0;

		/** Returns the state of the SPI MISI pin where
			true indicates that the pin is HIGH.
		 */
		virtual bool	misoState() noexcept=0;

		/** Delay for the specified number of micro-seconds.
		 */
		virtual void	delay( unsigned us ) noexcept=0;
	};

}
}
}
}

#endif
