/*
   Copyright (C) 2024 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_spi_master_bitbang_composerh_
#define _oscl_spi_master_bitbang_composerh_

#include "api.h"

/** */
namespace Oscl {
/** */
namespace SPI {
/** */
namespace Master {
/** */
namespace BitBang {

/** This composer makes it possible for an implementation
	to use composition to implement the bit-banged context
	API rather than inheritance.
 */
template < class Context >
class Composer : public Api {
	private:
		/** */
		Context&	_context;

		/** */
		void	( Context::*_initialize )();

		/** */
		void	( Context::*_assertSCK )();

		/** */
		void	( Context::*_negateSCK )();

		/** */
		void	( Context::*_assertMOSI )();

		/** */
		void	( Context::*_negateMOSI )();

		/** */
		bool	( Context::*_misoState )();

		/** */
		void	( Context::*_delay )( unsigned us );

	public:
		/** */
		Composer(
			Context&	context,
			void (Context::*initialize)(),
			void (Context::*assertSCK)(),
			void (Context::*negateSCK)(),
			void (Context::*assertMOSI)(),
			void (Context::*negateMOSI)(),
			bool (Context::*misoState)(),
			void (Context::*delay)( unsigned us )
			) noexcept;

		/** */
		void	initialize() noexcept override;

		/** */
		void	assertSCK() noexcept override;

		/** */
		void	negateSCK() noexcept override;

		/** */
		void	assertMOSI() noexcept override;

		/** */
		void	negateMOSI() noexcept override;

		/** */
		bool	misoState() noexcept override;

		/** */
		void	delay( unsigned us ) noexcept override;
	};

template < class Context >
Composer< Context >::Composer(
	Context&	context,
	void (Context::*initialize)(),
	void (Context::*assertSCK)(),
	void (Context::*negateSCK)(),
	void (Context::*assertMOSI)(),
	void (Context::*negateMOSI)(),
	bool (Context::*misoState)(),
	void (Context::*delay)( unsigned us )
	) noexcept :
		_context( context ),
		_initialize( initialize ),
		_assertSCK( assertSCK ),
		_negateSCK( negateSCK ),
		_assertMOSI( assertMOSI ),
		_negateMOSI( negateMOSI ),
		_misoState( misoState ),
		_delay( delay )
		{
	}

template < class Context >
void	Composer< Context >::initialize() noexcept {
	(_context.*_initialize)();
	}

template < class Context >
void	Composer< Context >::assertSCK() noexcept {
	(_context.*_assertSCK)();
	}

template < class Context >
void	Composer< Context >::negateSCK() noexcept {
	(_context.*_negateSCK)();
	}

template < class Context >
void	Composer< Context >::assertMOSI() noexcept {
	(_context.*_assertMOSI)();
	}

template < class Context >
void	Composer< Context >::negateMOSI() noexcept {
	(_context.*_negateMOSI)();
	}

template < class Context >
bool	Composer< Context >::misoState() noexcept {
	return (_context.*_misoState)();
	}

template < class Context >
void	Composer< Context >::delay( unsigned us ) noexcept {
	(_context.*_delay)( us );
	}

}
}
}
}

#endif
