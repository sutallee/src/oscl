/*
   Copyright (C) 2006 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_spi_master_select_apih_
#define _oscl_spi_master_select_apih_

/** */
namespace Oscl {
/** */
namespace SPI {
/** */
namespace Master {
/** */
namespace Select {

/** This interface abstracts the SPI device selection
	mechanism. Each instance of this API represents
	a different device on the SPI bus. There are many
	different ways of asserting the chip select to
	individual devices on any given SPI bus implementation.
	However, like radio buttons, only one device must be
	selected at any given time.
 */
class Api {
	public:
		/** */
		virtual ~Api(){}

		/** This operation selects the device. The caller assumes
			the operation to be atomic and that all other devices
			attached to the same SPI bus are deselected.
		 */
		virtual void	select() noexcept=0;

	};

}
}
}
}

#endif
