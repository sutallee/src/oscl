/*
   Copyright (C) 2024 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_spi_master16_itc_reqapih_
#define _oscl_spi_master16_itc_reqapih_

#include "oscl/mt/itc/mbox/srvreq.h"
#include "oscl/mt/itc/postmsgapi.h"
#include "oscl/mt/itc/mbox/sap.h"
#include <stdint.h>
#include "oscl/spi/master/select/api.h"

/** */
namespace Oscl {

/** */
namespace SPI {

/** */
namespace Master16 {

/** */
namespace Req {

/**	This class describes an ITC server interface. The
	interface includes the definition of messages and their
	associated payloads, as well as an interface that is to be
	implemented by a server that supports this ITC interface.
 */
/**	
 */
class Api {
	public:
		/** This class contains the data that is transfered
			between the client and server within the TransferReq
			ITC message.
		 */
		class TransferPayload {
			public:
				/**	
				 */
				Oscl::SPI::Master::Select::Api&	_deviceSelectApi;

				/**	
				 */
				const uint16_t*	_txDataBuffer;

				/**	
				 */
				uint16_t*	_rxDataBuffer;

				/**	
				 */
				unsigned int	_nDataWordsToTransfer;

				/**	
				 */
				uint32_t	_frequency;

				/**	
				 */
				bool	_lsbFirst;

				/**	
				 */
				bool	_cpol;

				/**	
				 */
				bool	_cpha;

				/**	
				 */
				bool	_failed;

			public:
				/** The TransferPayload constructor. */
				TransferPayload(

					Oscl::SPI::Master::Select::Api&	deviceSelectApi,

					const uint16_t*	txDataBuffer,

					uint16_t*	rxDataBuffer,

					unsigned int	nDataWordsToTransfer,

					uint32_t	frequency,

					bool	lsbFirst,

					bool	cpol,

					bool	cpha
					) noexcept;

			};

		/**	This message is a request to perform a SPI transfer.
		 */
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::SPI::Master16::Req::Api,
					TransferPayload
					>		TransferReq;

	public:
		/** This class contains the data that is transfered
			between the client and server within the CancelTransferReq
			ITC message.
		 */
		class CancelTransferPayload {
			public:
				TransferReq&	_requestToCancel;

			public:
				/** The CancelTransferPayload constructor. */
				CancelTransferPayload(TransferReq& requestToCancel) noexcept;
			};

		/**	This is a message used to cancel a TransferReq.
		 */	
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::SPI::Master16::Req::Api,
					CancelTransferPayload
					>		CancelTransferReq;

	public:
		/** This type defines a SAP (Service Access Point) for this
			ITC server interface. A SAP holds server address information
			that is required by clients to send messages to a server.
		 */
		typedef Oscl::Mt::Itc::SAP<	Oscl::SPI::Master16::
							Req::Api
							>			SAP;

	public:
		/** This type defines a SAP (Service Access Point) for this
			ITC server interface. A SAP holds server address information
			that is required by clients to send messages to a server.
		 */
		typedef Oscl::Mt::Itc::ConcreteSAP<	Oscl::SPI::Master16::
							Req::Api
							>			ConcreteSAP;

	public:
		/** Make the compiler happy. */
		virtual ~Api(){}

		/** This operation is invoked within the server thread
			whenever a TransferReq is received.
		 */
		virtual void	request(	Oscl::SPI::Master16::
									Req::Api::TransferReq& msg
									) noexcept=0;

		/** This operation is invoked within the server thread
			whenever a CancelTransferReq is received.
		 */
		virtual void	request(	Oscl::SPI::Master16::
									Req::Api::CancelTransferReq& msg
									) noexcept=0;

	};

}
}
}
}
#endif
