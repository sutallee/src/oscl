/*
   Copyright (C) 2024 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "reqapi.h"

using namespace Oscl::SPI::Master16;

Oscl::SPI::Master16::Req::Api::TransferPayload::
TransferPayload(
		Oscl::SPI::Master::Select::Api&	deviceSelectApi,
		const uint16_t*	txDataBuffer,
		uint16_t*	rxDataBuffer,
		unsigned int	nDataWordsToTransfer,
		uint32_t	frequency,
		bool	lsbFirst,
		bool	cpol,
		bool	cpha
		) noexcept:
		_deviceSelectApi(deviceSelectApi),
		_txDataBuffer(txDataBuffer),
		_rxDataBuffer(rxDataBuffer),
		_nDataWordsToTransfer(nDataWordsToTransfer),
		_frequency(frequency),
		_lsbFirst(lsbFirst),
		_cpol(cpol),
		_cpha(cpha),
		_failed(false)
		{}

Oscl::SPI::Master16::Req::Api::CancelTransferPayload::
CancelTransferPayload(Req::Api::TransferReq& requestToCancel) noexcept:
		_requestToCancel(requestToCancel)
		{}

