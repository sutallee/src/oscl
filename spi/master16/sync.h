/*
   Copyright (C) 2024 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_spi_master16_itc_synch_
#define _oscl_spi_master16_itc_synch_

#include "api.h"
#include "reqapi.h"

/** */
namespace Oscl {

/** */
namespace SPI {

/** */
namespace Master16 {

/**	This class implements synchronous ITC for the interface.
 */
class Sync : public Api {
	private:
		/** This SAP is used to identify the server.
		 */
		Oscl::SPI::Master16::Req::Api::ConcreteSAP	_sap;

	public:
		/** The constructor requires a reference to the server's
			thread/mailbox (myPapi), and a reference to the server's
			request interface.
		 */
		Sync(	Oscl::Mt::Itc::PostMsgApi&	myPapi,
				Req::Api&				reqApi
				) noexcept;

		/** It is frequently the case that this Sync class is declared
			within a server class. Since the server's clients need access
			to the server's SAP, and since the Sync class implementation
			needs the same information, this operation exports the SAP
			to the server/context such that it can be distributed to clients
			that use the server asynchronously. In this way, there is
			only a single copy of the SAP for each service.
		 */
		Req::Api::SAP&	getSAP() noexcept;

	public:
		/**	This operation transfers the number of words specified by
			the nDataWordsToTransfer argument from the buffer specified
			by the txDataBuffer argument to the device while
			simultaneously transfering the same number of octets from
			the device to the buffer specified by rxDataBuffer. Note:
			If frequency is 0 then the frequency, cpol, and cpha
			parameters are assumed to be the system selected values for
			buses that only have a single type of SPI slave device.
			Returns false when successful, or true if any error is
			encountered.
		 */
		bool	transfer(	
						Oscl::SPI::Master::Select::Api&	deviceSelectApi,
						const uint16_t*	txDataBuffer,
						uint16_t*	rxDataBuffer,
						unsigned int	nDataWordsToTransfer,
						uint32_t	frequency,
						bool	lsbFirst,
						bool	cpol,
						bool	cpha
						) noexcept;

	};

}
}
}
#endif
