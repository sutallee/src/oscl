/*
   Copyright (C) 2024 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_spi_master16_itc_respmemh_
#define _oscl_spi_master16_itc_respmemh_

#include "respapi.h"
#include "oscl/memory/block.h"

/**	These record types in this file describe blocks of memory
	that are large enough and appropriately aligned such that
	placement new operations creating instances of their
	corresponding objects can safely be performed. These records
	are useful to most clients that use the server's ITC
	interface asynchronously.
 */

/** */
namespace Oscl {

/** */
namespace SPI {

/** */
namespace Master16 {

/** */
namespace Resp {

/**	This record describes a block of memory that is
	large enough (and properly aligned) to hold a
	TransferResp and its corresponding Req::Api::TransferPayload.
	Clients which use the corresponding server asynchronously
	frequently need to reservere/manage memory for for these
	ITC interactions and instances of this record are useful
	for that purpose.
 */
struct TransferMem {
	/**	This is memory enough for the response message.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Resp::Api::TransferResp)>	resp;

	/**	This is memory enough for the message payload.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Req::Api::TransferPayload)>	payload;

	/**	This operation creates a response message using this
		 block of memory.
	 */
	Oscl::SPI::Master16::Resp::Api::TransferResp&
		build(	Oscl::SPI::Master16::Req::Api::SAP&	sap,
				Oscl::SPI::Master16::Resp::Api&		respApi,
				Oscl::Mt::Itc::PostMsgApi&		clientPapi,
				Oscl::SPI::Master::Select::Api&	deviceSelectApi,
				const uint16_t*	txDataBuffer,
				uint16_t*	rxDataBuffer,
				unsigned int	nDataWordsToTransfer,
				uint32_t	frequency,
				bool	lsbFirst,
				bool	cpol,
				bool	cpha
				) noexcept;

	};

/**	This record describes a block of memory that is
	large enough (and properly aligned) to hold a
	CancelTransferResp and its corresponding
	Req::Api::CancelTransferPayload. Clients, which use the
	corresponding server asynchronously, frequently need
	to reservere/manage memory for for these ITC interactions
	and instances of this record are useful for that purpose.
 */
struct CancelTransferMem {
	/**	This is memory enough for the response message.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Resp::Api::CancelTransferResp)>	resp;

	/**	This is memory enough for the message payload.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Req::Api::CancelTransferPayload)>	payload;

	/**	This operation creates a response message using this
		 block of memory.
	 */
	Oscl::SPI::Master16::Resp::Api::CancelTransferResp&
		build(	Oscl::SPI::Master16::Req::Api::SAP&	sap,
				Oscl::SPI::Master16::Resp::Api&		respApi,
				Oscl::Mt::Itc::PostMsgApi&		clientPapi,
				Req::Api::TransferReq&	requestToCancel
				) noexcept;

	};

}
}
}
}
#endif
