/*
   Copyright (C) 2024 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "part.h"

using namespace Oscl::SPI::Master16::BitBang;

Part::Part(
		Oscl::Mt::Itc::PostMsgApi&			papi,
		Oscl::SPI::Master::BitBang::Api&	api
		) noexcept:
	_api( api ),
	_sync(
		papi,
		*this
		)
	{
	}

void	Part::start() noexcept {
	_api.initialize();
	}

Oscl::SPI::Master16::Req::Api::SAP& Part::getSAP() noexcept {
	return _sync.getSAP();
	}

Oscl::SPI::Master16::Api&	Part::getSyncApi() noexcept {
	return _sync;
	}

uint16_t	Part::transfer(
				uint16_t	txWord,
				unsigned	periodInUs
				) noexcept {

	uint16_t	rxWord = 0;

	const unsigned
	sckDelay	= periodInUs/2;

	constexpr unsigned bitsInWord = 16;

	for( unsigned i=0; i < bitsInWord; ++i ){

		rxWord <<= 1;

		if( txWord & ( 1 << ( (bitsInWord -1) - i ) ) ){
			_api.assertMOSI();
			}
		else {
			_api.negateMOSI();
			}

		_api.delay( periodInUs/2 );

		_api.assertSCK();

		_api.delay( sckDelay );

		if( _api.misoState() ){
			rxWord	|= 1;
			}

		_api.negateSCK();

		_api.delay( sckDelay );
		}

	return rxWord;
	}

void	Part::request( Oscl::SPI::Master16::Req::Api::TransferReq& msg ) noexcept {

	Oscl::SPI::Master16::Req::Api::TransferPayload&
	payload = msg.getPayload();

	const uint16_t*
	txp	= payload._txDataBuffer;

	uint16_t*
	rxp	= payload._rxDataBuffer;

	const unsigned
	n	= payload._nDataWordsToTransfer;

	unsigned long
	periodInUs	= 1000000UL/payload._frequency;

	payload._deviceSelectApi.select();

	_api.delay( periodInUs );

	for( unsigned i = 0; i<n; ++i ){
		rxp[ i ]	= transfer( txp[ i ], periodInUs );
		}

	payload._failed	= false;

	msg.returnToSender();
	}

void	Part::request( Oscl::SPI::Master16::Req::Api::CancelTransferReq& msg ) noexcept {
	msg.returnToSender();
	}

