/*
   Copyright (C) 2024 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_spi_master16_bitbang_sharedh_
#define _oscl_spi_master16_bitbang_sharedh_

#include <stdint.h>
#include "oscl/spi/master/bitbang/api.h"
#include "part.h"
#include "oscl/driver/shared/symbiont.h"

/** */
namespace Oscl {
/** */
namespace SPI {
/** */
namespace Master16 {
/** */
namespace BitBang {

/** This implements the bit-banged SPI master as a
	shared driver symbiont, which enables it to share
	a thread with other such symbionts.
 */
class Shared :	public Oscl::Driver::Shared::Symbiont::Api {
	private:
		/** */
		Oscl::SPI::Master16::BitBang::Part	_part;

	public:
		/** */
		Shared(
			Oscl::Mt::Itc::PostMsgApi&			papi,
			Oscl::SPI::Master::BitBang::Api&	api
			) noexcept;

		/** */
		virtual ~Shared(){}

		/** */
		Oscl::SPI::Master16::Req::Api::SAP&	getSAP() noexcept;

		/** */
		Oscl::SPI::Master16::Api&	getSyncApi() noexcept;

	private: // Oscl::Driver::Shared::Symbiont::Api
		/** */
		void	initialize() noexcept override;

		/** */
		void	process() noexcept override;
	};

}
}
}
}

#endif
