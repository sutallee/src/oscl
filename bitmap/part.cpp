/*
   Copyright (C) 2020 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "part.h"

using namespace Oscl::BitMap;

Part::Part(
	uint32_t*	store,
	unsigned	storeSize,
	unsigned	lowLimit,
	unsigned	highLimit
	) noexcept:
		_store(store),
		_storeSize(storeSize),
		_maxBits(_storeSize*sizeof(uint32_t)),
		_lastBit(_maxBits -1),
		_lowLimit(lowLimit),
		_highLimit((highLimit > _lastBit)?_lastBit:highLimit)
		{
	clearAll();
	}

Part::~Part() noexcept{
	}

void	Part::clearAll() noexcept {
	for(unsigned i=0;i<_storeSize;++i){
		_store[i]	= 0;
		}
	}

void	Part::setAll() noexcept {
	for(unsigned i=0;i<_storeSize;++i){
		_store[i]	= ~0;
		}
	}

bool	Part::findContiguousClearAndSet(
			unsigned	nElements,
			unsigned&	result,
			unsigned	offset
			) noexcept{

	if(findContiguousClear(nElements,result,offset)){
		// failed to find range
		return true;
		}

	setContiguous(
		result,
		nElements
		);

	return false;
	}

void	Part::clearContiguous(
			unsigned	firstBit,
			unsigned	nBits
			) noexcept{

	for(unsigned i=0;i<nBits;++i){
		clear(firstBit+i);
		}
	}

void	Part::set(
			unsigned	bit
			) noexcept{

	if(bit > _lastBit){
		// Silently prevent overflow
		return;
		}

	unsigned	offsetWord	= bit / bitsPerWord;
	unsigned	offsetBit	= bit % bitsPerWord;

	_store[offsetWord]	|= 1<<offsetBit;
	}

void	Part::setContiguous(
			unsigned	firstBit,
			unsigned	nBits
			) noexcept{

	for(unsigned i=0;i<nBits;++i){
		set(firstBit+i);
		}

	}

void	Part::clear(
			unsigned	bit
			) noexcept{

	if(bit > _lastBit){
		// Silently prevent overflow
		return;
		}

	unsigned	offsetWord	= bit / bitsPerWord;
	unsigned	offsetBit	= bit % bitsPerWord;

	_store[offsetWord]	&= ~(1<<offsetBit);
	}

bool	Part::findFirstClearBit(
				unsigned	offset,
				unsigned&	result
				) const noexcept{

	if(offset > _lastBit){
		return true;
		}

	unsigned	offsetWord	= offset / bitsPerWord;
	unsigned	offsetBit	= offset % bitsPerWord;

	for(unsigned i = offsetWord; i < _storeSize ;++i,offsetBit=0){

		if(_store[i] == 0xFFFFFFFF){
			// Short-cut no clear bits in word
			continue;
			}

		for(unsigned j=offsetBit;j<bitsPerWord;++j){

			bool
			bitIsSet	= _store[i] & (1<<j);

			if(!bitIsSet){
				result	= i*bitsPerWord + j;
				return false;
				}
			}
		}

	return true;
	}

bool	Part::findFirstSetBit(
				unsigned	offset,
				unsigned&	result
				) const noexcept{

	if(offset > _lastBit){
		return true;
		}

	unsigned	offsetWord	= offset / bitsPerWord;
	unsigned	offsetBit	= offset % bitsPerWord;

	for(unsigned i = offsetWord; i < _storeSize ;++i){

		if(_store[i] == 0x00000000){
			// Short-cut no set bits in word
			continue;
			}

		for(unsigned j=offsetBit;j<bitsPerWord;++j,offsetBit=0){

			bool
			bitIsSet	= _store[i] & (1<<j);

			if(bitIsSet){
				result	= i*bitsPerWord + j;
				return false;
				}
			}
		}

	return true;
	}

bool	Part::findContiguousClear(
				unsigned	nBits,
				unsigned&	result,
				unsigned	offset
				) const noexcept{

	unsigned	firstBit	= offset;

	unsigned	remaining	= nBits;

	unsigned	startOffset	= (_lowLimit > offset)?_lowLimit:offset;

	for(
		unsigned i = startOffset;
		(i <= _highLimit) && (i < _maxBits );
		++i
		){

		unsigned a;

		if(findFirstClearBit(i,a)){
			// No space
			return true;
			}

		if((a != i) || !firstBit){
			// We are not contiguous
			// This is the first available
			// address found.
			i			= a;
			firstBit	= a;
			remaining	= nBits;
			}

		remaining		-= 1;

		if(!remaining){
			result	= firstBit;
			return false;
			}
		}

	// We can't find any available address
	// range of that size.

	return true;
	}

