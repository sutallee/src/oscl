/*
   Copyright (C) 2020 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_bitmap_parth_
#define _oscl_bitmap_parth_

#include <stdint.h>

/** */
namespace Oscl {
/** */
namespace BitMap {

/** */
class Part {
	private:
		/** */
		uint32_t* const 	_store;

		/** */
		const unsigned		_storeSize;

		/** */
		const unsigned		_maxBits;

		/** */
		const unsigned		_lastBit;

		/** */
		static constexpr unsigned	bitsPerWord = sizeof(uint32_t)*8;

		/** */
		const unsigned		_lowLimit;

		/** */
		const unsigned		_highLimit;

	public:
		/** */
		Part(
			uint32_t*	store,
			unsigned	storeSize,
			unsigned	lowLimit,
			unsigned	highLimit
			) noexcept;

		/** */
		~Part() noexcept;

		/** Find first contiguous set of bits that
			are 0 (clear), set them to 1 (set), and
			return the offset to the first bit in the set.

			This may be used as is a form of
			allocation/reservation. Freeing/un-reserving
			the bits can be done using clearContiguous().

			RETURN:
				true on failure
				false on success and set result to the
				offset of the first bit in contiguous set.
		 */
		bool	findContiguousClearAndSet(
						unsigned 	n,
						unsigned&	result,
						unsigned	offset	= 0
						) noexcept;

		/** */
		void	clearContiguous(
					unsigned	firstBit,
					unsigned	nBits
					) noexcept;

		/** */
		void	setContiguous(
					unsigned	firstBit,
					unsigned	nBits
					) noexcept;

		/** */
		void	set(unsigned bit) noexcept;

		/** */
		void	clear(unsigned bit) noexcept;

		/** */
		void	clearAll() noexcept;

		/** */
		void	setAll() noexcept;

		/**	Find a contiguous range of n bits that are not set.
			RETURN:
				true on failure
				false on success and result set to the first
				bit in the range.
		 */
		bool	findContiguousClear(
						unsigned	n,
						unsigned&	result,
						unsigned	offset	= 0
						) const noexcept;

		/** Find the first bit from offset that is set to 1.
			RETURN:
				Failure: true
				Success: false and first bit returned in result
		 */
		bool	findFirstSetBit(
						unsigned	offset,
						unsigned&	result
						) const noexcept;
	private:
		/** Find the first bit from offset that is not set.
			RETURN:
				Failure: true
				Success: false and first bit returned in result
		 */
		bool	findFirstClearBit(
						unsigned	offset,
						unsigned&	result
						) const noexcept;
	};
}
}

#endif
