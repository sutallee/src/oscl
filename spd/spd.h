#ifndef _oscl_spd_spdh_
#define _oscl_spd_spdh_

/** */
namespace Oscl {
/** */
namespace SPD {

/** This data structure represents the content of a JEDEC Standard No. 21-C
	Serial Presence Detect (SPD) EEPROM, which contains information about
	SDRAM module parameters.
 */
struct SerialPresenceDetectData {
	/** [0] unit: bytes
		Typically 128 bytes
		The total number of bytes used by the module manufacturer for the SPD
		data and any (optional) specific supplier information. The byte count
		includes the fields for all required and optional data.
		*/
	unsigned char	numberOfBytesWrittenDuringModuleProduction;
	/** [1] unit: bytes. 2^n where n is the value in the SPD
		Typically 256 bytes (i.e. value of 8, thus 2^8 == 256)
		Describes the total size of the serial memory used to hold the SPD
		data.
		*/
	unsigned char	totalNumberOfBytesInDevice;
	/** [2] unit: enumerated
			1	:	Standard FPM DRAM
			2	:	EDO
			3	:	Pipelined Nibble
			4	:	SDR SDRAM
			5	:	ROM
			6	:	DDR SGRAM
			7	:	DDR SDRAM
	 */
	unsigned char	fundamentalMemoryType;
	/** [3]
		Describes the row addressing on the module. If there is one physical
		bank on the module or if there are two physical banks of the same size
		and organization, then bits 0-3 are used to represent the number of
		row addresses for each physical bank. If the module has two physical
		banks of asynmetric size, then bits 0-3 represent the number of row
		addresses for physical bank 1 and bits 4-7 represent the number of
		row addresses for physical bank 2 (bank2 device is 2x bank 1 device
		width). Note that these do not include the Bank Address pin since
		physical bank selection of DIMM modules is asserted on dedicated BA
		(Bank Address) pins.
	 */
	unsigned char	numberOfRowAddresses;
	/** [4]
		Describes the column addressing on the module. If there is one physical
		bank on the module or if there are two physical banks of the same size,
		then bits 0-3 are used to represent the number of column addresses for
		physical bank 1 and bits 4-7 represent the number of column addresses
		for physical bank 2 (bank 2 device is 2x bank 1 device width.)
	 */
	unsigned char	numberOfColumnAddresses;
	/** [5]
		Describes the number of physical banks on the SDRAM module. The number
		of logical banks for the SDRAM is contained in the
		numberOfBanksOnSdramDevice field.
	 */
	unsigned char	numberOfPhysicalBanks;
	/** [6:7] unit: bits
		Designate the modules data width. The data width is presented as a
		16-bit word: bit 0 of the first byte becomes the LSB of the 16 bit
		width identififier and bit 7 of the second byte becomes the MSB.
		Consequently, if the module has a width of less than 255 bits, the
		second byte will be 0x00. If the data width is 256 bits or more,
		the second byte is used in conjunction with the first byte to
		designate the total module width.
	 */
	unsigned char	dataWidth[2];
	/** [8] unit: enumeration
		Describes the module's voltage interface.
	 */
	unsigned char	voltageInterfaceLevel;
	/** [9]
		units: seconds
		granularity: 1/10ns
		formate: xxxx.yyyy ns
		From datasheet.
		Defines the minimum cycle time for the SDRAM device at the highest
		CAS latency, CAS latency=X, defined in the casLatency field. This
		register is split into two nibbles: the higher order nibble (bits 4-7)
		designates the cycle time to a granularity of 1ns; the value presented
		by the lower order nibble (bits 0-3) has a granularity of 0.1 ns and is
		added to the value designated by the higher nibble.
	 */
	unsigned char	tCKmin;
	/** [10]
		units: seconds
		granularity: 1/100 ns
		format: 0.xxxxyyyy ns
		Defines the maximum clock to data out time for the SDRAM device. This
		is the clock to data out specification at the highest given CAS
		latency specified in the casLatency field. The byte is split into two
		nibbles: the higer order nibble designates the access time to a
		ranularity of 0.1ns; the value presented by the lower order nibble
		has the granularity of 0.01ns and is added to the value designated by
		the higher nibble.
	 */
	unsigned char	tAC;
	/** [11] unit: enumeration
		Describes the module's error detection and/or correction scheme.
	 */
	unsigned char	dimmConfigurationType;
	/** [12] units: enumerated
		From datasheet.
		High order bit is Self Refresh "flag".
		If set to "1", the assembly supports self refresh
		Describes the module's refresh rate and type.
		*/
	unsigned char	refreshRateType;
	/** [13] Bit 7 units: flag, Bit 0-6 units: bits
		Bits 0-6 indicate the width of the primary data SDRAM. Bit 7 is a
		flag which is set to "1" when there is a second physical bank on
		the module which is of different size from the first physical bank
		(the second physical bank's data RAMs are 2X the width of those on
		the first physical bank.) If there is a second physical bank of the
		same size and configuration as the first, then bit 7 remains as "0"
		and primary SDRAM width for both banks is expressed using bits 0-6.
		The primary SDRAM is that which is used for data; examples of primary
		(data) SDRAM widths are x4, x8, x16, and x32. Note that if the module
		is made with SDRAMs which provide for data and error checking,
		e.g. x9, x18, and x36, then it is also designated in this field.
	 */
	unsigned char	primarySdramWidth;
	/** [14] Bit 7 units: flag, Bit 0-6 units: bits
		If the module incorporates error checking and if the primary data
		SDRAM does not include these bits (i.e. there are separate error
		checking SDRAMs) then the error checking SDRAM's width is expressed
		in this byte. Bits 0-6 of this byte relate the error checking SDRAM's
		width. Bit 7 is a flag set to "1" when the module has a second physical
		bank that is a different size than the first physical bank. Bit 7 set
		to "1" indicates that Bank 2's error checking RAMS are 2X the width
		of those on the first physical bank. If there is a second physical
		bank of the same size and configration as the first, then bit 7
		remains as "0" and error checking SDRAM width for both physical banks
		is expressed using bits 0-6.
	 */
	unsigned char	errorCheckingSdramWidth;
	/** [15] units: clocks
		Describes the minimum clock delay for back-to-bak random
		column accesses.
	 */
	unsigned char	minClockDelayBackToBakRandomColumnAccess;
	/** [16]
		Describes which various programmable burst lengths are supported by
		the devices on the module. If the bit is "1", then that burst length
		is supported on the module; if the bit is "0", then that burst length
		is not supported by the module.
	 */
	unsigned char	supportedBurstLengths;
	/** [17] units: banks
		From datasheet.
		Details how many banks are on each SDRAM installed onto the module.
	 */
	unsigned char	numberOfBanksOnSdramDevice;
	/** [18]
		From datasheet.
		Describes which of the programmable CAS latencies (CAS to data out)
		are acceptable for the SDRAM devices used on the module. If the bit
		is "1", then that CAS latency is supported on the module; if the
		bit is "0", then that CAS latency is not supported by the module.
	 */
	unsigned char	casLatency;
	/** [19]
		From datasheet.
		Defines the chip select latency associated with the SDRAM devices used
		on the module. If the bit is "1", then that CS latency (Chip Select to
		command time) is supported on the module; if the bit is "0", then that
		CS latency is not supported by the module. For DDR SDRAMs, the CS
		latency is always 0 (0x01).
	 */
	unsigned char	chipSelectLatency;
	/** [20]
		From datasheet.
		Defines the write latency associated with the SDRAM devices used on
		the module. If the bit is "1", then that W latency (number of clocks
		from WE to data in) is supported on the module; if the bit is "0", then
		that W latency is not supported by the module. For DDR SDRAMS, the
		W latency is always 1 (0x02).
	 */
	unsigned char	writeLatency;
	/** [21]
		Depicts various aspects of the module. It details various unrelated
		but critical elements pertinent to the module. A given module
		characteristic is detailed in the designated bit; if the aspect is
		TRUE, then the bit is "1". Conversely, if the aspect is FALSE, the
		designated bit is "0"
	 */
	unsigned char	moduleAttributes;
	/** [22]
		From datasheet.
		Depeicts various aspects of the SDRAMs on the module. It details
		various elements pertinent to the SDRAMs. A given SDRAM characteristic
		is detailed in the designated bit; if the aspect is TRUE, then the bit
		is "1". Conversly, if the aspect is FALSE, the designated bit is "0".
	 */
	unsigned char	deviceAttributes;
	/** [23]
		units: seconds
		granularity: 1/10 ns
		format: xxxx.yyyy ns
		From datasheet.
		The higheset CAS latency identified in casLatency is X and the
		timing values associated with CAS latency 'X' are found at tCKmin
		and tAC. This byte denotes the minimum cycle time at CAS latency
		X-0.5. For example if casLatency is 1.5 to 2.5 then X is 2.5 and
		X-0.5 is 2. This byte then denotes the minimum cycle time at CAS
		latency 2. This byte is split into two nibbles: the higher order
		nibble (bits 4-7) designate the cycle time to a granularity of 1ns;
		the value presented by the low order nibble (bits 0-3) has a
		granularity of 0.1ns and is added to the value designated by the
		higher nibble.
	 */
	unsigned char	minimumClockCycleAtClx05;
	/** [24]
		units: seconds
		granularity: 1/100 ns
		format: 0.xxxxyyyy ns
		From datasheet.
		The highest CAS latency identified by casLatency is X. This byte
		denotes the maximum access time (+) from clock at CAS latency X-0.5.
		For example, if casLatency denotes supported CAS latencies of 1.5 to
		2.5, then X is 2.5 and X-0.5 is 2. This byte then denotes the maximum
		clock accesss time from CLK at CAS latency 2. This byte is split into
		two nibbles: the higher order nibble (bits 4-7) designate the accesss
		time to agranularity of 0.1ns; the value presented by the lower order
		nibble (0-3) has the granularity of 0.01ns and is added to the value
		designated by the higher nibble.
	 */
	unsigned char	tAcAtCLX05;
	/** [25]
		units: seconds
		granularity: 1/10 ns
		format: xxxx.yyyy ns
		From datasheet.
		The highest CAS latency identified by casLatency is X. This byte
		denotes the minimum cycle time at CAS latency X-1.
		For example, if casLatency denotes CAS latencies of 1.5 to 2.5, then
		X is 2.5 and X-1 is 1.5. This byte then denotes the minimum cycle
		time at CAS latency 1.5.
		This byte is split into two nibbles: the higher order nibble (bits 4-7)
		designates the cycle time to a granularity of 1ns; the value presented
		by the lower order nibble (bits 0-3) has a granularity of 0.1ns and
		is added to the value designated by the higher order nibble.
		
	 */
	unsigned char	minimumClockCycleAtCLX1;
	/** [26]
		units: seconds
		granularity: 1/100 ns
		format: 0.xxxxyyyy ns
		From datasheet.
		The highest CAS latency identified by casLatency is X. This byte
		denotes the maximum access time (+) from Clock at CAS latency X-1.
		For example, if casLatency denotes supported CAS latencies of
		1.5 to 2.5, then X is 2.5 and X-1 is 1.5. This byte then denotes the
		maximum data access time fro CLK at CAS latency 1.5.
		This byte is split into two nibbles: the higher order nibble
		(bits 4-7) designate the access time to a granularity of 0.1ns; the
		value presented by the lower order nibble (bits 0-3) has the
		granularity of 0.01ns and is added to the value designated by the
		higher nibble.
	 */
	unsigned char	tAcAtCLX1;
	/** [27]
		units: seconds
		granularity : 1/4 ns
		format: xxxxxx.yy ns
		From datasheet.
		This byte is used to designate the minimum row precharge time of the
		SDRAM devices on the module.
		This byte is split into two pieces: the higher order bits (bits 2-7)
		designate the time to a granularity of 1ns; the value presented by the
		lower order bits (bits 0-1) has a granularity of 0.25ns and is added
		to the value designated by the higher bits.
	 */
	unsigned char	tRP;
	/** [28]
		units: seconds
		granularity : 1/4 ns
		format: xxxxxx.yy ns
		From datasheet.
		This field describes the minimum required delay between different
		row activations.
		This byte is split into two pieces: the higher order bits (bits 2-7)
		designate the time to a granularity of 1ns; the value presented by the
		lower order bits (bits 0-1) has a granularity of 0.25ns and is added
		to the value designated by the higher bits.
	 */
	unsigned char	tRRD;
	/** [29]
		units: seconds
		granularity : 1/4 ns
		format: xxxxxx.yy ns
		From datasheet.
		This byte describes the minimum delay required between assertions
		of RAS and CAS.
		This byte is split into two pieces: the higher order bits (bits 2-7)
		designate the time to a granularity of 1ns; the value presented by the
		lower order bits (bits 0-1) has a granularity of 0.25ns and is added
		to the value designated by the higher bits.
		*/
	unsigned char	tRCD;
	/** [30]
		units: seconds
		granularity : 1 ns
		format: xxxxxxxx ns
		From datasheet.
		This byte describes the minimum active to precharge time.
	 */
	unsigned char	tRAS;
	/** [31] unit: enumerated
		This byte describes the density of each physical bank on the SRAM	
		DIMM. This byte will have at least one bit set to "1" to represent
		at least one bank's density. If there are more than one physical
		bank on the module (as represented by numberOfPhysicalBanks), and
		they have the same density, then only one bit is set in this field. If
		the module has more than one physical b ank of different sizes, then
		more than one bit will be set; each bit set for each desnsity
		represented.
	 */
	unsigned char	moduleBankDensity;
	/** [32]
		units: seconds
		granularity: 1/100 ns
		format: 0.xxxxyyyy ns
		From datasheet.
		This field describes the input setup time before the rising edge of
		the clock at the SDRAM device on unbuffered modules, or at the
		register on registered modules.
		This byte is split into two nibbles: the higher order nibble
		(bits 4-7) designate the access time to a granularity of 0.1ns; the
		value presented by the lower order nibble (bits 0-3) has the
		granularity of 0.01ns and is added to the value designated by the
		higher nibble.
	 */
	unsigned char	addressAndCommandInputSetupTimeBeforeClock;
	/** [33]
		units: seconds
		granularity: 1/100 ns
		format: 0.xxxxyyyy ns
		From datasheet.
		This field describes the input hold time after the rising edge of the
		clock at the SDRAM device on unbuffered modules, or at the register
		on registered modules.
		This byte is split into two nibbles: the higher order nibble
		(bits 4-7) designate the access time to a granularity of 0.1ns; the
		value presented by the lower order nibble (bits 0-3) has the
		granularity of 0.01ns and is added to the value designated by the
		higher nibble.
	 */
	unsigned char	addressAndCommandInputHoldTimeAfterClock;
	/** [34]
		units: seconds
		granularity: 1/100 ns
		format: 0.xxxxyyyy ns
		From datasheet.
		This field describes the input setup time before the rising or falling
		edge of the data strobe.
		This byte is split into two nibbles: the higher order nibble
		(bits 4-7) designate the access time to a granularity of 0.1ns; the
		value presented by the lower order nibble (bits 0-3) has the
		granularity of 0.01ns and is added to the value designated by the
		higher nibble.
	 */
	unsigned char	sdramDeviceDataMaskInputSetupTimeBeforeDataStrobe;
	/** [35]
		units: seconds
		granularity: 1/100 ns
		format: 0.xxxxyyyy ns
		From datasheet.
		This field describes the input hold time after the rising or falling
		edge of the data strobe.
		This byte is split into two nibbles: the higher order nibble
		(bits 4-7) designate the access time to a granularity of 0.1ns; the
		value presented by the lower order nibble (bits 0-3) has the
		granularity of 0.01ns and is added to the value designated by the
		higher nibble.
	 */
	unsigned char	sdramDeviceDataMaskInputHoldTimeAfterDataStrobe;
	/** [36:40] */
	unsigned char	reservedForVCSDRAM[41-36];
	/** [41]
		units: seconds
		granularity: 1 ns
		format: xxxxxxxx ns
		From datasheet.
		This byte identifies the minimum active to active or auto refresh time.
	 */
	unsigned char	tRC;
	/** [42] From datasheet */
	unsigned char	tRFC;
	/** [43] From datasheet */
	unsigned char	tCKmax;
	/** [44] From datasheet */
	unsigned char	tDQSQ;
	/** [45] From datasheet */
	unsigned char	tQHS;
	/** [46:61] */
	unsigned char	supersetInformation[62-46];
	/** [62] */
	unsigned char	spdRevision;
	/** [63] */
	unsigned char	checksumForBytes0thru62;
	/** [64:71] Optional, in accordance with the JEDEC spec.*/
	unsigned char	manufacturerersJedecIdCode[72-64];
	/** [72] Optional, in accordance with the JEDEC spec. */
	unsigned char	moduleManufacturingLocation;
	/** [73:90] Optional, in accordance with the JEDEC spec. */
	unsigned char	modulePartNumber[91-73];
	/** [91:92] Optional, in accordance with the JEDEC spec. */
	unsigned char	moduleRevisionCode[2];
	/** [93:94] Optional, in accordance with the JEDEC spec. */
	unsigned char	moduleManufacturingData[2];
	/** [95:98] Optional, in accordance with the JEDEC spec. */
	unsigned char	moduleSerialNumber[99-95];
	/** [99:127] Optional, in accordance with the JEDEC spec. */
	unsigned char	manufacturersSpecificData[128-99];
	/** [128:255] */
	unsigned char	openForCustomerUse[256-128];
	};

}
}

#endif
