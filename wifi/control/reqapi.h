/*
   Copyright (C) 2024 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_wifi_control_itc_reqapih_
#define _oscl_wifi_control_itc_reqapih_

#include "oscl/mt/itc/mbox/srvreq.h"
#include "oscl/mt/itc/postmsgapi.h"
#include "oscl/mt/itc/mbox/sap.h"
#include <stdint.h>
#include "station.h"

/** */
namespace Oscl {

/** */
namespace WiFi {

/** */
namespace Control {

/** */
namespace Req {

/**	This class describes an ITC server interface. The
	interface includes the definition of messages and their
	associated payloads, as well as an interface that is to be
	implemented by a server that supports this ITC interface.
 */
/**	
 */
class Api {
	public:
		/** This class contains the data that is transfered
			between the client and server within the DisconnectNetworkReq
			ITC message.
		 */
		class DisconnectNetworkPayload {
			public:
				/**	
				 */
				bool	_failed;

			public:
				/** The DisconnectNetworkPayload constructor. */
				DisconnectNetworkPayload(
					) noexcept;

			};

		/**	
		 */
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::WiFi::Control::Req::Api,
					DisconnectNetworkPayload
					>		DisconnectNetworkReq;

	public:
		/** This class contains the data that is transfered
			between the client and server within the JoinNetworkWPA2_AESReq
			ITC message.
		 */
		class JoinNetworkWPA2_AESPayload {
			public:
				/**	
				 */
				const char*	_ssid;

				/**	
				 */
				const char*	_passPhrase;

				/**	
				 */
				bool	_failed;

			public:
				/** The JoinNetworkWPA2_AESPayload constructor. */
				JoinNetworkWPA2_AESPayload(

					const char*	ssid,

					const char*	passPhrase
					) noexcept;

			};

		/**	
		 */
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::WiFi::Control::Req::Api,
					JoinNetworkWPA2_AESPayload
					>		JoinNetworkWPA2_AESReq;

	public:
		/** This class contains the data that is transfered
			between the client and server within the GetNextStationReq
			ITC message.
		 */
		class GetNextStationPayload {
			public:
				/**	
				 */
				Station&	_station;

				/**	
				 */
				bool	_failed;

			public:
				/** The GetNextStationPayload constructor. */
				GetNextStationPayload(
					Station&	station
					) noexcept;

			};

		/**	
		 */
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::WiFi::Control::Req::Api,
					GetNextStationPayload
					>		GetNextStationReq;

	public:
		/** This class contains the data that is transfered
			between the client and server within the StopScanReq
			ITC message.
		 */
		class StopScanPayload {
			public:
				/**	
				 */
				bool	_failed;

			public:
				/** The StopScanPayload constructor. */
				StopScanPayload(
					) noexcept;

			};

		/**	
		 */
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::WiFi::Control::Req::Api,
					StopScanPayload
					>		StopScanReq;

	public:
		/** This class contains the data that is transfered
			between the client and server within the StartScanReq
			ITC message.
		 */
		class StartScanPayload {
			public:
				/**	
				 */
				bool	_failed;

			public:
				/** The StartScanPayload constructor. */
				StartScanPayload(
					) noexcept;

			};

		/**	
		 */
		typedef Oscl::Mt::Itc::SrvRequest<
					Oscl::WiFi::Control::Req::Api,
					StartScanPayload
					>		StartScanReq;

	public:
		/** This type defines a SAP (Service Access Point) for this
			ITC server interface. A SAP holds server address information
			that is required by clients to send messages to a server.
		 */
		typedef Oscl::Mt::Itc::SAP<	Oscl::WiFi::Control::
							Req::Api
							>			SAP;

	public:
		/** This type defines a SAP (Service Access Point) for this
			ITC server interface. A SAP holds server address information
			that is required by clients to send messages to a server.
		 */
		typedef Oscl::Mt::Itc::ConcreteSAP<	Oscl::WiFi::Control::
							Req::Api
							>			ConcreteSAP;

	public:
		/** Make the compiler happy. */
		virtual ~Api(){}

		/** This operation is invoked within the server thread
			whenever a DisconnectNetworkReq is received.
		 */
		virtual void	request(	Oscl::WiFi::Control::
									Req::Api::DisconnectNetworkReq& msg
									) noexcept=0;

		/** This operation is invoked within the server thread
			whenever a JoinNetworkWPA2_AESReq is received.
		 */
		virtual void	request(	Oscl::WiFi::Control::
									Req::Api::JoinNetworkWPA2_AESReq& msg
									) noexcept=0;

		/** This operation is invoked within the server thread
			whenever a GetNextStationReq is received.
		 */
		virtual void	request(	Oscl::WiFi::Control::
									Req::Api::GetNextStationReq& msg
									) noexcept=0;

		/** This operation is invoked within the server thread
			whenever a StopScanReq is received.
		 */
		virtual void	request(	Oscl::WiFi::Control::
									Req::Api::StopScanReq& msg
									) noexcept=0;

		/** This operation is invoked within the server thread
			whenever a StartScanReq is received.
		 */
		virtual void	request(	Oscl::WiFi::Control::
									Req::Api::StartScanReq& msg
									) noexcept=0;

	};

}
}
}
}
#endif
