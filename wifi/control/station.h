#ifndef _oscl_wifi_stationh_
#define _oscl_wifi_stationh_
#include <stdint.h>
/** */
namespace Oscl {

/** */
namespace WiFi {

/** */
namespace Control {

/** */
struct Station {
	enum struct NetworkType : uint8_t {
		Infrastructure,
		unknown = 255
		};
	enum struct SecurityType : uint8_t {
		open,
		wpa,
		wpa2AES,
		wpa2Mixed,
		unknown = 255
		};
	enum struct RadioBand : uint8_t {
		_2_4GHz,
		_5GHz,
		unknown = 255
		};
	double			maxDataRate;	// Mbps
	int16_t			rssi;
	char			ssid[32+1];
	uint8_t			macAddress[6];
	NetworkType		networkType;
	SecurityType	securityType;
	RadioBand		radioBand;
	uint8_t			channel;
	};

}
}
}

#endif
