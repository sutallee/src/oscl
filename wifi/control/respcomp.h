/*
   Copyright (C) 2024 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_wifi_control_respcomph_
#define _oscl_wifi_control_respcomph_

#include "respapi.h"

/** */
namespace Oscl {

/** */
namespace WiFi {

/** */
namespace Control {

/** */
namespace Resp {

/**	This template class implements a kind of GOF decorator
	pattern allows the context to employ composition instead
	of inheritance. Frequently, a context may need to implement
	more than one interface that may result in method name
	clashes. This template solves the problem by implementing
	the interface and then invoking member function pointers
	in the context instead. One instance of this object is
	created in the context for each interface of this type used
	in the context. Each instance is constructed such that it
	refers to different member functions within the context.
	The interface of this object is passed to any object that
	requires the interface. When the object invokes any of the
	operations of the API, the corresponding member function
	of the context will subsequently be invoked.
 */
/**	
 */
template <class Context>
class Composer : public Api {
	private:
		/** A reference to the Context.
		 */
		Context&	_context;

	private:
		/**	
		 */
		void	(Context::*_DisconnectNetwork)(Oscl::WiFi::Control::Resp::Api::DisconnectNetworkResp& msg);

		/**	
		 */
		void	(Context::*_JoinNetworkWPA2_AES)(Oscl::WiFi::Control::Resp::Api::JoinNetworkWPA2_AESResp& msg);

		/**	
		 */
		void	(Context::*_GetNextStation)(Oscl::WiFi::Control::Resp::Api::GetNextStationResp& msg);

		/**	
		 */
		void	(Context::*_StopScan)(Oscl::WiFi::Control::Resp::Api::StopScanResp& msg);

		/**	
		 */
		void	(Context::*_StartScan)(Oscl::WiFi::Control::Resp::Api::StartScanResp& msg);

	public:
		/** */
		Composer(
			Context&		context,
			void	(Context::*DisconnectNetwork)(Oscl::WiFi::Control::Resp::Api::DisconnectNetworkResp& msg),
			void	(Context::*JoinNetworkWPA2_AES)(Oscl::WiFi::Control::Resp::Api::JoinNetworkWPA2_AESResp& msg),
			void	(Context::*GetNextStation)(Oscl::WiFi::Control::Resp::Api::GetNextStationResp& msg),
			void	(Context::*StopScan)(Oscl::WiFi::Control::Resp::Api::StopScanResp& msg),
			void	(Context::*StartScan)(Oscl::WiFi::Control::Resp::Api::StartScanResp& msg)
			) noexcept;

	private:
		/**	
		 */
		void	response(Oscl::WiFi::Control::Resp::Api::DisconnectNetworkResp& msg) noexcept;

		/**	
		 */
		void	response(Oscl::WiFi::Control::Resp::Api::JoinNetworkWPA2_AESResp& msg) noexcept;

		/**	
		 */
		void	response(Oscl::WiFi::Control::Resp::Api::GetNextStationResp& msg) noexcept;

		/**	
		 */
		void	response(Oscl::WiFi::Control::Resp::Api::StopScanResp& msg) noexcept;

		/**	
		 */
		void	response(Oscl::WiFi::Control::Resp::Api::StartScanResp& msg) noexcept;

	};

template <class Context>
Composer<Context>::Composer(
			Context&		context,
			void	(Context::*DisconnectNetwork)(Oscl::WiFi::Control::Resp::Api::DisconnectNetworkResp& msg),
			void	(Context::*JoinNetworkWPA2_AES)(Oscl::WiFi::Control::Resp::Api::JoinNetworkWPA2_AESResp& msg),
			void	(Context::*GetNextStation)(Oscl::WiFi::Control::Resp::Api::GetNextStationResp& msg),
			void	(Context::*StopScan)(Oscl::WiFi::Control::Resp::Api::StopScanResp& msg),
			void	(Context::*StartScan)(Oscl::WiFi::Control::Resp::Api::StartScanResp& msg)
			) noexcept:
		_context(context),
		_DisconnectNetwork(DisconnectNetwork),
		_JoinNetworkWPA2_AES(JoinNetworkWPA2_AES),
		_GetNextStation(GetNextStation),
		_StopScan(StopScan),
		_StartScan(StartScan)
		{
	}

template <class Context>
void	Composer<Context>::response(Oscl::WiFi::Control::Resp::Api::DisconnectNetworkResp& msg) noexcept{
	(_context.*_DisconnectNetwork)(msg);
	}

template <class Context>
void	Composer<Context>::response(Oscl::WiFi::Control::Resp::Api::JoinNetworkWPA2_AESResp& msg) noexcept{
	(_context.*_JoinNetworkWPA2_AES)(msg);
	}

template <class Context>
void	Composer<Context>::response(Oscl::WiFi::Control::Resp::Api::GetNextStationResp& msg) noexcept{
	(_context.*_GetNextStation)(msg);
	}

template <class Context>
void	Composer<Context>::response(Oscl::WiFi::Control::Resp::Api::StopScanResp& msg) noexcept{
	(_context.*_StopScan)(msg);
	}

template <class Context>
void	Composer<Context>::response(Oscl::WiFi::Control::Resp::Api::StartScanResp& msg) noexcept{
	(_context.*_StartScan)(msg);
	}

}
}
}
}
#endif
