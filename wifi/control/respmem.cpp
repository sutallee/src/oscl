/*
   Copyright (C) 2024 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <new>
#include "respmem.h"

using namespace Oscl::WiFi::Control;

Resp::Api::DisconnectNetworkResp&
	Resp::DisconnectNetworkMem::build(	Req::Api::SAP&			sap,
							Resp::Api&				respApi,
							Oscl::Mt::Itc::PostMsgApi&	clientPapi
							) noexcept{
	Req::Api::DisconnectNetworkPayload*
		p	=	new(&payload)
				Req::Api::DisconnectNetworkPayload(
);
	Resp::Api::DisconnectNetworkResp*
		r	=	new(&resp)
			Resp::Api::DisconnectNetworkResp(	sap.getReqApi(),
									respApi,
									clientPapi,
									*p
									);

	return *r;
	}

Resp::Api::JoinNetworkWPA2_AESResp&
	Resp::JoinNetworkWPA2_AESMem::build(	Req::Api::SAP&			sap,
							Resp::Api&				respApi,
							Oscl::Mt::Itc::PostMsgApi&	clientPapi,
							const char*	ssid,
							const char*	passPhrase
							) noexcept{
	Req::Api::JoinNetworkWPA2_AESPayload*
		p	=	new(&payload)
				Req::Api::JoinNetworkWPA2_AESPayload(
							ssid,
							passPhrase
							);
	Resp::Api::JoinNetworkWPA2_AESResp*
		r	=	new(&resp)
			Resp::Api::JoinNetworkWPA2_AESResp(	sap.getReqApi(),
									respApi,
									clientPapi,
									*p
									);

	return *r;
	}

Resp::Api::GetNextStationResp&
	Resp::GetNextStationMem::build(	Req::Api::SAP&			sap,
							Resp::Api&				respApi,
							Oscl::Mt::Itc::PostMsgApi&	clientPapi,
							Station&	station
							) noexcept{
	Req::Api::GetNextStationPayload*
		p	=	new(&payload)
				Req::Api::GetNextStationPayload(
							station
							);
	Resp::Api::GetNextStationResp*
		r	=	new(&resp)
			Resp::Api::GetNextStationResp(	sap.getReqApi(),
									respApi,
									clientPapi,
									*p
									);

	return *r;
	}

Resp::Api::StopScanResp&
	Resp::StopScanMem::build(	Req::Api::SAP&			sap,
							Resp::Api&				respApi,
							Oscl::Mt::Itc::PostMsgApi&	clientPapi
							) noexcept{
	Req::Api::StopScanPayload*
		p	=	new(&payload)
				Req::Api::StopScanPayload(
);
	Resp::Api::StopScanResp*
		r	=	new(&resp)
			Resp::Api::StopScanResp(	sap.getReqApi(),
									respApi,
									clientPapi,
									*p
									);

	return *r;
	}

Resp::Api::StartScanResp&
	Resp::StartScanMem::build(	Req::Api::SAP&			sap,
							Resp::Api&				respApi,
							Oscl::Mt::Itc::PostMsgApi&	clientPapi
							) noexcept{
	Req::Api::StartScanPayload*
		p	=	new(&payload)
				Req::Api::StartScanPayload(
);
	Resp::Api::StartScanResp*
		r	=	new(&resp)
			Resp::Api::StartScanResp(	sap.getReqApi(),
									respApi,
									clientPapi,
									*p
									);

	return *r;
	}

