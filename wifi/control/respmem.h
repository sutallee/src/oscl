/*
   Copyright (C) 2024 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_wifi_control_itc_respmemh_
#define _oscl_wifi_control_itc_respmemh_

#include "respapi.h"
#include "oscl/memory/block.h"

/**	These record types in this file describe blocks of memory
	that are large enough and appropriately aligned such that
	placement new operations creating instances of their
	corresponding objects can safely be performed. These records
	are useful to most clients that use the server's ITC
	interface asynchronously.
 */

/** */
namespace Oscl {

/** */
namespace WiFi {

/** */
namespace Control {

/** */
namespace Resp {

/**	This record describes a block of memory that is
	large enough (and properly aligned) to hold a
	DisconnectNetworkResp and its corresponding Req::Api::DisconnectNetworkPayload.
	Clients which use the corresponding server asynchronously
	frequently need to reservere/manage memory for for these
	ITC interactions and instances of this record are useful
	for that purpose.
 */
struct DisconnectNetworkMem {
	/**	This is memory enough for the response message.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Resp::Api::DisconnectNetworkResp)>	resp;

	/**	This is memory enough for the message payload.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Req::Api::DisconnectNetworkPayload)>	payload;

	/**	This operation creates a response message using this
		 block of memory.
	 */
	Oscl::WiFi::Control::Resp::Api::DisconnectNetworkResp&
		build(	Oscl::WiFi::Control::Req::Api::SAP&	sap,
				Oscl::WiFi::Control::Resp::Api&		respApi,
				Oscl::Mt::Itc::PostMsgApi&		clientPapi
				) noexcept;

	};

/**	This record describes a block of memory that is
	large enough (and properly aligned) to hold a
	JoinNetworkWPA2_AESResp and its corresponding Req::Api::JoinNetworkWPA2_AESPayload.
	Clients which use the corresponding server asynchronously
	frequently need to reservere/manage memory for for these
	ITC interactions and instances of this record are useful
	for that purpose.
 */
struct JoinNetworkWPA2_AESMem {
	/**	This is memory enough for the response message.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Resp::Api::JoinNetworkWPA2_AESResp)>	resp;

	/**	This is memory enough for the message payload.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Req::Api::JoinNetworkWPA2_AESPayload)>	payload;

	/**	This operation creates a response message using this
		 block of memory.
	 */
	Oscl::WiFi::Control::Resp::Api::JoinNetworkWPA2_AESResp&
		build(	Oscl::WiFi::Control::Req::Api::SAP&	sap,
				Oscl::WiFi::Control::Resp::Api&		respApi,
				Oscl::Mt::Itc::PostMsgApi&		clientPapi,
				const char*	ssid,
				const char*	passPhrase
				) noexcept;

	};

/**	This record describes a block of memory that is
	large enough (and properly aligned) to hold a
	GetNextStationResp and its corresponding Req::Api::GetNextStationPayload.
	Clients which use the corresponding server asynchronously
	frequently need to reservere/manage memory for for these
	ITC interactions and instances of this record are useful
	for that purpose.
 */
struct GetNextStationMem {
	/**	This is memory enough for the response message.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Resp::Api::GetNextStationResp)>	resp;

	/**	This is memory enough for the message payload.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Req::Api::GetNextStationPayload)>	payload;

	/**	This operation creates a response message using this
		 block of memory.
	 */
	Oscl::WiFi::Control::Resp::Api::GetNextStationResp&
		build(	Oscl::WiFi::Control::Req::Api::SAP&	sap,
				Oscl::WiFi::Control::Resp::Api&		respApi,
				Oscl::Mt::Itc::PostMsgApi&		clientPapi,
				Station&	station
				) noexcept;

	};

/**	This record describes a block of memory that is
	large enough (and properly aligned) to hold a
	StopScanResp and its corresponding Req::Api::StopScanPayload.
	Clients which use the corresponding server asynchronously
	frequently need to reservere/manage memory for for these
	ITC interactions and instances of this record are useful
	for that purpose.
 */
struct StopScanMem {
	/**	This is memory enough for the response message.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Resp::Api::StopScanResp)>	resp;

	/**	This is memory enough for the message payload.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Req::Api::StopScanPayload)>	payload;

	/**	This operation creates a response message using this
		 block of memory.
	 */
	Oscl::WiFi::Control::Resp::Api::StopScanResp&
		build(	Oscl::WiFi::Control::Req::Api::SAP&	sap,
				Oscl::WiFi::Control::Resp::Api&		respApi,
				Oscl::Mt::Itc::PostMsgApi&		clientPapi
				) noexcept;

	};

/**	This record describes a block of memory that is
	large enough (and properly aligned) to hold a
	StartScanResp and its corresponding Req::Api::StartScanPayload.
	Clients which use the corresponding server asynchronously
	frequently need to reservere/manage memory for for these
	ITC interactions and instances of this record are useful
	for that purpose.
 */
struct StartScanMem {
	/**	This is memory enough for the response message.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Resp::Api::StartScanResp)>	resp;

	/**	This is memory enough for the message payload.
	 */
	Oscl::Memory::AlignedBlock<sizeof(Req::Api::StartScanPayload)>	payload;

	/**	This operation creates a response message using this
		 block of memory.
	 */
	Oscl::WiFi::Control::Resp::Api::StartScanResp&
		build(	Oscl::WiFi::Control::Req::Api::SAP&	sap,
				Oscl::WiFi::Control::Resp::Api&		respApi,
				Oscl::Mt::Itc::PostMsgApi&		clientPapi
				) noexcept;

	};

}
}
}
}
#endif
