/*
   Copyright (C) 2024 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_wifi_control_itc_respapih_
#define _oscl_wifi_control_itc_respapih_

#include "reqapi.h"
#include "oscl/mt/itc/mbox/clirsp.h"

/** */
namespace Oscl {

/** */
namespace WiFi {

/** */
namespace Control {

/** */
namespace Resp {

/**	This class describes an ITC client interface. The
	interface includes the definition of client response messages
	based on their corresponding server request messages and
	associated payloads, as well as an interface that is to be
	implemented by a client that uses the corresponding server
	asynchronously.
 */
/**	
 */
class Api {
	public:
		/**	This describes a client response message that corresponds
			to the DisconnectNetworkReq message described in the "reqapi.h"
			header file. This DisconnectNetworkResp response message actually
			contains a DisconnectNetworkReq request message.
		 */
		typedef Oscl::Mt::Itc::CliResponse<
					Oscl::WiFi::Control::Req::Api,
					Api,
					Oscl::WiFi::Control::
					Req::Api::DisconnectNetworkPayload
					>		DisconnectNetworkResp;

	public:
		/**	This describes a client response message that corresponds
			to the JoinNetworkWPA2_AESReq message described in the "reqapi.h"
			header file. This JoinNetworkWPA2_AESResp response message actually
			contains a JoinNetworkWPA2_AESReq request message.
		 */
		typedef Oscl::Mt::Itc::CliResponse<
					Oscl::WiFi::Control::Req::Api,
					Api,
					Oscl::WiFi::Control::
					Req::Api::JoinNetworkWPA2_AESPayload
					>		JoinNetworkWPA2_AESResp;

	public:
		/**	This describes a client response message that corresponds
			to the GetNextStationReq message described in the "reqapi.h"
			header file. This GetNextStationResp response message actually
			contains a GetNextStationReq request message.
		 */
		typedef Oscl::Mt::Itc::CliResponse<
					Oscl::WiFi::Control::Req::Api,
					Api,
					Oscl::WiFi::Control::
					Req::Api::GetNextStationPayload
					>		GetNextStationResp;

	public:
		/**	This describes a client response message that corresponds
			to the StopScanReq message described in the "reqapi.h"
			header file. This StopScanResp response message actually
			contains a StopScanReq request message.
		 */
		typedef Oscl::Mt::Itc::CliResponse<
					Oscl::WiFi::Control::Req::Api,
					Api,
					Oscl::WiFi::Control::
					Req::Api::StopScanPayload
					>		StopScanResp;

	public:
		/**	This describes a client response message that corresponds
			to the StartScanReq message described in the "reqapi.h"
			header file. This StartScanResp response message actually
			contains a StartScanReq request message.
		 */
		typedef Oscl::Mt::Itc::CliResponse<
					Oscl::WiFi::Control::Req::Api,
					Api,
					Oscl::WiFi::Control::
					Req::Api::StartScanPayload
					>		StartScanResp;

	public:
		/** Make the compiler happy. */
		virtual ~Api(){}

		/** This operation is invoked within the client thread
			whenever a DisconnectNetworkResp message is received.
		 */
		virtual void	response(	Oscl::WiFi::Control::
									Resp::Api::DisconnectNetworkResp& msg
									) noexcept=0;

		/** This operation is invoked within the client thread
			whenever a JoinNetworkWPA2_AESResp message is received.
		 */
		virtual void	response(	Oscl::WiFi::Control::
									Resp::Api::JoinNetworkWPA2_AESResp& msg
									) noexcept=0;

		/** This operation is invoked within the client thread
			whenever a GetNextStationResp message is received.
		 */
		virtual void	response(	Oscl::WiFi::Control::
									Resp::Api::GetNextStationResp& msg
									) noexcept=0;

		/** This operation is invoked within the client thread
			whenever a StopScanResp message is received.
		 */
		virtual void	response(	Oscl::WiFi::Control::
									Resp::Api::StopScanResp& msg
									) noexcept=0;

		/** This operation is invoked within the client thread
			whenever a StartScanResp message is received.
		 */
		virtual void	response(	Oscl::WiFi::Control::
									Resp::Api::StartScanResp& msg
									) noexcept=0;

	};

}
}
}
}
#endif
