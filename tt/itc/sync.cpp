/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "sync.h"
#include "reqapi.h"
#include "oscl/mt/itc/mbox/syncrh.h"

using namespace Oscl::TT::ITC;

Sync::Sync(	Oscl::TT::ITC::Req::Api&	reqApi,
			Oscl::Mt::Itc::PostMsgApi&	myPapi
			) noexcept:
		_sap(reqApi,myPapi)
		{
	}

Oscl::TT::ITC::Req::Api::SAP&	Sync::getSAP() noexcept{
	return _sap;
	}

bool	Sync::reset() noexcept{
	Oscl::Mt::Itc::SyncReturnHandler	srh;
	Req::Api::ResetPayload				payload;
	Req::Api::ResetReq					req(_sap.getReqApi(),payload,srh);
	_sap.postSync(req);
	return payload._failed;
	}

bool	Sync::getMaxLUN(unsigned char& maxLun) noexcept{
	Oscl::Mt::Itc::SyncReturnHandler	srh;
	Req::Api::GetMaxLunPayload			payload;
	Req::Api::GetMaxLunReq				req(_sap.getReqApi(),payload,srh);
	_sap.postSync(req);
	maxLun	= payload._maxLun;
	return payload._failed;
	}

const Oscl::TT::Status::Result*	Sync::read(	unsigned char	lun,
											const void*		cb,
											unsigned		cbLen,
											void*			buffer,
											unsigned long	bufferLen,
											unsigned long&	residue
											) noexcept{
	Oscl::Mt::Itc::SyncReturnHandler	srh;
	Req::Api::ReadPayload				payload(	lun,
													cb,
													cbLen,
													buffer,
													bufferLen
													);
	Req::Api::ReadReq					req(_sap.getReqApi(),payload,srh);
	_sap.postSync(req);
	residue	= payload._residue;
	return payload._result;
	}

const Oscl::TT::Status::Result*	Sync::write(	unsigned char	lun,
												const void*		cb,
												unsigned		cbLen,
												const void*		buffer,
												unsigned long	bufferLen,
												unsigned long&	residue
												) noexcept{
	Oscl::Mt::Itc::SyncReturnHandler	srh;
	Req::Api::WritePayload				payload(	lun,
													cb,
													cbLen,
													buffer,
													bufferLen
													);
	Req::Api::WriteReq					req(_sap.getReqApi(),payload,srh);
	_sap.postSync(req);
	residue	= payload._residue;
	return payload._result;
	}

const Oscl::TT::Status::Result*	Sync::command(	unsigned char	lun,
												const void*		cb,
												unsigned		cbLen
												) noexcept{
	Oscl::Mt::Itc::SyncReturnHandler	srh;
	Req::Api::CommandPayload			payload(	lun,
													cb,
													cbLen
													);
	Req::Api::CommandReq					req(_sap.getReqApi(),payload,srh);
	_sap.postSync(req);
	return payload._result;
	}

