/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "reqapi.h"

using namespace Oscl::TT::ITC;

Req::Api::ResetPayload::
ResetPayload() noexcept:
		_failed(false)
		{
	}

Req::Api::GetMaxLunPayload::
GetMaxLunPayload() noexcept:
		_failed(false)
		{
	}

Req::Api::CancelPayload::
CancelPayload(Oscl::Mt::Itc::SrvMsg& msgToCancel) noexcept:
		_msgToCancel(msgToCancel)
		{
	}

Req::Api::ReadPayload::ReadPayload(	unsigned char	lun,
									const void*		cb,
									unsigned		cbLen,
									void*			buffer,
									unsigned long	bufferLen
									) noexcept:
		_lun(lun),
		_cb(cb),
		_cbLen(cbLen),
		_buffer(buffer),
		_bufferLen(bufferLen),
		_result(0)
		{
	}

Req::Api::WritePayload::WritePayload(	unsigned char	lun,
										const void*		cb,
										unsigned		cbLen,
										const void*		buffer,
										unsigned long	bufferLen
										) noexcept:
		_lun(lun),
		_cb(cb),
		_cbLen(cbLen),
		_buffer(buffer),
		_bufferLen(bufferLen),
		_result(0)
		{
	}

Req::Api::CommandPayload::CommandPayload(	unsigned char	lun,
											const void*		cb,
											unsigned		cbLen
											) noexcept:
		_lun(lun),
		_cb(cb),
		_cbLen(cbLen),
		_result(0)
		{
	}

