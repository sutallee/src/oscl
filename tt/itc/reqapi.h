/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_tt_itc_reqapih_
#define _oscl_tt_itc_reqapih_
#include "oscl/mt/itc/mbox/srvreq.h"
#include "oscl/mt/itc/mbox/sap.h"

/** */
namespace Oscl {
/** */
namespace TT {

/** */
namespace Status{
/** */
class Result;
}

/** */
namespace ITC {
/** */
namespace Req {

using namespace Oscl::Mt::Itc;

/** */
class Api {
	public:
		/** */
		virtual ~Api() {}
		/** */
		class ResetPayload {
			public:
				bool	_failed;
			public:
				ResetPayload() noexcept;
			};
		/** */
		class GetMaxLunPayload {
			public:
				/** */
				unsigned char					_maxLun;
				/** */
				bool							_failed;
			public:
				/** */
				GetMaxLunPayload() noexcept;
			};
		/** */
		class CancelPayload {
			public:
				/** */
				Oscl::Mt::Itc::SrvMsg&	_msgToCancel;
			public:
				/** */
				CancelPayload(Oscl::Mt::Itc::SrvMsg& msgToCancel) noexcept;
			};
		/** */
		class ReadPayload {
			public:
				/**	Logical Unit Number
					range 0 - 15
					Zero is the "Device" LUN.
				 */
				const unsigned char						_lun;
				/** Command Block
					Contains protocol specific command.
					The maximum length of the command is
					constrained to no more than 16 bytes.
				 */
				const void*	const						_cb;
				/** Command Block Length
					range 1 - 16
					Number of bytes in the command block.
				 */
				const unsigned							_cbLen;
				/** Memory where data read by command will
					be placed.
				 */
				void*	const							_buffer;
				/** Maximum length of buffer in bytes.
				 */
				const unsigned long						_bufferLen;
				/** The difference between the amount of data expected
					and the actual amount copied to buffer.
				 */
				unsigned long							_residue;
				/** Result pointer is ZERO if the operation completed
					without error. Otherwise, a pointer to result
					code is returned. The reference can be used
					to determine the reason for the failure.
				 */
				const Oscl::TT::Status::Result*			_result;

			public:
				/** */
				ReadPayload(	unsigned char	lun,
								const void*		cb,
								unsigned		cbLen,
								void*			buffer,
								unsigned long	bufferLen
								) noexcept;
			};
		/** */
		class WritePayload {
			public:
				/**	Logical Unit Number
					range 0 - 15
					Zero is the "Device" LUN.
				 */
				const unsigned char						_lun;
				/** Command Block
					Contains protocol specific command.
					The maximum length of the command is
					constrained to no more than 16 bytes.
				 */
				const void*	const						_cb;
				/** Command Block Length
					range 1 - 16
					Number of bytes in the command block.
				 */
				const unsigned							_cbLen;
				/** Points to memory containing data to be written
					to the LUN.
				 */
				const void*	const						_buffer;
				/** Number of valid bytes in the buffer that will
					be written to the LUN.
				 */
				const unsigned long						_bufferLen;
				/** A return value that is the difference between the
					amount of data actually written and that requested
					by _bufferLen.
				 */
				unsigned long							_residue;
				/** Result pointer is ZERO if the operation completed
					without error. Otherwise, a pointer to result
					code is returned. The reference can be used
					to determine the reason for the failure.
				 */
				const Oscl::TT::Status::Result*			_result;

			public:
				/** */
				WritePayload(	unsigned char	lun,
								const void*		cb,
								unsigned		cbLen,
								const void*		buffer,
								unsigned long	bufferLen
								) noexcept;
			};
		/** */
		class CommandPayload {
			public:
				/**	Logical Unit Number
					range 0 - 15
					Zero is the "Device" LUN.
				 */
				const unsigned char						_lun;
				/** Command Block
					Contains protocol specific command.
					The maximum length of the command is
					constrained to no more than 16 bytes.
				 */
				const void*	const						_cb;
				/** Command Block Length
					range 1 - 16
					Number of bytes in the command block.
				 */
				const unsigned							_cbLen;
				/** Result pointer is ZERO if the operation completed
					without error. Otherwise, a pointer to result
					code is returned. The reference can be used
					to determine the reason for the failure.
				 */
				const Oscl::TT::Status::Result*			_result;

			public:
				/** */
				CommandPayload(	unsigned char	lun,
								const void*		cb,
								unsigned		cbLen
								) noexcept;
			};
	public:
		/** A message that requests that the server RESET
			the "device".
		 */
		typedef SrvRequest<Api,ResetPayload>		ResetReq;
		/** A message that requests the server return
			the value of the largest Logical Unit Number.
			Devices with only one LUN will return 0.
		 */
		typedef SrvRequest<Api,GetMaxLunPayload>	GetMaxLunReq;
		/** A message that requests that a specific request
			to be cancelled.
		 */
		typedef SrvRequest<Api,CancelPayload>		CancelReq;
		/** */
		typedef SrvRequest<Api,ReadPayload>			ReadReq;
		/** */
		typedef SrvRequest<Api,WritePayload>		WriteReq;
		/** */
		typedef SrvRequest<Api,CommandPayload>		CommandReq;

	public:
		/** */
		typedef Oscl::Mt::Itc::SAP<Api>				SAP;
		/** */
		typedef Oscl::Mt::Itc::ConcreteSAP<Api>		ConcreteSAP;

	public:
		/** */
		virtual void	request(ResetReq& msg) noexcept=0;
		/** */
		virtual void	request(GetMaxLunReq& msg) noexcept=0;
		/** */
		virtual void	request(CancelReq& msg) noexcept=0;
		/** */
		virtual void	request(ReadReq& msg) noexcept=0;
		/** */
		virtual void	request(WriteReq& msg) noexcept=0;
		/** */
		virtual void	request(CommandReq& msg) noexcept=0;
	};

}
}
}
}

#endif
