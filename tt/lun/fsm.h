/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_tt_lun_fsmh_
#define _oscl_tt_lun_fsmh_
#include "oscl/queue/queue.h"
#include "oscl/queue/queueitem.h"

/** */
namespace Oscl {
/** */
namespace TT {
/** */
namespace LUN {

/** */
class StateVar {
	public:
		/** */
		class ContextApi {
			public:	// request actions
				/** Make GCC happy */
				virtual ~ContextApi() {}
				/** */
				virtual void	returnOpenReq() noexcept=0;
				/** */
				virtual void	returnCloseReq() noexcept=0;
				/** */
				virtual void	executeDeferredReq() noexcept=0;
				/** */
				virtual void	deferReadReq() noexcept=0;
				/** */
				virtual void	deferWriteReq() noexcept=0;
				/** */
				virtual void	deferFlushReq() noexcept=0;
				/** */
				virtual void	startRead() noexcept=0;
				/** */
				virtual void	startWrite() noexcept=0;
				/** */
				virtual void	startFlush() noexcept=0;
				/** */
				virtual	void	startRequestSense() noexcept=0;
				/** */
				virtual void	returnReadReq() noexcept=0;
				/** */
				virtual void	returnWriteReq() noexcept=0;
				/** */
				virtual void	returnFlushReq() noexcept=0;
				/** */
				virtual void	returnIncommingFlushReq() noexcept=0;
				/** */
				virtual void	returnIncommingReadCancelReq() noexcept=0;
				/** */
				virtual void	returnIncommingWriteCancelReq() noexcept=0;
				/** */
				virtual void	returnReadCancelReq() noexcept=0;
				/** */
				virtual void	returnWriteCancelReq() noexcept=0;
				/** */
				virtual void	cancelRead() noexcept=0;
				/** */
				virtual void	cancelWrite() noexcept=0;
				/** */
				virtual void	cancelCommand() noexcept=0;
				/** */
				virtual void	finishRead() noexcept=0;
				/** */
				virtual void	finishWrite() noexcept=0;
				/** */
				virtual void	finishCommand() noexcept=0;
				/** */
				virtual void	finishFlushReqSense() noexcept=0;
				/** */
				virtual bool	isCommandCheckCondition() noexcept=0;
				/** */
				virtual bool	deferQueueEmpty() noexcept=0;
				/** */
				virtual bool	isIncommingCancelForCurrentRead() noexcept=0;
				/** */
				virtual bool	isIncommingCancelForCurrentWrite() noexcept=0;
				/** */
				virtual bool	isIncommingCancelForCurrentFlush() noexcept=0;
				/** */
				virtual bool	isFlushSupportedByDevice() noexcept=0;
			};
	private:
		/** */
		class State {
			public:
				/** Make GCC happy */
				virtual ~State() {}

			public:	// Admin Events
				/** */
				virtual void	open(	ContextApi&	context,
										StateVar&	sv
										) const noexcept;
				/** */
				virtual void	close(	ContextApi&	context,
										StateVar&	sv
										) const noexcept;
				/** */
				virtual void	cancelResp(	ContextApi&	context,
											StateVar&	sv
											) const noexcept;
				/** */
				virtual void	writeResp(	ContextApi&	context,
											StateVar&	sv
											) const noexcept;
				/** */
				virtual void	readResp(	ContextApi&	context,
											StateVar&	sv
											) const noexcept;
				/** */
				virtual void	commandResp(	ContextApi&	context,
												StateVar&	sv
												) const noexcept;
				/** */
				virtual void	readReq(	ContextApi&	context,
											StateVar&	sv
											) const noexcept;
				/** */
				virtual void	writeReq(	ContextApi&	context,
											StateVar&	sv
											) const noexcept;
				/** */
				virtual void	flushReq(	ContextApi&	context,
											StateVar&	sv
											) const noexcept;
				/** */
				virtual void	readCancelReq(	ContextApi&	context,
												StateVar&	sv
												) const noexcept;
				/** */
				virtual void	writeCancelReq(	ContextApi&	context,
												StateVar&	sv
												) const noexcept;
			public:
				/** */
				virtual const char*	stateName() const noexcept=0;
			};

	public:
		/** */
		class Closed : public State {
			private:
				/** */
				const char*	stateName() const noexcept
					{ return "Closed";}
			private:
				/** */
				void	open(	ContextApi&	context,
								StateVar&	sv
								) const noexcept;
			};
		/** */
		class Idle : public State {
			private:
				/** */
				const char*	stateName() const noexcept
					{ return "Idle";}
			public:
				/** */
				void	close(	ContextApi&	context,
								StateVar&	sv
								) const noexcept;
				/** */
				void	readReq(	ContextApi&	context,
									StateVar&	sv
									) const noexcept;
				/** */
				void	writeReq(	ContextApi&	context,
									StateVar&	sv
									) const noexcept;
				/** */
				void	flushReq(	ContextApi&	context,
									StateVar&	sv
									) const noexcept;
				/** */
				void	readCancelReq(	ContextApi&	context,
										StateVar&	sv
										) const noexcept;
				/** */
				void	writeCancelReq(	ContextApi&	context,
										StateVar&	sv
										) const noexcept;
			};
		/** */
		class DeferReq : public State {
			public:
				/** */
				void	readReq(	ContextApi&	context,
									StateVar&	sv
									) const noexcept;
				/** */
				void	writeReq(	ContextApi&	context,
									StateVar&	sv
									) const noexcept;
				/** */
				void	flushReq(	ContextApi&	context,
									StateVar&	sv
									) const noexcept;
			};
		/** */
		class Write : public DeferReq {
			private:
				/** */
				const char*	stateName() const noexcept
					{ return "Write";}
			public:
				/** */
				void	writeResp(	ContextApi&	context,
									StateVar&	sv
									) const noexcept;
				/** */
				void	writeCancelReq(	ContextApi&	context,
										StateVar&	sv
										) const noexcept;
			};
		/** */
		class Flush : public DeferReq {
			private:
				/** */
				const char*	stateName() const noexcept
					{ return "Flush";}
			public:
				/** */
				void	commandResp(	ContextApi&	context,
										StateVar&	sv
										) const noexcept;
				/** */
				void	writeCancelReq(	ContextApi&	context,
										StateVar&	sv
										) const noexcept;
			};
		/** */
		class FlushReqSense : public DeferReq {
			private:
				/** */
				const char*	stateName() const noexcept
					{ return "FlushReqSense";}
			public:
				/** */
				void	readResp(	ContextApi&	context,
									StateVar&	sv
									) const noexcept;
				/** */
				void	writeCancelReq(	ContextApi&	context,
										StateVar&	sv
										) const noexcept;
			};
		/** */
		class Read : public DeferReq {
			private:
				/** */
				const char*	stateName() const noexcept
					{ return "Read";}
			public:
				/** */
				void	readResp(	ContextApi&	context,
									StateVar&	sv
									) const noexcept;
				/** */
				void	readCancelReq(	ContextApi&	context,
										StateVar&	sv
										) const noexcept;
			};

		/** */
		class ReadCanceling : public State {
			private:
				/** */
				const char*	stateName() const noexcept
					{ return "ReadCanceling";}
			public:
				/** */
				void	cancelResp(	ContextApi&	context,
									StateVar&	sv
									) const noexcept;
				/** */
				void	readResp(	ContextApi&	context,
									StateVar&	sv
									) const noexcept;
			};

		/** */
		class WriteCanceling : public State {
			private:
				/** */
				const char*	stateName() const noexcept
					{ return "WriteCanceling";}
			public:
				/** */
				void	cancelResp(	ContextApi&	context,
									StateVar&	sv
									) const noexcept;
				/** */
				void	writeResp(	ContextApi&	context,
									StateVar&	sv
									) const noexcept;
			};
		/** */
		class FlushCanceling : public State {
			private:
				/** */
				const char*	stateName() const noexcept
					{ return "FlushCanceling";}
			public:
				/** */
				void	cancelResp(	ContextApi&	context,
									StateVar&	sv
									) const noexcept;
				/** */
				void	commandResp(	ContextApi&	context,
										StateVar&	sv
										) const noexcept;
				/** */
				void	readResp(	ContextApi&	context,
									StateVar&	sv
									) const noexcept;
			};
	private:
		/** */
		struct TraceRec : public Oscl::QueueItem {
			public:
				/** */
				const State*	_state;
			};

	private:
		/** */
		ContextApi&		_context;
		/** */
		const State*	_state;

	private:
		/** */
		enum{nTraceRecs=20};
		/** */
		TraceRec				_traceRec[nTraceRecs];
		/** */
		Oscl::Queue<TraceRec>	_freeTraceRecs;
		/** */
		Oscl::Queue<TraceRec>	_trace;

	public:	// Constructors
		/** */
		StateVar(ContextApi& context) noexcept;

	public:	// events
		/** */
		void	open() noexcept;
		/** */
		void	close() noexcept;
		/** */
		void	cancelResp() noexcept;
		/** */
		void	writeResp() noexcept;
		/** */
		void	commandResp() noexcept;
		/** */
		void	readResp() noexcept;
		/** */
		void	writeReq() noexcept;
		/** */
		void	flushReq() noexcept;
		/** */
		void	readReq() noexcept;
		/** */
		void	writeCancelReq() noexcept;
		/** */
		void	readCancelReq() noexcept;

	private: // global actions and state change operations
		/** */
		friend class State;
		friend class Closed;
		friend class Idle;
		friend class Read;
		friend class Write;
		friend class Flush;
		friend class ReadCanceling;
		friend class WriteCanceling;
		friend class FlushCanceling;
		/** */
		void	protocolError(const char* state) noexcept;
		/** */
		void	changeToClosed() noexcept;
		/** */
		void	changeToIdle() noexcept;
		/** */
		void	changeToRead() noexcept;
		/** */
		void	changeToWrite() noexcept;
		/** */
		void	changeToFlush() noexcept;
		/** */
		void	changeToFlushReqSense() noexcept;
		/** */
		void	changeToReadCanceling() noexcept;
		/** */
		void	changeToWriteCanceling() noexcept;
		/** */
		void	changeToFlushCanceling() noexcept;

	private:
		/** */
		void	trace(const State& state) noexcept;
	};

}
}
}

#endif
