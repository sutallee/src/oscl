/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <new>
#include <string.h>
#include <stdio.h>
#include "driver.h"
#include "oscl/mt/thread.h"
#include "oscl/error/fatal.h"
#include "oscl/error/info.h"
#include "oscl/tt/status.h"
#include "oscl/tt/info.h"
#include "oscl/ms/fat/partition.h"
#include "oscl/fs/fat/fat.h"
#include "oscl/scsi/cmd/scsi.h" 
#include "oscl/block/read/status.h"
#include "oscl/block/write/status.h"

static const char trueStr[]="true";
static const char falseStr[]="false";

using namespace Oscl::TT::LUN;

Driver::Driver(	Oscl::Mt::Itc::PostMsgApi&		myPapi,
				Oscl::TT::ITC::Req::Api::SAP&	ttSAP,
				Oscl::TT::Api&					ttApi,
				PacketMem&						packetMem
				) noexcept:
		Oscl::Mt::Itc::Srv::CloseSync(*this,myPapi),
		_packetMem(packetMem),
		_myPapi(myPapi),
		_ttSAP(ttSAP),
		_ttApi(ttApi),
		_readSync(*this,myPapi),
		_writeSync(*this,myPapi),
		_state(*this),
		_openReq(0),
		_closeReq(0),
		_flushSupported(true)
		{
	}

void	Driver::request(	Oscl::Mt::Itc::Srv::
							Open::Req::Api::OpenReq&	msg
							) noexcept{
	_openReq	= &msg;
	_state.open();
	}

void	Driver::jungsoftSeq() noexcept{
	uint32_t	firstPartitionLBA;
	if(printMBR(firstPartitionLBA)){
		printRequestSense();
		}
	else {
		printPartitionHeader(firstPartitionLBA);
		}
	}

void	Driver::genericSeq() noexcept{
	while(printTestUnitReady()){
		printRequestSense();
		}
	if(printLoadMedium()){
		printRequestSense();
		}
	for(unsigned i=0; (i<3) && printTestUnitReady();++i){
		printRequestSense();
		}
	if(printStartSync()){
		printRequestSense();
		}
	for(unsigned i=0; (i<3) && printTestUnitReady();++i){
		printRequestSense();
		}
	if(printReadCapacity()){
		printRequestSense();
		}
	for(unsigned i=0; (i<3) && printTestUnitReady();++i){
		printRequestSense();
		}
	for(unsigned long i=0;i<3;++i){
		if(printRead10Block(i)){
			printRequestSense();
			}
		if(printTestUnitReady()){
			printRequestSense();
			}
		}
	if(printModeSense6(0x80)){
		printRequestSense();
		}
	if(printRead6Block(1)){
		printRequestSense();
		}
	}

void	Driver::request(	Oscl::Mt::Itc::Srv::
							Close::Req::Api::CloseReq&	msg
							) noexcept{
	_closeReq	= &msg;
	_state.close();
	}

void	Driver::response(ResetResp& msg) noexcept{
	// Not called from here ... yet?
	}

void	Driver::response(GetMaxLunResp& msg) noexcept{
	// Not called from here ... yet?
	}

void	Driver::response(CancelResp& msg) noexcept{
	_incomming.cancelResp	= &msg;
	_state.cancelResp();
	}

void	Driver::response(ReadResp& msg) noexcept{
	_incomming.readResp	= &msg;
	_state.readResp();
	}

void	Driver::response(WriteResp& msg) noexcept{
	_incomming.writeResp	= &msg;
	_state.writeResp();
	}

void	Driver::response(CommandResp& msg) noexcept{
	_incomming.commandResp	= &msg;
	_state.commandResp();
	}

////////////////////////

bool	Driver::printInquiry() noexcept{
	Oscl::SCSI::CMD::Inquiry::Standard	inquiry(Oscl::SCSI::CMD::Inquiry::Response::size);
	unsigned long	residue;
	Oscl::Error::Info::log("starting INQUIRY\n");
	const Oscl::TT::Status::Result*
	result = _ttApi.read(	0,
							&inquiry,
							sizeof(inquiry),
							&_packetMem.data,
							Oscl::SCSI::CMD::Inquiry::Response::size,
							residue
							);
	if(result){
		Oscl::Error::Info::log(	"Fatal USB Mass Storage"
								" Standard INQUIRY failure: "
								);
		Oscl::TT::Status::Info	info;
		result->query(info);
		return true;
		}
	Oscl::SCSI::CMD::Inquiry::print(*(Oscl::SCSI::CMD::Inquiry::Response*)&_packetMem.data);
	return false;
	}

bool	Driver::printRequestSense() noexcept{
	Oscl::Error::Info::log("starting REQUEST-SENSE\n");
	Oscl::SCSI::CMD::RequestSense::Standard	requestSense(Oscl::SCSI::CMD::RequestSense::Response::size);
	unsigned long	residue;
	const Oscl::TT::Status::Result*
	result = _ttApi.read(	0,
							&requestSense,
							sizeof(requestSense),
							&_packetMem.data,
							Oscl::SCSI::CMD::RequestSense::Response::size,
							residue
							);
	if(result){
		Oscl::Error::Info::log(	"Fatal USB Mass Storage"
								" Standard REQUEST-SENSE failure: "
								);
		Oscl::TT::Status::Info	info;
		result->query(info);
		return true;
		}
	Oscl::SCSI::CMD::RequestSense::print(*(Oscl::SCSI::CMD::RequestSense::Response*)&_packetMem.data);
	return false;
	}

bool	Driver::printReadCapacity() noexcept{
	unsigned long	residue;
	Oscl::Error::Info::log("starting READ-CAPACITY\n");
	Oscl::SCSI::CMD::ReadCapacity::RBC	readCapacity;
	const Oscl::TT::Status::Result*
	result = _ttApi.read(	0,
							&readCapacity,
							10, // sizeof(readCapacity),
							&_packetMem.data,
							Oscl::SCSI::CMD::ReadCapacity::Response::size,
							residue
							);
	if(result){
		Oscl::Error::Info::log(	"Fatal USB Mass Storage"
								" Standard READ-CAPACITY failure: "
								);
		Oscl::TT::Status::Info	info;
		result->query(info);
		return true;
		}
	Oscl::SCSI::CMD::ReadCapacity::print(*(Oscl::SCSI::CMD::ReadCapacity::Response*)&_packetMem.data);
	return false;
	}

bool	Driver::printModeSense10(uint8_t pageCode) noexcept{
	unsigned long	residue;
	Oscl::Error::Info::log("starting MODE-SENSE(10)\n");
	Oscl::SCSI::CMD::ModeSense10::Standard	modeSense(8,pageCode);
	const Oscl::TT::Status::Result*
	result = _ttApi.read(	0,
							&modeSense,
							10,
							&_packetMem.data,
							8,	// This size is imperitive for JUNSOFT (at least)
							residue
							);
	if(result){
		Oscl::Error::Info::log(	"Fatal USB Mass Storage"
								" MODE-SENSE(10) failure: "
								);
		Oscl::TT::Status::Info	info;
		result->query(info);
		return true;
		}
	Oscl::SCSI::CMD::ModeSense10::print(*(Oscl::SCSI::CMD::ModeSense10::Response*)&_packetMem.data);
	return false;
	}

bool	Driver::printModeSense6(uint8_t pageCode) noexcept{
	unsigned long	residue;
	Oscl::Error::Info::log("starting MODE-SENSE(6)\n");
	Oscl::SCSI::CMD::ModeSense6::RBC	modeSense(8,pageCode,false);
	const Oscl::TT::Status::Result*
	result = _ttApi.read(	0,
							&modeSense,
							6,
							&_packetMem.data,
							8,	// This size is imperitive for JUNSOFT (at least)
							residue
							);
	if(result){
		Oscl::Error::Info::log(	"Fatal USB Mass Storage"
								" MODE-SENSE(6) failure: "
								);
		Oscl::TT::Status::Info	info;
		result->query(info);
		return true;
		}
	Oscl::SCSI::CMD::ModeSense6::print(*(Oscl::SCSI::CMD::ModeSense6::Response*)&_packetMem.data);
	return false;
	}

bool	Driver::printRead6Block(unsigned long block) noexcept{
	unsigned long	residue;
	Oscl::Error::Info::log("starting READ(6)\n");
	Oscl::SCSI::CMD::Read6::Standard	read6(block,1);
	memset(&_packetMem.data,0,sizeof(_packetMem.data));
	const Oscl::TT::Status::Result*
	result = _ttApi.read(	0,
							&read6,
							6,
							&_packetMem.data,
							512,	// MUST be exactly nBlocks*512
							residue
							);
	if(result){
		Oscl::Error::Info::log(	"Fatal USB Mass Storage"
								" READ(6) failure: "
								);
		Oscl::TT::Status::Info	info;
		result->query(info);
		return true;
		}
	Oscl::SCSI::CMD::Read6::print(&_packetMem.data);
	return false;
	}

bool	Driver::printRead10Block(unsigned long block) noexcept{
	unsigned long	residue;
	char	buff[64];
	sprintf(buff,"starting READ(10) @ LBA 0x%8.8lX\n",block);
	Oscl::Error::Info::log(buff);
	Oscl::SCSI::CMD::Read10::Standard	read10(block,1);
	memset(&_packetMem.data,0xAA,sizeof(_packetMem.data));
	const Oscl::TT::Status::Result*
	result = _ttApi.read(	0,
							&read10,
							10,
							&_packetMem.data,
							512,	/// Must be exact
							residue
							);
	if(result){
		Oscl::Error::Info::log(	"Fatal USB Mass Storage"
								" READ(10) failure: "
								);
		Oscl::TT::Status::Info	info;
		result->query(info);
		return true;
		}
	Oscl::SCSI::CMD::Read10::print(&_packetMem.data);
	return false;
	}


bool	Driver::printStartSync() noexcept{
	Oscl::Error::Info::log("starting START(sync)\n");
	Oscl::SCSI::CMD::StartStopUnit::StartSync	start;
	const Oscl::TT::Status::Result*
	result = _ttApi.command(	0,
								&start,
								6
								);
	if(result){
		Oscl::Error::Info::log(	"Fatal USB Mass Storage"
								" START(sync) failure: "
								);
		Oscl::TT::Status::Info	info;
		result->query(info);
		return true;
		}
	Oscl::Error::Info::log(	"DONE\n");
	return false;
	}

bool	Driver::printGoActive() noexcept{
	Oscl::Error::Info::log("starting START(go active)\n");
	Oscl::SCSI::CMD::StartStopUnit::GoActive	go;
	const Oscl::TT::Status::Result*
	result = _ttApi.command(	0,
								&go,
								6
								);
	if(result){
		Oscl::Error::Info::log(	"Fatal USB Mass Storage"
								" START(go active) failure: "
								);
		Oscl::TT::Status::Info	info;
		result->query(info);
		return true;
		}
	Oscl::Error::Info::log(	"DONE\n");
	return false;
	}

bool	Driver::printLoadMedium() noexcept{
	Oscl::Error::Info::log("starting START(load medium)\n");
	Oscl::SCSI::CMD::StartStopUnit::LoadMedium	go;
	const Oscl::TT::Status::Result*
	result = _ttApi.command(	0,
								&go,
								6
								);
	if(result){
		Oscl::Error::Info::log(	"Fatal USB Mass Storage"
								" START(load medium) failure: "
								);
		Oscl::TT::Status::Info	info;
		result->query(info);
		return true;
		}
	Oscl::Error::Info::log(	"DONE\n");
	return false;
	}

bool	Driver::printTestUnitReady() noexcept{
	Oscl::Error::Info::log("starting TEST-UNIT-READY\n");
	Oscl::SCSI::CMD::TestUnitReady::Standard	test;
	const Oscl::TT::Status::Result*
	result = _ttApi.command(	0,
								&test,
								6
								);
	if(result){
		Oscl::Error::Info::log(	"Fatal USB Mass Storage"
								" TEST-UNIT-READY failure: "
								);
		Oscl::TT::Status::Info	info;
		result->query(info);
		return true;
		}
	Oscl::Error::Info::log(	"DONE\n");
	return false;
	}

bool	Driver::printMBR(uint32_t& part1Sector) noexcept{
	Oscl::Error::Info::log("starting printMBR()\n");
	// Lets read the MBR
	while(printRead10Block(0)){
		printRequestSense();
		unsigned i;
		for(i=0; (i<3) && printTestUnitReady();++i){
			printRequestSense();
			}
		if(i>=3) return true;
		}
	Oscl::MS::FAT::Partition::MBR*
	mbr	= (Oscl::MS::FAT::Partition::MBR*)&_packetMem.data;

	unsigned long
	sizeofMBR	= sizeof(Oscl::MS::FAT::Partition::MBR);

	char	buff[256];
	sprintf(buff,"sizeof(MBR): %lu\n",sizeofMBR);
	Oscl::Error::Info::log(buff);
	sprintf(buff,"MBR.validMarker: %u\n",mbr->validMarker());
	Oscl::Error::Info::log(buff);
	sprintf(	buff,
				"\tMBR._pe[0]._start.chs(): %8.8X\n"
				"\tMBR._pe[0]._end.chs(): %8.8X\n"
				"\tMBR._pe[0].startingLBA(): %8.8X\n"
				"\tMBR._pe[0].lengthInSectors(): %8.8X\n",
				(unsigned)mbr->_pe[0]._start.chs(),
				(unsigned)mbr->_pe[0]._end.chs(),
				(unsigned)mbr->_pe[0].startingLBA(),
				(unsigned)mbr->_pe[0].lengthInSectors()
				);
	part1Sector	= mbr->_pe[0].startingLBA();
	Oscl::Error::Info::log(buff);
	return false;
	}
bool	Driver::printPartitionHeader(uint32_t part1Sector) noexcept{
	Oscl::Error::Info::log("starting printPartitionHeader()\n");
	Oscl::Error::Info::log("reading MBR\n");
	// Lets read the MBR
	while(printRead10Block(part1Sector)){
		printRequestSense();
		unsigned i;
		for(i=0; (i<3) && printTestUnitReady();++i){
			printRequestSense();
			}
		if(i>=3) return true;
		}
	Oscl::FS::FAT::PartHeader*
	header	= (Oscl::FS::FAT::PartHeader*)&_packetMem.data;
	char	buff[512];
	unsigned long
	sizeofPartHeader	= sizeof(Oscl::FS::FAT::PartHeader);

	sprintf(buff,"sizeof(PartHeader): %lu\n",sizeofPartHeader);
	Oscl::Error::Info::log(buff);
	sprintf(buff,"header.isFat12(): %u\n",header->_bpb.isFat12());
	Oscl::Error::Info::log(buff);
	sprintf(buff,"header.isFat16(): %u\n",header->_bpb.isFat16());
	Oscl::Error::Info::log(buff);
	sprintf(buff,"header.isFat32(): %u\n",header->_bpb.isFat32());
	Oscl::Error::Info::log(buff);
	sprintf(	buff,
				"\theader->bs._oemName: "
				"%2.2X %2.2X %2.2X %2.2X %2.2X %2.2X %2.2X %2.2X\n",
				(unsigned)header->_bs._oemName[0],
				(unsigned)header->_bs._oemName[1],
				(unsigned)header->_bs._oemName[2],
				(unsigned)header->_bs._oemName[3],
				(unsigned)header->_bs._oemName[4],
				(unsigned)header->_bs._oemName[5],
				(unsigned)header->_bs._oemName[6],
				(unsigned)header->_bs._oemName[7]
				);
	Oscl::Error::Info::log(buff);
	sprintf(	buff,
				"\theader->_bpb.bytesPerSector(): %u\n"
				"\theader->_bpb._sectorsPerCluster: %u\n",
				(unsigned)header->_bpb.bytesPerSector(),
				(unsigned)header->_bpb._sectorsPerCluster
				);
	Oscl::Error::Info::log(buff);
	sprintf(	buff,
				"\theader->_bpb.reservedSectorCount(): %u\n",
				(unsigned)header->_bpb.reservedSectorCount()
				);
	Oscl::Error::Info::log(buff);
	sprintf(	buff,
				"\theader->_bpb._numberOfFats: %u\n"
				"\theader->_bpb.rootEntryCount(): %u\n"
				"\theader->_bpb.totalSectors(): %u\n",
				(unsigned)header->_bpb._numberOfFats,
				(unsigned)header->_bpb.rootEntryCount(),
				(unsigned)header->_bpb.totalSectors()
				);
	Oscl::Error::Info::log(buff);
	sprintf(	buff,
				"\theader->_bpb._media: %2.2X\n"
				"\theader->_bpb.fatSize(): %u\n",
				(unsigned)header->_bpb._media,
				(unsigned)header->_bpb.fatSize()
				);
	Oscl::Error::Info::log(buff);
	sprintf(	buff,
				"\theader->_bpb.sectorsPerTrack(): %u\n",
				(unsigned)header->_bpb.sectorsPerTrack()
				);
	Oscl::Error::Info::log(buff);
	sprintf(	buff,
				"\theader->_bpb.numberOfHeads(): %u\n"
				"\theader->_bpb.hiddenSectors(): %u\n",
				(unsigned)header->_bpb.numberOfHeads(),
				(unsigned)header->_bpb.hiddenSectors()
				);
	Oscl::Error::Info::log(buff);
	if(header->_bpb.isFat32()){
		sprintf(	buff,
					"\theader->_bpb._fat32.fatSize32(): %u\n"
					"\theader->_bpb._fat32._extFlags.fatsMirrored(): %u\n",
					(unsigned)header->_bpb._fat32.fatSize32(),
					(unsigned)header->_bpb._fat32.fatsMirrored()
					);
		Oscl::Error::Info::log(buff);
		sprintf(	buff,
					"\theader->_bpb._fat32._extFlags.activeFAT(): %u\n"
					"\theader->_bpb._fat32.fsVersion(): %4.4X\n",
					(unsigned)header->_bpb._fat32.activeFAT(),
					(unsigned)header->_bpb._fat32.fsVersion()
					);
		Oscl::Error::Info::log(buff);
		sprintf(	buff,
					"\theader->_bpb._fat32.rootCluster(): %8.8X\n"
					"\theader->_bpb._fat32.fileSystemInfo(): %4.4X\n",
					(unsigned)header->_bpb._fat32.rootCluster(),
					(unsigned)header->_bpb._fat32.fileSystemInfo()
					);
		Oscl::Error::Info::log(buff);
		sprintf(	buff,
					"\theader->_bpb._fat32.bkBootSector(): %4.4X\n",
					(unsigned)header->_bpb._fat32.bkBootSector()
					);
		Oscl::Error::Info::log(buff);
		sprintf(	buff,
					"\theader->_bs._type._fat32._bs64._driveNumber: %u\n"
					"\theader->_bs._type._fat32._bs64._bootSig: %2.2X\n"
					"\theader->_bs._type._fat32._bs64.volumeID(): %8.8X\n",
					(unsigned)header->_bs._type._fat32._bs64._driveNumber,
					(unsigned)header->_bs._type._fat32._bs64._bootSig,
					(unsigned)header->_bs._type._fat32._bs64.volumeID()
					);
		Oscl::Error::Info::log(buff);
		sprintf(	buff,
					"\theader->_bs._type._fat32._bs64._volumeLabel: %.11s\n"
					"\theader->_bs._type._fat32._bs64._fileSystemType: %.8s\n",
					header->_bs._type._fat32._bs64._volumeLabel,
					header->_bs._type._fat32._bs64._fileSystemType
					);
		Oscl::Error::Info::log(buff);
		}
	else {
		sprintf(	buff,
					"\theader->bs._type._fat16.bs._driveNumber: %d\n"
					"\theader->bs._type._fat16.bs._bootSig: %2.2X\n",
					(unsigned)header->_bs._type._fat16._bs36._driveNumber,
					(unsigned)header->_bs._type._fat16._bs36._bootSig
					);
		Oscl::Error::Info::log(buff);
		sprintf(	buff,
					"\theader->bs._type._fat16.bs.volumeID(): %8.8X\n",
					(unsigned)header->_bs._type._fat16._bs36.volumeID()
					);
		Oscl::Error::Info::log(buff);
		}
	Oscl::FS::FAT::FAT16::
	Partition	partition(	header->_bpb.rootDirSectors(),
							header->_bpb.firstDataSector(),
							header->_bpb._sectorsPerCluster,
							header->_bpb.reservedSectorCount(),
							header->_bpb.bytesPerSector(),
							header->_bpb.fatSize(),
							header->_bpb.firstRootDirSecNum(),
							header->_bpb.totalSectors()
							);
	sprintf(	buff,
				"\tpartition.firstSectorOfCluster(0): %8.8X\n"
				"\tpartition.firstSectorOfCluster(2): %8.8X\n",
				(unsigned)partition.firstSectorOfCluster(0),
				(unsigned)partition.firstSectorOfCluster(2)
				);
	Oscl::Error::Info::log(buff);
	Oscl::Error::Info::log("\n");
	sprintf(	buff,
				"\tpartition.firstRootDirSecNum(): 0x%8.8X\n",
				(unsigned)partition.firstRootDirSecNum()
				);
	Oscl::Error::Info::log(buff);
	sprintf(	buff,
				"\tpart1Sector: 0x%8.8X\n",
				(unsigned)part1Sector
				);
	Oscl::Error::Info::log(buff);
	Oscl::Error::Info::log("reading FAT16\n");
	// Lets read the MBR
	while(printRead10Block(part1Sector+partition.firstRootDirSecNum())){
		printRequestSense();
		unsigned i;
		for(i=0; (i<3) && printTestUnitReady();++i){
			printRequestSense();
			}
		if(i>=3) return true;
		}
	return false;
	}

Oscl::Block::Read::Api<512>&   Driver::readApi() noexcept{
	return _readSync;
	}

Oscl::Block::Write::Api<512>&  Driver::writeApi() noexcept{
	return _writeSync;
	}

Oscl::Block::Read::
ITC::Req::Api< 512 >::SAP&		Driver::getReadSAP() noexcept{
	return _readSync.getSAP();
	}

Oscl::Block::Write::
ITC::Req::Api< 512 >::SAP&		Driver::getWriteSAP() noexcept{
	return _writeSync.getSAP();
	}

void    Driver::request(	Oscl::Block::Read::
							ITC::Req::Api<512>::CancelReq&	msg
							) noexcept{
	Oscl::Mt::Itc::SrvMsg*	next;
	if((next=_pendingReqs.remove(&msg._payload._msgToCancel))){
		next->returnToSender();
		msg.returnToSender();
		return;
		}
	_incomming.readCancelReq	= &msg;
	_state.readCancelReq();
	}

void    Driver::request(	Oscl::Block::Read::
							ITC::Req::Api<512>::ReadReq&	msg
							) noexcept{
	_incomming.readReq	= &msg;
	_state.readReq();
	}

void    Driver::request(	Oscl::Block::Write::
							ITC::Req::Api<512>::CancelReq&	msg
							) noexcept{
	Oscl::Mt::Itc::SrvMsg*	next;
	if((next=_pendingReqs.remove(&msg._payload._msgToCancel))){
		next->returnToSender();
		msg.returnToSender();
		return;
		}
	_incomming.writeCancelReq	= &msg;
	_state.writeCancelReq();
	}

void    Driver::request(	Oscl::Block::Write::
							ITC::Req::Api<512>::WriteReq&	msg
							) noexcept{
	_incomming.writeReq	= &msg;
	_state.writeReq();
	}

void    Driver::request(	Oscl::Block::Write::
							ITC::Req::Api<512>::FlushReq&	msg
							) noexcept{
	_incomming.flushReq	= &msg;
	_state.flushReq();
	}

/////////////////////////
void	Driver::returnOpenReq() noexcept{
	_openReq->returnToSender();
	_openReq	= 0;
	}

void	Driver::returnCloseReq() noexcept{
	_closeReq->returnToSender();
	_closeReq	= 0;
	}

void	Driver::executeDeferredReq() noexcept{
	Oscl::Mt::Itc::SrvMsg*	next;
	if((next=_pendingReqs.get())){
		next->process();
		}
	}

void	Driver::deferReadReq() noexcept{
	_pendingReqs.put(_incomming.readReq);
	}

void	Driver::deferWriteReq() noexcept{
	_pendingReqs.put(_incomming.writeReq);
	}

void	Driver::deferFlushReq() noexcept{
	_pendingReqs.put(_incomming.flushReq);
	}

void	Driver::startRead() noexcept{
	_currentReq.readReq	= _incomming.readReq;

	Oscl::Block::Read::ITC::Req::Api<512>::ReadPayload&
	srcPayload	= _currentReq.readReq->_payload;

	Oscl::SCSI::CMD::Read10::Standard*
	read10	=	new(&_cmdMem._read10) Oscl::SCSI::CMD::Read10::
					Standard(	srcPayload._block,
								srcPayload._nBlocks
								);
	Oscl::TT::ITC::Req::Api::ReadPayload*
	payload	= new(&_respMem.tt._read.payload) Oscl::TT::ITC::Req::Api::
					ReadPayload(	0,	// assume LUN zero
									read10,	// cb
									10,	// cb length
									srcPayload._blocks,
									srcPayload._nBlocks*512
									);
	Oscl::TT::ITC::Resp::Api::ReadResp*
	resp	= new(&_respMem.tt._read.resp) Oscl::TT::ITC::Resp::Api::
					ReadResp(	_ttSAP.getReqApi(),
								*this,
								_myPapi,
								*payload
								);
	_ttSAP.post(resp->getSrvMsg());
	}

void	Driver::startWrite() noexcept{
	_currentReq.writeReq	= _incomming.writeReq;

	Oscl::Block::Write::ITC::Req::Api<512>::WritePayload&
	srcPayload	= _currentReq.writeReq->_payload;

	Oscl::SCSI::CMD::Write10::Standard*
	write10	=	new(&_cmdMem._write10) Oscl::SCSI::CMD::Write10::
					Standard(	srcPayload._block,
								srcPayload._nBlocks
								);
	Oscl::TT::ITC::Req::Api::WritePayload*
	payload	= new(&_respMem.tt._write.payload) Oscl::TT::ITC::Req::Api::
					WritePayload(	0,	// assume LUN zero
									write10,	// cb
									10,	// cb length
									srcPayload._blocks,
									srcPayload._nBlocks*512
									);
	Oscl::TT::ITC::Resp::Api::WriteResp*
	resp	= new(&_respMem.tt._write.resp) Oscl::TT::ITC::Resp::Api::
					WriteResp(	_ttSAP.getReqApi(),
								*this,
								_myPapi,
								*payload
								);
	_ttSAP.post(resp->getSrvMsg());
	}

void	Driver::startFlush() noexcept{
	_currentReq.flushReq	= _incomming.flushReq;

	Oscl::Block::Write::ITC::Req::Api<512>::FlushPayload&
	srcPayload	= _currentReq.flushReq->_payload;

	Oscl::SCSI::CMD::SynchronizeCache::Standard*
	cacheSync	=	new(&_cmdMem._syncCache) Oscl::SCSI::CMD::SynchronizeCache::
					Standard(	srcPayload._block,
								srcPayload._nBlocks
								);
	Oscl::TT::ITC::Req::Api::CommandPayload*
	payload	= new(&_respMem.tt._command.payload) Oscl::TT::ITC::Req::Api::
					CommandPayload(	0,	// assume LUN zero
									cacheSync,	// cb
									10	// cb length
									);
	Oscl::TT::ITC::Resp::Api::CommandResp*
	resp	= new(&_respMem.tt._command.resp) Oscl::TT::ITC::Resp::Api::
					CommandResp(	_ttSAP.getReqApi(),
									*this,
									_myPapi,
									*payload
									);
	_ttSAP.post(resp->getSrvMsg());
	}

void	Driver::startRequestSense() noexcept{
//	_currentReq.readReq	= _incomming.readReq;

	Oscl::SCSI::CMD::RequestSense::Standard*
	reqSense	=	new(&_cmdMem._reqSense) Oscl::SCSI::CMD::RequestSense::
					Standard(Oscl::SCSI::CMD::RequestSense::Response::size);
	Oscl::TT::ITC::Req::Api::ReadPayload*
	payload	= new(&_respMem.tt._read.payload) Oscl::TT::ITC::Req::Api::
					ReadPayload(	0,	// assume LUN zero
									reqSense,	// cb
//									sizeof(reqSense),	// cb length
									6,	// cb length
									&_packetMem.data,
									Oscl::SCSI::CMD::RequestSense::Response::size
									);
	Oscl::TT::ITC::Resp::Api::ReadResp*
	resp	= new(&_respMem.tt._read.resp) Oscl::TT::ITC::Resp::Api::
					ReadResp(	_ttSAP.getReqApi(),
								*this,
								_myPapi,
								*payload
								);
	_ttSAP.post(resp->getSrvMsg());
	}

void	Driver::returnReadReq() noexcept{
	_currentReq.readReq->returnToSender();
	_currentReq.readReq	= 0;
	}

void	Driver::returnWriteReq() noexcept{
	_currentReq.writeReq->returnToSender();
	_currentReq.writeReq	= 0;
	}

void	Driver::returnFlushReq() noexcept{
	_currentReq.flushReq->returnToSender();
	_currentReq.flushReq	= 0;
	}

void	Driver::returnIncommingReadCancelReq() noexcept{
	_incomming.readCancelReq->returnToSender();
	_incomming.readCancelReq	= 0;
	}

void	Driver::returnIncommingWriteCancelReq() noexcept{
	_incomming.writeCancelReq->returnToSender();
	_incomming.writeCancelReq	= 0;
	}

void	Driver::returnReadCancelReq() noexcept{
	_cancel.readCancelReq->returnToSender();
	_cancel.readCancelReq	= 0;
	}

void	Driver::returnWriteCancelReq() noexcept{
	_cancel.writeCancelReq->returnToSender();
	_cancel.writeCancelReq	= 0;
	}

void	Driver::cancelRead() noexcept{
	Oscl::TT::
	ITC::Req::Api::CancelPayload*
	payload	= new(&_cancelMem.tt.payload)
				Oscl::TT::
				ITC::Req::Api::
				CancelPayload(_currentResp.readResp->getSrvMsg());

	Oscl::TT::
	ITC::Resp::Api::CancelResp*
	resp	= new(&_cancelMem.tt.resp)
				Oscl::TT::
				ITC::Resp::Api::
				CancelResp(	_ttSAP.getReqApi(),
							*this,
							_myPapi,
							*payload
							);
	_currentResp.cancelResp	= resp;
	_cancel.readCancelReq	= _incomming.readCancelReq;
	_ttSAP.post(resp->getSrvMsg());
	}

void	Driver::cancelWrite() noexcept{
	Oscl::TT::
	ITC::Req::Api::CancelPayload*
	payload	= new(&_cancelMem.tt.payload)
				Oscl::TT::
				ITC::Req::Api::
				CancelPayload(_currentResp.writeResp->getSrvMsg());

	Oscl::TT::
	ITC::Resp::Api::CancelResp*
	resp	= new(&_cancelMem.tt.resp)
				Oscl::TT::
				ITC::Resp::Api::
				CancelResp(	_ttSAP.getReqApi(),
							*this,
							_myPapi,
							*payload
							);
	_currentResp.cancelResp	= resp;
	_cancel.writeCancelReq	= _incomming.writeCancelReq;
	_ttSAP.post(resp->getSrvMsg());
	}

void	Driver::cancelCommand() noexcept{
	Oscl::TT::
	ITC::Req::Api::CancelPayload*
	payload	= new(&_cancelMem.tt.payload)
				Oscl::TT::
				ITC::Req::Api::
				CancelPayload(_currentResp.commandResp->getSrvMsg());

	Oscl::TT::
	ITC::Resp::Api::CancelResp*
	resp	= new(&_cancelMem.tt.resp)
				Oscl::TT::
				ITC::Resp::Api::
				CancelResp(	_ttSAP.getReqApi(),
							*this,
							_myPapi,
							*payload
							);
	_currentResp.cancelResp	= resp;
	_cancel.writeCancelReq	= _incomming.writeCancelReq;
	_ttSAP.post(resp->getSrvMsg());
	}

/** */
class CheckConditionTest : public Oscl::TT::Status::Handler {
	public:
		/** */
		bool	_checkCondition;
	private:
		/** */
		void	failed() noexcept{_checkCondition=true;}
		/** */
		void	phaseError() noexcept{_checkCondition=false;}
		/** */
		void	transportError() noexcept{_checkCondition=false;}
	};

/** */
class ReadResultTranslate : public Oscl::TT::Status::Handler {
	public:
		/** */
		const Oscl::Block::Read::Status::Result*	_result;
	private:
		/** */
		void	failed() noexcept{_result=&Oscl::Block::Read::Status::getTransportError();}
		/** */
		void	phaseError() noexcept{_result=&Oscl::Block::Read::Status::getTransportError();}
		/** */
		void	transportError() noexcept{_result=&Oscl::Block::Read::Status::getTransportError();}
	};

/** */
class ReqSenseResultTranslate : public Oscl::TT::Status::Handler {
	private:
		/** */
		void	failed() noexcept{Oscl::Error::Info::log("CHECK-CONDITION\n");}
		/** */
		void	phaseError() noexcept{Oscl::Error::Info::log("phaseError\n");}
		/** */
		void	transportError() noexcept{Oscl::Error::Info::log("transportError\n");}
	};

void	Driver::finishRead() noexcept{
	if(_incomming.readResp->_payload._result){
		ReadResultTranslate	xlate;
		_incomming.readResp->_payload._result->query(xlate);
		_currentReq.readReq->_payload._result	= xlate._result;
		return;
		}
	_currentReq.readReq->_payload._result	= 0;
	}

class WriteResultTranslate : public Oscl::TT::Status::Handler {
	public:
		/** */
		const Oscl::Block::Write::Status::Result*	_result;
	private:
		/** */
		void	failed() noexcept{_result=&Oscl::Block::Write::Status::getTransportError();}
		/** */
		void	phaseError() noexcept{_result=&Oscl::Block::Write::Status::getTransportError();}
		/** */
		void	transportError() noexcept{_result=&Oscl::Block::Write::Status::getTransportError();}
	};

void	Driver::finishWrite() noexcept{
	if(_incomming.writeResp->_payload._result){
		WriteResultTranslate	xlate;
		_incomming.writeResp->_payload._result->query(xlate);
		_currentReq.writeReq->_payload._result	= xlate._result;
		return;
		}
	_currentReq.writeReq->_payload._result	= 0;
	}

bool	Driver::isCommandCheckCondition() noexcept{
	if(_incomming.commandResp->_payload._result){
		CheckConditionTest	xlate;
		_incomming.commandResp->_payload._result->query(xlate);
		return xlate._checkCondition;
		}
	return false;
	}

void	Driver::finishCommand() noexcept{
	if(_incomming.commandResp->_payload._result){
		WriteResultTranslate	xlate;
		_incomming.commandResp->_payload._result->query(xlate);
		_currentReq.flushReq->_payload._result	= xlate._result;
		return;
		}
	_currentReq.flushReq->_payload._result	= 0;
	}

void	Driver::returnIncommingFlushReq() noexcept{
	_incomming.flushReq->_payload._result	= 0;
	_incomming.flushReq->returnToSender();
	}

void	Driver::finishFlushReqSense() noexcept{
	if(_incomming.readResp->_payload._result){
		ReqSenseResultTranslate		xlate;
		// If the inquiry fails, then say so.
		Oscl::Error::Info::log(	"Oscl::TT::LUN::Driver:"
								" Flush REQ-SENSE failed:"
								);
		_incomming.readResp->_payload._result->query(xlate);
		return;
		}
	Oscl::SCSI::CMD::RequestSense::Response*
	resp	= (Oscl::SCSI::CMD::RequestSense::Response*)
				_incomming.readResp->_payload._buffer;
	// The SCSI REQ-SENSE completed successfully.
	// Lets display the results and then disable
	// future Flush requests if it is not supported
	// by the device.
	if(		resp->valid()
		&&	(resp->responseCode() == 0x070)					// current errors
		&&	(resp->senseKey() == 0x05)						// ILLEGAL REQUEST
		&&	(resp->_additionalSenseCode == 0x20)			// INVALID COMMAND
		&&	(resp->_additionalSeseCodeQualifier == 0x00)	// OPERATION CODE
		){
		// Flush command is not supported by
		// the device. Disable further use of
		// the command.
		_flushSupported	= false;
		return;
		}
	Oscl::Error::Info::log(	"Oscl::TT::LUN::Driver:"
							"Unexpected Flush REQ-SENSE result:\n"
							);
	char	buffer[64];
	// is valid
	sprintf(	buffer,
				"%s\n",
				(resp->valid())?"is valid":"not valid"
				);
	Oscl::Error::Info::log(buffer);
	// 0x70
	sprintf(	buffer,
				"responseCode:0x%2.2X\n",
				(unsigned)resp->responseCode()
				);
	Oscl::Error::Info::log(buffer);
	// 0x05	: ILLEGAL REQUEST
	sprintf(	buffer,
				"senseKey:0x%2.2X\n",
				(unsigned)resp->senseKey()
				);
	Oscl::Error::Info::log(buffer);
	// 0x20,0x00	: INVALID COMMAND OPERATION CODE
	sprintf(	buffer,
				"additionalSenseCode:0x%2.2X\n",
				(unsigned)resp->_additionalSenseCode
				);
	Oscl::Error::Info::log(buffer);
	sprintf(	buffer,
				"additionalSenseCodeQualifier:0x%2.2X\n",
				(unsigned)resp->_additionalSeseCodeQualifier
				);
	Oscl::Error::Info::log(buffer);
	}

bool	Driver::deferQueueEmpty() noexcept{
	return !_pendingReqs.first();
	}

bool	Driver::isIncommingCancelForCurrentRead() noexcept{
	Oscl::Mt::Itc::SrvMsg*	current = _currentReq.readReq;
	return &_incomming.readCancelReq->_payload._msgToCancel == current;
	}

bool	Driver::isIncommingCancelForCurrentWrite() noexcept{
	Oscl::Mt::Itc::SrvMsg*	current = _currentReq.writeReq;
	return &_incomming.writeCancelReq->_payload._msgToCancel == current;
	}

bool	Driver::isIncommingCancelForCurrentFlush() noexcept{
	Oscl::Mt::Itc::SrvMsg*	current = _currentReq.flushReq;
	return &_incomming.writeCancelReq->_payload._msgToCancel == current;
	}

bool	Driver::isFlushSupportedByDevice() noexcept{
	return _flushSupported;
	}

