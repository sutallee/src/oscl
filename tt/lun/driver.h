/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_tt_lun_driverh_
#define _oscl_tt_lun_driverh_
#include "oscl/memory/block.h"
#include "oscl/queue/queue.h"
#include "oscl/mt/itc/srv/close.h"
#include "oscl/tt/itc/respmem.h"
#include "oscl/tt/api.h"
#include "fsm.h"
#include "oscl/driver/usb/pipe/handler.h"
#include "oscl/block/rw/itc/api.h"
#include "oscl/block/rw/api.h"
#include "oscl/block/read/itc/sync.h"
#include "oscl/block/write/itc/sync.h"
#include "oscl/scsi/cmd/read10.h"
#include "oscl/scsi/cmd/write10.h"
#include "oscl/scsi/cmd/synccache.h"
#include "oscl/scsi/cmd/reqsense.h"

/** */
namespace Oscl {
/** */
namespace TT {
/** */
namespace LUN {

/** This implementation of the Oscl::TT::LUN::ITC::Req::Api
	provides services for a single Transparent Transport LUN.
	It operates asynchronously so that all or many instances
	can share a single thread.
 */
class Driver :	public Oscl::Mt::Itc::Srv::CloseSync,
				private Oscl::Mt::Itc::Srv::Close::Req::Api,
				private Oscl::TT::ITC::Resp::Api,
				private Oscl::Block::Read::ITC::Req::Api<512>,
				private Oscl::Block::Write::ITC::Req::Api<512>,
				public Oscl::Block::RW::Api<512>,
				public Oscl::Block::RW::ITC::Api<512>,
				private StateVar::ContextApi
				{
	public:
		/** */
		struct PacketMem {
			/** */
			Oscl::Memory::AlignedBlock<16>		cmd;
			union {
				/** */
				Oscl::Memory::AlignedBlock<1024>		mem;
				} data;
			};
	private:
		/** */
		union Requests {
			/** */
			Oscl::Mt::Itc::SrvMsg*						generic;

			Oscl::TT::ITC::Resp::Api::ResetResp*		resetResp;
			/** */
			Oscl::TT::ITC::Resp::Api::GetMaxLunResp*	getMaxLunResp;
			/** */
			Oscl::TT::ITC::Resp::Api::CancelResp*		cancelResp;
			/** */
			Oscl::TT::ITC::Resp::Api::ReadResp*			readResp;
			/** */
			Oscl::TT::ITC::Resp::Api::WriteResp*		writeResp;
			/** */
			Oscl::TT::ITC::Resp::Api::CommandResp*		commandResp;
			/** */
			Oscl::Block::Read::
			ITC::Req::Api<512>::ReadReq*				readReq;
			/** */
			Oscl::Block::Read::
			ITC::Req::Api<512>::CancelReq*				readCancelReq;
			/** */
			Oscl::Block::Write::
			ITC::Req::Api<512>::WriteReq*				writeReq;
			/** */
			Oscl::Block::Write::
			ITC::Req::Api<512>::FlushReq*				flushReq;
			/** */
			Oscl::Block::Write::
			ITC::Req::Api<512>::CancelReq*				writeCancelReq;
			};
		/** */
		union CancelMem {
			/** */
			Oscl::TT::ITC::
			Resp::CancelMem					tt;
			};

	private:
		/** */
		union {
			/** */
			Oscl::TT::ITC::Resp::Mem		tt;
			} _respMem;

		/** */
		union {
			/** */
			Oscl::Memory::
			AlignedBlock<	sizeof(Oscl::SCSI::CMD::Read10::Standard)
							>				_read10;
			/** */
			Oscl::Memory::
			AlignedBlock<	sizeof(Oscl::SCSI::CMD::Write10::Standard)
							>				_write10;
			/** */
			Oscl::Memory::
			AlignedBlock<	sizeof(Oscl::SCSI::CMD::SynchronizeCache::Standard)
							>				_syncCache;
			/** */
			Oscl::Memory::
			AlignedBlock<	sizeof(Oscl::SCSI::CMD::RequestSense::Standard)
							>				_reqSense;
			} _cmdMem;

		/** Requests that are queued/serialized
			for execution while the pending bulk
			transaction is executing.
		 */
		Oscl::Queue<Oscl::Mt::Itc::SrvMsg>	_pendingReqs;

		/** Memory used to cancel outstanding requests.
		 */
		CancelMem							_cancelMem;

		/** */
		PacketMem&							_packetMem;

		/** */
		Oscl::Mt::Itc::PostMsgApi&			_myPapi;

		/** */
		Oscl::TT::ITC::Req::Api::SAP&		_ttSAP;

		/** */
		Oscl::TT::Api&						_ttApi;

		/** */
		Oscl::Block::Read::ITC::Sync<512>	_readSync;

		/** */
		Oscl::Block::Write::ITC::Sync<512>	_writeSync;

		/** */
		StateVar							_state;

		/** */
		Oscl::Mt::Itc::Srv::Open::
		Req::Api::OpenReq*					_openReq;

		/** */
		Oscl::Mt::Itc::Srv::Close::
		Req::Api::CloseReq*					_closeReq;

		/** The "just received" request */
		Requests							_incomming;

		/** The currently executing request */
		Requests							_currentReq;

		/** The currently executing response */
		Requests							_currentResp;

		/** The currently executing cancel */
		Requests							_cancel;

		/** */
		union {
			/** */
			Oscl::Mt::Itc::SrvMsg*			generic;
			} _currentPipe;

		/** */
		bool								_flushSupported;

	public:
		/** */
		Driver(	Oscl::Mt::Itc::PostMsgApi&		myPapi,
				Oscl::TT::ITC::Req::Api::SAP&	ttSAP,
				Oscl::TT::Api&					ttApi,
				PacketMem&						packetMem
				) noexcept;

	private:	// Oscl::Mt::Itc::Srv::Close::Req::Api
		/** */
		void	request(	Oscl::Mt::Itc::Srv::
							Open::Req::Api::OpenReq&	msg
							) noexcept;
		/** */
		void	request(	Oscl::Mt::Itc::Srv::
							Close::Req::Api::CloseReq&	msg
							) noexcept;

	private:	// Oscl::TT::ITC::Resp::Api
		/** */
		void	response(ResetResp& msg) noexcept;
		/** */
		void	response(GetMaxLunResp& msg) noexcept;
		/** */
		void	response(CancelResp& msg) noexcept;
		/** */
		void	response(ReadResp& msg) noexcept;
		/** */
		void	response(WriteResp& msg) noexcept;
		/** */
		void	response(CommandResp& msg) noexcept;
	private:	// Support
		/** */
		void	jungsoftSeq() noexcept;
		/** */
		void	genericSeq() noexcept;
		/** */
		bool	printInquiry() noexcept;
		/** */
		bool	printRequestSense() noexcept;
		/** */
		bool	printReadCapacity() noexcept;
		/** */
		bool	printModeSense10(uint8_t pageCode) noexcept;
		/** */
		bool	printModeSense6(uint8_t pageCode) noexcept;
		/** */
		bool	printRead6Block(unsigned long block) noexcept;
		/** */
		bool	printRead10Block(unsigned long block) noexcept;
		/** */
		bool	printStartSync() noexcept;
		/** */
		bool	printGoActive() noexcept;
		/** */
		bool	printLoadMedium() noexcept;
		/** */
		bool	printTestUnitReady() noexcept;
		/** */
		bool	printMBR(uint32_t& part1Sector) noexcept;
		/** */
		bool	printPartitionHeader(uint32_t part1Sector) noexcept;

	private:	// Oscl::Block::RW::Api
        /** */
		Oscl::Block::Read::Api<512>&   readApi() noexcept;
        /** */
		Oscl::Block::Write::Api<512>&	writeApi() noexcept;

	private:	// Oscl::Block::RW::ITC::Api
		/** */
		Oscl::Block::Read::
		ITC::Req::Api< 512 >::SAP&		getReadSAP() noexcept;

		/** */
		Oscl::Block::Write::
		ITC::Req::Api< 512 >::SAP&		getWriteSAP() noexcept;

	private:
		/** */
		void    request(	Oscl::Block::Read::
							ITC::Req::Api<512>::CancelReq&	msg
							) noexcept;
		/** */
		void    request(	Oscl::Block::Read::
							ITC::Req::Api<512>::ReadReq&	msg
							) noexcept;

	private:
		/** */
		void    request(	Oscl::Block::Write::
							ITC::Req::Api<512>::CancelReq&	msg
							) noexcept;
		/** */
		void    request(	Oscl::Block::Write::
							ITC::Req::Api<512>::WriteReq&	msg
							) noexcept;
		/** */
		void    request(	Oscl::Block::Write::
							ITC::Req::Api<512>::FlushReq&	msg
							) noexcept;

	private:	// StateVar::ContextApi
		/** */
		void	returnOpenReq() noexcept;
		/** */
		void	returnCloseReq() noexcept;
		/** */
		void	executeDeferredReq() noexcept;
		/** */
		void	deferReadReq() noexcept;
		/** */
		void	deferWriteReq() noexcept;
		/** */
		void	deferFlushReq() noexcept;
		/** */
		void	startRead() noexcept;
		/** */
		void	startWrite() noexcept;
		/** */
		void	startFlush() noexcept;
		/** */
		void	startRequestSense() noexcept;
		/** */
		void	returnReadReq() noexcept;
		/** */
		void	returnWriteReq() noexcept;
		/** */
		void	returnFlushReq() noexcept;
		/** */
		void	returnIncommingFlushReq() noexcept;
		/** */
		void	returnIncommingReadCancelReq() noexcept;
		/** */
		void	returnIncommingWriteCancelReq() noexcept;
		/** */
		void	returnReadCancelReq() noexcept;
		/** */
		void	returnWriteCancelReq() noexcept;
		/** */
		void	cancelRead() noexcept;
		/** */
		void	cancelWrite() noexcept;
		/** */
		void	cancelCommand() noexcept;
		/** */
		void	finishRead() noexcept;
		/** */
		void	finishWrite() noexcept;
		/** */
		void	finishCommand() noexcept;
		/** */
		void	finishFlushReqSense() noexcept;
		/** */
		bool	isCommandCheckCondition() noexcept;
		/** */
		bool	deferQueueEmpty() noexcept;
		/** */
		bool	isIncommingCancelForCurrentRead() noexcept;
		/** */
		bool	isIncommingCancelForCurrentWrite() noexcept;
		/** */
		bool	isIncommingCancelForCurrentFlush() noexcept;
		/** */
		bool	isFlushSupportedByDevice() noexcept;
	};

}
}
}


#endif
