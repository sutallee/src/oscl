/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_tt_apih_
#define _oscl_tt_apih_

/** */
namespace Oscl {
/** */
namespace TT {

namespace Status {
class Result;
}

/** */
class Api {
	public:
		/** */
		virtual ~Api(){}
		/** Resets the device.
			Returns true if the operation fails.
		 */
		virtual bool	reset() noexcept=0;
		/** Returns the value of the last LUN. LUNs are numbered
			sequentially from zero to the value returned in the
			maxLun output.
			Returns true if the operation fails.
		 */
		virtual bool	getMaxLUN(unsigned char& maxLun) noexcept=0;
		/** Sends the command block "cb" of length "cbLen" to
			the specified "lun" and then attempts to read
			the number of bytes specified by "bufferLen" into
			"buffer" from the "lun".
			<pre>
			PARAMETERS:
				lun: Logical Unit Number
					INPUT
					range 0 - 15
					Zero is the "Device" LUN.
				cb: Command Block
					INPUT
				cbLen: Command Block Length
					INPUT, range 1 - 16
					Number of bytes in the command block.
				buffer:
					OUTPUT
					Alignment: Oscl::Memory::AlignedBlock<1>
					Important! The memory pointed to by buffer MUST be in
					a DMA addressable area of memory AND it MUST be aligned
					to a cache boundary. Any other variables that are within
					the same cache-line as this buffer *may* be corrupted
					as the processor caches are flushed during I/O.
					Memory where data read by command will be placed.
				bufferLen: Buffer Length
					INPUT
					Maximum length of buffer in bytes.
				residue:
					OUTPUT
					The difference between the amount of data expected
					and the actual amount copied to buffer.
			RETURN:
				Result is NULL if the operation completed
				without error. Otherwise, a pointer to result
				code is returned. The reference can be used
				to determine the reason for the failure.
			</pre>
		 */
		virtual const Status::Result*	read(	unsigned char	lun,
												const void*		cb,
												unsigned		cbLen,
												void*			buffer,
												unsigned long	bufferLen,
												unsigned long&	residue
												) noexcept=0;

		/** Sends the command block "cb" of length "cbLen" to
			the specified "lun" and then attempts to write
			the number of bytes specified by "bufferLen" from
			buffer to the "lun".
			PARAMETERS:
				lun: Logical Unit Number
					INPUT
					range 0 - 15
					Zero is the "Device" LUN.
				cb: Command Block
					INPUT
				cbLen: Command Block Length
					INPUT, range 1 - 16
					Number of bytes in the command block.
				buffer:
					INPUT
					Memory containing data to be sent to the device.
					Alignment: Oscl::Memory::AlignedBlock<1>
					Important! The memory pointed to by buffer MUST be in
					a DMA addressable area of memory AND it MUST be aligned
					to a cache boundary. Any other variables that are within
					the same cache-line as this buffer *may* be corrupted
					as the processor caches are invalidated.
				bufferLen: Buffer Length
					INPUT
					Maximum number of valid bytes in buffer.
				residue:
					OUTPUT
					The difference between the amount of data expected
					to be written and the actual number written to the device.
			RETURN:
				Result is NULL if the operation completed
				without error. Otherwise, a pointer to result
				code is returned. The reference can be used
				to determine the reason for the failure.
		 */
		virtual const Status::Result*	write(	unsigned char	lun,
												const void*		cb,
												unsigned		cbLen,
												const void*		buffer,
												unsigned long	bufferLen,
												unsigned long&	residue
												) noexcept=0;
		/** Sends the command block "cb" of length "cbLen" to
			the specified "lun".
			PARAMETERS:
				lun: Logical Unit Number
					INPUT
					range 0 - 15
					Zero is the "Device" LUN.
				cb: Command Block
					INPUT
				cbLen: Command Block Length
					INPUT, range 1 - 16
					Number of bytes in the command block.
			RETURN:
				Result is NULL if the operation completed
				without error. Otherwise, a pointer to result
				code is returned. The reference can be used
				to determine the reason for the failure.
		 */
		virtual const Status::Result*	command(	unsigned char	lun,
													const void*		cb,
													unsigned		cbLen
													) noexcept=0;
	};

}
}

#endif
