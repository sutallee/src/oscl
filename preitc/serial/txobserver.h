/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_preitc_serial_txobserverh_
#define _oscl_preitc_serial_txobserverh_
#include <stdint.h>

/** */
namespace Oscl {

/** This observer class is used by serial transmit stream clients
	who need to be informed about changes in state of the serial
	transmitter.
 */
class SerialTxObserver {
	public:
		/** This notification function is called by the serial
			transmitter when the state of the serializer becomes
			ready. A client implementation may use this indication
			to signal a semaphore, or alternately to transmit
			pending octets to the serializer. This function is
			always called by a thread, and NOT by an interrupt
			service routine.
		 */
		virtual void	transmitterReady()=0;
	};

};

#endif
