/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_preitc_serial_transmitterh_
#define _oscl_preitc_serial_transmitterh_
#include <stdint.h>

/** */
namespace Oscl {

class SerialTxObserver;

/** This class describes a generic serial transmit interface.
 */
class SerialTransmitter {
	public:
		/** This routine attaches the SerialTxObserver observer to the
			transmit stream, replacing any previous observer. The observer
			is used to notify the client of changes in the serial transmitter
			state. The previous observer, if any, is returned.
		 */
		virtual SerialTxObserver*	attach(SerialTxObserver* ready)=0;

		/** This function is called by the client in an attempt to
			transmit the specified number of octets from the octet
			array to the transmitter. The function writes as many
			octets as possible to the transmitter and returns the
			actual number of octets written. This function does
			not block.
		 */
		virtual unsigned		write(const uint8_t* data,unsigned len)=0;
	};

};

#endif
