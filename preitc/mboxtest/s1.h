/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_preitc_mbox_s1h_
#define _oscl_preitc_mbox_s1h_
#include "oscl/mt/itc/mbox/mbox.h"
#include "oscl/mt/runnable.h"
#include "oscl/krux/sema/bsema.h"
#include "oscl/mt/itc/mbox/srvreq.h"

/** */
namespace Oscl {

/** Server 1, Message 1 payload.
 */
class Msg1Payload {
	public:
		/** dummy variable.
		 */
		unsigned	dummy;
	};

/** Server 1 Message 2 payload.
 */
class Msg2Payload {
	public:
		/** another dummy variable.
		 */
		unsigned	dummy;
	};

class S1Srv;


/** Server 1 request 1 message.
 */
typedef Oscl::Mt::Itc::SrvRequest<S1Srv,Msg1Payload>	S1Msg1;

/** Server 1 request 2 message.
 */
typedef Oscl::Mt::Itc::SrvRequest<S1Srv,Msg2Payload>	S1Msg2;

/** Server 1 context interface.
 */
class S1Srv {
	public:
		virtual Oscl::Mt::Itc::Mailbox&	getMailbox()=0;
	public:
		/** Server 1 request 1 interface.
		 */
		virtual void	request(S1Msg1& msg)=0;

		/** Server 1 request 2 interface.
		 */
		virtual void	request(S1Msg2& msg)=0;
	};

/** Server 1 thread runnable and context.
 */
class S1 : public Oscl::Mt::Runnable, public S1Srv {
	private:
		Krux::Sema::Binary		_input;
		Oscl::Mt::Itc::Mailbox	_mbox;
	public:
		S1();
		/** The runnable interface.
		 */
		void run() noexcept;

	public:
		/** Mailbox accessor for?
		 */
		Oscl::Mt::Itc::Mailbox&	getMailbox();

	public:
		/** Request 1 message implementation.
		 */
		void	request(S1Msg1& msg);

		/** Request 2 message implementation.
		 */
		void	request(S1Msg2& msg);
	};

};

#endif
