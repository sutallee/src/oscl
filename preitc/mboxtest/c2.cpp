/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "c2.h"

using namespace Oscl;

unsigned	aDummy;


C2::C2(S1Srv& srv):
	_srv(srv),
	_timer(_sema,2){
	}

C2::~C2(){
	}

static inline C2Msg1* send(S1Srv& srv,C2Cli& c2cli,Mt::Itc::Mailbox& mbox,Msg1Payload& payload){
	return new C2Msg1(srv,c2cli,mbox,payload);
	}

void C2::run() noexcept {
	Msg1Payload			payload;
	while(true){
		Mt::Itc::Msg*				msg;
		_timer.start();
		_timer.wait();
		C2Msg1*	cmsg	= send(_srv,*this,_mbox,payload);
		if(cmsg){
			_srv.getMailbox().post(cmsg->getSrvMsg());
			}
		msg	= _mbox.waitNext();
		if(msg){
			msg->process();
			}
		}
	}

void C2::response(Mt::Itc::CliResponse<S1Srv,C2Cli,Msg1Payload>& msg){
	aDummy	= msg.getPayload().dummy;
	delete &msg;
	}

void C2::closed(Mt::Itc::CliResponse<S1Srv,C2Cli,Msg1Payload>& msg){
	delete &msg;
	}
