/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "c1.h"
#include "oscl/mt/itc/mbox/syncrh.h"

using namespace Oscl;

C1::C1(S1Srv& srv):
	_srv(srv),
	_timer(_sema,2){
	}

C1::~C1(){
	}

void C1::run() noexcept{
	while(true){
		Msg1Payload					m1Payload;
		Msg2Payload					m2Payload;
		Mt::Itc::SyncReturnHandler	rh;
		Mt::Itc::Msg*				msg;

		_timer.start();
		_timer.wait();
		msg		= new S1Msg1(_srv,m1Payload,rh);
		if(msg){
			_srv.getMailbox().post(*msg);
			_sema.wait();
			delete msg;
			}
		msg		= new S1Msg2(_srv,m2Payload,rh);
		if(msg){
			_srv.getMailbox().post(*msg);
			_sema.wait();
			delete msg;
			}
		}
	}

