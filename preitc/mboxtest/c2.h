/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_preitc_mbox_c2h_
#define _oscl_preitc_mbox_c2h_
#include "s1.h"
#include "oscl/krux/timer/sematimer.h"
#include "oscl/krux/sema/bsema.h"
#include "oscl/mt/runnable.h"
#include "oscl/mt/itc/mbox/clirsp.h"

namespace Oscl {

class C2Cli;

typedef Oscl::Mt::Itc::CliResponse<S1Srv,C2Cli,Msg1Payload>	C2Msg1;

/** Client 2 client response interface.
 */
class C2Cli {
	public:
		/** This is the abstract response interface for client 2.
		 */
		virtual void	response(Oscl::Mt::Itc::CliResponse<S1Srv,C2Cli,Msg1Payload>& msg)=0;
		/** This is the abstract response interface for client 2.
		 */
		virtual void	closed(Oscl::Mt::Itc::CliResponse<S1Srv,C2Cli,Msg1Payload>& msg)=0;
	};


/** Client 2 thread.
 */
class C2 : public Oscl::Mt::Runnable , public C2Cli {
	private:
		S1Srv&					_srv;
		Krux::Sema::Binary		_sema;
		Krux::SemaTimer			_timer;
		Oscl::Mt::Itc::Mailbox	_mbox;
	public:
		C2(S1Srv& srv);
		virtual ~C2();

		/** Client 2 run method.
		 */
		void run() noexcept;

	public:
		/** Implements the abstract response interface.
		 */
		void	response(Oscl::Mt::Itc::CliResponse<S1Srv,C2Cli,Msg1Payload>& msg);
		/** Implements the abstract closed response interface.
		 */
		void	closed(Oscl::Mt::Itc::CliResponse<S1Srv,C2Cli,Msg1Payload>& msg);
	};

};

#endif
