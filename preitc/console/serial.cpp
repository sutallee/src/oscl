/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <string.h>
#include "serial.h"
#include "oscl/preitc/serial/transmitter.h"

using namespace Oscl;

SerialConsoleOutput::SerialConsoleOutput(SerialTransmitter& serialTx):
		_serialTx(serialTx)
		{
	_serialTx.attach(this);
	}

void	SerialConsoleOutput::write(const uint8_t* data,unsigned len){
	unsigned			remaining;
	_mutex.lock();
	for(remaining=len;remaining;data+=len){
		len	= _serialTx.write(data,remaining);
		remaining	-=len;
		if(remaining){
			_txReadySema.wait();
			}
		}
	_mutex.unlock();
	}

void	SerialConsoleOutput::print(const char* string){
	write((const uint8_t*)string,strlen(string));
	}

void	SerialConsoleOutput::transmitterReady(){
	_txReadySema.signal();
	}

