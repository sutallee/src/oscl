/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_preitc_console_editorh_
#define _oscl_preitc_console_editorh_
#include <stdint.h>
#include "oscl/krux/mutex/sema.h"
#include "oscl/preitc/serial/rxobserver.h"
#include "oscl/mt/runnable.h"
#include "oscl/krux/sema/bsema.h"
#include "oscl/preitc/cmdline/fsmvar.h"

/** */
namespace Oscl {

class ConsoleOutput;
class SerialReceiver;
class LineEditor;

/** This concrete class implements a buffer that is used by a line editor
	to hold a command line as it is edited.
 */

class LineEditorBuffer {
	public:
		LineEditorBuffer*	_link;
	private:
		char*				_buffer;
		unsigned			_maxLength;
		unsigned			_offset;
	public:
		LineEditorBuffer(char* buffer,unsigned maxLength);

	public:	// Editor user interface
		const char*		get() const;
		const unsigned	length() const;

	public:	// Line editor interface
		void		reset();
		unsigned	append(char data);
		unsigned	erase();
	};

/** This interface is implemented by a line editor client and
	called by the line editor subject when the buffer contains
	an edited line and is ready to be processed by the client.
 */

class LineEditorObserver {
	public:
		virtual void	complete(LineEditorBuffer* buffer)=0;
	};

/** This concrete class is a thread which implements a command line editor.
	A creator/configuration manager will instantiate this class and
	attach it to a serial interface.
 */

class LineEditor : public Mt::Runnable, public SerialRxObserver, public CmdLineEditorStateVar::ContextApi {
	private:
		/** This monitor is used to contol access to data structures
			by different threads.
		 */
		Krux::Mutex::Semaphore		_mutex;

		/** This semaphore is signaled when an event needs to
			wakeup the line editor thread.
		 */
		Krux::Sema::Binary			_input;

		/** This is a reference to output serial device that
			is the destination for echoed characters and
			beeps.
		 */
		ConsoleOutput&				_console;

		/** This is a list of empty line buffers supplied by the client.
		 */
		LineEditorBuffer*			_emptyBuffers;

		/** This is the current line buffer that is being edited.
		 */
		LineEditorBuffer*			_current;

		/** */
		CmdLineEditorStateVar			_state;

		/** This is a pointer to the client which is interested in the
			completed command line.
		 */
		LineEditorObserver*			_observer;

		/** This is the maximum size of a command buffer.
		 */
		static const unsigned		_bufSize=32;

		/** This is one of the buffers used to create a
			LineEditorBuffer.
		 */
		char						_buff1[_bufSize];

		/** This is one of the buffers.
		 */
		LineEditorBuffer			_b1;

		/** This is one of the buffers used to create a
			LineEditorBuffer.
		 */
		char						_buff2[_bufSize];

		/** This is one of the buffers.
		 */
		LineEditorBuffer			_b2;

		/** This is used to maintain a queue of LineEditorBuffer's
			which are filled as octets are forwarded from the
			serial receiver.
		 */
		LineEditorBuffer*			_head;

		/** This is used to maintain a queue of LineEditorBuffer's
			which are filled as octets are forwarded from the
			serial receiver.
		 */
		LineEditorBuffer*			_tail;

		/** This is the current buffer that is being filled
			by the serial receiver.
		 */
		LineEditorBuffer*			_currentInput;

		/** This is a list of empty input buffers that are available
			to be filled by the serial receiver.
		 */
		LineEditorBuffer*			_freeInputBuffers;

	public:
		/** The constructor.
		 */
		LineEditor(ConsoleOutput& console);

		/** The constructor.
		 */
		LineEditor(ConsoleOutput& console,LineEditorObserver* observer);

		/** The destructor is virtual, of course.
		 */
		virtual ~LineEditor();

	public: // Mt::Runnable interface
		/** Implements the threads processing loop.
		 */
		void	run() noexcept;

	public:	// SerialRxObserver interface
		/** Called by the lower layer serial receiver for each character that is received.
		 */
		void forward(uint8_t data);

	public:	// User interface
		/** Called by the client to give the editor a buffer in which to place the command line.
		 */
		void				supply(LineEditorBuffer* buffer);

		/** Called by the client configuration entity to which complete edited command lines
			will be forwarded. Since only one observer is allowed, a pointer to any previous
			observer is returned.
		 */
		LineEditorObserver*	attach(LineEditorObserver* observer);

	private:	// CmdLineEditorStateVar::ContextApi
		/** Causes a beep to be sent to the console.
		 */
		void		beep() noexcept;

		/** Causes the current command line to be completed and forwarded to the
			upper layer observer.
		 */
		void		done() noexcept;

		/** Causes an erase sequence to be performed.
		 */
		void		erase() noexcept;

		/** Causes the specified data to be echoed on the console.
		 */
		void		echo(char data) noexcept;

		/** Causes the command line to be completely erased.
		 */
		void		lineKill() noexcept;

		/** Causes the specified data to be appended to the current buffer.
		 */
		unsigned	append(char data) noexcept;

	public:
		/** Returns a new command line buffer or null if none is available.
		 */
		LineEditorBuffer*	nextAlloc();

	private:
		/** Returns the buffer to the command line buffer free list.
		 */
		void				free(LineEditorBuffer* b);

	private:	// input interface
		/** Places a non-empty buffer in the input buffer queue.
		 */
		void				put(LineEditorBuffer* b);

		/** Gets a non-empty buffer from the input buffer queue.
		 */
		LineEditorBuffer*	get();

		/** Gets a non-empty buffer from the input buffer queue while
			protecting the queue using the specified mutex. The mutex
			is needed to guard the queue from lower layer thread receive
			access.
		 */
		LineEditorBuffer*	get(Oscl::Mt::Mutex::Simple::Api& mutex);

		/** Frees the input buffer for use by the serial receiver.
		 */
		void				freeInput(LineEditorBuffer* b);
	};

};

#endif
