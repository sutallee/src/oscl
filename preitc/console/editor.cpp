/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "editor.h"
#include "oscl/preitc/serial/receiver.h"
#include "output.h"

using namespace Oscl;

LineEditorBuffer::LineEditorBuffer(char* buffer,unsigned maxLength):
		_link(0),
		_buffer(buffer),
		_maxLength(maxLength),
		_offset(0)
		{
	}

const char*	LineEditorBuffer::get() const{
	return _buffer;
	}

const unsigned	LineEditorBuffer::length() const{
	return _offset;
	}

void		LineEditorBuffer::reset(){
	_offset		= 0;
	_buffer[0]	= '\0';
	}

unsigned	LineEditorBuffer::append(char data){
	if(_offset < _maxLength-1){
		_buffer[_offset]	= data;
		_offset++;
		_buffer[_offset]	= '\0';
		return 1;
		}
	return 0;
	}

unsigned	LineEditorBuffer::erase(){
	if(_offset > 0){
		_offset--;
		_buffer[_offset]	= '\0';
		return 1;
		}
	return 0;
	}

/////////////////// LineEditor //////////////////////

LineEditor::LineEditor(ConsoleOutput& console):
		_console(console),
		_emptyBuffers(0),
		_current(0),
		_state(*this),
		_b1(_buff1,_bufSize),
		_b2(_buff2,_bufSize),
		_head(0),
		_tail(0),
		_currentInput(0)
		{
	}

LineEditor::LineEditor(ConsoleOutput& console,LineEditorObserver* observer):
		_console(console),
		_emptyBuffers(0),
		_current(0),
		_state(*this),
		_observer(observer),
		_b1(_buff1,_bufSize),
		_b2(_buff2,_bufSize),
		_head(0),
		_tail(0),
		_currentInput(0)
		{
	}

LineEditor::~LineEditor(){
	}

void LineEditor::run() noexcept{
	LineEditorBuffer*	b;
	unsigned			i;
	const char*			p;
	freeInput(&_b1);
	freeInput(&_b2);
	while(true){
		_input.wait();
		while((b=get(_mutex))){
			for(i=0,p = b->get();i<b->length();i++,p++){
				_state.put(*p);
				}
			freeInput(b);
			}
		}
	}

void LineEditor::forward(uint8_t data){
	_mutex.lock();
	if(!_currentInput || (_currentInput != _head)){
		if((_currentInput = _freeInputBuffers)){
			_freeInputBuffers		= _currentInput->_link;
			put(_currentInput);
			_input.signal();
			}
		}
	if(_currentInput){
		if(!_currentInput->append(data)){
			if((_currentInput = _freeInputBuffers)){
				_freeInputBuffers		= _currentInput->_link;
				put(_currentInput);
				_input.signal();
				_currentInput->append(data);
				}
			}
		}
	_mutex.unlock();
	}

void	LineEditor::supply(LineEditorBuffer* buffer){
	this->free(buffer);
	}

LineEditorObserver*	LineEditor::attach(LineEditorObserver* observer){
	LineEditorObserver*	old;
	_mutex.lock();
	old			= _observer;
	_observer	= observer;
	_mutex.unlock();
	return old;
	}

void	LineEditor::beep() noexcept{
	_console.print("\007");
	}

void	LineEditor::done() noexcept{
	LineEditorBuffer*	b;

	b	= nextAlloc();

	_console.print("\n");

	if(b){
		if(_observer){
			_observer->complete(b);
			}
		else{
			this->free(b);
			}
		}
	}

void	LineEditor::erase() noexcept{
	if(_current){
		if(_current->erase()){
			_console.print("\010 \010");
			}
		}
	}

void	LineEditor::echo(char data) noexcept{
	_console.write((uint8_t*)&data,1);
	}

void	LineEditor::lineKill() noexcept{
	if(_current){
		while(_current->erase()){
			_console.print("\010 \010");
			}
		}
	}

unsigned	LineEditor::append(char data) noexcept{
	unsigned	n;
	if(_current){
		n	= _current->append(data);
		return n;
		}
	return 0;
	}

void		LineEditor::free(LineEditorBuffer* buffer){
	buffer->reset();
	_mutex.lock();
	if(!_current){
		_current	= buffer;
		}
	else{
		buffer->_link	= _emptyBuffers;
		_emptyBuffers	= buffer;
		}
	_mutex.unlock();
	}

LineEditorBuffer*		LineEditor::nextAlloc(){
	LineEditorBuffer*	b;
	_mutex.lock();
	b			= _current;
	_current	= _emptyBuffers;
	if(_emptyBuffers){
		_emptyBuffers	= _emptyBuffers->_link;
		}
	_mutex.unlock();
	return b;
	}

void	LineEditor::put(LineEditorBuffer* b){
	if(_head){
		_tail->_link	= b;
		_tail			= b;
		}
	else{
		_head	= _tail	= b;
		}
	b->_link		= 0;
	}

LineEditorBuffer*	LineEditor::get(Oscl::Mt::Mutex::Simple::Api& mutex){
	LineEditorBuffer*	b;
	mutex.lock();
	b	= get();
	mutex.unlock();
	return b;
	}

LineEditorBuffer*	LineEditor::get(){
	LineEditorBuffer*	b;
	if((b = _head)){
		_head	= b->_link;
		}
	return b;
	}

void		LineEditor::freeInput(LineEditorBuffer* b){
	b->reset();
	_mutex.lock();
	b->_link			= _freeInputBuffers;
	_freeInputBuffers	= b;
	_mutex.unlock();
	}

