/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_preitc_cmdline_fsmvarh_
#define _oscl_preitc_cmdline_fsmvarh_

/** */
namespace Oscl {

/** */
class CmdLineEditorStateVar {
	public:
		/** */
		class ContextApi {
			public:
				/** Causes a beep to be sent to the console.
				 */
				virtual void		beep() noexcept=0;

				/** Causes the current command line to be completed and forwarded to the
					upper layer observer.
				 */
				virtual void		done() noexcept=0;

				/** Causes an erase sequence to be performed.
				 */
				virtual void		erase() noexcept=0;

				/** Causes the specified data to be echoed on the console.
				 */
				virtual void		echo(char data) noexcept=0;

				/** Causes the command line to be completely erased.
				 */
				virtual void		lineKill() noexcept=0;

				/** Causes the specified data to be appended to the current buffer.
				 */
				virtual unsigned	append(char data) noexcept=0;

				/** Returns true if the data is to be interpreted as a delete character.
				 */
				virtual bool		isDelete(char data) const noexcept;

				/** Returns true if the data is to be interpreted as a line termination character.
				 */
				virtual bool		isLineTerminator(char data) const noexcept;

				/** Returns true if the data is to be interpreted as a line erase character.
				 */
				virtual bool		isLineKill(char data) const noexcept;
			};
	private:
		/** This abstract interface is used to implement a state pattern
			for the line editor.
		 */

		class State {
			public:
				/** When a character is available to be placed in the line editor buffer,
					this abstract function is called. The exact behavior is determined
					by the concrete derived classes which represent the state of the
					buffer.
				 */
				virtual void	put(CmdLineEditorStateVar& var,ContextApi& context,char data) const noexcept=0;
			};

		/** This concrete sub-class of State indicates that there is
			no buffer in which to place the data.
		 */
		class StateBufferWait : public State {
			public:
				/** Since there is no place to put the data the editor's beep
					function is called.
				 */
				void	put(CmdLineEditorStateVar& var,ContextApi& context,char data) const noexcept;
			};

		/** This concrete sub-class of State indicates that the current
			buffer is empty.
		 */
		class StateEmpty : public State {
			public:
				/** If the data is a delete character a beep is issued, otherwise
					the data is placed in the buffer.
				 */
				void	put(CmdLineEditorStateVar& var,ContextApi& context,char data) const noexcept;
			};

		/** This concrete sub-class of State indicates that the current
			buffer is neither empty nor full.
		 */
		class StateSomeData : public State {
			public:
				/** Buffer editing is performed based on the type of data.
				 */
				void	put(CmdLineEditorStateVar& var,ContextApi& context,char data) const noexcept;
			};

		/** This concrete sub-class of State indicates that the current
			buffer is full.
		 */
		class StateFull : public State {
			public:
				/** Beeps if the data is to be appended to the buffer.
				 */
				void	put(CmdLineEditorStateVar& var,ContextApi& context,char data) const noexcept;
			};

	private:
		/** */
		static const StateBufferWait	_bufferWaitState;

		/** */
		static const StateEmpty			_emptyState;

		/** */
		static const StateSomeData		_someDataState;

		/** */
		static const StateFull			_fullState;

	private:
		/** */
		ContextApi&		_context;

		/** */
		const State*	_state;

	public:
		/** The constructor
		 */
		CmdLineEditorStateVar(ContextApi& context) noexcept;

		/** */
		void	put(char data) noexcept;

	private:
		/** */
		friend class StateBufferWait;
		/** */
		friend class StateEmpty;
		/** */
		friend class StateSomeData;
		/** */
		friend class StateFull;

		/** Causes the line editor state to change to waiting for a new command
			line buffer.
		 */
		void		transitionToBufferWait() noexcept;

		/** Causes the line editor state to change to buffer empty.
		 */
		void		transitionToEmpty() noexcept;

		/** Causes the line editor state to change to not empty / not full state.
		 */
		void		transitionToSomeData() noexcept;

		/** Causes the line editor state to change to buffer full.
		 */
		void		transitionToFull() noexcept;

	};

};

#endif
