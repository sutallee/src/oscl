/*
   Copyright (C) 2020 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "random/rand32.h"

#include "oscl/entropy/rand.h"

/** */
namespace Oscl {
/** */
namespace Entropy {
/** */
namespace Random {

/** */
void	fetch(void* buffer, unsigned len) noexcept{
#if 0
	// NOT cryptographically secure
	sys_rand_get(
		buffer,
		len
		)
#endif
	int
	result	= sys_csrand_get(
				buffer,
				len
				);

	if(result){
		for(;;);
		}
	}

}
}
}

