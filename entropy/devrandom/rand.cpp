/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

#include "oscl/entropy/rand.h"

/** */
namespace Oscl {
/** */
namespace Entropy {
/** */
namespace Random {

/** */
void	fetch(void* buffer, unsigned len) noexcept{
	int	fd	= open("/dev/urandom",O_RDONLY);
	if(fd < 0){
		perror("Can't open /dev/urandom.");
		exit(1);
		}

	int
	err	= read(
			fd,
			buffer,
			len
			);

	if(err < 0){
		perror("Can't read /dev/urandom.");
		}
	close(fd);
	}

}
}
}

