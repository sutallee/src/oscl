/*
   Copyright (C) 2005 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include "oscl/entropy/random128.h"
#include "oscl/posix/errno/errstring.h"
#include "oscl/error/fatal.h"

/** */
namespace Oscl {
/** */
namespace Entropy {
/** */
namespace Random128 {

/** */
void	fetch(unsigned char	buffer[bufferSize]) noexcept{
	int	fd	= open("/dev/random",O_RDONLY);
	if(fd < 0){
		perror("Can't open /dev/random.");
		exit(1);
		}
	ssize_t
	result	= read(fd,buffer,bufferSize);
	if(result < 0){
		Oscl::Posix::Errno::ErrString	err;
		Oscl::ErrorFatal::logAndExit(
			"%s: read() failed: %s",
			__PRETTY_FUNCTION__,
			err.value()
			);
		}

	close(fd);
	}

}
}
}

