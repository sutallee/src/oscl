/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "entry.h"
#include "oscl/error/info.h"

using namespace Oscl::Block::RW::Cache::Basic;

Entry::Entry(	Oscl::Block::RW::Api<512>&			rwApi,
				Oscl::Memory::AlignedBlock<512>&	dmaBuffer
				) noexcept:
		Oscl::Tree::Node<Entry,unsigned long>(*this),
		_blockNumber(0),
		_rwApi(rwApi),
		_dirty(false),
		_valid(false),
		_dmaBuffer(dmaBuffer)
		{
	}

bool	Entry::assign(unsigned long blockNumber) noexcept{
	if(flush()){
		return true;
		}
	_blockNumber	= blockNumber;
	_valid			= false;
	return !content();
	}

Oscl::Memory::AlignedBlock<512>*	Entry::content() noexcept{
	if(!_valid){
		if(_rwApi.readApi().read(_blockNumber,1,&_dmaBuffer)){
			Oscl::Error::Info::log("Cache validate failed!\n");
			return 0;
			}
		_valid	= true;
		}
	return &_dmaBuffer;
	}

bool	Entry::flush() noexcept{
	if(_dirty && _valid){
		if(_rwApi.writeApi().write(_blockNumber,1,&_dmaBuffer)){
			Oscl::Error::Info::log("Cache flush failed!\n");
			return true;
			}
		_dirty	= false;
		}
	return false;
	}

void	Entry::invalidate() noexcept{
	_valid	= false;
	}

void	Entry::touch() noexcept{
	_dirty	= true;
	}

