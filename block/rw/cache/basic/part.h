/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_block_rw_cache_basic_parth_
#define _oscl_block_rw_cache_basic_parth_
#include "oscl/block/rw/cache/api.h"
#include "oscl/queue/dqueue.h"
#include "oscl/tree/root.h"
#include "entry.h"

/** */
namespace Oscl {
/** */
namespace Block {
/** */
namespace RW {
/** */
namespace Cache {
/** */
namespace Basic {

/** How does the user know if a cache flush succeeds?
	E.g. the device is removed or dies. Does the user
	care? Yes.
	Each of these operations should return an appropriate
	Oscl::Block::{Read|Write}::Status::Result. The state
	of the cache should remain "failed" after an error
	is detected until reset by the context. Since the
	cache is associated with a particular Volume, a device
	error would indicate the imminent destruction of the
	Volume.
 */
class Part : public Oscl::Block::RW::Cache::Api {
	public:
		/** This type defines the size of a single DMA
			cache block. Each of these blocks must be
			located in a DMA capable address space.
		 */
		typedef Oscl::Memory::AlignedBlock<512>	DmaMem;
		/** This structure defines the per cache block
			memory requirement. One of these blocks of
			memory is required for each DmaMem block.
			This memory, however, is not required to
			be in a DMA accessible memory space.
		 */
		struct VarMem {
			/** */
			Oscl::Memory::AlignedBlock<sizeof(Entry)>	_entry;
			};
	private:
		/** */
		Oscl::Tree::Root<Entry,unsigned long>	_tree;
		/** */
		Oscl::DQueue<Entry>					_lruQ;
		/** */
		Oscl::Block::RW::Api<512>&			_rwApi;
	public:
		/** */
		Part(	Oscl::Block::RW::Api<512>&	rwApi,
				DmaMem						dmaMem[],
				VarMem						varMem[],
				unsigned					nEntries
				) noexcept;
	public:	// Oscl::Block::RW::Cache::Api
		/** The pointer returned by this operation is good
			until one of the following operations is invoked:
			o fetch()
			o invalidate()
			The returned pointer is null for any kind of
			error.
		 */
		void*	fetch(unsigned long block) noexcept;
		/** Marks the cache line containing the specified
			block as invalid. The next invocation of fetch()
			will cause the block to be re-read. A pointer to
			the block previously obtained by fetch() is invalid
			after this operation. If the block is not in the
			cache, then no operation is performed.
		 */
		void	invalidate(unsigned long block) noexcept;
		/** If the specified block is "dirty" as caused by
			the touch() operation, then the block will immediately
			be written to the block device and the "dirty" bit
			cleared. This operation returns "true" if there is
			a transport error such that the block cannot be written
			to the block device, and returns "false" otherwise.
		 */
		bool	flush(unsigned long block) noexcept;
		/** If and blocks in the specified range are "dirty" as
			caused by the touch() operation then those blocks are
			written to the block device and the "dirty" bit is
			cleared. In addition, after all "dirty" blocks have
			been written, a flush is issued to the low level block
			device for all blocks in the specified range. This
			low level flush causes all data in the range to be
			immediately written to the physical media before this
			function completes.
			This operation returns "true" if there is
			a transport error such that any block cannot be written
			to the physical media , and returns "false" otherwise.
		 */
		bool	flushHard(	unsigned long	block,
							unsigned long	nBlocks
							) noexcept;
		/** Causes all "dirty" cache blocks to be immediately
			written to the block device.
			This operation returns "true" if there is a
			transport error such that any of the "dirty" cache
			blocks cannot be written to the block device, and returns
			"false" otherwise.
		 */
		bool	flush() noexcept;
		/** If the specified block is currently in the cache,
			it is marked as "dirty" such that it will be written
			to the block device the next time the block is
			flushed. This operation is only ever valid if the
			cache line specified by block is currently valid.
			Touch is only to be called by a client after he
			has changed the content of the buffer returned
			by fetch(), and before fetch() or invalidate() is
			invoked another time. It is a programming error to
			invoke this operation if the entry specified by block
			is NOT in the cache.
		 */
		void	touch(unsigned long block) noexcept;
	};

}
}
}
}
}

#endif
