/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_block_rw_cache_basic_configh_
#define _oscl_block_rw_cache_basic_configh_
#include <new>
#include "part.h"
#include "oscl/kernel/mmu.h"
#include "oscl/extalloc/api.h"
#include "oscl/error/fatal.h"



/** */
namespace Oscl {
/** */
namespace Block {
/** */
namespace RW {
/** */
namespace Cache {
/** */
namespace Basic {

/** */
template <unsigned nEntries>
class Config {
	private:
		/** */
		Oscl::ExtAlloc::Record						_dmaMemRec;
		/** */
		Part::VarMem								_varMem[nEntries];
		/** */
		Oscl::Memory::AlignedBlock<sizeof(Part)>	_partMem;
		/** */
		Part*										_part;
	public:
		/** */
		Config(Oscl::Block::RW::Api<512>&	rwApi) noexcept;
	};

template <unsigned nEntries>
Config<nEntries>::Config(	Oscl::Block::RW::Api<512>&	rwApi,
							Oscl::ExtAlloc::Api&		dmaMemAllocator
							) noexcept{
	const unsigned long	pageSize			= OsclKernelGetPageSize();
	const unsigned long	almostPageSize		= pageSize-1;

	const unsigned long	partDmaMemSize		=
		(sizeof(Oscl::Block::RW::Cache::Basic::Part::DmaMem));

	const unsigned long	dmaMemSize		= partDmaMemSize;

	const unsigned long	nPages			=	(dmaMemSize+almostPageSize)/pageSize;
	const unsigned long	monAllocSize	=	pageSize*nPages;
	const unsigned long	allocSize		=	monAllocSize*nEntries;

	if(!dmaMemAllocator.alloc(_dmaMemRec,allocSize,~almostPageSize)){
		Oscl::ErrorFatal::logAndExit(
			"Oscl::Block::RW::Cache::Basic::Config: cant allocate dma buffers."
			);
		}

	unsigned char*	partDmaMem	= (unsigned char*)_dmaMemRec.getFirstUnit();

		Oscl::Block::RW::Cache::Basic::Part::DmaMem*
		mem	= (Oscl::Bloc::RW::Cache::Basic::Part::DmaMem*)partDmaMem;
		Oscl::Block::RW::Cache::Basic::Part*
	_part	= new(&_partMem)
					Oscl::Block::RW::Cache::Basic::
					Part(	rwApi,
							mem,
							_varMem,
							nEntries
							);
	}
}
}
}
}
}

#endif
