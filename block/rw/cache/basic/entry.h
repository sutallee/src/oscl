/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_block_rw_cache_basic_entryh_
#define _oscl_block_rw_cache_basic_entryh_
#include "oscl/memory/block.h"
#include "oscl/tree/node.h"
#include "oscl/queue/dqueueitem.h"
#include "oscl/block/rw/api.h"

/** */
namespace Oscl {
/** */
namespace Block {
/** */
namespace RW {
/** */
namespace Cache {
/** */
namespace Basic {

/*
Rules for cache user.
	o The "content" is only valid between cache calls.
	o Each time one of the following cache operations is
		invoked, one or more cache entries may become
		invalidated.
		-

If the entries are maintained in a tree structure to speed up
access by blockNumber, then how will I implement the LRU
protocol? LRU is easiest to implement by keeping a Queue<>
of entries. Each time an entry is accessed, it is "remove"d
from the Queue<> and then "put" back into the Queue. The
LRU entry is the one that can be accessed by a Queue "get"
operation. However, this LRU technique is NOT scalable
since the time required to "remove" an entry from the
Queue varies with the Queue length. This problem *could* be
resolved by using a doubly linked list (DQueue) for the queue
implementation.

For the purpose of the
LRU algorithm, an "access" occurs when the "content" operation
is invoked.
*/

/** */
class Entry :	public Oscl::Tree::Node<Entry,unsigned long>,
				public Oscl::DQueueItem
				{
	private:
		/** */
		unsigned long							_blockNumber;
		/** */
		Oscl::Block::RW::Api<512>&				_rwApi;
		/** */
		bool									_dirty;
		/** */
		bool									_valid;
		/** */
		Oscl::Memory::AlignedBlock<512>&		_dmaBuffer;
	public:
		/** */
		Entry(	Oscl::Block::RW::Api<512>&				rwApi,
				Oscl::Memory::AlignedBlock<512>&		dmaBuffer
				) noexcept;
		/** This operation is issued to assign the specified
			blockNumber to this cache block. If the cache
			entry is dirty, then the block is first flushed.
			Returns true if an I/O error happens.
		 */
		bool	assign(unsigned long blockNumber) noexcept;
	public:
		/** Returns a pointer to the cache block.
			This operation may result in a block
			read operation if block has been
			invalidated by the invalidate operation.
		 */
		Oscl::Memory::AlignedBlock<512>*	content() noexcept;
		/** Writes the block to the block device
			if it is "dirty" as marked by the
			touch operation.
			Returns false for success and true if
			it fails.
		 */
		bool	flush() noexcept;
		/** Marks the block such that it will be
			re-read from the block device the next
			time the "content" operation is invoked.
//			If the block is dirty when this operation
//			is invoked, then the block will be written
//			to the block device.
		 */
		void	invalidate() noexcept;
		/** Marks the block as "dirty" such that it
			will be written before being re-claimed.
			This operation *MUST* be called after
			data in the buffer returned by the
			content operation has been changed.
		 */
		void	touch() noexcept;
	public:
		/** */
		const unsigned long&	getKey() const noexcept{
			return _blockNumber;
			}
	};

}
}
}
}
}

#endif
