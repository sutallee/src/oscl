/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <new>
#include "part.h"

volatile	bool	bigDummy;

using namespace Oscl::Block::RW::Cache::Basic;

Part::Part(	Oscl::Block::RW::Api<512>&	rwApi,
			DmaMem						dmaMem[],
			VarMem						varMem[],
			unsigned					nEntries
			) noexcept:
		_rwApi(rwApi)
		{
	for(unsigned i=0;i<nEntries;++i){
		Entry*
		entry	= new(&varMem[i])
					Entry(rwApi,dmaMem[i]);
		_lruQ.put(entry);
		}
	}

void*	Part::fetch(unsigned long block) noexcept{
	Entry*	entry;
	Oscl::Tree::Node<Oscl::Block::RW::Cache::Basic::Entry, unsigned long>*
	node	= _tree.find(block);
	if(node){
		bigDummy	= true;
		entry	= &node->content();
		_lruQ.remove(entry);
		_lruQ.put(entry);
		return entry->content();
		}
	bigDummy	= false;
	entry	= _lruQ.get();
	if(!entry){
		for(;;);
		}
	_tree.remove(entry);
	if(entry->assign(block)){
		_tree.insert(entry);
		_lruQ.put(entry);
		return 0;
		}
	_tree.insert(entry);
	_lruQ.put(entry);
	return entry->content();
	}

void	Part::invalidate(unsigned long block) noexcept{
	Oscl::Tree::Node<Oscl::Block::RW::Cache::Basic::Entry, unsigned long>*
	node	= _tree.find(block);
	if(!node){
		return;
		}
	_tree.remove(node);
	Entry*
	entry	= &node->content();
	_lruQ.remove(entry);
	entry->invalidate();
	_lruQ.push(entry);
	}

bool	Part::flush(unsigned long block) noexcept{
	Oscl::Tree::Node<Oscl::Block::RW::Cache::Basic::Entry, unsigned long>*
	node	= _tree.find(block);
	if(!node){
		return false;
		}
	Entry*
	entry	= &node->content();
	if(entry->flush()){
		return true;
		}
//	_lruQ.remove(entry);
//	_lruQ.put(entry);
	return false;
	}

bool	Part::flushHard(	unsigned long	block,
							unsigned long	nBlocks
							) noexcept{
	// First write all dirty blocks
	// in the range.
	for(unsigned i=0;i<nBlocks;++i){
		if(flush(block+i)){
			// Finish early for errors.
			return true;
			}
		}
	// Finally, force a low level flush
	// to the physical media of all blocks
	// in the range.
	if(_rwApi.writeApi().flush(block,nBlocks)){
		// Report errors.
		return true;
		}
	return false;
	}

bool	Part::flush() noexcept{
	Entry*	entry;
	for(entry=_lruQ.first();entry;entry=_lruQ.next(entry)){
		if(entry->flush()){
			return true;
			}
		}
	return false;
	}

void	Part::touch(unsigned long block) noexcept{
	Oscl::Tree::Node<Oscl::Block::RW::Cache::Basic::Entry, unsigned long>*
	node	= _tree.find(block);
	if(!node){
		// This is an illegal condition,
		// since the block must be valid
		// and in the cache to be modified,
		// and the only reason for invoking this
		// operation is to indicate that the block
		// has been modified.
		// Thus, if we get here it is a programming
		// error.
		for(;;);	// FIXME: should log programming error
		return;
		}
	node->content().touch();
	}

