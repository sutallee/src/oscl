/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "fsm.h"
#include "oscl/mt/thread.h"
using namespace Oscl::Block::RW::Decorator;

void	StateVar::State::
open(	ContextApi&	context,
		StateVar&	sv
		) const noexcept{
	sv.protocolError(stateName());
	}

void	StateVar::State::
close(	ContextApi&	context,
		StateVar&	sv
		) const noexcept{
	sv.protocolError(stateName());
	}

void	StateVar::State::
writeCancelResp(	ContextApi&	context,
					StateVar&	sv
					) const noexcept{
	sv.protocolError(stateName());
	}

void	StateVar::State::
readCancelResp(	ContextApi&	context,
				StateVar&	sv
				) const noexcept{
	sv.protocolError(stateName());
	}

void	StateVar::State::
writeResp(	ContextApi&	context,
			StateVar&	sv
			) const noexcept{
	sv.protocolError(stateName());
	}

void	StateVar::State::
flushResp(	ContextApi&	context,
			StateVar&	sv
			) const noexcept{
	sv.protocolError(stateName());
	}

void	StateVar::State::
readResp(	ContextApi&	context,
			StateVar&	sv
			) const noexcept{
	sv.protocolError(stateName());
	}

void	StateVar::State::
writeReq(	ContextApi&	context,
			StateVar&	sv
			) const noexcept{
	sv.protocolError(stateName());
	}

void	StateVar::State::
flushReq(	ContextApi&	context,
			StateVar&	sv
			) const noexcept{
	sv.protocolError(stateName());
	}

void	StateVar::State::
readReq(	ContextApi&	context,
			StateVar&	sv
			) const noexcept{
	sv.protocolError(stateName());
	}

void	StateVar::State::
readCancelReq(	ContextApi&	context,
				StateVar&	sv
				) const noexcept{
	sv.protocolError(stateName());
	}

void	StateVar::State::
writeCancelReq(	ContextApi&	context,
				StateVar&	sv
				) const noexcept{
	sv.protocolError(stateName());
	}

///////////////////////////////////

void	StateVar::Closed::
open(	ContextApi&	context,
		StateVar&	sv
		) const noexcept{
	context.returnOpenReq();
	sv.changeToIdle();
	}

///////////////////////////////////

void	StateVar::DeferReq::
writeReq(	ContextApi&	context,
			StateVar&	sv
			) const noexcept{
	context.deferWriteReq();
	}

void	StateVar::DeferReq::
flushReq(	ContextApi&	context,
			StateVar&	sv
			) const noexcept{
	context.deferFlushReq();
	}

void	StateVar::DeferReq::
readReq(	ContextApi&	context,
			StateVar&	sv
			) const noexcept{
	context.deferReadReq();
	}

///////////////////////////////////
void	StateVar::Idle::
close(	ContextApi&	context,
		StateVar&	sv
		) const noexcept{
	context.returnCloseReq();
	sv.changeToClosed();
	}

void	StateVar::Idle::
writeReq(	ContextApi&	context,
			StateVar&	sv
			) const noexcept{
	context.startWrite();
	sv.changeToWrite();
	}

void	StateVar::Idle::
flushReq(	ContextApi&	context,
			StateVar&	sv
			) const noexcept{
	context.startFlush();
	sv.changeToFlush();
	}

void	StateVar::Idle::
readReq(	ContextApi&	context,
			StateVar&	sv
			) const noexcept{
	context.startRead();
	sv.changeToRead();
	}

void	StateVar::Idle::
readCancelReq(	ContextApi&	context,
				StateVar&	sv
				) const noexcept{
	context.returnIncommingReadCancelReq();
	}

void	StateVar::Idle::
writeCancelReq(	ContextApi&	context,
				StateVar&	sv
				) const noexcept{
	context.returnIncommingWriteCancelReq();
	}

///////////////////////////////////

void	StateVar::Write::
writeResp(	ContextApi&	context,
			StateVar&	sv
			) const noexcept{
	context.finishWrite();
	context.returnWriteReq();
	sv.changeToIdle();
	}

void	StateVar::Write::
writeCancelReq(	ContextApi&	context,
				StateVar&	sv
				) const noexcept{
	if(context.isIncommingCancelForCurrentWrite()){
		context.cancelWrite();
		sv.changeToWriteCanceling();
		return;
		}
	context.returnIncommingWriteCancelReq();
	}

///////////////////////////////////

void	StateVar::Flush::
flushResp(	ContextApi&	context,
			StateVar&	sv
			) const noexcept{
	context.finishWrite();
	context.returnWriteReq();
	sv.changeToIdle();
	}

void	StateVar::Flush::
writeCancelReq(	ContextApi&	context,
				StateVar&	sv
				) const noexcept{
	if(context.isIncommingCancelForCurrentFlush()){
		context.cancelFlush();
		sv.changeToFlushCanceling();
		return;
		}
	context.returnIncommingWriteCancelReq();
	}

///////////////////////////////////

void	StateVar::Read::
readResp(	ContextApi&	context,
			StateVar&	sv
			) const noexcept{
	context.finishRead();
	context.returnReadReq();
	sv.changeToIdle();
	}

void	StateVar::Read::
readCancelReq(	ContextApi&	context,
				StateVar&	sv
				) const noexcept{
	if(context.isIncommingCancelForCurrentRead()){
		context.cancelRead();
		sv.changeToReadCanceling();
		return;
		}
	context.returnIncommingReadCancelReq();
	}

///////////////////////////////////

void	StateVar::ReadCanceling::
readResp(	ContextApi&	context,
			StateVar&	sv
			) const noexcept{
	context.returnReadReq();
	}

void	StateVar::ReadCanceling::
readCancelResp(	ContextApi&	context,
				StateVar&	sv
				) const noexcept{
	context.returnReadCancelReq();
	sv.changeToIdle();
	}

///////////////////////////////////

void	StateVar::WriteCanceling::
writeResp(	ContextApi&	context,
			StateVar&	sv
			) const noexcept{
	context.returnWriteReq();
	}

void	StateVar::WriteCanceling::
writeCancelResp(	ContextApi&	context,
					StateVar&	sv
					) const noexcept{
	context.returnWriteCancelReq();
	sv.changeToIdle();
	}

///////////////////////////////////

void	StateVar::FlushCanceling::
flushResp(	ContextApi&	context,
			StateVar&	sv
			) const noexcept{
	context.returnWriteReq();
	}

void	StateVar::FlushCanceling::
writeCancelResp(	ContextApi&	context,
					StateVar&	sv
					) const noexcept{
	context.returnWriteCancelReq();
	sv.changeToIdle();
	}

