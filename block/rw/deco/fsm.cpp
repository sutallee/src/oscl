/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "fsm.h"
#include "oscl/error/fatal.h"

using namespace Oscl::Block::RW::Decorator;

static const StateVar::Closed						closed;
static const StateVar::Idle							idle;
static const StateVar::Read							read;
static const StateVar::Write						write;
static const StateVar::Flush						flush;
static const StateVar::ReadCanceling				readCanceling;
static const StateVar::WriteCanceling				writeCanceling;
static const StateVar::FlushCanceling				flushCanceling;

// StateVar

StateVar::StateVar(ContextApi& context) noexcept:
		_context(context),
		_state(&closed)
		{
	for(unsigned i=0;i<nTraceRecs;++i){
		_freeTraceRecs.put(&_traceRec[i]);
		}
	}

void	StateVar::open() noexcept{
	_state->open(_context,*this);
	}

void	StateVar::close() noexcept{
	_state->close(_context,*this);
	}

void	StateVar::readCancelResp() noexcept{
	_state->readCancelResp(_context,*this);
	}

void	StateVar::writeCancelResp() noexcept{
	_state->writeCancelResp(_context,*this);
	}

void	StateVar::writeResp() noexcept{
	_state->writeResp(_context,*this);
	}

void	StateVar::flushResp() noexcept{
	_state->flushResp(_context,*this);
	}

void	StateVar::readResp() noexcept{
	_state->readResp(_context,*this);
	}

void	StateVar::writeReq() noexcept{
	_state->writeReq(_context,*this);
	}

void	StateVar::flushReq() noexcept{
	_state->flushReq(_context,*this);
	}

void	StateVar::readReq() noexcept{
	_state->readReq(_context,*this);
	}

void	StateVar::readCancelReq() noexcept{
	_state->readCancelReq(_context,*this);
	}

void	StateVar::writeCancelReq() noexcept{
	_state->writeCancelReq(_context,*this);
	}

// global actions
void	StateVar::protocolError(const char* state) noexcept{
	ErrorFatal::logAndExit(state);
	}

// state change operations
void	StateVar::changeToClosed() noexcept{
	trace(closed);
	_state	= &closed;
	}

void	StateVar::changeToIdle() noexcept{
	trace(idle);
	_state	= &idle;
	_context.executeDeferredReq();
	}

void	StateVar::changeToRead() noexcept{
	trace(read);
	_state	= &read;
	}

void	StateVar::changeToWrite() noexcept{
	trace(write);
	_state	= &write;
	}

void	StateVar::changeToFlush() noexcept{
	trace(flush);
	_state	= &flush;
	}

void	StateVar::changeToReadCanceling() noexcept{
	trace(readCanceling);
	_state	= &readCanceling;
	}

void	StateVar::changeToWriteCanceling() noexcept{
	trace(writeCanceling);
	_state	= &writeCanceling;
	}

void	StateVar::changeToFlushCanceling() noexcept{
	trace(flushCanceling);
	_state	= &flushCanceling;
	}

static unsigned long	_nTraces;

void	StateVar::trace(const State& state) noexcept{
	TraceRec*	rec;
	++_nTraces;
	rec	= _freeTraceRecs.get();
	if(!rec){
		rec	= _trace.get();
		}
	rec->_state	= &state;
	_trace.put(rec);
	}

