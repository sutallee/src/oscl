/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_block_rw_deco_driverh_
#define _oscl_block_rw_deco_driverh_
#include "oscl/memory/block.h"
#include "oscl/queue/queue.h"
#include "oscl/mt/itc/srv/close.h"
#include "oscl/block/read/itc/respmem.h"
#include "oscl/block/write/itc/respmem.h"
#include "oscl/block/read/api.h"
#include "oscl/block/write/api.h"
#include "fsm.h"
#include "oscl/block/rw/itc/api.h"
#include "oscl/block/rw/api.h"
#include "oscl/block/read/itc/sync.h"
#include "oscl/block/write/itc/sync.h"

/** */
namespace Oscl {
/** */
namespace Block {
/** */
namespace RW {
/** */
namespace Decorator {

/** This implementation of the Oscl::Block::RW::ITC::Req::Api
	"decorates" another Oscl::Block::RW::ITC::Req::Api. This
	is typical in sytems of multiple and/or nested partitions.
	It operates asynchronously so that all or many instances
	can share a single thread.
 */
class Driver :	public Oscl::Mt::Itc::Srv::CloseSync,
				private Oscl::Mt::Itc::Srv::Close::Req::Api,
				private Oscl::Block::Read::ITC::Resp::Api<512>,	// new
				private Oscl::Block::Read::ITC::Req::Api<512>,
				private Oscl::Block::Write::ITC::Resp::Api<512>,	// new
				private Oscl::Block::Write::ITC::Req::Api<512>,
				public Oscl::Block::RW::Api<512>,
				public Oscl::Block::RW::ITC::Api<512>,
				private StateVar::ContextApi
				{
	private:
		/** */
		union Requests {
			/** */
			Oscl::Mt::Itc::SrvMsg*				generic;

			/** */
			Oscl::Block::Read::
			ITC::Resp::Api<512>::ReadResp*		readResp;

			/** */
			Oscl::Block::Read::
			ITC::Req::Api<512>::ReadReq*		readReq;

			/** */
			Oscl::Block::Read::
			ITC::Resp::Api<512>::CancelResp*	readCancelResp;

			/** */
			Oscl::Block::Read::
			ITC::Req::Api<512>::CancelReq*		readCancelReq;

			/** */
			Oscl::Block::Write::
			ITC::Resp::Api<512>::WriteResp*		writeResp;

			/** */
			Oscl::Block::Write::
			ITC::Resp::Api<512>::FlushResp*		flushResp;

			/** */
			Oscl::Block::Write::
			ITC::Req::Api<512>::WriteReq*		writeReq;

			/** */
			Oscl::Block::Write::
			ITC::Req::Api<512>::FlushReq*		flushReq;

			/** */
			Oscl::Block::Write::
			ITC::Resp::Api<512>::CancelResp*	writeCancelResp;

			/** */
			Oscl::Block::Write::
			ITC::Req::Api<512>::CancelReq*		writeCancelReq;
			};

		/** */
		union CancelMem {
			/** */
			Oscl::Block::Read::ITC::
			Resp::CancelMem<512>			read;
			/** */
			Oscl::Block::Write::ITC::
			Resp::CancelMem<512>			write;
			};

	private:
		/** */
		union {
			/** */
			Oscl::Block::Read::ITC::Resp::Mem<512>		read;
			/** Write & Cancel */
			Oscl::Block::Write::ITC::Resp::Mem<512>		write;
			} _respMem;

		/** Requests that are queued/serialized
			for execution while the pending bulk
			transaction is executing.
		 */
		Oscl::Queue<Oscl::Mt::Itc::SrvMsg>	_pendingReqs;

		/** Memory used to cancel outstanding requests.
		 */
		CancelMem							_cancelMem;

		/** */
		Oscl::Mt::Itc::PostMsgApi&			_myPapi;

		/** */
		Oscl::Block::Read::
		ITC::Req::Api<512>::SAP&			_readSAP;

		/** */
		Oscl::Block::Read::Api<512>&		_readApi;

		/** */
		Oscl::Block::Write::
		ITC::Req::Api<512>::SAP&			_writeSAP;

		/** */
		Oscl::Block::Write::Api<512>&		_writeApi;

		/** */
		unsigned long						_lbaOffset;

		/** */
		unsigned long						_sizeInBlocks;

		/** */
		Oscl::Block::Read::ITC::Sync<512>	_readSync;

		/** */
		Oscl::Block::Write::ITC::Sync<512>	_writeSync;

		/** */
		StateVar							_state;

		/** */
		Oscl::Mt::Itc::Srv::Open::
		Req::Api::OpenReq*					_openReq;

		/** */
		Oscl::Mt::Itc::Srv::Close::
		Req::Api::CloseReq*					_closeReq;

		/** The "just received" request */
		Requests							_incomming;

		/** The currently executing request */
		Requests							_currentReq;

		/** The currently executing request */
		Requests							_currentResp;

		/** The currently executing cancel request */
		Requests							_cancel;

		/** */
		union {
			/** */
			Oscl::Mt::Itc::SrvMsg*			generic;
			} _currentPipe;

	public:
		/** */
		Driver(	Oscl::Mt::Itc::PostMsgApi&		myPapi,
				Oscl::Block::Read::
				ITC::Req::Api<512>::SAP&		readSAP,
				Oscl::Block::Read::Api<512>&	readApi,
				Oscl::Block::Write::
				ITC::Req::Api<512>::SAP&		writeSAP,
				Oscl::Block::Write::Api<512>&	writeApi,
				unsigned long					lbaOffset,
				unsigned long					sizeInBlocks
				) noexcept;

	private:	// Oscl::Mt::Itc::Srv::Close::Req::Api
		/** */
		void	request(	Oscl::Mt::Itc::Srv::
							Open::Req::Api::OpenReq&	msg
							) noexcept;
		/** */
		void	request(	Oscl::Mt::Itc::Srv::
							Close::Req::Api::CloseReq&	msg
							) noexcept;

	private:	// Oscl::Block::Read::ITC::Resp::Api
		/** */
		void	response(	Oscl::Block::Read::ITC::
							Resp::Api<512>::CancelResp&	msg
							) noexcept;
		/** */
		void	response(	Oscl::Block::Read::ITC::
							Resp::Api<512>::ReadResp&	msg
							) noexcept;

	private:	// Oscl::Block::Write::ITC::Resp::Api
		/** */
		void	response(	Oscl::Block::Write::ITC::
							Resp::Api<512>::CancelResp&	msg
							) noexcept;
		/** */
		void	response(	Oscl::Block::Write::ITC::
							Resp::Api<512>::WriteResp&	msg
							) noexcept;
		/** */
		void	response(	Oscl::Block::Write::ITC::
							Resp::Api<512>::FlushResp&	msg
							) noexcept;

	private:	// Oscl::Block::RW::Api
        /** */
        Oscl::Block::Read::Api<512>&	readApi() noexcept;
        /** */
		Oscl::Block::Write::Api<512>&	writeApi() noexcept;

	private:	// Oscl::Block::RW::ITC::Api
		/** */
		Oscl::Block::Read::
		ITC::Req::Api< 512 >::SAP&		getReadSAP() noexcept;
		/** */
		Oscl::Block::Write::
		ITC::Req::Api< 512 >::SAP&		getWriteSAP() noexcept;

	private:
		/** */
		void    request(	Oscl::Block::Read::
							ITC::Req::Api<512>::CancelReq&	msg
							) noexcept;
		/** */
		void    request(	Oscl::Block::Read::
							ITC::Req::Api<512>::ReadReq&	msg
							) noexcept;
	private:
		/** */
		void    request(	Oscl::Block::Write::
							ITC::Req::Api<512>::CancelReq&	msg
							) noexcept;
		/** */
		void    request(	Oscl::Block::Write::
							ITC::Req::Api<512>::WriteReq&	msg
							) noexcept;

		void    request(	Oscl::Block::Write::
							ITC::Req::Api<512>::FlushReq&	msg
							) noexcept;

	private: // StateVar::ContextApi
		/** */
		void	returnOpenReq() noexcept;
		/** */
		void	returnCloseReq() noexcept;
		/** */
		void	executeDeferredReq() noexcept;
		/** */
		void	deferReadReq() noexcept;
		/** */
		void	deferWriteReq() noexcept;
		/** */
		void	deferFlushReq() noexcept;
		/** */
		void	startRead() noexcept;
		/** */
		void	startWrite() noexcept;
		/** */
		void	startFlush() noexcept;
		/** */
		void	returnReadReq() noexcept;
		/** */
		void	returnWriteReq() noexcept;
		/** */
		void	returnFlushReq() noexcept;
		/** */
		void	returnIncommingReadCancelReq() noexcept;
		/** */
		void	returnIncommingWriteCancelReq() noexcept;
		/** */
		void	returnIncommingFlushCancelReq() noexcept;
		/** */
		void	returnReadCancelReq() noexcept;
		/** */
		void	returnWriteCancelReq() noexcept;
		/** */
		void	returnFlushCancelReq() noexcept;
		/** */
		void	cancelRead() noexcept;
		/** */
		void	cancelWrite() noexcept;
		/** */
		void	cancelFlush() noexcept;
		/** */
		void	finishRead() noexcept;
		/** */
		void	finishWrite() noexcept;
		/** */
		void	finishFlush() noexcept;
		/** */
		bool	deferQueueEmpty() noexcept;
		/** */
		bool	isIncommingCancelForCurrentRead() noexcept;
		/** */
		bool	isIncommingCancelForCurrentWrite() noexcept;
		/** */
		bool	isIncommingCancelForCurrentFlush() noexcept;
	};

}
}
}
}


#endif
