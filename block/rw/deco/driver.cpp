/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <new>
#include "driver.h"
#include "oscl/error/fatal.h"
#include "oscl/error/info.h"

using namespace Oscl::Block::RW::Decorator;

Driver::Driver(	Oscl::Mt::Itc::PostMsgApi&		myPapi,
				Oscl::Block::Read::
				ITC::Req::Api<512>::SAP&		readSAP,
				Oscl::Block::Read::Api<512>&	readApi,
				Oscl::Block::Write::
				ITC::Req::Api<512>::SAP&		writeSAP,
				Oscl::Block::Write::Api<512>&	writeApi,
				unsigned long					lbaOffset,
				unsigned long					sizeInBlocks
				) noexcept:
		Oscl::Mt::Itc::Srv::CloseSync(*this,myPapi),
		_myPapi(myPapi),
		_readSAP(readSAP),
		_readApi(readApi),
		_writeSAP(writeSAP),
		_writeApi(writeApi),
		_lbaOffset(lbaOffset),
		_sizeInBlocks(sizeInBlocks),
		_readSync(*this,myPapi),
		_writeSync(*this,myPapi),
		_state(*this),
		_openReq(0),
		_closeReq(0)
		{
	}

void	Driver::request(	Oscl::Mt::Itc::Srv::
							Open::Req::Api::OpenReq&	msg
							) noexcept{
	_openReq	= &msg;
	_state.open();
	}

void	Driver::request(	Oscl::Mt::Itc::Srv::
							Close::Req::Api::CloseReq&	msg
							) noexcept{
				// FIXME: Do I need to close any outstanding
				// and/or queued requests?
				//
				// No. If the queue
				// is not empty, or there is an outstanding
				// request, then there has been a protocol
				// error (i.e. the client has not been)
				// notified of the dynamic closure.
				// However, since this "layer" may have
				// purely synchronous, clients, which cannot
				// get the "release" notification, we should
				// probably go ahead and cancel the current
				// request (if any) and then retire all other
				// requests with a transportError.
				//
				// Hmmmm. This close may never come if the
				// upper layer does not respond to the "release"
				// notification.
				//
				// Lets start with just throwing a protocol
				// error if there are any outstanding requests.
				//
	_closeReq	= &msg;
	_state.close();
	}

void	Driver::response(	Oscl::Block::Read::
							ITC::Resp::Api<512>::CancelResp&	msg
							) noexcept{
	_incomming.readCancelResp	= &msg;
	_state.readCancelResp();
	}

void	Driver::response(	Oscl::Block::Read::
							ITC::Resp::Api<512>::ReadResp&	msg
							) noexcept{
	_incomming.readResp	= &msg;
	_state.readResp();
	}

void	Driver::response(	Oscl::Block::Write::ITC::
							Resp::Api<512>::CancelResp&	msg
							) noexcept{
	_incomming.writeCancelResp	= &msg;
	_state.writeCancelResp();
	}

void	Driver::response(	Oscl::Block::Write::ITC::
							Resp::Api<512>::WriteResp&	msg
							) noexcept{
	_incomming.writeResp	= &msg;
	_state.writeResp();
	}

void	Driver::response(	Oscl::Block::Write::ITC::
							Resp::Api<512>::FlushResp&	msg
							) noexcept{
	_incomming.flushResp	= &msg;
	_state.flushResp();
	}

////////////////////////

Oscl::Block::Read::Api<512>&   Driver::readApi() noexcept{
	return _readSync;
	}

Oscl::Block::Write::Api<512>&  Driver::writeApi() noexcept{
	return _writeSync;
	}

Oscl::Block::Read::
ITC::Req::Api< 512 >::SAP&		Driver::getReadSAP() noexcept{
	return _readSync.getSAP();
	}

Oscl::Block::Write::
ITC::Req::Api< 512 >::SAP&		Driver::getWriteSAP() noexcept{
	return _writeSync.getSAP();
	}

void    Driver::request(	Oscl::Block::Read::
							ITC::Req::Api<512>::CancelReq&	msg
							) noexcept{
	Oscl::Mt::Itc::SrvMsg*	next;
	if((next=_pendingReqs.remove(&msg._payload._msgToCancel))){
		next->returnToSender();
		msg.returnToSender();
		return;
		}
//	if(_current.read != &msg._payload._msgToCancel){
//		msg.returnToSender();
//		return;
//		}
	_incomming.readCancelReq	= &msg;
	_state.readCancelReq();
	}

void    Driver::request(	Oscl::Block::Read::
							ITC::Req::Api<512>::ReadReq&	msg
							) noexcept{
	_incomming.readReq	= &msg;
	_state.readReq();
	}

void    Driver::request(	Oscl::Block::Write::
							ITC::Req::Api<512>::CancelReq&	msg
							) noexcept{
	Oscl::Mt::Itc::SrvMsg*	next;
	if((next=_pendingReqs.remove(&msg._payload._msgToCancel))){
		next->returnToSender();
		msg.returnToSender();
		return;
		}
	_incomming.writeCancelReq	= &msg;
	_state.writeCancelReq();
	}

void    Driver::request(	Oscl::Block::Write::
							ITC::Req::Api<512>::WriteReq&	msg
							) noexcept{
	_incomming.writeReq	= &msg;
	_state.writeReq();
	}

void    Driver::request(	Oscl::Block::Write::
							ITC::Req::Api<512>::FlushReq&	msg
							) noexcept{
	_incomming.flushReq	= &msg;
	_state.flushReq();
	}

////////////////////////
void	Driver::returnOpenReq() noexcept{
	_openReq->returnToSender();
	_openReq	= 0;
	}

void	Driver::returnCloseReq() noexcept{
	_closeReq->returnToSender();
	_closeReq	= 0;
	}

void	Driver::executeDeferredReq() noexcept{
	Oscl::Mt::Itc::SrvMsg*	next;
	if((next=_pendingReqs.get())){
		next->process();
		}
	}

void	Driver::deferWriteReq() noexcept{
	_pendingReqs.put(_incomming.writeReq);
	}

void	Driver::deferFlushReq() noexcept{
	_pendingReqs.put(_incomming.flushReq);
	}

void	Driver::deferReadReq() noexcept{
	_pendingReqs.put(_incomming.readReq);
	}

void	Driver::startRead() noexcept{
	_currentReq.readReq	= _incomming.readReq;
	Oscl::Block::Read::
	ITC::Req::Api<512>::ReadPayload&
	srcPayload	= _currentReq.readReq->_payload;
	Oscl::Block::Read::
	ITC::Req::Api<512>::ReadPayload*
	payload	= new(&_respMem.read._read.payload)
				Oscl::Block::Read::ITC::Req::Api<512>::
				ReadPayload(	srcPayload._block+_lbaOffset,
								srcPayload._nBlocks,
								srcPayload._blocks
								);
	Oscl::Block::Read::
	ITC::Resp::Api<512>::ReadResp*
	resp	= new(&_respMem.read._read.resp)
				Oscl::Block::Read::
				ITC::Resp::Api<512>::
				ReadResp(	_readSAP.getReqApi(),
							*this,
							_myPapi,
							*payload
							);
	_readSAP.post(resp->getSrvMsg());
	}

void	Driver::startWrite() noexcept{
	_currentReq.writeReq	= _incomming.writeReq;
	Oscl::Block::Write::
	ITC::Req::Api<512>::WritePayload&
	srcPayload	= _currentReq.writeReq->_payload;
	Oscl::Block::Write::
	ITC::Req::Api<512>::WritePayload*
	payload	= new(&_respMem.write._write.payload)
				Oscl::Block::Write::ITC::Req::Api<512>::
				WritePayload(	srcPayload._block+_lbaOffset,
								srcPayload._nBlocks,
								srcPayload._blocks
								);
	Oscl::Block::Write::
	ITC::Resp::Api<512>::WriteResp*
	resp	= new(&_respMem.write._write.resp)
				Oscl::Block::Write::
				ITC::Resp::Api<512>::
				WriteResp(	_writeSAP.getReqApi(),
							*this,
							_myPapi,
							*payload
							);
	_writeSAP.post(resp->getSrvMsg());
	}

void	Driver::startFlush() noexcept{
	_currentReq.flushReq	= _incomming.flushReq;
	Oscl::Block::Write::
	ITC::Req::Api<512>::FlushPayload&
	srcPayload	= _currentReq.flushReq->_payload;
	Oscl::Block::Write::
	ITC::Req::Api<512>::FlushPayload*
	payload	= new(&_respMem.write._flush.payload)
				Oscl::Block::Write::ITC::Req::Api<512>::
				FlushPayload(	srcPayload._block+_lbaOffset,
								srcPayload._nBlocks
								);
	Oscl::Block::Write::
	ITC::Resp::Api<512>::FlushResp*
	resp	= new(&_respMem.write._flush.resp)
				Oscl::Block::Write::
				ITC::Resp::Api<512>::
				FlushResp(	_writeSAP.getReqApi(),
							*this,
							_myPapi,
							*payload
							);
	_writeSAP.post(resp->getSrvMsg());
	}

void	Driver::returnReadReq() noexcept{
	_currentReq.readReq->returnToSender();
	_currentReq.readReq	= 0;
	}

void	Driver::returnWriteReq() noexcept{
	_currentReq.writeReq->returnToSender();
	_currentReq.writeReq	= 0;
	}

void	Driver::returnFlushReq() noexcept{
	_currentReq.flushReq->returnToSender();
	_currentReq.flushReq	= 0;
	}

void	Driver::returnIncommingReadCancelReq() noexcept{
	_incomming.readCancelReq->returnToSender();
	_incomming.readCancelReq	= 0;
	}

void	Driver::returnIncommingWriteCancelReq() noexcept{
	_incomming.writeCancelReq->returnToSender();
	_incomming.writeCancelReq	= 0;
	}

void	Driver::returnIncommingFlushCancelReq() noexcept{
	returnIncommingWriteCancelReq();
	}

void	Driver::returnReadCancelReq() noexcept{
	_cancel.readCancelReq->returnToSender();
	_cancel.readCancelReq	= 0;
	}

void	Driver::returnWriteCancelReq() noexcept{
	_cancel.writeCancelReq->returnToSender();
	_cancel.writeCancelReq	= 0;
	}

void	Driver::returnFlushCancelReq() noexcept{
	returnWriteCancelReq();
	}

void	Driver::cancelRead() noexcept{
	Oscl::Block::Read::
	ITC::Req::Api<512>::CancelPayload*
	payload	= new(&_cancelMem.read.payload)
				Oscl::Block::Read::
				ITC::Req::Api<512>::
				CancelPayload(_currentResp.readResp->getSrvMsg());

	Oscl::Block::Read::
	ITC::Resp::Api<512>::CancelResp*
	resp	= new(&_cancelMem.read.resp)
				Oscl::Block::Read::
				ITC::Resp::Api<512>::
				CancelResp(	_readSAP.getReqApi(),
							*this,
							_myPapi,
							*payload
							);
	_currentResp.readCancelResp	= resp;
	_cancel.readCancelReq	= _incomming.readCancelReq;
	_readSAP.post(resp->getSrvMsg());
	}

void	Driver::cancelWrite() noexcept{
	Oscl::Block::Write::
	ITC::Req::Api<512>::CancelPayload*
	payload	= new(&_cancelMem.write.payload)
				Oscl::Block::Write::
				ITC::Req::Api<512>::
				CancelPayload(_currentResp.writeResp->getSrvMsg());

	Oscl::Block::Write::
	ITC::Resp::Api<512>::CancelResp*
	resp	= new(&_cancelMem.write.resp)
				Oscl::Block::Write::
				ITC::Resp::Api<512>::
				CancelResp(	_writeSAP.getReqApi(),
							*this,
							_myPapi,
							*payload
							);
	_currentResp.writeCancelResp	= resp;
	_cancel.writeCancelReq	= _incomming.writeCancelReq;
	_writeSAP.post(resp->getSrvMsg());
	}

void	Driver::cancelFlush() noexcept{
	Oscl::Block::Write::
	ITC::Req::Api<512>::CancelPayload*
	payload	= new(&_cancelMem.write.payload)
				Oscl::Block::Write::
				ITC::Req::Api<512>::
				CancelPayload(_currentResp.flushResp->getSrvMsg());

	Oscl::Block::Write::
	ITC::Resp::Api<512>::CancelResp*
	resp	= new(&_cancelMem.write.resp)
				Oscl::Block::Write::
				ITC::Resp::Api<512>::
				CancelResp(	_writeSAP.getReqApi(),
							*this,
							_myPapi,
							*payload
							);
	_currentResp.writeCancelResp	= resp;
	_cancel.writeCancelReq	= _incomming.writeCancelReq;
	_writeSAP.post(resp->getSrvMsg());
	}

void	Driver::finishRead() noexcept{
	_currentReq.readReq->_payload._result	= _incomming.readResp->_payload._result;
	}

void	Driver::finishWrite() noexcept{
	_currentReq.writeReq->_payload._result	= _incomming.writeResp->_payload._result;
	}

void	Driver::finishFlush() noexcept{
	_currentReq.flushReq->_payload._result	= _incomming.flushResp->_payload._result;
	}

bool	Driver::deferQueueEmpty() noexcept{
	return !_pendingReqs.first();
	}

bool	Driver::isIncommingCancelForCurrentRead() noexcept{
	Oscl::Mt::Itc::SrvMsg*	current = _currentReq.readReq;
	return &_incomming.readCancelReq->_payload._msgToCancel == current;
	}

bool	Driver::isIncommingCancelForCurrentWrite() noexcept{
	Oscl::Mt::Itc::SrvMsg*	current = _currentReq.writeReq;
	return &_incomming.writeCancelReq->_payload._msgToCancel == current;
	}

bool	Driver::isIncommingCancelForCurrentFlush() noexcept{
	Oscl::Mt::Itc::SrvMsg*	current = _currentReq.flushReq;
	return &_incomming.writeCancelReq->_payload._msgToCancel == current;
	}

