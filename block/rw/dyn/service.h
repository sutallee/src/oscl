/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_block_rw_dyn_serviceh_
#define _oscl_block_rw_dyn_serviceh_
#include "oscl/block/rw/itc/api.h"
#include "oscl/block/rw/api.h"
#include "oscl/mt/itc/dyn/adv/core.h"
#include "oscl/oid/fixed.h"
#include "oscl/mt/itc/mbox/srvevt.h"
#include "oscl/mt/itc/srv/closeapi.h"
#include "oscl/mt/itc/srv/close.h"

/** */
namespace Oscl {
/** */
namespace Block {
/** */
namespace RW {
/** */
namespace Dyn {

/** */
class Service :	public Oscl::Mt::Itc::Dyn::Adv::Core<Service> {
	public:
		/** */
		using Oscl::Mt::Itc::Dyn::Adv::Core<Service>::request;

	public:
		enum BlockType {
			raw,
			fat,
			lbaFat,
			extended,
			lbaExtended
			};
	private:
		/** */
		class LockPayload {};
		/** */
		class UnlockPayload {};
		/** */
		typedef Oscl::Mt::Itc::SrvEvent<Service,LockPayload>	LockReq;
		/** */
		typedef Oscl::Mt::Itc::SrvEvent<Service,UnlockPayload>	UnlockReq;

	private:
		/** */
		Oscl::Mt::Itc::PostMsgApi&			_myPapi;
		/** */
		Oscl::Block::RW::ITC::Api<512>&		_api;
		/** */
		Oscl::Block::RW::Api<512>&			_syncApi;
		/** */
		Oscl::Mt::Itc::Srv::
		OpenCloseSyncApi&					_ocApi;
		/** */
        Oscl::Mt::Itc::Srv::
		Close::Req::Api::SAP&				_ocSAP;
		/** */
		const BlockType						_blockType;
		/** */
		const unsigned long					_lengthInBlocks;

	public:
		/** */
		enum{ObjectIDSize=32};	// FIXME
		/** */
		Oscl::ObjectID::Fixed<ObjectIDSize>	_location;

	private:
		/** */
		Oscl::Queue<Oscl::Mt::Itc::SrvMsg>	_pendingLocks;

		/** */
		bool								_locked;
		/** */
		bool								_reserved;

	public:
		/** */
		Service(	Oscl::Mt::Itc::PostMsgApi&		myPapi,
					Oscl::Mt::Itc::Dyn::Adv::
					Core<Service>::ReleaseApi&		advRapi,
					Oscl::Mt::Itc::PostMsgApi&		advPapi,
					Oscl::Block::RW::ITC::Api<512>&	api,
					Oscl::Block::RW::Api<512>&		syncApi,
					Oscl::Mt::Itc::Srv::
					OpenCloseSyncApi&				ocApi,
        			Oscl::Mt::Itc::Srv::
					Close::Req::Api::SAP&			ocSAP,
					BlockType						blockType,
					unsigned long					lengthInBlocks,
					const Oscl::ObjectID::RO::Api*	location=0
					) noexcept;
		/** */
		Oscl::Block::RW::
		ITC::Api<512>&				getApi() noexcept;
		/** */
		Oscl::Block::RW::
		Api<512>&					getSyncApi() noexcept;

		/** */
		void						lock() noexcept;
		/** */
		void						unlock() noexcept;
		/** */
		bool						isReserved() noexcept;
		/** */
		void						reserve() noexcept;
		/** */
		BlockType					type() const noexcept;						
		/** */
		unsigned long				lengthInBlocks() const noexcept;						

	private: // Oscl::Mt::Itc::Dyn::Adv::Core
		/** */
		void	openDynamicService() noexcept;
		/** */
		void	closeDynamicService() noexcept;
		/** */
		Service&	getDynSrv() noexcept;

	private:
		/** */
		friend class Oscl::Mt::Itc::SrvEvent<Service,LockPayload>;
		/** */
		friend class Oscl::Mt::Itc::SrvEvent<Service,UnlockPayload>;
		/** */
		void	request(LockReq& msg) noexcept;
		/** */
		void	request(UnlockReq& msg) noexcept;
	};

}
}
}
}

#endif
