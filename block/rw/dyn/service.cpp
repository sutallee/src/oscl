/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "service.h"
#include "oscl/mt/itc/mbox/syncrh.h"
#include "oscl/error/fatal.h"

using namespace Oscl::Block::RW::Dyn;

Service::Service(	Oscl::Mt::Itc::PostMsgApi&			myPapi,
					Oscl::Mt::Itc::Dyn::Adv::
					Core<Service>::ReleaseApi&			advRapi,
					Oscl::Mt::Itc::PostMsgApi&			advPapi,
					Oscl::Block::RW::ITC::Api<512>&		api,
					Oscl::Block::RW::Api<512>&			syncApi,
					Oscl::Mt::Itc::Srv::
					OpenCloseSyncApi&					ocApi,
        			Oscl::Mt::Itc::Srv::
					Close::Req::Api::SAP&				ocSAP,
					BlockType							blockType,
					unsigned long						lengthInBlocks,
					const Oscl::ObjectID::RO::Api*		location
					) noexcept:
		Oscl::Mt::Itc::Dyn::Adv::Core<Service>(myPapi,advRapi,advPapi),
		_myPapi(myPapi),
		_api(api),
		_syncApi(syncApi),
		_ocApi(ocApi),
		_ocSAP(ocSAP),
		_blockType(blockType),
		_lengthInBlocks(lengthInBlocks),
		_locked(false),
		_reserved(false)
		{
	if(location){
		static_cast<Oscl::ObjectID::Api&>(_location)	= *location;
		}
	}

Oscl::Block::RW::
ITC::Api<512>&			Service::getApi() noexcept{
	return _api;
	}

Oscl::Block::RW::
Api<512>&				Service::getSyncApi() noexcept{
	return _syncApi;
	}

void	Service::lock() noexcept{
	Oscl::Mt::Itc::SyncReturnHandler	srh;
	LockReq								req(*this,srh);
	_myPapi.postSync(req);
	}

void	Service::unlock() noexcept{
	Oscl::Mt::Itc::SyncReturnHandler	srh;
	UnlockReq							req(*this,srh);
	_myPapi.postSync(req);
	}

bool	Service::isReserved() noexcept{
	return _reserved;
	}

void	Service::reserve() noexcept{
	if(_reserved){
		Oscl::ErrorFatal::
			logAndExit(	"Oscl::Block::RW::Dyn::Service::reserve()"
						"Programmer error:"
						"device already reserved."
						);
		}
	_reserved	= true;
	}

Service::BlockType	Service::type() const noexcept{
	return _blockType;
	}

unsigned long	Service::lengthInBlocks() const noexcept{
	return _lengthInBlocks;
	}

void	Service::openDynamicService() noexcept{
	_ocApi.syncOpen();
	}

void	Service::closeDynamicService() noexcept{
	_ocApi.syncClose();
	}

Service&	Service::getDynSrv() noexcept{
	return *this;
	}

void	Service::request(LockReq& msg) noexcept{
	if(_locked){
		_pendingLocks.put(&msg);
		return;
		}
	_locked	= true;
	msg.returnToSender();
	}

void	Service::request(UnlockReq& msg) noexcept{
	_locked	= false;
	msg.returnToSender();
	Oscl::Mt::Itc::SrvMsg*	req	= _pendingLocks.get();
	if(req){
		req->process();
		}
	}

