/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_block_read_itc_reqapih_
#define _oscl_block_read_itc_reqapih_
#include "oscl/mt/itc/mbox/srvreq.h"
#include "oscl/mt/itc/mbox/sap.h"
#include "oscl/memory/block.h"

/** */
namespace Oscl {
/** */
namespace Block {
/** */
namespace Read {

/** */
namespace Status{
/** */
class Result;
}

/** */
namespace ITC {
/** */
namespace Req {

using namespace Oscl::Mt::Itc;

/** */
template <unsigned blockSize>
class Api {
	public:
		/** */
		virtual ~Api() {}
		/** */
		class CancelPayload {
			public:
				/**	*/
				Oscl::Mt::Itc::SrvMsg&	_msgToCancel;
			public:
				/** */
				CancelPayload(Oscl::Mt::Itc::SrvMsg& msgToCancel) noexcept;
			};
		/** */
		class ReadPayload {
			public:
				/**	*/
				const unsigned long						_block;
				/** */
				const unsigned long						_nBlocks;
				/** */
				Oscl::Memory::AlignedBlock<blockSize>*	_blocks;
				/** Result pointer is ZERO if the operation completed
					without error. Otherwise, a pointer to result
					code is returned. The reference can be used
					to determine the reason for the failure.
				 */
				const Oscl::Block::Read::Status::Result*	_result;

			public:
				/** */
				ReadPayload(	unsigned long							block,
								unsigned long							nBlocks,
								Oscl::Memory::AlignedBlock<blockSize>*	blocks
								) noexcept;
			};
	public:
		/** */
		typedef SrvRequest<Api,ReadPayload>			ReadReq;
		/** */
		typedef SrvRequest<Api,CancelPayload>		CancelReq;

	public:
		/** */
		typedef Oscl::Mt::Itc::SAP<Api>				SAP;
		/** */
		typedef Oscl::Mt::Itc::ConcreteSAP<Api>		ConcreteSAP;

	public:
		/** */
		virtual void	request(ReadReq& msg) noexcept=0;
		/** */
		virtual void	request(CancelReq& msg) noexcept=0;
	};

template <unsigned blockSize>
Api<blockSize>::ReadPayload::
ReadPayload(	unsigned long	block,
				unsigned long	nBlocks,
				Oscl::Memory::AlignedBlock<blockSize>*	blocks
				) noexcept:
		_block(block),
		_nBlocks(nBlocks),
		_blocks(blocks),
		_result(0)
		{
	}

template <unsigned blockSize>
Api<blockSize>::CancelPayload::
CancelPayload(Oscl::Mt::Itc::SrvMsg& msgToCancel) noexcept:
		_msgToCancel(msgToCancel)
		{
	}

}
}
}
}
}

#endif
