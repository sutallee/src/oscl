/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_block_write_itc_respapih_
#define _oscl_block_write_itc_respapih_
#include "oscl/mt/itc/mbox/clirsp.h"
#include "reqapi.h"

/** */
namespace Oscl {
/** */
namespace Block {
/** */
namespace Write {
/** */
namespace ITC {
/** */
namespace Resp {

/** */
template <unsigned blockSize>
class Api {
	public:
		/** */
		typedef Oscl::Mt::Itc::
				CliResponse<	typename Req::Api<blockSize>,
								Api,
								typename Req::Api<blockSize>::WritePayload
								>	WriteResp;
		/** */
		typedef Oscl::Mt::Itc::
				CliResponse<	typename Req::Api<blockSize>,
								Api,
								typename Req::Api<blockSize>::FlushPayload
								>	FlushResp;
		/** */
		typedef Oscl::Mt::Itc::
				CliResponse<	typename Req::Api<blockSize>,
								Api,
								typename Req::Api<blockSize>::CancelPayload
								>	CancelResp;
	public:
		/** */
		virtual ~Api() {}
		/** */
		virtual void	response(WriteResp& msg) noexcept=0;
		/** */
		virtual void	response(FlushResp& msg) noexcept=0;
		/** */
		virtual void	response(CancelResp& msg) noexcept=0;
	};

}
}
}
}
}

#endif
