/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_block_write_itc_synch_
#define _oscl_block_write_itc_synch_
#include "reqapi.h"
#include "oscl/block/write/api.h"
#include "oscl/mt/itc/mbox/syncrh.h"

/** */
namespace Oscl {
/** */
namespace Block {
/** */
namespace Write {
/** */
namespace ITC {

using namespace Oscl::Mt::Itc;

/** */
template <unsigned blockSize>
class Sync : public Oscl::Block::Write::Api<blockSize> {
	private:
		/** */
		typename Oscl::Block::Write::ITC::Req::Api<blockSize>::ConcreteSAP	_sap;

	public:
		/** */
		Sync(	Oscl::Block::Write::ITC::Req::Api<blockSize>&	reqApi,
				Oscl::Mt::Itc::PostMsgApi&	myPapi
				) noexcept;
		/** */
		typename Oscl::Block::Write::ITC::Req::Api<blockSize>::SAP&	getSAP() noexcept;

	public:	// Oscl::Block::Write::Api
		/** */
		const Status::Result*
			write(	unsigned long									block,
					unsigned long									nBlocks,
					const Oscl::Memory::AlignedBlock<blockSize>*	blocks
					) noexcept;
		/** */
		const Status::Result*
			flush(	unsigned long	block,
					unsigned long	nBlocks
					) noexcept;

	};

template <unsigned blockSize>
Sync<blockSize>::
Sync(	typename Oscl::Block::Write::ITC::Req::Api<blockSize>&	reqApi,
		Oscl::Mt::Itc::PostMsgApi&								myPapi
			) noexcept:
		_sap(reqApi,myPapi)
		{
	}

template <unsigned blockSize>
typename Oscl::Block::Write::ITC::
Req::Api<blockSize>::SAP&	Sync<blockSize>::getSAP() noexcept{
	return _sap;
	}

template <unsigned blockSize>
const Oscl::Block::Write::Status::Result*
Sync<blockSize>::write(	unsigned long									block,
						unsigned long									nBlocks,
						const Oscl::Memory::AlignedBlock<blockSize>*	blocks
						) noexcept{
	Oscl::Mt::Itc::SyncReturnHandler	srh;
	typename Req::Api<blockSize>::WritePayload	payload(	block,
															nBlocks,
															blocks
															);
	typename Req::Api<blockSize>::WriteReq		req(	_sap.getReqApi(),
														payload,
														srh
														);
	_sap.postSync(req);
	return payload._result;
	}

template <unsigned blockSize>
const Oscl::Block::Write::Status::Result*
Sync<blockSize>::flush(	unsigned long	block,
						unsigned long	nBlocks
						) noexcept{
	Oscl::Mt::Itc::SyncReturnHandler	srh;
	typename Req::Api<blockSize>::FlushPayload	payload(	block,
															nBlocks
															);
	typename Req::Api<blockSize>::FlushReq		req(	_sap.getReqApi(),
														payload,
														srh
														);
	_sap.postSync(req);
	return payload._result;
	}

}
}
}
}

#endif
