/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_block_write_itc_respmemh_
#define _oscl_block_write_itc_respmemh_
#include "respapi.h"
#include "oscl/memory/block.h"


/** */
namespace Oscl {
/** */
namespace Block {
/** */
namespace Write {
/** */
namespace ITC {
/** */
namespace Resp {

/** */
template <unsigned blockSize>
struct WriteMem {
	/** */
	Oscl::Memory::
	AlignedBlock<sizeof( typename Resp::Api<blockSize>::WriteResp)>	resp;
	/** */
	Oscl::Memory::
	AlignedBlock<sizeof(typename Req::Api<blockSize>::WritePayload)>	payload;
	};

/** */
template <unsigned blockSize>
struct FlushMem {
	/** */
	Oscl::Memory::
	AlignedBlock<sizeof( typename Resp::Api<blockSize>::FlushResp)>	resp;
	/** */
	Oscl::Memory::
	AlignedBlock<sizeof(typename Req::Api<blockSize>::FlushPayload)>	payload;
	};

/** */
template <unsigned blockSize>
struct CancelMem {
	/** */
	Oscl::Memory::
	AlignedBlock<sizeof( typename Resp::Api<blockSize>::CancelResp)>	resp;
	/** */
	Oscl::Memory::
	AlignedBlock<sizeof(typename Req::Api<blockSize>::CancelPayload)>	payload;
	};

/** */
template <unsigned blockSize>
union Mem {
	/** */
	void*						__qitemlink;
	/** */
	WriteMem<blockSize>			_write;
	/** */
	FlushMem<blockSize>			_flush;
	};

}
}
}
}
}

#endif
