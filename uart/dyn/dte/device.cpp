/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "device.h"
#include "oscl/mt/itc/mbox/syncrh.h"
#include "oscl/error/fatal.h"

using namespace Oscl::UART::Dyn::DTE;

Device::Device(	Oscl::Mt::Itc::PostMsgApi&		myPapi,
				Oscl::Mt::Itc::Dyn::Adv::
				Core<Device>::ReleaseApi&		advRapi,
				Oscl::Mt::Itc::PostMsgApi&		advPapi,
				Oscl::UART::DTE::Api&			dte,
				const Oscl::ObjectID::RO::Api*	location
				) noexcept:
		Oscl::Mt::Itc::Dyn::Adv::Core<Device>(myPapi,advRapi,advPapi),
		_myPapi(myPapi),
		_dte(dte),
		_locked(false),
		_reserved(false)
		{
	if(location){
		static_cast<Oscl::ObjectID::Api&>(_location)	= *location;
		}
	}

void	Device::lock() noexcept{
	Oscl::Mt::Itc::SyncReturnHandler	srh;
	LockReq								req(*this,srh);
	_myPapi.postSync(req);
	}

void	Device::unlock() noexcept{
	Oscl::Mt::Itc::SyncReturnHandler	srh;
	UnlockReq							req(*this,srh);
	_myPapi.postSync(req);
	}

bool	Device::isReserved() noexcept{
	return _reserved;
	}

void	Device::reserve() noexcept{
	if(_reserved){
		Oscl::ErrorFatal::
			logAndExit(	"Oscl::UART::Dyn::DTE::Device::reserve()"
						"Programmer error:"
						"device already reserved."
						);
		}
	_reserved	= true;
	}


void	Device::openDynamicService() noexcept{
	// nothing to register
	}

void	Device::closeDynamicService() noexcept{
	}

Device&	Device::getDynSrv() noexcept{
	return *this;
	}

void	Device::request(LockReq& msg) noexcept{
	if(_locked){
		_pendingLocks.put(&msg);
		return;
		}
	_locked	= true;
	msg.returnToSender();
	}

void	Device::request(UnlockReq& msg) noexcept{
	_locked	= false;
	msg.returnToSender();
	Oscl::Mt::Itc::SrvMsg*	req	= _pendingLocks.get();
	if(req){
		req->process();
		}
	}

