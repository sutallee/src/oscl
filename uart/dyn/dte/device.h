/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_uart_dyn_dte_deviceh_
#define _oscl_uart_dyn_dte_deviceh_
#include "oscl/mt/itc/dyn/adv/core.h"
#include "oscl/oid/fixed.h"
#include "oscl/mt/itc/mbox/srvevt.h"
#include "oscl/uart/dte/api.h"

/** */
namespace Oscl {
/** */
namespace UART {
/** */
namespace Dyn {
/** */
namespace DTE {

/** */
class Device :	public Oscl::Mt::Itc::Dyn::Adv::Core<Device> {
	private:
		/** */
		class LockPayload {};
		/** */
		class UnlockPayload {};
		/** */
		typedef Oscl::Mt::Itc::SrvEvent<Device,LockPayload>		LockReq;
		/** */
		typedef Oscl::Mt::Itc::SrvEvent<Device,UnlockPayload>	UnlockReq;

	public:
		/** */
		Oscl::Mt::Itc::PostMsgApi&			_myPapi;
		/** */
		enum{ObjectIDSize=32};	// FIXME
		/** */
		Oscl::ObjectID::Fixed<ObjectIDSize>	_location;
		/** */
		Oscl::UART::DTE::Api&				_dte;

	private:
		/** */
		Oscl::Queue<Oscl::Mt::Itc::SrvMsg>	_pendingLocks;
		/** */
		bool								_locked;
		/** */
		bool								_reserved;

	public:
		/** */
		Device(	Oscl::Mt::Itc::PostMsgApi&		myPapi,
				Oscl::Mt::Itc::Dyn::Adv::
				Core<Device>::ReleaseApi&		advRapi,
				Oscl::Mt::Itc::PostMsgApi&		advPapi,
				Oscl::UART::DTE::Api&			dte,
				const Oscl::ObjectID::RO::Api*	location = 0
				) noexcept;
		/** */
		void						lock() noexcept;
		/** */
		void						unlock() noexcept;
		/** */
		bool						isReserved() noexcept;
		/** */
		void						reserve() noexcept;

	private: // Oscl::Mt::Itc::Dyn::Adv::Core
		/** The implementation takes whatever action is necessary
			to "open" the sub-class. This typically entails binding
			to lower layer services. Function call semantics are
			assumed, and thus ITC must be done synchronously.
		 */
		void	openDynamicService() noexcept;
		/** The implementation takes whatever action is necessary
			to "close" the sub-class in preparation for subsequent
			destruction. Function call semantics are assumed,
			and thus ITC must be done synchronously.
		 */
		void	closeDynamicService() noexcept;
		/** */
		Device&	getDynSrv() noexcept;

	private:
		/** */
		friend class Oscl::Mt::Itc::SrvEvent<Device,LockPayload>;
		/** */
		friend class Oscl::Mt::Itc::SrvEvent<Device,UnlockPayload>;
		/** */
		void	request(LockReq& msg) noexcept;
		/** */
		void	request(UnlockReq& msg) noexcept;
	};

}
}
}
}

#endif
