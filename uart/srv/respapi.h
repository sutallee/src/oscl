/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_uart_srv_respapih_
#define _oscl_uart_srv_respapih_
#include "reqapi.h"
#include "oscl/mt/itc/srv/clirsp.h"

/** */
namespace Oscl {
/** */
namespace UART {
/** */
namespace Resp {

/** */
class Api {
	public:
		/** */
		Oscl::Mt::Itc::CliResponse<	Req::Api,
									Api,
									Req::SetBaudRatePayload
									>	SetBaudRateResp;
		/** */
		Oscl::Mt::Itc::CliResponse<	Req::Api,
									Api,
									Req::NoParityPayload
									>		NoParityResp;
		/** */
		Oscl::Mt::Itc::CliResponse<	Req::Api,
									Api,
									Req::EvenParityPayload
									>	EvenParityResp;
		/** */
		Oscl::Mt::Itc::CliResponse<	Req::Api,
									Api,
									Req::OddParityPayload
									>		OddParityResp;
		/** */
		Oscl::Mt::Itc::CliResponse<	Req::Api,
									Api,
									Req::MarkParityPayload
									>	MarkParityResp;
		/** */
		Oscl::Mt::Itc::CliResponse<	Req::Api,
									Api,
									Req::SpaceParityPayload
									>	SpaceParityResp;
		/** */
		Oscl::Mt::Itc::CliResponse<	Req::Api,
									Api,
									Req::SetStartBitsPayload
									>	SetStartBitsResp;
		/** */
		Oscl::Mt::Itc::CliResponse<	Req::Api,
									Api,
									Req::SetStopBitsPayload
									>	SetStopBitsResp;
		/** */
		Oscl::Mt::Itc::CliResponse<	Req::Api,
									Api,
									Req::RtsCtsFlowControlPayload
									>	RtsCtsFlowControlResp;
		/** */
		Oscl::Mt::Itc::CliResponse<	Req::Api,
									Api,
									Req::ManualFlowControlPayload
									>	ManualFlowControlResp;
		/** */
		Oscl::Mt::Itc::CliResponse<	Req::Api,
									Api,
									Req::DisableFlowControlPayload
									>	DisableFlowControlResp;
		/** */
		Oscl::Mt::Itc::CliResponse<	Req::Api,
									Api,
									Req::AssertRtsPayload
									>				AssertRtsResp;
		/** */
		Oscl::Mt::Itc::CliResponse<	Req::Api,
									Api,
									Req::NegateRtsPayload
									>				NegateRtsResp;
		/** */
		Oscl::Mt::Itc::CliResponse<	Req::Api,
									Api,
									Req::AssertCtsPayload
									>				AssertCtsResp;
		/** */
		Oscl::Mt::Itc::CliResponse<	Req::Api,
									Api,
									Req::NegateCtsPayload
									>				NegateCtsResp;
	public:
		/** */
		virtual void	response(SetBaudRateResp& msg) noexcept=0;
		/** */
		virtual void	response(NoParityResp& msg) noexcept=0;
		/** */
		virtual void	response(EvenParityResp& msg) noexcept=0;
		/** */
		virtual void	response(OddParityResp& msg) noexcept=0;
		/** */
		virtual void	response(MarkParityResp& msg) noexcept=0;
		/** */
		virtual void	response(SpaceParityResp& msg) noexcept=0;
		/** */
		virtual void	response(SetStartBitsResp& msg) noexcept=0;
		/** */
		virtual void	response(SetStopBitsResp& msg) noexcept=0;
		/** */
		virtual void	response(RtsCtsFlowControlResp& msg) noexcept=0;
		/** */
		virtual void	response(ManualFlowControlResp& msg) noexcept=0;
		/** */
		virtual void	response(DisableFlowControlResp& msg) noexcept=0;
		/** */
		virtual void	response(AssertRtsResp& msg) noexcept=0;
		/** */
		virtual void	response(NegateRtsResp& msg) noexcept=0;
		/** */
		virtual void	response(AssertCtsResp& msg) noexcept=0;
		/** */
		virtual void	response(NegateCtsResp& msg) noexcept=0;
	};
}
}
}

#endif
