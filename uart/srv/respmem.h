/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_uart_srv_respmemh_
#define _oscl_uart_srv_respmemh_
#include "respapi.h"
#include "oscl/memory/block.h"

/** */
namespace Oscl {
/** */
namespace UART {
/** */
namespace Resp {

/** */
struct SetBaudRateMem {
	/** */
	Oscl::Memory::AlignedBlock< sizeof(Req::SetBaudRatePayload) >	payload;
	/** */
	Oscl::Memory::AlignedBlock< sizeof(SetBaudRateResp) >		resp;
	};

/** */
struct NoParityMem {
	/** */
	Oscl::Memory::AlignedBlock< sizeof(Req::NoParityPayload) >	payload;
	/** */
	Oscl::Memory::AlignedBlock< sizeof(NoParityResp) >		resp;
	};

/** */
struct EvenParityMem {
	/** */
	Oscl::Memory::AlignedBlock< sizeof(Req::EvenParityPayload) >	payload;
	/** */
	Oscl::Memory::AlignedBlock< sizeof(EvenParityResp) >		resp;
	};

/** */
struct OddParityMem {
	/** */
	Oscl::Memory::AlignedBlock< sizeof(Req::OddParityPayload) >	payload;
	/** */
	Oscl::Memory::AlignedBlock< sizeof(OddParityResp) >		resp;
	};

/** */
struct MarkParityMem {
	/** */
	Oscl::Memory::AlignedBlock< sizeof(Req::MarkParityPayload) >	payload;
	/** */
	Oscl::Memory::AlignedBlock< sizeof(MarkParityResp) >		resp;
	};

/** */
struct SpaceParityMem {
	/** */
	Oscl::Memory::AlignedBlock< sizeof(Req::SpaceParityPayload) >	payload;
	/** */
	Oscl::Memory::AlignedBlock< sizeof(SpaceParityResp) >		resp;
	};

/** */
struct SetStartBitsMem {
	/** */
	Oscl::Memory::AlignedBlock< sizeof(Req::SetStartBitsPayload) >	payload;
	/** */
	Oscl::Memory::AlignedBlock< sizeof(SetStartBitsResp) >		resp;
	};

/** */
struct SetStopBitsMem {
	/** */
	Oscl::Memory::AlignedBlock< sizeof(Req::SetStopBitsPayload) >	payload;
	/** */
	Oscl::Memory::AlignedBlock< sizeof(SetStopBitsResp) >		resp;
	};

/** */
struct RtsCtsFlowControlMem {
	/** */
	Oscl::Memory::AlignedBlock< sizeof(Req::RtsCtsFlowControlPayload) >	payload;
	/** */
	Oscl::Memory::AlignedBlock< sizeof(RtsCtsFlowControlResp) >		resp;
	};

/** */
struct ManualFlowControlMem {
	/** */
	Oscl::Memory::AlignedBlock< sizeof(Req::ManualFlowControlPayload) >	payload;
	/** */
	Oscl::Memory::AlignedBlock< sizeof(ManualFlowControlResp) >		resp;
	};

/** */
struct DisableFlowControlMem {
	/** */
	Oscl::Memory::AlignedBlock< sizeof(Req::DisableFlowControlPayload) >	payload;
	/** */
	Oscl::Memory::AlignedBlock< sizeof(DisableFlowControlResp) >		resp;
	};

/** */
struct AssertRtsMem {
	/** */
	Oscl::Memory::AlignedBlock< sizeof(Req::AssertRtsPayload) >	payload;
	/** */
	Oscl::Memory::AlignedBlock< sizeof(AssertRtsResp) >		resp;
	};

/** */
struct NegateRtsMem {
	/** */
	Oscl::Memory::AlignedBlock< sizeof(Req::NegateRtsPayload) >	payload;
	/** */
	Oscl::Memory::AlignedBlock< sizeof(NegateRtsResp) >		resp;
	};

/** */
struct AssertCtsMem {
	/** */
	Oscl::Memory::AlignedBlock< sizeof(Req::AssertCtsPayload) >	payload;
	/** */
	Oscl::Memory::AlignedBlock< sizeof(AssertCtsResp) >		resp;
	};

/** */
struct NegateCtsMem {
	/** */
	Oscl::Memory::AlignedBlock< sizeof(Req::NegateCtsPayload) >	payload;
	/** */
	Oscl::Memory::AlignedBlock< sizeof(NegateCtsResp) >		resp;
	};

/** */
union Mem {
	void*	__qitemlink;
	SetBaudRateMem			setBaudRate;
	NoParityMem				noParity;
	EvenParityMem			evenParity;
	OddParityMem			oddParity;
	MarkParityMem			markParity;
	SpaceParityMem			spaceParity;
	SetStartBitsMem			setStartBits;
	SetStopBitsMem			setStopBits;
	RtsCtsFlowControlMem	rtsCtsFlowControl;
	ManualFlowControlMem	manualFlowControl;
	DisableFlowControlMem	disableFlowControl;
	AssertRtsMem			assertRts;
	NegateRtsMem			negateRts;
	AssertCtsMem			assertCts;
	NegateCtsMem			negateCts;
	};

}
}
}

#endif
