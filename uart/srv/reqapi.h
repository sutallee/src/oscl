/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_uart_srv_reqapih_
#define _oscl_uart_srv_reqapih_
#include "oscl/mt/itc/mbox/srvreq.h"
#include "oscl/mt/itc/mbox/sap.h"

/** */
namespace Oscl {
/** */
namespace UART {
/** */
namespace Req {

/** */
class SetBaudRatePayload {
	public:
		/** */
		const unsigned long	_bitsPerSecond;
		/** */
		bool				_success;
	public:
		/** */
		SetBaudRatePayload(unsigned long bitsPerSecond) noexcept;
	};

/** */
class NoParityPayload {
	public:
		/** */
		bool				_success;
	public:
		/** */
		NoParityPayload() noexcept;
	};

/** */
class EvenParityPayload {
	public:
		/** */
		bool				_success;
	public:
		/** */
		EvenParityPayload() noexcept;
	};

/** */
class OddParityPayload {
	public:
		/** */
		bool				_success;
	public:
		/** */
		OddParityPayload() noexcept;
	};

/** */
class MarkParityPayload {
	public:
		/** */
		bool				_success;
	public:
		/** */
		MarkParityPayload() noexcept;
	};

/** */
class SpaceParityPayload {
	public:
		/** */
		bool				_success;
	public:
		/** */
		SpaceParityPayload() noexcept;
	};

/** */
class SetStartBitsPayload {
	public:
		/** */
		const unsigned char	_nHalfBits;
		/** */
		bool				_success;
	public:
		/** */
		SetStartBitsPayload(unsigned char nHalfBits) noexcept;
	};

/** */
class SetStopBitsPayload {
	public:
		/** */
		const unsigned char	_nHalfBits;
		/** */
		bool				_success;
	public:
		/** */
		SetStopBitsPayload(unsigned char nHalfBits) noexcept;
	};

/** */
class RtsCtsFlowControlPayload {
	public:
		/** */
		bool				_success;
	public:
		/** */
		RtsCtsFlowControlPayload() noexcept;
	};

/** */
class ManualFlowControlPayload {
	public:
		/** */
		bool				_success;
	public:
		/** */
		ManualFlowControlPayload() noexcept;
	};

/** */
class DisableFlowControlPayload {
	public:
		/** */
		bool				_success;
	public:
		/** */
		DisableFlowControlPayload() noexcept;
	};

/** */
class AssertRtsPayload {
	public:
		/** */
		bool				_success;
	public:
		/** */
		AssertRtsPayload() noexcept;
	};

/** */
class NegateRtsPayload {
	public:
		/** */
		bool				_success;
	public:
		/** */
		NegateRtsPayload() noexcept;
	};

/** */
class AssertCtsPayload {
	public:
		/** */
		bool				_success;
	public:
		/** */
		AssertCtsPayload() noexcept;
	};

/** */
class NegateCtsPayload {
	public:
		/** */
		bool				_success;
	public:
		/** */
		NegateCtsPayload() noexcept;
	};

/** */
class DoThisPayload {
	public:
		/** */
		const char*			_cmd;
		/** */
		bool				_success;
	public:
		/** */
		DoThisPayload(const char* cmd) noexcept;
	};

/** */
class Api {
	public:
		/** */
		typedef Oscl::Mt::Itc::SrvRequest<	Api,
											SetBaudRatePayload
											>	SetBaudRateReq;
		/** */
		typedef Oscl::Mt::Itc::SrvRequest<	Api,
											NoParityPayload
											>		NoParityReq;
		/** */
		typedef Oscl::Mt::Itc::SrvRequest<	Api,
											EvenParityPayload
											>	EvenParityReq;
		/** */
		typedef Oscl::Mt::Itc::SrvRequest<	Api,
											OddParityPayload
											>		OddParityReq;
		/** */
		typedef Oscl::Mt::Itc::SrvRequest<	Api,
											MarkParityPayload
											>	MarkParityReq;
		/** */
		typedef Oscl::Mt::Itc::SrvRequest<	Api,
											SpaceParityPayload
											>	SpaceParityReq;
		/** */
		typedef Oscl::Mt::Itc::SrvRequest<	Api,
											SetStartBitsPayload
											>	SetStartBitsReq;
		/** */
		typedef Oscl::Mt::Itc::SrvRequest<	Api,
											SetStopBitsPayload
											>	SetStopBitsReq;
		/** */
		typedef Oscl::Mt::Itc::SrvRequest<	Api,
											RtsCtsFlowControlPayload
											>	RtsCtsFlowControlReq;
		/** */
		typedef Oscl::Mt::Itc::SrvRequest<	Api,
											ManualFlowControlPayload
											>	ManualFlowControlReq;
		/** */
		typedef Oscl::Mt::Itc::SrvRequest<	Api,
											DisableFlowControlPayload
											>	DisableFlowControlReq;
		/** */
		typedef Oscl::Mt::Itc::SrvRequest<	Api,
											AssertRtsPayload
											>				AssertRtsReq;
		/** */
		typedef Oscl::Mt::Itc::SrvRequest<	Api,
											NegateRtsPayload
											>				NegateRtsReq;
		/** */
		typedef Oscl::Mt::Itc::SrvRequest<	Api,
											AssertCtsPayload
											>				AssertCtsReq;
		/** */
		typedef Oscl::Mt::Itc::SrvRequest<	Api,
											NegateCtsPayload
											>				NegateCtsReq;
		/** */
		typedef Oscl::Mt::Itc::SrvRequest<	Api,
											DoThisPayload
											>				DoThisReq;
		/** */
		typedef Oscl::Mt::Itc::SAP<Api>				SAP;
		/** */
		typedef Oscl::Mt::Itc::ConcreteSAP<Api>		ConcreteSAP;
	public:
		/** Make GCC happy */
		virtual ~Api() {}
		/** */
		virtual void	request(SetBaudRateReq& msg) noexcept=0;
		/** */
		virtual void	request(NoParityReq& msg) noexcept=0;
		/** */
		virtual void	request(EvenParityReq& msg) noexcept=0;
		/** */
		virtual void	request(OddParityReq& msg) noexcept=0;
		/** */
		virtual void	request(MarkParityReq& msg) noexcept=0;
		/** */
		virtual void	request(SpaceParityReq& msg) noexcept=0;
		/** */
		virtual void	request(SetStartBitsReq& msg) noexcept=0;
		/** */
		virtual void	request(SetStopBitsReq& msg) noexcept=0;
		/** */
		virtual void	request(RtsCtsFlowControlReq& msg) noexcept=0;
		/** */
		virtual void	request(ManualFlowControlReq& msg) noexcept=0;
		/** */
		virtual void	request(DisableFlowControlReq& msg) noexcept=0;
		/** */
		virtual void	request(AssertRtsReq& msg) noexcept=0;
		/** */
		virtual void	request(NegateRtsReq& msg) noexcept=0;
		/** */
		virtual void	request(AssertCtsReq& msg) noexcept=0;
		/** */
		virtual void	request(NegateCtsReq& msg) noexcept=0;
		/** */
		virtual void	request(DoThisReq& msg) noexcept=0;
	};
}
}
}

#endif
