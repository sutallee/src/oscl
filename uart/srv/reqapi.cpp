/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "reqapi.h"

using namespace Oscl::UART::Req;

SetBaudRatePayload::SetBaudRatePayload(unsigned long bitsPerSecond) noexcept:
		_bitsPerSecond(bitsPerSecond)
		{
	}

NoParityPayload::NoParityPayload() noexcept
		{
	}

EvenParityPayload::EvenParityPayload() noexcept
		{
	}

OddParityPayload::OddParityPayload() noexcept
		{
	}

MarkParityPayload::MarkParityPayload() noexcept
		{
	}

SpaceParityPayload::SpaceParityPayload() noexcept
		{
	}

SetStartBitsPayload::SetStartBitsPayload(unsigned char nHalfBits) noexcept:
		_nHalfBits(nHalfBits)
		{
	}

SetStopBitsPayload::SetStopBitsPayload(unsigned char nHalfBits) noexcept:
		_nHalfBits(nHalfBits)
		{
	}

RtsCtsFlowControlPayload::RtsCtsFlowControlPayload() noexcept
		{
	}

ManualFlowControlPayload::ManualFlowControlPayload() noexcept
		{
	}

DisableFlowControlPayload::DisableFlowControlPayload() noexcept
		{
	}

AssertRtsPayload::AssertRtsPayload() noexcept
		{
	}

NegateRtsPayload::NegateRtsPayload() noexcept
		{
	}

AssertCtsPayload::AssertCtsPayload() noexcept
		{
	}

NegateCtsPayload::NegateCtsPayload() noexcept
		{
	}

DoThisPayload::DoThisPayload(const char* cmd) noexcept:
		_cmd(cmd)
		{
	}

