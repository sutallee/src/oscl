/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_uart_srv_synch_
#define _oscl_uart_srv_synch_
#include "reqapi.h"
#include "oscl/uart/api.h"
#include "oscl/stream/input/itc/sync.h"
#include "oscl/stream/output/itc/sync.h"

/** */
namespace Oscl {
/** */
namespace UART {

/** */
class Sync : public Oscl::UART::Api {
	private:
		/** */
		Oscl::Stream::Input::Sync				_streamIn;
		/** */
		Oscl::Stream::Output::Sync				_streamOut;
		/** */
		Req::Api::ConcreteSAP					_sap;

	public:
		/** */
		Sync(	Oscl::Stream::Input::Req::Api&		streamInReqApi,
				Oscl::Mt::Itc::PostMsgApi&			streamInPapi,
				Oscl::Stream::Output::Req::Api&		streamOutReqApi,
				Oscl::Mt::Itc::PostMsgApi&			streamOutPapi,
				Req::Api&							reqApi,
				Oscl::Mt::Itc::PostMsgApi&			myPapi
				) noexcept;
	public:
		/** */
		Oscl::Stream::Input::Req::Api::SAP&		inputSAP() noexcept;
		/** */
		Oscl::Stream::Output::Req::Api::SAP&	outputSAP() noexcept;
		/** */
		Oscl::Stream::Input::Api&	input() noexcept;
		/** */
		Oscl::Stream::Output::Api&	output() noexcept;

	public: // Baud rate configuration
		/** Returns true if successful, or false if the
			baud rate is not supported. Sets both the TX
			and RX baud rate if successful.
		 */
		bool	setBaudRate(unsigned long bitsPerSecond) noexcept;

	public: // Parity configuration
		/** Returns true if successful, or false if the
			UART does not support this parity mode.
		 */
		bool	noParity() noexcept;

		/** Returns true if successful, or false if the
			UART does not support this parity mode.
		 */
		bool	evenParity() noexcept;

		/** Returns true if successful, or false if the
			UART does not support this parity mode.
		 */
		bool	oddParity() noexcept;

		/** Returns true if successful, or false if the
			UART does not support this parity mode.
		 */
		bool	markParity() noexcept;

		/** Returns true if successful, or false if the
			UART does not support this parity mode.
		 */
		bool	spaceParity() noexcept;

	public: // Start/Stop bit configuration

		/** Returns true if successful, or false if the
			UART does not support the number of half
			bits. The nHalfBits argument specifies the
			number of half stop bits to use. E.g. 1.5 start
			bits are specified by a nHalfBits value of 3.
		 */
		bool	setStartBits(unsigned char nHalfBits) noexcept;

		/** Returns true if successful, or false if the
			UART does not support the number of half
			bits. The nHalfBits argument specifies the
			number of half stop bits to use. E.g. 1.5 stop
			bits are specified by a nHalfBits value of 3.
		 */
		bool	setStopBits(unsigned char nHalfBits) noexcept;

	public: // CTS/RTS flow control
		/** Returns true if successful, or false if automatic CTS/RTS
			flow control is not supported by the hardware.
		 */
		bool	rtsCtsFlowControl() noexcept;

		/** Returns true if successful, or false if manual
			CTS/RTS flow control is not supported by the hardware.
		 */
		bool	manualFlowControl() noexcept;

		/** Returns true if successful, or false if
			CTS/RTS flow control cannot be disabled.
			This mode expects software to deal with
			flow control issues.
		 */
		bool	disableFlowControl() noexcept;

		/** This operation may be used when manual flow
			control is enabled to assert the RTS signal.
			Returns true if successful or false if the
			flow control mode is NOT manual or the RTS
			line is not available for the hardware.
		 */
		bool	assertRTS() noexcept;

		/** This operation may be used when manual flow
			control is enabled to negate the RTS signal.
			Returns true if successful or false if the
			flow control mode is NOT manual or the RTS
			line is not available for the hardware.
		 */
		bool	negateRTS() noexcept;

		/** This operation may be used when manual flow
			control is enabled to assert the CTS signal.
			Returns true if successful or false if the
			flow control mode is NOT manual or the CTS
			line is not available for the hardware.
		 */
		bool	assertCTS() noexcept;

		/** This operation may be used when manual flow
			control is enabled to negate the CTS signal.
			Returns true if successful or false if the
			flow control mode is NOT manual or the CTS
			line is not available for the hardware.
		 */
		bool	negateCTS() noexcept;
	public:
		/** FIXME This is for development reverse engineering
			purposes ONLY.
		 */
		bool	doThis(const char* cmd) noexcept;
	};

}
}

#endif
