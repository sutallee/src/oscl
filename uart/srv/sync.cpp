/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "sync.h"
#include "oscl/mt/itc/mbox/syncrh.h"

using namespace Oscl::UART;

Sync::Sync(	Oscl::Stream::Input::Req::Api&		streamInReqApi,
			Oscl::Mt::Itc::PostMsgApi&			streamInPapi,
			Oscl::Stream::Output::Req::Api&		streamOutReqApi,
			Oscl::Mt::Itc::PostMsgApi&			streamOutPapi,
			Req::Api&							reqApi,
			Oscl::Mt::Itc::PostMsgApi&			myPapi
			) noexcept:
		_streamIn(streamInPapi,streamInReqApi),
		_streamOut(streamOutPapi,streamOutReqApi),
		_sap(reqApi,myPapi)
		{
	}

Oscl::Stream::Input::Req::Api::SAP&		Sync::inputSAP() noexcept{
	return _streamIn.getSAP();
	}

Oscl::Stream::Output::Req::Api::SAP&	Sync::outputSAP() noexcept{
	return _streamOut.getSAP();
	}

Oscl::Stream::Input::Api&	Sync::input() noexcept{
	return _streamIn;
	}

Oscl::Stream::Output::Api&	Sync::output() noexcept{
	return _streamOut;
	}

bool	Sync::setBaudRate(unsigned long bitsPerSecond) noexcept{
	Oscl::Mt::Itc::SyncReturnHandler	srh;
	Req::SetBaudRatePayload		payload(bitsPerSecond);
	Req::Api::SetBaudRateReq	req(	_sap.getReqApi(),
										payload,
										srh
										);
	_sap.postSync(req);
	return payload._success;
	}

bool	Sync::noParity() noexcept{
	Oscl::Mt::Itc::SyncReturnHandler	srh;
	Req::NoParityPayload				payload;
	Req::Api::NoParityReq	req(	_sap.getReqApi(),
									payload,
									srh
									);
	_sap.postSync(req);
	return payload._success;
	}

bool	Sync::evenParity() noexcept{
	Oscl::Mt::Itc::SyncReturnHandler	srh;
	Req::EvenParityPayload				payload;
	Req::Api::EvenParityReq	req(	_sap.getReqApi(),
									payload,
									srh
									);
	_sap.postSync(req);
	return payload._success;
	}

bool	Sync::oddParity() noexcept{
	Oscl::Mt::Itc::SyncReturnHandler	srh;
	Req::OddParityPayload				payload;
	Req::Api::OddParityReq	req(	_sap.getReqApi(),
									payload,
									srh
									);
	_sap.postSync(req);
	return payload._success;
	}

bool	Sync::markParity() noexcept{
	Oscl::Mt::Itc::SyncReturnHandler	srh;
	Req::MarkParityPayload				payload;
	Req::Api::MarkParityReq	req(	_sap.getReqApi(),
									payload,
									srh
									);
	_sap.postSync(req);
	return payload._success;
	}

bool	Sync::spaceParity() noexcept{
	Oscl::Mt::Itc::SyncReturnHandler	srh;
	Req::SpaceParityPayload				payload;
	Req::Api::SpaceParityReq	req(	_sap.getReqApi(),
										payload,
										srh
										);
	_sap.postSync(req);
	return payload._success;
	}

bool	Sync::setStartBits(unsigned char nHalfBits) noexcept{
	Oscl::Mt::Itc::SyncReturnHandler	srh;
	Req::SetStartBitsPayload			payload(nHalfBits);
	Req::Api::SetStartBitsReq	req(	_sap.getReqApi(),
										payload,
										srh
										);
	_sap.postSync(req);
	return payload._success;
	}

bool	Sync::setStopBits(unsigned char nHalfBits) noexcept{
	Oscl::Mt::Itc::SyncReturnHandler	srh;
	Req::SetStopBitsPayload				payload(nHalfBits);
	Req::Api::SetStopBitsReq	req(	_sap.getReqApi(),
										payload,
										srh
										);
	_sap.postSync(req);
	return payload._success;
	}

bool	Sync::rtsCtsFlowControl() noexcept{
	Oscl::Mt::Itc::SyncReturnHandler	srh;
	Req::RtsCtsFlowControlPayload		payload;
	Req::Api::RtsCtsFlowControlReq	req(	_sap.getReqApi(),
											payload,
											srh
											);
	_sap.postSync(req);
	return payload._success;
	}

bool	Sync::manualFlowControl() noexcept{
	Oscl::Mt::Itc::SyncReturnHandler	srh;
	Req::ManualFlowControlPayload		payload;
	Req::Api::ManualFlowControlReq	req(	_sap.getReqApi(),
											payload,
											srh
											);
	_sap.postSync(req);
	return payload._success;
	}

bool	Sync::disableFlowControl() noexcept{
	Oscl::Mt::Itc::SyncReturnHandler	srh;
	Req::DisableFlowControlPayload		payload;
	Req::Api::DisableFlowControlReq	req(	_sap.getReqApi(),
											payload,
											srh
											);
	_sap.postSync(req);
	return payload._success;
	}

bool	Sync::assertRTS() noexcept{
	Oscl::Mt::Itc::SyncReturnHandler	srh;
	Req::AssertRtsPayload				payload;
	Req::Api::AssertRtsReq	req(	_sap.getReqApi(),
									payload,
									srh
									);
	_sap.postSync(req);
	return payload._success;
	}

bool	Sync::negateRTS() noexcept{
	Oscl::Mt::Itc::SyncReturnHandler	srh;
	Req::NegateRtsPayload				payload;
	Req::Api::NegateRtsReq	req(	_sap.getReqApi(),
									payload,
									srh
									);
	_sap.postSync(req);
	return payload._success;
	}

bool	Sync::assertCTS() noexcept{
	Oscl::Mt::Itc::SyncReturnHandler	srh;
	Req::AssertCtsPayload				payload;
	Req::Api::AssertCtsReq	req(	_sap.getReqApi(),
									payload,
									srh
									);
	_sap.postSync(req);
	return payload._success;
	}

bool	Sync::negateCTS() noexcept{
	Oscl::Mt::Itc::SyncReturnHandler	srh;
	Req::NegateCtsPayload				payload;
	Req::Api::NegateCtsReq	req(	_sap.getReqApi(),
									payload,
									srh
									);
	_sap.postSync(req);
	return payload._success;
	}

bool	Sync::doThis(const char* cmd) noexcept{
	Oscl::Mt::Itc::SyncReturnHandler	srh;
	Req::DoThisPayload					payload(cmd);
	Req::Api::DoThisReq	req(	_sap.getReqApi(),
								payload,
								srh
								);
	_sap.postSync(req);
	return payload._success;
	}

