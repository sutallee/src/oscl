/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_uart_apih_
#define _oscl_uart_apih_
#include "oscl/stream/input/api.h"
#include "oscl/stream/output/api.h"
#include "oscl/stream/input/itc/reqapi.h"
#include "oscl/stream/output/itc/reqapi.h"

/** */
namespace Oscl {
/** */
namespace UART {

/** This interface represents a "standard" Universal Asynchronous
	Receiver Transmitter (UART). Included are operations to set
	features such as baud rate, parity, start/stop bits and other
	typical features. Since there is no such thing as a "standard"
	UART, each configuration operation may fail if the particular
	mode is not available. Also, it does not encopass all possible
	features, such as independent TX and RX baud rates.
	This interface is intended to be used to publish dynamic
	UART devices (e.g. PCI and USB) interfaces to the application
	layer, but may also be used for static devices.
 */
class Api {
	public:
		/** The "obligatory" virtual destructor. */
		virtual ~Api() {}

	public:
		/** */
		virtual Oscl::Stream::Input::Req::Api::SAP&		inputSAP() noexcept=0;
		/** */
		virtual Oscl::Stream::Output::Req::Api::SAP&	outputSAP() noexcept=0;
		/** */
		virtual Oscl::Stream::Input::Api&	input() noexcept=0;
		/** */
		virtual Oscl::Stream::Output::Api&	output() noexcept=0;

	public: // Baud rate configuration
		/** Returns true if successful, or false if the
			baud rate is not supported. Sets both the TX
			and RX baud rate if successful.
		 */
		virtual bool	setBaudRate(unsigned long bitsPerSecond) noexcept=0;

	public: // Parity configuration
		/** Returns true if successful, or false if the
			UART does not support this parity mode.
		 */
		virtual bool	noParity() noexcept=0;

		/** Returns true if successful, or false if the
			UART does not support this parity mode.
		 */
		virtual bool	evenParity() noexcept=0;

		/** Returns true if successful, or false if the
			UART does not support this parity mode.
		 */
		virtual bool	oddParity() noexcept=0;

		/** Returns true if successful, or false if the
			UART does not support this parity mode.
		 */
		virtual bool	markParity() noexcept=0;

		/** Returns true if successful, or false if the
			UART does not support this parity mode.
		 */
		virtual bool	spaceParity() noexcept=0;

	public: // Start/Stop bit configuration

		/** Returns true if successful, or false if the
			UART does not support the number of half
			bits. The nHalfBits argument specifies the
			number of half stop bits to use. E.g. 1.5 start
			bits are specified by a nHalfBits value of 3.
		 */
		virtual bool	setStartBits(unsigned char nHalfBits) noexcept=0;

		/** Returns true if successful, or false if the
			UART does not support the number of half
			bits. The nHalfBits argument specifies the
			number of half stop bits to use. E.g. 1.5 stop
			bits are specified by a nHalfBits value of 3.
		 */
		virtual bool	setStopBits(unsigned char nHalfBits) noexcept=0;

	public: // CTS/RTS flow control
		/** Returns true if successful, or false if automatic CTS/RTS
			flow control is not supported by the hardware.
		 */
		virtual bool	rtsCtsFlowControl() noexcept=0;

		/** Returns true if successful, or false if manual
			CTS/RTS flow control is not supported by the hardware.
		 */
		virtual bool	manualFlowControl() noexcept=0;

		/** Returns true if successful, or false if
			CTS/RTS flow control cannot be disabled.
			This mode expects software to deal with
			flow control issues.
		 */
		virtual bool	disableFlowControl() noexcept=0;

		/** This operation may be used when manual flow
			control is enabled to assert the RTS signal.
			Returns true if successful or false if the
			flow control mode is NOT manual or the RTS
			line is not available for the hardware.
		 */
		virtual bool	assertRTS() noexcept=0;

		/** This operation may be used when manual flow
			control is enabled to negate the RTS signal.
			Returns true if successful or false if the
			flow control mode is NOT manual or the RTS
			line is not available for the hardware.
		 */
		virtual bool	negateRTS() noexcept=0;

		/** This operation may be used when manual flow
			control is enabled to assert the CTS signal.
			Returns true if successful or false if the
			flow control mode is NOT manual or the CTS
			line is not available for the hardware.
		 */
		virtual bool	assertCTS() noexcept=0;

		/** This operation may be used when manual flow
			control is enabled to negate the CTS signal.
			Returns true if successful or false if the
			flow control mode is NOT manual or the CTS
			line is not available for the hardware.
		 */
		virtual bool	negateCTS() noexcept=0;
	public:
		/** FIXME This is for development reverse engineering
			purposes ONLY.
		 */
		virtual bool	doThis(const char* cmd) noexcept=0;
	};

}
}

#endif
