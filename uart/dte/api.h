/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_uart_dte_apih_
#define _oscl_uart_dte_apih_
#include "oscl/uart/api.h"
#include "oscl/boolstate/srv/reqapi.h"

/** */
namespace Oscl {
/** */
namespace UART {
/** */
namespace DTE {

/** This interface represents a "standard" Universal Asynchronous
	Receiver Transmitter (UART) for a DTE interface. In addition
	to the usual UART configuration parameters, this interface
	provides the means to manipulate and monitor DTE modem control
	signals.
 */
class Api {
	public:
		/** The "obligatory" virtual destructor. */
		virtual ~Api() {}

	public:
		/** */
		virtual Oscl::UART::Api&	getUartApi() noexcept=0;

	public: // Input Modem Control
		/** This operation returns a SAP that provides access
			to the state of the DCD modem control signal of the DTE.
		 */
		virtual Oscl::BoolState::Req::Api::SAP&	dcdSAP() noexcept=0;

		/** This operation returns a SAP that provides access
			to the state of the DSR modem control signal of the DTE.
		 */
		virtual Oscl::BoolState::Req::Api::SAP&	dsrSAP() noexcept=0;

	public: // Output Modem Control
		/** This operation is used to assert the DTR
			signal. Returns true if successful
			or false if the signal is not supported by
			the hardware.
		 */
		virtual bool	assertDTR() noexcept=0;

		/** This operation is used to negate the DTR
			signal. Returns true if successful
			or false if the signal is not supported by
			the hardware.
		 */
		virtual bool	negateDTR() noexcept=0;
	};

}
}
}

#endif
