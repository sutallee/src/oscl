/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_uart_dte_srv_synch_
#define _oscl_uart_dte_srv_synch_
#include "reqapi.h"
#include "oscl/uart/dte/api.h"
#include "oscl/uart/srv/sync.h"

/** */
namespace Oscl {
/** */
namespace UART {
/** */
namespace DTE {

/** */
class Sync : public Oscl::UART::DTE::Api {
	private:
		/** */
		Oscl::UART::Sync						_uartSync;
		/** */
		Oscl::BoolState::Req::Api::ConcreteSAP	_dcdSAP;
		/** */
		Oscl::BoolState::Req::Api::ConcreteSAP	_dsrSAP;
		/** */
		Req::Api::ConcreteSAP					_sap;

	public:
		/** */
		Sync(	Oscl::Stream::Input::Req::Api&		streamInReqApi,
				Oscl::Mt::Itc::PostMsgApi&			streamInPapi,
				Oscl::Stream::Output::Req::Api&		streamOutReqApi,
				Oscl::Mt::Itc::PostMsgApi&			streamOutPapi,
				Oscl::UART::Req::Api&				uartReqApi,
				Oscl::Mt::Itc::PostMsgApi&			uartPapi,
				Oscl::BoolState::Req::Api&			dcdReqApi,
				Oscl::Mt::Itc::PostMsgApi&			dcdPapi,
				Oscl::BoolState::Req::Api&			dsrReqApi,
				Oscl::Mt::Itc::PostMsgApi&			dsrPapi,
				Oscl::UART::DTE::Req::Api&			dteReqApi,
				Oscl::Mt::Itc::PostMsgApi&			dtePapi
				) noexcept;

	public:
		/** */
		Oscl::UART::Api&	getUartApi() noexcept;

	public: // Input Modem Control
		/** This operation returns a SAP that provides access
			to the state of the DCD modem control signal of the DTE.
		 */
		Oscl::BoolState::Req::Api::SAP&	dcdSAP() noexcept;

		/** This operation returns a SAP that provides access
			to the state of the DSR modem control signal of the DTE.
		 */
		Oscl::BoolState::Req::Api::SAP&	dsrSAP() noexcept;

	public: // Output Modem Control
		/** This operation is used to assert the DTR
			signal. Returns true if successful
			or false if the signal is not supported by
			the hardware.
		 */
		bool	assertDTR() noexcept;

		/** This operation is used to negate the DTR
			signal. Returns true if successful
			or false if the signal is not supported by
			the hardware.
		 */
		bool	negateDTR() noexcept;
	};

}
}
}

#endif
