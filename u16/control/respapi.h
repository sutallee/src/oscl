/*
   Copyright (C) 2020 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_u16_control_itc_respapih_
#define _oscl_u16_control_itc_respapih_

#include "reqapi.h"
#include "oscl/mt/itc/mbox/clirsp.h"

/** */
namespace Oscl {

/** */
namespace U16 {

/** */
namespace Control {

/** */
namespace Resp {

/**	This class describes an ITC client interface. The
	interface includes the definition of client response messages
	based on their corresponding server request messages and
	associated payloads, as well as an interface that is to be
	implemented by a client that uses the corresponding server
	asynchronously.
 */
/**	The symbiont attach/detach interface for the creator
	implemented by the host.
 */
class Api {
	public:
		/**	This describes a client response message that corresponds
			to the UpdateReq message described in the "reqapi.h"
			header file. This UpdateResp response message actually
			contains a UpdateReq request message.
		 */
		typedef Oscl::Mt::Itc::CliResponse<
					Oscl::U16::Control::Req::Api,
					Api,
					Oscl::U16::Control::
					Req::Api::UpdatePayload
					>		UpdateResp;

	public:
		/** Make the compiler happy. */
		virtual ~Api(){}

		/** This operation is invoked within the client thread
			whenever a UpdateResp message is received.
		 */
		virtual void	response(	Oscl::U16::Control::
									Resp::Api::UpdateResp& msg
									) noexcept=0;

	};

}
}
}
}
#endif
