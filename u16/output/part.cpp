/*
   Copyright (C) 2020 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "part.h"

using namespace Oscl::U16::Output;

Part::Part(
	Oscl::Mt::Itc::PostMsgApi&	papi,
	uint16_t					initialState
	) noexcept:
		_sap(
			*this,
			papi
			),
		_state(initialState)
		{
	}

Oscl::U16::Observer::Req::Api::SAP&	Part::getSAP() noexcept{
	return _sap;
	}

void	Part::notify(uint16_t state) noexcept{
	_state	= state;

	Oscl::U16::Observer::Req::Api::ChangeReq* note;

	while((note=_noteQ.get())){
		note->getPayload()._state	= state;
		note->returnToSender();
		}
	}

void Part::request(Oscl::U16::Observer::Req::Api::ChangeReq& msg) noexcept{
	if(_state == msg.getPayload()._state){
		_noteQ.put(&msg);
		}
	else{
		msg.getPayload()._state	= _state;
		msg.returnToSender();
		}
	}

void Part::request(Oscl::U16::Observer::Req::Api::CancelChangeReq& msg) noexcept{
	for(
		Oscl::U16::Observer::Req::Api::ChangeReq* next=_noteQ.first();
		next;
		next=_noteQ.next(next)
		){
		if(next == &msg.getPayload()._requestToCancel){
			_noteQ.remove(next);
			next->returnToSender();
			break;
			}
		}
	msg.returnToSender();
	}

void	Part::update(uint16_t state) noexcept{
	if(state != _state){
		notify(state);
		}
	}

