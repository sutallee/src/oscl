/*
   Copyright (C) 2020 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_u16_output_parth_
#define _oscl_u16_output_parth_

#include "oscl/u16/observer/reqapi.h"
#include "oscl/queue/queue.h"
#include "oscl/u16/control/api.h"

/** */
namespace Oscl {

/** */
namespace U16 {

/** */
namespace Output {

/** */
class Part :
		public Oscl::U16::Observer::Req::Api,
		public Oscl::U16::Control::Api
		{
	private:
		/** */
		Oscl::U16::Observer::Req::Api::ConcreteSAP	_sap;

		/** */
		Oscl::Queue<Oscl::U16::Observer::Req::Api::ChangeReq>	_noteQ;

		/** This variable contains the current state of the
			monitor point.
		 */
		uint16_t									_state;

	public:
		/** */
		Part(
			Oscl::Mt::Itc::PostMsgApi&	papi,
			uint16_t					initialState
			) noexcept;

		/** */
		Oscl::U16::Observer::Req::Api::SAP&	getSAP() noexcept;

	private:
		/** */
		void	notify(uint16_t state) noexcept;

	public: // Oscl::U16::Control::Api
		/** */
		void	update(uint16_t state) noexcept;

	public:	// Oscl::U16::Observer::Req::Api
		/** */
		void request(Oscl::U16::Observer::Req::Api::ChangeReq& msg) noexcept;

		/** */
		void request(Oscl::U16::Observer::Req::Api::CancelChangeReq& msg) noexcept;
	};

}
}
}

#endif
