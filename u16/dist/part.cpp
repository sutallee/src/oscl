/*
   Copyright (C) 2020 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "part.h"
#include "oscl/mt/itc/mbox/syncrh.h"

using namespace Oscl::U16::Dist;

Part::Part(
	Oscl::Mt::Itc::PostMsgApi&	papi,
	uint16_t					initialState
	) noexcept:
		_output(
			papi,
			initialState
			)
		{
	}

Oscl::U16::Observer::Req::Api::SAP&	Part::getOutputSAP() noexcept{
	return _output.getSAP();
	}

Oscl::U16::Control::Api&	Part::getLocalControlApi() noexcept{
	return _output;
	}

void Part::request(Control::Req::Api::UpdateReq& msg) noexcept{
	_output.update(msg.getPayload()._state);
	msg.returnToSender();
	}

void	Part::update(uint16_t state) noexcept{
	Oscl::U16::Control::Req::Api::UpdatePayload	payload(state);
	Oscl::Mt::Itc::SyncReturnHandler			srh;
	Oscl::U16::Control::Req::Api::UpdateReq		req(
													*this,
													payload,
													srh
													);
	_output.getSAP().postSync(req);
	}

