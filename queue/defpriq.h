/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_queue_defpriqh_
#define _oscl_queue_defpriqh_
#include "priorityq.h"

/** */
namespace Oscl {

/** This template class is used by the DeferedPriorityQueue class
	to implement a singly linked list with both priority insertion
	and the ability to execute a callback function"activate()"
	when the item is removed from the DeferedPriorityQueue via
	the doNext() member function.
 */
template <class qType>
	class DeferedPrioritizedItem : public PrioritizedItem<qType>{
		public:
			/** Make GCC happy */
			virtual ~DeferedPrioritizedItem() {}

			/** This function is called by the DeferedPriorityQueue when
				the doNext() member fuction causes this item to be removed
				from the head of the priority queue. Sub-classes must
				implement this pure virtual function.
			 */
			virtual void	activate(void) noexcept = 0;
	};

/** This template class extends PriorityQueue to implement a prioritized
	singly linked list with the additional ability to execute an item's callback
	function "activate()" when the item is removed from the queue via
	the doNext() member function.
 */
template <class qType>
	class DeferedPriorityQueue : public PriorityQueue<qType>{
		public:
			/** Uses the inherited get() member function and then
				invokes the activate() member function of the item
				retrieved from the queue. The item removed from the
				queue is returned as the result.
			 */
			qType	*doNext(void) noexcept;
		};

template <class qType>
	qType	*DeferedPriorityQueue<qType>::doNext(void) noexcept{
		qType* item;
		item						= this->get();
		if(item){
			item->activate();
			return(item);
			}
		return(0);
		}

};

#endif /*  _defpriqh_ */

