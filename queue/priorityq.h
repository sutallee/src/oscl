/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_krux_priorityq_priorityqh_
#define _oscl_krux_priorityq_priorityqh_
#include "oscl/queue/queue.h"

/** */
namespace Oscl {

/** This template class is used by the PriorityQueue class
	to implement a prioritized singly linked list.
 */
template <class qType>
	class PrioritizedItem : public QueueItem{
		public:
			/** Make GCC happy */
			virtual ~PrioritizedItem() {}
			/** Returns true if the priority of this item is lower than
				the priority of the "priority" argument.
				Since the concept of priority is dependent on
				the application and can be represented in many ways, this is a
				pure virtual function which must be implemented by sub-classes.
			 */
			virtual bool	isHigherPriority(qType* priority) noexcept =0;
			/** Returns true if the priority of this item is lower than the
				priority of the "priority" argument. Since the concept of priority
				is dependent on the application and can be represented in many ways,
				this is a pure virtual function which must be implemented by sub-classes.
			 */
			virtual bool	isLowerPriority(qType* priority) noexcept =0;
	};

/** This template class implements a linked list queue into which items can
	be inserted by priority.
 */
template <class qType>
	class PriorityQueue : public Queue<qType>{
		public:
			/** Threads the "priority" item into the PriorityQueue according
				to the priority rating of the item.
			 */
			void	insertByPriority(qType* priority) noexcept ;
		};

template <class qType>
	void	PriorityQueue<qType>::insertByPriority(qType* priority) noexcept {
		qType*	item;

		for(item=this->first();item;item=this->next(item)){
			if(item->isLowerPriority(priority)){
				this->insertBefore(item,priority);
				return;
				}
			}
		this->put(priority);
		}

};

#endif /*  _priorityqh_ */

