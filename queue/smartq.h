/*
   Copyright (C) 2023 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_queue_smartqh_
#define _oscl_queue_smartqh_

#include <memory>

/** */
namespace Oscl {

/** This template class implements a singly linked list
	which maintains a FIFO ordering.

	WARNING: The C++ smart pointers used here
	use dynamic memory allocation.
 */
template <class qType>
	class SmartQ {
		private:

			/** Points to the first QueueItem in the list.
			 */
			std::shared_ptr< qType >	_head;

			/** Points to the last QueueItem in the list.
			 */
			std::shared_ptr< qType >	_tail;

		private:
			/** This copy constructor is intentionally NOT implemented.
				This would create two list heads with pointers to the
				same elements. In such a case, one list would be
				inconsistent if the content of one were changed.
			 */
			explicit SmartQ(const SmartQ&) noexcept;

		public:

			/** Public constructor initializes head and tail pointers.
			 */
			SmartQ() noexcept;

			/** Specialized copy constructor that moves "other"
				queue contents to this queue. The "other" queue
				is left empty.
			 */
			SmartQ(SmartQ& other) noexcept;

			/** Public destructor
			 */
			~SmartQ() noexcept;

			/** This assignment operator moves the contents of the
				other queue into this queue. The other queue is
				emptied after the operation.
			 */
			SmartQ&	operator=(SmartQ& other) noexcept;

			/** Remove next element from queue and return a pointer to
				it as a result. qType must be a subclass of QueueItem.
			 */
			std::shared_ptr< qType >	get() noexcept;

			/** Remove last element from queue and return a pointer to
				it as a result. qType must be a subclass of QueueItem.
			 */
			std::shared_ptr< qType >	getLast() noexcept;

			/** Remove next element from beginning of the queue and
				return a pointer to it as a result. This operation
				does the same thing as the get() operation, but
				clarifies the use of the queue as a stack in combination
				with the push() operation.
				qType must be a subclass of QueueItem.
			 */
			std::shared_ptr< qType >	pop() noexcept;

			/** Thread into the linked list at the end of the FIFO queue.
				qType is a subclass of QueueItem.
			 */
			void	put( std::shared_ptr< qType > item ) noexcept;

			/** Thread into the linked list at the beginning of the FIFO queue.
				qType is a subclass of QueueItem.
			 */
			void	push(std::shared_ptr< qType > item) noexcept;

			/** Remove specified qType element from the queue.
				qType must be a subclass of QueueItem.
				Returns zero if the item is not found in
				the queue. Otherwise it returns item.
			 */
			std::shared_ptr< qType >	remove( qType* item ) noexcept;

			/** Insert the "item" qType into the queue behind the
				"after" qType element.
				qType must be a subclass of QueueItem.
			 */
			void	insertAfter( qType*	after, std::shared_ptr< qType > item ) noexcept;

			/** Insert the "item" qType into the queue ahead of the
				"before" qType element.
				qType must be a subclass of QueueItem.
			 */
			void	insertBefore( qType* before, std::shared_ptr< qType > item ) noexcept;

			/** Return a pointer to the first qType item in the queue.
				The returned item remains in the queue.
				qType is a subclass of QueueItem.
			 */
			qType*	first() const noexcept;

			/** Return a pointer to the last qType item in the queue.
				The returned item remains in the queue.
				qType is a subclass of QueueItem.
			 */
			qType*	last() const noexcept;

			/** Return a pointer to the qType item after the qType item "prev".
				Both items remain in the queue.
				qType is a subclass of QueueItem.
			 */
			qType*	next(const qType* prev) const noexcept;

			/** */
			std::shared_ptr< qType >	find( const qType* item ) const noexcept;
		};

template <class qType>
	SmartQ<qType>::SmartQ() noexcept:
		_head(nullptr),
		_tail(nullptr)
		{
	}

template <class qType>
	SmartQ< qType >::~SmartQ() noexcept {
		std::shared_ptr< qType >	next;
		while( ( next = get() ) ) {
			next.reset();
			}
	}

template <class qType>
SmartQ<qType>::SmartQ(SmartQ<qType>& other) noexcept{
	_head	= other._head;
	_tail	= other._tail;
	other._head	= nullptr;
	other._tail	= nullptr;
	}

template <class qType>
SmartQ<qType>&	SmartQ<qType>::operator=(SmartQ<qType>& other) noexcept{
	_head	= other._head;
	_tail	= other._tail;
	other._head	= nullptr;
	other._tail	= nullptr;
	return *this;
	}

template <class qType>
	inline std::shared_ptr< qType > SmartQ<qType>::get() noexcept{

		std::shared_ptr< qType >	next = _head;

		if( ( next != nullptr ) ){
			_head	= next->__qitemlink;
			}

		return next;
		}

template <class qType>
	inline std::shared_ptr< qType >	SmartQ<qType>::getLast() noexcept{
		qType*			lastInQ;
		lastInQ	= last();
		if(!lastInQ) return 0;
		return remove(lastInQ);
		}

template <class qType>
	inline std::shared_ptr< qType >	SmartQ<qType>::pop() noexcept{
		return get();
		}

template <class qType>
	inline void	SmartQ<qType>::put( std::shared_ptr< qType > item ) noexcept{
		if(_head){
			_tail->__qitemlink	= item;
			}
		else{
			_head	= item;
			}
		_tail		= item;
		item->__qitemlink	= nullptr;
		}

template <class qType>
	inline void	SmartQ<qType>::push( std::shared_ptr< qType > item ) noexcept{
		if(_head){
			item->__qitemlink	= _head;
			_head				= item;
			}
		else{
			_head				= _tail	= item;
			item->__qitemlink	= nullptr;
			}
		}

template <class qType>
	inline std::shared_ptr< qType >	SmartQ<qType>::remove( qType* item ) noexcept {

		std::shared_ptr< qType >	nxt;
		std::shared_ptr< qType >	prv;

		for( nxt = _head ; nxt ;prv = nxt, nxt = nxt->__qitemlink ){

			if( nxt.get() == item ){

				if( prv ){

					if( !( prv->__qitemlink = nxt->__qitemlink ) ){
						_tail	= prv;
						}
					}
				else{

					if( !( _head = nxt->__qitemlink ) ){
						_tail	= nullptr;
						}
					}
				return nxt;
				}
			}
		return nullptr;
		}

template <class qType>
	inline void	SmartQ<qType>::insertAfter( qType* prev, std::shared_ptr< qType > item ) noexcept{
		item->__qitemlink	= prev->__qitemlink;
		prev->__qitemlink	= item;
		}

template < class qType >
	inline void	SmartQ<qType>::insertBefore( qType* before, std::shared_ptr< qType > item ) noexcept{

		std::shared_ptr< qType >	nxt;
		std::shared_ptr< qType >	prv;

		for( nxt = _head ; nxt ;prv = nxt, nxt = nxt->__qitemlink ){

			if( nxt.get() == before ){

				item->__qitemlink	= nxt;

				if(prv){
					qType*	p	= prv.get();
					p->__qitemlink	= item;
//					prv.get()->__qitemlink	= item;
					}
				else{
					_head		= item;
					}

				break;
				}
			}
		}

template <class qType>
	inline qType* SmartQ<qType>::first() const noexcept{
		return _head.get();
		}

template <class qType>
	inline qType* SmartQ<qType>::last() const noexcept{
		if(_head){
			return _tail.get();
			}
		return 0;
		}

template <class qType>
	inline qType* SmartQ<qType>::next(const qType* prev) const noexcept{
		return prev->__qitemlink.get();
		}

template <class qType>
	inline std::shared_ptr< qType >	SmartQ<qType>::find( const qType* item ) const noexcept {

		std::shared_ptr< qType >	nxt;
		std::shared_ptr< qType >	prv;

		for( nxt = _head ; nxt ;prv = nxt, nxt = nxt->__qitemlink ){

			if( nxt.get() == item ){
				return nxt;
				}
			}
		return nullptr;
		}
}

#endif
