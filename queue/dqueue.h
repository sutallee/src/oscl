/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_queue_dqueueh_
#define _oscl_queue_dqueueh_

/** */
namespace Oscl {

/** This template class implements a singly linked list
	which maintains a FIFO ordering.
 */
template <class qType>
	class DQueue {
		private:

			/** Points to the first DQueueItem in the list.
			 */
			qType*	_head;

			/** Points to the last DQueueItem in the list.
			 */
			qType*	_tail;

		private:
			/** This copy constructor is intentionally NOT implemented.
				This would create two list heads with pointers to the
				same elements. In such a case, one list would be
				inconsistent if the content of one were changed.
			 */
			explicit DQueue(const DQueue&) noexcept;

		public:

			/** Public constructor initializes head and tail pointers.
			 */
			DQueue() noexcept;

			/** Specialized copy constructor that moves "other"
				queue contents to this queue. The "other" queue
				is left empty.
			 */
			DQueue(DQueue& other) noexcept;

			/** This assignment operator moves the contents of the
				other queue into this queue. The other queue is
				emptied after the operation.
			 */
			DQueue&	operator=(DQueue& other) noexcept;

			/** Remove next element from queue and return a pointer to
				it as a result. qType must be a subclass of DQueueItem.
			 */
			qType*	get(void) noexcept;

			/** Remove last element from queue and return a pointer to
				it as a result. qType must be a subclass of DQueueItem.
			 */
			qType*	getLast(void) noexcept;

			/** Remove next element from beginning of the queue and
				return a pointer to it as a result. This operation
				does the same thing as the get() operation, but
				clarifies the use of the queue as a stack in combination
				with the push() operation.
				qType must be a subclass of DQueueItem.
			 */
			qType*	pop(void) noexcept;

			/** Thread into the linked list at the end of the FIFO queue.
				qType is a subclass of DQueueItem.
			 */
			void	put(qType* item) noexcept;

			/** Thread into the linked list at the beginning of the FIFO queue.
				qType is a subclass of DQueueItem.
			 */
			void	push(qType* item) noexcept;

			/** Remove specified qType element from the queue.
				qType must be a subclass of DQueueItem.
				Returns zero if the item is not found in
				the queue. Otherwise it returns item.
			 */
			qType*	remove(qType* item) noexcept;

			/** Insert the "item" qType into the queue behind the
				"after" qType element.
				qType must be a subclass of DQueueItem.
			 */
			void	insertAfter(qType* after,qType* item) noexcept;

			/** Insert the "item" qType into the queue ahead of the
				"before" qType element.
				qType must be a subclass of DQueueItem.
			 */
			void	insertBefore(qType* before,qType* item) noexcept;

			/** Return a pointer to the first qType item in the queue.
				The returned item remains in the queue.
				qType is a subclass of DQueueItem.
			 */
			qType*	first(void) const noexcept;

			/** Return a pointer to the last qType item in the queue.
				The returned item remains in the queue.
				qType is a subclass of DQueueItem.
			 */
			qType*	last(void) const noexcept;

			/** Return a pointer to the qType item after the qType item "prev".
				Both items remain in the queue.
				qType is a subclass of DQueueItem.
			 */
			qType*	next(qType* prev) const noexcept;

			/** Return a pointer to the qType item after the qType item "next".
				Both items remain in the queue.
				qType is a subclass of DQueueItem.
			 */
			qType*	prev(qType* next) const noexcept;

		};

template <class qType>
	DQueue<qType>::DQueue(void) noexcept:_head(0),_tail(0){
		}

template <class qType>
DQueue<qType>::DQueue(DQueue<qType>& other) noexcept{
	_head	= other._head;
	_tail	= other._tail;
	other._head	= 0;
	other._tail	= 0;
	}

template <class qType>
DQueue<qType>&	DQueue<qType>::operator=(DQueue<qType>& other) noexcept{
	_head	= other._head;
	_tail	= other._tail;
	other._head	= 0;
	other._tail	= 0;
	return *this;
	}

template <class qType>
	inline qType* DQueue<qType>::get(void) noexcept{
		qType*			next;
		if((next = _head)){
			_head	= (qType*)next->__qitemlink_next;
			if(_head){
				_head->__qitemlink_prev	= 0;
				}
			else{
				_tail	= 0;
				}
			// Zeroing the next link ensures that
			// both links are zeros to help with
			// assumptions in remove operations.
			// There is no need to zero the prev
			// link since it should already be zero
			// at the head of the list.
			next->__qitemlink_next	= 0;
			}
		return(next);
		}

template <class qType>
	inline qType* DQueue<qType>::getLast(void) noexcept{
		qType*			lastInQ;
		lastInQ	= last();
		if(!lastInQ) return 0;
		return remove(lastInQ);
		}

template <class qType>
	inline qType* DQueue<qType>::pop(void) noexcept{
		return get();
		}

template <class qType>
	inline void	DQueue<qType>::put(qType* item) noexcept{
		item->__qitemlink_next	= 0;
		item->__qitemlink_prev	= _tail;
		if(_tail){
			_tail->__qitemlink_next	= item;
			}
		_tail					= item;
		if(!_head){
			_head	= item;
			}
		}

template <class qType>
	inline void	DQueue<qType>::push(qType* item) noexcept{
		item->__qitemlink_prev	= 0;
		item->__qitemlink_next	= _tail;
		_head					= item;
		if(!_tail){
			_tail	= item;
			}
		}

template <class qType>
	inline qType*	DQueue<qType>::remove(qType* item) noexcept{
		// If the queue is empty, then the item
		// is not in the queue.
		if(!_head || !_tail){
			return 0;
			}

		qType*	next	= (qType*)item->__qitemlink_next;
		qType*	prev	= (qType*)item->__qitemlink_prev;

		// This condition is an optimization in lieu of a
		// search to find out if the item is actually in the list.
		// This is based on the expected behavior that
		// the item's link fields are zero if the item is
		// not linked into a list.
		if(!prev && !next){
			// Items that have both link fields set to
			// zero may not be in the queue.
			if((_head == item) && (_tail == item)){
				// In the case of only a single element in
				// the queue, the item's pointers will be
				// zero, and the queue's head and tail
				// pointers will both point at the
				// item.
				_head	= 0;
				_tail	= 0;
				return item;
				}
			return 0;
			}

		if(next){
			next->__qitemlink_prev	= prev;
			item->__qitemlink_next	= 0;
			}
		else {
			_tail	= prev;
			}

		if(prev){
			prev->__qitemlink_next	= next;
			item->__qitemlink_prev	= 0;
			}
		else{
			_head	= next;
			}

		return item;
		}

template <class qType>
	inline void	DQueue<qType>::insertAfter(qType* prev,qType* item) noexcept{
		qType*	next	= (qType*)prev->__qitemlink_next;
		item->__qitemlink_next	= next;
		if(next){
			next->__qitemlink_prev	= item;
			}
		else{
			_tail	= item;
			}
		prev->__qitemlink_next	= item;
		item->__qitemlink_prev	= prev;
		}

template <class qType>
	inline void	DQueue<qType>::insertBefore(qType* before,qType* item) noexcept{
		qType*	prev	= (qType*)before->__qitemlink_prev;
		item->__qitemlink_prev	= prev;
		if(prev){
			prev->__qitemlink_next	= item;
			}
		else{
			_head	= item;
			}
		before->__qitemlink_prev	= item;
		item->__qitemlink_next		= before;
		}

template <class qType>
	inline qType* DQueue<qType>::first(void) const noexcept{
		return(_head);
		}

template <class qType>
	inline qType* DQueue<qType>::last(void) const noexcept{
		return _tail;
		}

template <class qType>
	inline qType* DQueue<qType>::next(qType* prev) const noexcept{
		return((qType*)prev->__qitemlink_next);
		}

template <class qType>
	inline qType* DQueue<qType>::prev(qType* next) const noexcept{
		return((qType*)next->__qitemlink_prev);
		}

}

#endif
