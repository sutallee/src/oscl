/*
   Copyright (C) 2005 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_crypto_md5h_
#define _oscl_crypto_md5h_
#include "oscl/checksum/md5/calculator.h"
#include "hmac.h"

namespace Oscl {
namespace Crypto {
namespace HMAC {

class MD5Calc : public Oscl::Crypto::HMAC::CalcApi< Oscl::Checksum::MD5::digestSize > {
	private:
		Oscl::Checksum::MD5::Calculator	_calc;
	public:
		void	reset() noexcept;
		void	update(const void* input, unsigned inputLen) noexcept;
		void	final(unsigned char	digest[Oscl::Checksum::MD5::digestSize]) noexcept;
	};

class MD5 {
	private:
		MD5Calc	_calc;
		Oscl::Crypto::HMAC::Calculator< 16, Oscl::Checksum::MD5::digestSize >	_hmac;
	public:
		MD5(	const void*			key,
				unsigned			keyLengthInOctets
				) noexcept;
		void	reset() noexcept;
		void	update(const void* input, unsigned inputLen) noexcept;
		void	final(unsigned char	digest[Oscl::Checksum::MD5::digestSize]) noexcept;
		
	};

}
}
}

#endif
