/*
   Copyright (C) 2005 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_crypto_calcapih_
#define _oscl_crypto_calcapih_

namespace Oscl {
namespace Crypto {
namespace HMAC {

/*
	This template class is an interface used by the generic HMAC
	template class to provide the H (hash) function described in
	RFC2104.
 */
template <unsigned lSize>
class CalcApi {
	public:
		/* Make GCC happy */
		virtual ~CalcApi(){}

		/*	Reset the state of the calculator in preparatioin for
			a new hash calculation.
		 */
		virtual	void	reset() noexcept=0;

		/* Update the hash using the specified data.
		 */
		virtual void	update(const void* input, unsigned inputLen) noexcept=0;

		/*	Complete the hash calculation and copy the resulting hash into
			the digest array.
		 */
		virtual void	final(unsigned char	digest[lSize]) noexcept=0;
	};

}
}
}

#endif
