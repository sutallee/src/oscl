/*
   Copyright (C) 2005 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_crypto_hmach_
#define _oscl_crypto_hmach_
#include "calcapi.h"

namespace Oscl {
namespace Crypto {
namespace HMAC {

/* An implementation of the RFC2104 HMAC.
 */
template <unsigned bSize,unsigned lSize>
class Calculator {
	private:
		unsigned char		_b[bSize];
		CalcApi<lSize>&		_calcApi;

	public:
		/** */
		Calculator(	CalcApi<lSize>&		calcApi,
					const void*			key,
					unsigned			keyLengthInOctets
					) noexcept;
		/** */
		void	reset() noexcept;
		/** */
		void	update(const void* input, unsigned inputLen) noexcept;
		/** */
		void	final(unsigned char digest[lSize]) noexcept;
	};

template <unsigned bSize,unsigned lSize>
Calculator<bSize,lSize>::
Calculator(	CalcApi<lSize>&	calcApi,
			const void*			key,
			unsigned			keyLengthInOctets
			) noexcept:
		_calcApi(calcApi)
		{
	unsigned	keyLength;
	if(keyLengthInOctets>bSize){
		_calcApi.reset();
		_calcApi.update(key,keyLengthInOctets);
		_calcApi.final(_b);
		keyLength	= lSize;
		}
	else {
		keyLength	= keyLengthInOctets;
		memcpy(_b,key,keyLengthInOctets);
		}
	for(unsigned i=keyLength;i<bSize;++i){
		_b[i]	= 0;
		}
	reset();
	}

template <unsigned bSize,unsigned lSize>
void	Calculator<bSize,lSize>::
reset() noexcept{
	_calcApi.reset();
	unsigned char	_ipadXOR[bSize];
	for(unsigned i=0;i<bSize;++i){
		_ipadXOR[i]	= _b[i] ^ 0x36;
		}
	_calcApi.update(_ipadXOR,bSize);
	}

template <unsigned bSize,unsigned lSize>
void	Calculator<bSize,lSize>::
update(	const void*	input,
		unsigned	inputLength
		) noexcept{
	_calcApi.update(input,inputLength);
	}

template <unsigned bSize,unsigned lSize>
void	Calculator<bSize,lSize>::
final(unsigned char digest[lSize]) noexcept{
	unsigned char	iDigest[lSize];
	_calcApi.final(iDigest);
	unsigned char	_opadXOR[bSize];
	for(unsigned i=0;i<bSize;++i){
		_opadXOR[i]	= _b[i] ^ 0x5C;
		}
	_calcApi.reset();
	_calcApi.update(_opadXOR,bSize);
	_calcApi.update(iDigest,lSize);
	_calcApi.final(digest);
	}

}
}
}
#endif
