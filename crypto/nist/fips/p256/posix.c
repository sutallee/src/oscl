/*
 * Copyright (c) 2013, Kenneth MacKay
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *  * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 *  * Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*	This file contains the Posix dependent portions of
	the ECC code.
 */

#include <fcntl.h>
#include <unistd.h>
#include <sys/types.h>
#include <string.h>
#include <stdio.h>

#include "ecc.h"
#include "oscl/crypto/nist/fips/p256/types.h"

#if 0
static bool ecc_get_random_number(uint8_t *vli)
{
	char *ptr = (char *) vli;
	size_t left = ECC_BYTES;
	int fd;

	printf("%s: enter\n",__PRETTY_FUNCTION__);

	fd = open("/dev/urandom", O_RDONLY | O_CLOEXEC);
	if (fd < 0) {
		perror("read(/dev/urandom)");
		fd = open("/dev/random", O_RDONLY | O_CLOEXEC);
		if (fd < 0){
			perror("read(/dev/random)");
			return false;
		}
	}

	while (left > 0) {
		ssize_t ret;

		ret = read(fd, ptr, left);
		if (ret <= 0) {
			perror("read failed");
			close(fd);
			return false;
		}

		left -= ret;
		ptr += ret;
	}

	close(fd);

	printf("%s: done\n",__PRETTY_FUNCTION__);

	return true;
}
#endif

bool ecc_make_key(uint8_t public_key[64], uint8_t private_key[32])
{
	uint8_t	random[ECC_BYTES];
	unsigned tries = 0;

	do {
		if (!ecc_get_random_number(random) || (tries++ >= MAX_TRIES)){
			printf("%s: tries: %u\n",__PRETTY_FUNCTION__,tries);
			return false;
			}

		bool
		success	= ecc_make_key_using_external_random(
					random,
					private_key,
					public_key
					);

		if(success){
			return true;
			}

	} while (true);

	return true;
}

bool ecdh_shared_secret(const uint8_t public_key[64],
				const uint8_t private_key[32],
				uint8_t secret[32])
{
	uint8_t	random[ECC_BYTES];

	if (!ecc_get_random_number(random))
		return false;


	bool
	success	= ecdh_shared_secret_using_external_random(
				random,
				public_key,
				private_key,
				secret
				);

	return success;
}
