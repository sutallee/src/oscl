/*
   Copyright (C) 2005 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <string.h>
#include "md5.h"

using namespace Oscl::Crypto::HMAC;

void	MD5Calc::reset() noexcept{
	_calc.reset();
	}

void	MD5Calc::update(const void* input, unsigned inputLen) noexcept{
	_calc.update(input,inputLen);
	}

void	MD5Calc::final(unsigned char	digest[Oscl::Checksum::MD5::digestSize]) noexcept{
	_calc.final(digest);
	}

MD5::MD5(	const void*			key,
			unsigned			keyLengthInOctets
			) noexcept:
		_calc(),
		_hmac(_calc,key,keyLengthInOctets)
		{
	}

void	MD5::reset() noexcept{
	_hmac.reset();
	}

void	MD5::update(const void* input, unsigned inputLen) noexcept{
	_hmac.update(input,inputLen);
	}

void	MD5::final(unsigned char	digest[Oscl::Checksum::MD5::digestSize]) noexcept{
	_hmac.final(digest);
	}

