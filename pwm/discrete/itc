/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

/** */
namespace Oscl {
/** */
namespace PWM {
/** */
namespace Discrete {

/** This interface provides general control of
	a PWM pulse width as a count of clock ticks.
	The client must know apriori the maximum
	number of clock ticks within a period as
	supported by the implementation. 
	A typical PWM configuration uses a n-bit
	counter which rolls over at the end each period,
	beginning a new cycle.
 */
itc ITC {

	/** This message provides general control of
		a PWM pulse width as a count of clock ticks.
		The client must know apriori the maximum
		number of clock ticks within a period as
		supported by the implementation. 
		A typical PWM configuration uses a n-bit
		counter which rolls over at the end each period,
		beginning a new cycle.
	 */
	message	SetPulseWidth {
		fields{
			/** Width of the pulse in clock ticks.
			 */
			input	unsigned int	count;
			}

		/** This operation provides general control of
			a PWM pulse width as a count of clock ticks.
			The client must know the maximum
			number of clock ticks within a period as
			supported by the implementation. 
			Count values greater than the maximum number
			of clock ticks causes undefined behavior.
			A typical PWM configuration uses a n-bit
			counter which rolls over at the end each period,
			beginning a new cycle.
		 */
		sync	void setPulseWidth;
		}
	}
}
}
}
