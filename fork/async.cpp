/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "async.h"
#include <stdio.h>
#include <stdarg.h>
#include <unistd.h>
#include "fd.h"

int Oscl::Fork::async(
		const char*	command,
		...
		) noexcept{

	/*	We use vfork() instead of fork()
		for several reasons.
		1.	The fork() system call can duplicate
			the memory foot-print of the
			parent process. This is a problem
			in memory constrained systems.
		2.	The vfork() system call is faster
			because the parent page tables are
			not duplicated for the child.
		3.	Embedded systems often don't have
			a swap file-system to absorb the
			extra/temporary memory used by
			fork().
		4.	The vfork() system call can be implemented
			on platforms that do not have a MMU.

		There is much controversy surrounding
		the benefits of vfork() since Linux
		uses copy-on-write for the memory and
		because using vfork() is tricky given
		the fact that the child and parent
		processes share memory until the child
		invokes an exec*().
	 */

	int
	pid	= vfork();

	if(pid < 0) {
		// failure
		perror(__PRETTY_FUNCTION__);
		}
	else if(!pid) {
		// child

		static const unsigned	maxArguments	= 16;
		const char*				p[maxArguments];

		va_list ap;

		va_start(
			ap,
			command
			);

		for(unsigned i=0;i<maxArguments;++i){

			p[i]	= va_arg(ap,const char*);

			if(!p[i]){
				break;
				}
			}

		va_end(ap);

		/*	We close all file descriptors
			inherited from the parent with
			the exception of STDIN_FILENO(0),
			STDOUT_FILENO(1), and STDERR_FILENO(2).
		 */
		closeFileDescriptors(STDERR_FILENO+1);

		int
		err = execl(
				command,
				command,
				p[0],
				p[1],
				p[2],
				p[3],
				p[4],
				p[5],
				p[6],
				p[7],
				p[8],
				p[9],
				p[10],
				p[11],
				p[12],
				p[13],
				p[14],
				p[15],
				NULL
				);

		if(err) {
			perror(__PRETTY_FUNCTION__);
			_exit(err);
			}
		}

	return pid;
 	}

