/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "oscl/fork/fd.h"
#include <unistd.h>
#include <errno.h>
#include <sys/select.h>

void Oscl::Fork::closeFileDescriptors(int firstFD) noexcept {

	/*
		The FD_SETSIZE defines the maximum number
		of file-descriptors that can be used by
		the select() operation, which uses the 
		opaque fd_set type and the uses FD_SET,
		FD_CLR, FD_ZERO, and FD_ISSET macros to
		manipulate the descriptors. The maximum
		number of descriptors is FD_SETSIZE.

		Because the OSCL is targeted at embedded
		systems it would be very unusual for
		such a system to require more than
		FD_SETSIZE (typically 1024) file descriptors.

		An alternate implementation could be
		written using gitrlimit(RLIMIT_NOFILE) to
		obtain the possibly larger value. However,
		gitrlimit() sets errno, which may adversly
		affect the parent process in the typical
		case where this function is called from
		the child process immediately after vfork().
	 */

	/*
		Because:
		-	errno is shared between the parent
			and child process and
		-	because we are calling close() in
			the loop and
		-	because close() sets errno and
		-	because we are ignoring errno

		We save the current errno here and restore
		it after the loop.
	 */
	int	err	= errno;

	for(
		unsigned i=firstFD;
		i < FD_SETSIZE;
		++i
		){
		/*	We ignore the return value of
			close since we expect many of
			the file descriptors to be unused
			and, therfore, return an error.
		 */
		close(i);
		}

	errno	= err;
	}

