/*
   Copyright (C) 2024 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_persist_parent_littlefs_mounth_
#define _oscl_persist_parent_littlefs_mounth_

#include <lfs.h>

/** */
namespace Oscl {

/** */
namespace Persist {

/** */
namespace LittleFs {

/** This global function is used to return
	the LittleFS file-system that corresponds
	to the first directory in the specified
	path. LittleFS itself has one lfs_t per
	file-system.

	Example: "/mp1/blah" returns a pointer to the
	lfs_t tha corresponds to mount point "mp1".

	FIXME: perhaps we should return the mount-point
	corresponding to "/mp1/blah" to allow for nested
	mount-points?

	relativePath will be set to the first location in
	path after the mount-point portion (if any).

	RETURN: nullptr if there is no such mount-point.
 */
lfs_t*	getMountPoint( const char* path, const char** relativePath ) noexcept;

}
}
}

#endif
