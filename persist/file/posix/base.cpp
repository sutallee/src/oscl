/*
   Copyright (C) 2021 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <stdio.h>
#include <unistd.h>
#include <errno.h>

#include "base.h"
#include "oscl/error/info.h"
#include "oscl/strings/fixed.h"

//#define DEBUG_TRACE

using namespace Oscl::Persist::File::Posix;

Base::Base(
	Oscl::Persist::
	Parent::Posix::Part&	parent,
	const char*				fileName
	) noexcept:
		_parent(parent),
		_fileName(fileName)
		{
	}

Base::~Base() noexcept{
	destroyFile();
	}

void	Base::destroyFile() noexcept{

	Oscl::Strings::Fixed<1024>	path;

	_parent.buildPath(path);

	path	+= _fileName;

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s: %s\n",
		OSCL_PRETTY_FUNCTION,
		path.getString()
		);
	#endif

	if(path.truncated()){
		Oscl::Error::Info::log(
			"%s: trunacated path: %s\n",
			OSCL_PRETTY_FUNCTION,
			path.getString()
			);
		}

	int
	result	= unlink(path.getString());

	if(result){
		Oscl::Error::Info::log(
			"%s unlink() failed: %d\n",
			OSCL_PRETTY_FUNCTION,
			result
			);
		}
	}

bool	Base::read() noexcept {
	Oscl::Strings::Fixed<1024>	path;

	_parent.buildPath(path);

	path	+= _fileName;

	if(path.truncated()){
		Oscl::Error::Info::log(
			"%s: trunacated path: %s\n",
			OSCL_PRETTY_FUNCTION,
			path.getString()
			);
		}

	FILE*
	file	= fopen(
				path.getString(),
				"r"
				);

	if(!file){

		#ifdef DEBUG_TRACE
		Oscl::Error::Info::log(
			"%s: fopen(%s) failed. errno: %d\n",
			OSCL_PRETTY_FUNCTION,
			path.getString(),
			errno
			);
		#endif

		setDefaultValues();

		return true;
		}

	bool
	failed	= readValues(file);

	fclose(file);

	return failed;
	}

void	Base::write() noexcept{

	Oscl::Strings::Fixed<1024>	path;

	_parent.buildPath(path);

	path	+= _fileName;

	if(path.truncated()){
		Oscl::Error::Info::log(
			"%s: trunacated path: %s\n",
			OSCL_PRETTY_FUNCTION,
			path.getString()
			);
		}

	FILE*
	file	= fopen(
				path.getString(),
				"w"
				);

	if(!file){
		Oscl::Error::Info::log(
			"%s: fopen(%s) failed. errno: %d\n",
			OSCL_PRETTY_FUNCTION,
			path.getString(),
			errno
			);
		return;
		}

	writeValues(file);

	fclose(file);
	}

