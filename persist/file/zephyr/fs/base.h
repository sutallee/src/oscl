/*
   Copyright (C) 2021 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_persist_file_zephyr_fs_baseh_
#define _oscl_persist_file_zephyr_fs_baseh_

#include "oscl/fs/file/api.h"
#include "oscl/persist/parent/zephyr/fs/part.h"

/** */
namespace Oscl {
/** */
namespace Persist {
/** */
namespace File {
/** */
namespace Zephyr {
/** */
namespace FS {

/** The purpose of this abstract base
	class is to encapsulate the common
	operations used to maintain a ZephyrOS
	file based persistence mechanism.
 */
class Base {
	private:
		/** */
		Oscl::Persist::
		Parent::Zephyr::FS::Part&	_parent;

		/** */
		const char* const		_fileName;

	public:
		/** The "fileName" parameter must be
			valid for the life-time of this
			object.
		 */
		Base(
			Oscl::Persist::
			Parent::Zephyr::FS::Part&	parent,
			const char*					fileName
			) noexcept;

		/** */
		virtual ~Base() noexcept;

	private:

		/** */
		void	destroyFile() noexcept;

	protected:
		/** */
		bool	read() noexcept;

		/** */
		void	write() noexcept;

	public:
		/** */
		virtual void	setDefaultValues() noexcept=0;

		/** RETURN: true for failure
		 */
		virtual bool	writeValues(Oscl::FS::File::Api& file) noexcept=0;

		/** RETURN: true for failure
		 */
		virtual bool	readValues(Oscl::FS::File::Api& file) noexcept=0;
	};

}
}
}
}
}

#endif
