/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_persist_record_deco_parth_
#define _oscl_persist_record_deco_parth_
#include "oscl/persist/record/rw/api.h"

/** */
namespace Oscl {
/** */
namespace Persist {
/** */
namespace Record {
/** */
namespace Decorator {
/** */
namespace RW {

/** The purpose of this record decorator is to allow for
	nested records. This decorator adds the constant
	decorator offset to the offset specfied by the read
	and write operations and then invokes the corresponding
	super-record operation.
 */
class Part : public Oscl::Persist::Record::RW::Api {
	private:
		/** */
		Oscl::Persist::Record::RW::Api&	_rwApi;
		/** */
		const unsigned				_offset;
	public:
		/** */
		Part(	Oscl::Persist::Record::RW::Api&	rwApi,
				unsigned						offset
				) noexcept;
	private:
		/** */
		void	read(	void*		dest,
						unsigned	length,
						unsigned	offset
						) const noexcept;
		/** */
		void	write(	const void*	src,
						unsigned	length,
						unsigned	offset
						) const noexcept;
	};

}
}
}
}
}


#endif
