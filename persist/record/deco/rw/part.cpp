/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "part.h"

using namespace Oscl::Persist::Record::Decorator::RW;

Part::Part(	Oscl::Persist::Record::RW::Api&	rwApi,
			unsigned						offset
			) noexcept:
		_rwApi(rwApi),
		_offset(offset)
		{
	}

void	Part::read(	void*		dest,
					unsigned	length,
					unsigned	offset
					) const noexcept{
	_rwApi.read(dest,length,offset+_offset);
	}

void	Part::write(	const void*	src,
						unsigned	length,
						unsigned	offset
						) const noexcept{
	_rwApi.write(src,length,offset+_offset);
	}

