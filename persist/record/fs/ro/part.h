/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_persist_record_fs_ro_parth_
#define _oscl_persist_record_fs_ro_parth_
#include "oscl/persist/record/ro/api.h"
#include "oscl/fs/file/ro/access/api.h"

/** */
namespace Oscl {
/** */
namespace Persist {
/** */
namespace Record {
/** */
namespace FS {
/** */
namespace RO {

/** The purpose of this class is to map a record API
	onto a file.
 */
class Part : public Oscl::Persist::Record::RO::Api {
	private:
		/** */
		Oscl::FS::File::RO::Handle&			_fileHandle;
		/** */
		Oscl::FS::File::RO::Access::Api&	_roApi;
		/** */
		const unsigned						_offset;
	public:
		/** */
		Part(	Oscl::FS::File::RO::Handle&			fileHandle,
				Oscl::FS::File::RO::Access::Api&	roApi,
				unsigned							offset
				) noexcept;
	private:
		/** */
		void	read(	void*		dest,
						unsigned	length,
						unsigned	offset
						) const noexcept;
	};

}
}
}
}
}


#endif
