/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_persist_parent_posix_parth_
#define _oscl_persist_parent_posix_parth_

#include "oscl/strings/fixed.h"

/** */
namespace Oscl {

/** */
namespace Persist {

/** */
namespace Parent {

/** */
namespace Posix {

/** */
namespace Child {

/** This interface is a callback mechanism
	for iterating the files in this directory.
 */
class Api {
	public:
		/** */
		virtual void	item(
							const char*	path,
							const char*	file
							) noexcept=0;
	};

template <class Context> 
class Composer : public Api {
	private:
		/** A reference to the Context.
		 */
		Context&	_context;

	private:
		/**
		 */
		void	(Context::*_item)(
					const char*	path,
					const char*	file
					);

	public:
		/** */
		Composer(
			Context&		context,
			void	(Context::*item)(
						const char*	path,
						const char*	file
						)
			) noexcept;

	private:
		/**	This iteration callback is invoked for each
			persistent item.

			RETURN: true to stop iteration or false
			to continue the iteration.
		 */
		void	item(
					const char*	path,
					const char*	file
					) noexcept;

	};

template <class Context>
Composer<Context>::Composer(
			Context&	context,
			void		(Context::*item)(
							const char*	path,
							const char*	file
							)
			) noexcept:
		_context(context),
		_item(item)
		{
	}

template <class Context>
void	Composer<Context>::item(
			const char*	path,
			const char*	file
			) noexcept{

	return (_context.*_item)(
				path,
				file
				);
	}

}

/** The purpose of this class is to
	represent a single level of a
	heirarchical directory structure.
	It provides operations for building
	a directory path string and iterating
	the files in the directory for the client.
 */
class Part {
	private:
		/** This optional parent may be NIL
			if this nod represents the top
			of the application directory
			heirarchy.
		 */
		Part*						_parent;

		/** This string contains the name of
			the directory for this layer.

			If this instance represents the
			top of the application heirarchy,
			it may be an absolute path. Otherwise,
			it is a simple single directory name.
		 */
		Oscl::Strings::Api&			_directoryName;

	public:
		/** */
		Part(
			Part*				parent,
			Oscl::Strings::Api&	directoryName
			) noexcept;

		/** */
		~Part() noexcept;

		/**	This operation is used by a child in a parent
			heirarchy to build a directory-path string to
			the child.
		 */
		void	buildPath(Oscl::Strings::Api& path) noexcept;

		/** RETURN: the parent's directory name. */
		const char*	dirName() noexcept;

		/** This operation is used by the client to
			iterate through the files in this directory.
		 */
		void	walkDir(Child::Api& childApi) noexcept;
	};

}
}
}
}

#endif
