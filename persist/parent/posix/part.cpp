/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <sys/types.h>
#include <sys/stat.h>
#include <fts.h>
#include <errno.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>

#include "part.h"
#include "oscl/error/info.h"

using namespace Oscl::Persist::Parent::Posix;

Part::Part(
	Part*				parent,
	Oscl::Strings::Api&	directoryName
	) noexcept:
		_parent(parent),
		_directoryName(directoryName)
		{

	Oscl::Strings::Fixed<1024>	path;

	buildPath(path);

	if(path.truncated()){
		Oscl::Error::Info::log(
			"%s: trunacated path: %s\n",
			__PRETTY_FUNCTION__,
			path.getString()
			);
		}
	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s: path: \"%s\"\n",
		__PRETTY_FUNCTION__,
		path.getString()
		);
	#endif

	int
	result  = mkdir(
				path.getString(),
				S_IRWXU
				);

	if(result < 0){
		if(errno != EEXIST){
			Oscl::Error::Info::log(
				"%s: mkdir(%s) failed. errno: %d\n",
				__PRETTY_FUNCTION__,
				path.getString(),
				errno
				);
			}
		}
	}

Part::~Part() noexcept{

	if(!_parent){
		// The root directory does not
		// need to be deleted.
		return;
		}

	Oscl::Strings::Fixed<1024>	path;

	buildPath(path);

	if(path.truncated()){
		Oscl::Error::Info::log(
			"%s: trunacated path: %s\n",
			__PRETTY_FUNCTION__,
			path.getString()
			);
		}

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s: path: \"%s\"\n",
		__PRETTY_FUNCTION__,
		path.getString()
		);
	#endif

	const char*	s	= path.getString();
	unsigned	len	= path.length();

	if(len && (s[path.length()-1] == '/')){
		// Remove the path seperator
		path.truncateLast(1);
		}

	int
	result  = rmdir(path.getString());

	if(result < 0){
		if(result != -EEXIST){
			Oscl::Error::Info::log(
				"%s: rmdir(%s) failed. errno: %d\n",
				__PRETTY_FUNCTION__,
				path.getString(),
				result
				);
			}
		}
	}

void	Part::buildPath(Oscl::Strings::Api& path) noexcept{
	if(_parent){
		_parent->buildPath(path);
		}

	if(path.truncated()){
		return;
		}

	if(_directoryName.length()){
		path	+= _directoryName;

		if(path.truncated()){
			return;
			}

		path	+= "/";
		}
	}

const char*	Part::dirName() noexcept{
	return _directoryName.getString();
	}

void	Part::walkDir(Child::Api& childApi) noexcept{

	Oscl::Strings::Fixed<1024>	path;

	buildPath(path);

	if(path.truncated()){
		Oscl::Error::Info::log(
			"%s: truncated!\n",
			__PRETTY_FUNCTION__
			);
		return;
		}

	FTS*	file_system	= 0;
	FTSENT*	fent		= 0;
	char*	dirlist[2];
	dirlist[0]	= (char*)path.getString();
	dirlist[1]	= 0;

	file_system	= fts_open(dirlist,FTS_COMFOLLOW | FTS_NOCHDIR,0);

	if(!file_system){
		Oscl::Error::Info::log(
			"%s: fts_open failed. errno: %d\n",
			__PRETTY_FUNCTION__,
			errno
			);
		}

	int	err		= 0;

	while((fent = fts_read(file_system))) {
		switch (fent->fts_info) {
			case FTS_DP :	// A directory being visited in *postorder*
				break;
			case FTS_F :	// A regular file
				if(fent->fts_level == 1){
					childApi.item(
						fent->fts_accpath,
						fent->fts_name
						);
					}
				break;

			case FTS_SL:	// A symbolic link
				break;
			case FTS_ERR:	// This is an error return, see fts_errno
			case FTS_DNR:	//  directory which cannot be read, see fts_errno
			case FTS_NS:	//  file for which no stat(2) information was available, see fts_errno
				err	= fent->fts_errno;
				break;
			case FTS_D :	// A directory being visited in preorder
				if(fent->fts_level == 1){
					fent->fts_number	= strtoul(fent->fts_name,0,0);
					}
				else if(fent->fts_level == 2){
					fent->fts_number	= fent->fts_parent->fts_number;
					}
				break;
			default:
				break;
			}
		if(err) break;
		}

	fts_close(file_system);
	}

