/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_persist_parent_posix_configh_
#define _oscl_persist_parent_posix_configh_

#include "part.h"
#include "oscl/strings/fixed.h"

/** */
namespace Oscl {

/** */
namespace Persist {

/** */
namespace Parent {

/** */
namespace Posix {

/** This Config template allows the ease
	creation of the inherited Part with
	a configurable maximum string size.
	This allows for optimization of memory
	usage for both long and short path names.
 */
template <unsigned maxNameSize> 
class Config:
		/** NOTE: The Oscl::Strings::Fixed must be
			constructed before the Part, since the
			constructor of the Part uses the directory
			name to create the directory.
			Therefore, instead of making the string
			a member variable, we "inherit" the string
			and make it the first inherited class such
			that it will be constructed/initialized
			before the Part.
		 */
		private Oscl::Strings::Fixed<maxNameSize>,
		public Part
		{
	public:
		/** */
		using Oscl::Strings::Fixed<maxNameSize>::operator=;

	public:
		/** */
		Config(
			Part*		parent,
			const char*	directoryName
			) noexcept:
				Oscl::Strings::Fixed<maxNameSize>(directoryName),
				Part(
					parent,
					*this
					)
				{
				}
	};

}
}
}
}

#endif
