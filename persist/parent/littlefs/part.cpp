/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <lfs.h>

#include "part.h"
#include "oscl/persist/littlefs/mount.h"
#include "oscl/error/info.h"
#include "oscl/error/fatal.h"

using namespace Oscl::Persist::Parent::LittleFs;

Part::Part(
	Part*				parent,
	Oscl::Strings::Api&	directoryName
	) noexcept:
		_parent(parent),
		_directoryName(directoryName),
		_started( false )
		{
	}

Part::~Part() noexcept{

	if(!_parent){
		// The root directory does not
		// need to be deleted.
		return;
		}

	Oscl::Strings::Fixed<1024>	path;

	buildPath(path);

	if(path.truncated()){
		Oscl::Error::Info::log(
			"%s: trunacated path: %s\n",
			__PRETTY_FUNCTION__,
			path.getString()
			);
		}

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s: path: \"%s\"\n",
		__PRETTY_FUNCTION__,
		path.getString()
		);
	#endif

	const char*	s	= path.getString();
	unsigned	len	= path.length();

	if(len && (s[path.length()-1] == '/')){
		// Remove the path seperator
		path.truncateLast(1);
		}

	const char*	lfsPath = nullptr;

	lfs_t*
	lfs	= Oscl::Persist::LittleFs::getMountPoint( s, &lfsPath );

	if( !lfs ) {
		Oscl::ErrorFatal::logAndExit(
			"%s: no such LittleFS mount-point: %s\n",
			__PRETTY_FUNCTION__,
			s
			);
		return;
		}

	int
	result  = lfs_remove( lfs, lfsPath );

	if(result < 0){
		if(result != LFS_ERR_EXIST){
			Oscl::Error::Info::log(
				"%s: lfs_remove(%s) failed. errno: %d\n",
				__PRETTY_FUNCTION__,
				lfsPath,
				result
				);
			}
		}
	}

void	Part::start() noexcept {

	if( _parent ) {
		_parent->start();
		}

	if( _started ) {
		return;
		}

	_started	= true;

	Oscl::Strings::Fixed<1024>	path;

	buildPath(path);

	if(path.truncated()){
		Oscl::Error::Info::log(
			"%s: trunacated path: %s\n",
			__PRETTY_FUNCTION__,
			path.getString()
			);
		}
	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s: path: \"%s\"\n",
		__PRETTY_FUNCTION__,
		path.getString()
		);
	#endif

	const char*	s	= path.getString();
	unsigned	len	= path.length();

	if(len && (s[path.length()-1] == '/')){
		// Remove the path seperator
		path.truncateLast(1);
		}

	const char*	lfsPath = nullptr;

	lfs_t*
	lfs	= Oscl::Persist::LittleFs::getMountPoint( s, &lfsPath );

	if( !lfs ) {
		Oscl::ErrorFatal::logAndExit(
			"%s: no such LittleFS mount-point: %s\n",
			__PRETTY_FUNCTION__,
			s
			);
		return;
		}

	int
	err  = lfs_mkdir(
			lfs,
			lfsPath
			);

	if( err < 0 ){
		if( err != LFS_ERR_EXIST ){
			Oscl::Error::Info::log(
				"%s: lfs_mkdir(%s) failed. err: %d\n",
				__PRETTY_FUNCTION__,
				lfsPath,
				err
				);
			}
		}
	}

void	Part::buildPath(Oscl::Strings::Api& path) noexcept{
	if(_parent){
		_parent->buildPath(path);
		}

	if(path.truncated()){
		return;
		}

	if(_directoryName.length()){
		path	+= _directoryName;

		if(path.truncated()){
			return;
			}

		path	+= "/";
		}
	}

const char*	Part::dirName() noexcept{
	return _directoryName.getString();
	}

void	Part::walkDir(
			std::function<
				void (
					const char* path,
					const char* file
					)
				> callback
			) noexcept {

	Oscl::Strings::Fixed<1024>	path;

	buildPath(path);

	if(path.truncated()){
		Oscl::Error::Info::log(
			"%s: truncated!\n",
			__PRETTY_FUNCTION__
			);
		return;
		}

	const char*	s	= path.getString();

	const char*	lfsPath = nullptr;

	lfs_t*
	lfs	= Oscl::Persist::LittleFs::getMountPoint( s, &lfsPath );

	if( !lfs ) {
		Oscl::ErrorFatal::logAndExit(
			"%s: no such LittleFS mount-point: %s\n",
			__PRETTY_FUNCTION__,
			s
			);
		return;
		}

	lfs_dir_t	dir;
	
	int
	err	= lfs_dir_open(
			lfs,
			&dir,
			lfsPath
			);

	if( err < 0 ){
		Oscl::Error::Info::log(
			"%s: lfs_dir_open failed. err: %d\n",
			__PRETTY_FUNCTION__,
			err
			);
		}


	for( ;; ) {

		lfs_info	info;

		// int lfs_dir_read(lfs_t *lfs, lfs_dir_t *dir, struct lfs_info *info);
		err = lfs_dir_read( lfs, &dir, &info );

		if( err < 0 ){
			Oscl::Error::Info::log(
				"%s: lfs_dir_read failed. err: %d\n",
				__PRETTY_FUNCTION__,
				err
				);
			break;
			}

		if( err == 0 ) {
			break;
			}

		switch ( info.type ) {
			case LFS_TYPE_REG : {	// A regular file
				Oscl::Strings::Fixed<1024>	filePath(lfsPath);
				callback(
					filePath,
					info.name
					);
				}
				break;
			default:
				break;
			}
		}

	err = lfs_dir_close( lfs, &dir );

	if( err < 0 ){
		Oscl::Error::Info::log(
			"%s: lfs_dir_close failed. err: %d\n",
			__PRETTY_FUNCTION__,
			err
			);
		}
	}

