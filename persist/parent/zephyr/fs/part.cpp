/*
   Copyright (C) 2020 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <stdlib.h>
#include <string.h>
#include <fs/fs.h>
#include <errno.h>

#include "part.h"
#include "oscl/error/info.h"

using namespace Oscl::Persist::Parent::Zephyr::FS;

Part::Part(
	Part*				parent,
	Oscl::Strings::Api&	directoryName
	) noexcept:
		_parent(parent),
		_directoryName(directoryName)
		{

	if(!_parent){
		// The root directory does not
		// need to be created.
		return;
		}

	Oscl::Strings::Fixed<1024>	path;

	buildPath(path);

	if(path.truncated()){
		Oscl::Error::Info::log(
			"%s: trunacated path: %s\n",
			__PRETTY_FUNCTION__,
			path.getString()
			);
		}

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s: path: \"%s\"\n",
		__PRETTY_FUNCTION__,
		path.getString()
		);
	#endif

	const char*	s	= path.getString();
	unsigned	len	= path.length();

	if(len && (s[path.length()-1] == '/')){
		// Remove the path seperator
		path.truncateLast(1);
		}

	int
	result  = fs_mkdir(path.getString());

	if(result < 0){
		if(result != -EEXIST){
			Oscl::Error::Info::log(
				"%s: fs_mkdir(%s) failed. errno: %d\n",
				__PRETTY_FUNCTION__,
				path.getString(),
				result
				);
			}
		}
	}

Part::~Part() noexcept{

	if(!_parent){
		// The root directory does not
		// need to be deleted.
		return;
		}

	Oscl::Strings::Fixed<1024>	path;

	buildPath(path);

	if(path.truncated()){
		Oscl::Error::Info::log(
			"%s: trunacated path: %s\n",
			__PRETTY_FUNCTION__,
			path.getString()
			);
		}

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s: path: \"%s\"\n",
		__PRETTY_FUNCTION__,
		path.getString()
		);
	#endif

	const char*	s	= path.getString();
	unsigned	len	= path.length();

	if(len && (s[path.length()-1] == '/')){
		// Remove the path seperator
		path.truncateLast(1);
		}

	int
	result  = fs_unlink(path.getString());

	if(result < 0){
		if(result != -EEXIST){
			Oscl::Error::Info::log(
				"%s: fs_unlink(%s) failed. errno: %d\n",
				__PRETTY_FUNCTION__,
				path.getString(),
				result
				);
			}
		}
	}

void	Part::buildPath(Oscl::Strings::Api& path) noexcept{
	if(_parent){
		_parent->buildPath(path);
		}

	if(path.truncated()){
		return;
		}

	if(_directoryName.length()){
		path	+= _directoryName;

		if(path.truncated()){
			return;
			}

		path	+= "/";
		}
	}

const char*	Part::dirName() noexcept{
	return _directoryName.getString();
	}

void	Part::walkDir(Child::Api& childApi) noexcept{

	Oscl::Strings::Fixed<1024>	path;

	buildPath(path);

	if(path.truncated()){
		Oscl::Error::Info::log(
			"%s: truncated!\n",
			__PRETTY_FUNCTION__
			);
		return;
		}

	struct fs_dir_t	dir;

	int
	result	= fs_opendir(
				&dir,
				path.getString()
				);

	if(result < 0){
		Oscl::Error::Info::log(
			"%s: fs_opendir failed. result: %d\n",
			__PRETTY_FUNCTION__,
			result
			);
		}

	struct fs_dirent	entry;

	memset(
		&entry,
		0,
		sizeof(entry)
		);

	while(!(result = fs_readdir(&dir,&entry))) {
		if(!entry.name[0]){
			// End of directory
			break;
			}
		if(entry.type == FS_DIR_ENTRY_FILE){
			Oscl::Strings::Fixed<1024>	filePath(path);
			filePath	+= entry.name;
			
			childApi.item(
				filePath.getString(),
				entry.name
				);
			}
		}

	if(result){
		Oscl::Error::Info::log(
			"%s: fs_readdir() failed. result: %d\n",
			__PRETTY_FUNCTION__,
			result
			);
		}

	result	= fs_closedir(&dir);

	if(result){
		Oscl::Error::Info::log(
			"%s: fs_closedir() failed. result: %d\n",
			__PRETTY_FUNCTION__,
			result
			);
		}
	}

