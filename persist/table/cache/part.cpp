/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "part.h"

using namespace Oscl::Persist::Table::Cache;
using namespace Oscl;

Part::Part(	void*		mem,
			unsigned	nRecords,
			unsigned	recordSize,
			unsigned	headerSize
			) noexcept:
		_table(mem),
		_recordMem(&((uint8_t*)mem)[headerSize]),
		_recordSize(recordSize),
		_nRecords(nRecords)
		{
	}

unsigned long	Part::serialize(Oscl::Stream::Output::Api& stream) const noexcept{
	return stream.write(	_table,
								&_recordMem[_nRecords*_recordSize]
							-	((uint8_t*)_table)
							);
	}

unsigned long	Part::load(Oscl::Stream::Input::Api& stream) const noexcept{
	return stream.read(	_table,
							&_recordMem[_nRecords*_recordSize]
						-	((uint8_t*)_table)
						);
	}

uint8_t*	Part::startOfRecord(unsigned index) noexcept{
	if(index > _nRecords-1){
		return 0;
		}
	return &_recordMem[_recordSize*index];
	}
