/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_persist_table_meta_headerh_
#define _oscl_persist_table_meta_headerh_
#include "oscl/endian/be.h"

/** */
namespace Oscl {
/** */
namespace Persist {
/** */
namespace Table {
/** */
namespace Meta {

/** The table meta data describes the format
	of the record array that is contained in
	the table.
	Since it would be nice to read and write
	tables from various platforms without worrying
	about the endianess of the data stored on the
	disk, all data members that are larger than
	a single byte have accessor functions which
	convert to and from the persistence format
	to that format that is used natively by the
	program. Note that the format shall be
	BIG endian.
 */
struct Header {
	/** This value of this field indicates the length of the header
		field. The length of the header includes the length of the
		_length field itself. This field MUST *ALWAYS* the first field
		in the file.
		The application may use the value of this field as
		a part of checking the integrity of the table as the length
		MUST be no longer than the length of the table file, and
		for a given _version should be of a particular size.
	 */
	uint8_t	length;
	/** The type field holds a value that indicates the
		type of the table. Generally the table type indicates
		the format of the records contained in the table.
		Thus, one can consider the record and table type to
		be the same. However, it is also possible to have
		a variation in the header itself. We use the _version
		field to specify variations in the header, since the
		header format is expected to remain the same for all
		tables.
	 */
	Oscl::Endian::BEU16	type;
	/** The type field holds a value that indicates the
		version of the header. We use the _version
		field to specify variations in the header, since the
		header format is expected to remain the same for all
		tables.
	 */
	Oscl::Endian::BEU16	version;
	};

}
}
}
}

#endif
