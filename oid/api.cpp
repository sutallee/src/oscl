/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "api.h"
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

using namespace Oscl;

/**
	To support both dynamic and static strings, I will require dynamic
	strings to polymorphically override the appropriate members and
	then use the base operators subsequent to buffer allocation and
	expansion. Thus, this base class will implement the static
	functionality which is common to both static and dynmic string
	manipulation.
	We can assume and require that every modification operation
	results in a null terminated string.
 */

inline void	ObjectID::Api::append(const OidType* src,unsigned long len) noexcept{
	if(!_buffer){
		_truncated	= true;
		return;
		}
	OidType*		p			= _buffer;
	unsigned long	maxLen		= maxLength();
	unsigned long	remaining	= maxLen - _length;
	unsigned long	n;
	if(remaining < len){
		n	= remaining;
		_truncated	= true;
		}
	else{
		n	= len;
		_truncated	= false;
		}
	p	+=_length;
	for(unsigned long i=0;i<n;++i){
		p[i]	= src[i];
		}
	_length		+= n;
	}

inline void	ObjectID::Api::assign(	const OidType*	src,
									unsigned long	len
									) noexcept{
	if(!src) {
		_truncated	= false;
		return;
		}
	if(!_buffer){
		_truncated	= true;
		return;
		}
	OidType*		p			= _buffer;
	unsigned long	maxLen		= maxLength();
	unsigned long	n;
	if(maxLen < len){
		_truncated	= true;
		n			= maxLen;
		}
	else{
		_truncated	= false;
		n			= len;
		}
	for(unsigned long i=n;i;--i,++p,++src){
		*p	= *src;
		}
	_length	= n;
	}

ObjectID::Api::Api(OidType* buffer,unsigned long length) noexcept:
		_truncated(false),
		_length(length),
		_buffer(buffer)
		{
	}

ObjectID::Api&	ObjectID::Api::operator += (OidType num) noexcept{
	append(&num,1);
	return *this;
	}

ObjectID::Api&	ObjectID::Api::operator += (const Oscl::ObjectID::RO::Api& s) noexcept{
	append(s.getObjectID(),s.length());
	return *this;
	}

ObjectID::Api&	ObjectID::Api::operator += (const char* s) noexcept{
	const char*	p	= s;
	if(!(s && *s)) return *this;
	while(p){
		*this	+= (unsigned)strtoul(p,(char**)0,0);
		p	= index(p,'.');
		if(p) ++p;
		}
	return *this;
	}

ObjectID::Api&	ObjectID::Api::operator = (const char* s) noexcept{
	const char*	p	= s;
	_length	= 0;
	if(!(s && *s)) return *this;
	while(p){
		char*	endptr;
		unsigned long	value	= strtoul(p,&endptr,0);
		if(endptr == p){
			return *this;
			}
		*this	+= value;
		if(*endptr != '.'){
			return *this;
			}
		p	= endptr;
		++p;
		}
	return *this;
	}

ObjectID::Api&	ObjectID::Api::operator = (const Oscl::ObjectID::RO::Api& s) noexcept{
	assign(s.getObjectID(),s.length());
	return *this;
	}

void	ObjectID::Api::truncate(unsigned n) noexcept{
	n	= (n > _length)?_length:n;
	_length	-= n;
	}

bool	ObjectID::Api::operator == (const char* s) const noexcept{
	const Oscl::ObjectID::RO::Api&	ro	= *this;
	return ro == s;
	}

bool	ObjectID::Api::operator == (const ObjectID::Api& s) const noexcept{
	const Oscl::ObjectID::RO::Api&	ro		= *this;
	const Oscl::ObjectID::RO::Api&	that	= s;
	return ro == that;
	}

bool	ObjectID::Api::operator != (const char* s) const noexcept{
	return !(ObjectID::Api::operator==(s));
	}

bool	ObjectID::Api::operator != (const ObjectID::Api& s) const noexcept{
	return !(ObjectID::Api::operator==(s));
	}

bool	ObjectID::Api::operator < (const ObjectID::Api& cmp) const noexcept{
	const Oscl::ObjectID::RO::Api&	ro		= *this;
	const Oscl::ObjectID::RO::Api&	that	= cmp;
	return ro < that;
	}

const Oscl::ObjectID::OidType*	ObjectID::Api::getObjectID() const noexcept{
	return _buffer;
	}

unsigned long ObjectID::Api::length() const noexcept{
	return _length;
	}

