/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_oid_ro_apih_
#define _oscl_oid_ro_apih_
#include "oscl/oid/type.h"

/** */
namespace Oscl {
/** */
namespace ObjectID {
/** */
namespace RO {

class Iterator;

/** */
class Api {
	friend class Iterator;
	public:
		/** */
		virtual ~Api(){}
		/** */
		bool	operator == (const char* s) const noexcept;
		/** */
		bool	operator == (const Api& s) const noexcept;
		/** */
		bool	operator != (const char* s) const noexcept;
		/** */
		bool	operator != (const Api& s) const noexcept;
		/** */
		bool	operator < (const Api& s) const noexcept;
		/** */
		virtual unsigned long length() const noexcept=0;
		/** */
		virtual const OidType*	getObjectID() const noexcept=0;
	};

/** */
class Iterator {
		/** */
		const Api&		_oid;
		/** */
		unsigned		_offset;
	public:
		/** */
		Iterator(const Api& oid) noexcept:
				_oid(oid),
				_offset(0)
				{
			}

		/** */
		Iterator(const Iterator& oid) noexcept:
				_oid(oid._oid),
				_offset(oid._offset)
				{
			}

		/** */
		void	first() noexcept{
			_offset	= 0;
			}

		/** */
		bool	more() const noexcept{
			return _offset < _oid.length();
			}

		/** */
		void	next() noexcept{
			++_offset;
			}

		/** */
		OidType	current() const noexcept{
			return _oid.getObjectID()[_offset];
			}
	};

}
}
}

#endif
