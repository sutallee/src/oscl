/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_oid_apih_
#define _oscl_oid_apih_
#include "roapi.h"

/** */
namespace Oscl {
/** */
namespace ObjectID {

//class Iterator;

/** */
class Api : public Oscl::ObjectID::RO::Api {
	protected:
		/** */
		bool			_truncated;
		/** */
		unsigned long	_length;
		/** */
		OidType*		_buffer;
	public:
		/** */
		explicit Api(OidType* buffer,unsigned long length) noexcept;
		/** */
		virtual ~Api(){}
		/** */
		Api&	operator += (OidType num) noexcept;
		/** */
		Api&	operator += (const Oscl::ObjectID::RO::Api& s) noexcept;
		/** This operator converts a null terminated ascii
			string, containing a list of of unsigned integers
			delimited by non numeric ascii characters, and
			appends the converted values to the object ID.
		 */
		Api&	operator += (const char* s) noexcept;
		/** This operator converts a null terminated ascii
			string, containing a list of of unsigned integers
			delimited by non numeric ascii characters, and
			assigns the converted values to the object ID.
		 */
		Api&	operator = (const char* s) noexcept;
		/** */
		Api&	operator = (const Oscl::ObjectID::RO::Api& s) noexcept;
		/** Strips last n from sub-identifiers from object ID.
		 */
		void			truncate(unsigned n) noexcept;
		/** */
		bool	operator == (const char* s) const noexcept;
		/** */
		bool	operator == (const Oscl::ObjectID::Api& s) const noexcept;
		/** */
		bool	operator != (const char* s) const noexcept;
		/** */
		bool	operator != (const Oscl::ObjectID::Api& s) const noexcept;
		/** */
		bool	operator < (const Oscl::ObjectID::Api& s) const noexcept;
		/** */
		unsigned long length() const noexcept;
		/** Returns true if the oid was truncated during a
			modification operation. The flag is cleared by the next
			successful non-truncating operation.
		 */
		inline bool truncated() const noexcept{ return _truncated;}
	public:
		/** */
		const OidType*			getObjectID() const noexcept;
		/** */
		virtual unsigned long	maxLength() const noexcept=0;
	protected:
		/** */
		void	append(const OidType* src,unsigned long length) noexcept;
		/** */
		void	assign(const OidType* src,unsigned long length) noexcept;
	};

}
}

#endif
