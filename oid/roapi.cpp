/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "roapi.h"
#include <string.h>
#include <stdlib.h>

using namespace Oscl::ObjectID::RO;

bool	Api::operator == (const char* s) const noexcept{
	const char*	p	= s;
	const OidType*	buff	= getObjectID();
	if(s && (length()==0)) return false;
	if(!buff && !s) return true;
	if(!buff && s) return false;
	if(buff && !s) return false;
	unsigned long	i=0;
	for(unsigned long i=0;p && (i<length());++i){
		if(strtoul(p,(char**)0,0) != buff[i]) return false;
		p	= index(p,'.');
		if(p) ++p;
		}
	return (!p && (i == length()))?true:false;
	}

bool	Api::operator == (const Api& s) const noexcept{
	if(length() != s.length()) return false;
	const OidType*	p = getObjectID();
	const OidType*	o = s.getObjectID();
	for(unsigned long i=length();i;--i){
		if(*p != *o) return false;
		++p;
		++o;
		}
	return true;
	}

bool	Api::operator != (const char* s) const noexcept{
	return !(Api::operator==(s));
	}

bool	Api::operator != (const Api& s) const noexcept{
	return !(Api::operator==(s));
	}

bool	Api::operator < (const Api& cmp) const noexcept{
	unsigned long	n	= (length() < cmp.length())?length():cmp.length();
	const OidType*	buff	= getObjectID();
	const OidType*	cbuff	= cmp.getObjectID();
	for(unsigned i=0;i<n;++i){
		if(buff[i] == cbuff[i]) continue;
		return (buff[i] < cbuff[i]);
		}
	return (length()<cmp.length());
	}

