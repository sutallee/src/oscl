/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_oid_fixedh_
#define _oscl_oid_fixedh_
#include "api.h"

/** */
namespace Oscl {
/** */
namespace ObjectID {

/** */
template <unsigned long oidArraySize>
class Fixed : public Oscl::ObjectID::Api {
	private:
		/** */
		OidType	_array[oidArraySize];
	public:
		/** */
		Fixed() noexcept:Oscl::ObjectID::Api(_array,0){
			}
		/** */
		explicit Fixed(const char* s) noexcept:Oscl::ObjectID::Api(_array,0){
			Oscl::ObjectID::Api&	api	= *this;
			api	= s;
			}
		/** */
		explicit Fixed(	const OidType*	array,
						unsigned		length) noexcept:
					Oscl::ObjectID::Api(_array,0){
						assign(array,length);
						}
		/** */
		explicit Fixed(const Oscl::ObjectID::RO::Api& oid) noexcept:
				Oscl::ObjectID::Api(_array,0)
				{
			Oscl::ObjectID::Api&	obj	= *this;
			obj						= oid;
			}
	public:
		/** */
		unsigned long maxLength() const noexcept{return oidArraySize;}
	};

}
}

#endif
