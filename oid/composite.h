/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_oid_compositeh_
#define _oscl_oid_compositeh_
#include "node.h"
#include "oscl/queue/queue.h"

/** */
namespace Oscl {
/** */
namespace ObjectID {

template <class nType> class Visitor;

template<class nType>
class Composite : public Node<nType> {
	private:
		/** List of child nodes ordered by ID.
		 */
		Oscl::Queue< Node<nType> >	_children;

	public:
		/** */
		Composite(nType& type,unsigned id) noexcept;

		/** */
		void	insertChild(Node<nType>& node) noexcept;
		/** */
		Node<nType>*	removeChild(Node<nType>& node) noexcept;

		/** */
		Node<nType>*	findChild(unsigned id) const noexcept;

		/** */
		Node<nType>*	firstChild() const noexcept;
		/** */
		Node<nType>*	lastChild() const noexcept;
		/** */
		Node<nType>*	nextChild(unsigned prevID) const noexcept;
		/** */
		Node<nType>*	nextChild(Node<nType>& prevNode) const noexcept;

	public:	// Node
		/** */
		void	accept(Visitor<nType>& v) noexcept;
	};

template<class nType>
Composite<nType>::Composite(nType& type,unsigned id) noexcept:
		Node<nType>(type,id)
		{
	}

template<class nType>
void	Composite<nType>::insertChild(Node<nType>& node) noexcept{
	Node<nType>*	next;
	for(	next=_children.first();
			next;
			next=_children.next(next)
			){
		if(node._id < next->_id){
			_children.insertBefore(next,&node);
			return;
			}
		}
	_children.put(&node);
	}

template<class nType>
Node<nType>*	Composite<nType>::removeChild(Node<nType>& node) noexcept{
	return _children.remove(&node);
	}

template<class nType>
Node<nType>*	Composite<nType>::findChild(unsigned id) const noexcept{
	Node<nType>*	next;
	for(	next=_children.first();
			next;
			next=_children.next(next)
			){
		if(next->_id == id){
			return next;
			}
		}
	return 0;
	}

template<class nType>
Node<nType>*	Composite<nType>::firstChild() const noexcept{
	return _children.first();
	}

template<class nType>
Node<nType>*	Composite<nType>::lastChild() const noexcept{
	return _children.last();
	}

template<class nType>
Node<nType>*	Composite<nType>::nextChild(unsigned prevID) const noexcept{
	Node<nType>*	next	= findChild(prevID);
	if(!next) return 0;
	return _children.next(next);
	}

template<class nType>
Node<nType>*	Composite<nType>::nextChild(Node<nType>& prevNode) const noexcept{
	return _children.next(&prevNode);
	}

template<class nType>
void	Composite<nType>::accept(Visitor<nType>& v) noexcept{
	v.visit(*this);
	}

}
}

#endif
