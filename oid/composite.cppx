/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "composite.h"
#include "visitor.h"
using namespace Oscl::ObjectID;

template<class Visitor>
Composite<Visitor>::Composite(void* type,unsigned id) noexcept:
		Node<Visitor>(type,id)
		{
	}

template<class Visitor>
void	Composite<Visitor>::insertChild(Node<Visitor>& node) noexcept{
	Node<Visitor>*	next;
	for(	next=_children.first();
			next;
			next=_children.next(next)
			){
		if(node._id < next->_id){
			_children.insertBefore(next,&node);
			return;
			}
		}
	_children.put(&node);
	}

template<class Visitor>
Node<Visitor>*	Composite<Visitor>::removeChild(Node<Visitor>& node) noexcept{
	return _children.remove(&node);
	}

template<class Visitor>
Node<Visitor>*	Composite<Visitor>::findChild(unsigned id) const noexcept{
	Node<Visitor>*	next;
	for(	next=_children.first();
			next;
			next=_children.next(next)
			){
		if(next->_id == id){
			return next;
			}
		}
	return 0;
	}

template<class Visitor>
Node<Visitor>*	Composite<Visitor>::firstChild() const noexcept{
	return _children.first();
	}

template<class Visitor>
Node<Visitor>*	Composite<Visitor>::lastChild() const noexcept{
	return _children.last();
	}

template<class Visitor>
Node<Visitor>*	Composite<Visitor>::nextChild(unsigned prevID) const noexcept{
	Node<Visitor>*	next	= findChild(prevID);
	if(!next) return 0;
	return _children.next(next);
	}

template<class Visitor>
Node<Visitor>*	Composite<Visitor>::nextChild(Node<Visitor>& prevNode) const noexcept{
	return _children.next(&prevNode);
	}

template<class Visitor>
void	Composite<Visitor>::accept(Visitor& v) noexcept{
	v.visit(*this);
	}

