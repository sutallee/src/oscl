#include "part.h"

using namespace Oscl::U8::Output;

Part::Part(
	Oscl::Mt::Itc::PostMsgApi&	papi,
	uint8_t						initialState
	) noexcept:
		_sap(
			*this,
			papi
			),
		_state(initialState)
		{
	}

Oscl::U8::Observer::Req::Api::SAP&	Part::getSAP() noexcept{
	return _sap;
	}

void	Part::notify(uint8_t state) noexcept{
	_state	= state;

	Oscl::U8::Observer::Req::Api::ChangeReq* note;

	while((note=_noteQ.get())){
		note->getPayload()._state	= state;
		note->returnToSender();
		}
	}

void Part::request(Oscl::U8::Observer::Req::Api::ChangeReq& msg) noexcept{
	if(_state == msg.getPayload()._state){
		_noteQ.put(&msg);
		}
	else{
		msg.getPayload()._state	= _state;
		msg.returnToSender();
		}
	}

void Part::request(Oscl::U8::Observer::Req::Api::CancelChangeReq& msg) noexcept{
	for(
		Oscl::U8::Observer::Req::Api::ChangeReq* next=_noteQ.first();
		next;
		next=_noteQ.next(next)
		){
		if(next == &msg.getPayload()._requestToCancel){
			_noteQ.remove(next);
			next->returnToSender();
			break;
			}
		}
	msg.returnToSender();
	}

void	Part::update(uint8_t state) noexcept{
	if(state != _state){
		notify(state);
		}
	}

