/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_snmp_findh_
#define _oscl_snmp_findh_
#include "oscl/oid/visitor.h"
#include "variable.h"

/** */
namespace Oscl {
/** */
namespace SNMP {

/** */
class Find : public Oscl::ObjectID::Visitor<Variable> {
	public:
		/** */
		Oscl::ObjectID::Node<Variable>*		_foundNode;

	private:
		/** */
//		const Oscl::ObjectID::Api&	_oid;
		/** */
		Oscl::ObjectID::RO::Iterator	_iterator;
	
	public:
		/** */
		Find(const Oscl::ObjectID::RO::Api& oid) noexcept;

		/** */
		void	visit(Oscl::ObjectID::Composite<Variable>& node) noexcept;

		/** */
		void	visit(Oscl::ObjectID::Leaf<Variable>& node) noexcept;
	};

/** */
class FindNext : public Oscl::ObjectID::Visitor<Variable> {
	public:
		/** */
		bool								_first;
		/** */
		Oscl::ObjectID::Node<Variable>*		_foundNode;

	private:
		/** */
		Oscl::ObjectID::RO::Iterator	_iterator;

		/** */
		Oscl::ObjectID::Api&			_output;
	
		/** */
		bool							_foundPredecessor;

	public:
		/** */
		FindNext(	const Oscl::ObjectID::RO::Api&	predecessor,
					Oscl::ObjectID::Api&			output
					) noexcept;

		/** */
		void	visit(Oscl::ObjectID::Composite<Variable>& node) noexcept;

		/** */
		void	visit(Oscl::ObjectID::Leaf<Variable>& node) noexcept;
	};

}
}

#endif
