/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_snmp_snmph_
#define _oscl_snmp_snmph_
#include "oscl/oid/composite.h"
#include "oscl/ber/tx.h"
#include "oscl/ber/rx.h"
#include "variable.h"
#include "node.h"
#include "oscl/queue/queue.h"

/** */
namespace Oscl {
/** */
namespace SNMP {

/** This class implements the behavior of an SNMP Agent.
 */
class Agent {
	private:
		/** Constant for building SNMP GetResponse PDUs. */
		enum{getResponseTag=2};
	public:
		/** Internal error types including the
			standard SNMP response ErrStatus values.
		 */
		typedef enum{
			noError		= 0,
			tooBig		= 1,
			noSuchName	= 2,
			badValue	= 3,
			readOnly	= 4,
			genErr		= 5,
			discard		= ~0
			} ErrorStatus;

	private:
		/** The MIB */
		Oscl::ObjectID::Node<Oscl::SNMP::Variable>&		_oidRoot;

		/** */
		Oscl::Queue<Node>		_varBindList;

		/** */
		Oscl::Queue<NodeMem>	_freeNodes;

		/** Index of first errored VarBinding in a PDU. */
		unsigned					_errIndex;

		/** First error detected in a PDU */
		ErrorStatus					_errStatus;

	public:
		/** */
		Agent(	Oscl::ObjectID::Node<Oscl::SNMP::Variable>&	oidRoot,
				NodeMem										nodeMem[],
				unsigned									nNodes
				) noexcept;

		/** Parses the PDU and builds the appropriate response if any.
		 */
		unsigned	processPacket(	const void*		pdu,
									unsigned		length,
									BerTxEncoder&	response
									) noexcept;

	private:
		/** */
		NodeMem*	allocNodeMem() noexcept;
		/** */
		void		freeNodeMem(NodeMem* mem) noexcept;
		/** */
		Node*		allocNode(	const Oscl::ObjectID::RO::Api&	oid,
								Variable&						var
								) noexcept;
		/** */
		void		freeNode(Node* node) noexcept;
		/** */
		Node*		queueVarBind(	const Oscl::ObjectID::RO::Api&	oid,
									Variable&						var
									) noexcept;
		/** */
		void		freeVarBindList() noexcept;

		/**	Validates request header (RequestID, ErrStatus, ErrIndex, and
			VarBindList header); updates _errStatus, and returns request ID.
		 */
		unsigned long	validateRequestHeader(BerRxEncoding& enc) noexcept;

		/** Parses SNMP request type. */
		void	processAction(	BerRxEncoding&	enc,
								BerTxEncoder&	response
								) noexcept;

		/** Sequences a SET request. */
		void	processSET(	BerRxEncoding&	enc,
							BerTxEncoder&	response
							) noexcept;

		/** Sequences a GET request. */
		void	processGET(	BerRxEncoding& enc,
							BerTxEncoder&	response
							) noexcept;

		/** Sequences a GET NEXT request. */
		void	processGETNEXT(	BerRxEncoding& enc,
								BerTxEncoder&	response
								) noexcept;

		/** */
		void	encodeGetVarBinding(Node& node,BerTxEncoder& response) noexcept;
		/** */
		void	encodeGetVarBindingList(BerTxEncoder& response) noexcept;

		/** Parse each VarBinding in a SET request. */
		void	parseSetVarBindingList(BerRxEncoding&	enc) noexcept;

		/** Parse each VarBinding in a GET request. */
		void	parseGetVarBindingList(BerRxEncoding& enc) noexcept;

		/** Parse each VarBinding in a GET request. */
		void	parseGetNextVarBindingList(BerRxEncoding& enc) noexcept;

		/** Parse a VarBinding in a GET request. */
		bool	parseGetVarBinding(BerRxEncoding& enc) noexcept;

		/** Parse a VarBinding in a GET NEXT request. */
		bool	parseGetNextVarBinding(BerRxEncoding& enc) noexcept;

		/** Parse a VarBinding in a SET request. */
		bool	parseSetVarBinding(BerRxEncoding& enc) noexcept;
	};

}
}

#endif
