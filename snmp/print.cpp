/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "print.h"
#include "oscl/oid/fixed.h"
#include <stdio.h>

void	oidPrintLast(const Oscl::ObjectID::RO::Iterator& it) noexcept{
	Oscl::ObjectID::RO::Iterator	newIt(it);
	newIt.next();
	if(newIt.more()){
		oidPrintLast(newIt);
		printf(".");
		}
	printf("%d",it.current());
	}

void	oidPrintNext(const Oscl::ObjectID::RO::Iterator& it) noexcept{
	printf("%d",it.current());

	Oscl::ObjectID::RO::Iterator	newIt(it);
	newIt.next();
	if(newIt.more()){
		printf(".");
		oidPrintNext(newIt);
		}
	}

void printCurrentValue(BerRxEncoding& var){
	if(!var.currentClassIsUniversal()){
		printf("Not a universal type.\n");
		return;
		}
	if(!var.currentTypeIsPrimitive()){
		printf("Not a primitive type.\n");
		return;
		}
	switch(var.currentIdentifierTag()){
		case 0x02:
			// INTEGER
			printf(	"INTEGER: %ld\n",
					var.decodeINTEGER()
					);
			break;
		case 0x04:
			// OCTET STRING
			printf(	"OCTETSTRING: %.*s\n",
					(int)var.currentContentLength(),
					(char*)var.currentContent()
					);
			break;
		case 0x06:
			// OID
			{
			printf("OID:");
			Oscl::ObjectID::Fixed<128>	oid;
			var.decodeOID(oid);
			Oscl::ObjectID::RO::Iterator	it(oid);
			if(it.more()){
				printf(".");
				oidPrintNext(it);
				printf("\n");
				}
			}
			break;
		case 0x05:
			// NULL
			printf("NULL\n");
			break;
		default:
			printf("Unknown Type\n");
		}
	}

