/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <new>
#include <string.h>
#include "agent.h"
#include "find.h"
#include "oscl/oid/fixed.h"
#include "oscl/strings/fixed.h"
#include "print.h"

using namespace Oscl::SNMP;

/** */
class GetResult : public Result::GET::HandlerApi {
	public:
		/** */
		Agent::ErrorStatus		_errStatus;

	private:
		/** */
		void	noError() noexcept{
			_errStatus	= Agent::noError;
			}

		/** */
		void	tooBig() noexcept{
			_errStatus	= Agent::tooBig;
			}

		/** */
		void	genErr() noexcept{
			_errStatus	= Agent::genErr;
			}

		/** */
		void	formatError() noexcept{
			_errStatus	= Agent::discard;
			}
	};

/** */
class SetResult : public Result::SET::HandlerApi {
	public:
		/** */
		Agent::ErrorStatus		_errStatus;

	private:
		/** */
		void	noError() noexcept{
			_errStatus	= Agent::noError;
			}

		/** */
		void	badValue() noexcept{
			_errStatus	= Agent::badValue;
			}

		/** */
		void	readOnly() noexcept{
			_errStatus	= Agent::readOnly;
			}

		/** */
		void	genErr() noexcept{
			_errStatus	= Agent::genErr;
			}

		/** */
		void	formatError() noexcept{
			_errStatus	= Agent::discard;
			}
	};

Agent::Agent(	Oscl::ObjectID::Node<Oscl::SNMP::Variable>&	oidRoot,
				NodeMem										nodeMem[],
				unsigned									nNodes
				) noexcept:
		_oidRoot(oidRoot)
		{
	for(unsigned i=0;i<nNodes;++i){
		freeNodeMem(&nodeMem[i]);
		}
	}

NodeMem*	Agent::allocNodeMem() noexcept{
	return _freeNodes.get();
	}

void		Agent::freeNodeMem(NodeMem* mem) noexcept{
	_freeNodes.put(mem);
	}

Node*		Agent::allocNode(	const Oscl::ObjectID::RO::Api&	oid,
								Variable&						var
								) noexcept{
	NodeMem*	mem	= allocNodeMem();
	if(!mem) return 0;
	Node*		node	= new(mem) Oscl::SNMP::Node(oid,var);
	return node;
	}

void		Agent::freeNode(Node* node) noexcept{
	node->~Node();
	freeNodeMem((NodeMem*)node);
	}

Node*		Agent::queueVarBind(	const Oscl::ObjectID::RO::Api&	oid,
									Variable&						var
									) noexcept{
	Node*	node	= allocNode(oid,var);
	if(!node) return 0;
	_varBindList.put(node);
	return node;
	}

void		Agent::freeVarBindList() noexcept{
	Node*	node;
	while((node=_varBindList.get())){
		freeNode(node);
		}
	}

unsigned long	Agent::validateRequestHeader(BerRxEncoding& enc) noexcept{
	unsigned long		identifier;

	// Validate and extract ID
	if(!enc.encodingIsValid()){
		// Discard
		_errStatus	= discard;
		return 0;
		}
	if(!enc.currentIsINTEGER()){
		// Discard
		_errStatus	= discard;
		return 0;
		}

	identifier	= enc.decodeUINTEGER();

	enc.nextEncoding();

	// Validate Error Status
	if(!enc.encodingIsValid()){
		// Discard
		_errStatus	= discard;
		return 0;
		}
	if(!enc.currentIsINTEGER()){
		// Discard
		_errStatus	= discard;
		return 0;
		}

	enc.nextEncoding();

	// Validate Error Index
	if(!enc.encodingIsValid()){
		// Discard
		_errStatus	= discard;
		return 0;
		}

	if(!enc.currentIsINTEGER()){
		// Discard
		_errStatus	= discard;
		return 0;
		}

	enc.nextEncoding();

	// Validate VarBindList
	if(!enc.encodingIsValid()){
		// Discard
		_errStatus	= discard;
		return 0;
		}

	if(!enc.currentIsSEQUENCE()){
		// Discard
		_errStatus	= discard;
		return 0;
		}

	return identifier;
	}

bool Agent::parseGetVarBinding(BerRxEncoding& enc) noexcept{
	if(!enc.encodingIsValid()){
		// Discard
		_errStatus	= discard;
		return false;
		}
	if(!enc.currentIsOID()){
		// Discard
		_errStatus	= discard;
		return false;
		}

	Oscl::ObjectID::Fixed<128>	snmpOID;

	enc.decodeOID(snmpOID);

	Find	find(snmpOID);

	_oidRoot.accept(find);

	if(!find._foundNode){
		_errStatus	= noSuchName;
		return false;
		}

	if(!queueVarBind(snmpOID,find._foundNode->getType())){
		_errStatus	= tooBig;
		return false;
		}

	enc.nextEncoding();

	if(!enc.encodingIsValid()){
		// Discard
		_errStatus	= discard;
		return false;
		}

	return true;
	}

void Agent::parseGetVarBindingList(BerRxEncoding& enc) noexcept{
	unsigned	index=1;
	do{
		if(!enc.encodingIsValid()){
			// Discard
			_errStatus	= discard;
			_errIndex	= index;
			return;
			}

		if(!enc.currentIsSEQUENCE()){
			// Discard
			_errStatus	= discard;
			_errIndex	= index;
			return;
			}

		BerRxEncoding	var(enc.currentContent(),enc.currentContentLength());

		if(!parseGetVarBinding(var)){
			_errIndex	= index;
			return;
			}

		++index;
		}while(enc.nextEncoding());

	return;
	}

// Returns true for success and false for failure.
bool	Agent::parseSetVarBinding(BerRxEncoding& enc) noexcept{
	if(!enc.encodingIsValid()){
		_errStatus	= discard;
		return false;
		}
	if(!enc.currentIsOID()){
		// Discard
		_errStatus	= discard;
		return false;
		}

	Oscl::ObjectID::Fixed<128>	snmpOID;

	enc.decodeOID(snmpOID);

	Find	find(snmpOID);
	_oidRoot.accept(find);

	if(!find._foundNode){
		_errStatus	= noSuchName;
		return false;
		}

	if(!queueVarBind(snmpOID,find._foundNode->getType())){
		_errStatus	= tooBig;
		return false;
		}

	enc.nextEncoding();

	if(!enc.encodingIsValid()){
		// Discard
		_errStatus	= discard;
		return false;
		}

	const Result::SET::Api&	result	=
		find._foundNode->getType().decodeToCache(enc);

	SetResult	rh;
	result.query(rh);
	_errStatus	= rh._errStatus;

	return true;
	}

bool Agent::parseGetNextVarBinding(BerRxEncoding& enc) noexcept{
	if(!enc.encodingIsValid()){
		// Discard
		_errStatus	= discard;
		return false;
		}
	if(!enc.currentIsOID()){
		// Discard
		_errStatus	= discard;
		return false;
		}

	Oscl::ObjectID::Fixed<128>	snmpOID;
	Oscl::ObjectID::Fixed<128>	nextOID;

	enc.decodeOID(snmpOID);

	FindNext	find(snmpOID,nextOID);

	_oidRoot.accept(find);

	if(!find._foundNode){
		_errStatus	= noSuchName;
		return false;
		}

	if(!queueVarBind(nextOID,find._foundNode->getType())){
		_errStatus	= tooBig;
		return false;
		}

	enc.nextEncoding();

	if(!enc.encodingIsValid()){
		// Discard
		_errStatus	= discard;
		return false;
		}
	return true;
	}

void Agent::parseGetNextVarBindingList(BerRxEncoding& enc) noexcept{
	unsigned	index=1;
	do{
		if(!enc.encodingIsValid()){
			// Discard
			_errStatus	= discard;
			_errIndex	= index;
			return;
			}

		if(!enc.currentIsSEQUENCE()){
			// Discard
			_errStatus	= discard;
			_errIndex	= index;
			return;
			}

		// Save space for Length field
		BerRxEncoding	var(enc.currentContent(),enc.currentContentLength());

		if(!parseGetNextVarBinding(var)){
			_errIndex	= index;
			return;
			}

		++index;
		}while(enc.nextEncoding());

	return;
	}

void	Agent::parseSetVarBindingList(BerRxEncoding& enc) noexcept{
	unsigned	index=1;

	do{
		if(!enc.encodingIsValid()){
			// Discard
			_errStatus	= discard;
			_errIndex	= index;
			return;
			}
		if(!enc.currentIsSEQUENCE()){
			// Discard
			_errStatus	= discard;
			_errIndex	= index;
			return;
			}

		BerRxEncoding	var(enc.currentContent(),enc.currentContentLength());

		if(!parseSetVarBinding(var)){
			_errIndex	= index;
			return;
			}

		++index;
		}while(enc.nextEncoding());

	return;
	}

void	Agent::encodeGetVarBinding(Node& node,BerTxEncoder& response) noexcept{
	response.encodeOID(node._oid);

	const Result::GET::Api&	result	= node._var.encodeFromCache(response);

	GetResult	rh;
	result.query(rh);
	_errStatus	= rh._errStatus;
	if(_errStatus != noError){
		return;
		}
	}

void	Agent::encodeGetVarBindingList(BerTxEncoder& response) noexcept{
	Node*	node;
	response.encodeSequenceIdentifier();
	BerTxEncoder	lengthEnc(response.currentBuffer(),response.remaining());
	response.encodeDefiniteTwoOctetLength(0);	// dummy encoding
	BerTxEncoder	resp(response.currentBuffer(),response.remaining());
	for(unsigned i=1;(node=_varBindList.get());++i){
		encodeGetVarBinding(*node,resp);
		freeNode(node);
		if(_errStatus != noError){
			_errIndex	= i;
			return;
			}
		}
	lengthEnc.encodeDefiniteTwoOctetLength(resp.length());
	response.advance(resp.length());
	}

void	Agent::processGET(	BerRxEncoding& enc,
							BerTxEncoder&	response
							) noexcept{
	unsigned long	identifier;

	identifier	= validateRequestHeader(enc);

	if(_errStatus != noError){
		// Discard
		return;
		}

	BerRxEncoding	varBindList(	enc.currentContent(),
									enc.currentContentLength()
									);

	BerTxEncoder	resp(response.currentBuffer(),response.remaining());

	parseGetVarBindingList(varBindList);

	if(_errStatus == noError){
		// First get all values into cache.
		// Eventually, this should be done atomically
		// and the implementation given to the client.
		Node*	node;
		for(	node	= _varBindList.first();
				node;
				node	= _varBindList.next(node)
				){
				node->_var.getToCache();
			}
		// Next, encode the response PDU.
		resp.encodeINTEGER(identifier);
		resp.encodeINTEGER(0);	// ErrorStatus
		resp.encodeINTEGER(0);	// ErrorIndex
		resp.encodeSequenceIdentifier();
		BerTxEncoder	lengthEnc(resp.currentBuffer(),resp.remaining());
		resp.encodeDefiniteTwoOctetLength(0);	// dummy encoding
		BerTxEncoder	resp2(resp.currentBuffer(),resp.remaining());
		encodeGetVarBindingList(resp2);
		if(_errStatus == noError){
			lengthEnc.encodeDefiniteTwoOctetLength(resp2.length());
			resp.advance(resp2.length());
			}
		}

	if(_errStatus == discard){
		return;
		}
	else if(_errStatus != noError){
		// error
		response.encodeINTEGER(identifier);
		response.encodeINTEGER(_errStatus);	// ErrorStatus
		response.encodeINTEGER(_errIndex);	// ErrorIndex
		response.append(enc.currentEncoding(),enc.currentSize());
		}
	else{
		response.advance(resp.length());
		}
	}

void	Agent::processGETNEXT(	BerRxEncoding& enc,
								BerTxEncoder&	response
								) noexcept{
	unsigned long	identifier;

	identifier	= validateRequestHeader(enc);

	if(_errStatus != noError){
		// Discard
		return;
		}

	BerRxEncoding	varBindList(	enc.currentContent(),
									enc.currentContentLength()
									);

	BerTxEncoder	resp(response.currentBuffer(),response.remaining());

	parseGetNextVarBindingList(varBindList);

	if(_errStatus == noError){
		// First get all values into cache.
		// Eventually, this should be done atomically
		// and the implementation given to the client.
		Node*	node;
		for(	node	= _varBindList.first();
				node;
				node	= _varBindList.next(node)
				){
				node->_var.getToCache();
			}
		// Next, encode the response PDU.
		resp.encodeINTEGER(identifier);
		resp.encodeINTEGER(0);	// ErrorStatus
		resp.encodeINTEGER(0);	// ErrorIndex
		resp.encodeSequenceIdentifier();
		BerTxEncoder	lengthEnc(resp.currentBuffer(),resp.remaining());
		resp.encodeDefiniteTwoOctetLength(0);	// dummy encoding
		BerTxEncoder	resp2(resp.currentBuffer(),resp.remaining());
		encodeGetVarBindingList(resp2);
		if(_errStatus == noError){
			lengthEnc.encodeDefiniteTwoOctetLength(resp2.length());
			resp.advance(resp2.length());
			}
		}

	if(_errStatus == discard){
		return;
		}
	else if(_errStatus != noError){
		// error
		response.encodeINTEGER(identifier);
		response.encodeINTEGER(_errStatus);	// ErrorStatus
		response.encodeINTEGER(_errIndex);	// ErrorIndex
		response.append(enc.currentEncoding(),enc.currentSize());
		}
	else{
		response.advance(resp.length());
		}
	}

void Agent::processSET(	BerRxEncoding&	enc,
						BerTxEncoder&	response
						) noexcept{
	unsigned long	identifier;

	identifier	= validateRequestHeader(enc);

	if(_errStatus != noError){
		// Discard
		return;
		}

	BerRxEncoding	varBindList(	enc.currentContent(),
									enc.currentContentLength()
									);

	parseSetVarBindingList(varBindList);

	if(_errStatus == noError){
		// FIXME: This should happen "atomically"
		// as desired by the client.
		Node*	node;
		for(	node	= _varBindList.first();
				node;
				node	= _varBindList.next(node)
				){
				node->_var.setFromCache();
			}
		}

	if(_errStatus == discard){
		return;
		}

	response.encodeINTEGER(identifier);
	response.encodeINTEGER(_errStatus);
	response.encodeINTEGER(_errIndex);
	response.append(enc.currentEncoding(),enc.currentSize());
	}

void Agent::processAction(	BerRxEncoding&	enc,
							BerTxEncoder&	response
							) noexcept{
	if(!enc.encodingIsValid()){
		// Discard
		_errStatus	= discard;
		return;
		}

	if(!enc.currentClassIsContextSpecific()){
		// Discard
		_errStatus	= discard;
		return;
		}

	if(enc.currentTypeIsPrimitive()){
		// Discard
		_errStatus	= discard;
		return;
		}

	BerRxEncoding	enc2(enc.currentContent(),enc.currentContentLength());

	response.encodeConstructedIdentifier(	berClassTypeContextSpecific,
											getResponseTag
											);
	BerTxEncoder	lengthEnc(	response.currentBuffer(),
								response.remaining()
								);

	response.encodeDefiniteTwoOctetLength(0);	// dummy encoding

	BerTxEncoder	resp(response.currentBuffer(),response.remaining());

	switch(enc.currentIdentifierTag()){
		case 0x00:	// GET
			processGET(enc2,resp);
			break;
		case 0x01:	// GET NEXT
			processGETNEXT(enc2,resp);
			break;
		case 0x03:	// SET
			processSET(enc2,resp);
			break;
		case 0x02:	// GET RESP
		case 0x04:	// TRAP
		default:
			_errStatus	= discard;
			return;
		};

	lengthEnc.encodeDefiniteTwoOctetLength(resp.length());
	response.advance(resp.length());
	}

unsigned Agent::processPacket(	const void*		pdu,
								unsigned		length,
								BerTxEncoder&	response
								) noexcept{
	BerRxEncoding	enc(pdu,length);
	unsigned long	version;

	_errStatus	= noError;
	_errIndex	= 0;

	freeVarBindList();

	if(!enc.encodingIsValid()){
		// Discard
		return 0;
		}

	if(!enc.currentIsSEQUENCE()){
		// Discard
		return 0;
		}

	BerRxEncoding	enc2(enc.currentContent(),enc.currentContentLength());

	if(!enc2.encodingIsValid()){
		// Discard
		return 0;
		}

	if(!enc2.currentIsINTEGER()){
		// Discard
		return 0;
		}

	version	= enc2.decodeINTEGER();

	if(version != 0){
		// Discard wrong SNMP version
		return 0;
		}

	enc2.nextEncoding();
	if(!enc2.encodingIsValid()){
		// Discard
		return 0;
		}

	if(!enc2.currentIsOCTETSTRING()){
		// Discard
		return 0;
		}

	Oscl::Strings::Fixed<32>	communityName;
	communityName.fillFromBuffer(	enc2.currentContent(),
									enc2.currentContentLength()
									);
	
	enc2.nextEncoding();

	response.encodeSequenceIdentifier();
	BerTxEncoder	lengthEnc(	response.currentBuffer(),
								response.remaining()
								);
	response.encodeDefiniteTwoOctetLength(0);	// dummy encoding

	BerTxEncoder	resp(	response.currentBuffer(),
							response.remaining()
							);
	resp.encodeINTEGER(0);
	resp.encodeOCTETSTRING(communityName,strlen(communityName));

	processAction(enc2,resp);

	if(_errStatus == discard){
		// Discard
		return 0;
		}
	lengthEnc.encodeDefiniteTwoOctetLength(resp.length());
	response.advance(resp.length());
	return response.length();
	}

