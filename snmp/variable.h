/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_snmp_variableh_
#define _oscl_snmp_variableh_
#include "result.h"
#include "oscl/ber/tx.h"
#include "oscl/ber/rx.h"

/** */
namespace Oscl {
/** */
namespace SNMP {

/*
				+-------+
	+-----------+ Node	|<----------+
	|			+---+---+			|
	|				|				|
	|		+-------+---+			|
	|		|			|			|
	|		|			|			|
	|	+---+---+	+---+-------+	|
	|	| Leaf	+	| Composite	+---+
	|	+-------+	+-----------+
	|
	|			+-------------------+
	+---------->| SNMP::Variable	|
				+-------------------+
					   / \
					  /-+-\
						|
				+-------+-------+
				|				|
			+-----------+	+---------------+
			| INTEGER	|	| OCTET STRING	|		
			+-----------+	+---------------+
*/

/**
	This abstract class represents
	GET Sequence:
		1. getToCache()					- Copy system value to local
		2. encodeFromCache(response)	- Encode local value to response
	SET Sequence:
		1. decodeToCache(value)			- Decode value to local
		2. setFromCache()				- Copy local value to system
 */
class Variable {
	public:
		/** */
		Variable() noexcept{}
		/** */
		virtual ~Variable() {}

		/** The implementation of this operation shall decode
			the value and store it locally (cache). The commit
			from cache to the appropriate system variable is
			not executed during this step.
			Possible errors include:
				- Encoding format errors. (formatError)
				- Wrong type of variable. (badValue)
				- Not a writable entity. (readOnly)
		 */
		virtual const Result::SET::Api&	decodeToCache(const BerRxEncoding& enc) noexcept=0;

		/** The implementation of this operation shall encode
			the cached value of the variable into the BER
			response buffer provided as an argument. Note that
			the cached value (updated by the getToCache() operation)
			is used NOT the system value.
			Possible errors include:
				- BER tx encoder overflow. (tooBig)
		 */
		virtual const Result::GET::Api&	encodeFromCache(BerTxEncoder& response) const noexcept=0;

		/** The implementation of this operation shall use the
			locally cached value to update the appropriate system
			configuration(s).
		 */
		virtual void	setFromCache() noexcept = 0;

		/** The implementation of this operation shall query the
			appropriate system variables and cache the value
			locally.
		 */
		virtual void	getToCache() noexcept = 0;
	};
}
}

#endif
