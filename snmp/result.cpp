/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "result.h"

using namespace Oscl::SNMP::Result;

void	GET::NoError::query(HandlerApi& rh) const noexcept{
	rh.noError();
	}

void	GET::TooBig::query(HandlerApi& rh) const noexcept{
	rh.tooBig();
	}

void	GET::GenErr::query(HandlerApi& rh) const noexcept{
	rh.genErr();
	}

const GET::NoError		GET::noError;
const GET::TooBig		GET::tooBig;
const GET::GenErr		GET::genErr;

void	SET::NoError::query(HandlerApi& rh) const noexcept{
	rh.noError();
	}

void	SET::BadValue::query(HandlerApi& rh) const noexcept{
	rh.badValue();
	}

void	SET::ReadOnly::query(HandlerApi& rh) const noexcept{
	rh.readOnly();
	}

void	SET::GenErr::query(HandlerApi& rh) const noexcept{
	rh.genErr();
	}

void	SET::FormatError::query(HandlerApi& rh) const noexcept{
	rh.formatError();
	}

const SET::NoError		SET::noError;
const SET::BadValue		SET::badValue;
const SET::ReadOnly		SET::readOnly;
const SET::GenErr		SET::genErr;
const SET::FormatError	SET::formatError;

