/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <string.h>
#include "oscl/oid/fixed.h"
#include "oscl/oid/ro/fixed.h"
#include "oscl/oid/composite.h"
#include "oscl/oid/leaf.h"
#include "oscl/oid/visitor.h"
#include "simple.h"

static SignedVar<0,0,false>	noAccessibleDummy(0);

Oscl::ObjectID::Composite<Oscl::SNMP::Variable>	oidRoot(noAccessibleDummy,1);

/** */
class AddPathVisitor : public Oscl::ObjectID::Visitor<Oscl::SNMP::Variable> {
	private:
		/** */
		Oscl::ObjectID::Node<Oscl::SNMP::Variable>&		_newNode;

		/** */
		Oscl::ObjectID::RO::Iterator	_iterator;
		
	public:
		/** */
		AddPathVisitor(	Oscl::ObjectID::Node<Oscl::SNMP::Variable>&	newNode,
						const Oscl::ObjectID::RO::Api&				path
						) noexcept:
				_newNode(newNode),
				_iterator(path)
				{
			}

		/** */
		void	visit(Oscl::ObjectID::Composite<Oscl::SNMP::Variable>& node) noexcept{
			Oscl::ObjectID::Node<Oscl::SNMP::Variable>*	child;
			if(!_iterator.more()){
				node.insertChild(_newNode);
				return;
				}
			// Assumes _iterator.more() is true. That is to say that
			// is checked by the caller.
			child	= node.findChild(_iterator.current());
			if(!child){
				// Create parent here
				// FIXME: Should use allocator
				child	= new Oscl::ObjectID::Composite< Oscl::SNMP::Variable >(noAccessibleDummy,_iterator.current());
				if(!child){
					// FIXME: Out of memory
					for(;;);
					}
				node.insertChild(*child);
				}
			_iterator.next();
			child->accept(*this);
			}

		/** */
		void	visit(Oscl::ObjectID::Leaf<Oscl::SNMP::Variable>&) noexcept{
			// Cant insert to leaf.
			// Bad path OID. Path OID must point
			// to a composite.
			for(;;);
			}
	};

static const char	systemName[]		= {"MySystemName"};
static const char	systemDescriptor[]	= {"MySystemDescriptor"};

static const Oscl::ObjectID::OidType
	doidArray[]	= {	1,3,
					9,8,
					5
					};
Oscl::ObjectID::RO::Fixed	doid(	doidArray,
									sizeof(doidArray)
									/sizeof(Oscl::ObjectID::OidType)
									);
static OctetStringVar<8,32,false>	sysNameVar(systemName,strlen(systemName));
static OctetStringVar<8,32,false>	sysDescrVar(systemDescriptor,strlen(systemDescriptor));
static SignedVar<0,255,false>		dummy1(1);
static SignedVar<0,255,true>		dummy2(2);
static SignedVar<0,255,true>		dummy3(3);
static SignedVar<0,255,true>		dummy4(4);
static SignedVar<0,255,true>		dummy5(5);
//static OidVar<0,32,false>			dummyOID(Oscl::ObjectID::Fixed<10>("1.3.9.8.5"));
static OidVar<0,32,false>			dummyOID(doid);

static Oscl::ObjectID::Leaf<Oscl::SNMP::Variable>		sysName(sysNameVar,0);
static Oscl::ObjectID::Leaf<Oscl::SNMP::Variable>		sysDescr(sysDescrVar,0);
static Oscl::ObjectID::Leaf<Oscl::SNMP::Variable>		sysOID(dummyOID,0);

static Oscl::ObjectID::Leaf<Oscl::SNMP::Variable>	something[5]	= {
																			Oscl::ObjectID::Leaf<Oscl::SNMP::Variable>(dummy1,1),
																			Oscl::ObjectID::Leaf<Oscl::SNMP::Variable>(dummy2,2),
																			Oscl::ObjectID::Leaf<Oscl::SNMP::Variable>(dummy3,3),
																			Oscl::ObjectID::Leaf<Oscl::SNMP::Variable>(dummy4,4),
																			Oscl::ObjectID::Leaf<Oscl::SNMP::Variable>(dummy5,5)
																			};

static void	addNode(Oscl::ObjectID::Node<Oscl::SNMP::Variable>& node,Oscl::ObjectID::Api& path) noexcept{
	AddPathVisitor				addVisitor(node,path);
	oidRoot.accept(addVisitor);
	}

void	buildMibTree() noexcept{
	Oscl::ObjectID::Fixed<32>	fixed;
	Oscl::ObjectID::Api&		path	= fixed;
	path	= "1.3.6.1.2.1.1.1";
	addNode(sysDescr,path);
	path	= "1.3.6.1.2.1.1.2";
	addNode(sysOID,path);
//	path	= "1.3.6.1.2.1.1.3";	sysUpTime
//	path	= "1.3.6.1.2.1.1.4";	sysContact
	path	= "1.3.6.1.2.1.1.5";
//	path	= "1.3.6.1.2.1.1.6";	sysLocation
//	path	= "1.3.6.1.2.1.1.7";	sysServices
	addNode(sysName,path);
	path	= "1.3.3.4";
	for(unsigned i=0;i<5;++i){
		addNode(something[i],path);
		}
	}

