/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_snmp_srv_serverh_
#define _oscl_snmp_srv_serverh_
#include "oscl/memory/block.h"
#include "oscl/protocol/socket/connectionless/tx/api.h"
#include "oscl/protocol/socket/connectionless/rx/api.h"
#include "oscl/snmp/agent.h"
#include "oscl/mt/runnable.h"

/** */
namespace Oscl {
/** */
namespace SNMP {
/** */
namespace Srv {

/** */
class Server : public Oscl::Mt::Runnable {
	private:
		/** */
		enum{maxNodes=10};
		/** */
		Oscl::SNMP::NodeMem				_nodeMem[maxNodes];
		/** */
		Oscl::SNMP::Agent				_snmp;

		/** */
		Oscl::Protocol::Socket::
		Connectionless::TX::Api&		_txSocket;

		/** */
		Oscl::Protocol::Socket::
		Connectionless::RX::Api&	_rxSocket;

		/** */
		enum{txBufferSize=1024};
		/** */
		enum{rxBufferSize=1024};

		/** */
		Oscl::Memory::AlignedBlock<txBufferSize>	_snmpTxBuffer;
		/** */
		Oscl::Memory::AlignedBlock<rxBufferSize>	_snmpRxBuffer;

	public:
		/** */
		Server(	Oscl::Protocol::Socket::
				Connectionless::TX::Api&	txSocket,
				Oscl::Protocol::Socket::
				Connectionless::RX::Api&	rxSocket
				) noexcept;
	private:
		/** */
		void	run() noexcept;
	};

}
}
}

#endif
