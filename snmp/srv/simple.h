/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_snmp_srv_simpleh_
#define _oscl_snmp_srv_simpleh_

#include <string.h>
#include "oscl/snmp/signed.h"
#include "oscl/snmp/octetstring.h"
#include "oscl/snmp/oid.h"

template <int minVal,int maxVal,bool readWrite>
class SignedVar : public Oscl::SNMP::Signed<minVal,maxVal,readWrite> {
	private:
		signed long	_value;
	public:
		SignedVar(signed long value=0) noexcept:_value(value) {}
		void	update(signed long value) noexcept{_value=value;}
	private:	// SNMP::Variable
		void	setFromCache() noexcept{
			if(readWrite){
				_value = this->_cache;
				}
			}
		void	getToCache() noexcept{ this->_cache = this->_value; }
	};

template <int minLen,int maxLen,bool readWrite>
class OctetStringVar : public Oscl::SNMP::OctetString<minLen,maxLen,readWrite> {
	private:
		unsigned char	_value[maxLen];
		unsigned		_vLength;

	public:
		OctetStringVar(const void* value,unsigned length) noexcept {
			update(value,length);
			}
		void	update(const void* value,unsigned length) noexcept{
			_vLength	= (length > maxLen)?maxLen:length;
			memcpy(_value,value,_vLength);
			}
	private:	// SNMP::Variable
		void	setFromCache() noexcept{
			memcpy(this->_value,this->_cache,this->_length);
			_vLength	= this->_length;
			}
		void	getToCache() noexcept{
			memcpy(this->_cache,this->_value,this->_vLength);
			this->_length	= _vLength;
			}
	};

template <int minLen,int maxLen,bool readWrite>
class OidVar : public Oscl::SNMP::OID<minLen,maxLen,readWrite> {
	private:
		Oscl::ObjectID::Fixed<maxLen>	_value;

	public:
		OidVar(const Oscl::ObjectID::RO::Api& value) noexcept {
			update(value);
			}
		void	update(const Oscl::ObjectID::RO::Api& value) noexcept{
			Oscl::ObjectID::Api&	v	= this->_value;
			v	= value;
			}
	private:	// SNMP::Variable
		void	setFromCache() noexcept{
			this->_value	= this->_cache;
			}
		void	getToCache() noexcept{
			this->_cache	= this->_value;
			}
	};

#endif
