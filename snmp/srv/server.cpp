/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "server.h"
#include "mib.h"
#include "oscl/error/info.h"
#include "oscl/mt/thread.h"
#include "oscl/protocol/socket/connectionless/fixed.h"

using namespace Oscl::SNMP::Srv;

Server::Server(	Oscl::Protocol::Socket::
				Connectionless::TX::Api&	txSocket,
				Oscl::Protocol::Socket::
				Connectionless::RX::Api&	rxSocket
				) noexcept:
		_snmp(oidRoot,_nodeMem,maxNodes),
		_txSocket(txSocket),
		_rxSocket(rxSocket)
		{
	buildMibTree();
	}

void	Server::run() noexcept{
	while(true){
		BerTxEncoder	response(&_snmpTxBuffer,txBufferSize);
		Oscl::Protocol::Socket::Connectionless::FixedPeer<20>	peer;
		unsigned	length;
		length	= _rxSocket.recv(peer,&_snmpRxBuffer,rxBufferSize);
		if(!length) break;
		length	= _snmp.processPacket(&_snmpRxBuffer,length,response);
		if(!length) continue;
		const Oscl::Protocol::Socket::Connectionless::TxError*
		txError	= _txSocket.send(peer,&_snmpTxBuffer,length);
		if(txError) break;
		}
	Oscl::Error::Info::log("UDP Socket Closed");
	while(true){
		Oscl::Mt::Thread::sleep(1000);
		}
	}

