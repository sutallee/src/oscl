/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_snmp_signedh_
#define _oscl_snmp_signedh_
#include "variable.h"
#include "oscl/ber/rx.h"
#include "oscl/ber/tx.h"

/** */
namespace Oscl {
/** */
namespace SNMP {

/**
	This abstract class represents
	GET Sequence:
		1. getToCache()					- Copy system value to local
		2. encodeFromCache(response)	- Encode local value to response
	SET Sequence:
		1. decodeToCache(value)			- Decode value to local
		2. setFromCache()				- Copy local value to system
 */
template <int minValue,int maxValue,bool readWrite>
class Signed : public Variable {
	protected:
		/** */
		signed long	_cache;

	public:
		/** */
		Signed(signed long value=0) noexcept:_cache(value){}

		/** The implementation of this operation shall decode
			the value and store it locally (cache). The commit
			from cache to the appropriate system variable is
			not executed during this step.
			Possible errors include:
				- Encoding format errors. (formatError)
				- Wrong type of variable. (badValue)
				- Not a writable entity. (readWrite)
		 */
		const Result::SET::Api&	decodeToCache(const BerRxEncoding& enc) noexcept;

		/** The implementation of this operation shall encode
			the cached value of the variable into the BER
			response buffer provided as an argument. Note that
			the cached value (updated by the getToCache() operation)
			is used NOT the system value.
			Possible errors include:
				- buffer no large enough to hold encoding. (tooBig)
		 */
		const Result::GET::Api&	encodeFromCache(BerTxEncoder& response) const noexcept;

		/** The implementation of this operation shall use the
			locally cached value to update the appropriate system
			configuration(s).
		 */
		virtual void	setFromCache() noexcept=0;

		/** The implementation of this operation shall query the
			appropriate system variables and cache the value
			locally.
		 */
		virtual void	getToCache() noexcept=0;
	};

template <int minValue,int maxValue,bool readWrite>
const Result::SET::Api&
	Signed<minValue,maxValue,readWrite >::
	decodeToCache(const BerRxEncoding& enc) noexcept{
	if(!readWrite){
		return Result::SET::readOnly;
		}
	// 	Encoding is assumed to have already been validated.
	if(!enc.currentIsINTEGER()){
		return Result::SET::badValue;
		}
	_cache	= enc.decodeINTEGER();

	if(_cache > maxValue){
		return Result::SET::badValue;
		}
	else if(_cache < minValue){
		return Result::SET::badValue;
		}

	return Result::SET::noError;
	}

template <int minValue,int maxValue,bool readWrite>
const Result::GET::Api&
	Signed<minValue,maxValue,readWrite >::
	encodeFromCache(BerTxEncoder& response) const noexcept{

	response.encodeINTEGER(_cache);
	if(response.tooBig()){
		return Result::GET::tooBig;
		}
	return Result::GET::noError;
	}

}
}

#endif
