/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_snmp_resulth_
#define _oscl_snmp_resulth_

/** */
namespace Oscl {
/** */
namespace SNMP {
/** */
namespace Result {
/** */
namespace GET {

/** */
class HandlerApi {
	public:
		/** Make GCC happy */
		virtual ~HandlerApi() {}
		/** */
		virtual void	noError() noexcept = 0;
		/** */
		virtual void	tooBig() noexcept = 0;
		/** */
		virtual void	genErr() noexcept = 0;
		/** */
		virtual void	formatError() noexcept = 0;
	};

/** */
class Api {
	public:
		/** Make GCC happy */
		virtual ~Api() {}
		/** */
		virtual void	query(HandlerApi& rh) const noexcept=0;
	};

/** */
class NoError : public Api {
	public:
		/** */
		void	query(HandlerApi& rh) const noexcept;
	};

/** */
class TooBig : public Api {
	public:
		/** */
		void	query(HandlerApi& rh) const noexcept;
	};

/** */
class GenErr : public Api {
	public:
		/** */
		void	query(HandlerApi& rh) const noexcept;
	};

extern const NoError		noError;
extern const TooBig			tooBig;
extern const GenErr			genErr;

}

/** */
namespace SET {

/** */
class HandlerApi {
	public:
		/** Make GCC happy */
		virtual ~HandlerApi() {}
		/** */
		virtual void	noError() noexcept = 0;
		/** */
		virtual void	badValue() noexcept = 0;
		/** */
		virtual void	readOnly() noexcept = 0;
		/** */
		virtual void	genErr() noexcept = 0;
		/** */
		virtual void	formatError() noexcept = 0;
	};

/** */
class Api {
	public:
		/** Make GCC happy */
		virtual ~Api() {}
		/** */
		virtual void	query(HandlerApi& rh) const noexcept=0;
	};

/** */
class NoError : public Api {
	public:
		/** */
		void	query(HandlerApi& rh) const noexcept;
	};

/** */
class BadValue : public Api {
	public:
		/** */
		void	query(HandlerApi& rh) const noexcept;
	};

/** */
class ReadOnly : public Api {
	public:
		/** */
		void	query(HandlerApi& rh) const noexcept;
	};

/** */
class GenErr : public Api {
	public:
		/** */
		void	query(HandlerApi& rh) const noexcept;
	};

/** */
class FormatError : public Api {
	public:
		/** */
		void	query(HandlerApi& rh) const noexcept;
	};

extern const NoError		noError;
extern const BadValue		badValue;
extern const ReadOnly		readOnly;
extern const GenErr			genErr;
extern const FormatError	formatError;

}

}
}
}

#endif

