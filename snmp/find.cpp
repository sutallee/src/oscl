/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "oscl/oid/composite.h"
#include "oscl/oid/leaf.h"
#include "find.h"

using namespace Oscl::SNMP;

Find::Find(const Oscl::ObjectID::RO::Api& oid) noexcept:
		_foundNode(0),
		_iterator(oid)
		{
	}

void	Find::visit(Oscl::ObjectID::Composite<Variable>& node) noexcept{
	Oscl::ObjectID::Node<Variable>*	child;
	if(!_iterator.more()){
		return;
		}
	// Assumes _iterator.more() is true. That is to say that
	// is checked by the caller.
	child	= node.findChild(_iterator.current());
	if(!child){
		// noSuchName
		return;
		}
	_iterator.next();
	child->accept(*this);
	}

void	Find::visit(Oscl::ObjectID::Leaf<Variable>& node) noexcept{
	if(_iterator.more()){
		// noSuchName
		return;
		}
	_foundNode	= &node;
	}

////////////////////////////////

FindNext::FindNext(	const Oscl::ObjectID::RO::Api&	predecessor,
					Oscl::ObjectID::Api&			output
					) noexcept:
		_first(true),
		_foundNode(0),
		_iterator(predecessor),
		_output(output),
		_foundPredecessor(false)
		{
	}

void	FindNext::visit(Oscl::ObjectID::Composite<Variable>& node) noexcept{
	Oscl::ObjectID::Node<Variable>*	child;

	if(_foundPredecessor){
		child	= node.firstChild();
		if(!child) return;
		_output	+= node._id;
		child->accept(*this);
		if(_foundNode) return;
		_output.truncate(1);
		return;
		}
	else {
		if(_iterator.more()){
			// Assumes _iterator.more() is true. That is to say that
			// is checked by the caller.
			child	= node.findChild(_iterator.current());
			if(!child){
				// noSuchName
				_foundPredecessor	= true;
				return;
				}
			_iterator.next();
			if(_first){
				_first	= false;
				}
			else{
				_output	+= node._id;
				}
			child->accept(*this);
			if(_foundPredecessor){
				if(_foundNode) return;
				for(	child = node.nextChild(*child);
						child;
						child = node.nextChild(*child)
						){
					child->accept(*this);
					if(_foundNode) return;
					}
				_output.truncate(1);
				}
			}
		else {
			_foundPredecessor	= true;
			if(_first){
				_first	= false;
				}
			else{
				_output	+= node._id;
				}
			child	= node.firstChild();
			if(!child){
				_output.truncate(1);
				return;
				}
			child->accept(*this);
			return;
			}
		}
	}

void	FindNext::visit(Oscl::ObjectID::Leaf<Variable>& node) noexcept{
	if(_foundPredecessor){
		_output	+= node._id;
		_foundNode	= &node;
		}
	else {
		if(_iterator.more()){
			// If the predecessor MUST be an actual node,
			// then this behavior is correct since there
			// is no existing predecessor. Otherwise, this
			// condition can be simplified to simply
//			_foundPredecessor	= true;
			return;
			}
		_foundPredecessor	= true;
		}
	}

