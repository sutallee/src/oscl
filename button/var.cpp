/*
   Copyright (C) 2020 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "var.h"
/** */
namespace Oscl {

/** */
namespace Button {

class VarIsRelease : public Var::Query {
	public:
		bool	_match;
	public:
		inline void release() noexcept{
			_match	= true;
			}
		inline void press() noexcept{
			_match	= false;
			}
	};

class VarIsPress : public Var::Query {
	public:
		bool	_match;
	public:
		inline void release() noexcept{
			_match	= false;
			}
		inline void press() noexcept{
			_match	= true;
			}
	};

class GetState : public Var::Query {
	public:
		const Var::State*	_state;
	public:
		inline void release() noexcept{
			_state	= &Var::getRelease();
			}
		inline void press() noexcept{
			_state	= &Var::getPress();
			}
	};
class VarRelease : public Var::State {
	public:
		VarRelease(){}
		bool	operator == (const Var::State& other) const noexcept;
		bool	operator != (const Var::State& other) const noexcept;
		void	accept(Var::Query& q) const noexcept;
	};

class VarPress : public Var::State {
	public:
		VarPress(){}
		bool	operator == (const Var::State& other) const noexcept;
		bool	operator != (const Var::State& other) const noexcept;
		void	accept(Var::Query& q) const noexcept;
	};

}
}
using namespace Oscl::Button;
static const VarRelease	_release;
static const VarPress	_press;
bool VarRelease::operator == (const Var::State& other) const noexcept{
	VarIsRelease	query;
	other.accept(query);
	return query._match;
	}

bool VarRelease::operator != (const Var::State& other) const noexcept{
	VarIsRelease	query;
	other.accept(query);
	return !query._match;
	}

void VarRelease::accept(Var::Query& q) const noexcept{
	q.release();
	}

bool VarPress::operator == (const Var::State& other) const noexcept{
	VarIsPress	query;
	other.accept(query);
	return query._match;
	}

bool VarPress::operator != (const Var::State& other) const noexcept{
	VarIsPress	query;
	other.accept(query);
	return !query._match;
	}

void VarPress::accept(Var::Query& q) const noexcept{
	q.press();
	}


Var::Var(const State& initial) noexcept:
		_state(&initial)
		{
	}

void Var::accept(Query& q) noexcept{
	_state->accept(q);
	}

bool Var::operator == (const Var& other) const noexcept{
	return *other._state == *_state;
	}

bool Var::operator != (const Var& other) const noexcept{
	return *other._state != *_state;
	}

void Var::operator = (const Var& newState) noexcept{
	*this	= *newState._state;
	}

void Var::operator = (const State& newState) noexcept{
	GetState	getState;
	newState.accept(getState);
	_state	= getState._state;
	}

const Var::State& Var::getCurrentState() const noexcept{
	return *_state;
	}

void Var::release() noexcept{
	_state	= &_release;
	}

void Var::press() noexcept{
	_state	= &_press;
	}

const Var::State*	Var::get(long num) noexcept{
	switch(num){
		case 0:
			return &_release;
		case 1:
			return &_press;
		default:
			return 0;
		};
	}

const Var::State&	Var::getRelease() noexcept{
	return _release;
	}

const Var::State&	Var::getPress() noexcept{
	return _press;
	}

