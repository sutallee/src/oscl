/*
   Copyright (C) 2020 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_button_varh_
#define _oscl_button_varh_

/** */
namespace Oscl {

/** */
namespace Button {

/**	This Var class implements an EMORF. The Flyweight Pattern
	is used in the implementation of the state subclasses
	allowing a single set of concrete state classes to be shared
	between any number of instances of this EMORF.
 */
/**	This EMORF variable (Var) describes the states for a
	button.
 */
class Var {
	public:
		/**	This interface a kind of GOF visitor
			that allows the state of the variable
			to be determined/queried.
		 */
		class Query {
			public:
				/** Make the compiler happy. */
				virtual ~Query() {}
				/**	This operation is invoked by the variable
					when the variable is in the Release state.
				 */
				virtual void	release() noexcept=0;

				/**	This operation is invoked by the variable
					when the variable is in the Press state.
				 */
				virtual void	press() noexcept=0;

			};
	public:
		/**	This interface defines the operations that are
			supported by all of the states that the variable
			can assume. Sub-classes of this variable are to
			have no state (member variables) of their own.
			This allows a GOF fly-weight pattern to be used
			such that many instances of the Var can share the
			same concrete state instances.
		 */
		class State {
			public:
				/** Make the compiler happy. */
				virtual ~State() {}

				/**	Compare this state with another state for equality.
				 */
				virtual bool	operator ==(const State& other) const noexcept=0;

				/**	Compare this state with another state for inequality.
				 */
				virtual bool	operator !=(const State& other) const noexcept=0;

				/** Allow the concrete state to be queried to
					determine the actual state.
				 */
				virtual void	accept(Query& q) const noexcept=0;

			};
	private:
		/**	This member determines the current state of the EMORF.
		 */
		const State*	_state;

	public:
		/**	The constructor requires a reference to its context.
		 */
		Var(const State& initial=getRelease()) noexcept;

		/** Be happy compiler! */
		virtual ~Var() {}

	public:
		/**	This operation is invoked to determine the current
			state of the variable via visitation.
		 */
		void	accept(Query& q) noexcept;

		/**	Compare the state of this Var with another for equality.
		 */
		bool	operator == (const Var& other) const noexcept;

		/**	Compare the state of this Var with another for inequality.
		 */
		bool	operator != (const Var& other) const noexcept;

		/**	Assign the state of another Var to this var.
		 */
		void	operator=(const Var& newState) noexcept;

		/**	Assign the specified state to this Var.
		 */
		void	operator=(const State& newState) noexcept;

		/**	Returns a reference to the current state.
		 */
		const State&	getCurrentState() const noexcept;

	public:
		/** This operation is invoked to change the EMORF
			to the Release state.
		 */
		void release() noexcept;

		/** This operation is invoked to change the EMORF
			to the Press state.
		 */
		void press() noexcept;

	public:
		/** This operation returns an EMORF state pointer
			matching the numeric state, or zero if there is
			no matching state.
		 */
		static const State* get(long num) noexcept;

		/** This operation returns an EMORF state reference
			to the Release state.
		 */
		static const State& getRelease() noexcept;

		/** This operation returns an EMORF state reference
			to the Press state.
		 */
		static const State& getPress() noexcept;

	};
}
}
#endif
