/*
   Copyright (C) 2020 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_button_output_parth_
#define _oscl_button_output_parth_

#include "oscl/button/observer/reqapi.h"
#include "oscl/queue/queue.h"
#include "oscl/button/control/api.h"

/** */
namespace Oscl {

/** */
namespace Button {

/** */
namespace Output {

/** */
class Part :
		public Oscl::Button::Observer::Req::Api,
		public Oscl::Button::Control::Api
		{
	private:
		/** */
		Oscl::Button::Observer::Req::Api::ConcreteSAP	_sap;

		/** */
		Oscl::Queue<Oscl::Button::Observer::Req::Api::ChangeReq>	_noteQ;

		/** This variable contains the current state of the
			monitor point.
		 */
		Oscl::Button::Var							_state;

	public:
		/** */
		Part(
			Oscl::Mt::Itc::PostMsgApi&		papi,
			const Oscl::Button::Var::State&	initialState
			) noexcept;

		/** */
		Oscl::Button::Observer::Req::Api::SAP&	getSAP() noexcept;

	private:
		/** */
		void	notify(const Oscl::Button::Var::State& state) noexcept;

	public: // Oscl::Button::Control::Api
		/** */
		void	update(const Oscl::Button::Var::State& state) noexcept;

	public:	// Oscl::Button::Observer::Req::Api
		/** */
		void request(Oscl::Button::Observer::Req::Api::ChangeReq& msg) noexcept;

		/** */
		void request(Oscl::Button::Observer::Req::Api::CancelChangeReq& msg) noexcept;
	};

}
}
}

#endif
