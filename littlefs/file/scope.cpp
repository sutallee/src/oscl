/*
   Copyright (C) 2020 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <errno.h>
#include <string.h>
#include "scope.h"
#include "oscl/strings/base.h"

using namespace Oscl::LittleFS::File;

Scope::Scope(
	lfs_t&		lfs,
	const char* fileName,
	bool		create
	) noexcept:
	_lfs( lfs )
	{

	_file.cache.buffer	= _cache;

	_cfg.buffer		= _buffer;
	_cfg.attrs		= nullptr;
	_cfg.attr_count	= 0;

	if( !create ){
		struct lfs_info	entry;

		_openResult	= lfs_stat(
						&_lfs,	// lfs_t*
						fileName,	// const char*
						&entry	// struct lfs_info *
						);

		if( _openResult < 0 ){
			return;
			}

		if( entry.type != LFS_TYPE_REG ){
			// The fileName exists, but it is not a file.
			_openResult	= -EISDIR;
			return;
			}
		}

    _openResult = lfs_file_opencfg(
					&_lfs,		// lfs_t *lfs,
					&_file,		// lfs_file_t *file,
					fileName,	// const char *path,
					LFS_O_RDWR | LFS_O_CREAT,	// int flags,
					&_cfg		// const struct lfs_file_config *config
					);
	}

Scope::Scope( lfs_t& lfs ) noexcept:
		_lfs( lfs ),
		_openResult( -ENOENT )
		{
	}

Scope::~Scope() noexcept{
	if( _openResult ){
		return;
		}

	lfs_file_close(
		&_lfs,
		&_file
		);
	}

int	Scope::openResult() noexcept{
	return _openResult;
	}

bool	Scope::mkstemp( char* templateName ) noexcept{

	if( !_openResult ){
		// The current is already open. Close it.
		lfs_file_close(
			&_lfs,
			&_file
			);
		_openResult	= -ENOENT;
		}

	int
	templateNameLen	= strlen( templateName );

	if( templateNameLen < 6 ){
		// The last six characters of the string
		// are replaced by the new name. We must
		// have space for that and we don't.
		return true;
		}

	int
	baseNameLength	= templateNameLen-6;

	for( unsigned i=0; i < ( 1<<16 ); ++i ){
		Oscl::Strings::Base
			t(
				&templateName[ baseNameLength ],
				6+1
				);

		t	+= i;

		struct lfs_info	entry;

		int
		result	= lfs_stat(
					&_lfs,	// lfs_t*
					templateName,	// const char*
					&entry	// struct lfs_info *
					);

		if( result < 0 ){
			if( result != -ENOENT ){
				// Something wierd happend.
				return true;
				}
			}

		// At this point we have determined that
		// there is no entity with our chosen name.

    	_openResult = lfs_file_opencfg(
						&_lfs,		// lfs_t *lfs,
						&_file,		// lfs_file_t *file,
						templateName,	// const char *path,
						LFS_O_RDWR | LFS_O_CREAT,	// int flags,
						&_cfg		// const struct lfs_file_config *config
						);

		return _openResult?true:false;
		}

	// Out of numbers!
	// This should never happen.
	return true;
	}

ssize_t	Scope::read(
			void*	ptr,
			size_t	size
			) noexcept{

	return lfs_file_read(
				&_lfs,
				&_file,
				ptr,
				size
				);
	}

ssize_t	Scope::write(
			const void*	ptr,
			size_t		size
			) noexcept{

	return lfs_file_write(
				&_lfs,
				&_file,
				ptr,
				size
				);
	}

int	Scope::seekFromBeginning(
		off_t	offset
		) noexcept{

	return lfs_file_seek(
			&_lfs,	// lfs_t *lfs,
			&_file,	// lfs_file_t *file,
			offset,	// lfs_soff_t off,
			LFS_SEEK_SET	// int whence
			);
	}

int	Scope::seekFromCurrentPosition(
		off_t	offset
		) noexcept{

	return lfs_file_seek(
			&_lfs,	// lfs_t *lfs,
			&_file,	// lfs_file_t *file,
			offset,	// lfs_soff_t off,
			LFS_SEEK_CUR	// int whence
			);
	}

int	Scope::seekFromEnd(
		off_t	offset
		) noexcept{

	return lfs_file_seek(
			&_lfs,	// lfs_t *lfs,
			&_file,	// lfs_file_t *file,
			offset,	// lfs_soff_t off,
			LFS_SEEK_END	// int whence
			);
	}



off_t	Scope::tell() noexcept{

	return lfs_file_tell(
			&_lfs,	// lfs_t *lfs,
			&_file	// lfs_file_t *file
			);
	}


int	Scope::truncate( off_t length ) noexcept{

	return lfs_file_truncate(
				&_lfs,	// lfs_t *lfs,
				&_file,	// lfs_file_t *file,
				length	// lfs_off_t size
				);
	}

int	Scope::sync() noexcept{
	return lfs_file_sync(
			&_lfs,	// lfs_t *lfs,
			&_file	// lfs_file_t *file
			);
	}

