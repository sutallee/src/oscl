/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_iterator_queueh_
#define _oscl_iterator_queueh_
#include "api.h"
#include "oscl/queue/queue.h"

/* NOTE!:
	This file is under development.
	This file is un-tested.
	This file is volatile and may/will change,
	and may be deleted.
 */

/** */
namespace Oscl {
/** */
namespace Iterator {

/** */
template <class concreteType,class abstractType>
class Queue : public Api<abstractType> {
	private:
		/** */
		Oscl::Queue<concreteType>&	_queue;
		/** */
		concreteType*				_current;
	public:
		/** */
		Queue(Oscl::Queue<concreteType>& queue) noexcept;

	public: // Api<abstractType>
		/** */
		void	first() noexcept;
		/** */
		void	next() noexcept;
		/** */
		abstractType*	current() noexcept;
	};

};
};

template <class concreteType,class abstractType>
Oscl::Iterator::Queue<concreteType,abstractType>::
Queue(Oscl::Queue<concreteType>& queue) noexcept:
		_queue(queue),
		_current(0)
		{
	}

template <class concreteType,class abstractType>
void	Oscl::Iterator::Queue<concreteType,abstractType>::
first() noexcept{
	_current	= _queue.first();
	}

template <class concreteType,class abstractType>
void	Oscl::Iterator::Queue<concreteType,abstractType>::
next() noexcept{
	if(!_current) return;
	_current	= _queue.next(_current);
	}

template <class concreteType,class abstractType>
abstractType*	Oscl::Iterator::Queue<concreteType,abstractType>::
current() noexcept{
	return _current;
	}

#endif
