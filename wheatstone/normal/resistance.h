/*
   Copyright (C) 2021 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_wheatstone_normal_resistanceh_
#define _oscl_wheatstone_normal_resistanceh_

/** */
namespace Oscl {

/** */
namespace Wheatstone {

/** */
namespace Normal {

/** */
namespace Resistance {

#if 0
/**	This function calculates a normalized percentage
	of resistance.

	It is assumed that the supply voltage
	across the bridge is normalized to a value of 1 Volt.

	normalizedVoltage is a value between 0 and full scale
	(1 Volt). This is common when an A/D Converter is used
	and the unsigned integer value returned is an n-bit
	value where the full scale value is 2^n - 1. Thus, the
	normalized voltage is calculated as the value read
	from the A/D Converter divided by the full-scale
	reading (2^n - 1)

	RETURN: The value returned is a normalized resistance
	in the range of 0 to 1. The client can multiply this
	number times the value of the fixed resistors in the
	bridge to calculate the unknown resistance Rx. Rx is
	typically a sensor whose resistance changes due to
	some external stimulus (e.g. Thermistor). Therfore,
	Rx can be used to find the meaning of the resistance
	for that sensor (e.g. temperature for a Thermistor).
*/
template <const double& R1, const double& R2, const double& R3>
double	normalizedRx(double normalizedVoltage) noexcept{

	double	Vg	= normalizedVoltage;
	constexpr double	Vs	= 1.0;

	/*	Full equation
		Rx	= ((Vs*R2) - (Vg*(R1+R2))) / ((Vs*R1) + (Vg*(R1+R2)));
		Where:
			Vs is the input voltage across the bridge.
			Vg is the differential output voltage.

		For normalization, we assume
			Vs	= 1.0

		Substituting:

			Rx	= ((1.0*R2) - (Vg*(R1+R2))) / ((1.0*R1) + (Vg*(R1+R2)));
			Rx	= ((R2) - (Vg*(R1+R2))) / ((R1) + (Vg*(R1+R2)));
			Rx	= (R2 - (Vg*(R1+R2))) / (R1 + (Vg*(R1+R2)));
	 */

	double	Rx	= (R2 - (Vg*(R1+R2))) / (R1 + (Vg*(R1+R2)));

	return Rx;
	}
#endif

/**	This function calculates a normalized percentage
	of resistance. The assumption is that all resistors
	in the bridge are 1 Ohm with the exception of Rx.

	Furthermore, it is assumed that the supply voltage
	across the bridge is normalized to a value of 1 Volt.

	normalizedVoltage is a value between 0 and full scale
	(1 Volt). This is common when an A/D Converter is used
	and the unsigned integer value returned is an n-bit
	value where the full scale value is 2^n - 1. Thus, the
	normalized voltage is calculated as the value read
	from the A/D Converter divided by the full-scale
	reading (2^n - 1)

	RETURN: The value returned is a normalized resistance
	in the range of 0 to 1. The client can multiply this
	number times the value of the fixed resistors in the
	bridge to calculate the unknown resistance Rx. Rx is
	typically a sensor whose resistance changes due to
	some external stimulus (e.g. Thermistor). Therfore,
	Rx can be used to find the meaning of the resistance
	for that sensor (e.g. temperature for a Thermistor).
*/
double	normalizedRx(double normalizedVoltage) noexcept;

/*	This function calculates the unknown resistance (Rx) for
	a Wheatstone bridge in which all other fixed resistors
	have the value commonResistance.

	normalizedVoltage is a value between 0 and full scale
	(1 Volt). This is common when an A/D Converter is used
	and the unsigned integer value returned is an n-bit
	value where the full scale value is 2^n - 1. Thus, the
	normalized voltage is calculated as the value read
	from the A/D Converter divided by the full-scale
	reading (2^n - 1)

	commonResistance is the value of the three fixed
	resistors in the wheatstone bridge.

	RETURN: The unknown resistance Rx.
 */
double	calculateRx(double normalizedVoltage,double commonResistance) noexcept;

#if 0
/*
 */
template <const double& R1, const double& R2, const double& R3>
double	calculateRx(double normalizedVoltage) noexcept{

	double	Vg	= normalizedVoltage;

	double	nRx	= normalizedRx<R1,R2,R3>(Vg);

	return nRx*R3;
	}
#endif

}
}
}
}

#endif
