/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_pcisrv_enum_serverh_
#define _oscl_pcisrv_enum_serverh_
#include "oscl/mt/itc/mbox/server.h"
#include "oscl/driver/pci/config.h"
#include "oscl/extalloc/driver.h"
#include "oscl/mt/itc/dyn/adv/core.h"
#include "oscl/mt/itc/dyn/adv/creatorsync.h"
#include "oscl/pcisrv/func/server.h"

/** */
namespace Oscl {
/** */
namespace PciSrv {
/** */
namespace Enum {

/** */
class Server : public Oscl::Mt::Itc::Server {
	private:
		/** */
		Oscl::ExtAlloc::Driver				_pciAllocator;
		/** */
		Oscl::Pci::Config::Api&				_pciApi;
		/** */
		Oscl::Mt::Itc::PostMsgApi&			_dynSrvPostApi;
		/** */
		Oscl::Mt::Itc::PostMsgApi&			_advPostApi;
		/** */
		Oscl::Mt::Itc::Dyn::Adv::
		Core<Func::Server>::ReleaseApi&		_advertiser;
		/** */
		Oscl::Mt::Itc::Dyn::Adv::
		Creator::SyncApi<Func::Server>&		_advSyncApi;
		/** */
		Oscl::Interrupt::Shared::SourceApi&	_irqSource;
		/** */
		const unsigned						_bridgeOidType;
		/** */
		const unsigned						_slotOidType;
		/** */
		const unsigned						_functionOidType;
		/** */
		const unsigned						_bridgeID;
		
	public:
		/** FIXME: Since this enumeration function should not be specific
			to any particular mother board arangment, the logic for interrupt
			routing should be supplied through an external context. Alas,
			lazy-ness strikes and the current implementation assumes all
			interrupts are routed to a INTA as a shared interrupt source.
		 */
		Server(	Oscl::Pci::Config::Api&				pciApi,
				unsigned long						startOfPciAddressSpace,
				unsigned long						sizeOfPciAddressSpace,
				Oscl::Mt::Itc::PostMsgApi&			dynSrvPostApi,
				Oscl::Mt::Itc::PostMsgApi&			advPostApi,
				Oscl::Mt::Itc::Dyn::Adv::
				Core<Func::Server>::ReleaseApi&		advertiser,
				Oscl::Mt::Itc::Dyn::Adv::
				Creator::SyncApi<Func::Server>&		advSyncApi,
				Oscl::Interrupt::Shared::SourceApi&	irqSource,
				unsigned							bridgeOidType,
				unsigned							slotOidType,
				unsigned							functionOidType,
				unsigned							bridgeID
				) noexcept;

	private:
		/** */
		void	initialize() noexcept;
		/** */
		void	mboxSignaled() noexcept;

	private:
		/** */
		void	enumerate() noexcept;
		/** */
		void	enumerateBus(unsigned char bus) noexcept;
		/** */
		void	enumerateDevice(unsigned char bus,unsigned char device) noexcept;
		/** */
		void	enumerateFunction(	unsigned char	bus,
									unsigned char	device,
									unsigned char	function
									) noexcept;
		/** */
		void	enumerateStandard(	unsigned char	bus,
									unsigned char	device,
									unsigned char	function
									) noexcept;
		/** */
		void	enumeratePciToPciBridge() noexcept;
		/** */
		void	enumeratePciToCardBusBridge() noexcept;
	};

}
}
}

#endif
