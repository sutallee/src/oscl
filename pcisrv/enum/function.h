/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_pcisrv_enum_functionh_
#define _oscl_pcisrv_enum_functionh_
#include "oscl/pcisrv/func/server.h"
#include "oscl/extalloc/api.h"
#include "oscl/driver/pci/stdfunc.h"

/** */
namespace Oscl {
/** */
namespace PciSrv {
/** */
namespace Enum {

/** */
class Function :	public Oscl::PciSrv::Func::Server,
					public Oscl::PciSrv::Func::InfoApi
		{
	private:
		/** */
		Oscl::Pci::Config::Function::HeaderTypeZero	_func;
		/** */
		Oscl::Interrupt::Shared::SourceApi&			_irqSource;
		/** */
		enum{MaxBaseAddresses=6};
		/** */
		Oscl::ExtAlloc::Record						_bar[MaxBaseAddresses];
		/** */
		Location									_location;
		/** */
		bool										_allocated;
		/** */
		Oscl::Pci::Config::Api&						_pciConfigApi;
		/** */
		unsigned char								_bus;
		/** */
		unsigned char								_device;
		/** */
		unsigned char								_function;

	public:
		/** */
		Function(	Oscl::Pci::Config::Api&				pciConfigApi,
					unsigned char						bus,
					unsigned char						device,
					unsigned char						function,
					unsigned char						chassisID,
					const Oscl::ObjectID::RO::Api&		path,
					Oscl::Mt::Itc::PostMsgApi&			dynSrvPostApi,
					Oscl::Mt::Itc::PostMsgApi&			advPostApi,
					Oscl::Mt::Itc::Dyn::Adv::
					Core<Func::Server>::ReleaseApi&		advertiser,
					Oscl::Interrupt::Shared::SourceApi&	irqSource
					) noexcept;
		/** */
		bool		allocate(Oscl::ExtAlloc::Api& allocator) noexcept;
		/** */
		void		enableDecoders() noexcept;

	public:	// Func::InfoApi
		/** */
		uint16_t	getDeviceID() const noexcept;
		/** */
		uint16_t	getVendorID() const noexcept;
		/** */
		uint8_t	getClassCode() const noexcept;
		/** */
		uint8_t	getSubClassCode() const noexcept;
		/** */
		uint8_t	getProgIF() const noexcept;
		/** */
		uint8_t	getRevisionID() const noexcept;
		/** */
		const Oscl::PciSrv::LocationApi& getLocation() const noexcept;
		/** */
		unsigned long	getBaseAddress(unsigned char bar) const noexcept;
		/** */
		Oscl::Interrupt::Shared::SourceApi&	getInterruptSource() noexcept;
		/** */
		unsigned long	getSpaceSize(unsigned char bar) const noexcept;
	};

}
}
}

#endif

