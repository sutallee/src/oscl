/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_pcisrv_locationh_
#define _oscl_pcisrv_locationh_
#include "oscl/oid/fixed.h"
#include "oscl/pcisrv/locapi.h"

/** */
namespace Oscl {
/** */
namespace PciSrv {
/** */
namespace Enum {

/** */
class Location : public Oscl::PciSrv::LocationApi {
	private:
		/** */
		enum {MaxChassisDepth=8};
		/** */
		enum {ObjectIDSize=MaxChassisDepth*2+1};
		/** */
		Oscl::ObjectID::Fixed<ObjectIDSize>		_path;
		/** */
		const unsigned char						_chassisID;

	public:
		/** */
		Location(	unsigned char					chassisID,
					const Oscl::ObjectID::RO::Api&	path
					) noexcept;

		/** */
		Location(const Location& other) noexcept;

		/** */
		virtual ~Location() {}

		/** Accessor to allow enumerator to append or otherwise
			modify the initial path. However, once the function
			has been made available to drivers (advertised) at
			the completion of enumeration, the path must not be
			modified.
		 */
		Oscl::ObjectID::RO::Api&		oid() noexcept;

	public:	// LocationApi
		/** The path OID is an absolute identifier that is a series of
			alternating chassis and slot identifiers. The series will
			be zero length if the location is specifying an embedded
			function on the secondary side of the host bridge.
			Otherwise, the series always begins with a slot identifier.
			The root chassis is always implied. Thus, an object ID
			that has an even length identifies an function within
			a device embedded within a chassis, and always ends with
			chassis identifier. An object ID that has an odd length
			identifies a function contained in a removable device that
			is located in a slot, and always ends with a slot identifier.
		 */
		const Oscl::ObjectID::RO::Api&	path() const noexcept;

		/** This operation returns the unique PCI chassis ID assigned to
			the leaf chassis in which the function is contained. This
			chassis identifier is NOT the same as the chassis identifiers
			contained in the OID returned by the path() operation.
		 */
		unsigned char				chasisID() const noexcept;

	};

}
}
}

#endif
