/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "server.h"
#include "oscl/driver/pci/function.h"
#include "oscl/driver/pci/stdfunc.h"
#include "function.h"

using namespace Oscl::PciSrv::Enum;

Server::Server(	Oscl::Pci::Config::Api&				pciApi,
				unsigned long						startOfPciAddressSpace,
				unsigned long						sizeOfPciAddressSpace,
				Oscl::Mt::Itc::PostMsgApi&			dynSrvPostApi,
				Oscl::Mt::Itc::PostMsgApi&			advPostApi,
				Oscl::Mt::Itc::Dyn::Adv::
				Core<Func::Server>::ReleaseApi&		advertiser,
				Oscl::Mt::Itc::Dyn::Adv::
				Creator::SyncApi<Func::Server>&		advSyncApi,
				Oscl::Interrupt::Shared::SourceApi&	irqSource,
				unsigned							bridgeOidType,
				unsigned							slotOidType,
				unsigned							functionOidType,
				unsigned							bridgeID
				) noexcept:
		_pciAllocator(startOfPciAddressSpace,sizeOfPciAddressSpace),
		_pciApi(pciApi),
		_dynSrvPostApi(dynSrvPostApi),
		_advPostApi(advPostApi),
		_advertiser(advertiser),
		_advSyncApi(advSyncApi),
		_irqSource(irqSource),
		_bridgeOidType(bridgeOidType),
		_slotOidType(slotOidType),
		_functionOidType(functionOidType),
		_bridgeID(bridgeID)
		{
	}

void	Server::initialize() noexcept{
	enumerate();
	}

void	Server::mboxSignaled() noexcept{
	}

void	Server::enumerate() noexcept{
	using namespace Oscl::Pci::Config;
	for(unsigned bus=0;bus<MaxBusesPerSystem;++bus){
		enumerateBus(bus);
		}
	}

void	Server::enumerateBus(unsigned char bus) noexcept{
	using namespace Oscl::Pci::Config;
	for(unsigned device=0;device<MaxDevicesPerBus;++device){
		enumerateDevice(bus,device);
		}
	}

void	Server::enumerateDevice(unsigned char bus,unsigned char device) noexcept{
	using namespace Oscl::Pci::Config;
	for(unsigned function=0;function<MaxFunctionsPerDevice;++function){
		enumerateFunction(bus,device,function);
		}
	}

void	Server::enumerateFunction(	unsigned char	bus,
									unsigned char	device,
									unsigned char	function
									) noexcept{
	Pci::Config::Function::Generic	func(_pciApi,bus,device,function);
	if(!func.update()) return;
	switch(func.headerType()){
		case Pci::Config::Function::Standard:
			enumerateStandard(bus,device,function);
			break;
		case Pci::Config::Function::PciToPciBridge:
			enumeratePciToPciBridge();
			break;
		case Pci::Config::Function::PciToCardBusBridge:
			enumeratePciToCardBusBridge();
			break;
		}
	}

void	Server::enumerateStandard(	unsigned char	bus,
									unsigned char	device,
									unsigned char	function
									) noexcept{
	Oscl::PciSrv::Enum::Function*	func;
	Oscl::ObjectID::Fixed<32>		oid;	// FIXME:
	oid	+= _bridgeOidType;
	oid	+= _bridgeID;
	oid	+= _slotOidType;
	oid	+= device;
	oid	+= _functionOidType;
	oid	+= function;
	func	= new Oscl::PciSrv::Enum::Function(	_pciApi,
												bus,
												device,
												function,
												0,	// FIXME:Chasis ID
												oid,
												_dynSrvPostApi,
												_advPostApi,
												_advertiser,
												_irqSource
												);
	if(!func) return;
	if(func->allocate(_pciAllocator)){
		// FIXME: In a dynamic (CompactPCI) environment,
		// I would need to remember the function for when
		// the bus is enumerated and it has been removed.
		// I would then need to delete the function.
		// Also, I should be offering an interface to allow
		// applications to force hardware off-line, in which
		// case I would need to keep a reference to the
		// function to invoke the deactivate() operation
		// of its creator API.
		func->enableDecoders();
		func->syncOpen();
		_advSyncApi.add(*func);
		}
	else{
		delete func;
		}
	}

void	Server::enumeratePciToPciBridge() noexcept{
	while(true);
	}

void	Server::enumeratePciToCardBusBridge() noexcept{
	while(true);
	}

