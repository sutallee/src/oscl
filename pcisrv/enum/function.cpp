/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "function.h"
using namespace Oscl::PciSrv::Enum;
using namespace Oscl;

Function::Function(	Oscl::Pci::Config::Api&				pciConfigApi,
					unsigned char						bus,
					unsigned char						device,
					unsigned char						function,
					unsigned char						chassisID,
					const Oscl::ObjectID::RO::Api&		path,
					Oscl::Mt::Itc::PostMsgApi&			dynSrvPostApi,
					Oscl::Mt::Itc::PostMsgApi&			advPostApi,
					Oscl::Mt::Itc::Dyn::Adv::
					Core<Server>::ReleaseApi&			advertiser,
					Oscl::Interrupt::Shared::SourceApi&	irqSource
					) noexcept:
		Oscl::PciSrv::Func::Server(	*this,
									dynSrvPostApi,
									advPostApi,
									advertiser
									),
		_func(pciConfigApi,bus,device,function),
		_irqSource(irqSource),
		_location(chassisID,path),
		_allocated(false),
		_pciConfigApi(pciConfigApi),
		_bus(bus),
		_device(device),
		_function(function)
		{
	}

bool		Function::allocate(Oscl::ExtAlloc::Api& allocator) noexcept{
	using namespace Oscl::Pci::Config::HeaderTypeZero;
	if(!_func.update()) return false;
	for(unsigned bar=0;bar<MaxBaseAddressRegisters;++bar){
		if(_func.isImplemented(bar)){
			if(_func.isIoAddress(bar)){
				// I/O space not supported
				}
			else {
				// Memory space
				if(_func.prefetchable(bar)){
					// Prefetchable memory not supported
					}
				else {
					switch(_func.barConstraint(bar)){
						case Oscl::Pci::Config::Function::Lower4GB:
							break;
						case Oscl::Pci::Config::Function::Lower1MB:
							while(true);
							break;
						case Oscl::Pci::Config::Function::Anywhere:
							break;
						}
					if(allocator.alloc(	_bar[bar],
										((~_func.blockSizeMask(bar))+1),
										_func.blockSizeMask(bar)
										)
							){
						_func.setAddress(bar,_bar[bar].getFirstUnit());
						}
					else{
						return false;	// Cannot allocate all resources
						}
					}
				}
			}
		}
	_allocated=true;
	return _allocated;
	}

void		Function::enableDecoders() noexcept{
	_func.clearMasterDataParityError();
	_func.clearSignaledTargetAbort();
	_func.clearReceivedMasterAbort();
	_func.clearSignaledSystemError();
	_func.clearDetectedParityError();
	_func.enableMemoryDecoders();
	_func.enableBusMastering();
	_func.clearMasterDataParityError();
	_func.clearSignaledTargetAbort();
	_func.clearReceivedMasterAbort();
	_func.clearSignaledSystemError();
	_func.clearDetectedParityError();
	}

uint16_t	Function::getDeviceID() const noexcept{
	return _func.deviceID();
	}

uint16_t	Function::getVendorID() const noexcept{
	return _func.vendorID();
	}

uint8_t	Function::getClassCode() const noexcept{
	return _func.baseClass();
	}

uint8_t	Function::getSubClassCode() const noexcept{
	return _func.subClass();
	}

uint8_t	Function::getProgIF() const noexcept{
	return _func.programmingInterface();
	}

uint8_t	Function::getRevisionID() const noexcept{
	return _func.revisionID();
	}

const Oscl::PciSrv::LocationApi& Function::getLocation() const noexcept{
	return _location;
	}

unsigned long	Function::getBaseAddress(unsigned char bar) const noexcept{
	if(!_allocated) return ~0;
	if(bar >= MaxBaseAddresses) return ~0;	// ErrorFatal!
	if(_bar[bar].getNumberContiguousUnits() == 0) return ~0; // not implemented
	return _bar[bar].getFirstUnit();
	}

Oscl::Interrupt::Shared::SourceApi&	Function::getInterruptSource() noexcept{
	return _irqSource;
	}

unsigned long	Function::getSpaceSize(unsigned char bar) const noexcept{
	if(!_allocated) return 0;
	if(bar >= MaxBaseAddresses) return 0;	// ErrorFatal!
	return _bar[bar].getNumberContiguousUnits();
	}

