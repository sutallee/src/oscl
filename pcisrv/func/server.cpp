/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "server.h"

using namespace Oscl::PciSrv::Func;
using namespace Oscl;

Server::Server(	InfoApi&						funcInfo,
				Oscl::Mt::Itc::PostMsgApi&		dynSrvPostApi,
				Oscl::Mt::Itc::PostMsgApi&		advPostApi,
				Oscl::Mt::Itc::Dyn::Adv::
				Core<Server>::ReleaseApi&		advertiser
				) noexcept:
		Oscl::Mt::Itc::Dyn::Adv::
		Core<Server>(dynSrvPostApi,advertiser,advPostApi),
		_funcInfo(funcInfo),
		_claimed(false)
		{
	}

bool	Server::isClaimed() const noexcept{
	return _claimed;
	}

uint16_t	Server::getDeviceID() const noexcept{
	return _funcInfo.getDeviceID();
	}

uint16_t	Server::getVendorID() const noexcept{
	return _funcInfo.getVendorID();
	}

uint8_t	Server::getClassCode() const noexcept{
	return _funcInfo.getClassCode();
	}

uint8_t	Server::getSubClassCode() const noexcept{
	return _funcInfo.getSubClassCode();
	}

uint8_t	Server::getProgIF() const noexcept{
	return _funcInfo.getProgIF();
	}

uint8_t	Server::getRevisionID() const noexcept{
	return _funcInfo.getRevisionID();
	}

const Oscl::PciSrv::LocationApi&	Server::getLocation() const noexcept{
	return _funcInfo.getLocation();
	}

unsigned long	Server::getBaseAddress(unsigned char n) const noexcept{
	return _funcInfo.getBaseAddress(n);
	}

Oscl::Interrupt::Shared::SourceApi&	Server::getInterruptSource() noexcept{
	return _funcInfo.getInterruptSource();
	}

void	Server::openDynamicService() noexcept{
	// Nothing to attach yet
	}

void	Server::closeDynamicService() noexcept{
	// Nothing to detach yet
	}

Server&	Server::getDynSrv() noexcept{
	return *this;
	}

