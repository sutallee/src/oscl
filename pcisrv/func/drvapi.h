/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_pcisrv_func_driverapih_
#define _oscl_pcisrv_func_driverapih_
#include "oscl/pcisrv/locapi.h"
#include <stdint.h>
#include "oscl/interrupt/shared/api.h"

/** */
namespace Oscl {
/** */
namespace PciSrv {
/** */
namespace Func {

/** */
class DriverApi {
	public:
		/** Shut-up GCC. */
		virtual ~DriverApi() {}
	public:
		/** */
		virtual uint16_t	getDeviceID() const noexcept=0;
		/** */
		virtual uint16_t	getVendorID() const noexcept=0;
		/** */
		virtual uint8_t	getClassCode() const noexcept=0;
		/** */
		virtual uint8_t	getSubClassCode() const noexcept=0;
		/** */
		virtual uint8_t	getProgIF() const noexcept=0;
		/** */
		virtual uint8_t	getRevisionID() const noexcept=0;
	public:
		/** */
		virtual const Oscl::PciSrv::LocationApi& getLocation() const noexcept=0;
	public:
		/** */
		virtual unsigned long	getBaseAddress(unsigned char n) const noexcept=0;
		/** */
		virtual Oscl::Interrupt::Shared::
		SourceApi&	getInterruptSource() noexcept=0;
	};

}
}
}

#endif
