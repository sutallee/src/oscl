/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_pcisrv_func_serverh_
#define _oscl_pcisrv_func_serverh_
#include "drvapi.h"
#include "oscl/pcisrv/enum/location.h"
#include "oscl/pcisrv/enum/funcapi.h"
#include "oscl/mt/itc/dyn/adv/core.h"

/** */
namespace Oscl {
/** */
namespace PciSrv {
/** */
namespace Func {

class InfoApi {
	public:
		/** Shut-up GCC. */
		virtual ~InfoApi() {}
	public:
		/** */
		virtual uint16_t	getDeviceID() const noexcept=0;
		/** */
		virtual uint16_t	getVendorID() const noexcept=0;
		/** */
		virtual uint8_t	getClassCode() const noexcept=0;
		/** */
		virtual uint8_t	getSubClassCode() const noexcept=0;
		/** */
		virtual uint8_t	getProgIF() const noexcept=0;
		/** */
		virtual uint8_t	getRevisionID() const noexcept=0;
	public:
		/** */
		virtual const Oscl::PciSrv::LocationApi& getLocation() const noexcept=0;
	public:
		/** */
		virtual unsigned long	getBaseAddress(unsigned char n) const noexcept=0;
		/** */
		virtual Oscl::Interrupt::Shared::
		SourceApi&	getInterruptSource() noexcept=0;
	};

/** */
class Server :	public DriverApi,
				public Oscl::PciSrv::Enum::FuncApi,
				public Oscl::Mt::Itc::Dyn::Adv::Core<Server>
				{
	private:
		/** */
		InfoApi&						_funcInfo;
		/** */
		bool							_claimed;

	public:
		/** */
		Server(	InfoApi&									funcInfo,
				Oscl::Mt::Itc::PostMsgApi&					dynSrvPostApi,
				Oscl::Mt::Itc::PostMsgApi&					advPostApi,
				Oscl::Mt::Itc::Dyn::Adv::Core<Server>::
					ReleaseApi&								advertiser
				) noexcept;

	public: // FuncApi
		/** */
		bool	isClaimed() const noexcept;
	public:	// DriverApi
		/** */
		uint16_t	getDeviceID() const noexcept;
		/** */
		uint16_t	getVendorID() const noexcept;
		/** */
		uint8_t	getClassCode() const noexcept;
		/** */
		uint8_t	getSubClassCode() const noexcept;
		/** */
		uint8_t	getProgIF() const noexcept;
		/** */
		uint8_t	getRevisionID() const noexcept;
		/** */
		const Oscl::PciSrv::LocationApi&	getLocation() const noexcept;
		/** */
		unsigned long	getBaseAddress(unsigned char n) const noexcept;
		/** */
		Oscl::Interrupt::Shared::SourceApi&	getInterruptSource() noexcept;

	public: // Oscl::Mt::Itc::Dyn::Adv::Core
		/** Executes within the dynamic service thread. Connect
			to any lower layer services synchronously.
		 */
		void	openDynamicService() noexcept;
		/** Executes within the dynamic service thread. Disconnect
			from any lower layer services synchronously.
		 */
		void	closeDynamicService() noexcept;
		/** */
		Server&	getDynSrv() noexcept;
	};

}
}
}

#endif
