/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_pcisrv_class_sbc_usb_ohci_monitorh_
#define _oscl_pcisrv_class_sbc_usb_ohci_monitorh_
#include "match.h"
#include "oscl/pcisrv/func/server.h"
#include "oscl/mt/itc/dyn/adv/iterator.h"
#include "oscl/mt/itc/srv/close.h"
#include "oscl/mt/itc/dyn/cli/respapi.h"
#include "oscl/mt/itc/dyn/monitor/unit/monitor/monitor.h"
#include "oscl/driver/usb/ohci/hcd/server/driver.h"
#include "oscl/driver/usb/ohci/hub/root/driver.h"
#include "oscl/memory/block.h"
#include "oscl/mt/itc/mbox/srvapi.h"
#include "oscl/interrupt/shared/dynisrdsr.h"
#include "oscl/driver/usb/hub/port/driver/driver.h"
#include "oscl/oid/fixed.h"

/** */
namespace Oscl {
/** */
namespace PciSrv {
/** */
namespace SerialBusController {
/** */
namespace USB {
/** */
namespace OHCI {

/** */
class Monitor : public	Oscl::Mt::Itc::
						Dyn::Unit::Monitor<Oscl::PciSrv::Func::Server>
				{
	public:
		typedef Oscl::Memory::AlignedBlock
			<sizeof(Oscl::Usb::Hub::Port::Driver)>		HubPortMem;
	private:
		/** */
		Match											_match;

		/** */
		Oscl::Mt::Itc::Dyn::Adv::
		Creator::SyncApi<Oscl::Usb::DynDevice>&			_advCapi;
		/** */
		Oscl::Mt::Itc::Dyn::Adv::
		Core<Oscl::Usb::DynDevice>::ReleaseApi&			_advRapi;
		/** */
		Oscl::Mt::Itc::Delay::Req::Api::SAP&			_delayServiceSAP;
		/** */
		Oscl::Mt::Itc::PostMsgApi&						_advPapi;
		/** */
		Oscl::Mt::Itc::PostMsgApi&						_dynSrvPapi;
		/** */
		Oscl::Bus::Api&									_dmaBus;
		/** */
		Oscl::Mt::Itc::ServerApi&						_hcdServer;
		/** */
		Oscl::Mt::Itc::PostMsgApi&						_hubServer;
		/** */
		Oscl::Interrupt::Shared::DynIsrDsrApi&			_dynIsrDsr;
		/** */
		Oscl::Memory::AlignedBlock
			<	sizeof(	Oscl::Usb::Ohci::
						Hub::Root::Driver
						)
				>										_rootHubMem;
		/** */
		Oscl::Memory::AlignedBlock
			<sizeof(Oscl::Usb::Ohci::HCD::Driver)>		_hcdMem;
		/** _pipeMem[_maxPipeMem] */
		Oscl::Usb::Ohci::HCD::Driver::PipeMem*			_pipeMem;
		/** */
		const unsigned									_maxPipeMem;
		/** */
		Oscl::Usb::Ohci::
		Hub::Root::Driver::PortMem*						_rootHubPortMem;
		/** */
		HubPortMem*										_hubPortMem;
		/** */
		Oscl::Usb::Ohci::Hcca&							_hcca;
		/** */
		Oscl::Usb::Ohci::HCD::EndpointDescMem*			_edArray;
		/** */
		const unsigned									_edArraySize;
		/** */
		Oscl::Usb::Ohci::HCD::TransactionDescMem*		_tdArray;
		/** */
		const unsigned									_tdArraySize;
		/** */
		Oscl::Usb::Hub::Port::Driver::SetupPktMem*		_hubPortSetupPktMem;
		/** */
		Oscl::Usb::Hub::Port::Driver::PacketMem*		_hubPortPacketMem;
		/** */
		Oscl::ObjectID::Fixed<32>						_location;
		/** */
		const unsigned									_maxHubPorts;
		/** */
		const unsigned									_oidType;
		/** */
		Oscl::Usb::Ohci::Hub::Root::Driver*				_rootHub;
		/** */
		Oscl::Usb::Ohci::HCD::Driver*					_hcd;
		/** */
		unsigned										_nPorts;
		/** */
		Oscl::Queue<Oscl::Usb::Hub::Port::Driver>		_ports;

	public:
		/** */
		Monitor(	Oscl::Mt::Itc::Dyn::Adv::Cli::
					SyncApi<Oscl::PciSrv::Func::Server>&	advFind,
					Oscl::Mt::Itc::Dyn::Adv::Cli::
					Req::Api<	Oscl::PciSrv::
								Func::Server
								>::SAP&						advSAP,
					Oscl::Mt::Itc::PostMsgApi&				myPapi,
					Oscl::Mt::Itc::Dyn::Adv::
					Creator::SyncApi<Oscl::Usb::DynDevice>&	advCapi,
					Oscl::Mt::Itc::Dyn::Adv::
					Core<Oscl::Usb::DynDevice>::ReleaseApi&	advRapi,
					Oscl::Mt::Itc::Delay::Req::Api::SAP&	delayServiceSAP,
					Oscl::Mt::Itc::PostMsgApi&				advPapi,
					Oscl::Mt::Itc::PostMsgApi&				dynSrvPapi,
					Oscl::Bus::Api&							dmaBus,
					Oscl::Mt::Itc::ServerApi&				hcdServer,
					Oscl::Mt::Itc::PostMsgApi&				hubServer,
					Oscl::Interrupt::Shared::DynIsrDsrApi&	dynIsrDsr,
					Oscl::Usb::Ohci::HCD::Driver::
					PipeMem*								pipeMem,
					unsigned								maxPipeMem,
					Oscl::Usb::Ohci::
					Hub::Root::Driver::PortMem*				rootHubPortMem,
					HubPortMem*								hubPortMem,
					Oscl::Usb::Ohci::Hcca&					hcca,
					Oscl::Usb::Ohci::HCD::
					EndpointDescMem*						edArray,
					unsigned								edArraySize,
					Oscl::Usb::Ohci::HCD::
					TransactionDescMem*						tdArray,
					unsigned								tdArraySize,
					Oscl::Usb::Hub::Port::Driver::
					SetupPktMem*							hubPortSetupPktMem,
					Oscl::Usb::Hub::Port::Driver::
					PacketMem*								hubPortPacketMem,
					unsigned								maxHubPorts,
					unsigned								oidType,
					const Oscl::ObjectID::RO::Api*			location=0
					) noexcept;
		/** */
		virtual ~Monitor();

	private: // Oscl::Mt::Itc::Dyn::Unit::Monitor
		/** */
		void	activate() noexcept;
		/** */
		void	deactivate() noexcept;
	};

}
}
}
}
}

#endif
