/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "match.h"
#include "oscl/hw/pci/config.h"

using namespace Oscl::PciSrv::SerialBusController::USB::OHCI;

Match::Match(const Oscl::ObjectID::RO::Api* specificLocation) noexcept:
		_specificLocation(specificLocation)
		{
	}

Match::Match(const Oscl::ObjectID::RO::Api& specificLocation) noexcept:
		_specificLocation(&specificLocation)
		{
	}

bool	Match::next(Oscl::PciSrv::Func::Server& func) noexcept{
	using namespace Oscl::Pci::Config::ClassRev;

	if(func.getClassCode() != ClassCode::Value_SerialBusController){
		return false;
		}
	if(func.getSubClassCode() != SubClassCode::SerialBusController::Value_USB){
		return false;
		}
	if(func.getProgIF() != ProgIf::SerialBusController::USB::Value_Ohci){
		return false;
		}
	if(!_specificLocation) return true;
	if(*_specificLocation != func.getLocation().path()) return false;
	return true;
	}
