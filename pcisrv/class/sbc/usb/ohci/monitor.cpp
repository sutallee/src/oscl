/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <string.h>
#include "monitor.h"
#include "oscl/krux/platform/ppc/mmu.h"
#include "oscl/hw/usb/ohci/reg.h"
#include "oscl/error/fatal.h"

using namespace Oscl::PciSrv::SerialBusController::USB::OHCI;

Monitor::Monitor(	Oscl::Mt::Itc::Dyn::Adv::Cli::
					SyncApi<Oscl::PciSrv::Func::Server>&	advFind,
					Oscl::Mt::Itc::Dyn::Adv::Cli::
					Req::Api<	Oscl::PciSrv::
								Func::Server
								>::SAP&						advSAP,
					Oscl::Mt::Itc::PostMsgApi&				myPapi,
					Oscl::Mt::Itc::Dyn::Adv::
					Creator::SyncApi<Oscl::Usb::DynDevice>&	advCapi,
					Oscl::Mt::Itc::Dyn::Adv::
					Core<Oscl::Usb::DynDevice>::ReleaseApi&	advRapi,
					Oscl::Mt::Itc::Delay::Req::Api::SAP&	delayServiceSAP,
					Oscl::Mt::Itc::PostMsgApi&				advPapi,
					Oscl::Mt::Itc::PostMsgApi&				dynSrvPapi,
					Oscl::Bus::Api&							dmaBus,
					Oscl::Mt::Itc::ServerApi&				hcdServer,
					Oscl::Mt::Itc::PostMsgApi&				hubServer,
					Oscl::Interrupt::Shared::DynIsrDsrApi&	dynIsrDsr,
					Oscl::Usb::Ohci::HCD::Driver::
					PipeMem*								pipeMem,
					unsigned								maxPipeMem,
					Oscl::Usb::Ohci::
					Hub::Root::Driver::PortMem*				rootHubPortMem,
					HubPortMem*								hubPortMem,
					Oscl::Usb::Ohci::Hcca&					hcca,
					Oscl::Usb::Ohci::HCD::
					EndpointDescMem*						edArray,
					unsigned								edArraySize,
					Oscl::Usb::Ohci::HCD::
					TransactionDescMem*						tdArray,
					unsigned								tdArraySize,
					Oscl::Usb::Hub::Port::Driver::
					SetupPktMem*							hubPortSetupPktMem,
					Oscl::Usb::Hub::Port::Driver::
					PacketMem*								hubPortPacketMem,
					unsigned								maxHubPorts,
					unsigned								oidType,
					const Oscl::ObjectID::RO::Api*			location
					) noexcept:
		Oscl::Mt::Itc::Dyn::Unit::
		Monitor<Oscl::PciSrv::Func::Server>(	advSAP,
												advFind,
												myPapi,
												_match
												),
		_match(location),
		_advCapi(advCapi),
		_advRapi(advRapi),
		_delayServiceSAP(delayServiceSAP),
		_advPapi(advPapi),
		_dynSrvPapi(dynSrvPapi),
		_dmaBus(dmaBus),
		_hcdServer(hcdServer),
		_hubServer(hubServer),
		_dynIsrDsr(dynIsrDsr),
		_pipeMem(pipeMem),
		_maxPipeMem(maxPipeMem),
		_rootHubPortMem(rootHubPortMem),
		_hubPortMem(hubPortMem),
		_hcca(hcca),
		_edArray(edArray),
		_edArraySize(edArraySize),
		_tdArray(tdArray),
		_tdArraySize(tdArraySize),
		_hubPortSetupPktMem(hubPortSetupPktMem),
		_hubPortPacketMem(hubPortPacketMem),
		_maxHubPorts(maxHubPorts),
		_oidType(oidType),
		_rootHub(0),
		_hcd(0)
		{
	}

/** */
Monitor::~Monitor(){
	}

void	Monitor::activate() noexcept{
	Oscl::PciSrv::Func::Server&	server	= _handle->getDynSrv();
	Oscl::Usb::Ohci::Map*	ohciRegisters;
	ohciRegisters	= (Oscl::Usb::Ohci::Map*)server.getBaseAddress(0);
	MapIoRange(ohciRegisters,sizeof(Oscl::Usb::Ohci::Map));
	memset(&_hcca,0,sizeof(_hcca));
	_hcd	= new(&_hcdMem)
					Oscl::Usb::Ohci::HCD::Driver(	*ohciRegisters,
													_hcca,
													_tdArray,
													_tdArraySize,
													_edArray,
													_edArraySize,
													_dmaBus,
													_hcdServer,
													_pipeMem,
													_maxPipeMem
													);
	_rootHub	= new (&_rootHubMem)
					Oscl::Usb::Ohci::Hub::Root::Driver(	ohciRegisters->rootHub,
														_hcd->getRootHubSAP(),
														_hubServer,
														_rootHubPortMem,
														_maxHubPorts
														);
	_dynIsrDsr.attach(*_hcd,server.getInterruptSource());
	_hcd->syncOpen();
	_rootHub->syncOpen();

	for(_nPorts=0;_nPorts<_maxHubPorts;++_nPorts){
		Oscl::ObjectID::Fixed<32>	location;
		Oscl::ObjectID::Api&		l	= location;
		l	= server.getLocation().path();
		l	+= _oidType;
		l	+= _nPorts;
		Oscl::Usb::Hub::Port::Req::Api::SAP*	sap;
		sap	= _rootHub->getPortSAP(_nPorts);
		if(!sap) break;
		Oscl::Usb::Hub::Port::Driver*	driver;
		driver	= new(&_hubPortMem[_nPorts])
					Oscl::Usb::Hub::
					Port::Driver(	_hcd->getHubPortSAP(),
									*sap,
									_hcd->getMsgPipeAllocSAP(),
									_advCapi,
									_advRapi,
									_advPapi,
									_rootHub->getPowerSAP(),
									_rootHub->getPowerSyncApi(),
									_hcd->getEnumMonitorApi(),
									_hcd->getEnumDefCntrlPipeApi(),
									_dynSrvPapi,
									_hcd->getHsEpSAP(),
									_hcd->getLsEpSAP(),
									_delayServiceSAP,
									_hubServer,
									_hubPortPacketMem[_nPorts],
									_hubPortSetupPktMem[_nPorts],
									&location
									);
		driver->syncOpen();
		_ports.put(driver);
		}
	}

void	Monitor::deactivate() noexcept{
	_rootHub->syncClose();
	_hcd->syncClose();
	_dynIsrDsr.detach();
	_rootHub->~Driver();
	_hcd->~Driver();
	deactivateDone();
	// FIXME: need to unmap I/O registers
	}

