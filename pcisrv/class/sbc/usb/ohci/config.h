/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_pcisrv_class_sbc_usb_ohci_configh_
#define _oscl_pcisrv_class_sbc_usb_ohci_configh_
#include <new>
#include "monitor.h"
#include "oscl/error/fatal.h"
#include "oscl/kernel/mmu.h"
#include "oscl/cache/inhibit.h"
#include "oscl/extalloc/api.h"

/** */
namespace Oscl {
/** */
namespace PciSrv {
/** */
namespace SerialBusController {
/** */
namespace USB {
/** */
namespace OHCI {

enum{nAddrZeroEndpoints			= 2};
enum{nStaticBulkEndpoints		= 1};
enum{nStaticPeriodicEndpoints	= 63};

/**	The purpose of this template class is to provide a configurable
	means of static allocation for PCI OCHI USB Host Controller Driver.

	Each of the template parameters describes a variable that affects
	the quantity of memory reserved for the OHCI HCD. Each is a tradeoff
	between performance and memory utilization. In well bounded embedded
	applications, memory utilization can be minimized by carefully
	specifying these parameters.
	nEndpointDescriptors:
		the maximum number of endpoints supported on this bus that
		is above and beyond the static requirements of the HCD
		as listed:
			* two endpoints are always allocated for the default
			"address zero" low and high speed control pipes.
			* 63 endpoints may be allocated for the static
			periodic list structure (interrupt and isochronous)
			depending upon the implementation.
			* one static bulk list endpoint may always allocated
			for the static list maintenance.
	nTransferDescriptors:
		typical/example considerations:
			one "dummy" for each expected endpoint (127 max).
			two for each expected interrupt endpoint (double buffered)
			two for each expected isochronous endpoint (double buffered)
			a pool shared by all control and bulk endpoints. 
				minimum is a function of maximum buffer/transfer length divided
				by a minimum maximum packet size (8), plus one control setup
				packet and one control status packet.
					(65536)/8 + 2 = 8192+2 = 8194
	maxHubPorts:
		the maximum number ports supported by this OHCI HCD. The OHCI
		specification allows for a maximum of 15 downstream root hub
		ports (Oscl::Usb::Ohci::RhPortStatus::MaxDownstreamPorts). However,
		implementations typically only have two. Thus, if the system
		uses a constrained set of OHCI HC implementations, this parameter
		can be used to reduce memory requirements for such implementations.
		However, in an unconstrained system it should be set to 15.
 */
template<
			unsigned nEndpointDescriptors,
			unsigned nTransferDescriptors,
			unsigned maxHubPorts
			>
class Config {
	private:
		/** Memory for the actual monitor. The monitor is
			initialized (a la "new") in the body of the
			constructor such that "complex" memory calculations
			can be performed and dynamic memory allocated.
		 */
		Oscl::Memory::
		AlignedBlock<sizeof(Monitor)>			_monMem;
		/** */
		enum{maxPipes = nAddrZeroEndpoints+nEndpointDescriptors};
		/** */
		Oscl::Usb::Ohci::HCD::Driver::
		PipeMem									_pipeMem[maxPipes];
		/** */
		Oscl::Usb::Ohci::
		Hub::Root::Driver::PortMem				_rootHubPortMem[maxHubPorts];
		/** */
		Monitor::HubPortMem						_hubPortMem[maxHubPorts];
		/** */
		Oscl::ExtAlloc::Record					_hcdDmaMemRec;
		/** This pointer is initialized during construction,
			and points to _monMem after the "new" operation
			is complete.
		 */
		Monitor*								_theMonitor;
		
	public:
		/** */
		Config(		Oscl::Mt::Itc::Dyn::Adv::Cli::
					SyncApi<Oscl::PciSrv::Func::Server>&		advFind,
					Oscl::Mt::Itc::Dyn::Adv::Cli::
					Req::Api<Oscl::PciSrv::Func::Server>::SAP&	advSAP,
					Oscl::Mt::Itc::PostMsgApi&					myPapi,
					Oscl::Mt::Itc::Dyn::Adv::
					Creator::SyncApi<Oscl::Usb::DynDevice>&		advCapi,
					Oscl::Mt::Itc::Dyn::Adv::
					Core<Oscl::Usb::DynDevice>::ReleaseApi&		advRapi,
					Oscl::Mt::Itc::Delay::Req::Api::SAP&		delayServiceSAP,
					Oscl::Mt::Itc::PostMsgApi&					advPapi,
					Oscl::Mt::Itc::PostMsgApi&					dynSrvPapi,
					Oscl::Bus::Api&								dmaBus,
					Oscl::Mt::Itc::ServerApi&					hcdServer,
					Oscl::Mt::Itc::PostMsgApi&					rhServer,
					Oscl::Interrupt::Shared::DynIsrDsrApi&		dynIsrDsr,
					Oscl::ExtAlloc::Api&						usbMemAllocator,
					unsigned									oidType,
					const Oscl::ObjectID::RO::Api*				location=0
					) noexcept;
		/** */
		virtual ~Config(){}

		/** */
		Oscl::Mt::Itc::Srv::OpenCloseSyncApi&	getOpenCloseSyncApi() noexcept{
			return *_theMonitor;
			}
	};

/**
 */
template<	unsigned nEndpointDescriptors,
			unsigned nTransferDescriptors,
			unsigned maxHubPorts
			>
Config<	nEndpointDescriptors,
		nTransferDescriptors,
		maxHubPorts
		>::Config(	Oscl::Mt::Itc::Dyn::Adv::Cli::
					SyncApi<Oscl::PciSrv::Func::Server>&		advFind,
					Oscl::Mt::Itc::Dyn::Adv::Cli::
					Req::Api<Oscl::PciSrv::Func::Server>::SAP&	advSAP,
					Oscl::Mt::Itc::PostMsgApi&					myPapi,
					Oscl::Mt::Itc::Dyn::Adv::
					Creator::SyncApi<Oscl::Usb::DynDevice>&		advCapi,
					Oscl::Mt::Itc::Dyn::Adv::
					Core<Oscl::Usb::DynDevice>::ReleaseApi&		advRapi,
					Oscl::Mt::Itc::Delay::Req::Api::SAP&		delayServiceSAP,
					Oscl::Mt::Itc::PostMsgApi&					advPapi,
					Oscl::Mt::Itc::PostMsgApi&					dynSrvPapi,
					Oscl::Bus::Api&								dmaBus,
					Oscl::Mt::Itc::ServerApi&					hcdServer,
					Oscl::Mt::Itc::PostMsgApi&					rhServer,
					Oscl::Interrupt::Shared::DynIsrDsrApi&		dynIsrDsr,
					Oscl::ExtAlloc::Api&						usbMemAllocator,
					unsigned									oidType,
					const Oscl::ObjectID::RO::Api*				location
					) noexcept
		{
	// "Termination/dummy" TDs are not needed for static
	// EDs since they do not represent actual USB endpoints,
	// and thus are never used to transfer data.
	const unsigned long	minimumTransferDescriptors	=
			nAddrZeroEndpoints
		+	nEndpointDescriptors
		;
	const unsigned long edArraySize	=
			nAddrZeroEndpoints
		+	nStaticBulkEndpoints
		+	nStaticPeriodicEndpoints
		+	nEndpointDescriptors
		;
	const unsigned long tdArraySize	= 
			minimumTransferDescriptors
		+	nTransferDescriptors
		;
	const unsigned long	pageSize			= OsclKernelGetPageSize();
	const unsigned long	almostPageSize		= pageSize-1;

	const unsigned long	tdMemSize		=
		(tdArraySize*sizeof(Oscl::Usb::Ohci::HCD::TransactionDescMem));

	const unsigned long	edMemSize		=
		(edArraySize*sizeof(Oscl::Usb::Ohci::HCD::EndpointDescMem));

	const unsigned long	hubPortSetupPktMemSize		=
		(		maxHubPorts
			*	sizeof(Oscl::Usb::Hub::Port::Driver::SetupPktMem)
			)
			;

	const unsigned long	hubPortPacketMemSize		=
		(		maxHubPorts
			*	sizeof(Oscl::Usb::Hub::Port::Driver::PacketMem)
			)
			;


	const unsigned long	hccaMemSize		= 256;
	const unsigned long	hcdDmaMemSize		=
		(		hccaMemSize
			+	tdMemSize
			+	edMemSize
			+	hubPortSetupPktMemSize
			+	hubPortPacketMemSize
			);

	const unsigned long	nPages		=	(hcdDmaMemSize+almostPageSize)/pageSize;
	const unsigned long	hcdAllocSize	=	pageSize*nPages;

	if(!usbMemAllocator.alloc(_hcdDmaMemRec,hcdAllocSize,~almostPageSize)){
		Oscl::ErrorFatal::logAndExit(
			"Oscl::PciSrv::SerialBusController::USB::OHCI: cant allocate HCCA"
			);
		}

	Oscl::Usb::Ohci::Hcca*
	hcca	= (Oscl::Usb::Ohci::Hcca*)_hcdDmaMemRec.getFirstUnit();

// FIXME:
//  When I change oscl/driver/est/mdp8xx/krux/mmu/variation1/mmu.cpp
//	to use MapWriteableRamRange() instead of MapIoRange() for SRAM,
//	I will uncomment the next line.
//	OsclCacheInhibitRange(hcca,hcdAllocSize);

	Oscl::Usb::Ohci::HCD::TransactionDescMem*
	tdArray	= (Oscl::Usb::Ohci::HCD::TransactionDescMem*)
		(((unsigned long)hcca) + hccaMemSize);

	Oscl::Usb::Ohci::HCD::EndpointDescMem*
	edArray	= (Oscl::Usb::Ohci::HCD::EndpointDescMem*)
		(((unsigned long)tdArray) + tdMemSize);

	Oscl::Usb::Hub::Port::Driver::SetupPktMem*
	hubPortSetupPktMem	=
		(Oscl::Usb::Hub::Port::Driver::SetupPktMem*)
		(((unsigned long)edArray) + edMemSize);

	Oscl::Usb::Hub::Port::Driver::PacketMem*
	hubPortPacketMem	=
		(Oscl::Usb::Hub::Port::Driver::PacketMem*)
		(((unsigned long)hubPortSetupPktMem) + hubPortSetupPktMemSize);

	_theMonitor	= new(&_monMem)	Monitor(	advFind,
											advSAP,
											myPapi,
											advCapi,
											advRapi,
											delayServiceSAP,
											advPapi,
											dynSrvPapi,
											dmaBus,
											hcdServer,
											rhServer,
											dynIsrDsr,
											_pipeMem,
											maxPipes,
											_rootHubPortMem,
											_hubPortMem,
											*hcca,
											edArray,
											edArraySize,
											tdArray,
											tdArraySize,
											hubPortSetupPktMem,
											hubPortPacketMem,
											maxHubPorts,
											oidType,
											location
											);
	
	}

}
}
}
}
}

#endif
