/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_serial_itc_rx_driverh_
#define _oscl_serial_itc_rx_driverh_
#include "oscl/mt/itc/mbox/server.h"
#include "oscl/serial/driver/rx/api.h"
#include "oscl/stream/input/itc/reqapi.h"
#include "oscl/stream/input/itc/sync.h"
#include "oscl/memory/block.h"
#include "oscl/mt/itc/srv/closeapi.h"
#include "oscl/mt/itc/srv/close.h"

/** */
namespace Oscl {
/** */
namespace Serial {
/** */
namespace Itc {
/** */
namespace RX {

enum{bufferSize=256};

/** */
typedef Oscl::Memory::AlignedBlock< bufferSize >	BufferMem;

/** */
class Driver :
	public Oscl::Mt::Itc::Server,
	public Oscl::Mt::Itc::Srv::CloseSync,
	private Oscl::Mt::Itc::Srv::Close::Req::Api,
	public Oscl::Stream::Input::Req::Api
	{
	private:
		/** */
		Oscl::Serial::Driver::RX::Api&							_driver;
		/** */
		Oscl::Stream::Input::Sync								_sync;
		/** */
		BufferMem* const										_bufferMem;
		/** */
		const unsigned											_nBuffers;
		/** */
		Oscl::Queue<Oscl::Stream::Input::Req::Api::ReadReq>		_pending;
		/** */
		Oscl::Stream::Input::Req::Api::ReadReq*					_currentReq;
		/** */
		unsigned char*											_buffer;
		/** */
		unsigned												_offset;
		/** */
		unsigned												_length;

	public:
		/** */
		Driver(	Oscl::Serial::Driver::RX::Api&	driver,
				BufferMem						bufferMem[],
				unsigned						nBuffers
				) noexcept;

		/** */
		Oscl::Stream::Input::Api&	getSyncApi() noexcept;
		/** */
		Oscl::Stream::Input::Req::Api::SAP&	getSAP() noexcept;

	private:
		/** */
		void	mboxSignaled() noexcept;

	private:	// Oscl::Mt::Itc::Srv::Close::Req::Api
		/** */
		void    request(CloseReq& msg) noexcept;

	private:	// Oscl::Mt::Itc::Srv::Open::Req::Api
		/** */
		void    request(OpenReq& msg) noexcept;

	private: // Oscl::Stream::Input::Req::Api
		/** */
		void	request(Oscl::Stream::Input::Req::Api::ReadReq& msg) noexcept;
		/** */
		void	request(Oscl::Stream::Input::Req::Api::CancelReq& msg) noexcept;

	private:
		/** */
		void	getNextBuffer() noexcept;
		/** */
		bool	fillRequest() noexcept;
		/** */
		void	fillRequests() noexcept;
	};

}
}
}
}

#endif
