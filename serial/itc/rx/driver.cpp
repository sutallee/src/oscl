/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "driver.h"

using namespace Oscl::Serial::Itc::RX;

Driver::Driver(	Oscl::Serial::Driver::RX::Api&	driver,
				BufferMem						bufferMem[],
				unsigned						nBuffers
				) noexcept:
		CloseSync(*this,*this),
		_driver(driver),
		_sync(*this,*this),
		_bufferMem(bufferMem),
		_nBuffers(nBuffers),
		_buffer(0)
		{
	}

Oscl::Stream::Input::Api&	Driver::getSyncApi() noexcept{
	return _sync;
	}

Oscl::Stream::Input::Req::Api::SAP&	Driver::getSAP() noexcept{
	return _sync.getSAP();
	}

void	Driver::mboxSignaled() noexcept{
	fillRequests();
	}

void    Driver::request(CloseReq& msg) noexcept{
	msg.returnToSender();
	}

void    Driver::request(OpenReq& msg) noexcept{
	for(unsigned i=0;i<_nBuffers;++i){
		_driver.supplyBuffer(&_bufferMem[i],bufferSize);
		}
	msg.returnToSender();
	}

void	Driver::request(Oscl::Stream::Input::Req::Api::ReadReq& msg) noexcept{
	_pending.put(&msg);
	fillRequests();
	}

void	Driver::request(Oscl::Stream::Input::Req::Api::CancelReq& msg) noexcept{
	Oscl::Stream::Input::Req::Api::ReadReq*
	readToCancel	= _pending.remove(&msg._payload._readToCancel);
	if(readToCancel){
		readToCancel->returnToSender();
		}
	msg.returnToSender();
	}

void	Driver::getNextBuffer() noexcept{
	_length	= _driver.nextCompletedBuffer((void**)&_buffer);
	_offset	= 0;
	}

bool	Driver::fillRequest() noexcept{
	Oscl::Stream::Input::Req::Api::ReadReq*	req;
	if(!_buffer){
		getNextBuffer();
		}
	if(!_buffer) return false;

	req	= _pending.get();
	if(!req) return false;
	const unsigned	n	= _length-_offset;
	unsigned		len;
	len	= req->getPayload()._buffer.copyIn(&_buffer[_offset],n);
	_offset	+= len;
	if(_offset >= _length){
		_driver.reuseBuffer(_buffer);
		_buffer	= 0;
		}
	req->returnToSender();
	return true;
	}

void	Driver::fillRequests() noexcept{
	while(fillRequest());
	}
