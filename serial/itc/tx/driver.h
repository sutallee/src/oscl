/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_serial_itc_tx_driverh_
#define _oscl_serial_itc_tx_driverh_
#include "oscl/mt/itc/mbox/server.h"
#include "oscl/serial/driver/tx/api.h"
#include "oscl/stream/output/itc/reqapi.h"
#include "oscl/stream/output/itc/sync.h"
#include "oscl/memory/block.h"
#include "oscl/mt/itc/srv/closeapi.h"
#include "oscl/mt/itc/srv/close.h"

/** */
namespace Oscl {
/** */
namespace Serial {
/** */
namespace Itc {
/** */
namespace TX {

class Driver;

/** */
class Done : public Oscl::Done::Api {
	private:
		/** */
		Oscl::Stream::Output::Req::Api::WriteReq&	_req;
		/** */
		Driver&										_context;
	public:
		/** */
		Done(	Driver&										context,
				Oscl::Stream::Output::Req::Api::WriteReq&	req
				) noexcept;
	private:
		/** */
		void	done() noexcept;
	};

/** */
typedef Oscl::Memory::AlignedBlock< sizeof(Done) >	DoneMem;

/** */
class Driver :
	public Oscl::Mt::Itc::Server,
	public Oscl::Stream::Output::Req::Api,
	public Oscl::Mt::Itc::Srv::CloseSync,
	private Oscl::Mt::Itc::Srv::Close::Req::Api
	{
	private:
		/** */
		Oscl::Serial::Driver::TX::Api&							_driver;
		/** */
		Oscl::Stream::Output::Sync								_sync;
		/** */
		Oscl::Queue<Oscl::Stream::Output::Req::Api::WriteReq>	_pending;
		/** */
		Oscl::Queue<DoneMem>									_freeDoneList;

	public:
		/** */
		Driver(	Oscl::Serial::Driver::TX::Api&	driver,
				DoneMem							doneMem[],
				unsigned						nDoneMem
				) noexcept;

		/** */
		Oscl::Stream::Output::Api&				getSyncApi() noexcept;
		/** */
		Oscl::Stream::Output::Req::Api::SAP&	getSAP() noexcept;

	private:
		/** */
		void	mboxSignaled() noexcept;

	private:	// Oscl::Mt::Itc::Srv::Close::Req::Api
		/** */
		void    request(CloseReq& msg) noexcept;

	private:	// Oscl::Mt::Itc::Srv::Open::Req::Api
		/** */
		void    request(OpenReq& msg) noexcept;

	private: // Oscl::Stream::Output::Req::Api
		/** */
		void	request(Oscl::Stream::Output::Req::Api::WriteReq& msg) noexcept;
		/** */
		void	request(Oscl::Stream::Output::Req::Api::FlushReq& msg) noexcept;

	private:
		/** */
		friend class Done;
		/** */
		void	txPending() noexcept;
		/** */
		bool	tryTx(Oscl::Stream::Output::Req::Api::WriteReq& req) noexcept;
		/** */
		void	free(Done& done) noexcept;
	};

}
}
}
}

#endif
