/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <new>
#include "driver.h"

using namespace Oscl::Serial::Itc::TX;

Done::Done(	Driver&										context,
			Oscl::Stream::Output::Req::Api::WriteReq&	req) noexcept:
		_req(req),
		_context(context)
		{
	}

void	Done::done() noexcept{
	_req.getPayload()._nWritten	= _req.getPayload()._buffer.length();
	_req.returnToSender();
	_context.free(*this);
	}

Driver::Driver(	Oscl::Serial::Driver::TX::Api&	driver,
				DoneMem							doneMem[],
				unsigned						nDoneMem
				) noexcept:
		CloseSync(*this,*this),
		_driver(driver),
		_sync(*this,*this)
		{
	for(unsigned i=0;i<nDoneMem;++i){
		_freeDoneList.put(&doneMem[i]);
		}
	}

Oscl::Stream::Output::Api&	Driver::getSyncApi() noexcept{
	return _sync;
	}

Oscl::Stream::Output::Req::Api::SAP&	Driver::getSAP() noexcept{
	return _sync.getSAP();
	}

void	Driver::mboxSignaled() noexcept{
	_driver.processTransmitter();
	txPending();
	}

void    Driver::request(CloseReq& msg) noexcept{
	msg.returnToSender();
	}

void    Driver::request(OpenReq& msg) noexcept{
	msg.returnToSender();
	}

void	Driver::request(Oscl::Stream::Output::Req::Api::WriteReq& msg) noexcept{
	tryTx(msg);
	}

void	Driver::request(Oscl::Stream::Output::Req::Api::FlushReq& msg) noexcept{
	// FIXME: Is this right? Or should it be queued until pending write
	// requests are finished?
	msg.returnToSender();
	}

void	Driver::txPending() noexcept{
	Oscl::Queue<Oscl::Stream::Output::Req::Api::WriteReq>	pend;
	pend	= _pending;
	Oscl::Stream::Output::Req::Api::WriteReq*	req;
	while((req=pend.get())){
		if(!tryTx(*req)){
			break;
			}
		}
	while((req=pend.get())){
		_pending.put(req);
		}
	}

bool	Driver::tryTx(Oscl::Stream::Output::Req::Api::WriteReq& req) noexcept{
	DoneMem*	mem;
	mem	= _freeDoneList.get();
	if(!mem){
		_pending.put(&req);
		return false;
		}
	Done*	done	= new(mem) Done(*this,req);
	if(_driver.sendBuffer(	*done,
							req.getPayload()._buffer.getBuffer(),
							req.getPayload()._buffer.length()
							)
			){
		return true;
		}
	done->~Done();
	_freeDoneList.put(mem);
	_pending.put(&req);
	return false;
	}

void	Driver::free(Done& done) noexcept{
	done.~Done();
	_freeDoneList.put((DoneMem*)&done);
	txPending();
	}

