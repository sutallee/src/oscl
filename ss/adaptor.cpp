/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

//#define OSCL_SS_ADAPTOR_DEBUG

#ifdef OSCL_SS_ADAPTOR_DEBUG
#include <stdio.h>
#endif

#include <string.h>
#include "adaptor.h"
#include "oscl/endian/type.h"
#include "oscl/bignum/value.h"
#include "oscl/des/cbc.h"
#include "oscl/entropy/random.h"
#include "oscl/crypto/oakley.h"
#include "oscl/entropy/random128.h"
#include "oscl/buffer/fixed.h"

using namespace Oscl::SS;

enum{hugeSize=2*bigNumSize};
enum{smallSize=bigNumSize/2};

#ifdef OSCL_SS_ADAPTOR_DEBUG
static void print(const Oscl::BigNum::Value<bigNumSize>& v) {
	unsigned char	buffer[sizeofBigNumInOctets];
	unsigned len	= v.serialize(buffer,sizeof(buffer));
    for(unsigned i=0;i<len;++i){
        printf("%2.2X",(unsigned)buffer[i]);
        }
	printf("\n");
    }
#endif

static const Oscl::BigNum::Value<bigNumSize>
	diffieHellmanPrimeConst(	Oscl::Crypto::Oakley::GroupTwo::prime,
								sizeof(Oscl::Crypto::Oakley::GroupTwo::prime)
								);

static const Oscl::BigNum::Value<bigNumSize>
	diffieHellmanGConst(	Oscl::Crypto::Oakley::GroupTwo::generator,
							sizeof(Oscl::Crypto::Oakley::GroupTwo::generator)
							);

Adaptor::Adaptor(	Oscl::Stream::Input::Api&	streamIn,
					Oscl::Stream::Output::Api&	streamOut
					) noexcept:
		_streamIn(streamIn),
		_streamOut(streamOut),
		_open(false),
		_txLength(2),
		_rxLength(0),
		_rxOffset(0),
		_remainingInPacket(0)
		{
	}

void	Adaptor::open() noexcept{
	Oscl::Buffer::Fixed<sizeofBigNumInOctets>	buffer;
	const unsigned nIterations = sizeofBigNumInOctets/Oscl::Entropy::Random128::bufferSize;
	for(unsigned i=0;i<nIterations;++i){
		unsigned char	b[Oscl::Entropy::Random128::bufferSize];
		Oscl::Entropy::Random128::fetch(b);
		buffer.append(b,Oscl::Entropy::Random128::bufferSize);
		}

	Oscl::BigNum::Value<bigNumSize> privateKey(buffer.getBuffer(),buffer.bufferSize());
#ifdef OSCL_SS_ADAPTOR_DEBUG
	printf("private key.\n");
	print(privateKey);
#endif

	Oscl::BigNum::Value<bigNumSize> publicKey;
	publicKey	= diffieHellmanGConst;
	publicKey.powXmodY(privateKey,diffieHellmanPrimeConst);

#ifdef OSCL_SS_ADAPTOR_DEBUG
	printf("public key.\n");
	print(publicKey);
#endif
	unsigned	len;
	unsigned char	keyData[sizeofBigNumInOctets];
	unsigned n	= publicKey.serialize(keyData,sizeofBigNumInOctets);
	len	= _streamOut.write(keyData,n);
	if(len < sizeof(publicKey)){
		_open	= false;
		return;
		}

	_streamOut.flush();

	unsigned		remaining	= keySize;
	len	= 0;
	while(remaining){
		len=_streamIn.read(&_peerPublicKey[keySize-remaining],remaining);
		if(len == 0){
			_open	= false;
			return;
			}
		remaining	-= len;
		}

	Oscl::BigNum::Value<bigNumSize>	key(_peerPublicKey,keySize);

#ifdef OSCL_SS_ADAPTOR_DEBUG
	printf("peer public key.\n");
	print(key);
#endif

	key.powXmodY(privateKey,diffieHellmanPrimeConst);

#ifdef OSCL_SS_ADAPTOR_DEBUG
	printf("session key.\n");
	print(key);
#endif

	key.serialize(_sessionKey,sizeof(_sessionKey));

	_open	= true;
	}

void	Adaptor::close() noexcept{
	_open	= false;
	}

unsigned long	Adaptor::read(	void*			dest,
								unsigned long	maxSize
								) noexcept{
	while(true){
		if(!_open) return 0;
		unsigned	len	= copyOut(dest,maxSize);
		if(len){
			return len;
			}
		recv();
		}
	return 0;
	}

unsigned long	Adaptor::read(Buffer::Base& dest) noexcept{
	if(!_open) return 0;
	return 0;
	}

//
// Packet Format:
// * All packets must be an even multiple of 8 bytes including
//   the length field.
// * Packets must be at least 8 bytes long.
// * Any padding is always in the last bytes of the packet.
// * The length field is 32 bits Big Endian.
// * The length field specifies the length of the encrypted
//   data not including the length field or the padding.
// * Padding length is implicit since.
// * The maximum packet size is (2**16)-1 bytes.
// * The maximum payload size is thus 65,526 bytes.
// * The maximum value of the length fields is thus 65,526.
// * The minimum packet size is 8 bytes.
// * The minimum payload size is 1 bytes.
// * The minimum value of the length fields is 1.

unsigned long	Adaptor::write(	const void*			source,
								unsigned long		count
								) noexcept{
	if(!_open){
		return 0;
		}
	const unsigned char*	p	= (const unsigned char*)source;
	for(unsigned long i=0;count;){
		unsigned	len	= append(&p[i],count);
		if(len < count){
			send();
			if(!_open){
				return i;
				}
			}
		i		+= len;
		count	-= len;
		}
	return 0;
	}

unsigned long	Adaptor::write(const Buffer::Base& source) noexcept{
	return write(source.startOfData(),source.length());
	}

void	Adaptor::flush() noexcept{
	if(!_open) return;
	if(_txLength > 2){
		send();
		}
	_streamOut.flush();
	}

unsigned	Adaptor::append(	const void*	source,
								unsigned	length
								) noexcept{
	unsigned	remaining	= txBufferSize - _txLength;
	if(length > remaining){
		length	= remaining;
		}
	memcpy(&_txBuffer[_txLength],source,length);
	_txLength	+= length;
	return length;
	}

void	Adaptor::send() noexcept{
	Oscl::Endian::Big::U16*	length	= (Oscl::Endian::Big::U16*)_txBuffer;
	*length	= (_txLength - 2);
	unsigned	nBlocks	= ((_txLength)+7)/8;
	unsigned	size	= (nBlocks)*8;
	unsigned	len;
	len	= Oscl::DES::CBC::encrypt(	_sessionKey,
									_txBuffer,
									size,
									_txBuffer,
									size
									);
	if(len < size){
		// This should NEVER happen
		for(;;);
		}

	len	= _streamOut.write(_txBuffer,size);
	if(len < size){
		_open	= false;
		}
	_txLength		= sizeof(Oscl::Endian::Big::U16);
	}

void	Adaptor::recv() noexcept{
	if(_rxLength) return;

	unsigned long	offset	= 0;
	if(!_remainingInPacket){
		while(!_remainingInPacket){
			unsigned	len;
			// Read length field (minimum packet)
			for(unsigned i= 0;i<8;){
				len	= _streamIn.read(&_rxBuffer[i],(8-i));
				if(!len){
					_rxLength	= 0;
					_open		= false;
					return;
					}
				i	+= len;
				}
			len	= Oscl::DES::CBC::decrypt(	_sessionKey,
											_rxBuffer,
											8,
											_rxBuffer,
											8
											);
			_rxOffset	= 2;
			_rxLength	= 6;
			Oscl::Endian::Big::U16*	length	= (Oscl::Endian::Big::U16*)_rxBuffer;
			unsigned	pLength	= *length;
			unsigned	nBlocks	= ((pLength+2)+7)/8;
			_remainingInPacket	= nBlocks*8;
			_rxPadding			= _remainingInPacket-(pLength+2);
			_remainingInPacket	-= 8;
			}
		offset	= 8;
		}
	else {
		_rxLength	= 0;
		_rxOffset	= 0;
		}

	unsigned	nToRead		= _remainingInPacket;
	unsigned	remaining	= rxBufferSize-(_rxOffset+_rxLength);
	if(nToRead > remaining){
		nToRead	=	remaining;
		}

	for(unsigned i=0;i<nToRead;){
		unsigned	nRead	= _streamIn.read(&_rxBuffer[_rxOffset+_rxLength],nToRead);
		if(!nRead){
			_open	= false;
			return;
			}
		_rxLength	+= nRead;
		i			+= nRead;
		}
	_remainingInPacket	-= nToRead;

	Oscl::DES::CBC::decrypt(
		_sessionKey,
		&_rxBuffer[offset],
		nToRead,
		&_rxBuffer[offset],
		nToRead
		);

	if(!_remainingInPacket){
		_rxLength	-= _rxPadding;
		}
	}

unsigned	Adaptor::copyOut(	void*		b,
								unsigned	maxBufferSize
								) noexcept{
	if(maxBufferSize > _rxLength){
		maxBufferSize	= _rxLength;
		}
	if(!maxBufferSize) return 0;
	unsigned char*	buffer	= (unsigned char*)b;
	memcpy(buffer,&_rxBuffer[_rxOffset],maxBufferSize);
	_rxOffset	+= maxBufferSize;
	_rxLength	-= maxBufferSize;
	return maxBufferSize;
	}

