/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_ss_adaptorh_
#define _oscl_ss_adaptorh_
#include "oscl/stream/input/api.h"
#include "oscl/stream/output/api.h"
#include "oscl/bignum/value.h"

/** */
namespace Oscl {
/** */
namespace SS {

/** */
enum{sizeofBigNumInOctets=128};

/** */
enum{bigNumSize=sizeofBigNumInOctets/sizeof(Oscl::BigNum::FragType)};

/** This class provides an encryption layer over any
	communications byte stream (e.g. TCP). Diffie-Hellman
	key exchange is used to create encryption keys for
	each direction in the communication. Once established,
	the connection can be a client program like any other
	stream.
 */
class Adaptor :	public Oscl::Stream::Input::Api,
				public Oscl::Stream::Output::Api
				{
	private:
		/** */
		Oscl::Stream::Input::Api&		_streamIn;

		/** */
		Oscl::Stream::Output::Api&		_streamOut;

		/** */
		enum{keySize=1024/8};

		/** */
		unsigned char					_peerPublicKey[keySize];

		/** */
		bool							_open;

		/** */
		unsigned char					_sessionKey[sizeofBigNumInOctets];

	private:
		/** */
		enum{txBufferSize=2048};
		/** */
		unsigned char					_txBuffer[txBufferSize];
		/** */
		unsigned						_txLength;

	private:
		/** */
		enum{rxBufferSize=2048};
		/** */
		unsigned char					_rxBuffer[rxBufferSize];
		/** */
		unsigned						_rxLength;
		/** */
		unsigned						_rxOffset;
		/** */
		unsigned						_remainingInPacket;
		/** */
		unsigned						_rxPadding;

	public:
		/** */
		Adaptor(	Oscl::Stream::Input::Api&	streamIn,
					Oscl::Stream::Output::Api&	streamOut
					) noexcept;
		/** Upon openning, a Diffie-Helleman public key is
			generated and sent to the peer. When a public
			key is received from the peer, a session key
			is formed, and the open request returns.
		 */
		void	open() noexcept;
		/** */
		void	close() noexcept;
		/** */
		Oscl::Stream::Input::Api&	getInputStream() noexcept;
		/** */
		Oscl::Stream::Output::Api&	getOutputStream() noexcept;

	public:	// Oscl::Stream::Input::Api
		/** */
		unsigned long	read(	void*			dest,
								unsigned long	maxSize
								) noexcept;
		/** */
		unsigned long	read(Buffer::Base& dest) noexcept;

		/** */
		unsigned long	write(	const void*		source,
								unsigned long	count
								) noexcept;

		/** */
		unsigned long	write(const Buffer::Base& source) noexcept;

		/** */
		void	flush() noexcept;

	private:
		/** */
		unsigned		append(	const void*	source,
								unsigned	length
								) noexcept;
		/** */
		void			send() noexcept;
		/** */
		void			recv() noexcept;
		/** */
		unsigned		copyOut(	void*		buffer,
									unsigned	maxBufferSize
									) noexcept;
	};

}
}

#endif
