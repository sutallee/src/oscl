/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_fs_volume_apih_
#define _oscl_fs_volume_apih_
#include "oscl/oid/roapi.h"
#include "oscl/fs/file/ro/oc/result.h"
#include "oscl/fs/file/ro/access/result.h"
#include "oscl/fs/file/rw/oc/result.h"
#include "oscl/fs/file/rw/access/result.h"
#include "oscl/fs/dir/wo/result.h"
#include "state.h"

/** */
namespace Oscl {
/** */
namespace FS {
/** */
namespace Volume {

/** This is the API used by the file system to manipulate
	different kinds of Volumes (e.g. FAT16, HFS, EXT3 ... etc.).
 */
class Api {
	public:
		/** This link reserved for use by the File System.
		 */
		void*	__qitemlink;
	public:
		/** */
		virtual ~Api(){}
		/** */
		virtual const Oscl::ObjectID::RO::Api&	location() const noexcept=0;
		/** */
		virtual const Oscl::FS::File::RO::OC::Open::Status::Result*
				open(	RoStateMem&		stateMem,
						const char*		path,
						RoStateHandle&	handle
						) noexcept=0;
		/** */
		virtual const Oscl::FS::File::RO::OC::Close::Status::Result*
				close(RoStateHandle&	handle) noexcept=0;
		/** */
		virtual const Oscl::FS::File::RW::OC::Open::Status::Result*
				open(	RwStateMem&		stateMem,
						const char*		path,
						RwStateHandle&	handle
						) noexcept=0;
		/** */
		virtual const Oscl::FS::File::RW::OC::Close::Status::Result*
				close(RwStateHandle&	handle) noexcept=0;
		/** */
		virtual const Oscl::FS::File::RO::Access::Read::Status::Result*
				read(	RoStateHandle&	handle,
						void*			dest,
						unsigned long	maxSize,
						unsigned long	offset,
						unsigned long&	nRead
						) noexcept=0;
		/** */
		virtual const Oscl::FS::File::RW::Access::Write::Status::Result*
				write(	RwStateHandle&	handle,
						const void*		src,
						unsigned long	length,
						unsigned long	offset,
						unsigned long&	nWritten
						) noexcept=0;
		/** */
		virtual const Oscl::FS::File::RW::Access::Trunc::Status::Result*
				truncate(	RwStateHandle&	handle,
							unsigned long	size
							) noexcept=0;
		/** */
		virtual const Oscl::FS::Dir::WO::Move::Status::Result*
				move(	const char*	srcPath,
						const char*	destPath
						) noexcept=0;
		/** */
		virtual const Oscl::FS::Dir::WO::Create::Status::Result*
				createFile(const char* path) noexcept=0;
		/** */
		virtual const Oscl::FS::Dir::WO::Create::Status::Result*
				createDir(const char* path) noexcept=0;
		/** */
		virtual const Oscl::FS::Dir::WO::Remove::Status::Result*
				remove(const char* path) noexcept=0;
	};

}
}
}

#endif
