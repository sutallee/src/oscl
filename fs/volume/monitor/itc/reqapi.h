/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_fs_volume_monitor_reqapih_
#define _oscl_fs_volume_monitor_reqapih_
#include "oscl/mt/itc/mbox/srvreq.h"
#include "oscl/mt/itc/mbox/sap.h"
#include "oscl/oid/roapi.h"

/** */
namespace Oscl {
/** */
namespace FS {
/** */
namespace Volume {
/** */
namespace Monitor {
/** */
namespace ITC {
/** */
namespace Req {

using namespace Oscl::Mt::Itc;

/** */
class Api {
	public:
		/** This payload is used to asynchronously wait
			for the specified volume to be mounted in the
			file system.
		 */
		class MountPayload {
			public:
				/** */
				const Oscl::ObjectID::RO::Api&	_volumeID;
			public:
				MountPayload(const Oscl::ObjectID::RO::Api& volumeID) noexcept;
			};
		/** This payload is used to asynchronously wait
			for the specified volume to be unmounted in the
			file system.
		 */
		class UnmountPayload {
			public:
				/** */
				const Oscl::ObjectID::RO::Api&	_volumeID;
			public:
				UnmountPayload(const Oscl::ObjectID::RO::Api& volumeID) noexcept;
			};
		/** */
		class CancelPayload {
			public:
				/** */
				Oscl::Mt::Itc::SrvMsg&	_msgToCancel;
			public:
				/** */
				CancelPayload(Oscl::Mt::Itc::SrvMsg& msgToCancel) noexcept;
			};
	public:
		/** */
		typedef SrvRequest<Api,MountPayload>	MountReq;
		/** */
		typedef SrvRequest<Api,UnmountPayload>	UnmountReq;
		/** */
		typedef SrvRequest<Api,CancelPayload>	CancelReq;

	public:
		/** */
		typedef Oscl::Mt::Itc::SAP<Api>				SAP;
		/** */
		typedef Oscl::Mt::Itc::ConcreteSAP<Api>		ConcreteSAP;

	public:
		/** Make GCC happy */
		virtual ~Api() {}
		/** */
		virtual void	request(MountReq& msg) noexcept=0;
		/** */
		virtual void	request(UnmountReq& msg) noexcept=0;
		/** */
		virtual void	request(CancelReq& msg) noexcept=0;
	};

}
}
}
}
}
}
#endif
