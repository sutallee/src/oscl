/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_fs_volume_stateh_
#define _oscl_fs_volume_stateh_
#include "oscl/memory/block.h"

/** */
namespace Oscl {
/** */
namespace FS {
/** */
namespace Volume {

/** */
class RoState {
	public:
		/** */
		void*		__qitemlink;
	public:
		/** */
		const unsigned long	_sector;
		/** */
		const unsigned		_sectorIndex;
	public:
		/** */
		RoState(	unsigned long	sector,
					unsigned		sectorIndex
					) noexcept;
	};

/** */
typedef Oscl::Memory::AlignedBlock<sizeof(RoState)>	RoStateMem;

/** */
struct RoStateHandle {
	/** */
	RoState*	roState;
	/** */
	RoStateHandle() noexcept:roState(0){}
	};

/** */
class RwState : public RoState {
	public:
		/** */
		RwState(	unsigned long	sector,
					unsigned		sectorIndex
					) noexcept;
	};

/** */
typedef Oscl::Memory::AlignedBlock<sizeof(RwState)>	RwStateMem;

/** */
struct RwStateHandle : public RoStateHandle {
	/** */
	RwState*	rwState;
	/** */
	RwStateHandle() noexcept:RoStateHandle(),rwState(0){}
	};

}
}
}

#endif
