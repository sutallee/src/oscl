/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_fs_volume_fat16_dirh_
#define _oscl_fs_volume_fat16_dirh_
#include "oscl/block/rw/cache/api.h"
#include "oscl/fs/fat/fat.h"

/** */
namespace Oscl {
/** */
namespace FS {
/** */
namespace Volume {
/** */
namespace FAT16 {

/** */
class DirSector {
	private:
		/** */
		Oscl::Block::RW::Cache::Api&	_cacheApi;
		/** */
		const unsigned long				_sector;
		/** */
		const unsigned					_nEntries;
		/** */
		bool							_ioError;
	public:
		/** */
		DirSector(	Oscl::Block::RW::Cache::Api&	cacheApi,
					unsigned long					sector,
					unsigned						nEntries
					) noexcept;
		/** Returns true if any cache operations executed during
			the life-time of this object returned a transport error.
			This operation should be invoked by the client if any
			of this objects operations fail.
		 */
		bool	ioError() const noexcept;
		/** Returns zero if the index is out-of-range */
		Oscl::FS::FAT::DirEntry*	entry(unsigned index) noexcept;
		/** Indicate that the entry has been modified */
		void	touch() noexcept;
		/** Returns zero for failure. */
		Oscl::FS::FAT::DirEntry*
			makeDir(	unsigned							index,
						const char*							name,
						unsigned							clusterChain,
						unsigned							rootCluster,
						Oscl::FS::FAT::FAT16::Partition&	partition
						) noexcept;
		/** Returns zero for failure. */
		Oscl::FS::FAT::DirEntry*
			makeFile(	unsigned							index,
						const char*							name,
						Oscl::FS::FAT::FAT16::Partition&	partition
						) noexcept;
	};

/** */
class DirCluster {
	private:
		/** */
		Oscl::Block::RW::Cache::Api&	_cacheApi;
		/** */
		const unsigned long				_rootSector;
		/** */
		const unsigned					_nEntries;
		/** */
		bool							_ioError;
	public:
		/** */
		DirCluster(	Oscl::Block::RW::Cache::Api&	cacheApi,
					unsigned long					rootSector,
					unsigned						nEntries
					) noexcept;
		/** Returns true if any cache operations executed during
			the life-time of this object returned a transport error.
			This operation should be invoked by the client if any
			of this objects operations fail.
		 */
		bool	ioError() const noexcept;
		/** Returns zero if the index is out-of-range */
		Oscl::FS::FAT::DirEntry*	entry(unsigned index) noexcept;
		/** Returns zero if the index is out-of-range */
		Oscl::FS::FAT::DirEntry*	entry(	unsigned		index,
											unsigned long&	sector,
											unsigned&		sectorIndex
											) noexcept;
		/** This operation will attempt to create and a new directory
			entry, using the value of "model". This is a simple operation
			which cannot stand alone. In the case of a directory, the
			first directory cluster MUST have the dot and dotdot directory
			entries updated such that they reference the appropriate
			directory clusters.
			IMPORTANT: The "model" argument must NOT reference a volatile
			cached copy of the directory entry. Rather it MUST be a copy
			of the directory entry.
			A pointer to the new directory entry is returned, and the
			sector and sectorIndex are updated.  The _ioError flag of
			this class is set to "true" if any transport errors are
			encountered.
		 */
		Oscl::FS::FAT::DirEntry*
		newEntryCopy(	Oscl::FS::FAT::DirEntry&			entryCopy,
						Oscl::FS::FAT::FAT16::Partition&	partition,
						unsigned							rootCluster,
						unsigned long&						sector,
						unsigned&							sectorIndex
						) noexcept;
		/** */
		Oscl::FS::FAT::DirEntry*
		findFreeEntry(	unsigned long&	sector,
						unsigned&		sectorIndex
						) noexcept;
	};

/** */
class Directory {
	private:
		/** */
		Oscl::Block::RW::Cache::Api&		_cacheApi;
		/** */
		Oscl::FS::FAT::FAT16::Partition&	_partition;
		/** */
		const unsigned long					_rootCluster;
		/** */
		bool								_ioError;
	public:
		/** */
		Directory(	Oscl::Block::RW::Cache::Api&		cacheApi,
					Oscl::FS::FAT::FAT16::Partition&	partition,
					unsigned long						rootCluster
					) noexcept;
		/** Returns true if any cache operations executed during
			the life-time of this object returned a transport error.
			This operation should be invoked by the client if any
			of this objects operations fail.
		 */
		bool	ioError() const noexcept;

		/** */
		Oscl::FS::FAT::DirEntry*	nameMatch(	const char*		name,
												unsigned		nameLen,
												unsigned long&	sector,
												unsigned&		sectorIndex
												) noexcept;

		/** Returns zero if the index is out-of-range */
		Oscl::FS::FAT::DirEntry*	entry(unsigned index) noexcept;
		/** Returns zero if the index is out-of-range */
		Oscl::FS::FAT::DirEntry*	entry(	unsigned		index,
											unsigned long&	sector,
											unsigned&		sectorIndex
											) noexcept;
		/** This operation will attempt to allocate a new file
			directory entry in this directory. If successful,
			a pointer to the cached copy of the new directory
			entry is returned, along with the sector and sectorIndex
			that correspond to the new directory entry. If all entries
			in the currently allocated clusters for the directory
			are allocated, no attempt will be made to add a new
			cluster to the directory. It is the client's responsibility
			to allocate another cluster for the directory, append
			it to the directories cluster chain and the retry this
			operation.
		 */
		Oscl::FS::FAT::DirEntry*	newFile(	const char*		name,
												unsigned long&	sector,
												unsigned&		sectorIndex
												) noexcept;
		/** This operation will attempt to create and a new directory
			entry, using the value of "model". This is a simple operation
			which cannot stand alone. In the case of a directory, the
			first directory cluster MUST have the dot and dotdot directory
			entries updated such that they reference the appropriate
			directory clusters.
			IMPORTANT: The "model" argument must NOT reference a volatile
			cached copy of the directory entry. Rather it MUST be a copy
			of the directory entry.
			A pointer to the new directory entry is returned, and the
			sector and sectorIndex are updated.  The _ioError flag of
			this class is set to "true" if any transport errors are
			encountered.
		 */
		Oscl::FS::FAT::DirEntry*	newEntryCopy(	Oscl::FS::FAT::DirEntry&	model,
													unsigned long&				sector,
													unsigned&					sectorIndex
													) noexcept;
		/** This operation will attempt to create and initialize
			a new *empty* sub-directory entry. This entails first allocating
			and initializing a new directory entry. If the existing
			directory structure is full, then the operation returns zero.
			This operation will NOT allocate a new cluster for the
			directory to accomadate the new entry. Instead, the operation
			will return zero to indicate that there is no more free space
			in the directory. It is the responsibility of the client to
			allocate an additional structure, append it to the directory
			entry (appendClusterChain()), and then re-try this operation.
			The "clusterChain" argument is the space allocated by the
			client to contain the new directory's data structures.
			The "clusterChain" will be formatted during the execution
			of this operation and the dot and dotdot directory entries
			will be created and initialized.
			A pointer to the new directory entry is returned, and the
			sector and sectorIndex are updated.  The _ioError flag of
			this class is set to "true" if any transport errors are
			encountered.
		 */
		Oscl::FS::FAT::DirEntry*	newDirectory(	const char*		name,
													unsigned		clusterChain,
													unsigned long&	sector,
													unsigned&		sectorIndex
													) noexcept;
		/** This operation appends the specified cluster chain
			to the end of the directory's existing cluster chain.
			Returns true if the operation fails due to an I/O (a.k.a. transport)
			error. Returns false if successful.
		 */
		bool	appendClusterChain(unsigned clusterChain) noexcept;
		
#if 0
		/** Returns true if the procedure fails. */
		bool	Directory::formatClusterChain(unsigned chain) noexcept;
#endif

	private:
		/** */
		Oscl::FS::FAT::DirEntry*	findFreeEntry(	unsigned long&	sector,
													unsigned&		sectorIndex
													) noexcept;
#if 0
		/** This operation initializes the directory entries in the
			specified cluster. This is used when adding a new cluster
			to the directory's cluster chain.
		 */
		bool	formatCluster(unsigned cluster) noexcept;
#endif

		/** */
		bool	findTheFirstSectorofTheNthCluster(	unsigned long	firstCluster,
													unsigned long&	firstSector,
													unsigned 		clusterN
													) noexcept;
	};

/** A collection of FAT16 partition related functions.
 */
class Partition {
	private:
		/** */
		Oscl::Block::RW::Cache::Api&		_cacheApi;
		/** */
		Oscl::FS::FAT::FAT16::Partition&	_partition;
		/** */
		bool								_ioError;
	public:
		/** */
		Partition(	Oscl::Block::RW::Cache::Api&		cacheApi,
					Oscl::FS::FAT::FAT16::Partition&	partition
					) noexcept;
		/** Returns true if any cache operations executed during
			the life-time of this object returned a transport error.
			This operation should be invoked by the client if any
			of this objects operations fail.
		 */
		bool	ioError() const noexcept;

		/** Returns true if the procedure fails. */
		bool	formatDirectoryClusterChain(unsigned chain) noexcept;

		/** This operation initializes the directory entries in the
			specified cluster. This is used when adding a new cluster
			to the directory's cluster chain.
		 */
		bool	formatDirectoryCluster(unsigned cluster) noexcept;
	};

}
}
}
}

#endif
