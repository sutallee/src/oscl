/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_fs_volume_fat16_fileh_
#define _oscl_fs_volume_fat16_fileh_
#include "oscl/block/rw/cache/api.h"
#include "oscl/fs/fat/fat.h"

/** */
namespace Oscl {
/** */
namespace FS {
/** */
namespace Volume {
/** */
namespace FAT16 {

/** */
class FileCluster {
	private:
		/** */
		Oscl::Block::RW::Cache::Api&	_cacheApi;
		/** */
		const unsigned long				_rootSector;
		/** Number of valid bytes in the cluster.
			The last cluster in a file typically has
			fewer bytes than sectorsPerCluster*sectorSize.
		 */
		const unsigned					_clusterLen;
		/** Number of bytes in each sector.
		 */
		const unsigned					_sectorSize;
		/** */
		bool							_ioError;
	public:
		/** */
		FileCluster(	Oscl::Block::RW::Cache::Api&	cacheApi,
						unsigned long					rootSector,
						unsigned						clusterLen,
						unsigned						sectorSize
						) noexcept;

		/** Returns true if any cache operations executed during
			the life-time of this object returned a transport error.
			This operation should be invoked by the client if any
			of this objects operations fail.
		 */
		bool	ioError() const noexcept;

		/** Returns number of bytes copied */
		unsigned	read(	void*		buffer,
							unsigned	maxRead,
							unsigned	offset
							) noexcept;
		/** */
		void	truncate(unsigned long size) noexcept;

		/** Returns number of bytes copied */
		unsigned long	write(	const void*		buffer,
								unsigned 		length,
								unsigned 		offset
								) noexcept;

	private:
		/** Returns number of bytes copied */
		unsigned	readSector(	void*			buffer,
								unsigned		maxRead,
								unsigned		offset,
								unsigned long	sector,
								unsigned		sectorLen
								) noexcept;
		/** Returns number of bytes copied */
		unsigned	writeSector(	const void*		buffer,
									unsigned		length,
									unsigned		offset,
									unsigned long	sector,
									unsigned		sectorLen
									) noexcept;
	};

/** */
class File {
	private:
		/** */
		Oscl::Block::RW::Cache::Api&		_cacheApi;
		/** */
		Oscl::FS::FAT::FAT16::Partition&	_partition;
		/** */
		const unsigned long					_rootCluster;
		/** */
		const unsigned long					_fileSize;
		/** */
		bool								_ioError;
	public:
		/** */
		File(	Oscl::Block::RW::Cache::Api&		cacheApi,
				Oscl::FS::FAT::FAT16::Partition&	partition,
				unsigned long						rootCluster,
				unsigned long						fileSize
				) noexcept;
		/** Returns true if any cache operations executed during
			the life-time of this object returned a transport error.
			This operation should be invoked by the client if any
			of this objects operations fail.
		 */
		bool	ioError() const noexcept;

		/** Returns number of bytes copied */
		unsigned long	read(	void*			buffer,
								unsigned long	maxRead,
								unsigned long	offset
								) noexcept;

		/** */
		void	truncate(unsigned long size) noexcept;

		/** Returns number of bytes copied
			Note: This operation assumes that there are enough
			clusters in the files cluster chain to hold the
			specified data. This means that cluster chain allocation
			must be done before invoking this operation.
		 */
		unsigned long	write(	const void*		buffer,
								unsigned long	length,
								unsigned long	offset
								) noexcept;

	private:
		/** */
		bool	findTheFirstSectorofTheNthCluster(	unsigned long	firstCluster,
													unsigned long&	firstSector,
													unsigned 		clusterN
													) noexcept;
	};

}
}
}
}

#endif
