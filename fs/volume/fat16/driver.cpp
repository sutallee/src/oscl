/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <new>
#include <stdio.h>
#include <string.h>
#include "driver.h"
#include "oscl/error/info.h"
#include "oscl/mt/thread.h"
#include "file.h"
#include "oscl/fs/file/ro/oc/status.h"
#include "oscl/fs/file/ro/access/status.h"
#include "oscl/fs/file/rw/oc/status.h"
#include "oscl/fs/file/rw/access/status.h"
#include "oscl/fs/dir/wo/status.h"

#undef FAT16DEBUGSWITCH

static const char*	index(const char* s, unsigned sLen, char c) noexcept{
	for(unsigned i=0;i<sLen;++i){
		if(s[i] == c){
			return &s[i];
			}
		}
	return 0;
	}

#if 0
static const char*	rindex(const char* s, unsigned sLen, char c) noexcept{
	for(unsigned i=sLen;i>0;--i){
		if(s[i-1] == c){
			return &s[i-1];
			}
		}
	return 0;
	}
#endif

static const char*	leafNameFromPath(const char* name) noexcept{
	const char*	leafname	= name;
	while(true){
		leafname	= rindex(name,'/');
		if(!leafname){
			return name;
			}
		else {
			++leafname;
			if(*leafname != '\0'){
				return leafname;
				}
			}
		}
	}

using namespace Oscl::FS::Volume::FAT16;

Driver::Driver(	Oscl::Block::RW::Cache::Api&	cacheApi,
				const Oscl::ObjectID::RO::Api&	location,
				uint32_t						rootDirSectors,
				uint32_t						firstDataSector,
				uint8_t						sectorsPerCluster,
				uint16_t						reservedSectorCount,
				uint16_t						bytesPerSector,
				uint32_t						fatSize,
				uint32_t						firstRootDirSecNum,
				uint32_t						totalSectors,
				uint16_t						rootEntryCount
				) noexcept:
		_cacheApi(cacheApi),
		_location(location),
		_partition(	rootDirSectors,
					firstDataSector,
					sectorsPerCluster,
					reservedSectorCount,
					bytesPerSector,
					fatSize,
					firstRootDirSecNum,
					totalSectors
					),
		_rootDir(	cacheApi,
					firstRootDirSecNum,
					rootEntryCount
					)
		{
#ifdef FAT16DEBUGSWITCH 
	print();
	print2();
	static const char	str1[]	= "adir";
	static const char	str2[]	= "ADIR";
	static const char	str3[]	= "num.txt";
	static const char	str4[]	= "NUM.TXT";
	static const char	str5[]	= "/NUM.TXT";
	static const char	str6[]	= "ADIR/TEMP";
	static const char	str7[]	= "ADIR/3071";
	if(findDirEntry(str1)){
		Oscl::Error::Info::log("found str1\n");
		}
	if(findDirEntry(str2)){
		Oscl::Error::Info::log("found str2\n");
		}
	if(findDirEntry(str3)){
		Oscl::Error::Info::log("found str3\n");
		}
	if(findDirEntry(str4)){
		Oscl::Error::Info::log("found str4\n");
		}
	if(findDirEntry(str5)){
		Oscl::Error::Info::log("found str5\n");
		}
	testFile(str6,0);
	testFile(str7,3070);
#endif
	}

const Oscl::ObjectID::RO::Api&	Driver::location() const noexcept{
	return _location;
	}

///////////////////////////
const Oscl::FS::File::RO::OC::Open::Status::Result*
Driver::open(	RoStateMem&		stateMem,
				const char*		path,
				RoStateHandle&	handle
				) noexcept{
	unsigned long	sector=0;
	unsigned		sectorIndex=0;
	Oscl::FS::FAT::DirEntry*
	dirEntry	= findDirEntry(path,strlen(path),sector,sectorIndex);
	if(!dirEntry){
		return &Oscl::FS::File::RO::OC::Open::Status::getNoSuchDirectoryError();
		}
	if(dirEntry->directory()){
		return &Oscl::FS::File::RO::OC::Open::Status::getNoSuchFileError();
		}
	RoState*	state	= new(&stateMem)
						RoState(	sector,
									sectorIndex
									);
	_roStateList.put(state);
	handle.roState	= state;
	return 0;
	}

const Oscl::FS::File::RO::OC::Close::Status::Result*
Driver::close(RoStateHandle&	handle) noexcept{
	RoState*	state	= _roStateList.remove(handle.roState);
	if(!state){
		return &Oscl::FS::File::RO::OC::Close::Status::getNoSuchHandleError();
		}
	return 0;
	}

const Oscl::FS::File::RO::Access::Read::Status::Result*
Driver::read(	RoStateHandle&	handle,
				void*			dest,
				unsigned long	maxSize,
				unsigned long	offset,
				unsigned long&	nRead
				) noexcept{
	if(!handle.roState){
		return &Oscl::FS::File::RO::Access::Read::
				Status::getNoSuchHandleError();
		}
	DirSector	dir(	_cacheApi,
						handle.roState->_sector,
						512/sizeof(Oscl::FS::FAT::DirEntry)
						);
	Oscl::FS::FAT::DirEntry*
	dirEntry	= dir.entry(handle.roState->_sectorIndex);
	if(!dirEntry || dirEntry->directory()){	// FIXME seperate errors
		return &Oscl::FS::File::RO::Access::Read::
				Status::getTransportError();
		}
	File	file(	_cacheApi,
					_partition,
					dirEntry->firstCluster(),
					dirEntry->fileSize()
					);
	nRead	= file.read(dest,maxSize,offset);
	return 0;
	}

///////////////////////////

const Oscl::FS::File::RW::OC::Open::Status::Result*
Driver::open(	RwStateMem&		stateMem,
				const char*		path,
				RwStateHandle&	handle
				) noexcept{
	unsigned long	sector=0;
	unsigned		sectorIndex=0;
	Oscl::FS::FAT::DirEntry*
	dirEntry	= findDirEntry(path,strlen(path),sector,sectorIndex);
	if(!dirEntry){
		return &Oscl::FS::File::RW::OC::Open::Status::getNoSuchDirectoryError();
		}
	if(dirEntry->directory()){
		return &Oscl::FS::File::RW::OC::Open::Status::getNoSuchFileError();
		}
	RwState*	state	= new(&stateMem)
						RwState(	sector,
									sectorIndex
									);
	_rwStateList.put(state);
	handle.roState	= state;
	handle.rwState	= state;
	return 0;
	}

const Oscl::FS::File::RW::OC::Close::Status::Result*
Driver::close(RwStateHandle&	handle) noexcept{
	RwState*	state	= _rwStateList.remove(handle.rwState);
	if(!state){
		return &Oscl::FS::File::RW::OC::Close::Status::getNoSuchHandleError();
		}
	// FIXME: Instead of flushing the entire cache,
	// I should probably just flush the sectors/blocks that
	// belong to the file.
	_cacheApi.flushHard(0,_partition.totalSectors());
	return 0;
	}

unsigned	Driver::allocateNClusters(unsigned long n) noexcept{
	enum{sectorSize=512};
	enum{fatEntrySize=2};
	const unsigned long	nClusters	=		_partition._fatSize
										*	(sectorSize/fatEntrySize);

	uint16_t	previousCluster	= Oscl::FS::FAT::FAT16::Entry::EOC;
	// FIXME:
	// Currently I just do a brute force search for free
	// blocks from front to back. I'm sure there are more
	// elegant methods, if nothing more than cacheing the
	// index of the last allocated cluster.
	for(unsigned i=0;(i<nClusters) && n;++i){
		uint32_t	fatSector;
		uint32_t	fatEntryOffset;
		_partition.fatEntry(	0,	// fatN
								i,	// cluster
								fatSector,
								fatEntryOffset
								);
		uint8_t*
		fat	= (uint8_t*)_cacheApi.fetch(fatSector);
		Oscl::FS::FAT::FAT16::Entry*
		fatEntry	= (Oscl::FS::FAT::FAT16::Entry*)&fat[fatEntryOffset];
		if(fatEntry->isFree()){
			fatEntry->value(previousCluster);
			_cacheApi.touch(fatSector);
			--n;
			if(!n){
				return i;
				}
			previousCluster	= i;
			}
		}
	freeClusterChain(previousCluster);
	return 0;
	}

void	Driver::freeClusterChain(unsigned firstCluster) noexcept{
	// I need to free the chain that I have
	// accumulated thus far. The loop counter
	// is used to allow us to not hang the system
	// if the cluster chain is corrupt.
	for(unsigned i=0;i<(unsigned)(~0);++i){
		if(firstCluster == Oscl::FS::FAT::FAT16::Entry::EOC){
			return;
			}
		uint32_t	fatSector;
		uint32_t	fatEntryOffset;
		_partition.fatEntry(	0,	// fatN
								firstCluster,	// cluster
								fatSector,
								fatEntryOffset
								);
		uint8_t*
		fat	= (uint8_t*)_cacheApi.fetch(fatSector);
		Oscl::FS::FAT::FAT16::Entry*
		fatEntry	= (Oscl::FS::FAT::FAT16::Entry*)&fat[fatEntryOffset];
		firstCluster	= fatEntry->value();
		fatEntry->markAsFree();
		_cacheApi.touch(fatSector);
		}
	Oscl::Error::Info::log("freeClusterChain : FAT16 corrupt.\n");
	}

const Oscl::FS::File::RW::Access::Write::Status::Result*
Driver::appendClusterToFile(	RwStateHandle&	handle,
								unsigned long	cluster
								) noexcept{
	DirSector	dir(	_cacheApi,
						handle.rwState->_sector,
						512/sizeof(Oscl::FS::FAT::DirEntry)
						);
	Oscl::FS::FAT::DirEntry*
	dirEntry	= dir.entry(handle.rwState->_sectorIndex);
	if(!dirEntry || dirEntry->directory()){	// FIXME seperate errors
		return &Oscl::FS::File::RW::Access::Write::
				Status::getTransportError();
		}
	unsigned	currentCluster	= dirEntry->firstCluster();
	if(!currentCluster){
		// In this case, there were no clusters
		// allocated to this file, so we just tack
		// the cluster onto the directory entry.
		dirEntry->firstCluster(cluster);
		_cacheApi.touch(handle.rwState->_sector);
		return 0;
		}

	// FIXME:
	// The loop counter prevents a corrupt filesystem
	// from hanging me up.
	for(unsigned i=0;i<(unsigned)(~0);++i){
		uint32_t	fatSector;
		uint32_t	fatEntryOffset;
		_partition.fatEntry(	0,	// fatN
								currentCluster,	// cluster
								fatSector,
								fatEntryOffset
								);
		uint8_t*
		fat	= (uint8_t*)_cacheApi.fetch(fatSector);
		Oscl::FS::FAT::FAT16::Entry*
		fatEntry	= (Oscl::FS::FAT::FAT16::Entry*)&fat[fatEntryOffset];
		currentCluster	= fatEntry->value();
		if(currentCluster == Oscl::FS::FAT::FAT16::Entry::EOC){
			fatEntry->value(cluster);
			_cacheApi.touch(fatSector);
			return 0;
			}
		}
	Oscl::Error::Info::log("FAT16 corrupt.\n");
	return &Oscl::FS::File::RW::Access::Write::Status::getOutOfStorageSpaceError();
	}

const Oscl::FS::File::RW::Access::Write::Status::Result*
Driver::write(	RwStateHandle&	handle,
				const void*		src,
				unsigned long	length,
				unsigned long	offset,
				unsigned long&	nWritten
				) noexcept{
	enum{sectorSize=512};
	if(!handle.rwState){
		return &Oscl::FS::File::RW::Access::Write::
				Status::getNoSuchHandleError();
		}
	DirSector	dir(	_cacheApi,
						handle.rwState->_sector,
						512/sizeof(Oscl::FS::FAT::DirEntry)
						);
	Oscl::FS::FAT::DirEntry*
	dirEntry	= dir.entry(handle.rwState->_sectorIndex);
	if(!dirEntry || dirEntry->directory()){	// FIXME seperate errors
		return &Oscl::FS::File::RW::Access::Write::
				Status::getTransportError();
		}
	unsigned long	fileSize		= dirEntry->fileSize();
	// Need to allocate and append some clusters
	unsigned long	newFileSize	= offset+length;
	if(fileSize < newFileSize){
		// 
		unsigned long	bytesPerCluster	= 512*_partition.sectorsPerCluster();
		// 
		unsigned long	newTotalClusters	= ((offset+length)+(bytesPerCluster-1))/bytesPerCluster;
		// 
		unsigned long	currentTotalClusters	= ((fileSize)+(bytesPerCluster-1))/bytesPerCluster;
		// 
		unsigned long	nClustersToAllocate	= newTotalClusters - currentTotalClusters;
		// If we need more clusters, then allocate them
		// and link them to the end of the existing cluster
		// chain.
		if(nClustersToAllocate){
			// Allocate the new chain of clusters
			unsigned long cluster	= allocateNClusters(nClustersToAllocate);
			if(!cluster){
				// Not enough clusters, to satisfy
				nWritten	= 0;
				return 0;	// FIXME: need to Oscl::FS::File::RW::Access::Write::Status::getOutOfStorageSpaceError();
				}
			// Link the new chain of clusters to the end
			// of the existing clusters for the file.
			if(appendClusterToFile(	handle,
									cluster
									)){
				// The only way that this can fail is
				// an I/O error, since all allocation
				// is complete.
				return &Oscl::FS::File::RW::Access::Write::Status::getTransportError();
				}
			// Refresh the directory entry
			dirEntry	= dir.entry(handle.rwState->_sectorIndex);
			if(!dirEntry || dirEntry->directory()){	// FIXME seperate errors
				return &Oscl::FS::File::RW::Access::Write::
						Status::getTransportError();
				}
			}
		}
	unsigned long	firstCluster	= dirEntry->firstCluster();
	// If we make it to here, there are enough clusters in the file's
	// cluster chain to hold the user data.
	dirEntry->fileSize(newFileSize);
	dir.touch();
	File	file(	_cacheApi,
					_partition,
					firstCluster,
					newFileSize
					);
	nWritten	= file.write(src,length,offset);
	return 0;
	}

const Oscl::FS::File::RW::Access::Trunc::Status::Result*
Driver::truncate(	RwStateHandle&	handle,
					unsigned long	size
					) noexcept{
	enum{sectorSize=512};
	if(!handle.rwState){
		return &Oscl::FS::File::RW::Access::Trunc::
				Status::getNoSuchHandleError();
		}
	DirSector	dir(	_cacheApi,
						handle.rwState->_sector,
						sectorSize/sizeof(Oscl::FS::FAT::DirEntry)
						);
	Oscl::FS::FAT::DirEntry*
	dirEntry	= dir.entry(handle.rwState->_sectorIndex);
	if(!dirEntry || dirEntry->directory()){	// FIXME seperate errors
		return &Oscl::FS::File::RW::Access::Trunc::
				Status::getTransportError();
		}
	unsigned long	fileSize		= dirEntry->fileSize();
	unsigned long	firstCluster	= dirEntry->firstCluster();
	if(fileSize < size){
		/** nothing to truncate */
		return 0;
		}
	if(size == 0){
		dirEntry->firstCluster(0);
		}
	dirEntry->fileSize(size);
	dir.touch();
	File	file(	_cacheApi,
					_partition,
					firstCluster,
					fileSize
					);
	file.truncate(size);
	return 0;
	}

///////////////////////////

const Oscl::FS::Dir::WO::Move::Status::Result*
Driver::move(	const char*	srcPath,
				const char*	destPath
				) noexcept{
	enum{sectorSize=512};
	// First determine if the source and destination
	// are the same.
	// FIXME: Ensure that leading and trailing
	// path seperators ('/') do not cause the paths
	// to appear to be different.

	// Strip off leading path delimiters
	while(*srcPath && (*srcPath=='/')){
		++srcPath;
		}
	while(*destPath && (*destPath=='/')){
		++destPath;
		}

	// Strip off trailing path delimiters
	unsigned	srcLen	= strlen(srcPath);
	for(;srcLen;--srcLen){
		if(srcPath[srcLen-1] != '/'){
			break;
			}
		}
	unsigned	destLen	= strlen(destPath);
	for(;destLen;--destLen){
		if(destPath[destLen-1] != '/'){
			break;
			}
		}

	// Compare source and destination paths
	if(srcLen == destLen){
		if(!strncmp(srcPath,destPath,srcLen)){
			// Can't move source to itself
			return &Oscl::FS::Dir::WO::Move::Status::getInvalidDestPath();
			}
		}

	// Identify the source.
	unsigned long	srcSector;
	unsigned		srcSectorIndex;
	Oscl::FS::FAT::DirEntry*
	srcEntry	= findDirEntry(srcPath,strlen(srcPath),srcSector,srcSectorIndex);
	if(!srcEntry){
		return &Oscl::FS::Dir::WO::Move::Status::getNoSuchSrcPath();
		}
	Oscl::FS::FAT::DirEntry		srcEntryCopy;

	// Make a non-volatile (non-cached) copy of the source entry
	srcEntryCopy	= *srcEntry;

	// Note, that after this findDirEntry() operation,
	// the srcEntry pointer is NOT guaranteed to be valid
	// since the cache entry may be reused.
	unsigned long	destSector;
	unsigned		destSectorIndex;
	Oscl::FS::FAT::DirEntry*
	destEntry	= findDirEntry(destPath,strlen(destPath),destSector,destSectorIndex);
	if(!destEntry){
		//	Need to create the destination.
		//	1. Seperate the destination directory and leafname.
		const char*		destDirName	= leafNameFromPath(destPath);
		if(!Oscl::FS::FAT::isValidShortFileName(destDirName)){
			return &Oscl::FS::Dir::WO::Move::Status::getInvalidDestPath();
			}
		if((destDirName == destPath) || ((destPath[0] == '/') && (destDirName == &destPath[1]))){
			// We're looking to create the destination in the root directory.
			destEntry	= _rootDir.newEntryCopy(	srcEntryCopy,
													_partition,
													0,	// root cluster
													destSector,
													destSectorIndex
													);
			if(!destEntry){
				if(_rootDir.ioError()){
					return &Oscl::FS::Dir::WO::Move::Status::getTransportError();
					}
				return &Oscl::FS::Dir::WO::Move::Status::getOutOfSpace();
				}
			destEntry->name(destDirName);
			_cacheApi.touch(destSector);
			}
		else {
			// The destination will be created in a sub-directory
			// Find the destination directory for the new entry.
			destEntry	= findDirEntry(destPath,destLen-strlen(destDirName),destSector,destSectorIndex);
			if(!destEntry || !destEntry->directory()){
				return &Oscl::FS::Dir::WO::Move::Status::getInvalidDestPath();
				}
			unsigned	firstCluster	= destEntry->firstCluster();
			Directory	dir(	_cacheApi,
								_partition,
								firstCluster
								);
			destEntry	= dir.newEntryCopy(	srcEntryCopy,
											destSector,
											destSectorIndex
											);
			if(!destEntry){
				if(dir.ioError()){
					// Normally I would release the cluster
					// chains that I've allocated, but since
					// this failure is an I/O error, there's
					// not much I can do.
					return &Oscl::FS::Dir::WO::Move::Status::getTransportError();
					}
				unsigned	dCluster	= allocateNClusters(1);
				if(!dCluster){
					return &Oscl::FS::Dir::WO::Move::Status::getOutOfSpace();
					}
				if(dir.appendClusterChain(dCluster)){
					// Normally I would release the cluster
					// chains that I've allocated, but since
					// this failure is an I/O error, there's
					// not much I can do.
					return &Oscl::FS::Dir::WO::Move::Status::getTransportError();
					}
				destEntry	= dir.newEntryCopy(	srcEntryCopy,
												destSector,
												destSectorIndex
												);
				if(!destEntry){
					if(!dir.ioError()){
						// I don't know what the problem is if it ain't I/O
						// but I may as well attempt to release the allocated
						// cluster chains.
						}
					return &Oscl::FS::Dir::WO::Move::Status::getTransportError();
					}
				}
			destEntry->name(destDirName);
			_cacheApi.touch(destSector);
			}
		// At this point I have cloned the source file directory
		// entry into the destEntry.
		// Step 2. Free the original directory entry.

		// Refresh the srcEntry pointer.
		DirSector	ds(	_cacheApi,
						srcSector,
						sectorSize/sizeof(Oscl::FS::FAT::DirEntry)
						);
		srcEntry	= ds.entry(srcSectorIndex);
		if(!srcEntry){
			return &Oscl::FS::Dir::WO::Move::Status::getTransportError();
			}
		// Mark the source directory entry as free.
		srcEntry->free();
		ds.touch();
		return 0;
		}

	if(srcEntryCopy.directory()){
		if(!destEntry->directory()){
			// Can't move a directory to a file.
			return &Oscl::FS::Dir::WO::Move::Status::getInvalidDestPath();
			}
		// Recursively copy src directory to dest directory.
		// 
		const char*	srcDirName	= leafNameFromPath(srcPath);
		const char*	destDirName	= leafNameFromPath(destPath);
		if(!strcmp(srcDirName,destDirName)){
			// Cant move directory to directory of the same name.
			// This is standard Linux behavior.
			return &Oscl::FS::Dir::WO::Move::Status::getInvalidDestPath();
			}

		// Since the source and destination directory
		// names are NOT the same, we create a new
		// directory inside the destination directory
		// using the name of the source directory.
		unsigned	dotDotCluster	= destEntry->firstCluster();
		Directory	dir(	_cacheApi,
							_partition,
							dotDotCluster
							);
		unsigned long	sector;
		unsigned		sectorIndex;
		if(dir.nameMatch(srcDirName,strlen(srcDirName),sector,sectorIndex)){
			// Already an entry in the directory by that name
			return &Oscl::FS::Dir::WO::Move::Status::getInvalidDestPath();
			}
		// Allocate a cluster for the new directory.
		unsigned cluster	= allocateNClusters(1);
		if(!cluster){
			return &Oscl::FS::Dir::WO::Move::Status::getOutOfSpace();
			}
		destEntry	= dir.newEntryCopy(	srcEntryCopy,
										sector,
										sectorIndex
										);
		if(!destEntry){
			if(dir.ioError()){
				// Normally I would release the cluster
				// chains that I've allocated, but since
				// this failure is an I/O error, there's
				// not much I can do.
				return &Oscl::FS::Dir::WO::Move::Status::getTransportError();
				}
			unsigned	dCluster	= allocateNClusters(1);
			if(!dCluster){
				freeClusterChain(cluster);
				return &Oscl::FS::Dir::WO::Move::Status::getOutOfSpace();
				}
			if(dir.appendClusterChain(dCluster)){
				// Normally I would release the cluster
				// chains that I've allocated, but since
				// this failure is an I/O error, there's
				// not much I can do.
				return &Oscl::FS::Dir::WO::Move::Status::getTransportError();
				}
			destEntry	= dir.newEntryCopy(	srcEntryCopy,
											sector,
											sectorIndex
											);
			if(!destEntry){
				if(!dir.ioError()){
					// I don't know what the problem is if it ain't I/O
					// but I may as well attempt to release the allocated
					// cluster chains.
					freeClusterChain(cluster);
					}
				return &Oscl::FS::Dir::WO::Move::Status::getTransportError();
				}
			}
		// At this point I have created a new directory and
		// have a reference to its entry "destEntry".
		// Next, I need to adjust the dot and dotdot directory
		// entries of the new directory, and finally free the
		// source directory entry.
		// Since I have a valid "destEntry" pointer at this
		// point, I begin by updating the dot and dotdot
		// directories. I ASSUME that these directories already
		// exist and occupy the first and second directory
		// entries of the table.
		Directory	destDir(	_cacheApi,
								_partition,
								destEntry->firstCluster()
								);
		unsigned long	dotSector;
		unsigned		dotSectorIndex;

		// Update the "." dot directory entry
		destEntry	= destDir.entry(0,dotSector,dotSectorIndex);
		if(!destEntry){
			return &Oscl::FS::Dir::WO::Move::Status::getTransportError();
			}
		destEntry->firstCluster(srcEntryCopy.firstCluster());
		_cacheApi.touch(dotSector);

		// Create the ".." dotdot directory entry
		destEntry	= destDir.entry(1,dotSector,dotSectorIndex);
		if(!destEntry){
			return &Oscl::FS::Dir::WO::Move::Status::getTransportError();
			}
		destEntry->firstCluster(dotDotCluster);
		_cacheApi.touch(dotSector);

		// Now that I have updated the dot and dotdot
		// directories, I just need to remove/free the
		// old source directory entry.
		DirSector	ds(	_cacheApi,
						srcSector,
						sectorSize/sizeof(Oscl::FS::FAT::DirEntry)
						);
		srcEntry	= ds.entry(srcSectorIndex);
		srcEntry->free();
		ds.touch();
		return 0;
		}

	// Where moving a file if we get here.
	//
	if(destEntry->directory()){
		// The destination is a directory, then
		// we allocate a new directory entry in the
		// destination directory, copy the contents
		// of the original (source) directory entry
		// into the new entry, and finally free
		// the original (source) directory entry.

		// Step 1. Allocate a new directory entry
		// for the destination filie in the destination
		// directory.

		Directory	dir(	_cacheApi,
							_partition,
							destEntry->firstCluster()
							);
		unsigned long	sector;
		unsigned		sectorIndex;
		destEntry	= dir.newEntryCopy(	srcEntryCopy,
										sector,
										sectorIndex
										);
		if(!destEntry){
			// Either we have an I/O error, or we are
			// out of directory space.
			if(dir.ioError()){
				// Normally I would release the cluster
				// chains that I've allocated, but since
				// this failure is an I/O error, there's
				// not much I can do.
				return &Oscl::FS::Dir::WO::Move::Status::getTransportError();
				}
			// At this point we know that we're out of
			// directory space, so we allocate a new cluster
			// for the directory and then we append it to
			// the directory's cluster chain.
			unsigned	dCluster	= allocateNClusters(1);
			if(!dCluster){
				return &Oscl::FS::Dir::WO::Move::Status::getOutOfSpace();
				}
			// Now lets give the newly allocated cluster
			// chain to the directory.
			if(dir.appendClusterChain(dCluster)){
				// Normally I would release the cluster
				// chains that I've allocated, but since
				// this failure is an I/O error, there's
				// not much I can do.
				return &Oscl::FS::Dir::WO::Move::Status::getTransportError();
				}
			// Now try again to create the new directory
			// entry for the file.
			destEntry	= dir.newEntryCopy(	srcEntryCopy,
											sector,
											sectorIndex
											);
			if(!destEntry){
				// Really, the only way this can file now is a software
				// design issue or an I/O error.
				if(!dir.ioError()){
					// I don't know what the problem is so it must
					// be a software design error.
					Oscl::Error::Info::log("FAT16 move software error.\n");
					}
				return &Oscl::FS::Dir::WO::Move::Status::getTransportError();
				}
			}
		// At this point I have cloned the source file directory
		// entry into the destEntry.
		// Step 2. Free the original directory entry.

		// Refresh the srcEntry pointer.
		DirSector	ds(	_cacheApi,
						srcSector,
						sectorSize/sizeof(Oscl::FS::FAT::DirEntry)
						);
		srcEntry	= ds.entry(srcSectorIndex);
		if(!srcEntry){
			return &Oscl::FS::Dir::WO::Move::Status::getTransportError();
			}
		// Mark the source directory entry as free.
		srcEntry->free();
		ds.touch();
		return 0;
		}

	// If we get here then both the source and
	// destination entries are files, thus,
	// we truncate the destination file to free
	// all the clusters associated with the file,
	// copy the source directory entry to the
	// destination directory entry
	// (no allocation required), and finally free
	// the source entry.
	//

	// Step 1. Truncate the destination file.
	unsigned long	fileSize		= destEntry->fileSize();
	unsigned long	firstCluster	= destEntry->firstCluster();
	destEntry->firstCluster(0);
	destEntry->fileSize(0);
	_cacheApi.touch(destSector);
	File	file(	_cacheApi,
					_partition,
					firstCluster,
					fileSize
					);
	file.truncate(0);
	if(file.ioError()){
		// Destination file contents are possibly lost
		// if this happens, however the source is still
		// intact.
		return &Oscl::FS::Dir::WO::Move::Status::getTransportError();
		}

	// Refresh the destEntry pointer.
	DirSector	dd(	_cacheApi,
					destSector,
					sectorSize/sizeof(Oscl::FS::FAT::DirEntry)
					);
	destEntry	= dd.entry(destSectorIndex);
	if(!destEntry){
		// Destination file contents are possibly lost
		// if this happens, however the source is still
		// intact.
		return &Oscl::FS::Dir::WO::Move::Status::getTransportError();
		}

	// Step 2. Copy the source directory entry to the
	// destination directory entry

	// save the destination file name.
	uint8_t	shortName[Oscl::FS::FAT::DirEntry::shortNameLen];
	memcpy(	shortName,
			destEntry->_shortName,
			Oscl::FS::FAT::DirEntry::shortNameLen
			);

	// Copy the whole source entry to the destination
	*destEntry	= srcEntryCopy;
	_cacheApi.touch(destSector);

	// Restore the dest entry's name.
	memcpy(	destEntry->_shortName,
			shortName,
			Oscl::FS::FAT::DirEntry::shortNameLen
			);

	// Step 3. Free the source directory entry.

	// Refresh the srcEntry pointer.
	DirSector	ds(	_cacheApi,
					srcSector,
					sectorSize/sizeof(Oscl::FS::FAT::DirEntry)
					);
	srcEntry	= ds.entry(srcSectorIndex);
	if(!srcEntry){
		// In the unlikely case that this happens, I will have
		// two directory entries that reference the same cluster
		// chain! Yuk. This is where we need to have "dirty volume"
		// indication that would have been set at the beginning
		// of each modifying operation and cleared when the operation
		// is successful. The indication would be left untouched
		// once a failure is encountered and never cleared until
		// the volume is remounted and repaired.
		return &Oscl::FS::Dir::WO::Move::Status::getTransportError();
		}
	srcEntry->free();
	ds.touch();
	// FIXME: Should I flush the cache at this point?
	return 0;
	}

const Oscl::FS::Dir::WO::Create::Status::Result*
Driver::createFile(const char* path) noexcept{
	enum{sectorSize=512};
	Oscl::FS::FAT::DirEntry*	entry;
	entry	= findDirEntry(path);
	if(entry){
		// Already have a directory entry
		// with that name!
		return &Oscl::FS::Dir::WO::Create::Status::getPathAlreadyExists();
		}
	unsigned long	sector;
	unsigned		sectorIndex;
	const char*		leafname	= leafNameFromPath(path);
	if(!Oscl::FS::FAT::isValidShortFileName(leafname)){
		return &Oscl::FS::Dir::WO::Create::Status::getInvalidPath();
		}

	if((leafname == path) || ((path[0] == '/') && (leafname == &path[1]))){
		// We're looking to create a file in the root directory.
		// Thus, we need to create the new directory in the root.
		// Step 2. Allocate a directory entry.
		// Step 3. Initialize the directory entry
		for(unsigned i=0;i<((unsigned)~0);++i){
			entry	= _rootDir.entry(i,sector,sectorIndex);
			if(!entry){
				if(_rootDir.ioError()){
					return &Oscl::FS::Dir::WO::Create::Status::getTransportError();
					}
				return &Oscl::FS::Dir::WO::Create::Status::getDirectoryFull();
				}
			if(entry->isFree()){
				break;
				}
			}
		if(!entry){
			// This should never happen ... but.
			// May be a corrupt FAT16 ... but lets say its an I/O error.
			return &Oscl::FS::Dir::WO::Create::Status::getTransportError();
			}
		DirSector	ds(	_cacheApi,
						sector,
						sectorSize/sizeof(Oscl::FS::FAT::DirEntry)
						);
		entry	= ds.makeFile(	sectorIndex,
								leafname,
								_partition
								);
		if(!entry){
			if(ds.ioError()){
				return &Oscl::FS::Dir::WO::Create::Status::getTransportError();
				}
			// This shouldn't happen.
			return &Oscl::FS::Dir::WO::Create::Status::getTransportError();
			}
		}
	else {
		// We are going to create the new file in
		// a directory. Thus, we need to find the
		// directory entry for directory part of
		// the path.

		unsigned	pathLen	= leafname - path;
		entry	= findDirEntry(path,pathLen,sector,sectorIndex);
		if(!entry){
			// No such path
			return &Oscl::FS::Dir::WO::Create::Status::getInvalidPath();
			}
		if(!entry->directory()){
			// Destination must be a directory
			return &Oscl::FS::Dir::WO::Create::Status::getInvalidPath();
			}
		// At this point we have a pointer to the
		// directory entry into which we will place
		// the new file.
		unsigned	containerCluster	= entry->firstCluster();
		Directory	dir(	_cacheApi,
							_partition,
							containerCluster
							);
		entry	=	dir.newFile(	leafname,
									sector,
									sectorIndex
									);
		if(!entry){
			if(dir.ioError()){
				return &Oscl::FS::Dir::WO::Create::Status::getTransportError();
				}
			// What else could it be but an I/O error?
			// Oh yeah. It means that there are no more directory
			// entries in the directory's entry array. We need
			// to allocate a new cluster and attach it to the
			// directory.
			unsigned	dCluster	= allocateNClusters(1);
			if(!dCluster){
				// Can't create the directory without space for its
				// array of directory entries.
				return &Oscl::FS::Dir::WO::Create::Status::getOutOfSpace();
				}
			if(dir.appendClusterChain(dCluster)){
				return &Oscl::FS::Dir::WO::Create::Status::getTransportError();
				}
			entry	=	dir.newFile(	leafname,
										sector,
										sectorIndex
										);
			if(!entry){
				// MUST be an I/O error.
				return &Oscl::FS::Dir::WO::Create::Status::getTransportError();
				}
			}
		}
	return 0;
	}

const Oscl::FS::Dir::WO::Create::Status::Result*
Driver::createDir(const char* path) noexcept{
	enum{sectorSize=512};
	Oscl::FS::FAT::DirEntry*	entry;
	entry	= findDirEntry(path);
	if(entry){
		// Already have a directory entry
		// with that name!
		return &Oscl::FS::Dir::WO::Create::Status::getPathAlreadyExists();
		}
	unsigned long	sector;
	unsigned		sectorIndex;
	const char*		leafname	= leafNameFromPath(path);
	if(!Oscl::FS::FAT::isValidShortFileName(leafname)){
		return &Oscl::FS::Dir::WO::Create::Status::getInvalidPath();
		}
	// Step 1. Allocate a cluster for the new directory.
	unsigned		cluster	= allocateNClusters(1);
	if(!cluster){
		// Can't create the directory without space for its
		// array of directory entries.
		return &Oscl::FS::Dir::WO::Create::Status::getOutOfSpace();
		}

	if((leafname == path) || ((path[0] == '/') && (leafname == &path[1]))){
		// We're looking to create a file in the root directory.
		// Thus, we need to create the new directory in the root.
		// Step 2. Allocate a directory entry.
		// Step 3. Initialize the directory entry
		for(unsigned i=0;i<((unsigned)~0);++i){
			entry	= _rootDir.entry(i,sector,sectorIndex);
			if(!entry){
				if(_rootDir.ioError()){
					return &Oscl::FS::Dir::WO::Create::Status::getTransportError();
					}
				return &Oscl::FS::Dir::WO::Create::Status::getDirectoryFull();
				}
			if(entry->isFree()){
				break;
				}
			}
		if(!entry){
			// This should never happen ... but.
			// May be a corrupt FAT16 ... but lets say its an I/O error.
			return &Oscl::FS::Dir::WO::Create::Status::getTransportError();
			}
		DirSector	ds(	_cacheApi,
						sector,
						sectorSize/sizeof(Oscl::FS::FAT::DirEntry)
						);
		entry	= ds.makeDir(	sectorIndex,
								leafname,
								cluster,
								0,	// Root cluster
								_partition
								);
		if(!entry){
			if(ds.ioError()){
				return &Oscl::FS::Dir::WO::Create::Status::getTransportError();
				}
			// This shouldn't happen.
			return &Oscl::FS::Dir::WO::Create::Status::getTransportError();
			}
		}
	else {
		// We are going to create the new directory in
		// another directory. Thus, we need to find the
		// directory entry for directory part of the path.

		unsigned	pathLen	= leafname - path;
		entry	= findDirEntry(path,pathLen,sector,sectorIndex);
		if(!entry){
			// No such path
			return &Oscl::FS::Dir::WO::Create::Status::getInvalidPath();
			}
		if(!entry->directory()){
			// Destination must be a directory
			return &Oscl::FS::Dir::WO::Create::Status::getInvalidPath();
			}
		// At this point we have a pointe to the
		// directory entry into which we will place
		// a new directory.
		unsigned	containerCluster	= entry->firstCluster();
		Directory	dir(	_cacheApi,
							_partition,
							containerCluster
							);
		entry	=	dir.newDirectory(	leafname,
										cluster,
										sector,
										sectorIndex
										);
		if(!entry){
			if(dir.ioError()){
				return &Oscl::FS::Dir::WO::Create::Status::getTransportError();
				}
			// What else could it be but an I/O error?
			// Oh yeah. It means that there are no more directory
			// entries in the directory's entry array. We need
			// to allocate a new cluster and attach it to the
			// directory.
			unsigned	dCluster	= allocateNClusters(1);
			if(!dCluster){
				// Can't create the directory without space for its
				// array of directory entries.
				return &Oscl::FS::Dir::WO::Create::Status::getOutOfSpace();
				}
			if(dir.appendClusterChain(dCluster)){
				return &Oscl::FS::Dir::WO::Create::Status::getTransportError();
				}
			entry	=	dir.newDirectory(	leafname,
											cluster,
											sector,
											sectorIndex
											);
			if(!entry){
				// MUST be an I/O error.
				return &Oscl::FS::Dir::WO::Create::Status::getTransportError();
				}
			}
		}
	return 0;
	}

const Oscl::FS::Dir::WO::Remove::Status::Result*
Driver::remove(const char* path) noexcept{
	enum{sectorSize=512};
	unsigned long	sector;
	unsigned		sectorIndex;
	Oscl::FS::FAT::DirEntry*
	entry	= findDirEntry(path,strlen(path),sector,sectorIndex);
	if(!entry){
		return &Oscl::FS::Dir::WO::Remove::Status::getNoSuchPath();
		}
	if(entry->directory()){
		// Recursive directory remove!
		// The question is how to proceed without
		// using recursion.
		// 1.	Create a Directory object for the
		//		"root-of-destruction".
		// 2.	Set this Directory as the "current"
		//		directory.
		// 3.	Looping: search for sub-directories
		//		in the "current" directory.
		// 4.	Delete each non-directory entry that
		//		is encountered. Free cluster chains
		//		for "regular" files.
		// 5.	Recognize and free dot and dotdot
		//		directory entries.
		// 6.	When a directory is encountered,
		//		make the "current" directory this
		//		new directory and continue the
		//		process.
		// 7.	When there are no more directory
		//		entries remaining in the current
		//		directory, delete the current
		//		directory and re-start the process
		//		with the "root-of-destruction"
		//		directory as the "current" directory.
		// 8.	When the "root-of-destruction" directory
		//		has been emptied, the "root-of-destruction"
		//		directory entry is freed and the process
		//		is complte.

		unsigned	cluster	= entry->firstCluster();
		bool		ioError	= false;
		while(rmdir(cluster,ioError));
		if(ioError){
			return &Oscl::FS::Dir::WO::Remove::Status::getTransportError();
			}
		DirSector	ds(	_cacheApi,
						sector,
						sectorSize/sizeof(Oscl::FS::FAT::DirEntry)
						);
		entry	= ds.entry(sectorIndex);
		if(!entry){
			return &Oscl::FS::Dir::WO::Remove::Status::getTransportError();
			}
		freeClusterChain(entry->firstCluster());
		entry->free();
		_cacheApi.touch(sector);
		}
	else {
		// File deletion
		// Step 1. truncate the file to zero length.
		// Step 2. free the directory entry.
		unsigned firstCluster	= entry->firstCluster();
		unsigned long fileSize	= entry->fileSize();
		entry->firstCluster(0);
		entry->fileSize(0);
		entry->free();
		_cacheApi.touch(sector);
		File	file(	_cacheApi,
						_partition,
						firstCluster,
						fileSize
						);
		file.truncate(0);
		}
	return 0;
	}

// Returns true if the we need to restart. Returns false
// if there was an I/O error or the directory is empty.
bool	Driver::rmdir(unsigned cluster,bool& ioError) noexcept{
	enum{sectorSize=512};
	unsigned long				sector;
	unsigned					sectorIndex;
	Oscl::FS::FAT::DirEntry*	entry;
	Directory	dir(	_cacheApi,
						_partition,
						cluster
						);
	for(unsigned i=0;i<((unsigned)~0);++i){
		entry	= dir.entry(i,sector,sectorIndex);
		if(!entry){
			ioError=dir.ioError();
			return false;
			}
		if(entry->isFree()){
			if(entry->noMoreDirEntriesFollow()){
				// We're done with the directory
				ioError	= false;
				return false;
				}
			continue;
			}
		if((i==0) || (i==1)){
			// Catch dot and dotdot directory entries.
			entry->free();
			_cacheApi.touch(sector);
			continue;
			}
		if(entry->directory()){
			// We've found a sub directory
			cluster	= entry->firstCluster();
			break;
			}
		// If we get here, we have a regular file.
		unsigned		firstCluster	= entry->firstCluster();
		unsigned long	fileSize		= entry->fileSize();
		entry->free();
		_cacheApi.touch(sector);
		File	file(	_cacheApi,
						_partition,
						firstCluster,
						fileSize
						);
		file.truncate(0);
		}

	// If we arrive here, it is because we have
	// located a sub-directory, and we will *always*
	// return which a "true" indication unless there
	// is an I/O error.
	unsigned long	subSector;
	unsigned		subSectorIndex;
	while(true){
		Directory	subdir(	_cacheApi,
							_partition,
							cluster
							);
		for(unsigned i=0;i<((unsigned)~0);++i){
			entry	= subdir.entry(i,subSector,subSectorIndex);
			if(!entry){
				ioError=subdir.ioError();
				if(!ioError){
					// We're done with the sub-directory
					// we can delete it and return.
					DirSector	ds(	_cacheApi,
									sector,
									sectorSize/sizeof(Oscl::FS::FAT::DirEntry)
									);
					Oscl::FS::FAT::DirEntry*
					de	= ds.entry(sectorIndex);
					if(!de){
						ioError	= true;
						return false;
						}
					de->free();
					_cacheApi.touch(sector);
					}
				return !ioError;
				}
			if(entry->isFree()){
				if(entry->noMoreDirEntriesFollow()){
					// We're done with the sub-directory
					// we can delete it and return.
					DirSector	ds(	_cacheApi,
									sector,
									sectorSize/sizeof(Oscl::FS::FAT::DirEntry)
									);
					Oscl::FS::FAT::DirEntry*
					de	= ds.entry(sectorIndex);
					if(!de){
						ioError	= true;
						return false;
						}
					freeClusterChain(de->firstCluster());
					de->free();
					_cacheApi.touch(sector);
					ioError	= false;
					return !ioError;
					}
				continue;
				}
			if((i==0) || (i==1)){
				// Catch dot and dotdot directory entries.
				entry->free();
				_cacheApi.touch(subSector);
				continue;
				}
			if(entry->directory()){
				// We've found a sub directory
				cluster	= entry->firstCluster();
				break;
				}
			if(entry->longName()){
				entry->free();
				_cacheApi.touch(sector);
				continue;
				}
			// If we get here, we have a regular file.
			unsigned		firstCluster	= entry->firstCluster();
			unsigned long	fileSize		= entry->fileSize();
			entry->free();
			_cacheApi.touch(sector);
			File	file(	_cacheApi,
							_partition,
							firstCluster,
							fileSize
							);
			file.truncate(0);
			}
		}
	}

///////////////////////////

void	Driver::testFile(const char* path,unsigned long offset) noexcept{
	Oscl::FS::FAT::DirEntry*
	entry	= findDirEntry(path);
	if(!entry){
		Oscl::Error::Info::log("cant find path:");
		Oscl::Error::Info::log(path);
		Oscl::Error::Info::log("\n");
		return;
		}
	if(entry->directory()){
		Oscl::Error::Info::log("expected a file\n");
		return;
		}
	File	file(	_cacheApi,
					_partition,
					entry->firstCluster(),
					entry->fileSize()
					);
	uint8_t	buffer[64];
	unsigned long
	nRead	= file.read(buffer,64,offset);
	if(!nRead){
		Oscl::Error::Info::log("expected nRead to be non-zero\n");
		return;
		}
	if(nRead > 64){
		Oscl::Error::Info::log("expected nRead to not bigger than 64\n");
		return;
		}
	Oscl::Error::Info::hexDump(buffer,nRead);
	}

void	Driver::response(Oscl::Block::Read::ITC::Resp::Api<512>::ReadResp& msg) noexcept{
	}

void	Driver::response(Oscl::Block::Read::ITC::Resp::Api<512>::CancelResp& msg) noexcept{
	}

void	Driver::print() noexcept{
#ifdef FAT16DEBUGSWITCH 
	// Read and display the root directory
	char	buffer[128];
	uint32_t
	firstRootDirSecNum	= _partition.firstRootDirSecNum();
	sprintf(buffer,"firstRootDirSecNum: 0x%8.8lX\n",firstRootDirSecNum);
	Oscl::Error::Info::log(buffer);
	Oscl::Mt::Thread::sleep(1000);

	void*
	mem	= _cacheApi.fetch(_partition.firstRootDirSecNum());
	
	if(!mem){
		Oscl::Error::Info::log("Oscl::FS::Volume::FAT16::Driver: read first block failed.\n");
		return;
		}
	Oscl::Error::Info::hexDump(mem,512);
#endif
	}

void	Driver::print2() noexcept{
#ifdef FAT16DEBUGSWITCH 
	char	buffer[128];
	Oscl::FS::FAT::DirEntry*	entry;
	for(unsigned i=0;(entry=_rootDir.entry(i));++i){
		sprintf(buffer,"dir entry %u ",i);
		Oscl::Error::Info::log(buffer);
		if(entry->noMoreDirEntriesFollow()){
			Oscl::Error::Info::log("is followed by no other entries.\n");
			break;
			}
		if(entry->isFree()){
			Oscl::Error::Info::log("is a free entry.\n");
			continue;
			}
		if(entry->longName()){
			Oscl::Error::Info::log("is a long name entry.\n");
			continue;
			}
		if(entry->directory()){
			Oscl::Error::Info::log("is a directory entry.");
			}
		else {
			Oscl::Error::Info::log("is a file entry.");
			}
		char	name[16];
		strncpy(name,(char *)entry->_shortName,11);
		name[11]	= '\0';
		strcat(name,"\n");
		Oscl::Error::Info::log(name);
		}
#endif
	}

Oscl::FS::FAT::DirEntry*	Driver::findDirEntry(const char* path) noexcept{
	unsigned long	sector=0;
	unsigned		sectorIndex=0;
	return findDirEntry(path,strlen(path),sector,sectorIndex);
	}

Oscl::FS::FAT::DirEntry*	Driver::findDirEntry(	const char*		path,
													unsigned		pathLen,
													unsigned long&	sector,
													unsigned&		sectorIndex
													) noexcept{
	if(path[0] == '/'){
		++path;
		--pathLen;
		}
	const char*	p	= index(path,pathLen,'/');
	unsigned	tokenLen	= p?(p-path):pathLen;
	Oscl::FS::FAT::DirEntry*	entry=0;
	for(unsigned i=0;(entry=_rootDir.entry(i,sector,sectorIndex));++i){
		if(entry->noMoreDirEntriesFollow()){
			return 0;
			}
		if(entry->isFree()){
			continue;
			}
		if(entry->longName()){
			continue;
			}
		if(entry->nameMatch(path,tokenLen)){
			break;
			}
		}
	while(true){
		pathLen	-= tokenLen;
		if(!p){
			return entry;
			}
		if(!entry->directory()){
			// Entry is not a directory
			return 0;
			}
		++p;	// skip the '/'
		--pathLen;
		if(!pathLen){
			// path ends with '\/'
			return entry;
			}
		path	= p;
		p	= index(path,pathLen,'/');
		tokenLen	= p?(p-path):pathLen;
		Directory	dir(	_cacheApi,
							_partition,
							entry->firstCluster()
							);
		for(unsigned i=0;(entry=dir.entry(i,sector,sectorIndex));++i){
			if(entry->noMoreDirEntriesFollow()){
				return 0;
				}
			if(entry->isFree()){
				continue;
				}
			if(entry->longName()){
				continue;
				}
			if(entry->nameMatch(path,tokenLen)){
				break;
				}
			}
		}
	return 0;
	}

