/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_fs_volume_fat16_driverh_
#define _oscl_fs_volume_fat16_driverh_
#include "oscl/fs/volume/api.h"
#include "oscl/block/rw/api.h"
#include "oscl/block/rw/itc/api.h"
#include "oscl/block/read/itc/respmem.h"
#include "oscl/fs/fat/fat.h"
#include "oscl/block/rw/cache/api.h"
#include "dir.h"

/** */
namespace Oscl {
/** */
namespace FS {
/** */
namespace Volume {
/** */
namespace FAT16 {

/** This class is responsible for mapping the Volume API
	onto a FAT16 block driver.
 */
class Driver :	public Oscl::FS::Volume::Api,
				private Oscl::Block::Read::ITC::Resp::Api<512>
	{
	private:
		/** */
		Oscl::Block::RW::Cache::Api&		_cacheApi;
		/** */
		const Oscl::ObjectID::RO::Api&		_location;
		/** */
		Oscl::FS::FAT::FAT16::Partition		_partition;
		/** */
		DirCluster							_rootDir;
		/** */
		Oscl::Queue<RoState>				_roStateList;
		/** */
		Oscl::Queue<RwState>				_rwStateList;

	public:
		/** */
		Driver(	Oscl::Block::RW::Cache::Api&	cacheApi,
				const Oscl::ObjectID::RO::Api&	location,
				uint32_t						rootDirSectors,
				uint32_t						firstDataSector,
				uint8_t						sectorsPerCluster,
				uint16_t						reservedSectorCount,
				uint16_t						bytesPerSector,
				uint32_t						fatSize,
				uint32_t						firstRootDirSecNum,
				uint32_t						totalSectors,
				uint16_t						rootEntryCount
				) noexcept;

	private: // Oscl::FS::Volume::Api
		/** */
		const Oscl::ObjectID::RO::Api&	location() const noexcept;
		/** */
		const Oscl::FS::File::RO::OC::Open::Status::Result*
				open(	RoStateMem&		stateMem,
						const char*		path,
						RoStateHandle&	handle
						) noexcept;
		/** */
		const Oscl::FS::File::RO::OC::Close::Status::Result*
				close(RoStateHandle&	handle) noexcept;
		/** */
		const Oscl::FS::File::RW::OC::Open::Status::Result*
				open(	RwStateMem&		stateMem,
						const char*		path,
						RwStateHandle&	handle
						) noexcept;
		/** */
		const Oscl::FS::File::RW::OC::Close::Status::Result*
				close(RwStateHandle&	handle) noexcept;
		/** */
		const Oscl::FS::File::RO::Access::Read::Status::Result*
				read(	RoStateHandle&	handle,
						void*			dest,
						unsigned long	maxSize,
						unsigned long	offset,
						unsigned long&	nRead
						) noexcept;
		/** */
		const Oscl::FS::File::RW::Access::Write::Status::Result*
				write(	RwStateHandle&	handle,
						const void*		src,
						unsigned long	length,
						unsigned long	offset,
						unsigned long&	nWritten
						) noexcept;
		/** */
		const Oscl::FS::File::RW::Access::Trunc::Status::Result*
				truncate(	RwStateHandle&	handle,
							unsigned long	size
							) noexcept;
		/** */
		const Oscl::FS::Dir::WO::Move::Status::Result*
				move(	const char*	srcPath,
						const char*	destPath
						) noexcept;
		/** */
		const Oscl::FS::Dir::WO::Create::Status::Result*
				createFile(const char* path) noexcept;
		/** */
		const Oscl::FS::Dir::WO::Create::Status::Result*
				createDir(const char* path) noexcept;
		/** */
		const Oscl::FS::Dir::WO::Remove::Status::Result*
				remove(const char* path) noexcept;

	private: // Oscl::Block::Read::ITC::Resp::Api<512>
		/** */
		void	response(Oscl::Block::Read::ITC::Resp::Api<512>::ReadResp& msg) noexcept;
		/** */
		void	response(Oscl::Block::Read::ITC::Resp::Api<512>::CancelResp& msg) noexcept;
	private:
		/** */
		void	print() noexcept;
		/** */
		void	print2() noexcept;
		/** */
		void	testFile(const char* path,unsigned long offset) noexcept;
		/** */
		Oscl::FS::FAT::DirEntry*	findDirEntry(const char* path) noexcept;
		/** */
		Oscl::FS::FAT::DirEntry*	findDirEntry(	const char*		path,
													unsigned		pathLen,
													unsigned long&	sector,
													unsigned&		sectorIndex
													) noexcept;
		/** This operation allocates n clusters, forms them as
			a chain and then returns the cluster number of the
			first cluster in the chain. Returns zero if unable
			to allocate desired number of clusters.
		 */
		unsigned	allocateNClusters(unsigned long n) noexcept;

		/** This operation frees the clusters in the cluster
			chain that starts with "firstCluster".
		 */
		void		freeClusterChain(unsigned firstCluster) noexcept;

		/** This operation appends the cluster (or cluster chain)
			to the end of the existing cluster chain of the specified
			file.
		 */
		const Oscl::FS::File::RW::Access::Write::Status::Result*
				appendClusterToFile(	RwStateHandle&	handle,
										unsigned long	cluster
										) noexcept;
		/** This operation takes a reference to a directory's cluster
			chain and deletes directory entries depth first until
			it reaches a leaf directory and deletes it and its
			content. This is NOT a recursive operation. If this
			operation returns true, then the operation should be
			invoked again (and repeatedly) until it returns false.
			When this operation returns false, the ioError variable
			must be checked to determin if the operation succeeded
			or an I/O error was encountered. If the operation
			succeeded, then the directory entry to which "cluster"
			belonged is empty and must be freed by the client.
		 */
		bool	rmdir(unsigned cluster,bool& ioError) noexcept;
	};

}
}
}
}

#endif
