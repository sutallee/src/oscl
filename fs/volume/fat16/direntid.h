/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/


/** The purpose of this ID is to enable a type of
	locking protection for open files. Before a
	file can be written or a directory modified,
	the volume must consult its "database" to
	see if any openned files would be modified.
	If so, then the operation must fail without
	modifying the opened environment. For directory
	operation, this means that the directory entry
	represented by this ID must NOT be changed
	in any way that would cause the "cluster"
	and/or "index" fields to be come invalid.
	E.g. The directory operation must not:
		o delete the directory entry
		o move the directory entry
		o remove any directories containing the
			directory entry.
	E.g. The file operation must not:
		o allow the same file to be openned again.
	Only operations holding the DirEntryID may
	modify the directory or file that the DirEntryID
	references.
		

	Hmmm. Should this be in a tree? The tree would not
	be the same containment tree as the directory structure.
	Yes, it could speed searches for particular clusters.
 */
struct DirEntryID : public Oscl::Tree::Node<DirEntryID,unsigned> {
	/** */
	void*			__qitemlink;
	/** This member identifies the cluster
		that contains the FAT16::DirEntry.
	 */
	unsigned		cluster;
	/** */
	unsigned		index;
	};


