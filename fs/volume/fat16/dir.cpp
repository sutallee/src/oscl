/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <string.h>
#include "dir.h"
#include "oscl/error/info.h"

using namespace Oscl::FS::Volume::FAT16;

DirSector::DirSector(	Oscl::Block::RW::Cache::Api&	cacheApi,
						unsigned long					sector,
						unsigned						nEntries
						) noexcept:
		_cacheApi(cacheApi),
		_sector(sector),
		_nEntries(nEntries),
		_ioError(false)
		{
	}

bool	DirSector::ioError() const noexcept{
	return _ioError;
	}

Oscl::FS::FAT::DirEntry*	DirSector::entry(unsigned index) noexcept{
	enum {sectorSize=512};
	if(index >= _nEntries){
		// Not that many entries in the cluster.
		return 0;
		}
	Oscl::FS::FAT::DirEntry*
	entries	= (Oscl::FS::FAT::DirEntry*)_cacheApi.fetch(_sector);
	if(!entries){
		_ioError	= true;
		return 0;
		}
	return &entries[index];
	}

void	DirSector::touch() noexcept{
	_cacheApi.touch(_sector);
	}

Oscl::FS::FAT::DirEntry*
DirSector::makeDir(	unsigned							index,
					const char*							name,
					unsigned							clusterChain,
					unsigned							containingDirCluster,
					Oscl::FS::FAT::FAT16::Partition&	partition
												) noexcept{
	Partition	part(_cacheApi,partition);
	if(part.formatDirectoryClusterChain(clusterChain)){
		_ioError	= part.ioError();
		return 0;
		}
	Oscl::FS::FAT::DirEntry*	ent	= entry(index);
	if(!ent){
		return 0;
		}
	const unsigned		year		= 0;
	const unsigned char	monthOfYear	= 0;
	const unsigned char	dayOfMonth	= 0;
	const unsigned char	hours		= 0;
	const unsigned char	minutes		= 0;
	const unsigned char	seconds		= 0;
	if(ent->name(name)){
		// Bad filename. This is a program
		// design error.
		return 0;
		}
	ent->_attributes			= 0x10;	// mark as directory
	ent->_ntReserved			= 0x00;
	ent->_creationTimeTenths	= 0x00;
	ent->_creationTime.time(hours,minutes,seconds);
	ent->_creationDate.date(year,monthOfYear,dayOfMonth);
	ent->_lastAccessDate.date(year,monthOfYear,dayOfMonth);
	ent->_writeTime.time(hours,minutes,seconds);
	ent->_writeDate.date(year,monthOfYear,dayOfMonth);
	ent->firstCluster(clusterChain);
	ent->fileSize(0);	// Always zero for a directory
	_cacheApi.touch(_sector);

	// Now that the directory entry has been created,
	// I need to create two new directory entries in
	// the new directory.

	Directory	dir(	_cacheApi,
						partition,
						clusterChain
						);
	unsigned long	dotSector;
	unsigned		dotSectorIndex;


	// Create the "." dot directory entry
	ent	= dir.entry(0,dotSector,dotSectorIndex);
	if(!ent){
		_ioError	= dir.ioError();
		return 0;
		}
	ent->_shortName[0]	= '.';
	for(unsigned i=1;i<11;++i){
		ent->_shortName[i]	= 0x20;
		}
	ent->_attributes			= 0x10;	// mark as directory
	ent->_ntReserved			= 0x00;
	ent->_creationTimeTenths	= 0x00;
	ent->_creationTime.time(hours,minutes,seconds);
	ent->_creationDate.date(year,monthOfYear,dayOfMonth);
	ent->_lastAccessDate.date(year,monthOfYear,dayOfMonth);
	ent->_writeTime.time(hours,minutes,seconds);
	ent->_writeDate.date(year,monthOfYear,dayOfMonth);
	ent->firstCluster(clusterChain);	// This is this directory
	ent->fileSize(0);	// Always zero for a directory
	_cacheApi.touch(dotSector);
	// Create the ".." dotdot directory entry
	ent	= dir.entry(1,dotSector,dotSectorIndex);
	if(!ent){
		_ioError	= dir.ioError();
		return 0;
		}
	ent->_shortName[0]	= '.';
	ent->_shortName[1]	= '.';
	for(unsigned i=2;i<11;++i){
		ent->_shortName[i]	= 0x20;
		}
	ent->_attributes			= 0x10;	// mark as directory
	ent->_ntReserved			= 0x00;
	ent->_creationTimeTenths	= 0x00;
	ent->_creationTime.time(hours,minutes,seconds);
	ent->_creationDate.date(year,monthOfYear,dayOfMonth);
	ent->_lastAccessDate.date(year,monthOfYear,dayOfMonth);
	ent->_writeTime.time(hours,minutes,seconds);
	ent->_writeDate.date(year,monthOfYear,dayOfMonth);
	ent->firstCluster(containingDirCluster);
	ent->fileSize(0);	// Always zero for a directory
	_cacheApi.touch(dotSector);
	ent	= entry(index);
	if(!ent){
		// Only an I/O Error could cause this!
		return 0;
		}
	return ent;
	}

Oscl::FS::FAT::DirEntry*
DirSector::makeFile(	unsigned							index,
						const char*							name,
						Oscl::FS::FAT::FAT16::Partition&	partition
						) noexcept{
	Oscl::FS::FAT::DirEntry*	ent	= entry(index);
	if(!ent){
		return 0;
		}
	const unsigned		year		= 0;
	const unsigned char	monthOfYear	= 0;
	const unsigned char	dayOfMonth	= 0;
	const unsigned char	hours		= 0;
	const unsigned char	minutes		= 0;
	const unsigned char	seconds		= 0;
	if(ent->name(name)){
		// Bad filename. This is a program
		// design error.
		return 0;
		}
	ent->_attributes			= 0x00;	// mark as regular file
	ent->_ntReserved			= 0x00;
	ent->_creationTimeTenths	= 0x00;
	ent->_creationTime.time(hours,minutes,seconds);
	ent->_creationDate.date(year,monthOfYear,dayOfMonth);
	ent->_lastAccessDate.date(year,monthOfYear,dayOfMonth);
	ent->_writeTime.time(hours,minutes,seconds);
	ent->_writeDate.date(year,monthOfYear,dayOfMonth);
	ent->firstCluster(0);
	ent->fileSize(0);
	_cacheApi.touch(_sector);

	return entry(index);
	}

DirCluster::DirCluster(	Oscl::Block::RW::Cache::Api&	cacheApi,
						unsigned long					rootSector,
						unsigned						nEntries
						) noexcept:
		_cacheApi(cacheApi),
		_rootSector(rootSector),
		_nEntries(nEntries),
		_ioError(false)
		{
	}

bool	DirCluster::ioError() const noexcept{
	return _ioError;
	}

Oscl::FS::FAT::DirEntry*	DirCluster::entry(unsigned index) noexcept{
	unsigned long	sector;
	unsigned		sectorIndex;
	return entry(index,sector,sectorIndex);
	}

Oscl::FS::FAT::DirEntry*	DirCluster::entry(	unsigned		index,
												unsigned long&	sector,
												unsigned&		sectorIndex
												) noexcept{
	enum {sectorSize=512};
	if(index >= _nEntries){
		// Not that many entries in the cluster.
		return 0;
		}
	unsigned long
	byteOffset	= index*sizeof(Oscl::FS::FAT::DirEntry);
	unsigned long
	sectorOffset	= byteOffset/sectorSize;
	sector	= _rootSector+sectorOffset;
	Oscl::FS::FAT::DirEntry*
	entries	= (Oscl::FS::FAT::DirEntry*)_cacheApi.fetch(sector);
	if(!entries){
		_ioError	= true;
		return 0;
		}
	unsigned long
	entriesPerSector	= sectorSize/sizeof(Oscl::FS::FAT::DirEntry);
	index	= index%entriesPerSector;
	sectorIndex	= index;
	return &entries[index];
	}

Oscl::FS::FAT::DirEntry*
DirCluster::newEntryCopy(	Oscl::FS::FAT::DirEntry&			entryCopy,
							Oscl::FS::FAT::FAT16::Partition&	partition,
							unsigned							rootCluster,
							unsigned long&						sector,
							unsigned&							sectorIndex
							) noexcept{
	enum {sectorSize=512};
	
	Oscl::FS::FAT::DirEntry*
	entry	= findFreeEntry(	sector,
								sectorIndex
								);
	if(!entry){
		return 0;
		}
	*entry	= entryCopy;
	_cacheApi.touch(sector);
	if(!entry->directory()){
		return entry;
		}

	// FIXME: update the dotdot and dot
	// directory entries in the first
	// cluster.
	unsigned	firstCluster	= entry->firstCluster();
	Directory	dir(	_cacheApi,
						partition,
						firstCluster
						);
	unsigned long	dotSector;
	unsigned		dotSectorIndex;

	// Update the "." dot directory entry
	entry	= dir.entry(0,dotSector,dotSectorIndex);
	if(!entry){
		_ioError	= dir.ioError();
		return 0;
		}
	entry->firstCluster(firstCluster);
	_cacheApi.touch(dotSector);
	// Update the ".." dot directory entry
	entry	= dir.entry(1,dotSector,dotSectorIndex);
	if(!entry){
		_ioError	= dir.ioError();
		return 0;
		}
	entry->firstCluster(rootCluster);
	_cacheApi.touch(dotSector);
	// Finally, since were going to return
	// a valid pointer to the directory entry
	// we re-obtain the pointer.
	DirSector	dirSector(	_cacheApi,
							sector,
							sectorSize/sizeof(Oscl::FS::FAT::DirEntry)
							);
	entry	= dirSector.entry(sectorIndex);
	if(!entry){
		_ioError	= dirSector.ioError();
		return 0;
		}
	return entry;
	}

Oscl::FS::FAT::DirEntry*	DirCluster::findFreeEntry(	unsigned long&	sector,
														unsigned&		sectorIndex
														) noexcept{
	Oscl::FS::FAT::DirEntry*	e;
	for(unsigned i=0;i<((unsigned)~0);++i){
		e	= entry(i,sector,sectorIndex);
		if(!e){
			return 0;
			}
		if(e->isFree()){
			return e;
			}
		}
	Oscl::Error::Info::log("Corrupt FAT16\n");
	return 0;
	}

Directory::Directory(	Oscl::Block::RW::Cache::Api&		cacheApi,
						Oscl::FS::FAT::FAT16::Partition&	partition,
						unsigned long						rootCluster
						) noexcept:
		_cacheApi(cacheApi),
		_partition(partition),
		_rootCluster(rootCluster),
		_ioError(false)
		{
	}

bool	Directory::ioError() const noexcept{
	return _ioError;
	}

Oscl::FS::FAT::DirEntry*
Directory::nameMatch(	const char*		name,
						unsigned		nameLen,
						unsigned long&	sector,
						unsigned&		sectorIndex
						) noexcept{
	Oscl::FS::FAT::DirEntry*	e;
	for(unsigned i=0;(e=entry(i,sector,sectorIndex));++i){
		if(e->noMoreDirEntriesFollow()){
			return 0;
			}
		if(e->isFree()){
			continue;
			}
		if(e->longName()){
			continue;
			}
		if(e->nameMatch(name,nameLen)){
			return e;
			}
		}
	return 0;
	}

Oscl::FS::FAT::DirEntry*	Directory::entry(unsigned index) noexcept{
	unsigned long	sector;
	unsigned		sectorIndex;
	return entry(index,sector,sectorIndex);
	}

Oscl::FS::FAT::DirEntry*	Directory::entry(	unsigned		index,
												unsigned long&	sector,
												unsigned&		sectorIndex
												) noexcept{
	enum {sectorSize=512};
	enum {entriesPerSector=sectorSize/sizeof(Oscl::FS::FAT::DirEntry)};

	const unsigned long
	entriesPerCluster	= entriesPerSector*_partition.sectorsPerCluster();

	// desired cluster
	// counting along the fat cluster chain, this is the
	// cluster that we need to get.
	const unsigned long
	clusterIndex	= index/entriesPerCluster;

	// This is the offset of the desired entry
	// within the cluster.
	const unsigned long
	clusterOffset	= index%entriesPerCluster;

	unsigned long	clusterSector;
	if(findTheFirstSectorofTheNthCluster(	_rootCluster,
											clusterSector,
											clusterIndex
											)
			){
		return 0;
		}

	DirCluster	dir(	_cacheApi,
						clusterSector,
						entriesPerCluster
						);

	return dir.entry(clusterOffset,sector,sectorIndex);
	}


Oscl::FS::FAT::DirEntry*	Directory::newFile(	const char*		name,
												unsigned long&	sector,
												unsigned&		sectorIndex
												) noexcept{
	enum {sectorSize=512};
	Oscl::FS::FAT::DirEntry*
	entry	= findFreeEntry(	sector,
								sectorIndex
								);
	if(!entry){
		return 0;
		}
	DirSector	ds(	_cacheApi,
					sector,
					sectorSize/sizeof(Oscl::FS::FAT::DirEntry)
					);
	entry	= ds.makeFile(	sectorIndex,
							name,
							_partition
							);
	return entry;
	}

Oscl::FS::FAT::DirEntry*	Directory::newEntryCopy(	Oscl::FS::FAT::DirEntry&	entryCopy,
														unsigned long&				sector,
														unsigned&					sectorIndex
														) noexcept{
	enum {sectorSize=512};
	
	Oscl::FS::FAT::DirEntry*
	entry	= findFreeEntry(	sector,
								sectorIndex
								);
	if(!entry){
		return 0;
		}
	*entry	= entryCopy;
	_cacheApi.touch(sector);
	if(!entry->directory()){
		return entry;
		}

	// FIXME: update the dotdot and dot
	// directory entries in the first
	// cluster.
	unsigned	firstCluster	= entry->firstCluster();
	Directory	dir(	_cacheApi,
						_partition,
						firstCluster
						);
	unsigned long	dotSector;
	unsigned		dotSectorIndex;

	// Update the "." dot directory entry
	entry	= dir.entry(0,dotSector,dotSectorIndex);
	if(!entry){
		_ioError	= dir.ioError();
		return 0;
		}
	entry->firstCluster(firstCluster);	// This directory
	_cacheApi.touch(dotSector);
	// Update the ".." dot directory entry
	entry	= dir.entry(1,dotSector,dotSectorIndex);
	if(!entry){
		_ioError	= dir.ioError();
		return 0;
		}
	entry->firstCluster(_rootCluster);	// This directory
	_cacheApi.touch(dotSector);
	// Finally, since were going to return
	// a valid pointer to the directory entry
	// we re-obtain the pointer.
	DirSector	dirSector(	_cacheApi,
							sector,
							sectorSize/sizeof(Oscl::FS::FAT::DirEntry)
							);
	entry	= dirSector.entry(sectorIndex);
	if(!entry){
		_ioError	= dirSector.ioError();
		return 0;
		}
	return entry;
	}

Oscl::FS::FAT::DirEntry*	Directory::newDirectory(	const char*		name,
														unsigned		clusterChain,
														unsigned long&	sector,
														unsigned&		sectorIndex
														) noexcept{
	enum {sectorSize=512};
	Oscl::FS::FAT::DirEntry*
	entry	= findFreeEntry(	sector,
								sectorIndex
								);
	if(!entry){
		return 0;
		}
	DirSector	ds(	_cacheApi,
					sector,
					sectorSize/sizeof(Oscl::FS::FAT::DirEntry)
					);
	entry	= ds.makeDir(	sectorIndex,
							name,
							clusterChain,
							_rootCluster,
							_partition
							);
	return entry;
	}

bool	Directory::appendClusterChain(unsigned chain) noexcept{
	// First format the cluster chain
	Partition	part(_cacheApi,_partition);
	if(part.formatDirectoryClusterChain(chain)){
		return true;
		}

	// Now append the new cluster to the existing
	// cluster chain. Follow the cluster chain
	// until the end-of-chain mark is encountered
	// and then insert the new cluster.
	unsigned	cluster	= _rootCluster;
	for(unsigned i=0;i<((unsigned)~0);++i){
		uint32_t	fatSector;
		uint32_t	fatEntryOffset;
		_partition.fatEntry(	0,	// fatN
								cluster,	// cluster
								fatSector,
								fatEntryOffset
								);
		uint8_t*
		fat	= (uint8_t*)_cacheApi.fetch(fatSector);
		if(!fat){
			_ioError	= true;
			return true;
			}
		Oscl::FS::FAT::FAT16::Entry*
		fatEntry	= (Oscl::FS::FAT::FAT16::Entry*)&fat[fatEntryOffset];
		if(fatEntry->isEOC()){
			fatEntry->value(cluster);
			_cacheApi.touch(fatSector);
			return false;
			}
		cluster	= fatEntry->value();
		}
	// If I get to here, the FAT is corrupt.
	return false;
	}

// return true if clusterN does not exist in this chain.
bool	Directory::findTheFirstSectorofTheNthCluster(	unsigned long	firstCluster,
														unsigned long&	firstSector,
														unsigned 		clusterN
														) noexcept{
	unsigned long	cluster	= firstCluster;
	for(unsigned i=0;i<clusterN;++i){
		uint32_t	fatSector;
		uint32_t	fatEntryOffset;
		_partition.fatEntry(	0,	// fatN
								cluster,	// cluster
								fatSector,
								fatEntryOffset
								);
		uint8_t*
		fat	= (uint8_t*)_cacheApi.fetch(fatSector);
		if(!fat){
			_ioError	= true;
			return true;
			}
		Oscl::FS::FAT::FAT16::Entry*
		fatEntry	= (Oscl::FS::FAT::FAT16::Entry*)&fat[fatEntryOffset];
		if(fatEntry->isEOC()){
			return true;
			}
		cluster	= fatEntry->value();
		}

	firstSector	= _partition.firstSectorOfCluster(cluster);
	return false;
	}


Oscl::FS::FAT::DirEntry*	Directory::findFreeEntry(	unsigned long&	sector,
														unsigned&		sectorIndex
														) noexcept{
	enum {sectorSize=512};
	enum {entriesPerSector=sectorSize/sizeof(Oscl::FS::FAT::DirEntry)};

	const unsigned long
	entriesPerCluster	= entriesPerSector*_partition.sectorsPerCluster();

	// This test catches _rootClusters that are empty.
	if(_rootCluster == Oscl::FS::FAT::FAT16::Entry::EOC){
		// Bail if we've reached the end of the chain.
		return 0;
		}

	// This outer loop searches each cluster in the directory
	// cluster chain until it finds a free directory entry or
	// reaches the end of the chain.
	unsigned long	cluster	= _rootCluster;
	for(unsigned i=0;i<((unsigned)~0);++i){
		// Search the current cluster for a free
		// directory entry.
		DirCluster	dirCluster(	_cacheApi,
								_partition.firstSectorOfCluster(cluster),
								entriesPerCluster
								);
		for(unsigned j=0;j<((unsigned)~0);++i){
			Oscl::FS::FAT::DirEntry*
			entry	= dirCluster.entry(i,sector,sectorIndex);
			if(entry){
				if(entry->isFree()){
					// We found a free one!
					return entry;
					}
				}
			else {
				// We either have no more entries in the
				// cluster or we have encountered an I/O
				// error.
				if(dirCluster.ioError()){
					_ioError	= true;
					return 0;
					}
				// This cluster has no free entries.
				break;
				}
			}
		// At this point we have not found a free directory
		// entry  in the current cluster. Now, we use the
		// FAT to follow the cluster chain to the next
		// cluster (if any).
		uint32_t	fatSector;
		uint32_t	fatEntryOffset;
		_partition.fatEntry(	0,	// fatN
								cluster,	// cluster
								fatSector,
								fatEntryOffset
								);
		uint8_t*
		fat	= (uint8_t*)_cacheApi.fetch(fatSector);
		if(!fat){
			// Uh-Oh. I/O error.
			_ioError	= true;
			return 0;
			}
		Oscl::FS::FAT::FAT16::Entry*
		fatEntry	= (Oscl::FS::FAT::FAT16::Entry*)&fat[fatEntryOffset];
		if(fatEntry->isEOC()){
			// No more clusters,
			// we're out-of-here.
			return 0;
			}
		cluster	= fatEntry->value();
		}
	// The only way we get here is if we have a
	// corrupt FAT.
	Oscl::Error::Info::log("corrupt FAT16\n");
	return 0;
	}

Partition::Partition(	Oscl::Block::RW::Cache::Api&		cacheApi,
						Oscl::FS::FAT::FAT16::Partition&	partition
						) noexcept:
		_cacheApi(cacheApi),
		_partition(partition),
		_ioError(false)
		{
	}

bool	Partition::ioError() const noexcept{
	return _ioError;
	}

bool	Partition::formatDirectoryCluster(unsigned cluster) noexcept{
	enum {sectorSize=512};
	enum {entriesPerSector=sectorSize/sizeof(Oscl::FS::FAT::DirEntry)};

	const unsigned long
	entriesPerCluster	= entriesPerSector*_partition.sectorsPerCluster();

	DirCluster	dir(	_cacheApi,
						_partition.firstSectorOfCluster(cluster),
						entriesPerCluster
						);

	for(unsigned i=0;i<entriesPerCluster;++i){
		unsigned long	sector;
		unsigned		sectorIndex;
		Oscl::FS::FAT::DirEntry*
		entry	= dir.entry(i,sector,sectorIndex);
		if(!entry){
			_ioError	= true;
			return true;
			}
		entry->freeNoMoreDirEntriesFollow();
		_cacheApi.touch(sector);
		}
	return false;
	}

bool	Partition::formatDirectoryClusterChain(unsigned chain) noexcept{
	unsigned long	cluster	= chain;
	Partition		partition(_cacheApi,_partition);
	for(unsigned i=0;i<((unsigned)~0);++i){
		if(cluster == Oscl::FS::FAT::FAT16::Entry::EOC){
			break;
			}
		if(partition.formatDirectoryCluster(cluster)){
			// Format failure
			return true;
			}
		uint32_t	fatSector;
		uint32_t	fatEntryOffset;
		_partition.fatEntry(	0,	// fatN
								cluster,	// cluster
								fatSector,
								fatEntryOffset
								);
		uint8_t*
		fat	= (uint8_t*)_cacheApi.fetch(fatSector);
		if(!fat){
			_ioError	= true;
			return true;
			}
		Oscl::FS::FAT::FAT16::Entry*
		fatEntry	= (Oscl::FS::FAT::FAT16::Entry*)&fat[fatEntryOffset];
		if(fatEntry->isEOC()){
			break;
			}
		cluster	= fatEntry->value();
		}
	return false;
	}

