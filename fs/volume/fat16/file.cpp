/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <string.h>
#include "file.h"
#include "oscl/error/info.h"

using namespace Oscl::FS::Volume::FAT16;

FileCluster::FileCluster(	Oscl::Block::RW::Cache::Api&	cacheApi,
							unsigned long					rootSector,
							unsigned						clusterLen,
							unsigned						sectorSize
							) noexcept:
		_cacheApi(cacheApi),
		_rootSector(rootSector),
		_clusterLen(clusterLen),
		_sectorSize(sectorSize),
		_ioError(false)
		{
	}

bool	FileCluster::ioError() const noexcept{
	return _ioError;
	}

unsigned	FileCluster::read(	void*		buffer,
								unsigned	maxRead,
								unsigned	offset
								) noexcept{
	if(offset >= _clusterLen){
		return 0;
		}
	unsigned maxToRead	= _clusterLen-offset;
	if(maxRead > maxToRead){
		maxRead	= maxToRead;
		}
	unsigned long
	sectorOffset	= offset/_sectorSize;
	unsigned long
	sector	= _rootSector+sectorOffset;
	offset	= offset%_sectorSize;
	unsigned	remaining	= maxRead;
	unsigned char*	b	= (unsigned char*)buffer;
	const unsigned nSectorsToRead	= ((offset+maxRead)+(_sectorSize-1))/_sectorSize;
	for(unsigned i=0;i<nSectorsToRead;++i){
		unsigned long validSize = _clusterLen - ((sectorOffset+i)*_sectorSize);
		unsigned	nRead	= readSector(	b,
											remaining,
											offset,
											sector+i,
											validSize
											);
		if(!nRead){
			return maxRead-remaining;
			}
		offset		= 0;
		remaining	-= nRead;
		b			+= nRead;
		}
	return maxRead;
	}

void	FileCluster::truncate(unsigned long size) noexcept{
	for(;;);
	}

unsigned long	FileCluster::write(	const void*		buffer,
									unsigned		length,
									unsigned		offset
									) noexcept{
	if(offset >= _clusterLen){
		return 0;
		}
	// This should NOT be necessary.
	unsigned maxToWrite	= _clusterLen-offset;
	if(length > maxToWrite){
		length	= maxToWrite;
		}

	unsigned long
	sectorOffset	= offset/_sectorSize;

	unsigned long
	sector	= _rootSector+sectorOffset;

	offset	= offset%_sectorSize;

	unsigned char*	b	= (unsigned char*)buffer;

	const unsigned nSectorsToWrite	= ((offset+length)+(_sectorSize-1))/_sectorSize;

	unsigned	nWritten	= 0;
	for(unsigned i=0;i<nSectorsToWrite;++i){
		unsigned	nMax	= _sectorSize-offset;
		unsigned	nToWrite	= (length > nMax)?nMax:length;
		unsigned	n	= writeSector(	b,
										nToWrite,
										offset,
										sector+i,
										_sectorSize
										);
		if(n != nToWrite){
			return nWritten+n;
			}
		offset		= 0;
		length		-= n;
		b			+= n;
		nWritten	+= n;
		}
	return nWritten;
	}

unsigned	FileCluster::writeSector(	const void*		buffer,
										unsigned		length,
										unsigned		offset,
										unsigned long	sector,
										unsigned		sectorLen
										) noexcept{
	if((offset + length) > sectorLen) return 0;
	unsigned char*
	p	= (unsigned char*)_cacheApi.fetch(sector);
	if(!p){
		_ioError	= true;
		return 0;
		}
	memcpy(&p[offset],buffer,length);
	_cacheApi.touch(sector);
	return length;
	}

unsigned	FileCluster::readSector(	void*			buffer,
										unsigned		maxRead,
										unsigned		offset,
										unsigned long	sector,
										unsigned		sectorLen
										) noexcept{
	if(offset >= sectorLen) return 0;
	const unsigned	maxReadable	= sectorLen-offset;
	if(maxRead > maxReadable){
		maxRead	= maxReadable;
		}
	unsigned char*
	p	= (unsigned char*)_cacheApi.fetch(sector);
	if(!p){
		_ioError	= true;
		return 0;
		}
	memcpy(buffer,&p[offset],maxRead);
	return maxRead;
	}

File::File(	Oscl::Block::RW::Cache::Api&					cacheApi,
						Oscl::FS::FAT::FAT16::Partition&	partition,
						unsigned long						rootCluster,
						unsigned long						fileSize
						) noexcept:
		_cacheApi(cacheApi),
		_partition(partition),
		_rootCluster(rootCluster),
		_fileSize(fileSize),
		_ioError(false)
		{
	}

bool	File::ioError() const noexcept{
	return _ioError;
	}

unsigned long	File::read(	void*			buffer,
							unsigned long	maxRead,
							unsigned long	offset
							) noexcept{
	enum {sectorSize=512};
	enum {entriesPerSector=sectorSize};
	if(offset >= _fileSize) return 0;
	if(maxRead > _fileSize){
		maxRead	= _fileSize;
		}

	const unsigned long
	entriesPerCluster	= sectorSize*_partition.sectorsPerCluster();

	// desired cluster
	// counting along the fat cluster chain, this is the
	// cluster that we need to get.
	const unsigned long
	clusterIndex	= offset/entriesPerCluster;

	// This is the offset of the desired entry
	// within the cluster.
	unsigned long
	clusterOffset	= offset%entriesPerCluster;

	// Number of bytes from the beginning of the
	// cluster[clusterIndex] to the end of the
	// file.
	unsigned long
	clusterBytes	= _fileSize - (clusterIndex*entriesPerCluster);

	const unsigned long
	nClusters	= ((maxRead + clusterOffset) + (entriesPerCluster-1))/entriesPerCluster;

	unsigned long
	remaining	= maxRead;
	uint8_t*	b	= (uint8_t*)buffer;
	for(unsigned i=0;i<nClusters;++i){
		unsigned long	clusterSector;
		if(findTheFirstSectorofTheNthCluster(	_rootCluster,
												clusterSector,
												clusterIndex+i
												)
				){
			return 0;
			}
		unsigned long
		clusterLen	= clusterBytes;
		if(clusterLen > entriesPerCluster){
			clusterLen	= entriesPerCluster;
			}
		FileCluster	file(	_cacheApi,
							clusterSector,
							clusterLen,
							sectorSize
							);

		unsigned long
		nRead	= file.read(	b,
								remaining,
								clusterOffset
								);
		if(!nRead){
			return maxRead-remaining;
			}
		clusterOffset	= 0;
		clusterBytes	-= entriesPerCluster;
		remaining		-= nRead;
		b				+= nRead;
		}
	return maxRead-remaining;
	}

unsigned long	File::write(	const void*		buffer,
								unsigned long	length,
								unsigned long	offset
								) noexcept{
	enum {sectorSize=512};
	enum {entriesPerSector=sectorSize};
	if(length+offset > _fileSize) return 0;
	unsigned long	nWritten	= 0;

	// Number of bytes in a cluster
	const unsigned long
	bytesPerCluster	= sectorSize*_partition.sectorsPerCluster();

	// Offset into the first sector to
	// be written.
	unsigned long	firstClusterOffset	= offset%bytesPerCluster;

	// Number of clusters in file
	const unsigned long
	nClustersInFile	= (_fileSize + (bytesPerCluster-1))/bytesPerCluster;

	// First cluster to which we will append data.
	const unsigned firstWritableCluster		= offset/bytesPerCluster;

	const unsigned char*	b	= (const unsigned char*)buffer;

	unsigned long	cluster	= _rootCluster;

	unsigned long	bytesRemaining	= _fileSize;

	for(unsigned i=0;i<nClustersInFile;++i,bytesRemaining-=bytesPerCluster){
		uint32_t	fatSector;
		uint32_t	fatEntryOffset;
		_partition.fatEntry(	0,	// fatN
								cluster,	// cluster
								fatSector,
								fatEntryOffset
								);
		uint8_t*
		fat	= (uint8_t*)_cacheApi.fetch(fatSector);
		if(!fat){
			_ioError	= true;
			// FIXME: how do I deal with this error?
			Oscl::Error::Info::log("Can't fetch a FAT sector!\n");
			return nWritten;
			}
		Oscl::FS::FAT::FAT16::Entry*
		fatEntry	= (Oscl::FS::FAT::FAT16::Entry*)&fat[fatEntryOffset];
		unsigned	currentCluster	= cluster;
		cluster	= fatEntry->value();
		if(i == firstWritableCluster) {
			unsigned	clusterLen	= (bytesRemaining > bytesPerCluster)?bytesPerCluster:bytesRemaining;
			// This is the first cluster to which we will append data.
			FileCluster	file(	_cacheApi,
								_partition.firstSectorOfCluster(currentCluster),
								clusterLen,
								sectorSize
								);
			unsigned long	nToWrite	= bytesPerCluster-firstClusterOffset;
			nToWrite	= (length > nToWrite)?nToWrite:length;
			unsigned long
			n	= file.write(	b,
								nToWrite,
								firstClusterOffset
								);
			if(n != nToWrite){
				// FIXME: This is an error
				return nWritten + n;
				}
			nWritten	+= n;
			length		-= n;
			b			+= n;
			}
		else if(i > firstWritableCluster){
			unsigned	clusterLen	= (bytesRemaining > bytesPerCluster)?bytesPerCluster:bytesRemaining;
			// This appends the remaining data
			// to the last clusters.
			FileCluster	file(	_cacheApi,
								_partition.firstSectorOfCluster(currentCluster),
								clusterLen,
								sectorSize
								);
			unsigned long	nToWrite	= (length > bytesPerCluster)?bytesPerCluster:length;
			unsigned long
			n	= file.write(	b,
								nToWrite,
								0
								);
			if(n != nToWrite){
				// FIXME: This is an error
				return nWritten + n;
				}
			nWritten	+= n;
			length		-= n;
			b			+= n;
			}
		else continue;
		}
	return nWritten;
	}

void	File::truncate(unsigned long size) noexcept{
	enum {sectorSize=512};
	enum {entriesPerSector=sectorSize};
	if(size >= _fileSize) return;

	// Two things need to happen.
	//	1. The _fileSize in the directory entry for this
	//		file needs to be changed to be equal to
	//		offset.
	//	2. The clusters that are beyond the cluster that
	//		contains the "offset" byte need to be
	//		marked as unallocated.
	// Note: Since this class deals exclusively with
	// the data portion of the file and NOT the directory
	// entry, we will assume that the directory entry
	// will be modified by the client.
	//
	// In the case where a file is completely truncated
	// to a size of zero, the directory entry must be
	// modified such

	// Number of bytes in a cluster
	const unsigned long
	entriesPerCluster	= sectorSize*_partition.sectorsPerCluster();

	// Number of clusters in file
	const unsigned long
	nClustersInFile	= (_fileSize + (entriesPerCluster-1))/entriesPerCluster;

	// Number of clusters to be in file
	const unsigned long
	nClusters		= (size + (entriesPerCluster-1))/entriesPerCluster;

	// 
	if(nClusters >= nClustersInFile){
		// No clusters to delete
		return;
		}

	// The index of the last cluster that we will keep
	// Note that for nClusters == 0, this will roll over
	// to 0xFFFF. That means that there cannot be more than
	// 0xFFFE clusters in a file. Since clusters zero and
	// one are always reserved, this won't be a problem.
	const unsigned
	lastCluster		= nClusters-1;

	unsigned long	cluster	= _rootCluster;
	for(unsigned i=0;i<nClustersInFile;++i){
		uint32_t	fatSector;
		uint32_t	fatEntryOffset;
		_partition.fatEntry(	0,	// fatN
								cluster,	// cluster
								fatSector,
								fatEntryOffset
								);
		uint8_t*
		fat	= (uint8_t*)_cacheApi.fetch(fatSector);
		if(!fat){
			_ioError	= true;
			// FIXME: how do I deal with this error?
			Oscl::Error::Info::log("Can't fetch a FAT sector!\n");
			return;
			}
		Oscl::FS::FAT::FAT16::Entry*
		fatEntry	= (Oscl::FS::FAT::FAT16::Entry*)&fat[fatEntryOffset];
		cluster	= fatEntry->value();
		if(i >= nClusters) {
			// This is is one of the clusters that we are
			// deleting.
			// In this case, we need to read and save the 
			// cluster number of the NEXT cluster in the
			// chain (possibly EOC), and then mark this
			// cluster FAT entry as "free" (zero).
			fatEntry->markAsFree();
			}
		else if(i == lastCluster){
			// This is the last cluster that we will keep
			// In this case we only put an EOC in the FAT
			// for this cluster.
			fatEntry->markAsEOC();
			}
		else continue;
		// If I get to here, then I have modified the FAT
		// and I need to mark the FAT sector as dirty.
		_cacheApi.touch(fatSector);
		}
	}

// return true if clusterN does not exist in this chain.
bool	File::findTheFirstSectorofTheNthCluster(	unsigned long	firstCluster,
													unsigned long&	firstSector,
													unsigned 		clusterN
													) noexcept{
	unsigned long	cluster	= firstCluster;
	for(unsigned i=0;i<clusterN;++i){
		uint32_t	fatSector;
		uint32_t	fatEntryOffset;
		_partition.fatEntry(	0,	// fatN
								cluster,	// cluster
								fatSector,
								fatEntryOffset
								);
		uint8_t*
		fat	= (uint8_t*)_cacheApi.fetch(fatSector);
		if(!fat){
			_ioError	= true;
			return true;
			}
		Oscl::FS::FAT::FAT16::Entry*
		fatEntry	= (Oscl::FS::FAT::FAT16::Entry*)&fat[fatEntryOffset];
		if(fatEntry->isEOC()){
			return true;
			}
		cluster	= fatEntry->value();
		}

	firstSector	= _partition.firstSectorOfCluster(cluster);
	return false;
	}

