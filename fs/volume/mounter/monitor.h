/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_fs_volume_mounter_monitorh_
#define _oscl_fs_volume_mounter_monitorh_
#include "match.h"
#include "oscl/block/rw/dyn/service.h"
#include "oscl/mt/itc/dyn/monitor/unit/monitor/monitor.h"
#include "oscl/mt/itc/srv/crespmem.h"
#include "oscl/block/read/itc/respmem.h"
#include "oscl/ms/fat/partition.h"

#include "oscl/mt/itc/dyn/core/mem.h"
#include "oscl/fs/volume/fat16/driver.h"
#include "context.h"

#include "oscl/block/rw/cache/basic/part.h"

/** */
namespace Oscl {
/** */
namespace FS {
/** */
namespace Volume {
/** */
namespace Mounter {

/** This service runs in the File System thread and listens
	for a formated volume to become available. If the filesystem
	type and the location are a match, then the mounter creates
	a FileSystem Volume and mounts/registers it with the
	FileSystem. When the mounted volume becomes unavailable,
	the volume is "umount"ed from the FileSystem.
 */
class Monitor :
		public	Oscl::Mt::Itc::
				Dyn::Unit::Monitor<Oscl::Block::RW::Dyn::Service>
		{
	public:
		/** For use by the context.
		 */
		void*		__qitemlink;
	public:
		/** Number of entries in the block cache. */
		enum{nCacheEntries=4};

		/** DMA memory required by the monitor and
			by the volume when it is mounted. This memory
			*must* be allocated from a DMA capable address space.
		 */
		union DmaMem {
			/** */
			void*								__qitemlink;
			/** This memory is used to hold a 512 block for examining
				the partition table of the block device. It is only
				needed when bringing the device on-line as it is
				dynamically discovered.
			 */
			Oscl::Memory::
			AlignedBlock<512>						_blockMem[1];
			/** */
			Oscl::Block::RW::Cache::Basic::Part::DmaMem	_cache[nCacheEntries];
			};

		/** */
		struct Fat16VolumeMem {
			/** */
			Oscl::Block::RW::Cache::Basic::Part::VarMem	varMem[nCacheEntries];
			/** */
			Oscl::Memory::
			AlignedBlock<	sizeof(Oscl::Block::RW::Cache::Basic::Part)
							>	cache;
			/** */
			Oscl::Memory::
			AlignedBlock<	sizeof(Oscl::FS::Volume::FAT16::Driver)
							>	driver;
			};
		/** This type is for allocating a memory block large
			enough to hold any kind of volume driver.
		 */
		union VolumeMem {
			/** */
			Fat16VolumeMem	fat16;
			};
	private:
		/** */
		VolumeMem								_volumeMem;
		/** */
		Match									_match;
		/** */
		Oscl::Mt::Itc::PostMsgApi&				_myPapi;
		/** */
		Oscl::FS::Volume::Mounter::ContextApi&	_context;
		/** */
		DmaMem&									_dmaMem;
		/** */
		Oscl::Block::RW::Cache::Basic::Part*	_cache;
		/** */
		Oscl::FS::Volume::Api*					_volume;
		
	public:
		/** */
		Monitor(	Oscl::Mt::Itc::Dyn::Adv::Cli::
					SyncApi<	Oscl::Block::RW::
								Dyn::Service
								>&							advFind,
					Oscl::Mt::Itc::Dyn::Adv::Cli::
					Req::Api<	Oscl::Block::RW::
								Dyn::Service
								>::SAP&						advSAP,
					Oscl::Mt::Itc::PostMsgApi&				myPapi,
					Oscl::FS::Volume::Mounter::ContextApi&	context,
					DmaMem&									dmaMem
					) noexcept;
	private:
		/** */
		void	releaseResources() noexcept;

	private:
		/** */
		void	activate() noexcept;
		/** */
		void	deactivate() noexcept;

	private: // helpers
		/** */
		const void*	readFirstBlock() noexcept;
	};

}
}
}
}

#endif

