/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "monitor.h"
#include "oscl/error/info.h"
#include "oscl/error/fatal.h"
#include "oscl/oid/fixed.h"

using namespace Oscl::FS::Volume::Mounter;


Monitor::Monitor(	Oscl::Mt::Itc::Dyn::Adv::Cli::
					SyncApi<	Oscl::Block::RW::
								Dyn::Service
								>&							advFind,
					Oscl::Mt::Itc::Dyn::Adv::Cli::
					Req::Api<	Oscl::Block::RW::
								Dyn::Service
								>::SAP&						advSAP,
					Oscl::Mt::Itc::PostMsgApi&				myPapi,
					Oscl::FS::Volume::Mounter::ContextApi&	context,
					DmaMem&									dmaMem
					) noexcept:
		Oscl::Mt::Itc::Dyn::Unit::
		Monitor<Oscl::Block::RW::Dyn::Service>(	advSAP,
												advFind,
												myPapi,
												_match
												),
		_match(),
		_myPapi(myPapi),
		_context(context),
		_dmaMem(dmaMem)
		{
	}

void	Monitor::activate() noexcept{
	const void*
	firstBlock	= readFirstBlock();
	if(!firstBlock){
		// Message is issued by readFirstBlock()
		return;
		}
	Oscl::Block::RW::Dyn::Service&	service	= _handle->getDynSrv();
	switch(service.type()){
		case Oscl::Block::RW::Dyn::Service::fat:
		case Oscl::Block::RW::Dyn::Service::lbaFat:
			break;
		case Oscl::Block::RW::Dyn::Service::raw:
		case Oscl::Block::RW::Dyn::Service::extended:
		case Oscl::Block::RW::Dyn::Service::lbaExtended:
			return;
		}

	const Oscl::FS::FAT::PartHeader*
	header	= (const Oscl::FS::FAT::PartHeader*)firstBlock;

	if(!header->_bpb.isFat16()){
		Oscl::Error::Info::log("Volume Mounter: Not FAT16\n");
		return;
		}
	_cache	= new(&_volumeMem.fat16.cache)
				Oscl::Block::RW::Cache::Basic::
				Part(	_handle->getDynSrv().getSyncApi(),
						_dmaMem._cache,
						_volumeMem.fat16.varMem,
						nCacheEntries
						);
	_volume	= new(&_volumeMem.fat16.driver)
				Oscl::FS::Volume::FAT16::
				Driver(	*_cache,
						_handle->getDynSrv()._location,
						header->_bpb.rootDirSectors(),
						header->_bpb.firstDataSector(),
						header->_bpb._sectorsPerCluster,
						header->_bpb.reservedSectorCount(),
						header->_bpb.bytesPerSector(),
						header->_bpb.fatSize(),
						header->_bpb.firstRootDirSecNum(),
						header->_bpb.totalSectors(),
						header->_bpb.rootEntryCount()
						);
	_context.mount(*_volume);
	}

void	Monitor::releaseResources() noexcept{
	}

void	Monitor::deactivate() noexcept{
	_context.umount(*_volume);
	deactivateDone();
	}

const void*	Monitor::readFirstBlock() noexcept{
	Oscl::Block::RW::Dyn::Service&	dyndev	= _handle->getDynSrv();
	const Oscl::Block::Read::Status::Result*
	result	= dyndev.getSyncApi().readApi().read(	0,
													1,
													_dmaMem._blockMem
													);
	if(!result){
		return _dmaMem._blockMem;
		}
	return (const void*)0;	// FIXME
	}

