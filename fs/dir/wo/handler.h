/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_fs_dir_wo_handlerh_
#define _oscl_fs_dir_wo_handlerh_

/** */
namespace Oscl {
/** */
namespace FS {
/** */
namespace Dir {
/** */
namespace WO {

/** */
namespace Move {
/** */
namespace Status {
/** */
class Handler{
	public:
		/** Make GCC happy */
		virtual ~Handler() {}
		/** */
		virtual void	noSuchVolume() noexcept=0;
		/** */
		virtual void	noSuchSrcPath() noexcept=0;
		/** */
		virtual void	invalidDestPath() noexcept=0;
		/** */
		virtual void	destDirectoryFull() noexcept=0;
		/** */
		virtual void	srcPathElementBusy() noexcept=0;
		/** */
		virtual void	outOfSpace() noexcept=0;
		/** */
		virtual void	transportError() noexcept=0;
	};
}
}

/** */
namespace Copy {
/** */
namespace Status {
/** */
class Handler{
	public:
		/** Make GCC happy */
		virtual ~Handler() {}
		/** */
		virtual void	noSuchSrcVolume() noexcept=0;
		/** */
		virtual void	noSuchDestVolume() noexcept=0;
		/** */
		virtual void	noSuchSrcPath() noexcept=0;
		/** */
		virtual void	invalidDestPath() noexcept=0;
		/** */
		virtual void	destDirectoryFull() noexcept=0;
		/** */
		virtual void	destOutOfSpace() noexcept=0;
		/** */
		virtual void	srcTransportError() noexcept=0;
		/** */
		virtual void	destTransportError() noexcept=0;
	};
}
}

/** */
namespace Create {
/** */
namespace Status {
/** */
class Handler{
	public:
		/** Make GCC happy */
		virtual ~Handler() {}
		/** */
		virtual void	noSuchVolume() noexcept=0;
		/** Path does not specify a directory.
			One or more elements of the directory
			portion of the path is not a directory.
		 */
		virtual void	invalidPath() noexcept=0;
		/** Path already exists of the same type.
			E.g. the createDir() operation is invoked
			and a directory of the same name already
			exists. No change is made to the filesystem.
		 */
		virtual void	pathAlreadyExists() noexcept=0;
		/** Path already exists and is not of the
			same type. E.g. the createFile() operation
			is specified, the same name already exists,
			but is a directory instead of a file.
		 */
		virtual void	otherTypePathExists() noexcept=0;
		/** */
		virtual void	directoryFull() noexcept=0;
		/** */
		virtual void	outOfSpace() noexcept=0;
		/** */
		virtual void	transportError() noexcept=0;
	};
}
}

/** */
namespace Remove {
/** */
namespace Status {
/** */
class Handler{
	public:
		/** Make GCC happy */
		virtual ~Handler() {}
		/** */
		virtual void	noSuchVolume() noexcept=0;
		/** */
		virtual void	noSuchPath() noexcept=0;
		/** */
		virtual void	transportError() noexcept=0;
	};
}
}


}
}
}
}

#endif
