/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_fs_dir_wo_statush_
#define _oscl_fs_dir_wo_statush_

/** */
namespace Oscl {
/** */
namespace FS {
/** */
namespace Dir {
/** */
namespace WO {

/** */
namespace Move {
/** */
namespace Status {
/** */
class Result;
/** */
const Result&	getNoSuchVolume() noexcept;
/** */
const Result&	getNoSuchSrcPath() noexcept;
/** */
const Result&	getInvalidDestPath() noexcept;
/** */
const Result&	getDestDirectoryFull() noexcept;
/** */
const Result&	getSrcPathElementBusy() noexcept;
/** */
const Result&	getOutOfSpace() noexcept;
/** */
const Result&	getTransportError() noexcept;

}
}

/** */
namespace Copy {
/** */
namespace Status {
/** */
class Result;
/** */
const Result&	getNoSuchSrcVolume() noexcept;
/** */
const Result&	getNoSuchDestVolume() noexcept;
/** */
const Result&	getNoSuchSrcPath() noexcept;
/** */
const Result&	getInvalidDestPath() noexcept;
/** */
const Result&	getDestDirectoryFull() noexcept;
/** */
const Result&	getDestOutOfSpace() noexcept;
/** */
const Result&	getSrcTransportError() noexcept;
/** */
const Result&	getDestTransportError() noexcept;
}
}

/** */
namespace Create {
/** */
namespace Status {
/** */
class Result;
/** */
const Result&	getNoSuchVolume() noexcept;
/** */
const Result&	getInvalidPath() noexcept;
/** */
const Result&	getPathAlreadyExists() noexcept;
/** */
const Result&	getOtherTypePathExists() noexcept;
/** */
const Result&	getDirectoryFull() noexcept;
/** */
const Result&	getOutOfSpace() noexcept;
/** */
const Result&	getTransportError() noexcept;
}
}

/** */
namespace Remove {
/** */
namespace Status {
/** */
class Result;
/** */
const Result&	getNoSuchVolume() noexcept;
/** */
const Result&	getNoSuchPath() noexcept;
/** */
const Result&	getTransportError() noexcept;
}
}


}
}
}
}

#endif
