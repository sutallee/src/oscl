/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "reqapi.h"

using namespace Oscl::FS::Dir::WO::ITC::Req;

Api::MovePayload::
MovePayload(	const Oscl::ObjectID::RO::Api&	volume,
				const char*						srcPath,
				const char*						destPath
				) noexcept:
		_volume(volume),
		_srcPath(srcPath),
		_destPath(destPath),
		_result(0)
		{
	}

Api::CopyPayload::
CopyPayload(	const Oscl::ObjectID::RO::Api&	srcVolume,
				const char*						srcPath,
				const Oscl::ObjectID::RO::Api&	destVolume,
				const char*						destPath
				) noexcept:
		_srcVolume(srcVolume),
		_srcPath(srcPath),
		_destVolume(destVolume),
		_destPath(destPath),
		_result(0)
		{
	}

Api::CreateFilePayload::
CreateFilePayload(	const Oscl::ObjectID::RO::Api&	volume,
					const char*						path
					) noexcept:
		_volume(volume),
		_path(path),
		_result(0)
		{
	}

Api::CreateDirPayload::
CreateDirPayload(	const Oscl::ObjectID::RO::Api&	volume,
					const char*						path
					) noexcept:
		_volume(volume),
		_path(path),
		_result(0)
		{
	}

Api::RemovePayload::
RemovePayload(	const Oscl::ObjectID::RO::Api&	volume,
				const char*						path
				) noexcept:
		_volume(volume),
		_path(path),
		_result(0)
		{
	}

Api::CancelPayload::
CancelPayload(Oscl::Mt::Itc::SrvMsg& msgToCancel) noexcept:
		_msgToCancel(msgToCancel)
		{
	}

