/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_fs_dir_wo_itc_respapih_
#define _oscl_fs_dir_wo_itc_respapih_
#include "oscl/mt/itc/mbox/clirsp.h"
#include "reqapi.h"

/** */
namespace Oscl {
/** */
namespace FS {
/** */
namespace Dir {
/** */
namespace WO {
/** */
namespace ITC {
/** */
namespace Resp {

/** */
class Api {
	public:
		/** */
		typedef Oscl::Mt::Itc::
				CliResponse<	typename Req::Api,
								Api,
								typename Req::Api::WritePayload
								>	WriteResp;
		/** */
		typedef Oscl::Mt::Itc::
				CliResponse<	typename Req::Api,
								Api,
								typename Req::Api::MovePayload
								>	MoveResp;
		/** */
		typedef Oscl::Mt::Itc::
				CliResponse<	typename Req::Api,
								Api,
								typename Req::Api::CopyPayload
								>	CopyResp;
		/** */
		typedef Oscl::Mt::Itc::
				CliResponse<	typename Req::Api,
								Api,
								typename Req::Api::CreateFilePayload
								>	CreateFileResp;
		/** */
		typedef Oscl::Mt::Itc::
				CliResponse<	typename Req::Api,
								Api,
								typename Req::Api::CreateDirPayload
								>	CreateDirResp;
		/** */
		typedef Oscl::Mt::Itc::
				CliResponse<	typename Req::Api,
								Api,
								typename Req::Api::RemovePayload
								>	RemoveResp;
		/** */
		typedef Oscl::Mt::Itc::
				CliResponse<	typename Req::Api,
								Api,
								typename Req::Api::CancelPayload
								>	CancelResp;
	public:
		/** */
		virtual ~Api() {}
		/** */
		virtual void	response(MoveResp& msg) noexcept=0;
		/** */
		virtual void	response(CopyResp& msg) noexcept=0;
		/** */
		virtual void	response(CreateFileResp& msg) noexcept=0;
		/** */
		virtual void	response(CreateDirResp& msg) noexcept=0;
		/** */
		virtual void	response(RemoveResp& msg) noexcept=0;
		/** */
//		virtual void	response(CancelResp& msg) noexcept=0;
	};

}
}
}
}
}
}

#endif
