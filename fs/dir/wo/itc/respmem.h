/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_fs_dir_wo_itc_respmemh_
#define _oscl_fs_dir_wo_itc_respmemh_
#include "respapi.h"
#include "oscl/memory/block.h"


/** */
namespace Oscl {
/** */
namespace FS {
/** */
namespace Dir {
/** */
namespace WO {
/** */
namespace ITC {
/** */
namespace Resp {

/** */
struct MoveMem {
	/** */
	Oscl::Memory::
	AlignedBlock<sizeof( typename Resp::Api::MoveResp)>	resp;
	/** */
	Oscl::Memory::
	AlignedBlock<sizeof(typename Req::Api::MovePayload)>	payload;
	};

/** */
struct CopyMem {
	/** */
	Oscl::Memory::
	AlignedBlock<sizeof( typename Resp::Api::CopyResp)>		resp;
	/** */
	Oscl::Memory::
	AlignedBlock<sizeof(typename Req::Api::CopyPayload)>	payload;
	};

/** */
struct CreateFileMem {
	/** */
	Oscl::Memory::
	AlignedBlock<sizeof( typename Resp::Api::CreateFileResp)>		resp;
	/** */
	Oscl::Memory::
	AlignedBlock<sizeof(typename Req::Api::CreateFilePayload)>	payload;
	};

/** */
struct CreateDirMem {
	/** */
	Oscl::Memory::
	AlignedBlock<sizeof( typename Resp::Api::CreateDirResp)>	resp;
	/** */
	Oscl::Memory::
	AlignedBlock<sizeof(typename Req::Api::CreateDirPayload)>	payload;
	};

/** */
struct RemoveMem {
	/** */
	Oscl::Memory::
	AlignedBlock<sizeof( typename Resp::Api::RemoveResp)>	resp;
	/** */
	Oscl::Memory::
	AlignedBlock<sizeof(typename Req::Api::RemovePayload)>	payload;
	};

/** */
struct CancelMem {
	/** */
	Oscl::Memory::
	AlignedBlock<sizeof( typename Resp::Api::CancelResp)>	resp;
	/** */
	Oscl::Memory::
	AlignedBlock<sizeof(typename Req::Api::CancelPayload)>	payload;
	};

/** */
union Mem {
	/** */
	void*			__qitemlink;
	/** */
	MoveMem			_move;
	/** */
	CopyMem			_copy;
	/** */
	CreateFileMem	_createFile;
	/** */
	CreateDirMem	_createDir;
	/** */
	RemoveMem		_remove;
	};

}
}
}
}
}
}

#endif
