/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "sync.h"
#include "oscl/mt/itc/mbox/syncrh.h"

using namespace Oscl::FS::Dir::WO::ITC;

Sync::Sync(	Oscl::FS::Dir::WO::ITC::Req::Api&	reqApi,
			Oscl::Mt::Itc::PostMsgApi&					myPapi
			) noexcept:
		_sap(reqApi,myPapi)
		{
	}

Oscl::FS::Dir::WO::ITC::
Req::Api::SAP&	Sync::getSAP() noexcept{
	return _sap;
	}

const Oscl::FS::Dir::WO::Move::Status::Result*
Sync::move(	const Oscl::ObjectID::RO::Api&		volume,
			const char*							srcPath,
			const char*							destPath
			) noexcept{
	Oscl::Mt::Itc::SyncReturnHandler	srh;
	Req::Api::MovePayload	payload(	volume,
										srcPath,
										destPath
										);
	Req::Api::MoveReq		req(	_sap.getReqApi(),
									payload,
									srh
									);
	_sap.postSync(req);
	return payload._result;
	}

const Oscl::FS::Dir::WO::Copy::Status::Result*
Sync::copy(	const Oscl::ObjectID::RO::Api&		srcVolume,
			const char*							srcPath,
			const Oscl::ObjectID::RO::Api&		destVolume,
			const char*							destPath
			) noexcept{
	Oscl::Mt::Itc::SyncReturnHandler	srh;
	Req::Api::CopyPayload	payload(	srcVolume,
										srcPath,
										destVolume,
										destPath
										);
	Req::Api::CopyReq		req(	_sap.getReqApi(),
									payload,
									srh
									);
	_sap.postSync(req);
	return payload._result;
	}

const Oscl::FS::Dir::WO::Create::Status::Result*
Sync::createFile(	const Oscl::ObjectID::RO::Api&	volume,
					const char*						path
					) noexcept{
	Oscl::Mt::Itc::SyncReturnHandler	srh;
	Req::Api::CreateFilePayload	payload(	volume,
											path
											);
	Req::Api::CreateFileReq		req(	_sap.getReqApi(),
										payload,
										srh
										);
	_sap.postSync(req);
	return payload._result;
	}

const Oscl::FS::Dir::WO::Create::Status::Result*
Sync::createDir(	const Oscl::ObjectID::RO::Api&	volume,
					const char*						path
					) noexcept{
	Oscl::Mt::Itc::SyncReturnHandler	srh;
	Req::Api::CreateDirPayload	payload(	volume,
											path
											);
	Req::Api::CreateDirReq		req(	_sap.getReqApi(),
										payload,
										srh
										);
	_sap.postSync(req);
	return payload._result;
	}

const Oscl::FS::Dir::WO::Remove::Status::Result*
Sync::remove(	const Oscl::ObjectID::RO::Api&	volume,
				const char*						path
				) noexcept{
	Oscl::Mt::Itc::SyncReturnHandler	srh;
	Req::Api::RemovePayload	payload(	volume,
										path
										);
	Req::Api::RemoveReq		req(	_sap.getReqApi(),
									payload,
									srh
									);
	_sap.postSync(req);
	return payload._result;
	}

