/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_fs_dir_wo_itc_reqapih_
#define _oscl_fs_dir_wo_itc_reqapih_
#include "oscl/mt/itc/mbox/srvreq.h"
#include "oscl/mt/itc/mbox/sap.h"
#include "oscl/oid/roapi.h"

/** */
namespace Oscl {
/** */
namespace FS {
/** */
namespace Dir {
/** */
namespace WO {
/** */
struct Handle;

/** */
namespace Move {
/** */
namespace Status{
/** */
class Result;
}
}

/** */
namespace Copy {
/** */
namespace Status{
/** */
class Result;
}
}

/** */
namespace Create {
/** */
namespace Status{
/** */
class Result;
}
}

/** */
namespace Remove {
/** */
namespace Status{
/** */
class Result;
}
}

/** */
namespace ITC {
/** */
namespace Req {

using namespace Oscl::Mt::Itc;

/** */
class Api {
	public:
		/** */
		virtual ~Api() {}
		/** */
		class CancelPayload {
			public:
				/**	*/
				Oscl::Mt::Itc::SrvMsg&	_msgToCancel;
			public:
				/** */
				CancelPayload(Oscl::Mt::Itc::SrvMsg& msgToCancel) noexcept;
			};
		/** */
		class MovePayload {
			public:
				/** */
				const Oscl::ObjectID::RO::Api&		_volume;
				/** */
				const char*	const					_srcPath;
				/** */
				const char*	const					_destPath;
				/** Result pointer is ZERO if the operation completed
					without error. Otherwise, a pointer to result
					code is returned. The reference can be used
					to determine the reason for the failure.
				 */
				const Move::Status::Result*			_result;

			public:
				/** */
				MovePayload(	const Oscl::ObjectID::RO::Api&	volume,
								const char*						srcPath,
								const char*						destPath
								) noexcept;
			};
		/** */
		class CopyPayload {
			public:
				/** */
				const Oscl::ObjectID::RO::Api&		_srcVolume;
				/** */
				const char*	const					_srcPath;
				/** */
				const Oscl::ObjectID::RO::Api&		_destVolume;
				/** */
				const char*	const					_destPath;
				/** Result pointer is ZERO if the operation completed
					without error. Otherwise, a pointer to result
					code is returned. The reference can be used
					to determine the reason for the failure.
				 */
				const Copy::Status::Result*			_result;

			public:
				/** */
				CopyPayload(	const Oscl::ObjectID::RO::Api&	srcVolume,
								const char*						srcPath,
								const Oscl::ObjectID::RO::Api&	destVolume,
								const char*						destPath
								) noexcept;
			};
		/** */
		class CreateFilePayload {
			public:
				/** */
				const Oscl::ObjectID::RO::Api&		_volume;
				/** */
				const char*	const					_path;
				/** Result pointer is ZERO if the operation completed
					without error. Otherwise, a pointer to result
					code is returned. The reference can be used
					to determine the reason for the failure.
				 */
				const Create::Status::Result*		_result;

			public:
				/** */
				CreateFilePayload(	const Oscl::ObjectID::RO::Api&	volume,
									const char*						path
									) noexcept;
			};
		/** */
		class CreateDirPayload {
			public:
				/** */
				const Oscl::ObjectID::RO::Api&		_volume;
				/** */
				const char*	const					_path;
				/** Result pointer is ZERO if the operation completed
					without error. Otherwise, a pointer to result
					code is returned. The reference can be used
					to determine the reason for the failure.
				 */
				const Create::Status::Result*		_result;

			public:
				/** */
				CreateDirPayload(	const Oscl::ObjectID::RO::Api&	volume,
									const char*						path
									) noexcept;
			};
		/** */
		class RemovePayload {
			public:
				/** */
				const Oscl::ObjectID::RO::Api&		_volume;
				/** */
				const char*	const					_path;
				/** Result pointer is ZERO if the operation completed
					without error. Otherwise, a pointer to result
					code is returned. The reference can be used
					to determine the reason for the failure.
				 */
				const Remove::Status::Result*		_result;

			public:
				/** */
				RemovePayload(	const Oscl::ObjectID::RO::Api&	volume,
								const char*						path
								) noexcept;
			};
	public:
		/** */
		typedef SrvRequest<Api,MovePayload>			MoveReq;
		/** */
		typedef SrvRequest<Api,CopyPayload>			CopyReq;
		/** */
		typedef SrvRequest<Api,CreateFilePayload>	CreateFileReq;
		/** */
		typedef SrvRequest<Api,CreateDirPayload>	CreateDirReq;
		/** */
		typedef SrvRequest<Api,RemovePayload>		RemoveReq;

	public:
		/** */
		typedef Oscl::Mt::Itc::SAP<Api>				SAP;
		/** */
		typedef Oscl::Mt::Itc::ConcreteSAP<Api>		ConcreteSAP;

	public:
		/** */
		virtual void	request(MoveReq& msg) noexcept=0;
		/** */
		virtual void	request(CopyReq& msg) noexcept=0;
		/** */
		virtual void	request(CreateFileReq& msg) noexcept=0;
		/** */
		virtual void	request(CreateDirReq& msg) noexcept=0;
		/** */
		virtual void	request(RemoveReq& msg) noexcept=0;
	};

}
}
}
}
}
}

#endif
