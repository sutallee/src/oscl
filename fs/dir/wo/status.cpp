/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "result.h"
#include "status.h"
#include "handler.h"

/** */
namespace Oscl {
/** */
namespace FS {
/** */
namespace Dir {
/** */
namespace WO {

/** */
namespace Move {
/** */
namespace Status {

class NoSuchVolume : public Result {
	public:
		void	query(Handler& handler) const noexcept;
	};

void	NoSuchVolume::query(Handler& handler) const noexcept{
	handler.noSuchVolume();
	}

const Result&	getNoSuchVolume() noexcept{
	static const NoSuchVolume	err;
	return err;
	}

/** */
class NoSuchSrcPath : public Result {
	public:
		void	query(Handler& handler) const noexcept;
	};

void	NoSuchSrcPath::query(Handler& handler) const noexcept{
	handler.noSuchSrcPath();
	}

const Result&	getNoSuchSrcPath() noexcept{
	static const NoSuchSrcPath	err;
	return err;
	}

/** */
class InvalidDestPath : public Result {
	public:
		void	query(Handler& handler) const noexcept;
	};

void	InvalidDestPath::query(Handler& handler) const noexcept{
	handler.invalidDestPath();
	}

const Result&	getInvalidDestPath() noexcept{
	static const InvalidDestPath	err;
	return err;
	}

/** */
class DestDirectoryFull : public Result {
	public:
		void	query(Handler& handler) const noexcept;
	};

void	DestDirectoryFull::query(Handler& handler) const noexcept{
	handler.destDirectoryFull();
	}

const Result&	getDestDirectoryFull() noexcept{
	static const DestDirectoryFull	err;
	return err;
	}

/** */
class SrcPathElementBusy : public Result {
	public:
		void	query(Handler& handler) const noexcept;
	};

void	SrcPathElementBusy::query(Handler& handler) const noexcept{
	handler.srcPathElementBusy();
	}

const Result&	getSrcPathElementBusy() noexcept{
	static const SrcPathElementBusy	err;
	return err;
	}

/** */
class OutOfSpace : public Result {
	public:
		void	query(Handler& handler) const noexcept;
	};

void	OutOfSpace::query(Handler& handler) const noexcept{
	handler.outOfSpace();
	}

const Result&	getOutOfSpace() noexcept{
	static const OutOfSpace	err;
	return err;
	}

/** */
class TransportError : public Result {
	public:
		void	query(Handler& handler) const noexcept;
	};

void	TransportError::query(Handler& handler) const noexcept{
	handler.transportError();
	}

const Result&	getTransportError() noexcept{
	static const TransportError	err;
	return err;
	}

}
}

/** */
namespace Copy {
/** */
namespace Status {
/** */
class NoSuchSrcVolume : public Result {
	public:
		void	query(Handler& handler) const noexcept;
	};

void	NoSuchSrcVolume::query(Handler& handler) const noexcept{
	handler.noSuchSrcVolume();
	}

const Result&	getNoSuchSrcVolume() noexcept{
	static const NoSuchSrcVolume	err;
	return err;
	}

/** */
class NoSuchDestVolume : public Result {
	public:
		void	query(Handler& handler) const noexcept;
	};

void	NoSuchDestVolume::query(Handler& handler) const noexcept{
	handler.noSuchDestVolume();
	}

const Result&	getNoSuchDestVolume() noexcept{
	static const NoSuchDestVolume	err;
	return err;
	}

/** */
class NoSuchSrcPath : public Result {
	public:
		void	query(Handler& handler) const noexcept;
	};

void	NoSuchSrcPath::query(Handler& handler) const noexcept{
	handler.noSuchSrcPath();
	}

const Result&	getNoSuchSrcPath() noexcept{
	static const NoSuchSrcPath	err;
	return err;
	}

/** */
class InvalidDestPath : public Result {
	public:
		void	query(Handler& handler) const noexcept;
	};

void	InvalidDestPath::query(Handler& handler) const noexcept{
	handler.invalidDestPath();
	}

const Result&	getInvalidDestPath() noexcept{
	static const InvalidDestPath	err;
	return err;
	}

/** */
class DestDirectoryFull : public Result {
	public:
		void	query(Handler& handler) const noexcept;
	};

void	DestDirectoryFull::query(Handler& handler) const noexcept{
	handler.destDirectoryFull();
	}

const Result&	getDestDirectoryFull() noexcept{
	static const DestDirectoryFull	err;
	return err;
	}

/** */
class DestOutOfSpace : public Result {
	public:
		void	query(Handler& handler) const noexcept;
	};

void	DestOutOfSpace::query(Handler& handler) const noexcept{
	handler.destOutOfSpace();
	}

const Result&	getDestOutOfSpace() noexcept{
	static const DestOutOfSpace	err;
	return err;
	}

/** */
class SrcTransportError : public Result {
	public:
		void	query(Handler& handler) const noexcept;
	};

void	SrcTransportError::query(Handler& handler) const noexcept{
	handler.srcTransportError();
	}

const Result&	getSrcTransportError() noexcept{
	static const SrcTransportError	err;
	return err;
	}

/** */
class DestTransportError : public Result {
	public:
		void	query(Handler& handler) const noexcept;
	};

void	DestTransportError::query(Handler& handler) const noexcept{
	handler.destTransportError();
	}

const Result&	getDestTransportError() noexcept{
	static const DestTransportError	err;
	return err;
	}

}
}

/** */
namespace Create {
/** */
namespace Status {
/** */
class NoSuchVolume : public Result {
	public:
		void	query(Handler& handler) const noexcept;
	};

void	NoSuchVolume::query(Handler& handler) const noexcept{
	handler.noSuchVolume();
	}

const Result&	getNoSuchVolume() noexcept{
	static const NoSuchVolume	err;
	return err;
	}

/** */
class InvalidPath : public Result {
	public:
		void	query(Handler& handler) const noexcept;
	};

void	InvalidPath::query(Handler& handler) const noexcept{
	handler.invalidPath();
	}

const Result&	getInvalidPath() noexcept{
	static const InvalidPath	err;
	return err;
	}

/** */
class PathAlreadyExists : public Result {
	public:
		void	query(Handler& handler) const noexcept;
	};

void	PathAlreadyExists::query(Handler& handler) const noexcept{
	handler.pathAlreadyExists();
	}

const Result&	getPathAlreadyExists() noexcept{
	static const PathAlreadyExists	err;
	return err;
	}

/** */
class OtherTypePathExists : public Result {
	public:
		void	query(Handler& handler) const noexcept;
	};

void	OtherTypePathExists::query(Handler& handler) const noexcept{
	handler.otherTypePathExists();
	}

const Result&	getOtherTypePathExists() noexcept{
	static const OtherTypePathExists	err;
	return err;
	}

/** */
class DirectoryFull : public Result {
	public:
		void	query(Handler& handler) const noexcept;
	};

void	DirectoryFull::query(Handler& handler) const noexcept{
	handler.directoryFull();
	}

const Result&	getDirectoryFull() noexcept{
	static const DirectoryFull	err;
	return err;
	}

/** */
class OutOfSpace : public Result {
	public:
		void	query(Handler& handler) const noexcept;
	};

void	OutOfSpace::query(Handler& handler) const noexcept{
	handler.outOfSpace();
	}

const Result&	getOutOfSpace() noexcept{
	static const OutOfSpace	err;
	return err;
	}

/** */
class TransportError : public Result {
	public:
		void	query(Handler& handler) const noexcept;
	};

void	TransportError::query(Handler& handler) const noexcept{
	handler.transportError();
	}

const Result&	getTransportError() noexcept{
	static const TransportError	err;
	return err;
	}
}
}

/** */
namespace Remove {
/** */
namespace Status {
/** */
class NoSuchVolume : public Result {
	public:
		void	query(Handler& handler) const noexcept;
	};

void	NoSuchVolume::query(Handler& handler) const noexcept{
	handler.noSuchVolume();
	}

const Result&	getNoSuchVolume() noexcept{
	static const NoSuchVolume	err;
	return err;
	}
/** */
class NoSuchPath : public Result {
	public:
		void	query(Handler& handler) const noexcept;
	};

void	NoSuchPath::query(Handler& handler) const noexcept{
	handler.noSuchPath();
	}

const Result&	getNoSuchPath() noexcept{
	static const NoSuchPath	err;
	return err;
	}

/** */
class TransportError : public Result {
	public:
		void	query(Handler& handler) const noexcept;
	};

void	TransportError::query(Handler& handler) const noexcept{
	handler.transportError();
	}

const Result&	getTransportError() noexcept{
	static const TransportError	err;
	return err;
	}

}
}


}
}
}
}

