/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_fs_dir_wo_apih_
#define _oscl_fs_dir_wo_apih_
#include "oscl/oid/roapi.h"

/** */
namespace Oscl {
/** */
namespace FS {
/** */
namespace Dir {
/** */
namespace WO {

/** */
namespace Move {
/** */
namespace Status {
/** */
class Result;
}
}

/** */
namespace Copy {
/** */
namespace Status {
/** */
class Result;
}
}

/** */
namespace Create {
/** */
namespace Status {
/** */
class Result;
}
}

/** */
namespace Remove {
/** */
namespace Status {
/** */
class Result;
}
}

/** */
class Api {
	public:
		/** */
		virtual ~Api() {}
		/** */
		virtual const Move::Status::Result*
			move(	const Oscl::ObjectID::RO::Api&	volume,
					const char*						srcPath,
					const char*						destPath
					) noexcept=0;
		/** It srcPath specifies a directory, then a recursive
			deep copy is performed. In this case, if destPath
			previously existed, then any existing file entries
			are overwritten.
			If srcPath is a file, and destPath is a directory,
			then a copy of the file is placed in the destPath
			directory with the same file name.
			If srcPath is a directory and destPath is an existing
			directory with a different name, then a directory with
			the name specified by srcPath is created in the
			directory specified by destPath.
			If srcPath is a directory and destPath is an existing
			directory with the same name, then the contents of the
			srcPath directory are recursively copied to the destPath
			directory. New directories are created as required by
			the srcPath.
			The directory entries specified by srcPath and
			destPath must be of the same type. E.g. if srcPath
			specifies a directory, then dest path must either
			not exist (in which case it will be created) or
			it must be an existing directory.
			If the destination already exists, it will be
			overwritten with the contents of 
		 */
		virtual const Copy::Status::Result*
			copy(	const Oscl::ObjectID::RO::Api&	srcVolume,
					const char*						srcPath,
					const Oscl::ObjectID::RO::Api&	destVolume,
					const char*						destPath
					) noexcept=0;
		/** */
		virtual const Create::Status::Result*
			createFile(	const Oscl::ObjectID::RO::Api&	volume,
						const char*					path
						) noexcept=0;
		/** */
		virtual const Create::Status::Result*
			createDir(	const Oscl::ObjectID::RO::Api&	volume,
						const char*					path
						) noexcept=0;
		/** If the directory entry specified by path is a
			file then the file is deleted. If the directory
			entry specified by path is a directory, then
			the contents of the directory are recursively
			deleted and then the directory is deleted.
		 */
		virtual const Remove::Status::Result*
			remove(	const Oscl::ObjectID::RO::Api&	volume,
					const char*					path
					) noexcept=0;
	};

}
}
}
}

#endif
