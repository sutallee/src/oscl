/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <string.h>
#include <ctype.h>
#include "fat.h"

/** */
namespace Oscl {
/** */
namespace FS {
/** */
namespace FAT {

static bool	isValidShortFileName(	const char*	name,
									unsigned&	nameLen,
									unsigned&	extLen
									) noexcept{
	if(name[0] == 0x20){
		return false;
		}
	size_t	len	= strlen(name);
	if(!len){
		return false;
		}
	if(len > (8+3+1)){
		return false;
		}
	bool	dotFound	= false;
	nameLen	= 0;
	extLen	= 0;
	for(unsigned i=0;i<len;++i){
		switch(name[i]){
			case 0x2E:
				if(dotFound){
					// too many dots
					return false;
					}
				continue;
			case 0x22:
			case 0x2A:
			case 0x2B:
			case 0x2C:
			case 0x2F:
			case 0x3A:
			case 0x3B:
			case 0x3C:
			case 0x3D:
			case 0x3E:
			case 0x3F:
			case 0x5B:
			case 0x5C:
			case 0x5D:
				// Invalid Characters
				return false;
			}
		if(islower(name[i])){
			return false;
			}
		if(dotFound){
			++extLen;
			}
		else {
			++nameLen;
			}
		}
	if(nameLen > 8){
		return false;
		}
	if(extLen > 3){
		return false;
		}

	return true;
	}

bool	isValidShortFileName(const char* name) noexcept{
	unsigned	nameLen;
	unsigned	extLen;
	return isValidShortFileName(name,nameLen,extLen);
	}


void	Date::date(	unsigned		year,
					unsigned char	monthOfYear,
					unsigned char	dayOfMonth
					) noexcept{
	uint16_t
	value	=	monthOfYear - 1980;
	value	<<= 4;
	value	|=	(0x0F & monthOfYear);
	value	<<= 5;
	value	|=	(0x1F & dayOfMonth);
	}

void	Time::time(	unsigned char	hours,
					unsigned char	minutes,
					unsigned char	seconds
					) noexcept{
	uint16_t
	value	=	hours;
	value	<<= 6;
	value	|=	(0x3F & minutes);
	value	<<= 5;
	value	|=	(0x1F & (seconds>>1));
	}

Partition::Partition(	uint32_t	rootDirSectors,
						uint32_t	firstDataSector,
						uint32_t	totalSectors,
						uint8_t	sectorsPerCluster
						) noexcept:
		_rootDirSectors(rootDirSectors),
		_firstDataSector(firstDataSector),
		_totalSectors(totalSectors),
		_sectorsPerCluster(sectorsPerCluster)
		{
	}
uint32_t	Partition::firstSectorOfCluster(uint32_t cluster) const noexcept{
	return ((cluster-2)*_sectorsPerCluster) + _firstDataSector;
	}

bool	DirEntry::nameMatch(	const char*	name,
								unsigned	nameLength
								) const noexcept{
	unsigned i;
	bool	hasDot	= false;
	for(i=0;i<nameLength;++i){
		if(name[i] == '.'){
			hasDot	= true;
			break;
			}
		}
	unsigned	nameNameLen	= i;
	if(!mainMatch(name,nameNameLen)){
		return false;
		}

	if(!hasDot){
		return extensionMatch(&name[nameNameLen],0);
		}
	nameLength	-= nameNameLen;
	nameLength	-= 1;
	return extensionMatch(&name[nameNameLen+1],nameLength);
	}

bool	DirEntry::mainMatch(	const char*	main,
								unsigned	mainLength
								) const noexcept{
	if(mainLength > 8){
		return false;
		}
	unsigned i;
	for(i=0;i<8;++i){
		if(_shortName[i] == 0x20){
			break;
			}
		}
	unsigned	shortNameLen	= i;

	if(shortNameLen != mainLength){
		return false;
		}
	if(strncmp((char*)_shortName,main,mainLength)){
		return false;
		}
	return true;
	}

bool	DirEntry::extensionMatch(	const char*	extension,
									unsigned	extLength
									) const noexcept{
	if(extLength > 3){
		return false;
		}
	unsigned i;
	for(i=0;i<3;++i){
		if(_shortName[8+i] == 0x20){
			break;
			}
		}
	unsigned	shortNameLen	= i;

	if(shortNameLen != extLength){
		return false;
		}
	if(strncmp((char*)&_shortName[8],extension,shortNameLen)){
		return false;
		}
	return true;
	}

bool	DirEntry::name(const char* name) noexcept{
	unsigned	nameLen;
	unsigned	extLen;
   uint8_t*  p  = (uint8_t*)name;

	if(!isValidShortFileName(name,nameLen,extLen)){
		return true;
		}
	for(unsigned i=0;i<8;++i){
		if(i<nameLen){
			if(i==0){
				if(p[i] == 0xE5){
					_shortName[i] = 0x05;
					continue;
					}
				}
			_shortName[i] = p[i];
			}
		else {
			_shortName[i] = 0x20;
			}
		}
	for(unsigned i=0;i<3;++i){
		if(i<extLen){
			_shortName[8+i] = p[nameLen+1+i];
			}
		else {
			_shortName[8+i] = 0x20;
			}
		}
	return false;
	}

/** */
namespace FAT16 {

Partition::Partition(	uint32_t	rootDirSectors,
						uint32_t	firstDataSector,
						uint8_t	sectorsPerCluster,
						uint16_t	reservedSectorCount,
						uint16_t	bytesPerSector,
						uint32_t	fatSize,
						uint32_t	firstRootDirSecNum,
						uint32_t	totalSectors
						) noexcept:
		Oscl::FS::FAT::Partition(	rootDirSectors,
									firstDataSector,
									totalSectors,
									sectorsPerCluster
									),
		_reservedSectorCount(reservedSectorCount),
		_bytesPerSector(bytesPerSector),
		_fatSize(fatSize),
		_firstRootDirSecNum(firstRootDirSecNum)
		{
	}
void	Partition::fatEntry(	uint8_t		fatN,
								uint32_t		cluster,
								uint32_t&		fatSector,
								uint32_t&		fatEntryOffset
								) const noexcept{
	uint32_t
	fatOffset	= cluster * 2;
	fatSector	=		_reservedSectorCount
						+	(fatOffset/_bytesPerSector)
						+	(fatN * _fatSize)
						;
	fatEntryOffset	= fatOffset % _bytesPerSector;
	}
uint32_t	Partition::firstRootDirSecNum() const noexcept{
	return _firstRootDirSecNum;
	}


}

uint32_t	BPB::firstRootDirSecNum() const noexcept{
	return reservedSectorCount() + (_numberOfFats * fatSize());
	}

}
}
}

