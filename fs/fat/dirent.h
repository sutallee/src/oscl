/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_fs_fat_direnth_
#define _oscl_fs_fat_direnth_

/** */
namespace Oscl {
/** */
namespace FS {
/** */
namespace FAT {
/** */
namespace Cache {

/** This structure contains a cached copy of a single
	FAT directory entry. This entry is a proxy for the
	actual entry in the FAT directory. The GOF Proxy
	pattern enables a cached value to be maintained
	and refreshed as required by an access. In this
	case, the block containing the actual directory
	entry would be re-read as necessary when the
	entry is accessed. The implementation may also
	invalidate the entry as required when the FAT
	copy is changed, which would cause the FAT copy
	to be re-read. Some frequently used fields of
	the entry are also cached in the DirEntry such
	that the block need not be read into memory.
	Such cached fields *are* re-read if the proxy
	is marked as invalidated.
	Since the entry name is checked before the
	DirEntry is created, the name field is NOT
	cached.
	So what identifies the FAT directory which
	the DirEntry is a part? A "sector" number?
	Presumable the DirEntry does NOT move within
	the directory structure during its lifetime.
	It may be created or destroyed however.
 */
class DirEntry {
	private:
		/** */
		unsigned long		_directorySectorNumber;
		/** */
		unsigned long		_entryIndex;
		/** */
		unsigned long		_firstFileSectorNumber;
		/** */
		unsigned long		_fileSize;
	};

}
}
}
}

#endif
