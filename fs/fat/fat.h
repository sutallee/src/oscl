/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_fs_fat_fath_
#define _oscl_fs_fat_fath_
#include <stdint.h>
#include "oscl/compiler/align.h"

/** */
namespace Oscl {
/** */
namespace FS {
/** */
namespace FAT {

/** */
bool	isValidShortFileName(const char* filename) noexcept;

/** */
struct Date {
	/** */
	uint8_t	_value[2];
	/** */
	inline	uint16_t	value() const noexcept{
		uint16_t
		v	=	_value[1];
		v	<<=	8;
		v	|=	_value[0];
		return v;
		}
	/** */
	inline uint8_t	dayOfMonth() const noexcept{
		uint8_t
		v	=	_value[0];
		v	&=	0x1F;
		return v;
		}
	/** */
	inline uint8_t	monthOfYear() const noexcept{
		uint16_t
		v	=	value();
		v	>>=	5;
		v	&=	0x000F;
		return (uint8_t)v;
		}
	/** */
	inline uint8_t	yearsSince1980() const noexcept{
		uint8_t
		v	=	_value[1];
		v	>>=	1;
		return (uint8_t)v;
		}
	/** */
	inline void	value(uint16_t v) noexcept{
		_value[0]	=	(uint8_t)v;
		v			>>=	8;
		_value[1]	=	(uint8_t)v;
		}
	/** */
	void	date(	unsigned		year,
					unsigned char	monthOfYear,
					unsigned char	dayOfMonth
					) noexcept;
	};
/** */
struct Time {
	/** */
	uint8_t	_value[2];
	/** */
	inline	uint16_t	value() const noexcept{
		uint16_t
		v	=	_value[1];
		v	<<=	8;
		v	|=	_value[0];
		return v;
		}
	/** */
	inline void	value(uint16_t v) noexcept{
		_value[0]	=	(uint8_t)v;
		v			>>=	8;
		_value[1]	=	(uint8_t)v;
		}
	/** */
	inline uint8_t	seconds() const noexcept{
		uint8_t
		v	=	_value[0];
		v	<<=	1;
		return v;
		}
	/** */
	inline uint8_t	minutes() const noexcept{
		uint16_t
		v	=	value();
		v	>>=	5;
		v	&=	0x001F;
		return (uint8_t)v;
		}
	/** */
	inline uint8_t	hours() const noexcept{
		uint8_t
		v	=	_value[1];
		v	>>=	3;
		return (uint8_t)v;
		}
	/** */
	void	time(	unsigned char	hours,
					unsigned char	minutes,
					unsigned char	seconds
					) noexcept;
	};

/** */
struct DirEntry {
	/** */
	enum{shortNameLen=11};
	/** */
	uint8_t	_shortName[shortNameLen];
	/** */
	uint8_t	_attributes;
	/** */
	uint8_t	_ntReserved;
	/** */
	uint8_t	_creationTimeTenths;
	/** */
	Time		_creationTime	OsclPackedPostMacro();
	/** */
	Date		_creationDate	OsclPackedPostMacro();
	/** */
	Date		_lastAccessDate	OsclPackedPostMacro();
	/** */
	uint8_t	_firstClusterHigh[2];
	/** */
	Time		_writeTime	OsclPackedPostMacro();
	/** */
	Date		_writeDate	OsclPackedPostMacro();
	/** */
	uint8_t	_firstClusterLow[2];
	/** */
	uint8_t	_fileSize[4];
	/** */
	inline bool	longName() const noexcept{
		return _attributes & 0x0F;
		}
	/** */
	inline bool	readOnly() const noexcept{
		return (_attributes & 0x01) && !longName();
		}
	/** */
	inline bool	hidden() const noexcept{
		return (_attributes & 0x02) && !longName();
		}
	/** */
	inline bool	system() const noexcept{
		return (_attributes & 0x04) && !longName();
		}
	/** */
	inline bool	volumeID() const noexcept{
		return (_attributes & 0x08) && !longName();
		}
	/** */
	inline bool	directory() const noexcept{
		return _attributes & 0x10;
		}
	/** */
	inline bool	archive() const noexcept{
		return _attributes & 0x20;
		}
	/** */
	inline bool	isFree() const noexcept{
		return (_shortName[0] == 0xE5) || (_shortName[0] == 0x00);
		}
	/** */
	inline void	free() noexcept{
		_shortName[0]	= 0xE5;
		}
	/** */
	inline bool	noMoreDirEntriesFollow() const noexcept{
		return _shortName[0] == 0x00;
		}
	/** */
	inline void	freeNoMoreDirEntriesFollow() noexcept{
		_shortName[0] = 0x00;
		}
	/** */
	inline uint32_t	firstCluster() const noexcept{
		uint32_t
		value	=	_firstClusterHigh[1];
		value	<<=	8;
		value	|=	_firstClusterHigh[0];
		value	<<=	8;
		value	|=	_firstClusterLow[1];
		value	<<=	8;
		value	|=	_firstClusterLow[0];
		return value;
		}
	/** */
	inline void firstCluster(uint32_t value) noexcept{
		_firstClusterLow[0]		= (uint8_t)value;
		value	>>=	8;
		_firstClusterLow[1]		= (uint8_t)value;
		value	>>=	8;
		_firstClusterHigh[0]	= (uint8_t)value;
		value	>>=	8;
		_firstClusterHigh[1]	= (uint8_t)value;
		}
	/** */
	inline uint32_t	fileSize() const noexcept{
		uint32_t
		value	=	_fileSize[3];
		value	<<=	8;
		value	|=	_fileSize[2];
		value	<<=	8;
		value	|=	_fileSize[1];
		value	<<=	8;
		value	|=	_fileSize[0];
		return value;
		}
	/** */
	inline void		fileSize(uint32_t value) noexcept{
		_fileSize[0]	= (uint8_t)value;
		value	>>=	8;
		_fileSize[1]	= (uint8_t)value;
		value	>>=	8;
		_fileSize[2]	= (uint8_t)value;
		value	>>=	8;
		_fileSize[3]	= (uint8_t)value;
		}
	/** */
	bool	nameMatch(	const char*	name,
						unsigned	nameLength
						) const noexcept;
	/** */
	bool	mainMatch(	const char*	main,
						unsigned	mainLength
						) const noexcept;
	/** */
	bool	extensionMatch(	const char*	extension,
							unsigned	extLength
							) const noexcept;
	/** Parses, validates and sets the _shortName
		using the name parameter. If the "name"
		parameter does not meet the 8.3 restriction
		or contains any invalid filename characters,
		then this operation will return "true".
	 */
	bool	name(const char* name) noexcept;
	};

/** */
class Partition {
	public:
		/** */
		const uint32_t	_rootDirSectors;
		/** */
		const uint32_t	_firstDataSector;
		/** */
		const uint32_t	_totalSectors;
		/** */
		const uint8_t		_sectorsPerCluster;
	public:
		/** */
		Partition(	uint32_t	rootDirSectors,
					uint32_t	firstDataSector,
					uint32_t	totalSectors,
					uint8_t	sectorsPerCluster
					) noexcept;
		/** */
		uint32_t	firstSectorOfCluster(uint32_t cluster) const noexcept;
		/** */
		inline uint8_t	sectorsPerCluster() const noexcept{
			return _sectorsPerCluster;
			}
		/** */
		inline uint32_t	totalSectors() const noexcept{
			return _totalSectors;
			}
	};

/** */
namespace FAT16 {

/** */
struct Entry {
	/** */
	enum{EOC=0xFFF8};
	/** */
	uint8_t	_value[2];
	/** */
	inline	uint16_t	value() const noexcept{
		uint16_t
		v	=	_value[1];
		v	<<=	8;
		v	|=	_value[0];
		return v;
		}
	/** */
	inline	void	value(uint16_t v) noexcept{
		_value[0]	= v;
		v			>>=	8;
		_value[1]	= v;
		}
	/** */
	inline void	markAsEOC() noexcept{
		value(0xFFF8);
		}
	/** */
	inline void	markAsFree() noexcept{
		value(0);
		}
	/** */
	inline	bool	isEOC() const noexcept{
		return value() >= 0xFFF8;
		}
	/** */
	inline	bool	isFree() const noexcept{
		return value() == 0;
		}
	};

/** Note: All sector addresses are relative to the
	beginning of this partition.
 */
class Partition : public Oscl::FS::FAT::Partition {
	public:
		/** */
		const uint16_t	_reservedSectorCount;
		/** */
		const uint16_t	_bytesPerSector;
		/** */
		const uint32_t	_fatSize;
		/** */
		const uint32_t	_firstRootDirSecNum;
	public:
		/** */
		Partition(	uint32_t	rootDirSectors,
					uint32_t	firstDataSector,
					uint8_t	sectorsPerCluster,
					uint16_t	reservedSectorCount,
					uint16_t	bytesPerSector,
					uint32_t	fatSize,
					uint32_t	firstRootDirSecNum,
					uint32_t	totalSectors
					) noexcept;
		/** */
		void	fatEntry(	uint8_t		fatN,
							uint32_t		cluster,
							uint32_t&		fatSector,
							uint32_t&		fatEntryOffset
							) const noexcept;
		/** */
		uint32_t	firstRootDirSecNum() const noexcept;
	};

/** */
struct BS36 {
	/** */
	uint8_t	_driveNumber;
	/** */
	uint8_t	_reserved1;
	/** */
	uint8_t	_bootSig;
	/** */
	uint8_t	_volumeID[4];
	/** */
	uint8_t	_volumeLabel[11];
	/** */
	uint8_t	_fileSystemType[8];
	/** */
	inline uint32_t	volumeID() const noexcept{
		uint32_t
		value	=	_volumeID[3];
		value	<<=	8;
		value	|=	_volumeID[2];
		value	<<=	8;
		value	|=	_volumeID[1];
		value	<<=	8;
		value	|=	_volumeID[0];
		return value;
		}
	};
}

/** */
namespace FAT32 {

/** */
struct BPB36 {
	/** */
	uint8_t	_fatSize32[4];
	/** */
	uint8_t	_extFlags[2];
	/** */
	uint8_t	_fsVersion[2];
	/** */
	uint8_t	_rootCluster[4];
	/** */
	uint8_t	_fileSystemInfo[2];
	/** */
	uint8_t	_bkBootSector[2];
	/** */
	uint8_t	_reserved[12];
	/** */
	inline bool	fatsMirrored() const noexcept{
		return !(_extFlags[0] & 0x80);
		}
	/** */
	inline unsigned	activeFAT() const noexcept{
		return _extFlags[0] & 0x0F;
		}
	/** */
	inline uint32_t	fatSize32() const noexcept{
		uint32_t
		value	=	_fatSize32[3];
		value	<<=	8;
		value	|=	_fatSize32[2];
		value	<<=	8;
		value	|=	_fatSize32[1];
		value	<<=	8;
		value	|=	_fatSize32[0];
		return value;
		}
	/** */
	inline uint16_t	fsVersion() const noexcept{
		uint16_t
		value	=	_fsVersion[1];
		value	<<=	8;
		value	|=	_fsVersion[0];
		return value;
		}
	/** */
	inline uint32_t	rootCluster() const noexcept{
		uint32_t
		value	=	_rootCluster[3];
		value	<<=	8;
		value	|=	_rootCluster[2];
		value	<<=	8;
		value	|=	_rootCluster[1];
		value	<<=	8;
		value	|=	_rootCluster[0];
		return value;
		}
	/** */
	inline uint16_t	fileSystemInfo() const noexcept{
		uint16_t
		value	=	_fileSystemInfo[1];
		value	<<=	8;
		value	|=	_fileSystemInfo[0];
		return value;
		}
	/** */
	inline uint16_t	bkBootSector() const noexcept{
		uint16_t
		value	=	_bkBootSector[1];
		value	<<=	8;
		value	|=	_bkBootSector[0];
		return value;
		}
	};

struct BS64 {
	/** */
	uint8_t	_driveNumber;
	/** */
	uint8_t	_reserved1;
	/** */
	uint8_t	_bootSig;
	/** */
	uint8_t	_volumeID[4];
	/** */
	uint8_t	_volumeLabel[11];
	/** */
	uint8_t	_fileSystemType[8];
	/** */
	inline uint32_t	volumeID() const noexcept{
		uint32_t
		value	=	_volumeID[3];
		value	<<=	8;
		value	|=	_volumeID[2];
		value	<<=	8;
		value	|=	_volumeID[1];
		value	<<=	8;
		value	|=	_volumeID[0];
		return value;
		}
	};

}


/** */
struct BS {
	/** */
	uint8_t	_jmpBoot[3];
	/** */
	uint8_t	_oemName[8];
	/** */
	uint8_t	_pad11[25];
	union {
		struct {
			FAT16::BS36		_bs36;		// offset 36,26 bytes
			} _fat16;
		struct {
			uint8_t		_pad[28];	// offset 36, 28 bytes
			FAT32::BS64		_bs64;		// offset 64, 26 bytes
			} _fat32;
		} _type;
	};

/** */
struct BPB {
	/** */
	uint8_t	_pad0[11];
	/** */
	uint8_t	_bytesPerSector[2];
	/** */
	uint8_t	_sectorsPerCluster;
	/** */
	uint8_t	_reservedSectorCount[2];
	/** */
	uint8_t	_numberOfFats;
	/** */
	uint8_t	_rootEntryCount[2];
	/** */
	uint8_t	_totalSectors16[2];
	/** */
	uint8_t	_media;
	/** */
	uint8_t	_fatSize16[2];
	/** */
	uint8_t	_sectorsPerTrack[2];
	/** */
	uint8_t	_numberOfHeads[2];
	/** */
	uint8_t	_hiddenSectors[4];
	/** */
	uint8_t	_totalSectors32[4];
	/** */
	FAT32::BPB36	_fat32;	// offset 36, 28 bytes long
	/** */
	inline uint16_t	fatSize16() const noexcept{
		uint16_t
		value	=	_fatSize16[1];
		value	<<=	8;
		value	|=	_fatSize16[0];
		return value;
		}
	/** */
	inline uint16_t	totalSectors16() const noexcept{
		uint16_t
		value	=	_totalSectors16[1];
		value	<<=	8;
		value	|=	_totalSectors16[0];
		return value;
		}
	/** */
	inline uint16_t	bytesPerSector() const noexcept{
		uint16_t
		value	=	_bytesPerSector[1];
		value	<<=	8;
		value	|=	_bytesPerSector[0];
		return value;
		}
	/** */
	inline uint16_t	reservedSectorCount() const noexcept{
		uint16_t
		value	=	_reservedSectorCount[1];
		value	<<=	8;
		value	|=	_reservedSectorCount[0];
		return value;
		}
	/** */
	inline uint16_t	rootEntryCount() const noexcept{
		uint16_t
		value	=	_rootEntryCount[1];
		value	<<=	8;
		value	|=	_rootEntryCount[0];
		return value;
		}
	/** */
	inline uint16_t	sectorsPerTrack() const noexcept{
		uint16_t
		value	=	_sectorsPerTrack[1];
		value	<<=	8;
		value	|=	_sectorsPerTrack[0];
		return value;
		}
	/** */
	inline uint16_t	numberOfHeads() const noexcept{
		uint16_t
		value	=	_numberOfHeads[1];
		value	<<=	8;
		value	|=	_numberOfHeads[0];
		return value;
		}
	/** */
	inline uint32_t	hiddenSectors() const noexcept{
		uint32_t
		value	=	_hiddenSectors[3];
		value	<<=	8;
		value	|=	_hiddenSectors[2];
		value	<<=	8;
		value	|=	_hiddenSectors[1];
		value	<<=	8;
		value	|=	_hiddenSectors[0];
		return value;
		}
	/** */
	inline uint32_t	totalSectors32() const noexcept{
		uint32_t	value;
		value	=	_totalSectors32[3];
		value	<<=	8;
		value	|=	_totalSectors32[2];
		value	<<=	8;
		value	|=	_totalSectors32[1];
		value	<<=	8;
		value	|=	_totalSectors32[0];
		return value;
		}
	/** */
	inline uint32_t	totalSectors() const noexcept{
		uint32_t	value;
		value	= totalSectors16();
		if(!value){
			value	=	totalSectors32();
			}
		return value;
		}
	/** */
	inline uint32_t	fatSize() const noexcept{
		uint32_t	value;
		value	= fatSize16();
		if(!value){
			value	=	_fat32._fatSize32[3];
			value	<<=	8;
			value	|=	_fat32._fatSize32[2];
			value	<<=	8;
			value	|=	_fat32._fatSize32[1];
			value	<<=	8;
			value	|=	_fat32._fatSize32[0];
			}
		return value;
		}

	/** */
	uint32_t	firstRootDirSecNum() const noexcept;

	/** */
	inline uint32_t	rootDirSectors() const noexcept{
		uint32_t
		value	= (		(rootEntryCount()*32)
					+	(bytesPerSector()-1)
					) / bytesPerSector()
					;
		return value;
		}

	/** */
	inline uint32_t	firstDataSector() const noexcept{
		uint32_t
		value	=		reservedSectorCount()
					+	(_numberOfFats * fatSize())
					+	rootDirSectors()
					;
		return value;
		}
	/** */
	inline uint32_t	dataSectors() const noexcept{
		uint32_t
		value	=		totalSectors()
					-	(		reservedSectorCount()
							+	(_numberOfFats * fatSize())
							+	rootDirSectors()
							);
		return value;
		}
	/** */
	inline uint32_t	countOfClusters() const noexcept{
		uint32_t
		value	= dataSectors() / _sectorsPerCluster;
		return value;
		}
	/** */
	inline bool	isFat12() const noexcept{
		return (countOfClusters() < 4085);
		}
	/** */
	inline bool	isFat16() const noexcept{
		uint32_t	nClusters	= countOfClusters();
		return (nClusters >= 4085) && (nClusters < 65525);
		}
	/** */
	inline bool	isFat32() const noexcept{
		return (countOfClusters() >= 65525);
		}
	};

/** */
union PartHeader {
	/** */
	BS	_bs;
	/** */
	BPB	_bpb;
	};

}
}
}

#endif
