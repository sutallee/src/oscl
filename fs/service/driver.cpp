/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <new>
#include <stdio.h>
#include "oscl/error/fatal.h"
#include "oscl/error/info.h"
#include "driver.h"
#include "oscl/fs/volume/api.h"
#include "oscl/fs/file/ro/oc/status.h"
#include "oscl/fs/file/ro/access/status.h"
#include "oscl/fs/file/rw/oc/status.h"
#include "oscl/fs/file/rw/access/status.h"
#include "oscl/fs/dir/wo/status.h"

using namespace Oscl::FS::Service;

static void	printOID(const Oscl::ObjectID::RO::Api& oid) noexcept{
	Oscl::ObjectID::RO::Iterator	it(oid);
	for(it.first();it.more();it.next()){
		char	buffer[16];
		sprintf(buffer,".%u",it.current());
		Oscl::Error::Info::log(buffer);
		}
	}

Driver::Driver(Oscl::Mt::Itc::PostMsgApi& myPapi) noexcept:
		_volumeMonitorSync(*this,myPapi),
		_volumeMonitorSAP(*this,myPapi),
		_fileRoOcSync(*this,myPapi),
		_fileRoAccessSync(*this,myPapi),
		_fileRwOcSync(*this,myPapi),
		_fileRwAccessSync(*this,myPapi),
		_dirWoSync(*this,myPapi)
		{
	if(sizeof(Oscl::FS::File::RO::OC::State) < sizeof(RoFileState)){
		Oscl::ErrorFatal::logAndExit("Oscl::FS::File::RO::OC::State too small for filesystem.\n");
		}
	if(sizeof(Oscl::FS::File::RW::OC::State) < sizeof(RwFileState)){
		Oscl::ErrorFatal::logAndExit("Oscl::FS::File::RW::OC::State too small for filesystem.\n");
		}
	}

Oscl::FS::Volume::Monitor::
ITC::Req::Api::SAP&			Driver::getVolumeMonitorSAP() noexcept{
	return _volumeMonitorSAP;
	}

Oscl::FS::File::RO::OC::
ITC::Req::Api::SAP&			Driver::getFileRoOcSAP() noexcept{
	return _fileRoOcSync.getSAP();
	}

Oscl::FS::File::RO::Access::
ITC::Req::Api::SAP&			Driver::getFileRoAccessSAP() noexcept{
	return _fileRoAccessSync.getSAP();
	}

Oscl::FS::File::RW::OC::
ITC::Req::Api::SAP&			Driver::getFileRwOcSAP() noexcept{
	return _fileRwOcSync.getSAP();
	}

Oscl::FS::File::RW::Access::
ITC::Req::Api::SAP&			Driver::getFileRwAccessSAP() noexcept{
	return _fileRwAccessSync.getSAP();
	}

Oscl::FS::Dir::WO::
ITC::Req::Api::SAP&			Driver::getDirWoSAP() noexcept{
	return _dirWoSync.getSAP();
	}

Oscl::FS::Volume::Monitor::Api&		Driver::getVolumeMonitorSync() noexcept{
	return _volumeMonitorSync;
	}

Oscl::FS::File::RO::OC::Api&		Driver::getFileRoOcSync() noexcept{
	return _fileRoOcSync;
	}

Oscl::FS::File::RO::Access::Api&	Driver::getFileRoAccessSync() noexcept{
	return _fileRoAccessSync;
	}

Oscl::FS::File::RW::OC::Api&		Driver::getFileRwOcSync() noexcept{
	return _fileRwOcSync;
	}

Oscl::FS::File::RW::Access::Api&	Driver::getFileRwAccessSync() noexcept{
	return _fileRwAccessSync;
	}

Oscl::FS::Dir::WO::Api&				Driver::getDirWoSync() noexcept{
	return _dirWoSync;
	}

void	Driver::mount(Oscl::FS::Volume::Api& volume) noexcept{
	// Here's a good place to test volume function?
	// This executes during the mounter's activate()
	// operation though.
	Oscl::Error::Info::log("Mouting OID: ");
	printOID(volume.location());
	Oscl::Error::Info::log("\n");
	_activeVolumes.put(&volume);
	processVolumeMountWaiters();
	}

void	Driver::umount(Oscl::FS::Volume::Api& volume) noexcept{
	// FIXME: before removing, all outstanding operations
	// on the volume must be "completed". Perhaps the volume
	// itself will have performed this before invoking this
	// operation.
	_activeVolumes.remove(&volume);
	processVolumeUnmountWaiters();
	}

/////////////////
void	Driver::request(	Oscl::FS::File::RO::OC::
							ITC::Req::Api::OpenReq&		msg
							) noexcept{
	Oscl::FS::Volume::Api*
	volume	= lookupVolume(msg._payload._volumeID);
	if(!volume){
		msg._payload._result	=	&Oscl::FS::File::RO::OC::Open::
									Status::getNoSuchVolumeError();
		msg.returnToSender();
		return;
		}
	RoFileState*
	fs	= new(&msg._payload._state)RoFileState(*volume);

	const Oscl::FS::File::RO::OC::Open::Status::Result*
	result	= volume->open(	fs->_stateMem,
							msg._payload._path,
							fs->_handle
							);
	if(result){
		msg._payload._result	= result;
		msg.returnToSender();
		return;
		}
	_roState.put(fs);
	msg._payload._handle.vp	= fs;
	msg.returnToSender();
	}

void	Driver::request(	Oscl::FS::File::RO::OC::
							ITC::Req::Api::CloseReq&		msg
							) noexcept{
	if(!msg._payload._handle.vp){
		msg._payload._result	=	&Oscl::FS::File::RO::OC::Close::
									Status::getNoSuchHandleError();
		msg.returnToSender();
		return;
		}
	RoFileState*	fs	= (RoFileState*)msg._payload._handle.vp;
	if(!_roState.remove(fs)){
		msg._payload._result	=	&Oscl::FS::File::RO::OC::Close::
									Status::getNoSuchHandleError();
		msg.returnToSender();
		return;
		}
	msg._payload._result	= fs->_volume.close(fs->_handle);
	msg._payload._handle.vp	= 0;
	msg.returnToSender();
	}


/////////////////
void	Driver::request(	Oscl::FS::File::RO::Access::
							ITC::Req::Api::ReadReq&			msg
							) noexcept{
	Oscl::FS::Volume::Api*				volume;
	Oscl::FS::Volume::RoStateHandle*	handle;
	RoFileState*						rofs;

	rofs	= lookupRoState(msg._payload._handle.vp);
	if(!rofs){
		RwFileState*	rwfs;
		rwfs	= lookupRwState(msg._payload._handle.vp);
		if(!rwfs){
			msg._payload._result	=	&Oscl::FS::File::RO::Access::Read::
										Status::getNoSuchHandleError();
			msg.returnToSender();
			return;
			}
		volume	= &rwfs->_volume;
		handle	= &rwfs->_handle;
		}
	else {
		volume	= &rofs->_volume;
		handle	= &rofs->_handle;
		}
	msg._payload._result	= 
	volume->read(	*handle,
					msg._payload._dest,
					msg._payload._maxSize,
					msg._payload._offset,
					msg._payload._nRead
					);
	msg.returnToSender();
	}

/////////////////
void	Driver::request(	Oscl::FS::File::RW::OC::
							ITC::Req::Api::OpenReq&		msg
							) noexcept{
	Oscl::FS::Volume::Api*
	volume	= lookupVolume(msg._payload._volumeID);
	if(!volume){
		msg._payload._result	=	&Oscl::FS::File::RW::OC::Open::
									Status::getNoSuchVolumeError();
		msg.returnToSender();
		return;
		}
	RwFileState*
	fs	= new(&msg._payload._state)RwFileState(*volume);

	const Oscl::FS::File::RW::OC::Open::Status::Result*
	result	= volume->open(	fs->_stateMem,
							msg._payload._path,
							fs->_handle
							);
	if(result){
		msg._payload._result	= result;
		msg.returnToSender();
		return;
		}
	_rwState.put(fs);
	msg._payload._handle.vp	= fs;
	msg.returnToSender();
	}

void	Driver::request(	Oscl::FS::File::RW::OC::
							ITC::Req::Api::CloseReq&		msg
							) noexcept{
	if(!msg._payload._handle.vp){
		msg._payload._result	=	&Oscl::FS::File::RW::OC::Close::
									Status::getNoSuchHandleError();
		msg.returnToSender();
		return;
		}
	RwFileState*	fs	= (RwFileState*)msg._payload._handle.vp;
	if(!_rwState.remove(fs)){
		msg._payload._result	=	&Oscl::FS::File::RW::OC::Close::
									Status::getNoSuchHandleError();
		msg.returnToSender();
		return;
		}
	msg._payload._result	= fs->_volume.close(fs->_handle);
	msg._payload._handle.vp	= 0;
	msg.returnToSender();
	}


/////////////////
void	Driver::request(	Oscl::FS::File::RW::Access::
							ITC::Req::Api::WriteReq&		msg
							) noexcept{
	RwFileState*	fs;
	fs	= lookupRwState(msg._payload._handle.vp);
	if(!fs){
		msg._payload._result	=	&Oscl::FS::File::RW::Access::Write::
									Status::getNoSuchHandleError();
		msg.returnToSender();
		return;
		}

	msg._payload._result	= 
	fs->_volume.write(	fs->_handle,
						msg._payload._src,
						msg._payload._length,
						msg._payload._offset,
						msg._payload._nWritten
						);
	msg.returnToSender();
	}

void	Driver::request(	Oscl::FS::File::RW::Access::
							ITC::Req::Api::TruncateReq&		msg
							) noexcept{
	RwFileState*	fs;
	fs	= lookupRwState(msg._payload._handle.vp);
	if(!fs){
		msg._payload._result	=	&Oscl::FS::File::RW::Access::Trunc::
									Status::getNoSuchHandleError();
		msg.returnToSender();
		return;
		}

	msg._payload._result	= 
	fs->_volume.truncate(	fs->_handle,
							msg._payload._size
							);
	msg.returnToSender();
	}


/////////////////
void	Driver::request(	Oscl::FS::Volume::Monitor::
							ITC::Req::Api::MountReq&			msg
							) noexcept{
	if(lookupVolume(msg._payload._volumeID)){
		msg.returnToSender();
		return;
		}
	_mountWaiters.put(&msg);
	}

void	Driver::request(	Oscl::FS::Volume::Monitor::
							ITC::Req::Api::UnmountReq&		msg
							) noexcept{
	if(!lookupVolume(msg._payload._volumeID)){
		msg.returnToSender();
		return;
		}
	_unmountWaiters.put(&msg);
	}

void	Driver::request(	Oscl::FS::Volume::Monitor::
							ITC::Req::Api::CancelReq&		msg
							) noexcept{
	{
		Oscl::FS::Volume::Monitor::ITC::Req::Api::MountReq*	next;
		for(next=_mountWaiters.first();next;next=_mountWaiters.next(next)){
			Oscl::Mt::Itc::SrvMsg*	smsg	= next;
			if(smsg == &msg._payload._msgToCancel){
				_mountWaiters.remove(next);
				next->returnToSender();
				msg.returnToSender();
				return;
				}
			}
		}
	{
		Oscl::FS::Volume::Monitor::ITC::Req::Api::UnmountReq*	next;
		for(next=_unmountWaiters.first();next;next=_unmountWaiters.next(next)){
			Oscl::Mt::Itc::SrvMsg*	smsg	= next;
			if(smsg == &msg._payload._msgToCancel){
				_unmountWaiters.remove(next);
				next->returnToSender();
				msg.returnToSender();
				return;
				}
			}
		}
	}

/////////////////
void	Driver::request(	Oscl::FS::Dir::WO::
							ITC::Req::Api::MoveReq&	msg
							) noexcept{
	Oscl::FS::Volume::Api*
	volume	= lookupVolume(msg._payload._volume);
	if(!volume){
		msg._payload._result	=	&Oscl::FS::Dir::WO::Move::
									Status::getNoSuchVolume();
		msg.returnToSender();
		return;
		}
	msg._payload._result	= volume->move(	msg._payload._srcPath,
											msg._payload._destPath
											);
	msg.returnToSender();
	}

void	Driver::request(	Oscl::FS::Dir::WO::
							ITC::Req::Api::CopyReq&	msg
							) noexcept{
	Oscl::FS::Volume::Api*
	srcVolume	= lookupVolume(msg._payload._srcVolume);
	if(!srcVolume){
		msg._payload._result	=	&Oscl::FS::Dir::WO::Copy::
									Status::getNoSuchSrcVolume();
		msg.returnToSender();
		return;
		}
	Oscl::FS::Volume::Api*
	destVolume	= lookupVolume(msg._payload._destVolume);
	if(!destVolume){
		msg._payload._result	=	&Oscl::FS::Dir::WO::Copy::
									Status::getNoSuchDestVolume();
		msg.returnToSender();
		return;
		}
	for(;;);
	}

void	Driver::request(	Oscl::FS::Dir::WO::
							ITC::Req::Api::CreateFileReq&	msg
							) noexcept{
	Oscl::FS::Volume::Api*
	volume	= lookupVolume(msg._payload._volume);
	if(!volume){
		msg._payload._result	=	&Oscl::FS::Dir::WO::Create::
									Status::getNoSuchVolume();
		msg.returnToSender();
		return;
		}
	msg._payload._result	= volume->createFile(msg._payload._path);
	msg.returnToSender();
	}

void	Driver::request(	Oscl::FS::Dir::WO::
							ITC::Req::Api::CreateDirReq&	msg
							) noexcept{
	Oscl::FS::Volume::Api*
	volume	= lookupVolume(msg._payload._volume);
	if(!volume){
		msg._payload._result	=	&Oscl::FS::Dir::WO::Create::
									Status::getNoSuchVolume();
		msg.returnToSender();
		return;
		}
	msg._payload._result	= volume->createDir(msg._payload._path);
	msg.returnToSender();
	}

void	Driver::request(	Oscl::FS::Dir::WO::
							ITC::Req::Api::RemoveReq&	msg
							) noexcept{
	Oscl::FS::Volume::Api*
	volume	= lookupVolume(msg._payload._volume);
	if(!volume){
		msg._payload._result	=	&Oscl::FS::Dir::WO::Remove::
									Status::getNoSuchVolume();
		msg.returnToSender();
		return;
		}
	msg._payload._result	= volume->remove(msg._payload._path);
	msg.returnToSender();
	}

/////////////////
Driver::RoFileState*	Driver::lookupRoState(void* handle) noexcept{
	RoFileState*	next;
	for(next=_roState.first();next;next=_roState.next(next)){
		void*	list=next;
		if(list == handle){
			return next;
			}
		}
	return 0;
	}

Driver::RwFileState*	Driver::lookupRwState(void* handle) noexcept{
	RwFileState*	next;
	for(next=_rwState.first();next;next=_rwState.next(next)){
		void*	list=next;
		if(list == handle){
			return next;
			}
		}
	return 0;
	}

Oscl::FS::Volume::Api*
Driver::lookupVolume(const Oscl::ObjectID::RO::Api& volumeID) noexcept{
	Oscl::FS::Volume::Api*	next;
	for(next=_activeVolumes.first();next;next=_activeVolumes.next(next)){
		if(next->location() == volumeID){
			return next;
			}
		}
	return 0;
	}

void	Driver::processVolumeMountWaiters() noexcept{
	Oscl::Queue<	Oscl::FS::Volume::Monitor::
					ITC::Req::Api::MountReq
					>	qCopy;
	qCopy	= _mountWaiters;
	Oscl::FS::Volume::Monitor::ITC::Req::Api::MountReq*	next;
	while((next=qCopy.get())){
		if(lookupVolume(next->_payload._volumeID)){
			next->returnToSender();
			}
		else {
			_mountWaiters.put(next);
			}
		}
	}

void	Driver::processVolumeUnmountWaiters() noexcept{
	Oscl::Queue<	Oscl::FS::Volume::Monitor::
					ITC::Req::Api::UnmountReq
					>	qCopy;
	qCopy	= _unmountWaiters;
	Oscl::FS::Volume::Monitor::ITC::Req::Api::UnmountReq*	next;
	while((next=qCopy.get())){
		if(lookupVolume(next->_payload._volumeID)){
			_unmountWaiters.put(next);
			}
		else {
			next->returnToSender();
			}
		}
	}
