/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_fs_service_configh_
#define _oscl_fs_service_configh_
#include <new>
#include "driver.h"
#include "oscl/fs/volume/mounter/monitor.h"
#include "oscl/error/fatal.h"
#include "oscl/kernel/mmu.h"
#include "oscl/extalloc/api.h"

/** */
namespace Oscl {
/** */
namespace FS {
/** */
namespace Service {

/**	The purpose of this template class is to
	provide a configurable means of static allocation
	for the Monitor.
 */
template< unsigned nMounters >
class Config : public Driver {
	private:
		/** Memory for the actual monitor. The monitor is
			initialized (a la "new") in the body of the
			constructor such that "complex" memory calculations
			can be performed and dynamic memory allocated.
		 */
		Oscl::Memory::
		AlignedBlock<	sizeof(	Oscl::FS::Volume::
								Mounter::Monitor
								)
						>			_monMem[nMounters];

		/** */
		Oscl::ExtAlloc::Record					_dmaMemRec;

		/** */
		Oscl::Queue<Oscl::FS::Volume::Mounter::Monitor>	_activeMonitors;
		
	public:
		/** */
		Config(		Oscl::Mt::Itc::Dyn::Adv::Cli::
					SyncApi<	Oscl::Block::RW::
								Dyn::Service
								>&							advFind,
					Oscl::Mt::Itc::Dyn::Adv::Cli::
					Req::Api<	Oscl::Block::RW::
								Dyn::Service
								>::SAP&						advSAP,
					Oscl::Mt::Itc::PostMsgApi&				myPapi,
					Oscl::ExtAlloc::Api&					dmaMemAllocator
					) noexcept;
		/** */
		virtual ~Config(){}
		/** */
		void	start() noexcept;
	};

template<unsigned nMounters>
Config<	nMounters
		>::Config(	Oscl::Mt::Itc::Dyn::Adv::Cli::
					SyncApi<	Oscl::Block::RW::
								Dyn::Service
								>&							advFind,
					Oscl::Mt::Itc::Dyn::Adv::Cli::
					Req::Api<	Oscl::Block::RW::
								Dyn::Service
								>::SAP&						advSAP,
					Oscl::Mt::Itc::PostMsgApi&				myPapi,
					Oscl::ExtAlloc::Api&					dmaMemAllocator
					) noexcept:
		Driver(myPapi)
		{
	const unsigned long	pageSize			= OsclKernelGetPageSize();
	const unsigned long	almostPageSize		= pageSize-1;

	const unsigned long	monitorDmaMemSize		=
		(sizeof(Oscl::FS::Volume::Mounter::Monitor::DmaMem));

	const unsigned long	dmaMemSize		= monitorDmaMemSize;

	const unsigned long	nPages			=	(dmaMemSize+almostPageSize)/pageSize;
	const unsigned long	monAllocSize	=	pageSize*nPages;
	const unsigned long	allocSize		=	monAllocSize*nMounters;

	if(!dmaMemAllocator.alloc(_dmaMemRec,allocSize,~almostPageSize)){
		Oscl::ErrorFatal::logAndExit(
			"Oscl::FS::Volume::Mounter::Config: cant allocate dma buffers."
			);
		}

	unsigned char*	monitorDmaMem	= (unsigned char*)_dmaMemRec.getFirstUnit();

	for(unsigned i=0;i<nMounters;++i){
		Oscl::FS::Volume::Mounter::Monitor::DmaMem*
		mem	= (Oscl::FS::Volume::Mounter::Monitor::DmaMem*)&monitorDmaMem[i*monAllocSize];
		Oscl::FS::Volume::Mounter::Monitor*
		monitor	= new(&_monMem[i])
					Oscl::FS::Volume::Mounter::
					Monitor(	advFind,
								advSAP,
								myPapi,
								*this,
								*mem
								);
		_activeMonitors.put(monitor);
		}
	}

template<unsigned nMounters>
void Config<nMounters>::start() noexcept{
	Oscl::FS::Volume::Mounter::Monitor*	next;
	for(next=_activeMonitors.first();next;next=_activeMonitors.next(next)){
		next->syncOpen();
		}
	}

}
}
}

#endif
