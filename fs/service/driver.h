/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_fs_service_driverh_
#define _oscl_fs_service_driverh_
#include "oscl/queue/queue.h"
#include "oscl/fs/volume/mounter/context.h"
#include "oscl/fs/file/ro/oc/itc/sync.h"
#include "oscl/fs/file/ro/access/itc/sync.h"
#include "oscl/fs/file/rw/oc/itc/sync.h"
#include "oscl/fs/file/rw/access/itc/sync.h"
#include "oscl/fs/volume/monitor/itc/reqapi.h"
#include "oscl/fs/volume/monitor/itc/sync.h"
#include "oscl/fs/volume/state.h"
#include "oscl/fs/dir/wo/itc/sync.h"

/** */
namespace Oscl {
/** */
namespace FS {
/** */
namespace Service {

/** This class runs in the same thread with the volume
	mounters and is responsible for the overall operation
	and synchronization of the File System. Within this
	same thread, all file access operations will be serialized
	to provide atomic operations.
 */
class Driver :	public	Oscl::FS::Volume::Mounter::ContextApi,
				private	Oscl::FS::File::RO::OC::ITC::Req::Api,
				private	Oscl::FS::File::RO::Access::ITC::Req::Api,
				private	Oscl::FS::File::RW::OC::ITC::Req::Api,
				private	Oscl::FS::File::RW::Access::ITC::Req::Api,
				private	Oscl::FS::Volume::Monitor::ITC::Req::Api,
				private	Oscl::FS::Dir::WO::ITC::Req::Api
				{
	private:
		/** */
		class RoFileState {
			public:
				/** */
				void*							__qitemlink;
				/** */
				Oscl::FS::Volume::Api& 			_volume;
				/** */
				Oscl::FS::Volume::RoStateMem	_stateMem;
				/** */
				Oscl::FS::Volume::RoStateHandle	_handle;
			public:
				/** */
				inline RoFileState(Oscl::FS::Volume::Api& volume) noexcept:
					_volume(volume){}
			};
		/** */
		class RwFileState {
			public:
				/** */
				void*							__qitemlink;
				/** */
				Oscl::FS::Volume::Api& 			_volume;
				/** */
				Oscl::FS::Volume::RwStateMem	_stateMem;
				/** */
				Oscl::FS::Volume::RwStateHandle	_handle;
			public:
				/** */
				inline RwFileState(Oscl::FS::Volume::Api& volume) noexcept:
					_volume(volume){}
			};
	private:
		/** */
		Oscl::FS::Volume::Monitor::ITC::Sync	_volumeMonitorSync;
		/** */
		Oscl::FS::Volume::Monitor::
		ITC::Req::Api::ConcreteSAP				_volumeMonitorSAP;
		/** */
		Oscl::FS::File::RO::OC::ITC::Sync		_fileRoOcSync;
		/** */
		Oscl::FS::File::RO::Access::ITC::Sync	_fileRoAccessSync;
		/** */
		Oscl::FS::File::RW::OC::ITC::Sync		_fileRwOcSync;
		/** */
		Oscl::FS::File::RW::Access::ITC::Sync	_fileRwAccessSync;
		/** */
		Oscl::FS::Dir::WO::ITC::Sync			_dirWoSync;
		/** */
		Oscl::Queue<Oscl::FS::Volume::Api>	_activeVolumes;
		/** */
		Oscl::Queue<	Oscl::FS::Volume::Monitor::
						ITC::Req::Api::MountReq
						>	_mountWaiters;
		/** */
		Oscl::Queue<	Oscl::FS::Volume::Monitor::
						ITC::Req::Api::UnmountReq
						>	_unmountWaiters;
	private:
		/** */
		Oscl::Queue<RoFileState>	_roState;
		/** */
		Oscl::Queue<RwFileState>	_rwState;

	public:
		/** */
		Driver(Oscl::Mt::Itc::PostMsgApi& myPapi) noexcept;

		/** */
		Oscl::FS::Volume::Monitor::
		ITC::Req::Api::SAP&			getVolumeMonitorSAP() noexcept;
		/** */
		Oscl::FS::File::RO::OC::
		ITC::Req::Api::SAP&			getFileRoOcSAP() noexcept;
		/** */
		Oscl::FS::File::RO::Access::
		ITC::Req::Api::SAP&			getFileRoAccessSAP() noexcept;
		/** */
		Oscl::FS::File::RW::OC::
		ITC::Req::Api::SAP&			getFileRwOcSAP() noexcept;
		/** */
		Oscl::FS::File::RW::Access::
		ITC::Req::Api::SAP&			getFileRwAccessSAP() noexcept;
		/** */
		Oscl::FS::Dir::WO::
		ITC::Req::Api::SAP&			getDirWoSAP() noexcept;
		/** */
		Oscl::FS::Volume::Monitor::Api&		getVolumeMonitorSync() noexcept;
		/** */
		Oscl::FS::File::RO::OC::Api&		getFileRoOcSync() noexcept;
		/** */
		Oscl::FS::File::RO::Access::Api&	getFileRoAccessSync() noexcept;
		/** */
		Oscl::FS::File::RW::OC::Api&		getFileRwOcSync() noexcept;
		/** */
		Oscl::FS::File::RW::Access::Api&	getFileRwAccessSync() noexcept;
		/** */
		Oscl::FS::Dir::WO::Api&				getDirWoSync() noexcept;

	private:
		/** */
		void	mount(Oscl::FS::Volume::Api& volume) noexcept;
		/** */
		void	umount(Oscl::FS::Volume::Api& volume) noexcept;

	private:	// Oscl::FS::File::RO::OC::Req::Api
		/** */
		void	request(	Oscl::FS::File::RO::OC::
							ITC::Req::Api::OpenReq&		msg
							) noexcept;
		/** */
		void	request(	Oscl::FS::File::RO::OC::
							ITC::Req::Api::CloseReq&		msg
							) noexcept;

	public:	// Oscl::FS::File::RO::Access::Req::Api
		/** */
		void	request(	Oscl::FS::File::RO::Access::
							ITC::Req::Api::ReadReq&			msg
							) noexcept;

	private:	// Oscl::FS::File::RW::OC::Req::Api
		/** */
		void	request(	Oscl::FS::File::RW::OC::
							ITC::Req::Api::OpenReq&		msg
							) noexcept;
		/** */
		void	request(	Oscl::FS::File::RW::OC::
							ITC::Req::Api::CloseReq&		msg
							) noexcept;

	private:	// Oscl::FS::File::RW::Access::Req::Api
		/** */
		void	request(	Oscl::FS::File::RW::Access::
							ITC::Req::Api::WriteReq&		msg
							) noexcept;
		/** */
		void	request(	Oscl::FS::File::RW::Access::
							ITC::Req::Api::TruncateReq&		msg
							) noexcept;

	public:	// Oscl::FS::Volume::Monitor::ITC::Req::Api
		/** */
		void	request(	Oscl::FS::Volume::Monitor::
							ITC::Req::Api::MountReq&		msg
							) noexcept;
		/** */
		void	request(	Oscl::FS::Volume::Monitor::
							ITC::Req::Api::UnmountReq&		msg
							) noexcept;
		/** */
		void	request(	Oscl::FS::Volume::Monitor::
							ITC::Req::Api::CancelReq&		msg
							) noexcept;
	public:	// Oscl::FS::Dir::WO::ITC::Req::Api
		/** */
		void	request(	Oscl::FS::Dir::WO::
							ITC::Req::Api::MoveReq&	msg
							) noexcept;
		/** */
		void	request(	Oscl::FS::Dir::WO::
							ITC::Req::Api::CopyReq&	msg
							) noexcept;
		/** */
		void	request(	Oscl::FS::Dir::WO::
							ITC::Req::Api::CreateFileReq&	msg
							) noexcept;
		/** */
		void	request(	Oscl::FS::Dir::WO::
							ITC::Req::Api::CreateDirReq&	msg
							) noexcept;
		/** */
		void	request(	Oscl::FS::Dir::WO::
							ITC::Req::Api::RemoveReq&	msg
							) noexcept;
	private:
		/** */
		RoFileState*	lookupRoState(void* handle) noexcept;
		/** */
		RwFileState*	lookupRwState(void* handle) noexcept;
		/** */
		Oscl::FS::Volume::Api*	lookupVolume(const Oscl::ObjectID::RO::Api& volumeID) noexcept;
		/** */
		void	processVolumeMountWaiters() noexcept;
		/** */
		void	processVolumeUnmountWaiters() noexcept;
	};

}
}
}

#endif
