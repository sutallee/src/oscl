/*
   Copyright (C) 2020 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_fs_file_apih_
#define _oscl_fs_file_apih_

#include <sys/types.h>

/** */
namespace Oscl {
/** */
namespace FS {
/** */
namespace File {

/**	This abstract class is used to operate on a particular file.
 */
class Api {
	public:
		/**	Reads items of data of size bytes long.
			RETURN:	
			    Number of bytes read.
				On success, it will be equal to number of items
				requested to be read.
				Returns less than number of bytes requested if
				there are not enough bytes available in file.
				Will return -ERRNO code on error.
		 */
		virtual ssize_t	read(
							void*	ptr,
							size_t	size
							) noexcept=0;

		/**	Writes items of data of size bytes long.
			RETURN:
				Number of bytes written.
				On success, it will be equal to the number
				of bytes requested to be written.
				Any other value, indicates an error.
				Will return -ERRNO code on error.
				In the case where -ERRNO is returned, the file pointer
				will not be advanced because it couldn’t start the operation.
				In the case where it is able to write, but is not able to
				complete writing all of the requested number of bytes,
				then it is because the disk got full.
				In that case, it returns less number of bytes written
				than requested, but not a negative -ERRNO value as
				in regular error case.
		 */
		virtual ssize_t	write(
							const void*	ptr,
							size_t		size
							) noexcept=0;

		/**	Moves the file position to a new location in the file.
			The offset is added to beginning-of-file position.

			RETURN:
        		0: Success
        		-ERRNO: errno code if error.
		 */
		virtual int	seekFromBeginning(
						off_t	offset
						) noexcept=0;

		/**	Moves the file position to a new location in the file.
			The offset is added to current-file position.

			RETURN:
        		0: Success
        		-ERRNO: errno code if error.
		 */
		virtual int	seekFromCurrentPosition(
						off_t	offset
						) noexcept=0;

		/**	Moves the file position to a new location in the file.
			The offset is added to end-of-file position.

			RETURN:
        		0: Success
        		-ERRNO: errno code if error.
		 */
		virtual int	seekFromEnd(
						off_t	offset
						) noexcept=0;

		/**	Retrieves the current position in the file.
			RETURN:
				Current position in file.
				WARNING: Current revision does not validate the file object.
		 */
		virtual off_t	tell() noexcept=0;

		/**	Truncates the file to the new length if it is
			shorter than the current size of the file.
			Expands the file if the new length is greater
			than the current size of the file.
			The expanded region would be filled with zeroes.

			RETURN:
        		0: Success
        		-ERRNO: errno code if error.
		 */
		virtual int	truncate(off_t length) noexcept=0;

		/**	This function can be used to flush the cache
			of an open file.
			This can be called to ensure data gets written
			to the storage media immediately.
			This may be done to avoid data loss if power
			is removed unexpectedly.
			Note that closing a file will cause caches
			to be flushed correctly so it need not be
			called if the file is being closed.

			RETURN:
        		0: Success
        		-ERRNO: errno code if error.
		 */
		virtual int	sync() noexcept=0;
	};

}
}
}

#endif
