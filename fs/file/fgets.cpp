/*
   Copyright (C) 2025 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "fgets.h"

char*	Oscl::FS::File::fgets(
			char*					s,
			int						size,
			Oscl::FS::File::Api&	file
			){
	int	offset;

	for(
		offset=0;
		offset < (size-1);
		++offset
		){
		ssize_t
		n	= file.read(
				&s[offset],
				1
				);

		if(n < 1){
			s[offset]	= '\0';
			break;
			}
		if(s[offset] == '\n'){
			break;
			}
		}

	s[offset+1]	= 0;

	return s[offset]?s:0;
	}

