/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_fs_file_rw_oc_itc_reqapih_
#define _oscl_fs_file_rw_oc_itc_reqapih_
#include "oscl/mt/itc/mbox/srvreq.h"
#include "oscl/mt/itc/mbox/sap.h"
#include "oscl/fs/file/rw/oc/state.h"
#include "oscl/fs/file/rw/handle.h"
#include "oscl/oid/roapi.h"

/** */
namespace Oscl {
/** */
namespace FS {
/** */
namespace File {
/** */
namespace RW {
/** */
namespace OC {

/** */
namespace Open {
/** */
namespace Status{
/** */
class Result;
}
}

/** */
namespace Close {
/** */
namespace Status{
/** */
class Result;
}
}


/** */
namespace ITC {
/** */
namespace Req {

using namespace Oscl::Mt::Itc;

/** */
class Api {
	public:
		/** */
		virtual ~Api() {}
		/** */
		class OpenPayload {
			public:
				/** */
				State&							_state;
				/** */
				const Oscl::ObjectID::RO::Api&	_volumeID;
				/** */
				const char*						_path;
				/** */
				Handle&							_handle;
				/** Result pointer is ZERO if the operation completed
					without error. Otherwise, a pointer to result
					code is returned. The reference can be used
					to determine the reason for the failure.
				 */
				const Open::Status::Result*	_result;

			public:
				/** */
				OpenPayload(	State&							state,
								const Oscl::ObjectID::RO::Api&	volumeID,
								const char*						path,
								Handle&							handle
								) noexcept;
			};
		/** */
		class ClosePayload {
			public:
				/** */
				Handle&						_handle;
				/** Result pointer is ZERO if the operation completed
					without error. Otherwise, a pointer to result
					code is returned. The reference can be used
					to determine the reason for the failure.
				 */
				const Close::Status::Result*	_result;

			public:
				/** */
				ClosePayload(Handle& handle) noexcept;
			};
	public:
		/** */
		typedef SrvRequest<Api,OpenPayload>			OpenReq;
		/** */
		typedef SrvRequest<Api,ClosePayload>		CloseReq;

	public:
		/** */
		typedef Oscl::Mt::Itc::SAP<Api>				SAP;
		/** */
		typedef Oscl::Mt::Itc::ConcreteSAP<Api>		ConcreteSAP;

	public:
		/** */
		virtual void	request(OpenReq& msg) noexcept=0;
		/** */
		virtual void	request(CloseReq& msg) noexcept=0;
	};

}
}


}
}
}
}
}

#endif
