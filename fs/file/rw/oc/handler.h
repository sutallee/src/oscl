/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_fs_file_rw_oc_handlerh_
#define _oscl_fs_file_rw_oc_handlerh_

/** */
namespace Oscl {
/** */
namespace FS {
/** */
namespace File {
/** */
namespace RW {
/** */
namespace OC {

/** */
namespace Open {
/** */
namespace Status {

/** */
class Handler {
	public:
		/** Make GCC happy */
		virtual ~Handler() {}
		/** Indicates that the specified volume
			is not writable.
		 */
		virtual void	readOnlyVolume() const noexcept=0;
		/** Indicates that the specified volume
			is not mounted.
		 */
		virtual void	noSuchVolume() const noexcept=0;
		/** Indicates that at least one of the directory
			elements in the path does not exist.
			In this case, the specified volume does exist.
		 */
		virtual void	noSuchDirectory() const noexcept=0;
		/** Indicates that the file element of the path
			does not exist in the directory specified in
			the path.
			In this case, the specified volume and the
			directories in the path (if any) exist, but
			the file itself was not found in the leaf
			directory.
		 */
		virtual void	noSuchFile() const noexcept=0;
		/** */
		virtual void	transportError() const noexcept=0;
	};
}
}

/** */
namespace Close {
/** */
namespace Status {
/** */
class Handler {
	public:
		/** Make GCC happy */
		virtual ~Handler() {}
		/** */
		virtual void	noSuchHandle() const noexcept=0;
		/** */
		virtual void	transportError() const noexcept=0;
	};
}
}


}
}
}
}
}

#endif
