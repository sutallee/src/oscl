/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_fs_file_rw_oc_stateh_
#define _oscl_fs_file_rw_oc_stateh_
#include "oscl/memory/block.h"

/** */
namespace Oscl {
/** */
namespace FS {
/** */
namespace File {
/** */
namespace RW {
/** */
namespace OC {

/** This union defines an memory block who's
	type is opaque to FileSystem users. An object
	of this type given to the File System when
	a file is opened, and the memory occupied by
	the object becomes the property of the
	File System until the file is closed.
	The File System is free to use the memory
	for whatever purpose it desires while the
	file is open. Typically, it will be used
	to cache state information about the file
	on a Mass storage device, and to co-ordinate
	access to the file.
 */
union State {
	/** */
	Oscl::Memory::AlignedBlock<64>	minimal;
	};

}
}
}
}
}

#endif
