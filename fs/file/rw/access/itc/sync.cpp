/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "sync.h"
#include "oscl/mt/itc/mbox/syncrh.h"

using namespace Oscl::FS::File::RW::Access::ITC;

Sync::Sync(	Oscl::FS::File::RW::Access::ITC::Req::Api&	reqApi,
			Oscl::Mt::Itc::PostMsgApi&					myPapi
			) noexcept:
		_sap(reqApi,myPapi)
		{
	}

Oscl::FS::File::RW::Access::ITC::
Req::Api::SAP&	Sync::getSAP() noexcept{
	return _sap;
	}

const Oscl::FS::File::RW::Access::Write::Status::Result*
Sync::write(	Handle&			handle,
				const void*		src,
				unsigned long	length,
				unsigned long	offset,
				unsigned long&	nWritten
				) noexcept{
	Oscl::Mt::Itc::SyncReturnHandler	srh;
	Req::Api::WritePayload	payload(	handle,
										src,
										length,
										offset
										);
	Req::Api::WriteReq		req(	_sap.getReqApi(),
									payload,
									srh
									);
	_sap.postSync(req);
	nWritten	= payload._nWritten;
	return payload._result;
	}

const Oscl::FS::File::RW::Access::Trunc::Status::Result*
Sync::truncate(	Handle&			handle,
				unsigned long	size
				) noexcept{
	Oscl::Mt::Itc::SyncReturnHandler	srh;
	Req::Api::TruncatePayload	payload(	handle,
											size
											);
	Req::Api::TruncateReq		req(	_sap.getReqApi(),
										payload,
										srh
										);
	_sap.postSync(req);
	return payload._result;
	}

