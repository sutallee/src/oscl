/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_fs_file_rw_access_itc_reqapih_
#define _oscl_fs_file_rw_access_itc_reqapih_
#include "oscl/mt/itc/mbox/srvreq.h"
#include "oscl/mt/itc/mbox/sap.h"

/** */
namespace Oscl {
/** */
namespace FS {
/** */
namespace File {
/** */
namespace RW {
/** */
struct Handle;
/** */
namespace Access {

/** */
namespace Write{
/** */
namespace Status{
/** */
class Result;
}
}

/** */
namespace Trunc{
/** */
namespace Status{
/** */
class Result;
}
}

/** */
namespace ITC {
/** */
namespace Req {

using namespace Oscl::Mt::Itc;

/** */
class Api {
	public:
		/** */
		virtual ~Api() {}
		/** */
		class CancelPayload {
			public:
				/**	*/
				Oscl::Mt::Itc::SrvMsg&	_msgToCancel;
			public:
				/** */
				CancelPayload(Oscl::Mt::Itc::SrvMsg& msgToCancel) noexcept;
			};
		/** */
		class WritePayload {
			public:
				/** */
				Handle&				_handle;
				/** */
				const void*	const	_src;
				/** */
				const unsigned long	_length;
				/** */
				const unsigned long	_offset;
				/** */
				unsigned long		_nWritten;
				/** Result pointer is ZERO if the operation completed
					without error. Otherwise, a pointer to result
					code is returned. The reference can be used
					to determine the reason for the failure.
				 */
				const Write::Status::Result*	_result;

			public:
				/** */
				WritePayload(	Handle&			handle,
								const void*		src,
								unsigned long	length,
								unsigned long	offset
								) noexcept;
			};
		/** */
		class TruncatePayload {
			public:
				/** */
				Handle&				_handle;
				/** */
				const unsigned long	_size;
				/** Result pointer is ZERO if the operation completed
					without error. Otherwise, a pointer to result
					code is returned. The reference can be used
					to determine the reason for the failure.
				 */
				const Trunc::Status::Result*	_result;

			public:
				/** */
				TruncatePayload(	Handle&			handle,
									unsigned long	size
									) noexcept;
			};
	public:
		/** */
		typedef SrvRequest<Api,WritePayload>			WriteReq;
		/** */
		typedef SrvRequest<Api,TruncatePayload>			TruncateReq;
		/** */
		typedef SrvRequest<Api,CancelPayload>		CancelReq;

	public:
		/** */
		typedef Oscl::Mt::Itc::SAP<Api>				SAP;
		/** */
		typedef Oscl::Mt::Itc::ConcreteSAP<Api>		ConcreteSAP;

	public:
		/** */
		virtual void	request(WriteReq& msg) noexcept=0;
		/** */
		virtual void	request(TruncateReq& msg) noexcept=0;
		/** */
//		virtual void	request(CancelReq& msg) noexcept=0;
	};

}
}
}
}
}
}
}

#endif
