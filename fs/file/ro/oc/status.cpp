/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "result.h"
#include "status.h"
#include "handler.h"

/** */
namespace Oscl {
/** */
namespace FS {
/** */
namespace File {
/** */
namespace RO {
/** */
namespace OC {

/** */
namespace Open {
/** */
namespace Status {

class TransportError : public Result {
	public:
		void	query(Handler& handler) const noexcept;
	};

void	TransportError::query(Handler& handler) const noexcept{
	handler.transportError();
	}

const Result& getTransportError() noexcept{
	static const TransportError	transportError;
	return transportError;
	}

class NoSuchVolumeError : public Result {
	public:
		void	query(Handler& handler) const noexcept;
	};

void	NoSuchVolumeError::query(Handler& handler) const noexcept{
	handler.noSuchVolume();
	}

const Result& getNoSuchVolumeError() noexcept{
	static const NoSuchVolumeError	noSuchVolumeError;
	return noSuchVolumeError;
	}

class NoSuchDirectoryError : public Result {
	public:
		void	query(Handler& handler) const noexcept;
	};

void	NoSuchDirectoryError::query(Handler& handler) const noexcept{
	handler.noSuchDirectory();
	}

const Result& getNoSuchDirectoryError() noexcept{
	static const NoSuchDirectoryError	noSuchDirectoryError;
	return noSuchDirectoryError;
	}

class NoSuchFileError : public Result {
	public:
		void	query(Handler& handler) const noexcept;
	};

void	NoSuchFileError::query(Handler& handler) const noexcept{
	handler.noSuchFile();
	}

const Result& getNoSuchFileError() noexcept{
	static const NoSuchFileError	noSuchFileError;
	return noSuchFileError;
	}

}
}

/** */
namespace Close {
/** */
namespace Status {

class TransportError : public Result {
	public:
		void	query(Handler& handler) const noexcept;
	};

void	TransportError::query(Handler& handler) const noexcept{
	handler.transportError();
	}

const Result& getTransportError() noexcept{
	static const TransportError	transportError;
	return transportError;
	}

class NoSuchHandleError : public Result {
	public:
		void	query(Handler& handler) const noexcept;
	};

void	NoSuchHandleError::query(Handler& handler) const noexcept{
	handler.noSuchHandle();
	}

const Result& getNoSuchHandleError() noexcept{
	static const NoSuchHandleError	noSuchHandleError;
	return noSuchHandleError;
	}

}
}

}
}
}
}
}
