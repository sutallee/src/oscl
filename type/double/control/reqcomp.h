#ifndef _oscl_type_double_control_reqcomph_
#define _oscl_type_double_control_reqcomph_

#include "reqapi.h"

/** */
namespace Oscl {

/** */
namespace Type {

/** */
namespace Double {

/** */
namespace Control {

/** */
namespace Req {

/**	This template class implements a kind of GOF decorator
	pattern allows the context to employ composition instead
	of inheritance. Frequently, a context may need to implement
	more than one interface that may result in method name
	clashes. This template solves the problem by implementing
	the interface and then invoking member function pointers
	in the context instead. One instance of this object is
	created in the context for each interface of this type used
	in the context. Each instance is constructed such that it
	refers to different member functions within the context.
	The interface of this object is passed to any object that
	requires the interface. When the object invokes any of the
	operations of the API, the corresponding member function
	of the context will subsequently be invoked.
 */
/**	This is the DummyID ITC comment.
 */
template <class Context>
class Composer : public Api {
	private:
		/** A reference to the Context.
		 */
		Context&	_context;

	private:
		/**	This message requests to the server to set the new value
			of the double argument.
		 */
		void	(Context::*_Set)(Oscl::Type::Double::Control::Req::Api::SetReq& msg);

	public:
		/** */
		Composer(
			Context&		context,
			void	(Context::*Set)(Oscl::Type::Double::Control::Req::Api::SetReq& msg)
			) noexcept;

	private:
		/**	This message requests to the server to set the new value
			of the double argument.
		 */
		void	request(Oscl::Type::Double::Control::Req::Api::SetReq& msg) noexcept;

	};

template <class Context>
Composer<Context>::Composer(
			Context&		context,
			void	(Context::*Set)(Oscl::Type::Double::Control::Req::Api::SetReq& msg)
			) noexcept:
		_context(context),
		_Set(Set)
		{
	}

template <class Context>
void	Composer<Context>::request(Oscl::Type::Double::Control::Req::Api::SetReq& msg) noexcept{
	(_context.*_Set)(msg);
	}

}
}
}
}
}
#endif
