/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_bignum_valueh_
#define _oscl_bignum_valueh_
#include <stdint.h>

/** */
namespace Oscl {
/** */
namespace BigNum {

#if 1
// real    0m7.722s
// user    0m7.320s
// sys     0m0.010s
//
// real    0m7.551s
// user    0m7.320s
// sys     0m0.010s
//
// real    0m7.782s
// user    0m7.350s
// sys     0m0.010s
//
// real    0m8.530s
// user    0m7.500s
// sys     0m0.020s
//
// real    0m7.635s
// user    0m7.320s
// sys     0m0.000s

typedef uint16_t	FragType;
typedef uint32_t	DoubleFragType;
#else
// real    0m10.584s
// user    0m10.180s
// sys     0m0.010s
//
// real    0m10.744s
// user    0m10.170s
// sys     0m0.010s
//
// real    0m10.390s
// user    0m10.170s
// sys     0m0.010s
typedef uint32_t	FragType;
typedef uint16_t	DoubleFragType;
#endif

enum{nBytesPerFrag=(sizeof(FragType))};
enum{nFragBits=(nBytesPerFrag*8)};

/** */
template <unsigned size>
class Value {
		/** */
		friend class Value<size*2>;
		/** */
		friend class Value<size/2>;
	private:
		/** */
		FragType	_value[size];
	public:
		/** */
		Value() noexcept;
		/** */
		Value(	const void*	value,
				unsigned	length
				) noexcept;
		/** */
		Value(const FragType value[size]) noexcept;
		/** */
		Value(const Value& value) noexcept;
		/** */
		Value(const Value<size/2>& value) noexcept;
		/** */
		explicit Value(const Value<size*2>& value) noexcept;
		/** Serialize the value into a buffer in network
			byte order.
			Returns the number of octets written into the buffer.
		 */
		unsigned serialize(void* buffer, unsigned maxLength) const noexcept;
		// Assignment
		/** */
		Value&	operator = (const Value& value) noexcept;
		/** */
		Value&	operator = (unsigned value) noexcept;
		// Shift
		/** */
		Value&	operator <<= (unsigned value) noexcept;
		/** */
		Value&	operator >>= (unsigned value) noexcept;
		/** */
		Value	operator << (unsigned value) noexcept;
		/** */
		Value	operator >> (unsigned value) noexcept;
		// Comparison
		/** */
		bool	operator < (const Value& other) const noexcept;
		/** */
		bool	operator <= (const Value& other) const noexcept;
		/** */
		bool	operator > (const Value& other) const noexcept;
		/** */
		bool	operator >= (const Value& other) const noexcept;
		/** */
		bool	operator == (const Value& other) const noexcept;
		/** */
		bool	operator != (const Value& other) const noexcept;
		// Arithmetic
		/** */
		Value	operator ~ () const noexcept;
		/** Prefix increment */
		Value	operator ++ () noexcept;
		/** Prefix decrement */
		Value	operator -- () noexcept;
		/** Postfix increment */
		Value	operator ++ (int) noexcept;
		/** Postfix decrement */
		Value	operator -- (int) noexcept;
		/** Change sign */
		Value	operator - () noexcept;
		/** */
		Value&	operator += (const Value& value) noexcept;
		/** */
		Value&	operator -= (const Value& value) noexcept;
		/** */
		Value&	operator %= (const Value<size/2>& value) noexcept;
		/** */
		Value&	operator *= (const Value<size/2> value) noexcept;
		/** */
		Value&	powXmodY(const Value& pow,const Value& mod) noexcept;
		/** */
		Value&	pow(const Value& pow) noexcept;
		/** */
		Value&	squareModY(const Value& mod) noexcept;
		/** */
		Value<size*2>	square() noexcept;
		/** */
		bool	msb() const noexcept;
		/** */
		bool	lsb() const noexcept;
		/** */
		unsigned	ms1Bit() const noexcept;
	};

template <unsigned size>
Value<size>::Value() noexcept
		{
	}

template <unsigned size>
Value<size>::Value(	const void* v,
					unsigned	length
					) noexcept
		{
	const unsigned char*	value	= (const unsigned char*)v;
	unsigned	nFrags	= length/nBytesPerFrag;
	unsigned	nExtra	= length%nBytesPerFrag;
	if(nFrags>size){
		nFrags	= size;
		nExtra	= 0;
		}
	const unsigned char*	p	= &value[length-1];
	FragType	frag=0;
	for(unsigned i=0;i<nFrags;++i){
		p	-= nBytesPerFrag;
		for(unsigned j=0;j<nBytesPerFrag;++j){
			frag	<<= 8;
			frag	|= p[j+1];
			}
		_value[(size-1)-i]	= frag;
		}
	if(nExtra){
		frag=0;
		for(unsigned i=0;i<nExtra;++i,--p){
			frag	|= *p;
			frag	<<= 8;
			}
		_value[(size-1)-nFrags]	= frag;
		}
	unsigned	nZeroFill	= size-nFrags;
	if(nZeroFill && nExtra){
		nZeroFill	-= 1;
		}
	for(unsigned i=0;i<nZeroFill;++i){
		_value[i]	= 0;
		}
	}

template <unsigned size>
Value<size>::Value(const FragType value[size]) noexcept
		{
	for(unsigned i=0;i<size;++i){
		_value[i]	= value[i];
		}
	}

template <unsigned size>
Value<size>::Value(const Value<size/2>& value) noexcept
		{
	FragType*	p	= _value;
	for(unsigned i=0;i<(size/2);++i,++p){
		*p	= 0;
		}
	for(unsigned i=0;i<(size/2);++i,++p){
		*p	= value._value[i];
		}
	}

template <unsigned size>
Value<size>::Value(const Value<size*2>& value) noexcept
		{
	const FragType*	p	= &value._value[size];
	for(unsigned i=0;i<size;++i,++p){
		_value[i]	= *p;
		}
	}

template <unsigned size>
Value<size>::Value(const Value& value) noexcept
		{
	for(unsigned i=0;i<size;++i){
		_value[i]	= value._value[i];
		}
	}

template <unsigned size>
Value<size>&	Value<size>::operator = (const Value<size>& value) noexcept
		{
	for(unsigned i=0;i<size;++i){
		_value[i]	= value._value[i];
		}
	return *this;
	}

template <unsigned size>
unsigned Value<size>::serialize(void* buffer, unsigned maxLength) const noexcept{
	unsigned char*	p	= (unsigned char*)buffer;
	const unsigned	sizeofData	= size*nBytesPerFrag;
	unsigned	length	= (maxLength > sizeofData)?sizeofData:maxLength;
	unsigned	nFrags	= length/nBytesPerFrag;
	for(unsigned i=0;i<nFrags;++i,p+=nBytesPerFrag){
		FragType	frag	= _value[i];
		for(unsigned j=0;j<nBytesPerFrag;++j){
			p[(nBytesPerFrag-1)-j]	= frag;
			frag >>= 8;
			}
		}
	unsigned nResidual	= length%nBytesPerFrag;
	if(nResidual){
		FragType	frag	= _value[nFrags];
		for(unsigned i=0;i<nResidual;++i){
			p[i]	= frag<<(8*((nBytesPerFrag-1)-i));
			}
		}

	return length;
	}

//
// value=19
// nFrags	= 19/8 = 2
// nBits	= 19%8 = 3
// nMove	= size-nFrags = 6-2-1 = 3
// size		= 6 bytes;
//																						src
//										src[0]			src[1]
//         0               1               2               3               4               5
// |-+-+-+-+-+-+-+-|-+-+-+-+-+-+-+-|-+-+-+-+-+-+-+-|-+-+-+-+-+-+-+-|-+-+-+-+-+-+-+-|-+-+-+-+-+-+-+-|
//  a b c d e f g h i j k l m n o p q s t u v w x y z A B C D E F G H J K L M N O P Q S T U V W X Y 
// |-+-+-+-+-+-+-+-|-+-+-+-+-+-+-+-|-+-+-+-+-+-+-+-|-+-+-+-+-+-+-+-|-+-+-+-+-+-+-+-|-+-+-+-+-+-+-+-|
//  u v w x y z A B C D E F G H J K L M N O P Q S T U V W X Y 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
// |-+-+-+-+-+-+-+-|-+-+-+-+-+-+-+-|-+-+-+-+-+-+-+-|-+-+-+-+-+-+-+-|-+-+-+-+-+-+-+-|-+-+-+-+-+-+-+-|
//														dest
//		dest

template <unsigned size>
inline Value<size>&	Value<size>::operator <<= (unsigned value) noexcept {
	unsigned	nFrags	= value/nFragBits;
	unsigned	nBits	= value%nFragBits;
	if(nFrags >= size){
		*this	= 0;
		return *this;
		}
	unsigned			nMove	= size-nFrags-1;
	FragType*		src	= &_value[nFrags];
	FragType*		dest= &_value[0];
	for(unsigned i=0;i<nMove;++i){
		DoubleFragType	source	= src[0];
		source	<<= nFragBits;
		source	|=	src[1];
		source	>>= (nFragBits-nBits);
		*dest	= (FragType)(source);
		++src;
		++dest;
		}
	*dest	= src[0];
	*dest	<<= nBits;
	++dest;
	for(unsigned i=0;i<nFrags;++i){
		*dest	= 0;
		++dest;
		}
	return *this;
	}

template <unsigned size>
inline Value<size>	Value<size>::operator << (unsigned value) noexcept {
	Value<size>	result(*this);
	result <<= value;
	return result;
	}

//
// value=19
// nFrags	= 19/nFragBits = 2
// nBits	= 19%nFragBits = 3
// nMove	= size-nFrags-1	= 6-2-1 = 3
// size		= 6 bytes;
//										src[0]			src[1]
//		src[0]			src[1]
//		src[1]
//         0               1               2               3               4               5
// |-+-+-+-+-+-+-+-|-+-+-+-+-+-+-+-|-+-+-+-+-+-+-+-|-+-+-+-+-+-+-+-|-+-+-+-+-+-+-+-|-+-+-+-+-+-+-+-|
//  a b c d e f g h i j k l m n o p q s t u v w x y z A B C D E F G H J K L M N O P Q S T U V W X Y 
// |-+-+-+-+-+-+-+-|-+-+-+-+-+-+-+-|-+-+-+-+-+-+-+-|-+-+-+-+-+-+-+-|-+-+-+-+-+-+-+-|-+-+-+-+-+-+-+-|
//																						dest
//														dest
//										dest
// |-+-+-+-+-+-+-+-|-+-+-+-+-+-+-+-|-+-+-+-+-+-+-+-|-+-+-+-+-+-+-+-|-+-+-+-+-+-+-+-|-+-+-+-+-+-+-+-|
//  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 a b c d e f g h i j k l m n o p q s t u v w x y z A B C D
// |-+-+-+-+-+-+-+-|-+-+-+-+-+-+-+-|-+-+-+-+-+-+-+-|-+-+-+-+-+-+-+-|-+-+-+-+-+-+-+-|-+-+-+-+-+-+-+-|
//
template <unsigned size>
inline Value<size>&	Value<size>::operator >>= (unsigned value) noexcept {
	unsigned	nFrags	= value/nFragBits;
	unsigned	nBits	= value%nFragBits;
	if(nFrags >= size){
		*this	= 0;
		return *this;
		}
	unsigned			nMove	= size-nFrags-1;
	FragType*		dest	= &_value[(size-1)];
	FragType*		src		= &_value[(size-1)-nFrags-1];
	for(unsigned i=0;i<(nMove);++i){
		DoubleFragType	source	= src[0];
		source	<<=	nFragBits;
		source	|=	src[1];
		*dest	= (FragType)((source >> nBits));
		--src;
		--dest;
		}
	*dest	= src[1] >> nBits;
	dest	= &_value[0];
	for(unsigned i=0;i<nFrags;++i){
		*dest	= 0;
		++dest;
		}
	return *this;
	}

template <unsigned size>
inline Value<size>	Value<size>::operator >> (unsigned value) noexcept {
	Value<size>	result(*this);
	result >>= value;
	return result;
	}

//
// sizeof(FragType) == 1
// sizeof(value)	== 4
//	nValuesInFragType	== 4
//	nFragTypesInValue	== 0
//
// sizeof(FragType) == 4
// sizeof(value)	== 4
//	nValuesInFragType	== 1
//	nFragTypesInValue	== 1
//
template <unsigned size>
Value<size>&	Value<size>::operator = (unsigned value) noexcept {
	unsigned	nFragTypesInValue	= sizeof(value)/sizeof(FragType);
//	unsigned	nValuesInFragType	= sizeof(FragType)/sizeof(value);
	for(unsigned i=0;i<((size)-nFragTypesInValue);++i){
		_value[i]	= 0;
		}
	for(unsigned i=0;i<(nFragTypesInValue);++i){
		_value[(size-1)-i]	= value>>(i*nFragBits);
		}
	return *this;
	}

template <unsigned size>
inline Value<size>	Value<size>::operator ~ () const noexcept{
	Value<size>	result;
	for(unsigned i=0;i<size;++i){
		result._value[i]	= ~_value[i];
		}
	return result;
	}

template <unsigned size>
Value<size>	Value<size>::operator ++ () noexcept{
	DoubleFragType	carry	= 1;
	for(signed i=(size-1);i>=0;--i){
		carry		+= _value[i];
		_value[i]	= (FragType)carry;
		carry	>>= nFragBits;
		}
	return *this;
	}

template <unsigned size>
inline Value<size>	Value<size>::operator ++ (int) noexcept{
	Value<size>	result(*this);
	++*this;
	return result;
	}

template <unsigned size>
Value<size>	Value<size>::operator -- () noexcept{
	DoubleFragType	carry	= 0;
	for(signed i=(size-1);i>=0;--i){
		carry		+= _value[i] + (~((~((DoubleFragType)0))<<nFragBits));
		_value[i]	= (FragType)carry;
		carry	>>= nFragBits;
		}
	return *this;
	}

template <unsigned size>
inline Value<size>	Value<size>::operator -- (int) noexcept{
	Value<size>	result(*this);
	--*this;
	return result;
	}

template <unsigned size>
inline Value<size>	Value<size>::operator - () noexcept{
	Value<size>	result(*this);
	result	= ~result;
	++result;
	return result;
	}

template <unsigned size>
Value<size>&	Value<size>::operator += (const Value<size>& value) noexcept{
	DoubleFragType	carry	= 0;
	for(signed i=(size-1);i>=0;--i){
		carry		+= _value[i];
		carry		+= value._value[i];
		_value[i]	= (FragType)carry;
		carry		>>=	nFragBits;
		}
	return *this;
	}

template <unsigned size>
Value<size>&	Value<size>::operator -= (const Value<size>& value) noexcept{
	DoubleFragType	carry	= 1;
	for(signed i=(size-1);i>=0;--i){
		carry		+= ((FragType)(~value._value[i]));
		carry		+= _value[i];
		_value[i]	= (FragType)carry;
		carry		>>=	nFragBits;
		}
	return *this;
	}

template <unsigned size>
unsigned	Value<size>::ms1Bit() const noexcept{
	// The value must NOT be zero
	for(unsigned i=0;i<size;++i){
		if(_value[i]){
			for(unsigned j=(nFragBits-1);j != ((unsigned)~0);--j){
				if(_value[i] & (1<<j)){
					return (((size-1)-i)*nFragBits)+j;
					}
				}
			}
		}
	return ((unsigned)~0);	// Only if the value is zero
	}

template <unsigned size>
inline bool	Value<size>::operator < (const Value& other) const noexcept{
	for(unsigned i=0;i<size;++i){
		if(other._value[i] != _value[i]){
			return (_value[i] < other._value[i]);
			}
		}
	return false;
	}

template <unsigned size>
inline bool	Value<size>::operator <= (const Value& other) const noexcept{
	for(unsigned i=0;i<size;++i){
		if(other._value[i] != _value[i]){
			return (_value[i] < other._value[i]);
			}
		}
	return true;
	}

template <unsigned size>
inline bool	Value<size>::operator > (const Value& other) const noexcept{
	for(unsigned i=0;i<size;++i){
		if(other._value[i] != _value[i]){
			return (_value[i] > other._value[i]);
			}
		}
	return false;
	}

template <unsigned size>
inline bool	Value<size>::operator >= (const Value& other) const noexcept{
	for(unsigned i=0;i<size;++i){
		if(other._value[i] != _value[i]){
			return (_value[i] > other._value[i]);
			}
		}
	return true;
	}

template <unsigned size>
bool	Value<size>::operator == (const Value& other) const noexcept{
	for(unsigned i=0;i<size;++i){
		if(other._value[i] != _value[i]){
			return false;
			}
		}
	return true;
	}

template <unsigned size>
bool	Value<size>::operator != (const Value& other) const noexcept{
	for(unsigned i=0;i<size;++i){
		if(other._value[i] != _value[i]){
			return true;
			}
		}
	return false;
	}

template <unsigned size>
Value<size>&	Value<size>::operator %= (const Value<size/2>& value) noexcept{
	Value<size>	modulus	= value;
	unsigned	msbOfModulus	= modulus.ms1Bit();
	// Presumably, the modulus is NOT zero
	unsigned	msbOfDividend	= ms1Bit();
	if(msbOfDividend == ((unsigned)~0)){
		// The dividend is zero!
		// 0%anything == zero remainder
		return *this;
		}
	if(msbOfModulus > msbOfDividend){
		// This value *is* the remainder.
		return *this;
		}
	unsigned	nShifts = msbOfDividend - msbOfModulus;
	modulus	<<= nShifts;
	unsigned	nIterations = nShifts + 1;
	for(unsigned i=0;i<nIterations;++i){
		if(*this < modulus){
			modulus	>>=1;
			continue;
			}
		else {
			*this	-= modulus;
			modulus	>>=1;
			}
		}
	return *this;
	}

//	size	= 64
//	size/2	= 32
//	last64	= 63
//	last32	= 31
//			3				2				1
//			V				V				V
//										 last32											last64
//         0               1               2               3               4               5
// |-+-+-+-+-+-+-+-|-+-+-+-+-+-+-+-|-+-+-+-+-+-+-+-|-+-+-+-+-+-+-+-|-+-+-+-+-+-+-+-|-+-+-+-+-+-+-+-|
//  a b c d e f g h i j k l m n o p q s t u v w x y z A B C D E F G H J K L M N O P Q S T U V W X Y 
// |-+-+-+-+-+-+-+-|-+-+-+-+-+-+-+-|-+-+-+-+-+-+-+-|-+-+-+-+-+-+-+-|-+-+-+-+-+-+-+-|-+-+-+-+-+-+-+-|
//  u v w x y z A B C D E F G H J K L M N O P Q S T U V W X Y 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
// |-+-+-+-+-+-+-+-|-+-+-+-+-+-+-+-|-+-+-+-+-+-+-+-|-+-+-+-+-+-+-+-|-+-+-+-+-+-+-+-|-+-+-+-+-+-+-+-|
template <unsigned size>
Value<size>&	Value<size>::operator *= (const Value<size/2> value) noexcept{
	Value<size>	multiplicand(*this);
	*this	= 0;
	for(unsigned i=((size/2)-1);i!=((unsigned)~0);--i){
		Value<size>		temp;
		temp	= 0;
		DoubleFragType		carry	= 0;
		unsigned	shift	= (((size/2)-1)-i);
		FragType	multiplier	= value._value[i];
		for(unsigned j=((size)-1);j>=(size/2-1);--j){
			DoubleFragType	acc;
			acc	= multiplicand._value[j];
			acc	*= multiplier;
			acc	+= carry;
			temp._value[j-shift]	= (FragType)acc;
			carry	= acc;
			carry	>>= nFragBits;
			}
		*this			+= temp;
		}
	return *this;
	}

//template <unsigned size>
//Value<size>	Value<size>::operator * (const Value<size/2>& value) noexcept{
//	Value<size>	result	= *this;
//	result	*= value;
//	return result;
//	}

template <unsigned size>
inline bool	Value<size>::msb() const noexcept{
	return (_value[0] & (1<<(nFragBits-1)));
	}

template <unsigned size>
inline bool	Value<size>::lsb() const noexcept{
	return (_value[(size-1)] & 0x01);
	}

template <unsigned size>
Value<size>&	Value<size>::powXmodY(	const Value&	pow,
										const Value&	mod
										) noexcept{
	Value<size>			exponent(pow);
	Value<size>			accumulator(*this);
	Value<size*2>		result = *this;
	const unsigned		nPowBytes	= (pow.ms1Bit()+(nFragBits-1))/nFragBits;
	result	= 1;
	for(unsigned i=0;i<nPowBytes;++i){
		for(unsigned j=0;j<nFragBits;++j){
			if(exponent._value[(size-1)-i] & (1<<j)){
				result	*= accumulator;
				result	%= mod;
				}
			accumulator.squareModY(mod);
			}
		}
	*this	= Value<size>(result);
	return *this;
	}

template <unsigned size>
Value<size>&	Value<size>::pow(const Value& pow) noexcept{
	Value<size>				accumulator(*this);
	*this	= 1;
	for(unsigned i=0;i<size;++i){
		for(unsigned j=0;j<nFragBits;++j){
			if(pow._value[(size-1)-i] & (1<<j)){
				*this	*= accumulator;
				}
			accumulator.square();
			}
		}
	return *this;
	}

template <unsigned size>
Value<size>&	Value<size>::squareModY(const Value& mod) noexcept{
	Value<size*2>	result(*this);
	result	*= *this;
	result	%= mod;
	*this	= Value<size>(result);
	return *this;
	}

template <unsigned size>
inline Value<size*2>	Value<size>::square() noexcept{
	Value<size*2>	result(*this);
	result	*= *this;
	return result;
	}

}
}

#endif
