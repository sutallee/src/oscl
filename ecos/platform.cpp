/*
   Copyright (C) 2004 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "cyg/hal/hal_mem.h"
#include "oscl/mt/platform/platform.h"
#include "oscl/kernel/mmu.h"

/*
	MapIoRange() is used in:
	/src/swenv/src/oscl/pcisrv/class/sbc/usb/ohci/monitor.cpp

	hal/powerpc/arch/current/src/hal_misc.c
	hal/powerpc/mpc8xx/current/src/var_misc.c
	int cyg_hal_map_memory(	int id,
							CYG_ADDRESS virt,
							CYG_ADDRESS phys,
							cyg_int32 size,
							cyg_uint8 flags
							);
	typedef struct {
	    CYG_ADDRESS  virtual_addr;
	    CYG_ADDRESS  physical_addr;
	    cyg_int32    size;
	    cyg_uint8    flags;
	} cyg_memdesc_t;

	externC cyg_memdesc_t cyg_hal_mem_map[];

*/

extern "C" unsigned long	OsclKernelGetPageSize() noexcept{
	return 4*1024;
	}

extern "C" void*	OsclKernelVirtualToPhysical(void* virtualAddress) noexcept{
	// FIXME: Assuming 1:1 mapping
	return virtualAddress;
	}

#if 0
static int	findAvailableMmuSlot(){
	int	i;
	int maxTLBs	= 32;	// FIXME: ASSUMES 860
	for(i=0;i<maxTLBs;++i){
		if(cyg_hal_mem_map[i].size == 0){
			return i;
			}
		}
	return -1;
	}
#endif

void	MapIoRange(void* address,unsigned long size) noexcept{
// addresses are statically mapped in
// hal/powerpc/mdp/pro/860/ICM86A-860-X/current/src/hal_aux.c
// Only USB PCI creator uses this (incorrectly)
#if 0
	int	id;
	id=findAvailableMmuSlot();
	if(id < 0){
		// FIXME: no more TLB slots available
		while(true);
		}
	cyg_hal_map_memory(	id,
						(CYG_ADDRESS) address,	// virtual
						(CYG_ADDRESS) address,	// physical
						(cyg_int32) size,
						(cyg_uint8)(		CYGARC_MEMDESC_GUARDED
										|	CYGARC_MEMDESC_CI
										)
						);
#endif
	}
