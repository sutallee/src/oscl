/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_s8_dist_parth_
#define _oscl_s8_dist_parth_

#include "oscl/s8/control/reqapi.h"
#include "oscl/s8/control/api.h"
#include "oscl/s8/output/part.h"
#include "oscl/mt/itc/postmsgapi.h"

/** */
namespace Oscl {

/** */
namespace S8 {

/** */
namespace Dist {

/** */
class Part :
	public Oscl::S8::Control::Req::Api,
	public Oscl::S8::Control::Api
	{
	private:
		/** */
		Oscl::S8::Output::Part		_output;

	public:
		/** */
		Part(
			Oscl::Mt::Itc::PostMsgApi&	papi,
			int8_t						initialState
			) noexcept;

		/** */
		Oscl::S8::Observer::Req::Api::SAP&	getOutputSAP() noexcept;

		/** */
		Oscl::S8::Control::Api&	getLocalControlApi() noexcept;

	private: // Oscl::S8::Control::Req::Api
		/** */
		void request(Oscl::S8::Control::Req::Api::UpdateReq& msg) noexcept;

	public:	// Oscl::S8::Control::Api
		/** */
		void	update(int8_t state) noexcept;
	};

}
}
}

#endif
