/*
   Copyright (C) 2021 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_bluetooth_mesh_state_light_lightness_apih_
#define _oscl_bluetooth_mesh_state_light_lightness_apih_

#include <stdint.h>

/** */
namespace Oscl {
/** */
namespace BT {
/** */
namespace Mesh {
/** */
namespace State {
/** */
namespace Light {
/** */
namespace Lightness {

/** This abstract interface is used by a model
	to manipulate the Light Lightness State.

	Note: The transaction ID is not included
	in this interface since it must be used
	as follows:

	Mesh Model Spec: 3.3.2.2.2

		When a Light Lightness Server receives a
		Light Lightness Set message or a
		Light Lightness  Set Unacknowledged message,
		it shall set the Light Lightness state to
		the lightness field of the message,
		unless the message has the same values for
		the SRC, DST, and TID fields as the previous
		message received within the last 6 seconds.
 */
class Api {
	public:
		/*	This operation is invoked by a model
			to immediately change the Light Lightness State.

			lightness:
				Range: 0 : 0xFFFF
		 */
		virtual void	setLightLightnessState(
							uint16_t	lightness
							) noexcept=0;

		/*	This operation is invoked by a model
			to change the Light Lightness State.

			lightness:
				Range: 0 : 0xFFFF

			transitionTime: 3.1.3 Generic Default Transition Time
				bits 0:5: Number of steps
					0x00		immediate
					0x00:0x3E	number of steps
					0x3F		unknown
				bits 6:7: Step resolution
					0b00	Step Resolution is 100 milliseconds
					0b01	Step Resolution is 1 second
					0b10	Step Resolution is 10 seconds
					0b11	Step Resolution is 10 minutes

			executionDelayIn5msSteps: execution delay in 5ms steps
		 */
		virtual void	setLightLightnessState(
							uint16_t	lightness,
							uint8_t		transitionTime,
							uint8_t		executionDelayIn5msSteps
							) noexcept=0;

		/*	This operation is invoked by a model
			to immediately change the Generic Level State.

			lightness:
				Range: 0 : 0xFFFF
		 */
		virtual void	setLightLightnessLinearState(
							uint16_t	lightness
							) noexcept=0;

		/*	This operation is invoked by a model
			to change the Generic Level State.

			lightness:
				Range: 0 : 0xFFFF

			transitionTime: 3.1.3 Generic Default Transition Time
				bits 0:5: Number of steps
					0x00		immediate
					0x00:0x3E	number of steps
					0x3F		unknown
				bits 6:7: Step resolution
					0b00	Step Resolution is 100 milliseconds
					0b01	Step Resolution is 1 second
					0b10	Step Resolution is 10 seconds
					0b11	Step Resolution is 10 minutes

			executionDelayIn5msSteps: execution delay in 5ms steps
		 */
		virtual void	setLightLightnessLinearState(
							uint16_t	lightness,
							uint8_t		transitionTime,
							uint8_t		executionDelayIn5msSteps
							) noexcept=0;

		/*	This operation is invoked by a model
			to immediately change the Generic Level State.

			lightness:
				Range: 0 : 0xFFFF
		 */
		virtual void	setLightLightnessDefaultState(
							uint16_t	lightness
							) noexcept=0;

		/*	This operation is invoked by a model
			to immediately change the Generic Level State.

			rangeMin:
				Range: 0 : 0xFFFF

			rangeMax:
				Range: 0 : 0xFFFF
		 */
		virtual void	setLightLightnessRangeState(
							uint16_t	rangeMin,
							uint16_t	rangeMax
							) noexcept=0;

		/*	This operation is invoked by a model
			to obtain the current Level State.

			presentLightness:
				Range: 0 : 0xFFFF

			targetLightness:
				Range: 0 : 0xFFFF

			remainingTime: 3.1.3 Generic Default Transition Time
				bits 0:5: Number of steps
					0x00		immediate
					0x00:0x3E	number of steps
					0x3F		unknown
				bits 6:7: Step resolution
					0b00	Step Resolution is 100 milliseconds
					0b01	Step Resolution is 1 second
					0b10	Step Resolution is 10 seconds
					0b11	Step Resolution is 10 minutes
		 */
		virtual void	getLightLightnessState(
							uint16_t&	presentLightness,
							uint16_t&	targetLightness,
							uint8_t&	remainingTime
							) const noexcept=0;

		/*	This operation is invoked by a model
			to obtain the current Level State.

			presentLightness:
				Range: 0 : 0xFFFF

			targetLightness:
				Range: 0 : 0xFFFF

			remainingTime: 3.1.3 Generic Default Transition Time
				bits 0:5: Number of steps
					0x00		immediate
					0x00:0x3E	number of steps
					0x3F		unknown
				bits 6:7: Step resolution
					0b00	Step Resolution is 100 milliseconds
					0b01	Step Resolution is 1 second
					0b10	Step Resolution is 10 seconds
					0b11	Step Resolution is 10 minutes
		 */
		virtual void	getLightLightnessLinearState(
							uint16_t&	presentLightness,
							uint16_t&	targetLightness,
							uint8_t&	remainingTime
							) const noexcept=0;

		/*	This operation is invoked by a model
			to obtain the current Level State.

			lightness:
				Range: 0 : 0xFFFF
		 */
		virtual void	getLightLightnessLastState(
							uint16_t&	lightness
							) const noexcept=0;

		/*	This operation is invoked by a model
			to obtain the current Level State.

			lightness:
				Range: 0 : 0xFFFF
		 */
		virtual void	getLightLightnessDefaultState(
							uint16_t&	lightness
							) const noexcept=0;

		/*	This operation is invoked by a model
			to obtain the current Level State.

			rangeMin:
				Range: 0 : 0xFFFF

			rangeMax:
				Range: 0 : 0xFFFF

			statusCode:
				0x00:	Success
				0x01:	Cannot Set Range Min
				0x02:	Cannot Set Range Max

		 */
		virtual void	getLightLightnessRangeState(
							uint16_t&	rangeMin,
							uint16_t&	rangeMax,
							uint8_t&	statusCode
							) const noexcept=0;
	};

}
}
}
}
}
}

#endif
