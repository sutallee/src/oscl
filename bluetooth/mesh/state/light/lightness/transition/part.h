/*
   Copyright (C) 2021 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_bluetooth_mesh_state_light_lightness_transition_parth_
#define _oscl_bluetooth_mesh_state_light_lightness_transition_parth_

#include "oscl/bluetooth/mesh/state/light/lightness/api.h"
#include "oscl/u16/control/api.h"
#include "oscl/timer/api.h"
#include "oscl/done/operation.h"

/** */
namespace Oscl {
/** */
namespace BT {
/** */
namespace Mesh {
/** */
namespace State {
/** */
namespace Light {
/** */
namespace Lightness {
/** */
namespace Transition {

/** This component handles binary state
	transition timing according to the
	Mesh Model Light Lightness.

	It is structured like a GOF Adaptor
	to the U16 control and, optionally,
	a GOF Decorator of the Light Lightness state Api.
 */
class Part : public Oscl::BT::Mesh::State::Light::Lightness::Api {
	private:
		/** */
		Oscl::U16::Control::Api&	_ioApi;

		/** */
		Oscl::Timer::Api&			_timerApi;

		/** optional */
		Oscl::BT::Mesh::State::
		Light::Lightness::Api*		_decoratedApi;

		/** */
		const unsigned				_stepTimeInMs;

	private:
		/** */
		Oscl::Done::Operation<Part>	_delayTimerExpired;

		/** */
		Oscl::Done::Operation<Part>	_transitionTimerExpired;

		/**	Encoded according to 3.1.3 Generic Default Transition Time.
		 */
		uint8_t						_transitionTime;

		/** */
		uint16_t					_targetState;

		/** */
		uint16_t					_presentState;

		/** */
		int							_deltaState;

		/** */
		unsigned long long			_transitionStartTimestampInMs;

	public:
		/** */
		Part(
			Oscl::U16::Control::Api&	ioApi,
			Oscl::Timer::Api&			timerApi,
			int16_t						initialState,
			unsigned					stepTimeInMs,
			Oscl::BT::Mesh::State::
			Light::Lightness::Api*		decoratedApi	= 0
			) noexcept;

		/** */
		int16_t	getPresentState() noexcept;

	public: // Oscl::BT::Mesh::State::Light::Lightness::Api
		/*	This operation is invoked by a model
			to immediately change the Light Lightness State.

			lightness:
				Range: 0 : 0xFFFF
		 */
		void	setLightLightnessState(
					uint16_t	lightness
					) noexcept;

		/*	This operation is invoked by a model
			to change the Light Lightness State.

			lightness:
				Range: 0 : 0xFFFF

			transitionTime: 3.1.3 Generic Default Transition Time
				bits 0:5: Number of steps
					0x00		immediate
					0x00:0x3E	number of steps
					0x3F		unknown
				bits 6:7: Step resolution
					0b00	Step Resolution is 100 milliseconds
					0b01	Step Resolution is 1 second
					0b10	Step Resolution is 10 seconds
					0b11	Step Resolution is 10 minutes

			executionDelayIn5msSteps: execution delay in 5ms steps
		 */
		void	setLightLightnessState(
					uint16_t	lightness,
					uint8_t		transitionTime,
					uint8_t		executionDelayIn5msSteps
					) noexcept;

		/*	This operation is invoked by a model
			to immediately change the Generic Level State.

			lightness:
				Range: 0 : 0xFFFF
		 */
		void	setLightLightnessLinearState(
					uint16_t	lightness
					) noexcept;

		/*	This operation is invoked by a model
			to change the Generic Level State.

			lightness:
				Range: 0 : 0xFFFF

			transitionTime: 3.1.3 Generic Default Transition Time
				bits 0:5: Number of steps
					0x00		immediate
					0x00:0x3E	number of steps
					0x3F		unknown
				bits 6:7: Step resolution
					0b00	Step Resolution is 100 milliseconds
					0b01	Step Resolution is 1 second
					0b10	Step Resolution is 10 seconds
					0b11	Step Resolution is 10 minutes

			executionDelayIn5msSteps: execution delay in 5ms steps
		 */
		void	setLightLightnessLinearState(
					uint16_t	lightness,
					uint8_t		transitionTime,
					uint8_t		executionDelayIn5msSteps
					) noexcept;

		/*	This operation is invoked by a model
			to immediately change the Generic Level State.

			lightness:
				Range: 0 : 0xFFFF
		 */
		void	setLightLightnessDefaultState(
					uint16_t	lightness
					) noexcept;

		/*	This operation is invoked by a model
			to immediately change the Generic Level State.

			rangeMin:
				Range: 0 : 0xFFFF

			rangeMax:
				Range: 0 : 0xFFFF
		 */
		void	setLightLightnessRangeState(
					uint16_t	rangeMin,
					uint16_t	rangeMax
					) noexcept;

		/*	This operation is invoked by a model
			to obtain the current Level State.

			presentLightness:
				Range: 0 : 0xFFFF

			targetLightness:
				Range: 0 : 0xFFFF

			remainingTime: 3.1.3 Generic Default Transition Time
				bits 0:5: Number of steps
					0x00		immediate
					0x00:0x3E	number of steps
					0x3F		unknown
				bits 6:7: Step resolution
					0b00	Step Resolution is 100 milliseconds
					0b01	Step Resolution is 1 second
					0b10	Step Resolution is 10 seconds
					0b11	Step Resolution is 10 minutes
		 */
		void	getLightLightnessState(
					uint16_t&	presentLightness,
					uint16_t&	targetLightness,
					uint8_t&	remainingTime
					) const noexcept;

		/*	This operation is invoked by a model
			to obtain the current Level State.

			presentLightness:
				Range: 0 : 0xFFFF

			targetLightness:
				Range: 0 : 0xFFFF

			remainingTime: 3.1.3 Generic Default Transition Time
				bits 0:5: Number of steps
					0x00		immediate
					0x00:0x3E	number of steps
					0x3F		unknown
				bits 6:7: Step resolution
					0b00	Step Resolution is 100 milliseconds
					0b01	Step Resolution is 1 second
					0b10	Step Resolution is 10 seconds
					0b11	Step Resolution is 10 minutes
		 */
		void	getLightLightnessLinearState(
					uint16_t&	presentLightness,
					uint16_t&	targetLightness,
					uint8_t&	remainingTime
					) const noexcept;

		/*	This operation is invoked by a model
			to obtain the current Level State.

			lightness:
				Range: 0 : 0xFFFF
		 */
		void	getLightLightnessLastState(
					uint16_t&	lightness
					) const noexcept;

		/*	This operation is invoked by a model
			to obtain the current Level State.

			lightness:
				Range: 0 : 0xFFFF
		 */
		void	getLightLightnessDefaultState(
					uint16_t&	lightness
					) const noexcept;

		/*	This operation is invoked by a model
			to obtain the current Level State.

			rangeMin:
				Range: 0 : 0xFFFF

			rangeMax:
				Range: 0 : 0xFFFF

			statusCode:
				0x00:	Success
				0x01:	Cannot Set Range Min
				0x02:	Cannot Set Range Max

		 */
		void	getLightLightnessRangeState(
					uint16_t&	rangeMin,
					uint16_t&	rangeMax,
					uint8_t&	statusCode
					) const noexcept;

	private: // Oscl::Done::Operation<Part> _delayTimerExpired
		/** */
		void	delayTimerExpired() noexcept;

	private: // Oscl::Done::Operation<Part> _transitionTimerExpired
		/** */
		void	transitionTimerExpired() noexcept;
	};

}
}
}
}
}
}
}

#endif
