/*
   Copyright (C) 2021 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "part.h"
#include "oscl/bluetooth/mesh/state/transition/utils.h"
#include "oscl/timer/counter.h"

//#define DEBUG_TRACE

#ifdef DEBUG_TRACE
#include "oscl/error/info.h"
#endif

using namespace Oscl::BT::Mesh::State::Light::Lightness::Transition;

Part::Part(
	Oscl::U16::Control::Api&	ioApi,
	Oscl::Timer::Api&			timerApi,
	int16_t						initialState,
	unsigned					stepTimeInMs,
	Oscl::BT::Mesh::State::
	Light::Lightness::Api*		decoratedApi
	) noexcept:
		_ioApi(ioApi),
		_timerApi(timerApi),
		_decoratedApi(decoratedApi),
		_stepTimeInMs(stepTimeInMs),
		_delayTimerExpired(
			*this,
			&Part::delayTimerExpired
			),
		_transitionTimerExpired(
			*this,
			&Part::transitionTimerExpired
			),
		_transitionTime(0),
		_targetState(initialState),
		_presentState(initialState),
		_deltaState(0),
		_transitionStartTimestampInMs(0)
		{

	/*	For some reason, I need a delay
		after construction before actually
		invoking the ioApi.update().
	 */
	_timerApi.setExpirationCallback(_transitionTimerExpired);
	_timerApi.start(_stepTimeInMs);
	}

int16_t	Part::getPresentState() noexcept{
	return _presentState;
	}

void	Part::setLightLightnessState(
			uint16_t	lightness
			) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif

	_timerApi.stop();

	if(_decoratedApi){
		_decoratedApi->setLightLightnessState(lightness);
		}

	_ioApi.update(lightness);
	}

void	Part::setLightLightnessState(
			uint16_t	lightness,
			uint8_t		transitionTime,
			uint8_t		executionDelayIn5msSteps
			) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif

	_transitionTime	= transitionTime;
	_targetState	= lightness;

	if(_decoratedApi){
		_decoratedApi->setLightLightnessState(
			lightness,
			transitionTime,
			executionDelayIn5msSteps
			);
		}

	unsigned
	transitionTimeInMs	= Oscl::BT::Mesh::State::Transition::decodeToMilliseconds(transitionTime);

	if(executionDelayIn5msSteps){

		unsigned	executionDelayInMs	= executionDelayIn5msSteps * 5;

		if(transitionTimeInMs && (transitionTimeInMs < 37800000)){
			OsclTimerGetMillisecondCounter(_transitionStartTimestampInMs);

			_transitionStartTimestampInMs	+= transitionTimeInMs;
			_transitionStartTimestampInMs	+= executionDelayInMs;
			}

		_timerApi.setExpirationCallback(_delayTimerExpired);
		_timerApi.start(executionDelayInMs);
		return;
		}

	if(transitionTimeInMs && (transitionTimeInMs < 37800000)){

		OsclTimerGetMillisecondCounter(_transitionStartTimestampInMs);

		_transitionStartTimestampInMs	+= transitionTimeInMs;

		int	nTicks	= (transitionTimeInMs/_stepTimeInMs)+1;

		int	diff	= _targetState - _presentState;
		int	delta	= diff/nTicks;

		_deltaState	= delta;

		#ifdef DEBUG_TRACE
		Oscl::Error::Info::log(
			"%s: _transitionStartTimestampInMs: %llu, target: %d, present: %d, diff: %d, delta: %d, _deltaState: %d, nTicks: %u\n",
			OSCL_PRETTY_FUNCTION,
			_transitionStartTimestampInMs,
			_targetState,
			_presentState,
			diff,
			delta,
			_deltaState,
			nTicks
			);
		#endif

		_timerApi.setExpirationCallback(_transitionTimerExpired);

		_timerApi.start(_stepTimeInMs);

		return;
		}

	_presentState	= lightness;

	_ioApi.update(lightness);
	}

void	Part::setLightLightnessLinearState(
			uint16_t	lightness
			) noexcept{

	// FIXME:

	if(_decoratedApi){
		_decoratedApi->setLightLightnessLinearState(lightness);
		}

	}

void	Part::setLightLightnessLinearState(
			uint16_t	lightness,
			uint8_t		transitionTime,
			uint8_t		executionDelayIn5msSteps
			) noexcept{

	// FIXME:

	if(_decoratedApi){
		_decoratedApi->setLightLightnessLinearState(
			lightness,
			transitionTime,
			executionDelayIn5msSteps
			);
		}

	}

void	Part::setLightLightnessDefaultState(
			uint16_t	lightness
			) noexcept{

	// FIXME:

	if(_decoratedApi){
		_decoratedApi->setLightLightnessDefaultState(
			lightness
			);
		}

	}

void	Part::setLightLightnessRangeState(
			uint16_t	rangeMin,
			uint16_t	rangeMax
			) noexcept{

	// FIXME:

	if(_decoratedApi){
		_decoratedApi->setLightLightnessRangeState(
			rangeMin,
			rangeMax
			);
		}

	}

void	Part::getLightLightnessState(
			uint16_t&	presentLightness,
			uint16_t&	targetLightness,
			uint8_t&	remainingTime
			) const noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s: _presentState: %d, _targetState: %d\n",
		OSCL_PRETTY_FUNCTION,
		_presentState,
		_targetState
		);
	#endif

	if(_targetState != _presentState){
		unsigned long long	nowInMs;

		OsclTimerGetMillisecondCounter(nowInMs);

		if(nowInMs < _transitionStartTimestampInMs){
			unsigned long
			remainingTimeInMs	= _transitionStartTimestampInMs - nowInMs;
			remainingTime	= Oscl::BT::Mesh::State::Transition::encode(remainingTimeInMs);
			}
		else {
			remainingTime	= 0;
			}


		#ifdef DEBUG_TRACE
		Oscl::Error::Info::log(
			"%s: _transitionStartTimestampInMs: %llu, nowInMs: %llu, remainingTime: 0x%2.2X\n",
			OSCL_PRETTY_FUNCTION,
			_transitionStartTimestampInMs,
			nowInMs,
			remainingTime
			);
		#endif

		}
	else {
		remainingTime	= 0;
		}

	presentLightness	= _presentState;
	targetLightness		= _targetState;
	}

void	Part::getLightLightnessLinearState(
			uint16_t&	presentLightness,
			uint16_t&	targetLightness,
			uint8_t&	remainingTime
			) const noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s: _presentState: %d, _targetState: %d\n",
		OSCL_PRETTY_FUNCTION,
		_presentState,
		_targetState
		);
	#endif

	// FIXME: do conversions?
	}

void	Part::getLightLightnessLastState(
			uint16_t&	lightness
			) const noexcept{

	// FIXME:

	if(_decoratedApi){
		_decoratedApi->getLightLightnessLastState(
			lightness
			);
		}

	}

void	Part::getLightLightnessDefaultState(
			uint16_t&	lightness
			) const noexcept{

	// FIXME:

	if(_decoratedApi){
		_decoratedApi->getLightLightnessDefaultState(
			lightness
			);
		}

	}

void	Part::getLightLightnessRangeState(
			uint16_t&	rangeMin,
			uint16_t&	rangeMax,
			uint8_t&	statusCode
			) const noexcept{

	// FIXME:

	if(_decoratedApi){
		_decoratedApi->getLightLightnessRangeState(
			rangeMin,
			rangeMax,
			statusCode
			);
		}

	}

void	Part::delayTimerExpired() noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif

	unsigned
	transitionTimeInMs	= Oscl::BT::Mesh::State::Transition::decodeToMilliseconds(_transitionTime);

	if(transitionTimeInMs && transitionTimeInMs < 37800000){

		OsclTimerGetMillisecondCounter(_transitionStartTimestampInMs);

		_transitionStartTimestampInMs	+= transitionTimeInMs;

		int	nTicks	= (transitionTimeInMs/_stepTimeInMs)+1;

		int	diff	= _targetState - _presentState;
		int	delta	= diff/nTicks;

		_deltaState	= delta;

		#ifdef DEBUG_TRACE
		Oscl::Error::Info::log(
			"%s: _transitionStartTimestampInMs: %llu, target: %d, present: %d, diff: %d, delta: %d, _deltaState: %d, nTicks: %u\n",
			OSCL_PRETTY_FUNCTION,
			_transitionStartTimestampInMs,
			_targetState,
			_presentState,
			diff,
			delta,
			_deltaState,
			nTicks
			);
		#endif

		_timerApi.setExpirationCallback(_transitionTimerExpired);

		_timerApi.start(_stepTimeInMs);

		return;
		}

	_presentState	= _targetState;

	_ioApi.update(_targetState);
	}

void	Part::transitionTimerExpired() noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif

	int32_t
	newState	= _presentState + _deltaState;

	if(_deltaState < 0){
		if(newState <= _targetState){
			_presentState	= _targetState;
			_ioApi.update(_presentState);
			return;
			}
		}
	else {
		if(newState >= _targetState){
			_presentState	= _targetState;
			_ioApi.update(_presentState);
			return;
			}
		}

	_presentState	= newState;

	_timerApi.start(_stepTimeInMs);

	_ioApi.update(_presentState);
	}

