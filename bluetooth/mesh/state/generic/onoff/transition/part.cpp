/*
   Copyright (C) 2021 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "part.h"
#include "oscl/bluetooth/mesh/state/transition/utils.h"
#include "oscl/timer/counter.h"

//#define DEBUG_TRACE

#ifdef DEBUG_TRACE
#include "oscl/error/info.h"
#endif

using namespace Oscl::BT::Mesh::State::Generic::OnOff::Transition;

Part::Part(
	Oscl::Switch::
	Spst::Control::Api&			switchApi,
	Oscl::Timer::Api&			timerApi,
	uint8_t						initialState,
	Oscl::BT::Mesh::State::
	Generic::OnOff::Api*		decoratedApi
	) noexcept:
		_switchApi(switchApi),
		_timerApi(timerApi),
		_decoratedApi(decoratedApi),
		_delayTimerExpired(
			*this,
			&Part::delayTimerExpired
			),
		_transitionTimerExpired(
			*this,
			&Part::transitionTimerExpired
			),
		_transitionTime(0),
		_targetState(initialState),
		_presentState(initialState),
		_transitionStartTimestampInMs(0)
		{
	if(initialState){
		switchApi.on();
		}
	else {
		switchApi.off();
		}
	}

uint8_t	Part::getPresentState() noexcept{
	return _presentState;
	}

void	Part::setGenericOnOffState(
			uint8_t	onOff
			) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif

	_timerApi.stop();

	if(_decoratedApi){
		_decoratedApi->setGenericOnOffState(onOff);
		}

	if(onOff){
		_switchApi.on();
		}
	else {
		_switchApi.off();
		}
	}

void	Part::setGenericOnOffState(
			uint8_t	onOff,
			uint8_t	transitionTime,
			uint8_t	executionDelayIn5msSteps
			) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif

	_transitionTime	= transitionTime;
	_targetState	= onOff;

	if(_decoratedApi){
		_decoratedApi->setGenericOnOffState(
			onOff,
			transitionTime,
			executionDelayIn5msSteps
			);
		}

	unsigned
	transitionTimeInMs	= Oscl::BT::Mesh::State::Transition::decodeToMilliseconds(transitionTime);

	if(executionDelayIn5msSteps){

		unsigned	executionDelayInMs	= executionDelayIn5msSteps * 5;

		if(transitionTimeInMs && (transitionTimeInMs < 37800000)){
			OsclTimerGetMillisecondCounter(_transitionStartTimestampInMs);

			_transitionStartTimestampInMs	+= transitionTimeInMs;
			_transitionStartTimestampInMs	+= executionDelayInMs;
			}

		_timerApi.setExpirationCallback(_delayTimerExpired);
		_timerApi.start(executionDelayInMs);
		return;
		}

	if(transitionTimeInMs && (transitionTimeInMs < 37800000)){

		OsclTimerGetMillisecondCounter(_transitionStartTimestampInMs);

		_transitionStartTimestampInMs	+= transitionTimeInMs;

		#ifdef DEBUG_TRACE
		Oscl::Error::Info::log(
			"%s: _transitionStartTimestampInMs: %llu\n",
			OSCL_PRETTY_FUNCTION,
			_transitionStartTimestampInMs
			);
		#endif

		_timerApi.setExpirationCallback(_transitionTimerExpired);
		_timerApi.start(transitionTimeInMs);

		if(_targetState){
			_switchApi.on();
			}

		return;
		}

	_presentState	= onOff;

	if(_targetState){
		_switchApi.on();
		}
	else {
		_switchApi.off();
		}
	}

void	Part::getGenericOnOffState(
			uint8_t&	presentOnOff,
			uint8_t&	targetOnOff,
			uint8_t&	remainingTime
			) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s: _presentState: 0x%2.2X, _targetState: 0x%2.2X\n",
		OSCL_PRETTY_FUNCTION,
		_presentState,
		_targetState
		);
	#endif

	if(_targetState != _presentState){
		unsigned long long	nowInMs;

		OsclTimerGetMillisecondCounter(nowInMs);

		if(nowInMs < _transitionStartTimestampInMs){
			unsigned long
			remainingTimeInMs	= _transitionStartTimestampInMs - nowInMs;
			remainingTime	= Oscl::BT::Mesh::State::Transition::encode(remainingTimeInMs);
			}
		else {
			remainingTime	= 0;
			}


		#ifdef DEBUG_TRACE
		Oscl::Error::Info::log(
			"%s: _transitionStartTimestampInMs: %llu, nowInMs: %llu, remainingTime: 0x%2.2X\n",
			OSCL_PRETTY_FUNCTION,
			_transitionStartTimestampInMs,
			nowInMs,
			remainingTime
			);
		#endif

		}
	else {
		remainingTime	= 0;
		}

	presentOnOff	= _presentState;
	targetOnOff		= _targetState;
	}

void	Part::delayTimerExpired() noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif

	unsigned
	transitionTimeInMs	= Oscl::BT::Mesh::State::Transition::decodeToMilliseconds(_transitionTime);

	if(transitionTimeInMs && transitionTimeInMs < 37800000){

		OsclTimerGetMillisecondCounter(_transitionStartTimestampInMs);

		_transitionStartTimestampInMs	+= transitionTimeInMs;

		_timerApi.setExpirationCallback(_transitionTimerExpired);
		_timerApi.start(transitionTimeInMs);

		if(_targetState){
			_switchApi.on();
			}
		return;
		}

	_presentState	= _targetState;

	if(_targetState){
		_switchApi.on();
		}
	else {
		_switchApi.off();
		}
	}

void	Part::transitionTimerExpired() noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif

	_presentState	= _targetState;

	if(_targetState){
		_switchApi.on();
		}
	else {
		_switchApi.off();
		}
	}

