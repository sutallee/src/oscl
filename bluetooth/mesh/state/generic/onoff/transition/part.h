/*
   Copyright (C) 2021 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_bluetooth_mesh_state_generic_onoff_transition_parth_
#define _oscl_bluetooth_mesh_state_generic_onoff_transition_parth_

#include "oscl/bluetooth/mesh/state/generic/onoff/api.h"
#include "oscl/switch/spst/control/api.h"
#include "oscl/timer/api.h"
#include "oscl/done/operation.h"

/** */
namespace Oscl {
/** */
namespace BT {
/** */
namespace Mesh {
/** */
namespace State {
/** */
namespace Generic {
/** */
namespace OnOff {
/** */
namespace Transition {

/** This component handles binary state
	transition timing according to the
	Mesh Model 3.1.1 Generic OnOff.

	It is structured like a GOF Adaptor
	to the SPST Switch Api and, optionally,
	a GOF Decorator of the Generic OnOff state Api.
 */
class Part : public Oscl::BT::Mesh::State::Generic::OnOff::Api {
	private:
		/** */
		Oscl::Switch::
		Spst::Control::Api&			_switchApi;

		/** */
		Oscl::Timer::Api&			_timerApi;

		/** optional */
		Oscl::BT::Mesh::State::
		Generic::OnOff::Api*		_decoratedApi;

	private:
		/** */
		Oscl::Done::Operation<Part>	_delayTimerExpired;

		/** */
		Oscl::Done::Operation<Part>	_transitionTimerExpired;

		/**	Encoded according to 3.1.3 Generic Default Transition Time.
		 */
		uint8_t						_transitionTime;

		/** */
		uint8_t						_targetState;

		/** */
		uint8_t						_presentState;

		/** */
		unsigned long long			_transitionStartTimestampInMs;

	public:
		/** */
		Part(
			Oscl::Switch::
			Spst::Control::Api&			switchApi,
			Oscl::Timer::Api&			timerApi,
			uint8_t						initialState,
			Oscl::BT::Mesh::State::
			Generic::OnOff::Api*		decoratedApi	= 0
			) noexcept;

		/** */
		uint8_t	getPresentState() noexcept;

	public:
		/*	This operation is invoked by a model
			to immediately change the Generic OnOff State.
			onOff:
				0x00	Off
				0x01	On
		 */
		void	setGenericOnOffState(
					uint8_t	onOff
					) noexcept;

		/*	This operation is invoked by a model
			to change the Generic OnOff State.
			onOff:
				0x00	Off
				0x01	On
			transitionTime: 3.1.3 Generic Default Transition Time
				bits 0:5: Number of steps
					0x00		immediate
					0x00:0x3E	number of steps
					0x3F		unknown
				bits 6:7: Step resolution
					0b00	Step Resolution is 100 milliseconds
					0b01	Step Resolution is 1 second
					0b10	Step Resolution is 10 seconds
					0b11	Step Resolution is 10 minutes
			executionDelayIn5msSteps: execution delay in 5ms steps
		 */
		void	setGenericOnOffState(
					uint8_t	onOff,
					uint8_t	transitionTime,
					uint8_t	executionDelayIn5msSteps
					) noexcept;

		/*	This operation is invoked by a model
			to obtain the current OnOff State.
			presentOnOff:
				0x00	Off
				0x01	On
			targetOnOff:
				0x00	Off
				0x01	On
			remainingTime: 3.1.3 Generic Default Transition Time
				bits 0:5: Number of steps
					0x00		immediate
					0x00:0x3E	number of steps
					0x3F		unknown
				bits 6:7: Step resolution
					0b00	Step Resolution is 100 milliseconds
					0b01	Step Resolution is 1 second
					0b10	Step Resolution is 10 seconds
					0b11	Step Resolution is 10 minutes
		 */
		void	getGenericOnOffState(
					uint8_t&	presentOnOff,
					uint8_t&	targetOnOff,
					uint8_t&	remainingTime
					) noexcept;

	private: // Oscl::Done::Operation<Part> _delayTimerExpired
		/** */
		void	delayTimerExpired() noexcept;

	private: // Oscl::Done::Operation<Part> _transitionTimerExpired
		/** */
		void	transitionTimerExpired() noexcept;
	};

}
}
}
}
}
}
}

#endif
