/*
   Copyright (C) 2021 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <new>
#include <string.h>
#include <strings.h>
#include <stdlib.h>

#include "part.h"
#include "oscl/bluetooth/mesh/address/values.h"
#include "oscl/error/fatal.h"
#include "oscl/error/info.h"
#include "oscl/strings/hex.h"
#include "oscl/strings/fixed.h"
#include "oscl/uuid/string.h"
#include "oscl/uuid/generator.h"
#include "oscl/entropy/rand.h"
#include "oscl/zephyr/fs/file/scope.h"
#include "oscl/zephyr/fs/file/fgets.h"
#include <errno.h>

using namespace Oscl::BT::Mesh::State::Generic::OnOff::Persist::File::Zephyr::FS;

// #define DEBUG_TRACE

Part::Part(
	Oscl::Persist::
	Parent::Zephyr::FS::Part&	parent,
	const char*					fileName
	) noexcept:
		Base(
			parent,
			fileName
			)
		{
	read();
	}

Part::~Part() noexcept{
	}

void	Part::setTargetState(uint8_t state) noexcept{
	_targetState	= state;
	write();
	}

uint8_t	Part::getTargetState() const noexcept{
	return _targetState;
	}

void	Part::setDefaultValues() noexcept{
	setTargetState(0x00);	// OFF
	}

bool	Part::writeValues(Oscl::FS::File::Api& f) noexcept{

	Oscl::Strings::Fixed<1024>	value;
	value	= "targetState=";
	value	+= _targetState;
	value	+= "\n";

	ssize_t
	writeResult	= f.write(
					value.getString(),
					value.length()
					);

	if(writeResult < 0){
		Oscl::Error::Info::log(
			"%s: write() failed %d\n",
			OSCL_PRETTY_FUNCTION,
			writeResult
			);
		return true;
		}

	unsigned long
	nWritten	= writeResult;

	if(nWritten < value.length()){
		Oscl::Error::Info::log(
			"%s: write() failed. nWritten: %d\n",
			OSCL_PRETTY_FUNCTION,
			nWritten
			);
		return true;
		}

	return false;
	}

bool	Part::readValues(Oscl::FS::File::Api& f) noexcept{

	bool			targetStateIsValid	= false;

	char			buffer[1024];
	const char*		line;

	while((line = Oscl::Zephyr::FS::File::fgets(buffer,sizeof(buffer),f))){

		#ifdef DEBUG_TRACE
		Oscl::Error::Info::log(
			"%s: fgets(): \"%s\"\n",
			OSCL_PRETTY_FUNCTION,
			line
			);
		#endif

		if(line[0] == '#'){
			// comment
			continue;
			}

		char*
		assignment	= index(buffer,'=');

		if(!assignment){
			continue;
			}

		assignment[0]	= '\0';

		char*
		value	= &assignment[1];

		char*
		newline = rindex(value,'\n');

		if(newline){
			*newline	= '\0';
			}

		if(!strcmp(line,"targetState")){
			_targetState		= (uint8_t)strtoul(value,0,0);
			targetStateIsValid	= true;
			}
		}

	if(!targetStateIsValid){
		Oscl::Error::Info::log(
			"%s: targetStateIsValid is required.\n",
			OSCL_PRETTY_FUNCTION
			);
		return true;
		}

	return false;
	}
