/*
   Copyright (C) 2021 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "part.h"
#include "oscl/bluetooth/mesh/state/transition/utils.h"
#include "oscl/timer/counter.h"

//#define DEBUG_TRACE

#ifdef DEBUG_TRACE
#include "oscl/error/info.h"
#endif

using namespace Oscl::BT::Mesh::State::Generic::Level::Transition;

Part::Part(
	Oscl::S16::Control::Api&	ioApi,
	Oscl::Timer::Api&			timerApi,
	int16_t						initialState,
	unsigned					stepTimeInMs,
	Oscl::BT::Mesh::State::
	Generic::Level::Api*		decoratedApi
	) noexcept:
		_ioApi(ioApi),
		_timerApi(timerApi),
		_decoratedApi(decoratedApi),
		_stepTimeInMs(stepTimeInMs),
		_delayTimerExpired(
			*this,
			&Part::delayTimerExpired
			),
		_transitionTimerExpired(
			*this,
			&Part::transitionTimerExpired
			),
		_transitionTime(0),
		_targetState(initialState),
		_presentState(initialState),
		_transitionStartTimestampInMs(0)
		{

	ioApi.update(initialState);
	}

int16_t	Part::getPresentState() noexcept{
	return _presentState;
	}

void	Part::setGenericLevelState(
			int16_t	level
			) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif

	_timerApi.stop();

	if(_decoratedApi){
		_decoratedApi->setGenericLevelState(level);
		}

	_ioApi.update(level);
	}

void	Part::setGenericLevelState(
			int16_t	level,
			uint8_t	transitionTime,
			uint8_t	executionDelayIn5msSteps
			) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif

	_transitionTime	= transitionTime;
	_targetState	= level;

	if(_decoratedApi){
		_decoratedApi->setGenericLevelState(
			level,
			transitionTime,
			executionDelayIn5msSteps
			);
		}

	unsigned
	transitionTimeInMs	= Oscl::BT::Mesh::State::Transition::decodeToMilliseconds(transitionTime);

	if(executionDelayIn5msSteps){

		unsigned	executionDelayInMs	= executionDelayIn5msSteps * 5;

		if(transitionTimeInMs && (transitionTimeInMs < 37800000)){
			OsclTimerGetMillisecondCounter(_transitionStartTimestampInMs);

			_transitionStartTimestampInMs	+= transitionTimeInMs;
			_transitionStartTimestampInMs	+= executionDelayInMs;
			}

		_timerApi.setExpirationCallback(_delayTimerExpired);
		_timerApi.start(executionDelayInMs);
		return;
		}

	if(transitionTimeInMs && (transitionTimeInMs < 37800000)){

		OsclTimerGetMillisecondCounter(_transitionStartTimestampInMs);

		_transitionStartTimestampInMs	+= transitionTimeInMs;

		int	nTicks	= (transitionTimeInMs/_stepTimeInMs)+1;

		int	diff	= _targetState - _presentState;
		int	delta	= diff/nTicks;

		_deltaState	= delta;

		#ifdef DEBUG_TRACE
		Oscl::Error::Info::log(
			"%s: _transitionStartTimestampInMs: %llu, target: %d, present: %d, diff: %d, delta: %d, _deltaState: %d, nTicks: %u\n",
			OSCL_PRETTY_FUNCTION,
			_transitionStartTimestampInMs,
			_targetState,
			_presentState,
			diff,
			delta,
			_deltaState,
			nTicks
			);
		#endif

		_timerApi.setExpirationCallback(_transitionTimerExpired);

		_timerApi.start(_stepTimeInMs);

		return;
		}

	_presentState	= level;

	_ioApi.update(level);
	}

void	Part::setGenericDeltaState(
			int16_t	deltaLevel
			) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif

	}

void	Part::setGenericDeltaState(
			int16_t	deltaLevel,
			uint8_t	transitionTime,
			uint8_t	executionDelayIn5msSteps
			) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif

	/*	Mesh Model Spec: 3.3.2.2.3
		When a Generic Level Server receives a
		Generic Delta Set message or a
		Generic Delta Set Unacknowledged message,
		it shall set the Generic Level state to
		the Delta Level field of the message added
		to the Initial Generic Level state.

		When the Generic Level state is bound to
		another state that is not contiguous,
		the Generic Level Server shall set the
		Generic Level state to a value that satisfies
		the requirements of the bound state.

		For example, a Generic Level state may be bound
		to a Light Lightness state that is bound to a Light
		Lightness Range state such that only values 0 and
		1000–50000 are supported; therefore,
		if the resulting value of the bound Light Lightness
		range state is in the excluded open range of (0,1000),
		a value of 0 shall be set for negative Delta Level
		values and a value of 1000 shall be set for positive
		Delta Level values.

		When the Generic Level state is not bound to any state,
		the overflow/underflow handling is implementation-specific.
		Some Generic Level Servers may stop at their maximum or
		minimum level, and some may wrap around.

		When the Generic Level state is bound to another state,
		the overflow/underflow handling shall be defined by
		the wrap-around behavior of the bound state.

		If present, the Transition Time field value shall be
		used as the time for transition to the target state.
		If the Transition Time field is not included and
		the Generic Default Transition Time state is supported,
		the Generic Default Transition Time state shall be used.
		Otherwise the transition shall be instantaneous.

		If the Transition Time field is included,
		the Delay field is included and indicates a delay
		5-millisecond steps that the server shall wait
		before executing any state-changing behavior
		for this message.

		If the target state is equal to the current state,
		the transition shall not be started and is considered
		complete.

		It is recommended to set the value of the
		Transition Time field to the expected interval
		after which the next message will be sent.
		For example, if the messages are sent every
		200 milliseconds, the Transition Time should
		be set to 200 milliseconds.

	 */
#if 0
	// FIXME: targetState + deltaLevel

	int16_t	presentLevel;
	int16_t	targetLevel;
	uint8_t	remainingTime;

	_contextApi.getGenericLevelState(
		presentLevel,
		targetLevel,
		remainingTime
		);

	targetLevel	= presentLevel	+= deltaLevel; // FIXME: ? Handle overflow
#endif

	}

void	Part::setGenericMoveState(
			int16_t	moveLevel
			) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif

	}

void	Part::setGenericMoveState(
			int16_t	moveLevel,
			uint8_t	transitionTime,
			uint8_t	executionDelayIn5msSteps
			) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif

	/*	Mesh Model Spec: 3.3.2.2.4
		When a Generic Level Server receives a
		Generic Move Set message or a
		Generic Move Set Unacknowledged message,
		it shall start a process of changing the
		Generic Level state with a transition speed
		that is calculated by dividing the
		Delta Level by the Transition Time
		(i.e., it will be changing by a value
		of the Delta Level in time of the Transition Time)

		When the Generic Level state is not bound to
		another state, the overflow/underflow handling is
		implementation-specific. Some Generic Level Servers
		may stop at their maximum or minimum levels,
		and some may wrap around.

		When the Generic Level state is bound to another
		state, the overflow/underflow handling shall be
		defined by the wrap-around behavior of the bound state.

		When a Generic Level Server receives the message
		with a value of the Delta Level field equal to 0, it
		shall stop changing the Generic Level state.

		If present, the Transition Time field value shall be
		used as the time for calculating the transition speed
		towards the target state.

		If the Transition Time field is not present and the
		Generic Default Transition Time state (see Section 3.1.3)
		is supported, the Generic Default Transition Time
		state shall be used.

		Otherwise the Generic Move Set command shall not
		initiate any Generic Level state change.

		If the resulting Transition Time is equal to 0,
		the Generic Move Set command will not initiate any
		Generic Level state change.

		If the Transition Time field is included,
		the Delay field is included and indicates a delay
		5-millisecond steps that the server shall wait
		before executing any state-changing behavior for
		this message.

		If the target state is equal to the current state,
		the transition shall not be started and is considered
		complete.
	 */

#if 0
	int16_t	presentLevel;
	int16_t	targetLevel;
	uint8_t	remainingTime;

	_contextApi.getGenericLevelState(
		presentLevel,
		targetLevel,
		remainingTime
		);

	targetLevel	+= moveLevel;	// FIXME: ? Handle overflow
#endif

	}

void	Part::getGenericLevelState(
			int16_t&	presentLevel,
			int16_t&	targetLevel,
			uint8_t&	remainingTime
			) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s: _presentState: %d, _targetState: %d\n",
		OSCL_PRETTY_FUNCTION,
		_presentState,
		_targetState
		);
	#endif

	if(_targetState != _presentState){
		unsigned long long	nowInMs;

		OsclTimerGetMillisecondCounter(nowInMs);

		if(nowInMs < _transitionStartTimestampInMs){
			unsigned long
			remainingTimeInMs	= _transitionStartTimestampInMs - nowInMs;
			remainingTime	= Oscl::BT::Mesh::State::Transition::encode(remainingTimeInMs);
			}
		else {
			remainingTime	= 0;
			}


		#ifdef DEBUG_TRACE
		Oscl::Error::Info::log(
			"%s: _transitionStartTimestampInMs: %llu, nowInMs: %llu, remainingTime: 0x%2.2X\n",
			OSCL_PRETTY_FUNCTION,
			_transitionStartTimestampInMs,
			nowInMs,
			remainingTime
			);
		#endif

		}
	else {
		remainingTime	= 0;
		}

	presentLevel	= _presentState;
	targetLevel		= _targetState;
	}

void	Part::delayTimerExpired() noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif

	unsigned
	transitionTimeInMs	= Oscl::BT::Mesh::State::Transition::decodeToMilliseconds(_transitionTime);

	if(transitionTimeInMs && transitionTimeInMs < 37800000){

		OsclTimerGetMillisecondCounter(_transitionStartTimestampInMs);

		_transitionStartTimestampInMs	+= transitionTimeInMs;

		int	nTicks	= (transitionTimeInMs/_stepTimeInMs)+1;

		int	diff	= _targetState - _presentState;
		int	delta	= diff/nTicks;

		_deltaState	= delta;

		#ifdef DEBUG_TRACE
		Oscl::Error::Info::log(
			"%s: _transitionStartTimestampInMs: %llu, target: %d, present: %d, diff: %d, delta: %d, _deltaState: %d, nTicks: %u\n",
			OSCL_PRETTY_FUNCTION,
			_transitionStartTimestampInMs,
			_targetState,
			_presentState,
			diff,
			delta,
			_deltaState,
			nTicks
			);
		#endif

		_timerApi.setExpirationCallback(_transitionTimerExpired);

		_timerApi.start(_stepTimeInMs);

		return;
		}

	_presentState	= _targetState;

	_ioApi.update(_targetState);
	}

void	Part::transitionTimerExpired() noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif

	int32_t
	newState	= _presentState + _deltaState;

	if(_deltaState < 0){
		if(newState <= _targetState){
			_presentState	= _targetState;
			_ioApi.update(_presentState);
			return;
			}
		}
	else {
		if(newState >= _targetState){
			_presentState	= _targetState;
			_ioApi.update(_presentState);
			return;
			}
		}

	_presentState	= newState;

	_timerApi.start(_stepTimeInMs);

	_ioApi.update(_presentState);
	}

