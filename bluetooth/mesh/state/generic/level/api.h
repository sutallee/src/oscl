/*
   Copyright (C) 2021 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_bluetooth_mesh_state_generic_level_apih_
#define _oscl_bluetooth_mesh_state_generic_level_apih_

#include <stdint.h>

/** */
namespace Oscl {
/** */
namespace BT {
/** */
namespace Mesh {
/** */
namespace State {
/** */
namespace Generic {
/** */
namespace Level {

/** This abstract interface is used by a model
	to manipulate the Generic Level State.

	Note: The transaction ID is not included
	in this interface since it must be used
	as follows:

	Mesh Model Spec: 3.3.2.2.2

		When a Generic Level Server receives a
		Generic Level Set message or a
		Generic Level Set Unacknowledged message,
		it shall set the Generic Level state to
		the Level field of the message,
		unless the message has the same values for
		the SRC, DST, and TID fields as the previous
		message received within the last 6 seconds.
 */
class Api {
	public:
		/*	This operation is invoked by a model
			to immediately change the Generic Level State.

			level:
				Range: -32,768 : 32,767
		 */
		virtual void	setGenericLevelState(
							int16_t	level
							) noexcept=0;

		/*	This operation is invoked by a model
			to change the Generic Level State.

			level:
				Range: -32,768 : 32,767

			transitionTime: 3.1.3 Generic Default Transition Time
				bits 0:5: Number of steps
					0x00		immediate
					0x00:0x3E	number of steps
					0x3F		unknown
				bits 6:7: Step resolution
					0b00	Step Resolution is 100 milliseconds
					0b01	Step Resolution is 1 second
					0b10	Step Resolution is 10 seconds
					0b11	Step Resolution is 10 minutes

			executionDelayIn5msSteps: execution delay in 5ms steps
		 */
		virtual void	setGenericLevelState(
							int16_t	level,
							uint8_t	transitionTime,
							uint8_t	executionDelayIn5msSteps
							) noexcept=0;

		/*	This operation is invoked by a model
			to immediately change the Generic Level State.

			deltaLevel:
				Range: -32,768 : 32,767
		 */
		virtual void	setGenericDeltaState(
							int16_t	deltaLevel
							) noexcept=0;

		/*	This operation is invoked by a model
			to change the Generic Level State.

			deltaLevel:
				Range: -32,768 : 32,767

			transitionTime: 3.1.3 Generic Default Transition Time
				bits 0:5: Number of steps
					0x00		immediate
					0x00:0x3E	number of steps
					0x3F		unknown
				bits 6:7: Step resolution
					0b00	Step Resolution is 100 milliseconds
					0b01	Step Resolution is 1 second
					0b10	Step Resolution is 10 seconds
					0b11	Step Resolution is 10 minutes

			executionDelayIn5msSteps: execution delay in 5ms steps
		 */
		virtual void	setGenericDeltaState(
							int16_t	deltaLevel,
							uint8_t	transitionTime,
							uint8_t	executionDelayIn5msSteps
							) noexcept=0;

		/*	This operation is invoked by a model
			to immediately change the Generic Level State.

			moveLevel:
				Range: -32,768 : 32,767
		 */
		virtual void	setGenericMoveState(
							int16_t	moveLevel
							) noexcept=0;

		/*	This operation is invoked by a model
			to change the Generic Level State.

			moveLevel:
				Range: -32,768 : 32,767

			transitionTime: 3.1.3 Generic Default Transition Time
				bits 0:5: Number of steps
					0x00		immediate
					0x00:0x3E	number of steps
					0x3F		unknown
				bits 6:7: Step resolution
					0b00	Step Resolution is 100 milliseconds
					0b01	Step Resolution is 1 second
					0b10	Step Resolution is 10 seconds
					0b11	Step Resolution is 10 minutes

			executionDelayIn5msSteps: execution delay in 5ms steps
		 */
		virtual void	setGenericMoveState(
							int16_t	moveLevel,
							uint8_t	transitionTime,
							uint8_t	executionDelayIn5msSteps
							) noexcept=0;

		/*	This operation is invoked by a model
			to obtain the current Level State.

			presentLevel:
				Range: -32,768 : 32,767

			targetLevel:
				Range: -32,768 : 32,767

			remainingTime: 3.1.3 Generic Default Transition Time
				bits 0:5: Number of steps
					0x00		immediate
					0x00:0x3E	number of steps
					0x3F		unknown
				bits 6:7: Step resolution
					0b00	Step Resolution is 100 milliseconds
					0b01	Step Resolution is 1 second
					0b10	Step Resolution is 10 seconds
					0b11	Step Resolution is 10 minutes
		 */
		virtual void	getGenericLevelState(
							int16_t&	presentLevel,
							int16_t&	targetLevel,
							uint8_t&	remainingTime
							) noexcept=0;
	};

}
}
}
}
}
}

#endif
