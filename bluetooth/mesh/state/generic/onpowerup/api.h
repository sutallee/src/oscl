/*
   Copyright (C) 2021 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_bluetooth_mesh_state_generic_onpowerup_apih_
#define _oscl_bluetooth_mesh_state_generic_onpowerup_apih_

#include <stdint.h>

/** */
namespace Oscl {
/** */
namespace BT {
/** */
namespace Mesh {
/** */
namespace State {
/** */
namespace Generic {
/** */
namespace OnPowerUp {

/** This abstract interface is used by a model
	to manipulate a Generic OnPowerUp State.

	3.1.4 Generic OnPowerUp
		States:
		0x00 -	Off. After being powered up, the element is in an off state.
		0x01 -	Default. After being powered up, the element is in an On
				state and uses default state values.
		0x02 -	Restore. If a transition was in progress when powered down,
				the element restores the target state when powered up.
				Otherwise the element restores the state it was in when
				powered down.
 */
class Api {
	public:
		/*	This operation is invoked by a model
			to immediately change the Generic OnPowerUp State.
			state:
				0x00 -	Off. After being powered up, the element is in an off state.
				0x01 -	Default. After being powered up, the element is in an On
						state and uses default state values.
				0x02 -	Restore. If a transition was in progress when powered down,
						the element restores the target state when powered up.
						Otherwise the element restores the state it was in when
						powered down.
		 */
		virtual void	setGenericOnPowerUpState(
							uint8_t	state
							) noexcept=0;

		/*	This operation is invoked by a model
			to obtain the Generic OnPowerUp State.
			state:
				0x00 -	Off. After being powered up, the element is in an off state.
				0x01 -	Default. After being powered up, the element is in an On
						state and uses default state values.
				0x02 -	Restore. If a transition was in progress when powered down,
						the element restores the target state when powered up.
						Otherwise the element restores the state it was in when
						powered down.
		 */
		virtual uint8_t	getGenericOnPowerUpState() noexcept=0;
	};

}
}
}
}
}
}

#endif
