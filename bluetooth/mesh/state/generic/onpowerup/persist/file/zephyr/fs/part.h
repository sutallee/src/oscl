/*
   Copyright (C) 2021 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_bluetooth_mesh_state_generic_onpowerup_persist_file_zephyr_fs_parth_
#define _oscl_bluetooth_mesh_state_generic_onpowerup_persist_file_zephyr_fs_parth_

#include "oscl/bluetooth/mesh/state/generic/onpowerup/persist/api.h"
#include "oscl/persist/parent/zephyr/fs/part.h"

/** */
namespace Oscl {
/** */
namespace BT {
/** */
namespace Mesh {
/** */
namespace State {
/** */
namespace Generic {
/** */
namespace OnPowerUp {
/** */
namespace Persist {
/** */
namespace File {
/** */
namespace Zephyr {
/** */
namespace FS {

/** */
class Part :
		public Oscl::BT::Mesh::State::Generic::OnPowerUp::Persist::Api
		{
	private:
		/** */
		Oscl::Persist::
		Parent::Zephyr::FS::Part&	_parent;

		/** */
		static constexpr unsigned	maxFileName	= 10;

		/** */
		char						_fileName[maxFileName+1];

	private:
		/** */
		uint8_t		_powerUpState;

	public:
		/** */
		Part(
			Oscl::Persist::
			Parent::Zephyr::FS::Part&	parent,
			const char*					fileName
			) noexcept;

		/** */
		~Part() noexcept;

	public: // Oscl::BT::Mesh::State::Generic::OnPowerUp::Persist::Api
		/** */
		void	setPowerUpState(uint8_t state) noexcept;

		/** */
		uint8_t	getPowerUpState() const noexcept;

	private:
		/** */
		bool	read() noexcept;

		/** */
		void	write() noexcept;

		/** */
		void	destroyFile() noexcept;
	};

}
}
}
}
}
}
}
}
}
}

#endif
