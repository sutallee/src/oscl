/*
   Copyright (C) 2021 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_bluetooth_mesh_state_transition_utilsh_
#define _oscl_bluetooth_mesh_state_transition_utilsh_

#include <stdint.h>

/** */
namespace Oscl {
/** */
namespace BT {
/** */
namespace Mesh {
/** */
namespace State {
/** */
namespace Transition {

/**
	Mesh Model 3.1.3 Generic Default Transition Time

	Convert the encoded transitionTime to milliseconds.

	RETURN: time in milliseconds. If value returned is greater
	than 37,800,000 then the nSteps field was 0x3F indicating
	that the "remaining" time is unknown.
 */
unsigned	decodeToMilliseconds(uint8_t transitionTime) noexcept;

/**
	Mesh Model 3.1.3 Generic Default Transition Time

	Where:
		stepResolution is one of:
			0	- 100ms
			1	- 1s
			2	- 10s
			3	- 10 minutes

		numberOfSteps is in range 0-63

	If numberOfSteps is greater than 63, the
	stepResolution is set to 63.

	If stepResolution is greater than 3, then
	stepResolution is set to 3.

	If numberOfSteps is zero, then zero (immediate)
	is returned.

	RETURN: encoded transition time.

 */
uint8_t	encode(
			uint8_t	stepResolution,
			uint8_t	numberOfSteps
			) noexcept;

/**	This operation encodes a time expressed in milliseconds
	to a transitionTime encoded according to the format
	defined in:
	Mesh Model 3.1.3 Generic Default Transition Time
 */
uint8_t	encode(
			unsigned long	timeInMs
			) noexcept;

/**
	Mesh Model 3.1.3 Generic Default Transition Time

	Breakdown the encoded transitionTime into its
	constituent parts and return the time in milliseconds.

	RETURN: time in milliseconds
 */
unsigned	decode(
				uint8_t		transitionTime,
				uint8_t&	stepResolution,
				uint8_t&	numberOfSteps
				) noexcept;

unsigned	toMilliseconds(
				uint8_t	stepResolution,
				uint8_t	numberOfSteps
				) noexcept;
}
}
}
}
}

#endif
