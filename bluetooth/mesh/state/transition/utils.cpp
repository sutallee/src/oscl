/*
   Copyright (C) 2021 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "utils.h"

unsigned	Oscl::BT::Mesh::State::Transition::decodeToMilliseconds(uint8_t transitionTime) noexcept{

	unsigned
	nSteps	= (transitionTime & 0x3F);

	unsigned
	stepResolution	= (transitionTime >> 6) & 0x03;

	return toMilliseconds(
		stepResolution,
		nSteps
		);
	}

uint8_t	Oscl::BT::Mesh::State::Transition::encode(
			uint8_t stepResolution,
			uint8_t numberOfSteps
			) noexcept{

	if(!numberOfSteps){
		return 0;
		}

	if(numberOfSteps > 0x3E){
		numberOfSteps	= 0x3E;
		}

	if(stepResolution > 3){
		stepResolution	= 3;
		}

	uint8_t
	value	= (numberOfSteps & 0x3F);
	value	|= (stepResolution & 0x03)<<6;

	return value;
	}

unsigned	Oscl::BT::Mesh::State::Transition::decode(
				uint8_t		transitionTime,
				uint8_t&	stepResolution,
				uint8_t&	numberOfSteps
				) noexcept{

	numberOfSteps	= transitionTime & 0x3F;

	stepResolution	= transitionTime >> 6;

	return toMilliseconds(
			stepResolution,
			numberOfSteps
			);
	}

unsigned	Oscl::BT::Mesh::State::Transition::toMilliseconds(
				uint8_t	stepResolution,
				uint8_t	numberOfSteps
				) noexcept{

	unsigned
	transitionTimeInMs	= numberOfSteps;

	switch(stepResolution){
		case 0x00:
			// 100 milliseconds
			transitionTimeInMs	*= 100;
			break;
		case 0x01:
			// seconds
			transitionTimeInMs	*= 1000;
			break;
		case 0x02:
			// 10 seconds
			transitionTimeInMs	*= (1000*10);
			break;
		case 0x03:
			// 10 minutes
			transitionTimeInMs	*= (1000*10*60);
			break;
		};

	return transitionTimeInMs;
	}

uint8_t	Oscl::BT::Mesh::State::Transition::encode(
			unsigned long	timeInMs
			) noexcept{

	uint8_t	stepResolution	= 0;
	uint8_t	numberOfSteps	= 0;

	if(timeInMs > 37800000){
		// Unknown or higher than can be represented
		return 0xFF;
		}

	/*	The maximum time that can be represented
		is 62 * 10*60*1000 (37200000 ms).
	 */
	if(timeInMs > 37200000){
		stepResolution	= 3;
		numberOfSteps	= 62;
		return encode(
				stepResolution,
				numberOfSteps
				);
		}

	/*	The maximum time that can be represented
		with a stepResolution of 0x00 (100ms) is
		62 * 100 (6200 ms).
	 */
	if(timeInMs <= 6200 ){
		stepResolution	= 0;
		numberOfSteps	= timeInMs/100;
		}
	/*	The maximum time that can be represented
		with a stepResolution of 0x01 (1s) is
		62 * 1000 (62000 ms).
	 */
	else if(timeInMs <= 62000 ){
		stepResolution	= 1;
		numberOfSteps	= timeInMs/1000;
		}
	/*	The maximum time that can be represented
		with a stepResolution of 0x02 (10s) is
		62 * 10 * 1000 (620000 ms).
	 */
	else if(timeInMs <= 620000 ){
		stepResolution	= 2;
		numberOfSteps	= timeInMs/10000;
		}
	/*	The maximum time that can be represented
		with a stepResolution of 0x02 (10s) is
		62 * 10 * 60 * 1000 (620000 ms).
	 */
	else {
		stepResolution	= 3;
		numberOfSteps	= timeInMs/600000;
		}

	return encode(
			stepResolution,
			numberOfSteps
			);
	}

