/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_bluetooth_mesh_friend_creator_parth_
#define _oscl_bluetooth_mesh_friend_creator_parth_

#include <stdint.h>
#include <string.h>
#include "oscl/queue/queue.h"
#include "oscl/memory/block.h"
#include "oscl/done/operation.h"
#include "oscl/frame/fwd/composer.h"
#include "oscl/frame/fwd/itemcomp.h"
#include "oscl/bluetooth/mesh/bearer/tx/api.h"
#include "oscl/bluetooth/mesh/node/api.h"
#include "oscl/timer/factory/api.h"
#include "oscl/bluetooth/mesh/network/friendship/api.h"
#include "oscl/pdu/memory/api.h"

#include "oscl/bluetooth/mesh/friend/part.h"
#include "oscl/bluetooth/mesh/element/symbiont/kernel/api.h"
#include "oscl/bluetooth/mesh/network/api.h"

/** */
namespace Oscl {
/** */
namespace BT {
/** */
namespace Mesh {
/** */
namespace Friend {
/** */
namespace Creator {

/** This class is responsible for implementing the Friend
	function for a given Network.

	It handles the lifecycles of Friendships.
 */
class Part:
	public Oscl::BT::Mesh::Network::Friendship::Api,
	private Oscl::BT::Mesh::Friend::Part::ContextApi
	{
	public:
		/** */
		class ContextApi {
			public:
				/** */
				virtual Oscl::BT::Mesh::Network::Api&	getNetworkApi() noexcept=0;

				/** */
				virtual Oscl::BT::Mesh::Node::Api&	getNodeApi() noexcept=0;

				/** */
				virtual Oscl::BT::Mesh::Element::Symbiont::Kernel::Api&	getNetTxApi() noexcept=0;

				/** */
				virtual Oscl::BT::Mesh::Friend::Part*
						createFriend(
							Oscl::BT::Mesh::
							Friend::Part::ContextApi&	context,
							Oscl::Pdu::Memory::Api&		freeStore,
							uint32_t					pollTimeoutInMs,
							uint16_t					friendAddress,
							uint16_t					friendCounter,
							uint16_t					lpnAddress,
							uint16_t					lpnCounter,
							uint16_t					previousAddress,
							uint8_t						nElements,
							uint8_t						receiveDelayInMs
							) noexcept=0;

				/** */
				virtual void	free(Oscl::BT::Mesh::Friend::Part& part) noexcept=0;

				/** */
				virtual uint8_t	nSubscriptionsSupported() const noexcept=0;
			};

	private:
		/** */
		ContextApi&	_context;

		/** */
		Oscl::Timer::Factory::Api&	_timerFactoryApi;

		/** */
		Oscl::Pdu::Memory::Api&		_freeStore;

		/** */
		const uint16_t				_baseUnicastAddress;

		/** */
		bool						_enabled;

	private:
		/** */
		Oscl::Queue<Oscl::BT::Mesh::Friend::Part>	_friends;

	private:
		/** */
		const uint8_t	_supportedReceiveWindowInMs;

		/** */
		const uint8_t	_friendQueueSizeInMessages;

		/** */
		const uint8_t	_friendSubscriptionListSizeInEntries;

		/** */
		const uint8_t	_rssiInDBm;

		/** */
		uint16_t		_friendOfferCounter;

	private:
		/** */
		bool			_stopping;

	public:
		/** */
		Part(
			ContextApi&					context,
			Oscl::Timer::Factory::Api&	timerFactoryApi,
			Oscl::Pdu::Memory::Api&		freeStore,
			uint16_t					baseUnicastAddress,
			bool						enabled	= true
			) noexcept;

		/** */
		~Part() noexcept;

		/** */
		void	start() noexcept;

		/** */
		void	stop() noexcept;

		/** */
		void	enable() noexcept;

		/** */
		void	disable() noexcept;

		/** */
		bool	enabled() const noexcept;

	private: // Oscl::BT::Mesh::Network::Friendship::Api
		/** Process a received upper transport
			layer PDU.

			This operation is used by the Network
			layer to forward messages that are
			received by the Network layer encrypted
			using the master key material.

			From a Friendship perspective, these are
			should only Friend Request and Offer
			messages.

			All other Friendship related messages are
			encrypted using the Friendship credentials
			and are received through a separate channel.
		 */
		void	rxControl(
					const void*	upperTransportAccessPdu,
					unsigned	pduLength,
					uint32_t	ivIndex,
					uint32_t	seq,
					uint16_t	src,
					uint16_t	dst,
					uint8_t		ttl,
					uint8_t		opcode
					) noexcept;

		/** RETURN: true if the PDU was destined
			for a LPN.
		 */
		bool	rxLpnPDU(
					const void*		frame,
					unsigned		length,
					uint32_t		ivIndex,
					uint32_t		seq,
					uint16_t		src,
					uint16_t		dst,
					bool			ctl,
					uint8_t			ttl
					) noexcept;

		/** Process a received mesh network message
			that may belong to the implementation.

			RETURN: true if the frame belongs to a
			the friendship implementation.
		 */
		bool	rxNetwork(
					const void*	frame,
					unsigned 	length
					) noexcept;

		/** */
		void	rxBeacon(
					uint32_t	ivIndex,
					bool		keyRefreshFlag,
					bool		ivUpdateFlag
					) noexcept;

		/** */
		bool	wantsDestination(uint16_t address) const noexcept;

	private:
		/** */
		void	processFriendRequest(
					const void*	upperTransportAccessPdu,
					unsigned	pduLength,
					uint32_t	ivIndex,
					uint32_t	seq,
					uint16_t	src,
					uint16_t	dst
					) noexcept;

		/** */
		void	processFriendClearConfirm(
					const void*	upperTransportAccessPdu,
					unsigned	pduLength,
					uint32_t	ivIndex,
					uint32_t	seq,
					uint16_t	src,
					uint16_t	dst
					) noexcept;

		/** */
		void	processFriendClear(
					const void*	upperTransportAccessPdu,
					unsigned	pduLength,
					uint32_t	ivIndex,
					uint32_t	seq,
					uint16_t	src,
					uint16_t	dst,
					uint8_t		ttl
					) noexcept;

	private: // Oscl::BT::Mesh::Friend::Part::ContextApi

		/** */
		Oscl::BT::Mesh::Node::Api&	getNodeApi() noexcept;

		/** */
		Oscl::BT::Mesh::Network::Api&	getNetworkApi() noexcept;

		/** */
		void	stopped(Oscl::BT::Mesh::Friend::Part& part) noexcept;

		/** */
		Oscl::BT::Mesh::Element::Symbiont::Kernel::Api&	getNetTxApi() noexcept;

	private:
		/** */
		void	sendFriendOffer(uint16_t dst) noexcept;

		/** RETURN: zero if offer is unacceptable or
			resources are unavailable.
		 */
		Oscl::BT::Mesh::Friend::Part*
			evaluateFriendRequest(
				uint16_t	lpnAddress,
				uint8_t		criteria,
				uint8_t		receiveDelay,
				uint32_t	pollTimeoutIn100ms,
				uint16_t	previousAddress,
				uint8_t		nElements,
				uint16_t	lpnCounter
				) noexcept;

	};


}
}
}
}
}

#endif
