/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <new>
#include <string.h>
#include <stdlib.h>

#include "part.h"
#include "oscl/error/info.h"
#include "oscl/error/fatal.h"
#include "oscl/encoder/be/base.h"
#include "oscl/endian/decoder/linear/part.h"
#include "oscl/bluetooth/crypto/keys.h"
#include "oscl/bluetooth/crypto/salt.h"
#include "oscl/aes128/cipher.h"
#include "oscl/aes128/ccm.h"
#include "oscl/entropy/rand.h"
#include "oscl/bluetooth/mesh/address/types.h"
#include "oscl/bluetooth/mesh/address/values.h"

//#define DEBUG_TRACE
//#define DEBUG_TRACE_TMI

using namespace Oscl::BT::Mesh::Friend::Creator;

Part::Part(
	ContextApi&					context,
	Oscl::Timer::Factory::Api&	timerFactoryApi,
	Oscl::Pdu::Memory::Api&		freeStore,
	uint16_t					baseUnicastAddress,
	bool						enabled
	) noexcept:
		_context(context),
		_timerFactoryApi(timerFactoryApi),
		_freeStore(freeStore),
		_baseUnicastAddress(baseUnicastAddress),
		_enabled(enabled),
		_supportedReceiveWindowInMs(100),	// FIXME: randomly chosen
		_friendQueueSizeInMessages(64),	// FIXME: randomly chosen
		_friendSubscriptionListSizeInEntries(context.nSubscriptionsSupported()),
		_rssiInDBm(0x7F),	// FIXME: using unavaliable
		_friendOfferCounter(0),
		_stopping(false)
		{
	}

Part::~Part() noexcept{
	}

void	Part::start() noexcept{
	// Nothing to do.
	}

void	Part::stop() noexcept{

	_stopping	= true;

	Oscl::BT::Mesh::Friend::Part* f;

	while((f=_friends.get())){
		f->stop();
		}
	}

void	Part::enable() noexcept{
	_enabled	= true;
	}

void	Part::disable() noexcept{
	/*
		If the Friend feature is supported and the Friend state
		changes to value 0x00 and if a node is a friend for
		one or more Low Power nodes, the node shall terminate
		all friend relationships and clear the associated
		Friend Queue.

		The Friend feature is supported if this class
		is used in the application.
	 */

	if(!_enabled){
		return;
		}

	_enabled	= false;

	Oscl::BT::Mesh::Friend::Part* f;

	while((f=_friends.get())){
		f->stop();
		}

	_enabled	= false;
	}

bool	Part::enabled() const noexcept{
	return _enabled;
	}


void	Part::rxControl(
			const void*	upperTransportAccessPdu,
			unsigned	pduLength,
			uint32_t	ivIndex,
			uint32_t	seq,
			uint16_t	src,
			uint16_t	dst,
			uint8_t		ttl,
			uint8_t		opcode
			) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif

	if(_stopping){
		return;
		}

	if(!_enabled){
		return;
		}

	// Master Security Credential control messages.

	/*
		This rxControl() operation is NEVER called
		for ACK or Heartbeat messages.
		That leaves only Friend Request, Friend Clear,
		and Friend Clear Confirm messages.
	 */

	/*
		The Friend Poll, Friend Update, and Friend Subscription List
		Add/Remove/Confirm messages, as well as stored messages that the
		Friend node delivers to the Low Power node, are always encrypted
		using the friendship security credentials.

		The Friend Clear and Friend Clear Confirm messages are always
		encrypted using the master security credentials.
	 */

	/*
		The Low Power node starts by sending the Friend Request
		message using the master security credentials.

		A Friend node responds with a Friend Offer message, again
		using the master security credentials.

		Both the Low Power node and the Friend node must use the
		master security credentials as neither device is in a
		friendship with the other and therefore cannot use the
		friendship security credentials.

		The Low Power node accepts the offer of friendship and sends
		a Friend Poll to confirm this using the friendship security
		credentials.

		The Friend node will respond to this using a Friend Update
		message.

		The Low Power node can now configure the friend subscription
		list by using the Friend Subscription List Add message,
		confirmed using the Friend Subscription List Confirm message
		from the Friend node.

		Both of these messages are sent using the friendship security
		credentials.
	 */
	switch(opcode){
		case 0x03:
			// Friend Request
			processFriendRequest(
				upperTransportAccessPdu,
				pduLength,
				ivIndex,
				seq,
				src,
				dst
				);
			break;
		case 0x06:
			// Friend Clear Confirm
			processFriendClearConfirm(
				upperTransportAccessPdu,
				pduLength,
				ivIndex,
				seq,
				src,
				dst
				);
			break;
		case 0x05:
			// Friend Clear
			processFriendClear(
				upperTransportAccessPdu,
				pduLength,
				ivIndex,
				seq,
				src,
				dst,
				ttl
				);
			break;
		case 0x04:
			// Friend Offer
		case 0x01:
			// Friend Poll
		case 0x02:
			// Friend Update
		case 0x07:
			// Friend Subscription List Add
		case 0x08:
			// Friend Subscription List Remove
		case 0x09:
			// Friend Subscription List Confirm
		default:
			Oscl::Error::Info::log(
				"%s: Unexpected network encrypted control PDU: opcode: 0x%2.2X.\n",
				OSCL_PRETTY_FUNCTION,
				opcode
				);
			return;
		}

	}

bool	Part::rxLpnPDU(
			const void*		frame,
			unsigned		length,
			uint32_t		ivIndex,
			uint32_t		seq,
			uint16_t		src,
			uint16_t		dst,
			bool			ctl,
			uint8_t			ttl
			) noexcept{

	// This operation is called to handle
	// network PDUs addressed to LPNs.
	//
	// The PDUs have been decoded using
	// the master security creditials.
	//
	// Our job is to queue these for
	// delivery to the LPN.
	// We must, also, perform a proxy
	// function for segmented messages
	// in which we acknowledge segments
	// on-behalf-of the LPN. 

	#ifdef DEBUG_TRACE_TMI
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif


	if(_stopping){
		return false;
		}

	if(!_enabled){
		return false;
		}

	bool
	isUnicast	=  Oscl::BT::Mesh::Address::isUnicastAddress(dst);

	// When we have a unicast address,
	// at most one friend will handle
	// the PDU and so we can stop the
	// iteration early once the PDU
	// is handled.

	Oscl::BT::Mesh::Friend::Part*	f;

	for(
		f	= _friends.first();
		f;
		f	= _friends.next(f)
		){

		bool
		handled	= f->rxLpnPDU(
						frame,
						length,
						ivIndex,
						seq,
						src,
						dst,
						ctl,
						ttl
						);

		bool
		match	= f->isLpnUnicastAddress(dst);

		if(isUnicast && handled && match){
			// We're done if the destination
			// address was a unicast address;
			// was handled by the rxLpnPDU()
			// call and matched the unicast
			// address of the LPN.
			// There is no need to continue
			// the iteration since it will
			// match no others.
			return true;
			}
		}

	return false;
	}

bool	Part::rxNetwork(
			const void*	frame,
			unsigned 	length
			) noexcept{

	#ifdef DEBUG_TRACE_TMI
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	Oscl::Error::Info::hexDump(
		frame,
		length
		);
	#endif

	if(_stopping){
		return false;
		}

	if(!_enabled){
		return false;
		}

	for(
		Oscl::BT::Mesh::Friend::Part*	f = _friends.first();
		f;
		f	= _friends.next(f)
		){

		bool
		handled	= f->rxNetwork(
					frame,
					length
					);

		if(handled){
			return true;
			}
		}

	return false;
	}

void	Part::rxBeacon(
			uint32_t	ivIndex,
			bool		keyRefreshFlag,
			bool		ivUpdateFlag
			) noexcept{
	// This is called from the Network when
	// a secure update beacon is received.

	if(_stopping){
		return;
		}

	if(!_enabled){
		return;
		}

	Oscl::BT::Mesh::Friend::Part*	f;
	for(
		f	= _friends.first();
		f;
		f	= _friends.next(f)
		){
		f->rxBeacon(
			ivIndex,
			keyRefreshFlag,
			ivUpdateFlag
			);
		}

	}

bool	Part::wantsDestination(uint16_t address) const noexcept{

	#ifdef DEBUG_TRACE_TMI
	Oscl::Error::Info::log(
		"%s: address: 0x%4.4X\n",
		__PRETTY_FUNCTION__,
		address
		);
	#endif

	if(!_enabled){
		return false;
		}

	if(address == Oscl::BT::Mesh::Address::allFriends){
		#ifdef DEBUG_TRACE
		Oscl::Error::Info::log(
			"%s: ALL Friends\n",
			__PRETTY_FUNCTION__
			);
		#endif

		return true;
		}

	return false;
	}

void	Part::processFriendRequest(
			const void*	upperTransportPdu,
			unsigned	pduLength,
			uint32_t	ivIndex,
			uint32_t	seq,
			uint16_t	src,
			uint16_t	dst
			) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif

	/* 3.6.6.3.1 Friend establishment

		The time between receiving the Friend Request message
		and sending the Friend Offer message is called
		the Friend Offer Delay and shall be computed based
		on the RSSIFactor field and the ReceiveWindowFactor
		field as defined in the Friend Request message on
		the supported ReceiveWindow and on the RSSI measured
		by the Friend node for the Friend Request message.
	 */

	// dst should always be the all-friends address.

	/*
		[0]	Criteria (1-octet)
			The criteria that a Friend node should support
			in order to participate in friendship negotiation
			[7]	RFU (set to zero)
			[6:5]	RSSI Factor
					0b00	1
					0b01	1.5
					0b10	2
					0b11	2.5
			[4:3]	Receive Window Factor
					0b00	1
					0b01	1.5
					0b10	2
					0b11	2.5
			[2:0]	Min Queue Size Log
					0b000	Prohibited (do not use)
					0b001	N = 2
					0b010	N = 4
					0b011	N = 8
					0b100	N = 16
					0b101	N = 32
					0b110	N = 64
					0b111	N = 128
		[1]	ReceiveDelay (1-octet)	units of milliseconds.
			Receive delay requested by the Low Power node.
			0x00-0x09	Prohibited
			0x0A-0xFF	milliseconds

		[2:4] PollTimeout (3 octets)
			The initial value of the PollTimeout timer
			set by the Low Power node

			0x000000-0x000009	Prohibited
			0x00000A-0x34BBFF	100*milliseconds (1s - 345,599.9s,95.9h)
			0x34BC00-0xFFFFFF	Prohibited

		[5:6] PreviousAddress (2 octets)
			Unicast address of the primary element of
			the previous friend.

		[7]	NumElements	(1 octet)
			 Number of elements in the Low Power node

		[8:9]	LPNCounter	(2 octets)
			Number of Friend Request messages that
			the Low Power node has sent
	 */

	Oscl::Endian::Decoder::Linear::Part	beDecoder(
		upperTransportPdu,
		pduLength
		);

	Oscl::Decoder::Api&	decoder	= beDecoder.be();

	/*
			[7]	RFU (set to zero)
			[6:5]	RSSI Factor
					0b00	1
					0b01	1.5
					0b10	2
					0b11	2.5
			[4:3]	Receive Window Factor
					0b00	1
					0b01	1.5
					0b10	2
					0b11	2.5
			[2:0]	Min Queue Size Log
					0b000	Prohibited (do not use)
					0b001	N = 2
					0b010	N = 4
					0b011	N = 8
					0b100	N = 16
					0b101	N = 32
					0b110	N = 64
					0b111	N = 128
	*/
	uint8_t	criteria;

	/*
			0x00-0x09	Prohibited
			0x0A-0xFF	units of 1 millisecond

	*/
	uint8_t	receiveDelayInMs;

	/*
			0x000000-0x000009	Prohibited
			0x00000A-0x34BBFF	100*milliseconds (1s - 345,599.9s,95.9h)
			0x34BC00-0xFFFFFF	Prohibited
	*/

	uint32_t	pollTimeoutIn100ms;

	/*
		[5:6] PreviousAddress (2 octets)
			Unicast address of the primary element of
			the previous friend.
	*/
	uint16_t	previousAddress;

	/*
		[7]	NumElements	(1 octet)
			 Number of elements in the Low Power node
	*/
	uint8_t	nElements;

	/*
		[8:9]	LPNCounter	(2 octets)
			Number of Friend Request messages that
			the Low Power node has sent
	*/
	uint16_t	lpnCounter;

	uint8_t	opcodeUnused;

	decoder.decode(opcodeUnused);
	decoder.decode(criteria);
	decoder.decode(receiveDelayInMs);
	decoder.decode24Bit(pollTimeoutIn100ms);
	decoder.decode(previousAddress);
	decoder.decode(nElements);
	decoder.decode(lpnCounter);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			OSCL_PRETTY_FUNCTION
			);
		return;
		}

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		" criteria: 0x%2.2X:\n"
		"   rssiFactor: %u\n"
		"   receiveWindowFactor: %u\n"
		"   minQueueSizeLog: %u\n"
		" receiveDelay: %u ms\n"
		" pollTimeoutInMs: %u ms\n"
		" previousAddress: 0x%4.4X\n"
		" nElements: %u\n"
		" lpnCounter: %u\n"
		"",
		criteria,
		((criteria >> 5) & 0x03),
		((criteria >> 3) & 0x03),
		((criteria >> 0) & 0x07),
		receiveDelayInMs,
		pollTimeoutIn100ms*100,
		previousAddress,
		nElements,
		lpnCounter
		);
	#endif

	Oscl::BT::Mesh::Friend::Part*
	f	= evaluateFriendRequest(
			src,
			criteria,
			receiveDelayInMs,
			pollTimeoutIn100ms,
			previousAddress,
			nElements,
			lpnCounter
			);

	if(!f){
		return;
		}

	sendFriendOffer(src);
	}

void	Part::processFriendClearConfirm(
			const void*	upperTransportPdu,
			unsigned	pduLength,
			uint32_t	ivIndex,
			uint32_t	seq,
			uint16_t	src,
			uint16_t	dst
			) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif

	if(dst != _baseUnicastAddress){
		return;
		}

	/*
		[0:1]	LPNAddress (2-octets)
		[2:3]	LPNCounter (2-octets)
	 */

	Oscl::Endian::Decoder::Linear::Part
	beDecoder(
		upperTransportPdu,
		pduLength
		);

	Oscl::Decoder::Api&	decoder	= beDecoder.be();

	uint16_t	lpnAddress;
	uint16_t	lpnCounter;

	uint8_t		opcodeUnused;

	decoder.decode(opcodeUnused);
	decoder.decode(lpnAddress);
	decoder.decode(lpnCounter);
	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			OSCL_PRETTY_FUNCTION
			);
		return;
		}

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		" lpnAddress: 0x%4.4X\n"
		" lpnCounter: %u\n"
		"",
		lpnAddress,
		lpnCounter
		);
	#endif

	Oscl::BT::Mesh::Friend::Part*	f;

	for(
		f	= _friends.first();
		f;
		f	= _friends.next(f)
		){
		bool
		match	= f->rxFriendClearConfirm(
					lpnAddress,
					lpnCounter
					);
		if(match){
			break;
			}
		}

	}

void	Part::processFriendClear(
			const void*	upperTransportPdu,
			unsigned	pduLength,
			uint32_t	ivIndex,
			uint32_t	seq,
			uint16_t	src,
			uint16_t	dst,
			uint8_t		ttl
			) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif

	if(dst != _baseUnicastAddress){
		return;
		}

	/*
		[0:1]	LPNAddress (2-octets)
		[2:3]	LPNCounter (2-octets)
	 */

	Oscl::Endian::Decoder::Linear::Part	beDecoder(
		upperTransportPdu,
		pduLength
		);

	Oscl::Decoder::Api&	decoder	= beDecoder.be();

	uint16_t	lpnAddress;
	uint16_t	lpnCounter;

	uint8_t		opcodeUnused;

	decoder.decode(opcodeUnused);
	decoder.decode(lpnAddress);
	decoder.decode(lpnCounter);
	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			OSCL_PRETTY_FUNCTION
			);
		return;
		}

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		" lpnAddress: 0x%4.4X\n"
		" lpnCounter: %u\n"
		"",
		lpnAddress,
		lpnCounter
		);
	#endif

	Oscl::BT::Mesh::Friend::Part*	f;

	for(
		f	= _friends.first();
		f;
		f	= _friends.next(f)
		){
		bool
		match	= f->rxFriendClear(
					lpnAddress,
					lpnCounter,
					src,
					ttl
					);
		if(match){
			break;
			}
		}

	}

Oscl::BT::Mesh::Node::Api&	Part::getNodeApi() noexcept{
	return _context.getNodeApi();
	}

Oscl::BT::Mesh::Network::Api&	Part::getNetworkApi() noexcept{
	return _context.getNetworkApi();
	}

Oscl::BT::Mesh::Element::Symbiont::Kernel::Api&	Part::getNetTxApi() noexcept{
	return _context.getNetTxApi();
	}

void	Part::stopped(Oscl::BT::Mesh::Friend::Part& part) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s:\n",
		__PRETTY_FUNCTION__
		);
	#endif

	_friends.remove(&part);

	_context.free(part);
	}

void	Part::sendFriendOffer(
			uint16_t	dst
			) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif

	/*
		Friend Offer PDU [opcode = 0x03]:
		[0]	ReceiveWindow (1-octet)
			Receive Window value supported by the Friend node.
			0x00		Prohibited
			0x01-0xFF	Units of 1 ms.

		[1]	QueueSize (1-octet)
			Queue Size available on the Friend node
			- number of messages

		[2]	SubscriptionListSize (1-octet)
			Size of the Subscription List that can be
			supported by a Friend node for a Low Power node
			- number of entries

		[3]	RSSI (1-octet)
			RSSI measured by the Friend node.
			units dBm
			0x7F (127 dBm) if the signal strength is not available

		[4]	FriendCounter (2-octets)
			Number of Friend Offer messages that the Friend node has sent.
			The FriendCounter field value is set to the number of Friend Offer
			messages that the Friend node has sent.
	 */

	unsigned	bufferSize;

	void*
	buffer	= _freeStore.allocBuffer(bufferSize);

	if(!buffer){
		Oscl::Error::Info::log(
			"%s: Can't allocate buffer.\n",
			OSCL_PRETTY_FUNCTION
			);
		return;
		}

	Oscl::Encoder::BE::Base	beEncoder(
		buffer,
		bufferSize
		);

	Oscl::Encoder::Api&	encoder	= beEncoder;

	static const uint8_t	opcode		= 0x04;	// Friend Offer

	encoder.encode(dst);
	encoder.encode(opcode);
	encoder.encode(_supportedReceiveWindowInMs);
	encoder.encode(_friendQueueSizeInMessages);
	encoder.encode(_friendSubscriptionListSizeInEntries);
	encoder.encode(_rssiInDBm);
	encoder.encode(_friendOfferCounter);

	if(encoder.overflow()){
		Oscl::Error::Info::log(
			"%s: overflow!\n",
			OSCL_PRETTY_FUNCTION
			);
		return;
		}

	++_friendOfferCounter;


	Oscl::Handle<Oscl::Pdu::Fixed>
	fixed	= _freeStore.allocFixed(
				buffer,
				encoder.length()
				);

	if(!fixed){
		Oscl::Error::Info::log(
			"%s: Can't allocate fixed pdu.\n",
			OSCL_PRETTY_FUNCTION
			);
		_freeStore.freeBuffer(buffer);
		return;
		}

	const uint32_t	ivIndex	= _context.getNodeApi().getTxIvIndex();
	const uint32_t	seq	= _context.getNodeApi().allocateSequenceNumber();

	_context.getNetTxApi().transmitControl(
		fixed,			// Oscl::Pdu::Pdu* pdu,
		ivIndex,		// uint32_t        ivIndex,
		seq,			// uint32_t        seq,
		_baseUnicastAddress,	// uint16_t        src,
		dst,			// uint16_t        dst,
		0x00			// uint8_t         ttl
		);
	}

Oscl::BT::Mesh::Friend::Part*
Part::evaluateFriendRequest(
		uint16_t	lpnAddress,
		uint8_t		criteria,
		uint8_t		receiveDelayInMs,
		uint32_t	pollTimeoutIn100ms,
		uint16_t	previousAddress,
		uint8_t		nElements,
		uint16_t	lpnCounter
		) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif

	// FIXME: for now, I just accept all requests.

	// Stop any friends for the same LPN
	Oscl::Queue<Oscl::BT::Mesh::Friend::Part>	rmQ	= _friends;

	Oscl::BT::Mesh::Friend::Part*	f;

	while((f = rmQ.get())){
		if(lpnAddress == f->lpnAddress()){
			f->stop();
			continue;
			}
		_friends.put(f);
		}

	f	= _context.createFriend(
			*this,
			_freeStore,
			pollTimeoutIn100ms * 100,	// pollTimeoutInMs
			_baseUnicastAddress,	//  uint16_t		friendAddress,
			_friendOfferCounter,	// uint16_t		friendCounter,
			lpnAddress,
			lpnCounter,
			previousAddress,
			nElements,
			receiveDelayInMs
			);

	if(f){
		_friends.put(f);
		f->start();
		}

	return f;
	}
