/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_bluetooth_mesh_friend_parth_
#define _oscl_bluetooth_mesh_friend_parth_

#include <stdint.h>
#include <string.h>
#include "oscl/queue/queueitem.h"
#include "oscl/memory/block.h"
#include "oscl/done/operation.h"
#include "oscl/frame/fwd/composer.h"
#include "oscl/frame/fwd/itemcomp.h"
#include "oscl/bluetooth/mesh/bearer/tx/api.h"
#include "oscl/bluetooth/mesh/node/api.h"
#include "oscl/timer/api.h"
#include "oscl/done/operation.h"
#include "oscl/bluetooth/mesh/network/cache/config.h"
#include "oscl/bluetooth/mesh/transport/lower/reassemble/access/reaper.h"
#include "oscl/bluetooth/mesh/transport/upper/rx/composer.h"
#include "oscl/bluetooth/mesh/transport/upper/control/rx/composer.h"
#include "oscl/bluetooth/mesh/network/crypto.h"
#include "record.h"
#include "oscl/bluetooth/mesh/friend/queue/item.h"

/** */
namespace Oscl {
/** */
namespace BT {
/** */
namespace Mesh {
/** */
namespace Friend {

/** */
namespace Confirm {

/** */
class Api : public Oscl::QueueItem {
	public:
		/** */
		virtual void	process() noexcept=0;
	};

}

/** This class is responsible for a single friendship
	with an LPN.
 */
class Part :
	private Queue::Api,
	public Oscl::QueueItem,
	private Oscl::BT::Mesh::Transport::Lower::Reassemble::Access::Reaper::ContextApi
	{
	public:
		/** */
		class ContextApi {
			public:
				/** */
				virtual Oscl::BT::Mesh::Network::Api&	getNetworkApi() noexcept=0;

				/** */
				virtual Oscl::BT::Mesh::Node::Api&	getNodeApi() noexcept=0;

				/** */
				virtual void	stopped(Oscl::BT::Mesh::Friend::Part& part) noexcept=0;

				/** */
				virtual Oscl::BT::Mesh::Element::Symbiont::Kernel::Api&	getNetTxApi() noexcept=0;
			};

	private:
		/** */
		class SubscriptionListConfirm : private Confirm::Api {
			private:
				/** */
				Part&			_context;

				/** */
				const uint8_t	_transactionNumber;

			public:
				/** */
				SubscriptionListConfirm(
					Part&	context,
					uint8_t transactionNumber
					) noexcept;

			private:
				/** */
				void	process() noexcept;
			};

		friend class SubscriptionListConfirm;

	private:
		/** */
		class PollConfirm : private Confirm::Api {
			private:
				/** */
				Part&			_context;

			public:
				/** */
				PollConfirm(
					Part&	context
					) noexcept;

			private:
				/** */
				void	process() noexcept;
			};

		friend class PollConfirm;

	private:

		/** */
		static constexpr unsigned  maxLowerTransportPduSize    = ((128)/8);

		/** */
		class LpnNetMsg : private Confirm::Api {
			private:
				/** */
				Part&			_context;

				/** We size this such that it is the size of
					the encrypted portion of a network PDU,
					which includes the destination address
					and the Lower TransportPDU.
				 */
				uint8_t			_pdu[1+maxLowerTransportPduSize];

				/** */
				const unsigned	_length;

				/** */
				const uint32_t	_ivIndex;

				/** */
				const uint32_t	_seq;

				/** */
				const uint16_t	_src;

				/** */
				const uint16_t	_dst;

				/** */
				const bool		_ctl;

				/** */
				const uint8_t	_ttl;

			public:
				/** */
				LpnNetMsg(
					Part&		context,
					const void*	pdu,
					unsigned	length,
					uint32_t	ivIndex,
					uint32_t	seq,
					uint16_t	src,
					uint16_t	dst,
					bool		ctl,
					uint8_t		ttl
					) noexcept;

			private:
				/** */
				void	process() noexcept;
			};

		friend class LpnNetMsg;

	private:
		/** */
		ContextApi&	_context;

		/** */
		Oscl::Timer::Api&	_timerApi;

		/** */
		Oscl::Timer::Api&	_clearRepeatTimerApi;

		/** */
		Oscl::Timer::Api&	_clearProcedureTimerApi;

		/** */
		Oscl::Timer::Api&	_receiveWindowTimerApi;

		/** */
		Oscl::Pdu::Memory::Api&		_freeStore;

		/**	PollTimeout timer is used to measure time between
			two consecutive requests sent by the Low Power
			node. If no requests are received by the Friend node
			before the PollTimeout timer expires, then the
			friendship is considered terminated.
		 */
		const uint32_t		_pollTimeoutInMs;

		/** */
		const uint16_t		_friendAddress;

		/** */
		const uint16_t		_lpnAddress;

		/** */
		const uint16_t		_lpnCounter;

		/** This is from the previousAddress field
			of the Friend Request message that
			resulted in the creation of this
			friendship. It is used to perform
			the Friend Clear operation.
		 */
		const uint16_t		_previousAddress;

		/** */
		const uint8_t		_nElements;

		/** */
		const uint8_t		_receiveDelayInMs;

	private: // Friendship Security Material
		/** */
		Oscl::BT::Mesh::Network::
		Crypto::Credentials			_credentials;

	private:
		/** */
		Oscl::BT::Mesh::Network::Cache::Config<10>	_cache;

		/** */
		Oscl::BT::Mesh::Network::Cache::Config<10>	_lpnQueueCache;

	private:
		/** */
		Oscl::Done::Operation<Part>	_pollTimeoutExpired;

		/** */
		Oscl::Done::Operation<Part>	_clearRepeatTimerExpired;

		/** */
		Oscl::Done::Operation<Part>	_clearProcedureTimerExpired;

	private:
		/** */
		Oscl::Done::Operation<Part>	_receiveDelayTimerExpired;

	private:
		/** */
		Oscl::BT::Mesh::Transport::
		Upper::Control::RX::Composer<Part>	_upperTransportControlRxComposer;

		/** */
		Oscl::BT::Mesh::Transport::
		Upper::RX::Composer<Part>				_upperTransportRxComposer;

	public:
		/** */
		union AccessReaperMem{
			/** */
			void*	__qitemlink;

			/** */
			struct {
				/** */
				Oscl::Memory::AlignedBlock<
					sizeof(
						Oscl::BT::Mesh::Transport::
						Lower::Reassemble::Access::Reaper
						)
				>			reaper;
				Oscl::Timer::Api*	ackTimer;
				Oscl::Timer::Api*	incompleteTimer;
				} mem;
			};

	private:
		/** */
		Oscl::Queue<AccessReaperMem>	_freeAccessReaperMem;

		/** */
		Oscl::Queue<
			Oscl::BT::Mesh::
			Transport::Lower::
			Reassemble::Access::Reaper
			>							_activeAccessReapers;

	private:
		/** */
		Oscl::Queue<SubscribeRecordMem>		_freeSubscribeRecordMem;

		/** */
		Oscl::Queue<SubscribeRecord>		_subscriptions;

	private:
		/** */
		Oscl::Queue<Queue::ItemMem>		_freeQueueItemMem;

		/** */
		Oscl::Queue<Queue::Item>		_lpnQueue;

		/** */
		Queue::Item*					_fsnQueueItem;

		/** */
		bool							_pollFSN;

		/** */
		bool							_fsn;

	private:
		/** */
		unsigned				_clearRepeatTimerInMs;

		/** */
		unsigned				_clearProcedureTimerInMs;

		/** */
		bool					_friendshipTerminated;

		/** */
		bool					_starting;

		/** */
		bool					_stopping;

	public:
		/** */
		Part(
			ContextApi&				context,
			Oscl::Timer::Api&		timerApi,
			Oscl::Timer::Api&		clearRepeatTimerApi,
			Oscl::Timer::Api&		clearProcedureTimerApi,
			Oscl::Timer::Api&		receiveWindowTimerApi,
			Oscl::Pdu::Memory::Api&	freeStore,
			SubscribeRecordMem*		subscribeRecordMem,
			unsigned				nSubscribeRecordMem,
			Queue::ItemMem*			queueItemMem,
			unsigned				nQueueItemMem,
			AccessReaperMem*		accessReaperMem,
			unsigned				nAccessReaperMem,
			uint32_t				pollTimeoutInMs,
			uint16_t				friendAddress,
			uint16_t				friendCounter,
			uint16_t				lpnAddress,
			uint16_t				lpnCounter,
			uint16_t				previousAddress,
			uint8_t					nElements,
			uint8_t					receiveDelayInMs
			) noexcept;

		/** */
		~Part() noexcept;

		/** */
		void	start() noexcept;

		/** */
		void	stop() noexcept;

		/** */
		uint16_t	lpnAddress() const noexcept;

	private: // Oscl::BT::Mesh::Transport::Lower::Reassemble::Access::Reaper::ContextApi
		/** This operation is invoked for each
			unique segment that is received.
			This filtered list may be used by
			Friends to maintain the Friend
			queue for segments destined to the LPN.
		 */
		void	segmentReceived(
					const void*		frame,
					unsigned		length,
					uint32_t		ivIndex,
					uint32_t		seq,
					uint16_t		src,
					uint16_t		dst,
					bool			ctl,
					uint8_t			ttl
					) noexcept;

		/**  */
		void	stopped(Oscl::BT::Mesh::Transport::Lower::Reassemble::Access::Reaper& part) noexcept;

	public: // Interface used by the Friend Creator.
		/** This operation is invoked by the
			Friend creator when a message is
			received with valid master security
			credentials and *possibly* destined
			for the LPN of this Friend.

			RETURN: true if the message is
			handled by this Friend.
		 */
		bool	rxLpnPDU(
					const void*		frame,
					unsigned		length,
					uint32_t		ivIndex,
					uint32_t		seq,
					uint16_t		src,
					uint16_t		dst,
					bool			ctl,
					uint8_t			ttl
					) noexcept;

		/** This operation is invoked by the
			Friend creator when a raw encrypted
			message is received that may be
			from the LPN of this Friend.

			The Friend will attempt to decrypt
			the message using its friendship
			security credentials and process
			the resulting message as required.

			RETURN: true if the frame belongs to
			this Friend.
		 */
		bool	rxNetwork(
					const void*	frame,
					unsigned 	length
					) noexcept;

		/** This operation is invoked by the
			Friend creator when the node's
			security state changes.

			The security state may change as
			the result of receiving a secure
			beacon or as the result of an
			internal change caused by the
			Node/Network security state.
		 */
		void	rxBeacon(
					uint32_t	ivIndex,
					bool		keyRefreshFlag,
					bool		ivUpdateFlag
					) noexcept;

		/** This operation may be invoked by the
			Friend Creator to determine if the
			address matches the unicast address
			of the LPN address range represented
			by this Friend.
		 */
		bool	isLpnUnicastAddress(uint16_t address) const noexcept;

		/** This operation is invoked by the
			Friend Creator when it receives
			a Friend Clear Confirm message
			encrypted with the master security
			credentials.

			The Friend will determine if this
			Friend Clear Confirmation applies
			to it and take any required action
			if it does.

			RETURN: true if this Friend is the
			target of the Friend Clear Confirm
			message.
		 */
		bool	rxFriendClearConfirm(
					uint16_t	lpnAddress,
					uint16_t	lpnCounter
					) noexcept;

		/** This operation is invoked by the
			Friend Creator when it receives
			a Friend Clear message encrypted
			with the master security credentials.

			The Friend will determine if this
			Friend Clear applies to it and take
			any required action if it does.

			RETURN: true if this Friend is the
			target of the Friend Clear message.
		 */
		bool	rxFriendClear(
					uint16_t	lpnAddress,
					uint16_t	lpnCounter,
					uint16_t	src,
					uint8_t		ttl
					) noexcept;

	private:
		/** */
		void	stopNext() noexcept;

	private:
		/** */
		void	pollTimeoutExpired() noexcept;

		/** */
		void	clearRepeatTimerExpired() noexcept;

		/** */
		void	clearProcedureTimerExpired() noexcept;

		/** */
		void	receiveDelayTimerExpired() noexcept;

		/** */
		void	sendFriendUpdate(bool friendQueueIsEmpty) noexcept;

		/** */
		void	sendFriendSubscriptionListConfirm(uint8_t transactionNumber) noexcept;

		/** RETURN: true if the address is in the LPN
			subscription list.
		 */
		bool	isSubscriptionAddress(uint16_t address) const noexcept;

		/** */
		void	pollConfirm() noexcept;

	private:
		/** */
		void	sendFriendClear() noexcept;

		/** */
		void	sendFriendClearConfirm(
					uint16_t	lpnAddress,
					uint16_t	lpnCounter,
					uint16_t	dst,
					uint8_t		ttl
					) noexcept;

	private:
		/** */
		void	generateFriendshipSecurityMaterial(
					const void*	netKey,
					uint16_t	lpnAddress,
					uint16_t	friendAddress,
					uint16_t	lpnCounter,
					uint16_t	friendCounter
					) noexcept;

		/** Friendship credentials.
		 */
		bool	processLocalNetworkPDU(
					const void*		frame,
					unsigned		length,
					uint32_t		ivIndex,
					uint32_t		seq,
					uint16_t		src,
					uint16_t		dst,
					bool			ctl,
					uint8_t			ttl
					) noexcept;

		/** */
		bool	processLpnNetworkPDU(
					const void*		frame,
					unsigned		length,
					uint32_t		ivIndex,
					uint32_t		seq,
					uint16_t		src,
					uint16_t		dst,
					bool			ctl,
					uint8_t			ttl
					) noexcept;

		/** */
		bool	addressWantedLocally(uint16_t dst) noexcept;

		/** */
		void	processUnsegmentedTransportControlMessage(
					uint16_t	srcAddress,
					uint16_t	dstAddress,
					uint32_t	seq,
					uint32_t	ivIndex,
					uint8_t		ttl,
					const void*	lowerTransportPDU,
					unsigned	length,
					uint8_t		opcode
					) noexcept;

		/** */
		void	processSegmentedLowerTransportControlMessage(
					uint16_t	srcAddress,
					uint16_t	dstAddress,
					uint32_t	seq,
					uint32_t	ivIndex,
					uint8_t		ttl,
					const void*	lowerTransportPDU,
					unsigned	length,
					uint8_t		opcode
					) noexcept;

		/** */
		void	processUpperTransportControlPDU(
					const void*	upperTransportPdu,
					unsigned	pduLength,
					uint32_t	ivIndex,
					uint32_t	seq,
					uint16_t	src,
					uint16_t	dst,
					uint8_t		ttl,
					uint8_t		opcode
					) noexcept;

		/** */
		void	forwardLowerTransportAckPduToAllElements(
					const void*	lowerTransportAckPDU,
					unsigned	pduLength,
					uint32_t	seq,
					uint16_t	src,
					uint16_t	dst
					) noexcept;

		/** */
		void	queueMessageToLPN(
					const void*		pdu,
					unsigned		length,
					uint32_t		ivIndex,
					uint32_t		seq,
					uint16_t		src,
					uint16_t		dst,
					bool			ctl,
					uint8_t			ttl
					) noexcept;

		/** */
		void	processTransportControlMessage(
					uint16_t	srcAddress,
					uint16_t	dstAddress,
					uint32_t	seq,
					uint32_t	ivIndex,
					uint8_t		ttl,
					const void*	lowerTransportPDU,
					unsigned	length
					) noexcept;

		/** */
		void	processLowerTransport(
					uint16_t	srcAddress,
					uint16_t	dstAddress,
					uint32_t	ivIndex,
					uint32_t	seq,
					uint8_t		ttl,
					const void*	lowerTransportPDU,
					unsigned	length
					) noexcept;

		/** */
		void	processLpnLowerTransport(
					uint16_t	srcAddress,
					uint16_t	dstAddress,
					uint32_t	ivIndex,
					uint32_t	seq,
					uint8_t		ttl,
					const void*	lowerTransportPDU,
					unsigned	length
					) noexcept;

		/** */
		void	forwardUpperTransportPduToAllElements(
					const void*	upperTransportAccessPdu,
					unsigned	pduLength,
					uint32_t	ivIndex,
					uint32_t	seq,
					uint16_t	src,
					uint16_t	dst,
					uint8_t		aid,
					bool		akf,
					bool		segmented
					) noexcept;

	private:
		/** */
		void	processFriendPoll(
					uint16_t	srcAddress,
					uint16_t	dstAddress,
					uint32_t	seq,
					uint32_t	ivIndex,
					uint8_t		ttl,
					const void*	lowerTransportPDU,
					unsigned	length
					) noexcept;

		/** */
		void	processFriendSubscriptionListAdd(
					uint16_t	srcAddress,
					uint16_t	dstAddress,
					uint32_t	seq,
					uint32_t	ivIndex,
					uint8_t		ttl,
					const void*	lowerTransportPDU,
					unsigned	length
					) noexcept;

		/** */
		void	processFriendSubscriptionListRemove(
					uint16_t	srcAddress,
					uint16_t	dstAddress,
					uint32_t	seq,
					uint32_t	ivIndex,
					uint8_t		ttl,
					const void*	lowerTransportPDU,
					unsigned	length
					) noexcept;

		/** The transportPDU has the destination
			address pre-pended.

			param [in] ctl		Determines the MIC length
			param [in] ttl		Time-To-Live value
			param [in] src		Source address
			param [in] seq		24-bit Sequence number
			param [in] ivIndex	IV Index
			param [in] pdu		Pointer to the lower transport
								PDU. The destination address is
								encoded in the first two octets.
			param [in] pduLen	Length of the PDU
		 */
		Oscl::Pdu::Pdu*	buildNetworkPDU(
							bool		ctl,
							uint8_t		ttl,
							uint16_t	src,
							uint32_t	seq,
							uint32_t	ivIndex,
							const void*	pdu,
							unsigned	pduLen
							) noexcept;
	private:
		/** */
		void	free(SubscribeRecord* r) noexcept;

		/** */
		SubscribeRecord*	createSubscribeRecord(uint16_t address) noexcept;

		/** */
		SubscribeRecord*	find(uint16_t address) noexcept;

		/** */
		void	remove(uint16_t address) noexcept;

	private: // Queue::Api
		/** */
		void	sendNetMsgToLPN(
					const void*		frame,
					unsigned		length,
					uint32_t		ivIndex,
					uint32_t		seq,
					uint16_t		src,
					uint16_t		dst,
					bool			ctl,
					uint8_t			ttl
					) noexcept;

	private:
		/** */
		Queue::Item*	getOldestNonUpdateMessage() noexcept;

		/** */
		bool	duplicateMessageInLpnQueue(
					uint16_t	src,
					uint32_t	seq
					) noexcept;
	private:
		/** */
		void	transmitCantAcceptACK(
				uint16_t	srcAddress,
				uint16_t	dstAddress,
				uint32_t	ivIndex,
				uint32_t	seq,
				uint8_t		ttl,
				const void*	frame,
				unsigned	length
				) noexcept;
	};


}
}
}
}

#endif
