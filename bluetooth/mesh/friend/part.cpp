/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <new>
#include <string.h>
#include <stdlib.h>

#include "part.h"
#include "oscl/error/info.h"
#include "oscl/error/fatal.h"
#include "oscl/encoder/be/base.h"
#include "oscl/endian/decoder/linear/part.h"
#include "oscl/bluetooth/crypto/keys.h"
#include "oscl/bluetooth/crypto/salt.h"
#include "oscl/aes128/cipher.h"
#include "oscl/aes128/ccm.h"
#include "oscl/entropy/rand.h"
#include "oscl/bluetooth/mesh/address/types.h"
#include "oscl/bluetooth/mesh/address/values.h"
#include "oscl/bluetooth/mesh/network/constants.h"
#include "oscl/bluetooth/mesh/network/crypto.h"

//#define DEBUG_TRACE
//#define DEBUG_TRACE_TMI

using namespace Oscl::BT::Mesh::Friend;

Part::Part(
	ContextApi&				context,
	Oscl::Timer::Api&		timerApi,
	Oscl::Timer::Api&		clearRepeatTimerApi,
	Oscl::Timer::Api&		clearProcedureTimerApi,
	Oscl::Timer::Api&		receiveWindowTimerApi,
	Oscl::Pdu::Memory::Api&	freeStore,
	SubscribeRecordMem*		subscribeRecordMem,
	unsigned				nSubscribeRecordMem,
	Queue::ItemMem*			queueItemMem,
	unsigned				nQueueItemMem,
	AccessReaperMem*		accessReaperMem,
	unsigned				nAccessReaperMem,
	uint32_t				pollTimeoutInMs,
	uint16_t				friendAddress,
	uint16_t				friendCounter,
	uint16_t				lpnAddress,
	uint16_t				lpnCounter,
	uint16_t				previousAddress,
	uint8_t					nElements,
	uint8_t					receiveDelayInMs
	) noexcept:
		_context(context),
		_timerApi(timerApi),
		_clearRepeatTimerApi(clearRepeatTimerApi),
		_clearProcedureTimerApi(clearProcedureTimerApi),
		_receiveWindowTimerApi(receiveWindowTimerApi),
		_freeStore(freeStore),
		_pollTimeoutInMs(pollTimeoutInMs),
		_friendAddress(friendAddress),
		_lpnAddress(lpnAddress),
		_lpnCounter(lpnCounter),
		_previousAddress(previousAddress),
		_nElements(nElements),
		_receiveDelayInMs(receiveDelayInMs),
		_pollTimeoutExpired(
			*this,
			&Part::pollTimeoutExpired
			),
		_clearRepeatTimerExpired(
			*this,
			&Part::clearRepeatTimerExpired
			),
		_clearProcedureTimerExpired(
			*this,
			&Part::clearProcedureTimerExpired
			),
		_receiveDelayTimerExpired(
			*this,
			&Part::receiveDelayTimerExpired
			),
		_upperTransportControlRxComposer(
			*this,
			&Part::processUpperTransportControlPDU
			),
		_upperTransportRxComposer(
			*this,
			&Part::forwardUpperTransportPduToAllElements
			),
		_fsnQueueItem(0),
		_fsn(false),
		_clearRepeatTimerInMs(0),
		_clearProcedureTimerInMs(0),
		_friendshipTerminated(false),
		_starting(true),
		_stopping(false)
		{

	for(unsigned i=0;i<nSubscribeRecordMem;++i){
		_freeSubscribeRecordMem.put(&subscribeRecordMem[i]);
		}

	for(unsigned i=0;i<nQueueItemMem;++i){
		_freeQueueItemMem.put(&queueItemMem[i]);
		}

	for(unsigned i=0;i<nAccessReaperMem;++i){
		_freeAccessReaperMem.put(&accessReaperMem[i]);
		}

	generateFriendshipSecurityMaterial(
		_context.getNetworkApi().getNetKey(),
		lpnAddress,
		friendAddress,
		lpnCounter,
		friendCounter
		);
	}

Part::~Part() noexcept{
	}

void	Part::start() noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif

	_timerApi.setExpirationCallback(_pollTimeoutExpired);
	_timerApi.start(_pollTimeoutInMs);

	}

void	Part::stop() noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif

	if(!_starting){
		Oscl::Error::Info::log(
			"Friendship Terminated with 0x%4.4X\n",
			_lpnAddress
			);
		}
	else {
		Oscl::Error::Info::log(
			"Friendship with 0x%4.4X never consummated. Aborted.\n",
			_lpnAddress
			);
		}

	if(_stopping){
		return;
		}

	_stopping	= true;

	_timerApi.stop();
	_receiveWindowTimerApi.stop();
	_clearRepeatTimerApi.stop();
	_clearProcedureTimerApi.stop();

	Oscl::BT::Mesh::Transport::
	Lower::Reassemble::Access::Reaper*	reaper;

	while((reaper=_activeAccessReapers.get())){
		reaper->stop();
		}

	stopNext();
	}

uint16_t	Part::lpnAddress() const noexcept{
	return _lpnAddress;
	}

void	Part::segmentReceived(
			const void*		frame,
			unsigned		length,
			uint32_t		ivIndex,
			uint32_t		seq,
			uint16_t		src,
			uint16_t		dst,
			bool			ctl,
			uint8_t			ttl
			) noexcept{
	queueMessageToLPN(
		frame,
		length,
		ivIndex,
		seq,
		src,
		dst,
		ctl,
		ttl
		);
	}

void	Part::stopped(Oscl::BT::Mesh::Transport::Lower::Reassemble::Access::Reaper& part) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif

	_activeAccessReapers.remove(&part);

	part.~Reaper();

	_freeAccessReaperMem.put((AccessReaperMem*)&part);

	if(_stopping){
		stopNext();
		}
	}

void	Part::stopNext() noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif

	if(_activeAccessReapers.first()){
		// Wait for all active access
		// receivers to stop.
		return;
		}

	_context.stopped(*this);
	}

bool	Part::isLpnUnicastAddress(uint16_t address) const noexcept{
	if(address < _lpnAddress){
		return false;
		}

	if(address < (_lpnAddress+_nElements)){
		return true;
		}

	return false;
	}

bool	Part::isSubscriptionAddress(uint16_t address) const noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s: 0x%4.4X\n",
		__PRETTY_FUNCTION__,
		address
		);
	#endif

	if(Oscl::BT::Mesh::Address::isUnicastAddress(address)){
		return false;
		}

	SubscribeRecord*	r;

	for(
		r	= _subscriptions.first();
		r;
		r	= _subscriptions.next(r)
		){
		if(address == r->address()){
			return true;
			}
		}

	return false;
	}

bool	Part::rxLpnPDU(
			const void*		frame,
			unsigned		length,
			uint32_t		ivIndex,
			uint32_t		seq,
			uint16_t		src,
			uint16_t		dst,
			bool			ctl,
			uint8_t			ttl
			) noexcept{

	// Used Master credentials.

	// The rxLpnPDU() function is *NEVER*
	// called for control messages.

	if(ctl){
		#ifdef DEBUG_TRACE
		Oscl::Error::Info::log(
			"%s: UNEXPECTED control message ignored.\n"
			"  length: %u\n"
			"  ivIndex: 0x%8.8X\n"
			"  seq: 0x%6.6X\n"
			"  src: 0x%4.4X\n"
			"  dst: 0x%4.4X\n"
			"  ctl: 0x%2.2X\n"
			"  ttl: %u\n"
			"",
			__PRETTY_FUNCTION__,
			length,
			ivIndex,
			seq,
			src,
			dst,
			ctl,
			ttl
			);
		Oscl::Error::Info::hexDump(
			frame,
			length
			);
		#endif
		return false;
		}

	// This operation is called to handle
	// network PDUs addressed to this LPN.
	//
	// The PDUs have been decoded using
	// the master security material.
	//
	// Our job is to queue these for
	// delivery to the LPN.
	//
	// We must, also, perform a proxy
	// function for segmented messages
	// in which we acknowledge segments
	// on-behalf-of the LPN. 

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s: dst: 0x%4.4X\n",
		__PRETTY_FUNCTION__,
		dst
		);
	#endif

	// We handle unicast and subscription
	// addresses differently.

	// Segmented messages addressed to unicast
	// addresses must be acknowleged on-belhalf-of
	// the LPN node using master security
	// material.

	// Segmented Subscription addresses are not unicast
	// addresses and, therefore are not acknowleged.
	// In this case, we can simply queue the message
	// for the LPN.

		/* 3.5.5 Friend Queue

		The Friend node shall have a Friend Queue for each
		friend Low Power node. The Friend Queue stores
		Lower Transport PDUs for a Low Power node.
		No field of the Lower Transport PDU shall be changed due
		to the message being in the Friend Queue.
		The CTL, TTL, SEQ, SRC, and DST fields shall be stored
		with the associated Lower Transport PDU.

		When a Friend node receives a message that is destined
		for a friend Low Power node (i.e., the destination of
		the message is a unicast address of an element of the
		Low Power node or in the Friend Subscription List)
		and the TTL field has a value of 2 or greater,
		then the TTL field value shall be decremented by 1,
		and the message shall be stored into the Friend Queue.
	*/

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"  length: %u\n"
		"  ivIndex: 0x%8.8X\n"
		"  seq: 0x%6.6X\n"
		"  src: 0x%4.4X\n"
		"  dst: 0x%4.4X\n"
		"  ctl: 0x%2.2X\n"
		"  ttl: %u\n"
		"",
		length,
		ivIndex,
		seq,
		src,
		dst,
		ctl,
		ttl
		);
	#endif

	if(ttl < 2){
		return true;
		}

	--ttl;

	bool	dstIsUnicastAddress			= isLpnUnicastAddress(dst);
	bool	dstIsSubscriptionAddress	= isSubscriptionAddress(dst);

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"  dstIsUnicastAddress: %s\n"
		"  dstIsSubscriptionAddress: %s\n"
		"",
		dstIsUnicastAddress?"true":"false",
		dstIsSubscriptionAddress?"true":"false"
		);
	#endif

	if(!dstIsUnicastAddress && !dstIsSubscriptionAddress){
		return false;
		}

	if(duplicateMessageInLpnQueue(src,seq)){
		return true;
		}

	// At this point, we have a Lower Transport PDU in which the
	// LPN friend node is interested.
	// Therefore, we need to queue it.

	// The rxLpnPDU() function is *NEVER*
	// called for control messages.

	processLpnLowerTransport(
		src,
		dst,
		ivIndex,
		seq,
		ttl,
		frame,
		length
		);

	return true;
	}

void	Part::processLpnLowerTransport(
			uint16_t	srcAddress,
			uint16_t	dstAddress,
			uint32_t	ivIndex,
			uint32_t	seq,
			uint8_t		ttl,
			const void*	lowerTransportPDU,
			unsigned	length
			) noexcept{

	// Used Master credentials.

	if(_stopping){
		return;
		}

	// This function only receives
	// non-control Lower Transport Messages.

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif

	// At this point, we have a Lower Transport PDU
	//	[0] - First octet
	//		[7] - SEG Field
	//			0 - Unsegmented
	//			1 - Segmented
	//		[6] - AKF Field
	//			0 - Device
	//			1 - Application
	//		[6:0] - AID Field

	uint8_t		segAkfAid;
		{
		Oscl::Endian::Decoder::Linear::Part
		beDecoder(
			lowerTransportPDU,
			length
			);

		Oscl::Decoder::Api&	decoder	= beDecoder.be();

		decoder.decode(segAkfAid);

		if(decoder.underflow()){
			Oscl::Error::Info::log(
				"%s: underflow!\n",
				OSCL_PRETTY_FUNCTION
				);
			return;
			}
		}

	bool
	segmented	= (segAkfAid >> 7)?true:false;

	bool
	akf	= ((segAkfAid >> 6) & 0x01)?true:false;

	uint8_t
	aid	= (segAkfAid & 0x3F);

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s: segmented: %s\n",
		__PRETTY_FUNCTION__,
		segmented?"true":"false"
		);
	#endif

	if(!segmented){
		// No need for reassembly
		queueMessageToLPN(
			lowerTransportPDU,
			length,
			ivIndex,
			seq,
			srcAddress,
			dstAddress,
			false,	// FIXME: redundant
			ttl
			);
		return;
		}

	// At this point, we must reassemble the PDU

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s: need reassbly\n"
		"\tSegment:\n"
		"",
		__PRETTY_FUNCTION__
		);

	Oscl::Error::Info::hexDump(
		lowerTransportPDU,
		length
		);
	#endif

	bool	handled	= false;

	for(
			Oscl::BT::Mesh::Transport::
			Lower::Reassemble::Access::Reaper*
			reaper	= _activeAccessReapers.first();
			reaper;
			reaper	= _activeAccessReapers.next(reaper)
		){
		handled	= reaper->receive(
			srcAddress,
			dstAddress,
			seq,
			lowerTransportPDU,
			length
			);
		}

	if(handled){
		return;
		}

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s: Reassembling On-Behalf-Of 0x%4.4X\n",
		OSCL_PRETTY_FUNCTION,
		dstAddress
		);
	#endif

	AccessReaperMem*
	mem	= _freeAccessReaperMem.get();
	if(!mem){
		Oscl::Error::Info::log(
			"%s: out of reaper memory\n",
			OSCL_PRETTY_FUNCTION
			);
		transmitCantAcceptACK(
			srcAddress,
			dstAddress,
			ivIndex,
			seq,
			ttl,
			lowerTransportPDU,
			length
			);
		return;
		}

	Oscl::BT::Mesh::Transport::
	Lower::Reassemble::Access::Reaper*
	reaper	= new(&mem->mem.reaper)
				Oscl::BT::Mesh::Transport::
				Lower::Reassemble::Access::Reaper(
					*this,	// context,
					_freeStore,
					*mem->mem.ackTimer,
					*mem->mem.incompleteTimer,
					_context.getNetTxApi(),
					_upperTransportRxComposer,
					ivIndex,
					seq,
					srcAddress,
					dstAddress,
					ttl,
					aid,
					akf,
					false,	// noAck
            		true	// obo
					);

	_activeAccessReapers.put(reaper);

	reaper->start();

	reaper->receive(
		srcAddress,
		dstAddress,
		seq,
		lowerTransportPDU,
		length
		);
	}

Queue::Item*	Part::getOldestNonUpdateMessage() noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif

	Queue::Item*	item;
	Queue::Item*	lastNonUpdateItem	= 0;
	for(
		item	= _lpnQueue.first();
		item;
		item	= _lpnQueue.next(item)
		){
		if(item->isUpdateItem()){
			continue;
			}
		lastNonUpdateItem	= item;
		}

	if(lastNonUpdateItem){
		Oscl::Error::Info::log(
			"%s: Dequeued/discarded %p\n",
			OSCL_PRETTY_FUNCTION,
			lastNonUpdateItem
			);
		_lpnQueue.remove(lastNonUpdateItem);
		}


	return lastNonUpdateItem;
	}

bool	Part::rxFriendClearConfirm(
			uint16_t	lpnAddress,
			uint16_t	lpnCounter
			) noexcept{

	if(!Oscl::BT::Mesh::Address::isUnicastAddress(lpnAddress)){
		// This should *never* happen, but we
		// can't control what other nodes send.

		// Of course, our _lpnAddress should always
		// be a unicast address, so this is probably
		// redundant.
		return false;
		}

	if(lpnAddress != _lpnAddress){
		return false;
		}

	if(lpnCounter != _lpnCounter){
		return false;
		}

	/* 3.6.6.3.1 Friend establishment

		3.	If a Friend Clear Confirm message (see Section 3.6.5.6) is received
			in response to the Friend Clear message, both timers shall be canceled
			and the procedure is complete.
	 */

	_clearRepeatTimerApi.stop();
	_clearProcedureTimerApi.stop();

	return true;
	}

bool	Part::rxFriendClear(
			uint16_t	lpnAddress,
			uint16_t	lpnCounter,
			uint16_t	src,
			uint8_t		ttl
			) noexcept{

	/* 3.6.5.5 Friend Clear

		This is a confirmed message and the sending node
		expects to receive a Friend Clear Confirm message in
		response.

		If the Friend node does not receive a response,
		the new Friend node should resend the message at
		doubling intervals (2, 4, 8, 16 seconds etc.) until
		it either receives a response or it reaches
		the Poll Timeout of the Low Power node.

		This message shall be sent using the master
		security credentials.
	 */

	if(!Oscl::BT::Mesh::Address::isUnicastAddress(lpnAddress)){
		// This should *never* happen, but we
		// can't control what other nodes send.

		// Of course, our _lpnAddress should always
		// be a unicast address, so this is probably
		// redundant.
		return false;
		}

	if(lpnAddress != _lpnAddress){
		return false;
		}

	/* 3.6.6.3.2 Friend messaging

		When a Friend node receives a Friend Clear message where
		the LPNAddress field is a friend Low Power node, and
		the LPNCounter field is within the range defined in
		(Section 3.6.5.6), the friendship is terminated and the Friend
		node shall discard all entries in the Friend Queue.
	 */

	/* 3.6.5.6 Friend Clear Confirm

		The message should only be sent in response to a valid
		Friend Clear message received within the Poll Timeout
		of the previous friend relationship, but it should send
		a Friend Clear Confirm message for each valid Friend Clear
		message that it receives in that period.

		A Friend Clear message is considered valid if the result
		of the subtraction of the value of the LPNCounter field
		of the Friend Request message (the one that initiated
		the friendship) from the value of the LPNCounter field
		of the Friend Clear message, modulo 65536, is in the
		range 0 to 255 inclusive.

		This message shall be sent using the master
		security credentials.

	 */

	unsigned
	counterMath	= (lpnCounter - _lpnCounter) % 65536;

	if(counterMath >= 0){
		if(counterMath <= 255){
			_friendshipTerminated	= true;
			sendFriendClearConfirm(
				lpnAddress,
				lpnCounter,
				src,
				ttl
				);
			return true;
			}
		}

	return false;
	}

bool	Part::rxNetwork(
			const void*	frame,
			unsigned 	length
			) noexcept{

	// This operation is called to
	// handle messages from a friend LPN.

	// We are looking for messages encrypted
	// using the friendship security credentials.

	// The messages may be addressed either
	// to this Friend or to any node
	// and encrypted/authenticated using
	// the friendship security credentials.

	// Messages addressed to this Friend
	// *may conceivably* be addressed to
	// an access element on this node
	// in which case the message should
	// be forwarded to the network layer
	// and up through to the addressed
	// element. We would expect any responses
	// to be transmitted back to the LPN
	// through this Friend using the friendship
	// security credentials.

	// Friend Control Messages addressed to this
	// Friend node are handled here. (e.g. Friend
	// Poll, Friend Subscription List Add/Remove,
	// etc.)

	// We expect the following messages
	// from the LPN in this "channel":
	//	* control PDUs
	//		*	ACK from LPN? I think not, but "possible".
	//		*	Friend Poll
	// 		*	Friend Subscription List Add/Remove
	//		*	Friend Clear? NO! (master security credentials)
	//		*	Friend Clear Confirm? NO! (master security credentials)
	//	*	Network Messages
	//		*	Apparently (Figure 3.20) shows that
	//			an LPN may send "normal" network messages
	//			that must subsequently re-encrypted and
	//			relayed using
	//			master security credentials.
	//		

	/*	3.6.6.2 Friendship security

		Depending on the value of the Publish Friendship Credentials Flag
		(see Section 4.2.2.4), the Low Power node sends a message using
		either the friendship security credentials or the master security
		credentials (see Section 3.8.6.3.1).
	 */

	/*	Publish Friendship Credentials Flag

		4 Foundation models

		4.2 State definitions

		4.2.2 Model Publication

		4.2.2.4 Publish Friendship Credentials Flag

		When Publish Friendship Credential Flag is set to 1 *and*
		the friendship security material is not available, the
		master security material shall be used.

		4.3.2.16 Config Model Publication Set

		The CredentialFlag field shall contain the new
		Publish Friendship Credentials Flag state (see Section 4.2.2.4).

	 */


	#ifdef DEBUG_TRACE_TMI
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif

	using namespace Oscl::BT::Mesh::Network;

	uint32_t	ivIndex;
	uint8_t		ctl;
	uint8_t		ttl;
	uint32_t	seq;
	uint16_t	src;

	uint8_t		plaintext[128];

	unsigned
	len	= Oscl::BT::Mesh::Network::Crypto::decrypt(
			_context.getNodeApi(),
			_credentials,
			frame,
			length,
			plaintext,
			ivIndex,
			ctl,
			ttl,
			seq,
			src
			);

	if(!len){
		#ifdef DEBUG_TRACE_TMI
		Oscl::Error::Info::log(
			"%s: decrypt failed.\n",
			__PRETTY_FUNCTION__
			);
		#endif
		return false;
		}

	// At this point, we have received a valid
	// Network PDU for this LPN Friend "network".

	bool
	duplicate	= _cache.messageInCache(
					src,
					seq,
					ivIndex
					);

	if(duplicate){
		#ifdef DEBUG_TRACE
		Oscl::Error::Info::log(
			"Duplicate friend message (src:0x%4.4X,seq:0x%6.6X,ivIndex: 0x%8.8X) in cache. Dropped.\n",
			src,
			seq,
			ivIndex
			);
		#endif
		return true;
		}

	_cache.addToCache(
		src,
		seq,
		ivIndex
		);

	uint16_t	dst;
	unsigned	remaining;
	unsigned	lowerTransportPduOffset;
		{
		Oscl::Endian::Decoder::Linear::Part
		beDecoder(
			plaintext,
			len
			);

		Oscl::Decoder::Api&	decoder	= beDecoder.be();

		decoder.decode(dst);

		if(decoder.underflow()){
			Oscl::Error::Info::log(
				"%s: underflow!\n",
				OSCL_PRETTY_FUNCTION
				);
			// Return true, since there's something
			// wrong with this PDU (or the preceding
			// software, and we don't want to relay
			// this PDU.
			return true;
			}

		remaining	= decoder.remaining();

		lowerTransportPduOffset	= len - remaining;
		}

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s:"
		" ctl: (%u) \"%s\","
		" ttl: (0x%2.2X) %u,"
		" seq: (0x%6.6X) %u,"
		" src: (0x%4.4X)"
		" dst: (0x%4.4X)"
		" ivIndex: (0x%8.8X)"
		"\n",
		__PRETTY_FUNCTION__,
		ctl,
		ctl?"control":"network",
		ttl,
		ttl,
		seq,
		seq,
		src,
		dst,
		ivIndex
		);
	#endif

	bool
	locallyDestined	= processLocalNetworkPDU(
						&plaintext[lowerTransportPduOffset],
						remaining,
						ivIndex,
						seq,
						src,
						dst,
						ctl,
						ttl
						);

	if(locallyDestined){
		return true;
		}

	return true;
	}

void	Part::rxBeacon(
			uint32_t	ivIndex,
			bool		keyRefreshFlag,
			bool		ivUpdateFlag
			) noexcept{

	#ifdef DEBUG_TRACE_TMI
	Oscl::Error::Info::log(
		"%s:"
		" ivIndex: 0x%8.8X,"
		" keyRefreshFlag: %s,"
		" ivUpdateFlag: %s"
		"\n",
		__PRETTY_FUNCTION__,
		ivIndex,
		keyRefreshFlag?"true":"false",
		ivUpdateFlag?"true":"false"
		);
	#endif

	// TODO:
	// 1) search message queue for matching Friend Update.
	// 2) If match is found, ignore.
	// 3) If no match is found, queue a Friend Update.
	}

bool	Part::processLocalNetworkPDU(
			const void*		frame,
			unsigned		length,
			uint32_t		ivIndex,
			uint32_t		seq,
			uint16_t		src,
			uint16_t		dst,
			bool			ctl,
			uint8_t			ttl
			) noexcept{

	// Used Friendship credentials.

	#ifdef DEBUG_TRACE_TMI
	Oscl::Error::Info::log(
		"%s:\n"
		"\tctl: %s\n"
		"\tttl: %u\n"
		"\tsrc: 0x%4.4X\n"
		"\tdst: 0x%4.4X\n"
		"\tseq: 0x%8.8X\n"
		"",
		__PRETTY_FUNCTION__,
		ctl?"true":"false",
		ttl,
		src,
		dst,
		seq
		);

	Oscl::Error::Info::hexDump(
		frame,
		length
		);
	#endif

	bool	isUnicastDestAddress	= (dst & 0x8000)?false:true;

	bool
	wantsDestination	= addressWantedLocally(dst);

	bool
	locallyDestined	= (isUnicastDestAddress && wantsDestination);

	if(!locallyDestined && !wantsDestination){
		return false;
		}

	if(ctl){
		// Transport Control Message
		processTransportControlMessage(
			src,
			dst,
			seq,
			ivIndex,
			ttl,
			frame,
			length
			);
		}
	else {
		processLowerTransport(
			src,
			dst,
			ivIndex,
			seq,
			ttl,
			frame,
			length
			);
		}

	return locallyDestined;
	}

void	Part::generateFriendshipSecurityMaterial(
			const void*	netKey,
			uint16_t	lpnAddress,
			uint16_t	friendAddress,
			uint16_t	lpnCounter,
			uint16_t	friendCounter
			) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s:\n"
		" lpnAddress: 0x%4.4X\n"
		" friendAddress: 0x%4.4X\n"
		" lpnCounter: 0x%8.8X\n"
		" friendCounter: 0x%8.8X\n"
		" netKey:\n"
		"",
		__PRETTY_FUNCTION__,
		lpnAddress,
		friendAddress,
		lpnCounter,
		friendCounter
		);

	Oscl::Error::Info::hexDump(
		netKey,
		16
		);
	#endif

	/*
	The friendship security material is derived from the friendship
	security credentials using:

	NID || EncryptionKey || PrivacyKey = k2(
			NetKey,
			0x01
		||	LPNAddress
		||	FriendAddress
		||	LPNCounter
		||	FriendCounter
		)
	 */

	const uint8_t	one = 0x01;

	uint8_t	buffer[
				sizeof(one)
			+	sizeof(lpnAddress)
			+	sizeof(friendAddress)
			+	sizeof(lpnCounter)
			+	sizeof(friendCounter)
			];

	Oscl::Encoder::BE::Base
	beEncoder(
		buffer,
		sizeof(buffer)
		);

	Oscl::Encoder::Api&	encoder	= beEncoder;

	encoder.encode(one);
	encoder.encode(lpnAddress);
	encoder.encode(friendAddress);
	encoder.encode(lpnCounter);
	encoder.encode(friendCounter);

	uint8_t	k2Output[48];

	Oscl::BT::Crypto::Keys::k2(
		netKey,
		buffer,
		encoder.length(),
		k2Output
		);

	memcpy(
		_credentials._privacyKey,
		&k2Output[sizeof(k2Output) - 16],
		16
		); /* 128-bit */

	memcpy(
		_credentials._encryptionKey,
		&k2Output[sizeof(k2Output) - 32],
		16
		); /* 128-bit */

	_credentials._nid	= k2Output[sizeof(k2Output) - 33] & 0x7f; /* 7-bit */
	}

void	Part::pollTimeoutExpired() noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif

	/* 3.6.5.6 Friend Clear Confirm

		The message should only be sent in response to a valid
		Friend Clear message received within the Poll Timeout
		of the previous friend relationship, but it should send
		a Friend Clear Confirm message for each valid Friend Clear
		message that it receives in that period.
	 */

	/* 3.6.5.5 Friend Clear

		This is a confirmed message and the sending node expects
		to receive a Friend Clear Confirm message in response.

		If the Friend node does not receive a response, the new
		Friend node should resend the message at doubling intervals
		(2, 4, 8, 16 seconds etc.) until it either receives a
		response or it reaches the Poll Timeout of the Low Power node.
	 */

	stop();
	}

void	Part::clearRepeatTimerExpired() noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif

	/* 3.6.6.3.1 Friend establishment

		4.	If the Friend Clear Repeat timer expires, a new Friend Clear message
			shall be sent and the timer shall be restarted with a period that is
			double the previous Friend Clear Repeat timer period. For example,
			after the first expiration, the period shall be set to two seconds;
			on the next expiration, it shall be set to four seconds, and so on.
	 */

	unsigned
	clearRepeatTimerInMs	= _clearRepeatTimerInMs * 2;

	// This condition prevents wrap-around
	// of _clearRepeatTimerInMs.
	// That *should* never happen.

	if(clearRepeatTimerInMs > _clearRepeatTimerInMs){
		_clearRepeatTimerInMs	= clearRepeatTimerInMs;
		}

	sendFriendClear();

	_clearRepeatTimerApi.start(_clearRepeatTimerInMs);
	}

void	Part::clearProcedureTimerExpired() noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif

	/* 3.6.6.3.1 Friend establishment

		5.	If the Friend Clear Procedure timer expires, then the Friend Clear Repeat
			timer shall be cancelled and the procedure is complete.
	*/

	_clearRepeatTimerApi.stop();
	}

void	Part::receiveDelayTimerExpired() noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif

	/*	This operation is invoked after the receive delay
		timer expires when a Friend Poll has been received.

		If the LPN Queue is not empty, the next message
		in the queue is transmitted.

		If the LPN Queue is empty, a Friend Update is transmitted.
	 */

	if(_fsn == _pollFSN){
		if(!_fsnQueueItem){
			_fsnQueueItem	= _lpnQueue.get();
			}
		}

	if(_fsn != _pollFSN){
		_fsn	= _pollFSN;
		if(_fsnQueueItem){
			_fsnQueueItem->~Item();
			_freeQueueItemMem.put((Queue::ItemMem*)_fsnQueueItem);
			}
		_fsnQueueItem	= _lpnQueue.get();
		}

	if(!_fsnQueueItem){
		sendFriendUpdate(true);
		}
	else {
		_fsnQueueItem->process();
		}

	if(!_clearRepeatTimerInMs){
		// Friendship Established
		// This marks the point where the Friendship
		// has been established.

		/* 3.6.6.3.1 Friend establishment

			After a friendship has been established, if the PreviousAddress field
			of the Friend Request message contains a valid unicast address that
			is not the Friend node’s own unicast address, then the Friend node
			shall begin sending Friend Clear messages (see Section 3.6.5.5)
			to that unicast address according to the procedure below:
		 */

		if(Oscl::BT::Mesh::Address::isUnicastAddress(_previousAddress)){
			if(_previousAddress != _friendAddress){

				#ifdef DEBUG_TRACE
				Oscl::Error::Info::log(
					"%s\n"
					" _friendAddress: 0x%4.4X\n"
					" _previousAddress: 0x%4.4X\n"
					"",
					__PRETTY_FUNCTION__,
					_friendAddress,
					_previousAddress
					);
				#endif

				/* 3.6.6.3.1 Friend establishment
					After a friendship has been established, if the PreviousAddress field
					of the Friend Request message contains a valid unicast address that
					is not the Friend node’s own unicast address, then the Friend node
					shall begin sending Friend Clear messages (see Section 3.6.5.5)
					to that unicast address according to the procedure below:


					2.	The first Friend Clear message shall be sent as soon as the
						friendship is established; at the same time, a Friend Clear Repeat
						timer shall be started with the period set to 1 second,
						and a Friend Clear Procedure timer shall be started with the period
						equal to two times the Friend Poll Timeout value.
 				 */

				sendFriendClear();

				_clearRepeatTimerApi.setExpirationCallback(_clearRepeatTimerExpired);
				_clearRepeatTimerInMs	= 1000;
				_clearRepeatTimerApi.start(_clearRepeatTimerInMs);

				_clearProcedureTimerApi.setExpirationCallback(_clearProcedureTimerExpired);
				_clearProcedureTimerInMs	= 2*_pollTimeoutInMs;
				_clearProcedureTimerApi.start(_clearProcedureTimerInMs);
				}
			}
		}

	}

void	Part::pollConfirm() noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif

	/*	This operation is invoked after the receive window
		timer expires when a Friend Poll has been received.

		If the LPN Queue is not empty, the next message
		in the queue is transmitted.

		If the LPN Queue is empty, a Friend Update is transmitted.
	 */

	// TODO: process friend/lpn queue.

	sendFriendUpdate(true);
	}

void	Part::sendFriendUpdate(bool friendQueueIsEmpty) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif

	uint8_t		buffer[16];
	
	Oscl::Encoder::BE::Base	beEncoder(
		buffer,
		sizeof(buffer)
		);

	Oscl::Encoder::Api&	encoder	= beEncoder;

	static const uint8_t	opcode		= 0x02;	// Friend Update

	uint32_t	ivIndex;
	bool		ivUpdateFlag;
	bool		keyRefreshFlag;

	_context.getNetworkApi().getBeaconState(
		ivIndex,
		ivUpdateFlag,
		keyRefreshFlag
		);

	const uint8_t	keyRefreshMask	= (keyRefreshFlag) ? (1<<0): 0;
	const uint8_t	ivUpdateMask	= (ivUpdateFlag) ? (1<<1) : 0;

	uint8_t flags	= keyRefreshMask | ivUpdateMask;

	/*
			[7:1]	Padding (set to zero)
			[0]	FSN
					LSB of the friend sequence number
	*/

	const uint32_t	seq	= _context.getNodeApi().allocateSequenceNumber();

	uint16_t	dst	= _lpnAddress;


	uint8_t	md	= (friendQueueIsEmpty)?0x00:0x01;

	encoder.encode(dst);
	encoder.encode(opcode);
	encoder.encode(flags);
	encoder.encode(ivIndex);
	encoder.encode(md);

	/*	3.6.6.4.2 Low Power messaging
		In a Friend Poll message, the TTL field shall be set to 0.
	 */

	Oscl::Handle<Oscl::Pdu::Pdu>
	pdu	= buildNetworkPDU(
			true,				// bool		ctl,
			0,					// uint8_t	ttl,
			_friendAddress,		// uint16_t	src,
			seq,				// uint32_t	seq,
			_context.getNodeApi().getTxIvIndex(),	// uint32_t	ivIndex,
			buffer,				// const void*	transportPDU,
			encoder.length()	// unsigned	tranportPduLength
			);

	if(!pdu){
		return;
		}

	_context.getNetTxApi().transmitNetworkPDU(
		pdu,	// Oscl::Pdu::Pdu* pdu,
		dst		// uint16_t        dst
		);
	}

void	Part::sendFriendSubscriptionListConfirm(uint8_t transactionNumber) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif

	uint8_t		buffer[16];
	
	Oscl::Encoder::BE::Base	beEncoder(
		buffer,
		sizeof(buffer)
		);

	Oscl::Encoder::Api&	encoder	= beEncoder;

	static const uint8_t	opcode		= 0x09;	// Friend Subscription List Confirm

	uint16_t	dst	= _lpnAddress;

	encoder.encode(dst);
	encoder.encode(opcode);
	encoder.encode(transactionNumber);

	const uint32_t	seq	= _context.getNodeApi().allocateSequenceNumber();

	const uint32_t	ivIndex	= _context.getNodeApi().getTxIvIndex();

	/*	3.6.6.4.2 Low Power messaging
		In a Friend Poll message, the TTL field shall be set to 0.
	 */

	Oscl::Handle<Oscl::Pdu::Pdu>
	pdu	= buildNetworkPDU(
			true,				// bool		ctl,
			0,					// uint8_t	ttl,
			_lpnAddress,		// uint16_t	src,
			seq,				// uint32_t	seq,
			ivIndex,			// uint32_t	ivIndex,
			buffer,				// const void*	transportPDU,
			encoder.length()	// unsigned	tranportPduLength
			);

	if(!pdu){
		return;
		}

	_context.getNetTxApi().transmitNetworkPDU(
		pdu,	// Oscl::Pdu::Pdu* pdu,
		dst		// uint16_t        dst
		);
	}

Oscl::Pdu::Pdu*	Part::buildNetworkPDU(
			bool		ctl,
			uint8_t		ttl,
			uint16_t	src,
			uint32_t	seq,
			uint32_t	ivIndex,
			const void*	transportPDU,
			unsigned	tranportPduLength
			) noexcept{

	using namespace Oscl::BT::Mesh::Network;

	#ifdef DEBUG_TRACE_TMI
	Oscl::Error::Info::log(
		"%s\n"
		"\tttl: %u\n"
		"\tctl: %s\n"
		"\ttransportPDU:\n"
		"",
		__PRETTY_FUNCTION__,
		ttl,
		ctl?"true":"false"
		);
	Oscl::Error::Info::hexDump(
		transportPDU,
		tranportPduLength
		);
	#endif

	// At this point, we are ready
	// to build a new network PDU
	// (with a new obfuscated header)
	// and forward it to all bearers.

	unsigned	bufferSize;

	void*
	mem	= _freeStore.allocBuffer(bufferSize);

	if(!mem){
		Oscl::Error::Info::log(
			"%s: out of buffer memory.\n",
			OSCL_PRETTY_FUNCTION
			);
		return 0;
		}

	unsigned
	length	= Oscl::BT::Mesh::Network::Crypto::encrypt(
			_credentials,
			ctl,
			ttl,
			src,
			seq,
			ivIndex,
			transportPDU,
			tranportPduLength,
			mem,
			bufferSize
			);

	if(!length){
		_freeStore.freeBuffer(mem);
		Oscl::Error::Info::log(
			"%s: encrypt failed!\n",
			OSCL_PRETTY_FUNCTION
			);
		return 0;
		}

	Oscl::Pdu::Pdu*
	fixed	= _freeStore.allocFixed(
				mem,
				length
				);

	if(!fixed){
		_freeStore.freeBuffer(mem);
		Oscl::Error::Info::log(
			"%s: out of fixed memory.\n",
			OSCL_PRETTY_FUNCTION
			);
		return 0;
		}

	return fixed;
	}

bool	Part::addressWantedLocally(uint16_t dst) noexcept{

	if(dst == _friendAddress){
		return true;
		}

	if(find(dst)){
		return true;
		}

	return false;
	}

void	Part::processTransportControlMessage(
			uint16_t	srcAddress,
			uint16_t	dstAddress,
			uint32_t	seq,
			uint32_t	ivIndex,
			uint8_t		ttl,
			const void*	lowerTransportPDU,
			unsigned	length
			) noexcept{

	// Used Friendship credentials.

	#ifdef DEBUG_TRACE_TMI
	Oscl::Error::Info::log(
		"%s\n"
		"\tsrcAddress: 0x%4.4X\n"
		"\tdstAddress: 0x%4.4X\n"
		"\tseq: 0x%6.6X\n"
		"",
		__PRETTY_FUNCTION__,
		srcAddress,
		dstAddress,
		seq
		);
	#endif

	// At this point, we have a Lower Transport Control PDU
	//	[0] - First octet
	//		[7] - SEG Field
	//			0 - Unsegmented
	//			1 - Segmented
	//		[6:0] - Opcode

	uint8_t	segOpcode;
		{
		Oscl::Endian::Decoder::Linear::Part
		beDecoder(
			lowerTransportPDU,
			length
			);

		Oscl::Decoder::Api&	decoder	= beDecoder.be();

		decoder.decode(segOpcode);

		if(decoder.underflow()){
			Oscl::Error::Info::log(
				"%s: underflow!\n",
				OSCL_PRETTY_FUNCTION
				);
			return;
			}
		}

	bool
	segmented	= (segOpcode >> 7)?true:false;

	uint8_t
	opcode	= segOpcode & 0x7F;

	if(segmented){
		processSegmentedLowerTransportControlMessage(
			srcAddress,
			dstAddress,
			seq,
			ivIndex,
			ttl,
			lowerTransportPDU,
			length,
			opcode
			);

		return;
		}

	// At this point, we have an unsegmented
	// control message.

	if(opcode){
		// Not an ACK
		// This encompases heartbeat and
		// Friendship control messages that
		// are not supported by LPNs?
		processUnsegmentedTransportControlMessage(
			srcAddress,
			dstAddress,
			seq,
			ivIndex,
			ttl,
			lowerTransportPDU,
			length,
			opcode
			);

		return;
		}

	// At this point, we have a segment acknowledge
	// control message.

	const uint8_t*
	lowerTransportAckPdu	= (const uint8_t*)lowerTransportPDU;

	forwardLowerTransportAckPduToAllElements(
		lowerTransportAckPdu,
		length,
		seq,
		srcAddress,
		dstAddress
		);

	return;
	}

void	Part::processFriendPoll(
			uint16_t	srcAddress,
			uint16_t	dstAddress,
			uint32_t	seq,
			uint32_t	ivIndex,
			uint8_t		ttl,
			const void*	lowerTransportPDU,
			unsigned	length
			) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif

	if(_starting){
		_starting	= false;
		Oscl::Error::Info::log(
			"Friendship Established with 0x%4.4X\n",
			_lpnAddress
			);
		}

	Oscl::Endian::Decoder::Linear::Part
	beDecoder(
		lowerTransportPDU,
		length
		);

	Oscl::Decoder::Api&	decoder	= beDecoder.be();

	uint8_t		opcodeUnused;
	uint8_t		fsnAndPad;

	decoder.decode(opcodeUnused);
	decoder.decode(fsnAndPad);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			OSCL_PRETTY_FUNCTION
			);
		return;
		}


	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		" fsnAndPad: 0x%2.2X:\n"
		"   fsn: %u\n"
		"",
		fsnAndPad,
		((fsnAndPad >> 0) & 0x01)
		);
	#endif

	_pollFSN	= ((fsnAndPad >> 0) & 0x01)?true:false;

	_receiveWindowTimerApi.setExpirationCallback(_receiveDelayTimerExpired);
	_receiveWindowTimerApi.start(_receiveDelayInMs);

	_timerApi.start(_pollTimeoutInMs);
	}

void	Part::processFriendSubscriptionListAdd(
			uint16_t	srcAddress,
			uint16_t	dstAddress,
			uint32_t	seq,
			uint32_t	ivIndex,
			uint8_t		ttl,
			const void*	lowerTransportPDU,
			unsigned	length
			) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif

	Oscl::Endian::Decoder::Linear::Part
	beDecoder(
		lowerTransportPDU,
		length
		);

	Oscl::Decoder::Api&	decoder	= beDecoder.be();

	/*
		[0]	TransactionNumber (1 octet)  The number for identifying a transaction
				The TransactionNumber field is used to distinguish each
				individual transaction (see Section 3.6.6.4.3).

		[1:2]*N	AddressList  List of group addresses and virtual addresses
				where N is the number of group addresses and virtual addresses
				in this message

	 */
	uint8_t		opcodeUnused;
	uint8_t		transactionNumber;

	decoder.decode(opcodeUnused);
	decoder.decode(transactionNumber);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			OSCL_PRETTY_FUNCTION
			);
		return;
		}

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		" transactionNumber: 0x%2.2X:\n"
		" AddressList: remaining: %u\n"
		"",
		transactionNumber,
		decoder.remaining()
		);
	#endif

	for(;;){
		uint16_t	address;
		decoder.decode(address);
		if(decoder.underflow()){
			break;
			}
		#ifdef DEBUG_TRACE
		Oscl::Error::Info::log(
			"   0x%4.4X:\n"
			"",
			address
			);
		#endif

		if(find(address)){
			// We've already got one!
			continue;
			}

		// Add address to the subscription list.

		SubscribeRecord*
		r	= createSubscribeRecord(address);

		if(!r){
			// Out of subscription record memory.
			Oscl::Error::Info::log(
				"%s: no more subscription records!\n",
				OSCL_PRETTY_FUNCTION
				);
			break;
			}

		_subscriptions.put(r);
		}

	sendFriendSubscriptionListConfirm(transactionNumber);

	_timerApi.start(_pollTimeoutInMs);
	}

void	Part::processFriendSubscriptionListRemove(
			uint16_t	srcAddress,
			uint16_t	dstAddress,
			uint32_t	seq,
			uint32_t	ivIndex,
			uint8_t		ttl,
			const void*	lowerTransportPDU,
			unsigned	length
			) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif

	Oscl::Endian::Decoder::Linear::Part
	beDecoder(
		lowerTransportPDU,
		length
		);

	Oscl::Decoder::Api&	decoder	= beDecoder.be();

	/*
		[0]	TransactionNumber (1 octet)  The number for identifying a transaction
				The TransactionNumber field is used to distinguish each
				individual transaction (see Section 3.6.6.4.3).

		[1:2]*N	AddressList  List of group addresses and virtual addresses
				where N is the number of group addresses and virtual addresses
				in this message

	 */
	uint8_t		opcodeUnused;
	uint8_t		transactionNumber;

	decoder.decode(opcodeUnused);
	decoder.decode(transactionNumber);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			OSCL_PRETTY_FUNCTION
			);
		return;
		}

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		" transactionNumber: 0x%2.2X:\n"
		" AddressList: remaining: %u\n"
		"",
		transactionNumber,
		decoder.remaining()
		);
	#endif

	for(;;){
		uint16_t	address;
		decoder.decode(address);
		if(decoder.underflow()){
			break;
			}
		#ifdef DEBUG_TRACE
		Oscl::Error::Info::log(
			"   0x%4.4X:\n"
			"",
			address
			);
		#endif
		// Remove address from the subscription list.
		remove(address);
		}

	sendFriendSubscriptionListConfirm(transactionNumber);

	_timerApi.start(_pollTimeoutInMs);
	}

void	Part::processUnsegmentedTransportControlMessage(
			uint16_t	srcAddress,
			uint16_t	dstAddress,
			uint32_t	seq,
			uint32_t	ivIndex,
			uint8_t		ttl,
			const void*	lowerTransportPDU,
			unsigned	length,
			uint8_t		opcode
			) noexcept{

	#ifdef DEBUG_TRACE_TMI
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif

	switch(opcode){
		case 0x01:
			// Friend Poll
			processFriendPoll(
				srcAddress,
				dstAddress,
				seq,
				ivIndex,
				ttl,
				lowerTransportPDU,
				length
				);
			break;
		case 0x07:
			// Friend Subscription List Add
			processFriendSubscriptionListAdd(
				srcAddress,
				dstAddress,
				seq,
				ivIndex,
				ttl,
				lowerTransportPDU,
				length
				);
			break;
		case 0x08:
			// Friend Subscription List Remove
			processFriendSubscriptionListRemove(
				srcAddress,
				dstAddress,
				seq,
				ivIndex,
				ttl,
				lowerTransportPDU,
				length
				);
			break;
		case 0x03:
			// Friend Request
		case 0x04:
			// Friend Offer
		case 0x02:
			// Friend Update
		case 0x05:
			// Friend Clear
		case 0x06:
			// Friend Clear Confirm
		case 0x09:
			// Friend Subscription List Confirm
		default:
			Oscl::Error::Info::log(
				"%s: Unexpected network encrypted control PDU, opcode: 0x%2.2X.\n",
				OSCL_PRETTY_FUNCTION,
				opcode
				);
			return;
		}
	}

void	Part::processSegmentedLowerTransportControlMessage(
			uint16_t	srcAddress,
			uint16_t	dstAddress,
			uint32_t	seq,
			uint32_t	ivIndex,
			uint8_t		ttl,
			const void*	lowerTransportPDU,
			unsigned	length,
			uint8_t		opcode
			) noexcept{

	// Used Friendship credentials.
	// This is not expected by a Friend Node?
	}

void	Part::processUpperTransportControlPDU(
			const void*	upperTransportPdu,
			unsigned	pduLength,
			uint32_t	ivIndex,
			uint32_t	seq,
			uint16_t	src,
			uint16_t	dst,
			uint8_t		ttl,
			uint8_t		opcode
			) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s:\n"
		"\tivIndex: 0x%8.8X\n"
		"\tseq: 0x%8.8X\n"
		"\tsrc: 0x%4.4X\n"
		"\tdst: 0x%4.4X\n"
		"\tttl: 0x%2.2X\n"
		"\topcode: 0x%2.2X\n"
		"",
		__PRETTY_FUNCTION__,
		ivIndex,
		seq,
		src,
		dst,
		ttl,
		opcode
		);
	#endif

	// A friend doess nothing for this
	// since individual segments are queued
	// and sent to the LPN when polled.
	}

void	Part::queueMessageToLPN(
			const void*		pdu,
			unsigned		length,
			uint32_t		ivIndex,
			uint32_t		seq,
			uint16_t		src,
			uint16_t		dst,
			bool			ctl,
			uint8_t			ttl
			) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif

	Queue::ItemMem*
	mem	= _freeQueueItemMem.get();

	if(!mem){
		/* 3.5.5 Friend Queue

			If the Friend Queue is full and a new message needs
			to be stored that is not a Friend Update message,
			the oldest entries other than a Friend Update message
			shall be discarded to make room for the new message.

				Note: An implementation may have to discard multiple
				messages to fit the new message into the Friend Queue.

			If the message that is being stored is a Segment
			Acknowledgment message and the Friend Queue contains
			another Segment Acknowledgment message that has the
			same source and destination addresses, and the same
			SeqAuth value, but a lower IV Index or sequence number,
			then the older Segment Acknowledgment message shall
			be discarded.

			When a Friend node becomes aware of a security update,
			for example by receiving a valid Secure Network beacon
			or by having the Key Refresh Phase state changed,
			it shall add a Friend Update message into the Friend Queue.

			When the Low Power node requests a message from the
			Friend Queue, the oldest entry shall be sent. Once that
			message has been acknowledged by the Low Power node,
			that entry shall be discarded.

			If the Friend node is polled for a message from a
			Low Power node using a Friend Poll, and the Friend
			Queue for that node is empty, then the Friend node
			shall generate a new Friend Update message and
			add that message to the Friend Queue before sending
			the response, so that this Friend Update message
			can be sent in response to the Friend Poll message.
		*/
		Queue::Item*
		item	= getOldestNonUpdateMessage();

		if(!item){
			Oscl::Error::Info::log(
				"%s: cannot queue message\n",
				OSCL_PRETTY_FUNCTION
				);
			// This should not happen!
			return;
			}

		item->~Item();

		mem	= (Queue::ItemMem*)item;
		}

	Oscl::Error::Info::log(
		"Friend queued %p to LPN: 0x%4.4X:"
		" length: %u,"
		" seq: 0x%6.6X,"
		" src: 0x%4.4X,"
		" dst: 0x%4.4X,"
		" ctl: 0x%2.2X"
		" ttl: %u"
		"\n",
		mem,
		_lpnAddress,
		length,
		seq,
		src,
		dst,
		ctl,
		ttl
		);

	Queue::Item*
	item	= new(mem)
				Queue::NetItem(
					*this,
					pdu,
					length,
					ivIndex,
					seq,
					src,
					dst,
					ctl,
					ttl
					);

	_lpnQueue.put(item);	
	}

void	Part::forwardLowerTransportAckPduToAllElements(
			const void*	lowerTransportAckPDU,
			unsigned	pduLength,
			uint32_t	seq,
			uint16_t	src,
			uint16_t	dst
			) noexcept{
	// FIXME: Here we have an ACK that needs
	// to go to the LPN.
	// I suspect that this needs to be queued
	// until polled. The section
	// 3.6.6.5.2 Outgoing segmented message
	// confirms this.

	}

void	Part::processLowerTransport(
			uint16_t	srcAddress,
			uint16_t	dstAddress,
			uint32_t	ivIndex,
			uint32_t	seq,
			uint8_t		ttl,
			const void*	lowerTransportPDU,
			unsigned	length
			) noexcept{

	// Used Friendship credentials.

	// These are messages from the LPN.

	// This is called from processLocalNetworkPDU()
	// which is called from rxNetwork().

	// Generally, such messages from the LPN would be
	// destined for another node on the network and
	// would be retransmitted using master credentials
	// unless they are destined for this particular
	// Node.

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n"
		"\tsrcAddress: 0x%4.4X\n"
		"\tdstAddress: 0x%4.4X\n"
		"\tseq: 0x%6.6X\n"
		"\tttl: 0x%2.2X\n"
		"",
		__PRETTY_FUNCTION__,
		srcAddress,
		dstAddress,
		seq,
		ttl
		);
	#endif

	return;
	}

void	Part::forwardUpperTransportPduToAllElements(
			const void*	upperTransportAccessPdu,
			unsigned	pduLength,
			uint32_t	ivIndex,
			uint32_t	seq,
			uint16_t	src,
			uint16_t	dst,
			uint8_t		aid,
			bool		akf,
			bool		segmented
			) noexcept{
	// This is called when an access message
	// has been completely reassembled.

	// We do *nothing* here, since all of the
	// individual Lower Transport PDUs constituting
	// this message have already been placed in
	// the LPN queue.
	}

void	Part::sendFriendClear() noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif

	/* 3.6.6.2 Friendship security

		The Friend Clear and Friend Clear Confirm messages are always
		encrypted using the master security credentials.
	 */

	unsigned	bufferSize;

	void*
	buffer	= _freeStore.allocBuffer(bufferSize);

	if(!buffer){
		Oscl::Error::Info::log(
			"%s: Can't allocate buffer.\n",
			OSCL_PRETTY_FUNCTION
			);
		return;
		}

	Oscl::Encoder::BE::Base	beEncoder(
		buffer,
		bufferSize
		);

	Oscl::Encoder::Api&	encoder	= beEncoder;

	/*
		[0:1]	LPNAddress (2-octets)
		[2:3]	LPNCounter (2-octets)
	 */

	static const uint8_t	opcode		= 0x05;	// Friend Clear

	encoder.encode(_previousAddress);	// dst
	encoder.encode(opcode);
	encoder.encode(_lpnAddress);
	encoder.encode(_lpnCounter);

	if(encoder.overflow()){
		Oscl::Error::Info::log(
			"%s: overflow!\n",
			OSCL_PRETTY_FUNCTION
			);
		return;
		}

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		" dst: 0x%4.4X\n"
		" opcode: 0x%2.2X\n"
		" _lpnAddress: 0x%4.4X\n"
		" _lpnCounter: 0x%4.4X\n"
		"",
		_previousAddress,
		opcode,
		_lpnAddress,
		_lpnCounter
		);
	Oscl::Error::Info::hexDump(
		buffer,
		encoder.length()
		);
	#endif

	Oscl::Handle<Oscl::Pdu::Fixed>
	fixed	= _freeStore.allocFixed(
				buffer,
				encoder.length()
				);

	if(!fixed){
		Oscl::Error::Info::log(
			"%s: Can't allocate fixed pdu.\n",
			OSCL_PRETTY_FUNCTION
			);
		_freeStore.freeBuffer(buffer);
		return;
		}

	const uint32_t	ivIndex	= _context.getNodeApi().getTxIvIndex();
	const uint32_t	seq	= _context.getNodeApi().allocateSequenceNumber();

	/*
		1.	The TTL shall be set to the maximum valid value.
	 */
	_context.getNetTxApi().transmitControl(
		fixed,				// Oscl::Pdu::Pdu* pdu,
		ivIndex,			// uint32_t        ivIndex,
		seq,				// uint32_t        seq,
		_friendAddress,		// uint16_t        src,
		_previousAddress,	// uint16_t        dst,
		0x7F				// uint8_t         ttl
		);
	}

void	Part::sendFriendClearConfirm(
			uint16_t	lpnAddress,
			uint16_t	lpnCounter,
			uint16_t	dst,
			uint8_t		ttl
			) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif

	/* 3.6.5.6 Friend Clear Confirm

		The Friend Clear Confirm message is sent by the old Friend
		node in response to the Friend Clear message to confirm
		that the friendship has been terminated.

		If the Friend Clear message was received with a TTL of 0,
		the confirmation should use TTL of 0 as well.

		The message should only be sent in response to a valid
		Friend Clear message received within the Poll Timeout
		of the previous friend relationship, but it should send
		a Friend Clear Confirm message for each valid Friend Clear
		message that it receives in that period.

		A Friend Clear message is considered valid if the result
		of the subtraction of the value of the LPNCounter field
		of the Friend Request message (the one that initiated
		the friendship) from the value of the LPNCounter field
		of the Friend Clear message, modulo 65536, is in the
		range 0 to 255 inclusive.

		This message shall be sent using the master
		security credentials.

	 */

	/* 3.6.5.6 Friend Clear Confirm
		If the Friend Clear message was received with a TTL of 0,
		the confirmation should use TTL of 0 as well.
	 */
	ttl	= (ttl == 0)?0:0x7F;

	unsigned	bufferSize;

	void*
	buffer	= _freeStore.allocBuffer(bufferSize);

	if(!buffer){
		Oscl::Error::Info::log(
			"%s: Can't allocate buffer.\n",
			OSCL_PRETTY_FUNCTION
			);
		return;
		}

	Oscl::Encoder::BE::Base	beEncoder(
		buffer,
		bufferSize
		);

	Oscl::Encoder::Api&	encoder	= beEncoder;

	/*
		[0:1]	LPNAddress (2-octets)
		[2:3]	LPNCounter (2-octets)
	 */

	static const uint8_t	opcode		= 0x06;	// Friend Clear Confirm

	encoder.encode(dst);
	encoder.encode(opcode);
	encoder.encode(lpnAddress);
	encoder.encode(lpnCounter);

	if(encoder.overflow()){
		Oscl::Error::Info::log(
			"%s: overflow!\n",
			OSCL_PRETTY_FUNCTION
			);
		return;
		}

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		" dst: 0x%4.4X\n"
		" opcode: 0x%2.2X\n"
		" lpnAddress: 0x%4.4X\n"
		" lpnCounter: 0x%4.4X\n"
		"",
		_previousAddress,
		opcode,
		lpnAddress,
		lpnCounter
		);
	#endif

	Oscl::Handle<Oscl::Pdu::Fixed>
	fixed	= _freeStore.allocFixed(
				buffer,
				encoder.length()
				);

	if(!fixed){
		Oscl::Error::Info::log(
			"%s: Can't allocate fixed pdu.\n",
			OSCL_PRETTY_FUNCTION
			);
		_freeStore.freeBuffer(buffer);
		return;
		}

	const uint32_t	ivIndex	= _context.getNodeApi().getTxIvIndex();
	const uint32_t	seq	= _context.getNodeApi().allocateSequenceNumber();

	/*
		1.	The TTL shall be set to the maximum valid value.
	 */
	_context.getNetTxApi().transmitControl(
		fixed,				// Oscl::Pdu::Pdu* pdu,
		ivIndex,			// uint32_t        ivIndex,
		seq,				// uint32_t        seq,
		_friendAddress,		// uint16_t        src,
		dst,				// uint16_t        dst,
		ttl					// uint8_t         ttl
		);
	}

void	Part::free(SubscribeRecord* r) noexcept{
	r->~SubscribeRecord();

	_freeSubscribeRecordMem.put((SubscribeRecordMem*)r);
	}

SubscribeRecord*	Part::createSubscribeRecord(uint16_t address) noexcept{
	SubscribeRecordMem*
	mem	= _freeSubscribeRecordMem.get();

	if(!mem){
		return 0;
		}

	SubscribeRecord*
	r	= new(mem) SubscribeRecord(address);

	return r;
	}

SubscribeRecord*	Part::find(uint16_t address) noexcept{
	SubscribeRecord*	r;
	for(	r	= _subscriptions.first();
			r;
			r	= _subscriptions.next(r)
		){
		if(r->address() == address){
			return r;
			}
		}

	return 0;
	}

void	Part::remove(uint16_t address) noexcept{
	SubscribeRecord*
	r	= find(address);

	if(!r){
		return;
		}

	_subscriptions.remove(r);

	free(r);
	}

void	Part::sendNetMsgToLPN(
			const void*		frame,
			unsigned		length,
			uint32_t		ivIndex,
			uint32_t		seq,
			uint16_t		src,
			uint16_t		dst,
			bool			ctl,
			uint8_t			ttl
			) noexcept {

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif

	Oscl::Handle<Oscl::Pdu::Pdu>
	pdu	= buildNetworkPDU(
			ctl,				// bool		ctl,
			ttl,				// uint8_t	ttl,
			src,				// uint16_t	src,
			seq,				// uint32_t	seq,
			ivIndex,			// uint32_t	ivIndex,
			frame,				// const void*	transportPDU,
			length				// tranportPduLength + dst
			);

	if(!pdu){
		return;
		}

	_context.getNetTxApi().transmitNetworkPDU(
		pdu,	// Oscl::Pdu::Pdu* pdu,
		dst	// uint16_t        dst
		);
	}

bool	Part::duplicateMessageInLpnQueue(
			uint16_t	src,
			uint32_t	seq
			) noexcept{

	#ifdef DEBUG_TRACE_TMI
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif

	Queue::Item*	item;
	for(
		item	= _lpnQueue.first();
		item;
		item	= _lpnQueue.next(item)
		){
		if(item->isNetMsgMatch(src,seq)){
			return true;
			}
		}

	return false;
	}

/////////////////////////////////

Part::SubscriptionListConfirm::SubscriptionListConfirm(
	Part&	context,
	uint8_t transactionNumber
	) noexcept:
		_context(context),
		_transactionNumber(transactionNumber)
		{
	}

void	Part::SubscriptionListConfirm::process() noexcept{
	_context.sendFriendSubscriptionListConfirm(_transactionNumber);
	}

Part::PollConfirm::PollConfirm(
	Part&	context
	) noexcept:
		_context(context)
		{
	}

void	Part::PollConfirm::process() noexcept{
	_context.pollConfirm();
	}

Part::LpnNetMsg::LpnNetMsg(
	Part&		context,
	const void*	pdu,
	unsigned	length,
	uint32_t	ivIndex,
	uint32_t	seq,
	uint16_t	src,
	uint16_t	dst,
	bool		ctl,
	uint8_t		ttl
	) noexcept:
		_context(context),
		_length(length),
		_ivIndex(ivIndex),
		_seq(seq),
		_src(src),
		_dst(dst),
		_ctl(ctl),
		_ttl(ttl)
		{
	if(length > maxLowerTransportPduSize){
		Oscl::Error::Info::log(
			"%s: pdu too long for buffer.\n",
			OSCL_PRETTY_FUNCTION
			);
		length	= maxLowerTransportPduSize;
		}

	_pdu[0]	= dst;

	memcpy(
		&_pdu[1],
		pdu,
		length
		);
	}

void	Part::LpnNetMsg::process() noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif

	Oscl::Handle<Oscl::Pdu::Pdu>
	pdu	= _context.buildNetworkPDU(
			_ctl,				// bool		ctl,
			_ttl,				// uint8_t	ttl,
			_src,				// uint16_t	src,
			_seq,				// uint32_t	seq,
			_ivIndex,			// uint32_t	ivIndex,
			_pdu,				// const void*	transportPDU,
			_length+1			// tranportPduLength + dst
			);

	if(!pdu){
		return;
		}

	_context._context.getNetTxApi().transmitNetworkPDU(
		pdu,	// Oscl::Pdu::Pdu* pdu,
		_dst	// uint16_t        dst
		);
	}

void	Part::transmitCantAcceptACK(
			uint16_t	srcAddress,
			uint16_t	dstAddress,
			uint32_t	ivIndex,
			uint32_t	seq,
			uint8_t		ttl,
			const void*	frame,
			unsigned	length
			) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s: maybe.\n",
		__PRETTY_FUNCTION__
		);
	#endif

	/*
		If a lower transport layer receives a segment of
		a multi-segment message but cannot accept this multi-
		segment message at this time because it is currently
		busy or out of resources to accept this message,
		and if the message is destined to a unicast address,
		the lower transport layer shall respond with a
		Segment Acknowledgment message with the BlockAck field
		set to 0x00000000.
	 */

	if(!Oscl::BT::Mesh::Address::isUnicastAddress(dstAddress)){
		return;
		}

	//	Segmented Access Message:
	//	[0]
	//		[7]		SEG	(1)
	//		[6]		AKF
	//		[5:0]	AID
	//	[1]
	//		[7]		SZMIC, Size of TransMIC (0:32-bit, 1:64-bit)
	//		[6:0]	SeqZero MSBs
	//	[2]
	//		[7:2]	SeqZero LSBs
	//		[1:0]	SegO MSBs
	//	[3]
	//		[7:5]	SegO LSBs
	//		[4:0]	SegN
	//	[4:n]	Range: 8:96 bits, 1:12 octets
	//		[7:5]	Segment m
	//
	//	Every Segmented Access message for the same Upper Transport Access PDU
	//	shall have the same values for AKF, AID, SZMIC, SeqZero, and SegN.

	Oscl::Endian::Decoder::Linear::Part
	beDecoder(
		frame,
		length
		);

	Oscl::Decoder::Api&
	decoder	= beDecoder.be();

	uint8_t		segAkfAID;
	uint8_t		szmicSeqZero;
	uint8_t		seqZeroSegO;
	uint8_t		segOSegN;

	decoder.decode(segAkfAID);
	decoder.decode(szmicSeqZero);
	decoder.decode(seqZeroSegO);
	decoder.decode(segOSegN);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			OSCL_PRETTY_FUNCTION
			);
		return;
		}

	bool
	segmented	= (segAkfAID >> 7)?true:false;

	if(!segmented){
		#ifdef DEBUG_TRACE
		Oscl::Error::Info::log(
			"%s: not segmented!\n",
			__PRETTY_FUNCTION__
			);
		#endif
		return;
		}

	uint16_t
	seqZero	=	szmicSeqZero & 0x7F;
	seqZero	<<=	6;
	seqZero	|= (seqZeroSegO >> 2) & 0x3F;

	uint8_t
	segO	= (seqZeroSegO & 0x03);
	segO	<<= 3;
	segO	|= (segOSegN >> 5) & 0x07;

	//	Segment Acknowledgement message:
	//	[0]
	//		[7]		SEG	(0)
	//		[6:0]	0b0000000
	//	[1]
	//		[7]		OBO
	//			0	Direct
	//			1	Friend On Behalf Of LPN (Friend queued message for LPN)
	//		[6:0]	SeqZero MSBs
	//	[2]
	//		[7:2]	SeqZero LSBs
	//		[1:0]	RFU (tx as 0b00)
	//	[3:6]	BlockAck, 32-bit block mask (BE)
	//		[n]		Segment n ACK, Range 0 <= n <= 31

	unsigned	bufferSize;

	void*
	mem	= _freeStore.allocBuffer(bufferSize);

	if(!mem){
		Oscl::Error::Info::log(
			"%s: out of memory\n",
			OSCL_PRETTY_FUNCTION
			);
		return;
		}

	Oscl::Encoder::BE::Base
	beEncoder(
		mem,
		bufferSize
		);

	Oscl::Encoder::Api&	encoder	= beEncoder;

	static const uint8_t	opcode	= 0x00;
	static const uint32_t	cantAcceptBlockAck	= 0x00000000;

	uint8_t
	oboSeqZero	= 
			((0x01)<<7)	// OBO: For a friend.
		|	(seqZero >> 6)
		;

	uint8_t
	seqZeroRFU	= (seqZero & 0x3F) << 2;

	encoder.encode(srcAddress);
	encoder.encode(opcode);
	encoder.encode(oboSeqZero);
	encoder.encode(seqZeroRFU);
	encoder.encode(cantAcceptBlockAck);

	Oscl::Handle<Oscl::Pdu::Pdu>
	fixed	= _freeStore.allocFixed(
				mem,
				encoder.length()
				);
	if(!fixed){
		_freeStore.freeBuffer(mem);
		Oscl::Error::Info::log(
			"%s: out of memory\n",
			OSCL_PRETTY_FUNCTION
			);
		return;
		}

	uint32_t
	seqNum	= _context.getNetTxApi().allocSequenceNumber();

	/*	3.5.2.3.1 Segment Acknowledgment message
		If the received segments were sent with TTL set to 0,
		it is recommended that the corresponding Segment
		Acknowledgment message is sent with TTL set to 0.
	 */
	ttl	= (!ttl)?0x00:_context.getNetTxApi().getDefaultTTL();

	Oscl::Error::Info::log(
		"%s:\n",
		OSCL_PRETTY_FUNCTION
		);

	_context.getNetTxApi().transmitControl(
		fixed,
		ivIndex,	// ivIndex
		seqNum,		// seq
		dstAddress,	// src - for an ACK, the src
					// is the original dst address.
		srcAddress,	// dst
		ttl			// ttl
		);
	}
