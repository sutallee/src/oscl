/*
   Copyright (C) 2020 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <string.h>
#include "item.h"
#include "oscl/error/info.h"
#include "oscl/encoder/be/base.h"

using namespace Oscl::BT::Mesh::Friend::Queue;

NetItem::NetItem(
	Queue::Api&		context,
	const void*		pdu,
	unsigned		length,
	uint32_t		ivIndex,
	uint32_t		seq,
	uint16_t		src,
	uint16_t		dst,
	bool			ctl,
	uint8_t			ttl
	) noexcept:
		_context(context),
		_length(length),
		_ivIndex(ivIndex),
		_seq(seq),
		_src(src),
		_dst(dst),
		_ctl(ctl),
		_ttl(ttl)
		{
	if(length > maxLowerTransportPduSize){
		Oscl::Error::Info::log(
			"%s: pdu too long for buffer.\n",
			OSCL_PRETTY_FUNCTION
			);
		length	= maxLowerTransportPduSize;
		}

	Oscl::Encoder::BE::Base
	beEncoder(
		_pdu,
		sizeof(dst)
		);

	Oscl::Encoder::Api&	encoder	= beEncoder;

	encoder.encode(dst);

	memcpy(
		&_pdu[2],
		pdu,
		length
		);
	}

NetItem::~NetItem() noexcept{
	}

void	NetItem::process() noexcept{
	_context.sendNetMsgToLPN(
		_pdu,
		_length+sizeof(_dst),
		_ivIndex,
		_seq,
		_src,
		_dst,
		_ctl,
		_ttl
		);
	}

bool	NetItem::isNetMsgMatch(
			uint16_t	src,
			uint32_t	seq
			) noexcept{
	if(src != _src){
		return false;
		}

	if(seq != _seq){
		return false;
		}

	return true;
	}

UpdateItem::UpdateItem(
	Queue::Api&		context,
	uint32_t	ivIndex,
	bool		keyRefreshFlag,
	bool		ivUpdateFlag
	) noexcept:
		_context(context),
		_ivIndex(ivIndex),
		_keyRefreshFlag(keyRefreshFlag),
		_ivUpdateFlag(ivUpdateFlag)
		{
	}

UpdateItem::~UpdateItem() noexcept{
	}

void	UpdateItem::process() noexcept{
	}

bool	UpdateItem::isUpdateItem() noexcept{
	return true;
	}
