/*
   Copyright (C) 2020 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_bluetooth_mesh_friend_queue_itemh_
#define _oscl_bluetooth_mesh_friend_queue_itemh_

#include <stdint.h>
#include "api.h"
#include "oscl/queue/queueitem.h"
#include "oscl/memory/block.h"

/** */
namespace Oscl {
/** */
namespace BT {
/** */
namespace Mesh {
/** */
namespace Friend {
/** */
namespace Queue {

/** This is an abstract interface for
	items that will be placed in the
	Friend message queue.
 */
class Item : public Oscl::QueueItem {
	public:
		/** */
		virtual ~Item(){}

	public:
		/** This operation is invoked when the
			item is at the head of the queue.
		 */
		virtual void	process() noexcept=0;

		/** */
		virtual bool	isUpdateItem() noexcept{return false;}

		/** */
		virtual bool	isNetMsgMatch(
							uint16_t	src,
							uint32_t	seq
							) noexcept{return false;}
	};

/** This class contains a message that is destined
	for the friend Node.
	Each of these messages has been received using
	master security credentials or the master.
 */
class NetItem : public Item {
	private:
		/** */
		Queue::Api&		_context;

		/** */
		static constexpr unsigned  maxLowerTransportPduSize    = ((128)/8);

		/** We size this such that it is the size of
			the encrypted portion of a network PDU,
			which includes the destination address
			and the Lower TransportPDU.
		 */
		uint8_t			_pdu[sizeof(uint16_t)+maxLowerTransportPduSize];

		/** */
		const unsigned	_length;

		/** */
		const uint32_t	_ivIndex;

		/** */
		const uint32_t	_seq;

		/** */
		const uint16_t	_src;

		/** */
		const uint16_t	_dst;

		/** */
		const bool		_ctl;

		/** */
		const uint8_t	_ttl;


	public:
		/** */
		NetItem(
			Queue::Api&		context,
			const void*		pdu,
			unsigned		length,
			uint32_t		ivIndex,
			uint32_t		seq,
			uint16_t		src,
			uint16_t		dst,
			bool			ctl,
			uint8_t			ttl
			) noexcept;

		/** */
		~NetItem() noexcept;

		/** */
		void	process() noexcept;

		/** */
		bool	isNetMsgMatch(
					uint16_t	src,
					uint32_t	seq
					) noexcept;
	};

/** This class contains a update message
	indicating a change in the IV Index,
	keyRefreshFlag, and ivUpdateFlag.
 */
class UpdateItem : public Item {
	private:
		/** */
		Queue::Api&		_context;

		/** */
		uint32_t		_ivIndex;

		/** */
		bool			_keyRefreshFlag;

		/** */
		bool			_ivUpdateFlag;

	public:
		/** */
		UpdateItem(
			Queue::Api&		context,
			uint32_t	ivIndex,
			bool		keyRefreshFlag,
			bool		ivUpdateFlag
			) noexcept;

		/** */
		~UpdateItem() noexcept;

		/** */
		void	process() noexcept;

		bool	isUpdateItem() noexcept;
	};

/** */
union ItemMem {
	/** */
	void*	__qitemlink;
	/** */
	Oscl::Memory::AlignedBlock<sizeof(NetItem)>		net;
	/** */
	Oscl::Memory::AlignedBlock<sizeof(UpdateItem)>	update;
	};

}
}
}
}
}

#endif
