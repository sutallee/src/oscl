/*
   Copyright (C) 2021 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_bluetooth_mesh_constantsh_
#define _oscl_bluetooth_mesh_constantsh_

/** */
namespace Oscl {
/** */
namespace BT {
/** */
namespace Mesh {

/*
3.6.2.1 Encrypted access payload

The access payload is supplied by the access layer. If the TransMIC is 32 bits, the access payload can
be from a single octet to 380 octets in length. If the TransMIC is 64 bits, the access payload can be from
a single octet to 376 octets in length. At the upper transport layer, this field is opaque and no information
within this field may be used.

3.6.2.2 TransMIC

The Message Integrity Check for Transport (TransMIC) is a 32-bit or 64-bit field that authenticates that the
access payload has not been changed. For a segmented message, where SEG is set to 1, the size of the
TransMIC is determined by the value of the SZMIC field in the Lower Transport PDU. For unsegmented
messages, the size of the TransMIC is 32 bits for data messages.

Note: Control messages do not have a TransMIC.

*/

/** */
constexpr unsigned  maxSegments = 32;
/** */
constexpr unsigned  segmentSize = 12;
/** */
constexpr unsigned  maxUpperTransportPduSize    = maxSegments * segmentSize;

/** */
constexpr unsigned  transMic32Size  = 32/8;
/** */
constexpr unsigned  maxAccessPduSizeWithTransMic32  = maxUpperTransportPduSize - transMic32Size;

/** */
constexpr unsigned  transMic64Size  = 64/8;
/** */
constexpr unsigned  maxAccessPduSizeWithTransMic64  = maxUpperTransportPduSize - transMic64Size;

}
}
}

#endif
