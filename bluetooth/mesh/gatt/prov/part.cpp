/*
   Copyright (C) 2020 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "part.h"
#include <string.h>
#include <new>
#include "oscl/error/info.h"
#include "oscl/encoder/le/base.h"
#include "oscl/encoder/be/base.h"
#include "oscl/bluetooth/att/opcode.h"
#include "oscl/bluetooth/att/error.h"
#include "oscl/endian/decoder/linear/part.h"
#include "oscl/bluetooth/att/size.h"
#include "oscl/bluetooth/mesh/address/types.h"
#include "oscl/bluetooth/mesh/proxy/opcodes.h"
#include "oscl/bluetooth/mesh/proxy/sartx.h"
#include "oscl/bluetooth/gatt/services.h"
#include "oscl/bluetooth/gatt/declarations.h"
#include "oscl/bluetooth/gatt/descriptors.h"
#include "oscl/bluetooth/gatt/characteristics.h"

using namespace Oscl::BT::Mesh::GATT::Prov;

//#define DEBUG_TRACE
//#define MESHCTL_MTU_WORKAROUND

Part::Part(
	ContextApi&						context,
	Oscl::BT::Mesh::
	Node::Creator::Api&				nodeCreateApi,
	Oscl::BT::ATT::Server::TX::Api&	attSendApi,
	const uint16_t&					attMTU,
	bool							oobAvailable,
	uint8_t							outputOobSizeInCharacters
	) noexcept:
		Base(),
		_context(context),
		_nodeCreateApi(nodeCreateApi),
		_attSendApi(attSendApi),
		_attMTU(attMTU),
		_provGattTxComposer(
			*this,
			&Part::provGattTx
			),
		_notifyMeshProvDataOutComposer(
			*this,
			&Part::notifyMeshProvisioningDataOut
			),
		_allFreeNotification(
			*this,
			&Part::allFreeNotification
			),
		_deviceReaper(
			*this,
			_freeStore,
			_provGattTxComposer,
			oobAvailable,
			outputOobSizeInCharacters
			),
		_freeStore(
			"Part",
			&_allFreeNotification
			),
		_reasmEncoder(
			_reasmBuffer,
			sizeof(_reasmBuffer)
			),
		_provisioningEnableValue(0x0000),
		_stopDone(0)
		{
	}

void	Part::start() noexcept{
	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif

	_deviceReaper.start();
	}

void	Part::stop(Oscl::Done::Api& stopDone) noexcept{
	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif

	_stopDone	= &stopDone;

	stopNext();
	}

void	Part::stopNext() noexcept{
	if(!_stopDone){
		#ifdef DEBUG_TRACE
		Oscl::Error::Info::log(
			"%s: !_stopDone()\n",
			__PRETTY_FUNCTION__
			);
		#endif
		return;
		}

	if(!allMemoryIsFree()){
		#ifdef DEBUG_TRACE
		Oscl::Error::Info::log(
			"%s: !allMemoryIsFree()\n",
			__PRETTY_FUNCTION__
			);
		#endif
		return;
		}

	Oscl::Done::Api&	done	= *_stopDone;

	_stopDone	= 0;

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s: DONE()\n",
		__PRETTY_FUNCTION__
		);
	#endif
	done.done();
	}

void	Part::allFreeNotification() noexcept{
	if(!_stopDone){
		return;
		}

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif

	stopNext();	
	}

bool	Part::allMemoryIsFree() noexcept{
	return _freeStore.allMemoryIsFree();
	}

constexpr uint16_t	meshProvisioningServiceHandle					= 0x0010;

constexpr uint16_t	meshProvisioningDataInCharacteristicHandle		= 0x0011;
constexpr uint16_t	meshProvisioningDataInAttributeHandle			= 0x0012;

constexpr uint16_t	meshProvisioingDataOutCharacteristicHandle		= 0x0013;
constexpr uint16_t	meshProvisioningDataOutAttributeHandle			= 0x0014;

constexpr uint16_t	meshProvisioningDataOutHandle					= 0x0015;

constexpr uint16_t	meshProvisioningDataOutUuid16					= 0x2ADE;

constexpr	unsigned	nServices	= 1;

static const Oscl::BT::ATT::Server::TX::GroupInformation16bit	services[] = {
	{	// <<Mesh Provisioning Service>>
		.attributeHandle	= meshProvisioningServiceHandle,
		.groupEndHandle		= meshProvisioningDataOutHandle,
		.attributeValue		= Oscl::BT::GATT::meshProvisioningService
		}
	};

static const Oscl::BT::GATT::Server::CharacteristicDeclaration	characteristicDeclarations[] = {
	{	// Mesh Provisioning Data In Characteristic
		.handle = meshProvisioningDataInCharacteristicHandle,
		.properties	= 
				(0<<0)	// Broadcast
			|	(0<<1)	// Read
			|	(1<<2)	// Write Without Response
			|	(0<<3)	// Write
			|	(0<<4)	// Notify
			|	(0<<5)	// Indicate
			|	(0<<6)	// Authenticated Signed Writes
			|	(0<<7)	// Extended Properties
			,
		.attributeHandle	= meshProvisioningDataInAttributeHandle,
		.uuid16				= Oscl::BT::GATT::Characteristic::meshProvisioningDataIn
		},
	{	// Mesh Provisioning Data Out Characteristic
		.handle				= meshProvisioingDataOutCharacteristicHandle,
		.properties			= 
				(0<<0)	// Broadcast
			|	(0<<1)	// Read
			|	(0<<2)	// Write Without Response
			|	(0<<3)	// Write
			|	(1<<4)	// Notify
			|	(0<<5)	// Indicate
			|	(0<<6)	// Authenticated Signed Writes
			|	(0<<7)	// Extended Properties
			,
		.attributeHandle	= meshProvisioningDataOutAttributeHandle,
		.uuid16				= Oscl::BT::GATT::Characteristic::meshProvisioningDataOut
	}
	};

static const Oscl::BT::ATT::Server::TX::InformationData16bit	attributes[] = {
	{	// <<Mesh Provisioning Service>>
		.handle	= meshProvisioningServiceHandle,
		.uuid	= Oscl::BT::GATT::Declaration::primaryService
		},
	{	// Mesh Provisioning Data In Characteristic Declaration
		.handle	= meshProvisioningDataInCharacteristicHandle,
		.uuid	= Oscl::BT::GATT::Declaration::characteristic
		},
	{	// Mesh Provisioning Data In Attribute
		.handle	= meshProvisioningDataInAttributeHandle,
		.uuid	= Oscl::BT::GATT::Characteristic::meshProvisioningDataIn
		},
	{	// Mesh Provisioning Data Out Characteristic Declaration
		.handle	= meshProvisioingDataOutCharacteristicHandle,
		.uuid	= Oscl::BT::GATT::Declaration::characteristic
		},
	{	// Mesh Provisioning Data Out Attribute
		.handle	= meshProvisioningDataOutAttributeHandle,
		.uuid	= Oscl::BT::GATT::Characteristic::meshProvisioningDataOut
		},
	{	// Mesh Provisioning Data Out
		.handle	= meshProvisioningDataOutHandle,
		.uuid	= Oscl::BT::GATT::Descriptor::clientCharacteristicConfiguration
		}
	};

const Oscl::BT::ATT::Server::TX::GroupInformation16bit*
		Part::getServices(unsigned& nServices) const noexcept{

	nServices	= sizeof(services)/sizeof(Oscl::BT::ATT::Server::TX::GroupInformation16bit);

	return services;
	}

const Oscl::BT::GATT::Server::CharacteristicDeclaration*
		Part::getCharacteristicDeclarations(unsigned& nDeclarations) const noexcept{

	nDeclarations	= sizeof(characteristicDeclarations)/sizeof(Oscl::BT::GATT::Server::CharacteristicDeclaration);

	return characteristicDeclarations;
	}

const Oscl::BT::ATT::Server::TX::InformationData16bit*
		Part::getAttributes(unsigned& nAttributes) const noexcept{

	nAttributes	= sizeof(attributes)/sizeof(Oscl::BT::ATT::Server::TX::InformationData16bit);
	return attributes;
	}

Oscl::BT::ATT::Server::TX::Api&	Part::getAttSendApi() noexcept{
	return _attSendApi;
	}

bool	Part::hasAttribute(uint16_t attributeHandle) const noexcept{
/*
meshProvisioningDataInCharacteristicHandle		= 0x0011;
meshProvisioningDataInAttributeHandle			= 0x0012;
meshProvisioingDataOutCharacteristicHandle		= 0x0013;
meshProvisioningDataOutAttributeHandle			= 0x0014;

constexpr uint16_t	meshProvisioningDataOutHandle					= 0x0015;
*/
	switch(attributeHandle){
		case meshProvisioningDataOutHandle:
		case meshProvisioningDataInCharacteristicHandle:
		case meshProvisioningDataInAttributeHandle:
		case meshProvisioingDataOutCharacteristicHandle:
		case meshProvisioningDataOutAttributeHandle:
			return true;
		default:
			return false;
		}
	}

void	Part::processUnsupportedOpcode(
			const void*	frame,
			unsigned	length,
			const char*	opcodeString,
			uint8_t		opcode
			) noexcept{
	Oscl::Error::Info::log(
		"%s: opcode: 0x%2.2X (%s) Not supported for <<Mesh Provisoning Service>>\n",
		__PRETTY_FUNCTION__,
		opcode,
		opcodeString
		);
	Oscl::Error::Info::hexDump(
		frame,
		length
		);
	}

void	Part::processProvisioningPDU(
			const void*	frame,
			unsigned	length
			) noexcept{
	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	Oscl::Error::Info::hexDump(
		frame,
		length
		);
	#endif

	_deviceReaper.getRxApi().forward(
		frame,
		length
		);
	}

void	Part::processAssembledPDU(
			const void*	frame,
			unsigned	length
			) noexcept{

	const uint8_t*	pdu	= (const uint8_t*)frame;

	uint8_t	opcode	= pdu[0];

	switch(opcode){
		case 0x00:
			// Network PDU
			processUnsupportedOpcode(
				frame,
				length,
				"Network PDU",
				opcode
				);
			break;
		case 0x01:
			// Mesh Beacon
			processUnsupportedOpcode(
				frame,
				length,
				"Mesh Beacon",
				opcode
				);
			break;
		case 0x02:
			// Proxy Configuration
			processUnsupportedOpcode(
				frame,
				length,
				"Proxy Configuration",
				opcode
				);
			break;
		case 0x03:
			// Provisioning PDU
			processProvisioningPDU(
				&pdu[1],
				length - 1
				);
			break;
		default:
			Oscl::Error::Info::log(
				"%s: unknown Proxy PDU opcode (0x%2.2X)!\n",
				__PRETTY_FUNCTION__,
				opcode
				);
			break;
		}
	}

void	Part::notifyMeshProvisioningDataOut(Oscl::Pdu::Pdu* pdu) noexcept{
	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif

	_attSendApi.sendHandleValueNotification(
		meshProvisioningDataOutAttributeHandle,	// uint16_t        attributeHandle,
		pdu										// Oscl::Pdu::Pdu* attributeValue
		);
	}

void	Part::provGattTx(Oscl::Pdu::Pdu* pdu) noexcept{
	// It must wrap the PDU as a Provisioning PDU (opcode 0x03)
	// This operation must segment the pdu as required
	// by the MTU.
	// It must also be sent as an ATT Handle Value Notification
	//	Opcode: 0x1B
	//	Attribute Handle: 0x0014 (Mesh Provisioning Data Out handle)
	//	Provisioning PDU

	if(_stopDone){
		return;
		}

	unsigned
	length	= pdu->length();

	// WTF meshctl
	#ifdef MESHCTL_MTU_WORKAROUND
	unsigned	mtu	= 20 - (3+1);
	#else
	unsigned	mtu	= _attMTU - (3 + 1);
	#endif

	if(length > mtu){
		Oscl::BT::Mesh::Proxy::SAR::gattSegmentAndTransmit(
			pdu,
			_freeStore,
			_notifyMeshProvDataOutComposer,
			Oscl::BT::Mesh::Proxy::SAR::Opcodes::provisioningPduOpcodes,
			mtu
			);
		return;
		}

	Oscl::Handle<Oscl::Pdu::Composite>
	composite	= _freeStore.allocConstWrapper(
					&Oscl::BT::Mesh::Proxy::SAR::Opcodes::provisioningPduOpcodes._complete,
					sizeof(Oscl::BT::Mesh::Proxy::SAR::Opcodes::provisioningPduOpcodes._complete)
					);

	if(!composite){
		Oscl::Error::Info::log(
			"%s: out of composite wrappers.\n",
			__PRETTY_FUNCTION__
			);
		return;
		}

	Oscl::Handle<Oscl::Pdu::Fragment>
	fragment	= _freeStore.allocFragment(
					*pdu
					);

	if(!fragment){
		Oscl::Error::Info::log(
			"%s: out of fragments.\n",
			__PRETTY_FUNCTION__
			);
		return;
		}

	composite->append(fragment);

	notifyMeshProvisioningDataOut(composite);
	}

void	Part::stopped(Oscl::BT::Mesh::Prov::Device::Reaper& device) noexcept{
	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s: FIXME!\n",
		__PRETTY_FUNCTION__
		);
	#endif
	}

bool	Part::createNewNode(
			const uint8_t	netKey[16],
			const uint8_t	deviceKey[16],
			uint16_t		keyIndex,
			uint8_t			flags,
			uint32_t		ivIndex,
			uint16_t		unicastAddress
			) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s:\n"
		" Net Key: \n"
		"  %2.2X %2.2X %2.2X %2.2X %2.2X %2.2X %2.2X %2.2X\n"
		"  %2.2X %2.2X %2.2X %2.2X %2.2X %2.2X %2.2X %2.2X\n"
		" KeyIndex: 0x%4.4X\n"
		" Flags: 0x%2.2X\n"
		" IV Index: 0x%8.8X\n"
		" Unicast Address: 0x%4.4X\n"
		"",
		__PRETTY_FUNCTION__,
		netKey[0], netKey[1], netKey[2], netKey[3],
		netKey[4], netKey[5], netKey[6], netKey[7],
		netKey[8], netKey[9], netKey[10], netKey[11],
		netKey[12], netKey[13], netKey[14], netKey[15],
		keyIndex,
		flags,
		ivIndex,
		unicastAddress
		);
	#else
	Oscl::Error::Info::log(
		"New Network Added:\n"
		" KeyIndex: 0x%4.4X\n"
		" Unicast Address: 0x%4.4X\n"
		"",
		keyIndex,
		unicastAddress
		);
	#endif

	bool
	failed	= _nodeCreateApi.createNewNode(
				netKey,
				deviceKey,
				keyIndex,
				flags,
				ivIndex,
				unicastAddress
				);

	return failed;
	}

uint8_t	Part::getNumElements() const noexcept{
	return _nodeCreateApi.getNumElements();
	}

void	Part::setAttentionTimer(uint8_t nSeconds) noexcept{
	_context.setAttentionTimer(nSeconds);
	}

bool	Part::oobOutputActionBlinkIsSupported() const noexcept{
	return _context.oobOutputActionBlinkIsSupported();
	}

bool	Part::oobOutputActionBeepIsSupported() const noexcept{
	return _context.oobOutputActionBeepIsSupported();
	}

bool	Part::oobOutputActionVibrateIsSupported() const noexcept{
	return _context.oobOutputActionVibrateIsSupported();
	}

bool	Part::oobOutputActionNumericIsSupported() const noexcept{
	return _context.oobOutputActionNumericIsSupported();
	}

bool	Part::oobOutputActionAlphaNumericIsSupported() const noexcept{
	return _context.oobOutputActionAlphaNumericIsSupported();
	}

void	Part::oobOutputBlink(uint32_t n) noexcept{
	_context.oobOutputBlink(n);
	}

void	Part::oobOutputBeep(uint32_t n) noexcept{
	_context.oobOutputBeep(n);
	}

void	Part::oobOutputVibrate(uint32_t n) noexcept{
	_context.oobOutputVibrate(n);
	}

void	Part::oobOutputNumeric(uint32_t n) noexcept{
	_context.oobOutputNumeric(n);
	}

void	Part::oobOutputAlphaNumeric(const char* s) noexcept{
	_context.oobOutputAlphaNumeric(s);
	}

void	Part::provisioningComplete() noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif

	_context.provisioningStopped();
	_context.disconnect();
	}

bool	Part::writeRequest(
			uint16_t		attributeHandle,
			const uint8_t	attributeValue[],
			unsigned		attributeValueLength
			) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s: handle: 0x%4.4X\n",
		__PRETTY_FUNCTION__,
		attributeHandle
		);
	Oscl::Error::Info::hexDump(
		attributeValue,
		attributeValueLength
		);
	#endif

	Oscl::Endian::Decoder::Linear::Part
	leDecoder(
		attributeValue,
		attributeValueLength
		);

	Oscl::Decoder::Api&	decoder	= leDecoder.le();

	// Here we need to determine which attribute
	// is being written. Currently, the only attribute
	// that accepts Write operations is the
	// Mesh Provisioning Data Out <<Client Characteristic Configuration>>
	// This is used to enable/disable Handle Value Notifications
	// from the device.
	// IMHO, this is a waste since such notifications only
	// happen after a Provisioing Invite and not unsolicited.

	switch(attributeHandle){
		case meshProvisioningDataOutHandle:
			// <<Client Characteristic Configuration>>
			// Mesh Provisioning Data Out
			// Notification enable/disable
			// uint16_t	value
			decoder.decode(_provisioningEnableValue);
			if(decoder.underflow()){
				Oscl::Error::Info::log(
					"%s: _provisioningEnableValue underflow!\n",
					__PRETTY_FUNCTION__
					);
				_attSendApi.sendErrorResponse(
					Oscl::BT::ATT::Opcode::writeRequest,
					attributeHandle,
					Oscl::BT::ATT::ErrorCode::invalidPDU
					);
				return false;
				}
			_attSendApi.sendWriteResponse();
			break;
		default:
			_attSendApi.sendErrorResponse(
				Oscl::BT::ATT::Opcode::writeRequest,
				attributeHandle,
				Oscl::BT::ATT::ErrorCode::writeNotPermitted
				);
			break;
		}

	return true;
	}

bool	Part::writeCommand(
			uint16_t		attributeHandle,
			const uint8_t	attributeValue[],
			unsigned		attributeValueLength
			) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s:"
		"\tHandle: 0x%4.4X\n"
		"\tValue:\n",
		__PRETTY_FUNCTION__,
		attributeHandle
		);

	Oscl::Error::Info::hexDump(
		attributeValue,
		attributeValueLength
		);
	#endif

	if(!hasAttribute(attributeHandle)){
		return false;
		}

	// At this point, we have a command
	// destined for Not Mesh Provisioning Data In
	// The value is the command and it consists
	// of possibly segmented PDU that needs to
	// be reassembled.

	uint8_t	opcode	= attributeValue[0];
	uint8_t	sar		= opcode >> 6;
	opcode	&= 0x3F;

	Oscl::Encoder::Api&	encoder	= _reasmEncoder;

	switch(sar){
		case 0x00:
			// Complete Message
			processAssembledPDU(
				&attributeValue[0],
				attributeValueLength
				);
			break;
		case 0x01:
			// First Segment
			_reasmEncoder.reset();
			encoder.encode(opcode);
			encoder.copyIn(
				&attributeValue[1],
				attributeValueLength - 1
				);
			#ifdef DEBUG_TRACE
			Oscl::Error::Info::log(
				"\tFirst Segment packet (encoder.length(): %u):\n",
				encoder.length()
				);
			Oscl::Error::Info::hexDump(
				_reasmBuffer,
				encoder.length()
				);
			#endif
			break;
		case 0x02:
			// Continuation Segment
			encoder.copyIn(
				&attributeValue[1],
				attributeValueLength - 1
				);
			#ifdef DEBUG_TRACE
			Oscl::Error::Info::log(
				"\tContinuation Segment packet (encoder.length(): %u):\n",
				encoder.length()
				);
			Oscl::Error::Info::hexDump(
				_reasmBuffer,
				encoder.length()
				);
			#endif
			break;
		case 0x03:
			// Last Segment
			encoder.copyIn(
				&attributeValue[1],
				attributeValueLength - 1
				);
			if(encoder.overflow()){
				Oscl::Error::Info::log(
					"%s: overflow!\n",
					__PRETTY_FUNCTION__
					);
				}
			#ifdef DEBUG_TRACE
			Oscl::Error::Info::log(
				"\tReassembled packet (encoder.length(): %u):\n",
				encoder.length()
				);
			Oscl::Error::Info::hexDump(
				_reasmBuffer,
				encoder.length()
				);
			#endif
			processAssembledPDU(
				_reasmBuffer,
				encoder.length()
				);
			break;
		}

	return true;
	}

