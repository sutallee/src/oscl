/*
   Copyright (C) 2020 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_bluetooth_mesh_gatt_prov_parth_
#define _oscl_bluetooth_mesh_gatt_prov_parth_

#include <stdint.h>
#include "oscl/bluetooth/gatt/service/item.h"
#include "oscl/pdu/fwd/composer.h"
#include "oscl/pdu/memory/config.h"
#include "oscl/freestore/composers.h"
#include "oscl/done/operation.h"
#include "oscl/encoder/le/base.h"
#include "oscl/bluetooth/mesh/prov/device/reaper.h"
#include "oscl/bluetooth/mesh/node/creator/api.h"
#include "oscl/bluetooth/mesh/network/iteration/api.h"
#include "oscl/bluetooth/mesh/bearer/tx/itemcomp.h"
#include "oscl/bluetooth/mesh/bearer/tx/host/api.h"
#include "oscl/bluetooth/mesh/address/filter/record.h"
#include "oscl/bluetooth/mesh/network/interface/api.h"
#include "oscl/bluetooth/att/server/rx/api.h"
#include "oscl/bluetooth/att/server/tx/api.h"
#include "oscl/bluetooth/mesh/proxy/sar.h"
#include "oscl/bluetooth/gatt/server/base.h"

/** */
namespace Oscl {
/** */
namespace BT {
/** */
namespace Mesh {
/** */
namespace GATT {
/** */
namespace Prov {

/** */
class Part :
	private Oscl::BT::Mesh::Prov::Device::ContextApi,
	public Oscl::BT::GATT::Server::Base
	{
	public:
		/** */
		class ContextApi {
			public:
				/** */
				virtual void	setAttentionTimer(uint8_t nSeconds) noexcept=0;

				/** */
				virtual bool	oobOutputActionBlinkIsSupported() const noexcept=0;

				/** */
				virtual bool	oobOutputActionBeepIsSupported() const noexcept=0;

				/** */
				virtual bool	oobOutputActionVibrateIsSupported() const noexcept=0;

				/** */
				virtual bool	oobOutputActionNumericIsSupported() const noexcept=0;

				/** */
				virtual bool	oobOutputActionAlphaNumericIsSupported() const noexcept=0;

				/** */
				virtual void	oobOutputBlink(uint32_t n) noexcept=0;

				/** */
				virtual void	oobOutputBeep(uint32_t n) noexcept=0;

				/** */
				virtual void	oobOutputVibrate(uint32_t n) noexcept=0;

				/** */
				virtual void	oobOutputNumeric(uint32_t n) noexcept=0;

				/** */
				virtual void	oobOutputAlphaNumeric(const char* s) noexcept=0;

				/** */
				virtual void	provisioningStopped() noexcept=0;

				/** */
				virtual void	disconnect() noexcept=0;
			};

	private:
		/** */
		ContextApi&						_context;

		/** */
		Oscl::BT::Mesh::
		Node::Creator::Api&				_nodeCreateApi;

		/** */
		Oscl::BT::ATT::Server::TX::Api&	_attSendApi;

		/** */
		const uint16_t&					_attMTU;

	private:
		/** */
		Oscl::Pdu::FWD::Composer<Part>	_provGattTxComposer;

		/** */
		Oscl::Pdu::FWD::Composer<Part>	_notifyMeshProvDataOutComposer;

	private:
		/** */
		Oscl::Done::Operation<Part>		_allFreeNotification;

		/** */
		Oscl::BT::Mesh::Prov::Device::Reaper	_deviceReaper;

	private:
		/** */
		constexpr static unsigned	freeStoreBufferSize	= 128;
		/** */
		constexpr static unsigned	nFreeStoreBuffers	= 8;
		/** */
		constexpr static unsigned	nFixed		= 8;
		/** */
		constexpr static unsigned	nFragment	= 8;
		/** */
		constexpr static unsigned	nComposite	= 8;

		/** */
		Oscl::Pdu::Memory::
		Config<
			freeStoreBufferSize,
			nFreeStoreBuffers,
			nFixed,
			nFragment,
			nComposite
			>								_freeStore;

	private:
		/** FIXME: */
		static constexpr unsigned	maxPduSize	= 512;

		/** FIXME: */
		uint8_t						_reasmBuffer[maxPduSize];

		/** */
		Oscl::Encoder::LE::Base		_reasmEncoder;

	private:
		/**	<<Client Characteristic Configuration>>
			attribute value
			What in the world is the purpose?
		 */
		uint16_t		_provisioningEnableValue;

	private:
		/** */
		Oscl::Done::Api*	_stopDone;

	public:
		/** */
		Part(
			ContextApi&						context,
			Oscl::BT::Mesh::
			Node::Creator::Api&				nodeCreateApi,
			Oscl::BT::ATT::Server::TX::Api&	attSendApi,
			const uint16_t&					attMTU,
			bool							oobAvailable,
			uint8_t							outputOobSizeInCharacters
			) noexcept;

		/** */
		void	start() noexcept;

		/** */
		void	stop(Oscl::Done::Api& stopDone) noexcept;

	private:
		/** */
		void	processUnsupportedOpcode(
					const void*	frame,
					unsigned	length,
					const char*	opcodeString,
					uint8_t		opcode
					) noexcept;

		/** */
		void	processProvisioningPDU(
					const void*	frame,
					unsigned	length
					) noexcept;

	private:
		/** */
		void	stopNext() noexcept;

		/** */
		bool	allMemoryIsFree() noexcept;

	private: // Oscl::Done::Operation _allFreeNotification
		/** */
		void	allFreeNotification() noexcept;

	private:
		/** */
		void	notifyMeshProvisioningDataOut(Oscl::Pdu::Pdu* pdu) noexcept;

	private: // Oscl::Pdu::FWD::Composer _provGattTxComposer
		/** */
		void	provGattTx(Oscl::Pdu::Pdu* pdu) noexcept;

	private: // ContextApi
		/** This operation is called when the Reaper
			is stopped after it completes the process
			and is ready to be destroyed.
		 */
		void	stopped(Oscl::BT::Mesh::Prov::Device::Reaper& device) noexcept;

		/** This operation is called by the Reaper to
			create a new network.
		 */
		bool	createNewNode(
					const uint8_t	netKey[16],
					const uint8_t	deviceKey[16],
					uint16_t		keyIndex,
					uint8_t			flags,
					uint32_t		ivIndex,
					uint16_t		unicastAddress
					) noexcept;

		/** */
		uint8_t	getNumElements() const noexcept;

		/** */
		void	setAttentionTimer(uint8_t nSeconds) noexcept;

		/** */
		bool	oobOutputActionBlinkIsSupported() const noexcept;

		/** */
		bool	oobOutputActionBeepIsSupported() const noexcept;

		/** */
		bool	oobOutputActionVibrateIsSupported() const noexcept;

		/** */
		bool	oobOutputActionNumericIsSupported() const noexcept;

		/** */
		bool	oobOutputActionAlphaNumericIsSupported() const noexcept;

		/** */
		void	oobOutputBlink(uint32_t n) noexcept;

		/** */
		void	oobOutputBeep(uint32_t n) noexcept;

		/** */
		void	oobOutputVibrate(uint32_t n) noexcept;

		/** */
		void	oobOutputNumeric(uint32_t n) noexcept;

		/** */
		void	oobOutputAlphaNumeric(const char* s) noexcept;

		/** */
		void	provisioningComplete() noexcept;

	private:
		/** */
		bool	hasAttribute(uint16_t attributeHandle) const noexcept;

	private: // Oscl::BT::GATT::Server::Base
		/** */
		void	processAssembledPDU(
					const void*	frame,
					unsigned	length
					) noexcept;

		/** */
		const Oscl::BT::ATT::Server::TX::GroupInformation16bit*	getServices(unsigned& nServices) const noexcept;

		/** */
		const Oscl::BT::GATT::Server::CharacteristicDeclaration*	getCharacteristicDeclarations(unsigned& nDeclarations) const noexcept;

		/** */
		const Oscl::BT::ATT::Server::TX::InformationData16bit*	getAttributes(unsigned& nAttributes) const noexcept;

		/** */
		Oscl::BT::ATT::Server::TX::Api&	getAttSendApi() noexcept;

		/**
			uint8_t opcode = 0x12
			RETURN: true if processed or false if not implemented.
		 */
		bool	writeRequest(
					uint16_t		attributeHandle,
					const uint8_t	attributeValue[],
					unsigned		attributeValueLength
					) noexcept;

		/**
			uint8_t opcode = 0x52
			RETURN: true if processed or false if not implemented.
		 */
		bool	writeCommand(
					uint16_t		attributeHandle,
					const uint8_t	attributeValue[],
					unsigned		attributeValueLength
					) noexcept;
	};

}
}
}
}
}

#endif
