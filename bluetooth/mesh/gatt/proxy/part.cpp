/*
   Copyright (C) 2020 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "part.h"
#include <string.h>
#include <new>
#include "oscl/error/info.h"
#include "oscl/encoder/le/base.h"
#include "oscl/encoder/be/base.h"
#include "oscl/bluetooth/att/opcode.h"
#include "oscl/bluetooth/att/error.h"
#include "oscl/endian/decoder/linear/part.h"
#include "oscl/aes128/cmac.h"
#include "oscl/bluetooth/att/size.h"
#include "oscl/bluetooth/mesh/address/types.h"
#include "oscl/bluetooth/mesh/proxy/sartx.h"
#include "oscl/bluetooth/gatt/services.h"
#include "oscl/bluetooth/gatt/declarations.h"
#include "oscl/bluetooth/gatt/descriptors.h"
#include "oscl/bluetooth/gatt/characteristics.h"

using namespace Oscl::BT::Mesh::GATT::Proxy;

//#define DEBUG_TRACE

//#define MESHCTL_MTU_WORKAROUND

Part::Part(
	Oscl::BT::Mesh::Bearer::
	TX::Host::Api&					bearerHostApi,
	Oscl::BT::Mesh::
	Network::Iteration::Api&		networkIterator,
	Oscl::BT::ATT::Server::TX::Api&	attSendApi,
	const uint16_t&					attMTU
	) noexcept:
		Base(),
		_bearerHostApi(bearerHostApi),
		_networkIterator(networkIterator),
		_attSendApi(attSendApi),
		_attMTU(attMTU),
		_networkGattBearerTxComposer(
			*this,
			&Part::networkGattBearerTx
			),
		_proxyConfigurationReceiverComposer(
			*this,
			&Part::processDecryptedProxyConfiguration
			),
		_proxyConfigurationTxComposer(
			*this,
			&Part::proxyConfigurationTx
			),
		_notifyMeshProxyDataOutComposer(
			*this,
			&Part::notifyMeshProxyDataOut
			),
		_allFreeNotification(
			*this,
			&Part::allFreeNotification
			),
		_freeStore(
			"Part",
			&_allFreeNotification
			),
		_reasmEncoder(
			_reasmBuffer,
			sizeof(_reasmBuffer)
			),
		_currentNetwork(0),
		_provisioningEnableValue(0x0000),
		_proxyEnableValue(0x0000),
		_proxyFilterListSize(0),
		_proxyFilterType(0x00),
		_stopDone(0)
		{
	for(unsigned i=0;i<nFilterRecordMem;++i){
		_freeFilterRecordMem.put(&_filterRecordMem[i]);
		}
	}

void	Part::start() noexcept{
	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif

	_bearerHostApi.attach(_networkGattBearerTxComposer);
	}

void	Part::stop(Oscl::Done::Api& stopDone) noexcept{
	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif

	_bearerHostApi.detach(_networkGattBearerTxComposer);

	_stopDone	= &stopDone;

	stopNext();
	}

bool	Part::isDestinedForProxy(uint16_t address) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s.\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif

	if(!Oscl::BT::Mesh::Address::isUnicastAddress(address)){
		return false;
		}

	for(
		Oscl::BT::Mesh::Address::Filter::Record*
		record	= _whiteListFilters.first();
		record;
		record	= _whiteListFilters.next(record)
		){
		if(record->match(address)){
			#ifdef DEBUG_TRACE
			Oscl::Error::Info::log(
				"%s: match.\n",
				OSCL_PRETTY_FUNCTION
				);
			#endif
			return true;
			}
		}

	return false;
	}

void	Part::networkSecurityChanged() noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s.\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif

	if(_stopDone){
		return;
		}

	sendSecureNetworkBeacons();
	}

void	Part::stopNext() noexcept{
	if(!_stopDone){
		#ifdef DEBUG_TRACE
		Oscl::Error::Info::log(
			"%s: !_stopDone()\n",
			OSCL_PRETTY_FUNCTION
			);
		#endif
		return;
		}

	if(!allMemoryIsFree()){
		#ifdef DEBUG_TRACE
		Oscl::Error::Info::log(
			"%s: !allMemoryIsFree()\n",
			OSCL_PRETTY_FUNCTION
			);
		#endif
		return;
		}

	Oscl::Done::Api&	done	= *_stopDone;

	_stopDone	= 0;

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s: DONE()\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif

	done.done();
	}

void	Part::allFreeNotification() noexcept{
	if(!_stopDone){
		return;
		}

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif

	stopNext();	
	}

bool	Part::allMemoryIsFree() noexcept{
	return _freeStore.allMemoryIsFree();
	}

constexpr uint16_t	meshProxyServiceHandle					= 0x0020;

constexpr uint16_t	meshProxyDataInCharacteristicHandle		= 0x0021;
constexpr uint16_t	meshProxyDataInAttributeHandle			= 0x0022;

constexpr uint16_t	meshProxyDataOutCharacteristicHandle	= 0x0023;
constexpr uint16_t	meshProxyDataOutAttributeHandle			= 0x0024;

constexpr uint16_t	meshProxyDataOutHandle					= 0x0025;

constexpr	unsigned	nServices	= 1;

static const Oscl::BT::ATT::Server::TX::GroupInformation16bit	services[nServices] = {
	{	// <<Mesh Proxy Service>>
		.attributeHandle	= meshProxyServiceHandle,
		.groupEndHandle		= meshProxyDataOutHandle,
		.attributeValue		= Oscl::BT::GATT::meshProxyService
		}
	};

static const Oscl::BT::GATT::Server::CharacteristicDeclaration	characteristicDeclarations[] = {
	{	// Mesh Proxy Data In Characteristic
		.handle				= meshProxyDataInCharacteristicHandle,
		.properties			= 
				(0<<0)	// Broadcast
			|	(0<<1)	// Read
			|	(1<<2)	// Write Without Response
			|	(0<<3)	// Write
			|	(0<<4)	// Notify
			|	(0<<5)	// Indicate
			|	(0<<6)	// Authenticated Signed Writes
			|	(0<<7)	// Extended Properties
			,
		.attributeHandle	= meshProxyDataInAttributeHandle,
		.uuid16				= Oscl::BT::GATT::Characteristic::meshProxyDataIn
		},
	{	// Mesh Proxy Data Out Characteristic
		.handle				= meshProxyDataOutCharacteristicHandle,
		.properties			= 
				(0<<0)	// Broadcast
			|	(0<<1)	// Read
			|	(0<<2)	// Write Without Response
			|	(0<<3)	// Write
			|	(1<<4)	// Notify
			|	(0<<5)	// Indicate
			|	(0<<6)	// Authenticated Signed Writes
			|	(0<<7)	// Extended Properties
			,
		.attributeHandle	= meshProxyDataOutAttributeHandle,
		.uuid16				= Oscl::BT::GATT::Characteristic::meshProxyDataOut
		}
	};

static const Oscl::BT::ATT::Server::TX::InformationData16bit	attributes[] = {
	{	// <<Mesh Proxy Service>>
		.handle	= meshProxyServiceHandle,
		.uuid	= Oscl::BT::GATT::Declaration::primaryService
		},
	{	// Mesh Proxy Data In Characteristic Declaration
		.handle	= meshProxyDataInCharacteristicHandle,
		.uuid	= Oscl::BT::GATT::Declaration::characteristic
		},
	{	// Mesh Proxy Data In Attribute
		.handle	= meshProxyDataInAttributeHandle,
		.uuid	= Oscl::BT::GATT::Characteristic::meshProxyDataIn
		},
	{	// Mesh Proxy Data Out Characteristic Declaration
		.handle	= meshProxyDataOutCharacteristicHandle,
		.uuid	= Oscl::BT::GATT::Declaration::characteristic
		},
	{	// Mesh Proxy Data Out Attribute
		.handle	= meshProxyDataOutAttributeHandle,
		.uuid	= Oscl::BT::GATT::Characteristic::meshProxyDataOut
		},
	{	// Mesh Proxy Data Out
		.handle	= meshProxyDataOutHandle,
		.uuid	= Oscl::BT::GATT::Descriptor::clientCharacteristicConfiguration
		}
	};

const Oscl::BT::ATT::Server::TX::GroupInformation16bit*
		Part::getServices(unsigned& nServices) const noexcept{

	nServices	= sizeof(services)/sizeof(Oscl::BT::ATT::Server::TX::GroupInformation16bit);

	return services;
	}

const Oscl::BT::GATT::Server::CharacteristicDeclaration*
		Part::getCharacteristicDeclarations(unsigned& nDeclarations) const noexcept{

	nDeclarations	= sizeof(characteristicDeclarations)/sizeof(Oscl::BT::GATT::Server::CharacteristicDeclaration);

	return characteristicDeclarations;
	}

const Oscl::BT::ATT::Server::TX::InformationData16bit*
		Part::getAttributes(unsigned& nAttributes) const noexcept{

	nAttributes	= sizeof(attributes)/sizeof(Oscl::BT::ATT::Server::TX::InformationData16bit);
	return attributes;
	}

Oscl::BT::ATT::Server::TX::Api&	Part::getAttSendApi() noexcept{
	return _attSendApi;
	}

bool	Part::hasAttribute(uint16_t attributeHandle) const noexcept{
	switch(attributeHandle){
		case meshProxyDataInAttributeHandle:
		case meshProxyServiceHandle:
		case meshProxyDataInCharacteristicHandle:
		case meshProxyDataOutCharacteristicHandle:
		case meshProxyDataOutAttributeHandle:
			return true;
		default:
			return false;
		}
	}

void	Part::netPacketInfo(uint16_t src) noexcept{
	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s: processed, adding src address 0x%4.4X to filter.\n",
		OSCL_PRETTY_FUNCTION,
		src
		);
	#endif

	addWhiteListFilter(src);
	}

void	Part::processNetworkPDU(
			const void*	frame,
			unsigned	length
			) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION
		);
	Oscl::Error::Info::hexDump(
		frame,
		length
		);
	#endif

	for(
		_currentNetwork	= _networkIterator.first();
		_currentNetwork;
		_currentNetwork	= _networkIterator.next(_currentNetwork)
		){
		bool
		processed	= _currentNetwork->rxNetwork(
						frame,
						length,
						this
						);
		if(processed){
			_currentNetwork	= 0;
			return;
			}
		}
	}

void	Part::processMeshBeacon(
			const void*	frame,
			unsigned	length
			) noexcept{
	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION
		);
	Oscl::Error::Info::hexDump(
		frame,
		length
		);
	#endif
	}

void	Part::sendProxyFilterStatus() noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif

	unsigned	bufferSize;

	void*
	buffer	= _freeStore.allocBuffer(bufferSize);

	if(!buffer){
		Oscl::Error::Info::log(
			"%s: out of buffer memory.\n",
			OSCL_PRETTY_FUNCTION
			);
		return;
		}

	Oscl::Handle<Oscl::Pdu::Fixed>	fixed;
		{
		Oscl::Encoder::BE::Base
		beEncoder(
			buffer,
			bufferSize
			);

		Oscl::Encoder::Api&	encoder	= beEncoder;

		static const uint8_t	opcode	= 0x03;

		encoder.encode(opcode);
		encoder.encode(_proxyFilterType);
		encoder.encode(_proxyFilterListSize);

		if(encoder.overflow()){
			Oscl::Error::Info::log(
				"%s: overflow!\n",
				OSCL_PRETTY_FUNCTION
				);
			return;
			}

		fixed	= _freeStore.allocFixed(
					buffer,
					encoder.length()
					);

		if(!fixed){
			_freeStore.freeBuffer(buffer);
			Oscl::Error::Info::log(
				"%s: out of header memory.\n",
				OSCL_PRETTY_FUNCTION
				);
			return;
			}
		}

	_currentNetwork->encryptProxy(
		_proxyConfigurationTxComposer,
		fixed
		);
	}

const char*	Part::currentFilterTypeString() noexcept{
	switch(_proxyFilterType){
		case 0x00:
			return "White list filter";
		case 0x01:
			return "Black list filter";
		default:
			return "Unknown";
		}
	}

void	Part::processSetFilter(Oscl::Endian::Decoder::Api& dec) noexcept{

	Oscl::Decoder::Api&
	decoder	= dec.be();

	decoder.decode(_proxyFilterType);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			OSCL_PRETTY_FUNCTION
			);
		return;
		}

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n"
		"\tFilter Type: (0x%2.2X) %s\n"
		,
		OSCL_PRETTY_FUNCTION,
		_proxyFilterType,
		currentFilterTypeString()
		);
	#endif

	sendProxyFilterStatus();
	}

void	Part::processAddAddressesToFilter(Oscl::Endian::Decoder::Api& dec) noexcept{

	Oscl::Decoder::Api&
	decoder	= dec.be();

	unsigned
	nAddresses	= decoder.remaining()/2;

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n"
		"\t_proxyFilterType: (0x%2.2X) %s\n"
		"\tremaining: %u\n"
		"\tnAddresses: %u\n"
		"\tAddress Array:\n"
		"",
		OSCL_PRETTY_FUNCTION,
		_proxyFilterType,
		currentFilterTypeString(),
		decoder.remaining(),
		nAddresses
		);
	#endif

	for(unsigned i=0;i<nAddresses;++i){
		uint16_t	address;
		decoder.decode(address);
		if(decoder.underflow()){
			Oscl::Error::Info::log(
				"%s: underflow.\n",
				OSCL_PRETTY_FUNCTION
				);
			return;
			}

		#ifdef DEBUG_TRACE
		Oscl::Error::Info::log(
			"\t\t[%u]: 0x%4.4X\n"
			"",
			i,
			address
			);
		#endif

		switch(_proxyFilterType){
			case 0x00:
				// White List Filter
				addWhiteListFilter(address);
				break;
			case 0x01:
				// Black List Filter
				addBlackListFilter(address);
				break;
			default:
				break;
			}
		}
	sendProxyFilterStatus();
	}

void	Part::processRemoveAddressesFromFilter(Oscl::Endian::Decoder::Api& dec) noexcept{

	Oscl::Decoder::Api&
	decoder	= dec.be();

	unsigned
	nAddresses	= decoder.remaining()/2;

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n"
		"\tremaining: %u\n"
		"\tnAddresses: %u\n"
		"\tAddress Array:\n"
		"",
		OSCL_PRETTY_FUNCTION,
		decoder.remaining(),
		nAddresses
		);
	#endif

	for(unsigned i=0;i<nAddresses;++i){
		uint16_t	address;
		decoder.decode(address);
		if(decoder.underflow()){
			Oscl::Error::Info::log(
				"%s: underflow.\n",
				OSCL_PRETTY_FUNCTION
				);
			return;
			}
		#ifdef DEBUG_TRACE
		Oscl::Error::Info::log(
			"\t\t[%u]: 0x%4.4X\n"
			"",
			i,
			address
			);
		#endif
		switch(_proxyFilterType){
			case 0x00:
				// White List Filter
				removeWhiteListFilter(address);
				break;
			case 0x01:
				// Black List Filter
				removeBlackListFilter(address);
				break;
			default:
				break;
			}
		}
	sendProxyFilterStatus();
	}

bool	Part::processDecryptedProxyConfiguration(
			const void*	frame,
			unsigned	length
			) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION
		);
	Oscl::Error::Info::hexDump(
		frame,
		length
		);
	#endif

	Oscl::Endian::Decoder::Linear::Part
	beDecoder(
		frame,
		length
		);

	Oscl::Decoder::Api&	decoder = beDecoder.be();

	constexpr unsigned	seqSize	= 3;
	constexpr unsigned	srcAddressSize	= sizeof(uint16_t);

	uint8_t		iviNID;
	uint8_t		ctlTTL;
	uint16_t	dstAddress;
	uint8_t		opcode;

	decoder.decode(iviNID);
	decoder.decode(ctlTTL);

	decoder.skip(seqSize+srcAddressSize);

	decoder.decode(dstAddress);
	decoder.decode(opcode);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			OSCL_PRETTY_FUNCTION
			);
		return true;
		}

	if(dstAddress != 0x0000){
		// Not the required unassigned address!
		Oscl::Error::Info::log(
			"%s: Invalid destination address: 0x%4.4X, expected the unassigned address.\n",
			OSCL_PRETTY_FUNCTION,
			dstAddress
			);
		return true;
		}

	switch(opcode){
		case 0x00:
			// Set Filter
			processSetFilter(beDecoder);
			break;
		case 0x01:
			// Add Addresses To Filter
			processAddAddressesToFilter(beDecoder);
			break;
		case 0x02:
			// Remove Addresses From Filter
			processRemoveAddressesFromFilter(beDecoder);
			break;
		case 0x03:
			// Filter Status
			// This should not be received
			// by the server.
		default:
			Oscl::Error::Info::log(
				"%s: Invalid opcode (0x%2.2X)\n",
				OSCL_PRETTY_FUNCTION,
				opcode
				);
			break;
		}

	return true;
	}

void	Part::processUnsupportedOpcode(
			const void*	frame,
			unsigned	length,
			const char*	opcodeString,
			uint8_t		opcode
			) noexcept{
	Oscl::Error::Info::log(
		"%s: opcode: 0x%2.2X (%s) Not supported for <<Mesh Proxy Service>>\n",
		OSCL_PRETTY_FUNCTION,
		opcode,
		opcodeString
		);
	Oscl::Error::Info::hexDump(
		frame,
		length
		);
	}

void	Part::processProxyConfiguration(
			const void*	frame,
			unsigned	length
			) noexcept{
	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION
		);
	Oscl::Error::Info::hexDump(
		frame,
		length
		);
	#endif

	for(
		_currentNetwork	= _networkIterator.first();
		_currentNetwork;
		_currentNetwork	= _networkIterator.next(_currentNetwork)
		){
		bool
		processed	= _currentNetwork->decryptProxy(
						_proxyConfigurationReceiverComposer,
						frame,
						length
						);
		if(processed){
			_currentNetwork	= 0;
			return;
			}
		}
	}

bool	Part::addWhiteListFilter(uint16_t address) noexcept{

	if(matchWhiteListFilter(address)){
		return false;
		}

	Oscl::BT::Mesh::Address::Filter::RecordMem*
	mem	= _freeFilterRecordMem.get();
	if(!mem){
		Oscl::Error::Info::log(
			"%s: out of filter memory.\n",
			OSCL_PRETTY_FUNCTION
			);
		return true;
		}

	Oscl::BT::Mesh::Address::Filter::Record*
	filter	= new (mem)
				Oscl::BT::Mesh::Address::
				Filter::Record(address);

	_whiteListFilters.put(filter);
	++_proxyFilterListSize;

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s: added address 0x%4.4X\n",
		OSCL_PRETTY_FUNCTION,
		address
		);
	#endif

	return false;
	}

bool	Part::removeWhiteListFilter(uint16_t address) noexcept{

	for(
		Oscl::BT::Mesh::Address::Filter::Record*
		record	= _whiteListFilters.first();
		record;
		record	= _whiteListFilters.next(record)
		){
		if(record->match(address)){

			_whiteListFilters.remove(record);
			--_proxyFilterListSize;

			_freeFilterRecordMem.put(
				(Oscl::BT::Mesh::Address::Filter::RecordMem*)record
				);

			#ifdef DEBUG_TRACE
			Oscl::Error::Info::log(
				"%s: removed address 0x%4.4X\n",
				OSCL_PRETTY_FUNCTION,
				address
				);
			#endif

			return true;
			}
		}

	return false;
	}

bool	Part::addBlackListFilter(uint16_t address) noexcept{
	if(matchBlackListFilter(address)){
		return false;
		}

	Oscl::BT::Mesh::Address::Filter::RecordMem*
	mem	= _freeFilterRecordMem.get();
	if(!mem){
		Oscl::Error::Info::log(
			"%s: out of filter memory.\n",
			OSCL_PRETTY_FUNCTION
			);
		return true;
		}

	Oscl::BT::Mesh::Address::Filter::Record*
	filter	= new (mem)
				Oscl::BT::Mesh::Address::
				Filter::Record(address);

	_blackListFilters.put(filter);
	++_proxyFilterListSize;

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s: added address 0x%4.4X\n",
		OSCL_PRETTY_FUNCTION,
		address
		);
	#endif

	return false;
	}

bool	Part::removeBlackListFilter(uint16_t address) noexcept{
	for(
		Oscl::BT::Mesh::Address::Filter::Record*
		record	= _blackListFilters.first();
		record;
		record	= _blackListFilters.next(record)
		){
		if(record->match(address)){

			_blackListFilters.remove(record);
			--_proxyFilterListSize;

			_freeFilterRecordMem.put(
				(Oscl::BT::Mesh::Address::Filter::RecordMem*)record
				);

			#ifdef DEBUG_TRACE
			Oscl::Error::Info::log(
				"%s: removed address 0x%4.4X\n",
				OSCL_PRETTY_FUNCTION,
				address
				);
			#endif

			return true;
			}
		}

	return false;
	}

bool	Part::matchWhiteListFilter(uint16_t address) const noexcept{
	for(
		Oscl::BT::Mesh::Address::Filter::Record*
		record	= _whiteListFilters.first();
		record;
		record	= _whiteListFilters.next(record)
		){
		if(record->match(address)){
			return true;
			}
		}

	return false;
	}

bool	Part::matchBlackListFilter(uint16_t address) const noexcept{
	for(
		Oscl::BT::Mesh::Address::Filter::Record*
		record	= _blackListFilters.first();
		record;
		record	= _blackListFilters.next(record)
		){
		if(record->match(address)){
			return true;
			}
		}

	return false;
	}

void	Part::processAssembledPDU(
			const void*	frame,
			unsigned	length
			) noexcept{

	const uint8_t*	pdu	= (const uint8_t*)frame;

	uint8_t	opcode	= pdu[0];

	switch(opcode){
		case 0x00:
			// Network PDU
			processNetworkPDU(
				&pdu[1],
				length - 1
				);
			break;
		case 0x01:
			// Mesh Beacon
			processMeshBeacon(
				&pdu[1],
				length - 1
				);
			break;
		case 0x02:
			// Proxy Configuration
			processProxyConfiguration(
				&pdu[1],
				length - 1
				);
			break;
		case 0x03:
			// Provisioning PDU
			processUnsupportedOpcode(
				frame,
				length,
				"Provisioning PDU",
				opcode
				);

			break;
		default:
			Oscl::Error::Info::log(
				"%s: unknown Proxy PDU opcode (0x%2.2X)!\n",
				OSCL_PRETTY_FUNCTION,
				opcode
				);
			break;
		}
	}

void	Part::sendSecureNetworkBeacon(
					const uint8_t		beaconKey[16],
					const uint8_t		networkID[8],
					uint32_t			ivIndex,
					uint8_t				flags
					) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s:"
		" networkID: %2.2X:%2.2X:%2.2X:%2.2X:%2.2X:%2.2X:%2.2X:%2.2X,"
		" ivIndex: 0x%8.8X,"
		" flags: 0x%2.2X"
		"\n",
		OSCL_PRETTY_FUNCTION,
		networkID[0],
		networkID[1],
		networkID[2],
		networkID[3],
		networkID[4],
		networkID[5],
		networkID[6],
		networkID[7],
		ivIndex,
		flags
		);
	#endif

	unsigned	bufferSize;

	void*
	buffer	= _freeStore.allocBuffer(bufferSize);

	if(!buffer){
		Oscl::Error::Info::log(
			"%s: out of buffer memory.\n",
			OSCL_PRETTY_FUNCTION
			);
		return;
		}

	Oscl::Handle<Oscl::Pdu::Fixed>	fixed;
		{
		Oscl::Encoder::BE::Base
		beEncoder(
			buffer,
			bufferSize
			);

		Oscl::Encoder::Api&	encoder	= beEncoder;

		static const uint8_t	opcode	= 0x01;

		encoder.encode(opcode);
		encoder.encode(flags);
		encoder.copyIn(
			networkID,
			8
			);
		encoder.encode(ivIndex);

		if(encoder.overflow()){
			Oscl::Error::Info::log(
				"%s: overflow!\n",
				OSCL_PRETTY_FUNCTION
				);
			return;
			}

		uint8_t*	p	= (uint8_t*)buffer;

		Oscl::AES128::CMAC  cmac(beaconKey);

		const unsigned
		authOffset	= sizeof(opcode);

		const unsigned
		authSize	= sizeof(flags) + 8 + 4;

		const unsigned
		authValueSize	= 8;

		cmac.accumulate(
			&p[authOffset],
			authSize
			);

		cmac.finalize();

		cmac.copyOut(
			&p[authOffset + authSize],
			authValueSize
			);

		encoder.skip(authValueSize);

		fixed	= _freeStore.allocFixed(
					buffer,
					encoder.length()
					);

		if(!fixed){
			_freeStore.freeBuffer(buffer);
			Oscl::Error::Info::log(
				"%s: out of header memory.\n",
				OSCL_PRETTY_FUNCTION
				);
			return;
			}
		#ifdef DEBUG_TRACE
		Oscl::Error::Info::log(
			"%s: encoder.length(): %u, Secure Beacon:\n",
			OSCL_PRETTY_FUNCTION,
			encoder.length()
			);

		Oscl::Error::Info::hexDump(
			buffer,
			encoder.length()
			);
		#endif
		}

	meshBeaconGattTx(fixed);
	}

void	Part::sendSecureNetworkBeacons() noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif

	for(
		Oscl::BT::Mesh::Network::Api*
		network	= _networkIterator.first();
		network;
		network	= _networkIterator.next(network)
		){
		sendSecureNetworkBeacon(
			(const uint8_t*)network->getBeaconKey(),
			(const uint8_t*)network->getNetworkID(),
			network->getTxIvIndex(),
			network->getFlags()
			);
		}
	}

void	Part::notifyMeshProxyDataOut(Oscl::Pdu::Pdu* pdu) noexcept{
	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif

	_attSendApi.sendHandleValueNotification(
		meshProxyDataOutAttributeHandle,	// uint16_t        attributeHandle,
		pdu									// Oscl::Pdu::Pdu* attributeValue
		);
	}

void	Part::networkGattTx(Oscl::Pdu::Pdu* pdu) noexcept{
	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif

	if(_stopDone){
		return;
		}

	unsigned
	length	= pdu->length();

	// WTF meshctl!
	#ifdef MESHCTL_MTU_WORKAROUND
	unsigned	mtu	= 20 - (3+1);
	#else
	unsigned	mtu	= _attMTU - (3 + 1);
	#endif

	if(length > mtu){
		// FIXME: This gattSegmentAndTransmit() is needed
		// for meshctl, but breaks ST BLE Mesh, which uses
		// a larger MTU.
		// The work-around for this is to specify a larger
		// MTU on the btmesh command-line.
		Oscl::BT::Mesh::Proxy::SAR::gattSegmentAndTransmit(
			pdu,
			_freeStore,
			_notifyMeshProxyDataOutComposer,
			Oscl::BT::Mesh::Proxy::SAR::Opcodes::networkPduOpcodes,
			mtu
			);
		return;
		}

	Oscl::Handle<Oscl::Pdu::Composite>
	composite	= _freeStore.allocConstWrapper(
					&Oscl::BT::Mesh::Proxy::SAR::Opcodes::networkPduOpcodes._complete,
					sizeof(Oscl::BT::Mesh::Proxy::SAR::Opcodes::networkPduOpcodes._complete)
					);

	if(!composite){
		Oscl::Error::Info::log(
			"%s: out of composite wrappers.\n",
			OSCL_PRETTY_FUNCTION
			);
		return;
		}

	Oscl::Handle<Oscl::Pdu::Fragment>
	fragment	= _freeStore.allocFragment(
					*pdu
					);

	if(!fragment){
		Oscl::Error::Info::log(
			"%s: out of fragments.\n",
			OSCL_PRETTY_FUNCTION
			);
		return;
		}

	composite->append(fragment);

	notifyMeshProxyDataOut(composite);
	}

void	Part::meshBeaconGattTx(Oscl::Pdu::Pdu* pdu) noexcept{
	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s: _attMTU: %u,_attMTU-4: (%u): pdu->length(): %u\n",
		OSCL_PRETTY_FUNCTION,
		_attMTU,
		_attMTU - (3+1),
		pdu->length()
		);
	#endif

	if(_stopDone){
		return;
		}

	unsigned
	length	= pdu->length();

	// WTF meshctl
	#ifdef MESHCTL_MTU_WORKAROUND
	unsigned	mtu	= 20 - (3+1);
	#else
	unsigned	mtu	= _attMTU - (3 + 1);
	#endif

	if(length > mtu){
		Oscl::BT::Mesh::Proxy::SAR::gattSegmentAndTransmit(
			pdu,
			_freeStore,
			_notifyMeshProxyDataOutComposer,
			Oscl::BT::Mesh::Proxy::SAR::Opcodes::meshBeaconOpcodes,
			mtu
			);
		return;
		}

	Oscl::Handle<Oscl::Pdu::Composite>
	composite	= _freeStore.allocConstWrapper(
					&Oscl::BT::Mesh::Proxy::SAR::Opcodes::meshBeaconOpcodes._complete,
					sizeof(Oscl::BT::Mesh::Proxy::SAR::Opcodes::meshBeaconOpcodes._complete)
					);

	if(!composite){
		Oscl::Error::Info::log(
			"%s: out of composite wrappers.\n",
			OSCL_PRETTY_FUNCTION
			);
		return;
		}

	Oscl::Handle<Oscl::Pdu::Fragment>
	fragment	= _freeStore.allocFragment(
					*pdu
					);

	if(!fragment){
		Oscl::Error::Info::log(
			"%s: out of fragments.\n",
			OSCL_PRETTY_FUNCTION
			);
		return;
		}

	composite->append(fragment);

	notifyMeshProxyDataOut(composite);
	}

void	Part::networkGattBearerTx(
			Oscl::Pdu::Pdu* pdu,
			uint16_t		dst
			) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s: dst: 0x%4.4X\n",
		OSCL_PRETTY_FUNCTION,
		dst
		);
	#endif

	if(_stopDone){
		return;
		}

	// FIXME: implement output filter

	if(matchBlackListFilter(dst)){
		#ifdef DEBUG_TRACE
		Oscl::Error::Info::log(
			"%s: dst (0x%4.4X) matched black-list filter. Dropped.\n",
			OSCL_PRETTY_FUNCTION,
			dst
			);
		#endif
		return;
		}

	if(!matchWhiteListFilter(dst)){
		#ifdef DEBUG_TRACE
		Oscl::Error::Info::log(
			"%s: dst (0x%4.4X) did not match white-list filter. Dropped.\n",
			OSCL_PRETTY_FUNCTION,
			dst
			);
		#endif
		return;
		}

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s: dst (0x%4.4X) passed filter. forwarded\n",
		OSCL_PRETTY_FUNCTION,
		dst
		);
	#endif

	// Uncomment this is as a special test function
	// to test timeouts in the application.
//	return;

	networkGattTx(pdu);
	}

void	Part::proxyConfigurationTx(Oscl::Pdu::Pdu* pdu) noexcept{

	if(_stopDone){
		return;
		}

	unsigned
	length	= pdu->length();

	// WTF meshctl
	#ifdef MESHCTL_MTU_WORKAROUND
	unsigned	mtu	= 20 - (3+1);
	#else
	unsigned	mtu	= _attMTU - (3 + 1);
	#endif

	if(length > mtu){
		Oscl::BT::Mesh::Proxy::SAR::gattSegmentAndTransmit(
			pdu,
			_freeStore,
			_notifyMeshProxyDataOutComposer,
			Oscl::BT::Mesh::Proxy::SAR::Opcodes::proxyConfigurationOpcodes,
			mtu
			);
		return;
		}

	Oscl::Handle<Oscl::Pdu::Composite>
	composite	= _freeStore.allocConstWrapper(
					&Oscl::BT::Mesh::Proxy::SAR::Opcodes::proxyConfigurationOpcodes._complete,
					sizeof(Oscl::BT::Mesh::Proxy::SAR::Opcodes::proxyConfigurationOpcodes._complete)
					);

	if(!composite){
		Oscl::Error::Info::log(
			"%s: out of composite wrappers.\n",
			OSCL_PRETTY_FUNCTION
			);
		return;
		}

	Oscl::Handle<Oscl::Pdu::Fragment>
	fragment	= _freeStore.allocFragment(
					*pdu
					);

	if(!fragment){
		Oscl::Error::Info::log(
			"%s: out of fragments.\n",
			OSCL_PRETTY_FUNCTION
			);
		return;
		}

	composite->append(fragment);

	notifyMeshProxyDataOut(composite);
	}

bool	Part::writeRequest(
			uint16_t		attributeHandle,
			const uint8_t	attributeValue[],
			unsigned		attributeValueLength
			) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s: handle: 0x%4.4X\n",
		OSCL_PRETTY_FUNCTION,
		attributeHandle
		);
	Oscl::Error::Info::hexDump(
		attributeValue,
		attributeValueLength
		);
	#endif

	Oscl::Endian::Decoder::Linear::Part
	leDecoder(
		attributeValue,
		attributeValueLength
		);

	Oscl::Decoder::Api&	decoder	= leDecoder.le();

	// Here we need to determine which attribute
	// is being written. Currently, the only attribute
	// that accepts Write operations is the
	// Mesh Provisioning Data Out <<Client Characteristic Configuration>>
	// This is used to enable/disable Handle Value Notifications
	// from the device.
	// IMHO, this is a waste since such notifications only
	// happen after a Provisioing Invite and not unsolicited.

	switch(attributeHandle){
		case meshProxyDataOutHandle:
			{
			// <<Client Characteristic Configuration>>
			// Mesh Proxy Data Out
			// Notification enable/disable
			// uint16_t	value
			bool
			previousProxyEnableValue	= _proxyEnableValue;

			decoder.decode(_proxyEnableValue);

			if(decoder.underflow()){
				Oscl::Error::Info::log(
					"%s: _proxyEnableValue underflow!\n",
					OSCL_PRETTY_FUNCTION
					);
				_attSendApi.sendErrorResponse(
					Oscl::BT::ATT::Opcode::writeRequest,
					attributeHandle,
					Oscl::BT::ATT::ErrorCode::invalidPDU
					);
				return false;
				}

			_attSendApi.sendWriteResponse();

			if(!previousProxyEnableValue){
				if(_proxyEnableValue){
					// Transition from disbled to enabled.
					sendSecureNetworkBeacons();
					}
				}

			}
			break;
		default:
			_attSendApi.sendErrorResponse(
				Oscl::BT::ATT::Opcode::writeRequest,
				attributeHandle,
				Oscl::BT::ATT::ErrorCode::writeNotPermitted
				);
			break;
		}

	return true;
	}

bool	Part::writeCommand(
			uint16_t		attributeHandle,
			const uint8_t	attributeValue[],
			unsigned		attributeValueLength
			) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s:"
		"\tHandle: 0x%4.4X\n"
		"\tValue:\n",
		OSCL_PRETTY_FUNCTION,
		attributeHandle
		);

	Oscl::Error::Info::hexDump(
		attributeValue,
		attributeValueLength
		);
	#endif

	if(!hasAttribute(attributeHandle)){
		return false;
		}

	// At this point, we have a command
	// destined for Not Mesh Provisioning Data In
	// The value is the command and it consists
	// of possibly segmented PDU that needs to
	// be reassembled.

	uint8_t	opcode	= attributeValue[0];
	uint8_t	sar		= opcode >> 6;
	opcode	&= 0x3F;

	Oscl::Encoder::Api&	encoder	= _reasmEncoder;

	switch(sar){
		case 0x00:
			// Complete Message
			processAssembledPDU(
				&attributeValue[0],
				attributeValueLength
				);
			break;
		case 0x01:
			// First Segment
			_reasmEncoder.reset();
			encoder.encode(opcode);
			encoder.copyIn(
				&attributeValue[1],
				attributeValueLength - 1
				);
			#ifdef DEBUG_TRACE
			Oscl::Error::Info::log(
				"\tFirst Segment packet (encoder.length(): %u):\n",
				encoder.length()
				);
			Oscl::Error::Info::hexDump(
				_reasmBuffer,
				encoder.length()
				);
			#endif
			break;
		case 0x02:
			// Continuation Segment
			encoder.copyIn(
				&attributeValue[1],
				attributeValueLength - 1
				);
			#ifdef DEBUG_TRACE
			Oscl::Error::Info::log(
				"\tContinuation Segment packet (encoder.length(): %u):\n",
				encoder.length()
				);
			Oscl::Error::Info::hexDump(
				_reasmBuffer,
				encoder.length()
				);
			#endif
			break;
		case 0x03:
			// Last Segment
			encoder.copyIn(
				&attributeValue[1],
				attributeValueLength - 1
				);
			if(encoder.overflow()){
				Oscl::Error::Info::log(
					"%s: overflow!\n",
					OSCL_PRETTY_FUNCTION
					);
				}
			#ifdef DEBUG_TRACE
			Oscl::Error::Info::log(
				"\tReassembled packet (encoder.length(): %u):\n",
				encoder.length()
				);
			Oscl::Error::Info::hexDump(
				_reasmBuffer,
				encoder.length()
				);
			#endif
			processAssembledPDU(
				_reasmBuffer,
				encoder.length()
				);
			break;
		}

	return true;
	}

