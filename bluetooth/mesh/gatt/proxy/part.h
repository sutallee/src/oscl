/*
   Copyright (C) 2020 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_bluetooth_mesh_gatt_proxy_parth_
#define _oscl_bluetooth_mesh_gatt_proxy_parth_

#include <stdint.h>
#include "oscl/bluetooth/gatt/service/item.h"
#include "oscl/pdu/fwd/composer.h"
#include "oscl/pdu/memory/config.h"
#include "oscl/freestore/composers.h"
#include "oscl/done/operation.h"
#include "oscl/encoder/le/base.h"
#include "oscl/frame/fwd/composer.h"
#include "oscl/endian/decoder/api.h"
#include "oscl/bluetooth/mesh/node/creator/api.h"
#include "oscl/bluetooth/mesh/network/iteration/api.h"
#include "oscl/bluetooth/mesh/bearer/tx/itemcomp.h"
#include "oscl/bluetooth/mesh/bearer/tx/host/api.h"
#include "oscl/bluetooth/mesh/address/filter/record.h"
#include "oscl/bluetooth/mesh/network/interface/api.h"
#include "oscl/bluetooth/att/server/rx/api.h"
#include "oscl/bluetooth/att/server/tx/api.h"
#include "oscl/bluetooth/mesh/proxy/sar.h"
#include "oscl/bluetooth/gatt/server/base.h"

/** */
namespace Oscl {
/** */
namespace BT {
/** */
namespace Mesh {
/** */
namespace GATT {
/** */
namespace Proxy {

/** */
class Part :
	private Oscl::BT::Mesh::Network::Interface::Api,
	public Oscl::BT::GATT::Server::Base
	{
	private:
		/** */
		Oscl::BT::Mesh::Bearer::
		TX::Host::Api&					_bearerHostApi;

		Oscl::BT::Mesh::
		Network::Iteration::Api&		_networkIterator;

		Oscl::BT::ATT::Server::TX::Api&	_attSendApi;

		/** */
		const uint16_t&					_attMTU;

	private:
		/** */
		Oscl::BT::Mesh::Bearer::
		TX::ItemComposer<Part>			_networkGattBearerTxComposer;

	private:
		/** */
		Oscl::Frame::FWD::Composer<Part>	_proxyConfigurationReceiverComposer;

		/** */
		Oscl::Pdu::FWD::Composer<Part>	_proxyConfigurationTxComposer;

		/** */
		Oscl::Pdu::FWD::Composer<Part>	_notifyMeshProxyDataOutComposer;

	private:
		/** */
		Oscl::Done::Operation<Part>		_allFreeNotification;

	private:
		/** */
		constexpr static unsigned	freeStoreBufferSize	= 128;
		/** */
		constexpr static unsigned	nFreeStoreBuffers	= 32;
		/** */
		constexpr static unsigned	nFixed		= 32;
		/** */
		constexpr static unsigned	nFragment	= 32;
		/** */
		constexpr static unsigned	nComposite	= 32;

		/** */
		Oscl::Pdu::Memory::
		Config<
			freeStoreBufferSize,
			nFreeStoreBuffers,
			nFixed,
			nFragment,
			nComposite
			>								_freeStore;

	private:
		/** FIXME: */
		static constexpr unsigned	maxPduSize	= 512;

		/** FIXME: */
		uint8_t						_reasmBuffer[maxPduSize];

		/** */
		Oscl::Encoder::LE::Base		_reasmEncoder;

	private:
		/** */
		Oscl::BT::Mesh::Network::Api*	_currentNetwork;

	private:
		/** 0x0015 */
		uint16_t		_provisioningEnableValue;

	private:
		/** 0x0025 */
		uint16_t		_proxyEnableValue;

		/** */
		uint16_t		_proxyFilterListSize;

		/** */
		uint8_t			_proxyFilterType;

	private:
		/** */
		constexpr static unsigned	nFilterRecordMem	= 4;

		/** */
		Oscl::BT::Mesh::
		Address::Filter::RecordMem	_filterRecordMem[nFilterRecordMem];

		/** */
		Oscl::Queue<
			Oscl::BT::Mesh::
			Address::Filter::
			RecordMem
			>					_freeFilterRecordMem;

		/** */
		Oscl::Queue<
			Oscl::BT::Mesh::
			Address::Filter::
			Record
			>					_whiteListFilters;

		/** */
		Oscl::Queue<
			Oscl::BT::Mesh::
			Address::Filter::
			Record
			>					_blackListFilters;

	private:
		/** */
		Oscl::Done::Api*	_stopDone;

	public:
		/** */
		Part(
			Oscl::BT::Mesh::Bearer::
			TX::Host::Api&					bearerHostApi,
			Oscl::BT::Mesh::
			Network::Iteration::Api&		networkIterator,
			Oscl::BT::ATT::Server::TX::Api&	attSendApi,
			const uint16_t&					attMTU
			) noexcept;

		/** */
		void	start() noexcept;

		/** */
		void	stop(Oscl::Done::Api& stopDone) noexcept;

		/** */
		bool	isDestinedForProxy(uint16_t address) noexcept;

		/** */
		void	networkSecurityChanged() noexcept;

	private: // Oscl::BT::Mesh::Network::Interface::Api
		/** This operation is invoked by the Network
			if the packet is successfully decrypted
			and by the Network layer.
		 */
		void	netPacketInfo(uint16_t src) noexcept;

	private:
		/** */
		void	processUnsupportedOpcode(
					const void*	frame,
					unsigned	length,
					const char*	opcodeString,
					uint8_t		opcode
					) noexcept;

		/** */
		void	processNetworkPDU(
					const void*	frame,
					unsigned	length
					) noexcept;

		/** */
		void	processMeshBeacon(
					const void*	frame,
					unsigned	length
					) noexcept;

		/** */
		void	processProxyConfiguration(
					const void*	frame,
					unsigned	length
					) noexcept;

	private:
		/** */
		const char*	currentFilterTypeString() noexcept;

		/** */
		void	processSetFilter(Oscl::Endian::Decoder::Api& decoder) noexcept;

		/** */
		void	processAddAddressesToFilter(Oscl::Endian::Decoder::Api& decoder) noexcept;

		/** */
		void	processRemoveAddressesFromFilter(Oscl::Endian::Decoder::Api& decoder) noexcept;

		/** */
		bool	processDecryptedProxyConfiguration(
					const void*	frame,
					unsigned	length
					) noexcept;

	private:
		/** RETURN: true for out-of-resources. */
		bool	addWhiteListFilter(uint16_t address) noexcept;

		/** RETURN: true if address is in white-list. */
		bool	removeWhiteListFilter(uint16_t address) noexcept;

		/** RETURN: true for out-of-resources. */
		bool	addBlackListFilter(uint16_t address) noexcept;

		/** RETURN: true if address is in black-list. */
		bool	removeBlackListFilter(uint16_t address) noexcept;

		/** RETURN: true if address is in white-list. */
		bool	matchWhiteListFilter(uint16_t address) const noexcept;

		/** RETURN: true if address is in black-list. */
		bool	matchBlackListFilter(uint16_t address) const noexcept;

	private:
		/** */
		void	sendSecureNetworkBeacon(
					const uint8_t	beaconKey[16],
					const uint8_t	networkID[8],
					uint32_t		ivIndex,
					uint8_t			flags
					) noexcept;

		/** */
		void	sendSecureNetworkBeacons() noexcept;

		/** */
		void	sendProxyFilterStatus() noexcept;

	private:
		/** */
		void	stopNext() noexcept;

		/** */
		bool	allMemoryIsFree() noexcept;

	private: // Oscl::Done::Operation _allFreeNotification
		/** */
		void	allFreeNotification() noexcept;

	private:
		/** */
		void	notifyMeshProxyDataOut(Oscl::Pdu::Pdu* pdu) noexcept;

	private: // Oscl::BT::Mesh::Bearer::TX::Item _networkGattBearerTxComposer
		/** */
		void	networkGattBearerTx(
					Oscl::Pdu::Pdu* pdu,
					uint16_t		dst
					) noexcept;

	private:
		/** */
		void	networkGattTx(Oscl::Pdu::Pdu* pdu) noexcept;

	private:
		/** */
		void	meshBeaconGattTx(Oscl::Pdu::Pdu* pdu) noexcept;

	private: // Oscl::Pdu::FWD::Composer _proxyConfigurationTxComposer
		/** */
		void	proxyConfigurationTx(Oscl::Pdu::Pdu* pdu) noexcept;

		/** */
		bool	hasAttribute(uint16_t attributeHandle) const noexcept;

	private: // Oscl::BT::GATT::Server::Base, Oscl::BT::ATT::Server::RX::Api
		/** */
		void	processAssembledPDU(
					const void*	frame,
					unsigned	length
					) noexcept;

		/** */
		const Oscl::BT::ATT::Server::TX::GroupInformation16bit*	getServices(unsigned& nServices) const noexcept;

		/** */
		const Oscl::BT::GATT::Server::CharacteristicDeclaration*	getCharacteristicDeclarations(unsigned& nDeclarations) const noexcept;

		/** */
		const Oscl::BT::ATT::Server::TX::InformationData16bit*	getAttributes(unsigned& nAttributes) const noexcept;

		/** */
		Oscl::BT::ATT::Server::TX::Api&	getAttSendApi() noexcept;

		/**
			uint8_t opcode = 0x12
			RETURN: true if processed or false if not implemented.
		 */
		bool	writeRequest(
					uint16_t		attributeHandle,
					const uint8_t	attributeValue[],
					unsigned		attributeValueLength
					) noexcept;

		/**
			uint8_t opcode = 0x52
			RETURN: true if processed or false if not implemented.
		 */
		bool	writeCommand(
					uint16_t		attributeHandle,
					const uint8_t	attributeValue[],
					unsigned		attributeValueLength
					) noexcept;
	};

}
}
}
}
}

#endif
