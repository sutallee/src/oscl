/*
   Copyright (C) 2021 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_bluetooth_mesh_transport_upper_rx_context_apih_
#define _oscl_bluetooth_mesh_transport_upper_rx_context_apih_

#include "oscl/bluetooth/mesh/subscription/register/iteration.h"

/** */
namespace Oscl {
/** */
namespace BT {
/** */
namespace Mesh {
/** */
namespace Transport {
/** */
namespace Upper {
/** */
namespace RX {
/** */
namespace Context {

/** */
class Api {
	public:
		/** Process the decrypted Access Message
			The appKeyIndex is greater than 0x0FFF
			if this message is was decrypted using
			the DevKey.
		 */
		virtual void	processAccessMessage(
							const void*	frame,
							unsigned	length,
							uint16_t	src,
							uint16_t	dst,
							uint16_t	appKeyIndex
							) noexcept=0;

		/** */
		virtual Oscl::BT::Mesh::Subscription::
		Register::IterationApi&	getSubscriptionIterationApi() noexcept=0;

		/** */
		virtual const void*	getDeviceKey() const noexcept=0;
	};

}
}
}
}
}
}
}

#endif
