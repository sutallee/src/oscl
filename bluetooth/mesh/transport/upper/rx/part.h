/*
   Copyright (C) 2021 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_bluetooth_mesh_transport_upper_rx_parth_
#define _oscl_bluetooth_mesh_transport_upper_rx_parth_

#include "api.h"
#include "oscl/queue/queue.h"
#include "oscl/bluetooth/mesh/node/api.h"
#include "oscl/bluetooth/mesh/model/rx/item.h"
#include "oscl/bluetooth/mesh/transport/upper/tx/app/part.h"
#include "oscl/pdu/memory/api.h"
#include "oscl/bluetooth/mesh/model/api.h"
#include "oscl/bluetooth/mesh/network/api.h"
#include "oscl/bluetooth/mesh/model/key/api.h"
#include "oscl/bluetooth/mesh/model/key/record.h"
#include "oscl/bluetooth/mesh/model/key/persist/creator/api.h"
#include "oscl/bluetooth/mesh/transport/lower/tx/api.h"
#include "oscl/bluetooth/mesh/subscription/api.h"
#include "oscl/bluetooth/mesh/subscription/server/api.h"
#include "oscl/bluetooth/mesh/subscription/register/iteration.h"
#include "oscl/bluetooth/mesh/transport/upper/rx/context/api.h"

/** */
namespace Oscl {
/** */
namespace BT {
/** */
namespace Mesh {
/** */
namespace Transport {
/** */
namespace Upper {
/** */
namespace RX {

/** This is an implementation of the Bluetooth
	Mesh Upper Transport layer receiver. It is
	responsible for decrypting incomming upper
	transport layer PDUs.
 */
class Part :
	public Oscl::BT::Mesh::Transport::Upper::RX::Api
	{
	private:
		/** */
		Oscl::BT::Mesh::
		Transport::Upper::
		RX::Context::Api&				_context;

		/** */
		Oscl::BT::Mesh::Key::Api&		_appKeyApi;

	private:
		/** */
		class Replay : public Oscl::QueueItem {
			private:
				/** */
				const uint16_t	_src;

				/** */
				uint32_t		_seq;

				/** */
				const uint32_t	_ivIndex;

			public:
				/** */
				Replay(
					uint16_t	src,
					uint32_t	seq,
					uint32_t	ivIndex
					) noexcept;

				/** */
				bool	match(uint16_t src, uint32_t ivIndex) const noexcept;

				/** */
				bool	isReplay(uint32_t seq) const noexcept;

				/** */
				void	update(uint32_t seq) noexcept;
			};

		/** */
		union ReplayMem {
			/** */
			void*	__qitemlink;
			/** */
			Oscl::Memory::AlignedBlock<sizeof(Replay)>	mem;
			};

		/** FIXME: Could be as many as 0x7FFF unicast addresses! */
		static constexpr unsigned	nReplayMem	= 10;

		/** */
		ReplayMem	replayMem[nReplayMem];

		/** */
		Oscl::Queue<ReplayMem>	_freeReplayMem;

		/** */
		Oscl::Queue<Replay>		_replayList;

	private:	// Iteration variables
		/** */
		uint16_t						_appKeyIndex;

		/** */
		uint8_t*						_plainTextPDU;

		/** */
		unsigned						_plainTextLength;

		/** */
		const void*						_frame;

		/** */
		unsigned						_length;

		/** */
		uint32_t						_ivIndex;

		/** */
		uint32_t						_seq;

		/** */
		uint16_t						_src;

		/** */
		uint16_t						_dst;

		/** */
		uint8_t							_aid;

		/** */
		bool							_akf;

		/** */
		bool							_segmented;

	private:
		/** */
		uint16_t					_findKeyIndex;

	public:
		/** */
		Part(
			Oscl::BT::Mesh::
			Transport::Upper::
			RX::Context::Api&				context,
			Oscl::BT::Mesh::Key::Api&		appKeyApi
			) noexcept;

		/** */
		Oscl::BT::Mesh::Transport::Upper::TX::Api&	getUpperTransportApi() noexcept;

	public: // Oscl::BT::Mesh::Transport::Upper::RX::Api
		/** */
		void	receive(
					const void*	frame,
					unsigned	length,
					uint32_t	ivIndex,
					uint32_t	seq,
					uint16_t	src,
					uint16_t	dst,
					uint8_t		aid,
					bool		akf,
					bool		segmented
					) noexcept;

	private:
		/** RETURN: length of decrypted payload
					or zero if no decryption was
					possible.
		 */
		unsigned	tryAppKeysForAID() noexcept;

		/** */
		unsigned	decrypt(const void*	key) noexcept;

	private: // Oscl::BT::Mesh::Key::Iterator::Composer
		/** RETURN: true to stop iteration. */
		bool	networkKeyIteratorItem(
					unsigned		index,
					const uint8_t	key[16],
					uint8_t			id
					) noexcept;

	private:
		/** Process the encrypted Device Access PDU
		 */
		void	processDeviceAccessPDU(
					const void*	frame,
					unsigned	length,
					uint32_t	ivIndex,
					uint32_t	seq,
					uint16_t	src,
					uint16_t	dst,
					uint8_t		aid,
					bool		akf,
					bool		segmented
					) noexcept;

		/** Process the encrypted Access PDU
		 */
		void	processAccessPDU(
					const void*	frame,
					unsigned	length,
					uint32_t	ivIndex,
					uint32_t	seq,
					uint16_t	src,
					uint16_t	dst,
					uint8_t		aid,
					bool		akf,
					bool		segmented
					) noexcept;

	private:
		/** */
		Replay*	findReplayEntry(
					uint16_t	src,
					uint32_t	ivIndex
					) noexcept;

		/** */
		void	updateReplayProtection(
					uint16_t src,
					uint32_t seq,
					uint32_t ivIndex
					) noexcept;

		/** */
		bool	isReplay(
					uint16_t src,
					uint32_t seq,
					uint32_t ivIndex
					) noexcept;
	};

}
}
}
}
}
}

#endif
