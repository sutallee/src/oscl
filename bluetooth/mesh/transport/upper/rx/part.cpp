/*
   Copyright (C) 2021 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <new>
#include "part.h"
#include "oscl/encoder/be/base.h"
#include "oscl/error/info.h"
#include "oscl/error/fatal.h"
#include "oscl/aes128/ccm.h"
#include "oscl/bluetooth/mesh/address/types.h"
#include "oscl/bluetooth/mesh/address/values.h"
#include "oscl/bluetooth/mesh/model/key/iterator/composer.h"
#include "oscl/bluetooth/mesh/key/iterator/composer.h"
#include "oscl/bluetooth/mesh/model/key/persist/iterator/composer.h"
#include "oscl/bluetooth/mesh/constants.h"

using namespace Oscl::BT::Mesh::Transport::Upper::RX;

//#define DEBUG_TRACE

Part::Part(
	Oscl::BT::Mesh::
	Transport::Upper::
	RX::Context::Api&				context,
	Oscl::BT::Mesh::Key::Api&		appKeyApi
	) noexcept:
		_context(context),
		_appKeyApi(appKeyApi)
		{

	for(unsigned i=0;i<nReplayMem;++i){
		_freeReplayMem.put(&replayMem[i]);
		}
	}

unsigned	Part::tryAppKeysForAID() noexcept{

	Oscl::BT::Mesh::Key::
	Iterator::Composer<Part>
		networkKeyIterator(
			*this,
			&Part::networkKeyIteratorItem
			);

	bool
	found	= _appKeyApi.iterateRX(networkKeyIterator);

	if(found){
		return _plainTextLength;
		}

	return 0;
	}

void	Part::receive(
			const void*	upperTransportAccessPDU,
			unsigned	pduLength,
			uint32_t	ivIndex,
			uint32_t	seq,
			uint16_t	src,
			uint16_t	dst,
			uint8_t		aid,
			bool		akf,
			bool		segmented
			) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);

	Oscl::Error::Info::hexDump(
		upperTransportAccessPDU,
		pduLength
		);
	#endif

	if(isReplay(src,seq,ivIndex)){
		// Drop replays
		// Technically, this is not necessarily a
		// replay, but rather a duplicate.

		#ifdef DEBUG_TRACE
		Oscl::Error::Info::log(
			"%s: replay/duplicate (src:0x%4.4X,seq:0x%6.6X,ivIndex: 0x%8.8X) dropped.\n",
			__PRETTY_FUNCTION__,
			src,
			seq,
			ivIndex
			);
		#endif

		return;
		}

	updateReplayProtection(
		src,
		seq,
		ivIndex
		);

	if(!akf){
		processDeviceAccessPDU(
			upperTransportAccessPDU,
			pduLength,
			ivIndex,
			seq,
			src,
			dst,
			aid,
			akf,
			segmented
			);
		return;
		}

	processAccessPDU(
		upperTransportAccessPDU,
		pduLength,
		ivIndex,
		seq,
		src,
		dst,
		aid,
		akf,
		segmented
		);
	}

void	Part::processDeviceAccessPDU(
			const void*	frame,
			unsigned	length,
			uint32_t	ivIndex,
			uint32_t	seq,
			uint16_t	src,
			uint16_t	dst,
			uint8_t		aid,
			bool		akf,
			bool		segmented
			) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s:\n"
		"\tseq: 0x%8.8X\n"
		"\tsrc: 0x%4.4X\n"
		"\tdst: 0x%4.4X\n"
		"\taid: 0x%2.2X\n"
		"\takf: %s\n"
		"\tsegmented: %s\n"
		"",
		__PRETTY_FUNCTION__,
		seq,
		src,
		dst,
		aid,
		akf?"true":"false",
		segmented?"true":"false"
		);

	Oscl::Error::Info::hexDump(
		frame,
		length
		);
	#endif

	/*	Transport PDU
		[0...]	[ Transport PDU ]		encrypted
		[...]	[ MIC ]
	 */

	constexpr unsigned	maxDecryptedMessageSize	= 512;	// FIXME: need *real* value

	uint8_t	plainTextPDU[maxDecryptedMessageSize];

	const uint8_t*	p	= (const uint8_t*)frame;

	plainTextPDU[0]	= p[0];

	if(length > maxDecryptedMessageSize){
		Oscl::Error::Info::log(
			"%s: Frame too long for buffer.\n",
			OSCL_PRETTY_FUNCTION
			);
		return;
		}

	/* Device nonce
		offset
		[0]		Nonce Type		0x02
		[1]		ASZMIC and pad
			[7]	ASZMIC flag
				0 - unsegmented, indicates SEQ is 
				1 - all other, indicates SEQ is
					least significant 24 bits of SeqAuth
					for segmented messages
		[2:4]	SEQ or SeqAuth(LS 24-bits)
		[5:6]	SRC address
		[7:8]	DST address
		[9:12]	IV Index
	 */

	uint8_t	nonce[13];
		{
		Oscl::Encoder::BE::Base
		beNonceEncoder(
			nonce,
			sizeof(nonce)
			);

		Oscl::Encoder::Api&	nonceEnc	= beNonceEncoder;

		static const uint8_t	nonceType	= 0x02;	// Device Nonce
		uint8_t
		aszmic		= (segmented?0x01:0x00);
		aszmic		<<= 7;	// 7 LSBs are padded to zero.

		nonceEnc.encode(nonceType);
		nonceEnc.encode(aszmic);
		nonceEnc.encode24Bit(seq);
		nonceEnc.encode(src);
		nonceEnc.encode(dst);
		nonceEnc.encode(ivIndex);

		if(nonceEnc.overflow()){
			Oscl::ErrorFatal::logAndExit(
				"%s: WARNING! nonce overflow\n",
				OSCL_PRETTY_FUNCTION
				);
			}
		}

	unsigned	transMicSize	= segmented?transMic64Size:transMic32Size;

	const unsigned	encryptedLength	= length;

	unsigned
	len	= Oscl::AES128::Ccm::decrypt(
			_context.getDeviceKey(),
			nonce,
			0,
			0,
			p,
			encryptedLength,
			transMicSize,
			plainTextPDU
			);

	if(!len){
		Oscl::Error::Info::log(
			"%s: decrypt failed.\n",
			OSCL_PRETTY_FUNCTION
			);
		return;
		}

	_context.processAccessMessage(
				plainTextPDU,
				len,
				src,
				dst,
				0xFFFF	// DevKey "index"
				);
	}

void	Part::processAccessPDU(
			const void*	frame,
			unsigned	length,
			uint32_t	ivIndex,
			uint32_t	seq,
			uint16_t	src,
			uint16_t	dst,
			uint8_t		aid,
			bool		akf,
			bool		segmented
			) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s [%p]:\n"
		"\tseq: 0x%8.8X\n"
		"\tsrc: 0x%4.4X\n"
		"\tdst: 0x%4.4X\n"
		"\taid: 0x%2.2X\n"
		"\takf: %s\n"
		"\tsegmented: %s\n"
		"",
		__PRETTY_FUNCTION__,
		this,
		seq,
		src,
		dst,
		aid,
		akf?"true":"false",
		segmented?"true":"false"
		);

	Oscl::Error::Info::hexDump(
		frame,
		length
		);
	#endif

	/*	Transport PDU
		[0...]	[ Transport PDU ]		encrypted
		[...]	[ MIC ]
	 */

	if(!akf){
		Oscl::Error::Info::log(
			"%s: Not an AppKey.\n",
			OSCL_PRETTY_FUNCTION
			);
		return;
		}

	constexpr unsigned	maxDecryptedMessageSize	= 512;	// FIXME: need *real* value

	uint8_t	plainTextPDU[maxDecryptedMessageSize];

	if(length > maxDecryptedMessageSize){
		Oscl::Error::Info::log(
			"%s: Frame too long for buffer.\n",
			OSCL_PRETTY_FUNCTION
			);
		return;
		}

	// Setup parameters for decryption tries.
	_plainTextPDU	= plainTextPDU;
	_frame			= frame;
	_length			= length;
	_ivIndex		= ivIndex;
	_seq			= seq;
	_src			= src;
	_dst			= dst;
	_aid			= aid;
	_akf			= akf;
	_segmented		= segmented;

	unsigned
	len	= tryAppKeysForAID();

	if(!len){
		// This is "normal" if the containing model
		// does not have this particular key and the
		// message was destined for a different model.
		// Therefore, be quiet about it.
		#ifdef DEBUG_TRACE
		Oscl::Error::Info::log(
			"%s [%p]: no successful decryption for AID 0x%2.2X,"
			"seq: 0x%6.6X,ivIndex: 0x%8.8X,src: 0x%4.4X, dst: 0x%4.4X,segmented: %s"
			"\n",
			OSCL_PRETTY_FUNCTION,
			this,
			_aid,
			seq,
			ivIndex,
			src,
			dst,
			segmented?"true":"false"
			);
		#endif
		return;
		}

	_context.processAccessMessage(
				plainTextPDU,
				len,
				src,
				dst,
				_appKeyIndex
				);
	}

unsigned	Part::decrypt(
			const void*	key
			) noexcept{

	const uint8_t*	p	= (const uint8_t*)_frame;

	_plainTextPDU[0]	= p[0];

	/* Application nonce
		offset
		[0]		Nonce Type		0x01
		[1]		ASZMIC and pad
			[7]	ASZMIC flag
				0 - unsegmented
				1 - all other, indicates SEQ is
					least significant 24 bits of SeqAuth
					for segmented messages
		[2:4]	SEQ or SeqAuth(LS 24-bits)
		[5:6]	SRC address
		[7:8]	DST address
		[9:12]	IV Index
	 */

	uint8_t	nonce[13];
		{
		Oscl::Encoder::BE::Base
		beNonceEncoder(
			nonce,
			sizeof(nonce)
			);

		Oscl::Encoder::Api&	nonceEnc	= beNonceEncoder;

		static const uint8_t	nonceType	= 0x01;	// Application Nonce
		uint8_t
		aszmic		= (_segmented?0x01:0x00);
		aszmic		<<= 7;	// 7 LSBs are padded to zero.

		nonceEnc.encode(nonceType);
		nonceEnc.encode(aszmic);
		nonceEnc.encode24Bit(_seq);
		nonceEnc.encode(_src);
		nonceEnc.encode(_dst);
		nonceEnc.encode(_ivIndex);

		if(nonceEnc.overflow()){
			Oscl::ErrorFatal::logAndExit(
				"%s: WARNING! nonce overflow\n",
				OSCL_PRETTY_FUNCTION
				);
			return 0;
			}
		}

	unsigned	transMicSize	= _segmented?transMic64Size:transMic32Size;

	const unsigned	encryptedLength	= _length;

	bool
	isVirtualAddress	= Oscl::BT::Mesh::Address::isVirtualAddress(_dst);

	if(isVirtualAddress){

		unsigned	len	= 0;

        _context.getSubscriptionIterationApi().iterate(
			[&] (uint16_t address, const void* labelUUID){

				if(address != _dst){
					return false;
					}

				len	= Oscl::AES128::Ccm::decrypt(
						key,
						nonce,
						labelUUID,
						16,
						p,
						encryptedLength,
						transMicSize,
						_plainTextPDU
						);

				if(len){
					return true;
					}

				return false;
				}
			);

		return len;
		}


	// Not a virtual address

	unsigned
	len	= Oscl::AES128::Ccm::decrypt(
			key,
			nonce,
			0,
			0,
			p,
			encryptedLength,
			transMicSize,
			_plainTextPDU
			);

	if(!len){
		Oscl::Error::Info::log(
			"%s: decrypt failed.\n",
			OSCL_PRETTY_FUNCTION
			);
		return 0;
		}

	return len;
	}

bool	Part::networkKeyIteratorItem(
				unsigned		index,
				const uint8_t	key[16],
				uint8_t			id
				) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n"
		"\tindex: 0x%4.4X\n"
		"\tid: 0x%2.2X\n"
		"\t_aid: 0x%2.2X\n"
		"",
		__PRETTY_FUNCTION__,
		index,
		id,
		_aid
		);
	#endif

	if(_aid != id){
		// No match, keep going.
		return false;
		}

	_appKeyIndex	= index;

	/*	It is possible for more than one
		key to match the AID. Therefore,
		we try to decrypt the packet for
		each key that matches the AID.
	 */

	unsigned
	len	= decrypt(key);

	if(len){
		_plainTextLength	= len;
		return true;
		}

	return false;
	}

Part::Replay*	Part::findReplayEntry(uint16_t src, uint32_t ivIndex) noexcept{
	for(
		Replay*
		replay	= _replayList.first();
		replay;
		replay	= _replayList.next(replay)
		){
		if(replay->match(src,ivIndex)){
			return replay;
			}
		}

	return 0;
	}

void	Part::updateReplayProtection(
			uint16_t src,
			uint32_t seq,
			uint32_t ivIndex
			) noexcept{

	Replay*
	replay	= findReplayEntry(src,ivIndex);

	if(replay){
		replay->update(seq);
		return;
		}

	ReplayMem*
	mem	= _freeReplayMem.get();

	if(!mem){
		mem	= (ReplayMem*)_replayList.get();
		}

	if(!mem){
		// This should NEVER happen unless nReplayMem is zero
		}

	replay	= new(mem)
				Replay(
					src,
					seq,
					ivIndex
					);

	_replayList.put(replay);
	}

bool	Part::isReplay(
			uint16_t src,
			uint32_t seq,
			uint32_t ivIndex
			) noexcept{

	for(
		Replay*
		replay	= _replayList.first();
		replay;
		replay	= _replayList.next(replay)
		){
		if(replay->match(src,ivIndex)){
			if(replay->isReplay(seq)){
				return true;
				}
			else {
				replay->update(seq);
				}
			}
		}

	return false;
	}

Part::Replay::Replay(
	uint16_t	src,
	uint32_t	seq,
	uint32_t	ivIndex
	) noexcept:
		_src(src),
		_seq(seq),
		_ivIndex(ivIndex)
		{
	}

bool	Part::Replay::match(uint16_t src,uint32_t ivIndex) const noexcept{

	if(src == _src){
		if(ivIndex == _ivIndex){
			return true;
			}
		}

	return false;
	}

bool	Part::Replay::isReplay(uint32_t seq) const noexcept{
	return (seq <= _seq);
	}

void	Part::Replay::update(uint32_t seq) noexcept{
	if(seq > _seq){
		_seq	= seq;
		}
	}

