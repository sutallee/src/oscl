/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_bluetooth_mesh_transport_upper_tx_app_parth_
#define _oscl_bluetooth_mesh_transport_upper_tx_app_parth_

#include <stdint.h>
#include "oscl/bluetooth/mesh/node/api.h"
#include "oscl/bluetooth/mesh/transport/upper/tx/api.h"
#include "oscl/bluetooth/mesh/transport/lower/tx/api.h"
#include "oscl/done/operation.h"
#include "oscl/memory/block.h"
#include "oscl/pdu/memory/api.h"
#include "oscl/bluetooth/mesh/model/key/api.h"
#include "oscl/bluetooth/mesh/model/key/iterator/composer.h"
#include "oscl/bluetooth/mesh/key/iterator/composer.h"

/** */
namespace Oscl {
/** */
namespace BT {
/** */
namespace Mesh {
/** */
namespace Transport {
/** */
namespace Upper {
/** */
namespace TX {
/** */
namespace App {

/** This part implements the upper-transport tx layer
	for a model that uses the AppKey.
 */
class Part :
	public Oscl::BT::Mesh::Transport::Upper::TX::Api
	{
	public:
		/** */
		class ContextApi {
			public:
				/** */
				virtual void	stopped(Part& part) noexcept=0;
			};

	private:
		/** */
		Oscl::Pdu::Memory::Api&			_freeStoreApi;

		/** */
		Oscl::BT::Mesh::Node::Api&		_nodeApi;

		/** */
		Oscl::BT::Mesh::
		Transport::Lower::TX::Api&		_lowerTransportApi;

		/** */
		Oscl::BT::Mesh::Model::Key::Api&	_keyApi;

		/** */
		Oscl::BT::Mesh::Key::Api&			_networkKeyApi;

		/** */
		const uint16_t					_elementAddress;

	private:
		/** */
		Oscl::BT::Mesh::Model::
		Key::Iterator::Composer<Part>	_modelKeyIterator;

		/** */
		Oscl::BT::Mesh::Key::
		Iterator::Composer<Part>		_networkKeyIterator;

	private:
		/** */
		uint16_t						_appKeyIndex;

		/** */
		uint8_t							_aid;

		/** */
		const void*						_appKey;

	private:
		/** */
		bool								_stopping;

	public:
		/**	Constructor
			param [in] lowerTransportApi	Reference to the lower-transport
											layer interface.
			param [in] elementAddress		The unicast address of the
											containing element.
		 */
		Part(
			Oscl::Pdu::Memory::Api&				freeStoreApi,
			Oscl::BT::Mesh::Node::Api&			nodeApi,
			Oscl::BT::Mesh::
			Transport::Lower::TX::Api&			lowerTransportApi,
			Oscl::BT::Mesh::Model::Key::Api&	keyApi,
			Oscl::BT::Mesh::Key::Api&			networkKeyApi,
			uint16_t							elementAddress
			) noexcept;

		/** */
		void	start() noexcept;

		/** */
		void	stop() noexcept;

	private:
		/** */
		const void*	findAppKey(uint16_t appKeyIndex) noexcept;

	private:	// Oscl::BT::Mesh::Transport::Upper::TX::Api
		/*
			This operation transmits the access PDU using
			the Model's publish address for the destination
			address, if any. Otherwise, nothing is transmitted.

			The PDU will always be sent some number of times
			without expecting an acknowlegement.

			Based on the type of Model, either the AppKey or DeviceKey
			is used to encrypt this PDU.

			param [in] completeApi				Optional completion callback.
			param [in] accessPDU				The unencrypted access layer PDU.
			param [in] ttl						The desired TTL
		 */
		void	publishSegmented(
					Oscl::BT::Mesh::Transport::
					TX::Complete::Api*			completeApi,
					const void*					accessPDU,
					unsigned					length,
					uint16_t					appKeyIndex,
					uint8_t						ttl
					) noexcept;

		/*
			This operation transmits the access PDU using
			the Model's publish address for the destination
			address, if any. Otherwise, nothing is transmitted.

			The PDU will always be sent some number of times
			without expecting an acknowlegement.

			Based on the type of Model, either the AppKey or DeviceKey
			is used to encrypt this PDU.

			param [in] completeApi				Optional completion callback.
			param [in] accessPDU				The unencrypted access layer PDU.
			param [in] ttl						The desired TTL
		 */
		void	publishUnsegmented(
					Oscl::BT::Mesh::Transport::
					TX::Complete::Api*			completeApi,
					const void*					accessPDU,
					unsigned					length,
					uint16_t					appKeyIndex,
					uint8_t						ttl
					) noexcept;

		/*
			This operation transmits the access PDU using
			the specified destination address.

			If the destination address is unicast address,
			then the an acknowledgemen will be expected.
			Otherwise, the PDU will be sent some number of times.

			Based on the type of Model, either the AppKey or DeviceKey
			is used to encrypt this PDU.

			param [in] completeApi				Optional completion callback.
			param [in] accessPDU				The unencrypted access layer PDU.
			param [in] dst						The destination address to which
												this PDU is sent.
			param [in] appKeyIndex				The index of the AppKey to use.
			param [in] ttl						The desired TTL
		 */
		void	sendSegmented(
					Oscl::BT::Mesh::Transport::
					TX::Complete::Api*			completeApi,
					const void*					accessPDU,
					unsigned					length,
					uint16_t					dst,
					uint16_t					appKeyIndex,
					uint8_t						ttl,
					const void*					authData,
					unsigned					authDataLen
					) noexcept;

		/*
			This operation transmits the access PDU using
			the specified destination address.

			If the destination address is unicast address,
			then the an acknowledgemen will be expected.
			Otherwise, the PDU will be sent some number of times.

			Based on the type of Model, either the AppKey or DeviceKey
			is used to encrypt this PDU.

			param [in] completeApi				Optional completion callback.
			param [in] accessPDU				The unencrypted access layer PDU.
			param [in] dst						The destination address to which
												this PDU is sent.
			param [in] appKeyIndex				The index of the AppKey to use.
			param [in] ttl						The desired TTL
		 */
		void	sendUnsegmented(
					Oscl::BT::Mesh::Transport::
					TX::Complete::Api*			completeApi,
					const void*					accessPDU,
					unsigned					length,
					uint16_t					dst,
					uint16_t					appKeyIndex,
					uint8_t						ttl,
					const void*					authData,
					unsigned					authDataLen
					) noexcept;

	private:
		/** RETURN: zero for failure.
		 */
		Oscl::Pdu::Pdu*	encrypt(
			const void*	clearText,
			unsigned	length,
			uint32_t	seq,
			uint16_t	dst,
			uint16_t	appKeyIndex,
			bool		segmented,
			const void*	authData	= 0,
			unsigned	authDataLen	= 0
			) noexcept;

	public:
		/** */
		bool	receiveControl(
					uint16_t	srcAddress,
					uint16_t	dstAddress,
					uint32_t	seq,
					const void*	frame,
					unsigned	length
					) noexcept;

	private: // Oscl::BT::Mesh::Model::Key::Iterator::Composer	_modelKeyIterator;
		/** RETURN: true to stop iteration. */
		bool	modelKeyIteratorItem(
					unsigned		index
					) noexcept;

	private: // Oscl::BT::Mesh::Key::Iterator::Composer	_networkKeyIterator;
		/** RETURN: true to stop iteration. */
		bool	networkKeyIteratorItem(
					unsigned		index,
					const uint8_t	key[16],
					uint8_t			id
					) noexcept;
	};

}
}
}
}
}
}
}


#endif
