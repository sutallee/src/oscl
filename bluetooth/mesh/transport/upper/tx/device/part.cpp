/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <new>
#include "part.h"
#include "oscl/error/info.h"
#include "oscl/error/fatal.h"
#include "oscl/encoder/be/base.h"
#include "oscl/aes128/ccm.h"
#include "oscl/bluetooth/mesh/constants.h"

using namespace Oscl::BT::Mesh::Transport::Upper::TX::Device;

//#define DEBUG_TRACE

Part::Part(
	Oscl::Pdu::Memory::Api&		freeStoreApi,
	Oscl::BT::Mesh::Node::Api&	nodeApi,
	Oscl::BT::Mesh::
	Transport::Lower::TX::Api&	lowerTransportApi,
	uint16_t					elementAddress
	) noexcept:
		_freeStoreApi(freeStoreApi),
		_nodeApi(nodeApi),
		_lowerTransportApi(lowerTransportApi),
		_elementAddress(elementAddress),
		_stopping(false)
		{
	}

void	Part::start() noexcept{
	Oscl::Error::Info::log(
		"%s: NOT IMPLEMENTED!\n",
		__PRETTY_FUNCTION__
		);
	}

void	Part::stop() noexcept{
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	_stopping	= true;
	}

void	Part::publishSegmented(
			Oscl::BT::Mesh::Transport::
			TX::Complete::Api*			completeApi,
			const void*					accessPDU,
			unsigned					length,
			uint16_t					appKeyIndex,
			uint8_t						ttl
			) noexcept{
	Oscl::Error::Info::log(
		"%s: NOT IMPLEMENTED!\n",
		__PRETTY_FUNCTION__
		);
	}

void	Part::publishUnsegmented(
			Oscl::BT::Mesh::Transport::
			TX::Complete::Api*			completeApi,
			const void*					accessPDU,
			unsigned					length,
			uint16_t					appKeyIndex,
			uint8_t						ttl
			) noexcept{
	Oscl::Error::Info::log(
		"%s: NOT IMPLEMENTED!\n",
		__PRETTY_FUNCTION__
		);
	}

void	Part::sendSegmented(
			Oscl::BT::Mesh::Transport::
			TX::Complete::Api*			completeApi,
			const void*					accessPDU,
			unsigned					length,
			uint16_t					dst,
			uint16_t					appKeyIndex,
			uint8_t						ttl,
			const void*					authData,
			unsigned					authDataLen
			) noexcept{
	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s!\n",
		__PRETTY_FUNCTION__
		);
	#endif

	//========== un-comment for loopback testing
//	dst	= _elementAddress;
	//===========================================

	uint32_t
	seq	= _nodeApi.allocateSequenceNumber();

	Oscl::Handle<Oscl::Pdu::Pdu>
	pdu	= encrypt(
			accessPDU,
			length,
			seq,
			dst,
			true	// segmented
			);

	if(!pdu){
		Oscl::Error::Info::log(
			"%s: Can't encrypt accessPDU. Out of resources.\n",
			OSCL_PRETTY_FUNCTION
			);
		if(completeApi){
			completeApi->failedOutOfResources();
			}
		return;
		}

	_lowerTransportApi.transmitSegmentedAccess(
		completeApi,
		pdu,
		_nodeApi.getTxIvIndex(),
		seq,
		dst,
		0x00,	// akfAID (DeviceKey)
		true,	// szmic (segmented)
		ttl
		);
	}

void	Part::sendUnsegmented(
			Oscl::BT::Mesh::Transport::
			TX::Complete::Api*			completeApi,
			const void*					accessPDU,
			unsigned					length,
			uint16_t					dst,
			uint16_t					appKeyIndex,
			uint8_t						ttl,
			const void*					authData,
			unsigned					authDataLen
			) noexcept{
	Oscl::Error::Info::log(
		"%s: NOT IMPLEMENTED!\n",
		__PRETTY_FUNCTION__
		);
	}

Oscl::Pdu::Pdu*	Part::encrypt(
	const void*	clearText,
	unsigned	length,
	uint32_t	seq,
	uint16_t	dst,
	bool		segmented
	) noexcept{

	unsigned	transMicSize	= segmented?transMic64Size:transMic32Size;

	unsigned	bufferSize;

	void*
	mem	= _freeStoreApi.allocBuffer(bufferSize);

	if(!mem){
		Oscl::Error::Info::log(
			"%s: out of buffer memory.\n",
			OSCL_PRETTY_FUNCTION
			);
		return 0;
		}

	if(bufferSize < (length + transMicSize)){
		_freeStoreApi.freeBuffer(mem);
		Oscl::Error::Info::log(
			"%s: length+transMicSize (%u) too long for buffer (%u)!\n",
			OSCL_PRETTY_FUNCTION,
			length+transMicSize,
			bufferSize
			);
		return 0;
		}

	/* Device nonce
		offset
		[0]		Nonce Type		0x02
		[1]		ASZMIC and pad
			[7]	ASZMIC flag
				0 - unsegmented, indicates SEQ
				1 - all other, indicates SEQ is
					least significant 24 bits of SeqAuth
					for segmented messages
		[2:4]	SEQ or SeqAuth(LS 24-bits)
		[5:6]	SRC address
		[7:8]	DST address
		[9:12]	IV Index
	 */

	uint8_t	nonce[13];
		{
		Oscl::Encoder::BE::Base	beNonceEncoder(nonce,sizeof(nonce));
		Oscl::Encoder::Api&	nonceEnc	= beNonceEncoder;

		static const uint8_t	nonceType	= 0x02;	// Device Nonce
		uint8_t
		aszmic		= (segmented?0x01:0x00);
		aszmic		<<= 7;	// 7 LSBs are padded to zero.

		nonceEnc.encode(nonceType);
		nonceEnc.encode(aszmic);
		nonceEnc.encode24Bit(seq);
		nonceEnc.encode(_elementAddress);
		nonceEnc.encode(dst);
		nonceEnc.encode(_nodeApi.getTxIvIndex());

		if(nonceEnc.overflow()){
			Oscl::ErrorFatal::logAndExit(
				"%s: WARNING! nonce overflow\n",
				OSCL_PRETTY_FUNCTION
				);
			}
		}

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s: Device Nonce\n",
		__PRETTY_FUNCTION__
		);
	Oscl::Error::Info::hexDump(
		nonce,
		sizeof(nonce)
		);
	#endif

	uint8_t*	cipherText	= (uint8_t*)mem;

	unsigned
	len	= Oscl::AES128::Ccm::encrypt(
				_nodeApi.getDeviceKey(),
				nonce,
				0,	// adata
				0,	// alen
				clearText,
				length,
				transMicSize,
				cipherText
				);

	if(!len){
		_freeStoreApi.freeBuffer(mem);
		Oscl::Error::Info::log(
			"%s: encrypt failed!\n",
			OSCL_PRETTY_FUNCTION
			);
		return 0;
		}

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s: Encypted Upper Transport PDU\n",
		__PRETTY_FUNCTION__
		);
	Oscl::Error::Info::hexDump(
		mem,
		len
		);
	#endif

	Oscl::Pdu::Pdu*
	fixed	= _freeStoreApi.allocFixed(
				mem,
				len
				);

	if(!fixed){
		_freeStoreApi.freeBuffer(mem);
		Oscl::Error::Info::log(
			"%s: out of fixed memory.\n",
			OSCL_PRETTY_FUNCTION
			);
		}

	return fixed;
	}

	// There are two types of Lower Transport Layers
	// distinguished by the CTL field of the Network PDU:
	//
	// 1. Lower Transport Access Layer
	// 2. Lower Transport Control
	//
	// Access Layer messages are forwarded to
	// an Element containing the Lower Transport Layer?
	//
	// The first octet of a Lower Transport Access message contains:
	//   [7]	SEG (segmented or unsegmented)
	//   [6]	AKF (application/device key)
	//   [5:0]	AID (application or device key 0b000000)
	//
	// The first octet of a a Lower Transport message contains:
	//	[7]		SEG (segmented or unsegmented)
	//	[6:0]	Opcode
	//		0b0000000	- Segment Acknowledge (Unsegmented only)
	//		0b0000001	- Friend Poll
	//		0b0000010	- Friend Update
	//		0b0000011	- Friend Request (LPN TX: dest: <all-friends-group-address>, src:?)
	//		0b0000100	- Friend Offer
	//		0b0000101	- Friend Clear
	//		0b0000110	- Friend Clear Confirm
	//		0b0000111	- Friend Subscription List Add
	//		0b0001000	- Friend Subscription List Remove
	//		0b0001001	- Friend Subscription List Confirm
	//		0b0001010	- Heartbeat
	//
	//	All transport control messages originated by a Friend node shall be sent as
	//	Unsegmented Control messages with the SRC field set to the unicast address
	//	of the primary element of the Friend node.
	//
	//	All transport control messages originated by a Low Power node shall be sent as
	//	Unsegmented Control messages with the SRC field set to the unicast address
	//	of the primary element of the node that supports the Low Power feature.
	//

	//	Segment Acknowledgement message:
	//	[0]
	//		[7]		SEG	(0)
	//		[6:0]	0b0000000
	//	[1]
	//		[7]		OBO
	//			0	Direct
	//			1	Friend On Behalf Of LPN (Friend queued message for LPN)
	//		[6:0]	SeqZero MSBs
	//	[2]
	//		[7:2]	SeqZero LSBs
	//		[1:0]	RFU (tx as 0b00)
	//	[3:6]	BlockAck, 32-bit block mask (BE)
	//		[n]		Segment n ACK, Range 0 <= n <= 31

	//	Segmented Access Message:
	//	[0]
	//		[7]		SEG	(1)
	//		[6]		AKF
	//		[5:0]	AID
	//	[1]
	//		[7]		SZMIC, Size of TransMIC (0:32-bit, 1:64-bit)
	//		[6:0]	SeqZero MSBs
	//	[2]
	//		[7:2]	SeqZero LSBs
	//		[1:0]	SegO MSBs
	//	[3]
	//		[7:5]	SegO LSBs
	//		[4:0]	SegN
	//	[4:n]	Range: 8:96 bits, 1:12 octets
	//		[7:5]	Segment m
	//
	//	Every Segmented Access message for the same Upper Transport Access PDU
	//	shall have the same values for AKF, AID, SZMIC, SeqZero, and SegN.


	//	Segmented Control Message:
	//	[0]
	//		[7]		SEG	(1)
	//		[6:0]	Opcode, Rang 0x01:0x7F, (0x00 (0b0000000) is reserved and ignored on RX)
	//	[1]
	//		[7]		RFU (tx as 0) No encrypted Payload or MIC in Control PDU
	//		[6:0]	SeqZero MSBs
	//	[2]
	//		[7:2]	SeqZero LSBs
	//		[1:0]	SegO MSBs
	//	[3]
	//		[7:5]	SegO LSBs
	//		[4:0]	SegN
	//	[4:n]	Range: 8:64 bits, 1:8 octets
	//		[7:5]	Segment m
	//
	//	Every Segmented Control message for the same Upper Transport Control PDU
	//	shall have the same values for Opcode, SeqZero, and SegN.

	//	The lower transport layer shall only transmit Segmented Access messages
	//	or Segmented Control messages for a single Upper Transport PDU to the
	//	same *destination* at a time.
	//
	//	Hmmm... this implies that there is a special "layer" between the
	//	Element (src address) and the Network layer that has a queue for
	//	each destination address.
	//
	//	Does this rule apply across sub-networks?
	//

	//	Non-Unicast Destination Addresses
	//
	//	How are broadcast/group/virtual addresses treated? Can those even be
	//	segmented?
	//
	//	3.5.3.3 Segmentation behavior
	//
	//		Once an Upper Transport PDU has been segmented, the lower transport layer
	//		will transmit the initial Lower Transport PDUs of each segment of that message.
	//
	//	*	If the message is destined to a unicast address, then the lower transport layer
	//		will expect a Segment Acknowledgment message from that node, or from a Friend
	//		node on behalf of that node.
	//
	//		If the message is addressed to a virtual or group address, then Segment Acknowledgment
	//		messages will not be sent by those devices.
	//
	//	*	If the Lower Transport PDUs of the segmented Upper Transport PDU are being sent
	//		to a group address or a virtual address, then the lower transport layer shall
	//		send all Lower Transport PDUs of the segmented Upper Transport PDU.
	//
	//		It is recommended to send all Lower Transport PDUs multiple times, introducing
	//		small random delays between repetitions.
	//
	//			Note:The Upper Transport PDU sent to a group or virtual address is not
	//			acknowledged by recipients, thus the message delivery status is unknown
	//			and should be considered to be unacknowledged. The behavior recommended
	//			above is designed to significantly increase the probability of the successful
	//			delivery of the segmented Upper Transport PDU.

	//	A message sent to the all-proxies address shall be processed by the primary element
	//	of all nodes that have the proxy functionality enabled.
	//
	//	A message sent to the all-friends address shall be processed by the primary element
	//	of all nodes that have the friend functionality enabled.
	//
	//	A message sent to the all-relays address shall be processed by the primary element
	//	of all nodes that have the relay functionality enabled.
	//
	//	A message sent to the all-nodes address shall be processed by the primary element
	//	of all nodes.

bool	Part::receiveControl(
			uint16_t	srcAddress,
			uint16_t	dstAddress,
			uint32_t	seq,
			const void*	frame,
			unsigned	length
			) noexcept{
	return false;
	}

