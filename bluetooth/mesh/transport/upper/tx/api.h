/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_bluetooth_mesh_transport_upper_tx_apih_
#define _oscl_bluetooth_mesh_transport_upper_tx_apih_

#include <stdint.h>
#include "oscl/bluetooth/mesh/transport/tx/complete/api.h"

/** */
namespace Oscl {
/** */
namespace BT {
/** */
namespace Mesh {
/** */
namespace Transport {
/** */
namespace Upper {
/** */
namespace TX {

/** This interface is used by the access layer to
	transmit Access PDUs. This upper transport layer
	is reponsible for encrypting the Access PDU.
 */
class Api {
	public:
		/*
			This operation transmits the access PDU using
			the Model's publish address for the destination
			address, if any. Otherwise, nothing is transmitted.

			The PDU will always be sent some number of times
			without expecting an acknowlegement.

			Based on the type of Model, either the AppKey or DeviceKey
			is used to encrypt this PDU.

			param [in] completeApi				Optional completion callback.
			param [in] accessPDU				The unencrypted access layer PDU.
			param [in] length					The length of the PDU.
			param [in] ttl						The desired TTL
		 */
		virtual void	publishSegmented(
							Oscl::BT::Mesh::Transport::
							TX::Complete::Api*			completeApi,
							const void*					accessPDU,
							unsigned					length,
							uint16_t					appKeyIndex,
							uint8_t						ttl	= 0xFF
							) noexcept=0;

		/*
			This operation transmits the access PDU using
			the Model's publish address for the destination
			address, if any. Otherwise, nothing is transmitted.

			The PDU will always be sent some number of times
			without expecting an acknowlegement.

			Based on the type of Model, either the AppKey or DeviceKey
			is used to encrypt this PDU.

			param [in] completeApi				Optional completion callback.
			param [in] accessPDU				The unencrypted access layer PDU.
			param [in] length					The length of the PDU.
			param [in] ttl						The desired TTL
		 */
		virtual void	publishUnsegmented(
							Oscl::BT::Mesh::Transport::
							TX::Complete::Api*			completeApi,
							const void*					accessPDU,
							unsigned					length,
							uint16_t					appKeyIndex,
							uint8_t						ttl	= 0xFF
							) noexcept=0;

		/*
			This operation transmits the access PDU using
			the specified destination address.

			If the destination address is unicast address,
			then the an acknowledgemen will be expected.
			Otherwise, the PDU will be sent some number of times.

			Based on the type of Model, either the AppKey or DeviceKey
			is used to encrypt this PDU.

			param [in] completeApi				Optional completion callback.
			param [in] accessPDU				The unencrypted access layer PDU.
			param [in] length					The length of the PDU.
			param [in] dst						The destination address to which
												this PDU is sent.
			param [in] appKeyIndex				The index of the AppKey to use.
			param [in] ttl						The desired TTL
			param [in] authData					The authentication data used for
												virtual dst addresses.
			param [in] authDataLen				The number of octets in the authData
												used for virtual dst addresses.
		 */
		virtual void	sendSegmented(
							Oscl::BT::Mesh::Transport::
							TX::Complete::Api*			completeApi,
							const void*					accessPDU,
							unsigned					length,
							uint16_t					dst,
							uint16_t					appKeyIndex,
							uint8_t						ttl	= 0xFF,
							const void*					authData	= 0,
							unsigned					authDataLen	= 0
							) noexcept=0;

		/*
			This operation transmits the access PDU using
			the specified destination address.

			If the destination address is unicast address,
			then the an acknowledgemen will be expected.
			Otherwise, the PDU will be sent some number of times.

			Based on the type of Model, either the AppKey or DeviceKey
			is used to encrypt this PDU.

			param [in] completeApi				Optional completion callback.
			param [in] accessPDU				The unencrypted access layer PDU.
			param [in] length					The length of the PDU.
			param [in] dst						The destination address to which
												this PDU is sent.
			param [in] appKeyIndex				The index of the AppKey to use.
			param [in] ttl						The desired TTL
			param [in] authData					The authentication data used for
												virtual dst addresses.
			param [in] authDataLen				The number of octets in the authData
												used for virtual dst addresses.
		 */
		virtual void	sendUnsegmented(
							Oscl::BT::Mesh::Transport::
							TX::Complete::Api*			completeApi,
							const void*					accessPDU,
							unsigned					length,
							uint16_t					dst,
							uint16_t					appKeyIndex,
							uint8_t						ttl	= 0xFF,
							const void*					authData	= 0,
							unsigned					authDataLen	= 0
							) noexcept=0;
	};

}
}
}
}
}
}


#endif
