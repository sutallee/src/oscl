/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_bluetooth_mesh_transport_lower_reassemble_control_reaperh_
#define _oscl_bluetooth_mesh_transport_lower_reassemble_control_reaperh_

#include <stdint.h>
#include "oscl/bluetooth/mesh/element/symbiont/kernel/api.h"
#include "oscl/bluetooth/mesh/transport/tx/complete/api.h"
#include "oscl/bluetooth/mesh/model/rx/itemcomp.h"
#include "oscl/queue/queue.h"
#include "oscl/memory/block.h"
#include "oscl/done/operation.h"
#include "oscl/pdu/memory/api.h"
#include "oscl/bluetooth/mesh/transport/upper/control/rx/api.h"
#include "oscl/timer/api.h"

/** */
namespace Oscl {
/** */
namespace BT {
/** */
namespace Mesh {
/** */
namespace Transport {
/** */
namespace Lower {
/** */
namespace Reassemble {
/** */
namespace Control {

/** */
class Reaper: public Oscl::QueueItem {
	public:
		/** */
		class ContextApi {
			public:
				/**  */
				virtual void	stopped(Reaper& part) noexcept=0;
			};

	private:
		/** */
		ContextApi&						_context;

		/** */
		Oscl::Pdu::Memory::Api&			_freeStoreApi;

		/** */
		Oscl::Timer::Api&				_ackTimerApi;

		/** */
		Oscl::Timer::Api&				_incompleteTimerApi;

		/** */
		Oscl::BT::Mesh::Element::
		Symbiont::Kernel::Api&			_kernelApi;

		/** */
		Oscl::BT::Mesh::
		Transport::Upper::
		Control::RX::Api&				_upperTransportApi;

		/** */
		const uint32_t					_ivIndex;

		/** */
		uint32_t						_seq;

		/** */
		const uint16_t					_src;

		/** */
		const uint16_t					_dst;

		/** */
		const uint8_t					_opcode;

		/** */
		const uint8_t					_ttl;

	private:
		/** */
		Oscl::Done::Operation<Reaper>	_ackTimerExpired;

		/** */
		Oscl::Done::Operation<Reaper>	_incompleteTimerExpired;

	private:
		/** */
		constexpr static unsigned	maxTransportPduSize = 380+4;

		/** */
		unsigned							_lastSegmentLength;

		/** */
		uint8_t								_buffer[maxTransportPduSize];

		/** */
		bool								_szmic;

		/** */
		uint32_t							_blockAck;

		/** */
		uint16_t							_seqZero;

		/** */
		bool								_sendACK;

		/** */
		bool								_obo;

		/** */
		bool								_ackTimerIsActive;

		/** */
		bool								_stopping;

	public:
		/**	Constructor
			param [in] kernelApi		Reference to the subnet
										interface used to transmit
										transport PDUs.
			param [in] ivIndex			The IV Index used to encrypt
										the upper transport PDU.
		 */
		Reaper(
			ContextApi&					context,
			Oscl::Pdu::Memory::Api&		freeStoreApi,
			Oscl::Timer::Api&			ackTimerApi,
			Oscl::Timer::Api&			incompleteTimerApi,
			Oscl::BT::Mesh::Element::
			Symbiont::Kernel::Api&		kernelApi,
			Oscl::BT::Mesh::
			Transport::Upper::
			Control::RX::Api&			upperTransportApi,
			uint32_t					ivIndex,
			uint32_t					seq,
			uint16_t					src,
			uint16_t					dst,
			uint8_t						ttl,
			uint8_t						opcode,
			bool						obo			= false,
			bool						doNotACK	= false
			) noexcept;

		/** */
		void	start() noexcept;

		/** */
		void	stop() noexcept;

	private:
		/** */
		void	stopNext() noexcept;

		/** */
		bool	allMemoryIsFree() noexcept;

	private:
		/** */
		void	ackTimerExpired() noexcept;

		/** */
		void	incompleteTimerExpired() noexcept;

	private:
		/** */
		void	transmitACK(uint16_t seqZero) noexcept;

	public:
		/**	This is how we receive access segments.
		 */
		bool	receive(
					uint16_t	srcAddress,
					uint16_t	dstAddress,
					uint32_t	seq,
					const void*	lowerTransportPDU,
					unsigned	length
					) noexcept;

		/**	This is how we receive control segments.
		 */
		bool	receiveControl(
					uint16_t	srcAddress,
					uint16_t	dstAddress,
					uint32_t	seq,
					const void*	lowerTransportPDU,
					unsigned	length
					) noexcept;
	};

}
}
}
}
}
}
}


#endif
