/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <new>
#include <string.h>
#include "reaper.h"
#include "oscl/endian/decoder/linear/part.h"
#include "oscl/encoder/be/base.h"
#include "oscl/error/info.h"
#include "oscl/bluetooth/mesh/address/types.h"

using namespace Oscl::BT::Mesh::Transport::Lower::Reassemble::Access;

//#define DEBUG_TRACE
#define DEBUG_TRACE_COMPLETE

Reaper::Reaper(
	ContextApi&					context,
	Oscl::Pdu::Memory::Api&		freeStoreApi,
	Oscl::Timer::Api&			ackTimerApi,
	Oscl::Timer::Api&			incompleteTimerApi,
	Oscl::BT::Mesh::Element::
	Symbiont::Kernel::Api&		kernelApi,
	Oscl::BT::Mesh::
	Transport::Upper::RX::Api&	upperTransportApi,
	uint32_t					ivIndex,
	uint32_t					seq,
	uint16_t					src,
	uint16_t					dst,
	uint8_t						ttl,
	uint8_t						aid,
	bool						akf,
	bool						noAck,
	bool						obo
	) noexcept:
		_context(context),
		_freeStoreApi(freeStoreApi),
		_ackTimerApi(ackTimerApi),
		_incompleteTimerApi(incompleteTimerApi),
		_kernelApi(kernelApi),
		_upperTransportApi(upperTransportApi),
		_ivIndex(ivIndex),
		_seq(seq),
		_src(src),
		_dst(dst),
		_opcode(
			aid | ((akf?1:0)<<6)
			),
		_ttl(ttl),
		_aid(aid),
		_akf(akf),
		_ackTimerExpired(
			*this,
			&Reaper::ackTimerExpired
			),
		_incompleteTimerExpired(
			*this,
			&Reaper::incompleteTimerExpired
			),
		_stopTimerExpired(
			*this,
			&Reaper::stopTimerExpired
			),
		_lastSegmentLength(0),
		_szmic(false),
		_blockAck(0),
		_seqZero(0xFFFF),
		_segN(0),
		_sendACK(
				Oscl::BT::Mesh::Address::isUnicastAddress(dst)
			&&	!noAck
			),
		_obo(obo),
		_ackTimerIsActive(false),
		_fullyReceived(false),
		_stopping(false)
		{
	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n"
		"\t_ivIndex: 0x%8.8X\n"
		"\t_seq: 0x%6.6X\n"
		"\t_src: 0x%4.4X\n"
		"\t_dst: 0x%4.4X\n"
		"\t_aid: 0x%2.2X\n"
		"\takf: %s\n"
		"\t_opcode: 0x%4.4X\n"
		"",
		__PRETTY_FUNCTION__,
		_ivIndex,
		_seq,
		_src,
		_dst,
		aid,
		akf?"true":"false",
		_opcode
		);
	#endif

	_ackTimerApi.setExpirationCallback(
		_ackTimerExpired
		);
	_incompleteTimerApi.setExpirationCallback(
		_incompleteTimerExpired
		);
	}

void	Reaper::start() noexcept{
	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif

	_incompleteTimerApi.start(10000);
	}

void	Reaper::stop() noexcept{
	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif

	if(_stopping){
		return;
		}

	_ackTimerApi.stop();

	_incompleteTimerApi.stop();

	_stopping	= true;

	stopNext();
	}

void	Reaper::stopNext() noexcept{
	if(!_stopping){
		return;
		}

	_stopping	= false;

	_context.stopped(*this);
	}

void	Reaper::ackTimerExpired() noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"***************** %s\n",
		__PRETTY_FUNCTION__
		);
	#endif

	_ackTimerIsActive	= false;

	transmitACK(_seqZero);
	}

void	Reaper::incompleteTimerExpired() noexcept{

	#if defined(DEBUG_TRACE) || defined(DEBUG_TRACE_COMPLETE)
	Oscl::Error::Info::log(
		"***************** %s: _blockAck: 0x%8.8X, _segN: %u\n",
		__PRETTY_FUNCTION__,
		_blockAck,
		_segN
		);
	#endif

	stop();
	}

void	Reaper::stopTimerExpired() noexcept{

	#if defined(DEBUG_TRACE)
	Oscl::Error::Info::log(
		"***************** %s\n",
		__PRETTY_FUNCTION__
		);
	#endif

	stop();
	}

void	Reaper::transmitACK(uint16_t seqZero) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s: seqZero: 0x%4.4X\n",
		__PRETTY_FUNCTION__,
		seqZero
		);
	#endif

	if(!_sendACK){
		// We only send ACKs for unicast
		// dst addresses.
		return;
		}

	//	Segment Acknowledgement message:
	//	[0]
	//		[7]		SEG	(0)
	//		[6:0]	0b0000000
	//	[1]
	//		[7]		OBO
	//			0	Direct
	//			1	Friend On Behalf Of LPN (Friend queued message for LPN)
	//		[6:0]	SeqZero MSBs
	//	[2]
	//		[7:2]	SeqZero LSBs
	//		[1:0]	RFU (tx as 0b00)
	//	[3:6]	BlockAck, 32-bit block mask (BE)
	//		[n]		Segment n ACK, Range 0 <= n <= 31

	unsigned	bufferSize;

	void*
	mem	= _freeStoreApi.allocBuffer(bufferSize);

	if(!mem){
		Oscl::Error::Info::log(
			"%s: out of memory\n",
			OSCL_PRETTY_FUNCTION
			);
		return;
		}

	Oscl::Encoder::BE::Base
	beEncoder(
		mem,
		bufferSize
		);

	Oscl::Encoder::Api&	encoder	= beEncoder;

	static const uint8_t	opcode	= 0x00;

	uint8_t
	oboSeqZero	= 
			((_obo?0x01:0x00)<<7)
		|	(seqZero >> 6)
		;

	uint8_t
	seqZeroRFU	= (seqZero & 0x3F) << 2;

	encoder.encode(_src);
	encoder.encode(opcode);
	encoder.encode(oboSeqZero);
	encoder.encode(seqZeroRFU);
	encoder.encode(_blockAck);

	Oscl::Handle<Oscl::Pdu::Pdu>
	fixed	= _freeStoreApi.allocFixed(
				mem,
				encoder.length()
				);
	if(!fixed){
		_freeStoreApi.freeBuffer(mem);
		Oscl::Error::Info::log(
			"%s: out of memory\n",
			OSCL_PRETTY_FUNCTION
			);
		return;
		}

	uint32_t
	seq	= _kernelApi.allocSequenceNumber();

	/*	3.5.2.3.1 Segment Acknowledgment message
		If the received segments were sent with TTL set to 0,
		it is recommended that the corresponding Segment
		Acknowledgment message is sent with TTL set to 0.
	 */
	uint8_t	ttl	= (!_ttl)?0x00:0xFF;

	_kernelApi.transmitControl(
		fixed,
		_ivIndex,	// ivIndex
		seq,	// seq
		_dst,	// src - for an ACK, the src
				// is the original dst address.
		_src,	// dst
		ttl		// ttl
		);
	}

	// There are two types of Lower Transport Layers
	// distinguished by the CTL field of the Network PDU:
	//
	// 1. Lower Transport Access Layer
	// 2. Lower Transport Control
	//
	// Access Layer messages are forwarded to
	// an Element containing the Lower Transport Layer?
	//
	// The first octet of a Lower Transport Access message contains:
	//   [7]	SEG (segmented or unsegmented)
	//   [6]	AKF (application/device key)
	//   [5:0]	AID (application or device key 0b000000)
	//
	// The first octet of a a Lower Transport message contains:
	//	[7]		SEG (segmented or unsegmented)
	//	[6:0]	Opcode
	//		0b0000000	- Segment Acknowledge (Unsegmented only)
	//		0b0000001	- Friend Poll
	//		0b0000010	- Friend Update
	//		0b0000011	- Friend Request (LPN TX: dest: <all-friends-group-address>, src:?)
	//		0b0000100	- Friend Offer
	//		0b0000101	- Friend Clear
	//		0b0000110	- Friend Clear Confirm
	//		0b0000111	- Friend Subscription List Add
	//		0b0001000	- Friend Subscription List Remove
	//		0b0001001	- Friend Subscription List Confirm
	//		0b0001010	- Heartbeat
	//
	//	All transport control messages originated by a Friend node shall be sent as
	//	Unsegmented Control messages with the SRC field set to the unicast address
	//	of the primary element of the Friend node.
	//
	//	All transport control messages originated by a Low Power node shall be sent as
	//	Unsegmented Control messages with the SRC field set to the unicast address
	//	of the primary element of the node that supports the Low Power feature.
	//


	//	Segmented Control Message:
	//	[0]
	//		[7]		SEG	(1)
	//		[6:0]	Opcode, Rang 0x01:0x7F, (0x00 (0b0000000) is reserved and ignored on RX)
	//	[1]
	//		[7]		RFU (tx as 0) No encrypted Payload or MIC in Control PDU
	//		[6:0]	SeqZero MSBs
	//	[2]
	//		[7:2]	SeqZero LSBs
	//		[1:0]	SegO MSBs
	//	[3]
	//		[7:5]	SegO LSBs
	//		[4:0]	SegN
	//	[4:n]	Range: 8:64 bits, 1:8 octets
	//		[7:5]	Segment m
	//
	//	Every Segmented Control message for the same Upper Transport Control PDU
	//	shall have the same values for Opcode, SeqZero, and SegN.

	//	The lower transport layer shall only transmit Segmented Access messages
	//	or Segmented Control messages for a single Upper Transport PDU to the
	//	same *destination* at a time.
	//
	//	Hmmm... this implies that there is a special "layer" between the
	//	Element (src address) and the Network layer that has a queue for
	//	each destination address.
	//
	//	Does this rule apply across sub-networks?
	//

	//	Non-Unicast Destination Addresses
	//
	//	How are broadcast/group/virtual addresses treated? Can those even be
	//	segmented?
	//
	//	3.5.3.3 Segmentation behavior
	//
	//		Once an Upper Transport PDU has been segmented, the lower transport layer
	//		will transmit the initial Lower Transport PDUs of each segment of that message.
	//
	//	*	If the message is destined to a unicast address, then the lower transport layer
	//		will expect a Segment Acknowledgment message from that node, or from a Friend
	//		node on behalf of that node.
	//
	//		If the message is addressed to a virtual or group address, then Segment Acknowledgment
	//		messages will not be sent by those devices.
	//
	//	*	If the Lower Transport PDUs of the segmented Upper Transport PDU are being sent
	//		to a group address or a virtual address, then the lower transport layer shall
	//		send all Lower Transport PDUs of the segmented Upper Transport PDU.
	//
	//		It is recommended to send all Lower Transport PDUs multiple times, introducing
	//		small random delays between repetitions.
	//
	//			Note:The Upper Transport PDU sent to a group or virtual address is not
	//			acknowledged by recipients, thus the message delivery status is unknown
	//			and should be considered to be unacknowledged. The behavior recommended
	//			above is designed to significantly increase the probability of the successful
	//			delivery of the segmented Upper Transport PDU.

	//	A message sent to the all-proxies address shall be processed by the primary element
	//	of all nodes that have the proxy functionality enabled.
	//
	//	A message sent to the all-friends address shall be processed by the primary element
	//	of all nodes that have the friend functionality enabled.
	//
	//	A message sent to the all-relays address shall be processed by the primary element
	//	of all nodes that have the relay functionality enabled.
	//
	//	A message sent to the all-nodes address shall be processed by the primary element
	//	of all nodes.

	//	SeqAuth Notes:
	//
	//		647262	= SEQ
	//		645272	= SEQ - 8191
	//		  1849	= SegZero
	//		647262	= SegZero + SEQ
	//		646000	= Masked = SEQ & ~1FFF
	//		647849	= Masked + SegZero
	//
	//		Answer:
	//		645849
	//
	//		  2000	= Answer ^ (Masked + SegZero)
	//
	//		IvIndexSEQ = 0x58437AF2 || 0x647262 = 0x58437AF2647262
	//
	//		SeqAuth = ((IvIndexSEQ - 0x1FFF) & 0x1FFF) + SeqZero

	//	AppKey encryption parameters:
	//
	//		Application Nonce
	//			The nonce uses the sequence number and the source address, ensuring
	//			that two different nodes cannot use the same nonce. The IV Index is
	//			used to provide significantly more nonce values than the sequence
	//			number can provide for a given node. Management of the IV Index is
	//			described in Section 3.10.5.
	//
	//			Note: The authentication and encryption of the access payload is not
	//				dependent on the TTL value, meaning that as the access payload
	//				is relayed through a mesh network, the access payload does not
	//				need to be re-encrypted at each hop.
	//
	//			Application Nonce
	//			[0]		Nonce Type		0x01
	//			[1]		ASZMIC and Pad	0b10000000 (bit 7 is ASZMIC)
	//			[2:4]	SEQ (24 lowest bits of SeqAuth)
	//			[5:6]	Source Address
	//			[7:8]	Destination Address
	//			[9:12]	IV Index
	//
	//		Application Key

static uint32_t	calculateSeqAuth(
				uint32_t	ivIndex,
				uint32_t	seq,
				uint16_t	seqZero
				) noexcept{
	// SeqAuth = ((IvIndexSEQ - 0x1FFF) & 0x1FFF) + SeqZero
	// Hmmmm... I probably need IVI from the network layer
	// to get the correct IV Index to use for this in the
	// face of an IV Update.
	// However, I seem to recall that a given Upper Transport
	// session MUST use the same IV Index.
	// Still, the receive() operation probably needs to
	// supply the IV Index so that we can filter the
	// correct packets.

	/*	The SeqAuth is composed of the IV Index and the
		sequence number (SEQ) of the first segment and is
		therefore a 56-bit value, where the IV Index is
		the most significant octets and the sequence number is the
		least significant octets. Only the least significant 13 bits
		of the value (known as SeqZero) is included in the Segmented
		message and Segment Acknowledgment message.
	
		Upon reassembling a complete Segmented Access message,
		the SeqAuth value can be derived from the IV Index,
		SeqZero, and SEQ in any of the segments, by determining
		the largest SeqAuth value for which SeqZero is between
		SEQ - 8191 and SEQ inclusive and using the same IV Index.

		For example, if the received SEQ of a message was 0x647262,
		the IV Index was 0x58437AF2, and the received SeqZero
		value was 0x1849, then the SeqAuth value is 0x58437AF2645849.

		If the received SEQ of a message was 0x647262 and the
		received SeqZero value was 0x1263, then the SeqAuth
		value is 0x58437AF2645263.

		Because of the limited size of SeqZero, it is not possible
		to send a segmented message once the SEQ is 8192 higher than
		SeqAuth.  If a segmented message has not been acknowledged
		by the time that SEQ is 8192 higher than SeqAuth,
		then the delivery of the Upper Transport PDU shall be canceled.
	 */

	/*
		IV Index: 0x58437AF2, 0b0101 1000 0100 0011 0111 1010 1111 0020
		SeqZero: 0x1849, 0b1 1000 0100 1001
		SEQ: 0x647262, 0b0110 0100 0111 0010 0110 0010
                                    0b1 1111 1111 1111
		SEQ-Masked: 0x647262, 0b0110 0100 0110 0000 0000 0000

		We *know* that the 13-lsbs of SeqAuth *must* be SegZero.

		IV Index || SEQ : 0x58437AF2647262
		0b0101 1000 0100 0011 0111 1010 1111 0020 0110 0100 0111 0010 0110 0010

		We *know* that the 13-lsbs of SeqAuth *must* be SegZero.

		If seq < 0x1FFF, then SeqAuth is simply seq & 0x1

		SEQ: 0x100000
		SeqZero: 0x003

		0FE001
		0FE000
		0FE003

		SEQ: 0x101034
		SeqZero: 0x003

		0FE005
		0FE000
		0FE003

	 */

	constexpr uint32_t	segZeroMask	= 0x1FFF;

	uint32_t
	seqAuthA	= ((seq - segZeroMask) & ~segZeroMask);
	seqAuthA	+= seqZero;

	uint32_t
	seqAuthB	= ((seq) & ~segZeroMask);
	seqAuthB	+= seqZero;

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s:\n"
		"\tivIndex: 0x%8.8X\n"
		"\tseq: 0x%8.8X\n"
		"\tseqZero: 0x%4.4X\n"
		"\t(seqAuthA(0x%8.8X) > seqAuthB(0x%8.8X)): %s\n"
		"",
		__PRETTY_FUNCTION__,
		ivIndex,
		seq,
		seqZero,
		seqAuthA,
		seqAuthB,
		(seqAuthA > seqAuthB)?"true":"false"
		);
	#endif

	uint32_t	seqAuth;

	// FIXME: This "if" is a HACK.
	// I'm trying to avoid using uint64_t math
	// and the ivIndex. In my mind, this should
	// work to simply detect an unsigned wrap-around.
	//
	// NOTE: I may need to check the opposite case
	// when the MSB goes from 0 to 1.
	if(seqAuthA & 0x80000000){
		if(!(seqAuthB & 0x80000000)){
			// seqAuthA is "negative"
			return seqAuthB;
			}
		}
	else if(seqAuthB & 0x80000000){
		return seqAuthA;
		}

	seqAuth	= (seqAuthA > seqAuthB)?
				seqAuthA:
				seqAuthB
				;

	return seqAuth;
	}

bool	Reaper::receive(
			uint16_t	srcAddress,
			uint16_t	dstAddress,
			uint32_t	seq,
			const void*	frame,
			unsigned	length
			) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n"
		"\tsrcAddress: 0x%4.4X\n"
		"\tdstAddress: 0x%4.4X\n"
		"\tseq: 0x%8.8X\n"
		"",
		__PRETTY_FUNCTION__,
		srcAddress,
		dstAddress,
		seq
		);
	Oscl::Error::Info::hexDump(
		frame,
		length
		);
	#endif

	if(dstAddress != _dst){
		#ifdef DEBUG_TRACE
		Oscl::Error::Info::log(
			"%s: dstAddress(0x%4.4X) != _dst(0x%4.4X)\n",
			__PRETTY_FUNCTION__,
			dstAddress,
			_dst
			);
		#endif
		return false;
		}

	if(srcAddress != _src){
		#ifdef DEBUG_TRACE
		Oscl::Error::Info::log(
			"%s: srcAddress(0x%4.4X) != _src(0x%4.4X)\n",
			__PRETTY_FUNCTION__,
			srcAddress,
			_src
			);
		#endif
		return false;
		}

	if(seq < _seq){
		/*	If the lower transport layer receives a segment
			for a message with a SeqAuth value less than the
			sequence authentication value, then it shall ignore
			that segment. If the lower transport layer receives a
			segment for a new message, then it shall save the
			SeqAuth value from that segment as the new sequence
			authentication value.

			Note: The sequence authentication value logically
			includes the IV Index, so if a Lower Transport PDU
			is received using a previous IV Index, then this
			would be a SeqAuth value that is less than the
			sequence authentication value.
		 */
		return true;
		}

	_seq	= seq;

	/*	A lower transport layer that receives a segment of a
		multi-segment message for a SeqAuth value greater than
		the sequence authentication value shall start an
		incomplete timer, which defines the maximum amount
		of time the lower transport layer waits between unique
		segments of the same transaction. The incomplete timer
		shall be set to a minimum of 10 seconds.  A lower
		transport layer that receives a segment of a multi-
		segment message for a SeqAuth value greater than the
		sequence authentication value where the destination
		is a unicast address shall start an acknowledgment timer,
		which defines the amount of time after which the lower
		transport layer sends a Segment Acknowledgment message.
		The acknowledgment timer shall be set to a minimum of
		150 + 50 * TTL milliseconds.
		If the lower transport layer receives another segment
		for the sequence authentication value while the acknowledgment
		timer is inactive, it shall restart the acknowledgment timer.

			Note: If the lower transport layer receives any
			segment for the sequence authentication value while
			the acknowledgment timer is active, then the
			acknowledgment timer is not restarted.

		If the lower transport layer receives any segment for
		the sequence authentication while the incomplete timer
		is active, the incomplete timer shall be restarted.
		The lower transport layer shall mark each segment
		received into a block acknowledgment value that can
		be later transmitted back to the source node.
		When all segments of a Segmented message have been received,
		the lower transport layer shall send a Segment Acknowledgment
		message with the BlockAck field set to the block acknowledgment
		value for the sequence authentication value. It shall cancel
		the incomplete timer and the acknowledgment timer, and it
		shall send the reassembled message to the upper transport layer.
		If the segments were Segmented Access messages, then the
		reassembled message shall be processed as defined in
		Section 3.6.4.2. If the segments were Segmented Control messages,
		then the reassembled message shall be processed as
		defined in Section 3.6.5.
	 */

	//	Segmented Access Message:
	//	[0]
	//		[7]		SEG	(1)
	//		[6]		AKF
	//		[5:0]	AID
	//	[1]
	//		[7]		SZMIC, Size of TransMIC (0:32-bit, 1:64-bit)
	//		[6:0]	SeqZero MSBs
	//	[2]
	//		[7:2]	SeqZero LSBs
	//		[1:0]	SegO MSBs
	//	[3]
	//		[7:5]	SegO LSBs
	//		[4:0]	SegN
	//	[4:n]	Range: 8:96 bits, 1:12 octets
	//		[7:5]	Segment m
	//
	//	Every Segmented Access message for the same Upper Transport Access PDU
	//	shall have the same values for AKF, AID, SZMIC, SeqZero, and SegN.

	Oscl::Endian::Decoder::Linear::Part
	beDecoder(
		frame,
		length
		);

	Oscl::Decoder::Api&
	decoder	= beDecoder.be();

	uint8_t		segAkfAID;
	uint8_t		szmicSeqZero;
	uint8_t		seqZeroSegO;
	uint8_t		segOSegN;

	decoder.decode(segAkfAID);
	decoder.decode(szmicSeqZero);
	decoder.decode(seqZeroSegO);
	decoder.decode(segOSegN);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			OSCL_PRETTY_FUNCTION
			);
		return false;
		}

	// /home/mike/root/src/oscl/bluetooth/mesh/model/foundation/part.cpp

	//	Segmented Access Message:
	//	[0]
	//		[7]		SEG	(1)
	//		[6]		AKF
	//		[5:0]	AID
	//	[1]
	//		[7]		SZMIC, Size of TransMIC (0:32-bit, 1:64-bit)
	//		[6:0]	SeqZero MSBs
	//	[2]
	//		[7:2]	SeqZero LSBs
	//		[1:0]	SegO MSBs
	//	[3]
	//		[7:5]	SegO LSBs
	//		[4:0]	SegN
	//	[4:n]	Range: 8:96 bits, 1:12 octets
	//		[7:5]	Segment m
	//
	//	Every Segmented Access message for the same Upper Transport Access PDU
	//	shall have the same values for AKF, AID, SZMIC, SeqZero, and SegN.

	bool
	segmented	= (segAkfAID >> 7)?true:false;

	if(!segmented){
		#ifdef DEBUG_TRACE
		Oscl::Error::Info::log(
			"%s: not segmented!\n",
			__PRETTY_FUNCTION__
			);
		#endif
		return false;
		}

	bool
	szmic	= (szmicSeqZero & (1<<7))?true:false;

	uint8_t
	opcode	= segAkfAID & 0x7F;

	if(opcode != _opcode){
		#ifdef DEBUG_TRACE
		Oscl::Error::Info::log(
			"%s: opcode(0x%2.2X) != _opcode(0x%2.2X)\n",
			__PRETTY_FUNCTION__,
			opcode,
			_opcode
			);
		#endif
		return false;
		}

	uint16_t
	seqZero	=	szmicSeqZero & 0x7F;
	seqZero	<<=	6;
	seqZero	|= (seqZeroSegO >> 2) & 0x3F;

	if(_seqZero != 0xFFFF){
		// This is NOT the first segment received.
		if(seqZero != _seqZero){
			// This is for a different
			// segmented message
			return false;
			}
		}
	else {
		// This IS the first segment received.
		_seqZero	= seqZero;
		}

	uint8_t
	segO	= (seqZeroSegO & 0x03);
	segO	<<= 3;
	segO	|= (segOSegN >> 5) & 0x07;

	uint8_t
	segN	= segOSegN & 0x1F;

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n"
		"\tszmic: %s\n"
		"\t_szmic: %s\n"
		"\t_seqZero: 0x%4.4X\n"
		"\tsegO: %u\n"
		"\tsegN: %u\n"
		"",
		__PRETTY_FUNCTION__,
		szmic?"64-bit":"32-bit",
		_szmic?"64-bit":"32-bit",
		_seqZero,
		segO,
		segN
		);
	#endif

	if(!_blockAck){
		_szmic	= szmic;
		_segN	= segN;
		}

	uint32_t	segMask	= (1<<segO);

	uint32_t	maxMask	= (1<<(segN+1))-1;

	if(!(segMask & _blockAck)){
		// We havent seen this segment
		// before.
		_context.segmentReceived(
			frame,
			length,
			_ivIndex,
			seq,
			srcAddress,
			dstAddress,
			false,	// FIXME: redundant
			_ttl
			);
		}

	_blockAck	|= segMask;

	const uint8_t*	p	= (const uint8_t*)frame;
	unsigned	segmentOffset	= 4;
	unsigned	segmentLength	= length-segmentOffset;

	memcpy(
		&_buffer[segO*12],
		&p[segmentOffset],
		segmentLength
		);

	if(!_ackTimerIsActive){
		// 150 + (50 * TTL) milliseconds
		// This formula is just strange because
		// it could be either:
		//	a. TTL of the received packet?
		//	b. Default Network TTL?
		//
		// In neither case does it reflect the
		// number of hops from the sender, which
		// appears to be the idea behind the formula.
		//
		// If we assume it is the TTL of the
		// received packet, then the TTL may be
		// much smaller than the number of hops
		// in a large network.
		// 
		// If we assume it is the default Network
		// TTL, there is an issue with different
		// TTLs being configured for each Node
		// in the network. If the default TTL
		// for this Node is smaller than the
		// number of hops to the sender, then
		// we will:
		//  1. Be unable to reach the sender.
		//  2. Have a very short ack timer using
		//		more bandwidth.
		//
		// It is probably better to have a short
		// ACK timer to prevent the segmentation
		// process at the far end from timing out.
		//
		// Here, we use the smaller off the network TTL
		// or the network TTL minus the received TTL.

		uint8_t	ttl	= _kernelApi.getDefaultTTL();

		if(ttl < _ttl){
			ttl	= _ttl - ttl;
			}

		// Furthermore, we want to be sure that
		// the segmentation 

		if(ttl > 10){
			ttl	= 10;
			}

		_ackTimerApi.restart(
			150 + (50 * ttl)
			);

		_ackTimerIsActive	= true;
		}

#if 0
	// Let's ignore that last
	// segment to test the incomplete
	// timer.
	if(segO == segN){
		return true;
		}
#endif

	_incompleteTimerApi.restart(10000);

	if(segO == segN){
		_lastSegmentLength	= segmentLength;
		}

	if((_blockAck & maxMask) == maxMask){
		// We're DONE

		_ackTimerApi.stop();

		/* The specification says that we must
			stop the incomplete timer at this
			point. However, we may need to send
			the acknowledgement again if it is
			not received by the sender.

			Therefore, we will use this same
			timer run and delay the stopping
			of this reassembler for a time.
		 */
		_incompleteTimerApi.restart(5000);
		_incompleteTimerApi.setExpirationCallback(
			_stopTimerExpired
			);

		transmitACK(_seqZero);

		if(_fullyReceived){
			return true;
			}

		uint32_t
		seqAuth	= calculateSeqAuth(
						_ivIndex,	// Should come from network layer
						seq,
						_seqZero
						);

		#ifdef DEBUG_TRACE
		Oscl::Error::Info::log(
			"%s: DONE\n"
			"\tivIndex: 0x%8.8X\n"
			"\tseqAuth: 0x%8.8X\n"
			"\tPDU:\n"
			"",
			__PRETTY_FUNCTION__,
			_ivIndex,
			seqAuth
			);
		Oscl::Error::Info::hexDump(
			_buffer,
			_lastSegmentLength + (12*segN)
			);
		#endif

		_fullyReceived	= true;

		_upperTransportApi.receive(
			_buffer,
			_lastSegmentLength + (12*segN),
			_ivIndex,
			seqAuth,
			_src,
			_dst,
			_aid,
			_akf,
			_szmic
			);

		if(!_sendACK){
			// We will never send an ACK,
			// so there's no need to hang out
			// waiting for stopTimerExpired()
			stop();
			}

		}

	return true;
	}

bool	Reaper::receiveControl(
			uint16_t	srcAddress,
			uint16_t	dstAddress,
			uint32_t	seq,
			const void*	frame,
			unsigned	length
			) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n"
		"\tsrcAddress: 0x%4.4X\n"
		"\tdstAddress: 0x%4.4X\n"
		"\tseq: 0x%8.8X\n"
		"",
		__PRETTY_FUNCTION__,
		srcAddress,
		dstAddress,
		seq
		);
	Oscl::Error::Info::hexDump(
		frame,
		length
		);
	#endif

	return false;
	}
