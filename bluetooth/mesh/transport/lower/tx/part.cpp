/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <new>
#include "part.h"
#include "oscl/error/info.h"
#include "oscl/encoder/be/base.h"

using namespace Oscl::BT::Mesh::Transport::Lower::TX;

//#define DEBUG_TRACE

Part::Part(
	Oscl::Pdu::Memory::Api&		freeStoreApi,
	Oscl::Timer::Factory::Api&	timerFactoryApi,
	Oscl::BT::Mesh::Element::
	Symbiont::Kernel::Api&		kernelApi,
	Oscl::Inhibitor::Api&		ivUpdateInhibitorApi,
	Oscl::Inhibitor::Api&		rxDisableInhibitorApi,
	uint16_t					elementAddress
	) noexcept:
		_freeStoreApi(freeStoreApi),
		_timerFactoryApi(timerFactoryApi),
		_kernelApi(kernelApi),
		_ivUpdateInhibitorApi(ivUpdateInhibitorApi),
		_rxDisableInhibitorApi(rxDisableInhibitorApi),
		_elementAddress(elementAddress),
		_rxComposer(
			*this,
			&Part::receive
			)
		{
	for(unsigned i=0;i<nReaperMem;++i){
		// Pre-allocate timers.
		Oscl::Timer::Api*
		timer	= timerFactoryApi.allocate();
		_reaperMem[i].mem.timer	= timer;

		_freeReaperMem.put(&_reaperMem[i]);
		}
	}

Part::~Part() noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif

	for(unsigned i=0;i<nReaperMem;++i){
		if(_reaperMem[i].mem.timer->running()){
			Oscl::Error::Info::log(
				"%s: WARNING: timer [%u]%p is running!\n",
				OSCL_PRETTY_FUNCTION,
				i,
				_reaperMem[i].mem.timer
				);
			}
		_timerFactoryApi.free(*_reaperMem[i].mem.timer);
		}
	}

void	Part::stop() noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif

	Oscl::BT::Mesh::Transport::Lower::Segment::Access::Reaper*
	reaper;

	while((reaper = _pendingQ.get())){
		reaper->~Reaper();
		ReaperMem*
		mem	= (ReaperMem*)reaper;
		_freeReaperMem.put(mem);
		}
	
	while((reaper = _activeReapers.get())){
		reaper->stop();
		}
	}

void	Part::stopped(Oscl::BT::Mesh::Transport::Lower::Segment::Access::Reaper& part) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif

	_activeReapers.remove(&part);

	part.~Reaper();

	ReaperMem*
	mem	= (ReaperMem*)&part;

	_freeReaperMem.put(mem);

	processPendingQ();
	}

	// There are two types of Lower Transport Layers
	// distinguished by the CTL field of the Network PDU:
	//
	// 1. Lower Transport Access Layer
	// 2. Lower Transport Control
	//
	// Access Layer messages are forwarded to
	// an Element containing the Lower Transport Layer?
	//
	// The first octet of a Lower Transport Access message contains:
	//   [7]	SEG (segmented or unsegmented)
	//   [6]	AKF (application/device key)
	//   [5:0]	AID (application or device key 0b000000)
	//
	// The first octet of a a Lower Transport message contains:
	//	[7]		SEG (segmented or unsegmented)
	//	[6:0]	Opcode
	//		0b0000000	- Segment Acknowledge (Unsegmented only)
	//		0b0000001	- Friend Poll
	//		0b0000010	- Friend Update
	//		0b0000011	- Friend Request (LPN TX: dest: <all-friends-group-address>, src:?)
	//		0b0000100	- Friend Offer
	//		0b0000101	- Friend Clear
	//		0b0000110	- Friend Clear Confirm
	//		0b0000111	- Friend Subscription List Add
	//		0b0001000	- Friend Subscription List Remove
	//		0b0001001	- Friend Subscription List Confirm
	//		0b0001010	- Heartbeat
	//
	//	All transport control messages originated by a Friend node shall be sent as
	//	Unsegmented Control messages with the SRC field set to the unicast address
	//	of the primary element of the Friend node.
	//
	//	All transport control messages originated by a Low Power node shall be sent as
	//	Unsegmented Control messages with the SRC field set to the unicast address
	//	of the primary element of the node that supports the Low Power feature.
	//

	//	Segment Acknowledgement message:
	//	[0]
	//		[7]		SEG	(0)
	//		[6:0]	0b0000000
	//	[1]
	//		[7]		OBO
	//			0	Direct
	//			1	Friend On Behalf Of LPN (Friend queued message for LPN)
	//		[6:0]	SeqZero MSBs
	//	[2]
	//		[7:2]	SeqZero LSBs
	//		[1:0]	RFU (tx as 0b00)
	//	[3:6]	BlockAck, 32-bit block mask (BE)
	//		[n]		Segment n ACK, Range 0 <= n <= 31

	//	Segmented Access Message:
	//	[0]
	//		[7]		SEG	(1)
	//		[6]		AKF
	//		[5:0]	AID
	//	[1]
	//		[7]		SZMIC, Size of TransMIC (0:32-bit, 1:64-bit)
	//		[6:0]	SeqZero MSBs
	//	[2]
	//		[7:2]	SeqZero LSBs
	//		[1:0]	SegO MSBs
	//	[3]
	//		[7:5]	SegO LSBs
	//		[4:0]	SegN
	//	[4:n]	Range: 8:96 bits, 1:12 octets
	//		[7:5]	Segment m
	//
	//	Every Segmented Access message for the same Upper Transport Access PDU
	//	shall have the same values for AKF, AID, SZMIC, SeqZero, and SegN.


	//	Segmented Control Message:
	//	[0]
	//		[7]		SEG	(1)
	//		[6:0]	Opcode, Rang 0x01:0x7F, (0x00 (0b0000000) is reserved and ignored on RX)
	//	[1]
	//		[7]		RFU (tx as 0) No encrypted Payload or MIC in Control PDU
	//		[6:0]	SeqZero MSBs
	//	[2]
	//		[7:2]	SeqZero LSBs
	//		[1:0]	SegO MSBs
	//	[3]
	//		[7:5]	SegO LSBs
	//		[4:0]	SegN
	//	[4:n]	Range: 8:64 bits, 1:8 octets
	//		[7:5]	Segment m
	//
	//	Every Segmented Control message for the same Upper Transport Control PDU
	//	shall have the same values for Opcode, SeqZero, and SegN.

	//	The lower transport layer shall only transmit Segmented Access messages
	//	or Segmented Control messages for a single Upper Transport PDU to the
	//	same *destination* at a time.
	//
	//	Hmmm... this implies that there is a special "layer" between the
	//	Element (src address) and the Network layer that has a queue for
	//	each destination address.
	//
	//	Does this rule apply across sub-networks?
	//

	//	Non-Unicast Destination Addresses
	//
	//	How are broadcast/group/virtual addresses treated? Can those even be
	//	segmented?
	//
	//	3.5.3.3 Segmentation behavior
	//
	//		Once an Upper Transport PDU has been segmented, the lower transport layer
	//		will transmit the initial Lower Transport PDUs of each segment of that message.
	//
	//	*	If the message is destined to a unicast address, then the lower transport layer
	//		will expect a Segment Acknowledgment message from that node, or from a Friend
	//		node on behalf of that node.
	//
	//		If the message is addressed to a virtual or group address, then Segment Acknowledgment
	//		messages will not be sent by those devices.
	//
	//	*	If the Lower Transport PDUs of the segmented Upper Transport PDU are being sent
	//		to a group address or a virtual address, then the lower transport layer shall
	//		send all Lower Transport PDUs of the segmented Upper Transport PDU.
	//
	//		It is recommended to send all Lower Transport PDUs multiple times, introducing
	//		small random delays between repetitions.
	//
	//			Note:The Upper Transport PDU sent to a group or virtual address is not
	//			acknowledged by recipients, thus the message delivery status is unknown
	//			and should be considered to be unacknowledged. The behavior recommended
	//			above is designed to significantly increase the probability of the successful
	//			delivery of the segmented Upper Transport PDU.

	//	A message sent to the all-proxies address shall be processed by the primary element
	//	of all nodes that have the proxy functionality enabled.
	//
	//	A message sent to the all-friends address shall be processed by the primary element
	//	of all nodes that have the friend functionality enabled.
	//
	//	A message sent to the all-relays address shall be processed by the primary element
	//	of all nodes that have the relay functionality enabled.
	//
	//	A message sent to the all-nodes address shall be processed by the primary element
	//	of all nodes.

void	Part::transmitUnsegmentedAccess(
			Oscl::BT::Mesh::Transport::
			TX::Complete::Api*			completeApi,
			Oscl::Pdu::Pdu*				upperTransportAccessPDU,
			uint32_t					ivIndex,
			uint32_t					seq,
			uint16_t					dst,
			uint8_t						akfAID,
			uint8_t						ttl
			) noexcept{
	constexpr unsigned	maxUnsegmentedUpperTransportAccessPduSize	= 15;

	bool
	tooBig	= upperTransportAccessPDU->length() > maxUnsegmentedUpperTransportAccessPduSize;

	if(tooBig){
		if(completeApi){
			completeApi->failedTooBig();
			}
		return;
		}

	unsigned	bufferSize;
	void*
	mem		= _freeStoreApi.allocBuffer(bufferSize);

	if(!mem){
		if(completeApi){
			completeApi->failedBusy();
			}
		return;
		}

	Oscl::Encoder::BE::Base
	beEncoder(
		mem,
		bufferSize
		);

	Oscl::Encoder::Api&	encoder	= beEncoder;

	//	Lower Transport PDU:
	//	[0:1]	DST
	//	[2:n]	Unsegmented Access Message

	encoder.encode(dst);

	//	Unsegmented Access Message:
	//	[0]
	//		[7]		SEG	(0)
	//		[6]		AKF
	//		[5:0]	AID
	//	[1:n] Upper Transport PDU Range 40:120 bits, 5:15 octets

	uint8_t
	opcode	=
			(akfAID<<0)
		|	(0<<7)	//  SEG
		;

	encoder.encode(opcode);

	Oscl::Handle<Oscl::Pdu::Fragment>
	fragment	= _freeStoreApi.allocFragmentWrapper(
				mem,
				encoder.length()
				);

	if(!fragment){
		_freeStoreApi.freeBuffer(mem);
		if(completeApi){
			completeApi->failedBusy();
			}
		return;
		}

	Oscl::Handle<Oscl::Pdu::Composite>
	composite	= _freeStoreApi.allocCompositeWrapper(
				*upperTransportAccessPDU
				);

	if(!composite){
		_freeStoreApi.freeBuffer(mem);
		if(completeApi){
			completeApi->failedBusy();
			}
		return;
		}

	composite->prepend(fragment);

	_kernelApi.transmitNetwork(
		composite,
		ivIndex,
		seq,
		_elementAddress,
		dst,
		ttl
		);

	if(completeApi){
		completeApi->success();
		}
	}

Oscl::BT::Mesh::Transport::Lower::Segment::Access::Reaper*
	Part::findReaperWithMatchingDestination(uint16_t dst) noexcept{

	for(
		Oscl::BT::Mesh::Transport::Lower::Segment::Access::Reaper*
		reaper	= _activeReapers.first();
		reaper;
		reaper	= _activeReapers.next(reaper)
		){
		if(dst == reaper->getDestinationAddress()){
			return reaper;
			}
		}

	return 0;
	}

void	Part::processPendingQ() noexcept{
	// FIXME:
	//	The lower transport layer shall only transmit Segmented Access messages
	//	or Segmented Control messages for a single Upper Transport PDU to the
	//	same *destination* at a time.
	//
	//	Given that each Element has its own unicast address and that
	//	Elements are generally treated as independent entities on the
	//	network, I suspect that this rule is per Element.
	//
	//	Hmmm... this implies that there is a special "layer" between the
	//	Element (src address) and the Network layer that has a queue for
	//	each destination address.
	//
	//	Does this rule apply across sub-networks?

	Oscl::BT::Mesh::Transport::Lower::
	Segment::Access::Reaper*
	pending	= _pendingQ.first();

	if(!pending){
		return;
		}

	Oscl::BT::Mesh::Transport::Lower::
	Segment::Access::Reaper*
	reaper	= findReaperWithMatchingDestination(
				pending->getDestinationAddress()
				);

	if(reaper){
		// Wait till later.
		return;
		}

	reaper	= _pendingQ.get();

	_activeReapers.put(reaper);

	reaper->start();
	}

void	Part::transmitSegmentedAccess(
			Oscl::BT::Mesh::Transport::
			TX::Complete::Api*			completeApi,
			Oscl::Pdu::Pdu*				upperTransportAccessPdu,
			uint32_t					ivIndex,
			uint32_t					seq,
			uint16_t					dst,
			uint8_t						akfAID,
			bool						szmic,
			uint8_t						ttl
			) noexcept{

	#ifdef DEBUG_TRACE
	bool
	active	= findReaperWithMatchingDestination(dst);

	Oscl::Error::Info::log(
		"%s\n"
		"\tivIndex: 0x%8.8X\n"
		"\tdst: 0x%4.4X\n"
		"\tseq: 0x%6.6X\n"
		"\tttl: %u\n"
		"\tactive reaper: %s\n"
		"",
		OSCL_PRETTY_FUNCTION,
		ivIndex,
		dst,
		seq,
		ttl,
		active?"true":"false"
		);
	#endif

	// Allocate Reaper
	ReaperMem*
	mem	= _freeReaperMem.get();

	if(!mem){
		Oscl::Error::Info::log(
			"%s: out of lower-transport segmentation memory.\n",
			OSCL_PRETTY_FUNCTION
			);
		if(completeApi){
			completeApi->failedBusy();
			}
		return;
		}

	if(ttl == 0xFF){
		ttl	= _kernelApi.getDefaultTTL();
		}

	Oscl::BT::Mesh::Transport::Lower::
	Segment::Access::Reaper*
	reaper	= new(&mem->mem.reaper)
				Oscl::BT::Mesh::Transport::Lower::
				Segment::Access::Reaper(
					*this,	// context
					_ivUpdateInhibitorApi,
					_rxDisableInhibitorApi,
					_freeStoreApi,
					*mem->mem.timer,
					_kernelApi,
					completeApi,
					// rxHostApi,
					upperTransportAccessPdu,	// pdu
					ivIndex,
					seq,
					_elementAddress,
					dst,
					ttl,
					(akfAID & 0x3F),			// aid
					(akfAID & 0x40)?true:false,	// akf
					szmic
					);

	_pendingQ.put(reaper);
	processPendingQ();
	}

bool	Part::receive(
			bool		ctl,
			uint8_t		ttl,
			uint16_t	src,
			uint32_t	seq,
			const void*	frame,
			unsigned	length
			) noexcept{
	for(
		Oscl::BT::Mesh::Transport::Lower::
		Segment::Access::Reaper*
		reaper	= _activeReapers.first();
		reaper;
		reaper	= _activeReapers.next(reaper)
		){
		bool
		handled	= reaper->receive(
					ctl,
					ttl,
					src,
					seq,
					frame,
					length
					);
		if(handled){
			return true;
			}
		}

	return false;
	}

bool	Part::receiveControl(
			uint16_t	srcAddress,
			uint16_t	dstAddress,
			uint32_t	seq,
			const void*	frame,
			unsigned	length
			) noexcept{
	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif

	if(dstAddress != _elementAddress){
		// This TX class only expects
		// Control packets that are ACKs.
		// ACKs are only transmitted directly
		// to specific destinations. Therefore,
		// if the destination address is not
		// for us, neither is it for any of
		// our segments.
		return false;
		}

	for(
		Oscl::BT::Mesh::Transport::Lower::
		Segment::Access::Reaper*
		reaper	= _activeReapers.first();
		reaper;
		reaper	= _activeReapers.next(reaper)
		){
		bool
		handled	= reaper->receiveControl(
					srcAddress,
					dstAddress,
					seq,
					frame,
					length
					);
		if(handled){
			return true;
			}
		}

	return false;
	}

