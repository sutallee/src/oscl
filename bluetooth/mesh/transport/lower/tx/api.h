/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_bluetooth_mesh_transport_lower_apih_
#define _oscl_bluetooth_mesh_transport_lower_apih_

#include <stdint.h>
#include "oscl/bluetooth/mesh/transport/tx/complete/api.h"

/** */
namespace Oscl {
/** */
namespace BT {
/** */
namespace Mesh {
/** */
namespace Transport {
/** */
namespace Lower {
/** */
namespace TX {

/** This interface is used by the upper transport layer
	to send an upper transport PDU to its destination.
 */
class Api {
	public:
		/*
			param [in] completeApi				Optional completion callback.
			param [in] upperTransportAccessPdu	The encrypted upper transport upperTransportAccessPdu.
			param [in] ivIndex					The IV Index used to encrypt the upperTransportAccessPdu.
			param [in] seq						The SEQ number used to encrypt the upperTransportAccessPdu.
			param [in] akfAID					The AKF/AID value used to encrypt the upperTransportAccessPdu.
			param [in] dst						The destination address used to encrypt the upperTransportAccessPdu.
			param [in] src						The source address used to encrypt the upperTransportAccessPdu.
			param [in] ttl						The desired TTL
		 */
		virtual void	transmitUnsegmentedAccess(
							Oscl::BT::Mesh::Transport::
							TX::Complete::Api*			completeApi,
							Oscl::Pdu::Pdu*				upperTransportAccessPDU,
							uint32_t					ivIndex,
							uint32_t					seq,
							uint16_t					dst,
							uint8_t						akfAID,
							uint8_t						ttl	= 0xFF
							) noexcept=0;

		/*
			param [in] completeApi				Optional completion callback.
			param [in] upperTransportAccessPdu	The encrypted upper transport upperTransportAccessPdu.
			param [in] ivIndex					The IV Index used to encrypt the upperTransportAccessPdu.
			param [in] seq						The SEQ number used to encrypt the upperTransportAccessPdu.
			param [in] akfAID					The AKF/AID value used to encrypt the upperTransportAccessPdu.
			param [in] dst						The destination address used to encrypt the upperTransportAccessPdu.
			param [in] src						The source address used to encrypt the upperTransportAccessPdu.
			param [in] ttl						The desired TTL
		 */
		virtual void	transmitSegmentedAccess(
							Oscl::BT::Mesh::Transport::
							TX::Complete::Api*			completeApi,
							Oscl::Pdu::Pdu*				upperTransportAccessPdu,
							uint32_t					ivIndex,
							uint32_t					seq,
							uint16_t					dst,
							uint8_t						akfAID,
							bool						szmic,
							uint8_t						ttl	= 0xFF
							) noexcept=0;

	};

}
}
}
}
}
}


#endif
