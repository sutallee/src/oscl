/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_bluetooth_mesh_transport_lower_parth_
#define _oscl_bluetooth_mesh_transport_lower_parth_

#include <stdint.h>
#include "api.h"
#include "oscl/bluetooth/mesh/element/symbiont/kernel/api.h"
#include "oscl/bluetooth/mesh/transport/lower/segment/access/reaper.h"
#include "oscl/bluetooth/mesh/element/rx/itemcomp.h"
#include "oscl/memory/block.h"
#include "oscl/pdu/memory/api.h"
#include "oscl/done/operation.h"
#include "oscl/timer/factory/api.h"

/** */
namespace Oscl {
/** */
namespace BT {
/** */
namespace Mesh {
/** */
namespace Transport {
/** */
namespace Lower {
/** */
namespace TX {

/** This part implements the lower-transport layer.
 */
class Part :
	public Api,
	private Oscl::BT::Mesh::Transport::Lower::Segment::Access::Reaper::ContextApi
	{
	private:
		/** */
		Oscl::Pdu::Memory::Api&			_freeStoreApi;

		/** */
		Oscl::Timer::Factory::Api&		_timerFactoryApi;

		/** */
		Oscl::BT::Mesh::Element::
		Symbiont::Kernel::Api&			_kernelApi;

		/** */
		Oscl::Inhibitor::Api&			_ivUpdateInhibitorApi;

		/** */
		Oscl::Inhibitor::Api&			_rxDisableInhibitorApi;

		/** */
		const uint16_t					_elementAddress;

	private:
		/** */
		Oscl::BT::Mesh::Element::
		RX::ItemComposer<Part>			_rxComposer;

		/** */
		union ReaperMem {
			/** */
			void*	__qitemlink;

			struct {
				/** */
				Oscl::Memory::AlignedBlock<
					sizeof(
						Oscl::BT::Mesh::
						Transport::Lower::
						Segment::Access::
						Reaper
						)
					>			reaper;
				Oscl::Timer::Api*	timer;
				} mem;
			};

		/** */
		constexpr static unsigned	nReaperMem	= 4;

		/** */
		ReaperMem			_reaperMem[nReaperMem];

		/** */
		Oscl::Queue<ReaperMem>			_freeReaperMem;

	private:
		/** */
		Oscl::Queue<
			Oscl::BT::Mesh::
			Transport::Lower::
			Segment::Access::
			Reaper
			>							_activeReapers;

	private:
		/** */
		Oscl::Queue<
			Oscl::BT::Mesh::
			Transport::Lower::
			Segment::Access::
			Reaper
			>							_pendingQ;

	public:
		/**	Constructor
			param [in] kernelApi		Reference to the subnet
										interface used to transmit
										transport PDUs.
			param [in] completeApi		Optional callback to signal the status
										at the end of the transmission.
			param [in] pdu				The upper transport PDU
			param [in] ivIndex			The IV Index used to encrypt
										the upper transport PDU.
			param [in] remoteAddress	The destination address.
		 */
		Part(
			Oscl::Pdu::Memory::Api&		freeStoreApi,
			Oscl::Timer::Factory::Api&	timerFactoryApi,
			Oscl::BT::Mesh::Element::
			Symbiont::Kernel::Api&		kernelApi,
			Oscl::Inhibitor::Api&		ivUpdateInhibitorApi,
			Oscl::Inhibitor::Api&		rxDisableInhibitorApi,
			uint16_t					elementAddress
			) noexcept;

		~Part() noexcept;

		/** */
		void	stop() noexcept;

	private: // Oscl::BT::Mesh::Transport::Lower::Segment::Access::Reaper::ContextApi
		/**  */
		void	stopped(Oscl::BT::Mesh::Transport::Lower::Segment::Access::Reaper& part) noexcept;

	private:
		/** */
		void	processPendingQ() noexcept;

		/** */
		Oscl::BT::Mesh::Transport::Lower::Segment::Access::Reaper*
			findReaperWithMatchingDestination(uint16_t dst) noexcept;

	private:	// Oscl::BT::Mesh::Transport::Lower::TX::Api
		/*
			param [in] completeApi				Optional completion callback.
			param [in] upperTransportAccessPdu	The encrypted upper transport upperTransportAccessPdu.
			param [in] ivIndex					The IV Index used to encrypt the upperTransportAccessPdu.
			param [in] seq						The SEQ number used to encrypt the upperTransportAccessPdu.
			param [in] akfAID					The AKF/AID value used to encrypt the upperTransportAccessPdu.
			param [in] dst						The destination address used to encrypt the upperTransportAccessPdu.
			param [in] src						The source address used to encrypt the upperTransportAccessPdu.
			param [in] ttl						The desired TTL
		 */
		void	transmitUnsegmentedAccess(
					Oscl::BT::Mesh::Transport::
					TX::Complete::Api*			completeApi,
					Oscl::Pdu::Pdu*				upperTransportAccessPDU,
					uint32_t					ivIndex,
					uint32_t					seq,
					uint16_t					dst,
					uint8_t						akfAID,
					uint8_t						ttl	= 0xFF
					) noexcept;

		/*
			param [in] completeApi				Optional completion callback.
			param [in] upperTransportAccessPdu	The encrypted upper transport upperTransportAccessPdu.
			param [in] ivIndex					The IV Index used to encrypt the upperTransportAccessPdu.
			param [in] seq						The SEQ number used to encrypt the upperTransportAccessPdu.
			param [in] akfAID					The AKF/AID value used to encrypt the upperTransportAccessPdu.
			param [in] dst						The destination address used to encrypt the upperTransportAccessPdu.
			param [in] src						The source address used to encrypt the upperTransportAccessPdu.
			param [in] ttl						The desired TTL
		 */
		void	transmitSegmentedAccess(
					Oscl::BT::Mesh::Transport::
					TX::Complete::Api*			completeApi,
					Oscl::Pdu::Pdu*				upperTransportAccessPdu,
					uint32_t					ivIndex,
					uint32_t					seq,
					uint16_t					dst,
					uint8_t						akfAID,
					bool						szmic,
					uint8_t						ttl	= 0xFF
					) noexcept;

	private: // Oscl::BT::Mesh::Element::RX::Item	_rxComposer
		/**	This is how we receive segment
			acknowlegements for segmented messages.

			RETURN: true if this applies to an active
			segmentation activity for this lower-transport.
		 */
		bool	receive(
					bool		ctl,
					uint8_t		ttl,
					uint16_t	src,
					uint32_t	seq,
					const void*	frame,
					unsigned	length
					) noexcept;

	public:
		/** */
		bool	receiveControl(
					uint16_t	srcAddress,
					uint16_t	dstAddress,
					uint32_t	seq,
					const void*	frame,
					unsigned	length
					) noexcept;
	};

}
}
}
}
}
}


#endif
