/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <new>
#include "reaper.h"
#include "oscl/endian/decoder/linear/part.h"
#include "oscl/encoder/be/base.h"
#include "oscl/error/info.h"

//#define DEBUG_TRACE

using namespace Oscl::BT::Mesh::Transport::Lower::Segment::Access;

Reaper::Reaper(
	ContextApi&					context,
	Oscl::Inhibitor::Api&		ivUpdateInhibitorApi,
	Oscl::Inhibitor::Api&		rxDisableInhibitorApi,
	Oscl::Pdu::Memory::Api&		freeStoreApi,
	Oscl::Timer::Api&			timerApi,
	Oscl::BT::Mesh::Element::
	Symbiont::Kernel::Api&		kernelApi,
	Oscl::BT::Mesh::Transport::
	TX::Complete::Api*			completeApi,
	Oscl::Pdu::Pdu*				pdu,
	uint32_t					ivIndex,
	uint32_t					firstSequenceNumber,
	uint16_t					unicastAddress,
	uint16_t					remoteAddress,
	uint8_t						ttl,
	uint8_t						aid,
	bool						akf,
	bool						szmic
	) noexcept:
		_context(context),
		_freeStoreApi(freeStoreApi),
		_timerApi(timerApi),
		_kernelApi(kernelApi),
		_completeApi(completeApi),
		_pdu(pdu),
		_ivIndex(ivIndex),
		_firstSequenceNumber(firstSequenceNumber),
		_seqZero(firstSequenceNumber & 0x00001FFF),
		_unicastAddress(unicastAddress),
		_remoteAddress(remoteAddress),
		_ttl(ttl),
		_opcode(
			aid | ((akf?1:0)<<6)
			),
		_szmic(szmic),
		_ivUpdateInhibitor(ivUpdateInhibitorApi),
		_rxDisableInhibitor(rxDisableInhibitorApi),
		_rxComposer(
			*this,
			&Reaper::receive
			),
		_timerExpired(
			*this,
			&Reaper::timerExpired
			),
		_retryCounter(0),
		_expectACK(remoteAddress & (1<<15)?false:true),
		_obo(false),
		_firstPdu(true),
		_stopping(false)
		{
	for(	unsigned i=0;i<nSegmentMem;++i){
		 _freeSegmentMem.put(&_segmentMem[i]);
		}
	_timerApi.setExpirationCallback(
		_timerExpired
		);
	}

void	Reaper::start() noexcept{
	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s: %p\n",
		OSCL_PRETTY_FUNCTION,
		this
		);
	#endif

	bool
	failed	= buildSegments();
	if(failed){
		// Out-of-resources
		// FIXME: cancel with out-of-resources error.
		if(_completeApi){
			_completeApi->failedOutOfResources();
			}
		stop();
		return;
		}

	// Each Lower Transport PDU for an Upper Transport PDU shall
	// be transmitted at least two times unless acknowledged earlier.
	// If the lower transport layer stops retransmitting Lower Transport
	// PDUs before all Lower Transport PDUs have been acknowledged,
	// then the Upper Transport PDU is cancelled.

	_retryCounter	= 5;

	transmitSegments();
	}

Oscl::BT::Mesh::Element::RX::Item&	Reaper::getRxItem() noexcept{
	return _rxComposer;
	}

uint16_t	Reaper::getDestinationAddress() const noexcept{
	return _remoteAddress;
	}

void	Reaper::stop() noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s: %p\n",
		OSCL_PRETTY_FUNCTION,
		this
		);
	#endif

	_stopping	= true;

	_timerApi.stop();

	// Flush segment queue
	Segment*	segment;
	while((segment = _segments.get())){
		freeSegment(segment);
		}

	stopNext();
	}

void	Reaper::stopNext() noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif

	if(!_stopping){
		#ifdef DEBUG_TRACE
		Oscl::Error::Info::log(
			"%s: !_stopping\n",
			OSCL_PRETTY_FUNCTION
			);
		#endif
		return;
		}

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s: stopped.\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif

	_context.stopped(*this);
	}

void	Reaper::timerExpired() noexcept{
	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"***************** %s, %p\n",
		OSCL_PRETTY_FUNCTION,
		this
		);
	#endif

	// Each Lower Transport PDU for an Upper Transport PDU shall
	// be transmitted at least two times unless acknowledged earlier.
	// If the lower transport layer stops retransmitting Lower Transport
	// PDUs before all Lower Transport PDUs have been acknowledged,
	// then the Upper Transport PDU is cancelled.

	if(_retryCounter){
		--_retryCounter;
		}

	if(!_retryCounter){
		if(_completeApi){
			_completeApi->failedTimeout();
			}
		stop();
		return;
		}

	transmitSegments();
	}

Reaper::SegmentMem*	Reaper::allocSegmentMem() noexcept{
	return _freeSegmentMem.get();
	}

void	Reaper::freeSegment(Reaper::Segment* segment){
	segment->~Segment();
	_freeSegmentMem.put((SegmentMem*)segment);
	}

bool	Reaper::buildSegments() noexcept{
	unsigned
	transportLength	= _pdu->length();

	unsigned
	nSegments	= transportLength/12;

	if(transportLength % 12){
		++nSegments;
		}

	uint8_t
	segN		= nSegments;

	--segN;

	for(unsigned i=0;i<nSegments;++i){

		unsigned	bufferSize;

		void*
		mem		= _freeStoreApi.allocBuffer(bufferSize);

		if(!mem){
			Oscl::Error::Info::log(
				"%s: out of buffer memory.\n",
				OSCL_PRETTY_FUNCTION
				);
			return true;
			}

		constexpr unsigned	segmentHeaderSize	= 4;
		constexpr unsigned	maxPayloadSize		= 12;
		constexpr unsigned	maxSegmentSize		= segmentHeaderSize + maxPayloadSize;

		if(bufferSize < maxSegmentSize){
			Oscl::Error::Info::log(
				"%s: buffer too small.\n",
				OSCL_PRETTY_FUNCTION
				);
			return true;
			}

		uint8_t*	p	= (uint8_t*)mem;
		Oscl::Encoder::BE::Base
		beEncoder(
			mem,
			bufferSize
			);

		Oscl::Encoder::Api&	encoder	= beEncoder;

		//	Lower Transport PDU:
		//	[0:1]	DST
		//	[2:n]	Segmented Access Message

		encoder.encode(_remoteAddress);

		//	Segmented Access Message:
		//	[0]
		//		[7]		SEG	(1)
		//		[6]		AKF
		//		[5:0]	AID
		//	[1]
		//		[7]		SZMIC, Size of TransMIC (0:32-bit, 1:64-bit)
		//		[6:0]	SeqZero MSBs (12:
		//	[2]
		//		[7:2]	SeqZero LSBs from Upper Transport Layer
		//		[1:0]	SegO MSBs - MSBs of segment number
		//	[3]
		//		[7:5]	SegO LSBs - LSBs of segment number
		//		[4:0]	SegN - Last segment number (range 0:31)
		//	[4:n]	Range: 8:96 bits, 1:12 octets
		//		[7:5]	Segment m
		//
		//	Every Segmented Access message for the same Upper Transport Access PDU
		//	shall have the same values for AKF, AID, SZMIC, SeqZero, and SegN.

		uint8_t
		opcode	=
				_opcode
			|	(1<<7)	//  SEG
			;

		encoder.encode(opcode);

		uint8_t
		segO		= i;

		uint8_t
		szmicSeqZero	=
				(_szmic << 7)
			|	(_seqZero >> 6)
			;

		encoder.encode(szmicSeqZero);

		uint8_t
		segZeroSegO	=
				((_seqZero & 0x3F) << 2)
			|	(segO >> 3)
			;

		encoder.encode(segZeroSegO);

		uint8_t
		segOSegN	=
				((segO & 0x07) << 5)
			|	(segN << 0)
			;

		encoder.encode(segOSegN);

		unsigned
		headerLength	= encoder.length();

		unsigned
		n	= _pdu->read(
				&p[headerLength],
				12,		// max data
				i*12	// offset
				);

		unsigned
		len	= n + headerLength;

		#ifdef DEBUG_TRACE
		Oscl::Error::Info::log(
			"%s: Segment %u\n"
			"",
			OSCL_PRETTY_FUNCTION,
			i
			);
		Oscl::Error::Info::hexDump(
			p,
			len
			);
		#endif

		Oscl::Handle<Oscl::Pdu::Fixed>
		fixed	= _freeStoreApi.allocFixed(
					mem,
					len
					);

		if(!fixed){
			// This *should* never happen.
			_freeStoreApi.freeBuffer(mem);
			return true;
			}

		SegmentMem*
		segMem	= allocSegmentMem();

		if(!segMem){
			// This *should* never happen.
			_freeStoreApi.freeBuffer(mem);
			return true;
			}

		Segment*
		segment	= new(segMem)
					Reaper::Segment(
						fixed,
						segO
						);

		_segments.put(segment);
		}

	return false;
	}

void	Reaper::transmitSegments() noexcept{
	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif

	for(
		Segment*	segment	= _segments.first();
		segment;
		segment	= _segments.next(segment)
		){
		uint32_t	seq;

		if(segment->_acked){
			continue;
			}

		if(_firstPdu){
			seq	= _firstSequenceNumber;
			_firstPdu	= false;
			}
		else {
			seq	= _kernelApi.allocSequenceNumber();
			}

		if(seq > (_firstSequenceNumber + 8191)){
			// FIXME: die with error.
			Oscl::Error::Info::log(
				"%s: no more sequence numbers.\n",
				OSCL_PRETTY_FUNCTION
				);
			stop();
			return;
			}

		// FIXME: we probably need to queue these
		// PDUs and use a timer to empty that queue
		// one-at-a-time.

		#ifdef DEBUG_TRACE
		Oscl::Error::Info::log(
			"%s: _kernelApi.transmitNetwork(...) START\n",
			OSCL_PRETTY_FUNCTION
			);
		#endif

		_kernelApi.transmitNetwork(
			segment->_pdu,
			_ivIndex,
			seq,
			_unicastAddress,
			_remoteAddress,
			_ttl
			);

		if(_stopping){
			// This can happen in a loop
			// interface situation when
			// the _segments queue has been
			// flushed.
			return;
			}

		#ifdef DEBUG_TRACE
		Oscl::Error::Info::log(
			"%s: _kernelApi.transmitNetwork(...) DONE\n",
			OSCL_PRETTY_FUNCTION
			);
		#endif

		}

	// 200 + 50 * TTL milliseconds
	// This formula is just strange because
	// it could be either:
	//	a. TTL specified by the sender?
	//	b. Default Network TTL?
	//
	// In neither case does it reflect the
	// number of hops from the sender, which
	// appears to be the idea behind the formula.
	//
	// If we assume it is the TTL specified by
	// the sender, then the TTL may be
	// much smaller than the number of hops
	// in a large network. In this case,
	// the reassembling receiver will never
	// get our segments.
	// 
	// If we assume it is the default Network
	// TTL, there is an issue with different
	// TTLs being configured for each Node
	// in the network. If the default TTL
	// for this Node is smaller than the
	// number of hops to the sender, then
	// we will:
	//  1. Be unable to reach the sender.
	//  2. Have a very long ack timer using
	//		resulting in the reassembling
	//		receiver timing out.
	//
	// It is probably better to have a longer
	// retransmit timer to prevent the overloading
	// the network with unnecessary retransmissions.
	//
	// If the timer is shorter than the ack timer
	// of the destination.
	// 
	//
	// Here, we use the smaller off the network TTL
	// or the network TTL minus the received TTL.

	_timerApi.restart(200 + (50 * _ttl));
	}

	// There are two types of Lower Transport Layers
	// distinguished by the CTL field of the Network PDU:
	//
	// 1. Lower Transport Access Layer
	// 2. Lower Transport Control
	//
	// Access Layer messages are forwarded to
	// an Element containing the Lower Transport Layer?
	//
	// The first octet of a Lower Transport Access message contains:
	//   [7]	SEG (segmented or unsegmented)
	//   [6]	AKF (application/device key)
	//   [5:0]	AID (application or device key 0b000000)
	//
	// The first octet of a a Lower Transport message contains:
	//	[7]		SEG (segmented or unsegmented)
	//	[6:0]	Opcode
	//		0b0000000	- Segment Acknowledge (Unsegmented only)
	//		0b0000001	- Friend Poll
	//		0b0000010	- Friend Update
	//		0b0000011	- Friend Request (LPN TX: dest: <all-friends-group-address>, src:?)
	//		0b0000100	- Friend Offer
	//		0b0000101	- Friend Clear
	//		0b0000110	- Friend Clear Confirm
	//		0b0000111	- Friend Subscription List Add
	//		0b0001000	- Friend Subscription List Remove
	//		0b0001001	- Friend Subscription List Confirm
	//		0b0001010	- Heartbeat
	//
	//	All transport control messages originated by a Friend node shall be sent as
	//	Unsegmented Control messages with the SRC field set to the unicast address
	//	of the primary element of the Friend node.
	//
	//	All transport control messages originated by a Low Power node shall be sent as
	//	Unsegmented Control messages with the SRC field set to the unicast address
	//	of the primary element of the node that supports the Low Power feature.
	//

	//	Segment Acknowledgement message:
	//	[0]
	//		[7]		SEG	(0)
	//		[6:0]	0b0000000
	//	[1]
	//		[7]		OBO
	//			0	Direct
	//			1	Friend On Behalf Of LPN (Friend queued message for LPN)
	//		[6:0]	SeqZero MSBs
	//	[2]
	//		[7:2]	SeqZero LSBs
	//		[1:0]	RFU (tx as 0b00)
	//	[3:6]	BlockAck, 32-bit block mask (BE)
	//		[n]		Segment n ACK, Range 0 <= n <= 31

	//	Segmented Access Message:
	//	[0]
	//		[7]		SEG	(1)
	//		[6]		AKF
	//		[5:0]	AID
	//	[1]
	//		[7]		SZMIC, Size of TransMIC (0:32-bit, 1:64-bit)
	//		[6:0]	SeqZero MSBs
	//	[2]
	//		[7:2]	SeqZero LSBs
	//		[1:0]	SegO MSBs
	//	[3]
	//		[7:5]	SegO LSBs
	//		[4:0]	SegN
	//	[4:n]	Range: 8:96 bits, 1:12 octets
	//		[7:5]	Segment m
	//
	//	Every Segmented Access message for the same Upper Transport Access PDU
	//	shall have the same values for AKF, AID, SZMIC, SeqZero, and SegN.


	//	Segmented Control Message:
	//	[0]
	//		[7]		SEG	(1)
	//		[6:0]	Opcode, Rang 0x01:0x7F, (0x00 (0b0000000) is reserved and ignored on RX)
	//	[1]
	//		[7]		RFU (tx as 0) No encrypted Payload or MIC in Control PDU
	//		[6:0]	SeqZero MSBs
	//	[2]
	//		[7:2]	SeqZero LSBs
	//		[1:0]	SegO MSBs
	//	[3]
	//		[7:5]	SegO LSBs
	//		[4:0]	SegN
	//	[4:n]	Range: 8:64 bits, 1:8 octets
	//		[7:5]	Segment m
	//
	//	Every Segmented Control message for the same Upper Transport Control PDU
	//	shall have the same values for Opcode, SeqZero, and SegN.

	//	The lower transport layer shall only transmit Segmented Access messages
	//	or Segmented Control messages for a single Upper Transport PDU to the
	//	same *destination* at a time.
	//
	//	Hmmm... this implies that there is a special "layer" between the
	//	Element (src address) and the Network layer that has a queue for
	//	each destination address.
	//
	//	Does this rule apply across sub-networks?
	//

	//	Non-Unicast Destination Addresses
	//
	//	How are broadcast/group/virtual addresses treated? Can those even be
	//	segmented?
	//
	//	3.5.3.3 Segmentation behavior
	//
	//		Once an Upper Transport PDU has been segmented, the lower transport layer
	//		will transmit the initial Lower Transport PDUs of each segment of that message.
	//
	//	*	If the message is destined to a unicast address, then the lower transport layer
	//		will expect a Segment Acknowledgment message from that node, or from a Friend
	//		node on behalf of that node.
	//
	//		If the message is addressed to a virtual or group address, then Segment Acknowledgment
	//		messages will not be sent by those devices.
	//
	//	*	If the Lower Transport PDUs of the segmented Upper Transport PDU are being sent
	//		to a group address or a virtual address, then the lower transport layer shall
	//		send all Lower Transport PDUs of the segmented Upper Transport PDU.
	//
	//		It is recommended to send all Lower Transport PDUs multiple times, introducing
	//		small random delays between repetitions.
	//
	//			Note:The Upper Transport PDU sent to a group or virtual address is not
	//			acknowledged by recipients, thus the message delivery status is unknown
	//			and should be considered to be unacknowledged. The behavior recommended
	//			above is designed to significantly increase the probability of the successful
	//			delivery of the segmented Upper Transport PDU.

	//	A message sent to the all-proxies address shall be processed by the primary element
	//	of all nodes that have the proxy functionality enabled.
	//
	//	A message sent to the all-friends address shall be processed by the primary element
	//	of all nodes that have the friend functionality enabled.
	//
	//	A message sent to the all-relays address shall be processed by the primary element
	//	of all nodes that have the relay functionality enabled.
	//
	//	A message sent to the all-nodes address shall be processed by the primary element
	//	of all nodes.

	//	SeqAuth Notes:
	//
	//		647262	= SEQ
	//		645272	= SEQ - 8191
	//		  1849	= SegZero
	//		647262	= SegZero + SEQ
	//		646000	= Masked = SEQ & ~1FFF
	//		647849	= Masked + SegZero
	//
	//		Answer:
	//		645849
	//
	//		  2000	= Answer ^ (Masked + SegZero)
	//
	//		IvIndexSEQ = 0x58437AF2 || 0x647262 = 0x58437AF2647262
	//
	//		SeqAuth = ((IvIndexSEQ - 0x1FFF) & 0x1FFF) + SeqZero

	//	AppKey encryption parameters:
	//
	//		Application Nonce
	//			The nonce uses the sequence number and the source address, ensuring
	//			that two different nodes cannot use the same nonce. The IV Index is
	//			used to provide significantly more nonce values than the sequence
	//			number can provide for a given node. Management of the IV Index is
	//			described in Section 3.10.5.
	//
	//			Note: The authentication and encryption of the access payload is not
	//				dependent on the TTL value, meaning that as the access payload
	//				is relayed through a mesh network, the access payload does not
	//				need to be re-encrypted at each hop.
	//
	//			Application Nonce
	//			[0]		Nonce Type		0x01
	//			[1]		ASZMIC and Pad	0b10000000 (bit 7 is ASZMIC)
	//			[2:4]	SEQ (24 lowest bits of SeqAuth)
	//			[5:6]	Source Address
	//			[7:8]	Destination Address
	//			[9:12]	IV Index
	//
	//		Application Key

bool	Reaper::receive(
			bool		ctl,
			uint8_t		ttl,
			uint16_t	src,
			uint32_t	seq,
			const void*	frame,
			unsigned	length
			) noexcept{

	// We don't expect *any*
	// non-control segments.

	return false;
	}

bool	Reaper::receiveControl(
			uint16_t    srcAddress,
			uint16_t    dstAddress,
			uint32_t    seq,
			const void* frame,
			unsigned    length
			) noexcept{

//	return false; // Uncomment to test retransmit.

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s:\n"
		"\tsrcAddress: 0x%4.4X:\n"
		"\tdstAddress: 0x%4.4X:\n"
		"\t_remoteAddress: 0x%4.4X:\n"
		"\t_unicastAddress: 0x%4.4X:\n"
		"\tseq: 0x%8.8X:\n"
		"",
		OSCL_PRETTY_FUNCTION,
		srcAddress,
		dstAddress,
		_remoteAddress,
		_unicastAddress,
		seq
		);
	Oscl::Error::Info::hexDump(
		frame,
		length
		);
	#endif

	if(!_expectACK){
		// Not for me
		#ifdef DEBUG_TRACE
		Oscl::Error::Info::log(
			"%s: ACK not expected.\n",
			OSCL_PRETTY_FUNCTION
			);
		#endif
		return false;
		}

	if(srcAddress != _remoteAddress){
		return false;
		}

	if(dstAddress != _unicastAddress){
		return false;
		}

	// Here, we're looking for Segment Acknowledgements

	_retryCounter	= 5;

	Oscl::Endian::Decoder::Linear::Part
	beDecoder(
		frame,
		length
		);

	Oscl::Decoder::Api&
	decoder	= beDecoder.be();

	uint8_t		opcode;
	uint16_t	seqZeroEtc;
	uint32_t	blockAck;

	decoder.decode(opcode);
	decoder.decode(seqZeroEtc);
	decoder.decode(blockAck);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow.\n",
			OSCL_PRETTY_FUNCTION
			);
		return false;
		}
	/*
		If a Segment Acknowledgment message OBO field is set to 0,
		and the DST field is set to the unicast address of this element,
		and the SeqZero field is set to SeqZero of the segmented message,
		and the SRC field is set to the destination of the segmented message,
		the Segment Acknowledgment message is a valid acknowledgment for the
		segmented message.

		If a Segment Acknowledgment message OBO field is set to 1,
		and the DST field is set to the unicast address of this element,
		and the SeqZero field is set to SeqZero of the segmented message,
		the Segment Acknowledgment message is a valid acknowledgment for the
		segmented message.
		For a given SeqAuth, only the Segment Acknowledgment messages from
		the first SRC address received should be considered valid.

		If a Segment Acknowledgment message is received that is a valid acknowledgment
		for the segmented message, then the lower transport layer shall
		reset the segment transmission timer
		and retransmit all unacknowledged Lower Transport PDUs.
		If a Segment Acknowledgment message is received that acknowledges all
		Lower Transport PDUs for the Upper Transport PDU,
		then the Upper Transport PDU is complete.
		If a Segment Acknowledgment message with the BlockAck field set to 0x00000000
		is received, then the Upper Transport PDU shall be immediately cancelled
		and the higher layers shall be notified that the Upper Transport PDU
		has been cancelled.
	 */

	if(opcode != 0x00){
		// Opcode is NOT a Segment ACK
		#ifdef DEBUG_TRACE
		Oscl::Error::Info::log(
			"%s: Opcode is NOT a Segment ACK.\n",
			OSCL_PRETTY_FUNCTION
			);
		#endif
		return false;
		}

	if(dstAddress != _unicastAddress){
		// Not destined for me.
		#ifdef DEBUG_TRACE
		Oscl::Error::Info::log(
			"%s: dst(0x%4.4X) != _unicastAddress(0x%4.4X)\n",
			OSCL_PRETTY_FUNCTION,
			dstAddress,
			_unicastAddress
			);
		#endif
		return false;
		}

	uint16_t
	seqZero	= (seqZeroEtc & 0x7FFF) >> 2;

	if(seqZero != _seqZero){
		// Not for me.
		#ifdef DEBUG_TRACE
		Oscl::Error::Info::log(
			"%s: seqZero (%u) != _seqZero (%u).\n",
			OSCL_PRETTY_FUNCTION,
			seqZero,
			_seqZero
			);
		#endif
		return false;
		}

	// We now have a valid Segment Acknowledge Message.

	_obo	|= (seqZeroEtc >> 15)?true:false;	// Propagate to upper layer on success

	if(!blockAck){
		/* 3.5.3.3 Segmentation behavior

			If a Segment Acknowledgment message with the BlockAck field set
			to 0x00000000 is received, then the Upper Transport PDU shall
			be immediately cancelled and the higher layers shall be notified that
			the Upper Transport PDU has been cancelled.
		*/

		if(_completeApi){
			_completeApi->failedBusy();
			}

		stop();

		return true;
		}

	bool	allAcked	= true;

	for(
		Segment*	segment	= _segments.first();
		segment;
		segment	= _segments.next(segment)
		){

		if(segment->_acked){
			continue;
			}

		uint8_t
		segO	= segment->_segO;

		if((1<<segO) & blockAck){
			segment->_acked	= true;
			continue;
			}
		allAcked	= false;
		}

	if(allAcked){
		/* 3.5.3.3 Segmentation behavior

			If a Segment Acknowledgment message is received that acknowledges
			all Lower Transport PDUs for the Upper Transport PDU, then the Upper
			Transport PDU is complete.
		*/
		if(_completeApi){
			if(_obo){
				_completeApi->successOnBehalfOfLPN();
				}
			else {
				_completeApi->success();
				}
			}
		stop();
		return true;
		}

	/* 3.5.3.3 Segmentation behavior

		If a Segment Acknowledgment message is received that is a
		valid acknowledgment for the segmented message, then the
		lower transport layer shall reset the segment transmission timer
		and retransmit all unacknowledged Lower Transport PDUs.
	*/

	transmitSegments();

	return true;
	}

