/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_bluetooth_mesh_transport_lower_segment_access_reaperh_
#define _oscl_bluetooth_mesh_transport_lower_segment_access_reaperh_

#include <stdint.h>
#include "oscl/bluetooth/mesh/element/symbiont/kernel/api.h"
#include "oscl/bluetooth/mesh/transport/tx/complete/api.h"
#include "oscl/bluetooth/mesh/element/rx/itemcomp.h"
#include "oscl/queue/queue.h"
#include "oscl/memory/block.h"
#include "oscl/done/operation.h"
#include "oscl/pdu/memory/api.h"
#include "oscl/timer/api.h"
#include "oscl/pdu/memory/api.h"
#include "oscl/inhibitor/scope.h"

/** */
namespace Oscl {
/** */
namespace BT {
/** */
namespace Mesh {
/** */
namespace Transport {
/** */
namespace Lower {
/** */
namespace Segment {
/** */
namespace Access {

/** */
class Reaper: public Oscl::QueueItem {
	public:
		/** */
		class ContextApi {
			public:
				/**  */
				virtual void	stopped(Reaper& part) noexcept=0;
			};

	private:
		/** */
		ContextApi&						_context;

		/** */
		Oscl::Pdu::Memory::Api&			_freeStoreApi;

		/** */
		Oscl::Timer::Api&				_timerApi;

		/** */
		Oscl::BT::Mesh::Element::
		Symbiont::Kernel::Api&			_kernelApi;

		/** */
		Oscl::BT::Mesh::Transport::
		TX::Complete::Api*				_completeApi;

		/** */
		Oscl::Handle<Oscl::Pdu::Pdu>	_pdu;

		/** */
		const uint32_t					_ivIndex;

		/**
			This is initially firstSequenceNumber
			and is used for the first lower transport
			PDU transmitted and incremented for each
			subsequent lower transport PDU that is transmitted.

			OR, perhaps subsequent sequence numbers should be
			allocated from the Element.

			The following requirements may help:

				3.4.6.4 Transmitting a Network PDU

					The SEQ field shall be set by the network layer to
					the sequence number of the element. The sequence
					number shall then be incremented by one for every
					new Network PDU.

				3.6.4.1 Transmitting an access payload

					The upper transport layer shall not transmit a
					new segmented Upper Transport PDU to a given
					*destination* until the previous Upper Transport PDU
					to that destination has been either completed or
					cancelled.

				3.5.3.1 Segmentation

					The lower transport layer shall only transmit
					Segmented Access messages or Segmented Control
					messages for a single Upper Transport PDU to
					the same *destination* at a time.

				3.6.4.1 Transmitting an access payload

					A sequence number (SEQ) shall be allocated to this
					[Upper Transport] message. In the context of a message
					segmented in the lower transport layer, this SEQ
					corresponds to the 24 lowest bits of SeqAuth,
					the sequence number used for authenticating and
					decrypting the access message by the receiver...

			Conclusion:
				The first PDU transmitted to the Network MUST use
				firstSequenceNumber and subsequent segments and
				retransmissions must allocate new sequence numbers
				from the containing Element.
		 */
		const uint32_t					_firstSequenceNumber;

		/**	Least Significant 13-bits of SeqAuth
			used to encrypt/authenticate the
			Upper Transport PDU.
		 */
		const uint16_t					_seqZero;

		/** SRC address of Element */
		const uint16_t					_unicastAddress;

		/** DST address of remote.
			This *may* be a unicast, group,
			or virtual address.
		 */
		const uint16_t					_remoteAddress;

		/** */
		uint8_t							_ttl;

		/** */
		const uint8_t					_opcode;
		
		/** */
		const bool						_szmic;

	private:
		/** */
		Oscl::Inhibitor::Scope			_ivUpdateInhibitor;

		/** */
		Oscl::Inhibitor::Scope			_rxDisableInhibitor;

	private:
		/** */
		class Segment : public Oscl::QueueItem {
			public:
				/** */
				Oscl::Handle<Oscl::Pdu::Pdu>	_pdu;

				/** */
				const uint8_t	_segO;

				/** */
				bool			_acked;

			public:
				/** */
				Segment(
					Oscl::Pdu::Pdu*	pdu,
					uint8_t			segO
					) noexcept:
						_pdu(pdu),
						_segO(segO),
						_acked(false)
						{}
							

				/** */
				~Segment() noexcept{
					_pdu	= 0;
					}
			};

		/** */
		Oscl::Queue<Segment>	_segments;

		/** */
		union SegmentMem {
			/** */
			void*	__qitemlink;
			/** */
			Oscl::Memory::AlignedBlock<sizeof(Segment)>	mem;
			};

		/** */
		constexpr static unsigned	nSegmentMem	= 32;

		/** */
		SegmentMem	_segmentMem[nSegmentMem];

		/** */
		Oscl::Queue<SegmentMem>	_freeSegmentMem;

	private:
		/** */
		Oscl::BT::Mesh::Element::
		RX::ItemComposer<Reaper>			_rxComposer;

	private:
		/** */
		Oscl::Done::Operation<Reaper>	_timerExpired;

	private:
		/** */
		unsigned							_retryCounter;

		/** */
		bool								_expectACK;

		/** */
		bool								_obo;

		/** */
		bool								_firstPdu;

		/** */
		bool								_stopping;

	public:
		/**	Constructor
			param [in] kernelApi		Reference to the subnet
										interface used to transmit
										transport PDUs.
			param [in] completeApi		Optional callback to signal the status
										at the end of the transmission.
			param [in] pdu				The upper transport PDU
			param [in] ivIndex			The IV Index used to encrypt
										the upper transport PDU.
			param [in] remoteAddress	The destination address.
		 */
		Reaper(
			ContextApi&					context,
			Oscl::Inhibitor::Api&		ivUpdateInhibitorApi,
			Oscl::Inhibitor::Api&		rxDisableInhibitorApi,
			Oscl::Pdu::Memory::Api&		freeStoreApi,
			Oscl::Timer::Api&			timerApi,
			Oscl::BT::Mesh::Element::
			Symbiont::Kernel::Api&		kernelApi,
			Oscl::BT::Mesh::Transport::
			TX::Complete::Api*			completeApi,
			Oscl::Pdu::Pdu*				pdu,
			uint32_t					ivIndex,
			uint32_t					firstSequenceNumber,
			uint16_t					unicastAddress,
			uint16_t					remoteAddress,
			uint8_t						ttl,
			uint8_t						aid,
			bool						akf,
			bool						szmic
			) noexcept;

		/** */
		void	start() noexcept;

		/** */
		Oscl::BT::Mesh::Element::RX::Item&	getRxItem() noexcept;

		/** */
		uint16_t	getDestinationAddress() const noexcept;

		/** */
		void	stop() noexcept;

	private:
		/** */
		void	stopNext() noexcept;

	private: // Oscl::Done::Operation _allFreeNotification
		/** */
		void	allFreeNotification() noexcept;

	private: // Oscl::Done::Operation _timerExpired
		/** */
		void	timerExpired() noexcept;

	private:
		/** */
		SegmentMem*	allocSegmentMem() noexcept;

		/** */
		void	freeSegment(Segment* segment);

		/** RETURN: true for out-of-resources failure
		 */
		bool	buildSegments() noexcept;

		/** */
		void	transmitSegments() noexcept;

	public: // Oscl::BT::Mesh::Element::RX::Item	_rxComposer
		/**	This is how we receive segment
			acknowlegements from the Network
			layer.
		 */
		bool	receive(
					bool		ctl,
					uint8_t		ttl,
					uint16_t	src,
					uint32_t	seq,
					const void*	frame,
					unsigned	length
					) noexcept;

		/** */
		bool	receiveControl(
					uint16_t    srcAddress,
					uint16_t    dstAddress,
					uint32_t    seq,
					const void* frame,
					unsigned    length
					) noexcept;

	};

}
}
}
}
}
}
}


#endif
