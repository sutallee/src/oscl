/*
   Copyright (C) 2021 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_bluetooth_mesh_publication_server_parth_
#define _oscl_bluetooth_mesh_publication_server_parth_

#include "oscl/queue/queue.h"
#include "oscl/timer/factory/api.h"
#include "oscl/done/operation.h"
#include "oscl/bluetooth/mesh/publication/persist/api.h"
#include "oscl/bluetooth/mesh/publication/api.h"
#include "oscl/bluetooth/mesh/publication/server/context/api.h"

/** */
namespace Oscl {
/** */
namespace BT {
/** */
namespace Mesh {
/** */
namespace Publication {
/** */
namespace Server {

/** This part encapsulates the publication logic
	for a Bluetooth Mesh Model Server.
 */
class Part : public Oscl::BT::Mesh::Publication::Api {
	private:
		/** */
		Oscl::BT::Mesh::
		Publication::Server::
		Context::Api&				_context;

		/** */
		Oscl::Timer::Factory::Api&	_timerFactoryApi;

		/** */
		Oscl::BT::Mesh::
		Publication::Persist::Api&	_publicationPersistApi;

	private:
		/** */
		Oscl::Timer::Api&			_pubRetransmitTimerApi;

		/** */
		Oscl::Done::Operation<Part>	_pubRetransmitTimerExpired;

	private:
		/** */
		Oscl::Timer::Api&			_pubPeriodicTimerApi;

		/** */
		Oscl::Done::Operation<Part>	_pubPeriodicTimerExpired;

	private:
		/** */
		uint8_t						_remainingTransmitCount;

	public:
		/** */
		Part(
			Oscl::BT::Mesh::
			Publication::Server::
			Context::Api&					context,
			Oscl::Timer::Factory::Api&		timerFactoryApi,
			Oscl::BT::Mesh::
			Publication::Persist::Api&		publicationPersistApi
			) noexcept;


		/** */
		~Part() noexcept;

		/** Prepare for destruction.
		 */
		void	stop() noexcept;

		/** Invoked to begin the process of publishing,
			generally based on a change of state in the
			context.
		 */
		void	publish() noexcept;

		/** */
		uint16_t	getPublicationAppKeyIndex() const noexcept;

	public: // Oscl::BT::Mesh::Publication::Api
		/** RETURN: true if un-supported or failed. */
		bool	setPublicationParameters(
					uint16_t	publishAddress,
					uint16_t	appKeyIndex,
					bool		credentialFlag,
					uint8_t		publishTTL,
					uint8_t		publishPeriod,
					uint8_t		publishRetransmitCount,
					uint8_t		publishRetransmitIntervalSteps
					) noexcept;

		/** RETURN: true if un-supported or failed. */
		bool	setVirtualPulicationParameters(
					const uint8_t	publishAddress[16],
					uint16_t		appKeyIndex,
					bool			credentialFlag,
					uint8_t			publishTTL,
					uint8_t			publishPeriod,
					uint8_t			publishRetransmitCount,
					uint8_t			publishRetransmitIntervalSteps
					) noexcept;

		/** RETURN: true if un-supported. */
		bool	getPulicationParameters(
					uint16_t&	publishAddress,
					uint16_t&	appKeyIndex,
					bool&		credentialFlag,
					uint8_t&	publishTTL,
					uint8_t&	publishPeriod,
					uint8_t&	publishRetransmitCount,
					uint8_t&	publishRetransmitIntervalSteps
					) noexcept;

	private:
		/** */
		void	pubRetransmitTimerExpired() noexcept;

		/** */
		void	pubPeriodicTimerExpired() noexcept;

		/** */
		void	conditionallyStartPeriodicTimer() noexcept;

		/** */
		void	publishCurrentState(bool retransmitting) noexcept;
	};

}
}
}
}
}

#endif
