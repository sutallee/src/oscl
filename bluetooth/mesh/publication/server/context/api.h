/*
   Copyright (C) 2021 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_bluetooth_mesh_publication_server_context_apih_
#define _oscl_bluetooth_mesh_publication_server_context_apih_

#include <stdint.h>

/** */
namespace Oscl {
/** */
namespace BT {
/** */
namespace Mesh {
/** */
namespace Publication {
/** */
namespace Server {
/** */
namespace Context {

/** */
class Api {
	public:
		/**
			This operation is invoked to transmit
			a status message.
			The label may be nil.
			If the label is not nil, then
			the length is 16.
			The retransmitting parameter may be used
			by clients to manage a TID. If not
			retransmitting, the TID should be
			incremented.
		 */
		virtual void	publish(
							uint16_t	appKeyIndex,
							uint16_t	dstAddress,
							uint8_t		ttl,
							const void*	label,
							bool		retransmitting
							) noexcept=0;
	};

}
}
}
}
}
}

#endif
