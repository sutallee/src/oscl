/*
   Copyright (C) 2021 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <new>
#include "part.h"
#include "oscl/error/info.h"
#include "oscl/bluetooth/mesh/address/virtual.h"
#include "oscl/bluetooth/mesh/address/types.h"
#include "oscl/bluetooth/mesh/address/values.h"
#include "oscl/bluetooth/mesh/publication/utils.h"

using namespace Oscl::BT::Mesh::Publication::Server;

//#define DEBUG_TRACE

Part::Part(
	Oscl::BT::Mesh::
	Publication::Server::
	Context::Api&					context,
	Oscl::Timer::Factory::Api&		timerFactoryApi,
	Oscl::BT::Mesh::
	Publication::Persist::Api&		publicationPersistApi
	) noexcept:
		_context(context),
		_timerFactoryApi(timerFactoryApi),
		_publicationPersistApi(publicationPersistApi),
		_pubRetransmitTimerApi(
			*timerFactoryApi.allocate()
			),
		_pubRetransmitTimerExpired(
			*this,
			&Part::pubRetransmitTimerExpired
			),
		_pubPeriodicTimerApi(
			*timerFactoryApi.allocate()
			),
		_pubPeriodicTimerExpired(
			*this,
			&Part::pubPeriodicTimerExpired
			),
		_remainingTransmitCount(0)
		{

	_pubRetransmitTimerApi.setExpirationCallback(
		_pubRetransmitTimerExpired
		);

	_pubPeriodicTimerApi.setExpirationCallback(
		_pubPeriodicTimerExpired
		);

	conditionallyStartPeriodicTimer();
	}

Part::~Part() noexcept{
	_timerFactoryApi.free(_pubRetransmitTimerApi);
	_timerFactoryApi.free(_pubPeriodicTimerApi);
	}

void	Part::stop() noexcept{
	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s:\n",
		__PRETTY_FUNCTION__
		);
	#endif

	_pubRetransmitTimerApi.stop();
	_pubPeriodicTimerApi.stop();
	}

void	Part::publish() noexcept{

	_remainingTransmitCount = _publicationPersistApi.retransmitCount();

	publishCurrentState(false);
	}

void	Part::pubRetransmitTimerExpired() noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif

	if(!_remainingTransmitCount){
		conditionallyStartPeriodicTimer();
		return;
		}

	--_remainingTransmitCount;

	publishCurrentState(true);

	if(_remainingTransmitCount){
		// Restart the timer.
		/*
			The Publish Retransmit Interval Steps state is a 5-bit value
			controlling the interval between retransmissions of a message
			that is published by a model. The state represents the number of
			50 millisecond steps that shall transpire before a message
			that was published by a model is retransmitted.

			The retransmission interval is calculated using the following formula:

				retransmission interval = (Publish Retransmit Interval Steps + 1) * 50

			For example, a value of 0b10000 represents an interval of 850 milliseconds.
		 */
		_pubRetransmitTimerApi.restart(
			(_publicationPersistApi.retransmitIntervalSteps() + 1) * 50
			);

		// Stop the periodic timer while we're retransmitting.
		_pubPeriodicTimerApi.stop();
		}
	else {
		conditionallyStartPeriodicTimer();
		}
	}

void	Part::pubPeriodicTimerExpired() noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif

	if(_remainingTransmitCount){
		// We're currently retransmitting a publication.
		return;
		}

	_remainingTransmitCount	= _publicationPersistApi.retransmitCount();

	publishCurrentState(false);

	conditionallyStartPeriodicTimer();
	}

void	Part::publishCurrentState(bool retransmitting) noexcept{

	/* 4.2.2.1 Publish Address
		The Publish Address state determines the destination
		address in messages sent by a model. The publish
		address shall be the unassigned address, a unicast
		address, a Label UUID, or a group address.

		If the publish address of the model is the unassigned
		address, the model is inactive: it does not send any
		unsolicited messages out and can only send a response
		message to an incoming acknowledged message.
	 */
	if(!_publicationPersistApi.address()){
		// the unassigned address
		return;
		}

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif

	bool
	isVirtualAddress	= Oscl::BT::Mesh::Address::isVirtualAddress(_publicationPersistApi.address());

	_context.publish(
		_publicationPersistApi.appKeyIndex(),	// uint16_t	appKeyIndex,
		_publicationPersistApi.address(),		// uint16_t	dstAddress,
		_publicationPersistApi.ttl(),			// uint8_t		ttl,
		isVirtualAddress?_publicationPersistApi.virtualAddressLabelUUID():0,
		retransmitting
		);

	if(_remainingTransmitCount){
		// Restart the timer.
		/*
			The Publish Retransmit Interval Steps state is a 5-bit value
			controlling the interval between retransmissions of a message
			that is published by a model. The state represents the number of
			50 millisecond steps that shall transpire before a message
			that was published by a model is retransmitted.

			The retransmission interval is calculated using the following formula:

				retransmission interval = (Publish Retransmit Interval Steps + 1) * 50

			For example, a value of 0b10000 represents an interval of 850 milliseconds.
		 */
		_pubRetransmitTimerApi.restart(
			(_publicationPersistApi.retransmitIntervalSteps() + 1) * 50
			);

		// Stop the periodic timer while we're retransmitting.
		_pubPeriodicTimerApi.stop();
		}
	else {
		conditionallyStartPeriodicTimer();
		}
	}

void	Part::conditionallyStartPeriodicTimer() noexcept{

	uint8_t
	periodSteps	= _publicationPersistApi.period();

	unsigned
	periodInMs	= Oscl::BT::Mesh::Publication::periodParameterToMilliseconds(periodSteps);

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s: period: %u ms\n",
		__PRETTY_FUNCTION__,
		periodInMs
		);
	#endif

	if(!periodInMs){
		_pubPeriodicTimerApi.stop();
		return;
		}

	/*	The minimumPublishPeriodInMs is a work-around since
		some un-configurable configuration clients (e.g. SiLabs)
		set the publication period very low, which generates
		way too much unnecessary network traffic. Especially
		for OnOff things that change infrequently.

		Still, the standard wants support for this, which
		(IMHO) only makes sense for sensors. I suppose that
		a simple contact closure using the OnOff Server might
		want to poll the contact. Still, it seems that sending
		the publication in that case would only be done if
		the state changed.

		Ultimately, this should probably be a constructor
		parameter.
	 */
	constexpr unsigned	minimumPublishPeriodInMs	= 60000; // 60s

	if(periodInMs < minimumPublishPeriodInMs){
		periodInMs	= minimumPublishPeriodInMs;
		}

	_pubPeriodicTimerApi.start(periodInMs);
	}

bool	Part::setPublicationParameters(
			uint16_t	publishAddress,
			uint16_t	appKeyIndex,
			bool		credentialFlag,
			uint8_t		publishTTL,
			uint8_t		publishPeriod,
			uint8_t		publishRetransmitCount,
			uint8_t		publishRetransmitIntervalSteps
			) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s:\n"
		"\tpublishAddress: 0x%4.4X\n"
		"\tappKeyIndex: 0x%4.4X\n"
		"\tcredentialFlag: %s\n"
		"\tpublishTTL: 0x%2.2X\n"
		"\tpublishPeriod: 0x%2.2X\n"
		"\tpublishRetransmitCount: 0x%2.2X\n"
		"\tpublishRetransmitIntervalSteps: 0x%2.2X\n"
		"",
		__PRETTY_FUNCTION__,
		publishAddress,
		appKeyIndex,
		credentialFlag?"true":"false",
		publishTTL,
		publishPeriod,
		publishRetransmitCount,
		publishRetransmitIntervalSteps
		);
	#endif

	/*	4.4.1.2.7 Model Publication state
		When the PublishAddress is set to the
		unassigned address, the values of the
		AppKeyIndex, CredentialFlag, PublishTTL,
		PublishPeriod, PublishRetransmitCount, and
		PublishRetransmitIntervalSteps fields shall
		be set to 0x00.
	 */
	_publicationPersistApi.write(
		publishAddress,
		publishAddress?appKeyIndex:0,
		publishAddress?credentialFlag:false,
		publishAddress?publishTTL:0,
		publishAddress?publishPeriod:0,
		publishAddress?publishRetransmitCount:0,
		publishAddress?publishRetransmitIntervalSteps:0
		);

	conditionallyStartPeriodicTimer();

	return false;
	}

bool	Part::setVirtualPulicationParameters(
			const uint8_t	publishAddress[16],
			uint16_t		appKeyIndex,
			bool			credentialFlag,
			uint8_t			publishTTL,
			uint8_t			publishPeriod,
			uint8_t			publishRetransmitCount,
			uint8_t			publishRetransmitIntervalSteps
			) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif

	_publicationPersistApi.write(
		Oscl::BT::Mesh::Address::makeVirtualAddress(publishAddress),
		appKeyIndex,
		credentialFlag,
		publishTTL,
		publishPeriod,
		publishRetransmitCount,
		publishRetransmitIntervalSteps,
		publishAddress
		);

	conditionallyStartPeriodicTimer();

	return false;
	}

bool	Part::getPulicationParameters(
			uint16_t&	publishAddress,
			uint16_t&	appKeyIndex,
			bool&		credentialFlag,
			uint8_t&	publishTTL,
			uint8_t&	publishPeriod,
			uint8_t&	publishRetransmitCount,
			uint8_t&	publishRetransmitIntervalSteps
			) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif

	publishAddress					= _publicationPersistApi.address();
	appKeyIndex						= _publicationPersistApi.appKeyIndex();
	credentialFlag					= _publicationPersistApi.credentialFlag();
	publishTTL						= _publicationPersistApi.ttl();
	publishPeriod					= _publicationPersistApi.period();
	publishRetransmitCount			= _publicationPersistApi.retransmitCount();
	publishRetransmitIntervalSteps	= _publicationPersistApi.retransmitIntervalSteps();

	return false;	// failed
	}

uint16_t	Part::getPublicationAppKeyIndex() const noexcept{
	return _publicationPersistApi.appKeyIndex();
	}

