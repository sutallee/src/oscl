/*
   Copyright (C) 2020 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <new>
#include <string.h>
#include <strings.h>
#include <stdlib.h>
#include <stdio.h>

#include "part.h"
#include "oscl/bluetooth/mesh/address/values.h"
#include "oscl/error/info.h"
#include "oscl/strings/hex.h"
#include "oscl/strings/fixed.h"
#include "oscl/zephyr/fs/file/scope.h"
#include "oscl/zephyr/fs/file/fgets.h"
#include <errno.h>

//#define DEBUG_TRACE

using namespace Oscl::BT::Mesh::Publication::Persist::Zephyr::FS::File;

Part::Part(
	Oscl::Persist::
	Parent::Zephyr::FS::Part&	parent
	) noexcept:
		Base(
			parent,
			"publication"
			),
		_address(Oscl::BT::Mesh::Address::unassigned),
		_appKeyIndex(0),
		_credentialFlag(false),
		_ttl(0),
		_period(0),
		_retransmitCount(0),
		_retransmitIntervalSteps(0),
		_virtualAddressLabelUuidIsValid(false)
		{
	read();
	}

Part::~Part() noexcept{
	}

void	Part::write(
			uint16_t		address,
			uint16_t		appKeyIndex,
			bool			credentialFlag,
			uint8_t			ttl,
			uint8_t			period,
			uint8_t			retransmitCount,
			uint8_t			retransmitIntervalSteps,
			const uint8_t*	virtualAddressLabelUUID
			) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif

	_address					= address;
	_appKeyIndex				= appKeyIndex;
	_credentialFlag				= credentialFlag;
	_ttl						= ttl;
	_period						= period;
	_retransmitCount			= retransmitCount;
	_retransmitIntervalSteps	= retransmitIntervalSteps;

	if(virtualAddressLabelUUID){
		memcpy(
			_virtualAddressLabelUUID,
			virtualAddressLabelUUID,
			sizeof(_virtualAddressLabelUUID)
			);

		_virtualAddressLabelUuidIsValid	= true;
		}
	else {
		_virtualAddressLabelUuidIsValid	= false;
		}

	Base::write();
	}

uint16_t		Part::address() const noexcept{
	return _address;
	}

uint16_t		Part::appKeyIndex() const noexcept{
	return _appKeyIndex;
	}

bool			Part::credentialFlag() const noexcept{
	return _credentialFlag;
	}

uint8_t			Part::ttl() const noexcept{
	return _ttl;
	}

uint8_t			Part::period() const noexcept{
	return _period;
	}

uint8_t			Part::retransmitCount() const noexcept{
	return _retransmitCount;
	}

uint8_t			Part::retransmitIntervalSteps() const noexcept{
	return _retransmitIntervalSteps;
	}

const uint8_t*	Part::virtualAddressLabelUUID() const noexcept{
	return _virtualAddressLabelUuidIsValid?_virtualAddressLabelUUID:0;
	}

static void	convertBinaryToHexAsciiLabel(
				const uint8_t*	label,
				char*			asciiHex
				) noexcept{

	for(unsigned i=0;i<16;++i){
		Oscl::Strings::octetToLowerCaseHexAscii(
			label[i],
			asciiHex[(i*2)],	// msn
			asciiHex[(i*2)+1]	// lsn
			);
		}
	}

void	Part::setDefaultValues() noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif

	_address						= Oscl::BT::Mesh::Address::unassigned;
	_appKeyIndex					= 0;
	_credentialFlag					= false;
	_ttl							= 0;
	_period							= 0;
	_retransmitCount				= 0;
	_retransmitIntervalSteps		= 0;
	_virtualAddressLabelUuidIsValid	= false;
	}

bool	Part::writeValues(Oscl::FS::File::Api& f) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif

	char	labelArray[33];

	labelArray[0]	= '\0';
	labelArray[sizeof(labelArray)-1]	= '\0';

	if(_virtualAddressLabelUuidIsValid){
		convertBinaryToHexAsciiLabel(
			_virtualAddressLabelUUID,
			labelArray
			);
		}

	char	buffer[1024];

	unsigned	n	= 0;

	n	= snprintf(
			buffer,
			sizeof(buffer),
			"address=0x%4.4X\n"
			"appKeyIndex=0x%4.4X\n"
			"credentialFlag=%u\n"
			"ttl=0x%2.2X\n"
			"period=0x%2.2X\n"
			"retransmitCount=0x%2.2X\n"
			"retransmitIntervalSteps=0x%2.2X\n"
			"virtualAddressLabelUUID=%s\n"
			"",
			_address,
			_appKeyIndex,
			_credentialFlag,
			_ttl,
			_period,
			_retransmitCount,
			_retransmitIntervalSteps,
			labelArray
			);

	buffer[sizeof(buffer)-1]	= '\0';

	if(n >= sizeof(buffer)){
		// truncated
		Oscl::Error::Info::log(
			"%s: snprintf() failed buffer too small.\n",
			OSCL_PRETTY_FUNCTION
			);
		return true;
		}

	ssize_t
	writeResult	= f.write(
					buffer,
					n
					);

	if(writeResult < 0){
		Oscl::Error::Info::log(
			"%s: write() failed %d\n",
			OSCL_PRETTY_FUNCTION,
			writeResult
			);
		return true;
		}

	unsigned long
	nWritten	= writeResult;

	if(nWritten < n){
		Oscl::Error::Info::log(
			"%s: write() failed. n: %d\n",
			OSCL_PRETTY_FUNCTION,
			n
			);
		return true;
		}

	return false;
	}

bool	Part::readValues(Oscl::FS::File::Api& f) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif

	uint16_t	address	= 0;
	bool		addressIsValid	= false;

	uint16_t	appKeyIndex	= 0;
	bool		appKeyIndexIsValid	= false;

	bool		credentialFlag	= false;
	bool		credentialFlagIsValid	= false;

	uint8_t		ttl	= 0;
	bool		ttlIsValid	= false;

	uint8_t		period	= 0;
	bool		periodIsValid	= false;

	uint8_t		retransmitCount	= 0;
	bool		retransmitCountIsValid	= false;

	uint8_t		retransmitIntervalSteps	= 0;
	bool		retransmitIntervalStepsIsValid	= false;

	bool		labelUuidIsValid	= false;

	bool		virtualAddressLabelUuidIsValid	= false;

	char			buffer[1024];
	const char*		line;

	while((line = Oscl::Zephyr::FS::File::fgets(buffer,sizeof(buffer),f))){

		if(line[0] == '#'){
			// comment
			continue;
			}

		char*
		assignment	= index(buffer,'=');

		if(!assignment){
			continue;
			}

		assignment[0]	= '\0';

		char*
		value	= &assignment[1];

		char*
		newline = rindex(value,'\n');

		if(newline){
			*newline	= '\0';
			}

		if(!strcmp(line,"address")){
			address			= (uint16_t)strtoul(value,0,0);
			addressIsValid	= true;
			}
		else if(!strcmp(line,"appKeyIndex")){
			appKeyIndex			= (uint16_t)strtoul(value,0,0);
			appKeyIndexIsValid	= true;
			}
		else if(!strcmp(line,"credentialFlag")){
			credentialFlag		= (bool)strtoul(value,0,0);
			credentialFlagIsValid	= true;
			}
		else if(!strcmp(line,"ttl")){
			ttl		= (uint8_t)strtoul(value,0,0);
			ttlIsValid	= true;
			}
		else if(!strcmp(line,"period")){
			period		= (uint8_t)strtoul(value,0,0);
			periodIsValid	= true;
			}
		else if(!strcmp(line,"retransmitCount")){
			retransmitCount		= (uint8_t)strtoul(value,0,0);
			retransmitCountIsValid	= true;
			}
		else if(!strcmp(line,"retransmitIntervalSteps")){
			retransmitIntervalSteps		= (uint8_t)strtoul(value,0,0);
			retransmitIntervalStepsIsValid	= true;
			}
		else if(!strcmp(line,"virtualAddressLabelUUID")){
			
			unsigned
			len	= Oscl::Strings::hexAsciiStringToBinary(
					value,	// hexAscii
					_virtualAddressLabelUUID,
					sizeof(_virtualAddressLabelUUID)
					);

			if((len > 0) && (len !=16)){
				Oscl::Error::Info::log(
					"%s: (len(%u) > 0) && (len !=16), expected zero or 16.\n",
					OSCL_PRETTY_FUNCTION,
					len
					);
				return true;
				}

			virtualAddressLabelUuidIsValid	= (len)?true:false;

			labelUuidIsValid	= true;
			}
		}

	if(!addressIsValid){
		Oscl::Error::Info::log(
			"%s: address is required.\n",
			OSCL_PRETTY_FUNCTION
			);
		return true;
		}

	if(!appKeyIndexIsValid){
		Oscl::Error::Info::log(
			"%s: appKeyIndex is required.\n",
			OSCL_PRETTY_FUNCTION
			);
		return true;
		}

	if(!credentialFlagIsValid){
		Oscl::Error::Info::log(
			"%s: credentialFlag is required.\n",
			OSCL_PRETTY_FUNCTION
			);
		return true;
		}

	if(!ttlIsValid){
		Oscl::Error::Info::log(
			"%s: ttl is required.\n",
			OSCL_PRETTY_FUNCTION
			);
		return true;
		}

	if(!periodIsValid){
		Oscl::Error::Info::log(
			"%s: period is required.\n",
			OSCL_PRETTY_FUNCTION
			);
		return true;
		}

	if(!retransmitCountIsValid){
		Oscl::Error::Info::log(
			"%s: retransmitCount is required.\n",
			OSCL_PRETTY_FUNCTION
			);
		return true;
		}

	if(!retransmitIntervalStepsIsValid){
		Oscl::Error::Info::log(
			"%s: retransmitIntervalSteps is required.\n",
			OSCL_PRETTY_FUNCTION
			);
		return true;
		}

	if(!labelUuidIsValid){
		Oscl::Error::Info::log(
			"%s: virtualAddressLabelUUID is required.\n",
			OSCL_PRETTY_FUNCTION
			);
		return true;
		}

	_address						= address;
	_appKeyIndex					= appKeyIndex;
	_credentialFlag					= credentialFlag;
	_ttl							= ttl;
	_period							= period;
	_retransmitCount				= retransmitCount;
	_retransmitIntervalSteps		= retransmitIntervalSteps;
	_virtualAddressLabelUuidIsValid	= virtualAddressLabelUuidIsValid;

	return false;
	}
