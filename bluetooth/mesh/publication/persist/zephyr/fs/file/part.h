/*
   Copyright (C) 2020 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_bluetooth_mesh_publication_persist_zephyr_fs_file_parth_
#define _oscl_bluetooth_mesh_publication_persist_zephyr_fs_file_parth_

#include "oscl/bluetooth/mesh/publication/persist/api.h"
#include "oscl/persist/parent/zephyr/fs/part.h"
#include "oscl/persist/file/zephyr/fs/base.h"

/** */
namespace Oscl {
/** */
namespace BT {
/** */
namespace Mesh {
/** */
namespace Publication {
/** */
namespace Persist {
/** */
namespace Zephyr {
/** */
namespace FS {
/** */
namespace File {

/** */
class Part :
		private Oscl::Persist::File::Zephyr::FS::Base,
		public Oscl::BT::Mesh::Publication::Persist::Api
		{
	private:
		/** */
		uint16_t	_address;

		/** */
		uint16_t	_appKeyIndex;

		/** */
		bool		_credentialFlag;

		/** */
		uint8_t		_ttl;

		/** */
		uint8_t		_period;

		/** */
		uint8_t		_retransmitCount;

		/** */
		uint8_t		_retransmitIntervalSteps;

		/** */
		uint8_t		_virtualAddressLabelUUID[16];

		/** */
		bool		_virtualAddressLabelUuidIsValid;

	public:
		/** */
		Part(
			Oscl::Persist::
			Parent::Zephyr::FS::Part&	parent
			) noexcept;

		/** */
		~Part() noexcept;

		/** */
		void	write(
				uint16_t		address,
				uint16_t		appKeyIndex,
				bool			credentialFlag,
				uint8_t			ttl,
				uint8_t			period,
				uint8_t			retransmitCount,
				uint8_t			retransmitIntervalSteps,
				const uint8_t*	virtualAddressLabelUUID=0
				) noexcept;

		/** */
		uint16_t		address() const noexcept;

		/** */
		uint16_t		appKeyIndex() const noexcept;

		/** */
		bool			credentialFlag() const noexcept;

		/** */
		uint8_t			ttl() const noexcept;

		/** */
		uint8_t			period() const noexcept;

		/** */
		uint8_t			retransmitCount() const noexcept;

		/** */
		uint8_t			retransmitIntervalSteps() const noexcept;

		/** */
		const uint8_t*	virtualAddressLabelUUID() const noexcept;

	private: // Oscl::Persist::File::Zephyr::FS::Base
		/** */
		void	setDefaultValues() noexcept override;

		/** RETURN: true for failure
		 */
		bool	writeValues(Oscl::FS::File::Api& file) noexcept override;

		/** RETURN: true for failure
		 */
		bool	readValues(Oscl::FS::File::Api& file) noexcept override;
	};

}
}
}
}
}
}
}
}

#endif
