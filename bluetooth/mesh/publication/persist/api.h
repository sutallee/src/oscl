/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as publicationed by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_bluetooth_mesh_publication_persist_apih_
#define _oscl_bluetooth_mesh_publication_persist_apih_

#include <stdint.h>

/** */
namespace Oscl {
/** */
namespace BT {
/** */
namespace Mesh {
/** */
namespace Publication {
/** */
namespace Persist {

/** */
class Api {
	public:
		/** */
		virtual void	write(
							uint16_t		address,
							uint16_t		appKeyIndex,
							bool			credentialFlag,
							uint8_t			ttl,
							uint8_t			period,
							uint8_t			retransmitCount,
							uint8_t			retransmitIntervalSteps,
							const uint8_t*	virtualAddressLabelUUID=0
							) noexcept=0;

		/** */
		virtual uint16_t		address() const noexcept=0;

		/** */
		virtual uint16_t		appKeyIndex() const noexcept=0;

		/** */
		virtual bool			credentialFlag() const noexcept=0;

		/** */
		virtual uint8_t			ttl() const noexcept=0;

		/** */
		virtual uint8_t			period() const noexcept=0;

		/** */
		virtual uint8_t			retransmitCount() const noexcept=0;

		/** */
		virtual uint8_t			retransmitIntervalSteps() const noexcept=0;

		/** */
		virtual const uint8_t*	virtualAddressLabelUUID() const noexcept=0;
	};

}
}
}
}
}

#endif
