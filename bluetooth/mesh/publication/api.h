/*
   Copyright (C) 2021 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_bluetooth_mesh_publication_apih_
#define _oscl_bluetooth_mesh_publication_apih_

/** */
namespace Oscl {
/** */
namespace BT {
/** */
namespace Mesh {
/** */
namespace Publication {

/** This interface defines the interface needed
	to support publication. This is ultimately
	invoked by Foundation Model message
	processing.
 */
class Api {
	public:
		/** RETURN: true if un-supported or failed. */
		virtual bool	setPublicationParameters(
							uint16_t	publishAddress,
							uint16_t	appKeyIndex,
							bool		credentialFlag,
							uint8_t		publishTTL,
							uint8_t		publishPeriod,
							uint8_t		publishRetransmitCount,
							uint8_t		publishRetransmitIntervalSteps
							) noexcept=0;

		/** RETURN: true if un-supported or failed. */
		virtual bool	setVirtualPulicationParameters(
							const uint8_t	publishAddress[16],
							uint16_t		appKeyIndex,
							bool			credentialFlag,
							uint8_t			publishTTL,
							uint8_t			publishPeriod,
							uint8_t			publishRetransmitCount,
							uint8_t			publishRetransmitIntervalSteps
							) noexcept=0;

		/** RETURN: true if un-supported. */
		virtual bool	getPulicationParameters(
							uint16_t&	publishAddress,
							uint16_t&	appKeyIndex,
							bool&		credentialFlag,
							uint8_t&	publishTTL,
							uint8_t&	publishPeriod,
							uint8_t&	publishRetransmitCount,
							uint8_t&	publishRetransmitIntervalSteps
							) noexcept=0;
	};

}
}
}
}

#endif
