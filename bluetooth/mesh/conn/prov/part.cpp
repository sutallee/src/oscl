/*
   Copyright (C) 2020 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <stdint.h>
#include <string.h>

#include "part.h"
#include "oscl/error/fatal.h"
#include "oscl/error/info.h"
#include "oscl/decoder/linear/le/base.h"
#include "oscl/decoder/linear/be/base.h"
#include "oscl/bluetooth/strings/module.h"
#include "oscl/uuid/string.h"
#include "oscl/encoder/le/base.h"
#include "oscl/encoder/be/base.h"
#include "oscl/bluetooth/uuid/base.h"
#include "oscl/bluetooth/att/size.h"
#include "oscl/bluetooth/att/opcode.h"
#include "oscl/bluetooth/att/error.h"

using namespace Oscl::BT::Mesh::Conn::Prov;

//#define DEBUG_TRACE

constexpr unsigned	initialSessionTimeoutInMs	= 10000;

Part::Part(
	ContextApi&								context,
	Oscl::BT::Mesh::Node::Creator::Api&		nodeCreateApi,
	Oscl::BT::Conn::TX::Api&				connTxApi,
	Oscl::Timer::Factory::Api&				timerFactoryApi,
	Oscl::Inhibitor::Api&					controllerDisableInhibitorApi,
	uint16_t								connHandle,
	uint16_t								attMTU,
	bool									oobAvailable,
	uint8_t									outputOobSizeInCharacters
	) noexcept:
		_context(context),
		_nodeCreateApi(nodeCreateApi),
		_timerFactoryApi(timerFactoryApi),
		_connHandle(connHandle),
		_mtu(attMTU),
		_controllerDisableInhibitor(controllerDisableInhibitorApi),
		_sessionTimerExpired(
			*this,
			&Part::sessionTimerExpired
			),
		_sessionTimerApi(
			*timerFactoryApi.allocate()
			),
		_allFreeNotificationComposer(
			*this,
			&Part::allFreeNotification
			),
		_primaryStoppedComposer(
			*this,
			&Part::primaryStopped
			),
		_freeStore(
			"L2CAP",
			&_allFreeNotificationComposer
			),
		_attServerLayer(
			_meshProv,		// Oscl::BT::ATT::Server::RX::Api&     upperLayerApi,
			_l2capLayer,	// Oscl::BT::L2CAP::Channel::TX::Api&  lowerLayerApi,
			_freeStore,		// Oscl::Pdu::Memory::Api&             freeStore,
			attMTU			// uint16_t                            attMTU
			),
		_l2capLayer(
			_connLayer,	// Oscl::BT::Conn::TX::Api&			lowerLayerApi,
			_freeStore	// Oscl::Pdu::Memory::Api&				freeStore,
			),
		_connLayer(
			connTxApi,		// Oscl::BT::Conn::TX::Api&		lowerLayerApi,
			_l2capLayer,	// Oscl::BT::L2CAP::RX::Api&	upperLayerApi
			_freeStore,		// Oscl::Pdu::Memory::Api&		freeStore,
			connHandle		// uint16_t						connectionHandle
			),
		_meshProv(
			*this,
			nodeCreateApi,
			_attServerLayer,
			_mtu,
			oobAvailable,
			outputOobSizeInCharacters
			),
		_attMTU(attMTU),
		_stopping(false),
		_primaryIsStopped(false)
		{
	_l2capLayer.attach(_attServerLayer.getRxItem());

	_sessionTimerApi.setExpirationCallback(_sessionTimerExpired);
	}

Part::~Part() noexcept{
	_timerFactoryApi.free(_sessionTimerApi);
	}

void	Part::start() noexcept{
//	#ifdef DEBUG_TRACE
	#if 1
	Oscl::Error::Info::log(
		"%s: handle: 0x%4.4X\n",
		__PRETTY_FUNCTION__,
		_connHandle
		);
	#endif
	_meshProv.start();

	// FIXME: TEMPORARILY Disable the session timer
//	_sessionTimerApi.start(initialSessionTimeoutInMs);
	}

bool	Part::stop(uint16_t handle) noexcept{
	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s: handle: 0x%4.4X\n",
		__PRETTY_FUNCTION__,
		handle
		);
	#endif

	if(handle != _connHandle){
		#ifdef DEBUG_TRACE
		Oscl::Error::Info::log(
			"%s: (handle != _connHandle)\n",
			__PRETTY_FUNCTION__
			);
		#endif
		return false;
		}

	_sessionTimerApi.stop();

	_stopping	= true;

	_meshProv.stop(_primaryStoppedComposer);

	stopNext();

	return true;
	}

void	Part::sessionTimerExpired() noexcept{
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);

	_context.disconnect(
		_connHandle,
		true
		);
	}

Oscl::BT::Conn::RX::Item&	Part::getRxConnItem() noexcept{
	return _connLayer.getRxItem();
	}

void	Part::allFreeNotification() noexcept{
	if(!_stopping){
		return;
		}

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif

	stopNext();	
	}

void	Part::primaryStopped() noexcept{
	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif

	_primaryIsStopped	= true;

	stopNext();
	}


void	Part::stopNext() noexcept{
	if(!_stopping){
		#ifdef DEBUG_TRACE
		Oscl::Error::Info::log(
			"%s: !_stopping()\n",
			__PRETTY_FUNCTION__
			);
		#endif
		return;
		}

	if(!allMemoryIsFree()){
		#ifdef DEBUG_TRACE
		Oscl::Error::Info::log(
			"%s: !allMemoryIsFree()\n",
			__PRETTY_FUNCTION__
			);
		#endif
		return;
		}

	if(!_primaryIsStopped){
		#ifdef DEBUG_TRACE
		Oscl::Error::Info::log(
			"%s: !_primaryIsStopped()\n",
			__PRETTY_FUNCTION__
			);
		#endif
		return;
		}

	_stopping	= false;

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s: DONE()\n",
		__PRETTY_FUNCTION__
		);
	#endif

	_context.stopped(*this);
	}

bool	Part::allMemoryIsFree() noexcept{
	return	_freeStore.allMemoryIsFree();
	}

void	Part::setAttentionTimer(uint8_t nSeconds) noexcept{
	_context.setAttentionTimer(nSeconds);
	}

bool	Part::oobOutputActionBlinkIsSupported() const noexcept{
	return _context.oobOutputActionBlinkIsSupported();
	}

bool	Part::oobOutputActionBeepIsSupported() const noexcept{
	return _context.oobOutputActionBeepIsSupported();
	}

bool	Part::oobOutputActionVibrateIsSupported() const noexcept{
	return _context.oobOutputActionVibrateIsSupported();
	}

bool	Part::oobOutputActionNumericIsSupported() const noexcept{
	return _context.oobOutputActionNumericIsSupported();
	}

bool	Part::oobOutputActionAlphaNumericIsSupported() const noexcept{
	return _context.oobOutputActionAlphaNumericIsSupported();
	}

void	Part::oobOutputBlink(uint32_t n) noexcept{
	_context.oobOutputBlink(n);
	}

void	Part::oobOutputBeep(uint32_t n) noexcept{
	_context.oobOutputBeep(n);
	}

void	Part::oobOutputVibrate(uint32_t n) noexcept{
	_context.oobOutputVibrate(n);
	}

void	Part::oobOutputNumeric(uint32_t n) noexcept{
	_context.oobOutputNumeric(n);
	}

void	Part::oobOutputAlphaNumeric(const char* s) noexcept{
	_context.oobOutputAlphaNumeric(s);
	}

void	Part::provisioningStopped() noexcept{
	_context.provisioningStopped();
	}

void	Part::disconnect() noexcept{
	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s: handle: 0x%4.4X\n",
		__PRETTY_FUNCTION__,
		_connHandle
		);
	#endif

	_context.disconnect(
		_connHandle,
		false
		);
	}

void	Part::forceDisconnect() noexcept{
	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s: handle: 0x%4.4X\n",
		__PRETTY_FUNCTION__,
		_connHandle
		);
	#endif

	_context.disconnect(
		_connHandle,
		true
		);
	}

