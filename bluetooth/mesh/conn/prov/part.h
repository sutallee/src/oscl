/*
   Copyright (C) 2020 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_bluetooth_mesh_conn_prov_parth_
#define _oscl_bluetooth_mesh_conn_prov_parth_

#include <stdint.h>
#include "oscl/memory/block.h"
#include "oscl/done/operation.h"
#include "oscl/frame/fwd/composer.h"
#include "oscl/pdu/fwd/composer.h"
#include "oscl/timer/api.h"
#include "oscl/pdu/memory/config.h"
#include "oscl/frame/fwd/itemcomp.h"
#include "oscl/endian/decoder/api.h"
#include "part.h"
#include "oscl/bluetooth/mesh/bearer/tx/itemcomp.h"
#include "oscl/timer/factory/api.h"
#include "oscl/bluetooth/l2cap/tx/api.h"
#include "oscl/bluetooth/l2cap/channel/layer/part.h"
#include "oscl/bluetooth/att/server/layer/part.h"
#include "oscl/bluetooth/l2cap/layer/part.h"
#include "oscl/bluetooth/conn/layer/part.h"
#include "oscl/bluetooth/mesh/gatt/prov/part.h"
#include "oscl/inhibitor/scope.h"

/** */
namespace Oscl {
/** */
namespace BT {
/** */
namespace Mesh {
/** */
namespace Conn {
/** */
namespace Prov {

/** */
class Part :
	public Oscl::BT::Mesh::GATT::Prov::Part::ContextApi,
	public Oscl::QueueItem
	{
	public:
		/** */
		class ContextApi {
			public:
				/** */
				virtual void	stopped(Part& connection) noexcept=0;

				/** */
				virtual void	setAttentionTimer(uint8_t nSeconds) noexcept=0;

				/** */
				virtual bool	oobOutputActionBlinkIsSupported() const noexcept=0;

				/** */
				virtual bool	oobOutputActionBeepIsSupported() const noexcept=0;

				/** */
				virtual bool	oobOutputActionVibrateIsSupported() const noexcept=0;

				/** */
				virtual bool	oobOutputActionNumericIsSupported() const noexcept=0;

				/** */
				virtual bool	oobOutputActionAlphaNumericIsSupported() const noexcept=0;

				/** */
				virtual void	oobOutputBlink(uint32_t n) noexcept=0;

				/** */
				virtual void	oobOutputBeep(uint32_t n) noexcept=0;

				/** */
				virtual void	oobOutputVibrate(uint32_t n) noexcept=0;

				/** */
				virtual void	oobOutputNumeric(uint32_t n) noexcept=0;

				/** */
				virtual void	oobOutputAlphaNumeric(const char* s) noexcept=0;

				/** */
				virtual void	provisioningStopped() noexcept=0;

				/** */
				virtual void	disconnect(
									uint16_t	handle,
									bool		force
									) noexcept=0;
			};

	private:
		/** */
		ContextApi&					_context;

		/** */
		Oscl::BT::Mesh::
		Node::Creator::Api&			_nodeCreateApi;

		/** */
		Oscl::Timer::Factory::Api&	_timerFactoryApi;

		/** */
		const uint16_t				_connHandle;

		/** */
		const uint16_t				_mtu;

	private:
		/** */
		Oscl::Inhibitor::Scope		_controllerDisableInhibitor;

	private:
		/** */
		Oscl::Done::Operation<Part>	_sessionTimerExpired;

		/** */
		Oscl::Timer::Api&			_sessionTimerApi;

	private:
		/** */
		Oscl::Done::Operation<Part>	_allFreeNotificationComposer;

		/** */
		Oscl::Done::Operation<Part>	_primaryStoppedComposer;

	private:
		/** */
		constexpr static unsigned	bufferSize	= 128;
		/** */
		constexpr static unsigned	nBuffers	= 32;
		/** */
		constexpr static unsigned	nFixed		= 32;
		/** */
		constexpr static unsigned	nFragment	= 32;
		/** */
		constexpr static unsigned	nComposite	= 32;

		/** */
		Oscl::Pdu::Memory::
		Config<
			bufferSize,
			nBuffers,
			nFixed,
			nFragment,
			nComposite
			>								_freeStore;

	private:
		/** */
		Oscl::BT::ATT::Server::Layer::Part	_attServerLayer;

	private:
		/** */
		Oscl::BT::L2CAP::
		Layer::Part							_l2capLayer;

	private:
		/** */
		Oscl::BT::Conn::
		Layer::Part							_connLayer;

	private:
		/** */
		Oscl::BT::Mesh::GATT::Prov::Part	_meshProv;

	private:
		/** */
		uint16_t							_attMTU;

		/** */
		bool								_stopping;

		/** */
		bool								_primaryIsStopped;

	public:
		/** */
		Part(
			ContextApi&								context,
			Oscl::BT::Mesh::Node::Creator::Api&		nodeCreateApi,
			Oscl::BT::Conn::TX::Api&				connTxApi,
			Oscl::Timer::Factory::Api&				timerFactoryApi,
			Oscl::Inhibitor::Api&					controllerDisableInhibitorApi,
			uint16_t								connHandle,
			uint16_t								attMTU,
			bool									oobAvailable,
			uint8_t									outputOobSizeInCharacters
			) noexcept;

		/** */
		~Part() noexcept;

		/** */
		void	start() noexcept;

		/** RETURN: true if handle matches. */
		bool	stop(uint16_t handle) noexcept;

		/** */
		Oscl::BT::Conn::RX::Item&	getRxConnItem() noexcept;

	private:
		/** */
		void	sessionTimerExpired() noexcept;

	private:
		/** */
		void	stopNext() noexcept;

		/** */
		bool	allMemoryIsFree() noexcept;

	private:
		/** */
		void	allFreeNotification() noexcept;

	private:
		/** */
		void	primaryStopped() noexcept;

	private:
		/**	Raw L2CAP (ACL) packets are received
			using this operation.
			RETURN: The implementation is expected to return true if
			it recognizes the frame type and processes it. This *may*
			be used by the client to halt iteration of a list of
			interfaces of this type.
		 */
		bool	forward(	
					const void*	frame,
					unsigned int	length
					) noexcept;

	private:
		/** RETURN: true for failure. */
		bool	sendErrorResponse(
					uint8_t		opcodeInError,
					uint16_t	attributeHandleInError,
					uint8_t		errorCode
					) noexcept;

		/** RETURN: true for failure. */
		bool	sendAttExchangeMtuResponse(uint16_t mtu) noexcept;

	private: // Oscl::BT::Mesh::GATT::Prov::ContextApi
		/** */
		void	setAttentionTimer(uint8_t nSeconds) noexcept;

		/** */
		bool	oobOutputActionBlinkIsSupported() const noexcept;

		/** */
		bool	oobOutputActionBeepIsSupported() const noexcept;

		/** */
		bool	oobOutputActionVibrateIsSupported() const noexcept;

		/** */
		bool	oobOutputActionNumericIsSupported() const noexcept;

		/** */
		bool	oobOutputActionAlphaNumericIsSupported() const noexcept;

		/** */
		void	oobOutputBlink(uint32_t n) noexcept;

		/** */
		void	oobOutputBeep(uint32_t n) noexcept;

		/** */
		void	oobOutputVibrate(uint32_t n) noexcept;

		/** */
		void	oobOutputNumeric(uint32_t n) noexcept;

		/** */
		void	oobOutputAlphaNumeric(const char* s) noexcept;

		/** */
		void	provisioningStopped() noexcept;

		/** */
		void	disconnect() noexcept;

	public:
		/** */
		void	forceDisconnect() noexcept;
	};

}
}
}
}
}

#endif
