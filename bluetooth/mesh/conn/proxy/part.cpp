/*
   Copyright (C) 2020 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <stdint.h>
#include <string.h>

#include "part.h"
#include "oscl/error/fatal.h"
#include "oscl/error/info.h"
#include "oscl/decoder/linear/le/base.h"
#include "oscl/decoder/linear/be/base.h"
#include "oscl/bluetooth/strings/module.h"
#include "oscl/uuid/string.h"
#include "oscl/encoder/le/base.h"
#include "oscl/encoder/be/base.h"
#include "oscl/bluetooth/uuid/base.h"
#include "oscl/bluetooth/att/size.h"
#include "oscl/bluetooth/att/opcode.h"
#include "oscl/bluetooth/att/error.h"

using namespace Oscl::BT::Mesh::Conn::Proxy;

//#define DEBUG_TRACE

constexpr unsigned	initialSessionTimeoutInMs	= 10000;

Part::Part(
	ContextApi&								context,
	Oscl::BT::Mesh::Bearer::TX::Host::Api&	bearerHostApi,
	Oscl::BT::Mesh::
	Network::Iteration::Api&				networkIterator,
	Oscl::BT::Conn::TX::Api&				connTxApi,
	Oscl::Timer::Factory::Api&				timerFactoryApi,
	Oscl::Inhibitor::Api&					controllerDisableInhibitorApi,
	uint16_t								connHandle,
	uint16_t								attMTU
	) noexcept:
		_context(context),
		_timerFactoryApi(timerFactoryApi),
		_connHandle(connHandle),
		_mtu(attMTU),
		_controllerDisableInhibitor(controllerDisableInhibitorApi),
		_sessionTimerExpired(
			*this,
			&Part::sessionTimerExpired
			),
		_sessionTimerApi(
			*timerFactoryApi.allocate()
			),
		_allFreeNotificationComposer(
			*this,
			&Part::allFreeNotification
			),
		_primaryStoppedComposer(
			*this,
			&Part::primaryStopped
			),
		_freeStore(
			"L2CAP",
			&_allFreeNotificationComposer
			),
		_attServerLayer(
			_meshProxy,		// Oscl::BT::ATT::Server::RX::Api&     upperLayerApi,
			_l2capLayer,	// Oscl::BT::L2CAP::Channel::TX::Api&  lowerLayerApi,
			_freeStore,		// Oscl::Pdu::Memory::Api&             freeStore,
			attMTU			// uint16_t&                           attMTU
			),
		_l2capLayer(
			_connLayer,	// Oscl::BT::Conn::TX::Api&			lowerLayerApi,
			_freeStore	// Oscl::Pdu::Memory::Api&				freeStore,
			),
		_connLayer(
			connTxApi,		// Oscl::BT::Conn::TX::Api&		lowerLayerApi,
			_l2capLayer,	// Oscl::BT::L2CAP::RX::Api&	upperLayerApi
			_freeStore,		// Oscl::Pdu::Memory::Api&		freeStore,
			connHandle		// uint16_t						connectionHandle
			),
		_meshProxy(
			bearerHostApi,
			networkIterator,
			_attServerLayer,
			_mtu
			),
		_attMTU(attMTU),
		_stopping(false),
		_primaryIsStopped(false)
		{
	_l2capLayer.attach(_attServerLayer.getRxItem());

	_sessionTimerApi.setExpirationCallback(_sessionTimerExpired);
	}

Part::~Part() noexcept{
	_timerFactoryApi.free(_sessionTimerApi);
	}

void	Part::start() noexcept{
//	#ifdef DEBUG_TRACE
	#if 1
	Oscl::Error::Info::log(
		"%s: handle: 0x%4.4X\n",
		__PRETTY_FUNCTION__,
		_connHandle
		);
	#endif
	_meshProxy.start();

	// FIXME: TEMPORARILY Disable the session timer
//	_sessionTimerApi.start(initialSessionTimeoutInMs);
	}

bool	Part::stop(uint16_t handle) noexcept{
	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s: handle: 0x%4.4X\n",
		__PRETTY_FUNCTION__,
		handle
		);
	#endif

	if(handle != _connHandle){
		#ifdef DEBUG_TRACE
		Oscl::Error::Info::log(
			"%s: (handle != _connHandle)\n",
			__PRETTY_FUNCTION__
			);
		#endif
		return false;
		}

	_sessionTimerApi.stop();

	_stopping	= true;

	_meshProxy.stop(_primaryStoppedComposer);

	stopNext();

	return true;
	}

void	Part::sessionTimerExpired() noexcept{
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);

	_context.disconnect(
		_connHandle,
		true
		);
	}

Oscl::BT::Conn::RX::Item&	Part::getRxConnItem() noexcept{
	return _connLayer.getRxItem();
	}

bool	Part::isDestinedForProxy(uint16_t address) noexcept{
	return _meshProxy.isDestinedForProxy(address);
	}

void	Part::networkSecurityChanged() noexcept{

	if(_stopping){
		return;
		}

	_meshProxy.networkSecurityChanged();
	}

void	Part::allFreeNotification() noexcept{
	if(!_stopping){
		return;
		}

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif

	stopNext();	
	}

void	Part::primaryStopped() noexcept{
	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif

	_primaryIsStopped	= true;

	stopNext();
	}


void	Part::stopNext() noexcept{
	if(!_stopping){
		#ifdef DEBUG_TRACE
		Oscl::Error::Info::log(
			"%s: !_stopping()\n",
			__PRETTY_FUNCTION__
			);
		#endif
		return;
		}

	if(!allMemoryIsFree()){
		#ifdef DEBUG_TRACE
		Oscl::Error::Info::log(
			"%s: !allMemoryIsFree()\n",
			__PRETTY_FUNCTION__
			);
		#endif
		return;
		}

	if(!_primaryIsStopped){
		#ifdef DEBUG_TRACE
		Oscl::Error::Info::log(
			"%s: !_primaryIsStopped()\n",
			__PRETTY_FUNCTION__
			);
		#endif
		return;
		}

	_stopping	= false;

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s: DONE()\n",
		__PRETTY_FUNCTION__
		);
	#endif

	_context.stopped(*this);
	}

bool	Part::allMemoryIsFree() noexcept{
	return	_freeStore.allMemoryIsFree();
	}

void	Part::forceDisconnect() noexcept{
	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s: handle: 0x%4.4X\n",
		__PRETTY_FUNCTION__,
		_connHandle
		);
	#endif

	_context.disconnect(
		_connHandle,
		true
		);
	}

