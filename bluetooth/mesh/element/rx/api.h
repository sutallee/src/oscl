/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_bluetooth_mesh_element_rx_apih_
#define _oscl_bluetooth_mesh_element_rx_apih_

/** */
namespace Oscl {
/** */
namespace BT {
/** */
namespace Mesh {
/** */
namespace Element {
/** */
namespace RX {

/** 
 */
class Api {
	public:
		/**
			This operation is used to receive a Network PDU
			from the network layer. 

			This is effectively the interface used by the
			network to forward lower-transport layer PDUs
			to the Element.

			RETURN: The implementation is expected to
			return true if it recognizes the frame type
			and processes it. The result will be used
			by the lower layer to determine if it
			has forwarded the PDU to the correct element.

			The implementation MUST return true ONLY
			if the destination address in the PDU matches
			the Element's unicast address.

			param [in] ctl		true if this is a control PDU.
			param [in] ttl		the TTL of the received message.
			param [in] ttl		the time-to-live of the received message.
			param [in] src		the source address of the message.
			param [in] seq		the SEQ of the received message.
			param [in] frame	the received DEST and lower-transport PDU
			param [in] length	the length of the DEST + lower-transport PDU
		 */
		bool	receive(
					bool		ctl,
					uint8_t		ttl,
					uint16_t	src,
					uint32_t	seq,
					const void*	frame,
					unsigned	length
					) noexcept;
	};

}
}
}
}
}

#endif
