/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_bluetooth_mesh_element_parth_
#define _oscl_bluetooth_mesh_element_parth_

#include "oscl/bluetooth/mesh/transport/lower/tx/part.h"
#include "oscl/bluetooth/mesh/element/symbiont/api.h"
#include "oscl/frame/fwd/composer.h"
#include "oscl/bluetooth/mesh/model/rx/item.h"
#include "oscl/queue/queue.h"
#include "oscl/queue/queueitem.h"
#include "oscl/pdu/memory/api.h"
#include "oscl/memory/block.h"
#include "api.h"
#include "oscl/timer/factory/api.h"
#include "oscl/inhibitor/api.h"

/** */
namespace Oscl {
/** */
namespace BT {
/** */
namespace Mesh {
/** */
namespace Element {

/** This interface is used to register an Element with
	the lower layer Mesh Network sub-system.
	The QueueItem link is reserved for use by the
	lower layer Mesh Network host sub-system.
 */
class Part :
	public Oscl::BT::Mesh::Element::Symbiont::Api,
	private Oscl::BT::Mesh::Element::Api
	{
	private:
		/** */
		Oscl::Pdu::Memory::Api&	_freeStoreApi;

	private:
		/** */
		Oscl::BT::Mesh::Element::Symbiont::Kernel::Api*	_kernelApi;

		/** */
		uint16_t	_unicastAddress;

	private:
		/** */
		Oscl::Queue<
			Oscl::BT::Mesh::Model::RX::Item
			>								_models;

	private:
		/** Try it. */
		Oscl::BT::Mesh::Transport::
		Lower::TX::Part					_lowerTransportTx;

	public:
		/** */
		Part(
			Oscl::Pdu::Memory::Api&		freeStoreApi,
			Oscl::BT::Mesh::Element::
			Symbiont::Kernel::Api&		kernelApi,
			Oscl::Timer::Factory::Api&	timerFactoryApi,
			Oscl::Inhibitor::Api&		ivUpdateInhibitorApi,
			Oscl::Inhibitor::Api&		rxDisableInhibitorApi,
			uint16_t					unicastAddressOffset
			) noexcept;

		/** */
		Oscl::BT::Mesh::Element::Api&	getElementApi() noexcept;

		/** */
		Oscl::BT::Mesh::Transport::
		Lower::TX::Api&					getLowerTransportApi() noexcept;

	public:
		/** */
		void	attach(Oscl::BT::Mesh::Model::RX::Item& model) noexcept;

	private: // Oscl::BT::Mesh::Element::Api
		/** RETURN: 0 for failure. */
		Oscl::BT::Mesh::Model::Api*	findSigModel(uint16_t modelID) noexcept;

		/** RETURN: 0 for failure. */
		Oscl::BT::Mesh::Model::Api*	findVendorModel(uint16_t cid,uint16_t modelID) noexcept;

		/** RETURN: true for match */
		bool	match(uint16_t unicastAddress) const noexcept;

	private: // Oscl::BT::Mesh::Element::Symbiont::Api
		/** This operation is invoked once by the host
			when the symbiont is attached.
		 */
		void	start() noexcept;

		/** This operation is invoked once by the host
			when the symbiont is detached. The implementation
			must halt operations and cease using the kernelApi.
		 */
		void	stop() noexcept;

		/** */
		void	rxLowerTransportACK(
					const void*	lowerTransportAckPDU,
					unsigned	pduLength,
					uint32_t	seq,
					uint16_t	src,
					uint16_t	dst
					) noexcept;

		/** This operation is used to determine if
			the Element or any of its Models subscribes
			to the specified destination address.

			param [in]	dst	The destination address in question.

			RETURN: true if the Element wants the destination
			address (dst).
		 */
		bool	wantsDestination(uint16_t dst) noexcept;

		/** Process the decrypted Access Message
			The appKeyIndex is greater than 0x0FFF
			if this message is was decrypted using
			the DevKey.
		 */
		void	processAccessMessage(
					const void*	frame,
					unsigned	length,
					uint16_t	src,
					uint16_t	dst,
					uint16_t	appKeyIndex
					) noexcept;

	private:
		/** */
		bool	subscriptionMatch(uint16_t dst) noexcept;

	};

}
}
}
}

#endif
