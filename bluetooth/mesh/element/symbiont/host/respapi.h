/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_bt_mesh_element_symbiont_host_itc_respapih_
#define _oscl_bt_mesh_element_symbiont_host_itc_respapih_

#include "reqapi.h"
#include "oscl/mt/itc/mbox/clirsp.h"

/** */
namespace Oscl {

/** */
namespace BT {

/** */
namespace Mesh {

/** */
namespace Element {

/** */
namespace Symbiont {

/** */
namespace Host {

/** */
namespace Resp {

/**	This class describes an ITC client interface. The
	interface includes the definition of client response messages
	based on their corresponding server request messages and
	associated payloads, as well as an interface that is to be
	implemented by a client that uses the corresponding server
	asynchronously.
 */
/**	This is the host interface for attaching and detaching a
	symbiont to/from the host. It is to be implemented by the
	host and used by the context.
 */
class Api {
	public:
		/**	This describes a client response message that corresponds
			to the AttachReq message described in the "reqapi.h"
			header file. This AttachResp response message actually
			contains a AttachReq request message.
		 */
		typedef Oscl::Mt::Itc::CliResponse<
					Oscl::BT::Mesh::Element::Symbiont::Host::Req::Api,
					Api,
					Oscl::BT::Mesh::Element::Symbiont::Host::
					Req::Api::AttachPayload
					>		AttachResp;

	public:
		/**	This describes a client response message that corresponds
			to the DetachReq message described in the "reqapi.h"
			header file. This DetachResp response message actually
			contains a DetachReq request message.
		 */
		typedef Oscl::Mt::Itc::CliResponse<
					Oscl::BT::Mesh::Element::Symbiont::Host::Req::Api,
					Api,
					Oscl::BT::Mesh::Element::Symbiont::Host::
					Req::Api::DetachPayload
					>		DetachResp;

	public:
		/** Make the compiler happy. */
		virtual ~Api(){}

		/** This operation is invoked within the client thread
			whenever a AttachResp message is received.
		 */
		virtual void	response(	Oscl::BT::Mesh::Element::Symbiont::Host::
									Resp::Api::AttachResp& msg
									) noexcept=0;

		/** This operation is invoked within the client thread
			whenever a DetachResp message is received.
		 */
		virtual void	response(	Oscl::BT::Mesh::Element::Symbiont::Host::
									Resp::Api::DetachResp& msg
									) noexcept=0;

	};

}
}
}
}
}
}
}
#endif
