/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_bt_mesh_element_symbiont_host_respcomph_
#define _oscl_bt_mesh_element_symbiont_host_respcomph_

#include "respapi.h"

/** */
namespace Oscl {

/** */
namespace BT {

/** */
namespace Mesh {

/** */
namespace Element {

/** */
namespace Symbiont {

/** */
namespace Host {

/** */
namespace Resp {

/**	This template class implements a kind of GOF decorator
	pattern allows the context to employ composition instead
	of inheritance. Frequently, a context may need to implement
	more than one interface that may result in method name
	clashes. This template solves the problem by implementing
	the interface and then invoking member function pointers
	in the context instead. One instance of this object is
	created in the context for each interface of this type used
	in the context. Each instance is constructed such that it
	refers to different member functions within the context.
	The interface of this object is passed to any object that
	requires the interface. When the object invokes any of the
	operations of the API, the corresponding member function
	of the context will subsequently be invoked.
 */
/**	This is the host interface for attaching and detaching a
	symbiont to/from the host. It is to be implemented by the
	host and used by the context.
 */
template <class Context>
class Composer : public Api {
	private:
		/** A reference to the Context.
		 */
		Context&	_context;

	private:
		/**	This message attaches a symbiont to the Mesh Network
			host.
		 */
		void	(Context::*_Attach)(Oscl::BT::Mesh::Element::Symbiont::Host::Resp::Api::AttachResp& msg);

		/**	This message detaches a symbiont from the Mesh Network
			host.
		 */
		void	(Context::*_Detach)(Oscl::BT::Mesh::Element::Symbiont::Host::Resp::Api::DetachResp& msg);

	public:
		/** */
		Composer(
			Context&		context,
			void	(Context::*Attach)(Oscl::BT::Mesh::Element::Symbiont::Host::Resp::Api::AttachResp& msg),
			void	(Context::*Detach)(Oscl::BT::Mesh::Element::Symbiont::Host::Resp::Api::DetachResp& msg)
			) noexcept;

	private:
		/**	This message attaches a symbiont to the Mesh Network
			host.
		 */
		void	response(Oscl::BT::Mesh::Element::Symbiont::Host::Resp::Api::AttachResp& msg) noexcept;

		/**	This message detaches a symbiont from the Mesh Network
			host.
		 */
		void	response(Oscl::BT::Mesh::Element::Symbiont::Host::Resp::Api::DetachResp& msg) noexcept;

	};

template <class Context>
Composer<Context>::Composer(
			Context&		context,
			void	(Context::*Attach)(Oscl::BT::Mesh::Element::Symbiont::Host::Resp::Api::AttachResp& msg),
			void	(Context::*Detach)(Oscl::BT::Mesh::Element::Symbiont::Host::Resp::Api::DetachResp& msg)
			) noexcept:
		_context(context),
		_Attach(Attach),
		_Detach(Detach)
		{
	}

template <class Context>
void	Composer<Context>::response(Oscl::BT::Mesh::Element::Symbiont::Host::Resp::Api::AttachResp& msg) noexcept{
	(_context.*_Attach)(msg);
	}

template <class Context>
void	Composer<Context>::response(Oscl::BT::Mesh::Element::Symbiont::Host::Resp::Api::DetachResp& msg) noexcept{
	(_context.*_Detach)(msg);
	}

}
}
}
}
}
}
}
#endif
