/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_bluetooth_mesh_element_symbiont_apih_
#define _oscl_bluetooth_mesh_element_symbiont_apih_

#include <stdint.h>
#include "oscl/queue/queueitem.h"
#include "oscl/bluetooth/mesh/element/symbiont/kernel/api.h"
#include "oscl/bluetooth/mesh/element/api.h"

/** */
namespace Oscl {
/** */
namespace BT {
/** */
namespace Mesh {
/** */
namespace Element {
/** */
namespace Symbiont {

/** This interface is used to register an Element with
	the lower layer Mesh Network sub-system.
	The QueueItem link is reserved for use by the
	lower layer Mesh Network host sub-system.
 */
class Api : public Oscl::QueueItem {
	public:
		/** This operation is invoked once by the host
			when the symbiont is attached.
		 */
		virtual void	start() noexcept=0;

		/** This operation is invoked once by the host
			when the symbiont is detached. The implementation
			must halt operations and cease using the kernelApi.
		 */
		virtual void	stop() noexcept=0;

		/** */
		virtual void	rxLowerTransportACK(
					const void*	lowerTransportAckPDU,
					unsigned	pduLength,
					uint32_t	seq,
					uint16_t	src,
					uint16_t	dst
					) noexcept=0;

		/** This operation is used to determine if
			the Element or any of its Models subscribes
			to the specified destination address.

			param [in]	dst	The destination address in question.

			RETURN: true if the Element wants the destination
			address (dst).
		 */
		virtual bool	wantsDestination(uint16_t dst) noexcept=0;

		/** */
		virtual Oscl::BT::Mesh::Element::Api&	getElementApi() noexcept=0;

		/** RETURN: true for match */
		virtual bool	match(uint16_t unicastAddress) const noexcept=0;

		/** Process the decrypted Access Message
			The appKeyIndex is greater than 0x0FFF
			if this message is was decrypted using
			the DevKey.
		 */
		virtual void	processAccessMessage(
							const void*	frame,
							unsigned	length,
							uint16_t	src,
							uint16_t	dst,
							uint16_t	appKeyIndex
							) noexcept=0;

	};

}
}
}
}
}

#endif
