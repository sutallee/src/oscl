/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_bluetooth_mesh_element_symbiont_kernel_apih_
#define _oscl_bluetooth_mesh_element_symbiont_kernel_apih_

#include "oscl/pdu/pdu.h"
#include "oscl/frame/fwd/api.h"
#include "oscl/bluetooth/mesh/subscription/register/api.h"

/** */
namespace Oscl {
/** */
namespace BT {
/** */
namespace Mesh {
/** */
namespace Element {
/** */
namespace Symbiont {
/** */
namespace Kernel {

/** This interface specifies all of the 
	symbiont host operations that may be used by 
	the symbiont.
 */
class Api {
	public:
		/** This operation may be used by the symbiont
			to queue a network PDU for transmission on the
			mesh network.
			This is effectively the interface to the
			network used by the lower-transport layer.

			param [in] pdu		Pointer to the lower transport
								PDU. The destination address is
								encoded in the first two octets.
			param [in] ivIndex	IV Index
			param [in] seq		24-bit Sequence number
			param [in] src		Source address
			param [in] dst		Destination address
			param [in] ttl		Optional Time-To-Live value
		 */
		virtual void	transmitNetwork(
							Oscl::Pdu::Pdu* pdu,
							uint32_t		ivIndex,
							uint32_t		seq,
							uint16_t		src,
							uint16_t		dst,
							uint8_t			ttl	= 0xFF
							) noexcept=0;

		/** This operation may be used by the symbiont
			to queue a control PDU for transmission on the
			mesh network.
			This is effectively the interface to the
			network used by the lower-transport layer.

			param [in] pdu		Pointer to the lower transport
								PDU. The destination address is
								encoded in the first two octets.
			param [in] ivIndex	IV Index
			param [in] seq		24-bit Sequence number
			param [in] src		Source address
			param [in] dst		Destination address
			param [in] ttl		Optional Time-To-Live value
		 */
		virtual void	transmitControl(
							Oscl::Pdu::Pdu* pdu,
							uint32_t		ivIndex,
							uint32_t		seq,
							uint16_t		src,
							uint16_t		dst,
							uint8_t			ttl	= 0xFF
							) noexcept=0;

		/** Transmit/queue the fully encrypted/formed network PDU.
		 */
		virtual void	transmitNetworkPDU(
							Oscl::Pdu::Pdu* pdu,
							uint16_t		dst
							) noexcept=0;

		/** */
		virtual uint32_t	allocSequenceNumber() noexcept=0;

		/** */
		virtual uint8_t		getDefaultTTL() const noexcept=0;

		/** */
		virtual Oscl::BT::Mesh::Subscription::Register::Api&
				getSubscriptionRegisterApi() noexcept=0;

		/** This operation is invoked by LPNs to forward
			received upper transport access PDUs to Elements.
		 */
		virtual void	forwardUpperTransportPduToAllElements(
							const void*	upperTransportAccessPdu,
							unsigned	pduLength,
							uint32_t	ivIndex,
							uint32_t	seq,
							uint16_t	src,
							uint16_t	dst,
							uint8_t		aid,
							bool		akf,
							bool		segmented
							) noexcept=0;

	};

}
}
}
}
}
}

#endif
