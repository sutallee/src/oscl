/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <new>
#include "part.h"
#include "oscl/error/info.h"
#include "oscl/decoder/linear/be/base.h"
#include "oscl/encoder/be/base.h"
#include "oscl/bluetooth/strings/module.h"
#include "oscl/bluetooth/mesh/address/types.h"

using namespace Oscl::BT::Mesh::Element;

//#define DEBUG_TRACE

Part::Part(
	Oscl::Pdu::Memory::Api&		freeStoreApi,
	Oscl::BT::Mesh::Element::
	Symbiont::Kernel::Api&		kernelApi,
	Oscl::Timer::Factory::Api&	timerFactoryApi,
	Oscl::Inhibitor::Api&		ivUpdateInhibitorApi,
	Oscl::Inhibitor::Api&		rxDisableInhibitorApi,
	uint16_t					unicastAddress
	) noexcept:
		_freeStoreApi(freeStoreApi),
		_kernelApi(&kernelApi),
		_unicastAddress(unicastAddress),
		_lowerTransportTx(
			freeStoreApi,
			timerFactoryApi,
			kernelApi,
			ivUpdateInhibitorApi,
			rxDisableInhibitorApi,
			unicastAddress
			)
		{
	}

Oscl::BT::Mesh::Element::Api&	Part::getElementApi() noexcept{
	return *this;
	}

Oscl::BT::Mesh::Transport::
Lower::TX::Api&					Part::getLowerTransportApi() noexcept{
	return _lowerTransportTx;
	}

void	Part::attach(Oscl::BT::Mesh::Model::RX::Item& model) noexcept{
	_models.put(&model);
	}

Oscl::BT::Mesh::Model::Api*	Part::findSigModel(uint16_t modelID) noexcept{
	for(
		Oscl::BT::Mesh::Model::RX::Item*	item	= _models.first();
		item;
		item	= _models.next(item)
		){
		Oscl::BT::Mesh::Model::Api*
		model	= item->getSigModelApi(modelID);
		if(model){
			return model;
			}
		}
	return 0;
	}

Oscl::BT::Mesh::Model::Api*	Part::findVendorModel(uint16_t cid,uint16_t modelID) noexcept{
	for(
		Oscl::BT::Mesh::Model::RX::Item*	item	= _models.first();
		item;
		item	= _models.next(item)
		){
		Oscl::BT::Mesh::Model::Api*
		model	= item->getVendorModelApi(cid,modelID);
		if(model){
			return model;
			}
		}
	return 0;
	}

void	Part::start() noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif
	}

void	Part::stop() noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif

	Oscl::BT::Mesh::Model::RX::Item* model;

	while((model = _models.get())){
		model->stop();
		}

	_lowerTransportTx.stop();
	}

void	Part::rxLowerTransportACK(
			const void*	lowerTransportAckPDU,
			unsigned	pduLength,
			uint32_t	seq,
			uint16_t	src,
			uint16_t	dst
			) noexcept{

	if(dst != _unicastAddress){
		// Get out quick.
		// ACKs are always sent to unicast
		// addresses, and it must match
		// this element.
		return;
		}

	_lowerTransportTx.receiveControl(
		src,
		dst,
		seq,
		lowerTransportAckPDU,
		pduLength
		);
	}

bool	Part::match(uint16_t unicastAddress) const noexcept{
	return (unicastAddress == _unicastAddress);
	}

bool	Part::wantsDestination(uint16_t dst) noexcept{
	if(dst == _unicastAddress){
		return true;
		}

	return subscriptionMatch(dst);
	}

void	Part::processAccessMessage(
			const void*	frame,
			unsigned	length,
			uint16_t	src,
			uint16_t	dst,
			uint16_t	appKeyIndex
			) noexcept{
	/*	When we get here, we know that at least one
		model wants the destination address.

		If the destination address (dst) is a unicast
		address, then we just forward to each model.

		If the destination address (dst) is NOT a
		unicast address, then we need to check
		the model's subscription list (wantsDestination())
		and forward to the model only if it matches.
	 */
	for(
		Oscl::BT::Mesh::Model::RX::Item*	item	= _models.first();
		item;
		item	= _models.next(item)
		){
		if(!Oscl::BT::Mesh::Address::isUnicastAddress(dst) && !item->wantsDestination(dst)){
			continue;
			}

		item->processAccessMessage(
				frame,
				length,
				src,
				dst,
				appKeyIndex
				);
		}
	}

bool	Part::subscriptionMatch(uint16_t dst) noexcept{
	for(
		Oscl::BT::Mesh::Model::RX::Item*	item	= _models.first();
		item;
		item	= _models.next(item)
		){
		bool
		match	= item->wantsDestination(dst);
		if(match){
			return true;
			}
		}

	return false;
	}

