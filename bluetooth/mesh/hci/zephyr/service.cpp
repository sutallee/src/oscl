/*
   Copyright (C) 2020 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "oscl/hw/nordic/nrf52840/map.h"
#include "service.h"
#include "oscl/error/info.h"
#include "oscl/error/fatal.h"
#include "oscl/pdu/zephyr/netcopy.h"
#include <bluetooth/hci_raw.h>
#include "oscl/bluetooth/debug/print/parse.h"
#include <string.h>

using namespace Oscl::BT::Mesh::HCI::Zephyr;

Service::Service(
	Oscl::BT::HCI::Base::Part::ContextApi&	context,
	Oscl::Zephyr::Poll::Api&		pollApi,
	Oscl::BT::Mesh::Bearer::
	TX::Host::Api&					bearerHostApi,
	Oscl::BT::Mesh::
	Node::Creator::Api&				nodeCreateApi,
	Oscl::BT::Mesh::
    Network::Iteration::Api&		networkIterator,
	Oscl::Frame::FWD::Api&			provRxApi,
	Oscl::Timer::Api&				hciFramerTimerApi,
	Oscl::Timer::Api&				advTxTimerApi,
	Oscl::Timer::Api&				disconnectTimerApi,
	Oscl::Timer::Factory::Api&		timerFactoryApi,
	struct net_buf_pool&			btNetPool,
	struct k_fifo&					fromBtControllerQ,
	const uint8_t					boardAddress[6],
	const char*						deviceName,
	bool							logTxPackets,
	bool							logRxPackets,
	uint16_t						attMTU,
	bool							oobAvailable,
	uint8_t							outputOobSizeInCharacters
	) noexcept:
		_pollApi(pollApi),
		_btNetPool(btNetPool),
		_fromBtControllerQ(fromBtControllerQ),
		_logTxPackets(logTxPackets),
		_logRxPackets(logRxPackets),
		_openSync(
			*this,
			pollApi.getPostMsgApi()
			),
		_part(
			context,
			bearerHostApi,
			nodeCreateApi,
			networkIterator,
			provRxApi,
			hciFramerTimerApi,
			advTxTimerApi,
			disconnectTimerApi,
			timerFactoryApi,
			*this,	// Oscl::Pdu::FWD::Api
			boardAddress,
			deviceName,
			false, // logTxPackets, log ACL
			logRxPackets,
			attMTU,
			oobAvailable,
			outputOobSizeInCharacters,
			false	// alwaysOnDevice
			),
		_fromBtControllerComp(
			*this,
			&Service::fromControllerStart,
			&Service::fromControllerStop,
			&Service::fromControllerProcess
			)
		{
	}

Oscl::Pdu::FWD::Api&	Service::getProvTxBearerApi() noexcept{
	return _part.getProvTxBearerApi();
	}

Oscl::Pdu::FWD::Api&	Service::getBeaconTxBearerApi() noexcept{
	return _part.getBeaconTxBearerApi();
	}

Oscl::Pdu::FWD::Api&	Service::getMeshTxBearerApi() noexcept{
	return _part.getMeshTxBearerApi();
	}

Oscl::Pdu::FWD::Api&	Service::getAdvGattBearerApi() noexcept{
	return _part.getAdvGattBearerApi();
	}

Oscl::Frame::RF::RX::Host::Api&	Service::getMeshMessageRxHostApi() noexcept{
	return _part.getMeshMessageRxHostApi();
	}

Oscl::Frame::FWD::Host::Api&	Service::getSecureNetworkBeaconRxHostApi() noexcept{
	return _part.getSecureNetworkBeaconRxHostApi();
	}

Oscl::Inhibitor::Api&	Service::getHostControllerDisableInhibitApi() noexcept{
	return _part.getHostControllerDisableInhibitApi();
	}

Oscl::Mt::Itc::Srv::OpenSyncApi&	Service::getOpenSyncApi() noexcept{
	return _openSync;
	}

void	Service::dumpStatus(Oscl::Print::Api& printApi) noexcept{
	_part.dumpStatus(printApi);
	}

void	Service::hciTickle(Oscl::Print::Api& printApi) noexcept{
	_part.hciTickle(printApi);
	}

void	Service::disconnectL2CAP(Oscl::Print::Api& printApi) noexcept{
	_part.disconnectL2CAP(printApi);
	}

void	Service::logging(bool enable) noexcept{
	_part.logging(enable);
	}

bool	Service::gattConnectionIsActive() const noexcept{
	return _part.gattConnectionIsActive();
	}

bool	Service::isDestinedForProxy(uint16_t address) noexcept{
	return _part.isDestinedForProxy(address);
	}

void	Service::networkSecurityChanged() noexcept{
	_part.networkSecurityChanged();
	}

static void logPacket(Oscl::Pdu::Pdu* pdu) noexcept {

	static uint8_t	parseBuffer[1024];

	memset(
		parseBuffer,
		-1,
		sizeof(parseBuffer)
		);

	unsigned
	nRead	= pdu->read(
				parseBuffer,
				sizeof(parseBuffer),	// max,
				0	// offset
				);

	
	Oscl::Bluetooth::Debug::Print::parseHciTxPacket(
		parseBuffer,
		nRead
		);
	}

void	Service::transfer(Oscl::Pdu::Pdu* pdu) noexcept{
	// FIXME: This is where we will
	// copy the pdu into a Zephyr message and
	// send it to the controller.

	struct net_buf*
	buffer	= net_buf_alloc(
				&_btNetPool,
				K_NO_WAIT
				);

	if(!buffer){
		Oscl::Error::Info::log(
			"%s: net_buf_alloc() failed.\n",
			OSCL_PRETTY_FUNCTION
			);
		return;
		}

	Oscl::Pdu::Zephyr::NetCopy	copyVisitor(*buffer);

	// Copy data from the pdu to the net_buf.
	pdu->visit(
			copyVisitor,
			0,	// position
			0,	// firstValid
			~0	// nValid (All)
			);

	if(copyVisitor.overflow()){
		Oscl::Error::Info::log(
			"%s: overflow.\n",
			OSCL_PRETTY_FUNCTION
			);
		return;
		}

	// Neet to set the type according to
	// the type in the pdu.

	if(_logTxPackets){
		logPacket(pdu);
		}

	uint8_t	type;

	type	= net_buf_pull_u8(buffer);

	/*
		Normally, I would use this below:

		bt_buf_set_type(
			buffer,
			BT_BUF_ACL_OUT
			);

		However, C++ doesn't like the Zepher file
		"include/bluetooth/conn.h" that is
		included by "include/bluetooth/buf.h"

		As a result:

		I need to *manually* set the type in the
		user_data field.

		I must, also, define BT_BUF_CMD and BT_BUF_ACL_OUT
		locally instead of using the values in the header.
	 */
	static constexpr uint8_t	BT_BUF_CMD		= 0;
//	static constexpr uint8_t	BT_BUF_EVT		= 1;
	static constexpr uint8_t	BT_BUF_ACL_OUT	= 2;
//	static constexpr uint8_t	BT_BUF_ACL_IN	= 3;

	switch(type){
		case 0x01:	// H4_CMD
			{
			uint8_t*
			userData	= (uint8_t*)net_buf_user_data(buffer);
			userData[0]	= BT_BUF_CMD;
			}
			break;
		case 0x02:	// H4_ACL:
			{
			uint8_t*
			userData	= (uint8_t*)net_buf_user_data(buffer);
			userData[0]	= BT_BUF_ACL_OUT;
			}
			break;
		default:
			for(;;);
		}


	int
	result	= bt_send(buffer);

	if(result){
		// This should never happen unless the
		// packet format is invalid.
		Oscl::Error::Info::log(
			"%s: bt_send() failed. result: %d.\n",
			OSCL_PRETTY_FUNCTION,
			result
			);
		}
	}

void	Service::fromControllerStart(struct k_poll_event& event) noexcept{
	k_poll_event_init(
		&event,
		K_POLL_TYPE_FIFO_DATA_AVAILABLE,	// type
		K_POLL_MODE_NOTIFY_ONLY,	// mode
		&_fromBtControllerQ
		);
	}

void	Service::fromControllerStop() noexcept{
	// We NEVER stop the queue
	}

bool	Service::fromControllerProcess(struct k_poll_event& event) noexcept{

	Oscl::Frame::FWD::Api&
	receiver	= _part.getFrameRxApi();

	for(;;){
		struct net_buf*
		buf	= net_buf_get(
				&_fromBtControllerQ,
				K_NO_WAIT
				);

		if(!buf){
			break;
			}

		static constexpr size_t	maxHciFrameSize	= 128;

		uint8_t	hciFrame[maxHciFrameSize];

		uint8_t*
		userData	= (uint8_t*)net_buf_user_data(buf);

//		static constexpr uint8_t	BT_BUF_CMD		= 0;
		static constexpr uint8_t	BT_BUF_EVT		= 1;
//		static constexpr uint8_t	BT_BUF_ACL_OUT	= 2;
		static constexpr uint8_t	BT_BUF_ACL_IN	= 3;

		// Here, we need to prepend the correct
		// HCI type to the frame based on
		// the internal BT Controller userData.
		switch(userData[0]){
			case BT_BUF_EVT:
				hciFrame[0]	= 0x04;	// H4_EVT
				break;
			case BT_BUF_ACL_IN:
				hciFrame[0]	= 0x02;	// H4_ACL
				break;
			default:
				for(;;);
			}

		size_t
		len	= net_buf_linearize(
				&hciFrame[1],
				sizeof(hciFrame)-1,
             		buf,		// struct net_buf *src,
				0,			// size_t offset,
				SIZE_MAX	// size_t len
				);

		net_buf_unref(buf);

		receiver.forward(
			hciFrame,
			len+1
			);
		}

	return false;
	}

void	Service::startController() noexcept{
	// FIXME: initialize the controller interface (if needed)

	// The Zephyr Bluetooth HCI controller subsystem
	// has two interfaces, both of which uses
	// Zephyr message queues.
	//	- One for HCI transfers from the Host to the Controller
	//	- One for HCI transfers from the Controller to the Host
	//
	// Transfers from the Controller to the Host will be handled
	// by a Oscl::Zephyr::Poll::Api handler that will require
	// a reference to the receive queue.
	//
	// Transfers from the Host to the Controller will require
	// allocation of buffers from the controller and a call
	// to the hci transmitter.

	_pollApi.attach(_fromBtControllerComp);
	}

void	Service::request(Oscl::Mt::Itc::Srv::Open::Req::Api::OpenReq& msg) noexcept{

	startController();

	_part.start();
	_part.getResetApi().sendReset();

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"Nrf52840CLOCK: "
		"Nrf52840CLOCK.lfrcmode: %p, "
		"hfclkstat: 0x%8.8X, "
		"lfclkstat: 0x%8.8X, "
		"lfclksrc: 0x%8.8X"
		"\n",
		&Nrf52840CLOCK.lfrcmode,
		Nrf52840CLOCK.hfclkstat,
		Nrf52840CLOCK.lfclkstat,
		Nrf52840CLOCK.lfclksrc
		);
	#endif

	msg.returnToSender();
	}

