/*
   Copyright (C) 2020 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_bluetooth_mesh_hci_zephyr_serviceh_
#define _oscl_bluetooth_mesh_hci_zephyr_serviceh_

#include "oscl/bluetooth/hci/base/part.h"
#include "oscl/zephyr/poll/api.h"
#include "oscl/zephyr/poll/handler.h"
#include "oscl/mt/itc/srv/open.h"
#include <net/buf.h>

/** */
namespace Oscl {
/** */
namespace BT {
/** */
namespace Mesh {
/** */
namespace HCI {
/** */
namespace Zephyr {

/** */
class Service:
	private Oscl::Mt::Itc::Srv::Open::Req::Api,
	private Oscl::Pdu::FWD::Api
	{
	private:
		/** */
		Oscl::Zephyr::Poll::Api&	_pollApi;

		/** */
		struct net_buf_pool&		_btNetPool;

		/** */
		struct k_fifo&				_fromBtControllerQ;

		/** */
		const bool					_logTxPackets;

		/** */
		const bool					_logRxPackets;

	private:
		/** */
		Oscl::Mt::Itc::Srv::OpenSync	_openSync;

		/** */
		Oscl::BT::HCI::Base::Part		_part;

	private:
		/** */
		Oscl::Zephyr::Poll::
		HandlerComposer<Service>		_fromBtControllerComp;

	public:
		/** */
		Service(
			Oscl::BT::HCI::Base::Part::ContextApi&	context,
			Oscl::Zephyr::Poll::Api&		pollApi,
			Oscl::BT::Mesh::Bearer::
			TX::Host::Api&					bearerHostApi,
			Oscl::BT::Mesh::
			Node::Creator::Api&				nodeCreateApi,
			Oscl::BT::Mesh::
    		Network::Iteration::Api&		networkIterator,
			Oscl::Frame::FWD::Api&			provRxApi,
			Oscl::Timer::Api&				hciFramerTimerApi,
			Oscl::Timer::Api&				advTxTimerApi,
			Oscl::Timer::Api&				disconnectTimerApi,
			Oscl::Timer::Factory::Api&		timerFactoryApi,
			struct net_buf_pool&			btNetPool,
			struct k_fifo&					fromBtControllerQ,
			const uint8_t					boardAddress[6],
			const char*						deviceName,
			bool							logTxPackets,
			bool							logRxPackets,
			uint16_t						attMTU,
			bool							oobAvailable,
			uint8_t							outputOobSizeInCharacters
			) noexcept;

		/** */
		Oscl::Pdu::FWD::Api&	getProvTxBearerApi() noexcept;

		/** */
		Oscl::Pdu::FWD::Api&	getBeaconTxBearerApi() noexcept;

		/** */
		Oscl::Pdu::FWD::Api&	getMeshTxBearerApi() noexcept;

		/** */
		Oscl::Pdu::FWD::Api&	getAdvGattBearerApi() noexcept;

		/** */
		Oscl::Frame::RF::RX::Host::Api&	getMeshMessageRxHostApi() noexcept;

		/** */
		Oscl::Frame::FWD::Host::Api&	getSecureNetworkBeaconRxHostApi() noexcept;

		/** */
		Oscl::Inhibitor::Api&	getHostControllerDisableInhibitApi() noexcept;

		/** */
		Oscl::Mt::Itc::Srv::OpenSyncApi&	getOpenSyncApi() noexcept;

		/** */
		bool	gattConnectionIsActive() const noexcept;

		/** */
		bool	isDestinedForProxy(uint16_t address) noexcept;

		/** */
		void	networkSecurityChanged() noexcept;

	public:
		/** */
		void	dumpStatus(Oscl::Print::Api& printApi) noexcept;

		/** */
		void	hciTickle(Oscl::Print::Api& printApi) noexcept;

		/** */
		void	disconnectL2CAP(Oscl::Print::Api& printApi) noexcept;

		/** */
		void	logging(bool enable) noexcept;

	public: // Oscl::Frame::FWD::Host::Api
		/** */
		void	attach(Oscl::Frame::FWD::Item& network) noexcept;

		/** */
		void	detach(Oscl::Frame::FWD::Item& network) noexcept;

	private: // Oscl::Pdu::FWD::Api
		/** */
		void	transfer(Oscl::Pdu::Pdu* pdu) noexcept;

	private: // Oscl::Zephyr::Poll::Handler
		/**	This  operation is invoked to allow the implementation to
			add its file descriptors to the appropriates file descriptor
			sets. This operation is only invoked when the operation
			is established.
		 */
		void	fromControllerStart(struct k_poll_event& event) noexcept;

		/**	When this  operation is invoked the implementation must
			clear the file descriptor from the next sets. This is
			usually done when this IO handler is removed from the
			select handler.
		 */
		void	fromControllerStop() noexcept;

		/**	This operation is invoked whenever the select returns
			and the state of the event indicates that it is pending.
			The implementation then performs any I/O based on the
			event type and resets the state of the event.

			RETURN: The handler returns TRUE if it wishes to be
			removed from the event list.
			If the handler returns TRUE, the stop() operation
			of the handler will be invoked immediately after
			this operation returns.

			The handler returns FALSE if it wishes to remain
			attached to the event list.
		 */
		bool	fromControllerProcess(struct k_poll_event& event) noexcept;

	private:
		/** */
		void	startController() noexcept;

	private:
		/** */
		void	request(Oscl::Mt::Itc::Srv::Open::Req::Api::OpenReq& msg) noexcept;

	};

}
}
}
}
}

#endif
