/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as publicationed by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_bluetooth_mesh_device_persist_apih_
#define _oscl_bluetooth_mesh_device_persist_apih_

#include <stdint.h>

/** */
namespace Oscl {
/** */
namespace BT {
/** */
namespace Mesh {
/** */
namespace Device {
/** */
namespace Persist {

/** */
class Api {
	public:
		/** */
		virtual void	write(
							const uint8_t*	deviceUUID,
							const uint8_t*	boardAddress
							) noexcept=0;

		/** */
		virtual const uint8_t*	deviceUUID() const noexcept=0;

		/** */
		virtual const uint8_t*	boardAddress() const noexcept=0;

	};

}
}
}
}
}

#endif
