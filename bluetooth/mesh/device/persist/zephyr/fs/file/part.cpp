/*
   Copyright (C) 2020 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <new>
#include <string.h>
#include <strings.h>

#include "part.h"
#include "oscl/bluetooth/mesh/address/values.h"
#include "oscl/error/fatal.h"
#include "oscl/error/info.h"
#include "oscl/strings/hex.h"
#include "oscl/strings/fixed.h"
#include "oscl/uuid/string.h"
#include "oscl/uuid/generator.h"
#include "oscl/entropy/rand.h"
#include "oscl/zephyr/fs/file/scope.h"
#include "oscl/zephyr/fs/file/fgets.h"
#include <errno.h>

using namespace Oscl::BT::Mesh::Device::Persist::Zephyr::FS::File;

// #define DEBUG_TRACE

Part::Part(
	Oscl::Persist::
	Parent::Zephyr::FS::Part&	parent
	) noexcept:
		_parent(parent)
		{
	read();
	}

Part::~Part() noexcept{
	destroyDeviceFile();
	}

void	Part::destroyDeviceFile() noexcept{
	Oscl::Strings::Fixed<1024>	path;

	_parent.buildPath(path);

	path	+= "device";

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s: %s\n",
		__PRETTY_FUNCTION__,
		path.getString()
		);
	#endif

	if(path.truncated()){
		Oscl::Error::Info::log(
			"%s: trunacated path: %s\n",
			OSCL_PRETTY_FUNCTION,
			path.getString()
			);
		}

	int
	result	= fs_unlink(path.getString());

	if(result){
		Oscl::Error::Info::log(
			"%s fs_unlink() failed: %d\n",
			OSCL_PRETTY_FUNCTION,
			result
			);
		}
	}

bool	Part::read() noexcept {
	Oscl::Strings::Fixed<1024>	path;

	_parent.buildPath(path);

	path	+= "device";

	if(path.truncated()){
		Oscl::Error::Info::log(
			"%s: trunacated path: %s\n",
			OSCL_PRETTY_FUNCTION,
			path.getString()
			);
		}

	Oscl::Zephyr::FS::File::Scope
		f(	path.getString(),
			false	// create
			);

	bool	fileDoesNotExist	= false;

	if(f.openResult()) {
		// The open failed.
		// We will assume that the file
		// does not exist.
		fileDoesNotExist	= true;
		}

	if(fileDoesNotExist){
		// This is "normal" if the model
		// device parameters have never
		// been set. The defaults will be
		// used.
		#ifdef DEBUG_TRACE
		Oscl::Error::Info::log(
			"%s: fopen(%s) failed. errno: %d\n",
			OSCL_PRETTY_FUNCTION,
			path.getString(),
			errno
			);
		#endif

		Oscl::UUID::Generator::random(
			_deviceUUID
			);

		Oscl::Entropy::Random::fetch(
			_boardAddress,
			sizeof(_boardAddress)
			);

		/*
			The two most significant bits of the static
			address shall be equal to ‘1’

			FIXME: I'm not really sure about the byte
			order. Also, IEEE likes to specify bits in
			an odd way. Therefore, I will "simply" cover
			all of the bases in the next lines.

			Furthermore, the address MUST NOT be all ones.
		 */
		_boardAddress[0]	|= 0xC3;
		_boardAddress[5]	|= 0xC3;
		_boardAddress[3]	&= 0xF7; // ensure we have a zero bit.

		write(
			_deviceUUID,
			_boardAddress
			);

		return true;
		}

	bool		labelUuidIsValid	= false;
	bool		boardAddressIsValid	= false;

	char			buffer[1024];
	const char*		line;

	while((line = Oscl::Zephyr::FS::File::fgets(buffer,sizeof(buffer),f))){

		#ifdef DEBUG_TRACE
		Oscl::Error::Info::log(
			"%s: fgets(): \"%s\"\n",
			OSCL_PRETTY_FUNCTION,
			line
			);
		#endif

		if(line[0] == '#'){
			// comment
			continue;
			}

		char*
		assignment	= index(buffer,'=');

		if(!assignment){
			continue;
			}

		assignment[0]	= '\0';

		char*
		value	= &assignment[1];

		char*
		newline = rindex(value,'\n');

		if(newline){
			*newline	= '\0';
			}

		if(!strcmp(line,"deviceUUID")){
			bool
			failed	= Oscl::UUID::String::stringToUUID(
						_deviceUUID,
						value
						);

			if(failed){
				Oscl::Error::Info::log(
					"%s: Invalid UUID string.\"%s\"\n",
					OSCL_PRETTY_FUNCTION,
					value
					);

				return true;
				}

			labelUuidIsValid	= true;
			}
		else if(!strcmp(line,"boardAddress")){
			unsigned
			len	= Oscl::Strings::hexAsciiStringToBinary(
					value,
					_boardAddress,
					sizeof(_boardAddress)
					);

			if(len != 6){
				Oscl::Error::Info::log(
					"%s: bad boardAddress length (%u), should be 6.\n",
					OSCL_PRETTY_FUNCTION,
					len
					);
				}
			else {
				boardAddressIsValid	= true;
				}
			}
		}

	if(!labelUuidIsValid){
		Oscl::Error::Info::log(
			"%s: deviceUUID is required.\n",
			OSCL_PRETTY_FUNCTION
			);
		return true;
		}

	if(!boardAddressIsValid){
		Oscl::Error::Info::log(
			"%s: boardAddress is required.\n",
			OSCL_PRETTY_FUNCTION
			);
		return true;
		}

	return false;
	}

static void	convertBinaryToHexAsciiBoardAddress(
				const uint8_t*	boardAddress,
				char*			asciiHex
				) noexcept{
	for(unsigned i=0;i<6;++i){
		Oscl::Strings::octetToLowerCaseHexAscii(
			boardAddress[i],
			asciiHex[(i*2)],	// msn
			asciiHex[(i*2)+1]	// lsn
			);
		}
	}

void	Part::write(
			const uint8_t*	deviceUUID,
			const uint8_t*	boardAddress
			) noexcept{

	char	deviceUuidArray[64];

	deviceUuidArray[0]	= '\0';
	deviceUuidArray[sizeof(deviceUuidArray)-1]	= '\0';

	bool
	failed	= Oscl::UUID::String::uuidToString(
           		deviceUUID,
           		deviceUuidArray,
           		sizeof(deviceUuidArray)
           		);

	if(failed){
		Oscl::ErrorFatal::logAndExit(
			"%s: Invalid Device UUID!\n",
			OSCL_PRETTY_FUNCTION
			);
		}

	char	boardAddressArray[64];

	boardAddressArray[0]	= '\0';
	boardAddressArray[sizeof(boardAddressArray)-1]	= '\0';

	convertBinaryToHexAsciiBoardAddress(
		_boardAddress,
		boardAddressArray
		);

	boardAddressArray[2*6]	= '\0';

	Oscl::Strings::Fixed<1024>	path;

	_parent.buildPath(path);

	path	+= "device";

	if(path.truncated()){
		Oscl::Error::Info::log(
			"%s: trunacated path: %s\n",
			OSCL_PRETTY_FUNCTION,
			path.getString()
			);
		}

	Oscl::Zephyr::FS::File::Scope
		f(	path.getString(),
			true	// create
			);
	int
	result	= f.openResult();

	if(result){
		Oscl::Error::Info::log(
			"%s: open() failed %d\n",
			OSCL_PRETTY_FUNCTION,
			result
			);
		return;
		}

	result	= f.truncate(0);

	if(result){
		Oscl::Error::Info::log(
			"%s: truncate() failed %d\n",
			OSCL_PRETTY_FUNCTION,
			result
			);
		}

	Oscl::Strings::Fixed<1024>	value;
	value	= "deviceUUID=";
	value	+= deviceUuidArray;
	value	+= "\n";
	value	+= "boardAddress=";
	value	+= boardAddressArray;
	value	+= "\n";

	ssize_t
	writeResult	= f.write(
					value.getString(),
					value.length()
					);

	if(writeResult < 0){
		Oscl::Error::Info::log(
			"%s: write() failed %d\n",
			OSCL_PRETTY_FUNCTION,
			writeResult
			);
		}
	else {
		unsigned long
		nWritten	= writeResult;

		if(nWritten < value.length()){
			Oscl::Error::Info::log(
				"%s: write() failed. nWritten: %d\n",
				OSCL_PRETTY_FUNCTION,
				nWritten
				);
			}
		}

	if(deviceUUID){
		memcpy(
			_deviceUUID,
			deviceUUID,
			sizeof(_deviceUUID)
			);
		}
	}

const uint8_t*	Part::deviceUUID() const noexcept{
	return _deviceUUID;
	}

const uint8_t*	Part::boardAddress() const noexcept{
	return _boardAddress;
	}


