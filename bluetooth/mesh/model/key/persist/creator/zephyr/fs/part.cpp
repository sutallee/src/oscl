/*
   Copyright (C) 2020 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <new>
#include <string.h>
#include <strings.h>
#include <stdio.h>
#include <stdlib.h>

#include "part.h"
#include "oscl/error/info.h"
#include "oscl/strings/hex.h"
#include "oscl/strings/base.h"
#include "oscl/strings/fixed.h"
#include "oscl/zephyr/fs/file/scope.h"
#include "oscl/zephyr/fs/file/fgets.h"

using namespace Oscl::BT::Mesh::Model::Key::Persist::Creator;

Part::Part(
	Oscl::Persist::
	Parent::Zephyr::FS::Part&	parent
	) noexcept:
		_parent(parent)
		{

	for(unsigned i=0;i<nFileMem;++i){
		_freeFileMem.put(&_fileMem[i]);
		}

	createKeys();
	}

Part::~Part() noexcept{
	destroyKeyFiles();
	}

Oscl::BT::Mesh::Model::Key::Persist::Api*
	Part::create(
				uint16_t	keyIndex
				) noexcept{

	FileMem*
	mem	= _freeFileMem.get();
	if(!mem){
		return 0;
		}

	File*
	persist	= new(mem) File(
				0,
				keyIndex
				);


	char	buffer[1024];

	Oscl::Strings::Base
	path(
		buffer,
		sizeof(buffer)
		);

	_parent.buildPath(path);

	unsigned
	offset	= path.length();
	if(offset){
		}

	path += "keyXXXXXX";

	if(path.truncated()){
		Oscl::Error::Info::log(
			"%s: trunacated path: %s\n",
			OSCL_PRETTY_FUNCTION,
			path.getString()
			);
		_freeFileMem.put(mem);
		return 0;
		}

	Oscl::Zephyr::FS::File::Scope	f;

	bool
	failed	= f.mkstemp(buffer);

	if(failed){
		Oscl::Error::Info::log(
			"%s: mkstemp(%s) failed. errno: %d\n",
			OSCL_PRETTY_FUNCTION,
			buffer,
			f.openResult()
			);
		_freeFileMem.put(mem);
		return 0;
		}

	strncpy(
		persist->_fileName,
		&buffer[offset],
		sizeof(persist->_fileName)
		);

	persist->_fileName[sizeof(persist->_fileName)-1]	= '\0';

	persist->write(f);

	_fileList.put(persist);

	return persist;
	}

void	Part::destroy(Oscl::BT::Mesh::Model::Key::Persist::Api& p) noexcept{

	char	buffer[1024];

	Oscl::Strings::Base
	path(
		buffer,
		sizeof(buffer)
		);

	_parent.buildPath(path);

	for(
		File*	persist	= _fileList.first();
		persist;
		persist	= _fileList.next(persist)
		){

		Oscl::BT::Mesh::Model::Key::Persist::Api*
		persistApi	= persist;
		if(persistApi == &p){

			_fileList.remove(persist);

			path	+= persist->_fileName;

			if(path.truncated()){
				Oscl::Error::Info::log(
					"%s: trunacated path: %s\n",
					OSCL_PRETTY_FUNCTION,
					path.getString()
					);
				return;
				}

			int
			result	= fs_unlink(path.getString());

			if(result){
				Oscl::Error::Info::log(
					"%s fs_unlink() failed: %d\n",
					OSCL_PRETTY_FUNCTION,
					result
					);
				}

			_fileList.remove(persist);

			persist->~File();

			_freeFileMem.put((FileMem*)persist);

			break;
			}
		}
	}

void	Part::iterate(
			Oscl::BT::Mesh::Model::Key::Persist::Iterator::Api&	iterator
			) noexcept{

	for(
		File*	persist	= _fileList.first();
		persist;
		persist	= _fileList.next(persist)
		){
		bool
		haltIteration	= iterator.item(
							*persist,
							persist->_keyIndex
							);
		if(haltIteration){
			return;
			}
		}
	}

void	Part::createKey(
			const char*		fileName,
			uint16_t		keyIndex
			) noexcept{

	FileMem*
	mem	= _freeFileMem.get();
	if(!mem){
		Oscl::Error::Info::log(
			"%s: out of FileMem.\n",
			OSCL_PRETTY_FUNCTION
			);
		return;
		}

	File*
	persist	= new(mem) File(
				fileName,
				keyIndex
				);

	_fileList.put(persist);
	}

void	Part::createKeyFromFile(
			const char*	filePath,
			const char*	fileName
			) noexcept{

	static const char	prefix[] = {"key"};

	bool
	different	= strncmp(
					prefix,
					fileName,
					sizeof(prefix)-1
					);

	if(different){
		return;
		}

	uint16_t	keyIndex	= 0;
	bool		keyIndexIsValid	= false;

	Oscl::Zephyr::FS::File::Scope
		f(
			filePath,
			false	// create
			);

	if(f.openResult()) {
		Oscl::Error::Info::log(
			"%s\topen() failed. errno: %d\n",
			OSCL_PRETTY_FUNCTION,
			f.openResult()
			);
		return;
		}

	char			buffer[1024];
	const char*		line;

	while((line = Oscl::Zephyr::FS::File::fgets(buffer,sizeof(buffer),f))){

		if(line[0] == '#'){
			// comment
			continue;
			}

		char*
		assignment	= index(buffer,'=');

		if(!assignment){
			continue;
			}

		assignment[0]	= '\0';

		char*
		value	= &assignment[1];

		char*
		newline = rindex(value,'\n');

		if(newline){
			*newline	= '\0';
			}

		if(!strcmp(line,"keyIndex")){
			keyIndex			= (uint16_t)strtoul(value,0,0);
			keyIndexIsValid	= true;
			}
		}

	if(!keyIndexIsValid){
		Oscl::Error::Info::log(
			"%s: keyIndex is missing.\n",
			OSCL_PRETTY_FUNCTION
			);
		return;
		}

	createKey(
		fileName,
		keyIndex
		);
	}

void	Part::destroyKeyFile(
			const char*	filePath,
			const char*	fileName
			) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s: %s\n",
		OSCL_PRETTY_FUNCTION,
		filePath
		);
	#endif

	static const char	prefix[] = {"key"};

	bool
	different	= strncmp(
					prefix,
					fileName,
					sizeof(prefix)-1
					);

	if(different){
		return;
		}

	int
	result	= fs_unlink(filePath);

	if(result){
		Oscl::Error::Info::log(
			"%s fs_unlink() failed: %d\n",
			OSCL_PRETTY_FUNCTION,
			result
			);
		}
	}

void	Part::createKeys() noexcept{
	Oscl::Persist::Parent::Zephyr::FS::Child::Composer<Part>
		iterator(
			*this,
			&Part::createKeyFromFile
			);

	_parent.walkDir(iterator);
	}

void	Part::destroyKeyFiles() noexcept{

	Oscl::Persist::Parent::Zephyr::FS::Child::Composer<Part>
		iterator(
			*this,
			&Part::destroyKeyFile
			);

	_parent.walkDir(iterator);
	}

File::File(
	const char*		fileName,
	uint16_t		keyIndex
	) noexcept:
		_keyIndex(keyIndex)
		{
	if(fileName){
		strncpy(
			_fileName,
			fileName,
			sizeof(_fileName)
			);
		}
	}

void	File::write(Oscl::FS::File::Api& f) noexcept{
	int
	result	= f.truncate(0);

	if(result){
		Oscl::Error::Info::log(
			"%s: truncate() failed %d\n",
			OSCL_PRETTY_FUNCTION,
			result
			);
		}

	char	buffer[128];

	unsigned
	n	= snprintf(
			buffer,
			sizeof(buffer),
			"keyIndex=0x%4.4X\n"
			"",
			_keyIndex
			);

	buffer[sizeof(buffer)-1]	= '\0';

	if(n >= sizeof(buffer)){
		// truncated
		Oscl::Error::Info::log(
			"%s: snprintf() failed buffer too small.\n",
			OSCL_PRETTY_FUNCTION
			);
		}

	ssize_t
	writeResult	= f.write(
					buffer,
					n
					);

	if(writeResult < 0){
		Oscl::Error::Info::log(
			"%s: write() failed %d\n",
			OSCL_PRETTY_FUNCTION,
			writeResult
			);
		}
	else {
		unsigned long
		nWritten	= writeResult;

		if(nWritten < n){
			Oscl::Error::Info::log(
				"%s: write() failed. n: %d\n",
				OSCL_PRETTY_FUNCTION,
				n
				);
			}
		}
	}
