/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "var.h"
/** */
namespace Oscl {

/** */
namespace BT {

/** */
namespace Mesh {

/** */
namespace Model {

/** */
namespace Key {

/** */
namespace Result {

class VarIsSuccess : public Var::Query {
	public:
		bool	_match;
	public:
		inline void success() noexcept{
			_match	= true;
			}
		inline void noSuchIndex() noexcept{
			_match	= false;
			}
		inline void indexAlreadyExists() noexcept{
			_match	= false;
			}
		inline void outOfResources() noexcept{
			_match	= false;
			}
	};

class VarIsNoSuchIndex : public Var::Query {
	public:
		bool	_match;
	public:
		inline void success() noexcept{
			_match	= false;
			}
		inline void noSuchIndex() noexcept{
			_match	= true;
			}
		inline void indexAlreadyExists() noexcept{
			_match	= false;
			}
		inline void outOfResources() noexcept{
			_match	= false;
			}
	};

class VarIsIndexAlreadyExists : public Var::Query {
	public:
		bool	_match;
	public:
		inline void success() noexcept{
			_match	= false;
			}
		inline void noSuchIndex() noexcept{
			_match	= false;
			}
		inline void indexAlreadyExists() noexcept{
			_match	= true;
			}
		inline void outOfResources() noexcept{
			_match	= false;
			}
	};

class VarIsOutOfResources : public Var::Query {
	public:
		bool	_match;
	public:
		inline void success() noexcept{
			_match	= false;
			}
		inline void noSuchIndex() noexcept{
			_match	= false;
			}
		inline void indexAlreadyExists() noexcept{
			_match	= false;
			}
		inline void outOfResources() noexcept{
			_match	= true;
			}
	};

class GetState : public Var::Query {
	public:
		const Var::State*	_state;
	public:
		inline void success() noexcept{
			_state	= &Var::getSuccess();
			}
		inline void noSuchIndex() noexcept{
			_state	= &Var::getNoSuchIndex();
			}
		inline void indexAlreadyExists() noexcept{
			_state	= &Var::getIndexAlreadyExists();
			}
		inline void outOfResources() noexcept{
			_state	= &Var::getOutOfResources();
			}
	};
class VarSuccess : public Var::State {
	public:
		VarSuccess(){}
		bool	operator == (const Var::State& other) const noexcept;
		bool	operator != (const Var::State& other) const noexcept;
		void	accept(Var::Query& q) const noexcept;
	};

class VarNoSuchIndex : public Var::State {
	public:
		VarNoSuchIndex(){}
		bool	operator == (const Var::State& other) const noexcept;
		bool	operator != (const Var::State& other) const noexcept;
		void	accept(Var::Query& q) const noexcept;
	};

class VarIndexAlreadyExists : public Var::State {
	public:
		VarIndexAlreadyExists(){}
		bool	operator == (const Var::State& other) const noexcept;
		bool	operator != (const Var::State& other) const noexcept;
		void	accept(Var::Query& q) const noexcept;
	};

class VarOutOfResources : public Var::State {
	public:
		VarOutOfResources(){}
		bool	operator == (const Var::State& other) const noexcept;
		bool	operator != (const Var::State& other) const noexcept;
		void	accept(Var::Query& q) const noexcept;
	};

}
}
}
}
}
}
using namespace Oscl::BT::Mesh::Model::Key::Result;
static const VarSuccess	_success;
static const VarNoSuchIndex	_noSuchIndex;
static const VarIndexAlreadyExists	_indexAlreadyExists;
static const VarOutOfResources	_outOfResources;
bool VarSuccess::operator == (const Var::State& other) const noexcept{
	VarIsSuccess	query;
	other.accept(query);
	return query._match;
	}

bool VarSuccess::operator != (const Var::State& other) const noexcept{
	VarIsSuccess	query;
	other.accept(query);
	return !query._match;
	}

void VarSuccess::accept(Var::Query& q) const noexcept{
	q.success();
	}

bool VarNoSuchIndex::operator == (const Var::State& other) const noexcept{
	VarIsNoSuchIndex	query;
	other.accept(query);
	return query._match;
	}

bool VarNoSuchIndex::operator != (const Var::State& other) const noexcept{
	VarIsNoSuchIndex	query;
	other.accept(query);
	return !query._match;
	}

void VarNoSuchIndex::accept(Var::Query& q) const noexcept{
	q.noSuchIndex();
	}

bool VarIndexAlreadyExists::operator == (const Var::State& other) const noexcept{
	VarIsIndexAlreadyExists	query;
	other.accept(query);
	return query._match;
	}

bool VarIndexAlreadyExists::operator != (const Var::State& other) const noexcept{
	VarIsIndexAlreadyExists	query;
	other.accept(query);
	return !query._match;
	}

void VarIndexAlreadyExists::accept(Var::Query& q) const noexcept{
	q.indexAlreadyExists();
	}

bool VarOutOfResources::operator == (const Var::State& other) const noexcept{
	VarIsOutOfResources	query;
	other.accept(query);
	return query._match;
	}

bool VarOutOfResources::operator != (const Var::State& other) const noexcept{
	VarIsOutOfResources	query;
	other.accept(query);
	return !query._match;
	}

void VarOutOfResources::accept(Var::Query& q) const noexcept{
	q.outOfResources();
	}


Var::Var(const State& initial) noexcept:
		_state(&initial)
		{
	}

void Var::accept(Query& q) noexcept{
	_state->accept(q);
	}

bool Var::operator == (const Var& other) const noexcept{
	return *other._state == *_state;
	}

bool Var::operator != (const Var& other) const noexcept{
	return *other._state != *_state;
	}

void Var::operator = (const Var& newState) noexcept{
	*this	= *newState._state;
	}

void Var::operator = (const State& newState) noexcept{
	GetState	getState;
	newState.accept(getState);
	_state	= getState._state;
	}

const Var::State& Var::getCurrentState() const noexcept{
	return *_state;
	}

void Var::success() noexcept{
	_state	= &_success;
	}

void Var::noSuchIndex() noexcept{
	_state	= &_noSuchIndex;
	}

void Var::indexAlreadyExists() noexcept{
	_state	= &_indexAlreadyExists;
	}

void Var::outOfResources() noexcept{
	_state	= &_outOfResources;
	}

const Var::State&	Var::getSuccess() noexcept{
	return _success;
	}

const Var::State&	Var::getNoSuchIndex() noexcept{
	return _noSuchIndex;
	}

const Var::State&	Var::getIndexAlreadyExists() noexcept{
	return _indexAlreadyExists;
	}

const Var::State&	Var::getOutOfResources() noexcept{
	return _outOfResources;
	}

