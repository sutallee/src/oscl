/*
   Copyright (C) 2020 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <new>
#include "part.h"
#include "oscl/endian/decoder/linear/part.h"
#include "oscl/encoder/le/base.h"
#include "oscl/encoder/be/base.h"
#include "oscl/error/info.h"
#include "oscl/bluetooth/mesh/address/virtual.h"
#include "oscl/bluetooth/mesh/address/types.h"
#include "oscl/bluetooth/mesh/address/values.h"
#include "oscl/bluetooth/mesh/publication/utils.h"
#include "oscl/bluetooth/mesh/model/sensor/status/mpid.h"
#include "oscl/bluetooth/mesh/model/opcode/decoder.h"
#include "oscl/bluetooth/mesh/model/opcode/encoder.h"
#include "oscl/bluetooth/mesh/constants.h"

using namespace Oscl::BT::Mesh::Model::Sensor::Server;

//#define DEBUG_TRACE
//#define DEBUG_TRACE_TMI
//#define DEBUG_TRACE_TX_STATUS

Part::Part(
	Oscl::Pdu::Memory::Api&			freeStoreApi,
	Oscl::Timer::Factory::Api&		timerFactoryApi,
	Oscl::BT::Mesh::
	Publication::Persist::Api&		publicationPersistApi,
	Oscl::BT::Mesh::Node::Api&		nodeApi,
	Oscl::BT::Mesh::Element::
	Symbiont::Kernel::Api&			kernelApi,
	Oscl::BT::Mesh::
	Transport::Lower::TX::Api&		lowerTransportApi,
	Oscl::BT::Mesh::Network::Api&	networkApi,
	Oscl::BT::Mesh::Subscription::
	Persist::Creator::Api&			subscriptionPersistApi,
	Oscl::BT::Mesh::Model::Key::
	Persist::Creator::Api&			keyPersistApi,
	uint16_t						elementAddress,
	bool							allowSegmentedStatus
	) noexcept:
		_freeStoreApi(freeStoreApi),
		_timerFactoryApi(timerFactoryApi),
		_allowSegmentedStatus(allowSegmentedStatus),
		_appServerContext(
			*this,
			&Part::processAccessMessage,
			&Part::sigModelIdMatch,
			&Part::vendorModelIdMatch,
			&Part::modelSupportsAppKeyBinding,
			&Part::getPublicationApi,
			&Part::getPublicationAppKeyIndex,
			&Part::getSubscriptionApi,
			&Part::getSubscriptionServerApi,
			&Part::stop
			),
		_subscriptionServerPart(
			subscriptionPersistApi,
			kernelApi.getSubscriptionRegisterApi()
			),
		_appServerPart(
			_appServerContext,
			freeStoreApi,
			nodeApi,
			lowerTransportApi,
			networkApi,
			keyPersistApi,
			elementAddress
			),
		_publicationPart(
            *this,
            timerFactoryApi,
            publicationPersistApi
			),
		_rxSensorDescriptorGet(
			*this,
			&Part::rxSensorDescriptorGet
			),
		_rxSensorGet(
			*this,
			&Part::rxSensorGet
			),
		_rxSensorColumnGet(
			*this,
			&Part::rxSensorColumnGet
			),
		_rxSensorSeriesGet(
			*this,
			&Part::rxSensorSeriesGet
			)
		{

	_messageHandlers.put(&_rxSensorDescriptorGet);
	_messageHandlers.put(&_rxSensorGet);
	_messageHandlers.put(&_rxSensorColumnGet);
	_messageHandlers.put(&_rxSensorSeriesGet);
	}

Part::~Part() noexcept{
	}

Oscl::BT::Mesh::Model::RX::Item&	Part::getModelRxItem() noexcept{
	return _appServerPart;
	}

Oscl::BT::Mesh::Model::Api&	Part::getModelApi() noexcept{
	return _appServerPart.getModelApi();
	}

void	Part::attach(Oscl::BT::Mesh::Model::Sensor::Server::Property::Item&	item) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif

	_properties.put(&item);
	}

void	Part::processAccessMessage(
			const void*	frame,
			unsigned	length,
			uint16_t	src,
			uint16_t	dst,
			uint16_t	appKeyIndex
			) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);

	Oscl::Error::Info::hexDump(
		frame,
		length
		);
	#endif

	Oscl::Endian::Decoder::Linear::Part
	leDecoder(
		frame,
		length
		);

	Oscl::Decoder::Api&	decoder	= leDecoder.le();

	uint32_t
	opcode	= Oscl::BT::Mesh::Model::OpCode::decode(decoder);

	using namespace Oscl::BT::Mesh::Model::OpCode;

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s:\n"
		"\topcode: (0x%4.4X)\n",
		__PRETTY_FUNCTION__,
		opcode
		);
	#endif

	const uint8_t*	p	= (const uint8_t*)frame;

	unsigned
	remaining	= decoder.remaining();

	unsigned
	payloadOffset	= length - remaining;

	for(
		Oscl::BT::Mesh::Message::Item*	item	= _messageHandlers.first();
		item;
		item	= _messageHandlers.next(item)
		){
		bool
		handled	= item->receive(
					opcode,
					src,
					dst,
					&p[payloadOffset],
					remaining,
					appKeyIndex
					);
		if(handled){
			return;
			}
		}
	}

bool	Part::rxSensorDescriptorGet(
			uint32_t	opcode,
			uint16_t	srcAddress,
			uint16_t	dstAddress,
			const void*	frame,
			unsigned	length,
			uint16_t	appKeyIndex
			) noexcept{

	if(opcode != Oscl::BT::Mesh::Model::OpCode::sensorDescriptorGet){
		return false;
		}

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n"
		"",
		__PRETTY_FUNCTION__
		);
	#endif

	Oscl::Endian::Decoder::Linear::Part
	leDecoder(
		frame,
		length
		);

	Oscl::Decoder::Api&	decoder	= leDecoder.le();

	uint16_t	propertyID;
	bool		propertyIdIsValid	= true;

	decoder.decode(propertyID);

	if(decoder.underflow()){
		propertyIdIsValid	= false;
		}

	sendSensorDescriptorStatus(
		appKeyIndex,
		srcAddress,
		propertyID,
		propertyIdIsValid,
		Oscl::BT::Mesh::Address::isUnicastAddress(dstAddress)
		);

	return true;
	}

bool	Part::rxSensorGet(
			uint32_t	opcode,
			uint16_t	srcAddress,
			uint16_t	dstAddress,
			const void*	frame,
			unsigned	length,
			uint16_t	appKeyIndex
			) noexcept{

	if(opcode != Oscl::BT::Mesh::Model::OpCode::sensorGet){
		return false;
		}

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n"
		"",
		__PRETTY_FUNCTION__
		);
	#endif

	Oscl::Endian::Decoder::Linear::Part
	leDecoder(
		frame,
		length
		);

	Oscl::Decoder::Api&	decoder	= leDecoder.le();

	uint16_t	propertyID;
	bool		propertyIdIsValid	= true;

	decoder.decode(propertyID);

	if(decoder.underflow()){
		propertyIdIsValid	= false;
		}

	sendSensorStatus(
		appKeyIndex,
		srcAddress,
		propertyID,
		propertyIdIsValid,
		Oscl::BT::Mesh::Address::isUnicastAddress(dstAddress)
		);

	return true;
	}

bool	Part::rxSensorColumnGet(
			uint32_t	opcode,
			uint16_t	srcAddress,
			uint16_t	dstAddress,
			const void*	frame,
			unsigned	length,
			uint16_t	appKeyIndex
			) noexcept{

	if(opcode != Oscl::BT::Mesh::Model::OpCode::sensorColumnGet){
		return false;
		}

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n"
		"",
		__PRETTY_FUNCTION__
		);
	#endif

	Oscl::Endian::Decoder::Linear::Part
	leDecoder(
		frame,
		length
		);

	Oscl::Decoder::Api&	decoder	= leDecoder.le();

	uint16_t	propertyID;

	decoder.decode(propertyID);

	if(decoder.underflow()){
		// Property ID is required.
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			OSCL_PRETTY_FUNCTION
			);
		return true;
		}

	sendSensorColumnStatus(
		appKeyIndex,
		srcAddress,
		propertyID,
		leDecoder,
		Oscl::BT::Mesh::Address::isUnicastAddress(dstAddress)
		);

	return true;
	}

bool	Part::rxSensorSeriesGet(
			uint32_t	opcode,
			uint16_t	srcAddress,
			uint16_t	dstAddress,
			const void*	frame,
			unsigned	length,
			uint16_t	appKeyIndex
			) noexcept{

	if(opcode != Oscl::BT::Mesh::Model::OpCode::sensorSeriesGet){
		return false;
		}

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n"
		"",
		__PRETTY_FUNCTION__
		);
	#endif

	Oscl::Endian::Decoder::Linear::Part
	leDecoder(
		frame,
		length
		);

	Oscl::Decoder::Api&	decoder	= leDecoder.le();

	uint16_t	propertyID;

	decoder.decode(propertyID);

	if(decoder.underflow()){
		// Property ID is required.
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			OSCL_PRETTY_FUNCTION
			);
		return true;
		}

	sendSensorSeriesStatus(
		appKeyIndex,
		srcAddress,
		propertyID,
		leDecoder,
		Oscl::BT::Mesh::Address::isUnicastAddress(dstAddress)
		);

	return true;
	}

void	Part::sendSensorDescriptorStatus(
			uint16_t	appKeyIndex,
			uint16_t	dstAddress,
			uint16_t	propertyID,
			bool		propertyIdIsValid,
			bool		sendSegmented
			) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif

	uint8_t	buffer[maxAccessPduSizeWithTransMic32];

	Oscl::Encoder::LE::Base
	leEncoder(
		buffer,
		sizeof(buffer)
		);
	Oscl::Encoder::Api&	encoder	= leEncoder;

	Oscl::BT::Mesh::Model::OpCode::encode(
		encoder,
		Oscl::BT::Mesh::Model::OpCode::sensorDescriptorStatus
		);

	if(propertyIdIsValid){
		Oscl::BT::Mesh::Model::
		Sensor::Server::Property::Item*	item;
		for(
			item	= _properties.first();
			item;
			item	= _properties.next(item)
			){
			if(item->getPropertyID() == propertyID){
				item->encodeDescriptor(encoder);
				break;
				}
			}
		if(!item){
			// No matching Property-ID
			/*
				When the message is a response to a Sensor Descriptor Get message
				that identifies a sensor descriptor property that does not exist
				on the element, the Descriptor field shall contain the requested
				Property ID value and the other fields of the Sensor Descriptor
				state shall be omitted.
			*/
			encoder.encode(propertyID);
			}
		}
	else {
		for(
			Oscl::BT::Mesh::Model::
			Sensor::Server::Property::Item*
			item	= _properties.first();
			item;
			item	= _properties.next(item)
			){
			item->encodeDescriptor(encoder);
			}
		}

	if(encoder.overflow()){
		Oscl::Error::Info::log(
			"%s: overflow!\n",
			OSCL_PRETTY_FUNCTION
			);
		return;
		}

	Oscl::BT::Mesh::Transport::Upper::TX::Api&
	transportApi	= _appServerPart.getUpperTransportApi();

	if(_allowSegmentedStatus && sendSegmented){
		transportApi.sendSegmented(
			this,	// completeApi
			buffer,
			encoder.length(),
			dstAddress,	// dst
			appKeyIndex,
			0xFF	// use Default TTL
			);
		}
	else {
		transportApi.sendUnsegmented(
			this,	// completeApi
			buffer,
			encoder.length(),
			dstAddress,	// dst
			appKeyIndex,
			0xFF	// use Default TTL
			);
		}
	}

void	Part::sendSensorStatus(
			uint16_t	appKeyIndex,
			uint16_t	dstAddress,
			uint16_t	propertyID,
			bool		propertyIdIsValid,
			bool		sendSegmented
			) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif

	uint8_t	buffer[maxAccessPduSizeWithTransMic32];

	Oscl::Encoder::LE::Base
	leEncoder(
		buffer,
		sizeof(buffer)
		);
	Oscl::Encoder::Api&	encoder	= leEncoder;

	Oscl::BT::Mesh::Model::OpCode::encode(
		encoder,
		Oscl::BT::Mesh::Model::OpCode::sensorStatus
		);

	if(propertyIdIsValid){
		Oscl::BT::Mesh::Model::
		Sensor::Server::Property::Item*	item;
		for(
			item	= _properties.first();
			item;
			item	= _properties.next(item)
			){
			if(item->getPropertyID() == propertyID){
				item->encodeStatus(encoder);
				break;
				}
			}
		if(!item){
			// No matching Property-ID
			/*
				When the message is a response to a Sensor Get message that identifies
				a sensor property that does not exist on the element, the Length field
				shall represent the value of zero and the Raw Value for that property
				shall be omitted.
			 */
			Oscl::BT::Mesh::Model::Sensor::Status::encodeMPID(
				encoder,
				propertyID,
				0
				);
			}
		}
	else {
		for(
			Oscl::BT::Mesh::Model::
			Sensor::Server::Property::Item*
			item	= _properties.first();
			item;
			item	= _properties.next(item)
			){
			item->encodeStatus(encoder);
			}
		}

	if(encoder.overflow()){
		Oscl::Error::Info::log(
			"%s: overflow!\n",
			OSCL_PRETTY_FUNCTION
			);
		return;
		}

	#ifdef DEBUG_TRACE_TMI
	Oscl::Error::Info::hexDump(
		buffer,
		encoder.length()
		);
	#endif

	Oscl::BT::Mesh::Transport::Upper::TX::Api&
	transportApi	= _appServerPart.getUpperTransportApi();

	if(_allowSegmentedStatus && sendSegmented){
		transportApi.sendSegmented(
			this,	// completeApi
			buffer,
			encoder.length(),
			dstAddress,	// dst
			appKeyIndex,
			0xFF	// use Default TTL
			);
		}
	else {
		transportApi.sendUnsegmented(
			this,	// completeApi
			buffer,
			encoder.length(),
			dstAddress,	// dst
			appKeyIndex,
			0xFF	// use Default TTL
			);
		}
	}

void	Part::sendSensorColumnStatus(
			uint16_t			appKeyIndex,
			uint16_t			dstAddress,
			uint16_t			propertyID,
			Oscl::Endian::Decoder::Api&	dec,
			bool				sendSegmented
			) noexcept{

	/*
		A Sensor Server shall send a Sensor Column Status message,
		setting the Property ID field to the value of the Sensor Property ID state,
		the Raw Value X to the value of the Sensor Raw Value X state,
		the Column Width field to the value of the Sensor Column Width state,
		and the Raw Value Y field to the value of the Sensor Raw Value Y state.

		If there is no Sensor Raw Value Y state present that matches
		the Raw Value X state as defined by the referenced device property,
		the Column Width field and the Raw Value Y field shall be omitted.

		The message shall be sent as a response to a
		Sensor Column Get message (see Section 4.2.15) or
		may be sent at any time as an unsolicited message.
	 */

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif

	Oscl::Decoder::Api&
	decoder	= dec.le();

	uint8_t	buffer[maxAccessPduSizeWithTransMic32];

	Oscl::Encoder::LE::Base
	leEncoder(
		buffer,
		sizeof(buffer)
		);

	Oscl::Encoder::Api&	encoder	= leEncoder;

	Oscl::BT::Mesh::Model::OpCode::encode(
		encoder,
		Oscl::BT::Mesh::Model::OpCode::sensorColumnStatus
		);

	encoder.encode(propertyID);

	/*	Copy the variable length Raw Value X.
		We *assume* that Raw Value X is no more than 32 octets.
	 */
	uint8_t	rawValueX[32];

	unsigned
	remaining	= decoder.remaining();

	if(remaining > sizeof(rawValueX)){
		Oscl::Error::Info::log(
			"%s: rawValueX[] is too small for property-id 0x%4.4X!\n",
			OSCL_PRETTY_FUNCTION,
			propertyID
			);
		return;
		}

	decoder.copyOut(
		rawValueX,
		remaining
		);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: copyOut() underflow for property-id 0x%4.4X!\n",
			OSCL_PRETTY_FUNCTION,
			propertyID
			);
		return;
		}

	encoder.copyIn(
		rawValueX,
		remaining
		);

	if(encoder.overflow()){
		Oscl::Error::Info::log(
			"%s: copyIn() overflow for property-id 0x%4.4X!\n",
			OSCL_PRETTY_FUNCTION,
			propertyID
			);
		}

	// FIXME: Build payload

	Oscl::BT::Mesh::Model::
	Sensor::Server::Property::Item*	item;

	for(
		item	= _properties.first();
		item;
		item	= _properties.next(item)
		){
		if(item->getPropertyID() != propertyID){
			continue;
			}

		item->encodeColumnStatus(encoder,dec);
		break;
		}

	if(!item){
		Oscl::Error::Info::log(
			"%s: no such property-id 0x%4.4X!\n",
			OSCL_PRETTY_FUNCTION,
			propertyID
			);
		}

	if(encoder.overflow()){
		Oscl::Error::Info::log(
			"%s: overflow!\n",
			OSCL_PRETTY_FUNCTION
			);
		return;
		}

	Oscl::BT::Mesh::Transport::Upper::TX::Api&
	transportApi	= _appServerPart.getUpperTransportApi();

	if(_allowSegmentedStatus && sendSegmented){
		transportApi.sendSegmented(
			this,	// completeApi
			buffer,
			encoder.length(),
			dstAddress,	// dst
			appKeyIndex,
			0xFF	// use Default TTL
			);
		}
	else {
		transportApi.sendUnsegmented(
			this,	// completeApi
			buffer,
			encoder.length(),
			dstAddress,	// dst
			appKeyIndex,
			0xFF	// use Default TTL
			);
		}
	}

void	Part::sendSensorSeriesStatus(
			uint16_t			appKeyIndex,
			uint16_t			dstAddress,
			uint16_t			propertyID,
			Oscl::Endian::Decoder::Api&	dec,
			bool				sendSegmented
			) noexcept{

	/*
		A Sensor Server shall send a Sensor Series Status message,
		setting the Property ID field to the value of the Property ID state,
		the following triplets of the
		Raw Value X to the value of the Sensor Raw Value X state,
		the Column Width field to the value of the Sensor Column Width state,
		and the Raw Value Y field to the value of the Sensor Raw Value Y state
		for each value of Raw Value X that is within the inclusive range from
		Raw Value X1 through Raw Value X2 field value of the requesting message.

		When the Raw Value X1 and Raw Value X2 fields are not present
		in the incoming message, the Sensor Server shall report all columns
		of the Property ID state.

		If the requested Property ID is not recognized by the Sensor Server or
		if there is no Sensor Series Column state for requested Property ID,
		then the Raw Value X field, the Sensor Column Width field,
		and the Raw Value Y field shall be omitted.
	 */

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif

	Oscl::Decoder::Api&
	decoder	= dec.le();

	uint8_t	buffer[maxAccessPduSizeWithTransMic32];

	Oscl::Encoder::LE::Base
	leEncoder(
		buffer,
		sizeof(buffer)
		);

	Oscl::Encoder::Api&	encoder	= leEncoder;

	Oscl::BT::Mesh::Model::OpCode::encode(
		encoder,
		Oscl::BT::Mesh::Model::OpCode::sensorSeriesStatus
		);

	encoder.encode(propertyID);

	/*	Copy the variable length Raw Value X.
		We *assume* that Raw Value X is no more than 32 octets.
	 */
	uint8_t	rawValueX[32];

	unsigned
	remaining	= decoder.remaining();

	if(remaining > sizeof(rawValueX)){
		Oscl::Error::Info::log(
			"%s: rawValueX[] is too small for property-id 0x%4.4X!\n",
			OSCL_PRETTY_FUNCTION,
			propertyID
			);
		return;
		}

	decoder.copyOut(
		rawValueX,
		remaining
		);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: copyOut() underflow for property-id 0x%4.4X!\n",
			OSCL_PRETTY_FUNCTION,
			propertyID
			);
		return;
		}

	encoder.copyIn(
		rawValueX,
		remaining
		);

	if(encoder.overflow()){
		Oscl::Error::Info::log(
			"%s: copyIn() overflow for property-id 0x%4.4X!\n",
			OSCL_PRETTY_FUNCTION,
			propertyID
			);
		}


	// FIXME: Build payload

	Oscl::BT::Mesh::Model::
	Sensor::Server::Property::Item*	item;

	for(
		item	= _properties.first();
		item;
		item	= _properties.next(item)
		){
		if(item->getPropertyID() != propertyID){
			continue;
			}

		item->encodeSeriesStatus(
			encoder,
			dec
			);
		break;
		}

	if(!item){
		Oscl::Error::Info::log(
			"%s: no such property-id 0x%4.4X!\n",
			OSCL_PRETTY_FUNCTION,
			propertyID
			);
		}

	if(encoder.overflow()){
		Oscl::Error::Info::log(
			"%s: overflow!\n",
			OSCL_PRETTY_FUNCTION
			);
		return;
		}

	Oscl::BT::Mesh::Transport::Upper::TX::Api&
	transportApi	= _appServerPart.getUpperTransportApi();

	if(_allowSegmentedStatus && sendSegmented){
		transportApi.sendSegmented(
			this,	// completeApi
			buffer,
			encoder.length(),
			dstAddress,	// dst
			appKeyIndex,
			0xFF	// use Default TTL
			);
		}
	else {
		transportApi.sendUnsegmented(
			this,	// completeApi
			buffer,
			encoder.length(),
			dstAddress,	// dst
			appKeyIndex,
			0xFF	// use Default TTL
			);
		}
	}

void	Part::success() noexcept{
	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif
	}

void	Part::successOnBehalfOfLPN() noexcept{
	#if defined(DEBUG_TRACE) || defined (DEBUG_TRACE_TX_STATUS)
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif
	}

void	Part::failedTimeout() noexcept{
	#if defined(DEBUG_TRACE) || defined (DEBUG_TRACE_TX_STATUS)
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif
	}

void	Part::failedBusy() noexcept{
	#if defined(DEBUG_TRACE) || defined (DEBUG_TRACE_TX_STATUS)
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif
	}

void	Part::failedOutOfResources() noexcept{
	#if defined(DEBUG_TRACE) || defined (DEBUG_TRACE_TX_STATUS)
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif
	}

void	Part::failedTooBig() noexcept{
	#if defined(DEBUG_TRACE) || defined (DEBUG_TRACE_TX_STATUS)
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif
	}

bool	Part::sigModelIdMatch(uint16_t modelID) const noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif

	return (modelID == 0x1100); // Sensor Server
	}

bool	Part::vendorModelIdMatch(uint16_t vendorID, uint16_t modelID) const noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif

	return false;
	}

bool	Part::modelSupportsAppKeyBinding() const noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif

	return true;
	}

Oscl::BT::Mesh::Publication::Api*	Part::getPublicationApi() noexcept{
	return &_publicationPart;
	}

uint16_t	Part::getPublicationAppKeyIndex() const noexcept{
	return _publicationPart.getPublicationAppKeyIndex();
	}

Oscl::BT::Mesh::Subscription::Api*	Part::getSubscriptionApi() noexcept{
	return &_subscriptionServerPart;
	}

Oscl::BT::Mesh::Subscription::Server::Api*	Part::getSubscriptionServerApi() noexcept{
	return &_subscriptionServerPart;
	}

void	Part::stop() noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s:\n",
		__PRETTY_FUNCTION__
		);
	#endif

	_publicationPart.stop();
	}

void	Part::publish(
			uint16_t	appKeyIndex,
			uint16_t	dstAddress,
			uint8_t		ttl,
			const void*	label,
			bool		retransmitting
			) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif

	uint8_t	buffer[maxAccessPduSizeWithTransMic32];

	Oscl::Encoder::LE::Base
	leEncoder(
		buffer,
		sizeof(buffer)
		);
	Oscl::Encoder::Api&	encoder	= leEncoder;

	Oscl::BT::Mesh::Model::OpCode::encode(
		encoder,
		Oscl::BT::Mesh::Model::OpCode::sensorStatus
		);

	for(
		Oscl::BT::Mesh::Model::
		Sensor::Server::Property::Item*
		item	= _properties.first();
		item;
		item	= _properties.next(item)
		){
		item->encodeStatus(encoder);
		}

	if(encoder.overflow()){
		Oscl::Error::Info::log(
			"%s: overflow!\n",
			OSCL_PRETTY_FUNCTION
			);
		return;
		}

	Oscl::BT::Mesh::Transport::Upper::TX::Api&
	transportApi	= _appServerPart.getUpperTransportApi();

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s:\n"
		"\t_publishAddress: 0x%4.4X\n"
		"\t_pubAppKeyIndex: 0x%4.4X\n"
		"\t_publishTTL: 0x%2.2X\n"
		"",
		__PRETTY_FUNCTION__,
		dstAddress,
		appKeyIndex,
		ttl
		);
	#endif

	bool
	isUnicastAddress	= Oscl::BT::Mesh::Address::isUnicastAddress(dstAddress);

	if(_allowSegmentedStatus && isUnicastAddress){

		transportApi.sendSegmented(
			this,	// completeApi
			buffer,
			encoder.length(),
			dstAddress,
			appKeyIndex,
			ttl
			);
		}
	else {

		transportApi.sendUnsegmented(
			this,	// completeApi
			buffer,
			encoder.length(),
			dstAddress,
			appKeyIndex,
			ttl,
			label,
			label?16:0
			);
		}
	}
