/*
   Copyright (C) 2020 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_bluetooth_mesh_model_sensor_server_property_composerh_
#define _oscl_bluetooth_mesh_model_sensor_server_property_composerh_

#include "item.h"

/** */
namespace Oscl {
/** */
namespace BT {
/** */
namespace Mesh {
/** */
namespace Model {
/** */
namespace Sensor {
/** */
namespace Server {
/** */
namespace Property {

/** */
template <class Context>
class Composer : public Item {
	private:
		/** */
		Context&	_context;

		/** */
		uint16_t	(Context::*_getPropertyID)() const;

		/** */
		void	(Context::*_encodeDescriptor)(Oscl::Encoder::Api& encoder) const;

		/** */
		void	(Context::*_encodeStatus)(Oscl::Encoder::Api& encoder) const;

		/** */
		void	(Context::*_encodeColumnStatus)(
					Oscl::Encoder::Api& encoder,
					Oscl::Endian::Decoder::Api& rawValueX
					) const;

		/** */
		void	(Context::*_encodeSeriesStatus)(
					Oscl::Encoder::Api&	encoder,
					Oscl::Endian::Decoder::Api&	rawValueX1AndX2
					) const;

	public:
		/** */
		Composer(
			Context&	context,
			uint16_t	(Context::*getPropertyID)() const,
			void		(Context::*encodeDescriptor)(Oscl::Encoder::Api& encoder) const,
			void		(Context::*encodeStatus)(Oscl::Encoder::Api& encoder) const,
			void		(Context::*encodeColumnStatus)(
							Oscl::Encoder::Api&	encoder,
							Oscl::Endian::Decoder::Api& rawValueX
							) const,
			void		(Context::*encodeSeriesStatus)(
							Oscl::Encoder::Api&	encoder,
							Oscl::Endian::Decoder::Api&	rawValueX1AndX2
							) const
			) noexcept;

		/** Returns the sensor's 16-bit Property-ID.
		 */
		uint16_t	getPropertyID() const noexcept;

		/** This operation instructs the Property to encode its
			16-bit Property-ID. This is used by the Server when
			building a Sensor Descriptor Status message.
		 */
		void	encodeDescriptor(Oscl::Encoder::Api& encoder) const noexcept;

		/** This operation instructs the Property to encode its
			Property-ID and current status.
			This is ued by the Server when building a Sensor Status message.
		 */
		void	encodeStatus(Oscl::Encoder::Api& encoder) const noexcept;

		/** This operation instructs the Property to encode its
			16-bit Property-ID and column data.
			This is used by the Server when building a Sensor Column Status message.
		 */
		void	encodeColumnStatus(
					Oscl::Encoder::Api&	encoder,
					Oscl::Endian::Decoder::Api& rawValueX
					) const noexcept;

		/** This operation instructs the Property to encode its
			16-bit Property-ID and series data.
			This is used by the Server when building a Sensor Series Status message.
		 */
		void	encodeSeriesStatus(
					Oscl::Encoder::Api&	encoder,
					Oscl::Endian::Decoder::Api&	rawValueX1AndX2
					) const noexcept;

	};

template <class Context>
Composer<Context>::Composer(
	Context&	context,
	uint16_t	(Context::*getPropertyID)() const,
	void		(Context::*encodeDescriptor)(Oscl::Encoder::Api& encoder) const,
	void		(Context::*encodeStatus)(Oscl::Encoder::Api& encoder) const,
	void		(Context::*encodeColumnStatus)(
					Oscl::Encoder::Api&	encoder,
					Oscl::Endian::Decoder::Api& rawValueX
					) const,
	void		(Context::*encodeSeriesStatus)(
					Oscl::Encoder::Api&	encoder,
					Oscl::Endian::Decoder::Api&	rawValueX1AndX2
					) const
	)noexcept:
		_context(context),
		_getPropertyID(getPropertyID),
		_encodeDescriptor(encodeDescriptor),
		_encodeStatus(encodeStatus),
		_encodeColumnStatus(encodeColumnStatus),
		_encodeSeriesStatus(encodeSeriesStatus)
		{
	}

template <class Context>
uint16_t Composer<Context>::getPropertyID() const noexcept{
	return (_context.*_getPropertyID)();
	}

template <class Context>
void Composer<Context>::encodeDescriptor(Oscl::Encoder::Api& encoder) const noexcept{
	(_context.*_encodeDescriptor)(encoder);
	}

template <class Context>
void Composer<Context>::encodeStatus(Oscl::Encoder::Api& encoder) const noexcept{
	(_context.*_encodeStatus)(encoder);
	}

template <class Context>
void Composer<Context>::encodeColumnStatus(
		Oscl::Encoder::Api&	encoder,
		Oscl::Endian::Decoder::Api& rawValueX
		) const noexcept{
	(_context.*_encodeColumnStatus)(
		encoder,
		rawValueX
		);
	}

template <class Context>
void Composer<Context>::encodeSeriesStatus(
		Oscl::Encoder::Api&	encoder,
		Oscl::Endian::Decoder::Api&	rawValueX1AndX2
		) const noexcept{
	(_context.*_encodeSeriesStatus)(
		encoder,
		rawValueX1AndX2
		);
	}

}
}
}
}
}
}
}

#endif
