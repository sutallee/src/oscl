/*
   Copyright (C) 2020 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_bluetooth_mesh_model_sensor_server_property_apih_
#define _oscl_bluetooth_mesh_model_sensor_server_property_apih_

#include <stdint.h>
#include "oscl/encoder/api.h"
#include "oscl/endian/decoder/api.h"

/** */
namespace Oscl {
/** */
namespace BT {
/** */
namespace Mesh {
/** */
namespace Model {
/** */
namespace Sensor {
/** */
namespace Server {
/** */
namespace Property {

/** This abstract interface is used by the Sensor
	Server to manipulate its properties.

	4.1 Sensor states

	The Sensor state is a composite state that consists of four states:
		*	the Sensor Descriptor state (see Section 4.1.1), which is constant
			throughout the lifetime of an element;
		*	the Sensor Setting and
		*	Sensor Cadence states, which can be configured; and
		*	the measurement value, which may be represented as
			*	a single data point Sensor Data state (see Section 4.1.4) or
			*	as a column of a series of data points, such as a histogram
				Sensor Series Column state (see Section 4.1.5).

	The measurement value can change over time.

 */
class Api {
	public:
		/** Returns the sensor's 16-bit Property-ID.
		 */
		virtual uint16_t	getPropertyID() const noexcept=0;

		/** This operation instructs the Property to encode its
			16-bit Property-ID. This is used by the Server when
			building a Sensor Descriptor Status message.
		 */
		virtual void	encodeDescriptor(Oscl::Encoder::Api& encoder) const noexcept=0;

		/** This operation instructs the Property to encode its
			Property-ID and current status.
			This is ued by the Server when building a Sensor Status message.
		 */
		virtual void	encodeStatus(Oscl::Encoder::Api& encoder) const noexcept=0;

		/** This operation instructs the Property to encode its
			16-bit Property-ID and column data.

			The rawValueX holds the variable-length argument to the
			Sensor Column Status Get message.

			The property-id and rawValueX have already been encoded when this
			is invoked.

			This is used by the Server when building a Sensor Column Status message.
		 */
		virtual void	encodeColumnStatus(
							Oscl::Encoder::Api& encoder,
							Oscl::Endian::Decoder::Api& rawValueX
							) const noexcept=0;

		/** This operation instructs the Property to encode its
			16-bit Property-ID and series data.

			The rawValueX1AndX2 holds the variable-length arguments
			to the Sensor Series Status Get message.

			The property-id has already been encoded when this is invoked.

			This is used by the Server when building a Sensor Series Status message.
		 */
		virtual void	encodeSeriesStatus(
							Oscl::Encoder::Api&	encoder,
							Oscl::Endian::Decoder::Api&	rawValueX1AndX2
							) const noexcept=0;

	};

}
}
}
}
}
}
}

#endif
