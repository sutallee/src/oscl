/*
   Copyright (C) 2020 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_bluetooth_mesh_model_sensor_server_property_temperature8_context_apih_
#define _oscl_bluetooth_mesh_model_sensor_server_property_temperature8_context_apih_

#include <stdint.h>

/** */
namespace Oscl {
/** */
namespace BT {
/** */
namespace Mesh {
/** */
namespace Model {
/** */
namespace Sensor {
/** */
namespace Server {
/** */
namespace Property {
/** */
namespace Temperature8 {
/** */
namespace Context {

/** This abstract interface is used by the Temperature8
	Property to obtain information from the application
	context.
 */
class Api {
	public:
		/**
			The Temperature 8 characteristic is used to represent a measure
			of temperature with a unit of 0.5 degree Celsius.

			Unit:		Celsius
			Resolution:	1/2 degree.
			Format:		sint8
			Minimum:	-64 C
			maximum:	63.5 C
			Decimal Exponent:	0
			Binary Exponent:	-1
			Multipler:			1

			* A value of 0xFF represents 'value is not known'.
		 */
		virtual int8_t	getStatus() const noexcept=0;
	};

}
}
}
}
}
}
}
}
}

#endif
