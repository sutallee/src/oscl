/*
   Copyright (C) 2020 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "part.h"
#include "oscl/bluetooth/mesh/model/sensor/status/mpid.h"

using namespace Oscl::BT::Mesh::Model::Sensor::Server::Property::Voltage;

Part::Part(
	Oscl::BT::Mesh::Model::
	Sensor::Server::Property::
	Voltage::Context::Api&		context,
	uint16_t					propertyID,
	uint16_t					positiveTolerance,
	uint16_t					negativeTolerance,
	uint8_t						samplingFunction,
	uint8_t						measurementPeriod,
	uint8_t						updateInterval
	) noexcept:
		_context(context),
		_propertyID(propertyID),
		_positiveTolerance(positiveTolerance),
		_negativeTolerance(negativeTolerance),
		_samplingFunction(samplingFunction),
		_measurementPeriod(measurementPeriod),
		_updateInterval(updateInterval),
		_propertyComposer(
			*this,
			&Part::getPropertyID,
			&Part::encodeDescriptor,
			&Part::encodeStatus,
			&Part::encodeColumnStatus,
			&Part::encodeSeriesStatus
			)
		{
	}

Oscl::BT::Mesh::Model::
Sensor::Server::Property::Item&	Part::getSensortItem() noexcept{
	return _propertyComposer;
	}

uint16_t	Part::getPropertyID() const noexcept{
	return _propertyID;
	}

void	Part::encodeDescriptor(Oscl::Encoder::Api& encoder) const noexcept{
	/*
		All multiple-octet numeric values shall be little endian.

		Where data structures are made of multiple fields,
		the fields are listed in the tables from top to bottom,
		and they appear in the corresponding figures from left to right
		(i.e., the top row of the table corresponds to the left of the figure).
		Table 1.1 and Figure 1.3 show an example data structure made
		up of multiple fields.

		Encoded Descriptor
		16-bit	Property ID
		12-bit	Positive Tolerance
		12-bit	Negative Tolerance
		8-bit	Sampling Function
		8-bit	Measurement Period
		8-bit	Update Interval

		| 16-bit | 12-bit | 12-bit | 8-bit | 8-bit | 8-bit |

		| 16-bit |
		| 12-bit | 12-bit |
		| 8-bit | 8-bit | 8-bit |
	 */
	encoder.encode(_propertyID);

	// The combination of the 12-bit positiveTolerance
	// and 12-bit negativeTolerance is 24-bits.
	uint32_t
	tolerance	= _positiveTolerance;
	tolerance	<<=	12;
	tolerance	|= _negativeTolerance;
	encoder.encode24Bit(tolerance);

	encoder.encode(_samplingFunction);
	encoder.encode(_measurementPeriod);
	encoder.encode(_updateInterval);
	}

void	Part::encodeStatus(Oscl::Encoder::Api& encoder) const noexcept{

	Oscl::BT::Mesh::Model::Sensor::Status::encodeMPID(
		encoder,
		_propertyID,
		sizeof(uint16_t)
		);

	uint16_t	voltageStatus	= _context.getStatus();

	encoder.encode(voltageStatus);
	}

void	Part::encodeColumnStatus(
			Oscl::Encoder::Api&	encoder,
			Oscl::Endian::Decoder::Api&	rawValueX
			) const noexcept{
	/*
		A Sensor Server shall send a Sensor Column Status message,
		setting the Property ID field to the value of the Sensor Property ID state,
		the Raw Value X to the value of the Sensor Raw Value X state,
		the Column Width field to the value of the Sensor Column Width state,
		and the Raw Value Y field to the value of the Sensor Raw Value Y state.

		If there is no Sensor Raw Value Y state present that matches
		the Raw Value X state as defined by the referenced device property,
		the Column Width field and the Raw Value Y field shall be omitted.

		The message shall be sent as a response to a
		Sensor Column Get message (see Section 4.2.15) or
		may be sent at any time as an unsolicited message.
	 */

	/*
		This implementation is a Sensor Data type that
		does not support Column operations.
	 */
	}

void	Part::encodeSeriesStatus(
			Oscl::Encoder::Api&	encoder,
			Oscl::Endian::Decoder::Api&	rawValueX1AndX2
			) const noexcept{
	/*
		A Sensor Server shall send a Sensor Series Status message,
		setting the Property ID field to the value of the Property ID state,
		the following triplets of the
		Raw Value X to the value of the Sensor Raw Value X state,
		the Column Width field to the value of the Sensor Column Width state,
		and the Raw Value Y field to the value of the Sensor Raw Value Y state
		for each value of Raw Value X that is within the inclusive range from
		Raw Value X1 through Raw Value X2 field value of the requesting message.

		When the Raw Value X1 and Raw Value X2 fields are not present
		in the incoming message, the Sensor Server shall report all columns
		of the Property ID state.

		If the requested Property ID is not recognized by the Sensor Server or
		if there is no Sensor Series Column state for requested Property ID,
		then the Raw Value X field, the Sensor Column Width field,
		and the Raw Value Y field shall be omitted.
	 */

	/*
		This implementation is a Sensor Data type that
		does not support Column operations.
	 */
	}


