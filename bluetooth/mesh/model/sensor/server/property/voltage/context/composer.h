/*
   Copyright (C) 2020 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_bluetooth_mesh_model_sensor_server_property_voltage_context_composerh_
#define _oscl_bluetooth_mesh_model_sensor_server_property_voltage_context_composerh_

#include "api.h"

/** */
namespace Oscl {
/** */
namespace BT {
/** */
namespace Mesh {
/** */
namespace Model {
/** */
namespace Sensor {
/** */
namespace Server {
/** */
namespace Property {
/** */
namespace Voltage {
/** */
namespace Context {

/** */
template <class Context>
class Composer : public Api {
	private:
		/** */
		Context&	_context;

		/** */
		uint16_t	(Context::*_getStatus)() const;

	public:
		/** */
		Composer(
			Context&	context,
			uint16_t	(Context::*getStatus)() const
			) noexcept;

		/** 
			The Voltage characteristic is used to represent a measure
			of positive electric potential difference in units of volts.

			Unit:		Volt
			Resolution:	1/64V.
			Format:		uint16
			Minimum:	0.0V
			maximum:	1022.0V
			Decimal Exponent:	0
			Binary Exponent:	-6
			Multipler:			1

			* A value of 0xFFFF represents 'value is not known'.
			* The minimum representable value represents the minimum value or lower
			* The maximum representable value represents the maximum value or higher.
		 */
		uint16_t	getStatus() const noexcept;
	};

template <class Context>
Composer<Context>::Composer(
	Context&	context,
	uint16_t	(Context::*getStatus)() const
	)noexcept:
		_context(context),
		_getStatus(getStatus)
		{
	}

template <class Context>
uint16_t Composer<Context>::getStatus() const noexcept{
	return (_context.*_getStatus)();
	}

}
}
}
}
}
}
}
}
}

#endif
