/*
   Copyright (C) 2020 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_bluetooth_mesh_model_sensor_server_property_voltage_parth_
#define _oscl_bluetooth_mesh_model_sensor_server_property_voltage_parth_

#include "oscl/bluetooth/mesh/model/sensor/server/property/composer.h"
#include "oscl/bluetooth/mesh/model/sensor/server/property/voltage/context/api.h"

/** */
namespace Oscl {
/** */
namespace BT {
/** */
namespace Mesh {
/** */
namespace Model {
/** */
namespace Sensor {
/** */
namespace Server {
/** */
namespace Property {
/** */
namespace Voltage {

/**
	Voltage characteristic is used to represent a measure
	of positive electric potential difference in units of volts.

        Unit:		Volt
		Resolution:	1/64V.
        Format:		uint16
        Minimum:	0.0
        Maximum:	1022.0
        Decimal Exponent:	0
        Binary Exponent:	-6
        Multipler:			1
        A value of 0xFFFF represents 'value is not known'.
		The minimum representable value represents
		the minimum value or lower,
		the maximum representable value represents
		the maximum value or higher.

	This implementation represents a
	"single data point Sensor data state" a defined
	in Section 4.1.4 of the Mesh Model document.
 */
class Part {
	private:
		/** */
		Oscl::BT::Mesh::Model::
		Sensor::Server::Property::
		Voltage::Context::Api&		_context;

		/** */
		const uint16_t				_propertyID;

		/** */
		const uint16_t				_positiveTolerance;

		/** */
		const uint16_t				_negativeTolerance;

		/** */
		const uint8_t				_samplingFunction;

		/** */
		const uint8_t				_measurementPeriod;

		/** */
		const uint8_t				_updateInterval;

	private:
		/** */
		Oscl::BT::Mesh::Model::
		Sensor::Server::Property::Composer<Part>	_propertyComposer;

	public:
		/** */
		Part(
			Oscl::BT::Mesh::Model::
			Sensor::Server::Property::
			Voltage::Context::Api&		context,
			uint16_t					propertyID,
			uint16_t					positiveTolerance,
			uint16_t					negativeTolerance,
			uint8_t						samplingFunction,
			uint8_t						measurementPeriod,
			uint8_t						updateInterval
			) noexcept;

	/** */
	Oscl::BT::Mesh::Model::
	Sensor::Server::Property::Item&	getSensortItem() noexcept;

	private:
		/** Returns the sensor's 16-bit Property-ID.
		 */
		uint16_t	getPropertyID() const noexcept;

		/** This operation instructs the Property to encode its
			16-bit Property-ID. This is used by the Server when
			building a Sensor Descriptor Status message.
		 */
		void	encodeDescriptor(Oscl::Encoder::Api& encoder) const noexcept;

		/** This operation instructs the Property to encode its
			Property-ID and current status.
			This is ued by the Server when building a Sensor Status message.
		 */
		void	encodeStatus(Oscl::Encoder::Api& encoder) const noexcept;

		/** This operation instructs the Property to encode its
			16-bit Property-ID and column data.
			The rawValueX holds the variable-length argument to the
			Sensor Column Status Get message.
			The property-id and rawValueX have already been encoded when this
			is invoked.
			This is used by the Server when building a Sensor Column Status message.
		 */
		void	encodeColumnStatus(
					Oscl::Encoder::Api& encoder,
					Oscl::Endian::Decoder::Api& rawValueX
					) const noexcept;

		/** This operation instructs the Property to encode its
			16-bit Property-ID and series data.

			The rawValueX1AndX2 holds the variable-length arguments
			to the Sensor Series Status Get message.

			The property-id has already been encoded when this is invoked.

			This is used by the Server when building a Sensor Series Status message.
		 */
		void	encodeSeriesStatus(
					Oscl::Encoder::Api&	encoder,
					Oscl::Endian::Decoder::Api&	rawValueX1AndX2
					) const noexcept;

	};

}
}
}
}
}
}
}
}

#endif
