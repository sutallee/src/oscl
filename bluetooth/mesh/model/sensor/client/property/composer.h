/*
   Copyright (C) 2020 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_bluetooth_mesh_model_sensor_client_property_composerh_
#define _oscl_bluetooth_mesh_model_sensor_client_property_composerh_

#include "item.h"

/** */
namespace Oscl {
/** */
namespace BT {
/** */
namespace Mesh {
/** */
namespace Model {
/** */
namespace Sensor {
/** */
namespace Client {
/** */
namespace Property {

/** */
template <class Context>
class Composer : public Item {
	private:
		/** */
		Context&	_context;

		/** */
		uint16_t	(Context::*_getPropertyID)() const;

		/** */
		bool		(Context::*_processStatus)(
						Oscl::Endian::Decoder::Api&	decoder,
						uint16_t			srcAddress,
						uint16_t			dstAddress,
						uint16_t			propertyID,
						uint8_t				valueLength
						);
	public:
		/** */
		Composer(
			Context&	context,
			uint16_t	(Context::*getPropertyID)() const,
			bool		(Context::*processStatus)(
							Oscl::Endian::Decoder::Api&	decoder,
							uint16_t			srcAddress,
							uint16_t			dstAddress,
							uint16_t			propertyID,
							uint8_t				valueLength
							)
			) noexcept;

		/** Returns the sensor's 16-bit Property-ID.
		 */
		uint16_t	getPropertyID() const noexcept;

		/** This operation instructs the Property to process the
			received value.

			The MPID (property-id and value-length) have already
			been decoded when this is invoked. Only the variable
			length value remains to be decoded. The implementation
			must not decode more than valueLength octets.

			RETURN: true for decoder overflow failure.
		 */
		bool	processStatus(
					Oscl::Endian::Decoder::Api&	decoder,
					uint16_t			srcAddress,
					uint16_t			dstAddress,
					uint16_t			propertyID,
					uint8_t				valueLength
					) noexcept;
	};

template <class Context>
Composer<Context>::Composer(
	Context&	context,
	uint16_t	(Context::*getPropertyID)() const,
	bool		(Context::*processStatus)(
					Oscl::Endian::Decoder::Api&	decoder,
					uint16_t			srcAddress,
					uint16_t			dstAddress,
					uint16_t			propertyID,
					uint8_t				valueLength
					)
	)noexcept:
		_context(context),
		_getPropertyID(getPropertyID),
		_processStatus(processStatus)
		{
	}

template <class Context>
uint16_t Composer<Context>::getPropertyID() const noexcept{
	return (_context.*_getPropertyID)();
	}

template <class Context>
bool Composer<Context>::processStatus(
		Oscl::Endian::Decoder::Api&	decoder,
		uint16_t			srcAddress,
		uint16_t			dstAddress,
		uint16_t			propertyID,
		uint8_t				valueLength
		) noexcept{
	return (_context.*_processStatus)(
			decoder,
			srcAddress,
			dstAddress,
			propertyID,
			valueLength
			);
	}

}
}
}
}
}
}
}

#endif
