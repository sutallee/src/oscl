/*
   Copyright (C) 2020 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_bluetooth_mesh_model_sensor_client_property_apih_
#define _oscl_bluetooth_mesh_model_sensor_client_property_apih_

#include <stdint.h>
#include "oscl/encoder/api.h"
#include "oscl/endian/decoder/api.h"

/** */
namespace Oscl {
/** */
namespace BT {
/** */
namespace Mesh {
/** */
namespace Model {
/** */
namespace Sensor {
/** */
namespace Client {
/** */
namespace Property {

/** This abstract interface is used by the Sensor
	Client to manipulate its properties.

	4.1 Sensor states

	The Sensor state is a composite state that consists of four states:
		*	the Sensor Descriptor state (see Section 4.1.1), which is constant
			throughout the lifetime of an element;
		*	the Sensor Setting and
		*	Sensor Cadence states, which can be configured; and
		*	the measurement value, which may be represented as
			*	a single data point Sensor Data state (see Section 4.1.4) or
			*	as a column of a series of data points, such as a histogram
				Sensor Series Column state (see Section 4.1.5).

	The measurement value can change over time.

 */
class Api {
	public:
		/** Returns the sensor's 16-bit Property-ID.
		 */
		virtual uint16_t	getPropertyID() const noexcept=0;

		/** This operation instructs the Property to process the
			received value.

			The MPID (property-id and value-length) have already
			been decoded when this is invoked. Only the variable
			length value remains to be decoded. The implementation
			must not decode more than valueLength octets.

			RETURN: true for decoder overflow failure.
		 */
		virtual bool	processStatus(
							Oscl::Endian::Decoder::Api&	decoder,
							uint16_t			srcAddress,
							uint16_t			dstAddress,
							uint16_t			propertyID,
							uint8_t				valueLength
							) noexcept=0;
	};

}
}
}
}
}
}
}

#endif
