/*
   Copyright (C) 2020 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_bluetooth_mesh_model_sensor_client_property_voltage_context_apih_
#define _oscl_bluetooth_mesh_model_sensor_client_property_voltage_context_apih_

#include <stdint.h>

/** */
namespace Oscl {
/** */
namespace BT {
/** */
namespace Mesh {
/** */
namespace Model {
/** */
namespace Sensor {
/** */
namespace Client {
/** */
namespace Property {
/** */
namespace Voltage {
/** */
namespace Context {

/** This abstract interface is used by the Voltage Property
	Client to communicate and control a specific application
	context. The implementation is platform and application
	specific.
 */
class Api {
	public:
		/** This operation allows the context to record changes
			of state that are sent to this client.

			The voltageLevel has a resolution of in 1/64 Volts.

			org.bluetooth.characteristic.voltage.xml
		 */
		virtual void	recordStatus(
							uint16_t	src,
							uint16_t	dst,
							uint16_t	propertyID,
							uint16_t	voltageLevel
							) noexcept=0;
	};

}
}
}
}
}
}
}
}
}

#endif
