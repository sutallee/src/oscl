/*
   Copyright (C) 2020 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "part.h"
#include "oscl/bluetooth/mesh/model/sensor/status/mpid.h"
#include "oscl/error/info.h"

using namespace Oscl::BT::Mesh::Model::Sensor::Client::Property::Temperature8;

//#define DEBUG_TRACE

Part::Part(
	Oscl::BT::Mesh::Model::
	Sensor::Client::Property::
	Temperature8::Context::Api&	context,
	uint16_t					propertyID
	) noexcept:
		_context(context),
		_propertyID(propertyID),
		_propertyComposer(
			*this,
			&Part::getPropertyID,
			&Part::processStatus
			)
		{
	}

Oscl::BT::Mesh::Model::
Sensor::Client::Property::Item&	Part::getSensortItem() noexcept{
	return _propertyComposer;
	}

uint16_t	Part::getPropertyID() const noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s: property-id 0x%4.4X\n",
		__PRETTY_FUNCTION__,
		_propertyID
		);
	#endif

	return _propertyID;
	}

bool	Part::processStatus(
			Oscl::Endian::Decoder::Api&	decoder,
			uint16_t			srcAddress,
			uint16_t			dstAddress,
			uint16_t			propertyID,
			uint8_t				valueLength
			) noexcept{


	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif

	if(!valueLength){
		/*	The server did not encode a value.
			Strange, but not an invalid encoding.
		 */
		#ifdef DEBUG_TRACE
		Oscl::Error::Info::log(
			"%s: valueLength 0\n",
			__PRETTY_FUNCTION__
			);
		#endif
		return false;
		}

	if(valueLength != sizeof(uint16_t)){
		/*	This is an invalid encoding.
			We *could* (should?) strip
			valueLength octets and return
			false. However, if the server
			has blown the encoding of the
			length field, and encoded a
			proper value, we fuck any subsequent
			properties.
			On the other hand, if the valueLength
			was encoded correctly and the server
			has truely appended the wrong number
			of octets, then NOT stripping valueLength
			octets will, also, fuck subsequent
			properties.
			We choose to fail loudly.
		 */
		Oscl::Error::Info::log(
			"%s: valueLength(%u) != 2\n",
			__PRETTY_FUNCTION__,
			valueLength
			);
		return true;
		}

	int8_t	temperature;

	decoder.le().decode(temperature);

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s: temperature: 0x%2.2X\n",
		__PRETTY_FUNCTION__,
		temperature
		);
	#endif

	_context.recordStatus(
		srcAddress,
		dstAddress,
		propertyID,
		temperature
		);

	return false;
	}


