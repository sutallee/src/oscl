/*
   Copyright (C) 2020 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_bluetooth_mesh_model_sensor_client_property_temperature8_parth_
#define _oscl_bluetooth_mesh_model_sensor_client_property_temperature8_parth_

#include "oscl/bluetooth/mesh/model/sensor/client/property/composer.h"
#include "oscl/bluetooth/mesh/model/sensor/client/property/temperature8/context/api.h"

/** */
namespace Oscl {
/** */
namespace BT {
/** */
namespace Mesh {
/** */
namespace Model {
/** */
namespace Sensor {
/** */
namespace Client {
/** */
namespace Property {
/** */
namespace Temperature8 {

/**
	The Temperature 8 characteristic is used to represent a measure
	of temperature with a unit of 0.5 degree Celsius.

	Unit:		Celsius
	Resolution:	1/2 degree.
	Format:		sint8
	Minimum:	-64 C
	maximum:	63.5 C
	Decimal Exponent:	0
	Binary Exponent:	-1
	Multipler:			1

		* A value of 0xFF represents 'value is not known'.

	This implementation represents a
	"single data point Sensor data state" a defined
	in Section 4.1.4 of the Mesh Model document.
 */
class Part {
	private:
		/** */
		Oscl::BT::Mesh::Model::
		Sensor::Client::Property::
		Temperature8::Context::Api&		_context;

		/** */
		const uint16_t				_propertyID;

	private:
		/** */
		Oscl::BT::Mesh::Model::
		Sensor::Client::Property::Composer<Part>	_propertyComposer;

	public:
		/** */
		Part(
			Oscl::BT::Mesh::Model::
			Sensor::Client::Property::
			Temperature8::Context::Api&		context,
			uint16_t					propertyID
			) noexcept;

	/** */
	Oscl::BT::Mesh::Model::
	Sensor::Client::Property::Item&	getSensortItem() noexcept;

	private:
		/** Returns the sensor's 16-bit Property-ID.
		 */
		uint16_t	getPropertyID() const noexcept;

		/** This operation instructs the Property to process the
			received value.

			The MPID (property-id and value-length) have already
			been decoded when this is invoked. Only the variable
			length value remains to be decoded. The implementation
			must not decode more than valueLength octets.

			RETURN: true for decoder overflow failure.
		 */
		bool	processStatus(
					Oscl::Endian::Decoder::Api&	decoder,
					uint16_t			srcAddress,
					uint16_t			dstAddress,
					uint16_t			propertyID,
					uint8_t				valueLength
					) noexcept;
	};

}
}
}
}
}
}
}
}

#endif
