/*
   Copyright (C) 2020 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_bluetooth_mesh_model_sensor_client_property_temperature8_context_itemcomph_
#define _oscl_bluetooth_mesh_model_sensor_client_property_temperature8_context_itemcomph_

#include "item.h"

/** */
namespace Oscl {
/** */
namespace BT {
/** */
namespace Mesh {
/** */
namespace Model {
/** */
namespace Sensor {
/** */
namespace Client {
/** */
namespace Property {
/** */
namespace Temperature8 {
/** */
namespace Context {

/**	This template class implements a kind of GOF decorator
	pattern allows the context to employ composition instead
	of inheritance. Frequently, a context may need to implement
	more than one interface that may result in method name
	clashes. This template solves the problem by implementing
	the interface and then invoking member function pointers
	in the context instead. One instance of this object is
	created in the context for each interface of this type used
	in the context. Each instance is constructed such that it
	refers to different member functions within the context.
	The interface of this object is passed to any object that
	requires the interface. When the object invokes any of the
	operations of the API, the corresponding member function
	of the context will subsequently be invoked.
 */
template <class Context>
class ItemComposer : public Item {
	private:
		/** A reference to the Context.
		 */
		Context&	_context;

	private:
		/**	*/
		void	(Context::*_recordStatus)(	
					uint16_t	src,
					uint16_t	dst,
					uint16_t	propertyID,
					int8_t		temperatureLevel
					);

	public:
		/** */
		ItemComposer(
			Context&		context,
			void	(Context::*recordStatus)(	
						uint16_t	src,
						uint16_t	dst,
						uint16_t	propertyID,
						int8_t		temperatureLevel
						)
			) noexcept;

	private:
		/**	*/
		void	recordStatus(	
					uint16_t	src,
					uint16_t	dst,
					uint16_t	propertyID,
					int8_t		temperatureLevel
					) noexcept;

	};

template <class Context>
ItemComposer<Context>::ItemComposer(
			Context&		context,
			void	(Context::*recordStatus)(	
						uint16_t	src,
						uint16_t	dst,
						uint16_t	propertyID,
						int8_t		temperatureLevel
						)
			) noexcept:
		_context(context),
		_recordStatus(recordStatus)
		{
	}

template <class Context>
void	ItemComposer<Context>::recordStatus(	
			uint16_t	src,
			uint16_t	dst,
			uint16_t	propertyID,
			int8_t		temperatureLevel
			) noexcept{

	(_context.*_recordStatus)(
		src,
		dst,
		propertyID,
		temperatureLevel
		);
	}

}
}
}
}
}
}
}
}
}

#endif
