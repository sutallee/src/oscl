/*
   Copyright (C) 2020 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "mpid.h"
#include "oscl/error/info.h"

//#define DEBUG_TRACE

void	Oscl::BT::Mesh::Model::Sensor::Status::encodeMPID(
			Oscl::Encoder::Api& encoder,
			uint16_t			propertyID,
			uint8_t				valueLength
			) noexcept{

	if((propertyID < 2048)){
		const uint8_t
		mpidFormatAndLen	=
				(0<<0)				// Format 0
			|	(valueLength<<1)	// Length
			;

		uint16_t
		mpid	= (propertyID << 5);	// Shift and chop-off msbs
		mpid	|= mpidFormatAndLen;

		#ifdef DEBUG_TRACE
		Oscl::Error::Info::log(
			"%s: propertyID: 0x%4.4X, valueLength: 0x%4.4X, mpid: 0x%4.4X\n",
			__PRETTY_FUNCTION__,
			propertyID,
			valueLength,
			mpid
			);
		#endif

		encoder.encode(mpid);
		}
	else {
		const uint8_t
		mpidFormatAndLen	=
				(1<<0)				// Format 1
			|	(valueLength<<1)	// Length
			;

		#ifdef DEBUG_TRACE
		Oscl::Error::Info::log(
			"%s: propertyID: 0x%4.4X, valueLength: 0x%4.4X, mpidFormatAndLen: %u\n",
			__PRETTY_FUNCTION__,
			propertyID,
			valueLength,
			mpidFormatAndLen
			);
		#endif

		encoder.encode(mpidFormatAndLen);
		encoder.encode(propertyID);
		}
	}

bool	Oscl::BT::Mesh::Model::Sensor::Status::decodeMPID(
			Oscl::Endian::Decoder::Api& decoder,
			uint16_t&			propertyID,
			uint8_t	&			valueLength
			) noexcept{

	Oscl::Decoder::Api&	decapi	= decoder.le();

	uint8_t	format;
	decapi.decode(format);

	if(decapi.underflow()){
		/*	This is not necessarily an error
			for the client, but may just be
			that there are no further MPIDs.
		 */
		return true;
		}

	bool
	longMPID	= (format & (1<<0))?true:false;

	if(longMPID){
		uint16_t	pid;
		decapi.decode(pid);
		if(decapi.underflow()){
			/*	This should not happen and indicates
				an encoding error by the server.
			 */
			return true;
			}
		propertyID	= pid;
		valueLength	=  (format >> 1);
		return false;
		}

	uint8_t	msb;

	decapi.decode(msb);

	if(decapi.underflow()){
		/*	This should not happen and indicates
			an encoding error by the server.
		 */
		return true;
		}

	uint16_t
	mpid	= msb;
	mpid	<<= 8;
	mpid	|= format;

	propertyID	= (mpid >> 5);
	valueLength	= ((format & 0x1F) >> 1);

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s: propertyID: 0x%4.4X, valueLength: 0x%4.4X, format: 0x%2.2X, msb: 0x%2.2X, mpid: 0x%4.4X\n",
		__PRETTY_FUNCTION__,
		propertyID,
		valueLength,
		format,
		msb,
		mpid
		);
		#endif

	return false;
	}

