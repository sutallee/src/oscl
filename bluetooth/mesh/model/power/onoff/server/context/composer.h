/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_bluetooth_mesh_model_power_onoff_server_context_composerh_
#define _oscl_bluetooth_mesh_model_power_onoff_server_context_composerh_

#include "api.h"

/** */
namespace Oscl {
/** */
namespace BT {
/** */
namespace Mesh {
/** */
namespace Model {
/** */
namespace Power {
/** */
namespace OnOff {
/** */
namespace Server {
/** */
namespace Context {

/** */
template <class Context>
class Composer : public Api {
	private:
		/** */
		Context&	_context;

		/**	*/
		uint8_t	(Context::*_getOnPowerUpState)();

	public:
		/** */
		Composer(
			Context&	context,
			void	(Context::*getOnPowerUpState)()
			) noexcept;

		/**	*/
		uint8_t	getOnPowerUpState() noexcept;
	};

template <class Context>
Composer<Context>::Composer(
	Context&	context,
	void	(Context::*getOnPowerUpState)()
	)noexcept:
		_context(context),
		_getOnPowerUpState(getOnPowerUpState)
		{
	}

template <class Context>
void Composer<Context>::getOnPowerUpState() noexcept{
	(_context.*_getOnPowerUpState)();
	}

}
}
}
}
}
}
}
}

#endif
