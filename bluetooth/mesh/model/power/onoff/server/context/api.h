/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_bluetooth_mesh_model_power_onoff_server_context_apih_
#define _oscl_bluetooth_mesh_model_power_onoff_server_context_apih_

#include <stdint.h>

/** */
namespace Oscl {
/** */
namespace BT {
/** */
namespace Mesh {
/** */
namespace Model {
/** */
namespace Power {
/** */
namespace OnOff {
/** */
namespace Server {
/** */
namespace Context {

/** This abstract interface is used by the Generic PowerOn
	Generic Power OnOff Server (0x1006) and the Generic Power
	OnOff Setup Server (0x1007) to control a specific application
	context. The implementation and behavior is platform and
	application specific and varies depending with model extensions.
 */
class Api {
	public:
		/** RETURNS one of:
			0x00	:	Off.  After being powered up,
						the element is in an off state.
			0x01	:	Default. After being powered up,
						the element is in an On state and
						uses default state values.
			0x02	:	Restore. If a transition was in progress
						when powered down, the element restores
						the target state when powered up.
						Otherwise the element restores the
						state it was in when powered down.
		 */
		virtual uint8_t	getOnPowerUpState() noexcept=0;

		/** This operation will do nothing for models
			that do not implement the Generic Power
    		OnOff Setup Server (model: 0x1007).

			The state argument is one of:
			0x00	:	Off.  After being powered up,
						the element is in an off state.
			0x01	:	Default. After being powered up,
						the element is in an On state and
						uses default state values.
			0x02	:	Restore. If a transition was in progress
						when powered down, the element restores
						the target state when powered up.
						Otherwise the element restores the
						state it was in when powered down.
		 */
		virtual void	setOnPowerUpState(uint8_t state) noexcept=0;
	};

}
}
}
}
}
}
}
}

#endif
