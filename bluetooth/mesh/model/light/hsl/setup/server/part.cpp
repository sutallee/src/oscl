/*
   Copyright (C) 2021 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <new>
#include "part.h"
#include "oscl/endian/decoder/linear/part.h"
#include "oscl/encoder/le/base.h"
#include "oscl/encoder/be/base.h"
#include "oscl/error/info.h"
#include "oscl/bluetooth/mesh/address/virtual.h"
#include "oscl/bluetooth/mesh/address/types.h"
#include "oscl/bluetooth/mesh/address/values.h"
#include "oscl/bluetooth/mesh/publication/utils.h"
#include "oscl/bluetooth/mesh/model/opcode/decoder.h"
#include "oscl/bluetooth/mesh/constants.h"
#include "oscl/bluetooth/mesh/sigmodelid.h"

using namespace Oscl::BT::Mesh::Model::Light::Hsl::Setup::Server;

//#define DEBUG_TRACE
//#define DEBUG_TRACE_TX_STATUS

Part::Part(
	Oscl::Pdu::Memory::Api&			freeStoreApi,
	Oscl::Timer::Factory::Api&		timerFactoryApi,
	Oscl::BT::Mesh::Node::Api&		nodeApi,
	Oscl::BT::Mesh::Element::
	Symbiont::Kernel::Api&			kernelApi,
	Oscl::BT::Mesh::
	Transport::Lower::TX::Api&		lowerTransportApi,
	Oscl::BT::Mesh::Network::Api&	networkApi,
	Oscl::BT::Mesh::Model::
	Light::Hsl::Setup::Server::
	Context::Api&					contextApi,
	Oscl::BT::Mesh::Model::
	Light::Hsl::Setup::Server::
	IO::Api&						ioApi,
	uint16_t						elementAddress,
	bool							allowSegmentedStatus
	) noexcept:
		_timerFactoryApi(timerFactoryApi),
		_contextApi(contextApi),
		_ioApi(ioApi),
		_allowSegmentedStatus(allowSegmentedStatus),
		_genericOnOffAppServerContext(
			*this,
			&Part::genericOnOffServerProcessAccessMessage,
			&Part::genericOnOffServerSigModelIdMatch,
			&Part::genericOnOffServerVendorModelIdMatch,
			&Part::genericOnOffServerModelSupportsAppKeyBinding,
			&Part::genericOnOffServerGetPublicationApi,
			&Part::genericOnOffServerGetPublicationAppKeyIndex,
			&Part::getSubscriptionApi,
			&Part::getSubscriptionServerApi,
			&Part::genericOnOffServerStop
			),
		_genericOnPowerUpAppServerContext(
			*this,
			&Part::genericOnPowerUpServerProcessAccessMessage,
			&Part::genericOnPowerUpServerSigModelIdMatch,
			&Part::genericOnPowerUpServerVendorModelIdMatch,
			&Part::genericOnPowerUpServerModelSupportsAppKeyBinding,
			&Part::genericOnPowerUpServerGetPublicationApi,
			&Part::genericOnPowerUpServerGetPublicationAppKeyIndex,
			&Part::getSubscriptionApi,
			&Part::getSubscriptionServerApi,
			&Part::genericOnPowerUpServerStop
			),
		_genericLevelAppServerContext(
			*this,
			&Part::genericLevelServerProcessAccessMessage,
			&Part::genericLevelServerSigModelIdMatch,
			&Part::genericLevelServerVendorModelIdMatch,
			&Part::genericLevelServerModelSupportsAppKeyBinding,
			&Part::genericLevelServerGetPublicationApi,
			&Part::genericLevelServerGetPublicationAppKeyIndex,
			&Part::getSubscriptionApi,
			&Part::getSubscriptionServerApi,
			&Part::genericLevelServerStop
			),
		_lightLightnessAppServerContext(
			*this,
			&Part::lightLightnessServerProcessAccessMessage,
			&Part::lightLightnessServerSigModelIdMatch,
			&Part::lightLightnessServerVendorModelIdMatch,
			&Part::lightLightnessServerModelSupportsAppKeyBinding,
			&Part::lightLightnessServerGetPublicationApi,
			&Part::lightLightnessServerGetPublicationAppKeyIndex,
			&Part::getSubscriptionApi,
			&Part::getSubscriptionServerApi,
			&Part::lightLightnessServerStop
			),
		_subscriptionServerPart(
			contextApi.getSubscriptionPersistApi(),
			kernelApi.getSubscriptionRegisterApi()
			),
		_genericOnOffAppServerPart(
			_genericOnOffAppServerContext,
			freeStoreApi,
			nodeApi,
			lowerTransportApi,
			networkApi,
			contextApi.getOnOffKeyPersistApi(),
			elementAddress
			),
		_genericOnPowerUpAppServerPart(
			_genericOnPowerUpAppServerContext,
			freeStoreApi,
			nodeApi,
			lowerTransportApi,
			networkApi,
			contextApi.getOnPowerUpKeyPersistApi(),
			elementAddress
			),
		_genericLevelAppServerPart(
			_genericLevelAppServerContext,
			freeStoreApi,
			nodeApi,
			lowerTransportApi,
			networkApi,
			contextApi.getLevelKeyPersistApi(),
			elementAddress
			),
		_lightLightnessAppServerPart(
			_lightLightnessAppServerContext,
			freeStoreApi,
			nodeApi,
			lowerTransportApi,
			networkApi,
			contextApi.getLightnessKeyPersistApi(),
			elementAddress
			),
		_onOffPublicationComposer(
			*this,
			&Part::onOffPublish
			),
		_genericOnOffServerPublicationPart(
            _onOffPublicationComposer,
            timerFactoryApi,
            contextApi.getOnOffPublicationPersistApi()
			),
		_onOffTidPeriodicTimerApi(
			*timerFactoryApi.allocate()
			),
		_levelTidPeriodicTimerApi(
			*timerFactoryApi.allocate()
			),
		_lightnessTidPeriodicTimerApi(
			*timerFactoryApi.allocate()
			),
		_lightnessTransitionTimerApi(
			*timerFactoryApi.allocate()
			),
		_lightnessControlComposer(
			*this,
			&Part::lightnessUpdate
			),
		_lightnessTransitionAdaptor(
			_lightnessControlComposer,
			_lightnessTransitionTimerApi,
			getInitialLightnessState(),
			200,	// stepTimeInMs
			this
			),
		_genericOnOffTransceiver(
			*this,	// _onOffTransitionAdaptor,
			_genericOnOffAppServerPart.getUpperTransportApi(),
			_onOffTidPeriodicTimerApi,
			allowSegmentedStatus
			),
		_genericOnPowerUpTransceiver(
			*this,
			_genericOnPowerUpAppServerPart.getUpperTransportApi(),
			allowSegmentedStatus
			),
		_genericLevelTransceiver(
			*this,
			_genericLevelAppServerPart.getUpperTransportApi(),
			_levelTidPeriodicTimerApi,
			allowSegmentedStatus
			),
		_lightLightnessTransceiver(
			_lightnessTransitionAdaptor,
			_lightLightnessAppServerPart.getUpperTransportApi(),
			_lightnessTidPeriodicTimerApi,
			allowSegmentedStatus
			),
		_receiver(
			*this,
			&Part::stop,
			&Part::processAccessMessage,
			&Part::getSigModelApi,
			&Part::getVendorModelApi,
			&Part::wantsDestination
			),
		_onOffObserver(
			*this,
			&Part::onOffObserverUpdate
			)
		{

	_receivers.put(&_genericOnOffAppServerPart);
	_receivers.put(&_genericOnPowerUpAppServerPart);
	_receivers.put(&_genericLevelAppServerPart);
	_receivers.put(&_lightLightnessAppServerPart);

	_ioApi.getSubjectApi().attach(_onOffObserver);
	}

Part::~Part() noexcept{
	_timerFactoryApi.free(_onOffTidPeriodicTimerApi);
	_timerFactoryApi.free(_levelTidPeriodicTimerApi);
	_timerFactoryApi.free(_lightnessTidPeriodicTimerApi);

	_timerFactoryApi.free(_lightnessTransitionTimerApi);
	}

Oscl::BT::Mesh::Model::RX::Item&	Part::getModelRxItem() noexcept{
	return _receiver;
	}

uint16_t	Part::getInitialLightnessState() const noexcept{

	/* 6.1.2.2.4 Binding with the Generic OnPowerUp state
	 */

	switch(_contextApi.getOnPowerUpPersistApi().getPowerUpState()){

		case 0x02:{ // Restore

			uint16_t
			state	= _contextApi.getLightLightnessStatePersistApi().getLightLightnessTargetState();

			#ifdef DEBUG_TRACE
			Oscl::Error::Info::log(
				"%s: Restore state: %u\n",
				OSCL_PRETTY_FUNCTION,
				state
				);
			#endif

			return state;
			}

		case 0x01:{ // Default

			uint16_t
			defaultState	= _contextApi.getLightLightnessStatePersistApi().getLightLightnessDefaultState();

			if(!defaultState){

				uint16_t
				state	= _contextApi.getLightLightnessStatePersistApi().getLightLightnessLastState();

				#ifdef DEBUG_TRACE
				Oscl::Error::Info::log(
					"%s: Default (0) Last state: %u\n",
					OSCL_PRETTY_FUNCTION,
					state
					);
				#endif

				return state;
				}
			else {
				#ifdef DEBUG_TRACE
				Oscl::Error::Info::log(
					"%s: Default (!0) default state: %u\n",
					OSCL_PRETTY_FUNCTION,
					defaultState
					);
				#endif

				return defaultState;
				}
			}

		case 0x00:{ // Off

			#ifdef DEBUG_TRACE
			Oscl::Error::Info::log(
				"%s: Off state\n",
				OSCL_PRETTY_FUNCTION
				);
			#endif

			}
		default:
			return 0x0000;
		}
	}

void	Part::stop() noexcept{
	for(
		Oscl::BT::Mesh::Model::RX::Item*
		item	= _receivers.first();
		item;
		item	= _receivers.next(item)
		){
		// /home/mike/root/src/oscl/bluetooth/mesh/model/rx/itemcomp.h
		item->stop();
		}
	}

void	Part::processAccessMessage(
			const void*	frame,
			unsigned	length,
			uint16_t	src,
			uint16_t	dst,
			uint16_t	appKeyIndex
			) noexcept{
	for(
		Oscl::BT::Mesh::Model::RX::Item*
		item	= _receivers.first();
		item;
		item	= _receivers.next(item)
		){
		item->processAccessMessage(
			frame,
			length,
			src,
			dst,
			appKeyIndex
			);
		}
	}

Oscl::BT::Mesh::Model::Api*	Part::getSigModelApi(uint16_t modelID) noexcept{

	for(
		Oscl::BT::Mesh::Model::RX::Item*
		item	= _receivers.first();
		item;
		item	= _receivers.next(item)
		){
		Oscl::BT::Mesh::Model::Api*
		api	= item->getSigModelApi(modelID);
		if(api){
			return api;
			}
		}

	return 0;
	}

Oscl::BT::Mesh::Model::Api*	Part::getVendorModelApi(uint16_t cid, uint16_t modelID) noexcept{

	for(
		Oscl::BT::Mesh::Model::RX::Item*
		item	= _receivers.first();
		item;
		item	= _receivers.next(item)
		){
		Oscl::BT::Mesh::Model::Api*
		api	= item->getVendorModelApi(cid,modelID);
		if(api){
			return api;
			}
		}

	return 0;
	}

bool	Part::wantsDestination(uint16_t dst) noexcept{
	for(
		Oscl::BT::Mesh::Model::RX::Item*
		item	= _receivers.first();
		item;
		item	= _receivers.next(item)
		){
		if(item->wantsDestination(dst)){
			return true;
			}
		}
	return false;
	}

void	Part::lightnessUpdate(uint16_t state) noexcept{

	double	lightLightnessActual	= state;

	double	percentage	= lightLightnessActual/65535.0;

	uint32_t	value	= percentage * 255;

	uint32_t
	pixel	=
			value << 16	// red
		|	value << 8	// green
		|	value << 0	// blue
		;

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s: state: %u, %f%%, rgb: 0x%8.8X\n",
		OSCL_PRETTY_FUNCTION,
		state,
		percentage*100,
		pixel
		);
	#endif

	_ioApi.getArgb32Api().update(
		&pixel,
		1
		);
	}

void	Part::genericOnOffServerProcessAccessMessage(
			const void*	frame,
			unsigned	length,
			uint16_t	src,
			uint16_t	dst,
			uint16_t	appKeyIndex
			) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION
		);

	Oscl::Error::Info::hexDump(
		frame,
		length
		);
	#endif

	Oscl::Endian::Decoder::Linear::Part
	leDecoder(
		frame,
		length
		);

	Oscl::Decoder::Api&	decoder	= leDecoder.le();

	uint32_t
	opcode	= Oscl::BT::Mesh::Model::OpCode::decode(decoder);

	using namespace Oscl::BT::Mesh::Model::OpCode;

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s:\n"
		"\topcode: (0x%4.4X)\n",
		OSCL_PRETTY_FUNCTION,
		opcode
		);
	#endif

	const uint8_t*	p	= (const uint8_t*)frame;

	unsigned
	remaining	= decoder.remaining();

	unsigned
	payloadOffset	= length - remaining;

	_genericOnOffTransceiver.receive(
		opcode,
		src,
		dst,
		&p[payloadOffset],
		remaining,
		appKeyIndex
		);

	}

void	Part::onOffObserverUpdate(bool& state) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s: state: %s\n",
		OSCL_PRETTY_FUNCTION,
		state?"ON":"OFF"
		);
	#endif

	_genericOnOffServerPublicationPart.publish();
	}

bool	Part::genericOnOffServerSigModelIdMatch(uint16_t modelID) const noexcept{

	using namespace Oscl::BT::Mesh::SigModelID;

	switch(modelID){
		case genericOnOffServer:
			return true;
		default:
			return false;
		}
	}

bool	Part::genericOnOffServerVendorModelIdMatch(uint16_t vendorID, uint16_t modelID) const noexcept{
	return false;
	}

bool	Part::genericOnOffServerModelSupportsAppKeyBinding() const noexcept{
	return true;
	}

Oscl::BT::Mesh::Publication::Api*	Part::genericOnOffServerGetPublicationApi() noexcept{
	return &_genericOnOffServerPublicationPart;
	}

uint16_t	Part::genericOnOffServerGetPublicationAppKeyIndex() const noexcept{
	return _genericOnOffServerPublicationPart.getPublicationAppKeyIndex();
	}

Oscl::BT::Mesh::Subscription::Api*	Part::getSubscriptionApi() noexcept{
	return &_subscriptionServerPart;
	}

Oscl::BT::Mesh::Subscription::Server::Api*	Part::getSubscriptionServerApi() noexcept{
	return &_subscriptionServerPart;
	}

void	Part::genericOnOffServerStop() noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s:\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif

	_genericOnOffServerPublicationPart.stop();

	_onOffTidPeriodicTimerApi.stop();

	_ioApi.getSubjectApi().detach(_onOffObserver);
	}

void	Part::genericOnPowerUpServerProcessAccessMessage(
			const void*	frame,
			unsigned	length,
			uint16_t	src,
			uint16_t	dst,
			uint16_t	appKeyIndex
			) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION
		);

	Oscl::Error::Info::hexDump(
		frame,
		length
		);
	#endif

	Oscl::Endian::Decoder::Linear::Part
	leDecoder(
		frame,
		length
		);

	Oscl::Decoder::Api&	decoder	= leDecoder.le();

	uint32_t
	opcode	= Oscl::BT::Mesh::Model::OpCode::decode(decoder);

	using namespace Oscl::BT::Mesh::Model::OpCode;

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s:\n"
		"\topcode: (0x%4.4X)\n",
		OSCL_PRETTY_FUNCTION,
		opcode
		);
	#endif

	const uint8_t*	p	= (const uint8_t*)frame;

	unsigned
	remaining	= decoder.remaining();

	unsigned
	payloadOffset	= length - remaining;

	_genericOnPowerUpTransceiver.receive(
		opcode,
		src,
		dst,
		&p[payloadOffset],
		remaining,
		appKeyIndex
		);

	}

bool	Part::genericOnPowerUpServerSigModelIdMatch(uint16_t modelID) const noexcept{

	using namespace Oscl::BT::Mesh::SigModelID;

	switch(modelID){
		case genericPowerOnOffServer:
		case genericPowerOnOffSetupServer:
			return true;
		default:
			return false;
		}
	}

bool	Part::genericOnPowerUpServerVendorModelIdMatch(uint16_t vendorID, uint16_t modelID) const noexcept{
	return false;
	}

bool	Part::genericOnPowerUpServerModelSupportsAppKeyBinding() const noexcept{
	return true;
	}

Oscl::BT::Mesh::Publication::Api*	Part::genericOnPowerUpServerGetPublicationApi() noexcept{
	// FIXME: Does OnPowerUp need to support publication?
	return 0;
	}

uint16_t	Part::genericOnPowerUpServerGetPublicationAppKeyIndex() const noexcept{
	/*	This is only needed if OnPowerUp supports publication.
		If genericOnPowerUpServerGetPublicationApi() returns zero, then
		this will never be called.
	 */
	return 0;
	}

void	Part::genericOnPowerUpServerStop() noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s:\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif
	}

void	Part::genericLevelServerProcessAccessMessage(
			const void*	frame,
			unsigned	length,
			uint16_t	src,
			uint16_t	dst,
			uint16_t	appKeyIndex
			) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION
		);

	Oscl::Error::Info::hexDump(
		frame,
		length
		);
	#endif

	Oscl::Endian::Decoder::Linear::Part
	leDecoder(
		frame,
		length
		);

	Oscl::Decoder::Api&	decoder	= leDecoder.le();

	uint32_t
	opcode	= Oscl::BT::Mesh::Model::OpCode::decode(decoder);

	using namespace Oscl::BT::Mesh::Model::OpCode;

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s:\n"
		"\topcode: (0x%4.4X)\n",
		OSCL_PRETTY_FUNCTION,
		opcode
		);
	#endif

	const uint8_t*	p	= (const uint8_t*)frame;

	unsigned
	remaining	= decoder.remaining();

	unsigned
	payloadOffset	= length - remaining;

	_genericLevelTransceiver.receive(
		opcode,
		src,
		dst,
		&p[payloadOffset],
		remaining,
		appKeyIndex
		);

	}

bool	Part::genericLevelServerSigModelIdMatch(uint16_t modelID) const noexcept{

	using namespace Oscl::BT::Mesh::SigModelID;

	switch(modelID){
		case genericLevelServer:
			return true;
		default:
			return false;
		}
	}

bool	Part::genericLevelServerVendorModelIdMatch(uint16_t vendorID, uint16_t modelID) const noexcept{
	return false;
	}

bool	Part::genericLevelServerModelSupportsAppKeyBinding() const noexcept{
	return true;
	}

Oscl::BT::Mesh::Publication::Api*	Part::genericLevelServerGetPublicationApi() noexcept{
	// FIXME: Does Level need to support publication?
	return 0;
	}

uint16_t	Part::genericLevelServerGetPublicationAppKeyIndex() const noexcept{
	/*	This is only needed if Level supports publication.
		If genericLevelServerGetPublicationApi() returns zero, then
		this will never be called.
	 */
	return 0;
	}

void	Part::genericLevelServerStop() noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s:\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif
	}

void	Part::lightLightnessServerProcessAccessMessage(
			const void*	frame,
			unsigned	length,
			uint16_t	src,
			uint16_t	dst,
			uint16_t	appKeyIndex
			) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION
		);

	Oscl::Error::Info::hexDump(
		frame,
		length
		);
	#endif

	Oscl::Endian::Decoder::Linear::Part
	leDecoder(
		frame,
		length
		);

	Oscl::Decoder::Api&	decoder	= leDecoder.le();

	uint32_t
	opcode	= Oscl::BT::Mesh::Model::OpCode::decode(decoder);

	using namespace Oscl::BT::Mesh::Model::OpCode;

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s:\n"
		"\topcode: (0x%4.4X)\n",
		OSCL_PRETTY_FUNCTION,
		opcode
		);
	#endif

	const uint8_t*	p	= (const uint8_t*)frame;

	unsigned
	remaining	= decoder.remaining();

	unsigned
	payloadOffset	= length - remaining;

	_lightLightnessTransceiver.receive(
		opcode,
		src,
		dst,
		&p[payloadOffset],
		remaining,
		appKeyIndex
		);

	}

bool	Part::lightLightnessServerSigModelIdMatch(uint16_t modelID) const noexcept{

	using namespace Oscl::BT::Mesh::SigModelID;

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s:\n"
		"\tmodelID: 0x%4.4X, this modelID: 0x%4.4X\n",
		OSCL_PRETTY_FUNCTION,
		modelID,
		lightLightnessServer
		);
	#endif

	switch(modelID){
		case lightLightnessServer:
			return true;
		default:
			return false;
		}
	}

bool	Part::lightLightnessServerVendorModelIdMatch(uint16_t vendorID, uint16_t modelID) const noexcept{
	return false;
	}

bool	Part::lightLightnessServerModelSupportsAppKeyBinding() const noexcept{
	return true;
	}

Oscl::BT::Mesh::Publication::Api*	Part::lightLightnessServerGetPublicationApi() noexcept{
	// FIXME: Does Level need to support publication?
	return 0;
	}

uint16_t	Part::lightLightnessServerGetPublicationAppKeyIndex() const noexcept{
	/*	This is only needed if Level supports publication.
		If lightLightnessServerGetPublicationApi() returns zero, then
		this will never be called.
	 */
	return 0;
	}

void	Part::lightLightnessServerStop() noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s:\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif
	}

void	Part::setGenericOnOffState(
			uint8_t	onOff
			) noexcept{

	/*	Delegated to the Light Lightness State.
		6.1.2.2.3 Binding with the Generic OnOff state
	 */

	uint16_t	lightness	= 0;

	if(onOff){

		uint16_t
		defaultState	= _contextApi.getLightLightnessStatePersistApi().getLightLightnessDefaultState();

		if(!defaultState){
			lightness	= _contextApi.getLightLightnessStatePersistApi().getLightLightnessLastState();
			}
		else {
			lightness	= defaultState;
			}
		}

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s: lightness: %u\n",
		OSCL_PRETTY_FUNCTION,
		lightness
		);
	#endif

	_lightnessTransitionAdaptor.setLightLightnessState(
		lightness
		);
	}

void	Part::setGenericOnOffState(
			uint8_t	onOff,
			uint8_t	transitionTime,
			uint8_t	executionDelayIn5msSteps
			) noexcept{

	/*	Delegated to the Light Lightness State.
		6.1.2.2.3 Binding with the Generic OnOff state
	 */

	uint16_t	lightness	= 0;

	if(onOff){

		uint16_t
		defaultState	= _contextApi.getLightLightnessStatePersistApi().getLightLightnessDefaultState();

		if(!defaultState){
			lightness	= _contextApi.getLightLightnessStatePersistApi().getLightLightnessLastState();
			}
		else {
			lightness	= defaultState;
			}
		}

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s: lightness: %u\n",
		OSCL_PRETTY_FUNCTION,
		lightness
		);
	#endif

	_lightnessTransitionAdaptor.setLightLightnessState(
		lightness,
		transitionTime,
		executionDelayIn5msSteps
		);
	}

void	Part::getGenericOnOffState(
			uint8_t&	presentOnOff,
			uint8_t&	targetOnOff,
			uint8_t&	remainingTime
			) noexcept{

	/*	Delegated to the Light Lightness State.
	 */

	uint16_t	presentLightness;
	uint16_t	targetLightness;

	_lightnessTransitionAdaptor.getLightLightnessState(
		presentLightness,
		targetLightness,
		remainingTime
		);

	presentOnOff	= presentLightness?0x01:0x00;
	targetOnOff		= targetLightness?0x01:0x00;
	}

void	Part::onOffPublish(
			uint16_t	appKeyIndex,
			uint16_t	dstAddress,
			uint8_t		ttl,
			const void*	label,
			bool		retransmitting
			) noexcept{

	const unsigned
	labelLen	= label?16:0;

	const bool
	sendSegmented	= (
			_allowSegmentedStatus
		&&	Oscl::BT::Mesh::Address::isUnicastAddress(dstAddress)
		);

	_genericOnOffTransceiver.sendGenericOnOffStatus(
		appKeyIndex,
		dstAddress,
		ttl,
		sendSegmented,
		label,
		labelLen
		);
	}

void	Part::setGenericOnPowerUpState(uint8_t state) noexcept{
	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s: state: 0x%2.2X\n",
		OSCL_PRETTY_FUNCTION,
		state
		);
	#endif

	_contextApi.getOnPowerUpPersistApi().setPowerUpState(state);
	}

uint8_t	Part::getGenericOnPowerUpState() noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif

	return _contextApi.getOnPowerUpPersistApi().getPowerUpState();
	}

static uint16_t	convertLevelToLightnessActual(int16_t level) noexcept{

	int	lightnessActual	= level;

	lightnessActual	+= 32768;

	uint16_t	value;

	if(lightnessActual < 0){
		value	= 0;
		}
	else if(lightnessActual > 65535){
		value	= 65535;
		}
	else {
		value	= lightnessActual;
		}

	return value;
	}

void	Part::setGenericLevelState(
			int16_t	level
			) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif

	_lightnessTransitionAdaptor.setLightLightnessState(
		convertLevelToLightnessActual(level)
		);
	}

void	Part::setGenericLevelState(
			int16_t	level,
			uint8_t	transitionTime,
			uint8_t	executionDelayIn5msSteps
			) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif

	_lightnessTransitionAdaptor.setLightLightnessState(
		convertLevelToLightnessActual(level),
		transitionTime,
		executionDelayIn5msSteps
		);
	}

void	Part::setGenericDeltaState(
			int16_t	deltaLevel
			) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif

	}

void	Part::setGenericDeltaState(
			int16_t	deltaLevel,
			uint8_t	transitionTime,
			uint8_t	executionDelayIn5msSteps
			) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif

	/*	Mesh Model Spec: 3.3.2.2.3
		When a Generic Level Server receives a
		Generic Delta Set message or a
		Generic Delta Set Unacknowledged message,
		it shall set the Generic Level state to
		the Delta Level field of the message added
		to the Initial Generic Level state.

		When the Generic Level state is bound to
		another state that is not contiguous,
		the Generic Level Server shall set the
		Generic Level state to a value that satisfies
		the requirements of the bound state.

		For example, a Generic Level state may be bound
		to a Light Lightness state that is bound to a Light
		Lightness Range state such that only values 0 and
		1000–50000 are supported; therefore,
		if the resulting value of the bound Light Lightness
		range state is in the excluded open range of (0,1000),
		a value of 0 shall be set for negative Delta Level
		values and a value of 1000 shall be set for positive
		Delta Level values.

		When the Generic Level state is not bound to any state,
		the overflow/underflow handling is implementation-specific.
		Some Generic Level Servers may stop at their maximum or
		minimum level, and some may wrap around.

		When the Generic Level state is bound to another state,
		the overflow/underflow handling shall be defined by
		the wrap-around behavior of the bound state.

		If present, the Transition Time field value shall be
		used as the time for transition to the target state.
		If the Transition Time field is not included and
		the Generic Default Transition Time state is supported,
		the Generic Default Transition Time state shall be used.
		Otherwise the transition shall be instantaneous.

		If the Transition Time field is included,
		the Delay field is included and indicates a delay
		5-millisecond steps that the server shall wait
		before executing any state-changing behavior
		for this message.

		If the target state is equal to the current state,
		the transition shall not be started and is considered
		complete.

		It is recommended to set the value of the
		Transition Time field to the expected interval
		after which the next message will be sent.
		For example, if the messages are sent every
		200 milliseconds, the Transition Time should
		be set to 200 milliseconds.

	 */
#if 0
	// FIXME: targetState + deltaLevel

	int16_t	presentLevel;
	int16_t	targetLevel;
	uint8_t	remainingTime;

	_contextApi.getGenericLevelState(
		presentLevel,
		targetLevel,
		remainingTime
		);

	targetLevel	= presentLevel	+= deltaLevel; // FIXME: ? Handle overflow
#endif

	}

void	Part::setGenericMoveState(
			int16_t	moveLevel
			) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif

	}

void	Part::setGenericMoveState(
			int16_t	moveLevel,
			uint8_t	transitionTime,
			uint8_t	executionDelayIn5msSteps
			) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif

	/*	Mesh Model Spec: 3.3.2.2.4
		When a Generic Level Server receives a
		Generic Move Set message or a
		Generic Move Set Unacknowledged message,
		it shall start a process of changing the
		Generic Level state with a transition speed
		that is calculated by dividing the
		Delta Level by the Transition Time
		(i.e., it will be changing by a value
		of the Delta Level in time of the Transition Time)

		When the Generic Level state is not bound to
		another state, the overflow/underflow handling is
		implementation-specific. Some Generic Level Servers
		may stop at their maximum or minimum levels,
		and some may wrap around.

		When the Generic Level state is bound to another
		state, the overflow/underflow handling shall be
		defined by the wrap-around behavior of the bound state.

		When a Generic Level Server receives the message
		with a value of the Delta Level field equal to 0, it
		shall stop changing the Generic Level state.

		If present, the Transition Time field value shall be
		used as the time for calculating the transition speed
		towards the target state.

		If the Transition Time field is not present and the
		Generic Default Transition Time state (see Section 3.1.3)
		is supported, the Generic Default Transition Time
		state shall be used.

		Otherwise the Generic Move Set command shall not
		initiate any Generic Level state change.

		If the resulting Transition Time is equal to 0,
		the Generic Move Set command will not initiate any
		Generic Level state change.

		If the Transition Time field is included,
		the Delay field is included and indicates a delay
		5-millisecond steps that the server shall wait
		before executing any state-changing behavior for
		this message.

		If the target state is equal to the current state,
		the transition shall not be started and is considered
		complete.
	 */

#if 0
	int16_t	presentLevel;
	int16_t	targetLevel;
	uint8_t	remainingTime;

	_contextApi.getGenericLevelState(
		presentLevel,
		targetLevel,
		remainingTime
		);

	targetLevel	+= moveLevel;	// FIXME: ? Handle overflow
#endif

	}

static int16_t	convertLightnessActualToLevel(uint16_t lightness) noexcept{

	int
	level	= lightness;
	level	-= 32768;

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s: lightness: %u, level: %d\n",
		OSCL_PRETTY_FUNCTION,
		lightness,
		level
		);
	#endif

	if(level < -32768){
		level	= -32768;
		}

	return level;
	}

void	Part::getGenericLevelState(
			int16_t&	presentLevel,
			int16_t&	targetLevel,
			uint8_t&	remainingTime
			) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif

	uint16_t	presentLightness;
	uint16_t	targetLightness;

	_lightnessTransitionAdaptor.getLightLightnessState(
		presentLightness,
		targetLightness,
		remainingTime
		);

	presentLevel	= convertLightnessActualToLevel(presentLightness);
	targetLevel		= convertLightnessActualToLevel(targetLightness);
	}

void	Part::setLightLightnessState(
			uint16_t	lightness
			) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif

	if(lightness){
		_contextApi.getLightLightnessStatePersistApi().setLightLightnessLastState(lightness);
		}

	_contextApi.getLightLightnessStatePersistApi().setLightLightnessTargetState(lightness);
	}

void	Part::setLightLightnessState(
			uint16_t	lightness,
			uint8_t		transitionTime,
			uint8_t		executionDelayIn5msSteps
			) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif

	if(lightness){
		_contextApi.getLightLightnessStatePersistApi().setLightLightnessLastState(lightness);
		}

	_contextApi.getLightLightnessStatePersistApi().setLightLightnessTargetState(lightness);
	}

void	Part::setLightLightnessLinearState(
			uint16_t	lightness
			) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif

	}

void	Part::setLightLightnessLinearState(
			uint16_t	lightness,
			uint8_t		transitionTime,
			uint8_t		executionDelayIn5msSteps
			) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif

	}

void	Part::setLightLightnessDefaultState(
			uint16_t	lightness
			) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif

	_contextApi.getLightLightnessStatePersistApi().setLightLightnessDefaultState(lightness);
	}

void	Part::setLightLightnessRangeState(
			uint16_t	rangeMin,
			uint16_t	rangeMax
			) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif

	_contextApi.getLightLightnessStatePersistApi().setLightLightnessRangeState(
		rangeMin,
		rangeMax
		);
	}

void	Part::getLightLightnessState(
			uint16_t&	presentLightness,
			uint16_t&	targetLightness,
			uint8_t&	remainingTime
			) const noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif

	}

void	Part::getLightLightnessLinearState(
			uint16_t&	presentLightness,
			uint16_t&	targetLightness,
			uint8_t&	remainingTime
			) const noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif

	}

void	Part::getLightLightnessLastState(
			uint16_t&	lightness
			) const noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif

	lightness	= _contextApi.getLightLightnessStatePersistApi().getLightLightnessLastState();
	}

void	Part::getLightLightnessDefaultState(
			uint16_t&	lightness
			) const noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif

	lightness	= _contextApi.getLightLightnessStatePersistApi().getLightLightnessDefaultState();
	}

void	Part::getLightLightnessRangeState(
			uint16_t&	rangeMin,
			uint16_t&	rangeMax,
			uint8_t&	statusCode
			) const noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif

	_contextApi.getLightLightnessStatePersistApi().getLightLightnessRangeState(
		rangeMin,
		rangeMax
		);

	statusCode	= 0x00;	// Success
	}

