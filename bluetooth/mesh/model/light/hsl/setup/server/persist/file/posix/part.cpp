/*
   Copyright (C) 2021 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <errno.h>
#include <stdio.h>
#include <strings.h>
#include <string.h>
#include <stdlib.h>
#include <new>
#include <unistd.h>

#include "part.h"
#include "oscl/error/fatal.h"
#include "oscl/error/info.h"
#include "oscl/strings/fixed.h"

using namespace Oscl::BT::Mesh::Model::Light::Hsl::Setup::Server::Persist::File::Posix;

//#define DEBUG_TRACE

Part::Part(
	Oscl::Persist::
	Parent::Posix::Part&	parent,
	const char*				fileName
	) noexcept:
		Base(
			parent,
			fileName
			),
		_targetState(defaultTargetState),
		_powerUpState(defaultPowerUpState),
		_levelTargetState(defaultLevelTargetState),
		_lightLightnessTargetState(defaultLightLightnessTargetState),
		_lightLightnessDefaultState(defaultLightLightnessDefaultState),
		_lightLightnessLastState(defaultLightLightnessLastState),
		_lightLightnessRangeMinState(defaultLightLightnessRangeMinState),
		_lightLightnessRangeMaxState(defaultLightLightnessRangeMaxState)
		{
	read();
	}

Part::~Part() noexcept{
	}

void	Part::setTargetState(uint8_t state) noexcept{
	_targetState	= state;
	write();
	}

uint8_t	Part::getTargetState() const noexcept{
	return _targetState;
	}

void	Part::setPowerUpState(uint8_t state) noexcept{
	_powerUpState	= state;
	write();
	}

uint8_t	Part::getPowerUpState() const noexcept{
	return _powerUpState;
	}

void	Part::setLevelTargetState(int16_t state) noexcept{
	_levelTargetState	= state;
	write();
	}

int16_t	Part::getLevelTargetState() const noexcept{
	return _levelTargetState;
	}

void	Part::setLightLightnessTargetState(uint16_t state) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif

	_lightLightnessTargetState	= state;
	write();
	}

uint16_t	Part::getLightLightnessTargetState() const noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif

	return _lightLightnessTargetState;
	}

void	Part::setLightLightnessDefaultState(uint16_t state) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif

	_lightLightnessDefaultState	= state;
	write();
	}

uint16_t	Part::getLightLightnessDefaultState() const noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif

	return _lightLightnessDefaultState;
	}

void	Part::setLightLightnessLastState(uint16_t state) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif

	_lightLightnessLastState	= state;
	write();
	}

uint16_t	Part::getLightLightnessLastState() const noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif

	return _lightLightnessLastState;
	}

void	Part::setLightLightnessRangeState(
			uint16_t	rangeMin,
			uint16_t	rangeMax
			) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif

	_lightLightnessRangeMinState	= rangeMin;
	_lightLightnessRangeMaxState	= rangeMax;

	write();
	}

void	Part::getLightLightnessRangeState(
			uint16_t&	rangeMin,
			uint16_t&	rangeMax
			) const noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif

	rangeMin	= _lightLightnessRangeMinState;
	rangeMin	= _lightLightnessRangeMaxState;
	}

void	Part::setDefaultValues() noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif

	setTargetState(defaultTargetState);
	setPowerUpState(defaultPowerUpState);
	setLevelTargetState(defaultLevelTargetState);

	setLightLightnessTargetState(defaultLightLightnessTargetState);
	setLightLightnessDefaultState(defaultLightLightnessDefaultState);
	setLightLightnessLastState(defaultLightLightnessLastState);
	setLightLightnessRangeState(
		defaultLightLightnessRangeMinState,
		defaultLightLightnessRangeMaxState
		);
	}

bool	Part::writeValues(FILE* file) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif

	int
	result	= fprintf(
				file,
				"targetState=0x%2.2X\n"
				"powerUpState=0x%2.2X\n"
				"levelTargetState=%d\n"
				"lightLightnessTargetState=%u\n"
				"lightLightnessDefaultState=%u\n"
				"lightLightnessLastState=%u\n"
				"lightLightnessRangeMinState=%u\n"
				"lightLightnessRangeMaxState=%u\n"
				"",
				_targetState,
				_powerUpState,
				_levelTargetState,
				_lightLightnessTargetState,
				_lightLightnessDefaultState,
				_lightLightnessLastState,
				_lightLightnessRangeMinState,
				_lightLightnessRangeMaxState
				);

	if(result < 0){
		return true;
		}

	return false;
	}

bool	Part::readValues(FILE* file) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif

	bool			targetStateIsValid	= false;
	bool			powerUpStateIsValid	= false;
	bool			levelTargetStateIsValid	= false;
	bool			lightLightnessTargetStateIsValid	= false;
	bool			lightLightnessDefaultStateIsValid	= false;
	bool			lightLightnessLastStateIsValid		= false;
	bool			lightLightnessRangeMinStateIsValid	= false;
	bool			lightLightnessRangeMaxStateIsValid	= false;

	char			buffer[1024];
	const char*		line;

	while((line = fgets(buffer,sizeof(buffer),file))){

		if(line[0] == '#'){
			// comment
			continue;
			}

		char*
		assignment	= index(buffer,'=');

		if(!assignment){
			continue;
			}

		assignment[0]	= '\0';

		char*
		value	= &assignment[1];

		char*
		newline = rindex(value,'\n');

		if(newline){
			*newline	= '\0';
			}

		if(!strcmp(line,"targetState")){
			_targetState		= (uint8_t)strtoul(value,0,0);
			targetStateIsValid	= true;
			}
		else if(!strcmp(line,"powerUpState")){
			_powerUpState		= (uint8_t)strtoul(value,0,0);
			powerUpStateIsValid	= true;
			}
		else if(!strcmp(line,"levelTargetState")){
			_levelTargetState		= (int16_t)strtol(value,0,0);
			levelTargetStateIsValid	= true;
			}
		else if(!strcmp(line,"lightLightnessTargetState")){
			_lightLightnessTargetState			= (uint16_t)strtoul(value,0,0);
			lightLightnessTargetStateIsValid	= true;
			}
		else if(!strcmp(line,"lightLightnessDefaultState")){
			_lightLightnessDefaultState			= (uint16_t)strtoul(value,0,0);
			lightLightnessDefaultStateIsValid	= true;
			}
		else if(!strcmp(line,"lightLightnessLastState")){
			_lightLightnessLastState			= (uint16_t)strtoul(value,0,0);
			lightLightnessLastStateIsValid		= true;
			}
		else if(!strcmp(line,"lightLightnessRangeMinState")){
			_lightLightnessRangeMinState		= (uint16_t)strtoul(value,0,0);
			lightLightnessRangeMinStateIsValid	= true;
			}
		else if(!strcmp(line,"lightLightnessRangeMaxState")){
			_lightLightnessRangeMaxState		= (uint16_t)strtoul(value,0,0);
			lightLightnessRangeMaxStateIsValid	= true;
			}
		}

	if(!targetStateIsValid){
		Oscl::Error::Info::log(
			"%s: targetStateIsValid is missing.\n",
			OSCL_PRETTY_FUNCTION
			);
		}

	if(!powerUpStateIsValid){
		Oscl::Error::Info::log(
			"%s: powerUpStateIsValid is missing.\n",
			OSCL_PRETTY_FUNCTION
			);
		}

	if(!levelTargetStateIsValid){
		Oscl::Error::Info::log(
			"%s: levelTargetStateIsValid is missing.\n",
			OSCL_PRETTY_FUNCTION
			);
		}

	if(!lightLightnessTargetStateIsValid){
		Oscl::Error::Info::log(
			"%s: lightLightnessTargetStateIsValid is missing.\n",
			OSCL_PRETTY_FUNCTION
			);
		}

	if(!lightLightnessDefaultStateIsValid){
		Oscl::Error::Info::log(
			"%s: lightLightnessDefaultStateIsValid is missing.\n",
			OSCL_PRETTY_FUNCTION
			);
		}

	if(!lightLightnessLastStateIsValid){
		Oscl::Error::Info::log(
			"%s: lightLightnessLastStateIsValid is missing.\n",
			OSCL_PRETTY_FUNCTION
			);
		}

	if(!lightLightnessRangeMinStateIsValid){
		Oscl::Error::Info::log(
			"%s: lightLightnessRangeMinStateIsValid is missing.\n",
			OSCL_PRETTY_FUNCTION
			);
		}

	if(!lightLightnessRangeMaxStateIsValid){
		Oscl::Error::Info::log(
			"%s: lightLightnessRangeMaxStateIsValid is missing.\n",
			OSCL_PRETTY_FUNCTION
			);
		}

	return false;
	}

