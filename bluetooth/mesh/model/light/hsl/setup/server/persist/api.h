/*
   Copyright (C) 2021 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_bluetooth_mesh_model_light_hsl_setup_server_persist_apih_
#define _oscl_bluetooth_mesh_model_light_hsl_setup_server_persist_apih_

#include <stdint.h>
#include "oscl/bluetooth/mesh/state/generic/onpowerup/persist/api.h"
#include "oscl/bluetooth/mesh/state/generic/onoff/persist/api.h"
#include "oscl/bluetooth/mesh/state/generic/level/persist/api.h"
#include "oscl/bluetooth/mesh/state/light/lightness/persist/api.h"

/** */
namespace Oscl {
/** */
namespace BT {
/** */
namespace Mesh {
/** */
namespace Model {
/** */
namespace Light {
/** */
namespace Hsl {
/** */
namespace Setup {
/** */
namespace Server {
/** */
namespace Persist {

static constexpr uint8_t	defaultTargetState	= 0x00;	// OFF
static constexpr uint8_t	defaultPowerUpState	= 0x02;	// Restore
static constexpr int16_t	defaultLevelTargetState	= -32768;	// Minimum
static constexpr uint16_t	defaultLightLightnessTargetState	= 0;	// Minimum
static constexpr uint16_t	defaultLightLightnessLastState	= 0xFFFF;	// 6.1.2.3
static constexpr uint16_t	defaultLightLightnessDefaultState	= 0x0000;	// 6.1.2.4
static constexpr uint16_t	defaultLightLightnessRangeMinState	= 0x0001;	// 6.1.2.5 (vendor specific)
static constexpr uint16_t	defaultLightLightnessRangeMaxState	= 0xFFFF;	// 6.1.2.5 (vendor specific)

/** This platform abstraction is used by the
	Light HSL Setup Server to access persistence.

	It combines many of the state attributes
	into a single "file", which can significantly
	decrease the storage requirements for some
	implementations which have relatively "large"
	per-file overhead (e.g. LittleFS/ZephyrOS).
 */
class Api :
	public Oscl::BT::Mesh::State::Generic::OnOff::Persist::Api,
	public Oscl::BT::Mesh::State::Generic::OnPowerUp::Persist::Api,
	public Oscl::BT::Mesh::State::Generic::Level::Persist::Api,
	public Oscl::BT::Mesh::State::Light::Lightness::Persist::Api
	{
	public: // Oscl::BT::Mesh::State::Generic::OnOff::Persist::Api
		/** */
		virtual void	setTargetState(uint8_t state) noexcept=0;

		/** */
		virtual uint8_t	getTargetState() const noexcept=0;

	public: // Oscl::BT::Mesh::State::Generic::OnPowerUp::Persist::Api
		/** */
		virtual void	setPowerUpState(uint8_t state) noexcept=0;

		/** */
		virtual uint8_t	getPowerUpState() const noexcept=0;

	public: // Oscl::BT::Mesh::State::Generic::Level::Persist::Api
		/** */
		virtual void	setLevelTargetState(int16_t state) noexcept=0;

		/** */
		virtual int16_t	getLevelTargetState() const noexcept=0;

	public:
		/** */
		virtual void	setLightLightnessTargetState(uint16_t state) noexcept=0;

		/** */
		virtual uint16_t	getLightLightnessTargetState() const noexcept=0;

		/** */
		virtual void	setLightLightnessDefaultState(uint16_t state) noexcept=0;

		/** */
		virtual uint16_t	getLightLightnessDefaultState() const noexcept=0;

		/** */
		virtual void	setLightLightnessLastState(uint16_t state) noexcept=0;

		/** */
		virtual uint16_t	getLightLightnessLastState() const noexcept=0;

		/** */
		virtual void	setLightLightnessRangeState(
							uint16_t	rangeMin,
							uint16_t	rangeMax
							) noexcept=0;

		/** */
		virtual void	getLightLightnessRangeState(
							uint16_t&	rangeMin,
							uint16_t&	rangeMax
							) const noexcept=0;
	};

}
}
}
}
}
}
}
}
}

#endif
