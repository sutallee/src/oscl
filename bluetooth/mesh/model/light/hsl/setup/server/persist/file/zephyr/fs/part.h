/*
   Copyright (C) 2021 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_bluetooth_mesh_model_light_hsl_setup_server_persist_file_zephyr_fs_parth_
#define _oscl_bluetooth_mesh_model_light_hsl_setup_server_persist_file_zephyr_fs_parth_

#include "oscl/bluetooth/mesh/model/light/hsl/setup/server/persist/api.h"
#include "oscl/persist/parent/zephyr/fs/part.h"
#include "oscl/persist/file/zephyr/fs/base.h"

/** */
namespace Oscl {
/** */
namespace BT {
/** */
namespace Mesh {
/** */
namespace Model {
/** */
namespace Light {
/** */
namespace Hsl {
/** */
namespace Setup {
/** */
namespace Server {
/** */
namespace Persist {
/** */
namespace File {
/** */
namespace Zephyr {
/** */
namespace FS {

/** */
class Part :
		private Oscl::Persist::File::Zephyr::FS::Base,
		public Oscl::BT::Mesh::Model::Light::Hsl::Setup::Server::Persist::Api
		{
	private:
		/** */
		uint8_t		_targetState;

	private:
		/** */
		uint8_t		_powerUpState;

	private:
		/** */
		int16_t		_levelTargetState;

	private:
		/** */
		uint16_t	_lightLightnessTargetState;

		/** */
		uint16_t	_lightLightnessDefaultState;

		/** */
		uint16_t	_lightLightnessLastState;

		/** */
		uint16_t	_lightLightnessRangeMinState;

		/** */
		uint16_t	_lightLightnessRangeMaxState;

	public:
		/** */
		Part(
			Oscl::Persist::
			Parent::Zephyr::FS::Part&	parent,
			const char*					fileName
			) noexcept;

		/** */
		~Part() noexcept;

	public: // Oscl::BT::Mesh::State::Generic::OnOff::Persist::Api
		/** */
		void	setTargetState(uint8_t state) noexcept;

		/** */
		uint8_t	getTargetState() const noexcept;

	public: // Oscl::BT::Mesh::State::Generic::OnPowerUp::Persist::Api
		/** */
		void	setPowerUpState(uint8_t state) noexcept;

		/** */
		uint8_t	getPowerUpState() const noexcept;

	public: // Oscl::BT::Mesh::State::Generic::Level::Persist::Api
		/** */
		void	setLevelTargetState(int16_t state) noexcept;

		/** */
		int16_t	getLevelTargetState() const noexcept;

	public: // Oscl::BT::Mesh::State::Light::Lightness::Persist::Api
		/** */
		void	setLightLightnessTargetState(uint16_t state) noexcept;

		/** */
		uint16_t	getLightLightnessTargetState() const noexcept;

		/** */
		void	setLightLightnessDefaultState(uint16_t state) noexcept;

		/** */
		uint16_t	getLightLightnessDefaultState() const noexcept;

		/** */
		void	setLightLightnessLastState(uint16_t state) noexcept;

		/** */
		uint16_t	getLightLightnessLastState() const noexcept;

		/** */
		void	setLightLightnessRangeState(
					uint16_t	rangeMin,
					uint16_t	rangeMax
					) noexcept;

		/** */
		void	getLightLightnessRangeState(
					uint16_t&	rangeMin,
					uint16_t&	rangeMax
					) const noexcept;

	public: // Oscl::Persist::File::Zephyr::FS::Base
		/** */
		void	setDefaultValues() noexcept;

		/** RETURN: true for failure
		 */
		bool	writeValues(Oscl::FS::File::Api& file) noexcept;

		/** RETURN: true for failure
		 */
		bool	readValues(Oscl::FS::File::Api& file) noexcept;
	};

}
}
}
}
}
}
}
}
}
}
}
}

#endif
