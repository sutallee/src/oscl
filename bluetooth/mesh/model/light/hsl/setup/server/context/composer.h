/*
   Copyright (C) 2021 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_bluetooth_mesh_model_light_hsl_setup_server_context_composerh_
#define _oscl_bluetooth_mesh_model_light_hsl_setup_server_context_composerh_

#include "api.h"

/** */
namespace Oscl {
/** */
namespace BT {
/** */
namespace Mesh {
/** */
namespace Model {
/** */
namespace Light {
/** */
namespace Hsl {
/** */
namespace Setup {
/** */
namespace Server {
/** */
namespace Context {

/** */
template <class Context>
class Composer : public Api {
	private:
		/** */
		Context&	_context;

		/** */
		Oscl::BT::Mesh::Publication::Persist::Api&
		(Context::*_getOnOffPublicationPersistApi)();

		/** */
		Oscl::BT::Mesh::Model::Key::Persist::Creator::Api&
		(Context::*_getOnOffKeyPersistApi)();

		/** */
		Oscl::BT::Mesh::State::Generic::OnOff::Persist::Api&
		(Context::*_getOnOffStatePersistApi)();

		/** */
		Oscl::BT::Mesh::State::Generic::Level::Persist::Api&
		(Context::*_getLevelStatePersistApi)();

		/** */
		Oscl::BT::Mesh::State::Light::Lightness::Persist::Api&
		(Context::*_getLightLightnessStatePersistApi)();

		/** */
		Oscl::BT::Mesh::Subscription::Persist::Creator::Api&
		(Context::*_getSubscriptionPersistApi)();

		/** */
		Oscl::BT::Mesh::State::Generic::OnPowerUp::Persist::Api&
		(Context::*_getOnPowerUpPersistApi)();

		/** */
		Oscl::BT::Mesh::Model::Key::Persist::Creator::Api&
		(Context::*_getOnPowerUpKeyPersistApi)();

		/** */
		Oscl::BT::Mesh::Model::Key::Persist::Creator::Api&
		(Context::*_getLevelKeyPersistApi)();

		/** */
		Oscl::BT::Mesh::Model::Key::Persist::Creator::Api&
		(Context::*_getLightnessKeyPersistApi)();

	public:
		/** */
		Composer(
			Context&	context,
			Oscl::BT::Mesh::Publication::Persist::Api&
				(Context::*getOnOffPublicationPersistApi)(),
			Oscl::BT::Mesh::Model::Key::Persist::Creator::Api&
				(Context::*getOnOffKeyPersistApi)(),
			Oscl::BT::Mesh::State::Generic::OnOff::Persist::Api&
				(Context::*getOnOffStatePersistApi)(),
			Oscl::BT::Mesh::State::Generic::Level::Persist::Api&
				(Context::*getLevelStatePersistApi)(),
			Oscl::BT::Mesh::State::Light::Lightness::Persist::Api&
				(Context::*getLightLightnessStatePersistApi)(),
			Oscl::BT::Mesh::Subscription::Persist::Creator::Api&
				(Context::*getSubscriptionPersistApi)(),
			Oscl::BT::Mesh::State::Generic::OnPowerUp::Persist::Api&
				(Context::*getOnPowerUpPersistApi)(),
			Oscl::BT::Mesh::Model::Key::Persist::Creator::Api&
				(Context::*getOnPowerUpKeyPersistApi)(),
			Oscl::BT::Mesh::Model::Key::Persist::Creator::Api&
				(Context::*getLevelKeyPersistApi)(),
			Oscl::BT::Mesh::Model::Key::Persist::Creator::Api&
				(Context::*getLightnessKeyPersistApi)()
			) noexcept;

		/** */
		Oscl::BT::Mesh::Publication::Persist::Api&
			getOnOffPublicationPersistApi() noexcept;

		/** */
		Oscl::BT::Mesh::Model::Key::Persist::Creator::Api&
			getOnOffKeyPersistApi() noexcept;

		/** */
		Oscl::BT::Mesh::State::Generic::OnOff::Persist::Api&
			getOnOffStatePersistApi() noexcept;

		/** */
		Oscl::BT::Mesh::State::Generic::Level::Persist::Api&
			getLevelStatePersistApi() noexcept;

		/** */
		Oscl::BT::Mesh::State::Light::Lightness::Persist::Api&
			getLightLightnessStatePersistApi() noexcept;

		/** */
		Oscl::BT::Mesh::Subscription::Persist::Creator::Api&
			getSubscriptionPersistApi() noexcept;

		/** */
		Oscl::BT::Mesh::State::Generic::OnPowerUp::Persist::Api&
			getOnPowerUpPersistApi() noexcept;

		/** */
		Oscl::BT::Mesh::Model::Key::Persist::Creator::Api&
			getOnPowerUpKeyPersistApi() noexcept;

		/** */
		Oscl::BT::Mesh::Model::Key::Persist::Creator::Api&
			getLevelKeyPersistApi() noexcept;

		/** */
		Oscl::BT::Mesh::Model::Key::Persist::Creator::Api&
			getLightnessKeyPersistApi() noexcept;
	};

template <class Context>
Composer<Context>::Composer(
	Context&	context,
	Oscl::BT::Mesh::Publication::Persist::Api&
		(Context::*getOnOffPublicationPersistApi)(),
	Oscl::BT::Mesh::Model::Key::Persist::Creator::Api&
		(Context::*getOnOffKeyPersistApi)(),
	Oscl::BT::Mesh::State::Generic::OnOff::Persist::Api&
		(Context::*getOnOffStatePersistApi)(),
	Oscl::BT::Mesh::State::Generic::Level::Persist::Api&
		(Context::*getLevelStatePersistApi)(),
	Oscl::BT::Mesh::State::Light::Lightness::Persist::Api&
		(Context::*getLightLightnessStatePersistApi)(),
	Oscl::BT::Mesh::Subscription::Persist::Creator::Api&
		(Context::*getSubscriptionPersistApi)(),
	Oscl::BT::Mesh::State::Generic::OnPowerUp::Persist::Api&
		(Context::*getOnPowerUpPersistApi)(),
	Oscl::BT::Mesh::Model::Key::Persist::Creator::Api&
		(Context::*getOnPowerUpKeyPersistApi)(),
	Oscl::BT::Mesh::Model::Key::Persist::Creator::Api&
		(Context::*getLevelKeyPersistApi)(),
	Oscl::BT::Mesh::Model::Key::Persist::Creator::Api&
		(Context::*getLightnessKeyPersistApi)()
	)noexcept:
		_context(context),
		_getOnOffPublicationPersistApi(getOnOffPublicationPersistApi),
		_getOnOffKeyPersistApi(getOnOffKeyPersistApi),
		_getOnOffStatePersistApi(getOnOffStatePersistApi),
		_getLevelStatePersistApi(getLevelStatePersistApi),
		_getLightLightnessStatePersistApi(getLightLightnessStatePersistApi),
		_getSubscriptionPersistApi(getSubscriptionPersistApi),
		_getOnPowerUpPersistApi(getOnPowerUpPersistApi),
		_getOnPowerUpKeyPersistApi(getOnPowerUpKeyPersistApi),
		_getLevelKeyPersistApi(getLevelKeyPersistApi),
		_getLightnessKeyPersistApi(getLightnessKeyPersistApi)
		{
	}

template <class Context>
Oscl::BT::Mesh::Publication::Persist::Api&
	Composer<Context>::getOnOffPublicationPersistApi() noexcept{
	return (_context.*_getOnOffPublicationPersistApi)();
	}

template <class Context>
Oscl::BT::Mesh::Model::Key::Persist::Creator::Api&
	Composer<Context>::getOnOffKeyPersistApi() noexcept{
	return (_context.*_getOnOffKeyPersistApi)();
	}

template <class Context>
Oscl::BT::Mesh::State::Generic::OnOff::Persist::Api&
	Composer<Context>::getOnOffStatePersistApi() noexcept{
	return (_context.*_getOnOffStatePersistApi)();
	}

template <class Context>
Oscl::BT::Mesh::State::Generic::Level::Persist::Api&
	Composer<Context>::getLevelStatePersistApi() noexcept{
	return (_context.*_getLevelStatePersistApi)();
	}

template <class Context>
Oscl::BT::Mesh::State::Light::Lightness::Persist::Api&
	Composer<Context>::getLightLightnessStatePersistApi() noexcept{
	return (_context.*_getLightLightnessStatePersistApi)();
	}

template <class Context>
Oscl::BT::Mesh::Subscription::Persist::Creator::Api&
	Composer<Context>::getSubscriptionPersistApi() noexcept{
	return (_context.*_getSubscriptionPersistApi)();
	}

template <class Context>
Oscl::BT::Mesh::State::Generic::OnPowerUp::Persist::Api&
	Composer<Context>::getOnPowerUpPersistApi() noexcept{
	return (_context.*_getOnPowerUpPersistApi)();
	}

template <class Context>
Oscl::BT::Mesh::Model::Key::Persist::Creator::Api&
	Composer<Context>::getOnPowerUpKeyPersistApi() noexcept{
	return (_context.*_getOnPowerUpKeyPersistApi)();
	}

template <class Context>
Oscl::BT::Mesh::Model::Key::Persist::Creator::Api&
	Composer<Context>::getLevelKeyPersistApi() noexcept{
	return (_context.*_getLevelKeyPersistApi)();
	}

template <class Context>
Oscl::BT::Mesh::Model::Key::Persist::Creator::Api&
	Composer<Context>::getLightnessKeyPersistApi() noexcept{
	return (_context.*_getLightnessKeyPersistApi)();
	}

}
}
}
}
}
}
}
}
}

#endif
