/*
   Copyright (C) 2021 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_bluetooth_mesh_model_light_hsl_setup_server_context_apih_
#define _oscl_bluetooth_mesh_model_light_hsl_setup_server_context_apih_

#include "oscl/bluetooth/mesh/publication/persist/api.h"
#include "oscl/bluetooth/mesh/model/key/persist/creator/api.h"
#include "oscl/bluetooth/mesh/state/generic/onoff/persist/api.h"
#include "oscl/bluetooth/mesh/state/generic/onpowerup/persist/api.h"
#include "oscl/bluetooth/mesh/subscription/persist/api.h"
#include "oscl/bluetooth/mesh/state/generic/level/persist/api.h"
#include "oscl/bluetooth/mesh/state/light/lightness/persist/api.h"

/** */
namespace Oscl {
/** */
namespace BT {
/** */
namespace Mesh {
/** */
namespace Model {
/** */
namespace Light {
/** */
namespace Hsl {
/** */
namespace Setup {
/** */
namespace Server {
/** */
namespace Context {

/** This abstract interface is used by the Light HSL Setup
	Server to communicate and control a specific application
	context. The implementation is platform and application
	specific.
 */
class Api {
	public:
		/** */
		virtual Oscl::BT::Mesh::Publication::Persist::Api&
			getOnOffPublicationPersistApi() noexcept=0;

		/** */
		virtual Oscl::BT::Mesh::Model::Key::Persist::Creator::Api&
			getOnOffKeyPersistApi() noexcept=0;

		/** */
		virtual Oscl::BT::Mesh::State::Generic::OnOff::Persist::Api&
			getOnOffStatePersistApi() noexcept=0;

		/** */
		virtual Oscl::BT::Mesh::State::Generic::Level::Persist::Api&
			getLevelStatePersistApi() noexcept=0;

		/** */
		virtual Oscl::BT::Mesh::State::Light::Lightness::Persist::Api&
			getLightLightnessStatePersistApi() noexcept=0;

		/** */
		virtual Oscl::BT::Mesh::Subscription::Persist::Creator::Api&
			getSubscriptionPersistApi() noexcept=0;

		/** */
		virtual Oscl::BT::Mesh::State::Generic::OnPowerUp::Persist::Api&
			getOnPowerUpPersistApi() noexcept=0;

		/** */
		virtual Oscl::BT::Mesh::Model::Key::Persist::Creator::Api&
			getOnPowerUpKeyPersistApi() noexcept=0;

		/** */
		virtual Oscl::BT::Mesh::Model::Key::Persist::Creator::Api&
			getLevelKeyPersistApi() noexcept=0;

		/** */
		virtual Oscl::BT::Mesh::Model::Key::Persist::Creator::Api&
			getLightnessKeyPersistApi() noexcept=0;

	};

}
}
}
}
}
}
}
}
}

#endif
