/*
   Copyright (C) 2021 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_bluetooth_mesh_model_light_hsl_setup_server_io_composerh_
#define _oscl_bluetooth_mesh_model_light_hsl_setup_server_io_composerh_

#include "api.h"

/** */
namespace Oscl {
/** */
namespace BT {
/** */
namespace Mesh {
/** */
namespace Model {
/** */
namespace Light {
/** */
namespace Hsl {
/** */
namespace Setup {
/** */
namespace Server {
/** */
namespace IO {

/** */
template <class Context>
class Composer : public Api {
	private:
		/** */
		Context&	_context;

		/**	*/
		Oscl::RGB::Frame::Api&				(Context::*_getArgb32Api)();

		/**	*/
		Oscl::Update::Subject::Api<bool>&	(Context::*_getSubjectApi)();

	public:
		/** */
		Composer(
			Context&	context,
			Oscl::RGB::Frame::Api&				(Context::*getArgb32Api)(),
			Oscl::Update::Subject::Api<bool>&	(Context::*getSubjectApi)()
			) noexcept;

		/**	*/
		Oscl::RGB::Frame::Api&	getArgb32Api() noexcept;

		/**	*/
		Oscl::Update::Subject::Api<bool>&	getSubjectApi() noexcept;

	};

template <class Context>
Composer<Context>::Composer(
	Context&	context,
	Oscl::RGB::Frame::Api&				(Context::*getArgb32Api)(),
	Oscl::Update::Subject::Api<bool>&	(Context::*getSubjectApi)()
	)noexcept:
		_context(context),
		_getArgb32Api(getArgb32Api),
		_getSubjectApi(getSubjectApi)
		{
	}

template <class Context>
Oscl::RGB::Frame::Api&				Composer<Context>::getArgb32Api() noexcept{
	return (_context.*_getArgb32Api)();
	}

template <class Context>
Oscl::Update::Subject::Api<bool>&	Composer<Context>::getSubjectApi() noexcept{
	return (_context.*_getSubjectApi)();
	}

}
}
}
}
}
}
}
}
}

#endif
