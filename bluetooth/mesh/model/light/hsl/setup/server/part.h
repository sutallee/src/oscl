/*
   Copyright (C) 2021 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_bluetooth_mesh_model_light_hsl_setup_server_parth_
#define _oscl_bluetooth_mesh_model_light_hsl_setup_server_parth_

#include "oscl/queue/queue.h"
#include "oscl/bluetooth/mesh/node/api.h"
#include "oscl/bluetooth/mesh/model/rx/item.h"
#include "oscl/pdu/memory/api.h"
#include "oscl/timer/factory/api.h"
#include "oscl/done/operation.h"
#include "oscl/bluetooth/mesh/model/api.h"
#include "oscl/bluetooth/mesh/element/api.h"
#include "oscl/bluetooth/mesh/network/api.h"
#include "oscl/bluetooth/mesh/message/itemcomp.h"
#include "oscl/bluetooth/mesh/model/key/api.h"
#include "oscl/update/observer/composer.h"
#include "oscl/bluetooth/mesh/model/onoff/server/context/api.h"
#include "oscl/bluetooth/mesh/model/app/server/part.h"
#include "oscl/bluetooth/mesh/publication/persist/api.h"
#include "oscl/bluetooth/mesh/message/generic/onoff/server/part.h"
#include "oscl/bluetooth/mesh/state/generic/onoff/persist/api.h"
#include "oscl/bluetooth/mesh/publication/server/part.h"
#include "oscl/bluetooth/mesh/subscription/server/part.h"
#include "oscl/bluetooth/mesh/model/app/server/context/composer.h"
#include "oscl/bluetooth/mesh/model/light/hsl/setup/server/context/api.h"
#include "oscl/switch/spst/control/composer.h"
#include "oscl/bluetooth/mesh/publication/server/context/composer.h"
#include "oscl/bluetooth/mesh/model/light/hsl/setup/server/io/api.h"
#include "oscl/bluetooth/mesh/state/generic/onpowerup/api.h"
#include "oscl/bluetooth/mesh/message/generic/onpowerup/server/part.h"
#include "oscl/bluetooth/mesh/model/rx/itemcomp.h"
#include "oscl/bluetooth/mesh/state/generic/level/api.h"
#include "oscl/bluetooth/mesh/message/generic/level/server/part.h"

#include "oscl/bluetooth/mesh/state/light/lightness/api.h"
#include "oscl/bluetooth/mesh/message/light/lightness/server/part.h"
#include "oscl/bluetooth/mesh/state/light/lightness/transition/part.h"
#include "oscl/u16/control/composer.h"

/** */
namespace Oscl {
/** */
namespace BT {
/** */
namespace Mesh {
/** */
namespace Model {
/** */
namespace Light {
/** */
namespace Hsl {
/** */
namespace Setup {
/** */
namespace Server {

/** This is an implementation of the Bluetooth
	Mesh Light HSL Server Setup model.
 */
class Part :
	private Oscl::BT::Mesh::State::Generic::OnOff::Api,
	private Oscl::BT::Mesh::State::Generic::OnPowerUp::Api,
	private Oscl::BT::Mesh::State::Generic::Level::Api,
	private Oscl::BT::Mesh::State::Light::Lightness::Api
	{
	private:
		/** */
		Oscl::Timer::Factory::Api&	_timerFactoryApi;

		/** */
		Oscl::BT::Mesh::Model::
		Light::Hsl::Setup::Server::
		Context::Api&				_contextApi;

		/** */
		Oscl::BT::Mesh::Model::
		Light::Hsl::Setup::Server::
		IO::Api&					_ioApi;

		/** */
		const bool					_allowSegmentedStatus;

	private:
		/** */
		Oscl::BT::Mesh::
		Model::App::Server::
		Context::Composer<Part>		_genericOnOffAppServerContext;

		/** */
		Oscl::BT::Mesh::
		Model::App::Server::
		Context::Composer<Part>		_genericOnPowerUpAppServerContext;

		/** */
		Oscl::BT::Mesh::
		Model::App::Server::
		Context::Composer<Part>		_genericLevelAppServerContext;

		/** */
		Oscl::BT::Mesh::
		Model::App::Server::
		Context::Composer<Part>		_lightLightnessAppServerContext;

	private:
		/** */
		Oscl::BT::Mesh::
		Subscription::Server::Part	_subscriptionServerPart;

	private:
		/** */
		Oscl::BT::Mesh::Model::
		App::Server::Part			_genericOnOffAppServerPart;

		/** */
		Oscl::BT::Mesh::Model::
		App::Server::Part			_genericOnPowerUpAppServerPart;

		/** */
		Oscl::BT::Mesh::Model::
		App::Server::Part			_genericLevelAppServerPart;

		/** */
		Oscl::BT::Mesh::Model::
		App::Server::Part			_lightLightnessAppServerPart;

	private:
		/** */
		Oscl::BT::Mesh::Publication::
		Server::Context::Composer<Part>	_onOffPublicationComposer;

		/** */
		Oscl::BT::Mesh::
		Publication::Server::Part	_genericOnOffServerPublicationPart;

	private:
		/** */
		Oscl::Timer::Api&			_onOffTidPeriodicTimerApi;

		/** */
		Oscl::Timer::Api&			_levelTidPeriodicTimerApi;

		/** */
		Oscl::Timer::Api&			_lightnessTidPeriodicTimerApi;

	private:
		/** */
		Oscl::Timer::Api&					_lightnessTransitionTimerApi;

		/** */
		Oscl::U16::Control::Composer<Part>	_lightnessControlComposer;

		/** */
		Oscl::BT::Mesh::State::
		Light::Lightness::Transition::Part	_lightnessTransitionAdaptor;

	private:
		/** */
		Oscl::BT::Mesh::Message::
		Generic::OnOff::Server::Part	_genericOnOffTransceiver;

		/** */
		Oscl::BT::Mesh::Message::
		Generic::OnPowerUp::Server::Part	_genericOnPowerUpTransceiver;

		/** */
		Oscl::BT::Mesh::Message::
		Generic::Level::Server::Part	_genericLevelTransceiver;

		/** */
		Oscl::BT::Mesh::Message::
		Light::Lightness::Server::Part	_lightLightnessTransceiver;

	private:
		/** */
		Oscl::BT::Mesh::Model::
		RX::ItemComposer<Part>				_receiver;

	private:
		/** */
		Oscl::Queue<
			Oscl::BT::Mesh::
			Model::RX::Item
			>								_receivers;

	private:
		/** */
		Oscl::Update::Observer::
		Composer<Part,bool>			_onOffObserver;

	public:
		/** */
		Part(
			Oscl::Pdu::Memory::Api&			freeStoreApi,
			Oscl::Timer::Factory::Api&		timerFactoryApi,
			Oscl::BT::Mesh::Node::Api&		nodeApi,
			Oscl::BT::Mesh::Element::
			Symbiont::Kernel::Api&			kernelApi,
			Oscl::BT::Mesh::
			Transport::Lower::TX::Api&		lowerTransportApi,
			Oscl::BT::Mesh::Network::Api&	networkApi,
			Oscl::BT::Mesh::Model::
			Light::Hsl::Setup::Server::
			Context::Api&					contextApi,
			Oscl::BT::Mesh::Model::
			Light::Hsl::Setup::Server::
			IO::Api&						ioApi,
			uint16_t						elementAddress,
			bool							allowSegmentedStatus	= false
			) noexcept;


		/** */
		~Part() noexcept;

		/** */
		Oscl::BT::Mesh::Model::RX::Item&	getModelRxItem() noexcept;

	private:
		/** */
		uint16_t	getInitialLightnessState() const noexcept;

	private: // Oscl::BT::Mesh::Model::RX::ItemComposer _receiver;
		/** */
		void	stop() noexcept;

		/**	*/
		void	processAccessMessage(
					const void*	frame,
					unsigned	length,
					uint16_t	src,
					uint16_t	dst,
					uint16_t	appKeyIndex
					) noexcept;

		/** RETURN: zero if there is no matching SIG model.
		 */
		Oscl::BT::Mesh::Model::Api*	getSigModelApi(uint16_t modelID) noexcept;

		/** RETURN: zero if there is no matching vendor model.
		 */
		Oscl::BT::Mesh::Model::Api*	getVendorModelApi(uint16_t cid, uint16_t modelID) noexcept;

		/** This operation is used to determine if
			the model subscribes to the specified
			destination address. This is only called
			by the containing Element if the destination
			address is not a unicast address. 

			param [in]	dst	The destination address in question.

			RETURN: true if the model is subscribed
			to the destination address (dst).
		 */
		bool	wantsDestination(uint16_t dst) noexcept;

	private: // Oscl::Update::Observer::Composer _onOffObserver
		/** */
		void	onOffObserverUpdate(bool& state) noexcept;

	private: // Oscl::U16::Control::Composer _lightnessControlComposer
		/** */
		void	lightnessUpdate(uint16_t state) noexcept;

	private: // Oscl::BT::Mesh::Transport::TX::Complete::Api
		/** This operation is invoked when the
			PDU has been successfully transmitted
			by the transport layer.

			For unsegmented messages, this is invoked
			immediately since there is no acknowledgment.

			For segmented messages sent to a unicast
			address destination, this operation is only
			invoked after all segments have been acknowledged
			by the destination or its friend.

			For segmented messages sent to a broadcast,
			group, or virtual address, this is invoked
			after all segments have been transmitted for
			the specified number of retrys.
		 */
		void	success() noexcept;

		/** This operation is invoked when the
			PDU has been successfully transmitted
			by the transport layer.

			For unsegmented messages, this is invoked
			immediately since there is no acknowledgment.

			For segmented messages sent to a unicast
			address destination, this operation is only
			invoked after all segments have been acknowledged
			by the destination or its friend.

			For segmented messages sent to a broadcast,
			group, or virtual address, this is invoked
			after all segments have been transmitted for
			the specified number of retrys.
		 */
		void	successOnBehalfOfLPN() noexcept;

		/**	This operation is invoked only for segmented
			messages sent to a unicast address where the
			remote did not acknowledge all segments before
			the timer expired.
		 */
		void	failedTimeout() noexcept;

		/**	This operation is invoked only for segmented
			messages sent to a unicast address where the
			remote indicated that it is unable to receive
			the message at this time (due to resource
			limitations).
		 */
		void	failedBusy() noexcept;

		/**	This operation is invoked when the transport
			layer does not currently have enough resources
			to send the message.
		 */
		void	failedOutOfResources() noexcept;

		/**	This operation is invoked for messages that
			are too big to be handled by the transport
			layer. This indicates a software error.
		 */
		void	failedTooBig() noexcept;

	private: // Oscl::BT::Mesh::Model::App::Server::Context::Composer common/shared
		/** RETURN: zero if the model does NOT support subscription.

			NOTE:	The subscription mechanism is shared with all
					models in this server.
		 */
		Oscl::BT::Mesh::Subscription::Api*	getSubscriptionApi() noexcept;

		/** RETURN: zero if the model does NOT support subscription.

			NOTE:	The subscription mechanism is shared with all
					models in this server.
		 */
		Oscl::BT::Mesh::Subscription::Server::Api*	getSubscriptionServerApi() noexcept;

	private: // Oscl::BT::Mesh::Model::App::Server::Context::Composer _genericOnOffAppServerContext
		/** Process the decrypted Access Message
		 */
		void	genericOnOffServerProcessAccessMessage(
					const void*	frame,
					unsigned	length,
					uint16_t	src,
					uint16_t	dst,
					uint16_t	appKeyIndex
					) noexcept;

		/** RETURN: true for match */
		bool	genericOnOffServerSigModelIdMatch(uint16_t modelID) const noexcept;

		/** RETURN: true for match */
		bool	genericOnOffServerVendorModelIdMatch(
					uint16_t vendorID,
					uint16_t modelID
					) const noexcept;

		/** RETURN: true if the model supports AppKey binding. */
		bool	genericOnOffServerModelSupportsAppKeyBinding() const noexcept;

		/** RETURN: zero if the model does NOT support publication.
		 */
		Oscl::BT::Mesh::Publication::Api*	genericOnOffServerGetPublicationApi() noexcept;

		/** */
		uint16_t	genericOnOffServerGetPublicationAppKeyIndex() const noexcept;

		/** */
		void	genericOnOffServerStop() noexcept;

	private: // Oscl::BT::Mesh::Model::App::Server::Context::Composer _genericOnPowerUpAppServerContext
		/** Process the decrypted Access Message
		 */
		void	genericOnPowerUpServerProcessAccessMessage(
					const void*	frame,
					unsigned	length,
					uint16_t	src,
					uint16_t	dst,
					uint16_t	appKeyIndex
					) noexcept;

		/** RETURN: true for match */
		bool	genericOnPowerUpServerSigModelIdMatch(uint16_t modelID) const noexcept;

		/** RETURN: true for match */
		bool	genericOnPowerUpServerVendorModelIdMatch(
					uint16_t vendorID,
					uint16_t modelID
					) const noexcept;

		/** RETURN: true if the model supports AppKey binding. */
		bool	genericOnPowerUpServerModelSupportsAppKeyBinding() const noexcept;

		/** RETURN: zero if the model does NOT support publication.
		 */
		Oscl::BT::Mesh::Publication::Api*	genericOnPowerUpServerGetPublicationApi() noexcept;

		/** */
		uint16_t	genericOnPowerUpServerGetPublicationAppKeyIndex() const noexcept;

		/** */
		void	genericOnPowerUpServerStop() noexcept;

	private: // Oscl::BT::Mesh::Model::App::Server::Context::Composer _genericLevelAppServerContext
		/** Process the decrypted Access Message
		 */
		void	genericLevelServerProcessAccessMessage(
					const void*	frame,
					unsigned	length,
					uint16_t	src,
					uint16_t	dst,
					uint16_t	appKeyIndex
					) noexcept;

		/** RETURN: true for match */
		bool	genericLevelServerSigModelIdMatch(uint16_t modelID) const noexcept;

		/** RETURN: true for match */
		bool	genericLevelServerVendorModelIdMatch(
					uint16_t vendorID,
					uint16_t modelID
					) const noexcept;

		/** RETURN: true if the model supports AppKey binding. */
		bool	genericLevelServerModelSupportsAppKeyBinding() const noexcept;

		/** RETURN: zero if the model does NOT support publication.
		 */
		Oscl::BT::Mesh::Publication::Api*	genericLevelServerGetPublicationApi() noexcept;

		/** */
		uint16_t	genericLevelServerGetPublicationAppKeyIndex() const noexcept;

		/** */
		void	genericLevelServerStop() noexcept;

	private: // Oscl::BT::Mesh::Model::App::Server::Context::Composer _lightLightnessAppServerContext
		/** Process the decrypted Access Message
		 */
		void	lightLightnessServerProcessAccessMessage(
					const void*	frame,
					unsigned	length,
					uint16_t	src,
					uint16_t	dst,
					uint16_t	appKeyIndex
					) noexcept;

		/** RETURN: true for match */
		bool	lightLightnessServerSigModelIdMatch(uint16_t modelID) const noexcept;

		/** RETURN: true for match */
		bool	lightLightnessServerVendorModelIdMatch(
					uint16_t vendorID,
					uint16_t modelID
					) const noexcept;

		/** RETURN: true if the model supports AppKey binding. */
		bool	lightLightnessServerModelSupportsAppKeyBinding() const noexcept;

		/** RETURN: zero if the model does NOT support publication.
		 */
		Oscl::BT::Mesh::Publication::Api*	lightLightnessServerGetPublicationApi() noexcept;

		/** */
		uint16_t	lightLightnessServerGetPublicationAppKeyIndex() const noexcept;

		/** */
		void	lightLightnessServerStop() noexcept;

	private: // Oscl::BT::Mesh::State::Generic::OnOff::Api
		// /home/mike/root/src/oscl/bluetooth/mesh/state/generic/onoff/api.h
		/*	This operation is invoked by a model
			to immediately change the Generic OnOff State.
			onOff:
				0x00	Off
				0x01	On
		 */
		void	setGenericOnOffState(
					uint8_t	onOff
					) noexcept;

		/*	This operation is invoked by a model
			to change the Generic OnOff State.
			onOff:
				0x00	Off
				0x01	On
			transitionTime: 3.1.3 Generic Default Transition Time
				bits 0:5: Number of steps
					0x00		immediate
					0x00:0x3E	number of steps
					0x3F		unknown
				bits 6:7: Step resolution
					0b00	Step Resolution is 100 milliseconds
					0b01	Step Resolution is 1 second
					0b10	Step Resolution is 10 seconds
					0b11	Step Resolution is 10 minutes
			executionDelayIn5msSteps: execution delay in 5ms steps
		 */
		void	setGenericOnOffState(
					uint8_t	onOff,
					uint8_t	transitionTime,
					uint8_t	executionDelayIn5msSteps
					) noexcept;

		/*	This operation is invoked by a model
			to obtain the current OnOff State.
			presentOnOff:
				0x00	Off
				0x01	On
			targetOnOff:
				0x00	Off
				0x01	On
			remainingTime: 3.1.3 Generic Default Transition Time
				bits 0:5: Number of steps
					0x00		immediate
					0x00:0x3E	number of steps
					0x3F		unknown
				bits 6:7: Step resolution
					0b00	Step Resolution is 100 milliseconds
					0b01	Step Resolution is 1 second
					0b10	Step Resolution is 10 seconds
					0b11	Step Resolution is 10 minutes
		 */
		void	getGenericOnOffState(
					uint8_t&	presentOnOff,
					uint8_t&	targetOnOff,
					uint8_t&	remainingTime
					) noexcept;

	private: // Oscl::BT::Mesh::Publication::Server::Context::Api
		/**
			This operation is invoked to transmit
			a status message.
			The label may be nil.
			If the label is not nil, then
			the length is 16.
		 */
		void	onOffPublish(
					uint16_t	appKeyIndex,
					uint16_t	dstAddress,
					uint8_t		ttl,
					const void*	label,
					bool		retransmitting
					) noexcept;

	private: // Oscl::BT::Mesh::State::Generic::OnPowerUp::Api
		/*	This operation is invoked by a model
			to immediately change the Generic OnPowerUp State.
			state:
				0x00 -	Off. After being powered up, the element is in an off state.
				0x01 -	Default. After being powered up, the element is in an On
						state and uses default state values.
				0x02 -	Restore. If a transition was in progress when powered down,
						the element restores the target state when powered up.
						Otherwise the element restores the state it was in when
						powered down.
		 */
		void	setGenericOnPowerUpState(
					uint8_t	state
					) noexcept;

		/*	This operation is invoked by a model
			to obtain the Generic OnPowerUp State.
			state:
				0x00 -	Off. After being powered up, the element is in an off state.
				0x01 -	Default. After being powered up, the element is in an On
						state and uses default state values.
				0x02 -	Restore. If a transition was in progress when powered down,
						the element restores the target state when powered up.
						Otherwise the element restores the state it was in when
						powered down.
		 */
		uint8_t	getGenericOnPowerUpState() noexcept;

	private: // Oscl::BT::Mesh::State::Generic::Level::Api
		/*	This operation is invoked by a model
			to immediately change the Generic Level State.

			level:
				Range: -32,768 : 32,767
		 */
		void	setGenericLevelState(
					int16_t	level
					) noexcept;

		/*	This operation is invoked by a model
			to change the Generic Level State.

			level:
				Range: -32,768 : 32,767

			transitionTime: 3.1.3 Generic Default Transition Time
				bits 0:5: Number of steps
					0x00		immediate
					0x00:0x3E	number of steps
					0x3F		unknown
				bits 6:7: Step resolution
					0b00	Step Resolution is 100 milliseconds
					0b01	Step Resolution is 1 second
					0b10	Step Resolution is 10 seconds
					0b11	Step Resolution is 10 minutes

			executionDelayIn5msSteps: execution delay in 5ms steps
		 */
		void	setGenericLevelState(
					int16_t	level,
					uint8_t	transitionTime,
					uint8_t	executionDelayIn5msSteps
					) noexcept;

		/*	This operation is invoked by a model
			to immediately change the Generic Level State.

			deltaLevel:
				Range: -32,768 : 32,767
		 */
		void	setGenericDeltaState(
					int16_t	deltaLevel
					) noexcept;

		/*	This operation is invoked by a model
			to change the Generic Level State.

			deltaLevel:
				Range: -32,768 : 32,767

			transitionTime: 3.1.3 Generic Default Transition Time
				bits 0:5: Number of steps
					0x00		immediate
					0x00:0x3E	number of steps
					0x3F		unknown
				bits 6:7: Step resolution
					0b00	Step Resolution is 100 milliseconds
					0b01	Step Resolution is 1 second
					0b10	Step Resolution is 10 seconds
					0b11	Step Resolution is 10 minutes

			executionDelayIn5msSteps: execution delay in 5ms steps
		 */
		void	setGenericDeltaState(
					int16_t	deltaLevel,
					uint8_t	transitionTime,
					uint8_t	executionDelayIn5msSteps
					) noexcept;

		/*	This operation is invoked by a model
			to immediately change the Generic Level State.

			moveLevel:
				Range: -32,768 : 32,767
		 */
		void	setGenericMoveState(
					int16_t	moveLevel
					) noexcept;

		/*	This operation is invoked by a model
			to change the Generic Level State.

			moveLevel:
				Range: -32,768 : 32,767

			transitionTime: 3.1.3 Generic Default Transition Time
				bits 0:5: Number of steps
					0x00		immediate
					0x00:0x3E	number of steps
					0x3F		unknown
				bits 6:7: Step resolution
					0b00	Step Resolution is 100 milliseconds
					0b01	Step Resolution is 1 second
					0b10	Step Resolution is 10 seconds
					0b11	Step Resolution is 10 minutes

			executionDelayIn5msSteps: execution delay in 5ms steps
		 */
		void	setGenericMoveState(
					int16_t	moveLevel,
					uint8_t	transitionTime,
					uint8_t	executionDelayIn5msSteps
					) noexcept;

		/*	This operation is invoked by a model
			to obtain the current Level State.

			presentLevel:
				Range: -32,768 : 32,767

			targetLevel:
				Range: -32,768 : 32,767

			remainingTime: 3.1.3 Generic Default Transition Time
				bits 0:5: Number of steps
					0x00		immediate
					0x00:0x3E	number of steps
					0x3F		unknown
				bits 6:7: Step resolution
					0b00	Step Resolution is 100 milliseconds
					0b01	Step Resolution is 1 second
					0b10	Step Resolution is 10 seconds
					0b11	Step Resolution is 10 minutes
		 */
		void	getGenericLevelState(
					int16_t&	presentLevel,
					int16_t&	targetLevel,
					uint8_t&	remainingTime
					) noexcept;

	private: // Oscl::BT::Mesh::State::Light::Lightness::Api
		/*	This operation is invoked by a model
			to immediately change the Light Lightness State.

			lightness:
				Range: 0 : 0xFFFF
		 */
		void	setLightLightnessState(
					uint16_t	lightness
					) noexcept;

		/*	This operation is invoked by a model
			to change the Light Lightness State.

			lightness:
				Range: 0 : 0xFFFF

			transitionTime: 3.1.3 Generic Default Transition Time
				bits 0:5: Number of steps
					0x00		immediate
					0x00:0x3E	number of steps
					0x3F		unknown
				bits 6:7: Step resolution
					0b00	Step Resolution is 100 milliseconds
					0b01	Step Resolution is 1 second
					0b10	Step Resolution is 10 seconds
					0b11	Step Resolution is 10 minutes

			executionDelayIn5msSteps: execution delay in 5ms steps
		 */
		void	setLightLightnessState(
					uint16_t	lightness,
					uint8_t		transitionTime,
					uint8_t		executionDelayIn5msSteps
					) noexcept;

		/*	This operation is invoked by a model
			to immediately change the Generic Level State.

			lightness:
				Range: 0 : 0xFFFF
		 */
		void	setLightLightnessLinearState(
					uint16_t	lightness
					) noexcept;

		/*	This operation is invoked by a model
			to change the Generic Level State.

			lightness:
				Range: 0 : 0xFFFF

			transitionTime: 3.1.3 Generic Default Transition Time
				bits 0:5: Number of steps
					0x00		immediate
					0x00:0x3E	number of steps
					0x3F		unknown
				bits 6:7: Step resolution
					0b00	Step Resolution is 100 milliseconds
					0b01	Step Resolution is 1 second
					0b10	Step Resolution is 10 seconds
					0b11	Step Resolution is 10 minutes

			executionDelayIn5msSteps: execution delay in 5ms steps
		 */
		void	setLightLightnessLinearState(
					uint16_t	lightness,
					uint8_t		transitionTime,
					uint8_t		executionDelayIn5msSteps
					) noexcept;

		/*	This operation is invoked by a model
			to immediately change the Generic Level State.

			lightness:
				Range: 0 : 0xFFFF
		 */
		void	setLightLightnessDefaultState(
					uint16_t	lightness
					) noexcept;

		/*	This operation is invoked by a model
			to immediately change the Generic Level State.

			rangeMin:
				Range: 0 : 0xFFFF

			rangeMax:
				Range: 0 : 0xFFFF
		 */
		void	setLightLightnessRangeState(
					uint16_t	rangeMin,
					uint16_t	rangeMax
					) noexcept;

		/*	This operation is invoked by a model
			to obtain the current Level State.

			presentLightness:
				Range: 0 : 0xFFFF

			targetLightness:
				Range: 0 : 0xFFFF

			remainingTime: 3.1.3 Generic Default Transition Time
				bits 0:5: Number of steps
					0x00		immediate
					0x00:0x3E	number of steps
					0x3F		unknown
				bits 6:7: Step resolution
					0b00	Step Resolution is 100 milliseconds
					0b01	Step Resolution is 1 second
					0b10	Step Resolution is 10 seconds
					0b11	Step Resolution is 10 minutes
		 */
		void	getLightLightnessState(
					uint16_t&	presentLightness,
					uint16_t&	targetLightness,
					uint8_t&	remainingTime
					) const noexcept;

		/*	This operation is invoked by a model
			to obtain the current Level State.

			presentLightness:
				Range: 0 : 0xFFFF

			targetLightness:
				Range: 0 : 0xFFFF

			remainingTime: 3.1.3 Generic Default Transition Time
				bits 0:5: Number of steps
					0x00		immediate
					0x00:0x3E	number of steps
					0x3F		unknown
				bits 6:7: Step resolution
					0b00	Step Resolution is 100 milliseconds
					0b01	Step Resolution is 1 second
					0b10	Step Resolution is 10 seconds
					0b11	Step Resolution is 10 minutes
		 */
		void	getLightLightnessLinearState(
					uint16_t&	presentLightness,
					uint16_t&	targetLightness,
					uint8_t&	remainingTime
					) const noexcept;

		/*	This operation is invoked by a model
			to obtain the current Level State.

			lightness:
				Range: 0 : 0xFFFF
		 */
		void	getLightLightnessLastState(
					uint16_t&	lightness
					) const noexcept;

		/*	This operation is invoked by a model
			to obtain the current Level State.

			lightness:
				Range: 0 : 0xFFFF
		 */
		void	getLightLightnessDefaultState(
					uint16_t&	lightness
					) const noexcept;

		/*	This operation is invoked by a model
			to obtain the current Level State.

			rangeMin:
				Range: 0 : 0xFFFF

			rangeMax:
				Range: 0 : 0xFFFF

			statusCode:
				0x00:	Success
				0x01:	Cannot Set Range Min
				0x02:	Cannot Set Range Max

		 */
		void	getLightLightnessRangeState(
					uint16_t&	rangeMin,
					uint16_t&	rangeMax,
					uint8_t&	statusCode
					) const noexcept;
	};

}
}
}
}
}
}
}
}

#endif
