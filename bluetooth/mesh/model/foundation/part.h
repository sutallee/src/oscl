/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_bluetooth_mesh_model_foundation_parth_
#define _oscl_bluetooth_mesh_model_foundation_parth_

#include "oscl/queue/queue.h"
#include "oscl/bluetooth/mesh/node/api.h"
#include "oscl/bluetooth/mesh/model/rx/item.h"
#include "oscl/bluetooth/mesh/message/config/composition/data/status/part.h"
#include "oscl/bluetooth/mesh/transport/lower/tx/api.h"
#include "oscl/bluetooth/mesh/transport/upper/tx/device/part.h"
#include "oscl/memory/block.h"
#include "oscl/bluetooth/mesh/transport/upper/rx/composer.h"
#include "oscl/pdu/memory/api.h"
#include "oscl/timer/factory/api.h"
#include "oscl/bluetooth/mesh/model/api.h"
#include "oscl/bluetooth/mesh/element/api.h"
#include "oscl/bluetooth/mesh/network/api.h"
#include "oscl/bluetooth/mesh/message/itemcomp.h"
#include "oscl/bluetooth/mesh/key/comp.h"
#include "oscl/bluetooth/mesh/subscription/api.h"

/** */
namespace Oscl {
/** */
namespace BT {
/** */
namespace Mesh {
/** */
namespace Model {
/** */
namespace Foundation {

/** This is an implementation of the Bluetooth
	Mesh Foundation model.
 */
class Part :
	public Oscl::BT::Mesh::Model::RX::Item,
	private Oscl::BT::Mesh::Transport::TX::Complete::Api,
	private Oscl::BT::Mesh::Model::Api
	{
	private:
		/** */
		Oscl::Pdu::Memory::Api&		_freeStoreApi;

		/** */
		Oscl::Timer::Factory::Api&	_timerFactoryApi;

		/** */
		Oscl::BT::Mesh::Node::Api&	_nodeApi;

		/** */
		Oscl::BT::Mesh::Element::
		Symbiont::Kernel::Api&		_kernelApi;

		/** */
		Oscl::BT::Mesh::Network::Api&	_networkApi;

		/** */
		Oscl::BT::Mesh::Element::Api&	_elementApi;

		/** */
		Oscl::BT::Mesh::Message::
		Config::Composition::
		Data::State::Api&			_compositionDataStateApi;

		/** */
		const uint16_t				_elementAddress;

	private:
		/** */
		Oscl::BT::Mesh::Message::
		ItemComposer<Part>			_rxConfigBeaconGet;

		/** */
		Oscl::BT::Mesh::Message::
		ItemComposer<Part>			_rxConfigBeaconSet;

		/** */
		Oscl::BT::Mesh::Message::
		ItemComposer<Part>			_rxConfigCompositionDataGet;

		/** */
		Oscl::BT::Mesh::Message::
		ItemComposer<Part>			_rxConfigDefaultTtlGet;

		/** */
		Oscl::BT::Mesh::Message::
		ItemComposer<Part>			_rxConfigDefaultTtlSet;

		/** */
		Oscl::BT::Mesh::Message::
		ItemComposer<Part>			_rxConfigGattProxyGet;

		/** */
		Oscl::BT::Mesh::Message::
		ItemComposer<Part>			_rxConfigGattProxySet;

		/** */
		Oscl::BT::Mesh::Message::
		ItemComposer<Part>			_rxConfigRelayGet;

		/** */
		Oscl::BT::Mesh::Message::
		ItemComposer<Part>			_rxConfigRelaySet;

		/** */
		Oscl::BT::Mesh::Message::
		ItemComposer<Part>			_rxConfigModelPublicationGet;

		/** */
		Oscl::BT::Mesh::Message::
		ItemComposer<Part>			_rxConfigModelPublicationSet;

		/** */
		Oscl::BT::Mesh::Message::
		ItemComposer<Part>			_rxConfigModelPublicationVirtualAddressSet;

		/** */
		Oscl::BT::Mesh::Message::
		ItemComposer<Part>			_rxConfigModelSubscriptionAdd;

		/** */
		Oscl::BT::Mesh::Message::
		ItemComposer<Part>			_rxConfigModelSubscriptionDelete;

		/** */
		Oscl::BT::Mesh::Message::
		ItemComposer<Part>			_rxConfigModelSubscriptionOverwrite;

		/** */
		Oscl::BT::Mesh::Message::
		ItemComposer<Part>			_rxConfigModelSubscriptionDeleteAll;

		/** */
		Oscl::BT::Mesh::Message::
		ItemComposer<Part>			_rxConfigModelSubscriptionVirtualAddressAdd;

		/** */
		Oscl::BT::Mesh::Message::
		ItemComposer<Part>			_rxConfigModelSubscriptionVirtualAddressDelete;

		/** */
		Oscl::BT::Mesh::Message::
		ItemComposer<Part>			_rxConfigModelSubscriptionVirtualAddressOverwrite;

		/** */
		Oscl::BT::Mesh::Message::
		ItemComposer<Part>			_rxConfigSigModelSubscriptionGet;

		/** */
		Oscl::BT::Mesh::Message::
		ItemComposer<Part>			_rxConfigVendorModelSubscriptionGet;

		/** */
		Oscl::BT::Mesh::Message::
		ItemComposer<Part>			_rxConfigNetKeyAdd;

		/** */
		Oscl::BT::Mesh::Message::
		ItemComposer<Part>			_rxConfigNetKeyUpdate;

		/** */
		Oscl::BT::Mesh::Message::
		ItemComposer<Part>			_rxConfigNetKeyDelete;

		/** */
		Oscl::BT::Mesh::Message::
		ItemComposer<Part>			_rxConfigNetKeyGet;

		/** */
		Oscl::BT::Mesh::Message::
		ItemComposer<Part>			_rxConfigAppKeyAdd;

		/** */
		Oscl::BT::Mesh::Message::
		ItemComposer<Part>			_rxConfigAppKeyUpdate;

		/** */
		Oscl::BT::Mesh::Message::
		ItemComposer<Part>			_rxConfigAppKeyDelete;

		/** */
		Oscl::BT::Mesh::Message::
		ItemComposer<Part>			_rxConfigAppKeyGet;

		/** */
		Oscl::BT::Mesh::Message::
		ItemComposer<Part>			_rxConfigNodeIdentityGet;

		/** */
		Oscl::BT::Mesh::Message::
		ItemComposer<Part>			_rxConfigNodeIdentitySet;

		/** */
		Oscl::BT::Mesh::Message::
		ItemComposer<Part>			_rxConfigModelAppBind;

		/** */
		Oscl::BT::Mesh::Message::
		ItemComposer<Part>			_rxConfigModelAppUnbind;

		/** */
		Oscl::BT::Mesh::Message::
		ItemComposer<Part>			_rxConfigSigModelAppGet;

		/** */
		Oscl::BT::Mesh::Message::
		ItemComposer<Part>			_rxConfigNodeReset;

		/** */
		Oscl::BT::Mesh::Message::
		ItemComposer<Part>			_rxConfigFriendGet;

		/** */
		Oscl::BT::Mesh::Message::
		ItemComposer<Part>			_rxConfigFriendSet;

		/** */
		Oscl::BT::Mesh::Message::
		ItemComposer<Part>			_rxConfigKeyRefreshPhaseGet;

		/** */
		Oscl::BT::Mesh::Message::
		ItemComposer<Part>			_rxConfigKeyRefreshPhaseSet;

		/** */
		Oscl::BT::Mesh::Message::
		ItemComposer<Part>			_rxConfigHeartbeatPublicationGet;

		/** */
		Oscl::BT::Mesh::Message::
		ItemComposer<Part>			_rxConfigHeartbeatPublicationSet;

		/** */
		Oscl::BT::Mesh::Message::
		ItemComposer<Part>			_rxConfigHeartbeatSubscriptionGet;

		/** */
		Oscl::BT::Mesh::Message::
		ItemComposer<Part>			_rxConfigHeartbeatSubscriptionSet;

		/** */
		Oscl::BT::Mesh::Message::
		ItemComposer<Part>			_rxConfigLowPowerNodePollTimeoutGet;

		/** */
		Oscl::BT::Mesh::Message::
		ItemComposer<Part>			_rxConfigNetworkTransmitGet;

		/** */
		Oscl::BT::Mesh::Message::
		ItemComposer<Part>			_rxConfigNetworkTransmitSet;

	private:
		/** FIXME: Inappropriate for server! */
		Oscl::BT::Mesh::Message::
		Config::Composition::
		Data::Status::Part			_configCompositionDataStatus;

	private:
		/** */
		Oscl::BT::Mesh::Transport::
		Upper::TX::Device::Part		_upperTransportTx;

	private:
		/** */
		Oscl::BT::Mesh::Key::
		Result::Composer<Part>	_configAppKeyAddResult;

		/** */
		Oscl::BT::Mesh::Key::
		Result::Composer<Part>	_configAppKeyUpdateResult;

		/** */
		Oscl::BT::Mesh::Key::
		Result::Composer<Part>	_configAppKeyDeleteResult;

		/** */
		Oscl::BT::Mesh::Key::
		Result::Composer<Part>	_configAppKeyGetResult;

		/** */
		Oscl::BT::Mesh::Key::
		Result::Composer<Part>	_configModelAppBindResult;

	private:
		/** */
		uint8_t						_buffer[384+4];

		/** */
		unsigned					_offset;

		/** */
		unsigned					_nAppKeys;

		/** */
		const Oscl::BT::Mesh::Key::
		Result::Var::State*			_resultState;

	private:
		/** */
		uint16_t				_elementAddr;

		/** */
		uint16_t				_appKeyIndex;

		/** */
		uint16_t				_companyIdentifier;

		/** */
		uint16_t				_modelIdentifier;

		/** */
		bool					_sigModelID;

	private:
		/** */
		uint16_t	_srcAddress;

		/** */
		uint8_t		_keyIndicie0;

		/** */
		uint8_t		_keyIndicie1;

		/** */
		uint8_t		_keyIndicie2;

	private:
		/** */
		Oscl::Queue<
			Oscl::BT::Mesh::
			Message::Item
			>					_messageHandlers;

	private:
			/** */
			bool				_resetting;

	public:
		/** */
		Part(
			Oscl::Pdu::Memory::Api&					freeStoreApi,
			Oscl::Timer::Factory::Api&				timerFactoryApi,
			Oscl::BT::Mesh::Node::Api&				nodeApi,
			Oscl::BT::Mesh::Element::
			Symbiont::Kernel::Api&					kernelApi,
			Oscl::BT::Mesh::
			Transport::Lower::TX::Api&				lowerTransportApi,
			Oscl::BT::Mesh::Network::Api&			networkApi,
			Oscl::BT::Mesh::Element::Api&			elementApi,
			Oscl::BT::Mesh::Message::
			Config::Composition::Data::State::Api&	compositionDataStateApi,
			uint16_t								elementAddress
			) noexcept;

		/** */
		~Part() noexcept;

	private: // Oscl::BT::Mesh::Model::RX::Item
		/** */
		void	stop() noexcept;

		/** This operation is used to determine if
			the model subscribes to the specified
			destination address. This is only called
			by the containing Element if the destination
			address is not a unicast address. 

			param [in]	dst	The destination address in question.

			RETURN: true if the model is subscribed
			to the destination address (dst).
		 */
		bool	wantsDestination(uint16_t dst) noexcept;

		/** RETURN: zero if there is no matching SIG model.
		 */
		Oscl::BT::Mesh::Model::Api*	getSigModelApi(uint16_t modelID) noexcept;

		/** RETURN: zero if there is no matching vendor model.
		 */
		Oscl::BT::Mesh::Model::Api*	getVendorModelApi(uint16_t cid, uint16_t modelID) noexcept;

		/** Process the decrypted Access Message
			The appKeyIndex is greater than 0x0FFF
			if this message is was decrypted using
			the DevKey.
		 */
		void	processAccessMessage(
					const void*	frame,
					unsigned	length,
					uint16_t	src,
					uint16_t	dst,
					uint16_t	appKeyIndex
					) noexcept;

	private: // Oscl::BT::Mesh::Message::ItemComposer _rxConfigBeaconGet
		/** */
		bool	rxConfigBeaconGet(
					uint32_t	opcode,
					uint16_t	srcAddress,
					uint16_t	dstAddress,
					const void*	frame,
					unsigned	length,
					uint16_t	appKeyIndex
					) noexcept;

	private: // Oscl::BT::Mesh::Message::ItemComposer _rxConfigBeaconSet
		/** */
		bool	rxConfigBeaconSet(
					uint32_t	opcode,
					uint16_t	srcAddress,
					uint16_t	dstAddress,
					const void*	frame,
					unsigned	length,
					uint16_t	appKeyIndex
					) noexcept;

	private: // Oscl::BT::Mesh::Message::ItemComposer _rxConfigCompositionDataGet
		/** */
		bool	rxConfigCompositionDataGet(
					uint32_t	opcode,
					uint16_t	srcAddress,
					uint16_t	dstAddress,
					const void*	frame,
					unsigned	length,
					uint16_t	appKeyIndex
					) noexcept;

	private: // Oscl::BT::Mesh::Message::ItemComposer _rxConfigDefaultTtlGet
		/** */
		bool	rxConfigDefaultTtlGet(
					uint32_t	opcode,
					uint16_t	srcAddress,
					uint16_t	dstAddress,
					const void*	frame,
					unsigned	length,
					uint16_t	appKeyIndex
					) noexcept;

	private: // Oscl::BT::Mesh::Message::ItemComposer _rxConfigDefaultTtlSet
		/** */
		bool	rxConfigDefaultTtlSet(
					uint32_t	opcode,
					uint16_t	srcAddress,
					uint16_t	dstAddress,
					const void*	frame,
					unsigned	length,
					uint16_t	appKeyIndex
					) noexcept;

	private: // Oscl::BT::Mesh::Message::ItemComposer _rxConfigGattProxyGet
		/** */
		bool	rxConfigGattProxyGet(
					uint32_t	opcode,
					uint16_t	srcAddress,
					uint16_t	dstAddress,
					const void*	frame,
					unsigned	length,
					uint16_t	appKeyIndex
					) noexcept;

	private: // Oscl::BT::Mesh::Message::ItemComposer _rxConfigGattProxySet
		/** */
		bool	rxConfigGattProxySet(
					uint32_t	opcode,
					uint16_t	srcAddress,
					uint16_t	dstAddress,
					const void*	frame,
					unsigned	length,
					uint16_t	appKeyIndex
					) noexcept;

	private: // Oscl::BT::Mesh::Message::ItemComposer _rxConfigRelayGet
		/** */
		bool	rxConfigRelayGet(
					uint32_t	opcode,
					uint16_t	srcAddress,
					uint16_t	dstAddress,
					const void*	frame,
					unsigned	length,
					uint16_t	appKeyIndex
					) noexcept;

	private: // Oscl::BT::Mesh::Message::ItemComposer _rxConfigRelaySet
		/** */
		bool	rxConfigRelaySet(
					uint32_t	opcode,
					uint16_t	srcAddress,
					uint16_t	dstAddress,
					const void*	frame,
					unsigned	length,
					uint16_t	appKeyIndex
					) noexcept;

	private: // Oscl::BT::Mesh::Message::ItemComposer _rxConfigModelPublicationGet
		/** */
		bool	rxConfigModelPublicationGet(
					uint32_t	opcode,
					uint16_t	srcAddress,
					uint16_t	dstAddress,
					const void*	frame,
					unsigned	length,
					uint16_t	appKeyIndex
					) noexcept;

	private: // Oscl::BT::Mesh::Message::ItemComposer _rxConfigModelPublicationSet
		/** */
		bool	rxConfigModelPublicationSet(
					uint32_t	opcode,
					uint16_t	srcAddress,
					uint16_t	dstAddress,
					const void*	frame,
					unsigned	length,
					uint16_t	appKeyIndex
					) noexcept;

	private: // Oscl::BT::Mesh::Message::ItemComposer _rxConfigModelPublicationVirtualAddressSet
		/** */
		bool	rxConfigModelPublicationVirtualAddressSet(
					uint32_t	opcode,
					uint16_t	srcAddress,
					uint16_t	dstAddress,
					const void*	frame,
					unsigned	length,
					uint16_t	appKeyIndex
					) noexcept;

	private: // Oscl::BT::Mesh::Message::ItemComposer _rxConfigModelSubscriptionAdd
		/** */
		bool	rxConfigModelSubscriptionAdd(
					uint32_t	opcode,
					uint16_t	srcAddress,
					uint16_t	dstAddress,
					const void*	frame,
					unsigned	length,
					uint16_t	appKeyIndex
					) noexcept;

	private: // Oscl::BT::Mesh::Message::ItemComposer _rxConfigModelSubscriptionDelete
		/** */
		bool	rxConfigModelSubscriptionDelete(
					uint32_t	opcode,
					uint16_t	srcAddress,
					uint16_t	dstAddress,
					const void*	frame,
					unsigned	length,
					uint16_t	appKeyIndex
					) noexcept;

	private: // Oscl::BT::Mesh::Message::ItemComposer _rxConfigModelSubscriptionOverwrite
		/** */
		bool	rxConfigModelSubscriptionOverwrite(
					uint32_t	opcode,
					uint16_t	srcAddress,
					uint16_t	dstAddress,
					const void*	frame,
					unsigned	length,
					uint16_t	appKeyIndex
					) noexcept;

	private: // Oscl::BT::Mesh::Message::ItemComposer _rxConfigModelSubscriptionDeleteAll
		/** */
		bool	rxConfigModelSubscriptionDeleteAll(
					uint32_t	opcode,
					uint16_t	srcAddress,
					uint16_t	dstAddress,
					const void*	frame,
					unsigned	length,
					uint16_t	appKeyIndex
					) noexcept;

	private: // Oscl::BT::Mesh::Message::ItemComposer _rxConfigModelSubscriptionVirtualAddressAdd
		/** */
		bool	rxConfigModelSubscriptionVirtualAddressAdd(
					uint32_t	opcode,
					uint16_t	srcAddress,
					uint16_t	dstAddress,
					const void*	frame,
					unsigned	length,
					uint16_t	appKeyIndex
					) noexcept;

	private: // Oscl::BT::Mesh::Message::ItemComposer _rxConfigModelSubscriptionVirtualAddressDelete
		/** */
		bool	rxConfigModelSubscriptionVirtualAddressDelete(
					uint32_t	opcode,
					uint16_t	srcAddress,
					uint16_t	dstAddress,
					const void*	frame,
					unsigned	length,
					uint16_t	appKeyIndex
					) noexcept;

	private: // Oscl::BT::Mesh::Message::ItemComposer _rxConfigModelSubscriptionVirtualAddressOverwrite
		/** */
		bool	rxConfigModelSubscriptionVirtualAddressOverwrite(
					uint32_t	opcode,
					uint16_t	srcAddress,
					uint16_t	dstAddress,
					const void*	frame,
					unsigned	length,
					uint16_t	appKeyIndex
					) noexcept;

	private: // Oscl::BT::Mesh::Message::ItemComposer _rxConfigSigModelSubscriptionGet
		/** */
		bool	rxConfigSigModelSubscriptionGet(
					uint32_t	opcode,
					uint16_t	srcAddress,
					uint16_t	dstAddress,
					const void*	frame,
					unsigned	length,
					uint16_t	appKeyIndex
					) noexcept;

	private: // Oscl::BT::Mesh::Message::ItemComposer _rxConfigVendorModelSubscriptionGet
		/** */
		bool	rxConfigVendorModelSubscriptionGet(
					uint32_t	opcode,
					uint16_t	srcAddress,
					uint16_t	dstAddress,
					const void*	frame,
					unsigned	length,
					uint16_t	appKeyIndex
					) noexcept;

	private: // Oscl::BT::Mesh::Message::ItemComposer _rxConfigAppKeyAdd
		/** */
		bool	rxConfigAppKeyAdd(
					uint32_t	opcode,
					uint16_t	srcAddress,
					uint16_t	dstAddress,
					const void*	frame,
					unsigned	length,
					uint16_t	appKeyIndex
					) noexcept;

	private: // Oscl::BT::Mesh::Message::ItemComposer _rxConfigNetKeyAdd
		/** */
		bool	rxConfigNetKeyAdd(
					uint32_t	opcode,
					uint16_t	srcAddress,
					uint16_t	dstAddress,
					const void*	frame,
					unsigned	length,
					uint16_t	appKeyIndex
					) noexcept;

	private: // Oscl::BT::Mesh::Message::ItemComposer _rxConfigNetKeyUpdate
		/** */
		bool	rxConfigNetKeyUpdate(
					uint32_t	opcode,
					uint16_t	srcAddress,
					uint16_t	dstAddress,
					const void*	frame,
					unsigned	length,
					uint16_t	appKeyIndex
					) noexcept;

	private: // Oscl::BT::Mesh::Message::ItemComposer _rxConfigNetKeyDelete
		/** */
		bool	rxConfigNetKeyDelete(
					uint32_t	opcode,
					uint16_t	srcAddress,
					uint16_t	dstAddress,
					const void*	frame,
					unsigned	length,
					uint16_t	appKeyIndex
					) noexcept;

	private: // Oscl::BT::Mesh::Message::ItemComposer _rxConfigNetKeyGet
		/** */
		bool	rxConfigNetKeyGet(
					uint32_t	opcode,
					uint16_t	srcAddress,
					uint16_t	dstAddress,
					const void*	frame,
					unsigned	length,
					uint16_t	appKeyIndex
					) noexcept;

	private: // Oscl::BT::Mesh::Message::ItemComposer _rxConfigAppKeyUpdate
		/** */
		bool	rxConfigAppKeyUpdate(
					uint32_t	opcode,
					uint16_t	srcAddress,
					uint16_t	dstAddress,
					const void*	frame,
					unsigned	length,
					uint16_t	appKeyIndex
					) noexcept;

	private: // Oscl::BT::Mesh::Message::ItemComposer _rxConfigAppKeyDelete
		/** */
		bool	rxConfigAppKeyDelete(
					uint32_t	opcode,
					uint16_t	srcAddress,
					uint16_t	dstAddress,
					const void*	frame,
					unsigned	length,
					uint16_t	appKeyIndex
					) noexcept;

	private: // Oscl::BT::Mesh::Message::ItemComposer _rxConfigAppKeyGet
		/** */
		bool	rxConfigAppKeyGet(
					uint32_t	opcode,
					uint16_t	srcAddress,
					uint16_t	dstAddress,
					const void*	frame,
					unsigned	length,
					uint16_t	appKeyIndex
					) noexcept;

	private: // Oscl::BT::Mesh::Message::ItemComposer _rxConfigNodeIdentityGet
		/** */
		bool	rxConfigNodeIdentityGet(
					uint32_t	opcode,
					uint16_t	srcAddress,
					uint16_t	dstAddress,
					const void*	frame,
					unsigned	length,
					uint16_t	appKeyIndex
					) noexcept;

	private: // Oscl::BT::Mesh::Message::ItemComposer _rxConfigNodeIdentitySet
		/** */
		bool	rxConfigNodeIdentitySet(
					uint32_t	opcode,
					uint16_t	srcAddress,
					uint16_t	dstAddress,
					const void*	frame,
					unsigned	length,
					uint16_t	appKeyIndex
					) noexcept;

	private: // Oscl::BT::Mesh::Message::ItemComposer _rxConfigModelAppBind
		/** */
		bool	rxConfigModelAppBind(
					uint32_t	opcode,
					uint16_t	srcAddress,
					uint16_t	dstAddress,
					const void*	frame,
					unsigned	length,
					uint16_t	appKeyIndex
					) noexcept;

	private: // Oscl::BT::Mesh::Message::ItemComposer _rxConfigModelAppUnbind
		/** */
		bool	rxConfigModelAppUnbind(
					uint32_t	opcode,
					uint16_t	srcAddress,
					uint16_t	dstAddress,
					const void*	frame,
					unsigned	length,
					uint16_t	appKeyIndex
					) noexcept;

	private: // Oscl::BT::Mesh::Message::ItemComposer _rxConfigSigModelAppGet
		/** */
		bool	rxConfigSigModelAppGet(
					uint32_t	opcode,
					uint16_t	srcAddress,
					uint16_t	dstAddress,
					const void*	frame,
					unsigned	length,
					uint16_t	appKeyIndex
					) noexcept;

	private: // Oscl::BT::Mesh::Message::ItemComposer _rxConfigNodeReset
		/** */
		bool	rxConfigNodeReset(
					uint32_t	opcode,
					uint16_t	srcAddress,
					uint16_t	dstAddress,
					const void*	frame,
					unsigned	length,
					uint16_t	appKeyIndex
					) noexcept;

	private: // Oscl::BT::Mesh::Message::ItemComposer _rxConfigFriendGet
		/** */
		bool	rxConfigFriendGet(
					uint32_t	opcode,
					uint16_t	srcAddress,
					uint16_t	dstAddress,
					const void*	frame,
					unsigned	length,
					uint16_t	appKeyIndex
					) noexcept;

	private: // Oscl::BT::Mesh::Message::ItemComposer _rxConfigFriendSet
		/** */
		bool	rxConfigFriendSet(
					uint32_t	opcode,
					uint16_t	srcAddress,
					uint16_t	dstAddress,
					const void*	frame,
					unsigned	length,
					uint16_t	appKeyIndex
					) noexcept;

	private: // Oscl::BT::Mesh::Message::ItemComposer _rxConfigKeyRefreshPhaseGet
		/** */
		bool	rxConfigKeyRefreshPhaseGet(
					uint32_t	opcode,
					uint16_t	srcAddress,
					uint16_t	dstAddress,
					const void*	frame,
					unsigned	length,
					uint16_t	appKeyIndex
					) noexcept;

	private: // Oscl::BT::Mesh::Message::ItemComposer _rxConfigKeyRefreshPhaseSet
		/** */
		bool	rxConfigKeyRefreshPhaseSet(
					uint32_t	opcode,
					uint16_t	srcAddress,
					uint16_t	dstAddress,
					const void*	frame,
					unsigned	length,
					uint16_t	appKeyIndex
					) noexcept;

	private: // Oscl::BT::Mesh::Message::ItemComposer _rxConfigHeartbeatPublicationGet
		/** */
		bool	rxConfigHeartbeatPublicationGet(
					uint32_t	opcode,
					uint16_t	srcAddress,
					uint16_t	dstAddress,
					const void*	frame,
					unsigned	length,
					uint16_t	appKeyIndex
					) noexcept;

	private: // Oscl::BT::Mesh::Message::ItemComposer _rxConfigHeartbeatPublicationSet
		/** */
		bool	rxConfigHeartbeatPublicationSet(
					uint32_t	opcode,
					uint16_t	srcAddress,
					uint16_t	dstAddress,
					const void*	frame,
					unsigned	length,
					uint16_t	appKeyIndex
					) noexcept;

	private: // Oscl::BT::Mesh::Message::ItemComposer _rxConfigHeartbeatSubscriptionGet
		/** */
		bool	rxConfigHeartbeatSubscriptionGet(
					uint32_t	opcode,
					uint16_t	srcAddress,
					uint16_t	dstAddress,
					const void*	frame,
					unsigned	length,
					uint16_t	appKeyIndex
					) noexcept;

	private: // Oscl::BT::Mesh::Message::ItemComposer _rxConfigHeartbeatSubscriptionSet
		/** */
		bool	rxConfigHeartbeatSubscriptionSet(
					uint32_t	opcode,
					uint16_t	srcAddress,
					uint16_t	dstAddress,
					const void*	frame,
					unsigned	length,
					uint16_t	appKeyIndex
					) noexcept;

	private: // Oscl::BT::Mesh::Message::ItemComposer _rxConfigLowPowerNodePollTimeoutGet
		/** */
		bool	rxConfigLowPowerNodePollTimeoutGet(
					uint32_t	opcode,
					uint16_t	srcAddress,
					uint16_t	dstAddress,
					const void*	frame,
					unsigned	length,
					uint16_t	appKeyIndex
					) noexcept;

	private: // Oscl::BT::Mesh::Message::ItemComposer _rxConfigNetworkTransmitGet
		/** */
		bool	rxConfigNetworkTransmitGet(
					uint32_t	opcode,
					uint16_t	srcAddress,
					uint16_t	dstAddress,
					const void*	frame,
					unsigned	length,
					uint16_t	appKeyIndex
					) noexcept;

	private: // Oscl::BT::Mesh::Message::ItemComposer _rxConfigNetworkTransmitSet
		/** */
		bool	rxConfigNetworkTransmitSet(
					uint32_t	opcode,
					uint16_t	srcAddress,
					uint16_t	dstAddress,
					const void*	frame,
					unsigned	length,
					uint16_t	appKeyIndex
					) noexcept;

	private:
		/** */
		void	sendConfigCompositionDataStatus(
					uint16_t	dst,
					uint8_t		page
					) noexcept;

		/** */
		void	sendConfigDefaultTtlStatus(
					uint16_t	dst,
					uint8_t		defaultTTL
					) noexcept;

		/** */
		void	sendConfigGattProxyStatus(
					uint16_t	dst,
					uint8_t		state
					) noexcept;

		/** */
		void	sendConfigRelayStatus(
					uint16_t	dst,
					uint8_t		relay,
					uint8_t		retransmitCount,
					uint8_t		retransmitIntervalSteps
					) noexcept;

		/** */
		void	sendConfigModelPublicationStatus(
					uint16_t	dst,
					uint8_t		statusCode,
					uint16_t	elementAddress,
					uint16_t	publishAddress,
					uint16_t	appKeyIndex,
					bool		credentialFlag,
					uint8_t		publishTTL,
					uint8_t		publishPeriod,
					uint8_t		publishRetransmitCount,
					uint8_t		publishRetransmitIntervalSteps,
					bool		sigModel,
					uint16_t	modelIdentifier,
					uint16_t	companyID	= 0
					) noexcept;

		/** */
		void	sendConfigModelSubscriptionStatus(
					uint16_t	dst,
					uint8_t		statusCode,
					uint16_t	elementAddress,
					uint16_t	address,
					bool		sigModel,
					uint16_t	modelIdentifier,
					uint16_t	companyID
					) noexcept;

		/** */
		void	sendConfigSigModelSubscriptionList(
					uint16_t		dst,
					uint8_t			statusCode,
					uint16_t		elementAddress,
					uint16_t		modelIdentifier,
					const uint16_t*	addressList	= 0,
					unsigned		nAddressListItems	= 0
					) noexcept;

		/** */
		void	sendConfigVendorModelSubscriptionList(
					uint16_t		dst,
					uint8_t			statusCode,
					uint16_t		elementAddress,
					uint16_t		modelIdentifier,
					uint16_t		companyID,
					const uint16_t*	addressList	= 0,
					unsigned		nAddressListItems	= 0
					) noexcept;

		/** */
		void	sendConfigNetKeyStatus(
					uint8_t		statusCode,
					uint8_t		keyIndicie0,
					uint8_t		keyIndicie1,
					uint16_t	dst
					) noexcept;

		/** */
		void	sendConfigNetKeyList(
					uint16_t		dst,
					const uint16_t*	netKeyIndicies,
					unsigned		nNetKeyIndicies
					) noexcept;

		/** */
		void	sendConfigAppKeyStatus(
					uint8_t		statusCode,
					uint8_t		keyIndicie0,
					uint8_t		keyIndicie1,
					uint8_t		keyIndicie2,
					uint16_t	dst
					) noexcept;

		/** */
		void	sendConfigAppKeyList(
					uint8_t		statusCode,
					uint8_t		keyIndicie0,
					uint8_t		keyIndicie1,
					uint16_t	dst
					) noexcept;

		/** */
		void	sendNodeIdentityStatus(
					uint8_t		statusCode,
					uint8_t		keyIndicie0,
					uint8_t		keyIndicie1,
					uint16_t	dst,
					uint8_t		state
					) noexcept;

		/** */
		void	sendConfigModelAppStatus(
					uint8_t		statusCode
					) noexcept;

		/** */
		void	sendConfigBeaconStatus(
					uint16_t	dst,
					uint8_t		state
					) noexcept;

		/** */
		void	sendConfigNodeResetStatus(
					uint16_t	dst
					) noexcept;

		/** */
		void	sendConfigFriendStatus(
					uint16_t	dst,
					uint8_t		state
					) noexcept;

		/** */
		void	sendConfigKeyRefreshPhaseStatus(
					uint16_t	dst,
					uint8_t		status,
					uint16_t	netKeyIndex,
					uint8_t		phase
					) noexcept;

		/** */
		void	sendConfigHeartbeatPublicationStatus(
					uint16_t	dst,
					uint8_t		status,
					uint16_t	destination,
					uint8_t		countLog,
					uint8_t		periodLog,
					uint8_t		ttl,
					uint16_t	features,
					uint16_t	netKeyIndex
					) noexcept;

		/** */
		void	sendConfigHeartbeatSubscriptionStatus(
					uint16_t	dst,
					uint8_t		status,
					uint16_t	source,
					uint16_t	destination,
					uint8_t		periodLog,
					uint8_t		countLog,
					uint8_t		minHops,
					uint8_t		maxHops
					) noexcept;

		/** */
		void	sendConfigLowPowerNodePollTimeoutStatus(
					uint16_t	dst,
					uint16_t	lpnAddress,
					uint32_t	pollTimeout
					) noexcept;

		/** */
		void	sendConfigNetworkTransmitStatus(
					uint16_t	dst,
					uint8_t		networkTransmitCount,
					uint8_t		networkTransmitIntervalSteps
					) noexcept;

		/** */
		void	sendConfigSigModelAppList(
					uint16_t	dst,
					uint8_t		status,
					uint16_t	elementAddress,
					uint16_t	sigModelID,
					unsigned	nAppKeys,
					const void*	appKeys	= 0
					) noexcept;

	private:
		/** RETURN: 0 for failure and subscription status
			has been sent. Otherwise, return pointer to
			the model.
		 */
		Oscl::BT::Mesh::Model::Api*	findSubscriptionModelApi(
										uint16_t	dst,
										uint16_t	elementAddress,
										uint16_t	address,
										bool		isSigModel,
										uint16_t	modelID,
										uint16_t	companyID
										) noexcept;

		/** */
		Oscl::BT::Mesh::Network::Api*	findNetwork(uint16_t index) noexcept;

	private: // Oscl::BT::Mesh::Key::Iterator::Composer
		/** */
		bool	configAppKeyGetItem(
					unsigned		index,
					const uint8_t	key[16],
					uint8_t			id
					) noexcept;

	private:
		/** */
		void	resetIfReset() noexcept;

	private: // Oscl::BT::Mesh::Transport::TX::Complete::Api
		/** This operation is invoked when the
			PDU has been successfully transmitted
			by the transport layer.

			For unsegmented messages, this is invoked
			immediately since there is no acknowledgment.

			For segmented messages sent to a unicast
			address destination, this operation is only
			invoked after all segments have been acknowledged
			by the destination or its friend.

			For segmented messages sent to a broadcast,
			group, or virtual address, this is invoked
			after all segments have been transmitted for
			the specified number of retrys.
		 */
		void	success() noexcept;

		/** This operation is invoked when the
			PDU has been successfully transmitted
			by the transport layer.

			For unsegmented messages, this is invoked
			immediately since there is no acknowledgment.

			For segmented messages sent to a unicast
			address destination, this operation is only
			invoked after all segments have been acknowledged
			by the destination or its friend.

			For segmented messages sent to a broadcast,
			group, or virtual address, this is invoked
			after all segments have been transmitted for
			the specified number of retrys.
		 */
		void	successOnBehalfOfLPN() noexcept;

		/**	This operation is invoked only for segmented
			messages sent to a unicast address where the
			remote did not acknowledge all segments before
			the timer expired.
		 */
		void	failedTimeout() noexcept;

		/**	This operation is invoked only for segmented
			messages sent to a unicast address where the
			remote indicated that it is unable to receive
			the message at this time (due to resource
			limitations).
		 */
		void	failedBusy() noexcept;

		/**	This operation is invoked when the transport
			layer does not currently have enough resources
			to send the message.
		 */
		void	failedOutOfResources() noexcept;

		/**	This operation is invoked for messages that
			are too big to be handled by the transport
			layer. This indicates a software error.
		 */
		void	failedTooBig() noexcept;

	private: // Oscl::BT::Mesh::Model::Api
		/** */
		const Oscl::BT::Mesh::Key::Result::Var::State&	bind(uint16_t appKeyIndex) noexcept;

		/** RETURN: true for failure. */
		const Oscl::BT::Mesh::Key::Result::Var::State&	unbind(uint16_t appKeyIndex) noexcept;

		/** RETURN: 0 if binding is not supported. */
		Oscl::BT::Mesh::Model::Key::Api*	getModelKeyApi() noexcept;

		/** RETURN: true for match */
		bool	sigModelIdMatch(uint16_t modelID) const noexcept;

		/** RETURN: true for match */
		bool	vendorModelIdMatch(uint16_t vendorID, uint16_t modelID) const noexcept;

		/** RETURN: true if the model supports AppKey binding. */
		bool	modelSupportsAppKeyBinding() const noexcept;

		/** RETURN: zero if the model does NOT support publication.
		 */
		Oscl::BT::Mesh::Publication::Api*	getPublicationApi() noexcept;

		/** RETURN: zero if the model does NOT support subscription.
		 */
		Oscl::BT::Mesh::Subscription::Api*	getSubscriptionApi() noexcept;

	private: // Oscl::BT::Mesh::Transport::Upper::RX::Composer	_upperTransportRxComposer;
		/** Process the encrypted Access PDU
		 */
		void	processDeviceAccessPDU(
					const void*	frame,
					unsigned	length,
					uint32_t	ivIndex,
					uint32_t	seq,
					uint16_t	src,
					uint16_t	dst,
					uint8_t		aid,
					bool		akf,
					bool		segmented
					) noexcept;

	private:
		/** Process the decrypted Access Message
		 */
		void	processAccessMessage(
					const void*	frame,
					unsigned	length,
					uint16_t	src,
					uint16_t	dst
					) noexcept;

	private:
		/** RETURN: true if the dst address matches a
			subscription address.
		 */
		bool	subscriptionMatch(uint16_t dst) noexcept;

	private: // Oscl::BT::Mesh::Key::Result::Composer  _configAppKeyAddResult
		/**	*/
		void	configAppKeyAddResultSuccess() noexcept;

		/**	*/
		void	configAppKeyAddResultInvalidIndex() noexcept;

		/**	*/
		void	configAppKeyAddResultNoSuchIndex() noexcept;

		/**	*/
		void	configAppKeyAddResultIndexAlreadyExists() noexcept;

		/**	*/
		void	configAppKeyAddResultOutOfResources() noexcept;

		/**	*/
		void	configAppKeyAddResultInvalidState() noexcept;

	private: // Oscl::BT::Mesh::Key::Result::Composer  _configAppKeyUpdateResult
		/**	*/
		void	configAppKeyUpdateResultSuccess() noexcept;

		/**	*/
		void	configAppKeyUpdateResultInvalidIndex() noexcept;

		/**	*/
		void	configAppKeyUpdateResultNoSuchIndex() noexcept;

		/**	*/
		void	configAppKeyUpdateResultIndexAlreadyExists() noexcept;

		/**	*/
		void	configAppKeyUpdateResultOutOfResources() noexcept;

		/**	*/
		void	configAppKeyUpdateResultInvalidState() noexcept;

	private: // Oscl::BT::Mesh::Key::Result::Composer  _configAppKeyDeleteResult
		/**	*/
		void	configAppKeyDeleteResultSuccess() noexcept;

		/**	*/
		void	configAppKeyDeleteResultInvalidIndex() noexcept;

		/**	*/
		void	configAppKeyDeleteResultNoSuchIndex() noexcept;

		/**	*/
		void	configAppKeyDeleteResultIndexAlreadyExists() noexcept;

		/**	*/
		void	configAppKeyDeleteResultOutOfResources() noexcept;

		/**	*/
		void	configAppKeyDeleteResultInvalidState() noexcept;

	private: // Oscl::BT::Mesh::Key::Result::Composer  _configAppKeyGetResult
		/**	*/
		void	configAppKeyGetResultSuccess() noexcept;

		/**	*/
		void	configAppKeyGetResultInvalidIndex() noexcept;

		/**	*/
		void	configAppKeyGetResultNoSuchIndex() noexcept;

		/**	*/
		void	configAppKeyGetResultIndexAlreadyExists() noexcept;

		/**	*/
		void	configAppKeyGetResultOutOfResources() noexcept;

		/**	*/
		void	configAppKeyGetResultInvalidState() noexcept;

	private: // Oscl::BT::Mesh::Key::Result::Composer  _configModelAppBindResult
		/**	*/
		void	configModelAppBindResultSuccess() noexcept;

		/**	*/
		void	configModelAppBindResultInvalidIndex() noexcept;

		/**	*/
		void	configModelAppBindResultNoSuchIndex() noexcept;

		/**	*/
		void	configModelAppBindResultIndexAlreadyExists() noexcept;

		/**	*/
		void	configModelAppBindResultOutOfResources() noexcept;

		/**	*/
		void	configModelAppBindResultInvalidState() noexcept;

	private: // Oscl::BT::Mesh::Key::Iterator::Composer
		/** */
		bool	configModelAppBindItem(
					unsigned		index,
					const uint8_t	key[16],
					uint8_t			id
					) noexcept;

	private: // Oscl::BT::Mesh::Key::Iterator::Composer
		/** */
		bool	configSigModelAppBindItem(
					unsigned		index
					) noexcept;
	};

}
}
}
}
}

#endif
