/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <new>
#include "part.h"
#include "oscl/endian/decoder/linear/part.h"
#include "oscl/encoder/le/base.h"
#include "oscl/encoder/be/base.h"
#include "oscl/error/info.h"
#include "oscl/error/fatal.h"
#include "oscl/aes128/ccm.h"
#include "oscl/bluetooth/strings/module.h"
#include "oscl/bluetooth/mesh/message/foundation/status/value.h"
#include "oscl/bluetooth/mesh/address/virtual.h"
#include "oscl/bluetooth/mesh/model/key/iterator/composer.h"
#include "oscl/bluetooth/mesh/key/iterator/composer.h"
#include "oscl/bluetooth/mesh/model/opcode/decoder.h"
#include "oscl/bluetooth/mesh/model/opcode/encoder.h"
#include "oscl/bluetooth/mesh/constants.h"

using namespace Oscl::BT::Mesh::Model::Foundation;

// The txTTL is used to send responses.
// It was originally the default TTL (0xFF),
// which apparently doesn't work well with some
// <cough> SiLabs configuration app when the
// system default TTL is relatively low (4).
// In lower layers, the TTL is used for timer
// calculations. The replies were timing out.
//
// Unfortuneately, that did not resolve the problem.
constexpr uint8_t	txTTL	= 0xFF;	// Max TTL

//#define DEBUG_TRACE

static void	packEven(
			uint16_t	keyIndex,
			uint8_t		packedOutput[2]
			) noexcept{

	keyIndex	&= 0x0FFF;	// only 12-bits are "significant"

	packedOutput[0]	= keyIndex & 0x00FF;
	packedOutput[1]	= keyIndex >> 8;
	}

static void	packOdd(
			uint16_t	keyIndex,
			uint8_t		packedOutput[2]
			) noexcept{

	keyIndex	&= 0x0FFF;	// only 12-bits are "significant"

	packedOutput[0]	|= (keyIndex & 0x000F) << 4;
	packedOutput[1]	= keyIndex >> 4;
	}

Part::Part(
	Oscl::Pdu::Memory::Api&					freeStoreApi,
	Oscl::Timer::Factory::Api&				timerFactoryApi,
	Oscl::BT::Mesh::Node::Api&				nodeApi,
	Oscl::BT::Mesh::Element::
	Symbiont::Kernel::Api&					kernelApi,
	Oscl::BT::Mesh::
	Transport::Lower::TX::Api&				lowerTransportApi,
	Oscl::BT::Mesh::Network::Api&			networkApi,
	Oscl::BT::Mesh::Element::Api&			elementApi,
	Oscl::BT::Mesh::Message::
	Config::Composition::Data::State::Api&	compositionDataStateApi,
	uint16_t								elementAddress
	) noexcept:
		_freeStoreApi(freeStoreApi),
		_timerFactoryApi(timerFactoryApi),
		_nodeApi(nodeApi),
		_kernelApi(kernelApi),
		_networkApi(networkApi),
		_elementApi(elementApi),
		_compositionDataStateApi(compositionDataStateApi),
		_elementAddress(elementAddress),
		_rxConfigBeaconGet(
			*this,
			&Part::rxConfigBeaconGet
			),
		_rxConfigBeaconSet(
			*this,
			&Part::rxConfigBeaconSet
			),
		_rxConfigCompositionDataGet(
			*this,
			&Part::rxConfigCompositionDataGet
			),
		_rxConfigDefaultTtlGet(
			*this,
			&Part::rxConfigDefaultTtlGet
			),
		_rxConfigDefaultTtlSet(
			*this,
			&Part::rxConfigDefaultTtlSet
			),
		_rxConfigGattProxyGet(
			*this,
			&Part::rxConfigGattProxyGet
			),
		_rxConfigGattProxySet(
			*this,
			&Part::rxConfigGattProxySet
			),
		_rxConfigRelayGet(
			*this,
			&Part::rxConfigRelayGet
			),
		_rxConfigRelaySet(
			*this,
			&Part::rxConfigRelaySet
			),
		_rxConfigModelPublicationGet(
			*this,
			&Part::rxConfigModelPublicationGet
			),
		_rxConfigModelPublicationSet(
			*this,
			&Part::rxConfigModelPublicationSet
			),
		_rxConfigModelPublicationVirtualAddressSet(
			*this,
			&Part::rxConfigModelPublicationVirtualAddressSet
			),
		_rxConfigModelSubscriptionAdd(
			*this,
			&Part::rxConfigModelSubscriptionAdd
			),
		_rxConfigModelSubscriptionDelete(
			*this,
			&Part::rxConfigModelSubscriptionDelete
			),
		_rxConfigModelSubscriptionOverwrite(
			*this,
			&Part::rxConfigModelSubscriptionOverwrite
			),
		_rxConfigModelSubscriptionDeleteAll(
			*this,
			&Part::rxConfigModelSubscriptionDeleteAll
			),
		_rxConfigModelSubscriptionVirtualAddressAdd(
			*this,
			&Part::rxConfigModelSubscriptionVirtualAddressAdd
			),
		_rxConfigModelSubscriptionVirtualAddressDelete(
			*this,
			&Part::rxConfigModelSubscriptionVirtualAddressDelete
			),
		_rxConfigModelSubscriptionVirtualAddressOverwrite(
			*this,
			&Part::rxConfigModelSubscriptionVirtualAddressOverwrite
			),
		_rxConfigSigModelSubscriptionGet(
			*this,
			&Part::rxConfigSigModelSubscriptionGet
			),
		_rxConfigVendorModelSubscriptionGet(
			*this,
			&Part::rxConfigVendorModelSubscriptionGet
			),
		_rxConfigNetKeyAdd(
			*this,
			&Part::rxConfigNetKeyAdd
			),
		_rxConfigNetKeyUpdate(
			*this,
			&Part::rxConfigNetKeyUpdate
			),
		_rxConfigNetKeyDelete(
			*this,
			&Part::rxConfigNetKeyDelete
			),
		_rxConfigNetKeyGet(
			*this,
			&Part::rxConfigNetKeyGet
			),
		_rxConfigAppKeyAdd(
			*this,
			&Part::rxConfigAppKeyAdd
			),
		_rxConfigAppKeyUpdate(
			*this,
			&Part::rxConfigAppKeyUpdate
			),
		_rxConfigAppKeyDelete(
			*this,
			&Part::rxConfigAppKeyDelete
			),
		_rxConfigAppKeyGet(
			*this,
			&Part::rxConfigAppKeyGet
			),
		_rxConfigNodeIdentityGet(
			*this,
			&Part::rxConfigNodeIdentityGet
			),
		_rxConfigNodeIdentitySet(
			*this,
			&Part::rxConfigNodeIdentitySet
			),
		_rxConfigModelAppBind(
			*this,
			&Part::rxConfigModelAppBind
			),
		_rxConfigModelAppUnbind(
			*this,
			&Part::rxConfigModelAppUnbind
			),
		_rxConfigSigModelAppGet(
			*this,
			&Part::rxConfigSigModelAppGet
			),
		_rxConfigNodeReset(
			*this,
			&Part::rxConfigNodeReset
			),
		_rxConfigFriendGet(
			*this,
			&Part::rxConfigFriendGet
			),
		_rxConfigFriendSet(
			*this,
			&Part::rxConfigFriendSet
			),
		_rxConfigKeyRefreshPhaseGet(
			*this,
			&Part::rxConfigKeyRefreshPhaseGet
			),
		_rxConfigKeyRefreshPhaseSet(
			*this,
			&Part::rxConfigKeyRefreshPhaseSet
			),
		_rxConfigHeartbeatPublicationGet(
			*this,
			&Part::rxConfigHeartbeatPublicationGet
			),
		_rxConfigHeartbeatPublicationSet(
			*this,
			&Part::rxConfigHeartbeatPublicationSet
			),
		_rxConfigHeartbeatSubscriptionGet(
			*this,
			&Part::rxConfigHeartbeatSubscriptionGet
			),
		_rxConfigHeartbeatSubscriptionSet(
			*this,
			&Part::rxConfigHeartbeatSubscriptionSet
			),
		_rxConfigLowPowerNodePollTimeoutGet(
			*this,
			&Part::rxConfigLowPowerNodePollTimeoutGet
			),
		_rxConfigNetworkTransmitGet(
			*this,
			&Part::rxConfigNetworkTransmitGet
			),
		_rxConfigNetworkTransmitSet(
			*this,
			&Part::rxConfigNetworkTransmitSet
			),
		_configCompositionDataStatus(
			_upperTransportTx,
			compositionDataStateApi
			),
		_upperTransportTx(
			freeStoreApi,
			nodeApi,
			lowerTransportApi,
			elementAddress
			),
		_configAppKeyAddResult(
			*this,
			&Part::configAppKeyAddResultSuccess,
			&Part::configAppKeyAddResultInvalidIndex,
			&Part::configAppKeyAddResultNoSuchIndex,
			&Part::configAppKeyAddResultIndexAlreadyExists,
			&Part::configAppKeyAddResultOutOfResources,
			&Part::configAppKeyAddResultInvalidState
			),
		_configAppKeyUpdateResult(
			*this,
			&Part::configAppKeyUpdateResultSuccess,
			&Part::configAppKeyUpdateResultInvalidIndex,
			&Part::configAppKeyUpdateResultNoSuchIndex,
			&Part::configAppKeyUpdateResultIndexAlreadyExists,
			&Part::configAppKeyUpdateResultOutOfResources,
			&Part::configAppKeyUpdateResultInvalidState
			),
		_configAppKeyDeleteResult(
			*this,
			&Part::configAppKeyDeleteResultSuccess,
			&Part::configAppKeyDeleteResultInvalidIndex,
			&Part::configAppKeyDeleteResultNoSuchIndex,
			&Part::configAppKeyDeleteResultIndexAlreadyExists,
			&Part::configAppKeyDeleteResultOutOfResources,
			&Part::configAppKeyDeleteResultInvalidState
			),
		_configAppKeyGetResult(
			*this,
			&Part::configAppKeyGetResultSuccess,
			&Part::configAppKeyGetResultInvalidIndex,
			&Part::configAppKeyGetResultNoSuchIndex,
			&Part::configAppKeyGetResultIndexAlreadyExists,
			&Part::configAppKeyGetResultOutOfResources,
			&Part::configAppKeyGetResultInvalidState
			),
		_configModelAppBindResult(
			*this,
			&Part::configModelAppBindResultSuccess,
			&Part::configModelAppBindResultInvalidIndex,
			&Part::configModelAppBindResultNoSuchIndex,
			&Part::configModelAppBindResultIndexAlreadyExists,
			&Part::configModelAppBindResultOutOfResources,
			&Part::configModelAppBindResultInvalidState
			),
		_resetting(false)
		{
	_messageHandlers.put(&_rxConfigBeaconGet);
	_messageHandlers.put(&_rxConfigBeaconSet);
	_messageHandlers.put(&_rxConfigCompositionDataGet);
	_messageHandlers.put(&_rxConfigDefaultTtlGet);
	_messageHandlers.put(&_rxConfigDefaultTtlSet);
	_messageHandlers.put(&_rxConfigGattProxyGet);
	_messageHandlers.put(&_rxConfigGattProxySet);
	_messageHandlers.put(&_rxConfigRelayGet);
	_messageHandlers.put(&_rxConfigRelaySet);
	_messageHandlers.put(&_rxConfigModelPublicationGet);
	_messageHandlers.put(&_rxConfigModelPublicationSet);
	_messageHandlers.put(&_rxConfigModelPublicationVirtualAddressSet);
	_messageHandlers.put(&_rxConfigModelSubscriptionAdd);
	_messageHandlers.put(&_rxConfigModelSubscriptionDelete);
	_messageHandlers.put(&_rxConfigModelSubscriptionDeleteAll);
	_messageHandlers.put(&_rxConfigModelSubscriptionOverwrite);
	_messageHandlers.put(&_rxConfigModelSubscriptionVirtualAddressAdd);
	_messageHandlers.put(&_rxConfigModelSubscriptionVirtualAddressDelete);
	_messageHandlers.put(&_rxConfigModelSubscriptionVirtualAddressOverwrite);
	_messageHandlers.put(&_rxConfigSigModelSubscriptionGet);
	_messageHandlers.put(&_rxConfigVendorModelSubscriptionGet);
	_messageHandlers.put(&_rxConfigNetKeyAdd);
	_messageHandlers.put(&_rxConfigNetKeyUpdate);
	_messageHandlers.put(&_rxConfigNetKeyDelete);
	_messageHandlers.put(&_rxConfigNetKeyGet);
	_messageHandlers.put(&_rxConfigAppKeyAdd);
	_messageHandlers.put(&_rxConfigAppKeyUpdate);
	_messageHandlers.put(&_rxConfigAppKeyDelete);
	_messageHandlers.put(&_rxConfigAppKeyGet);
	_messageHandlers.put(&_rxConfigNodeIdentityGet);
	_messageHandlers.put(&_rxConfigNodeIdentitySet);
	_messageHandlers.put(&_rxConfigModelAppBind);
	_messageHandlers.put(&_rxConfigModelAppUnbind);
	_messageHandlers.put(&_rxConfigSigModelAppGet);
	_messageHandlers.put(&_rxConfigNodeReset);
	_messageHandlers.put(&_rxConfigFriendGet);
	_messageHandlers.put(&_rxConfigFriendSet);
	_messageHandlers.put(&_rxConfigKeyRefreshPhaseGet);
	_messageHandlers.put(&_rxConfigKeyRefreshPhaseSet);
	_messageHandlers.put(&_rxConfigHeartbeatPublicationGet);
	_messageHandlers.put(&_rxConfigHeartbeatPublicationSet);
	_messageHandlers.put(&_rxConfigHeartbeatSubscriptionGet);
	_messageHandlers.put(&_rxConfigHeartbeatSubscriptionSet);
	_messageHandlers.put(&_rxConfigLowPowerNodePollTimeoutGet);
	_messageHandlers.put(&_rxConfigNetworkTransmitGet);
	_messageHandlers.put(&_rxConfigNetworkTransmitSet);
	}

Part::~Part() noexcept{
	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif
	}

void	Part::stop() noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif

	_upperTransportTx.stop();
	}

void	Part::processAccessMessage(
			const void*	frame,
			unsigned	length,
			uint16_t	src,
			uint16_t	dst
			) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);

	Oscl::Error::Info::hexDump(
		frame,
		length
		);
	#endif

	Oscl::Endian::Decoder::Linear::Part
	leDecoder(
		frame,
		length
		);

	Oscl::Decoder::Api&	decoder	= leDecoder.le();

	uint32_t
	opcode	= Oscl::BT::Mesh::Model::OpCode::decode(decoder);

	using namespace Oscl::BT::Mesh::Model::OpCode;

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s:\n"
		"\topcode: (0x%4.4X) %s\n",
		__PRETTY_FUNCTION__,
		opcode,
		Oscl::Bluetooth::Strings::meshFoundationModelOpcode(opcode)
		);

	#endif

	const uint8_t*	p	= (const uint8_t*)frame;

	unsigned
	remaining	= decoder.remaining();

	unsigned
	payloadOffset	= length - remaining;

	for(
		Oscl::BT::Mesh::Message::Item*	item	= _messageHandlers.first();
		item;
		item	= _messageHandlers.next(item)
		){
		bool
		handled	= item->receive(
					opcode,
					src,
					dst,
					&p[payloadOffset],
					remaining,
					0xFFFF	// No AppKey for Foundation Model
					);
		if(handled){
			return;
			}
		}
	}

bool	Part::wantsDestination(uint16_t dst) noexcept{
	return subscriptionMatch(dst);
	}

void	Part::processAccessMessage(
			const void*	frame,
			unsigned	length,
			uint16_t	src,
			uint16_t	dst,
			uint16_t	appKeyIndex
			) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif

	if(appKeyIndex <= 0x0FFF){
		// We only handle DevKey access messages.
		return;
		}

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s: DevKey match.\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif

	processAccessMessage(
		frame,
		length,
		src,
		dst
		);
	}

bool	Part::subscriptionMatch(uint16_t dst) noexcept{
	return false;
	}

Oscl::BT::Mesh::Model::Api*	Part::getSigModelApi(uint16_t modelID) noexcept{
	if(sigModelIdMatch(modelID)){
		return this;
		}

	return 0;
	}

Oscl::BT::Mesh::Model::Api*	Part::getVendorModelApi(uint16_t cid, uint16_t modelID) noexcept{
	if(vendorModelIdMatch(cid,modelID)){
		return this;
		}

	return 0;
	}

bool	Part::rxConfigBeaconGet(
			uint32_t	opcode,
			uint16_t	srcAddress,
			uint16_t	dstAddress,
			const void*	frame,
			unsigned	length,
			uint16_t	appKeyIndex
			) noexcept{

	if(opcode != Oscl::BT::Mesh::Model::OpCode::configBeaconGet){
		return false;
		}

	// This message has no parameters

	sendConfigBeaconStatus(
		srcAddress,
		_nodeApi.getConfigSecureNetworkBeacon()
		);

	return true;
	}

bool	Part::rxConfigBeaconSet(
			uint32_t	opcode,
			uint16_t	srcAddress,
			uint16_t	dstAddress,
			const void*	frame,
			unsigned	length,
			uint16_t	appKeyIndex
			) noexcept{

	if(opcode != Oscl::BT::Mesh::Model::OpCode::configBeaconSet){
		return false;
		}

	Oscl::Endian::Decoder::Linear::Part
	leDecoder(
		frame,
		length
		);

	Oscl::Decoder::Api&	decoder	= leDecoder.le();

	uint8_t	state;

	decoder.decode(state);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			OSCL_PRETTY_FUNCTION
			);
		return true;
		}

	_nodeApi.setConfigSecureNetworkBeacon(state);

	sendConfigBeaconStatus(
		srcAddress,
		_nodeApi.getConfigSecureNetworkBeacon()
		);

	return true;
	}

bool	Part::rxConfigCompositionDataGet(
			uint32_t	opcode,
			uint16_t	srcAddress,
			uint16_t	dstAddress,
			const void*	frame,
			unsigned	length,
			uint16_t	appKeyIndex
			) noexcept{

	if(opcode != Oscl::BT::Mesh::Model::OpCode::configCompositionDataGet){
		return false;
		}

	Oscl::Endian::Decoder::Linear::Part
	leDecoder(
		frame,
		length
		);

	Oscl::Decoder::Api&	decoder	= leDecoder.le();

	uint8_t	page;

	decoder.decode(page);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			OSCL_PRETTY_FUNCTION
			);
		return true;
		}

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n"
		"\tPage: %u\n"
		"",
		__PRETTY_FUNCTION__,
		page
		);

	Oscl::Error::Info::hexDump(
		frame,
		length
		);
	#endif

	uint16_t	dst	= srcAddress;

	// ===================
	// FIXME: test segmentation timers, by transmitting
	// to a bogus unicast address instead of the srcAddres.
//	dst	= 0x0FFF;
	// ===================

	sendConfigCompositionDataStatus(
		dst,
		page
		);

	return true;
	}

bool	Part::rxConfigDefaultTtlGet(
			uint32_t	opcode,
			uint16_t	srcAddress,
			uint16_t	dstAddress,
			const void*	frame,
			unsigned	length,
			uint16_t	appKeyIndex
			) noexcept{

	if(opcode != Oscl::BT::Mesh::Model::OpCode::configDefaultTtlGet){
		return false;
		}


	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n"
		"",
		__PRETTY_FUNCTION__
		);

	Oscl::Error::Info::hexDump(
		frame,
		length
		);
	#endif

	uint16_t	dst	= srcAddress;

	uint8_t	defaultTTL	= _nodeApi.getConfigDefaultTtl();

	sendConfigDefaultTtlStatus(
		dst,
		defaultTTL
		);

	return true;
	}

bool	Part::rxConfigDefaultTtlSet(
			uint32_t	opcode,
			uint16_t	srcAddress,
			uint16_t	dstAddress,
			const void*	frame,
			unsigned	length,
			uint16_t	appKeyIndex
			) noexcept{

	if(opcode != Oscl::BT::Mesh::Model::OpCode::configDefaultTtlSet){
		return false;
		}

	Oscl::Endian::Decoder::Linear::Part
	leDecoder(
		frame,
		length
		);

	Oscl::Decoder::Api&
	decoder	= leDecoder.le();

	uint8_t	defaultTTL;

	decoder.decode(defaultTTL);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			OSCL_PRETTY_FUNCTION
			);
		return true;
		}

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n"
		"\tDefault TTL: %u\n"
		"",
		__PRETTY_FUNCTION__,
		defaultTTL
		);

	Oscl::Error::Info::hexDump(
		frame,
		length
		);
	#endif

	uint16_t	dst	= srcAddress;

	_nodeApi.setConfigDefaultTtl(defaultTTL);

	sendConfigDefaultTtlStatus(
		dst,
		defaultTTL
		);

	return true;
	}

bool	Part::rxConfigGattProxyGet(
			uint32_t	opcode,
			uint16_t	srcAddress,
			uint16_t	dstAddress,
			const void*	frame,
			unsigned	length,
			uint16_t	appKeyIndex
			) noexcept{

	if(opcode != Oscl::BT::Mesh::Model::OpCode::configGattProxyGet){
		return false;
		}

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n"
		"",
		__PRETTY_FUNCTION__
		);

	Oscl::Error::Info::hexDump(
		frame,
		length
		);
	#endif

	uint16_t	dst	= srcAddress;

	uint8_t	state	= _nodeApi.getConfigGattProxy();

	sendConfigGattProxyStatus(
		dst,
		state
		);

	return true;
	}

bool	Part::rxConfigGattProxySet(
			uint32_t	opcode,
			uint16_t	srcAddress,
			uint16_t	dstAddress,
			const void*	frame,
			unsigned	length,
			uint16_t	appKeyIndex
			) noexcept{

	if(opcode != Oscl::BT::Mesh::Model::OpCode::configGattProxySet){
		return false;
		}

	Oscl::Endian::Decoder::Linear::Part
	leDecoder(
		frame,
		length
		);

	Oscl::Decoder::Api&
	decoder	= leDecoder.le();

	uint8_t	state;

	decoder.decode(state);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			OSCL_PRETTY_FUNCTION
			);
		return true;
		}

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n"
		"\tState: 0x%2.2X\n"
		"",
		__PRETTY_FUNCTION__,
		state
		);

	Oscl::Error::Info::hexDump(
		frame,
		length
		);
	#endif

	uint16_t	dst	= srcAddress;

	_nodeApi.setConfigGattProxy(state);

	sendConfigGattProxyStatus(
		dst,
		state
		);

	return true;
	}

bool	Part::rxConfigRelayGet(
			uint32_t	opcode,
			uint16_t	srcAddress,
			uint16_t	dstAddress,
			const void*	frame,
			unsigned	length,
			uint16_t	appKeyIndex
			) noexcept{

	if(opcode != Oscl::BT::Mesh::Model::OpCode::configRelayGet){
		return false;
		}

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n"
		"",
		__PRETTY_FUNCTION__
		);

	Oscl::Error::Info::hexDump(
		frame,
		length
		);
	#endif

	uint16_t	dst	= srcAddress;

	uint8_t
	relay	= _nodeApi.getConfigRelayState();

	uint8_t
	retransmitCount	= _nodeApi.getConfigRelayRetransmitCount();

	uint8_t
	retransmitIntervalSteps	= _nodeApi.getConfigRelayRetransmitIntervalSteps();

	sendConfigRelayStatus(
		dst,
		relay,
		retransmitCount,
		retransmitIntervalSteps
		);

	return true;
	}

bool	Part::rxConfigRelaySet(
			uint32_t	opcode,
			uint16_t	srcAddress,
			uint16_t	dstAddress,
			const void*	frame,
			unsigned	length,
			uint16_t	appKeyIndex
			) noexcept{

	if(opcode != Oscl::BT::Mesh::Model::OpCode::configRelaySet){
		return false;
		}

	Oscl::Endian::Decoder::Linear::Part
	leDecoder(
		frame,
		length
		);

	Oscl::Decoder::Api&
	decoder	= leDecoder.le();

	uint8_t	relay;
	uint8_t	countSteps;

	decoder.decode(relay);
	decoder.decode(countSteps);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			OSCL_PRETTY_FUNCTION
			);
		return true;
		}

	uint8_t
	retransmitCount	= (countSteps >> 5);

	uint8_t
	retransmitIntervalSteps	= (countSteps & 0x1F);


	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n"
		"\tRelay: 0x%2.2X\n"
		"\tcountSteps: 0x%2.2X\n"
		"\t\tRetransmit Count: (0x%2.2X) %u\n"
		"\t\tRetransmit Interval Steps: (0x%2.2X) %u\n"
		"",
		__PRETTY_FUNCTION__,
		relay,
		countSteps,
		retransmitCount,
		retransmitCount,
		retransmitIntervalSteps,
		retransmitIntervalSteps
		);

	Oscl::Error::Info::hexDump(
		frame,
		length
		);
	#endif

	uint16_t	dst	= srcAddress;

	_nodeApi.setConfigRelay(
		relay,
		retransmitCount,
		retransmitIntervalSteps
		);

	sendConfigRelayStatus(
		dst,
		relay,
		retransmitCount,
		retransmitIntervalSteps
		);

	return true;
	}

bool	Part::rxConfigModelPublicationGet(
			uint32_t	opcode,
			uint16_t	srcAddress,
			uint16_t	dstAddress,
			const void*	frame,
			unsigned	length,
			uint16_t	appKeyIndex
			) noexcept{

	if(opcode != Oscl::BT::Mesh::Model::OpCode::configModelPublicationGet){
		return false;
		}

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n"
		"\topcode: 0x%4.4X\n"
		"",
		__PRETTY_FUNCTION__,
		opcode
		);

	Oscl::Error::Info::hexDump(
		frame,
		length
		);
	#endif

	Oscl::Endian::Decoder::Linear::Part
	leDecoder(
		frame,
		length
		);

	Oscl::Decoder::Api&
	decoder	= leDecoder.le();

	uint16_t	elementAddress;
	uint16_t	modelID;

	decoder.decode(elementAddress);
	decoder.decode(modelID);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			OSCL_PRETTY_FUNCTION
			);
		return true;
		}

	uint16_t	companyID;

	decoder.decode(companyID);

	bool
	isSigModel	= decoder.underflow()?true:false;

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n"
		"\tElementAddress: 0x%4.4X\n"
		"\tModel ID: 0x%4.4X\n"
		"\tisSigModel: %s\n"
		"\tCompany ID: 0x%4.4X [%s]\n"
		"",
		__PRETTY_FUNCTION__,
		elementAddress,
		modelID,
		isSigModel?"true":"false",
		companyID,
		isSigModel?"INVALID":"VALID"
		);
	#endif

	Oscl::BT::Mesh::Element::Api*
	elementApi	= _networkApi.findElement(elementAddress);

	if(!elementApi){
		sendConfigModelPublicationStatus(
			srcAddress,
			Oscl::BT::Mesh::Message::Foundation::Status::invalidAddress,
			0,	// elementAddress,
			0,	// publishAddress,
			0,	// pubAppKeyIndex,
			false,	// credentialFlag,
			0,	// publishTTL,
			0,	// publishPeriod,
			0,	// publishRetransmitCount,
			0,	// publishRetransmitIntervalSteps,
			isSigModel,
			modelID,
			companyID
			);
		return true;
		}
	Oscl::BT::Mesh::Model::Api*
	modelApi	= (isSigModel)?
					elementApi->findSigModel(modelID):
					elementApi->findVendorModel(companyID,modelID)
					;

	if(!modelApi){
		sendConfigModelPublicationStatus(
			srcAddress,
			Oscl::BT::Mesh::Message::Foundation::Status::invalidPublishParameters,
			0,	// elementAddress,
			0,	// publishAddress,
			0,	// pubAppKeyIndex,
			false,	// credentialFlag,
			0,	// publishTTL,
			0,	// publishPeriod,
			0,	// publishRetransmitCount,
			0,	// publishRetransmitIntervalSteps,
			isSigModel,
			modelID,
			companyID
			);
		return true;
		}

	Oscl::BT::Mesh::Publication::Api*
	publicationApi	= modelApi->getPublicationApi();

	if(!publicationApi){
		sendConfigModelPublicationStatus(
			srcAddress,
			Oscl::BT::Mesh::Message::Foundation::Status::invalidPublishParameters,
			0,	// elementAddress,
			0,	// publishAddress,
			0,	// pubAppKeyIndex,
			false,	// credentialFlag,
			0,	// publishTTL,
			0,	// publishPeriod,
			0,	// publishRetransmitCount,
			0,	// publishRetransmitIntervalSteps,
			isSigModel,
			modelID,
			companyID
			);
		return true;
		}

	uint16_t	publishAddress;
	uint16_t	pubAppKeyIndex;
	bool		credentialFlag;
	uint8_t		publishTTL;
	uint8_t		publishPeriod;
	uint8_t		publishRetransmitCount;
	uint8_t		publishRetransmitIntervalSteps;

	bool
	failed	= publicationApi->getPulicationParameters(
				publishAddress,
				pubAppKeyIndex,
				credentialFlag,
				publishTTL,
				publishPeriod,
				publishRetransmitCount,
				publishRetransmitIntervalSteps
				);

	if(failed){
		sendConfigModelPublicationStatus(
			srcAddress,
			Oscl::BT::Mesh::Message::Foundation::Status::invalidPublishParameters,
			0,	// elementAddress,
			0,	// publishAddress,
			0,	// pubAppKeyIndex,
			false,	// credentialFlag,
			0,	// publishTTL,
			0,	// publishPeriod,
			0,	// publishRetransmitCount,
			0,	// publishRetransmitIntervalSteps,
			isSigModel,
			modelID,
			companyID
			);
		return true;
		}

	sendConfigModelPublicationStatus(
		srcAddress,
		Oscl::BT::Mesh::Message::Foundation::Status::success,
		elementAddress,
		publishAddress,
		pubAppKeyIndex,
		credentialFlag,
		publishTTL,
		publishPeriod,
		publishRetransmitCount,
		publishRetransmitIntervalSteps,
		isSigModel,
		modelID,
		companyID
		);

	return true;
	}

bool	Part::rxConfigModelPublicationSet(
			uint32_t	opcode,
			uint16_t	srcAddress,
			uint16_t	dstAddress,
			const void*	frame,
			unsigned	length,
			uint16_t	appKeyIndex
			) noexcept{

	if(opcode != Oscl::BT::Mesh::Model::OpCode::configModelPublicationSet){
		return false;
		}

	Oscl::Endian::Decoder::Linear::Part
	leDecoder(
		frame,
		length
		);

	Oscl::Decoder::Api&
	decoder	= leDecoder.le();

	uint16_t	elementAddress;
	uint16_t	publishAddress;
	uint16_t	appKeyIndexAndCredentialFlag;
	uint8_t		publishTTL;
	uint8_t		publishPeriod;
	uint8_t		publishRetransmit;
	uint16_t	modelID;
	uint16_t	companyID;

	decoder.decode(elementAddress);
	decoder.decode(publishAddress);
	decoder.decode(appKeyIndexAndCredentialFlag);
	decoder.decode(publishTTL);
	decoder.decode(publishPeriod);
	decoder.decode(publishRetransmit);
	decoder.decode(modelID);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			OSCL_PRETTY_FUNCTION
			);
		return true;
		}

	decoder.decode(companyID);

	bool
	isSigModel	= decoder.underflow()?true:false;

	uint16_t
	pubAppKeyIndex	= appKeyIndexAndCredentialFlag & 0x0FFF;

	bool
	credentialFlag	= (appKeyIndexAndCredentialFlag & (1<<12))?true:false;

	uint8_t
	publishRetransmitCount	= (publishRetransmit & 0x07);

	uint8_t
	publishRetransmitIntervalSteps	= (publishRetransmit >> 3);

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n"
		"\tElementAddress: 0x%4.4X\n"
		"\tPublish Address: 0x%4.4X\n"
		"\tAppKeyIndexAndCredentialFlag: 0x%4.4X\n"
		"\t\tAppKey Index: (0x%4.4X) %u\n"
		"\t\tCredential Flag: %s\n"
		"\tPublish TTL: 0x%2.2X\n"
		"\tPublish Period: %u\n"
		"\tPublish Retransmit: 0x%2.2X\n"
		"\t\tPublish Retransmit Count: %u\n"
		"\t\tPublish Retransmit Interval Steps: %u\n"
		"\tModel ID: 0x%4.4X\n"
		"\tisSigModel: %s\n"
		"\tCompany ID: 0x%4.4X [%s]\n"
		"",
		__PRETTY_FUNCTION__,
		elementAddress,
		publishAddress,
		appKeyIndexAndCredentialFlag,
		pubAppKeyIndex,
		pubAppKeyIndex,
		credentialFlag?"true":"false",
		publishTTL,
		publishPeriod,
		publishRetransmit,
		publishRetransmitCount,
		publishRetransmitIntervalSteps,
		modelID,
		isSigModel?"true":"false",
		companyID,
		isSigModel?"INVALID":"VALID"
		);
	#endif

	Oscl::BT::Mesh::Element::Api*
	elementApi	= _networkApi.findElement(elementAddress);

	if(!elementApi){
		sendConfigModelPublicationStatus(
			srcAddress,
			Oscl::BT::Mesh::Message::Foundation::Status::invalidAddress,
			elementAddress,
			publishAddress,
			pubAppKeyIndex,
			credentialFlag,
			publishTTL,
			publishPeriod,
			publishRetransmitCount,
			publishRetransmitIntervalSteps,
			isSigModel,
			modelID,
			companyID
			);
		return true;
		}
	Oscl::BT::Mesh::Model::Api*
	modelApi	= (isSigModel)?
					elementApi->findSigModel(modelID):
					elementApi->findVendorModel(companyID,modelID)
					;

	if(!modelApi){
		sendConfigModelPublicationStatus(
			srcAddress,
			Oscl::BT::Mesh::Message::Foundation::Status::invalidPublishParameters,
			elementAddress,
			publishAddress,
			pubAppKeyIndex,
			credentialFlag,
			publishTTL,
			publishPeriod,
			publishRetransmitCount,
			publishRetransmitIntervalSteps,
			isSigModel,
			modelID,
			companyID
			);
		return true;
		}

	Oscl::BT::Mesh::Publication::Api*
	publicationApi	= modelApi->getPublicationApi();

	if(!publicationApi){
		sendConfigModelPublicationStatus(
			srcAddress,
			Oscl::BT::Mesh::Message::Foundation::Status::invalidPublishParameters,
			elementAddress,
			publishAddress,
			pubAppKeyIndex,
			credentialFlag,
			publishTTL,
			publishPeriod,
			publishRetransmitCount,
			publishRetransmitIntervalSteps,
			isSigModel,
			modelID,
			companyID
			);
		return true;
		}

	bool
	failed	= publicationApi->setPublicationParameters(
				publishAddress,
				pubAppKeyIndex,
				credentialFlag,
				publishTTL,
				publishPeriod,
				publishRetransmitCount,
				publishRetransmitIntervalSteps
				);
	uint8_t
	statusCode	= failed?
					Oscl::BT::Mesh::Message::Foundation::Status::unspecifiedError:
					Oscl::BT::Mesh::Message::Foundation::Status::success
					;

	sendConfigModelPublicationStatus(
		srcAddress,
		statusCode,
		elementAddress,
		publishAddress,
		pubAppKeyIndex,
		credentialFlag,
		publishTTL,
		publishPeriod,
		publishRetransmitCount,
		publishRetransmitIntervalSteps,
		isSigModel,
		modelID,
		companyID
		);

	return true;
	}

bool	Part::rxConfigModelPublicationVirtualAddressSet(
			uint32_t	opcode,
			uint16_t	srcAddress,
			uint16_t	dstAddress,
			const void*	frame,
			unsigned	length,
			uint16_t	appKeyIndex
			) noexcept{

	if(opcode != Oscl::BT::Mesh::Model::OpCode::configModelPublicationVirtualAddressSet){
		return false;
		}

	Oscl::Endian::Decoder::Linear::Part
	leDecoder(
		frame,
		length
		);

	Oscl::Decoder::Api&
	decoder	= leDecoder.le();

	uint16_t	elementAddress;
	uint8_t		publishAddress[16];
	uint16_t	appKeyIndexAndCredentialFlag;
	uint8_t		publishTTL;
	uint8_t		publishPeriod;
	uint8_t		publishRetransmit;
	uint16_t	modelID;
	uint16_t	companyID;

	decoder.decode(elementAddress);
	decoder.copyOut(
		publishAddress,
		sizeof(publishAddress)
		);
	decoder.decode(appKeyIndexAndCredentialFlag);
	decoder.decode(publishTTL);
	decoder.decode(publishPeriod);
	decoder.decode(publishRetransmit);
	decoder.decode(modelID);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			OSCL_PRETTY_FUNCTION
			);
		return true;
		}

	decoder.decode(companyID);

	bool	isSigModel	= true;

	if(!decoder.underflow()){
		isSigModel	= false;
		}

	uint16_t
	pubAppKeyIndex	= appKeyIndexAndCredentialFlag & 0x0FFF;

	bool
	credentialFlag	= (appKeyIndexAndCredentialFlag & (1<<12))?true:false;

	uint8_t
	publishRetransmitCount	= (publishRetransmit & 0x07);

	uint8_t
	publishRetransmitIntervalSteps	= (publishRetransmit >> 3);


	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n"
		"\tElementAddress: 0x%4.4X\n"
		"\tAppKeyIndexAndCredentialFlag: 0x%4.4X\n"
		"\t\tAppKey Index: (0x%4.4X) %u\n"
		"\t\tCredential Flag: %s\n"
		"\tPublish TTL: 0x%2.2X\n"
		"\tPublish Period: %u\n"
		"\tPublish Retransmit: 0x%2.2X\n"
		"\t\tPublish Retransmit Count: %u\n"
		"\t\tPublish Retransmit Interval Steps: %u\n"
		"\tModel ID: 0x%4.4X\n"
		"\tisSigModel: %s\n"
		"\tCompany ID: 0x%4.4X [%s]\n"
		"\tPublish Address:\n"
		"",
		__PRETTY_FUNCTION__,
		elementAddress,
		appKeyIndexAndCredentialFlag,
		pubAppKeyIndex,
		pubAppKeyIndex,
		credentialFlag?"true":"false",
		publishTTL,
		publishPeriod,
		publishRetransmit,
		publishRetransmitCount,
		publishRetransmitIntervalSteps,
		modelID,
		isSigModel?"true":"false",
		companyID,
		isSigModel?"INVALID":"VALID"
		);
	Oscl::Error::Info::hexDump(
		publishAddress,
		sizeof(publishAddress)
		);
	#endif

	/*
		Virtual Address: 0b10xx xxxx xxxx xxxx

		SALT = s1 (“vtad”)
		hash = AES-CMACsalt (Label UUID) mod 2^14

	 */
	uint16_t
	publishAddressHash	= Oscl::BT::Mesh::Address::makeVirtualAddress(publishAddress);

	Oscl::BT::Mesh::Element::Api*
	elementApi	= _networkApi.findElement(elementAddress);

	if(!elementApi){
		sendConfigModelPublicationStatus(
			srcAddress,
			Oscl::BT::Mesh::Message::Foundation::Status::invalidAddress,
			elementAddress,
			publishAddressHash,
			pubAppKeyIndex,
			credentialFlag,
			publishTTL,
			publishPeriod,
			publishRetransmitCount,
			publishRetransmitIntervalSteps,
			isSigModel,
			modelID,
			companyID
			);
		return true;
		}
	Oscl::BT::Mesh::Model::Api*
	modelApi	= (isSigModel)?
					elementApi->findSigModel(modelID):
					elementApi->findVendorModel(companyID,modelID)
					;

	if(!modelApi){
		sendConfigModelPublicationStatus(
			srcAddress,
			Oscl::BT::Mesh::Message::Foundation::Status::invalidPublishParameters,
			elementAddress,
			publishAddressHash,
			pubAppKeyIndex,
			credentialFlag,
			publishTTL,
			publishPeriod,
			publishRetransmitCount,
			publishRetransmitIntervalSteps,
			isSigModel,
			modelID,
			companyID
			);
		return true;
		}

	Oscl::BT::Mesh::Publication::Api*
	publicationApi	= modelApi->getPublicationApi();

	if(!publicationApi){
		sendConfigModelPublicationStatus(
			srcAddress,
			Oscl::BT::Mesh::Message::Foundation::Status::invalidPublishParameters,
			elementAddress,
			publishAddressHash,
			pubAppKeyIndex,
			credentialFlag,
			publishTTL,
			publishPeriod,
			publishRetransmitCount,
			publishRetransmitIntervalSteps,
			isSigModel,
			modelID,
			companyID
			);
		return true;
		}

	bool
	failed	= publicationApi->setVirtualPulicationParameters(
				publishAddress,
				pubAppKeyIndex,
				credentialFlag,
				publishTTL,
				publishPeriod,
				publishRetransmitCount,
				publishRetransmitIntervalSteps
				);

	uint8_t
	statusCode	= failed?
					Oscl::BT::Mesh::Message::Foundation::Status::unspecifiedError:
					Oscl::BT::Mesh::Message::Foundation::Status::success
					;

	sendConfigModelPublicationStatus(
		srcAddress,
		statusCode,
		elementAddress,
		publishAddressHash,
		pubAppKeyIndex,
		credentialFlag,
		publishTTL,
		publishPeriod,
		publishRetransmitCount,
		publishRetransmitIntervalSteps,
		isSigModel,
		modelID,
		companyID
		);

	return true;
	}

bool	Part::rxConfigModelSubscriptionAdd(
			uint32_t	opcode,
			uint16_t	srcAddress,
			uint16_t	dstAddress,
			const void*	frame,
			unsigned	length,
			uint16_t	appKeyIndex
			) noexcept{

	if(opcode != Oscl::BT::Mesh::Model::OpCode::configModelSubscriptionAdd){
		return false;
		}

	Oscl::Endian::Decoder::Linear::Part
	leDecoder(
		frame,
		length
		);

	Oscl::Decoder::Api&
	decoder	= leDecoder.le();

	uint16_t	elementAddress;
	uint16_t	address;
	uint16_t	modelID;
	uint16_t	companyID;

	decoder.decode(elementAddress);
	decoder.decode(address);
	decoder.decode(modelID);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			OSCL_PRETTY_FUNCTION
			);
		return true;
		}

	decoder.decode(companyID);

	bool	isSigModel	= decoder.underflow()?true:false;

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n"
		"\tElementAddress: 0x%4.4X\n"
		"\tAddress: 0x%4.4X\n"
		"\tModel ID: 0x%4.4X\n"
		"\tisSigModel: %s\n"
		"\tCompany ID: 0x%4.4X [%s]\n"
		"",
		__PRETTY_FUNCTION__,
		elementAddress,
		address,
		modelID,
		isSigModel?"true":"false",
		companyID,
		isSigModel?"INVALID":"VALID"
		);
	#endif

	Oscl::BT::Mesh::Model::Api*
	modelApi	= findSubscriptionModelApi(
					srcAddress,
					elementAddress,
					address,
					isSigModel,
					modelID,
					companyID
					);

	if(!modelApi) {
		return true;
		}

	Oscl::BT::Mesh::Subscription::Api*
	subscriptionApi	= modelApi->getSubscriptionApi();

	bool failed	= true;

	if(subscriptionApi){
		failed	= subscriptionApi->addSubscription(
					address
					);
		}

	uint8_t
	statusCode	= failed?
					Oscl::BT::Mesh::Message::Foundation::Status::insufficientResources:
					Oscl::BT::Mesh::Message::Foundation::Status::success
					;

	sendConfigModelSubscriptionStatus(
		srcAddress,
		statusCode,
		elementAddress,
		address,
		isSigModel,
		modelID,
		companyID
		);
	return true;
	}

bool	Part::rxConfigModelSubscriptionDelete(
			uint32_t	opcode,
			uint16_t	srcAddress,
			uint16_t	dstAddress,
			const void*	frame,
			unsigned	length,
			uint16_t	appKeyIndex
			) noexcept{

	if(opcode != Oscl::BT::Mesh::Model::OpCode::configModelSubscriptionDelete){
		return false;
		}

	Oscl::Endian::Decoder::Linear::Part
	leDecoder(
		frame,
		length
		);

	Oscl::Decoder::Api&
	decoder	= leDecoder.le();

	uint16_t	elementAddress;
	uint16_t	address;
	uint16_t	modelID;
	uint16_t	companyID;

	decoder.decode(elementAddress);
	decoder.decode(address);
	decoder.decode(modelID);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			OSCL_PRETTY_FUNCTION
			);
		return true;
		}

	decoder.decode(companyID);

	bool	isSigModel	= decoder.underflow()?true:false;

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n"
		"\tElementAddress: 0x%4.4X\n"
		"\tAddress: 0x%4.4X\n"
		"\tModel ID: 0x%4.4X\n"
		"\tisSigModel: %s\n"
		"\tCompany ID: 0x%4.4X [%s]\n"
		"",
		__PRETTY_FUNCTION__,
		elementAddress,
		address,
		modelID,
		isSigModel?"true":"false",
		companyID,
		isSigModel?"INVALID":"VALID"
		);
	#endif

	Oscl::BT::Mesh::Model::Api*
	modelApi	= findSubscriptionModelApi(
					srcAddress,
					elementAddress,
					address,
					isSigModel,
					modelID,
					companyID
					);

	if(!modelApi) {
		return true;
		}

	Oscl::BT::Mesh::Subscription::Api*
	subscriptionApi	= modelApi->getSubscriptionApi();

	bool failed	= true;

	if(subscriptionApi){
		failed	= subscriptionApi->deleteSubscription(
					address
					);
		}

	uint8_t
	statusCode	= failed?
					Oscl::BT::Mesh::Message::Foundation::Status::insufficientResources:
					Oscl::BT::Mesh::Message::Foundation::Status::success
					;

	sendConfigModelSubscriptionStatus(
		srcAddress,
		statusCode,
		elementAddress,
		address,
		isSigModel,
		modelID,
		companyID
		);

	return true;
	}

bool	Part::rxConfigModelSubscriptionOverwrite(
			uint32_t	opcode,
			uint16_t	srcAddress,
			uint16_t	dstAddress,
			const void*	frame,
			unsigned	length,
			uint16_t	appKeyIndex
			) noexcept{

	if(opcode != Oscl::BT::Mesh::Model::OpCode::configModelSubscriptionOverwrite){
		return false;
		}

	Oscl::Endian::Decoder::Linear::Part
	leDecoder(
		frame,
		length
		);

	Oscl::Decoder::Api&
	decoder	= leDecoder.le();

	uint16_t	elementAddress;
	uint16_t	address;
	uint16_t	modelID;
	uint16_t	companyID;

	decoder.decode(elementAddress);
	decoder.decode(address);
	decoder.decode(modelID);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			OSCL_PRETTY_FUNCTION
			);
		return true;
		}

	decoder.decode(companyID);

	bool	isSigModel	= decoder.underflow()?true:false;

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n"
		"\tElementAddress: 0x%4.4X\n"
		"\tAddress: 0x%4.4X\n"
		"\tModel ID: 0x%4.4X\n"
		"\tisSigModel: %s\n"
		"\tCompany ID: 0x%4.4X [%s]\n"
		"",
		__PRETTY_FUNCTION__,
		elementAddress,
		address,
		modelID,
		isSigModel?"true":"false",
		companyID,
		isSigModel?"INVALID":"VALID"
		);
	#endif

	Oscl::BT::Mesh::Model::Api*
	modelApi	= findSubscriptionModelApi(
					srcAddress,
					elementAddress,
					address,
					isSigModel,
					modelID,
					companyID
					);

	if(!modelApi) {
		return true;
		}

	Oscl::BT::Mesh::Subscription::Api*
	subscriptionApi	= modelApi->getSubscriptionApi();

	bool failed	= true;

	if(subscriptionApi){
		failed	= subscriptionApi->overwriteSubscription(
					address
					);
		}

	uint8_t
	statusCode	= failed?
					Oscl::BT::Mesh::Message::Foundation::Status::insufficientResources:
					Oscl::BT::Mesh::Message::Foundation::Status::success
					;

	sendConfigModelSubscriptionStatus(
		srcAddress,
		statusCode,
		elementAddress,
		address,
		isSigModel,
		modelID,
		companyID
		);
	return true;
	}

bool	Part::rxConfigModelSubscriptionDeleteAll(
			uint32_t	opcode,
			uint16_t	srcAddress,
			uint16_t	dstAddress,
			const void*	frame,
			unsigned	length,
			uint16_t	appKeyIndex
			) noexcept{

	if(opcode != Oscl::BT::Mesh::Model::OpCode::configModelSubscriptionDeleteAll){
		return false;
		}

	Oscl::Endian::Decoder::Linear::Part
	leDecoder(
		frame,
		length
		);

	Oscl::Decoder::Api&
	decoder	= leDecoder.le();

	uint16_t	elementAddress;
	uint16_t	modelID;
	uint16_t	companyID;

	decoder.decode(elementAddress);
	decoder.decode(modelID);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			OSCL_PRETTY_FUNCTION
			);
		return true;
		}

	decoder.decode(companyID);

	bool	isSigModel	= decoder.underflow()?true:false;

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n"
		"\tElementAddress: 0x%4.4X\n"
		"\tModel ID: 0x%4.4X\n"
		"\tisSigModel: %s\n"
		"\tCompany ID: 0x%4.4X [%s]\n"
		"",
		__PRETTY_FUNCTION__,
		elementAddress,
		modelID,
		isSigModel?"true":"false",
		companyID,
		isSigModel?"INVALID":"VALID"
		);
	#endif

	Oscl::BT::Mesh::Model::Api*
	modelApi	= findSubscriptionModelApi(
					srcAddress,
					elementAddress,
					0x0000,
					isSigModel,
					modelID,
					companyID
					);

	if(!modelApi) {
		return true;
		}

	Oscl::BT::Mesh::Subscription::Api*
	subscriptionApi	= modelApi->getSubscriptionApi();

	bool failed	= true;

	if(subscriptionApi){
		failed	= subscriptionApi->deleteAllSubscriptions();
		}

	uint8_t
	statusCode	= failed?
					Oscl::BT::Mesh::Message::Foundation::Status::insufficientResources:
					Oscl::BT::Mesh::Message::Foundation::Status::success
					;

	sendConfigModelSubscriptionStatus(
		srcAddress,
		statusCode,
		elementAddress,
		0x0000,
		isSigModel,
		modelID,
		companyID
		);
	return true;
	}

bool	Part::rxConfigModelSubscriptionVirtualAddressAdd(
			uint32_t	opcode,
			uint16_t	srcAddress,
			uint16_t	dstAddress,
			const void*	frame,
			unsigned	length,
			uint16_t	appKeyIndex
			) noexcept{

	if(opcode != Oscl::BT::Mesh::Model::OpCode::configModelSubscriptionVirtualAddressAdd){
		return false;
		}

	Oscl::Endian::Decoder::Linear::Part
	leDecoder(
		frame,
		length
		);

	Oscl::Decoder::Api&
	decoder	= leDecoder.le();

	uint16_t	elementAddress;
	uint8_t		labelUUID[16];
	uint16_t	modelID;
	uint16_t	companyID;

	decoder.decode(elementAddress);
	decoder.copyOut(
		labelUUID,
		sizeof(labelUUID)
		);
	decoder.decode(modelID);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			OSCL_PRETTY_FUNCTION
			);
		return true;
		}

	decoder.decode(companyID);

	bool	isSigModel	= decoder.underflow()?true:false;

	uint16_t
	virtualAddress	= Oscl::BT::Mesh::Address::makeVirtualAddress(labelUUID);

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n"
		"\tElementAddress: 0x%4.4X\n"
		"\tVirtual  Address: 0x%4.4X\n"
		"\tModel ID: 0x%4.4X\n"
		"\tisSigModel: %s\n"
		"\tCompany ID: 0x%4.4X [%s]\n"
		"\tLabel UUID:\n"
		"",
		__PRETTY_FUNCTION__,
		elementAddress,
		virtualAddress,
		modelID,
		isSigModel?"true":"false",
		companyID,
		isSigModel?"INVALID":"VALID"
		);
	Oscl::Error::Info::hexDump(
		labelUUID,
		sizeof(labelUUID)
		);
	#endif

	Oscl::BT::Mesh::Model::Api*
	modelApi	= findSubscriptionModelApi(
					srcAddress,
					elementAddress,
					virtualAddress,
					isSigModel,
					modelID,
					companyID
					);

	if(!modelApi) {
		return true;
		}

	Oscl::BT::Mesh::Subscription::Api*
	subscriptionApi	= modelApi->getSubscriptionApi();

	bool failed	= true;

	if(subscriptionApi){
		failed	= subscriptionApi->addSubscription(
					virtualAddress,
					labelUUID
					);
		}

	uint8_t
	statusCode	= failed?
					Oscl::BT::Mesh::Message::Foundation::Status::insufficientResources:
					Oscl::BT::Mesh::Message::Foundation::Status::success
					;

	sendConfigModelSubscriptionStatus(
		srcAddress,
		statusCode,
		elementAddress,
		virtualAddress,
		isSigModel,
		modelID,
		companyID
		);
	return true;
	}

bool	Part::rxConfigModelSubscriptionVirtualAddressDelete(
			uint32_t	opcode,
			uint16_t	srcAddress,
			uint16_t	dstAddress,
			const void*	frame,
			unsigned	length,
			uint16_t	appKeyIndex
			) noexcept{

	if(opcode != Oscl::BT::Mesh::Model::OpCode::configModelSubscriptionVirtualAddressDelete){
		return false;
		}

	Oscl::Endian::Decoder::Linear::Part
	leDecoder(
		frame,
		length
		);

	Oscl::Decoder::Api&
	decoder	= leDecoder.le();

	uint16_t	elementAddress;
	uint8_t		labelUUID[16];
	uint16_t	modelID;
	uint16_t	companyID;

	decoder.decode(elementAddress);
	decoder.copyOut(
		labelUUID,
		sizeof(labelUUID)
		);
	decoder.decode(modelID);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			OSCL_PRETTY_FUNCTION
			);
		return true;
		}

	decoder.decode(companyID);

	bool	isSigModel	= decoder.underflow()?true:false;

	uint16_t
	virtualAddress	= Oscl::BT::Mesh::Address::makeVirtualAddress(labelUUID);

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n"
		"\tElementAddress: 0x%4.4X\n"
		"\tVirtual  Address: 0x%4.4X\n"
		"\tModel ID: 0x%4.4X\n"
		"\tisSigModel: %s\n"
		"\tCompany ID: 0x%4.4X [%s]\n"
		"\tLabel UUID:\n"
		"",
		__PRETTY_FUNCTION__,
		elementAddress,
		virtualAddress,
		modelID,
		isSigModel?"true":"false",
		companyID,
		isSigModel?"INVALID":"VALID"
		);
	Oscl::Error::Info::hexDump(
		labelUUID,
		sizeof(labelUUID)
		);
	#endif

	Oscl::BT::Mesh::Model::Api*
	modelApi	= findSubscriptionModelApi(
					srcAddress,
					elementAddress,
					virtualAddress,
					isSigModel,
					modelID,
					companyID
					);

	if(!modelApi) {
		return true;
		}

	Oscl::BT::Mesh::Subscription::Api*
	subscriptionApi	= modelApi->getSubscriptionApi();

	bool failed	= true;

	if(subscriptionApi){
		failed	= subscriptionApi->deleteSubscription(
					virtualAddress,
					labelUUID
					);
		}

	uint8_t
	statusCode	= failed?
					Oscl::BT::Mesh::Message::Foundation::Status::insufficientResources:
					Oscl::BT::Mesh::Message::Foundation::Status::success
					;

	sendConfigModelSubscriptionStatus(
		srcAddress,
		statusCode,
		elementAddress,
		virtualAddress,
		isSigModel,
		modelID,
		companyID
		);
	return true;
	}

bool	Part::rxConfigModelSubscriptionVirtualAddressOverwrite(
			uint32_t	opcode,
			uint16_t	srcAddress,
			uint16_t	dstAddress,
			const void*	frame,
			unsigned	length,
			uint16_t	appKeyIndex
			) noexcept{

	if(opcode != Oscl::BT::Mesh::Model::OpCode::configModelSubscriptionVirtualAddressOverwrite){
		return false;
		}

	Oscl::Endian::Decoder::Linear::Part
	leDecoder(
		frame,
		length
		);

	Oscl::Decoder::Api&
	decoder	= leDecoder.le();

	uint16_t	elementAddress;
	uint8_t		labelUUID[16];
	uint16_t	modelID;
	uint16_t	companyID;

	decoder.decode(elementAddress);
	decoder.copyOut(
		labelUUID,
		sizeof(labelUUID)
		);
	decoder.decode(modelID);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			OSCL_PRETTY_FUNCTION
			);
		return true;
		}

	decoder.decode(companyID);

	bool	isSigModel	= decoder.underflow()?true:false;

	uint16_t
	virtualAddress	= Oscl::BT::Mesh::Address::makeVirtualAddress(labelUUID);

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n"
		"\tElementAddress: 0x%4.4X\n"
		"\tVirtual  Address: 0x%4.4X\n"
		"\tModel ID: 0x%4.4X\n"
		"\tisSigModel: %s\n"
		"\tCompany ID: 0x%4.4X [%s]\n"
		"\tLabel UUID:\n"
		"",
		__PRETTY_FUNCTION__,
		elementAddress,
		virtualAddress,
		modelID,
		isSigModel?"true":"false",
		companyID,
		isSigModel?"INVALID":"VALID"
		);
	Oscl::Error::Info::hexDump(
		labelUUID,
		sizeof(labelUUID)
		);
	#endif

	Oscl::BT::Mesh::Model::Api*
	modelApi	= findSubscriptionModelApi(
					srcAddress,
					elementAddress,
					virtualAddress,
					isSigModel,
					modelID,
					companyID
					);

	if(!modelApi) {
		return true;
		}

	Oscl::BT::Mesh::Subscription::Api*
	subscriptionApi	= modelApi->getSubscriptionApi();

	bool failed	= true;

	if(subscriptionApi){
		failed	= subscriptionApi->overwriteSubscription(
					virtualAddress,
					labelUUID
					);
		}

	uint8_t
	statusCode	= failed?
					Oscl::BT::Mesh::Message::Foundation::Status::insufficientResources:
					Oscl::BT::Mesh::Message::Foundation::Status::success
					;

	sendConfigModelSubscriptionStatus(
		srcAddress,
		statusCode,
		elementAddress,
		virtualAddress,
		isSigModel,
		modelID,
		companyID
		);
	return true;
	}

bool	Part::rxConfigSigModelSubscriptionGet(
			uint32_t	opcode,
			uint16_t	srcAddress,
			uint16_t	dstAddress,
			const void*	frame,
			unsigned	length,
			uint16_t	appKeyIndex
			) noexcept{

	if(opcode != Oscl::BT::Mesh::Model::OpCode::configSigModelSubscriptionGet){
		return false;
		}

	Oscl::Endian::Decoder::Linear::Part
	leDecoder(
		frame,
		length
		);

	Oscl::Decoder::Api&
	decoder	= leDecoder.le();

	uint16_t	elementAddress;
	uint16_t	modelID;

	decoder.decode(elementAddress);
	decoder.decode(modelID);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			OSCL_PRETTY_FUNCTION
			);
		return true;
		}

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n"
		"\tElementAddress: 0x%4.4X\n"
		"\tModel ID: 0x%4.4X\n"
		"",
		__PRETTY_FUNCTION__,
		elementAddress,
		modelID
		);
	#endif

	Oscl::BT::Mesh::Element::Api*
	elementApi	= _networkApi.findElement(elementAddress);

	if(!elementApi){
		sendConfigSigModelSubscriptionList(
			srcAddress,
			Oscl::BT::Mesh::Message::Foundation::Status::invalidAddress,
			elementAddress,
			modelID
			);
		return true;
		}
	Oscl::BT::Mesh::Model::Api*
	modelApi	= elementApi->findSigModel(modelID);

	if(!modelApi){
		sendConfigSigModelSubscriptionList(
			srcAddress,
			Oscl::BT::Mesh::Message::Foundation::Status::invalidPublishParameters,
			elementAddress,
			modelID
			);
		return true;
		}

	Oscl::BT::Mesh::Subscription::Api*
	subscriptionApi	= modelApi->getSubscriptionApi();

	if(!subscriptionApi){
		sendConfigSigModelSubscriptionList(
			srcAddress,
			Oscl::BT::Mesh::Message::Foundation::Status::invalidPublishParameters,
			elementAddress,
			modelID
			);
		return true;
		}

	constexpr unsigned	maxAddresses	= 64;

	uint16_t	addressList[maxAddresses];

	unsigned
	nListItems	= subscriptionApi->getSubscriptionList(
					addressList,
					maxAddresses
					);

	sendConfigSigModelSubscriptionList(
		srcAddress,
		Oscl::BT::Mesh::Message::Foundation::Status::success,
		elementAddress,
		modelID,
		addressList,
		nListItems
		);

	return true;
	}

bool	Part::rxConfigVendorModelSubscriptionGet(
			uint32_t	opcode,
			uint16_t	srcAddress,
			uint16_t	dstAddress,
			const void*	frame,
			unsigned	length,
			uint16_t	appKeyIndex
			) noexcept{

	if(opcode != Oscl::BT::Mesh::Model::OpCode::configVendorModelSubscriptionGet){
		return false;
		}

	Oscl::Endian::Decoder::Linear::Part
	leDecoder(
		frame,
		length
		);

	Oscl::Decoder::Api&
	decoder	= leDecoder.le();

	uint16_t	elementAddress;
	uint16_t	modelID;
	uint16_t	companyID;

	decoder.decode(elementAddress);
	decoder.decode(modelID);
	decoder.decode(companyID);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			OSCL_PRETTY_FUNCTION
			);
		return true;
		}

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n"
		"\tElementAddress: 0x%4.4X\n"
		"\tModel ID: 0x%4.4X\n"
		"",
		__PRETTY_FUNCTION__,
		elementAddress,
		modelID
		);
	#endif

	Oscl::BT::Mesh::Element::Api*
	elementApi	= _networkApi.findElement(elementAddress);

	if(!elementApi){
		sendConfigVendorModelSubscriptionList(
			srcAddress,
			Oscl::BT::Mesh::Message::Foundation::Status::invalidAddress,
			elementAddress,
			modelID,
			companyID
			);
		return true;
		}
	Oscl::BT::Mesh::Model::Api*
	modelApi	= elementApi->findVendorModel(
					companyID,
					modelID
					);

	if(!modelApi){
		sendConfigVendorModelSubscriptionList(
			srcAddress,
			Oscl::BT::Mesh::Message::Foundation::Status::invalidPublishParameters,
			elementAddress,
			modelID,
			companyID
			);
		return true;
		}

	Oscl::BT::Mesh::Subscription::Api*
	subscriptionApi	= modelApi->getSubscriptionApi();

	if(!subscriptionApi){
		sendConfigVendorModelSubscriptionList(
			srcAddress,
			Oscl::BT::Mesh::Message::Foundation::Status::invalidPublishParameters,
			elementAddress,
			modelID,
			companyID
			);
		return true;
		}

	constexpr unsigned	maxAddresses	= 64;

	uint16_t	addressList[maxAddresses];

	unsigned
	nListItems	= subscriptionApi->getSubscriptionList(
					addressList,
					maxAddresses
					);

	sendConfigVendorModelSubscriptionList(
		srcAddress,
		Oscl::BT::Mesh::Message::Foundation::Status::success,
		elementAddress,
		modelID,
		companyID,
		addressList,
		nListItems
		);

	return true;
	}

Oscl::BT::Mesh::Model::Api*	Part::findSubscriptionModelApi(
								uint16_t	dst,
								uint16_t	elementAddress,
								uint16_t	address,
								bool		isSigModel,
								uint16_t	modelID,
								uint16_t	companyID
								) noexcept{

	Oscl::BT::Mesh::Element::Api*
	elementApi	= _networkApi.findElement(elementAddress);

	if(!elementApi){
		sendConfigModelSubscriptionStatus(
			dst,
			Oscl::BT::Mesh::Message::Foundation::Status::invalidAddress,
			elementAddress,
			address,
			isSigModel,
			modelID,
			companyID
			);
		return 0;
		}
	Oscl::BT::Mesh::Model::Api*
	modelApi	= (isSigModel)?
					elementApi->findSigModel(modelID):
					elementApi->findVendorModel(companyID,modelID)
					;

	if(!modelApi){
		sendConfigModelSubscriptionStatus(
			dst,
			Oscl::BT::Mesh::Message::Foundation::Status::invalidPublishParameters,
			elementAddress,
			address,
			isSigModel,
			modelID,
			companyID
			);
		return 0;
		}

	Oscl::BT::Mesh::Subscription::Api*
	subscriptionApi	= modelApi->getSubscriptionApi();

	if(!subscriptionApi){
		sendConfigModelSubscriptionStatus(
			dst,
			Oscl::BT::Mesh::Message::Foundation::Status::invalidPublishParameters,
			elementAddress,
			address,
			isSigModel,
			modelID,
			companyID
			);
		return 0;
		}

	return modelApi;
	}

Oscl::BT::Mesh::Network::Api*	Part::findNetwork(uint16_t index) noexcept{

	Oscl::BT::Mesh::Network::Iteration::Api&
	iterationApi	= _nodeApi.getNetworkIterationApi();

	for(
		Oscl::BT::Mesh::Network::Api*
		network	= iterationApi.first();
		network;
		network	= iterationApi.next(network)
		){
		if(network->getKeyIndex() == index){
			return network;
			}
		}

	return 0;
	}

bool	Part::rxConfigNetKeyAdd(
			uint32_t	opcode,
			uint16_t	srcAddress,
			uint16_t	dstAddress,
			const void*	frame,
			unsigned	length,
			uint16_t	appKeyIndex
			) noexcept{

	if(opcode != Oscl::BT::Mesh::Model::OpCode::configNetKeyAdd){
		// Not Config NetKey Add
		return false;
		}

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n"
		"",
		__PRETTY_FUNCTION__
		);
	Oscl::Error::Info::hexDump(
		frame,
		length
		);
	#endif

	Oscl::Endian::Decoder::Linear::Part
	leDecoder(
		frame,
		length
		);

	Oscl::Decoder::Api&
	decoder	= leDecoder.le();

	uint8_t		keyIndicie0;
	uint8_t		keyIndicie1;
	uint8_t		key[16];

	decoder.decode(keyIndicie0);
	decoder.decode(keyIndicie1);
	decoder.copyOut(
		key,
		sizeof(key)
		);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			OSCL_PRETTY_FUNCTION
			);
		return true;
		}

	/*	4.3.1.1 Key indexes

		To pack two key indexes into three octets, 8 LSbs of first
		key index value are packed into the first octet, placing the
		remaining 4 MSbs into 4 LSbs of the second octet.

		The first 4 LSbs of the second 12-bit key index are packed
		into the 4 MSbs of the second octet with the remaining 8
		MSbs into the third octet.

		**	NOTE! IGNORE Figure 4.3 and follow the bizzare
			description above!
	 */

	uint16_t
	keyId1	= keyIndicie1 & 0x0F;	// 4 MSBs
	keyId1	<<= 8;
	keyId1	|= keyIndicie0;		// 8 LSBs

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n"
		"\tNetKey Index: 0x%4.4X\n"
		"\tKey:\n"
		"",
		__PRETTY_FUNCTION__,
		keyId1
		);
	Oscl::Error::Info::hexDump(
		key,
		sizeof(key)
		);
	#endif

	bool
	failed	= _nodeApi.addNetworkKey(
				keyId1,
				key
				);

	uint8_t
	status	= failed?
				Oscl::BT::Mesh::Message::Foundation::Status::insufficientResources:
				Oscl::BT::Mesh::Message::Foundation::Status::success
				;

	sendConfigNetKeyStatus(
		status,
		keyIndicie0,
		keyIndicie1,
		srcAddress
		);

	return true;
	}

bool	Part::rxConfigNetKeyUpdate(
			uint32_t	opcode,
			uint16_t	srcAddress,
			uint16_t	dstAddress,
			const void*	frame,
			unsigned	length,
			uint16_t	appKeyIndex
			) noexcept{

	if(opcode != Oscl::BT::Mesh::Model::OpCode::configNetKeyUpdate){
		// Not Config NetKey Update
		return false;
		}

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n"
		"",
		__PRETTY_FUNCTION__
		);
	Oscl::Error::Info::hexDump(
		frame,
		length
		);
	#endif

	Oscl::Endian::Decoder::Linear::Part
	leDecoder(
		frame,
		length
		);

	Oscl::Decoder::Api&
	decoder	= leDecoder.le();

	uint8_t		keyIndicie0;
	uint8_t		keyIndicie1;
	uint8_t		key[16];

	decoder.decode(keyIndicie0);
	decoder.decode(keyIndicie1);
	decoder.copyOut(
		key,
		sizeof(key)
		);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			OSCL_PRETTY_FUNCTION
			);
		return true;
		}

	uint16_t
	keyId1	= keyIndicie1 & 0x0F;	// 4 MSBs
	keyId1	<<= 8;
	keyId1	|= keyIndicie0;		// 8 LSBs

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n"
		"\tNetKey Index: 0x%4.4X\n"
		"\tKey:\n"
		"",
		__PRETTY_FUNCTION__,
		keyId1
		);
	Oscl::Error::Info::hexDump(
		key,
		sizeof(key)
		);
	#endif

	Oscl::BT::Mesh::Network::Api*
	network	= findNetwork(keyId1);

	if(!network){
		sendConfigNetKeyStatus(
			Oscl::BT::Mesh::Message::Foundation::Status::invalidNetKeyIndex,
			keyIndicie0,
			keyIndicie1,
			srcAddress
			);
		return true;
		}

	bool
	failed	= network->updateKey(key);

	uint8_t
	status	= failed?
				Oscl::BT::Mesh::Message::Foundation::Status::insufficientResources:
				Oscl::BT::Mesh::Message::Foundation::Status::success
				;

	sendConfigNetKeyStatus(
		status,
		keyIndicie0,
		keyIndicie1,
		srcAddress
		);

	return true;
	}

bool	Part::rxConfigNetKeyDelete(
			uint32_t	opcode,
			uint16_t	srcAddress,
			uint16_t	dstAddress,
			const void*	frame,
			unsigned	length,
			uint16_t	appKeyIndex
			) noexcept{

	if(opcode != Oscl::BT::Mesh::Model::OpCode::configNetKeyDelete){
		// Not Config NetKey Delete
		return false;
		}

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n"
		"",
		__PRETTY_FUNCTION__
		);
	Oscl::Error::Info::hexDump(
		frame,
		length
		);
	#endif

	Oscl::Endian::Decoder::Linear::Part
	leDecoder(
		frame,
		length
		);

	Oscl::Decoder::Api&
	decoder	= leDecoder.le();

	uint8_t	keyIndicie0;
	uint8_t	keyIndicie1;

	decoder.decode(keyIndicie0);
	decoder.decode(keyIndicie1);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			OSCL_PRETTY_FUNCTION
			);
		return true;
		}

	uint16_t
	keyId1	= keyIndicie1 & 0x0F;	// 4 MSBs
	keyId1	<<= 8;
	keyId1	|= keyIndicie0;		// 8 LSBs

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n"
		"\tNetKey Index: 0x%4.4X\n"
		"",
		__PRETTY_FUNCTION__,
		keyId1
		);
	#endif

	Oscl::BT::Mesh::Network::Api*
	network	= findNetwork(keyId1);

	if(!network){
		sendConfigNetKeyStatus(
			Oscl::BT::Mesh::Message::Foundation::Status::invalidNetKeyIndex,
			keyIndicie0,
			keyIndicie1,
			srcAddress
			);
		return true;
		}

	bool
	failed	= network->deleteNetwork();

	uint8_t
	status	= failed?
				Oscl::BT::Mesh::Message::Foundation::Status::unspecifiedError:
				Oscl::BT::Mesh::Message::Foundation::Status::success
				;

	sendConfigNetKeyStatus(
		status,
		keyIndicie0,
		keyIndicie1,
		srcAddress
		);

	return true;
	}

bool	Part::rxConfigNetKeyGet(
			uint32_t	opcode,
			uint16_t	srcAddress,
			uint16_t	dstAddress,
			const void*	frame,
			unsigned	length,
			uint16_t	appKeyIndex
			) noexcept{

	if(opcode != Oscl::BT::Mesh::Model::OpCode::configNetKeyGet){
		// Not Config NetKey Get
		return false;
		}

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n"
		"",
		__PRETTY_FUNCTION__
		);
	Oscl::Error::Info::hexDump(
		frame,
		length
		);
	#endif

	Oscl::BT::Mesh::Network::Iteration::Api&
	iterationApi	= _nodeApi.getNetworkIterationApi();

	constexpr unsigned	maxNetKeyIndicies	= 20;

	uint16_t	netKeyIndicies[maxNetKeyIndicies];

	unsigned	i	= 0;

	for(
		Oscl::BT::Mesh::Network::Api*
		network	= iterationApi.first();
		network && (i<maxNetKeyIndicies);
		network	= iterationApi.next(network),++i
		){
		netKeyIndicies[i]	= network->getKeyIndex();
		}

	sendConfigNetKeyList(
		srcAddress,
		netKeyIndicies,
		i
		);

	return true;
	}

bool	Part::rxConfigAppKeyAdd(
			uint32_t	opcode,
			uint16_t	srcAddress,
			uint16_t	dstAddress,
			const void*	frame,
			unsigned	length,
			uint16_t	appKeyIndex
			) noexcept{

	if(opcode != Oscl::BT::Mesh::Model::OpCode::configAppKeyAdd){
		// Not Config AppKey Add
		return false;
		}

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n"
		"",
		__PRETTY_FUNCTION__
		);
	Oscl::Error::Info::hexDump(
		frame,
		length
		);
	#endif

	Oscl::Endian::Decoder::Linear::Part
	leDecoder(
		frame,
		length
		);

	Oscl::Decoder::Api&
	decoder	= leDecoder.le();

	uint8_t		key[16];

	decoder.decode(_keyIndicie0);
	decoder.decode(_keyIndicie1);
	decoder.decode(_keyIndicie2);
	decoder.copyOut(
		key,
		sizeof(key)
		);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			OSCL_PRETTY_FUNCTION
			);
		return true;
		}

	/*	4.3.1.1 Key indexes

		To pack two key indexes into three octets, 8 LSbs of first
		key index value are packed into the first octet, placing the
		remaining 4 MSbs into 4 LSbs of the second octet.

		The first 4 LSbs of the second 12-bit key index are packed
		into the 4 MSbs of the second octet with the remaining 8
		MSbs into the third octet.

		**	NOTE! IGNORE Figure 4.3 and follow the bizzare
			description above!
	 */

	uint16_t
	keyId1	= _keyIndicie1 & 0x0F;	// 4 MSBs
	keyId1	<<= 8;
	keyId1	|= _keyIndicie0;		// 8 LSBs

	uint16_t
	keyId2	= _keyIndicie2;	// 8 MSBs
	keyId2	<<= 4;
	keyId2	|= (_keyIndicie1 >> 4);	// 4 LSBs

	/*	4.3.2.37 Config AppKey Add

		The NetKeyIndexAndAppKeyIndex field contains two indexes
		that shall identify the global NetKey Index of the NetKey
		and the global AppKey Index of the AppKey. These two indexes
		shall be encoded as defined in Section 4.3.1.1 using NetKey
		Index as first key index and AppKey Index as second key index.

	 */
	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n"
		"\tNetKey Index: 0x%4.4X\n"
		"\tAppKey Index: 0x%4.4X\n"
		"\tKey:\n"
		"",
		__PRETTY_FUNCTION__,
		keyId1,
		keyId2
		);
	Oscl::Error::Info::hexDump(
		key,
		sizeof(key)
		);
	#endif

	/* Config AppKey ADD
		3 octets:	Index of the NetKey and index of the AppKey
		16 octets:	AppKey value
	 */

	Oscl::BT::Mesh::Network::Api*
	network	= findNetwork(keyId1);

	if(!network){
		sendConfigAppKeyStatus(
			Oscl::BT::Mesh::Message::Foundation::Status::invalidNetKeyIndex,
			_keyIndicie0,
			_keyIndicie1,
			_keyIndicie2,
			srcAddress
			);
		return true;
		}

	Oscl::BT::Mesh::Key::Api&
	appKeyApi	= network->getAppKeyApi();

	const Oscl::BT::Mesh::Key::Result::Var::State*
	result	= appKeyApi.add(
				keyId2,
				key
				);

	if(result){
		_srcAddress	= srcAddress;
		result->accept(_configAppKeyAddResult);
		return true;
		}

	sendConfigAppKeyStatus(
		Oscl::BT::Mesh::Message::Foundation::Status::success,
		_keyIndicie0,
		_keyIndicie1,
		_keyIndicie2,
		srcAddress
		);

	return true;
	}

bool	Part::rxConfigAppKeyUpdate(
			uint32_t	opcode,
			uint16_t	srcAddress,
			uint16_t	dstAddress,
			const void*	frame,
			unsigned	length,
			uint16_t	appKeyIndex
			) noexcept{

	if(opcode != Oscl::BT::Mesh::Model::OpCode::configAppKeyUpdate){
		// Not Config AppKey Update
		return false;
		}

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n"
		"",
		__PRETTY_FUNCTION__
		);
	Oscl::Error::Info::hexDump(
		frame,
		length
		);
	#endif

	Oscl::Endian::Decoder::Linear::Part
	leDecoder(
		frame,
		length
		);

	Oscl::Decoder::Api&
	decoder	= leDecoder.le();

	uint8_t		key[16];

	decoder.decode(_keyIndicie0);
	decoder.decode(_keyIndicie1);
	decoder.decode(_keyIndicie2);
	decoder.copyOut(
		key,
		sizeof(key)
		);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			OSCL_PRETTY_FUNCTION
			);
		return true;
		}

	/*	4.3.1.1 Key indexes

		To pack two key indexes into three octets, 8 LSbs of first
		key index value are packed into the first octet, placing the
		remaining 4 MSbs into 4 LSbs of the second octet.

		The first 4 LSbs of the second 12-bit key index are packed
		into the 4 MSbs of the second octet with the remaining 8
		MSbs into the third octet.

		**	NOTE! IGNORE Figure 4.3 and follow the bizzare
			description above!
	 */

	uint16_t
	keyId1	= _keyIndicie1 & 0x0F;	// 4 MSBs
	keyId1	<<= 8;
	keyId1	|= _keyIndicie0;		// 8 LSBs

	uint16_t
	keyId2	= _keyIndicie2;	// 8 MSBs
	keyId2	<<= 4;
	keyId2	|= (_keyIndicie1 >> 4);	// 4 LSBs

	/*	4.3.2.37 Config AppKey Add

		The NetKeyIndexAndAppKeyIndex field contains two indexes
		that shall identify the global NetKey Index of the NetKey
		and the global AppKey Index of the AppKey. These two indexes
		shall be encoded as defined in Section 4.3.1.1 using NetKey
		Index as first key index and AppKey Index as second key index.

	 */
	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n"
		"\tNetKey Index: 0x%4.4X\n"
		"\tAppKey Index: 0x%4.4X\n"
		"\tKey:\n"
		"",
		__PRETTY_FUNCTION__,
		keyId1,
		keyId2
		);
	Oscl::Error::Info::hexDump(
		key,
		sizeof(key)
		);
	#endif

	/* Config AppKey ADD
		3 octets:	Index of the NetKey and index of the AppKey
		16 octets:	AppKey value
	 */

	Oscl::BT::Mesh::Network::Api*
	network	= findNetwork(keyId1);

	if(!network){
		sendConfigAppKeyStatus(
			Oscl::BT::Mesh::Message::Foundation::Status::invalidNetKeyIndex,
			_keyIndicie0,
			_keyIndicie1,
			_keyIndicie2,
			srcAddress
			);
		return true;
		}

	Oscl::BT::Mesh::Key::Api&
	appKeyApi	= network->getAppKeyApi();

	const Oscl::BT::Mesh::Key::Result::Var::State*
	result	= appKeyApi.update(
				keyId2,
				key
				);

	if(result){
		_srcAddress	= srcAddress;
		result->accept(_configAppKeyUpdateResult);
		return true;
		}

	sendConfigAppKeyStatus(
		Oscl::BT::Mesh::Message::Foundation::Status::success,
		_keyIndicie0,
		_keyIndicie1,
		_keyIndicie2,
		srcAddress
		);

	return true;
	}

bool	Part::rxConfigAppKeyDelete(
			uint32_t	opcode,
			uint16_t	srcAddress,
			uint16_t	dstAddress,
			const void*	frame,
			unsigned	length,
			uint16_t	appKeyIndex
			) noexcept{

	if(opcode != Oscl::BT::Mesh::Model::OpCode::configAppKeyDelete){
		// Not Config AppKey Delete
		return false;
		}

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n"
		"",
		__PRETTY_FUNCTION__
		);
	Oscl::Error::Info::hexDump(
		frame,
		length
		);
	#endif

	Oscl::Endian::Decoder::Linear::Part
	leDecoder(
		frame,
		length
		);

	Oscl::Decoder::Api&
	decoder	= leDecoder.le();

	decoder.decode(_keyIndicie0);
	decoder.decode(_keyIndicie1);
	decoder.decode(_keyIndicie2);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			OSCL_PRETTY_FUNCTION
			);
		return true;
		}

	/*	4.3.1.1 Key indexes

		To pack two key indexes into three octets, 8 LSbs of first
		key index value are packed into the first octet, placing the
		remaining 4 MSbs into 4 LSbs of the second octet.

		The first 4 LSbs of the second 12-bit key index are packed
		into the 4 MSbs of the second octet with the remaining 8
		MSbs into the third octet.

		**	NOTE! IGNORE Figure 4.3 and follow the bizzare
			description above!
	 */

	uint16_t
	keyId1	= _keyIndicie1 & 0x0F;	// 4 MSBs
	keyId1	<<= 8;
	keyId1	|= _keyIndicie0;		// 8 LSBs

	uint16_t
	keyId2	= _keyIndicie2;	// 8 MSBs
	keyId2	<<= 4;
	keyId2	|= (_keyIndicie1 >> 4);	// 4 LSBs

	/*	4.3.2.37 Config AppKey Add

		The NetKeyIndexAndAppKeyIndex field contains two indexes
		that shall identify the global NetKey Index of the NetKey
		and the global AppKey Index of the AppKey. These two indexes
		shall be encoded as defined in Section 4.3.1.1 using NetKey
		Index as first key index and AppKey Index as second key index.

	 */
	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n"
		"\tNetKey Index: 0x%4.4X\n"
		"\tAppKey Index: 0x%4.4X\n"
		"\tKey:\n"
		"",
		__PRETTY_FUNCTION__,
		keyId1,
		keyId2
		);
	#endif

	/* Config AppKey ADD
		3 octets:	Index of the NetKey and index of the AppKey
		16 octets:	AppKey value
	 */

	Oscl::BT::Mesh::Network::Api*
	network	= findNetwork(keyId1);

	if(!network){
		sendConfigAppKeyStatus(
			Oscl::BT::Mesh::Message::Foundation::Status::invalidNetKeyIndex,
			_keyIndicie0,
			_keyIndicie1,
			_keyIndicie2,
			srcAddress
			);
		return true;
		}

	Oscl::BT::Mesh::Key::Api&
	appKeyApi	= network->getAppKeyApi();

	const Oscl::BT::Mesh::Key::Result::Var::State*
	result	= appKeyApi.remove(
				keyId2
				);

	if(result){
		_srcAddress	= srcAddress;
		result->accept(_configAppKeyDeleteResult);
		return true;
		}

	sendConfigAppKeyStatus(
		Oscl::BT::Mesh::Message::Foundation::Status::success,
		_keyIndicie0,
		_keyIndicie1,
		_keyIndicie2,
		srcAddress
		);

	return true;
	}

bool	Part::rxConfigAppKeyGet(
			uint32_t	opcode,
			uint16_t	srcAddress,
			uint16_t	dstAddress,
			const void*	frame,
			unsigned	length,
			uint16_t	appKeyIndex
			) noexcept{

	if(opcode != Oscl::BT::Mesh::Model::OpCode::configAppKeyGet){
		// Not Config AppKey Get
		return false;
		}

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n"
		"",
		__PRETTY_FUNCTION__
		);
	Oscl::Error::Info::hexDump(
		frame,
		length
		);
	#endif

	Oscl::Endian::Decoder::Linear::Part
	leDecoder(
		frame,
		length
		);

	Oscl::Decoder::Api&
	decoder	= leDecoder.le();

	decoder.decode(_keyIndicie0);
	decoder.decode(_keyIndicie1);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			OSCL_PRETTY_FUNCTION
			);
		return true;
		}

	/*	4.3.1.1 Key indexes

		To pack two key indexes into three octets, 8 LSbs of first
		key index value are packed into the first octet, placing the
		remaining 4 MSbs into 4 LSbs of the second octet.

		The first 4 LSbs of the second 12-bit key index are packed
		into the 4 MSbs of the second octet with the remaining 8
		MSbs into the third octet.

		**	NOTE! IGNORE Figure 4.3 and follow the bizzare
			description above!
	 */

	uint16_t
	keyId1	= _keyIndicie1 & 0x0F;	// 4 MSBs
	keyId1	<<= 8;
	keyId1	|= _keyIndicie0;		// 8 LSBs

	/*	4.3.2.37 Config AppKey Add

		The NetKeyIndexAndAppKeyIndex field contains two indexes
		that shall identify the global NetKey Index of the NetKey
		and the global AppKey Index of the AppKey. These two indexes
		shall be encoded as defined in Section 4.3.1.1 using NetKey
		Index as first key index and AppKey Index as second key index.

	 */
	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n"
		"\tNetKey Index: 0x%4.4X\n"
		"\tKey:\n"
		"",
		__PRETTY_FUNCTION__,
		keyId1
		);
	#endif

	/* Config AppKey ADD
		3 octets:	Index of the NetKey and index of the AppKey
		16 octets:	AppKey value
	 */

	Oscl::BT::Mesh::Network::Api*
	network	= findNetwork(keyId1);

	if(!network){
		sendConfigAppKeyList(
			Oscl::BT::Mesh::Message::Foundation::Status::invalidNetKeyIndex,
			_keyIndicie0,
			_keyIndicie1,
			srcAddress
			);
		return true;
		}

	Oscl::BT::Mesh::Key::Api&
	appKeyApi	= network->getAppKeyApi();

	_nAppKeys		= 0;
	_resultState	= 0;

	Oscl::BT::Mesh::Key::
	Iterator::Composer<Part>
		iterator(
			*this,
			&Part::configAppKeyGetItem
			);

	appKeyApi.iterateTX(
		iterator
		);

	if(_resultState){
		_srcAddress	= srcAddress;
		_resultState->accept(_configAppKeyGetResult);
		return true;
		}

	sendConfigAppKeyList(
		Oscl::BT::Mesh::Message::Foundation::Status::success,
		_keyIndicie0,
		_keyIndicie1,
		srcAddress
		);

	return true;
	}

bool	Part::rxConfigNodeIdentityGet(
			uint32_t	opcode,
			uint16_t	srcAddress,
			uint16_t	dstAddress,
			const void*	frame,
			unsigned	length,
			uint16_t	appKeyIndex
			) noexcept{

	if(opcode != Oscl::BT::Mesh::Model::OpCode::configNodeIdentityGet){
		// Not Config AppKey Get
		return false;
		}

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n"
		"",
		__PRETTY_FUNCTION__
		);
	Oscl::Error::Info::hexDump(
		frame,
		length
		);
	#endif

	Oscl::Endian::Decoder::Linear::Part
	leDecoder(
		frame,
		length
		);

	Oscl::Decoder::Api&
	decoder	= leDecoder.le();

	uint8_t	keyIndicie0;
	uint8_t	keyIndicie1;

	decoder.decode(keyIndicie0);
	decoder.decode(keyIndicie1);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			OSCL_PRETTY_FUNCTION
			);
		return true;
		}

	/*	4.3.1.1 Key indexes

		To pack two key indexes into three octets, 8 LSbs of first
		key index value are packed into the first octet, placing the
		remaining 4 MSbs into 4 LSbs of the second octet.

		The first 4 LSbs of the second 12-bit key index are packed
		into the 4 MSbs of the second octet with the remaining 8
		MSbs into the third octet.

		**	NOTE! IGNORE Figure 4.3 and follow the bizzare
			description above!
	 */

	uint16_t
	keyId1	= keyIndicie1 & 0x0F;	// 4 MSBs
	keyId1	<<= 8;
	keyId1	|= keyIndicie0;		// 8 LSBs

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n"
		"\tNetKey Index: 0x%4.4X\n"
		"\tKey:\n"
		"",
		__PRETTY_FUNCTION__,
		keyId1
		);
	#endif

	Oscl::BT::Mesh::Network::Api*
	network	= findNetwork(keyId1);

	if(!network){
		sendNodeIdentityStatus(
			Oscl::BT::Mesh::Message::Foundation::Status::invalidNetKeyIndex,
			keyIndicie0,
			keyIndicie1,
			srcAddress,
			0x00
			);
		return true;
		}

	uint8_t
	state	= network->getNodeIdentityState();

	sendNodeIdentityStatus(
		Oscl::BT::Mesh::Message::Foundation::Status::success,
		keyIndicie0,
		keyIndicie1,
		srcAddress,
		state
		);

	return true;
	}

bool	Part::rxConfigNodeIdentitySet(
			uint32_t	opcode,
			uint16_t	srcAddress,
			uint16_t	dstAddress,
			const void*	frame,
			unsigned	length,
			uint16_t	appKeyIndex
			) noexcept{

	if(opcode != Oscl::BT::Mesh::Model::OpCode::configNodeIdentitySet){
		// Not Config AppKey Set
		return false;
		}

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n"
		"",
		__PRETTY_FUNCTION__
		);
	Oscl::Error::Info::hexDump(
		frame,
		length
		);
	#endif

	Oscl::Endian::Decoder::Linear::Part
	leDecoder(
		frame,
		length
		);

	Oscl::Decoder::Api&
	decoder	= leDecoder.le();

	uint8_t	keyIndicie0;
	uint8_t	keyIndicie1;
	uint8_t	state;

	decoder.decode(keyIndicie0);
	decoder.decode(keyIndicie1);
	decoder.decode(state);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			OSCL_PRETTY_FUNCTION
			);
		return true;
		}

	/*	4.3.1.1 Key indexes

		To pack two key indexes into three octets, 8 LSbs of first
		key index value are packed into the first octet, placing the
		remaining 4 MSbs into 4 LSbs of the second octet.

		The first 4 LSbs of the second 12-bit key index are packed
		into the 4 MSbs of the second octet with the remaining 8
		MSbs into the third octet.

		**	NOTE! IGNORE Figure 4.3 and follow the bizzare
			description above!
	 */

	uint16_t
	keyId1	= keyIndicie1 & 0x0F;	// 4 MSBs
	keyId1	<<= 8;
	keyId1	|= keyIndicie0;		// 8 LSBs

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n"
		"\tNetKey Index: 0x%4.4X\n"
		"\tKey:\n"
		"",
		__PRETTY_FUNCTION__,
		keyId1
		);
	#endif

	Oscl::BT::Mesh::Network::Api*
	network	= findNetwork(keyId1);

	if(!network){
		sendNodeIdentityStatus(
			Oscl::BT::Mesh::Message::Foundation::Status::invalidNetKeyIndex,
			keyIndicie0,
			keyIndicie1,
			srcAddress,
			0x00
			);
		return true;
		}

	network->setNodeIdentityState(state);

	state	= network->getNodeIdentityState();

	sendNodeIdentityStatus(
		Oscl::BT::Mesh::Message::Foundation::Status::success,
		keyIndicie0,
		keyIndicie1,
		srcAddress,
		state
		);

	return true;
	}

bool	Part::rxConfigModelAppBind(
			uint32_t	opcode,
			uint16_t	srcAddress,
			uint16_t	dstAddress,
			const void*	frame,
			unsigned	length,
			uint16_t	appKeyIndex
			) noexcept{

	if(opcode != Oscl::BT::Mesh::Model::OpCode::configModelAppBind){
		// Not Config Model App Bind
		return false;
		}

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n"
		"",
		__PRETTY_FUNCTION__
		);
	Oscl::Error::Info::hexDump(
		frame,
		length
		);
	#endif

	Oscl::Endian::Decoder::Linear::Part
	leDecoder(
		frame,
		length
		);

	Oscl::Decoder::Api&
	decoder	= leDecoder.le();

	decoder.decode(_elementAddr);
	decoder.decode(_keyIndicie0);
	decoder.decode(_keyIndicie1);
	decoder.decode(_companyIdentifier);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			OSCL_PRETTY_FUNCTION
			);
		return true;
		}

	decoder.decode(_modelIdentifier);

	_sigModelID	= (decoder.underflow())?true:false;

	if(_sigModelID){
		_modelIdentifier	= _companyIdentifier;
		}

	/*	4.3.1.1 Key indexes

		To pack two key indexes into three octets, 8 LSbs of first
		key index value are packed into the first octet, placing the
		remaining 4 MSbs into 4 LSbs of the second octet.

		The first 4 LSbs of the second 12-bit key index are packed
		into the 4 MSbs of the second octet with the remaining 8
		MSbs into the third octet.

		**	NOTE! IGNORE Figure 4.3 and follow the bizzare
			description above!
	 */

	_appKeyIndex	= _keyIndicie1 & 0x0F;	// 4 MSBs
	_appKeyIndex	<<= 8;
	_appKeyIndex	|= _keyIndicie0;		// 8 LSBs

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n"
		"\tElement Address: 0x%4.4X\n"
		"\tAppKey Index: 0x%4.4X\n"
		"\tisSigModelID: %s\n"
		"\tCompany ID: 0x%4.4X\n"
		"\tModel ID: 0x%4.4X\n"
		"",
		__PRETTY_FUNCTION__,
		_elementAddr,
		_appKeyIndex,
		_sigModelID?"true":"false",
		_companyIdentifier,
		_modelIdentifier
		);
	#endif

	_srcAddress	= srcAddress;

	Oscl::BT::Mesh::Element::Api*
	element	= _networkApi.findElement(_elementAddr);

	if(!element){
		Oscl::Error::Info::log(
			"%s: No Such Element (0x%4.4X)\n",
			OSCL_PRETTY_FUNCTION,
			_elementAddr
			);
		sendConfigModelAppStatus(
			Oscl::BT::Mesh::Message::Foundation::Status::invalidAddress
			);
		return true;
		}

	Oscl::BT::Mesh::Key::Api&
	appKeyApi	= _networkApi.getAppKeyApi();

	Oscl::BT::Mesh::Key::
	Iterator::Composer<Part>
		iterator(
			*this,
			&Part::configModelAppBindItem
			);

	bool
	found	= appKeyApi.iterateTX(
				iterator
				);

	if(!found){
		sendConfigModelAppStatus(
			Oscl::BT::Mesh::Message::Foundation::Status::invalidAppKeyIndex
			);
		return true;
		}

	if(_sigModelID){
		Oscl::BT::Mesh::Model::Api*
		model	= element->findSigModel(_modelIdentifier);
		if(model){
			if(!model->modelSupportsAppKeyBinding()){
				sendConfigModelAppStatus(
					Oscl::BT::Mesh::Message::Foundation::Status::cannotBind
					);
				}
			const Oscl::BT::Mesh::Key::Result::Var::State&
			result	= model->bind(_appKeyIndex);
			result.accept(_configModelAppBindResult);
			return true;
			}
		}
	else {
		Oscl::BT::Mesh::Model::Api*
		model	= element->findVendorModel(_companyIdentifier,_modelIdentifier);
		if(model){
			const Oscl::BT::Mesh::Key::Result::Var::State&
			result	= model->bind(_appKeyIndex);
			result.accept(_configModelAppBindResult);
			return true;
			}
		}

	// Failed to find model.
	sendConfigModelAppStatus(
		Oscl::BT::Mesh::Message::Foundation::Status::invalidModel
		);
	return true;
	}

bool	Part::rxConfigModelAppUnbind(
			uint32_t	opcode,
			uint16_t	srcAddress,
			uint16_t	dstAddress,
			const void*	frame,
			unsigned	length,
			uint16_t	appKeyIndex
			) noexcept{

	if(opcode != Oscl::BT::Mesh::Model::OpCode::configModelAppUnbind){
		// Not Config Model App Unbind
		return false;
		}

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n"
		"",
		__PRETTY_FUNCTION__
		);
	Oscl::Error::Info::hexDump(
		frame,
		length
		);
	#endif

	Oscl::Endian::Decoder::Linear::Part
	leDecoder(
		frame,
		length
		);

	Oscl::Decoder::Api&
	decoder	= leDecoder.le();

	decoder.decode(_elementAddr);
	decoder.decode(_keyIndicie0);
	decoder.decode(_keyIndicie1);
	decoder.decode(_companyIdentifier);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			OSCL_PRETTY_FUNCTION
			);
		return true;
		}

	decoder.decode(_modelIdentifier);

	_sigModelID	= (decoder.underflow())?true:false;

	if(_sigModelID){
		_modelIdentifier	= _companyIdentifier;
		}

	/*	4.3.1.1 Key indexes

		To pack two key indexes into three octets, 8 LSbs of first
		key index value are packed into the first octet, placing the
		remaining 4 MSbs into 4 LSbs of the second octet.

		The first 4 LSbs of the second 12-bit key index are packed
		into the 4 MSbs of the second octet with the remaining 8
		MSbs into the third octet.

		**	NOTE! IGNORE Figure 4.3 and follow the bizzare
			description above!
	 */

	_appKeyIndex	= _keyIndicie1 & 0x0F;	// 4 MSBs
	_appKeyIndex	<<= 8;
	_appKeyIndex	|= _keyIndicie0;		// 8 LSBs

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n"
		"\tElement Address: 0x%4.4X\n"
		"\tAppKey Index: 0x%4.4X\n"
		"\tisSigModelID: %s\n"
		"\tCompany ID: 0x%4.4X\n"
		"\tModel ID: 0x%4.4X\n"
		"",
		__PRETTY_FUNCTION__,
		_elementAddr,
		_appKeyIndex,
		_sigModelID?"true":"false",
		_companyIdentifier,
		_modelIdentifier
		);
	#endif

	_srcAddress	= srcAddress;

	Oscl::BT::Mesh::Element::Api*
	element	= _networkApi.findElement(_elementAddr);

	if(!element){
		Oscl::Error::Info::log(
			"%s: No Such Element (0x%4.4X)\n",
			OSCL_PRETTY_FUNCTION,
			_elementAddr
			);
		sendConfigModelAppStatus(
			Oscl::BT::Mesh::Message::Foundation::Status::invalidAddress
			);
		return true;
		}

	if(_sigModelID){
		Oscl::BT::Mesh::Model::Api*
		model	= element->findSigModel(_modelIdentifier);
		if(model){
			if(!model->modelSupportsAppKeyBinding()){
				sendConfigModelAppStatus(
					Oscl::BT::Mesh::Message::Foundation::Status::cannotBind
					);
				}
			const Oscl::BT::Mesh::Key::Result::Var::State&
			result	= model->unbind(_appKeyIndex);
			result.accept(_configModelAppBindResult);
			return true;
			}
		}
	else {
		Oscl::BT::Mesh::Model::Api*
		model	= element->findVendorModel(_companyIdentifier,_modelIdentifier);
		if(model){
			const Oscl::BT::Mesh::Key::Result::Var::State&
			result	= model->unbind(_appKeyIndex);
			result.accept(_configModelAppBindResult);
			return true;
			}
		}

	// Failed to find model.
	Oscl::Error::Info::log(
		"%s: No Such Model (0x%4.4X)\n",
		OSCL_PRETTY_FUNCTION,
		_modelIdentifier
		);

	sendConfigModelAppStatus(
		Oscl::BT::Mesh::Message::Foundation::Status::invalidModel
		);
	return true;
	}

bool	Part::rxConfigSigModelAppGet(
			uint32_t	opcode,
			uint16_t	srcAddress,
			uint16_t	dstAddress,
			const void*	frame,
			unsigned	length,
			uint16_t	appKeyIndex
			) noexcept{

	if(opcode != Oscl::BT::Mesh::Model::OpCode::configSigModelAppGet){
		// Not Config Sig Model App Get
		return false;
		}

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n"
		"",
		__PRETTY_FUNCTION__
		);
	Oscl::Error::Info::hexDump(
		frame,
		length
		);
	#endif

	Oscl::Endian::Decoder::Linear::Part
	leDecoder(
		frame,
		length
		);

	Oscl::Decoder::Api&
	decoder	= leDecoder.le();

	uint16_t	elementAddress;
	uint16_t	modelIdentifier;

	decoder.decode(elementAddress);
	decoder.decode(modelIdentifier);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			OSCL_PRETTY_FUNCTION
			);
		return true;
		}

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n"
		"\tElement Address: 0x%4.4X\n"
		"\tSigModelID: 0x%4.4X\n"
		"",
		__PRETTY_FUNCTION__,
		elementAddress,
		modelIdentifier
		);
	#endif

	_nAppKeys	= 0;

	Oscl::BT::Mesh::Element::Api*
	element	= _networkApi.findElement(elementAddress);

	if(!element){
		Oscl::Error::Info::log(
			"%s: No Such Element (0x%4.4X)\n",
			OSCL_PRETTY_FUNCTION,
			elementAddress
			);
		sendConfigSigModelAppList(
			srcAddress,
			Oscl::BT::Mesh::Message::Foundation::Status::invalidAddress,
			elementAddress,
			modelIdentifier,
			0
			);
		return true;
		}

	Oscl::BT::Mesh::Model::Api*
	modelApi	= element->findSigModel(modelIdentifier);

	if(!modelApi){
		sendConfigSigModelAppList(
			srcAddress,
			Oscl::BT::Mesh::Message::Foundation::Status::invalidModel,
			elementAddress,
			modelIdentifier,
			0
			);
		return true;
		}

	Oscl::BT::Mesh::Model::Key::Api*
	modelKeyApi	= modelApi->getModelKeyApi();

	if(!modelKeyApi){
		return true;
		}

	Oscl::BT::Mesh::Model::Key::
	Iterator::Composer<Part>
		iterator(
			*this,
			&Part::configSigModelAppBindItem
			);

	_nAppKeys	= 0;

	bool
	stopped	= modelKeyApi->iterate(
				iterator
				);

	if(stopped){
		// If iteration was stopped, then we
		// have run out of _buffer space.

		// This is very unlikely.

		// FIXME: Should we indicate and
		// error status (outOfResources) or
		// just send what we have?

		// For now, we just send what we have.
		}

	sendConfigSigModelAppList(
		srcAddress,
		Oscl::BT::Mesh::Message::Foundation::Status::success,
		elementAddress,
		modelIdentifier,
		_nAppKeys,
		_buffer
		);

	return true;
	}

bool	Part::rxConfigNodeReset(
			uint32_t	opcode,
			uint16_t	srcAddress,
			uint16_t	dstAddress,
			const void*	frame,
			unsigned	length,
			uint16_t	appKeyIndex
			) noexcept{

	if(opcode != Oscl::BT::Mesh::Model::OpCode::configNodeReset){
		// Not Config Node Reset
		return false;
		}

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n"
		"",
		__PRETTY_FUNCTION__
		);
	Oscl::Error::Info::hexDump(
		frame,
		length
		);
	#endif

	sendConfigNodeResetStatus(
		srcAddress
		);

	// FIXME: The result of this message
	// is to remove this node from this
	// network.
	// Because the Config Node Reset Status
	// message must traverse the various
	// queues in the transmit stack, I can
	// imagine that the actual destruction
	// of the node may need to be delayed.
	// However, I can also imagine that the
	// node will not be completely destroyed
	// until all transmit PDU buffers have
	// been recovered and, therefore, this
	// problem may be imaginary.
	// In either case, we perform the actual
	// node reset *after* we send the Config
	// Node Reset Status response.

	return true;
	}

bool	Part::rxConfigFriendGet(
			uint32_t	opcode,
			uint16_t	srcAddress,
			uint16_t	dstAddress,
			const void*	frame,
			unsigned	length,
			uint16_t	appKeyIndex
			) noexcept{

	if(opcode != Oscl::BT::Mesh::Model::OpCode::configFriendGet){
		// Not Config Friend Get
		return false;
		}

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n"
		"",
		__PRETTY_FUNCTION__
		);
	Oscl::Error::Info::hexDump(
		frame,
		length
		);
	#endif

	sendConfigFriendStatus(
		srcAddress,
		_networkApi.getFriendState()
		);

	return true;
	}

bool	Part::rxConfigFriendSet(
			uint32_t	opcode,
			uint16_t	srcAddress,
			uint16_t	dstAddress,
			const void*	frame,
			unsigned	length,
			uint16_t	appKeyIndex
			) noexcept{

	if(opcode != Oscl::BT::Mesh::Model::OpCode::configFriendSet){
		// Not Config Friend Set
		return false;
		}

	Oscl::Endian::Decoder::Linear::Part
	leDecoder(
		frame,
		length
		);

	Oscl::Decoder::Api&
	decoder	= leDecoder.le();

	uint8_t	state;

	decoder.decode(state);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			__PRETTY_FUNCTION__
			);
		return true;
		}

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n"
		"\tState: 0x%2.2X\n"
		"",
		__PRETTY_FUNCTION__,
		state
		);
	Oscl::Error::Info::hexDump(
		frame,
		length
		);
	#endif

	if(state > 0x01){
		// Invalid friend state value.
		// Only disable (0x00) or enable (0x01)
		// are allowed.
		#ifdef DEBUG_TRACE
		Oscl::Error::Info::log(
			"%s: Friend state value 0x%2.2X not allowed.\n",
			__PRETTY_FUNCTION__,
			state
			);
		#endif
		}
	else {
		bool
		failed	= _networkApi.setFriendState(state);
		if(failed){
			#ifdef DEBUG_TRACE
			Oscl::Error::Info::log(
				"%s: Friend not supported.\n",
				__PRETTY_FUNCTION__
				);
			#endif
			}
		}

	sendConfigFriendStatus(
		srcAddress,
		_networkApi.getFriendState()
		);

	return true;
	}

bool	Part::rxConfigKeyRefreshPhaseGet(
			uint32_t	opcode,
			uint16_t	srcAddress,
			uint16_t	dstAddress,
			const void*	frame,
			unsigned	length,
			uint16_t	appKeyIndex
			) noexcept{

	if(opcode != Oscl::BT::Mesh::Model::OpCode::configKeyRefreshPhaseGet){
		// Not Config Key Refresh Phase Get
		return false;
		}

	Oscl::Endian::Decoder::Linear::Part
	leDecoder(
		frame,
		length
		);

	Oscl::Decoder::Api&
	decoder	= leDecoder.le();

	uint16_t	netKeyIndex;

	decoder.decode(netKeyIndex);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			OSCL_PRETTY_FUNCTION
			);
		return true;
		}

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n"
		"\tNetKey Index: 0x%4.4X\n"
		"",
		__PRETTY_FUNCTION__,
		netKeyIndex
		);
	Oscl::Error::Info::hexDump(
		frame,
		length
		);
	#endif


	Oscl::BT::Mesh::Network::Api*
	network	= findNetwork(netKeyIndex);

	uint8_t	phase;
	uint8_t	statusCode;

	if(network){
		phase		= network->getNetworkKeyPhase();
		statusCode	= Oscl::BT::Mesh::Message::Foundation::Status::success;
		}
	else {
		phase		= 0;
		statusCode	= Oscl::BT::Mesh::Message::Foundation::Status::invalidNetKeyIndex;
		}

	sendConfigKeyRefreshPhaseStatus(
		srcAddress,
		statusCode,
		netKeyIndex,
		phase
		);

	return true;
	}

bool	Part::rxConfigKeyRefreshPhaseSet(
			uint32_t	opcode,
			uint16_t	srcAddress,
			uint16_t	dstAddress,
			const void*	frame,
			unsigned	length,
			uint16_t	appKeyIndex
			) noexcept{

	if(opcode != Oscl::BT::Mesh::Model::OpCode::configKeyRefreshPhaseSet){
		// Not Config Refresh Phase Set
		return false;
		}

	Oscl::Endian::Decoder::Linear::Part
	leDecoder(
		frame,
		length
		);

	Oscl::Decoder::Api&
	decoder	= leDecoder.le();

	uint16_t	netKeyIndex;
	uint8_t		transition;

	decoder.decode(netKeyIndex);
	decoder.decode(transition);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			OSCL_PRETTY_FUNCTION
			);
		return true;
		}

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n"
		"\tNetKey Index: 0x%4.4X\n"
		"\tTransition: 0x%2.2X\n"
		"",
		__PRETTY_FUNCTION__,
		netKeyIndex,
		transition
		);
	#endif

	Oscl::BT::Mesh::Network::Api*
	network	= findNetwork(netKeyIndex);

	uint8_t	phase;
	uint8_t	statusCode;

	if(network){
		network->setNetworkKeyPhase(transition);
		phase		= network->getNetworkKeyPhase();
		statusCode	= Oscl::BT::Mesh::Message::Foundation::Status::success;
		}
	else {
		phase		= 0;
		statusCode	= Oscl::BT::Mesh::Message::Foundation::Status::invalidNetKeyIndex;
		}

	sendConfigKeyRefreshPhaseStatus(
		srcAddress,
		statusCode,
		netKeyIndex,
		phase
		);


	return true;
	}

bool	Part::rxConfigHeartbeatPublicationGet(
			uint32_t	opcode,
			uint16_t	srcAddress,
			uint16_t	dstAddress,
			const void*	frame,
			unsigned	length,
			uint16_t	appKeyIndex
			) noexcept{

	if(opcode != Oscl::BT::Mesh::Model::OpCode::configHeartbeatPublicationGet){
		// Not Config Heartbeat Publication Get
		return false;
		}

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n"
		"",
		__PRETTY_FUNCTION__
		);
	Oscl::Error::Info::hexDump(
		frame,
		length
		);
	#endif

	uint16_t	destination;
	uint8_t		countLog;
	uint8_t		periodLog;
	uint8_t		ttl;
	uint16_t	features;
	uint16_t	netKeyIndex;

	_nodeApi.getConfigHeartbeatPublicationStatus(
		destination,
		countLog,
		periodLog,
		ttl,
		features,
		netKeyIndex
		);

	sendConfigHeartbeatPublicationStatus(
		srcAddress,
		Oscl::BT::Mesh::Message::Foundation::Status::success,
		destination,
		countLog,
		periodLog,
		ttl,
		features,
		netKeyIndex
		);

	return true;
	}

bool	Part::rxConfigHeartbeatPublicationSet(
			uint32_t	opcode,
			uint16_t	srcAddress,
			uint16_t	dstAddress,
			const void*	frame,
			unsigned	length,
			uint16_t	appKeyIndex
			) noexcept{

	if(opcode != Oscl::BT::Mesh::Model::OpCode::configHeartbeatPublicationSet){
		// Not Config Heartbeat Publication Set
		return false;
		}

	Oscl::Endian::Decoder::Linear::Part
	leDecoder(
		frame,
		length
		);

	Oscl::Decoder::Api&
	decoder	= leDecoder.le();

	uint16_t	destination;
	uint8_t		countLog;
	uint8_t		periodLog;
	uint8_t		ttl;
	uint16_t	features;
	uint16_t	netKeyIndex;

	decoder.decode(destination);
	decoder.decode(countLog);
	decoder.decode(periodLog);
	decoder.decode(ttl);
	decoder.decode(features);
	decoder.decode(netKeyIndex);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			OSCL_PRETTY_FUNCTION
			);
		return true;
		}

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n"
		"\tDestination: 0x%4.4X\n"
		"\tCountLog: 0x%2.2X\n"
		"\tPeriodLog: 0x%2.2X\n"
		"\tTTL: 0x%2.2X\n"
		"\tFeatures: 0x%4.4X\n"
		"\tNetKey Index: 0x%4.4X\n"
		"",
		__PRETTY_FUNCTION__,
		destination,
		countLog,
		periodLog,
		ttl,
		features,
		netKeyIndex
		);
	#endif

	bool
	failed	= _nodeApi.setConfigHeartbeatPublication(
				destination,
				countLog,
				periodLog,
				ttl,
				features,
				netKeyIndex
				);

	uint8_t
	statusCode	= failed?
					Oscl::BT::Mesh::Message::Foundation::Status::invalidNetKeyIndex:
					Oscl::BT::Mesh::Message::Foundation::Status::success
					;

	sendConfigHeartbeatPublicationStatus(
		srcAddress,
		statusCode,
		destination,
		countLog,
		periodLog,
		ttl,
		features,
		netKeyIndex
		);

	return true;
	}

bool	Part::rxConfigHeartbeatSubscriptionGet(
			uint32_t	opcode,
			uint16_t	srcAddress,
			uint16_t	dstAddress,
			const void*	frame,
			unsigned	length,
			uint16_t	appKeyIndex
			) noexcept{

	if(opcode != Oscl::BT::Mesh::Model::OpCode::configHeartbeatSubscriptionGet){
		// Not Config Heartbeat Subscription Get
		return false;
		}

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n"
		"",
		__PRETTY_FUNCTION__
		);
	Oscl::Error::Info::hexDump(
		frame,
		length
		);
	#endif

	uint16_t	source;
	uint16_t	destination;
	uint8_t		periodLog;
	uint8_t		countLog;
	uint8_t		minHops;
	uint8_t		maxHops;

	_networkApi.getConfigHeartbeatSubscriptionStatus(
		source,
		destination,
		periodLog,
		countLog,
		minHops,
		maxHops
		);

	sendConfigHeartbeatSubscriptionStatus(
		srcAddress,
		Oscl::BT::Mesh::Message::Foundation::Status::success,
		source,
		destination,
		periodLog,
		countLog,
		minHops,
		maxHops
		);

	return true;
	}

bool	Part::rxConfigHeartbeatSubscriptionSet(
			uint32_t	opcode,
			uint16_t	srcAddress,
			uint16_t	dstAddress,
			const void*	frame,
			unsigned	length,
			uint16_t	appKeyIndex
			) noexcept{

	if(opcode != Oscl::BT::Mesh::Model::OpCode::configHeartbeatSubscriptionSet){
		// Not Config Heartbeat Subscription Set
		return false;
		}

	Oscl::Endian::Decoder::Linear::Part
	leDecoder(
		frame,
		length
		);

	Oscl::Decoder::Api&
	decoder	= leDecoder.le();

	uint16_t	source;
	uint16_t	destination;
	uint8_t		periodLog;

	decoder.decode(source);
	decoder.decode(destination);
	decoder.decode(periodLog);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			OSCL_PRETTY_FUNCTION
			);
		return true;
		}

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n"
		"\tSource: 0x%4.4X\n"
		"\tDestination: 0x%4.4X\n"
		"\tPeriod Log: 0x%2.2X\n"
		"",
		__PRETTY_FUNCTION__,
		source,
		destination,
		periodLog
		);
	#endif

	bool
	failed	= _networkApi.setConfigHeartbeatSubscription(
				source,
				destination,
				periodLog
				);

	uint8_t
	statusCode	= failed?
					Oscl::BT::Mesh::Message::Foundation::Status::invalidAddress:
					Oscl::BT::Mesh::Message::Foundation::Status::success
					;

	sendConfigHeartbeatSubscriptionStatus(
		srcAddress,
		statusCode,
		source,
		destination,
		periodLog,
		0,	// countLog,
		0,	// minHops,
		0	// maxHops
		);

	return true;
	}

bool	Part::rxConfigLowPowerNodePollTimeoutGet(
			uint32_t	opcode,
			uint16_t	srcAddress,
			uint16_t	dstAddress,
			const void*	frame,
			unsigned	length,
			uint16_t	appKeyIndex
			) noexcept{

	if(opcode != Oscl::BT::Mesh::Model::OpCode::configLowPowerNodePollTimeoutGet){
		// Not Config Low Power Node PollTimeout Get
		return false;
		}

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n"
		"",
		__PRETTY_FUNCTION__
		);
	Oscl::Error::Info::hexDump(
		frame,
		length
		);
	#endif

	Oscl::Endian::Decoder::Linear::Part
	leDecoder(
		frame,
		length
		);

	Oscl::Decoder::Api&
	decoder	= leDecoder.le();

	uint16_t	lpnAddress;

	decoder.decode(lpnAddress);

	uint32_t	pollTimeout;

	_networkApi.getLowPowerNodePollTimeout(
		lpnAddress,
		pollTimeout
		);

	sendConfigLowPowerNodePollTimeoutStatus(
		srcAddress,
		lpnAddress,
		pollTimeout
		);

	return true;
	}

bool	Part::rxConfigNetworkTransmitGet(
			uint32_t	opcode,
			uint16_t	srcAddress,
			uint16_t	dstAddress,
			const void*	frame,
			unsigned	length,
			uint16_t	appKeyIndex
			) noexcept{

	if(opcode != Oscl::BT::Mesh::Model::OpCode::configNetworkTransmitGet){
		// Not Config Network Transmit Get
		return false;
		}

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n"
		"",
		__PRETTY_FUNCTION__
		);
	Oscl::Error::Info::hexDump(
		frame,
		length
		);
	#endif

	uint8_t		networkTransmitCount;
	uint8_t		networkTransmitIntervalSteps;

	_networkApi.getNetworkTransmit(
		networkTransmitCount,
		networkTransmitIntervalSteps
		);

	sendConfigNetworkTransmitStatus(
		srcAddress,
		networkTransmitCount,
		networkTransmitIntervalSteps
		);

	return true;
	}

bool	Part::rxConfigNetworkTransmitSet(
			uint32_t	opcode,
			uint16_t	srcAddress,
			uint16_t	dstAddress,
			const void*	frame,
			unsigned	length,
			uint16_t	appKeyIndex
			) noexcept{

	if(opcode != Oscl::BT::Mesh::Model::OpCode::configNetworkTransmitSet){
		// Not Config Network Transmit Set
		return false;
		}

	Oscl::Endian::Decoder::Linear::Part
	leDecoder(
		frame,
		length
		);

	Oscl::Decoder::Api&
	decoder	= leDecoder.le();

	uint8_t		countIntervalSteps;

	decoder.decode(countIntervalSteps);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underfow!\n",
			OSCL_PRETTY_FUNCTION
			);
		return true;
		}

	uint8_t
	networkTransmitCount	= (countIntervalSteps >> 5);
	uint8_t
	networkTransmitIntervalSteps	= (countIntervalSteps & 0x1F);

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n"
		"\tOctet: 0x%2.2X\n"
		"\t\tNetwork Transmit Count: 0x%2.2X\n"
		"\t\tNetwork Transmit Interval Steps: 0x%2.2X\n"
		"",
		__PRETTY_FUNCTION__,
		countIntervalSteps,
		networkTransmitCount,
		networkTransmitIntervalSteps
		);
	#endif

	_networkApi.setNetworkTransmit(
		networkTransmitCount,
		networkTransmitIntervalSteps
		);

	sendConfigNetworkTransmitStatus(
		srcAddress,
		networkTransmitCount,
		networkTransmitIntervalSteps
		);

	return true;
	}

void	Part::sendConfigCompositionDataStatus(
			uint16_t	dst,
			uint8_t		page
			) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n"
		"",
		__PRETTY_FUNCTION__
		);
	#endif

	if(page != 0){
		Oscl::Error::Info::log(
			"%s: No such page (%u). Sending page zero.\n",
			OSCL_PRETTY_FUNCTION,
			page
			);
		page	= 0;
		}

	unsigned	pageLength;

	const void*
	data	= _compositionDataStateApi.getPageZero(pageLength);

	if(!data || !pageLength){
		Oscl::Error::Info::log(
			"%s: No data for page %u\n",
			OSCL_PRETTY_FUNCTION,
			page
			);
		return;
		}

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n"
		"\tPage: %u\n"
		"",
		__PRETTY_FUNCTION__,
		page
		);

	Oscl::Error::Info::hexDump(
		data,
		pageLength
		);
	#endif

	uint8_t	buffer[maxAccessPduSizeWithTransMic32];

	// The Access Payload is Little-Endian
	Oscl::Encoder::LE::Base
	leEncoder(
		buffer,
		sizeof(buffer)
		);
	Oscl::Encoder::Api&	encoder	= leEncoder;

	Oscl::BT::Mesh::Model::OpCode::encode(
		encoder,
		Oscl::BT::Mesh::Model::OpCode::configCompositionDataStatus
		);

	encoder.encode(page);
	encoder.copyIn(
		data,
		pageLength
		);

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s: Access PDU:\n"
		"\tOpcode: 0x%6.6X\n"
		"\tPage: %u\n"
		"",
		__PRETTY_FUNCTION__,
		Oscl::BT::Mesh::Model::OpCode::configCompositionDataStatus,
		page
		);

	Oscl::Error::Info::hexDump(
		buffer,
		encoder.length()
		);
	#endif

	Oscl::BT::Mesh::Transport::Upper::TX::Api&
	transportApi	= _upperTransportTx;

	transportApi.sendSegmented(
		this,	// completeApi
		buffer,
		encoder.length(),
		dst,
		0,	// appKeyIndex (ignored for device key)
		txTTL
		);
	}

void	Part::sendConfigDefaultTtlStatus(
			uint16_t	dst,
			uint8_t		defaultTTL
			) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n"
		"",
		__PRETTY_FUNCTION__
		);
	#endif

	uint8_t	buffer[maxAccessPduSizeWithTransMic32];

	Oscl::Encoder::LE::Base
	leEncoder(
		buffer,
		sizeof(buffer)
		);
	Oscl::Encoder::Api&	encoder	= leEncoder;

	Oscl::BT::Mesh::Model::OpCode::encode(
		encoder,
		Oscl::BT::Mesh::Model::OpCode::configDefaultTtlStatus
		);

	encoder.encode(defaultTTL);

	if(encoder.overflow()){
		Oscl::Error::Info::log(
			"%s: overflow!\n",
			OSCL_PRETTY_FUNCTION
			);
		return;
		}

	Oscl::BT::Mesh::Transport::Upper::TX::Api&
	transportApi	= _upperTransportTx;

	transportApi.sendSegmented(
		this,	// completeApi
		buffer,
		encoder.length(),
		dst,	// dst
		0,	// appKeyIndex (ignored for device key)
		txTTL
		);
	}

void	Part::sendConfigGattProxyStatus(
			uint16_t	dst,
			uint8_t		state
			) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n"
		"",
		__PRETTY_FUNCTION__
		);
	#endif

	uint8_t	buffer[maxAccessPduSizeWithTransMic32];

	Oscl::Encoder::LE::Base
	leEncoder(
		buffer,
		sizeof(buffer)
		);
	Oscl::Encoder::Api&	encoder	= leEncoder;

	Oscl::BT::Mesh::Model::OpCode::encode(
		encoder,
		Oscl::BT::Mesh::Model::OpCode::configGattProxyStatus
		);

	encoder.encode(state);

	if(encoder.overflow()){
		Oscl::Error::Info::log(
			"%s: overflow!\n",
			OSCL_PRETTY_FUNCTION
			);
		return;
		}

	Oscl::BT::Mesh::Transport::Upper::TX::Api&
	transportApi	= _upperTransportTx;

	transportApi.sendSegmented(
		this,	// completeApi
		buffer,
		encoder.length(),
		dst,	// dst
		0,	// appKeyIndex (ignored for device key)
		txTTL
		);
	}

void	Part::sendConfigRelayStatus(
			uint16_t	dst,
			uint8_t		relay,
			uint8_t		retransmitCount,
			uint8_t		retransmitIntervalSteps
			) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n"
		"",
		__PRETTY_FUNCTION__
		);
	#endif

	uint8_t	buffer[maxAccessPduSizeWithTransMic32];

	Oscl::Encoder::LE::Base
	leEncoder(
		buffer,
		sizeof(buffer)
		);
	Oscl::Encoder::Api&	encoder	= leEncoder;

	Oscl::BT::Mesh::Model::OpCode::encode(
		encoder,
		Oscl::BT::Mesh::Model::OpCode::configRelayStatus
		);

	uint8_t
	countSteps	= (retransmitCount & 0x07);
	countSteps	<<= 5;
	countSteps	|= (retransmitIntervalSteps & 0x1F);

	encoder.encode(relay);
	encoder.encode(countSteps);

	if(encoder.overflow()){
		Oscl::Error::Info::log(
			"%s: overflow!\n",
			OSCL_PRETTY_FUNCTION
			);
		return;
		}

	Oscl::BT::Mesh::Transport::Upper::TX::Api&
	transportApi	= _upperTransportTx;

	transportApi.sendSegmented(
		this,	// completeApi
		buffer,
		encoder.length(),
		dst,	// dst
		0,	// appKeyIndex (ignored for device key)
		txTTL
		);
	}

void	Part::sendConfigModelPublicationStatus(
					uint16_t	dst,
					uint8_t		statusCode,
					uint16_t	elementAddress,
					uint16_t	publishAddress,
					uint16_t	appKeyIndex,
					bool		credentialFlag,
					uint8_t		publishTTL,
					uint8_t		publishPeriod,
					uint8_t		publishRetransmitCount,
					uint8_t		publishRetransmitIntervalSteps,
					bool		sigModel,
					uint16_t	modelIdentifier,
					uint16_t	companyID
					) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n"
		"\tElementAddress: 0x%4.4X\n"
		"\tPublish Address: 0x%4.4X\n"
		"\tAppKey Index: (0x%4.4X) %u\n"
		"\tCredential Flag: %s\n"
		"\tPublish TTL: 0x%2.2X\n"
		"\tPublish Period: %u\n"
		"\tPublish Retransmit Count: %u\n"
		"\tPublish Retransmit Interval Steps: %u\n"
		"\tModel ID: 0x%4.4X\n"
		"\tisSigModel: %s\n"
		"\tCompany ID: 0x%4.4X [%s]\n"
		"",
		__PRETTY_FUNCTION__,
		elementAddress,
		publishAddress,
		appKeyIndex,
		appKeyIndex,
		credentialFlag?"true":"false",
		publishTTL,
		publishPeriod,
		publishRetransmitCount,
		publishRetransmitIntervalSteps,
		modelIdentifier,
		sigModel?"true":"false",
		companyID,
		sigModel?"INVALID":"VALID"
		);
	#endif

	uint8_t	buffer[maxAccessPduSizeWithTransMic32];

	Oscl::Encoder::LE::Base
	leEncoder(
		buffer,
		sizeof(buffer)
		);
	Oscl::Encoder::Api&	encoder	= leEncoder;

	uint16_t
	appKeyIndexAndCredentialFlag	= appKeyIndex;
	if(credentialFlag){
		appKeyIndexAndCredentialFlag	|= (1<<12);
		}

	uint8_t
	publishRetransmit	= publishRetransmitIntervalSteps;
	publishRetransmit	<<= 3;
	publishRetransmit	|= (publishRetransmitCount & 0x07);

	Oscl::BT::Mesh::Model::OpCode::encode(
		encoder,
		Oscl::BT::Mesh::Model::OpCode::configModelPublicationStatus
		);

	encoder.encode(statusCode);
	encoder.encode(elementAddress);
	encoder.encode(publishAddress);
	encoder.encode(appKeyIndexAndCredentialFlag);
	encoder.encode(publishTTL);
	encoder.encode(publishPeriod);
	encoder.encode(publishRetransmit);
	encoder.encode(modelIdentifier);
	if(!sigModel){
		encoder.encode(companyID);
		}

	if(encoder.overflow()){
		Oscl::Error::Info::log(
			"%s: overflow!\n",
			OSCL_PRETTY_FUNCTION
			);
		return;
		}

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s: PDU:\n",
		__PRETTY_FUNCTION__
		);
	Oscl::Error::Info::hexDump(
		buffer,
		encoder.length()
		);
	#endif

	Oscl::BT::Mesh::Transport::Upper::TX::Api&
	transportApi	= _upperTransportTx;

	transportApi.sendSegmented(
		this,	// completeApi
		buffer,
		encoder.length(),
		dst,	// dst
		0,	// appKeyIndex (ignored for device key)
		txTTL
		);
	}

void	Part::sendConfigModelSubscriptionStatus(
			uint16_t	dst,
			uint8_t		statusCode,
			uint16_t	elementAddress,
			uint16_t	address,
			bool		sigModel,
			uint16_t	modelIdentifier,
			uint16_t	companyID
			) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n"
		"\tStatus: 0x%2.2X\n"
		"\tElementAddress: 0x%4.4X\n"
		"\tAddress: 0x%4.4X\n"
		"\tModel ID: 0x%4.4X\n"
		"\tisSigModel: %s\n"
		"\tCompany ID: 0x%4.4X [%s]\n"
		"",
		__PRETTY_FUNCTION__,
		statusCode,
		elementAddress,
		address,
		modelIdentifier,
		sigModel?"true":"false",
		companyID,
		sigModel?"INVALID":"VALID"
		);
	#endif

	uint8_t	buffer[maxAccessPduSizeWithTransMic32];

	Oscl::Encoder::LE::Base
	leEncoder(
		buffer,
		sizeof(buffer)
		);
	Oscl::Encoder::Api&	encoder	= leEncoder;

	Oscl::BT::Mesh::Model::OpCode::encode(
		encoder,
		Oscl::BT::Mesh::Model::OpCode::configModelSubscriptionStatus
		);

	encoder.encode(statusCode);
	encoder.encode(elementAddress);
	encoder.encode(address);
	encoder.encode(modelIdentifier);
	if(!sigModel){
		encoder.encode(companyID);
		}

	if(encoder.overflow()){
		Oscl::Error::Info::log(
			"%s: overflow!\n",
			OSCL_PRETTY_FUNCTION
			);
		return;
		}

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s: PDU:\n",
		__PRETTY_FUNCTION__
		);
	Oscl::Error::Info::hexDump(
		buffer,
		encoder.length()
		);
	#endif

	Oscl::BT::Mesh::Transport::Upper::TX::Api&
	transportApi	= _upperTransportTx;

	transportApi.sendSegmented(
		this,	// completeApi
		buffer,
		encoder.length(),
		dst,	// dst
		0,	// appKeyIndex (ignored for device key)
		txTTL
		);
	}

void	Part::sendConfigSigModelSubscriptionList(
			uint16_t		dst,
			uint8_t			statusCode,
			uint16_t		elementAddress,
			uint16_t		modelIdentifier,
			const uint16_t*	addressList,
			unsigned		nAddressListItems
			) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n"
		"\tElementAddress: 0x%4.4X\n"
		"\tModel ID: 0x%4.4X\n"
		"",
		__PRETTY_FUNCTION__,
		elementAddress,
		modelIdentifier
		);
	#endif

	uint8_t	buffer[maxAccessPduSizeWithTransMic32];

	Oscl::Encoder::LE::Base
	leEncoder(
		buffer,
		sizeof(buffer)
		);
	Oscl::Encoder::Api&	encoder	= leEncoder;

	Oscl::BT::Mesh::Model::OpCode::encode(
		encoder,
		Oscl::BT::Mesh::Model::OpCode::configSigModelSubscriptionList
		);

	encoder.encode(statusCode);
	encoder.encode(elementAddress);
	encoder.encode(modelIdentifier);

	if(encoder.overflow()){
		Oscl::Error::Info::log(
			"%s: overflow!\n",
			OSCL_PRETTY_FUNCTION
			);
		return;
		}

	if(addressList){
		for(unsigned i=0;i<nAddressListItems;++i){
			encoder.encode(addressList[i]);
			}
		}

	if(encoder.overflow()){
		// Too many addresses to fit in a maximum sized
		// Access PDU (maxAccessPduSizeWithTransMic32).
		// Some addresses are lost, but we still report
		// as many as possible.
		Oscl::Error::Info::log(
			"%s: overflow ignored!\n",
			OSCL_PRETTY_FUNCTION
			);
		}

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s: PDU:\n",
		__PRETTY_FUNCTION__
		);
	Oscl::Error::Info::hexDump(
		buffer,
		encoder.length()
		);
	#endif

	Oscl::BT::Mesh::Transport::Upper::TX::Api&
	transportApi	= _upperTransportTx;

	transportApi.sendSegmented(
		this,	// completeApi
		buffer,
		encoder.length(),
		dst,	// dst
		0,	// appKeyIndex (ignored for device key)
		txTTL
		);
	}

void	Part::sendConfigVendorModelSubscriptionList(
			uint16_t		dst,
			uint8_t			statusCode,
			uint16_t		elementAddress,
			uint16_t		modelIdentifier,
			uint16_t		companyID,
			const uint16_t*	addressList,
			unsigned		nAddressListItems
			) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n"
		"\tElementAddress: 0x%4.4X\n"
		"\tModel ID: 0x%4.4X\n"
		"\tCompany ID: 0x%4.4X\n"
		"",
		__PRETTY_FUNCTION__,
		elementAddress,
		modelIdentifier,
		companyID
		);
	#endif

	uint8_t	buffer[maxAccessPduSizeWithTransMic32];

	Oscl::Encoder::LE::Base
	leEncoder(
		buffer,
		sizeof(buffer)
		);
	Oscl::Encoder::Api&	encoder	= leEncoder;

	Oscl::BT::Mesh::Model::OpCode::encode(
		encoder,
		Oscl::BT::Mesh::Model::OpCode::configVendorModelSubscriptionList
		);

	encoder.encode(statusCode);
	encoder.encode(elementAddress);
	encoder.encode(modelIdentifier);
	encoder.encode(companyID);

	if(encoder.overflow()){
		Oscl::Error::Info::log(
			"%s: overflow!\n",
			OSCL_PRETTY_FUNCTION
			);
		return;
		}

	if(addressList){
		for(unsigned i=0;i<nAddressListItems;++i){
			encoder.encode(addressList[i]);
			}
		}

	if(encoder.overflow()){
		// Too many addresses to fit in a maximum sized
		// Access PDU (maxAccessPduSizeWithTransMic32).
		// Some addresses are lost, but we still report
		// as many as possible.
		Oscl::Error::Info::log(
			"%s: overflow ignored!\n",
			OSCL_PRETTY_FUNCTION
			);
		}

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s: PDU:\n",
		__PRETTY_FUNCTION__
		);
	Oscl::Error::Info::hexDump(
		buffer,
		encoder.length()
		);
	#endif

	Oscl::BT::Mesh::Transport::Upper::TX::Api&
	transportApi	= _upperTransportTx;

	transportApi.sendSegmented(
		this,	// completeApi
		buffer,
		encoder.length(),
		dst,	// dst
		0,	// appKeyIndex (ignored for device key)
		txTTL
		);
	}

void	Part::sendConfigNetKeyStatus(
			uint8_t		statusCode,
			uint8_t		keyIndicie0,
			uint8_t		keyIndicie1,
			uint16_t	dst
			) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n"
		"",
		__PRETTY_FUNCTION__
		);
	#endif

	uint8_t	buffer[maxAccessPduSizeWithTransMic32];

	Oscl::Encoder::LE::Base
	leEncoder(
		buffer,
		sizeof(buffer)
		);
	Oscl::Encoder::Api&	encoder	= leEncoder;

	Oscl::BT::Mesh::Model::OpCode::encode(
		encoder,
		Oscl::BT::Mesh::Model::OpCode::configNetKeyStatus
		);

	encoder.encode(statusCode);
	encoder.encode(keyIndicie0);
	encoder.encode(keyIndicie1);

	Oscl::BT::Mesh::Transport::Upper::TX::Api&
	transportApi	= _upperTransportTx;

	transportApi.sendSegmented(
		this,	// completeApi
		buffer,
		encoder.length(),
		dst,
		0,	// appKeyIndex (ignored for device key)
		txTTL
		);
	}

void	Part::sendConfigNetKeyList(
			uint16_t		dst,
			const uint16_t*	netKeyIndicies,
			unsigned		nNetKeyIndicies
			) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n"
		"",
		__PRETTY_FUNCTION__
		);
	#endif

	uint8_t	buffer[maxAccessPduSizeWithTransMic32];

	Oscl::Encoder::LE::Base
	leEncoder(
		buffer,
		sizeof(buffer)
		);
	Oscl::Encoder::Api&	encoder	= leEncoder;

	Oscl::BT::Mesh::Model::OpCode::encode(
		encoder,
		Oscl::BT::Mesh::Model::OpCode::configNetKeyList
		);

	unsigned	nNetKeys = 0;

	for(nNetKeys=0;nNetKeys<nNetKeyIndicies;++nNetKeys){
		unsigned
		offset	= ((nNetKeys * 3)/2);

		if((offset + 2) > sizeof(_buffer)){
			// Halt iteration
			break;
			}

		if(nNetKeys & 0x0001){
			packOdd(
				netKeyIndicies[nNetKeys],
				&_buffer[offset]
				);
			}
		else {
			packEven(
				netKeyIndicies[nNetKeys],
				&_buffer[offset]
				);
			}
		}

	unsigned
	offset	= ((nNetKeys * 3)/2);

	unsigned
	nNetKeyOctets	= offset + (nNetKeys % 2);

	encoder.copyIn(
		_buffer,
		nNetKeyOctets
		);

	Oscl::BT::Mesh::Transport::Upper::TX::Api&
	transportApi	= _upperTransportTx;

	transportApi.sendSegmented(
		this,	// completeApi
		buffer,
		encoder.length(),
		dst,
		0,	// appKeyIndex (ignored for device key)
		txTTL
		);
	}

void	Part::sendConfigAppKeyStatus(
			uint8_t		statusCode,
			uint8_t		keyIndicie0,
			uint8_t		keyIndicie1,
			uint8_t		keyIndicie2,
			uint16_t	dst
			) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n"
		"",
		__PRETTY_FUNCTION__
		);
	#endif

	uint8_t	buffer[maxAccessPduSizeWithTransMic32];

	/*	4.3.1.1 Key indexes

		To pack two key indexes into three octets, 8 LSbs of first
		key index value are packed into the first octet, placing the
		remaining 4 MSbs into 4 LSbs of the second octet.

		The first 4 LSbs of the second 12-bit key index are packed
		into the 4 MSbs of the second octet with the remaining 8
		MSbs into the third octet.

		**	NOTE! IGNORE Figure 4.3 and follow the bizzare
			description above!
	 */

	Oscl::Encoder::LE::Base
	leEncoder(
		buffer,
		sizeof(buffer)
		);
	Oscl::Encoder::Api&	encoder	= leEncoder;

	Oscl::BT::Mesh::Model::OpCode::encode(
		encoder,
		Oscl::BT::Mesh::Model::OpCode::configAppKeyStatus
		);

	encoder.encode(statusCode);
	encoder.encode(keyIndicie0);
	encoder.encode(keyIndicie1);
	encoder.encode(keyIndicie2);

	Oscl::BT::Mesh::Transport::Upper::TX::Api&
	transportApi	= _upperTransportTx;

	transportApi.sendSegmented(
		this,	// completeApi
		buffer,
		encoder.length(),
		dst,
		0,	// appKeyIndex (ignored for device key)
		txTTL
		);
	}

void	Part::sendConfigAppKeyList(
			uint8_t		statusCode,
			uint8_t		keyIndicie0,
			uint8_t		keyIndicie1,
			uint16_t	dst
			) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n"
		"",
		__PRETTY_FUNCTION__
		);
	#endif

	uint8_t	buffer[maxAccessPduSizeWithTransMic32];

	/*	4.3.1.1 Key indexes

		To pack two key indexes into three octets, 8 LSbs of first
		key index value are packed into the first octet, placing the
		remaining 4 MSbs into 4 LSbs of the second octet.

		The first 4 LSbs of the second 12-bit key index are packed
		into the 4 MSbs of the second octet with the remaining 8
		MSbs into the third octet.

		**	NOTE! IGNORE Figure 4.3 and follow the bizzare
			description above!
	 */

	Oscl::Encoder::LE::Base
	leEncoder(
		buffer,
		sizeof(buffer)
		);
	Oscl::Encoder::Api&	encoder	= leEncoder;

	Oscl::BT::Mesh::Model::OpCode::encode(
		encoder,
		Oscl::BT::Mesh::Model::OpCode::configAppKeyList
		);

	unsigned
	offset	= ((_nAppKeys * 3)/2);

	unsigned
	nAppKeyOctets	= offset + (_nAppKeys % 2);

	encoder.encode(statusCode);
	encoder.encode(keyIndicie0);
	encoder.encode(keyIndicie1);
	encoder.copyIn(
		_buffer,
		nAppKeyOctets
		);

	if(encoder.overflow()){
		Oscl::Error::Info::log(
			"%s: overflow!\n",
			OSCL_PRETTY_FUNCTION
			);
		return;
		}

	Oscl::BT::Mesh::Transport::Upper::TX::Api&
	transportApi	= _upperTransportTx;

	transportApi.sendSegmented(
		this,	// completeApi
		buffer,
		encoder.length(),
		dst,
		0,	// appKeyIndex (ignored for device key)
		txTTL
		);
	}

void	Part::sendNodeIdentityStatus(
			uint8_t		statusCode,
			uint8_t		keyIndicie0,
			uint8_t		keyIndicie1,
			uint16_t	dst,
			uint8_t		state
			) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n"
		"",
		__PRETTY_FUNCTION__
		);
	#endif

	uint8_t	buffer[maxAccessPduSizeWithTransMic32];

	/*	4.3.1.1 Key indexes

		To pack two key indexes into three octets, 8 LSbs of first
		key index value are packed into the first octet, placing the
		remaining 4 MSbs into 4 LSbs of the second octet.

		The first 4 LSbs of the second 12-bit key index are packed
		into the 4 MSbs of the second octet with the remaining 8
		MSbs into the third octet.

		**	NOTE! IGNORE Figure 4.3 and follow the bizzare
			description above!
	 */

	Oscl::Encoder::LE::Base
	leEncoder(
		buffer,
		sizeof(buffer)
		);
	Oscl::Encoder::Api&	encoder	= leEncoder;

	Oscl::BT::Mesh::Model::OpCode::encode(
		encoder,
		Oscl::BT::Mesh::Model::OpCode::configNodeIdentityStatus
		);

	encoder.encode(statusCode);
	encoder.encode(keyIndicie0);
	encoder.encode(keyIndicie1);
	encoder.encode(state);

	if(encoder.overflow()){
		Oscl::Error::Info::log(
			"%s: overflow!\n",
			OSCL_PRETTY_FUNCTION
			);
		return;
		}

	Oscl::BT::Mesh::Transport::Upper::TX::Api&
	transportApi	= _upperTransportTx;

	transportApi.sendSegmented(
		this,	// completeApi
		buffer,
		encoder.length(),
		dst,
		0,	// appKeyIndex (ignored for device key)
		txTTL
		);
	}

void	Part::sendConfigNodeResetStatus(
			uint16_t	dst
			) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n"
		"",
		__PRETTY_FUNCTION__
		);
	#endif

	uint8_t	buffer[maxAccessPduSizeWithTransMic32];

	Oscl::Encoder::LE::Base
	leEncoder(
		buffer,
		sizeof(buffer)
		);
	Oscl::Encoder::Api&	encoder	= leEncoder;

	Oscl::BT::Mesh::Model::OpCode::encode(
		encoder,
		Oscl::BT::Mesh::Model::OpCode::configNodeResetStatus
		);

	if(encoder.overflow()){
		Oscl::Error::Info::log(
			"%s: overflow!\n",
			OSCL_PRETTY_FUNCTION
			);
		return;
		}

	/*	The actual reset will be performed when
		the sendSegmented() operation completes,
		whether it completes successfully or not.
	 */
	_resetting	= true;

	Oscl::BT::Mesh::Transport::Upper::TX::Api&
	transportApi	= _upperTransportTx;

	transportApi.sendSegmented(
		this,	// completeApi
		buffer,
		encoder.length(),
		dst,	// dst
		0,	// appKeyIndex (ignored for device key)
		txTTL
		);
	}

void	Part::sendConfigFriendStatus(
			uint16_t	dst,
			uint8_t		state
			) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n"
		"",
		__PRETTY_FUNCTION__
		);
	#endif

	uint8_t	buffer[maxAccessPduSizeWithTransMic32];

	Oscl::Encoder::LE::Base
	leEncoder(
		buffer,
		sizeof(buffer)
		);
	Oscl::Encoder::Api&	encoder	= leEncoder;

	Oscl::BT::Mesh::Model::OpCode::encode(
		encoder,
		Oscl::BT::Mesh::Model::OpCode::configFriendStatus
		);

	encoder.encode(state);

	if(encoder.overflow()){
		Oscl::Error::Info::log(
			"%s: overflow!\n",
			OSCL_PRETTY_FUNCTION
			);
		return;
		}

	Oscl::BT::Mesh::Transport::Upper::TX::Api&
	transportApi	= _upperTransportTx;

	transportApi.sendSegmented(
		this,	// completeApi
		buffer,
		encoder.length(),
		dst,	// dst
		0,	// appKeyIndex (ignored for device key)
		txTTL
		);
	}

void	Part::sendConfigKeyRefreshPhaseStatus(
			uint16_t	dst,
			uint8_t		status,
			uint16_t	netKeyIndex,
			uint8_t		phase
			) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n"
		"",
		__PRETTY_FUNCTION__
		);
	#endif

	uint8_t	buffer[maxAccessPduSizeWithTransMic32];

	Oscl::Encoder::LE::Base
	leEncoder(
		buffer,
		sizeof(buffer)
		);

	Oscl::Encoder::Api&	encoder	= leEncoder;

	Oscl::BT::Mesh::Model::OpCode::encode(
		encoder,
		Oscl::BT::Mesh::Model::OpCode::configKeyRefreshPhaseStatus
		);

	encoder.encode(status);
	encoder.encode(netKeyIndex);
	encoder.encode(phase);

	if(encoder.overflow()){
		Oscl::Error::Info::log(
			"%s: overflow!\n",
			OSCL_PRETTY_FUNCTION
			);
		return;
		}

	Oscl::BT::Mesh::Transport::Upper::TX::Api&
	transportApi	= _upperTransportTx;

	transportApi.sendSegmented(
		this,	// completeApi
		buffer,
		encoder.length(),
		dst,	// dst
		0,	// appKeyIndex (ignored for device key)
		txTTL
		);
	}

void	Part::sendConfigHeartbeatPublicationStatus(
			uint16_t	dst,
			uint8_t		status,
			uint16_t	destination,
			uint8_t		countLog,
			uint8_t		periodLog,
			uint8_t		ttl,
			uint16_t	features,
			uint16_t	netKeyIndex
			) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n"
		"",
		__PRETTY_FUNCTION__
		);
	#endif

	uint8_t	buffer[maxAccessPduSizeWithTransMic32];

	Oscl::Encoder::LE::Base
	leEncoder(
		buffer,
		sizeof(buffer)
		);

	Oscl::Encoder::Api&	encoder	= leEncoder;

	Oscl::BT::Mesh::Model::OpCode::encode(
		encoder,
		Oscl::BT::Mesh::Model::OpCode::configHeartbeatPublicationStatus
		);

	encoder.encode(status);
	encoder.encode(destination);
	encoder.encode(countLog);
	encoder.encode(periodLog);
	encoder.encode(ttl);
	encoder.encode(features);
	encoder.encode(netKeyIndex);

	if(encoder.overflow()){
		Oscl::Error::Info::log(
			"%s: overflow!\n",
			OSCL_PRETTY_FUNCTION
			);
		return;
		}

	Oscl::BT::Mesh::Transport::Upper::TX::Api&
	transportApi	= _upperTransportTx;

	transportApi.sendSegmented(
		this,	// completeApi
		buffer,
		encoder.length(),
		dst,	// dst
		0,	// appKeyIndex (ignored for device key)
		txTTL
		);
	}

void	Part::sendConfigHeartbeatSubscriptionStatus(
			uint16_t	dst,
			uint8_t		status,
			uint16_t	source,
			uint16_t	destination,
			uint8_t		periodLog,
			uint8_t		countLog,
			uint8_t		minHops,
			uint8_t		maxHops
			) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n"
		"",
		__PRETTY_FUNCTION__
		);
	#endif

	uint8_t	buffer[maxAccessPduSizeWithTransMic32];

	Oscl::Encoder::LE::Base
	leEncoder(
		buffer,
		sizeof(buffer)
		);

	Oscl::Encoder::Api&	encoder	= leEncoder;

	Oscl::BT::Mesh::Model::OpCode::encode(
		encoder,
		Oscl::BT::Mesh::Model::OpCode::configHeartbeatSubscriptionStatus
		);

	encoder.encode(status);
	encoder.encode(source);
	encoder.encode(destination);
	encoder.encode(periodLog);
	encoder.encode(countLog);
	encoder.encode(minHops);
	encoder.encode(maxHops);

	if(encoder.overflow()){
		Oscl::Error::Info::log(
			"%s: overflow!\n",
			OSCL_PRETTY_FUNCTION
			);
		return;
		}

	Oscl::BT::Mesh::Transport::Upper::TX::Api&
	transportApi	= _upperTransportTx;

	transportApi.sendSegmented(
		this,	// completeApi
		buffer,
		encoder.length(),
		dst,	// dst
		0,	// appKeyIndex (ignored for device key)
		txTTL
		);
	}

void	Part::sendConfigLowPowerNodePollTimeoutStatus(
			uint16_t	dst,
			uint16_t	lpnAddress,
			uint32_t	pollTimeout
			) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n"
		"",
		__PRETTY_FUNCTION__
		);
	#endif

	uint8_t	buffer[maxAccessPduSizeWithTransMic32];

	Oscl::Encoder::LE::Base
	leEncoder(
		buffer,
		sizeof(buffer)
		);

	Oscl::Encoder::Api&	encoder	= leEncoder;

	Oscl::BT::Mesh::Model::OpCode::encode(
		encoder,
		Oscl::BT::Mesh::Model::OpCode::configLowPowerNodePollTimeoutStatus
		);

	encoder.encode(lpnAddress);
	encoder.encode24Bit(pollTimeout);

	if(encoder.overflow()){
		Oscl::Error::Info::log(
			"%s: overflow!\n",
			OSCL_PRETTY_FUNCTION
			);
		return;
		}

	Oscl::BT::Mesh::Transport::Upper::TX::Api&
	transportApi	= _upperTransportTx;

	transportApi.sendSegmented(
		this,	// completeApi
		buffer,
		encoder.length(),
		dst,	// dst
		0,	// appKeyIndex (ignored for device key)
		txTTL
		);
	}

void	Part::sendConfigNetworkTransmitStatus(
			uint16_t	dst,
			uint8_t		networkTransmitCount,
			uint8_t		networkTransmitIntervalSteps
			) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n"
		"",
		__PRETTY_FUNCTION__
		);
	#endif

	uint8_t	buffer[maxAccessPduSizeWithTransMic32];

	Oscl::Encoder::LE::Base
	leEncoder(
		buffer,
		sizeof(buffer)
		);

	Oscl::Encoder::Api&	encoder	= leEncoder;

	// FIXME: It is unclear where the fields
	// two fields are located in the octet.
	uint8_t
	countIntervalSteps	= networkTransmitCount;
	countIntervalSteps	<<= 5;
	countIntervalSteps	|= networkTransmitIntervalSteps;

	Oscl::BT::Mesh::Model::OpCode::encode(
		encoder,
		Oscl::BT::Mesh::Model::OpCode::configNetworkTransmitStatus
		);

	encoder.encode(countIntervalSteps);

	if(encoder.overflow()){
		Oscl::Error::Info::log(
			"%s: overflow!\n",
			OSCL_PRETTY_FUNCTION
			);
		return;
		}

	Oscl::BT::Mesh::Transport::Upper::TX::Api&
	transportApi	= _upperTransportTx;

	transportApi.sendSegmented(
		this,	// completeApi
		buffer,
		encoder.length(),
		dst,	// dst
		0,	// appKeyIndex (ignored for device key)
		txTTL
		);
	}


void	Part::sendConfigModelAppStatus(
			uint8_t		statusCode
			) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n"
		"",
		__PRETTY_FUNCTION__
		);
	#endif

	uint8_t	buffer[maxAccessPduSizeWithTransMic32];

	/*	4.3.1.1 Key indexes

		To pack two key indexes into three octets, 8 LSbs of first
		key index value are packed into the first octet, placing the
		remaining 4 MSbs into 4 LSbs of the second octet.

		The first 4 LSbs of the second 12-bit key index are packed
		into the 4 MSbs of the second octet with the remaining 8
		MSbs into the third octet.

		**	NOTE! IGNORE Figure 4.3 and follow the bizzare
			description above!
	 */

	Oscl::Encoder::LE::Base
	leEncoder(
		buffer,
		sizeof(buffer)
		);
	Oscl::Encoder::Api&	encoder	= leEncoder;

	Oscl::BT::Mesh::Model::OpCode::encode(
		encoder,
		Oscl::BT::Mesh::Model::OpCode::configModelAppStatus
		);

	encoder.encode(statusCode);
	encoder.encode(_elementAddr);
	encoder.encode(_keyIndicie0);
	encoder.encode(_keyIndicie1);
	if(_sigModelID){
		encoder.encode(_modelIdentifier);
		}
	else {
		encoder.encode(_companyIdentifier);
		encoder.encode(_modelIdentifier);
		}

	if(encoder.overflow()){
		Oscl::Error::Info::log(
			"%s: overflow!\n",
			OSCL_PRETTY_FUNCTION
			);
		return;
		}

	Oscl::BT::Mesh::Transport::Upper::TX::Api&
	transportApi	= _upperTransportTx;

	transportApi.sendSegmented(
		this,	// completeApi
		buffer,
		encoder.length(),
		_srcAddress,	// dst
		0,	// appKeyIndex (ignored for device key)
		txTTL
		);
	}

void	Part::sendConfigBeaconStatus(
			uint16_t	dst,
			uint8_t		state
			) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n"
		"",
		__PRETTY_FUNCTION__
		);
	#endif

	uint8_t	buffer[maxAccessPduSizeWithTransMic32];

	/*	4.3.1.1 Key indexes

		To pack two key indexes into three octets, 8 LSbs of first
		key index value are packed into the first octet, placing the
		remaining 4 MSbs into 4 LSbs of the second octet.

		The first 4 LSbs of the second 12-bit key index are packed
		into the 4 MSbs of the second octet with the remaining 8
		MSbs into the third octet.

		**	NOTE! IGNORE Figure 4.3 and follow the bizzare
			description above!
	 */

	Oscl::Encoder::LE::Base
	leEncoder(
		buffer,
		sizeof(buffer)
		);
	Oscl::Encoder::Api&	encoder	= leEncoder;

	Oscl::BT::Mesh::Model::OpCode::encode(
		encoder,
		Oscl::BT::Mesh::Model::OpCode::configBeaconStatus
		);

	encoder.encode(state);

	if(encoder.overflow()){
		Oscl::Error::Info::log(
			"%s: overflow!\n",
			OSCL_PRETTY_FUNCTION
			);
		return;
		}

	Oscl::BT::Mesh::Transport::Upper::TX::Api&
	transportApi	= _upperTransportTx;

	transportApi.sendSegmented(
		this,	// completeApi
		buffer,
		encoder.length(),
		dst,
		0,	// appKeyIndex (ignored for device key)
		txTTL
		);
	}

void	Part::sendConfigSigModelAppList(
			uint16_t	dst,
			uint8_t		status,
			uint16_t	elementAddress,
			uint16_t	sigModelID,
			unsigned	nAppKeys,
			const void*	appKeys
			) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n"
		"",
		__PRETTY_FUNCTION__
		);
	#endif

	uint8_t	buffer[maxAccessPduSizeWithTransMic32];

	Oscl::Encoder::LE::Base
	leEncoder(
		buffer,
		sizeof(buffer)
		);
	Oscl::Encoder::Api&	encoder	= leEncoder;

	Oscl::BT::Mesh::Model::OpCode::encode(
		encoder,
		Oscl::BT::Mesh::Model::OpCode::configSigModelAppList
		);

	unsigned
	offset	= ((nAppKeys * 3)/2);

	unsigned
	nAppKeyOctets	= offset + (nAppKeys % 2);

	encoder.encode(status);
	encoder.encode(elementAddress);
	encoder.encode(sigModelID);

	if(appKeys){
		encoder.copyIn(
			appKeys,
			nAppKeyOctets
			);
		}

	if(encoder.overflow()){
		Oscl::Error::Info::log(
			"%s: overflow!\n",
			OSCL_PRETTY_FUNCTION
			);
		return;
		}

	Oscl::BT::Mesh::Transport::Upper::TX::Api&
	transportApi	= _upperTransportTx;

	transportApi.sendSegmented(
		this,	// completeApi
		buffer,
		encoder.length(),
		dst,
		0,	// appKeyIndex (ignored for device key)
		txTTL
		);
	}

void	Part::resetIfReset() noexcept{
	if(_resetting){
		_nodeApi.nodeReset();
		}
	}

void	Part::success() noexcept{
	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif
	resetIfReset();
	}

void	Part::successOnBehalfOfLPN() noexcept{
	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif
	resetIfReset();
	}

void	Part::failedTimeout() noexcept{
	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif
	resetIfReset();
	}

void	Part::failedBusy() noexcept{
	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif
	resetIfReset();
	}

void	Part::failedOutOfResources() noexcept{
	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif
	resetIfReset();
	}

void	Part::failedTooBig() noexcept{
	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif
	resetIfReset();
	}

const Oscl::BT::Mesh::Key::Result::Var::State&	Part::bind(uint16_t appKeyIndex) noexcept{
	return Oscl::BT::Mesh::Key::Result::Var::getOutOfResources();
	}

const Oscl::BT::Mesh::Key::Result::Var::State&	Part::unbind(uint16_t appKeyIndex) noexcept{
	return Oscl::BT::Mesh::Key::Result::Var::getOutOfResources();
	}

Oscl::BT::Mesh::Model::Key::Api*	Part::getModelKeyApi() noexcept{
	return 0;
	}

bool	Part::sigModelIdMatch(uint16_t modelID) const noexcept{
	return (modelID == 0x0000); // Configuration Server
	}

bool	Part::vendorModelIdMatch(uint16_t vendorID, uint16_t modelID) const noexcept{
	return false;
	}

bool	Part::modelSupportsAppKeyBinding() const noexcept{
	return false;
	}

Oscl::BT::Mesh::Publication::Api*	Part::getPublicationApi() noexcept{
	return 0;
	}

Oscl::BT::Mesh::Subscription::Api*	Part::getSubscriptionApi() noexcept{
	return 0;
	}

void	Part::configAppKeyAddResultSuccess() noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif

	sendConfigAppKeyStatus(
		Oscl::BT::Mesh::Message::Foundation::Status::success,
		_keyIndicie0,
		_keyIndicie1,
		_keyIndicie2,
		_srcAddress
		);
	}

void	Part::configAppKeyAddResultInvalidIndex() noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif

	sendConfigAppKeyStatus(
		Oscl::BT::Mesh::Message::Foundation::Status::invalidAppKeyIndex,
		_keyIndicie0,
		_keyIndicie1,
		_keyIndicie2,
		_srcAddress
		);

	}

void	Part::configAppKeyAddResultNoSuchIndex() noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif

	sendConfigAppKeyStatus(
		Oscl::BT::Mesh::Message::Foundation::Status::invalidAppKeyIndex,
		_keyIndicie0,
		_keyIndicie1,
		_keyIndicie2,
		_srcAddress
		);
	}

void	Part::configAppKeyAddResultIndexAlreadyExists() noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif

	sendConfigAppKeyStatus(
		Oscl::BT::Mesh::Message::Foundation::Status::keyIndexAlreadyStored,
		_keyIndicie0,
		_keyIndicie1,
		_keyIndicie2,
		_srcAddress
		);
	}

void	Part::configAppKeyAddResultOutOfResources() noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif

	sendConfigAppKeyStatus(
		Oscl::BT::Mesh::Message::Foundation::Status::insufficientResources,
		_keyIndicie0,
		_keyIndicie1,
		_keyIndicie2,
		_srcAddress
		);
	}

void	Part::configAppKeyAddResultInvalidState() noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif

	sendConfigAppKeyStatus(
		Oscl::BT::Mesh::Message::Foundation::Status::unspecifiedError,
		_keyIndicie0,
		_keyIndicie1,
		_keyIndicie2,
		_srcAddress
		);
	}

void	Part::configAppKeyUpdateResultSuccess() noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif

	sendConfigAppKeyStatus(
		Oscl::BT::Mesh::Message::Foundation::Status::success,
		_keyIndicie0,
		_keyIndicie1,
		_keyIndicie2,
		_srcAddress
		);
	}

void	Part::configAppKeyUpdateResultInvalidIndex() noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif

	sendConfigAppKeyStatus(
		Oscl::BT::Mesh::Message::Foundation::Status::invalidAppKeyIndex,
		_keyIndicie0,
		_keyIndicie1,
		_keyIndicie2,
		_srcAddress
		);

	}

void	Part::configAppKeyUpdateResultNoSuchIndex() noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif

	sendConfigAppKeyStatus(
		Oscl::BT::Mesh::Message::Foundation::Status::invalidAppKeyIndex,
		_keyIndicie0,
		_keyIndicie1,
		_keyIndicie2,
		_srcAddress
		);
	}

void	Part::configAppKeyUpdateResultIndexAlreadyExists() noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif

	sendConfigAppKeyStatus(
		Oscl::BT::Mesh::Message::Foundation::Status::keyIndexAlreadyStored,
		_keyIndicie0,
		_keyIndicie1,
		_keyIndicie2,
		_srcAddress
		);
	}

void	Part::configAppKeyUpdateResultOutOfResources() noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif

	sendConfigAppKeyStatus(
		Oscl::BT::Mesh::Message::Foundation::Status::insufficientResources,
		_keyIndicie0,
		_keyIndicie1,
		_keyIndicie2,
		_srcAddress
		);
	}

void	Part::configAppKeyUpdateResultInvalidState() noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif

	sendConfigAppKeyStatus(
		Oscl::BT::Mesh::Message::Foundation::Status::cannotUpdate,
		_keyIndicie0,
		_keyIndicie1,
		_keyIndicie2,
		_srcAddress
		);
	}

void	Part::configAppKeyDeleteResultSuccess() noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif

	sendConfigAppKeyStatus(
		Oscl::BT::Mesh::Message::Foundation::Status::success,
		_keyIndicie0,
		_keyIndicie1,
		_keyIndicie2,
		_srcAddress
		);
	}

void	Part::configAppKeyDeleteResultInvalidIndex() noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif

	sendConfigAppKeyStatus(
		Oscl::BT::Mesh::Message::Foundation::Status::invalidAppKeyIndex,
		_keyIndicie0,
		_keyIndicie1,
		_keyIndicie2,
		_srcAddress
		);

	}

void	Part::configAppKeyDeleteResultNoSuchIndex() noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif

	sendConfigAppKeyStatus(
		Oscl::BT::Mesh::Message::Foundation::Status::invalidAppKeyIndex,
		_keyIndicie0,
		_keyIndicie1,
		_keyIndicie2,
		_srcAddress
		);
	}

void	Part::configAppKeyDeleteResultIndexAlreadyExists() noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif

	sendConfigAppKeyStatus(
		Oscl::BT::Mesh::Message::Foundation::Status::keyIndexAlreadyStored,
		_keyIndicie0,
		_keyIndicie1,
		_keyIndicie2,
		_srcAddress
		);
	}

void	Part::configAppKeyDeleteResultOutOfResources() noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif

	sendConfigAppKeyStatus(
		Oscl::BT::Mesh::Message::Foundation::Status::insufficientResources,
		_keyIndicie0,
		_keyIndicie1,
		_keyIndicie2,
		_srcAddress
		);
	}

void	Part::configAppKeyDeleteResultInvalidState() noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif

	sendConfigAppKeyStatus(
		Oscl::BT::Mesh::Message::Foundation::Status::unspecifiedError,
		_keyIndicie0,
		_keyIndicie1,
		_keyIndicie2,
		_srcAddress
		);
	}

bool	Part::configAppKeyGetItem(
			unsigned		index,
			const uint8_t	key[16],
			uint8_t			id
			) noexcept{

	unsigned
	offset	= ((_nAppKeys * 3)/2);

	if((offset + 2) > sizeof(_buffer)){
		// Halt iteration
		_resultState	= &Oscl::BT::Mesh::Key::Result::Var::getOutOfResources();
		return true;
		}

	if(_nAppKeys & 0x0001){
		packOdd(
			index,
			&_buffer[offset]
			);
		}
	else {
		packEven(
			index,
			&_buffer[offset]
			);
		}

	++_nAppKeys;

	return false;
	}

void	Part::configAppKeyGetResultSuccess() noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif

	sendConfigAppKeyList(
		Oscl::BT::Mesh::Message::Foundation::Status::success,
		_keyIndicie0,
		_keyIndicie1,
		_srcAddress
		);
	}

void	Part::configAppKeyGetResultInvalidIndex() noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif

	sendConfigAppKeyList(
		Oscl::BT::Mesh::Message::Foundation::Status::invalidAppKeyIndex,
		_keyIndicie0,
		_keyIndicie1,
		_srcAddress
		);

	}

void	Part::configAppKeyGetResultNoSuchIndex() noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif

	sendConfigAppKeyList(
		Oscl::BT::Mesh::Message::Foundation::Status::invalidAppKeyIndex,
		_keyIndicie0,
		_keyIndicie1,
		_srcAddress
		);
	}

void	Part::configAppKeyGetResultIndexAlreadyExists() noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif

	sendConfigAppKeyList(
		Oscl::BT::Mesh::Message::Foundation::Status::keyIndexAlreadyStored,
		_keyIndicie0,
		_keyIndicie1,
		_srcAddress
		);
	}

void	Part::configAppKeyGetResultOutOfResources() noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif

	sendConfigAppKeyList(
		Oscl::BT::Mesh::Message::Foundation::Status::insufficientResources,
		_keyIndicie0,
		_keyIndicie1,
		_srcAddress
		);
	}

void	Part::configAppKeyGetResultInvalidState() noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif

	sendConfigAppKeyList(
		Oscl::BT::Mesh::Message::Foundation::Status::unspecifiedError,
		_keyIndicie0,
		_keyIndicie1,
		_srcAddress
		);
	}

void	Part::configModelAppBindResultSuccess() noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif

	sendConfigModelAppStatus(
		Oscl::BT::Mesh::Message::Foundation::Status::success
		);
	}

void	Part::configModelAppBindResultInvalidIndex() noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif

	sendConfigModelAppStatus(
		Oscl::BT::Mesh::Message::Foundation::Status::invalidAppKeyIndex
		);
	}

void	Part::configModelAppBindResultNoSuchIndex() noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif

	sendConfigModelAppStatus(
		Oscl::BT::Mesh::Message::Foundation::Status::invalidAppKeyIndex
		);
	}

void	Part::configModelAppBindResultIndexAlreadyExists() noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif

	sendConfigModelAppStatus(
		Oscl::BT::Mesh::Message::Foundation::Status::keyIndexAlreadyStored
		);
	}

void	Part::configModelAppBindResultOutOfResources() noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif

	sendConfigModelAppStatus(
		Oscl::BT::Mesh::Message::Foundation::Status::insufficientResources
		);
	}

void	Part::configModelAppBindResultInvalidState() noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif

	sendConfigModelAppStatus(
		Oscl::BT::Mesh::Message::Foundation::Status::unspecifiedError
		);
	}

bool	Part::configModelAppBindItem(
			unsigned		index,
			const uint8_t	key[16],
			uint8_t			id
			) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n"
		"\tindex: 0x%3.3X\n"
		"\t_appKeyIndex: 0x%3.3X\n"
		"\tid: 0x%2.2X\n"
		"",
		__PRETTY_FUNCTION__,
		index,
		_appKeyIndex,
		id
		);
	#endif

	if(index == _appKeyIndex){
		return true;
		}

	return false;
	}

bool	Part::configSigModelAppBindItem(
			unsigned		index
			) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n"
		"\tindex: 0x%3.3X\n"
		"",
		__PRETTY_FUNCTION__,
		index
		);
	#endif

	unsigned
	offset	= ((_nAppKeys * 3)/2);

	if((offset + 2) > sizeof(_buffer)){
		// Halt iteration
		_resultState	= &Oscl::BT::Mesh::Key::Result::Var::getOutOfResources();
		return true;
		}

	if(_nAppKeys & 0x0001){
		packOdd(
			index,
			&_buffer[offset]
			);
		}
	else {
		packEven(
			index,
			&_buffer[offset]
			);
		}

	++_nAppKeys;

	return false;
	}
