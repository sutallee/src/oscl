/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_bluetooth_mesh_model_rx_apih_
#define _oscl_bluetooth_mesh_model_rx_apih_

#include <stdint.h>
#include "oscl/bluetooth/mesh/model/api.h"

/** */
namespace Oscl {
/** */
namespace BT {
/** */
namespace Mesh {
/** */
namespace Model {
/** */
namespace RX {

/** This interface is used to transfer received Access
	PDUs from the Element containing the Model to the
	Model.
 */
class Api {
	public:
		/** */
		virtual void	stop() noexcept=0;

		/** This operation is used to determine if
			the model subscribes to the specified
			destination address. This is only called
			by the containing Element if the destination
			address is not a unicast address. 

			param [in]	dst	The destination address in question.

			RETURN: true if the model is subscribed
			to the destination address (dst).
		 */
		virtual bool	wantsDestination(uint16_t dst) noexcept=0;

		/** RETURN: zero if there is no matching SIG model.
		 */
		virtual Oscl::BT::Mesh::Model::Api*	getSigModelApi(uint16_t modelID) noexcept=0;

		/** RETURN: zero if there is no matching vendor model.
		 */
		virtual Oscl::BT::Mesh::Model::Api*	getVendorModelApi(uint16_t cid, uint16_t modelID) noexcept=0;

		/** Process the decrypted Access Message
			The appKeyIndex is greater than 0x0FFF
			if this message is was decrypted using
			the DevKey.
		 */
		virtual void	processAccessMessage(
							const void*	frame,
							unsigned	length,
							uint16_t	src,
							uint16_t	dst,
							uint16_t	appKeyIndex
							) noexcept=0;
	};

}
}
}
}
}

#endif
