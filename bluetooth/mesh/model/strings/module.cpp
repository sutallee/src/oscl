/*
   Copyright (C) 2020 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "module.h"
#include "oscl/bluetooth/mesh/message/foundation/status/value.h"

/** */
const char*	Oscl::BT::Mesh::Model::Strings::sigModelString(uint16_t sigModelID) noexcept {

	switch(sigModelID){
		case 0x0000:
			return "Configuration Server";
		case 0x0001:
			return "Configuration Client";
		case 0x0002:
			return "Health Server";
		case 0x0003:
			return "Health Client";
		case 0x1000:
			return "Generic OnOff Server";
		case 0x1001:
			return "Generic OnOff Client";
		case 0x1002:
			return "Generic Level Server";
		case 0x1003:
			return "Generic Level Client";
		case 0x1004:
			return "Generic Default Transition Time Server";
		case 0x1005:
			return "Generic Default Transition Time Client";
		case 0x1006:
			return "Generic Power OnOff Server";
		case 0x1007:
			return "Generic Power OnOff Setup Server";
		case 0x1008:
			return "Generic Power OnOff Client";
		case 0x1009:
			return "Generic Power Level Server";
		case 0x100A:
			return "Generic Power Level Setup Server";
		case 0x100B:
			return "Generic Power Level Client";
		case 0x100C:
			return "Generic Battery Server";
		case 0x100D:
			return "Generic Battery Client";
		case 0x100E:
			return "Generic Location Server";
		case 0x100F:
			return "Generic Location Setup Server";
		case 0x1010:
			return "Generic Location Client";
		case 0x1011:
			return "Generic Admin Property Server";
		case 0x1012:
			return "Generic Manufacturer Property Server";
		case 0x1013:
			return "Generic User Property Server";
		case 0x1014:
			return "Generic Client Property Server";
		case 0x1015:
			return "Generic Property Client";
		case 0x1100:
			return "Sensor Server";
		case 0x1101:
			return "Sensor Setup Server";
		case 0x1102:
			return "Sensor Client";
		case 0x1200:
			return "Time Server";
		case 0x1201:
			return "Time Setup Server";
		case 0x1202:
			return "Time Client";
		case 0x1203:
			return "Scene Server";
		case 0x1204:
			return "Scene Setup Server";
		case 0x1205:
			return "Scene Client";
		case 0x1206:
			return "Scheduler Server";
		case 0x1207:
			return "Scheduler Setup Server";
		case 0x1208:
			return "Scheduler Client";
		case 0x1300:
			return "Light Lightness Server";
		case 0x1301:
			return "Light Lightness Setup Server";
		case 0x1302:
			return "Light Lightness Client";
		case 0x1303:
			return "Light CTL Server";
		case 0x1304:
			return "Light CTL Setup Server";
		case 0x1305:
			return "Light CTL Client";
		case 0x1306:
			return "Light CTL Temperature Server";
		case 0x1307:
			return "Light HSL Server";
		case 0x1308:
			return "Light HSL Setup Server";
		case 0x1309:
			return "Light HSL Client";
		case 0x130A:
			return "Light HSL Hue Server";
		case 0x130B:
			return "Light HSL Saturation Server";
		case 0x130C:
			return "Light xyL Server";
		case 0x130D:
			return "Light xyL Setup Server";
		case 0x130E:
			return "Light xyL Client";
		case 0x130F:
			return "Light LC Server";
		case 0x1310:
			return "Light LC Setup Server";
		case 0x1311:
			return "Light LC Client";
		default:
			return "Unknown";
		}
	}

const char*	Oscl::BT::Mesh::Model::Strings::foundationModelMessageStatus(uint8_t status) noexcept{

	switch(status){
		case Oscl::BT::Mesh::Message::Foundation::Status::success:
			return "Success";
		case Oscl::BT::Mesh::Message::Foundation::Status::invalidAddress:
			return "Invalid Address";
		case Oscl::BT::Mesh::Message::Foundation::Status::invalidModel:
			return "Invalid Model";
		case Oscl::BT::Mesh::Message::Foundation::Status::invalidAppKeyIndex:
			return "Invalid AppKey Index";
		case Oscl::BT::Mesh::Message::Foundation::Status::invalidNetKeyIndex:
			return "Invalid NetKey Index";
		case Oscl::BT::Mesh::Message::Foundation::Status::insufficientResources:
			return "Insufficient Resources";
		case Oscl::BT::Mesh::Message::Foundation::Status::keyIndexAlreadyStored:
			return "Key Index Already Stored";
		case Oscl::BT::Mesh::Message::Foundation::Status::invalidPublishParameters:
			return "Invalid Publish Parameters";
		case Oscl::BT::Mesh::Message::Foundation::Status::notASubscribeModel:
			return "Not A Subscribe Model";
		case Oscl::BT::Mesh::Message::Foundation::Status::storageFailure:
			return "Storage Failure";
		case Oscl::BT::Mesh::Message::Foundation::Status::featureNotSupported:
			return "Feature Not Supported";
		case Oscl::BT::Mesh::Message::Foundation::Status::cannotUpdate:
			return "Cannot Update";
		case Oscl::BT::Mesh::Message::Foundation::Status::cannotRemove:
			return "Cannot Remove";
		case Oscl::BT::Mesh::Message::Foundation::Status::cannotBind:
			return "Cannot Bind";
		case Oscl::BT::Mesh::Message::Foundation::Status::temporarilyUnableToChangeState:
			return "Temporarily Unable To Change State";
		case Oscl::BT::Mesh::Message::Foundation::Status::cannotSet:
			return "Cannot Set";
		case Oscl::BT::Mesh::Message::Foundation::Status::unspecifiedError:
			return "Unspecified Error";
		case Oscl::BT::Mesh::Message::Foundation::Status::invalidBinding:
			return "Invalid Binding";
		default:
			return "Unknown Status";
		};
	}

