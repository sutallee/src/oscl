/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <new>
#include "part.h"
#include "oscl/encoder/le/base.h"
#include "oscl/endian/decoder/linear/part.h"
#include "oscl/encoder/be/base.h"
#include "oscl/error/info.h"
#include "oscl/bluetooth/mesh/address/virtual.h"
#include "oscl/bluetooth/mesh/address/types.h"
#include "oscl/bluetooth/mesh/address/values.h"
#include "oscl/bluetooth/mesh/publication/utils.h"
#include "oscl/bluetooth/mesh/model/opcode/decoder.h"
#include "oscl/bluetooth/mesh/model/opcode/encoder.h"
#include "oscl/bluetooth/mesh/constants.h"

using namespace Oscl::BT::Mesh::Model::Health::Server;

//#define DEBUG_TRACE
//#define DEBUG_TRACE_TX_STATUS

Part::Part(
	Oscl::Pdu::Memory::Api&			freeStoreApi,
	Oscl::Timer::Factory::Api&		timerFactoryApi,
	Oscl::BT::Mesh::
	Publication::Persist::Api&		publicationPersistApi,
	Oscl::BT::Mesh::Node::Api&		nodeApi,
	Oscl::BT::Mesh::Element::
	Symbiont::Kernel::Api&			kernelApi,
	Oscl::BT::Mesh::
	Transport::Lower::TX::Api&		lowerTransportApi,
	Oscl::BT::Mesh::Network::Api&	networkApi,
	Oscl::BT::Mesh::Model::Health::
	Server::Context::Api&			contextApi,
	Oscl::BT::Mesh::Subscription::
	Persist::Creator::Api&			subscriptionPersistApi,
	Oscl::BT::Mesh::Model::Key::
	Persist::Creator::Api&			keyPersistApi,
	uint16_t						elementAddress,
	uint16_t						companyID,
	bool							allowSegmentedStatus
	) noexcept:
		_freeStoreApi(freeStoreApi),
		_timerFactoryApi(timerFactoryApi),
		_contextApi(contextApi),
		_allowSegmentedStatus(allowSegmentedStatus),
		_appServerContext(
			*this,
			&Part::processAccessMessage,
			&Part::sigModelIdMatch,
			&Part::vendorModelIdMatch,
			&Part::modelSupportsAppKeyBinding,
			&Part::getPublicationApi,
			&Part::getPublicationAppKeyIndex,
			&Part::getSubscriptionApi,
			&Part::getSubscriptionServerApi,
			&Part::stop
			),
		_subscriptionServerPart(
			subscriptionPersistApi,
			kernelApi.getSubscriptionRegisterApi()
			),
		_appServerPart(
			_appServerContext,
			freeStoreApi,
			nodeApi,
			lowerTransportApi,
			networkApi,
			keyPersistApi,
			elementAddress
			),
		_publicationPart(
            *this,
            timerFactoryApi,
            publicationPersistApi
			),
		_rxHealthFaultGet(
			*this,
			&Part::rxHealthFaultGet
			),
		_rxHealthFaultClear(
			*this,
			&Part::rxHealthFaultClear
			),
		_rxHealthFaultClearUnacknowledged(
			*this,
			&Part::rxHealthFaultClearUnacknowledged
			),
		_rxHealthFaultTest(
			*this,
			&Part::rxHealthFaultTest
			),
		_rxHealthFaultTestUnacknowledged(
			*this,
			&Part::rxHealthFaultTestUnacknowledged
			),
		_rxHealthPeriodGet(
			*this,
			&Part::rxHealthPeriodGet
			),
		_rxHealthPeriodSet(
			*this,
			&Part::rxHealthPeriodSet
			),
		_rxHealthPeriodSetUnacknowledged(
			*this,
			&Part::rxHealthPeriodSetUnacknowledged
			),
		_rxHealthAttentionGet(
			*this,
			&Part::rxHealthAttentionGet
			),
		_rxHealthAttentionSet(
			*this,
			&Part::rxHealthAttentionSet
			),
		_rxHealthAttentionSetUnacknowledged(
			*this,
			&Part::rxHealthAttentionSetUnacknowledged
			),
		_healthFaultObserver(
			*this,
			&Part::healthFaultObserverUpdate
			),
		_companyID(companyID),
		_testID(0)
		{

	_messageHandlers.put(&_rxHealthFaultGet);
	_messageHandlers.put(&_rxHealthFaultClear);
	_messageHandlers.put(&_rxHealthFaultClearUnacknowledged);
	_messageHandlers.put(&_rxHealthFaultTest);
	_messageHandlers.put(&_rxHealthFaultTestUnacknowledged);
	_messageHandlers.put(&_rxHealthPeriodGet);
	_messageHandlers.put(&_rxHealthPeriodSet);
	_messageHandlers.put(&_rxHealthPeriodSetUnacknowledged);
	_messageHandlers.put(&_rxHealthAttentionGet);
	_messageHandlers.put(&_rxHealthAttentionSet);
	_messageHandlers.put(&_rxHealthAttentionSetUnacknowledged);

	_contextApi.getSubjectApi().attach(_healthFaultObserver);
	}

Part::~Part() noexcept{
	}

Oscl::BT::Mesh::Model::RX::Item&	Part::getModelRxItem() noexcept{
	return _appServerPart;
	}

Oscl::BT::Mesh::Model::Api&	Part::getModelApi() noexcept{
	return _appServerPart.getModelApi();
	}

void	Part::processAccessMessage(
			const void*	frame,
			unsigned	length,
			uint16_t	src,
			uint16_t	dst,
			uint16_t	appKeyIndex
			) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);

	Oscl::Error::Info::hexDump(
		frame,
		length
		);
	#endif

	Oscl::Endian::Decoder::Linear::Part
	leDecoder(
		frame,
		length
		);

	Oscl::Decoder::Api&	decoder	= leDecoder.le();

	uint32_t
	opcode	= Oscl::BT::Mesh::Model::OpCode::decode(decoder);

	using namespace Oscl::BT::Mesh::Model::OpCode;

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s:\n"
		"\topcode: (0x%4.4X)\n",
		__PRETTY_FUNCTION__,
		opcode
		);
	#endif

	const uint8_t*	p	= (const uint8_t*)frame;

	unsigned
	remaining	= decoder.remaining();

	unsigned
	payloadOffset	= length - remaining;

	for(
		Oscl::BT::Mesh::Message::Item*	item	= _messageHandlers.first();
		item;
		item	= _messageHandlers.next(item)
		){
		bool
		handled	= item->receive(
					opcode,
					src,
					dst,
					&p[payloadOffset],
					remaining,
					appKeyIndex
					);
		if(handled){
			return;
			}
		}
	}

bool	Part::rxHealthFaultGet(
			uint32_t	opcode,
			uint16_t	srcAddress,
			uint16_t	dstAddress,
			const void*	frame,
			unsigned	length,
			uint16_t	appKeyIndex
			) noexcept{

	if(opcode != Oscl::BT::Mesh::Model::OpCode::healthFaultGet){
		return false;
		}

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n"
		"",
		__PRETTY_FUNCTION__
		);
	#endif

	sendHealthFaultStatus(
		appKeyIndex,
		srcAddress
		);

	return true;
	}

bool	Part::rxHealthFaultClear(
			uint32_t	opcode,
			uint16_t	srcAddress,
			uint16_t	dstAddress,
			const void*	frame,
			unsigned	length,
			uint16_t	appKeyIndex
			) noexcept{

	if(opcode != Oscl::BT::Mesh::Model::OpCode::healthFaultClear){
		return false;
		}

	uint8_t	companyID;

	Oscl::Endian::Decoder::Linear::Part
	leDecoder(
		frame,
		length
		);

	Oscl::Decoder::Api&
	decoder	= leDecoder.le();

	decoder.decode(companyID);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			OSCL_PRETTY_FUNCTION
			);
		return true;
		}

	_contextApi.clearRegisteredFaults();

	sendHealthFaultStatus(
		appKeyIndex,
		srcAddress
		);

	return true;
	}

bool	Part::rxHealthFaultClearUnacknowledged(
			uint32_t	opcode,
			uint16_t	srcAddress,
			uint16_t	dstAddress,
			const void*	frame,
			unsigned	length,
			uint16_t	appKeyIndex
			) noexcept{

	if(opcode != Oscl::BT::Mesh::Model::OpCode::healthFaultClearUnacknowledged){
		return false;
		}

	uint8_t	targetState;
	uint8_t	tid;
	uint8_t	transitionTime;
	uint8_t	delay;

	Oscl::Endian::Decoder::Linear::Part
	leDecoder(
		frame,
		length
		);

	Oscl::Decoder::Api&	decoder	= leDecoder.le();

	decoder.decode(targetState);
	decoder.decode(tid);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			OSCL_PRETTY_FUNCTION
			);
		return true;
		}

	decoder.decode(transitionTime);

	bool	transitionTimeIsValid	= true;

	if(decoder.underflow()){
		transitionTimeIsValid	= false;
		}

	if(transitionTimeIsValid){
		decoder.decode(delay);
		if(decoder.underflow()){
			Oscl::Error::Info::log(
				"%s: underflow! Delay value not present.\n",
				OSCL_PRETTY_FUNCTION
				);
			return true;
			}
		}

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n"
		"\tOnOff State: 0x%2.2X\n"
		"\tTID: 0x%2.2X\n"
		"",
		__PRETTY_FUNCTION__,
		targetState,
		tid
		);
	#endif

	if(transitionTimeIsValid){
		#ifdef DEBUG_TRACE
		Oscl::Error::Info::log(
			"%s\n"
			"\tTransition Time: 0x%2.2X\n"
			"\tDelay: 0x%2.2X\n"
			"",
			OSCL_PRETTY_FUNCTION,
			transitionTime,
			delay
			);
		#endif
		}

//	_presentOnOffState	= targetState;

	return true;
	}

bool	Part::rxHealthFaultTest(
			uint32_t	opcode,
			uint16_t	srcAddress,
			uint16_t	dstAddress,
			const void*	frame,
			unsigned	length,
			uint16_t	appKeyIndex
			) noexcept{

	if(opcode != Oscl::BT::Mesh::Model::OpCode::healthFaultTest){
		return false;
		}

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n"
		"",
		__PRETTY_FUNCTION__
		);
	#endif

	return true;
	}

bool	Part::rxHealthFaultTestUnacknowledged(
			uint32_t	opcode,
			uint16_t	srcAddress,
			uint16_t	dstAddress,
			const void*	frame,
			unsigned	length,
			uint16_t	appKeyIndex
			) noexcept{

	if(opcode != Oscl::BT::Mesh::Model::OpCode::healthFaultTestUnacknowledged){
		return false;
		}

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n"
		"",
		__PRETTY_FUNCTION__
		);
	#endif

	return true;
	}

bool	Part::rxHealthPeriodGet(
			uint32_t	opcode,
			uint16_t	srcAddress,
			uint16_t	dstAddress,
			const void*	frame,
			unsigned	length,
			uint16_t	appKeyIndex
			) noexcept{

	if(opcode != Oscl::BT::Mesh::Model::OpCode::healthPeriodGet){
		return false;
		}

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n"
		"",
		__PRETTY_FUNCTION__
		);
	#endif

	return true;
	}

bool	Part::rxHealthPeriodSet(
			uint32_t	opcode,
			uint16_t	srcAddress,
			uint16_t	dstAddress,
			const void*	frame,
			unsigned	length,
			uint16_t	appKeyIndex
			) noexcept{

	if(opcode != Oscl::BT::Mesh::Model::OpCode::healthPeriodSet){
		return false;
		}

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n"
		"",
		__PRETTY_FUNCTION__
		);
	#endif

	return true;
	}

bool	Part::rxHealthPeriodSetUnacknowledged(
			uint32_t	opcode,
			uint16_t	srcAddress,
			uint16_t	dstAddress,
			const void*	frame,
			unsigned	length,
			uint16_t	appKeyIndex
			) noexcept{

	if(opcode != Oscl::BT::Mesh::Model::OpCode::healthPeriodSetUnacknowledged){
		return false;
		}

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n"
		"",
		__PRETTY_FUNCTION__
		);
	#endif

	return true;
	}

bool	Part::rxHealthAttentionGet(
			uint32_t	opcode,
			uint16_t	srcAddress,
			uint16_t	dstAddress,
			const void*	frame,
			unsigned	length,
			uint16_t	appKeyIndex
			) noexcept{

	if(opcode != Oscl::BT::Mesh::Model::OpCode::healthAttentionGet){
		return false;
		}


	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n"
		"",
		__PRETTY_FUNCTION__
		);
	#endif

	sendHealthAttentionStatus(
		appKeyIndex,
		srcAddress
		);

	return true;
	}

bool	Part::rxHealthAttentionSet(
			uint32_t	opcode,
			uint16_t	srcAddress,
			uint16_t	dstAddress,
			const void*	frame,
			unsigned	length,
			uint16_t	appKeyIndex
			) noexcept{

	if(opcode != Oscl::BT::Mesh::Model::OpCode::healthAttentionSet){
		return false;
		}

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n"
		"",
		OSCL_PRETTY_FUNCTION
		);
	#endif

	uint8_t	attentionTimer;

	Oscl::Endian::Decoder::Linear::Part
	leDecoder(
		frame,
		length
		);

	Oscl::Decoder::Api&	decoder	= leDecoder.le();

	decoder.decode(attentionTimer);

	_contextApi.setHealthAttentionTimer(attentionTimer);

	sendHealthAttentionStatus(
		appKeyIndex,
		srcAddress
		);

	return true;
	}

bool	Part::rxHealthAttentionSetUnacknowledged(
			uint32_t	opcode,
			uint16_t	srcAddress,
			uint16_t	dstAddress,
			const void*	frame,
			unsigned	length,
			uint16_t	appKeyIndex
			) noexcept{

	if(opcode != Oscl::BT::Mesh::Model::OpCode::healthAttentionSetUnacknowledged){
		return false;
		}

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n"
		"",
		__PRETTY_FUNCTION__
		);
	#endif

	return true;
	}

void	Part::sendHealthCurrentStatus(
			uint16_t	appKeyIndex,
			uint16_t	dstAddress,
			bool		sendSegmented
			) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s: NOT IMPLEMENTED\n",
		__PRETTY_FUNCTION__
		);
	#endif

	}

void	Part::sendHealthFaultStatus(
			uint16_t	appKeyIndex,
			uint16_t	dstAddress,
			bool		sendSegmented
			) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif

	uint8_t	buffer[maxAccessPduSizeWithTransMic32];

	Oscl::Encoder::LE::Base
	leEncoder(
		buffer,
		sizeof(buffer)
		);
	Oscl::Encoder::Api&	encoder	= leEncoder;

	Oscl::BT::Mesh::Model::OpCode::encode(
		encoder,
		Oscl::BT::Mesh::Model::OpCode::healthFaultStatus
		);

	encoder.encode(_testID);
	encoder.encode(_companyID);

	unsigned
	remaining	= sizeof(buffer)-encoder.length();

	unsigned
	nFaults	= _contextApi.copyRegisteredFaultArray(
				&buffer[encoder.length()],
				remaining
				);

	if(nFaults){
		encoder.skip(nFaults);
		}

	if(encoder.overflow()){
		Oscl::Error::Info::log(
			"%s: overflow!\n",
			OSCL_PRETTY_FUNCTION
			);
		return;
		}

	Oscl::BT::Mesh::Transport::Upper::TX::Api&
	transportApi	= _appServerPart.getUpperTransportApi();

	if(_allowSegmentedStatus && sendSegmented){
	transportApi.sendSegmented(
		this,	// completeApi
		buffer,
		encoder.length(),
		dstAddress,	// dst
		appKeyIndex,
		0xFF	// use Default TTL
		);
	}
	else {
		transportApi.sendUnsegmented(
			this,	// completeApi
			buffer,
			encoder.length(),
			dstAddress,	// dst
			appKeyIndex,
			0xFF	// use Default TTL
			);
		}
	}

void	Part::sendHealthPeriodStatus(
			uint16_t	appKeyIndex,
			uint16_t	dstAddress,
			bool		sendSegmented
			) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s: NOT IMPLEMENTED\n",
		__PRETTY_FUNCTION__
		);
	#endif

	}

void	Part::sendHealthAttentionStatus(
			uint16_t	appKeyIndex,
			uint16_t	dstAddress,
			bool		sendSegmented
			) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif

	// opcode: 0x8007

	uint8_t	buffer[maxAccessPduSizeWithTransMic32];

	Oscl::Encoder::LE::Base
	leEncoder(
		buffer,
		sizeof(buffer)
		);
	Oscl::Encoder::Api&	encoder	= leEncoder;

	Oscl::BT::Mesh::Model::OpCode::encode(
		encoder,
		Oscl::BT::Mesh::Model::OpCode::healthAttentionStatus
		);

	uint8_t
	attentionStatus	= _contextApi.getHealthAttentionTimer();

	encoder.encode(attentionStatus);

	Oscl::BT::Mesh::Transport::Upper::TX::Api&
	transportApi	= _appServerPart.getUpperTransportApi();

	if(_allowSegmentedStatus && sendSegmented){
	transportApi.sendSegmented(
		this,	// completeApi
		buffer,
		encoder.length(),
		dstAddress,	// dst
		appKeyIndex,
		0xFF	// use Default TTL
		);
	}
	else {
		transportApi.sendUnsegmented(
			this,	// completeApi
			buffer,
			encoder.length(),
			dstAddress,	// dst
			appKeyIndex,
			0xFF	// use Default TTL
			);
		}
	}

void	Part::success() noexcept{
	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif
	}

void	Part::successOnBehalfOfLPN() noexcept{
	#if defined(DEBUG_TRACE) || defined (DEBUG_TRACE_TX_STATUS)
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif
	}

void	Part::failedTimeout() noexcept{
	#if defined(DEBUG_TRACE) || defined (DEBUG_TRACE_TX_STATUS)
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif
	}

void	Part::failedBusy() noexcept{
	#if defined(DEBUG_TRACE) || defined (DEBUG_TRACE_TX_STATUS)
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif
	}

void	Part::failedOutOfResources() noexcept{
	#if defined(DEBUG_TRACE) || defined (DEBUG_TRACE_TX_STATUS)
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif
	}

void	Part::failedTooBig() noexcept{
	#if defined(DEBUG_TRACE) || defined (DEBUG_TRACE_TX_STATUS)
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif
	}

void	Part::healthFaultObserverUpdate(bool& state) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s: state: %s, address: 0x%4.4X\n",
		__PRETTY_FUNCTION__,
		state?"ON":"OFF",
		_publicationPersistApi.address()
		);
	#endif

	_publicationPart.publish();
	}

bool	Part::sigModelIdMatch(uint16_t modelID) const noexcept{
	return (modelID == 0x0002); // Health Server
	}

bool	Part::vendorModelIdMatch(uint16_t vendorID, uint16_t modelID) const noexcept{
	return false;
	}

bool	Part::modelSupportsAppKeyBinding() const noexcept{
	return true;
	}

Oscl::BT::Mesh::Publication::Api*	Part::getPublicationApi() noexcept{
	return &_publicationPart;
	}

uint16_t	Part::getPublicationAppKeyIndex() const noexcept{
	return _publicationPart.getPublicationAppKeyIndex();
	}

Oscl::BT::Mesh::Subscription::Api*	Part::getSubscriptionApi() noexcept{
	return &_subscriptionServerPart;
	}

Oscl::BT::Mesh::Subscription::Server::Api*	Part::getSubscriptionServerApi() noexcept{
	return &_subscriptionServerPart;
	}

void	Part::stop() noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s:\n",
		__PRETTY_FUNCTION__
		);
	#endif

	_publicationPart.stop();

	_contextApi.getSubjectApi().detach(_healthFaultObserver);
	}

void	Part::publish(
			uint16_t	appKeyIndex,
			uint16_t	dstAddress,
			uint8_t		ttl,
			const void*	label,
			bool		retransmitting
			) noexcept{

	uint8_t	buffer[maxAccessPduSizeWithTransMic32];

	Oscl::Encoder::LE::Base
	leEncoder(
		buffer,
		sizeof(buffer)
		);
	Oscl::Encoder::Api&	encoder	= leEncoder;

	Oscl::BT::Mesh::Model::OpCode::encode(
		encoder,
		Oscl::BT::Mesh::Model::OpCode::healthCurrentStatus
		);

	encoder.encode(_testID);
	encoder.encode(_companyID);

	unsigned
	remaining	= sizeof(buffer)-encoder.length();

	unsigned
	nFaults	= _contextApi.copyCurrentFaultArray(
				&buffer[encoder.length()],
				remaining
				);

	if(nFaults){
		encoder.skip(nFaults);
		}

	if(encoder.overflow()){
		Oscl::Error::Info::log(
			"%s: overflow!\n",
			OSCL_PRETTY_FUNCTION
			);
		return;
		}

	Oscl::BT::Mesh::Transport::Upper::TX::Api&
	transportApi	= _appServerPart.getUpperTransportApi();

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s:\n"
		"\t_publishAddress: 0x%4.4X\n"
		"\t_pubAppKeyIndex: 0x%4.4X\n"
		"\t_publishTTL: 0x%2.2X\n"
		"",
		__PRETTY_FUNCTION__,
		dstAddress,
		appKeyIndex,
		ttl
		);
	#endif

	bool
	isUnicastAddress	= Oscl::BT::Mesh::Address::isUnicastAddress(dstAddress);

	if(_allowSegmentedStatus && isUnicastAddress){

		transportApi.sendSegmented(
			this,	// completeApi
			buffer,
			encoder.length(),
			dstAddress,
			appKeyIndex,
			ttl
			);
	}
	else {

		transportApi.sendUnsegmented(
			this,	// completeApi
			buffer,
			encoder.length(),
			dstAddress,
			appKeyIndex,
			ttl,
			label,
			label?16:0
			);
		}
	}
