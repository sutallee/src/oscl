/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_bluetooth_mesh_model_health_server_context_apih_
#define _oscl_bluetooth_mesh_model_health_server_context_apih_

#include <stdint.h>
#include "oscl/update/subject/api.h"

/** */
namespace Oscl {
/** */
namespace BT {
/** */
namespace Mesh {
/** */
namespace Model {
/** */
namespace Health {
/** */
namespace Server {
/** */
namespace Context {

/** This abstract interface is used by the Generic OnOff
	Server to communicate and control a specific application
	context. The implementation is platform and application
	specific.
 */
class Api {
	public:
		/**	This operation is invoked by the stack
			to get a copy of the first faultArraySize
			octets of the registered faults.

			RETURN: number of faults written to the
			array if any.
		 */
		virtual unsigned	copyRegisteredFaultArray(
								void*		faultArray,
								unsigned	faultArraySize
								) noexcept=0;

		/**	This operation is invoked by the stack
			to get a copy of the first faultArraySize
			octets of the current faults.

			RETURN: number of faults written to the
			array if any.
		 */
		virtual unsigned	copyCurrentFaultArray(
								void*		faultArray,
								unsigned	faultArraySize
								) noexcept=0;

		/**	This operation is invoked by the stack
			clear all currently registered faults.
		 */
		virtual void	clearRegisteredFaults() noexcept=0;

		/*	This operation is invoked by the stack
			clear all currently registered vendor faults.
		 */
//		virtual void	clearVendorFaults() noexcept=0;

		/** */
		virtual void	setHealthAttentionTimer(uint8_t seconds) noexcept=0;

		/** */
		virtual uint8_t	getHealthAttentionTimer() const noexcept=0;

		/** */
		virtual Oscl::Update::Subject::Api<bool>&	getSubjectApi() noexcept=0;
	};

}
}
}
}
}
}
}

#endif
