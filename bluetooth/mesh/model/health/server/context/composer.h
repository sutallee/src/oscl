/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_bluetooth_mesh_model_health_server_context_composerh_
#define _oscl_bluetooth_mesh_model_health_server_context_composerh_

#include "api.h"

/** */
namespace Oscl {
/** */
namespace BT {
/** */
namespace Mesh {
/** */
namespace Model {
/** */
namespace Health {
/** */
namespace Server {
/** */
namespace Context {

/** */
template <class Context>
class Composer : public Api {
	private:
		/** */
		Context&	_context;

		/**	*/
		unsigned	(Context::*_copyRegisteredFaultArray)(
						void*		faultArray,
						unsigned	faultArraySize
						);

		/**	*/
		unsigned	(Context::*_copyCurrentFaultArray)(
						void*		faultArray,
						unsigned	faultArraySize
						);

		/** */
		void	(Context::*_clearRegisteredFaults)();

		/**	*/
		Oscl::Update::Subject::Api<bool>&	(Context::*_getSubjectApi)();

		/** */
		void	(Context::*_setHealthAttentionTimer)(uint8_t seconds);

		/** */
		uint8_t	(Context::*_getHealthAttentionTimer)() const;

	public:
		/** */
		Composer(
			Context&	context,
			unsigned	(Context::*copyRegisteredFaultArray)(
							void*		faultArray,
							unsigned	faultArraySize
							),
			unsigned	(Context::*copyCurrentFaultArray)(
							void*		faultArray,
							unsigned	faultArraySize
							),
			void		(Context::*clearRegisteredFaults)(),
			Oscl::Update::Subject::Api<bool>&	(Context::*getSubjectApi)(),
			void		(Context::*setHealthAttentionTimer)(uint8_t seconds),
			uint8_t		(Context::*getHealthAttentionTimer)() const
			) noexcept;

		/** */
		unsigned	copyRegisteredFaultArray(
						void*		faultArray,
						unsigned	faultArraySize
						) noexcept;

		/**	*/
		unsigned	copyCurrentFaultArray(
						void*		faultArray,
						unsigned	faultArraySize
						) noexcept;

		/** */
		void	clearRegisteredFaults() noexcept;

		/**	*/
		Oscl::Update::Subject::Api<bool>&	getSubjectApi() noexcept;

		/** */
		void	setHealthAttentionTimer(uint8_t seconds) noexcept;

		/** */
		uint8_t	getHealthAttentionTimer() const noexcept;

	};

template <class Context>
Composer<Context>::Composer(
	Context&	context,
	unsigned	(Context::*copyRegisteredFaultArray)(
					void*		faultArray,
					unsigned	faultArraySize
					),
	unsigned	(Context::*copyCurrentFaultArray)(
					void*		faultArray,
					unsigned	faultArraySize
					),
	void		(Context::*clearRegisteredFaults)(),
	Oscl::Update::Subject::Api<bool>&	(Context::*getSubjectApi)(),
	void		(Context::*setHealthAttentionTimer)(uint8_t seconds),
	uint8_t		(Context::*getHealthAttentionTimer)() const
	)noexcept:
		_context(context),
		_copyRegisteredFaultArray(copyRegisteredFaultArray),
		_copyCurrentFaultArray(copyCurrentFaultArray),
		_clearRegisteredFaults(clearRegisteredFaults),
		_getSubjectApi(getSubjectApi),
		_setHealthAttentionTimer(setHealthAttentionTimer),
		_getHealthAttentionTimer(getHealthAttentionTimer)
		{
	}

template <class Context>
unsigned	Composer<Context>::copyRegisteredFaultArray(
				void*		faultArray,
				unsigned	faultArraySize
				) noexcept{
	return (_context.*_copyRegisteredFaultArray)(
			faultArray,
			faultArraySize
			);
	}

template <class Context>
unsigned	Composer<Context>::copyCurrentFaultArray(
				void*		faultArray,
				unsigned	faultArraySize
				) noexcept{
	return (_context.*_copyCurrentFaultArray)(
			faultArray,
			faultArraySize
			);
	}

template <class Context>
void	Composer<Context>::clearRegisteredFaults() noexcept{
	(_context.*_clearRegisteredFaults)();
	}

template <class Context>
Oscl::Update::Subject::Api<bool>&	Composer<Context>::getSubjectApi() noexcept{
	return (_context.*_getSubjectApi)();
	}

template <class Context>
void	Composer<Context>::setHealthAttentionTimer(uint8_t seconds) noexcept{
	(_context.*_setHealthAttentionTimer)(seconds);
	}

template <class Context>
uint8_t	Composer<Context>::getHealthAttentionTimer() const noexcept{
	return (_context.*_getHealthAttentionTimer)();
	}
}
}
}
}
}
}
}

#endif
