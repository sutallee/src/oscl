/*
   Copyright (C) 2020 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_bluetooth_mesh_model_battery_server_context_apih_
#define _oscl_bluetooth_mesh_model_battery_server_context_apih_

#include "oscl/change/subject/api.h"

/** */
namespace Oscl {
/** */
namespace BT {
/** */
namespace Mesh {
/** */
namespace Model {
/** */
namespace Battery {
/** */
namespace Server {
/** */
namespace Context {

/** This abstract interface is used by the Generic Battery
	Server to communicate and control a specific application
	context. The implementation is platform and application
	specific.
 */
class Api {
	public:
		/** This operation is used to get the
			battery status.

			batteryLevel:
				0x00-0x64 (0-100)	precentage of charge level
				0xFF				charge level is unknown

			timeToDischarge:	24-bit significant
				0x000000-0xFFFFFE	remaining time in minutes
				0xFFFFFF			remaining time unknown

			timeToCharge:	24-bit significant
				0x000000-0xFFFFFE	remaining time in minutes
				0xFFFFFF			remaining time unknown

			presence:	2-bits significant
				0x00	battery is not present
				0x01	battery is present and removable
				0x02	battery is present and non-removable
				0x03	battery presence is unknown

			indicator:	2-bits significant
				0x00	battery charge is Critically Low level.
				0x01	battery charge is Low Level.
				0x02	battery charge is Good Level.
				0x03	battery charge is unknown.

			charging:	2-bits significant
				0x00	battery is not chargable
				0x01	battery is chargable and not charging.
				0x02	battery is chargable and is charging.
				0x03	battery charging state is unknown.

			serviceability:	2-bits significant
				0x00	reserved
				0x01	battery does not require service.
				0x02	battery requires service.
				0x03	battery serviceability unknown.
		 */
		virtual void	getStatus(
							uint8_t&	batteryLevel,
							uint32_t&	timeToDischarge,
							uint32_t&	timeToCharge,
							uint8_t&	presence,
							uint8_t&	indicator,
							uint8_t&	charging,
							uint8_t&	serviceability
							) noexcept=0;

		/** */
		virtual Oscl::Change::Subject::Api&	getSubjectApi() noexcept=0;
	};

}
}
}
}
}
}
}

#endif
