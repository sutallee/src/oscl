/*
   Copyright (C) 2020 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <new>
#include "part.h"
#include "oscl/endian/decoder/linear/part.h"
#include "oscl/encoder/le/base.h"
#include "oscl/encoder/be/base.h"
#include "oscl/error/info.h"
#include "oscl/bluetooth/mesh/address/virtual.h"
#include "oscl/bluetooth/mesh/address/types.h"
#include "oscl/bluetooth/mesh/address/values.h"
#include "oscl/bluetooth/mesh/publication/utils.h"
#include "oscl/bluetooth/mesh/model/opcode/decoder.h"
#include "oscl/bluetooth/mesh/model/opcode/encoder.h"
#include "oscl/bluetooth/mesh/constants.h"

using namespace Oscl::BT::Mesh::Model::Battery::Server;

//#define DEBUG_TRACE
//#define DEBUG_TRACE_TX_STATUS

Part::Part(
	Oscl::Pdu::Memory::Api&			freeStoreApi,
	Oscl::Timer::Factory::Api&		timerFactoryApi,
	Oscl::BT::Mesh::
	Publication::Persist::Api&		publicationPersistApi,
	Oscl::BT::Mesh::Node::Api&		nodeApi,
	Oscl::BT::Mesh::Element::
	Symbiont::Kernel::Api&			kernelApi,
	Oscl::BT::Mesh::
	Transport::Lower::TX::Api&		lowerTransportApi,
	Oscl::BT::Mesh::Network::Api&	networkApi,
	Oscl::BT::Mesh::Model::Battery::
	Server::Context::Api&			contextApi,
	Oscl::BT::Mesh::Subscription::
	Persist::Creator::Api&			subscriptionPersistApi,
	Oscl::BT::Mesh::Model::Key::
	Persist::Creator::Api&			keyPersistApi,
	uint16_t						elementAddress,
	bool							allowSegmentedStatus
	) noexcept:
		_freeStoreApi(freeStoreApi),
		_timerFactoryApi(timerFactoryApi),
		_contextApi(contextApi),
		_allowSegmentedStatus(allowSegmentedStatus),
		_appServerContext(
			*this,
			&Part::processAccessMessage,
			&Part::sigModelIdMatch,
			&Part::vendorModelIdMatch,
			&Part::modelSupportsAppKeyBinding,
			&Part::getPublicationApi,
			&Part::getPublicationAppKeyIndex,
			&Part::getSubscriptionApi,
			&Part::getSubscriptionServerApi,
			&Part::stop
			),
		_subscriptionServerPart(
			subscriptionPersistApi,
			kernelApi.getSubscriptionRegisterApi()
			),
		_appServerPart(
			_appServerContext,
			freeStoreApi,
			nodeApi,
			lowerTransportApi,
			networkApi,
			keyPersistApi,
			elementAddress
			),
		_publicationPart(
            *this,
            timerFactoryApi,
            publicationPersistApi
			),
		_rxGenericBatteryGet(
			*this,
			&Part::rxGenericBatteryGet
			),
		_batteryObserver(
			*this,
			&Part::batteryObserverUpdate
			)
		{

	_messageHandlers.put(&_rxGenericBatteryGet);

	_contextApi.getSubjectApi().attach(_batteryObserver);
	}

Part::~Part() noexcept{
	}

Oscl::BT::Mesh::Model::RX::Item&	Part::getModelRxItem() noexcept{
	return _appServerPart;
	}

Oscl::BT::Mesh::Model::Api&	Part::getModelApi() noexcept{
	return _appServerPart.getModelApi();
	}

void	Part::processAccessMessage(
			const void*	frame,
			unsigned	length,
			uint16_t	src,
			uint16_t	dst,
			uint16_t	appKeyIndex
			) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION
		);

	Oscl::Error::Info::hexDump(
		frame,
		length
		);
	#endif

	Oscl::Endian::Decoder::Linear::Part
	leDecoder(
		frame,
		length
		);

	Oscl::Decoder::Api&	decoder	= leDecoder.le();

	uint32_t
	opcode	= Oscl::BT::Mesh::Model::OpCode::decode(decoder);

	using namespace Oscl::BT::Mesh::Model::OpCode;

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s:\n"
		"\topcode: (0x%4.4X)\n",
		OSCL_PRETTY_FUNCTION,
		opcode
		);
	#endif

	const uint8_t*	p	= (const uint8_t*)frame;

	unsigned
	remaining	= decoder.remaining();

	unsigned
	payloadOffset	= length - remaining;

	for(
		Oscl::BT::Mesh::Message::Item*	item	= _messageHandlers.first();
		item;
		item	= _messageHandlers.next(item)
		){
		bool
		handled	= item->receive(
					opcode,
					src,
					dst,
					&p[payloadOffset],
					remaining,
					appKeyIndex
					);
		if(handled){
			return;
			}
		}
	}

bool	Part::rxGenericBatteryGet(
			uint32_t	opcode,
			uint16_t	srcAddress,
			uint16_t	dstAddress,
			const void*	frame,
			unsigned	length,
			uint16_t	appKeyIndex
			) noexcept{

	if(opcode != Oscl::BT::Mesh::Model::OpCode::genericBatteryGet){
		return false;
		}

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n"
		"",
		OSCL_PRETTY_FUNCTION
		);
	#endif

	sendGenericBatteryStatus(
		appKeyIndex,
		srcAddress,
		Oscl::BT::Mesh::Address::isUnicastAddress(dstAddress)
		);

	return true;
	}

void	Part::sendGenericBatteryStatus(
			uint16_t	appKeyIndex,
			uint16_t	dstAddress,
			bool		sendSegmented
			) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif

	uint8_t		batteryLevel;
	uint32_t	timeToDischarge;
	uint32_t	timeToCharge;
	uint8_t		presence;
	uint8_t		indicator;
	uint8_t		charging;
	uint8_t		serviceability;

	_contextApi.getStatus(
		batteryLevel,
		timeToDischarge,
		timeToCharge,
		presence,
		indicator,
		charging,
		serviceability
		);

	uint8_t
	flags	=
			((presence & 0x03)			<< 0)
		|	((indicator & 0x03)			<< 2)
		|	((charging & 0x03)			<< 4)
		|	((serviceability & 0x03)	<< 6)
		;

	uint8_t	buffer[maxAccessPduSizeWithTransMic32];

	Oscl::Encoder::LE::Base
	leEncoder(
		buffer,
		sizeof(buffer)
		);
	Oscl::Encoder::Api&	encoder	= leEncoder;

	Oscl::BT::Mesh::Model::OpCode::encode(
		encoder,
		Oscl::BT::Mesh::Model::OpCode::genericBatteryStatus
		);

	encoder.encode(batteryLevel);
	encoder.encode24Bit(timeToDischarge);
	encoder.encode24Bit(timeToCharge);
	encoder.encode(flags);

	if(encoder.overflow()){
		Oscl::Error::Info::log(
			"%s: overflow!\n",
			OSCL_PRETTY_FUNCTION
			);
		return;
		}

	Oscl::BT::Mesh::Transport::Upper::TX::Api&
	transportApi	= _appServerPart.getUpperTransportApi();

	if(_allowSegmentedStatus && sendSegmented){
		transportApi.sendSegmented(
			this,	// completeApi
			buffer,
			encoder.length(),
			dstAddress,	// dst
			appKeyIndex,
			0xFF	// use Default TTL
			);
		}
	else {
		transportApi.sendUnsegmented(
			this,	// completeApi
			buffer,
			encoder.length(),
			dstAddress,	// dst
			appKeyIndex,
			0xFF	// use Default TTL
			);
		}
	}

void	Part::success() noexcept{
	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif
	}

void	Part::successOnBehalfOfLPN() noexcept{
	#if defined(DEBUG_TRACE) || defined (DEBUG_TRACE_TX_STATUS)
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif
	}

void	Part::failedTimeout() noexcept{
	#if defined(DEBUG_TRACE) || defined (DEBUG_TRACE_TX_STATUS)
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif
	}

void	Part::failedBusy() noexcept{
	#if defined(DEBUG_TRACE) || defined (DEBUG_TRACE_TX_STATUS)
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif
	}

void	Part::failedOutOfResources() noexcept{
	#if defined(DEBUG_TRACE) || defined (DEBUG_TRACE_TX_STATUS)
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif
	}

void	Part::failedTooBig() noexcept{
	#if defined(DEBUG_TRACE) || defined (DEBUG_TRACE_TX_STATUS)
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif
	}

void	Part::batteryObserverUpdate() noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif

	_publicationPart.publish();
	}

bool	Part::sigModelIdMatch(uint16_t modelID) const noexcept{
	return (modelID == 0x100C); // Generic Battery Server
	}

bool	Part::vendorModelIdMatch(uint16_t vendorID, uint16_t modelID) const noexcept{
	return false;
	}

bool	Part::modelSupportsAppKeyBinding() const noexcept{
	return true;
	}

Oscl::BT::Mesh::Publication::Api*	Part::getPublicationApi() noexcept{
	return &_publicationPart;
	}

uint16_t	Part::getPublicationAppKeyIndex() const noexcept{
	return _publicationPart.getPublicationAppKeyIndex();
	}

Oscl::BT::Mesh::Subscription::Api*	Part::getSubscriptionApi() noexcept{
	return &_subscriptionServerPart;
	}

Oscl::BT::Mesh::Subscription::Server::Api*	Part::getSubscriptionServerApi() noexcept{
	return &_subscriptionServerPart;
	}

void	Part::stop() noexcept{
	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s:\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif

	_publicationPart.stop();

	_contextApi.getSubjectApi().detach(_batteryObserver);
	}

void	Part::publish(
			uint16_t	appKeyIndex,
			uint16_t	dstAddress,
			uint8_t		ttl,
			const void*	label,
			bool		retransmitting
			) noexcept{

	const unsigned
	labelLen	= label?16:0;

	/* 4.2.2.1 Publish Address
		The Publish Address state determines the destination
		address in messages sent by a model. The publish
		address shall be the unassigned address, a unicast
		address, a Label UUID, or a group address.

		If the publish address of the model is the unassigned
		address, the model is inactive: it does not send any
		unsolicited messages out and can only send a response
		message to an incoming acknowledged message.
	 */
	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif

	uint8_t		batteryLevel;
	uint32_t	timeToDischarge;
	uint32_t	timeToCharge;
	uint8_t		presence;
	uint8_t		indicator;
	uint8_t		charging;
	uint8_t		serviceability;

	_contextApi.getStatus(
		batteryLevel,
		timeToDischarge,
		timeToCharge,
		presence,
		indicator,
		charging,
		serviceability
		);

	uint8_t
	flags	=
			((presence & 0x03)			<< 0)
		|	((indicator & 0x03)			<< 2)
		|	((charging & 0x03)			<< 4)
		|	((serviceability & 0x03)	<< 6)
		;

	uint8_t	buffer[maxAccessPduSizeWithTransMic32];

	Oscl::Encoder::LE::Base
	leEncoder(
		buffer,
		sizeof(buffer)
		);
	Oscl::Encoder::Api&	encoder	= leEncoder;

	Oscl::BT::Mesh::Model::OpCode::encode(
		encoder,
		Oscl::BT::Mesh::Model::OpCode::genericBatteryStatus
		);

	encoder.encode(batteryLevel);
	encoder.encode24Bit(timeToDischarge);
	encoder.encode24Bit(timeToCharge);
	encoder.encode(flags);

	if(encoder.overflow()){
		Oscl::Error::Info::log(
			"%s: overflow!\n",
			OSCL_PRETTY_FUNCTION
			);
		return;
		}

	Oscl::BT::Mesh::Transport::Upper::TX::Api&
	transportApi	= _appServerPart.getUpperTransportApi();

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s:\n"
		"\t_publishAddress: 0x%4.4X\n"
		"\t_pubAppKeyIndex: 0x%4.4X\n"
		"\t_publishTTL: 0x%2.2X\n"
		"",
		OSCL_PRETTY_FUNCTION,
		dstAddress,
		appKeyIndex,
		ttl
		);
	#endif

	bool
	isUnicastAddress	= Oscl::BT::Mesh::Address::isUnicastAddress(dstAddress);

	if(_allowSegmentedStatus && isUnicastAddress){
		transportApi.sendSegmented(
			this,	// completeApi
			buffer,
			encoder.length(),
			dstAddress,
			appKeyIndex,
			ttl
			);
		}
	else {
		transportApi.sendUnsegmented(
			this,	// completeApi
			buffer,
			encoder.length(),
			dstAddress,
			appKeyIndex,
			ttl,
			label,
			labelLen
			);
		}
	}

