/*
   Copyright (C) 2020 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_bluetooth_mesh_model_battery_server_context_statush_
#define _oscl_bluetooth_mesh_model_battery_server_context_statush_

#include <stdint.h>

/** */
namespace Oscl {
/** */
namespace BT {
/** */
namespace Mesh {
/** */
namespace Model {
/** */
namespace Battery {
/** */
namespace Server {

/** This structure contains the battery status information.
 */
struct Status {
	/** Charge level
		0x00-0x64 (0-100)	precentage of charge level
		0xFF				charge level is unknown
	 */
	uint8_t		batteryLevel;

	/** Time to discharge in minutes.
		Only the least significant 24-bits are used.
		0x000000-0xFFFFFE	remaining time in minutes
		0xFFFFFF			remaining time unknown

	 */
	uint32_t	timeToDischarge;

	/**	Time to charge in minutes
		Only the least significant 24-bits are used.
		0x000000-0xFFFFFE	remaining time in minutes
		0xFFFFFF			remaining time unknown
	 */
	uint32_t	timeToCharge;

	/** Battery presence indication.
		Only the least significant 2-bits are used.
		0x00	battery is not present
		0x01	battery is present and removable
		0x02	battery is present and non-removable
		0x03	battery presence is unknown
	 */
	uint8_t		presence;

	/** Charge indicator
		Only the least significant 2-bits are used.
		0x00	battery charge is Critically Low level.
		0x01	battery charge is Low Level.
		0x02	battery charge is Good Level.
		0x03	battery charge is unknown.
	 */
	uint8_t		indicator;

	/** Charging state
		Only the least significant 2-bits are used.
		0x00	battery is not chargable
		0x01	battery is chargable and not charging.
		0x02	battery is chargable and is charging.
		0x03	battery charging state is unknown.
	 */
	uint8_t		charging;

	/** Servicability
		Only the least significant 2-bits are used.
		0x00	reserved
		0x01	battery does not require service.
		0x02	battery requires service.
		0x03	battery serviceability unknown.
	 */
	uint8_t		serviceability;

	Status() noexcept:
		batteryLevel(~0),
		timeToDischarge(~0),
		timeToCharge(~0),
		presence(~0),
		indicator(~0),
		charging(~0),
		serviceability(~0)
		{
		}
	};

}
}
}
}
}
}

#endif
