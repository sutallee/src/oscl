/*
   Copyright (C) 2020 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_bluetooth_mesh_model_battery_client_context_apih_
#define _oscl_bluetooth_mesh_model_battery_client_context_apih_

#include <stdint.h>
#include "oscl/update/subject/api.h"

/** */
namespace Oscl {
/** */
namespace BT {
/** */
namespace Mesh {
/** */
namespace Model {
/** */
namespace Battery {
/** */
namespace Client {
/** */
namespace Context {

/** This abstract interface is used by the Generic Battery
	Client to communicate and control a specific application
	context. The implementation is platform and application
	specific.
 */
class Api {
	public:
		/** This operation allows the context to record changes
			of state that are sent to this client.
		 */
		virtual void	recordStatus(
							uint16_t	src,
							uint16_t	dst,
							uint8_t		batteryLevel,
							uint32_t	timeToDischarge,
							uint32_t	timeToCharge,
							uint8_t		presence,
							uint8_t		indicator,
							uint8_t		charging,
							uint8_t		serviceability
							) noexcept=0;
	};

}
}
}
}
}
}
}

#endif
