/*
   Copyright (C) 2020 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

//#include <string.h>

#include "module.h"

using namespace Oscl::BT::Mesh::Model::Battery::Strings;

const char*	Oscl::BT::Mesh::Model::Battery::Strings::presence(uint8_t value) noexcept{
	switch(value){
		case 0x00:
			return "Not Present";
		case 0x01:
			return "Present and Removable";
		case 0x02:
			return "Present and Non-removable";
		case 0x03:
			return "Presence unknown";
		default:
			return "Invalid";
		}
	}

const char*	Oscl::BT::Mesh::Model::Battery::Strings::indicator(uint8_t value) noexcept{
	switch(value){
		case 0x00:
			return "Critically Low Level";
		case 0x01:
			return "Low Level";
		case 0x02:
			return "Good Level";
		case 0x03:
			return "Unknown";
		default:
			return "Invalid";
		}
	}

const char*	Oscl::BT::Mesh::Model::Battery::Strings::charging(uint8_t value) noexcept{
	switch(value){
		case 0x00:
			return "Not Chargable";
		case 0x01:
			return "Chargable and not charging";
		case 0x02:
			return "Chargable and charging";
		case 0x03:
			return "Unknown";
		default:
			return "Invalid";
		}
	}

const char*	Oscl::BT::Mesh::Model::Battery::Strings::serviceability(uint8_t value) noexcept{
	switch(value){
		case 0x00:
			return "Reserved Invalid";
		case 0x01:
			return "Does not require service";
		case 0x02:
			return "Requires service";
		case 0x03:
			return "Unknown";
		default:
			return "Invalid";
		}
	}

