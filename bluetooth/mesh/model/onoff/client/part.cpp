/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <new>
#include "part.h"
#include "oscl/encoder/le/base.h"
#include "oscl/endian/decoder/linear/part.h"
#include "oscl/encoder/be/base.h"
#include "oscl/error/info.h"
#include "oscl/bluetooth/mesh/address/virtual.h"
#include "oscl/bluetooth/mesh/address/types.h"
#include "oscl/bluetooth/mesh/address/values.h"
#include "oscl/bluetooth/mesh/publication/utils.h"
#include "oscl/bluetooth/mesh/model/opcode/decoder.h"
#include "oscl/bluetooth/mesh/model/opcode/encoder.h"
#include "oscl/bluetooth/mesh/constants.h"

using namespace Oscl::BT::Mesh::Model::OnOff::Client;

//#define DEBUG_TRACE_TX_STATUS
//#define DEBUG_TRACE
//#define ONOFF_CLIENT_MULTI_SEGMENT_PDU_TEST

Part::Part(
	Oscl::Pdu::Memory::Api&			freeStoreApi,
	Oscl::Timer::Factory::Api&		timerFactoryApi,
	Oscl::BT::Mesh::
	Publication::Persist::Api&		publicationPersistApi,
	Oscl::BT::Mesh::Node::Api&		nodeApi,
	Oscl::BT::Mesh::Element::
	Symbiont::Kernel::Api&			kernelApi,
	Oscl::BT::Mesh::
	Transport::Lower::TX::Api&		lowerTransportApi,
	Oscl::BT::Mesh::Network::Api&	networkApi,
	Oscl::BT::Mesh::Model::OnOff::
	Client::Context::Api&			contextApi,
	Oscl::BT::Mesh::Subscription::
	Persist::Creator::Api&			subscriptionPersistApi,
	Oscl::BT::Mesh::Model::Key::
	Persist::Creator::Api&			keyPersistApi,
	uint16_t						elementAddress,
	bool							publishUnacknowledged,
	bool							publishOnChange,
	bool							periodicPublicationEnabled
	) noexcept:
		_freeStoreApi(freeStoreApi),
		_timerFactoryApi(timerFactoryApi),
		_contextApi(contextApi),
		_publishUnacknowledged(publishUnacknowledged),
		_periodicPublicationEnabled(periodicPublicationEnabled),
		_appServerContext(
			*this,
			&Part::processAccessMessage,
			&Part::sigModelIdMatch,
			&Part::vendorModelIdMatch,
			&Part::modelSupportsAppKeyBinding,
			&Part::getPublicationApi,
			&Part::getPublicationAppKeyIndex,
			&Part::getSubscriptionApi,
			&Part::getSubscriptionServerApi,
			&Part::stop
			),
		_subscriptionServerPart(
			subscriptionPersistApi,
			kernelApi.getSubscriptionRegisterApi()
			),
		_appServerPart(
			_appServerContext,
			freeStoreApi,
			nodeApi,
			lowerTransportApi,
			networkApi,
			keyPersistApi,
			elementAddress
			),
		_publicationPart(
            *this,
            timerFactoryApi,
            publicationPersistApi
			),
		_rxGenericOnOffStatus(
			*this,
			&Part::rxGenericOnOffStatus
			),
		_onOffObserver(
			*this,
			&Part::onOffObserverUpdate
			),
		_presentOnOffState(0xFF), // unknown state
		_tid(0)
		{

	_messageHandlers.put(&_rxGenericOnOffStatus);

	if(publishOnChange){
		_contextApi.getSubjectApi().attach(_onOffObserver);
		}
	}

Part::~Part() noexcept{
	}

Oscl::BT::Mesh::Model::RX::Item&	Part::getModelRxItem() noexcept{
	return _appServerPart;
	}

Oscl::BT::Mesh::Model::Api&	Part::getModelApi() noexcept{
	return _appServerPart.getModelApi();
	}

void	Part::sendOnOffClientState(
			uint16_t	dst,
			bool		on
			) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s, dst: 0x%4.4X, on: %s\n",
		__PRETTY_FUNCTION__,
		dst,
		on?"true":"false"
		);
	#endif

	++_tid;

	_presentOnOffState	= on?0x01:0x00;

	_publicationPart.publish();
	}

void	Part::processAccessMessage(
			const void*	frame,
			unsigned	length,
			uint16_t	src,
			uint16_t	dst,
			uint16_t	appKeyIndex
			) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);

	Oscl::Error::Info::hexDump(
		frame,
		length
		);
	#endif

	Oscl::Endian::Decoder::Linear::Part
	leDecoder(
		frame,
		length
		);

	Oscl::Decoder::Api&	decoder	= leDecoder.le();

	uint32_t
	opcode	= Oscl::BT::Mesh::Model::OpCode::decode(decoder);

	using namespace Oscl::BT::Mesh::Model::OpCode;

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s:\n"
		"\topcode: (0x%4.4X)\n",
		__PRETTY_FUNCTION__,
		opcode
		);
	#endif

	const uint8_t*	p	= (const uint8_t*)frame;

	unsigned
	remaining	= decoder.remaining();

	unsigned
	payloadOffset	= length - remaining;

	for(
		Oscl::BT::Mesh::Message::Item*	item	= _messageHandlers.first();
		item;
		item	= _messageHandlers.next(item)
		){
		bool
		handled	= item->receive(
					opcode,
					src,
					dst,
					&p[payloadOffset],
					remaining,
					appKeyIndex
					);
		if(handled){
			return;
			}
		}
	}

bool	Part::rxGenericOnOffStatus(
			uint32_t	opcode,
			uint16_t	srcAddress,
			uint16_t	dstAddress,
			const void*	frame,
			unsigned	length,
			uint16_t	appKeyIndex
			) noexcept{

	if(opcode != Oscl::BT::Mesh::Model::OpCode::genericOnOffStatus){
		return false;
		}

	uint8_t	state;

	Oscl::Endian::Decoder::Linear::Part
	leDecoder(
		frame,
		length
		);

	Oscl::Decoder::Api&	decoder	= leDecoder.le();

	decoder.decode(state);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			OSCL_PRETTY_FUNCTION
			);
		return true;
		}

	_contextApi.recordStatus(
		srcAddress,
		dstAddress,
		state
		);

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s: src: 0x%4.4X, dst: 0x%4.4X, state: (0x%2.2X) %s\n"
		"",
		OSCL_PRETTY_FUNCTION,
		srcAddress,
		dstAddress,
		state,
		state?"ON":"OFF"
		);
	#endif

	return true;
	}

void	Part::success() noexcept{
	#if defined(DEBUG_TRACE) || defined (DEBUG_TRACE_TX_STATUS)
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif
	}

void	Part::successOnBehalfOfLPN() noexcept{
	#if defined(DEBUG_TRACE) || defined (DEBUG_TRACE_TX_STATUS)
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif
	}

void	Part::failedTimeout() noexcept{
	#if defined(DEBUG_TRACE) || defined (DEBUG_TRACE_TX_STATUS)
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif
	}

void	Part::failedBusy() noexcept{
	#if defined(DEBUG_TRACE) || defined (DEBUG_TRACE_TX_STATUS)
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif
	}

void	Part::failedOutOfResources() noexcept{
	#if defined(DEBUG_TRACE) || defined (DEBUG_TRACE_TX_STATUS)
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif
	}

void	Part::failedTooBig() noexcept{
	#if defined(DEBUG_TRACE) || defined (DEBUG_TRACE_TX_STATUS)
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif
	}

void	Part::onOffObserverUpdate(bool& state) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s: state: %s\n",
		__PRETTY_FUNCTION__,
		state?"ON":"OFF"
		);
	#endif

	_presentOnOffState	= state;

	++_tid;

	_publicationPart.publish();
	}

bool	Part::sigModelIdMatch(uint16_t modelID) const noexcept{
	return (modelID == 0x1001); // Generic OnOff Client
	}

bool	Part::vendorModelIdMatch(uint16_t vendorID, uint16_t modelID) const noexcept{
	return false;
	}

bool	Part::modelSupportsAppKeyBinding() const noexcept{
	return true;
	}

Oscl::BT::Mesh::Publication::Api*	Part::getPublicationApi() noexcept{
	return &_publicationPart;
	}

uint16_t	Part::getPublicationAppKeyIndex() const noexcept{
	return _publicationPart.getPublicationAppKeyIndex();
	}

Oscl::BT::Mesh::Subscription::Api*	Part::getSubscriptionApi() noexcept{
	return &_subscriptionServerPart;
	}

Oscl::BT::Mesh::Subscription::Server::Api*	Part::getSubscriptionServerApi() noexcept{
	return &_subscriptionServerPart;
	}

void	Part::stop() noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s:\n",
		__PRETTY_FUNCTION__
		);
	#endif

	_publicationPart.stop();

	_contextApi.getSubjectApi().detach(_onOffObserver);
	}

void	Part::publish(
			uint16_t	appKeyIndex,
			uint16_t	dstAddress,
			uint8_t		ttl,
			const void*	label,
			bool		retransmitting
			) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s, dst: 0x%4.4X, on: %s\n",
		__PRETTY_FUNCTION__,
		dst,
		on?"true":"false"
		);
	#endif

	if(!retransmitting){
		++_tid;
		}

	uint8_t	buffer[maxAccessPduSizeWithTransMic32];

	Oscl::Encoder::LE::Base
	leEncoder(
		buffer,
		sizeof(buffer)
		);
	Oscl::Encoder::Api&	encoder	= leEncoder;

	uint32_t
	opcode	= _publishUnacknowledged?
				Oscl::BT::Mesh::Model::OpCode::genericOnOffSetUnacknowledged:
				Oscl::BT::Mesh::Model::OpCode::genericOnOffSet
				;

	Oscl::BT::Mesh::Model::OpCode::encode(
		encoder,
		opcode
		);

	encoder.encode(_presentOnOffState);
	encoder.encode(_tid);

	// We do NOT currently encode the optional
	// Transition Time or Delay.

	bool
	isUnicastAddress	= Oscl::BT::Mesh::Address::isUnicastAddress(dstAddress);

	#ifdef ONOFF_CLIENT_MULTI_SEGMENT_PDU_TEST
	// This code is used to test segmented transmission
	// to a unicast address.
	//
	// This can be used to verify the segmentation
	// and reassembly mechanisms for Access messages
	// in general.
	//
	// If the unicast address is that of an LPN,
	// this test can be used to verify that
	// the LPN's Friend node responds to the
	// segments on-behalf-of the LPN.
	// 
	// We *assume* that the extra octets in the
	// extra large OnOff Set message will be ignored
	// by the OnOff Server recipient.
	// 
	if(isUnicastAddress){
		constexpr uint8_t	transitionNumberOfSteps		= 0;
		constexpr uint8_t	transitionStepResolution	= 0;
		static const uint8_t	transitionTime	= 
				(transitionNumberOfSteps << 2)
			|	((transitionStepResolution & 0x03)<< 0)
			;

		static const uint8_t	delay	= 0;

		encoder.encode(transitionTime);
		encoder.encode(delay);

		constexpr unsigned	nExtraSegments	= 3;

		for(unsigned i=0;i<nExtraSegments;++i){
			encoder.skip(segmentSize);
			}
		}
	#endif

	if(encoder.overflow()){
		Oscl::Error::Info::log(
			"%s: overflow!\n",
			OSCL_PRETTY_FUNCTION
			);
		return;
		}

	Oscl::BT::Mesh::Transport::Upper::TX::Api&
	transportApi	= _appServerPart.getUpperTransportApi();

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s:\n"
		"\t dstAddress: 0x%4.4X\n"
		"\t appKeyIndex: 0x%4.4X\n"
		"\t ttl: 0x%2.2X\n"
		"",
		__PRETTY_FUNCTION__,
		dstAddress,
		appKeyIndex,
		ttl
		);
	#endif

	const unsigned
	labelLen	= label?16:0;

	if(isUnicastAddress){
		transportApi.sendSegmented(
			this,	// completeApi
			buffer,
			encoder.length(),
			dstAddress,
			appKeyIndex,
			ttl
			);
		}
	else {
		transportApi.sendUnsegmented(
			this,	// completeApi
			buffer,
			encoder.length(),
			dstAddress,
			appKeyIndex,
			ttl,
			label,
			labelLen
			);
		}
	}

