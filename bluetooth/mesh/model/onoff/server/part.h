/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_bluetooth_mesh_model_onoff_server_parth_
#define _oscl_bluetooth_mesh_model_onoff_server_parth_

#include "oscl/queue/queue.h"
#include "oscl/bluetooth/mesh/node/api.h"
#include "oscl/bluetooth/mesh/model/rx/item.h"
#include "oscl/pdu/memory/api.h"
#include "oscl/timer/factory/api.h"
#include "oscl/done/operation.h"
#include "oscl/bluetooth/mesh/model/api.h"
#include "oscl/bluetooth/mesh/element/api.h"
#include "oscl/bluetooth/mesh/network/api.h"
#include "oscl/bluetooth/mesh/message/itemcomp.h"
#include "oscl/bluetooth/mesh/model/key/api.h"
#include "oscl/update/observer/composer.h"
#include "oscl/bluetooth/mesh/model/onoff/server/context/api.h"
#include "oscl/bluetooth/mesh/model/app/server/part.h"
#include "oscl/bluetooth/mesh/publication/persist/api.h"
#include "oscl/bluetooth/mesh/message/generic/onoff/server/part.h"
#include "oscl/bluetooth/mesh/state/generic/onoff/persist/api.h"
#include "oscl/bluetooth/mesh/state/generic/onoff/transition/part.h"
#include "oscl/bluetooth/mesh/publication/server/part.h"
#include "oscl/bluetooth/mesh/subscription/server/part.h"
#include "oscl/bluetooth/mesh/model/app/server/context/composer.h"

/** */
namespace Oscl {
/** */
namespace BT {
/** */
namespace Mesh {
/** */
namespace Model {
/** */
namespace OnOff {
/** */
namespace Server {

/** This is an implementation of the Bluetooth
	Mesh Generic OnOff Server model.
 */
class Part :
	private Oscl::BT::Mesh::State::Generic::OnOff::Api,
	private Oscl::BT::Mesh::Publication::Server::Context::Api
	{
	private:
		/** */
		Oscl::Timer::Factory::Api&	_timerFactoryApi;

		/** */
		Oscl::BT::Mesh::Model::OnOff::
		Server::Context::Api&			_contextApi;

		/** */
		Oscl::BT::Mesh::State::
		Generic::OnOff::Persist::Api&	_onOffStatePersistApi;

		/** */
		const bool						_allowSegmentedStatus;

	private:
		/** */
		Oscl::BT::Mesh::
		Model::App::Server::
		Context::Composer<Part>		_appServerContext;

	private:
		/** */
		Oscl::BT::Mesh::
		Subscription::Server::Part	_subscriptionServerPart;

	private:
		/** */
		Oscl::BT::Mesh::Model::
		App::Server::Part			_appServerPart;

	private:
		/** */
		Oscl::BT::Mesh::
		Publication::Server::Part	_publicationPart;

	private:
		/** */
		Oscl::Timer::Api&			_onOffTidPeriodicTimerApi;

	private:
		/** */
		Oscl::Timer::Api&					_onOffTransitionTimerApi;

		/** */
		Oscl::BT::Mesh::State::
		Generic::OnOff::Transition::Part	_transitionAdaptor;
	
	private:
		/** */
		Oscl::BT::Mesh::Message::
		Generic::OnOff::Server::Part	_genericOnOffTransceiver;

	private:
		/** */
		Oscl::Update::Observer::
		Composer<Part,bool>			_onOffObserver;

	public:
		/** */
		Part(
			Oscl::Pdu::Memory::Api&			freeStoreApi,
			Oscl::Timer::Factory::Api&		timerFactoryApi,
			Oscl::BT::Mesh::
			Publication::Persist::Api&		publicationPersistApi,
			Oscl::BT::Mesh::Node::Api&		nodeApi,
			Oscl::BT::Mesh::Element::
			Symbiont::Kernel::Api&			kernelApi,
			Oscl::BT::Mesh::
			Transport::Lower::TX::Api&		lowerTransportApi,
			Oscl::BT::Mesh::Network::Api&	networkApi,
			Oscl::BT::Mesh::Model::OnOff::
			Server::Context::Api&			contextApi,
			Oscl::BT::Mesh::Subscription::
			Persist::Creator::Api&			subscriptionPersistApi,
			Oscl::BT::Mesh::Model::Key::
			Persist::Creator::Api&			keyPersistApi,
			Oscl::BT::Mesh::State::
			Generic::OnOff::Persist::Api&	onOffStatePersistApi,
			uint16_t						elementAddress,
			bool							allowSegmentedStatus	= false
			) noexcept;


		/** */
		~Part() noexcept;

		/** */
		Oscl::BT::Mesh::Model::RX::Item&	getModelRxItem() noexcept;

		/** */
		Oscl::BT::Mesh::Model::Api&	getModelApi() noexcept;

	private: // Oscl::Update::Observer::Composer _onOffObserver
		/** */
		void	onOffObserverUpdate(bool& state) noexcept;

	private: // Oscl::BT::Mesh::Transport::TX::Complete::Api
		/** This operation is invoked when the
			PDU has been successfully transmitted
			by the transport layer.

			For unsegmented messages, this is invoked
			immediately since there is no acknowledgment.

			For segmented messages sent to a unicast
			address destination, this operation is only
			invoked after all segments have been acknowledged
			by the destination or its friend.

			For segmented messages sent to a broadcast,
			group, or virtual address, this is invoked
			after all segments have been transmitted for
			the specified number of retrys.
		 */
		void	success() noexcept;

		/** This operation is invoked when the
			PDU has been successfully transmitted
			by the transport layer.

			For unsegmented messages, this is invoked
			immediately since there is no acknowledgment.

			For segmented messages sent to a unicast
			address destination, this operation is only
			invoked after all segments have been acknowledged
			by the destination or its friend.

			For segmented messages sent to a broadcast,
			group, or virtual address, this is invoked
			after all segments have been transmitted for
			the specified number of retrys.
		 */
		void	successOnBehalfOfLPN() noexcept;

		/**	This operation is invoked only for segmented
			messages sent to a unicast address where the
			remote did not acknowledge all segments before
			the timer expired.
		 */
		void	failedTimeout() noexcept;

		/**	This operation is invoked only for segmented
			messages sent to a unicast address where the
			remote indicated that it is unable to receive
			the message at this time (due to resource
			limitations).
		 */
		void	failedBusy() noexcept;

		/**	This operation is invoked when the transport
			layer does not currently have enough resources
			to send the message.
		 */
		void	failedOutOfResources() noexcept;

		/**	This operation is invoked for messages that
			are too big to be handled by the transport
			layer. This indicates a software error.
		 */
		void	failedTooBig() noexcept;

	private: // Oscl::BT::Mesh::Model::App::Server::Context::Composer _appServerContext
		/** Process the decrypted Access Message
		 */
		void	processAccessMessage(
					const void*	frame,
					unsigned	length,
					uint16_t	src,
					uint16_t	dst,
					uint16_t	appKeyIndex
					) noexcept;

		/** RETURN: true for match */
		bool	sigModelIdMatch(uint16_t modelID) const noexcept;

		/** RETURN: true for match */
		bool	vendorModelIdMatch(uint16_t vendorID, uint16_t modelID) const noexcept;

		/** RETURN: true if the model supports AppKey binding. */
		bool	modelSupportsAppKeyBinding() const noexcept;

		/** RETURN: zero if the model does NOT support publication.
		 */
		Oscl::BT::Mesh::Publication::Api*	getPublicationApi() noexcept;

		/** */
		uint16_t	getPublicationAppKeyIndex() const noexcept;

		/** RETURN: zero if the model does NOT support subscription.
		 */
		Oscl::BT::Mesh::Subscription::Api*	getSubscriptionApi() noexcept;

		/** RETURN: zero if the model does NOT support subscription.
		 */
		Oscl::BT::Mesh::Subscription::Server::Api*	getSubscriptionServerApi() noexcept;

		/** */
		void	stop() noexcept;

	private: // Oscl::BT::Mesh::State::Generic::OnOff::Api
		// /home/mike/root/src/oscl/bluetooth/mesh/state/generic/onoff/api.h
		/*	This operation is invoked by a model
			to immediately change the Generic OnOff State.
			onOff:
				0x00	Off
				0x01	On
		 */
		void	setGenericOnOffState(
					uint8_t	onOff
					) noexcept;

		/*	This operation is invoked by a model
			to change the Generic OnOff State.
			onOff:
				0x00	Off
				0x01	On
			transitionTime: 3.1.3 Generic Default Transition Time
				bits 0:5: Number of steps
					0x00		immediate
					0x00:0x3E	number of steps
					0x3F		unknown
				bits 6:7: Step resolution
					0b00	Step Resolution is 100 milliseconds
					0b01	Step Resolution is 1 second
					0b10	Step Resolution is 10 seconds
					0b11	Step Resolution is 10 minutes
			executionDelayIn5msSteps: execution delay in 5ms steps
		 */
		void	setGenericOnOffState(
					uint8_t	onOff,
					uint8_t	transitionTime,
					uint8_t	executionDelayIn5msSteps
					) noexcept;

		/*	This operation is invoked by a model
			to obtain the current OnOff State.
			presentOnOff:
				0x00	Off
				0x01	On
			targetOnOff:
				0x00	Off
				0x01	On
			remainingTime: 3.1.3 Generic Default Transition Time
				bits 0:5: Number of steps
					0x00		immediate
					0x00:0x3E	number of steps
					0x3F		unknown
				bits 6:7: Step resolution
					0b00	Step Resolution is 100 milliseconds
					0b01	Step Resolution is 1 second
					0b10	Step Resolution is 10 seconds
					0b11	Step Resolution is 10 minutes
		 */
		void	getGenericOnOffState(
					uint8_t&	presentOnOff,
					uint8_t&	targetOnOff,
					uint8_t&	remainingTime
					) noexcept;

	private: // Oscl::BT::Mesh::Publication::Server::Context::Api
		/**
			This operation is invoked to transmit
			a status message.
			The label may be nil.
			If the label is not nil, then
			the length is 16.
		 */
		void	publish(
					uint16_t	appKeyIndex,
					uint16_t	dstAddress,
					uint8_t		ttl,
					const void*	label,
					bool		retransmitting
					) noexcept;
	};

}
}
}
}
}
}

#endif
