/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <new>
#include "part.h"
#include "oscl/endian/decoder/linear/part.h"
#include "oscl/encoder/le/base.h"
#include "oscl/encoder/be/base.h"
#include "oscl/error/info.h"
#include "oscl/bluetooth/mesh/address/virtual.h"
#include "oscl/bluetooth/mesh/address/types.h"
#include "oscl/bluetooth/mesh/address/values.h"
#include "oscl/bluetooth/mesh/publication/utils.h"
#include "oscl/bluetooth/mesh/model/opcode/decoder.h"
#include "oscl/bluetooth/mesh/constants.h"

using namespace Oscl::BT::Mesh::Model::OnOff::Server;

//#define DEBUG_TRACE
//#define DEBUG_TRACE_TX_STATUS

Part::Part(
	Oscl::Pdu::Memory::Api&			freeStoreApi,
	Oscl::Timer::Factory::Api&		timerFactoryApi,
	Oscl::BT::Mesh::
	Publication::Persist::Api&		publicationPersistApi,
	Oscl::BT::Mesh::Node::Api&		nodeApi,
	Oscl::BT::Mesh::Element::
	Symbiont::Kernel::Api&			kernelApi,
	Oscl::BT::Mesh::
	Transport::Lower::TX::Api&		lowerTransportApi,
	Oscl::BT::Mesh::Network::Api&	networkApi,
	Oscl::BT::Mesh::Model::OnOff::
	Server::Context::Api&			contextApi,
	Oscl::BT::Mesh::Subscription::
	Persist::Creator::Api&			subscriptionPersistApi,
	Oscl::BT::Mesh::Model::Key::
	Persist::Creator::Api&			keyPersistApi,
	Oscl::BT::Mesh::State::
	Generic::OnOff::Persist::Api&	onOffStatePersistApi,
	uint16_t						elementAddress,
	bool							allowSegmentedStatus
	) noexcept:
		_timerFactoryApi(timerFactoryApi),
		_contextApi(contextApi),
		_onOffStatePersistApi(onOffStatePersistApi),
		_allowSegmentedStatus(allowSegmentedStatus),
		_appServerContext(
			*this,
			&Part::processAccessMessage,
			&Part::sigModelIdMatch,
			&Part::vendorModelIdMatch,
			&Part::modelSupportsAppKeyBinding,
			&Part::getPublicationApi,
			&Part::getPublicationAppKeyIndex,
			&Part::getSubscriptionApi,
			&Part::getSubscriptionServerApi,
			&Part::stop
			),
		_subscriptionServerPart(
			subscriptionPersistApi,
			kernelApi.getSubscriptionRegisterApi()
			),
		_appServerPart(
			_appServerContext,
			freeStoreApi,
			nodeApi,
			lowerTransportApi,
			networkApi,
			keyPersistApi,
			elementAddress
			),
		_publicationPart(
            *this,
            timerFactoryApi,
            publicationPersistApi
			),
		_onOffTidPeriodicTimerApi(
			*timerFactoryApi.allocate()
			),
		_onOffTransitionTimerApi(
			*timerFactoryApi.allocate()
			),
		_transitionAdaptor(
			contextApi.getSwitchApi(),
			_onOffTransitionTimerApi,
			onOffStatePersistApi.getTargetState(),
			this
			),
		_genericOnOffTransceiver(
			_transitionAdaptor,
			_appServerPart.getUpperTransportApi(),
			_onOffTidPeriodicTimerApi,
			allowSegmentedStatus
			),
		_onOffObserver(
			*this,
			&Part::onOffObserverUpdate
			)
		{

	_contextApi.getSubjectApi().attach(_onOffObserver);
	}

Part::~Part() noexcept{
	_timerFactoryApi.free(_onOffTidPeriodicTimerApi);
	_timerFactoryApi.free(_onOffTransitionTimerApi);
	}

Oscl::BT::Mesh::Model::RX::Item&	Part::getModelRxItem() noexcept{
	return _appServerPart;
	}

Oscl::BT::Mesh::Model::Api&	Part::getModelApi() noexcept{
	return _appServerPart.getModelApi();
	}

void	Part::processAccessMessage(
			const void*	frame,
			unsigned	length,
			uint16_t	src,
			uint16_t	dst,
			uint16_t	appKeyIndex
			) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);

	Oscl::Error::Info::hexDump(
		frame,
		length
		);
	#endif

	Oscl::Endian::Decoder::Linear::Part
	leDecoder(
		frame,
		length
		);

	Oscl::Decoder::Api&	decoder	= leDecoder.le();

	uint32_t
	opcode	= Oscl::BT::Mesh::Model::OpCode::decode(decoder);

	using namespace Oscl::BT::Mesh::Model::OpCode;

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s:\n"
		"\topcode: (0x%4.4X)\n",
		__PRETTY_FUNCTION__,
		opcode
		);
	#endif

	const uint8_t*	p	= (const uint8_t*)frame;

	unsigned
	remaining	= decoder.remaining();

	unsigned
	payloadOffset	= length - remaining;

	_genericOnOffTransceiver.receive(
		opcode,
		src,
		dst,
		&p[payloadOffset],
		remaining,
		appKeyIndex
		);
	}

void	Part::onOffObserverUpdate(bool& state) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s: state: %s\n",
		__PRETTY_FUNCTION__,
		state?"ON":"OFF"
		);
	#endif

	_publicationPart.publish();
	}

bool	Part::sigModelIdMatch(uint16_t modelID) const noexcept{
	return (modelID == 0x1000); // Generic OnOff Server
	}

bool	Part::vendorModelIdMatch(uint16_t vendorID, uint16_t modelID) const noexcept{
	return false;
	}

bool	Part::modelSupportsAppKeyBinding() const noexcept{
	return true;
	}

Oscl::BT::Mesh::Publication::Api*	Part::getPublicationApi() noexcept{
	return &_publicationPart;
	}

uint16_t	Part::getPublicationAppKeyIndex() const noexcept{
	return _publicationPart.getPublicationAppKeyIndex();
	}

Oscl::BT::Mesh::Subscription::Api*	Part::getSubscriptionApi() noexcept{
	return &_subscriptionServerPart;
	}

Oscl::BT::Mesh::Subscription::Server::Api*	Part::getSubscriptionServerApi() noexcept{
	return &_subscriptionServerPart;
	}

void	Part::stop() noexcept{
	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s:\n",
		__PRETTY_FUNCTION__
		);
	#endif

	_publicationPart.stop();

	_onOffTidPeriodicTimerApi.stop();
	_contextApi.getSubjectApi().detach(_onOffObserver);
	}

void	Part::setGenericOnOffState(
			uint8_t	onOff
			) noexcept{

	/*	This operation is mostly overridden
		by the _transitionAdaptor. This
		implementation only needs to update
		persistence.
	 */

	_onOffStatePersistApi.setTargetState(onOff);
	}

void	Part::setGenericOnOffState(
			uint8_t	onOff,
			uint8_t	transitionTime,
			uint8_t	executionDelayIn5msSteps
			) noexcept{

	/*	This operation is mostly overridden
		by the _transitionAdaptor. This
		implementation only needs to update
		persistence.
	 */

	_onOffStatePersistApi.setTargetState(onOff);
	}

void	Part::getGenericOnOffState(
			uint8_t&	presentOnOff,
			uint8_t&	targetOnOff,
			uint8_t&	remainingTime
			) noexcept{
	/*	This operation is overridden by the
		_transitionAdaptor acting as a decorator.
	 */
	}

void	Part::publish(
			uint16_t	appKeyIndex,
			uint16_t	dstAddress,
			uint8_t		ttl,
			const void*	label,
			bool		retransmitting
			) noexcept{

	const unsigned
	labelLen	= label?16:0;

	const bool
	sendSegmented	= (
			_allowSegmentedStatus
		&&	Oscl::BT::Mesh::Address::isUnicastAddress(dstAddress)
		);

	_genericOnOffTransceiver.sendGenericOnOffStatus(
		appKeyIndex,
		dstAddress,
		ttl,
		sendSegmented,
		label,
		labelLen
		);
	}

