/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_bluetooth_mesh_model_onoff_server_context_composerh_
#define _oscl_bluetooth_mesh_model_onoff_server_context_composerh_

#include "api.h"

/** */
namespace Oscl {
/** */
namespace BT {
/** */
namespace Mesh {
/** */
namespace Model {
/** */
namespace OnOff {
/** */
namespace Server {
/** */
namespace Context {

/** */
template <class Context>
class Composer : public Api {
	private:
		/** */
		Context&	_context;

		/**	*/
		Oscl::Switch::Spst::Control::Api&	(Context::*_getSwitchApi)();

		/**	*/
		Oscl::Update::Subject::Api<bool>&	(Context::*_getSubjectApi)();

	public:
		/** */
		Composer(
			Context&	context,
			Oscl::Switch::Spst::Control::Api&	(Context::*getSwitchApi)(),
			Oscl::Update::Subject::Api<bool>&	(Context::*getSubjectApi)()
			) noexcept;

		/**	*/
		Oscl::Switch::Spst::Control::Api&	getSwitchApi() noexcept;

		/**	*/
		Oscl::Update::Subject::Api<bool>&	getSubjectApi() noexcept;

	};

template <class Context>
Composer<Context>::Composer(
	Context&	context,
	Oscl::Switch::Spst::Control::Api&	(Context::*getSwitchApi)(),
	Oscl::Update::Subject::Api<bool>&	(Context::*getSubjectApi)()
	)noexcept:
		_context(context),
		_getSwitchApi(getSwitchApi),
		_getSubjectApi(getSubjectApi)
		{
	}

template <class Context>
Oscl::Switch::Spst::Control::Api&	Composer<Context>::getSwitchApi() noexcept{
	return (_context.*_getSwitchApi)();
	}

template <class Context>
Oscl::Update::Subject::Api<bool>&	Composer<Context>::getSubjectApi() noexcept{
	return (_context.*_getSubjectApi)();
	}

}
}
}
}
}
}
}

#endif
