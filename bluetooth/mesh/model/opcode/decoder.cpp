/*
   Copyright (C) 2020 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "decoder.h"

using namespace Oscl::BT::Mesh::Model::OpCode;

/** */
uint32_t	Oscl::BT::Mesh::Model::OpCode::decode(Oscl::Decoder::Api& decoder) noexcept{
	uint8_t		opcodeOctet;

	decoder.decode(opcodeOctet);

	if(opcodeOctet == 0x7F){
		// Reserved for Future use
		return invalidOpcode;
		}

	if(decoder.underflow()){
		return invalidOpcode;
		}

	uint8_t
	opcodeClass	= opcodeOctet >> 6;

	uint32_t
	opcode	= opcodeOctet;

	switch(opcodeClass){
		case 0:
		case 1:
			break;
		case 2:
			opcode	<<= 8;
			decoder.decode(opcodeOctet);
			opcode	|= opcodeOctet;
			break;
		case 3:
			opcode	<<= 8;
			decoder.decode(opcodeOctet);
			opcode	<<= 8;
			decoder.decode(opcodeOctet);
			opcode	|= opcodeOctet;
			break;
		}

	if(decoder.underflow()){
		return invalidOpcode;
		}

	return opcode;
	}

