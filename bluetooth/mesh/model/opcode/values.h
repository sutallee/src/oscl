/*
   Copyright (C) 2020 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_bluetooth_mesh_model_opcode_valuesh_
#define _oscl_bluetooth_mesh_model_opcode_valuesh_

#include <stdint.h>
#include "oscl/decoder/api.h"

/** */
namespace Oscl {
/** */
namespace BT {
/** */
namespace Mesh {
/** */
namespace Model {
/** */
namespace OpCode {

/** */
constexpr uint32_t	invalidOpcode	= 0xFFFFFFFF;

constexpr uint32_t  configAppKeyAdd							= 0x00;
constexpr uint32_t  configAppKeyUpdate						= 0x01;
constexpr uint32_t  configCompositionDataStatus				= 0x02;
constexpr uint32_t  configModelPublicationSet				= 0x03;
constexpr uint32_t  healthCurrentStatus						= 0x04;
constexpr uint32_t  healthFaultStatus						= 0x05;
constexpr uint32_t  configHeartbeatPublicationStatus		= 0x06;
constexpr uint32_t  configAppKeyDelete						= 0x8000;
constexpr uint32_t  configAppKeyGet							= 0x8001;
constexpr uint32_t  configAppKeyList						= 0x8002;
constexpr uint32_t  configAppKeyStatus						= 0x8003;
constexpr uint32_t  healthAttentionGet						= 0x8004;
constexpr uint32_t  healthAttentionSet						= 0x8005;
constexpr uint32_t  healthAttentionSetUnacknowledged		= 0x8006;
constexpr uint32_t  healthAttentionStatus					= 0x8007;
constexpr uint32_t  configCompositionDataGet				= 0x8008;
constexpr uint32_t  configBeaconGet							= 0x8009;
constexpr uint32_t  configBeaconSet							= 0x800A;
constexpr uint32_t  configBeaconStatus						= 0x800B;
constexpr uint32_t  configDefaultTtlGet						= 0x800C;
constexpr uint32_t  configDefaultTtlSet						= 0x800D;
constexpr uint32_t  configDefaultTtlStatus					= 0x800E;
constexpr uint32_t  configFriendGet							= 0x800F;
constexpr uint32_t  configFriendSet							= 0x8010;
constexpr uint32_t  configFriendStatus						= 0x8011;
constexpr uint32_t  configGattProxyGet						= 0x8012;
constexpr uint32_t  configGattProxySet						= 0x8013;
constexpr uint32_t  configGattProxyStatus					= 0x8014;
constexpr uint32_t  configKeyRefreshPhaseGet				= 0x8015;
constexpr uint32_t  configKeyRefreshPhaseSet				= 0x8016;
constexpr uint32_t  configKeyRefreshPhaseStatus				= 0x8017;
constexpr uint32_t  configModelPublicationGet				= 0x8018;
constexpr uint32_t  configModelPublicationStatus			= 0x8019;
constexpr uint32_t  configModelPublicationVirtualAddressSet	= 0x801A;
constexpr uint32_t  configModelSubscriptionAdd				= 0x801B;
constexpr uint32_t  configModelSubscriptionDelete			= 0x801C;
constexpr uint32_t  configModelSubscriptionDeleteAll		= 0x801D;
constexpr uint32_t  configModelSubscriptionOverwrite		= 0x801E;
constexpr uint32_t  configModelSubscriptionStatus				= 0x801F;
constexpr uint32_t  configModelSubscriptionVirtualAddressAdd	= 0x8020;
constexpr uint32_t  configModelSubscriptionVirtualAddressDelete	= 0x8021;
constexpr uint32_t  configModelSubscriptionVirtualAddressOverwrite	= 0x8022;
constexpr uint32_t  configNetworkTransmitGet						= 0x8023;
constexpr uint32_t  configNetworkTransmitSet						= 0x8024;
constexpr uint32_t  configNetworkTransmitStatus						= 0x8025;
constexpr uint32_t  configRelayGet									= 0x8026;
constexpr uint32_t  configRelaySet									= 0x8027;
constexpr uint32_t  configRelayStatus								= 0x8028;
constexpr uint32_t  configSigModelSubscriptionGet					= 0x8029;
constexpr uint32_t  configSigModelSubscriptionList					= 0x802A;
constexpr uint32_t  configVendorModelSubscriptionGet				= 0x802B;
constexpr uint32_t  configVendorModelSubscriptionList				= 0x802C;
constexpr uint32_t  configLowPowerNodePollTimeoutGet				= 0x802D;
constexpr uint32_t  configLowPowerNodePollTimeoutStatus				= 0x802E;
constexpr uint32_t  healthFaultClear								= 0x802F;
constexpr uint32_t  healthFaultClearUnacknowledged					= 0x8030;
constexpr uint32_t  healthFaultGet									= 0x8031;
constexpr uint32_t  healthFaultTest									= 0x8032;
constexpr uint32_t  healthFaultTestUnacknowledged					= 0x8033;
constexpr uint32_t  healthPeriodGet									= 0x8034;
constexpr uint32_t  healthPeriodSet									= 0x8035;
constexpr uint32_t  healthPeriodSetUnacknowledged					= 0x8036;
constexpr uint32_t  healthPeriodStatus								= 0x8037;
constexpr uint32_t  configHeartbeatPublicationGet					= 0x8038;
constexpr uint32_t  configHeartbeatPublicationSet					= 0x8039;
constexpr uint32_t  configHeartbeatSubscriptionGet					= 0x803A;
constexpr uint32_t  configHeartbeatSubscriptionSet					= 0x803B;
constexpr uint32_t  configHeartbeatSubscriptionStatus				= 0x803C;
constexpr uint32_t  configModelAppBind								= 0x803D;
constexpr uint32_t  configModelAppStatus							= 0x803E;
constexpr uint32_t  configModelAppUnbind							= 0x803F;
constexpr uint32_t  configNetKeyAdd									= 0x8040;
constexpr uint32_t  configNetKeyDelete								= 0x8041;
constexpr uint32_t  configNetKeyGet									= 0x8042;
constexpr uint32_t  configNetKeyList								= 0x8043;
constexpr uint32_t  configNetKeyStatus								= 0x8044;
constexpr uint32_t  configNetKeyUpdate								= 0x8045;
constexpr uint32_t  configNodeIdentityGet							= 0x8046;
constexpr uint32_t  configNodeIdentitySet							= 0x8047;
constexpr uint32_t  configNodeIdentityStatus						= 0x8048;
constexpr uint32_t  configNodeReset									= 0x8049;
constexpr uint32_t  configNodeResetStatus							= 0x804A;
constexpr uint32_t  configSigModelAppGet							= 0x804B;
constexpr uint32_t  configSigModelAppList							= 0x804C;
constexpr uint32_t  configVendorModelAppGet							= 0x804D;
constexpr uint32_t  configVendorModelAppList						= 0x804E;

constexpr uint32_t  genericOnOffGet									= 0x8201;
constexpr uint32_t  genericOnOffSet									= 0x8202;
constexpr uint32_t  genericOnOffSetUnacknowledged					= 0x8203;
constexpr uint32_t  genericOnOffStatus								= 0x8204;
constexpr uint32_t  genericLevelGet									= 0x8205;
constexpr uint32_t  genericLevelSet									= 0x8206;
constexpr uint32_t  genericLevelSetUnacknowledged					= 0x8207;
constexpr uint32_t  genericLevelStatus								= 0x8208;
constexpr uint32_t  genericDeltaSet									= 0x8209;
constexpr uint32_t  genericDeltaSetUnacknowledged					= 0x820A;
constexpr uint32_t  genericMoveSet									= 0x820B;
constexpr uint32_t  genericMoveSetUnacknowledged					= 0x820C;
constexpr uint32_t  genericDefaultTransisitionTimeGet				= 0x820D;
constexpr uint32_t  genericDefaultTransisitionTimeSet				= 0x820E;
constexpr uint32_t  genericDefaultTransisitionTimeSetUnacknowledged	= 0x820F;
constexpr uint32_t  genericDefaultTransisitionTimeStatus			= 0x8210;
constexpr uint32_t  genericOnPowerUpGet								= 0x8211;
constexpr uint32_t  genericOnPowerUpStatus							= 0x8212;
constexpr uint32_t  genericOnPowerUpSet								= 0x8213;
constexpr uint32_t  genericOnPowerUpSetUnacknowledged				= 0x8214;
constexpr uint32_t  genericPowerLevelGet							= 0x8215;
constexpr uint32_t  genericPowerLevelSet							= 0x8216;
constexpr uint32_t  genericPowerLevelSetUnacknowledged				= 0x8217;
constexpr uint32_t  genericPowerLevelStatus							= 0x8218;
constexpr uint32_t  genericPowerLastGet								= 0x8219;
constexpr uint32_t  genericPowerLastStatus							= 0x821A;
constexpr uint32_t  genericPowerDefaultGet							= 0x821B;
constexpr uint32_t  genericPowerDefaultStatus						= 0x821C;
constexpr uint32_t  genericPowerRangeGet							= 0x821D;
constexpr uint32_t  genericPowerRangeStatus							= 0x821E;
constexpr uint32_t  genericPowerDefaultSet							= 0x821F;
constexpr uint32_t  genericPowerDefaultSetUnacknowledged			= 0x8220;
constexpr uint32_t  genericPowerRangeSet							= 0x8221;
constexpr uint32_t  genericPowerRangeSetUnacknowledged				= 0x8222;
constexpr uint32_t  genericBatteryGet								= 0x8223;
constexpr uint32_t  genericBatteryStatus							= 0x8224;
constexpr uint32_t  genericLocationGlobalGet						= 0x8225;
constexpr uint32_t  genericLocationGlobalStatus						= 0x40;
constexpr uint32_t  genericLocationLocalGet							= 0x8226;
constexpr uint32_t  genericLocationLocalStatus						= 0x8227;
constexpr uint32_t  genericLocationGlobalSet						= 0x41;
constexpr uint32_t  genericLocationGlobalSetUnacknowledged			= 0x42;
constexpr uint32_t  genericLocationLocalSet							= 0x8228;
constexpr uint32_t  genericLocationLocalSetUnacknowledged			= 0x8229;
constexpr uint32_t  genericManufacturerPropertiesGet				= 0x822A;
constexpr uint32_t  genericManufacturerPropertiesStatus				= 0x43;
constexpr uint32_t  genericManufacturerPropertyGet					= 0x822B;
constexpr uint32_t  genericManufacturerPropertySet					= 0x44;
constexpr uint32_t  genericManufacturerPropertySetUnacknowledged	= 0x45;
constexpr uint32_t  genericManufacturerPropertyStatus				= 0x46;
constexpr uint32_t  genericAdminPropertiesGet						= 0x822C;
constexpr uint32_t  genericAdminPropertiesStatus					= 0x47;
constexpr uint32_t  genericAdminPropertyGet							= 0x822D;
constexpr uint32_t  genericAdminPropertySet							= 0x48;
constexpr uint32_t  genericAdminPropertySetUnacknowledged			= 0x49;
constexpr uint32_t  genericAdminPropertyStatus						= 0x4A;
constexpr uint32_t  genericUserPropertiesGet						= 0x822E;
constexpr uint32_t  genericUserPropertiesStatus						= 0x4B;
constexpr uint32_t  genericUserPropertyGet							= 0x822F;
constexpr uint32_t  genericUserPropertySet							= 0x4C;
constexpr uint32_t  genericUserPropertySetUnacknowledged			= 0x4D;
constexpr uint32_t  genericUserPropertyStatus						= 0x4E;
constexpr uint32_t  genericClientPropertiesGet						= 0x4F;
constexpr uint32_t  genericClientPropertiesStatus					= 0x50;
constexpr uint32_t  sensorDescriptorGet								= 0x8230;
constexpr uint32_t  sensorDescriptorStatus							= 0x51;
constexpr uint32_t  sensorGet										= 0x8231;
constexpr uint32_t  sensorStatus									= 0x52;
constexpr uint32_t  sensorColumnGet									= 0x8232;
constexpr uint32_t  sensorColumnStatus								= 0x53;
constexpr uint32_t  sensorSeriesGet									= 0x8233;
constexpr uint32_t  sensorSeriesStatus								= 0x54;
constexpr uint32_t  sensorCadenceGet								= 0x8234;
constexpr uint32_t  sensorCadenceSet								= 0x55;
constexpr uint32_t  sensorCadenceSetUnacknowledged					= 0x56;
constexpr uint32_t  sensorCadenceStatus								= 0x57;
constexpr uint32_t  sensorSettingsGet								= 0x8235;
constexpr uint32_t  sensorSettingsStatus							= 0x58;
constexpr uint32_t  sensorSettingGet								= 0x8236;
constexpr uint32_t  sensorSettingSet								= 0x59;
constexpr uint32_t  sensorSettingSetUnacknowledged					= 0x5A;
constexpr uint32_t  sensorSettingStatus								= 0x5B;
constexpr uint32_t  timeGet											= 0x8237;
constexpr uint32_t  timeSet											= 0x5C;
constexpr uint32_t  timeStatus										= 0x5D;
constexpr uint32_t  timeRoleGet										= 0x8238;
constexpr uint32_t  timeRoleSet										= 0x8239;
constexpr uint32_t  timeRoleStatus									= 0x823A;
constexpr uint32_t  timeZoneGet										= 0x823B;
constexpr uint32_t  timeZoneSet										= 0x823C;
constexpr uint32_t  timeZoneStatus									= 0x823D;
constexpr uint32_t  taiUtcDeltaGet									= 0x823E;
constexpr uint32_t  taiUtcDeltaSet									= 0x823F;
constexpr uint32_t  taiUtcDeltaStatus								= 0x8240;
constexpr uint32_t  sceneGet										= 0x8241;
constexpr uint32_t  sceneRecall										= 0x8242;
constexpr uint32_t  sceneRecallUnacknowledged						= 0x8243;
constexpr uint32_t  sceneStatus										= 0x5E;
constexpr uint32_t  sceneRegisterGet								= 0x8244;
constexpr uint32_t  sceneRegisterStatus								= 0x8245;
constexpr uint32_t  sceneStore										= 0x8246;
constexpr uint32_t  sceneStoreUnacknowledged						= 0x8247;
constexpr uint32_t  sceneDelete										= 0x829E;
constexpr uint32_t  sceneDeleteUnacknowledged						= 0x829F;
constexpr uint32_t  schedulerActionGet								= 0x8248;
constexpr uint32_t  schedulerActionStatus							= 0x5F;
constexpr uint32_t  schedulerGet									= 0x8249;
constexpr uint32_t  schedulerStatus									= 0x824A;
constexpr uint32_t  schedulerActionSet								= 0x60;
constexpr uint32_t  schedulerActionSetUnacknowledged				= 0x61;
constexpr uint32_t  lightLightnessGet								= 0x824B;
constexpr uint32_t  lightLightnessSet								= 0x824C;
constexpr uint32_t  lightLightnessSetUnacknowledged					= 0x824D;
constexpr uint32_t  lightLightnessStatus							= 0x824E;
constexpr uint32_t  lightLightnessLinearGet							= 0x824F;
constexpr uint32_t  lightLightnessLinearSet							= 0x8250;
constexpr uint32_t  lightLightnessLinearSetUnacknowledged			= 0x8251;
constexpr uint32_t  lightLightnessLinearStatus						= 0x8252;
constexpr uint32_t  lightLightnessLastGet							= 0x8253;
constexpr uint32_t  lightLightnessLastStatus						= 0x8254;
constexpr uint32_t  lightLightnessDefaultGet						= 0x8255;
constexpr uint32_t  lightLightnessDefaultStatus						= 0x8256;
constexpr uint32_t  lightLightnessRangeGet							= 0x8257;
constexpr uint32_t  lightLightnessRangeStatus						= 0x8258;
constexpr uint32_t  lightLightnessDefaultSet						= 0x8259;
constexpr uint32_t  lightLightnessDefaultSetUnacknowledged			= 0x825A;
constexpr uint32_t  lightLightnessRangeSet							= 0x825B;
constexpr uint32_t  lightLightnessRangeSetUnacknowledged			= 0x825C;
constexpr uint32_t  lightCtlGet										= 0x825D;
constexpr uint32_t  lightCtlSet										= 0x825E;
constexpr uint32_t  lightCtlSetUnacknowledged						= 0x825F;
constexpr uint32_t  lightCtlStatus									= 0x8260;
constexpr uint32_t  lightCtlTemperatureGet							= 0x8261;
constexpr uint32_t  lightCtlTemperatureRangeGet						= 0x8262;
constexpr uint32_t  lightCtlTemperatureRangeStatus					= 0x8263;
constexpr uint32_t  lightCtlTemperatureSet							= 0x8264;
constexpr uint32_t  lightCtlTemperatureSetUnacknowledged			= 0x8265;
constexpr uint32_t  lightCtlTemperatureStatus						= 0x8266;
constexpr uint32_t  lightCtlDefaultGet								= 0x8267;
constexpr uint32_t  lightCtlDefaultStatus							= 0x8268;
constexpr uint32_t  lightCtlDefaultSet								= 0x8269;
constexpr uint32_t  lightCtlDefaultSetUnacknowledged				= 0x826A;
constexpr uint32_t  lightCtlTemperatureRangeSet						= 0x826B;
constexpr uint32_t  lightCtlTemperatureRangeSetUnacknowledged		= 0x826C;
constexpr uint32_t  lightHslGet										= 0x826D;
constexpr uint32_t  lightHslHueGet									= 0x826E;
constexpr uint32_t  lightHslHueSet									= 0x826F;
constexpr uint32_t  lightHslHueSetUnacknowledged					= 0x8270;
constexpr uint32_t  lightHslHueStatus								= 0x8271;
constexpr uint32_t  lightHslSaturationGet							= 0x8272;
constexpr uint32_t  lightHslSaturationSet							= 0x8273;
constexpr uint32_t  lightHslSaturationSetUnacknowledged				= 0x8274;
constexpr uint32_t  lightHslSaturationStatus						= 0x8275;
constexpr uint32_t  lightHslSet										= 0x8276;
constexpr uint32_t  lightHslSetUnacknowledged						= 0x8277;
constexpr uint32_t  lightHslStatus									= 0x8278;
constexpr uint32_t  lightHslTargetGet								= 0x8279;
constexpr uint32_t  lightHslTargetStatus							= 0x827A;
constexpr uint32_t  lightHslDefaultGet								= 0x827B;
constexpr uint32_t  lightHslDefaultStatus							= 0x827C;
constexpr uint32_t  lightHslRangeGet								= 0x827D;
constexpr uint32_t  lightHslRangeStatus								= 0x827E;
constexpr uint32_t  lightHslDefaultSet								= 0x827F;
constexpr uint32_t  lightHslDefaultSetUnacknowledged				= 0x8280;
constexpr uint32_t  lightHslRangeSet								= 0x8281;
constexpr uint32_t  lightHslRangeSetUnacknowledged					= 0x82;	// ??? 0x8282?
constexpr uint32_t  lightXylGet										= 0x8283;
constexpr uint32_t  lightXylSet										= 0x8284;
constexpr uint32_t  lightXylSetUnacknowledged						= 0x8285;
constexpr uint32_t  lightXylStatus									= 0x8286;
constexpr uint32_t  lightXylTargetGet								= 0x8287;
constexpr uint32_t  lightXylTargetStatus							= 0x8288;
constexpr uint32_t  lightXylDefaultGet								= 0x8289;
constexpr uint32_t  lightXylDefaultStatus							= 0x828A;
constexpr uint32_t  lightXylRangeGet								= 0x828B;
constexpr uint32_t  lightXylRangeStatus								= 0x828C;
constexpr uint32_t  lightXylDefaultSet								= 0x828D;
constexpr uint32_t  lightXylDefaultSetUnacknowledged				= 0x828E;
constexpr uint32_t  lightXylRangeSet								= 0x828F;
constexpr uint32_t  lightXylRangeSetUnacknowledged					= 0x8290;
constexpr uint32_t  lightLcModeGet									= 0x8291;
constexpr uint32_t  lightLcModeSet									= 0x8292;
constexpr uint32_t  lightLcModeSetUnacknowledged					= 0x8293;
constexpr uint32_t  lightLcModeStatus								= 0x8294;
constexpr uint32_t  lightLcOmGet									= 0x8295;
constexpr uint32_t  lightLcOmSet									= 0x8296;
constexpr uint32_t  lightLcOmSetUnacknowledged						= 0x8297;
constexpr uint32_t  lightLcOmStatus									= 0x8298;
constexpr uint32_t  lightLcLightOnOffGet							= 0x8299;
constexpr uint32_t  lightLcLightOnOffSet							= 0x829A;
constexpr uint32_t  lightLcLightOnOffSetUnacknowledged				= 0x829B;
constexpr uint32_t  lightLcLightOnOffStatus							= 0x829C;
constexpr uint32_t  lightLcPropertyGet								= 0x829D;
constexpr uint32_t  lightLcPropertySet								= 0x62;
constexpr uint32_t  lightLcPropertySetUnacknowledged				= 0x63;
constexpr uint32_t  lightLcPropertyStatus							= 0x64;

}
}
}
}
}

#endif
