/*
   Copyright (C) 2021 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "encoder.h"

//#define DEBUG_TRACE

#ifdef DEBUG_TRACE
#include "oscl/error/fatal.h"
#include "oscl/error/info.h"
#endif

using namespace Oscl::BT::Mesh::Model::OpCode;

/** */
void	Oscl::BT::Mesh::Model::OpCode::encode(
			Oscl::Encoder::Api&	encoder,
			uint32_t			opcode
			) noexcept{

	if(opcode >= 0x007F0000){
		/*	Illegal opcode
			An initial value of 0x7F is reserved for
			future use.
		 */
		#ifdef DEBUG_TRACE
		Oscl::ErrorFatal::logAndExit(
			"%s: opcode >= 0x007F0000\n",
			OSCL_PRETTY_FUNCTION
			);
		#endif
		return;
		}

	if(opcode > 0x0000FFFF){
		#ifdef DEBUG_TRACE
		Oscl::Error::Info::log(
			"%s: 3 octet\n",
			OSCL_PRETTY_FUNCTION
			);
		#endif
		uint8_t
		opcodeOctet	= opcode >> 16;
		encoder.encode(opcodeOctet);
		opcodeOctet	= opcode >> 8;
		encoder.encode(opcodeOctet);
		opcodeOctet	= opcode >> 0;
		encoder.encode(opcodeOctet);
		return;
		}

	if(opcode > 0x000000FF){
		#ifdef DEBUG_TRACE
		Oscl::Error::Info::log(
			"%s: 2 octet\n",
			OSCL_PRETTY_FUNCTION
			);
		#endif
		uint8_t
		opcodeOctet	= opcode >> 8;
		encoder.encode(opcodeOctet);
		opcodeOctet	= opcode >> 0;
		encoder.encode(opcodeOctet);
		return;
		}

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s: 1 octet\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif

	uint8_t
	opcodeOctet	= opcode >> 0;
	encoder.encode(opcodeOctet);
	}

