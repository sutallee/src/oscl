/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <new>
#include "part.h"
#include "oscl/encoder/be/base.h"
#include "oscl/error/info.h"
#include "oscl/error/fatal.h"
#include "oscl/aes128/ccm.h"
#include "oscl/bluetooth/mesh/address/types.h"
#include "oscl/bluetooth/mesh/address/values.h"
#include "oscl/bluetooth/mesh/model/key/iterator/composer.h"
#include "oscl/bluetooth/mesh/key/iterator/composer.h"
#include "oscl/bluetooth/mesh/model/key/persist/iterator/composer.h"
#include "oscl/bluetooth/mesh/constants.h"

using namespace Oscl::BT::Mesh::Model::App::Server;

//#define DEBUG_TRACE

Part::Part(
	Oscl::BT::Mesh::Model::
	App::Server::Context::Api&				context,
	Oscl::Pdu::Memory::Api&					freeStoreApi,
	Oscl::BT::Mesh::Node::Api&				nodeApi,
	Oscl::BT::Mesh::
	Transport::Lower::TX::Api&				lowerTransportApi,
	Oscl::BT::Mesh::Network::Api&			networkApi,
	Oscl::BT::Mesh::Model::Key::
	Persist::Creator::Api&					keyPersistApi,
	uint16_t								elementAddress
	) noexcept:
		_context(context),
		_nodeApi(nodeApi),
		_networkApi(networkApi),
		_keyPersistApi(keyPersistApi),
		_upperTransportTx(
			freeStoreApi,
			nodeApi,
			lowerTransportApi,
			*this,	// keyApi
			networkApi.getAppKeyApi(),
			elementAddress
			)
		{
	for(unsigned i=0;i<nKeyRecordMem;++i){
		_freeKeyRecordMem.put(&_keyRecordMem[i]);
		}

	Oscl::BT::Mesh::Model::Key::
	Persist::Iterator::Composer<Part>
		constructorKeyPersistIterator(
			*this,
			&Part::constructorNextKeyItem
			);

	keyPersistApi.iterate(constructorKeyPersistIterator);
	}

Oscl::BT::Mesh::Transport::Upper::TX::Api&	Part::getUpperTransportApi() noexcept{
	return _upperTransportTx;
	}

void	Part::stop() noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif

	_upperTransportTx.stop();
	_context.stop();
	}


bool	Part::wantsDestination(uint16_t dst) noexcept{


	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif

	Oscl::BT::Mesh::Subscription::Server::Api*
	subscriptionServerApi	= _context.getSubscriptionServerApi();

	if(!subscriptionServerApi){
		return false;
		}

	return subscriptionServerApi->subscriptionMatch(dst);
	}

void	Part::processAccessMessage(
			const void*	frame,
			unsigned	length,
			uint16_t	src,
			uint16_t	dst,
			uint16_t	appKeyIndex
			) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif

	if(appKeyIndex > 0x0FFF){
		return;
		}

	if(!find(appKeyIndex)){
		// AppKey not bound to this model.
		return;
		}

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s: AppKey bound.\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif

	_context.processAccessMessage(
		frame,
		length,
		src,
		dst,
		appKeyIndex
		);
	}

Oscl::BT::Mesh::Model::Api*	Part::getSigModelApi(uint16_t modelID) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif

	if(_context.sigModelIdMatch(modelID)){
		return this;
		}

	return 0;
	}

Oscl::BT::Mesh::Model::Api*	Part::getVendorModelApi(uint16_t cid, uint16_t modelID) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif

	if(_context.vendorModelIdMatch(cid,modelID)){
		return this;
		}

	return 0;
	}

Oscl::BT::Mesh::Model::Api&	Part::getModelApi() noexcept{
	return *this;
	}

Oscl::BT::Mesh::Model::Key::Record*	Part::find(uint16_t appKeyIndex) noexcept{

	for(
		Oscl::BT::Mesh::Model::Key::Record*
		record	= _boundKeys.first();
		record;
		record	= _boundKeys.next(record)
		){
		if(record->index() == appKeyIndex){
			return record;
			}
		}

	return 0;
	}

const Oscl::BT::Mesh::Key::Result::Var::State&	Part::bind(uint16_t appKeyIndex) noexcept{

	if(find(appKeyIndex)){
		// This key is already bound.
		return Oscl::BT::Mesh::Key::Result::Var::getSuccess();
		}

	bool
	failed	= createKey(appKeyIndex);

	if(failed){
		return Oscl::BT::Mesh::Key::Result::Var::getOutOfResources();
		}

	Oscl::BT::Mesh::Model::Key::Persist::Api*
	persistApi	= _keyPersistApi.create(
					appKeyIndex
					);

	if(!persistApi){
		Oscl::Error::Info::log(
			"%s: Unable to create persistence.\n",
			OSCL_PRETTY_FUNCTION
			);
		}

	return Oscl::BT::Mesh::Key::Result::Var::getSuccess();
	}

const Oscl::BT::Mesh::Key::Result::Var::State&	Part::unbind(uint16_t appKeyIndex) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s: appKeyIndex: 0x%4.4X\n",
		__PRETTY_FUNCTION__,
		appKeyIndex
		);
	#endif

	/*	4.4.1.2.11 Model to AppKey List state
		When an element receives a Config Model App Unbind
		message that is successfully processed (i.e., it
		does not result in any error conditions listed in Table 4.116),
		it shall unbind the AppKey referenced by the AppKeyIndex from
		the identified model and respond with a Config Model App Status
		message, setting the ElementAddress, AppKeyIndex, and
		ModelIdentifier fields as defined by the incoming message, and
		setting the Status field to Success.

		When the specified AppKeyIndex is used by the identified model
		as a Publish AppKeyIndex the Model Publication, the publication
		for the model shall be disabled.
	 */
	Oscl::BT::Mesh::Publication::Api*
	publicationApi	= _context.getPublicationApi();

	if(publicationApi){
		if(appKeyIndex == _context.getPublicationAppKeyIndex()){
			// Disable Publishing
			publicationApi->setPublicationParameters(
					Oscl::BT::Mesh::Address::unassigned,	// publishAddress,
					0,		// appKeyIndex,
					false,	// credentialFlag
					0,		// publishTTL
					0,		// publishPeriod
					0,		// publishRetransmitCount
					0		// publishRetransmitIntervalSteps
					);
			}
		}

	Oscl::BT::Mesh::Model::Key::Record*
	record	= find(appKeyIndex);

	if(!record){
		return Oscl::BT::Mesh::Key::Result::Var::getNoSuchIndex();
		}

	_boundKeys.remove(record);

	_freeKeyRecordMem.put((Oscl::BT::Mesh::Model::Key::RecordMem*)record);

	Oscl::BT::Mesh::Model::Key::Persist::Api*
	keyPersistApi	= findPersistKeyApi(appKeyIndex);

	if(!keyPersistApi){
		Oscl::Error::Info::log(
			"%s: no persistent appKeyIndex 0x%4.4X.\n",
			OSCL_PRETTY_FUNCTION,
			appKeyIndex
			);
		}
	else {
		_keyPersistApi.destroy(*keyPersistApi);
		}

	return Oscl::BT::Mesh::Key::Result::Var::getSuccess();
	}

Oscl::BT::Mesh::Model::Key::Api*	Part::getModelKeyApi() noexcept{

	if(!_context.modelSupportsAppKeyBinding()){
		return 0;
		}

	return this;
	}

bool	Part::sigModelIdMatch(uint16_t modelID) const noexcept{
	return _context.sigModelIdMatch(modelID);
	}

bool	Part::vendorModelIdMatch(uint16_t vendorID, uint16_t modelID) const noexcept{
	return _context.vendorModelIdMatch(vendorID,modelID);
	}

bool	Part::modelSupportsAppKeyBinding() const noexcept{
	return _context.modelSupportsAppKeyBinding();
	}

Oscl::BT::Mesh::Publication::Api*	Part::getPublicationApi() noexcept{
	return _context.getPublicationApi();
	}

Oscl::BT::Mesh::Subscription::Api*	Part::getSubscriptionApi() noexcept{
	return _context.getSubscriptionApi();
	}

bool	Part::createKey(
			uint16_t	keyIndex
			) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif

	Oscl::BT::Mesh::Model::Key::RecordMem*
	mem	= _freeKeyRecordMem.get();

	if(!mem){
		return true;
		}

	Oscl::BT::Mesh::Model::Key::Record*
	record	= new(mem)
				Oscl::BT::Mesh::Model::Key::Record(
					keyIndex
					);

	_boundKeys.put(record);

	return false;
	}

bool	Part::iterate(Oscl::BT::Mesh::Model::Key::Iterator::Api& iterator) noexcept{

	for(
		Oscl::BT::Mesh::Model::Key::Record*
		record	= _boundKeys.first();
		record;
		record	= _boundKeys.next(record)
		){
		bool
		stop	= iterator.item(record->index());
		if(stop){
			return true;
			}
		}

	return false;
	}

bool	Part::constructorNextKeyItem(
			Oscl::BT::Mesh::
			Model::Key::Persist::Api&	api,
			uint16_t					keyIndex
			) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n"
		" &api: %p\n"
		" keyIndex: 0x%4.4X\n"
		"",
		__PRETTY_FUNCTION__,
		&api,
		keyIndex
		);
	#endif

	bool
	failed	= createKey(
				keyIndex
				);

	bool
	haltIteration	= failed?true:false;

	return haltIteration;
	}

Oscl::BT::Mesh::Model::Key::Persist::Api*
	Part::findPersistKeyApi(uint16_t keyIndex) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s: keyIndex: 0x%4.4X\n",
		__PRETTY_FUNCTION__,
		keyIndex
		);
	#endif

	_findKeyIndex	= keyIndex;

	_findPersistApi	= 0;

	Oscl::BT::Mesh::Model::Key::
	Persist::Iterator::Composer<Part>
		findKeyPersistIterator(
			*this,
			&Part::findNextKeyItem
			);

	_keyPersistApi.iterate(findKeyPersistIterator);

	return _findPersistApi;
	}

bool	Part::findNextKeyItem(
			Oscl::BT::Mesh::
			Model::Key::Persist::Api&	api,
			uint16_t					keyIndex
			) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n"
		" &api: %p\n"
		" keyIndex: 0x%4.4X\n"
		"",
		__PRETTY_FUNCTION__,
		&api,
		keyIndex
		);
	#endif

	if(_findKeyIndex == keyIndex){
		_findPersistApi	= &api;
		return true;	// halt iteration
		}

	return false;
	}

