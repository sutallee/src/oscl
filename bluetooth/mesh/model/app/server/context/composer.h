/*
   Copyright (C) 2021 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_bluetooth_mesh_model_app_server_context_composerh_
#define _oscl_bluetooth_mesh_model_app_server_context_composerh_

#include "api.h"

/** */
namespace Oscl {
/** */
namespace BT {
/** */
namespace Mesh {
/** */
namespace Model {
/** */
namespace App {
/** */
namespace Server {
/** */
namespace Context {

/**	This template class implements a kind of GOF decorator
	pattern allows the context to employ composition instead
	of inheritance. Frequently, a context may need to implement
	more than one interface that may result in method name
	clashes. This template solves the problem by implementing
	the interface and then invoking member function pointers
	in the context instead. One instance of this object is
	created in the context for each interface of this type used
	in the context. Each instance is constructed such that it
	refers to different member functions within the context.
	The interface of this object is passed to any object that
	requires the interface. When the object invokes any of the
	operations of the API, the corresponding member function
	of the context will subsequently be invoked.
 */
template <class Context>
class Composer : public Api {
	private:
		/** A reference to the Context.
		 */
		Context&	_context;

	private:
		/**	*/
		void	(Context::*_processAccessMessage)(
					const void*	frame,
					unsigned	length,
					uint16_t	src,
					uint16_t	dst,
					uint16_t	appKeyIndex
					);

		/**	*/
		bool	(Context::*_sigModelIdMatch)(uint16_t modelID) const;

		/**	*/
		bool	(Context::*_vendorModelIdMatch)(
					uint16_t	vendorID,
					uint16_t	modelID
					) const;

		/**	*/
		bool	(Context::*_modelSupportsAppKeyBinding)() const;

		/**	*/
		Oscl::BT::Mesh::Publication::Api*	(Context::*_getPublicationApi)();

		/**	*/
		uint16_t	(Context::*_getPublicationAppKeyIndex)() const;

		/**	*/
		Oscl::BT::Mesh::Subscription::Api*	(Context::*_getSubscriptionApi)();

		/**	*/
		Oscl::BT::Mesh::Subscription::Server::Api*	(Context::*_getSubscriptionServerApi)();

		/**	*/
		void	(Context::*_stop)();

	public:
		/**	*/
		Composer(
			Context&	context,
			void		(Context::*processAccessMessage)(
							const void*	frame,
							unsigned	length,
							uint16_t	src,
							uint16_t	dst,
							uint16_t	appKeyIndex
							),
			bool		(Context::*sigModelIdMatch)(uint16_t modelID) const,
			bool		(Context::*vendorModelIdMatch)(
							uint16_t	vendorID,
							uint16_t	modelID
							) const,
			bool		(Context::*modelSupportsAppKeyBinding)() const,
			Oscl::BT::Mesh::Publication::Api*	(Context::*getPublicationApi)(),
			uint16_t	(Context::*getPublicationAppKeyIndex)() const,
			Oscl::BT::Mesh::Subscription::Api*	(Context::*getSubscriptionApi)(),
			Oscl::BT::Mesh::Subscription::Server::Api*	(Context::*getSubscriptionServerApi)(),
			void		(Context::*stop)()
			) noexcept;

	public:
		/** Process the decrypted Access Message
		 */
		void	processAccessMessage(
					const void*	frame,
					unsigned	length,
					uint16_t	src,
					uint16_t	dst,
					uint16_t	appKeyIndex
					) noexcept;

		/** RETURN: true for match */
		bool	sigModelIdMatch(uint16_t modelID) const noexcept;

		/** RETURN: true for match */
		bool	vendorModelIdMatch(uint16_t vendorID, uint16_t modelID) const noexcept;

		/** RETURN: true if the model supports AppKey binding. */
		bool	modelSupportsAppKeyBinding() const noexcept;

		/** RETURN: zero if the model does NOT support publication.
		 */
		Oscl::BT::Mesh::Publication::Api*	getPublicationApi() noexcept;

		/** If the model does NOT support publication, then
			this operation will never be called. Therefore,
			it may simply return zero.
		 */
		uint16_t	getPublicationAppKeyIndex() const noexcept;

		/** RETURN: zero if the model does NOT support subscription.
		 */
		Oscl::BT::Mesh::Subscription::Api*	getSubscriptionApi() noexcept;

		/** RETURN: zero if the model does NOT support subscription.
		 */
		Oscl::BT::Mesh::Subscription::Server::Api*	getSubscriptionServerApi() noexcept;

		/** */
		void	stop() noexcept;
	};

template <class Context>
Composer<Context>::Composer(
	Context&	context,
	void		(Context::*processAccessMessage)(
					const void*	frame,
					unsigned	length,
					uint16_t	src,
					uint16_t	dst,
					uint16_t	appKeyIndex
					),
	bool		(Context::*sigModelIdMatch)(uint16_t modelID) const,
	bool		(Context::*vendorModelIdMatch)(
					uint16_t	vendorID,
					uint16_t	modelID
					) const,
	bool		(Context::*modelSupportsAppKeyBinding)() const,
	Oscl::BT::Mesh::Publication::Api*	(Context::*getPublicationApi)(),
	uint16_t	(Context::*getPublicationAppKeyIndex)() const,
	Oscl::BT::Mesh::Subscription::Api*	(Context::*getSubscriptionApi)(),
	Oscl::BT::Mesh::Subscription::Server::Api*	(Context::*getSubscriptionServerApi)(),
	void		(Context::*stop)()
	) noexcept:
		_context(context),
		_processAccessMessage(processAccessMessage),
		_sigModelIdMatch(sigModelIdMatch),
		_vendorModelIdMatch(vendorModelIdMatch),
		_modelSupportsAppKeyBinding(modelSupportsAppKeyBinding),
		_getPublicationApi(getPublicationApi),
		_getPublicationAppKeyIndex(getPublicationAppKeyIndex),
		_getSubscriptionApi(getSubscriptionApi),
		_getSubscriptionServerApi(getSubscriptionServerApi),
		_stop(stop)
		{
	}

template <class Context>
void	Composer<Context>::processAccessMessage(
			const void*	frame,
			unsigned	length,
			uint16_t	src,
			uint16_t	dst,
			uint16_t	appKeyIndex
			) noexcept{

	(_context.*_processAccessMessage)(
			frame,
			length,
			src,
			dst,
			appKeyIndex
			);
	}

template <class Context>
bool	Composer<Context>::sigModelIdMatch(uint16_t modelID) const noexcept{
	return (_context.*_sigModelIdMatch)(
			modelID
			);
	}

template <class Context>
bool	Composer<Context>::vendorModelIdMatch(uint16_t vendorID, uint16_t modelID) const noexcept{
	return (_context.*_vendorModelIdMatch)(
			vendorID,
			modelID
			);
	}

template <class Context>
bool	Composer<Context>::modelSupportsAppKeyBinding() const noexcept{
	return (_context.*_modelSupportsAppKeyBinding)();
	}

template <class Context>
Oscl::BT::Mesh::Publication::Api*
Composer<Context>::getPublicationApi() noexcept{
	return (_context.*_getPublicationApi)();
	}

template <class Context>
uint16_t	Composer<Context>::getPublicationAppKeyIndex() const noexcept{
	return (_context.*_getPublicationAppKeyIndex)();
	}

template <class Context>
Oscl::BT::Mesh::Subscription::Api*
Composer<Context>::getSubscriptionApi() noexcept{
	return (_context.*_getSubscriptionApi)();
	}

template <class Context>
Oscl::BT::Mesh::Subscription::Server::Api*
Composer<Context>::getSubscriptionServerApi() noexcept{
	return (_context.*_getSubscriptionServerApi)();
	}

template <class Context>
void	Composer<Context>::stop() noexcept{
	return (_context.*_stop)();
	}

}
}
}
}
}
}
}

#endif
