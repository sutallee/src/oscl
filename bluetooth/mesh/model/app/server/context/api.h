/*
   Copyright (C) 2021 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_bluetooth_mesh_model_app_server_context_apih_
#define _oscl_bluetooth_mesh_model_app_server_context_apih_

#include "oscl/bluetooth/mesh/publication/api.h"
#include "oscl/bluetooth/mesh/subscription/api.h"
#include "oscl/bluetooth/mesh/subscription/server/api.h"

/** */
namespace Oscl {
/** */
namespace BT {
/** */
namespace Mesh {
/** */
namespace Model {
/** */
namespace App {
/** */
namespace Server {
/** */
namespace Context {

/** */
class Api {
	public:
		/** Process the decrypted Access Message
			FIXME: Change this to match 
				Oscl::BT::Mesh::Message::Api::receive(...)
				such that opcode decoding happens inside
				the App::Server::Part.
		 */
		virtual void	processAccessMessage(
							const void*	frame,
							unsigned	length,
							uint16_t	src,
							uint16_t	dst,
							uint16_t	appKeyIndex
							) noexcept=0;

		/** RETURN: true for match */
		virtual bool	sigModelIdMatch(uint16_t modelID) const noexcept=0;

		/** RETURN: true for match */
		virtual bool	vendorModelIdMatch(uint16_t vendorID, uint16_t modelID) const noexcept=0;

		/** RETURN: true if the model supports AppKey binding. */
		virtual bool	modelSupportsAppKeyBinding() const noexcept=0;

		/** RETURN: zero if the model does NOT support publication.
		 */
		virtual Oscl::BT::Mesh::Publication::Api*	getPublicationApi() noexcept=0;

		/** If the model does NOT support publication, then
			this operation will never be called. Therefore,
			it may simply return zero.
		 */
		virtual uint16_t	getPublicationAppKeyIndex() const noexcept=0;

		/** RETURN: zero if the model does NOT support subscription.
		 */
		virtual Oscl::BT::Mesh::Subscription::Api*	getSubscriptionApi() noexcept=0;

		/** RETURN: zero if the model does NOT support subscription.
		 */
		virtual Oscl::BT::Mesh::Subscription::Server::Api*	getSubscriptionServerApi() noexcept=0;

		/** */
		virtual void	stop() noexcept=0;
	};

}
}
}
}
}
}
}

#endif
