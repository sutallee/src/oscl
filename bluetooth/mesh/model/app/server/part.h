/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_bluetooth_mesh_model_app_server_parth_
#define _oscl_bluetooth_mesh_model_app_server_parth_

#include "oscl/queue/queue.h"
#include "oscl/bluetooth/mesh/node/api.h"
#include "oscl/bluetooth/mesh/model/rx/item.h"
#include "oscl/bluetooth/mesh/transport/upper/tx/app/part.h"
#include "oscl/pdu/memory/api.h"
#include "oscl/bluetooth/mesh/model/api.h"
#include "oscl/bluetooth/mesh/network/api.h"
#include "oscl/bluetooth/mesh/model/key/api.h"
#include "oscl/bluetooth/mesh/model/key/record.h"
#include "oscl/bluetooth/mesh/model/key/persist/creator/api.h"
#include "oscl/bluetooth/mesh/transport/lower/tx/api.h"
#include "oscl/bluetooth/mesh/subscription/api.h"
#include "oscl/bluetooth/mesh/subscription/server/api.h"
#include "oscl/bluetooth/mesh/model/app/server/context/api.h"

/** */
namespace Oscl {
/** */
namespace BT {
/** */
namespace Mesh {
/** */
namespace Model {
/** */
namespace App {
/** */
namespace Server {

/** This is an implementation of the Bluetooth
	Mesh Foundation model.
 */
class Part :
	public Oscl::BT::Mesh::Model::RX::Item,
	public Oscl::BT::Mesh::Model::Api,
	private Oscl::BT::Mesh::Model::Key::Api
	{
	private:
		/** */
		Oscl::BT::Mesh::Model::
		App::Server::Context::Api&		_context;

		/** */
		Oscl::BT::Mesh::Node::Api&		_nodeApi;

		/** */
		Oscl::BT::Mesh::Network::Api&	_networkApi;

		/** */
		Oscl::BT::Mesh::
		Model::Key::Persist::
		Creator::Api&					_keyPersistApi;

	private:
		/** */
		Oscl::BT::Mesh::Transport::
		Upper::TX::App::Part		_upperTransportTx;

	private:
		/** */
		constexpr static unsigned	nKeyRecordMem	= 2;

		/** */
		Oscl::BT::Mesh::Model::Key::RecordMem	_keyRecordMem[nKeyRecordMem];

		/** */
		Oscl::Queue<
			Oscl::BT::Mesh::
			Model::Key::RecordMem
			>							_freeKeyRecordMem;

		/** */
		Oscl::Queue<
			Oscl::BT::Mesh::
			Model::Key::Record
			>							_boundKeys;

	private:
		/** */
		uint16_t					_findKeyIndex;

		/** */
		Oscl::BT::Mesh::
		Model::Key::Persist::Api*	_findPersistApi;

	public:
		/** */
		Part(
			Oscl::BT::Mesh::Model::
			App::Server::Context::Api&				context,
			Oscl::Pdu::Memory::Api&					freeStoreApi,
			Oscl::BT::Mesh::Node::Api&				nodeApi,
			Oscl::BT::Mesh::
			Transport::Lower::TX::Api&				lowerTransportApi,
			Oscl::BT::Mesh::Network::Api&			networkApi,
			Oscl::BT::Mesh::Model::Key::
			Persist::Creator::Api&					keyPersistApi,
			uint16_t								elementAddress
			) noexcept;

		/** */
		Oscl::BT::Mesh::Transport::Upper::TX::Api&	getUpperTransportApi() noexcept;

	private:
		/** */
		Oscl::BT::Mesh::Model::Key::Record*	find(uint16_t appKeyIndex) noexcept;

		/** RETURN: 0 if there is no matching index. */
		Oscl::BT::Mesh::Model::Key::Persist::Api*	findPersistKeyApi(uint16_t keyIndex) noexcept;

	private: // Oscl::BT::Mesh::Model::RX::Item
		/**	*/
		void	stop() noexcept;

		/** This operation is used to determine if
			the model subscribes to the specified
			destination address. This is only called
			by the containing Element if the destination
			address is not a unicast address. 

			param [in]	dst	The destination address in question.

			RETURN: true if the model is subscribed
			to the destination address (dst).
		 */
		bool	wantsDestination(uint16_t dst) noexcept;

		/** Process the decrypted Access Message
			The appKeyIndex is greater than 0x0FFF
			if this message is was decrypted using
			the DevKey.
		 */
		void	processAccessMessage(
					const void*	frame,
					unsigned	length,
					uint16_t	src,
					uint16_t	dst,
					uint16_t	appKeyIndex
					) noexcept;

		/** RETURN: zero if there is no matching SIG model.
		 */
		Oscl::BT::Mesh::Model::Api*	getSigModelApi(uint16_t modelID) noexcept;

		/** RETURN: zero if there is no matching vendor model.
		 */
		Oscl::BT::Mesh::Model::Api*	getVendorModelApi(uint16_t cid, uint16_t modelID) noexcept;

	public:
		/** */
		Oscl::BT::Mesh::Model::Api&	getModelApi() noexcept;

	private: // Oscl::BT::Mesh::Model::Api
		/** */
		const Oscl::BT::Mesh::Key::Result::Var::State&	bind(uint16_t appKeyIndex) noexcept;

		/** RETURN: true for failure. */
		const Oscl::BT::Mesh::Key::Result::Var::State&	unbind(uint16_t appKeyIndex) noexcept;

		/** RETURN: 0 if binding is not supported. */
		Oscl::BT::Mesh::Model::Key::Api*	getModelKeyApi() noexcept;

		/** RETURN: true for match */
		bool	sigModelIdMatch(uint16_t modelID) const noexcept;

		/** RETURN: true for match */
		bool	vendorModelIdMatch(uint16_t vendorID, uint16_t modelID) const noexcept;

		/** RETURN: true if the model supports AppKey binding. */
		bool	modelSupportsAppKeyBinding() const noexcept;

		/** RETURN: zero if the model does NOT support publication.
		 */
		Oscl::BT::Mesh::Publication::Api*	getPublicationApi() noexcept;

		/** RETURN: zero if the model does NOT support subscription.
		 */
		Oscl::BT::Mesh::Subscription::Api*	getSubscriptionApi() noexcept;

	private: // Oscl::BT::Mesh::Model::Key::Api
		/** RETURN: true if item found or iteration stopped. */
		bool	iterate(Oscl::BT::Mesh::Model::Key::Iterator::Api& iterator) noexcept;

	private:
		/** RETURN: true if un-supported or failed. */
		bool	createKey(
					uint16_t	keyIndex
					) noexcept;


	private: // Oscl::BT::Mesh::Model::Key::Persist::Iterator::Composer

		/**	This iteration callback is invoked for each
			persistent item.

			RETURN: true to stop iteration or false
			to continue the iteration.
		 */
		bool	constructorNextKeyItem(
					Oscl::BT::Mesh::
					Model::Key::Persist::Api&	api,
					uint16_t					keyIndex
					) noexcept;

	private: // Oscl::BT::Mesh::Model::Key::Persist::Iterator::Composer

		/**	This iteration callback is invoked for each
			persistent item.

			RETURN: true to stop iteration or false
			to continue the iteration.
		 */
		bool	findNextKeyItem(
					Oscl::BT::Mesh::
					Model::Key::Persist::Api&	api,
					uint16_t					keyIndex
					) noexcept;
	};

}
}
}
}
}
}

#endif
