/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <new>
#include "part.h"
#include "oscl/endian/decoder/linear/part.h"
#include "oscl/encoder/le/base.h"
#include "oscl/bluetooth/mesh/address/virtual.h"
#include "oscl/bluetooth/mesh/address/types.h"
#include "oscl/bluetooth/mesh/address/values.h"
#include "oscl/bluetooth/mesh/model/opcode/decoder.h"
#include "oscl/error/info.h"
#include "oscl/bluetooth/mesh/constants.h"
#include "oscl/bluetooth/mesh/model/opcode/encoder.h"
#include "oscl/bluetooth/mesh/model/opcode/values.h"

using namespace Oscl::BT::Mesh::Message::Light::Lightness::Server;

//#define DEBUG_TRACE
//#define DEBUG_TRACE_TX_STATUS

Part::Part(
	Oscl::BT::Mesh::State::
	Light::Lightness::Api&		contextApi,
	Oscl::BT::Mesh::
	Transport::Upper::TX::Api&	transportApi,
	Oscl::Timer::Api&			tidTimerApi,
	bool						allowSegmentedStatus
	) noexcept:
		_contextApi(contextApi),
		_transportApi(transportApi),
		_tidTimerApi(tidTimerApi),
		_allowSegmentedStatus(allowSegmentedStatus),
		_rxLightLightnessGet(
			*this,
			&Part::rxLightLightnessGet
			),
		_rxLightLightnessSet(
			*this,
			&Part::rxLightLightnessSet
			),
		_rxLightLightnessSetUnacknowledged(
			*this,
			&Part::rxLightLightnessSetUnacknowledged
			),
		_rxLightLightnessLinearGet(
			*this,
			&Part::rxLightLightnessLinearGet
			),
		_rxLightLightnessLinearSet(
			*this,
			&Part::rxLightLightnessLinearSet
			),
		_rxLightLightnessLinearSetUnacknowledged(
			*this,
			&Part::rxLightLightnessLinearSetUnacknowledged
			),
		_rxLightLightnessLastGet(
			*this,
			&Part::rxLightLightnessLastGet
			),
		_rxLightLightnessDefaultGet(
			*this,
			&Part::rxLightLightnessDefaultGet
			),
		_rxLightLightnessDefaultSet(
			*this,
			&Part::rxLightLightnessDefaultSet
			),
		_rxLightLightnessDefaultSetUnacknowledged(
			*this,
			&Part::rxLightLightnessDefaultSetUnacknowledged
			),
		_rxLightLightnessRangeGet(
			*this,
			&Part::rxLightLightnessRangeGet
			),
		_rxLightLightnessRangeSet(
			*this,
			&Part::rxLightLightnessRangeSet
			),
		_rxLightLightnessRangeSetUnacknowledged(
			*this,
			&Part::rxLightLightnessRangeSetUnacknowledged
			),
		_receiver(
			*this,
			&Part::receive
			),
		_lastLightnessTID(~0),
		_lastLightnessSrcAddress(Oscl::BT::Mesh::Address::unassigned),
		_lastLightnessDstAddress(Oscl::BT::Mesh::Address::unassigned),
		_lastLightnessLinearTID(~0),
		_lastLightnessLinearSrcAddress(Oscl::BT::Mesh::Address::unassigned),
		_lastLightnessLinearDstAddress(Oscl::BT::Mesh::Address::unassigned)
		{

	_messageHandlers.put(&_rxLightLightnessGet);
	_messageHandlers.put(&_rxLightLightnessSet);
	_messageHandlers.put(&_rxLightLightnessSetUnacknowledged);
	_messageHandlers.put(&_rxLightLightnessLinearGet);
	_messageHandlers.put(&_rxLightLightnessLinearSet);
	_messageHandlers.put(&_rxLightLightnessLinearSetUnacknowledged);
	_messageHandlers.put(&_rxLightLightnessLastGet);
	_messageHandlers.put(&_rxLightLightnessDefaultGet);
	_messageHandlers.put(&_rxLightLightnessDefaultSet);
	_messageHandlers.put(&_rxLightLightnessDefaultSetUnacknowledged);
	_messageHandlers.put(&_rxLightLightnessRangeGet);
	_messageHandlers.put(&_rxLightLightnessRangeSet);
	_messageHandlers.put(&_rxLightLightnessRangeSetUnacknowledged);

	_tidTimerApi.setExpirationCallback(*this);
	}

Part::~Part() noexcept{
	}

Oscl::BT::Mesh::Message::Item&	Part::getRxItem() noexcept{
	return _receiver;
	}

bool	Part::receive(
			uint32_t	opcode,
			uint16_t	srcAddress,
			uint16_t	dstAddress,
			const void*	frame,
			unsigned	length,
			uint16_t	appKeyIndex
			) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);

	Oscl::Error::Info::hexDump(
		frame,
		length
		);
	#endif

	for(
		Oscl::BT::Mesh::Message::Item*	item	= _messageHandlers.first();
		item;
		item	= _messageHandlers.next(item)
		){
		bool
		handled	= item->receive(
					opcode,
					srcAddress,
					dstAddress,
					frame,
					length,
					appKeyIndex
					);
		if(handled){
			return true;
			}
		}

	return false;
	}

bool	Part::rxLightnessSet(
			uint16_t			srcAddress,
			uint16_t			dstAddress,
			const void*			frame,
			unsigned			length,
			uint16_t			appKeyIndex
			) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif

	uint16_t	lightness;
	uint8_t		tid;
	uint8_t		transitionTime;
	uint8_t		delay;

	Oscl::Endian::Decoder::Linear::Part
	leDecoder(
		frame,
		length
		);

	Oscl::Decoder::Api&	decoder	= leDecoder.le();

	decoder.decode(lightness);
	decoder.decode(tid);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			OSCL_PRETTY_FUNCTION
			);
		return true;
		}

	decoder.decode(transitionTime);

	bool	transitionTimeIsValid	= true;

	if(decoder.underflow()){
		transitionTimeIsValid	= false;
		}

	if(transitionTimeIsValid){
		decoder.decode(delay);
		if(decoder.underflow()){
			Oscl::Error::Info::log(
				"%s: underflow! Delay value not present.\n",
				OSCL_PRETTY_FUNCTION
				);
			return true;
			}
		}

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n"
		"\tLightness: %d\n"
		"\tTID: 0x%2.2X\n"
		"",
		__PRETTY_FUNCTION__,
		lightness,
		tid
		);

	if(transitionTimeIsValid){
		Oscl::Error::Info::log(
			"%s\n"
			"\tTransition Time: 0x%2.2X\n"
			"\tDelay: 0x%2.2X\n"
			"",
			OSCL_PRETTY_FUNCTION,
			transitionTime,
			delay
			);
		}
	#endif

	/*
		Mesh Model Spec: 3.3.1.2.2

		When a Generic Light Lightness Server receives a
		Generic Light Lightness Set message or a
		Generic Light Lightness Set Unacknowledged message,
		it shall set the Generic Light Lightness state to
		the Lightness field of the message,
		unless the message has the same value for
		the SRC, DST, and TID fields as the previous
		message received within the past 6 seconds.
	*/

	if(tid == _lastLightnessTID){
		if(srcAddress == _lastLightnessSrcAddress){
			if(dstAddress == _lastLightnessDstAddress){
				/*	It's a repeat, but we still want
					to send a response if required.
				 */
				return false;
				}
			}
		}

	_lastLightnessTID			= tid;
	_lastLightnessSrcAddress	= srcAddress;
	_lastLightnessDstAddress	= dstAddress;

	constexpr unsigned long	tidDelayInMs	= 6000;

	_tidTimerApi.start(tidDelayInMs);

	if(transitionTimeIsValid){

		_contextApi.setLightLightnessState(
			lightness,
			transitionTime,
			delay
			);
		}
	else {

		_contextApi.setLightLightnessState(
			lightness
			);
		}

	return false;
	}

bool	Part::rxLightnessLinearSet(
			uint16_t			srcAddress,
			uint16_t			dstAddress,
			const void*			frame,
			unsigned			length,
			uint16_t			appKeyIndex
			) noexcept{


	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif

	uint16_t	lightness;
	uint8_t		tid;
	uint8_t		transitionTime;
	uint8_t		delay;

	Oscl::Endian::Decoder::Linear::Part
	leDecoder(
		frame,
		length
		);

	Oscl::Decoder::Api&	decoder	= leDecoder.le();

	decoder.decode(lightness);
	decoder.decode(tid);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			OSCL_PRETTY_FUNCTION
			);
		return true;
		}

	decoder.decode(transitionTime);

	bool	transitionTimeIsValid	= true;

	if(decoder.underflow()){
		transitionTimeIsValid	= false;
		}

	if(transitionTimeIsValid){
		decoder.decode(delay);
		if(decoder.underflow()){
			Oscl::Error::Info::log(
				"%s: underflow! Delay value not present.\n",
				OSCL_PRETTY_FUNCTION
				);
			return true;
			}
		}

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n"
		"\tLightness: %d\n"
		"\tTID: 0x%2.2X\n"
		"",
		__PRETTY_FUNCTION__,
		lightness,
		tid
		);

	if(transitionTimeIsValid){
		Oscl::Error::Info::log(
			"%s\n"
			"\tTransition Time: 0x%2.2X\n"
			"\tDelay: 0x%2.2X\n"
			"",
			OSCL_PRETTY_FUNCTION,
			transitionTime,
			delay
			);
		}
	#endif

	/*
		Mesh Model Spec: 3.3.1.2.2

		When a Generic Level Server receives a
		Generic Level Set message or a
		Generic Level Set Unacknowledged message,
		it shall set the Generic Level state to
		the Level field of the message,
		unless the message has the same value for
		the SRC, DST, and TID fields as the previous
		message received within the past 6 seconds.
	*/

	if(tid == _lastLightnessLinearTID){
		if(srcAddress == _lastLightnessLinearSrcAddress){
			if(dstAddress == _lastLightnessLinearDstAddress){
				/*	It's a repeat, but we still want
					to send a response if required.
				 */
				return false;
				}
			}
		}

	_lastLightnessLinearTID		= tid;
	_lastLightnessLinearSrcAddress	= srcAddress;
	_lastLightnessLinearDstAddress	= dstAddress;

	constexpr unsigned long	tidDelayInMs	= 6000;

	_tidTimerApi.start(tidDelayInMs);

	if(transitionTimeIsValid){

		_contextApi.setLightLightnessLinearState(
			lightness,
			transitionTime,
			delay
			);
		}
	else {

		_contextApi.setLightLightnessLinearState(
			lightness
			);
		}

	return false;
	}

bool	Part::rxLightnessDefaultSet(
			uint16_t			srcAddress,
			uint16_t			dstAddress,
			const void*			frame,
			unsigned			length,
			uint16_t			appKeyIndex
			) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif

	uint16_t	lightness;

	Oscl::Endian::Decoder::Linear::Part
	leDecoder(
		frame,
		length
		);

	Oscl::Decoder::Api&	decoder	= leDecoder.le();

	decoder.decode(lightness);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			OSCL_PRETTY_FUNCTION
			);
		return true;
		}

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n"
		"\tLightness: %d\n"
		"",
		__PRETTY_FUNCTION__,
		lightness
		);
	#endif

	_contextApi.setLightLightnessDefaultState(
		lightness
		);

	return false;
	}

bool	Part::rxLightnessRangeSet(
			uint16_t			srcAddress,
			uint16_t			dstAddress,
			const void*			frame,
			unsigned			length,
			uint16_t			appKeyIndex
			) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif

	uint16_t	rangeMin;
	uint16_t	rangeMax;

	Oscl::Endian::Decoder::Linear::Part
	leDecoder(
		frame,
		length
		);

	Oscl::Decoder::Api&	decoder	= leDecoder.le();

	decoder.decode(rangeMin);
	decoder.decode(rangeMax);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			OSCL_PRETTY_FUNCTION
			);
		return true;
		}

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n"
		"\tRangeMin: 0x%4.4X\n"
		"\tRangeMax: 0x%4.4X\n"
		"",
		__PRETTY_FUNCTION__,
		rangeMin,
		rangeMax
		);
	#endif

	_contextApi.setLightLightnessRangeState(
		rangeMin,
		rangeMax
		);

	return false;
	}

bool	Part::rxLightLightnessGet(
			uint32_t	opcode,
			uint16_t	srcAddress,
			uint16_t	dstAddress,
			const void*	frame,
			unsigned	length,
			uint16_t	appKeyIndex
			) noexcept{

	if(opcode != Oscl::BT::Mesh::Model::OpCode::lightLightnessGet){
		return false;
		}

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n"
		"",
		__PRETTY_FUNCTION__
		);
	#endif

	sendLightLightnessStatus(
		appKeyIndex,
		srcAddress,
		0xFF,	// default
		Oscl::BT::Mesh::Address::isUnicastAddress(dstAddress)
		);

	return true;
	}

bool	Part::rxLightLightnessSet(
			uint32_t	opcode,
			uint16_t	srcAddress,
			uint16_t	dstAddress,
			const void*	frame,
			unsigned	length,
			uint16_t	appKeyIndex
			) noexcept{

	if(opcode != Oscl::BT::Mesh::Model::OpCode::lightLightnessSet){
		return false;
		}

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif

	bool
	failed	= rxLightnessSet(
				srcAddress,
				dstAddress,
				frame,
				length,
				appKeyIndex
				);

	if(failed){
		/*	Something was wrong with the
			received packet format.

			FIXME: We MAY want to send a status anyway.
		 */
		return true;
		}

	sendLightLightnessStatus(
		appKeyIndex,
		srcAddress,
		0xFF,	// default
		Oscl::BT::Mesh::Address::isUnicastAddress(dstAddress)
		);

	return true;
	}

bool	Part::rxLightLightnessSetUnacknowledged(
			uint32_t	opcode,
			uint16_t	srcAddress,
			uint16_t	dstAddress,
			const void*	frame,
			unsigned	length,
			uint16_t	appKeyIndex
			) noexcept{

	if(opcode != Oscl::BT::Mesh::Model::OpCode::lightLightnessSetUnacknowledged){
		return false;
		}

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif

	rxLightnessSet(
		srcAddress,
		dstAddress,
		frame,
		length,
		appKeyIndex
		);

	return true;
	}

bool	Part::rxLightLightnessLinearGet(
			uint32_t	opcode,
			uint16_t	srcAddress,
			uint16_t	dstAddress,
			const void*	frame,
			unsigned	length,
			uint16_t	appKeyIndex
			) noexcept{

	if(opcode != Oscl::BT::Mesh::Model::OpCode::lightLightnessLinearGet){
		return false;
		}

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n"
		"",
		__PRETTY_FUNCTION__
		);
	#endif

	sendLightLightnessLinearStatus(
		appKeyIndex,
		srcAddress,
		0xFF,	// default
		Oscl::BT::Mesh::Address::isUnicastAddress(dstAddress)
		);

	return true;
	}

bool	Part::rxLightLightnessLinearSet(
			uint32_t	opcode,
			uint16_t	srcAddress,
			uint16_t	dstAddress,
			const void*	frame,
			unsigned	length,
			uint16_t	appKeyIndex
			) noexcept{

	if(opcode != Oscl::BT::Mesh::Model::OpCode::lightLightnessLinearSet){
		return false;
		}

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif

	bool
	failed	= rxLightnessLinearSet(
				srcAddress,
				dstAddress,
				frame,
				length,
				appKeyIndex
				);

	if(failed){
		/*	Something was wrong with the
			received packet format.

			FIXME: We MAY want to send a status anyway.
		 */
		return true;
		}

	sendLightLightnessLinearStatus(
		appKeyIndex,
		srcAddress,
		0xFF,	// default
		Oscl::BT::Mesh::Address::isUnicastAddress(dstAddress)
		);

	return true;
	}

bool	Part::rxLightLightnessLinearSetUnacknowledged(
			uint32_t	opcode,
			uint16_t	srcAddress,
			uint16_t	dstAddress,
			const void*	frame,
			unsigned	length,
			uint16_t	appKeyIndex
			) noexcept{

	if(opcode != Oscl::BT::Mesh::Model::OpCode::lightLightnessLinearSetUnacknowledged){
		return false;
		}

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif

	rxLightnessLinearSet(
		srcAddress,
		dstAddress,
		frame,
		length,
		appKeyIndex
		);

	return true;
	}

bool	Part::rxLightLightnessLastGet(
			uint32_t	opcode,
			uint16_t	srcAddress,
			uint16_t	dstAddress,
			const void*	frame,
			unsigned	length,
			uint16_t	appKeyIndex
			) noexcept{

	if(opcode != Oscl::BT::Mesh::Model::OpCode::lightLightnessLastGet){
		return false;
		}

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n"
		"",
		__PRETTY_FUNCTION__
		);
	#endif

	sendLightLightnessLastStatus(
		appKeyIndex,
		srcAddress,
		0xFF,	// default
		Oscl::BT::Mesh::Address::isUnicastAddress(dstAddress)
		);

	return true;
	}

bool	Part::rxLightLightnessDefaultGet(
			uint32_t	opcode,
			uint16_t	srcAddress,
			uint16_t	dstAddress,
			const void*	frame,
			unsigned	length,
			uint16_t	appKeyIndex
			) noexcept{

	if(opcode != Oscl::BT::Mesh::Model::OpCode::lightLightnessDefaultGet){
		return false;
		}

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n"
		"",
		__PRETTY_FUNCTION__
		);
	#endif

	sendLightLightnessDefaultStatus(
		appKeyIndex,
		srcAddress,
		0xFF,	// default
		Oscl::BT::Mesh::Address::isUnicastAddress(dstAddress)
		);

	return true;
	}

bool	Part::rxLightLightnessDefaultSet(
			uint32_t	opcode,
			uint16_t	srcAddress,
			uint16_t	dstAddress,
			const void*	frame,
			unsigned	length,
			uint16_t	appKeyIndex
			) noexcept{

	if(opcode != Oscl::BT::Mesh::Model::OpCode::lightLightnessDefaultSet){
		return false;
		}

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif

	bool
	failed	= rxLightnessDefaultSet(
				srcAddress,
				dstAddress,
				frame,
				length,
				appKeyIndex
				);

	if(failed){
		/*	Something was wrong with the
			received packet format.

			FIXME: We MAY want to send a status anyway.
		 */
		return true;
		}

	sendLightLightnessDefaultStatus(
		appKeyIndex,
		srcAddress,
		0xFF,	// default
		Oscl::BT::Mesh::Address::isUnicastAddress(dstAddress)
		);

	return true;
	}

bool	Part::rxLightLightnessDefaultSetUnacknowledged(
			uint32_t	opcode,
			uint16_t	srcAddress,
			uint16_t	dstAddress,
			const void*	frame,
			unsigned	length,
			uint16_t	appKeyIndex
			) noexcept{

	if(opcode != Oscl::BT::Mesh::Model::OpCode::lightLightnessDefaultSetUnacknowledged){
		return false;
		}

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif

	rxLightnessDefaultSet(
		srcAddress,
		dstAddress,
		frame,
		length,
		appKeyIndex
		);

	return true;
	}

bool	Part::rxLightLightnessRangeGet(
			uint32_t	opcode,
			uint16_t	srcAddress,
			uint16_t	dstAddress,
			const void*	frame,
			unsigned	length,
			uint16_t	appKeyIndex
			) noexcept{

	if(opcode != Oscl::BT::Mesh::Model::OpCode::lightLightnessRangeGet){
		return false;
		}

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n"
		"",
		__PRETTY_FUNCTION__
		);
	#endif

	sendLightLightnessRangeStatus(
		appKeyIndex,
		srcAddress,
		0xFF,	// default
		Oscl::BT::Mesh::Address::isUnicastAddress(dstAddress)
		);

	return true;
	}

bool	Part::rxLightLightnessRangeSet(
			uint32_t	opcode,
			uint16_t	srcAddress,
			uint16_t	dstAddress,
			const void*	frame,
			unsigned	length,
			uint16_t	appKeyIndex
			) noexcept{

	if(opcode != Oscl::BT::Mesh::Model::OpCode::lightLightnessRangeSet){
		return false;
		}

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif

	bool
	failed	= rxLightnessRangeSet(
				srcAddress,
				dstAddress,
				frame,
				length,
				appKeyIndex
				);

	if(failed){
		/*	Something was wrong with the
			received packet format.

			FIXME: We MAY want to send a status anyway.
		 */
		return true;
		}

	sendLightLightnessRangeStatus(
		appKeyIndex,
		srcAddress,
		0xFF,	// default
		Oscl::BT::Mesh::Address::isUnicastAddress(dstAddress)
		);

	return true;
	}

bool	Part::rxLightLightnessRangeSetUnacknowledged(
			uint32_t	opcode,
			uint16_t	srcAddress,
			uint16_t	dstAddress,
			const void*	frame,
			unsigned	length,
			uint16_t	appKeyIndex
			) noexcept{

	if(opcode != Oscl::BT::Mesh::Model::OpCode::lightLightnessRangeSetUnacknowledged){
		return false;
		}

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif

	rxLightnessRangeSet(
		srcAddress,
		dstAddress,
		frame,
		length,
		appKeyIndex
		);

	return true;
	}

void	Part::sendLightLightnessStatus(
			uint16_t	appKeyIndex,
			uint16_t	dstAddress,
			uint8_t		ttl,
			bool		sendSegmented,
			const void*	authData,
			unsigned	authDataLen
			) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif

	uint8_t	buffer[maxAccessPduSizeWithTransMic32];

	Oscl::Encoder::LE::Base
	leEncoder(
		buffer,
		sizeof(buffer)
		);

	Oscl::Encoder::Api&	encoder	= leEncoder;

	Oscl::BT::Mesh::Model::OpCode::encode(
		encoder,
		Oscl::BT::Mesh::Model::OpCode::lightLightnessStatus
		);


	uint16_t	presentLightness;
	uint16_t	targetLightness;
	uint8_t		remainingTime;

	_contextApi.getLightLightnessState(
		presentLightness,
		targetLightness,
		remainingTime
		);

	encoder.encode(presentLightness);
	encoder.encode(targetLightness);
	encoder.encode(remainingTime);

	if(encoder.overflow()){
		Oscl::Error::Info::log(
			"%s: overflow!\n",
			OSCL_PRETTY_FUNCTION
			);
		return;
		}

	sendStatus(
		buffer,
		encoder.length(),
		dstAddress,	// dst
		appKeyIndex,
		ttl,
		authData,
		authDataLen,
		sendSegmented
		);
	}

void	Part::sendLightLightnessLinearStatus(
			uint16_t	appKeyIndex,
			uint16_t	dstAddress,
			uint8_t		ttl,
			bool		sendSegmented,
			const void*	authData,
			unsigned	authDataLen
			) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif

	uint8_t	buffer[maxAccessPduSizeWithTransMic32];

	Oscl::Encoder::LE::Base
	leEncoder(
		buffer,
		sizeof(buffer)
		);

	Oscl::Encoder::Api&	encoder	= leEncoder;

	Oscl::BT::Mesh::Model::OpCode::encode(
		encoder,
		Oscl::BT::Mesh::Model::OpCode::lightLightnessLinearStatus
		);


	uint16_t	presentLightness;
	uint16_t	targetLightness;
	uint8_t		remainingTime;

	_contextApi.getLightLightnessLinearState(
		presentLightness,
		targetLightness,
		remainingTime
		);

	encoder.encode(presentLightness);
	encoder.encode(targetLightness);
	encoder.encode(remainingTime);

	if(encoder.overflow()){
		Oscl::Error::Info::log(
			"%s: overflow!\n",
			OSCL_PRETTY_FUNCTION
			);
		return;
		}

	sendStatus(
		buffer,
		encoder.length(),
		dstAddress,	// dst
		appKeyIndex,
		ttl,
		authData,
		authDataLen,
		sendSegmented
		);
	}

void	Part::sendLightLightnessLastStatus(
			uint16_t	appKeyIndex,
			uint16_t	dstAddress,
			uint8_t		ttl,
			bool		sendSegmented,
			const void*	authData,
			unsigned	authDataLen
			) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif

	uint8_t	buffer[maxAccessPduSizeWithTransMic32];

	Oscl::Encoder::LE::Base
	leEncoder(
		buffer,
		sizeof(buffer)
		);

	Oscl::Encoder::Api&	encoder	= leEncoder;

	Oscl::BT::Mesh::Model::OpCode::encode(
		encoder,
		Oscl::BT::Mesh::Model::OpCode::lightLightnessLastStatus
		);


	uint16_t	lightness;

	_contextApi.getLightLightnessLastState(
		lightness
		);

	encoder.encode(lightness);

	if(encoder.overflow()){
		Oscl::Error::Info::log(
			"%s: overflow!\n",
			OSCL_PRETTY_FUNCTION
			);
		return;
		}

	sendStatus(
		buffer,
		encoder.length(),
		dstAddress,	// dst
		appKeyIndex,
		ttl,
		authData,
		authDataLen,
		sendSegmented
		);
	}

void	Part::sendLightLightnessDefaultStatus(
			uint16_t	appKeyIndex,
			uint16_t	dstAddress,
			uint8_t		ttl,
			bool		sendSegmented,
			const void*	authData,
			unsigned	authDataLen
			) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif

	uint8_t	buffer[maxAccessPduSizeWithTransMic32];

	Oscl::Encoder::LE::Base
	leEncoder(
		buffer,
		sizeof(buffer)
		);

	Oscl::Encoder::Api&	encoder	= leEncoder;

	Oscl::BT::Mesh::Model::OpCode::encode(
		encoder,
		Oscl::BT::Mesh::Model::OpCode::lightLightnessDefaultStatus
		);


	uint16_t	lightness;

	_contextApi.getLightLightnessDefaultState(
		lightness
		);

	encoder.encode(lightness);

	if(encoder.overflow()){
		Oscl::Error::Info::log(
			"%s: overflow!\n",
			OSCL_PRETTY_FUNCTION
			);
		return;
		}

	sendStatus(
		buffer,
		encoder.length(),
		dstAddress,	// dst
		appKeyIndex,
		ttl,
		authData,
		authDataLen,
		sendSegmented
		);
	}

void	Part::sendLightLightnessRangeStatus(
			uint16_t	appKeyIndex,
			uint16_t	dstAddress,
			uint8_t		ttl,
			bool		sendSegmented,
			const void*	authData,
			unsigned	authDataLen
			) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif

	uint8_t	buffer[maxAccessPduSizeWithTransMic32];

	Oscl::Encoder::LE::Base
	leEncoder(
		buffer,
		sizeof(buffer)
		);

	Oscl::Encoder::Api&	encoder	= leEncoder;

	Oscl::BT::Mesh::Model::OpCode::encode(
		encoder,
		Oscl::BT::Mesh::Model::OpCode::lightLightnessRangeStatus
		);


	uint8_t		statusCode;
	uint16_t	rangeMin;
	uint16_t	rangeMax;

	_contextApi.getLightLightnessRangeState(
		rangeMin,
		rangeMax,
		statusCode
		);

	encoder.encode(statusCode);
	encoder.encode(rangeMin);
	encoder.encode(rangeMax);

	if(encoder.overflow()){
		Oscl::Error::Info::log(
			"%s: overflow!\n",
			OSCL_PRETTY_FUNCTION
			);
		return;
		}

	sendStatus(
		buffer,
		encoder.length(),
		dstAddress,	// dst
		appKeyIndex,
		ttl,
		authData,
		authDataLen,
		sendSegmented
		);
	}

void	Part::sendStatus(
			const void*	buffer,
			unsigned	length,
			uint16_t	dstAddress,
			uint16_t	appKeyIndex,
			uint8_t		ttl,
			const void*	authData,
			unsigned	authDataLen,
			bool		sendSegmented
			) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif

	if(_allowSegmentedStatus && sendSegmented){
		_transportApi.sendSegmented(
			this,	// completeApi
			buffer,
			length,
			dstAddress,
			appKeyIndex,
			ttl,
			authData,
			authDataLen
			);
		}
	else {
		_transportApi.sendUnsegmented(
			this,	// completeApi
			buffer,
			length,
			dstAddress,
			appKeyIndex,
			ttl,
			authData,
			authDataLen
			);
		}
	}

void	Part::success() noexcept{
	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif
	}

void	Part::successOnBehalfOfLPN() noexcept{
	#if defined(DEBUG_TRACE) || defined (DEBUG_TRACE_TX_STATUS)
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif
	}

void	Part::failedTimeout() noexcept{
	#if defined(DEBUG_TRACE) || defined (DEBUG_TRACE_TX_STATUS)
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif
	}

void	Part::failedBusy() noexcept{
	#if defined(DEBUG_TRACE) || defined (DEBUG_TRACE_TX_STATUS)
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif
	}

void	Part::failedOutOfResources() noexcept{
	#if defined(DEBUG_TRACE) || defined (DEBUG_TRACE_TX_STATUS)
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif
	}

void	Part::failedTooBig() noexcept{
	#if defined(DEBUG_TRACE) || defined (DEBUG_TRACE_TX_STATUS)
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif
	}

void	Part::stop() noexcept{
	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s:\n",
		__PRETTY_FUNCTION__
		);
	#endif
	}

void	Part::done() noexcept{
	_lastLightnessTID		= ~0;
	_lastLightnessLinearTID	= ~0;
	}
