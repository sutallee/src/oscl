/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_bluetooth_mesh_message_light_lightness_server_parth_
#define _oscl_bluetooth_mesh_message_light_lightness_server_parth_

#include "oscl/bluetooth/mesh/state/light/lightness/api.h"
#include "oscl/queue/queue.h"
#include "oscl/bluetooth/mesh/node/api.h"
#include "oscl/bluetooth/mesh/model/rx/item.h"
#include "oscl/bluetooth/mesh/model/api.h"
#include "oscl/bluetooth/mesh/transport/upper/tx/api.h"
#include "oscl/bluetooth/mesh/message/itemcomp.h"
#include "oscl/bluetooth/mesh/model/app/server/part.h"
#include "oscl/timer/api.h"
#include "oscl/done/operation.h"

/** */
namespace Oscl {
/** */
namespace BT {
/** */
namespace Mesh {
/** */
namespace Message {
/** */
namespace Light {
/** */
namespace Lightness {
/** */
namespace Server {

/** This is an implementation of the Bluetooth
	Mesh Generic Light Lightness Server messages.
 */
class Part :
	private Oscl::BT::Mesh::Transport::TX::Complete::Api,
	private Oscl::Done::Api
	{
	private:
		/** */
		Oscl::BT::Mesh::State::
		Light::Lightness::Api&		_contextApi;

		/** */
		Oscl::BT::Mesh::
		Transport::Upper::TX::Api&	_transportApi;

		/** */
		Oscl::Timer::Api&			_tidTimerApi;

		/** */
		const bool					_allowSegmentedStatus;

	private:
		/** */
		Oscl::BT::Mesh::Message::
		ItemComposer<Part>			_rxLightLightnessGet;

		/** */
		Oscl::BT::Mesh::Message::
		ItemComposer<Part>			_rxLightLightnessSet;

		/** */
		Oscl::BT::Mesh::Message::
		ItemComposer<Part>			_rxLightLightnessSetUnacknowledged;

		/** */
		Oscl::BT::Mesh::Message::
		ItemComposer<Part>			_rxLightLightnessLinearGet;

		/** */
		Oscl::BT::Mesh::Message::
		ItemComposer<Part>			_rxLightLightnessLinearSet;

		/** */
		Oscl::BT::Mesh::Message::
		ItemComposer<Part>			_rxLightLightnessLinearSetUnacknowledged;

		/** */
		Oscl::BT::Mesh::Message::
		ItemComposer<Part>			_rxLightLightnessLastGet;

		/** */
		Oscl::BT::Mesh::Message::
		ItemComposer<Part>			_rxLightLightnessDefaultGet;

		/** */
		Oscl::BT::Mesh::Message::
		ItemComposer<Part>			_rxLightLightnessDefaultSet;

		/** */
		Oscl::BT::Mesh::Message::
		ItemComposer<Part>			_rxLightLightnessDefaultSetUnacknowledged;

		/** */
		Oscl::BT::Mesh::Message::
		ItemComposer<Part>			_rxLightLightnessRangeGet;

		/** */
		Oscl::BT::Mesh::Message::
		ItemComposer<Part>			_rxLightLightnessRangeSet;

		/** */
		Oscl::BT::Mesh::Message::
		ItemComposer<Part>			_rxLightLightnessRangeSetUnacknowledged;

		/** */
		Oscl::BT::Mesh::Message::
		ItemComposer<Part>			_receiver;

	private:
		/** */
		Oscl::Queue<
			Oscl::BT::Mesh::
			Message::Item
			>					_messageHandlers;

	private:
		/** */
		uint16_t	_lastLightnessTID;

		/** */
		uint16_t	_lastLightnessSrcAddress;

		/** */
		uint16_t	_lastLightnessDstAddress;

	private:
		/** */
		uint16_t	_lastLightnessLinearTID;

		/** */
		uint16_t	_lastLightnessLinearSrcAddress;

		/** */
		uint16_t	_lastLightnessLinearDstAddress;

	public:
		/** */
		Part(
			Oscl::BT::Mesh::State::
			Light::Lightness::Api&		contextApi,
			Oscl::BT::Mesh::
			Transport::Upper::TX::Api&	transportApi,
			Oscl::Timer::Api&			tidTimerApi,
			bool						allowSegmentedStatus	= false
			) noexcept;


		/** */
		~Part() noexcept;

		/** */
		void	stop() noexcept;

		/** */
		Oscl::BT::Mesh::Message::Item&	getRxItem() noexcept;

		/** Public for publication.
		 */
		void	sendLightLightnessStatus(
					uint16_t	appKeyIndex,
					uint16_t	dstAddress,
					uint8_t		ttl,
					bool		sendSegmented	= true,
					const void*	authData	= 0,
					unsigned	authDataLen	= 0
					) noexcept;

		/** Public for publication.
		 */
		void	sendLightLightnessLinearStatus(
					uint16_t	appKeyIndex,
					uint16_t	dstAddress,
					uint8_t		ttl,
					bool		sendSegmented	= true,
					const void*	authData	= 0,
					unsigned	authDataLen	= 0
					) noexcept;

		/** Public for publication.
		 */
		void	sendLightLightnessLastStatus(
					uint16_t	appKeyIndex,
					uint16_t	dstAddress,
					uint8_t		ttl,
					bool		sendSegmented	= true,
					const void*	authData	= 0,
					unsigned	authDataLen	= 0
					) noexcept;

		/** Public for publication.
		 */
		void	sendLightLightnessDefaultStatus(
					uint16_t	appKeyIndex,
					uint16_t	dstAddress,
					uint8_t		ttl,
					bool		sendSegmented	= true,
					const void*	authData	= 0,
					unsigned	authDataLen	= 0
					) noexcept;

		/** Public for publication.
		 */
		void	sendLightLightnessRangeStatus(
					uint16_t	appKeyIndex,
					uint16_t	dstAddress,
					uint8_t		ttl,
					bool		sendSegmented	= true,
					const void*	authData	= 0,
					unsigned	authDataLen	= 0
					) noexcept;

	private:
		/** */
		void	sendStatus(
					const void*	buffer,
					unsigned	length,
					uint16_t	dstAddress,
					uint16_t	appKeyIndex,
					uint8_t		ttl,
					const void*	authData,
					unsigned	authDataLen,
					bool		sendSegmented
					) noexcept;

	public: // Oscl::BT::Mesh::Message::Composer _receiver
		/** Process a decrypted Access Message
			RETURN: true if handled (opcode match).
		 */
		bool	receive(
					uint32_t	opcode,
					uint16_t	srcAddress,
					uint16_t	dstAddress,
					const void*	frame,
					unsigned	length,
					uint16_t	appKeyIndex
					) noexcept;

	private: // Oscl::BT::Mesh::Message::ItemComposer _rxLightLightnessGet
		/** */
		bool	rxLightLightnessGet(
					uint32_t	opcode,
					uint16_t	srcAddress,
					uint16_t	dstAddress,
					const void*	frame,
					unsigned	length,
					uint16_t	appKeyIndex
					) noexcept;

	private: // Oscl::BT::Mesh::Message::ItemComposer _rxLightLightnessSet
		/** */
		bool	rxLightLightnessSet(
					uint32_t	opcode,
					uint16_t	srcAddress,
					uint16_t	dstAddress,
					const void*	frame,
					unsigned	length,
					uint16_t	appKeyIndex
					) noexcept;

	private: // Oscl::BT::Mesh::Message::ItemComposer _rxLightLightnessSetUnacknowledged
		/** */
		bool	rxLightLightnessSetUnacknowledged(
					uint32_t	opcode,
					uint16_t	srcAddress,
					uint16_t	dstAddress,
					const void*	frame,
					unsigned	length,
					uint16_t	appKeyIndex
					) noexcept;

	private: // Oscl::BT::Mesh::Message::ItemComposer _rxLightLightnessLinearGet
		/** */
		bool	rxLightLightnessLinearGet(
					uint32_t	opcode,
					uint16_t	srcAddress,
					uint16_t	dstAddress,
					const void*	frame,
					unsigned	length,
					uint16_t	appKeyIndex
					) noexcept;

	private: // Oscl::BT::Mesh::Message::ItemComposer _rxLightLightnessLinearSet
		/** */
		bool	rxLightLightnessLinearSet(
					uint32_t	opcode,
					uint16_t	srcAddress,
					uint16_t	dstAddress,
					const void*	frame,
					unsigned	length,
					uint16_t	appKeyIndex
					) noexcept;

	private: // Oscl::BT::Mesh::Message::ItemComposer _rxLightLightnessLinearSetUnacknowledged
		/** */
		bool	rxLightLightnessLinearSetUnacknowledged(
					uint32_t	opcode,
					uint16_t	srcAddress,
					uint16_t	dstAddress,
					const void*	frame,
					unsigned	length,
					uint16_t	appKeyIndex
					) noexcept;

	private: // Oscl::BT::Mesh::Message::ItemComposer _rxLightLightnessLastGet
		/** */
		bool	rxLightLightnessLastGet(
					uint32_t	opcode,
					uint16_t	srcAddress,
					uint16_t	dstAddress,
					const void*	frame,
					unsigned	length,
					uint16_t	appKeyIndex
					) noexcept;

	private: // Oscl::BT::Mesh::Message::ItemComposer _rxLightLightnessDefaultGet
		/** */
		bool	rxLightLightnessDefaultGet(
					uint32_t	opcode,
					uint16_t	srcAddress,
					uint16_t	dstAddress,
					const void*	frame,
					unsigned	length,
					uint16_t	appKeyIndex
					) noexcept;

	private: // Oscl::BT::Mesh::Message::ItemComposer _rxLightLightnessDefaultSet
		/** */
		bool	rxLightLightnessDefaultSet(
					uint32_t	opcode,
					uint16_t	srcAddress,
					uint16_t	dstAddress,
					const void*	frame,
					unsigned	length,
					uint16_t	appKeyIndex
					) noexcept;

	private: // Oscl::BT::Mesh::Message::ItemComposer _rxLightLightnessDefaultSetUnacknowledged
		/** */
		bool	rxLightLightnessDefaultSetUnacknowledged(
					uint32_t	opcode,
					uint16_t	srcAddress,
					uint16_t	dstAddress,
					const void*	frame,
					unsigned	length,
					uint16_t	appKeyIndex
					) noexcept;

	private: // Oscl::BT::Mesh::Message::ItemComposer _rxLightLightnessRangeGet
		/** */
		bool	rxLightLightnessRangeGet(
					uint32_t	opcode,
					uint16_t	srcAddress,
					uint16_t	dstAddress,
					const void*	frame,
					unsigned	length,
					uint16_t	appKeyIndex
					) noexcept;

	private: // Oscl::BT::Mesh::Message::ItemComposer _rxLightLightnessRangeSet
		/** */
		bool	rxLightLightnessRangeSet(
					uint32_t	opcode,
					uint16_t	srcAddress,
					uint16_t	dstAddress,
					const void*	frame,
					unsigned	length,
					uint16_t	appKeyIndex
					) noexcept;


	private: // Oscl::BT::Mesh::Message::ItemComposer _rxLightLightnessRangeSetUnacknowledged
		/** */
		bool	rxLightLightnessRangeSetUnacknowledged(
					uint32_t	opcode,
					uint16_t	srcAddress,
					uint16_t	dstAddress,
					const void*	frame,
					unsigned	length,
					uint16_t	appKeyIndex
					) noexcept;

	private:
		/** RETURN: true for failure
		 */
		bool	rxLightnessSet(
					uint16_t			srcAddress,
					uint16_t			dstAddress,
					const void*			frame,
					unsigned			length,
					uint16_t			appKeyIndex
					) noexcept;

		/** RETURN: true for failure
		 */
		bool	rxLightnessLinearSet(
					uint16_t			srcAddress,
					uint16_t			dstAddress,
					const void*			frame,
					unsigned			length,
					uint16_t			appKeyIndex
					) noexcept;

		/** RETURN: true for failure
		 */
		bool	rxLightnessDefaultSet(
					uint16_t			srcAddress,
					uint16_t			dstAddress,
					const void*			frame,
					unsigned			length,
					uint16_t			appKeyIndex
					) noexcept;

		/** RETURN: true for failure
		 */
		bool	rxLightnessRangeSet(
					uint16_t			srcAddress,
					uint16_t			dstAddress,
					const void*			frame,
					unsigned			length,
					uint16_t			appKeyIndex
					) noexcept;

	private: // Oscl::BT::Mesh::Transport::TX::Complete::Api
		/** This operation is invoked when the
			PDU has been successfully transmitted
			by the transport layer.

			For unsegmented messages, this is invoked
			immediately since there is no acknowledgment.

			For segmented messages sent to a unicast
			address destination, this operation is only
			invoked after all segments have been acknowledged
			by the destination or its friend.

			For segmented messages sent to a broadcast,
			group, or virtual address, this is invoked
			after all segments have been transmitted for
			the specified number of retrys.
		 */
		void	success() noexcept;

		/** This operation is invoked when the
			PDU has been successfully transmitted
			by the transport layer.

			For unsegmented messages, this is invoked
			immediately since there is no acknowledgment.

			For segmented messages sent to a unicast
			address destination, this operation is only
			invoked after all segments have been acknowledged
			by the destination or its friend.

			For segmented messages sent to a broadcast,
			group, or virtual address, this is invoked
			after all segments have been transmitted for
			the specified number of retrys.
		 */
		void	successOnBehalfOfLPN() noexcept;

		/**	This operation is invoked only for segmented
			messages sent to a unicast address where the
			remote did not acknowledge all segments before
			the timer expired.
		 */
		void	failedTimeout() noexcept;

		/**	This operation is invoked only for segmented
			messages sent to a unicast address where the
			remote indicated that it is unable to receive
			the message at this time (due to resource
			limitations).
		 */
		void	failedBusy() noexcept;

		/**	This operation is invoked when the transport
			layer does not currently have enough resources
			to send the message.
		 */
		void	failedOutOfResources() noexcept;

		/**	This operation is invoked for messages that
			are too big to be handled by the transport
			layer. This indicates a software error.
		 */
		void	failedTooBig() noexcept;

	private: // Oscl::Done::Api
		/**	This operation is invoked when the
			TID timer expires.
		 */
		void	done() noexcept;
	};

}
}
}
}
}
}
}

#endif
