/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_bluetooth_mesh_message_apih_
#define _oscl_bluetooth_mesh_message_apih_

#include <stdint.h>

/** */
namespace Oscl {
/** */
namespace BT {
/** */
namespace Mesh {
/** */
namespace Message {

/** This interface is used by an Element to interact
	with an upper layer message handler.
 */
class Api {
	public:
		/** 
			This is the interface used by an element
			to dispatch a received Message for processing
			to by the Element.

			RETURN: The implementation is expected to
			return true if it recognizes the opcode.
			The result will be used by the lower layer
			to determine if it has forwarded the Message to
			the correct element.

			The implementation MUST return true ONLY
			if the opcode matches one that it handles.

			param [in]	opcode		The opcode from the Access payload
			param [in]	srcAddress	The originator of this message. This
									is used for sending responses.
			param [in]	frame		The Application Parameters defined by opcode.
			param [in]	length		The length of the Application Parameters
			param [in]	appKeyIndex	The index of the AppKey used to
									decrypt this message. This is used for
									sending responses.
		 */
		virtual bool	receive(
							uint32_t	opcode,
							uint16_t	srcAddress,
							uint16_t	dstAddress,
							const void*	frame,
							unsigned	length,
							uint16_t	appKeyIndex	= 0
							) noexcept=0;
	};

}
}
}
}

#endif
