/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "part.h"
#include "oscl/error/info.h"
#include "oscl/decoder/linear/le/base.h"
#include "oscl/encoder/le/base.h"
#include "oscl/bluetooth/mesh/model/opcode/values.h"

using namespace Oscl::BT::Mesh::Message::Config::Composition::Data::Status;

Part::Part(
	Oscl::BT::Mesh::
	Transport::Upper::TX::Api&				transportApi,
	Oscl::BT::Mesh::Message::
	Config::Composition::Data::State::Api&	stateApi
	) noexcept:
		_transportApi(transportApi),
		_stateApi(stateApi)
		{
	}

bool	Part::receive(
			uint32_t	opcode,
			uint16_t	srcAddress,
			uint16_t	dstAddress,
			const void*	frame,
			unsigned	length,
			uint16_t	appKeyIndex
			) noexcept{

	if(opcode != Oscl::BT::Mesh::Model::OpCode::configCompositionDataStatus){
		return false;
		}

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s:\n"
		"",
		__PRETTY_FUNCTION__
		);

	Oscl::Error::Info::hexDump(
		frame,
		length
		);
	#endif

	return true;
	}

/*
	Composition Data Page 0

	CID: 2 octets	Contains a 16-bit company identifier assigned by the Bluetooth SIG
		0xFFFF 	This value has special meaning depending on the context in which it used.
					Link Manager Protocol (LMP): This value may be used in the internal
					and interoperability tests before a Company ID has been assigned.
					This value shall not be used in shipping end products.
					Device ID Profile: This value is reserved as the default vendor ID
					when no Device ID service record is present in a remote device.

	PID: 2 octets	Contains a 16-bit vendor-assigned product identifier

	VID: 2 octets	Contains a 16-bit vendor-assigned product version identifier

	CRPL: 2 octets	Contains a 16-bit value representing the minimum number of replay
					protection list entries in a device (see Section 3.8.8)

	Features: 2 octets	Contains a bit field indicating the device features:
			Bit		Feature		Notes
			[0]		Relay		Relay feature support: 0 = False, 1 = True
			[1]		Proxy		Proxy feature support: 0 = False, 1 = True
			[2]		Friend		Friend feature support: 0 = False, 1 = True
			[3]		Low Power	Low Power feature support: 0 = False, 1 = True
			[4–15]	RFU			Reserved for Future Use

	Elements: variable	 Contains a sequence of element descriptions
		Field			Size		Notes
		Loc				2			Contains a location descriptor
		NumS			1			Contains a count of SIG Model IDs in this element
		NumV			1			Contains a count of Vendor Model IDs in this element
		SIG Models		variable	Contains a sequence of NumS SIG Model IDs
		Vendor Models	variable	Contains a sequence of NumV Vendor Model IDs

*/

