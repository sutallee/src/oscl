/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_bluetooth_mesh_message_config_composition_data_status_parth_
#define _oscl_bluetooth_mesh_message_config_composition_data_status_parth_

#include "oscl/bluetooth/mesh/message/item.h"
#include "oscl/bluetooth/mesh/message/config/composition/data/state/api.h"
#include "oscl/bluetooth/mesh/transport/upper/tx/api.h"

/** */
namespace Oscl {
/** */
namespace BT {
/** */
namespace Mesh {
/** */
namespace Message {
/** */
namespace Config {
/** */
namespace Composition {
/** */
namespace Data {
/** */
namespace Status {

class Part : public Oscl::BT::Mesh::Message::Item {
	private:
		/** */
		Oscl::BT::Mesh::
		Transport::Upper::TX::Api&				_transportApi;

		/** */
		Oscl::BT::Mesh::Message::
		Config::Composition::Data::State::Api&	_stateApi;

	public:
		Part(
			Oscl::BT::Mesh::
			Transport::Upper::TX::Api&				transportApi,
			Oscl::BT::Mesh::Message::
			Config::Composition::Data::State::Api&	stateApi
			) noexcept;

	private: // Oscl::BT::Mesh::Message::Api
		/** 
			This is the interface used by an element
			to dispatch a received Message for processing
			to by the Element.

			RETURN: The implementation is expected to
			return true if it recognizes the opcode.
			The result will be used by the lower layer
			to determine if it has forwarded the Messae to
			the correct element.

			The implementation MUST return true ONLY
			if the opcode matches one that it handles.
		 */
		bool	receive(
					uint32_t	opcode,
					uint16_t	srcAddress,
					uint16_t	dstAddress,
					const void*	frame,
					unsigned	length,
					uint16_t	appKeyIndex
					) noexcept;

	private:
		/** */
		void	sendConfigCompositionDataStatus(
					uint16_t	dst,
					uint8_t		page
					) noexcept;

	};

}
}
}
}
}
}
}
}

#endif
