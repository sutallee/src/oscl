/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "part.h"
#include "oscl/error/info.h"
#include "oscl/endian/decoder/linear/part.h"
#include "oscl/encoder/le/base.h"
#include "oscl/bluetooth/mesh/model/opcode/values.h"
#include "oscl/bluetooth/mesh/constants.h"

using namespace Oscl::BT::Mesh::Message::Config::Composition::Data::Get;

//#define DEBUG_TRACE

Part::Part(
	Oscl::BT::Mesh::
	Transport::Upper::TX::Api&				transportApi,
	Oscl::BT::Mesh::Message::
	Config::Composition::Data::State::Api&	stateApi
	) noexcept:
		_transportApi(transportApi),
		_stateApi(stateApi)
		{
	}

bool	Part::receive(
			uint16_t	opcode,
			uint16_t	srcAddress,
			const void*	frame,
			unsigned	length
			) noexcept{

	if(opcode != Oscl::BT::Mesh::Model::OpCode::configCompositionDataGet){
		return false;
		}

	Oscl::Endian::Decoder::Linear::Part
	leDecoder(
		frame,
		length
		);

	Oscl::Decoder::Api&	decoder	= leDecoder.le();

	uint8_t	page;

	decoder.decode(page);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			OSCL_PRETTY_FUNCTION
			);
		return true;
		}

#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n"
		"\tPage: %u\n"
		"",
		__PRETTY_FUNCTION__,
		page
		);

	Oscl::Error::Info::hexDump(
		frame,
		length
		);
#endif

	uint16_t	dst	= srcAddress;

	// ===================
	// FIXME: test segmentation timers, by transmitting
	// to a bogus unicast address instead of the srcAddres.
//	dst	= 0x0FFF;
	// ===================

	sendConfigCompositionDataStatus(
		dst,
		page
		);

	return true;
	}

/*
	Composition Data Page 0

	CID: 2 octets	Contains a 16-bit company identifier assigned by the Bluetooth SIG
		0xFFFF 	This value has special meaning depending on the context in which it used.
					Link Manager Protocol (LMP): This value may be used in the internal
					and interoperability tests before a Company ID has been assigned.
					This value shall not be used in shipping end products.
					Device ID Profile: This value is reserved as the default vendor ID
					when no Device ID service record is present in a remote device.

	PID: 2 octets	Contains a 16-bit vendor-assigned product identifier

	VID: 2 octets	Contains a 16-bit vendor-assigned product version identifier

	CRPL: 2 octets	Contains a 16-bit value representing the minimum number of replay
					protection list entries in a device (see Section 3.8.8)

	Features: 2 octets	Contains a bit field indicating the device features:
			Bit		Feature		Notes
			[0]		Relay		Relay feature support: 0 = False, 1 = True
			[1]		Proxy		Proxy feature support: 0 = False, 1 = True
			[2]		Friend		Friend feature support: 0 = False, 1 = True
			[3]		Low Power	Low Power feature support: 0 = False, 1 = True
			[4–15]	RFU			Reserved for Future Use

	Elements: variable	 Contains a sequence of element descriptions
		Field			Size		Notes
		Loc				2			Contains a location descriptor
		NumS			1			Contains a count of SIG Model IDs in this element
		NumV			1			Contains a count of Vendor Model IDs in this element
		SIG Models		variable	Contains a sequence of NumS SIG Model IDs
		Vendor Models	variable	Contains a sequence of NumV Vendor Model IDs

*/

/*
	3.7.4.1 Transmitting an access message

	A message is transmitted by a model to a destination address that is a unicast address,
	a group address, or a virtual address.

	A message is transmitted from a source address, which is the transmitting element’s
	unicast address.

	The TTL field may be specified by the application by setting it to the number of hops
	required to transfer the message from the source element to all the destination addresses.
	However, if this is not specified, the Default TTL shall be applied by the access layer.

	The SRC field shall be set to the unicast address of the element within the node that
	is originating the message.

	The DST field shall be set to the unicast address, group address, or virtual address
	that the message is directed toward.

	The access layer does not guarantee delivery of messages. Each model should decide if a
	message is to be retransmitted and how potential duplicates are handled.

	If the message is sent in response to a received message that was sent to a unicast address,
	the node should transmit the response message with a random delay between 20 and 50 milliseconds.
	If the message is sent in response to a received message that was sent to a group address or a
	virtual address, the node should transmit the response message with a random delay between
	20 and 500 milliseconds. This reduces the probability of multiple nodes responding to this
	message at exactly the same time, and therefore increases the probability of message delivery
	rather than message collisions.

	Due to limited bandwidth available that is shared among all nodes and other Bluetooth devices,
	it is important to observe the volume of traffic a node is originating. A node should originate
	less than 100 Lower Transport PDUs in a moving 10-second window.

*/
	/*
		The transmit iterface should then look something like this:
		transmitSegmentedAccesMessage(
			Oscl::Pdu::Pdu*	pdu,	// Maximum of 380 octets
			uint8_t		ttl,
			uint16_t	srcAddress,
			uint16_t	destAddress,
			uint16_t	destAddress,
			);
		transmitUnsegmentedAccessMessage(
			Oscl::Pdu::Pdu*	pdu,	// Maximum of 15 octets
			uint8_t		ttl,
			uint16_t	srcAddress,
			uint16_t	destAddress,
			uint16_t	destAddress,
			);
		transmitSegmentedControlMessage(
			Oscl::Pdu::Pdu*	pdu,	// Maximum of 376 octets
			uint8_t		ttl,
			uint16_t	srcAddress,
			uint16_t	destAddress,
			uint16_t	destAddress,
			);
		transmitUnsegmentedControMessage(
			Oscl::Pdu::Pdu*	pdu,	// Maximum of 11 octets
			uint8_t		ttl,
			uint16_t	srcAddress,
			uint16_t	destAddress,
			uint16_t	destAddress,
			);
	 */

void	Part::sendConfigCompositionDataStatus(
			uint16_t	dst,
			uint8_t		page
			) noexcept{

	if(page != 0){
		Oscl::Error::Info::log(
			"%s: No such page (%u)\n",
			OSCL_PRETTY_FUNCTION,
			page
			);
		return;
		}

	unsigned	pageLength;

	const void*
	data	= _stateApi.getPageZero(pageLength);

	if(!data || !pageLength){
		Oscl::Error::Info::log(
			"%s: No data for page %u\n",
			OSCL_PRETTY_FUNCTION,
			page
			);
		return;
		}

#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n"
		"\tPage: %u\n"
		"",
		__PRETTY_FUNCTION__,
		page
		);

	Oscl::Error::Info::hexDump(
		data,
		pageLength
		);
#endif

	uint8_t	buffer[maxAccessPduSizeWithTransMic32];

	Oscl::Encoder::LE::Base
	leEncoder(
		buffer,
		sizeof(buffer)
		);
	Oscl::Encoder::Api&	encoder	= leEncoder;

	static const uint8_t	opcode	= 0x02;

	encoder.encode(opcode);
	encoder.encode(page);
	encoder.copyIn(
		data,
		pageLength
		);

#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s: Access PDU:\n"
		"\tOpcode: 0x%2.2X\n"
		"\tPage: %u\n"
		"",
		__PRETTY_FUNCTION__,
		opcode,
		page
		);

	Oscl::Error::Info::hexDump(
		buffer,
		encoder.length()
		);
#endif

	_transportApi.sendSegmented(
		this,	// completeApi
		buffer,
		encoder.length(),
		dst,
		0xFF	// use Default TTL
		);
	}

void	Part::success() noexcept{
	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif
	}

void	Part::failedTimeout() noexcept{
	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif
	}

void	Part::failedBusy() noexcept{
	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif
	}

void	Part::failedOutOfResources() noexcept{
	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif
	}

void	Part::failedTooBig() noexcept{
	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif
	}

