/*
   Copyright (C) 2021 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <new>
#include "part.h"
#include "oscl/endian/decoder/linear/part.h"
#include "oscl/encoder/le/base.h"
#include "oscl/bluetooth/mesh/address/virtual.h"
#include "oscl/bluetooth/mesh/address/types.h"
#include "oscl/bluetooth/mesh/address/values.h"
#include "oscl/bluetooth/mesh/model/opcode/decoder.h"
#include "oscl/error/info.h"
#include "oscl/bluetooth/mesh/constants.h"

using namespace Oscl::BT::Mesh::Message::Generic::OnPowerUp::Server;

//#define DEBUG_TRACE
//#define DEBUG_TRACE_TX_STATUS

Part::Part(
	Oscl::BT::Mesh::State::
	Generic::OnPowerUp::Api&	contextApi,
	Oscl::BT::Mesh::
	Transport::Upper::TX::Api&	transportApi,
	bool						allowSegmentedStatus
	) noexcept:
		_contextApi(contextApi),
		_transportApi(transportApi),
		_allowSegmentedStatus(allowSegmentedStatus),
		_rxGenericOnPowerUpGet(
			*this,
			&Part::rxGenericOnPowerUpGet
			),
		_rxGenericOnPowerUpSet(
			*this,
			&Part::rxGenericOnPowerUpSet
			),
		_rxGenericOnPowerUpSetUnacknowledged(
			*this,
			&Part::rxGenericOnPowerUpSetUnacknowledged
			),
		_receiver(
			*this,
			&Part::receive
			)
		{

	_messageHandlers.put(&_rxGenericOnPowerUpGet);
	_messageHandlers.put(&_rxGenericOnPowerUpSet);
	_messageHandlers.put(&_rxGenericOnPowerUpSetUnacknowledged);
	}

Part::~Part() noexcept{
	}

Oscl::BT::Mesh::Message::Item&	Part::getRxItem() noexcept{
	return _receiver;
	}

bool	Part::receive(
			uint32_t	opcode,
			uint16_t	srcAddress,
			uint16_t	dstAddress,
			const void*	frame,
			unsigned	length,
			uint16_t	appKeyIndex
			) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);

	Oscl::Error::Info::hexDump(
		frame,
		length
		);
	#endif

	for(
		Oscl::BT::Mesh::Message::Item*	item	= _messageHandlers.first();
		item;
		item	= _messageHandlers.next(item)
		){
		bool
		handled	= item->receive(
					opcode,
					srcAddress,
					dstAddress,
					frame,
					length,
					appKeyIndex
					);
		if(handled){
			return true;
			}
		}

	return false;
	}

bool	Part::rxOnPowerUpSet(
			uint16_t			srcAddress,
			uint16_t			dstAddress,
			const void*			frame,
			unsigned			length,
			uint16_t			appKeyIndex
			) noexcept{

	uint8_t	powerUpState;

	Oscl::Endian::Decoder::Linear::Part
	leDecoder(
		frame,
		length
		);

	Oscl::Decoder::Api&	decoder	= leDecoder.le();

	decoder.decode(powerUpState);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			OSCL_PRETTY_FUNCTION
			);
		return true;
		}

	_contextApi.setGenericOnPowerUpState(
		powerUpState
		);

	return false;
	}

bool	Part::rxGenericOnPowerUpGet(
			uint32_t	opcode,
			uint16_t	srcAddress,
			uint16_t	dstAddress,
			const void*	frame,
			unsigned	length,
			uint16_t	appKeyIndex
			) noexcept{

	if(opcode != Oscl::BT::Mesh::Model::OpCode::genericOnPowerUpGet){
		return false;
		}

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n"
		"",
		__PRETTY_FUNCTION__
		);
	#endif

	sendGenericOnPowerUpStatus(
		appKeyIndex,
		srcAddress,
		0xFF,	// default
		Oscl::BT::Mesh::Address::isUnicastAddress(dstAddress)
		);

	return true;
	}

bool	Part::rxGenericOnPowerUpSet(
			uint32_t	opcode,
			uint16_t	srcAddress,
			uint16_t	dstAddress,
			const void*	frame,
			unsigned	length,
			uint16_t	appKeyIndex
			) noexcept{

	if(opcode != Oscl::BT::Mesh::Model::OpCode::genericOnPowerUpSet){
		return false;
		}

	bool
	failed	= rxOnPowerUpSet(
				srcAddress,
				dstAddress,
				frame,
				length,
				appKeyIndex
				);

	if(failed){
		/*	Something was wrong with the
			received packet format.

			FIXME: We MAY want to send a status anyway.
		 */
		return true;
		}

	sendGenericOnPowerUpStatus(
		appKeyIndex,
		srcAddress,
		0xFF,	// default
		Oscl::BT::Mesh::Address::isUnicastAddress(dstAddress)
		);

	return true;
	}

bool	Part::rxGenericOnPowerUpSetUnacknowledged(
			uint32_t	opcode,
			uint16_t	srcAddress,
			uint16_t	dstAddress,
			const void*	frame,
			unsigned	length,
			uint16_t	appKeyIndex
			) noexcept{

	if(opcode != Oscl::BT::Mesh::Model::OpCode::genericOnPowerUpSetUnacknowledged){
		return false;
		}

	rxOnPowerUpSet(
		srcAddress,
		dstAddress,
		frame,
		length,
		appKeyIndex
		);

	return true;
	}

void	Part::sendGenericOnPowerUpStatus(
			uint16_t	appKeyIndex,
			uint16_t	dstAddress,
			uint8_t		ttl,
			bool		sendSegmented,
			const void*	authData,
			unsigned	authDataLen
			) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif

	uint8_t	buffer[maxAccessPduSizeWithTransMic32];

	Oscl::Encoder::LE::Base
	leEncoder(
		buffer,
		sizeof(buffer)
		);

	Oscl::Encoder::Api&	encoder	= leEncoder;

	static const uint8_t	opcode0	= 0x82; // Generic OnPowerUp Status
	static const uint8_t	opcode1	= 0x12; // Generic OnPowerUp Status

	uint8_t
	powerUpState	= _contextApi.getGenericOnPowerUpState();

	encoder.encode(opcode0);
	encoder.encode(opcode1);
	encoder.encode(powerUpState);

	if(encoder.overflow()){
		Oscl::Error::Info::log(
			"%s: overflow!\n",
			OSCL_PRETTY_FUNCTION
			);
		return;
		}

	if(_allowSegmentedStatus && sendSegmented){
		_transportApi.sendSegmented(
			this,	// completeApi
			buffer,
			encoder.length(),
			dstAddress,	// dst
			appKeyIndex,
			ttl,
			authData,
			authDataLen
			);
		}
	else {
		_transportApi.sendUnsegmented(
			this,	// completeApi
			buffer,
			encoder.length(),
			dstAddress,	// dst
			appKeyIndex,
			ttl,
			authData,
			authDataLen
			);
		}
	}

void	Part::success() noexcept{
	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif
	}

void	Part::successOnBehalfOfLPN() noexcept{
	#if defined(DEBUG_TRACE) || defined (DEBUG_TRACE_TX_STATUS)
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif
	}

void	Part::failedTimeout() noexcept{
	#if defined(DEBUG_TRACE) || defined (DEBUG_TRACE_TX_STATUS)
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif
	}

void	Part::failedBusy() noexcept{
	#if defined(DEBUG_TRACE) || defined (DEBUG_TRACE_TX_STATUS)
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif
	}

void	Part::failedOutOfResources() noexcept{
	#if defined(DEBUG_TRACE) || defined (DEBUG_TRACE_TX_STATUS)
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif
	}

void	Part::failedTooBig() noexcept{
	#if defined(DEBUG_TRACE) || defined (DEBUG_TRACE_TX_STATUS)
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif
	}

void	Part::stop() noexcept{
	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s:\n",
		__PRETTY_FUNCTION__
		);
	#endif
	}
