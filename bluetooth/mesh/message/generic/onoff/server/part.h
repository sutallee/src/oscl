/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_bluetooth_mesh_message_generic_onoff_server_parth_
#define _oscl_bluetooth_mesh_message_generic_onoff_server_parth_

#include "oscl/bluetooth/mesh/state/generic/onoff/api.h"
#include "oscl/queue/queue.h"
#include "oscl/bluetooth/mesh/node/api.h"
#include "oscl/bluetooth/mesh/model/rx/item.h"
#include "oscl/bluetooth/mesh/model/api.h"
#include "oscl/bluetooth/mesh/transport/upper/tx/api.h"
#include "oscl/bluetooth/mesh/message/itemcomp.h"
#include "oscl/bluetooth/mesh/model/app/server/part.h"
#include "oscl/timer/api.h"
#include "oscl/done/operation.h"

/** */
namespace Oscl {
/** */
namespace BT {
/** */
namespace Mesh {
/** */
namespace Message {
/** */
namespace Generic {
/** */
namespace OnOff {
/** */
namespace Server {

/** This is an implementation of the Bluetooth
	Mesh Generic OnOff Server messages.
 */
class Part :
	private Oscl::BT::Mesh::Transport::TX::Complete::Api,
	private Oscl::Done::Api
	{
	private:
		/** */
		Oscl::BT::Mesh::State::
		Generic::OnOff::Api&		_contextApi;

		/** */
		Oscl::BT::Mesh::
		Transport::Upper::TX::Api&	_transportApi;

		/** */
		Oscl::Timer::Api&			_tidTimerApi;

		/** */
		const bool					_allowSegmentedStatus;

	private:
		/** */
		Oscl::BT::Mesh::Message::
		ItemComposer<Part>			_rxGenericOnOffGet;

		/** */
		Oscl::BT::Mesh::Message::
		ItemComposer<Part>			_rxGenericOnOffSet;

		/** */
		Oscl::BT::Mesh::Message::
		ItemComposer<Part>			_rxGenericOnOffSetUnacknowledged;

		/** */
		Oscl::BT::Mesh::Message::
		ItemComposer<Part>			_receiver;

	private:
		/** */
		Oscl::Queue<
			Oscl::BT::Mesh::
			Message::Item
			>					_messageHandlers;

	private:
		/** */
		uint16_t	_lastTID;

		/** */
		uint16_t	_lastSrcAddress;

		/** */
		uint16_t	_lastDstAddress;

	public:
		/** */
		Part(
			Oscl::BT::Mesh::State::
			Generic::OnOff::Api&		contextApi,
			Oscl::BT::Mesh::
			Transport::Upper::TX::Api&	transportApi,
			Oscl::Timer::Api&			tidTimerApi,
			bool						allowSegmentedStatus	= false
			) noexcept;


		/** */
		~Part() noexcept;

		/** */
		void	stop() noexcept;

		/** */
		Oscl::BT::Mesh::Message::Item&	getRxItem() noexcept;

		/** Public for publication.
		 */
		void	sendGenericOnOffStatus(
					uint16_t	appKeyIndex,
					uint16_t	dstAddress,
					uint8_t		ttl,
					bool		sendSegmented	= true,
					const void*	authData	= 0,
					unsigned	authDataLen	= 0
					) noexcept;

	public: // Oscl::BT::Mesh::Message::Composer _receiver
		/** Process a decrypted Access Message
			RETURN: true if handled (opcode match).
		 */
		bool	receive(
					uint32_t	opcode,
					uint16_t	srcAddress,
					uint16_t	dstAddress,
					const void*	frame,
					unsigned	length,
					uint16_t	appKeyIndex
					) noexcept;

	private: // Oscl::BT::Mesh::Message::ItemComposer _rxGenericOnOffGet
		/** */
		bool	rxGenericOnOffGet(
					uint32_t	opcode,
					uint16_t	srcAddress,
					uint16_t	dstAddress,
					const void*	frame,
					unsigned	length,
					uint16_t	appKeyIndex
					) noexcept;

	private: // Oscl::BT::Mesh::Message::ItemComposer _rxGenericOnOffSet
		/** */
		bool	rxGenericOnOffSet(
					uint32_t	opcode,
					uint16_t	srcAddress,
					uint16_t	dstAddress,
					const void*	frame,
					unsigned	length,
					uint16_t	appKeyIndex
					) noexcept;

	private: // Oscl::BT::Mesh::Message::ItemComposer _rxGenericOnOffSetUnacknowledged
		/** */
		bool	rxGenericOnOffSetUnacknowledged(
					uint32_t	opcode,
					uint16_t	srcAddress,
					uint16_t	dstAddress,
					const void*	frame,
					unsigned	length,
					uint16_t	appKeyIndex
					) noexcept;

	private:
		/** RETURN: true for failure
		 */
		bool	rxOnOffSet(
					uint16_t			srcAddress,
					uint16_t			dstAddress,
					const void*			frame,
					unsigned			length,
					uint16_t			appKeyIndex
					) noexcept;

	private: // Oscl::BT::Mesh::Transport::TX::Complete::Api
		/** This operation is invoked when the
			PDU has been successfully transmitted
			by the transport layer.

			For unsegmented messages, this is invoked
			immediately since there is no acknowledgment.

			For segmented messages sent to a unicast
			address destination, this operation is only
			invoked after all segments have been acknowledged
			by the destination or its friend.

			For segmented messages sent to a broadcast,
			group, or virtual address, this is invoked
			after all segments have been transmitted for
			the specified number of retrys.
		 */
		void	success() noexcept;

		/** This operation is invoked when the
			PDU has been successfully transmitted
			by the transport layer.

			For unsegmented messages, this is invoked
			immediately since there is no acknowledgment.

			For segmented messages sent to a unicast
			address destination, this operation is only
			invoked after all segments have been acknowledged
			by the destination or its friend.

			For segmented messages sent to a broadcast,
			group, or virtual address, this is invoked
			after all segments have been transmitted for
			the specified number of retrys.
		 */
		void	successOnBehalfOfLPN() noexcept;

		/**	This operation is invoked only for segmented
			messages sent to a unicast address where the
			remote did not acknowledge all segments before
			the timer expired.
		 */
		void	failedTimeout() noexcept;

		/**	This operation is invoked only for segmented
			messages sent to a unicast address where the
			remote indicated that it is unable to receive
			the message at this time (due to resource
			limitations).
		 */
		void	failedBusy() noexcept;

		/**	This operation is invoked when the transport
			layer does not currently have enough resources
			to send the message.
		 */
		void	failedOutOfResources() noexcept;

		/**	This operation is invoked for messages that
			are too big to be handled by the transport
			layer. This indicates a software error.
		 */
		void	failedTooBig() noexcept;

	private: // Oscl::Done::Api
		/**	This operation is invoked when the
			TID timer expires.
		 */
		void	done() noexcept;
	};

}
}
}
}
}
}
}

#endif
