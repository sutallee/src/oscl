/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <new>
#include "part.h"
#include "oscl/endian/decoder/linear/part.h"
#include "oscl/encoder/le/base.h"
#include "oscl/bluetooth/mesh/address/virtual.h"
#include "oscl/bluetooth/mesh/address/types.h"
#include "oscl/bluetooth/mesh/address/values.h"
#include "oscl/bluetooth/mesh/model/opcode/decoder.h"
#include "oscl/error/info.h"
#include "oscl/bluetooth/mesh/constants.h"

using namespace Oscl::BT::Mesh::Message::Generic::OnOff::Server;

//#define DEBUG_TRACE
//#define DEBUG_TRACE_TX_STATUS

Part::Part(
	Oscl::BT::Mesh::State::
	Generic::OnOff::Api&		contextApi,
	Oscl::BT::Mesh::
	Transport::Upper::TX::Api&	transportApi,
	Oscl::Timer::Api&			tidTimerApi,
	bool						allowSegmentedStatus
	) noexcept:
		_contextApi(contextApi),
		_transportApi(transportApi),
		_tidTimerApi(tidTimerApi),
		_allowSegmentedStatus(allowSegmentedStatus),
		_rxGenericOnOffGet(
			*this,
			&Part::rxGenericOnOffGet
			),
		_rxGenericOnOffSet(
			*this,
			&Part::rxGenericOnOffSet
			),
		_rxGenericOnOffSetUnacknowledged(
			*this,
			&Part::rxGenericOnOffSetUnacknowledged
			),
		_receiver(
			*this,
			&Part::receive
			),
		_lastTID(~0),
		_lastSrcAddress(Oscl::BT::Mesh::Address::unassigned),
		_lastDstAddress(Oscl::BT::Mesh::Address::unassigned)
		{

	_messageHandlers.put(&_rxGenericOnOffGet);
	_messageHandlers.put(&_rxGenericOnOffSet);
	_messageHandlers.put(&_rxGenericOnOffSetUnacknowledged);

	_tidTimerApi.setExpirationCallback(*this);
	}

Part::~Part() noexcept{
	}

Oscl::BT::Mesh::Message::Item&	Part::getRxItem() noexcept{
	return _receiver;
	}

bool	Part::receive(
			uint32_t	opcode,
			uint16_t	srcAddress,
			uint16_t	dstAddress,
			const void*	frame,
			unsigned	length,
			uint16_t	appKeyIndex
			) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);

	Oscl::Error::Info::hexDump(
		frame,
		length
		);
	#endif

	for(
		Oscl::BT::Mesh::Message::Item*	item	= _messageHandlers.first();
		item;
		item	= _messageHandlers.next(item)
		){
		bool
		handled	= item->receive(
					opcode,
					srcAddress,
					dstAddress,
					frame,
					length,
					appKeyIndex
					);
		if(handled){
			return true;
			}
		}

	return false;
	}

bool	Part::rxOnOffSet(
			uint16_t			srcAddress,
			uint16_t			dstAddress,
			const void*			frame,
			unsigned			length,
			uint16_t			appKeyIndex
			) noexcept{

	uint8_t	targetState;
	uint8_t	tid;
	uint8_t	transitionTime;
	uint8_t	delay;

	Oscl::Endian::Decoder::Linear::Part
	leDecoder(
		frame,
		length
		);

	Oscl::Decoder::Api&	decoder	= leDecoder.le();

	decoder.decode(targetState);
	decoder.decode(tid);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			OSCL_PRETTY_FUNCTION
			);
		return true;
		}

	decoder.decode(transitionTime);

	bool	transitionTimeIsValid	= true;

	if(decoder.underflow()){
		transitionTimeIsValid	= false;
		}

	if(transitionTimeIsValid){
		decoder.decode(delay);
		if(decoder.underflow()){
			Oscl::Error::Info::log(
				"%s: underflow! Delay value not present.\n",
				OSCL_PRETTY_FUNCTION
				);
			return true;
			}
		}

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n"
		"\tOnOff State: 0x%2.2X\n"
		"\tTID: 0x%2.2X\n"
		"",
		__PRETTY_FUNCTION__,
		targetState,
		tid
		);

	if(transitionTimeIsValid){
		Oscl::Error::Info::log(
			"%s\n"
			"\tTransition Time: 0x%2.2X\n"
			"\tDelay: 0x%2.2X\n"
			"",
			OSCL_PRETTY_FUNCTION,
			transitionTime,
			delay
			);
		}
	#endif

	/*
		Mesh Model Spec: 3.3.1.2.2

		When a Generic OnOff Server receives a
		Generic OnOff Set message or a
		Generic OnOff Set Unacknowledged message,
		it shall set the Generic OnOff state to
		the OnOff field of the message,
		unless the message has the same value for
		the SRC, DST, and TID fields as the previous
		message received within the past 6 seconds.
	*/

	if(tid == _lastTID){
		if(srcAddress == _lastSrcAddress){
			if(dstAddress == _lastDstAddress){
				/*	It's a repeat, but we still want
					to send a response if required.
				 */
				return false;
				}
			}
		}

	_lastTID		= tid;
	_lastSrcAddress	= srcAddress;
	_lastDstAddress	= dstAddress;

	constexpr unsigned long	tidDelayInMs	= 6000;

	_tidTimerApi.start(tidDelayInMs);

	if(transitionTimeIsValid){

		_contextApi.setGenericOnOffState(
			targetState,
			transitionTime,
			delay
			);
		}
	else {

		_contextApi.setGenericOnOffState(
			targetState
			);
		}

	return false;
	}

bool	Part::rxGenericOnOffGet(
			uint32_t	opcode,
			uint16_t	srcAddress,
			uint16_t	dstAddress,
			const void*	frame,
			unsigned	length,
			uint16_t	appKeyIndex
			) noexcept{

	if(opcode != Oscl::BT::Mesh::Model::OpCode::genericOnOffGet){
		return false;
		}

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n"
		"",
		__PRETTY_FUNCTION__
		);
	#endif

	sendGenericOnOffStatus(
		appKeyIndex,
		srcAddress,
		0xFF,	// default
		Oscl::BT::Mesh::Address::isUnicastAddress(dstAddress)
		);

	return true;
	}

bool	Part::rxGenericOnOffSet(
			uint32_t	opcode,
			uint16_t	srcAddress,
			uint16_t	dstAddress,
			const void*	frame,
			unsigned	length,
			uint16_t	appKeyIndex
			) noexcept{

	if(opcode != Oscl::BT::Mesh::Model::OpCode::genericOnOffSet){
		return false;
		}

	bool
	failed	= rxOnOffSet(
				srcAddress,
				dstAddress,
				frame,
				length,
				appKeyIndex
				);

	if(failed){
		/*	Something was wrong with the
			received packet format.

			FIXME: We MAY want to send a status anyway.
		 */
		return true;
		}

	sendGenericOnOffStatus(
		appKeyIndex,
		srcAddress,
		0xFF,	// default
		Oscl::BT::Mesh::Address::isUnicastAddress(dstAddress)
		);

	return true;
	}

bool	Part::rxGenericOnOffSetUnacknowledged(
			uint32_t	opcode,
			uint16_t	srcAddress,
			uint16_t	dstAddress,
			const void*	frame,
			unsigned	length,
			uint16_t	appKeyIndex
			) noexcept{

	if(opcode != Oscl::BT::Mesh::Model::OpCode::genericOnOffSetUnacknowledged){
		return false;
		}

	rxOnOffSet(
		srcAddress,
		dstAddress,
		frame,
		length,
		appKeyIndex
		);

	return true;
	}

void	Part::sendGenericOnOffStatus(
			uint16_t	appKeyIndex,
			uint16_t	dstAddress,
			uint8_t		ttl,
			bool		sendSegmented,
			const void*	authData,
			unsigned	authDataLen
			) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif

	uint8_t	buffer[maxAccessPduSizeWithTransMic32];

	Oscl::Encoder::LE::Base
	leEncoder(
		buffer,
		sizeof(buffer)
		);

	Oscl::Encoder::Api&	encoder	= leEncoder;

	static const uint8_t	opcode0	= 0x82; // Generic OnOff Status
	static const uint8_t	opcode1	= 0x04; // Generic OnOff Status

	uint8_t	presentOnOff;
	uint8_t	targetOnOff;
	uint8_t	remainingTime;

	_contextApi.getGenericOnOffState(
		presentOnOff,
		targetOnOff,
		remainingTime
		);

	encoder.encode(opcode0);
	encoder.encode(opcode1);
	encoder.encode(presentOnOff);
	encoder.encode(targetOnOff);
	encoder.encode(remainingTime);

	if(encoder.overflow()){
		Oscl::Error::Info::log(
			"%s: overflow!\n",
			OSCL_PRETTY_FUNCTION
			);
		return;
		}

	if(_allowSegmentedStatus && sendSegmented){
		_transportApi.sendSegmented(
			this,	// completeApi
			buffer,
			encoder.length(),
			dstAddress,	// dst
			appKeyIndex,
			ttl,
			authData,
			authDataLen
			);
		}
	else {
		_transportApi.sendUnsegmented(
			this,	// completeApi
			buffer,
			encoder.length(),
			dstAddress,	// dst
			appKeyIndex,
			ttl,
			authData,
			authDataLen
			);
		}
	}

void	Part::success() noexcept{
	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif
	}

void	Part::successOnBehalfOfLPN() noexcept{
	#if defined(DEBUG_TRACE) || defined (DEBUG_TRACE_TX_STATUS)
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif
	}

void	Part::failedTimeout() noexcept{
	#if defined(DEBUG_TRACE) || defined (DEBUG_TRACE_TX_STATUS)
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif
	}

void	Part::failedBusy() noexcept{
	#if defined(DEBUG_TRACE) || defined (DEBUG_TRACE_TX_STATUS)
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif
	}

void	Part::failedOutOfResources() noexcept{
	#if defined(DEBUG_TRACE) || defined (DEBUG_TRACE_TX_STATUS)
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif
	}

void	Part::failedTooBig() noexcept{
	#if defined(DEBUG_TRACE) || defined (DEBUG_TRACE_TX_STATUS)
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif
	}

void	Part::stop() noexcept{
	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s:\n",
		__PRETTY_FUNCTION__
		);
	#endif
	}

void	Part::done() noexcept{
	_lastTID	= ~0;
	}
