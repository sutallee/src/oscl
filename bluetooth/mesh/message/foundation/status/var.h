/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_bt_mesh_message_foundation_status_varh_
#define _oscl_bt_mesh_message_foundation_status_varh_

/** */
namespace Oscl {

/** */
namespace BT {

/** */
namespace Mesh {

/** */
namespace Message {

/** */
namespace Foundation {

/** */
namespace Status {

/**	This Var class implements an EMORF. The Flyweight Pattern
	is used in the implementation of the state subclasses
	allowing a single set of concrete state classes to be shared
	between any number of instances of this EMORF.
 */
/**	
 */
class Var {
	public:
		/**	This interface a kind of GOF visitor
			that allows the state of the variable
			to be determined/queried.
		 */
		class Query {
			public:
				/** Make the compiler happy. */
				virtual ~Query() {}
				/**	This operation is invoked by the variable
					when the variable is in the Success state.
				 */
				virtual void	success() noexcept=0;

				/**	This operation is invoked by the variable
					when the variable is in the InvalidAddress state.
				 */
				virtual void	invalidAddress() noexcept=0;

				/**	This operation is invoked by the variable
					when the variable is in the InvalidModel state.
				 */
				virtual void	invalidModel() noexcept=0;

				/**	This operation is invoked by the variable
					when the variable is in the InvalidAppKeyIndex state.
				 */
				virtual void	invalidAppKeyIndex() noexcept=0;

				/**	This operation is invoked by the variable
					when the variable is in the InvalidNetKeyIndex state.
				 */
				virtual void	invalidNetKeyIndex() noexcept=0;

				/**	This operation is invoked by the variable
					when the variable is in the InsufficientResources state.
				 */
				virtual void	insufficientResources() noexcept=0;

				/**	This operation is invoked by the variable
					when the variable is in the KeyIndexAlreadyStored state.
				 */
				virtual void	keyIndexAlreadyStored() noexcept=0;

				/**	This operation is invoked by the variable
					when the variable is in the InvalidPublishParameters state.
				 */
				virtual void	invalidPublishParameters() noexcept=0;

				/**	This operation is invoked by the variable
					when the variable is in the NotASubscribeModel state.
				 */
				virtual void	notASubscribeModel() noexcept=0;

				/**	This operation is invoked by the variable
					when the variable is in the StorageFailure state.
				 */
				virtual void	storageFailure() noexcept=0;

				/**	This operation is invoked by the variable
					when the variable is in the FeatureNotSupported state.
				 */
				virtual void	featureNotSupported() noexcept=0;

				/**	This operation is invoked by the variable
					when the variable is in the CannotUpdate state.
				 */
				virtual void	cannotUpdate() noexcept=0;

				/**	This operation is invoked by the variable
					when the variable is in the CannotRemove state.
				 */
				virtual void	cannotRemove() noexcept=0;

				/**	This operation is invoked by the variable
					when the variable is in the CannotBind state.
				 */
				virtual void	cannotBind() noexcept=0;

				/**	This operation is invoked by the variable
					when the variable is in the TemporarilyUnableToChangeState state.
				 */
				virtual void	temporarilyUnableToChangeState() noexcept=0;

				/**	This operation is invoked by the variable
					when the variable is in the CannotSet state.
				 */
				virtual void	cannotSet() noexcept=0;

				/**	This operation is invoked by the variable
					when the variable is in the UnspecifiedError state.
				 */
				virtual void	unspecifiedError() noexcept=0;

				/**	This operation is invoked by the variable
					when the variable is in the InvalidBinding state.
				 */
				virtual void	invalidBinding() noexcept=0;

			};
	public:
		/**	This interface defines the operations that are
			supported by all of the states that the variable
			can assume. Sub-classes of this variable are to
			have no state (member variables) of their own.
			This allows a GOF fly-weight pattern to be used
			such that many instances of the Var can share the
			same concrete state instances.
		 */
		class State {
			public:
				/** Make the compiler happy. */
				virtual ~State() {}

				/**	Compare this state with another state for equality.
				 */
				virtual bool	operator ==(const State& other) const noexcept=0;

				/**	Compare this state with another state for inequality.
				 */
				virtual bool	operator !=(const State& other) const noexcept=0;

				/** Allow the concrete state to be queried to
					determine the actual state.
				 */
				virtual void	accept(Query& q) const noexcept=0;

			};
	private:
		/**	This member determines the current state of the EMORF.
		 */
		const State*	_state;

	public:
		/**	The constructor requires a reference to its context.
		 */
		Var(const State& initial=getSuccess()) noexcept;

		/** Be happy compiler! */
		virtual ~Var() {}

	public:
		/**	This operation is invoked to determine the current
			state of the variable via visitation.
		 */
		void	accept(Query& q) noexcept;

		/**	Compare the state of this Var with another for equality.
		 */
		bool	operator == (const Var& other) const noexcept;

		/**	Compare the state of this Var with another for inequality.
		 */
		bool	operator != (const Var& other) const noexcept;

		/**	Assign the state of another Var to this var.
		 */
		void	operator=(const Var& newState) noexcept;

		/**	Assign the specified state to this Var.
		 */
		void	operator=(const State& newState) noexcept;

		/**	Returns a reference to the current state.
		 */
		const State&	getCurrentState() const noexcept;

	public:
		/** This operation is invoked to change the EMORF
			to the Success state.
		 */
		void success() noexcept;

		/** This operation is invoked to change the EMORF
			to the InvalidAddress state.
		 */
		void invalidAddress() noexcept;

		/** This operation is invoked to change the EMORF
			to the InvalidModel state.
		 */
		void invalidModel() noexcept;

		/** This operation is invoked to change the EMORF
			to the InvalidAppKeyIndex state.
		 */
		void invalidAppKeyIndex() noexcept;

		/** This operation is invoked to change the EMORF
			to the InvalidNetKeyIndex state.
		 */
		void invalidNetKeyIndex() noexcept;

		/** This operation is invoked to change the EMORF
			to the InsufficientResources state.
		 */
		void insufficientResources() noexcept;

		/** This operation is invoked to change the EMORF
			to the KeyIndexAlreadyStored state.
		 */
		void keyIndexAlreadyStored() noexcept;

		/** This operation is invoked to change the EMORF
			to the InvalidPublishParameters state.
		 */
		void invalidPublishParameters() noexcept;

		/** This operation is invoked to change the EMORF
			to the NotASubscribeModel state.
		 */
		void notASubscribeModel() noexcept;

		/** This operation is invoked to change the EMORF
			to the StorageFailure state.
		 */
		void storageFailure() noexcept;

		/** This operation is invoked to change the EMORF
			to the FeatureNotSupported state.
		 */
		void featureNotSupported() noexcept;

		/** This operation is invoked to change the EMORF
			to the CannotUpdate state.
		 */
		void cannotUpdate() noexcept;

		/** This operation is invoked to change the EMORF
			to the CannotRemove state.
		 */
		void cannotRemove() noexcept;

		/** This operation is invoked to change the EMORF
			to the CannotBind state.
		 */
		void cannotBind() noexcept;

		/** This operation is invoked to change the EMORF
			to the TemporarilyUnableToChangeState state.
		 */
		void temporarilyUnableToChangeState() noexcept;

		/** This operation is invoked to change the EMORF
			to the CannotSet state.
		 */
		void cannotSet() noexcept;

		/** This operation is invoked to change the EMORF
			to the UnspecifiedError state.
		 */
		void unspecifiedError() noexcept;

		/** This operation is invoked to change the EMORF
			to the InvalidBinding state.
		 */
		void invalidBinding() noexcept;

	public:
		/** This operation returns an EMORF state pointer
			matching the numeric state, or zero if there is
			no matching state.
		 */
		static const State* get(long num) noexcept;

		/** This operation returns an EMORF state reference
			to the Success state.
		 */
		static const State& getSuccess() noexcept;

		/** This operation returns an EMORF state reference
			to the InvalidAddress state.
		 */
		static const State& getInvalidAddress() noexcept;

		/** This operation returns an EMORF state reference
			to the InvalidModel state.
		 */
		static const State& getInvalidModel() noexcept;

		/** This operation returns an EMORF state reference
			to the InvalidAppKeyIndex state.
		 */
		static const State& getInvalidAppKeyIndex() noexcept;

		/** This operation returns an EMORF state reference
			to the InvalidNetKeyIndex state.
		 */
		static const State& getInvalidNetKeyIndex() noexcept;

		/** This operation returns an EMORF state reference
			to the InsufficientResources state.
		 */
		static const State& getInsufficientResources() noexcept;

		/** This operation returns an EMORF state reference
			to the KeyIndexAlreadyStored state.
		 */
		static const State& getKeyIndexAlreadyStored() noexcept;

		/** This operation returns an EMORF state reference
			to the InvalidPublishParameters state.
		 */
		static const State& getInvalidPublishParameters() noexcept;

		/** This operation returns an EMORF state reference
			to the NotASubscribeModel state.
		 */
		static const State& getNotASubscribeModel() noexcept;

		/** This operation returns an EMORF state reference
			to the StorageFailure state.
		 */
		static const State& getStorageFailure() noexcept;

		/** This operation returns an EMORF state reference
			to the FeatureNotSupported state.
		 */
		static const State& getFeatureNotSupported() noexcept;

		/** This operation returns an EMORF state reference
			to the CannotUpdate state.
		 */
		static const State& getCannotUpdate() noexcept;

		/** This operation returns an EMORF state reference
			to the CannotRemove state.
		 */
		static const State& getCannotRemove() noexcept;

		/** This operation returns an EMORF state reference
			to the CannotBind state.
		 */
		static const State& getCannotBind() noexcept;

		/** This operation returns an EMORF state reference
			to the TemporarilyUnableToChangeState state.
		 */
		static const State& getTemporarilyUnableToChangeState() noexcept;

		/** This operation returns an EMORF state reference
			to the CannotSet state.
		 */
		static const State& getCannotSet() noexcept;

		/** This operation returns an EMORF state reference
			to the UnspecifiedError state.
		 */
		static const State& getUnspecifiedError() noexcept;

		/** This operation returns an EMORF state reference
			to the InvalidBinding state.
		 */
		static const State& getInvalidBinding() noexcept;

	};
}
}
}
}
}
}
#endif
