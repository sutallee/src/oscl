/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_bt_mesh_message_foundation_status_composerh_
#define _oscl_bt_mesh_message_foundation_status_composerh_

#include "var.h"

/** */
namespace Oscl {

/** */
namespace BT {

/** */
namespace Mesh {

/** */
namespace Message {

/** */
namespace Foundation {

/** */
namespace Status {

/**	This template class implements a kind of GOF decorator
	pattern allows the context to employ composition instead
	of inheritance. Frequently, a context may need to implement
	more than one interface that may result in method name
	clashes. This template solves the problem by implementing
	the interface and then invoking member function pointers
	in the context instead. One instance of this object is
	created in the context for each interface of this type used
	in the context. Each instance is constructed such that it
	refers to different member functions within the context.
	The interface of this object is passed to any object that
	requires the interface. When the object invokes any of the
	operations of the API, the corresponding member function
	of the context will subsequently be invoked.
 */
/**	
 */
template <class Context>
class Composer : public Var::Query {
	private:
		/** A reference to the Context.
		 */
		Context&	_context;

	private:
		/**	success
		 */
		void	(Context::*_success)();

		/**	invalidAddress
		 */
		void	(Context::*_invalidAddress)();

		/**	invalidModel
		 */
		void	(Context::*_invalidModel)();

		/**	invalidAppKeyIndex
		 */
		void	(Context::*_invalidAppKeyIndex)();

		/**	invalidNetKeyIndex
		 */
		void	(Context::*_invalidNetKeyIndex)();

		/**	insufficientResources
		 */
		void	(Context::*_insufficientResources)();

		/**	keyIndexAlreadyStored
		 */
		void	(Context::*_keyIndexAlreadyStored)();

		/**	invalidPublishParameters
		 */
		void	(Context::*_invalidPublishParameters)();

		/**	notASubscribeModel
		 */
		void	(Context::*_notASubscribeModel)();

		/**	storageFailure
		 */
		void	(Context::*_storageFailure)();

		/**	featureNotSupported
		 */
		void	(Context::*_featureNotSupported)();

		/**	cannotUpdate
		 */
		void	(Context::*_cannotUpdate)();

		/**	cannotRemove
		 */
		void	(Context::*_cannotRemove)();

		/**	cannotBind
		 */
		void	(Context::*_cannotBind)();

		/**	temporarilyUnableToChangeState
		 */
		void	(Context::*_temporarilyUnableToChangeState)();

		/**	cannotSet
		 */
		void	(Context::*_cannotSet)();

		/**	unspecifiedError
		 */
		void	(Context::*_unspecifiedError)();

		/**	invalidBinding
		 */
		void	(Context::*_invalidBinding)();

	public:
		/** */
		Composer(
			Context&		context,
			void	(Context::*success)(),
			void	(Context::*invalidAddress)(),
			void	(Context::*invalidModel)(),
			void	(Context::*invalidAppKeyIndex)(),
			void	(Context::*invalidNetKeyIndex)(),
			void	(Context::*insufficientResources)(),
			void	(Context::*keyIndexAlreadyStored)(),
			void	(Context::*invalidPublishParameters)(),
			void	(Context::*notASubscribeModel)(),
			void	(Context::*storageFailure)(),
			void	(Context::*featureNotSupported)(),
			void	(Context::*cannotUpdate)(),
			void	(Context::*cannotRemove)(),
			void	(Context::*cannotBind)(),
			void	(Context::*temporarilyUnableToChangeState)(),
			void	(Context::*cannotSet)(),
			void	(Context::*unspecifiedError)(),
			void	(Context::*invalidBinding)()
			) noexcept;

	private:
		/**	
		 */
		void	success() noexcept;

		/**	
		 */
		void	invalidAddress() noexcept;

		/**	
		 */
		void	invalidModel() noexcept;

		/**	
		 */
		void	invalidAppKeyIndex() noexcept;

		/**	
		 */
		void	invalidNetKeyIndex() noexcept;

		/**	
		 */
		void	insufficientResources() noexcept;

		/**	
		 */
		void	keyIndexAlreadyStored() noexcept;

		/**	
		 */
		void	invalidPublishParameters() noexcept;

		/**	
		 */
		void	notASubscribeModel() noexcept;

		/**	
		 */
		void	storageFailure() noexcept;

		/**	
		 */
		void	featureNotSupported() noexcept;

		/**	
		 */
		void	cannotUpdate() noexcept;

		/**	
		 */
		void	cannotRemove() noexcept;

		/**	
		 */
		void	cannotBind() noexcept;

		/**	
		 */
		void	temporarilyUnableToChangeState() noexcept;

		/**	
		 */
		void	cannotSet() noexcept;

		/**	
		 */
		void	unspecifiedError() noexcept;

		/**	
		 */
		void	invalidBinding() noexcept;

	};

template <class Context>
Composer<Context>::Composer(
			Context&		context,
			void	(Context::*success)(),
			void	(Context::*invalidAddress)(),
			void	(Context::*invalidModel)(),
			void	(Context::*invalidAppKeyIndex)(),
			void	(Context::*invalidNetKeyIndex)(),
			void	(Context::*insufficientResources)(),
			void	(Context::*keyIndexAlreadyStored)(),
			void	(Context::*invalidPublishParameters)(),
			void	(Context::*notASubscribeModel)(),
			void	(Context::*storageFailure)(),
			void	(Context::*featureNotSupported)(),
			void	(Context::*cannotUpdate)(),
			void	(Context::*cannotRemove)(),
			void	(Context::*cannotBind)(),
			void	(Context::*temporarilyUnableToChangeState)(),
			void	(Context::*cannotSet)(),
			void	(Context::*unspecifiedError)(),
			void	(Context::*invalidBinding)()
			) noexcept:
		_context(context),
		_success(success),
		_invalidAddress(invalidAddress),
		_invalidModel(invalidModel),
		_invalidAppKeyIndex(invalidAppKeyIndex),
		_invalidNetKeyIndex(invalidNetKeyIndex),
		_insufficientResources(insufficientResources),
		_keyIndexAlreadyStored(keyIndexAlreadyStored),
		_invalidPublishParameters(invalidPublishParameters),
		_notASubscribeModel(notASubscribeModel),
		_storageFailure(storageFailure),
		_featureNotSupported(featureNotSupported),
		_cannotUpdate(cannotUpdate),
		_cannotRemove(cannotRemove),
		_cannotBind(cannotBind),
		_temporarilyUnableToChangeState(temporarilyUnableToChangeState),
		_cannotSet(cannotSet),
		_unspecifiedError(unspecifiedError),
		_invalidBinding(invalidBinding)
		{
	}

template <class Context>
void	Composer<Context>::success() noexcept{
	(_context.*_success)();
	}

template <class Context>
void	Composer<Context>::invalidAddress() noexcept{
	(_context.*_invalidAddress)();
	}

template <class Context>
void	Composer<Context>::invalidModel() noexcept{
	(_context.*_invalidModel)();
	}

template <class Context>
void	Composer<Context>::invalidAppKeyIndex() noexcept{
	(_context.*_invalidAppKeyIndex)();
	}

template <class Context>
void	Composer<Context>::invalidNetKeyIndex() noexcept{
	(_context.*_invalidNetKeyIndex)();
	}

template <class Context>
void	Composer<Context>::insufficientResources() noexcept{
	(_context.*_insufficientResources)();
	}

template <class Context>
void	Composer<Context>::keyIndexAlreadyStored() noexcept{
	(_context.*_keyIndexAlreadyStored)();
	}

template <class Context>
void	Composer<Context>::invalidPublishParameters() noexcept{
	(_context.*_invalidPublishParameters)();
	}

template <class Context>
void	Composer<Context>::notASubscribeModel() noexcept{
	(_context.*_notASubscribeModel)();
	}

template <class Context>
void	Composer<Context>::storageFailure() noexcept{
	(_context.*_storageFailure)();
	}

template <class Context>
void	Composer<Context>::featureNotSupported() noexcept{
	(_context.*_featureNotSupported)();
	}

template <class Context>
void	Composer<Context>::cannotUpdate() noexcept{
	(_context.*_cannotUpdate)();
	}

template <class Context>
void	Composer<Context>::cannotRemove() noexcept{
	(_context.*_cannotRemove)();
	}

template <class Context>
void	Composer<Context>::cannotBind() noexcept{
	(_context.*_cannotBind)();
	}

template <class Context>
void	Composer<Context>::temporarilyUnableToChangeState() noexcept{
	(_context.*_temporarilyUnableToChangeState)();
	}

template <class Context>
void	Composer<Context>::cannotSet() noexcept{
	(_context.*_cannotSet)();
	}

template <class Context>
void	Composer<Context>::unspecifiedError() noexcept{
	(_context.*_unspecifiedError)();
	}

template <class Context>
void	Composer<Context>::invalidBinding() noexcept{
	(_context.*_invalidBinding)();
	}

}
}
}
}
}
}
#endif
