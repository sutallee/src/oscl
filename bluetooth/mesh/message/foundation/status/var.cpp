/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "var.h"
/** */
namespace Oscl {

/** */
namespace BT {

/** */
namespace Mesh {

/** */
namespace Message {

/** */
namespace Foundation {

/** */
namespace Status {

class VarIsSuccess : public Var::Query {
	public:
		bool	_match;
	public:
		inline void success() noexcept{
			_match	= true;
			}
		inline void invalidAddress() noexcept{
			_match	= false;
			}
		inline void invalidModel() noexcept{
			_match	= false;
			}
		inline void invalidAppKeyIndex() noexcept{
			_match	= false;
			}
		inline void invalidNetKeyIndex() noexcept{
			_match	= false;
			}
		inline void insufficientResources() noexcept{
			_match	= false;
			}
		inline void keyIndexAlreadyStored() noexcept{
			_match	= false;
			}
		inline void invalidPublishParameters() noexcept{
			_match	= false;
			}
		inline void notASubscribeModel() noexcept{
			_match	= false;
			}
		inline void storageFailure() noexcept{
			_match	= false;
			}
		inline void featureNotSupported() noexcept{
			_match	= false;
			}
		inline void cannotUpdate() noexcept{
			_match	= false;
			}
		inline void cannotRemove() noexcept{
			_match	= false;
			}
		inline void cannotBind() noexcept{
			_match	= false;
			}
		inline void temporarilyUnableToChangeState() noexcept{
			_match	= false;
			}
		inline void cannotSet() noexcept{
			_match	= false;
			}
		inline void unspecifiedError() noexcept{
			_match	= false;
			}
		inline void invalidBinding() noexcept{
			_match	= false;
			}
	};

class VarIsInvalidAddress : public Var::Query {
	public:
		bool	_match;
	public:
		inline void success() noexcept{
			_match	= false;
			}
		inline void invalidAddress() noexcept{
			_match	= true;
			}
		inline void invalidModel() noexcept{
			_match	= false;
			}
		inline void invalidAppKeyIndex() noexcept{
			_match	= false;
			}
		inline void invalidNetKeyIndex() noexcept{
			_match	= false;
			}
		inline void insufficientResources() noexcept{
			_match	= false;
			}
		inline void keyIndexAlreadyStored() noexcept{
			_match	= false;
			}
		inline void invalidPublishParameters() noexcept{
			_match	= false;
			}
		inline void notASubscribeModel() noexcept{
			_match	= false;
			}
		inline void storageFailure() noexcept{
			_match	= false;
			}
		inline void featureNotSupported() noexcept{
			_match	= false;
			}
		inline void cannotUpdate() noexcept{
			_match	= false;
			}
		inline void cannotRemove() noexcept{
			_match	= false;
			}
		inline void cannotBind() noexcept{
			_match	= false;
			}
		inline void temporarilyUnableToChangeState() noexcept{
			_match	= false;
			}
		inline void cannotSet() noexcept{
			_match	= false;
			}
		inline void unspecifiedError() noexcept{
			_match	= false;
			}
		inline void invalidBinding() noexcept{
			_match	= false;
			}
	};

class VarIsInvalidModel : public Var::Query {
	public:
		bool	_match;
	public:
		inline void success() noexcept{
			_match	= false;
			}
		inline void invalidAddress() noexcept{
			_match	= false;
			}
		inline void invalidModel() noexcept{
			_match	= true;
			}
		inline void invalidAppKeyIndex() noexcept{
			_match	= false;
			}
		inline void invalidNetKeyIndex() noexcept{
			_match	= false;
			}
		inline void insufficientResources() noexcept{
			_match	= false;
			}
		inline void keyIndexAlreadyStored() noexcept{
			_match	= false;
			}
		inline void invalidPublishParameters() noexcept{
			_match	= false;
			}
		inline void notASubscribeModel() noexcept{
			_match	= false;
			}
		inline void storageFailure() noexcept{
			_match	= false;
			}
		inline void featureNotSupported() noexcept{
			_match	= false;
			}
		inline void cannotUpdate() noexcept{
			_match	= false;
			}
		inline void cannotRemove() noexcept{
			_match	= false;
			}
		inline void cannotBind() noexcept{
			_match	= false;
			}
		inline void temporarilyUnableToChangeState() noexcept{
			_match	= false;
			}
		inline void cannotSet() noexcept{
			_match	= false;
			}
		inline void unspecifiedError() noexcept{
			_match	= false;
			}
		inline void invalidBinding() noexcept{
			_match	= false;
			}
	};

class VarIsInvalidAppKeyIndex : public Var::Query {
	public:
		bool	_match;
	public:
		inline void success() noexcept{
			_match	= false;
			}
		inline void invalidAddress() noexcept{
			_match	= false;
			}
		inline void invalidModel() noexcept{
			_match	= false;
			}
		inline void invalidAppKeyIndex() noexcept{
			_match	= true;
			}
		inline void invalidNetKeyIndex() noexcept{
			_match	= false;
			}
		inline void insufficientResources() noexcept{
			_match	= false;
			}
		inline void keyIndexAlreadyStored() noexcept{
			_match	= false;
			}
		inline void invalidPublishParameters() noexcept{
			_match	= false;
			}
		inline void notASubscribeModel() noexcept{
			_match	= false;
			}
		inline void storageFailure() noexcept{
			_match	= false;
			}
		inline void featureNotSupported() noexcept{
			_match	= false;
			}
		inline void cannotUpdate() noexcept{
			_match	= false;
			}
		inline void cannotRemove() noexcept{
			_match	= false;
			}
		inline void cannotBind() noexcept{
			_match	= false;
			}
		inline void temporarilyUnableToChangeState() noexcept{
			_match	= false;
			}
		inline void cannotSet() noexcept{
			_match	= false;
			}
		inline void unspecifiedError() noexcept{
			_match	= false;
			}
		inline void invalidBinding() noexcept{
			_match	= false;
			}
	};

class VarIsInvalidNetKeyIndex : public Var::Query {
	public:
		bool	_match;
	public:
		inline void success() noexcept{
			_match	= false;
			}
		inline void invalidAddress() noexcept{
			_match	= false;
			}
		inline void invalidModel() noexcept{
			_match	= false;
			}
		inline void invalidAppKeyIndex() noexcept{
			_match	= false;
			}
		inline void invalidNetKeyIndex() noexcept{
			_match	= true;
			}
		inline void insufficientResources() noexcept{
			_match	= false;
			}
		inline void keyIndexAlreadyStored() noexcept{
			_match	= false;
			}
		inline void invalidPublishParameters() noexcept{
			_match	= false;
			}
		inline void notASubscribeModel() noexcept{
			_match	= false;
			}
		inline void storageFailure() noexcept{
			_match	= false;
			}
		inline void featureNotSupported() noexcept{
			_match	= false;
			}
		inline void cannotUpdate() noexcept{
			_match	= false;
			}
		inline void cannotRemove() noexcept{
			_match	= false;
			}
		inline void cannotBind() noexcept{
			_match	= false;
			}
		inline void temporarilyUnableToChangeState() noexcept{
			_match	= false;
			}
		inline void cannotSet() noexcept{
			_match	= false;
			}
		inline void unspecifiedError() noexcept{
			_match	= false;
			}
		inline void invalidBinding() noexcept{
			_match	= false;
			}
	};

class VarIsInsufficientResources : public Var::Query {
	public:
		bool	_match;
	public:
		inline void success() noexcept{
			_match	= false;
			}
		inline void invalidAddress() noexcept{
			_match	= false;
			}
		inline void invalidModel() noexcept{
			_match	= false;
			}
		inline void invalidAppKeyIndex() noexcept{
			_match	= false;
			}
		inline void invalidNetKeyIndex() noexcept{
			_match	= false;
			}
		inline void insufficientResources() noexcept{
			_match	= true;
			}
		inline void keyIndexAlreadyStored() noexcept{
			_match	= false;
			}
		inline void invalidPublishParameters() noexcept{
			_match	= false;
			}
		inline void notASubscribeModel() noexcept{
			_match	= false;
			}
		inline void storageFailure() noexcept{
			_match	= false;
			}
		inline void featureNotSupported() noexcept{
			_match	= false;
			}
		inline void cannotUpdate() noexcept{
			_match	= false;
			}
		inline void cannotRemove() noexcept{
			_match	= false;
			}
		inline void cannotBind() noexcept{
			_match	= false;
			}
		inline void temporarilyUnableToChangeState() noexcept{
			_match	= false;
			}
		inline void cannotSet() noexcept{
			_match	= false;
			}
		inline void unspecifiedError() noexcept{
			_match	= false;
			}
		inline void invalidBinding() noexcept{
			_match	= false;
			}
	};

class VarIsKeyIndexAlreadyStored : public Var::Query {
	public:
		bool	_match;
	public:
		inline void success() noexcept{
			_match	= false;
			}
		inline void invalidAddress() noexcept{
			_match	= false;
			}
		inline void invalidModel() noexcept{
			_match	= false;
			}
		inline void invalidAppKeyIndex() noexcept{
			_match	= false;
			}
		inline void invalidNetKeyIndex() noexcept{
			_match	= false;
			}
		inline void insufficientResources() noexcept{
			_match	= false;
			}
		inline void keyIndexAlreadyStored() noexcept{
			_match	= true;
			}
		inline void invalidPublishParameters() noexcept{
			_match	= false;
			}
		inline void notASubscribeModel() noexcept{
			_match	= false;
			}
		inline void storageFailure() noexcept{
			_match	= false;
			}
		inline void featureNotSupported() noexcept{
			_match	= false;
			}
		inline void cannotUpdate() noexcept{
			_match	= false;
			}
		inline void cannotRemove() noexcept{
			_match	= false;
			}
		inline void cannotBind() noexcept{
			_match	= false;
			}
		inline void temporarilyUnableToChangeState() noexcept{
			_match	= false;
			}
		inline void cannotSet() noexcept{
			_match	= false;
			}
		inline void unspecifiedError() noexcept{
			_match	= false;
			}
		inline void invalidBinding() noexcept{
			_match	= false;
			}
	};

class VarIsInvalidPublishParameters : public Var::Query {
	public:
		bool	_match;
	public:
		inline void success() noexcept{
			_match	= false;
			}
		inline void invalidAddress() noexcept{
			_match	= false;
			}
		inline void invalidModel() noexcept{
			_match	= false;
			}
		inline void invalidAppKeyIndex() noexcept{
			_match	= false;
			}
		inline void invalidNetKeyIndex() noexcept{
			_match	= false;
			}
		inline void insufficientResources() noexcept{
			_match	= false;
			}
		inline void keyIndexAlreadyStored() noexcept{
			_match	= false;
			}
		inline void invalidPublishParameters() noexcept{
			_match	= true;
			}
		inline void notASubscribeModel() noexcept{
			_match	= false;
			}
		inline void storageFailure() noexcept{
			_match	= false;
			}
		inline void featureNotSupported() noexcept{
			_match	= false;
			}
		inline void cannotUpdate() noexcept{
			_match	= false;
			}
		inline void cannotRemove() noexcept{
			_match	= false;
			}
		inline void cannotBind() noexcept{
			_match	= false;
			}
		inline void temporarilyUnableToChangeState() noexcept{
			_match	= false;
			}
		inline void cannotSet() noexcept{
			_match	= false;
			}
		inline void unspecifiedError() noexcept{
			_match	= false;
			}
		inline void invalidBinding() noexcept{
			_match	= false;
			}
	};

class VarIsNotASubscribeModel : public Var::Query {
	public:
		bool	_match;
	public:
		inline void success() noexcept{
			_match	= false;
			}
		inline void invalidAddress() noexcept{
			_match	= false;
			}
		inline void invalidModel() noexcept{
			_match	= false;
			}
		inline void invalidAppKeyIndex() noexcept{
			_match	= false;
			}
		inline void invalidNetKeyIndex() noexcept{
			_match	= false;
			}
		inline void insufficientResources() noexcept{
			_match	= false;
			}
		inline void keyIndexAlreadyStored() noexcept{
			_match	= false;
			}
		inline void invalidPublishParameters() noexcept{
			_match	= false;
			}
		inline void notASubscribeModel() noexcept{
			_match	= true;
			}
		inline void storageFailure() noexcept{
			_match	= false;
			}
		inline void featureNotSupported() noexcept{
			_match	= false;
			}
		inline void cannotUpdate() noexcept{
			_match	= false;
			}
		inline void cannotRemove() noexcept{
			_match	= false;
			}
		inline void cannotBind() noexcept{
			_match	= false;
			}
		inline void temporarilyUnableToChangeState() noexcept{
			_match	= false;
			}
		inline void cannotSet() noexcept{
			_match	= false;
			}
		inline void unspecifiedError() noexcept{
			_match	= false;
			}
		inline void invalidBinding() noexcept{
			_match	= false;
			}
	};

class VarIsStorageFailure : public Var::Query {
	public:
		bool	_match;
	public:
		inline void success() noexcept{
			_match	= false;
			}
		inline void invalidAddress() noexcept{
			_match	= false;
			}
		inline void invalidModel() noexcept{
			_match	= false;
			}
		inline void invalidAppKeyIndex() noexcept{
			_match	= false;
			}
		inline void invalidNetKeyIndex() noexcept{
			_match	= false;
			}
		inline void insufficientResources() noexcept{
			_match	= false;
			}
		inline void keyIndexAlreadyStored() noexcept{
			_match	= false;
			}
		inline void invalidPublishParameters() noexcept{
			_match	= false;
			}
		inline void notASubscribeModel() noexcept{
			_match	= false;
			}
		inline void storageFailure() noexcept{
			_match	= true;
			}
		inline void featureNotSupported() noexcept{
			_match	= false;
			}
		inline void cannotUpdate() noexcept{
			_match	= false;
			}
		inline void cannotRemove() noexcept{
			_match	= false;
			}
		inline void cannotBind() noexcept{
			_match	= false;
			}
		inline void temporarilyUnableToChangeState() noexcept{
			_match	= false;
			}
		inline void cannotSet() noexcept{
			_match	= false;
			}
		inline void unspecifiedError() noexcept{
			_match	= false;
			}
		inline void invalidBinding() noexcept{
			_match	= false;
			}
	};

class VarIsFeatureNotSupported : public Var::Query {
	public:
		bool	_match;
	public:
		inline void success() noexcept{
			_match	= false;
			}
		inline void invalidAddress() noexcept{
			_match	= false;
			}
		inline void invalidModel() noexcept{
			_match	= false;
			}
		inline void invalidAppKeyIndex() noexcept{
			_match	= false;
			}
		inline void invalidNetKeyIndex() noexcept{
			_match	= false;
			}
		inline void insufficientResources() noexcept{
			_match	= false;
			}
		inline void keyIndexAlreadyStored() noexcept{
			_match	= false;
			}
		inline void invalidPublishParameters() noexcept{
			_match	= false;
			}
		inline void notASubscribeModel() noexcept{
			_match	= false;
			}
		inline void storageFailure() noexcept{
			_match	= false;
			}
		inline void featureNotSupported() noexcept{
			_match	= true;
			}
		inline void cannotUpdate() noexcept{
			_match	= false;
			}
		inline void cannotRemove() noexcept{
			_match	= false;
			}
		inline void cannotBind() noexcept{
			_match	= false;
			}
		inline void temporarilyUnableToChangeState() noexcept{
			_match	= false;
			}
		inline void cannotSet() noexcept{
			_match	= false;
			}
		inline void unspecifiedError() noexcept{
			_match	= false;
			}
		inline void invalidBinding() noexcept{
			_match	= false;
			}
	};

class VarIsCannotUpdate : public Var::Query {
	public:
		bool	_match;
	public:
		inline void success() noexcept{
			_match	= false;
			}
		inline void invalidAddress() noexcept{
			_match	= false;
			}
		inline void invalidModel() noexcept{
			_match	= false;
			}
		inline void invalidAppKeyIndex() noexcept{
			_match	= false;
			}
		inline void invalidNetKeyIndex() noexcept{
			_match	= false;
			}
		inline void insufficientResources() noexcept{
			_match	= false;
			}
		inline void keyIndexAlreadyStored() noexcept{
			_match	= false;
			}
		inline void invalidPublishParameters() noexcept{
			_match	= false;
			}
		inline void notASubscribeModel() noexcept{
			_match	= false;
			}
		inline void storageFailure() noexcept{
			_match	= false;
			}
		inline void featureNotSupported() noexcept{
			_match	= false;
			}
		inline void cannotUpdate() noexcept{
			_match	= true;
			}
		inline void cannotRemove() noexcept{
			_match	= false;
			}
		inline void cannotBind() noexcept{
			_match	= false;
			}
		inline void temporarilyUnableToChangeState() noexcept{
			_match	= false;
			}
		inline void cannotSet() noexcept{
			_match	= false;
			}
		inline void unspecifiedError() noexcept{
			_match	= false;
			}
		inline void invalidBinding() noexcept{
			_match	= false;
			}
	};

class VarIsCannotRemove : public Var::Query {
	public:
		bool	_match;
	public:
		inline void success() noexcept{
			_match	= false;
			}
		inline void invalidAddress() noexcept{
			_match	= false;
			}
		inline void invalidModel() noexcept{
			_match	= false;
			}
		inline void invalidAppKeyIndex() noexcept{
			_match	= false;
			}
		inline void invalidNetKeyIndex() noexcept{
			_match	= false;
			}
		inline void insufficientResources() noexcept{
			_match	= false;
			}
		inline void keyIndexAlreadyStored() noexcept{
			_match	= false;
			}
		inline void invalidPublishParameters() noexcept{
			_match	= false;
			}
		inline void notASubscribeModel() noexcept{
			_match	= false;
			}
		inline void storageFailure() noexcept{
			_match	= false;
			}
		inline void featureNotSupported() noexcept{
			_match	= false;
			}
		inline void cannotUpdate() noexcept{
			_match	= false;
			}
		inline void cannotRemove() noexcept{
			_match	= true;
			}
		inline void cannotBind() noexcept{
			_match	= false;
			}
		inline void temporarilyUnableToChangeState() noexcept{
			_match	= false;
			}
		inline void cannotSet() noexcept{
			_match	= false;
			}
		inline void unspecifiedError() noexcept{
			_match	= false;
			}
		inline void invalidBinding() noexcept{
			_match	= false;
			}
	};

class VarIsCannotBind : public Var::Query {
	public:
		bool	_match;
	public:
		inline void success() noexcept{
			_match	= false;
			}
		inline void invalidAddress() noexcept{
			_match	= false;
			}
		inline void invalidModel() noexcept{
			_match	= false;
			}
		inline void invalidAppKeyIndex() noexcept{
			_match	= false;
			}
		inline void invalidNetKeyIndex() noexcept{
			_match	= false;
			}
		inline void insufficientResources() noexcept{
			_match	= false;
			}
		inline void keyIndexAlreadyStored() noexcept{
			_match	= false;
			}
		inline void invalidPublishParameters() noexcept{
			_match	= false;
			}
		inline void notASubscribeModel() noexcept{
			_match	= false;
			}
		inline void storageFailure() noexcept{
			_match	= false;
			}
		inline void featureNotSupported() noexcept{
			_match	= false;
			}
		inline void cannotUpdate() noexcept{
			_match	= false;
			}
		inline void cannotRemove() noexcept{
			_match	= false;
			}
		inline void cannotBind() noexcept{
			_match	= true;
			}
		inline void temporarilyUnableToChangeState() noexcept{
			_match	= false;
			}
		inline void cannotSet() noexcept{
			_match	= false;
			}
		inline void unspecifiedError() noexcept{
			_match	= false;
			}
		inline void invalidBinding() noexcept{
			_match	= false;
			}
	};

class VarIsTemporarilyUnableToChangeState : public Var::Query {
	public:
		bool	_match;
	public:
		inline void success() noexcept{
			_match	= false;
			}
		inline void invalidAddress() noexcept{
			_match	= false;
			}
		inline void invalidModel() noexcept{
			_match	= false;
			}
		inline void invalidAppKeyIndex() noexcept{
			_match	= false;
			}
		inline void invalidNetKeyIndex() noexcept{
			_match	= false;
			}
		inline void insufficientResources() noexcept{
			_match	= false;
			}
		inline void keyIndexAlreadyStored() noexcept{
			_match	= false;
			}
		inline void invalidPublishParameters() noexcept{
			_match	= false;
			}
		inline void notASubscribeModel() noexcept{
			_match	= false;
			}
		inline void storageFailure() noexcept{
			_match	= false;
			}
		inline void featureNotSupported() noexcept{
			_match	= false;
			}
		inline void cannotUpdate() noexcept{
			_match	= false;
			}
		inline void cannotRemove() noexcept{
			_match	= false;
			}
		inline void cannotBind() noexcept{
			_match	= false;
			}
		inline void temporarilyUnableToChangeState() noexcept{
			_match	= true;
			}
		inline void cannotSet() noexcept{
			_match	= false;
			}
		inline void unspecifiedError() noexcept{
			_match	= false;
			}
		inline void invalidBinding() noexcept{
			_match	= false;
			}
	};

class VarIsCannotSet : public Var::Query {
	public:
		bool	_match;
	public:
		inline void success() noexcept{
			_match	= false;
			}
		inline void invalidAddress() noexcept{
			_match	= false;
			}
		inline void invalidModel() noexcept{
			_match	= false;
			}
		inline void invalidAppKeyIndex() noexcept{
			_match	= false;
			}
		inline void invalidNetKeyIndex() noexcept{
			_match	= false;
			}
		inline void insufficientResources() noexcept{
			_match	= false;
			}
		inline void keyIndexAlreadyStored() noexcept{
			_match	= false;
			}
		inline void invalidPublishParameters() noexcept{
			_match	= false;
			}
		inline void notASubscribeModel() noexcept{
			_match	= false;
			}
		inline void storageFailure() noexcept{
			_match	= false;
			}
		inline void featureNotSupported() noexcept{
			_match	= false;
			}
		inline void cannotUpdate() noexcept{
			_match	= false;
			}
		inline void cannotRemove() noexcept{
			_match	= false;
			}
		inline void cannotBind() noexcept{
			_match	= false;
			}
		inline void temporarilyUnableToChangeState() noexcept{
			_match	= false;
			}
		inline void cannotSet() noexcept{
			_match	= true;
			}
		inline void unspecifiedError() noexcept{
			_match	= false;
			}
		inline void invalidBinding() noexcept{
			_match	= false;
			}
	};

class VarIsUnspecifiedError : public Var::Query {
	public:
		bool	_match;
	public:
		inline void success() noexcept{
			_match	= false;
			}
		inline void invalidAddress() noexcept{
			_match	= false;
			}
		inline void invalidModel() noexcept{
			_match	= false;
			}
		inline void invalidAppKeyIndex() noexcept{
			_match	= false;
			}
		inline void invalidNetKeyIndex() noexcept{
			_match	= false;
			}
		inline void insufficientResources() noexcept{
			_match	= false;
			}
		inline void keyIndexAlreadyStored() noexcept{
			_match	= false;
			}
		inline void invalidPublishParameters() noexcept{
			_match	= false;
			}
		inline void notASubscribeModel() noexcept{
			_match	= false;
			}
		inline void storageFailure() noexcept{
			_match	= false;
			}
		inline void featureNotSupported() noexcept{
			_match	= false;
			}
		inline void cannotUpdate() noexcept{
			_match	= false;
			}
		inline void cannotRemove() noexcept{
			_match	= false;
			}
		inline void cannotBind() noexcept{
			_match	= false;
			}
		inline void temporarilyUnableToChangeState() noexcept{
			_match	= false;
			}
		inline void cannotSet() noexcept{
			_match	= false;
			}
		inline void unspecifiedError() noexcept{
			_match	= true;
			}
		inline void invalidBinding() noexcept{
			_match	= false;
			}
	};

class VarIsInvalidBinding : public Var::Query {
	public:
		bool	_match;
	public:
		inline void success() noexcept{
			_match	= false;
			}
		inline void invalidAddress() noexcept{
			_match	= false;
			}
		inline void invalidModel() noexcept{
			_match	= false;
			}
		inline void invalidAppKeyIndex() noexcept{
			_match	= false;
			}
		inline void invalidNetKeyIndex() noexcept{
			_match	= false;
			}
		inline void insufficientResources() noexcept{
			_match	= false;
			}
		inline void keyIndexAlreadyStored() noexcept{
			_match	= false;
			}
		inline void invalidPublishParameters() noexcept{
			_match	= false;
			}
		inline void notASubscribeModel() noexcept{
			_match	= false;
			}
		inline void storageFailure() noexcept{
			_match	= false;
			}
		inline void featureNotSupported() noexcept{
			_match	= false;
			}
		inline void cannotUpdate() noexcept{
			_match	= false;
			}
		inline void cannotRemove() noexcept{
			_match	= false;
			}
		inline void cannotBind() noexcept{
			_match	= false;
			}
		inline void temporarilyUnableToChangeState() noexcept{
			_match	= false;
			}
		inline void cannotSet() noexcept{
			_match	= false;
			}
		inline void unspecifiedError() noexcept{
			_match	= false;
			}
		inline void invalidBinding() noexcept{
			_match	= true;
			}
	};

class GetState : public Var::Query {
	public:
		const Var::State*	_state;
	public:
		inline void success() noexcept{
			_state	= &Var::getSuccess();
			}
		inline void invalidAddress() noexcept{
			_state	= &Var::getInvalidAddress();
			}
		inline void invalidModel() noexcept{
			_state	= &Var::getInvalidModel();
			}
		inline void invalidAppKeyIndex() noexcept{
			_state	= &Var::getInvalidAppKeyIndex();
			}
		inline void invalidNetKeyIndex() noexcept{
			_state	= &Var::getInvalidNetKeyIndex();
			}
		inline void insufficientResources() noexcept{
			_state	= &Var::getInsufficientResources();
			}
		inline void keyIndexAlreadyStored() noexcept{
			_state	= &Var::getKeyIndexAlreadyStored();
			}
		inline void invalidPublishParameters() noexcept{
			_state	= &Var::getInvalidPublishParameters();
			}
		inline void notASubscribeModel() noexcept{
			_state	= &Var::getNotASubscribeModel();
			}
		inline void storageFailure() noexcept{
			_state	= &Var::getStorageFailure();
			}
		inline void featureNotSupported() noexcept{
			_state	= &Var::getFeatureNotSupported();
			}
		inline void cannotUpdate() noexcept{
			_state	= &Var::getCannotUpdate();
			}
		inline void cannotRemove() noexcept{
			_state	= &Var::getCannotRemove();
			}
		inline void cannotBind() noexcept{
			_state	= &Var::getCannotBind();
			}
		inline void temporarilyUnableToChangeState() noexcept{
			_state	= &Var::getTemporarilyUnableToChangeState();
			}
		inline void cannotSet() noexcept{
			_state	= &Var::getCannotSet();
			}
		inline void unspecifiedError() noexcept{
			_state	= &Var::getUnspecifiedError();
			}
		inline void invalidBinding() noexcept{
			_state	= &Var::getInvalidBinding();
			}
	};
class VarSuccess : public Var::State {
	public:
		VarSuccess(){}
		bool	operator == (const Var::State& other) const noexcept;
		bool	operator != (const Var::State& other) const noexcept;
		void	accept(Var::Query& q) const noexcept;
	};

class VarInvalidAddress : public Var::State {
	public:
		VarInvalidAddress(){}
		bool	operator == (const Var::State& other) const noexcept;
		bool	operator != (const Var::State& other) const noexcept;
		void	accept(Var::Query& q) const noexcept;
	};

class VarInvalidModel : public Var::State {
	public:
		VarInvalidModel(){}
		bool	operator == (const Var::State& other) const noexcept;
		bool	operator != (const Var::State& other) const noexcept;
		void	accept(Var::Query& q) const noexcept;
	};

class VarInvalidAppKeyIndex : public Var::State {
	public:
		VarInvalidAppKeyIndex(){}
		bool	operator == (const Var::State& other) const noexcept;
		bool	operator != (const Var::State& other) const noexcept;
		void	accept(Var::Query& q) const noexcept;
	};

class VarInvalidNetKeyIndex : public Var::State {
	public:
		VarInvalidNetKeyIndex(){}
		bool	operator == (const Var::State& other) const noexcept;
		bool	operator != (const Var::State& other) const noexcept;
		void	accept(Var::Query& q) const noexcept;
	};

class VarInsufficientResources : public Var::State {
	public:
		VarInsufficientResources(){}
		bool	operator == (const Var::State& other) const noexcept;
		bool	operator != (const Var::State& other) const noexcept;
		void	accept(Var::Query& q) const noexcept;
	};

class VarKeyIndexAlreadyStored : public Var::State {
	public:
		VarKeyIndexAlreadyStored(){}
		bool	operator == (const Var::State& other) const noexcept;
		bool	operator != (const Var::State& other) const noexcept;
		void	accept(Var::Query& q) const noexcept;
	};

class VarInvalidPublishParameters : public Var::State {
	public:
		VarInvalidPublishParameters(){}
		bool	operator == (const Var::State& other) const noexcept;
		bool	operator != (const Var::State& other) const noexcept;
		void	accept(Var::Query& q) const noexcept;
	};

class VarNotASubscribeModel : public Var::State {
	public:
		VarNotASubscribeModel(){}
		bool	operator == (const Var::State& other) const noexcept;
		bool	operator != (const Var::State& other) const noexcept;
		void	accept(Var::Query& q) const noexcept;
	};

class VarStorageFailure : public Var::State {
	public:
		VarStorageFailure(){}
		bool	operator == (const Var::State& other) const noexcept;
		bool	operator != (const Var::State& other) const noexcept;
		void	accept(Var::Query& q) const noexcept;
	};

class VarFeatureNotSupported : public Var::State {
	public:
		VarFeatureNotSupported(){}
		bool	operator == (const Var::State& other) const noexcept;
		bool	operator != (const Var::State& other) const noexcept;
		void	accept(Var::Query& q) const noexcept;
	};

class VarCannotUpdate : public Var::State {
	public:
		VarCannotUpdate(){}
		bool	operator == (const Var::State& other) const noexcept;
		bool	operator != (const Var::State& other) const noexcept;
		void	accept(Var::Query& q) const noexcept;
	};

class VarCannotRemove : public Var::State {
	public:
		VarCannotRemove(){}
		bool	operator == (const Var::State& other) const noexcept;
		bool	operator != (const Var::State& other) const noexcept;
		void	accept(Var::Query& q) const noexcept;
	};

class VarCannotBind : public Var::State {
	public:
		VarCannotBind(){}
		bool	operator == (const Var::State& other) const noexcept;
		bool	operator != (const Var::State& other) const noexcept;
		void	accept(Var::Query& q) const noexcept;
	};

class VarTemporarilyUnableToChangeState : public Var::State {
	public:
		VarTemporarilyUnableToChangeState(){}
		bool	operator == (const Var::State& other) const noexcept;
		bool	operator != (const Var::State& other) const noexcept;
		void	accept(Var::Query& q) const noexcept;
	};

class VarCannotSet : public Var::State {
	public:
		VarCannotSet(){}
		bool	operator == (const Var::State& other) const noexcept;
		bool	operator != (const Var::State& other) const noexcept;
		void	accept(Var::Query& q) const noexcept;
	};

class VarUnspecifiedError : public Var::State {
	public:
		VarUnspecifiedError(){}
		bool	operator == (const Var::State& other) const noexcept;
		bool	operator != (const Var::State& other) const noexcept;
		void	accept(Var::Query& q) const noexcept;
	};

class VarInvalidBinding : public Var::State {
	public:
		VarInvalidBinding(){}
		bool	operator == (const Var::State& other) const noexcept;
		bool	operator != (const Var::State& other) const noexcept;
		void	accept(Var::Query& q) const noexcept;
	};

}
}
}
}
}
}
using namespace Oscl::BT::Mesh::Message::Foundation::Status;
static const VarSuccess	_success;
static const VarInvalidAddress	_invalidAddress;
static const VarInvalidModel	_invalidModel;
static const VarInvalidAppKeyIndex	_invalidAppKeyIndex;
static const VarInvalidNetKeyIndex	_invalidNetKeyIndex;
static const VarInsufficientResources	_insufficientResources;
static const VarKeyIndexAlreadyStored	_keyIndexAlreadyStored;
static const VarInvalidPublishParameters	_invalidPublishParameters;
static const VarNotASubscribeModel	_notASubscribeModel;
static const VarStorageFailure	_storageFailure;
static const VarFeatureNotSupported	_featureNotSupported;
static const VarCannotUpdate	_cannotUpdate;
static const VarCannotRemove	_cannotRemove;
static const VarCannotBind	_cannotBind;
static const VarTemporarilyUnableToChangeState	_temporarilyUnableToChangeState;
static const VarCannotSet	_cannotSet;
static const VarUnspecifiedError	_unspecifiedError;
static const VarInvalidBinding	_invalidBinding;
bool VarSuccess::operator == (const Var::State& other) const noexcept{
	VarIsSuccess	query;
	other.accept(query);
	return query._match;
	}

bool VarSuccess::operator != (const Var::State& other) const noexcept{
	VarIsSuccess	query;
	other.accept(query);
	return !query._match;
	}

void VarSuccess::accept(Var::Query& q) const noexcept{
	q.success();
	}

bool VarInvalidAddress::operator == (const Var::State& other) const noexcept{
	VarIsInvalidAddress	query;
	other.accept(query);
	return query._match;
	}

bool VarInvalidAddress::operator != (const Var::State& other) const noexcept{
	VarIsInvalidAddress	query;
	other.accept(query);
	return !query._match;
	}

void VarInvalidAddress::accept(Var::Query& q) const noexcept{
	q.invalidAddress();
	}

bool VarInvalidModel::operator == (const Var::State& other) const noexcept{
	VarIsInvalidModel	query;
	other.accept(query);
	return query._match;
	}

bool VarInvalidModel::operator != (const Var::State& other) const noexcept{
	VarIsInvalidModel	query;
	other.accept(query);
	return !query._match;
	}

void VarInvalidModel::accept(Var::Query& q) const noexcept{
	q.invalidModel();
	}

bool VarInvalidAppKeyIndex::operator == (const Var::State& other) const noexcept{
	VarIsInvalidAppKeyIndex	query;
	other.accept(query);
	return query._match;
	}

bool VarInvalidAppKeyIndex::operator != (const Var::State& other) const noexcept{
	VarIsInvalidAppKeyIndex	query;
	other.accept(query);
	return !query._match;
	}

void VarInvalidAppKeyIndex::accept(Var::Query& q) const noexcept{
	q.invalidAppKeyIndex();
	}

bool VarInvalidNetKeyIndex::operator == (const Var::State& other) const noexcept{
	VarIsInvalidNetKeyIndex	query;
	other.accept(query);
	return query._match;
	}

bool VarInvalidNetKeyIndex::operator != (const Var::State& other) const noexcept{
	VarIsInvalidNetKeyIndex	query;
	other.accept(query);
	return !query._match;
	}

void VarInvalidNetKeyIndex::accept(Var::Query& q) const noexcept{
	q.invalidNetKeyIndex();
	}

bool VarInsufficientResources::operator == (const Var::State& other) const noexcept{
	VarIsInsufficientResources	query;
	other.accept(query);
	return query._match;
	}

bool VarInsufficientResources::operator != (const Var::State& other) const noexcept{
	VarIsInsufficientResources	query;
	other.accept(query);
	return !query._match;
	}

void VarInsufficientResources::accept(Var::Query& q) const noexcept{
	q.insufficientResources();
	}

bool VarKeyIndexAlreadyStored::operator == (const Var::State& other) const noexcept{
	VarIsKeyIndexAlreadyStored	query;
	other.accept(query);
	return query._match;
	}

bool VarKeyIndexAlreadyStored::operator != (const Var::State& other) const noexcept{
	VarIsKeyIndexAlreadyStored	query;
	other.accept(query);
	return !query._match;
	}

void VarKeyIndexAlreadyStored::accept(Var::Query& q) const noexcept{
	q.keyIndexAlreadyStored();
	}

bool VarInvalidPublishParameters::operator == (const Var::State& other) const noexcept{
	VarIsInvalidPublishParameters	query;
	other.accept(query);
	return query._match;
	}

bool VarInvalidPublishParameters::operator != (const Var::State& other) const noexcept{
	VarIsInvalidPublishParameters	query;
	other.accept(query);
	return !query._match;
	}

void VarInvalidPublishParameters::accept(Var::Query& q) const noexcept{
	q.invalidPublishParameters();
	}

bool VarNotASubscribeModel::operator == (const Var::State& other) const noexcept{
	VarIsNotASubscribeModel	query;
	other.accept(query);
	return query._match;
	}

bool VarNotASubscribeModel::operator != (const Var::State& other) const noexcept{
	VarIsNotASubscribeModel	query;
	other.accept(query);
	return !query._match;
	}

void VarNotASubscribeModel::accept(Var::Query& q) const noexcept{
	q.notASubscribeModel();
	}

bool VarStorageFailure::operator == (const Var::State& other) const noexcept{
	VarIsStorageFailure	query;
	other.accept(query);
	return query._match;
	}

bool VarStorageFailure::operator != (const Var::State& other) const noexcept{
	VarIsStorageFailure	query;
	other.accept(query);
	return !query._match;
	}

void VarStorageFailure::accept(Var::Query& q) const noexcept{
	q.storageFailure();
	}

bool VarFeatureNotSupported::operator == (const Var::State& other) const noexcept{
	VarIsFeatureNotSupported	query;
	other.accept(query);
	return query._match;
	}

bool VarFeatureNotSupported::operator != (const Var::State& other) const noexcept{
	VarIsFeatureNotSupported	query;
	other.accept(query);
	return !query._match;
	}

void VarFeatureNotSupported::accept(Var::Query& q) const noexcept{
	q.featureNotSupported();
	}

bool VarCannotUpdate::operator == (const Var::State& other) const noexcept{
	VarIsCannotUpdate	query;
	other.accept(query);
	return query._match;
	}

bool VarCannotUpdate::operator != (const Var::State& other) const noexcept{
	VarIsCannotUpdate	query;
	other.accept(query);
	return !query._match;
	}

void VarCannotUpdate::accept(Var::Query& q) const noexcept{
	q.cannotUpdate();
	}

bool VarCannotRemove::operator == (const Var::State& other) const noexcept{
	VarIsCannotRemove	query;
	other.accept(query);
	return query._match;
	}

bool VarCannotRemove::operator != (const Var::State& other) const noexcept{
	VarIsCannotRemove	query;
	other.accept(query);
	return !query._match;
	}

void VarCannotRemove::accept(Var::Query& q) const noexcept{
	q.cannotRemove();
	}

bool VarCannotBind::operator == (const Var::State& other) const noexcept{
	VarIsCannotBind	query;
	other.accept(query);
	return query._match;
	}

bool VarCannotBind::operator != (const Var::State& other) const noexcept{
	VarIsCannotBind	query;
	other.accept(query);
	return !query._match;
	}

void VarCannotBind::accept(Var::Query& q) const noexcept{
	q.cannotBind();
	}

bool VarTemporarilyUnableToChangeState::operator == (const Var::State& other) const noexcept{
	VarIsTemporarilyUnableToChangeState	query;
	other.accept(query);
	return query._match;
	}

bool VarTemporarilyUnableToChangeState::operator != (const Var::State& other) const noexcept{
	VarIsTemporarilyUnableToChangeState	query;
	other.accept(query);
	return !query._match;
	}

void VarTemporarilyUnableToChangeState::accept(Var::Query& q) const noexcept{
	q.temporarilyUnableToChangeState();
	}

bool VarCannotSet::operator == (const Var::State& other) const noexcept{
	VarIsCannotSet	query;
	other.accept(query);
	return query._match;
	}

bool VarCannotSet::operator != (const Var::State& other) const noexcept{
	VarIsCannotSet	query;
	other.accept(query);
	return !query._match;
	}

void VarCannotSet::accept(Var::Query& q) const noexcept{
	q.cannotSet();
	}

bool VarUnspecifiedError::operator == (const Var::State& other) const noexcept{
	VarIsUnspecifiedError	query;
	other.accept(query);
	return query._match;
	}

bool VarUnspecifiedError::operator != (const Var::State& other) const noexcept{
	VarIsUnspecifiedError	query;
	other.accept(query);
	return !query._match;
	}

void VarUnspecifiedError::accept(Var::Query& q) const noexcept{
	q.unspecifiedError();
	}

bool VarInvalidBinding::operator == (const Var::State& other) const noexcept{
	VarIsInvalidBinding	query;
	other.accept(query);
	return query._match;
	}

bool VarInvalidBinding::operator != (const Var::State& other) const noexcept{
	VarIsInvalidBinding	query;
	other.accept(query);
	return !query._match;
	}

void VarInvalidBinding::accept(Var::Query& q) const noexcept{
	q.invalidBinding();
	}


Var::Var(const State& initial) noexcept:
		_state(&initial)
		{
	}

void Var::accept(Query& q) noexcept{
	_state->accept(q);
	}

bool Var::operator == (const Var& other) const noexcept{
	return *other._state == *_state;
	}

bool Var::operator != (const Var& other) const noexcept{
	return *other._state != *_state;
	}

void Var::operator = (const Var& newState) noexcept{
	*this	= *newState._state;
	}

void Var::operator = (const State& newState) noexcept{
	GetState	getState;
	newState.accept(getState);
	_state	= getState._state;
	}

const Var::State& Var::getCurrentState() const noexcept{
	return *_state;
	}

void Var::success() noexcept{
	_state	= &_success;
	}

void Var::invalidAddress() noexcept{
	_state	= &_invalidAddress;
	}

void Var::invalidModel() noexcept{
	_state	= &_invalidModel;
	}

void Var::invalidAppKeyIndex() noexcept{
	_state	= &_invalidAppKeyIndex;
	}

void Var::invalidNetKeyIndex() noexcept{
	_state	= &_invalidNetKeyIndex;
	}

void Var::insufficientResources() noexcept{
	_state	= &_insufficientResources;
	}

void Var::keyIndexAlreadyStored() noexcept{
	_state	= &_keyIndexAlreadyStored;
	}

void Var::invalidPublishParameters() noexcept{
	_state	= &_invalidPublishParameters;
	}

void Var::notASubscribeModel() noexcept{
	_state	= &_notASubscribeModel;
	}

void Var::storageFailure() noexcept{
	_state	= &_storageFailure;
	}

void Var::featureNotSupported() noexcept{
	_state	= &_featureNotSupported;
	}

void Var::cannotUpdate() noexcept{
	_state	= &_cannotUpdate;
	}

void Var::cannotRemove() noexcept{
	_state	= &_cannotRemove;
	}

void Var::cannotBind() noexcept{
	_state	= &_cannotBind;
	}

void Var::temporarilyUnableToChangeState() noexcept{
	_state	= &_temporarilyUnableToChangeState;
	}

void Var::cannotSet() noexcept{
	_state	= &_cannotSet;
	}

void Var::unspecifiedError() noexcept{
	_state	= &_unspecifiedError;
	}

void Var::invalidBinding() noexcept{
	_state	= &_invalidBinding;
	}

const Var::State*	Var::get(long num) noexcept{
	switch(num){
		case 0x00:
			return &_success;
		case 0x01:
			return &_invalidAddress;
		case 0x02:
			return &_invalidModel;
		case 0x03:
			return &_invalidAppKeyIndex;
		case 0x04:
			return &_invalidNetKeyIndex;
		case 0x05:
			return &_insufficientResources;
		case 0x06:
			return &_keyIndexAlreadyStored;
		case 0x07:
			return &_invalidPublishParameters;
		case 0x08:
			return &_notASubscribeModel;
		case 0x09:
			return &_storageFailure;
		case 0x0A:
			return &_featureNotSupported;
		case 0x0B:
			return &_cannotUpdate;
		case 0x0C:
			return &_cannotRemove;
		case 0x0D:
			return &_cannotBind;
		case 0x0E:
			return &_temporarilyUnableToChangeState;
		case 0x0F:
			return &_cannotSet;
		case 0x10:
			return &_unspecifiedError;
		case 0x11:
			return &_invalidBinding;
		default:
			return 0;
		};
	}

const Var::State&	Var::getSuccess() noexcept{
	return _success;
	}

const Var::State&	Var::getInvalidAddress() noexcept{
	return _invalidAddress;
	}

const Var::State&	Var::getInvalidModel() noexcept{
	return _invalidModel;
	}

const Var::State&	Var::getInvalidAppKeyIndex() noexcept{
	return _invalidAppKeyIndex;
	}

const Var::State&	Var::getInvalidNetKeyIndex() noexcept{
	return _invalidNetKeyIndex;
	}

const Var::State&	Var::getInsufficientResources() noexcept{
	return _insufficientResources;
	}

const Var::State&	Var::getKeyIndexAlreadyStored() noexcept{
	return _keyIndexAlreadyStored;
	}

const Var::State&	Var::getInvalidPublishParameters() noexcept{
	return _invalidPublishParameters;
	}

const Var::State&	Var::getNotASubscribeModel() noexcept{
	return _notASubscribeModel;
	}

const Var::State&	Var::getStorageFailure() noexcept{
	return _storageFailure;
	}

const Var::State&	Var::getFeatureNotSupported() noexcept{
	return _featureNotSupported;
	}

const Var::State&	Var::getCannotUpdate() noexcept{
	return _cannotUpdate;
	}

const Var::State&	Var::getCannotRemove() noexcept{
	return _cannotRemove;
	}

const Var::State&	Var::getCannotBind() noexcept{
	return _cannotBind;
	}

const Var::State&	Var::getTemporarilyUnableToChangeState() noexcept{
	return _temporarilyUnableToChangeState;
	}

const Var::State&	Var::getCannotSet() noexcept{
	return _cannotSet;
	}

const Var::State&	Var::getUnspecifiedError() noexcept{
	return _unspecifiedError;
	}

const Var::State&	Var::getInvalidBinding() noexcept{
	return _invalidBinding;
	}

