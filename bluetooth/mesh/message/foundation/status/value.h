/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

/** */
namespace Oscl {
/** */
namespace BT {
/** */
namespace Mesh {
/** */
namespace Message {
/** */
namespace Foundation {
/** */
namespace Status {

constexpr uint8_t	success							= 0x00;
constexpr uint8_t	invalidAddress					= 0x01;
constexpr uint8_t	invalidModel					= 0x02;
constexpr uint8_t	invalidAppKeyIndex				= 0x03;
constexpr uint8_t	invalidNetKeyIndex				= 0x04;
constexpr uint8_t	insufficientResources			= 0x05;
constexpr uint8_t	keyIndexAlreadyStored			= 0x06;
constexpr uint8_t	invalidPublishParameters		= 0x07;
constexpr uint8_t	notASubscribeModel				= 0x08;
constexpr uint8_t	storageFailure					= 0x09;
constexpr uint8_t	featureNotSupported				= 0x0A;
constexpr uint8_t	cannotUpdate					= 0x0B;
constexpr uint8_t	cannotRemove					= 0x0C;
constexpr uint8_t	cannotBind						= 0x0D;
constexpr uint8_t	temporarilyUnableToChangeState	= 0x0E;
constexpr uint8_t	cannotSet						= 0x0F;
constexpr uint8_t	unspecifiedError				= 0x10;
constexpr uint8_t	invalidBinding					= 0x11;

}
}
}
}
}
}

