/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_bluetooth_mesh_prov_link_reaperh_
#define _oscl_bluetooth_mesh_prov_link_reaperh_

#include <stdint.h>
#include "oscl/memory/block.h"
#include "oscl/stream/hdlc/fcs/fcs8.h"
#include "oscl/frame/fwd/api.h"
#include "oscl/pdu/fwd/composer.h"
#include "oscl/timer/api.h"
#include "oscl/pdu/visitor.h"
#include "oscl/pdu/memory/config.h"
#include "oscl/done/operation.h"

/** */
namespace Oscl {
/** */
namespace BT {
/** */
namespace Mesh {
/** */
namespace Prov {
/** */
namespace Link {

/** */
class Reaper;

/** */
class ContextApi {
	public:
		/** This operation is called when the Reaper
			is stopped after it completes the process
			and is ready to be destroyed.
		 */
		virtual void	stopped(Reaper& device) noexcept=0;
	};

/** */
class Reaper : private Oscl::Pdu::ReadOnlyVisitor {
	public:
		/** */
		void*		__qitemlink;

	private:
		/** */
		Link::ContextApi&			_context;

		/** */
		Oscl::Pdu::FWD::Api&		_provBearerApi;

		/** */
		Oscl::Timer::Api&			_linkTxTimerApi;

		/** */
		Oscl::Frame::FWD::Api&		_clientRxApi;

		/** */
		const uint32_t				_linkID;

		/** */
		const bool					_provisioner;

	private:
		/** */
		Oscl::Pdu::FWD::Composer<Reaper>	_provTxComposer;

		/** */
		Oscl::Pdu::FWD::Composer<Reaper>	_rawTxComposer;

	private:
		/** */
		Oscl::Done::Operation<Reaper>		_allFreeNotification;

	private:
		/** */
		Oscl::Done::Operation<Reaper>		_linkTxTimerExpired;

	private:
		/** */
		constexpr static unsigned	bufferSize	= 128;
		/** */
		constexpr static unsigned	nBuffers	= 8;
		/** */
		constexpr static unsigned	nFixed		= 8;
		/** */
		constexpr static unsigned	nFragment	= 8;
		/** */
		constexpr static unsigned	nComposite	= 8;

		/** */
		Oscl::Pdu::Memory::
		Config<
			bufferSize,
			nBuffers,
			nFixed,
			nFragment,
			nComposite
			>								_freeStore;

	private:
		/** */
		Oscl::HQueue<Oscl::Pdu::Pdu>	_txQ;

		/** */
		Oscl::Handle<Oscl::Pdu::Pdu>	_currentTx;

		/** */
		Oscl::HQueue<Oscl::Pdu::Pdu>	_transactionQ;

		/** */
		unsigned						_transactionRetryCount;

	private:
		/** */
		Oscl::Stream::HDLC::FCS8::Accumulator	_fcsAccumulator;

	private:
		/** */
		uint16_t		_totalLength;

		/** */
		uint8_t			_fec;

		/** The maximum number of segments is 64.
			The maximum MTU (for PB-ADV) is 24.
			Therefore, (for PB-ADV) the maximum provisioning PDU
			size is 64 * 24 = 1,536 octets.
			A maximum Provisioning PDU must be no greater than this.
			*IF* a PB-GATT MT is greater than 24, a Provisioning PDU
			must still be no greater than the maximum calculated
			for PB-ADV, so that Provisioning must work on either.
			*IF* a PB-GATT MTU is less than 24, then a full
			Provisioning PDU can still fit in this size.
		 */
		constexpr static unsigned	maxReassemblyBufferSize	= 1536;

		/** */
		uint8_t			_reasmBuffer[maxReassemblyBufferSize];

		/** */
		unsigned		_reasmOffset;
		
		/** */
		uint8_t			_lastExpectedSegment;

		/** */
		uint8_t			_nextExpectedSegment;

		/** */
		uint8_t			_rxTransactionNumber;

		/** */
		uint8_t			_txTransactionNumber;

	private:
		/** */
		bool			_stopping;

	public:
		/** */
		Reaper(
			Link::ContextApi&			context,
			Oscl::Pdu::FWD::Api&		provBearerApi,
			Oscl::Timer::Api&			linkTxTimerApi,
			Oscl::Frame::FWD::Api&		clientRxApi,
			uint32_t					linkID,
			bool						provisioner = false
			) noexcept;

		/** */
		Oscl::Pdu::FWD::Api&	getTxApi() noexcept;

		/** */
		Oscl::Pdu::FWD::Api&	getRawTxApi() noexcept;

		/** */
		void	txTimerExpired() noexcept;

		/** */
		void	start() noexcept;

		/** */
		void	stop() noexcept;

		/** */
		bool	match(uint32_t linkID) noexcept;

		/** */
		bool	parseTransactionStart(
					uint8_t			transactionNumber,
					uint8_t			segN,
					const void*		frame,
					uint8_t 		length
					) noexcept;

		/** */
		bool	parseTransactionAcknowledgement(
					uint8_t			transactionNumber,
					const void*		frame,
					uint8_t 		length
					) noexcept;

		/** */
		bool	parseTransactionContinuation(
					uint8_t			transactionNumber,
					uint8_t			segIndex,
					const void*		frame,
					uint8_t 		length
				) noexcept;

	private: // Oscl::PDU::FWD::Composer	_provTxComposer;
		/** */
		void	txForward(Oscl::Pdu::Pdu* pdu) noexcept;

	private: // Oscl::PDU::FWD::Composer	_rawTxComposer;
		/** */
		void	rawTxForward(Oscl::Pdu::Pdu* pdu) noexcept;

	private:
		/** */
		void	processTxQ() noexcept;

		/** */
		void	incrementTxTransactionNumber() noexcept;

		/** */
		void	flushTransactionQ() noexcept;

		/** */
		void	sendTransactionQ() noexcept;

		/** */
		uint8_t	calculateFCS(Oscl::Pdu::Pdu* provPdu) noexcept;

		/** */
		Oscl::Pdu::Composite*	buildStartPdu(
									Oscl::Pdu::Pdu* provPdu,
									unsigned&		remainingSegments
									) noexcept;

		/** */
		Oscl::Pdu::Composite*	buildContinuationPdu(
									Oscl::Pdu::Pdu* provPdu,
									unsigned		startOffset,
									uint8_t			segmentIndex
									) noexcept;

		/** */
		Oscl::Pdu::Composite*	buildProvisiningBearerControlPdu(
									Oscl::Pdu::Pdu* provPdu
									) noexcept;

		/** RETURN: true for failure. */
		bool	buildNewTransactions() noexcept;

	private:
		/** */
		void	completeSegment() noexcept;

	private:
		/** */
		void	sendTransactionACK(uint8_t transactionNumber) noexcept;

	private:
		/** */
		void	stopNext() noexcept;

	private: // Oscl::Done::Operation _allFreeNotification
		/** */
		void	allFreeNotification() noexcept;

	private: // Oscl::Pdu::ReadOnlyVisitor
		/** This operation is called when a Composite node
			is visited in a compound PDU tree. The position is the
			offset relative to the beginning of the client packet.
			The position may be unique to each client.
		 */
		bool /* stop */	accept(
							const Oscl::Pdu::Composite&	p,
							unsigned					position
							) noexcept;

		/** This operation is called when a Composite node
			is visited in a compound PDU tree. The position is the
			offset relative to the beginning of the client packet.
			The position may be unique to each client.
		 */
		bool /* stop */	accept(
							const Oscl::Pdu::ReadOnlyComposite&	p,
							unsigned							position
							) noexcept;

		/** This operation is called when a Leaf node
			is visited in a compound PDU tree. The position is the
			offset relative to the beginning of the client packet.
			The position may be unique to each client. The offset indexes
			the first valid octet in the leaf buffer for the current client.
			The length is the the number of valid octets beginning from
			the offset argument.
		 */
		bool /* stop */ accept(
							const Oscl::Pdu::Leaf&	p,
							unsigned				position,
							unsigned				offset,
							unsigned				length
							) noexcept;

		/** This operation is called when a Leaf node
			is visited in a compound PDU tree. The position is the
			offset relative to the beginning of the client packet.
			The position may be unique to each client.
		 */
		bool /* stop */ accept(
							const Oscl::Pdu::Fragment&	p,
							unsigned					position
							) noexcept;

		/** This operation is called when a Leaf node
			is visited in a compound PDU tree. The position is the
			offset relative to the beginning of the client packet.
			The position may be unique to each client.
		 */
		bool /* stop */ accept(
							const Oscl::Pdu::ReadOnlyFragment&	p,
							unsigned							position
							) noexcept;
	};

}
}
}
}
}

#endif
