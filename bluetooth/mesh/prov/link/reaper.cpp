/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <new>
#include <string.h>

#include "reaper.h"
#include "oscl/error/info.h"
#include "oscl/encoder/be/base.h"
#include "oscl/endian/decoder/linear/part.h"
#include "oscl/bluetooth/debug/print/parse.h"
#include "oscl/bluetooth/constants.h"

using namespace Oscl::BT::Mesh::Prov::Link;

//#define DEBUG_TRACE
//#define DEBUG_TRACE_TMI

static void	tracePbAdvPdu(
				const char*		prefix,
				Oscl::Pdu::Pdu*	pdu
				) noexcept{

	#ifdef DEBUG_TRACE_TMI
	Oscl::Error::Info::log(
		"%s:\n",
		prefix
		);

	uint8_t	buffer[128];

	unsigned
	length	= pdu->read(
				buffer,
				sizeof(buffer),
				0	// offset
				);

	Oscl::Bluetooth::Debug::Print::parseMeshProvisioningPDU(
		buffer,
		length
		);
	#endif
	}

constexpr unsigned	maxPbAdvMTU = 24;

Reaper::Reaper(
	Link::ContextApi&			context,
	Oscl::Pdu::FWD::Api&		provBearerApi,
	Oscl::Timer::Api&			linkTxTimerApi,
	Oscl::Frame::FWD::Api&		clientRxApi,
	uint32_t					linkID,
	bool						provisioner
	) noexcept:
		_context(context),
		_provBearerApi(provBearerApi),
		_linkTxTimerApi(linkTxTimerApi),
		_clientRxApi(clientRxApi),
		_linkID(linkID),
		_provisioner(provisioner),
		_provTxComposer(
			*this,
			&Reaper::txForward
			),
		_rawTxComposer(
			*this,
			&Reaper::rawTxForward
			),
		_allFreeNotification(
			*this,
			&Reaper::allFreeNotification
			),
		_linkTxTimerExpired(
			*this,
			&Reaper::txTimerExpired
			),
		_freeStore(
			OSCL_PRETTY_FUNCTION,
			&_allFreeNotification
			),
		_fcsAccumulator(),
		_rxTransactionNumber(0x00),
		_txTransactionNumber(0xFF),
		_stopping(false)
		{
	_linkTxTimerApi.setExpirationCallback(
		_linkTxTimerExpired
		);
	}

Oscl::Pdu::FWD::Api&	Reaper::getTxApi() noexcept{
	return _provTxComposer;
	}

Oscl::Pdu::FWD::Api&	Reaper::getRawTxApi() noexcept{
	return _rawTxComposer;
	}

void	Reaper::start() noexcept{
	}

void	Reaper::stopNext() noexcept{
	if(!_stopping){
		return;
		}

	if(!_freeStore.allMemoryIsFree()){
		return;
		}

	_stopping	= false;

	_context.stopped(*this);
	}

void	Reaper::allFreeNotification() noexcept{
	if(!_stopping){
		return;
		}
	stopNext();
	}

void	Reaper::stop() noexcept{

	_stopping	= true;

	_linkTxTimerApi.stop();

	flushTransactionQ();

	_currentTx	= 0;

	stopNext();
	}

bool	Reaper::match(uint32_t linkID) noexcept{
	return (linkID == _linkID);
	}

void	Reaper::completeSegment() noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION
		);
	Oscl::Error::Info::hexDump(
		_reasmBuffer,
		_totalLength
		);

	#endif

	Oscl::Stream::HDLC::FCS::Api&			fcsApi	= _fcsAccumulator;

	fcsApi.initialize();

	for(unsigned i=0;i<_totalLength;++i){
		fcsApi.accumulate(_reasmBuffer[i]);
		}

	fcsApi.prepareToGetCRC();

	uint8_t
	fcs	= fcsApi.getNextFcsOctet();

	if(_fec != fcs){
		Oscl::Error::Info::log(
			"%s: FCS FAILED, _fec(0x%2.2X) != fcs(0x%2.2X)\n",
			OSCL_PRETTY_FUNCTION,
			_fec,
			fcs
			);
		return;
		}

	sendTransactionACK(_rxTransactionNumber);

	_clientRxApi.forward(_reasmBuffer,_totalLength);
	}

void	Reaper::txForward(Oscl::Pdu::Pdu* pdu) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif

	_txQ.put(pdu);
	processTxQ();
	}

void	Reaper::rawTxForward(Oscl::Pdu::Pdu* pdu) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif

	Oscl::Handle<Oscl::Pdu::Composite>
	composite	= buildProvisiningBearerControlPdu(pdu);

	if(!composite){
		return;
		}

	_provBearerApi.transfer(composite);
	}

void	Reaper::processTxQ() noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif

	if(_currentTx){
		return;
		}

	_currentTx	= _txQ.get();

	if(!_currentTx){
		return;
		}

	_transactionRetryCount	= 15;

	bool
	failed	= buildNewTransactions();

	if(failed){
		return;
		}

	sendTransactionQ();
	}

void	Reaper::flushTransactionQ() noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif

	Oscl::Handle<Oscl::Pdu::Pdu>	pdu;
	while((pdu=_transactionQ.get())){
		pdu	= 0;
		}
	}

void	Reaper::sendTransactionQ() noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif

	Oscl::Pdu::Pdu*	pdu;
	for(
		pdu	= _transactionQ.first();
		pdu;
		pdu	= _transactionQ.next(pdu)
		){
			tracePbAdvPdu(
				OSCL_PRETTY_FUNCTION,
				pdu
				);
			_provBearerApi.transfer(pdu);
		}

	// FIXME: Need a *real* timer value.
	static const unsigned	retryTimerInMs	= 2000;

	_linkTxTimerApi.restart(retryTimerInMs);
	}

void	Reaper::txTimerExpired() noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"***************** %s\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif

	if(!_transactionQ.first()){
		return;
		}

	if(!_transactionRetryCount){

		flushTransactionQ();

		_currentTx	= 0;

		Oscl::Error::Info::log(
			"%s: Failed no more retries.\n",
			OSCL_PRETTY_FUNCTION
			);

		processTxQ();
		return;
		}

	--_transactionRetryCount;

	sendTransactionQ();
	}

void	Reaper::incrementTxTransactionNumber() noexcept{
	++_txTransactionNumber;
	if(_provisioner){
		_txTransactionNumber	&= ~0x80;
		}
	else {
		_txTransactionNumber	|= 0x80;
		}
	}

uint8_t	Reaper::calculateFCS(Oscl::Pdu::Pdu* provPdu) noexcept{
	Oscl::Stream::HDLC::FCS::Api&	fcsApi	= _fcsAccumulator;

	fcsApi.initialize();

	provPdu->visit(
				*this,
				0,					// position,
				0,					// firstValid,
				provPdu->length()	// nValid
				);

	fcsApi.prepareToGetCRC();

	return fcsApi.getNextFcsOctet();
	}

constexpr unsigned	startHeaderSize	=
							sizeof(uint32_t)	// sizeof(linkID)
						+	sizeof(uint8_t)		// sizeof(transactionNumber)
						+	sizeof(uint8_t)		// sizeof(typeSeg)
						+	sizeof(uint16_t)	// sizeof(totalLength)
						+	sizeof(uint8_t)		// sizeof(fcs)
						;

constexpr unsigned	continueHeaderSize	=
							sizeof(uint32_t)	// sizeof(linkID)
						+	sizeof(uint8_t)		// sizeof(transactionNumber)
						+	sizeof(uint8_t)		// sizeof(typeSeg)
						;

Oscl::Pdu::Composite*	Reaper::buildProvisiningBearerControlPdu(
							Oscl::Pdu::Pdu* provPdu
							) noexcept{

	Oscl::Handle<Oscl::Pdu::Fragment>
	encapsulated	= _freeStore.allocFragment(*provPdu);

	if(!encapsulated){
		Oscl::Error::Info::log(
			"%s: out of encapsulated fragment memory.\n",
			OSCL_PRETTY_FUNCTION
			);
		return 0;
		}

	unsigned	bufferSize;

	void*
	bufferMem	= _freeStore.allocBuffer(bufferSize);

	if(!bufferMem){
		Oscl::Error::Info::log(
			"%s: out of buffer memory.\n",
			OSCL_PRETTY_FUNCTION
			);
		return 0;
		}

	Oscl::Encoder::BE::Base
	beEncoder(
		bufferMem,
		bufferSize
		);

	Oscl::Encoder::Api&		encoder	= beEncoder;

	encoder.encode(_linkID);
	encoder.encode(_txTransactionNumber);

	Oscl::Handle<Oscl::Pdu::Fragment>
	header	=	_freeStore.allocFragmentWrapper(
						bufferMem,
						startHeaderSize
						);

	if(!header){
		Oscl::Error::Info::log(
			"%s: out of header fragment memory.\n",
			OSCL_PRETTY_FUNCTION
			);
		return 0;
		}

	Oscl::Pdu::Composite*
	composite	= _freeStore.allocComposite();

	composite->append(header);
	composite->append(encapsulated);

	tracePbAdvPdu(
		OSCL_PRETTY_FUNCTION,
		composite
		);

	return composite;
	}

Oscl::Pdu::Composite*	Reaper::buildStartPdu(
							Oscl::Pdu::Pdu* provPdu,
							unsigned&		remainingSegments
							) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s: _txTransactionNumber: 0x%2.2X\n",
		OSCL_PRETTY_FUNCTION,
		_txTransactionNumber
		);
	#endif

	Oscl::Handle<Oscl::Pdu::Fragment>
	encapsulated	= _freeStore.allocFragment(*provPdu);

	if(!encapsulated){
		Oscl::Error::Info::log(
			"%s: out of encapsulated fragment memory.\n",
			OSCL_PRETTY_FUNCTION
			);
		return 0;
		}

	unsigned	bufferSize;

	void*
	bufferMem	= _freeStore.allocBuffer(bufferSize);

	if(!bufferMem){
		Oscl::Error::Info::log(
			"%s: out of buffer memory.\n",
			OSCL_PRETTY_FUNCTION
			);
		return 0;
		}

	Oscl::Encoder::BE::Base
	beEncoder(
		bufferMem,
		bufferSize
		);

	Oscl::Encoder::Api&		encoder	= beEncoder;

	unsigned	len	= provPdu->length();

	if(len > 0xFFFF){
		Oscl::Error::Info::log(
			"%s: PDU too long (%u octets).\n",
			OSCL_PRETTY_FUNCTION,
			len
			);
		return 0;
		}

	uint8_t		typeSeg;
	uint16_t	totalLength	= len;
	uint8_t		lastSegment;
	uint8_t		fcs;

	unsigned	maxStartPayloadSize	= Oscl::BT::maxAdDataLength - startHeaderSize;

	unsigned	maxContinuePayloadSize	= Oscl::BT::maxAdDataLength - continueHeaderSize;

	unsigned	nSegments	= 1;

	unsigned	totalContinuePayload	= 0;

	if(len > maxStartPayloadSize){
		totalContinuePayload	= totalLength - maxStartPayloadSize;
		nSegments	+= totalContinuePayload / maxContinuePayloadSize + 1;
		}

	if(nSegments > 0x3F){
		Oscl::Error::Info::log(
			"%s: too many segments (%u segments).\n",
			OSCL_PRETTY_FUNCTION,
			nSegments
			);
		return 0;
		}

	lastSegment	= nSegments - 1;

	typeSeg	=
				(0x00<<0)			// GPCF: Transaction Start PDU
			|	(lastSegment<<2)	// last segment
			;

	fcs			= calculateFCS(provPdu);

	encoder.encode(_linkID);
	encoder.encode(_txTransactionNumber);
	encoder.encode(typeSeg);
	encoder.encode(totalLength);
	encoder.encode(fcs);

	Oscl::Handle<Oscl::Pdu::Fragment>
	header	=	_freeStore.allocFragmentWrapper(
						bufferMem,
						startHeaderSize
						);

	if(!header){
		Oscl::Error::Info::log(
			"%s: out of header fragment memory.\n",
			OSCL_PRETTY_FUNCTION
			);
		return 0;
		}

	Oscl::Pdu::Composite*
	composite	= _freeStore.allocComposite();

	if(totalContinuePayload){
		encapsulated->stripTrailer(totalContinuePayload);
		}

	composite->append(header);
	composite->append(encapsulated);

	remainingSegments	= lastSegment;

	tracePbAdvPdu(
		OSCL_PRETTY_FUNCTION,
		composite
		);

	return composite;
	}

Oscl::Pdu::Composite*	Reaper::buildContinuationPdu(
							Oscl::Pdu::Pdu* provPdu,
							unsigned		startOffset,
							uint8_t			segmentIndex
							) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s: startOffset: %u, segmentIndex: %u\n",
		OSCL_PRETTY_FUNCTION,
		startOffset,
		segmentIndex
		);
	#endif

	unsigned	len	= provPdu->length();

	if(len < startOffset){
		Oscl::Error::Info::log(
			"%s: (len(%u) < startOffset(%u)).\n",
			OSCL_PRETTY_FUNCTION,
			len,
			startOffset
			);
		return 0;
		}

	Oscl::Handle<Oscl::Pdu::Fragment>
	encapsulated	= _freeStore.allocFragment(*provPdu);

	if(!encapsulated){
		Oscl::Error::Info::log(
			"%s: out of encapsulated fragment memory.\n",
			OSCL_PRETTY_FUNCTION
			);
		return 0;
		}

	unsigned	bufferSize;

	void*
	bufferMem	= _freeStore.allocBuffer(bufferSize);

	if(!bufferMem){
		Oscl::Error::Info::log(
			"%s: out of buffer memory.\n",
			OSCL_PRETTY_FUNCTION
			);
		return 0;
		}

	Oscl::Encoder::BE::Base
	beEncoder(
		bufferMem,
		bufferSize
		);

	Oscl::Encoder::Api&		encoder	= beEncoder;

	const	uint8_t	typeSeg	=
							(0x02<<0)			// GPCF: Transaction Start PDU
						|	(segmentIndex<<2)	// This segment
						;

	encoder.encode(_linkID);
	encoder.encode(_txTransactionNumber);
	encoder.encode(typeSeg);

	Oscl::Handle<Oscl::Pdu::Fragment>
	header	=	_freeStore.allocFragmentWrapper(
						bufferMem,
						encoder.length()
						);

	if(!header){
		Oscl::Error::Info::log(
			"%s: out of header fragment memory.\n",
			OSCL_PRETTY_FUNCTION
			);
		_freeStore.freeBuffer(bufferMem);
		return 0;
		}

	Oscl::Pdu::Composite*
	composite	= _freeStore.allocComposite();

	encapsulated->stripHeader(startOffset);

	const unsigned
	maxContinuePayloadSize	= Oscl::BT::maxAdDataLength - continueHeaderSize;

	unsigned
	remaining	= encapsulated->getLength();

	if(remaining > maxContinuePayloadSize){
		remaining	-= maxContinuePayloadSize;
		encapsulated->stripTrailer(remaining);
		}

	composite->append(header);
	composite->append(encapsulated);

	tracePbAdvPdu(
		OSCL_PRETTY_FUNCTION,
		composite
		);

	return composite;
	}

bool	Reaper::buildNewTransactions() noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif

	// FIXME:
	// Allocate new transaction number.
	// Wrap _currentTx and add header.

	// allocate a fragment wrapper for each segment.
	// - one for the transaction start pdu
	// - one each additional transaction continuation pdu
	// adjust each fragment using stripHeader() and stripTrailer()
	// to the MTU size.
	// allocate a composite for each segment
	// allocate and create a header buffer for each
	//  segment and prepend to each composite.
	// queue all segments in preparation for transfer
	// to the bearer.

	incrementTxTransactionNumber();
//	_transactionNumber	= _txTransactionNumber;

	unsigned	remainingSegments;

	Oscl::Pdu::Composite*
	pdu	= buildStartPdu(_currentTx,remainingSegments);

	if(!pdu){
		Oscl::Error::Info::log(
			"%s: buildStartPdu() failed.\n",
			OSCL_PRETTY_FUNCTION
			);
		return true;
		}

	_transactionQ.put(pdu);

	unsigned	startPduMtuLength		= Oscl::BT::maxAdDataLength - startHeaderSize;

	unsigned	continuePduMtuLength	= Oscl::BT::maxAdDataLength - continueHeaderSize;

	unsigned	offset	= startPduMtuLength;;

	unsigned	segmentIndex	= 1;

	for(;remainingSegments;--remainingSegments,++segmentIndex){
		pdu	= buildContinuationPdu(
				_currentTx,
				offset,
				segmentIndex
				);
		if(!pdu){
			flushTransactionQ();
			Oscl::Error::Info::log(
				"%s: buildContinuationPdu() failed.\n",
				OSCL_PRETTY_FUNCTION
				);
			return true;
			}

		offset	+= continuePduMtuLength;

		_transactionQ.put(pdu);
		}

	return false;
	}

bool	Reaper::parseTransactionStart(
			uint8_t			transactionNumber,
			uint8_t			segN,
			const void*		frame,
			uint8_t 		length
			) noexcept{

	Oscl::Endian::Decoder::Linear::Part
	beDecoder(
		frame,
		length
		);

	Oscl::Decoder::Api&
	decoder	= beDecoder.be();

	decoder.decode(_totalLength);
	decoder.decode(_fec);

	// Expect invite
	// type octet: 0x00 PROV_TYPE_INVITE
	// attention octet: time in seconds?

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow\n",
			OSCL_PRETTY_FUNCTION
			);
		return true;
		}

	const uint8_t*	p	= (const uint8_t*)frame;

	unsigned
	headerSize	= sizeof(_totalLength)+sizeof(_fec);

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s: _linkID: 0x%8.8X, transactionNumber: 0x%2.2X, segN: 0x%2.2X, _totalLength: %u, _fec: 0x%2.2X\n",
		OSCL_PRETTY_FUNCTION,
		_linkID,
		transactionNumber,
		segN,
		_totalLength,
		_fec
		);
	Oscl::Error::Info::hexDump(
		p,
		length
		);
	#endif

	_reasmOffset	= 0;

	unsigned	startPduMtu	= maxPbAdvMTU - 4;

	_rxTransactionNumber	= transactionNumber;

	if(_totalLength <= startPduMtu){
		memcpy(
			&_reasmBuffer[_reasmOffset],
			&p[headerSize],
			_totalLength
			);
		#ifdef DEBUG_TRACE_TMI
		Oscl::Error::Info::hexDump(
			_reasmBuffer,
			_totalLength
			);
		#endif
		completeSegment();
		return true;
		}

	memcpy(
		_reasmBuffer,
		&p[headerSize],
		startPduMtu
		);

	#ifdef DEBUG_TRACE_TMI
	Oscl::Error::Info::hexDump(
		_reasmBuffer,
		startPduMtu
		);
	#endif

	_reasmOffset		= startPduMtu;

	_lastExpectedSegment	= segN;

	_nextExpectedSegment	= 1;

	return true;
	}

bool	Reaper::parseTransactionAcknowledgement(
			uint8_t			transactionNumber,
			const void*		frame,
			uint8_t 		length
			) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s: _linkID: 0x%8.8X, transactionNumber: 0x%2.2X\n",
		OSCL_PRETTY_FUNCTION,
		_linkID,
		transactionNumber
		);
	#endif

	if(!_currentTx){
		return true;
		}

	if(transactionNumber != _txTransactionNumber){
		// This is a duplicate transaction.

		#ifdef DEBUG_TRACE
		Oscl::Error::Info::log(
			"%s: (transactionNumber(0x%2.2X) != _txTransactionNumber(0x%2.2X))\n",
			OSCL_PRETTY_FUNCTION,
			transactionNumber,
			_txTransactionNumber
			);
		#endif
		return true;
		}

	_linkTxTimerApi.stop();

	flushTransactionQ();

	_currentTx	= 0;

	processTxQ();

	return true;
	}

bool	Reaper::parseTransactionContinuation(
			uint8_t			transactionNumber,
			uint8_t			segIndex,
			const void*		frame,
			uint8_t 		length
			) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s: _linkID: 0x%8.8X, transactionNumber: 0x%2.2X, segIndex: 0x%2.2X\n",
		OSCL_PRETTY_FUNCTION,
		_linkID,
		transactionNumber,
		segIndex
		);

	Oscl::Error::Info::hexDump(
		frame,
		length
		);
	#endif

	if(transactionNumber != _rxTransactionNumber){
		// We either have not received a transaction start
		// for this transaction number, or something is hosed.

		#ifdef DEBUG_TRACE
		Oscl::Error::Info::log(
			"%s: (transactionNumber(0x%2.2X) != _rxTransactionNumber(0x%2.2X))\n",
			OSCL_PRETTY_FUNCTION,
			transactionNumber,
			_rxTransactionNumber
			);
		#endif
		return true;
		}

	if(segIndex != _nextExpectedSegment){
		// We missed a segment.

		#ifdef DEBUG_TRACE
		Oscl::Error::Info::log(
			"%s: (segIndex(0x%2.2X) != _nextExpectedSegment(0x%2.2X))\n",
			OSCL_PRETTY_FUNCTION,
			segIndex,
			_nextExpectedSegment
			);
		#endif
		return true;
		}

	unsigned	continuePduMtu	= maxPbAdvMTU - 1;

	bool
	lastSegment	= (segIndex == _lastExpectedSegment);

	unsigned len	= lastSegment?
						(_totalLength-_reasmOffset):
						continuePduMtu
						;

	const uint8_t*	p	= (const uint8_t*)frame;

	memcpy(
		&_reasmBuffer[_reasmOffset],
		p,
		len
		);

	_reasmOffset	+= len;

	if(!lastSegment){
		_nextExpectedSegment	+= 1;
		return true;
		}

	if(_reasmOffset != _totalLength){
		Oscl::Error::Info::log(
			"%s: _reasmOffset != _totalLength\n",
			OSCL_PRETTY_FUNCTION
			);
		return true;
		}

	completeSegment();

	return true;
	}

void	Reaper::sendTransactionACK(
			uint8_t		transactionNumber
			) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s: _linkID: 0x%8.8X, transactionNumber: 0x%2.2X\n",
		OSCL_PRETTY_FUNCTION,
		_linkID,
		transactionNumber
		);
	#endif

	unsigned	bufferSize;

	void*
	bufferMem	= _freeStore.allocBuffer(bufferSize);

	if(!bufferMem){
		Oscl::Error::Info::log(
			"%s: out of buffer memory.\n",
			OSCL_PRETTY_FUNCTION
			);
		return;
		}

	Oscl::Encoder::BE::Base
	beEncoder(
		bufferMem,
		bufferSize
		);

	Oscl::Encoder::Api&		encoder	= beEncoder;

	static const	uint8_t	typeSeg	=
										(0x01<<0)	// GPCF: Transaction Acknowledgment
									|	(0x00<<2)	// padding
									;

	encoder.encode(_linkID);
	encoder.encode(transactionNumber);
	encoder.encode(typeSeg);

	Oscl::Handle<Oscl::Pdu::Fixed>
	fixed	= _freeStore.allocFixed(
				bufferMem,
				encoder.length()
				);

	if(!fixed){
		_freeStore.freeBuffer(bufferMem);
		Oscl::Error::Info::log(
			"%s: out of fragment memory.\n",
			OSCL_PRETTY_FUNCTION
			);
		return;
		}

	tracePbAdvPdu(
		OSCL_PRETTY_FUNCTION,
		fixed
		);

	_provBearerApi.transfer(fixed);
	}

bool Reaper::accept(
		const Oscl::Pdu::Composite&	p,
		unsigned					position
		) noexcept{
	return false;
	}

bool Reaper::accept(
		const Oscl::Pdu::ReadOnlyComposite&	p,
		unsigned							position
		) noexcept{
	return false;
	}

bool Reaper::accept(
		const Oscl::Pdu::Leaf&	p,
		unsigned				position,
		unsigned				offset,
		unsigned				length
		) noexcept{

	Oscl::Stream::HDLC::FCS::Api&	fcsApi	= _fcsAccumulator;

	unsigned	len	= length;

	const uint8_t*
	buffer	= p.getBuffer(
				0, // offset
				len
				);

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s:\n",
		OSCL_PRETTY_FUNCTION
		);

	Oscl::Error::Info::hexDump(
		buffer,
		len
		);
	#endif

	for(unsigned i=0;i<len;++i){
		fcsApi.accumulate(buffer[i]);
		}

	return false;
	}

bool Reaper::accept(
		const Oscl::Pdu::Fragment&	p,
		unsigned					position
		) noexcept{
	return false;
	}

bool Reaper::accept(
		const Oscl::Pdu::ReadOnlyFragment&	p,
		unsigned							position
		) noexcept{
	return false;
	}
