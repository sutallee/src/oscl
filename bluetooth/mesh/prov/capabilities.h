/*
   Copyright (C) 2020 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_bluetooth_mesh_prov_capabilitiesh_
#define _oscl_bluetooth_mesh_prov_capabilitiesh_

namespace Oscl {
namespace BT {
namespace Mesh {
namespace Prov {
namespace Capabilities {
namespace OOB {
namespace Action {
namespace Output {

constexpr unsigned	blinkBit		= 0;
constexpr unsigned	beepBit			= 1;
constexpr unsigned	vibrateBit		= 2;
constexpr unsigned	numericBit		= 3;
constexpr unsigned	alphaNumericBit	= 4;

}

namespace Input {

constexpr static unsigned	pushBit			= 0;
constexpr static unsigned	twistBit		= 1;
constexpr static unsigned	numberBit		= 2;
constexpr static unsigned	alphanumericBit	= 3;

}

}
}
}
}
}
}
}

#endif

