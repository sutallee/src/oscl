/*
   Copyright (C) 2020 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_bluetooth_mesh_prov_errorsh_
#define _oscl_bluetooth_mesh_prov_errorsh_

#include <stdint.h>

namespace Oscl {
namespace BT {
namespace Mesh {
namespace Prov {
namespace Errors {

constexpr uint8_t	prohibited			= 0x00;
constexpr uint8_t	invalidPDU			= 0x01;
constexpr uint8_t	invalidFormat		= 0x02;
constexpr uint8_t	unexpectedPDU		= 0x03;
constexpr uint8_t	confirmationFailed	= 0x04;
constexpr uint8_t	outOfResources		= 0x05;
constexpr uint8_t	decryptionFailed	= 0x06;
constexpr uint8_t	unexpectedError		= 0x07;
constexpr uint8_t	cannotAssignAddress	= 0x08;

}
}
}
}
}

#endif


