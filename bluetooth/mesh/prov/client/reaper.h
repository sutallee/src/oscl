/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_bluetooth_mesh_prov_client_reaperh_
#define _oscl_bluetooth_mesh_prov_client_reaperh_

#include <stdint.h>
#include "oscl/memory/block.h"
#include "oscl/done/operation.h"
#include "oscl/frame/fwd/composer.h"
#include "oscl/pdu/fwd/api.h"
#include "oscl/freestore/composers.h"
#include "oscl/queue/queue.h"
#include "oscl/pdu/fixed.h"
#include "oscl/pdu/composite.h"
#include "oscl/pdu/fragment.h"

#include "oscl/endian/decoder/api.h"
#include "oscl/pdu/memory/api.h"
#include "oscl/bluetooth/mesh/network/api.h"

/** */
namespace Oscl {
/** */
namespace BT {
/** */
namespace Mesh {
/** */
namespace Prov {
/** */
namespace Client {

/** */
class Reaper;

/** */
class ContextApi {
	public:
		/** This operation is called when the Reaper
			is stopped after it completes the process
			and is ready to be destroyed.
		 */
		virtual void	stopped(Reaper& device) noexcept=0;

		/** This operation is called by the Reaper to
			register a provisioned node on the network.
		 */
		virtual void	registerNewNode(
							uint16_t		networkIndex,
							uint16_t		unicastAddress
							) noexcept=0;

		/** */
		virtual uint16_t	allocateAddresses(uint16_t nElements) noexcept=0;

		/** */
		virtual const void*		getNetKey() const noexcept=0;

		/** */
		virtual uint16_t	getKeyIndex() const noexcept=0;

		/** */
		virtual uint8_t		getFlags() const noexcept=0;

		/** */
		virtual uint32_t	getTxIvIndex() const noexcept=0;
	};

/** */
class Reaper {
	private:
		/** */
		Client::ContextApi&				_context;

		/** */
		Oscl::Pdu::Memory::Api&			_freeStoreApi;

		/** */
		Oscl::Pdu::FWD::Api&			_linkApi;

		/** */
		Oscl::Pdu::FWD::Api&			_rawLinkApi;

		/** */
		const uint32_t					_linkID;

	private:
		/** */
		Oscl::Frame::FWD::Composer<Reaper>	_rxComposer;

	private:
		/** */
		constexpr static unsigned	bufferSize	= 128;

		/** */
		union BufferMem {                   
			void*   __qitemlink;
			Oscl::Memory::AlignedBlock<bufferSize>	mem;
			};

		/** */
		constexpr static unsigned	nProvBuffers	= 32;

		/** */
		BufferMem								_buffers[nProvBuffers];

		/** */
		Oscl::Queue<BufferMem>					_bufferPool;

		/** */
		Oscl::FreeStore::Composer<Reaper,void>	_bufferFreeStoreApiComposer;

		/** */
		Oscl::FreeStore::FreeStoreMgr<void>		_bufferFreeStoreMgrComposer;

	private:
		/** Stored in ECDH byte-order
		 */
		uint8_t			_privateKey[32];

		/** Stored in BT byte-order
		 */
		uint8_t			_publicKeyX[32];

		/** Stored in BT byte-order
		 */
		uint8_t			_publicKeyY[32];

		/** Stored in BT byte order
		 */
		uint8_t			_ecdhSecret[32];

	private:
		/** */
		uint8_t			_invitePdu[1];

		/** */
		uint8_t			_capabilitiesPdu[11];

		/** */
		uint8_t			_startPdu[5];

		/** Stored in BT byte-order
		 */
		uint8_t			_devicePublicKey[64];

		/** */
		uint8_t			_deviceRandom[16];

		/** */
		uint8_t			_provisionerRandom[16];

	private:
		/** */
		uint8_t			_deviceConfirmationData[16];

		/** */
		uint8_t			_confirmationData[16];

		/** */
		uint8_t			_confirmationKey[16];

		/** */
		uint8_t			_confirmationSalt[16];

		/** */
		uint8_t			_provisioningSalt[16];

		/** */
		uint8_t			_sessionKey[16];

		/** */
		uint8_t			_sessionNonce[16];

	private:
		/** */
		bool			_stopping;

		/** */
		bool			_inviteSent;

		/** */
		bool			_startSent;

		/** */
		bool			_publicKeySent;

		/** */
		bool			_confirmationSent;

		/** */
		bool			_randomSent;

		/** */
		bool			_dataSent;

	private:
		/** */
		uint8_t			_nElements;

		/** */
		uint16_t		_algorithms;

		/** */
		uint8_t			_publicKeyType;

		/** */
		uint8_t			_staticOobType;

		/** */
		uint8_t			_staticOobSize;

	private:
		/** */
		uint16_t		_networkKeyIndex;

		/** */
		uint16_t		_unicastAddress;

	public:
		/** */
		Reaper(
			Client::ContextApi&				context,
			Oscl::Pdu::Memory::Api&			freeStoreApi,
			Oscl::Pdu::FWD::Api&			linkApi,
			Oscl::Pdu::FWD::Api&			rawLinkApi,
			uint32_t						linkID
			) noexcept;

		/** */
		Oscl::Frame::FWD::Api&	getRxApi() noexcept;

		/** */
		uint32_t	getLinkID() const noexcept;

		/** */
		void	start() noexcept;

		/** */
		void	stop() noexcept;

	private:
		/** */
		void	stopNext() noexcept;

		/** */
		bool	allMemoryIsFree() noexcept;

		/** */
		bool	allBuffersFree() noexcept;

	private: // Oscl::FreeStore::FreeStoreApi	_bufferFreeStoreApiComposer;
		/** */
		void	bufferFree(void* buffer) noexcept;

		/** */
		void	freeBuffer(BufferMem* buffer) noexcept;

		/** */
		BufferMem*	allocBuffer() noexcept;

	private: // Oscl::BT::Mesh::::Prov::Session::Api
		/** */
		bool	receive(
					const void*		frame,
					unsigned int	length
					) noexcept;
	private:
		/** */
		void	extractAndSaveKeys(
					const uint8_t*	privateKey,
					const uint8_t*	publicKey
					) noexcept;

		/** */
		bool	createProvisionerPublicAndPrivateKeys() noexcept;

		/** */
		void	calculateConfirmationKey() noexcept;

		/** */
		void	calculateConfirmationData() noexcept;

		/** */
		void	calculateEcdhSecret() noexcept;

		/** RETURN: true for failure */
		bool	checkNoOobConfirmation(const uint8_t* random) noexcept;

		/** */
		void	calculateSessionNonce() noexcept;

		/** */
		void	calculateSessionKey() noexcept;

	private:
		/** */
		bool	processInvite(Oscl::Decoder::Api& decoder) noexcept;

		/** */
		bool	processCapabilities(Oscl::Decoder::Api& decoder) noexcept;

		/** */
		bool	processStart(Oscl::Decoder::Api& decoder) noexcept;

		/** */
		bool	processPublicKey(Oscl::Decoder::Api& decoder) noexcept;

		/** */
		bool	processInputComplete(Oscl::Decoder::Api& decoder) noexcept;

		/** */
		bool	processConfirmation(Oscl::Decoder::Api& decoder) noexcept;

		/** */
		bool	processRandom(Oscl::Decoder::Api& decoder) noexcept;

		/** */
		bool	processData(Oscl::Decoder::Api& decoder) noexcept;

		/** */
		bool	processComplete(Oscl::Decoder::Api& decoder) noexcept;

		/** */
		bool	processFailed(Oscl::Decoder::Api& decoder) noexcept;

	private:
		/** RETURN: true for failure */
		bool	sendClose(uint8_t reason) noexcept;

		/** RETURN: true for failure */
		bool	sendInvite() noexcept;

		/** RETURN: true for failure */
		bool	sendStart(
					uint8_t	algorithm,
					uint8_t	publicKeyType,
					uint8_t	authenticationMethod,
					uint8_t	authenticationAction,
					uint8_t	authenticationSize
					) noexcept;

		/** RETURN: true for failure */
		bool	sendPublicKey() noexcept;

		/** RETURN: true for failure */
		bool	sendConfirmation() noexcept;

		/** RETURN: true for failure */
		bool	sendRandom() noexcept;

		/** RETURN: true for failure */
		bool	sendData() noexcept;

		/** RETURN: true for failure */
		bool	sendFailed(uint8_t errorCode) noexcept;
	};

}
}
}
}
}

#endif
