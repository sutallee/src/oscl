/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <new>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <string.h>

#include "reaper.h"
#include "oscl/error/fatal.h"
#include "oscl/error/info.h"
#include "oscl/posix/errno/errstring.h"
#include "oscl/encoder/be/base.h"
#include "oscl/endian/decoder/linear/part.h"
#include "oscl/bluetooth/debug/print/parse.h"
#include "oscl/bluetooth/strings/module.h"

#include "oscl/freestore/nullmgr.h"

#include "oscl/entropy/rand.h"
#include "oscl/crypto/nist/fips/p256/ecc.h"
#include "oscl/bluetooth/crypto/keys.h"
#include "oscl/bluetooth/crypto/salt.h"
#include "oscl/aes128/cmac.h"
#include "oscl/aes128/ccm.h"

constexpr uint8_t	errorCodeProhibited			= 0x00;
constexpr uint8_t	errorCodeInvalidPDU			= 0x01;
constexpr uint8_t	errorCodeInvalidFormat		= 0x02;
constexpr uint8_t	errorCodeUnexpectedPDU		= 0x03;
constexpr uint8_t	errorCodeConfirmationFailed	= 0x04;
constexpr uint8_t	errorCodeOutOfResources		= 0x05;
constexpr uint8_t	errorCodeDecryptionFailed	= 0x06;

constexpr uint8_t	closeReasonSuccess				= 0x00;
constexpr uint8_t	closeReasonTransactionTimeout	= 0x01;
constexpr uint8_t	closeReasonProvisioningFailed	= 0x02;

using namespace Oscl::BT::Mesh::Prov::Client;

static Oscl::FreeStore::NullMgr	nullFreestoreMgr;

Reaper::Reaper(
	Client::ContextApi&				context,
	Oscl::Pdu::Memory::Api&			freeStoreApi,
	Oscl::Pdu::FWD::Api&			linkApi,
	Oscl::Pdu::FWD::Api&			rawLinkApi,
	uint32_t						linkID
	) noexcept:
		_context(context),
		_freeStoreApi(freeStoreApi),
		_linkApi(linkApi),
		_rawLinkApi(rawLinkApi),
		_linkID(linkID),
		_rxComposer(
			*this,
			&Reaper::receive
			),
		_bufferFreeStoreApiComposer(
			*this,
			&Reaper::bufferFree
			),
		_bufferFreeStoreMgrComposer(
			_bufferFreeStoreApiComposer
			),
		_stopping(false),
		_inviteSent(false),
		_startSent(false),
		_publicKeySent(false),
		_confirmationSent(false),
		_randomSent(false),
		_dataSent(false),
		_networkKeyIndex(0),
		_unicastAddress(0)
		{
	}

Oscl::Frame::FWD::Api&	Reaper::getRxApi() noexcept{
	return _rxComposer;
	}

uint32_t	Reaper::getLinkID() const noexcept{
	return _linkID;
	}

void	Reaper::start() noexcept{

#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
#endif

	for(unsigned i=0;i<nProvBuffers;++i){
		_bufferPool.put(&_buffers[i]);
		}

	bool
	failed	= createProvisionerPublicAndPrivateKeys();

	if(failed){
		Oscl::Error::Info::log(
			"%s: ecc_make_key_using_external_random() failed!\n",
			OSCL_PRETTY_FUNCTION
			);
		stop();
		return;
		}

	failed	= sendInvite();

	if(failed){
		// This *should* never happen, since
		// it means we're out of resources (already).
		Oscl::Error::Info::log(
			"%s: sendInvite() failed!\n",
			OSCL_PRETTY_FUNCTION
			);
		stop();
		return;
		}

	_inviteSent	= true;
	}

void	Reaper::stop() noexcept{

	if(_stopping){
		return;
		}

	_stopping	= true;

	stopNext();
	}

void	Reaper::stopNext() noexcept{
	if(!_stopping){
		return;
		}

	if(!allMemoryIsFree()){
		return;
		}

	_stopping	= false;

	_context.stopped(*this);
	}

bool	Reaper::allMemoryIsFree() noexcept{
	bool
	buffersFree	= allBuffersFree();

	if(!buffersFree){
#ifdef DEBUG_TRACE
		Oscl::Error::Info::log(
			"%s: all buffers not free.\n",
			__PRETTY_FUNCTION__
			);
#endif
		return false;
		}

	return true;
	}

bool	Reaper::allBuffersFree() noexcept{
	unsigned	n	= 0;
	BufferMem*	mem;
	for(
		mem	= _bufferPool.first();
		mem;
		mem	= _bufferPool.next(mem)
		){
		++n;
		}

#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s: (n(%u) < nProvBuffers(%u)): %s\n",
		__PRETTY_FUNCTION__,
		n,
		nProvBuffers,
		(n < nProvBuffers)?"false":"true"
		);
#endif

	return (n < nProvBuffers)?false:true;
	}

void	Reaper::bufferFree(void* buffer) noexcept{
	// This would do ITC in an MT application.
	freeBuffer((BufferMem*)buffer);
	}

void	Reaper::freeBuffer(BufferMem* buffer) noexcept{
	_bufferPool.put(buffer);
	stopNext();
	}

Oscl::BT::Mesh::Prov::Client::Reaper::BufferMem*	Reaper::allocBuffer() noexcept{
	return _bufferPool.get();
	}

bool	Reaper::processInvite(Oscl::Decoder::Api& decoder) noexcept{
#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
#endif

	// Invalid for Provisioner Client
	sendFailed(errorCodeUnexpectedPDU);

	return true;
	}

bool	Reaper::processCapabilities(Oscl::Decoder::Api& decoder) noexcept{
#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
#endif

	if(_startSent){
		return true;
		}

	Oscl::Decoder::State	state;

	decoder.push(
		state,
		decoder.remaining()
		);

	decoder.copyOut(
		_capabilitiesPdu,
		sizeof(_capabilitiesPdu)
		);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow\n",
			OSCL_PRETTY_FUNCTION
			);
		sendFailed(errorCodeInvalidFormat);
		return true;
		}

	decoder.pop();

	decoder.decode(_nElements);
	decoder.decode(_algorithms);
	decoder.decode(_publicKeyType);
	decoder.decode(_staticOobType);
	decoder.decode(_staticOobSize);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow\n",
			OSCL_PRETTY_FUNCTION
			);
		sendFailed(errorCodeInvalidFormat);
		return false;
		}

#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s:"
		" Number of Elements: %u,"
		" Algorithms: 0x%4.4X,"
		" Public Key Type: 0x%2.2X,"
		" Static OOB Type: 0x%2.2X,"
		" Static OOB Size: 0x%2.2X"
		"\n",
		__PRETTY_FUNCTION__,
		_nElements,
		_algorithms,
		_publicKeyType,
		_staticOobType,
		_staticOobSize
		);
#endif

	constexpr uint8_t	algorithm	= 0x00;	// FIPS P-256 Elliptic Curve

	// FIXME:
	// SHOULD choose these based on the
	// capabilities. For now, however,
	// We just use the universal
	// unauthenticated method.
	uint8_t	publicKeyType	= 0x00;	// No OOB Public Key is used
	uint8_t	authenticationMethod	= 0x00;	// No OOB authentication is used
	uint8_t	authenticationAction	= 0x00;	// Blink (ignored)
	uint8_t	authenticationSize		= 0x00;	// Prohibited (ignored)

	if(!_startSent){
		bool
		failed	= sendStart(
						algorithm,
						publicKeyType,
						authenticationMethod,
						authenticationAction,
						authenticationSize
						);

		if(failed){
			// This *should* never happen.
			Oscl::Error::Info::log(
				"%s: sendStart() failed.\n",
				OSCL_PRETTY_FUNCTION
				);
			// FIXME: We should start a timer and try again?
			// Or, we could just wait for a retransmission
			// of the capabilities?
			// Alternately, we could just stop the session
			// under the assumption that the server/device
			// will time-out itself.
			stop();
			return true;
			}
		}

	_startSent	= true;

	if(_publicKeySent){
		return true;
		}

	bool
	failed	= sendPublicKey();

	if(failed){
		// This *should* never happen.
		Oscl::Error::Info::log(
			"%s: sendPublicKey() failed.\n",
			OSCL_PRETTY_FUNCTION
			);
		stop();
		return true;
		}

	_publicKeySent	= true;

	return true;
	}

void	Reaper::extractAndSaveKeys(
			const uint8_t*	privateKey,
			const uint8_t*	publicKey
			) noexcept{

#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
#endif

	// The privateKey is 32 octets.
	// The publicKey is 64 octets.
	// The privateKey is local only, so we store it in ECDH byte-order.
	// The publicKey is exchanged, so we store it in BT byte-order.
	// The BT implementation requires the publicKey
	// to be split into X and Y arrays.

	for(unsigned i=0;i<32;++i){
		_privateKey[i]	= privateKey[i];
		_publicKeyX[i]	= publicKey[31-i];
		_publicKeyY[i]	= publicKey[32+31-i];
		}
	}

bool	Reaper::processStart(Oscl::Decoder::Api& decoder) noexcept{
#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
#endif

	// Not valid for Provisioner
	sendFailed(errorCodeUnexpectedPDU);

	return true;
	}

bool	Reaper::processPublicKey(Oscl::Decoder::Api& decoder) noexcept{

#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
#endif

	decoder.copyOut(
		_devicePublicKey,
		sizeof(_devicePublicKey)
		);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow\n",
			OSCL_PRETTY_FUNCTION
			);
		sendFailed(errorCodeInvalidFormat);
		return false;
		}

#ifdef DEBUG_TRACE
	const uint8_t*	x	= &_devicePublicKey[0];
	const uint8_t*	y	= &_devicePublicKey[32];

	Oscl::Error::Info::log(
		"%s:\n"
		" Public Key X:\n"
		" %2.2X %2.2X %2.2X %2.2X %2.2X %2.2X %2.2X %2.2X\n"
		" %2.2X %2.2X %2.2X %2.2X %2.2X %2.2X %2.2X %2.2X\n"
		" %2.2X %2.2X %2.2X %2.2X %2.2X %2.2X %2.2X %2.2X\n"
		" %2.2X %2.2X %2.2X %2.2X %2.2X %2.2X %2.2X %2.2X\n"
		" Public Key Y:\n"
		" %2.2X %2.2X %2.2X %2.2X %2.2X %2.2X %2.2X %2.2X\n"
		" %2.2X %2.2X %2.2X %2.2X %2.2X %2.2X %2.2X %2.2X\n"
		" %2.2X %2.2X %2.2X %2.2X %2.2X %2.2X %2.2X %2.2X\n"
		" %2.2X %2.2X %2.2X %2.2X %2.2X %2.2X %2.2X %2.2X\n"
		"\n",
		__PRETTY_FUNCTION__,
		x[0], x[1], x[2], x[3], x[4], x[5], x[6], x[7],
		x[8], x[9], x[10], x[11], x[12], x[13], x[14], x[15],
		x[16], x[17], x[18], x[19], x[20], x[21], x[22], x[23],
		x[24], x[25], x[26], x[27], x[28], x[29], x[30], x[31],
		y[0], y[1], y[2], y[3], y[4], y[5], y[6], y[7],
		y[8], y[9], y[10], y[11], y[12], y[13], y[14], y[15],
		y[16], y[17], y[18], y[19], y[20], y[21], y[22], y[23],
		y[24], y[25], y[26], y[27], y[28], y[29], y[30], y[31]
		);
#endif

	calculateEcdhSecret();

	if(_confirmationSent){
		return true;
		}

	bool
	failed	= sendConfirmation();

	if(failed){
		Oscl::Error::Info::log(
			"%s: sendConfirmation() failed.\n",
			OSCL_PRETTY_FUNCTION
			);
		return true;
		}

	_confirmationSent	= true;

	return true;
	}

bool	Reaper::processInputComplete(Oscl::Decoder::Api& decoder) noexcept{
#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s:\n",
		__PRETTY_FUNCTION__
		);
#endif

	// Sent from Device to Provisioner
	// When using input OOB.

	return true;
	}

bool	Reaper::processConfirmation(Oscl::Decoder::Api& decoder) noexcept{
#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
#endif

	decoder.copyOut(
		_deviceConfirmationData,
		sizeof(_deviceConfirmationData)
		);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow\n",
			OSCL_PRETTY_FUNCTION
			);
		sendFailed(errorCodeInvalidFormat);
		return false;
		}

#ifdef DEBUG_TRACE
	const uint8_t*	c	= _deviceConfirmationData;

	Oscl::Error::Info::log(
		"%s:\n"
		" Confirmation:\n"
		" %2.2X %2.2X %2.2X %2.2X %2.2X %2.2X %2.2X %2.2X\n"
		" %2.2X %2.2X %2.2X %2.2X %2.2X %2.2X %2.2X %2.2X\n"
		"\n",
		__PRETTY_FUNCTION__,
		c[0], c[1], c[2], c[3], c[4], c[5], c[6], c[7],
		c[8], c[9], c[10], c[11], c[12], c[13], c[14], c[15]
		);
#endif

	if(_randomSent){
		return true;
		}

	bool
	failed	= sendRandom();

	if(failed){
		Oscl::Error::Info::log(
			"%s: sendRandom() failed.\n",
			OSCL_PRETTY_FUNCTION
			);
		return true;
		}

	_randomSent	= true;

	return true;
	}

bool	Reaper::checkNoOobConfirmation(const uint8_t* random) noexcept{
#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
#endif

	// input:
	//	_deviceConfirmationData
	//	random

	// RandomProvisioner - generated by provisioner
	// ConfirmationProvisioner = AES-CMAC[ConfirmationKey] (RandomProvisioner || AuthValue)

	Oscl::AES128::CMAC	cmac(_confirmationKey);

	cmac.accumulate(
		random,
		16
		);

	static const uint8_t	authValue[] = {
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
		};

	cmac.accumulate(
		authValue,
		sizeof(authValue)
		);

	cmac.finalize();

	uint8_t	confirmationData[16];

	cmac.copyOut(
		confirmationData,
		sizeof(confirmationData)
		);

	bool
	failed	= memcmp(
				confirmationData,
				_deviceConfirmationData,
				sizeof(confirmationData)
				)?true:false;

	return failed;
	}

bool	Reaper::processRandom(Oscl::Decoder::Api& decoder) noexcept{
#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
#endif

	decoder.copyOut(
		_deviceRandom,
		sizeof(_deviceRandom)
		);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow\n",
			OSCL_PRETTY_FUNCTION
			);
		sendFailed(errorCodeInvalidFormat);
		return false;
		}

#ifdef DEBUG_TRACE
	const uint8_t*	r	= _deviceRandom;

	Oscl::Error::Info::log(
		"%s:\n"
		" Random:\n"
		" %2.2X %2.2X %2.2X %2.2X %2.2X %2.2X %2.2X %2.2X\n"
		" %2.2X %2.2X %2.2X %2.2X %2.2X %2.2X %2.2X %2.2X\n"
		"\n",
		__PRETTY_FUNCTION__,
		r[0], r[1], r[2], r[3], r[4], r[5], r[6], r[7],
		r[8], r[9], r[10], r[11], r[12], r[13], r[14], r[15]
		);
#endif

	if(_dataSent){
		return true;
		}

	bool
	failed	= checkNoOobConfirmation(_deviceRandom);

	if(failed){
		Oscl::Error::Info::log(
			"%s: checkNoOobConfirmation() failed.\n",
			OSCL_PRETTY_FUNCTION
			);
		sendFailed(errorCodeConfirmationFailed);

		// FIXME:
		// Presumably, we SHOULD send a link-close
		// and stop the session at this point.
		// However, we should wait for the
		// sendFailed() PDU to be sent first.
		// That probably means starting and waiting
		// for a timer.
		return true;
		}

	failed	= sendData();
	if(failed){
		Oscl::Error::Info::log(
			"%s: sendData() failed.\n",
			OSCL_PRETTY_FUNCTION
			);
		return true;
		}

	_dataSent	= true;

	return true;
	}

bool	Reaper::processData(Oscl::Decoder::Api& decoder) noexcept{
#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
#endif

	// This should never happen for a Provisioner Client.
	sendFailed(errorCodeUnexpectedPDU);

	return true;
	}

bool	Reaper::processComplete(Oscl::Decoder::Api& decoder) noexcept{
#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s:\n",
		__PRETTY_FUNCTION__
		);
#endif

	_context.registerNewNode(
		_networkKeyIndex,
		_unicastAddress
		);

	sendClose(closeReasonSuccess);

	stop();

	return true;
	}

static const char*	errorCodeString(uint8_t errorCode) noexcept{

	switch(errorCode){
		case 0x00:
			return "Prohibited";
		case 0x01:
			return "Invalid PDU";
		case 0x02:
			return "Invalid Format";
		case 0x03:
			return "Unexpected PDU";
		case 0x04:
			return "Confirmation Failed";
		case 0x05:
			return "Out of Resources";
		case 0x06:
			return "Decryption Failed";
		case 0x07:
			return "Unexpected Error";
		case 0x08:
			return "Cannot Assign Addresses";
		default:
			return "RFU";
		}
	}

bool	Reaper::processFailed(Oscl::Decoder::Api& decoder) noexcept{
#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
#endif

	uint8_t	errorCode;
	decoder.decode(errorCode);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow\n",
			OSCL_PRETTY_FUNCTION
			);
		sendFailed(errorCodeInvalidFormat);
		return false;
		}

	Oscl::Error::Info::log(
		"%s: Error Code: (0x%2.2X), \"%s\".\n",
		OSCL_PRETTY_FUNCTION,
		errorCode,
		errorCodeString(errorCode)
		);

	sendClose(closeReasonProvisioningFailed);

	stop();

	return true;
	}

bool	Reaper::receive(
				const void*		frame,
				unsigned int	length
				) noexcept{
#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	Oscl::Error::Info::hexDump(
		frame,
		length
		);
#endif

	Oscl::Endian::Decoder::Linear::Part
	beDecoder(
		frame,
		length
		);

	Oscl::Decoder::Api&	decoder	= beDecoder.be();

	uint8_t	octet;
	decoder.decode(octet);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow\n",
			OSCL_PRETTY_FUNCTION
			);
		sendFailed(errorCodeInvalidFormat);
		return false;
		}

	uint8_t	type	= octet & 0x3F;

	switch(type){
		case 0x00:
			// Invite
			return processInvite(decoder);
		case 0x01:
			// Capabilities
			return processCapabilities(decoder);
		case 0x02:
			// Start
			return processStart(decoder);
		case 0x03:
			// Public Key
			return processPublicKey(decoder);
		case 0x04:
			// Input Complete
			return processInputComplete(decoder);
		case 0x05:
			// Confirmation
			return processConfirmation(decoder);
		case 0x06:
			// Random
			return processRandom(decoder);
		case 0x07:
			// Data
			return processData(decoder);
		case 0x08:
			// Complete
			return processComplete(decoder);
		case 0x09:
			// Failed
			return processFailed(decoder);
		default:
			sendFailed(errorCodeInvalidPDU);
			return false;
		}

	return true;
	}

bool	Reaper::sendClose(uint8_t reason) noexcept{
	BufferMem*
	mem	= allocBuffer();

	if(!mem){
		Oscl::Error::Info::log(
			"%s: out of buffer memory.\n",
			OSCL_PRETTY_FUNCTION
			);
		return true;
		}

	Oscl::Encoder::BE::Base
	beEncoder(
		mem,
		sizeof(BufferMem)
		);

	Oscl::Encoder::Api&	encoder	= beEncoder;

	static const uint8_t	type	=
									(0x03<<0)	// Provisioning Bearer Control
								|	(0x02<<2)	// Link Close
								;

	encoder.encode(type);
	encoder.encode(reason);

	if(encoder.overflow()){
		freeBuffer(mem);
		Oscl::Error::Info::log(
			"%s: overflow.\n",
			OSCL_PRETTY_FUNCTION
			);
		return true;
		}

	Oscl::Handle<Oscl::Pdu::Fixed>
	handle	= _freeStoreApi.allocFixed(
				_bufferFreeStoreMgrComposer,
				mem,
				encoder.length()
				);

	if(!handle){
		freeBuffer(mem);
		Oscl::Error::Info::log(
			"%s: out of fixed memory.\n",
			OSCL_PRETTY_FUNCTION
			);
		return true;
		}

	_rawLinkApi.transfer(handle);

	return false;
	}

bool	Reaper::sendInvite() noexcept{
#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
#endif

	constexpr static uint8_t	type				= 0x00;	// Invite
	constexpr static uint8_t	attentionDuration	= 0x05;	// 5s

	_invitePdu[0]	= attentionDuration;

	static const uint8_t	pdu[]	= {
		type,
		attentionDuration
		};

	Oscl::Handle<Oscl::Pdu::Fixed>
	handle	= _freeStoreApi.allocConstFixed(
				pdu,
				sizeof(pdu)
				);

	if(!handle){
		return true;
		}

	_linkApi.transfer(handle);

	return false;
	}

bool	Reaper::sendStart(
			uint8_t	algorithm,
			uint8_t	publicKeyType,
			uint8_t	authenticationMethod,
			uint8_t	authenticationAction,
			uint8_t	authenticationSize
			) noexcept{
#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
#endif

	BufferMem*
	mem	= allocBuffer();

	if(!mem){
		Oscl::Error::Info::log(
			"%s: out of buffer memory.\n",
			OSCL_PRETTY_FUNCTION
			);
		return true;
		}

	Oscl::Encoder::BE::Base
	beEncoder(
		mem,
		sizeof(BufferMem)
		);

	Oscl::Encoder::Api&	encoder	= beEncoder;

	static const uint8_t	type		= 0x02;	// Provisioning Start

	encoder.encode(type);
	encoder.encode(algorithm);
	encoder.encode(publicKeyType);
	encoder.encode(authenticationMethod);
	encoder.encode(authenticationAction);
	encoder.encode(authenticationSize);
	

	if(encoder.overflow()){
		freeBuffer(mem);
		Oscl::Error::Info::log(
			"%s: overflow.\n",
			OSCL_PRETTY_FUNCTION
			);
		return true;
		}

	const uint8_t*	p	= (const uint8_t*)mem;

	memcpy(
		_startPdu,
		&p[1],
		sizeof(_startPdu)
		);

	Oscl::Handle<Oscl::Pdu::Fixed>
	handle	= _freeStoreApi.allocFixed(
				_bufferFreeStoreMgrComposer,
				mem,
				encoder.length()
				);

	if(!handle){
		freeBuffer(mem);
		Oscl::Error::Info::log(
			"%s: out of fixed memory.\n",
			OSCL_PRETTY_FUNCTION
			);
		return true;
		}

	_linkApi.transfer(handle);

	return false;
	}

bool	Reaper::sendPublicKey() noexcept{
#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
#endif

	BufferMem*
	mem	= allocBuffer();

	if(!mem){
		Oscl::Error::Info::log(
			"%s: out of buffer memory.\n",
			OSCL_PRETTY_FUNCTION
			);
		return true;
		}

	Oscl::Encoder::BE::Base
	beEncoder(
		mem,
		sizeof(BufferMem)
		);

	Oscl::Encoder::Api&	encoder	= beEncoder;

	static const uint8_t	type	= 0x03;	// Public Key

	encoder.encode(type);
	encoder.copyIn(_publicKeyX,sizeof(_publicKeyX));
	encoder.copyIn(_publicKeyY,sizeof(_publicKeyY));

	if(encoder.overflow()){
		freeBuffer(mem);
		Oscl::Error::Info::log(
			"%s: overflow.\n",
			OSCL_PRETTY_FUNCTION
			);
		return true;
		}

	Oscl::Handle<Oscl::Pdu::Fixed>
	handle	= _freeStoreApi.allocFixed(
				_bufferFreeStoreMgrComposer,
				mem,
				encoder.length()
				);

	if(!handle){
		freeBuffer(mem);
		Oscl::Error::Info::log(
			"%s: out of fixed memory.\n",
			OSCL_PRETTY_FUNCTION
			);
		return true;
		}

	_linkApi.transfer(handle);

	return false;
	}

bool	Reaper::createProvisionerPublicAndPrivateKeys() noexcept{

	uint8_t	random[32];

	Oscl::Entropy::Random::fetch(
		random,
		sizeof(random)
		);

	uint8_t	privateKey[32];
	uint8_t	publicKey[64];

	bool
	success	= ecc_make_key_using_external_random(
				random,
				privateKey,
				publicKey
				);

	if(!success){
		Oscl::Error::Info::log(
			"%s: ecc_make_key_using_external_random() failed!\n",
			OSCL_PRETTY_FUNCTION
			);
		return true;
		}

	extractAndSaveKeys(
		privateKey,
		publicKey
		);

	return false;
	}

void	Reaper::calculateEcdhSecret() noexcept{
#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
#endif

	// Convert BT byte-order to ECDH byte-order
	uint8_t	devicePublicKey[64];
	
	for(unsigned i=0;i<32;++i){
		// X
		devicePublicKey[i]		= _devicePublicKey[31-i];
		devicePublicKey[32+i]	= _devicePublicKey[63-i];
		}

	uint8_t	ecdhSecret[32];

	bool
	success	= ecdh_shared_secret_using_fixed_z(
				devicePublicKey,
				_privateKey,
				ecdhSecret
				);

	if(!success){
		// I suspect that this can't happen, since
		// there is no I/O involved.
		Oscl::Error::Info::log(
			"%s: ecdh_shared_secret_using_fixed_z() failed!\n",
			OSCL_PRETTY_FUNCTION
			);
		return;
		}

	// ECDH secret is stored in BT byte-order
	for(unsigned i=0;i<32;++i){
		_ecdhSecret[i]	= ecdhSecret[31-i];
		}

#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"_ecdhSecret:\n"
		);

	Oscl::Error::Info::hexDump(
		_ecdhSecret,
		sizeof(_ecdhSecret)
		);
#endif

	}

void	Reaper::calculateSessionNonce() noexcept{
#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
#endif

	// SessionNonce = k1(ECDHSecret, ProvisioningSalt, “prsn”)

	static const uint8_t	prsn[]	= {'p','r','s','n'};

	Oscl::BT::Crypto::Keys::k1(
		_ecdhSecret,
		sizeof(_ecdhSecret),
		_provisioningSalt,
		prsn,
		sizeof(prsn),
		_sessionNonce
		);
	}

void	Reaper::calculateSessionKey() noexcept{
#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
#endif

	uint8_t	input[
					sizeof(_confirmationSalt)
				+	sizeof(_provisionerRandom)
				+	sizeof(_deviceRandom)
				];

	memcpy(
		&input[0],
		_confirmationSalt,
		sizeof(_confirmationSalt)
		);

	memcpy(
		&input[sizeof(_confirmationSalt)],
		_provisionerRandom,
		sizeof(_provisionerRandom)
		);

	memcpy(
		&input[
				sizeof(_confirmationSalt)
			+	sizeof(_provisionerRandom)
			],
		_deviceRandom,
		sizeof(_deviceRandom)
		);

	// ProvisioningSalt = s1(ConfirmationSalt || RandomProvisioner || RandomDevice)
	Oscl::BT::Crypto::Salt::s1(
		input,
		sizeof(input),
		_provisioningSalt,
		sizeof(_provisioningSalt)
		);

	// SessionKey = k1(ECDHSecret, ProvisioningSalt, “prsk”)

	static const uint8_t	prsk[]	= {'p','r','s','k'};

	Oscl::BT::Crypto::Keys::k1(
		_ecdhSecret,
		sizeof(_ecdhSecret),
		_provisioningSalt,
		prsk,
		sizeof(prsk),
		_sessionKey
		);
	}

void	Reaper::calculateConfirmationKey() noexcept{
#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
#endif

	// ConfirmationInputs = ProvisioningInvitePDUValue || ProvisioningCapabilitiesPDUValue || ProvisioningStartPDUValue || PublicKeyProvisioner || PublicKeyDevice
	// ConfirmationSalt = s1(ConfirmationInputs)
	// ConfirmationKey = k1(ECDHSecret, ConfirmationSalt, “prck”)
	uint8_t	confirmationInputs[
					sizeof(_invitePdu)
				+	sizeof(_capabilitiesPdu)
				+	sizeof(_startPdu)
				+	sizeof(_devicePublicKey)
				+	sizeof(_publicKeyX)
				+	sizeof(_publicKeyY)
				];

	// concate
	// ConfirmationInputs = ProvisioningInvitePDUValue || ProvisioningCapabilitiesPDUValue || ProvisioningStartPDUValue || PublicKeyProvisioner || PublicKeyDevice
	memcpy(
		&confirmationInputs[0],
		_invitePdu,
		sizeof(_invitePdu)
		);

	memcpy(
		&confirmationInputs[sizeof(_invitePdu)],
		_capabilitiesPdu,
		sizeof(_capabilitiesPdu)
		);

	memcpy(
		&confirmationInputs[
				sizeof(_invitePdu)
			+	sizeof(_capabilitiesPdu)
			],
		_startPdu,
		sizeof(_startPdu)
		);

	memcpy(
		&confirmationInputs[
				sizeof(_invitePdu)
			+	sizeof(_capabilitiesPdu)
			+	sizeof(_startPdu)
			],
		_publicKeyX,
		sizeof(_publicKeyX)
		);

	memcpy(
		&confirmationInputs[
				sizeof(_invitePdu)
			+	sizeof(_capabilitiesPdu)
			+	sizeof(_startPdu)
			+	sizeof(_publicKeyX)
			],
		_publicKeyY,
		sizeof(_publicKeyY)
		);

	memcpy(
		&confirmationInputs[
				sizeof(_invitePdu)
			+	sizeof(_capabilitiesPdu)
			+	sizeof(_startPdu)
			+	sizeof(_publicKeyX)
			+	sizeof(_publicKeyY)
			],
		_devicePublicKey,
		sizeof(_devicePublicKey)
		);

	Oscl::BT::Crypto::Salt::s1(
		confirmationInputs,
		sizeof(confirmationInputs),
		_confirmationSalt,
		sizeof(_confirmationSalt)
		);

	static const uint8_t	prck[]	= {'p','r','c','k'};

	Oscl::BT::Crypto::Keys::k1(
		_ecdhSecret,
		sizeof(_ecdhSecret),
		_confirmationSalt,
		prck,
		sizeof(prck),
		_confirmationKey
		);

	}

void	Reaper::calculateConfirmationData() noexcept{
#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
#endif

	// RandomDevice - generated by device and later sent in Provisioning Random PDU
	// ConfirmationDevice = AES-CMAC[ConfirmationKey] (RandomDevice || AuthValue)

	calculateConfirmationKey();

	Oscl::Entropy::Random::fetch(
		_provisionerRandom,
		sizeof(_provisionerRandom)
		);

	Oscl::AES128::CMAC	cmac(_confirmationKey);

	cmac.accumulate(
		_provisionerRandom,
		sizeof(_provisionerRandom)
		);

	static const uint8_t	authValue[] = {
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
		};

	cmac.accumulate(
		authValue,
		sizeof(authValue)
		);

	cmac.finalize();

	cmac.copyOut(
		_confirmationData,
		sizeof(_confirmationData)
		);
	}

bool	Reaper::sendConfirmation() noexcept{
#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
#endif

	// AuthValue (No OOB) = 0x00000000000000000000000000000000, {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}
	// RandomDevice - generated by device and later sent in
	// ConfirmationInputs = ProvisioningInvitePDUValue || ProvisioningCapabilitiesPDUValue || ProvisioningStartPDUValue || PublicKeyProvisioner || PublicKeyDevice
	// ConfirmationSalt = s1(ConfirmationInputs)
	// ConfirmationKey = k1(ECDHSecret, ConfirmationSalt, “prck”)
	// ConfirmationDevice = AES-CMAC[ConfirmationKey] (RandomDevice || AuthValue)

	calculateConfirmationData();

	BufferMem*
	mem	= allocBuffer();

	if(!mem){
		Oscl::Error::Info::log(
			"%s: out of buffer memory.\n",
			OSCL_PRETTY_FUNCTION
			);
		return true;
		}

	Oscl::Encoder::BE::Base
	beEncoder(
		mem,
		sizeof(BufferMem)
		);

	Oscl::Encoder::Api&	encoder	= beEncoder;

	static const uint8_t	type	= 0x05;	// Confirmation

	encoder.encode(type);
	encoder.copyIn(_confirmationData,sizeof(_confirmationData));

	if(encoder.overflow()){
		Oscl::Error::Info::log(
			"%s: overflow.\n",
			OSCL_PRETTY_FUNCTION
			);
		return true;
		}

	Oscl::Handle<Oscl::Pdu::Fixed>
	handle	= _freeStoreApi.allocFixed(
				_bufferFreeStoreMgrComposer,
				mem,
				encoder.length()
				);

	if(!handle){
		freeBuffer(mem);
		Oscl::Error::Info::log(
			"%s: out of fixed memory.\n",
			OSCL_PRETTY_FUNCTION
			);
		return true;
		}

	_linkApi.transfer(handle);

	return false;
	}

bool	Reaper::sendRandom() noexcept{
#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
#endif

	// _provisionerRandom

	BufferMem*
	mem	= allocBuffer();

	if(!mem){
		Oscl::Error::Info::log(
			"%s: out of buffer memory.\n",
			OSCL_PRETTY_FUNCTION
			);
		return true;
		}

	Oscl::Encoder::BE::Base
	beEncoder(
		mem,
		sizeof(BufferMem)
		);

	Oscl::Encoder::Api&	encoder	= beEncoder;

	static const uint8_t	type	= 0x06;	// Random

	encoder.encode(type);
	encoder.copyIn(_provisionerRandom,sizeof(_provisionerRandom));

	if(encoder.overflow()){
		freeBuffer(mem);
		Oscl::Error::Info::log(
			"%s: overflow.\n",
			OSCL_PRETTY_FUNCTION
			);
		return true;
		}

	Oscl::Handle<Oscl::Pdu::Fixed>
	handle	= _freeStoreApi.allocFixed(
				_bufferFreeStoreMgrComposer,
				mem,
				encoder.length()
				);

	if(!handle){
		freeBuffer(mem);
		Oscl::Error::Info::log(
			"%s: out of fixed memory.\n",
			OSCL_PRETTY_FUNCTION
			);
		return true;
		}

	_linkApi.transfer(handle);

	return false;
	}

bool	Reaper::sendData() noexcept{
#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
#endif

	if(!_nElements){
		_nElements	= 1;
		}

	_unicastAddress	= _context.allocateAddresses(_nElements);

	if(!_unicastAddress){
		// DOH! We've got no space for you!
		Oscl::Error::Info::log(
			"%s: No available unicast addresses.\n",
			OSCL_PRETTY_FUNCTION
			);
		sendFailed(errorCodeOutOfResources);
		sendClose(closeReasonProvisioningFailed);
		stop();
		return true;
		}

	BufferMem*
	mem	= allocBuffer();

	if(!mem){
		Oscl::Error::Info::log(
			"%s: out of buffer memory.\n",
			OSCL_PRETTY_FUNCTION
			);
		// FIXME:
		// Should we close up shop here or
		// perhaps wait for the next try?
		// For now, we hope that we receive
		// another Provisioning Random so
		// that we try again.
		// HOWEVER, we should probably start
		// a timer.
		return true;
		}

	Oscl::Encoder::BE::Base
	beEncoder(
		mem,
		sizeof(BufferMem)
		);

	Oscl::Encoder::Api&	encoder	= beEncoder;

	static const uint8_t	type	= 0x07;	// Provisioning Data

	const void*	netKey			= _context.getNetKey();
	_networkKeyIndex			= _context.getKeyIndex();
	uint8_t     flags			= _context.getFlags();
	uint32_t	ivIndex			= _context.getTxIvIndex();

	encoder.encode(type);
	encoder.copyIn(
		netKey,
		16
		);
	encoder.encode(_networkKeyIndex	);
	encoder.encode(flags);
	encoder.encode(ivIndex);
	encoder.encode(_unicastAddress);

	calculateSessionKey();
	calculateSessionNonce();

	uint8_t*	p	= (uint8_t*)mem;

#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s: Plaintext Data\n"
		" type: 0x%2.2X\n"
		" netKey: %2.2X %2.2X %2.2X %2.2X %2.2X %2.2X %2.2X %2.2X %2.2X %2.2X %2.2X %2.2X %2.2X %2.2X %2.2X %2.2X\n"
		" _networkKeyIndex	: 0x%4.4X\n"
		" flags: 0x%2.2X\n"
		" ivIndex: 0x%8.8X\n"
		" _unicastAddress: 0x%4.4X\n"
		"",
		__PRETTY_FUNCTION__,
		p[0],
		p[1], p[2], p[3], p[4], p[5], p[6], p[7], p[8],
		p[9], p[10], p[11], p[12], p[13], p[14], p[15], p[16],
		_networkKeyIndex,
		flags,
		ivIndex,
		_unicastAddress
		);
#endif

	unsigned
	length	= Oscl::AES128::Ccm::encrypt(
				_sessionKey,
				&_sessionNonce[3],
				0,	// adata
				0,	// adata length
				&p[1],	// plain text
				encoder.length()-1,	// plain text length
				8,	// size-of-MIC
				&p[1]	// output buffer
				);

	if(!length){
		// This should never happen.
		freeBuffer(mem);
		Oscl::Error::Info::log(
			"%s: Oscl::AES128::decrypt() failed\n",
			OSCL_PRETTY_FUNCTION
			);
		sendClose(closeReasonProvisioningFailed);
		stop();
		return true;
		}

	Oscl::Handle<Oscl::Pdu::Fixed>
	handle	= _freeStoreApi.allocFixed(
				_bufferFreeStoreMgrComposer,
				mem,
				length + 1
				);

	if(!handle){
		freeBuffer(mem);
		Oscl::Error::Info::log(
			"%s: out of fixed memory.\n",
			OSCL_PRETTY_FUNCTION
			);
		sendFailed(errorCodeOutOfResources);
		sendClose(closeReasonProvisioningFailed);
		stop();
		return true;
		}

	_linkApi.transfer(handle);

	return false;
	}

bool	Reaper::sendFailed(uint8_t errorCode) noexcept{
#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
#endif

	BufferMem*
	mem	= allocBuffer();

	if(!mem){
		Oscl::Error::Info::log(
			"%s: out of buffer memory.\n",
			OSCL_PRETTY_FUNCTION
			);
		return true;
		}

	Oscl::Encoder::BE::Base
	beEncoder(
		mem,
		sizeof(BufferMem)
		);

	Oscl::Encoder::Api&	encoder	= beEncoder;

	static const uint8_t	type	= 0x09;

	encoder.encode(type);
	encoder.encode(errorCode);

	if(encoder.overflow()){
		freeBuffer(mem);
		Oscl::Error::Info::log(
			"%s: overflow.\n",
			OSCL_PRETTY_FUNCTION
			);
		return true;
		}

	Oscl::Handle<Oscl::Pdu::Fixed>
	handle	= _freeStoreApi.allocFixed(
				_bufferFreeStoreMgrComposer,
				mem,
				encoder.length()
				);

	if(!handle){
		freeBuffer(mem);
		Oscl::Error::Info::log(
			"%s: out of fixed memory.\n",
			OSCL_PRETTY_FUNCTION
			);
		return true;
		}

	_linkApi.transfer(handle);

	return false;
	}

