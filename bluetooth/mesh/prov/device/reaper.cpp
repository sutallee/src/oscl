/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <new>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <string.h>

#include "reaper.h"
#include "oscl/error/fatal.h"
#include "oscl/error/info.h"
#include "oscl/posix/errno/errstring.h"
#include "oscl/encoder/be/base.h"
#include "oscl/endian/decoder/linear/part.h"
#include "oscl/bluetooth/debug/print/parse.h"
#include "oscl/bluetooth/strings/module.h"

#include "oscl/freestore/nullmgr.h"

#include "oscl/entropy/rand.h"
#include "oscl/crypto/nist/fips/p256/ecc.h"
#include "oscl/bluetooth/crypto/keys.h"
#include "oscl/bluetooth/crypto/salt.h"
#include "oscl/aes128/cmac.h"
#include "oscl/aes128/ccm.h"
#include "oscl/bluetooth/mesh/prov/capabilities.h"
#include "oscl/bluetooth/mesh/prov/errors.h"

//#define DEBUG_TRACE
//#define DEBUG_TRACE_TMI

using namespace Oscl::BT::Mesh::Prov::Device;

static Oscl::FreeStore::NullMgr	nullFreestoreMgr;

Reaper::Reaper(
	Device::ContextApi&			context,
	Oscl::Pdu::Memory::Api&		freeStoreApi,
	Oscl::Pdu::FWD::Api&		linkApi,
	bool						oobAvailable,
	uint8_t						outputOobSizeInCharacters
	) noexcept:
		_context(context),
		_freeStoreApi(freeStoreApi),
		_linkApi(linkApi),
		_oobAvailable(oobAvailable),
		_outputOobSizeInCharacters(outputOobSizeInCharacters),
		_rxComposer(
			*this,
			&Reaper::receive
			),
		_bufferFreeStoreApiComposer(
			*this,
			&Reaper::bufferFree
			),
		_bufferFreeStoreMgrComposer(
			_bufferFreeStoreApiComposer
			),
		_stopping(false),
		_started(false),
		_capabilitiesSent(false),
		_publicKeySent(false),
		_confirmationSent(false),
		_randomSent(false),
		_completeSent(false)
		{
	}

Oscl::Frame::FWD::Api&	Reaper::getRxApi() noexcept{
	return _rxComposer;
	}

void	Reaper::start() noexcept{
	for(unsigned i=0;i<nProvBuffers;++i){
		_bufferPool.put(&_buffers[i]);
		}
	}

void	Reaper::stop() noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif


	if(_stopping){
		return;
		}

	_stopping	= true;

	stopNext();
	}

void	Reaper::stopNext() noexcept{
	if(!_stopping){
		return;
		}

	if(!allMemoryIsFree()){
		return;
		}

	_stopping	= false;

	_context.stopped(*this);
	}

bool	Reaper::allMemoryIsFree() noexcept{
	return	(
				allBuffersFree() 
			);
	}

bool	Reaper::allBuffersFree() noexcept{
	unsigned	n	= 0;
	BufferMem*	mem;
	for(
		mem	= _bufferPool.first();
		mem;
		mem	= _bufferPool.next(mem)
		){
		++n;
		}

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s: (n(%u) < nProvBuffers(%u)): %s\n",
		OSCL_PRETTY_FUNCTION,
		n,
		nProvBuffers,
		(n < nProvBuffers)?"false":"true"
		);
	#endif

	return (n < nProvBuffers)?false:true;
	}

void	Reaper::bufferFree(void* buffer) noexcept{
	// This would do ITC in an MT application.
	freeBuffer((BufferMem*)buffer);
	}

void	Reaper::freeBuffer(BufferMem* buffer) noexcept{
	_bufferPool.put(buffer);
	stopNext();
	}

Oscl::BT::Mesh::Prov::Device::Reaper::BufferMem*	Reaper::allocBuffer() noexcept{
	return _bufferPool.get();
	}

void	Reaper::generateRandomNumericAuthValue(uint8_t nDigits) noexcept{

	// 0 < nDigits < 9
	if(nDigits > 8){
		nDigits	= 8;
		}
	else if(nDigits < 1){
		nDigits	= 1;
		}

	uint32_t	value;

	Oscl::Entropy::Random::fetch(
		&value,
		sizeof(value)
		);

	// What I want is to calculate the 10^nDigits
	// Then I'll use that value as a modulus on
	// the random number.
	// modulus = 10^nDigits
	// value	= randomValue%modulus;

	// Generate the modulus by base 10 exponentiation
	// This loop calculates 10^nDigits.
	unsigned	modulus = 1;
	for(unsigned i=0;i<nDigits;++i){
		modulus	*= 10;
		}

	#ifdef DEBUG_TRACE_TMI
	Oscl::Error::Info::log(
		"%s:\n"
		" nDigits: %u\n"
		" value: 0x%8.8X, %10.10u\n"
		" modulus: %10.10u\n"
		"",
		OSCL_PRETTY_FUNCTION,
		nDigits,
		value,
		value,
		modulus
		);
	#endif

	value	%= modulus;

	if(!value){
		// Zero is problematic for blink,
		// beep and vibrate, since there
		// is no observable phenomena
		// for the user.
		++value;
		}

	_authNumber	= value;

	#ifdef DEBUG_TRACE_TMI
	Oscl::Error::Info::log(
		" value %% modulus: 0x%8.8x, %10.10u\n"
		" _authValue:\n"
		"",
		value,
		value
		);
	#endif

	// Write big-endian representation.
	for(unsigned i=0;(i<sizeof(value)) && (i<sizeof(_authValue));++i){
		_authValue[15-i]	= (uint8_t)value;
		value				>>= 8;
		}

	#ifdef DEBUG_TRACE_TMI
	Oscl::Error::Info::hexDump(
		_authValue,
		sizeof(_authValue)
		);
	#endif
	}

unsigned char	Reaper::generateRandomAlphaNumeric() noexcept{
	unsigned char	value;

	Oscl::Entropy::Random::fetch(
		&value,
		sizeof(value)
		);

	// Arbitrarily, We look at the MSB and 
	// choose to produce a base 10 digit
	// or an upper-case alphabetic character.
	if(value & (1<<7)){
		// Digit: 0-9
		value	%= 10;
		value	+=	'0';
		}
	else {
		// Alphabetic: A-Z
		value	%= 26;
		value	+=	'A';
		}

	return value;
	}

void	Reaper::generateRandomAlphaNumericAuthValue(uint8_t nCharacters) noexcept{

	if(nCharacters > (sizeof(_authValue)-1)){
		// This *should* never happen.
		nCharacters	= sizeof(_authValue)-1;
		}

	for(unsigned i=0;i<nCharacters;++i){
		_authValue[i]	= generateRandomAlphaNumeric();
		}

	for(unsigned i=nCharacters;i<sizeof(_authValue);++i){
		_authValue[i]	= 0;
		}
	}

bool	Reaper::processInvite(Oscl::Decoder::Api& decoder) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif

	if(_capabilitiesSent){
		return true;
		}

	uint8_t	attentionDuration;
	decoder.decode(attentionDuration);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow\n",
			OSCL_PRETTY_FUNCTION
			);
		sendFailed(Oscl::BT::Mesh::Prov::Errors::invalidFormat);
		return false;
		}

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s: Attention Duration: %s, %u s.\n",
		OSCL_PRETTY_FUNCTION,
		attentionDuration?"ON":"OFF",
		attentionDuration
		);
	#endif

	_invitePdu[0]	= attentionDuration;

	_context.setAttentionTimer(attentionDuration);

	bool
	failed	= sendCapabilities();

	if(failed){
		return true;
		}

	_capabilitiesSent	= true;

	return true;
	}

bool	Reaper::processCapabilities(Oscl::Decoder::Api& decoder) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif

	uint8_t		nElements;
	uint16_t	algorithms;
	uint8_t		publicKeyType;
	uint8_t		staticOobType;
	uint8_t		staticOobSize;

	decoder.decode(nElements);
	decoder.decode(algorithms);
	decoder.decode(publicKeyType);
	decoder.decode(staticOobType);
	decoder.decode(staticOobSize);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow\n",
			OSCL_PRETTY_FUNCTION
			);
		sendFailed(Oscl::BT::Mesh::Prov::Errors::invalidFormat);
		return false;
		}

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s:"
		" Number of Elements: %u,"
		" Algorithms: 0x%4.4X,"
		" Public Key Type: 0x%2.2X,"
		" Static OOB Type: 0x%2.2X,"
		" Static OOB Size: 0x%2.2X"
		"\n",
		OSCL_PRETTY_FUNCTION,
		nElements,
		algorithms,
		publicKeyType,
		staticOobType,
		staticOobSize
		);
	#endif

	// Invalid for a device
	sendFailed(Oscl::BT::Mesh::Prov::Errors::unexpectedPDU);

	return true;
	}

#ifdef DEBUG_TRACE
static const char*	algorithmType(uint8_t type) noexcept{
	switch(type){
		case 0x00:
			return "IPS P-256 Elliptic Curve";
		default:
			return "Unknown";
		}
	}

static const char*	publicKeyType(uint8_t type) noexcept{
	switch(type){
		case 0x00:
			return "No OOB Public Key is used.";
		case 0x01:
			return "OOB Public Key is used.";
		default:
			return "Unknown";
		}
	}

static const char*	authenticationType(uint8_t type) noexcept{
	switch(type){
		case 0x00:
			return "No OOB authentication is used.";
		case 0x01:
			return "Static OOB authentication is used.";
		case 0x02:
			return "Output OOB authentication is used.";
		case 0x03:
			return "Input OOB authentication is used.";
		default:
			return "Unknown";
		}
	}

static const char*	outputOobActionType(uint8_t type) noexcept{
	switch(type){
		case 0x00:
			return "Blink";
		case 0x01:
			return "Beep";
		case 0x02:
			return "Vibrate";
		case 0x03:
			return "Output Numeric";
		case 0x04:
			return "Output Alphanumeric";
		default:
			return "Unknown";
		}
	}

static const char*	inputOobActionType(uint8_t type) noexcept{
	switch(type){
		case 0x00:
			return "Push";
		case 0x01:
			return "Twist";
		case 0x02:
			return "Input Numeric";
		case 0x03:
			return "Input Alphanumeric";
		default:
			return "Unknown";
		}
	}

static const char*	actionType(uint8_t methodType, uint8_t type) noexcept{
	switch(methodType){
		case 0x00:
		case 0x01:
			return "None";
		case 0x02:
			return outputOobActionType(type);
		case 0x03:
			return inputOobActionType(type);
		default:
			return "Unknown";
		}
	}
#endif

void	Reaper::displayPublicKey() noexcept{
	Oscl::Error::Info::log(
		"Public Key:\n"
		);

	Oscl::Error::Info::log(
		"\t"
		);
	for(unsigned i=0;i<sizeof(_publicKeyX);++i){
		Oscl::Error::Info::log(
			"%2.2X",
			_publicKeyX[i]
			);
		}
	for(unsigned i=0;i<sizeof(_publicKeyY);++i){
		Oscl::Error::Info::log(
			"%2.2X",
			_publicKeyY[i]
			);
		}
	Oscl::Error::Info::log(
		"\n"
		);
	}

void	Reaper::blinkAuthValue() noexcept{

	generateRandomNumericAuthValue(_authenticationSize);

	_context.oobOutputBlink(_authNumber);
	}

void	Reaper::beepAuthValue() noexcept{

	generateRandomNumericAuthValue(_authenticationSize);

	_context.oobOutputBeep(_authNumber);
	}

void	Reaper::vibrateAuthValue() noexcept{

	generateRandomNumericAuthValue(_authenticationSize);

	_context.oobOutputVibrate(_authNumber);
	}

void	Reaper::outputNumericAuthValue() noexcept{

	generateRandomNumericAuthValue(_authenticationSize);

	_context.oobOutputNumeric(_authNumber);
	}

void	Reaper::outputAlphaNumericAuthValue() noexcept{

	generateRandomAlphaNumericAuthValue(_authenticationSize);

	_context.oobOutputAlphaNumeric((const char*)_authValue);
	}

void	Reaper::extractAndSaveKeys(
			const uint8_t*	privateKey,
			const uint8_t*	publicKey
			) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif

	// The privateKey is 32 octets.
	// The publicKey is 64 octets.
	// The privateKey is local only, so we store it in ECDH byte-order.
	// The publicKey is exchanged, so we store it in BT byte-order.
	// The BT implementation requires the publicKey
	// to be split into X and Y arrays.

	for(unsigned i=0;i<32;++i){
		_privateKey[i]	= privateKey[i];
		_publicKeyX[i]	= publicKey[31-i];
		_publicKeyY[i]	= publicKey[32+31-i];
		}
	}

bool	Reaper::processStart(Oscl::Decoder::Api& decoder) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif

	if(_started){
		return true;
		}

	Oscl::Decoder::State	state;

	decoder.push(
		state,
		decoder.remaining()
		);

	decoder.copyOut(
		_startPdu,
		sizeof(_startPdu)
		);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow\n",
			OSCL_PRETTY_FUNCTION
			);
		sendFailed(Oscl::BT::Mesh::Prov::Errors::invalidFormat);
		return false;
		}

	decoder.pop();

	decoder.decode(_algorithm);
	decoder.decode(_pubKeyType);
	decoder.decode(_authenticationMethod);
	decoder.decode(_authenticationAction);
	decoder.decode(_authenticationSize);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow\n",
			OSCL_PRETTY_FUNCTION
			);
		sendFailed(Oscl::BT::Mesh::Prov::Errors::invalidFormat);
		return false;
		}

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s:\n"
		" Algorithm: 0x%2.2X,\"%s\"\n"
		" Public Key: 0x%2.2X,\"%s\"\n"
		" Authentication Method: 0x%2.2X,\"%s\"\n"
		" Authentication Action: 0x%2.2X,\"%s\"\n"
		" Authentication Size: %u\n"
		"\n",
		OSCL_PRETTY_FUNCTION,
		_algorithm,
		algorithmType(_algorithm),
		_pubKeyType,
		publicKeyType(_pubKeyType),
		_authenticationMethod,
		authenticationType(_authenticationMethod),
		_authenticationAction,
		actionType(_authenticationMethod,_authenticationAction),
		_authenticationSize
		);
	#endif

	_started	= true;

	bool
	failed	= createDevicePublicAndPrivateKeys();

	if(failed){
		Oscl::Error::Info::log(
			"%s: ecc_make_key_using_external_random() failed!\n",
			OSCL_PRETTY_FUNCTION
			);
		return true;
		}

	// Initialize the AuthValue to that used in a "No OOB" situation.
	for(unsigned i=0;i<sizeof(_authValue);++i){
		_authValue[i]	= 0;
		}

	if(_pubKeyType == 0x00){
		// No OOB Public Key is used.
		// Start without OOB Public Key
		// The device shall send its public key if
		// the key is not delivered OOB.
		// 1. Provisioner sends its Public Key
		// 2. Device sends its Public Key

		// Our public key will be sent after
		// we receive the provisioner's public key.
		}
	else if(_pubKeyType == 0x01){
		// OOB Public Key is used.
		// 1. Device displays public key
		// 2. Provisioner reads public key
		// 3. Provisioner send public key
		displayPublicKey();
		}

	if(_authenticationMethod == 0x02){
		// Output OOB authentication is used
		switch(_authenticationAction){
			case 0x00:
				// Blink
				generateRandomNumericAuthValue(_authenticationSize);
				blinkAuthValue();
				break;
			case 0x01:
				// Beep
				generateRandomNumericAuthValue(_authenticationSize);
				beepAuthValue();
				break;
			case 0x02:
				// Vibrate
				generateRandomNumericAuthValue(_authenticationSize);
				vibrateAuthValue();
				break;
			case 0x03:
				// Output Numeric
				generateRandomNumericAuthValue(_authenticationSize);
				outputNumericAuthValue();
				break;
			case 0x04:
				// Output Alphanumeric
				generateRandomAlphaNumericAuthValue(_authenticationSize);
				outputAlphaNumericAuthValue();
				break;
			default:
				// Output Numeric
				generateRandomNumericAuthValue(_authenticationSize);
				outputNumericAuthValue();
				break;
			}
		}

	return true;
	}

bool	Reaper::processPublicKey(Oscl::Decoder::Api& decoder) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif

	if(_publicKeySent){
		return true;
		}

	decoder.copyOut(
		_provisionerPublicKey,
		sizeof(_provisionerPublicKey)
		);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow\n",
			OSCL_PRETTY_FUNCTION
			);
		sendFailed(Oscl::BT::Mesh::Prov::Errors::invalidFormat);
		return false;
		}

	#ifdef DEBUG_TRACE_TMI
	const uint8_t*	x	= &_provisionerPublicKey[0];
	const uint8_t*	y	= &_provisionerPublicKey[32];

	Oscl::Error::Info::log(
		"%s:\n"
		" Public Key X:\n"
		" %2.2X %2.2X %2.2X %2.2X %2.2X %2.2X %2.2X %2.2X\n"
		" %2.2X %2.2X %2.2X %2.2X %2.2X %2.2X %2.2X %2.2X\n"
		" %2.2X %2.2X %2.2X %2.2X %2.2X %2.2X %2.2X %2.2X\n"
		" %2.2X %2.2X %2.2X %2.2X %2.2X %2.2X %2.2X %2.2X\n"
		" Public Key Y:\n"
		" %2.2X %2.2X %2.2X %2.2X %2.2X %2.2X %2.2X %2.2X\n"
		" %2.2X %2.2X %2.2X %2.2X %2.2X %2.2X %2.2X %2.2X\n"
		" %2.2X %2.2X %2.2X %2.2X %2.2X %2.2X %2.2X %2.2X\n"
		" %2.2X %2.2X %2.2X %2.2X %2.2X %2.2X %2.2X %2.2X\n"
		"\n",
		OSCL_PRETTY_FUNCTION,
		x[0], x[1], x[2], x[3], x[4], x[5], x[6], x[7],
		x[8], x[9], x[10], x[11], x[12], x[13], x[14], x[15],
		x[16], x[17], x[18], x[19], x[20], x[21], x[22], x[23],
		x[24], x[25], x[26], x[27], x[28], x[29], x[30], x[31],
		y[0], y[1], y[2], y[3], y[4], y[5], y[6], y[7],
		y[8], y[9], y[10], y[11], y[12], y[13], y[14], y[15],
		y[16], y[17], y[18], y[19], y[20], y[21], y[22], y[23],
		y[24], y[25], y[26], y[27], y[28], y[29], y[30], y[31]
		);
	#endif

	calculateEcdhSecret();

	if(_pubKeyType){
		return true;
		}

	// No OOB Public Key is used

	if(_pubKeyType == 0x00){
		// No OOB public key is used.

		bool
		failed	= sendPublicKey();

		if(failed){
			Oscl::Error::Info::log(
				"%s: sendPublicKey failed.\n",
				OSCL_PRETTY_FUNCTION
				);
			}
		else {
			_publicKeySent	= true;
			}
		}

	return true;
	}

bool	Reaper::processInputComplete(Oscl::Decoder::Api& decoder) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s:\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif

	return true;
	}

bool	Reaper::processConfirmation(Oscl::Decoder::Api& decoder) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif

	decoder.copyOut(
		_provisionerConfirmationData,
		sizeof(_provisionerConfirmationData)
		);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow\n",
			OSCL_PRETTY_FUNCTION
			);
		sendFailed(Oscl::BT::Mesh::Prov::Errors::invalidFormat);
		return false;
		}

	#ifdef DEBUG_TRACE_TMI
	const uint8_t*	c	= _provisionerConfirmationData;

	Oscl::Error::Info::log(
		"%s:\n"
		" Confirmation:\n"
		" %2.2X %2.2X %2.2X %2.2X %2.2X %2.2X %2.2X %2.2X\n"
		" %2.2X %2.2X %2.2X %2.2X %2.2X %2.2X %2.2X %2.2X\n"
		"\n",
		OSCL_PRETTY_FUNCTION,
		c[0], c[1], c[2], c[3], c[4], c[5], c[6], c[7],
		c[8], c[9], c[10], c[11], c[12], c[13], c[14], c[15]
		);
	#endif

	if(_confirmationSent){
		return true;
		}

	bool
	failed	= sendProvisioningConfirmation();

	if(!failed){
		_confirmationSent	= true;
		}

	return true;
	}

bool	Reaper::checkNoOobConfirmation(const uint8_t* random) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif

	// input:
	//	_provisionerConfirmationData
	//	random

	// RandomProvisioner - generated by provisioner
	// ConfirmationProvisioner = AES-CMAC[ConfirmationKey] (RandomProvisioner || AuthValue)

	Oscl::AES128::CMAC	cmac(_confirmationKey);

	cmac.accumulate(
		random,
		16
		);

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s: AuthValue:\n",
		OSCL_PRETTY_FUNCTION
		);
	Oscl::Error::Info::hexDump(
		_authValue,
		sizeof(_authValue)
		);
	#endif

	cmac.accumulate(
		_authValue,
		sizeof(_authValue)
		);

	cmac.finalize();

	uint8_t	confirmationData[16];

	cmac.copyOut(
		confirmationData,
		sizeof(confirmationData)
		);

	bool
	failed	= memcmp(
				confirmationData,
				_provisionerConfirmationData,
				sizeof(confirmationData)
				)?true:false;

	return failed;
	}

bool	Reaper::processRandom(Oscl::Decoder::Api& decoder) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif

	decoder.copyOut(
		_provisionerRandom,
		sizeof(_provisionerRandom)
		);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow\n",
			OSCL_PRETTY_FUNCTION
			);
		sendFailed(Oscl::BT::Mesh::Prov::Errors::invalidFormat);
		return false;
		}

	#ifdef DEBUG_TRACE_TMI
	const uint8_t*	r	= _provisionerRandom;

	Oscl::Error::Info::log(
		"%s:\n"
		" Random:\n"
		" %2.2X %2.2X %2.2X %2.2X %2.2X %2.2X %2.2X %2.2X\n"
		" %2.2X %2.2X %2.2X %2.2X %2.2X %2.2X %2.2X %2.2X\n"
		"\n",
		OSCL_PRETTY_FUNCTION,
		r[0], r[1], r[2], r[3], r[4], r[5], r[6], r[7],
		r[8], r[9], r[10], r[11], r[12], r[13], r[14], r[15]
		);
	#endif

	if(_randomSent){
		return true;
		}

	bool
	failed	= checkNoOobConfirmation(_provisionerRandom);

	if(failed){
		Oscl::Error::Info::log(
			"%s: checkNoOobConfirmation() failed.\n",
			OSCL_PRETTY_FUNCTION
			);
		sendFailed(Oscl::BT::Mesh::Prov::Errors::confirmationFailed);
		return true;
		}

	failed	= sendRandom();
	if(failed){
		Oscl::Error::Info::log(
			"%s: sendRandom() failed.\n",
			OSCL_PRETTY_FUNCTION
			);
		return true;
		}

	_randomSent	= true;

	return true;
	}

bool	Reaper::processData(Oscl::Decoder::Api& decoder) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif

	uint8_t	payload[25+8];

	decoder.copyOut(
		payload,
		sizeof(payload)
		);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow\n",
			OSCL_PRETTY_FUNCTION
			);
		sendFailed(Oscl::BT::Mesh::Prov::Errors::invalidFormat);
		return false;
		}

	#ifdef DEBUG_TRACE_TMI
	const uint8_t*	d	= &payload[0];
	const uint8_t*	m	= &payload[25];

	Oscl::Error::Info::log(
		"%s:\n"
		" Encrypted Provisioning Data:\n"
		" %2.2X %2.2X %2.2X %2.2X %2.2X %2.2X %2.2X %2.2X\n"
		" %2.2X %2.2X %2.2X %2.2X %2.2X %2.2X %2.2X %2.2X\n"
		" %2.2X %2.2X %2.2X %2.2X %2.2X %2.2X %2.2X %2.2X\n"
		" %2.2X\n"
		" Provisioning Data MIC:\n"
		" %2.2X %2.2X %2.2X %2.2X %2.2X %2.2X %2.2X %2.2X\n"
		"\n",
		OSCL_PRETTY_FUNCTION,
		d[0], d[1], d[2], d[3], d[4], d[5], d[6], d[7],
		d[8], d[9], d[10], d[11], d[12], d[13], d[14], d[15],
		d[16], d[17], d[18], d[19], d[20], d[21], d[22], d[23],
		d[24],
		m[0], m[1], m[2], m[3], m[4], m[5], m[6], m[7]
		);
	#endif

	calculateSessionKey();
	calculateSessionNonce();

	uint8_t	plainText[sizeof(payload)];

	// Authenticate and/then decrypt the payload
	unsigned
	length	= Oscl::AES128::Ccm::decrypt(
				_sessionKey,
				&_sessionNonce[3],
				0,	// adata
				0,	// adata length
				payload,	// ciphermic
				sizeof(payload),	// ciphermic length
				8,	// size-of-MIC
				plainText	// output buffer
				);

	if(!length){
		Oscl::Error::Info::log(
			"%s: Oscl::AES128::decrypt() failed\n",
			OSCL_PRETTY_FUNCTION
			);
		sendFailed(Oscl::BT::Mesh::Prov::Errors::decryptionFailed);
		return true;
		}

	Oscl::Endian::Decoder::Linear::Part
	beDecoder(
		plainText,
		length
		);

	Oscl::Decoder::Api&
	pDecoder	= beDecoder.be();

	uint8_t		netKey[16];
	uint16_t	keyIndex;
	uint8_t		flags;
	uint32_t	ivIndex;
	uint16_t	unicastAddress;

	pDecoder.copyOut(
		netKey,
		sizeof(netKey)
		);

	pDecoder.decode(keyIndex);
	pDecoder.decode(flags);
	pDecoder.decode(ivIndex);
	pDecoder.decode(unicastAddress);

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s:\n"
		" Net Key: \n"
		"  %2.2X %2.2X %2.2X %2.2X %2.2X %2.2X %2.2X %2.2X\n"
		"  %2.2X %2.2X %2.2X %2.2X %2.2X %2.2X %2.2X %2.2X\n"
		" KeyIndex: 0x%4.4X\n"
		" Flags: 0x%2.2X\n"
		" IV Index: 0x%8.8X\n"
		" Unicast Address: 0x%4.4X\n"
		"",
		OSCL_PRETTY_FUNCTION,
		netKey[0], netKey[1], netKey[2], netKey[3],
		netKey[4], netKey[5], netKey[6], netKey[7],
		netKey[8], netKey[9], netKey[10], netKey[11],
		netKey[12], netKey[13], netKey[14], netKey[15],
		keyIndex,
		flags,
		ivIndex,
		unicastAddress
		);
	#endif

	if(_completeSent){
		return true;
		}

	uint8_t	deviceKey[16];

	generateDeviceKey(deviceKey);

	bool
	failed	= _context.createNewNode(
				netKey,
				deviceKey,
				keyIndex,
				flags,
				ivIndex,
				unicastAddress
				);

	if(failed){
		sendFailed(Oscl::BT::Mesh::Prov::Errors::outOfResources);
		return true;
		}

	failed	= sendComplete();

	if(failed){
		return true;
		}

	_completeSent	= true;

	return true;
	}

bool	Reaper::processComplete(Oscl::Decoder::Api& decoder) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s:\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif

	// Not expected by a device
	sendFailed(Oscl::BT::Mesh::Prov::Errors::unexpectedPDU);

	return true;
	}

void	Reaper::generateDeviceKey(uint8_t deviceKey[16]) const noexcept{
	static const uint8_t	prdk[] = {'p','r','d','k'};

	Oscl::BT::Crypto::Keys::k1(
		_ecdhSecret,
		sizeof(_ecdhSecret),
		_provisioningSalt,
		prdk,
		sizeof(prdk),
		deviceKey
		);
	}

static const char*	errorCodeString(uint8_t errorCode) noexcept{

	switch(errorCode){
		case 0x00:
			return "Prohibited";
		case 0x01:
			return "Invalid PDU";
		case 0x02:
			return "Invalid Format";
		case 0x03:
			return "Unexpected PDU";
		case 0x04:
			return "Confirmation Failed";
		case 0x05:
			return "Out of Resources";
		case 0x06:
			return "Decryption Failed";
		case 0x07:
			return "Unexpected Error";
		case 0x08:
			return "Cannot Assign Addresses";
		default:
			return "RFU";
		}
	}

bool	Reaper::processFailed(Oscl::Decoder::Api& decoder) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif

	uint8_t	errorCode;
	decoder.decode(errorCode);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow\n",
			OSCL_PRETTY_FUNCTION
			);
		sendFailed(Oscl::BT::Mesh::Prov::Errors::invalidFormat);
		return false;
		}

	Oscl::Error::Info::log(
		"%s: Error Code: (0x%2.2X), \"%s\".\n",
		OSCL_PRETTY_FUNCTION,
		errorCode,
		errorCodeString(errorCode)
		);

	return true;
	}

bool	Reaper::receive(
				const void*		frame,
				unsigned int	length
				) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION
		);
	#ifdef DEBUG_TRACE_TMI
	Oscl::Error::Info::hexDump(
		frame,
		length
		);
	#endif
	#endif

	Oscl::Endian::Decoder::Linear::Part
	beDecoder(
		frame,
		length
		);

	Oscl::Decoder::Api&
	decoder	= beDecoder.be();

	uint8_t	octet;
	decoder.decode(octet);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow\n",
			OSCL_PRETTY_FUNCTION
			);
		sendFailed(Oscl::BT::Mesh::Prov::Errors::invalidFormat);
		return false;
		}

	uint8_t	type	= octet & 0x3F;

	switch(type){
		case 0x00:
			// Invite
			return processInvite(decoder);
		case 0x01:
			// Capabilities
			return processCapabilities(decoder);
		case 0x02:
			// Start
			return processStart(decoder);
		case 0x03:
			// Public Key
			return processPublicKey(decoder);
		case 0x04:
			// Input Complete
			return processInputComplete(decoder);
		case 0x05:
			// Confirmation
			return processConfirmation(decoder);
		case 0x06:
			// Random
			return processRandom(decoder);
		case 0x07:
			// Data
			return processData(decoder);
		case 0x08:
			// Complete
			return processComplete(decoder);
		case 0x09:
			// Failed
			return processFailed(decoder);
		default:
			sendFailed(Oscl::BT::Mesh::Prov::Errors::invalidPDU);
			return false;
		}

	return true;
	}

bool	Reaper::sendCapabilities() noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif

	constexpr static uint8_t	type	= 0x01;	// Provisioning Capabilities

	uint8_t	nElements	= _context.getNumElements();

	constexpr static uint16_t	algorithmsBits	=
		1<<0	// IPS P-256 Eliptic Curve
		;
	constexpr static uint8_t	publicKeyTypeBits	=
		0<<0	// Public Key OOB information avavilble
		;

	constexpr static uint8_t	staticOobTypeAvailable	=
#if 0
		// SiLabs doesn't like this!
		// Nordic is good with this.
		1<<0	// Static OOB information available
#else
		0<<0	// Static OOB information un-available
#endif
		;

	constexpr static uint8_t	staticOobTypeUnavailable	=
		0<<0	// Static OOB information available
		;

	constexpr static uint8_t	outputOobSizeUnavailable	= 0x00;	// The device does not support output OOB

	const uint16_t	outputOobActionBits	=
		0
		|	(
				_context.oobOutputActionBlinkIsSupported()
				?	(1<<Oscl::BT::Mesh::Prov::Capabilities::OOB::Action::Output::blinkBit)
				:	0
			)
		|	(
				_context.oobOutputActionBeepIsSupported()
				?	(1<<Oscl::BT::Mesh::Prov::Capabilities::OOB::Action::Output::beepBit)
				:	0
			)
		|	(
				_context.oobOutputActionVibrateIsSupported()
				?	(1<<Oscl::BT::Mesh::Prov::Capabilities::OOB::Action::Output::vibrateBit)
				:	0
			)
		|	(
				_context.oobOutputActionNumericIsSupported()
				?	(1<<Oscl::BT::Mesh::Prov::Capabilities::OOB::Action::Output::numericBit)
				:	0
			)
		|	(
				_context.oobOutputActionAlphaNumericIsSupported()
				?	(1<<Oscl::BT::Mesh::Prov::Capabilities::OOB::Action::Output::alphaNumericBit)
				:	0
			)
		;

	constexpr static uint8_t	inputOobSize	= 0;	// The device does not support input OOB

	constexpr static uint16_t	inputOobActionBits	=
		0
//		|	1<< Oscl::BT::Mesh::Prov::Capabilities::OOB::Action::Input::pushBit
//		|	1<< Oscl::BT::Mesh::Prov::Capabilities::OOB::Action::Input::twistBit
//		|	1<< Oscl::BT::Mesh::Prov::Capabilities::OOB::Action::Input::numberBit
//		|	1<< Oscl::BT::Mesh::Prov::Capabilities::OOB::Action::Input::alphanumericBit
		;

	static const uint8_t	oobAvailablePdu[]	= {
		type,
		nElements,
		algorithmsBits>>8,	// MSB
		algorithmsBits,		// LSB
		publicKeyTypeBits,
		staticOobTypeAvailable,
		_outputOobSizeInCharacters,
		(uint8_t)(outputOobActionBits>>8),	// MSB
		(uint8_t)(outputOobActionBits),	// LSB
		inputOobSize,
		inputOobActionBits>>8,	// MSB
		inputOobActionBits		// LSB
		};

	static const uint8_t	oobUnavailablePdu[]	= {
		type,
		nElements,
		algorithmsBits>>8,	// MSB
		algorithmsBits,		// LSB
		publicKeyTypeBits,
		staticOobTypeUnavailable,
		outputOobSizeUnavailable,
		(uint8_t)(outputOobActionBits>>8),	// MSB
		(uint8_t)(outputOobActionBits),	// LSB
		inputOobSize,
		inputOobActionBits>>8,	// MSB
		inputOobActionBits		// LSB
		};

	const uint8_t*
	pdu	= _oobAvailable?
				oobAvailablePdu:
				oobUnavailablePdu
				;

	unsigned
	pduSize	= _oobAvailable?
				sizeof(oobAvailablePdu):
				sizeof(oobUnavailablePdu)
				;

	memcpy(
		_capabilitiesPdu,
		&pdu[1],
		sizeof(_capabilitiesPdu)
		);

	#ifdef DEBUG_TRACE_TMI
	Oscl::Error::Info::hexDump(
		pdu,
		pduSize
		);
	#endif

	Oscl::Handle<Oscl::Pdu::Fixed>
	handle	= _freeStoreApi.allocConstFixed(
				pdu,
				pduSize
				);

	if(!handle){
		Oscl::Error::Info::log(
			"%s: out of memory.\n",
			OSCL_PRETTY_FUNCTION
			);
		return true;
		}

	_linkApi.transfer(handle);

	return false;
	}

bool	Reaper::sendPublicKey() noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif

	BufferMem*
	mem	= allocBuffer();

	if(!mem){
		Oscl::Error::Info::log(
			"%s: out of buffer memory.\n",
			OSCL_PRETTY_FUNCTION
			);
		return true;
		}

	Oscl::Encoder::BE::Base	beEncoder(mem,sizeof(BufferMem));
	Oscl::Encoder::Api&	encoder	= beEncoder;

	static const uint8_t	type	= 0x03;

	encoder.encode(type);
	encoder.copyIn(_publicKeyX,sizeof(_publicKeyX));
	encoder.copyIn(_publicKeyY,sizeof(_publicKeyY));

	if(encoder.overflow()){
		Oscl::Error::Info::log(
			"%s: overflow.\n",
			OSCL_PRETTY_FUNCTION
			);
		return true;
		}

	Oscl::Handle<Oscl::Pdu::Fixed>
	handle	= _freeStoreApi.allocFixed(
				_bufferFreeStoreMgrComposer,
				mem,
				encoder.length()
				);

	if(!handle){
		freeBuffer(mem);
		Oscl::Error::Info::log(
			"%s: out of fixed memory.\n",
			OSCL_PRETTY_FUNCTION
			);
		return true;
		}

	_linkApi.transfer(handle);

	return false;
	}

bool	Reaper::sendInputComplete() noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif

	BufferMem*
	mem	= allocBuffer();

	if(!mem){
		Oscl::Error::Info::log(
			"%s: out of buffer memory.\n",
			OSCL_PRETTY_FUNCTION
			);
		return true;
		}

	Oscl::Encoder::BE::Base	beEncoder(mem,sizeof(BufferMem));
	Oscl::Encoder::Api&	encoder	= beEncoder;

	static const uint8_t	type	= 0x04; // Provisioning Input Complete

	encoder.encode(type);

	if(encoder.overflow()){
		Oscl::Error::Info::log(
			"%s: overflow.\n",
			OSCL_PRETTY_FUNCTION
			);
		return true;
		}

	Oscl::Handle<Oscl::Pdu::Fixed>
	handle	= _freeStoreApi.allocFixed(
				_bufferFreeStoreMgrComposer,
				mem,
				encoder.length()
				);

	if(!handle){
		freeBuffer(mem);
		Oscl::Error::Info::log(
			"%s: out of fixed memory.\n",
			OSCL_PRETTY_FUNCTION
			);
		return true;
		}

	_linkApi.transfer(handle);

	return false;
	}

bool	Reaper::createDevicePublicAndPrivateKeys() noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif

	uint8_t	random[32];

	Oscl::Entropy::Random::fetch(
		random,
		sizeof(random)
		);

	uint8_t	privateKey[32];
	uint8_t	publicKey[64];

	bool
	success	= ecc_make_key_using_external_random(
				random,
				privateKey,
				publicKey
				);

	if(!success){
		Oscl::Error::Info::log(
			"%s: ecc_make_key_using_external_random() failed!\n",
			OSCL_PRETTY_FUNCTION
			);
		return true;
		}

	extractAndSaveKeys(
		privateKey,
		publicKey
		);

	return false;
	}

void	Reaper::calculateEcdhSecret() noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif

	// Convert BT byte-order to ECDH byte-order
	uint8_t	provisionerPublicKey[64];
	
	for(unsigned i=0;i<32;++i){
		// X
		provisionerPublicKey[i]		= _provisionerPublicKey[31-i];
		provisionerPublicKey[32+i]	= _provisionerPublicKey[63-i];
		}

	uint8_t	ecdhSecret[32];

	bool
	success	= ecdh_shared_secret_using_fixed_z(
				provisionerPublicKey,
				_privateKey,
				ecdhSecret
				);

	if(!success){
		// I suspect that this can't happen, since
		// there is no I/O involved.
		Oscl::Error::Info::log(
			"%s: ecdh_shared_secret_using_fixed_z() failed!\n",
			OSCL_PRETTY_FUNCTION
			);
		return;
		}

	// ECDH secret is stored in BT byte-order
	for(unsigned i=0;i<32;++i){
		_ecdhSecret[i]	= ecdhSecret[31-i];
		}

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"_ecdhSecret:\n"
		);

	#ifdef DEBUG_TRACE_TMI
	Oscl::Error::Info::hexDump(
		_ecdhSecret,
		sizeof(_ecdhSecret)
		);
	#endif
	#endif

	}

void	Reaper::calculateSessionNonce() noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif

	// SessionNonce = k1(ECDHSecret, ProvisioningSalt, “prsn”)

	static const uint8_t	prsn[]	= {'p','r','s','n'};

	Oscl::BT::Crypto::Keys::k1(
		_ecdhSecret,
		sizeof(_ecdhSecret),
		_provisioningSalt,
		prsn,
		sizeof(prsn),
		_sessionNonce
		);
	}

void	Reaper::calculateSessionKey() noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif

	uint8_t	input[
					sizeof(_confirmationSalt)
				+	sizeof(_provisionerRandom)
				+	sizeof(_deviceRandom)
				];

	memcpy(
		&input[0],
		_confirmationSalt,
		sizeof(_confirmationSalt)
		);

	memcpy(
		&input[sizeof(_confirmationSalt)],
		_provisionerRandom,
		sizeof(_provisionerRandom)
		);

	memcpy(
		&input[
				sizeof(_confirmationSalt)
			+	sizeof(_provisionerRandom)
			],
		_deviceRandom,
		sizeof(_deviceRandom)
		);

	// ProvisioningSalt = s1(ConfirmationSalt || RandomProvisioner || RandomDevice)
	Oscl::BT::Crypto::Salt::s1(
		input,
		sizeof(input),
		_provisioningSalt,
		sizeof(_provisioningSalt)
		);

	// SessionKey = k1(ECDHSecret, ProvisioningSalt, “prsk”)

	static const uint8_t	prsk[]	= {'p','r','s','k'};

	Oscl::BT::Crypto::Keys::k1(
		_ecdhSecret,
		sizeof(_ecdhSecret),
		_provisioningSalt,
		prsk,
		sizeof(prsk),
		_sessionKey
		);
	}

void	Reaper::calculateConfirmationKey() noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif

	// ConfirmationInputs = ProvisioningInvitePDUValue || ProvisioningCapabilitiesPDUValue || ProvisioningStartPDUValue || PublicKeyProvisioner || PublicKeyDevice
	// ConfirmationSalt = s1(ConfirmationInputs)
	// ConfirmationKey = k1(ECDHSecret, ConfirmationSalt, “prck”)
	uint8_t	confirmationInputs[
					sizeof(_invitePdu)
				+	sizeof(_capabilitiesPdu)
				+	sizeof(_startPdu)
				+	sizeof(_provisionerPublicKey)
				+	sizeof(_publicKeyX)
				+	sizeof(_publicKeyY)
				];

	// concate
	// ConfirmationInputs = ProvisioningInvitePDUValue || ProvisioningCapabilitiesPDUValue || ProvisioningStartPDUValue || PublicKeyProvisioner || PublicKeyDevice
	memcpy(
		&confirmationInputs[0],
		_invitePdu,
		sizeof(_invitePdu)
		);

	memcpy(
		&confirmationInputs[sizeof(_invitePdu)],
		_capabilitiesPdu,
		sizeof(_capabilitiesPdu)
		);

	memcpy(
		&confirmationInputs[
				sizeof(_invitePdu)
			+	sizeof(_capabilitiesPdu)
			],
		_startPdu,
		sizeof(_startPdu)
		);

	memcpy(
		&confirmationInputs[
				sizeof(_invitePdu)
			+	sizeof(_capabilitiesPdu)
			+	sizeof(_startPdu)
			],
		_provisionerPublicKey,
		sizeof(_provisionerPublicKey)
		);

	memcpy(
		&confirmationInputs[
				sizeof(_invitePdu)
			+	sizeof(_capabilitiesPdu)
			+	sizeof(_startPdu)
			+	sizeof(_provisionerPublicKey)
			],
		_publicKeyX,
		sizeof(_publicKeyX)
		);

	memcpy(
		&confirmationInputs[
				sizeof(_invitePdu)
			+	sizeof(_capabilitiesPdu)
			+	sizeof(_startPdu)
			+	sizeof(_provisionerPublicKey)
			+	sizeof(_publicKeyX)
			],
		_publicKeyY,
		sizeof(_publicKeyY)
		);

	Oscl::BT::Crypto::Salt::s1(
		confirmationInputs,
		sizeof(confirmationInputs),
		_confirmationSalt,
		sizeof(_confirmationSalt)
		);

	static const uint8_t	prck[]	= {'p','r','c','k'};

	Oscl::BT::Crypto::Keys::k1(
		_ecdhSecret,
		sizeof(_ecdhSecret),
		_confirmationSalt,
		prck,
		sizeof(prck),
		_confirmationKey
		);

	}

void	Reaper::calculateConfirmationData() noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif

	// RandomDevice - generated by device and later sent in Provisioning Random PDU
	// ConfirmationDevice = AES-CMAC[ConfirmationKey] (RandomDevice || AuthValue)

	calculateConfirmationKey();

	Oscl::Entropy::Random::fetch(
		_deviceRandom,
		sizeof(_deviceRandom)
		);

	Oscl::AES128::CMAC	cmac(_confirmationKey);

	cmac.accumulate(
		_deviceRandom,
		sizeof(_deviceRandom)
		);

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s: AuthValue:\n",
		OSCL_PRETTY_FUNCTION
		);
	Oscl::Error::Info::hexDump(
		_authValue,
		sizeof(_authValue)
		);
	#endif

	cmac.accumulate(
		_authValue,
		sizeof(_authValue)
		);

	cmac.finalize();

	cmac.copyOut(
		_confirmationData,
		sizeof(_confirmationData)
		);
	}

bool	Reaper::sendProvisioningConfirmation() noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif

	// AuthValue (No OOB) = 0x00000000000000000000000000000000, {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}
	// RandomDevice - generated by device and later sent in
	// ConfirmationInputs = ProvisioningInvitePDUValue || ProvisioningCapabilitiesPDUValue || ProvisioningStartPDUValue || PublicKeyProvisioner || PublicKeyDevice
	// ConfirmationSalt = s1(ConfirmationInputs)
	// ConfirmationKey = k1(ECDHSecret, ConfirmationSalt, “prck”)
	// ConfirmationDevice = AES-CMAC[ConfirmationKey] (RandomDevice || AuthValue)

	calculateConfirmationData();

	BufferMem*
	mem	= allocBuffer();

	if(!mem){
		Oscl::Error::Info::log(
			"%s: out of buffer memory.\n",
			OSCL_PRETTY_FUNCTION
			);
		return true;
		}

	Oscl::Encoder::BE::Base
	beEncoder(
		mem,
		sizeof(BufferMem)
		);

	Oscl::Encoder::Api&	encoder	= beEncoder;

	static const uint8_t	type	= 0x05;

	encoder.encode(type);
	encoder.copyIn(_confirmationData,sizeof(_confirmationData));

	if(encoder.overflow()){
		freeBuffer(mem);
		Oscl::Error::Info::log(
			"%s: overflow.\n",
			OSCL_PRETTY_FUNCTION
			);
		return true;
		}

	Oscl::Handle<Oscl::Pdu::Fixed>
	handle	= _freeStoreApi.allocFixed(
				_bufferFreeStoreMgrComposer,
				mem,
				encoder.length()
				);

	if(!handle){
		freeBuffer(mem);
		Oscl::Error::Info::log(
			"%s: out of fixed memory.\n",
			OSCL_PRETTY_FUNCTION
			);
		return true;
		}

	_linkApi.transfer(handle);

	return false;
	}

bool	Reaper::sendRandom() noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif

	// _deviceRandom

	BufferMem*
	mem	= allocBuffer();

	if(!mem){
		Oscl::Error::Info::log(
			"%s: out of buffer memory.\n",
			OSCL_PRETTY_FUNCTION
			);
		return true;
		}

	Oscl::Encoder::BE::Base
	beEncoder(
		mem,
		sizeof(BufferMem)
		);

	Oscl::Encoder::Api&	encoder	= beEncoder;

	static const uint8_t	type	= 0x06;

	encoder.encode(type);
	encoder.copyIn(_deviceRandom,sizeof(_deviceRandom));

	if(encoder.overflow()){
		freeBuffer(mem);
		Oscl::Error::Info::log(
			"%s: overflow.\n",
			OSCL_PRETTY_FUNCTION
			);
		return true;
		}

	Oscl::Handle<Oscl::Pdu::Fixed>
	handle	= _freeStoreApi.allocFixed(
				_bufferFreeStoreMgrComposer,
				mem,
				encoder.length()
				);

	if(!handle){
		freeBuffer(mem);
		Oscl::Error::Info::log(
			"%s: out of fixed memory.\n",
			OSCL_PRETTY_FUNCTION
			);
		return true;
		}

	_linkApi.transfer(handle);

	return false;
	}

bool	Reaper::sendComplete() noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif

	static const uint8_t	type	= 0x08;

	Oscl::Handle<Oscl::Pdu::Fixed>
	handle	= _freeStoreApi.allocConstFixed(
				&type,
				sizeof(type)
				);

	if(!handle){
		Oscl::Error::Info::log(
			"%s: out of fixed memory.\n",
			OSCL_PRETTY_FUNCTION
			);
		return true;
		}

	_linkApi.transfer(handle);

	_context.provisioningComplete();

	return false;
	}

bool	Reaper::sendFailed(uint8_t errorCode) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif

	BufferMem*
	mem	= allocBuffer();

	if(!mem){
		Oscl::Error::Info::log(
			"%s: out of buffer memory.\n",
			OSCL_PRETTY_FUNCTION
			);
		return true;
		}

	Oscl::Encoder::BE::Base
	beEncoder(
		mem,
		sizeof(BufferMem)
		);

	Oscl::Encoder::Api&	encoder	= beEncoder;

	static const uint8_t	type	= 0x09;

	encoder.encode(type);
	encoder.encode(errorCode);

	if(encoder.overflow()){
		freeBuffer(mem);
		Oscl::Error::Info::log(
			"%s: overflow.\n",
			OSCL_PRETTY_FUNCTION
			);
		return true;
		}

	Oscl::Handle<Oscl::Pdu::Fixed>
	handle	= _freeStoreApi.allocFixed(
				_bufferFreeStoreMgrComposer,
				mem,
				encoder.length()
				);

	if(!handle){
		freeBuffer(mem);
		Oscl::Error::Info::log(
			"%s: out of fixed memory.\n",
			OSCL_PRETTY_FUNCTION
			);
		return true;
		}

	_linkApi.transfer(handle);

	return false;
	}

