/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_bluetooth_mesh_prov_device_reaperh_
#define _oscl_bluetooth_mesh_prov_device_reaperh_

#include <stdint.h>
#include "oscl/memory/block.h"
#include "oscl/done/operation.h"
#include "oscl/frame/fwd/composer.h"
#include "oscl/pdu/fwd/api.h"
#include "oscl/freestore/composers.h"
#include "oscl/queue/queue.h"
#include "oscl/pdu/fixed.h"
#include "oscl/pdu/composite.h"
#include "oscl/pdu/fragment.h"

#include "oscl/endian/decoder/api.h"
#include "oscl/pdu/memory/api.h"

/** */
namespace Oscl {
/** */
namespace BT {
/** */
namespace Mesh {
/** */
namespace Prov {
/** */
namespace Device {

/** */
class Reaper;

/** */
class ContextApi {
	public:
		/** This operation is called when the Reaper
			is stopped after it completes the process
			and is ready to be destroyed.
		 */
		virtual void	stopped(Reaper& device) noexcept=0;

		/** This operation is called by the Reaper to
			create a new network.
		 */
		virtual bool	createNewNode(
							const uint8_t	netKey[16],
							const uint8_t	deviceKey[16],
							uint16_t		keyIndex,
							uint8_t			flags,
							uint32_t		ivIndex,
							uint16_t		unicastAddress
							) noexcept=0;

		/** */
		virtual uint8_t	getNumElements() const noexcept=0;

		/** */
		virtual void	setAttentionTimer(uint8_t nSeconds) noexcept=0;

		/** */
		virtual bool	oobOutputActionBlinkIsSupported() const noexcept=0;

		/** */
		virtual bool	oobOutputActionBeepIsSupported() const noexcept=0;

		/** */
		virtual bool	oobOutputActionVibrateIsSupported() const noexcept=0;

		/** */
		virtual bool	oobOutputActionNumericIsSupported() const noexcept=0;

		/** */
		virtual bool	oobOutputActionAlphaNumericIsSupported() const noexcept=0;

		/** */
		virtual void	oobOutputBlink(uint32_t n) noexcept=0;

		/** */
		virtual void	oobOutputBeep(uint32_t n) noexcept=0;

		/** */
		virtual void	oobOutputVibrate(uint32_t n) noexcept=0;

		/** */
		virtual void	oobOutputNumeric(uint32_t n) noexcept=0;

		/** */
		virtual void	oobOutputAlphaNumeric(const char* s) noexcept=0;

		/** This operation is invoked to notify the context
			that provisioning has been completed.
		 */
		virtual void	provisioningComplete() noexcept=0;

	};

/** */
class Reaper {
	private:
		/** */
		Device::ContextApi&			_context;

		/** */
		Oscl::Pdu::Memory::Api&		_freeStoreApi;

		/** */
		Oscl::Pdu::FWD::Api&		_linkApi;

		/** */
		const bool					_oobAvailable;

		/** */
		const uint8_t				_outputOobSizeInCharacters;

	private:
		/** */
		Oscl::Frame::FWD::Composer<Reaper>	_rxComposer;

	private:
		/** */
		constexpr static unsigned	bufferSize	= 128;

		/** */
		union BufferMem {                   
			void*   __qitemlink;
			Oscl::Memory::AlignedBlock<bufferSize>	mem;
			};

		/** */
		constexpr static unsigned	nProvBuffers	= 32;

		/** */
		BufferMem								_buffers[nProvBuffers];

		/** */
		Oscl::Queue<BufferMem>					_bufferPool;

		/** */
		Oscl::FreeStore::Composer<Reaper,void>	_bufferFreeStoreApiComposer;

		/** */
		Oscl::FreeStore::FreeStoreMgr<void>		_bufferFreeStoreMgrComposer;

	private:
		/** Stored in ECDH byte-order
		 */
		uint8_t			_privateKey[32];

		/** Stored in BT byte-order
		 */
		uint8_t			_publicKeyX[32];

		/** Stored in BT byte-order
		 */
		uint8_t			_publicKeyY[32];

		/** Stored in BT byte order
		 */
		uint8_t			_ecdhSecret[32];

	private:
		/** */
		uint8_t			_invitePdu[1];

		/** */
		uint8_t			_capabilitiesPdu[11];

		/** */
		uint8_t			_startPdu[5];

		/** Stored in BT byte-order
		 */
		uint8_t			_provisionerPublicKey[64];

		/** */
		uint8_t			_deviceRandom[16];

		/** */
		uint8_t			_provisionerRandom[16];

	private:
		/** */
		uint8_t			_provisionerConfirmationData[16];

		/** */
		uint8_t			_confirmationData[16];

		/** */
		uint8_t			_confirmationKey[16];

		/** */
		uint8_t			_confirmationSalt[16];

		/** */
		uint8_t			_provisioningSalt[16];

		/** */
		uint8_t			_sessionKey[16];

		/** */
		uint8_t			_sessionNonce[16];

	private:
		/** From Start */
		uint8_t			_algorithm;

		/** From Start */
		uint8_t			_pubKeyType;

		/** From Start */
		uint8_t			_authenticationMethod;

		/** From Start */
		uint8_t			_authenticationAction;

		/** From Start */
		uint8_t			_authenticationSize;

	private:
		/** This value depends upon Authentication Method,
			Authentication Size,
			-	If the Authentication Method is
				"No OOB authentication is used" the value is all-zeros.
			-	If Authentication Method "Output OOB authentication is used" and
				The Authentication Action:
				-	Output Numeric then the value is a big-endian binary representation
					of the random number of the appropriate Authentication Size.
				-	Output Alphanumeric then the value is an ASCII string located
					in the beginning of the array with all other values as zero.
		 */
		uint8_t			_authValue[16];

		/** When Output OOB authentication is used, this
			is the value that is displayed or blinked.
		 */
		uint32_t		_authNumber;

	private:
		/** */
		bool			_stopping;

		/** */
		bool			_started;

		/** */
		bool			_capabilitiesSent;

		/** */
		bool			_publicKeySent;

		/** */
		bool			_confirmationSent;

		/** */
		bool			_randomSent;

		/** */
		bool			_completeSent;

	public:
		/** */
		Reaper(
			Device::ContextApi&			context,
			Oscl::Pdu::Memory::Api&		freeStoreApi,
			Oscl::Pdu::FWD::Api&		linkApi,
			bool						oobAvailable,
			uint8_t						outputOobSizeInCharacters
			) noexcept;

		/** */
		Oscl::Frame::FWD::Api&	getRxApi() noexcept;

		/** */
		void	start() noexcept;

		/** */
		void	stop() noexcept;

	private:
		/** */
		void	stopNext() noexcept;

		/** */
		bool	allMemoryIsFree() noexcept;

		/** */
		bool	allBuffersFree() noexcept;

	private: // Oscl::FreeStore::FreeStoreApi	_bufferFreeStoreApiComposer;
		/** */
		void	bufferFree(void* buffer) noexcept;

		/** */
		void	freeBuffer(BufferMem* buffer) noexcept;

		/** */
		BufferMem*	allocBuffer() noexcept;

	private: // Oscl::BT::Mesh::::Prov::Session::Api
		/** */
		bool	receive(
					const void*		frame,
					unsigned int	length
					) noexcept;
	private:
		/** */
		void	displayPublicKey() noexcept;

		/** */
		void	blinkAuthValue() noexcept;

		/** */
		void	beepAuthValue() noexcept;

		/** */
		void	vibrateAuthValue() noexcept;

		/** */
		void	outputNumericAuthValue() noexcept;

		/** */
		void	outputAlphaNumericAuthValue() noexcept;

		/** */
		void	extractAndSaveKeys(
					const uint8_t*	privateKey,
					const uint8_t*	publicKey
					) noexcept;

		/** */
		bool	createDevicePublicAndPrivateKeys() noexcept;

		/** */
		void	calculateConfirmationKey() noexcept;

		/** */
		void	calculateConfirmationData() noexcept;

		/** */
		void	calculateEcdhSecret() noexcept;

		/** RETURN: true for failure */
		bool	checkNoOobConfirmation(const uint8_t* random) noexcept;

		/** */
		void	calculateSessionNonce() noexcept;

		/** */
		void	calculateSessionKey() noexcept;

		/** */
		void	generateDeviceKey(uint8_t deviceKey[16]) const noexcept;

		/** */
		void	generateRandomNumericAuthValue(uint8_t nDigits) noexcept;

		/** */
		void	generateRandomAlphaNumericAuthValue(uint8_t nCharacters) noexcept;

		/** */
		unsigned char	generateRandomAlphaNumeric() noexcept;

	private:
		/** */
		bool	processInvite(Oscl::Decoder::Api& decoder) noexcept;

		/** */
		bool	processCapabilities(Oscl::Decoder::Api& decoder) noexcept;

		/** */
		bool	processStart(Oscl::Decoder::Api& decoder) noexcept;

		/** */
		bool	processPublicKey(Oscl::Decoder::Api& decoder) noexcept;

		/** */
		bool	processInputComplete(Oscl::Decoder::Api& decoder) noexcept;

		/** */
		bool	processConfirmation(Oscl::Decoder::Api& decoder) noexcept;

		/** */
		bool	processRandom(Oscl::Decoder::Api& decoder) noexcept;

		/** */
		bool	processData(Oscl::Decoder::Api& decoder) noexcept;

		/** */
		bool	processComplete(Oscl::Decoder::Api& decoder) noexcept;

		/** */
		bool	processFailed(Oscl::Decoder::Api& decoder) noexcept;

	private:
		/** RETURN: true for failure */
		bool	sendCapabilities() noexcept;

		/** RETURN: true for failure */
		bool	sendPublicKey() noexcept;

		/** RETURN: true for failure */
		bool	sendInputComplete() noexcept;

		/** RETURN: true for failure */
		bool	sendProvisioningConfirmation() noexcept;

		/** RETURN: true for failure */
		bool	sendRandom() noexcept;

		/** RETURN: true for failure */
		bool	sendComplete() noexcept;

		/** RETURN: true for failure */
		bool	sendFailed(uint8_t errorCode) noexcept;
	};

}
}
}
}
}

#endif
