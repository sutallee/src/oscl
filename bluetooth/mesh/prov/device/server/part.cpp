/*
   Copyright (C) 2020 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <new>
#include <string.h>
#include "part.h"
#include "oscl/error/info.h"
#include "oscl/encoder/be/base.h"
#include "oscl/encoder/le/base.h"
#include "oscl/endian/decoder/linear/part.h"
#include "oscl/bluetooth/debug/print/parse.h"
#include "oscl/bluetooth/strings/module.h"
#include "oscl/entropy/rand.h"
#include "oscl/aes128/cipher.h"
#include "oscl/bluetooth/att/size.h"

using namespace Oscl::BT::Mesh::Prov::Device::Server;

static const unsigned   beaconPeriodInMs  = 5000;

//#define DEBUG_TRACE
//#define DEBUG_TRACE_ADV
//#define DEBUG_TRACE_TMI
//#define EVENT_TRACE

static const uint8_t	generalDicoverableModeEirFlags		=
							(0<<0) // LE Limited Discoverable Mode
						|	(1<<1) // LE General Discoverable Mode
						|	(1<<2) // BR/EDR not Supported
						|	(0<<3) // Simultaneous LE and BR/EDR
						;

static void	tracePbAdvPdu(
				const char*		prefix,
				Oscl::Pdu::Pdu*	pdu
				) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s:\n",
		prefix
		);

	uint8_t	buffer[128];

	unsigned
	length	= pdu->read(
				buffer,
				sizeof(buffer),
				0	// offset
				);

	Oscl::Bluetooth::Debug::Print::parseMeshProvisioningPDU(
		buffer,
		length
		);
	#endif
	}

static void	traceAdvData(
				const char*		prefix,
				Oscl::Pdu::Pdu*	pdu
				) noexcept{

	#ifdef DEBUG_TRACE_TMI
	Oscl::Error::Info::log(
		"%s:\n",
		prefix
		);

	uint8_t	buffer[128];

	unsigned
	length	= pdu->read(
				buffer,
				sizeof(buffer),
				0	// offset
				);

	Oscl::Bluetooth::Debug::Print::parseLeAdvertisingData(
		buffer,
		length
		);
	#endif
	}

Part::Part(
	ContextApi&					context,
	Oscl::BT::Mesh::
	Node::Creator::Api&			nodeCreateApi,
	Oscl::Pdu::FWD::Api&		provBearerApi,
	Oscl::Pdu::FWD::Api&		beaconBearerApi,
	Oscl::Pdu::FWD::Api&		advGattBearerApi,
	Oscl::Timer::Factory::Api&	timerFactoryApi,
	Oscl::BT::Mesh::
	Network::Iteration::Api&	networkIterator,
	const uint8_t				deviceUUID[16],
	uint16_t					oobInfo,
	bool						oobAvailable,
	uint8_t						outputOobSizeInCharacters
	) noexcept:
		_context(context),
		_nodeCreateApi(nodeCreateApi),
		_provBearerApi(provBearerApi),
		_beaconBearerApi(beaconBearerApi),
		_advGattBearerApi(advGattBearerApi),
		_timerFactoryApi(timerFactoryApi),
		_linkTxTimerApi(
			*timerFactoryApi.allocate()
			),
		_unprovTxTimerApi(
			*timerFactoryApi.allocate()
			),
		_networkIterator(networkIterator),
		_deviceUUID(deviceUUID),
		_oobInfo(oobInfo),
		_oobAvailable(oobAvailable),
		_outputOobSizeInCharacters(outputOobSizeInCharacters),
		_rxMeshProv(
			*this,
			&Part::parseMeshProvisioning
			),
		_linkTxTimerExpired(
			*this,
			&Part::linkTxTimerExpired
			),
		_unprovTxTimerExpired(
			*this,
			&Part::unprovTxTimerExpired
			),
		_freeStore(
			"PROV"
			),
		_fcsAccumulator(),
		_sessionDevice(0),
		_sessionApi(0),
		_provSessionRx(
			*this,
			&Part::provSessionRx
			),
		_currentNetworkApi(0),
		_advAlternate(false)
		{
	_unprovTxTimerApi.setExpirationCallback(
		_unprovTxTimerExpired
		);
	_linkTxTimerApi.setExpirationCallback(
		_linkTxTimerExpired
		);
	}

Part::~Part() noexcept{
	_timerFactoryApi.free(_linkTxTimerApi);
	_timerFactoryApi.free(_unprovTxTimerApi);
	}

Oscl::Frame::FWD::Api&	Part::getRxMeshProvApi() noexcept{
	return _rxMeshProv;
	}

void	Part::start() noexcept{

	for(unsigned i=0;i<nLinks;++i){
		_linkMemPool.put(&_linkMem[i]);
		}

	for(unsigned i=0;i<nDevices;++i){
		_deviceMemPool.put(&_deviceMem[i]);
		}

	_unprovTxTimerApi.start(
		beaconPeriodInMs
		);
	}

Oscl::BT::Mesh::Prov::Link::Reaper*	Part::findLink(uint32_t linkID) noexcept{
	Oscl::BT::Mesh::Prov::Link::Reaper*	link;
	for(
			link	= _linkList.first();
			link;
			link	= _linkList.next(link)
			){
		if(link->match(linkID)){
			return link;
			}
		}

	return 0;
	}

bool	Part::parseTransactionStart(
			uint32_t		linkID,
			uint8_t			transactionNumber,
			uint8_t			segN,
			const void*		frame,
			uint8_t 		length
			) noexcept{

	Oscl::BT::Mesh::Prov::Link::Reaper*
	link	=  findLink(linkID);

	if(!link){

		#ifdef DEBUG_TRACE
		Oscl::Error::Info::log(
			"%s: No such linkID: 0x%8.8X\n",
			OSCL_PRETTY_FUNCTION,
			linkID
			);
		#endif

		return true;
		}

	return link->parseTransactionStart(
					transactionNumber,
					segN,
					frame,
					length
					);
	}

bool	Part::parseTransactionAcknowledgement(
			uint32_t		linkID,
			uint8_t			transactionNumber,
			const void*		frame,
			uint8_t 		length
			) noexcept{
	Oscl::BT::Mesh::Prov::Link::Reaper*
	link	=  findLink(linkID);

	if(!link){
		/*	It is common for this to happen
			when another node is being provisioned.
		 */
		#ifdef DEBUG_TRACE
		Oscl::Error::Info::log(
			"%s: No such linkID: 0x%8.8X\n",
			OSCL_PRETTY_FUNCTION,
			linkID
			);
		#endif
		return true;
		}

	return link->parseTransactionAcknowledgement(
					transactionNumber,
					frame,
					length
					);
	}

bool	Part::parseTransactionContinuation(
			uint32_t		linkID,
			uint8_t			transactionNumber,
			uint8_t			segIndex,
			const void*		frame,
			uint8_t 		length
			) noexcept{
	Oscl::BT::Mesh::Prov::Link::Reaper*
	link	=  findLink(linkID);

	if(!link){
		/*	It is common for this to happen
			when another node is being provisioned.
		 */
		#ifdef DEBUG_TRACE
		Oscl::Error::Info::log(
			"%s: No such linkID: 0x%8.8X\n",
			OSCL_PRETTY_FUNCTION,
			linkID
			);
		#endif
		return true;
		}

	return link->parseTransactionContinuation(
					transactionNumber,
					segIndex,
					frame,
					length
					);
	return true;
	}

bool	Part::parseProvisioningBearerControlLinkOpen(
			uint32_t		linkID,
			uint8_t			transactionNumber,
			const void*		frame,
			uint8_t 		length
			) noexcept{

	if(length != 16){
		Oscl::Error::Info::log(
			"%s: length(%u) != 16\n",
			OSCL_PRETTY_FUNCTION,
			length
			);
		return false;
		}

	#ifdef DEBUG_TRACE
	const uint8_t*	deviceUUID	= (const uint8_t*)frame;

	Oscl::Error::Info::log(
		"%s:"
		" linkID: 0x%8.8X, "
		" UUID: "
		"%2.2X%2.2X%2.2X%2.2X-"
		"%2.2X%2.2X-%2.2X%2.2X-%2.2X%2.2X-"
		"%2.2X%2.2X%2.2X%2.2X%2.2X%2.2X"
		"\n",
		OSCL_PRETTY_FUNCTION,
		linkID,
		deviceUUID[0],
		deviceUUID[1],
		deviceUUID[2],
		deviceUUID[3],
		deviceUUID[4],
		deviceUUID[5],
		deviceUUID[6],
		deviceUUID[7],
		deviceUUID[8],
		deviceUUID[9],
		deviceUUID[10],
		deviceUUID[11],
		deviceUUID[12],
		deviceUUID[13],
		deviceUUID[14],
		deviceUUID[15]
		);
	#endif

	if(memcmp(frame,_deviceUUID,16)){
		// Not for our device UUID

		#ifdef DEBUG_TRACE
		Oscl::Error::Info::log(
			"%s: NOT MY UUID\n",
			OSCL_PRETTY_FUNCTION
			);
		#endif

		return true;
		}

	if(findLink(linkID)){

		#ifdef DEBUG_TRACE
		Oscl::Error::Info::log(
			"%s: Link already exists.\n",
			OSCL_PRETTY_FUNCTION
			);
		#endif

		sendProvisioningBearerControlACK(linkID,transactionNumber);
		return true;
		}

	if(_sessionApi){
		Oscl::Error::Info::log(
			"%s: Session already active.\n",
			OSCL_PRETTY_FUNCTION
			);
		return true;
		}

	DeviceMem*
	deviceMem	= _deviceMemPool.get();

	if(!deviceMem){
		Oscl::Error::Info::log(
			"%s: out of device memory.\n",
			OSCL_PRETTY_FUNCTION
			);
		return true;
		}

	LinkMem*
	linkMem	= _linkMemPool.get();

	if(!linkMem){
		_deviceMemPool.put(deviceMem);
		Oscl::Error::Info::log(
			"%s: out of link memory.\n",
			OSCL_PRETTY_FUNCTION
			);
		return true;
		}

	Oscl::BT::Mesh::Prov::Link::Reaper*
	link	= new(linkMem)
				Oscl::BT::Mesh::Prov::Link::Reaper(
					*this,
					_provBearerApi,
					_linkTxTimerApi,
					_provSessionRx,
					linkID,
					false		// Not provisioner... Device
					);

	_linkList.put(link);

	link->start();

	_sessionDevice	= new(deviceMem)
						Oscl::BT::Mesh::Prov::Device::Reaper(
							*this,
							_freeStore,
							link->getTxApi(),
							_oobAvailable,
							_outputOobSizeInCharacters
							);

	_sessionDevice->start();

	_sessionApi	= &_sessionDevice->getRxApi();

	sendProvisioningBearerControlACK(linkID,transactionNumber);

	return true;
	}

void	Part::sendProvisioningBearerControlLinkOpen(
			uint32_t	linkID,
			uint8_t		transactionNumber,
			uint8_t		deviceUUID[16]
			) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s: linkID: 0x%8.8X, transactionNumber: 0x%2.2X\n",
		OSCL_PRETTY_FUNCTION,
		linkID,
		transactionNumber
		);
	#endif

	unsigned	bufferSize;

	void*
	bufferMem	= _freeStore.allocBuffer(bufferSize);

	if(!bufferMem){
		Oscl::Error::Info::log(
			"%s: out of buffer memory.\n",
			OSCL_PRETTY_FUNCTION
			);
		return;
		}

	Oscl::Handle<Oscl::Pdu::Fixed>	fixed;
		{
		Oscl::Encoder::BE::Base
		beEncoder(
			bufferMem,
			bufferSize
			);

		Oscl::Encoder::Api&		encoder	= beEncoder;

		static const	uint8_t	typeSeg	=
											(0x03<<0)	// type: Provisioning Bearer Control
										|	(0x00<<2)	// opcode: OPEN
										;	// typeSeg

		encoder.encode(linkID);
		encoder.encode(transactionNumber);
		encoder.encode(typeSeg);
		encoder.copyIn(deviceUUID,16);

		fixed	= _freeStore.allocFixed(
					bufferMem,
					encoder.length()
					);
		}

	if(!fixed){
		_freeStore.freeBuffer(bufferMem);
		Oscl::Error::Info::log(
			"%s: out of fxed memory.\n",
			OSCL_PRETTY_FUNCTION
			);
		return;
		}

	tracePbAdvPdu(
		OSCL_PRETTY_FUNCTION,
		fixed
		);

	_provBearerApi.transfer(fixed);
	}

void	Part::sendProvisioningBearerControlACK(
			uint32_t	linkID,
			uint8_t		transactionNumber
			) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s: linkID: 0x%8.8X, transactionNumber: 0x%2.2X\n",
		OSCL_PRETTY_FUNCTION,
		linkID,
		transactionNumber
		);
	#endif

	unsigned	bufferSize;

	void*
	bufferMem	= _freeStore.allocBuffer(bufferSize);

	if(!bufferMem){
		Oscl::Error::Info::log(
			"%s: out of buffer memory.\n",
			OSCL_PRETTY_FUNCTION
			);
		return;
		}

	Oscl::Handle<Oscl::Pdu::Fixed>	fixed;
		{
		Oscl::Encoder::BE::Base
		beEncoder(
			bufferMem,
			bufferSize
			);
		Oscl::Encoder::Api&		encoder	= beEncoder;

		static const	uint8_t	typeSeg	=
											(0x03<<0)	// type: Provisioning Bearer Control
										|	(0x01<<2)	// opcode: ACK
										;	// typeSeg

		encoder.encode(linkID);
		encoder.encode(transactionNumber);
		encoder.encode(typeSeg);

		fixed	= _freeStore.allocFixed(
					bufferMem,
					encoder.length()
					);

		}

	if(!fixed){
		_freeStore.freeBuffer(bufferMem);
		Oscl::Error::Info::log(
			"%s: out of fragment memory.\n",
			OSCL_PRETTY_FUNCTION
			);
		return;
		}

	tracePbAdvPdu(
		OSCL_PRETTY_FUNCTION,
		fixed
		);

	_provBearerApi.transfer(fixed);
	}

bool	Part::parseProvisioningBearerControlLinkClose(
			uint32_t		linkID,
			uint8_t			transactionNumber,
			const void*		frame,
			uint8_t 		length
			) noexcept{

	if(length != 1){
		Oscl::Error::Info::log(
			"%s: length(%u) != 16\n",
			OSCL_PRETTY_FUNCTION,
			length
			);
		return false;
		}

	#ifdef DEBUG_TRACE
	const uint8_t*	p	= (const uint8_t*)frame;

	uint8_t
	reason	= p[0];

	Oscl::Error::Info::log(
		"%s: reason: (0x%2.2X) \"%s\"\n",
		OSCL_PRETTY_FUNCTION,
		reason,
		Oscl::Bluetooth::Strings::gpcfLinkCloseReason(reason)
		);
	#endif

	Oscl::BT::Mesh::Prov::Link::Reaper*
	link	= findLink(linkID);

	if(!link){

		#ifdef DEBUG_TRACE
		Oscl::Error::Info::log(
			"%s: No such link.\n",
			OSCL_PRETTY_FUNCTION
			);
		#endif

		return true;
		}

	_linkList.remove(link);

	link->stop();

	if(_sessionDevice){
		_sessionDevice->stop();
		}

	return true;
	}

bool	Part::parseProvisioningBearerControl(
			uint32_t		linkID,
			uint8_t			transactionNumber,
			uint8_t			bearerOpcode,
			const void*		frame,
			uint8_t 		length
			) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s: linkID: 0x%8.8X, transactionNumber: 0x%2.2X, bearerOpcode: 0x%2.2X \"%s\"\n",
		OSCL_PRETTY_FUNCTION,
		linkID,
		transactionNumber,
		bearerOpcode,
		Oscl::Bluetooth::Strings::gpcfBearerOpcode(bearerOpcode)
		);
	#endif

	switch(bearerOpcode){
		case 0x00:
			// Link Open
			return parseProvisioningBearerControlLinkOpen(
					linkID,
					transactionNumber,
					frame,
					length
					);
		case 0x01:
			// Link ACK
			Oscl::Error::Info::log(
				"%s: unexpected Link ACK\n",
				OSCL_PRETTY_FUNCTION
				);
			return false;
		case 0x02:
			// Link Close
			return parseProvisioningBearerControlLinkClose(
					linkID,
					transactionNumber,
					frame,
					length
					);
		default:
			return false;
		}
	}

bool	Part::parseMeshProvisioning(
			const void*		frame,
			unsigned 		length
			) noexcept{
	// <<PB-ADV>>

	const uint8_t*	p	= (const uint8_t*)frame;

	Oscl::Endian::Decoder::Linear::Part
	beDecoder(
		frame,
		length
		);

	Oscl::Decoder::Api& decoder	= beDecoder.be();

	uint32_t	linkID;
	uint8_t		transactionNumber;

	decoder.decode(linkID);
	decoder.decode(transactionNumber);

	// Generic Provisioning PDU

	// Generic Provisioning Control
	uint8_t	typeSeg;
	decoder.decode(typeSeg);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow\n",
			OSCL_PRETTY_FUNCTION
			);
		return false;
		}

	uint8_t	type	= (typeSeg & 0x3);
	uint8_t	seg		= (typeSeg >> 2);

	unsigned
	headerSize	=		sizeof(linkID)
					+	sizeof(transactionNumber)
					+	sizeof(typeSeg)
					;

	// Generic Provisioning Payload
	switch(type){
		case 0x00:
			// Transaction Start
			return parseTransactionStart(
					linkID,
					transactionNumber,
					seg,
					&p[headerSize],
					decoder.remaining()
					);
			break;
		case 0x01:
			// Transaction Acknowledgement
			return parseTransactionAcknowledgement(
					linkID,
					transactionNumber,
					&p[headerSize],
					decoder.remaining()
					);
			break;
		case 0x02:
			// Transaction Continuation
			return parseTransactionContinuation(
					linkID,
					transactionNumber,
					seg,
					&p[headerSize],
					decoder.remaining()
					);
		case 0x03:
			// Provisioning Bearer Control
			return parseProvisioningBearerControl(
					linkID,
					transactionNumber,
					seg,
					&p[headerSize],
					decoder.remaining()
					);
		default:
			Oscl::Error::Info::log(
				"%s: Unrecognized type: 0x%2.2X\n",
				OSCL_PRETTY_FUNCTION,
				type
				);
			return false;
		}
	}

bool	Part::provSessionRx(
				const void*		frame,
				unsigned int	length
				) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif

	if(!_sessionApi){
		Oscl::Error::Info::log(
			"%s: NO Session!\n",
			OSCL_PRETTY_FUNCTION
			);
		return true;
		}

	_sessionApi->forward(frame,length);

	return true;
	}

void	Part::linkTxTimerExpired() noexcept{

	#if defined(DEBUG_TRACE) || defined(EVENT_TRACE)
	Oscl::Error::Info::log(
		"***************** %s\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif

	// FIXME:
	// This is just wierd, but there is only
	// ever one link... I think.
	for(
		Oscl::BT::Mesh::Prov::Link::Reaper*	link = _linkList.first();
		link;
		link	= _linkList.next(link)
		){
			link->txTimerExpired();
		}
	}

void	Part::unprovTxTimerExpired() noexcept{

	#if defined(DEBUG_TRACE_ADV) || defined(EVENT_TRACE)
	Oscl::Error::Info::log(
		"***************** %s\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif

	if(_nodeCreateApi.gattConnectionIsActive() || _sessionApi){
		// In principle, there is no need to send
		// connectable advertisements if we're
		// already connected or if we are currently holding
		// a provisioning session.
		// Connectable advertisments consume a significant
		// amount of advertising time causing us to miss
		// transmit opportunities.
		_unprovTxTimerApi.restart(beaconPeriodInMs);
		return;
		}

	if(_nodeCreateApi.isProvisioned()){
		/*	Advertising 7.2.2.2.1
			The server shall use the GAP General Discoverable mode with
			connectable undirected advertising events in the format
			described in Table 7.6 below.
		 */

		/* [Proxy] Advertising 7.2.2.2.1

			A node that supports the Proxy feature and has
			the Proxy feature enabled shall support advertising with
			Network ID.

			A node that does not support the Proxy feature or has
			the Proxy feature disabled shall not advertise with
			Network ID.

			A node may advertise with Node Identity regardless of
			the Proxy feature being supported or enabled.

			The Identification Parameters for each Identification
			Type is defined in the subsections below.

			The «Mesh Proxy Service» shall be included in the Incomplete
			List of 16-bit Service UUIDs or the Complete List of 16-bit Service UUIDs.
		 */

		if(_nodeCreateApi.hasEnabledProxy() && !_nodeCreateApi.gattConnectionIsActive()){

			if(_advAlternate){
				/*	Advertising with Node Identity 7.2.2.2.3

					Advertising using the Node Identity is used to identify
					a node based on the unicast address of the primary
					element and the network key of a subnet to which the
					node belongs. This can be useful when large amounts
					of data need to be delivered to a node via GATT for
					cases when the node cannot be easily identified or
					is not advertising. This advertisement may be used to
					initiate a GATT connection to the selected node.

					If both PB-GATT and Mesh Proxy Service are supported,
					immediately after provisioning is completed using
					PB-GATT (see Section 5.2.2), the Mesh Proxy Service
					shall start advertising with Node Identity using the
					provisioned network.

					When the server starts advertising as a result of user
					interaction, the server shall interleave the advertising
					of each subnet it is a member of.

					When the server starts advertising as a result of the Node
					Identity state being enabled, the server shall only advertise
					using the subnet that it was enabled on. The duration of
					advertising in these two cases is limited to 60 seconds.

					The format of the Service Data for the «Mesh Proxy Service»
					when Advertising with Node Identity is defined in Table 7.10
					and illustrated in Figure 7.3.
				 */
				if(!_nodeCreateApi.gattConnectionIsActive()){
					sendMeshProxyServiceDataWithNodeIdentity();
					}
				}
			else {
				/*	Advertising with Network ID 7.2.2.2.2

					The format of the Service Data for the «Mesh Proxy Service» when
					Advertising with the Network ID is defined in Table 7.9 and
					illustrated in Figure 7.2.
					When a server is a member of multiple subnets, it shall interleave
					the advertising of each subnet.
				 */
				sendMeshProxyServiceDataWithNetworkID();
				}

			_advAlternate	= _advAlternate?false:true;

			}
		_unprovTxTimerApi.restart(beaconPeriodInMs);
		return;
		}

	sendUnprovisionedBeacon();

	if(!_nodeCreateApi.gattConnectionIsActive()){
		sendMeshProvisioningServiceData();
		}

	_unprovTxTimerApi.restart(beaconPeriodInMs);
	}

void	Part::stopped(Oscl::BT::Mesh::Prov::Link::Reaper& link) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif

	LinkMem*	linkMem	= (LinkMem*)&link;
	_linkMemPool.put(linkMem);
	}

void	Part::stopped(Oscl::BT::Mesh::Prov::Device::Reaper& device) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif

	DeviceMem*	deviceMem	= (DeviceMem*)&device;
	_deviceMemPool.put(deviceMem);

	_sessionApi		= 0;
	_sessionDevice	= 0;
	}

bool	Part::createNewNode(
			const uint8_t	netKey[16],
			const uint8_t	deviceKey[16],
			uint16_t		keyIndex,
			uint8_t			flags,
			uint32_t		ivIndex,
			uint16_t		unicastAddress
			) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s:\n"
		" Net Key: \n"
		"  %2.2X %2.2X %2.2X %2.2X %2.2X %2.2X %2.2X %2.2X\n"
		"  %2.2X %2.2X %2.2X %2.2X %2.2X %2.2X %2.2X %2.2X\n"
		" KeyIndex: 0x%4.4X\n"
		" Flags: 0x%2.2X\n"
		" IV Index: 0x%8.8X\n"
		" Unicast Address: 0x%4.4X\n"
		"",
		OSCL_PRETTY_FUNCTION,
		netKey[0], netKey[1], netKey[2], netKey[3],
		netKey[4], netKey[5], netKey[6], netKey[7],
		netKey[8], netKey[9], netKey[10], netKey[11],
		netKey[12], netKey[13], netKey[14], netKey[15],
		keyIndex,
		flags,
		ivIndex,
		unicastAddress
		);
	#else
	Oscl::Error::Info::log(
		"New Network Added:\n"
		" KeyIndex: 0x%4.4X\n"
		" Unicast Address: 0x%4.4X\n"
		"",
		keyIndex,
		unicastAddress
		);
	#endif

	bool
	failed	= _nodeCreateApi.createNewNode(
				netKey,
				deviceKey,
				keyIndex,
				flags,
				ivIndex,
				unicastAddress
				);

	return failed;
	}

uint8_t	Part::getNumElements() const noexcept{
	return _nodeCreateApi.getNumElements();
	}

void	Part::setAttentionTimer(uint8_t nSeconds) noexcept{
	_context.setAttentionTimer(nSeconds);
	}

bool	Part::oobOutputActionBlinkIsSupported() const noexcept{
	return _context.oobOutputActionBlinkIsSupported();
	}

bool	Part::oobOutputActionBeepIsSupported() const noexcept{
	return _context.oobOutputActionBeepIsSupported();
	}

bool	Part::oobOutputActionVibrateIsSupported() const noexcept{
	return _context.oobOutputActionVibrateIsSupported();
	}

bool	Part::oobOutputActionNumericIsSupported() const noexcept{
	return _context.oobOutputActionNumericIsSupported();
	}

bool	Part::oobOutputActionAlphaNumericIsSupported() const noexcept{
	return _context.oobOutputActionAlphaNumericIsSupported();
	}

void	Part::oobOutputBlink(uint32_t n) noexcept{
	_context.oobOutputBlink(n);
	}

void	Part::oobOutputBeep(uint32_t n) noexcept{
	_context.oobOutputBeep(n);
	}

void	Part::oobOutputVibrate(uint32_t n) noexcept{
	_context.oobOutputVibrate(n);
	}

void	Part::oobOutputNumeric(uint32_t n) noexcept{
	_context.oobOutputNumeric(n);
	}

void	Part::oobOutputAlphaNumeric(const char* s) noexcept{
	_context.oobOutputAlphaNumeric(s);
	}

void	Part::provisioningComplete() noexcept{
	// Nothing to do for PB-ADV bearer.
	_context.provisioningStopped();
	}

void	Part::sendUnprovisionedBeacon() noexcept{

	#ifdef DEBUG_TRACE_ADV
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif

	unsigned	bufferSize;

	void*
	bufferMem	= _freeStore.allocBuffer(bufferSize);

	if(!bufferMem){
		Oscl::Error::Info::log(
			"%s: out of buffer memory.\n",
			OSCL_PRETTY_FUNCTION
			);
		return;
		}

	Oscl::Handle<Oscl::Pdu::Fixed>	fixed;
		{
		Oscl::Encoder::BE::Base
		beEncoder(
			bufferMem,
			bufferSize
			);

		Oscl::Encoder::Api&		encoder	= beEncoder;

		static const	uint8_t		type	=	0x00; // Unprovisioned Device Beacon
		encoder.encode(type);
		encoder.copyIn(
			_deviceUUID,
			16
			);
		encoder.encode(_oobInfo);

		fixed	= _freeStore.allocFixed(
					bufferMem,
					encoder.length()
					);
		}

	if(!fixed){
		_freeStore.freeBuffer(bufferMem);
		Oscl::Error::Info::log(
			"%s: out of fragment memory.\n",
			OSCL_PRETTY_FUNCTION
			);
		return;
		}

	_beaconBearerApi.transfer(fixed);
	}

void	Part::sendMeshProvisioningServiceData() noexcept{

	#ifdef DEBUG_TRACE_ADV
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif

	unsigned	bufferSize;

	void*
	bufferMem	= _freeStore.allocBuffer(bufferSize);

	if(!bufferMem){
		Oscl::Error::Info::log(
			"%s: out of buffer memory.\n",
			OSCL_PRETTY_FUNCTION
			);
		return;
		}

	Oscl::Handle<Oscl::Pdu::Fixed>	fixed;
		{
		Oscl::Encoder::BE::Base
		leEncoder(
			bufferMem,
			bufferSize
			);

		Oscl::Encoder::Api&		encoder	= leEncoder;

		static const	uint8_t		eirFlagsLen		=	0x02;
		static const	uint8_t		eirFlagsType	=	0x01; // Flags
		static const	uint8_t		eirFlags		=
										(1<<0) // LE Limited Discoverable Mode
									|	(0<<1) // LE General Discoverable Mode
									|	(1<<2) // BR/EDR not Supported
									|	(0<<3) // Simultaneous LE and BR/EDR
									;
		encoder.encode(eirFlagsLen);
		encoder.encode(eirFlagsType);
		encoder.encode(eirFlags);

		static const	uint8_t		serviceClassLen		=	0x03;
		static const	uint8_t		serviceClassType	=	0x03; // <<Complete List of 16-bit Service Class UUIDs>>
		static const	uint16_t	provServiceClassUUID	=	0x2718; // <<Mesh Provisioning Service>> Endian-BIG
		encoder.encode(serviceClassLen);
		encoder.encode(serviceClassType);
		encoder.encode(provServiceClassUUID);

		static const	uint8_t		serviceDataLen		=	0x15;
		static const	uint8_t		serviceDataType		=	0x16; // <<Service Data>
		encoder.encode(serviceDataLen);
		encoder.encode(serviceDataType);
		encoder.encode(provServiceClassUUID);

		encoder.copyIn(
			_deviceUUID,
			16
			);

		encoder.encode(_oobInfo);

		fixed	= _freeStore.allocFixed(
					bufferMem,
					encoder.length()
					);

		}

	if(!fixed){
		_freeStore.freeBuffer(bufferMem);
		Oscl::Error::Info::log(
			"%s: out of fragment memory.\n",
			OSCL_PRETTY_FUNCTION
			);
		return;
		}

	traceAdvData(
		OSCL_PRETTY_FUNCTION,
		fixed
		);

	_advGattBearerApi.transfer(fixed);
	}

void	Part::sendMeshProxyServiceDataWithNetworkID() noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif

	if(_currentNetworkApi){
		_currentNetworkApi	= _networkIterator.next(_currentNetworkApi);
		}

	if(!_currentNetworkApi) {
		_currentNetworkApi	= _networkIterator.first();
		}

	if(!_currentNetworkApi){
		// There are no networks.
		Oscl::Error::Info::log(
			"%s: !_currentNetworkApi\n",
			OSCL_PRETTY_FUNCTION
			);
		return;
		}

	unsigned	bufferSize;

	void*
	bufferMem	= _freeStore.allocBuffer(bufferSize);

	if(!bufferMem){
		Oscl::Error::Info::log(
			"%s: out of buffer memory.\n",
			OSCL_PRETTY_FUNCTION
			);
		return;
		}

	Oscl::Handle<Oscl::Pdu::Fixed>	fixed;
		{
		Oscl::Encoder::BE::Base
		leEncoder(
			bufferMem,
			bufferSize
			);

		Oscl::Encoder::Api&		encoder	= leEncoder;

		static const	uint8_t		eirFlagsLen		=	0x02;
		static const	uint8_t		eirFlagsType	=	0x01; // Flags

		encoder.encode(eirFlagsLen);
		encoder.encode(eirFlagsType);
		encoder.encode(generalDicoverableModeEirFlags);

		static const	uint8_t		serviceClassLen		=	0x03;
		static const	uint8_t		serviceClassType	=	0x03; // <<Complete List of 16-bit Service Class UUIDs>>
		static const	uint16_t	proxyServiceClassUUID	=	0x2818; // <<Mesh Proxy Service>> Endian-BIG
		encoder.encode(serviceClassLen);
		encoder.encode(serviceClassType);
		encoder.encode(proxyServiceClassUUID);

		static const	uint8_t		serviceDataLen		=	0x0C;
		static const	uint8_t		serviceDataType		=	0x16; // <<Service Data>>
		static const	uint8_t		identificationType	=	0x00; // Network ID type
		encoder.encode(serviceDataLen);
		encoder.encode(serviceDataType);
		encoder.encode(proxyServiceClassUUID);
		encoder.encode(identificationType);

		encoder.copyIn(
			_currentNetworkApi->getNetworkID(),
			8
			);

		#ifdef DEBUG_TRACE
		Oscl::Error::Info::hexDump(
			bufferMem,
			encoder.length()
			);
		#endif

		fixed	= _freeStore.allocFixed(
					bufferMem,
					encoder.length()
					);
		}

	if(!fixed){
		_freeStore.freeBuffer(bufferMem);
		Oscl::Error::Info::log(
			"%s: out of fragment memory.\n",
			OSCL_PRETTY_FUNCTION
			);
		return;
		}

	traceAdvData(
		OSCL_PRETTY_FUNCTION,
		fixed
		);

	_advGattBearerApi.transfer(fixed);
	}

/* Advertising with Node Identity 7.2.2.2.3

	Advertising using the Node Identity is used to identify a node based
	on the unicast address of the primarelement and the network key of a
	subnet to which the node belongs. This can be useful when large
	amounts of data need to be delivered to a node via GATT for cases
	when the node cannot be easily identified or is not advertising.
	This advertisement may be used to initiate a GATT connection to the
	selected node.

	If both PB-GATT and Mesh Proxy Service are supported, immediately
	after provisioning is completed using PB-GATT (see Section 5.2.2),
	the Mesh Proxy Service shall start advertising with Node Identity
	using the provisioned network.

	When the server starts advertising as a result of user interaction,
	the server shall interleave the advertising of each subnet it is
	a member of. 

	When the server starts advertising as a result of the Node Identity
	state being enabled, the server shall only advertise using the subnet
	that it was enabled on.

	The duration of advertising in these two cases is limited to 60 seconds.
	The format of the Service Data for the «Mesh Proxy Service» when
	Advertising with Node Identity is defined in Table 7.10 and
	illustrated in Figure 7.3.

 */
void	Part::sendMeshProxyServiceDataWithNodeIdentity() noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif

	// FIXME: 
	if(_currentNetworkApi){
		_currentNetworkApi	= _networkIterator.next(_currentNetworkApi);
		}

	if(!_currentNetworkApi) {
		_currentNetworkApi	= _networkIterator.first();
		}

	if(!_currentNetworkApi){
		// There are no networks.
		Oscl::Error::Info::log(
			"%s: !_currentNetworkApi\n",
			OSCL_PRETTY_FUNCTION
			);
		return;
		}

	unsigned	bufferSize;

	void*
	bufferMem	= _freeStore.allocBuffer(bufferSize);

	if(!bufferMem){
		Oscl::Error::Info::log(
			"%s: out of buffer memory.\n",
			OSCL_PRETTY_FUNCTION
			);
		return;
		}

	Oscl::Handle<Oscl::Pdu::Fixed>	fixed;
		{
		Oscl::Encoder::BE::Base
		leEncoder(
			bufferMem,
			bufferSize
			);

		Oscl::Encoder::Api&		encoder	= leEncoder;

		static const	uint8_t		eirFlagsLen		=	0x02;
		static const	uint8_t		eirFlagsType	=	0x01; // Flags

		encoder.encode(eirFlagsLen);
		encoder.encode(eirFlagsType);
		encoder.encode(generalDicoverableModeEirFlags);

		static const	uint8_t		serviceClassLen		=	0x03;
		static const	uint8_t		serviceClassType	=	0x03; // <<Complete List of 16-bit Service Class UUIDs>>
		static const	uint16_t	proxyServiceClassUUID	=	0x2818; // <<Mesh Proxy Service>> Endian-BIG

		encoder.encode(serviceClassLen);
		encoder.encode(serviceClassType);
		encoder.encode(proxyServiceClassUUID);

		static const	uint8_t		serviceDataLen		=	0x14;
		static const	uint8_t		serviceDataType		=	0x16; // <<Service Data>
		static const	uint8_t		identificationType	=	0x01; // Node Identity type

		uint8_t	buffer[16];

		uint8_t	hash[8];

		memset(
			&buffer[0],
			0,
			6	// padding length
			);

		uint8_t	random[8];

		Oscl::Entropy::Random::fetch(
			random,
			sizeof(random)
			);

		memcpy(
			&buffer[6],
			random,
			sizeof(random)
			);

		uint16_t
		unicastAddress	= _currentNetworkApi->getUnicastAddress();
			{
			Oscl::Encoder::BE::Base
			bufferEncoder(
				&buffer[14],
				2
				);

			Oscl::Encoder::Api&	encoderApi	= bufferEncoder;

			encoderApi.encode(unicastAddress);
			}

		Oscl::AES128::Cipher	ecb(_currentNetworkApi->getIdentityKey());

		ecb.cipher(buffer);

		ecb.copyOut(
			hash,
			sizeof(hash),	// len
			8				// offset (mod 64)
			);

		encoder.encode(serviceDataLen);
		encoder.encode(serviceDataType);
		encoder.encode(proxyServiceClassUUID);
		encoder.encode(identificationType);

		encoder.copyIn(
			hash,
			sizeof(hash)
			);

		encoder.copyIn(
			random,
			sizeof(random)
			);

		#ifdef DEBUG_TRACE
		Oscl::Error::Info::log(
			"%s: \n"
			"\tunicastAddress: 0x%4.4X\n"
			"\trandom:\n"
			"",
			OSCL_PRETTY_FUNCTION,
			unicastAddress
			);

		Oscl::Error::Info::hexDump(
			random,
			sizeof(random)
			);
		Oscl::Error::Info::log(
			"\thash:\n"
			);
		Oscl::Error::Info::hexDump(
			hash,
			sizeof(hash)
			);

		Oscl::Error::Info::log(
			"\tadvertisment:\n"
			);

		Oscl::Error::Info::hexDump(
			bufferMem,
			encoder.length()
			);
	#endif

		fixed	= _freeStore.allocFixed(
					bufferMem,
					encoder.length()
					);
		}

	if(!fixed){
		_freeStore.freeBuffer(bufferMem);
		Oscl::Error::Info::log(
			"%s: out of fragment memory.\n",
			OSCL_PRETTY_FUNCTION
			);
		return;
		}

	traceAdvData(
		OSCL_PRETTY_FUNCTION,
		fixed
		);

	_advGattBearerApi.transfer(fixed);
	}

