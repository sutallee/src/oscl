/*
   Copyright (C) 2020 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "service.h"

using namespace Oscl::BT::Mesh::Prov::Device::Server::ITC;

Service::Service(
	Oscl::BT::Mesh::Prov::Device::
	Server::Part::ContextApi&		context,
	Oscl::BT::Mesh::
	Node::Creator::Api&				nodeCreateApi,
	Oscl::Mt::Itc::PostMsgApi&		papi,
	Oscl::Pdu::FWD::Api&			provBearerApi,
	Oscl::Pdu::FWD::Api&			beaconBearerApi,
	Oscl::Pdu::FWD::Api&			advGattBearerApi,
	Oscl::Timer::Factory::Api&		timerFactoryApi,
	Oscl::BT::Mesh::
	Network::Iteration::Api&		networkIterator,
	const uint8_t					deviceUUID[16],
	uint16_t						oobInfo,
	bool							oobAvailable,
	uint8_t							outputOobSizeInCharacters
	) noexcept:
		_papi(papi),
		_openSync(
			*this,
			papi
			),
		_part(
			context,
			nodeCreateApi,
			provBearerApi,
			beaconBearerApi,
			advGattBearerApi,
			timerFactoryApi,
			networkIterator,
			deviceUUID,
			oobInfo,
			oobAvailable,
			outputOobSizeInCharacters
			)
		{
	}

Oscl::Frame::FWD::Api&	Service::getRxMeshProvApi() noexcept{
	return _part.getRxMeshProvApi();
	}

Oscl::Mt::Itc::Srv::OpenSyncApi&	Service::getOpenSyncApi() noexcept{
	return _openSync;
	}

void	Service::request(Oscl::Mt::Itc::Srv::Open::Req::Api::OpenReq& msg) noexcept{

	_part.start();

	msg.returnToSender();
	}

