/*
   Copyright (C) 2020 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_bluetooth_mesh_prov_server_parth_
#define _oscl_bluetooth_mesh_prov_server_parth_

#include <stdint.h>
#include "oscl/memory/block.h"
#include "oscl/done/operation.h"
#include "oscl/frame/fwd/composer.h"
#include "oscl/pdu/fwd/api.h"
#include "oscl/queue/queue.h"
#include "oscl/stream/hdlc/fcs/fcs8.h"
#include "oscl/bluetooth/mesh/prov/link/reaper.h"
#include "oscl/bluetooth/mesh/prov/device/reaper.h"
#include "oscl/endian/decoder/api.h"
#include "oscl/pdu/memory/config.h"
#include "oscl/bluetooth/mesh/node/creator/api.h"
#include "oscl/bluetooth/mesh/prov/client/reaper.h"
#include "oscl/iterator/api.h"
#include "oscl/bluetooth/mesh/network/iteration/api.h"
#include "oscl/timer/factory/api.h"
#include "oscl/bluetooth/mesh/provisioner/network/api.h"

/** */
namespace Oscl {
/** */
namespace BT {
/** */
namespace Mesh {
/** */
namespace Prov {
/** */
namespace Server {

/** */
class Part :
	private Oscl::BT::Mesh::Prov::Device::ContextApi,
	private Oscl::BT::Mesh::Prov::Client::ContextApi,
	private Oscl::BT::Mesh::Prov::Link::ContextApi
	{
	public:
		/** */
		class ContextApi {
			public:
				/** */
				virtual void	setAttentionTimer(uint8_t nSeconds) noexcept=0;

				/** */
				virtual bool	oobOutputActionBlinkIsSupported() const noexcept=0;

				/** */
				virtual bool	oobOutputActionBeepIsSupported() const noexcept=0;

				/** */
				virtual bool	oobOutputActionVibrateIsSupported() const noexcept=0;

				/** */
				virtual bool	oobOutputActionNumericIsSupported() const noexcept=0;

				/** */
				virtual bool	oobOutputActionAlphaNumericIsSupported() const noexcept=0;

				/** */
				virtual void	oobOutputBlink(uint32_t n) noexcept=0;

				/** */
				virtual void	oobOutputBeep(uint32_t n) noexcept=0;

				/** */
				virtual void	oobOutputVibrate(uint32_t n) noexcept=0;

				/** */
				virtual void	oobOutputNumeric(uint32_t n) noexcept=0;

				/** */
				virtual void	oobOutputAlphaNumeric(const char* s) noexcept=0;

				/** */
				virtual void	provisioningStopped() noexcept=0;

				/** */
				virtual Oscl::BT::Mesh::Provisioner::Network::Api&	getProvNetworkApi() noexcept=0;
			};

	private:
		/** */
		ContextApi&					_context;

		/** */
		Oscl::BT::Mesh::
		Node::Creator::Api&			_nodeCreateApi;

		/** */
		Oscl::Pdu::FWD::Api&		_provBearerApi;

		/** */
		Oscl::Pdu::FWD::Api&		_beaconBearerApi;

		/** */
		Oscl::Pdu::FWD::Api&		_advGattBearerApi;

		/** */
		Oscl::Timer::Factory::Api&	_timerFactoryApi;

		/** */
		Oscl::Timer::Api&			_linkTxTimerApi;

		/** */
		Oscl::Timer::Api&			_unprovTxTimerApi;

		/** */
		Oscl::BT::Mesh::
		Network::Iteration::Api&	_networkIterator;

		/** */
		const uint8_t*				_deviceUUID;

		/** */
		const uint16_t				_oobInfo;

		/** */
		const bool					_oobAvailable;

		/** */
		const uint8_t				_outputOobSizeInCharacters;

	private:
		/** */
		Oscl::Frame::FWD::Composer<Part>	_rxMeshProv;

	private:
		/** */
		Oscl::Done::Operation<Part>			_linkTxTimerExpired;

	private:
		/** */
		Oscl::Done::Operation<Part>			_unprovTxTimerExpired;

	private:
		/** */
		constexpr static unsigned	bufferSize	= 128;
		/** */
		constexpr static unsigned	nBuffers	= 32;
		/** */
		constexpr static unsigned	nFixed		= 32;
		/** */
		constexpr static unsigned	nFragment	= 32;
		/** */
		constexpr static unsigned	nComposite	= 32;

		/** */
		Oscl::Pdu::Memory::
		Config<
			bufferSize,
			nBuffers,
			nFixed,
			nFragment,
			nComposite
			>								_freeStore;

	private:
		/** */
		Oscl::HQueue<Oscl::Pdu::Pdu>	_txQ;

		/** */
		Oscl::Handle<Oscl::Pdu::Pdu>	_currentTx;

	private:
		/** */
		Oscl::Stream::HDLC::FCS8::Accumulator				_fcsAccumulator;

	private:
		/** */
		Oscl::Queue<Oscl::BT::Mesh::Prov::Link::Reaper>		_linkList;

		/** */
		union LinkMem {                   
			void*   __qitemlink;
			Oscl::Memory::AlignedBlock<sizeof(Oscl::BT::Mesh::Prov::Link::Reaper)>	mem;
			};

		/** */
		constexpr static unsigned	nLinks	= 1;

		/** */
		LinkMem					_linkMem[nLinks];

		/** */
		Oscl::Queue<LinkMem>	_linkMemPool;

	private:
		/** */
		union DeviceMem {                   
			void*   __qitemlink;
			Oscl::Memory::AlignedBlock<sizeof(Oscl::BT::Mesh::Prov::Device::Reaper)>	mem;
			};

		/** */
		constexpr static unsigned	nDevices	= 1;

		/** */
		DeviceMem					_deviceMem[nDevices];

		/** */
		Oscl::Queue<DeviceMem>	_deviceMemPool;

		/** */
		Oscl::BT::Mesh::Prov::Device::Reaper*		_sessionDevice;

	private:
		/** */
		union ClientMem {                   
			void*   __qitemlink;
			Oscl::Memory::AlignedBlock<sizeof(Oscl::BT::Mesh::Prov::Client::Reaper)>	mem;
			};

		/** */
		constexpr static unsigned	nClients	= 1;

		/** */
		ClientMem					_clientMem[nClients];

		/** */
		Oscl::Queue<ClientMem>		_clientMemPool;

		/** */
		Oscl::BT::Mesh::Prov::Client::Reaper*		_sessionClient;

	private:
		/** May reference either a device or a provisioner client.
		 */
		Oscl::Frame::FWD::Api*					_sessionApi;

	private:
		/** DUMMY stand-in for provisining sequencer client*/
		Oscl::Frame::FWD::Composer<Part>		_provSessionRx;

	private:
		/** */
		Oscl::BT::Mesh::Network::Api*			_networkApiHack;

	private:
		/** */
		Oscl::BT::Mesh::Network::Api*			_currentNetworkApi;

		/** */
		bool									_advAlternate;

	public:
		/** */
		Part(
			ContextApi&					context,
			Oscl::BT::Mesh::
			Node::Creator::Api&			nodeCreateApi,
			Oscl::Pdu::FWD::Api&		provBearerApi,
			Oscl::Pdu::FWD::Api&		beaconBearerApi,
			Oscl::Pdu::FWD::Api&		advGattBearerApi,
			Oscl::Timer::Factory::Api&	timerFactoryApi,
			Oscl::BT::Mesh::
			Network::Iteration::Api&	networkIterator,
			const uint8_t				deviceUUID[16],
			uint16_t					oobInfo,
			bool						oobAvailable,
			uint8_t						outputOobSizeInCharacters
			) noexcept;

		/** */
		~Part() noexcept;

		/** */
		Oscl::Frame::FWD::Api&	getRxMeshProvApi() noexcept;

		/** */
		void	provision(
					Oscl::BT::Mesh::Network::Api&	networkApi,
					uint8_t							uuid[16]
					) noexcept;

		/** */
		void	start() noexcept;

	private: // Oscl::BT::Mesh::Prov::Device::ContextApi
		/** This operation is called when the Reaper
			is stopped after it completes the process
			and is ready to be destroyed.
		 */
		void	stopped(Oscl::BT::Mesh::Prov::Device::Reaper& device) noexcept;

		/** This operation is called by the Reaper to
			create a new network.
		 */
		bool	createNewNode(
					const uint8_t	netKey[16],
					const uint8_t	deviceKey[16],
					uint16_t		keyIndex,
					uint8_t			flags,
					uint32_t		ivIndex,
					uint16_t		unicastAddress
					) noexcept;

		/** */
		uint8_t	getNumElements() const noexcept;

		/** */
		void	setAttentionTimer(uint8_t nSeconds) noexcept;

		/** */
		bool	oobOutputActionBlinkIsSupported() const noexcept;

		/** */
		bool	oobOutputActionBeepIsSupported() const noexcept;

		/** */
		bool	oobOutputActionVibrateIsSupported() const noexcept;

		/** */
		bool	oobOutputActionNumericIsSupported() const noexcept;

		/** */
		bool	oobOutputActionAlphaNumericIsSupported() const noexcept;

		/** */
		void	oobOutputBlink(uint32_t n) noexcept;

		/** */
		void	oobOutputBeep(uint32_t n) noexcept;

		/** */
		void	oobOutputVibrate(uint32_t n) noexcept;

		/** */
		void	oobOutputNumeric(uint32_t n) noexcept;

		/** */
		void	oobOutputAlphaNumeric(const char* s) noexcept;

		/** */
		void	provisioningComplete() noexcept;

	private: // Oscl::BT::Mesh::Prov::Link::ContextApi
		/** This operation is called when the Reaper
			is stopped after it completes the process
			and is ready to be destroyed.
		 */
		void	stopped(Oscl::BT::Mesh::Prov::Link::Reaper& device) noexcept;

	private: // Oscl::BT::Mesh::Prov::Client::ContextApi
		/** This operation is called when the Reaper
			is stopped after it completes the process
			and is ready to be destroyed.
		 */
		void	stopped(Oscl::BT::Mesh::Prov::Client::Reaper& device) noexcept;

		/** This operation is called by the Reaper to
			register a provisioned node on the network.
		 */
		void	registerNewNode(
					uint16_t		networkIndex,
					uint16_t		unicastAddress
					) noexcept;

		/** */
		uint16_t	allocateAddresses(uint16_t nElements) noexcept;

		/** */
		const void*	getNetKey() const noexcept;

		/** */
		uint16_t	getKeyIndex() const noexcept;

		/** */
		uint8_t		getFlags() const noexcept;

		/** */
		uint32_t	getTxIvIndex() const noexcept;

	private:
		/** */
		Oscl::BT::Mesh::Prov::Link::Reaper*	findLink(uint32_t linkID) noexcept;

	private:
		/** */
		bool	parseTransactionStart(
					uint32_t		linkID,
					uint8_t			transactionNumber,
					uint8_t			segN,
					const void*		frame,
					uint8_t 		length
					) noexcept;

		/** */
		bool	parseTransactionAcknowledgement(
					uint32_t		linkID,
					uint8_t			transactionNumber,
					const void*		frame,
					uint8_t 		length
					) noexcept;

		/** */
		bool	parseTransactionContinuation(
					uint32_t		linkID,
					uint8_t			transactionNumber,
					uint8_t			segIndex,
					const void*		frame,
					uint8_t 		length
				) noexcept;

		/** */
		bool	parseProvisioningBearerControlLinkOpen(
					uint32_t		linkID,
					uint8_t			transactionNumber,
					const void*		frame,
					uint8_t 		length
					) noexcept;

		/** */
		bool	parseProvisioningBearerControlLinkACK(
					uint32_t		linkID,
					uint8_t			transactionNumber,
					const void*		frame,
					uint8_t 		length
					) noexcept;

		/** */
		bool	parseProvisioningBearerControlLinkClose(
					uint32_t		linkID,
					uint8_t			transactionNumber,
					const void*		frame,
					uint8_t 		length
					) noexcept;

		/** */
		bool	parseProvisioningBearerControl(
					uint32_t		linkID,
					uint8_t			transactionNumber,
					uint8_t			bearerOpcode,
					const void*		frame,
					uint8_t 		length
					) noexcept;

		/** */
		bool	parseUnprovisionedDeviceBeacon(
					const void*		frame,
					uint8_t 		length
					) noexcept;

		/** */
		bool	parseSecureNetworkBeacon(
					const void*		frame,
					uint8_t 		length
					) noexcept;

		/** */
		bool	parseMeshMessage(
					const void*		frame,
					uint8_t 		length
					) noexcept;

		/** */
		bool	parseMeshBeacon(
					const void*		frame,
					uint8_t 		length
					) noexcept;

	private:
		/** */
		void	sendProvisioningBearerControlACK(
					uint32_t	linkID,
					uint8_t		transactionNumber
					) noexcept;

		/** */
		void	sendProvisioningBearerControlLinkOpen(
					uint32_t	linkID,
					uint8_t		transactionNumber,
					uint8_t		deviceUUID[16]
					) noexcept;

	private: // Oscl::Frame::FWD::Composer _rxMeshProv
		/** */
		bool	parseMeshProvisioning(	
					const void*		frame,
					unsigned 		length
					) noexcept;

	private: // Oscl::Frame::FWD::Composer _provSessionRx
		/** */
		bool	provSessionRx(	
					const void*		frame,
					unsigned int	length
					) noexcept;

	private:	// Oscl::Done::Operation _linkTxTimerExpired
		/** */
		void	linkTxTimerExpired() noexcept;

	private:	// Oscl::Done::Operation _unprovTxTimerExpired
		/** */
		void	unprovTxTimerExpired() noexcept;

	private:
		/** */
		void	sendUnprovisionedBeacon() noexcept;

		/** */
		void	sendMeshProvisioningServiceData() noexcept;

		/** */
		void	sendMeshProxyServiceDataWithNetworkID() noexcept;

		/** */
		void	sendMeshProxyServiceDataWithNodeIdentity() noexcept;

	};

}
}
}
}
}

#endif
