/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_bluetooth_mesh_ivupdate_parth_
#define _oscl_bluetooth_mesh_ivupdate_parth_

#include <stdint.h>
#include "context.h"
#include "oscl/inhibitor/composer.h"
#include "oscl/timer/factory/api.h"
#include "oscl/done/operation.h"

/** */
namespace Oscl {
/** */
namespace BT {
/** */
namespace Mesh {
/** */
namespace IvUpdate {

/** This Part maintains the IV Index for the
	node. It encapsulates the behaviour
	required for the IV Index Update procedure.
 */
class Part {
	private:
		/** */
		ContextApi&	_context;

		/** */
		Oscl::Timer::Factory::Api&	_timerFactoryApi;

		/** */
		uint32_t	_ivIndex;

		/** */
		uint32_t	_hoursSinceChange;

		/** True when an IV Update is in Progress.
			The initial value comes from persistence.
		 */
		bool		_ivUpdateInProgress;

	private:
		/** */
		Oscl::Done::Operation<Part>	_timerExpired;

		/** */
		Oscl::Timer::Api&	_timerApi;

	private:
		/** */
		Oscl::Inhibitor::Composer<Part>	_inhibitor;

		/** When this counter is non-zero the transition
			from IV Update in Progress to Normal Operation
			is deferred.
		 */
		unsigned	_inhibitTransitionToNormal;

		/** */
		bool		_ivUpdatePending;

	public:
		/** Parameters:
            param [in] ivIndex				from persistence
            param [in] hoursSinceChange  	from persistence.
											The number of hours that have
											elapsed since the change began.
            param [in] ivUpdateInProgress	from persistence
		 */
		Part(
			ContextApi&					context,
			Oscl::Timer::Factory::Api&	timerFactoryApi,
			uint32_t					ivIndex,
			uint32_t					hoursSinceChange,
			bool						ivUpdateInProgress
			) noexcept;

		/** */
		~Part() noexcept;

		/** */
		void	start() noexcept;

		/** */
		void	stop() noexcept;

		/** */
		void	testNormalState() noexcept;

		/** */
		void	testInProgressState() noexcept;

		/** */
		Oscl::Inhibitor::Api&	getIvUpdateInhibitorApi() noexcept;

		/** This operation is invoked by the context
			to request an IV Update. This is done
			when the node determines that it is at
			risk of exhausting its sequence numbers
			or determines another node is close to
			exhausting its sequence numbers.
		 */
		void	initiateIvUpdate() noexcept;

		/** This operation returns the current
			transmit IV Index based on the current
			IV update state.
		 */
		uint32_t	getTxIvIndex() const noexcept;

		/** This operation returns the current
			receive IV Index based on the current
			IV update state.
		 */
		uint32_t	getRxIvIndex(bool ivi) const noexcept;

		/** */
		void	getIvIndexState(
					uint32_t&	ivIndex,
					bool&		ivUpdateFlag
					) const noexcept;

		/** This is called when a Secure Network
			beacon is received.
		 */
		void	beaconReceived(
					uint32_t	ivIndex,
					bool		ivUpdateFlag
					) noexcept;

	private:
		/** */
		void	processNormalStateIvUpdateFlagChange(
					uint32_t	ivIndex,
					bool		state
					) noexcept;

		void	startIvIndexRecoverProcedure(
					uint32_t	ivIndex,
					bool		ivUpdateInProgress
					) noexcept;

	private: // Oscl::Inhibitor::Composer _inhibitor
		/**	This operation indicates to the server that the process
			is inhibited by this client. Each invocation of this
			operation MUST eventually be followed by an allow operation
			invocation.
		 */
		void	preventTransitionToNormal() noexcept;

		/**	This operation indicates to the server that the process
			is allowed to proceed by this client. Each invocation of
			this operation MUST have been preceeded by a prevent
			operation invocation.
		 */
		void	allowTransitionToNormal() noexcept;

	private: // Oscl::Done::Operation _timerExpired
		/** */
		void	timerExpired() noexcept;

		/** */
		void	transitionToNormal() noexcept;

		/** */
		void	transitionToUpdateInProgress() noexcept;
	};

}
}
}
}

#endif
