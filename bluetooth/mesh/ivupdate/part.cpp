/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "part.h"
#include "oscl/error/fatal.h"
#include "oscl/error/info.h"

using namespace Oscl::BT::Mesh::IvUpdate;

//#define DEBUG_TRACE

Part::Part(
	ContextApi&					context,
	Oscl::Timer::Factory::Api&	timerFactoryApi,
	uint32_t					ivIndex,
	uint32_t					hoursSinceChange,
	bool						ivUpdateInProgress
	) noexcept:
		_context(context),
		_timerFactoryApi(timerFactoryApi),
		_ivIndex(ivIndex),
		_ivUpdateInProgress(ivUpdateInProgress),
		_timerExpired(
			*this,
			&Part::timerExpired
			),
		_timerApi(*timerFactoryApi.allocate()),
		_inhibitor(
			*this,
			&Part::preventTransitionToNormal,
			&Part::allowTransitionToNormal
			),
		_inhibitTransitionToNormal(0),
		_ivUpdatePending(false)
		{
	_timerApi.setExpirationCallback(
		_timerExpired
		);
	}

Part::~Part() noexcept{
	_timerFactoryApi.free(_timerApi);
	}

constexpr unsigned	hoursInMs	= 60*60*1000;

void	Part::start() noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif

	_timerApi.start(hoursInMs);
	}

void	Part::stop() noexcept{
	_timerApi.stop();
	}

void	Part::testNormalState() noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif

	if(!_ivUpdateInProgress){
		return;
		}

	// Setting _hoursSinceChange to a value
	// greater than 96 allows the transitionToNormal()
	// when _inhibitTransitionToNormal goes to zero.
	_hoursSinceChange = 100;

	if(!_inhibitTransitionToNormal){

		transitionToNormal();
		}
	}

void	Part::testInProgressState() noexcept{
	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif

	if(_ivUpdateInProgress){
		return;
		}

	transitionToUpdateInProgress();
	}

Oscl::Inhibitor::Api&	Part::getIvUpdateInhibitorApi() noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif

	return _inhibitor;
	}

void	Part::initiateIvUpdate() noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif

	if(_ivUpdatePending){
		return;
		}

	if(_ivUpdateInProgress){
		return;
		}

	if(_hoursSinceChange <= 96){
		// We expect that this will be called
		// repeatedly 
		_ivUpdatePending	= true;
		return;
		}

	transitionToUpdateInProgress();
	}

void	Part::transitionToNormal() noexcept{

//	#ifdef DEBUG_TRACE
	#if 1
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif

	_hoursSinceChange	= 0;
	_ivUpdateInProgress	= false;

	_context.storeIvIndexState(
		_ivIndex,
		_ivUpdateInProgress,
		_hoursSinceChange
		);

	_context.resetSequenceNumber();

	_context.ivIndexStateChanged();
	}

void	Part::transitionToUpdateInProgress() noexcept{

//	#ifdef DEBUG_TRACE
	#if 1
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif

	_hoursSinceChange	= 0;
	_ivUpdateInProgress	= true;
	++_ivIndex;

	_context.storeIvIndexState(
		_ivIndex,
		_ivUpdateInProgress,
		_hoursSinceChange
		);

	_context.ivIndexStateChanged();
	}

uint32_t	Part::getTxIvIndex() const noexcept{

	if(_ivUpdateInProgress){
		// IV Update State: In Progress
		return _ivIndex-1;
		}

	// IV Update State: Normal
	return _ivIndex;
	}

uint32_t	Part::getRxIvIndex(bool ivi) const noexcept{

	bool	lsb	= (_ivIndex & 1)?true:false;

	if(lsb == ivi){
		return _ivIndex;
		}

	return _ivIndex-1;
	}

void	Part::getIvIndexState(
			uint32_t&	ivIndex,
			bool&		ivUpdateFlag
			) const noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s:"
		" _ivIndex: 0x%8.8x,"
		" _ivUpdateInProgress: %s\n",
		OSCL_PRETTY_FUNCTION,
		_ivIndex,
		_ivUpdateInProgress?"true":"false"
		);
	#endif

	ivIndex			= _ivIndex;
	ivUpdateFlag	= _ivUpdateInProgress;

	}

void	Part::beaconReceived(
			uint32_t	ivIndex,
			bool		ivUpdateFlag
			) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s:"
		" ivIndex: 0x%8.8X,"
		" ivUpdateFlag: %s"
		"\n",
		OSCL_PRETTY_FUNCTION,
		ivIndex,
		ivUpdateFlag?"true":"false"
		);
	#endif

	if(ivIndex == _ivIndex){
		// We're already aware of this IV index.
		// At this point, we only react if we're
		// in the IV update in progress state AND
		// the ivUpdateFlag is false.
		if(ivUpdateFlag){
			// We're already aware of this.
			return;
			}
		if(!_ivUpdateInProgress){
			// No change
			return;
			}
		/*	The IV index matches our current
			AND we're in the IV Update Progress state
			AND the ivUpdateFlag is false.
			Therefore, we transition to the Normal state.
		 */
		if(!_inhibitTransitionToNormal){
			transitionToNormal();
			}
		return;
		}

	if(!_ivUpdateInProgress){
		processNormalStateIvUpdateFlagChange(
			ivIndex,
			ivUpdateFlag
			);
		}

	}

void	Part::processNormalStateIvUpdateFlagChange(
			uint32_t	ivIndex,
			bool		ivUpdateFlag
			) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s: ivIndex: 0x%8.8X, ivUpdateFlag: %s\n",
		OSCL_PRETTY_FUNCTION,
		ivIndex,
		ivUpdateFlag?"true":"false"
		);
	#endif

	/*
		If a node in Normal Operation receives a Secure Network beacon
		with an IV index greater than the last known IV Index + 1,
		it may initiate an IV Index Recovery procedure.
	 */

	if(ivIndex > (_ivIndex+1)){
		// Start IV Index Recovery procedure.
		startIvIndexRecoverProcedure(
			ivIndex,
			ivUpdateFlag
			);
		return;
		}

	/*
		If a node in Normal Operation receives a Secure Network beacon with
		an IV index equal to the last known IV index+1 and the IV Update Flag
		set to 0, the node may update its IV without going to the IV Update in
		Progress state, or it may initiate an IV Index Recovery procedure,
		or it may ignore the Secure Network beacon. The node makes the choice
		depending on the time since last IV update and the likelihood that the
		node has missed the Secure Network beacons with the IV update Flag set to 1.
	 */
	if(ivIndex == (_ivIndex+1)){
		if(!ivUpdateFlag){
			// TODO: what to do?
			// For now, I choose to simply update my state.
			_ivIndex			= ivIndex;
			_ivUpdateInProgress	= false;
			return;
			}
		}

	/*
		If a node in Normal Operation receives a Secure Network beacon with
		an IV index less than the last known IV Index or greater than the
		last known IV Index + 42, the Secure Network beacon shall be ignored.

		Note:This above requirement allows a node to be away from the network
		for 48 weeks. A node that is away from a network for longer than
		48 weeks must be reprovisioned.
	 */

	if(ivIndex < (_ivIndex)){
		return;
		}

	if(ivIndex > (_ivIndex+42)){
		return;
		}

	if(ivUpdateFlag){
		transitionToUpdateInProgress();
		}
	}

void	Part::startIvIndexRecoverProcedure(
			uint32_t	ivIndex,
			bool		ivUpdateInProgress
			) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif

	/*
		A node shall support the IV index recovery procedure because a node that
		is away from the network for long time may miss IV Update procedures,
		in which case it can no longer communicate with the other nodes.

		In order to recover the IV Index, the node must listen for a Secure
		Network beacon, which contains the Network ID and the current IV Index.
		Upon receiving and successfully authenticating a Secure Network beacon
		for a primary subnet whose IV Index is 1 or more higher than the
		current known IV Index, the node shall set its current IV Index
		and its current IV Update procedure state from the values in this
		Secure Network beacon.

		Note: After the IV Index Recovery procedure has updated the IV Index,
		the 96 hour time limits for changing the IV Update procedure state,
		as defined in the IV Update procedure, do not apply.

		Given that nodes collectively transmit a Secure Network beacon once
		every 10 seconds, a low duty cycle node will have to listen for an
		average of 5 seconds to recover the current IV Index before transmitting
		and receiving mesh messages. If a Low Power node has insufficient power
		to listen for 5 seconds, then it must stay up to date with the current
		IV Index by polling its Friend node at least once every 96 hours.

		The node shall not execute more than one IV Index Recovery
		within a period of 192 hours.
	 */

	_hoursSinceChange		= 0;
	_ivIndex				= ivIndex;
	_ivUpdateInProgress		= ivUpdateInProgress;

	_context.storeIvIndexState(
		_ivIndex,
		_ivUpdateInProgress,
		_hoursSinceChange
		);
	}

void	Part::preventTransitionToNormal() noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif

	if(!_inhibitTransitionToNormal){
		#ifdef DEBUG_TRACE
		Oscl::Error::Info::log(
			"%s: Inhibit Transition To Normal\n",
			OSCL_PRETTY_FUNCTION
			);
		#endif
		}

	++_inhibitTransitionToNormal;
	}

void	Part::allowTransitionToNormal() noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif

	if(!_inhibitTransitionToNormal){
		Oscl::ErrorFatal::logAndExit(
			"%s: unbalanced inhibit!\n",
			OSCL_PRETTY_FUNCTION
			);
		}

	--_inhibitTransitionToNormal;

	if(_inhibitTransitionToNormal){
		return;
		}

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s: Allow Transition To Normal\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif

	if(_ivUpdateInProgress){
		if(_hoursSinceChange > 96){
			_hoursSinceChange		= 0;
			_ivUpdateInProgress	= false;

			_context.storeIvIndexState(
				_ivIndex,
				_ivUpdateInProgress,
				_hoursSinceChange
				);

			transitionToNormal();
			}
		}
	}

void	Part::timerExpired() noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif

	_timerApi.start(hoursInMs);

	if(_hoursSinceChange > 192){
		return;
		}

	++_hoursSinceChange;

	if(_ivUpdateInProgress && !_inhibitTransitionToNormal){
		if(_hoursSinceChange > 96){
			transitionToNormal();
			return;
			}
		}

	_context.storeIvIndexState(
		_ivIndex,
		_ivUpdateInProgress,
		_hoursSinceChange
		);

	}

