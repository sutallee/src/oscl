/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_bluetooth_mesh_ivupdate_contexth_
#define _oscl_bluetooth_mesh_ivupdate_contexth_

#include <stdint.h>

/** */
namespace Oscl {
/** */
namespace BT {
/** */
namespace Mesh {
/** */
namespace IvUpdate {

/** */
class ContextApi {
	public:
		/** This operation is used to indicate to
			context that it should update the persitence
			for the IV Index and its state.

            param [in] ivIndex				The current IV Index
            param [in] ivUpdateInProgress	The IV Index Update flag
            param [in] hoursSinceChange  	The number of hours that have
											elapsed since the change began.
		 */
		virtual void	storeIvIndexState(
							uint32_t	ivIndex,
							bool		ivUpdateInProgress,
							uint32_t	hoursSinceChange
							) noexcept=0;

		/** This operation is used to reset the value
			of the Sequence Number counter to zero.
		 */
		virtual void	resetSequenceNumber() noexcept=0;

		/** This operation is used to notify the context
			that the IV index state has changed. This
			is generally used to cause a secure network
			beacon to be transmitted.
		 */
		virtual void	ivIndexStateChanged() noexcept=0;
	};

}
}
}
}

#endif
