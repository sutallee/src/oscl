/*
   Copyright (C) 2020 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "sartx.h"
#include "oscl/error/info.h"

void	Oscl::BT::Mesh::Proxy::SAR::gattSegmentAndTransmit(
			Oscl::Pdu::Pdu* 		pdu,
			Oscl::Pdu::Memory::Api&	freeStore,
			Oscl::Pdu::FWD::Api&	txApi,
			const Oscl::BT::Mesh::
			Proxy::SAR::Opcodes&    opcodes,
			unsigned				mtu
			) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s: mtu: %u\n",
		__PRETTY_FUNCTION__,
		mtu
		);
	#endif

	unsigned
	length	= pdu->length();

	unsigned	nSegments	= length/mtu;

	unsigned	remaining	= length % mtu;

	if(remaining){
		++nSegments;
		}

	Oscl::Handle<Oscl::Pdu::Composite>
	composite	= freeStore.allocConstWrapper(
					&opcodes._firstSegment,
					sizeof(opcodes._firstSegment)
					);

	if(!composite){
		Oscl::Error::Info::log(
			"%s: out of composite wrappers.\n",
			__PRETTY_FUNCTION__
			);
		return;
		}

	Oscl::Handle<Oscl::Pdu::Fragment>
	fragment	= freeStore.allocFragment(
					*pdu
					);

	if(!fragment){
		Oscl::Error::Info::log(
			"%s: out of fragments.\n",
			__PRETTY_FUNCTION__
			);
		return;
		}

	fragment->stripTrailer(length-mtu);

	composite->append(fragment);

	txApi.transfer(composite);

	if(nSegments > 2){
		for(unsigned i=1;i<(nSegments-2);++i){

			composite	= freeStore.allocConstWrapper(
							&opcodes._continuationSegment,
							sizeof(opcodes._continuationSegment)
							);

			if(!composite){
				Oscl::Error::Info::log(
					"%s: out of composite wrappers.\n",
					__PRETTY_FUNCTION__
					);
				return;
				}

			fragment	= freeStore.allocFragment(
							*pdu
							);

			if(!fragment){
				Oscl::Error::Info::log(
					"%s: out of fragments.\n",
					__PRETTY_FUNCTION__
					);
				return;
				}

			fragment->stripHeader(i*mtu);
			fragment->stripTrailer(length-((i*mtu)+1));

			composite->append(fragment);

			txApi.transfer(composite);
			}
		}

	composite	= freeStore.allocConstWrapper(
					&opcodes._lastSegment,
					sizeof(opcodes._lastSegment)
					);

	if(!composite){
		Oscl::Error::Info::log(
			"%s: out of composite wrappers.\n",
			__PRETTY_FUNCTION__
			);
		return;
		}

	fragment	= freeStore.allocFragment(
					*pdu
					);

	if(!fragment){
		Oscl::Error::Info::log(
			"%s: out of fragments.\n",
			__PRETTY_FUNCTION__
			);
		return;
		}

	if(!remaining){
		remaining	= mtu;
		}

	fragment->stripHeader((nSegments-1)*mtu);

	composite->append(fragment);

	txApi.transfer(composite);
	}

