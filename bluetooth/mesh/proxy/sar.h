/*
   Copyright (C) 2020 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_bluetooth_mesh_proxy_sarh_
#define _oscl_bluetooth_mesh_proxy_sarh_

#include <stdint.h>

/** */
namespace Oscl {
/** */
namespace BT {
/** */
namespace Mesh {
/** */
namespace Proxy {
/** */
namespace SAR {

/** */
constexpr uint8_t	complete				= 0x00;
/** */
constexpr uint8_t	firstSegment			= 0x01;
/** */
constexpr uint8_t	continuationSegment		= 0x02;
/** */
constexpr uint8_t	lastSegment				= 0x03;

/** This structure forms the basis of a set of
	
 */
struct Opcodes {
	/** */
	const uint8_t	_complete;

	/** */
	const uint8_t	_firstSegment;

	/** */
	const uint8_t	_continuationSegment;

	/** */
	const uint8_t	_lastSegment;

	/** */
	static const Opcodes	networkPduOpcodes;

	/** */
	static const Opcodes	meshBeaconOpcodes;

	/** */
	static const Opcodes	proxyConfigurationOpcodes;

	/** */
	static const Opcodes	provisioningPduOpcodes;
	};

}
}
}
}
}

#endif
