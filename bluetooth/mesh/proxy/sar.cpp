/*
   Copyright (C) 2020 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "sar.h"
#include "opcodes.h"

using namespace Oscl::BT::Mesh::Proxy::SAR;

const Opcodes	Oscl::BT::Mesh::Proxy::SAR::Opcodes::networkPduOpcodes	= {
	._complete = (
			(Oscl::BT::Mesh::Proxy::Opcodes::networkPDU << 0)
		|	(Oscl::BT::Mesh::Proxy::SAR::complete << 6)
		),
	._firstSegment = (
			(Oscl::BT::Mesh::Proxy::Opcodes::networkPDU << 0)
		|	(Oscl::BT::Mesh::Proxy::SAR::firstSegment << 6)
		),
	._continuationSegment = (
			(Oscl::BT::Mesh::Proxy::Opcodes::networkPDU << 0)
		|	(Oscl::BT::Mesh::Proxy::SAR::continuationSegment << 6)
		),
	._lastSegment = (
			(Oscl::BT::Mesh::Proxy::Opcodes::networkPDU << 0)
		|	(Oscl::BT::Mesh::Proxy::SAR::lastSegment << 6)
		)
	};

const Opcodes	Oscl::BT::Mesh::Proxy::SAR::Opcodes::meshBeaconOpcodes	= {
	._complete = (
			(Oscl::BT::Mesh::Proxy::Opcodes::meshBeacon << 0)
		|	(Oscl::BT::Mesh::Proxy::SAR::complete << 6)
		),
	._firstSegment = (
			(Oscl::BT::Mesh::Proxy::Opcodes::meshBeacon << 0)
		|	(Oscl::BT::Mesh::Proxy::SAR::firstSegment << 6)
		),
	._continuationSegment = (
			(Oscl::BT::Mesh::Proxy::Opcodes::meshBeacon << 0)
		|	(Oscl::BT::Mesh::Proxy::SAR::continuationSegment << 6)
		),
	._lastSegment = (
			(Oscl::BT::Mesh::Proxy::Opcodes::meshBeacon << 0)
		|	(Oscl::BT::Mesh::Proxy::SAR::lastSegment << 6)
		)
	};

const Opcodes	Oscl::BT::Mesh::Proxy::SAR::Opcodes::proxyConfigurationOpcodes	= {
	._complete = (
			(Oscl::BT::Mesh::Proxy::Opcodes::proxyConfiguration << 0)
		|	(Oscl::BT::Mesh::Proxy::SAR::complete << 6)
		),
	._firstSegment = (
			(Oscl::BT::Mesh::Proxy::Opcodes::proxyConfiguration << 0)
		|	(Oscl::BT::Mesh::Proxy::SAR::firstSegment << 6)
		),
	._continuationSegment = (
			(Oscl::BT::Mesh::Proxy::Opcodes::proxyConfiguration << 0)
		|	(Oscl::BT::Mesh::Proxy::SAR::continuationSegment << 6)
		),
	._lastSegment = (
			(Oscl::BT::Mesh::Proxy::Opcodes::proxyConfiguration << 0)
		|	(Oscl::BT::Mesh::Proxy::SAR::lastSegment << 6)
		)
	};

const Opcodes	Oscl::BT::Mesh::Proxy::SAR::Opcodes::provisioningPduOpcodes	= {
	._complete = (
			(Oscl::BT::Mesh::Proxy::Opcodes::provisioningPDU << 0)
		|	(Oscl::BT::Mesh::Proxy::SAR::complete << 6)
		),
	._firstSegment = (
			(Oscl::BT::Mesh::Proxy::Opcodes::provisioningPDU << 0)
		|	(Oscl::BT::Mesh::Proxy::SAR::firstSegment << 6)
		),
	._continuationSegment = (
			(Oscl::BT::Mesh::Proxy::Opcodes::provisioningPDU << 0)
		|	(Oscl::BT::Mesh::Proxy::SAR::continuationSegment << 6)
		),
	._lastSegment = (
			(Oscl::BT::Mesh::Proxy::Opcodes::provisioningPDU << 0)
		|	(Oscl::BT::Mesh::Proxy::SAR::lastSegment << 6)
		)
	};

