/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_bluetooth_mesh_beacon_secure_network_parth_
#define _oscl_bluetooth_mesh_beacon_secure_network_parth_

#include <stdint.h>
#include "api.h"
#include "context.h"
#include "oscl/timer/factory/api.h"
#include "oscl/done/operation.h"

/** */
namespace Oscl {
/** */
namespace BT {
/** */
namespace Mesh {
/** */
namespace Beacon {
/** */
namespace Secure {
/** */
namespace Network {

/** The purpose of this class is to determine
	when to send Secure Network Beacons.
 */
class Part : public Api {
	private:
		/** */
		ContextApi&	_context;

		/** */
		Oscl::Timer::Factory::Api&	_timerFactoryApi;

	private:
		/** */
		Oscl::Done::Operation<Part>	_beaconTimerExpired;

		/** */
		Oscl::Timer::Api&			_beaconTimerApi;

	private:
		/** */
		Oscl::Done::Operation<Part>	_observationTimerExpired;

		/** */
		Oscl::Timer::Api&			_observationTimerApi;

	private:
		/** */
		unsigned long				_observationPeriodInMs;

		/** */
		unsigned long				_beaconIntervalInMs;

		/** */
		unsigned 					_observedNumberOfBeacons;

		/** */
		unsigned 					_expectedNumberOfBeacons;

	public:
		/** */
		Part(
			ContextApi&					context,
			Oscl::Timer::Factory::Api&	timerFactoryApi
			) noexcept;

		/** */
		~Part() noexcept;

	public: // Oscl::BT::Mesh::Beacon::Secure::Network::Api
		/** */
		void	start() noexcept;

		/** */
		void	stop() noexcept;

		/** */
		void	beaconReceived() noexcept;

	private:
		/** */
		void	beaconTimerExpired() noexcept;

		/** */
		void	observationTimerExpired() noexcept;
	};

}
}
}
}
}
}
#endif
