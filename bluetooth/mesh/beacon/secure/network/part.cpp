/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "part.h"
#include "oscl/error/info.h"
#include "oscl/entropy/rand.h"

using namespace Oscl::BT::Mesh::Beacon::Secure::Network;

//#define DEBUG_TRACE

static constexpr unsigned	typicalBeaconIntervalInMs	= 10*1000;

Part::Part(
	ContextApi&					context,
	Oscl::Timer::Factory::Api&	timerFactoryApi
	) noexcept:
		_context(context),
		_timerFactoryApi(timerFactoryApi),
		_beaconTimerExpired(
			*this,
			&Part::beaconTimerExpired
			),
		_beaconTimerApi(
			*timerFactoryApi.allocate()
			),
		_observationTimerExpired(
			*this,
			&Part::observationTimerExpired
			),
		_observationTimerApi(
			*timerFactoryApi.allocate()
			),
		_observationPeriodInMs(3*typicalBeaconIntervalInMs),
		_beaconIntervalInMs(typicalBeaconIntervalInMs),
		_observedNumberOfBeacons(0),
		_expectedNumberOfBeacons(2)
		{
	_beaconTimerApi.setExpirationCallback(
		_beaconTimerExpired
		);
	_observationTimerApi.setExpirationCallback(
		_observationTimerExpired
		);
	}

Part::~Part() noexcept{
	_timerFactoryApi.free(_beaconTimerApi);
	_timerFactoryApi.free(_observationTimerApi);
	}

void	Part::start() noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif

	_observationTimerApi.start(_observationPeriodInMs);
	}

void	Part::stop() noexcept{
	_observationTimerApi.stop();
	_beaconTimerApi.stop();
	}

void	Part::beaconReceived() noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif

	++_observedNumberOfBeacons;
	}

void	Part::beaconTimerExpired() noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif

	_context.sendSecureNetworkBeacon();

	_beaconTimerApi.start(_beaconIntervalInMs);
	}

void	Part::observationTimerExpired() noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s:\n"
		" _observationPeriodInMs: %lums\n"
		" _beaconIntervalInMs: %lums\n"
		" _observedNumberOfBeacons: %u\n"
		" _expectedNumberOfBeacons: %u\n"
		"",
		__PRETTY_FUNCTION__,
		_observationPeriodInMs,
		_beaconIntervalInMs,
		_observedNumberOfBeacons,
		_expectedNumberOfBeacons
		);
	#endif

	// Beacon Interval = Observation Period * (Observed Number of Beacons + 1) / Expected Number of Beacons
	_beaconIntervalInMs	= _observationPeriodInMs * (_observedNumberOfBeacons + 1) / _expectedNumberOfBeacons;

	// The Expected Number of Beacons is the
	// Observation Period divided by 10 seconds.
	_expectedNumberOfBeacons	= _observationPeriodInMs / (10*1000);

	if(!_expectedNumberOfBeacons){
		_expectedNumberOfBeacons	= 1;
		}

	_observedNumberOfBeacons	= 0;

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s:\n"
		" new _observationPeriodInMs: %lums\n"
		" new _beaconIntervalInMs: %lums\n"
		"",
		__PRETTY_FUNCTION__,
		_observationPeriodInMs,
		_beaconIntervalInMs
		);
	#endif

	_observationTimerApi.start(_observationPeriodInMs);

	if(!_beaconTimerApi.running()){
		// This should only happens after the first
		// observation period timer expiration.
		beaconTimerExpired();
		}
	}


