/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_bluetooth_mesh_bearer_tx_apih_
#define _oscl_bluetooth_mesh_bearer_tx_apih_

#include "oscl/pdu/fwd/api.h"

/** */
namespace Oscl {
/** */
namespace BT {
/** */
namespace Mesh {
/** */
namespace Bearer {
/** */
namespace TX {

/** This interface is used by the mesh Network
	layer to transmit network PDUs over a beaer
	(network interface).

	This interface is necessary to support output
	filtering.
 */
class Api {
	public:
		/**
			This operation is used to transmit a Network PDU
			over a particular bearer. The destination address
			is redundant with the one encrypted in the Network
			PDU, but is required to implement output filtering
			by the bearer.

			param [in] pdu	Pointer to the encrypted network PDU.
			param [in] dst	The destination address contained in
							the network PDU.
		 */
		virtual void	transmit(
					Oscl::Pdu::Pdu*	pdu,
					uint16_t		dst
					) noexcept=0;
	};

}
}
}
}
}

#endif
