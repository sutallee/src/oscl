/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_bluetooth_mesh_bearer_tx_itemcomph_
#define _oscl_bluetooth_mesh_bearer_tx_itemcomph_

#include "item.h"

// Oscl::BT::Mesh::Bearer::TX::Item

/** */
namespace Oscl {
/** */
namespace BT {
/** */
namespace Mesh {
/** */
namespace Bearer {
/** */
namespace TX {

/**	This template class implements a kind of GOF decorator
	pattern allows the context to employ composition instead
	of inheritance. Frequently, a context may need to implement
	more than one interface that may result in method name
	clashes. This template solves the problem by implementing
	the interface and then invoking member function pointers
	in the context instead. One instance of this object is
	created in the context for each interface of this type used
	in the context. Each instance is constructed such that it
	refers to different member functions within the context.
	The interface of this object is passed to any object that
	requires the interface. When the object invokes any of the
	operations of the API, the corresponding member function
	of the context will subsequently be invoked.
 */
template <class Context>
class ItemComposer : public Item {
	private:
		/** A reference to the Context.
		 */
		Context&	_context;

	private:
		/**	This operation is used to transmit a pdu contained in a
			single buffer to another layer. The implementation is
			expected to examine and possibly copy the pdu, as the
			caller will reclaim the buffer after the transmit returns.
			RETURN: The implementation is expected to return true if
			it recognizes the pdu type and processes it. This *may*
			be used by the client to halt iteration of a list of
			interfaces of this type.
		 */
		void	(Context::*_transmit)(	
							Oscl::Pdu::Pdu*	pdu,
							uint16_t		dst
							);

	public:
		/** */
		ItemComposer(
			Context&		context,
			void	(Context::*transmit)(	
								Oscl::Pdu::Pdu*	pdu,
								uint16_t		dst
								)
			) noexcept;

	private:
		/**	This operation is used to transmit a PDU contained in a
			single buffer to another layer. The implementation is
			expected to examine and possibly copy the PDU, as the
			caller will reclaim the buffer after the transmit returns.
			RETURN: The implementation is expected to return true if
			it recognizes the PDU type and processes it. This *may*
			be used by the client to halt iteration of a list of
			interfaces of this type.
		 */
		void	transmit(	
					Oscl::Pdu::Pdu*	pdu,
					uint16_t		dst
					) noexcept;

	};

template <class Context>
ItemComposer<Context>::ItemComposer(
			Context&		context,
			void	(Context::*transmit)(	
						Oscl::Pdu::Pdu*	pdu,
						uint16_t		dst
						)
			) noexcept:
		_context(context),
		_transmit(transmit)
		{
	}

template <class Context>
void	ItemComposer<Context>::transmit(	
							Oscl::Pdu::Pdu*	pdu,
							uint16_t		dst
							) noexcept{

	return (_context.*_transmit)(
				pdu,
				dst
				);
	}

}
}
}
}
}

#endif
