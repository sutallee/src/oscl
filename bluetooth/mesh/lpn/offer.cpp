/*
   Copyright (C) 2020 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "offer.h"
using namespace Oscl::BT::Mesh::LPN;

Offer::Offer(
	uint16_t	friendAddress,
	uint8_t		receiveWindowInMs,
	uint8_t		queueSizeInMessages,
	uint8_t		subscriptionListSizeInEntries,
	uint8_t		rssiInDBm,
	uint16_t	friendCounter,
	uint16_t	lpnCounter
	) noexcept:
		_friendAddress(friendAddress),
		_receiveWindowInMs(receiveWindowInMs),
		_queueSizeInMessages(queueSizeInMessages),
		_subscriptionListSizeInEntries(subscriptionListSizeInEntries),
		_rssiInDBm(rssiInDBm),
		_friendCounter(friendCounter),
		_lpnCounter(lpnCounter)
		{
	}
