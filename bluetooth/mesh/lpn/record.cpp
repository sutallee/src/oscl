/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "record.h"
#include "oscl/error/info.h"

using namespace Oscl::BT::Mesh::LPN;

#define DEBUG_TRACE

SubscribeRecord::SubscribeRecord(
	uint16_t	address
	) noexcept:
		_address(address),
		_present(true)
		{
	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s: address: 0x%4.4X\n",
		__PRETTY_FUNCTION__,
		_address
		);
	#endif
	}

SubscribeRecord::~SubscribeRecord() noexcept{
	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s: address: 0x%4.4X\n",
		__PRETTY_FUNCTION__,
		_address
		);
	#endif
	}

uint16_t	SubscribeRecord::address() const noexcept{
	return _address;
	}

bool	SubscribeRecord::present() const noexcept{
	return _present;
	}

void	SubscribeRecord::markAbsent() noexcept{
	_present	= false;
	}

void	SubscribeRecord::markPresent() noexcept{
	_present	= true;
	}

