/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <new>
#include <string.h>
#include <stdlib.h>

#include "part.h"
#include "oscl/error/info.h"
#include "oscl/error/fatal.h"
#include "oscl/encoder/be/base.h"
#include "oscl/endian/decoder/linear/part.h"
#include "oscl/bluetooth/crypto/keys.h"
#include "oscl/bluetooth/crypto/salt.h"
#include "oscl/aes128/cipher.h"
#include "oscl/aes128/ccm.h"
#include "oscl/entropy/rand.h"
#include "oscl/bluetooth/mesh/address/types.h"
#include "oscl/bluetooth/mesh/address/values.h"
#include "oscl/bluetooth/mesh/network/constants.h"

//#define DEBUG_TRACE
//#define DEBUG_TRACE_TMI

/*
	After 100 milliseconds have passed from the Friend Request,
	the node should listen for up to 1 second for the Friend Offer
	messages sent by potential Friend nodes, and it may select one
	of the Friend nodes to establish a friendship.
	The Low Power node may accept a received Friend Offer or
	continue to listen for other Friend Offer messages for comparison.

	If no acceptable Friend Offer message is received, the node may
	send a new Friend Request message.

	The time interval between two consecutive Friend Request messages
	shall be greater than 1.1 seconds.

	To establish a friendship with a potential Friend that has sent a
	Friend Offer message, the node shall set the Friend Sequence Number
	to zero and send a Friend Poll message to the selected Friend node
	within 1 second after the reception of the Friend Offer message.

	If a Friend Update message is received in response, the friendship
	is established, the Low Power feature of the node supporting it is
	in use and the Friend feature of the node supporting it is in use.

	The node should restart the Low Power Establishment operation if
	it does not receive a Friend Update message after several attempts
	(e.g., 6 attempts).
 */

/*	3.6.6.4 Low Power feature

	All transport control messages originated by a Low Power node shall
	be sent as Unsegmented Control messages with the SRC field set to
	the unicast address of the primary element of the node that supports
	the Low Power feature.
 */

constexpr unsigned long	friendRequestTimerInMs	= 5000;

constexpr unsigned long	offerTimerInMs	= 1000;

constexpr unsigned long	updateTimerInMs	= 1000;

constexpr unsigned long	nPollRetries	= 6;

constexpr uint8_t	lpnReceiveDelayInMs	= 128;	// FIXME: arbitrary

/* 3.6.6.4.2 Low Power messaging

	A Low Power node that is friends with a Friend node shall
	send a Friend Poll message to the Friend node *before*
	the PollTimeout timer expires.
 */
constexpr unsigned	pollTimeoutInMs	= 1 * 60 * 1000; // 1 minute

constexpr unsigned long	subscriptionRetransmitTimer	= 1000;

constexpr unsigned  maxNetworkControlTransportPduSize    = (96/8);

using namespace Oscl::BT::Mesh::LPN;

Part::Part(
	ContextApi&						context,
	Oscl::Timer::Factory::Api&		timerFactoryApi,
	Oscl::Pdu::Memory::Api&			freeStore,
	Oscl::BT::Mesh::Subscription::
	Register::IterationApi&			subscriptionIterationApi,
	Oscl::Inhibitor::Api&			hciDisableInhibitorApi,
	SubscribeRecordMem*				subscribeRecordMem,
	unsigned						nSubscribeRecordMem,
	uint16_t						lpnAddress
	) noexcept:
		_context(context),
		_timerFactoryApi(timerFactoryApi),
		_freeStore(freeStore),
		_subscriptionIterationApi(subscriptionIterationApi),
		_hciDisableInhibitorApi(hciDisableInhibitorApi),
		_lpnAddress(lpnAddress),
		_previousFriendAddress(Oscl::BT::Mesh::Address::unassigned),	// This should come from persistence
		_lpnCounter(0),	// Should this be persistent?
		_fsn(0),
		_subscriptionListChanged(
			*this,
			&Part::subscriptionListChanged
			),
		_timerApi(*timerFactoryApi.allocate()),
		_friendRequestTimerExpired(
			*this,
			&Part::friendRequestTimerExpired
			),
		_offerTimerExpired(
			*this,
			&Part::offerTimerExpired
			),
		_updateTimerExpired(
			*this,
			&Part::updateTimerExpired
			),
		_pollTimerExpired(
			*this,
			&Part::pollTimerExpired
			),
		_receiveDelayTimerExpired(
			*this,
			&Part::receiveDelayTimerExpired
			),
		_subscriptionTimerApi(*timerFactoryApi.allocate()),
		_subscriptionTimerExpired(
			*this,
			&Part::subscriptionTimerExpired
			),
		_networkRxItem(
			*this,
			&Part::rxNetwork
			),
		_offer(0),
		_upperTransportRxComposer(
			*this,
			&Part::forwardUpperTransportPduToAllElements
			),
		_hcReceiverDisableAllowed(false),
		_subscriptionTransactionNumber(0),
		_stopping(false)
		{
	for(unsigned i=0;i<nOfferMem;++i){
		_freeOfferMem.put(&_offerMem[i]);
		}

	for(unsigned i=0;i<nSubscribeRecordMem;++i){
		_freeSubscribeRecordMem.put(&subscribeRecordMem[i]);
		}

	for(unsigned i=0;i<nAccessReaperMem;++i){
		// Pre-allocate timers
		Oscl::Timer::Api*
		timer	= _timerFactoryApi.allocate();

		accessReaperMem[i].mem.ackTimer	= timer;

		timer	= _timerFactoryApi.allocate();
		accessReaperMem[i].mem.incompleteTimer	= timer;

		_freeAccessReaperMem.put(&accessReaperMem[i]);
		}

	_subscriptionIterationApi.getSubjectApi().attach(_subscriptionListChanged);
	}

Part::~Part() noexcept{
	_timerFactoryApi.free(_timerApi);
	_timerFactoryApi.free(_subscriptionTimerApi);
	_subscriptionIterationApi.getSubjectApi().detach(_subscriptionListChanged);

	AccessReaperMem*	mem;
	while((mem = _freeAccessReaperMem.get())){
		_timerFactoryApi.free(*mem->mem.ackTimer);
		_timerFactoryApi.free(*mem->mem.incompleteTimer);
		}
	}

void	Part::start() noexcept{
	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif

	_hciDisableInhibitorApi.prevent();

	sendFriendRequest();

	startFriendRequestTimer();

	subscriptionListChanged();
	}

void	Part::stop() noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif

	_stopping	= true;

	_timerApi.stop();
	_subscriptionTimerApi.stop();

	Oscl::BT::Mesh::Transport::
	Lower::Reassemble::Access::Reaper*	reaper;

	while((reaper = _activeAccessReapers.get())){
		reaper->stop();
		}
	}

Oscl::Frame::FWD::Item&	Part::getRxMeshMessageItem() noexcept{
	return _networkRxItem;
	}

void	Part::segmentReceived(
			const void*		frame,
			unsigned		length,
			uint32_t		ivIndex,
			uint32_t		seq,
			uint16_t		src,
			uint16_t		dst,
			bool			ctl,
			uint8_t			ttl
			) noexcept{
	// This is not used in the LPN.
	}

void	Part::stopped(Oscl::BT::Mesh::Transport::Lower::Reassemble::Access::Reaper& part) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif

	_activeAccessReapers.remove(&part);

	part.~Reaper();

	_freeAccessReaperMem.put((AccessReaperMem*)&part);
	}

void	Part::resetSubscriptions() noexcept{

	_subscriptionTimerApi.stop();

	SubscribeRecord*	r;

	while((r = _activeSubscribeList.get())){
		_pendingSubscribeList.put(r);
		}

	while((r = _pendingUnsubscribeList.get())){
		free(r);
		}

	while((r = _pendingConfirmList.get())){
		free(r);
		}
	}

void	Part::startFriendRequestTimer() noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif

	_timerApi.setExpirationCallback(_friendRequestTimerExpired);

	_timerApi.start(friendRequestTimerInMs);
	}

void	Part::startOfferTimer() noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif

	_timerApi.setExpirationCallback(_offerTimerExpired);

	_timerApi.start(offerTimerInMs);
	}

void	Part::startUpdateTimer() noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif

	_timerApi.setExpirationCallback(_updateTimerExpired);

	_timerApi.start(updateTimerInMs);
	}

void	Part::startPollTimer() noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif

	_timerApi.setExpirationCallback(_pollTimerExpired);

	_timerApi.start(pollTimeoutInMs);

	allowHcReceiverToBeDisabled();
	}

void	Part::startReceiveDelayTimer() noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif

	_timerApi.setExpirationCallback(_receiveDelayTimerExpired);

	_timerApi.start(lpnReceiveDelayInMs);

	allowHcReceiverToBeDisabled();
	}

void	Part::startSubscriptionTimer() noexcept{

	preventHcReceiverFromBeingDisabled();

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif

	_subscriptionTimerApi.setExpirationCallback(_subscriptionTimerExpired);
	_subscriptionTimerApi.start(subscriptionRetransmitTimer);
	}

void	Part::friendRequestTimerExpired() noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif

	resetSubscriptions();

	sendFriendRequest();

	startFriendRequestTimer();
	}

void	Part::offerTimerExpired() noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif

	/*	3.6.6.4.1 Low Power establishment
		To establish a friendship with a potential Friend
		that has sent a Friend Offer message, the node shall set
		the Friend Sequence Number to zero and send a Friend Poll
		message to the selected Friend node within 1 second after
		the reception of the Friend Offer message.

		If a Friend Update message is received in response,
		the friendship is established, the Low Power feature
		of the node supporting it is in use and the Friend
		feature of the node supporting it is in use.
	 */

	_offer	= chooseOffer();

	generateFriendshipSecurityMaterial(
		_context. getNetworkApi().getNetKey(),
		_lpnAddress,
		_offer->_friendAddress,
		_offer->_lpnCounter,
		_offer->_friendCounter
		);

	_fsn	= 0;

	sendFriendPoll();

	freeRemainingOffers();

	_pollRetries	= nPollRetries;
	}

void	Part::updateTimerExpired() noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s: _pollRetries: %u\n",
		__PRETTY_FUNCTION__,
		_pollRetries
		);
	#endif

	if(!_pollRetries){
//		#ifdef DEBUG_TRACE
		#if 1
		Oscl::Error::Info::log(
			"%s: poll timeout friendship terminated.\n",
			__PRETTY_FUNCTION__
			);
		#endif

		_previousFriendAddress	= _offer->_friendAddress;

		_freeOfferMem.put((Oscl::BT::Mesh::LPN::OfferMem*)_offer);

		_offer	= 0;

		resetSubscriptions();

		sendFriendRequest();

		startFriendRequestTimer();
		return;
		}

	--_pollRetries;

	sendFriendPoll();
	}

void	Part::pollTimerExpired() noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif

	/* 3.6.6.4.2 Low Power messaging

		A Low Power node that is friends with a Friend node shall
		send a Friend Poll message to the Friend node *before*
		the PollTimeout timer expires.
	 */

	_pollRetries	= nPollRetries;

	sendFriendPoll();
	}

void	Part::receiveDelayTimerExpired() noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif

	/*	The receive delay timer has expired
		and its time to enable the HCI
		receiver.
	 */

	preventHcReceiverFromBeingDisabled();

	startUpdateTimer();
	}

void	Part::subscriptionTimerExpired() noexcept{
	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif

	// Determine type of subscriptions in
	// _pendingConfirmList and retransmit.

	retransmitFriendSubscriptionList();
	}

void	Part::allowHcReceiverToBeDisabled() noexcept{

	#ifdef DEBUG_TRACE_TMI
	Oscl::Error::Info::log(
		"%s: _hcReceiverDisableAllowed: %s\n",
		__PRETTY_FUNCTION__,
		_hcReceiverDisableAllowed?"true":"false"
		);
	#endif

	if(!_hcReceiverDisableAllowed){
		_hciDisableInhibitorApi.allow();
		_hcReceiverDisableAllowed	= true;
		}
	}

void	Part::preventHcReceiverFromBeingDisabled() noexcept{

	#ifdef DEBUG_TRACE_TMI
	Oscl::Error::Info::log(
		"%s: _hcReceiverDisableAllowed: %s\n",
		__PRETTY_FUNCTION__,
		_hcReceiverDisableAllowed?"true":"false"
		);
	#endif

	if(_hcReceiverDisableAllowed){
		_hciDisableInhibitorApi.prevent();
		_hcReceiverDisableAllowed	= false;
		}
	}

Oscl::BT::Mesh::LPN::Offer*	Part::chooseOffer() noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif

	return _offers.get();
	}

void	Part::sendFriendPoll() noexcept{

//	#ifdef DEBUG_TRACE
	#if 1
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif

	uint8_t		buffer[8];
	
	Oscl::Encoder::BE::Base	beEncoder(
		buffer,
		sizeof(buffer)
		);

	Oscl::Encoder::Api&	encoder	= beEncoder;

	static const uint8_t	opcode		= 0x01;	// Friend Poll

	/*
			[7:1]	Padding (set to zero)
			[0]	FSN
					LSB of the friend sequence number
	*/

	const uint32_t	seq	= _context.getNodeApi().allocateSequenceNumber();

	uint8_t	fsnAndPad	= (_fsn)?0x01:0x00;

	uint16_t	dst	= _offer->_friendAddress;

	encoder.encode(dst);
	encoder.encode(opcode);
	encoder.encode(fsnAndPad);

	const uint32_t	ivIndex	= _context.getNodeApi().getTxIvIndex();

	/*	3.6.6.4.2 Low Power messaging
		In a Friend Poll message, the TTL field shall be set to 0.
	 */

	Oscl::Handle<Oscl::Pdu::Pdu>
	pdu	= buildNetworkPDU(
			true,				// bool		ctl,
			0,					// uint8_t	ttl,
			_lpnAddress,		// uint16_t	src,
			seq,				// uint32_t	seq,
			ivIndex,			// uint32_t	ivIndex,
			buffer,				// const void*	transportPDU,
			encoder.length()	// unsigned	tranportPduLength
			);

	if(!pdu){
		startUpdateTimer();
		return;
		}

	_context.getNetTxApi().transmitNetworkPDU(
		pdu,	// Oscl::Pdu::Pdu* pdu,
		dst		// uint16_t        dst
		);

	startReceiveDelayTimer();
	}

Oscl::Pdu::Pdu*	Part::buildNetworkPDU(
			bool		ctl,
			uint8_t		ttl,
			uint16_t	src,
			uint32_t	seq,
			uint32_t	ivIndex,
			const void*	transportPDU,
			unsigned	tranportPduLength
			) noexcept{

	using namespace Oscl::BT::Mesh::Network;

	#ifdef DEBUG_TRACE_TMI
	Oscl::Error::Info::log(
		"%s\n"
		"\tttl: %u\n"
		"\tctl: %s\n"
		"\ttransportPDU:\n"
		"",
		__PRETTY_FUNCTION__,
		ttl,
		ctl?"true":"false"
		);
	Oscl::Error::Info::hexDump(
		transportPDU,
		tranportPduLength
		);
	#endif

	// At this point, we are ready
	// to build a new network PDU
	// (with a new obfuscated header)
	// and forward it to all bearers.

	unsigned	bufferSize;

	void*
	mem	= _freeStore.allocBuffer(bufferSize);

	if(!mem){
		Oscl::Error::Info::log(
			"%s: out of buffer memory.\n",
			OSCL_PRETTY_FUNCTION
			);
		return 0;
		}

	unsigned
	length	= Oscl::BT::Mesh::Network::Crypto::encrypt(
			_credentials,
			ctl,
			ttl,
			src,
			seq,
			ivIndex,
			transportPDU,
			tranportPduLength,
			mem,
			bufferSize
			);

	if(!length){
		_freeStore.freeBuffer(mem);
		Oscl::Error::Info::log(
			"%s: encrypt failed!\n",
			OSCL_PRETTY_FUNCTION
			);
		return 0;
		}

	Oscl::Pdu::Pdu*
	fixed	= _freeStore.allocFixed(
				mem,
				length
				);

	if(!fixed){
		_freeStore.freeBuffer(mem);
		Oscl::Error::Info::log(
			"%s: out of fixed memory.\n",
			OSCL_PRETTY_FUNCTION
			);
		return 0;
		}

	return fixed;
	}

void	Part::freeRemainingOffers() noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif

	Oscl::BT::Mesh::LPN::Offer*	offer;

	while((offer = _offers.get())){
		_freeOfferMem.put((Oscl::BT::Mesh::LPN::OfferMem*)offer);
		}
	}

void	Part::sendFriendRequest() noexcept{

//	#ifdef DEBUG_TRACE
	#if 1
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif

	preventHcReceiverFromBeingDisabled();

	/*	3.6.6.4.1 Low Power establishment

		The Friend Request message shall be sent with the TTL set to 0 and
		the DST field set to the all-friends address.
		The node shall keep a Low Power node counter (LPNCounter),
		which is a 2-octet value initialized to 0.
		This value shall be sent in the Friend Request message and used to
		derive the friendship security material if a friendship is established
		as a result of the Friend Request message.
		After each Friend Request message is sent, this value shall be incremented by 1.
		The LPNCounter may wrap.
	 */

	/*
		Friend Request PDU [opcode = 0x03]:
		[0]	Criteria (1-octet)
			The criteria that a Friend node should support
			in order to participate in friendship negotiation
			[7]	RFU (set to zero)
			[6:5]	RSSI Factor
					0b00	1
					0b01	1.5
					0b10	2
					0b11	2.5
			[4:3]	Receive Window Factor
					0b00	1
					0b01	1.5
					0b10	2
					0b11	2.5
			[2:0]	Min Queue Size Log
					0b000	Prohibited (do not use)
					0b001	N = 2
					0b010	N = 4
					0b011	N = 8
					0b100	N = 16
					0b101	N = 32
					0b110	N = 64
					0b111	N = 128
		[1]	ReceiveDelay (1-octet)	units of milliseconds.
			Receive delay requested by the Low Power node.
			0x00-0x09	Prohibited
			0x0A-0xFF	milliseconds

		[2:4] PollTimeout (3 octets)
			The initial value of the PollTimeout timer
			set by the Low Power node

			0x000000-0x000009	Prohibited
			0x00000A-0x34BBFF	100*milliseconds (1s - 345,599.9s,95.9h)
			0x34BC00-0xFFFFFF	Prohibited

		[5:6] PreviousAddress (2 octets)
			Unicast address of the primary element of
			the previous friend.

		[7]	NumElements	(1 octet)
			 Number of elements in the Low Power node

		[8:9]	LPNCounter	(2 octets)
			Number of Friend Request messages that
			the Low Power node has sent
	 */

	unsigned	bufferSize;

	void*
	buffer	= _freeStore.allocBuffer(bufferSize);

	if(!buffer){
		Oscl::Error::Info::log(
			"%s: Can't allocate buffer.\n",
			OSCL_PRETTY_FUNCTION
			);
		return;
		}

	Oscl::Encoder::BE::Base	beEncoder(
		buffer,
		bufferSize
		);

	Oscl::Encoder::Api&	encoder	= beEncoder;

	static const uint8_t	opcode		= 0x03;	// Friend Request
	/*
			[7]	RFU (set to zero)
			[6:5]	RSSI Factor
					0b00	1
					0b01	1.5
					0b10	2
					0b11	2.5
			[4:3]	Receive Window Factor
					0b00	1
					0b01	1.5
					0b10	2
					0b11	2.5
			[2:0]	Min Queue Size Log
					0b000	Prohibited (do not use)
					0b001	N = 2
					0b010	N = 4
					0b011	N = 8
					0b100	N = 16
					0b101	N = 32
					0b110	N = 64
					0b111	N = 128
	*/
	static const uint8_t	criteria	= (
		0
		|	2<<5	// RSSI Factor (2)
		|	2<<3	// Receive Window Factor (2)
		|	0x05<<0	// Min Queue Size Log (N=32)
		);

	/*
			0x00-0x09	Prohibited
			0x0A-0xFF	milliseconds
	*/
	static const uint8_t	receiveDelayInMs	= (
		lpnReceiveDelayInMs
		);

	/* 3.6.5.3 Friend Request

		The initial value of the PollTimeout timer
		set by the Low Power node
	 */

	/*
			0x000000-0x000009	Prohibited
			0x00000A-0x34BBFF	100*milliseconds (1s - 345,599.9s,95.9h)
			0x34BC00-0xFFFFFF	Prohibited
	*/

	static const uint32_t	pollTimeoutIn100ms = (
		(pollTimeoutInMs + ((nPollRetries+1)*updateTimerInMs))/100	// units 100ms
		);

	/*
		[5:6] PreviousAddress (2 octets)
			Unicast address of the primary element of
			the previous friend.
	*/
	const uint16_t	previousAddress		= _previousFriendAddress;

	/*
		[7]	NumElements	(1 octet)
			 Number of elements in the Low Power node
	*/
	const uint8_t	nElements	= _context.getNumElements();


	/*
		[8:9]	LPNCounter	(2 octets)
			Number of Friend Request messages that
			the Low Power node has sent
	*/

	++_lpnCounter;

	const uint16_t	lpnCounter	= _lpnCounter;

	encoder.encode(Oscl::BT::Mesh::Address::allFriends);
	encoder.encode(opcode);
	encoder.encode(criteria);
	encoder.encode(receiveDelayInMs);
	encoder.encode24Bit(pollTimeoutIn100ms);
	encoder.encode(previousAddress);
	encoder.encode(nElements);
	encoder.encode(lpnCounter);

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		" dst: 0x%4.4X\n"
		" opcode: 0x%2.2X\n"
		" criteria: 0x%2.2X\n"
		" receiveDelayInMs: %d ms\n"
		" pollTimeout: %d ms\n"
		" previousAddress: 0x%4.4X\n"
		" nElements: %d\n"
		" lpnCounter: %d\n"
		"",
		Oscl::BT::Mesh::Address::allFriends,
		opcode,
		criteria,
		receiveDelayInMs,
		pollTimeoutIn100ms * 100,
		nElements,
		previousAddress,
		lpnCounter
		);
	#endif

	Oscl::Handle<Oscl::Pdu::Fixed>
	fixed	= _freeStore.allocFixed(
				buffer,
				encoder.length()
				);

	if(!fixed){
		Oscl::Error::Info::log(
			"%s: Can't allocate fixed pdu.\n",
			OSCL_PRETTY_FUNCTION
			);
		_freeStore.freeBuffer(buffer);	// FIXME: temporary until we actually transmit
		return;
		}

	const uint32_t	ivIndex	= _context.getNodeApi().getTxIvIndex();
	const uint32_t	seq	= _context.getNodeApi().allocateSequenceNumber();

	_context.getNetTxApi().transmitControl(
		fixed,			// Oscl::Pdu::Pdu* pdu,
		ivIndex,		// uint32_t        ivIndex,
		seq,			// uint32_t        seq,
		_lpnAddress,	// uint16_t        src,
		Oscl::BT::Mesh::Address::allFriends,	// uint16_t        dst,
		0x00			// uint8_t         ttl
		);
	}

void	Part::rxControl(
			const void*	upperTransportAccessPdu,
			unsigned	pduLength,
			uint32_t	ivIndex,
			uint32_t	seq,
			uint16_t	src,
			uint16_t	dst,
			uint8_t		ttl,
			uint8_t		opcode
			) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif

	if(_stopping){
		return;
		}

	/*	The rxControl operation is invoked by
		the Network layer when it receives a
		control message encrypted with
		the master security credentials.
	 */

	/*
		Depending on the value of the Publish Friendship
		Credentials Flag (see Section 4.2.2.4), the Low Power
		node sends a message using either the friendship
		security credentials or the master security credentials
		(see Section 3.8.6.3.1).

		FIXME:
		Is that correct? I think this only applies to
		publication.
	 */

	switch(opcode){
		case 0x04:
			// Friend Offer
			processFriendOffer(
				upperTransportAccessPdu,
				pduLength,
				ivIndex,
				seq,
				src,
				dst
				);
			break;
		case 0x03:
			// Friend Request
			// Ignore looped-back all-friends broadcasts
			break;
		case 0x01:
			// Friend Poll
		case 0x02:
			// Friend Update
		case 0x05:
			// Friend Clear
		case 0x06:
			// Friend Clear Confirm
		case 0x07:
			// Friend Subscription List Add
		case 0x08:
			// Friend Subscription List Remove
		case 0x09:
			// Friend Subscription List Confirm
		default:
			Oscl::Error::Info::log(
				"%s: Unexpected network encrypted control PDU opcode: 0x%2.2X.\n",
				OSCL_PRETTY_FUNCTION,
				opcode
				);
			return;
		}

	}

bool	Part::rxLpnPDU(
			const void*		frame,
			unsigned		length,
			uint32_t		ivIndex,
			uint32_t		seq,
			uint16_t		src,
			uint16_t		dst,
			bool			ctl,
			uint8_t			ttl
			) noexcept{


	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif

	if(_stopping){
		return false;
		}

	/*	The rxLpnPDU operation is invoked by the
		Network layer when it receives a message
		encrypted with the master security credentials.

		This is probably never going to be wanted
		by a LPN.
	 */

	return false;
	}

void	Part::processFriendOffer(
			const void*	upperTransportPdu,
			unsigned	pduLength,
			uint32_t	ivIndex,
			uint32_t	seq,
			uint16_t	src,
			uint16_t	dst
			) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif

	/*
		Friend Offer PDU [opcode = 0x03]:
		[0]	ReceiveWindow (1-octet)
			Receive Window value supported by the Friend node.
			0x00		Prohibited
			0x01-0xFF	Units of 1 ms.

		[1]	QueueSize (1-octet)
			Queue Size available on the Friend node
			- number of messages

		[2]	SubscriptionListSize (1-octet)
			Size of the Subscription List that can be
			supported by a Friend node for a Low Power node
			- number of entries

		[3]	RSSI (1-octet)
			RSSI measured by the Friend node.
			units dBm
			0x7F (127 dBm) if the signal strength is not available

		[4]	FriendCounter (2-octets)
			Number of Friend Offer messages that the Friend node has sent.
			The FriendCounter field value is set to the number of Friend Offer
			messages that the Friend node has sent.
	 */

	Oscl::Endian::Decoder::Linear::Part
	beDecoder(
		upperTransportPdu,
		pduLength
		);

	Oscl::Decoder::Api&	decoder	= beDecoder.be();

	uint8_t		opcodeUnused;
	uint8_t		receiveWindowInMs;
	uint8_t		queueSizeInMessages;
	uint8_t		subscriptionListSizeInEntries;
	uint8_t		rssiInDBm;
	uint16_t	friendCounter;

	decoder.decode(opcodeUnused);
	decoder.decode(receiveWindowInMs);
	decoder.decode(queueSizeInMessages);
	decoder.decode(subscriptionListSizeInEntries);
	decoder.decode(rssiInDBm);
	decoder.decode(friendCounter);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			OSCL_PRETTY_FUNCTION
			);
		return;
		}

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		" receiveWindowInMs: %u ms\n"
		" queueSizeInMessages: %u messages\n"
		" subscriptionListSizeInEntries: %u entries\n"
		" rssiInDBm: %u dBm\n"
		" friendCounter: %u\n"
		"",
		receiveWindowInMs,
		queueSizeInMessages,
		subscriptionListSizeInEntries,
		rssiInDBm,
		friendCounter
		);
	#endif

	Oscl::BT::Mesh::LPN::OfferMem*
	mem	= _freeOfferMem.get();
	if(!mem){
		Oscl::Error::Info::log(
			"%s: out of offer mem.\n",
			OSCL_PRETTY_FUNCTION
			);
		return;
		}

	Oscl::BT::Mesh::LPN::Offer*
	offer	= new(mem)
				Oscl::BT::Mesh::LPN::Offer(
					src,
					receiveWindowInMs,
					queueSizeInMessages,
					subscriptionListSizeInEntries,
					rssiInDBm,
					friendCounter,
					_lpnCounter
					);

	if(!_offers.first()){
		startOfferTimer();
		}

	_offers.put(offer);
	}

void	Part::processFriendUpdate(
			const void*	upperTransportPdu,
			unsigned	pduLength,
			uint32_t	ivIndex,
			uint32_t	seq,
			uint16_t	src,
			uint16_t	dst
			) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#ifdef DEBUG_TRACE_TMI
	Oscl::Error::Info::hexDump(
		upperTransportPdu,
		pduLength
		);
	#endif
	#endif

	/*	3.6.6.4.2 Low Power messaging

		If the Low Power node receives a response from the Friend node,
		then it shall toggle the Friend Sequence Number.
	 */

	Oscl::Endian::Decoder::Linear::Part
	beDecoder(
		upperTransportPdu,
		pduLength
		);

	Oscl::Decoder::Api&	decoder	= beDecoder.be();

	uint8_t		flags;
	uint32_t	currentIvIndex;
	uint8_t		md;

	decoder.decode(flags);
	decoder.decode(currentIvIndex);
	decoder.decode(md);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			OSCL_PRETTY_FUNCTION
			);
		return;
		}

	bool	ivUpdateFlag	= ((flags >> 1) & 0x01)?true:false;
	bool	keyRefreshFlag	= ((flags >> 0) & 0x01)?true:false;

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		" flags: (0x%2.2X):\n"
		"   Key Refresh Flag: %s\n"
		"   IV Update Flag: %s\n"
		" IvIndex: 0x%8.8X\n"
		" MD: 0x%2.2X\n"
		"",
		flags,
		keyRefreshFlag?"true":"false",	// Key Refresh Flag
		ivUpdateFlag?"true":"false",	// IV Update Flag
		currentIvIndex,
		md
		);
	#endif

	_context.getNodeApi().secureNetworkBeaconReceived(
		currentIvIndex,
		keyRefreshFlag,
		ivUpdateFlag
		);

	/* 3.6.6.4.2 Low Power messaging

		A Low Power node that is friends with a Friend node shall
		send a Friend Poll message to the Friend node *before*
		the PollTimeout timer expires.
	 */

	_wantsMorePoll	= ((md >> 0) & 0x01)?true:false;

	sendNextSubscriptionTransaction();
	}

void	Part::sendNextSubscriptionTransaction() noexcept{

	if(_pendingConfirmList.first()){
		// We have pending confirmation.
		// A timer should take care of this.
		return;
		}

	if(_pendingUnsubscribeList.first()){
		sendFriendSubscriptionListRemove();
		return;
		}

	if(_pendingSubscribeList.first()){
		sendFriendSubscriptionListAdd();
		return;
		}

	allowHcReceiverToBeDisabled();
	}

void	Part::sendFriendSubscriptionListAdd() noexcept{

//	#ifdef DEBUG_TRACE
	#if 1
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif

	uint8_t		buffer[maxNetworkControlTransportPduSize+1]; // +dst
	
	Oscl::Encoder::BE::Base	beEncoder(
		buffer,
		sizeof(buffer)
		);

	Oscl::Encoder::Api&	encoder	= beEncoder;

	static const uint8_t	opcode		= 0x07;	// Friend Subscription List Add

	/*
		[0]	TransactionNumber (1 octet)  The number for identifying a transaction
				The TransactionNumber field is used to distinguish each
				individual transaction (see Section 3.6.6.4.3).

		[1:2]*N	AddressList  List of group addresses and virtual addresses
				where N is the number of group addresses and virtual addresses
				in this message

	 */

	const uint32_t	seq	= _context.getNodeApi().allocateSequenceNumber();

	uint16_t	dst	= _offer->_friendAddress;

	encoder.encode(dst);
	encoder.encode(opcode);
	encoder.encode(_subscriptionTransactionNumber);

	SubscribeRecord*	r;

	while((r = _pendingSubscribeList.get())){
		encoder.encode(r->address());
		if(encoder.overflow()){
			_pendingSubscribeList.push(r);
			break;
			}
		_pendingConfirmList.put(r);
		}

	const uint32_t	ivIndex	= _context.getNodeApi().getTxIvIndex();

	startSubscriptionTimer();

	/*	3.6.6.4.2 Low Power messaging
		In a Friend Poll message, the TTL field shall be set to 0.
	 */

	Oscl::Handle<Oscl::Pdu::Pdu>
	pdu	= buildNetworkPDU(
			true,				// bool		ctl,
			0,					// uint8_t	ttl,
			_lpnAddress,		// uint16_t	src,
			seq,				// uint32_t	seq,
			ivIndex,			// uint32_t	ivIndex,
			buffer,				// const void*	transportPDU,
			encoder.length()	// unsigned	tranportPduLength
			);

	if(!pdu){
		return;
		}

	_context.getNetTxApi().transmitNetworkPDU(
		pdu,	// Oscl::Pdu::Pdu* pdu,
		dst		// uint16_t        dst
		);
	}

void	Part::sendFriendSubscriptionListRemove() noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif

	uint8_t		buffer[maxNetworkControlTransportPduSize+1]; // +dst
	
	Oscl::Encoder::BE::Base	beEncoder(
		buffer,
		sizeof(buffer)
		);

	Oscl::Encoder::Api&	encoder	= beEncoder;

	static const uint8_t	opcode		= 0x08;	// Friend Subscription List Remove

	/*
		[0]	TransactionNumber (1 octet)  The number for identifying a transaction
				The TransactionNumber field is used to distinguish each
				individual transaction (see Section 3.6.6.4.3).

		[1:2]*N	AddressList  List of group addresses and virtual addresses
				where N is the number of group addresses and virtual addresses
				in this message

	 */

	const uint32_t	seq	= _context.getNodeApi().allocateSequenceNumber();

	uint16_t	dst	= _offer->_friendAddress;

	encoder.encode(dst);
	encoder.encode(opcode);
	encoder.encode(_subscriptionTransactionNumber);

	SubscribeRecord*	r;

	while((r = _pendingUnsubscribeList.get())){
		encoder.encode(r->address());
		if(encoder.overflow()){
			_pendingSubscribeList.push(r);
			break;
			}
		_pendingConfirmList.put(r);
		}

	const uint32_t	ivIndex	= _context.getNodeApi().getTxIvIndex();

	startSubscriptionTimer();

	/*	3.6.6.4.2 Low Power messaging
		In a Friend Poll message, the TTL field shall be set to 0.
	 */

	Oscl::Handle<Oscl::Pdu::Pdu>
	pdu	= buildNetworkPDU(
			true,				// bool		ctl,
			0,					// uint8_t	ttl,
			_lpnAddress,		// uint16_t	src,
			seq,				// uint32_t	seq,
			ivIndex,			// uint32_t	ivIndex,
			buffer,				// const void*	transportPDU,
			encoder.length()	// unsigned	tranportPduLength
			);

	if(!pdu){
		return;
		}

	_context.getNetTxApi().transmitNetworkPDU(
		pdu,	// Oscl::Pdu::Pdu* pdu,
		dst		// uint16_t        dst
		);
	}

void	Part::retransmitFriendSubscriptionList() noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif

	uint8_t		buffer[maxNetworkControlTransportPduSize+1]; // +dst
	
	Oscl::Encoder::BE::Base	beEncoder(
		buffer,
		sizeof(buffer)
		);

	Oscl::Encoder::Api&	encoder	= beEncoder;

	SubscribeRecord*
	r	= _pendingConfirmList.first();

	if(!r){
		sendNextSubscriptionTransaction();
		return;
		}

	uint8_t	opcode	= (r->present())
						? 0x07	// Friend Subscription List Add
						: 0x08	// Friend Subscription List Remove
						; 

	/*
		[0]	TransactionNumber (1 octet)  The number for identifying a transaction
				The TransactionNumber field is used to distinguish each
				individual transaction (see Section 3.6.6.4.3).

		[1:2]*N	AddressList  List of group addresses and virtual addresses
				where N is the number of group addresses and virtual addresses
				in this message

	 */

	const uint32_t	seq	= _context.getNodeApi().allocateSequenceNumber();

	uint16_t	dst	= _offer->_friendAddress;

	encoder.encode(dst);
	encoder.encode(opcode);
	encoder.encode(_subscriptionTransactionNumber);

	for(
		r	= _pendingConfirmList.first();
		r;
		r	= _pendingConfirmList.next(r)
		){
		encoder.encode(r->address());

		if(encoder.overflow()){
			// This should *NEVER* happen since we've
			// already previously transmited the members
			// of _pendingConfirmList previously.
			Oscl::Error::Info::log(
				"%s: unexpected subscription overflow!\n",
				OSCL_PRETTY_FUNCTION
				);
			break;
			}
		}

	const uint32_t	ivIndex	= _context.getNodeApi().getTxIvIndex();

	startSubscriptionTimer();

	/*	3.6.6.4.2 Low Power messaging
		In a Friend Poll message, the TTL field shall be set to 0.
	 */

	Oscl::Handle<Oscl::Pdu::Pdu>
	pdu	= buildNetworkPDU(
			true,				// bool		ctl,
			0,					// uint8_t	ttl,
			_lpnAddress,		// uint16_t	src,
			seq,				// uint32_t	seq,
			ivIndex,			// uint32_t	ivIndex,
			buffer,				// const void*	transportPDU,
			encoder.length()	// unsigned	tranportPduLength
			);

	if(!pdu){
		return;
		}

	_context.getNetTxApi().transmitNetworkPDU(
		pdu,	// Oscl::Pdu::Pdu* pdu,
		dst		// uint16_t        dst
		);
	}

void	Part::processFriendSubscriptionListConfirm(
			const void*	upperTransportPdu,
			unsigned	pduLength,
			uint32_t	ivIndex,
			uint32_t	seq,
			uint16_t	src,
			uint16_t	dst
			) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif

	Oscl::Endian::Decoder::Linear::Part
	beDecoder(
		upperTransportPdu,
		pduLength
		);

	Oscl::Decoder::Api&	decoder	= beDecoder.be();

	uint8_t		transactionNumber;

	decoder.decode(transactionNumber);

	if(decoder.underflow()){
		Oscl::Error::Info::log(
			"%s: underflow!\n",
			OSCL_PRETTY_FUNCTION
			);
		return;
		}

	if(_subscriptionTransactionNumber != transactionNumber){
		Oscl::Error::Info::log(
			"%s: unexpected transactionNumber: %u!\n",
			OSCL_PRETTY_FUNCTION,
			transactionNumber
			);
		return;
		}

	_subscriptionTimerApi.stop();

	++_subscriptionTransactionNumber;

	SubscribeRecord*	r;

	while((r = _pendingConfirmList.get())){
		if(r->present()){
			_activeSubscribeList.put(r);
			continue;
			}
		free(r);
		}

	sendNextSubscriptionTransaction();
	}

bool	Part::rxNetwork(
			const void*	frame,
			unsigned	length
			) noexcept{

	#ifdef DEBUG_TRACE_TMI
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif

	if(_stopping){
		return false;
		}

	if(!_offer){
		#ifdef DEBUG_TRACE
		Oscl::Error::Info::log(
			"%s: discard. No current friend.\n",
			__PRETTY_FUNCTION__
			);
		#endif

		// There is no current friendship.
		// This may happen due to an update timeout.
		// At this point the _credentials are invalid.
		return false;
		}

	using namespace Oscl::BT::Mesh::Network;

	uint32_t	ivIndex;
	uint8_t		ctl;
	uint8_t		ttl;
	uint32_t	seq;
	uint16_t	src;

	uint8_t		plaintext[128];

	unsigned
	len	= Oscl::BT::Mesh::Network::Crypto::decrypt(
			_context.getNodeApi(),
			_credentials,
			frame,
			length,
			plaintext,
			ivIndex,
			ctl,
			ttl,
			seq,
			src
			);

	if(!len){
		#ifdef DEBUG_TRACE_TMI
		Oscl::Error::Info::log(
			"%s: decrypt failed.\n",
			__PRETTY_FUNCTION__
			);
		#endif
		return false;
		}

	// At this point, we have received a valid
	// Network PDU for this LPN Friend "network".

	bool
	duplicate	= _cache.messageInCache(
					src,
					seq,
					ivIndex
					);

	if(duplicate){
		#ifdef DEBUG_TRACE_TMI
		Oscl::Error::Info::log(
			"Duplicate LPN message (src:0x%4.4X,seq:0x%6.6X,ivIndex: 0x%8.8X) in cache. Dropped.\n",
			src,
			seq,
			ivIndex
			);
		#endif
		return true;
		}

	_cache.addToCache(
		src,
		seq,
		ivIndex
		);

	// toggle the FSN
	_fsn	= !_fsn;

	_pollRetries	= nPollRetries;

	_wantsMorePoll	= true;

	uint16_t	dst;
	unsigned	remaining;
	unsigned	lowerTransportPduOffset;
		{
		Oscl::Endian::Decoder::Linear::Part
		beDecoder(
			plaintext,
			len
			);

		Oscl::Decoder::Api&	decoder	= beDecoder.be();

		decoder.decode(dst);

		if(decoder.underflow()){
			Oscl::Error::Info::log(
				"%s: underflow!\n",
				OSCL_PRETTY_FUNCTION
				);
			// Return true, since there's something
			// wrong with this PDU (or the preceding
			// software, and we don't want to relay
			// this PDU.
			return true;
			}

		remaining	= decoder.remaining();

		lowerTransportPduOffset	= len - remaining;
		}

	#ifdef DEBUG_TRACE_TMI
	Oscl::Error::Info::log(
		"%s:"
		" ctl: (%u) \"%s\","
		" ttl: (0x%2.2X) %u,"
		" seq: (0x%6.6X) %u,"
		" src: (0x%4.4X)"
		" dst: (0x%4.4X)"
		" ivIndex: (0x%8.8X)"
		"\n",
		__PRETTY_FUNCTION__,
		ctl,
		ctl?"control":"network",
		ttl,
		ttl,
		seq,
		seq,
		src,
		dst,
		ivIndex
		);
	#endif

	bool
	locallyDestined	= processLocalNetworkPDU(
						&plaintext[lowerTransportPduOffset],
						remaining,
						ivIndex,
						seq,
						src,
						dst,
						ctl,
						ttl
						);

	if(_wantsMorePoll){
		_pollRetries	= nPollRetries;
		sendFriendPoll();
		}
	else {
		startPollTimer();
		}

	if(locallyDestined){
		return true;
		}

	return true;
	}

void	Part::rxBeacon(
			uint32_t	ivIndex,
			bool		keyRefreshFlag,
			bool		ivUpdateFlag
			) noexcept{
	// This is called from the Network when
	// a secure update beacon is received.

	// This is not used by a LPN, since this
	// information is normally received through
	// the Friend Update message.
	}

bool	Part::processLocalNetworkPDU(
			const void*		frame,
			unsigned		length,
			uint32_t		ivIndex,
			uint32_t		seq,
			uint16_t		src,
			uint16_t		dst,
			bool			ctl,
			uint8_t			ttl
			) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s:\n"
		"\tctl: %s\n"
		"\tttl: %u\n"
		"\tsrc: 0x%4.4X\n"
		"\tdst: 0x%4.4X\n"
		"\tseq: 0x%8.8X\n"
		"",
		__PRETTY_FUNCTION__,
		ctl?"true":"false",
		ttl,
		src,
		dst,
		seq
		);

	Oscl::Error::Info::hexDump(
		frame,
		length
		);
	#endif

	// At this point, we have a message
	// that has been received using the
	// friendship credentials and, therefore,
	// has been intentionally sent to this
	// LPN by a Friend from its queue in
	// response to a poll.

	if(!findActive(dst) && (dst != _lpnAddress)){
		// Ultimately, we will need to handle
		// messages addressed to other nodes
		// and relay them.
		return false;
		}

	if(ctl){
		// At this point, we have a control
		// message destined for this node.
		// Transport Control Message
		processLowerTransportControlMessage(
			src,
			dst,
			seq,
			ivIndex,
			ttl,
			frame,
			length
			);
		}
	else {
		processLowerTransportMessage(
			src,
			dst,
			seq,
			ivIndex,
			ttl,
			frame,
			length
			);
		}

	return true;
	}

void	Part::processLowerTransportControlMessage(
			uint16_t	srcAddress,
			uint16_t	dstAddress,
			uint32_t	seq,
			uint32_t	ivIndex,
			uint8_t		ttl,
			const void*	lowerTransportPDU,
			unsigned	length
			) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n"
		"\tsrcAddress: 0x%4.4X\n"
		"\tdstAddress: 0x%4.4X\n"
		"\tseq: 0x%6.6X\n"
		"",
		__PRETTY_FUNCTION__,
		srcAddress,
		dstAddress,
		seq
		);
	#ifdef DEBUG_TRACE_TMI
	Oscl::Error::Info::hexDump(
		lowerTransportPDU,
		length
		);
	#endif
	#endif

	// At this point we have a Lower Transport Control
	// PDU decrypted with friendship security credentials
	// which may or may not be segmented.

	/* 3.6.5 Transport Control messages

		The Transport Control messages can be transmitted using
		either a single Unsegmented Control message or sequence
		of Segmented Control messages.

		Each of these messages has a 7-bit opcode field that
		determines the format of the parameters field.

		Each Transport Control Message shall be sent in the
		smallest number of Lower Transport PDUs possible.
	 */

	// At this point, we have a Lower Transport Control PDU
	//	[0] - First octet
	//		[7] - SEG Field
	//			0 - Unsegmented
	//			1 - Segmented
	//		[6:0] - Opcode

	uint8_t		segOpcode;
	unsigned	remaining;
		{
		Oscl::Endian::Decoder::Linear::Part
		beDecoder(
			lowerTransportPDU,
			length
			);

		Oscl::Decoder::Api&	decoder	= beDecoder.be();

		decoder.decode(segOpcode);

		if(decoder.underflow()){
			Oscl::Error::Info::log(
				"%s: underflow!\n",
				OSCL_PRETTY_FUNCTION
				);
			return;
			}

		remaining	= decoder.remaining();
		}

	bool
	segmented	= (segOpcode >> 7)?true:false;

	uint8_t
	opcode	= segOpcode & 0x7F;

	if(segmented){
		processSegmentedLowerTransportControlMessage(
			srcAddress,
			dstAddress,
			seq,
			ivIndex,
			ttl,
			lowerTransportPDU,
			length,
			opcode
			);
		return;
		}

	// At this point, we have an unsegmented
	// control message.

	const uint8_t*	p	= (const uint8_t*)lowerTransportPDU;

	processUnsegmentedLowerTransportControlMessage(
		srcAddress,
		dstAddress,
		seq,
		ivIndex,
		ttl,
		&p[sizeof(segOpcode)],
		remaining,
		opcode
		);

	return;
	}

void	Part::processLowerTransportMessage(
			uint16_t	srcAddress,
			uint16_t	dstAddress,
			uint32_t	seq,
			uint32_t	ivIndex,
			uint8_t		ttl,
			const void*	lowerTransportPDU,
			unsigned	length
			) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif

	// At this point we have a Lower Transport PDU
	// that is *not* a Control PDU that has been
	// decrypted with friendship security credentials
	// which may or may not be segmented.

	// At this point, we have a Lower Transport PDU
	//	[0] - First octet
	//		[7] - SEG Field
	//			0 - Unsegmented
	//			1 - Segmented
	//		[6] - AKF Field
	//			0 - Device
	//			1 - Application
	//		[6:0] - AID Field

	uint8_t		segAkfAid;
	unsigned	remaining;
		{
		Oscl::Endian::Decoder::Linear::Part
		beDecoder(
			lowerTransportPDU,
			length
			);

		Oscl::Decoder::Api&	decoder	= beDecoder.be();

		decoder.decode(segAkfAid);

		if(decoder.underflow()){
			Oscl::Error::Info::log(
				"%s: underflow!\n",
				OSCL_PRETTY_FUNCTION
				);
			return;
			}

		remaining	= decoder.remaining();
		}

	bool
	segmented	= (segAkfAid >> 7)?true:false;

	bool
	akf	= ((segAkfAid >> 6) & 0x01)?true:false;

	uint8_t
	aid	= (segAkfAid & 0x3F);

	const uint8_t*	p = (const uint8_t*)lowerTransportPDU;

	unsigned
	upperTransportAccessPduOffset	= length - remaining;

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s: segmented: %s\n",
		__PRETTY_FUNCTION__,
		segmented?"true":"false"
		);
	#endif

	if(!segmented){
		// No need for reassembly
		// Forward to transport PDU to
		// all Elements. The Elements
		// will determine if they want
		// the Upper Transport PDU based
		// on the akf/aid, destination
		// address, etc.
		forwardUpperTransportPduToAllElements(
			&p[upperTransportAccessPduOffset],
			remaining,
			ivIndex,
			seq,
			srcAddress,
			dstAddress,
			aid,
			akf,
			false
			);
		return;
		}

	// At this point, we must reassemble the PDU

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s: need reassbly\n"
		"\tSegment:\n"
		"",
		__PRETTY_FUNCTION__
		);

	Oscl::Error::Info::hexDump(
		lowerTransportPDU,
		length
		);
	#endif

	bool	handled	= false;

	for(
			Oscl::BT::Mesh::Transport::
			Lower::Reassemble::Access::Reaper*
			reaper	= _activeAccessReapers.first();
			reaper;
			reaper	= _activeAccessReapers.next(reaper)
		){
		handled	= reaper->receive(
			srcAddress,
			dstAddress,
			seq,
			lowerTransportPDU,
			length
			);
		}

	if(handled){
		return;
		}

	AccessReaperMem*
	mem	= _freeAccessReaperMem.get();
	if(!mem){
		Oscl::Error::Info::log(
			"%s: out of reaper memory\n",
			OSCL_PRETTY_FUNCTION
			);
		return;
		}

	Oscl::BT::Mesh::Transport::
	Lower::Reassemble::Access::Reaper*
	reaper	= new(&mem->mem.reaper)
				Oscl::BT::Mesh::Transport::
				Lower::Reassemble::Access::Reaper(
					*this,	// context,
					_freeStore,
					*mem->mem.ackTimer,
					*mem->mem.incompleteTimer,
					_context.getNetTxApi(),
					_upperTransportRxComposer,
					ivIndex,
					seq,
					srcAddress,
					dstAddress,
					ttl,
					aid,
					akf,
					true	// noAck
					);

	_activeAccessReapers.put(reaper);

	reaper->start();

	reaper->receive(
		srcAddress,
		dstAddress,
		seq,
		lowerTransportPDU,
		length
		);

	}

void	Part::forwardUpperTransportPduToAllElements(
			const void*	upperTransportAccessPdu,
			unsigned	pduLength,
			uint32_t	ivIndex,
			uint32_t	seq,
			uint16_t	src,
			uint16_t	dst,
			uint8_t		aid,
			bool		akf,
			bool		segmented
			) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif

	_context.getNetTxApi().forwardUpperTransportPduToAllElements(
		upperTransportAccessPdu,
		pduLength,
		ivIndex,
		seq,
		src,
		dst,
		aid,
		akf,
		segmented
		);
	}

void	Part::processUnsegmentedLowerTransportControlMessage(
			uint16_t	srcAddress,
			uint16_t	dstAddress,
			uint32_t	seq,
			uint32_t	ivIndex,
			uint8_t		ttl,
			const void*	lowerTransportPDU,
			unsigned	length,
			uint8_t		opcode
			) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif

	// At this point we have a Lower Transport Control PDU
	// that is *not* a Control PDU that has been
	// decrypted with friendship security credentials
	// which may or may not be segmented.

	// For a Friend using friendship security
	// credentials, we do not currently
	// handle segmented control messages,
	// since the "normal" Friend control
	// messages are small enough to fit within
	// a single control PDU.

	if(!opcode){
		// This is an ACK.
		Oscl::Error::Info::log(
			"%s: ACK NOT IMPLEMENTED!\n",
			OSCL_PRETTY_FUNCTION
			);
		return;
		}

	switch(opcode){
		case 0x02:
			// Friend Update
			processFriendUpdate(
				lowerTransportPDU,
				length,
				ivIndex,
				seq,
				srcAddress,
				dstAddress
				);
			break;
		case 0x09:
			// Friend Subscription List Confirm
			processFriendSubscriptionListConfirm(
				lowerTransportPDU,
				length,
				ivIndex,
				seq,
				srcAddress,
				dstAddress
				);
			break;
		case 0x03:
			// Friend Request
			// Ignore looped-back all-friends broadcasts
			break;
		case 0x01:
			// Friend Poll
		case 0x04:
			// Friend Offer
		case 0x05:
			// Friend Clear
		case 0x06:
			// Friend Clear Confirm
		case 0x07:
			// Friend Subscription List Add
		case 0x08:
			// Friend Subscription List Remove
		default:
			Oscl::Error::Info::log(
				"%s: Unexpected network encrypted control PDU opcode: 0x%2.2X.\n",
				OSCL_PRETTY_FUNCTION,
				opcode
				);
			return;
		}

	}

void	Part::processSegmentedLowerTransportControlMessage(
			uint16_t	srcAddress,
			uint16_t	dstAddress,
			uint32_t	seq,
			uint32_t	ivIndex,
			uint8_t		ttl,
			const void*	lowerTransportPDU,
			unsigned	length,
			uint8_t		opcode
			) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif

	Oscl::Error::Info::log(
		"%s: segmented transport control message NOT IMPLEMENTED!\n",
		OSCL_PRETTY_FUNCTION
		);

	// At this point we have a Lower Transport Control PDU
	// that is *not* a Control PDU that has been
	// decrypted with friendship security credentials
	// which may or may not be segmented.

	// For a Friend using friendship security
	// credentials, we do not currently
	// handle segmented control messages,
	// since the "normal" Friend control
	// messages are small enough to fit within
	// a single control PDU.
	}

bool	Part::wantsDestination(uint16_t address) const noexcept{

	#ifdef DEBUG_TRACE_TMI
	Oscl::Error::Info::log(
		"%s: address: 0x%4.4X\n",
		__PRETTY_FUNCTION__,
		address
		);
	#endif

	if(address == Oscl::BT::Mesh::Address::allFriends){
		return true;
		}

	return false;
	}

void	Part::generateFriendshipSecurityMaterial(
			const void*	netKey,
			uint16_t	lpnAddress,
			uint16_t	friendAddress,
			uint16_t	lpnCounter,
			uint16_t	friendCounter
			) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s:\n"
		" lpnAddress: 0x%4.4X\n"
		" friendAddress: 0x%4.4X\n"
		" lpnCounter: 0x%8.8X\n"
		" friendCounter: 0x%8.8X\n"
		" netKey:\n"
		"",
		__PRETTY_FUNCTION__,
		lpnAddress,
		friendAddress,
		lpnCounter,
		friendCounter
		);

	Oscl::Error::Info::hexDump(
		netKey,
		16
		);
	#endif

	/*
	The friendship security material is derived from the friendship
	security credentials using:

	NID || EncryptionKey || PrivacyKey = k2(
			NetKey,
			0x01
		||	LPNAddress
		||	FriendAddress
		||	LPNCounter
		||	FriendCounter
		)
	 */

	const uint8_t	one = 0x01;

	uint8_t	buffer[
				sizeof(one)
			+	sizeof(lpnAddress)
			+	sizeof(friendAddress)
			+	sizeof(lpnCounter)
			+	sizeof(friendCounter)
			];

	Oscl::Encoder::BE::Base
	beEncoder(
		buffer,
		sizeof(buffer)
		);

	Oscl::Encoder::Api&	encoder	= beEncoder;

	encoder.encode(one);
	encoder.encode(lpnAddress);
	encoder.encode(friendAddress);
	encoder.encode(lpnCounter);
	encoder.encode(friendCounter);

	uint8_t	k2Output[48];

	Oscl::BT::Crypto::Keys::k2(
		netKey,
		buffer,
		encoder.length(),
		k2Output
		);

	memcpy(
		_credentials._privacyKey,
		&k2Output[sizeof(k2Output) - 16],
		16
		); /* 128-bit */

	memcpy(
		_credentials._encryptionKey,
		&k2Output[sizeof(k2Output) - 32],
		16
		); /* 128-bit */

	_credentials._nid	= k2Output[sizeof(k2Output) - 33] & 0x7f; /* 7-bit */
	}

SubscribeRecord*	Part::findActiveSubscription(uint16_t address) noexcept{
	for(
		SubscribeRecord*
		r	= _activeSubscribeList.first();
		r;
		r	= _activeSubscribeList.next(r)
		){
			if(r->address() == address){
				return r;
				}
		}
	return 0;
	}

SubscribeRecord*	Part::findPendingActiveSubscription(uint16_t address) noexcept{
	for(
		SubscribeRecord*
		r	= _pendingSubscribeList.first();
		r;
		r	= _pendingSubscribeList.next(r)
		){
			if(r->address() == address){
				return r;
				}
		}

	return 0;
	}

SubscribeRecord*	Part::findPendingUnsubscribe(uint16_t address) noexcept{
	for(
		SubscribeRecord*
		r	= _pendingSubscribeList.first();
		r;
		r	= _pendingSubscribeList.next(r)
		){
			if(r->address() == address){
				return r;
				}
		}

	return 0;
	}

SubscribeRecord*	Part::findActive(uint16_t address) noexcept{
	SubscribeRecord*
	r	= findActiveSubscription(address);

	if(r){
		return r;
		}

	r	= findPendingActiveSubscription(address);

	if(r){
		return r;
		}

	return 0;
	}

void	Part::createNewSubscriptions() noexcept{

	_subscriptionIterationApi.iterate(
		[&] (uint16_t address, const void* labelUUID) {

			SubscribeRecord*
			rec	= findActive(address);

			if(rec){
				// We already know this address
				return false;
				}

			SubscribeRecordMem*
			mem	= _freeSubscribeRecordMem.get();

			if(!mem){
				// No more memory.
				return true;
				}
			SubscribeRecord*
			r	= new(mem) SubscribeRecord(address);

			_pendingSubscribeList.put(r);

			return false;
			}
		);
	}

void	Part::markAllSubscriptionsAbsent() noexcept{
	for(
		SubscribeRecord*
		r	= _activeSubscribeList.first();
		r;
		r	= _activeSubscribeList.next(r)
		){
			r->markAbsent();
		}

	for(
		SubscribeRecord*
		r	= _pendingSubscribeList.first();
		r;
		r	= _pendingSubscribeList.next(r)
		){
			r->markAbsent();
		}

	for(
		SubscribeRecord*
		r	= _pendingUnsubscribeList.first();
		r;
		r	= _pendingUnsubscribeList.next(r)
		){
			// These should already be marked as
			// absent.
			r->markAbsent();
		}
	}

void	Part::markPresent() noexcept{

	_subscriptionIterationApi.iterate(
		[&] (uint16_t address, const void* labelUUID) {

			SubscribeRecord*
			rec	= findActive(address);

			if(rec){
				rec->markPresent();
				return false;
				}

			rec	= findPendingUnsubscribe(address);

			if(rec){
				rec->markPresent();
				}

			return false;
			}
		);
	}

void	Part::free(SubscribeRecord* r) noexcept{

	r->~SubscribeRecord();

	_freeSubscribeRecordMem.put((SubscribeRecordMem*)r);
	}

void	Part::moveAbsentToUnsubcribe() noexcept{
	Oscl::Queue<SubscribeRecord>	_tmpQ;

	SubscribeRecord*	r;

	/*	The _activeSubscribeList contains subscriptions
		which have been added to the Friend.
		Therefore, they need to be Removed.
	 */
	while((r = _activeSubscribeList.get())){

		if(r->present()){
			_tmpQ.put(r);
			continue;
			}

		_pendingUnsubscribeList.put(r);
		}

	_activeSubscribeList	= _tmpQ;

	/*	The _pendingSubscribeList contains subscriptions
		which have NOT been added to the Friend.
		Therefore, absent subscriptions simply need
		to be destroyed.
	 */
	while((r = _pendingSubscribeList.get())){
		if(r->present()){
			_tmpQ.put(r);
			continue;
			}
		free(r);
		}

	_pendingSubscribeList	= _tmpQ;
	}

void	Part::movePresentToActive() noexcept{
	Oscl::Queue<SubscribeRecord>	_tmpQ;

	SubscribeRecord*	r;

	/*	The _pendingUnsubscribeList contains
		subscriptions that are still known
		to the Friend Node and need to be
		Removed.
		Therefore, if any are present, they
		will simply be moved to the
		_activeSubscribeList.
	 */
	while((r = _pendingUnsubscribeList.get())){
		if(!r->present()){
			_tmpQ.put(r);
			continue;
			}
		_activeSubscribeList.put(r);
		}

	_pendingUnsubscribeList	= _tmpQ;

	}

void	Part::subscriptionListChanged() noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		__PRETTY_FUNCTION__
		);
	#endif

	markAllSubscriptionsAbsent();

	createNewSubscriptions();

	markPresent();

	moveAbsentToUnsubcribe();

	movePresentToActive();

	}

