/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_bluetooth_mesh_lpn_parth_
#define _oscl_bluetooth_mesh_lpn_parth_

#include <stdint.h>
#include <string.h>
#include "oscl/memory/block.h"
#include "oscl/done/operation.h"
#include "oscl/frame/fwd/composer.h"
#include "oscl/frame/fwd/itemcomp.h"
#include "oscl/bluetooth/mesh/bearer/tx/api.h"
#include "oscl/bluetooth/mesh/node/api.h"
#include "oscl/bluetooth/mesh/network/api.h"
#include "oscl/bluetooth/mesh/node/api.h"
#include "oscl/timer/factory/api.h"
#include "oscl/bluetooth/mesh/element/symbiont/kernel/api.h"
#include "oscl/pdu/memory/api.h"
#include "oscl/bluetooth/mesh/network/friendship/api.h"
#include "oscl/bluetooth/mesh/network/crypto.h"
#include "oscl/bluetooth/mesh/network/cache/config.h"

#include "oscl/queue/queue.h"
#include "offer.h"
#include "oscl/bluetooth/mesh/subscription/register/iteration.h"
#include "oscl/change/observer/composer.h"
#include "record.h"
#include "oscl/inhibitor/api.h"

#include "oscl/bluetooth/mesh/transport/upper/rx/composer.h"
#include "oscl/bluetooth/mesh/transport/lower/reassemble/access/reaper.h"

/** */
namespace Oscl {
/** */
namespace BT {
/** */
namespace Mesh {
/** */
namespace LPN {

/** */
class Part:
	public Oscl::BT::Mesh::Network::Friendship::Api,
	private Oscl::BT::Mesh::Transport::Lower::Reassemble::Access::Reaper::ContextApi
	{
	public:
		/** */
		class ContextApi {
			public:
				/** */
				virtual uint8_t	getNumElements() const noexcept=0;

				/** */
				virtual Oscl::BT::Mesh::Network::Api&	getNetworkApi() noexcept=0;

				/** */
				virtual Oscl::BT::Mesh::Node::Api&	getNodeApi() noexcept=0;

				/** */
				virtual Oscl::BT::Mesh::Element::Symbiont::Kernel::Api&	getNetTxApi() noexcept=0;
			};

	private:
		/** */
		ContextApi&						_context;

		/** */
		Oscl::Timer::Factory::Api&		_timerFactoryApi;

		/** */
		Oscl::Pdu::Memory::Api&			_freeStore;

		/** */
		Oscl::BT::Mesh::Subscription::
		Register::IterationApi&			_subscriptionIterationApi;

		/** */
		Oscl::Inhibitor::Api&			_hciDisableInhibitorApi;

		/** */
		const uint16_t					_lpnAddress;

		/** */
		uint16_t						_previousFriendAddress;

		/** */
		uint16_t						_lpnCounter;

		/**  Friend Sequence Number (FSN)
		 */
		bool							_fsn;

	private:
		/** */
		Oscl::Change::Observer::Composer<Part>	_subscriptionListChanged;

		/** */
		Oscl::Queue<SubscribeRecordMem>		_freeSubscribeRecordMem;

		/** This list contains active subscriptions that have
			been confirmed to have been added to the Friend Node.
		 */
		Oscl::Queue<SubscribeRecord>		_activeSubscribeList;

		/** This list contains active subscriptions that have
			yet to be added to the Friend Node.
		 */
		Oscl::Queue<SubscribeRecord>		_pendingSubscribeList;

		/** This list contains absent subscriptions that have
			yet to be removed in the Friend Node.
		 */
		Oscl::Queue<SubscribeRecord>		_pendingUnsubscribeList;

		/** This list contains subscriptions that are either
			active or absent that are waiting for the
			subscription list confirmation.

			Upon being confirmed, these will be either
			destroyed or moved to the _activeSubcribeList
			based on their active status.
		 */
		Oscl::Queue<SubscribeRecord>		_pendingConfirmList;

	private:
		/** */
		Oscl::Queue<Oscl::BT::Mesh::LPN::Offer>	_offers;

		/** */
		static constexpr unsigned	nOfferMem	= 4;

		/** */
		Oscl::BT::Mesh::LPN::OfferMem			_offerMem[nOfferMem];

		/** */
		Oscl::Queue<Oscl::BT::Mesh::LPN::OfferMem>	_freeOfferMem;

	private:
		/** */
		Oscl::Timer::Api&						_timerApi;

		/** This callback is executed when it is time
			to transmit Friend Request message.
		 */
		Oscl::Done::Operation<Part>				_friendRequestTimerExpired;

		/** This callback is excuted when it is time
			for the LPN to choose an offer (if any).
		 */
		Oscl::Done::Operation<Part>				_offerTimerExpired;

		/** This callback is executed after a Friend Poll
			has been sent and no expected Friend Update has been
			received.
		 */
		Oscl::Done::Operation<Part>				_updateTimerExpired;

		/** This callback is executed when it is time
			to transmit Friend Poll message.

			PollTimeout timer is used to measure time between
			two consecutive requests sent by the Low Power
			node. If no requests are received by the Friend node
			before the PollTimeout timer expires, then the
			friendship is considered terminated.
		 */
		Oscl::Done::Operation<Part>				_pollTimerExpired;

		/** This callback is executed when receiveDelay
			window timer expires. The HCI receiver is
			renabled when this happens.

			The HCI receiver may be disabled while the
			receive delay timer is running.
		 */
		Oscl::Done::Operation<Part>				_receiveDelayTimerExpired;

	private:
		/** */
		Oscl::Timer::Api&						_subscriptionTimerApi;

		/** This callback is executed when the subscription
			timer expires and the subscription add/remove
			needs to be retransmitted.
		 */
		Oscl::Done::Operation<Part>				_subscriptionTimerExpired;

	private:
		/** */
		Oscl::Frame::FWD::ItemComposer<Part>	_networkRxItem;

	private:
		/** */
		Oscl::BT::Mesh::LPN::Offer*				_offer;


	private: // Friendship Security Material
		/** */
		Oscl::BT::Mesh::Network::
		Crypto::Credentials			_credentials;

	private:
		/** */
		Oscl::BT::Mesh::Network::Cache::Config<10>	_cache;

	private:
		/** */
		Oscl::BT::Mesh::Transport::
		Upper::RX::Composer<Part>				_upperTransportRxComposer;

	private:
		/** */
		union AccessReaperMem{
			/** */
			void*	__qitemlink;

			struct {
				/** */
				Oscl::Memory::AlignedBlock<
					sizeof(
						Oscl::BT::Mesh::Transport::
						Lower::Reassemble::Access::Reaper
						)
				>			reaper;
				Oscl::Timer::Api*	ackTimer;
				Oscl::Timer::Api*	incompleteTimer;
				} mem;
			};

		/** */
		constexpr static unsigned	nAccessReaperMem	= 2;

		/** */
		AccessReaperMem	accessReaperMem[nAccessReaperMem];

		/** */
		Oscl::Queue<AccessReaperMem>	_freeAccessReaperMem;

		/** */
		Oscl::Queue<
			Oscl::BT::Mesh::
			Transport::Lower::
			Reassemble::Access::Reaper
			>							_activeAccessReapers;

	private:
		/** */
		unsigned								_pollRetries;

		/** */
		bool									_hcReceiverDisableAllowed;

	private:
		/** */
		uint8_t									_subscriptionTransactionNumber;

		/** */
		bool									_wantsMorePoll;

	private:
		/** */
		bool									_stopping;

	public:
		/** */
		Part(
			ContextApi&						context,
			Oscl::Timer::Factory::Api&		timerFactoryApi,
			Oscl::Pdu::Memory::Api&			freeStore,
			Oscl::BT::Mesh::Subscription::
			Register::IterationApi&			subscriptionIterationApi,
			Oscl::Inhibitor::Api&			hciDisableInhibitorApi,
			SubscribeRecordMem*				subscribeRecordMem,
			unsigned						nSubscribeRecordMem,
			uint16_t						lpnAddress
			) noexcept;

		/** */
		~Part() noexcept;

		/** */
		void	start() noexcept;

		/** */
		void	stop() noexcept;

		/** */
		Oscl::Frame::FWD::Item&	getRxMeshMessageItem() noexcept;

	private: // Oscl::BT::Mesh::Transport::Lower::Reassemble::Access::Reaper::ContextApi
		/** This operation is invoked for each
			unique segment that is received.
			This filtered list may be used by
			Friends to maintain the Friend
			queue for segments destined to the LPN.
		 */
		void	segmentReceived(
					const void*		frame,
					unsigned		length,
					uint32_t		ivIndex,
					uint32_t		seq,
					uint16_t		src,
					uint16_t		dst,
					bool			ctl,
					uint8_t			ttl
					) noexcept;

		/**  */
		void	stopped(Oscl::BT::Mesh::Transport::Lower::Reassemble::Access::Reaper& part) noexcept;

	private:
		/** */
		void	startFriendRequestTimer() noexcept;

		/** */
		void	startOfferTimer() noexcept;

		/** */
		void	startUpdateTimer() noexcept;

		/** */
		void	startPollTimer() noexcept;

		/** */
		void	startReceiveDelayTimer() noexcept;

		/** */
		void	startSubscriptionTimer() noexcept;

	private:
		/** This callback is executed when it is time
			to transmit Friend Request message.
		 */
		void	friendRequestTimerExpired() noexcept;

		/** This callback is excuted when it is time
			for the LPN to choose an offer (if any).
		 */
		void	offerTimerExpired() noexcept;

		/** This callback is executed after a Friend Poll
			has been sent and no expected Friend Update has been
			received.
		 */
		void	updateTimerExpired() noexcept;

		/** This callback is executed when it is time
			to transmit Friend Poll message.

			PollTimeout timer is used to measure time between
			two consecutive requests sent by the Low Power
			node. If no requests are received by the Friend node
			before the PollTimeout timer expires, then the
			friendship is considered terminated.
		 */
		void	pollTimerExpired() noexcept;

		/** This callback is executed when receiveDelay
			window timer expires. The HCI receiver is
			renabled when this happens.

			The HCI receiver may be disabled while the
			receive delay timer is running.
		 */
		void	receiveDelayTimerExpired() noexcept;

		/** This callback is executed when it is time
			to retransmit a subscription add/remove.
		 */
		void	subscriptionTimerExpired() noexcept;

		/** */
		void	sendFriendRequest() noexcept;

		/** */
		Oscl::BT::Mesh::LPN::Offer*	chooseOffer() noexcept;

		/** */
		void	sendFriendPoll() noexcept;

		/** */
		void	allowHcReceiverToBeDisabled() noexcept;

		/** */
		void	preventHcReceiverFromBeingDisabled() noexcept;

		/** */
		void	freeRemainingOffers() noexcept;

		/** */
		void	generateFriendshipSecurityMaterial(
					const void*	netKey,
					uint16_t	lpnAddress,
					uint16_t	friendAddress,
					uint16_t	lpnCounter,
					uint16_t	friendCounter
					) noexcept;

		/** The transportPDU has the destination
			address pre-pended.

			param [in] ctl		Determines the MIC length
			param [in] ttl		Time-To-Live value
			param [in] src		Source address
			param [in] seq		24-bit Sequence number
			param [in] ivIndex	IV Index
			param [in] pdu		Pointer to the lower transport
								PDU. The destination address is
								encoded in the first two octets.
			param [in] pduLen	Length of the PDU
		 */
		Oscl::Pdu::Pdu*	buildNetworkPDU(
							bool		ctl,
							uint8_t		ttl,
							uint16_t	src,
							uint32_t	seq,
							uint32_t	ivIndex,
							const void*	pdu,
							unsigned	pduLen
							) noexcept;

		/** */
		bool	processLocalNetworkPDU(
					const void*		frame,
					unsigned		length,
					uint32_t		ivIndex,
					uint32_t		seq,
					uint16_t		src,
					uint16_t		dst,
					bool			ctl,
					uint8_t			ttl
					) noexcept;

		/** */
		void	processSegmentedLowerTransportControlMessage(
					uint16_t	srcAddress,
					uint16_t	dstAddress,
					uint32_t	seq,
					uint32_t	ivIndex,
					uint8_t		ttl,
					const void*	lowerTransportPDU,
					unsigned	length,
					uint8_t		opcode
					) noexcept;

		/** */
		void	processUnsegmentedLowerTransportControlMessage(
					uint16_t	srcAddress,
					uint16_t	dstAddress,
					uint32_t	seq,
					uint32_t	ivIndex,
					uint8_t		ttl,
					const void*	lowerTransportPDU,
					unsigned	length,
					uint8_t		opcode
					) noexcept;

		/** */
		void	processLowerTransportControlMessage(
					uint16_t	srcAddress,
					uint16_t	dstAddress,
					uint32_t	seq,
					uint32_t	ivIndex,
					uint8_t		ttl,
					const void*	lowerTransportPDU,
					unsigned	length
					) noexcept;

		/** */
		void	processLowerTransportMessage(
					uint16_t	srcAddress,
					uint16_t	dstAddress,
					uint32_t	seq,
					uint32_t	ivIndex,
					uint8_t		ttl,
					const void*	lowerTransportPDU,
					unsigned	length
					) noexcept;

	private: // Oscl::BT::Mesh::Network::Friendship::Api
		/** Process a received upper transport
			layer PDU.

			This operation is used by the Network
			layer to forward messages that are
			received by the Network layer encrypted
			using the master key material.

			From a Friendship perspective, these are
			should only Friend Request and Offer
			messages.

			All other Friendship related messages are
			encrypted using the Friendship credentials
			and are received through a separate channel.
		 */
		void	rxControl(
					const void*	upperTransportAccessPdu,
					unsigned	pduLength,
					uint32_t	ivIndex,
					uint32_t	seq,
					uint16_t	src,
					uint16_t	dst,
					uint8_t		ttl,
					uint8_t		opcode
					) noexcept;

		/** RETURN: true if the PDU was destined
			for a LPN.
		 */
		bool	rxLpnPDU(
					const void*		frame,
					unsigned		length,
					uint32_t		ivIndex,
					uint32_t		seq,
					uint16_t		src,
					uint16_t		dst,
					bool			ctl,
					uint8_t			ttl
					) noexcept;

		/** Process a received mesh network message
			that may belong to the implementation.

			RETURN: true if the frame belongs to a
			the friendship implementation.
		 */
		bool	rxNetwork(
					const void*	frame,
					unsigned 	length
					) noexcept;

		/** */
		void	rxBeacon(
					uint32_t	ivIndex,
					bool		keyRefreshFlag,
					bool		ivUpdateFlag
					) noexcept;

		/** */
		bool	wantsDestination(uint16_t address) const noexcept;

	private:
		/** */
		void	sendFriendSubscriptionListAdd() noexcept;

		/** */
		void	sendFriendSubscriptionListRemove() noexcept;

		/** */
		void	retransmitFriendSubscriptionList() noexcept;

		/** */
		void	sendNextSubscriptionTransaction() noexcept;

	private:
		/** */
		void	processFriendOffer(
					const void*	upperTransportAccessPdu,
					unsigned	pduLength,
					uint32_t	ivIndex,
					uint32_t	seq,
					uint16_t	src,
					uint16_t	dst
					) noexcept;

		/** */
		void	processFriendUpdate(
					const void*	upperTransportAccessPdu,
					unsigned	pduLength,
					uint32_t	ivIndex,
					uint32_t	seq,
					uint16_t	src,
					uint16_t	dst
					) noexcept;

		/** */
		void	processFriendSubscriptionListConfirm(
					const void*	upperTransportAccessPdu,
					unsigned	pduLength,
					uint32_t	ivIndex,
					uint32_t	seq,
					uint16_t	src,
					uint16_t	dst
					) noexcept;

		/** */
		void	forwardUpperTransportPduToAllElements(
					const void*	upperTransportAccessPdu,
					unsigned	pduLength,
					uint32_t	ivIndex,
					uint32_t	seq,
					uint16_t	src,
					uint16_t	dst,
					uint8_t		aid,
					bool		akf,
					bool		segmented
					) noexcept;

	private:
		/** */
		void	subscriptionListChanged() noexcept;

		SubscribeRecord*	findActiveSubscription(uint16_t address) noexcept;

		/** */
		SubscribeRecord*	findPendingActiveSubscription(uint16_t address) noexcept;

		/** */
		SubscribeRecord*	findPendingUnsubscribe(uint16_t address) noexcept;

		/** */
		SubscribeRecord*	findActive(uint16_t address) noexcept;

		/** */
		void	createNewSubscriptions() noexcept;

		/** */
		void	markAllSubscriptionsAbsent() noexcept;

		/** */
		void	markPresent() noexcept;

		/** */
		void	free(SubscribeRecord* r) noexcept;

		/** */
		void	moveAbsentToUnsubcribe() noexcept;

		/** */
		void	movePresentToActive() noexcept;
	
		/** */
		void	resetSubscriptions() noexcept;

	};


}
}
}
}

#endif
