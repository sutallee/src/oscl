/*
   Copyright (C) 2020 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_bluetooth_mesh_lpn_offerh_
#define _oscl_bluetooth_mesh_lpn_offerh_

#include <stdint.h>
#include "oscl/queue/queueitem.h"
#include "oscl/memory/block.h"

/** */
namespace Oscl {
/** */
namespace BT {
/** */
namespace Mesh {
/** */
namespace LPN {

/** */
class Offer : public Oscl::QueueItem {
	public:
		/** */
		const uint16_t	_friendAddress;
		/** */
		const uint8_t	_receiveWindowInMs;
		/** */
		const uint8_t	_queueSizeInMessages;
		/** */
		const uint8_t	_subscriptionListSizeInEntries;
		/** */
		const uint8_t	_rssiInDBm;
		/** */
		const uint16_t	_friendCounter;
		/** */
		const uint16_t	_lpnCounter;

	public:
		/** */
		Offer(
			uint16_t	friendAddress,
			uint8_t		receiveWindowInMs,
			uint8_t		queueSizeInMessages,
			uint8_t		subscriptionListSizeInEntries,
			uint8_t		rssiInDBm,
			uint16_t	friendCounter,
			uint16_t	lpnCounter
			) noexcept;
	};

/** */
union OfferMem {
	/** */
	void*		__qitemlink;

	/** */
	Oscl::Memory::AlignedBlock<sizeof(Offer)>	offerMem;
	};

}
}
}
}

#endif
