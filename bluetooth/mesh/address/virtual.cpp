/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "virtual.h"
#include "oscl/bluetooth/crypto/salt.h"
#include "oscl/aes128/cmac.h"

uint16_t	Oscl::BT::Mesh::Address::makeVirtualAddress(const uint8_t labelUUID[16]) noexcept{
	/*
		Virtual Address: 0b10xx xxxx xxxx xxxx

		SALT = s1 (“vtad”)
		hash = AES-CMACsalt (Label UUID) mod 2^14

		When an Access message is received to a virtual address
		that has a matching hash, each corresponding Label UUID
		is used by the upper transport layer as additional
		data as part of the authentication of the message
		until a match is found.  Control messages cannot use
		virtual addresses.

		Label UUIDs may be generated randomly as defined in [8].
		A Configuration Client may assign and track virtual addresses,
		however two devices can also create a virtual address using
		some out-of-band (OOB) mechanism. Unlike group addresses,
		these could be agreed upon by the devices involved and would
		not need to be registered in the centralized provisioning
		database, as they are unlikely to be duplicated.

		A disadvantage of virtual addresses is that a multi-segment
		message is required to transfer a Label UUID to a publishing
		or subscribing node during configuration.
	 */

	static const uint8_t	vtad[] = {'v','t','a','d'};

	uint8_t	salt[16];

	Oscl::BT::Crypto::Salt::s1(
		vtad,
		sizeof(vtad),
		salt,
		sizeof(salt)
		);

	Oscl::AES128::CMAC	cmac(
		salt
		);

	cmac.accumulate(
		labelUUID,
		16
		);

	cmac.finalize();

	uint8_t	hash[2];

	cmac.copyOut(
		hash,
		sizeof(hash)
		);

	uint16_t	virtualAddress;

	// Assume big-endian
	virtualAddress	= hash[0];
	virtualAddress	<<= 8;
	virtualAddress	|= hash[1];

	virtualAddress	&= 0x3FFF;
	virtualAddress	|= 0x8000;

	return virtualAddress;
	}

