/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_bluetooth_mesh_address_filter_recordh_
#define _oscl_bluetooth_mesh_address_filter_recordh_

#include <stdint.h>
#include "oscl/queue/queueitem.h"

/** */
namespace Oscl {
/** */
namespace BT {
/** */
namespace Mesh {
/** */
namespace Address {
/** */
namespace Filter {

/** */
class Record : public Oscl::QueueItem {
	private:
		/** */
		uint16_t	_address;

	public:
		/** */
		Record(
			uint16_t address
			) noexcept:
				_address(address)
					{
				}

		/** */
		bool	match(uint16_t address) noexcept{
					return (address == _address);
					}
	};

/** */
union RecordMem {
	/** */
	Oscl::Memory::AlignedBlock<sizeof(Record)>	mem;

	/** */
	void*	__qitemlink;
	};

}
}
}
}
}
#endif
