/*
   Copyright (C) 2020 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_bluetooth_mesh_network_cache_parth_
#define _oscl_bluetooth_mesh_network_cache_parth_

#include <stdint.h>
#include "oscl/queue/queue.h"
#include "oscl/memory/block.h"

/** */
namespace Oscl {
/** */
namespace BT {
/** */
namespace Mesh {
/** */
namespace Network {
/** */
namespace Cache {

/** */
class Entry: public Oscl::QueueItem {
	private:
		/** */
		const uint16_t	_src;
		/** */
		const uint32_t	_seq;
		/** */
		const uint32_t	_ivIndex;
		/** */
		unsigned		_matchCount;

	public:
		/** */
		Entry(
			uint16_t	src,
			uint32_t	seq,
			uint32_t	ivIndex
			) noexcept;

		/** RETURN: true if the pdu
			this cache entry.
		 */
		bool	match(
					uint16_t	src,
					uint32_t	seq,
					uint32_t	ivIndex
					) noexcept;
	};

/** */
union EntryMem {
	/** */
	void*	__qitemlink;

	/** */
	Oscl::Memory::AlignedBlock<sizeof(Entry)>	mem;
	};

/** */
class Part {
	private:
		/** */
		Oscl::Queue<EntryMem>	_freeCacheMem;

		/** */
		Oscl::Queue<Entry>		_cacheEntryList;

	public:
		/** */
		Part(
			EntryMem*	cacheMem,
			unsigned	nCacheMem
			) noexcept;

		/** */
		bool	messageInCache(
					uint16_t	src,
					uint32_t	seq,
					uint32_t	ivIndex
					) noexcept;

		/** */
		void	addToCache(
					uint16_t	src,
					uint32_t	seq,
					uint32_t	ivIndex
					) noexcept;
	};

}
}
}
}
}

#endif
