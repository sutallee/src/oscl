/*
   Copyright (C) 2020 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <new>
#include "part.h"
#include "oscl/error/info.h"
#include "oscl/error/fatal.h"

using namespace Oscl::BT::Mesh::Network::Cache;

Entry::Entry(
	uint16_t	src,
	uint32_t	seq,
	uint32_t	ivIndex
	) noexcept:
		_src(src),
		_seq(seq),
		_ivIndex(ivIndex),
		_matchCount(0)
		{
	}

bool	Entry::match(
			uint16_t	src,
			uint32_t	seq,
			uint32_t	ivIndex
			) noexcept{

	if(src != _src){
		return false;
		}

	if(seq != _seq){
		return false;
		}

	if(ivIndex != _ivIndex){
		return false;
		}

	++_matchCount;

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"Cache hit: src(%4.4X),seq(%8.8X),ivIndex(%8.8X),match count: %u\n",
		src,
		seq,
		ivIndex,
		_matchCount
		);
	#endif

	return true;
	}

Part::Part(
	EntryMem*	cacheMem,
	unsigned	nCacheMem
	) noexcept{
	for(unsigned i=0;i<nCacheMem;++i){
		_freeCacheMem.put(&cacheMem[i]);
		}
	}

bool	Part::messageInCache(
			uint16_t	src,
			uint32_t	seq,
			uint32_t	ivIndex
			) noexcept{

	Entry*	entry;

	for(
		entry	= _cacheEntryList.first();
		entry;
		entry	= _cacheEntryList.next(entry)
		){
		bool
		match	= entry->match(
					src,
					seq,
					ivIndex
					);

		if(match){
			return true;
			}
		}

	return false;
	}

void	Part::addToCache(
			uint16_t	src,
			uint32_t	seq,
			uint32_t	ivIndex
			) noexcept{

	EntryMem*
	mem	= _freeCacheMem.get();

	if(!mem){
		#ifdef DEBUG_TRACE
		Oscl::Error::Info::log(
			"%s: cache wrap [src:0x%4.4X,seq:0x%6.6X,ivIndex:0x%8.8X].\n",
			__PRETTY_FUNCTION__,
			src,
			seq,
			ivIndex
			);
		#endif
		mem	= (EntryMem*)_cacheEntryList.get();
		}

	if(!mem){
		// This should never happen
		Oscl::ErrorFatal::logAndExit(
			"%s: cache system is screwed!\n",
			OSCL_PRETTY_FUNCTION
			);
		}

	Entry*
	entry	= new(mem) Entry(
				src,
				seq,
				ivIndex
				);

	_cacheEntryList.put(entry);
	}

