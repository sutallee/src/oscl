/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_bluetooth_mesh_network_configh_
#define _oscl_bluetooth_mesh_network_configh_

#include "part.h"

/** */
namespace Oscl {
/** */
namespace BT {
/** */
namespace Mesh {
/** */
namespace Network {

/** */
template <
	unsigned	bufferSize,
	unsigned	nBuffers,
	unsigned	nFixed,
	unsigned	nFragment,
	unsigned	nComposite,
	unsigned	nAccessReaperMem,
	unsigned	nControlReaperMem,
	unsigned	nAppKeyRecordMem,
	unsigned	nCacheEntries,
	unsigned	nNetMessages
	>
class Config : public Part {
	private:
		/** */
		Oscl::Pdu::Memory::
		Config<
			bufferSize,
			nBuffers,
			nFixed,
			nFragment,
			nComposite
			>						_freeStore;

	private:
		/** */
		Part::AccessReaperMem	_accessReaperMem[nAccessReaperMem];

	private:
		/** */
		Part::ControlReaperMem	_controlReaperMem[nControlReaperMem];

	private:
		/** */
		Oscl::BT::Mesh::Key::RecordMem	_appKeyRecordMem[nAppKeyRecordMem];

	private:
		Cache::EntryMem					_cacheEntryMem[nCacheEntries];

	private:
		/** */
		NetMessageMem				_relayMessageMem[nNetMessages];

	public:
		/** */
		Config(
			Oscl::BT::Mesh::
			Network::Part::ContextApi&			context,
			Oscl::BT::Mesh::Node::Api&			nodeApi,
			Oscl::BT::Mesh::Bearer::TX::Api&	bearerApi,
			Oscl::Pdu::FWD::Api&				beaconBearerApi,
			Oscl::Timer::Factory::Api&			timerFactoryApi,
			Oscl::BT::Mesh::
		    Network::Persist::Api&				networkPersistApi,
			Oscl::BT::Mesh::Key::
			Persist::Creator::Api&				keyPersistApi,
			Oscl::BT::Mesh::
			Subscription::Register::RecordMem*	subscriptionRecordMem,
			unsigned							nSubscriptionRecords,
			const uint8_t						netKey[16],
			const uint8_t						newKey[16],
			uint16_t							keyIndex,
			uint16_t							unicastAddress,
			uint8_t								nElements,
			uint8_t								defaultTTL,
			uint8_t								nodeIdentityState,
			uint8_t								friendState,
			uint8_t								networkKeyPhase,
			uint16_t							heartbeatSubscriptionSource,
			uint16_t							heartbeatSubscriptionDestination,
			uint8_t								heartbeatSubscriptionPeriodLog,
			uint8_t								networkTransmitCount,
			uint8_t								networkTransmitIntervalSteps,
			bool								relayEnabled,
			uint8_t								relayRetransmitCount,
			uint8_t								relayRetransmitIntervalSteps,
			uint16_t							heartbeatPublishDestination,
			uint16_t							heartbeatPublishCount,
			uint8_t								heartbeatPublishPeriodLog,
			uint8_t								heartbeatPublishTtl,
			uint16_t							heartbeatPublishFeatures
			) noexcept:
				Part(
					context,
					nodeApi,
					bearerApi,
					beaconBearerApi,
					timerFactoryApi,
		   		 	networkPersistApi,
					keyPersistApi,
					subscriptionRecordMem,
					nSubscriptionRecords,
					netKey,
					newKey,
					keyIndex,
					unicastAddress,
					nElements,
					defaultTTL,
					nodeIdentityState,
					friendState,
					networkKeyPhase,
					heartbeatSubscriptionSource,
					heartbeatSubscriptionDestination,
					heartbeatSubscriptionPeriodLog,
					networkTransmitCount,
					networkTransmitIntervalSteps,
					relayEnabled,
					relayRetransmitCount,
					relayRetransmitIntervalSteps,
					heartbeatPublishDestination,
					heartbeatPublishCount,
					heartbeatPublishPeriodLog,
					heartbeatPublishTtl,
					heartbeatPublishFeatures,
					_freeStore,
					_accessReaperMem,
					nAccessReaperMem,
					_controlReaperMem,
					nControlReaperMem,
					_appKeyRecordMem,
					nAppKeyRecordMem,
					_cacheEntryMem,
					nCacheEntries,
					_relayMessageMem,
					nNetMessages
					),
				_freeStore(
					"Network",
					&_allFreeNotification
					)
				{
				}

		/** */
		~Config() noexcept{}

	};

}
}
}
}

#endif
