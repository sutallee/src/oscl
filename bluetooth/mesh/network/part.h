/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_bluetooth_mesh_network_parth_
#define _oscl_bluetooth_mesh_network_parth_

#include <stdint.h>
#include <string.h>
#include "crypto.h"
#include "netkey.h"
#include "oscl/memory/block.h"
#include "oscl/done/operation.h"
#include "oscl/frame/fwd/composer.h"
#include "oscl/frame/fwd/host/composer.h"
#include "oscl/frame/fwd/itemcomp.h"

#include "oscl/frame/rf/rx/composer.h"
#include "oscl/frame/rf/rx/host/composer.h"
#include "oscl/frame/rf/rx/itemcomp.h"

#include "oscl/pdu/fwd/api.h"
#include "oscl/queue/queue.h"
#include "oscl/queue/queueitem.h"
#include "oscl/stream/hdlc/fcs/fcs8.h"
#include "oscl/bluetooth/mesh/prov/link/reaper.h"
#include "oscl/bluetooth/mesh/prov/device/reaper.h"
#include "oscl/endian/decoder/api.h"
#include "oscl/pdu/memory/config.h"
#include "oscl/bluetooth/mesh/element/symbiont/api.h"
#include "oscl/bluetooth/mesh/element/symbiont/host/api.h"
#include "oscl/bluetooth/mesh/element/symbiont/kernel/api.h"
#include "oscl/bluetooth/mesh/node/api.h"
#include "oscl/bluetooth/mesh/bearer/tx/api.h"
#include "api.h"
#include "oscl/bluetooth/mesh/bearer/tx/itemcomp.h"
#include "oscl/bluetooth/mesh/bearer/tx/host/api.h"
#include "oscl/timer/factory/api.h"
#include "oscl/bluetooth/mesh/transport/lower/reassemble/access/reaper.h"
#include "oscl/bluetooth/mesh/transport/lower/reassemble/control/reaper.h"
#include "oscl/bluetooth/mesh/transport/upper/rx/composer.h"
#include "oscl/bluetooth/mesh/transport/upper/control/rx/composer.h"
#include "oscl/bluetooth/mesh/key/api.h"
#include "oscl/bluetooth/mesh/key/record.h"
#include "oscl/bluetooth/mesh/key/persist/creator/api.h"
#include "oscl/bluetooth/mesh/network/persist/api.h"
#include "oscl/print/api.h"
#include "oscl/bluetooth/mesh/beacon/secure/network/api.h"
#include "oscl/bluetooth/mesh/network/friendship/api.h"
#include "oscl/bluetooth/mesh/network/cache/part.h"
#include "oscl/bluetooth/mesh/subscription/register/part.h"
#include "oscl/bluetooth/mesh/network/keyrefresh/fsm.h"
#include "oscl/bluetooth/mesh/transport/upper/rx/part.h"

/** */
namespace Oscl {
/** */
namespace BT {
/** */
namespace Mesh {
/** */
namespace Network {

/** */
class NetMessage: public Oscl::QueueItem {
	public:
		/** */
		Oscl::Handle<Oscl::Pdu::Pdu>	_pdu;

		/** */
		const uint16_t	_dst;

		/** */
		uint8_t			_txCount;

	public:
		/** */
		NetMessage(
			Oscl::Pdu::Pdu*	pdu,
			uint16_t		dst,
			uint8_t			txCount
			) noexcept;
	};

/** */
class Part :
	public Oscl::QueueItem,
	private Oscl::BT::Mesh::Element::Symbiont::Host::Api,
	public Oscl::BT::Mesh::Element::Symbiont::Kernel::Api,
	private Oscl::BT::Mesh::Transport::Lower::Reassemble::Access::Reaper::ContextApi,
	private Oscl::BT::Mesh::Transport::Lower::Reassemble::Control::Reaper::ContextApi,
	public Oscl::BT::Mesh::Key::Api,
	public Oscl::BT::Mesh::Network::Api,
	public Oscl::BT::Mesh::Beacon::Secure::Network::ContextApi,
	private Oscl::BT::Mesh::Network::KeyRefresh::StateVar::ContextApi,
	private Oscl::BT::Mesh::Transport::Upper::RX::Context::Api
	{
	public:
		/** */
		class ContextApi {
			public:
				/** */
				virtual void	stopped(Part& network) noexcept=0;

				/** This interface allows an implementation to
					receive all heartbeat messages for analysis.
					This is an extension of the standard heartbeat
					subscription behavior.
					The intialTTL and featuresMask include the
					RFU fields exactly as received. It is the
					responsibility of the implementation to
					mask-off and use the RFU fields appropriately.
				 */
				virtual void	recordHeartbeat(
									uint16_t	netKeyIndex,
									uint16_t	srcAddress,
									uint16_t	dstAddress,
									uint8_t		ttl,
									uint8_t		initialTTL,
									uint16_t	featuresMask,
									signed		rssi
									) noexcept {};

				/** */
				virtual void	enableFriendFeature() noexcept{};

				/** */
				virtual void	disableFriendFeature() noexcept{};
			};

	private:
		/** */
		ContextApi&							_context;

		/** */
		Oscl::BT::Mesh::Node::Api&			_nodeApi;

		/** */
		Oscl::BT::Mesh::Bearer::TX::Api&	_bearerApi;

		/** */
		Oscl::Pdu::FWD::Api&				_beaconBearerApi;

		/** */
		Oscl::Timer::Factory::Api&	_timerFactoryApi;

		/** */
		Oscl::BT::Mesh::
		Network::Persist::Api&		_networkPersistApi;

		/** */
		Oscl::BT::Mesh::Key::
		Persist::Creator::Api&		_keyPersistApi;

		/** */
		const uint16_t				_keyIndex;

		/** */
		const uint16_t				_unicastAddress;

		/** */
		const uint8_t				_nElements;

	private:
		/** */
		Oscl::BT::Mesh::Network::
		KeyRefresh::StateVar		_keyRefreshState;

	private:
		/** */
		Oscl::BT::Mesh::Transport::
		Upper::RX::Part				_transportUpperRX;

	private:
		/** Enough memory for active/old and new NetKey.
		 */
		static constexpr unsigned	nNetKeyMem	= 2;

		/** */
		NetKeyMem				_netKeyMem[nNetKeyMem];

		/** */
		Oscl::Queue<NetKeyMem>	_freeNetKeyMem;

		/** */
		Oscl::Queue<NetKey>		_netKeys;

		/** */
		NetKey*					_activeNetKey;

		/** */
		NetKey*					_newNetKey;

	private:
		/** This is only valid during
			the updateKey() method execution.
		 */
		const uint8_t*			_newNetKeyCandidate;

		/** */
		bool					_configNetKeyUpdateFailed;

	private:
		/** This is updated when a Secure Network Beacon
			is successfully received/decrypted.
		 */
		NetKey*					_lastRxBeaconNetKey;

		/** */
		bool					_lastRxBeaconKeyRefreshFlag;

	private:
		/**	This is a pointer to an optional frienship
			object. The object may be either a LPN or
			a Friend implementation (not both, duh).
		 */
		Oscl::BT::Mesh::
	    Network::Friendship::Api*			_friendshipApi;

		Oscl::BT::Mesh::
		Subscription::Register::Part		_subscriptionRegister;

	private:
		/** */
		Oscl::Frame::RF::RX::ItemComposer<Part>	_rxMeshMessageItem;

	private:
		/** */
		Oscl::Frame::FWD::ItemComposer<Part>	_rxSecureNetworkBeaconItem;

	private:
		/** */
		Oscl::Done::Operation<Part>			_relayTxTimerExpired;

		/** */
		Oscl::Timer::Api&					_relayTxTimerApi;

	private:
		/** */
		Oscl::Done::Operation<Part>			_netTxTimerExpired;

		/** */
		Oscl::Timer::Api&					_netTxTimerApi;

		/** */
		Oscl::Done::Operation<Part>			_netReTxTimerExpired;

		/** */
		Oscl::Timer::Api&					_netReTxTimerApi;

	private:
		/** */
		Oscl::Done::Operation<Part>			_heartbeatTxTimerExpired;

		/** */
		Oscl::Timer::Api&					_heartbeatTxTimerApi;

	private:
		/** */
		Oscl::Done::Operation<Part>			_heartbeatSubTimerExpired;

		/** */
		Oscl::Timer::Api&					_heartbeatSubTimerApi;

	protected:
		/** */
		Oscl::Done::Operation<Part>			_allFreeNotification;

	private:
		/** */
		Oscl::Pdu::Memory::Part&			_freeStore;

	protected:
		// Oscl::BT::Mesh::Transport::Lower::Reassemble::Access::Reaper
		/** */
		union AccessReaperMem{
			/** */
			void*	__qitemlink;

			struct {
				/** */
				Oscl::Memory::AlignedBlock<
					sizeof(
						Oscl::BT::Mesh::Transport::
						Lower::Reassemble::Access::Reaper
						)
				>			reaper;
				Oscl::Timer::Api*	ackTimer;
				Oscl::Timer::Api*	incompleteTimer;
				} mem;
			};

	private:
		/** */
		Oscl::Queue<AccessReaperMem>	_freeAccessReaperMem;

		/** */
		Oscl::Queue<
			Oscl::BT::Mesh::
			Transport::Lower::
			Reassemble::Access::Reaper
			>							_activeAccessReapers;

	protected:
		// Oscl::BT::Mesh::Transport::Lower::Reassemble::Control::Reaper
		/** */
		union ControlReaperMem{
			/** */
			void*	__qitemlink;

			struct {
				/** */
				Oscl::Memory::AlignedBlock<
					sizeof(
						Oscl::BT::Mesh::Transport::
						Lower::Reassemble::Control::Reaper
						)
				>			reaper;
				Oscl::Timer::Api*	ackTimer;
				Oscl::Timer::Api*	incompleteTimer;
				} mem;
			};

	private:
		/** */
		Oscl::Queue<ControlReaperMem>	_freeControlReaperMem;

		/** */
		Oscl::Queue<
			Oscl::BT::Mesh::
			Transport::Lower::
			Reassemble::Control::Reaper
			>							_activeControlReapers;

	private:
		/** */
		Oscl::Queue<Oscl::BT::Mesh::Key::RecordMem>	_freeAppKeyRecordMem;

		/** */
		Oscl::Queue<
			Oscl::BT::Mesh::
			Key::Record
			>							_appKeys;

	private:
		/** */
		Oscl::BT::Mesh::Transport::
		Upper::Control::RX::Composer<Part>	_upperTransportControlRxComposer;

	private:
		/** */
		Oscl::Queue<NetMessage>	_relayQ;

	private:
		/** */
		Oscl::Queue<NetMessage>	_netQ;

		/** */
		Oscl::Queue<NetMessage>	_netRetransmitQ;

	private:
		/** */
		Oscl::HQueue<Oscl::Pdu::Pdu>	_loopQ;

		/** */
		bool							_loopLocked;

	private:
		/** */
		Oscl::BT::Mesh::Network::
		Cache::Part						_cache;

	protected:
		/** */
		union NetMessageMem {
			void*	__qitemlink;
			Oscl::Memory::AlignedBlock<sizeof(NetMessage)>	mem;
			};

	private:
		/** */
		Oscl::Queue<NetMessageMem>	_freeNetMessageMem;

		/** */
		unsigned					_nDynamicNetMessageMem;

		/** */
		unsigned					_nOutstandingNetMessageMem;

	private:
		/** */
		Oscl::Queue<Oscl::BT::Mesh::Element::Symbiont::Api>	_elementList;

	private:
		/** */
		uint16_t							_findKeyIndex;

		/** */
		Oscl::BT::Mesh::Key::Persist::Api*	_findKeyPersistApi;

	private:
		/** */
		uint8_t		_defaultTTL;

	private:
		/** */
		bool		_relayEnabled;

		/** */
		uint8_t		_relayRetransmitCount;

		/** */
		uint8_t		_relayRetransmitIntervalSteps;

		/** */
		uint8_t		_nodeIdentityState;

		/** */
		uint8_t		_friendState;

		/** */
		uint8_t		_networkKeyPhase;

	private:
		/** This interface is optional and must
			be set before the start() operation
			is invoked.
		 */
		Oscl::BT::Mesh::Beacon::Secure::Network::Api* _secureNetworkBeaconApi;

	private: // Config Heartbeat Publish State
		/** */
		uint16_t	_configHeartbeatPublishDestination;

		/** */
		uint16_t	_configHeartbeatPublishCount;

		/** */
		uint8_t		_configHeartbeatPublishPeriodLog;

		/** */
		uint8_t		_configHeartbeatPublishTtl;

		/** */
		uint16_t	_configHeartbeatPublishFeatures;

	private: // Config Heartbeat Subscription State
		/** */
		uint16_t	_configHeartbeatSubscriptionSource;

		/** */
		uint16_t	_configHeartbeatSubscriptionDestination;

		/** */
		uint16_t	_configHeartbeatSubscriptionPeriod;

		/** */
		uint16_t	_configHeartbeatSubscriptionCount;

		/** */
		uint8_t		_configHeartbeatSubscriptionMinHops;

		/** */
		uint8_t		_configHeartbeatSubscriptionMaxHops;

	private:
		/** */
		uint8_t		_networkTransmitCount;

		/** */
		uint8_t		_networkTransmitIntervalSteps;

	private:
		/** */
		unsigned	_netTxCount;

	private:
		/** The RSSI of the current frame.
		 */
		signed		_rssi;

	private:
		/** */
		bool		_stopping;

	public:
		/** */
		Part(
			ContextApi&							context,
			Oscl::BT::Mesh::Node::Api&			nodeApi,
			Oscl::BT::Mesh::Bearer::TX::Api&	bearerApi,
			Oscl::Pdu::FWD::Api&				beaconBearerApi,
			Oscl::Timer::Factory::Api&			timerFactoryApi,
			Oscl::BT::Mesh::
		    Network::Persist::Api&				networkPersistApi,
			Oscl::BT::Mesh::Key::
			Persist::Creator::Api&				keyPersistApi,
			Oscl::BT::Mesh::
			Subscription::Register::RecordMem*	subscriptionRecordMem,
			unsigned							nSubscriptionRecords,
			const uint8_t						netKey[16],
			const uint8_t						newKey[16],
			uint16_t							keyIndex,
			uint16_t							unicastAddress,
			uint8_t								nElements,
			uint8_t								defaultTTL,
			uint8_t								nodeIdentityState,
			uint8_t								friendState,
			uint8_t								networkKeyPhase,
			uint16_t							heartbeatSubscriptionSource,
			uint16_t							heartbeatSubscriptionDestination,
			uint8_t								heartbeatSubscriptionPeriodLog,
			uint8_t								networkTransmitCount,
			uint8_t								networkTransmitIntervalSteps,
			bool								relayEnabled,
			uint8_t								relayRetransmitCount,
			uint8_t								relayRetransmitIntervalSteps,
			uint16_t							heartbeatPublishDestination,
			uint16_t							heartbeatPublishCount,
			uint8_t								heartbeatPublishPeriodLog,
			uint8_t								heartbeatPublishTtl,
			uint16_t							heartbeatPublishFeatures,
			Oscl::Pdu::Memory::Part&			freeStore,
			AccessReaperMem*					accessReaperMem,
			unsigned							nAccessReaperMem,
			ControlReaperMem*					controlReaperMem,
			unsigned							nControlReaperMem,
			Oscl::BT::Mesh::Key::RecordMem*		appKeyRecordMem,
			unsigned							nAppKeyRecordMem,
			Cache::EntryMem*					cacheEntryMem,
			unsigned							nCacheEntries,
			NetMessageMem*						relayMessageMem,
			unsigned							nNetMessages
			) noexcept;

		/** */
		~Part() noexcept;

		/** */
		Oscl::Frame::RF::RX::Item&	getRxMeshMessageItem() noexcept;

		/** */
		Oscl::Frame::FWD::Item&	getRxSecureNetworkBeaconItem() noexcept;

		/** */
		Oscl::Pdu::Memory::Api&	getFreeStoreApi() noexcept;

		/** */
		void	start() noexcept;

		/** */
		void	stop() noexcept;

		/** This operation is used to attach a Secure Network Beacon
			object if the implementation supports it.
			This operation must be used *before* the stop() operation
			is invoked.
		 */
		void	attach(Oscl::BT::Mesh::Beacon::Secure::Network::Api& secureNetworkBeaconApi) noexcept;

		/** */
		void	dumpStatus(Oscl::Print::Api& printApi) noexcept;

		/** */
		bool	nidMatch(uint8_t nid) const noexcept;

		/** */
		void	setDefaultTTL(uint8_t defaultTTL) noexcept;

		/** */
		void	setRelay(
					uint8_t relay,
					uint8_t retransmitCount,
					uint8_t retransmitIntervalSteps
					) noexcept;

		/** */
		void	getConfigHeartbeatPublicationStatus(
					uint16_t&	destination,
					uint8_t&	countLog,
					uint8_t&	periodLog,
					uint8_t&	ttl,
					uint16_t&	features
					) noexcept;

		/** RETURN: true on failure (invalid netKeyIndex) */
		bool	setConfigHeartbeatPublication(
					uint16_t	destination,
					uint8_t		countLog,
					uint8_t		periodLog,
					uint8_t		ttl,
					uint16_t	features
					) noexcept;

		/** */
		void	setFriendshipApi(
					Oscl::BT::Mesh::Network::Friendship::Api& api
					) noexcept;

		/** */
		Oscl::BT::Mesh::Subscription::
		Register::IterationApi& getSubscriptionIterationApi() noexcept;

	private: // Oscl::BT::Mesh::Transport::Upper::RX::Context::Api
		/** Process the decrypted Access Message
			The appKeyIndex is 0xFFFF if this message
			is was decrypted using the DevKey.
		 */
		void	processAccessMessage(
							const void*	frame,
							unsigned	length,
							uint16_t	src,
							uint16_t	dst,
							uint16_t	appKeyIndex
							) noexcept;

		/** */
		const void*	getDeviceKey() const noexcept;

	private:
		/** */
		const Oscl::BT::Mesh::Network::Crypto::Credentials&	getTxCredentials() const noexcept;

		/** */
		const NetKey&	getTxNetKey() const noexcept;

	private:
		/** */
		void	stopAllTimers() noexcept;

		/** */
		void	stopAccessReassmeblers() noexcept;

		/** */
		void	stopControlReassmeblers() noexcept;

		/** */
		void	stopReassmeblers() noexcept;

		/** */
		void	flushRelayQ() noexcept;

		/** */
		void	flushNetQ() noexcept;

		/** */
		void	flushRetransmitQ() noexcept;

		/** */
		void	flushLoopQ() noexcept;

    private: // Oscl::Done::Operation _allFreeNotification
		/** */
		void	allFreeNotification() noexcept;

	private:
		/** */
		Oscl::BT::Mesh::Key::Record* findAppKey(unsigned index) noexcept;

		/** */
		bool	addressWantedLocally(uint16_t dst) const noexcept;

	private: // Oscl::BT::Mesh::Key::Api
		/** RETURN: zero on success or result. */
		const Oscl::BT::Mesh::Key::Result::Var::State*
			add(
				unsigned index,
				const uint8_t	key[16]
				) noexcept;

		/** RETURN: zero on success or result. */
		const Oscl::BT::Mesh::Key::Result::Var::State*
			remove(unsigned index) noexcept;

		/** RETURN: zero on success or result. */
		const Oscl::BT::Mesh::Key::Result::Var::State*
			update(
				unsigned index,
				const uint8_t	key[16]
				) noexcept;

		/** RETURN: true if item found or iteration stopped. */
		bool	iterateTX(Oscl::BT::Mesh::Key::Iterator::Api& iterator) noexcept;

		/** RETURN: true if item found or iteration stopped. */
		bool	iterateRX(Oscl::BT::Mesh::Key::Iterator::Api& iterator) noexcept;

	public: // Oscl::BT::Mesh::Network::Api
		/** */
		const void*	getNetKey() const noexcept;

		/** */
		const void*	getBeaconKey() const noexcept;

		/** */
		uint16_t	getKeyIndex() const noexcept;

		/** */
		uint8_t		getFlags() const noexcept;

		/** */
		uint32_t	getTxIvIndex() const noexcept;

		/** */
		uint16_t	getUnicastAddress() const noexcept;

		/** */
		const void*	getNetworkID() const noexcept;

		/** */
		const void*	getIdentityKey() const noexcept;

		/** */
		uint16_t	allocateAddresses(uint8_t nElements) noexcept;

		/** */
		Oscl::BT::Mesh::Key::Api&	getAppKeyApi() noexcept;

		/** */
		uint8_t	getNodeIdentityState() const noexcept;

		/** */
		void	setNodeIdentityState(uint8_t state) noexcept;

		/** RETURN: 0 for failure. */
		Oscl::BT::Mesh::Element::Api*	findElement(uint16_t unicastAddress) noexcept;

		/** RETURN: true for failure. */
		bool	updateKey(const uint8_t key[16]) noexcept;

		/** RETURN: true for failure. */
		bool	deleteNetwork() noexcept;

		/** RETURN: true for failure (friend not supported). */
		bool	setFriendState(uint8_t state) noexcept;

		/** */
		uint8_t	getFriendState() noexcept;

		/** */
		void	setNetworkKeyPhase(uint8_t transition) noexcept;

		/** */
        uint8_t	getNetworkKeyPhase() noexcept;

		/** */
		void	getConfigHeartbeatSubscriptionStatus(
					uint16_t&	source,
					uint16_t&	destination,
					uint8_t&	periodLog,
					uint8_t&	countLog,
					uint8_t&	minHops,
					uint8_t&	maxHops
					) noexcept;

		/** RETURN: true if invalid address */
		bool	setConfigHeartbeatSubscription(
					uint16_t	source,
					uint16_t	destination,
					uint8_t		periodLog
					) noexcept;

		/** */
		void	getLowPowerNodePollTimeout(
					uint16_t	lpnAddress,
					uint32_t&	pollTimeout
					) noexcept;

		/** */
		void	getNetworkTransmit(
					uint8_t&	networkTransmitCount,
					uint8_t&	networkTransmitIntervalSteps
					) noexcept;

		/** */
		void	setNetworkTransmit(
					uint8_t	networkTransmitCount,
					uint8_t	networkTransmitIntervalSteps
					) noexcept;

		/** This operation attempts to decrypt the proxy frame
			and forward the result to the fwdApi.
			RETURN: true if the frame is successfully
			decrypted (belongs to this network) and forwarded.
		 */
		bool	decryptProxy(
					Oscl::Frame::FWD::Api&	fwdApi,
					const void*				frame,
					unsigned int			length
					) const noexcept;

		/**	This operation encrypts a proxy message
			and forwards the result to the fwdApi.
		 */
		void	encryptProxy(
					Oscl::Pdu::FWD::Api&	fwdApi,
					Oscl::Pdu::Pdu* 		pdu
					) noexcept;

		/** Process a received mesh network message.

			The ifApi pointer is optional.

			RETURN: true if the frame belongs to this
			network.
		 */
		bool	rxNetwork(
					const void*					frame,
					unsigned 					length,
					Oscl::BT::Mesh::
					Network::Interface::Api*	ifApi	= 0
					) noexcept;

		/** */
		void	getBeaconState(
					uint32_t&	ivIndex,
					bool&		ivUpdateFlag,
					bool&		keyRefreshFlag
					) const noexcept;

		/** */
		void	ivIndexStateChanged() noexcept;

	private: // Oscl::BT::Mesh::Transport::Lower::Reassemble::Access::Reaper::ContextApi
		/** This operation is invoked for each
			unique segment that is received.
			This filtered list may be used by
			Friends to maintain the Friend
			queue for segments destined to the LPN.
		 */
		void	segmentReceived(
					const void*		frame,
					unsigned		length,
					uint32_t		ivIndex,
					uint32_t		seq,
					uint16_t		src,
					uint16_t		dst,
					bool			ctl,
					uint8_t			ttl
					) noexcept;

		/** */
		void	stopped(Oscl::BT::Mesh::Transport::Lower::Reassemble::Access::Reaper& part) noexcept;

	private: // Oscl::BT::Mesh::Transport::Lower::Reassemble::Control::Reaper::ContextApi
		/** */
		void	stopped(Oscl::BT::Mesh::Transport::Lower::Reassemble::Control::Reaper& part) noexcept;

	private:
		/** */
		void	rxLowerTransport(
					uint16_t	srcAddress,
					uint16_t	dstAddress,
					uint32_t	ivIndex,
					uint32_t	seq,
					uint8_t		ttl,
					const void*	lowerTransportPDU,
					unsigned	length
					) noexcept;

		/** */
		void	rxUpperTransportControlPDU(
					const void*	upperTransportAccessPdu,
					unsigned	pduLength,
					uint32_t	ivIndex,
					uint32_t	seq,
					uint16_t	src,
					uint16_t	dst,
					uint8_t		ttl,
					uint8_t		opcode
					) noexcept;

		/** */
		void	forwardLowerTransportAckPduToAllElements(
					const void*	lowerTransportAckPDU,
					unsigned	pduLength,
					uint32_t	seq,
					uint16_t	src,
					uint16_t	dst
					) noexcept;

		/** RETURN: true if the PDU was destined
			for a local Element.
		 */
		bool	rxLocalNetworkPDU(
					const void*	frame,
					unsigned	length,
					uint32_t	ivIndex,
					uint32_t	seq,
					uint16_t	src,
					uint16_t	dst,
					bool		ctl,
					uint8_t		ttl
					) noexcept;

		/** */
		void	processAccessMessage(
					const void*	frame,
					unsigned	length,
					uint16_t	src
					) noexcept;

		/** */
		void	sendHeartbeat() noexcept;

		/** */
		void	processSegmentedTransportControlMessage(
					uint16_t	srcAddress,
					uint16_t	dstAddress,
					uint32_t	seq,
					uint32_t	ivIndex,
					uint8_t		ttl,
					const void*	lowerTransportPDU,
					unsigned	length,
					uint8_t		opcode
					) noexcept;

		/** */
		void	processTransportControlMessage(
					uint16_t	srcAddress,
					uint16_t	dstAddress,
					uint32_t	seq,
					uint32_t	ivIndex,
					uint8_t		ttl,
					const void*	lowerTransportPDU,
					unsigned	length
					) noexcept;

		/** */
		void	processUnsegmentedHeartbeat(
					uint16_t	srcAddress,
					uint16_t	dstAddress,
					uint32_t	seq,
					uint8_t		ttl,
					const void*	lowerTransportPDU,
					unsigned	length
					) noexcept;

	public: // Oscl::BT::Mesh::Element::Symbiont::Host::Api
		/**	This operation attaches the symbiont to the Mesh Network
			host.
		 */
		void	attach(
					Oscl::BT::Mesh::Element::Symbiont::Api&	symbiont
					) noexcept;

		/**	This operation detaches the symbiont from the Mesh
			Network host.
		 */
		void	detach(	
					Oscl::BT::Mesh::Element::Symbiont::Api&	symbiont
					) noexcept;

	private: // Oscl::BT::Mesh::Element::Symbiont::Kernel::Api
		/** This operation may be used by the symbiont
			to queue a network PDU for transmission on the
			mesh network.
			This is the interface to the network used
			by the lower-transport layer.

			param [in] pdu		Pointer to the lower transport
								PDU. The destination address is
								encoded in the first two octets.
			param [in] ivIndex	IV Index
			param [in] seq		24-bit Sequence number
			param [in] src		Source address
			param [in] dst		Destination address
			param [in] ttl		Optional Time-To-Live value
		 */
		void	transmitNetwork(
					Oscl::Pdu::Pdu* pdu,
					uint32_t		ivIndex,
					uint32_t		seq,
					uint16_t		src,
					uint16_t		dst,
					uint8_t			ttl
					) noexcept;

		/** This operation may be used by the symbiont
			to queue a control PDU for transmission on the
			mesh network.
			This is the interface to the network used
			by the lower-transport layer.

			param [in] pdu		Pointer to the lower transport
								PDU. The destination address is
								encoded in the first two octets.
			param [in] ivIndex	IV Index
			param [in] seq		24-bit Sequence number
			param [in] src		Source address
			param [in] dst		Destination address
			param [in] ttl		Optional Time-To-Live value
		 */
		void	transmitControl(
					Oscl::Pdu::Pdu* pdu,
					uint32_t		ivIndex,
					uint32_t		seq,
					uint16_t		src,
					uint16_t		dst,
					uint8_t			ttl
					) noexcept;

		/** Transmit/queue the fully encrypted/formed network PDU.
		 */
		void	transmitNetworkPDU(
					Oscl::Pdu::Pdu* pdu,
					uint16_t		dst
					) noexcept;

		/** */
		uint32_t	allocSequenceNumber() noexcept;

		/** */
		uint8_t		getDefaultTTL() const noexcept;

		/** */
		Oscl::BT::Mesh::Subscription::Register::Api&
				getSubscriptionRegisterApi() noexcept;

		/** */
		void	forwardUpperTransportPduToAllElements(
					const void*	upperTransportAccessPdu,
					unsigned	pduLength,
					uint32_t	ivIndex,
					uint32_t	seq,
					uint16_t	src,
					uint16_t	dst,
					uint8_t		aid,
					bool		akf,
					bool		segmented
					) noexcept;

	private:
		/** */
		void	generateKeys() noexcept;

		/** */
		void	generateMasterSecurityMaterial() noexcept;

		/** */
		void	generateNetworkID() noexcept;

		/** */
		void	generateIdentityKey() noexcept;

		/** */
		void	generateBeaconKey() noexcept;

		/** The transportPDU has the destination
			address pre-pended.

			param [in] ctl		Determines the MIC length
			param [in] ttl		Time-To-Live value
			param [in] src		Source address
			param [in] dst		Destination address
			param [in] seq		24-bit Sequence number
			param [in] ivIndex	IV Index
			param [in] pdu		Pointer to the lower transport
								PDU. The destination address is
								encoded in the first two octets.
			param [in] pduLen	Length of the PDU
		 */
		void	transmitNetwork(
					bool		ctl,
					uint8_t		ttl,
					uint16_t	src,
					uint16_t	dst,
					uint32_t	seq,
					uint32_t	ivIndex,
					const void*	pdu,
					unsigned	pduLen
					) noexcept;

		/** The transportPDU has the destination
			address pre-pended.

			param [in] ctl		Determines the MIC length
			param [in] ttl		Time-To-Live value
			param [in] src		Source address
			param [in] seq		24-bit Sequence number
			param [in] ivIndex	IV Index
			param [in] pdu		Pointer to the lower transport
								PDU. The destination address is
								encoded in the first two octets.
			param [in] pduLen	Length of the PDU
		 */
		Oscl::Pdu::Pdu*	buildNetworkPDU(
							bool		ctl,
							uint8_t		ttl,
							uint16_t	src,
							uint32_t	seq,
							uint32_t	ivIndex,
							const void*	pdu,
							unsigned	pduLen
							) noexcept;


		/** The transportPDU has the destination
			address pre-pended.
		 */
		void	relayMessage(
					bool		ctl,
					uint8_t		ttl,
					uint16_t	src,
					uint16_t	dst,
					uint32_t	seq,
					uint32_t	ivIndex,
					const void*	transportPDU,
					unsigned	length
					) noexcept;

	private: // Oscl::Frame::RF::RX::Composer	_rxMeshMessage;
		/** The rssi value is assumed to be compatible
			with the Specification of the Bluetooth System, v5.0.

			Therefore, We will assume that a value of 127 indicates
			that the RSSI parameter is not available.

			However, we will allow values above 20, expecting
			an new standard values to simply be extensions
			in dBm. We expect the caller (Host Controller) to
			supply only dBm values or 127. It should filter
			and map any other "special" values to comply with
			the expected meaning here.
		 */
		bool	rxMeshMessage(
					const void*		frame,
					unsigned 		length,
					signed			rssi
					) noexcept;

	private: // Oscl::Frame::FWD::Composer	_rxSecureNetworkBeaconItem;
		/** */
		bool	rxSecureNetworkBeacon(
					const void*		frame,
					unsigned 		length
					) noexcept;

	private:	// Oscl::Done::Operation _netTxTimerExpired
		/** */
		void	netTxTimerExpired() noexcept;

	private:	// Oscl::Done::Operation _netReTxTimerExpired
		/** */
		void	netReTxTimerExpired() noexcept;

	private:	// Oscl::Done::Operation _relayTxTimerExpired
		/** */
		void	relayTxTimerExpired() noexcept;

	private:	// Oscl::Done::Operation _heartbeatTxTimerExpired
		/** */
		void	heartbeatTxTimerExpired() noexcept;

	private:
		/** */
		void	publishHeartbeat() noexcept;

	private:	// Oscl::Done::Operation _heartbeatSubTimerExpired
		/** */
		void	heartbeatSubTimerExpired() noexcept;

	private:
		/** */
		void	loopBearerTx(
					Oscl::Pdu::Pdu*	pdu,
					uint16_t		dst
					) noexcept;
	private:
		/** */
		void	processLoopQ() noexcept;

	private:
		/** */
		NetMessageMem*	allocNetMessageMem() noexcept;

		/** */
		void	freeNetMessage(NetMessage* mem) noexcept;

	private:
		/** */
		bool	createKey(
					uint16_t		keyIndex,
					const uint8_t	key[16],
					uint8_t			aid,
					bool			updating,
					const uint8_t	newKey[16],
					uint8_t			newAID
					) noexcept;

	private: // Oscl::BT::Mesh::Key::Persist::Iterator::Composer
		/**	This iteration callback is invoked for each
			persistent item.

			RETURN: true to stop iteration or false
			to continue the iteration.
		 */
		bool	constructorNextKeyItem(
					Oscl::BT::Mesh::
					Key::Persist::Api&	api,
					uint16_t			keyIndex,
					const uint8_t		key[16],
					uint8_t				aid,
					bool				updating,
					const uint8_t		newKey[16],
					uint8_t				keyAID
					) noexcept;

	private:
		/** RETURN: 0 if not found */
		Oscl::BT::Mesh::Key::Persist::Api*	findKeyPersistApi(uint16_t keyIndex) noexcept;

	private: // Oscl::BT::Mesh::Key::Persist::Iterator::Composer
		/**	This iteration callback is invoked for each
			persistent item.

			RETURN: true to stop iteration or false
			to continue the iteration.
		 */
		bool	findNextKeyItem(
					Oscl::BT::Mesh::
					Key::Persist::Api&	api,
					uint16_t			keyIndex,
					const uint8_t		key[16],
					uint8_t				aid,
					bool				updating,
					const uint8_t		newKey[16],
					uint8_t				newAID
					) noexcept;

	private: // Oscl::BT::Mesh::Beacon::Secure::Network::ContextApi
		/** */
		void	sendSecureNetworkBeacon() noexcept;

	private: // Key Refresh
		/** */
		void	revokeOldNetKey() noexcept;

		/** */
		void	revokeUpdatingAppKeys() noexcept;

	private: // Oscl::BT::Mesh::Network::KeyRefresh::StatVar::ContextApi
		/**	This operation is used to determine if the
			received Secure Network Beacon was received with
			the new NetKey.
		 */
		bool	secureNetworkBeaconReceivedWithNewKey() noexcept;

		/**	This operation is used to determine if the Key
			Refresh Flag was set in the last received Secure
			Network Beacon.
		 */
		bool	isKeyRefreshFlagSetInLastBeacon() noexcept;

		/**	This operation is used to determine if the key
			received in the most recent Config NetKey UpdateKey
			is the same as the new NetKey.
		 */
		bool	isUpdateKeyEqualToTheNewKey() noexcept;

		/**	This operation is used to determine if the key
			received in the most recent Config NetKey UpdateKey
			is the same as the old NetKey.
		 */
		bool	isUpdateKeyEqualToTheOldKey() noexcept;

		/**	This operation is used to revoke the old NetKey
			and resume normal operation. This operation may
			asynchronously write persistence and then generate
			a revokeOldKeysComplete when that operation
			completes.
		 */
		void	revokeOldKeys() noexcept;

		/**	This operation is used to cause the new Key
			from the last Config NetKey Update message to be
			activated as the new key.
		 */
		void	initializeNewNetKey() noexcept;

		/**	This operation is used to indicate that the
			last Config NetKey Update that generated the
			configNetKeyUpdate event should indicate a failure.
		 */
		void	configNetKeyUpdateFailed() noexcept;

		/**	This operation is used to indicate that the
			last Config NetKey Update that generated the
			configNetKeyUpdate event should indicate success.
		 */
		void	configNetKeyUpdateSucceeded() noexcept;

		/**	This operation is used to tell the context that
			the curent Phase is 0.
		 */
		void	enteredPhase0() noexcept;

		/**	This operation is used to tell the context that
			the curent Phase is 1.
		 */
		void	enteredPhase1() noexcept;

		/**	This operation is used to tell the context that
			the curent Phase is 2.
		 */
		void	enteredPhase2() noexcept;

		/**	This operation is used to tell the context that
			the curent Phase is 3.
		 */
		void	enteredPhase3() noexcept;
	};

}
}
}
}

#endif
