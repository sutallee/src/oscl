/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_bluetooth_mesh_network_interface_apih_
#define _oscl_bluetooth_mesh_network_interface_apih_

#include "oscl/frame/fwd/api.h"
#include "oscl/pdu/fwd/api.h"
#include "oscl/bluetooth/mesh/key/api.h"
#include "oscl/bluetooth/mesh/element/api.h"

/** */
namespace Oscl {
/** */
namespace BT {
/** */
namespace Mesh {
/** */
namespace Network {
/** */
namespace Interface {

/** This interface may be implemented by by
	a network interface and used to collect
	network information about individual
	packets.

	The operations are invoked by the Network
	layer during the procesing of a packet.

	This allows the lower-layer network
	interface to collect various information
	about each packet that is not available
	to the interface itself since the packets
	are decrypted at the network layer.
 */
class Api {
	public:
		/** This operation is invoked by the Network
			if the packet is successfully decrypted
			and by the Network layer.
		 */
		virtual void	netPacketInfo(uint16_t src) noexcept=0;
	};

}
}
}
}
}

#endif
