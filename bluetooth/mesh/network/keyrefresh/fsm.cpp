/*
   Copyright (C) 2020 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "fsm.h"
#include "oscl/error/info.h"
using namespace Oscl::BT::Mesh::Network::KeyRefresh;

//--------------- Concrete Fly-Weight States ----------------
const StateVar::Phase0	StateVar::_Phase0;
const StateVar::Phase1	StateVar::_Phase1;
const StateVar::Phase2	StateVar::_Phase2;
const StateVar::Phase3	StateVar::_Phase3;

//--------------- StateVar constructor ----------------

StateVar::StateVar(
	ContextApi&				context,
	const StateVar::State*	initialState
	) noexcept:
		_context(context),
		_state(initialState?initialState:&_Phase0)
		{
	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s: initialState: %p\n",
		OSCL_PRETTY_FUNCTION,
		initialState
		);
	#endif
	}

//--------------- StateVar events ----------------
void	StateVar::configNetKeyUpdate() noexcept{
	_state->configNetKeyUpdate(_context,*this);
	}

void	StateVar::configKeyRefreshPhaseSet2() noexcept{
	_state->configKeyRefreshPhaseSet2(_context,*this);
	}

void	StateVar::configKeyRefreshPhaseSet3() noexcept{
	_state->configKeyRefreshPhaseSet3(_context,*this);
	}

void	StateVar::secureNetworkBeaconReceived() noexcept{
	_state->secureNetworkBeaconReceived(_context,*this);
	}

void	StateVar::revokeOldKeysComplete() noexcept{
	_state->revokeOldKeysComplete(_context,*this);
	}

//--------------- StateVar state-change operations ----------------
void StateVar::changeToPhase0() noexcept{
	_state	= &_Phase0;
	}

void StateVar::changeToPhase1() noexcept{
	_state	= &_Phase1;
	}

void StateVar::changeToPhase2() noexcept{
	_state	= &_Phase2;
	}

void StateVar::changeToPhase3() noexcept{
	_state	= &_Phase3;
	}

//--------------- StateVar state-accessor operations ----------------
const StateVar::State& StateVar::getPhase0() noexcept{
	return _Phase0;
	}

const StateVar::State& StateVar::getPhase1() noexcept{
	return _Phase1;
	}

const StateVar::State& StateVar::getPhase2() noexcept{
	return _Phase2;
	}

const StateVar::State& StateVar::getPhase3() noexcept{
	return _Phase3;
	}

//--------------- State ----------------
