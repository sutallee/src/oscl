/*
   Copyright (C) 2020 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_bt_mesh_network_keyrefresh_fsmh_
#define _oscl_bt_mesh_network_keyrefresh_fsmh_

/** */
namespace Oscl {

/** */
namespace BT {

/** */
namespace Mesh {

/** */
namespace Network {

/** */
namespace KeyRefresh {

/**	This StateVar class implements a Finite State Machine
	(FSM) by applying a refinement of the GOF State Pattern,
	which is relized by applying another GOF pattern named the
	Flyweight Pattern. The Flyweight Pattern is used in the
	implementation of the state subclasses allowing a single
	set of concrete state classes to be shared between any number
	of instances of this FSM.
 */
/**	This FSM describes the Bluetooth Mesh Key Refresh
	behavior.
 */
class StateVar {
	public:
		/**	This interface is implemented by the context whose behavior
			is controlled by the FSM implemented by this StateVar.
		 */
		class ContextApi {
			public:
				/** Make the compiler happy. */
				virtual ~ContextApi() {}
				/**	This operation is used to determine if the
					received Secure Network Beacon was received with
					the new NetKey.
				 */
				virtual bool	secureNetworkBeaconReceivedWithNewKey() noexcept=0;

				/**	This operation is used to determine if the Key
					Refresh Flag was set in the last received Secure
					Network Beacon.
				 */
				virtual bool	isKeyRefreshFlagSetInLastBeacon() noexcept=0;

				/**	This operation is used to determine if the key
					received in the most recent Config NetKey UpdateKey
					is the same as the new NetKey.
				 */
				virtual bool	isUpdateKeyEqualToTheNewKey() noexcept=0;

				/**	This operation is used to determine if the key
					received in the most recent Config NetKey UpdateKey
					is the same as the old NetKey.
				 */
				virtual bool	isUpdateKeyEqualToTheOldKey() noexcept=0;

				/**	This operation is used to revoke the old NetKey
					and resume normal operation. This operation may
					asynchronously write persistence and then generate
					a revokeOldKeysComplete when that operation
					completes.
				 */
				virtual void	revokeOldKeys() noexcept=0;

				/**	This operation is used to cause the new Key
					from the last Config NetKey Update message to be
					activated as the new key.
				 */
				virtual void	initializeNewNetKey() noexcept=0;

				/**	This operation is used to indicate that the
					last Config NetKey Update that generated the
					configNetKeyUpdate event should indicate a failure.
				 */
				virtual void	configNetKeyUpdateFailed() noexcept=0;

				/**	This operation is used to indicate that the
					last Config NetKey Update that generated the
					configNetKeyUpdate event should indicate success.
				 */
				virtual void	configNetKeyUpdateSucceeded() noexcept=0;

				/**	This operation is used to tell the context that
					the curent Phase is 0.
				 */
				virtual void	enteredPhase0() noexcept=0;

				/**	This operation is used to tell the context that
					the curent Phase is 1.
				 */
				virtual void	enteredPhase1() noexcept=0;

				/**	This operation is used to tell the context that
					the curent Phase is 2.
				 */
				virtual void	enteredPhase2() noexcept=0;

				/**	This operation is used to tell the context that
					the curent Phase is 3.
				 */
				virtual void	enteredPhase3() noexcept=0;

			};
	private:
		/** */
		class State {
			public:
				/** Make the compiler happy. */
				virtual ~State() {}
				/**	This event is invoked whenever a
					Config NetKey Update is received.
				 */
				virtual void	configNetKeyUpdate(	ContextApi&	context,
											StateVar&	stateVar
											) const noexcept;

				/**	This event is invoked whenever a
					Config Key Refresh Phase Set with
					transition 2 is received.
				 */
				virtual void	configKeyRefreshPhaseSet2(	ContextApi&	context,
											StateVar&	stateVar
											) const noexcept;

				/**	This event is invoked whenever a
					Config Key Refresh Phase Set with
					transition 3 is received.
				 */
				virtual void	configKeyRefreshPhaseSet3(	ContextApi&	context,
											StateVar&	stateVar
											) const noexcept;

				/**	This event is happens whenever a
					Secure Network Beacon is received.
				 */
				virtual void	secureNetworkBeaconReceived(	ContextApi&	context,
											StateVar&	stateVar
											) const noexcept;

				/**	This event is happens when the
					revokeOldKeys operation completes.
				 */
				virtual void	revokeOldKeysComplete(	ContextApi&	context,
											StateVar&	stateVar
											) const noexcept;

			};
		/** */
		friend class State;

	private:
		/**	While in this state the network transmits using the
			normal/old NetKey and receives with either the new or old
			NetKey.
		 */
		class Phase0 : public State {
			public:
				/** The cosntructor.
				 */
				Phase0() noexcept;

			public:
				/**	This event is invoked whenever a Config NetKey Update is
					received.
				 */
				void	configNetKeyUpdate(	ContextApi&	context,
									StateVar&	stateVar
									) const noexcept;

			};

		/** */
		friend class Phase0;

	private:
		/**	While in this state the network transmits using the new
			NetKey and receives with either the new or old NetKey.
		 */
		class Phase1 : public State {
			public:
				/** The cosntructor.
				 */
				Phase1() noexcept;

			public:
				/**	This event is invoked whenever a Config NetKey Update is
					received.
				 */
				void	configNetKeyUpdate(	ContextApi&	context,
									StateVar&	stateVar
									) const noexcept;

				/**	This event is invoked whenever a Config Key Refresh Phase
					Set with transition 2 is received.
				 */
				void	configKeyRefreshPhaseSet2(	ContextApi&	context,
									StateVar&	stateVar
									) const noexcept;

				/**	This event is invoked whenever a Config Key Refresh Phase
					Set with transition 3 is received.
				 */
				void	configKeyRefreshPhaseSet3(	ContextApi&	context,
									StateVar&	stateVar
									) const noexcept;

				/**	This event is happens whenever a Secure Network Beacon is
					received.
				 */
				void	secureNetworkBeaconReceived(	ContextApi&	context,
									StateVar&	stateVar
									) const noexcept;

			};

		/** */
		friend class Phase1;

	private:
		/**	While in this state the network transmits using the new
			NetKey and receives with the new NetKey.
		 */
		class Phase2 : public State {
			public:
				/** The cosntructor.
				 */
				Phase2() noexcept;

			public:
				/**	This event is invoked whenever a Config Key Refresh Phase
					Set with transition 3 is received.
				 */
				void	configKeyRefreshPhaseSet3(	ContextApi&	context,
									StateVar&	stateVar
									) const noexcept;

				/**	This event is happens whenever a Secure Network Beacon is
					received.
				 */
				void	secureNetworkBeaconReceived(	ContextApi&	context,
									StateVar&	stateVar
									) const noexcept;

			};

		/** */
		friend class Phase2;

	private:
		/**	
		 */
		class Phase3 : public State {
			public:
				/** The cosntructor.
				 */
				Phase3() noexcept;

			public:
				/**	This event is happens when the revokeOldKeys operation
					completes.
				 */
				void	revokeOldKeysComplete(	ContextApi&	context,
									StateVar&	stateVar
									) const noexcept;

			};

		/** */
		friend class Phase3;

	private:
		/** This is the state-less shared instance (fly-weight)
			of the Phase0 state.
		 */
		const static Phase0	_Phase0;

		/** This is the state-less shared instance (fly-weight)
			of the Phase1 state.
		 */
		const static Phase1	_Phase1;

		/** This is the state-less shared instance (fly-weight)
			of the Phase2 state.
		 */
		const static Phase2	_Phase2;

		/** This is the state-less shared instance (fly-weight)
			of the Phase3 state.
		 */
		const static Phase3	_Phase3;

	private:
		/** This member references the context.
		 */
		ContextApi&		_context;

		/** This member determines the current state of the FSM.
		 */
		const State*	_state;

	public:
		/** The constructor requires a reference to its context.
		 */
		StateVar(
			ContextApi&				context,
			const StateVar::State*	initialState = 0
			) noexcept;

		/** Be happy compiler! */
		virtual ~StateVar() {}

	public:
		/**	This event is invoked whenever a Config NetKey Update is
			received.
		 */
		void	configNetKeyUpdate() noexcept;

		/**	This event is invoked whenever a Config Key Refresh Phase
			Set with transition 2 is received.
		 */
		void	configKeyRefreshPhaseSet2() noexcept;

		/**	This event is invoked whenever a Config Key Refresh Phase
			Set with transition 3 is received.
		 */
		void	configKeyRefreshPhaseSet3() noexcept;

		/**	This event is happens whenever a Secure Network Beacon is
			received.
		 */
		void	secureNetworkBeaconReceived() noexcept;

		/**	This event is happens when the revokeOldKeys operation
			completes.
		 */
		void	revokeOldKeysComplete() noexcept;

	private:
		/** This operation is invoked during FSM transitions and causes
			the FSM to change to the Phase0 state.
		 */
		void changeToPhase0() noexcept;

		/** This operation is invoked during FSM transitions and causes
			the FSM to change to the Phase1 state.
		 */
		void changeToPhase1() noexcept;

		/** This operation is invoked during FSM transitions and causes
			the FSM to change to the Phase2 state.
		 */
		void changeToPhase2() noexcept;

		/** This operation is invoked during FSM transitions and causes
			the FSM to change to the Phase3 state.
		 */
		void changeToPhase3() noexcept;

	public:
		/** This operation is used to geta reference
			to the Phase0 state fly-weight.
		 */
		static const StateVar::State& getPhase0() noexcept;

		/** This operation is used to geta reference
			to the Phase1 state fly-weight.
		 */
		static const StateVar::State& getPhase1() noexcept;

		/** This operation is used to geta reference
			to the Phase2 state fly-weight.
		 */
		static const StateVar::State& getPhase2() noexcept;

		/** This operation is used to geta reference
			to the Phase3 state fly-weight.
		 */
		static const StateVar::State& getPhase3() noexcept;

	private:
	private:
		/** This operation is invoked when the FSM detects
			a protocol violation.
		 */
		void	protocolError() noexcept;
	};
}
}
}
}
}
#endif
