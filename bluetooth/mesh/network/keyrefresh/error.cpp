/*
   Copyright (C) 2020 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "fsm.h"
#include "oscl/error/fatal.h"
#include "oscl/error/info.h"
using namespace Oscl::BT::Mesh::Network::KeyRefresh;

//--------------- protocol error ----------------
void	StateVar::protocolError() noexcept{
	// There is a design flaw if this operation is ever invoked.
	// Either the context is invoking the contrary to the way
	// in which the FSM was intended to be used, or the implementation
	// of the FSM event handlers in this file are not written
	// properly.
	Oscl::ErrorFatal::logAndExit(
		"%s: %p\n",
		OSCL_PRETTY_FUNCTION,
		this
		);
	}

