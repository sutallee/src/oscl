/*
   Copyright (C) 2020 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

//#define DEBUG_TRACE
//#define DEBUG_TRACE_TMI

#include "fsm.h"

#ifdef DEBUG_TRACE
#include "oscl/error/info.h"
#endif

using namespace Oscl::BT::Mesh::Network::KeyRefresh;


//--------------- Procedures ----------------
//--------------- State ----------------
void	StateVar::State::configNetKeyUpdate(	ContextApi&		context,
								StateVar&	stateVar
								) const noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif

	context.configNetKeyUpdateFailed();
	}

void	StateVar::State::configKeyRefreshPhaseSet2(	ContextApi&		context,
								StateVar&	stateVar
								) const noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s: ignored.\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif

	/*
		We're ignoring this since it may be retransmitted
		by the configuration client and therefore may
		be received while in the wrong state.
	 */
	}

void	StateVar::State::configKeyRefreshPhaseSet3(	ContextApi&		context,
								StateVar&	stateVar
								) const noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif

	/*
		We're ignoring this since it may be retransmitted
		by the configuration client and therefore may
		be received while in the wrong state.
	 */
	}

void	StateVar::State::secureNetworkBeaconReceived(	ContextApi&		context,
								StateVar&	stateVar
								) const noexcept{

	#ifdef DEBUG_TRACE_TMI
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif

	}

void	StateVar::State::revokeOldKeysComplete(	ContextApi&		context,
								StateVar&	stateVar
								) const noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif

	stateVar.protocolError();
	}

//--------------- Phase0 ----------------
StateVar::Phase0::Phase0() noexcept
		{
	}
void	StateVar::Phase0::configNetKeyUpdate(	ContextApi&	context,
									StateVar&	stateVar
									) const noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif

	if(context.isUpdateKeyEqualToTheOldKey()){
		context.configNetKeyUpdateFailed();
		}
	else {
		stateVar.changeToPhase1();
		context.enteredPhase1();
		context.initializeNewNetKey();
		context.configNetKeyUpdateSucceeded();
		}

	}
//--------------- Phase1 ----------------
StateVar::Phase1::Phase1() noexcept
		{
	}

void	StateVar::Phase1::configNetKeyUpdate(	ContextApi&	context,
									StateVar&	stateVar
									) const noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif

	if(context.isUpdateKeyEqualToTheNewKey()){
		context.configNetKeyUpdateSucceeded();
		}
	else {
		context.configNetKeyUpdateFailed();
		}

	}

void	StateVar::Phase1::configKeyRefreshPhaseSet2(	ContextApi&	context,
									StateVar&	stateVar
									) const noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif

	stateVar.changeToPhase2();
	context.enteredPhase2();

	}

void	StateVar::Phase1::configKeyRefreshPhaseSet3(	ContextApi&	context,
									StateVar&	stateVar
									) const noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif

	stateVar.changeToPhase3();
	context.enteredPhase3();
	context.revokeOldKeys();

	}

void	StateVar::Phase1::secureNetworkBeaconReceived(	ContextApi&	context,
									StateVar&	stateVar
									) const noexcept{

	if(context.secureNetworkBeaconReceivedWithNewKey()){

		if(context.isKeyRefreshFlagSetInLastBeacon()){

			#ifdef DEBUG_TRACE
			Oscl::Error::Info::log(
				"%s: to Phase 2\n",
				OSCL_PRETTY_FUNCTION
				);
			#endif

			stateVar.changeToPhase2();
			context.enteredPhase2();
			}
		else {

			#ifdef DEBUG_TRACE
			Oscl::Error::Info::log(
				"%s: to Phase 3\n",
				OSCL_PRETTY_FUNCTION
				);
			#endif

			stateVar.changeToPhase3();
			context.enteredPhase3();
			context.revokeOldKeys();
			}
		}

	}

//--------------- Phase2 ----------------
StateVar::Phase2::Phase2() noexcept
		{
	}
void	StateVar::Phase2::configKeyRefreshPhaseSet3(	ContextApi&	context,
									StateVar&	stateVar
									) const noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif

	stateVar.changeToPhase3();
	context.enteredPhase3();
	context.revokeOldKeys();

	}

void	StateVar::Phase2::secureNetworkBeaconReceived(	ContextApi&	context,
									StateVar&	stateVar
									) const noexcept{

	if(context.secureNetworkBeaconReceivedWithNewKey()){
		if(context.isKeyRefreshFlagSetInLastBeacon()){

			#ifdef DEBUG_TRACE
			Oscl::Error::Info::log(
				"%s: to Phase 3\n",
				OSCL_PRETTY_FUNCTION
				);
			#endif

			stateVar.changeToPhase3();
			context.enteredPhase3();
			context.revokeOldKeys();
			}
		}

	}

//--------------- Phase3 ----------------
StateVar::Phase3::Phase3() noexcept
		{
	}
void	StateVar::Phase3::revokeOldKeysComplete(	ContextApi&	context,
									StateVar&	stateVar
									) const noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n",
		OSCL_PRETTY_FUNCTION
		);
	#endif

	stateVar.changeToPhase0();
	context.enteredPhase0();
	}

