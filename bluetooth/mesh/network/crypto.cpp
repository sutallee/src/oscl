/*
   Copyright (C) 2020 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include "crypto.h"
#include "oscl/bluetooth/mesh/node/api.h"
#include "oscl/error/info.h"
#include "oscl/error/fatal.h"
#include "oscl/encoder/be/base.h"
#include "oscl/endian/decoder/linear/part.h"
#include "oscl/aes128/cipher.h"
#include "oscl/aes128/ccm.h"
#include "oscl/aes128/cmac.h"
#include "oscl/entropy/rand.h"
#include "constants.h"

//#define DEBUG_TRACE

unsigned	Oscl::BT::Mesh::Network::Crypto::decrypt(
				Oscl::BT::Mesh::Node::Api&	nodeApi,
				const Credentials&			credentials,
				const void*					frame,
				unsigned 					length,
				uint8_t						plaintext[128],
				uint32_t&					ivIndex,
				uint8_t&					ctl,
				uint8_t&					ttl,
				uint32_t&					seq,
				uint16_t&					src
				) noexcept {

	/*	Network PDU
		[0]		[ IVI[7] | NID[6:0] ]	clear
		[1]		[ CTL[7] | TTL[6:0] ]	obfuscated
		[2:4]	[ SEQ ]					obfuscated
		[5:6]	[ SRC ]					obfuscated
		[7:8]	[ DST ]					encrypted
		[9...]	[ Transport PDU ]		encrypted
		[...]	[ NetMIC ]
	 */

	const uint8_t*	p	= (const uint8_t*)frame;

	uint8_t	iviNID	= p[0];

	// FIXME:
	// We need to use ivi to calculate the
	// correct IV Index, and then propagate
	// that IV Index to upper receive layers.
	// Otherwise, we'll likely break after/during
	// an IV Update.
	uint8_t	ivi		= iviNID >> 7;

	uint8_t	nid		= iviNID & 0x7F;

	if(nid != credentials._nid){
		#ifdef DEBUG_TRACE
		Oscl::Error::Info::log(
			"%s: ( nid(0x%2.2X) != _nid(0x%2.2X) ) packet dropped.\n",
			__PRETTY_FUNCTION__,
			nid,
			credentials._nid
			);
		#endif
		return 0;
		}

	ivIndex	= nodeApi.getRxIvIndex(ivi);

	const uint8_t*	privacyRandom	= &p[dstOffset];

	uint8_t	pecbData[16];
	uint8_t	header[6];
		{
		Oscl::AES128::Cipher	pecb(credentials._privacyKey);

		Oscl::Encoder::BE::Base
		beEncoder(
			pecbData,
			sizeof(pecbData)
			);

		Oscl::Encoder::Api&	encoder	= beEncoder;

		static const uint8_t	zeros[5] = {0x00,0x00,0x00,0x00,0x00};

		constexpr unsigned privacyRandomLength	= 7;

		encoder.copyIn(zeros,sizeof(zeros));
		encoder.encode(ivIndex);
		encoder.copyIn(privacyRandom,privacyRandomLength);

		pecb.cipher(pecbData);

		pecb.xorBlock(
			&p[obfuscatedDataOffset],
			header,
			sizeof(header)
			);
		}

		{
		uint8_t		ctlTTL;

		Oscl::Endian::Decoder::Linear::Part
		beDecoder(
			header,
			sizeof(header)
			);

		Oscl::Decoder::Api&
		decoder	= beDecoder.be();

		decoder.decode(ctlTTL);
		decoder.decode24Bit(seq);
		decoder.decode(src);

		ctl	= ctlTTL >> 7;
		ttl	= ctlTTL & 0x7F;
		}

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s:"
		" ctl: (%u) \"%s\","
		" ttl: (0x%2.2X) %u,"
		" seq: (0x%6.6X) %u,"
		" src: (0x%4.4X)"
		" ivIndex: (0x%8.8X)"
		"\n",
		__PRETTY_FUNCTION__,
		ctl,
		ctl?"control":"network",
		ttl,
		ttl,
		seq,
		seq,
		src,
		ivIndex
		);
	Oscl::Error::Info::hexDump(
		header,
		sizeof(header)
		);
	#endif

	/* Network nonce
		offset
		[0]		Nonce Type		0x00
		[1]		CTL and TTL
		[2:4]	SEQ
		[5:6]	SRC
		[7:8]	Pad				0x0000
		[9:12]	IV Index
	 */

	uint8_t	nonce[13];
		{
		Oscl::Encoder::BE::Base
		beNonceEncoder(
			nonce,
			sizeof(nonce)
			);

		Oscl::Encoder::Api&	nonceEnc	= beNonceEncoder;

		static const uint8_t	networkNonceType	= 0x00;
		static const uint8_t	pad[]	= {0x00,0x00};

		nonceEnc.encode(networkNonceType);
		nonceEnc.copyIn(header,sizeof(header));
		nonceEnc.copyIn(pad,sizeof(pad));
		nonceEnc.encode(ivIndex);

		if(nonceEnc.overflow()){
			Oscl::ErrorFatal::logAndExit(
				"%s: WARNING! nonce overflow\n",
				OSCL_PRETTY_FUNCTION
				);
			}
		}

	const unsigned	encryptedLength	= length - dstOffset;

	unsigned
	len	= Oscl::AES128::Ccm::decrypt(
			credentials._encryptionKey,
			nonce,
			0,
			0,
			&p[dstOffset],
			encryptedLength,
			ctl?ctlMicLength:netMicLength,
			plaintext
			);

	return len;
	}

unsigned Oscl::BT::Mesh::Network::Crypto::encrypt(
			const Credentials&	credentials,
			bool				ctl,
			uint8_t				ttl,
			uint16_t			src,
			uint32_t			seq,
			uint32_t			ivIndex,
			const void*			transportPDU,
			unsigned			tranportPduLength,
			void*				buffer,
			unsigned			bufferLen
			) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s\n"
		"\tttl: %u\n"
		"\tctl: %s\n"
		"\ttransportPDU:\n"
		"",
		__PRETTY_FUNCTION__,
		ttl,
		ctl?"true":"false"
		);
	Oscl::Error::Info::hexDump(
		transportPDU,
		tranportPduLength
		);
	#endif

	const uint8_t	ctlTTL	= (
								((ctl?1:0)<<7)
							|	(ttl<<0)
							);

	uint8_t	header[dstOffset - ctlTtlOffset];
		{
		Oscl::Encoder::BE::Base
		beEncoder(
			header,
			sizeof(header)
			);
		Oscl::Encoder::Api&	encoder	= beEncoder;

		encoder.encode(ctlTTL);
		encoder.encode24Bit(seq);
		encoder.encode(src);
		}

	/* Network nonce
		offset
		[0]		Nonce Type		0x00
		[1]		CTL and TTL
		[2:4]	SEQ
		[5:6]	SRC
		[7:8]	Pad				0x0000
		[9:12]	IV Index
	 */

	uint8_t	nonce[13];
		{
		Oscl::Encoder::BE::Base
		beNonceEncoder(
			nonce,
			sizeof(nonce)
			);

		Oscl::Encoder::Api&	nonceEnc	= beNonceEncoder;

		static const uint8_t	networkNonceType	= 0x00;
		static const uint8_t	pad[]	= {0x00,0x00};

		nonceEnc.encode(networkNonceType);

		nonceEnc.copyIn(
			header,
			dstOffset - ctlTtlOffset
			);

		nonceEnc.copyIn(
			pad,
			sizeof(pad)
			);

		nonceEnc.encode(ivIndex);

		if(nonceEnc.overflow()){
			Oscl::ErrorFatal::logAndExit(
				"%s: WARNING! nonce overflow\n",
				OSCL_PRETTY_FUNCTION
				);
			}
		}

	uint8_t*	cipherText	= (uint8_t*)buffer;

	cipherText	= &cipherText[dstOffset];

	unsigned
	length	= Oscl::AES128::Ccm::encrypt(
				credentials._encryptionKey,
				nonce,
				0,	// adata
				0,	// alen
				transportPDU,		// payload at destination address
				tranportPduLength,
				ctl?ctlMicLength:netMicLength,
				cipherText
				);

	if(!length){
		Oscl::Error::Info::log(
			"%s: encrypt failed!\n",
			OSCL_PRETTY_FUNCTION
			);
		return 0;
		}

	/*	Network PDU
		[0]		[ IVI[7] | NID[6:0] ]	clear
		[1]		[ CTL[7] | TTL[6:0] ]	obfuscated
		[2:4]	[ SEQ ]					obfuscated
		[5:6]	[ SRC ]					obfuscated
		[7:8]	[ DST ]					encrypted
		[9...]	[ Transport PDU ]		encrypted
		[...]	[ NetMIC ]
	 */

	uint8_t*	p	= (uint8_t*)buffer;
		{
		Oscl::Encoder::BE::Base
		beHeaderEncoder(
			p,
			7
			);

		Oscl::Encoder::Api&	headerEncoder	= beHeaderEncoder;

		bool
		ivi	= (ivIndex & 0x00000001)?true:false;

		uint8_t
		iviNID	= (ivi)?0x80:0x00;
		iviNID	|= credentials._nid;

		headerEncoder.encode(iviNID);
		headerEncoder.encode(ctlTTL);
		headerEncoder.encode24Bit(seq);
		headerEncoder.encode(src);
		}

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s pre-obfuscation cipherText: ctlTTL: 0x%2.2X\n",
		__PRETTY_FUNCTION__,
		ctlTTL
		);

	Oscl::Error::Info::hexDump(
		p,
		length+dstOffset
		);
	#endif

	// Obfuscation
	uint8_t	pecbData[16];
		{

		Oscl::AES128::Cipher	pecb(credentials._privacyKey);

		Oscl::Encoder::BE::Base
		bePecbEncoder(
			pecbData,
			sizeof(pecbData)
			);

		Oscl::Encoder::Api&	pecbEncoder	= bePecbEncoder;

		static const uint8_t	zeros[5] = {0x00,0x00,0x00,0x00,0x00};

		constexpr unsigned privacyRandomLength	= 7;

		pecbEncoder.copyIn(
			zeros,
			sizeof(zeros)
			);

		pecbEncoder.encode(ivIndex);

		pecbEncoder.copyIn(
			cipherText,
			privacyRandomLength
			);

		pecb.cipher(pecbData);

		pecb.xorBlock(
			&p[obfuscatedDataOffset],
			&p[obfuscatedDataOffset],
			obfuscatedDataLength
			);
		}

	return obfuscatedDataOffset + obfuscatedDataLength + length;
	}
