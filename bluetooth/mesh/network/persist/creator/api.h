/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_bluetooth_mesh_network_persist_creator_apih_
#define _oscl_bluetooth_mesh_network_persist_creator_apih_

#include "oscl/bluetooth/mesh/network/persist/api.h"
#include "oscl/bluetooth/mesh/network/persist/iterator/api.h"

/** */
namespace Oscl {
/** */
namespace BT {
/** */
namespace Mesh {
/** */
namespace Network {
/** */
namespace Persist {
/** */
namespace Creator {

/** */
class Api {
	public:
		/**	Create a new/initialized persistence record.
			RETURN: 0 if out-of-resources or other failure.
		 */
		virtual Oscl::BT::Mesh::Network::Persist::Api*
					create(
						const uint8_t	netKey[16],
						const uint8_t	newKey[16],
						uint16_t		keyIndex,
						uint8_t			nNodeIdentityState,
						uint8_t			friendState,
						uint8_t			networkKeyPhase,
						uint16_t		heartbeatSubscriptionSource,
						uint16_t		heartbeatSubscriptionDestination,
						uint8_t			heartbeatSubscriptionPeriodLog,
						uint8_t			networkTransmitCount,
						uint8_t			networkTransmitIntervalSteps,
						uint8_t			defaultTTL,
						bool			relayEnabled,
						uint8_t			relayRetransmitCount,
						uint8_t			relayRetransmitIntervalSteps,
						uint16_t		heartbeatPublishDestination,
						uint16_t		heartbeatPublishCount,
						uint8_t			heartbeatPublishPeriodLog,
						uint8_t			heartbeatPublishTtl,
						uint16_t		heartbeatPublishFeatures
						) noexcept=0;

		/** Destroy the persistence associated.
		 */
		virtual void destroy(Oscl::BT::Mesh::Network::Persist::Api& p) noexcept=0;

		/** Iterate the existing persistence.
		 */
		virtual void	iterate(
							Oscl::BT::Mesh::Network::Persist::Iterator::Api&	iterator
							) noexcept=0;

		/** */
		virtual void	write(
							Oscl::BT::Mesh::
							Network::Persist::Api&	api
							) noexcept=0;
	};

}
}
}
}
}
}

#endif
