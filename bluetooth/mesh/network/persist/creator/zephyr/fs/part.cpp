/*
   Copyright (C) 2020 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <new>
#include <string.h>
#include <strings.h>
#include <stdlib.h>
#include <stdio.h>

#include "part.h"
#include "oscl/error/info.h"
#include "oscl/strings/hex.h"
#include "oscl/strings/base.h"
#include "oscl/strings/fixed.h"
#include "oscl/zephyr/fs/file/scope.h"
#include "oscl/zephyr/fs/file/fgets.h"

using namespace Oscl::BT::Mesh::Network::Persist::Creator;

Part::Part(
	Oscl::Persist::
	Parent::Zephyr::FS::Part&	parent
	) noexcept:
		_parent(parent)
		{

	for(unsigned i=0;i<nFileMem;++i){
		_freeFileMem.put(&_fileMem[i]);
		}

	createNetworks();
	}

Part::~Part() noexcept{
	destroyNetworkFiles();
	}

static void	convertBinaryToHexAsciiLabel(
				const uint8_t*	label,
				char*			asciiHex
				) noexcept{
	for(unsigned i=0;i<16;++i){
		Oscl::Strings::octetToLowerCaseHexAscii(
			label[i],
			asciiHex[(i*2)],	// msn
			asciiHex[(i*2)+1]	// lsn
			);
		}
	}

Oscl::BT::Mesh::Network::Persist::Api*
	Part::create(
			const uint8_t	netKey[16],
			const uint8_t	newKey[16],
			uint16_t		keyIndex,
			uint8_t			nodeIdentityState,
			uint8_t			friendState,
			uint8_t			networkKeyPhase,
			uint16_t		heartbeatSubscriptionSource,
			uint16_t		heartbeatSubscriptionDestination,
			uint8_t			heartbeatSubscriptionPeriodLog,
			uint8_t			networkTransmitCount,
			uint8_t			networkTransmitIntervalSteps,
			uint8_t			defaultTTL,
			bool			relayEnabled,
			uint8_t			relayRetransmitCount,
			uint8_t			relayRetransmitIntervalSteps,
			uint16_t		heartbeatPublishDestination,
			uint16_t		heartbeatPublishCount,
			uint8_t			heartbeatPublishPeriodLog,
			uint8_t			heartbeatPublishTtl,
			uint16_t		heartbeatPublishFeatures
			) noexcept{
	FileMem*
	mem	= _freeFileMem.get();
	if(!mem){
		return 0;
		}

	File*
	persist	= new(mem) File(
				*this,
				0,
				netKey,
				newKey,
				keyIndex,
				nodeIdentityState,
				friendState,
				networkKeyPhase,
				heartbeatSubscriptionSource,
				heartbeatSubscriptionDestination,
				heartbeatSubscriptionPeriodLog,
				networkTransmitCount,
				networkTransmitIntervalSteps,
				defaultTTL,
				relayEnabled,
				relayRetransmitCount,
				relayRetransmitIntervalSteps,
				heartbeatPublishDestination,
				heartbeatPublishCount,
				heartbeatPublishPeriodLog,
				heartbeatPublishTtl,
				heartbeatPublishFeatures
				);

	char	netKeyArray[33];

	netKeyArray[0]	= '\0';
	netKeyArray[sizeof(netKeyArray)-1]	= '\0';

	convertBinaryToHexAsciiLabel(
		netKey,
		netKeyArray
		);

	char	newKeyArray[33];

	newKeyArray[0]	= '\0';
	newKeyArray[sizeof(newKeyArray)-1]	= '\0';

	convertBinaryToHexAsciiLabel(
		newKey,
		newKeyArray
		);

	char	buffer[1024];

	Oscl::Strings::Base
	path(
		buffer,
		sizeof(buffer)
		);

	_parent.buildPath(path);

	unsigned
	offset	= path.length();
	if(offset){
		}

	path += "networkXXXXXX";

	if(path.truncated()){
		Oscl::Error::Info::log(
			"%s: trunacated path: %s\n",
			OSCL_PRETTY_FUNCTION,
			path.getString()
			);
		_freeFileMem.put(mem);
		return 0;
		}

	// /home/mike/root/src/oscl/bluetooth/mesh/node/persist/creator/zephyr/fs/part.cpp
	Oscl::Zephyr::FS::File::Scope	f;

	bool
	failed	= f.mkstemp(buffer);

	if(failed){
		Oscl::Error::Info::log(
			"%s: mkstemp(%s) failed. errno: %d\n",
			OSCL_PRETTY_FUNCTION,
			buffer,
			f.openResult()
			);
		_freeFileMem.put(mem);
		return 0;
		}

	strncpy(
		persist->_fileName,
		&buffer[offset],
		sizeof(persist->_fileName)
		);

	persist->_fileName[sizeof(persist->_fileName)-1]	= '\0';

	persist->write(f);

	_fileList.put(persist);

	return persist;
	}

void	Part::destroy(Oscl::BT::Mesh::Network::Persist::Api& p) noexcept{

	Oscl::BT::Mesh::Network::
	Persist::Creator::File*	persistApi	= find(p);

	if(!persistApi){
		Oscl::Error::Info::log(
			"%s: unknown persistence API.\n",
			OSCL_PRETTY_FUNCTION
			);
		return;
		}

	char	buffer[1024];

	Oscl::Strings::Base
	path(
		buffer,
		sizeof(buffer)
		);

	_parent.buildPath(path);

	path	+= persistApi->_fileName;

	int
	result	= fs_unlink(path.getString());

	if(result){
		Oscl::Error::Info::log(
			"%s fs_unlink() failed: %d\n",
			OSCL_PRETTY_FUNCTION,
			result
			);
		}

	_fileList.remove(persistApi);

	persistApi->~File();

	_freeFileMem.put((FileMem*)persistApi);
	}

void	Part::iterate(
			Oscl::BT::Mesh::Network::Persist::Iterator::Api&	iterator
			) noexcept{

	for(
		File*	persist	= _fileList.first();
		persist;
		persist	= _fileList.next(persist)
		){
		bool
		haltIteration	= iterator.item(
							*persist,
							persist->_netKey,
							persist->_newKey,
							persist->_keyIndex,
							persist->_nodeIdentityState,
							persist->_friendState,
							persist->_networkKeyPhase,
							persist->_heartbeatSubscriptionSource,
							persist->_heartbeatSubscriptionDestination,
							persist->_heartbeatSubscriptionPeriodLog,
							persist->_networkTransmitCount,
							persist->_networkTransmitIntervalSteps,
							persist->_defaultTTL,
							persist->_relayEnabled,
							persist->_relayRetransmitCount,
							persist->_relayRetransmitIntervalSteps,
							persist->_heartbeatPublishDestination,
							persist->_heartbeatPublishCount,
							persist->_heartbeatPublishPeriodLog,
							persist->_heartbeatPublishTtl,
							persist->_heartbeatPublishFeatures
							);
		if(haltIteration){
			return;
			}
		}
	}

void	Part::createNetwork(
			const char*		fileName,
			const uint8_t	netKey[16],
			const uint8_t	newKey[16],
			uint16_t		keyIndex,
			uint8_t			nodeIdentityState,
			uint8_t			friendState,
			uint8_t			networkKeyPhase,
			uint16_t		heartbeatSubscriptionSource,
			uint16_t		heartbeatSubscriptionDestination,
			uint8_t			heartbeatSubscriptionPeriodLog,
			uint8_t			networkTransmitCount,
			uint8_t			networkTransmitIntervalSteps,
			uint8_t			defaultTTL,
			bool			relayEnabled,
			uint8_t			relayRetransmitCount,
			uint8_t			relayRetransmitIntervalSteps,
			uint16_t		heartbeatPublishDestination,
			uint16_t		heartbeatPublishCount,
			uint8_t			heartbeatPublishPeriodLog,
			uint8_t			heartbeatPublishTtl,
			uint16_t		heartbeatPublishFeatures
			) noexcept{

	FileMem*
	mem	= _freeFileMem.get();
	if(!mem){
		Oscl::Error::Info::log(
			"%s: out of FileMem.\n",
			OSCL_PRETTY_FUNCTION
			);
		return;
		}

	File*
	persist	= new(mem) File(
				*this,
				fileName,
				netKey,
				newKey,
				keyIndex,
				nodeIdentityState,
				friendState,
				networkKeyPhase,
				heartbeatSubscriptionSource,
				heartbeatSubscriptionDestination,
				heartbeatSubscriptionPeriodLog,
				networkTransmitCount,
				networkTransmitIntervalSteps,
				defaultTTL,
				relayEnabled,
				relayRetransmitCount,
				relayRetransmitIntervalSteps,
				heartbeatPublishDestination,
				heartbeatPublishCount,
				heartbeatPublishPeriodLog,
				heartbeatPublishTtl,
				heartbeatPublishFeatures
				);

	_fileList.put(persist);
	}

void	Part::createNetworkFromFile(
			const char*	filePath,
			const char*	fileName
			) noexcept{

	static const char	prefix[] = {"network"};

	bool
	different	= strncmp(
					prefix,
					fileName,
					sizeof(prefix)-1
					);

	if(different){
		return;
		}

	Oscl::Zephyr::FS::File::Scope
		f(	filePath,
			false	// create
			);

	if(f.openResult()) {
		Oscl::Error::Info::log(
			"%s\topen() failed. errno: %d\n",
			OSCL_PRETTY_FUNCTION,
			f.openResult()
			);
		return;
		}

	uint8_t		netKey[16];
	bool		netKeyIsValid	= false;

	uint8_t		newKey[16];
	bool		newKeyIsValid	= false;

	uint16_t	keyIndex		= 0;
	bool		keyIndexIsValid	= false;

	uint8_t		nodeIdentityState			= 0;
	bool		nodeIdentityStateIsValid	= false;

	uint8_t		friendState			= 0;
	bool		friendStateIsValid	= false;

	uint8_t		networkKeyPhase			= 0;
	bool		networkKeyPhaseIsValid	= false;

	uint16_t	heartbeatSubscriptionSource			= 0;
	bool		heartbeatSubscriptionSourceIsValid	= false;

	uint16_t	heartbeatSubscriptionDestination		= 0;
	bool		heartbeatSubscriptionDestinationIsValid	= false;

	uint8_t		heartbeatSubscriptionPeriodLog			= 0;
	bool		heartbeatSubscriptionPeriodLogIsValid	= false;

	uint8_t		networkTransmitCount		= 0;
	bool		networkTransmitCountIsValid	= false;

	uint8_t		networkTransmitIntervalSteps		= 0;
	bool		networkTransmitIntervalStepsIsValid	= false;

	uint8_t		defaultTTL			= 0;
	bool		defaultTTLIsValid	= false;

	uint8_t		relayEnabled		= 0;
	bool		relayEnabledIsValid	= false;

	uint8_t		relayRetransmitCount		= 0;
	bool		relayRetransmitCountIsValid	= false;

	uint8_t		relayRetransmitIntervalSteps	= 0;
	bool		relayRetransmitIntervalStepsIsValid	= false;

	uint16_t	heartbeatPublishDestination			= 0;
	bool		heartbeatPublishDestinationIsValid	= false;

	uint16_t	heartbeatPublishCount			= 0;
	bool		heartbeatPublishCountIsValid	= false;

	uint8_t		heartbeatPublishPeriodLog	= 0;
	bool		heartbeatPublishPeriodLogIsValid	= false;

	uint8_t		heartbeatPublishTtl	= 0;
	bool		heartbeatPublishTtlIsValid	= false;

	uint16_t	heartbeatPublishFeatures			= 0;
	bool		heartbeatPublishFeaturesIsValid	= false;

	char			buffer[1024];
	const char*		line;

	while((line = Oscl::Zephyr::FS::File::fgets(buffer,sizeof(buffer),f))){

		if(line[0] == '#'){
			// comment
			continue;
			}

		char*
		assignment	= index(buffer,'=');

		if(!assignment){
			continue;
			}

		assignment[0]	= '\0';

		char*
		value	= &assignment[1];

		char*
		newline = rindex(value,'\n');

		if(newline){
			*newline	= '\0';
			}

		if(!strcmp(line,"nodeIdentityState")){
			nodeIdentityState			= (uint8_t)strtoul(value,0,0);
			nodeIdentityStateIsValid	= true;
			}
		else if(!strcmp(line,"keyIndex")){
			keyIndex		= (uint16_t)strtoul(value,0,0);
			keyIndexIsValid	= true;
			}
		else if(!strcmp(line,"friendState")){
			friendState			= (uint8_t)strtoul(value,0,0);
			friendStateIsValid	= true;
			}
		else if(!strcmp(line,"networkKeyPhase")){
			networkKeyPhase			= (uint8_t)strtoul(value,0,0);
			networkKeyPhaseIsValid	= true;
			}
		else if(!strcmp(line,"heartbeatSubscriptionSource")){
			heartbeatSubscriptionSource			= (uint16_t)strtoul(value,0,0);
			heartbeatSubscriptionSourceIsValid	= true;
			}
		else if(!strcmp(line,"heartbeatSubscriptionDestination")){
			heartbeatSubscriptionDestination		= (uint16_t)strtoul(value,0,0);
			heartbeatSubscriptionDestinationIsValid	= true;
			}
		else if(!strcmp(line,"heartbeatSubscriptionPeriodLog")){
			heartbeatSubscriptionPeriodLog		= (uint8_t)strtoul(value,0,0);
			heartbeatSubscriptionPeriodLogIsValid	= true;
			}
		else if(!strcmp(line,"networkTransmitCount")){
			networkTransmitCount		= (uint8_t)strtoul(value,0,0);
			networkTransmitCountIsValid	= true;
			}
		else if(!strcmp(line,"networkTransmitIntervalSteps")){
			networkTransmitIntervalSteps		= (uint8_t)strtoul(value,0,0);
			networkTransmitIntervalStepsIsValid	= true;
			}
		else if(!strcmp(line,"defaultTTL")){
			defaultTTL		= (uint8_t)strtoul(value,0,0);
			defaultTTLIsValid	= true;
			}
		else if(!strcmp(line,"relayEnabled")){
			relayEnabled		= (uint8_t)strtoul(value,0,0);
			relayEnabledIsValid	= true;
			}
		else if(!strcmp(line,"relayRetransmitCount")){
			relayRetransmitCount		= (uint8_t)strtoul(value,0,0);
			relayRetransmitCountIsValid	= true;
			}
		else if(!strcmp(line,"relayRetransmitIntervalSteps")){
			relayRetransmitIntervalSteps		= (uint8_t)strtoul(value,0,0);
			relayRetransmitIntervalStepsIsValid	= true;
			}
		else if(!strcmp(line,"heartbeatPublishDestination")){
			heartbeatPublishDestination		= (uint16_t)strtoul(value,0,0);
			heartbeatPublishDestinationIsValid	= true;
			}
		else if(!strcmp(line,"heartbeatPublishCount")){
			heartbeatPublishCount		= (uint16_t)strtoul(value,0,0);
			heartbeatPublishCountIsValid	= true;
			}
		else if(!strcmp(line,"heartbeatPublishPeriodLog")){
			heartbeatPublishPeriodLog		= (uint8_t)strtoul(value,0,0);
			heartbeatPublishPeriodLogIsValid	= true;
			}
		else if(!strcmp(line,"heartbeatPublishTtl")){
			heartbeatPublishTtl			= (uint8_t)strtoul(value,0,0);
			heartbeatPublishTtlIsValid	= true;
			}
		else if(!strcmp(line,"heartbeatPublishFeatures")){
			heartbeatPublishFeatures		= (uint16_t)strtoul(value,0,0);
			heartbeatPublishFeaturesIsValid	= true;
			}
		else if(!strcmp(line,"netKey")){
			
			unsigned
			len	= Oscl::Strings::hexAsciiStringToBinary(
					value,	// hexAscii
					netKey,
					sizeof(netKey)
					);

			if((len > 0) && (len !=16)){
				Oscl::Error::Info::log(
					"%s: (len(%u) > 0) && (len !=16), expected zero or 16.\n",
					OSCL_PRETTY_FUNCTION,
					len
					);
				continue;
				}

			netKeyIsValid	= true;
			}
		else if(!strcmp(line,"newKey")){
			
			unsigned
			len	= Oscl::Strings::hexAsciiStringToBinary(
					value,	// hexAscii
					newKey,
					sizeof(newKey)
					);

			if((len > 0) && (len !=16)){
				Oscl::Error::Info::log(
					"%s: (len(%u) > 0) && (len !=16), expected zero or 16.\n",
					OSCL_PRETTY_FUNCTION,
					len
					);
				continue;
				}

			newKeyIsValid	= true;
			}
		}

	if(!netKeyIsValid){
		Oscl::Error::Info::log(
			"%s: netKey is missing.\n",
			OSCL_PRETTY_FUNCTION
			);
		return;
		}

	if(!keyIndexIsValid){
		Oscl::Error::Info::log(
			"%s: keyIndex is missing.\n",
			OSCL_PRETTY_FUNCTION
			);
		return;
		}

	if(!nodeIdentityStateIsValid){
		Oscl::Error::Info::log(
			"%s: nodeIdentityStateIsValid is missing.\n",
			OSCL_PRETTY_FUNCTION
			);
		return;
		}

	if(!friendStateIsValid){
		Oscl::Error::Info::log(
			"%s: friendStateIsValid is missing.\n",
			OSCL_PRETTY_FUNCTION
			);
		return;
		}

	if(!networkKeyPhaseIsValid){
		Oscl::Error::Info::log(
			"%s: networkKeyPhaseIsValid is missing.\n",
			OSCL_PRETTY_FUNCTION
			);
		return;
		}

	if(!newKeyIsValid){
		/*	For backward compatibility, we allow
			this if the key phase is zero.
		 */
		if(networkKeyPhase != 0){
			Oscl::Error::Info::log(
				"%s: newKey is missing, and networkKeyPhase != 0\n",
				OSCL_PRETTY_FUNCTION
				);
			return;
			}
		}

	if(!heartbeatSubscriptionSourceIsValid){
		Oscl::Error::Info::log(
			"%s: heartbeatSubscriptionSourceIsValid is missing.\n",
			OSCL_PRETTY_FUNCTION
			);
		return;
		}

	if(!heartbeatSubscriptionDestinationIsValid){
		Oscl::Error::Info::log(
			"%s: heartbeatSubscriptionDestinationIsValid is missing.\n",
			OSCL_PRETTY_FUNCTION
			);
		return;
		}

	if(!heartbeatSubscriptionPeriodLogIsValid){
		Oscl::Error::Info::log(
			"%s: heartbeatSubscriptionPeriodLogIsValid is missing.\n",
			OSCL_PRETTY_FUNCTION
			);
		return;
		}

	if(!networkTransmitCountIsValid){
		Oscl::Error::Info::log(
			"%s: networkTransmitCountIsValid is missing.\n",
			OSCL_PRETTY_FUNCTION
			);
		return;
		}

	if(!networkTransmitIntervalStepsIsValid){
		Oscl::Error::Info::log(
			"%s: networkTransmitIntervalStepsIsValid is missing.\n",
			OSCL_PRETTY_FUNCTION
			);
		return;
		}

	if(!defaultTTLIsValid){
		Oscl::Error::Info::log(
			"%s: defaultTTL is missing.\n",
			OSCL_PRETTY_FUNCTION
			);
		return;
		}

	if(!relayEnabledIsValid){
		Oscl::Error::Info::log(
			"%s: relayEnabled is missing.\n",
			OSCL_PRETTY_FUNCTION
			);
		return;
		}

	if(!relayRetransmitCountIsValid){
		Oscl::Error::Info::log(
			"%s: relayRetransmitCount is missing.\n",
			OSCL_PRETTY_FUNCTION
			);
		return;
		}

	if(!relayRetransmitIntervalStepsIsValid){
		Oscl::Error::Info::log(
			"%s: relayRetransmitIntervalSteps is missing.\n",
			OSCL_PRETTY_FUNCTION
			);
		return;
		}

	if(!heartbeatPublishDestinationIsValid){
		Oscl::Error::Info::log(
			"%s: heartbeatPublishDestination is missing.\n",
			OSCL_PRETTY_FUNCTION
			);
		return;
		}

	if(!heartbeatPublishCountIsValid){
		Oscl::Error::Info::log(
			"%s: heartbeatPublishCount is missing.\n",
			OSCL_PRETTY_FUNCTION
			);
		return;
		}

	if(!heartbeatPublishPeriodLogIsValid){
		Oscl::Error::Info::log(
			"%s: heartbeatPublishPeriodLog is missing.\n",
			OSCL_PRETTY_FUNCTION
			);
		return;
		}

	if(!heartbeatPublishTtlIsValid){
		Oscl::Error::Info::log(
			"%s: heartbeatPublishTtl is missing.\n",
			OSCL_PRETTY_FUNCTION
			);
		return;
		}

	if(!heartbeatPublishFeaturesIsValid){
		Oscl::Error::Info::log(
			"%s: heartbeatPublishFeatures is missing.\n",
			OSCL_PRETTY_FUNCTION
			);
		return;
		}

	createNetwork(
		fileName,
		netKey,
		newKey,
		keyIndex,
		nodeIdentityState,
		friendState,
		networkKeyPhase,
		heartbeatSubscriptionSource,
		heartbeatSubscriptionDestination,
		heartbeatSubscriptionPeriodLog,
		networkTransmitCount,
		networkTransmitIntervalSteps,
		defaultTTL,
		relayEnabled,
		relayRetransmitCount,
		relayRetransmitIntervalSteps,
		heartbeatPublishDestination,
		heartbeatPublishCount,
		heartbeatPublishPeriodLog,
		heartbeatPublishTtl,
		heartbeatPublishFeatures
		);
	}

void	Part::destroyNetworkFile(
			const char*	filePath,
			const char*	fileName
			) noexcept{

	#ifdef DEBUG_TRACE
	Oscl::Error::Info::log(
		"%s: %s\n",
		__PRETTY_FUNCTION__,
		filePath
		);
	#endif

	static const char	prefix[] = {"network"};

	bool
	different	= strncmp(
					prefix,
					fileName,
					sizeof(prefix)-1
					);

	if(different){
		return;
		}

	int
	result	= fs_unlink(filePath);

	if(result){
		Oscl::Error::Info::log(
			"%s fs_unlink() failed: %d\n",
			OSCL_PRETTY_FUNCTION,
			result
			);
		}
	}

void	Part::createNetworks() noexcept{

	Oscl::Persist::Parent::Zephyr::FS::Child::Composer<Part>
		iterator(
			*this,
			&Part::createNetworkFromFile
			);

	_parent.walkDir(iterator);
	}

void	Part::destroyNetworkFiles() noexcept{

	Oscl::Persist::Parent::Zephyr::FS::Child::Composer<Part>
		iterator(
			*this,
			&Part::destroyNetworkFile
			);

	_parent.walkDir(iterator);
	}

Oscl::BT::Mesh::Network::Persist::Creator::File*
	Part::find(Oscl::BT::Mesh::Network::Persist::Api&	api) noexcept{
	for(
		File*	persist	= _fileList.first();
		persist;
		persist	= _fileList.next(persist)
		){
		Oscl::BT::Mesh::Network::Persist::Api*
		persistApi	= persist;
		if(persistApi == &api){
			return persist;
			}
		}
	return 0;
	}

void	Part::write(
			Oscl::BT::Mesh::
			Network::Persist::Api&	api
			) noexcept{

	Oscl::BT::Mesh::Network::
	Persist::Creator::File*	persistApi	= find(api);

	if(!persistApi){
		Oscl::Error::Info::log(
			"%s: unknown persistence API.\n",
			OSCL_PRETTY_FUNCTION
			);
		}

	Oscl::Strings::Fixed<1024>	path;

	_parent.buildPath(path);

	path	+= persistApi->_fileName;

	if(path.truncated()){
		Oscl::Error::Info::log(
			"%s: trunacated path: %s\n",
			OSCL_PRETTY_FUNCTION,
			path.getString()
			);
		}

	Oscl::Zephyr::FS::File::Scope
		f(	path.getString(),
			true	// create
			);
	int
	result	= f.openResult();

	if(result){
		Oscl::Error::Info::log(
			"%s: open() failed %d\n",
			OSCL_PRETTY_FUNCTION,
			result
			);
		return;
		}

	result	= f.truncate(0);

	if(result){
		Oscl::Error::Info::log(
			"%s: truncate() failed %d\n",
			OSCL_PRETTY_FUNCTION,
			result
			);
		}

	persistApi->write(f);
	}

File::File(
	Oscl::BT::Mesh::
	Network::Persist::
	Creator::Api&	creatorApi,
	const char*		fileName,
	const uint8_t	netKey[16],
	const uint8_t	newKey[16],
	uint16_t		keyIndex,
	uint8_t			nodeIdentityState,
	uint8_t			friendState,
	uint8_t			networkKeyPhase,
	uint16_t		heartbeatSubscriptionSource,
	uint16_t		heartbeatSubscriptionDestination,
	uint8_t			heartbeatSubscriptionPeriodLog,
	uint8_t			networkTransmitCount,
	uint8_t			networkTransmitIntervalSteps,
	uint8_t			defaultTTL,
	bool			relayEnabled,
	uint8_t			relayRetransmitCount,
	uint8_t			relayRetransmitIntervalSteps,
	uint16_t		heartbeatPublishDestination,
	uint16_t		heartbeatPublishCount,
	uint8_t			heartbeatPublishPeriodLog,
	uint8_t			heartbeatPublishTtl,
	uint16_t		heartbeatPublishFeatures
	) noexcept:
		_creatorApi(creatorApi),
		_keyIndex(keyIndex),
		_nodeIdentityState(nodeIdentityState),
		_friendState(friendState),
		_networkKeyPhase(networkKeyPhase),
		_heartbeatSubscriptionSource(heartbeatSubscriptionSource),
		_heartbeatSubscriptionDestination(heartbeatSubscriptionDestination),
		_heartbeatSubscriptionPeriodLog(heartbeatSubscriptionPeriodLog),
		_networkTransmitCount(networkTransmitCount),
		_networkTransmitIntervalSteps(networkTransmitIntervalSteps),
		_defaultTTL(defaultTTL),
		_relayEnabled(relayEnabled),
		_relayRetransmitCount(relayRetransmitCount),
		_relayRetransmitIntervalSteps(relayRetransmitIntervalSteps),
		_heartbeatPublishDestination(heartbeatPublishDestination),
		_heartbeatPublishCount(heartbeatPublishCount),
		_heartbeatPublishPeriodLog(heartbeatPublishPeriodLog),
		_heartbeatPublishTtl(heartbeatPublishTtl),
		_heartbeatPublishFeatures(heartbeatPublishFeatures)
		{

	memcpy(
		_netKey,
		netKey,
		sizeof(_netKey)
		);


	memcpy(
		_newKey,
		newKey,
		sizeof(_newKey)
		);

	if(fileName){
		strncpy(
			_fileName,
			fileName,
			sizeof(_fileName)
			);
		}
	}

void	File::update() noexcept{
	_creatorApi.write(
		*this
		);
	}

void	File::setDefaultTTL(uint8_t ttl) noexcept{
	_defaultTTL	= ttl;

	update();
	}

void	File::setRelay(
			uint8_t state,
			uint8_t	retransmitCount,
			uint8_t	retransmitIntervalSteps
			) noexcept{
	_relayEnabled					= state?true:false;
	_relayRetransmitCount			= retransmitCount;
	_relayRetransmitIntervalSteps	= retransmitIntervalSteps;

	update();
	}

void	File::setNodeIdentityState(uint8_t state) noexcept{

	_nodeIdentityState	= state;

	update();
	}

void	File::setFriendState(uint8_t state) noexcept{

	_friendState	= state;

	update();
	}

void	File::setNetworkKeyPhase(uint8_t transition) noexcept{

	_networkKeyPhase	= transition;

	update();
	}

void	File::setNewKey(const uint8_t key[16]) noexcept{

	memcpy(
		_newKey,
		key,
		sizeof(_newKey)
		);

	update();
	}

void	File::setNetKey(const uint8_t key[16]) noexcept{

	memcpy(
		_netKey,
		key,
		sizeof(_netKey)
		);

	update();
	}

void	File::setHeartbeatSubscription(
			uint16_t	source,
			uint16_t	destination,
			uint8_t		periodLog
			) noexcept{

	_heartbeatSubscriptionSource		= source;
	_heartbeatSubscriptionDestination	= destination;
	_heartbeatSubscriptionPeriodLog		= periodLog;

	update();
	}

void	File::setNetworkTransmit(
			uint8_t	networkTransmitCount,
			uint8_t	networkTransmitIntervalSteps
			) noexcept{

	_networkTransmitCount			= networkTransmitCount;
	_networkTransmitIntervalSteps	= networkTransmitIntervalSteps;

	update();
	}

void	File::write(Oscl::FS::File::Api& f) noexcept{

	char	netKeyArray[33];

	netKeyArray[0]	= '\0';
	netKeyArray[sizeof(netKeyArray)-1]	= '\0';

	convertBinaryToHexAsciiLabel(
		_netKey,
		netKeyArray
		);

	char	newKeyArray[33];

	newKeyArray[0]	= '\0';
	newKeyArray[sizeof(newKeyArray)-1]	= '\0';

	convertBinaryToHexAsciiLabel(
		_newKey,
		newKeyArray
		);

	int
	result	= f.truncate(0);

	if(result){
		Oscl::Error::Info::log(
			"%s: truncate() failed %d\n",
			OSCL_PRETTY_FUNCTION,
			result
			);
		}

	char	buffer[1024];

	unsigned
	n	= snprintf(
			buffer,
			sizeof(buffer),
			"netKey=%s\n"
			"newKey=%s\n"
			"keyIndex=0x%4.4X\n"
			"nodeIdentityState=0x%2.2X\n"
			"friendState=0x%2.2X\n"
			"networkKeyPhase=0x%2.2X\n"
			"heartbeatSubscriptionSource=0x%4.4X\n"
			"heartbeatSubscriptionDestination=0x%4.4X\n"
			"heartbeatSubscriptionPeriodLog=0x%2.2X\n"
			"networkTransmitCount=0x%2.2X\n"
			"networkTransmitIntervalSteps=0x%2.2X\n"
			"defaultTTL=0x%2.2X\n"
			"relayEnabled=0x%2.2X\n"
			"relayRetransmitCount=0x%2.2X\n"
			"relayRetransmitIntervalSteps=0x%2.2X\n"
			"heartbeatPublishDestination=0x%4.4X\n"
			"heartbeatPublishCount=0x%4.4X\n"
			"heartbeatPublishPeriodLog=0x%2.2X\n"
			"heartbeatPublishTtl=0x%2.2X\n"
			"heartbeatPublishFeatures=0x%4.4X\n"
			"",
			netKeyArray,
			newKeyArray,
			_keyIndex,
			_nodeIdentityState,
			_friendState,
			_networkKeyPhase,
			_heartbeatSubscriptionSource,
			_heartbeatSubscriptionDestination,
			_heartbeatSubscriptionPeriodLog,
			_networkTransmitCount,
			_networkTransmitIntervalSteps,
			_defaultTTL,
			_relayEnabled,
			_relayRetransmitCount,
			_relayRetransmitIntervalSteps,
			_heartbeatPublishDestination,
			_heartbeatPublishCount,
			_heartbeatPublishPeriodLog,
			_heartbeatPublishTtl,
			_heartbeatPublishFeatures
			);

	buffer[sizeof(buffer)-1]	= '\0';

	if(n >= sizeof(buffer)){
		// truncated
		Oscl::Error::Info::log(
			"%s: snprintf() failed buffer too small.\n",
			OSCL_PRETTY_FUNCTION
			);
		}

	ssize_t
	writeResult	= f.write(
					buffer,
					n
					);

	if(writeResult < 0){
		Oscl::Error::Info::log(
			"%s: write() failed %d\n",
			OSCL_PRETTY_FUNCTION,
			writeResult
			);
		}
	else {
		unsigned long
		nWritten	= writeResult;

		if(nWritten < n){
			Oscl::Error::Info::log(
				"%s: write() failed. nWritten: %d, expected: %d\n",
				OSCL_PRETTY_FUNCTION,
				nWritten,
				n
				);
			}
		}
	}

