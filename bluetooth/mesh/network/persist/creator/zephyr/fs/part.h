/*
   Copyright (C) 2020 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_bluetooth_mesh_network_persist_creator_zephyr_fs_parth_
#define _oscl_bluetooth_mesh_network_persist_creator_zephyr_fs_parth_

#include "oscl/bluetooth/mesh/network/persist/creator/api.h"
#include "oscl/bluetooth/mesh/network/persist/api.h"
#include "oscl/bluetooth/mesh/network/persist/iterator/api.h"

#include "oscl/queue/queueitem.h"
#include "oscl/queue/queue.h"
#include "oscl/memory/block.h"
#include "oscl/persist/parent/zephyr/fs/part.h"
#include "oscl/fs/file/api.h"

/** */
namespace Oscl {
/** */
namespace BT {
/** */
namespace Mesh {
/** */
namespace Network {
/** */
namespace Persist {
/** */
namespace Creator {

/** */
class File :
		public Oscl::QueueItem,
		public Oscl::BT::Mesh::Network::Persist::Api
		{
	friend class Part;
	private:
		/** */
		Oscl::BT::Mesh::
		Network::Persist::
		Creator::Api&		_creatorApi;

	public:
		/** networkXXXXXX len = 13 */
		char			_fileName[13+1];

		/** */
		uint8_t			_netKey[16];

		/** */
		uint8_t			_newKey[16];

		/** */
		const uint16_t	_keyIndex;

		/** */
		uint8_t			_nodeIdentityState;

		/** */
		uint8_t			_friendState;

		/** */
		uint8_t			_networkKeyPhase;

		/** */
		uint16_t		_heartbeatSubscriptionSource;

		/** */
		uint16_t		_heartbeatSubscriptionDestination;

		/** */
		uint8_t			_heartbeatSubscriptionPeriodLog;

		/** */
		uint8_t			_networkTransmitCount;

		/** */
		uint8_t			_networkTransmitIntervalSteps;

		/** */
		uint8_t			_defaultTTL;

		/** */
		uint8_t			_relayEnabled;

		/** */
		uint8_t			_relayRetransmitCount;

		/** */
		uint8_t			_relayRetransmitIntervalSteps;

		/** */
		uint16_t		_heartbeatPublishDestination;

		/** */
		uint16_t		_heartbeatPublishCount;

		/** */
		uint8_t			_heartbeatPublishPeriodLog;

		/** */
		uint8_t			_heartbeatPublishTtl;

		/** */
		uint16_t		_heartbeatPublishFeatures;

	public:
		/** */
		File(
			Oscl::BT::Mesh::
			Network::Persist::
			Creator::Api&		creatorApi,
			const char*			fileName,
			const uint8_t		netKey[16],
			const uint8_t		newKey[16],
			uint16_t			keyIndex,
			uint8_t				nNodeIdentityState,
			uint8_t				friendState,
			uint8_t				networkKeyPhase,
			uint16_t			heartbeatSubscriptionSource,
			uint16_t			heartbeatSubscriptionDestination,
			uint8_t				heartbeatSubscriptionPeriodLog,
			uint8_t				networkTransmitCount,
			uint8_t				networkTransmitIntervalSteps,
			uint8_t				defaultTTL,
			bool				relayEnabled,
			uint8_t				relayRetransmitCount,
			uint8_t				relayRetransmitIntervalSteps,
			uint16_t			heartbeatPublishDestination,
			uint16_t			heartbeatPublishCount,
			uint8_t				heartbeatPublishPeriodLog,
			uint8_t				heartbeatPublishTtl,
			uint16_t			heartbeatPublishFeatures
			) noexcept;

		/** */
		void	setDefaultTTL(uint8_t ttl) noexcept;

		/** */
		void	setRelay(
					uint8_t state,
					uint8_t	retransmitCount,
					uint8_t	retransmitIntervalSteps
					) noexcept;

		/** */
		void	setNodeIdentityState(uint8_t state) noexcept;

		/** */
		void	setFriendState(uint8_t state) noexcept;

		/** */
		void	setNetworkKeyPhase(uint8_t transition) noexcept;

		/** */
		void	setNewKey(const uint8_t key[16]) noexcept;

		/** */
		void	setNetKey(const uint8_t key[16]) noexcept;

		/** */
		void	setHeartbeatSubscription(
					uint16_t	source,
					uint16_t	destination,
					uint8_t		periodLog
					) noexcept;

		/** */
		void	setNetworkTransmit(
					uint8_t	networkTransmitCount,
					uint8_t	networkTransmitIntervalSteps
					) noexcept;

	private:
		/** */
		void	update() noexcept;

		/** */
		void	write(Oscl::FS::File::Api& file) noexcept;
	};

union FileMem {
	void*	__qitemlink;
	Oscl::Memory::AlignedBlock<sizeof(File)>	mem;
	};


/** */
class Part : public Oscl::BT::Mesh::Network::Persist::Creator::Api {

	private:
		/** */
		Oscl::Persist::
		Parent::Zephyr::FS::Part&			_parent;

	private:
		/** */
		static constexpr unsigned	nFileMem	= 10;

		/** */
		FileMem							_fileMem[nFileMem];

		/** */
		Oscl::Queue<
			FileMem
			>							_freeFileMem;

		/** */
		Oscl::Queue<
			File
			>							_fileList;

	public:
		/** */
		Part(
			Oscl::Persist::
			Parent::Zephyr::FS::Part&	parent
			) noexcept;

		/** */
		~Part() noexcept;

	public: // Oscl::BT::Mesh::Network::Persist::Creator::Api
		/**	Create a new/initialized persistence record.
			RETURN: 0 if out-of-resources or other failure.
		 */
		Oscl::BT::Mesh::Network::Persist::Api*
			create(
				const uint8_t	netKey[16],
				const uint8_t	newKey[16],
				uint16_t		keyIndex,
				uint8_t			nNodeIdentityState,
				uint8_t			friendState,
				uint8_t			networkKeyPhase,
				uint16_t		heartbeatSubscriptionSource,
				uint16_t		heartbeatSubscriptionDestination,
				uint8_t			heartbeatSubscriptionPeriodLog,
				uint8_t			networkTransmitCount,
				uint8_t			networkTransmitIntervalSteps,
				uint8_t			defaultTTL,
				bool			relayEnabled,
				uint8_t			relayRetransmitCount,
				uint8_t			relayRetransmitIntervalSteps,
				uint16_t		heartbeatPublishDestination,
				uint16_t		heartbeatPublishCount,
				uint8_t			heartbeatPublishPeriodLog,
				uint8_t			heartbeatPublishTtl,
				uint16_t		heartbeatPublishFeatures
				) noexcept;

		/** Destroy the persistence associated.
		 */
		void destroy(Oscl::BT::Mesh::Network::Persist::Api& p) noexcept;

		/** Iterate the existing persistence.
		 */
		void	iterate(
					Oscl::BT::Mesh::Network::
					Persist::Iterator::Api&			iterator
					) noexcept;

	private:
		/** */
		void	createNetwork(
					const char*		fileName,
					const uint8_t	netKey[16],
					const uint8_t	newKey[16],
					uint16_t		keyIndex,
					uint8_t			nNodeIdentityState,
					uint8_t			friendState,
					uint8_t			networkKeyPhase,
					uint16_t		heartbeatSubscriptionSource,
					uint16_t		heartbeatSubscriptionDestination,
					uint8_t			heartbeatSubscriptionPeriodLog,
					uint8_t			networkTransmitCount,
					uint8_t			networkTransmitIntervalSteps,
					uint8_t			defaultTTL,
					bool			relayEnabled,
					uint8_t			relayRetransmitCount,
					uint8_t			relayRetransmitIntervalSteps,
					uint16_t		heartbeatPublishDestination,
					uint16_t		heartbeatPublishCount,
					uint8_t			heartbeatPublishPeriodLog,
					uint8_t			heartbeatPublishTtl,
					uint16_t		heartbeatPublishFeatures
					) noexcept;
	
		/** */
		void	createNetworkFromFile(
					const char*	filePath,
					const char*	fileName
					) noexcept;

		/** */
		void	destroyNetworkFile(
					const char*	filePath,
					const char*	fileName
					) noexcept;

		/** */
		void	createNetworks() noexcept;

		/** */
		void	destroyNetworkFiles() noexcept;

		/** */
		void	write(
					Oscl::BT::Mesh::
					Network::Persist::Api&	api
					) noexcept;

		/** */
		Oscl::BT::Mesh::Network::Persist::Creator::File*
			find(Oscl::BT::Mesh::Network::Persist::Api& api) noexcept;
	};

}
}
}
}
}
}

#endif
