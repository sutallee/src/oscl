/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_bluetooth_mesh_network_persist_iterator_apih_
#define _oscl_bluetooth_mesh_network_persist_iterator_apih_

#include "oscl/bluetooth/mesh/network/persist/api.h"

/** */
namespace Oscl {
/** */
namespace BT {
/** */
namespace Mesh {
/** */
namespace Network {
/** */
namespace Persist {
/** */
namespace Iterator {

/** */
class Api {
	public:
		/**	This iteration callback is invoked for each
			persistent item.

			RETURN: true to stop iteration or false
			to continue the iteration.
		 */
		virtual bool	item(
							Oscl::BT::Mesh::
							Network::Persist::Api&	api,
							const uint8_t			netKey[16],
							const uint8_t			newKey[16],
							uint16_t				keyIndex,
							uint8_t					nNodeIdentityState,
							uint8_t					friendState,
							uint8_t					networkKeyPhase,
							uint16_t				heartbeatSubscriptionSource,
							uint16_t				heartbeatSubscriptionDestination,
							uint8_t					heartbeatSubscriptionPeriodLog,
							uint8_t					networkTransmitCount,
							uint8_t					networkTransmitIntervalSteps,
							uint8_t					defaultTTL,
							bool					relayEnabled,
							uint8_t					relayRetransmitCount,
							uint8_t					relayRetransmitIntervalSteps,
							uint16_t				heartbeatPublishDestination,
							uint16_t				heartbeatPublishCount,
							uint8_t					heartbeatPublishPeriodLog,
							uint8_t					heartbeatPublishTtl,
							uint16_t				heartbeatPublishFeatures
							) noexcept=0;
	};

}
}
}
}
}
}

#endif
