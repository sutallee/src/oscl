/*
   Copyright (C) 2020 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/


#ifndef _oscl_bluetooth_mesh_network_constantsh_
#define _oscl_bluetooth_mesh_network_constantsh_

#include <stdint.h>

/** */
namespace Oscl {
/** */
namespace BT {
/** */
namespace Mesh {
/** */
namespace Network {

/** */
constexpr unsigned	ctlMicLength	= 64/8;

/** */
constexpr unsigned	netMicLength	= 32/8;

/** */
constexpr unsigned	dstOffset		= 7;

/** */
constexpr unsigned	obfuscatedDataOffset	= 1;

/** */
constexpr unsigned	obfuscatedDataLength	= 6;

/** */
constexpr unsigned	sequenceNumberOffset	= 2;

/** */
constexpr unsigned	ctlTtlOffset	= 1;

}
}
}
}

#endif
