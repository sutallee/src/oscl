/*
   Copyright (C) 2020 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangements/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#include <string.h>
#include "netkey.h"
#include "oscl/bluetooth/crypto/keys.h"
#include "oscl/bluetooth/crypto/salt.h"

using namespace Oscl::BT::Mesh::Network;

NetKey::NetKey(
	const uint8_t	netKey[16]
	) noexcept{

	memcpy(
		_netKey,
		netKey,
		16
		);

	generateMasterSecurityMaterial();
	generateNetworkID();
	generateIdentityKey();
	generateBeaconKey();
	}

void	NetKey::generateMasterSecurityMaterial() noexcept{

	uint8_t	k2Output[48];

	uint8_t	zero[1] = {0x00};

	Oscl::BT::Crypto::Keys::k2(
		_netKey,
		zero,
		sizeof(zero),
		k2Output
		);

	memcpy(
		_credentials._privacyKey,
		&k2Output[sizeof(k2Output) - 16],
		16)
		;

	memcpy(
		_credentials._encryptionKey,
		&k2Output[sizeof(k2Output) - 32],
		16
		);

	_credentials._nid	= k2Output[sizeof(k2Output) - 33] & 0x7f; /* 7-bit */
	}

void	NetKey::generateNetworkID() noexcept{

	Oscl::BT::Crypto::Keys::k3(
		_netKey,
		_networkID
		);

	}

void	NetKey::generateIdentityKey() noexcept{

	static const uint8_t	nkik[] = {'n','k','i','k'};

	uint8_t	salt[16];

	Oscl::BT::Crypto::Salt::s1(
		nkik,
		sizeof(nkik),
		salt,
		sizeof(salt)
		);

	static const uint8_t	P[] = {'i','d','1','2','8',0x01};

	Oscl::BT::Crypto::Keys::k1(
		_netKey,
		sizeof(_netKey),
		salt,
		P,
		sizeof(P),
		_identityKey
		);
	}

void	NetKey::generateBeaconKey() noexcept{

	static const uint8_t	nkik[] = {'n','k','b','k'};

	uint8_t	salt[16];

	Oscl::BT::Crypto::Salt::s1(
		nkik,
		sizeof(nkik),
		salt,
		sizeof(salt)
		);

	static const uint8_t	P[] = {'i','d','1','2','8',0x01};

	Oscl::BT::Crypto::Keys::k1(
		_netKey,
		sizeof(_netKey),
		salt,
		P,
		sizeof(P),
		_beaconKey
		);
	}

