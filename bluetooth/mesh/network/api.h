/*
   Copyright (C) 2019 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_bluetooth_mesh_network_apih_
#define _oscl_bluetooth_mesh_network_apih_

#include "oscl/frame/fwd/api.h"
#include "oscl/pdu/fwd/api.h"
#include "oscl/bluetooth/mesh/key/api.h"
#include "oscl/bluetooth/mesh/element/api.h"
#include "oscl/bluetooth/mesh/network/interface/api.h"

/** */
namespace Oscl {
/** */
namespace BT {
/** */
namespace Mesh {
/** */
namespace Network {

/** */
class Api {
	public:
		/** */
		virtual const void*	getNetKey() const noexcept=0;

		/** */
		virtual const void*	getBeaconKey() const noexcept=0;

		/** */
		virtual uint16_t	getKeyIndex() const noexcept=0;

		/** */
		virtual uint8_t		getFlags() const noexcept=0;

		/** */
		virtual uint32_t	getTxIvIndex() const noexcept=0;

		/** */
		virtual uint16_t	getUnicastAddress() const noexcept=0;

		/** */
		virtual const void*	getNetworkID() const noexcept=0;

		/** */
		virtual const void*	getIdentityKey() const noexcept=0;

		/** */
		virtual Oscl::BT::Mesh::Key::Api&	getAppKeyApi() noexcept=0;

		/** */
		virtual uint8_t	getNodeIdentityState() const noexcept=0;

		/** */
		virtual void	setNodeIdentityState(uint8_t state) noexcept=0;

		/** RETURN: 0 for failure. */
		virtual Oscl::BT::Mesh::Element::Api*	findElement(uint16_t unicastAddress) noexcept=0;

		/** */
		virtual bool	updateKey(const uint8_t key[16]) noexcept=0;

		/** */
		virtual bool	deleteNetwork() noexcept=0;

		/** RETURN: true for failure (friend not supported). */
		virtual bool	setFriendState(uint8_t state) noexcept=0;

		/** */
		virtual uint8_t	getFriendState() noexcept=0;

		/** */
		virtual void	setNetworkKeyPhase(uint8_t transition) noexcept=0;

		/** */
        virtual uint8_t	getNetworkKeyPhase() noexcept=0;

		/** */
		virtual void	getConfigHeartbeatPublicationStatus(
							uint16_t&	destination,
							uint8_t&	countLog,
							uint8_t&	periodLog,
							uint8_t&	ttl,
							uint16_t&	features
							) noexcept=0;

		/** RETURN: true on failure (invalid netKeyIndex) */
		virtual bool	setConfigHeartbeatPublication(
							uint16_t	destination,
							uint8_t		countLog,
							uint8_t		periodLog,
							uint8_t		ttl,
							uint16_t	features
							) noexcept=0;

		/** */
		virtual void	getConfigHeartbeatSubscriptionStatus(
							uint16_t&	source,
							uint16_t&	destination,
							uint8_t&	periodLog,
							uint8_t&	countLog,
							uint8_t&	minHops,
							uint8_t&	maxHops
							) noexcept=0;

		/** RETURN: true if invalid address */
		virtual bool	setConfigHeartbeatSubscription(
							uint16_t	source,
							uint16_t	destination,
							uint8_t		periodLog
							) noexcept=0;

		/** */
		virtual void	getLowPowerNodePollTimeout(
							uint16_t	lpnAddress,
							uint32_t&	pollTimeout
							) noexcept=0;

		/** */
		virtual void	getNetworkTransmit(
							uint8_t&	networkTransmitCount,
							uint8_t&	networkTransmitIntervalSteps
							) noexcept=0;

		/** */
		virtual void	setNetworkTransmit(
							uint8_t	networkTransmitCount,
							uint8_t	networkTransmitIntervalSteps
							) noexcept=0;

		/** */
		virtual void	setDefaultTTL(uint8_t defaultTTL) noexcept=0;

		/** */
		virtual void	setRelay(
							uint8_t relay,
							uint8_t retransmitCount,
							uint8_t retransmitIntervalSteps
							) noexcept=0;

		/** This operation attempts to decrypt the proxy frame
			and forward the result to the fwdApi.
			RETURN: true if the frame is successfully
			decrypted (belongs to this network) and forwarded.
		 */
		virtual bool	decryptProxy(
							Oscl::Frame::FWD::Api&	fwdApi,
							const void*				frame,
							unsigned int			length
							) const noexcept=0;

		/**	This operation encrypts a proxy message
			and forwards the result to the fwdApi.
		 */
		virtual void	encryptProxy(
							Oscl::Pdu::FWD::Api&	fwdApi,
							Oscl::Pdu::Pdu* 		pdu
							) noexcept=0;

		/** Process a received mesh network message.

			The ifApi pointer is optional.

			RETURN: true if the frame belongs to this
			network.
		 */
		virtual bool	rxNetwork(
					const void*					frame,
					unsigned 					length,
					Oscl::BT::Mesh::
					Network::Interface::Api*	ifApi	= 0
					) noexcept=0;

		/** */
		virtual void	getBeaconState(
							uint32_t&	ivIndex,
							bool&		ivUpdateFlag,
							bool&		keyRefreshFlag
							) const noexcept=0;

		/** */
		virtual void	ivIndexStateChanged() noexcept=0;
	};

}
}
}
}

#endif
