/*
   Copyright (C) 2020 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_bluetooth_mesh_network_friendship_apih_
#define _oscl_bluetooth_mesh_network_friendship_apih_

#include <stdint.h>

/** */
namespace Oscl {
/** */
namespace BT {
/** */
namespace Mesh {
/** */
namespace Network {
/** */
namespace Friendship {

/**	This interface is intended to be implemented
	by either a Friend or a LPN.
 */
class Api {
	public:
		/** Process a received upper transport
			layer PDU.

			This operation is used by the Network
			layer to forward messages that are
			received by the Network layer encrypted
			using the master key material.

			From a Friendship perspective, these are
			should only Friend Request and Offer
			messages.

			All other Friendship related messages are
			encrypted using the Friendship credentials
			and are received through a separate channel.
		 */
		virtual void	rxControl(
							const void*	upperTransportAccessPdu,
							unsigned	pduLength,
							uint32_t	ivIndex,
							uint32_t	seq,
							uint16_t	src,
							uint16_t	dst,
							uint8_t		ttl,
							uint8_t		opcode
							) noexcept=0;

		/** RETURN: true if the PDU was destined
			for a LPN.
		 */
		virtual bool	rxLpnPDU(
							const void*		frame,
							unsigned		length,
							uint32_t		ivIndex,
							uint32_t		seq,
							uint16_t		src,
							uint16_t		dst,
							bool			ctl,
							uint8_t			ttl
							) noexcept=0;

		/** Process a received mesh network message
			that may belong to the implementation.

			RETURN: true if the frame belongs to a
			the friendship implementation.
		 */
		virtual bool	rxNetwork(
					const void*	frame,
					unsigned 	length
					) noexcept=0;

		/** */
		virtual void	rxBeacon(
							uint32_t	ivIndex,
							bool		keyRefreshFlag,
							bool		ivUpdateFlag
							) noexcept=0;

		/** */
		virtual bool	wantsDestination(uint16_t address) const noexcept=0;
	};

}
}
}
}
}

#endif
