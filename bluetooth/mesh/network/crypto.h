/*
   Copyright (C) 2020 Michael Nelson Moran

   This file is part of OSCL.

   OSCL is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   OSCL is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
   License for more details.

   You should have received a copy of the GNU General Public License
   along with OSCL; see the file COPYING.  If not, write to the Free
   Software Foundation, 59 Temple Place - Suite 330, Boston, MA
   02111-1307, USA.

   To negotiate other licensing arrangments/agreements contact
   the copyright holder:

   Michael N. Moran
   218 Wilshire Terrace       mike@mnmoran.org
   White, GA, USA 30184       http://mnmoran.org
*/

#ifndef _oscl_bluetooth_mesh_network_cryptoh_
#define _oscl_bluetooth_mesh_network_cryptoh_

#include <stdint.h>
#include "oscl/bluetooth/mesh/node/api.h"

/** */
namespace Oscl {
/** */
namespace BT {
/** */
namespace Mesh {
/** */
namespace Network {
/** */
namespace Crypto {

/** */
struct Credentials {
	/** Section: 3.8.6.3.1 NID, Encryption Key, and Privacy Key

		NID || EncryptionKey || PrivacyKey = k2(NetKey, 0x00)

		The friendship security material is derived from the
		friendship security credentials using:

		NID || EncryptionKey || PrivacyKey = k2(NetKey, 0x01
		|| LPNAddress || FriendAddress || LPNCounter || FriendCounter)
	 */
	uint8_t		_nid;

	/** */
	uint8_t		_privacyKey[16];

	/** Section: 3.8.6.3.1 NID, Encryption Key, and Privacy Key

		NID || EncryptionKey || PrivacyKey = k2(NetKey, 0x00)

		The friendship security material is derived from the
		friendship security credentials using:

		NID || EncryptionKey || PrivacyKey = k2(NetKey, 0x01
		|| LPNAddress || FriendAddress || LPNCounter || FriendCounter)
	 */
	uint8_t		_encryptionKey[16];
	};

/**
	RETURN: zero if decryption failed. Otherwise,
	the length of valid data copied into the
	plaintext buffer.
 */
unsigned	decrypt(
				Oscl::BT::Mesh::Node::Api&	nodeApi,
				const Credentials&			credentials,
				const void*					frame,
				unsigned 					length,
				uint8_t						plaintext[128],
				uint32_t&					ivIndex,
				uint8_t&					ctl,
				uint8_t&					ttl,
				uint32_t&					seq,
				uint16_t&					src
				) noexcept;

/**
	RETURN: zero if encryption failed. Otherwise,
	the length of valid data copied into the
	buffer.
 */
unsigned	encrypt(
				const Credentials&	credentials,
				bool				ctl,
				uint8_t				ttl,
				uint16_t			src,
				uint32_t			seq,
				uint32_t			ivIndex,
				const void*			transportPDU,
				unsigned			tranportPduLength,
				void*				buffer,
				unsigned			bufferLen
				) noexcept;
}
}
}
}
}

#endif

